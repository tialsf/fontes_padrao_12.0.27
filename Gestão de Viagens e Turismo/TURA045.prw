#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE "TOTVS.CH"
#INCLUDE "TURA045.CH"

#DEFINE OPER_HISTOR		2
#DEFINE OPER_FATURA		3
#DEFINE OPER_CANCEL		6
#DEFINE TP_VENDA			1
#DEFINE TP_APURA			2	
#DEFINE TP_BREAK			3
#DEFINE TP_CREDITO		4		
#DEFINE GRIDMAXLIN 	99999		

Static _nOper 	:= 0 		// Operacao da rotina
Static cNFSNatur	:= ''		// Natureza para NFS
Static nHandle		:= 0
Static nSeconds		:= 0

/*/{Protheus.doc} TA45Histor
Fun��o para visualiza��o da Fatura

@type 		Function
@author 	Jos� Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7		
/*/
//+----------------------------------------------------------------------------------------

Function TA45Histor()

Local cTitulo       := ""
Local cPrograma     := ""
Local nOperation    := 0

If G84->G84_STATUS == "1"
	cTitulo	:= STR0020		//"Visualiza��o"
Else
	cTitulo	:= STR0021		//"Hist�rico de Fatura cancelada"
EndIf

_nOper      	:= OPER_HISTOR
cPrograma    	:= 'TURA045'
nOperation   	:= MODEL_OPERATION_VIEW
nRet         	:= FWExecView( cTitulo , cPrograma, nOperation, /*oDlg*/, {|| .T. } ,/*bOk*/ , /*nPercReducao*/, /*aEnableButtons*/, /*bCancel*/ , /*cOperatId*/, /*cToolBar*/,/* oModel*/ )
_nOper      	:= 0


Return


/*/{Protheus.doc} TA45Cancel
Fun��o de cancelamento da fatura

@type 		Function
@author 	Jos� Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------

Function TA45Cancel()

Local cTitulo 	:= ""
Local cPrograma	:= ""
Local aEnableBut 	:= {{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.T.,OemToAnsi(STR0022)},{.T.,OemToAnsi(STR0023)},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil} }//"Confirmar" //"Fechar"
Local nOperation	:= MODEL_OPERATION_UPDATE
Local bCancel		:= {|oModel| TA45NoAlt(oModel)}

_nOper      	:= OPER_CANCEL
cTitulo      	:= STR0008
cPrograma    	:= 'TURA045'
nRet         	:= FWExecView( cTitulo , cPrograma, nOperation, /*oDlg*/, {|| .T. } ,/*bOk*/ , /*nPercReducao*/, aEnableBut, bCancel, /*cOperatId*/, /*cToolBar*/,/* oModel*/ )
_nOper      	:= 0

Return


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Fun��o respons�vel pela cria��o do modelo de dados.

@type 		Function
@author 	Jose Domingos Caldana jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel   	:= Nil
Local oStruG84 	:= FWFormStruct(1, 'G84', /*bAvalCampo*/, .F./*lViewUsado*/)			
Local oStruG85 	:= FWFormStruct(1, 'G85', /*bAvalCampo*/, .F./*lViewUsado*/)	
Local oStruG86 	:= FWFormStruct(1, 'G86', /*bAvalCampo*/, .F./*lViewUsado*/)	
Local oStruG8E 	:= FWFormStruct(1, 'G8E', /*bAvalCampo*/, .F./*lViewUsado*/)

oStruG84:SetProperty('*', MODEL_FIELD_OBRIGAT, .F.)
oStruG85:SetProperty('*', MODEL_FIELD_OBRIGAT, .F.)
oStruG86:SetProperty('*', MODEL_FIELD_OBRIGAT, .F.)
oStruG8E:SetProperty('*', MODEL_FIELD_OBRIGAT, .F.)

oModel := MPFormModel():New('TURA045', /*bPreValidacao*/, /*bPosValidacao*/, { |oModel| TA45Grava( oModel )}/*bCommit*/, /*bCancel*/)

If TA44RTpFat() == TP_VENDA
	oModel:SetDescription( STR0001 ) //"Fatura de Vendas"
ElseIf TA44RTpFat() == TP_APURA
	oModel:SetDescription( STR0002 )  //"Fatura de Apura��es"
ElseIf TA44RTpFat() == TP_BREAK
	oModel:SetDescription( STR0024 )  //"Fatura de Breakage"
ElseIf TA44RTpFat() == TP_CREDITO
	oModel:SetDescription( STR0032 )  //"Fatura de Cr�dito"
EndIf

oModel:AddFields('G84_MASTER', /*cOwner*/, oStruG84 )
oModel:GetModel('G84_MASTER'):SetDescription( STR0003 ) // "Fatura"

oModel:AddGrid('G85_DETAIL', 'G84_MASTER', oStruG85, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)
oModel:SetRelation('G85_DETAIL', {{'G85_FILIAL', 'xFilial( "G85" )' }, { 'G85_PREFIX', 'G84_PREFIX' },{ 'G85_NUMFAT', 'G84_NUMFAT' } }, G85->(IndexKey(1)))


oModel:AddGrid('G86_DETAIL', 'G84_MASTER', oStruG86, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)
oModel:SetRelation('G86_DETAIL', {{'G86_FILIAL', 'xFilial( "G86" )' }, { 'G86_PREFIX', 'G84_PREFIX' },{ 'G86_NUMFAT', 'G84_NUMFAT' } }, G86->(IndexKey(1)))

oModel:AddGrid('G8E_DETAIL', 'G84_MASTER', oStruG8E, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)
oModel:SetRelation('G8E_DETAIL', {{'G8E_FILIAL', 'xFilial( "G8E" )' }, { 'G8E_PREFIX', 'G84_PREFIX' },{ 'G8E_NUMFAT', 'G84_NUMFAT' } }, G8E->(IndexKey(1)))

oModel:AddCalc('TOT_CALC' , 'G84_MASTER', 'G85_DETAIL',  'G85_VALOR' , 'TOT_MARCAD'	, 'FORMULA', {|oModel| TA45Total(oModel,'0')}	,, STR0011,{|oModel,nTotalAtual,xValor,lSomando| TA45Calc(oModel,nTotalAtual,xValor,lSomando,'0')} , 14, 2) //"Total da Fatura"
If TA44RTpFat() == TP_VENDA .Or. TA44RTpFat() == TP_BREAK .Or. TA44RTpFat() == TP_CREDITO
	oModel:AddCalc('TOT_CALC' , 'G84_MASTER', 'G85_DETAIL',  'G85_VALOR' , 'TOT_VENDA' 	, 'FORMULA', {|oModel| TA45Total(oModel,'1')}	,, STR0012+"(+)" ,{|oModel,nTotalAtual,xValor,lSomando| TA45Calc(oModel,nTotalAtual,xValor,lSomando,'1')} , 14, 2) //"Total de Vendas"
	oModel:AddCalc('TOT_CALC' , 'G84_MASTER', 'G85_DETAIL',  'G85_VALOR' , 'TOT_REEMB' 	, 'FORMULA', {|oModel| TA45Total(oModel,'2|5')}	,,  STR0013+"(-)" ,{|oModel,nTotalAtual,xValor,lSomando| TA45Calc(oModel,nTotalAtual,xValor,lSomando,'2')} , 14, 2) //"Total de Reembolso"
EndIf
oModel:AddCalc('TOT_CALC' , 'G84_MASTER', 'G85_DETAIL',  'G85_VALOR' , 'TOT_RECEIT'	, 'FORMULA', {|oModel| TA45Total(oModel,'3')}	,, STR0014+"(+)" ,{|oModel,nTotalAtual,xValor,lSomando| TA45Calc(oModel,nTotalAtual,xValor,lSomando,'3')} , 14, 2) //"Total de Receitas"
oModel:AddCalc('TOT_CALC' , 'G84_MASTER', 'G85_DETAIL',  'G85_VALOR' , 'TOT_ABATIM'	, 'FORMULA', {|oModel| TA45Total(oModel,'4')}	,, STR0015+"(-)" ,{|oModel,nTotalAtual,xValor,lSomando| TA45Calc(oModel,nTotalAtual,xValor,lSomando,'4')} , 14, 2) //"Total de Abatimento"

oModel:GetModel('G85_DETAIL' ):SetOptional(.T.)
oModel:GetModel('G86_DETAIL' ):SetOptional(.T.)
oModel:GetModel('G8E_DETAIL' ):SetOptional(.T.)

If _nOper == OPER_CANCEL
	oModel:GetModel("G84_MASTER"):SetOnlyView(.T.)
	oModel:GetModel("G85_DETAIL"):SetOnlyView(.T.)
	oModel:GetModel("G86_DETAIL"):SetOnlyView(.T.)
	oModel:GetModel("G8E_DETAIL"):SetOnlyView(.T.)
EndIf
	
oModel:GetModel( 'G85_DETAIL' ):SetMaxLine(GRIDMAXLIN)

If TA44RTpFat() == TP_VENDA .Or. TA44RTpFat() == TP_BREAK .Or. TA44RTpFat() == TP_CREDITO
	oModel:GetModel( 'G85_DETAIL' ):SetUniqueLine( { "G85_FILREF" , "G85_IDIF"},{|| TurHelp(TURX3Title("G85_FILIAL")+": "+oModel:GetModel( 'G85_DETAIL' ):GetValue("G85_FILREF")+TURX3Title("G85_IDIF")+": "+oModel:GetModel( 'G85_DETAIL' ):GetValue("G85_IDIF"),STR0039,STR0040)} ) //"Item n�o ser� faturado. Avaliar duplicidade.","Item Duplicado"
Else
	oModel:GetModel( 'G85_DETAIL' ):SetUniqueLine( { "G85_FILREF" , "G85_IDIFA"},{|| TurHelp(TURX3Title("G85_FILIAL")+": "+oModel:GetModel( 'G85_DETAIL' ):GetValue("G85_FILREF")+TURX3Title("G85_IDIF")+": "+oModel:GetModel( 'G85_DETAIL' ):GetValue("G85_IDIF"),STR0039,STR0040)} )
EndIf
	
oModel:SetVldActivate( { |oModel| TA45VldMod( oModel) } )
oModel:SetActivate( {|oModel| TA45LoadMd(oModel) } )

Return oModel


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Fun��o respons�vel pela defini��o da vis�o da Apura��o de Receita de Clientes.

@type 		Function
@author 	Jose Domingos Caldana jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function ViewDef()

Local oModel    := FWLoadModel('TURA045')
Local oStruG84  := FWFormStruct(2, 'G84', /*bAvalCampo*/, .F./*lViewUsado*/)	
Local oStruG85  := FWFormStruct(2, 'G85', /*bAvalCampo*/, .F./*lViewUsado*/)	
Local oStruG86  := FWFormStruct(2, 'G86', /*bAvalCampo*/, .F./*lViewUsado*/)	
Local oStruG8E  := FWFormStruct(2, 'G8E', /*bAvalCampo*/, .F./*lViewUsado*/)	
Local oView     := FWFormView():New()
Local oStruTot  := FWCalcStruct(oModel:GetModel('TOT_CALC' ))

oView:SetModel( oModel )

//Remo��o de campos da G84
If _nOper <> OPER_CANCEL .And. G84->G84_STATUS == "1"
	oStruG84:RemoveField('G84_DTCANC')
	oStruG84:RemoveField('G84_USUCAN')
	oStruG84:RemoveField('G84_NOMUSU')
EndIf

oStruG84:RemoveField('G84_FECHA')
oStruG84:RemoveField('G84_ITEMFE')
oStruG84:RemoveField('G84_ENVELE')

//Remo��o de campos da G85
oStruG85:RemoveField('G85_FILIAL')
oStruG85:RemoveField('G85_PREFIX')
oStruG85:RemoveField('G85_NUMFAT')
oStruG85:RemoveField('G85_TIPO')
oStruG85:RemoveField('G85_VLRAUX')

If _nOper <> OPER_CANCEL .And. G84->G84_STATUS == "1"
	oStruG85:RemoveField('G85_STATUA')
	oStruG85:RemoveField('G85_FATATU')
	oStruG85:RemoveField('G85_FILATU')
	oStruG85:RemoveField('G85_PRFATU')
EndIf

oStruG85:RemoveField('G85_FECHA')
oStruG85:RemoveField('G85_ITEMFE')

If TA44RTpFat() == TP_VENDA .Or. TA44RTpFat() == TP_BREAK .Or. TA44RTpFat() == TP_CREDITO
	oStruG85:RemoveField('G85_IDIFA')
	oStruG85:RemoveField('G85_CODAPU')
ElseIf TA44RTpFat() == TP_APURA
	oStruG85:RemoveField('G85_IDIF')
	oStruG85:RemoveField('G85_REGVEN')
	oStruG85:RemoveField('G85_ITVEND')
	oStruG85:RemoveField('G85_SEQIV')
	oStruG85:RemoveField('G85_DOC')
	oStruG85:RemoveField('G85_GRPPRD')
	oStruG85:RemoveField('G85_GRPDES')
	oStruG85:RemoveField('G85_SOLIC')
	oStruG85:RemoveField('G85_ITPRIN')
EndIf

//Remo��o de campos da G86
oStruG86:RemoveField('G86_FILIAL')
oStruG86:RemoveField('G86_PREFIX')
oStruG86:RemoveField('G86_NUMFAT')

//Remo��o de campos da G85
oStruG8E:RemoveField('G8E_FILIAL')
oStruG8E:RemoveField('G8E_PREFIX')
oStruG8E:RemoveField('G8E_NUMFAT')


oView:AddField('VIEW_G84', oStruG84, 'G84_MASTER')

oView:CreateHorizontalBox('GERAL',100)

oView:CreateFolder('PASTAS','GERAL')

oView:AddSheet('PASTAS','PASTA1',STR0003)  // "Fatura"
oView:CreateHorizontalBox('ID_PASTA_G84_MASTER',100,,,'PASTAS', 'PASTA1' )
oView:SetOwnerView('G84_MASTER','ID_PASTA_G84_MASTER')

oView:AddSheet('PASTAS','PASTA2',"Itens")  
oView:CreateHorizontalBox('ID_PASTA_ITF',88,,,'PASTAS', 'PASTA2' )
oView:CreateHorizontalBox('ID_PASTA_TOT',12,,,'PASTAS', 'PASTA2' )

oView:AddGrid('VIEW_G85' , oStruG85, 'G85_DETAIL')
oView:SetOwnerView('G85_DETAIL','ID_PASTA_ITF')

If TA44RTpFat() == TP_VENDA .Or. TA44RTpFat() == TP_BREAK .Or. TA44RTpFat() == TP_CREDITO
	oView:EnableTitleView("G85_DETAIL",STR0016)  //"Itens de Venda"
ElseIf TA44RTpFat() == TP_APURA
	oView:EnableTitleView("G85_DETAIL",STR0017) //Apura��es"
EndIf

oView:AddField('VIEW_CALC_1', oStruTot, 'TOT_CALC')
oView:SetOwnerView('TOT_CALC','ID_PASTA_TOT')

oView:AddGrid('VIEW_G86' , oStruG86, 'G86_DETAIL')
oView:AddGrid('VIEW_G8E' , oStruG8E, 'G8E_DETAIL')
oView:AddSheet('PASTAS','PASTA3',STR0005)  //"Detalhes"
oView:CreateHorizontalBox('ID_PASTA_G8E',60,,,'PASTAS', 'PASTA3' )
oView:CreateHorizontalBox('ID_PASTA_G86',40,,,'PASTAS', 'PASTA3' )
oView:SetOwnerView('G8E_DETAIL','ID_PASTA_G8E') 
oView:SetOwnerView('G86_DETAIL','ID_PASTA_G86')

oView:AddIncrementField( 'VIEW_G86', 'G86_SEQUEN' )
oView:AddIncrementField( 'VIEW_G85', 'G85_ITEM' )
oView:AddIncrementField( 'VIEW_G8E', 'G8E_SEQUEN' )

oView:EnableTitleView("G8E_DETAIL",STR0006) //"Financeiro e Fiscal"
oView:EnableTitleView("G86_DETAIL",STR0007) //"Quebras utilizadas"


oView:showUpdateMsg(.F.)
oView:SetInsertMessage(STR0010, STR0018) // "Faturamento" //"Fatura gerada com sucesso."

Return oView


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA45VLDMOD
Fun��o respons�vel pela valida��o do cancelamento e carga dos itens para faturamento.

@type 		Function
@author 	Jos� Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA45VldMod(oModel,lTodasFil,cSelFil,cSegNeg,cOperac,cMoeda)

Local lRet       := .T.

If _nOper == OPER_CANCEL 
	If G84->G84_STATUS == "2"
		Help( , , "TURA045_JACANCEL", , STR0019 , 1, 0) //"Fatura atual j� est� cancelada."
		lRet := .F.
	EndIf
EndIf

Return lRet


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA45LoadMd
Fun��o que carrega os dados no model

@type 		Function
@author 	Jos� Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA45LoadMd(oModel)

Local lRet       := .T.
Local oModelG84	:= oModel:GetModel('G84_MASTER')

If _nOper == OPER_CANCEL 
	
	oModel:GetModel("G84_MASTER"):SetOnlyView(.F.)

	oModelG84:LoadValue("G84_DTCANC"	,dDataBase					)
	oModelG84:LoadValue("G84_USUCAN"	,__cUserId					)
	oModelG84:LoadValue("G84_NOMUSU"	,cUserName					)
	
	oModel:GetModel("G84_MASTER"):SetOnlyView(.T.)		
EndIf

Return lRet


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA45Total
Fun��o sele��o de itens para totaliza��o

@type 		Function
@author 	Jos� Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA45Total(oModel,cTipo)

Local lRet			:= .F.
Local oModelG85	:= oModel:GetModel('G85_DETAIL')

If cTipo = '0'
	lRet := .T.
Else
	lRet :=  oModelG85:GetValue("G85_TIPO") $ cTipo
EndIf

Return lRet



//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA45Grava
Fun��o de grava��o do Model (Fatura)

@type 		Function
@author 	Jos� Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA45Grava( oModel )

Local oModelG84	:= oModel:GetModel('G84_MASTER')
Local oModelG85	:= oModel:GetModel('G85_DETAIL')
Local oView		:= FwViewActive()
Local nX
Local lRet			:= .T.
Local lTurBolFt	:= ExistBlock("TURBOLFT")
Local cDirPdf		:= ""
Local nPos			:= 0
Local aRv			:= {}
Local cAliasQry	:= ""
Local cAliasIV	:= ""
Local cConinu   	:= Space(TamSx3("G4C_CONINU")[1])
Local lTemAberto	:= .F.
Local cChaveG48	:= ""
Local cChaveG4C	:= ""
Local cAliasDupl	:= ""

Local aRecItens	:= {}
Local aRecCTB	:= {}
Local aAreaG84	:= {}
Local aAreaG85	:= {}

Local nRecG84	:= 0
Local nRecG85	:= 0
Local nRecG4C	:= 0
Local nRecG81	:= 0

Local lExist 	:= TURExistX1("TURA045C")
Local lOnLine 	:= .F.
Local lMostrLanc:= .F.
Local lAglutLanc:= .F.

If ( lExist )
	
	Pergunte("TURA045C",.F.)
	
	lOnLine 	:= mv_par01 == 1
	lMostrLanc	:= mv_par02 == 1
	lAglutLanc	:= mv_par03 == 1
	
Endif

If TA44RTpFat() == TP_VENDA .Or. TA44RTpFat() == TP_BREAK .Or. TA44RTpFat() == TP_CREDITO
	DbSelectArea("G4C")
	G4C->(DbsetOrder(2))
Else
	DbSelectArea("G81")
	G81->(DbsetOrder(1))
EndIf

DbSelectArea("G48")
G48->(DbsetOrder(4))

If _nOper == OPER_CANCEL
	
	FWMsgRun(,{|| lRet := TA45RunCan(oModel,oView,.t.,lMostrLanc,lAglutLanc)}, , STR0025 ) //"Aguarde... Cancelando fatura..."
	If lRet
		FwAlertSuccess(STR0009,STR0008) 
	Else
		FwAlertError(STR0034,STR0008)  //"N�o foi poss�vel cancelar a fatura."
	Endif	
Else
	

	T45Log( '1', '', AllTrim( oModelG84:GetValue( 'G84_PREFIX' ) ) + '_' + AllTrim( oModelG84:GetValue( 'G84_NUMFAT' ) ), nSeconds )
	
	//Valida G85 Duplicada (N�o retirar)
	//A mesma valida��o no TURA044 n�o inutiliza essa, pois h� suspeita de gera��o da duplidade ap�s a sele��o dos itens
	If TA44RTpFat() == TP_VENDA .Or. TA44RTpFat() == TP_BREAK .Or. TA44RTpFat() == TP_CREDITO
	
		T45Log( '2', 'TA45Grava', 'Validando G85', nSeconds )
	
		For nX := 1 To oModelG85:Length()
			oModelG85:GoLine(nX)
			If !oModelG85:IsDeleted() 
			
			cAliasDupl := GetNextAlias()
			
				BeginSql Alias cAliasDupl
					SELECT G85_FILIAL, G85_PREFIX, G85_NUMFAT FROM %Table:G85% G85
					INNER JOIN %Table:G84% G84 ON
					G84_FILIAL = G85_FILIAL AND
					G84_PREFIX = G85_PREFIX AND
					G84_NUMFAT = G85_NUMFAT AND
 					G84.%NotDel%
					WHERE
					G85_FILREF = %exp:oModelG85:GetValue("G85_FILREF",nX)% AND
					G85_IDIF = %exp:oModelG85:GetValue("G85_IDIF",nX)% AND  
					G84_STATUS = '1' AND 
					G85.%NotDel%
				EndSql
				
				If (cAliasDupl)->(! EOF())
					lRet := .F.
					T45Log( '2', 'TURA045_NOGRV', "Item seria faturado duplicado: " + oModelG85:GetValue("G85_FILREF",nX) + " - " + oModelG85:GetValue("G85_IDIF",nX))	
				EndIf
				
				(cAliasDupl)->(DbCloseArea())
				
				If !lRet
					Exit
				EndIf
				
			EndIf
		Next nX
		
	EndIf
	
	If lRet
		
		BEGIN TRANSACTION
		
		If TA45Consol(oModel,.F.)
		
			If FWFormCommit(oModel)
			
				//Captura os Registros das tabelas associadas
				aAreaG85 := G85->(GetArea()) 
				aAreaG84 := G84->(GetArea()) 
		
				//+----------------------------------------------------------a
				// Atualiza G4C e G81 com Numero da Fatura, Prefixo e Status
				//+----------------------------------------------------------
				T45Log( '2', 'TA45Grava', 'Avaliando G85', nSeconds )
	
				For nX := 1 To oModelG85:Length()
				
					oModelG85:GoLine(nX)
					
					If ( !oModelG85:IsDeleted() ) 
					
						If TA44RTpFat() == TP_VENDA .Or. TA44RTpFat() == TP_BREAK .Or. TA44RTpFat() == TP_CREDITO
							G4C->(DbsetOrder(4))
							//G4C_FILIAL+G4C_NUMID+G4C_IDITEM+G4C_NUMSEQ+G4C_IDIF+G4C_CONINU
							G4C->(DbSeek(xFilial("G4C",oModelG85:GetValue("G85_FILREF"))+oModelG85:GetValue("G85_REGVEN") + oModelG85:GetValue("G85_ITVEND") + oModelG85:GetValue("G85_SEQIV") + oModelG85:GetValue("G85_IDIF")+cConinu))
							RecLock("G4C",.F.)
								G4C->G4C_FILFAT := xFilial("G84") 
								G4C->G4C_PREFIX := oModelG84:GetValue("G84_PREFIX")
								G4C->G4C_NUMFAT := oModelG84:GetValue("G84_NUMFAT")
								G4C->G4C_STATUS := "4" //Finalizado
							G4C->(MsUnLock())
						
							T45Log( '2', '', 'Finalizando G4C, RecNo: ' + cValToChar( G4C->(RecNo()) ) + ' Indice 1:' + G4C->(&(IndexKey(1))) , nSeconds )
						
							nRecG4C := G4C->(Recno())
							
							nPos := Ascan(aRv,{|x| x[1]+x[2]+x[3]+x[4] == G4C->G4C_FILIAL + G4C->G4C_NUMID + G4C->G4C_IDITEM + G4C->G4C_NUMSEQ})
							
							If nPos <= 0 
								Aadd(aRv,{G4C->G4C_FILIAL,G4C->G4C_NUMID,G4C->G4C_IDITEM,G4C->G4C_NUMSEQ})
							EndIf
							
							If !Empty(G4C->G4C_CLASS)  
								cChaveG4C		:= G4C->(G4C_FILIAL+G4C_NUMID+G4C_IDITEM+G4C_NUMSEQ+G4C_APLICA+G4C_NUMACD)
								cChaveG48		:= xFilial("G48",oModelG85:GetValue("G85_FILREF"))+G4C->(G4C_NUMID+G4C_IDITEM+G4C_NUMSEQ+G4C_CLIFOR+G4C_APLICA+cConinu)
								lTemAberto		:= .F.
			
								G4C->(DbsetOrder(1))
								G4C->(DbSeek(cChaveG4C))
								While G4C->(!Eof()) .And. G4C->(G4C_FILIAL+G4C_NUMID+G4C_IDITEM+G4C_NUMSEQ+G4C_APLICA+G4C_NUMACD) == cChaveG4C
									If G4C->G4C_CLIFOR == '1' .And. G4C->G4C_STATUS <> '4' .And. Empty(G4C->G4C_CONINU)  //Cliente //Finalizado
										lTemAberto := .T.
										Exit
									EndIf
									G4C->(DbSkip())
								EndDo
								
								If !lTemAberto
									G48->(DbsetOrder(4))
									G48->(DbSeek(cChaveG48))
									RecLock("G48",.F.)
										G48->G48_STATUS := '4' //Finalizado
									G48->(MsUnLock())
									
									T45Log( '2', '', 'Finalizando G48, RecNo: ' + cValToChar( G48->(RecNo()) ) + ' Indice 1:' + G48->(&(IndexKey(1)))  , nSeconds )
								EndIf
							EndIf
							
						Else
							
							G81->(DbSeek(xFilial("G81",oModelG85:GetValue("G85_FILREF"))+oModelG85:GetValue("G85_IDIFA"))) 
						
							RecLock("G81",.F.)
						
								G81->G81_FILFAT := xFilial("G84") 
								G81->G81_PREFIX := oModelG84:GetValue("G84_PREFIX")
								G81->G81_NUMFAT := oModelG84:GetValue("G84_NUMFAT")
								G81->G81_STATUS := "2" //Finalizado
						
							G81->(MsUnLock())
			
							T45Log( '2', 'TA45Grava', 'Finalizando G81, RecNo: ' + cValToChar( G81->(RecNo()) ) + ' Indice 1:' + G81->(&(IndexKey(1)))   )
							nRecG81 := G81->(Recno())							
							
						EndIf
						
						G85->(DbSetOrder(1)) //G85_FILIAL+G85_PREFIX+G85_NUMFAT+G85_ITEM
						
						If ( G85->( DbSeek(XFilial("G85") + oModelG84:GetValue("G84_PREFIX")+oModelG84:GetValue("G84_NUMFAT")+oModelG85:GetValue("G85_ITEM",nX) ) ) )
							nRecG85 := G85->(Recno())
						Endif
							
						aAdd(aRecItens, {nRecG85,nRecG4C,nRecG81})
					
					EndIf
					
				Next nX
				
				G84->(DbSetOrder(1)) //G84_FILIAL+G84_PREFIX+G84_NUMFAT
				
				If ( G84->(DbSeek(XFilial("G84") + oModelG84:GetValue("G84_PREFIX") + oModelG84:GetValue("G84_NUMFAT")) ) )
					nRecG84 := G84->(Recno())
				Endif
				
				AAdd(aRecCTB,{nRecG84,AClone(aRecItens)})
				aRecItens := {}
				
				//Atualiza Status do IV e RV 
				For nX := 1 To Len(aRv)
					
					TurAtuStRV(aRv[nX,1], aRv[nX,2], aRv[nX,3], aRv[nX,4], cConinu, "1", .T. )
					
				Next
						
			Else
				DisarmTrans()
				
				lRet := .F.
	
				If ValType( oView ) == "O"
					oView:showInsertMsg( .F. )
					TurHelp(STR0026 + oModelG84:GetValue("G84_NUMFAT")+", " + STR0027 ,STR0041,'TURA045_NOGRV')
					T45Log( '2', 'TURA045_NOGRV', STR0026 + oModelG84:GetValue("G84_NUMFAT")+", " + STR0027 )
				EndIf
			EndIf
		Else
			DisarmTrans()
				
			lRet := .F.
	
			If ValType( oView ) == "O"
				oView:showInsertMsg( .F. )
				TurHelp(STR0026 + oModelG84:GetValue("G84_NUMFAT")+", " + STR0027,STR0042,'TURA045_NOGRV')
				T45Log( '2', 'TURA045_NOGRV', STR0026 + oModelG84:GetValue("G84_NUMFAT")+", " + STR0027 )
			EndIf
		
		EndIf
		
		END TRANSACTION
		
		T45Log( '2', 'TA45Grava', IIF( InTransaction(), 'ATEN��O: TRANSA��O EM ABERTO.', 'Transa��o finalizada com sucesso.') )
		
		T45Log( '3', '', '' )
	
	Else
		
		G84->(RollBackSXE())
		
		If ValType( oView ) == "O"
			oView:showInsertMsg( .F. )
			TurHelp(STR0026 + oModelG84:GetValue("G84_NUMFAT") ,STR0039,STR0040)
			T45Log( '2', 'TURA045_NOGRV', STR0026 + oModelG84:GetValue("G84_NUMFAT")+", " + STR0027 )
		EndIf
	EndIf
	
	
	If lRet

		//chamar a contabiliza��o do faturamento - tabelas G84/G85/G4C/G81
		If ( lOnline ) //Contabiliza��o On Line? Na Gera��o poder� ou n�o contabilizar online
			TurCtOnFat(lMostrLanc,lAglutLanc,.F.,"TURA045",aRecCTB)
		Endif
		
		RestArea(aAreaG84)
		RestArea(aAreaG85)
		
		//Fechamos e abrimos a �rea porque o seek no TURR013() n�o est� funcionando
		G84->(DbCloseArea())
		DbSelectArea("G84")
		G84->(dbSetOrder(1)) //G84_FILIAL + G84_PREFIX + G84_NUMFAT + G84_CLIENT + G84_LOJ

		//Avalia Impress�o da Fatura
		TURR012(oModelG84:GetValue("G84_CLIENT"),oModelG84:GetValue("G84_LOJA"),oModelG84:GetValue("G84_PREFIX"),oModelG84:GetValue("G84_NUMFAT"),'2',.T.,.T.,@cDirPDF,.F.)
		
		//PE para Impress�o do Boleto
		If lTurBolFt .And. oModelG84:GetValue("G84_TOTAL") <> 0 //Se o total for zero n�o imprime
			ExecBlock("TURBOLFT",.F.,.F.,{oModelG84:GetValue("G84_CLIENT"),oModelG84:GetValue("G84_LOJA"),oModelG84:GetValue("G84_PREFIX"),oModelG84:GetValue("G84_NUMFAT"),cDirPDF})
		EndIf
	EndIf

EndIf

Return .T.

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA45ItEnt
Fun��o que retorna a descri��o do Item da entidade adicional do item da fatura

@type 		Function
@author 	Jos� Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA45ItEnt()

Local cRet		:= ""
Local aArea	:= GetArea()

cRet := Posicione("G3G",1,xFilial("G3G")+G84->(G84_CLIENT+G84_LOJA)+G85->(G85_TPENT+G85_ITENT),"G3G_DESCR")

RestArea(aArea)

Return cRet


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA45ItEnt
Fun��o que retorna o status atual do item

@type 		Function
@author 	Jos� Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA45StaAtu()

Local cRet := ""
Local aArea	:= GetArea()

If G84->G84_TPFAT $ "1|3"
	cRet := Posicione("G4C",2,xFilial("G4C",G85->G85_FILREF)+G85->G85_IDIF,"G4C_STATUS")
	If cRet == "3"
		cRet := "1" //Liberado
	ElseIf cRet == "4"
		cRet := "2" //Faturado
	Else
		cRet := "3" //Indisponivel
	EndIf		
Else
	cRet := Posicione("G81",1,xFilial("G81",G85->G85_FILREF)+G85->G85_IDIFA,"G81_STATUS")
	If !(cRet $ "12")
		cRet := "3" //Indisponivel
	EndIf	
EndIf

RestArea(aArea)

Return cRet


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA45FatAtu
Fun��o que retorna a fatura do item

@type 		Function
@author 	Jos� Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA45FatAtu()

Local cRet := ""
Local aArea	:= GetArea()

If G84->G84_TPFAT $ "1|3"
	cRet := Posicione("G4C",2,xFilial("G4C",G85->G85_FILREF)+G85->G85_IDIF,"G4C_NUMFAT")
Else
	cRet := Posicione("G81",1,xFilial("G81",G85->G85_FILREF)+G85->G85_IDIFA,"G81_NUMFAT")
EndIf

RestArea(aArea)

Return cRet


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA45PrfAtu
Fun��o que retorna o prefixo da fatura atual do item

@type 		Function
@author 	Jos� Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA45PrfAtu()

Local cRet := ""
Local aArea	:= GetArea()

If G84->G84_TPFAT $ "1|3"
	cRet := Posicione("G4C",2,xFilial("G4C",G85->G85_FILREF)+G85->G85_IDIF,"G4C_PREFIX")
Else
	cRet := Posicione("G81",1,xFilial("G81",G85->G85_FILREF)+G85->G85_IDIFA,"G81_PREFIX")
EndIf

RestArea(aArea)

Return cRet


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA45FilFat
Fun��o que retorna a filial da fatura atual do item

@type 		Function
@author 	Jos� Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA45FilFat()

Local cRet := ""
Local aArea	:= GetArea()

If G84->G84_TPFAT $ "1|3"
	cRet := Posicione("G4C",2,xFilial("G4C",G85->G85_FILREF)+G85->G85_IDIF,"G4C_FILFAT")
Else
	cRet := Posicione("G81",1,xFilial("G81",G85->G85_FILREF)+G85->G85_IDIFA,"G81_FILFAT")
EndIf

RestArea(aArea)

Return cRet


//-------------------------------------------------------------------
/*/{Protheus.doc} TA45NoAlt() 
Rotina para inibir a pergunta se deseja salvar ou n�o

@author Jos� Domingos Caldana Jr
@since�21/10/2015
@version 12.1.7
/*/
//-------------------------------------------------------------------
Function TA45NoAlt(oModel)

Local oView 		:= FWViewActive()
Local lRet 		:= .T.
	
oView:SetModified(.F.)
 
Return lRet


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA45Calc
Fun��o para calculo do total selecionado

@type 		Function
@author 	Jos� Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA45Calc(oModel,nTotalAtual,xValor,lSomando,cTipo)

Local nRet 		:= 0
Local oModelG85	:= oModel:GetModel("G85_DETAIL")
Local lTpPositiv	:= cTipo $ "0|1|3"

If	(lTpPositiv .And. oModelG85:GetValue("G85_PAGREC") == "2" .And. lSomando) .Or.;
	(lTpPositiv .And. oModelG85:GetValue("G85_PAGREC") == "1" .And. !lSomando) .Or.;
	(!lTpPositiv .And. oModelG85:GetValue("G85_PAGREC") == "2" .And. !lSomando) .Or.;
	(!lTpPositiv .And. oModelG85:GetValue("G85_PAGREC") == "1" .And. lSomando) 
	nRet := nTotalAtual + xValor 
Else
	nRet := nTotalAtual - xValor 
EndIf

Return nRet


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA45RunCan
Fun��o para cancelamento da fatura

@type 		Function
@author 	Jos� Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA45RunCan(oModel,oView,lOnline,lMostrLanc,lAglutLanc)

Local oModelG84	:= oModel:GetModel('G84_MASTER')
Local oModelG85	:= oModel:GetModel('G85_DETAIL')
Local nX			:= 0
Local nRecG4C 	:= 0
Local nRecG81 	:= 0
Local nRecG84 	:= 0
Local nRecG85 	:= 0
Local cConinu   	:= Space(TamSx3("G4C_CONINU")[1])
Local nPos			:= 0
Local aRv			:= {}

Local aAreaG84	:= G84->(GetArea())
Local aAreaG85	:= G85->(GetArea())
Local aRecItens	:= {}
Local aRecCTB		:= {}
Local lRet			:= .T.
Local cIdIf		:= ''
Default lOnline 	:= .t. 
Default lMostrLanc	:= .f. 
Default lAglutLanc	:= .f.


BEGIN TRANSACTION

If TA45Consol(oModel,.T.)

	oModelG84:LoadValue("G84_STATUS"	,'2'	)

	If FWFormCommit( oModel,,,,, ) 
 	
		//+---------------------------------------------------------------------------
		// Atualiza G4C e G81 limpando Numero da Fatura, Prefixo e estornando Status
		//+---------------------------------------------------------------------------
		G4C->(DbsetOrder(4))
		
		For nX := 1 To oModelG85:Length()
			oModelG85:GoLine(nX)
			If TA44RTpFat() == TP_VENDA .Or. TA44RTpFat() == TP_BREAK .Or. TA44RTpFat() == TP_CREDITO
				If !Empty(oModelG85:GetValue("G85_IDIF"))
					//G4C_FILIAL+G4C_NUMID+G4C_IDITEM+G4C_NUMSEQ+G4C_IDIF+G4C_CONINU
					If G4C->(DbSeek(xFilial("G4C",oModelG85:GetValue("G85_FILREF"))+oModelG85:GetValue("G85_REGVEN") + oModelG85:GetValue("G85_ITVEND") + oModelG85:GetValue("G85_SEQIV") + oModelG85:GetValue("G85_IDIF")+cConinu))
						If	G4C->G4C_FILFAT == oModelG85:GetValue("G85_FILIAL") .And. G4C->G4C_PREFIX == oModelG85:GetValue("G85_PREFIX") .And. G4C->G4C_NUMFAT == oModelG85:GetValue("G85_NUMFAT") //Prote��o contra cancelamento de Fatura duplicada
							If SoftLock("G4C")
								G4C->G4C_FILFAT := ""
								G4C->G4C_PREFIX := ""
								G4C->G4C_NUMFAT := ""
								G4C->G4C_STATUS := "3" //Liberado
								G4C->(MsUnLock())
							Else
								lRet := .F.
								DisarmTransaction()
								cIdIf := oModelG85:GetValue("G85_FILREF")+'-'+oModelG85:GetValue("G85_REGVEN") +'-'+ oModelG85:GetValue("G85_ITVEND") +'-'+ oModelG85:GetValue("G85_SEQIV") +'-'+ oModelG85:GetValue("G85_IDIF")
								Help(" ",1,"TA45RunCan",,STR0035+cIdIf,1,0)		//"Item financeiro bloqueado: "
								T45Log( '1', '', "TA45RunCan "+XFilial("G84")+"-"+oModelG84:GetValue("G84_PREFIX")+"-"+oModelG84:GetValue("G84_NUMFAT"))
								T45Log( '2', '', STR0035+cIdIf)
								T45Log( '3', '', '')
								Break
							EndIf
							
							nRecG4C := G4C->(Recno())
							
							nPos := Ascan(aRv,{|x| x[1]+x[2]+x[3]+x[4] == G4C->G4C_FILIAL + G4C->G4C_NUMID + G4C->G4C_IDITEM + G4C->G4C_NUMSEQ})
								
							If nPos <= 0 
								Aadd(aRv,{G4C->G4C_FILIAL,G4C->G4C_NUMID,G4C->G4C_IDITEM,G4C->G4C_NUMSEQ})
							EndIf
							
							//Atualiza status dos Acordos
							If !Empty(G4C->G4C_CLASS)
								G48->(DbsetOrder(4))
								G48->(DbSeek(xFilial("G48",oModelG85:GetValue("G85_FILREF"))+G4C->(G4C_NUMID+G4C_IDITEM+G4C_NUMSEQ+G4C_CLIFOR+G4C_APLICA+cConinu)))
								RecLock("G48",.F.)
									G48->G48_STATUS := '1' //Em Aberto
								G48->(MsUnLock())
								
								
							EndIf
					
							//Se o item faturado n�o foi liberado pela concilia��o, atualiza o status do registro inutilizado
							If Empty( G4C->G4C_TPCONC )
								T45AtuInu( 'G4C', { 'G4C_FILFAT', 'G4C_PREFIX', 'G4C_NUMFAT', 'G4C_STATUS' } )
								T45AtuInu( 'G48', { 'G48_STATUS' } )
							EndIf	
						EndIf
					Else
						lRet := .F.
						DisarmTransaction()
							
						If ValType( oView ) == "O"
							oView:showUpdateMsg( .F. )
							Help( , , "TURA045_NO_CANC", , STR0028 + oModelG84:GetValue("G84_NUMFAT")+", " + STR0027, 1, 0) //"Ocorreu um erro ao cancelar a fatura: " //"o processo n�o ser� efetivado."
						EndIf
						
						Break
					EndIf
				EndIf
			Else
				
				G81->(DbSeek(xFilial("G81",oModelG85:GetValue("G85_FILREF"))+oModelG85:GetValue("G85_IDIFA")))
				
				RecLock("G81",.F.)
				
					G81->G81_FILFAT := ""
					G81->G81_PREFIX := ""
					G81->G81_NUMFAT := ""
					G81->G81_STATUS := "1" //Liberado
				
				G81->(MsUnLock())

				nRecG81 := G81->(Recno())
				
			EndIf
			
			//Captura os Registros das tabelas associadas
			aAreaG85 := G85->(GetArea())
			
			G85->(DbSetOrder(1)) //G85_FILIAL+G85_PREFIX+G85_NUMFAT+G85_ITEM
			
			If ( G85->( DbSeek(XFilial("G85") + oModelG84:GetValue("G84_PREFIX")+oModelG84:GetValue("G84_NUMFAT")+oModelG85:GetValue("G85_ITEM",nX) ) ) )
				nRecG85 := G85->(Recno())
			Endif
				
			aAdd(aRecItens, {nRecG85,nRecG4C,nRecG81})

		Next
		
		If lRet
			aAreaG84 := G84->(GetArea())
			
			G84->(DbSetOrder(1)) //G84_FILIAL+G84_PREFIX+G84_NUMFAT
			
			If ( G84->(DbSeek(XFilial("G84") + oModelG84:GetValue("G84_PREFIX") + oModelG84:GetValue("G84_NUMFAT")) ) )
				nRecG84 := G84->(Recno())
			Endif			
			
			AAdd(aRecCTB,{nRecG84,AClone(aRecItens)})
			aRecItens := {}
			
			//Atualiza Status do IV e RV 
			For nX := 1 To Len(aRv)
				TurAtuStRV(aRv[nX,1], aRv[nX,2], aRv[nX,3], aRv[nX,4], cConinu, "1", .F. )	
			Next
			
			If ( lOnline ) //Por Padr�o, o Estorno sempre ser� contabilizado (na implementa��o padr�om lOnline ser� sempre .t.)
				TurCtOnFat(lMostrLanc,lAglutLanc,.T.,"TURA045",aRecCTB)
			Endif	
			
			RestArea(aAreaG84)
			RestArea(aAreaG85)
		Endif	
				
	Else
	
		DisarmTransaction()
			
		If ValType( oView ) == "O"
			oView:showUpdateMsg( .F. )
			Help( , , "TURA045_NO_CANC", , STR0028 + oModelG84:GetValue("G84_NUMFAT")+", " + STR0027, 1, 0) //"Ocorreu um erro ao cancelar a fatura: " //"o processo n�o ser� efetivado."
		EndIf
		
		lRet := .F.
		
	EndIf

Else 
	DisarmTransaction()
	lRet := .F.
EndIf

END TRANSACTION

If InTransact()
	TurHelp(STR0036,STR0037,STR0038) //"O processo de cancelamento n�o foi concluido com sucesso, pois h� Transa��o presa.","Por favor, acione o administrador do sistema.","Transa��o presa"
	lRet := .F.
End

Return lRet
 

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} T45AtuInu
Fun��o para atualizar informa��es de registros com _CONINU preenchido

@type 		Function
@author 	Anderson Soares Toledo
@since 		18/11/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function T45AtuInu( cAlias, aCpyFlds )
	Local aIndex	:= StrTokArr( (cAlias)->(IndexKey(1)), '+' )
	Local cQry		:= ""
	Local cWhere	:= ""
	Local cType	:= ""
	Local nX		:= 0
	
	For nX := 1 to len( aCpyFlds )
		If nX > 1
			cQry += ', '
		EndIf
		
		cType	:= ValType( (cAlias)->( &( aCpyFlds[nX] ) ) )
		
		If cType == 'D'
			cQry += aCpyFlds[nX] + " = '" + dtos( (cAlias)->( &( aCpyFlds[nX] ) ) )+ "'"
		ElseIf cType == 'C'
			cQry += aCpyFlds[nX] + " = '" + cValToChar( (cAlias)->( &( aCpyFlds[nX] ) ) ) + "'"
		ElseIf cType == 'L'
			If (cAlias)->( &( aCpyFlds[nX] ) )
				cQry += aCpyFlds[nX] + " = 'T'"
			Else
				cQry += aCpyFlds[nX] + " = 'F'"
			EndIf	
		Else
			cQry += aCpyFlds[nX] + ' = ' + cValToChar( (cAlias)->( &( aCpyFlds[nX] ) ) )
		EndIf
	Next

	For nX := 1 to len( aIndex )
		If nX > 1
			cWhere += ' AND '
		EndIf	 

		cType	:= ValType( (cAlias)->( &( aIndex[nX]  ) ) )

		If cType == 'D'
			cWhere += aIndex[nX] + " = '" + dtos( (cAlias)->( &( aIndex[nX] ) ) )+ "'"
		ElseIf cType == 'C'
			cWhere += aIndex[nX] + " = '" + cValToChar( (cAlias)->( &( aIndex[nX] ) ) ) + "'"
		ElseIf cType == 'L'
			If (cAlias)->( &( aIndex[nX] ) )
				cWhere += aIndex[nX] + " = 'T'"
			Else
				cWhere += aIndex[nX] + " = 'F'"
			EndIf	
		Else
			cWhere += aIndex[nX] + ' = ' + cValToChar( ( cAlias )->( &( aIndex[nX] ) ) )
		EndIf
		 
		 
	Next
	
	If len( cQry ) > 0 .And. len( cWhere ) > 0
		TCSQLExec( 'UPDATE ' + RetSqlName( cAlias ) + ' SET ' + cQry + ' WHERE ' + cAlias + "_CONINU <> '' AND " + cWhere )
	EndIf
		 	
Return

Function T45SETOPER(nOper)
_nOper      	:= nOper

Return

/*/{Protheus.doc} T45Log
Fun��o para grava��o de Log
@type function
@author Anderson Toledo
@since 06/07/2017
@version 1.0
/*/
Function T45Log( cAction, cMsgId, cMessage )	
	Local cDirLog := '\LogFaturamento\'
	
	If SuperGetMV( 'MV_TURLGFA',,.F.)	
		If cAction == '1'
			FwMakeDir( cDirLog )
			nHandle := FCreate( cDirLog +;
			 										dtos( dDataBase ) + '_' +;
													StrTran( Time(),":","" ) + '_' +;
													cMessage + '.log' )
			nSeconds := Seconds()										
		ElseIf cAction == '2'
			If !Empty( cMsgId )
				FWrite(nHandle,	'Step: ' + cMsgId + '-' + EncodeUTF8( cMessage ) + ' Tempo decorrido: ' + cValToChar( ( Seconds() - nSeconds ) ) + 's' + CRLF )
			Else
				FWrite(nHandle,'Mensagem: ' + EncodeUTF8( cMessage ) + CRLF )
			EndIf
			
			
			
		ElseIf cAction == '3'
			FWrite(nHandle,	'Finalizando Log. Tempo Total: ' + cValToChar( ( Seconds() - nSeconds ) ) + ' s' )
			FClose( nHandle )
			nHandle := 0
			nSeconds := 0
		EndIf
		
	EndIf

Return