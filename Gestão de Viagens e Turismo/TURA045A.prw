#include "TURA045A.CH"
#Include 'Totvs.ch'

#DEFINE TP_VENDA			1
#DEFINE TP_APURA			2
#DEFINE TP_BREAK			3

/*/{Protheus.doc} TA45Consol
Fun��o para gera��o da consolida��o financeira da fatura ou o cancelamento da mesma
@type function
@author Anderson Toledo
@since 27/05/2016
@version 1.0
@param oModel, objeto, Modelo do fonte TURA045 com os dados para gera��o da consolida��o
@param lCancel, l�gico, Indica se a chamada � para cancelamento da consolida��o
/*/
Function TA45Consol(oModel,lCancel)
	Local lRet := .T.

	If lCancel
		lRet := TA45EstFt( oModel )
	Else
		lRet := TA45ProcFt( oModel )
	EndIf

Return lRet

/*/{Protheus.doc} TA45ProcFt
Fun��o para processamento da gera��o da consolida��o financeira
@type function
@author Anderson Toledo
@since 27/05/2016
@version 1.0
@param oModel, objeto, Modelo do fonte TURA045 com os dados para gera��o da consolida��o
/*/
Function TA45ProcFt( oModel )
	Local aSaveLines 	:= FwSaveRows( oModel )
	Local aLogG8E		:= {}
	Local aItensNF	:= {}
	Local aAbat		:= {}
	Local aTitAvulso	:= {}
	Local aTitNCC		:= {}
	Local aTitREC		:= {}
	Local aSaldoRec	:= {}
	Local aSaldoVen	:= {}
	Local aSaldoTot	:= {}
	Local cSerieNFS	:= ''
	Local cNumNFS 	:= ''
	Local cNatureza	:= ''
	Local nX			:= 0
	Local nTotFat		:= 0
	Local nTotReemb	:= 0
	Local nTotReceita	:= 0
	Local nTotAbat	:= 0
	Local nSldReceita	:= 0
	Local nSldVenda	:= 0
	Local nSldTotal	:= 0
	Local nFilRef		:= 0
	Local nPosSerie	:= 0
	Local nPosNatur	:= 0
	Local lContinua	:= .T.
	Local lGeraFatura	:= .F.
	Local oModelG84	:= oModel:GetModel( 'G84_MASTER' )
	Local oModelG85	:= oModel:GetModel( 'G85_DETAIL' )
	Local aItemRv		:= {}
	Local aRv			:= {}
	Local aFatAdt		:= {}
	Local cFilAntBkp	:= cFilAnt

	//Calcula os totais
	T45Log( '2', 'TA045Totais', 'In�cio' )
	TA045Totais( oModel, @nTotFat, @nTotReemb, @nTotReceita, @nTotAbat, @nSldReceita, @nSldVenda, @nSldTotal, @aItemRv, @aRv, @aSaldoRec, @aSaldoVen, @aSaldoTot )
	T45Log( '2', 'TA045Totais', 'Fim' )


	SED->( dbSetOrder(1) )

	//Se o saldo da Receita � possitivo, avalia emiss�o de NF
	If nSldReceita > 0

		//Avalia se emite NF s� da receita ou total
		If nSldVenda <= 0 .Or. SuperGetMV('MV_TURNFTO') == '2'

			T45Log( '2', 'TA45ItNFS', 'Obtendo itens NFS - In�cio' )		
			//Obtem os itens para gera��o da NF conforme cadastro parametros financeiros
			lContinua := TA45ItNFS( {'3','4'}, aItensNF, aAbat, aTitAvulso )
			T45Log( '2', 'TA45ItNFS', 'Obtendo itens NFS - Fim' )

			If lContinua
				For nFilRef := 1 To Len( aItensNF ) //Loop por filiais de refer�ncia
					If !lContinua
						Exit
					EndIf

					If !Empty( aItensNF[nFilRef][1] )
						cFilAnt := aItensNF[nFilRef][1]
					EndIf

					For nPosSerie := 1 To Len( aItensNF[nFilRef][2] ) //Loop por s�rie das notas fiscais

						For nPosNatur := 1 To Len( aItensNF[nFilRef][2][nPosSerie][2] ) //Loop por natureza

							If Empty( aItensNF[nFilRef][2][nPosSerie][2] )
								Help( , , "TA045EFNAT", , STR0019, 1, 0,,,,,,{STR0020}) //"Natureza em branco."###"Verifique o cadastro de 'Parametros Fiscais e Financeiros' se os c�digos de Naturezas est�o corretos."
								lContinua := .F.
								Exit
							ElseIf !SED->( dbSeek( xFilial('SED') + aItensNF[nFilRef][2][nPosSerie][2][nPosNatur][1] ) )
								Help( , , "TA045EFNAT2", , I18N(STR0021,{aItensNF[nFilRef][2][nPosSerie][2][nPosNatur][1],cFilAnt}), 1, 0,,,,,,{STR0022}) //"Natureza #1[codigo natureza]# n�o cadastrada na filial #2[C�digo filial]#."###"Verifique o cadastro de Naturezas na filial informada."
								lContinua := .F.
								Exit
							EndIf

							cSerieNFS	:= aItensNF[nFilRef][2][nPosSerie][1]
							cNumNFS 	:= ''
							TurSetNatur(  aItensNF[nFilRef][2][nPosSerie][2][nPosNatur][1] )

							//Valida se ja existe a numera��o de nota no sistema, caso exista n�o permiti continuidade no processo.
							DbSelectArea("SF2")
							DbSetOrder(1)	//F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENTE+F2_LOJA+F2_FORMUL+F2_TIPO

							If SF2->(DbSeek(xFilial("SF2")+cNumNFS+cSerieNFS+oModelG84:GetValue('G84_CLIENT')+oModelG84:GetValue('G84_LOJA')))
								Help(,,"TA045AGERNF",,i18N(STR0024,{cNumNFS,cSerieNFS,oModelG84:GetValue( 'G84_CLIENT' )}),1,0)	//"J� existe uma NFS com o n�mero #1[n�mero]# e s�rie #2[s�rie]# para o cliente #3[cliente]#"
								Return
							EndIf

							T45Log( '2', 'TA45GerNFS', 'Gerando NFS - In�cio' )	
							//Gera��o da nota fiscal de sa�da
							If TA45GerNFS( oModelG84:GetValue( 'G84_CLIENT' ), oModelG84:GetValue( 'G84_LOJA' ), @cSerieNFS, @cNumNFS, aItensNF[nFilRef][2][nPosSerie][2][nPosNatur][2], aLogG8E )
								If TA45SetVl(oModel,'G8E_DETAIL',aLogG8E,!oModel:GetModel('G8E_DETAIL'):IsEmpty(),"G8E_SEQUEN")
									lContinua := .T.
								Else
									Help(,,'TA45G8ENFS',,STR0001,1,0) //"Erro ao gravar o log."
									lContinua := .F.
								EndIf
							Else
								lContinua := .F.
							EndIf
							T45Log( '2', 'TA45GerNFS', 'Gerando NFS - Fim' )

						Next
					Next
				Next

				If Len( aTitAvulso ) > 0
					T45Log( '2', 'TA45TitRec', 'Gerando t�tulos a receber (avulsos) - In�cio' )

					For nFilRef := 1 To Len( aTitAvulso )
						If !lContinua
							Exit
						EndIf

						If !Empty( aTitAvulso[nFilRef][1] )
							cFilAnt := aTitAvulso[nFilRef][1]
						EndIf

						If Empty( aTitAvulso[nFilRef][3] )
							Loop
						EndIf
						
						//Gera��o do t�tulo de receitas que n�o possui Parametros Fiscais
						If TA45TitRec( aTitAvulso[nFilRef][3], 'REC', aTitAvulso[nFilRef][2], oModelG84:GetValue( 'G84_EMISS' ), oModelG84:GetValue( 'G84_EMISS' ), aLogG8E, oModel )
							If TA45SetVl(oModel,'G8E_DETAIL',aLogG8E,!oModel:GetModel('G8E_DETAIL'):IsEmpty(),"G8E_SEQUEN")
								lContinua := .T.
							Else
								Help(,,'TA45G8ESLDVEN',,STR0002,1,0) //"Erro ao gravar o log."
								lContinua := .F.
							EndIf

						Else
							lContinua := .F.
						EndIf
					Next

					T45Log( '2', 'TA45TitRec', 'Gerando t�tulos a receber - Fim' )
				EndIf

				cFilAnt := cFilAntBkp
			EndIf

			If lContinua
				For nX := 1 to len( aSaldoVen )
					If aSaldoVen[nX][2] > 0
						
						T45Log( '2', 'TA45TitRec', 'Gerando t�tulos a receber (Repasse) - In�cio' )
						
						//Gera��o do t�tulo de repasse
						If TA45TitRec( aSaldoVen[nX][2], 'INT', aSaldoVen[nX][1], oModelG84:GetValue( 'G84_EMISS' ), oModelG84:GetValue( 'G84_EMISS' ), aLogG8E, oModel )
							If TA45SetVl(oModel,'G8E_DETAIL',aLogG8E,!oModel:GetModel('G8E_DETAIL'):IsEmpty(),"G8E_SEQUEN")
								lContinua := .T.
							Else
								Help(,,'TA45G8ESLDVEN',,STR0002,1,0) //"Erro ao gravar o log."
								lContinua := .F.
								Exit
							EndIf

						Else
							lContinua := .F.
							Exit
						EndIf

						T45Log( '2', 'TA45TitRec', 'Gerando t�tulos a receber (Repasse) - Fim' )
						
					Else
						
						T45Log( '2', 'TA45TitRec', 'Gerando t�tulos a receber (NCC) - In�cio' )
						
						//Gera��o do t�tulo de cr�dito do cliente
						If TA45TitRec( aSaldoVen[nX][2] * -1, 'NCC', aSaldoVen[nX][1],dDataBase,dDataBase,aLogG8E, oModel )
							If TA45SetVl(oModel,'G8E_DETAIL',aLogG8E,!oModel:GetModel('G8E_DETAIL'):IsEmpty(),"G8E_SEQUEN")
								lContinua := .T.
							Else
								Help(,,'TA45G8ESLDVEN2',,STR0003,1,0) //"Erro ao gravar o log."
								lContinua := .F.
							EndIf
						Else
							lContinua := .F.
							Exit
						EndIf

						T45Log( '2', 'TA45TitRec', 'Gerando t�tulos a receber (Fim) - In�cio' )	
					EndIf
				Next

				If lContinua
					T45Log( '2', 'TA045GetTit', 'Consultando t�tulos gerados - In�cio' )
					
					//Consulta titulos de NCC
					If !TA045GetTit( oModel:GetModel('G8E_DETAIL'), aTitNCC, '1', { 'NCC' },.T. )
						lContinua := .F.
					EndIf

					//Consulta Titulos das Nfs
					If !TA045GetTit( oModel:GetModel('G8E_DETAIL'), aTitREC , '2',,.T. )
						lContinua := .F.
					EndIf

					//Consulta Titulos de receitas
					If !TA045GetTit( oModel:GetModel('G8E_DETAIL'), aTitREC , '1',{ 'REC' },.T. )
						lContinua := .F.
					EndIf

					T45Log( '2', 'TA045GetTit', 'Consultando t�tulos gerados - Fim' )
					
					//Se houver t�tulos a compensar, realiza a compensa��o
					If Len(aTitNCC) > 0 .And. Len(aTitREC) > 0
					
						T45Log( '2', 'TA45CompTit', 'Realizando compensacao entre carteiras - In�cio' )
						If !TA45CompTit( oModel, aTitREC, aTitNCC, '4' )
							lContinua := .F.
						EndIf
						T45Log( '2', 'TA45CompTit', 'Realizando compensacao entre carteiras - Fim' )
					EndIf

				EndIf

			EndIf
		Else

			T45Log( '2', 'TA45ItNFS', 'Obtem os itens para gera��o da NF conforme cadastro parametros financeiros - Inicio')
			//Se o valor da venda for positivo e emite a NF total
			//Obtem os itens para gera��o da NF conforme cadastro parametros financeiros
			TA45ItNFS( {'1','2','3','4'}, aItensNF, aAbat, aTitAvulso )

			T45Log( '2', 'TA45ItNFS', 'Obtem os itens para gera��o da NF conforme cadastro parametros financeiros - Fim')
			
			For nFilRef := 1 To Len( aItensNF ) //Loop por filiais de refer�ncia
				If !lContinua
					Exit
				EndIf

				If !Empty( aItensNF[nFilRef][1] )
					cFilAnt := aItensNF[nFilRef][1]
				EndIf

				For nPosSerie := 1 To Len( aItensNF[nFilRef][2] ) //Loop por s�rie das notas fiscais
					If !lContinua
						Exit
					EndIf

					For nPosNatur := 1 To Len( aItensNF[nFilRef][2][nPosSerie][2]) //Loop por natureza

						If Empty( aItensNF[nFilRef][2][nPosSerie][2] )
							Help( , , "TA045EFNAT", , STR0019, 1, 0,,,,,,{STR0020}) //"Natureza em branco."###"Verifique o cadastro de 'Parametros Fiscais e Financeiros' se os c�digos de Naturezas est�o corretos."
							lContinua := .F.
							Exit
						ElseIf !SED->( dbSeek( xFilial('SED') + aItensNF[nFilRef][2][nPosSerie][2][nPosNatur][1] ) )
							Help( , , "TA045EFNAT2", , I18N(STR0021,{aItensNF[nFilRef][2][nPosSerie][2][nPosNatur][1],cFilAnt}), 1, 0,,,,,,{STR0022}) //"Natureza #1[codigo natureza]# n�o cadastrada na filial #2[C�digo filial]#."###"Verifique o cadastro de Naturezas na filial informada."
							lContinua := .F.
							Exit
						EndIf


						cSerieNFS	:= aItensNF[nFilRef][2][nPosSerie][1]
						cNumNFS 	:= '' 
						TurSetNatur(  aItensNF[nFilRef][2][nPosSerie][2][nPosNatur][1] )

						T45Log( '2', 'TA45GerNFS', 'Geracao da nota fiscal de saida - Inicio' )
						
						//Gera��o da nota fiscal de sa�da
						If TA45GerNFS( oModelG84:GetValue( 'G84_CLIENT' ), oModelG84:GetValue( 'G84_LOJA' ), @cSerieNFS, @cNumNFS, aItensNF[nFilRef][2][nPosSerie][2][nPosNatur][2], aLogG8E )
							If TA45SetVl(oModel,'G8E_DETAIL',aLogG8E,!oModel:GetModel('G8E_DETAIL'):IsEmpty(),"G8E_SEQUEN")
								lContinua := .T.
							Else
								Help(,,'TA45G8ENFS',,STR0004,1,0) //"Erro ao gravar o log."
								lContinua := .F.
								Exit
							EndIf
						Else
							lContinua := .F.
							Exit
						EndIf
						
						T45Log( '2', 'TA45GerNFS', 'Geracao da nota fiscal de saida - Fim' )
					Next
				Next
			Next

			If Len( aTitAvulso ) > 0
				T45Log( '2', 'TA45TitRec', 'Geracao do titulo de receitas que nao possui Parametros Fiscais - In�cio' )

				For nFilRef := 1 To Len( aTitAvulso )
					If !lContinua
						Exit
					EndIf

					If !Empty( aTitAvulso[nFilRef][1] )
						cFilAnt := aTitAvulso[nFilRef][1]
					EndIf

					//Gera��o do t�tulo de receitas que n�o possui Parametros Fiscais
					If TA45TitRec( aTitAvulso[nFilRef][3], 'REC', aTitAvulso[nFilRef][2], oModelG84:GetValue( 'G84_EMISS' ), oModelG84:GetValue( 'G84_EMISS' ), aLogG8E, oModel )
						If TA45SetVl(oModel,'G8E_DETAIL',aLogG8E,!oModel:GetModel('G8E_DETAIL'):IsEmpty(),"G8E_SEQUEN")
							lContinua := .T.
						Else
							Help(,,'TA45G8ESLDVEN',,STR0002,1,0) //"Erro ao gravar o log."
							lContinua := .F.
						EndIf

					Else
						lContinua := .F.
					EndIf
				Next

				T45Log( '2', 'TA45TitRec', 'Geracao do titulo de receitas que nao possui Parametros Fiscais - Fim' )
			EndIf
			cFilAnt := cFilAntBkp

		EndIf

	Else

		For nX := 1 to len( aSaldoTot )
			If aSaldoTot[nX][2] > 0
				T45Log( '2', 'TA45TitRec', 'Gerando t�tulos a receber (FTC) - In�cio' )
				
				//Gera T�tulo (FTC)
				If TA45TitRec( aSaldoTot[nX][2], 'INT', aSaldoTot[nX][1],dDataBase,dDataBase,aLogG8E, oModel )
					If TA45SetVl(oModel,'G8E_DETAIL',aLogG8E,!oModel:GetModel('G8E_DETAIL'):IsEmpty(),"G8E_SEQUEN")
						lContinua := .T.
					Else
						Help(,,'TA45G8ESLDTOT1',,STR0006,1,0) //"Erro ao gravar o log."
						lContinua := .F.
					EndIf

				Else
					lContinua := .F.
				EndIf

				T45Log( '2', 'TA45TitRec', 'Gerando t�tulos a receber (FTC) - Fim' )
			ElseIf aSaldoTot[nX][2] < 0
				T45Log( '2', 'TA45TitRec', 'Gerando t�tulos a receber (NCC) - In�cio' )
				
				//Gera T�tulo (NCC)

				If TA45TitRec( aSaldoTot[nX][2] * -1, 'NCC', aSaldoTot[nX][1],dDataBase,dDataBase,aLogG8E, oModel )
					If TA45SetVl(oModel,'G8E_DETAIL',aLogG8E,!oModel:GetModel('G8E_DETAIL'):IsEmpty(),"G8E_SEQUEN")
						lContinua := .T.
					Else
						Help(,,'TA45G8ESLDTOT3',,STR0007,1,0) //"Erro ao gravar o log."
						lContinua := .F.
					EndIf

				Else
					lContinua := .F.
				EndIf

				T45Log( '2', 'TA45TitRec', 'Gerando t�tulos a receber (NCC) - Fim' )
			EndIf

		Next
		
		If lContinua
			T45Log( '2', 'TA045GetTit', 'Consultando t�tulos gerados - In�cio' )
			
			//Consulta titulos de NCC
			If !TA045GetTit( oModel:GetModel('G8E_DETAIL'), aTitNCC, '1', { 'NCC' },.T. )
				lContinua := .F.
			EndIf

			//Consulta Titulos de receitas
			If !TA045GetTit( oModel:GetModel('G8E_DETAIL'), aTitREC , '1',{ 'INT' },.T. )
				lContinua := .F.
			EndIf

			T45Log( '2', 'TA045GetTit', 'Consultando t�tulos gerados - Fim' )
			
			//Se houver t�tulos a compensar, realiza a compensa��o
			If Len(aTitNCC) > 0 .And. Len(aTitREC) > 0
			
				T45Log( '2', 'TA45CompTit', 'Realizando compensacao entre carteiras - In�cio' )
				If !TA45CompTit( oModel, aTitREC, aTitNCC, '4' )
					lContinua := .F.
				EndIf
				T45Log( '2', 'TA45CompTit', 'Realizando compensacao entre carteiras - Fim' )
			EndIf

		EndIf
	EndIf

	//Gerar Liquida��o, aglutinando todos os itens a receber em apenas 1 t�tulo
	If lContinua .And. nSldTotal > 0
		T45Log( '2', 'TA45GerLiq', 'Gera��o da liquida��o - In�cio' )
	
		If TA45GerLiq( oModel, aLogG8E )
			For nX := 1 To Len(aLogG8E)
				If TA45SetVl(oModel,'G8E_DETAIL',aLogG8E[nX],!oModel:GetModel('G8E_DETAIL'):IsEmpty(),"G8E_SEQUEN")
					lContinua := .T.
				Else
					Help(,,'TA45G8ENFS',,STR0005,1,0) //"Erro ao gravar o log."
					lContinua := .F.
				EndIf
			Next
		Else
			lContinua := .F.
		EndIf
		
		T45Log( '2', 'TA45GerLiq', 'Gera��o da liquida��o - Fim' )
	EndIf

	T45Log( '2', 'TA45CmpAdi', 'Compensacao de adiantamentos - In�cio' )
	
	//Realiza a compensa��o dos adiantamentos
	If lContinua .And. !TA45CmpAdi( oModel, aRv )
		Help(,,'TA45CMPRA',,STR0008,1,0) //"Erro ao compensar o adiantamento."
		lContinua := .F.
	EndIf

	T45Log( '2', 'TA45CmpAdi', 'Compensacao de adiantamentos - Fim' )

	cFilAnt := cFilAntBkp

	FwRestRows( aSaveLines )

Return lContinua


/*/{Protheus.doc} TA45EstFt
Fun��o para processamento do estorno da consolida��o financeira
@type function
@author Anderson Toledo
@since 27/05/2016
@version 1.0
@param oModel, objeto, Modelo do fonte TURA045 com os dados para gera��o da consolida��o
/*/
Function TA45EstFt( oModel )
	Local aDocSE5			:= {}
	Local aNumLiq			:= {}
	Local cFilAntBkp		:= cFilAnt
	Local lRet 			:= .T.
	Local lContabiliza  	:= .F.
	Local lAglutina   	:= .F.
	Local lDigita   		:= .F.
	Local aAutCan			:= {}
	Local lAutCan			:= .F.
	Local nX				:= 0
	Local oModelG8E		:= oModel:GetModel( 'G8E_DETAIL' )
	Local cGrpFIS		 	:= GetNewPar("MV_GRPFISC","000001") //Par�metro que indica o grupo referente a Fiscal
	Local cUserLog		:= RetCodUsr()
	Local aGrpUserLog		:= UsrRetGrp(cUserLog)
	Local lUserFis		:= .F.
	Local cTexto			:= space(150)
	Local nOpcA			:= 0
	Local oMemo,OdlgX		
	 
	
	Private lMsErroAuto	:= .F.	

	Pergunte('AFI340',.F.)

	lContabiliza  := MV_PAR11 == 1
	lAglutina   	:= MV_PAR08 == 1
	lDigita   		:= MV_PAR09 == 1

	SE1->( dbSetOrder(1) )

	//Efetua o estorno das compensa��es de fatura de adiantamento
	For nX := 1 to oModelG8E:Length()
		oModelG8E:GoLine( nX )

		If oModelG8E:GetValue( 'G8E_TIPO' ) == '5'
			aDocSE5	:= {}

			If SE1->( dbSeek(	xFilial('SE1',oModelG8E:GetValue('G8E_FILREF')) +;
								oModelG8E:GetValue( 'G8E_PFXTIT' ) +;
								oModelG8E:GetValue( 'G8E_NUMTIT' ) +;
								oModelG8E:GetValue( 'G8E_PARCEL' )	+;
								oModelG8E:GetValue( 'G8E_TIPOTI' ) ) )

				SE5->( dbSetOrder(7) )
				SE5->( dbSeek( xFilial('SE5') + SE1->( E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO+E1_CLIENTE+E1_LOJA ) ) )

				While SE5->( !EOF() ) .And. SE5->(E5_FILIAL+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIFOR+E5_LOJA) == xFilial('SE5') + SE1->( E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO+E1_CLIENTE+E1_LOJA )
					If SE5->E5_MOTBX == 'CMP'
						aAdd(aDocSE5,{SE5->E5_DOCUMEN})
					EndIf

					SE5->( dbSkip() )
				EndDo

				//realiza o estorno das compensa��es
			 	If Len(aDocSE5) > 0
			 		lRet := 	MaIntBxCR(3,{SE1->(Recno())},,,,{lContabiliza,lAglutina,lDigita,.F.,.F.,.F.},,aDocSE5,,, )
				EndIf
  			Else
  				lRet := .F.
  				Exit
  			EndIf

		EndIf

	Next

	If lRet
		dbSelectArea("G8I")
		G8I->(dbSetOrder(2))
		G8I->(dbSeek(xFilial("G8I")+ oModelG8E:GetValue('G8E_PREFIX')+ oModelG8E:GetValue('G8E_NUMFAT')))
			While G8I->(!Eof()) .And. G8I->(G8I_FILFAT+G8I_PFXFAT+G8I_NUMFAT) == xFilial("G8I")+ oModelG8E:GetValue('G8E_PREFIX')+ oModelG8E:GetValue('G8E_NUMFAT')
				RecLock("G8I",.F.)
					G8I->(DbDelete())
				G8I->(MsUnLock())
				G8I->(DbSkip())
			EndDo
	EndIf


	//Obtem todos os c�digos de liquida��o
	For nX := 1 to oModelG8E:Length()
		oModelG8E:GoLine( nX )

		If oModelG8E:GetValue( 'G8E_TIPO' ) == '3'
			cFilAnt 	:= oModelG8E:GetValue( 'G8E_FILREF' )

			If SE1->( dbSeek(	xFilial('SE1') +;
								oModelG8E:GetValue( 'G8E_PFXTIT' ) +;
								oModelG8E:GetValue( 'G8E_NUMTIT' ) +;
								oModelG8E:GetValue( 'G8E_PARCEL' )	+;
								oModelG8E:GetValue( 'G8E_TIPOTI' ) ) )

				If !Empty( SE1->E1_NUMLIQ ) .And. aScan(aNumLiq, {|x| x == SE1->E1_NUMLIQ } ) == 0
					aAdd( aNumLiq, SE1->E1_NUMLIQ  )
				EndIf

			EndIf

		EndIf

	Next

	//Realiza o estorno da liquida��o
	For nX := 1 to len( aNumLiq )
		FINA460(,,,5,,aNumLiq[nX])

		If lMsErroAuto
			MostraErro()
			Help(,,'TA45ESTLIQ',,i18N(STR0009,{aNumLiq[nX]}) ,1,0) 	//"N�o foi poss�vel estornar a liquida��o #1[Numero Liquida��o]#."
			lRet := .F.
			Exit
		EndIf

	Next
	
	If lRet
		//Efetua o estorno das compensa��es de NCC
		For nX := 1 to oModelG8E:Length()
			oModelG8E:GoLine( nX )
	
			If oModelG8E:GetValue( 'G8E_TIPO' ) == '4'
				aDocSE5	:= {}
	
				If SE1->( dbSeek(	xFilial('SE1',oModelG8E:GetValue('G8E_FILREF')) +;
									oModelG8E:GetValue( 'G8E_PFXTIT' ) +;
									oModelG8E:GetValue( 'G8E_NUMTIT' ) +;
									oModelG8E:GetValue( 'G8E_PARCEL' )	+;
									oModelG8E:GetValue( 'G8E_TIPOTI' ) ) )
	
					SE5->( dbSetOrder(7) )
					SE5->( dbSeek( xFilial('SE5') + SE1->( E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO+E1_CLIENTE+E1_LOJA ) ) )
	
					While SE5->( !EOF() ) .And. SE5->(E5_FILIAL+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIFOR+E5_LOJA) == xFilial('SE5') + SE1->( E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO+E1_CLIENTE+E1_LOJA )
						If SE5->E5_MOTBX == 'CMP'
							aAdd(aDocSE5,{SE5->E5_DOCUMEN})
						EndIf
	
						SE5->( dbSkip() )
					EndDo
	
					//realiza o estorno das compensa��es
				 	If MaIntBxCR(3,{SE1->(Recno())},,,,{lContabiliza,lAglutina,lDigita,.F.,.F.,.F.},,aDocSE5,,,)
						lRet := .T.
					Else
						lRet := .F.
					EndIf
	  			Else
	  				lRet := .F.
	  				Exit
	  			EndIf
	
			EndIf
	
		Next
	EndIf

	//Efetua estorno de t�tulos e notas fiscais
	If lRet
		For nX := 1 to oModelG8E:Length()
			oModelG8E:GoLine( nX )

			If !lRet
				Exit
			EndIf

			cFilAnt := oModelG8E:GetValue( 'G8E_FILREF' )

			If oModelG8E:GetValue( 'G8E_TIPO' ) == '1' //T�tulo a receber
				lRet := TA045DeTiR( oModelG8E:GetValue( 'G8E_PFXTIT' ), oModelG8E:GetValue( 'G8E_NUMTIT' ), oModelG8E:GetValue( 'G8E_TIPOTI' ), oModelG8E:GetValue( 'G8E_PARCEL' ),  oModelG8E:GetValue( 'G8E_CLIENT' ), oModelG8E:GetValue( 'G8E_LOJA' ) )
			ElseIf oModelG8E:GetValue( 'G8E_TIPO' ) == '2' //Nota Fiscal de saida
				lRet := TA045DeNFS( oModelG8E:GetValue( 'G8E_NUMNF' ), oModelG8E:GetValue( 'G8E_SERIE' ), oModelG8E:GetValue( 'G8E_CLIENT' ), oModelG8E:GetValue( 'G8E_LOJA' ))
			EndIf
						
			If !lAutCan .And. !lRet
				//Identifica se o usu�rio logado pertence a grupo de FISCAL.
				Aeval(aGrpUserLog,{|x| IIF(x $ cGrpFIS,lUserFis := .T.,nil) })
				
				If lUSERFIS .AND. oModelG8E:GetValue( 'G8E_TIPO' ) == '2'
					If FwAlertNoYes(STR0031 , STR0032)  // "H� notas fiscais que passaram o prazo para cancelamento. Deseja desvincular?"###"Desvincula?"
					 	DEFINE MSDIALOG oDlgx FROM  94,1 TO 250,240 TITLE "Lider" PIXEL

						@ 05,07 TO 60, 110 PROMPT STR0035 OF oDlgx PIXEL  // " Informe o motivo para o desvinculo"
						@ 011, 011 GET oMemo VAR cTexto OF oDlgx MULTILINE SIZE 095, 045 HSCROLL PIXEL 
						DEFINE SBUTTON FROM 63,83 TYPE 1 ENABLE OF oDlgx ACTION (If(Len(alltrim(cTexto)) >= 10, (nOpcA:=1,oDlgx:End()),FwAlertWarning(STR0036,STR0037)) )  //"Aten��o, este texto n�o � valido como motivo de desvinculo."####"Motivo de Desvinculo"
                                                                             
						ACTIVATE MSDIALOG oDlgx CENTERED

						If nOpcA == 1
							aAdd(aAutCan,{oModelG8E:GetValue( 'G8E_NUMNF' ), oModelG8E:GetValue( 'G8E_SERIE' ), oModelG8E:GetValue( 'G8E_CLIENT' ), oModelG8E:GetValue( 'G8E_LOJA' )})
							lAutCan 	:= .T.
							lRet 		:= .T.
						EndIf
						
					Else
						lRet 		:= .F.
					EndIf
				
				Endif
			Elseif lAutCan .And. !lRet
				If oModelG8E:GetValue( 'G8E_TIPO' ) == '2'
					aAdd(aAutCan,{oModelG8E:GetValue( 'G8E_NUMNF' ), oModelG8E:GetValue( 'G8E_SERIE' ), oModelG8E:GetValue( 'G8E_CLIENT' ), oModelG8E:GetValue( 'G8E_LOJA' )})
				EndIf
				lRet 		:= .T.				
			Endif
		
			
		Next
	Endif
	If lRet .And. lAutCan
		cTexto := alltrim(cTexto)
		lRet := TA45DESV(oModel,oModelG8E, aAutCan,cTexto)		
	EndIf

	cFilAnt := cFilAntBkp
Return lRet



/*/{Protheus.doc} TA045Totais
Fun��o para calculo dos valores da fatura
@type function
@author Anderson Toledo
@since 27/05/2016
@version 1.0
@param oModel, objeto, Modelo do fonte TURA045
@param nValFat, num�rico, variavel por referencia com valor da fatura
@param nValReemb, num�rico, variavel por referencia com valor da reembolso
@param nValReceita, num�rico, variavel por referencia com valor da receita
@param nValAbat, num�rico, variavel por referencia com valor da abatimento
@param nSaldoRec, num�rico, variavel por referencia com valor do saldo da receita
@param nSaldoVen, num�rico, variavel por referencia com valor do saldo de vendas
@param nSaldoTot, num�rico, variavel por referencia com valor total do faturamento
/*/
Static Function TA045Totais( oModel, nTotFat, nTotReemb, nTotReceita, nTotAbat, nSaldoRec, nSaldoVen, nSaldoTot, aItemRv, aRv, aSaldoRec, aSaldoVen, aSaldoTot )
	Local aResReemb	:= {}
	Local aResAbat	:= {}
	Local nLine		:= 0
	Local oModelG85 	:= oModel:GetModel('G85_DETAIL')
	Local nPos			:= 0
	Local nValFat		:= 0
	Local nValReemb	:= 0
	Local nValReceita	:= 0
	Local nValAbat	:= 0

	For nLine := 1 to oModelG85:Length()
		oModelG85:GoLine( nLine )

		nValFat		:= 0
		nValReemb		:= 0
		nValReceita	:= 0
		nValAbat		:= 0

		If oModelG85:GetValue('G85_TIPO') == '1' //Itens de Venda
			If oModelG85:GetValue('G85_PAGREC') == '1'
				nValFat := oModelG85:GetValue( 'G85_VALOR' ) * -1
			Else
				nValFat := oModelG85:GetValue( 'G85_VALOR' )
			EndIf

			T45AddVlr( aSaldoVen, oModelG85:GetValue( 'G85_NATURE' ), nValFat )

		ElseIf oModelG85:GetValue('G85_TIPO') $ '2|5' //Itens de Reembolso
			If oModelG85:GetValue('G85_PAGREC') == '1'
				nValReemb := oModelG85:GetValue( 'G85_VALOR' )
			Else
				nValReemb := oModelG85:GetValue( 'G85_VALOR' ) * -1
			EndIf

			T45AddVlr( aResReemb, oModelG85:GetValue( 'G85_NATURE' ), nValReemb )

		ElseIf oModelG85:GetValue('G85_TIPO') == '3' //Itens de Receita
			If oModelG85:GetValue('G85_PAGREC') == '1'
				nValReceita := oModelG85:GetValue( 'G85_VALOR' ) * -1
			Else
				nValReceita := oModelG85:GetValue( 'G85_VALOR' )
			EndIf

			T45AddVlr( aSaldoRec, oModelG85:GetValue( 'G85_NATURE' ), nValReceita )

		ElseIf oModelG85:GetValue('G85_TIPO') == '4' //Itens de Abatimento de Receita
			If oModelG85:GetValue('G85_PAGREC') == '1'
				nValAbat := oModelG85:GetValue( 'G85_VALOR' )
			Else
				nValAbat := oModelG85:GetValue( 'G85_VALOR' ) * -1
			EndIf

			T45AddVlr( aResAbat, oModelG85:GetValue( 'G85_NATURE' ), nValAbat )
		EndIf

		nPos := Ascan(aItemRv,{|x| x[1]+x[2]+x[3]+x[4] == oModelG85:GetValue('G85_FILREF') + oModelG85:GetValue('G85_REGVEN') + oModelG85:GetValue('G85_ITVEND') + oModelG85:GetValue('G85_SEQIV')})
		If nPos <= 0
			Aadd(aItemRv,{oModelG85:GetValue('G85_FILREF'),oModelG85:GetValue('G85_REGVEN'),oModelG85:GetValue('G85_ITVEND'),oModelG85:GetValue('G85_SEQIV'),nValFat,nValReemb,nValReceita,nValAbat,nValFat-nValReemb+nValReceita-nValAbat})
		Else
			aItemRv[nPos,5] := aItemRv[nPos,5] + nValFat
			aItemRv[nPos,6] := aItemRv[nPos,6] + nValReemb
			aItemRv[nPos,7] := aItemRv[nPos,7] + nValReceita
			aItemRv[nPos,8] := aItemRv[nPos,8] + nValAbat
			aItemRv[nPos,9] := aItemRv[nPos,5] - aItemRv[nPos,6] + aItemRv[nPos,7] - aItemRv[nPos,8]

		EndIf

		nPos := Ascan(aRv,{|x| x[1]+x[2] == oModelG85:GetValue('G85_FILREF') + oModelG85:GetValue('G85_REGVEN')})
		If nPos <= 0
			Aadd(aRv,{oModelG85:GetValue('G85_FILREF'),oModelG85:GetValue('G85_REGVEN'),nValFat,nValReemb,nValReceita,nValAbat,nValFat-nValReemb+nValReceita-nValAbat})
		Else
			aRv[nPos,3] := aRv[nPos,3] + nValFat
			aRv[nPos,4] := aRv[nPos,4] + nValReemb
			aRv[nPos,5] := aRv[nPos,5] + nValReceita
			aRv[nPos,6] := aRv[nPos,6] + nValAbat
			aRv[nPos,7] := aRv[nPos,3] - aRv[nPos,4] + aRv[nPos,5] - aRv[nPos,6]
		EndIf

		nTotFat		+= nValFat
		nTotReemb		+= nValReemb
		nTotReceita	+= nValReceita
		nTotAbat		+= nValAbat

	Next

	//Saldo Itens de venda
	nSaldoVen := nTotFat - nTotReemb

	//Saldo Itens de receita
	nSaldoRec	:= nTotReceita - nTotAbat

	//Total de receitas e vendas
	nSaldoTot := nSaldoRec + nSaldoVen

	IIF( len( aResAbat ) > 0, ASort(aResAbat,,,{|x,y| x[2] < y[2] } ), )
	IIF( len( aSaldoRec ) > 0, ASort(aSaldoRec,,,{|x,y| x[2] < y[2] } ), )
	IIF( len( aResReemb ) > 0, ASort(aResReemb,,,{|x,y| x[2] < y[2] } ), )
	IIF( len( aSaldoVen ) > 0, ASort(aSaldoVen,,,{|x,y| x[2] < y[2] } ), )


	//Realiza o desconto de abatimento de receitas e reembolsos por natureza
	While len( aResAbat ) > 0 .And. len( aSaldoRec ) > 0
		If aSaldoRec[1][2] > aResAbat[1][2]
			aSaldoRec[1][2] -= aResAbat[1][2]

			aDel( aResAbat, 1 )
			aSize( aResAbat, len(aResAbat) - 1 )

		ElseIf aSaldoRec[1][2] < aResAbat[1][2]
			aResAbat[1][2] -= aSaldoRec[1][2]

			aDel( aSaldoRec, 1 )
			aSize( aSaldoRec, len(aSaldoRec) - 1 )


		Else
			aDel( aSaldoRec, 1 )
			aSize( aSaldoRec, len(aSaldoRec) - 1 )

			aDel( aResAbat, 1 )
			aSize( aResAbat, len(aResAbat) - 1 )

		EndIf
	EndDo


	While len( aResReemb ) > 0 .And. len( aSaldoVen ) > 0
		If aSaldoVen[1][2] > aResReemb[1][2]
			aSaldoVen[1][2] -= aResReemb[1][2]

			aDel( aResReemb, 1 )
			aSize( aResReemb, len(aResReemb) - 1 )

		ElseIf aSaldoVen[1][2] < aResReemb[1][2]
			aResReemb[1][2] -= aSaldoRec[1][2]

			aDel( aSaldoVen, 1 )
			aSize( aSaldoVen, len(aSaldoRec) - 1 )


		Else
			aDel( aSaldoVen, 1 )
			aSize( aSaldoVen, len(aSaldoVen) - 1 )

			aDel( aResReemb, 1 )
			aSize( aResReemb, len(aResReemb) - 1 )

		EndIf

	EndDo

	//Alimenta o vetor original com valor invertido, se negativo gera NCC
	For nLine := 1 to len( aResReemb )
		aAdd( aSaldoVen, { aResReemb[nLine][1], aResReemb[nLine][2] * -1 }  )
	Next

	For nLine := 1 to len( aResAbat )
		aAdd( aSaldoRec, { aResAbat[nLine][1], aResAbat[nLine][2] * -1 }  )
	Next

	//Monta o vetor com o saldo total
	aSaldoTot := aClone( aSaldoRec )
	For nLine := 1 to len( aSaldoVen )
		nPos := aScan( aSaldoTot, { |x| x[1] == aSaldoVen[nLine][1] } )

		If nPos == 0
			aAdd( aSaldoTot, { aSaldoVen[nLine][1], 0 } )
			nPos := len( aSaldoTot )
		EndIf

		aSaldoTot[nPos][2] += aSaldoVen[nLine][2]
	Next

Return


/*/{Protheus.doc} TA45TitRec
Fun��o para gera��o de t�tulos a receber
@type function
@author Anderson Toledo
@since 27/05/2016
@version 1.0
@param nValor, num�rico, Valor do t�tulo
@param cTipo, character, Tipo do t�tulo
@param cNatureza, character, Natureza do t�tulo
@param dDtEmiss, data, data de emiss�o
@param dDtVencto, data, data de vencimento do t�tulo
@param aLog, array, Vetor com os valores para log para a tabela G8E
/*/
Static Function TA45TitRec( nValor, cTipo, cNatureza, dDtEmiss, dDtVencto, aLog ,oModel)
	Local aArray 		:= {}
	Local cNumTit		:= ''
	Local lRet			:= .T.
	
	Local oModelG84	:= Nil

	Private lMsErroAuto 	:= .F.

	DEFAULT aLog			:= {}
	DEFAULT oModel			:= FwModelActive(oModel)
	oModelG84				:= oModel:GetModel( 'G84_MASTER' )
	
	cNumTit := oModelG84:GetValue( 'G84_NUMFAT' )

	SE1->(dbSetOrder(1))
	While SE1->( DbSeek(xFilial('SE1') + oModelG84:GetValue( 'G84_PREFIX' ) + cNumtit + Space(TamSX3('E1_PARCELA')[1]) + PADR(cTipo,TamSX3('E1_TIPO')[1]) ) )
	 	cNumtit := Soma1( cNumtit )
	 	//Help(,,'TA045ATITREC',,I18N(STR0023, {oModelG84:GetValue('G84_NUMFAT'),oModelG84:GetValue('G84_PREFIX'),cTipo} ) ,1,0)// "T�tulo: #1[ Numero Titulo ], Prefixo: #2[ Prefixo Titulo ], Tipo: #3[ Tipo Titulo ] j� cadastrado."
		//Return .F.
	EndDo

	aAdd( aArray,	{ 'E1_PREFIXO'	, oModelG84:GetValue( 'G84_PREFIX' ) 	, NIL } )
	aAdd( aArray,	{ 'E1_NUM' 		, cNumTit									, NIL } )
	aAdd( aArray,	{ 'E1_TIPO'		, cTipo 									, NIL } )
	aAdd( aArray,	{ 'E1_NATUREZ'	, cNatureza 								, NIL } )
	aAdd( aArray,	{ 'E1_CLIENTE' 	, oModelG84:GetValue( 'G84_CLIENT' ) 	, NIL } )
	aAdd( aArray,	{ 'E1_LOJA'		, oModelG84:GetValue( 'G84_LOJA'	)	, NIL } )
	aAdd( aArray,	{ 'E1_EMISSAO'	, dDtEmiss									, NIL } )
	aAdd( aArray,	{ 'E1_VENCTO'		, dDtVencto								, NIL } )
	aAdd( aArray,	{ 'E1_VALOR'		, nValor									, NIL } )
	aAdd( aArray,	{ 'E1_ORIGEM'		, 'TURA045' 								, NIL } )

	MsExecAuto( { |x,y| FINA040(x,y)} , aArray, 3) // 3-Inclusao,4-Altera��o,5-Exclus�o

	If lMsErroAuto
		lRet := .F.
		MostraErro()

	Else
		aLog :={}
		aAdd( aLog,{ 'G8E_PREFIX'	, oModelG84:GetValue( 'G84_PREFIX' ) 	} )
		aAdd( aLog,{ 'G8E_NUMFAT'	, oModelG84:GetValue( 'G84_NUMFAT' ) 	} )
		aAdd( aLog,{ 'G8E_CLIENT'	, oModelG84:GetValue( 'G84_CLIENT' ) 	} )
		aAdd( aLog,{ 'G8E_LOJA'		, oModelG84:GetValue( 'G84_LOJA' )   	} )
		aAdd( aLog,{ 'G8E_TIPO'		, '1' 									 	} )
		aAdd( aLog,{ 'G8E_FILREF'	, cFilAnt 									} )
		aAdd( aLog,{ 'G8E_PFXTIT'	, SE1->E1_PREFIXO 						} )
		aAdd( aLog,{ 'G8E_NUMTIT'	, SE1->E1_NUM								} )
		aAdd( aLog,{ 'G8E_TIPOTI'	, SE1->E1_TIPO							} )
		aAdd( aLog,{ 'G8E_PARCEL'	, SE1->E1_PARCELA							} )
		aAdd( aLog,{ 'G8E_VALOR'		, SE1->E1_SALDO 							} )
		aAdd( aLog,{ 'G8E_IDDOC'	, FINGRVFK7('SE1',&(TurKeyTitulo('SE1')))} ) //FINGRVFK7()

		T45Log( '2', '', 'SE1 Indice 1:' + SE1->( &( IndexKey(1) ) ) + ' RecNo: ' + cValToChar( SE1->(RecNo()) ) )
	EndIf

Return lRet



/*/{Protheus.doc} TA45ItNFS
Fun��o para obter os itens para nota fiscal de sa�da
@type function
@author Anderson Toledo e Jos� Domingos
@since 30/05/2016
@version 1.0
@param aTipos, array, tipos de registros da G85 que ser�o considerados
@param aItensNF, array, Vetor com os dados dos itens que ir�o compor as notas
@param aAbat, array, Vetor com informa��es de valores onde n�o foi poss�vel abater dos itens da NF
@param aTitAvulso, array, Vetor com informa��es de valores para gera��o de t�tulos avulsos
/*/
Function TA45ItNFS( aTipos, aItensNF, aAbat, aTitAvulso )
	Local aVetAux		:= {}
	Local cTpCli		:= ''
	Local cTpRV		:= ''
	Local cSerieVen	:= PadR( SuperGetMV( 'MV_TURSERV' ),TamSX3( 'F2_SERIE' )[1])
	Local nX			:= 0
	Local nY			:= 0
	Local nZ			:= 0
	Local nPosFRef	:= 0
	Local nPosSerie	:= 0
	Local nPosNaturez	:= 0
	Local oModel		:= FwModelActive()
	Local oModelG84	:= oModel:GetModel( 'G84_MASTER' )
	Local oModelG85	:= oModel:GetModel( 'G85_DETAIL' )
	Local nTamVl		:= TamSx3("G85_VALOR")[1]
	Local nDecVl		:= TamSx3("G85_VALOR")[2]
	Local aIt			:= {}
	Local nP			:= 0
	Local nFil			:= 0
	Local nSer			:= 0
	Local nNat			:= 0
	Local nIte			:= 0
	Local cNatVSP		:= "" // Natureza de Venda de Servi�o Pr�prio
	Local nItFilial	:= 0
	Local aParFis		:= {}	

	SA1->( dbSetOrder(1) )
	SB1->( dbSetOrder(1) )

	For nX	:= 1 To oModelG85:Length()
		oModelG85:GoLine( nX )

		If aScan( aTipos, {|x| x == oModelG85:GetValue('G85_TIPO') } ) == 0
			Loop
		EndIf

		//+--------------------------------------------------------------------------
		//|Tipo 1 (Venda) e 3 (Receita) s�o valores a receber, por isso s�o somados
		//+--------------------------------------------------------------------------
		If (oModelG85:GetValue('G85_TIPO') $ '1|3' .And. oModelG85:GetValue('G85_PAGREC') == '2')

			//+-------------------------------------------------------------------------------------------
			//|Tipo 3 emite NF, por isso � preciso consultar paramentros Financeiros e Fiscal (G6A e G6B)
			//+-------------------------------------------------------------------------------------------
			If oModelG85:GetValue('G85_TIPO') == '3'

				aParFis := T35PesqPFF(	oModelG85:GetValue('G85_FILREF'),;
											oModelG85:GetValue('G85_CLASS'),;
											oModelG85:GetValue( 'G85_SEGNEG' ),;
											IIf( oModelG84:GetValue('G84_TPFAT') == STR(TP_APURA,1) .And. oModelG85:GetValue('G85_CLASS') <> "V01" , "", oModelG85:GetValue( 'G85_CODPRD' )),;
											'1',;
											oModelG84:GetValue( 'G84_CLIENT' ),;
											oModelG84:GetValue( 'G84_LOJA' ))

				//+-------------------------------------------------------------------------------------------
				//|Se tiver produto nos parametros, emite NF. Sen�o, apenas t�tulo avulso
				//+-------------------------------------------------------------------------------------------
				If (!Empty(aParFis[2]) .And. !Empty(aParFis[3]) .And. !Empty(aParFis[4])) .AND. !(oModelG85:GetValue('G85_CLASS') $ 'C12')  //Sem TES, S�rie e Produto Ou diferente de Markup|Cred.N.T

					 TA45AddItNf( aItensNF, oModelG85, aParFis[3], oModelG85:GetValue( 'G85_NATURE' ), aParFis[4], aParFis[2]  )

				ElseIf (oModelG85:GetValue('G85_CLASS') $ 'C07|C12') .OR. T45TitAv( oModelG85:GetValue('G85_IDIF') ) //C07-Repasse|C12-Cr�dito n�o tribut�vel n�o exige emiss�o de NF OU Estorna no reembolso
					
					//Verifica se j� existe filial de refer�ncia
					nPosFRef := aScan( aTitAvulso, {|x|, x[1] == oModelG85:GetValue('G85_FILREF') .And. x[2] == oModelG85:GetValue('G85_NATURE') } )
					If nPosFRef == 0
						AAdd(aTitAvulso, { oModelG85:GetValue('G85_FILREF'), oModelG85:GetValue('G85_NATURE'), 0 } )
						nPosFRef := Len(aTitAvulso)
					EndIf

					aTitAvulso[nPosFRef][3] += oModelG85:GetValue( 'G85_VALOR' )
				
				Else
					
					Help(,,'TA045A_NO_PARAM',,I18N(STR0030, {oModelG85:GetValue('G85_CLASS'), oModelG85:GetValue('G85_FILREF')}) ,1,0) // "N�o h� par�metro fiscal cadastrado para a receita: #1 na Filial: #2"
					Return .F.

				EndIf

			Else

				//+-------------------------------------------------------------------------------------------
				//|Tipo 1 (Venda) emite NF com base no produto do RV mesmo
				//+-------------------------------------------------------------------------------------------
				SB1->( dbSeek( xFilial('SB1') + oModelG85:GetValue('G85_CODPRD') ) )

				If Empty( cSerieVen )
					Help(,,'TA045ANOSERIE',,STR0010,1,0)//"Solicite ao administrador do sistema cadastrar o param�tro 'MV_TURSERV' a s�rie utilizada na nota fiscal de sa�da para itens de venda."
					Return .F.
				Else
					TA45AddItNf( aItensNF, oModelG85, cSerieVen, oModelG85:GetValue('G85_NATURE'), oModelG85:GetValue('G85_CODPRD'), SB1->B1_TS  )
				EndIf

			EndIf

		ElseIf (oModelG85:GetValue('G85_TIPO') $ '2|4|5' .And. oModelG85:GetValue('G85_PAGREC') == '2')

			//Verifica se j� existe filial de refer�ncia e natureza
			nPosFRef := aScan( aTitAvulso, {|x|, x[1] == oModelG85:GetValue('G85_FILREF') .And. x[2] == oModelG85:GetValue('G85_NATURE') } )
			If nPosFRef == 0
				AAdd(aTitAvulso, { oModelG85:GetValue('G85_FILREF'), oModelG85:GetValue('G85_NATURE'), 0 } )
				nPosFRef := Len(aTitAvulso)
			EndIf

			aTitAvulso[nPosFRef][3] += oModelG85:GetValue( 'G85_VALOR' )

		Else

			//+-------------------------------------------------------------------------------------------
			//|Valores (Negativo s�o abatido dos valores positivo de cobran�a ao cliente
			//+-------------------------------------------------------------------------------------------

			//Verifica se j� existe filial de refer�ncia
			nPosFRef := aScan( aAbat, {|x|, x[1] == oModelG85:GetValue('G85_FILREF') } )
			If nPosFRef == 0
				AAdd(aAbat, { oModelG85:GetValue('G85_FILREF'), 0 } )
				nPosFRef := Len(aAbat)
			EndIf

			aAbat[nPosFRef][2] += oModelG85:GetValue( 'G85_VALOR' )

		EndIf
	Next

	//+-------------------------------------------------------------------------------------------
	//|Abaixo � realizado o tratamento de abatimento dos Itens da NF. Positivo(aItensNF) x Negativo(aAbat)
	//+-------------------------------------------------------------------------------------------
	//TUR45AbNf(aAbat,aItensNF)
	TUR45AbTA(aAbat,aTitAvulso)
	//Sempre depois de TUR45AbNf(aAbat,aItensNF)
	If Len( aItensNF ) == 0
		aItensNF := {}
	EndIf

	//+-------------------------------------------------------------------------------------------
	//|Abaixo � realizado o abatimento dos t�tulos avulso. Positivo (aTitAvulso) x Negativo(aAbat)
	//+-------------------------------------------------------------------------------------------
	//TUR45AbTA(aAbat,aTitAvulso)
	TUR45AbNf(aAbat,aItensNF)
Return .T.


/*/{Protheus.doc} TA45AddItNf
Fun��o auxiliar para obter os itens da NF
@type function
@author Anderson Toledo
@since 27/05/2016
@version 1.0
/*/
Static Function TA45AddItNf( aItensNF, oModelG85, cSerieNF, cNaturNF, cCodPrd, cTES  )
	Local nPosFRef	:= 0
	Local nPosSerie	:= 0
	Local nPosNaturez	:= 0
	Local nPosItem	:= 0

	//Verifica se j� existe filial de refer�ncia
	nPosFRef := aScan( aItensNF, {|x|, x[1] == oModelG85:GetValue('G85_FILREF') } )
	If nPosFRef == 0
		AAdd(aItensNF, { oModelG85:GetValue('G85_FILREF'), {} } )
		nPosFRef := Len(aItensNF)
	EndIf

	//Verifica a posi��o da s�rie da nota fical
	nPosSerie	:= aScan( aItensNF[nPosFRef][2], {|x| x[1] == cSerieNF }  )
	If nPosSerie == 0
		AAdd( aItensNF[nPosFRef][2], { cSerieNF, {} } )
		nPosSerie := Len(aItensNF[nPosFRef][2])
	EndIf

	//Verifica a posi��o da natureza
	nPosNaturez := aScan( aItensNF[nPosFRef][2][nPosSerie][2], {|x| x[1] == cNaturNF }  )
	If nPosNaturez == 0
		AAdd( aItensNF[nPosFRef][2][nPosSerie][2], { cNaturNF, {} } )
		nPosNaturez := Len(aItensNF[nPosFRef][2][nPosSerie][2])
	EndIf

	//Verifica a posi��o do item na NF
	nPosItem := aScan( aItensNF[nPosFRef][2][nPosSerie][2][nPosNaturez][2], {|x|  x[1] == cCodPrd .And. x[2] == cTES  } )
	If nPosItem == 0
		AAdd( aItensNF[nPosFRef][2][nPosSerie][2][nPosNaturez][2], { cCodPrd, cTES, 0 } )
		nPosItem := Len( aItensNF[nPosFRef][2][nPosSerie][2][nPosNaturez][2] )
	EndIf

 	aItensNF[nPosFRef][2][nPosSerie][2][nPosNaturez][2][nPosItem][3] += oModelG85:GetValue( 'G85_VALOR' )

Return


/*/{Protheus.doc} TA45GerNFS
Fun��o para gera��o da Nota Fiscal de sa�da
@type function
@author Anderson Toledo
@since 27/05/2016
@version 1.0
@param cCliente, character, C�digo do cliente
@param cLoja, character, C�digo da loja
@param cSerNFS, character, S�rie da nota fiscal
@param cNumNFS, character, Numero da nota fiscal
@param aItensNF, array, Itens para gera��o da nota fiscal
@param aLogG8E, array, Vetor que ser� retornado o log para a tabela G8E
/*/
Static Function TA45GerNFS( cCliente, cLoja, cSeriePar, cNumPar, aItensNF, aLogG8E )
	Local aCabs      	:= {}
	Local aItens     	:= {}
	Local aStruSF2   	:= {}
	Local aStruSD2   	:= {}
	Local aDocOri		:= {}
	Local cAlias		:= GetNextAlias()
	Local cTpCli		:= ''
	Local cTpRV		:= ''
	Local cItem 		:= '00'
	Local cCondPgto 	:= SuperGetMV('MV_TURCPCO')
	Local lRet			:= .T.
	Local nX        	:= 1
	Local nJ 			:= 0
	Local cNumero		:= cNumPar
	Local cSerie     	:= cSeriePar

	Local nF2FILIAL   := 0
	Local nF2TIPO     := 0
	Local nF2DOC      := 0
	Local nF2SERIE    := 0
	Local nF2EMISSAO  := 0
	Local nF2CLIENTE  := 0
	Local nF2LOJA     := 0
	Local nF2ESPECIE  := 0
	Local nF2COND     := 0
	Local nF2DTDIGIT  := 0
	Local nF2EST      := 0
	Local nF2VALMERC  := 0
	Local nF2TIPOCLI	 := 0
	Local nF2MOEDA	 := 0

	Local nD2FILIAL   := 0
	Local nD2DOC      := 0
	Local nD2SERIE    := 0
	Local nD2CLIENTE  := 0
	Local nD2LOJA     := 0
	Local nD2EMISSAO  := 0
	Local nD2TIPO     := 0
	Local nD2ITEM     := 0
	Local nD2CF       := 0
	Local nD2COD      := 0
	Local nD2UM       := 0
	Local nD2QUANT    := 0
	Local nD2PRCVEN   := 0
	Local nD2TOTAL    := 0
	Local nD2LOCAL    := 0
	Local nD2TES      := 0
	Local nD2BASEIPI  := 0
	Local nD2ALIQIPI  := 0
	Local nD2VALIPI   := 0
	Local nD2BASEICM  := 0
	Local nD2ALIQICM  := 0
	Local nD2VALICM   := 0
	Local nD2TP		:= 0
	Local nD2CODISS  	:= 0
	Local nD2ESTOQUE 	:= 0
	Local nD2PRUNIT	:= 0
	Local nD2CONTA  := 0

	Local oModelFat		 := FwModelActive()

	Private lMsErroAuto 	:= .F.

	If Empty( cCondPgto )
		Help(,,'TA045ANOCP',,STR0011,1,0)//"Solicite ao administrador do sistema cadastrar o param�tro 'MV_TURCPCO' com a condi��o de pagamento da nota fiscal de sa�da."
		Return .F.
	EndIf

	SE4->( dbSetOrder(1) )
	If  !(SE4->( DbSeek( xFilial( 'SE4' ) + cCondPgto )))
		Help(,,'TA045ACOND',,i18N(STR0018,{cCondPgto}),1,0)			//"Condi��o de pagamento #1[ condi��o pgto ]# n�o cadastrada."
		Return .F.
	EndIf


	SA1->( dbSetOrder(1) )
	If !SA1->( dbSeek( xFilial( 'SA1' ) + cCliente + cLoja ) )
		Help(,,'TA45NFS',,i18N(STR0013,{cCliente,cLoja}),1,0) //"C�digo #1[ C�digo cliente ]#/#2[ Loja cliente ]# n�o localizado no cadastro de cliente."
		Return .F.
	EndIf

	//Estrutura do dicionario utilizado pela rotina automatica
	aStruSF2    :=  SF2->(dbStruct())

	//Montagem da capa do documento fiscal
	nF2FILIAL   := Ascan(aStruSF2,{|x| AllTrim(x[1]) == 'F2_FILIAL'})
	nF2TIPO     := Ascan(aStruSF2,{|x| AllTrim(x[1]) == 'F2_TIPO'})
	nF2DOC      := Ascan(aStruSF2,{|x| AllTrim(x[1]) == 'F2_DOC'})
	nF2SERIE    := Ascan(aStruSF2,{|x| AllTrim(x[1]) == 'F2_SERIE'})
	nF2EMISSAO  := Ascan(aStruSF2,{|x| AllTrim(x[1]) == 'F2_EMISSAO'})
	nF2CLIENTE  := Ascan(aStruSF2,{|x| AllTrim(x[1]) == 'F2_CLIENTE'})
	nF2LOJA     := Ascan(aStruSF2,{|x| AllTrim(x[1]) == 'F2_LOJA'})
	nF2ESPECIE  := Ascan(aStruSF2,{|x| AllTrim(x[1]) == 'F2_ESPECIE'})
	nF2COND     := Ascan(aStruSF2,{|x| AllTrim(x[1]) == 'F2_COND'})
	nF2DTDIGIT  := Ascan(aStruSF2,{|x| AllTrim(x[1]) == 'F2_DTDIGIT'})
	nF2EST      := Ascan(aStruSF2,{|x| AllTrim(x[1]) == 'F2_EST'})
	nF2VALMERC  := Ascan(aStruSF2,{|x| AllTrim(x[1]) == 'F2_VALMERC'})
	nF2TIPOCLI	  := Ascan(aStruSF2,{|x| AllTrim(x[1]) == "F2_TIPOCLI"})
	nF2MOEDA	  := Ascan(aStruSF2,{|x| AllTrim(x[1]) == "F2_MOEDA"})
	nF2DTDIGIT  := Ascan(aStruSF2,{|x| AllTrim(x[1]) == "F2_DTDIGIT"})


	For nX := 1 To Len(aStruSF2)
	    If aStruSF2[nX][2] $ 'C/M'
	        Aadd(aCabs,'')
	    ElseIf aStruSF2[nX][2] == 'N'
	        Aadd(aCabs,0)
	    ElseIf aStruSF2[nX][2] == 'D'
	        Aadd(aCabs,CtoD('  /  /  '))
	    ElseIf aStruSF2[nX][2] == 'L'
	        Aadd(aCabs,.F.)
	    EndIf
	Next

	aCabs[nF2FILIAL]    :=  xFilial('SF2')
	aCabs[nF2TIPO]      := 'N'
	aCabs[nF2DOC]       := cNumero
	aCabs[nF2SERIE]     := cSerie
	aCabs[nF2EMISSAO]   := dDataBase
	aCabs[nF2CLIENTE]   := SA1->A1_COD
	aCabs[nF2LOJA]      := SA1->A1_LOJA
	aCabs[nF2ESPECIE]   := 'NF'
	aCabs[nF2COND]      := cCondPgto
	aCabs[nF2DTDIGIT]   := dDataBase
	aCabs[nF2EST]       := SA1->A1_EST
	aCabs[nF2TIPOCLI]	   := SA1->A1_TIPO
	aCabs[nF2MOEDA]		:= CriaVar( 'F2_MOEDA' )
	aCabs[nF2DTDIGIT]  	:= dDataBase


	//Estrutura do dicionario utilizado pela rotina automatica
	aStruSD2    :=  SD2->(dbStruct())

	//Montagem dos itens do documento fiscal
	nD2FILIAL   := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_FILIAL'})
	nD2DOC      := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_DOC'	})
	nD2SERIE    := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_SERIE'	})
	nD2CLIENTE  := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_CLIENTE'	})
	nD2LOJA     := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_LOJA'		})
	nD2EMISSAO  := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_EMISSAO'	})
	nD2TIPO     := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_TIPO'	})
	nD2ITEM     := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_ITEM'	})
	nD2CF       := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_CF'		})
	nD2COD      := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_COD'	})
	nD2UM       := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_UM'		})
	nD2QUANT    := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_QUANT'	})
	nD2PRUNIT	  := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_PRUNIT"	})
	nD2PRCVEN   := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_PRCVEN'})
	nD2TOTAL    := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_TOTAL'	})
	nD2LOCAL    := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_LOCAL'	})
	nD2TES      := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_TES'	})
	nD2BASEIPI  := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_BASEIPI'})
	nD2ALIQIPI  := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_IPI'	})
	nD2VALIPI   := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_VALIPI'})
	nD2BASEICM  := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_BASEICM'})
	nD2ALIQICM  := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_PICM'	})
	nD2VALICM   := Ascan(aStruSD2,{|x| AllTrim(x[1]) == 'D2_VALICM'})
	nD2TP		  := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_TP"})
	nD2CODISS   := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_CODISS"})
	nD2ESTOQUE  := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_ESTOQUE"})
	nD2CONTA    := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_CONTA"})


	SF4->( dbSetOrder(1) )
	SB1->( dbSetOrder(1) )
	SBZ->( dbSetOrder(1) )


	For nX := 1 to len( aItensNF )
		aCabs[nF2VALMERC]   += aItensNF[nX][3]

		aAdd(aItens, {})
		AADD(aDocOri,0)

		nPos := Len(aItens)

		For nJ := 1 To Len(aStruSD2)
			If  aStruSD2[nJ][2]$'C/M'
				aAdd(aItens[nPos],'')
			ElseIf aStruSD2[nJ][2]=='D'
				aAdd(aItens[nPos],CToD(''))
			ElseIf aStruSD2[nJ][2]=='N'
			   	aAdd(aItens[nPos],0)
			ElseIf aStruSD2[nJ][2]=='L'
			   	aAdd(aItens[nPos],.T.)
			EndIf
		Next

		aItens[Len(aItens),nD2FILIAL]  	:=  xFilial('SF2')
		cItem := Soma1( cItem )
		aItens[Len(aItens),nD2ITEM]    	:=  cItem
		aItens[Len(aItens),nD2DOC]     	:=  cNumero
		aItens[Len(aItens),nD2SERIE]   	:=  cSerie
		aItens[Len(aItens),nD2CLIENTE]	:=  SA1->A1_COD
		aItens[Len(aItens),nD2LOJA]    	:=  SA1->A1_LOJA
		aItens[Len(aItens),nD2EMISSAO] 	:=  dDatabase
		aItens[Len(aItens),nD2TIPO]    	:=  'N'
		aItens[Len(aItens),nD2UM]     	:=  'UN'
		aItens[Len(aItens),nD2QUANT]  	:=  1
		aItens[Len(aItens),nD2PRUNIT] 	:=  aItensNF[nX][3]
		aItens[Len(aItens),nD2PRCVEN] 	:=  aItensNF[nX][3]
		aItens[Len(aItens),nD2TOTAL]  	:=  aItensNF[nX][3]

		SB1->( dbSeek( xFilial('SB1') + aItensNF[nX][1] ) )

		aItens[Len(aItens),nD2LOCAL]  	:= SB1->B1_LOCPAD
		aItens[Len(aItens),nD2COD]     	:= SB1->B1_COD
		aItens[Len(aItens),nD2TP]     	:= SB1->B1_TIPO
		aItens[Len(aItens),nD2CONTA]    := SB1->B1_CONTA


		If !Empty( SB1->B1_CODISS )
			aItens[Len(aItens),ND2CODISS]	:= SB1->B1_CODISS
		ElseIf SBZ->( dbSeek( xFilial("SBZ") + aItensNF[nX][1] ) ) .And. !Empty( SBZ->BZ_CODISS )
			aItens[Len(aItens),ND2CODISS]	:= SBZ->BZ_CODISS
		EndIf


		If Empty( aItensNF[nX][2] ) .OR. !(SF4->( DbSeek( xFilial( 'SF4' ) + aItensNF[nX][2] ) ) .And. SF4->F4_DUPLIC == 'S')
			If Empty(SB1->B1_TS) .Or. !(SF4->( dbSeek( xFilial('SF4') + SB1->B1_TS) ) .And. SF4->F4_DUPLIC == 'S')
				Help(,,'TA45EFETES',,i18N(STR0014,{SB1->B1_COD}),1,0)			//"TES n�o cadastrada ou TES informada n�o gera duplicata para o produto #1[ codigo produto ]#."
				Return .F.
			EndIf
		EndIf

		aItens[Len(aItens),nD2TES]    	:= SF4->F4_CODIGO
		aItens[Len(aItens),nD2CF]		:= SF4->F4_CF
		aItens[Len(aItens),nD2ESTOQUE]	:= SF4->F4_ESTOQUE

	Next

	cNumero := MaNfs2Nfs(	"",; 		//Serie do Documento de Origem
				            	"",; 		//Numero do Documento de Origem
           			 		"",; 		//Cliente/Fornecedor do documento do origem
            					"",; 		//Loja do Documento de origem
           					cSerie,; 	//Serie do Documento a ser gerado
            					,;			//Mostra Lct.Contabil (OPC)
            					,;			//Aglutina Lct.Contabil (OPC)
            					,;			//Contabiliza On-Line (OPC)
            					,;			//Contabiliza Custo On-Line (OPC)
            					,;			//Reajuste de preco na nota fiscal (OPC)
            					,;			//Tipo de Acrescimo Financeiro (OPC)
            					,;			//Tipo de Arredondamento (OPC)
            					,;			//Atualiza Amarracao Cliente x Produto (OPC)
            					,;			//Cupom Fiscal (OPC)
            					,;			//CodeBlock de Selecao do SD2 (OPC)
            					,;			//CodeBlock a ser executado para o SD2 (OPC)
            					,;			//CodeBlock a ser executado para o SF2 (OPC)
            					,;			//CodeBlock a ser executado no final da transacao (OPC)
            					aDocOri,;	//Array com os Recnos do SF2 (OPC)
            					aItens,;	//Array com os itens do SD2 (OPC)
            					aCabs,;	//Array com os dados do SF2 (OPC)
          					  	.F.,;		//Calculo Fiscal - Desabilita o calculo fiscal pois as informacoes ja foram passadas nos campos do SD2 e SF2 (OPC)
           					,;			//code block para tratamento do fiscal - SF2 (OPC)
            					,;			//code block para tratamento do fiscal - SD2 (OPC)
            					,;			//code block para tratamento do fiscal - SE1 (OPC)
            					)	//Numero do documento fiscal (OPC)



	If lMsErroAuto
		MostraErro()
		lRet := .F.
	Else
		aLogG8E 	:= {}
		oModel 	:= FwModelActive(oModelFat)

		cNumPar	:= PadR(cNumero, TamSX3('F2_DOC')[1]	)
		cSeriePar	:= PadR(cSerie , TamSX3('F2_SERIE')[1]	)

		SF2->( dbSetOrder(1) )
		If SF2->( dbSeek( xFilial('SF2') + cNumPar + cSeriePar ) )
			T45Log( '2', '', 'Nota Fiscal: ' + cNumPar + ' S�rie: ' + cSeriePar + ' SF2 RecNo: ' + cValToChar( SF2->( RecNo() ) ) )

		aAdd( aLogG8E,{ 'G8E_PREFIX'	, oModel:GetValue( 'G84_MASTER','G84_PREFIX' ) 	} )
		aAdd( aLogG8E,{ 'G8E_NUMFAT'	, oModel:GetValue( 'G84_MASTER','G84_NUMFAT' ) 	} )
		aAdd( aLogG8E,{ 'G8E_CLIENT'	, oModel:GetValue( 'G84_MASTER','G84_CLIENT' ) 	} )
		aAdd( aLogG8E,{ 'G8E_LOJA'		, oModel:GetValue( 'G84_MASTER','G84_LOJA'   )   } )
		aAdd( aLogG8E,{ 'G8E_TIPO'		, '2' 									 	} )
		aAdd( aLogG8E,{ 'G8E_FILREF'	, cFilAnt									} )
			aAdd( aLogG8E,{ 'G8E_SERIE' 	, cSeriePar								} )
			aAdd( aLogG8E,{ 'G8E_NUMNF' 	, cNumPar									} )
		aAdd( aLogG8E,{ 'G8E_VALOR'		, aCabs[nF2VALMERC] 						} )
	
		Else
			FwAlertHelp(	i18N(STR0038,{ cNumPar, cSeriePar }),"",	"TA45ASEMNFS" ) //"Nota fiscal de sa�da #1[ Numero nota ]# S�rie #2[ S�rie ]# n�o localizado."
				
			lRet := .F.

		EndIf
	
	EndIf

Return lRet


/*/{Protheus.doc} TA45SetVl
Realiza o SetValue no modelo de dados informado com as informa��es passadas via parametro
@type function
@author Anderson Toledo
@since 27/05/2016
@version 1.0
@param oMdlMaster, objeto, Objeto onde ser�o setados os valores
@param cIdModel, character, Id do modelo que ser�o setados os valores
@param aDados, array, Valores que ser�o atribuidos ao modelo
@param lLinha, l�gico, Indica se ser� adicionado uma nova linha no modelo
/*/
Function TA45SetVl(oMdlMaster,cIdModel,aDados,lLinha,cCmpSeq)
	Local lRet		:= .T.
	Local nX		:= 0
	Local nY		:= 0
	Local nPos		:= 0
	Local nSeq		:= 0

	Local oModel 	:= oMdlMaster:GetModel(cIdModel)
	Local oStru	:= oModel:GetStruct()

	Default lLinha	:= .F.
	Default cCmpSeq	:= ""

	If !Empty(cCmpSeq)
		nSeq := Val(oModel:GetValue(cCmpSeq,oModel:Length())) + 1
	EndIf

	If lLinha

		If oModel:Length() == oModel:AddLine()
			Return .F.
		EndIf

	EndIf

	If !Empty(cCmpSeq)
		oModel:SetValue(cCmpSeq, StrZero(nSeq,TamSx3(cCmpSeq)[1]))
	EndIf

	For nX := 1 To Len( aDados )
		// Verifica se os campos passados existem na estrutura do modelo
		If oStru:HasField( aDados[nX][1] )
			If !( lRet := oMdlMaster:SetValue(cIdModel, aDados[nX][1], aDados[nX][2] ) )
				lRet := .F.
				Exit
			EndIf
		EndIf
	Next

Return lRet


/*/{Protheus.doc} TA045GetTit
Fun��o para obter os t�tulos gerados no faturamento
@type function
@author Anderson Toledo
@since 37/05/2016
@version 1.0
@param oModelG8E, objeto, Modelo da tabela G8E
@param aRet, array, Vetor onde ser�o retornado os valores obtidos
@param cTipo, character, Tipo de busca, sendo, 1 - T�tulos a receber, 2 - T�tulos gerados atrav�s de NFS
@param aTiposTit, array, Tipos de t�tulos que ser�o considerados
@param lRecno, l�gico, Indica se ser� retornado o Recno dos campos, ou chave prim�ria da SE1
@param lSaldo, l�gico, Indica se deve considerar somente t�tulos com saldo
/*/
Static Function TA045GetTit( oModelG8E, aRet, cTipo, aTiposTit, lRecno, lSaldo, nSaldo )
	Local aSaveLines := FwSaveRows()
	Local cTipoTit	:= ''
	Local lAllTipos 	:= .F.
	Local lRet			:= .T.
	Local nX			:= 0
	Local nPos			:= 0

	DEFAULT aTiposTit 	:= {}
	DEFAULT lRecno		:= .F.
	DEFAULT lSaldo		:= .F.
	DEFAULT nSaldo		:= 0

	lAllTipos := len( aTiposTit ) == 0

	SE1->( dbSetOrder(1) )

	For nX := 1 to oModelG8E:Length()
		oModelG8E:GoLine( nX )
		cTipoTit := oModelG8E:GetValue( 'G8E_TIPO' )

		If cTipoTit == cTipo

			If cTipoTit == '1' //Contas a Receber
				If lAllTipos .Or. aScan(aTiposTit, {|x| AllTrim(x) == AllTrim( oModelG8E:GetValue( 'G8E_TIPOTI' ) ) }) > 0

					If SE1->( dbSeek( xFilial('SE1',oModelG8E:GetValue('G8E_FILREF')) +;
					 						oModelG8E:GetValue( 'G8E_PFXTIT' ) +;
											oModelG8E:GetValue( 'G8E_NUMTIT' ) +;
											oModelG8E:GetValue( 'G8E_PARCEL' ) +;
											oModelG8E:GetValue( 'G8E_TIPOTI' ) ) )

						If lSaldo .And. SE1->E1_SALDO <= 0
							Loop
						Else
							nSaldo += SE1->E1_SALDO
						EndIf

						If lRecno
							aAdd( aRet, SE1->( Recno() ) )
						Else
							aAdd( aRet, SE1->(&(IndexKey(1))) )
						EndIf

						T45Log( '2', '', 'Tipo: ' + AllTrim( oModelG8E:GetValue( 'G8E_TIPOTI' ) ) + ' SE1 Index 1: ' + SE1->( &(IndexKey(1)) ) + ' RecNo: ' + cValToChar( SE1->( RecNo() ) ) )

					Else
						Exit
						lRet := .F.
					EndIf

				EndIf

			ElseIf cTipoTit == '2' //T�tulos de nota Fiscal

				If SE1->( dbSeek( xFilial('SE1',oModelG8E:GetValue('G8E_FILREF')) + oModelG8E:GetValue( 'G8E_SERIE' ) + oModelG8E:GetValue( 'G8E_NUMNF' ) ) )

					While SE1->( !EOF() ) .And. SE1->(E1_FILIAL + E1_PREFIXO + E1_NUM ) == xFilial('SE1',oModelG8E:GetValue('G8E_FILREF')) + oModelG8E:GetValue( 'G8E_SERIE' ) + oModelG8E:GetValue( 'G8E_NUMNF' )

						If SE1->E1_TIPO $ MVIRABT+"/"+MVCSABT+"/"+MVCFABT+"/"+MVPIABT+"/"+MVABATIM+"/"+MVRECANT	
							SE1->( dbSkip() )
							Loop						
						ElseIf lSaldo .And. SE1->E1_SALDO <= 0
							SE1->( dbSkip() )
							Loop
						Else
							nSaldo += SE1->E1_SALDO
						EndIf

						If lRecno
							aAdd( aRet, SE1->( Recno() ) )
						Else
							aAdd( aRet, SE1->(&(IndexKey(1))) )
						EndIf

						T45Log( '2', '', 'Tipo: NF SE1 Index 1: ' + SE1->( &(IndexKey(1)) ) + ' RecNo: ' + cValToChar( SE1->( RecNo() ) ) )

						SE1->( dbSkip() )
					EndDo
				Else
					lRet := .F.
					Exit
				EndIf

			ElseIf cTipoTit == '3' //T�tulos de liquida��o

				If SE1->( dbSeek(xFilial('SE1',oModelG8E:GetValue('G8E_FILREF')) +;
													oModelG8E:GetValue( 'G8E_PFXTIT' ) +;
													oModelG8E:GetValue( 'G8E_NUMTIT' ) +;
													oModelG8E:GetValue( 'G8E_PARCEL' ) +;
													oModelG8E:GetValue( 'G8E_TIPOTI' ) ) )

					If lSaldo .And. SE1->E1_SALDO <= 0
						Loop
					Else
						nSaldo += SE1->E1_SALDO
					EndIf

					T45Log( '2', '', 'Tipo: Liquida��o SE1 Index 1: ' + SE1->( &(IndexKey(1)) ) + ' RecNo: ' + cValToChar( SE1->( RecNo() ) ) )

					If lRecno
						aAdd( aRet, SE1->( Recno() ) )
					Else
						aAdd( aRet, SE1->(&(IndexKey(1))) )
					EndIf

				Else
					Exit
					lRet := .F.
				EndIf


			EndIf

		EndIf

	Next

	FwRestRows( aSaveLines )

Return lRet



/*/{Protheus.doc} TA45CompTit
Fun��o para compensa��o dos t�tulos
@type function
@author Anderson Toledo
@since 27/05/2016
@version 1.0
@param oModel	 , objeto, Modelo do fonte TURA045
@param aTitulos, array, Vetor com os t�tulos para compensa��o
@param aTitComp, array, Vetor com os t�tulos de adiantamento
@param cTpConc, caractere, Tipo da compensa��o '4' - Compensa��o de NCC e '5' - Compensa��o Fatura de adiantamento
/*/
Static Function TA45CompTit( oModel, aTitulos, aTitComp, cTpConc, nValCmp )
	Local aLog				:= {}
	Local aVlrTit			:= {}
	Local lRet 			:= .T.

	Local lContabiliza 	:= .F.
	Local lAglutina   	:= .F.
	Local lDigita   		:= .F.

	Local nX				:= 0
 	Local oModelG84 		:= oModel:GetModel( 'G84_MASTER' )

	DEFAULT cTpConc	:= '4'
	DEFAULT nValCmp	:= 0

	Pergunte('AFI340',.F.)

	lContabiliza  := MV_PAR11 == 1
	lAglutina   	:= MV_PAR08 == 1
	lDigita   		:= MV_PAR09 == 1

	//Armazena o saldo atual antes da compensa��o, para verificar quais t�tulos sofreram compensa��o
	For nX := 1 to len( aTitulos )
		SE1->( dbGoTo( aTitulos[nX] ) )
		aAdd( aVlrTit, SE1->E1_SALDO )
	Next


 	If MaIntBxCR(3,aTitulos,,aTitComp,,{lContabiliza,lAglutina,lDigita,.F.,.F.,.F.},,,,,IIf(nValCmp > 0,nValCmp,Nil))

    	For nX := 1 to len( aTitulos )
    		aLog := {}
    		SE1->( dbGoTo( aTitulos[nX] ) )

    		If SE1->E1_SALDO <> aVlrTit[nX]

				aAdd( aLog,{ 'G8E_PREFIX'	, oModelG84:GetValue( 'G84_PREFIX' ) 	} )
				aAdd( aLog,{ 'G8E_NUMFAT'	, oModelG84:GetValue( 'G84_NUMFAT' ) 	} )
				aAdd( aLog,{ 'G8E_CLIENT'	, oModelG84:GetValue( 'G84_CLIENT' ) 	} )
				aAdd( aLog,{ 'G8E_LOJA'		, oModelG84:GetValue( 'G84_LOJA' )   	} )
				aAdd( aLog,{ 'G8E_TIPO'		, cTpConc 									} )
				aAdd( aLog,{ 'G8E_FILREF'	, SE1->E1_FILORIG							} )
				aAdd( aLog,{ 'G8E_PFXTIT'	, SE1->E1_PREFIXO 						} )
				aAdd( aLog,{ 'G8E_NUMTIT'	, SE1->E1_NUM								} )
				aAdd( aLog,{ 'G8E_TIPOTI'	, SE1->E1_TIPO							} )
				aAdd( aLog,{ 'G8E_PARCEL'	, SE1->E1_PARCELA							} )
				aAdd( aLog,{ 'G8E_VALOR'		, aVlrTit[nX] - SE1->E1_SALDO 			} )
				aAdd( aLog,{ 'G8E_IDDOC'	, FINGRVFK7('SE1',&(TurKeyTitulo('SE1')))	} ) //FINGRVFK7()

				T45Log( '2', '', ' SE1 Index 1: ' + SE1->( &(IndexKey(1)) ) + ' RecNo: ' + cValToChar( SE1->( RecNo() ) ) + ' Valor Orig.: ' + cValToChar(aVlrTit[nX]) + ' Valor Atual: ' + cValToChar( SE1->E1_SALDO ) )

		    	If TA45SetVl(oModel,'G8E_DETAIL',aLog,!oModel:GetModel('G8E_DETAIL'):IsEmpty(),"G8E_SEQUEN")
					lRet := .T.
				Else
					Help(,,'TA45G8ENFS',,STR0015,1,0) //"Erro ao gravar o log."
					lRet := .F.
					Exit
				EndIf
		    EndIf

    	Next
    Else
    	Help('TA45COMPTI',1,"HELP",'TA042COMPA',STR0016,1,0)//"N�o foi poss�vel a compensa��o dos titulos."
 		lRet := .F.
	EndIf

Return lRet


/*/{Protheus.doc} TA45CmpAdi
Fun��o para compensa��o de adiantamentos
@type function
@author Anderson Toledo
@since 27/05/2016
@version 1.0
@param oModel, objeto, Modelo do fonte TURA045
/*/
Function TA45CmpAdi( oModel, aRegVen )

	Local aTitAdi 	:= {}
	Local aTitRV		:= {}
	Local lRet 		:= .T.
	Local nX			:= 0
	Local nY			:= 0
	Local nZ			:= 0
	Local oModelG85 	:= oModel:GetModel( 'G85_DETAIL' )
	Local oModelG84 	:= oModel:GetModel( 'G84_MASTER' )
	Local oModelG8E 	:= oModel:GetModel( 'G8E_DETAIL' )
	Local nSaldo		:= 0

	//verifica se existe t�tulo de adiantamento
	G8H->( dbSetOrder(1) )
	For nX := 1 To Len( aRegVen )

		If !lRet
			Exit
		EndIf

		If aRegVen[nX][7] > 0
			aTitAdi := TA45GetTAd( aRegVen[nX][1], aRegVen[nX][2] )

			For nY := 1 To Len(aTitAdi)

				aTitRV := {}

				//obtem os t�tulos gerados no faturamento
				TA045GetTit( oModel:GetModel('G8E_DETAIL'), aTitRV , '3',	,.T.,.T.,@nSaldo ) //Obtem os t�tulos gerados atrav�s de liquida��o

				//faz a compensa��o dos t�tulos
				If Len( aTitRV ) > 0

					//aRegVen[nX][7]	Valor do RV
					//aTitAdi[nY,4]	Saldo do RA - Fatura de Adiantamento
					//nSaldo			Valor dos t�tulos a receber da Fatura

					nValComp := Min(nSaldo,aRegVen[nX][7])
					nValComp := Min(nValComp, aTitAdi[nY,4])

					lRet := TA45CompTit( oModel, aTitRV, {aTitAdi[nY,3]}, '5', nValComp )

					If lRet
						//Posiciona no SE1
						dbSelectArea("SE1")
						DbGoTo(aTitAdi[nY,3])

						//Grava tabela G8I
		    			dbSelectArea("G8I")
		    			dbSetOrder(1)
		    			RecLock("G8I",.T.)
							G8I->G8I_FILIAL 	:= aTitAdi[nY,1]		//FilRef
							G8I->G8I_FATTUR 	:= aTitAdi[nY,2]		//Fatura de adiantamento
							G8I->G8I_CART 	:= 'R' 				//Receber
							G8I->G8I_SERIE	:= SE1->E1_PREFIXO
							G8I->G8I_NUM 		:= SE1->E1_NUM
							G8I->G8I_PARCEL 	:= SE1->E1_PARCELA
							G8I->G8I_TIPO 	:= SE1->E1_TIPO
							G8I->G8I_CLIENT 	:= SE1->E1_CLIENTE
							G8I->G8I_LOJA 	:= SE1->E1_LOJA
							G8I->G8I_VALOR 	:= nValComp
							G8I->G8I_DATAE 	:= dDataBase
							G8I->G8I_FILFAT 	:= xFilial("G84")
							G8I->G8I_PFXFAT 	:= oModelG84:GetValue( 'G84_PREFIX' )
							G8I->G8I_NUMFAT 	:= oModelG84:GetValue( 'G84_NUMFAT' )
							G8I->G8I_NUMID	:= aRegVen[nX][2]

						G8I->(MsUnLock())
					EndIf
				EndIf
			Next
		EndIf
	Next

Return lRet


/*/{Protheus.doc} TA45GetTAd
Fun��o para obter as faturas de adiantamento
@type function
@author Anderson Toledo
@since 27/05/2016
@version 1.0
@param cRegVen, character, C�digo do registro de venda
/*/
Static Function TA45GetTAd( cFilRef, cRegVen )
	Local cAliasG8H	:= GetNextAlias()
	Local nPOs			:= 0
	Local aFatAdt		:= {}

	BeginSql Alias cAliasG8H
		SELECT G8H_FILIAL G8HFILIAL, G8H_FATADI FATADI, SE1.R_E_C_N_O_ SE1RECNO, SE1.E1_SALDO SALDO
			FROM %Table:G8H% G8H
			INNER JOIN %Table:G8G% G8G
				ON G8H_FATADI = G8G_FATADI
					AND G8G_FILIAL = G8H_FILIAL
					AND G8G.%notDel%
			INNER JOIN %Table:SE1% SE1
				ON E1_PREFIXO = G8G_RAPREF
					AND E1_NUM = G8G_RA
					AND E1_PARCELA = G8G_RAPARC
					AND E1_TIPO = 'RA'
					AND E1_SALDO > 0
					AND E1_FILIAL = %Exp:xFilial('SE1',cFilRef)%
					AND SE1.%notDel%
			WHERE G8H_NUMID = %Exp:cRegVen%
				AND G8H_FILIAL = %Exp:xFilial('G8H',cFilRef)%
				AND G8H.%notDel%
	EndSql

	While (cAliasG8H)->( !Eof())

		Aadd( aFatAdt, {(cAliasG8H)->G8HFILIAL,(cAliasG8H)->FATADI,(cAliasG8H)->SE1RECNO,(cAliasG8H)->SALDO} )

		(cAliasG8H)->( dbSkip() )
	EndDo

	(cAliasG8H)->( dbCloseArea() )

Return	aFatAdt

/*/{Protheus.doc} TA045DeNFS
Fun��o para estorno de notas fiscais de sa�da gerada na efetiva��o
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA045DeNFS( cDoc, cSerie, cFornece, cLoja)
	Local aRegSD2	:= {}
	Local aRegSE1	:= {}
	Local aRegSE2	:= {}
	Local cMarca	:= ''
	Local lRet		:= .T.
	
	SF2->( dbSetOrder(1) )
	If SF2->( dbSeek( xFilial('SF2') + cDoc + cSerie ) )
		cMarca := GetMark(,'SF2','F2_OK')
		RecLock('SF2',.F.)
			SF2->F2_OK := cMarca
		SF2->( MsUnlock() )
		
		If MaCanDelF2('SF2',SF2->(RecNo()),@aRegSD2,@aRegSE1,@aRegSE2)
			 SF2->( MaDelNFS(aRegSD2,aRegSE1,aRegSE2))
		Else
			lRet 		:= .F.			
		EndIf
	EndIf

Return	lRet

/*/{Protheus.doc} TA045DeTiR
Fun��o para excluir titulos a receber
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA045DeTiR( cPrefixo, cNum, cTipo, cParc, cCliente, cLoja )
	Local aArray := {}

	Private lMsErroAuto := .F.

	//Exclus�o deve ter o registro SE1 posicionado
	SE1->( DbSetOrder(1) )
	If SE1->( DbSeek(xFilial('SE1') + PADR(cPrefixo,TamSX3('E1_PREFIXO')[1]) + PADR(cNum,TamSX3('E1_NUM')[1]) + PADR(cParc,TamSX3('E1_PARCELA')[1]) + PADR(cTipo,TamSX3('E1_TIPO')[1]) ) )

		aAdd(aArray,{ 'E1_PREFIXO' 	, cPrefixo  	, NIL } )
		aAdd(aArray,{ 'E1_NUM' 		, cNum 		, NIL } )
		aAdd(aArray,{ 'E1_TIPO' 		, cTipo 		, NIL } )
		aAdd(aArray,{ 'E1_PARCELA' 	, cParc		, NIL } )

		SA2->( dbSetOrder(1) )
		If SA2->( dbSeek( xFilial('SA2') + cCliente + cLoja ) )

			aAdd(aArray,{ 'E1_CLIENTE'	, SA2->A2_CLIENTE		, NIL } )
			aAdd(aArray,{ 'E1_LOJA'   	, SA2->A2_LOJCLI 		, NIL } )

		EndIf

	 	MsExecAuto( { |x,y| FINA040(x,y)}, aArray, 5)

		If lMsErroAuto
			MostraErro()
		EndIf

	EndIf

Return !lMsErroAuto

/*/{Protheus.doc} TA45GerLiq //N�o mudar o nome da fun��o, pois � utilizada no FINA460A
Fun��o para gera��o da liquida��o, agrupando todos os t�tulos a receber em um �nico t�tulo
@type function
@author Anderson Toledo
@since 27/05/2016
@version 1.0
@param oModel, objeto, Modelo do fonte TURA045 com os dados para gera��o da consolida��o
@param aLogG8E, array, Vetor onde ser�o armazenadas informa��es de log para a tabela G8E
/*/
Function TA45GerLiq( oModel, aLogG8E)
	
	Local aTitulos		:= {}
	Local aParcelas		:= {}
	Local aLog			:= {}
	Local aBaixar		:= {}
	Local aCond			:= {}
	
	Local cNumTit		:= ""
	Local cTipoTit		:= "FTC"
	Local cPrefix		:= ""
	Local cErrorMsg		:= ""
	Local cPrefixoRec	:= ""
	Local cTipoDocRec	:= ""
	Local cDocNumRec	:= ""
	Local cNatureRec	:= ""
	Local cChave		:= ""
	
	Local nTotFat		:= 0
	Local nValDesc		:= 0
	Local nValAbat		:= 0
	Local nValAcres		:= 0
	Local nValMult		:= 0
	Local nValAcre		:= 0 
	Local nValDecre		:= 0
	Local nX			:= 0
	
	Local dDateProc		:= dDataBase
	
	Local lRet 		:= .T.
	Local oModelG84	:= oModel:GetModel('G84_MASTER')
	
	If !( TA45ChkNat() )
		FwAlertHelp(STR0025,STR0026,STR0027)//"Natureza de Liquida��o Inv�lida"#"Confira o par�metro MV_TURLQNT e sua respectiva natureza."#"Natureza Inv�lida"	
		Return(.f.)
	EndIf
		
	If TA44RTpFat() == TP_VENDA
		cPrefix := SuperGetMV("MV_PFXFAT")
	ElseIf  TA44RTpFat() == TP_APURA
		cPrefix := SuperGetMV("MV_PFXAPU")
	ElseIf TA44RTpFat() == TP_BREAK
		cPrefix := SuperGetMV("MV_PFXBRK")
	EndIf

	//obtem os t�tulos gerados no faturamento
	TA045GetTit( oModel:GetModel('G8E_DETAIL'), aTitulos, '1',{'INT','REC'},.F.,.T. ) //Obtem os t�tulos de fatura com saldo
 	TA045GetTit( oModel:GetModel('G8E_DETAIL'), aTitulos, '2',		 		,.F.,.T. ) //Obtem os t�tulos gerados atrav�s de nota fiscal
	
	//Montagem do array aBaixar - ele � composto por todos os t�tulos que dever�o ser baixados para formar a liquida��o 
	If ( Len( aTitulos ) > 0 )
		
		SE1->( dbSetOrder(1) )
		
		For nX := 1 to Len(aTitulos)
		
			SE1->( dbSeek( aTitulos[nX] ) )
			
			//Armazena o total da liquida��o a ser gerada e os outros valores pertinentes ao processo
			nTotFat 	+= SE1->E1_SALDO
			nValDesc	:= 0
			nValAbat	:= SE1->E1_DESCONT
			nValAcres	:= SE1->E1_ACRESC
			nValMult	:= SE1->E1_MULTA
			dDateProc	:= dDataBase
			nValAcre	:= 0 
			nValDecre	:= SE1->E1_DESCFIN
			
			//Armazena os dados de T�tulo
		 	cNatureRec		:= GetMv("MV_TURLQNT")	//Natureza do T�tulo de Liquida��o 
			
			//Estrutura aBaixar
			aAdd(aBaixar,{	SE1->(Recno()),;	//[01] Recno SE1
							SE1->E1_SALDO,;		//[02] Valor Baixa
							SE1->E1_JUROS,;		//[03] Valor Juros
							nValDesc,;			//[04] Valor Desconto - Verificar como capturar o desconto
							nValAcres,;			//[05] Valor Acrescimo 
							nValMult,;			//[06] Valor Multa
							nValAcre,;			//[07] Valor de Acrescimo - o que indica o valor de acr�scimo, al�m do campo E1_ACRESC?
							nValDecre} )		//[08] Valor de Decrescimo
			
		Next nX
		
		//Validar titulo
		SE1->(dbSetOrder(1))	//E1_FILIAL+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO

		cNumTit := oModelG84:GetValue('G84_NUMFAT')

		lExiteTit := SE1->( DbSeek(xFilial('SE1')+oModelG84:GetValue('G84_PREFIX')+cNumTit))

		While lExiteTit

			lExiteTit := .F.

			If SE1->( DbSeek(xFilial('SE1')+oModelG84:GetValue('G84_PREFIX')+cNumTit))
			
				While SE1->(!Eof()) .And. xFilial('SE1')+oModelG84:GetValue('G84_PREFIX')+cNumTit == SE1->(E1_FILIAL+E1_PREFIXO+E1_NUM)

					If SE1->E1_TIPO == cTipoTit
						lExiteTit := .T.
						Exit
					EndIf

					SE1->(DbSkip())
			
				EndDo

				If lExiteTit
					cNumTit := Soma1(cNumTit)
				EndIf
				
			EndIf
			
		EndDo
		
		aCond := TURXCond(nTotFat,oModelG84:GetValue('G84_CONDPG'))
		
		lRet := Len(aCond) > 0 .And. nTotFat > 0
		
		If lRet 
		
			For nX := 1 to len(aCond)
				
				//Estrutura aParcelas
				aAdd(aParcelas,{	aCond[nX,2],;						//[01] Valor da Parcela
									aCond[nX,1],;						//[02] Data de Vencimento
									oModelG84:GetValue('G84_CLIENT'),;	//[03] Cliente
									oModelG84:GetValue('G84_LOJA'),;	//[04] Loja
									cPrefix,;							//[05] Prefixo do Titulo
								 	cNumTit,;							//[06] Numero do Titulo
								 	cNatureRec,;						//[07] Natureza do Titulo
								 	Char( 64 + nX)} )					//[08] Parcela do T�tulo
				
			Next nX
			
			lRet := TurLiqAut(cPrefix, cNumTit, cTipoTit, cNatureRec, 3, dDataBase, aBaixar, aParcelas, , @cErrorMsg)
			
			If ( !lRet )
				FwAlertHelp(cErrorMsg,STR0028,STR0029)//"Contacte o administrador de sistema."#"Liquida��o - Turismo"
			Else
	
				SE1->(DbSetOrder(2))//E1_FILIAL+E1_CLIENTE+E1_LOJA+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO
				
				aLogG8E := {}
				
				For nX := 1 to Len(aParcelas)
					
					cChave := xFilial("SE1")
					cChave += Padr(oModelG84:GetValue('G84_CLIENT'),TamSX3("E1_CLIENTE")[1])
					cChave += Padr(oModelG84:GetValue('G84_LOJA'),TamSX3("E1_LOJA")[1])
					cChave += Padr(aParcelas[nX,05],TamSX3("E1_PREFIXO")[1])
					cChave += Padr(aParcelas[nX,06],TamSX3("E1_NUM")[1])
					cChave += Padr(aParcelas[nX,08],TamSX3("E1_PARCELA")[1])
					cChave += Padr(cTipoTit,TamSX3("E1_TIPO")[1])
					
					If SE1->( dbSeek(cChave) )
						
						aLog := {}
	
						//Atualiza o parametro para n�o gerar duplicidade na liquida��o
						PutMV('MV_NUMLIQ', Soma1( SE1->E1_NUMLIQ ) )
	
						aAdd( aLog,{ 'G8E_PREFIX'	, oModelG84:GetValue('G84_PREFIX' ) 		} )
						aAdd( aLog,{ 'G8E_NUMFAT'	, oModelG84:GetValue('G84_NUMFAT' ) 		} )
						aAdd( aLog,{ 'G8E_CLIENT'	, oModelG84:GetValue('G84_CLIENT' ) 		} )
						aAdd( aLog,{ 'G8E_LOJA'		, oModelG84:GetValue('G84_LOJA' )  	 	} )
						aAdd( aLog,{ 'G8E_TIPO'		, '3' 									 		} )
						aAdd( aLog,{ 'G8E_FILREF'	, cFilAnt		 								} )
						aAdd( aLog,{ 'G8E_PFXTIT'	, SE1->E1_PREFIXO 							} )
						aAdd( aLog,{ 'G8E_NUMTIT'	, SE1->E1_NUM									} )
						aAdd( aLog,{ 'G8E_TIPOTI'	, SE1->E1_TIPO								} )
						aAdd( aLog,{ 'G8E_PARCEL'	, SE1->E1_PARCELA								} )
						aAdd( aLog,{ 'G8E_VALOR'	, SE1->E1_SALDO								} )
						aAdd( aLog,{ 'G8E_IDDOC'	, FINGRVFK7('SE1',&(TurKeyTitulo('SE1')))	} )
	
						T45Log( '2', '', 'Liquida��o: SE1 Indice 1:' + SE1->( &( IndexKey(1) ) ) + ' RecNo: ' + cValToChar( SE1->(RecNo()) ) + ' Valor: ' + cValToChar( SE1->E1_SALDO )  )	
						aAdd( aLogG8E, aClone(aLog) )
					Else
						Help(,,'TA45G8ELIQ',,STR0017,1,0) //"Erro ao gerar o log, t�tulo n�o encontrado."
						lRet := .F.
						Exit
					EndIf
				Next
			EndIf
		Else
			FwAlertHelp(STR0039,STR0028,STR0029)//"Contacte o administrador de sistema."#"Liquida��o - Turismo"
		EndIf

		FreeUsedCode()  //libera codigos de correlativos reservados pela MayIUseCode()

		aSize(aBaixar,0)
		aBaixar := Nil
		
		aSize(aParcelas,0)
		aParcelas := Nil
		
	EndIf

Return lRet


/*/{Protheus.doc} TA045ANNFS
Fun��o para valida��o se j� existe nota fiscal de sa�da com a chave indicada
@type function
@author Anderson Toledo
@since 06/07/2016
@version 1.0
@param cSerieNFS, caractere, Prefixo da nota fiscal
@param cNumNFS, caractere, Numero da nota fiscal
/*/
Static Function TA045ANNFS( cSerieNFS )
	Local aArea		:= GetArea()
	Local aAreaSF2	:= SF2->(GetArea())
	Local aAreaSM0	:= SM0->(GetArea())
	Local aFilCGC   	:= {"",""}
	Local cNumNFS		:= ""
	Local cAliasQry 	:= GetNextAlias()
	Local cFilCond	:= ""
	Local lBack     	:= .F.
	Local lGrpCNPJ  	:= .F.
	Local nFor      	:= 0
	Local cTpNrNfs	:= SuperGetMv('MV_TPNRNFS')

	SM0->( dbSetOrder(1) )
	If !SM0->( dbSeek( cEmpAnt + cFilAnt ) )
		RestArea(aAreaSM0)
		Return
	EndIf

	lGrpCNPJ  	:= MaIsNumCgc() // Verifica a utilizacao da numeracao por Agrupamento por CNPJ

	While !lBack

		If cTpNrNfs == "3"
			cNumNFS := MA461NumNf(.T.,cSerieNFS,cNumNFs,,)
		Else
			cNumNFS := NxtSX5Nota( cSerieNFS,.F.,cTpNrNfs )
		EndIf	

		If lGrpCNPJ
			aFilCGC := MatFilCgc(SM0->M0_CGC,"SF2") // Retorna o Compartilhamento conforme CNPJ
			cFilCond := "% SF2.F2_FILIAL IN " + FormatIN(aFilCGC[1],aFilCGC[2]) + " %"
		Else
			cFilCond := "% SF2.F2_FILIAL = '" + xFilial('SF2') + "' %"
		EndIf

		BeginSql Alias cAliasQry
			SELECT COUNT(*) QTD_SF2
				FROM %Table:SF2% SF2
				WHERE %Exp:cFilCond%
					AND SF2.F2_SERIE = %Exp:cSerieNFS%
					AND SF2.F2_DOC = %Exp:cNumNFS%
					AND SF2.%notDel%

		EndSql

		If (cAliasQry)->QTD_SF2 == 0 .And. TA045ATSE1( cSerieNFS, cNumNFS )
			lBack := .T.
		EndIf

		(cAliasQry)->(dbCloseArea())

	EndDo

	RestArea(aAreaSM0)
	RestArea(aAreaSF2)
	RestArea(aArea)

Return cNumNFS


/*/{Protheus.doc} TA045ATSE1
Fun��o para valida��o se j� existe t�tulo a receber com a chave indicada
@type function
@author Anderson Toledo
@since 06/07/2016
@version 1.0
@param cSerieNFS, caractere, Prefixo da nota fiscal/t�tulo
@param cNumNFS, caractere, Numero da nota fiscal/t�tulo
/*/
Static Function TA045ATSE1( cSerieNFS, cNumNFS )
	Local aArea    	:= GetArea()
	Local aAreaSE1 	:= SE1->( GetArea() )
	Local aAreaSM0 	:= SM0->( GetArea() )
	Local aFilCGC  	:= {"",""}
	Local cFilCond	:= ""
	Local cAliasQry	:= GetNextAlias()
	Local lBack    	:= .T.

	SM0->( dbSetOrder(1) )
	If !SM0->( dbSeek( cEmpAnt + cFilAnt ) )
		RestArea(aAreaSM0)
		Return
	EndIf

	If MaIsNumCgc() // Verifica a utilizacao da numeracao por Agrupamento por CNPJ
		aFilCGC := MatFilCgc(SM0->M0_CGC,'SE1') // Retorna o Compartilhamento conforme CNPJ
		cFilCond := "% SE1.E1_FILIAL IN " + FormatIN(aFilCGC[1],aFilCGC[2]) + " %"
	Else
		cFilCond := "% SE1.E1_FILIAL = '" + xFilial('SE1') + "' %"
	EndIf

	BeginSql Alias cAliasQry
		SELECT COUNT(E1_FILIAL) TITULOS
			FROM %Table:SE1% SE1
			WHERE %Exp:cFilCond%
				AND SE1.E1_PREFIXO = %Exp:cSerieNFS%
				AND SE1.E1_NUM = %Exp:cNumNFS%
				AND SE1.E1_TIPO = %Exp:MVNOTAFIS%
				AND SE1.%notdel%
	EndSql

	If (cAliasQry)->TITULOS > 0
		lBack := .F.
	EndIf

	(cAliasQry)->(dbCloseArea())

	RestArea( aAreaSM0 )
	RestArea( aAreaSE1 )
	RestArea( aArea )

Return lBack

Static Function T45AddVlr( aVetor, cNatureza, nValor )
	Local nPos := 0

	nPos := aScan( aVetor, {|x| x[1] == cNatureza} )

	If nPos == 0
		aAdd( aVetor, { cNatureza, 0 } )
		nPos := len( aVetor )
	EndIf

	aVetor[nPos][2] += nValor
Return

/*/{Protheus.doc} TA45ChkNat
Fun��o para valida��o da natureza de liquida��o
@type function
@author fernando Radu Muscalu
@since 28/10/2016
@version 1.0
/*/
Function TA45ChkNat()

Local cNatureza	:= GetMv("MV_TURLQNT",,"") 

Local lRet		:= .T.

If ( !Empty(cNatureza) )
	lRet := TurVldNat(cNatureza, .f.)
Else
	lRet := .F.
Endif
	
Return(lRet)


/*/{Protheus.doc} TA45DESV
Fun��o para Desvinculo de titulos e notas fiscais para prosseguir o estorno da fatura
@type function
@author Fernando Amorim(Cafu)
@since 31/05/2017
@version 1.0
/*/
Function TA45DESV(oModel,oModelG8E, aAutCan,cTexto)
Local lRet 		:= .T.
Local nX			:= 0	
Local cParcela 	:= ' '//SuperGetMv("MV_1DUP")
Local cTipoParc 	:= MVNOTAFIS 
Local aBaixa		:=	{}
Local aArea    	:= GetArea()
Local oModelG84	:= oModel:GetModel( 'G84_MASTER' )

Private lMsErroAuto 	:= .F.


For nX := 1 to Len(aAutCan)
	If !lRet
		Exit
	Endif
	SF2->( dbSetOrder(1) )
	If SF2->( dbSeek( xFilial('SF2') + aAutCan[nX][1] + aAutCan[nX][2] ) )
		SE1->( DbSetOrder(1) )
		If SE1->( DbSeek(xFilial('SE1') + PADR(SF2->F2_PREFIXO,TamSX3('E1_PREFIXO')[1]) + PADR(SF2->F2_DUPL,TamSX3('E1_NUM')[1]) + PADR(cParcela,TamSX3('E1_PARCELA')[1]) + PADR(cTipoParc,TamSX3('E1_TIPO')[1]) ) )
			aBaixa	:=	{}
			AADD( aBaixa, { "E1_PREFIXO" 	, SE1->E1_PREFIXO		, Nil } )	// 01
			AADD( aBaixa, { "E1_NUM"     	, SE1->E1_NUM		 	, Nil } )	// 02
			AADD( aBaixa, { "E1_PARCELA" 	, SE1->E1_PARCELA		, Nil } )	// 03
			AADD( aBaixa, { "E1_TIPO"    	, SE1->E1_TIPO		, Nil } )	// 04
			AADD( aBaixa, { "E1_CLIENTE"	, SE1->E1_CLIENTE		, Nil } )	// 05
			AADD( aBaixa, { "E1_LOJA"    	, SE1->E1_LOJA		, Nil } )	// 06
			AADD( aBaixa, { "AUTMOTBX"  	, "DAC"				, Nil } )	// 07
			AADD( aBaixa, { "AUTDTBAIXA"	, dDatabase			, Nil } )	// 08
			AADD( aBaixa, { "AUTHIST"   	, STR0033				, Nil } )	// 09 "Baixa por Desvinculo da NF"
			AADD( aBaixa, { "AUTDESCONT" 	, 0						, Nil } )	// 10
			AADD( aBaixa, { "AUTMULTA"	 	, 0						, Nil } )	// 11
			AADD( aBaixa, { "AUTJUROS" 		, 0						, Nil } )	// 12
			AADD( aBaixa, { "AUTOUTGAS" 	, 0						, Nil } )	// 13
			AADD( aBaixa, { "AUTVLRPG"  	, 0        			, Nil } )	// 14
			AADD( aBaixa, { "AUTVLRME"  	, 0						, Nil } )	// 15
			AADD( aBaixa, { "AUTCHEQUE"  	, ""					, Nil } )	// 16
			AADD( aBaixa, { "AUTVALREC"  	, SE1->E1_VALOR   	, Nil } )	// 17
			lMsErroAuto := .F.
			MSExecAuto({|x,y| Fina070(x,y)},aBaixa,3)
			
			If lMsErroAuto
				lRet := .F.
				MostraErro()				
			Endif
		
		EndIf
	Endif
Next nX

If lRet
	For nX := 1 to oModelG8E:Length()
		oModelG8E:GoLine( nX )
	
		cFilAnt := oModelG8E:GetValue( 'G8E_FILREF' )
		If oModelG8E:GetValue( 'G8E_TIPO' ) == '2' .And. Ascan(aAutCan, {|x| ALLTRIM(x[1]+x[2]) == ALLTRIM(oModelG8E:GetValue( 'G8E_NUMNF' )+ oModelG8E:GetValue( 'G8E_SERIE' ))}) > 0 //Nota Fiscal de saida
			lRet := oModelG8E:LoadValue( 'G8E_STAITE',STR0034) //'DESVINCULADO'
		EndIf
	Next
Endif	
If lRet 
	lRet := oModelG84:LoadValue( 'G84_MOTDES',cTexto)	
Endif
RestArea( aArea )		
Return	lRet


/*/{Protheus.doc} T45TitAv
Verifica se IF � estorno de Reembolso
@type function
@author osmar.junior
@since 04/09/2017
@version 1.0
@param cIdIf, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function T45TitAv(cIdIf)
	Local lRet := .F.
	Local aAreaG4C    	:= G4C->(GetArea())

	dbSelectArea("G4C")
	G4C->(dbSetOrder(2))
	G4C->( DbSeek(xFilial("G4C") + cIdIf) )
	If G4C->G4C_ESTORN == '1'
		lRet := .T.		
	EndIF

	RestArea( aAreaG4C )

Return lRet

/*/{Protheus.doc} TUR45AbNf
Abatimento dos Itens da NF. Positivo(aItensNF) x Negativo(aAbat)
@type function
@author osmar.junior
@since 08/09/2017
@version 1.0
@param aAbat, array, Abatimentos
@param aItensNF, array, Notas Fiscais
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function TUR45AbNf(aAbat,aItensNF)

	Local nFil		:= 0
	Local nSer		:= 0
	Local nNat		:= 0
	Local nIte		:= 0
	Local aIt		:= {}
	Local nTamVl	:= TamSx3("G85_VALOR")[1]
	Local nDecVl	:= TamSx3("G85_VALOR")[2]
	Local nX		:= 0
	Local nP		:= 0
	Local nItFilial	:= 0
	
	//+-------------------------------------------------------------------------------------------
	//|Abaixo � realizado o tratamento de abatimento dos Itens da NF. Positivo(aItensNF) x Negativo(aAbat)
	//+-------------------------------------------------------------------------------------------

	//Cria array auxiliar ordenado para abatimento por ordem do menor valor por filial
	If Len(aAbat) > 0 .And. Len(aItensNF) > 0
		For nFil := 1 To Len(aItensNF) //Filiais
			For nSer := 1 To Len(aItensNF[nFil,2]) //S�ries
				For nNat := 1 To Len(aItensNF[nFil,2,nSer,2]) //Natureza
					For nIte := 1 To Len(aItensNF[nFil,2,nSer,2,nNat,2]) //Itens
						AAdd(aIt,{aItensNF[nFil,1],nFil,nSer,nNat,nIte,aItensNF[nFil,2,nSer,2,nNat,2,nIte,3]})
					Next
				Next
			Next
		Next

		aIt := ASort(aIt,,,{|x,y| x[1]+STR(x[6],nTamVl,nDecVl) < y[1]+STR(y[6],nTamVl,nDecVl) })

		For nX := 1 To Len( aAbat )

			nP := AScan( aIt, {|x|, x[1] == aAbat[nX][1] } )

			If nP > 0

				nItFilial := 1

				//Abate enquanto existir itens da NF para Filial e valor de abatimento para a filial
				While nItFilial <= Len( aItensNF[aIt[nP,2]][2] ) .And. aAbat[nX][2] > 0

					nItFilial++

					If aIt[nP,6] > aAbat[nX][2]
						//Se o valor do item � maior que o abatimento, diminui o Item e zera o abatimento
						aIt[nP,6] -= aAbat[nX][2]
						aItensNF[aIt[nP,2]][2][aIt[nP,3]][2][aIt[nP,4]][2][aIt[nP,5]][3] = aIt[nP,6]
						aAbat[nX][2] 	:= 0

					Else
						//Se o valor do item � menor ou igual que o abatimento, diminui o abatimento e apaga o item
						aAbat[nX][2] -= aIt[nP,6]

						//Apaga o Item (Produto e TES)
						ADel( aItensNF[aIt[nP,2]][2][aIt[nP,3]][2][aIt[nP,4]][2], aIt[nP,5])
						ASize(aItensNF[aIt[nP,2]][2][aIt[nP,3]][2][aIt[nP,4]][2], Len(aItensNF[aIt[nP,2]][2][aIt[nP,3]][2][aIt[nP,4]][2]) - 1)
								 
						If Len(aItensNF[aIt[nP,2]][2][aIt[nP,3]][2][aIt[nP,4]][2]) == 0
							
							//Apagar a Natureza
							ADel(  aItensNF[aIt[nP,2]][2][aIt[nP,3]][2], aIt[nP,4] )
							ASize( aItensNF[aIt[nP,2]][2][aIt[nP,3]][2], Len( aItensNF[aIt[nP,2]][2][aIt[nP,3]][2] ) - 1 )
								
							If Len( aItensNF[aIt[nP,2]][2][aIt[nP,3]][2] ) == 0
								
								//Apagar a S�rie
								ADel(  aItensNF[aIt[nP,2]][2],aIt[nP,3] )
								ASize( aItensNF[aIt[nP,2]][2], Len( aItensNF[aIt[nP,2]][2] ) - 1 )
							
							EndIf		
						EndIf		

						//Apaga Array auxiliar
						ADel(aIt, nP)
						ASize(aIt, Len(aIt)-1)

						Exit

					EndIf
				EndDo
			EndIf
		Next
	EndIf
	
Return


/*/{Protheus.doc} TUR45AbTA
Abatimento dos t�tulos avulso. Positivo (aTitAvulso) x Negativo(aAbat)
@type function
@author osmar.junior
@since 08/09/2017
@version 1.0
@param aAbat, array, Abatimentos
@param aTitAvulso, array, T�tulos gerados avulsos
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function TUR45AbTA(aAbat,aTitAvulso)	

	Local nX			:= 0
	Local nPosFRef		:= 0
	
	//+-------------------------------------------------------------------------------------------
	//|Abaixo � realizado o abatimento dos t�tulos avulso. Positivo (aTitAvulso) x Negativo(aAbat)
	//+-------------------------------------------------------------------------------------------
	If Len(aAbat) > 0 .And. Len(aTitAvulso) > 0

		If Len(aAbat) >= Len(aTitAvulso)
			For nX := 1 To Len(aAbat)
				nPosFRef := AScan( aTitAvulso, {|x|, x[1] == aAbat[nX][1] } )

				If nPosFRef > 0
					If aTitAvulso[nPosFRef][3] >= aAbat[nX][2]
						aTitAvulso[nPosFRef][3] -= aAbat[nX][2]
						aAbat[nX][2] := 0
					Else
						aAbat[nX][2] -= aTitAvulso[nPosFRef][3]
						aTitAvulso[nPosFRef][3] := 0
					EndIf
				EndIf
			Next

		Else
			For nX := 1 To Len(aTitAvulso)
				nPosFRef := AScan( aAbat, {|x|, x[1] == aTitAvulso[nX][1] } )

				If nPosFRef > 0
					If aAbat[nPosFRef][2] >= aTitAvulso[nX][3]
						aAbat[nPosFRef][2] -= aTitAvulso[nX][3]
						aTitAvulso[nX][3] := 0
					Else
						aTitAvulso[nX][3] -= aAbat[nPosFRef][2]
						aAbat[nPosFRef][2] := 0
					EndIf
				EndIf
			Next
		EndIf
	EndIf
	
Return	