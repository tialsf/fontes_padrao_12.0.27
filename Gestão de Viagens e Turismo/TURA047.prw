#Include 'Protheus.ch'
#Include 'FWMVCDef.ch'
#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"
#INCLUDE "TURA047.ch"

#DEFINE IMP_SPOOL 2
#DEFINE IMP_PDF 6

/*/{Protheus.doc} TURA047
Cadastro de Fatura de Adiantamento - ID - PCREQ-6276/SIGATUR

@author Anderson Alberto
@since 23/09/2015
@version 1.0
/*/

Function TURA047()

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//?Declaracao de variaveis                                      ?
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Local aArea     := GetArea()
Local oBrowse   := Nil

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//?Cria Objeto Browse                                           ?
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
oBrowse:= FWMBrowse():New()
oBrowse:SetAlias('G8G')
oBrowse:AddLegend("G8G_ATIVA == '1'", 'RED'   ,STR0001 ) //'Faturas Desativadas'
oBrowse:AddLegend("G8G_ATIVA == '2'", 'GREEN' ,STR0002 ) //'Faturas Ativas'
oBrowse:SetCacheView(.F.)

oBrowse:SetDescription(STR0003)   // 'Faturas de Adiantamento'
oBrowse:Activate()

Return(aArea)

/*/{Protheus.doc} MenuDef
(Op寤es do menu)

@author Anderson Alberto
@since 23/09/2015
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/

Static Function MenuDef()

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//?Declaracao de variaveis                                      ?
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Local aRotina   := {}

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//?Op寤es para o usuarios                                       ?
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
ADD OPTION  aRotina TITLE  STR0004  ACTION  'PesqBrw'           OPERATION 1 ACCESS 0    // "Pesquisar"
ADD OPTION  aRotina TITLE  STR0005  ACTION  'VIEWDEF.TURA047'   OPERATION 2 ACCESS 0    // "Visualizar"
ADD OPTION  aRotina TITLE  STR0006  ACTION  'VIEWDEF.TURA047'   OPERATION 3 ACCESS 0    // "Incluir"
ADD OPTION  aRotina TITLE  STR0007  ACTION  'VIEWDEF.TURA047'   OPERATION 4 ACCESS 0    // "Alterar"
ADD OPTION  aRotina TITLE  STR0008  ACTION  'VIEWDEF.TURA047'   OPERATION 5 ACCESS 0    // "Excluir"
ADD OPTION  aRotina TITLE  STR0009  ACTION  'TURR047'           OPERATION 6 ACCESS 0    // "Imprimir"
ADD OPTION  aRotina TITLE  STR0010  ACTION  'TURA047ATIVA'      OPERATION 8 ACCESS 0    // "Ativar"
ADD OPTION  aRotina TITLE  STR0047  ACTION  'TURA047LEG'    	OPERATION 9 ACCESS 0    // "Legenda"


Return(aRotina)

/*/{Protheus.doc} ModelDef
(Regra de Negocio)

@author Anderson Alberto
@since 23/09/2015
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/

Static Function ModelDef()

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//?Declaracao de variaveis                                      ?
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Local oStruG8G  := FWFormStruct(1, 'G8G', /*lViewUsado*/)
Local oStruG8H  := FWFormStruct(1, 'G8H', /*lViewUsado*/)
Local oModel    := Nil
Local aRelacG8G := {}
Local aAux		:= {}

Local bValid	:= {||.T.}

aAdd(aRelacG8G,{ 'G8H_FILIAL', 'xFilial("G8G")'})
aAdd(aRelacG8G,{ 'G8H_FATADI', 'G8G_FATADI'    })

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//?Cria o objeto do Modelo de Dados                          ?
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
oModel := MPFormModel():New('TURA047MODEL', /*bPreValidacao*/, /*bPosValidacao*/, { |oModel| TURA047GRV( oModel )}, /*bCancel*/ )

oStruG8G:SetProperty('G8G_CLIENT'	,MODEL_FIELD_WHEN		,{|| Empty(FwFldGet("G8G_LOJA") )                                	})
oStruG8G:SetProperty('G8G_LOJA'		,MODEL_FIELD_WHEN		,{|| Empty(FwFldGet("G8G_LOJA") )                                	})
oStruG8G:SetProperty('G8G_VALOR'	,MODEL_FIELD_WHEN		,{|| INCLUI	})
oStruG8G:SetProperty('G8G_SEGMEN'	,MODEL_FIELD_WHEN		,{|| INCLUI	})
oStruG8G:SetProperty('G8G_MOEDA'	,MODEL_FIELD_WHEN		,{|| INCLUI .Or. ( ALTERA .And. Empty(G8G->G8G_RA))	})

oStruG8G:SetProperty('G8G_SEGMEN'	,MODEL_FIELD_VALID	,{|| Tur047Segm(oModel,"G8G_SEGMEN")  	})

oStruG8G:SetProperty('G8G_ATIVA'	,MODEL_FIELD_INIT		,{|| Iif(INCLUI,"1",G8G->G8G_ATIVA) 	})

xAux := FwStruTrigger( 'G8G_VALOR', 'G8G_SALDO', 'TUR047Sald()', .F. )
oStruG8G:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])

oModel:AddFields('G8GMASTER' , /*cOwner*/, oStruG8G, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )
oModel:addGrid('G8HDETAIL'   ,'G8GMASTER' ,oStruG8H, { |oModelGrid, nLine, cAcao,cCampo| G8HLINPRE(oModelGrid, nLine, cAcao, cCampo)}, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ ) 

oStruG8H:SetProperty('G8H_VLRUTI'	,MODEL_FIELD_INIT		,{|| Iif(!INCLUI,TUR047VlrUt(.T.),0)  	})
oStruG8G:SetProperty('G8G_VLRUTI'	,MODEL_FIELD_INIT		,{|| Iif(!INCLUI,TUR047Jauti(),0) 	})
oStruG8G:SetProperty('G8G_SALDO'	,MODEL_FIELD_INIT		,{|| Iif(!INCLUI,TUR047Sald(),0)	})

xAux := FwStruTrigger( 'G8H_NUMID', 'G8H_VLRUTI', 'TUR047VlrUt(.F.)', .F. )
oStruG8H:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4]) 

bValid := FWBuildFeature( STRUCT_FEATURE_VALID, "Tur047Cod()")
oStruG8H:SetProperty('G8H_NUMID',MODEL_FIELD_VALID,bValid)

oStruG8H := FWFormStruct(1, "G8H")
oModel:SetRelation('G8HDETAIL', aRelacG8G,  G8H->(IndexKey(1)))

oModel:SetPrimaryKey({'G8G_FILIAL', 'G8G_FATADI'})

oModel:AddCalc( 'TOTAL', 'G8GMASTER', 'G8HDETAIL', 'G8H_VLRUTI', 'VLRUTIL', 'SUM', , /*bInitValue*/,STR0051 /*cTitle*/, /*bFormula*/) // "Total utilizado: "

oModel:GetModel('G8HDETAIL'):SetUniqueLine({ 'G8H_FILIAL','G8H_NUMID' })

oModel:GetModel('G8GMASTER'):SetDescription(STR0011) //'Cadastro de Faturas Adiantamento'
oModel:GetModel('G8HDETAIL'):SetDescription(STR0003) //'Faturas de Adiantamento'

oModel:GetModel('G8HDETAIL'):SetOptional(.T.)

oModel:SetVldActivate( {|oModel| A047VLMod(oModel) } )

Return(oModel)

/*/{Protheus.doc} ViewDef
Defini豫o do interface

@author Anderson Alberto
@since 23/09/2015
@version 1.0
/*/

Static Function ViewDef()

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//?Declaracao de variaveis                                      ?
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Local oModel    := FWLoadModel('TURA047')
Local oStruG8G  := FWFormStruct(2, 'G8G')
Local oStruG8H  := FWFormStruct(2, 'G8H')
Local oStrTotal	:= FWCalcStruct( oModel:GetModel('TOTAL') )
Local oView     := FWFormView():New()
Local oRowHeight:= TGCSetRowHeight():New() // Classe para ajuste na Altura do Grid

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//?Remove campos da oStruG8G/oStruG8H                            ?
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
oStruG8G:RemoveField('G8G_STATUS')

oStruG8H:RemoveField('G8H_FATADI')

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//?Cria o objeto de View                                         ?
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
oView:SetModel(oModel)
oView:AddField('VIEW_G8G', oStruG8G, 'G8GMASTER')
oView:AddGrid('VIEW_G8H', oStruG8H, 'G8HDETAIL')
oView:AddField('TOTAL', oStrTotal,'TOTAL')

oView:CreateHorizontalBox('SUPERIOR', 70)
oView:CreateHorizontalBox('INFERIOR', 20)
oView:CreateHorizontalBox( 'TOTAL', 10,,.F.)
oView:CreateVerticalBox( 'BOXTOTAL', 100, 'TOTAL')

oView:SetOwnerView('VIEW_G8G', 'SUPERIOR')
oView:SetOwnerView('VIEW_G8H', 'INFERIOR')
oView:SetOwnerView('TOTAL','BOXTOTAL')

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//?Titulo das Pastas                                             ?
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
oView:EnableTitleView('VIEW_G8G' ,STR0003)    // 'Faturas de Adiantamento'

oView:AddIncrementField( 'VIEW_G8H', 'G8H_SEQUEN' ) 

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//?Atualiza豫o da altura do Grid na View                          ?
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
oRowHeight:SetRowHeigh(@oView)

Return(oView)

/*/{Protheus.doc} A047VLMod
Valida se formulario pode ser excluido

@author Anderson Alberto
@since 25/09/2015
@version 1.0
@param oModel, objeto, (Descri豫o do par?etro)
@return ${return}, ${return_description}
/*/

Static Function A047VLMod(oModel)
Local   nOperation  := oModel:GetOperation()
Local   lRet        := .T.

If lRet .And. (nOperation == MODEL_OPERATION_DELETE ) .And. !Empty(G8G->G8G_RA)
   SE1->(DbSetOrder(1)) //E1_FILIAL+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO
   If SE1->(DbSeek(xFilial("SE1")+G8G->(G8G_RAPREF+G8G_RA+G8G_RAPARC+'RA ')))
   		If SE1->E1_SALDO <> SE1->E1_VALOR
   			lRet	:= .F.
			Help(,,"TURA47JAMOV",,STR0049,1,0) // "N�o � poss�vel excluir a fatura, pois o t�tulo j� possui movimenta寤es financeiras."   			
   		EndIf
   EndIf
Endif


If lRet .And. (nOperation == MODEL_OPERATION_UPDATE ) .And. !Empty(G8G->G8G_RA)
   oModel:GetModel("G8GMASTER"):SetOnlyView(.T.)
EndIf

Return(lRet)

/*/{Protheus.doc} TURA047ATIVA
Ativa a fatura de adiantamento

@author Anderson Alberto
@since 25/09/2015
@version 1.0
@param oModel, objeto, (Descri豫o do par?etro)
@return ${return}, ${return_description}
/*/

Function TURA047ATIVA(oModel)

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//?Declaracao de variaveis           ?
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Local aFina040      := {}
Local cNatSE1       := Padr(GetNewPar('MV_FTADTNT',  ''), Len(SE1->E1_NATUREZ))
Local cNumTitRA     := ''
Local cPrefTitRA    := Padr(GetNewPar('MV_FTADTPR',  ''), Len(G8G->G8G_RAPREF))
Local cParcTitRA    := ''
Local cBancoAdt     := CriaVar("A6_COD")
Local cAgenciaAdt   := CriaVar("A6_AGENCIA")
Local cNumCon       := CriaVar("A6_NUMCON")
Local nMoedAdt      := Posicione("G5T",1,xFilial("G5T") + G8G->G8G_MOEDA,"G5T_MOEDAF")//Val(G8G->G8G_MOEDA)

Local cAliasG8H		:= "G8H"
Local cNumFatAdi	:= G8G->G8G_FATADI

Private lMsErroAuto := .F.

//Verifica se possui itens antes de efetivar. Se n�o possuir, n�o permite a ativa豫o.

If !Empty(G8G->G8G_RA)
	Help(,,"TURA47JAAT",,STR0048,1,0) // "Essa fatura de adiantamento j� est� ativa."
	Return(.T.)
EndIf

If Empty(cNatSE1)
	Help(,,"TURA47NAT",,STR0050,1,0) // "Favor preencher o par�metro MV_FTADTNT com o c�digo da natureza do t�tulo RA a ser gerado."
	Return(.T.)
EndIf

cAliasG8H			:= GetNextAlias()

BeginSql Alias cAliasG8H
	SELECT COUNT(*) REGS
	FROM %table:G8H% G8H
	WHERE G8H.G8H_FILIAL = %xFilial:G8H% 
	AND G8H.G8H_FATADI = %Exp:cNumFatAdi%
	AND G8H.%NotDel%
EndSql

DbSelectArea(cAliasG8H)
If (cAliasG8H)->REGS == 0 
	Help(,,"TURA47SEMRV",,STR0044,1,0) // "N�o � possivel ativar uma fatura sem RV."
	dbSelectArea(cAliasG8H)
	dbCloseArea()	
	Return(.T.)
EndIf

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//?Executa Inclusao do Titulo SE1                               ?
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

//UAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA¿
//³ Mostra Get do Banco de Entrada                              ³
//AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU
nOpca := 0
DEFINE MSDIALOG oDlg FROM 15, 5 TO 25, 38 TITLE STR0012 //"Local de Entrada"
@   1.0,2   Say STR0013 Of oDlg //"Banco : "

If cPaisLoc == "BRA"
    @   1.0,7.5 MSGET cBancoAdt F3 "SA6" Valid CarregaSa6( @cBancoAdt,,,,,,,) Of oDlg HASBUTTON
Else
    @   1.0,7.5 MSGET cBancoAdt F3 "SA6" Valid CarregaSa6( @cBancoAdt ) Of oDlg HASBUTTON
EndIf

@   2.0,2   Say STR0014 Of  oDlg //"Agencia : "
@   2.0,7.5 MSGET cAgenciaAdt Valid CarregaSa6(@cBancoAdt,@cAgenciaAdt) Of oDlg
@   3.0,2   Say STR0015 Of  oDlg //"Conta : "
@   3.0,7.5 MSGET cNumCon Valid CarregaSa6(@cBancoAdt,@cAgenciaAdt,@cNumCon,,,.T.) Of oDlg

@    .3,1 TO 5.3,15.5 OF oDlg
DEFINE SBUTTON FROM 061,097.1   TYPE 1 ACTION (nOpca := 1,If(!Empty(cBancoAdt).and. CarregaSa6(@cBancoAdt,@cAgenciaAdt,@cNumCon,,,.T.,, ) .And. VldMoedSA6(cBancoAdt,cAgenciaAdt,cNumCon,nMoedAdt),oDlg:End(),nOpca:=0)) ENABLE OF oDlg
ACTIVATE MSDIALOG oDlg

If nOpca == 1
   cNumTitRA := GETSXENUM("SE1", "E1_NUM",cPrefTitRA,1)
   ConfirmSX8()
   AAdd(aFina040, {'E1_NUM'        ,cNumTitRA                    ,NIL})
   AAdd(aFina040, {'E1_PARCELA'    ,cParcTitRA                   ,NIL})
   AAdd(aFina040, {'E1_PREFIXO'    ,cPrefTitRA                   ,NIL})
   AAdd(aFina040, {'E1_NATUREZ'    ,cNatSE1                      ,NIL})
   AAdd(aFina040, {'E1_TIPO'       ,'RA'                         ,NIL})
   AAdd(aFina040, {'E1_CLIENTE'    ,G8G->G8G_CLIENT              ,NIL})
   AAdd(aFina040, {'E1_LOJA'       ,G8G->G8G_LOJA                ,NIL})
   AAdd(aFina040, {'E1_VALOR'      ,G8G->G8G_VALOR               ,NIL})
   AADD(aFina040, {'E1_MOEDA'      ,nMoedAdt                     ,NIL})
   AAdd(aFina040, {'E1_TXMOEDA'    ,RecMoeda(dDatabase, nMoedAdt),NIL})
   AAdd(aFina040, {'E1_EMISSAO'    ,dDataBase                    ,NIL})
   AAdd(aFina040, {'E1_VENCTO'     ,dDataBase                    ,NIL})
   AAdd(aFina040, {'E1_VENCREA'    ,DataValida(dDataBase, .T.)   ,NIL})
   AADD(aFina040, {'E1_VENCORI'    ,DataValida(ddatabase, .T.)   ,NIL})
   AADD(aFina040, {'E1_ORIGEM'     ,'TURA047'                    ,NIL})
   AAdd(aFina040, {'CBCOAUTO'      ,cBancoAdt                    ,NIL})
   AAdd(aFina040, {'CAGEAUTO'      ,cAgenciaAdt                  ,NIL})
   AAdd(aFina040, {'CCTAAUTO'      ,cNumCon                      ,NIL})
   MSExecAuto({|x, y| FINA040(x, y)}, aFina040, 3)
   
   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
   //?Confirma豫o Grava豫o do Form                                 ?
   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
   If !lMsErroAuto
      RecLock("G8G",.F.)
      G8G->G8G_RA     := cNumTitRA
      G8G->G8G_RAPREF := cPrefTitRA
      G8G->G8G_RAPARC := cParcTitRA
      G8G->G8G_ATIVA  := "2"        
      G8G->(MsUnLock())
   Else
       MostraErro()
   Endif
EndIf

Return(.t.)

/*{Protheus.doc} TGCSetRowHeight
Classe para Ajuste do Tamanho da Grid

@author Anderson Alberto
@since 24/07/2015
@version P12
*/

Class TGCSetRowHeight
    Data nTamGrids

    Method New() CONSTRUCTOR
    Method SetRowHeigh(oView)
EndClass

/*{Protheus.doc} New
Inicializacao do Objeto

@author Anderson Alberto
@since 24/07/2015
@version P12
*/

Method New() Class TGCSetRowHeight

::nTamGrids := 20//GetMV('TI_GV2ALTG',, 30)

Return(Self)

/*{Protheus.doc} SetRowHeigh
Atribuicao do Tamanho das Grids da View recebida por parametro

@author Anderson Alberto
@since 24/07/2015
@version P12
*/

Method SetRowHeigh(oView) Class TGCSetRowHeight

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//?Declaracao de variaveis                                      ?
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Local aViewsT   := oView:aViews
Local nV        := 0
Local oAuxView  := Nil

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//?Atualiza豫o da altura do Grid na View                        ?
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
For nV:= 1 To Len(aViewsT)
    If Alltrim(aViewsT[nV][3]:CCLASSNAME) == "FWFORMGRID"
        oAuxView := aViewsT[nV][3]
        oAuxView:SetRowHeight(::nTamGrids)
    EndIf
Next nV

Return(Nil)

/*/{Protheus.doc} TURRVSEGM
Filtro do RV por Segmento. Utilizado na consulta padr�o G3PSEG

@author Simone Mie Sato Kakinoana
@since 27/01/2016
@version 1.0
/*/
Function TurRvSegm()

Local cRet		:= ""
Local oModel	:= FWModelActive()
Local cSegm 	:= oModel:GetValue("G8GMASTER","G8G_SEGMEN")
Local cCli 	:= oModel:GetValue("G8GMASTER","G8G_CLIENT")
Local cLoja	:= oModel:GetValue("G8GMASTER","G8G_LOJA")
DEFAULT	cSegm	:= ""

cRet	:= G3P->G3P_SEGNEG + G3P->G3P_CLIENT + G3P->G3P_LOJA == cSegm + cCli + cLoja

Return(cRet)

/*/{Protheus.doc} TUR047Sald
Atualizar o saldo: Valor - Valor utilizado

@author Simone Mie Sato Kakinoana
@since 27/01/2016
@version 1.0
/*/
Function TUR047Sald()

Local oModel	:= FWModelActive()
Local nValor	:= oModel:GetValue("G8GMASTER","G8G_VALOR")
Local nVlrUtil	:= oModel:GetValue("G8GMASTER","G8G_VLRUTI")
Local nRet		:= 0

nRet	:= nValor - nVlrUtil 

Return(nRet)

/*/{Protheus.doc} TUR047VlrUt
Atualizar o valor utilizado na grid na digita豫o do numero do RV.

@author Simone Mie Sato Kakinoana
@since 28/01/2016
@version 1.0
/*/
Function TUR047VlrUt(lIniPad)

Local nI		:= 1
Local nVlrUtil	:= 0

Local cAliasG8I		:= "G8I"
Local oModel		:= FWModelActive()
Local oModelG8H		:= oModel:GetModel("G8HDETAIL")
Local cPrefixo		:= oModel:GetValue("G8GMASTER","G8G_RAPREF")
Local cNum			:= oModel:GetValue("G8GMASTER","G8G_RA") 
Local cParcela		:= oModel:GetValue("G8GMASTER","G8G_RAPARC")
Local cTipo			:= "RA" 
Local cCliente		:= oModel:GetValue("G8GMASTER","G8G_CLIENT") 
Local cLoja			:= oModel:GetValue("G8GMASTER","G8G_LOJA") 
Local cNumID		:= ""
Local nOperation 		:= oModel:GetOperation()

DEFAULT lniPad := .T.

Private lMsErroAuto := .F.

//If nOperation == MODEL_OPERATION_INSERT
If lIniPad	//Se tiver sido chamado pelo inicializador padr�o.
	cNumID		:= G8H->G8H_NUMID	
Else		//Se for gatilho.	
	cNumID		:= oModelG8H:GetValue("G8H_NUMID")	
EndIF
 
cAliasG8I			:= GetNextAlias()

BeginSql Alias cAliasG8I
	SELECT SUM(G8I_VALOR) VALOR
	FROM %table:G8I% G8I
	WHERE G8I.G8I_FILIAL = %xFilial:G8I%
	AND G8I.G8I_SERIE =  %Exp:cPrefixo%
	AND G8I.G8I_NUM =  %Exp:cNum%
	AND G8I.G8I_PARCEL =  %Exp:cParcela%
	AND G8I.G8I_TIPO =  %Exp:cTipo%
	AND G8I.G8I_CLIENT =  %Exp:cCliente%
	AND G8I.G8I_LOJA =  %Exp:cLoja%
	AND G8I.G8I_NUMID = %Exp:cNumID%
	AND G8I.%NotDel%
EndSql
 
DbSelectArea(cAliasG8I)
nVlrUtil	:= (cAliasG8I)->VALOR 

dbSelectArea(cAliasG8I)
dbCloseArea()

Return(nVlrUtil)


/*/{Protheus.doc} TUR047JaUti
Atualizar o valor utilizado do cabe�alho.

@author Simone Mie Sato Kakinoana
@since 28/01/2016
@version 1.0
/*/
Function TUR047JaUti()

Local nVlrUtil	:= 0

Local cAliasG8I		:= "G8I"
Local oModel		:= FWModelActive()
Local cPrefixo		:= oModel:GetValue("G8GMASTER","G8G_RAPREF")
Local cNum			:= oModel:GetValue("G8GMASTER","G8G_RA") 
Local cParcela		:= oModel:GetValue("G8GMASTER","G8G_RAPARC")
Local cTipo			:= "RA "  
Local cCliente		:= oModel:GetValue("G8GMASTER","G8G_CLIENT") 
Local cLoja			:= oModel:GetValue("G8GMASTER","G8G_LOJA") 
Local nOperation 	:= oModel:GetOperation()

DbSelectArea("SE1")
DbSetOrder(2)	//E1_FILIAL+E1_CLIENTE+E1_LOJA+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO                                                                                                                                                                                                                 
If SE1->(DbSeek(xFilial("SE1")+cCliente+cLoja+cPrefixo+cNum+cParcela+cTipo))
	nVlrUtil	:= SE1->E1_VALOR - SE1->E1_SALDO
EndIf


Return(nVlrUtil)

/*/{Protheus.doc} TUR047Segm
Na altera豫o do tipo do segmento, dever� limpar a grid. Somente na INCLUS홒.

@author Simone Mie Sato Kakinoana
@since 28/01/2016
@version 1.0
/*/
Function TUR047Segm(cCampo)

Local nI				:= 1
Local oModel			:= FWModelActive()
Local oModelG8G			:= oModel:GetModel("G8GMASTER")
Local oModelG8H			:= oModel:GetModel("G8HDETAIL")
Local oView				:= FWViewActive()
Local cSegmento			:= oModelG8G:GetValue("G8G_SEGMEN")
Local aSaveLines		:= FWSaveRows()

If !Empty(cSegmento)
	
	If oModelG8H:Length()	== 1 .And. Empty(oModelG8H:GetValue("G8H_NUMID"))
		Return(.T.)	
	EndIf
	
	//Deleta todas as linhas 
	For nI := oModelG8H:Length() to  1 Step -1
		oModelG8H:Goline(nI)
		oModelG8H:DeleteLine(.T.,.T.)
	Next nI
          
	//Adiciona uma nova linha e troca o valor dela com a ultima linha que ficou como deletada
	nNewLine := oModelG8H:AddLine()
	oModelG8H:LineShift(1,nNewLine)
	
	//deleta novamente
	For nI := oModelG8H:Length() to  1 Step -1
		oModelG8H:Goline(nI)
		oModelG8H:DeleteLine(.T.,.T.)
	Next nI
	
	//tira o delete da linha em branco
	oModelG8H:UnDeleteLine()
	//Limpa o modelo da Grid como se n�o tivesse altera豫o, por�m, N홒 deve ser utilizado sozinho na ALTERA플O, pois,;
	//limpa apenas o que est� na mem�ria (modelo) e n�o no banco de dados, permanecendo os dados no banco ap�s o commit.;
	//No caso desta fun豫o primeiro deletamos todas as linhas e apenas chamamos o ClearData para limpar a �ltima linha em branco 
	oModelG8H:ClearData()
	
EndIf

FWRestRows(aSaveLines)

Return(.T.)

/*/{Protheus.doc} G8HLINPRE(oModelGrid, nLine, cAcao, cCampo)
Se j� existe valor utilizado, nao � permitida a dele豫o da linha.

@author Simone Mie Sato Kakinoana
@since 29/01/2016
@version 1.0
/*/
Static Function G8HLINPRE(oModelGrid, nLine, cAcao, cCampo)							
Local lRet    	:= .T.
Local oModel	:= FWModelActive()
Local oModelG8H	:= oModel:GetModel("G8HDETAIL")
Local cNumID	:= oModelG8H:GetValue("G8H_NUMID")
Local nVlrUtil	:= oModelG8H:GetValue("G8H_VLRUTI")
Local nOperation:= oModel:GetOperation()

If cAcao = 'DELETE' .And. nOperation == MODEL_OPERATION_UPDATE .And. nVlrUtil > 0 
	Help( " ", 1, "TURA47NODEL",,STR0043, 1, 0 )//"N�o � possivel excluir este registro, pois o mesmo ja possui movimenta豫o."
	lRet := .F.
ElseIf cAcao = 'CANSETVALUE' .And. nOperation == MODEL_OPERATION_UPDATE .And. nVlrUtil > 0
	Help( " ", 1, "TURA47NOALT",,STR0045, 1, 0 )//"N�o � possivel alterar  o c�digo do RV, pois o mesmo ja possui movimenta豫o."
	lRet := .F.
EndIf

Return lRet

/*/{Protheus.doc} TUR047Cod
Verifica se pode alterar o codigo do RV.

@author Simone Mie Sato Kakinoana
@since 29/01/2016
@version 1.0
/*/
Function TUR047Cod()

Local aSaveLines:= FWSaveRows()

Local lRet	:= .T.

Local oModel	:= FWModelActive()
Local oModelG8G	:= oModel:GetModel("G8GMASTER")
Local oModelG8H	:= oModel:GetModel("G8HDETAIL")

Local cNumID		:= oModelG8H:GetValue("G8H_NUMID")
Local cSegmen		:= oModelG8G:GetValue("G8G_SEGMEN")
Local cNumIDAtu	:= ""
Local cMensagem	:= "'

Local nLinAtu		:= oModelG8H:GetLine()	
Local nVlrUtil	:= oModelG8H:GetValue("G8H_VLRUTI")
Local nOperation	:= oModel:GetOperation()
Local nX			:= 0
 
If nVlrUtil = 0

	G3P->(DbSetOrder(1))
	If !G3P->(DbSeek(xFilial('G3P')+cNumId+cSegmen))
		Help(" ",1,"REGNOIS")
		lRet	:= .F.
	ElseIf  G3P->G3P_CLIENT <> oModelG8G:GetValue("G8G_CLIENT") .or. G3P->G3P_LOJA <> oModelG8G:GetValue("G8G_LOJA")
		Help(" ",1,"TURA47_CLIENTE",,STR0053,1,0)//'Registro de Venda invalido para esse Cliente e Loja'
		lRet	:= .F.
	EndIf
	//Se esta incluindo uma nova linha na altera豫o, verifica se o codigo RV j� foi utilizado
	If lRet
		For nX := 1 to oModelG8H:Length()
		
			If nX == nLinAtu
				Loop
			EndIf
			
			oModelG8H:GoLine(nX)
	
			cNumIDAtu	:= Alltrim(oModelG8H:GetValue("G8H_NUMID"))
				
			If !oModelG8H:IsDeleted()
				
				If Alltrim(cNumID) == cNumIDAtu
					cMensagem	:= STR0046 + CRLF						// "O codigo do RV pode ser utilizado somente uma vez por fatura de adiantamento."								
					Help(" ",1,"TURA47USAD",,cMensagem,1,0)
					cMensagem	:= ""		
					lRet := .F.
					Exit
				EndIf
			
			EndIf
		Next nX
		
		oModelG8H:GoLine(nLinAtu)
	Endif
Endif

Return(lRet)

/*/{Protheus.doc} TURA047Leg
Legenda.

@author Simone Mie Sato Kakinoana
@since 01/02/2016
@version 1.0
/*/
Function Tura047Leg()

BrwLegenda(STR0047,STR0003,{;                               //"Legenda"###"Faturas de Adiantamento"###
	                        { "BR_VERMELHO" , STR0001 },;  //'Faturas Desativadas'
                            { "BR_VERDE"    , STR0002 }})  //'Faturas Ativas'

Return Nil

/*/{Protheus.doc} VldMoedSA6
Valida a moeda do cadastro de banco.

@author Simone Mie Sato Kakinoana
@since 12/02/2016
@version 1.0
/*/
Static Function VldMoedSA6(cBancoAdt,cAgenciaAdt,cNumCon,nMoedAdt) 

Local aSaveArea	:= GetArea()
Local lRet	:= .T.

DbSelectArea("SA6")
DbSetOrder(1)
If SA6->(DbSeek(xFilial("SA6")+cBancoAdt+cAgenciaAdt+cNumCon))	
	If SA6->A6_MOEDA <> nMoedAdt
		lRet	:= .F.
		Help(,,"TURA47MOED",,STR0052,1,0) // "A moeda do banco selecionado � diferente da moeda da fatura."	
	Endif
EndIf


RestArea(aSaveArea)

Return(lRet)
//-------------------------------------------------------------------
/*/{Protheus.doc} Tura047Grv
Grava豫o dos dados

@author Simone Mie Sato Kakinoana
@since 02/02/2016
@version 12
/*/
//-------------------------------------------------------------------
Function Tura047Grv( oModel )

Local   nOperation  := oModel:GetOperation()
Local   lRet        := .T.
Local   aFina040    := {}
Local   cNatSE1     := Padr(GetNewPar('MV_FTADTNT',  ''), Len(SE1->E1_NATUREZ))
Private lMsErroAuto := .F.

If lRet .And. (nOperation == MODEL_OPERATION_DELETE ) .And. !Empty(G8G->G8G_RA)
   SE1->(DbSetOrder(1)) //E1_FILIAL+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO
   SE1->(DbSeek(xFilial("SE1")+G8G->(G8G_RAPREF+G8G_RA+G8G_RAPARC+'RA ')))
   
   AAdd(aFina040, {'E1_NUM'        ,G8G->G8G_RA       ,NIL})
   AAdd(aFina040, {'E1_PARCELA'    ,G8G->G8G_RAPARC   ,NIL})
   AAdd(aFina040, {'E1_PREFIXO'    ,G8G->G8G_RAPREF   ,NIL})
   AAdd(aFina040, {'E1_NATUREZ'    ,cNatSE1           ,NIL})
   AAdd(aFina040, {'E1_TIPO'       ,'RA'              ,NIL})
   AAdd(aFina040, {'E1_CLIENTE'    ,G8G->G8G_CLIENT   ,NIL})
   AAdd(aFina040, {'E1_LOJA'       ,G8G->G8G_LOJA     ,NIL})
   AAdd(aFina040, {'E1_VALOR'      ,G8G->G8G_VALOR    ,NIL})
   AAdd(aFina040, {'E1_EMISSAO'    ,SE1->E1_EMISSAO   ,NIL})
   AAdd(aFina040, {'E1_VENCTO'     ,SE1->E1_VENCTO    ,NIL})
   AAdd(aFina040, {'E1_VENCREA'    ,SE1->E1_VENCREA   ,NIL})
   MSExecAuto({|x, y| FINA040(x, y)}, aFina040, 5)
   
   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
   //?Confirma豫o Grava豫o do Form                                 ?
   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
   If lMsErroAuto
      MostraErro()
      lRet := .F.
   Endif
EndIf

If lRet
	FWFormCommit( oModel )
EndIf

Return(lRet)

/*/{Protheus.doc} TURR047
Impressao da fatura de adiantamento

@author Anderson Alberto
@since 25/09/2015
@version 1.0
@param oModel, objeto, (Descri豫o do par?etro)
@return ${return}, ${return_description}
/*/
Function TURR047(oModel)

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//?Declaracao de variaveis                                      ?
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Local oFnt12  := TFont():New( "Times New Roman" ,,12,,.F.,,,,,.F. )
Local oFnt12B := TFont():New( "Times New Roman" ,,12,,.T.,,,,,.F. )
Local oFnt14  := TFont():New( "Times New Roman" ,,14,,.F.,,,,,.F. )
Local oFnt14B := TFont():New( "Times New Roman" ,,14,,.T.,,,,,.F. )
Local oFnt15  := TFont():New( "Times New Roman" ,,15,,.F.,,,,,.F. )
Local oFnt15B := TFont():New( "Times New Roman" ,,15,,.T.,,,,,.F. )
Local oFnt18B := TFont():New( "Times New Roman" ,,18,,.T.,,,,,.F. )
 
Local lAdjustToLegacy := .T.
Local lDisableSetup := .T.

Local cMemo	:= ""
Local cFilePrint,oPrn
Local nCol   :=0010
Local nLinIni:=0050
Local nLinFim:=3100
Local nColIni:=0050
Local nColFim:=2300
Local nI	:= 0 

Local aMemo	:= {}

cFilePrint := "fatura_adiantamento_"+ALLTRIM(G8G->G8G_FATADI)
oPrn := FWMSPrinter():New(cFilePrint,IMP_PDF, lAdjustToLegacy, ,lDisableSetup,,,,,.F.,,.T.) 
oPrn:SetPortrait()
oPrn:SetPaperSize(DMPAPER_A4) 
oPrn:SetMargin(05,05,05,05) // nEsquerda, nSuperior, nDireita, nInferior 
oPrn:cPathPDF := AllTrim(GetTempPath())
oPrn:lPDFasPNG := .F. 

oPrn:StartPage()

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//?Cabecalho do relatorio            ?
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

oPrn:Say(nLinini+010,nColIni+200,Alltrim(SM0->M0_NOMECOM),oFnt12B)
oPrn:Say(nLinini+050,nColIni+200,STR0016+Alltrim(SM0->M0_ENDCOB)+", "+Alltrim(SM0->M0_COMPCOB)+"  "+Alltrim(SM0->M0_BAIRCOB),oFnt12B) //"Endereco : "
oPrn:Say(nLinini+100,nColIni+200,STR0017+Alltrim(SM0->M0_CIDCOB)+" - "+STR0018+Alltrim(SM0->M0_ESTCOB)+" "+STR0019+Alltrim(SM0->M0_CEPCOB),oFnt12B) //"Cidade: # Estado: # Cep:"
oPrn:Say(nLinIni+150,nColIni+200,STR0020+TransForm(SM0->M0_TEL, "@R (99) 9999-9999" )+" / "+STR0021+TransForm(SM0->M0_FAX, "@R (99) 9999-9999" ),oFnt12B) //"Fone: # Fax: "
oPrn:Say(nLinIni+200,nColIni+200,STR0022+SM0->M0_CGC,oFnt12B) // "CNPJ: "

oPrn:Line (nLinIni+250,nColIni,nLinIni+250,nColFim)
oPrn:Say(nLinIni+300,nColIni+1200,G8G->G8G_FATADI,oFnt14B)

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//?Dados do sacado                   ?
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
SA1->(DbSetOrder(1))
SA1->(DbSeek(xFilial("SA1")+G8G->(G8G_CLIENT+G8G_LOJA)))

oPrn:Box(nLinIni+350,nColIni+0100,nLinIni+800,nColFim-100)

oPrn:Say(nLinini+400,nColIni+0200,STR0023                               ,oFnt12B) //"Nome do Sacado"
oPrn:Say(nLinini+400,nColIni+0500,": "+SA1->A1_NOME                     ,oFnt12B)
oPrn:Say(nLinini+400,nColIni+1400,STR0024                               ,oFnt12B) //"Codigo"
oPrn:Say(nLinini+400,nColIni+1600,": "+SA1->(A1_COD+A1_LOJA)            ,oFnt12B)

oPrn:Say(nLinini+450,nColIni+0200,STR0025                               ,oFnt12B) //"Endereco"
oPrn:Say(nLinini+450,nColIni+0500,": "+SA1->A1_END                      ,oFnt12B)

oPrn:Say(nLinini+500,nColIni+0200,STR0026                               ,oFnt12B) //"Complemento"
oPrn:Say(nLinini+500,nColIni+0500,": "+SA1->A1_COMPLEM                  ,oFnt12B)
oPrn:Say(nLinini+500,nColIni+1400,STR0027                               ,oFnt12B) //"Cep"
oPrn:Say(nLinini+500,nColIni+1600,": "+SA1->A1_CEP                      ,oFnt12B)

oPrn:Say(nLinini+550,nColIni+0200,STR0028                               ,oFnt12B) //"Cidade/Estado"
oPrn:Say(nLinini+550,nColIni+0500,": "+SA1->(Trim(A1_MUN)+"/"+A1_EST)   ,oFnt12B)
oPrn:Say(nLinini+550,nColIni+1400,STR0029                               ,oFnt12B) //"Bairro"
oPrn:Say(nLinini+550,nColIni+1600,": "+SA1->A1_BAIRROC                  ,oFnt12B)

oPrn:Say(nLinini+600,nColIni+0200,STR0030                               ,oFnt12B) //"Conta Contabil"
oPrn:Say(nLinini+600,nColIni+0500,": "+SA1->A1_CONTA                    ,oFnt12B)
oPrn:Say(nLinini+600,nColIni+1400,STR0031                               ,oFnt12B) //"Fone"
oPrn:Say(nLinini+600,nColIni+1600,": "+ALLTRIM(TRANSFORM(SA1->A1_TEL,PesqPict('SA1','A1_TEL')))+" "+STR0021+ALLTRIM(TRANSFORM(SA1->A1_FAX,PesqPict('SA1','A1_FAX'))),oFnt12B) //"Fax: "

oPrn:Say(nLinini+650,nColIni+0200,STR0032                               ,oFnt12B) //"CGC/CPF"
oPrn:Say(nLinini+650,nColIni+0500,": "+SA1->A1_CGC                      ,oFnt12B)
oPrn:Say(nLinini+650,nColIni+1400,STR0033                               ,oFnt12B) //"Inscr.Estadual"
oPrn:Say(nLinini+650,nColIni+1600,": "+SA1->A1_INSCR                    ,oFnt12B)

oPrn:Say(nLinini+700,nColIni+0200,STR0034                               ,oFnt12B) //"Endereco Cobranca"
oPrn:Say(nLinini+700,nColIni+0500,": "+SA1->A1_ENDCOB                   ,oFnt12B)

oPrn:Say(nLinini+750,nColIni+0200,STR0027                               ,oFnt12B) //"Cep"
oPrn:Say(nLinini+750,nColIni+0500,": "+SA1->A1_CEPC                     ,oFnt12B)
oPrn:Say(nLinini+750,nColIni+1400,STR0028                               ,oFnt12B) //"Cidade/Estado"
oPrn:Say(nLinini+750,nColIni+1600,": "+SA1->(Trim(A1_MUNC)+"/"+A1_ESTC) ,oFnt12B)

oPrn:Say(nLinini+850,nColIni+0100,STR0035,oFnt12B) //"Nessa data, por vossa solicitacao, prestamos os servicos relatorios aos itens abaixo"

oPrn:Say(nLinini+900,nColIni+0200,STR0036                               ,oFnt12) //"Emissao"  
oPrn:Say(nLinini+900,nColIni+0500,STR0037                               ,oFnt12) //"Descricao"
oPrn:Say(nLinini+900,nColIni+2000,STR0038                               ,oFnt12) //"Total"    

cMemo := G8G->G8G_OBSERV
nMemCount := MlCount( cMemo , 100 )
aMemo := {}
For nI := 1 To nMemCount
    AAdd( aMemo , AllTrim(MemoLine( cMemo, 100, nI )) )
Next nI
oPrn:Say(nLinini+1000,nColIni+0200,DToC(G8G->G8G_EMISSA)                ,oFnt12B)
oPrn:Say(nLinini+1000,nColIni+0500,aMemo[1]                             ,oFnt12B)
oPrn:Say(nLinini+1000,nColIni+2000,Transform(G8G->G8G_VALOR,"@E 999,999,999.99"),oFnt12B)

nLin := 1000
For nI:= 2 To Len(aMemo)
    nLin += 50
    oPrn:Say(nLinIni+nLin,nColIni+0500,aMemo[nI]                             ,oFnt12B)
Next nI

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//?Rodape do relatorio               ?
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
oPrn:Box(nLinFim-300,nColIni+0100,nLinFim-100,nColFim-100)

oPrn:Say(nLinFim-250,nColIni+0200,STR0039,oFnt12B) //"recebemos e aceitamos os itens nas condicoes expostas"

oPrn:Say(nLinFim-175,nColIni+0200,STR0040+" ......./...../......"  ,oFnt12B) // "Aceite em:"
oPrn:Say(nLinFim-200,nColIni+1400,"_______________________________",oFnt12B)
oPrn:Say(nLinFim-150,nColIni+1500,STR0041                          ,oFnt12B) //"Assinatura"


oPrn:Say(nLinFim-050,nColIni+1400,STR0042,oFnt12B) //

oPrn:EndPage()
oPrn:Preview()

Return
