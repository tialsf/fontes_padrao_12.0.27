#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'FILEIO.CH'
#INCLUDE 'DIRECTRY.CH'
#INCLUDE 'TURA048.CH'

#DEFINE CRLF Chr(13) + Chr(10)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TURA048

Importa��o do PNR Sabre

@sample	TURA048(cPathPNR, cFilePNR)
@param 		cPathPNR: String - caminho do(s) arquivo(s) a ser(em) integrado(s) via WIZARD
 			cFilePNR: String - nome do arquivo a ser integrado
@return 	Array[1]: L�gico - se verdadeiro indica que houve falha 
			Array[2]: L�gico - se verdadeiro indica que arquivo integrado 
@author 	Thiago Tavares
@since 		12/05/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TURA048(cPathPNR, cFilePNR)

Local aArquivos := {}  	
Local lRet      := .F. 
Local cDia      := StrZero(  Day(dDataBase), 2)
Local cMes      := StrZero(Month(dDataBase), 2)
Local cAno      := StrZero( Year(dDataBase), 4)
Local nX        := 0
Local nMove     := 0
Local cDataHora := '.' + cAno + cMes + cDia + '_' + StrTran(Time(), ':', '')
Local cDirLido  := 'Lidos\' //  + cAno + cMes + cDia + '\'
Local cDirErro  := 'Erros\'  + cAno + cMes + cDia + '\'

Private lMsgTipo3  := .F.
Private lMsgTipo1  := .F.
Private aLogG8VErr := {}
Private aCntIniArr := Array(0)
Private INCLUI     := .T.

Default cPathPNR   := ''
Default cFilePNR   := ''
 
// Criando o diretorio dos arquivos PNR caso n�o exista
If Empty(AllTrim(cPathPNR)) .Or. cPathPNR == Nil		
	cPathPNR := SuperGetMV('MV_DIRSABR', .F., '\sigatur\importacao\PNR\Sabre\', )
	TA048MKDIR(cPathPNR)
	TA048MKDIR(cPathPNR + 'Lidos\')
	TA048MKDIR(cPathPNR + 'Erros\')
	TA048MKDIR(cPathPNR + cDirLido)
	TA048MKDIR(cPathPNR + cDirErro)
EndIf

//Trata o Caminho informado em cPathPNR
cPathPNR := IIf(Right(AllTrim(StrTran(cPathPNR, '/', '\')), 1) <> '\', cPathPNR + '\', cPathPNR)

If Empty(AllTrim(cFilePNR))						
	aArquivos := Directory(cPathPNR + '*.pnr*')
Else
	aAdd(aArquivos, {cFilePNR, 100, Date(), Time(), ''})
EndIf

For nX := 1 To Len(aArquivos)
	If TURXVLDPNR(aArquivos[nX][1])
		nMove := FRenameEx(cPathPNR + aArquivos[nX][1], cPathPNR + cDirErro + aArquivos[nX][1] + '_REPROC_' + cDataHora)
		TA048G8VLOG(Nil , '1', '', '', '2', I18N(STR0028, {aArquivos[nX][1]}), '', {})  // "Arquivo #1 j� processado." 
	Else
		lRet       := .F.
		aLogG8VErr := {}
		If !(IsInCallStack('TURA070'))	
		   	FwMsgRun( , {|| lRet:= TA048PROC(cPathPNR, aArquivos[nX][1])}, , I18N(STR0021, {aArquivos[nX][1]}))	// Processando o arquivo #1...      	
		Else
			lRet := TA048PROC(cPathPNR, aArquivos[nX][1])
		EndIf
	
		// movendo o arquivo para a pasta LIDOS ou ERROS de acordo com o resultado LRet da integra��o
		If lRet
			nMove := FRenameEx(cPathPNR + aArquivos[nX][1], cPathPNR + cDirLido + aArquivos[nX][1] + cDataHora)
		Else 
			nMove := FRenameEx(cPathPNR + aArquivos[nX][1], cPathPNR + cDirErro + aArquivos[nX][1] + cDataHora) 
		EndIf
	EndIf
	
	If nMove == -1
		TA048G8VLOG(Nil , '1', cVersion, '', '2', I18N(STR0026 + CRLF + FError(), {aArquivos[nX][1]}), '', {})  // 'Ocorreu um erro ao tentar mover o arquivo: #1'
	EndIf

	TA048GRVG8V(cPathPNR, aArquivos[nX][1])
Next nX

Return IIf(IsInCallStack('TURA070'), {lMsgTipo3, lMsgTipo1}, Nil)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048MKDIR

Fun��o que cria o diret�rio que ser� o reposit�rio dos arquivos PNR

@sample	TA048MKDIR(cCaminho)
@param		cCaminho: String - caminho do diret�rio a ser criado 
@return	 
@author    Marcelo Cardoso Barbosa
@since     02/09/2015
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048MKDIR(cCaminho)

Local aCaminho := ''
Local cCriaDir := ''
Local cNivel   := ''
Local nNivel   := 0

Default cCaminho := '' 

If cCaminho != ''
	aCaminho := StrTokArr2(StrTran(cCaminho, '/', '\'), '\', .F.)
	For nNivel := 1 To Len(aCaminho)
		cNivel := Lower(AllTrim(StrTran(aCaminho[nNivel], '\', '')))
		If !Empty(cNivel)
			cCriaDir += '\' + cNivel
			MakeDir(cCriaDir)
		EndIf
	Next nNivel
EndIf	

Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048PROC

Fun��o que processa os arquivos PNR

@sample	TA048PROC(cPathPNR, cFilePNR)
@param 		cPathPNR: String - caminho do arquivo 
			cFilePNR: String - nome do arquivo
@return 	lRet: L�gico - se verdadeiro indica que o arquivo foi integrado 
@author 	Thiago Tavares
@since 		13/05/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TA048PROC(cPathPNR, cFilePNR)

Local oModel     := Nil
Local oMdlG3Q    := Nil
Local oMdlG3S    := Nil
Local oMdlG3T    := Nil
Local oMdlG46    := Nil
Local oMdlG4D    := Nil
Local lRet       := .T.
Local lAltFil    := .F.
Local lRetM9     := .T. 
Local lRetFOP    := .T.
Local nX, nY     := 0
Local nW, nZ     := 0 
Local nQtdeDR    := 0
Local nQtdSeg    := 0
Local nQtdePax   := 0
Local nG3Q       := 1
Local nG3T       := 1
Local nG46       := 1
Local nPosMPD    := 0
Local nPosMCO    := 0
Local nInicio    := 0
Local nTamanh    := 0 
Local nPosica    := 0
Local nValorDU   := 0  
Local nLinAux    := 0
Local cOperac    := ''
Local cVersao    := ''
Local cM9Tags    := ''
Local cFOPs      := ''
Local cG3PNumID  := ''
Local cMsgErro   := ''
Local cBloco     := ''
local cSepara    := ''
Local cValor     := ''
Local cFilBkp    := cFilAnt
Local aM9Tags    := {}
Local aG3QDoc    := {}
Local aIncrement := {}
Local aRepeat    := {}
Local aErro      := {}
Local aListProd  := {}	// {C�digo Produto PNR (AIR, HHT, HHL, HTL, CAR), C�digo do Segmento, Nome da Tabela do Segmento, Controle FOP, {n�mero documento, {Lista de Passageiros}}}
Local aPNR       := TA048LEPNR(cPathPNR, cFilePNR, @cVersao, @aListProd)
Local aG8U       := TURXG8U('1', cVersao)	// 1-Sabre 2-Amadeus
local aVlBase    := TURXG8UDET(aG8U, '1', 'G44_APLICA', " == 'AIR'")
Local aOperac    := TURXG8UDET(aG8U, '2', 'CIU0TYP' , )

Private cArquivo := cFilePNR
Private nAtuPass := 1
Private cPNRProd := ''
Private cDocOri  := ''
Private aListPax := {}	// {C�digo do Bloco, Conte�do da Linha}	
Private aTratam  := {}

If Len(aOperac) > 0
	cBloco  := aOperac[aScan(aOperac, {|x| x[1] == 'G8U_BLOCO' })][2]
	nInicio := aOperac[aScan(aOperac, {|x| x[1] == 'G8U_INICIO'})][2]
	nTamanh := aOperac[aScan(aOperac, {|x| x[1] == 'G8U_TAMANH'})][2]
	cOperac := TA048GET(aPNR[aScan(aPNR, {|x| x[1] == 'M0'})], cBloco, '', nInicio, nTamanh, '', .T.) 
EndIf

// validando a opera��o do arquivo PNR (5-Cancelamento / 1-Emiss�o)
If cOperac == '5'
	TA048CANCRV(aPNR, aG8U, cVersao) 
Else
	If Len(aG8U) == 0
		TA048G8VLOG(Nil , '1', cVersao, '', '2', I18N(STR0023 + CRLF + STR0024, {cVersao, cFilePNR}), '', {})	// 'N�o existe cadastro de Layout GDS para a vers�o #1 informada no arquivo #2.' 'O arquivo #1 foi movido para a pasta de erros.' 
	Else
		TA048ARRCTRL(aG8U, @aM9Tags, @aIncrement, @aRepeat, @aTratam)		// atualizando os arrays de controle
	
		// validando se TAGs obrigat�rias foram informadas
		TA048VLDM9(aPNR, @aM9Tags)															
		For nX := 1 To Len(aM9Tags)
			If !aM9Tags[nX][2] 
				cM9Tags += aM9Tags[nX][1] + CRLF
				lRetM9 := .F.
			EndIf
		Next nX
		
		// se TAGs M9 foram informadas, validar se foram informadas as FOP's dos produtos que ser�o integrados
		If lRetM9
			TA048VLDFOP(aPNR, @aListProd)
			For nX := 1 To Len(aListProd)
				If !aListProd[nX][4]
					cFOPs += aListProd[nX][1] + CRLF
					lRetFOP := .F.
				EndIf
			Next nX
		EndIf
		
		If !lRetM9 .Or. !lRetFOP
			If !lRetM9
				TA048G8VLOG(Nil , '1', cVersao, '', '2', STR0022 + CRLF + cM9Tags, '', {})	// 'TAGs obrigat�rias do bloco M9 n�o informadas. As TAGs faltantes s�o: '
			ElseIf !lRetFOP
				TA048G8VLOG(Nil , '1', cVersao, '', '2', STR0016 + CRLF + cFOPs, '', {})	// 'Forma de Pagamento n�o informada para os produtos abaixo: '
			EndIf
			lRet := .F.
		Else
			If Len(aListProd) > 0
				// alterando o array da lista de produtos acrescentando a lista de passageiros com suas respectivas linhas do arquivo PNR
				TA048LSTIVS(aPNR, aG8U, @aListProd)		 	
				
				// processar a leitura das vari�veis declaradas atrav�s do Layout GDS 
				If (lRet := TA048PRCPNR(aPNR, aG8U, '2', '', Nil, aRepeat, aIncrement))
					// validando se a filial do posto de emissao foi encontrada
					If Empty(AllTrim(cFilialRV))
						TA048G8VLOG(Nil , '1', cVersao, '', '2', STR0031, '', {})		// "Filial do Posto de Atendimento de Emiss�o n�o encontrado."	
						lRet := .F.
					Else				
						// validando se o cliente possui cadastro de complemento
						If Len(Tur18RtCmp(cCodCli, cCodLoja, Val(Posicione('G6K', 2, xFilial('G6K') + '000001' + cSegNeg, 'G6K_CDDE')))) == 0
							TA048G8VLOG(Nil , '1', cVersao, '', '2', I18N(STR0030, {cCodCli}), '', {})	// "Cliente (#1) sem nenhum Complemento cadastrado."
							lRet := .F.
						Else 
							// caso a filial do RV seja diferente da logada, alterar para a filial do RV
							If cFilBkp <> cFilialRV			 
								cFilAnt := cFilialRV
								lAltFil := .T.
							EndIf
				
							oModel := FWLoadModel('TURA034')					
							oModel:SetOperation(MODEL_OPERATION_INSERT)
							TA048MDLDEF(@oModel)
							oModel:Activate()
			
							oMdlG3Q := oModel:GetModel('G3Q_ITENS')
							oMdlG3S := oModel:GetModel('G3S_ITENS')
							oMdlG3T := oModel:GetModel('G3T_ITENS')
							oMdlG46 := oModel:GetModel('G46_ITENS')
							oMdlG4D := oModel:GetModel('G4D_ITENS')
			
							// processar a leitura dos campos da tabela G3P - cabe�alho RV
							If (lRet := TA048PRCPNR(aPNR, aG8U, '1', 'G3P', @oModel, {}, aIncrement))
								For nX := 1 To Len(aListProd)
									// processando o IVs
									For nY := 1 To Len(aListProd[nX][5])
										If lRet
											nAtuPass   := 1
											cPNRProd   := aListProd[nX][1]
											aListPax   := aListProd[nX][5][nY][2]
											nQtdePax   := TA048QTBLCS(aPNR, 'M1')
											
											// Apura as repeti��es de Tag ou Bloco
											If Len(aRepeat) > 0
												For nW := 1 To Len(aRepeat)		
													aRepeat[nW][4] := 0 			
													aRepeat[nW][6] := 0 			
												Next nW
													
												// Varre o controle de repeti��es
												For nW := 1 To Len(aRepeat)
													If aRepeat[nW][2] == 'T'
														If (nPosM9 := aScan(aPNR, {|x| x[1] == 'M9'})) > 0 
															For nZ := nPosM9 To Len(aPNR)				 
																If SubStr(aPNR[nZ][3], 5, Len(aRepeat[nW][3])) == aRepeat[nW][3]
																	aRepeat[nW][4]++
																EndIf 							
															Next nZ
														EndIf
													EndIf 	
												Next nW
											EndIf
										
											// verificando se novo IV e adicionando nova linha no GRID
											If nX > 1 .Or. nY > 1
												nLinAux := oMdlG3Q:AddLine()
												If nLinAux <= nG3Q
													aErro := oModel:GetErrorMessage()
													lRet  := .F.
													Exit
												EndIf
											EndIf
			
											// processar a leitura dos campos da tabela G3Q - Item de Venda
											If !(lRet := TA048PRCPNR(aListPax[1], aG8U, '1', 'G3Q', @oModel, aRepeat, aIncrement)); Exit; EndIf
											
											// verificando se � REEMISSAO e guardando o bilhete original para a linha do EMD
											If oMdlG3Q:GetValue('G3Q_OPERAC') == '3'
												cDocOri := AllTrim(oMdlG3Q:GetValue('G3Q_DOCORI'))
											EndIf
			
											// processar a leitura dos campos da tabela G3R - Documento de Reserva
											If !(lRet := TA048PRCPNR(aListPax[1], aG8U, '1', 'G3R', @oModel, aRepeat, aIncrement)); Exit; EndIf
	
											// preenchendo os dados do fornecedor de reporte
											If !(lRet := TA048FNRPT(@oModel))
												TA048G8VLOG(Nil , '1', cVersao, '', '2', STR0032, '', {})		// "N�o foi poss�vel determinar o Fornecedor de Reporte."	 
												Exit
											EndIf
			
											// avaliar o preenchimento do campo G3P_DESTIN de acordo com o fornecedor
											TA048GETDST(, , @oModel)
			
											For nW := 1 To Len(aListPax)
												If lRet
	
													// caso seja apenas um passageiro, n�o adicionar novo IV 
													If nQtdePax > 1 .And. nW > 1 
														nLinAux := oMdlG3S:AddLine()
														If nLinAux <= nAtuPass
															aErro := oModel:GetErrorMessage()
															lRet  := .F.
															Exit
														EndIf
														nAtuPass++
													EndIf
													
													// processar a leitura dos campos da tabela G3S - Passageiros
													If !(lRet := TA048PRCPNR(aListPax[nW], aG8U, '1', 'G3S', @oModel, aRepeat, aIncrement)); Exit; EndIf
														
													If cPNRProd != 'AIR' 
														// processar a leitura dos campos da tabela referente ao respectivo segmento
														If !(lRet := TA048PRCPNR(aListPax[nW], aG8U, '1', aListProd[nX][3], @oModel, aRepeat, aIncrement)); Exit; EndIf  
				
														// processar a leitura dos campos da tabela G44 - Tarifas  
														If !(lRet := TA048PRCPNR(aListPax[nW], aG8U, '1', 'G44', @oModel, aRepeat, aIncrement)); Exit; EndIf

														// processar a leitura dos campos da tabela G47 - Extras
														If cPNRPRod == 'CAR'  
															If !(lRet := TA048PRCPNR(aListPax[nW], aG8U, '1', 'G47', @oModel, aRepeat, aIncrement)); Exit; EndIf
														EndIf
													Else
														// validando se o bilhete j� foi integrado
														aG3QDoc := TA048G3QDOC(aListPax[nW], aG8U)
													 	If !(lRet := aG3QDoc[1]) 
															TA048G8VLOG(Nil , '1', cVersao, '', '2', I18N(STR0029, {aG3QDoc[2], aG3QDoc[3]}), '', {})	// "N�mero de bilhete inv�lido (#). Ele j� foi utilizado em outro Item de Venda (#2)."
															Exit
														EndIf 

														// caso seja apenas um passageiro, processar segmentos e cart�o apenas uma vez
														If (nW == 1) .Or. (nQtdePax > 1 .And. nW > 1)
															// processar a leitura dos campos da tabela G3T - Segmento AEREO
															nQtdSeg := TA048QTSEG(aListPax[nW], aG8U, cPNRProd, '')[1]
															nG3T    := 1
															For nZ := 1 To nQtdSeg
																If nZ > 1 .Or. nW > 1 
																	nLinAux := oMdlG3T:AddLine()
																	If nLinAux <= nG3T
																		aErro := oModel:GetErrorMessage()
																		lREt  := .F. 
																		Exit
																	EndIf
																EndIf
																				
																If !(lRet := TA048PRCPNR(aListPax[nW], aG8U, '1', 'G3T', @oModel, aRepeat, aIncrement)); Exit; EndIf 
				
																// altera o bloco/tag para n�o ser reprocessado
																TA048ALTBLC(@aListPax[nW], aG8U, 'M3', cPNRProd)		
																TA048ALTBLC(@aListPax[nW], aG8U, 'M4', 'M4' + StrZero(nZ, 2))		
																nG3T := oMdlG3T:Length()
															Next n
															
															If lRet
																If nQtdePax > 1 .And. nW > 1 
																	nLinAux := oMdlG4D:AddLine()
																	If nLinAux <= nAtuPass
																		aErro := oModel:GetErrorMessage()
																		lRet := .F. 
																		Exit
																	EndIf
																EndIf
																
																If aScan(aListPax[nW], {|x| x[1] == 'M5' .And. '/EMD' $ x[3]}) == 0 .And. ;   	
													   			   aScan(aListPax[nW], {|x| x[1] == 'M5' .And. '/F-@' $ x[3]}) == 0			
																
																	// processar a leitura dos campos da tabela G4D - Dados do Cart�o
																	If !(lRet := TA048PRCPNR(aListPax[nW], aG8U, '1', 'G4D', @oModel, aRepeat, aIncrement))
																		Exit
																	Else
																		If !(aVldG4D := TA048VLDG4D(oModel))[1]
																			TA048G8VLOG(Nil, '1', cVersao, '', '2', STR0027, '', {})		// "Cart�o de Ag�ncia informado no arquivo n�o est� cadastrado."
																			lRet := .F.
																			Exit
																		Else
																			// preenchendo os dados com as informa��es do cart�o encontrado
																			If !Empty(aVldG4D[7]); oModel:GetModel('G4D_ITENS'):LoadValue('G4D_PROPRI', aVldG4D[7]); EndIf		
																			If !Empty(aVldG4D[2]); oModel:GetModel('G4D_ITENS'):LoadValue('G4D_CODCAR', aVldG4D[2]); EndIf		
																			If !Empty(aVldG4D[3]); oModel:GetModel('G4D_ITENS'):LoadValue('G4D_CODBAN', aVldG4D[3]); EndIf		
																			If !Empty(aVldG4D[4]); oModel:GetModel('G4D_ITENS'):LoadValue('G4D_TITULA', aVldG4D[4]); EndIf		
																			If !Empty(aVldG4D[5]); oModel:GetModel('G4D_ITENS'):LoadValue('G4D_MESVAL', aVldG4D[5]); EndIf		
																			If !Empty(aVldG4D[6]); oModel:GetModel('G4D_ITENS'):LoadValue('G4D_ANOVAL', aVldG4D[6]); EndIf
																		EndIf
																	EndIf
																EndIf
															EndIf
														EndIf
														 
														// validando se � EMD ou MCO, caso negativo processar TARIFA e TAXA normalmente
														If lRet .And. ;  
														   aScan(aListPax[nW], {|x| x[1] == 'M5' .And. '/EMD' $ x[3]}) == 0 .And. ;   	
											   			   aScan(aListPax[nW], {|x| x[1] == 'M5' .And. '/F-@' $ x[3]}) == 0			
														
															// processar a leitura dos campos da tabela G44 - Tarifas  
															If !(lRet := TA048PRCPNR(aListPax[nW], aG8U, '1', 'G44', @oModel, aRepeat, aIncrement)); Exit; EndIf
					
															// processar a leitura dos campos da tabela G46 - Taxas
															If !(lRet := TA048PRCPNR(aListPax[nW], aG8U, '1', 'G46', @oModel, aRepeat, aIncrement)); Exit; EndIf
															
															// verificando se foi informado a TAXA DU e, caso afirmativo, � preciso subtrair do valor da taxa gravado
															If cIDTax1 == 'DU' .Or. cIDTax2 == 'DU' .Or. cIDTax3 == 'DU'
																If cIDTax1 == 'DU'
																	nValorDU := nVlrTax1 
																ElseIf	cIDTax2 == 'DU'
																	nValorDU := nVlrTax2 
																ElseIf cIDTax3 == 'DU'
																	nValorDU := nVlrTax3 
																EndIf
	
																oMdlG46:SetValue('G46_VLBASE', oMdlG46:GetValue('G46_VLBASE') - nValorDU)
															EndIf 
	
														// se EMD ou MCO o valor entra como TAXA
														ElseIf lRet .And.	; 
														      ((nPosMPD := aScan(aListPax[nW], {|x| x[1] == 'M5' .And. '/EMD' $ x[3]})) > 0 .Or. ;   	
													   	       (nPosMCO := aScan(aListPax[nW], {|x| x[1] == 'M5' .And. '/MCO' $ x[3]})) > 0)			
														
															If Len(aVlBase) > 0
																// pegando o valor da multa/credito
																cBloco  := aVlBase[aScan(aVlBase, {|x| x[1] == 'G8U_BLOCO' })][2]
																nInicio := aVlBase[aScan(aVlBase, {|x| x[1] == 'G8U_INICIO'})][2]
																nTamanh := aVlBase[aScan(aVlBase, {|x| x[1] == 'G8U_TAMANH'})][2]
																cSepara := aVlBase[aScan(aVlBase, {|x| x[1] == 'G8U_SEPARA'})][2]
																nPosica := aVlBase[aScan(aVlBase, {|x| x[1] == 'G8U_POSICA'})][2]
																cValor  := TA048GET(aListPax[nW][IIf(nPosMPD > 0, nPosMPD, nPosMCO)], cBloco, '', nInicio, nTamanh, '', .T.)
																If !Empty(cValor) .And. At(cSepara, cValor) > 0
																	cValor  := StrTokArr2(cValor, cSepara, .F.)[nPosica]
																
																	//oMdlG46:SetValue('G46_SEQTAX', StrZero(Val(oMdlG46:GetValue('G46_SEQTAX', nG46 - 1)) + 1, TamSx3('G46_SEQTAX')[1]))
																	oMdlG46:SetValue('G46_EMISS' , dDtEmiss)
																	oMdlG46:SetValue('G46_CODTX' , Posicione('G3A', 1, xFilial('G3A') + PadR(IIf(nPosMPD > 0, 'MPD', 'MCO'), TAMSX3('G3A_CODIGO')[1], ' '), 'G3A_CODIGO'))
																	oMdlG46:SetValue('G46_MOEDFO', Posicione('G5T', 4, xFilial('G5T') + 'BRL', 'G5T_CODIGO'))
																	oMdlG46:SetValue('G46_TXCAMB', 1)
																	oMdlG46:SetValue('G46_VLBASE', Val(cValor))
																EndIf
															EndIf									    
														EndIf
													EndIf
												EndIf
											Next nW
			
											If lRet
												// processar a leitura dos campos da tabela G4B - Entidades Adicionais  
												TA048PG4B(aListPax[1], aG8U, @oModel, oMdlG3Q:Length())
			
												// processar a leitura dos campos da tabela G4A - Rateio
												If !(lRet := TA048PRCPNR(aListPax[1], aG8U, '1', 'G4A', @oModel, aRepeat, aIncrement)); Exit; EndIf
											EndIf
			
											nG3Q := oMdlG3Q:Length()
										EndIf
									Next nY
								Next nX
								
								If lRet .And. Len(aLogG8VErr) == 0
									bBlock := ErrorBlock({|e| TA048G8VLOG(e, '1', cVersao, '', '3', '', '',  oModel:GetErrorMessage())})
									BEGIN SEQUENCE
										TURA34Next(.F.)						// Aplicacao de Acordo
										If (lRet := oModel:VldData())		// Se os dados foram validados aplica acordos e grava
											If oModel:CommitData()			
												cG3PNumID := ' ' + TransForm(G3P->G3P_NUMID, PesqPict('G3P', 'G3P_NUMID'))
											EndIf
										Else
											aErro := oModel:GetErrorMessage() 
										EndIf
									RECOVER
										lRet  := .F.
									END SEQUENCE
			 						ErrorBlock(bBlock)
								EndIf
							EndIf
						EndIf
					EndIf
				EndIf
			EndIf
		EndIf
	EndIf

	If !lRet .Or. Len(aLogG8VErr) > 0
		If !lRet .And. Len(aLogG8VErr) == 0
			TA048G8VLOG(Nil, '1', cVersao, '', '3', '', '', aErro)
		EndIf
	Else
		TA048G8VLOG(Nil, '1', cVersao, '', '1', I18N(STR0002, {cG3PNumID, cFilAnt}), '', {})  // 'ARQUIVO IMPORTADO COM SUCESSO'
	EndIf
	
	If oModel <> Nil .And. oModel:IsActive(); oModel:DeActivate(); EndIf
	
	If lAltFil; cFilAnt := cFilBkp; EndIf

EndIf	 

Return lRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048LEPNR

Fun��o respons�vel por ler e retornar um array com as linhas do arquivo     

@sample	TA048LEPNR(cPathPNR, cFilePNR)
@param 		cPathPNR: String - caminho do arquivo 
			cFilePNR: String - nome do arquivo
			cVersao: String - versao do arquivo que vai ser integrado
			aListProd: Array - lista com os produtos encontrados no arquivo
@return 	aPNR: Array - lista com as linhas obtidas do arquivo PNR  
@author    Marcelo Cardoso Barbosa
@since     02/09/2015
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048LEPNR(cPathPNR, cFilePNR, cVersao, aListProd)

Local aArea      := GetArea()
Local aPNR       := {}
Local aNewPNR    := {} 
Local aSplitPNR  := {}		
Local aMCod      := {{'M0'}, {'M1'}, {'M2'}, {'M3'}, {'M4'}, {'M5'}, {'M6'}, {'M7'}, {'M8'}, {'M9'}}
Local aVldProds  := {{'AIR', 1, 'G3T'}, {'HHT', 2, 'G3U'}, {'HTL', 2, 'G3U'}, {'HHL', 2, 'G3U'}, {'CAR', 3, 'G3V'}}
Local cLinha     := '' 
Local cMCod      := 'M0'
Local cMSeq      := '00'
Local cLevaUlt   := ''
Local cProduto   := ''
Local nVarre     := 0
Local nLenSplPNR := 0
Local nForPNR    := 0
Local nNew       := 0
Local nPosProd   := 0
Local lLevaUlt   := .F.
	
// L� o arquivo e o adiciona a um Array. 
If File(cPathPNR + cFilePNR)
	FT_FUse(cPathPNR + cFilePNR)
	While !FT_FEOF()
		cLinha := Upper(FT_FReadLn())
		aAdd(aPNR, cLinha)
		FT_FSkip()
	EndDo
	FT_FUse()
EndIf
			
//Varre o array original
For nVarre := 1 To Len(aPNR)
	If !(Chr(13) $ aPNR[nVarre])
		aAdd(aNewPNR, aPNR[nVarre])
	Else
		aSplitPNR  := {}												//Inicializa um array tempor�rio
		aSplitPNR  := StrTokArr2(aPNR[nVarre], Chr(13), .T.)		//Divide cada posi��o inicial do array original em um novo array em fun��o do separador CR - C�digo ASCII 13
		nLenSplPNR := Len(aSplitPNR)								//Determina o tamanho total do array tempor�rio
		lLevaUlt   := Right(aPNR[nVarre], 1) <> Chr(13)			//Caso o �ltimo String n�o seja o separador CR - C�digo ASCII, define que o �ltimo trecho ser� adicionado ao �nicio do pr�ximo
		nForPNR    := nLenSplPNR - IIf(lLevaUlt, 1, 0)			//Caso seja o caso, ir� varrer o array tempor�rio at� o pen�ltimo item apenas

		If nForPNR == 0 .And. nLenSplPNR == 1
			nForPNR := 1
		EndIf
		
		If nForPNR > 0
			For nNew := 1 To nForPNR
				aAdd(aNewPNR,  IIf(nNew == 1, cLevaUlt, '') + aSplitPNR[nNew] )		// Caso seja o caso, adiciona o �ltimo peda�o anterior ao in�cio deste novo trecho
			Next nNew
		EndIf
		
		If lLevaUlt			//Se for o caso, armazena o �ltimo trecho para ser adicionado ao in�cio do pr�ximo trecho
			cLevaUlt := aSplitPNR[nLenSplPNR]
		Else
			cLevaUlt := ''
		EndIf
	EndIf
Next nVarre
	
//Reconstr�i o array original, com base no novo array
aPNR      := {}
aListProd := {}
For nVarre := 1 To Len(aNewPNR)
	If (nPosMCod := aScan(aMCod, {|x| x[1] == SubStr(aNewPNR[nVarre], 01, 02)})) > 0
		cMCod := aMCod[nPosMCod][1]
		cMSeq := SubStr(aNewPNR[nVarre], 03, 02)
	EndIf

	If nVarre == 1
		cVersao := SubStr(aNewPNR[nVarre], 15, 2)
		cMCod   := 'M0'
		cMSeq   := '00'
	EndIf

	aAdd(aPNR, {cMCod, cMSeq, aNewPNR[nVarre]})

	//Armazena apenas os Produtos A�reo, Hotel ou Carro 
	If cMCod == 'M3' .And. nPosMCod > 0
		cProduto := SubStr(aNewPNR[nVarre], 15, 3)
		If (nPosProd := aScan(aVldProds, {|x| x[1] == cProduto})) > 0
			If aScan(aListProd, {|x| x[1] == cProduto}) == 0
				aAdd(aListProd, {cProduto, aVldProds[nPosProd][2], aVldProds[nPosProd][3], .T., {}})
			EndIf
		EndIf
	EndIf
Next nVarre

RestArea(aArea)

Return aPNR

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048G8VLOG

Fun��o respons�vel por preencher o array que ser� utilizado para gravar as 
informa��es para o monitor de integra��o PNR

@sample	TA048G8VLOG(e, cG8UGDS, cG8UVERSAO, cG8USEQUEN, cTipoG8V, cMsg, cExpressao, aErro)
@param		cGDS: String - nome do GDS de integra��o
			cVerGDS: String - Vers�o GDS  
			cSequen: String - Sequencia do Layout GDS que gerou o erro
			cTipoG8V: String - Tipo da Ocorr�ncia (3-erro / 1-sucesso)
			cMsg: String - mensagem reportada
			cExpressao: String - express�o do Layout GDS onde apresentou o erro
			aErro: Array - lista de retornada atrav�s do m�todo oModel:GetErrorMessage()
@return	
@author    Marcelo Cardoso Barbosa
@since     25/11/2015
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048G8VLOG(e, cG8UGDS, cG8UVERSAO, cG8USEQUEN, cTipoG8V, cMsg, cExpressao, aErro) 

Local nStopError := 1
Local cErroG8V   := cMsg
Local lAlias     := .F.

If Len(aErro) > 0 .And. ValType(aErro) == 'A' 
	cErroG8V := STR0003 + CRLF  											// "ERRO AO ATRIBUIR CONTEUDO AO REGISTRO DE VENDA:"
	cErroG8V += STR0004 + ' [' + AllToChar( aErro[1] ) + ']' + CRLF 	// "Id do formul�rio de origem:"
	cErroG8V += STR0005 + ' [' + AllToChar( aErro[2] ) + ']' + CRLF 	// "Id do campo de origem: "
	cErroG8V += STR0006 + ' [' + AllToChar( aErro[3] ) + ']' + CRLF 	// "Id do formul�rio de erro: "
	cErroG8V += STR0007 + ' [' + AllToChar( aErro[4] ) + ']' + CRLF 	// "Id do campo de erro: "
	cErroG8V += STR0008 + ' [' + AllToChar( aErro[5] ) + ']' + CRLF 	// "Id do erro: "
	cErroG8V += STR0009 + ' [' + AllToChar( aErro[6] ) + ']' + CRLF  	// "Mensagem do erro: "
	cErroG8V += STR0010 + ' [' + AllToChar( aErro[7] ) + ']' + CRLF 	// "Mensagem da solu��o: "
	cErroG8V += STR0011 + ' [' + AllToChar( aErro[8] ) + ']' + CRLF 	// "Valor atribu�do: "
	cErroG8V += STR0012 + ' [' + AllToChar( aErro[9] ) + ']' + CRLF	// "Valor anterior: "
EndIf

If cTipoG8V $ '2|3'
	If !Empty(AllTrim(cExpressao))
		cErroG8V += STR0014 + CRLF  //'OCORREU UM ERRO '
		cErroG8V += STR0015 + CRLF  //'APURACAO DA EXPRESSAO: '
		cErroG8V += UPPER(cExpressao) + CRLF
	EndIf
	If ValType(e) != 'U'
		cErroG8V += UPPER(e:Description) + CRLF 
		cErroG8V += UPPER(e:ErrorStack)
	EndIf	 
EndIf

aAdd(aLogG8VErr, {xFilial('G8V'), cG8UGDS, cG8UVERSAO, cG8USEQUEN, cTipoG8V, cErroG8V, __cUserId, Date(), Time()})

If ValType(e) != 'U'
	Break
EndIf

Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048ARRCTRL

Fun��o que monta os arrays de controle para obten��o dos dados      

@sample	TA048ARRCTRL(aG8U, @aM9Tags, @aIncrement, @aRepeat)
@param		aG8U: Array - lista de registros do layout GDS 
			aM9Tags: Array - lista de TAGs obrigat�rias do bloco M9
			aIncrement: Array - lista de de campos incrementais
			aRepeat: Array - Lista de TAGs do bloco M9 que s�o lidas repetidas vezes
			aTratam: Array - lista de tratamentos dos passageiros 
@return 	
@author 	Thiago Tavares
@since 		13/05/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048ARRCTRL(aG8U, aM9Tags, aIncrement, aRepeat, aTratam)

Local aArea   := GetArea()
Local nX      := 0
Local cSequen := '' 
Local cVariav := ''
Local cTipo   := ''
Local cCampo  := ''
Local cTabela := ''
Local cContex := ''
Local cObriga := ''
Local cTag    := ''
Local cIncrem := ''
Local cTagMul := ''
Local aVariav := {}

For nX := 1 To Len(aG8U)
	cSequen := aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_SEQUEN')})][2]
	cVariav := aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_VARIAV')})][2]
	cTipo   := aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_TIPO'  )})][2]
	cCampo  := aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_CAMPO' )})][2]
	cTabela := aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_TABELA')})][2]
	cContex := aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_CONTEX')})][2]
	cObriga := aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_OBRIGA')})][2]
	cTag    := aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_TAG'   )})][2]
	cIncrem := aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_INCREM')})][2]
	cTagMul := aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_TAGMUL')})][2]

	// declarando os registros que s�o vari�veis 
	If !Empty(AllTrim(cVariav)) 						
		If aScan(aVariav, {|x| x == AllTrim(cVariav)}) == 0
			TA048VARS(AllTrim(cVariav), cTipo)
			aAdd(aVariav, AllTrim(cVariav))
		EndIf
	EndIf

	// verifica se � TAG obrigatoria do bloco M9
	If cObriga == '1'
		If aScan(aM9Tags, {|x| x[1] == AllTrim(cTag)}) == 0 
			aAdd(aM9Tags, {AllTrim(cTag), .T.})
		EndIf
	EndIf

	// Cataloga os campos que s�o preenchidos incrementalmente
	If cIncrem == '1'
		If aScan(aIncrement, AllTrim(cCampo)) == 0
			aAdd(aIncrement, {AllTrim(cCampo), AllTrim(cSequen)})
		EndIf
	EndIf

	// Cataloga o controle de repeti��o de campos
	If cTagMul == '1'
		If aScan(aRepeat, {|x| AllTrim(x[1]) == AllTrim(cTabela)}) == 0 .And. !Empty(AllTrim(cTag))
			aAdd(aRepeat, {AllTrim(cTabela), 'T', AllTrim(cTag), 0, nX, 0})		
		EndIf
	EndIf
Next nX

//Apura os tratamentos cadastrados no sistema
DBSelectArea('G6K')
G6K->(DBSetOrder(1)) 		// G6K_FILIAL+G6K_ATRIB+G6K_CDDE+G6K_CDPARA
If G6K->(DbSeek(xFilial('G6K') + '000005'))
	While G6K->(!EOF()) .and. G6K->G6K_FILIAL + G6K->G6K_ATRIB == xFilial('G6K') + '000005'
		aAdd(aTratam, AllTrim(G6K->G6K_CDPARA))
		G6K->(DBSkip())
	End
EndIf
G6K->(DbCloseArea())

RestArea(aArea)

Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048VARS

Fun��o que declara as vari�veis do Layout GDS e as inicializa

@sample	TA048VARS(uVar, cTipo)
@param		uVar: Undefined - vari�vel a ser declarada 
			cTipo: String - tipo da vari�vel que ser� declarada
@return	 
@author    Marcelo Cardoso Barbosa
@since     02/09/2015
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TA048VARS(uVar, cTipo)

Do Case
	Case cTipo $ 'C1'
		&('_SetOwnerPrvt("' + uVar + '", "")')
		&('_SetNamedPrvt("' + uVar + '", "", "TA048PROC")')

	Case cTipo $ 'N2'
		&('_SetOwnerPrvt("' + uVar + '", 0)')
		&('_SetNamedPrvt("' + uVar + '", 0, "TA048PROC")')

	Case cTipo $ 'D3'
		&('_SetOwnerPrvt("' + uVar + '", CToD(""))')  
		&('_SetNamedPrvt("' + uVar + '", CToD(""), "TA048PROC")')
		
	Case cTipo $ 'A4'
		&('_SetOwnerPrvt("' + uVar + '", Array(0))')
		&('_SetNamedPrvt("' + uVar + '", Array(0), "TA048PROC")')
EndCase
	
Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048VLDM9

Fun��o respons�vel por validar as tags obrigatorias do bloco M9 

@sample	TA048VLDM9(aPNR, @aM9Tags)
@param 		aPNR: Array - linhas arquivo PNR
			aM9Tags: Array - lista das TAGs do bloco M9 a serem validadas
@return 	
@author 	Thiago Tavares
@since 		12/05/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048VLDM9(aPNR, aM9Tags) 

Local lRet := .F.
Local nX   := 0
Local nM9  := 1

While nM9 <= Len(aM9Tags)
	lRet := .F.	
	For nX := aScan(aPNR, {|x| x[1] == 'M9'}) To Len(aPNR)
		If SubStr(aPNR[nX][3], 5, Len(aM9Tags[nM9][1])) == aM9Tags[nM9][1]
			lRet := .T.			
			Exit 		
		EndIf
	Next nX
	aM9Tags[nM9][2] := lRet
	nM9++
EndDo

Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048VLDFOP

Fun��o respons�vel por validar as FOP's dos produtos informados no PNR  

@sample	TA048VLDFOP(aPNR, @aM9Tags)
@param 		aPNR: Array - linhas arquivo PNR
			aListProd: Array - lista dos produtos informados no PNR
@return 	
@author 	Thiago Tavares
@since 		06/06/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048VLDFOP(aPNR, aListProd)

Local aArea  := GetArea()
Local lRet   := .F.
Local nX     := 1 
Local nY     := 0
Local cTag   := ''  

While nX <= Len(aListProd)
	lRet := .F.	
	DbSelectArea('G6K')
	G6K->(DbSetOrder(1))		// G6K_FILIAL+G6K_ATRIB+G6K_CDDE+G6K_CDPARA
	If (DbSeek(xFilial('G6K') + '000007' + aListProd[nX][1]))
		cTag := 'FOP.' + AllTrim(G6K->G6K_CDPARA) + '.'	
		For nY := aScan(aPNR, {|x| x[1] == 'M9'}) To Len(aPNR)
			If SubStr(aPNR[nY][3], 5, Len(cTag)) == AllTrim(cTag)
				lRet := .T.	
				Exit
			EndIf
		Next nY
	EndIf
	aListProd[nX][4] := lRet
	nX++
EndDo

G6K->(DbCloseArea())
RestArea(aArea)

Return 

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048LSTIVS

Fun��o respons�vel por gerar a lista de passageeiros a partir do arquivo PNR   

@sample	TA048LSTIVS(aPNR, aG8U, aListProd)
@param 		aPNR: Array - linhas arquivo PNR
			aG8U: Array - lista de registros do layout GDS
			aListProd: Array - lista dos produtos informados no PNR
@return 	
@author 	Thiago Tavares
@since 		06/06/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048LSTIVS(aPNR, aG8U, aListProd)

Local nX, nY    := 0
Local nM1       := 0
Local nM2       := 0
Local nPosM1    := 0 
Local nPosM2    := 0
Local nPosM3    := 0
Local nPosM5    := 0
Local nPosDoc   := 0
Local nBlocoPos := 0 
Local nProds    := 0
Local nIniProd  := 0
Local nInicio   := 0
Local nTamanh   := 0
Local aAmarraM1 := {'M3', 'M5', 'M7', 'M8', 'M9'}
Local aAmarraM2 := {'M4', 'M6'}
Local aAux      := {}
Local aPax      := {}
Local aListG3Q  := {}
Local cBloco    := ''
Local cBlocoSeq := ''
Local cM1Seq    := ''
Local cM3Seq    := ''
local cM5Seq    := ''
Local cG3QDoc   := '' 

For nProds := 1 To Len(aListProd)
	aListG3Q := {}
	nPosM1   := aScan(aPNR, {|x| x[1] == 'M1'})
 	While nPosM1 > 0
 		cM1Seq := SubStr(aPNR[nPosM1][3], 3, 2)
		If aListProd[nProds][1] != 'AIR'
			// lendo a linha do bloco M3
			For nX := 1 To Len(aPNR[nPosM1 + aScan(aAmarraM1, {|x| x == 'M3'})][3]) Step 2
				cM3Seq := SubStr(aPNR[nPosM1 + aScan(aAmarraM1, {|x| x == 'M3'})][3], nX, 2)
				If (nPosM3 := aScan(aPNR, {|x| x[1] == 'M3' .And. x[2] == cM3Seq .And. aListProd[nProds][1] $ x[3]})) > 0 
					aPax := {}  
	 				aAdd(aPax, {'M0', cM1Seq, aPNR[aScan(aPNR, {|x| x[1] == 'M0'})][3]})
	 				aAdd(aPax, {'M1', cM1Seq, aPNR[nPosM1][3]}) 

					// lendo as linhas que definem os blocos amarrados ao bloco M1
					For nM1 := 1 To Len(aAmarraM1)
						If aAmarraM1[nM1] != 'M5'
							For nY := 1 To Len(aPNR[nPosM1 + nM1][3]) Step 2
								IIf(aAmarraM1[nM1] != 'M3', cBlocoSeq := SubStr(aPNR[nPosM1 + nM1][3], nY, 2), cBlocoSeq := cM3Seq) 
								If (nBlocoPos := aScan(aPNR, {|x| x[1] == aAmarraM1[nM1] .And. x[2] == cBlocoSeq})) > 0
									aAdd(aPax, {aAmarraM1[nM1], cM1Seq, aPNR[nBlocoPos][3]})
									If aAmarraM1[nM1] == 'M3'; Exit; EndIf
								EndIf
							Next nY
						EndIf
					Next nM1
					aAdd(aListG3Q, {'', {aPax}})
				EndIf
			Next nX
		Else
			aAux     := TURXG8UDET(aG8U, '1', 'G3Q_PROD')
			nIniProd := aAux[aScan(aAux, {|x| x[1] == 'G8U_INICIO'})][2]
		
			aAux     := TURXG8UDET(aG8U, '1', 'G3Q_DOC')
			cBloco   := aAux[aScan(aAux, {|x| x[1] == 'G8U_BLOCO' })][2]
			nInicio  := aAux[aScan(aAux, {|x| x[1] == 'G8U_INICIO'})][2]
			nTamanh  := aAux[aScan(aAux, {|x| x[1] == 'G8U_TAMANH'})][2]

			// lendo a linha do bloco M5
			For nX := 1 To Len(aPNR[nPosM1 + aScan(aAmarraM1, {|x| x == 'M5'})][3]) Step 2
				cM5Seq := SubStr(aPNR[nPosM1 + aScan(aAmarraM1, {|x| x == 'M5'})][3], nX, 2)
				If (nPosM5 := aScan(aPNR, {|x| x[1] == 'M5' .And. x[2] == cM5Seq})) > 0
					cG3QDoc := TA048GET(aPNR[nPosM5], cBloco, '', nInicio, nTamanh, '', .T.)

					aPax := {}
	 				aAdd(aPax, {'M0', cM1Seq, aPNR[aScan(aPNR, {|x| x[1] == 'M0'})][3]})
	 				aAdd(aPax, {'M1', cM1Seq, aPNR[nPosM1][3]}) 
	
					// lendo as linhas que definem os blocos amarrados ao bloco M1
					For nM1 := 1 To Len(aAmarraM1)
						For nY := 1 To Len(aPNR[nPosM1 + nM1][3]) Step 2
							IIf(aAmarraM1[nM1] != 'M5', cBlocoSeq := SubStr(aPNR[nPosM1 + nM1][3], nY, 2), cBlocoSeq := cM5Seq) 
							If (nBlocoPos := aScan(aPNR, {|x| x[1] == aAmarraM1[nM1] .And. x[2] == cBlocoSeq})) > 0
								If aAmarraM1[nM1] == 'M3' .And. aListProd[nProds][1] $ aPNR[nBlocoPos][3] .And. At(aListProd[nProds][1], aPNR[nBlocoPos][3]) == nIniProd 
									aAdd(aPax, {aAmarraM1[nM1], cM1Seq, aPNR[nBlocoPos][3]})
								ElseIf aAmarraM1[nM1] != 'M3' 
									aAdd(aPax, {aAmarraM1[nM1], cM1Seq, aPNR[nBlocoPos][3]})
									If aAmarraM1[nM1] == 'M5'; Exit; EndIf
								EndIf
							EndIf
						Next nY
					Next nM1
		
					// caso exista o bloco M2, ler as linhas que definem os blocos amarrados a ele
					If (nPosM2 := aScan(aPNR, {|x| x[1] == 'M2' .And. x[2] == cM1Seq})) > 0
						aAdd(aPax, {'M2', cM1Seq, aPNR[nPosM2][3]}) 			
						For nM2 := 1 To Len(aAmarraM2)
							For nY := 1 To Len(aPNR[nPosM2 + nM2][3]) Step 2
								cBlocoSeq := SubStr(aPNR[nPosM2 + nM2][3], nY, 2)
								If (nBlocoPos := aScan(aPNR, {|x| x[1] == aAmarraM2[nM2] .And. x[2] == cBlocoSeq})) > 0
									aAdd(aPax, {aAmarraM2[nM2], cM1Seq, aPNR[nBlocoPos][3]})
								EndIf
							Next nY
						Next nM1
					EndIf
					
					If (nPosDoc := aScan(aListG3Q, {|x| x[1] == cG3QDoc})) > 0
						aAdd(aListG3Q[nPosDoc][2], aPax)
					Else
						aAdd(aListG3Q, {cG3QDoc, {aPax}})
					EndIf
				EndIf
			Next nX 
		EndIf
		
		nPosM1 := aScan(aPNR, {|x| x[1] == 'M1' .And. x[2] == Soma1(cM1Seq)})
	EndDo

	aListProd[nProds][5] := aClone(aListG3Q)
Next nProds

Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048PRCPNR

Fun��o que realiza a obten��o dos dados do arquivo PNR atrav�s do Layout GDS

@sample	TA048PRCPNR(aPNR, aG8U, cDado, cTabela, oModel, aRepeat, aIncrement)
@param     aPNR: Array - lista com as linhas do arquivo PNR
            aG8U: Array - dados da tabela Layout GDS
            cDado: String - informa o tipo de dado a ser obtido (1-Vari�vel / 2-Campo) 
			cTabela: String - informa a tabela que ter� os dados obtidos 
			oModel: Objeto - modelo de dados da tabela que receber� as informa��es apuradas 
			aRepeat: Array - lista de TAGs do bloco M9 que s�o lidas repetidas vezes
			aIncrement: Array - lista de de campos incrementais
@return 	
@author    Thiago Tavares
@since     17/05/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048PRCPNR(aPNR, aG8U, cDado, cTabProc, oModel, aRepeat, aIncrement)

Local aArea      := GetArea()
Local lProssegue := .F.
Local lRet       := .T.
Local lWhen      := .F.
Local cGDS       := ''
Local cVersao    := ''
Local cSequen    := ''
Local cTabela    := ''
Local cCampo     := ''
Local cVariav    := ''
Local cContex    := ''
Local cBloco     := ''
Local cTag       := ''
Local cTagMul    := ''
Local cPassag    := ''
Local cSepara    := ''
Local cFRead     := ''
Local cSeek      := ''
Local cAliasX    := ''
Local cChave     := ''
Local cReturn    := ''
Local cFPos      := ''
Local cTipo      := ''
Local cFForm     := ''
Local cWhen      := ''
Local cMacFix    := '' 
Local cIncrem    := ''
Local cAddTag    := ''
local cVarAux    := ''
Local cCodePNR   := ''
Local cErro      := ''
Local cPosicione := ''
Local cTabAnt    := ''
Local nX         := 1
Local nY         := 1
Local nQtde      := 0
Local nInicio    := 0 
Local nTamanh    := 0
Local nPosica    := 0
Local nIndice    := 0
Local nSubIni    := 0
Local nSubTam    := 0
Local aVariav    := {} 
Local bBlock     := {|| }
Local aSplit     := {}
Local aSetStc    := {}
Local aModels    := {}
Local oMdlAux    := Nil

aAdd(aModels, {'G3P', 'G3P_FIELDS'})
aAdd(aModels, {'G3Q', 'G3Q_ITENS' })
aAdd(aModels, {'G3R', 'G3R_ITENS' })
aAdd(aModels, {'G3S', 'G3S_ITENS' })
aAdd(aModels, {'G3T', 'G3T_ITENS' })
aAdd(aModels, {'G3U', 'G3U_ITENS' })
aAdd(aModels, {'G3V', 'G3V_ITENS' })
aAdd(aModels, {'G44', 'G44_ITENS' })
aAdd(aModels, {'G46', 'G46_ITENS' })
aAdd(aModels, {'G47', 'G47_ITENS' })
aAdd(aModels, {'G4A', 'G4A_ITENS' })
aAdd(aModels, {'G4B', 'G4B_ITENS' })
aAdd(aModels, {'G4D', 'G4D_ITENS' })

If !Empty(cTabProc)
	oMdlAux := oModel:GetModel(aModels[aScan(aModels, {|x| x[1] == cTabProc})][2]) 
EndIf

While nX <= Len(aG8U)
	If lRet
		lProssegue := .F.
		If cDado == '2' .And. ; 			// 2-vari�vel
		   aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_DADO'  )})][2] == cDado .And. ;
		   Empty(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_TABELA')})][2])  	
			lProssegue := .T.
		ElseIf cDado == '1' .And. ;
		       aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_TABELA')})][2] == cTabProc
			lProssegue := .T.	
		EndIf
	
		If lProssegue
			cGDS    := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_GDS'   )})][2])
			cVersao := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_VERSAO')})][2])
			cSequen := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_SEQUEN')})][2])
			cTabela := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_TABELA')})][2])
			cCampo  := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_CAMPO' )})][2])
			cVariav := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_VARIAV')})][2])
			cContex := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_CONTEX')})][2])
			cBloco  := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_BLOCO' )})][2])
			cTag    := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_TAG'   )})][2])
			cTagMul := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_TAGMUL')})][2])
			cPassag := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_PASSAG')})][2])
			cSepara := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_SEPARA')})][2])
			cFRead  := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_FREAD' )})][2])
			cSeek   := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_SEEK'  )})][2])
			cAliasX := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_ALIAS' )})][2])
			cChave  := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_CHAVE' )})][2])
			cReturn := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_RETURN')})][2])
			cFPos   := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_FPOS'  )})][2])
			cTipo   := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_TIPO'  )})][2])
			cFForm  := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_FFORM' )})][2])
			cWhen   := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_WHEN'  )})][2])
			cMacFix := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_MACFIX')})][2]) 
			cIncrem := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_INCREM')})][2])
			nInicio := aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_INICIO')})][2] 
			nTamanh := aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_TAMANH')})][2]
			nPosica := aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_POSICA')})][2]
			nIndice := aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_INDICE')})][2]
			nSubIni := aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_SUBINI')})][2]
			nSubTam := aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_SUBTAM')})][2]
	
			//Tratamento da instru��o de cancelamento
			If cIU0TYP == NIL
				lProssegue := .T. 
			Else 
				If cIU0TYP == '1' 				// Verifica se � igual a 1-Cancelamento
					If cSequen == '000' 			// � igual a 1-Cancelamento e a Sequencia � 000, prossegue  
						lProssegue := .T. 		 
					Else
						lProssegue := .F. 		// � igual a 1-Cancelamento e a Sequencia � diferente de 000, N�O prossegue.
						Exit
					EndIf
				Else 								// Se for diferente de 1-Cancelamento, prossegue   
					lProssegue := .T. 			
				EndIf
			EndIf
	
			If lProssegue
			
				//Cria uma variavel para armazenar o conteudo lido de modo que possa ser utilizado ou manipulado posteriormente
				Do Case
					Case cGDS == '1'
						cCodePNR := 'SAB'
	
					Case cGDS == '2'
						cCodePNR := 'AMA'
						
					OtherWise
						cCodePNR := 'OTH'
				EndCase
	
				cVarAux := 'CVAR' + cCodePNR + AllTrim(cSequen)
				If aScan(aVariav, {|x| x == cVarAux}) == 0
					TA048VARS(cVarAux, 'C')
					aAdd(aVariav, cVarAux)
				EndIf
	
				cAddTag := ''
				If cPassag == '1' .And. !Empty(AllTrim(cTag))
					cAddTag := StrZero(nAtuPass, 2) + '.'
				ElseIf cCampo == 'G3Q_FORMPG'
					cAddTag := AllTrim(Posicione('G6K', 1, xFilial('G6K') + '000007' + cPNRProd, 'G6K_CDPARA')) + '.'
				EndIf
	
				cLido := TA048GET(aPNR, cBloco, cTag + cAddTag, nInicio, nTamanh, cSepara, .F.)
				If !Empty(cSepara)
					aSplit := {}
					aSplit := StrTokArr2(cLido, cSepara, .F.)
					
					If nPosica <= Len(aSplit) .And. nPosica <> 0
						cLido := AllTrim(aSplit[nPosica])
						cTag  := cTag
						If !Empty(cTag)
							If cTag $ cLido
								cVar  := StrTran(cLido, cTag, '')
								cLido := cVar
							EndIf
						EndIf
					Else
						cLido := ''
					EndIf
				EndIf
				&(cVarAux + ' := AllTrim(cLido)')
	
				// Caso uma fun��o ou express�o de leitura tenha sido informada, este ser� o metodo de leitura utilizado
				If !Empty(cFRead)							
					If cMacFix == '2' 			// 2-Fixo
						cLido := cFRead
					Else 							// 1-Macro Execu��o
						bBlock := ErrorBlock({|e| TA048G8VLOG(e, cGDS, cVersao, cSequen, '3', '', cFRead, {})})
						BEGIN SEQUENCE
							cLido := &(cFRead)
						RECOVER
							lRet := .F.
						END SEQUENCE
	 					ErrorBlock(bBlock)
					EndIf
				EndIf
				&(cVarAux + ' := AllTrim(cLido)')
	
				//Caso uma fun��o ou express�o de Posicionamento tenha sido informada, este ser� o metodo de Posicionamento utilizado
				If lRet .And. !Empty(cFPos)		
					bBlock := ErrorBlock({|e| TA048G8VLOG(e, cGDS, cVersao, cSequen, '3', '', cFPos, {})})
					BEGIN SEQUENCE
						cPosicione := &(cFPos)
					RECOVER
						lRet := .F.
					END SEQUENCE
					ErrorBlock(bBlock)
				ElseIf lRet
					If cSeek == '1' 	
						If !Empty(cFPos)
							If FindFunction(cFPos) .Or. ExistBlock(cFPos)
								bBlock := ErrorBlock({|e| TA048G8VLOG(e, cGDS, cVersao, cSequen, '3', '', cFPos, {})})
								BEGIN SEQUENCE
									cPosicione := &(cFPos + '(cLido)')
								RECOVER
									lRet := .F.
								END SEQUENCE
								ErrorBlock(bBlock)
							Else
								TA048G8VLOG(Nil, cGDS, cVersao, cSequen, '3', STR0001 + cFPos, cFPos, {})  //'Fun��o inexistente: '
								lRet := .F.  
							EndIf
						Else
							bBlock := ErrorBlock({|e| TA048G8VLOG(e, cGDS, cVersao, cSequen, '3', '', cChave, {})})
							BEGIN SEQUENCE
								cPosicione := Posicione(cAliasX, nIndice, &(cChave), cReturn)
							RECOVER
								lRet := .F.
							END SEQUENCE
							ErrorBlock(bBlock)
						EndIf
					Else
						cPosicione := cLido
					EndIf
				EndIf
					
				//Caso o tipo da Vari�vel seja String
				If lRet .And. cTipo $ '1C'
					//Caso tenha sido informadas instru��es para execu��o de Substring (In�cio e Tamanho)																	
					If nSubIni <> 0 .And. nSubTam <> 0								
						cPosicione := Substr(cPosicione, nSubIni, nSubTam)			
					EndIf
					uConteudo := AllTrim(cPosicione)													
				EndIf
				
				//Caso uma fun��o ou express�o de Formata��o tenha sido informada, executa a express�o
				If lRet .And. !Empty(cFForm)							
					bBlock := ErrorBlock({|e| TA048G8VLOG(e, cGDS, cVersao, cSequen, '3', '', cFForm, {})})
					BEGIN SEQUENCE
						uConteudo := &(cFForm)
					RECOVER
						lRet := .F.
					END SEQUENCE
					ErrorBlock(bBlock)
				ElseIf lRet .And. cTipo $ '2N' .And. ValType(cPosicione) == 'C'
					uConteudo := AllTrim(cPosicione)
				ElseIf lRet .And. cTipo $ '2N' .And. ValType(cPosicione) == 'N'
					uConteudo := cPosicione
				ElseIf lRet .And. cTipo $ '3D' 
					uConteudo := cPosicione
				EndIf
				
				If lRet .And. cTipo $ '2N' .And. ValType(uConteudo) == 'C'
					uConteudo := Val(uConteudo)
				EndIf
	
				If lRet .And. !Empty(cWhen)
					cWhen := AllTrim(cWhen)
				ElseIf lRet  
					cWhen := '.T.'
				EndIf
				
				If Upper(cVariav) == 'CSEGNEG'
					aAdd(aSetStc, {'lHierarquia', .F.})
					Do Case
						Case AllTrim(Upper(uConteudo)) $ '1C'
							aAdd(aSetStc, {'nSegmento', 1})																			
	
						Case AllTrim(Upper(uConteudo)) $ '2E'
							aAdd(aSetStc, {'nSegmento', 2})
							
						Case AllTrim(Upper(uConteudo)) $ '3L'
							aAdd(aSetStc, {'nSegmento', 3})
					EndCase
					
					T034SetStc(aSetStc)
				EndIf
				
				If lRet 
					bBlock := ErrorBlock({|e| TA048G8VLOG(e, cGDS, cVersao, cSequen, '3', '', cWhen, {})})
					BEGIN SEQUENCE
						lWhen := &(cWhen)
					RECOVER
						lRet := .F.
					END SEQUENCE
					ErrorBlock(bBlock)
				EndIf 
					
				If lWhen .And. lRet  
					If cDado == '1' .And. !Empty(cCampo)
						// verificando se o campo � incremental, caso afirmativo, pega o pr�ximo valor
						If aScan(aIncrement, {|x| x[1] == cCampo}) > 0 .And. aScan(aIncrement, {|x| x[2] == cSequen}) > 0 													
							nLenModel := oMdlAux:Length() - 1							
							nLenModel := IIf(nLenModel == 0, 1, nLenModel)									 
							uConteudo := StrZero(Val(oMdlAux:GetValue(cCampo, nLenModel)) + 1, TamSx3(cCampo)[1])		
	
							If Empty(AllTrim(uConteudo))								
								uConteudo := StrZero(1, TamSx3(cCampo)[1])		
							EndIf
						EndIf
						
						If cPNRProd == 'AIR' .And. (cCampo == 'G44_TARBAS' .Or. cCampo == 'G46_VLBASE')
							uConteudo += oMdlAux:GetValue(cCampo)
						EndIf
						
						If !(lRet := oMdlAux:SetValue(cCampo, uConteudo))
							TA048G8VLOG(Nil, cGDS, cVersao, cSequen, '3', '', '', oModel:GetErrorMessage())
							Exit
						ElseIf cCampo == 'G44_TXCAMB'
							oModel:GetModel('G3Q_ITENS'):LoadValue('G3Q_TXCAMB', oMdlAux:GetValue('G44_TXCAMB'))
							oModel:GetModel('G3R_ITENS'):LoadValue('G3R_TXCAMB', oMdlAux:GetValue('G44_TXCAMB'))
						EndIf
					ElseIf !Empty(cVariav)
						&(cVariav + ' := uConteudo')
					EndIf
				EndIf
				
				//Controle de repeti��es
				cTabAnt := AllTrim(aG8U[nX][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_TABELA')})][2])
				If (nPosRpt := AScan(aRepeat, {|x| x[1] == cTabAnt})) <> 0
					If nX + 1 > Len(aG8U) .Or. AllTrim(aG8U[nX + 1][aScan(aG8U[nX], {|x| x[1] == AllTrim('G8U_TABELA')})][2]) <> cTabAnt
						If aRepeat[nPosRpt][2] == 'T'								
							If aRepeat[nPosRpt][4] > 1								
								aRepeat[nPosRpt][6]++								
								If aRepeat[nPosRpt][6] < aRepeat[nPosRpt][4]		
									TA048ALTBLC(@aPNR, aG8U, 'M9', cTag)
									nX    := aRepeat[nPosRpt][5] - 1
									nQtde := oMdlAux:Length() 
									If oMdlAux:AddLine() == nQtde
										TA048G8VLOG(Nil, '1', cVersao, cSequen, '3', '', '', oModel:GetErrorMessage())
										lRet := .F.
										Exit
									EndIf 
								EndIf	
							EndIf
						EndIf
					EndIf	
				EndIf
			EndIf
		EndIf
	EndIf
	nX++
EndDo	

RestArea(aArea)

Return lRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048GET

Fun��o para obter o conteudo de uma posicao ou tag  

@sample	TA048GET(aPNR, cBloco, cTag, nInicio, nTam, cSepara, lUnica)
@param		aPNR: Array - linhas arquivo PNR
			cBloco: String - nome do bloco do qual ser� obtido o valor 
 			cTag: String - TAG a ser removida do valor obtido
 			nIni: Numerico - posicao inicial na linha
 			nTam: Numerico - quantidade de Strings a serem lidos
 			cSepara: String - informa o serapador
 			lUnica: L�gico - indica se a leitura ser� a partir de uma �nica linha 
@return 	cRet: String - valor obtido do arquivo 
@author    Marcelo Cardoso Barbosa
@since     02/09/2015
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048GET(aPNR, cBloco, cTag, nInicio, nTam, cSepara, lUnica)
	
Local lRet      := .F.
Local cRet      := ''
Local nPosM     := 0
Local nSplit    := 0
Local nBloco    := 0
Local nPNR      := 0
Local aSplit    := {}
Local aBloco    := {}

If lUnica
	cRet := SubStr(aPNR[3], nInicio, nTam)
Else
	If (nPosM := aScan(aPNR, {|x| SubStr(x[1], 1, Len(cBloco)) == cBloco})) > 0
		If !Empty(AllTrim(cTag)) .And. cBloco = 'M9'
			For nPNR := nPosM To Len(aPNR)
				If lRet; Exit; EndIf
				aSplit := StrTokArr2(SubStr(aPNR[nPNR][3], 5, Len(aPNR[nPNR][3])), cSepara, .F.)
				If Len(aSplit) > 0 .And. SubStr(aPNR[nPNR][3], 1, 2) == 'M9' 
					For nSplit := 1 To Len(aSplit)
						If cTag $ aSplit[nSplit]
							cRet := StrTran(aSplit[nSplit], cTag, '')
							lRet := .T.
							Exit
						EndIf
					Next nSplit
				EndIf
			Next nPNR
		
			If Len(aBloco) > 0
				For nBloco := 1 To Len(aBloco)
				Next nBloco
			EndIf
		Else
			If nInicio <> 0 .And. nTam <> 0
				cRet := SubStr(aPNR[nPosM][3], nInicio, nTam)
			EndIf
		EndIf
	EndIf
EndIf
		
Return cRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048PG4B

Fun��o que realiza a obten��o dos dados do arquivo PNR atrav�s do Layout GDS

@sample	TA048PG4B(aPNR, aG8U, oModel, nIV)
@param     aPNR: Array - lista com as linhas do arquivo PNR
			aG8U: Array - lista de registros do layout GDS
			oModel: Objeto - modelo de dados da tabela que receber� as informa��es apuradas
			nIV: N�merico - linha do IV dentro do modelo de dados 
@return 	
@author    Thiago Tavares
@since     17/05/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048PG4B(aPNR, aG8U, oModel, nIV)

Local aArea   := GetArea()
Local lRet    := .T.
Local oMdlG3Q := Nil
Local oMdlG3S := Nil
Local oMdlG4B := Nil
Local nX      := 0 
Local nY      := 0
Local nPosM9  := 0
Local cTag    := ''
Local cTagPax := ''

oMdlG3Q := oModel:GetModel('G3Q_ITENS')
oMdlG3Q:GoLine(nIV)

oMdlG3S := oModel:GetModel('G3S_ITENS')
For nX := 1 To oMdlG3S:Length()
	oMdlG3S:GoLine(nX)
	oMdlG4B := oModel:GetModel('G4B_ITENS')
	For nY := 1 To oMdlG4B:Length()
		oMdlG4B:GoLine(nY)
		If !Empty(cTag := AllTrim(Posicione('G6K', 1, xFilial('G6K') + '000009' + oMdlG4B:GetValue('G4B_TPENT'), 'G6K_CDPARA')))
			cTag    += '.'
			cTagPax := cTag + StrZero(nX, 2) + '.'
			For nPosM9 := aScan(aPNR, {|x| x[1] == 'M9'}) To Len(aPNR) 
				If cTagPax $ aPNR[nPosM9][3]
					oMdlG4B:SetValue('G4B_ITEM', StrTran(AllTrim(TA048GET(aPNR[nPosM9], 'M9', cTagPax, 5, Len(aPNR[nPosM9][3]), '', .T.)), cTagPax, ''))
					TA048ALTBLC(@aPNR, aG8U, 'M9', cTagPax)
					Exit
				ElseIf cTag $ aPNR[nPosM9][3]
					oMdlG4B:SetValue('G4B_ITEM', StrTran(AllTrim(TA048GET(aPNR[nPosM9], 'M9', cTag, 5, Len(aPNR[nPosM9][3]), '', .T.)), cTag, ''))
					Exit
				EndIf	
			Next nPosM9
		EndIf
	Next nY
Next nX 

Return 

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048MDLDEF

Fun��o que monta a estrutura do model  

@sample	TA048MDLDEF(oModel)
@param 		cBloco: Obejto - modelo de dados do RV 
@return 	 
@author    Thiago Tavares
@since     17/05/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048MDLDEF(oModel)

Local oStrG3P   := Nil
Local oStrG3Q   := Nil
Local oStrG3R   := Nil
Local oStrG3T   := Nil
Local oStrG3U   := Nil
Local oStrG3V   := Nil
Local oStrG44   := Nil
Local oStrG4A   := Nil
Local oStrG4B   := Nil
Local oStrG4D   := Nil
Local oStrG46   := Nil
Local oStrG47   := Nil
Local oStrG48A  := Nil
Local oStrG48B  := Nil

oStrG3P  := oModel:GetModel('G3P_FIELDS'):GetStruct()
oStrG3Q  := oModel:GetModel('G3Q_ITENS' ):GetStruct()
oStrG3R  := oModel:GetModel('G3R_ITENS' ):GetStruct()
oStrG3T  := oModel:GetModel('G3T_ITENS' ):GetStruct()
oStrG3U  := oModel:GetModel('G3U_ITENS' ):GetStruct()
oStrG3V  := oModel:GetModel('G3V_ITENS' ):GetStruct()
oStrG44  := oModel:GetModel('G44_ITENS' ):GetStruct()
oStrG4A  := oModel:GetModel('G4A_ITENS' ):GetStruct()
oStrG4B  := oModel:GetModel('G4B_ITENS' ):GetStruct()
oStrG4D  := oModel:GetModel('G4D_ITENS' ):GetStruct()
oStrG46  := oModel:GetModel('G46_ITENS' ):GetStruct()
oStrG47  := oModel:GetModel('G47_ITENS' ):GetStruct()
oStrG48A := oModel:GetModel('G48A_ITENS'):GetStruct()
oStrG48B := oModel:GetModel('G48B_ITENS'):GetStruct()

oStrG3P:SetProperty('G3P_UNINEG', MODEL_FIELD_OBRIGAT,     .F. )

// Item de Venda
oStrG3Q:SetProperty('G3Q_TARIFA', MODEL_FIELD_OBRIGAT,     .F. )
oStrG3Q:SetProperty('G3Q_TAXA'  , MODEL_FIELD_OBRIGAT,     .F. )
oStrG3Q:SetProperty('G3Q_EXTRA' , MODEL_FIELD_OBRIGAT,     .F. )
oStrG3Q:SetProperty('G3Q_VLRIMP', MODEL_FIELD_OBRIGAT,     .F. )
oStrG3Q:SetProperty('G3Q_VLRSER', MODEL_FIELD_OBRIGAT,     .F. )
oStrG3Q:SetProperty('G3Q_MARKUP', MODEL_FIELD_OBRIGAT,     .F. )
oStrG3Q:SetProperty('G3Q_PRCBAS', MODEL_FIELD_OBRIGAT,     .F. )
oStrG3Q:SetProperty('G3Q_DESC'  , MODEL_FIELD_OBRIGAT,     .F. )
oStrG3Q:SetProperty('G3Q_PRECAG', MODEL_FIELD_OBRIGAT,     .F. )
oStrG3Q:SetProperty('G3Q_TXCAMB', MODEL_FIELD_OBRIGAT,     .F. )
oStrG3Q:SetProperty('G3Q_TAXADU', MODEL_FIELD_OBRIGAT,     .F. )
oStrG3Q:SetProperty('G3Q_TAXAAG', MODEL_FIELD_OBRIGAT,     .F. )
oStrG3Q:SetProperty('G3Q_FEE'   , MODEL_FIELD_OBRIGAT,     .F. )
oStrG3Q:SetProperty('G3Q_REPASS', MODEL_FIELD_OBRIGAT,     .F. )
oStrG3Q:SetProperty('G3Q_TOTREC', MODEL_FIELD_OBRIGAT,     .F. )
oStrG3Q:SetProperty('G3Q_VLFIM' , MODEL_FIELD_OBRIGAT,     .F. )
oStrG3Q:SetProperty('G3Q_MOEDCL', MODEL_FIELD_VALID  , {|| .T.})

// Documento de Reserva
oStrG3R:SetProperty('G3R_FORNEC', MODEL_FIELD_OBRIGAT,     .F. )
oStrG3R:SetProperty('G3R_LOJA'  , MODEL_FIELD_OBRIGAT,     .F. )
oStrG3R:SetProperty('G3R_FORREP', MODEL_FIELD_VALID  , {|| .T.})
oStrG3R:SetProperty('G3R_LOJREP', MODEL_FIELD_VALID  , {|| .T.})
oStrG3R:SetProperty('G3R_MOEDA' , MODEL_FIELD_VALID  , {|| .T.})
oStrG3R:SetProperty('G3R_AGRESE', MODEL_FIELD_VALID  , {|| .T.})
oStrG3R:SetProperty('G3R_SISORI', MODEL_FIELD_VALID  , {|| .T.})

// Segmento AEREO
oStrG3T:SetProperty('G3T_CLASRV', MODEL_FIELD_VALID  , {|| .T.})

// Segmento HOTEL
oStrG3U:SetProperty('G3U_BROKER', MODEL_FIELD_VALID  , {|| .T.})
oStrG3U:SetProperty('G3U_CODAPO', MODEL_FIELD_VALID  , {|| .T.})

// Segmento CARRO
oStrG3V:SetProperty('G3V_BROKER', MODEL_FIELD_VALID  , {|| .T.})
oStrG3V:SetProperty('G3V_CIDRET', MODEL_FIELD_VALID  , {|| .T.})
oStrG3V:SetProperty('G3V_CIDDEV', MODEL_FIELD_VALID  , {|| .T.})
oStrG3V:SetProperty('G3V_CATAUT', MODEL_FIELD_VALID  , {|| .T.})
oStrG3V:SetProperty('G3V_TPAUT' , MODEL_FIELD_VALID  , {|| .T.})
oStrG3V:SetProperty('G3V_COMBAR', MODEL_FIELD_VALID  , {|| .T.})
oStrG3V:SetProperty('G3V_TRADIR', MODEL_FIELD_VALID  , {|| .T.})

// Tarifas
oStrG44:SetProperty('G44_MOEDFO', MODEL_FIELD_VALID  , {|| .T.})
oStrG44:SetProperty('G44_MENOR' , MODEL_FIELD_VALID  , {|| .T.})
oStrG44:SetProperty('G44_CHEIA' , MODEL_FIELD_VALID  , {|| .T.})
oStrG44:SetProperty('G44_MAIOR' , MODEL_FIELD_VALID  , {|| .T.})
oStrG44:SetProperty('G44_PUBLIC', MODEL_FIELD_VALID  , {|| .T.})

// Taxas
oStrG46:SetProperty('*'         , MODEL_FIELD_VALID  , {|| .T. })

// Extras
oStrG47:SetProperty('*'         , MODEL_FIELD_VALID  , {|| .T. })

// Rateio
oStrG4A:SetProperty('G4A_TPENT' , MODEL_FIELD_VALID  , {|| .T.})
oStrG4A:SetProperty('G4A_ITEM'  , MODEL_FIELD_VALID  , {|| .T.})
oStrG4A:SetProperty('G4A_LOJA'  , MODEL_FIELD_VALID  , {|| .T.})

// Entidades Adicionais 
oStrG4B:SetProperty('G4B_TPENT', MODEL_FIELD_VALID   , {|| .T.})
oStrG4B:SetProperty('G4B_ITEM' , MODEL_FIELD_VALID   , {|| .T.})

// Dados do Cart�o - altera��o realizada referente nova regra de importa��o dos dados do cart�o - DEFEITO PCDEF-97705
oStrG4D:SetProperty('*'         , MODEL_FIELD_VALID  , {|| .T. })
oStrG4D:SetProperty('G4D_MESVAL', MODEL_FIELD_OBRIGAT, .F.      )
oStrG4D:SetProperty('G4D_ANOVAL', MODEL_FIELD_OBRIGAT, .F.      )
oStrG4D:SetProperty('G4D_CODCAR', MODEL_FIELD_WHEN   , {|| .T. })
oStrG4D:SetProperty('G4D_NUMCAR', MODEL_FIELD_WHEN   , {|| .T. })
oStrG4D:SetProperty('G4D_CODBAN', MODEL_FIELD_WHEN   , {|| .T. })
oStrG4D:SetProperty('G4D_TITULA', MODEL_FIELD_WHEN   , {|| .T. })
oStrG4D:SetProperty('G4D_MESVAL', MODEL_FIELD_WHEN   , {|| .T. })
oStrG4D:SetProperty('G4D_ANOVAL', MODEL_FIELD_WHEN   , {|| .T. })

// Acordos de Cliente
oStrG48A:SetProperty('G48_MOEDA', MODEL_FIELD_VALID  , {|| .T.})
 
// Acordos de Cliente
oStrG48B:SetProperty('G48_MOEDA', MODEL_FIELD_VALID  , {|| .T.})
 
Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048QTBLCS

Fun��o que retorna a quantidade de uma determinado bloco  

@sample	TA048QTBLCS(aPNR, cBloco)
@param 		aPNR: Array - lista com as linhas do arquivo PNR
            cBloco: String - c�digo do bloco que ser� quantificado
@return 	nRet: N�merico - quantidade de blocos encontrados
@author 	Thiago Tavares
@since 		21/06/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048QTBLCS(aPNR, cBloco)

Local nRet := 0
Local nPos := 0
Local cSeq := 0 

nPos := aScan(aPNR, {|x| x[1] == cBloco})
While nPos > 0
	nRet++
	cSeq := SubStr(aPNR[nPos][3], 3, 2)
	nPos := aScan(aPNR, {|x| x[1] == cBloco .And. x[2] == Soma1(cSeq)})
EndDo

Return nRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048QTSEG

Fun��o que retorna a quantidade de segmentos de um produto  

@sample	TA048QTSEG(aPAXs, aG8U, cProd)
@param 		aPAXs: Array - lista de linhas de cada passageiro
            aG8U: Array - dados da tabela Layout GDS
            cProd: String - c�digo do produto no arquivo PNR (AIR/HHT/HTL/HHL/CAR)
            cNumConj: String - quantidade de bilhetes conjugados extra�do do PNR
@return 	Array: nRet: N�merico - quantidade de segmentos encontrados
			        nNumConj: N�merico - quantidade de blihetes conjudados 
@author 	Thiago Tavares
@since 		25/05/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048QTSEG(aPAXs, aG8U, cProd, cNumConj)

Local cBloco   := ''
Local nRet     := 0
Local nNumConj := Val(cNumConj)
Local nInicio  := 0
Local nTamanh  := 0
Local nPos     := 0
Local nRest    := 0
Local aAux     := TURXG8UDET(aG8U, '1', 'G3Q_PROD')

If Len(aAux) > 0
	cBloco  := aAux[aScan(aAux, {|x| x[1] == 'G8U_BLOCO' })][2]
	nInicio := aAux[aScan(aAux, {|x| x[1] == 'G8U_INICIO'})][2]
	nTamanh := aAux[aScan(aAux, {|x| x[1] == 'G8U_TAMANH'})][2]
	
	If (nPos := aScan(aPAXs, {|x| x[1] == cBloco})) > 0
		While aPAXs[nPos][1] == cBloco
			If TA048GET(aPAXs[nPos], cBloco, '', nInicio, nTamanh, '', .T.) == cProd
				nRet++
			EndIf
			nPos++
		EndDo
	EndIf
	
	If (cNumConj == '0' .Or. Empty(cNumConj)) .And. nRet > 4
		nNumConj := (Int(nRet / 4) + IIf(nRet % 4 > 0, 1, 0)) - 1
	EndIf
EndIf

Return {nRet, nNumConj}

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048ALTBLC

Fun��o que altera o bloco/tag j� processada para que n�o seja reprocessado 

@sample	TA048ALTBLC(aPAX, aG8U, cBloco, cFind)
@param 		aPAX: Array - lista de linhas de cada passageiro
            aG8U: Array - dados da tabela Layout GDS
			cBloco: String - c�digo do bloco
			cFind: String - tag ou trecho que ser� alterada
@return 	
@author 	Thiago Tavares
@since 		25/05/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048ALTBLC(aPAX, aG8U, cBloco, cFind)

Local nInicio  := 0
Local nTamanh  := 0
Local nPos     := 0
Local aProdPNR := {}

If cBloco == 'M3' .And. Len((aProdPNR := TURXG8UDET(aG8U, '1', 'G3Q_PROD'))) > 0
	nInicio := aProdPNR[aScan(aProdPNR, {|x| x[1] == 'G8U_INICIO'})][2]
	nTamanh := aProdPNR[aScan(aProdPNR, {|x| x[1] == 'G8U_TAMANH'})][2]
EndIf
	
If (nPos := aScan(aPAX, {|x| x[1] == cBloco})) > 0
	While aPAX[nPos][1] == cBloco
		If (cBloco == 'M3' .And. TA048GET(aPAX[nPos], cBloco, '', nInicio, nTamanh, '', .T.) == cFind) .Or. ;
		   (cBloco == 'M9' .And. aPAX[nPos][1] == 'M9' .And. cFind $ aPAX[nPos][3]) .Or. ;
		   (cBloco $ 'M4|M5')
			aPAX[nPos][1] := 'XX'
			aPAX[nPos][3] := StrTran(aPAX[nPos][3], cBloco, 'XX', 1)
			If cBloco == 'M9' 
				aPAX[nPos][3] := StrTran(aPAX[nPos][3], cFind , 'XX', 1)
			EndIf
			Exit
		EndIf
		nPos++
	EndDo	
EndIf

Return 

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048VLDG4D

Fun��o respons�vel por validar as informa��es dos dados do cart�o mediante a FOP informada   

@sample	TA048VLDG4D(oModel)
@param 		oModel: Objeto - modelo de dados do Registro de Venda
@return 	Array: lRet: L�gico - determina se o RV deve ser integrado 
			        cCodCar: String - c�digo do cart�o 
			        cCodBan: String - c�digo da bandeira do cart�o
			        cTitula: String - titular do cart�o
			        cMesVal: String - m�s de validade do cart�o
			        cAnoVal: String - ano de validade do cart�o
			        cPropri: String - propriet�rio do cart�o (1-Cliente / 2-Ag�ncia)
@author 	Thiago Tavares
@since 		23/05/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048VLDG4D(oModel) 

Local aArea   := GetArea()
Local cCodCli := oModel:GetModel('G3P_FIELDS'):GetValue('G3P_CLIENT')
Local cLoja   := oModel:GetModel('G3P_FIELDS'):GetValue('G3P_LOJA'  )
Local cCodFop := oModel:GetModel('G3Q_ITENS' ):GetValue('G3Q_FORMPG')
Local cNumCC  := oModel:GetModel('G4D_ITENS' ):GetValue('G4D_NUMCAR')
Local cNumero := ''
Local cCodCar := ''
Local cCodBan := ''
Local cTitula := ''
Local cMesVal := ''
Local cAnoVal := '' 
Local cPropri := ''
Local lRet    := .F.

If !Empty(cNumCC)
	DbSelectArea('G3O')
	G3O->(DbSetOrder(1))		// G3O_FILIAL+G3O_CODIGO+G3O_ITEM
	If G3O->(DbSeek(xFilial('G3O') + cCodFop))
		// cart�o cliente
		lRet := .T.
		If G3O->G3O_TIPO == '4'
			If !Empty(cNumCC)  		
				cPropri := '1'
			EndIf
			DbSelectArea('G3J')
			G3J->(DbSetOrder(3))		// G3J_FILIAL+G3J_CODCLI+G3J_LOJA
			If G3J->(DbSeek(xFilial('G3J') + cCodCli + cLoja))
				While G3J->(!Eof())
					If G3J->G3J_MSBLQL == '2'
						cNumero := rc4crypt(G3J->G3J_NCARD, G3J->G3J_CODIGO, .F.)
						If AllTrim(cNumero) == AllTrim(cNumCC)
							cCodCar := G3J->G3J_CODIGO
							cCodBan := G3J->G3J_CODBAN
							cTitula := G3J->G3J_TITULA
							cMesVal := G3J->G3J_MVALID
							cAnoVal := G3J->G3J_AVALID
							cPropri := '1'
							Exit
						EndIf
					EndIf
					G3J->(DbSkip())
				EndDo
				G3J->(DbCloseArea())
			EndIf
		// intermedia��o + cart�o pr�prio
		ElseIf G3O->G3O_TIPO == '1' .And. G3O->G3O_ORICP == '4'
			lRet := .F.		
			DbSelectArea('G3D')
			G3D->(DbGoTop())
			While G3D->(!Eof())
				If G3D->G3D_MSBLQL == '2'
					cNumero := rc4crypt(G3D->G3D_NCARD, G3D->G3D_CODIGO, .F.)
					If AllTrim(cNumero) == AllTrim(cNumCC)
						cCodCar := G3D->G3D_CODIGO
						cCodBan := G3D->G3D_CODBAN
						cTitula := G3D->G3D_TITULA
						cMesVal := G3D->G3D_MVALID
						cAnoVal := G3D->G3D_AVALID
						cPropri := '2'
						lRet    := .T.
						Exit
					EndIf
				EndIf
				G3D->(DbSkip())
			EndDo
		EndIf
	EndIf
	G3O->(DbCloseArea())
EndIf

RestArea(aArea)

Return {lRet, cCodCar, cCodBan, cTitula, cMesVal, cAnoVal, cPropri}

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048G3QDOC

Fun��o que verifica se o bilhete j� foi utilizado em algum Registro de Venda 

@sample	TA048G3QDOC(aPNR, aG8U)
@param     aPNR: Array - lista com as linhas do arquivo PNR
            aG8U: Array - dados da tabela Layout GDS
@return	Array - lRet: L�gico - informa se o bilhete j� foi integrado (True) ou n�o (False)
					 cG3QDoc: String - n�mero do bilhete
					 cNumID: String - n�mero do RV caso o bilhete j� tenha sido importado 	
@author    Thiago Tavares
@since     17/05/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048G3QDOC(aPNR, aG8U)

Local aArea     := GetArea()
Local lRet      := .T.
Local cAliasG3Q := GetNextAlias()
Local cNumID    := ''
Local cG3QDoc   := ''
Local cBloco    := ''
Local nInicio   := 0
Local nTamanh   := 0
Local aAux      := TURXG8UDET(aG8U, '1', 'G3Q_DOC')

If Len(aAux) > 0 
	cBloco  := aAux[aScan(aAux, {|x| x[1] == 'G8U_BLOCO' })][2]
	nInicio := aAux[aScan(aAux, {|x| x[1] == 'G8U_INICIO'})][2]
	nTamanh := aAux[aScan(aAux, {|x| x[1] == 'G8U_TAMANH'})][2]
	cG3QDoc := TA048GET(aPNR[aScan(aPNR, {|x| x[1] == 'M5'})], cBloco, '', nInicio, nTamanh, '', .T.)
	
	BeginSql Alias cAliasG3Q
		SELECT G3Q_NUMID, G3Q_DOC
		FROM %Table:G3Q% G3Q
		WHERE G3Q_DOC = %Exp:cG3QDoc%
		AND G3Q.%NotDel%
	EndSql

	If (cAliasG3Q)->(!EOF())
		cNumID := (cAliasG3Q)->G3Q_NUMID 
		lRet := .F.
	EndIf
	(cAliasG3Q)->(DbCloseArea())
EndIf

RestArea(aArea)

Return {lRet, cG3QDoc, cNumID}

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048GRVG8V

Fun��o respons�vel por gravar as informa��es da tabela G8V (Monitor de Integra��o PNR)

@sample	TA048GRVG8V()
@param		cPathPNR: String - diret�rio do arquivo que est� sendo integrado
			cFilePNR: String - nome do arquivo que est� sendo integrado
@return 	
@author 	Marcelo Cardoso Barbosa
@since 		27/08/2015
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048GRVG8V(cPathPNR, cFilePNR) 
	
Local cAliasG8V := GetNextAlias()
Local aArea     := GetArea()
Local nLenG8V   := Len(aLogG8VErr)
Local nPosG8V   := 0 
Local nRet      := 0
Local cCodigo   := '0000000000'

If nLenG8V > 0
	BeginSql Alias cAliasG8V
		SELECT MAX(G8V_CODIGO) as G8V_CODIGO
		FROM %Table:G8V% G8V
		WHERE G8V.%NotDel%
	EndSql
	
	If (cAliasG8V)->(!Eof()) .And. !Empty((cAliasG8V)->G8V_CODIGO)    
		cCodigo := (cAliasG8V)->G8V_CODIGO
	EndIf
	(cAliasG8V)->(DbCloseArea())

	DbSelectArea('G8V')
	For nPosG8V := 1 To nLenG8V 
		RecLock('G8V', .T.)
		
		G8V->G8V_FILIAL := aLogG8VErr[nPosG8V][01] 
		G8V->G8V_GDS    := aLogG8VErr[nPosG8V][02] 
		G8V->G8V_VERSAO := aLogG8VErr[nPosG8V][03] 
		G8V->G8V_SEQUEN := aLogG8VErr[nPosG8V][04] 
		G8V->G8V_TIPO   := aLogG8VErr[nPosG8V][05] 
		G8V->G8V_MSG    := aLogG8VErr[nPosG8V][06] 
		G8V->G8V_DIR    := Upper(cPathPNR) 		 
		G8V->G8V_ARQUIV := Upper(cFilePNR)			 
		G8V->G8V_CODUSR := aLogG8VErr[nPosG8V][07] 
		G8V->G8V_DATA   := aLogG8VErr[nPosG8V][08] 
		G8V->G8V_HORA   := aLogG8VErr[nPosG8V][09]  
		G8V->G8V_CODIGO := cCodigo := Soma1(cCodigo) 
		
		If aLogG8VErr[nPosG8V][05] $ '2|3'
			lMsgTipo3  := .T.
		Else
			lMsgTipo1  := .T. 
		EndIf

		MsUnLock()
	Next nPosG8V
	G8V->(DbCloseArea())
EndIf

RestArea(aArea)

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} SchedDef

Retorna os parametros no schedule.

@sample	SchedDef()
@param		
@return 	aReturn Array com os parametros
@author  	Thiago Tavares
@since   	18/05/2016
@version 	12.1.13
/*/
//-------------------------------------------------------------------
Static Function SchedDef()

Local aParam  := {}

aParam := {"P",;			// Tipo R para relatorio P para processo
              ,;			// Pergunte do relatorio, caso nao use passar ParamDef
              ,;			// Alias
              ,;			// Array de ordens
           STR0025}		// Titulo		// "Integra��o PNR Sabre"

Return aParam

//------------------------------------------- FUN��ES CHAMADAS ATRAV�S DO LAYOUT GDS ------------------------------// 
//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048CANCRV

Fun��o respons�vel pelo cancelamento de um IV

@sample	TA048CANCRV(aPNR, aG8U, cVersao)
@param		aPNR: Array - linhas arquivo PNR
			aG8U: Array - dados da tabela Layout GDS
			cVersao: String - vers�o do arquivo PNR  
@return	
@author    Marcelo Cardoso Barbosa
@since     25/11/2015
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048CANCRV(aPNR, aG8U, cVersao)

Local aArea	  := GetArea()
Local nI        := 0
Local cAliasG3Q := GetNextAlias()
Local cFilG3Q   := ''
Local cNumIdRV  := ''
Local cG3QDoc   := '' 
Local cBloco    := ''
Local nInicio   := 0
Local nTamanh   := 0
Local nLenG3Q   := 0 
Local aSetStc   := {}
Local aAux      := TURXG8UDET(aG8U, '2', 'CLOCALIZA' , )

cBloco  := aAux[aScan(aAux, {|x| x[1] == 'G8U_BLOCO' })][2]
nInicio := aAux[aScan(aAux, {|x| x[1] == 'G8U_INICIO'})][2]
nTamanh := aAux[aScan(aAux, {|x| x[1] == 'G8U_TAMANH'})][2]
cG3QDoc := TA048GET(aPNR[aScan(aPNR, {|x| x[1] == 'M0'})], cBloco, '', nInicio, nTamanh, '', .T.)
cG3QDoc := AllTrim(SubStr(cG3QDoc, Len(AllTrim(cG3QDoc)) - 9, Len(AllTrim(cG3QDoc))))

BeginSql Alias cAliasG3Q
	SELECT TOP 1 G3Q_FILIAL, G3Q_NUMID 
	FROM %Table:G3Q% G3Q
	WHERE (G3Q_TPDOC = '2' OR G3Q_TPDOC = '3') AND 
	       G3Q_DOC = %Exp:cG3QDoc% AND 
	       G3Q_OPERAC = '1' AND 
	       G3Q.%NotDel%
EndSql

If (cAliasG3Q)->(!EOF())
	cFilG3Q  := (cAliasG3Q)->G3Q_FILIAL
	cNumIdRV := (cAliasG3Q)->G3Q_NUMID
EndIf
(cAliasG3Q)->(DBCloseArea())

DbSelectArea('G3P')
G3P->(DbSetOrder(1))		// G3P_FILIAL+G3P_NUMID+G3P_SEGNEG
If !Empty(AllTrim(cNumIdRV)) .And. G3P->(DbSeek(cFilG3Q + cNumIdRV)) 
	oModel := FWLoadModel('TURA034')
	oModel:SetOperation(MODEL_OPERATION_UPDATE)
	oModel:Activate()

	aAdd(aSetStc, {'lHierarquia', .F.})
	aAdd(aSetStc, {'nSegmento', Val(G3P->G3P_SEGNEG)}) 
	T034SetStc(aSetStc)
	
	oStrG3Q := oModel:GetModel('G3Q_ITENS')
	nLenG3Q := oStrG3Q:Length()		
			
	If nLenG3Q > 0
		For nI := 1 To nLenG3Q
			oStrG3Q:GoLine(nI)
			If AllTrim(oStrG3Q:GetValue('G3Q_DOC')) == cG3QDoc
				BEGIN SEQUENCE
					If TUR034Canc()					
						If (lRet := oModel:VldData())
							oModel:CommitData()
							TA048G8VLOG(Nil , '1', cVersao, '', '1', STR0019 + cG3QDoc, '', {})  //  'CANCELAMENTO EFETUADO COM SUCESSO'
						Else
							TA048G8VLOG(Nil, '1', cVersao, '', '3', '', '', oModel:GetErrorMessage())
						EndIf
					EndIf
				RECOVER
					lRet  := .F.
				END SEQUENCE
				Exit
			EndIf
		Next nI
	EndIf
	oModel:DeActivate()
Else
	TA048G8VLOG(Nil, '1', cVersao, '', '2', STR0020, '', {})  //  'CANCELAMENTO N�O EFETUADO '                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
EndIf
G3P->(DBCloseArea())

If oModel <> Nil .And. oModel:IsActive(); oModel:DeActivate(); EndIf

RestArea(aArea)

Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048DTEMIS

Fun��o para retornar a data de emis�o do Registro de Venda  

@sample	TA048DTEMIS(cData)
@param		cData: String - data no formato DDMMM enviado no arquivo
@return	dEmis: Date - Data de emiss�o do Registro de Venda
@author    Thiago Tavares
@since     01/06/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048DTEMIS(cData)

Local aMesExt := {'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'}
Local nDia    := Val(SubStr(cData, 01, 02))
Local nMes    := aScan(aMesExt, Upper(SubStr(cData, 03, 03)))
Local nAno    := Year(dDataBase)
Local cDia    := ''
Local cAno    := ''
Local dEmis   := CToD('  /  /  ')

If nMes <> 0 .And. nDia <= 31
	cDia  += StrZero(nDia, 2) + '/'
	cDia  += StrZero(nMes, 2) + '/'
	cAno  := StrZero(nAno, 4) 
	dEmis := CToD(cDia + cAno)

	If dEmis > dDataBase		
		cAno  := StrZero(Year(dDataBase) - 1, 4)
		dEmis := CToD(cDia + cAno)
	EndIf
EndIf
	
Return dEmis

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048SOLIC

Fun��o para retornar o c�digo do solicitante de acordo com o extra�do do arquivo PNR  

@sample	TA048SOLIC(cNome, cCodCli, cLoja)
@param		cNome: String - nome do solicitante 
			cCodCli: String - c�digo do cliente 
			cLoja: String - loja do cliente
@return	cRet: String - c�digo do solicitante encontrado
@author    Thiago Tavares
@since     30/08/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048SOLIC(cNome, cCodCli, cLoja)

Local aArea := GetArea()
Local cRet  := ''

DbSelectArea('SU5')
SU5->(DbSetOrder(2))		// U5_FILIAL+U5_CONTAT
If SU5->(DbSeek(xFilial('SU5') + cNome))

	DbSelectArea('AC8')
	AC8->(DbSetOrder(1))		// AC8_FILIAL+AC8_CODCON+AC8_ENTIDA+AC8_FILENT+AC8_CODENT
	If AC8->(DbSeek(xFilial('AC8') + SU5->(U5_CODCONT + 'SA1' + xFilial('SA1') + cCodCli + cLoja)))
		cRet := SU5->(U5_CODCONT)
	EndIf
	AC8->(DbCloseArea())		

EndIf
SU5->(DbCloseArea())

RestArea(aArea)

Return cRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048FNPRD

Fun��o respons�vel por determinar o fornecedor de reporte do documento de reserva

@sample	TA048FNPRD(cLido)
@param		cLido: String - nome do fornecedor extra�do do arquivo 
@return	lRet: L�gico - verdadeiro se conseguir preencher as informa��es do reporte
@author    Thiago Tavares
@since     22/08/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TA048FNPRD(cLido, cCampo)

Local aArea     := GetArea()
Local cAliasSA2 := GetNextAlias()
Local cRet      := ''
Local cCampoAux := '%' + cCampo + '%'
Local aAux      := StrTokArr2(cLido, ' ', .F.)
Local lContinua := .T.
Local cWhere    := cLido + '%'
Local nX        := 0

If !Empty(cWhere) 
	While lContinua
		BeginSql Alias cAliasSA2
			SELECT %Exp:cCampoAux%, A2_NOME, A2_NREDUZ
			FROM %Table:SA2% SA2
			WHERE (A2_NOME LIKE %Exp:cWhere% OR A2_NREDUZ LIKE %Exp:cWhere%) AND SA2.%NotDel%
		EndSql
	
		While (cAliasSA2)->(!Eof())
			If AllTrim((cAliasSA2)->A2_NOME) $ cLido .Or. AllTrim((cAliasSA2)->A2_NREDUZ) $ cLido 
				cRet := (cAliasSA2)->&(cCampo)
				lContinua := .F.
				Exit	
			EndIf
			(cAliasSA2)->(DbSkip())
		EndDo
		(cAliasSA2)->(DbCloseArea())
		
		If lContinua 
			cWhere := ''
			For nX := 1 To Len(aAux) - 1
				cWhere += aAux[nX] + ' '
			Next nX
			
			If Len((aAux := StrTokArr2((cWhere := AllTrim(cWhere) + '%'), ' ', .F.))) == 1
				lContinua := .F.
			EndIf
		EndIf 
	EndDo
EndIf

RestArea(aArea)

Return cRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048FNRPT

Fun��o respons�vel por determinar o destino do RV

@sample	TA048FNRPT(aPNR, aG8U, oModel)
@param		oModel: Objeto - modelo de dados
@return	lRet: L�gico - verdadeiro se conseguir preencher as informa��es do reporte
@author    Thiago Tavares
@since     10/08/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TA048FNRPT(oModel)

Local aArea    := GetArea()
Local lRet     := .F.
Local lReporte := .T.
Local cProdPNR := ''
Local cCodFor  := oModel:GetModel('G3R_ITENS'):GetValue('G3R_FORNEC')
Local cLoja    := oModel:GetModel('G3R_ITENS'):GetValue('G3R_LOJA')
Local cForRep  := oModel:GetModel('G3R_ITENS'):GetValue('G3R_FORNEC')
Local cLojRep  := oModel:GetModel('G3R_ITENS'):GetValue('G3R_LOJA')
Local cRptForn := SuperGetMv('MV_RPTFORN', .T., '')
Local cRptLoja := SuperGetMv('MV_RPTLOJA', .T., '')

If !Empty(cCodFor) .And. !Empty(cLoja)
	If cPNRProd == 'AIR'
		DbSelectArea('G4S')
		G4S->(DbSetOrder(1))		// G4S_FILIAL+G4S_FORNEC+G4S_LOJA
		If G4S->(DbSeek(xFilial('G4S') + cCodFor + cLoja))
			// verificando se pertence BSP e se o c�digo e loja do BSP foi informado
			If G4S->G4S_BSP == '1' .And. !Empty(cRptForn) .And. !Empty(cRptLoja)
				cForRep := cRptForn
				cLojRep := cRptLoja 
			ElseIf G4S->G4S_BSP == '2' .And. G4S->G4S_REPORT == '2'
				lReporte := .F.
			EndIf
		EndIf
		G4S->(DbCloseArea())
	EndIf

	If lReporte .And. (cPNRProd != 'AIR' .Or. (!Empty(AllTRim(cForRep)) .And. !Empty(AllTrim(cLojRep)) .And. cPNRProd == 'AIR'))
		oModel:GetModel('G3R_ITENS'):SetValue('G3R_FORREP', cForRep)
		oModel:GetModel('G3R_ITENS'):SetValue('G3R_LOJREP', cLojRep)
		lRet := .T.
	EndIf
ElseIf cPNRProd != 'AIR' .And. (Empty(cCodFor) .oR. Empty(cLoja))
	lRet := .T.
EndIf 

RestArea(aArea)

Return lRet
	
//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048GETDST

Fun��o respons�vel por determinar o destino do RV

@sample	TA048GETDST(aPNR, aG8U, oModel)
@param		aPNR: Array - linhas arquivo PNR
			aG8U: Array - dados da tabela Layout GDS
			oModel: Objeto - modelo de dados
@return	cDest: String - retorna o destino do RV (1-Nacional / 2-Internacional)
@author    Thiago Tavares
@since     27/05/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TA048GETDST(aPNR, aG8U, oModel)

Local aArea    := GetArea()
Local oMdlG3P  := Nil
Local oMdlG3R  := Nil
Local cDest    := '1'
Local cProdPNR := ''
Local cOrigem  := ''
Local cDestino := ''
Local cCidIni  := '' 
Local cCidFim  := ''
Local cCodFor  := ''
Local cLoja    := '' 
Local cBloco   := ''
Local cSeoara  := ''
Local nPos     := 0
Local nInicio  := 0
Local nTamanh  := 0
Local nPosica  := 0
Local aOrigem  := {}
Local aDestino := {}
Local aProdPNR := {}

If oModel == Nil
	If Len((aProdPNR := TURXG8UDET(aG8U, '1', 'G3Q_PROD'))) > 0 .And. (nPos := aScan(aPNR, {|x| x[1] == 'M3'})) > 0
		While aPNR[nPos][1] == 'M3'
			cBloco   := aProdPNR[aScan(aProdPNR, {|x| x[1] == 'G8U_BLOCO' })][2]
			nInicio  := aProdPNR[aScan(aProdPNR, {|x| x[1] == 'G8U_INICIO'})][2]
			nTamanh  := aProdPNR[aScan(aProdPNR, {|x| x[1] == 'G8U_TAMANH'})][2]
			aOrigem  := {}
			aDestino := {}
	
			cProdPNR := TA048GET(aPNR[nPos], cBloco, '', nInicio, nTamanh, '', .T.)
			If cProdPNR == 'AIR'
				aOrigem  := TURXG8UDET(aG8U, '1', 'G3T_TERORI')
				aDestino := TURXG8UDET(aG8U, '1', 'G3T_TERDST')
			ElseIf cProdPNR $ 'HHT|HTL|HHL'
				aDestino := TURXG8UDET(aG8U, '1', 'G3U_CIDHOT')
			ElseIf cProdPNR == 'CAR'
				aOrigem  := TURXG8UDET(aG8U, '1', 'G3V_CIDRET')
				aDestino := TURXG8UDET(aG8U, '1', 'G3V_CIDDEV')
			EndIf
			
			// cidade de origem
			If Len(aOrigem) > 0
				cSepara := aOrigem[aScan(aOrigem, {|x| x[1] == 'G8U_SEPARA'})][2]
				nInicio := aOrigem[aScan(aOrigem, {|x| x[1] == 'G8U_INICIO'})][2]
				nTamanh := aOrigem[aScan(aOrigem, {|x| x[1] == 'G8U_TAMANH'})][2]
				nPosica := aOrigem[aScan(aOrigem, {|x| x[1] == 'G8U_POSICA'})][2]
				cOrigem := TA048GET(aPNR[nPos], 'M3', '', nInicio, nTamanh, '', .T.)
				
				If !Empty(cSepara) .And. nPosica > 0
					cOrigem := StrTokArr2(cOrigem, AllTrim(cSepara), .F.)[nPosica]
				EndIf 
				
				If cProdPNR $ 'AIR|HHL' .And. !Empty(cOrigem)
					If Posicione('G3B', 2, xFilial('G3B') + cOrigem, 'G3B_EST') == 'EX'		// G3B_FILIAL+G3B_SIGLA
						cDest := '2'
						Exit
					EndIf
				Else  
					If Posicione('G5S', 3, xFilial('G5S') + cOrigem, 'G5S_SIGLA') == 'EX'
						cDest := '2'
						Exit
					EndIf
				EndIf
			EndIf

			// cidade de destino
			If Len(aDestino) > 0
				nInicio  := aDestino[aScan(aDestino, {|x| x[1] == 'G8U_INICIO'})][2]
				nTamanh  := aDestino[aScan(aDestino, {|x| x[1] == 'G8U_TAMANH'})][2]
				cDestino := TA048GET(aPNR[nPos], 'M3', '', nInicio, nTamanh, '', .T.)
				
				If cProdPNR $ 'AIR|HHL' .And. !Empty(cDestino)  
					If Posicione('G3B', 2, xFilial('G3B') + cDestino, 'G3B_EST') == 'EX'		// G3B_FILIAL+G3B_SIGLA
						cDest := '2'
						Exit
					EndIf
				Else
					If Posicione('G5S', 3, xFilial('G5S') + cDestino, 'G5S_SIGLA') == 'EX'	// G5S_FILIAL+G5S_IATA
						cDest := '2'
						Exit
					EndIf
				EndIf
			EndIf

			nPos++
		EndDo
	EndIf
Else
	// verificando o destino do RV de acordo com o fornecedor do produto
	// s� avaliar se o campo G3P_DESTIN == 1 e o c�digo e a loja do fornecedor estiverem preenchidos
	oMdlG3P := oModel:GetModel('G3P_FIELDS')
	oMdlG3R := oModel:GetModel('G3R_ITENS')
	
	If oMdlG3P:GetValue('G3P_DESTIN') == '1' .And. !Empty((cCodFor := oMdlG3R:GetValue('G3R_FORNEC'))) .And. !Empty((cLoja := oMdlG3R:GetValue('G3R_LOJA')))
		If AllTrim(Posicione('SA2', 1, xFilial('SA2') + cCodFor + cLoja, 'A2_EST')) == 'EX'
			oMdlG3P:LoadValue('G3P_DESTIN') := '2'
		EndIf
	EndIf    
EndIf

RestArea(aArea)

Return cDest

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048OPERAC

Fun��o que retorna se a opera��o ser� uma 1-Emiss�o ou 3-Reemiss�o  

@sample	TA048OPERAC(aPAX)
@param 		aPAX: Array - lista de linhas do passageiro
@return 	Array - opera��o e o n�mero do bilhete  
@author 	Thiago Tavares
@since 		25/05/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048OPERAC(aPAX)

Local cOperac := '1'		// 1-Emiss�o
Local cTpDoc  := '2' 	// 2-Bilhete
Local cG3QDoc := ''
Local aSplit  := {}
Local nPosM5  := 0
Local nPos    := 0  

If (nPosM5 := aScan(aPAX, {|x| x[1] == 'M5'})) > 0
	aSplit := StrTokArr2(aPAX[nPosM5][3], '/', .F.)
	If (nPos := aScan(aSplit, {|x| 'E-@' $ x})) > 0 .Or.; 			// reemiss�o
	   aScan(aSplit, {|x| 'EMD' $ x}) > 0 .Or.;						// EMD
	   (nPos := aScan(aSplit, {|x| 'F-@' $ x})) > 0					// MCO	
		cOperac := '3'	// 3-Reemissao
		If aScan(aSplit, {|x| 'EMD' $ x}) > 0 
			cTpDoc  := '7'	// 7-EMD  
		ElseIf aScan(aSplit, {|x| 'F-@' $ x}) > 0					
			cTpDoc  := '8'	// 8-MCO
		EndIf	
	EndIf
	
	If nPos > 0
		cG3QDoc := SubStr(aSplit[nPos], Len(aSplit[nPos])-9, Len(aSplit[nPos]))
	EndIf
EndIf

Return {cOperac, cG3QDoc, cTpDoc} 

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048MOEDA

Fun��o respons�vel por retornar a Moeda para segmento CAR

@sample	TA048MOEDA(cLido, nTipo)
@param		cLido: String - texto obtido do arquivo PNR
			nTipo: N�merico - Determina o tipo de retorno (1-Sigla da Moeda / 2-Valor / 3-Taxa de Cambio / 4-Valor Extra para carro) 
@return	cRet: String - retorna o c�digo da Moeda
@author    Thiago Tavares
@since     30/05/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TA048MOEDA(cLido, nTipo)

Local cRet    := ''
Local cDescr  := '' 
Local aAux    := StrTokArr2(cLido, '/', .F.)
Local aAux2   := {}
Local nPos    := 0
Local nX      := 0 
Local aMoedas := {}

If nTipo == 4 			

	If (nPos := aScan(aAux, {|x| 'RG-' $ x .Or. 'RQ-' $ x})) > 0 
		aAux2 := StrTokArr2(aAux[nPos], ' ', .F.)
		If (nPos := aScan(aAux2, {|x| 'XH' $ x})) > 0
			cRet := StrTran(aAux2[nPos], 'XH', '')
		EndIf
	EndIf

ElseIf nTipo == 3 .And. cPNRProd $ 'HHT|HTL|CAR' 		
	
	aMoedas := TA048TXMOED()
	cDescr  := Posicione('G5T', 4, xFilial('G5T') + cLido, 'G5T_DESCR')
	If (nPos := aScan(aMoedas, {|x| Upper(x[1]) $ Upper(cDescr)})) > 0
		cRet := IIF(aMoedas[nPos][2] == 0, '1', Str(aMoedas[nPos][2]))
	EndIf

Else			
	If (nPos := aScan(aAux, {|x| 'RG-' $ x .Or. 'RQ-' $ x})) > 0 
		cRet := StrTran(StrTran(aAux[nPos], 'RG-', ''), 'RQ-', '')
		
		If nTipo == 1		
			cRet := SubStr(cRet, 1, 3)
		Else
			cRet := SubStr(cRet, 4, Len(cRet))
		EndIf 
	EndIf
EndIf

Return cRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048TXMOED

Fun��o que retorna as cotacoes e nome das moedas segundo arquivo SM2

@sample	TA048TXMOED()
@param		
@return	aMoedas: Array - com as moedas e as respectivas taxas de cambio
@author    Thiago Tavares
@since     18/08/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048TXMOED()

Local nMoedas := MoedFin()
Local nX      := 0
Local cMoeda	:= ''
Local aMoedas := {}

For nX	:=	1 To nMoedas
	cMoeda	:=	Str(nX, IIF(nX <= 9, 1, 2))
	If !Empty(GetMv("MV_MOEDA" + cMoeda))
		aAdd(aMoedas, {GetMv("MV_MOEDA" + cMoeda), RecMoeda(dDataBase, nX), PesqPict('SM2', 'M2_MOEDA'+ cMoeda)})
	Else
		Exit
	Endif
Next nX

Return aMoedas

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048PAXNM

Fun��o para tratamento do Nome do Passageiro

@sample	TA048PAXNM(cNome, nRetType)
@param		cNome: String - nome obito do arquivo PNR
			nRetType: Num�rico - determina o retorno  
@return	cRetNome: String - valor obtido
@author    Marcelo Cardoso Barbosa
@since     25/11/2015
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TA048PAXNM(cNome, nRetType)

Local aArea      := GetArea()
Local aSplitNome := {}
Local cRetNome   := ''
Local cSplitNom2 := ''
Local nLenTrat   := 0
Local cPrimNome  := ''
Local cTratament := ''
Local nX         := 0

// pegando o nome do titular do cart�o do PNR
If nRetType == 6
	cRetNome   := '' 
	aSplitNome := StrTokArr2(SubStr(StrTokArr2(cNome, '.', .F.)[2], 2, Len(StrTokArr2(cNome, '.', .F.)[2])), ' ', .F.)
	For	nX := 2 To Len(aSplitNome)
		If aScan(aTratam, {|x| x == AllTrim(aSplitNome[nX])}) == 0    
			cRetNome += AllTrim(aSplitNome[nX]) + ' '
		EndIf
	Next nX
	cRetNome += aSplitNome[1]
Else
	aSplitNome := StrTokArr2(cNome, '/', .F.)
	
	If Len(aSplitNome) > 1
	
		//Verifica se algum dos tratamentos est� no final do Primeiro Nome 
		For nX := 1 To Len(aTratam)
			cSplitNom2 := AllTrim(aSplitNome[2])
			nLenTrat   := Len(' ' + aTratam[nX])
	
			If Right(cSplitNom2, nLenTrat) == ' ' + aTratam[nX]
				cPrimNome  := SubStr(cSplitNom2, 1, Len(cSplitNom2) - nLenTrat)
				cTratament := aTratam[nX]
				Exit
			Else
				cPrimNome  := cSplitNom2
				cTratament := ''
			EndIf
		Next nX
	
		Do Case
			Case nRetType == 1 //Tratamento
				cRetNome := Posicione('G6K', 2, xFilial('G6K') + '000005' + cTratament, 'G6K_CDDE')
			
			Case nRetType == 2 //Primeiro Nome
				cRetNome := cPrimNome
		
			Case nRetType == 3 //Ultimo Nome
				cRetNome := AllTrim(aSplitNome[1])
			 			
			Case nRetType == 4 //Nome
				cRetNome := AllTrim(cPrimNome) + ' ' + AllTrim(aSplitNome[1])
		
			Case nRetType == 5 //Nome Invertido
				cRetNome := AllTrim(aSplitNome[1]) + ' | ' + AllTrim(cPrimNome)
		EndCase
	
	EndIf
EndIf

RestArea(aArea)

Return AllTrim(cRetNome)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048DTSAB

Fun��o para retornar a data no formato DD/MM/AAAA a partir do formato DDMMM  

@sample	TA048DTSAB(cData, dEmis)
@param		cData: String - data no formato DDMMM enviado no arquivo
			dEmis: Date - data de emiss�o
@return	dData: Date - data no formato DD/MM/AAAA
@author    Marcelo Cardoso Barbosa
@since     27/08/2015
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048DTSAB(cData, dEmis)
	
Local aMesExt := {'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'}
Local nDia    := Val(SubStr(cData, 01, 02))
Local nMes    := aScan(aMesExt, Upper(SubStr(cData, 03, 03)))
Local nAno    := Year(dEmis)
Local cDia    := ''
Local cRet    := ''

If nMes <> 0 .And. nDia <= 31
	cDia  += StrZero(nDia, 2) + '/'
	cDia  += StrZero(nMes, 2) + '/'
	cRet  := cDia + AllTrim(Str(nAno))
	
	//Tratamento da Virada do Ano
	If CToD(cRet) < dEmis 		
		nAno++
		cRet := cDia + AllTrim(Str(nAno))
	EndIf
EndIf
	
Return cRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048HRSAB

Fun��o para retornar a hora  

@sample	TA048HRSAB(cTexto, cProd, cTag)
@param		cTexto: String - texto extra�do do bloco de onde ser� feita a leitura da hora
			cProd: String - c�digo do produto no arquivo PNR (AIR/HHT/HTL/HHL/CAR)
			cTag: String - tag de onde ser� retirada a hora 
@return	cHora: String - hora no formato HH:MM
@author    Thiago Tavares
@since     01/06/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048HRSAB(cTexto, cProd, cTag)
	
Local cHora := '12:00'
Local nPos  := 0
Local aAux  := StrTokArr2(cTexto, '/', .T.)

If (nPos := aScan(aAux, {|x| cTag $ x})) > 0
	cHora := StrTran(aAux[nPos], cTag, '') 
	cHora := SubStr(cHora, 1, 2) + ':' + SubStr(cHora, 3, 2)
EndIf

Return cHora

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA048FNCAR

Fun��o para retornar o c�digo GDS que ser� utilizado para integrar o fornecedor do produto CARRO   

@sample	TA048FNCAR(cTexto)
@param		cTexto: String - texto extra�do do bloco de onde ser� feita a leitura 
@return	cCodGDS: String - c�digo GDS do fornecedor
@author    Thiago Tavares
@since     01/06/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Static Function TA048FNCAR(cTexto, cAbrev)
	
Local cCodGDS := ''
local nPos    := 0
Local aAux    := StrTokArr2(cTexto, '/', .T.)

If (nPos := aScan(aAux, {|x| 'PUP-' $ x})) > 0
	cCodGDS := StrTran(StrTokArr2(aAux[nPos], ' ', .T.)[1], 'PUP-', '')
	If Empty(Posicione('G8L', 2, xFilial('G8L') + cCodSabre + cCodGDS, 'G8L_FORNEC'))
		cCodGDS := StrTran(StrTokArr2(aAux[nPos + 1], ' ', .T.)[1], 'DC', '')
	EndIf
Else
	cCodGDS := cAbrev + AllTrim(aAux[1])  
EndIf

Return cCodGDS