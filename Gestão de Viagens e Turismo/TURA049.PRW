#INCLUDE "TURA049.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'FWBROWSE.CH'


/*/{Protheus.doc} MenuDef
//Defini��o do Menu
@author Renan Ribeiro Brando
@since 17/01/2017
@version 01

@type function
@sample MenuDef()
@return aRotina array com op��es do menu
/*/
Static Function MenuDef()

Local aRotina := {} 
ADD OPTION aRotina Title STR0001 	Action 'VIEWDEF.TURA049' OPERATION 2 ACCESS 0 // Visualizar
ADD OPTION aRotina Title STR0002 	Action 'VIEWDEF.TURA049' OPERATION 3 ACCESS 0 // Incluir
ADD OPTION aRotina Title STR0003	Action 'VIEWDEF.TURA049' OPERATION 4 ACCESS 0 // Alterar
ADD OPTION aRotina Title STR0004 	Action 'VIEWDEF.TURA049' OPERATION 5 ACCESS 0 // Excluir
ADD OPTION aRotina Title STR0005 	Action 'VIEWDEF.TURA049' OPERATION 8 ACCESS 0 // Imprimir
ADD OPTION aRotina Title STR0006 	Action 'VIEWDEF.TURA049' OPERATION 9 ACCESS 0 // Copiar

Return aRotina



/*/{Protheus.doc} TURA049
// Fun��o TURA049
@author Renan Ribeiro Brando
@since 17/01/2017
@version 01

@type function
/*/
Function TURA049()

Local oBrowse 
oBrowse := FWMBrowse():New()
oBrowse:SetAlias('G9N')
oBrowse:SetDescription(STR0007) // Cadastro de Motoboy
oBrowse:DisableDetails()
oBrowse:Activate()

Return 


/*/{Protheus.doc} ModelDef
//Defini��o do ModelDef
@author Renan Ribeiro Brando
@since 17/01/2017
@version 01

@type function
@sample ModelDef()
@return oModel objeto do Model
/*/
Static Function ModelDef()

Local oStruG9N := FWFormStruct(1, 'G9N')
Local oModel 
oModel := MPFormModel():New('TURA049', /*bPreValid*/, {|oModel| TA049TDOK(oModel)}/*bPosValid*/, /*bCommit*/)
oModel:AddFields('G9NMASTER', /*cOwner*/, oStruG9N)
oModel:SetPrimaryKey({'G9N_FILIAL','G9N_COD'})
oModel:SetDescription(STR0007) // Cadastro de Motoboy
oModel:GetModel( 'G9NMASTER' ):SetDescription(STR0008) // Dados do Motoboy
Return oModel
 
/*/{Protheus.doc} ViewDef
//Defini��o do ViewDef
@author Renan Ribeiro Brando
@since 17/01/2017
@version 01

@type function
@sample ViewDef()
@return oView objeto da View
/*/
Function ViewDef()

Local oModel := FWLoadModel('TURA049')
Local oStruG9N := FWFormStruct(2, 'G9N')
Local oView := FWFormView():New()
oView:SetModel(oModel)
oView:AddField('VIEW_G9N', oStruG9N, 'G9NMASTER')
oView:CreateHorizontalBox('FULL', 100)
oView:SetOwnerView('VIEW_G9N','FULL')
oView:EnableTitleView('VIEW_G9N', STR0007)

Return oView

/*/{Protheus.doc} TA049TDOK
//Valida��o do modelo
@author Thiago Tavares
@since 30/01/2017
@version 01

@type function
@sample TA049TDOK()
@return lRet
/*/
Static Function TA049TDOK(oModel)

Local aArea := GetArea()
Local lRet  := .T.

If oModel:GetOperation() == MODEL_OPERATION_INSERT 
	DbSelectArea('G9N')
	G9N->(DbSetOrder(1))		// G9N_FILIAL+G9N_COD
	If G9N->(DbSeek(xFilial('G9N') + oModel:GetValue('G9NMASTER', 'G9N_COD')))
		oModel:SetErrorMessage(oModel:GetModel():GetId(), , oModel:GetModel():GetId(), , "TA049TDOK", STR0009, STR0010)		// "C�digo inv�lido" 	"O c�digo est� em uso, por favor informe outro valor."
		lRet := .F.
	EndIf
	G9N->(DbCloseArea())
EndIf

RestArea(aArea)

Return lRet