#include "TURA050.CH"
#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FWEDITPANEL.CH"

#DEFINE ARR_DADOS	1
#DEFINE ARR_FILHOS	2
#DEFINE GRIDMAXLIN 99999

Static aTA050Acordo	:= {}
Static cTA50RetF3	:= ""
Static aRecnoG48    :={}
Static aG48Dif	    :={}

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TURA050
Função chamada pelo menu responsável pela manutenção da receita de fornecedores.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Function TURA050()

Local oBrowse 	:= Nil
Local aColG6L	:= {}
Local aColBrw	:= {}
Local nX		:= 0

SetKey (VK_F12,{|a,b| AcessaPerg("TURA050C",.T.)})
oBrowse := FwMBrowse():New()

oBrowse:SetAlias('G6L')
oBrowse:SetDescription(STR0001)		// "Apuração de Receita de Cliente"//"Apuração de Receita de Fornecedor"

oBrowse:AddLegend( "G6L_STATUS == '1'", 'GREEN', STR0002 ) //"Apuração em Aberto"
oBrowse:AddLegend( "G6L_STATUS == '2'", 'RED'  , STR0003 ) //"Apuração Liberada"

oBrowse:SetFilterDefault("G6L_TPAPUR == '3'")

aColG6L := oBrowse:LoadColumns()
For nX := 1 To Len(aColG6L)
	If !aColG6L[nX,12] $ "G6L_CLIENT|G6L_LOJA|G6L_NOME"
		AAdd(aColBrw,aColG6L[nX,12])
	EndIf
Next

oBrowse:SetOnlyFields( aColBrw )

oBrowse:AddButton(STR0005, {|| TA050Gerar(oBrowse),G6L->(DbGoTop()),oBrowse:ExecuteFilter()},,3)//"Gerar"

oBrowse:Activate()

Set Key VK_F12 To

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Função responsável pela criação do modelo de dados.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel   := Nil
Local oStruG6L := FWFormStruct(1, 'G6L', /*bAvalCampo*/,    /*lViewUsado*/)	// APURACAO
Local oStruG6M := FWFormStruct(1, 'G6M', /*bAvalCampo*/, .F./*lViewUsado*/)	// SEGMENTOS APURACAO
Local oStruG6O := FWFormStruct(1, 'G6O', /*bAvalCampo*/, .F./*lViewUsado*/)	// RESUMO APURACAO
Local oStruG48 := FWFormStruct(1, 'G48', /*bAvalCampo*/, .F./*lViewUsado*/)	// ACORDOS APLICADOS
Local oStruG81 := FWFormStruct(1, 'G81', /*bAvalCampo*/, .F./*lViewUsado*/)	// ITENS FINANCEIROS DE APURAÇÃO
Local oStruG3Q := FWFormStruct(1, 'G3Q', /*bAvalCampo*/, .F./*lViewUsado*/)	// I.V
Local oStruG3T := FWFormStruct(1, 'G3T', /*bAvalCampo*/, .F./*lViewUsado*/)	// Passageiros AEREO
Local oStruG3U := FWFormStruct(1, 'G3U', /*bAvalCampo*/, .F./*lViewUsado*/)	// Passageiros HOTEL
Local oStruG3V := FWFormStruct(1, 'G3V', /*bAvalCampo*/, .F./*lViewUsado*/)	// Passageiros VEICULO
Local oStruG3W := FWFormStruct(1, 'G3W', /*bAvalCampo*/, .F./*lViewUsado*/)	// Passageiros RODOV
Local oStruG3X := FWFormStruct(1, 'G3X', /*bAvalCampo*/, .F./*lViewUsado*/)	// Passageiros TREM
Local oStruG3Y := FWFormStruct(1, 'G3Y', /*bAvalCampo*/, .F./*lViewUsado*/)	// Passageiros CRUZEIRO
Local oStruG3Z := FWFormStruct(1, 'G3Z', /*bAvalCampo*/, .F./*lViewUsado*/)	// Passageiros PACOTE
Local oStruG40 := FWFormStruct(1, 'G40', /*bAvalCampo*/, .F./*lViewUsado*/)	// Passageiros TOUR
Local oStruG41 := FWFormStruct(1, 'G41', /*bAvalCampo*/, .F./*lViewUsado*/)	// Passageiros SEGURO
Local oStruG42 := FWFormStruct(1, 'G42', /*bAvalCampo*/, .F./*lViewUsado*/)	// Passageiros VISTO
Local oStruG43 := FWFormStruct(1, 'G43', /*bAvalCampo*/, .F./*lViewUsado*/)	// Passageiros OUTROS

Local aRelation := {}
Local xAux	:= {}


oModel := MPFormModel():New('TURA050', /*bPreValidacao*/, /*bPosValidacao*/, { |oModel| TA050Grv( oModel ) }/*bCommit*/, /*bCancel*/)

oModel:AddFields('G6L_MASTER', /*cOwner*/, oStruG6L )

// Retira a obrigatoriedado dos campos cliente e loja para gerar a apuração caso o fornecedor não tenho seu código de cliente cadastrado
// Ao liberar a apuração foi criada uma validação para verificar o preenchimento desses campos
oStruG6L:SetProperty('G6L_CLIENT', MODEL_FIELD_OBRIGAT, .F.)
oStruG6L:SetProperty('G6L_LOJA',   MODEL_FIELD_OBRIGAT, .F.)

oModel:AddGrid('G6M_DETAIL', 'G6L_MASTER', oStruG6M, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)

aRelation := {	{'G6M_FILIAL', 'xFilial( "G6M" )'},; 
				{'G6M_CODAPU', 'G6L_CODAPU'} }

oModel:SetRelation('G6M_DETAIL', aRelation, "G6M_FILREF+G6M_SEGNEG+G6M_TIPOAC")

oModel:AddGrid('G6O_DETAIL', 'G6M_DETAIL', oStruG6O, {|oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue| TA050G6OLP(oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue)}/*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)

aRelation := {	{'G6O_FILIAL', 'xFilial( "G6O" )'},; 
				{'G6O_CODAPU', 'G6L_CODAPU'},; 
				{'G6O_SEGNEG', 'G6M_SEGNEG' },; 
				{'G6O_TIPOAC', 'G6M_TIPOAC'},;
				{"G6O_FILREF", "G6M_FILREF"}}

oModel:SetRelation('G6O_DETAIL', aRelation, "G6O_FILREF+G6O_SEGNEG+G6O_TIPOAC")

oStruG48:SetProperty('*', MODEL_FIELD_OBRIGAT, .F.)

oStruG81:SetProperty('G81_CONDPG', MODEL_FIELD_OBRIGAT, .F.)

oModel:AddGrid('G48A_DETAIL', 'G6O_DETAIL', oStruG48 , {|oMdl,nLn,cAct,cFld,xVlBef,xVlAft| Ta050VldMrk(oMdl,nLn,cAct,cFld,xVlBef,xVlAft) },/*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)

aRelation := {	{'G48_FILREF', 	'G6O_FILREF'},;
				{'G48_CODAPU', 'G6L_CODAPU'},; 
				{'G48_SEGNEG', 'G6O_SEGNEG'},; 
				{'G48_CLASS', 'G6M_TIPOAC'},; 
				{'G48_CODACD', 'G6O_CODACD'},; 
				{'G48_CODREC', 'G6O_CODREV'}}

oModel:SetRelation('G48A_DETAIL', aRelation, G48->( IndexKey( 3 ) ) )

oModel:AddGrid('G81A_DETAIL', 'G6M_DETAIL', oStruG81 , /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)

aRelation := {	{'G81_FILIAL', 	'G6M_FILIAL'},;
				{'G81_CODAPU', 'G6L_CODAPU'},; 
				{'G81_SEGNEG', 'G6M_SEGNEG'},; 
				{'G81_CLASS', 	'G6M_TIPOAC'},;
				{'G81_FILREF', 	'G6M_FILREF'}} 

oModel:SetRelation('G81A_DETAIL', aRelation, G81->(IndexKey(1)))

oStruG48:SetProperty('G48_PERACD', MODEL_FIELD_WHEN, {|| .T.})  
oStruG48:SetProperty('G48_VLACD' , MODEL_FIELD_WHEN, {|| .T.})  
oStruG48:SetProperty('G48_STATUS', MODEL_FIELD_WHEN, {|| .F.})  
oStruG48:SetProperty('G48_CONTAC', MODEL_FIELD_WHEN, {|| .F.})  
oStruG48:SetProperty('G48_CCUSTO', MODEL_FIELD_WHEN, {|| .F.})  
oStruG48:SetProperty('G48_ITEMCO', MODEL_FIELD_WHEN, {|| .F.})  
oStruG48:SetProperty('G48_CLASVL', MODEL_FIELD_WHEN, {|| .F.})
oStruG48:SetProperty('G48_NATURE', MODEL_FIELD_WHEN, {|| .F.}) 

oModel:GetModel('G6M_DETAIL' ):SetUniqueLine({'G6M_FILIAL', 'G6M_CODAPU', 'G6M_SEGNEG', 'G6M_TIPOAC', 'G6M_FILREF'})
oModel:GetModel('G6O_DETAIL' ):SetUniqueLine({'G6O_FILIAL', 'G6O_CODAPU', 'G6O_SEGNEG', 'G6O_CODACD', 'G6O_CODREV', 'G6O_FILREF'})
oModel:GetModel('G48A_DETAIL'):SetUniqueLine({'G48_FILIAL', 'G48_NUMID' , 'G48_IDITEM', 'G48_NUMSEQ', 'G48_APLICA', 'G48_CODACD'})
oModel:GetModel('G81A_DETAIL'):SetUniqueLine({'G81_FILIAL', 'G81_IDIFA'})

oModel:GetModel('G6M_DETAIL' ):SetOptional(.T.)
oModel:GetModel('G6O_DETAIL' ):SetOptional(.T.)
oModel:GetModel('G48A_DETAIL'):SetOptional(.T.)
oModel:GetModel('G81A_DETAIL'):SetOptional(.T.)

oModel:GetModel('G6M_DETAIL' ):SetNoDeleteLine(.T.)
oModel:GetModel('G48A_DETAIL'):SetNoDeleteLine(.T.)
oModel:GetModel('G81A_DETAIL'):SetNoDeleteLine(.T.)

oModel:GetModel('G6M_DETAIL' ):SetNoInsertLine(.T.)
oModel:GetModel('G6O_DETAIL' ):SetNoInsertLine(.T.)
oModel:GetModel('G48A_DETAIL'):SetNoInsertLine(.T.)
oModel:GetModel('G81A_DETAIL'):SetNoInsertLine(.T.)

oModel:GetModel('G48A_DETAIL'):SetOnlyQuery(.T.) 

// I.V
oModel:AddGrid('G3Q_DETAIL', 'G48A_DETAIL', oStruG3Q , /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)

aRelation := {	{'G3Q_FILIAL', 	'G48_FILIAL'},;//{'G3Q_FILIAL', 'xFilial( "G48" )'} 
				{'G3Q_NUMID',  'G48_NUMID'},; 
				{'G3Q_IDITEM', 'G48_IDITEM'},; 
				{'G3Q_NUMSEQ', 	'G48_NUMSEQ'},;
				{'G3Q_CONINU', 	'""'}}

oModel:SetRelation('G3Q_DETAIL', aRelation, G3Q->( IndexKey( 1 ) ) )
oModel:GetModel('G3Q_DETAIL'):SetOptional(.T.)
oModel:GetModel('G3Q_DETAIL'):SetOnlyQuery(.T.)
oModel:GetModel('G3Q_DETAIL'):SetNoInsertLine(.T.)
oModel:GetModel('G3Q_DETAIL'):SetOnlyView(.T.)


// Passageiros AEREO
oModel:AddGrid('G3T_DETAIL', 'G48A_DETAIL', oStruG3T , /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)

aRelation := {	{'G3T_FILIAL',	'G48_FILIAL'},;//{'G3T_FILIAL', 'xFilial( "G48" )'} 
				{'G3T_NUMID',  'G48_NUMID'},; 
				{'G3T_IDITEM', 'G48_IDITEM'},; 
				{'G3T_NUMSEQ', 	'G48_NUMSEQ'},;
				{'G3T_CONINU', 	'""'}}

oModel:SetRelation('G3T_DETAIL', aRelation, G3T->( IndexKey( 1 ) ) )
oModel:GetModel('G3T_DETAIL'):SetOptional(.T.)
oModel:GetModel('G3T_DETAIL'):SetOnlyQuery(.T.)
oModel:GetModel('G3T_DETAIL'):SetOnlyView(.T.)

// Passageiros HOTEL
oModel:AddGrid('G3U_DETAIL', 'G48A_DETAIL', oStruG3U , /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)

aRelation := {	{'G3U_FILIAL', 	'G48_FILIAL'},;	//{'G3U_FILIAL', 'xFilial( "G48" )'} 
				{'G3U_NUMID',  'G48_NUMID'},; 
				{'G3U_IDITEM', 'G48_IDITEM'},; 
				{'G3U_NUMSEQ', 	'G48_NUMSEQ'},;
				{'G3U_CONINU',	'""'}}

oModel:SetRelation('G3U_DETAIL', aRelation, G3U->( IndexKey( 1 ) ) )
oModel:GetModel('G3U_DETAIL'):SetOptional(.T.)
oModel:GetModel('G3U_DETAIL'):SetOnlyQuery(.T.)
oModel:GetModel('G3U_DETAIL'):SetOnlyView(.T.)

// Passageiros VEICULO
oModel:AddGrid('G3V_DETAIL', 'G48A_DETAIL', oStruG3V , /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)

aRelation := {	{'G3V_FILIAL', 	'G48_FILIAL'},;//{'G3V_FILIAL', 'xFilial( "G48" )'} 
				{'G3V_NUMID',  'G48_NUMID'},; 
				{'G3V_IDITEM', 'G48_IDITEM'},; 
				{'G3V_NUMSEQ', 	'G48_NUMSEQ'},;
				{'G3V_CONINU', 	'""'}}

oModel:SetRelation('G3V_DETAIL', aRelation, G3V->( IndexKey( 1 ) ) )
oModel:GetModel('G3V_DETAIL'):SetOptional(.T.)
oModel:GetModel('G3V_DETAIL'):SetOnlyQuery(.T.)
oModel:GetModel('G3V_DETAIL'):SetOnlyView(.T.)

// Passageiros RODOV
oModel:AddGrid('G3W_DETAIL', 'G48A_DETAIL', oStruG3W , /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)

aRelation := {	{'G3W_FILIAL', 	'G48_FILIAL'},;//{'G3W_FILIAL', 'xFilial( "G48" )'} 
				{'G3W_NUMID',  'G48_NUMID'},; 
				{'G3W_IDITEM', 'G48_IDITEM'},; 
				{'G3W_NUMSEQ', 	'G48_NUMSEQ'},;
				{'G3W_CONINU', 	'""'}}

oModel:SetRelation('G3W_DETAIL', aRelation, G3W->( IndexKey( 1 ) ) )
oModel:GetModel('G3W_DETAIL'):SetOptional(.T.)
oModel:GetModel('G3W_DETAIL'):SetOnlyQuery(.T.)
oModel:GetModel('G3W_DETAIL'):SetOnlyView(.T.)

// Passageiros TREM
oModel:AddGrid('G3X_DETAIL', 'G48A_DETAIL', oStruG3X , /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)

aRelation := {	{'G3X_FILIAL', 	'G48_FILIAL'},;//{'G3X_FILIAL', 'xFilial( "G48" )'} 
				{'G3X_NUMID',  'G48_NUMID'},; 
				{'G3X_IDITEM', 'G48_IDITEM'},; 
				{'G3X_NUMSEQ', 	'G48_NUMSEQ'},;
				{'G3X_CONINU', 	'""'}}

oModel:SetRelation('G3X_DETAIL', aRelation, G3X->( IndexKey( 1 ) ) )
oModel:GetModel('G3X_DETAIL'):SetOptional(.T.)
oModel:GetModel('G3X_DETAIL'):SetOnlyQuery(.T.)
oModel:GetModel('G3X_DETAIL'):SetOnlyView(.T.)

// Passageiros CRUZEIRO
oModel:AddGrid('G3Y_DETAIL', 'G48A_DETAIL', oStruG3Y , /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)

aRelation := {	{'G3Y_FILIAL', 	'G48_FILIAL'},;//{'G3Y_FILIAL', 'xFilial( "G48" )'} 
				{'G3Y_NUMID',  'G48_NUMID'},; 
				{'G3Y_IDITEM', 'G48_IDITEM'},; 
				{'G3Y_NUMSEQ', 	'G48_NUMSEQ'},;
				{'G3Y_CONINU', 	'""'}}

oModel:SetRelation('G3Y_DETAIL', aRelation, G3Y->( IndexKey( 1 ) ) )
oModel:GetModel('G3Y_DETAIL'):SetOptional(.T.)
oModel:GetModel('G3Y_DETAIL'):SetOnlyQuery(.T.)
oModel:GetModel('G3Y_DETAIL'):SetOnlyView(.T.)

// Passageiros PACOTE
oModel:AddGrid('G3Z_DETAIL', 'G48A_DETAIL', oStruG3Z , /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)

aRelation := {	{'G3Z_FILIAL', 	'G48_FILIAL'},;//{'G3Z_FILIAL', 'xFilial( "G48" )'} 
				{'G3Z_NUMID',  'G48_NUMID'},; 
				{'G3Z_IDITEM', 'G48_IDITEM'},; 
				{'G3Z_NUMSEQ', 	'G48_NUMSEQ'},;
				{'G3Z_CONINU', 	'""'}}

oModel:SetRelation('G3Z_DETAIL', aRelation, G3Z->( IndexKey( 1 ) ) )
oModel:GetModel('G3Z_DETAIL'):SetOptional(.T.)
oModel:GetModel('G3Z_DETAIL'):SetOnlyQuery(.T.)
oModel:GetModel('G3Z_DETAIL'):SetOnlyView(.T.)

// Passageiros TOUR
oModel:AddGrid('G40_DETAIL', 'G48A_DETAIL', oStruG40 , /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)

aRelation := {	{'G40_FILIAL', 	'G48_FILIAL'},;//{'G40_FILIAL', 'xFilial( "G48" )'} 
				{'G40_NUMID',  'G48_NUMID'},; 
				{'G40_IDITEM', 'G48_IDITEM'},; 
				{'G40_NUMSEQ', 	'G48_NUMSEQ'},;
				{'G40_CONINU',	'""'}}

oModel:SetRelation('G40_DETAIL', aRelation, G40->( IndexKey( 1 ) ) )
oModel:GetModel('G40_DETAIL'):SetOptional(.T.)
oModel:GetModel('G40_DETAIL'):SetOnlyQuery(.T.)
oModel:GetModel('G40_DETAIL'):SetOnlyView(.T.)

// Passageiros SEGURO
oModel:AddGrid('G41_DETAIL', 'G48A_DETAIL', oStruG41 , /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)

aRelation := {	{'G41_FILIAL', 	'G48_FILIAL'},;//{'G41_FILIAL', 'xFilial( "G48" )'} 
				{'G41_NUMID',  'G48_NUMID'},; 
				{'G41_IDITEM', 'G48_IDITEM'},; 
				{'G41_NUMSEQ', 	'G48_NUMSEQ'},;
				{'G41_CONINU',	'""'}}

oModel:SetRelation('G41_DETAIL', aRelation, G41->( IndexKey( 1 ) ) )
oModel:GetModel('G41_DETAIL'):SetOptional(.T.)
oModel:GetModel('G41_DETAIL'):SetOnlyQuery(.T.)
oModel:GetModel('G41_DETAIL'):SetOnlyView(.T.)

// Passageiros VISTO
oModel:AddGrid('G42_DETAIL', 'G48A_DETAIL', oStruG42 , /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)

aRelation := {	{'G42_FILIAL', 	'G48_FILIAL'},;//{'G42_FILIAL', 'xFilial( "G48" )'} 
				{'G42_NUMID',  'G48_NUMID'},; 
				{'G42_IDITEM', 'G48_IDITEM'},; 
				{'G42_NUMSEQ', 	'G48_NUMSEQ'},;
				{'G42_CONINU',	'""'}}

oModel:SetRelation('G42_DETAIL', aRelation, G42->( IndexKey( 1 ) ) )
oModel:GetModel('G42_DETAIL'):SetOptional(.T.)
oModel:GetModel('G42_DETAIL'):SetOnlyQuery(.T.)
oModel:GetModel('G42_DETAIL'):SetOnlyView(.T.)

// Passageiros OUTROS
oModel:AddGrid('G43_DETAIL', 'G48A_DETAIL', oStruG43 , /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)

aRelation := {	{'G43_FILIAL', 	'G48_FILIAL'},;//{'G43_FILIAL', 'xFilial( "G48" )'} 
				{'G43_NUMID',  'G48_NUMID'},; 
				{'G43_IDITEM', 'G48_IDITEM'},; 
				{'G43_NUMSEQ', 	'G48_NUMSEQ'},;
				{'G43_CONINU',	'""'}}

oModel:SetRelation('G43_DETAIL', aRelation, G43->( IndexKey( 1 ) ) )
oModel:GetModel('G43_DETAIL'):SetOptional(.T.)
oModel:GetModel('G43_DETAIL'):SetOnlyQuery(.T.)
oModel:GetModel('G43_DETAIL'):SetOnlyView(.T.)

// Define o número máximo de linhas que o model poderá receber, de acordo com a define GRIDMAXLIN.
oModel:GetModel( 'G48A_DETAIL' ):SetMaxLine(GRIDMAXLIN)

oModel:SetActivate()
oModel:SetVldActivate( { |oModel| TA050VLDAC( oModel ) } )

oModel:SetDescription(STR0001)

Return oModel


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Função responsável pela montagem do menu

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE STR0004 ACTION 'VIEWDEF.TURA050' 	OPERATION 2	ACCESS 0//"Visualizar"
ADD OPTION aRotina TITLE STR0006 ACTION 'TA050Alterar'    	OPERATION 4	ACCESS 0//"Alterar"
ADD OPTION aRotina TITLE STR0007 ACTION 'VIEWDEF.TURA050' 	OPERATION 5	ACCESS 0//"Excluir"
ADD OPTION aRotina TITLE STR0008 ACTION 'TURXLibApura'		OPERATION 4	ACCESS 0//"Liberar"
ADD OPTION aRotina TITLE STR0009 ACTION 'TURXEstApura'		OPERATION 4	ACCESS 0//"Estornar"
ADD OPTION aRotina TITLE STR0010 ACTION 'TA050Report'     	OPERATION 6	ACCESS 0//"Demonstrativo"

Return(aRotina)

/*+----------------------------------------------------------------------------------------
/{Protheus.doc} TA050Gerar
Função responsável pela tela de parâmetros e tipos de acordos para geração da apuração.

@Params		
@return		Nil
@sample		TA050Gerar()	
@author 	Fernando Radu Muscalu
@since 		18/03/2016

//+----------------------------------------------------------------------------------------*/
Function TA050Gerar()

//Apresenta a tela de Parâmetros
If ( Pergunte(Padr("TUR50A",10)) )
	If TurMultFiliais()
		If ( mv_par10 == 1 .or. mv_par11 == 1 .or. mv_par12 == 1 )
			TURA050APU()
		Else
			Help( , , "TA050Gerar", , STR0013, 1, 0)//"Escolha pelo menos um segmento de negócio."
		Endif
	Endif
EndIf

Return

/*+----------------------------------------------------------------------------------------
/{Protheus.doc} TURA050APU
Função responsável pelo processamento da apuração de receitas

@Params
	cGrpProd:	Caractere. Código do Grupo de Produtos 
	cTipoDest:	Caractere. Tipo de Destino (1=Nacional; 2=Internacional; 3=Ambos)  
	cFornecIni:	Caractere. Código de fornecedor inicial
	cLojaIni:	Caractere. Loja de fornecedor inicial
	cFornecFim: Caractere. Código de fornecedor final
	cLojaFim:	Caracter. Loja de fornecedor final 
	dApurarDe:	Data. Apura Itens de Registros de Venda a partir desta data
	dApurarAte: Data. Apura Itens de Registros de Venda até esta data 
	lSegCorp:	Lógico. Considera Segmento Corporativo (.t. sim; .f. não) 
	lSegEven:	Lógico. Considera Segmento de Eventos (.t. sim; .f. não) 
	lSegLazer:	Lógico. Considera Segmento de Lazer (.t. sim; .f. não)
	lGerDemons: Lógico. Deve gerar o relatório de demonstrativo? (.t. sim. .f. não)
	lEnvMail:	Lógico. Enviará e-mail para o fornecedor? (.t. sim; .f. não)
	cEmailRem:	Caracter. Endereço de e-mail do remetente
	lAglutina:	Lógico. Efetua a quebra do demonstrativo somente por Fornecedor(.t.) ou por Fornecedor+Loja (.f.)
	lComplFor:	Lógico. Utiliza o cadastro de complemento de Fornecedor? (.t. sim; .f. não)  
			
@return		Nil
@sample		TA050Proc()	
@author 	Fernando Radu Muscalu
@since 		18/03/2016

//+----------------------------------------------------------------------------------------*/
Static Function TURA050APU(cGrpProd, cTipoDest, cFornecIni, cLojaIni, cFornecFim, cLojaFim,; 
							dApurarDe, dApurarAte, lSegCorp, lSegEven, lSegLazer, lGerDemons,;
							lEnviaMail, cEmailRem, lAglutina, lComplFor, nTipoData, lDelAnt)
							
Local cFilQuebra    := ""
Local cFilRef		:= ""
Local cAliasTrb		:= ""
Local cApur        	:= ""
Local cCodFornec	:= ""
Local clojaFornec	:= ""
Local cNReduz       := ""
Local cMoeda		:= ""
Local cSegmento		:= ""
Local cTpAcordo		:= ""
Local cCodAcordo	:= ""
Local cRevAcordo	:= ""
Local cDiretorio	:= GetMv("MV_DIRAPU",,"c:\totvs\")
Local aDestinos     := {{'1', {'1'}}, {'2', {'2'}}, {'3', {'1', '2'}}} 
Local nPodDest      := 0
Local nX            := 0
Local cFilBkp       := cFilAnt 
Local aApura       	:= {}
Local aApurados    	:= {}
Local aFornDeAte	:= {}
Local aDadosG6M		:= {}
Local aDadosG6O		:= {}
Local aAuxG48		:= {}
Local aCliente		:= {}
Local lApurou       := .F.
Local lApura       	:= .F.
Local lQuebra		:= .F.
Local lQuebraG6M 	:= .F.
Local lQuebraG6O	:= .F.
Local lContinue     := .F.

Default cGrpProd	:= mv_par01
Default cTipoDest	:= cValToChar(mv_par02)
Default cFornecIni	:= mv_par03
Default cLojaIni	:= mv_par04
Default cFornecFim	:= mv_par05
Default cLojaFim	:= mv_par06
Default dApurarDe	:= mv_par08
Default dApurarAte	:= mv_par09
Default lSegCorp	:= mv_par10 == 1
Default lSegEven	:= mv_par11 == 1
Default lSegLazer	:= mv_par12 == 1
Default lGerDemons	:= mv_par13 == 1
Default lEnviaMail	:= mv_par14 == 1 
Default cEmailRem	:= Lower(Alltrim(mv_par15))
Default lAglutina	:= mv_par16 == 1
Default lComplFor	:= mv_par17 == 1
Default nTipoData	:= mv_par07
Default lDelAnt		:= mv_par18 == 1

aAdd(aApurados, {xFilial('G6L')})
aAdd(aApurados[Len(aApurados)], {})

If !Empty(mv_par19)
	cDiretorio	:= Alltrim(mv_par19)
Endif

//Carrega os tipos de acordo de fornecedores - todos
If mv_par20 == 2 
	lContinue := Len(TA050GetTpAcordo("F05")) > 0
Else
	lContinue := TA050TpAco()
EndIf

If lContinue
	If !Empty(AllTrim(cFornecFim)) .And. !Empty(AllTrim(cLojaFim)) 
		aFornDeAte := {{cFornecIni,cLojaIni},{cFornecFim,cLojaFim}}
	Else
		aFornDeAte := {{cFornecIni,cLojaIni},{Replicate('Z', TamSx3('A2_COD')[1]), Replicate('Z', TamSx3('A2_LOJA')[1])}}
	EndIf
	
	//PCDEF-101012 - Apuração de Fornecedor - Nova apuração deve ser somada a antiga apuração que esteja em aberto.
	If lDelAnt
		TA050DELAPU(aFornDeAte, dApurarDe, dApurarAte, cGrpProd, cTipoDest)
	EndIf
	
	If (nPosDest := aScan(aDestinos, {|x| x[1] == cTipoDest})) > 0
		For nX := 1 To Len(aDestinos[nPosDest][2])
			
			//Carrega os dados em um ResultSet
			Processa({|| cAliasTrb := TA050Query(cGrpProd, aDestinos[nPosDest][2][nX], aFornDeAte, aTA050Acordo, dApurarDe, dApurarAte, lSegCorp, lSegEven, lSegLazer, nTipoData)}, STR0011, STR0111)		// "Aguarde"	"Selecionando os Fornecedores e os respectivos acordos a serem apurados..."	
			 
			If ( !Empty(cAliasTrb) )
				
				lQuebra 	:= .t.
				lQuebraG6M	:= .t.
				lQuebraG6O	:= .t.
				
				While ( (cAliasTrb)->(!Eof()) )
				
					//Compoe as informacoes que alimentarão a tabela G6L
					If ( lQuebra ) 
	
						cFilAnt   := (cAliasTrb)->(G48_FILREF)
						aApura    := Array(2)
						
						aApura[ARR_DADOS]	:= (cAliasTrb)->(TA050ArG6L(G4C_CODIGO, G4C_LOJA, G4C_MOEDA, dApurarDe, dApurarAte, cGrpProd, aDestinos[nPosDest][2][nX]))
						aApura[ARR_FILHOS]	:= {}
					
					Endif	
					
					//Compoe as informações que alimentarão a tabela G6M
					If ( lQuebraG6M )
						
						aDadosG6M	:= Array(2)
						
						aDadosG6M[ARR_DADOS]	:= TA050ArG6M((cAliasTrb)->G4C_SEGNEG,(cAliasTrb)->G4C_CLASS, (cAliasTrb)->G4W_TPACO, (cAliasTrb)->G4C_CODIGO, (cAliasTrb)->G4C_LOJA, (cAliasTrb)->G4C_CODPRO,(cAliasTrb)->G48_FILREF)
						aDadosG6M[ARR_FILHOS]	:= {}
									
					Endif
					
					//Compoe as informacoes que alimentarão a tabela G6O
					If ( lQuebraG6O )
						
						aDadosG6O := Array(2)
						
						aDadosG6O[ARR_DADOS]	:= (cAliasTrb)->(TA050ArG6O(G4W_CODACO, G4W_CODREV, G4W_DESCRI, G4W_TPVAL, G48_FILREF, G4W_VALOR)) 
						aDadosG6O[ARR_FILHOS]	:= {}
						
					Endif
					
					//a estrutura dos itens de G48, deverá ser um array equivalente ao adadosG6o
					
					//Compoe as informações que atualização a tabela G48
					aAdd(aAuxG48, {'G48_NUMID' , (cAliasTrb)->G48_NUMID})
					aAdd(aAuxG48, {'G48_IDITEM', (cAliasTrb)->G48_IDITEM})
					aAdd(aAuxG48, {'G48_NUMSEQ', (cAliasTrb)->G48_NUMSEQ})
					aAdd(aAuxG48, {'G48_APLICA', (cAliasTrb)->G48_APLICA})
					aAdd(aAuxG48, {'G48_CODACD', (cAliasTrb)->G4W_CODACO})
					aAdd(aAuxG48, {'G48_CODREC', (cAliasTrb)->G4W_CODREV})
					aAdd(aAuxG48, {'G48_CLASS' , (cAliasTrb)->G4C_CLASS})
					aAdd(aAuxG48, {'G48_VLBASE', (cAliasTrb)->G48_VLBASE})
					aAdd(aAuxG48, {'G48_SEGNEG', (cAliasTrb)->G4C_SEGNEG})
					aAdd(aAuxG48, {'G48_MARK'  , .T.})
					aAdd(aAuxG48, {'G3P_RECNO' , (cAliasTrb)->G3P_RECNO})
					aAdd(aAuxG48, {'G3R_RECNO' , (cAliasTrb)->G3R_RECNO})
					aAdd(aAuxG48, {'G48_RECNO' , (cAliasTrb)->G48_RECNO})
					aAdd(aAuxG48, {'G48_VLACD' , (cAliasTrb)->G48_VLACD})
					aAdd(aAuxG48, {'G48_TPACD' , (cAliasTrb)->G48_TPACD})
					aAdd(aAuxG48, {'G3R_VLRSER', (cAliasTrb)->G3R_VLRSER})
					
					aAdd(aDadosG6O[ARR_FILHOS],aClone(aAuxG48))
					
					aAuxG48 := {}
					cFilQuebra  := xFilial("G6L", (cAliasTrb)->G48_FILREF)
					cFilRef		:= (cAliasTrb)->G48_FILREF
					cCodFornec	:= (cAliasTrb)->G4C_CODIGO
					clojaFornec	:= (cAliasTrb)->G4C_LOJA
					cNReduz     := Posicione('SA2', 1, xFilial('SA2') + (cAliasTrb)->(G4C_CODIGO + G4C_LOJA), 'A2_NREDUZ')
					cMoeda		:= (cAliasTrb)->G4C_MOEDA
					cSegmento	:= (cAliasTrb)->G4C_SEGNEG
					cTpAcordo	:= (cAliasTrb)->G4C_CLASS
					cCodAcordo	:= (cAliasTrb)->G4W_CODACO
					cRevAcordo	:= (cAliasTrb)->G4W_CODREV
					
					(cAliasTrb)->(DbSkip())
					
					//Verifica se há quebra - então será feita a composição para um novo fornecedor
					lQuebra		:= Alltrim(cFilQuebra+cCodFornec+cLojaFornec+cMoeda) <> Alltrim(xFilial("G6L", (cAliasTrb)->G48_FILREF)+(cAliasTrb)->(G4C_CODIGO+G4C_LOJA+G4C_MOEDA))
					lQuebraG6M	:= lQuebra .Or. Alltrim(cSegmento+cTpAcordo+cFilRef) <> Alltrim((cAliasTRB)->(G4C_SEGNEG+G4C_CLASS+G48_FILREF))
					lQuebraG6O	:= lQuebraG6M .Or. Alltrim(cCodAcordo+cRevAcordo) <> Alltrim((cAliasTrb)->(G4W_CODACO+G4W_CODREV))
					
					If ( lQuebraG6O )
						AAdd(aDadosG6M[ARR_FILHOS],aClone(aDadosG6O))
						aDadosG6O := {}
					Endif
					
					If ( lQuebraG6M )
						aAdd(aApura[ARR_FILHOS],aDadosG6M)
						aDadosG6M := {} 	
					Endif
					
					If ( lQuebra )
						Processa({|| lApura := TA050ExApu(@cApur, aApura)}, STR0011, I18N(STR0012, {cNReduz}))		// "Aguarde"	"Gerando apuração para o fornecedor #1"	
						If ( lApura .And. lGerDemons )
							lApurou := .T.
	
							If (nPos := aScan(aApurados,{|x| x[1] == xFilial('G6L')})) > 0
								aAdd(aApurados[nPos][2], cApur)
							Else
								aAdd(aApurados, {xFilial('G6L')})
								aAdd(aApurados[Len(aApurados)], {})
								aAdd(aApurados[Len(aApurados)][2], cApur)
							EndIf
	
						ElseIf lApura
							lApurou := .T.
						EndIf
					Endif
				EndDo
			Endif
		Next nX
	
		If lApurou .And. Len(aApurados[1]) > 0 .And. lGerDemons
			For nX := 1 To Len(aApurados)
				If !Empty(aApurados[nX][1]) .And. Len(aApurados[nX][2]) >= 1
					If ( FindFunction("U_TURR050A") )
						U_TURR050A(aApurados[nX][2][1], aApurados[nX][2][Len(aApurados[nX][2])], aFornDeAte[1][1], aFornDeAte[1][2], aFornDeAte[2][1], aFornDeAte[2][2], lAglutina, lEnviaMail, cEmailRem, cDiretorio, lComplFor, aApurados[nX][1])
					Else
						TURR050(aApurados[nX][2][1], aApurados[nX][2][Len(aApurados[nX][2])], aFornDeAte[1][1], aFornDeAte[1][2], aFornDeAte[2][1], aFornDeAte[2][2], lAglutina, lEnviaMail, cEmailRem, cDiretorio, lComplFor, .T., aApurados[nX][1])
					EndIf
				EndIf
			Next nX
		EndIf
	EndIf
EndIf

If !lApurou
	Help( , , "TURA050", , STR0014, 1, 0)//"Não há fornecedores para os parâmetros informados."
Else
	FwAlertInfo(STR0015 + Alltrim(cDiretorio), STR0106) 		// "Processo finalizado com sucesso. Verifique se foi(ram) gerado(s) o(s) arquivo(s) de demonstrativo de cobrança. Veja o diretório "	//"Atenção"	
EndIf

cTA50RetF3   := Nil
aTpAcordo    := Nil
aApura       := Nil
aApurados    := Nil
aFornDeAte   := Nil
aDadosG6M    := Nil
aDadosG6O    := Nil
aAuxG48      := Nil
aCliente     := Nil
aDelAnt      := Nil
DelClassIntF()

cFilAnt := cFilBkp

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050G48Up
Função responsável pela alteração dos acordos aplicados através do registro de venda (TURA034).

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050G48Up(nOperation)

Local aArea     := GetArea()
Local aAreaG4C	:= G4C->( GetArea() )
Local aSaveRows := FwSaveRows()
Local nX        := 0

If nOperation == MODEL_OPERATION_INSERT

	G4C->( dbSetOrder(1) )

	For nX := 1 to Len(aRecnoG48) 
		
		G48->(DbGoTo(aRecnoG48[nX][1]))
		
		If G48->(!EOF())
			RecLock('G48', .F.)
			G48->G48_MARK   := .T.
			G48->G48_FILAPU := xFilial("G6L")
			G48->G48_CODAPU := aRecnoG48[nX][2]
			G48->(MsUnLock())
		EndIf

		If G4C->( dbSeek( G48->( G48_FILIAL+G48_NUMID+G48_IDITEM+G48_NUMSEQ+G48_APLICA+G48_CODACD ) ) )
			While G4C->(!Eof()) .And. G48->(G48_FILIAL+G48_NUMID+G48_IDITEM+G48_NUMSEQ+G48_APLICA+G48_CODACD) == ;
			                          G4C->(G4C_FILIAL+G4C_NUMID+G4C_IDITEM+G4C_NUMSEQ+G4C_APLICA+G4C_NUMACD)
				If Empty(G4C->G4C_CONINU) .And. G4C->G4C_CLIFOR == '2'
					RecLock('G4C', .F.)
					G4C->G4C_CODAPU := aRecnoG48[nX][2]
					G4C->( MsUnlock() )
				EndIf
				G4C->(DbSkip())
			EndDo
		EndIf

	Next nX

EndIf

RestArea(aArea)
FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050ExApu
Função responsável pela criação da apuração através do modelo de dados.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050ExApu(cApur, aApur)

Local oModel050  := FWLoadModel('TURA050')
Local oModelG6L  := oModel050:GetModel('G6L_MASTER')

Local nDetalhe   := 0
Local nLinhaG6O  := 1
Local nLinhaG6P  := 1
Local nLinhaG6M  := 0
Local nG6O       := 0

Local aG6LDados  := AClone(aApur[ARR_DADOS])
Local aG6LFilhos := AClone(aApur[ARR_FILHOS])
Local aRecDados  := {}
Local aRecFilhos := {}
Local aG6ODados  := {}
Local aG6OFilhos := {}
Local aDetalhes  := {}

Local cTipoAc    := ''

Local lRet       := .T.
aRecnoG48			:={}
oModel050:GetModel('G6M_DETAIL'):SetNoDeleteLine(.F.)
oModel050:SetOperation(MODEL_OPERATION_INSERT)
oModel050:Activate()

//+------------------------------------------------------
//|	Apuração de Receita Cliente - G6L
//+------------------------------------------------------
lRet := TA050SetVl(@oModel050, 'G6L_MASTER', aG6LDados)	// G6L

If ( lRet )

	For nLinhaG6M := 1 to Len(aG6LFilhos)
		
		//+------------------------------------------------------
		//|	Tipos de Apuração - G6M
		//+------------------------------------------------------
		aRecDados 	:= AClone(aG6LFilhos[nLinhaG6M][ARR_DADOS])
		aRecFilhos	:= AClone(aG6LFilhos[nLinhaG6M][ARR_FILHOS])
		
		If AScan(aRecDados,{|x, y| x[1] == 'G6M_TIPOAC'}) > 0
			cTipoAc := aRecDados[AScan(aRecDados,{|x, y| x[1] == 'G6M_TIPOAC'})][2]
		EndIf
		
		lRet := TA050SetVl(@oModel050, 'G6M_DETAIL', aRecDados, nLinhaG6M > 1) 	// G6M
		
		If ( lRet )
			
			For nG6O := 1 to Len(aRecFilhos)
			
				//+------------------------------------------------------
				//|	Acordos da Apuração - G6O
				//+------------------------------------------------------
				
				aG6ODados  := AClone(aRecFilhos[nG6O][ARR_DADOS])
				aG6OFilhos := AClone(aRecFilhos[nG6O][ARR_FILHOS])
			
				cTipoAc    := aRecDados[AScan(aRecDados,{|x, y| x[1] == 'G6M_TIPOAC'})][2]
		
				lRet := TA050SetVl(@oModel050, 'G6O_DETAIL', aG6ODados, nG6O > 1)	// G6O
				
				If ( lRet )
					
					For nDetalhe := 1 To Len(aG6OFilhos)
		
						aDetalhes := AClone(aG6OFilhos[nDetalhe])
						
						AADD(aRecnoG48,{aDetalhes[AScan(aDetalhes,{|x, y| x[1] == 'G48_RECNO'})][2],oModelG6L:GetValue('G6L_CODAPU')})
						
						TA050XCP32(oModel050, aDetalhes)				
						TA050XCP33(oModel050, aDetalhes)				
			
					Next nDetalhe
				
				Else
					Exit	
				Endif
				
			Next nG6O
			
		Endif
		
		If ( !lRet )
			Exit
		Endif
						
	Next nLinhaG6M

	If ( lRet )
			
		cApur := oModelG6L:GetValue('G6L_CODAPU')
	
		If ( lRet := oModel050:VldData() )
			oModel050:CommitData()
		Else
			TA050GetEr(oModel050)
			RollBackSX8()
		EndIf
	
	Else
		TA050GetEr(oModel050)
		RollBackSX8()
	Endif
		
Endif

oModel050:DeActivate()	
oModel050:Destroy()

aG6LDados  := Nil
aG6LFilhos := Nil
aRecDados  := Nil
aRecFilhos := Nil
aG6ODados  := Nil
aG6OFilhos := Nil
aDetalhes  := Nil
DelClassIntF()
	
Return(lRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050SetVl
Função responsável pela atualização dos valores do model.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050SetVl(oModel050, cIdModel, aDados, lLinha)

Local lRet     := .T.
Local nX       := 0
Local nPos     := 0
Local oMdl     := oModel050:GetModel(cIdModel)
Local oStru    := oMdl:GetStruct()
Local aStru    := oStru:GetFields()
Local cClasse  := ''

Default lLinha := .F.

If (cClasse := oMdl:ClassName()) == 'FWFORMGRID'
	oMdl:SetNoInsertLine(.F.)
EndIf

If lLinha
	oMdl:AddLine()
EndIf

For nX := 1 To Len( aDados )
	
	//+------------------------------------------------------
	//|	Verifica se os campos passados existem na estrutura do modelo
	//+------------------------------------------------------
	If (nPos := aScan(aStru, {|x| AllTrim(x[3])== AllTrim(aDados[nX][1])})) > 0
	
		If !(lRet := oModel050:SetValue(cIdModel, aStru[nPos][3], aDados[nX][2]))
			MsgAlert(STR0017 + aStru[nPos][3]) // STR0017//"Erro ao atribuir valor "
			Exit
		EndIf
	
	EndIf

Next nX

If (cClasse := oMdl:ClassName()) == 'FWFORMGRID'
	oMdl:SetNoInsertLine(.T.)
EndIf

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050ArG6L
Função responsável pela montagem do array com os dados a serem gravados na G6L.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050ArG6L(cFornecedor, cLojaFornec, cMoeda, dDtIni, dDtFim, cGrpProd, cTipoDest)

Local aRet  	:= {}
Local aSA2		:= {}

Local cNome 	:= ""
Local cCodCli	:= ""
Local cLojaCli	:= ""

aSA2 := ClienteFor(cFornecedor,cLojaFornec)

If ( Len(aSA2) > 0 )
	
	cNome 		:= aSA2[1]
	cCodCli		:= aSA2[2]
	cLojaCli	:= aSA2[3]
	
Endif

aAdd(aRet, {'G6L_TPAPUR', '3'		})		// 1=Clientes;2=Meta;3=Fornecedores
aAdd(aRet, {'G6L_CLIENT', cCodCli	})
aAdd(aRet, {'G6L_LOJA'  , cLojaCli	})
aAdd(aRet, {'G6L_FORNEC', cFornecedor	})
aAdd(aRet, {'G6L_LJFORN', cLojaFornec	})
aAdd(aRet, {'G6L_NOME'  , cNome		})
aAdd(aRet, {'G6L_DTGERA', dDataBase})
aAdd(aRet, {'G6L_DTINI' , dDtIni	})
aAdd(aRet, {'G6L_DTFIM' , dDtFim	})
aAdd(aRet, {'G6L_STATUS', '1'		})		// 1=Em aberto;2=Liberado
aAdd(aRet, {'G6L_MOEDA' , cMoeda})		// Moeda do Item de Venda
aAdd(aRet, {'G6L_GRPPRO', cGrpProd})
aAdd(aRet, {'G6L_DESTIN', cTipoDest})

Return(AClone(aRet))

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050ArG6M
Função responsável pela montagem do array com os dados a serem gravados na G6M.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050ArG6M(cSegmento, cTipoAC, cTpTpAC, cForCod, cForLoja, cProduto, cFilRefer)

Local aRet	:= {}
Local lExtTurNat	:= FindFunction('U_TURNAT')
Local cNat			:= ""

aAdd(aRet, {'G6M_SEGNEG', cSegmento})	// 1=Corporativo;2= Eventos;3=Lazer
aAdd(aRet, {'G6M_TIPOAC', cTipoAC  })	// G8B
aAdd(aRet, {'G6M_RATPAD', ' '      })	// 1=Sim;2=Não

cNat := IIf(lExtTurNat, U_TURNAT(cFilAnt,IIf(cTpTpAC == '1','3','4'), cTipoAC, cSegmento, cProduto, '2', cForCod, cForLoja ), "")
If !Empty(cNat) .And. TurVldNat(cNat, .F., )
	aAdd(aRet, {'G6M_NATURE', cNat })	
EndIf

aAdd(aRet, {'G6M_FILREF', cFilRefer})

Return AClone(aRet)


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050ArG6O
Função responsável pela montagem do array com os dados a serem gravados na G6O.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050ArG6O(cCodAcordo, cRevAcordo, cDescAcordo, cTipoVal, cFilRefer, nValor)

Local aRet := {}

aAdd(aRet, {'G6O_CODACD', cCodAcordo})
aAdd(aRet, {'G6O_CODREV', cRevAcordo})
aAdd(aRet, {'G6O_DCACD' , cDescAcordo})
aAdd(aRet, {'G6O_TIPOVL', cTipoVal})	// 1=Porcentagem;2=Valor Fixo
aAdd(aRet, {'G6O_FILREF', cFilRefer})
aAdd(aRet, {IIF(cTipoVal == '1', 'G6O_PERCEN', 'G6O_VALOR'), nValor})

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050GetEr
Função responsável recuperação do erro ocorrido no modelo de dados.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050GetEr(oModel)

Local aErro := oModel:GetErrorMessage()

AutoGrLog(STR0018 + ' [' + AllToChar( aErro[1] ) + ']') //"Id do formulário de origem: "
AutoGrLog(STR0019 + ' [' + AllToChar( aErro[2] ) + ']') //"Id do campo de origem: "
AutoGrLog(STR0020 + ' [' + AllToChar( aErro[3] ) + ']') //"Id do formulário de erro: "
AutoGrLog(STR0021 + ' [' + AllToChar( aErro[4] ) + ']') //"Id do campo de erro: "
AutoGrLog(STR0022 + ' [' + AllToChar( aErro[5] ) + ']') //"Id do erro: "
AutoGrLog(STR0023 + ' [' + AllToChar( aErro[6] ) + ']') //"Mensagem do erro: "
AutoGrLog(STR0024 + ' [' + AllToChar( aErro[7] ) + ']') //"Mensagem da solução: "
AutoGrLog(STR0025 + ' [' + AllToChar( aErro[8] ) + ']') //"Valor atribuído: "
AutoGrLog(STR0026 + ' [' + AllToChar( aErro[9] ) + ']')  //"Valor anterior: "
MostraErro()

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050XGAT
Função chamada através de gatilho de campos.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA050XGAT(oModel,cCampo)

Local oModelG6M := oModel:GetModel('G6M_DETAIL')
Local cTipoAc   := oModelG6M:GetValue('G6M_TIPOAC')

//Jogar o bloco do case deste fonte para a funcao TA040XGAT - TURA040
Do Case
	//Quando efetua-se a marcação ou desmarcação
	Case cCampo == 'G48_MARK' //ok
	
		TA050XCP32(oModel) 	//Atualiza o campo G6O_VAPLIC 
		TA050XCP33(oModel)	//Atualiza o campo G6O_VLBASE
	
	Case cCampo == 'G6O_VAPLIC'	//ok
	
		TA050XCP29(oModel)
		TA050XCP07(oModel, cCampo)

	Case cCampo == 'G6O_CODREV'
		
		TA050XCP03(oModel, cCampo)

	Case cCampo == 'G6O_VLBASE'
		
		TA050XCP29(oModel)

	Case cCampo == 'G6M_PERDES'

		TA050XCP11(oModel)

	Case cCampo == 'G6M_VLDESC'

		TA050XCP10(oModel)
		TA050XCP01(oModel)

	Case cCampo == 'G6M_PERCTX'

		TA050XCP09(oModel)

	Case cCampo == 'G6M_VLTXAD'

		TA050XCP08(oModel)
		TA050XCP01(oModel)

	Case cCampo == 'G6M_VLACD'
		
		If ( oModelG6M:GetValue('G6M_PERDES') > 0 )
			IIF(!FwIsInCallStack('TA50MarkAll') .And. !FwIsInCallStack('TA050RaDel'), MsgInfo(STR0027, STR0114), ) 	//"O Vlr. Descont. será recalculado com base no % Desconto."###"Recálculo Desconto"
			TA050XCP11(oModel)
		EndIf
		
		If ( oModelG6M:GetValue('G6M_PERCTX') > 0 )
			IIF(!FwIsInCallStack('TA50MarkAll') .And. !FwIsInCallStack('TA050RaDel'), MsgInfo(STR0028, STR0115), )  	//"O Vlr. Tx. Ad. será recalculado com base no % Tx. Adic.."###"Recálculo Taxa Adicional"
			TA050XCP09(oModel)
		EndIf
		
		TA050XCP01(oModel)
		
	Case cCampo == 'G6M_TOTAL'
		
		TA050XCP14(oModel)
		
	Case cCampo == 'G6L_TOTACD'
		
		TA050XCP15(oModel)

	Case cCampo == 'G6L_TOTPRD'
		
		TA050XCP15(oModel)
					
End Case

Return Nil

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050XCP33
Função responsável pela atualização do campo G6O_VLBASE.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050XCP33(oModel,aDetalhes)

Local oModelG48 := oModel:GetModel('G48A_DETAIL')
Local oModelG6O := oModel:GetModel('G6O_DETAIL')

Local aSaveRows := FwSaveRows()

Local nValor    := 0
Local nVlBase   := oModelG6O:GetValue('G6O_VLBASE')

Local cTipoVl   := oModelG6O:GetValue('G6O_TIPOVL')

Local lOk       := oModelG48:GetValue('G48_MARK')

Default aDetalhes := {}

//+-----------------------------------------------------
// Se Tipo for Valor fixo, não atualiza campo G6O_VLBASE
//+-----------------------------------------------------
If cTipoVl == '1' // Porcentagem
	
	If Len(aDetalhes) > 0
	
		G48->(DbGoTo(aDetalhes[(AScan(aDetalhes,{|x, y| x[1] == 'G48_RECNO'}))][2]))
	
		If G48->(!EOF())
			nValor := G48->G48_VLBASE
		EndIf
	
	Else
	
		If lOk
			nValor += oModelG48:GetValue('G48_VLBASE')
		Else
			nValor -= oModelG48:GetValue('G48_VLBASE')
		EndIf
	
	EndIf

	If !(lOk := oModelG6O:SetValue('G6O_VLBASE', nVlBase + nValor))
		Help( , ,"TURA050", , STR0029, 1, 0) //"Erro ao atualizar Valor Base do Acordo."
	EndIf
	
EndIf

FwRestRows(aSaveRows)

Return()

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050XCP32
Função responsável pela atualização do campo G6O_VAPLIC.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050XCP32(oModel, aDetalhes)

Local aSaveRows := FwSaveRows()
Local oModelG48 := oModel:GetModel('G48A_DETAIL')
Local oModelG6O := oModel:GetModel('G6O_DETAIL')

Local nValor    := 0
Local nVAplic   := 0
Local nSinal	:= 0

Local lOk       := .t.

Default aDetalhes	:= {}

If !oModelG6O:IsDeleted(oModelG6O:GetLine())
	
	nVAplic := oModelG6O:GetValue('G6O_VAPLIC')
	
	If Len(aDetalhes) > 0
		
		nSinal	:= Iif(aDetalhes[(AScan(aDetalhes,{|x, y| x[1] == 'G48_TPACD'}))][2] == "2", -1, 1)
		nValor	:= aDetalhes[(AScan(aDetalhes,{|x, y| x[1] == 'G48_VLACD'}))][2] * nSinal
		
	Else
	
		nSinal	:= Iif(oModelG48:GetValue('G48_TPACD') == "2", -1, 1)
		
		lOk     := oModelG48:GetValue('G48_MARK')
	
		If lOk 
			nValor += (oModelG48:GetValue('G48_VLACD') * nSinal)
		Else
			nValor -= (oModelG48:GetValue('G48_VLACD') * nSinal)
		EndIf
		
	EndIf
	
	If !(lOk := oModelG6O:SetValue('G6O_VAPLIC', nVAplic + nValor))
		Help( , , "TURA050", , STR0030, 1, 0) //"Erro ao atualizar Valor Total do Acordo."
	EndIf
	
EndIf 

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050XCP29
Função responsável pela atualização do campo G6O_VALOR G6O_PERCEN.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050XCP29(oModel)

Local aSaveRows  := FwSaveRows()
Local oModelG6O  := oModel:GetModel('G6O_DETAIL')
Local cTpVal     := oModelG6O:GetValue('G6O_TIPOVL')
Local nOperation := oModel:GetOperation()
Local nValor     := 0
Local lOk        := .T.

If nOperation == MODEL_OPERATION_UPDATE .And. oModel:GetValue('G48A_DETAIL', 'G48_MARK')
	If cTpVal == '2' // Valor Fixo
		nValor := (oModelG6O:GetValue('G6O_VAPLIC'))
		If !(lOk := oModelG6O:SetValue('G6O_VALOR', nValor))
			Help( , , "TURA050", , STR0031 + oModelG6O:GetValue('G6O_CODACD'), 1, 0)  //"Erro ao atualizar Valor do Acordo. "
		EndIf
	ElseIf cTpVal == '1' // Porcentagem
		nValor := (oModelG6O:GetValue('G6O_VAPLIC') / oModelG6O:GetValue('G6O_VLBASE')) * 100
		If !(lOk := oModelG6O:SetValue('G6O_PERCEN', nValor * IIF(nValor < 0, -1, 1)))
			Help( , , "TURA050", , STR0032 + oModelG6O:GetValue('G6O_CODACD'), 1, 0) //"Erro ao atualizar Valor do Acordo. "
		EndIf
	EndIf
EndIf

FwRestRows(aSaveRows)

Return


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050XCP15
Função responsável pela atualização do campo G6L_TOTAL.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050XCP15(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG6L := oModel:GetModel('G6L_MASTER')
Local nValor    := 0
Local lOk       := .T.

nValor	:= oModelG6L:GetValue('G6L_TOTACD') + oModelG6L:GetValue('G6L_TOTPRD')

If !(lOk := oModelG6L:SetValue('G6L_TOTAL', nValor))
	Help( , , "TURA050", , STR0034, 1, 0) //"Erro ao calcular Valor Total da Apuração."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050XCP14
Função responsável pela atualização do campo G6L_TOTACD.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050XCP14(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG6M := oModel:GetModel('G6M_DETAIL')
Local oModelG6L := oModel:GetModel('G6L_MASTER')
Local nValor    := 0
Local nX        := 0
Local lOk       := .T.

For nX := 1 To oModelG6M:Length()
	oModelG6M:GoLine(nX)
	If ! (oModelG6M:IsDeleted())
		nValor += oModelG6M:GetValue('G6M_TOTAL')
	EndIf
Next

If !(lOk := oModelG6L:SetValue('G6L_TOTACD', nValor))
	Help( , , "TURA050", , STR0035, 1, 0)  //"Erro ao totalizar Valor Acordo."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050XCP11
Função responsável pela atualização do campo G6M_VLDESC.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050XCP11(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG6M := oModel:GetModel('G6M_DETAIL')
Local xValor    := (oModelG6M:GetValue('G6M_VLACD') * oModelG6M:GetValue('G6M_PERDES')) / 100
Local lOk       := .T.

If !(lOk := oModelG6M:SetValue('G6M_VLDESC', xValor))
	Help( , , "TURA050", , STR0036, 1, 0) //"Erro ao calcular Valor do Desconto do Acordo."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050XCP10
Função responsável pela atualização do campo G6M_PERDES.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050XCP10(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG6M := oModel:GetModel('G6M_DETAIL')
Local xValor    := (100 * oModelG6M:GetValue('G6M_VLDESC')) / oModelG6M:GetValue('G6M_VLACD')
Local lOk       := .T.

If !(lOk := oModelG6M:SetValue('G6M_PERDES', xValor))
	Help( , , "TURA050", , STR0037	, 1, 0) //"Erro ao calcular Percentual do Desconto do Acordo."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050XCP09
Função responsável pela atualização do campo G6M_VLTXAD.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050XCP09(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG6M := oModel:GetModel('G6M_DETAIL')
Local xValor    := (oModelG6M:GetValue('G6M_VLACD') * oModelG6M:GetValue('G6M_PERCTX')) / 100
Local lOk       := .T.

If !(lOk := oModelG6M:SetValue('G6M_VLTXAD', xValor))
	Help( , , "TURA050", , STR0038, 1, 0)//"Erro ao calcular Valor da Taxa Adicional do Acordo."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050XCP08
Função responsável pela atualização do campo G6M_PERCTX.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050XCP08(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG6M := oModel:GetModel('G6M_DETAIL')
Local xValor    := (100 * oModelG6M:GetValue('G6M_VLTXAD')) / oModelG6M:GetValue('G6M_VLACD')
Local lOk       := .T.

If !(lOk := oModelG6M:SetValue('G6M_PERCTX', xValor))
	Help( , , "TURA050", , STR0037, 1, 0)//"Erro ao calcular Percentual do Desconto do Acordo."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050XCP07
Função responsável pela atualização do campo G6M_VLACD.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050XCP07(oModel, cCampo)

Local aSaveRows := FwSaveRows()
Local oModelG6M := oModel:GetModel('G6M_DETAIL')
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local xRet      := oModelG6O:GetValue(cCampo)
Local xValor    := 0
Local nX        := 0
Local lOk       := .T.
For nX := 1 To oModelG6O:Length()
	oModelG6O:GoLine(nX)
	If !(oModelG6O:IsDeleted())
		xValor += oModelG6O:GetValue('G6O_VAPLIC')
	EndIf
Next

If !(lOk := oModelG6M:SetValue('G6M_VLACD', xValor))
	Help( , , "TURA050", , STR0040, 1, 0) //"Erro ao calcular Valor Apurado do Acordo."
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050XCP03
Função responsável pela atualização do campo G6M_TOTAL.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050XCP03(oModel, cCampo)

Local aSaveRows := FwSaveRows()
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local xRet      := oModelG6O:GetValue(cCampo)
Local xValor    := ''
Local xAux      := Posicione('G4W', 1, xFilial('G4W') + oModelG6O:GetValue('G6O_CODACD') + oModelG6O:GetValue('G6O_CODREV'), 'G4W_VIGFIM')
Local lOk       := .T.

If Empty(xAux) .Or. xAux >= dDataBase
	xValor	:= '1'
Else
	xValor	:= '3'
EndIf

If !(lOk := oModelG6O:SetValue('G6O_STATUS', xValor))
	Help( , , "TURA050", , STR0041, 1, 0) //"Erro ao atualizar Status do Acordo."
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050XCP01
Função responsável pela atualização do campo G6M_TOTAL.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050XCP01(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG6M := oModel:GetModel('G6M_DETAIL')
Local xValor    := oModelG6M:GetValue('G6M_VLACD') - oModelG6M:GetValue('G6M_VLDESC') + oModelG6M:GetValue('G6M_VLTXAD')
Local lOk       := .T.

If !(lOk := oModelG6M:SetValue('G6M_TOTAL', xValor))
	Help( , , "TURA050", , STR0042, 1, 0)//"Erro ao atualizar G6M_TOTAL."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050VLDAC
Função responsável pela validação antes da exclusão da apuração.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050VLDAC(oModel)

Local lRet       := .T.
Local nOperation := oModel:GetOperation()
	
//+-----------------------------------------------------
// Não permite excluir apurações liberadas
//+-----------------------------------------------------
Do Case
	Case nOperation == MODEL_OPERATION_DELETE
		If !(lRet := !(G6L->G6L_STATUS == '2'))	// 1=Em aberto;2=Liberado
			Help( , , "TURA050", , STR0043, 1, 0)  //"Apuração Liberada não pode ser excluída."
		EndIf
	Case nOperation == MODEL_OPERATION_UPDATE .And. !FwIsInCallStack("TURXEstApura") .and. !FwIsInCallStack("U_TURR050A")//!IsInCallStack('TUR050Est')
		If !(lRet := !(G6L->G6L_STATUS == '2'))	// 1=Em aberto;2=Liberado
			Help( , , "TURA050", , STR0044, 1, 0) //"Apuração Liberada não pode ser alterada."
		EndIf
EndCase

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050G6OLP
Função responsável pela validação da G6O.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA050G6OLP(oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue)

Local oModelMaster := FwModelActive()
Local cTpAcordo    := ''
Local nValor       := 0
Local nG80         := 0
Local lRet         := .T.

If ( cAction == 'DELETE' )
		
	cTpAcordo := AllTrim(Posicione('G8B', 1, xFilial('G8B') + oModelMaster:GetModel('G6M_DETAIL'):GetValue('G6M_TIPOAC'), 'G8B_DESCRI'))
	
	If !(lRet := MsgYesNo(I18N(STR0045 + CRLF + ;//"Confirma a exclusão do Acordo #1 - #2 ?"
                              STR0046 + CRLF + ;  //"Após salvar a apuração, este acordo não será mais "
                              STR0047, ; //"considerado nesta apuração."
                             {oGridModel:GetValue('G6O_CODACD'), cTpAcordo}), ;
                              STR0048))//"Exclusão de Acordos"

		Help( , , "TURA050", , STR0049, 1, 0)//"Ação cancelada pelo usuário."
	
	
	EndIf

Endif

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050Grv
Função responsável pela gravação da apuração.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050Grv(oModel)

Local aSaveRows  := FwSaveRows()
Local aArea      := GetArea()
Local nOperation := oModel:GetOperation()
Local oModelG48  := oModel:GetModel('G48A_DETAIL')
Local oModelG6O  := oModel:GetModel('G6O_DETAIL')
Local oModelG6M  := oModel:GetModel('G6M_DETAIL')
Local oModelG6L  := oModel:GetModel('G6L_MASTER')
Local nItem      := 0
Local nAcordo    := 0
Local nApu       := 0

Local lRet       := .T.

Local aG48Del    := {}
Local aAux       := {}
Local bAfterSTTS := {|| TA050G48Up(nOperation)}

If nOperation == MODEL_OPERATION_UPDATE .Or. nOperation == MODEL_OPERATION_DELETE
	//+------------------------------------------------------
	//| Verifica apuração de acordos
	//+------------------------------------------------------
	For nApu := 1 to oModelG6M:Length()
		
		oModelG6M:GoLine(nApu)
		
		For nAcordo := 1 to oModelG6O:Length()
		
			oModelG6O:GoLine(nAcordo)
		
			If !Empty(oModelG6O:GetValue('G6O_TIPOAC'))// $ TA050AcordToString()
		
				If oModelG6O:IsDeleted()
		
					For nItem := 1 to oModelG48:Length()
		
						oModelG48:GoLine(nItem)
		
						aAux := {}
		
						aAdd(aAux, {'SEGNEG'    , oModelG6M:GetValue('G6M_SEGNEG')})
						aAdd(aAux, {'G48_FILIAL', oModelG48:GetValue('G48_FILIAL')})
						aAdd(aAux, {'G48_NUMID' , oModelG48:GetValue('G48_NUMID' )})
						aAdd(aAux, {'G48_IDITEM', oModelG48:GetValue('G48_IDITEM')})
						aAdd(aAux, {'G48_NUMSEQ', oModelG48:GetValue('G48_NUMSEQ')})
						aAdd(aAux, {'G48_APLICA', oModelG48:GetValue('G48_APLICA')})
						aAdd(aAux, {'G48_CODACD', oModelG48:GetValue('G48_CODACD')})
						aAdd(aAux, {'G48_CODREC', oModelG48:GetValue('G48_CODREC')})
						aAdd(aAux, {'G48_FILAPU', oModelG48:GetValue('G48_FILAPU')})
						aAdd(aAux, {'G48_CODAPU', oModelG6L:GetValue('G6L_CODAPU')})
						aAdd(aAux, {'G48_PERACD', oModelG48:GetValue('G48_PERACD')})
						aAdd(aAux, {'G48_VLACD' , oModelG48:GetValue('G48_VLACD' )})
						aAdd(aAux, {'G48_ATUIF' , oModelG48:GetValue('G48_ATUIF' )})
						aAdd(aAux, {'G48_MARK'  , .F.})
						aAdd(aAux, {'G48_COMSER', oModelG48:GetValue('G48_COMSER')})
						
						If TA050IVCanc(oModelG48:GetValue('G48_FILIAL'), ;
						               oModelG48:GetValue('G48_NUMID'), ;
						               oModelG48:GetValue('G48_IDITEM'), ;
						               oModelG48:GetValue('G48_NUMSEQ')) == '3' 	// 3=Cancelado
							aAdd(aAux, {'G48_DTINT' , dDataBase})
							aAdd(aAux, {'G48_USUINT', __cUserId})
							aAdd(aAux, {'G48_NOMUSU', cUserName})
							aAdd(aAux, {'G48_STATUS', "3"})	
							
						EndIf
							
						aAdd(aG48Del, AClone(aAux))
		
					Next nItem
		
				Else

					For nItem := 1 to oModelG48:Length()

						oModelG48:GoLine(nItem)

						If oModelG48:IsUpdated() .Or. nOperation == MODEL_OPERATION_DELETE

							aAux := {}

							aAdd(aAux, {'SEGNEG'    , oModelG6M:GetValue('G6M_SEGNEG')})
							aAdd(aAux, {'G48_FILIAL', oModelG48:GetValue('G48_FILIAL')})
							aAdd(aAux, {'G48_NUMID' , oModelG48:GetValue('G48_NUMID' )})
							aAdd(aAux, {'G48_IDITEM', oModelG48:GetValue('G48_IDITEM')})
							aAdd(aAux, {'G48_NUMSEQ', oModelG48:GetValue('G48_NUMSEQ')})
							aAdd(aAux, {'G48_APLICA', oModelG48:GetValue('G48_APLICA')})
							aAdd(aAux, {'G48_CODACD', oModelG48:GetValue('G48_CODACD')})
							aAdd(aAux, {'G48_CODREC', oModelG48:GetValue('G48_CODREC')})
							aAdd(aAux, {'G48_FILAPU', oModelG48:GetValue('G48_FILAPU')})
							aAdd(aAux, {'G48_CODAPU', oModelG6L:GetValue('G6L_CODAPU')})
							aAdd(aAux, {'G48_MARK'  , IIF(nOperation != MODEL_OPERATION_DELETE, oModelG48:GetValue('G48_MARK'  ), .F.)})
							aAdd(aAux, {'G48_PERACD', oModelG48:GetValue('G48_PERACD')})
							aAdd(aAux, {'G48_VLACD' , oModelG48:GetValue('G48_VLACD' )})
							aAdd(aAux, {'G48_ATUIF' , oModelG48:GetValue('G48_ATUIF' )})
							aAdd(aAux, {'G48_COMSER', oModelG48:GetValue('G48_COMSER')})
												
							If TA050IVCanc(oModelG48:GetValue('G48_FILIAL'), ;
							               oModelG48:GetValue('G48_NUMID'), ;
							               oModelG48:GetValue('G48_IDITEM'), ;
							               oModelG48:GetValue('G48_NUMSEQ')) == '3' 	// 3=Cancelado
								aAdd(aAux, {'G48_DTINT' , dDataBase})
								aAdd(aAux, {'G48_USUINT', __cUserId})
								aAdd(aAux, {'G48_NOMUSU', cUserName})
								aAdd(aAux, {'G48_STATUS', "3"})	
							
							EndIf
								
							aAdd(aG48Del, AClone(aAux))

						EndIf

					Next nItem

				EndIf

			EndIf

		Next nItem

	Next nApu
	
EndIf

If ( nOperation == MODEL_OPERATION_DELETE )		
	lRet := TurClearApu(oModelG6L:GetValue("G6L_FILIAL"), oModelG6L:GetValue("G6L_CODAPU"), "TURA050")							
EndIf

If ( lRet )

	FWFormCommit( oModel, /*[ bBefore ]*/, /*[ bAfter ]*/, bAfterSTTS )
	
	If ( nOperation == MODEL_OPERATION_UPDATE .Or. nOperation == MODEL_OPERATION_DELETE)	
		Processa({ || TA050Upd(aG48Del, /*lLiber*/, /*lEstor*/, nOperation == MODEL_OPERATION_DELETE)})
	EndIf	

EndIf	

FWRestRows(aSaveRows)
RestArea( aArea )

aG48Del := Nil
aAux    := Nil
DelClassIntF()

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050Upd
Função responsável pela alteração no RV através da TURA034.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA050Upd(aG48Del, lLiber, lEstor, lExcluir)

Local oModelG48  := Nil
Local oModelG4C  := Nil
Local oModelG48M := Nil
Local oModelG48R := Nil
Local oStruG48   := FWFormStruct(1, 'G48', /*bAvalCampo*/, .F./*lViewUsado*/)	 
Local oStruG4C   := FWFormStruct(1, 'G4C', /*bAvalCampo*/, .F./*lViewUsado*/)	 

Local aArea      := GetArea()
Local aSaveRows  := FwSaveRows()
Local aStruG48   := oStruG48:GetFields()
Local aRV		 := {}		
Local nG48       := 0
Local nG4C       := 0
Local nX         := 0
Local nPos       := 0
Local nPMark	 := 0

Local cChave	 := ""
Local cSegNeg	 := ""
Local cConinu    := Space(TamSx3("G4C_CONINU")[1])

Local lRet       := .T.
Local lCancelado := .F.

Default lLiber   := .F.
Default lEstor   := .F.
Default lExcluir := .F.

oModelG48 := FwLoadModel("TURA050A") 
oModelG48:SetOperation(MODEL_OPERATION_UPDATE)

oModelG48M := oModelG48:GetModel("G48_MASTER")
oModelG48R := oModelG48:GetModel("G48_REPASSE") 

//+------------------------------------------------------------
//| Posiciona no RV
//+------------------------------------------------------------
ProcRegua(Len(aG48Del))

Begin Transaction

For nG48 := 1 To Len(aG48Del)
	
	IncProc()
	cChave := ""
	IIF(!lExcluir, cChave += PadR(aG48Del[nG48][AScan(aG48Del[nG48],{|x, y| x[1] == 'G48_FILAPU'})][2], TamSx3('G48_FILAPU')[1]), "")
	IIF(!lExcluir, cChave += PadR(aG48Del[nG48][AScan(aG48Del[nG48],{|x, y| x[1] == 'G48_CODAPU'})][2], TamSx3('G48_CODAPU')[1]), "")
	cChave += PadR(aG48Del[nG48][AScan(aG48Del[nG48],{|x, y| x[1] == 'G48_FILIAL'})][2], TamSx3('G48_FILIAL')[1]) //?? SERIA G48_FILREF 
	cChave += PadR(aG48Del[nG48][AScan(aG48Del[nG48],{|x, y| x[1] == 'G48_NUMID' })][2], TamSx3('G48_NUMID')[1])
	cChave += PadR(aG48Del[nG48][AScan(aG48Del[nG48],{|x, y| x[1] == 'G48_IDITEM'})][2], TamSx3('G48_IDITEM')[1])
	cChave += PadR(aG48Del[nG48][AScan(aG48Del[nG48],{|x, y| x[1] == 'G48_NUMSEQ'})][2], TamSx3('G48_NUMSEQ')[1])
	cChave += PadR(aG48Del[nG48][AScan(aG48Del[nG48],{|x, y| x[1] == 'G48_APLICA'})][2], TamSx3('G48_APLICA')[1])
	cChave += PadR(aG48Del[nG48][AScan(aG48Del[nG48],{|x, y| x[1] == 'G48_CODACD'})][2], TamSx3('G48_CODACD')[1])
	cChave += PadR(aG48Del[nG48][AScan(aG48Del[nG48],{|x, y| x[1] == 'G48_CODREC'})][2], TamSx3('G48_CODREC')[1])
	
	cSegNeg := aG48Del[nG48][AScan(aG48Del[nG48],{|x, y| x[1] == 'SEGNEG'    })][2]
	
	//Ajuste efetuado devido ao campo CONINU. O teste inicial não deu certo
	//Todas as tabelas que possuem o campo CONINU deverão ter um possível tratamento no seek para não considerar
	//o registro localizado que possui este campo preenchido.
	//Iniciei o desenvolvimento de uma função chamada TURSeek para ser utilizada para efetuar busca
	//nas tabelas que existem o campo CONINU. A função ainda está em desenvolvimento e bastante incompleta; 
	If (TURSearch("G48", IIF(!lExcluir, 5, 1), cChave) ) //G48 ÎDICE 5: G48_FILAPU+G48_CODAPU+G48_FILIAL+G49_NUMID+G48_IDITEM+G48_NUMSEQ+G48_APLICA+G48_CODACD+G48_CODREC
		
		If G48->G48_SEGNEG == cSegNeg 
			
			If oModelG48:Activate()
			
				For nX := 1 To Len(aG48Del[nG48] )
				
					If (nPos := aScan(aStruG48, {|x| AllTrim(x[3])== AllTrim(aG48Del[nG48][nX][1])})) > 0
				
						If ( AllTrim(aG48Del[nG48][nX][1]) <> 'G48_MARK' )
							
							If ( AllTrim(aG48Del[nG48][nX][1]) $ "G48_CODAPU|G48_FILAPU" )
								
								nPMark := aScan(aG48Del[nG48], {|x| Alltrim(x[1]) == 'G48_MARK' })	
								
								If ( nPMark > 0 )
									//Somente quando tiver selecionado que será atualizado o código da apuração na G48
									If ( aG48Del[nG48][nPMark,2] )
										lRet := oModelG48:SetValue( 'G48_MASTER', aG48Del[nG48][nX][1], aG48Del[nG48][nX][2])
									Else
										lRet := oModelG48:SetValue( 'G48_MASTER', aG48Del[nG48][nX][1], "") 
									Endif
										 
								Endif
							
							Else
								lRet := oModelG48:SetValue( 'G48_MASTER', aStruG48[nPos][3], aG48Del[nG48][nX][2])
							Endif	
								
						Else
							lRet := oModelG48:LoadValue('G48_MASTER', aStruG48[nPos][3], aG48Del[nG48][nX][2])
						Endif	
				
						If ( !lRet )			
						
							Help( , , "TURA050", , I18N(STR0050, { aStruG48[nPos][3] }), 1, 0) //"Erro ao atribuir valor #1"
							Exit
							
						EndIf
				
					EndIf
				
				Next nX
				
				lCancelado := oModelG48:GetValue('G48_MASTER', 'G48_STATUS') == '3'
	
				//+--------------------------------------------------------------
				//|Atualiza Repasse, caso necessário							|
				//+--------------------------------------------------------------
				If ( lRet )
					
					//Existe Diferença de Valores de Acordo?
					If ( Len(aG48Dif) > 0)
						
						//Busca pelo RV, Item e Sequencia no array aG48Dif
						cChave := Alltrim(oModelG48M:GetValue('G48_NUMID') + oModelG48M:GetValue('G48_IDITEM') + oModelG48M:GetValue('G48_NUMSEQ'))
						nPos   := aScan(aG48Dif, {|x| Alltrim(x[1]+x[2]+x[3]) == cChave})
						
						//Possui Repasse?	
						If ( nPos > 0 .And. aG48Dif[nPos,7] )
							
							//Atualiza dados de percentual de acordo e valor de acordo do Repasse (C07)
							If ( oModelG48R:GetValue("G48_TPVLR") == "1" ) //Se o Tipo do acordo é porcentagem
								lRet := oModelG48R:LoadValue("G48_VLBASE",oModelG48M:GetValue("G48_VLACD")) .And. oModelG48R:LoadValue("G48_PERACD",aG48Dif[nPos,8][7,2])
							Endif
													
							If ( lRet )
								lRet :=	oModelG48R:LoadValue("G48_COMSER",aG48Dif[nPos,8][9,2])
								lRet :=	oModelG48R:LoadValue("G48_VLACD",aG48Dif[nPos,8][8,2]) .And. oModelG48R:LoadValue("G48_ATUIF","1")
																
								If ( !lRet )
								
									cMsgErro := STR0105 + Alltrim(aG48Dif[nPos,8][6,2]) + chr(13) //"Problema na Atualização acordo de Repasse "
									cMsgErro += TurGetErrorMsg(oModelRV)
		
									FwAlertInfo(cMsgErro,STR0106) // "Atenção"
									lRet := .f.
									
								EndIf	
							
							EndIf
							
						EndIf
						
					EndIf	
					
				EndIf
				
				//+------------------------------------------------------------
				//|	Se for liberação, finaliza acordo aplicado
				//+------------------------------------------------------------
				If lRet .And. (lLiber .Or. lEstor .Or. lCancelado)
		
					oModelG4C := oModelG48:GetModel('G4C_DETAIL')
		
					For nG4C := 1 To oModelG4C:Length()
						
						oModelG4C:GoLine(nG4C)	
						
						If !oModelG4C:IsDeleted() .And. !Empty(oModelG4C:GetValue('G4C_IDIF')) 
							
							lRet := oModelG4C:SetValue('G4C_STATUS', IIF(lLiber .Or. lCancelado, '4', '2'))
							
							If ( lRet )
								
								nPos := Ascan(aRv,{|x| x[1]+x[2]+x[3]+x[4] == oModelG4C:GetValue("G4C_FILIAL") + oModelG4C:GetValue("G4C_NUMID") + oModelG4C:GetValue("G4C_IDITEM") + oModelG4C:GetValue("G4C_NUMSEQ")})
							
								If ( nPos <= 0 ) 
									
									Aadd(aRv,{	oModelG4C:GetValue("G4C_FILIAL"),;
												oModelG4C:GetValue("G4C_NUMID"),;
												oModelG4C:GetValue("G4C_IDITEM"),;
												oModelG4C:GetValue("G4C_NUMSEQ")})
								
								EndIf
							
							EndIf
									
						EndIf
						
					Next nG4C
		
				EndIf
				
				//Atualização dos Acordos de Fornecedor
				If ( lRet .And. (lRet := oModelG48:VldData()) )
					
					oModelG48:CommitData()
					
					For nX := 1 to Len(aRV)
						TurAtuStRV(aRv[nX,1], aRv[nX,2], aRv[nX,3], aRv[nX,4], cConinu, "2", !lEstor ) //TurAtuStRV(oModelG4C:GetValue("G4C_FILREF"),oModelG4C:GetValue("G4C_NUMID"),oModelG4C:GetValue("G4C_IDITEM"),oModelG4C:GetValue("G4C_NUMSEQ"), cConinu, "2", !lEstor ) //ida, true					
					Next nX 
					
				Else
					DisarmTransaction()
					TA050GetEr(oModelG48)
					Exit
				EndIf
	
				oModelG48:DeActivate()
			EndIf	
		EndIf
		
	Endif
	
Next nG48

If lRet .AND. !lLiber .AND. !lEstor
	
	If !(Tur50AtuRV())
		DisarmTransaction()
	EndIf
	
EndIf

End Transaction

oModelG48:Destroy()

RestArea(aArea)
FwRestRows(aSaveRows)

aRV := Nil		
DelClassIntF()

Return(lRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Função responsável pela definição da visão da Apuração de Receita de Clientes.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function ViewDef()

Local oModel    := FWLoadModel('TURA050')
Local oStruG6L  := FWFormStruct(2, 'G6L')
Local oStruG6M  := FWFormStruct(2, 'G6M')
Local oStruG6O  := FWFormStruct(2, 'G6O')
Local oStruG48  := FWFormStruct(2, 'G48')
Local oStruG81  := FWFormStruct(2, 'G81')
Local oStruG3Q  := FWFormStruct(2, 'G3Q') //I.V 
Local oStruG3T := FWFormStruct(2, 'G3T')	// Passageiros AEREO
Local oStruG3U := FWFormStruct(2, 'G3U')	// Passageiros HOTEL
Local oStruG3V := FWFormStruct(2, 'G3V')	// Passageiros VEICULO
Local oStruG3W := FWFormStruct(2, 'G3W')	// Passageiros RODOV
Local oStruG3X := FWFormStruct(2, 'G3X')	// Passageiros TREM
Local oStruG3Y := FWFormStruct(2, 'G3Y')	// Passageiros CRUZEIRO
Local oStruG3Z := FWFormStruct(2, 'G3Z')	// Passageiros PACOTE
Local oStruG40 := FWFormStruct(2, 'G40')	// Passageiros TOUR
Local oStruG41 := FWFormStruct(2, 'G41')	// Passageiros SEGURO
Local oStruG42 := FWFormStruct(2, 'G42')	// Passageiros VISTO
Local oStruG43 := FWFormStruct(2, 'G43')	// Passageiros OUTROS
Local oView     := FWFormView():New()

Local aOrdem    := {} 
Local aNoFldG6L	:= {}
Local aNoFldG6M	:= {}
Local aNoFldG6O	:= {}
Local aNoFldG48	:= {}
Local aNoFldG81	:= {}
Local aNoFldG3Q	:= {}

Local nI		:= 0

aNoFldG6L := {	"G6L_CLIENT",;
				"G6L_LOJA",;
				"G6L_NOME",;
				"G6L_TOTACD",;
				"G6L_TOTPRD",;
				"G6L_TPAPUR" 	}

aNoFldG6M := {	"G6M_RATPAD",;
				"G6M_NUMFAT",;
				"G6M_MSGOBS",;
				"G6M_CODAPU"	}

aNoFldG6O := {	"G6O_STATUS",;
				"G6O_TARMAX",;
				"G6O_TARAPL",;
				"G6O_TOTMET",;
				"G6O_TOTNEC",;
				"G6O_PERNEC",;
				"G6O_FAIXA",;
				"G6O_FXDE",;
				"G6O_FXATE",;
				"G6O_FXVLR",;
				"G6O_FXATU",;
				"G6O_CODAPU"	}

aNoFldG48 := {	'G48_FILAPU',;
				'G48_DSFAPU',;
				'G48_CODAPU',;
				'G48_CODACD',;
				'G48_CODREC',;
				'G48_OK',;
				'G48_RECCON',;
				'G48_TPCOMB',;
				'G48_CHAVEC',;
				'G48_SEGNEG',;
				'G48_CLIFOR',;
				'G48_ACDBAS',;
				'G48_DACDBA',;
				'G48_ESTORN',;
				'G48_APUGER',;
				'G48_CANSIS',;
				'G48_DSCMTI',;
				'G48_DSCCT1',;
				'G48_DSCCTT',;
				'G48_DSCCTD',;
				'G48_DSCCTH' }

aNoFldG81 := {	'G81_SEGNEG',;
				'G81_TPENT',;
				'G81_ITENT',;	
				'G81_CODPRD',;
				'G81_DSCPRD'}

aNoFldG3Q := {	'G3Q_CONTAC',;
				'G3Q_CCUSTO',;
				'G3Q_ITEMCO',;	
				'G3Q_CLASVL',;
				'G3Q_DSCCT1',;
				'G3Q_DSCCTT',;
				'G3Q_DSCCTD',;
				'G3Q_DSCCTH'}

oView:SetModel( oModel )

//Remove campos que não serão apresentados no Field, Representado pela tabela G6L
For nI := 1 to Len(aNoFldG6L)
	
	If ( oStruG6L:HasField(aNoFldG6L[nI]) )
		oStruG6L:RemoveField(aNoFldG6L[nI])
	Endif
	
Next nI

//Remove campos que não serão apresentados no Grid Receita Acordos, representado pela tabela G6M
For nI := 1 to Len(aNoFldG6M)
	
	If ( oStruG6M:HasField(aNoFldG6M[nI]) )
		oStruG6M:RemoveField(aNoFldG6M[nI])
	Endif
	
Next nI

//Remove campos que não serão apresentados no Grid Acordos, representado pela tabela G6O
For nI := 1 to Len(aNoFldG6O)
	
	If ( oStruG6O:HasField(aNoFldG6O[nI]) )
		oStruG6O:RemoveField(aNoFldG6O[nI])
	Endif
	
Next nI

//Remove campos que seriam apresentados no Grid Acordo x Item de Venda - Tabela G48
For nI := 1 to Len(aNoFldG48)
	
	If ( oStruG48:HasField(aNoFldG48[nI]) )
		oStruG48:RemoveField(aNoFldG48[nI])
	Endif
	
Next nI

//Remove campos que seriam apresentados no Grid Itens Financeiros (Aba de mesmo nome) - Tabela G81
For nI := 1 to Len(aNoFldG81)
	
	If ( oStruG81:HasField(aNoFldG81[nI]) )
		oStruG81:RemoveField(aNoFldG81[nI])
	Endif
	
Next nI

//Remove campos que seriam apresentados no Grid Itens de Venda (Aba de mesmo nome) - Tabela G3Q
For nI := 1 to Len(aNoFldG3Q)
	
	If ( oStruG3Q:HasField(aNoFldG3Q[nI]) )
		oStruG3Q:RemoveField(aNoFldG3Q[nI])
	Endif
	
Next nI

//-------------------------------------------------------------------------
//Ordenação dos Campos
//-------------------------------------------------------------------------

//Acordos Aplicados x Itens De Venda
TurOrdViewStruct("G48",oStruG48)	

//Itens Financeiros de Apuração
oStruG81:SetProperty( "G81_FILREF" , MVC_VIEW_ORDEM, "01")

aOrdem := {	{"G81_STATUS","G81_FILFAT"},;
			{"G81_FILFAT","G81_NUMFAT"}}

TurOrdViewStruct("G81",oStruG81,aOrdem)	

//Itens de Venda - RV
aOrdem := {	{"G3Q_TPEMIS","G3Q_DSCTPE"},;
			{"G3Q_FILPST","G3Q_DSFPST"},;
			{"G3Q_DSFPST","G3Q_POSTO"},;
			{"G3Q_POSTO","G3Q_DSCPST"},;
			{"G3Q_MOEDCL","G3Q_DSCMCL"},;
			{"G3Q_MOTCAN","G3Q_DESCAN"},;			
			{"G3Q_NATURE","G3Q_DSCNAT"}}

TurOrdViewStruct("G3Q",oStruG3Q,aOrdem)

//Passageiros AEREO
aOrdem := {	{"G3T_TERORI","G3T_DSCORI"},;
			{"G3T_TERDST","G3T_DSCDST"}}

TurOrdViewStruct("G3T",oStruG3T,aOrdem)

// Passageiros HOTEL
aOrdem := {	{"G3U_CATPRO","G3U_DSCTPA"},;
			{"G3U_PLALIM","G3U_DSCPLA"},;
			{"G3U_BROKER","G3U_NBROKR"}}

TurOrdViewStruct("G3U",oStruG3U,aOrdem)

// Passageiros VEICULO
aOrdem := {	{"G3V_CIDDEV","G3V_NCIDRT"},;
			{"G3V_LOCDEV","G3V_NCIDDV"},;
			{"G3V_CATAUT","G3V_NCTAUT"},;
			{"G3V_TPAUT","G3V_NTPAUT"},;
			{"G3V_TRADIR","G3V_NTRDIR"},;
			{"G3V_COMBAR","G3V_NCOMAR"},;
			{"G3V_BROKER","G3V_NBROKR"}}

TurOrdViewStruct("G3V",oStruG3V,aOrdem)	

// Passageiros RODOV
aOrdem := {	{"G3W_LINHA","G3W_DSCLIN"},;
			{"G3W_CIDEMB","G3W_NCIDEM"},;
			{"G3W_CIDDES","G3W_NCIDDS"}}

TurOrdViewStruct("G3W",oStruG3W,aOrdem)

// Passageiros TREM
aOrdem := {	{"G3X_CIDEMB","G3X_NCIDEM"},;
			{"G3X_CIDDES","G3X_NCIDDS"}}

TurOrdViewStruct("G3X",oStruG3X,aOrdem)

// Passageiros CRUZEIRO
aOrdem := {	{"G3Y_CIDPRI","G3Y_NCIDPR"},;
			{"G3Y_CIDEMB","G3Y_NCIDEM"},;
			{"G3Y_TPCABI","G3Y_NTPCAB"}}

TurOrdViewStruct("G3Y",oStruG3Y,aOrdem)
 	
// Passageiros PACOTE
aOrdem := {	{"G3Z_CIDPRI","G3Z_NCIDPR"},;
			{"G3Z_CIDEMB","G3Z_NCIDEM"}}

TurOrdViewStruct("G3Z",oStruG3Z,aOrdem)

// Passageiros TOUR
aOrdem := {	{"G40_CIDPRI","G40_CIDPRI"},;
			{"G40_CIDEMB","G40_CIDEMB"}}

TurOrdViewStruct("G40",oStruG40,aOrdem)

// Passageiros VISTO
aOrdem := {	{"G42_PAIS","G42_NPAIS"},;
			{"G42_TPVIST","G42_NTPVST"}}

TurOrdViewStruct("G42",oStruG42,aOrdem)

// Passageiros OUTROS
aOrdem := {	{"G43_CIDEMB","G43_NCIDEM"},;
			{"G43_CIDDES","G43_NCIDDS"}}

TurOrdViewStruct("G43",oStruG43,aOrdem)

//Altera a propriedade do G3Q
oStruG3Q:SetProperty('*', MVC_VIEW_CANCHANGE , .F.)

//Altera as propriedades de G6L
oStruG6L:SetProperty('G6L_CODAPU', MVC_VIEW_CANCHANGE , .F.)
oStruG6L:SetProperty('G6L_CODAPU', MVC_VIEW_ORDEM , '01')
oStruG6L:SetProperty('G6L_FORNEC', MVC_VIEW_ORDEM , '02')
oStruG6L:SetProperty('G6L_LJFORN', MVC_VIEW_ORDEM , '03')
oStruG6L:SetProperty('G6L_NOMFOR', MVC_VIEW_ORDEM , '04')
oStruG6L:SetProperty('G6L_DEMENV', MVC_VIEW_TITULO , STR0100)	//"E-mail Enviado?"

//Altera as propriedades de G6M
oStruG6M:SetProperty('G6M_FILREF', MVC_VIEW_CANCHANGE , .F.)
oStruG6M:SetProperty('G6M_FILREF', MVC_VIEW_ORDEM , '01')

//Altera as propriedades de G6O
oStruG6O:SetProperty('G6O_FILREF', MVC_VIEW_CANCHANGE , .F.)
oStruG6O:SetProperty('G6O_FILREF', MVC_VIEW_ORDEM , '01')
oStruG6O:SetProperty('G6O_PERCEN', MVC_VIEW_ORDEM , '10')
oStruG6O:SetProperty('G6O_VALOR', MVC_VIEW_TITULO , STR0101)	//"Valor Fixo"

//Altera as propriedades de G81
oStruG81:SetProperty('G81_FILREF', MVC_VIEW_CANCHANGE , .F.)
oStruG81:SetProperty('G81_FILREF', MVC_VIEW_ORDEM , '01')


//Altera as propriedades do campo G48_MARK 
oStruG48:SetProperty('G48_FILREF', MVC_VIEW_CANCHANGE , .F.)
oStruG48:SetProperty('G48_MARK', MVC_VIEW_TITULO , '')
oStruG48:SetProperty('G48_MARK', MVC_VIEW_ORDEM , '01')
oStruG48:SetProperty('G48_FILREF', MVC_VIEW_ORDEM , '02')

oView:AddField('VIEW_G6L', oStruG6L, 'G6L_MASTER')

oView:AddGrid('VIEW_G6M' , oStruG6M, 'G6M_DETAIL' )
oView:AddGrid('VIEW_G6O' , oStruG6O, 'G6O_DETAIL' )
oView:AddGrid('VIEW_G48A', oStruG48, 'G48A_DETAIL')
oView:AddGrid('VIEW_G81A', oStruG81, 'G81A_DETAIL')

oView:AddGrid('VIEW_G3Q', oStruG3Q, 'G3Q_DETAIL')	// I.V
oView:AddGrid('VIEW_G3T', oStruG3T, 'G3T_DETAIL')	// Passageiros AEREO
oView:AddGrid('VIEW_G3U', oStruG3U, 'G3U_DETAIL')	// Passageiros HOTEL
oView:AddGrid('VIEW_G3V', oStruG3V, 'G3V_DETAIL')	// Passageiros VEICULO
oView:AddGrid('VIEW_G3W', oStruG3W, 'G3W_DETAIL')	// Passageiros RODOV
oView:AddGrid('VIEW_G3X', oStruG3X, 'G3X_DETAIL')	// Passageiros TREM
oView:AddGrid('VIEW_G3Y', oStruG3Y, 'G3Y_DETAIL')	// Passageiros CRUZEIRO
oView:AddGrid('VIEW_G3Z', oStruG3Z, 'G3Z_DETAIL')	// Passageiros PACOTE
oView:AddGrid('VIEW_G40', oStruG40, 'G40_DETAIL')	// Passageiros TOUR
oView:AddGrid('VIEW_G41', oStruG41, 'G41_DETAIL')	// Passageiros SEGURO
oView:AddGrid('VIEW_G42', oStruG42, 'G42_DETAIL')	// Passageiros VISTO
oView:AddGrid('VIEW_G43', oStruG43, 'G43_DETAIL')	// Passageiros OUTROS


oView:SetViewProperty('VIEW_G6L', 'SETLAYOUT',{ FF_LAYOUT_HORZ_DESCR_TOP , 2 } )

oView:AddIncrementField('VIEW_G48A', 'G48_APLICA')

//+-----------------------------------------------------------------------
//| TELA ESQUERDA
//+-----------------------------------------------------------------------
oView:CreateVerticallBox('TELA_ESQ', 29)	// Apuração

//+-----------------------------------------------------------------------
//| TELA DIREITA
//+-----------------------------------------------------------------------
oView:CreateVerticallBox('TELA_DIR', 71)
oView:CreateHorizontalBox('BOX_DIR', 100, 'TELA_DIR')
oView:CreateFolder('F_ALL', 'BOX_DIR')

//+-----------------------------------------------------------------------
//| TELA DIREITA - Receita de Acordo
//+-----------------------------------------------------------------------
oView:AddSheet('F_ALL', 'S_REC_ACORDO', STR0051) //"Receita de Acordos"
oView:CreateHorizontalBox('BOX_AC_SUP', 23, , , 'F_ALL', 'S_REC_ACORDO')

//+-------------------------------------------------------------------
//|	ACORDOS
//+-------------------------------------------------------------------
oView:CreateHorizontalBox('BOX_AC_MEI', 77, , , 'F_ALL', 'S_REC_ACORDO')
oView:CreateFolder('F_ACORDOS', 'BOX_AC_MEI')

//+-----------------------------------------------------------
//|	Acordos
//+-----------------------------------------------------------
oView:AddSheet('F_ACORDOS', 'S_ACORDOS', STR0052)  //"Acordos"
oView:CreateHorizontalBox('BOX_S_ACORDOS_UP', 35, , , 'F_ACORDOS', 'S_ACORDOS')

//+-------------------------------------------------------------------
//|	DETALHES ACORDOS
//+-------------------------------------------------------------------
oView:CreateHorizontalBox('BOX_S_ACORDOS_DOW', 65, , ,'F_ACORDOS', 'S_ACORDOS')
oView:CreateFolder('F_DETALHES', 'BOX_S_ACORDOS_DOW')

//+-----------------------------------------------------------
//|	Acordo x Itens de Venda G48
//+-----------------------------------------------------------
oView:AddSheet('F_DETALHES', 'S_ITENS_G48', STR0053) //"Acordo x Itens de Venda"
oView:CreateHorizontalBox('BOX_S_ITENS_G48', 100, , , 'F_DETALHES', 'S_ITENS_G48')

//+-----------------------------------------------------------
//|	Passageiros
//+-----------------------------------------------------------
oView:AddSheet('F_DETALHES', 'S_ITENS_G3Q', STR0086)  //"I.V"
oView:CreateHorizontalBox('BOX_S_ITENS_G3Q', 100, , , 'F_DETALHES', 'S_ITENS_G3Q')

oView:AddSheet('F_DETALHES', 'S_ITENS_G3T', STR0087)  
oView:CreateHorizontalBox('BOX_S_ITENS_G3T', 100, , , 'F_DETALHES', 'S_ITENS_G3T')
// Passageiros HOTEL
oView:AddSheet('F_DETALHES', 'S_ITENS_G3U',STR0088)  
oView:CreateHorizontalBox('BOX_S_ITENS_G3U', 100, , , 'F_DETALHES', 'S_ITENS_G3U')

oView:AddSheet('F_DETALHES', 'S_ITENS_G3V', STR0089)  
oView:CreateHorizontalBox('BOX_S_ITENS_G3V', 100, , , 'F_DETALHES', 'S_ITENS_G3V')

oView:AddSheet('F_DETALHES', 'S_ITENS_G3W', STR0090)  
oView:CreateHorizontalBox('BOX_S_ITENS_G3W', 100, , , 'F_DETALHES', 'S_ITENS_G3W')

oView:AddSheet('F_DETALHES', 'S_ITENS_G3Y', STR0091)  
oView:CreateHorizontalBox('BOX_S_ITENS_G3Y', 100, , , 'F_DETALHES', 'S_ITENS_G3Y')

oView:AddSheet('F_DETALHES', 'S_ITENS_G3X', STR0092)  
oView:CreateHorizontalBox('BOX_S_ITENS_G3X', 100, , , 'F_DETALHES', 'S_ITENS_G3X')
 
oView:AddSheet('F_DETALHES', 'S_ITENS_G42', STR0093)  
oView:CreateHorizontalBox('BOX_S_ITENS_G42', 100, , , 'F_DETALHES', 'S_ITENS_G42')

oView:AddSheet('F_DETALHES', 'S_ITENS_G41', STR0094)  
oView:CreateHorizontalBox('BOX_S_ITENS_G41', 100, , , 'F_DETALHES', 'S_ITENS_G41')

oView:AddSheet('F_DETALHES', 'S_ITENS_G40', STR0095)  
oView:CreateHorizontalBox('BOX_S_ITENS_G40', 100, , , 'F_DETALHES', 'S_ITENS_G40')

oView:AddSheet('F_DETALHES', 'S_ITENS_G3Z', STR0096)  
oView:CreateHorizontalBox('BOX_S_ITENS_G3Z', 100, , , 'F_DETALHES', 'S_ITENS_G3Z')

oView:AddSheet('F_DETALHES', 'S_ITENS_G43', STR0097)  
oView:CreateHorizontalBox('BOX_S_ITENS_G43', 100, , , 'F_DETALHES', 'S_ITENS_G43')


//+-----------------------------------------------------------
//|	Itens Financeiro
//+-----------------------------------------------------------
oView:AddSheet('F_ACORDOS', 'S_ITENSF', STR0054) //"Itens Financeiros"
oView:CreateHorizontalBox('BOX_S_ITENSF', 100, , , 'F_ACORDOS', 'S_ITENSF')

oView:SetOwnerView('VIEW_G6L'   , 'TELA_ESQ'          )
oView:SetOwnerView('VIEW_G6M'   , 'BOX_AC_SUP'        )
oView:SetOwnerView('VIEW_G6O'   , 'BOX_S_ACORDOS_UP'  )

oView:SetOwnerView('VIEW_G81A'  , 'BOX_S_ITENSF'   )
oView:SetOwnerView('VIEW_G48A'  , 'BOX_S_ITENS_G48')


oView:SetOwnerView('VIEW_G3Q','BOX_S_ITENS_G3Q') //I.V
oView:SetOwnerView('VIEW_G3T','BOX_S_ITENS_G3T')	// Passageiros AEREO
oView:SetOwnerView('VIEW_G3U','BOX_S_ITENS_G3U')	// Passageiros HOTEL
oView:SetOwnerView('VIEW_G3V','BOX_S_ITENS_G3V')	// Passageiros VEICULO
oView:SetOwnerView('VIEW_G3W','BOX_S_ITENS_G3W')	// Passageiros RODOV
oView:SetOwnerView('VIEW_G3X','BOX_S_ITENS_G3X')	// Passageiros TREM
oView:SetOwnerView('VIEW_G3Y','BOX_S_ITENS_G3Y')	// Passageiros CRUZEIRO
oView:SetOwnerView('VIEW_G3Z','BOX_S_ITENS_G3Z')	// Passageiros PACOTE
oView:SetOwnerView('VIEW_G40','BOX_S_ITENS_G40')	// Passageiros TOUR
oView:SetOwnerView('VIEW_G41','BOX_S_ITENS_G41')	// Passageiros SEGURO
oView:SetOwnerView('VIEW_G42','BOX_S_ITENS_G42')	// Passageiros VISTO
oView:SetOwnerView('VIEW_G43','BOX_S_ITENS_G43')	// Passageiros OUTROS


oView:SetViewAction('DELETELINE'  , {|oView, cIdView, nNumLine| TA050RaDel(oView, cIdView, nNumLine)})
oView:SetViewAction('UNDELETELINE', {|oView, cIdView, nNumLine| TA050RaDel(oView, cIdView, nNumLine)})

oView:SetAfterViewActivate({|oVw| TA50InitFld(oView) })
oView:SetVldFolder({|cFolderID, nOldSheet, nSelSheet| TA050VlFol(cFolderID, nOldSheet, nSelSheet)})
oView:SetViewProperty("G48A_DETAIL","CHANGELINE",{{|oView, cViewID| TA050ChLin( oView, cViewID ) }})

//Adição de novos botoes
oView:AddUserButton(STR0102, 'CLIPS', {|oVw| TA50MarkAll(oVw)  },,VK_F11,{MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE} ) // "Marcar/Desmarcar Todos"

aOrdem    := Nil
aNoFldG6L := Nil
aNoFldG6M := Nil
aNoFldG6O := Nil
aNoFldG48 := Nil
aNoFldG81 := Nil
aNoFldG3Q := Nil
DelClassIntF()

Return oView

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050LibP
Função responsável pela liberação da apuração.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050LibP()

Local oModel     	:= FWLoadModel('TURA050')
Local oModelG6M  	:= oModel:GetModel('G6M_DETAIL')
Local oModelG81A 	:= oModel:GetModel('G81A_DETAIL')
Local oStruG81A  	:= oModelG81A:GetStruct()

Local aArea      	:= GetArea()
Local aSaveRows  	:= FwSaveRows()
Local aStruG81A  	:= oStruG81A:GetFields()
Local aDados		:= {}
Local aDadosG6L  	:= {}
Local aDadosG6M  	:= {}
Local aDadosG82  	:= {}
Local aDadosG81A 	:= {}
Local aAux       	:= {}
Local aCtbG81A		:= {}
Local aCtbG81B		:= {}

Local nG6LRecno  	:= G6L->(Recno())
Local nG81       	:= 0
Local nG6M       	:= 0
Local nX         	:= 0
Local nPos       	:= 0

Local cCodApu    	:= G6L->G6L_CODAPU
Local cFilRef		:= ""

Local lRet       	:= .T.
Local lOnLIne		:= .F.
Local lMostrLanc	:= .F.
Local lAglutLanc	:= .F.
Local lExist		:= .F.

lExist := TURExistX1("TURA050C")

If ( lExist )
	Pergunte("TURA050C",.F.)
Endif	

lOnLine 	:= Iif(lExist .And. mv_par01 == 1,.T.,.F.)
lMostrLanc	:= Iif(lExist .And. mv_par02 == 1,.T.,.F.)
lAglutLanc	:= Iif(lExist .And. mv_par03 == 1,.T.,.F.)

//+-------------------------------------------------
//| Localiza Receita de Acordos
//+-------------------------------------------------
aDadosG6M := TA050SeG81(cCodApu)

For nG6M := 1 To Len(aDadosG6M)

	aAux := TA050ArG81(aDadosG6M[nG6M])
	
	If ( Len(aAux) == 0 )
		lRet := .f.
		Exit
	Endif
	
	aAdd(aDadosG81A, AClone(aAux))

Next nG6M

If ( lRet )

	//+-------------------------------------------------
	//| Inclui IF da Apuração
	//+-------------------------------------------------
	oModel:SetOperation(MODEL_OPERATION_UPDATE)
	oModel:Activate()
	
	If len(aDadosG81A) > 0
		oModelG81A:SetNoInsertLine(.F.)
		
		nCol := aScan(aDadosG81A[1],{|x| Alltrim(x[1]) == "G81_FILREF"})
		nCont := 1	

		For nG6M := 1 To oModelG6M:Length()
		
			oModelG6M:GoLine(nG6M)
		
			cFilRef := oModelG6M:GetValue('G6M_FILREF')
		
			For nG81 := nCont To Len(aDadosG81A)
			
				If ( cFilRef == aDadosG81A[nG81][nCol,2] ) 
		
					If oModelG6M:GetValue('G6M_TIPOAC') == aDadosG81A[nG81][AScan(aDadosG81A[nG81], {|x, y| x[1] == 'G81_CLASS'})][2] .And. oModelG6M:GetValue('G6M_SEGNEG') == aDadosG81A[nG81][AScan(aDadosG81A[nG81], {|x, y| x[1] == 'G81_SEGNEG'})][2]
			
						If !(oModelG81A:Length() == 1 .And. Empty(oModelG81A:GetValue('G81_CLIENT')))
							oModelG81A:AddLine()
						EndIf
		
						//+-------------------------------------------
						//| Inclui dados na G81
						//+-------------------------------------------
						aDados := AClone(aDadosG81A[nG81])
			
						For nX := 1 To Len( aDados )
			
							If (nPos := aScan(aStruG81A, {|x| AllTrim(x[3])== AllTrim(aDados[nX][1])})) > 0
			
								If !(lRet := oModel:SetValue('G81A_DETAIL', aStruG81A[nPos][3], aDados[nX][2]))
									MsgAlert(STR0017 + aStruG81A[nPos][3]) //"Erro ao atribuir valor "
									Exit
								EndIf
			
							EndIf
			
						Next nX
					
						AADD(aCtbG81A,oModelG81A:GetValue('G81_IDIFA'))	
			
					EndIf
		
				Else
					nCont := nG81
					Exit 
				Endif
			
			Next nG81
	
		Next nG6M
		oModelG81A:SetNoInsertLine(.T.)
	EndIf
		
	//+-------------------------------------------
	//| Atualiza G4C e G48
	//+-------------------------------------------
	lRet := TA050LibG(nG6LRecno)
	
	If ( lRet )	
	
		//+-------------------------------------------
		//|	Atualiza status da apuração
		//+-------------------------------------------
		oModel:SetValue('G6L_MASTER', 'G6L_STATUS', '2')
		
		If (lRet := oModel:VldData())
			BEGIN TRANSACTION
				oModel:CommitData()
				If lOnLine
					TurCtOnApu(lOnline,lMostrLanc,lAglutLanc,aCtbG81A,aCtbG81B,"TURA050")
				EndIf
			END TRANSACTION
			
		Else
			TA050GetEr(oModel)
		EndIf
		
	Endif
	oModel:DeActivate()
	oModel:Destroy()
		
Endif

FwRestRows(aSaveRows)
RestArea(aArea)

aDados     := {}
aDadosG6L  := {}
aDadosG6M  := {}
aDadosG82  := {}
aDadosG81A := {}
aAux       := {}
aCtbG81A   := {}
aCtbG81B   := {}
DelClassIntF()

Return(lRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050ArG81
Função responsável pela montagem do array da G81.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050ArG81(aDados)

Local aRet    	:= {}
Local cGrpCod 	:= ''
Local cFilRef	:= aDados[AScan(aDados,{|x, y| x[1] == 'FILREF'})][2]
Local cTipoAc 	:= aDados[AScan(aDados,{|x, y| x[1] == 'TIPOAC'})][2]
Local cSegNeg	:= aDados[AScan(aDados,{|x, y| x[1] == 'SEGNEG'})][2]
Local cTipo   	:= POSICIONE('G8B', 1, xFilial('G8B') + cTipoAc, 'G8B_TIPO')
Local cPagRec	:= ""
Local cSegmento	:= ""
Local cMsg		:= ""


	cPagRec := TA50RetPagRec(G6L->G6L_FILIAL, G6L->G6L_CODAPU, cTipoAc, G6L->G6L_FORNEC, G6L->G6L_LJFORN)
	
	aAdd(aRet, {'G81_IDIFA' , GetSXENum('G81', 'G81_IDIFA')}) 
	
	ConfirmSx8()
	
	aAdd(aRet, {'G81_TIPO'  , cTipo})
	aAdd(aRet, {'G81_SEGNEG', aDados[AScan(aDados,{|x, y| x[1] == 'SEGNEG'})][2]})
	aAdd(aRet, {'G81_CLASS' , aDados[AScan(aDados,{|x, y| x[1] == 'TIPOAC'})][2]})
	aAdd(aRet, {'G81_CLIENT', G6L->G6L_CLIENT})
	aAdd(aRet, {'G81_LOJA'  , G6L->G6L_LOJA})
	aAdd(aRet, {'G81_EMISSA', dDataBase})
	aAdd(aRet, {'G81_PAGREC', cPagRec}) // 1=Pagar;2=Receber - deverá ser checado se há - pegar da G4C 
	aAdd(aRet, {'G81_MOEDA' , G6L->G6L_MOEDA})
	aAdd(aRet, {'G81_VALOR' , aDados[AScan(aDados,{|x, y| x[1] == 'TOTAL'})][2]})
	aAdd(aRet, {'G81_CONDPG', TURXCndPag(G6L->G6L_CLIENT, G6L->G6L_LOJA, aDados[AScan(aDados,{|x, y| x[1] == 'SEGNEG'})][2], /*cGrpCod*/, '2')})	
	aAdd(aRet, {'G81_STATUS', '1'})	// Liberado
	aAdd(aRet, {'G81_DTLIB' , dDataBase})
	aAdd(aRet, {'G81_FILREF', cFilRef})
	aAdd(aRet, {'G81_NATURE', aDados[AScan(aDados,{|x, y| x[1] == 'NATURE'})][2]})
	
Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050SeG81
Função responsável pela seleção da G80.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050SeG81(cCodApu)

Local aArea     := GetArea()
Local aAux      := {}
Local aRet      := {}
Local cAliasG6M := GetNextAlias()
Local nX        := 0
Local nValor	:= 0

BeginSql Alias cAliasG6M

	SELECT 
		G6M_CODAPU CODAPU,
	    G6M_SEGNEG SEGNEG,
	    G6M_FILREF FILREF,
	    G6M_TIPOAC TIPOAC,
	    G6M_NATURE NATURE,
	    SUM(G6M_TOTAL) TOTAL
	FROM 
		%Table:G6M% G6M
	WHERE 
		G6M_FILIAL = %xFilial:G6M% AND 
	    G6M_CODAPU = %Exp:cCodApu% AND
	    G6M.%NotDel% 
	GROUP BY 
		G6M_CODAPU, 
		G6M_SEGNEG, 
		G6M_FILREF,
		G6M_TIPOAC,
		G6M_NATURE
	ORDER BY 
		G6M_FILREF,
		G6M_TIPOAC
		
EndSql

While ( (cAliasG6M)->(!EOF()) )
	
	If ( (cAliasG6M)->TOTAL <> 0 )
	
		aAux := {}
		
		For nX := 1 To (cAliasG6M)->(FCount())
		
			If ( (cAliasG6M)->(FieldName(nX)) == "TOTAL" )
				
				If ( (cAliasG6M)->(FieldGet(nX)) < 0 )
					nValor := (cAliasG6M)->(FieldGet(nX)) * (-1)
				Else
					nValor := (cAliasG6M)->(FieldGet(nX))
				Endif
				
				aAdd(aAux, {(cAliasG6M)->(FieldName(nX)), nValor})
			
			Else	
				aAdd(aAux, {(cAliasG6M)->(FieldName(nX)), (cAliasG6M)->(FieldGet(nX))})	
			Endif	
		
			
		
		Next nX
		
		aAdd(aRet, AClone(aAux))
	
	Endif
	
	(cAliasG6M)->(DbSkip())
	
EndDo

(cAliasG6M)->(DbCloseArea())
RestArea(aArea)

Return(AClone(aRet))

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050LibG
Função responsável pela chamada da TURA034. 
Através dela serão atualizados os status da G48 e G4C.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050LibG(nG6LRecno, lEstorno)

Local aArea     := GetArea()
Local aSaveRows := FwSaveRows()
Local aAux		:= {}
Local aG48      := {}

Local cAliasG48 := GetNextAlias()
Local cCodApu   := ''

Local lRet		:= .t.

Default lEstorno := .F.

//+------------------------------------------------
//| Posiciona na Apuração
//+------------------------------------------------
G6L->(DbGoTo(nG6LRecno))

cCodApu := G6L->G6L_CODAPU

//+------------------------------------------------
// Seleciona Itens do Acordo
//+------------------------------------------------
BeginSql Alias cAliasG48
	Column G48_MARK as Logical
	
	SELECT	
		G48_FILIAL,
		G48_NUMID, 
		G48_IDITEM, 
		G48_NUMSEQ, 
		G48_APLICA, 
		G48_CODACD, 
		G48_FILAPU,
		G48_CODAPU,
		G48_CODREC,
	    G48_MARK, 
	    G48_CODPRD, 
	    G48_CLASS, 
	    G48_VLACD, 
	    G6M_SEGNEG SEGNEG
	FROM 
		%Table:G6M% G6M
	INNER JOIN 
		 %Table:G48% G48
	ON 
		G48_FILAPU = G6M_FILIAL AND
	    G48_CODAPU = G6M_CODAPU AND
	    G48_FILREF = G6M_FILREF AND
	    G48_CLASS = G6M_TIPOAC AND 
      	G48_SEGNEG = G6M_SEGNEG  AND 
      	G48_CONINU = ''  AND 
      	G48.%NotDel%
	WHERE 
		G6M_FILIAL = %xFilial:G6M% AND 
      	G6M_CODAPU = %Exp:cCodApu% AND 
	                              G6M.%NotDel%
	ORDER BY
		G48_FILAPU,
		G48_CODAPU,
		G48_CODREC,
		G48_FILREF, 
		G48_NUMID, 
		G48_IDITEM, 
		G48_NUMSEQ, 
		G48_CODACD, 
		G48_APLICA
	
EndSql

While (cAliasG48)->(!EOF())
	
	// Liberar apenas o G4C do Combo principal
	If ( (cAliasG48)->G48_VLACD == 0 )
		(cAliasG48)->(DbSkip())
		Loop
	EndIf 

	aAux := {}
	
	aAdd(aAux, {'G48_FILAPU' , (cAliasG48)->G48_FILAPU	})
	aAdd(aAux, {'G48_CODAPU' , (cAliasG48)->G48_CODAPU	})
	aAdd(aAux, {'G48_CODREC' , (cAliasG48)->G48_CODREC	})
	aAdd(aAux, {'G48_FILIAL' , (cAliasG48)->G48_FILIAL	})
	aAdd(aAux, {'SEGNEG'    , (cAliasG48)->SEGNEG    })
	aAdd(aAux, {'G48_NUMID' , (cAliasG48)->G48_NUMID	})
	aAdd(aAux, {'G48_IDITEM', (cAliasG48)->G48_IDITEM})
	aAdd(aAux, {'G48_NUMSEQ', (cAliasG48)->G48_NUMSEQ})
	aAdd(aAux, {'G48_APLICA', (cAliasG48)->G48_APLICA})
	aAdd(aAux, {'G48_CODACD', (cAliasG48)->G48_CODACD})
	aAdd(aAux, {'G48_CODPRD', (cAliasG48)->G48_CODPRD})
	aAdd(aAux, {'G48_CLASS' , (cAliasG48)->G48_CLASS })
	aAdd(aAux, {'G48_MARK'    , (cAliasG48)->G48_MARK    })

	If (cAliasG48)->G48_MARK
		aAdd(aAux, {'G48_CODAPU', (cAliasG48)->G48_CODAPU})
		aAdd(aAux, {'G48_STATUS', IIF(!lEstorno, '4', '1')})
	Else
		aAdd(aAux, {'G48_CODAPU', ''})
		aAdd(aAux, {'G48_STATUS', '1'})
	EndIf
	
	aAdd(aG48, AClone(aAux))
	
	(cAliasG48)->(DbSkip())
	
EndDo

(cAliasG48)->(DbCloseArea())

lRet := TA050Upd(aG48, !lEstorno, lEstorno)

FwRestRows(aSaveRows)
RestArea(aArea)

aAux := Nil
aG48 := Nil
DelClassIntF()

Return(lRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050RaDel
Função responsável pelo recálculo do rateio padrão ao deletar uma linha

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050RaDel(oView, cIdView, nNumLine)

Local oModel  := FwModelActive()
Local oMdlG6M := oModel:GetModel('G6M_DETAIL')
Local oMdlG6O := oModel:GetModel('G6O_DETAIL')
Local cTpVal  := oMdlG6O:GetValue('G6O_TIPOVL')
Local nValor  := 0
Local lRet    := .F.
Local nX      := 0
Local oMdlDet := Nil

Do Case
	Case cIdView == 'G6O_DETAIL'
		If oMdlG6O:Length() > 1
			If oModel:GetValue("G6M_DETAIL", "G6M_PERDES") > 0
				MsgInfo(STR0116, STR0114)	// "O Valor de Desconto será recalculado com base no % Desconto."###"Recálculo Desconto"
			EndIf
			If oModel:GetValue("G6M_DETAIL", "G6M_PERCTX") > 0
				MsgInfo(STR0117, STR0115)	// "O Valor da Taxa Adicional será recalculado com base no % Tx. Adic.."###"Recálculo Taxa Adicional"
			EndIf
		EndIf

		nValor := oMdlG6M:GetValue('G6M_VLACD') - oMdlG6O:GetValue('G6O_VAPLIC')

		If !oMdlG6O:IsDeleted(nNumLine)
			nValor := oMdlG6M:GetValue('G6M_VLACD') + oMdlG6O:GetValue('G6O_VAPLIC')
		EndIf

		If !(lRet := oModel:SetValue('G6M_DETAIL', 'G6M_VLACD', nValor))
			Help( , ,"TURA050", , STR0056, 1, 0) //"Erro ao excluir acordo."
		Else
			// Atualizando os itens de acordo com o tipo de acordo
			If oMdlG6O:GetValue('G6O_TIPOAC') $ TA050AcordToString() 
				nValor  := 0
				oMdlDet := oModel:GetModel('G48A_DETAIL')

				For nX := 1 To oMdlDet:Length()
					oMdlDet:GoLine(nX)

					// Caso a linha G6O esteja excluída desmarcar os acordos, do contrário deve marca-los
					oMdlDet:LoadValue('G48_MARK', IIF(oMdlG6O:IsDeleted(nNumLine), .F., .T.))

					If oMdlDet:GetValue('G48_MARK')
						nValor += oMdlDet:GetValue(IIF(cTpVal == '1', 'G48_VLBASE', 'G48_VLACD'))
					EndIf
				Next nX
				If oMdlG6O:IsDeleted(nNumLine)
					oMdlG6O:LoadValue(IIF(cTpVal == '1', 'G6O_VLBASE', 'G6O_VALOR'), 0)
					oMdlG6O:LoadValue('G6O_PERCEN', 0)
					oMdlG6O:LoadValue('G6O_VAPLIC', 0)
				Else
					oMdlG6O:SetValue(IIF(cTpVal == '1', 'G6O_VLBASE', 'G6O_VALOR'), nValor)
					If cTpVal == '1'
						nPercen := Posicione('G4W', 1, xFilial('G4W') + oMdlG6O:GetValue('G6O_CODACD') + oMdlG6O:GetValue('G6O_CODREV'), 'G4W_VALOR')
						oMdlG6O:SetValue('G6O_PERCEN', nPercen)
					EndIf
					oMdlG6O:SetValue('G6O_VAPLIC', IIF(cTpVal == '1', oMdlG6O:GetValue('G6O_VLBASE') * (oMdlG6O:GetValue('G6O_PERCEN')/100), oMdlG6O:GetValue(IIF(cTpVal == '1', 'G6O_VLBASE', 'G6O_VALOR'))) * IIF(oMdlDet:GetValue('G48_TPACD') == '1', 1, -1))
				EndIf					
			EndIf 				
		EndIf
		oView:Refresh()		
EndCase

Return()

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050EstL
Função responsável pelo estorno da liberação da apuração.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050EstL()

Local oModel     := FWLoadModel('TURA050')
Local oModelG6M  := oModel:GetModel('G6M_DETAIL')
Local oModelG81A := oModel:GetModel('G81A_DETAIL')
Local aArea      := GetArea()
Local aSaveRows  := FwSaveRows()
Local nG6LRecno  := G6L->(Recno())
Local nG6M       := 0
Local nG82       := 0
Local cCodApu    := G6L->G6L_CODAPU
Local aCtbG81A	 := {}
Local aCtbG81B	 := {}
Local lOnLine	 := .F.
Local lMostrLanc := .F.
Local lAglutLanc := .F.
Local lRet		 := .t.
Local lExist	 := .F.

lExist := TURExistX1("TURA050C")

If (lExist) 
	Pergunte("TURA050C",.F.)
Endif

lOnLine 	:= .T.	//Sempre será on-line no estorno
lMostrLanc	:= Iif(lExist .And. mv_par02 == 1,.T.,.F.)
lAglutLanc	:= Iif(lExist .And. mv_par03 == 1,.T.,.F.)

//+-------------------------------------------------
//| Exclui IF da Apuração
//+-------------------------------------------------
oModel:SetOperation(MODEL_OPERATION_UPDATE)

If oModel:Activate()
	
	oModelG81A:SetNoDeleteLine(.F.)
	
	For nG6M := 1 To oModelG6M:Length()
		oModelG6M:GoLine(nG6M)
		AADD(aCtbG81A,oModelG81A:GetValue('G81_IDIFA'))
		oModelG81A:DelAllLine()
	Next nG6M
	
	oModelG81A:SetNoDeleteLine(.T.)
	
	//+-------------------------------------------
	//| Atualiza G4C e G48
	//+-------------------------------------------
	lRet := TA050LibG(nG6LRecno, .T.)
	
	If ( lRet )
	
		//+-------------------------------------------
		//|	Atualiza status da apuração
		//+-------------------------------------------
		oModel:SetValue('G6L_MASTER', 'G6L_STATUS', '1')
		
		If (lRet := oModel:VldData())
			
			BEGIN TRANSACTION
				
			TurCtOnEAp(oModel,lOnLine,lMostrLanc,lAglutLanc,aCtbG81A,aCtbG81B,"TURA050")
				
			oModel:CommitData()
				
			END TRANSACTION
				
		Else
			TA050GetEr(oModel)
		EndIf
	
	EndIf
	
	oModel:DeActivate()
	
Endif
oModel:Destroy()

FwRestRows(aSaveRows)
RestArea(aArea)

aCtbG81A := Nil
aCtbG81B := Nil
DelClassIntF()

Return()

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050Alterar
Função responsável pela montagem da View sem o botão "Salvar e Criar Novo"

@type 		Function
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA050Alterar()

Local aButtons  := {{.F., Nil}, {.F., Nil}    , {.F., Nil}    , {.F., Nil}, {.F., Nil}, ;
                    {.F., Nil}, {.T., STR0058}, {.T., STR0057}, {.F., Nil}, {.F., Nil}, ;//"Cancelar"//"Confirmar"
                    {.F., Nil}, {.F., Nil}    , {.F., Nil}    , {.F., Nil}}


 FWExecView(STR0006,"VIEWDEF.TURA050",4,,{|| .T.},,,aButtons) 
		

Return

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA050AcordToString
Função que retorna uma string com os tipos de acordos de fornecedor, separados pelo token
passado via parâmetro.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Static Function TA050AcordToString(cToken)

Local cString 	:= ""
Local nI		:= 0

Default cToken  := "/"

For nI := 1 To Len(aTA050Acordo) 
	cString += Alltrim(aTA050Acordo[nI]) + IIF(nI < Len(aTA050Acordo), cToken, "")
Next nI

Return cString

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA050GetTpAcordo
Função que retorna um array com os tipos de acordos de fornecedor. caso seja passado o pa-
râmetro cTipoExc, os acordos dele serão ignorados

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Static Function TA050GetTpAcordo(cTipoExc)

Local cAliasG8B	:= ""
Local aRet		:= {}

If ( Len(aTA050Acordo) == 0 )
	
	cAliasG8B := GetNextAlias()
	
	BeginSQL Alias cAliasG8B
	
		SELECT
			DISTINCT
			G8B_CODIGO
		FROM
			%Table:G8B% G8B
		WHERE
			G8B_FILIAL = %XFILIAL:G8B%
			AND G8B_CODIGO LIKE 'F%'
			AND G8B_CLIFOR = '2'			
			AND G8B.%NotDel%
			
	EndSQL
	
	While ( !(cAliasG8B)->(Eof()) )
		
		If !( Alltrim(Upper((cAliasG8B)->G8B_CODIGO)) $ Alltrim(Upper(cTipoExc)) )
			aAdd(aTA050Acordo, (cAliasG8B)->G8B_CODIGO )
		Endif
			
		(cAliasG8B)->(DbSkip())
	EndDo
	
	(cAliasG8B)->(DbCloseArea())

Endif

Return(aTA050Acordo)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA050Query
Função responsável por montar a consulta dos Itens de Vendas com acordos de fornecedores
aplicados. Esta query utiliza a parametrização definida pelo usuário, na primeira tela (pergunte)
da rotina de Apuração de Receita de Fornecedor

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Static Function TA050Query(cGrpProd, cTipoDest, aFornecedor, aTpAcordo, dApurarDe, dApurarAte, lSegCorp, lSegEven, lSegLazer, nTipoData) 

Local cAliasAux		:= GetNextAlias()
Local cAuxWhere		:= ""
Local cTpAcordo		:= ""
Local cCampo		:= ""
Local nI			:= 0
Local aSegmentos	:= {}

Default aFornecedor	:= {}

Do Case
	Case nTipoData == 1 //Inclusão
		cCampo := '%EMISSAO%' 
	Case nTipoData == 2//Check-IN
		cCampo := '%DTINI%'
	Case nTipoData == 3//CHECK-OUT 
		cCampo := '%DTFIM%' 	
EndCase


If ( Len(aFornecedor) == 0 )
	aFornecedor := {{"", ""}, {Replicate('Z', TamSx3('A2_COD')[1]), Replicate('Z', TamSx3('A2_LOJA')[1])}}
Endif

For nI := 1 to Len(aTpAcordo)
	cTpAcordo += "'" + aTpAcordo[nI] + "'" + Iif(nI < Len(aTpAcordo), ",","")
Next nI

//Filtro dos tipos de Acordos de Fornecedores
If ( !Empty(cTpAcordo) )
	cAuxWhere := "% AND G4C_CLASS IN (" + cTpAcordo + ") " + chr(13)
Endif	

//Filtro dos Segmentos de Negócio selecionados pelo usuário - Início
If ( lSegCorp .or. lSegEven .or. lSegLazer )
	
	//Segmento Corporativo
	If ( lSegCorp )
		AAdd(aSegmentos, "'1'")
	Endif 
	
	//Segmento Eventos
	If ( lSegEven )
		AAdd(aSegmentos, "'2'")
	Endif
	
	//Segmento Lazer
	If ( lSegLazer )
		AAdd(aSegmentos, "'3'")
	Endif

	If ( Len(aSegmentos) > 0 )
		
		If ( Empty(cAuxWhere) )
			cAuxWhere := "%"
		Endif	
		
		cAuxWhere += " AND G4C_SEGNEG IN ("
		
		For nI := 1 to Len(aSegmentos)
			cAuxWhere += aSegmentos[nI] + Iif(nI < Len(aSegmentos), ",", "") 
		Next nI	
		
		cAuxWhere += ")"
		
	Endif

Endif
//Filtro dos Segmentos de Negócio selecionados pelo usuário - Fim

If ( !Empty(cAuxWhere) )
	cAuxWhere += "%"
Endif

cFilG4C := "%" + TurFilFilter("G4C",.T.,.T.) + "%"

BeginSQL Alias cAliasAux

SELECT 
  G4C_CODIGO,
  G4C_LOJA,
  G4C_GRPPRD,
  G4C_CODPRO,
  G4C_DESTIN,
  G4C_MOEDA,
  G4C_SEGNEG,
  G4C_CLASS,
  G4W_CODACO,
  G4W_CODREV,
  G4W_DESCRI,
  G4W_BASCAL,
  G4W_TPVAL,
  G4W_VALOR,
  G4W_TPACO,
  G48_FILREF,
  G48_TPACD,
  G48_NUMID,
  G48_IDITEM,
  G48_NUMSEQ,
  G48_APLICA,
  G48_MARK,
  G48_CODAPU,
  G48_VLBASE,
  G48_VLACD,
  G3R_VLRSER,
  G48_RECNO,
  G3R_RECNO,
  G3P_RECNO,
  MAX(DTFIM) DTFIM
FROM 
	(SELECT
		G4C_CODIGO,
		G4C_LOJA, 
		G4C_GRPPRD,
		G4C_CODPRO,
		G4C_DESTIN,
		G4C_MOEDA,
		G4C_SEGNEG,
		G4C_CLASS,
		G4W_CODACO,
		G4W_CODREV,
		G4W_DESCRI,
		G4W_BASCAL,
		G4W_TPVAL,
		G4W_VALOR,
		(
			SELECT
				G8B_TIPO
			FROM
				%Table:G8B% G8B
			WHERE
				G8B.%NotDel%
				AND G8B_FILIAL = %XFilial:G8B%
				AND G8B_CODIGO = G4C_CLASS
		) G4W_TPACO,
		  G48_FILREF,	
		  G48_TPACD,
		  G48_NUMID,
		  G48_IDITEM,
		  G48_NUMSEQ,
		  G48_APLICA,
		  G48_MARK,
		  G48_CODAPU,
		  G48_VLBASE,
		  G48_VLACD,
		  G3R_VLRSER,
		  G48.R_E_C_N_O_ G48_RECNO,
		  G3R.R_E_C_N_O_ G3R_RECNO,
		  G3P.R_E_C_N_O_ G3P_RECNO,
		  COALESCE(G3T_DTSAID,G3U_DTFIM,G3V_DTFIM,G3W_DTFIM,G3X_DTFIM,G3Y_DTFIM,G3Z_DTFIM,G40_DTFIM,G41_DTFIM,G43_DTFIM,G48_DTINCL) DTFIM,
		  COALESCE(G3T_DTCHEG,G3U_DTINI,G3V_DTINI,G3W_DTINI,G3X_DTINI,G3Y_DTINI,G3Z_DTINI,G40_DTINI,G41_DTINI,G43_DTINI)DTINI,
		  G3R.G3R_EMISS EMISSAO
	FROM
		%Table:G4C% G4C
	INNER JOIN
		%Table:G48% G48
	ON
		G4C_FILIAL = G48_FILIAL
		AND G4C_NUMID = G48_NUMID
		AND G4C_IDITEM = G48_IDITEM
		AND G4C_NUMSEQ = G48_NUMSEQ
		AND G4C_APLICA = G48_APLICA
		AND G4C_CLIFOR = G48_CLIFOR
		AND G48_STATUS = '1'														//Status do Item Acordado - fixado em '1' (Em aberto)
		AND G48_CODAPU = ''
		AND G48_CONINU = ''
		AND G48.%NotDel%
	INNER JOIN  
		%Table:G4W% G4W
	ON 
		G48_FILIAL LIKE RTRIM(G4W_FILIAL) + '%' 
		AND G48_CODACD = G4W_CODACO
		AND G48_CODREC = G4W_CODREV
		AND G4W.%NotDel%
	INNER JOIN
		%Table:G3R% G3R
	ON
		G48_FILIAL = G3R_FILIAL
		AND G48_NUMID = G3R_NUMID
		AND G48_NUMSEQ = G3R_NUMSEQ
		AND G48_IDITEM = G3R_IDITEM
		AND G3R_CONINU = ''
		AND G3R.%NotDel%
	INNER JOIN
		%Table:G3P% G3P
	ON
		G3R_FILIAL = G3P_FILIAL
		AND G3R_NUMID = G3P_NUMID
		AND G3P.%NotDel%
  	LEFT JOIN %Table:G3T% G3T
		ON G3T_FILIAL = G3R_FILIAL
			AND G3T_NUMID = G3R_NUMID
			AND G3T_IDITEM = G3R_IDITEM
			AND G3T_NUMSEQ = G3R_NUMSEQ
			AND G3T.%NotDel%
			AND G3T.G3T_CONINU = ''
  	LEFT JOIN %Table:G3U% G3U
		ON G3U_FILIAL = G3R_FILIAL
			AND G3U_NUMID = G3R_NUMID
			AND G3U_IDITEM = G3R_IDITEM
			AND G3U_NUMSEQ = G3R_NUMSEQ
			AND G3U.%NotDel%
			AND G3U.G3U_CONINU = ''
	LEFT JOIN %Table:G3V% G3V
		ON G3V_FILIAL = G3R_FILIAL
			AND G3V_NUMID = G3R_NUMID
			AND G3V_IDITEM = G3R_IDITEM
			AND G3V_NUMSEQ = G3R_NUMSEQ
			AND G3V.%NotDel%
			AND G3V.G3V_CONINU = ''
	LEFT JOIN %Table:G3W% G3W
		ON G3W_FILIAL = G3R_FILIAL
			AND G3W_NUMID = G3R_NUMID
			AND G3W_IDITEM = G3R_IDITEM
			AND G3W_NUMSEQ = G3R_NUMSEQ
			AND G3W.%NotDel%
			AND G3W.G3W_CONINU = ''
	LEFT JOIN %Table:G3X% G3X
		ON G3X_FILIAL = G3R_FILIAL
			AND G3X_NUMID = G3R_NUMID
			AND G3X_IDITEM = G3R_IDITEM
			AND G3X_NUMSEQ = G3R_NUMSEQ
			AND G3X.%NotDel%
			AND G3X.G3X_CONINU = ''
	LEFT JOIN %Table:G3Y% G3Y
		ON G3Y_FILIAL = G3R_FILIAL
			AND G3Y_NUMID = G3R_NUMID
			AND G3Y_IDITEM = G3R_IDITEM
			AND G3Y_NUMSEQ = G3R_NUMSEQ
			AND G3Y.%NotDel%
			AND G3Y.G3Y_CONINU = ''
	LEFT JOIN %Table:G3Z% G3Z
		ON G3Z_FILIAL = G3R_FILIAL
			AND G3Z_NUMID = G3R_NUMID
			AND G3Z_IDITEM = G3R_IDITEM
			AND G3Z_NUMSEQ = G3R_NUMSEQ
			AND G3Z.%NotDel%
			AND G3Z.G3Z_CONINU=''
	LEFT JOIN %Table:G40% G40
		ON G40_FILIAL = G3R_FILIAL
			AND G40_NUMID = G3R_NUMID
			AND G40_IDITEM = G3R_IDITEM
			AND G40_NUMSEQ = G3R_NUMSEQ
			AND G40.%NotDel%
			AND G40.G40_CONINU = ''
	LEFT JOIN %Table:G41% G41
		ON G41_FILIAL = G3R_FILIAL
			AND G41_NUMID = G3R_NUMID
			AND G41_IDITEM = G3R_IDITEM
			AND G41_NUMSEQ = G3R_NUMSEQ
			AND G41.%NotDel%
			AND G41.G41_CONINU=''
	LEFT JOIN %Table:G42% G42
		ON G42_FILIAL = G3R_FILIAL
			AND G42_NUMID = G3R_NUMID
			AND G42_IDITEM = G3R_IDITEM
			AND G42_NUMSEQ = G3R_NUMSEQ
			AND G42.%NotDel%
			AND G42.G42_CONINU=''
	LEFT JOIN %Table:G43% G43
		ON G43_FILIAL = G3R_FILIAL
			AND G43_NUMID = G3R_NUMID
			AND G43_IDITEM = G3R_IDITEM
			AND G43_NUMSEQ = G3R_NUMSEQ
			AND G43.%NotDel%		
			AND G43.G43_CONINU=''
	WHERE
		%Exp:cFilG4C%
		AND G4C_STATUS = '2'														//fixado em '2' (Aguardando Liberação)
		AND G4C_CLIFOR = '2'														//Itens de fornecedor - fixado em '2' (Fornecedor)
		%Exp:cAuxWhere%
		AND G4C_TIPO IN ('3','4') 															//Item de Receita - fixado em '3'
		AND G4C_CODIGO BETWEEN %Exp:aFornecedor[1,1]% AND %Exp:aFornecedor[2,1]%	//codigo do fornecedor de/ate
		AND G4C_LOJA BETWEEN %Exp:aFornecedor[1,2]% AND %Exp:aFornecedor[2,2]%		//loja do fornecedor de/ate
		AND G4C_GRPPRD = %Exp:cGrpProd%												//grupo de produto - cGrpProd
		AND G4C_DESTIN = %Exp:cTipoDest%											//Destino de viagem (1-Nacional,2-Internacional) - cTipoDest 
		AND G4C_APLICA <> ''
		AND G4C_CONINU = ''
		AND G4C_CONORI = ''
		AND G4C_CODIGO <> ''
		AND G4C_LOJA <> ''
		AND G4C.%NotDel% 
	) TMP
	WHERE %Exp:cCampo% BETWEEN %Exp:dApurarDe% AND %Exp:dApurarAte%				//	AND G48_DTINCL BETWEEN %Exp:dApurarDe% AND %Exp:dApurarAte%					//data de inclusão do item acordado, será de/ate - dApurarDe/dApurarAte //osmar
	GROUP BY
	  G4C_CODIGO,
	  G4C_LOJA,
	  G4C_GRPPRD,
	  G4C_CODPRO,
	  G4C_DESTIN,
	  G4C_MOEDA,
	  G4C_SEGNEG,
	  G4C_CLASS,
	  G4W_CODACO,
	  G4W_CODREV,
	  G4W_DESCRI,
	  G4W_BASCAL,
	  G4W_TPVAL,
	  G4W_VALOR,
	  G4W_TPACO,
	  G48_FILREF,
	  G48_TPACD,
	  G48_NUMID,
	  G48_IDITEM,
	  G48_NUMSEQ,
	  G48_APLICA,
	  G48_MARK,
	  G48_CODAPU,
	  G48_VLBASE,
	  G48_VLACD,
	  G3R_VLRSER,
	  G48_RECNO,
	  G3R_RECNO,
	  G3P_RECNO
	ORDER BY
		G4C_CODIGO, 
		G4C_LOJA, 
		G48_FILREF,
		G4C_GRPPRD, 
		G4C_MOEDA,
		G4C_SEGNEG,
		G4C_CLASS,
		G4W_CODACO,
		G4W_CODREV,
		G48_NUMID,
		G48_IDITEM

EndSQL
	
If ( (cAliasAux)->(Eof()) )
	(cAliasAux)->(DbCloseArea())
	cAliasAux := ""
Endif	
	
Return(cAliasAux)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA50CondTrig
Função utilizada no campo X7_CONDIC dos gatilhos das tabelas que são utilizadas nas apurações,
tanto de clientes quanto de fornecedores.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------

Function TA50CondTrig(cCalled)

Local oModel

Local lRet := .T.

Default cCalled := ""

If ( !Empty(cCalled) )
	
	If ( cCalled == "TURA040" )
		
		oModel := FwModelActive()
	
		lRet := oModel:GetOperation() <> 3 .And. FWISINCALLSTACK('TURA040') 
		
	EndIf
	
Else
	lRet := FWISINCALLSTACK('TURA040') .or. FWISINCALLSTACK('TURA050')	
EndIf	 

Return(lRet)


//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA50InitFld
Função responsável por carregar dados para os segmentos de produtos
 
@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Static Function TA50InitFld(oView)

	TA050HiFol( oView )
	TA050VlFol()
	oView:SelectFolder("F_DETALHES", 1, 2)
	oView:Refresh()

Return()

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA50RetPagRec
Função responsável por retornar o tipo de item financeiro. Se ele é um item de receita ou
de despesa.
 
@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------

Static Function TA50RetPagRec(cFilApura, cCodApura, cTpAcordo, cFornece, cLjForn)

Local cAliasAux	:= GetNextAlias()
Local cConinu  	:= Space(TamSx3("G4C_CONINU")[1])
Local cRet		:= ""

BeginSQL Alias cAliasAux

	SELECT
		PAG.VALOR VAL_PAGAR,
		REC.VALOR VAL_REC
	FROM
	(
		SELECT 
			Sum(G4C_VALOR) VALOR,
			'PAGAR' TIPO,
			'X' CPO_COMUM
		FROM 
			%Table:G48% G48
		INNER JOIN
			%Table:G4C% G4C
		ON
			G4C_FILIAL = G48_FILIAL
			AND G4C_NUMID = G48_NUMID
			AND G4C_IDITEM = G48_IDITEM
			AND G4C_NUMSEQ = G48_NUMSEQ
			AND G4C_NUMACD = G48_CODACD
			AND G4C_APLICA = G48_APLICA
			AND G4C_CODIGO = %Exp:cFornece%
			AND G4C_LOJA = %Exp:cLjForn%
			AND G4C_PAGREC = '1'
			AND G4C_CONINU = G48_CONINU
			AND G4C.%NotDel%			
		WHERE
			G48_FILAPU = %Exp:cFilApura%
			AND G48_CODAPU = %Exp:cCodApura%
			AND G48_CLASS = %Exp:cTpAcordo%
			AND G48_CONINU = %Exp:cConinu%
			AND G48_CLIFOR = '2'
			AND	G48.%NotDel%		
						
	) PAG
	INNER JOIN
	(
		SELECT 
			Sum(G4C_VALOR) VALOR,
			'RECEBER' TIPO,
			'X' CPO_COMUM
		FROM 
			%Table:G48% G48
		INNER JOIN
			%Table:G4C% G4C
		ON
			G4C_FILIAL = G48_FILIAL
			AND G4C_NUMID = G48_NUMID
			AND G4C_IDITEM = G48_IDITEM
			AND G4C_NUMSEQ = G48_NUMSEQ
			AND G4C_NUMACD = G48_CODACD
			AND G4C_APLICA = G48_APLICA
			AND G4C_CODIGO = %Exp:cFornece%
			AND G4C_LOJA = %Exp:cLjForn%
			AND G4C_PAGREC = '2'
			AND G4C_CONINU = G48_CONINU
			AND G4C.%NotDel%			
		WHERE
			G48_FILAPU = %Exp:cFilApura%
			AND G48_CODAPU = %Exp:cCodApura%
			AND G48_CLASS = %Exp:cTpAcordo%
			AND G48_CONINU = %Exp:cConinu%
			AND G48_CLIFOR = '2'
			AND	G48.%NotDel%						
	) REC
	ON
		PAG.CPO_COMUM = REC.CPO_COMUM

EndSQL

cRet := IIf((cAliasAux)->VAL_PAGAR > (cAliasAux)->VAL_REC, "1", "2")

(cAliasAux)->(DbCloseArea())

Return(cRet)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA050GetFile
Função utilizada no jogo de perguntas TUR50A, para efetuar a apresentação da tela de 
seleção de diretórios padrão do sistema.
 
@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Function TA050GetFile(cDirFile)

cDirFile := ""
cDirFile := cGetFile(,STR0075,0,"",.T.,GETF_RETDIRECTORY+GETF_LOCALHARD+GETF_NETWORKDRIVE)//"Selecione"

Return(.t.)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA050Report
Função chamada pelo menu, responsável por invocar o programa de impressão do demonstrativo
 de cobranças
 
@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Function TA050Report()

If ( FindFunction("U_TURR050A") )
	U_TURR050A()
Else
	TURR050()
Endif

Return()

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA050TgPerg
Função utilizada no jogo de perguntas TUR50A. Ela atualiza o conteúdo do campo 
'Fornecedor/loja até', de acordo com o que foi preenchido nos respectivos campos 
'fornecedor/loja de'
 
@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------

Function TA050TgPerg()

Local lRet	:= .t.


Do Case
Case ( Upper(Alltrim(ReadVar())) == "MV_PAR03" )
	
	If ( !Empty(MV_PAR03) )
		
		If ( Alltrim(Upper(mv_par03)) <> Replicate("Z",TamSx3("A2_COD")[1]) )
			lRet := ExistCpo("SA2") 
		Endif	
		
	Endif
	
	If ( lRet )
		MV_PAR05 := MV_PAR03
		MV_PAR06 := MV_PAR04
	Endif
			
Case ( Upper(Alltrim(ReadVar())) == "MV_PAR04" )

	If ( !Empty(MV_PAR04) )
		
		If ( Alltrim(Upper(mv_par04)) <> Replicate("Z",TamSx3("A2_LOJA")[1]) )
			lRet	:= ExistCpo("SA2", PadR(MV_PAR03,TamSx3("A2_COD")[1]) + Alltrim(MV_PAR04) ) 
		Endif	
	
	Endif

	If ( lRet )
		MV_PAR06 := MV_PAR04
	Endif

Case ( Upper(Alltrim(ReadVar())) == "MV_PAR05" )
	
	If ( !Empty(MV_PAR05) )
		
		If ( Alltrim(Upper(MV_PAR05)) <> Replicate("Z",TamSx3("A2_COD")[1]) )
			lRet := ExistCpo("SA2") 
		Endif	
		
	Endif
	
Case ( Upper(Alltrim(ReadVar())) == "MV_PAR06" )

	If ( !Empty(MV_PAR06) )
		
		If ( Alltrim(Upper(MV_PAR06)) <> Replicate("Z",TamSx3("A2_LOJA")[1]) )
			lRet	:= ExistCpo("SA2", PadR(MV_PAR05,TamSx3("A2_COD")[1]) + Alltrim(MV_PAR06) ) 
		Endif	
	
	Endif
End Case

Return(lRet)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA50DemApura
Função que atualiza a tabela G6L - Apuração com os seguintes dados, de acordo com o array
 aDados:
 
 aDados - Array. Contem as informações que serão atualizadas no Cabeçalho da Apuração (G6L) 
		aDados[1] - Conteúdo que irá para G6L_DEMONS: (Demonstrativo: 1-Sim;2-Não)
		aDados[2] - Conteúdo que irá para G6L_DEMENV: (Se o e-mail foi enviado "1" / 
					Se o email não foi enviado, pois o fornecedor não aceita envio "2" / 
					Se o email não foi enviado pq o email do fornecedor não foi cadastrado) 
		aDados[3] - Conteúdo que irá para G6L_EMAILD: com o endereço de e-mail informado no 
					parâmetro "Remetente"
		aDados[4] - Conteúdo que irá para G6L_EMAILP: com o endereço de e-mail do 
					fornecedor (G4V ou SA2)
					
@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Function TA50DemApura(cIdModel, cApuracao, aDados)

Local aArea   := GetArea()
Local lRet	  := .T.
Local oMdlG6L := Nil

Default aDados := {}

If ( Len(aDados) > 0 )

	G6L->(DbSetOrder(1)) //G6L_FILIAL+G6L_CODAPU
	If ( G6L->(DbSeek(XFilial("G6L") + Padr(cApuracao,TamSx3("G6L_CODAPU")[1]))) )
		If (lRet := RecLock('G6L', .F.))		
			G6L->G6L_DEMONS := aDados[1]
			G6L->G6L_DEMENV := aDados[2]
			G6L->G6L_EMAILD := aDados[3]
			G6L->G6L_EMAILP := aDados[4]
			G6L->(MsunLock())
		EndIf
	Else
		lRet := .f.							
	Endif
		
Endif

RestArea(aArea)

Return(lRet)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA050InitPad
Função utilizada para o inicializador padrão do campo G6O_DCACD. Esta função retorna a 
descrição do Acordo (que pode ser proveniente dos acordos de fornecedor, clientes e metas). 
 
@type 		Function
@author 	Fernando Radu Muscalu
@since 		03/05/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Function TA050InitPad() 

Local cRet	:= ""

If ( FWIsInCallStack("TURA050") )
	cRet := Posicione("G4W", 1, XFilial("G4W") + G6O->G6O_CODACD + G6O->G6O_CODREV, "G4W_DESCRI")
ElseIf ( FWIsInCallStack("TURA040") )
	cRet := Posicione('G5V', 1, XFILIAL('G5V') + G6O->G6O_CODACD + G6O->G6O_CODREV, 'G5V_DESCRI')
Else
	cRet := Posicione('G6D', 1, XFILIAL('G6D') + G6O->G6O_CODACD + G6O->G6O_CODREV, 'G6D_DESCRI')
Endif	

Return(cRet)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA050VldApura
Função utilizada pelo jogo de Perguntas TUR50D com o intuíto de validar o código da apuração 
digitado.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		03/05/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Function TA050VldApura(cContent)

Return(Vazio() .or. ( At("ZZ",Upper(cContent)) > 0 ) .or. ExistCpo('G6L'))

/*/{Protheus.doc} TipoSegm
Retorna o tipo de segmento (Aereo,Hotel,Carro,etc) relacionado ao Acordo
@type function
@author osmar.junior
@since 13/07/2016
@version 1.0
/*/
Static Function TipoSegm()

Local cTpseg   := ''
Local cChave   := ""
Local oModel   := FwModelActive()
Local oMdlG48A := oModel:GetModel('G48A_DETAIL')

cChave := Padr(oMdlG48A:GetValue('G48_FILREF'), TamSx3("G3Q_FILIAL")[1]) //XFilial("G3Q")
cChave += Padr(oMdlG48A:GetValue('G48_NUMID') , TamSx3("G3Q_NUMID" )[1])
cChave += Padr(oMdlG48A:GetValue('G48_IDITEM'), TamSx3("G3Q_IDITEM")[1])
cChave += Padr(oMdlG48A:GetValue('G48_NUMSEQ'), TamSx3("G3Q_NUMSEQ")[1])

dbSelectArea("G3Q")
G3Q->(dbSetOrder(1))

If G3Q->(dbSeek(cChave))
	cTpseg := G3Q->G3Q_TPSEG
EndIf

Return cTpseg

/*/{Protheus.doc} TA050HiFol
Função que retira da visualização as abas de segmento
@type function
@author osmar.junior
@since 13/07/2016
@version 1.0
@param oView, objeto, (Descrição do parâmetro)
/*/
Static Function TA050HiFol( oView )

	oView:HideFolder("F_DETALHES", 3, 2) //Aéreo
	oView:HideFolder("F_DETALHES", 4, 2) //Hotel
	oView:HideFolder("F_DETALHES", 5, 2) //Carro
	oView:HideFolder("F_DETALHES", 6, 2) //Rodoviário
	oView:HideFolder("F_DETALHES", 7, 2) //Cruzeiro
	oView:HideFolder("F_DETALHES", 8, 2) //Trem
	oView:HideFolder("F_DETALHES", 9, 2) //Visto
	oView:HideFolder("F_DETALHES", 10, 2) //Seguro
	oView:HideFolder("F_DETALHES", 11, 2) //Tour
	oView:HideFolder("F_DETALHES", 12, 2) //Pacote
	oView:HideFolder("F_DETALHES", 13, 2) //Outros
Return

/*/{Protheus.doc} TA050VlFol
Função utilizada para apresentar somente a aba referente ao segmento
@type function
@author osmar.junior
@since 13/07/2016
@version 1.0
@param cFolderID, character, (Descrição do parâmetro)
@param nOldSheet, numérico, (Descrição do parâmetro)
@param nSelSheet, numérico, (Descrição do parâmetro)
/*/
Static Function TA050VlFol( cFolderID, nOldSheet, nSelSheet )
Local cTpseg	:= TipoSegm()
Local oView := FwViewActive()
	
TA050HiFol( oView )
oView:SelectFolder("F_DETALHES", Val(cTpseg)+2, 2)
oView:SelectFolder("F_DETALHES", 1, 2)
	
Return


/*/{Protheus.doc} TA050ChLin
Função executada a partir da movimentação do grid de acordos
@type function
@author osmar.junior
@since 13/07/2016
@version 1.0
@param oView, objeto, (Descrição do parâmetro)
@param cViewID, character, (Descrição do parâmetro)
/*/
Static Function TA050ChLin( oView, cViewID )

Local nLine := oView:GetModel(cViewID):GetLine() 

TA050VlFol( )

oView:GetModel(cViewID):GoLine(nLine)

Return

//---------------------------------------------------------------------------------
/*/{Protheus.doc} TA050QryAnt()
Seleciona as apurações anteriores em aberto a serem deletadas.

@sample		TA050QryAnt(aFornecedor, dApuradoDe, dApuradoAte, cGrpProd)
@param		aFornecedor		Range de Fornecedores
			dApuradoDe		Data Apuração Inicial
			dApuradoAte		Data Apuração Final
			cGrpProd		Grupo de Produto
			cTipoDest		Destino
@return		aRet			Array Apurações
@author 	Cristiane Nishizaka
@since 		29/07/2016
@version 	P12
/*/
//---------------------------------------------------------------------------------
Static Function TA050QryAnt(aFornecedor, dApuradoDe, dApuradoAte, cGrpProd, cTipoDest)

Local aArea     	:= GetArea()
Local cAliasAp		:= GetNextAlias()
Local aRet			:= {}
Local cDestino		:= "%"
Local cFilG6L		:= "%" + TurFilFilter("G6L",.T.,.T.) + "%"

Default aFornecedor	:= {}
Default dApuradoDe	:= ''
Default dApuradoAte	:= ''
Default cGrpProd    := ''
Default cTipoDest   := ''

If cTipoDest != '3'		// diferente de ambos, valida o destino
	cDestino += "AND G6L.G6L_DESTIN = '" + cTipoDest + "'%"
Else	// igual ambos, n㯠valida o destino
	cDestino += "%"
EndIf

If ( Len(aFornecedor) == 0 )
	aFornecedor := {{"", ""}, {Replicate('Z', TamSx3('A2_COD')[1]), Replicate('Z', TamSx3('A2_LOJA')[1])}}
EndIf

If Empty(dApuradoAte)
	dApuradoAte := Replicate('Z', TamSx3('A2_COD')[1])
EndIf

BeginSQL Alias cAliasAp

	SELECT G6L.G6L_FILIAL, G6L.G6L_CODAPU
	FROM %Table:G6L% G6L
	WHERE 
	%Exp:cFilG6L%
	AND G6L.G6L_TPAPUR = '3'														//Apuração de Fornecedor
	AND G6L.G6L_FORNEC BETWEEN %Exp:aFornecedor[1,1]% AND %Exp:aFornecedor[2,1]% 	//Fornecedor De/Até
	AND G6L.G6L_LJFORN BETWEEN %Exp:aFornecedor[1,2]% AND %Exp:aFornecedor[2,2]%	//Fornecedor Loja De/Até
	AND G6L.G6L_GRPPRO = %Exp:cGrpProd%												//Grupo de Produto
	AND (G6L.G6L_DTINI BETWEEN %Exp:dApuradoDe% AND %Exp:dApuradoAte%				//Data Inicial de Apuração
	OR G6L.G6L_DTFIM BETWEEN %Exp:dApuradoDe% AND %Exp:dApuradoAte%)				//Data Final de Apuração	
	AND G6L.G6L_STATUS = '1'														//Apurações em Aberto
	AND G6L.%NotDel%
	%Exp:cDestino%

EndSQL
	
While (cAliasAp)->(!Eof())
	aAdd(aRet, {(cAliasAp)->G6L_FILIAL,(cAliasAp)->G6L_CODAPU})
	(cAliasAp)->(DbSkip())
EndDo

(cAliasAp)->(DbCloseArea())
RestArea(aArea)
		
Return aRet

//---------------------------------------------------------------------------------
/*/{Protheus.doc} TA050DelAnt()
Deleta as apurações anteriores em aberto, possibilitando que a apuração a ser 
gerada seja um acumulado do que há pendente.

@sample		TA050DelAnt(cCodApu)
@param		cCodApu		Código Apuração	
@return		lRet		.T. Deleção Efetuada
						.F. Deleção não Efetuada
@author 	Cristiane Nishizaka
@since 		29/07/2016
@version 	P12
/*/
//---------------------------------------------------------------------------------
Static Function TA050DelAnt(cFilApu,cCodApu)

Local aArea     := GetArea()
Local aAreaG6L  := G6L->(GetArea())
Local aTabelas  := {'G6M', 'G6O', 'G6N', 'G6P', 'G80', 'G81', 'G82', 'G48'}
Local cQuery    := ''
Local cAliasAux := GetNextAlias()
Local nX        := 0
Local lRet		:= .T.

G6L->(DbSetOrder(1)) //G6L_FILIAL+G6L_CODAPU
		
If G6L->(DbSeek(cFilApu + cCodApu))
	BEGIN TRANSACTION
		
		// deletando as linhas das tabelas filhas e desvinculando as tabelas G48 e G4C
		For nX := 1 To Len(aTabelas)
			cQuery   := ''
			aAreaAux := &(aTabelas[nX])->(GetArea())
			
			// montando a query de UPDATE 
			If aTabelas[nX] != 'G48'
				&(aTabelas[nX])->(DbSetOrder(1))
				If &(aTabelas[nX])->(DbSeek(cFilApu + cCodApu))
					cQuery := "UPDATE " + RetSQLName(aTabelas[nX]) 
					cQuery += " SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ " 
					cQuery += " WHERE " + aTabelas[nX] + "_FILIAL = '" + cFilApu + "' AND " 
					cQuery += aTabelas[nX] + "_CODAPU = '" + cCodApu + "' "
					cQuery += IIF(TCSrvType() == "AS/400", "AND @DELETED@ = ' ' ", "AND D_E_L_E_T_ = ' '")			
				EndIf
			Else
				&(aTabelas[nX])->(DbSetOrder(5))	// G48_FILAPU+G48_CODAPU+G48_FILIAL+G48_NUMID+G48_IDITEM+G48_NUMSEQ+G48_APLICA+G48_CODACD+G48_CODREC 
				If &(aTabelas[nX])->(DbSeek(cFilApu + cCodApu))
					cQuery := "UPDATE " + RetSQLName(aTabelas[nX]) 
					cQuery += " SET G48_CODAPU = '', G48_MARK = 'F' " 
					cQuery += " WHERE G48_FILAPU = '" + cFilApu + "' AND G48_CODAPU = '" + cCodApu + "' "
					cQuery += IIF(TCSrvType() == "AS/400", "AND @DELETED@ = ' ' ", "AND D_E_L_E_T_ = ' '")			

					If TCSQLExec(cQuery) < 0
						DisarmTransaction()
						lRet := .F. 
						Break
					Else
						cQuery := "UPDATE " + RetSQLName('G4C') 
						cQuery += " SET G4C_CODAPU = '' " 
						cQuery += " WHERE G4C_FILIAL = (SELECT G48_FILIAL FROM " + RetSQLName(aTabelas[nX]) + " WHERE G48_FILAPU = '" + cFilApu + "' AND G48_CODAPU = '" + cCodApu + "') AND G4C_CODAPU = '" + cCodApu + "' "
						cQuery += IIF( TCSrvType() == "AS/400", "AND @DELETED@ = ' ' ", "AND D_E_L_E_T_ = ' '")			
					EndIf
				EndIf
			EndIf
			
			// executando a query de UPDATE
			If !Empty(cQuery) 
				If TCSQLExec(cQuery) < 0
					DisarmTransaction()
					lRet := .F. 
					Break
				EndIf
			EndIf 
			
			RestArea(aAreaAux)	
		Next nX
		
		// deletando a linha da tabela pai	
		If lRet
			cQuery := "UPDATE " + RetSQLName('G6L') 
			cQuery += " SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ " 
			cQuery += " WHERE G6L_FILIAL = '" + cFilApu + "' AND G6L_CODAPU = '" + cCodApu + "' "
			cQuery += IIF( TCSrvType() == "AS/400", "AND @DELETED@ = ' ' ", "AND D_E_L_E_T_ = ' '")			
			If TCSQLExec(cQuery) < 0
				DisarmTransaction()
				lRet := .F. 
				Break
			EndIf 
		EndIf	
	END TRANSACTION		
EndIf

aTabelas := Nil

RestArea(aAreaG6L)	
RestArea(aArea)
		
Return lRet

//---------------------------------------------------------------------------------
/*/{Protheus.doc} TA50MarkAll()
Marca e desmarca todos os itens. 

@sample		TA50MarkAll(oView,lApuCli)
@param		oView		FormView	
@return		

@author 	Fernando Radu Muscalu
@since 		29/08/2016
@version 	P12
/*/
//---------------------------------------------------------------------------------
Function TA50MarkAll(oView,lApuCli)

Local oModel    := Nil
Local oModelG6O := Nil
Local oModelG82 := Nil
Local oModelG48 := Nil
Local cCampo	:= ""
Local nI		:= 0
Local nX		:= 0
Local nZ		:= 0
Local nFolder   := 0
Local lFirstG6O	:= .T.
Local aSaveRows := FwSaveRows()
Default oView 	:= FwViewActive()
Default lApuCli	:= .F.

oModel  := oView:GetModel()
nFolder := oView:GetFolderActive('F_ALL', 2)[1]
cCampo  := IIF(lApuCli, "G48_OK", "G48_MARK")

If nFolder == 1 .And. !(oModel:GetValue("G6M_DETAIL", "G6M_TIPOAC") $ 'C02|C03|C04')
	oModelG6O := oModel:GetModel("G6O_DETAIL")
	oModelG48 := oModel:GetModel("G48A_DETAIL")
	If !oModelG6O:IsEmpty()
		For nX := 1 to oModelG6O:Length()
			oModelG6O:GoLine(nX)
		
			If !oModelG6O:IsDeleted()
				If !oModelG48:IsEmpty()
					For nZ := 1 to oModelG48:Length() 
						oModelG48:GoLine(nZ)
						
						If ( lFirstG6O .and. nZ == 1 .And. oModelG48:GetValue(cCampo) )
							lMarkOn := .F.
						ElseIf ( lFirstG6O .and. nZ == 1 .And. !oModelG48:GetValue(cCampo) )	
							lMarkOn := .T.	
						Endif
						
						lFirstG6O := .F.
						
						oModelG48:SetValue(cCampo, lMarkOn)
					Next nZ
				EndIf
			Endif
		Next nX
	EndIf 
	oView:Refresh("G48A_DETAIL")
	
ElseIf nFolder == 2
	oModelG82 := oModel:GetModel("G82_DETAIL")
	oModelG48 := oModel:GetModel("G48B_DETAIL")
	If !oModelG48:IsEmpty()
		For nZ := 1 to oModelG48:Length() 
			oModelG48:GoLine(nZ)
			oModelG48:SetValue(cCampo, IIF(oModelG48:GetValue(cCampo), .F., .T.))
		Next nZ
	EndIf
	oView:Refresh("G48B_DETAIL")
EndIf

FwRestRows(aSaveRows)

Return()

//---------------------------------------------------------------------------------
/*/{Protheus.doc} Ta050VldMrk()
Efetua a validação da marcação do item

@sample		Ta050VldMrk(oSubMdl,nLine,cAction,cCampo)
@param		
	oSubMdl:	Tipo -> FormGridView; Submodelo da Grid G48A_DETAIL
	nLine:		Tipo -> Numérico; Linha atual que está em validação
	cAction:	Tipo -> Caractere; Tipo da Ação que está em execução (SETVALUE, CANSETVALUE,DELETE...)
	cCampo:		Tipo -> Caractere; Campo que está sendo validado 		
@return		

@author 	Fernando Radu Muscalu
@since 		29/08/2016
@version 	P12
/*/
//---------------------------------------------------------------------------------

Static Function Ta050VldMrk(oSubMdl,nLine,cAction,cCampo,xVlCurrent,xVlBefore)//a=oModel

Local lRet	  := .T.
Local aIFFin  := NIL
Local oView	  := IIf(!IsBlind(), FwViewActive(), Nil)
Local oMdlG6O := oSubMdl:GetModel():GetModel('G6O_DETAIL')

If !oMdlG6O:IsDeleted()
	If oSubMdl:GetModel():GetOperation() == MODEL_OPERATION_UPDATE
		If cAction == "CANSETVALUE"
			If cCampo == "G48_MARK"
				If !oSubMdl:IsDeleted() 
					If Empty(oSubMdl:GetValue("G48_NUMID"))
						lRet := .F.
					EndIf	
				Else
					lRet := .F.
				EndIf
			EndIf
			
			If lRet .And. cCampo $ 'G48_PERACD|G48_VLACD' 
				If !(aIFFin := Tur50IFFin(oSubMdl))[1]
					If cCampo == "G48_PERACD"
						lRet := .F.
					Else
						lRet := Tur50VdEdt(oSubMdl, oSubMdl:GetValue("G48_TPVLR"), cCampo)
					EndIf
				EndIf
			EndIf 
			
		ElseIf cAction == "SETVALUE"
			If cCampo != "G48_MARK"
				aIFFin := Tur50IFFin(oSubMdl)
			EndIf
				
			If cCampo $ "G48_PERACD|G48_VLACD|G48_MARK|G48_STATUS"
				If cCampo $ "G48_PERACD|G48_VLACD" .And. xVlCurrent < 0
					lRet := .F.
					oSubMdl:GetModel():SetErrorMessage(oSubMdl:GetModel():GetId(),,oSubMdl:GetModel():GetId(),,STR0106,STR0108,STR0109)//"Atencao"#"Entre somente com valores superiores a zero."#"nao pode existir valor apurado negativo"
				Else
					lRet := .T. 
					IIf(!IsBlind(), oView:Refresh("G48A_DETAIL"), Nil)
				Endif	
			Else
				lRet := .F.
			EndIf		
		EndIf
	EndIf
Else
	lRet := .F.
EndIf

If lRet .And. cCampo == "G48_VLACD" .And. cAction == "SETVALUE"
	Tur50Trig(cCampo, xVlBefore, xVlCurrent, aIFFin[2], aIFFin[3]) 
EndIf

TURXNIL(aIFFin)
DelClassIntF()

Return(lRet)


/*/{Protheus.doc} Tur50Trig
(long_description)
@type function
@author osmar.junior
@since 17/11/2016
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/

Function Tur50Trig(cCampo,xVlBefore,xVlCurrent,lRepasse,aRepasse)

Local xRetorno 	 := Nil
Local oModel 	 := FwModelActive()
Local oMdlG48	 := oModel:GetModel('G48A_DETAIL')
Local oMdlG6O    := oModel:GetModel('G6O_DETAIL')
Local lRet		 := .t.
Local nValorAnt  := Iif(cCampo == "G48_PERACD",oMdlG48:GetValue('G48_VLACD'),xVlBefore)
Local nDiferenca := 0
Local nPos		 := 0
Local lAbat		 := oMdlG48:GetValue('G48_TPACD') == '2'
Local aAux       := {}

If cCampo == "G48_VLACD"
	xRetorno := xVlCurrent
	nDiferenca	:= xRetorno - nValorAnt 
	If lAbat
		nDiferenca := nDiferenca * -1
	EndIf 
	
	If oMdlG48:GetValue('G48_TPVLR') == '1' // Porcentagem
		nValor := (xVlCurrent / oMdlG48:GetValue('G48_VLBASE')) * 100
		If !(lOk := oMdlG48:SetValue('G48_PERACD', nValor * IIF(nValor < 0, -1, 1)))
			Help( , , "TURA050", , STR0032 + oMdlG48:GetValue('G48_CODACD'), 1, 0) //"Erro ao atualizar Valor do Acordo. "
		EndIf
	EndIf
	
	oMdlG6O:SetValue('G6O_VAPLIC', oMdlG6O:GetValue('G6O_VAPLIC') + nDiferenca)
EndIf

oMdlG48:LoadValue('G48_ATUIF','1' )

If ( lRepasse .and. Len(aRepasse) > 0 )
	
	/*/////////////////////////////////////////////////////////////////////////////////////////////// 
	
	Mapa do Array aRepasse - Este array contem as informações do Item Financeiro do Acordo de Repasse
			
		aRepasse[1]	= Num. Id
		aRepasse[2] = Id. Item
		aRepasse[3] = Num. Seq.
		aRepasse[4] = Cod. Acordo
		aRepasse[5] = Aplica
		aRepasse[6] = Id. Item Fin. 
		aRepasse[7] = Cód. Cliente 
		aRepasse[8] = Loja Cliente 
		aRepasse[9] = Grupo de Produto
		aRepasse[10] = Segmento de Negócio
		aRepasse[11] = Filial
		
	///////////////////////////////////////////////////////////////////////////////////////////////*/
	
	//como houve alteração do acordo original (fornecedor), então deverá ser atualizado o acordo de
	//cliente, quando se tratar de acordo de repasse.
	G48->(DbSetOrder(1)) //G48_FILIAL, G48_NUMID, G48_IDITEM, G48_NUMSEQ, G48_APLICA, G48_CODACD
	
	cChave := Padr(aRepasse[11],TamSx3("G48_FILIAL")[1])
	cChave += Padr(aRepasse[01],TamSx3("G48_NUMID")[1])
	cChave += Padr(aRepasse[02],TamSx3("G48_IDITEM")[1])
	cChave += Padr(aRepasse[03],TamSx3("G48_NUMSEQ")[1])
	cChave += Padr(aRepasse[05],TamSx3("G48_APLICA")[1])
	cChave += Padr(aRepasse[04],TamSx3("G48_CODACD")[1])
	
	If ( G48->(DbSeek(cChave)) )

		aAdd(aAux,{"G48_FILIAL"	,aRepasse[11]})
		aAdd(aAux,{"G48_NUMID"	,aRepasse[01]})
		aAdd(aAux,{"G48_IDITEM"	,aRepasse[02]})
		aAdd(aAux,{"G48_NUMSEQ"	,aRepasse[03]})
		aAdd(aAux,{"G48_APLICA"	,aRepasse[05]})
		aAdd(aAux,{"G48_CODACD"	,aRepasse[04]})
		aAdd(aAux,{"G48_PERACD"	,G48->G48_PERACD})
		aAdd(aAux,{"G48_VLACD"	,(G48->G48_PERACD * xRetorno)/100 })
		aAdd(aAux,{"G48_COMSER"	,G48->G48_COMSER})
	
	EndIf
	
EndIf

AADD(aG48Dif,{	oMdlG48:GetValue('G48_NUMID'),;
				oMdlG48:GetValue('G48_IDITEM'),;
				oMdlG48:GetValue('G48_NUMSEQ'),;
				nValorAnt,;
				nDiferenca,;
				oMdlG48:GetValue('G48_CLASS'),;
				lRepasse,;
				aClone(aAux)})

Return(lRet)


/*/{Protheus.doc} Tur50IFFin
(long_description)
@type function
@author osmar.junior
@since 17/11/2016
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/

Function Tur50IFFin(oModel) 

Local lRet      := .F.
Local cFilG48   := "% G48_FILIAL LIKE '" + RTRIM(oModel:GetValue('G48_FILIAL')) + " %' %" 
Local cNumId    := oModel:GetValue('G48_NUMID')
Local cIdItem   := oModel:GetValue('G48_IDITEM')
Local cNumSeq   := oModel:GetValue('G48_NUMSEQ')
Local cNumACO   := oModel:GetValue('G48_CODACD')
Local cAliasG48 := GetNextAlias()
Local cAliasG4C := GetNextAlias()
Local cCodAcdR 	:= ""
Local lPossuiR  := .F.
Local aAcdRepas	:= {}

BeginSQL Alias cAliasG48
	SELECT G48_NUMID, G48_IDITEM, G48_NUMSEQ, G48_CODACD
 	FROM %Table:G48% G48
 	WHERE %Exp:cFilG48% AND 
 		  G48_NUMID = %Exp:cNumId% AND 
 		  G48_IDITEM = %Exp:cIdItem% AND 
 		  G48_NUMSEQ = %Exp:cNumSeq% AND 
 		  G48_ACDBAS = %Exp:cNumACO% AND
 		  G48_CLIFOR =  '1' AND
 		  G48.%NotDel% 	
EndSQL

While (cAliasG48)->(!EOF())
	cCodAcdR := (cAliasG48)->G48_CODACD
	lPossuiR := .T.	
	(cAliasG48)->(DbSkip())
EndDo

(cAliasG48)->(DbCloseArea())

If lPossuiR
	
	cFilG48 := "% G4C_FILIAL LIKE '" + RTRIM(oModel:GetValue('G48_FILIAL')) + " %' %" 
	BeginSQL Alias cAliasG4C
		SELECT DISTINCT G4C_FILIAL, G4C_IDIF, G4C_CODIGO, G4C_LOJA, G4C_GRPPRD, G4C_SEGNEG, G4C_APLICA, G4C_STATUS
	 	FROM %Table:G4C% G4C
	 	WHERE %Exp:cFilG48% AND 
	 		  G4C_NUMID = %Exp:cNumId% AND 
	 		  G4C_IDITEM = %Exp:cIdItem% AND 
	 		  G4C_NUMSEQ = %Exp:cNumSeq% AND 
	 		  G4C_NUMACD = %Exp:cCodAcdR% AND
	 		  G4C_CLIFOR =  '1' AND
	 		  G4C.%NotDel% 	
	EndSQL
	
	If ( (cAliasG4C)->(!EOF()) )
		
		If (cAliasG4C)->G4C_STATUS == "4"
			lRet := .T.	
		Else
			aAdd(aAcdRepas, cNumId)
			aAdd(aAcdRepas, cIdItem)
			aAdd(aAcdRepas, cNumSeq)
			aAdd(aAcdRepas, cCodAcdR)
			aAdd(aAcdRepas, (cAliasG4C)->G4C_APLICA)
			aAdd(aAcdRepas, (cAliasG4C)->G4C_IDIF) 
			aAdd(aAcdRepas, (cAliasG4C)->G4C_CODIGO) 
			aAdd(aAcdRepas, (cAliasG4C)->G4C_LOJA) 
			aAdd(aAcdRepas, (cAliasG4C)->G4C_GRPPRD)
			aAdd(aAcdRepas, (cAliasG4C)->G4C_SEGNEG)
			aAdd(aAcdRepas, (cAliasG4C)->G4C_FILIAL)
		EndIf
		(cAliasG4C)->(DbSkip())
	EndIf
	(cAliasG4C)->(DbCloseArea())
EndIf

If lRet
	If !IsBlind() 
		FwAlertHelp(STR0104, '', 'Tur50IFFin')	// "O Acordo de Repasse originado por esse acordo já possui item financeiro finalizado, edição não permitida."
	Else
		oModel:SetErrorMessage(oModel:GetId(), , oModel:GetId(), , 'Tur50IFFin', STR0104)	// "O Acordo de Repasse referente a esse acordo já possui item financeiro finalizado, edição não permitida."
	Endif
EndIf


Return({lRet,lPossuiR,aAcdRepas})


/*/{Protheus.doc} Tur50AtuRV
(long_description)
@type function
@author osmar.junior
@since 21/11/2016
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Function Tur50AtuRV()

Local oModelRV	:= Nil
Local oModelG3Q := Nil
Local oModelG3R := Nil
Local lRet		:= .T.
Local nItemG3Q	:= 0
Local nIndex	:= 0
Local cCampo	:= ''
Local cMsgErro	:= ""
Local aDefACO	:= {}
Local lAbati	:= .F.

For nIndex := 1 To Len(aG48Dif)

	If aG48Dif[nIndex][5] <> 0 //Diferença
	
		dbSelectArea("G3P")
		G3P->( dbSetOrder(1) )
		G3P->( dbSeek(xFilial("G3P") + aG48Dif[nIndex][1] ) )
		
		oModelRV := FWLoadModel("TURA034")
		oModelRV:SetOperation(MODEL_OPERATION_UPDATE)
		
		If oModelRV:Activate()
			oModelG3Q := oModelRV:GetModel("G3Q_ITENS")
			oModelG3R := oModelRV:GetModel("G3R_ITENS")
		
			For nItemG3Q := 1 To oModelG3Q:Length()
		
				oModelG3Q:GoLine(nItemG3Q)
				
				If oModelG3Q:GetValue("G3Q_IDITEM") == aG48Dif[nIndex][2] .And. oModelG3Q:GetValue("G3Q_NUMSEQ") == aG48Dif[nIndex][3] 
					
					aDefACO := Tur50CpoAc(aG48Dif[nIndex][6]) 
					cCampo:= aDefACO[1]
					lAbati:= IIF(aDefACO[2]=='2',.T.,.F.)
					
					oModelG3R:SetValue(cCampo, oModelG3R:GetValue(cCampo)+IIF(lAbati,aG48Dif[nIndex][5]*-1,aG48Dif[nIndex][5]))					
					
					If (aG48Dif[nIndex,7])	//Possui Repasse
						Tur34ItFin(oModelRV, ' ', .F., '3' , .T. ) //Por causa do Repasse
					Else
						Tur34ItFin(oModelRV, ' ', .F., '2' , .T. ) //2 para para naão pegar default										
					EndIf											

				EndIf
				
			Next nItemG3Q
			
			//Confirma a alteracao
			If (lRet) 
			
				If ( oModelRV:VldData() ) 
					oModelRV:CommitData()
					lRet := .t.
				Else
					cMsgErro := STR0107 + Alltrim(aG48Dif[nIndex,1]+"/"+aG48Dif[nIndex,2]+"-"+aG48Dif[nIndex,3]) + chr(13)	//"Problema na Atualização dos valores de acordo do RV/Item-Seq: "
					cMsgErro += TurGetErrorMsg(oModelRV)
	
					FwAlertInfo(cMsgErro,STR0106) // "Atenção"
					lRet := .f.
					Exit		
				EndIf
			
			EndIf
			
			oModelRV:DeActivate()
			
		EndIf
		oModelRV:Destroy()
	EndIf

Next nIndex

aG48Dif := {} 

Return(lRet)



/*/{Protheus.doc} Tur50CpoAc
(long_description)
@type function
@author osmar.junior
@since 22/11/2016
@version 1.0
@param ${param}, ${param_type}, ${param_descr}
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/Static Function Tur50CpoAc(cClassif)
Local aRet := GetAdvFVal("G8B", { "G8B_DESTIN", "G8B_TIPO" }, xFilial('G8B') + cClassif, 1, {"",""})

Return aRet



/*/{Protheus.doc} Tur50VdEdt
(long_description)
@type function
@author osmar.junior
@since 22/11/2016
@version 1.0
@param ${param}, ${param_type}, ${param_descr}
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/Function Tur50VdEdt(oModel,cTipo,cCampo)

Local lRet := .F.
 
If ( FwIsInCallStack("TURA050") .AND. !oModel:GetValue('G48_MARK') )
	FwAlertHelp(STR0112, STR0113, 'Tur50VdEdt')	// "Não é possível alterar um acordo desmarcado da apuração." 
	lRet := .F.
Else
	If ( IsBlind() .Or. (FwAlertYesNo(STR0103, '') .AND. Tura34Inte(oModel, .T.)) )	// "Deseja alterar manualmente?" 
		lRet := .T.
	Else
		lRet := .F.
	EndIf	
EndIf 

Return lRet 

//---------------------------------------------------------------------------------
/*/{Protheus.doc} TA050DELAPU()

Função que realiza a exclusão das apurações anteriores

@sample		TA050DELAPU(aFornDeAte, dApurarDe, dApurarAte, cGrpProd)
@param		aFornDeAte - Array com os fornecedores
			dApurarDe - Data inicial do período que está sendo apurado
			dApurarAte - Data final do período que está sendo apurado
			cGrpProd - Código do grupo de produto que está sendo apurado
@return		
@author 	Thiago Tavares
@since 		23/03/2017
@version 	P12
/*/
//---------------------------------------------------------------------------------
Static Function TA050DELAPU(aFornDeAte, dApurarDe, dApurarAte, cGrpProd, cTipoDest)

Local aDelAnt := {}	
Local nX      := 1

//Seleciona as apurações em aberto dos fornecedores
aDelAnt := TA050QryAnt(aFornDeAte, dApurarDe, dApurarAte, cGrpProd, cTipoDest)

If !Empty(aDelAnt)
	For nX := 1 To Len(aDelAnt)	
		//Deleta as apurações em aberto
		Processa({|| TA050DelAnt(aDelAnt[nX,1],aDelAnt[nX,2])}, STR0011, I18N(STR0110, {aDelAnt[nX,2]}))		// "Aguarde"	"Excluindo a apuração #1..."
	Next nX
EndIf 

aDelAnt := Nil
DelClassIntF()

Return

//---------------------------------------------------------------------------------
/*/{Protheus.doc} TA050IVCanc()

Fun磯 que retorna o status do Item de Venda

@sample		TA050IVCanc(cFilG3Q, cNumId, cIdItem, cNumSeq)
@param		cFilG3Q - Caracter, filial
			cNumId  - Caracter, n?mero do RV
			cIdItem - Caracter, n?mero do IV
			cNumSeq - Caracter, sequꮣia do IV
@return		cStatus - Caracter, status do IV
@author 	Thiago Tavares
@since 		08/08/2018
@version 	P12
/*/
//---------------------------------------------------------------------------------
Static Function TA050IVCanc(cFilG3Q, cNumId, cIdItem, cNumSeq)

Local aArea     := GetArea()
Local cAliasAux := GetNextAlias()
Local cStatus   := ''

BeginSql Alias cAliasAux

	SELECT G3Q_STATUS 
	FROM 
		%Table:G3Q% G3Q
	WHERE 
		G3Q_FILIAL = %Exp:cFilG3Q% AND 
	    G3Q_NUMID  = %Exp:cNumId%  AND
	    G3Q_IDITEM = %Exp:cIdItem% AND
	    G3Q_NUMSEQ = %Exp:cNumSeq% AND
	    G3Q.%NotDel% 
		
EndSql

If (cAliasAux)->(!EOF())
	cStatus := (cAliasAux)->G3Q_STATUS
EndIf

(cAliasAux)->(DbCloseArea())
RestArea(aArea)

Return cStatus

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050TpAco
Fun磯 responsᶥl pela tela de tipos de acordos.

@type 		Function
@author 	Thiago Tavares
@since 		26/04/2019
@version 	12.1.17
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050TpAco()

Local oDlg     := Nil
Local oFWLayer := FWLayer():New()
Local oMark    := FWMarkBrowse():New()
Local oPanel   := Nil
Local oButton  := FwButtonBar():New()
Local aAcordos := {}
Local lRet     := .F.

Define MsDialog oDlg Title STR0118 From 0, 0 To 360, 560 Pixel // "Sele磯 de Tipos de Acordo" 

oFWLayer:Init(oDlg, .F., .T.)
oFWLayer:AddLine('ALLL', 100, .F.)
oFWLayer:AddCollumn('ALLC' , 100, .T., 'ALLL')
oPanel := oFWLayer:GetColPanel('ALLC' , 'ALLL')

oMark:SetAlias('G8B')
oMark:SetFieldMark('G8B_OK')
oMark:SetAllMark({|| oMark:AllMark()})
oMark:AddFilter(STR0119, "SUBSTR(G8B_CODIGO, 1, 1) == 'F' .AND. G8B_CODIGO != 'F05'", .T., .T.)		// "Filtro Padr㯢 
oMark:SetOwner(oPanel)

oMark:SetMenuDef('')
oMark:SetProfileID('1')
oMark:ForceQuitButton()
oMark:Activate()

oButton:Init(oDlg, 015, 015, CONTROL_ALIGN_BOTTOM, .T.)
oButton:AddBtnText(STR0120, STR0121, {||oDlg:End()}, , , CONTROL_ALIGN_RIGHT, .T.) // "Sair"###"Cancela Opera磯"
oButton:AddBtnText("OK", "OK", {||lRet := TA050OkAc(oMark), oDlg:End()}, , , CONTROL_ALIGN_RIGHT, .T.) 

Activate MsDialog oDlg Center

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050OkAc
Fun磯 responsᶥl sele磯 dos tipos de acordo marcados.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050OkAc(oMark, aTpAcordo)

Local cMark := oMark:cMark
Local lRet  := .F.

oMark:Mark(cMark)
G8B->(DbGoTop())

//Limpa array para carrega-lo novamente a cada filtro
If !Empty(aTA050Acordo)
	aSize(aTA050Acordo,0)
EndIf

While G8B->(!EOF())
	If oMark:IsMark(cMark)
		aAdd(aTA050Acordo, G8B->G8B_CODIGO)
		lRet := .T.
	EndIf
	G8B->(DbSkip())
EndDo

Return lRet

/*
	Mapa do Array aApur
	
	aApur[1] = Array com os dados da G6L
		aApur[1][1,1] = "G6L_TPAPUR"
		aApur[1][1,2] = "3"	(1: Clientes; 2: Meta; 3: Fornecedores)
		aApur[1][2,1] = "G6L_CLIENT"
		aApur[1][2,2] = cFornecedor
		aApur[1][3,1] = "G6L_LOJA"
		aApur[1][3,2] = cLojaFornec
		aApur[1][4,1] = "G6L_NOME"
		aApur[1][4,2] = cNome (Posicione('SA2', 1, xFilial('SA2') + cFornecedor + cCliLoja, 'A1_NOME'))
		aApur[1][5,1] = "G6L_DTGERA"
		aApur[1][5,2] = dDataBase
		aApur[1][6,1] = "G6L_DTINI"
		aApur[1][6,2] = dDtIni
		aApur[1][7,1] = "G6L_DTFIM"
		aApur[1][7,2] = dDtFim
		aApur[1][8,1] = "G6L_STATUS"
		aApur[1][8,2] = "1"	(1: Em Aberto; 2: Liberado)
		aApur[1][9,1] = "G6L_MOEDA"
		aApur[1][9,2] = cMoeda
	aApur[2] - Array Filho de aApur[1]
		aApur[2,1] - Array com os dados de G6M
			aApur[2,1][1,1] = "G6M_SEGNEG"
			aApur[2,1][1,2] = cSegmento (1: Corporativo; 2: Evento; 3: Lazer)
			aApur[2,1][1,1] = "G6M_TIPOAC"
			aApur[2,1][1,2] = cTipoAcordo (F01...F05)
			aApur[2,1][1,1] = "G6M_RATPAD"
			aApur[2,1][1,2] = "" 
		aApur[2,2] - Array filho de aApur[2,1]
			aApur[2,2][n,1] - Array com os dados de G6O
				aApur[2,2][n,1][2,1] = "G6O_CODACD"
				aApur[2,2][n,1][2,2] = conteúdo proveniente G4W_CODACO
				aApur[2,2][n,1][2,1] = "G6O_CODREV"
				aApur[2,2][n,1][2,2] = conteúdo proveniente G4W_CODREV
				aApur[2,2][n,1][3,1] = "G6O_DCACD"
				aApur[2,2][n,1][3,2] = conteúdo proveniente G4W_DESCRI
				aApur[2,2][n,1][4,1] = "G6O_TIPOVL"
				aApur[2,2][n,1][4,2] = conteúdo proveniente G4W_TPVAL
				aApur[2,2][n,1][5,1] = "G6O_BASAPU"
				aApur[2,2][n,1][5,2] = conteúdo proveniente G4W_BASCAL
			aApur[2,2][n,2] - Array filho de aApur[2,2][n,1] 	 
				aApur[2,2][n,2][1] - Array com os dados de G48
					aApur[2,2][n,2][1][1,1] = "G48_NUMID"
					aApur[2,2][n,2][1][1,2] = conteúdo proveniente G48_NUMID
					aApur[2,2][n,2][1][2,1] = "G48_IDITEM"
					aApur[2,2][n,2][1][2,2] = conteúdo proveniente G48_IDITEM
					aApur[2,2][n,2][1][3,1] = "G48_NUMSEQ"
					aApur[2,2][n,2][1][3,2] = conteúdo proveniente G48_NUMSEQ
					aApur[2,2][n,2][1][4,1] = "G48_APLICA"
					aApur[2,2][n,2][1][4,2] = conteúdo proveniente G48_APLICA
					aApur[2,2][n,2][1][5,1] = "G48_CODACD"
					aApur[2,2][n,2][1][5,2] = conteúdo proveniente G48_CODACD
					aApur[2,2][n,2][1][6,1] = "G48_CODREC"
					aApur[2,2][n,2][1][6,2] = conteúdo proveniente G48_CODREC
					aApur[2,2][n,2][1][7,1] = "G48_CLASS"
					aApur[2,2][n,2][1][7,2] = conteúdo proveniente G48_CLASS
					aApur[2,2][n,2][1][8,1] = "G48_VLBASE"
					aApur[2,2][n,2][1][8,2] = conteúdo proveniente G48_VLBASE
					aApur[2,2][n,2][1][9,1] = "G48_SEGNEG"
					aApur[2,2][n,2][1][9,2] = conteúdo proveniente G3P_SEGNEG
					aApur[2,2][n,2][1][10,1] = "G48_MARK"
					aApur[2,2][n,2][1][10,2] = .T.
					aApur[2,2][n,2][1][11,1] = "G3P_RECNO"
					aApur[2,2][n,2][1][11,2] = conteúdo proveniente G3P_RECNO
					aApur[2,2][n,2][1][12,1] = "G3R_RECNO"
					aApur[2,2][n,2][1][12,2] = conteúdo proveniente G3R_RECNO
					aApur[2,2][n,2][1][13,1] = "G48_RECNO"
					aApur[2,2][n,2][1][13,2] = conteúdo proveniente G48_RECNO
					aApur[2,2][n,2][1][14,1] = "G48_VLACD"
					aApur[2,2][n,2][1][14,2] = conteúdo proveniente G48_VLACD
					aApur[2,2][n,2][1][15,1] = "G3R_VLRSER"
					aApur[2,2][n,2][1][15,2] = conteúdo proveniente G3R_VLRSER
				aApur[2,2][n,2][2] - Array: Não identificado ainda

*/