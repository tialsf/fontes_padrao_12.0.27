#Include 'Protheus.ch'
#INCLUDE "FWMVCDEF.CH"

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Fun��o respons�vel pela cria��o do modelo de dados.
O modelo de dados aqui se baseia na estrutura
	Cabe�alho - Tabela G48 - Acordos Aplicados (Id: G48_MASTER)
		Detalhes - Tabela G4C - Itens Financeiros (Id: G4C_DETAIL; pai G48_MASTER) 
		Detalhes - Tabela G48 - Acordos Aplicados (repasse) (Id: G48_REPASSE; pai G48_MASTER)

@type 		Function
@author 	Fernando Radu Muscalu
@since 		06/12/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------

Static Function ModelDef()

Local oModel

Local oStruG48   := FWFormStruct(1, 'G48', /*bAvalCampo*/, .F./*lViewUsado*/)	 
Local oStruG4C   := FWFormStruct(1, 'G4C', /*bAvalCampo*/, .F./*lViewUsado*/)

//Defini��o do Modelo de Dados 
oModel := MPFormModel():New('TURA050A', /*bPreValidacao*/, /*bPosValidacao*/, /*bCommit*/, /*bCancel*/)

//Cria��o do Field (Cabe�alho) - Item de Acordo Aplicado
oModel:AddFields('G48_MASTER', /*cOwner*/, oStruG48 )

//Altera��o das Propriedades
oStruG48:SetProperty('G48_CODAPU', MODEL_FIELD_OBRIGAT, .F.)
oStruG48:SetProperty('G48_CODACD', MODEL_FIELD_OBRIGAT, .F.)
oStruG48:SetProperty('G48_CODREC', MODEL_FIELD_OBRIGAT, .F.)

//Cria��o do Grid - Itens de Acordo de Repasse
oModel:AddGrid('G48_REPASSE', 'G48_MASTER', oStruG48)
oModel:SetRelation('G48_REPASSE', {	{'G48_FILIAL', 'G48_FILIAL' 	},;
										{ 'G48_NUMID', 'G48_NUMID' 		},;
										{ 'G48_IDITEM', 'G48_IDITEM' 	},;
										{ 'G48_NUMSEQ', 'G48_NUMSEQ' 	},;
										{ 'G48_ACDBAS', 'G48_CODACD' 	},;
										{ "G48_CONINU", "''"},; 
										{ 'G48_CLASS', '"C07"' 	}} , G48->( IndexKey( 1 ) ) )
										
//Cria��o do Grid - Itens Financeiros
oModel:AddGrid('G4C_DETAIL', 'G48_MASTER', oStruG4C, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)
oModel:SetRelation('G4C_DETAIL', {	{'G4C_FILIAL', 'G48_FILIAL' 	},;
										{ 'G4C_NUMID', 'G48_NUMID' 		},;
										{ 'G4C_IDITEM', 'G48_IDITEM' 	},;
										{ 'G4C_NUMSEQ', 'G48_NUMSEQ' 	},;
										{ 'G4C_APLICA', 'G48_APLICA' 	},;
										{ 'G4C_NUMACD', 'G48_CODACD' 	},;
										{ 'G4C_CODAPU', 'G48_CODAPU' 	},;
										{ 'G4C_CONINU', 'G48_CONINU' 	} } , G4C->( IndexKey( 1 ) ) )

Space(TamSx3("G3Q_PROD")[1])

oModel:GetModel('G48_REPASSE'):SetOptional(.T.)
oModel:GetModel('G4C_DETAIL'):SetOptional(.T.)

Return(oModel)
