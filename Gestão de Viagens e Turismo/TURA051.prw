#INCLUDE "TURA051.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE 'FWMVCDEF.CH'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA051()

CADASTRO DE DE/PARA - SIGATUR

@sample 	TURA051()
@return                             
@author  	Marcelo Cardoso Barbosa
@since   	25/08/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Function TURA051()
Local oBrowse	:= Nil
 
//================
// Cria o Browse 
//================

FwMsgRun( , {|| TURA051A()}, , STR0032)	// "Aguarde... Gerando registros padr�es do cadastro de DE/PARA" 
FwMsgRun( , {|| TURA051B()}, , STR0032)	// "Aguarde... Gerando registros padr�es do Layout GDS" 

oBrowse := FWMBrowse():New()
oBrowse:SetAlias('G6K')
oBrowse:SetDescription(STR0001) //"Cadastro de De/Para SigaTur"
oBrowse:Activate()

Return(.T.)


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef()

CADASTRO DE TAXAS - DEFINE MODELO DE DADOS (MVC) 

@sample 	TURA051()
@return    oModel                       
@author  	Marcelo Cardoso Barbosa
@since   	25/08/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function ModelDef()

Local oModel
Local oStruG6K := FWFormStruct(1,'G6K',/*bAvalCampo*/,/*lViewUsado*/)

oModel := MPFormModel():New('TURA051',/*bPreValidacao*/,/*bPosValidacao*/,/*bCommit*/,/*bCancel*/)
oModel:AddFields('G6KMASTER',/*cOwner*/,oStruG6K,/*bPreValidacao*/,/*bPosValidacao*/,/*bCarga*/)
oModel:SetPrimaryKey({})
oModel:SetDescription(STR0001) // Cadastro de Taxas

Return(oModel)


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef()

CADASTRO DE DE/PARA SIGATUR - DEFINE A INTERFACE DO CADASTRO (MVC) 

@sample 	TURA051()
@return    oView                       
@author  	Marcelo Cardoso Barbosa
@since   	25/08/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function ViewDef()

Local oView
Local oModel   := FWLoadModel('TURA051')
Local oStruG6K := FWFormStruct(2,'G6K')

oView := FWFormView():New()
oView:SetModel(oModel)
oView:AddField('VIEW_G6K', oStruG6K,'G6KMASTER')
oView:CreateHorizontalBox('TELA',100)
oView:SetOwnerView('VIEW_G6K','TELA')

Return(oView)


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef()

CADASTRO DE TAXAS - DEFINE AROTINA (MVC) 

@sample 	TURA051()
@return    aRotina                       
@author  	Marcelo Cardoso Barbosa
@since   	25/08/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE STR0002 ACTION 'PesqBrw' 		   	OPERATION 1	ACCESS 0 // Buscar
ADD OPTION aRotina TITLE STR0003 ACTION 'VIEWDEF.TURA051'	OPERATION 2	ACCESS 0 // Visualizar
ADD OPTION aRotina TITLE STR0004 ACTION 'VIEWDEF.TURA051'	OPERATION 3	ACCESS 0 // Incluir 
ADD OPTION aRotina TITLE STR0005 ACTION 'VIEWDEF.TURA051'	OPERATION 4	ACCESS 0 // Alterar
ADD OPTION aRotina TITLE STR0006 ACTION 'VIEWDEF.TURA051'	OPERATION 5	ACCESS 0 // Excluir

Return(aRotina)
