#INCLUDE 'TOTVS.CH'
#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'TURA052.CH'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA052
Rotina principal para cadastro de linhas e trechos

@sample 	TURA052()
@author  	Veronica de Almeida
@since   	15/09/2015
@version  	12.1.7
/*/
//------------------------------------------------------------------------------------------
Function TURA052()

Local oBrw	:= FwmBrowse():New()

oBrw:SetAlias('G8M')
oBrw:SetDescription(STR0008)   // "Cadastro de Linhas e Trechos"
oBrw:Activate()

Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Rotina respons�vel pela defini��o do menu

@sample 	MenuDef()
@return	aRotina: array com op��es do menu
@author  	Veronica de Almeida
@since   	15/09/2015
@version  	12.1.7
/*/
//------------------------------------------------------------------------------------------
Static Function MenuDef()
	
Local aRotina := {}

ADD OPTION aRotina TITLE STR0003 ACTION 'PesqBrw' 		   	OPERATION 1	ACCESS 0 // "Pesquisar"
ADD OPTION aRotina TITLE STR0004 ACTION 'VIEWDEF.TURA052'	OPERATION 2	ACCESS 0 // "Visualizar"
ADD OPTION aRotina TITLE STR0005 ACTION 'VIEWDEF.TURA052'	OPERATION 3	ACCESS 0 // "Incluir" 
ADD OPTION aRotina TITLE STR0006 ACTION 'VIEWDEF.TURA052'	OPERATION 4	ACCESS 0 // "Alterar"
ADD OPTION aRotina TITLE STR0007 ACTION 'VIEWDEF.TURA052'	OPERATION 5	ACCESS 0 // "Excluir"

Return aRotina

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Rotina respons�vel pela defini��o do modelo de neg�cio

@sample 	ModelDef()
@return	oModel: objeto com defini��o do modelo
@author  	Veronica de Almeida
@since   	15/09/2015
@version  	12.1.7
/*/
//------------------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel   := Nil
Local oStruG8M := FwFormStruct(1, 'G8M', /*bAvalCampo*/, /*lViewUsado*/ )
Local oStruG8N := FwFormStruct(1, 'G8N', /*bAvalCampo*/, /*lViewUsado*/ )
Local bPosVld  := { |oModel| TA052POSVLD(oModel) }

oStruG8N:SetProperty('G8N_CODLIN', MODEL_FIELD_OBRIGAT,.F.)

oModel	:= MpFormModel():New('TURA052', /*bPreValidacao*/, bPosVld/*bPosValidacao*/, /*bCommit*/, /*bCancel*/ )

oModel:AddFields('G8MMASTER', , oStruG8M)
oModel:AddGrid('G8NDETAIL', 'G8MMASTER', oStruG8N, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )

oModel:SetRelation('G8NDETAIL', { { 'G8N_FILIAL', 'xFilial("G8N")' }, { 'G8N_CODLIN', 'G8M_CODIGO' } }, G8N->(IndexKey(1)) )
oModel:GetModel('G8NDETAIL'):SetOptional( .T. )
oModel:GetModel('G8NDETAIL'):SetUniqueLine( {'G8N_FILIAL', 'G8N_CODLIN', 'G8N_CODIGO'} )
oModel:GetModel('G8NDETAIL'):SetDescription(STR0009)   // "Trechos"

Return oModel

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Rotina respons�vel pela defini��o da interface com o usu�rio

@sample 	ViewDef()
@return	oView: objeto com defini��o da interface
@author  	Veronica de Almeida
@since   	15/09/2015
@version  	12.1.7
/*/
//------------------------------------------------------------------------------------------
Static Function ViewDef()

Local oModel   := FwLoadModel('TURA052')
Local oStruG8M := FwFormStruct(2, 'G8M')
Local oStruG8N := FwFormStruct(2, 'G8N')
Local oView    := FwFormView():New()

// Remove campo da chave para utilizar preenchimento autom�tico da tela
oStruG8N:RemoveField('G8N_CODLIN')

oView:SetModel(oModel)

oView:AddField('VIEW_G8M', oStruG8M, 'G8MMASTER')
oView:AddGrid ('VIEW_G8N', oStruG8N, 'G8NDETAIL')

oView:CreateHorizontalBox('SUPERIOR', 40 )
oView:CreateHorizontalBox('INFERIOR', 60 )

oView:SetOwnerView('VIEW_G8M', 'SUPERIOR')
oView:SetOwnerView('VIEW_G8N', 'INFERIOR')

oView:EnableTitleView('VIEW_G8N', STR0009)   // "Trechos"

oView:AddIncrementField('VIEW_G8N', 'G8N_CODIGO')
	
Return oView

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA052POSVLD
Rotina respons�vel pela valida��o dos dados no momento da confirma��o do usu�rio

@sample 	TA052POSVLD(oModel)
@param		oModel: objeto do modelo de dados
@return	lRet: indica se os dados s�o v�lidos (.T.) ou n�o (.F)
@author  	Veronica de Almeida
@since   	15/09/2015
@version  	12.1.7
/*/
//------------------------------------------------------------------------------------------
Static Function TA052POSVLD(oModel)

Local aSaveLines  := FWSaveRows()
Local aArea       := GetArea()
Local lRet        := .T.
Local nOperation  := oModel:GetOperation()
Local aDistancias := {}

// Valida��es na INCLUS�O e ALTERA��O
If oModel:GetModel('G8NDETAIL'):Length() > 0 
	If nOperation == MODEL_OPERATION_INSERT .Or. nOperation == MODEL_OPERATION_UPDATE
		If !TA052VLDDST(oModel, aDistancias)
			Help('TURA052', 1, STR0001, , I18N(STR0002, aDistancias) , 1, 0)	// "Aten��o" // "Soma das dist�ncia do Trecho: #1 deve ser igual a dist�ncia da Linha: #2 (margem de seguran�a 10 KM)."
			lRet := .F.
		EndIf
	EndIf
EndIf

RestArea(aArea)
FWRestRows(aSaveLines)
	
Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA052VLDDST
Rotina respons�vel pela valida��o da soma das dist�ncias iformadas nos trechos com o total
informado na linha.
Ser� utilizada uma margem de seguran�a de 10 KM

@sample 	TA052VLDDST(oModel)
@param		oModel: objeto do modelo de dados
@param		aDistancias: Array com distancias (trecho,linha) retornadas para exibir na
			mensagem de Help
@return	lRet: indica se os dados s�o v�lidos (.T.) ou n�o (.F)
@author  	Veronica de Almeida
@since   	15/09/2015
@version  	12.1.7
/*/
//------------------------------------------------------------------------------------------
Static Function TA052VLDDST(oModel, aDistancias)

Local lRet			:= .T.
Local aSaveLines	:= FWSaveRows()
Local aArea			:= GetArea()
Local oModelG8N   := oModel:GetModel('G8NDETAIL')
Local oModelG8M   := oModel:GetModel('G8MMASTER')
Local nLine       := 0
Local nDistLinha  := 0
Local nDistTrecho := oModelG8M:GetValue('G8M_DIST')
Local nDistMargem := SuperGetMV('MV_TCHDIST', .F., 10 )

For nLine := 1 To oModelG8N:Length()
	oModelG8N:GoLine(nLine)
	If !oModelG8N:IsDeleted()
		nDistLinha += oModelG8N:GetValue('G8N_DIST')
	Endif
Next

// Valida de a distancia da linha possui uma diferen�a MENOR/IGUAL a MARGEM DE SEGURAN�A
If Abs(nDistTrecho - nDistLinha) > nDistMargem
	lRet 		:= .F.
	aDistancias	:= { nDistTrecho, nDistLinha }
EndIf

RestArea(aArea)
FWRestRows(aSaveLines)

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} IntegDef

Fun��o para chamar o Adapter para integra��o via Mensagem �nica 

@sample 	IntegDef( cXML, nTypeTrans, cTypeMessage )
@param		cXml � O XML recebido pelo EAI Protheus
			cType � Tipo de transa��o
				'0'- para mensagem sendo recebida (DEFINE TRANS_RECEIVE)
				'1'- para mensagem sendo enviada (DEFINE TRANS_SEND) 
			cTypeMessage � Tipo da mensagem do EAI
				'20' � Business Message (DEFINE EAI_MESSAGE_BUSINESS)
				'21' � Response Message (DEFINE EAI_MESSAGE_RESPONSE)
				'22' � Receipt Message (DEFINE EAI_MESSAGE_RECEIPT)
				'23' - WhoIs Message (DEFINE EAI_MESSAGE_WHOIS)
@return  	aRet[1] � Vari�vel l�gica, indicando se o processamento foi executado com sucesso (.T.) ou n�o (.F.)
			aRet[2] � String contendo informa��es sobre o processamento
			aRet[3] � String com o nome da mensagem �nica deste cadastro                        
@author  	Jacomo Lisa
@since   	28/09/2015
@version  	P12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function IntegDef( cXML, nTypeTrans, cTypeMessage )

Local aRet := {}

aRet:= TURI052( cXml, nTypeTrans, cTypeMessage )

Return aRet