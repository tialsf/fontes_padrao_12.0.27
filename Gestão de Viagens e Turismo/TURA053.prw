#INCLUDE 'TOTVS.CH'
#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'TURA053.CH'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA053()

CADASTRO DE SISTEMAS DE BROKER

@sample 	TURA053()
@return                             
@author  	Marcelo Cardoso Barbosa
@since   	09/09/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Function TURA053()
Local oBrowse	:= Nil
 
//================
// Cria o Browse 
//================
oBrowse := FWMBrowse():New()
oBrowse:SetAlias('G8O')
oBrowse:SetDescription(STR0001) //"Cadastro de Sistemas de Broker"
oBrowse:Activate()

Return(.T.)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef()

CADASTRO DE SISTEMAS DE BROKER - DEFINE MODELO DE DADOS (MVC) 

@sample 	TURA053()
@return    oModel                       
@author  	Marcelo Cardoso Barbosa
@since   	09/09/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel
Local oStruG8O := FWFormStruct(1,'G8O',/*bAvalCampo*/,/*lViewUsado*/)

oModel := MPFormModel():New('TURA053',/*bPreValidacao*/,/*bPosValidacao*/,/*bCommit*/,/*bCancel*/)
oModel:AddFields('G8OMASTER',/*cOwner*/,oStruG8O,/*bPreValidacao*/,/*bPosValidacao*/,/*bCarga*/)
oModel:SetPrimaryKey({})
oModel:SetDescription(STR0001) // "Cadastro de Sistemas de Broker"

Return(oModel)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef()

CADASTRO DE SISTEMAS DE BROKER - DEFINE A INTERFACE DO CADASTRO (MVC) 

@sample 	TURA051()
@return    oView                       
@author  	Marcelo Cardoso Barbosa
@since   	09/09/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Static Function ViewDef()

Local oView
Local oModel   := FWLoadModel('TURA053')
Local oStruG8O := FWFormStruct(2,'G8O')

oView := FWFormView():New()
oView:SetModel(oModel)
oView:AddField('VIEW_G8O', oStruG8O,'G8OMASTER')
oView:CreateHorizontalBox('TELA',100)
oView:SetOwnerView('VIEW_G8O','TELA')

Return(oView)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef()

CADASTRO DE SISTEMAS DE BROKER - DEFINE AROTINA (MVC) 

@sample 	TURA053()
@return    aRotina                       
@author  	Marcelo Cardoso Barbosa
@since   	09/09/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE STR0002 ACTION 'PesqBrw' 		   	OPERATION 1	ACCESS 0 // "Buscar"
ADD OPTION aRotina TITLE STR0003 ACTION 'VIEWDEF.TURA053'	OPERATION 2	ACCESS 0 // "Visualizar"
ADD OPTION aRotina TITLE STR0004 ACTION 'VIEWDEF.TURA053'	OPERATION 3	ACCESS 0 // "Incluir" 
ADD OPTION aRotina TITLE STR0005 ACTION 'VIEWDEF.TURA053'	OPERATION 4	ACCESS 0 // "Alterar"
ADD OPTION aRotina TITLE STR0006 ACTION 'VIEWDEF.TURA053'	OPERATION 5	ACCESS 0 // "Excluir"

Return(aRotina)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} IntegDef

Fun��o para chamar o Adapter para integra��o via Mensagem �nica 

@sample 	IntegDef( cXML, nTypeTrans, cTypeMessage )
@param		cXml � O XML recebido pelo EAI Protheus
			cType � Tipo de transa��o
				'0'- para mensagem sendo recebida (DEFINE TRANS_RECEIVE)
				'1'- para mensagem sendo enviada (DEFINE TRANS_SEND) 
			cTypeMessage � Tipo da mensagem do EAI
				'20' � Business Message (DEFINE EAI_MESSAGE_BUSINESS)
				'21' � Response Message (DEFINE EAI_MESSAGE_RESPONSE)
				'22' � Receipt Message (DEFINE EAI_MESSAGE_RECEIPT)
				'23' - WhoIs Message (DEFINE EAI_MESSAGE_WHOIS)
@return  	aRet[1] � Vari�vel l�gica, indicando se o processamento foi executado com sucesso (.T.) ou n�o (.F.)
			aRet[2] � String contendo informa��es sobre o processamento
			aRet[3] � String com o nome da mensagem �nica deste cadastro                        
@author  	Jacomo Lisa
@since   	28/09/2015
@version  	P12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function IntegDef( cXML, nTypeTrans, cTypeMessage )

Local aRet := {}

aRet:= TURI053( cXml, nTypeTrans, cTypeMessage )

Return aRet