#INCLUDE 'TOTVS.CH'
#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'TURA055.CH'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA055

Cadastro de classifica��o de cart�es

@sample 	TURA055()
@author  	Veronica de Almeida
@since   	10/09/2015
@version  	12.1.7
/*/
//------------------------------------------------------------------------------------------
Function TURA055()

Local oBrowse	:= FWmBrowse():New()

oBrowse:SetAlias('G8Q')
oBrowse:AddLegend("G8Q_MSBLQL == '2'", "GREEN", STR0007) // "Ativo"
oBrowse:AddLegend("G8Q_MSBLQL == '1'", "GRAY" , STR0008) // "Bloqueado"
oBrowse:SetDescription(STR0009)   // "Cadastro de Classifica��o de Cart�es"
oBrowse:Activate()

Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Cadastro do modelo com regras de neg�cio

@sample 	ModelDef()
@return		oModel: objeto do modelo
@author  	Veronica de Almeida
@since   	10/09/2015
@version  	12.1.7
/*/
//------------------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel    := MPFormModel():New('TURA055', /*bPreValidacao*/, /*bPosValidacao*/, /*bCommit*/, /*bCancel*/ )
Local oStruG8Q  := FWFormStruct( 1, 'G8Q', /*bAvalCampo*/, /*lViewUsado*/ )
Local oStruG8R  := FWFormStruct( 1, 'G8R', /*bAvalCampo*/, /*lViewUsado*/ )

oModel:AddFields('G8QMASTER', /*cOwner*/, oStruG8Q )
oModel:AddGrid('G8RDETAIL', 'G8QMASTER', oStruG8R, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
oModel:SetRelation('G8RDETAIL', { { 'G8R_FILIAL', 'xFilial( "G8R" )' }, { 'G8R_CODCLS', 'G8Q_CODIGO' } }, G8R->( IndexKey( 1 ) ) )

oModel:GetModel('G8RDETAIL'):SetUniqueLine( { 'G8R_CODGRP' } )
oModel:GetModel('G8RDETAIL'):SetDescription(STR0001)				//  "Classifica��o de Cart�es"

Return oModel

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Cadastro da vis�o

@sample 	ViewDef()
@return		oView: objeto da vis�o
@author  	Veronica de Almeida
@since   	10/09/2015
@version  	12.1.7
/*/
//------------------------------------------------------------------------------------------
Static Function ViewDef()

Local oModel   	:= FWLoadModel('TURA055')
Local oStruG8Q 	:= FWFormStruct(2, 'G8Q')
Local oStruG8R 	:= FWFormStruct(2, 'G8R')
Local oView		:= FWFormView():New()

oStruG8R:RemoveField('G8R_CODCLS')

oView:SetModel( oModel )

oView:AddField('VIEW_G8Q', oStruG8Q, 'G8QMASTER')
oView:AddGrid( 'VIEW_G8R', oStruG8R, 'G8RDETAIL')

oView:CreateHorizontalBox('SUPERIOR', 20)
oView:CreateHorizontalBox('INFERIOR', 80)

oView:SetOwnerView('VIEW_G8Q', 'SUPERIOR')
oView:SetOwnerView('VIEW_G8R', 'INFERIOR')

oView:EnableTitleView('VIEW_G8R', STR0001)	//  "Classifica��o de Cart�es"

Return oView

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Cadastro de menu

@sample 	MenuDef()
@return		aRotina: array com op��es do menu
@author  	Veronica de Almeida
@since   	10/09/2015
@version  	12.1.7
/*/
//------------------------------------------------------------------------------------------
Static Function MenuDef()

Local aRotina	:= {}

ADD OPTION aRotina TITLE STR0002 ACTION 'PesqBrw' 			OPERATION 1	ACCESS 0 // "Pesquisar"
ADD OPTION aRotina TITLE STR0003 ACTION 'VIEWDEF.TURA055'	OPERATION 2	ACCESS 0 // "Visualizar"
ADD OPTION aRotina TITLE STR0004 ACTION 'VIEWDEF.TURA055'	OPERATION 3	ACCESS 0 // "Incluir"
ADD OPTION aRotina TITLE STR0005 ACTION 'VIEWDEF.TURA055'	OPERATION 4	ACCESS 0 // "Alterar"
ADD OPTION aRotina TITLE STR0006 ACTION 'VIEWDEF.TURA055'	OPERATION 5	ACCESS 0 // "Excluir"

Return aRotina

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} IntegDef

Funcao para chamar o Adapter para integracao via Mensagem Unica 

@sample 	IntegDef( cXML, nTypeTrans, cTypeMessage )
@param		cXml - O XML recebido pelo EAI Protheus
			cType - Tipo de transacao
				'0'- para mensagem sendo recebida (DEFINE TRANS_RECEIVE)
				'1'- para mensagem sendo enviada (DEFINE TRANS_SEND) 
			cTypeMessage - Tipo da mensagem do EAI
				'20' - Business Message (DEFINE EAI_MESSAGE_BUSINESS)
				'21' - Response Message (DEFINE EAI_MESSAGE_RESPONSE)
				'22' - Receipt Message (DEFINE EAI_MESSAGE_RECEIPT)
				'23' - WhoIs Message (DEFINE EAI_MESSAGE_WHOIS)
@return  	aRet[1] - Variavel logica, indicando se o processamento foi executado com sucesso (.T.) ou nao (.F.)
			aRet[2] - String contendo informacoes sobre o processamento
			aRet[3] - String com o nome da mensagem Unica deste cadastro                        
@author  	Jacomo Lisa
@since   	28/09/2015
@version  	P12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function IntegDef( cXML, nTypeTrans, cTypeMessage )

Local aRet := {}

aRet:= TURI055( cXml, nTypeTrans, cTypeMessage )

Return aRet
