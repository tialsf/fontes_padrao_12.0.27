#INCLUDE "TURA056.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA056()

MONITOR DE INTEGRAÇÃO PNR

@sample 	TURA056()
@return                             
@author  	MARCELO CARDOSO BARBOSA
@since   	07/10/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Function TURA056()

Local   oBrowse	:= Nil
 
//------------------------------------------------------------------------------------------
// Cria o Browse 
//------------------------------------------------------------------------------------------

oBrowse := FWMBrowse():New()
oBrowse:SetAlias('G8V')

oBrowse:AddLegend	( "G8V_TIPO=='1'", "GREEN",  STR0005 ) // "Sucesso"
oBrowse:AddLegend	( "G8V_TIPO=='2'", "YELLOW", STR0006 ) // "Warning"
oBrowse:AddLegend	( "G8V_TIPO=='3'", "RED",    STR0007 ) // "Erro"

oBrowse:SetDescription(STR0001) // "Monitor de Integração PNR
oBrowse:Activate()

Return(.T.)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef()

MONITOR DE INTEGRAÇÃO PNR - DEFINE MODELO DE DADOS (MVC) 

@sample 	TURA056()
@return  	oModel                       
@author  	Marcelo Cardoso Barbosa
@since   	07/10/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function ModelDef()

Local oModel
Local oStruG8V := FWFormStruct(1,'G8V',/*bAvalCampo*/,/*lViewUsado*/)

oModel:= MPFormModel():New('TURA056',/*bPreValidacao*/,/*bPosValidacao*/,/*bCommit*/,/*bCancel*/)

oModel:AddFields('G8VMASTER',/*cOwner*/,oStruG8V,/*Criptog()/,/*bPosValidacao*/,/*bCarga*/)

oModel:SetPrimaryKey({})
oModel:SetDescription(STR0001) // "Monitor de Integração PNR

Return(oModel)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef()

MONITOR DE INTEGRAÇÃO PNR - DEFINE A INTERFACE DO CADASTRO (MVC) 

@sample 	TURA056()
@return   	oView                       
@author  	Marcelo Cardoso Barbosa
@since   	07/10/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function ViewDef()

Local oView
Local oModel   := FWLoadModel('TURA056')
Local oStruG8V := FWFormStruct(2,'G8V')

oView:= FWFormView():New()

oView:SetModel(oModel)

oView:AddField('VIEW_G8V', oStruG8V,'G8VMASTER')

oView:CreateHorizontalBox( 'TELA', 100 )

oView:SetOwnerView( 'VIEW_G8V', 'TELA' )

Return(oView)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef()

MONITOR DE INTEGRAÇÃO PNR - DEFINE AROTINA (MVC) 

@sample 	TURA056()
@return  	aRotina                       
@author  	Marcelo Cardoso Barbosa
@since   	07/10/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE STR0002 ACTION 'PesqBrw' 			OPERATION 1	ACCESS 0 // "Pesquisar"
ADD OPTION aRotina TITLE STR0003 ACTION 'VIEWDEF.TURA056'	OPERATION 2	ACCESS 0 // "Visualizar"
ADD OPTION aRotina TITLE STR0004 ACTION 'VIEWDEF.TURA056'	OPERATION 5	ACCESS 0 // "Excluir"

Return(aRotina)