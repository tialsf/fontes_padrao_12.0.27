#INCLUDE "TURA057.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA057()

CADASTRO DE LAYOUTS GDS - SIGATUR

@sample 	TURA057()
@return                             
@author  	MARCELO CARDOSO BARBOSA
@since   	24/09/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Function TURA057()

Local   oBrowse	:= Nil
Private cAliasRV  := "G3P/G3Q/G3R/G3S/G3T/G3U/G3V/G3W/G3X/G3Y/G3Z/G40/G41/G42/G43/G44/G45/G46/G47/G48/G49/G4A/G4B/G4C/G4D/"

FwMsgRun( , {|| TURA057A()}, , STR0007)	// "Aguarde... Gerando registros padr�es do Layout GDS" 
 
//------------------------------------------------------------------------------------------
// Cria o Browse 
//------------------------------------------------------------------------------------------

oBrowse := FWMBrowse():New()
oBrowse:SetAlias('G8U')
oBrowse:SetDescription(STR0001) // "Cadastro de Layouts GDS"
oBrowse:Activate()

Return(.T.)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef()

CADASTRO DE LAYOUTS GDS - DEFINE MODELO DE DADOS (MVC) 

@sample 	TURA057()
@return  	oModel                       
@author  	Marcelo Cardoso Barbosa
@since   	24/09/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function ModelDef()

Local oModel
Local oStruG8U := FWFormStruct(1,'G8U',/*bAvalCampo*/,/*lViewUsado*/)

oModel:= MPFormModel():New('TURA057',/*bPreValidacao*/,/*bPosValidacao*/,/*bCommit*/,/*bCancel*/)

oModel:AddFields('G8UMASTER',/*cOwner*/,oStruG8U,/*Criptog()/,/*bPosValidacao*/,/*bCarga*/)

oModel:SetPrimaryKey({})
oModel:SetDescription(STR0001) // "Cadastro de Layouts GDS"

Return(oModel)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef()

CADASTRO DE LAYOUTS GDS - DEFINE A INTERFACE DO CADASTRO (MVC) 

@sample 	TURA057()
@return   	oView                       
@author  	Marcelo Cardoso Barbosa
@since   	24/09/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function ViewDef()

Local oView
Local oModel   := FWLoadModel('TURA057')
Local oStruG8U := FWFormStruct(2,'G8U')

oView:= FWFormView():New()

oView:SetModel(oModel)

oView:AddField('VIEW_G8U', oStruG8U,'G8UMASTER')

oView:CreateHorizontalBox( 'TELA', 100 )

oView:SetOwnerView( 'VIEW_G8U', 'TELA' )

Return(oView)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef()

CADASTRO DE LAYOUTS GDS - DEFINE AROTINA (MVC) 

@sample 	TURA057()
@return  	aRotina                       
@author  	Marcelo Cardoso Barbosa
@since   	24/09/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE STR0002 ACTION 'PesqBrw' 			OPERATION 1	ACCESS 0 // "Pesquisar"
ADD OPTION aRotina TITLE STR0003 ACTION 'VIEWDEF.TURA057'	OPERATION 2	ACCESS 0 // "Visualizar"
ADD OPTION aRotina TITLE STR0004 ACTION 'VIEWDEF.TURA057'	OPERATION 3	ACCESS 0 // "Incluir"
ADD OPTION aRotina TITLE STR0005 ACTION 'VIEWDEF.TURA057'	OPERATION 4	ACCESS 0 // "Alterar"
ADD OPTION aRotina TITLE STR0006 ACTION 'VIEWDEF.TURA057'	OPERATION 5	ACCESS 0 // "Excluir"

Return(aRotina)