#INCLUDE "TURA058.ch"
#Include "Totvs.ch"
#Include "FWMVCDef.ch"

Static oModelFat	:= nil


/*/{Protheus.doc} TURA058
Rotina de antecipa��o de concilia��o
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
Function TURA058()
	Local oBrowse

	If !FWIsInCallStack( "TURA042A" ) .And. !FWIsInCallStack( "TURA038" )
		Help(,,"TURA058",,STR0001,1,0) //"Esta rotina deve ser chamda somente atrav�s da concilia��o - terrestre."
		Return
	EndIf

	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias("G8X")
	oBrowse:SetMenuDef( "TURA058" )

	oBrowse:SetDescription(STR0002) //"Fatura x Antecipa��o"

	If FWIsInCallStack( "TURA042A" )
		oBrowse:SetFilterDefault( "G8X->G8X_FATURA == G8C->G8C_FATURA" )
	ElseIf FWIsInCallStack( "TURA038" )
		oBrowse:SetFilterDefault( "G8X->G8X_FATURA == G6H->G6H_FATURA" )
	EndIf

	oBrowse:Activate()
Return


/*/{Protheus.doc} ModelDef
Modelo de dados da antecipa��o
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
Static Function ModelDef()
	Local oModel	:= Nil
	Local oStruG8X 	:= FWFormStruct( 1, "G8X", /*bAvalCampo*/, /*lViewUsado*/ )	//Fatura x Antecipa��o

	oModel := MPFormModel():New( "TURA058", /*bPreValidacao*/, /*bPosValidacao*/,{|oModel| TA058Commit(oModel) }, /*bCancel*/ )
	oModel:AddFields( "G8X_MASTER", /*cOwner*/, oStruG8X )

	oModel:SetVldActivate( {||  TA058VlAct( oModel ) }  )
	oModel:SetActivate( {||  TA058Init( oModel ) } )

Return oModel

/*/{Protheus.doc} ViewDef
ViewDef de dados da antecipa��o
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
Static Function ViewDef()
	Local oModel   	:= FWLoadModel( "TURA058" )
	Local oStruG8X 	:= FWFormStruct( 2, "G8X" )	//Fatura x Antecipa��o
	Local oView		:= FWFormView():New()

	oView:SetModel( oModel )

	oView:AddField("VIEW_G8X"	, oStruG8X		, "G8X_MASTER" )

	oView:CreateHorizontalBox( "ALL"	, 100 )

	oView:SetOwnerView( "VIEW_G8X", "ALL" 	)

Return oView

Static Function TA058VlAct( oModel )
	Local aArea		:= GetArea()
	Local cAlias		:= GetNextAlias()
	Local cFatura 	:=	""
	Local cFornece 	:= 	""
	Local cLoja		:= ""
	Local lRet 		:= .T.
	Local lTerrestre	:= !(FWIsInCallStack( "TURA038" ))

	If oModel:GetOperation() == MODEL_OPERATION_INSERT
		If lTerrestre
			cFatura 	:= G8C->G8C_FATURA
			cFornece 	:= G8C->G8C_FORNEC
			cLoja		:= G8C->G8C_LOJA

			If G8C->G8C_STATUS == "1"
				Help("TA058INS",1,"HELP","TA058VlAct",STR0008,1,0)		// "Concilia��o j� efetivada."
				lRet := .F.
			EndIf
		Else
			cFatura 	:= G6H->G6H_FATURA
			cFornece 	:= G6H->G6H_FORNEC
			cLoja		:= G6H->G6H_LOJA

			If G6H->G6H_EFETIV == "1"
				Help("TA058INS",1,"HELP","TA058VlAct",STR0008,1,0)		// "Concilia��o j� efetivada."
				lRet := .F.
			EndIf
		EndIf

		If lRet
			BeginSql Alias cAlias
				SELECT Count(*) NREGS
					FROM %Table:G8X% G8X
					WHERE G8X_FATURA 		= %Exp:cFatura%
						AND G8X_FORNEC	= %Exp:cFornece%
						AND G8X_LOJA	 	= %Exp:cLoja%
						AND %notDel%
			EndSql
	
			If (cAlias)->NREGS  > 0
				Help("TA058INS",1,"HELP","TA058VlAct",STR0009,1,0)		// "J� existe t�tulo de antecipa��o para esta fatura."
				lRet := .F.
			EndIf
			(cAlias)->( dbCloseArea() )
		EndIf
		
	ElseIf oModel:GetOperation() == MODEL_OPERATION_UPDATE

		Help("TA058ALT",1,"HELP","TA058ALT",STR0010,1,0)		// "A��o de altera��o n�o permitida, exclua a antecipa��o e insira novamente."
		lRet := .F.

	ElseIf oModel:GetOperation() == MODEL_OPERATION_DELETE

		If !lTerrestre
			If G6H->G6H_EFETIV == "1"
				Help("TA058EXC",1,"HELP","TA058VlAct",STR0008,1,0)		// "Concilia��o j� efetivada."
				lRet := .F.
			EndIf
		Else
			If G8C->G8C_STATUS == "1"
				Help("TA058EXC",1,"HELP","TA058VlAct",STR0008,1,0)		// "Concilia��o j� efetivada."
				lRet := .F.
			EndIf
		EndIf

	EndIf
	
	RestArea(aArea)

Return lRet


/*/{Protheus.doc} MenuDef
Menu Funcional da antecipa��o
@type function
@author anderson
@since 24/03/2016
@version 1.0
/*/
Static Function MenuDef()
	Local aRotina := {}

	ADD OPTION aRotina Title STR0003 	Action "VIEWDEF.TURA058" 	OPERATION 2 ACCESS 0 //"Visualizar"
	ADD OPTION aRotina Title STR0004	Action "VIEWDEF.TURA058"		OPERATION 3 ACCESS 0 //"Incluir"
	ADD OPTION aRotina Title STR0005 	Action "VIEWDEF.TURA058" 	OPERATION 4 ACCESS 0 //"Alterar"
	ADD OPTION aRotina Title STR0006 	Action "VIEWDEF.TURA058" 	OPERATION 5 ACCESS 0 //"Excluir"

Return aRotina


/*/{Protheus.doc} TA058Init
Rotina para inicializa��o dos valores do model
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
Static Function TA058Init( oModel )

	If oModel:GetOperation() == MODEL_OPERATION_INSERT

		If FwIsInCallStack( "TURA042A" )
			oModel:SetValue( "G8X_MASTER", "G8X_TIPOFA", "2"					)
			oModel:SetValue( "G8X_MASTER", "G8X_FATURA", G8C->G8C_FATURA	)
			oModel:SetValue( "G8X_MASTER", "G8X_FORNEC", G8C->G8C_FORNEC	)
			oModel:SetValue( "G8X_MASTER", "G8X_LOJA"  , G8C->G8C_LOJA		)
			oModel:SetValue( "G8X_MASTER", "G8X_NOME"  , POSICIONE("SA2",1,XFILIAL("SA2")+G8C->G8C_FORNEC+G8C->G8C_LOJA,"A2_NOME")	)
			                                   
			If G8C->G8C_VLRFAT >= 0 //A antecipa��o ser� a receber
				oModel:SetValue( "G8X_MASTER", "G8X_TIPOTI", "1" ) //RA
				oModel:SetValue( "G8X_MASTER", "G8X_VALOR" , G8C->G8C_VLRFAT	)
			Else
				oModel:SetValue( "G8X_MASTER", "G8X_TIPOTI", "2" ) //PA
				oModel:SetValue( "G8X_MASTER", "G8X_VALOR" , G8C->G8C_VLRFAT * -1	)
			EndIf

		ElseIf FwIsInCallStack( "TURA038" )
			oModel:SetValue( "G8X_MASTER", "G8X_TIPOFA", "1"				)
			oModel:SetValue( "G8X_MASTER", "G8X_FATURA", G6H->G6H_FATURA	)
			oModel:SetValue( "G8X_MASTER", "G8X_FORNEC", G6H->G6H_FORNEC	)
			oModel:SetValue( "G8X_MASTER", "G8X_LOJA"  , G6H->G6H_LOJA		)
			oModel:SetValue( "G8X_MASTER", "G8X_NOME"  , POSICIONE("SA2",1,XFILIAL("SA2")+G6H->G6H_FORNECC+G6H->G6H_LOJA,"A2_NOME")	)

			If G6H->G6H_VALOR	 >= 0 //A antecipa��o ser� a pagar
				oModel:SetValue( "G8X_MASTER", "G8X_TIPOTI", "1" ) //PA
				oModel:SetValue( "G8X_MASTER", "G8X_VALOR" , G6H->G6H_VALOR	)
			Else
				oModel:SetValue( "G8X_MASTER", "G8X_TIPOTI", "2" ) //RA
				oModel:SetValue( "G8X_MASTER", "G8X_VALOR" , G6H->G6H_VALOR  * -1	)
			EndIf

		EndIf

	EndIf

Return


/*/{Protheus.doc} TA058Commit
Rotina para grava��o do model e gera��o dos t�tulos de antecipa��o
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
Static Function TA058Commit(oModel)
	Local lRet 	:= .T.
	Local aTit		:=	{}
	Local aArray	:= {}

	Private	lMsErroAuto	:= .F.

	If oModel:GetOperation() == MODEL_OPERATION_INSERT
		If oModel:GetValue( "G8X_MASTER", "G8X_TIPOTI" ) == "1" // PA
			If TA058GerPA( oModel, aTit )
				oModel:SetValue( "G8X_MASTER", "G8X_PREFIX"	, aTit[1])
				oModel:SetValue( "G8X_MASTER", "G8X_NUM"		, aTit[2])
				oModel:SetValue( "G8X_MASTER", "G8X_TIPO"		, aTit[3])

				lRet := FwFormCommit( oModel )
			EndIf

		Else //RA
			If TA058GerRA( oModel, aTit )
				oModel:SetValue( "G8X_MASTER", "G8X_PREFIX"	, aTit[1])
				oModel:SetValue( "G8X_MASTER", "G8X_NUM"		, aTit[2])
				oModel:SetValue( "G8X_MASTER", "G8X_TIPO"		, aTit[3])

				lRet := FwFormCommit( oModel )
			EndIf

		EndIf
	ElseIf oModel:GetOperation() == MODEL_OPERATION_DELETE

		If oModel:GetValue( "G8X_MASTER", "G8X_TIPOTI" ) == "1"

			SE2->( dbSetOrder(1) )
			If SE2->( dbSeek( xFilial('SE2') + oModel:GetValue( "G8X_MASTER", "G8X_PREFIX") + oModel:GetValue( "G8X_MASTER", "G8X_NUM"	) + oModel:GetValue( "G8X_MASTER", "G8X_PARCEL") + oModel:GetValue( "G8X_MASTER", "G8X_TIPO" ) ) )

				aAdd(aArray,{ "E2_PREFIXO" 	, SE2->E2_PREFIXO	, NIL })
				aAdd(aArray,{ "E2_NUM" 		, SE2->E2_NUM		, NIL })
				aAdd(aArray,{ "E2_TIPO" 		, SE2->E2_TIPO	, NIL })
				aAdd(aArray,{ "E2_PARCELA" 	, SE2->E2_PARCELA	, NIL })
				aAdd(aArray,{ "E2_FORNECE" 	, SE2->E2_FORNECE	, NIL })
				aAdd(aArray,{ "E2_LOJA" 		, SE2->E2_LOJA	, NIL })

				MsExecAuto( { |x,y,z| FINA050(x,y,z)}, aArray,,5) //3-Inclusao,4-Altera��o,5-Exclus�o

				If lMsErroAuto
					lRet := .F.
		 			MostraErro()
				EndIf

			EndIf



		ElseIf oModel:GetValue( "G8X_MASTER", "G8X_TIPOTI" ) == "2"

			SE1->( dbSetOrder(1) )
			If SE1->( dbSeek( xFilial('SE1') + oModel:GetValue( "G8X_MASTER", "G8X_PREFIX") + oModel:GetValue( "G8X_MASTER", "G8X_NUM"	) + oModel:GetValue( "G8X_MASTER", "G8X_PARCEL") + oModel:GetValue( "G8X_MASTER", "G8X_TIPO" ) ) )

				aAdd(aArray,{ "E1_PREFIXO" 	, SE1->E1_PREFIXO		, NIL })
				aAdd(aArray,{ "E1_NUM" 		, SE1->E1_NUM			, NIL })
				aAdd(aArray,{ "E1_TIPO" 		, SE1->E1_TIPO		, NIL })
				aAdd(aArray,{ "E1_PARCELA" 	, SE1->E1_PARCELA		, NIL })
				aAdd(aArray,{ "E1_FORNECE" 	, SE1->E1_CLIENTE 	, NIL })
				aAdd(aArray,{ "E1_LOJA" 		, SE1->E1_LOJA		, NIL })

				MsExecAuto( { |x,y,z| FINA040(x,y)}, aArray, 5) //3-Inclusao,4-Altera��o,5-Exclus�o

				If lMsErroAuto
					lRet := .F.
		 			MostraErro()
				EndIf
			EndIf

		EndIf

		If lRet
			lRet := FwFormCommit( oModel )
		EndIf
	EndIf

Return lRet




/*/{Protheus.doc} TA058GerPA
Fun��o para gera��o do t�tulo de pagamento antecipado
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
Static Function TA058GerPA( oModel, aTitulo )
	Local aArray 	:= {}
	Local cNum		:= ""
	Local lRet		:= .T.

	Private lMsErroAuto := .F.

	cNum	:= GetSXENum("SE2","E2_NUM","E2_NUM_PA")

	SE2->( dbSetOrder(1) )
	While SE2->( dbSeek( xFilial("SE2") + PADR( "PA", TamSX3("E2_PREFIXO")[1]  ) + cNum ) )
		cNum	:= GetSXENum("SE2","E2_NUM","E2_NUM_PA")
	EndDo

	aAdd(aArray,{ "E2_PREFIXO" 	, "PA" 											, NIL })
	aAdd(aArray,{ "E2_NUM" 		, cNum  											, NIL })
	aAdd(aArray,{ "E2_TIPO" 		, "PA" 											, NIL })
	aAdd(aArray,{ "E2_NATUREZ" 	, oModel:GetValue("G8X_MASTER","G8X_NATUR") 	, NIL })
	aAdd(aArray,{ "E2_FORNECE" 	, oModel:GetValue("G8X_MASTER","G8X_FORNEC") 	, NIL })
	aAdd(aArray,{ "E2_LOJA" 		, oModel:GetValue("G8X_MASTER","G8X_LOJA") 	, NIL })
	aAdd(aArray,{ "E2_EMISSAO" 	, oModel:GetValue("G8X_MASTER","G8X_EMISSA")	, NIL })
	aAdd(aArray,{ "E2_VENCTO" 	, oModel:GetValue("G8X_MASTER","G8X_EMISSA")	, NIL })
	aAdd(aArray,{ "E2_VENCREA" 	, Datavalida(oModel:GetValue("G8X_MASTER","G8X_VENCTO")), NIL })
	aAdd(aArray,{ "E2_VALOR" 	, oModel:GetValue("G8X_MASTER","G8X_VALOR") 	, NIL })
	aAdd(aArray,{ "AUTBANCO" 	, oModel:GetValue("G8X_MASTER","G8X_BANCO") 	, NIL })
	aAdd(aArray,{ "AUTAGENCIA" 	, oModel:GetValue("G8X_MASTER","G8X_AGENCI") 	, NIL })
	aAdd(aArray,{ "AUTCONTA" 	, oModel:GetValue("G8X_MASTER","G8X_CONTA")  	, NIL })
	aAdd(aArray,{ "E2_ORIGEM" 	, "TURA058" 									 	, NIL })
	

	MsExecAuto( { |x,y,z| FINA050(x,y,z)}, aArray,, 3) //3-Inclusao,4-Altera��o,5-Exclus�o

	If lMsErroAuto
		lRet := .F.
	 	MostraErro()
	Else
		aAdd( aTitulo, "PA" 	) //Prefixo
		aAdd( aTitulo, cNum	) //Numero
		aAdd( aTitulo, "PA"	) //Tipo
	EndIf

Return lRet


/*/{Protheus.doc} TA058GerRA
Fun��o para gera��o do t�tulo de recebimento antecipado
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
Static Function TA058GerRA( oModel, aTitulo )
	Local aArray 	:= {}
	Local cNum		:= ""
	Local lRet		:= .T.

	Private lMsErroAuto := .F.

	SA2->( dbSetOrder(1) )

	If !SA2->( dbSeek( xFilial("SA2") + oModel:GetValue( "G8X_MASTER", "G8X_FORNEC" ) + oModel:GetValue( "G8X_MASTER", "G8X_LOJA " ) ) )
		Return .F.
	Else
		cNum	:= GetSXENum("SE1","E1_NUM","E1_NUM_RA")

		SE1->( dbSetOrder(1) )
		If SE1->( dbSeek( xFilial("SE1") + PADR( "RA", TamSX3("E1_PREFIXO")[1]  ) + cNum ) )
			cNum	:= GetSXENum("SE1","E1_NUM","E1_NUM_RA")
		EndIf

		aAdd( aArray,	{ "E1_PREFIXO"	, "RA" 	, NIL } )
		aAdd( aArray,	{ "E1_NUM" 		, cNum		, NIL } )
		aAdd( aArray,	{ "E1_TIPO" 		, "RA" 	, NIL } )
		aAdd( aArray,	{ "E1_NATUREZ"	, oModel:GetValue("G8X_MASTER","G8X_NATUR") , NIL } )

		aAdd( aArray,	{ "E1_CLIENTE" 	, SA2->A2_CLIENTE 	, NIL } )
		aAdd( aArray,	{ "E1_LOJA"		, SA2->A2_LOJCLI		, NIL } )

		aAdd( aArray,	{ "E1_EMISSAO"	, oModel:GetValue("G8X_MASTER","G8X_EMISSA")	, NIL } )
		aAdd( aArray,	{ "E1_VENCTO"		, oModel:GetValue("G8X_MASTER","G8X_VENCTO")	, NIL } )
		aAdd( aArray,	{ "E1_VENCREA"	, Datavalida(oModel:GetValue("G8X_MASTER","G8X_VENCTO")), NIL } )
		aAdd( aArray,	{ "E1_VALOR" 		, oModel:GetValue("G8X_MASTER","G8X_VALOR")				, NIL } )
		aAdd( aArray,	{ "cBancoAdt" 	, oModel:GetValue("G8X_MASTER","G8X_BANCO") 				, Nil } )
		aAdd( aArray,	{ "cAgenciaAdt"	, oModel:GetValue("G8X_MASTER","G8X_AGENCI") 				, Nil } )
		aAdd( aArray,	{ "cNumCon" 		, oModel:GetValue("G8X_MASTER","G8X_CONTA") 				, Nil } )

		MsExecAuto( { |x,y| FINA040(x,y)} , aArray, 3) // 3-Inclusao,4-Altera��o,5-Exclus�o


		If lMsErroAuto
	   		lRet := .F.
	   		MostraErro()
		Else
			aAdd( aTitulo, "RA" 	) //Prefixo
			aAdd( aTitulo, cNum	) //Numero
			aAdd( aTitulo, "RA"	) //Tipo
		EndIf
	EndIf

Return lRet