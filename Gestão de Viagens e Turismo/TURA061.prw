#Include 'Protheus.ch'
#Include 'FWMVCDef.ch'
#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"
#INCLUDE "TURA061.ch"

#DEFINE IMP_SPOOL 2
#DEFINE IMP_PDF 6
#DEFINE GRIDMAXLIN 99999

/*/{Protheus.doc} TURA061
Cadastro de Fatura de  Cart�es EBTA/CTA - ID - PCREQ-2764/SIGATUR

@author Simone Mie Sato Kakinoana
@since 19/02/2016
@version 1.0
/*/
Function TURA061()
	
	//-----------------------------------------------------------------------
	//Declaracao de variaveis
	//-----------------------------------------------------------------------
	Local aArea     := GetArea()
	Local oBrowse   := Nil
	
	TURA051B()
	
	SetKey (VK_F12,{|a,b| AcessaPerg("TURA061",.T.)})
	//-----------------------------------------------------------------------
	//Cria Objeto Browse
	//-----------------------------------------------------------------------
	oBrowse:= FWMBrowse():New()
	
	oBrowse:SetAlias('G90')
	
	oBrowse:AddLegend("G90_STATUS == '1'", 'GREEN'   ,STR0001 ) //"Faturas em aberto"
	oBrowse:AddLegend("G90_STATUS == '2'", 'RED' ,STR0002 ) 	//"Faturas efetivadas"
	oBrowse:SetCacheView(.F.)
	
	oBrowse:SetDescription(STR0003)   // "Faturas de Cart�es"
	oBrowse:Activate()
	
	Set Key VK_F12 To
	
	RestArea(aArea)
Return()

/*/{Protheus.doc} MenuDef
(Regra de Negocio)

@author Simone Mie Sato Kakinoana
@since 19/02/2016
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function MenuDef()
	Local aRotina := {}
	
	ADD OPTION aRotina Title STR0004	Action 'VIEWDEF.TURA061' OPERATION 1 ACCESS 0 //"Pesquisar"
	ADD OPTION aRotina Title STR0005	Action 'VIEWDEF.TURA061' OPERATION 2 ACCESS 0 //"Visualizar"
	ADD OPTION aRotina Title STR0006	Action 'VIEWDEF.TURA061' OPERATION 3 ACCESS 0 //"Incluir"
	ADD OPTION aRotina Title STR0007	Action 'VIEWDEF.TURA061' OPERATION 4 ACCESS 0 //"Alterar"
	ADD OPTION aRotina Title STR0008	Action 'VIEWDEF.TURA061' OPERATION 5 ACCESS 0 //"Excluir"
	ADD OPTION aRotina TITLE STR0010    Action 'TURA061D'   	 OPERATION 7 ACCESS 0 // "Conciliar"
	ADD OPTION aRotina TITLE STR0022	Action 'T061EXCCONC'  	 OPERATION 7 ACCESS 0 // "Excl.Concil."
	ADD OPTION aRotina TITLE STR0011   	Action 'TUR061EFET'	     OPERATION 8 ACCESS 0 // "Efetivar"
	ADD OPTION aRotina TITLE STR0012  	Action 'TUR061ESTORN'    OPERATION 9 ACCESS 0 // "Estornar"
	ADD OPTION aRotina TITLE STR0013  	Action 'TURR061'     	 OPERATION 0 ACCESS 0  // "Relat�rio"
	
Return aRotina

/*/{Protheus.doc} ModelDef
(Regra de Negocio)

@author Simone Mie Sato Kakinoana
@since 19/02/2016
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function ModelDef()
	
	//��������������������������������������������������������������Ŀ
	//?Declaracao de variaveis                                      ?
	//����������������������������������������������������������������
	Local oStruG90  := FWFormStruct(1, 'G90', /*lViewUsado*/)
	Local oStruG91  := FWFormStruct(1, 'G91', /*lViewUsado*/)
	Local oModel    := Nil
	
	Local bLoad     := { |oMdl| T061LOAD( oMdl ) }
	Local bValid	:= {||.T.}
	Local aRelacG90 := {}
	
	aAdd(aRelacG90,{ 'G91_FILIAL', 'xFilial("G90")'})
	aAdd(aRelacG90,{ 'G91_CODIGO', 'G90_CODIGO'    })
	aAdd(aRelacG90,{ 'G91_NUMFAT', 'G90_NUMFAT'    })
	
	//-----------------------------------------------------------------------
	//Cria o objeto do Modelo de Dados
	//-----------------------------------------------------------------------
	oModel := MPFormModel():New('TURA061', /*bPreValidacao*/, /*bPosValidacao*/, {|oModel| TA061Commit(oModel)}, /*bCancel*/ )
	
	T061Stru(@oStruG91,.T.)
	
	oStruG90:SetProperty('G90_CODIGO'	,MODEL_FIELD_WHEN		,{|| Tur61When()	})
	oStruG90:SetProperty('G90_NUMFAT'	,MODEL_FIELD_WHEN		,{|| Tur61When()	})
	oStruG90:SetProperty('G90_TIPO'		,MODEL_FIELD_WHEN		,{|| Tur61When()	})
	oStruG90:SetProperty('G90_MOEDA'	,MODEL_FIELD_WHEN		,{|| Tur61When() 	})
	
	oStruG91:SetProperty('G91_OPERAC'	,MODEL_FIELD_WHEN	,{||T61ChkImport()   	})
	oStruG91:SetProperty('G91_DOCFOR'	,MODEL_FIELD_WHEN	,{||T61ChkImport()   	})
	oStruG91:SetProperty('G91_FORNEC'	,MODEL_FIELD_WHEN	,{||T61ChkImport()   	})
	oStruG91:SetProperty('G91_VALOR'	,MODEL_FIELD_WHEN	,{||T61ChkImport()   	})
	oStruG91:SetProperty('G91_TARIFA'	,MODEL_FIELD_WHEN	,{||T61ChkImport()   	})
	oStruG91:SetProperty('G91_TAXAS'	,MODEL_FIELD_WHEN	,{||T61ChkImport()   	})
	oStruG91:SetProperty('G91_EXTRAS'	,MODEL_FIELD_WHEN	,{||T61ChkImport()   	})
	oStruG91:SetProperty('G91_FORNEC'	,MODEL_FIELD_WHEN	,{||T61ChkImport()   	})
	oStruG91:SetProperty('G91_DTEMIS'	,MODEL_FIELD_WHEN	,{||T61ChkImport()   	})
	oStruG91:SetProperty('G91_TIPO'		,MODEL_FIELD_WHEN	,{||T61ChkImport()   	})
	oStruG91:SetProperty('G91_DETAIL'	,MODEL_FIELD_WHEN	,{||T61ChkImport()   	})
	
	oModel:AddFields('G90MASTER', /*cOwner*/, oStruG90, /*Criptog()/, /*bPosValidacao*/, /*bCancel*/, bLoad)
	oModel:addGrid('G91DETAIL'   ,'G90MASTER' ,oStruG91, /*{ |oModelGrid, nLine, cAcao,cCampo| G91LINPRE(oModelGrid, nLine, cAcao, cCampo)}*/, /*bLinePost*/, {|oMdl,nLn,cAct,cCpo| TA061Vld(oMdl,nLn,cAct,cCpo)}, /*bPosVal*/, /*BLoad*/ )
	
	// Define o n�mero m�ximo de linhas que o model poder� receber, de acordo com a define GRIDMAXLIN.
	oModel:GetModel( 'G91DETAIL' ):SetMaxLine(GRIDMAXLIN)

	//-----------------------------------------------------------------------
	//Verifica se o n�mero da fatura j� existe para o cart�o.
	//-----------------------------------------------------------------------
	bValid := FWBuildFeature( STRUCT_FEATURE_VALID, "T061NumFat()" )
	oStruG90:SetProperty('G90_NUMFAT',MODEL_FIELD_VALID,bValid)
		
	oStruG91 := FWFormStruct(1, "G91")
	oModel:SetRelation('G91DETAIL', aRelacG90,  G91->(IndexKey(1)))
	
	oModel:SetPrimaryKey({'G90_FILIAL', 'G90_CODIGO', 'G90_NUMFAT'})
	
	If !isincallstack("FWMILEIMPORT")
		oModel:AddCalc( 'TOTAL', 'G90MASTER', 'G91DETAIL', 'G91_VALOR', 'TOTDEB', 'SUM', {|oModel| T061SUM(oModel,'D')}, /*bInitValue*/,STR0021 /*cTitle*/, /*bFormula*/) // "Total D�bito"
		oModel:AddCalc( 'TOTAL', 'G90MASTER', 'G91DETAIL', 'G91_VALOR', 'TOTCRD', 'SUM',{|oModel| T061SUM(oModel,'C')} , /*bInitValue*/,STR0020 /*cTitle*/, /*bFormula*/) // "Total Cr�dito"
		oModel:AddCalc( 'TOTAL', 'G90MASTER', 'G91DETAIL', 'G91_VALOR', 'VALOR', 'FORMULA', , /*bInitValue*/,STR0015 /*cTitle*/, {|oModel| T061VLRLIQ(oModel)} /*bFormula*/) // "Total L�quido"
	EndIf
	
	oModel:GetModel('G90MASTER'):SetDescription(STR0016) //"Fatura de Cart�es de Turismo"
	oModel:GetModel('G91DETAIL'):SetDescription(STR0017) //"Itens da Fatura"
	
	//oModel:GetModel('G91DETAIL'):SetOptional(.T.)
	
	oModel:SetVldActivate( {|oModel| T061VLMod(oModel) } )
	
Return(oModel)

/*/{Protheus.doc} ViewDef
Defini��o do interface

@author Simone Mie Sato Kakinoana
@since 19/02/2016
@version 1.0
/*/

Static Function ViewDef()
	
	//-----------------------------------------------------------------------
	//Declaracao de variaveis
	//-----------------------------------------------------------------------
	Local oModel    := ModelDef()
	Local oStruG90  := FWFormStruct(2, 'G90')
	Local oStruG91  := FWFormStruct(2, 'G91')
	Local oStrTotal := Nil
	Local oView     := Nil
	
	If !isincallstack("FWMILEIMPORT")
		oStrTotal	:= FWCalcStruct( oModel:GetModel('TOTAL') )
	EndIf
	
	T061Stru(@oStruG91,.F.)
	oView     := FWFormView():New()
	//-----------------------------------------------------------------------
	//Cria o objeto de View
	//-----------------------------------------------------------------------
	oView:SetModel(oModel)
	oView:AddField('VIEW_G90', oStruG90, 'G90MASTER')
	oView:AddGrid('VIEW_G91', oStruG91, 'G91DETAIL')
	If !isincallstack("FWMILEIMPORT")
		oView:AddField('TOTAL', oStrTotal,'TOTAL')
	EndIf
	
	
	oView:CreateHorizontalBox('SUPERIOR', 40)
	oView:CreateHorizontalBox('INFERIOR', 50)
	oView:CreateHorizontalBox( 'TOTAL', 10,,.F.)
	oView:CreateVerticalBox( 'BOXTOTAL', 100, 'TOTAL')
	
	oView:SetOwnerView('VIEW_G90', 'SUPERIOR')
	oView:SetOwnerView('VIEW_G91', 'INFERIOR')
	If !isincallstack("FWMILEIMPORT")
		oView:SetOwnerView('TOTAL','BOXTOTAL')
	EndIf
	
	//-----------------------------------------------------------------------
	//Titulo das Pastas
	//-----------------------------------------------------------------------
	oView:EnableTitleView('VIEW_G90' ,STR0003)    // "Faturas de cart�es"
	
	oView:AddIncrementField( 'VIEW_G91', 'G91_ITEM' )
	
	//-----------------------------------------------------------------------
	//Inclus�o ou  altera��o  - Bot�o Execu��o Script
	//-----------------------------------------------------------------------
	If (INCLUI .OR. ALTERA)
		oView:AddUserButton( STR0019, 'CLIPS', {|oView| T061Script(@oModel,@oView)} ) //"Execu��o Script"
		oView:AddUserButton( STR0024, 'CLIPS', {|oView| T061Mark()} ) //"Marca Todos"
		oView:AddUserButton( STR0025, 'CLIPS', {|oView| T061NoMark()} ) //"Desmarca Todos"
	EndIF
	
Return(oView)

/*/{Protheus.doc} T061NumFat
Verifica se o n�mero da fatura existe

@author Simone Mie Sato Kakinoana
@since 19/02/2016
@version 1.0
/*/
Function T061NumFat()
	
	Local aSaveArea			:= GetArea()
	Local lRet				:= .T.
	Local oModel			:= FWModelActive()
	Local oModelG90			:= oModel:GetModel("G90MASTER")
	Local oModelG91			:= oModel:GetModel("G91DETAIL")
	Local oView				:= FWViewActive()
	Local cCartao			:= oModelG90:GetValue("G90_CODIGO")
	Local cFatura			:= oModelG90:GetValue("G90_NUMFAT")
	
	If !Empty(cCartao) .And. !Empty(cFatura)
		G90->(dbSetOrder(1))
		If G90->(DbSeek(xFilial("G90")+cCartao+cFatura))
			lRet	:= .F.
			Help(" ",1,"TURA61NUMFAT",,STR0014,1,0)	//"N�mero de fatura j� cadastrado para esse cart�o. "
		EndIf
	EndIf
	
	RestArea(aSaveArea)
	
Return(lRet)

/*/{Protheus.doc} G91LINPRE(oModelGrid, nLine, cAcao, cCampo)
Valida��o

@author Simone Mie Sato Kakinoana
@since 20/02/2016
@version 1.0
/*/
Static Function G91LINPRE(oModelGrid, nLine, cAcao, cCampo)
	Local lRet    	:= .T.
	
Return lRet

/*/{Protheus.doc} T061VLMod
Valida se formulario pode ser excluido

@author Simone Mie Sato Kakinoana
@since 20/02/2016
@version 1.0
@param oModel, objeto, (Descri��o do par?etro)
@return ${return}, ${return_description}
/*/

Static Function T061VLMod(oModel)
	Local   nOperation  	:= oModel:GetOperation()
	Local   lRet        	:= .T.
	
	If ((nOperation == MODEL_OPERATION_DELETE ) .Or. (nOperation == MODEL_OPERATION_UPDATE )) .And. T061AChkEfet(.F.)
		lRet	:= .F.
		Help(,,"TUR61EFET",,STR0018,1,0) // "N�o � poss�vel alterar ou excluir uma fatura j� efetivada."
	Endif
	
	If lRet
		If nOperation == MODEL_OPERATION_INSERT
			If !CtbValiDt(,dDataBase,,,,{"TUR001"},)
				lRet	:= .F.
			EndIf
		ElseIf 	(nOperation == MODEL_OPERATION_DELETE ) .Or. (nOperation == MODEL_OPERATION_UPDATE )
			If !CtbValiDt(,G90->G90_DTLANC,,,,{"TUR001"},)
				lRet	:= .F.
			EndIf
		EndIf
	EndIf
	
Return(lRet)


/*/{Protheus.doc} T061Sum
Efetua a soma para totalizador do rodap�.

@author Simone Mie Sato Kakinoana
@since 20/02/2016
@version 1.0
@param oModel, objeto, (Descri��o do par?etro)
@return ${return}, ${return_description}
/*/
Static Function T061Sum(oModel,cTipo)
	
	Local lRet := .F.
	Local oModelG91	:= oModel:GetModel('G91DETAIL')
	
	If cTipo == 'D' .And.  oModelG91:GetValue("G91_TIPO") == "1"
		lRet	:= .T.
	ElseIf	cTipo == 'C' .And.  oModelG91:GetValue("G91_TIPO") == "2"
		lRet	:= .T.
	EndIf
	
Return lRet


/*/{Protheus.doc} T061VlrLiq
Formula do Valor liquido

@author Simone Mie Sato Kakinoana

@since 21/02/2016
@version 1.0
/*/
Function T061VlrLiq(oModel)
	
	Local nRet				:= 0
	Local oModelTotal		:= oModel:GetModel("TOTAL")
	
	nRet :=  oModelTotal:GetValue("TOTCRD") - oModelTotal:GetValue("TOTDEB")
	
Return nRet

/*/{Protheus.doc} T061Script(oModel,oView)
Execu��o de Script

@author Simone Mie Sato Kakinoana
@since 20/02/2016
@version 1.0
@param oModel, objeto, (Descri��o do par?etro)
@return ${return}, ${return_description}
/*/
Static Function T061Script(oModel,oView)
	
	TURA061B(@oModel,@oView)
	
Return

/*/{Protheus.doc} T061Stru
Adiciona campo de mark

@author Simone Mie Sato Kakinoana
@since 22/02/2016
@version 1.0
@param oModel, objeto, (Descri��o do par?etro)
@return ${return}, ${return_description}
/*/
Static Function T061Stru(oStruG91,lModel)
	Default lModel := .T.
	
	If lModel
		//Estrutra do modelo
		oStruG91:AddField(	"",;						// Titulo //"Numero"
		"Check",;					// Descri��o Tooltip
		"G91_OK",;					// Nome do Campo
		"L",;						// Tipo de dado do campo
		1,;							// Tamanho do campo
		0,;							// Tamanho das casas decimais
		{|| .T.},;					// Bloco de Valida��o do campo
		,;							// Bloco de Edi��o do campo
		{},; 						// Op��es do combo
		.F.,; 						// Obrigat�rio
		NIL,; 						// Bloco de Inicializa��o Padr�o
		.F.,; 						// Campo � chave
		.F.,; 						// Atualiza?
		.T.) 						// Virtual?
	
	Else
		
		If ( INCLUI .Or. ALTERA )
		
			//Estrutura da View
			oStruG91:AddField(	"G91_OK",;						// [01] C Nome do Campo
								"01",;							// [02] C Ordem
								"",; 							// [03] C Titulo do campo //
								"Check",; 						// [04] C Descri��o do campo //
								{"Check"} ,;					// [05] A Array com Help //
								"GET",; 						// [06] C Tipo do campo - GET, COMBO OU CHECK
								"@!",;							// [07] C Picture
								NIL,; 							// [08] B Bloco de Picture Var
								"",; 							// [09] C Consulta F3
								.T.,; 							// [10] L Indica se o campo � edit�vel
								NIL, ; 							// [11] C Pasta do campo
								NIL,; 							// [12] C Agrupamento do campo
								{},; 							// [13] A Lista de valores permitido do campo (Combo)
								NIL,; 							// [14] N Tamanho Maximo da maior op��o do combo
								NIL,;	 						// [15] C Inicializador de Browse
								.T.) 							// [16] L Indica se o campo � virtual
		
		EndIf
							
	Endif
	
Return


/*/{Protheus.doc} T61ChkImport()
Verifica se o registro foi importado

@author Simone Mie Sato Kakinoana

@since 23/02/2016
@version 1.0
/*/
Static Function T61ChkImport()
	
	Local lRet		:= .T.
	
	Local oModel	:= FWModelActive()
	Local oModelG91	:= oModel:GetModel('G91DETAIL')
	
	Local cImporta	:= oModelG91:GetValue("G91_IMPORT")
	
	
	If cImporta == '1'
		lRet	:= .F.
	Else
		lRet	:= .T.
	Endif
	
Return(lRet)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T061LOAD

Carrega dados descriptografados quando opera��o diferente de insert.

@sample 	T061LOAD(oMdl)
@return  	aDados: dados descriptografados
@author  	Simone Mie Sato Kakinoana
@since   	23/02/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function T061LOAD(oMdl)
	
	Local aArea	   		:= GetArea()
	Local oStructG90 	:= oMdl:GetStruct()
	Local aCampos    	:= oStructG90:GetFields()
	Local aLoad			:= {}
	Local aDados		:= {}
	Local nOperation	:= oMdl:GetOperation()
	Local nX			:= 0
	Local cChave    	:= G90->G90_CODIGO
	Local cNCard    	:= G90->G90_NCARD
	Local uValor 		:= Nil
	
	// Carrega campos G90_NCARD descriptografados quando opera��o
	// for diferente de insert
	If ( nOperation <> MODEL_OPERATION_INSERT )
		
		For nX := 1 To Len(aCampos)
			If !(aCampos[nX][MODEL_FIELD_VIRTUAL])
				uValor	:= G90->&( aCampos[nX][MODEL_FIELD_IDFIELD] )
				aAdd( aLoad, uValor )
			Else
				uValor	:= CriaVar( aCampos[nX][MODEL_FIELD_IDFIELD], .F. )
				aAdd( aLoad, uValor )
			EndIf
		Next
		
		aDados:= { aLoad, G90->( Recno() ) }
	EndIf
	
	RestArea( aArea )
	
Return aDados

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T061EXCCONC

Exclui a concilia��o.

@sample 	T061EXCCONC
@return
@author  	Simone Mie Sato Kakinoana
@since   	29/02/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T061EXCCONC()
	
	Local aSaveArea	:=GetArea()
	
	Local cAliasTMP	:= "TMP"
	Local cCartao	:= G90->G90_CODIGO
	Local cFatura	:= G90->G90_NUMFAT
	
	If !SoftLock("G90")
		RestArea(aSaveArea)
		Return
	Else
		If !CtbValiDt(,G90->G90_DTLANC,,,,{"TUR001"},)
			RestArea(aSaveArea)
			Return
		Else
			cAliasTMP		:= GetNextAlias()
			
			If T061AChkEfet(.F.)	//Se tiver sido efetivada, n�o pode excluir.
				Help(,,"TUR61EFET2",,STR0023,1,0) //"N�o � poss�vel excluir a concilia��o de uma fatura j� efetivada."
			Else
				If MsgYesNo(STR0027) //"Confirma exclus�o da concilia��o?
					BeginSql Alias cAliasTMP
						
						SELECT 'G91' TAB, R_E_C_N_O_ RECNO
						FROM %table:G91% G91
						WHERE G91.G91_FILIAL = %xFilial:G91%
						AND G91.G91_CODIGO = %Exp:cCartao%
						AND G91.G91_NUMFAT = %Exp:cFatura%
						AND (G91.G91_ASSOCI <> '' OR (G91.G91_ASSOCI = '' AND G91.G91_CLASSI <> ''))
						AND G91.%NotDel%
						UNION
						SELECT 'G4C' TAB, R_E_C_N_O_ RECNO
						FROM %table:G4C% G4C
						WHERE G4C.G4C_FILIAL = %xFilial:G4C%
						AND G4C.G4C_CARTUR = %Exp:cCartao%
						AND G4C.G4C_FATCAR = %Exp:cFatura%
						AND G4C.G4C_ASSOCI <> ''
						AND G4C.%NotDel%
					EndSql
					
					DbSelectArea(cAliasTMP)
					DbGotop()
			
					While !Eof()
						
						DbSelectArea((cAliasTMP)->TAB)
						DbGoto((cAliasTMP)->RECNO)
			
						Reclock(( cAliasTMP)->TAB,.F.)
			
						If (cAliasTMP)->TAB	== "G91"
							G91->G91_ASSOCI	:= ""
							G91->G91_CLASSI	:= ""
						Else
							G4C->G4C_ASSOCI		:= ""
							G4C->G4C_FATCAR 	:= ""
						EndIf
						
						((cAliasTMP)->TAB)->(MsUnlock())
						
						DbSelectArea(cAliasTMP)
						DbSkip()
						
					End
					
					MsgInfo(STR0045)	// " Exclus�o da concilia��o conclu�da com sucesso."
					
				EndIf
				
			EndIf
			
		EndIf
		G90->(MsUnlock())
		RestArea(aSaveArea)
		
	EndIf
	
Return


//-------------------------------------------------------------------
/*/{Protheus.doc} T061Mark
Marca todos os itens

@author Simone Mie Sato Kakinoana
@since 02/03/2016
@version 12
/*/
//-------------------------------------------------------------------

Function T061Mark()
	
	Local aSaveLines	:= FWSaveRows()
	Local oView			:= FWViewActive()
	Local oModel		:= FWModelActive()
	Local oModelG91 	:= oModel:GetModel("G91DETAIL")
	
	Local nX			:= 0
	
	For nX := 1 to oModelG91:Length()
		oModelG91:GoLine(nX)
		oModelG91:SetValue("G91_OK" , .T. )
	Next
	
	FWRestRows(aSaveLines)
	
	If oView != Nil
		oView:Refresh()
	EndIf
	
Return(.T.)


//-------------------------------------------------------------------
/*/{Protheus.doc} T061NoMark
Demsmarcatodos os itens

@author Simone Mie Sato Kakinoana
@since 02/03/2016
@version 12
/*/
//-------------------------------------------------------------------

Function T061NoMark()
	
	Local aSaveLines	:= FWSaveRows()
	Local oView			:= FWViewActive()
	Local oModel		:= FWModelActive()
	Local oModelG91 	:= oModel:GetModel("G91DETAIL")
	
	Local nX			:= 0
	
	For nX := 1 to oModelG91:Length()
		oModelG91:GoLine(nX)
		oModelG91:SetValue("G91_OK" , .F. )
	Next
	
	FWRestRows(aSaveLines)
	
	If oView != Nil
		oView:Refresh()
	EndIf
	
Return(.T.)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TUR061EFET

Efetiva��o da concilia��o

@sample 	T061EFET
@return
@author  	Simone Mie Sato Kakinoana
@since   	03/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function TUR061EFET()
	
	Local aSaveArea	:=GetArea()
Local cContest	:= ""
	
	If !SoftLock("G90")
	
		RestArea(aSaveArea)
	Return()
	
	Else
	
		If !CtbValiDt(,G90->G90_DTLANC,,,,{"TUR001"},)
	
			RestArea(aSaveArea)
		Return()
	
		Else
			
			If T061AChkEfet(.F.)
				Help(,,"TUR61EFET3",,STR0028,1,0) //"Concilia��o j� efetivada."
				RestArea(aSaveArea)

			Return()

			Else
				If MsgYesNo(STR0029) //"Confirma efetiva��o da concilia��o?
					//Verifica se a concilia�ao pode ser efetivada
				If T061VldEfet(@cContest)
						//Gera o t�tulo no financeiro
					
					If ( !Empty(cContest) .And. (cContest)->(!Eof()) )
						
						cMsgYesNo := STR0053 + chr(13) + chr(13) 			//"H� itens da fatura de tipo 'd�bito' que:"  
						cMsgYesNo += STR0054 + chr(13)						//" -> ou n�o foram associados;"
						cMsgYesNo += STR0055 + chr(13)						//" -> ou n�o foram classificados;" 
						cMsgYesNo += STR0056 + chr(13) + chr(13)			//" -> ou n�o foram contestados"
						cMsgYesNo += STR0057 + chr(13) + chr(13) + chr(10)	//"Deseja Prosseguir?"
						cMsgYesNo += STR0058								//"Importante: Ao prosseguir, todos os itens que n�o foram associados/classificados, "  
						cMsgYesNo += STR0059								//"ser�o contestados automaticamente."
						
						lRet := MsgYesNo(cMsgYesNo)
					
					Endif
					
					If ( lRet )
						FwMsgRun(, {|| T061GeraTit(cContest), TA61Contest(cContest)  },,STR0026)	//"Processando..."
					EndIf
					
					If ( Select(cContest) > 0 )
						(cContest)->(DbCloseArea())
					EndIf	
						
					EndiF
				EndIf
			EndIf
		EndIf
		
		G90->(MsUnlock())
	EndIf
	
	RestArea(aSaveArea)
	
Return
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T061VldEfet

Valida��o da efetiva��o da concilia��o

@sample 	T061VldEfet
@return
@author  	Simone Mie Sato Kakinoana
@since   	03/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T061VldEfet(cContest)
	
Local aSaveArea	:= GetArea()

Local lRet			:= .T.

Local cAliasTMP		:= ""
Local cCartao	:= G90->G90_CODIGO
Local cFatura	:= G90->G90_NUMFAT
Local cVlrDiff 		:= ""
Local cVlrToler 	:= ""

Local nTolera		:= G90->G90_TOLERA
Local nDiferenca	:= 0
	
cContest := GetNextAlias()

//Verifica se h� o que efetivar
BeginSql Alias cContest	
	
	SELECT
		DISTINCT
		G91.R_E_C_N_O_ G91_RECNO 
	FROM
		%Table:G91% G91
	INNER JOIN
		%Table:G90% G90
	ON
		G91_FILIAL = G90_FILIAL
		AND G91_CODIGO = G90_CODIGO
		AND G91_NUMFAT = G90_NUMFAT
		AND G90_STATUS = '1'
		AND G90.%NotDel%
	WHERE
		G91_FILIAL = %xFilial:G91%
		AND (G91_ASSOCI = '' OR (G91_ASSOCI <> '' AND G91_CLASSI = ''))
		AND G91.%NotDel%
		
EndSQL
	
	cAliasTMP		:= GetNextAlias()

//Verifica quais itens ser�o efetivados. Somente os itens que foram associados s�o verificados, quanto ao valor de tolerancia.
//Itens classificados n�o passam por este crivo  
BeginSql Alias cAliasTMP
	
	SELECT 
		G91_ASSOCI,
		( 
			SELECT 
				SUM(G91_VALOR)
			FROM 
				%table:G91% G91A
			WHERE 
				G91A.G91_FILIAL = %xFilial:G91%
				AND G91A.%NotDel%
				AND G91A.G91_CODIGO = G91.G91_CODIGO
				AND G91A.G91_NUMFAT = G91.G91_NUMFAT
				AND G91A.G91_ASSOCI = G91.G91_ASSOCI
				AND G91A.G91_TIPO ='1'
		) VALORFATD,
		( 
			SELECT 
				SUM(G91_VALOR)
			FROM 
				%table:G91% G91A
			WHERE 
				G91A.G91_FILIAL = %xFilial:G91%
				AND G91A.%NotDel%
				AND G91A.G91_CODIGO = G91.G91_CODIGO
				AND G91A.G91_NUMFAT = G91.G91_NUMFAT
				AND G91A.G91_ASSOCI = G91.G91_ASSOCI
				AND G91A.G91_TIPO ='2'
		) VALORFATC,
		( 
			SELECT 
				SUM(G4C_VALOR)
			FROM 
				%table:G4C% G4C
			WHERE 
				G4C.G4C_FILIAL = %xFilial:G4C%
				AND G4C.%NotDel%
				AND G4C.G4C_CARTUR = G91.G91_CODIGO
				AND G4C.G4C_FATCAR = G91.G91_NUMFAT
				AND G4C.G4C_PAGREC='1'
				AND G91.G91_ASSOCI = G4C.G4C_ASSOCI
		) VALORFIND,
		( 
			SELECT 
				SUM(G4C_VALOR)
			FROM 
				%table:G4C% G4C
			WHERE 
				G4C.G4C_FILIAL = %xFilial:G4C%
				AND G4C.%NotDel%
				AND G4C.G4C_CARTUR = G91.G91_CODIGO
				AND G4C.G4C_FATCAR = G91.G91_NUMFAT
				AND G4C.G4C_PAGREC='2'
				AND G91.G91_ASSOCI = G4C.G4C_ASSOCI
		) VALORFINC
	FROM 
		%table:G91% G91
	WHERE 
		G91.G91_FILIAL = %xFilial:G91%
		AND G91.G91_CODIGO = %Exp:cCartao%
		AND G91.G91_NUMFAT = %Exp:cFatura%
		AND G91.%NotDel%
		AND G91_ASSOCI <> '' 
		
EndSql

//Verifica se tem algum item a pagar maior que a taxa de tolerancia
nDiferenca	:= ((cAliasTMP)->VALORFATD -(cAliasTMP)->VALORFATC) - ((cAliasTMP)->VALORFIND -(cAliasTMP)->VALORFINC)

If  nDiferenca > nTolera

	lRet	:= .F.

cVlrDiff 	:= Alltrim(Transform(nDiferenca,PesqPict("SE2","E2_VALOR")))
cVlrToler 	:= Alltrim(Transform(nTolera,PesqPict("SE2","E2_VALOR")))

FwAlertHelp(STR0060, STR0061 + cVlrDiff + STR0062 + cVlrToler)//"Diverg�ncia maior que a toler�ncia."#"O Valor Divergente �: "#" e o Valor de Toler�ncia � "

EndIf
		
RestArea(aSaveArea)
(cAliasTMP)->(DbCloseArea())
	
Return(lRet)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T061GeraTit

Gera o titulo no financeiro

@sample 	T061GeraTit
@return
@author  	Simone Mie Sato Kakinoana
@since   	03/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T061GeraTit()
	
	Local aSaveArea	:= GetArea()
	
	Local aFina050      := {}
	Local cNaturez      := G90->G90_NATURE
	Local cPrefixo		:= Padr(GetNewPar('MV_PREFTC',  ''), Len(SE2->E2_PREFIXO))
	Local cNum      	:= G90->G90_NUMFAT
	Local cParcela		:= CriaVar("SE2->E2_PARCELA")
	Local cTipo			:= ""
	Local cFornece		:= G90->G90_CODFOR
	Local cLoja			:= G90->G90_LJFOR
	Local nMoeda        := Posicione("G5T",1,xFilial("G5T") + G90->G90_MOEDA,"G5T_MOEDAF") //Val(G90->G90_MOEDA)
	Local dDtVencto		:= G90->G90_DTVENC
	Local nValor		:= T061VlrFat()
	
	Local lOnLIne		:= .F.
	Local lMostrLanc	:= .F.
	Local lAglutLanc	:= .F.
	
	Private lMsErroAuto := .F.
	
	If ( nValor <> 0 )
	
		Pergunte("TURA061",.F.)
		
		lOnLine 	:= Iif(mv_par01 == 1,.T.,.F.)
		lMostrLanc	:= Iif(mv_par02 == 1,.T.,.F.)
		lAglutLanc	:= Iif(mv_par03 == 1,.T.,.F.)
		
		//Tratar o tipo
		If nValor > 0
			cTipo	:= "FTB"
		Else
			cTipo	:= "NDF"
		Endif
		
		SE2->(DbSetOrder(1))
		If SE2->(!DbSeek(xFilial("SE2")+cPrefixo+cNum+cParcela+cTipo+cFornece+cLoja))//E2_FILIAL+E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO+E2_FORNECE+E2_LOJA
			
			AAdd(aFina050, {'E2_FILIAL'     ,xFilial("SE2")               ,NIL})
			AAdd(aFina050, {'E2_NUM'        ,cNum                         ,NIL})
			AAdd(aFina050, {'E2_PARCELA'    ,cParcela                     ,NIL})
			AAdd(aFina050, {'E2_PREFIXO'    ,cPrefixo                     ,NIL})
			AAdd(aFina050, {'E2_NATUREZ'    ,cNaturez                     ,NIL})
			AAdd(aFina050, {'E2_TIPO'       ,cTipo                        ,NIL})
			AAdd(aFina050, {'E2_FORNECE'    ,cFornece                     ,NIL})
			AAdd(aFina050, {'E2_LOJA'       ,cLoja                        ,NIL})
			AAdd(aFina050, {'E2_VALOR'      ,ABS(nValor)                  ,NIL})
			AADD(aFina050, {'E2_MOEDA'      ,nMoeda                       ,NIL})
			AAdd(aFina050, {'E2_TXMOEDA'    ,RecMoeda(dDatabase, nMoeda)  ,NIL})
			AAdd(aFina050, {'E2_EMISSAO'    ,dDataBase                    ,NIL})
			AAdd(aFina050, {'E2_VENCTO'     ,dDtVencto                   ,NIL})
			AAdd(aFina050, {'E2_VENCREA'    ,DataValida(dDtVencto, .T.)   ,NIL})
			AADD(aFina050, {'E2_VENCORI'    ,DataValida(dDtVencto, .T.)   ,NIL})
			AADD(aFina050, {'E2_ORIGEM'     ,'TURA061'                    ,NIL})
			
			BEGIN TRANSACTION
				
				MSExecAuto({|x, y| FINA050(x, y)}, aFina050, 3)
				
				If !lMsErroAuto
					
					RecLock("G90",.F.)
					
					G90->G90_PREFIX := cPrefixo
					G90->G90_NUMTIT := cNum
					G90->G90_PARCELA:= cParcela
					G90->G90_TPTIT	 := cTipo
					G90->G90_STATUS := "2"
					
					G90->(MsUnLock())
					
					//Atualiza flag do G4C
					FwMsgRun(,{||T61Flag(.T.)},,STR0026)	//"Processando..."
					
					If lOnLine
						Tura061Ctb(lOnline,lMostrLanc,lAglutLanc,"TURA061",.T.)
					EndIf
					
					MsgAlert( STR0047 ) //"Efetiva��o realizada com sucesso!" 
					
				Else
					MostraErro()
				Endif
				
			END TRANSACTION
			
		Else
			Help(" ",1,"TURA61NUMTIT",,STR0035+cPrefixo+cNum+cParcela+cTipo+cFornece+cLoja+STR0036,1,0)	//	"N�mero do t�tulo com a chave: "### " j� existe."
		EndIf
	
	Else
	
		RecLock("G90",.F.)
			G90->G90_STATUS := "2"
		G90->(MsUnLock())
	
	EndIf
	
	RestArea(aSaveArea)
	
Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TUR061ESTORN

Estorno da Efetiva��o

@sample 	T061Estorn
@return
@author  	Simone Mie Sato Kakinoana
@since   	03/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function TUR061ESTORN()
	
	Local aSaveArea	:=GetArea()
	
	If !SoftLock("G90")
		RestArea(aSaveArea)
		Return
	Else
		If !CtbValiDt(,G90->G90_DTLANC,,,,{"TUR001"},)
			RestArea(aSaveArea)
			Return
		Else
			If !T061AChkEfet(.F.)
				Help(" ",1,"TURA61NUMFAT",,STR0032,1,0)	//"Concilia��o ainda n�o foi efetivada."
				Return
			Else
				If MsgYesNo(STR0033) //"Confirma estorno da efetiva��o ?"
					//Verifica se a efetiva��o pode ser estornada
					If T061VldEst()
						//Gera o t�tulo no financeiro
						FwMsgRun(,{||T061DelTit()  },,STR0026)	//"Processando..."
						MsgAlert(STR0048) //"Estorno realizado com sucesso!"
					EndIf
					
				EndIf
			EndIf
		EndIf
		G90->(MsUnlock())
	EndIf
	
	RestArea(aSaveArea)
	
Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T061VldEst

Valida��o do estorno da Efetiva��o

@sample 	T061VldEst
@return
@author  	Simone Mie Sato Kakinoana
@since   	03/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T061VldEst()
	
	Local aSaveArea	:= GetArea()
	
	Local lRet			:= .T.
	
	SE2->(DbSetOrder(1))
	If SE2->(DbSeek(xFilial("SE2")+G90->G90_PREFIX+G90->G90_NUMTIT+G90->G90_PARCEL+G90->G90_TPTIT+G90->G90_CODFOR+G90->G90_LJFOR))//E2_FILIAL+E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO+E2_FORNECE+E2_LOJA
		If SE2->E2_SALDO <> SE2->E2_VALOR
			lRet	:= .F.
			Help(" ",1,"TURA61TITMOV",,STR0034,1,0)	//"O t�tulo j� sofreu movimenta��o financeira.N�o � poss�vel efetuar o estorno da efetiva��o."
		Else
			T061DelTit()
		EndIf
	EndIf
	
	
	RestArea(aSaveArea)
	
Return(lRet)


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T061DelTit

Gera o titulo no financeiro

@sample 	T061DelTit
@return
@author  	Simone Mie Sato Kakinoana
@since   	04/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T061DelTit()
	
	Local aSaveArea	:= GetArea()
	
	Local aFina050      := {}
	Local cNaturez      := G90->G90_NATURE
	Local cPrefixo		:= G90->G90_PREFIX
	Local cNum      	:= G90->G90_NUMTIT
	Local cParcela		:= G90->G90_PARCEL
	Local cTipo			:= G90->G90_TPTIT
	Local cFornece		:= G90->G90_CODFOR
	Local cLoja			:= G90->G90_LJFOR
	
	Local lOnLine		:= .T.
	Local lMostrLanc	:= .F.
	Local lAglutLanc	:= .F.
	
	Private lMsErroAuto := .F.
	
	Pergunte("TURA061",.F.)
	
	lOnLine 	:= .T.	//Por defini��o, o estorno � sempre on-line
	lMostrLanc	:= Iif(mv_par02 == 1,.T.,.F.)
	lAglutLanc	:= Iif(mv_par03 == 1,.T.,.F.)
	
	SE2->(DbSetOrder(1))
	If SE2->(DbSeek(xFilial("SE2")+cPrefixo+cNum+cParcela+cTipo+cFornece+cLoja))//E2_FILIAL+E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO+E2_FORNECE+E2_LOJA
		
		AAdd(aFina050, {'E2_FILIAL'	,xFilial("SE2")	,NIL})
		AAdd(aFina050, {'E2_NUM'	,cNum			,NIL})
		AAdd(aFina050, {'E2_PARCELA',cParcela		,NIL})
		AAdd(aFina050, {'E2_PREFIXO',cPrefixo		,NIL})
		AAdd(aFina050, {'E2_NATUREZ',cNaturez		,NIL})
		AAdd(aFina050, {'E2_TIPO'	,cTipo			,NIL})
		AAdd(aFina050, {'E2_FORNECE',cFornece		,NIL})
		AAdd(aFina050, {'E2_LOJA'	,cLoja			,NIL})
		
		MsExecAuto( { |x,y,z| FINA050(x,y,z)}, aFina050,, 5)
		
		If !lMsErroAuto
			BEGIN TRANSACTION
				RecLock("G90",.F.)
				G90->G90_PREFIX  	:= ""
				G90->G90_NUMTIT 	:= ""
				G90->G90_PARCELA	:= ""
				G90->G90_TPTIT	 	:= ""
				G90->G90_STATUS		:= "1"
				G90->(MsUnLock())
				
				//Atualiza flag do G4C
				FwMsgRun(,{||T61Flag(.F.)},,STR0026)	//"Processando..."
				If lOnLine	//Sempre On-line
					Tura061Ctb(lOnline,lMostrLanc,lAglutLanc,"TURA061",.F.)
				EndIf
				
			END TRANSACTION
		Else
			MostraErro()
		Endif
	
	Else
		
		RecLock("G90",.F.)
			G90->G90_STATUS	:= "1"
		G90->(MsUnLock())
		
	EndIf
	
	RestArea(aSaveArea)
	
Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T061VlrFat

Calcula o valor da fatura a ser gerado no t�tulo.

@sample 	T061VlrFat
@return
@author  	Simone Mie Sato Kakinoana
@since   	04/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T061VlrFat()
	Local aArea			:= GetArea()
	Local cAliasTMP		:= ""
	Local cCartao		:= G90->G90_CODIGO
	Local cFatura		:= G90->G90_NUMFAT
	
	Local nDebOk		:= 0
	Local nCredOk		:= 0
	Local nNoDeb		:= 0
	Local nValFat		:= 0
	Local nTolera		:= 0
	Local nDiverg		:= 0
	Local nCredExtra	:= 0
	Local nCredOk		:= 0
	Local nNoCred		:= 0
	Local nVlrAdic		:= 0
	Local nDiferenca 	:= 0
	
	cAliasTMP		:= GetNextAlias()
	
	BeginSql Alias cAliasTMP
	
		SELECT 
			G91.G91_CODIGO, 
			G91.G91_NUMFAT, 
			G91.G91_ASSOCI, 
			G90.G90_TOLERA,
			(
				SELECT 
					ISNULL(SUM(G91_VALOR),0)
				FROM 
					%table:G91% G91A
				WHERE 
					G91A.G91_FILIAL = %xFilial:G91%
					AND G91A.G91_CODIGO = G91.G91_CODIGO
					AND G91A.G91_NUMFAT = G91.G91_NUMFAT
					AND G91A.G91_ASSOCI = G91.G91_ASSOCI
					AND G91A.G91_CLASSI = ''
					AND G91A.G91_TIPO = '1'
					AND G91A.%NotDel% 
			)G91VLRD,
			(
				SELECT 
					ISNULL(SUM(G4C_VALOR),0)
				FROM 
					%table:G4C% G4CA
				WHERE 
					G4CA.G4C_FILIAL = %xFilial:G4C%
					AND G4CA.G4C_CARTUR = G91.G91_CODIGO
					AND G4CA.G4C_FATCAR = G91.G91_NUMFAT
					AND G4CA.G4C_ASSOCI = G91.G91_ASSOCI
					AND G4CA.G4C_PAGREC = '1'
					AND G4CA.%NotDel% 
			)G4CVLRD,
			(
				SELECT 
					ISNULL(SUM(G91_VALOR),0)
				FROM 
					%table:G91% G91A
				WHERE 
					G91A.G91_FILIAL = %xFilial:G91%
					AND G91A.G91_CODIGO = G91.G91_CODIGO
					AND G91A.G91_NUMFAT = G91.G91_NUMFAT
					AND G91A.G91_ASSOCI = G91.G91_ASSOCI
					AND G91A.G91_CLASSI = ''
					AND G91A.G91_TIPO = '2'
					AND G91A.%NotDel% 
			)G91VLRC,
			(
				SELECT 
					ISNULL(SUM(G4C_VALOR),0)
				FROM 
					%table:G4C% G4CA
				WHERE 
					G4CA.G4C_FILIAL = %xFilial:G4C%
					AND G4CA.G4C_CARTUR = G91.G91_CODIGO
					AND G4CA.G4C_FATCAR = G91.G91_NUMFAT
					AND G4CA.G4C_ASSOCI = G91.G91_ASSOCI
					AND G4CA.G4C_PAGREC = '2'
					AND G4CA.%NotDel% 
			)G4CVLRC
		FROM  
			%table:G91% G91
		JOIN 
			%table:G90% G90
		ON 
			G91.G91_CODIGO = G90.G90_CODIGO
			AND G91.G91_NUMFAT = G90.G90_NUMFAT
			AND G91.D_E_L_E_T_ = G90.D_E_L_E_T_
		WHERE 
			G91.G91_FILIAL = %xFilial:G91%
			AND G91.D_E_L_E_T_ = ' '
			AND G91.G91_CODIGO = %Exp:cCartao%
			AND G91.G91_NUMFAT = %Exp:cFatura%
		GROUP BY 
			G91.G91_CODIGO, 
			G91.G91_NUMFAT, 
			G91.G91_ASSOCI, 
			G90.G90_TOLERA

	EndSql

	While (cAliasTMP)->(!Eof())		
		
		nValDebG91		:= (cAliasTMP)->G91VLRD
		nValCrdG91		:= (cAliasTMP)->G91VLRC
		nValDebG4C		:= (cAliasTMP)->G4CVLRD
		nValCrdG4C		:= (cAliasTMP)->G4CVLRC
		
		If !Empty((cAliastMP)->G91_ASSOCI)
			
			If ( nValDebG91 == nValDebG4C ) .And. ( nValCrdG91 == nValCrdG4C )
				If nValDebG91 > 0
					nDebOk	+=   nValDebG91
				EndIf
				If nValCrdG91 > 0
					nCredOk	+=   nValCrdG91
				EndIf
				
			ElseIf nValDebG91 <> nValDebG4C .And. nValCrdG91 == nValCrdG4C	//Diferen�a s� a debito
				nCredOk += nValCrdG91
				nDebOk	+= nValDebG4C
				If ( nValDebG91 - nValDebG4C) > 0					
					If nValDebG91 - nValDebG4C <= (cAliasTMP)->G90_TOLERA //Calcula valor de Tolerancia/ Divergencia
						nTolera := nValDebG91 - nValDebG4C
					Else
						nDiverg := nValDebG91 - nValDebG4C
					EndIf
				ElseIf ( nValDebG91 - nValDebG4C) < 0
					nCredExtra +=  nValCrdG4C - nValCrdG91
				EndIf
			ElseIf nValCrdG91 <> nValCrdG4C .And. nValDebG91 == nValDebG4C  	//Diferen�a s� a credito
				nDebOk	+= nValDebG4C
				nCredOk += nValCrdG4C
				If ( nValCrdG4C - nValCrdG91) > 0					
					If nValCrdG4C - nValCrdG91  <=  (cAliasTMP)->G90_TOLERA //// Se a diferen�a for menor que a toler�ncia		 
					 	nTolera	 +=  nValCrdG4C - nValCrdG91		 	 				 	
					Else  // Se a diferen�a for maior que a toler�ncia
		 				nDiverg	 += nValCrdG4C - nValCrdG91
		 			EndIf 
				Else
					nCredExtra += nValCrdG91 - nValCrdG4C					
				EndIf
			ElseIf  nValCrdG91 <> nValCrdG4C .And. nValDebG91 <> nValDebG4C  	//Se houver diferen�a no debito e no credito
				nDiferenca	:= (( nValDebG91 - nValCrdG91) - (nValDebG4C - nValCrdG4C))
				
				nDebOk += nValDebG4C
				nCredOk += nValCrdG4C
				
				If nDiferenca <> 0	 
					If nDiferenca < 0 	//Associa��o com credito
						nCredExtra	+= ABS(nDiferenca)					
					ElseIf nDiferenca <= (cAliasTMP)->G90_TOLERA	//Menor que a tolerancia
						nTolera	 += nDiferenca
					Else							
						nDiverg	 += nDiferenca					
					EndIf	
		 		EndIf
			EndIf
		EndIf
		
		(cAliasTMP)->(DbSkip())
	End		
	
	(cAliasTMP)->(DbCloseArea())
	// Busca os itens de fatura que n�o est�o associados para obter os valores 
	// de cr�ditos adicionais e itens classificados
	cAliasTMP		:= GetNextAlias()
	
	BeginSql Alias cAliasTMP
		SELECT	G91.G91_CODIGO
				,G91.G91_NUMFAT
				,G91.G91_ASSOCI
				,G91.G91_CLASSI
				,G91.G91_VALOR
				,G91.G91_TIPO 
		FROM  %table:G91% G91
		WHERE G91.G91_FILIAL = %xFilial:G91%
		AND G91.D_E_L_E_T_ = ' '
		AND G91.G91_CODIGO = %Exp:cCartao%
		AND G91.G91_NUMFAT = %Exp:cFatura%
		AND G91.G91_ASSOCI = ''		
		AND G91.G91_CLASSI <> '99'	//Constedado n�o entra no c�lculo		
	EndSql	
	
	While (cAliasTMP)->(!Eof())			
		If Empty((cAliasTMP)->G91_CLASSI) .And. (cAliasTMP)->G91_TIPO == "2"
			nNoCred += (cAliasTMP)->G91_VALOR
		ElseIf !Empty((cAliasTMP)->G91_CLASSI)
			If (cAliasTMP)->G91_TIPO == "2"
				nVlrAdic -= (cAliasTMP)->G91_VALOR
			Else
				nVlrAdic += (cAliasTMP)->G91_VALOR
			EndIf
		EndIf
		(cAliasTMP)->(DbSkip())
	End
	(cAliasTMP)->(DbCloseArea())	
	
	nValFat := (((nDebOk + nTolera) - (nCredOk + nCredExtra + nNoCred)) + nVlrAdic)

	RestArea(aArea)
	
Return(nValFat)


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T61Flag(lEfet)


@sample 	T61Flag()
@return
@author  	Simone Mie Sato Kakinoana
@since   	05/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T61Flag(lEfetiva)
		
Local aSaveArea		:= GetArea()
Local aAreaG3R		:= G3R->(GetArea())
Local cAliasTMP1	:= GetNextAlias()
Local cAliasTMP2	:= GetNextAlias()
Local cQuery		:= ""
Local cCartao		:= G90->G90_CODIGO
Local cFatura		:= G90->G90_NUMFAT	

cQuery := "UPDATE "+ RetSQLName( "G4C" ) + " "
If lEfetiva //Se for efetiva��o
	cQuery += "SET G4C_STATUS = '4' "
Else		//Se for estorno da efetiva��o
	cQuery += "SET G4C_STATUS = '1' "
EndIf
cQuery += "WHERE "
cQuery += " G4C_FILIAL ='"+xFilial("G4C")+"' "
cQuery += " AND G4C_CARTUR ='"+cCartao+"'"
cQuery += " AND G4C_FATCAR ='"+cFatura+"'"
cQuery += " AND G4C_ASSOCI <> '' "
cQuery += " AND D_E_L_E_T_ = ' ' "	
TCSQLExec( cQuery )

BeginSql Alias cAliasTMP1		
	SELECT DISTINCT G4C_NUMID, G4C_IDITEM, G4C_NUMSEQ
	FROM %table:G4C% G4C
	WHERE G4C.G4C_FILIAL = %xFilial:G4C%
	AND G4C.G4C_CARTUR = %Exp:cCartao%
	AND G4C.G4C_FATCAR = %Exp:cFatura%
	AND G4C.G4C_ASSOCI <> ''
	AND G4C.%NotDel%		
EndSql
	
If lEfetiva
	DbSelectArea("G3R")
	G3R->(dbSetOrder(1)) //G3R_FILIAL+G3R_NUMID+G3R_IDITEM+G3R_NUMSEQ
	While (cAliasTMP1)->(!Eof())		
		BeginSql Alias cAliasTMP2				
			SELECT COUNT(*) REGS
			FROM %table:G4C% G4C
			WHERE G4C.G4C_FILIAL = %xFilial:G4C%
			AND G4C.G4C_NUMID = %Exp:(cAliasTmp1)->G4C_NUMID%
			AND G4C.G4C_IDITEM = %Exp:(cAliasTmp1)->G4C_IDITEM%
			AND G4C.G4C_NUMSEQ = %Exp:(cAliasTmp1)->G4C_NUMSEQ%
			AND G4C.G4C_STATUS <> '4'
			AND G4C.%NotDel%				
		EndSql
		If (cAliasTMP2)->REGS == 0 // Todos est�o encerrados, atualizar o flago do G3R
			If G3R->(DbSeek(xFilial("G3R") + (cAliasTmp1)->G4C_NUMID + (cAliasTmp1)->G4C_IDITEM + (cAliasTmp1)->G4C_NUMSEQ))
				Reclock("G3R",.F.)
					G3R->G3R_STATUS	:= "3"
				G3R->(MsUnlock())
			EndIf
		EndIf		
		(cAliasTMP2)->(DbCloseArea())		
		(cAliasTMP1)->(DbSkip())
	EndDo
Else
	While (cAliasTMP1)->(!Eof())
		If G3R->(DbSeek(xFilial("G3R")+	(cAliasTmp1)->G4C_NUMID +(cAliasTmp1)->G4C_IDITEM+(cAliasTmp1)->G4C_NUMSEQ))
			Reclock("G3R",.F.)
				G3R->G3R_STATUS	:= "1"
			G3R->(MsUnlock())
		EndIf		
		(cAliasTMP1)->(DbSkip())
	EndDo
EndIf

(cAliasTMP1)->(DbCloseArea())

RestArea(aAreaG3R)
RestArea(aSaveArea)

Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T61ImpDet()

@sample 	T61ImpDet()
@return
@author  	Simone Mie Sato Kakinoana
@since   	05/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T61ImpDet(cCC,cMat,cDepto,cAutViag,cNome,cDataIda,cClasse,cItiner)
	
	Local aSaveArea	:= GetArea()
	Local cRet			:= 	""
	
	cRet	:= STR0037 + Alltrim(cCC) +CRLF					//" Centro de custo: "
	cRet	+= STR0038 + Alltrim(cMat) +CRLF				//" Matr�cula Funcion.: "
	cRet	+= STR0039 + Alltrim(cDepto) +CRLF				//" Departamento: "
	cRet	+= STR0040 + Alltrim(cAutViag) +CRLF			//" Autoriz. Viagem: "
	cRet	+= STR0041 + Alltrim(cNome) +CRLF				//" Nome do Passageiro: "
	cRet	+= STR0042 + Alltrim(cDataIda) +CRLF			//" Data da viagem (Ida): "
	cRet	+= STR0043 + Alltrim(cClasse) +CRLF				//" Classe de Reserva: "
	cRet	+= STR0044 + Alltrim(cItiner) +CRLF				//" Itiner�rio: "
	
	RestArea(aSaveArea)
	
Return(cRet)


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T61ImpVlr()

@sample 	T61ImpDet()
@return
@author  	Simone Mie Sato Kakinoana
@since   	05/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T61ImpVlr(cValor)
	
	Local aSaveArea	:= GetArea()
	Local cRet		:= cValor
	Local nValor	:= 0
	
	cValor := StrTran(cValor,"R$ ","")
	nValor	:= Val(StrTran(cValor,",",".")) 
	
	If  nValor < 0
		cRet	:= Alltrim(Str(Abs(nValor)))
		cRet	:= Strtran(cRet,".",".")
	EndIf
	
	RestArea(aSaveArea)
Return(cRet)


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T61ImpOper()

@sample 	T61ImpOper()
@return
@author  	Simone Mie Sato Kakinoana
@since   	05/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T61ImpOper(cTpTrans,cSinal)
	
	Local cRet	:= ""
	
	If cTpTrans == "2" .And. cSinal =="D"
		cRet	:= "1"
	ElseIf  cTpTrans == "2" .And. cSinal =="C"
		cRet	:= "2"
	ElseIf  cTpTrans <> "2" .And. cSinal =="D"
		cRet	:= "3"
	ElseIf  cTpTrans <> "2" .And. cSinal =="C"
		cRet	:= "4"
	EndIf
	
Return(cRet)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tura061Ctb()

@sample 	Tura061Ctb()
@return
@author  	Simone Mie Sato Kakinoana
@since   	04/05/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function Tura061Ctb(lOnline,lMostrLanc,lAglutLanc,cOrigem,lEfetiva)
	
	Local aSaveArea	:= GetArea()
	
	Local aFlagCTB 	:= {}
	Local cAliasG91	:= GetNextAlias()
	Local cCartao		:= G90->G90_CODIGO
	Local cFatura		:= G90->G90_NUMFAT
	Local cPadT50    	:= "T50"
	Local cPadT51    	:= "T51"
	Local cLoteTur	 	:= LoteCont("TUR")
	Local cFiltLA		:= "% "
	
	Local lPadT50	  	:= VerPadrao(cPadT50)
	Local lPadT51	  	:= VerPadrao(cPadT51)
	Local lUsaFlag		:= SuperGetMV( "MV_CTBFLAG" , .T. /*lHelp*/, .F. /*cPadrao*/)
	
	Local nPosReg		:= 0
	
	Private cArquivo  	:= ""
	Private nHdlPrv		:= 0
	Private nTotal	 		:= 0
	Private LanceiCtb		:= .F.
	
	nHdlPrv := HeadProva(cLoteTur,cOrigem,Substr(cUsuario,7,6),@cArquivo)
	
	If lEfetiva
		cFiltLA	 += " AND G91_LA <> 'S' "
	Else
		cFiltLA	 += " AND G91_LA = 'S' "
	Endif
	cFiltLA		+= " %"
	
	If Select(cAliasG91) > 0
		dbSelectArea(cAliasG91)
		dbCloseArea()
	EndIf
	
	BeginSql Alias cAliasG91
		SELECT *
		FROM %Table:G91% G91
		WHERE G91.G91_FILIAL = %xFilial:G91%
		AND G91.%notDel%
		%Exp:cFiltLA%
		AND (G91_ASSOCI <> '' OR (G91_ASSOCI = '' AND G91_CLASSI NOT IN ('','99')) ) //Somente itens associados ou classificados, que n�o sejam classificados como 'contestados'
		AND G91.G91_CODIGO = %Exp:cCartao%
		AND G91.G91_NUMFAT = %Exp:cFatura%
		ORDER BY G91_CODIGO, G91_NUMFAT, G91_ITEM
	EndSql
	
	While (cAliasG91)->( !EOF() )
		
		DbSelectArea("G91")
		G91->(dbSetOrder(1))
		If G91->(DbSeek(xFilial("G91")+ (cAliasG91)->G91_CODIGO + (cAliasG91)->G91_NUMFAT + (cAliasG91)->G91_ITEM ))
			
			If lEfetiva
				If lPadT50
					If lUsaFlag
						aAdd(aFlagCTB,{"G91_LA","S","G91",G91->(Recno()),0,0,0})
					EndIf
					
					nTotal += DetProva(nHdlPrv,cPadT50,"TURA061",cLoteTur,,,,,,,,@aFlagCTB)
					
					If LanceiCtb // Vem do DetProva
						If !lUsaFlag
							RecLock("G91")
							G91->G91_LA    := "S"
							G91->(MsUnlock())
						EndIf
					ElseIf lUsaFlag
						If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == G91->(Recno()) }))>0
							aFlagCTB := Adel(aFlagCTB,nPosReg)
							aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
						Endif
					EndIf
				EndIf
				
			Else
				If lPadT51
					If lUsaFlag
						aAdd(aFlagCTB,{"G91_LA"," ","G91",G91->(Recno()),0,0,0})
					EndIf
					
					nTotal += DetProva(nHdlPrv,cPadT51,"TURA061",cLoteTur,,,,,,,,@aFlagCTB)
					
					If LanceiCtb // Vem do DetProva
						If !lUsaFlag
							RecLock("G91")
							G91->G91_LA    := " "
							G91->(MsUnlock())
						EndIf
					ElseIf lUsaFlag
						If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == G91->(Recno()) }))>0
							aFlagCTB := Adel(aFlagCTB,nPosReg)
							aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
						Endif
					EndIf
				EndIf
				
			EndIf
		EndIf
		DbSelectArea(cAliasG91)
		(cAliasG91)->( DbSkip() )
	EndDo
	
	If nHdlPrv > 0 .And. nTotal > 0
		RodaProva(nHdlPrv,nTotal)
		BEGIN TRANSACTION
			cA100Incl(cArquivo, nHdlPrv, 3, cLoteTur, lMostrLanc, lAglutLanc,,,,@aFlagCTB,,)
		END TRANSACTION
		cArquivo	:= ""
		nHdlPrv	:= 0
		nTotal		:= 0
	Endif
	
	aFlagCTB := {}
	
	RestArea(aSaveArea)
	
Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA061Commit

Fun��o respons�vel para efetuar o commit do MVC TURA061. Assim, poder� ser 
atualizado os dados dos itens financeiros, quando a opera��o � de exclus�o.

@sample	TurGetErrorMsg()
@author    Fernando Radu Muscalu
@since     16/08/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TA061Commit(oModel)

Local cNumFat	:= oModel:GetModel("G90MASTER"):GetValue("G90_NUMFAT")
Local cNxtAlias	:= GetNextAlias()
Local oModelG4C	

Local lRet	:= .t.

BEGIN TRANSACTION

lRet := oModel:VldData() .And. FwFormCommit(oModel)

If ( lRet .And. oModel:GetOperation() == MODEL_OPERATION_DELETE )

	BeginSQL Alias cNxtAlias
	
		SELECT
			G4C.R_E_C_N_O_ G4C_RECNO
		FROM
			%Table:G4C% G4C
		WHERE
			G4C.%NotDel%
			AND G4C_FATCAR = %Exp:cNumFat%
			AND G4C_ASSOCI <> ''
						
	EndSQL
	
	If ( Select(cNxtAlias) > 0 )
	
		oModelG4C := FwLoadModel("TURA061X")
		
		While ( (cNxtAlias)->(!Eof()) )
			
			oModelG4C:SetOperation(MODEL_OPERATION_UPDATE)
			
			G4C->(DbGoto((cNxtAlias)->G4C_RECNO))
			
			oModelG4C:Activate()
			
			lRet := oModelG4C:GetModel("G4C_MASTER"):SetValue("G4C_ASSOCI","") .And.;
					oModelG4C:GetModel("G4C_MASTER"):SetValue("G4C_FATCAR","")
					
			If ( lRet )
			
				lRet := FwFormCommit(oModelG4C)
					
				oModelG4C:DeActivate()
			Else
				cMsg := TurGetErrorMsg(oModel)						
			Endif
			
			If ( !lRet )
				Exit	
			Endif
				
			(cNxtAlias)->(DbSkip())
			
		EndDo	
			
		(cNxtAlias)->(DbCloseArea())		
	
	Endif
	
Else
	If ( !lRet )
		cMsg := TurGetErrorMsg(oModel)
	Endif		 
Endif

If ( !lRet )
	
	DisarmTransaction()	
	
	FwAlertError(STR0049 + chr(13) + cMsg,STR0050)//"Nao foi poss�vel completar a opera��o, pois ocorreu o seguinte erro: "#"Problema na atualiza��o"
	
Endif

END TRANSACTION

Return(lRet)

//------------------------------------------------------------------------------
/*/{Protheus.doc} Tur61When
Tratamento do when dos campos da rotina.

@autor		Enaldo Cardoso Junior
@since		06/09/2016
@version	P12
/*/
//------------------------------------------------------------------------------
Static Function Tur61When()

Local oModel 	:= FwModelActive()
Local lRet 		:= .F.

	If oModel:GetOperation() == MODEL_OPERATION_INSERT
		lRet := .T.
	Else
		lRet := .F.
	EndIf

Return lRet

/*/{Protheus.doc} TA061Vld()
Efetua a Valida��o na tentativa de alterar os dados dos campos.

@author Fernando Radu Muscalu

@since 11/11/2016
@version 1.0
/*/
Static Function TA061Vld(oSubMdl,nLine,cAction,cCampo) 

Local lRet := .t.

If ( oSubMdl:GetId() == "G91DETAIL" )
	
	If ( !oSubMdl:IsDeleted() )
	
		If ( cAction $ "CANSETVALUE|DELETE" )
			
			//Esta Associado? Ent�o n�o permite a edi��o		
			If ( !Empty(oSubMdl:GetValue("G91_ASSOCI")) .Or. (Empty(oSubMdl:GetValue("G91_ASSOCI")) .And. !Empty(oSubMdl:GetValue("G91_CLASSI")) ) )
				lRet := .F.
				FwAlertHelp(STR0051,STR0052)//"N�o � poss�vel alterar o tipo de opera��o"#"Este item foi previamente associado."
			EndIf
			
		EndIf
	
	EndIf
	
EndIf

Return(lRet)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA61Contest
Fun��o que atualiza os itens da fatura do tipo '1' (d�bito) que n�o foram 
classificados ou associados. Esta atualiza��o passa estes itens para'contestados' 
(classifica��o "99"). 

Os itens em quest�o s�o resultados do filtro utilizado na fun��o T061VldEfet()

@params:	cContest, caractere, Nome do arquivo tempor�rio do resultset da query
utilizada na fun��o T061VldEfet()
 
@sample TA61Contest(cContest)

@autor		Fernando Radu Muscalu
@since		22/11/2016
@version	P12
/*/
//------------------------------------------------------------------------------
Static Function TA61Contest(cContest)

While ( (cContest)->(!Eof()) )
	
	G91->(DbGoTo((cContest)->G91_RECNO))
	
	//Somente itens de d�bito n�o classificados e associados
	If ( G91->G91_TIPO == "1" .And. Empty(G91->G91_CLASSI) .And. Empty(G91->G91_ASSOCI) )
	
		RecLock("G91",.F.)
			G91_CLASSI := "99"
		G91->(MsUnlock())
	
	EndIf
	
	(cContest)->(DbSkip())
		
EndDo	

Return()
