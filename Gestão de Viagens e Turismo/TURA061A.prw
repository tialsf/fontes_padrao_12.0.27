#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TURA061A.CH"

STATIC cCodAssoc		:= NIL

/*/{Protheus.doc} TURA061A
Tela Concilia��o da Fatura de CArt�o 
@type Function
@author Simone Mie Sato Kakinoana
@since 21/02/2016
@version 12.1.7
/*/
Function TURA061A()

Local aEnableButtons 	:= {}

If G90->G90_STATUS == "1"	//Se a concilia��o n�o foi efetivada 
	aEnableButtons 	:= {{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.T.,STR0034},{.T.,STR0035},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil}} //"Confirmar"###"Fechar" 
Else
	//Est� como "Fechar" na posi��o do "Confirmar", para n�o exibir a mensagem do cancelamento.
	aEnableButtons 	:= {{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.T.,STR0035},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil}} //"Fechar"
EndIf

If !SoftLock("G90")	
	Return
Else
	FWExecView(STR0001,'TURA061A', MODEL_OPERATION_UPDATE,, { || .T. },,,aEnableButtons) //'Concilia��o'	
	G90->(MsUnlock())		
EndIf

Return()

/*/{Protheus.doc} TURA061A
Modelo de dados
@type Static Function
@author Simone Mie Sato Kakinoana
@since 21/02/2016
@version 12.1.7
/*/
Static Function ModelDef()

Local oModel   := Nil

Local bLoadG90     	:= { |oMdl| T61ALOAG90( oMdl ) }

Local oStruG90 := FWFormStruct( 1, 'G90', /*bAvalCampo*/,/*lViewUsado*/ )
Local oStruG91 := FWFormStruct( 1, 'G91', /*bAvalCampo*/,/*lViewUsado*/ )
Local oStruG4C := FWFormStruct( 1, 'G4C', /*bAvalCampo*/,.F. /*lViewUsado*/ )

Local oTotFat := FWFormModelStruct():New()
Local oTotFin := FWFormModelStruct():New()

T61AStrTot(oTotFat,oTotFin,1)

// Cria o objeto do Modelo de Dados
oModel := MPFormModel():New('TURA061A', /*bPreValidacao*/, { |oModel| T061TUDOK( oModel )}/*bPosValidacao*/, { |oModel| T061AGRAVA( oModel )}/*bCommit*/, { |oModel| T061ADelCart(oModel)}/*bCancel*/ )

oStruG91:SetProperty('G91_OPERAC'	,MODEL_FIELD_WHEN	,{||.F.   	})
oStruG91:SetProperty('G91_DOCFOR'	,MODEL_FIELD_WHEN	,{||.F.   	})
oStruG91:SetProperty('G91_FORNEC'	,MODEL_FIELD_WHEN	,{||.F.   	})
oStruG91:SetProperty('G91_VALOR'	,MODEL_FIELD_WHEN	,{||.F.   	})
oStruG91:SetProperty('G91_TARIFA'	,MODEL_FIELD_WHEN	,{||.F.   	})
oStruG91:SetProperty('G91_TAXAS'	,MODEL_FIELD_WHEN	,{||.F.   	})
oStruG91:SetProperty('G91_EXTRAS'	,MODEL_FIELD_WHEN	,{||.F.   	})
oStruG91:SetProperty('G91_FORNEC'	,MODEL_FIELD_WHEN	,{||.F.   	})
oStruG91:SetProperty('G91_DTEMIS'	,MODEL_FIELD_WHEN	,{||.F.   	})
oStruG91:SetProperty('G91_TIPO'		,MODEL_FIELD_WHEN	,{||.F.   	})
oStruG91:SetProperty('G91_DETAIL'	,MODEL_FIELD_WHEN	,{||.F.   	})

oStruG4C:SetProperty('G4C_CLASS'	,MODEL_FIELD_WHEN	,{||.F.   	})
oStruG4C:SetProperty('G4C_CONDPG',MODEL_FIELD_WHEN	,{||.F.   	})
oStruG4C:SetProperty('G4C_NATUR',MODEL_FIELD_WHEN	,{||.F.   	})
oStruG4C:SetProperty('G4C_OBS',MODEL_FIELD_WHEN	,{||.F.   	})
oStruG4C:SetProperty('G4C_TPFOP',MODEL_FIELD_WHEN	,{||.F.   	})

// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
oModel:AddFields('G90MASTER', /*cOwner*/, oStruG90, /*Criptog()/, /*bPosValidacao*/, /*bCancel*/, bLoadG90)
oModel:AddGrid( 'G91DETAIL', 'G90MASTER', oStruG91, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/ )
oModel:AddGrid( 'G4CDETAIL', 'G90MASTER', oStruG4C, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/,   )
oModel:AddFields('TOTFAT', 'G90MASTER'/*cOwner*/, oTotFat, /*Criptog()/, /*bPosValidacao*/, /*bCancel*/,{||} )
oModel:AddFields('TOTFIN', 'G90MASTER'/*cOwner*/, oTotFin, /*Criptog()/, /*bPosValidacao*/, /*bCancel*/,{||} )


// Faz relacionamento pai com filho
oModel:SetRelation( 'G91DETAIL', { { 'G91_FILIAL', 'xFilial( "G91" ) ' } ,;
								   { 'G91_CODIGO', 'G90_CODIGO' } ,;
								   { 'G91_NUMFAT', 'G90_NUMFAT' 	   } } , G91->( IndexKey( 1 ) ) )

oModel:SetRelation( 'G4CDETAIL', { { 'G4C_FILIAL' , 'xFilial( "G4C" ) ' } , ;
								   { 'G4C_CARTUR'  , 'G90_CODIGO'         } , ;
								   { 'G4C_FATCAR' , 'G90_NUMFAT'        } } , G4C->( IndexKey( 1 ) ) )

oModel:GetModel("G91DETAIL"):SetNoInsertLine(.T.)
oModel:GetModel("G91DETAIL"):SetNoDeleteLine(.T.)

oModel:GetModel("G4CDETAIL"):SetNoInsertLine(.T.)
oModel:GetModel("G4CDETAIL"):SetNoDeleteLine(.T.)

  

	oStruG91:AddField( ;
							" " 				, ; // [01] C Titulo do campo
							"" 				, ; // [02] C ToolTip do campo
							"G91_LEGEN1" 	, ; // [03] C identificador (ID) do Field
							"C" 			, ; // [04] C Tipo do campo
							15 				, ; // [05] N Tamanho do campo
							0 				, ; // [06] N Decimal do campo
							NIL				, ; // [07] B Code-block de valida��o do campo
							NIL 			, ; // [08] B Code-block de valida��o When do campoz
							NIL 			, ; // [09] A Lista de valores permitido do campo
							NIL 			, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
							{||""} 			  , ;
							NIL 			, ; // [12] L Indica se trata de um campo chave
							.T. 			, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. 			) // [14] L Indica se o campo � virtual

	oStruG91:AddField(	"",;						// Titulo //"Numero"
						"Check",;					// Descri��o Tooltip 
						"G91_OK",;					// Nome do Campo
						"L",;						// Tipo de dado do campo
						1,;							// Tamanho do campo
						0,;							// Tamanho das casas decimais
						{|| .T.},;					// Bloco de Valida��o do campo
						{|| !T061AChkEfet(.T.)},;					// Bloco de Edi��o do campo
						{},; 						// Op��es do combo
						.F.,; 						// Obrigat�rio
						NIL,; 						// Bloco de Inicializa��o Padr�o
						.F.,; 						// Campo � chave
						.F.,; 						// Atualiza?
						.F.) 						// Virtual?


	oStruG4C:AddField( ;
							" " 				, ; // [01] C Titulo do campo
							"" 				, ; // [02] C ToolTip do campo
							"G4C_LEGEN1" 	, ; // [03] C identificador (ID) do Field
							"C" 			, ; // [04] C Tipo do campo
							15				, ; // [05] N Tamanho do campo
							0 				, ; // [06] N Decimal do campo
							NIL				, ; // [07] B Code-block de valida��o do campo
							NIL 			, ; // [08] B Code-block de valida��o When do campoz
							NIL 			, ; // [09] A Lista de valores permitido do campo
							NIL 			, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
							{||""}			, ;
							NIL 			, ; // [12] L Indica se trata de um campo chave
							.T. 			, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. 			) // [14] L Indica se o campo � virtual

							
	oStruG4C:AddField(	"",;						// Titulo //"Numero"
						"Check",;					// Descri��o Tooltip 
						"G4C_OK",;					// Nome do Campo
						"L",;						// Tipo de dado do campo
						1,;							// Tamanho do campo
						0,;							// Tamanho das casas decimais
						{|| .T.},;					// Bloco de Valida��o do campo
						{|| !T061AChkEfet(.T.)},;					// Bloco de Edi��o do campo
						{},; 						// Op��es do combo
						.F.,; 						// Obrigat�rio
						NIL,; 						// Bloco de Inicializa��o Padr�o
						.F.,; 						// Campo � chave
						.F.,; 						// Atualiza?
						.F.) 						// Virtual?

// Adiciona a descricao do Modelo de Dados
oModel:SetDescription( STR0001 )	//"Concilia��o

// Adiciona a descricao do Componente do Modelo de Dados
oModel:GetModel( "G90MASTER" ):SetDescription( STR0002 )	//"Fatura de Cart�o de Turismo"
oModel:GetModel( "G91DETAIL" ):SetDescription( STR0003 )	//"Itens da Fatura"	
oModel:GetModel( "G4CDETAIL" ):SetDescription(  STR0004 )	//"Itens Financeiros"
oModel:GetModel( "TOTFAT" ):SetDescription(  STR0019  )	//"Total da Fatura
oModel:GetModel( "TOTFIN" ):SetDescription(  STR0021 )	//"Total dos Itens Financeiros:"

oModel:GetModel( 'G90MASTER' ):SetOnlyView ( .T. )
oModel:GetModel( 'G91DETAIL'):SetOptional(.T.) 
oModel:GetModel( 'G4CDETAIL'):SetOptional(.T.)
oModel:GetModel( 'G4CDETAIL'):SetOnlyQuery(.T.)
  
oModel:SetActivate({|oModel| TA061ALoad(oModel) })


Return oModel

/*/{Protheus.doc} TURA061A
ViewDef
@type Static Function
@author Simone Mie Sato Kakinoana
@since 21/02/2016
@version 12.1.7
/*/
Static Function ViewDef()

Local oStruG90 	:= FWFormStruct( 2, 'G90' )
Local oStruG91 	:= FWFormStruct( 2, 'G91' )
Local oStruG4C 	:= FWFormStruct( 2, 'G4C' )

Local oModel   	:= FWLoadModel( "TURA061A" )
Local oGridG4C		:= oModel:GetModel("G4CDETAIL")
Local oTotFat 		:= FWFormViewStruct():New()
Local oTotFin 		:= FWFormViewStruct():New()
Local oView	   	:= Nil

Local aStruG4C		:= {}
Local aAux			:= {}
Local aFieldsG4C	:= {"G4C_NUMID","G4C_IDITEM","G4C_IDIF","G4C_TIPO","G4C_PAGREC","G4C_VALOR","G4C_DOC","G4C_TARIFA","G4C_TAXA","G4C_EXTRA","G4C_EMISS","G4C_FATCAR","G4C_ASSOCI","G4C_CARTUR"}

Local nCont		:= 0 

aAux		:=  oGridG4C:GetStruct()
aStruG4C	:= aAux:GetFields()

// Cria o objeto de View
oView := FWFormView():New()

//Remove campos do G4C
For nCont := 1 to Len(aStruG4C)
	If ASCAN(aFieldsG4C,aStruG4C[nCont][3]) == 0 
		oStruG4C:RemoveField(aStruG4C[nCont][3])
	EndIf
Next

// Define qual o Modelo de dados ser� utilizado
oView:SetModel( oModel )


T61AStrTot(oTotFat,oTotFin,2)

//Campo virtual para apresentar legenda 
oStruG91:AddField( ;
						"G91_LEGEN1" 	, ; // [01] C Nome do Campo
						"00" 			, ; // [02] C Ordem
						"" 				, ; // [03] C Titulo do campo
						""				, ; // [04] C Descri��o do campo
						NIL				, ; // [05] A Array com Help
						"C" 			, ; // [06] C Tipo do campo
						"@BMP" 			, ; // [07] C Picture
						NIL 			, ; // [08] B Bloco de Picture Var
						"" 				, ; // [09] C Consulta F3
						.F. 			, ; // [10] L Indica se o campo � edit�vel
						NIL 			, ; // [11] C Pasta do campo
						NIL 			, ; // [12] C Agrupamento do campo
						NIL 			, ; // [13] A Lista de valores permitido do campo (Combo)
						NIL 			, ; // [14] N Tamanho M�ximo da maior op��o do combo
						NIL 			, ; // [15] C Inicializador de Browse
						.T. 			, ; // [16] L Indica se o campo � virtual
						NIL 			) 	// [17] C Picture Vari�vel

												
oStruG91:AddField(	"G91_OK",;						// [01] C Nome do Campo
					"02",;							// [02] C Ordem
					" ",; 							// [03] C Titulo do campo //"Numero"
					"Check",; 						// [04] C Descri��o do campo //"Numero do Docto. de Saida"
					{"Check."} ,;					// [05] A Array com Help //"Numero do Docto. de Saida."
					"GET",; 						// [06] C Tipo do campo - GET, COMBO OU CHECK
					"@!",;							// [07] C Picture
					NIL,; 							// [08] B Bloco de Picture Var
					"",; 							// [09] C Consulta F3
					.T.,; 							// [10] L Indica se o campo � edit�vel
					NIL, ; 							// [11] C Pasta do campo
					NIL,; 							// [12] C Agrupamento do campo
					{},; 							// [13] A Lista de valores permitido do campo (Combo)
					NIL,; 							// [14] N Tamanho Maximo da maior op��o do combo
					NIL,;	 						// [15] C Inicializador de Browse
					.T.) 							// [16] L Indica se o campo � virtual						
						

//Campo virtual para apresentar legenda 
oStruG4C:AddField( ;
						"G4C_LEGEN1" 	, ; // [01] C Nome do Campo
						"00" 			, ; // [02] C Ordem
						"" 				, ; // [03] C Titulo do campo
						""				, ; // [04] C Descri��o do campo
						NIL				, ; // [05] A Array com Help
						"C" 			, ; // [06] C Tipo do campo
						"@BMP" 		, ; // [07] C Picture
						NIL 			, ; // [08] B Bloco de Picture Var
						"" 				, ; // [09] C Consulta F3
						.F. 			, ; // [10] L Indica se o campo � edit�vel
						NIL 			, ; // [11] C Pasta do campo
						NIL 			, ; // [12] C Agrupamento do campo
						NIL 			, ; // [13] A Lista de valores permitido do campo (Combo)
						NIL 			, ; // [14] N Tamanho M�ximo da maior op��o do combo
						NIL 			, ; // [15] C Inicializador de Browse
						.T. 			, ; // [16] L Indica se o campo � virtual
						NIL 			) 	// [17] C Picture Vari�vel

oStruG4C:AddField(	"G4C_OK",;						// [01] C Nome do Campo
					"02",;							// [02] C Ordem
					" ",; 							// [03] C Titulo do campo //"Numero"
					"Check",; 						// [04] C Descri��o do campo //"Numero do Docto. de Saida"
					{"Check."} ,;					// [05] A Array com Help //"Numero do Docto. de Saida."
					"GET",; 						// [06] C Tipo do campo - GET, COMBO OU CHECK
					"@!",;							// [07] C Picture
					NIL,; 							// [08] B Bloco de Picture Var
					"",; 							// [09] C Consulta F3
					.T.,; 							// [10] L Indica se o campo � edit�vel
					NIL, ; 							// [11] C Pasta do campo
					NIL,; 							// [12] C Agrupamento do campo
					{},; 							// [13] A Lista de valores permitido do campo (Combo)
					NIL,; 							// [14] N Tamanho Maximo da maior op��o do combo
					NIL,;	 						// [15] C Inicializador de Browse
					.T.) 							// [16] L Indica se o campo � virtual						
						

oView:AddField( 'VIEW_G90', oStruG90, 'G90MASTER' )
oView:AddGrid(  'VIEW_G91', oStruG91, 'G91DETAIL' )
oView:AddGrid(	'VIEW_G4C', oStruG4C, 'G4CDETAIL' )

oView:AddField( 'VIEW_TOTFAT', oTotFat, 'TOTFAT' )
oView:AddField( 'VIEW_TOTFIN', oTotFin, 'TOTFIN' )

oView:SetViewProperty('VIEW_G91', "ENABLENEWGRID")
oView:SetViewProperty("VIEW_G91", "GRIDSEEK", {.T.})
oView:SetViewProperty("VIEW_G91", "GRIDROWHEIGHT", {20}) 

 
oView:SetViewProperty('VIEW_G4C', "ENABLENEWGRID")
oView:SetViewProperty("VIEW_G4C", "GRIDSEEK", {.T.})
oView:SetViewProperty("VIEW_G4C", "GRIDROWHEIGHT", {20}) 

If ( G90->G90_STATUS == "1" ) 
	oView:SetViewProperty("VIEW_G4C", "GRIDDOUBLECLICK", {{|oFormulario,cFieldName,nLineGrid,nLineModel| MostraAssocia(oFormulario,cFieldName,nLineGrid,nLineModel, oModel:GetValue("G4CDETAIL","G4C_ASSOCI"))}}) 
	oView:SetViewProperty("VIEW_G91", "GRIDDOUBLECLICK", {{|oFormulario,cFieldName,nLineGrid,nLineModel| MostraAssocia(oFormulario,cFieldName,nLineGrid,nLineModel, oModel:GetValue("G91DETAIL","G91_ASSOCI"))}})
EndIf

oView:CreateHorizontalBox( "GERAL", 100 )
oView:CreateFolder('PASTAS','GERAL')
oView:AddSheet('PASTAS','PASTA1',STR0014) //"Dados Fatura"
oView:CreateHorizontalBox( "SUPERIOR", 100,,,'PASTAS','PASTA1')
oView:SetOwnerView( "VIEW_G90", "SUPERIOR"	)

oView:AddSheet('PASTAS','PASTA2',STR0015)	//"Concilia��o"
oView:CreateHorizantalBox( "PRINCIPAL",85 ,,,'PASTAS','PASTA2')
oView:CreateVerticalBox( "ITEMFAT", 50 ,'PRINCIPAL',,'PASTAS','PASTA2')
oView:CreateVerticalBox( "ITEMFIN", 50 ,'PRINCIPAL',,'PASTAS','PASTA2')

oView:CreateVerticalBox( "MEIO_ESQ" ,100, "ITEMFAT" ,.F.,'PASTAS','PASTA2')
oView:CreateVerticalBox( "INF_ESQ" , 100, "ITEMFIN" ,.F.,'PASTAS','PASTA2')

oView:CreateHorizontalBox( 'TOTAL' , 15 ,,,'PASTAS','PASTA2')
oView:CreateVerticalBox( "TOT_ESQ", 50 ,'TOTAL',,'PASTAS','PASTA2')
oView:CreateVerticalBox( "TOT_DIR", 50 ,'TOTAL',,'PASTAS','PASTA2')

oView:CreateVerticalBox( "TOT_FAT" ,100, "TOT_ESQ" ,.F.,'PASTAS','PASTA2')
oView:CreateVerticalBox( "TOT_FIN" ,100, "TOT_DIR" ,.F.,'PASTAS','PASTA2')

oView:SetOwnerView( "VIEW_G91", "MEIO_ESQ" )
oView:SetOwnerView( "VIEW_G4C", "INF_ESQ" )

oView:EnableTitleView( "VIEW_G91",STR0003)	//"Itens da Fatura"
oView:EnableTitleView( "VIEW_G4C",STR0004)	//"Itens Financeiros"

oView:SetOwnerView( 'VIEW_TOTFAT', 'TOT_FAT' )
oView:SetOwnerView( 'VIEW_TOTFIN', 'TOT_FIN' )

oView:AddUserButton( STR0005, "CLIPS", {|oView|  TURA061E(oModel,oView)} ) //"Pesquisar DR"##Processando
oView:AddUserButton( STR0022, 'CLIPS', {|oView|  FwMsgRun(,{||T061Associ()   },,STR0018)} ) //"Associar" ## "Processando" 
oView:AddUserButton( STR0006, 'CLIPS', {|oView|  FwMsgRun(,{||T061ADesAs()   },,STR0018)} ) //""Desassociar"
oView:AddUserButton( STR0007, 'CLIPS', {|oView|  T061AClass(oModel,oView)} ) //"Classif.Adic."
oView:AddUserButton( STR0009, 'CLIPS', {|oView| TURA061C(oModel,oView)} ) //"Resumo"
oView:AddUserButton( STR0026, 'CLIPS', {|oView| T061ALeg(oModel,oView)} ) //"Legenda"
//oView:AddUserButton( STR0008, 'CLIPS', {|oView|FwMsgRun(,{||T061AVisRV()   },,STR0008)} )//"Visualizar RV"

If G90->G90_STATUS == "2"
	oView:ShowUpdateMsg(.F.)
EndIf

Return oView

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T61ALOAG90

Carrega dados descriptografados quando opera��o diferente de insert.

@sample 	T61ALOAG90(oMdl)
@return  	aDados: dados descriptografados
@author  	Simone Mie Sato Kakinoana
@since   	23/02/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T61ALOAG90(oMdl)

Local aArea	   		:= GetArea()
Local oStructG90 	:= oMdl:GetStruct()
Local aCampos    	:= oStructG90:GetFields()
Local aLoad			:= {}
Local aDados		:= {}
Local nOperation	:= oMdl:GetOperation()
Local nX			:= 0
Local cChave    	:= G90->G90_CODIGO
Local cNCard    	:= G90->G90_NCARD
Local uValor 		:= Nil

// Carrega campos G90_NCARD descriptografados quando opera��o
// for diferente de insert
If ( nOperation <> MODEL_OPERATION_INSERT )

	For nX := 1 To Len(aCampos)
		If !(aCampos[nX][MODEL_FIELD_VIRTUAL])
   			uValor	:= G90->&( aCampos[nX][MODEL_FIELD_IDFIELD] )
   		 	aAdd( aLoad, uValor )
       	Else
			uValor	:= CriaVar( aCampos[nX][MODEL_FIELD_IDFIELD], .F. )
			aAdd( aLoad, uValor )
        EndIf
	Next

	aDados:= { aLoad, G90->( Recno() ) }
EndIf

RestArea( aArea )

Return aDados


/*/{Protheus.doc} T061AChkEfet(lHelp)
Verifica se o registro foi efetivado

@author Simone Mie Sato Kakinoana

@since 24/02/2016
@version 1.0
/*/
Function T061AChkEfet(lHelp)

Local lRet		:= .T.

DEFAULT lHelp	:= .T.

If G90->G90_STATUS == '1'//Aberta
	lRet	:= .F.	
Else
	lRet	:= .T.
	If lHelp
		Help(" ",1,"T061AJAEFET",,STR0010,3,1) // "Concilia��o j� efetivada."
	EndIf
Endif	

Return(lRet)

/*/{Protheus.doc} T061AClass()
Classifica o item adicional

@author Simone Mie Sato Kakinoana

@since 24/02/2016
@version 1.0
/*/
Function T061AClass(oModel,oView)

Local lContinua	:= .F.
Local lOk        := .F.

Local cCodigo	:= Replicate (" ", Len(G91->G91_CLASSI))
Local cDescricao := ""

Local oBtn1      := Nil
Local oBtn2      := Nil
Local oDlg       := Nil
Local oModel     := FwModelActive()
Local oGridG91	:= oModel:GetModel('G91DETAIL')

Local nX		:= 0
Local nLinAtu	:= oGridG91:GetLine()

If T061AChkEfet(.T.)	
	Return
Endif

Define MsDialog oDlg TITLE STR0012 FROM 000, 000 To 090, 500 Pixel //"Classificar Adicional "	      

@ 003,003 SAY STR0011 OF oDlg PIXEL SIZE oDlg:nClientWidth/2-5,10 //"C�d. Classif. "
@ 015,003 MSGET cCodigo OF oDlg VALID (Empty(cCodigo) .Or. Iif(ExistCpo("SX5","X6"+cCodigo),(cDescricao := Posicione("SX5",1,xFilial("SX5")+"X6"+cCodigo,"SX5->X5_DESCRI" ),.T.),.F.)) F3 "X6" PICTURE "@!" PIXEL SIZE 70,010

@ 003,080 SAY STR0016 OF oDlg PIXEL SIZE oDlg:nClientWidth/2-85,10 //"Descri��o"
@ 015,080 MSGET cDescricao OF oDlg PICTURE "@!" WHEN .F. PIXEL SIZE oDlg:nClientWidth/2-95,010

@ 030,003 Button oBtn1 Prompt STR0017  Of oDlg Size 035, 010 Pixel //"Ok" 
      oBtn1:bAction := { || lContinua := .T., oDlg:End() }

Activate MsDialog oDlg Centered

If ( lContinua )
	
	For nX := 1 To oGridG91:Length()	
		
		oGridG91:GoLine(nX)		
		
		lOk := oGridG91:GetValue("G91_OK")
		
		If ( lOk )
			
			//Se o item j� estiver associado, n�o permite classificar adicional. 
			If 	!Empty(oGridG91:GetValue("G91_ASSOCI"))				
				oGridG91:SetValue("G91_OK",.F.)
				Loop
			Endif
			
			If ( !Empty(cCodigo) )
				
				oGridG91:SetValue("G91_CLASSI",cCodigo)
				
				If ( Alltrim(cCodigo) <> "99" )
					oGridG91:LoadValue("G91_LEGEN1","BR_PINK")
				Else
					oGridG91:LoadValue("G91_LEGEN1","BR_BRANCO")
				EndIf
						
			Else
				
				oGridG91:SetValue("G91_CLASSI","")
				
				If ( oGridG91:GetValue("G91_TIPO") == "1" ) 
					oGridG91:LoadValue("G91_LEGEN1","BR_VERMELHO")
				Else
					oGridG91:LoadValue("G91_LEGEN1","BR_PRETO")
				EndIf
					
			Endif		
			
			oGridG91:SetValue("G91_OK",.F.)
			
		Endif
		
	Next nX

	oGridG91:GoLine(nLinAtu)

	oView:Refresh("VIEW_G4C")
	oView:Refresh("VIEW_G91")
                         
EndIf

Return(lContinua)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA061ALoad

Carrega dados da Grid G91

@sample 	TA061ALoad(oMdl)
@return  	aDados 
@author  	Simone Mie Sato Kakinoana
@since   	27/02/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function TA061ALoad(oModel)

Local aArea	   		:= GetArea()
Local aAuxG91		:= {}	//C�d. assoc/Valor Debito/Valor Credito
Local aAuxG4C		:= {}

Local oModelG91		:= oModel:GetModel("G91DETAIL")
Local oModelG4C		:= oModel:GetModel("G4CDETAIL")
Local oTotFat		:= oModel:GetModel("TOTFAT")
Local oTotFin		:= oModel:GetModel("TOTFIN")

Local nX			:= 0
Local nTolera		:= G90->G90_TOLERA
Local nPosG4C		:= 0 
Local nPosG91		:= 0
Local nAssoci		:= 0
Local nTotFat 		:= 0
Local nTotFin		:= 0
Local nTotFatAss	:= 0
Local nTotFinAss	:= 0
Local nValDebG91	:= 0
Local nValCrdG91	:= 0 
Local nValDebG4C	:= 0
Local nValCrdG4C	:= 0
Local nDiferenca	:= 0  
Local cCodAssoci	:= ""

//Varre grid G91
For nX := 1 To oModelG91:Length()
	oModelG91:GoLine(nX)
	
	If oModelG91:GetValue("G91_TIPO") == "1"		//Se for debito 
		nTotFat		+= oModelG91:GetValue("G91_VALOR")
		
		//Se n�o estiver associado, atualiza a legenda	
		If Empty(oModelG91:GetValue("G91_ASSOCI"))
			
			If !Empty(oModelG91:GetValue("G91_CLASSI"))	//Se for classifica��o adicional
				
				If ( Alltrim(oModelG91:GetValue("G91_CLASSI")) == "99" ) //Classifica��o de Contestado
					oModelG91:LoadValue("G91_LEGEN1","BR_BRANCO")
				Else	
					oModelG91:LoadValue("G91_LEGEN1","BR_PINK")
				EndIf
				
			Else
				oModelG91:LoadValue("G91_LEGEN1","BR_VERMELHO")			
			EndIf
			
		Else
			nPosG91	:= ASCAN(aAuxG91,{|x| x[1]== oModelG91:GetValue("G91_ASSOCI")}) 
			If  nPosG91 == 0				 
				AADD(aAuxG91,{oModelG91:GetValue("G91_ASSOCI"),oModelG91:GetValue("G91_VALOR"),0})
			Else			
				aAuxG91[nPosG91][2]	+= oModelG91:GetValue("G91_VALOR")
			EndIf
		EndIf
	Else 		//Se for credito
		
		nTotFat		-= oModelG91:GetValue("G91_VALOR")
		
		//Se n�o estiver associado, atualiza a legenda	
		If Empty(oModelG91:GetValue("G91_ASSOCI"))
		
			If !Empty(oModelG91:GetValue("G91_CLASSI"))	//Se for classifica��o adicional
				
				If ( Alltrim(oModelG91:GetValue("G91_CLASSI")) == "99" ) //Classifica��o de Contestado
					oModelG91:LoadValue("G91_LEGEN1","BR_BRANCO")
				Else	
					oModelG91:LoadValue("G91_LEGEN1","BR_PINK")
				EndIf
					
			Else		
				oModelG91:LoadValue("G91_LEGEN1","BR_PRETO")
			EndIf
		Else 
			nPosG91	:= ASCAN(aAuxG91,{|x| x[1]== oModelG91:GetValue("G91_ASSOCI")}) 
			If  nPosG91 == 0				 
				AADD(aAuxG91,{oModelG91:GetValue("G91_ASSOCI"),0,oModelG91:GetValue("G91_VALOR")})
			Else			
				aAuxG91[nPosG91][3]	+= oModelG91:GetValue("G91_VALOR")
			EndIf
		EndIf
	Endif
					
Next

For nX := 1 To oModelG4C:Length()
	oModelG4C:GoLine(nX)
	
	If oModelG4C:GetValue("G4C_PAGREC") == "1"		//Se for debito 
		nTotFin		+= oModelG4C:GetValue("G4C_VALOR")
		
		//Se n�o estiver associado, atualiza a legenda	
		If Empty(oModelG4C:GetValue("G4C_ASSOCI"))
			oModelG4C:LoadValue("G4C_LEGEN1","BR_VERMELHO")
		Else
			nPosG4C	:= ASCAN(aAuxG4C,{|x| x[1]==oModelG4C:GetValue("G4C_ASSOCI")}) 
			If  nPosG4C == 0				 
				AADD(aAuxG4C,{oModelG4C:GetValue("G4C_ASSOCI"),oModelG4C:GetValue("G4C_VALOR"),0})
			Else			
				aAuxG4C[nPosG4C][2]	+= oModelG4C:GetValue("G4C_VALOR")
			EndIf
		EndIf
	ElseIf oModelG4C:GetValue("G4C_PAGREC") == "2" 		//Se for credito
		nTotFin		-= oModelG4C:GetValue("G4C_VALOR")
		//Se n�o estiver associado, atualiza a legenda	
		If Empty(oModelG4C:GetValue("G4C_ASSOCI"))
			oModelG4C:LoadValue("G4C_LEGEN1","BR_PRETO")
		Else
			nPosG4C	:= ASCAN(aAuxG4C,{|x| x[1]==oModelG4C:GetValue("G4C_ASSOCI")})
			If nPosG4C== 0				 
				AADD(aAuxG4C,{oModelG4C:GetValue("G4C_ASSOCI"),0,oModelG4C:GetValue("G4C_VALOR")})				
			Else			
				aAuxG4C[nPosG4C][3]	+= oModelG4C:GetValue("G4C_VALOR")
			EndIf
		EndIf
	Endif				
Next nX
oModelG91:GoLine(1)

//Atualiza legendas G91
For nX := 1 To oModelG91:Length()
	oModelG91		:GoLine(nX)
	cCodAssoci		:= oModelG91:GetValue("G91_ASSOCI")
		
	nPosG91			:= ASCAN(aAuxG91,{|x| x[1]== cCodAssoci }) 
	nPosG4C			:= ASCAN(aAuxG4C,{|x| x[1]== cCodAssoci })
	
	If nPosG91 > 0 .And. nPosG4C > 0  
		nValDebG91		:= aAuxG91[nPosG91][2]
		nValCrdG91		:= aAuxG91[nPosG91][3]
		nValDebG4C		:= aAuxG4C[nPosG4C][2]
		nValCrdG4C		:= aAuxG4C[nPosG4C][3] 
			
		
		If nValDebG91 == nValDebG4C .And. nValCrdG91 == nValCrdG4C
			oModelG91:LoadValue("G91_LEGEN1","BR_VERDE")
		Else
			If  nValDebG91 <> nValDebG4C .And. nValCrdG91 == nValCrdG4C	//Diferen�a s� a debito
				If ( nValDebG91 - nValDebG4C) > 0 
					If  nValDebG91 - nValDebG4C  <=  nTolera
						oModelG91:LoadValue("G91_LEGEN1","BR_AMARELO")
					Else
						oModelG91:LoadValue("G91_LEGEN1","BR_LARANJA")
					EndIf	
				ElseIf ( nValDebG91 - nValDebG4C) < 0				
					oModelG91:LoadValue("G91_LEGEN1","BR_AZUL")	//ASSOCIA��O COM CR�DITO
				EndIf 
		 	ElseIf nValCrdG91 <> nValCrdG4C .And. nValDebG91 == nValDebG4C  	//Diferen�a s� a credito
		 		If ( nValCrdG4C - nValCrdG91) > 0
					If (ABS( nValCrdG4C - nValCrdG91)  <=  nTolera	  ) // Se a diferen�a for menor que a toler�ncia
					 	oModelG91:LoadValue("G91_LEGEN1","BR_AMARELO")				 	
					ElseIf ABS( nValCrdG4C - nValCrdG91) >  nTolera  // Se a diferen�a for maior que a toler�ncia				 	   
		 				oModelG91:LoadValue("G91_LEGEN1","BR_LARANJA")		 			
		 			EndIf
		 		ElseIf ( nValCrdG4C - nValCrdG91) < 0
		 			oModelG91:LoadValue("G91_LEGEN1","BR_AZUL")	//ASSOCIA��O COM CR�DITO	
		 		EndIf
		 	ElseIf  nValCrdG91 <> nValCrdG4C .And. nValDebG91 <> nValDebG4C  	//Se houver diferen�a no debito e no credito
		 		nDiferenca	:= (( nValDebG91 - nValCrdG91) - (nValDebG4C - nValCrdG4C))
		 			 
		 		If nDiferenca <> 0	 
					If nDiferenca > 0 	//Associa��o com debito
						If ABS(nDiferenca) <= nTolera	//Menor que a tolerancia
							oModelG91:LoadValue("G91_LEGEN1","BR_AMARELO")
						Else			//Maior que a toler�ncia
							oModelG91:LoadValue("G91_LEGEN1","BR_LARANJA")						
						EndIf		
					ElseIf nDiferenca < 0		//Associa��o com credito
						oModelG91:LoadValue("G91_LEGEN1","BR_AZUL")						
		 			EndIf
		 		Else	//Se nao teve diferenca	 				
		 			oModelG91:LoadValue("G91_LEGEN1","BR_VERDE")
		 		EndIf
		 		
		 	EndIf
		EndIf		
	EndIf
Next

//Atualiza legendas G4C
For nX := 1 To oModelG4C:Length()
	oModelG4C:GoLine(nX)
	cCodAssoci		:= oModelG4C:GetValue("G4C_ASSOCI")
		
	nPosG91			:= ASCAN(aAuxG91,{|x| x[1]== cCodAssoci }) 
	nPosG4C			:= ASCAN(aAuxG4C,{|x| x[1]== cCodAssoci })
	
	If nPosG91 > 0 .And. nPosG4C > 0  
		nValDebG91		:= aAuxG91[nPosG91][2]
		nValCrdG91		:= aAuxG91[nPosG91][3]
		nValDebG4C		:= aAuxG4C[nPosG4C][2]
		nValCrdG4C		:= aAuxG4C[nPosG4C][3] 
			
		
		If nValDebG91 == nValDebG4C .And. nValCrdG91 == nValCrdG4C
			oModelG4C:LoadValue("G4C_LEGEN1","BR_VERDE")
		Else
			If  nValDebG91 <> nValDebG4C .And. nValCrdG91 == nValCrdG4C	//Diferen�a s� a debito
				If ( nValDebG91 - nValDebG4C) > 0 
					If  nValDebG91 - nValDebG4C  <=  nTolera
						oModelG4C:LoadValue("G4C_LEGEN1","BR_AMARELO")
					Else
						oModelG4C:LoadValue("G4C_LEGEN1","BR_LARANJA")
					EndIf	
				ElseIf ( nValDebG91 - nValDebG4C) < 0				
					oModelG4C:LoadValue("G4C_LEGEN1","BR_AZUL")	//ASSOCIA��O COM CR�DITO
				EndIf 
		 	ElseIf nValCrdG91 <> nValCrdG4C .And. nValDebG91 == nValDebG4C  	//Diferen�a s� a credito
		 		If nValCrdG4C - nValCrdG91 > 0 
					If (ABS( nValCrdG4C - nValCrdG91)  <=  nTolera	 ) // Se a diferen�a for menor que a toler�ncia
				 		oModelG4C:LoadValue("G4C_LEGEN1","BR_AMARELO")				 	
					ElseIf ABS( nValCrdG91 - nValCrdG4C) >  nTolera  // Se a diferen�a for maior que a toler�ncia				 	   
		 				oModelG4C:LoadValue("G4C_LEGEN1","BR_LARANJA")		 			
		 			EndIf
		 		ElseIf ( nValCrdG4C - nValCrdG91) < 0
		 			oModelG4C:LoadValue("G4C_LEGEN1","BR_AZUL")	//ASSOCIA��O COM CR�DITO
		 		EndIf
		 	ElseIf  nValCrdG91 <> nValCrdG4C .And. nValDebG91 <> nValDebG4C  	//Se houver diferen�a no debito e no credito
		 		nDiferenca	:= (( nValDebG91 - nValCrdG91) - (nValDebG4C - nValCrdG4C))
		 			 
		 		If nDiferenca <> 0	 
					If nDiferenca > 0 	//Associa��o comm d�bito
						If ABS(nDiferenca) <= nTolera	//"Menor que a toler�ncia"
							oModelG4C:LoadValue("G4C_LEGEN1","BR_AMARELO")
						Else								// "Maior que a toler�ncia"
							oModelG4C:LoadValue("G4C_LEGEN1","BR_LARANJA")						
						EndIf		
					ElseIf nDiferenca < 0		//Associa��o com cr�dito 
						oModelG4C:LoadValue("G4C_LEGEN1","BR_AZUL")
		 			EndIf
		 		Else	//Se nao teve diferenca	 				
		 			oModelG4C:LoadValue("G4C_LEGEN1","BR_VERDE")
		 		EndIf
		 		
		 	EndIf
		EndIf		
	EndIf
Next
oModelG4C:GoLine(1)

oTotFat:LoadValue("XXX_TOTFAT",nTotFat)
oTotFat:LoadValue("XXX_TFATASS",nTotFatAss)
oTotFin:LoadValue("XXX_TOTFIN",nTotFin)
oTotFin:LoadValue("XXX_TFINASS",nTotFinAss)

cCodAssoc	:= T061AAssoc()  

RestArea( aArea )

Return()

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T061AAssoc

Codigo de Associa��o

@sample 	T061AAssoc()
@return  	aDados 
@author  	Simone Mie Sato Kakinoana
@since   	27/02/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function T061AAssoc()

Local cCodigo	:= "000000"
Local oModel    := FwModelActive()

Local cCartao	:= G90->G90_CODIGO
Local cFatura	:= G90->G90_NUMFAT

Local cAliasTMP	:= "TMP"

cAliasTMP		:= GetNextAlias()

BeginSql Alias cAliasTMP

	SELECT ISNULL(MAX(G91_ASSOCI),'') CODIGO
			FROM %table:G91% G91
			WHERE G91.G91_FILIAL = %xFilial:G91%
			AND G91.G91_CODIGO = %Exp:cCartao%
			AND G91.G91_NUMFAT = %Exp:cFatura%			
			AND G91.%NotDel%  
	UNION
	SELECT ISNULL(MAX(G4C_ASSOCI),'') CODIGO			
			FROM %table:G4C% G4C
			WHERE G4C.G4C_FILIAL = %xFilial:G4C%
			AND G4C.G4C_CARTUR = %Exp:cCartao%
			AND G4C.G4C_FATCAR = %Exp:cFatura%			
			AND G4C.%NotDel%  
EndSql


DbSelectArea(cAliasTMP)
DbGotop()
While !Eof()
	If (cAliasTMP)-> CODIGO > cCodigo
		cCodigo	:= (cAliasTMP)-> CODIGO
	EndIf
	DbSkip()
End
  
cCodigo	:= Soma1(cCodigo)

Return(cCodigo)


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T061Associ

Rotina para efetuar a associa��o. 

@sample 	T061Associ()
@return  	  
@author  	Simone Mie Sato Kakinoana
@since   	01/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function T061Associ()

Local aSaveArea	:= GetArea()

Local lOk			:= .F.
Local lContinua	:= .F.
Local lMsgYesNo	:= .F.
Local lTemAssoc	:= .F.
Local lMarkG91		:= .F.
Local lMarkG4C		:= .F.
Local lGravou		:= .F.

Local oModel		:= Nil
Local oModelG91	:= Nil
Local oModelG4C	:= Nil
Local oView		:= NIl

Local nLinAntG91	:= 0
Local nLinAntG4C	:= 0
Local nX			:= 0 

Local aCodAssAnt	:= {}
Local aAssocia		:= {}
Local aLinG91		:= {}
Local aLinG4C		:= {}

If T061AChkEfet(.T.)
	RestARea(aSaveArea)
	Return
EndIF

oModel				:= FwModelActive()
oModelG91			:= oModel:GetModel("G91DETAIL")
oModelG4C			:= oModel:GetModel("G4CDETAIL")

oView             := FWViewActive()

nLinAntG91			:= oModelG91:GetLine()
nLinAntG4C			:= oModelG4C:GetLine()

//Deixa todas as linhas sem estarem pintadas
oModelG4C:SetNoDeleteLine(.F.)
oModelG91:SetNoDeleteLine(.F.)

//Verifica se ja existe c�digo de associa��o para os itens marcados. Pergunta ao usu�rio se deseja gravar o codigo da nova associa��o
For nX := 1 To oModelG91:Length()

	oModelG91:GoLine(nX)	
	
	lOk := oModelG91:GetValue("G91_OK")
	oModelG91:UnDeleteLine()
	
	If lOk
		//Se estiver com classifica��o adicional, desmarca item. 
		If !Empty(oModelG91:GetValue("G91_CLASSI"))			
			oModelG91:SetValue("G91_OK",.F.)
			Loop
		EndIf
		
		lMarkG91	:= .T.
		If !Empty(oModelG91:GetValue("G91_ASSOCI"))
			lTemAssoc	:= .T.			
			If !lMsgYesNo
				lMsgYesNO	:= .T.
				If MsgYesNo(STR0023) //"Os itens ser�o reassociados. Confirma reassocia��o? "
					lContinua	:= .T.
				Else
					lContinua	:= .F.
				EndIf
			EndIF	
		Endif
	Endif 

Next nX
 
//Se no G91 n�o tem nenhum item j� associado, verifica se no G4C existe algum item j� associado.
For nX := 1 To oModelG4C:Length()
	
	oModelG4C:GoLine(nX)
	lOk := oModelG4C:GetValue("G4C_OK")
	oModelG4C:UnDeleteLine()	
	If lOk
		lMarkG4C	:= .T.
		If !lTemAssoc	.And. !Empty(oModelG4C:GetValue("G4C_ASSOCI"))
			lTemAssoc	:= .T.
			If !lMsgYesNo
				lMsgYesNO	:= .T.
				If MsgYesNo(STR0023) //"Os itens ser�o reassociados. Confirma reassocia��o? "
					lContinua	:= .T.
				Else
					lContinua	:= .F.
				EndIf
			EndIf	
		Endif
	Endif 
	
Next nX  

//Verifica se foi marcado dos dois lados.
If !lMarkG91 .Or. !lMarkG4C
	oModelG91:GoLine(nLinAntG91)
	oModelG4C:GoLine(nLinAntG4C) 
	Return
Endif

If ( lTemAssoc .And. lContinua) .Or. !lTemAssoc
	//Adiciona no array para atualizar a legenda e o valor total associado
	AADD(aAssocia,{cCodAssoc,{0,0},{0,0}}) 
	
	For nX := 1 To oModelG91:Length()
		oModelG91:GoLine(nX)
		lOk := oModelG91:GetValue("G91_OK")
	
		If lOk		
			If !Empty(oModelG91:GetValue("G91_ASSOCI"))
				If ASCAN(aCodAssAnt,oModelG91:GetValue("G91_ASSOCI")) == 0
					AADD(aCodAssAnt,oModelG91:GetValue("G91_ASSOCI"))
				EndIf		
			EndIf
			lGravou	:= .T.			 
			oModelG91:SetValue("G91_ASSOCI",cCodAssoc)			
			oModelG91:SetValue("G91_OK",.F.)
			
			AADD(aLinG91,{nX,cCodAssoc,oModelG91:GetValue("G91_ITEM"),oModelG91:GetValue("G91_DTEMIS")})
			
			If oModelG91:GetValue("G91_TIPO") == '1' 
				aAssocia[1][2][1]	+= oModelG91:GetValue("G91_VALOR")
			Else 
				aAssocia[1][2][2]	+= oModelG91:GetValue("G91_VALOR")		
			EndIf
			oModelG91:DeleteLine()
		Endif 
	Next nX
	
	For nX := 1 To oModelG4C:Length()
		oModelG4C:GoLine(nX)
		lOk := oModelG4C:GetValue("G4C_OK")
	
		If lOk
			If !Empty(oModelG4C:GetValue("G4C_ASSOCI"))
				If ASCAN(aCodAssAnt,oModelG4C:GetValue("G4C_ASSOCI")) == 0
					AADD(aCodAssAnt,oModelG4C:GetValue("G4C_ASSOCI"))
				EndIf		
			EndIf
			lGravou	:= .T.
			oModelG4C:SetValue("G4C_ASSOCI",cCodAssoc)			
			oModelG4C:SetValue("G4C_OK",.F.)
			AADD(aLinG4C,{nX,cCodAssoc,,oModelG4C:GetValue("G4C_EMISS")})
			
			If oModelG4C:GetValue("G4C_PAGREC") == '1' 
				aAssocia[1][3][1]	+= oModelG4C:GetValue("G4C_VALOR")
			Else 
				aAssocia[1][3][2]	+= oModelG4C:GetValue("G4C_VALOR")		
			EndIf
						
			OModelG4C:DeleteLine()		
		Endif 
	Next nX
EndIf

//Atualiza as legendas e o total do valor associado

FwMsgRun(,{||T061AAtual(oModel,aAssocia,aLinG91,aLinG4C,.T.) },,STR0018)//"Processando"

If lGravou
	cCodAssoc:= Soma1(cCodAssoc)
Endif

//Apos a atualiza��o dos codigos de associa��o, verifica se ficou algum c�digo "perdido", sem ter associa��o na outra grid.
FwMsgRun(,{||T061ADelCod(oModel,aCodAssAnt) },,STR0018)//"Processando"

oModelG91:GoLine(nLinAntG91)
oModelG4C:GoLine(nLinAntG4C)

oModelG4C:SetNoDeleteLine(.T.)
oModelG91:SetNoDeleteLine(.T.)


oView:Refresh()

RestArea(aSaveArea)
Return

//------------------------------------------------------------------------
/*/{Protheus.doc} T061AGRAVA()

Deixa todos os itens sem deletar. 
Verifica se tem algum item financeiro com c�digo de cart�o e fatura sem relacionamento.Se tiver, limpa esses campos. 

@sample 	T061AGRAVA() 
@return  	  
@author  	Simone Mie Sato Kakinoana
@since   	02/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T061AGRAVA( oModel )

Local lRet			:= .T.

Local oModelG4C 	:= Nil
Local oModelG91 	:= Nil
Local oModelG4CTab := Nil 
Local oModelG4CAux := Nil	 	

Local nLinAntG4C	:= 0
Local nLinAntG91	:= 0
Local nX			:= 0
	
Local aCodAssAnt := {} 

If G90->G90_STATUS == "2"//Se for efetivada
	Return(.T.)
EndIf

oModelG4C			:= oModel:GetModel("G4CDETAIL")
oModelG91			:= oModel:GetModel("G91DETAIL")


nLinAntG4C			:= oModelG4C:GetLine()
nLinAntG91			:= oModelG91:GetLine()

oModelG4C:SetNoDeleteLine(.F.)
OModelG91:SetNoDeleteLine(.F.)

For nX := 1 To oModelG91:Length()
	oModelG91:GoLine(nX)
	oModelG91:UnDeleteLine()
	If !Empty(oModelG91:GetValue("G91_ASSOCI"))
		If ASCAN(aCodAssAnt,oModelG91:GetValue("G91_ASSOCI")) == 0
			AADD(aCodAssAnt,oModelG91:GetValue("G91_ASSOCI"))
		EndIf		
	EndIf
Next
 
For nX := 1 To oModelG4C:Length()
	oModelG4C:GoLine(nX)	
	oModelG4C:UnDeleteLine()
	
	G4C->(DbSetOrder(2))
	If G4C->(DbSeek(xFilial("G4C")+oModelG4C:GetValue("G4C_IDIF")))
	
		oModelG4CTab 		:= FwLoadModel("TURA061X") 	
		oModelG4CTab:SetOperation( MODEL_OPERATION_UPDATE )
		oModelG4CAux:= oModelG4CTab:GetModel("G4C_MASTER")
			
		If oModelG4CTab:Activate()
			oModelG4CAux:SetValue("G4C_ASSOCI",oModelG4C:GetValue("G4C_ASSOCI"))
			oModelG4CAux:SetValue("G4C_FATCAR",oModelG4C:GetValue("G4C_FATCAR"))
			If oModelG4CTab:VldData()
				FwFormCommit(oModelG4CTab)
			Else
				JurShowErro( oModelG4CTab:GetModel():GetErrorMessage())
				lRet := .F.
			EndIf
			oModelG4CTab:DeActivate()
			oModelG4CTab:Destroy()
		EndIf
	Endif
	
	If !Empty(oModelG4C:GetValue("G4C_ASSOCI"))
		If ASCAN(aCodAssAnt,oModelG4C:GetValue("G4C_ASSOCI")) == 0
			AADD(aCodAssAnt,oModelG4C:GetValue("G4C_ASSOCI"))
		EndIf		
	EndIf
Next nX


//Apaga os codigos de associa��o qua n�o tem item correspondente
T061ADelCod(@oModel,aCodAssAnt)	

oModelG4C:SetNoDeleteLine(.T.)
OModelG91:SetNoDeleteLine(.T.)

oModelG4C:GoLine(nLinAntG4C)
oModelG91:GoLine(nLinAntG91)

FWFormCommit( oModel )

//Apaga codigo do cart�o e n�mero de fatura do G4C, caso n�o esteja com c�digo de associa��o preenchido. 
T061ADelCart()

Return(lRet)

//------------------------------------------------------------------------
/*/{Protheus.doc} T061AVisRV

Visualiza��o do RV => Posicionado no item financeiro 

@sample 	T061AVisRV
@return  	  
@author  	Simone Mie Sato Kakinoana
@since   	02/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T061AVisRV( )

Local lRet	:= .T.

Local aSaveG3P	:= G3P->(GetArea())

Local oModel	:= Nil
Local oModelG4C	:= Nil

oModel				:= FwModelActive()
oModelG4C			:= oModel:GetModel("G4CDETAIL")

 
 
G3P->(DbSetOrder(1))	//G3P_FILIAL+G3P_NUMID+G3P_SEGNEG                                                                                                                                 
If G3P->(DbSeek(xFilial("G3P")+oModelG4C:GetValue("G4C_NUMID")+oModelG4C:GetValue("G4C_SEGNEG")))
	T034SetStc({ {"lHierarquia",.F.} , {"nSegmento",Val( G3P->G3P_SEGNEG )} }) 
	FWExecView(STR0008,"VIEWDEF.TURA034",MODEL_OPERATION_VIEW,,{|| .T.},,,,,)	//Visualizar RV
Endif

RestArea(aSaveG3P)

Return(lRet)

//------------------------------------------------------------------------
/*/{Protheus.doc} MostraAssocia

Mostra a associa��o 

@sample 	MostraAssocia
@return  	  
@author  	Simone Mie Sato Kakinoana
@since   	02/03/2016
@version  	12.1.8/*/
//------------------------------------------------------------------------------------------
Function MostraAssocia (oForm,cFieldName,nLineGrid,nLineModel, cAssoci)

Local oModel            := Nil
Local oView             := Nil
Local oModelG4C         := Nil
Local oModelG91         := Nil
Local oTotFat			   := Nil
Local oTotFin			   := Nil

Local nX                 := 0
Local nLinAntG4C			:= 0
Local nLinAntG91			:= 0
Local nTotFatAss			:= 0 
Local nTotFinAss			:= 0

oModel            := FWModelActive()
oView             := FWViewActive()
oModelG4C         := oModel:GetModel("G4CDETAIL")
oModelG91         := oModel:GetModel("G91DETAIL")
oTotFat				:= oModel:GetModel("TOTFAT")
oTotFin				:= oModel:GetModel("TOTFIN")


nLinAntG4C			:= oModelG4C:GetLine()
nLinAntG91			:= oModelG91:GetLine()

If ( oModel:GetOperation() == MODEL_OPERATION_UPDATE ) 

	//Trata o Model da grid
	oModelG4C:SetNoDeleteLine(.F.)
	oModelG91:SetNoDeleteLine(.F.)

	If cFieldName ==  "G4C_OK" .Or. cFieldName ==  "G91_OK"
		//Tira o delete de todas as linhas
		For nX := 1 To oModelG4C:Length() 
	   	   oModelG4C:GoLine(nX) 
	   	   oModelG4C:UnDeleteLine()
		Next
		If cFieldName ==  "G4C_OK" 
			oModelG4C:GoLine(nLineModel)
			oModelG4C:SetNoDeleteLine(.T.)
		Endif	
		
		For nX := 1 To oModelG91:Length() 
	   	   OModelG91:GoLine(nX) 
	       OModelG91:UnDeleteLine()
		Next
		If cFieldName ==  "G91_OK" 
			OModelG91:GoLine(nLineModel)
			OModelG91:SetNoDeleteLine(.T.)
		Endif
			
		oTotFat:LoadValue("XXX_TFATASS",0)
		oTotFin:LoadValue("XXX_TFINASS",0)
		
	EndIf
	
	If  cFieldName <>  "G4C_OK" .And. cFieldName <>  "G91_OK"
	
		For nX := 1 To oModelG4C:Length() 
				
	   	   oModelG4C:GoLine(nX) 
		   	   
	   	   If cAssoci == oModelG4C:GetValue("G4C_ASSOCI") .And. !Empty(oModelG4C:GetValue("G4C_ASSOCI"))
		   	   		
	   	         oModelG4C:DeleteLine()
			        
	   	         If oModelG4C:GetValue("G4C_PAGREC") == "1"
	   	         	nTotFinASS += oModelG4C:GetValue("G4C_VALOR")
	   	         ElseIf oModelG4C:GetValue("G4C_PAGREC") == "2"
	   	         	nTotFinASS -= oModelG4C:GetValue("G4C_VALOR")
	   	         EndIf
				
	   	   Else
	   	         oModelG4C:UnDeleteLine()
	   	   EndIf
		Next
		
		oTotFin:LoadValue("XXX_TFINASS",nTotFinAss)
		
		
		For nX := 1 To oModelG91:Length() 
	   	   OModelG91:GoLine(nX) 
	   	   If cAssoci == oModelG91:GetValue("G91_ASSOCI")  .And. !Empty(oModelG91:GetValue("G91_ASSOCI"))
	   	         oModelG91:DeleteLine()
				  If oModelG91:GetValue("G91_TIPO") == "1"
	   	         	nTotFatASS += oModelG91:GetValue("G91_VALOR")
	   	         ElseIf oModelG91:GetValue("G91_TIPO") == "2"
	   	         	nTotFatASS -= oModelG91:GetValue("G91_VALOR")
	   	         EndIf   	         
	   	   Else
	   	       oModelG91:UnDeleteLine()
	   	   EndIf
		Next
		
		oTotFat:LoadValue("XXX_TFATASS",nTotFatAss)
		
	EndIf
		
	oModelG91:GoLine(nLinAntG91)
	oModelG4C:GoLine(nLinAntG4C)
	
	oModelG4C:SetNoDeleteLine(.T.)
	oModelG91:SetNoDeleteLine(.T.)
	
	oView:Refresh()

EndIf

Return .T.


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T061ADesAs 

Rotina para efetuar a desassocia��o 

@sample 	T061ADesAs()
@return  	  
@author  	Simone Mie Sato Kakinoana
@since   	02/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function T061ADesAs()

Local aSaveArea	:= GetArea()

Local oModel		:= Nil
Local oModelG91	:= Nil
Local oModelG4C	:= Nil

Local lOk			:= .F.
Local cCodAssoci	:= ""

Local aCodAssAnt	:= {}
Local aAssocia		:= {}
Local aLinG91		:= {}
Local aLinG4C		:= {}

Local nX			:= 0 
Local nY			:= 0 

If T061AChkEfet(.T.)
	RestARea(aSaveArea)
	Return
EndIF

oModel				:= FwModelActive()
oModelG91			:= oModel:GetModel("G91DETAIL")
oModelG4C			:= oModel:GetModel("G4CDETAIL")

nLinAntG91			:= oModelG91:GetLine()
nLinAntG4C			:= oModelG4C:GetLine()

oModelG4C:SetNoDeleteLine(.F.)
oModelG91:SetNoDeleteLine(.F.)

AADD(aAssocia,{cCodAssoc,{0,0},{0,0}})

//Verifica os itens que est�o sendo marcados para desassociar no G91.
For nX := 1 To oModelG91:Length()
	
	If ( !Empty(oModelG91:GetValue("G91_ASSOCI",nX)) .Or. !Empty(oModelG91:GetValue("G91_CLASSI",nX)) )
	
		oModelG91:GoLine(nX)	
		
		lOk := oModelG91:GetValue("G91_OK")
		
		oModelG91:UnDeleteLine()	
		
		If ( lOk )
		
			cCodAssoci	:= oModelG91:GetValue("G91_ASSOCI")
			
			If ASCAN(aCodAssAnt,oModelG91:GetValue("G91_ASSOCI")) == 0
				AADD(aCodAssAnt,oModelG91:GetValue("G91_ASSOCI"))
			EndIf
		
			oModelG91:SetValue("G91_ASSOCI","")	
			
			oModelG91:SetValue("G91_OK",.F.)
			
			If ( oModelG91:GetValue("G91_TIPO") == "1" )
				oModelG91:LoadValue("G91_LEGEN1","BR_VERMELHO")
			Else
				oModelG91:LoadValue("G91_LEGEN1","BR_PRETO")
			EndIf
				
			oModelG91:SetValue("G91_DIVERG","")
			
		EndIf
	
	EndIf
	
Next nX

For nX := 1 To oModelG4C:Length()
	
	If ( !Empty(oModelG4C:GetValue("G4C_ASSOCI",nX)) ) 
	
		oModelG4C:GoLine(nX)
		oModelG4C:UnDeleteLine()	
		
		lOk := oModelG4C:GetValue("G4C_OK")
		
		If ( lOk )
		
			cCodAssoci	:= oModelG4C:GetValue("G4C_ASSOCI")
			
			If ASCAN(aCodAssAnt,oModelG4C:GetValue("G4C_ASSOCI")) == 0
				AADD(aCodAssAnt,oModelG4C:GetValue("G4C_ASSOCI"))
			EndIf
			
			oModelG4C:SetValue("G4C_ASSOCI","")	
			
			If ( oModelG4C:GetValue("G4C_PAGREC") == "1" )
				oModelG4C:LoadValue("G4C_LEGEN1","BR_VERMELHO")
			Else
				oModelG4C:LoadValue("G4C_LEGEN1","BR_PRETO")
			EndIf
					
			oModelG4C:SetValue("G4C_OK",.F.)
					
		EndIf
	
	EndIf
	
Next nX

For nX := 1 To oModelG91:Length() 

	oModelG91:GoLine(nX)
	
	If ASCAN(aCodAssAnt,oModelG91:GetValue("G91_ASSOCI")) > 0 

		AADD(aLinG91,{nX,oModelG91:GetValue("G91_ASSOCI"),oModelG91:GetValue("G91_ITEM"),oModelG91:GetValue("G91_DTEMIS")})	

		If oModelG91:GetValue("G91_TIPO") == '1' 
			aAssocia[1][2][1]	+= oModelG91:GetValue("G91_VALOR")
		Else 
			aAssocia[1][2][2]	+= oModelG91:GetValue("G91_VALOR")		
		EndIf
		
	Endif
	
Next

For nX := 1 To oModelG4C:Length()
	
	oModelG4C:GoLine(nX)
	
	If ASCAN(aCodAssAnt,oModelG4C:GetValue("G4C_ASSOCI")) > 0
	
		AADD(aLinG4C,{nX,oModelG4C:GetValue("G4C_ASSOCI"),,oModelG4C:GetValue("G4C_EMISS")})
		 
		If oModelG4C:GetValue("G4C_PAGREC") == '1' 
			aAssocia[1][3][1]	+= oModelG4C:GetValue("G4C_VALOR")
		Else 
			aAssocia[1][3][2]	+= oModelG4C:GetValue("G4C_VALOR")		
		EndIf

	Endif
	
Next

//Atualiza as legendas e o total do valor associado
FwMsgRun(,{||T061AAtual(oModel,aAssocia,aLinG91,aLinG4C,.F.) },,STR0018)//"Processando"

//Apos a atualiza��o dos codigos de associa��o, verifica se ficou algum c�digo "perdido", sem ter associa��o na outra grid.
T061ADelCod(oModel,aCodAssAnt)

oModelG91:GoLine(nLinAntG91)
oModelG4C:GoLine(nLinAntG4C)

oModelG4C:SetNoDeleteLine(.T.)
oModelG91:SetNoDeleteLine(.T.)

RestArea(aSaveArea)

Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T061ADelCod  

Rotina para apagar o c�digo de associa��o que n�o est�o sendo mais utilizados. 

@sample 	T061ADelCod()                 
@return  	  
@author  	Simone Mie Sato Kakinoana
@since   	02/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T061ADelCod(oModel,aCodAssAnt)

Local aSaveArea	:= GetArea()

Local oModelG91	:= Nil
Local oModelG4C	:= Nil

Local nLinAntG91	:= 0
Local nLinAntG4C	:= 0
Local nX			:= 0
Local nY			:= 0 
Local nZ			:= 0 

Local lG4C			:= .F.
Local lG91		 	:= .F.

oModelG91			:= oModel:GetModel("G91DETAIL")
oModelG4C			:= oModel:GetModel("G4CDETAIL")

nLinAntG91			:= oModelG91:GetLine()
nLinAntG4C			:= oModelG4C:GetLine()

For nX := 1 to Len(aCodAssAnt)


	If oModelG91:SeekLine({{"G91_ASSOCI",aCodAssAnt[nX]}})
		lG91	:= .T.
	EndIf 
	
	If oModelG4C:SeekLine({{"G4C_ASSOCI",aCodAssAnt[nX]}})
		lG4C	:= .T.
	EndIf 
	
	//Se tiver em apenas uma das grids
	If ( lG91 .And. !lG4C ) .Or. ( !lG91 .And. lG4C )  

		If lG91
			For nY := 1 To oModelG91:Length()
				oModelG91:GoLine(nY)
				If aCodAssAnt[nX] == oModelG91:GetValue("G91_ASSOCI")	
					oModelG91:SetValue("G91_ASSOCI","")
					oModelG91:LoadValue("G91_LEGEN1","BR_VERMELHO")
					oModelG91:SetValue("G91_DIVERG","")
				EndIf
			Next
		EndIf
		
		If lG4C	
			For nZ := 1 To oModelG4C:Length()	
				oModelG4C:GoLine(nZ)
				If aCodAssAnt[nX] == oModelG4C:GetValue("G4C_ASSOCI")	
					oModelG4C:SetValue("G4C_ASSOCI","") 
					oModelG4C:LoadValue("G4C_LEGEN1","BR_VERMELHO")					
				EndIf	
			Next
		EndIf
	EndIf
	lG91	:= .F.
	lG4C	:= .F.	
Next

oModelG91:GoLine(nLinAntG91)
oModelG4C:GoLine(nLinAntG4C)

RestArea(aSaveArea)

Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T061ADelCart

Rotina para apagar  o n�mero da fatura da G4C se estiver com o c�digo
de associa��o em branco.

@sample 	T061ADelCart()
@return  	  
@author  	Simone Mie Sato Kakinoana
@since   	04/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T061ADelCart()

Local cCodigo		:= G90->G90_CODIGO
Local cNumFat		:= G90->G90_NUMFAT

Local cQuery		:= ""
	
	cQuery := "UPDATE "+ RetSQLName( "G4C" ) + " " 
	cQuery += "SET G4C_FATCAR ='' "
	cQuery += "WHERE "
	cQuery += " G4C_FILIAL ='"+xFilial("G4C")+"' "
	cQuery += " AND G4C_CARTUR = '"+cCodigo+"' "
	cQuery	+= " AND G4C_FATCAR = '"+cNumFat+"' " 
	cQuery	+= " AND G4C_ASSOCI = '' "
	cQuery += " AND D_E_L_E_T_ = ' ' "	
							
	TCSQLExec( cQuery )

Return(.T.)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T61ATotal 
Rotina para c�lculo do toalizador.            
@sample 	T61ATotal()
@return  	  
@author  	Simone Mie Sato Kakinoana
@since   	07/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T61ATotal(oModel,cGrid)

Local aSaveArea	:= GetArea()

Local oGridG91 	:= oModel:GetModel("G91DETAIL")
Local oGridG4C 	:= oModel:GetModel("G4CDETAIL")

Local nRet			:= 0
Local nX			:= 0
Local nLinAnt		:= 0

If cGrid == "G91"
	oGridG91:GetLine()
	For nX := 1 To oGridG91:Length()
		oGridG91:Goline(nX)
		nRet += oGridG91:Getvalue("G91_VALOR")
	Next nX
	oGridG91:Goline(nLinAnt)
Else
	oGridG4C:GetLine()
	For nX := 1 To oGridG4C:Length()
		oGridG4C:Goline(nX)
		nRet += oGridG4C:Getvalue("G4C_VALOR")
	Next nX
	oGridG4C:Goline(nLinAnt)
EndIf

RestArea(aSaveArea)

Return(nRet)


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T61AStrTot 
Adiciona campos na estrutura do model do totalizador            
@sample 	T61AStrTot()
@return  	  
@author  	Simone Mie Sato Kakinoana
@since   	07/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T61AStrTot(oTotFat,oTotFin,nOpc)


If nOpc == 1

		oTotFat:AddField( ;
							STR0019 				, ; // [01] C Titulo do campo		//"Total da Fatura"
							STR0019 				, ; // [02] C ToolTip do campo//"Total da Fatura"
							"XXX_TOTFAT" 	, ; // [03] C identificador (ID) do Field
							"N" 			, ; // [04] C Tipo do campo
							16 				, ; // [05] N Tamanho do campo
							2 				, ; // [06] N Decimal do campo
							NIL				, ; // [07] B Code-block de valida��o do campo
							NIL 			, ; // [08] B Code-block de valida��o When do campoz
							NIL 			, ; // [09] A Lista de valores permitido do campo
							NIL 			, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
							NIL 			, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
							NIL 			, ; // [12] L Indica se trata de um campo chave
							.T. 			, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. 			) // [14] L Indica se o campo � virtual

		oTotFat:AddField( ; // Ord. Tipo Desc.	
								STR0020			, ; // [01] C Titulo do campo	//"Total Associado"
								STR0020			, ; // [02] C ToolTip do campo	//"Total Associado"
								"XXX_TFATASS"	, ; // [03] C identificador (ID) do Field
								"N"			, ; // [04] C Tipo do campo
								16			, ; // [05] N Tamanho do campo
								2			, ; // [06] N Decimal do campo
								NIL			, ; // [07] B Code-block de valida��o do campo
								NIL 		, ; // [08] B Code-block de valida��o When do campoz
								NIL 		, ; // [09] A Lista de valores permitido do campo
								NIL 		, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
								NIL			, ; // [11] B Code-block de inicializacao do campo
								NIL 		, ; // [12] L Indica se trata de um campo chave
								.T. 		, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
								.T. 		) // [14] L Indica se o campo � virtual
								
		oTotFin:AddField( ; // Ord. Tipo Desc.
								STR0021			, ; // [01] C Titulo do campo	//"Total dos Itens Financeiros"	
								STR0021 			, ; // [02] C ToolTip do campo//"Total dos Itens Financeiros"
								"XXX_TOTFIN"	, ; // [03] C identificador (ID) do Field
								"N"			, ; // [04] C Tipo do campo
								16			, ; // [05] N Tamanho do campo
								2			, ; // [06] N Decimal do campo
								NIL			, ; // [07] B Code-block de valida��o do campo
								NIL 		, ; // [08] B Code-block de valida��o When do campoz
								NIL 		, ; // [09] A Lista de valores permitido do campo
								NIL 		, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
								NIL			, ; // [11] B Code-block de inicializacao do campo
								NIL 		, ; // [12] L Indica se trata de um campo chave
								.T. 		, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
								.T. 		) // [14] L Indica se o campo � virtual
								
			oTotFin:AddField( ; // Ord. Tipo Desc.
								STR0021			, ; // [01] C Titulo do campo			//"Total dos Itens Financeiros"	
								STR0021 			, ; // [02] C ToolTip do campo		//"Total dos Itens Financeiros"	
								"XXX_TFINASS"	, ; // [03] C identificador (ID) do Field
								"N"			, ; // [04] C Tipo do campo
								16			, ; // [05] N Tamanho do campo
								2			, ; // [06] N Decimal do campo
								NIL			, ; // [07] B Code-block de valida��o do campo
								NIL 		, ; // [08] B Code-block de valida��o When do campoz
								NIL 		, ; // [09] A Lista de valores permitido do campo
								NIL 		, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
								NIL			, ; // [11] B Code-block de inicializacao do campo
								NIL 		, ; // [12] L Indica se trata de um campo chave
								.T. 		, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
								.T. 		) // [14] L Indica se o campo � virtual

	Else
	
				oTotFat:AddField(	"XXX_TOTFAT",;				// [01] C Nome do Campo
								"01",;							// [02] C Ordem
								STR0019,; 						// [03] C Titulo do campo // "Total da Fatura"
								STR0019,; 						// [04] C Descri��o do campo //"Total da Fatura"
								 ,;								// [05] A Array com Help //"Numero do Docto. de Saida."
								"",; 							// [06] C Tipo do campo - GET, COMBO OU CHECK
								"@E 999,999,999.99",;			// [07] C Picture
								NIL,; 							// [08] B Bloco de Picture Var
								"",; 							// [09] C Consulta F3
								.F.,; 							// [10] L Indica se o campo � edit�vel
								NIL, ; 							// [11] C Pasta do campo
								NIL,; 							// [12] C Agrupamento do campo
								{},; 							// [13] A Lista de valores permitido do campo (Combo)
								NIL,; 							// [14] N Tamanho Maximo da maior op��o do combo
								NIL,;	 						// [15] C Inicializador de Browse
								.T.) 							// [16] L Indica se o campo � virtual
			

				oTotFat:AddField(	"XXX_TFATASS",;				// [01] C Nome do Campo
								"02",;							// [02] C Ordem
								STR0020,; 						// [03] C Titulo do campo // "Total Associado"
								STR0020,; 						// [04] C Descri��o do campo //"Total Associado"
								 ,;								// [05] A Array com Help //"Numero do Docto. de Saida."
								"",; 							// [06] C Tipo do campo - GET, COMBO OU CHECK
								"@E 999,999,999.99",;			// [07] C Picture
								NIL,; 							// [08] B Bloco de Picture Var
								"",; 							// [09] C Consulta F3
								.F.,; 							// [10] L Indica se o campo � edit�vel
								NIL, ; 							// [11] C Pasta do campo
								NIL,; 							// [12] C Agrupamento do campo
								{},; 							// [13] A Lista de valores permitido do campo (Combo)
								NIL,; 							// [14] N Tamanho Maximo da maior op��o do combo
								NIL,;	 						// [15] C Inicializador de Browse
								.T.) 							// [16] L Indica se o campo � virtual
								

				oTotFin:AddField(	"XXX_TOTFIN",;				// [01] C Nome do Campo
								"01",;							// [02] C Ordem
								STR0021,; 						// [03] C Titulo do campo // "Total dos Itens Financeiros"
								STR0021,; 						// [04] C Descri��o do campo //"Total dos Itens Financeiros"
								 ,;								// [05] A Array com Help //"Numero do Docto. de Saida."
								"",; 							// [06] C Tipo do campo - GET, COMBO OU CHECK
								"@E 999,999,999.99",;			// [07] C Picture
								NIL,; 							// [08] B Bloco de Picture Var
								"",; 							// [09] C Consulta F3
								.F.,; 							// [10] L Indica se o campo � edit�vel
								NIL, ; 							// [11] C Pasta do campo
								NIL,; 							// [12] C Agrupamento do campo
								{},; 							// [13] A Lista de valores permitido do campo (Combo)
								NIL,; 							// [14] N Tamanho Maximo da maior op��o do combo
								NIL,;	 						// [15] C Inicializador de Browse
								.T.) 							// [16] L Indica se o campo � virtual

								
				oTotFin:AddField(	"XXX_TFINASS",;				// [01] C Nome do Campo
								"02",;							// [02] C Ordem
								STR0020,; 						// [03] C Titulo do campo // "Total Associado"
								STR0020,; 						// [04] C Descri��o do campo //"Total Associado"
								 ,;								// [05] A Array com Help //"Numero do Docto. de Saida."
								"",; 							// [06] C Tipo do campo - GET, COMBO OU CHECK
								"@E 999,999,999.99",;			// [07] C Picture
								NIL,; 							// [08] B Bloco de Picture Var
								"",; 							// [09] C Consulta F3
								.F.,; 							// [10] L Indica se o campo � edit�vel
								NIL, ; 							// [11] C Pasta do campo
								NIL,; 							// [12] C Agrupamento do campo
								{},; 							// [13] A Lista de valores permitido do campo (Combo)
								NIL,; 							// [14] N Tamanho Maximo da maior op��o do combo
								NIL,;	 						// [15] C Inicializador de Browse
								.T.) 							// [16] L Indica se o campo � virtual
								
								
	EndIf


Return

//------------------------------------------------------------------------------------------
/*{Protheus.doc} T061AAtual(aAssocia,aLinG91,aLinG4C)
 
Atualiza a legenda e o total asasociado
            
@sample 	T61AAtual()
@return  	  
@author  	Simone Mie Sato Kakinoana
@since   	10/03/2016
@version  	12.1.8
*/
//------------------------------------------------------------------------------------------
Function T061AAtual(oModel,aAssocia,aLinG91,aLinG4C,lAssocia)

Local oModelG91		:= oModel:GetModel("G91DETAIL")
Local oModelG4C		:= oModel:GetModel("G4CDETAIL")
Local oTotFat			:= oModel:GetModel("TOTFAT")
Local oTotFin			:= oModel:GetModel("TOTFIN")

Local nCont := 0 
Local nValDebG91	:= 0 
Local nValCrdG91	:= 0
Local nValDebG4C	:= 0 
Local nValCrdG4C	:= 0
Local nDiferenca	:= 0
Local nLinG91		:= 0 
Local nLinG4C		:= 0
Local nPosG4C		:= 0 
Local nTotAssFat	:= 0   
Local nTotAssFin	:= 0
Local nTolera		:= G90->G90_TOLERA

Local cLegenda		:= ""
Local cDiverg	:=	STR0024 //"Data de emiss�o da Fatura diferente da data de emiss�o do IF."

DEFAULT lAssocia	:= .T.

If Len(aAssocia) == 0 
	Return
EndIf

nValDebG91		:= aAssocia[1][2][1] 	 
nValCrdG91		:= aAssocia[1][2][2]
nValDebG4C 	:= aAssocia[1][3][1] 	 
nValCrdG4C		:= aAssocia[1][3][2]

 
If nValDebG91 == nValDebG4C .And. nValCrdG91 == nValCrdG4C
	cLegenda	:= "BR_VERDE"
Else
	If  nValDebG91 <> nValDebG4C .And. nValCrdG91 == nValCrdG4C	//Diferen�a s� a debito
		If ( nValDebG91 - nValDebG4C) > 0 
			If  nValDebG91 - nValDebG4C  <=  nTolera
				cLegenda	:= "BR_AMARELO"
			Else
				cLegenda	:= "BR_LARANJA"
			EndIf	
		ElseIf ( nValDebG91 - nValDebG4C) < 0				
			cLegenda	:= "BR_AZUL"
		EndIf 
 	ElseIf nValCrdG91 <> nValCrdG4C .And. nValDebG91 == nValDebG4C  	//Diferen�a s� a credito
 		If ( nValCrdG4C - nValCrdG91) > 0
			If ABS( nValCrdG4C - nValCrdG91)  <=  nTolera	 // Se a diferen�a for menor que a toler�ncia
			 	cLegenda	:= "BR_AMARELO"				 	
			ElseIf ABS( nValCrdG4C - nValCrdG91) >  nTolera  // Se a diferen�a for maior que a toler�ncia				 	   
 				cLegenda	:= "BR_LARANJA"		 			
 			EndIf 		
 		ElseIf ( nValCrdG4C - nValCrdG91) < 0				
			cLegenda	:= "BR_AZUL"
		EndIf  		
 	ElseIf  nValCrdG91 <> nValCrdG4C .And. nValDebG91 <> nValDebG4C  	//Se houver diferen�a no debito e no credito
 		nDiferenca	:= (( nValDebG91 - nValCrdG91) - (nValDebG4C - nValCrdG4C))
 			 
 		If nDiferenca <> 0	 
			If nDiferenca > 0 	//Valor a pagar
				If ABS(nDiferenca) <= nTolera
					cLegenda	:= "BR_AMARELO"
				Else
					cLegenda	:= "BR_LARANJA"						
				EndIf		
			ElseIf nDiferenca < 0		//Valor a receber  
				cLegenda	:= "BR_AZUL"
 			EndIf
 		Else	//Se nao teve diferenca	 				
 			cLegenda	:= "BR_VERDE"
 		EndIf	 		
 	EndIf
EndIf

//Atualiza legenda da G91
If Len(aLinG91) > 0 
	nLinG91		:= 	oModelG91:GetLine()	
	For nCont := 1 to Len(aLinG91)
		oModelG91:GoLine(aLinG91[nCont][1])
		
		nPosG4C		:= ASCAN(aLinG4C,{|x| x[2]== aLinG91[nCont][2]})

		If nPosG4C > 0 		
			If aLinG91[nCont][4] <> aLinG4C[nPosG4C][4]
				oModelG91:LoadValue("G91_DIVERG",cDiverg)	
			Else
				oModelG91:LoadValue("G91_DIVERG","")	
			Endif 
		EndIf
		If lAssocia
			If oModelG91:GetValue("G91_TIPO") == "1"
				nTotAssFat	+= oModelG91:GetValue("G91_VALOR")
			Else	
				nTotAssFat	-= oModelG91:GetValue("G91_VALOR")	
			EndIf
		EndIf
		oModelG91:LoadValue("G91_LEGEN1",cLegenda)
	Next
	oModelG91:GoLine(nLinG91)
Endif

//Atualiza legenda da G4C
If Len(aLinG4C) > 0 
	nLinG4C		:= 	oModelG4C:GetLine()	
	For nCont := 1 to Len(aLinG4C)
		oModelG4C:GoLine(aLinG4C[nCont][1])
		oModelG4C:LoadValue("G4C_LEGEN1",cLegenda)
		If lAssocia
			If oModelG4C:GetValue("G4C_PAGREC") == "1"
				nTotAssFin	+= oModelG4C:GetValue("G4C_VALOR")
			ElseIf oModelG4C:GetValue("G4C_PAGREC") == "2"	
				nTotAssFin	-= oModelG4C:GetValue("G4C_VALOR")	
			EndIf
		Endif
	Next
	oMOdelG4C:GoLine(nLinG4C)
EndIf

oTotFat:LoadValue("XXX_TFATASS",nTotAssFat)
oTotFin:LoadValue("XXX_TFINASS",nTotAssFin)

Return

//------------------------------------------------------------------------------------------
/*{Protheus.doc} T061Tudok(oModel)
 
Deixa todos os registros sem estarem deletados.
            
@sample 	T061Tudok()
@return  	  
@author  	Simone Mie Sato Kakinoana
@since   	29/03/2016
@version  	12.1.8
*/
//------------------------------------------------------------------------------------------
Function T061TudOk(oModel)

Local oModelG91		:= Nil
Local oModelG4C		:= Nil

Local nX				:= 0 

oModelG91		:= oModel:GetModel("G91DETAIL")
oModelG4C		:= oModel:GetModel("G4CDETAIL")

oModelG4C:SetNoDeleteLine(.F.)
oModelG91:SetNoDeleteLine(.F.)

For nX := 1 To oModelG91:Length()
	oModelG91:GoLine(nX)
	oModelG91:UnDeleteLine()
Next

For nX := 1 To oModelG4C:Length()
	oModelG4C:GoLine(nX)
	oModelG4C:UnDeleteLine()
Next

oModelG4C:SetNoDeleteLine(.T.)
oModelG91:SetNoDeleteLine(.T.)

Return(.T.)


/*{Protheus.doc} T061ALeg
Legenda.

@author Simone Mie Sato Kakinoana
@since 30/03/2016
@version 1.0
*/
Function T061ALeg()

BrwLegenda(STR0026,STR0015,{;                               	//"Legenda"###"Concilia��o"###
	                        { "BR_VERDE" 		, STR0027 },; 	//'Associa��o com valor id�ntico'
	                        { "BR_AMARELO" 		, STR0028 },; 	//'Associa��o com d�bito, dentro da toler�ncia'
	                        { "BR_LARANJA" 		, STR0029 },; 	//'Associa��o com d�bito, maior que a toler�ncia'
	                        { "BR_AZUL" 		, STR0030 },; 	//'Associa��o com cr�dito'
	                        { "BR_VERMELHO" 	, STR0031 },; 	//'Item de d�bito n�o associado'
	                        { "BR_PINK" 		, STR0032 },; 	//'Item adicional da opera��o'
                            { "BR_PRETO"    	, STR0033 },; 	//'Item de cr�dito n�o associado'
                            { "BR_BRANCO"		, STR0036}})	//"Item CONTESTADO" 

Return Nil


