#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TURA061B.CH"

STATIC oModConc		:= NIL
STATIC oViewConc	:= NIL

/*/{Protheus.doc} TURA061B
Tela para configura��o do Script para ajuste da Chave. 
@type Function
@author Simone Mie Sato Kakinoana
@since 22/02/2016
@version 12.1.7
/*/
Function TURA061B(oModAnt,oViewAnt)

Local oGrid := Nil

oModConc 	:= oModAnt
oViewConc	:= oViewAnt

oGrid	:= oModConc:GetModel('G91DETAIL')

If oGrid:SeekLine({{'G91_OK', .T. }})
	FWExecView(STR0002,'TURA061B', MODEL_OPERATION_INSERT,, { || .T. }) //"Script para ajuste de Chave"
Else
	Help(" ",1,"TURA061B",,STR0023,3,1) // "'Nenhum item da fatura foi selecionado!'"
EndIf
	
Return

/*/{Protheus.doc} TURA061B
Modelo de dados
@type Static Function
@author Simone Mie Sato Kakinoana
@since 22/02/2016
@version 12.1.7
/*/
Static Function ModelDef()

Local oModelAtv		:= FwModelActive()
Local oModel 			:= Nil
Local oStruField		:= T061BModField()
Local oStruGrid		:= T061BModGrid()

oModel 		:= MPFormModel():New('TURA061B',/*bPre*/,/*{||F761ValDoc()}*/,{ |oModel| T061BGrv( oModel )}/*bCommit*/,/*bCancel*/)

oModel:Addfields("FKMASTER",/*cOwner*/,oStruField)
oModel:AddGrid("TMPDETAIL","FKMASTER" ,oStruGrid, {|oMdl,nLn,cAct,cFld| TA61VldEdit(oMdl,nLn,cAct,cFld)} , { |oModel| COMP061BLPOS(oModel) })

oModel:SetDescription(STR0001) //"Execu��o de Script"
oModel:GetModel("FKMASTER"):SetDescription(STR0001)	//"Execu��o de Script"
oModel:GetModel("TMPDETAIL"):SetDescription(STR0001)//"Execu��o de Script"

oModel:SetActivate({|oModel| T61BLoad(oModel) })

oModel:SetPrimaryKey({})

Return oModel

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef

@author Simone Mie Sato Kakinoana
@since 22/02/2016
@version 12.1.7
/*/
//-------------------------------------------------------------------
Static Function ViewDef()

Local oModel 	:= FWLoadModel('TURA061B')
Local oView		:= FWFormView():New()'
Local oViewField:= T061BVField()
Local oViewGrid	:= T061BVGrid()

oView:SetModel(oModel)

oView:AddGrid( 'VIEW_TMP', oViewGrid, 'TMPDETAIL')

oView:CreateHorizontalBox( 'BOXFORM1', 100)

oView:SetOwnerView('VIEW_TMP','BOXFORM1')

oView:AddIncrementField( 'VIEW_TMP', 'SEQ' )

Return oView
//-------------------------------------------------------------------
/*/{Protheus.doc} T061BModField	

Estrutura de dados.

@author Simone Mie Sato Kakinoana
@since 24/02/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function T061BModField()

Local oStruModel := FWFormModelStruct():New()

Local lObrigat	 := .F.

Local cTipoCpo	 := ""

Local aComboValues := {}
Local aCampos	:= {}
Local bWhen		:= ""
Local bValid	:= ""

Local nCont		:= 0 

AADD(aCampos, {"Texto","Texto","C",10,"G",,.F.,"TEXTO"})

For nCont := 1 to Len(aCampos)
	cTipoCpo	:= aCampos[nCont][3]
	bValid 		:= MontaBlock("{|| AllwaysTrue() } ")
	bWhen 		:= MontaBlock("{|| AllwaysTrue() } ")
	lObrigat	:= aCampos[nCont][7]
			
	If aCampos[nCont][5] == "C"
		aComboValues := aCampos[nCont][6]
	Else
		aComboValues := {}		
	EndIf
	
	oStruModel:AddField(			  ;
		aCampos[nCont][1] 	, ;	// [01] Titulo do campo		
		aCampos[nCont][2]	, ;	// [02] ToolTip do campo	
		aCampos[nCont][8]	, ;	// [03] Id do Field
		cTipoCpo			, ;	// [04] Tipo do campo
		aCampos[nCont][4]	, ;	// [05] Tamanho do campo
		0					, ;	// [06] Decimais do Campo
		{ || .T. }			, ;	// [07] Code-block de valida��o do campo
		bWhen				, ;	// [08] Code-block de valida��o do campo
		aComboValues		, ;	// [09] aComboValues (Op��es)
		lObrigat			)	// [10] Indica se o campo tem preenchimento obrigat�rio
	
Next

Return oStruModel


//-------------------------------------------------------------------
/*/{Protheus.doc} T061BModGrid	

Estrutura de dados.

@author Simone Mie Sato Kakinoana
@since 22/02/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function T061BModGrid()

Local oStruModel := FWFormModelStruct():New()

Local lObrigat	 := .F.

Local cTipoCpo	 := ""

Local aComboValues := {}
Local aCampos	:= {}
Local bWhen		:= ""
Local bValid	:= ""

Local nCont		:= 0 

AADD(aCampos, {STR0003,STR0004,"C",3,"G",,.F.,"SEQ"})	//"Seq"##"Sequ�ncia"
AADD(aCampos, {STR0005,STR0005,"C",1,"C",{STR0006,STR0007,STR0008,STR0009,STR0010,STR0011},.T.,"TIPO"})	//"Tipo"##'1=Elim.Caract.Esp.'##'2=Cortar'##3=Concatenar'##4=Mai�scula'##5=Min�scula'##6=Substituir'
AADD(aCampos, {STR0012,STR0012,"N",3,"G",,.F.,"INICORTE"})	//"Ini Corte"
AADD(aCampos, {STR0013,STR0013,"N",3,"G",,.F.,"QTDCORTE"})	//"Qtd. Corte"
AADD(aCampos, {STR0014,STR0014,"C",1,"C",{STR0018,STR0019},.F.,"TPCONCAT"})//"Tipo Concat."##'1=Texto'##'2=Campo'
AADD(aCampos, {STR0015,STR0015,"C",30,"G",,.F.,"CONCAT"})//"Concat."
AADD(aCampos, {STR0016,STR0016,"C",30,"G",,.F.,"SUBSDE"})	//"Substiutir de"
AADD(aCampos, {STR0017,STR0017,"C",30,"G",,.F.,"SUBSPARA"})	//"Substiutir para"

For nCont := 1 to Len(aCampos)
	cTipoCpo	:= aCampos[nCont][3]
		
	lObrigat	:= aCampos[nCont][7]
	
	
	If aCampos[nCont][8] $ "SEQ"
		bWhen 		:= MontaBlock("{|| AllwaysFalse() } ")	
	Else
		bWhen 		:= MontaBlock("{|| AllwaysTrue() } ")		
	EndIf	
	
	If aCampos[nCont][5] == "C"
		aComboValues := aCampos[nCont][6]
	Else
		aComboValues := {}		
	EndIf
	
	bValid 		:= MontaBlock("{|| AllwaysTrue() } ")
	
	oStruModel:AddField(			  ;
		aCampos[nCont][1] 	, ;	// [01] Titulo do campo		
		aCampos[nCont][2]	, ;	// [02] ToolTip do campo	
		aCampos[nCont][8]	, ;	// [03] Id do Field
		cTipoCpo			, ;	// [04] Tipo do campo
		aCampos[nCont][4]	, ;	// [05] Tamanho do campo
		0					, ;	// [06] Decimais do Campo
		{ || .T. }			, ;	// [07] Code-block de valida��o do campo
		bWhen				, ;	// [08] Code-block de valida��o do campo
		aComboValues		, ;	// [09] aComboValues (Op��es)
		lObrigat			)	// [10] Indica se o campo tem preenchimento obrigat�rio
	
Next

Return oStruModel


//------------------------------------------------------------------- 
/*/{Protheus.doc} T061BVField		

Estrutura de interface.

@author Simone Mie Sato Kakinoana
@since 24/02/2016
@version 1.0
/*/
//-------------------------------------------------------------------
//Monta estrutura da View
Static Function T061BVField()

Local oStruView		:= FWFormViewStruct():New()
Local cPicture		:= ""
Local cTipoCpo		:= ""

Local aComboValues 	:= {}
Local aCampos		:= {}

Local nCont			:= 0 

AADD(aCampos, {"TEXTO","TEXTO","C","G",,"TEXTO","01"})

For nCont := 1 to Len(aCampos)
	cTipoCpo := aCampos[nCont][3]
	If aCampos[nCont][4] == "C"
		aComboValues := aCampos[nCont][5]
	Else
		aComboValues := {}
	EndIf	
	
	oStruView:AddField(				  ;
		aCampos[nCont][6]			, ;		// [01] Id do Field
		aCampos[nCont][7]			, ;		// [02] Ordem
		aCampos[nCont][1]			, ;		// [03] Titulo do campo		//"Filial do Sistema"
		aCampos[nCont][2]			, ;		// [04] ToolTip do campo	//"Filial do Sistema"
									, ;		// [05] Help
		cTipoCpo					, ;		// [06] Tipo do campo
									, ;		// [07] Picture
									, ;		// [08] PictVar
									, ;		// [09] F3
		.T.							, ;		// [10] Cmpo Evit�vel ?
									, ;		// [11] Folder
									, ;		// [12] Grupo
		aComboValues				, ;		// [13] aComboValues (Op��es)
									, ;		// [14] nMaxLenCombo
		          	   				  )		// [15] cIniBrow

Next

Return oStruView
//------------------------------------------------------------------- 
/*/{Protheus.doc} T061BVGrid		

Estrutura de interface.

@author Simone Mie Sato Kakinoana
@since 22/02/2016
@version 1.0
/*/
//-------------------------------------------------------------------
//Monta estrutura da View
Static Function T061BVGrid()

Local oStruView		:= FWFormViewStruct():New()
Local cPicture		:= ""
Local cTipoCpo		:= ""

Local aComboValues 	:= {}
Local aCampos		:= {}

Local nCont			:= 0 

AADD(aCampos, {STR0003,STR0004,"C","G",,"SEQ","01","@!"})	//"Seq"##"Sequ�ncia"
AADD(aCampos, {STR0005,STR0005,"C","C",{STR0006,STR0007,STR0008,STR0009,STR0010,STR0011},"TIPO","02","@!"})//"Tipo"##'1=Elim.Caract.Esp.'##'2=Cortar'##3=Concatenar'##4=Mai�scula'##5=Min�scula'##6=Substituir'
AADD(aCampos, {STR0012,STR0012,"N","G",,"INICORTE","03","@e 999"})//"Ini Corte"
AADD(aCampos, {STR0013,STR0013,"N","G",,"QTDCORTE","04","@e 999"})//"Qtd. Corte"
AADD(aCampos, {STR0014,STR0014,"C","C",{'1=Texto','2=Campo'},"TPCONCAT","05","@!"})//"Tipo Concat."##'1=Texto'##'2=Campo'
AADD(aCampos, {STR0015,STR0015,"C","G",,"CONCAT","06",,""})//"Concat."
AADD(aCampos, {STR0016,STR0016,"C","G",,"SUBSDE","07",""})//"Substiutir de"
AADD(aCampos, {STR0017,STR0017,"C","G",,"SUBSPARA","08",""})//"Substiutir para"

For nCont := 1 to Len(aCampos)

	cTipoCpo := aCampos[nCont][3]
	If aCampos[nCont][4] == "C"	//Se for combo
		aComboValues := aCampos[nCont][5]
	Else
		aComboValues := {}
	EndIf	

	oStruView:AddField(				  ;
		aCampos[nCont][6]			, ;		// [01] Id do Field
		aCampos[nCont][7]			, ;		// [02] Ordem
		aCampos[nCont][1]			, ;		// [03] Titulo do campo		//"Filial do Sistema"
		aCampos[nCont][2]			, ;		// [04] ToolTip do campo	//"Filial do Sistema"
									, ;		// [05] Help
		cTipoCpo					, ;		// [06] Tipo do campo
		aCampos[nCont][8]			, ;		// [07] Picture
									, ;		// [08] PictVar
									, ;		// [09] F3
		.T.							, ;		// [10] Cmpo Evit�vel ?
									, ;		// [11] Folder
									, ;		// [12] Grupo
		aComboValues				, ;		// [13] aComboValues (Op��es)
									, ;		// [14] nMaxLenCombo
		          	   				  )		// [15] cIniBrow

Next

Return oStruView

//------------------------------------------------------------------- 
/*/{Protheus.doc} COMP061BLPOS(oModel) 		

Valida��o da linha

@author Simone Mie Sato Kakinoana
@since 24/02/2016
@version 1.0
/*/
//-------------------------------------------------------------------
//Monta estrutura da View
Static Function COMP061BLPOS(oModel)

Local lRet	:= .T.

Local cTipo		:= oModel:GetValue("TIPO")
Local nIniCorte	:= oModel:GetValue("INICORTE")  
Local nQtdCorte	:= oModel:GetValue("QTDCORTE")
Local cTpConcat	:= oModel:GetValue("TPCONCAT")
Local cConcat		:= oModel:GetValue("CONCAT")
Local cSubsDe		:= oModel:GetValue("SUBSDE")
Local cSubsPara	:= oModel:GetValue("SUBSPARA")

If cTipo == "2"
	If nIniCorte == 0 .Or. nQtdCorte == 0 
		lRet	:= .F.
		Help(" ",1,"T061BTIPO2",,STR0020,3,1) // "O Ini Corte e a Qtd. Corte devem ser preenchidos."
	EndIf
ElseIf cTipo == "3"
	If Empty(cTpConcat) .Or. Empty(cConcat)
		lRet	:= .F.
		Help(" ",1,"T061BTIPO3",,STR0021,3,1) // "O Tipo Concat.e Concat. devem ser preenchidos."
	EndIf
ElseIf cTipo == "6"
	If AllTrim(cSubsDe) == '""' .Or. Empty(AllTrim(cSubsDe))
		lRet	:= .F.
		Help(" ",1,"T061BTIPO6",,STR0022,3,1) // "O campo Substituir De deve ser preenchido." 
	EndIf
EndIf

Return(lRet)

//-------------------------------------------------------------------
/*/{Protheus.doc} T061BGrv(oModel)

Grava��o dos dados

@author Simone Mie Sato Kakinoana
@since 24/02/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Function T061BGrv(oModel)

Local lRet	:= .T.
Local lOk	:= .F.

Local cCodAnt	:= ""

Local oGridG91	:= oModConc:GetModel('G91DETAIL')

Local nX	:= 0
Local nLinAtu	:= oGridG91:GetLine()

For nX := 1 To oGridG91:Length()
	oGridG91:GoLine(nX)
	lOk := oGridG91:GetValue("G91_OK")
	If lOk
		cCodAnt	:= oGridG91:GetValue("G91_DOCFOR")
		cCodigo	:= T61BExeScript(cCodAnt)
		oGridG91:SetValue("G91_CHAVE",cCodigo)
		
		If !oModConc:VldData()		
			Help(" ",1,"TURA61BEXECSCRIPT",,oGridG91:GetModel():GetErrorMessage()[6],1,0)
			lRet := .F.
			Exit					
		EndIf					
	Endif 
Next nX

If lRet
	FWFormCommit( oModConc )
	oViewConc:Refresh()
EndIf

oGridG91:GoLine(nLinAtu)

Return(lRet)


//-------------------------------------------------------------------
/*/{Protheus.doc} T61BExeScript(cCodAnt)

Executa Script

@author Simone Mie Sato Kakinoana
@since 24/02/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Function T61BExeScript(cCodAnt)


Local oModel		:= FWModelActive()
Local oGrid		:= oModel:GetModel('TMPDETAIL')	 
Local cCharEsp	:= "��������������.&/-_!@#$%�*(){}[]?/;:><|\�,~^�`+="
Local cRet			:= ""
Local cMacro		:= ""
Local cAux			:= cCodAnt
Local nX			:= 0
Local nCont		:= 0 
Local nTam			:= 0
Local nIniCorte	:= 0 	 
Local nQtdCorte	:= 0 
Local nLinAtu		:= oGrid:GetLine()
Local lBcoDe 		:= .F.
Local lBcoPara 	:= .F.
Local lErro 		:= .F.

Local oError 		:= Nil 

For nX := 1 To oGrid:Length()
	oGrid:GoLine(nX)
	If oGrid:GetValue("TIPO") == "1"//Tira caracteres especiais
		For nCont := 1 to Len(Alltrim(cCodAnt))
			If Subs(cAux,nCont,1) $ cCharEsp
				cRet	:= STRTRAN(cAux,Subs(cAux,nCont,1),"")
				cAux	:= cRet
			EndIf
		Next
	EndIf 
	
	If oGrid:GetValue("TIPO") == "2"	//Cortar
		nTam		:= Len(Alltrim(cAux))
		nIniCorte	:= oGrid:GetValue("INICORTE")
		nQtdCorte	:= oGrid:GetValue("QTDCORTE")
		cRet		:= Subs(cAux,1,nIniCorte-1)
		cRet		+= Subs(cAux,nIniCorte+nQtdCorte,nTam-nQtdCorte-(nIniCorte-1))
		cAux		:= cRet		
	EndIf
		
	If oGrid:GetValue("TIPO") == "3"	//Concatenar
		If oGrid:GetValue("TPCONCAT")	== "1"	//Texto
			cRet	:=	Alltrim(cAux)+ Alltrim(oGrid:GetValue("CONCAT"))
			cAux	:= cRet 
		Else	 	
			
			cMacro := oGrid:GetValue("CONCAT")
			
			//-- Executa Macro Execu��o			
			oError     := ErrorBlock({|e| Aviso(STR0001,STR0024 + cMacro + CRLF + CRLF + ;//"Execu��o de Script."#"Erro na formula do campo Concat. -> "
								e:DESCRIPTION    ,{"Ok"} ),  lErro := .T. })
			
			Begin Sequence
				cMacro := &(cMacro)	
			End Sequence
			
			ErrorBlock( oError,lErro ) // Restaura rotina de erro anterior
													
			cMacro := IIF( !lErro .And. ValType(cMacro) == "C" ,cMacro,"") 		
			
			If !Empty(cMacro)	
				cRet	:=	Alltrim(cAux) + Alltrim(cMacro)			
			Else
				cRet	:= cAux
			EndIf
		
		EndIf
	EndIf
		
	If oGrid:GetValue("TIPO") == "4"	//Mai�scula 
		cRet	:=	UPPER(cAux)
		cAux	:= cRet 		
	EndIf
	
		
	If oGrid:GetValue("TIPO") == "5"	//Min�scula 
		cRet	:=	LOWER(cAux)
		cAux	:= cRet 
	EndIf
	
	
	If oGrid:GetValue("TIPO") == "6"	//Substituir 
		// Se as duas extremidades forem "" e o conte�do entre as "" for Branco
		If 	Left(AllTrim(oGrid:GetValue("SUBSDE")),1) == '"' .And.;
			Right(AllTrim(oGrid:GetValue("SUBSDE")),1) == '"' .And.;
			AllTrim(SubStr(AllTrim(oGrid:GetValue("SUBSDE")),2,RAt(AllTrim(oGrid:GetValue("SUBSDE")),'"')-1)) = "" .And.;
			Len(AllTrim(oGrid:GetValue("SUBSDE"))) > 2
				lBcoDe := .T.
		EndIf 
		
		If	Left(AllTrim(oGrid:GetValue("SUBSPARA")),1) == '"' .And.;
			Right(AllTrim(oGrid:GetValue("SUBSPARA")),1) == '"' .And.;
			(AllTrim(SubStr(AllTrim(oGrid:GetValue("SUBSPARA")),2,RAt(AllTrim(oGrid:GetValue("SUBSPARA")),'"')-1)) = "" .Or.;
			Len(AllTrim(oGrid:GetValue("SUBSDE"))) >= 2)
				lBcoPara := .T.
		EndIf
		
		If lBcoDe .And. lBcoPara
			cRet	:= STRTRAN(AllTrim(cAux),&(ALLTRIM(oGrid:GetValue("SUBSDE"))),&(ALLTRIM(oGrid:GetValue("SUBSPARA"))))
		ElseIf lBcoDe
			cRet	:= STRTRAN(AllTrim(cAux),&(ALLTRIM(oGrid:GetValue("SUBSDE"))),ALLTRIM(oGrid:GetValue("SUBSPARA")))
		ElseIf lBcoPara
			cRet	:= STRTRAN(AllTrim(cAux),ALLTRIM(oGrid:GetValue("SUBSDE")),&(ALLTRIM(oGrid:GetValue("SUBSPARA"))))
		Else
			cRet	:= STRTRAN(cAux,ALLTRIM(oGrid:GetValue("SUBSDE")),ALLTRIM(oGrid:GetValue("SUBSPARA")))
		EndIf
		cAux	:= cRet		
	EndIf 
	
Next nX

oGrid:GoLine(nLinAtu)


Return(cRet)


//-------------------------------------------------------------------
/*/{Protheus.doc} T61BLoad(oModel)

Carrega os dados

@author Simone Mie Sato Kakinoana
@since 25/02/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Function T61BLoad(oModel)

Local oFake	:= Nil

oFake := oModel:GetModel('FKMASTER')
oFake:SetValue("TEXTO","Teste")

Return(.T.)

//-------------------------------------------------------------------
/*/{Protheus.doc} TA61VldEdit()

Valida a edi��o dos campos

@author Fernando Radu Muscalu
@since 25/11/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function TA61VldEdit(oMdl,nLn,cAct,cFld)

Local lRet := .T.

If ( cAct == "CANSETVALUE" )
	
	If ( cFld <> "TIPO" )
		
		Do Case
		//Tipos = Eliminar Caractere Especial/Mai�sculo/Min�sculo, deve travar todos os campos, exceto o campo Tipo (ele mesmo)
		Case ( oMdl:GetValue("TIPO") $ "1|4|5" .And. !(cFld $ "TIPO") ) 
			lRet := .f.
		//Tipo = Cortar, travar os campos Tipo Concat, Concat, Substituir de e Substituir para.	
		Case ( oMdl:GetValue("TIPO") == "2" .And. !(cFld $ "TIPO|INICORTE|QTDCORTE") )
			lRet := .f.
		//Tipo = Concatenar, deixar somente edit�veis os campos Tipo Concat e Concat.
		Case ( oMdl:GetValue("TIPO") == "3" .And. !(cFld $ "TIPO|TPCONCAT|CONCAT") )
			lRet := .f.	
		//Tipo = Substituir, deixa somente edit�veis os campos Substituir de e Substituir para
		Case ( oMdl:GetValue("TIPO") == "6" .And. !(cFld $ "TIPO|SUBSDE|SUBSPARA") )
			lRet := .f.
		End Case
		
	Endif

EndIf

Return(lRet)
