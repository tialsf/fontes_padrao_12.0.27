#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TURA061C.CH"

STATIC oModConc		:= NIL
STATIC oViewConc	:= NIL

/*/{Protheus.doc} TURA061C
Tela do Resumo 
@type Function
@author Simone Mie Sato Kakinoana
@since 25/02/2016
@version 12.1.7
/*/
Function TURA061C(oModAnt,oViewAnt)

Local aEnableButtons 	:= {}
//Est� sendo habilitado  bot�o "Confirmar", com a descri��o "Fechar" para n�o exibir mensagem do cancelamento. 
aEnableButtons 	:= {{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.T.,STR0015},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil}} //"Fechar"

oModConc 	:= oModAnt
oViewConc	:= oViewAnt

FWExecView(STR0001,'TURA061C', MODEL_OPERATION_INSERT,, { || .T. },,,aEnableButtons) //"Resumo da Concilia��o"

Return

/*/{Protheus.doc} TURA061C
Modelo de dados
@type Static Function
@author Simone Mie Sato Kakinoana
@since 25/02/2016
@version 12.1.7
/*/
Static Function ModelDef()

Local oModelAtv		:= FwModelActive()
Local oModel 		:= Nil
Local oStruField	:= T061CModField()
Local oStruGrid		:= T061CModGrid()

oModel 		:= MPFormModel():New('TURA061C',/*bPre*/,/*{||F761ValDoc()}*/,{||.T.},/*bCancel*/)

oModel:Addfields("FKMASTER",/*cOwner*/,oStruField)
oModel:AddGrid("TMPDETAIL","FKMASTER" ,oStruGrid,)

oModel:SetDescription(STR0004) //"Resumo"
oModel:GetModel("FKMASTER"):SetDescription(STR0001)	//"Resumo da Concilia��o"
oModel:GetModel("TMPDETAIL"):SetDescription(STR0005)//"Concilia��o"

oModel:SetActivate({|oModel| T61CLoad(oModel) })

oModel:SetPrimaryKey({})

oModel:GetModel("FKMASTER"):SetOnlyQuery(.T.)
oModel:GetModel("TMPDETAIL"):SetOnlyQuery(.T.)

oModel:GetModel("FKMASTER"):SetOnlyView(.T.)
oModel:GetModel("TMPDETAIL"):SetOnlyView(.T.)

Return oModel

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef

@author Simone Mie Sato Kakinoana
@since 25/02/2016
@version 12.1.7
/*/
//-------------------------------------------------------------------
Static Function ViewDef()

Local oModel 	:= FWLoadModel('TURA061C')
Local oView		:= FWFormView():New()'
Local oViewField:= T061CVField()
Local oViewGrid	:= T061CVGrid()

oView:SetModel(oModel)

oView:AddGrid( 'VIEW_TMP', oViewGrid, 'TMPDETAIL')

oView:CreateHorizontalBox( 'BOXFORM1', 100)

oView:SetOwnerView('VIEW_TMP','BOXFORM1')

oViewGrid:SetProperty( '*' , MVC_VIEW_CANCHANGE,.F.)

oView:ShowInsertMsg(.F.)

Return oView
//-------------------------------------------------------------------
/*/{Protheus.doc} T061CModField	

Estrutura de dados.

@author Simone Mie Sato Kakinoana
@since 25/02/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function T061CModField()

Local oStruModel := FWFormModelStruct():New()

Local lObrigat	 := .F.

Local cTipoCpo	 := ""

Local aComboValues := {}
Local aCampos	:= {}
Local bWhen		:= ""
Local bValid	:= ""

Local nCont		:= 0 

AADD(aCampos, {"Texto","Texto","C",10,"G",,.F.,"TEXTO"})

For nCont := 1 to Len(aCampos)
	cTipoCpo	:= aCampos[nCont][3]
	bValid 		:= MontaBlock("{|| AllwaysTrue() } ")
	bWhen 		:= MontaBlock("{|| AllwaysTrue() } ")
	lObrigat	:= aCampos[nCont][7]
			
	If aCampos[nCont][5] == "C"
		aComboValues := aCampos[nCont][6]
	Else
		aComboValues := {}		
	EndIf
	
	oStruModel:AddField(			  ;
		aCampos[nCont][1] 	, ;	// [01] Titulo do campo		
		aCampos[nCont][2]	, ;	// [02] ToolTip do campo	
		aCampos[nCont][8]	, ;	// [03] Id do Field
		cTipoCpo			, ;	// [04] Tipo do campo
		aCampos[nCont][4]	, ;	// [05] Tamanho do campo
		0					, ;	// [06] Decimais do Campo
		{ || .T. }			, ;	// [07] Code-block de valida��o do campo
		bWhen				, ;	// [08] Code-block de valida��o do campo
		aComboValues		, ;	// [09] aComboValues (Op��es)
		lObrigat			)	// [10] Indica se o campo tem preenchimento obrigat�rio
	
Next

Return oStruModel


//-------------------------------------------------------------------
/*/{Protheus.doc} T061CModGrid	

Estrutura de dados.

@author Simone Mie Sato Kakinoana
@since 2542/02/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function T061CModGrid()

Local oStruModel := FWFormModelStruct():New()

Local lObrigat	 := .F.

Local cTipoCpo	 := ""

Local aComboValues := {}
Local aCampos	:= {}
Local bWhen		:= ""
Local bValid	:= ""

Local nCont		:= 0 

AADD(aCampos, {STR0003,STR0003,"C",150,"G",,.T.,"DESCRI"})	//"DESCRI��O" 
AADD(aCampos, {STR0002,STR0002,"N",16,"G",,.F.,"VALOR"})	//"VALOR"

For nCont := 1 to Len(aCampos)
	cTipoCpo	:= aCampos[nCont][3]
		
	lObrigat	:= aCampos[nCont][7]
	
	
	If aCampos[nCont][8] $ "SEQ"
		bWhen 		:= MontaBlock("{|| AllwaysFalse() } ")	
	Else
		bWhen 		:= MontaBlock("{|| AllwaysTrue() } ")		
	EndIf	
	
	If aCampos[nCont][5] == "C"
		aComboValues := aCampos[nCont][6]
	Else
		aComboValues := {}		
	EndIf
	
	bValid 		:= MontaBlock("{|| AllwaysTrue() } ")
	
	oStruModel:AddField(			  ;
		aCampos[nCont][1] 	, ;	// [01] Titulo do campo		
		aCampos[nCont][2]	, ;	// [02] ToolTip do campo	
		aCampos[nCont][8]	, ;	// [03] Id do Field
		cTipoCpo			, ;	// [04] Tipo do campo
		aCampos[nCont][4]	, ;	// [05] Tamanho do campo
		2					, ;	// [06] Decimais do Campo
		{ || .T. }			, ;	// [07] Code-block de valida��o do campo
		bWhen				, ;	// [08] Code-block de valida��o do campo
		aComboValues		, ;	// [09] aComboValues (Op��es)
		lObrigat			)	// [10] Indica se o campo tem preenchimento obrigat�rio
	
Next

Return oStruModel


//------------------------------------------------------------------- 
/*/{Protheus.doc} T061CVField		

Estrutura de interface.

@author Simone Mie Sato Kakinoana
@since 25/02/2016
@version 1.0
/*/
//-------------------------------------------------------------------
//Monta estrutura da View
Static Function T061CVField()

Local oStruView		:= FWFormViewStruct():New()
Local cPicture		:= ""
Local cTipoCpo		:= ""

Local aComboValues 	:= {}
Local aCampos		:= {}

Local nCont			:= 0 

AADD(aCampos, {"TEXTO","TEXTO","C","G",,"TEXTO","01"})

For nCont := 1 to Len(aCampos)
	cTipoCpo := aCampos[nCont][3]
	If aCampos[nCont][4] == "C"
		aComboValues := aCampos[nCont][5]
	Else
		aComboValues := {}
	EndIf	

	oStruView:AddField(				  ;
		aCampos[nCont][6]			, ;		// [01] Id do Field
		aCampos[nCont][7]			, ;		// [02] Ordem
		aCampos[nCont][1]			, ;		// [03] Titulo do campo		//"Filial do Sistema"
		aCampos[nCont][2]			, ;		// [04] ToolTip do campo	//"Filial do Sistema"
									, ;		// [05] Help
		cTipoCpo					, ;		// [06] Tipo do campo
		cPicture					, ;		// [07] Picture
									, ;		// [08] PictVar
									, ;		// [09] F3
		.T.							, ;		// [10] Cmpo Evit�vel ?
									, ;		// [11] Folder
									, ;		// [12] Grupo
		aComboValues				, ;		// [13] aComboValues (Op��es)
									, ;		// [14] nMaxLenCombo
		          	   				  )		// [15] cIniBrow

Next

Return oStruView
//------------------------------------------------------------------- 
/*/{Protheus.doc} T061CVGrid		

Estrutura de interface.

@author Simone Mie Sato Kakinoana
@since 25/02/2016
@version 1.0
/*/
//-------------------------------------------------------------------
//Monta estrutura da View
Static Function T061CVGrid()

Local oStruView		:= FWFormViewStruct():New()
Local cPicture		:= ""
Local cTipoCpo		:= ""

Local aComboValues 	:= {}
Local aCampos		:= {}

Local nCont			:= 0 

AADD(aCampos, {STR0003,STR0003,"C","G",,"DESCRI","01",})	//"DESCRI��O"
AADD(aCampos, {STR0002,STR0002,"N","G",,"VALOR","02",})		//"VALOR"	

For nCont := 1 to Len(aCampos)

	cTipoCpo := aCampos[nCont][3]
	If aCampos[nCont][4] == "C"	//Se for combo
		aComboValues := aCampos[nCont][5]
	Else
		aComboValues := {}
	EndIf	


	If aCampos[nCont][3] == "C"	//Se for caracter
		cPicture	:= "@!"
	ElseIf aCampos[nCont][3] == "N"	//Se for num�rico
		If aCampos[nCont][6] == "VALOR"
			cPicture	:= "@E 9,999,999,999,999.99"	
		Endif
		
	EndIf
	
	oStruView:AddField(				  ;
		aCampos[nCont][6]			, ;		// [01] Id do Field
		aCampos[nCont][7]			, ;		// [02] Ordem
		aCampos[nCont][1]			, ;		// [03] Titulo do campo		//"Filial do Sistema"
		aCampos[nCont][2]			, ;		// [04] ToolTip do campo	//"Filial do Sistema"
									, ;		// [05] Help
		cTipoCpo					, ;		// [06] Tipo do campo
		cPicture					, ;		// [07] Picture
									, ;		// [08] PictVar
									, ;		// [09] F3
		.T.							, ;		// [10] Cmpo Evit�vel ?
									, ;		// [11] Folder
									, ;		// [12] Grupo
		aComboValues				, ;		// [13] aComboValues (Op��es)
									, ;		// [14] nMaxLenCombo
		          	   				  )		// [15] cIniBrow

Next

Return oStruView


//-------------------------------------------------------------------
/*/{Protheus.doc} T61CLoad(oModel)

Carrega os dados

@author Simone Mie Sato Kakinoana
@since 25/02/2016
@version 1.0
/*/
//-------------------------------------------------------------------


Function T61CLoad(oModel)

Local aResumo		:= {}
Local aAdic		:= {}

Local oFieldG90	:= oModConc:GetModel('G90MASTER')
Local oModelG91	:= oModConc:GetModel('G91DETAIL')
Local oModelG4C	:= oModConc:GetModel('G4CDETAIL')

Local oFake		:= Nil
Local oGrid		:= Nil

Local cCodigo		:= oFieldG90:GetValue("G90_CODIGO")
Local cNumFat		:= oFieldG90:GetValue("G90_NUMFAT")

Local nLinAnt		:= 0 
Local nLinAntG91	:= 0
Local nLinAntG4C	:= 0
Local nX			:= 0
Local nY			:= 0 
Local nPos			:= 0   
Local nFatDeb		:= 0 
Local nFatCrd		:= 0 
Local nFinDeb		:= 0 
Local nFinCrd		:= 0 
Local nDebOk		:= 0 
Local nCredOk		:= 0
Local nNoDeb		:= 0 
Local nNoCred		:= 0
Local nVlrAdic		:= 0 
Local nValDebG91	:= 0 
Local nValCrdG91	:= 0
Local nValDebG4C	:= 0 
Local nValCrdG4C	:= 0
Local nCont			:= 0 
Local nTolera		:= G90->G90_TOLERA 
Local nValTol		:= 0
Local nDiverg		:= 0
Local nDiferenca	:= 0  
Local nCredExtra	:= 0 
Local nVlrCont		:= 0
Local nTotalFat		:= 0

Local aAuxG91		:= {}
Local aAuxG4C		:= {}

nLinAntG91			:= oModelG91:GetLine()
nLinAntG4C			:= oModelG4C:GetLine()

//Item da Fatura
For nX := 1 To oModelG91:Length()
	
	nSinal := Iif(oModelG91:GetValue("G91_TIPO") == "1" ,1,-1)
	
	oModelG91:GoLine(nX)
	
	If oModelG91:GetValue("G91_TIPO") == "1"		//Se for debito 
		
		nFatDeb		+= oModelG91:GetValue("G91_VALOR")
		
		//Se n�o estiver associado, atualiza a legenda	
		If Empty(oModelG91:GetValue("G91_ASSOCI"))
			
			If !Empty(oModelG91:GetValue("G91_CLASSI"))	//Se for classifica��o adicional
				
				If ( oModelG91:GetValue("G91_CLASSI") <> "99" )	//Sen�o for classifica��o de "Contestado"
					nVlrAdic	+=  oModelG91:GetValue("G91_VALOR")
				Else
					nVlrCont +=  oModelG91:GetValue("G91_VALOR") * nSinal
				Endif
					
			Else
				nNoDeb		+=  oModelG91:GetValue("G91_VALOR")				
			EndIf
			
		Else
			
			nPosG91	:= ASCAN(aAuxG91,{|x| x[1]== oModelG91:GetValue("G91_ASSOCI")}) 
			
			If  nPosG91 == 0				 
				AADD(aAuxG91,{oModelG91:GetValue("G91_ASSOCI"),oModelG91:GetValue("G91_VALOR"),0})
			Else			
				aAuxG91[nPosG91][2]	+= oModelG91:GetValue("G91_VALOR")
			EndIf
			
		EndIf
		
	Else 		//Se for credito
		
		nFatCrd		+= oModelG91:GetValue("G91_VALOR")
		
		//Se n�o estiver associado, atualiza a legenda	
		If Empty(oModelG91:GetValue("G91_ASSOCI"))
			
			If ( !Empty(oModelG91:GetValue("G91_CLASSI")) )	//Se for classifica��o adicional
				
				If ( oModelG91:GetValue("G91_CLASSI") <> "99" )
					nVlrAdic	-=  oModelG91:GetValue("G91_VALOR")
				Else
					nVlrCont	+=  oModelG91:GetValue("G91_VALOR") * nSinal
					nNoCred		+=  oModelG91:GetValue("G91_VALOR")				
				EndIf
					
			Else		
				nNoCred		+=  oModelG91:GetValue("G91_VALOR")
			EndIf
			
		Else
		 
			nPosG91	:= ASCAN(aAuxG91,{|x| x[1]== oModelG91:GetValue("G91_ASSOCI")}) 
		
			If  nPosG91 == 0				 
				AADD(aAuxG91,{oModelG91:GetValue("G91_ASSOCI"),0,oModelG91:GetValue("G91_VALOR")})
			Else			
				aAuxG91[nPosG91][3]	+= oModelG91:GetValue("G91_VALOR")
			EndIf
			
		EndIf
		
	Endif
					
Next

//Item Financeiro
For nX := 1 To oModelG4C:Length()
	
	oModelG4C:GoLine(nX)
	
	If oModelG4C:GetValue("G4C_PAGREC") == "1"		//Se for debito 
		
		nFinDeb		+= oModelG4C:GetValue("G4C_VALOR")
		
		nPosG4C	:= ASCAN(aAuxG4C,{|x| x[1]==oModelG4C:GetValue("G4C_ASSOCI")}) 
		
		If  nPosG4C == 0				 
			AADD(aAuxG4C,{oModelG4C:GetValue("G4C_ASSOCI"),oModelG4C:GetValue("G4C_VALOR"),0})
		Else			
			aAuxG4C[nPosG4C][2]	+= oModelG4C:GetValue("G4C_VALOR")
		EndIf
		
	ElseIf oModelG4C:GetValue("G4C_PAGREC") == "2" 		//Se for credito
		
		nFinCrd	+= oModelG4C:GetValue("G4C_VALOR")
		nPosG4C	:= ASCAN(aAuxG4C,{|x| x[1]==oModelG4C:GetValue("G4C_ASSOCI")})
		
		If nPosG4C== 0				 
			AADD(aAuxG4C,{oModelG4C:GetValue("G4C_ASSOCI"),0,oModelG4C:GetValue("G4C_VALOR")})				
		Else			
			aAuxG4C[nPosG4C][3]	+= oModelG4C:GetValue("G4C_VALOR")
		EndIf
		
	Endif
					
Next nX


For nCont := 1 to Len(aAuxG91)
	
	cCodAssoci		:= aAuxG91[nCont][1]
	nPosG4C			:= ASCAN(aAuxG4C,{|x| x[1]== cCodAssoci })
	
	nValDebG91		:= aAuxG91[nCont][2]
	nValCrdG91		:= aAuxG91[nCont][3]
	nValDebG4C		:= aAuxG4C[nPosG4C][2]
	nValCrdG4C		:= aAuxG4C[nPosG4C][3]
	
	If ( nValDebG91 == nValDebG4C ) .And. ( nValCrdG91 == nValCrdG4C )
	
		If nValDebG91 > 0   
			nDebOk	+=   nValDebG91
		Endif
		If nValCrdG91 > 0   
			nCredOk	+=   nValCrdG91
		EndIf	
	
	ElseIf nValDebG91 <> nValDebG4C .And. nValCrdG91 == nValCrdG4C	//Diferen�a s� a debito
	
		nCredOk += nValCrdG91	
		nDebOk	+= nValDebG4C
	
		If ( nValDebG91 - nValDebG4C) > 0
			
			If nValDebG91 - nValDebG4C  <=  nTolera   // Se a diferen�a for menor que a toler�ncia
				nValTol	 += nValDebG91 - nValDebG4C
			ElseIf nValDebG91 - nValDebG4C  >  nTolera	  // Se a diferen�a for maior que a toler�ncia
	 			nDiverg	 += nValDebG91 - nValDebG4C
 			EndIf	
 						
		ElseIf ( nValDebG91 - nValDebG4C) < 0	
			nCredExtra	+= nValDebG4C - nValDebG91
		EndIf
 	
 	ElseIf nValCrdG91 <> nValCrdG4C .And. nValDebG91 == nValDebG4C  	//Diferen�a s� a credito
 	
 		nDebOk	+= nValDebG4C
 		nCredOk += nValCrdG4C
 	
 		If ( nValCrdG4C - nValCrdG91) > 0 
 	
 			If ABS( nValCrdG4C - nValCrdG91)  <=  nTolera //// Se a diferen�a for menor que a toler�ncia		 
			 	nValTol	 +=  ABS( nValCrdG4C - nValCrdG91)		 	 				 	
			ElseIf ABS( nValCrdG91 - nValCrdG4C) >  nTolera  // Se a diferen�a for maior que a toler�ncia
 				nDiverg	 += ABS( nValCrdG91 - nValCrdG4C)
 			EndIf 
 	
 		Else
 			nCredExtra	+= nValCrdG91 - nValCrdG4C
 		EndIf
 		
 	ElseIf  ( nValCrdG91 <> nValCrdG4C .And. nValDebG91 <> nValDebG4C ) 	//Se houver diferen�a no debito e no credito
 		
 		nDiferenca	:= (( nValDebG91 - nValCrdG91) - (nValDebG4C - nValCrdG4C))

		nDebOk	+= nValDebG4C
		nCredOk += nValCrdG4C
			 		 
 		If nDiferenca <> 0	 
		
			If nDiferenca > 0 	//Associa��o com debito
			
				If ABS(nDiferenca) <= nTolera	//Menor que a tolerancia
					nValTol	 += nDiferenca
				Else			//Maior que a toler�ncia
					nDiverg	 += nDiferenca						
				EndIf
						
			ElseIf nDiferenca < 0		//Associa��o com credito
				nCredExtra	+= ABS(nDiferenca) 						
 			EndIf
 			
 		EndIf
 		
 	EndIf
 	
Next

nTotalFat := nFatDeb-nFatCrd

AADD(aResumo,{STR0006,nTotalFat})					//"Total da Fatura"
AADD(aResumo,{STR0007,nDebOk})						//"D�bito Conciliado"
AADD(aResumo,{STR0008,nCredOk})						//"Cr�dito Conciliado"
AADD(aResumo,{STR0009,nDiverg})						//"Diverg�ncia"
AADD(aResumo,{STR0010,nValTol})						//"Toler�ncia"
AADD(aResumo,{STR0011,nCredExtra+nNoCred})			//"Cr�ditos Extras"
AADD(aResumo,{STR0012,nVlrAdic})					//"Adicional"
AADD(aResumo,{STR0016,nVlrCont})					//"Contestados"
AADD(aResumo,{STR0013,nNoDeb})						//"D�bito n�o reconhecido"
AADD(aResumo,{STR0014,(((nDebOk + nValTol) - (nCredOk + nCredExtra + nNoCred)) + nVlrAdic) })//"L�quido a Pagar"
	
oFake := oModel:GetModel('FKMASTER')
oFake:SetValue("TEXTO","Teste")

oGrid:= oModel:GetModel('TMPDETAIL')

For nCont := 1 to Len(aResumo)
	oGrid:SetValue("DESCRI",aResumo[nCont][1])
	oGrid:SetValue("VALOR",aResumo[nCont][2])
	If nCont < Len(aResumo)
		oGrid:AddLine(.T.)
	Endif
Next


oModelG91:GoLine(nLinAntG91)
oModelG4C:GoLine(nLinAntG4C)

Return(.T.)
