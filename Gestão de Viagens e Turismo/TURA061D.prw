#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TURA061D.CH"


/*/{Protheus.doc} TURA061D
Rotina para atualizar os campos de n�mero de cart�o, fatura e c�digo de associa��o do G4C e G91. 
 
@type Function
@author Simone Mie Sato Kakinoana
@since 29/02/2016
@version 12.1.8
/*/
Function TURA061D()

Local aArea		:= GetArea()

If !SoftLock("G90")	
	RestArea(aArea)
	Return
Else
	If !CtbValiDt(,G90->G90_DTLANC,,,,{"TUR001"},)
		RestArea(aArea)
		Return
	Else         
		//Rotina para atualizar os campos de fatura, numero de cartao e associa��o
		If !T061AChkEfet(.F.)	//Se ainda n�o foi efetivada, efetua a concilia��o autom�tica
			If T061DReass()
				FwMsgRun(,{||T061DProc()   },,STR0018)	//"Processando..."
			EndIf
		EndIf
		
		FwMsgRun(,{||TURA061A()   },,STR0018)	//"Processando..."
	EndIf	
	G90->(MsUnlock())
	
EndIf

RestArea(aArea)

Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T061DProc

Atualiza os campos de fatura  associa��o. 

@sample 	T061DProc()
@return  	  
@author  	Simone Mie Sato Kakinoana
@since   	01/03/2016
@version  	12.1.8 
/*/
//------------------------------------------------------------------------------------------
Static Function T061DProc()

Local cAssoci		:= T061DAssoc() 
Local cAliasTMP	:= ""
Local cCodigo		:= G90->G90_CODIGO
Local cNumFat		:= G90->G90_NUMFAT
Local cCodForn		:= G90->G90_CODFOR
Local cLjForn		:= G90->G90_LJFOR
Local cChaveAnt	:= ""

Local cQuery		:= ""
Local cCond		:= "% "
Local cDiverg		:= ""

cCond += " AND G4C_CODIGO =  '" + cCodForn + "' AND G4C_LOJA = '" + cLjForn + "' "
cCond += "%"

cAliasTMP		:= GetNextAlias()

BeginSql Alias cAliasTMP

	SELECT 
		DISTINCT 
		G91_CHAVE, 
		G91_OPERAC,
		G4C_IDIF
	FROM  
		%table:G4C% G4C
	INNER JOIN  
		%table:G91% G91 
	ON 
		G91.G91_FILIAL = %xFilial:G91% 
		AND G4C.G4C_DOC = G91.G91_CHAVE   
		AND G91.%NotDel% 
		AND G91_CODIGO = %Exp:cCodigo% 
		AND G91_NUMFAT = %Exp:cNumFat%
		AND G91_CHAVE <> ''
		AND G91.G91_ASSOCI = ''
	WHERE 
		G4C.G4C_FILIAL = %xFilial:G4C%
		AND G4C.G4C_CARTUR =  %Exp:cCodigo% 
		AND G4C_STATUS <> '4'
		AND G4C.G4C_ASSOCI = '' 
		AND G4C_DOC <> ''
		%Exp:cCond%
		AND G4C.%NotDel%
	ORDER BY 
		G91_CHAVE
	
EndSql

DbSelectArea(cAliasTMP)
While!Eof()

	cQuery := "UPDATE "+ RetSQLName( "G91" ) + " " 
	cQuery += "SET G91_ASSOCI = '"+ cAssoci+ "' "
	cQuery += "WHERE "
	cQuery += " G91_FILIAL ='"+xFilial("G91")+"' "
	cQuery += " AND G91_CHAVE ='"+(cAliasTmp)->G91_CHAVE+"'"
	cQuery += " AND G91_CLASSI = '' "
	cQuery += " AND D_E_L_E_T_ = ' ' "	
							
	TCSQLExec( cQuery )
	
	cQuery := "UPDATE "+ RetSQLName( "G4C" ) + " " 
	cQuery += "SET G4C_FATCAR ='" + cNumFat +"', G4C_ASSOCI = '"+ cAssoci+ "' "
	cQuery += "WHERE "
	cQuery += " G4C_FILIAL ='"+xFilial("G4C")+"' "
	cQuery += " AND G4C_DOC ='"+(cAliasTmp)->G91_CHAVE+"'"
	cQuery += " AND G4C_CLIFOR = '2' "
	cQuery += " AND G4C_CODIGO = '" + cCodForn + "'"
	cQuery += " AND G4C_LOJA = '" + cLjForn + "'"
	
	//Filtra as opera��es de Emiss�o e Reembolso ser�o filtradas para consistir que os mesmos IFs 	
	If ( (cAliasTmp)->G91_OPERAC $ "1|2" )
		cQuery += " AND G4C_TIPO = '" + (cAliasTmp)->G91_OPERAC + "'"
	Endif
		
	cQuery += " AND D_E_L_E_T_ = ' ' "	
							
	TCSQLExec( cQuery )

	cChaveAnt	:= (cAliasTmp)->G91_CHAVE
	DbSelectArea(cAliasTMP)
	dbSkip()
	If (cAliasTMP)->(!Eof()) .And. cChaveAnt	<> (cAliasTmp)->G91_CHAVE
		cAssoci	:= Soma1(cAssoci)
	EndIf
End

//Grava Diverg�ncias
cAliasTMP		:= GetNextAlias()

BeginSql Alias cAliasTMP

	SELECT G4C_ASSOCI, G4C_EMISS, G91_DTEMIS, G91_ITEM, G91_DIVERG
	FROM  %table:G4C% G4C
	INNER JOIN  %table:G91% G91 ON G91.G91_FILIAL = %xFilial:G91% AND G4C.G4C_ASSOCI = G91.G91_ASSOCI   
	AND G91.%NotDel% 
	AND G91_CODIGO = %Exp:cCodigo% 
	AND G91_NUMFAT = %Exp:cNumFat%
	AND G91_CHAVE <> ''
	AND G91.G91_ASSOCI <> ''
	WHERE G4C.G4C_FILIAL = %xFilial:G4C%
	AND G4C.G4C_CARTUR =  %Exp:cCodigo%
	AND G4C_FATCAR = %Exp:cNumFat%	 
	AND G4C.G4C_ASSOCI <> '' 
	AND G4C_DOC <> ''	
	AND G4C.%NotDel%
	AND (( G4C_EMISS <> G91_DTEMIS) OR (G4C_EMISS = G91_DTEMIS AND G91_DIVERG <> ''))
		
EndSql

DbSelectArea(cAliasTMP)
While !Eof()
	cDiverg	:=	STR0020 //"Data de emiss�o da Fatura diferente da data de emiss�o do IF."
	
	 
	G91->(DbSetOrder(1))
	If G91->(DbSeek(xFilial("G91")+cCodigo+cNumFat+(cAliasTmp)->G91_ITEM))
		If (cAliasTmp)->G91_DTEMIS <> (cAliasTmp)->G4C_EMISS
			Reclock("G91",.F.)
			G91->G91_DIVERG	:=cDiverg
			G91->(MsUnlock())
		ElseIf (cAliasTmp)->G91_DTEMIS == (cAliasTmp)->G4C_EMISS .And. !Empty((cAliasTmp)->G91_DIVERG)
			Reclock("G91",.F.)
			G91->G91_DIVERG	:= ""
			G91->(MsUnlock())
		EndIf
	Endif
	DbSelectArea(cAliasTMP)
	DbSkip()
End

Return


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T061DAssoc

Codigo de Associa��o

@sample 	T061DAssoc()
@return  	aDados 
@author  	Simone Mie Sato Kakinoana
@since   	27/02/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T061DAssoc()

Local cCodigo	:= "000000"
Local oModel    := FwModelActive()
Local cCartao	:= G90->G90_CODIGO
Local cFatura	:= G90->G90_NUMFAT

Local cAliasTMP	:= "TMP"

cAliasTMP		:= GetNextAlias()

BeginSql Alias cAliasTMP

	SELECT ISNULL(MAX(G91_ASSOCI),'') CODIGO
			FROM %table:G91% G91
			WHERE G91.G91_FILIAL = %xFilial:G91%
			AND G91.G91_CODIGO = %Exp:cCartao%
			AND G91.G91_NUMFAT = %Exp:cFatura%			
			AND G91.%NotDel%  
	UNION
	SELECT ISNULL(MAX(G4C_ASSOCI),'') CODIGO			
			FROM %table:G4C% G4C
			WHERE G4C.G4C_FILIAL = %xFilial:G4C%
			AND G4C.G4C_CARTUR = %Exp:cCartao%
			AND G4C.G4C_FATCAR = %Exp:cFatura%			
			AND G4C.%NotDel%  
EndSql


DbSelectArea(cAliasTMP)
DbGotop()
While !Eof()
	If (cAliasTMP)-> CODIGO > cCodigo
		cCodigo	:= (cAliasTMP)-> CODIGO
	EndIf
	DbSkip()
End
  
cCodigo	:= Soma1(cCodigo)

Return(cCodigo)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T061dReass()

Verifica se j� possui associa��o e pergunta se deseja associar os itens ainda n�o associados.

@sample 	 T061dReass()
@return  	.T./.F.  
@author  	Simone Mie Sato Kakinoana
@since   	10/03/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T061DReass()

Local lRet	:= .T.

Local cCartao	:= G90->G90_CODIGO
Local cFatura	:= G90->G90_NUMFAT

Local cAliasTMP	:= "TMP"

cAliasTMP		:= GetNextAlias()

BeginSql Alias cAliasTMP

	SELECT COUNT(*) QUANT
			FROM %table:G91% G91
			WHERE G91.G91_FILIAL = %xFilial:G91%
			AND G91.G91_CODIGO = %Exp:cCartao%
			AND G91.G91_NUMFAT = %Exp:cFatura%			
			AND G91.G91_ASSOCI <> ''
			AND G91.%NotDel%
EndSql

DbSelectArea(cAliasTMP)
If (cAliasTMP)->QUANT > 0
	If MsgYesNo(STR0019)		//"Deseja associar os itens ainda n�o associados? " 
		lRet	:= .T.
	Else
		lRet	:= .F.
	Endif
Else
	lRet	:= .T.
EndIf 

Return(lRet)

