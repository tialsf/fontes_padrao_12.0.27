#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TURA061E.CH"
#Include "FWEditPanel.ch"

Static oTblHeader	:= nil //Tabela tempor�ria com os campos referente a pesquisaTA038Posto
Static oTblDetail	:= nil //Tabela tempor�ria com os campos referente aos DR's encontrados
Static oTblDetG4C	:= Nil//Tabela tempor�ria com os campos referente aos IF's encontrados

Static oMdlFake		:= Nil

STATIC oModConc		:= NIL
STATIC oViewConc	:= NIL

/*/{Protheus.doc} TURA061E
Rotina para efetuar a pesquisa dos documentos de reserva e atualizar a grid de itens financeiros. 
 
@type Function
@author Simone Mie Sato Kakinoana
@since 07/03/2016
@version 12.1.8
/*/
Function TURA061E(oModAnt,oViewAnt)

oModConc 	:= oModAnt
oViewConc	:= oViewAnt

//Verifica se a concilia��o j� foi efetivada. 
If !T061AChkEfet(.T.)	//Se ainda n�o foi efetivada, permite efetuar a pesquisa Doc Reserva. 
	FWExecView(STR0001,"VIEWDEF.TURA061E",3,,{|| .T.})	//"Pesquisa DR"
EndIf

Return

/*/{Protheus.doc} TURA061E
Modelo de dados
@type Static Function
@author Simone Mie Sato Kakinoana
@since 07/03/2016
@version 12.1.8
/*/
Static Function ModelDef()

Local oModel		:= MPFormModel():New( "TURA061E", /*bPreValidacao*/, /*bPosValidacao*/,{|oModel| T061ECommit(oModel), TA061EDestroy() }, {|oModel| FWFormCancel(oModel),  TA061EDestroy() } )

Local oStructCab 	:= FWFormModelStruct():New()
Local oStructDet 	:= FWFormModelStruct():New()
Local oStructG4C 	:= FWFormStruct( 1, 'G4C' )

Local bValid		:= {||.T.}

TA061ECrTb()

oStructCab:AddTable( oTblHeader:GetAlias(), , 	STR0003	, {|| oTblHeader:GetRealName() })	//"Dados compara��o"
oStructDet:AddTable( oTblDetail:GetAlias(), , STR0004 , {|| oTblDetail:GetRealName() })		//"Documentos de reserva"

AddFldCab( oStructCab, 1 )
AddFldDet( @oStructDet, 1 ,"DET")
AddFldDet( @oStructG4C, 1 ,"G4C")

AddGatilhos( oStructCab )

bValid := FWBuildFeature( STRUCT_FEATURE_VALID, "T061EVld()" )
oStructG4C:SetProperty('G4C_OK',MODEL_FIELD_VALID,bValid)

oModel:AddFields( "TMP_MASTER", /*cOwner*/, oStructCab )

oModel:AddGrid( 	"TMP_DETAIL", "TMP_MASTER", oStructDet, /*bLinePre*/, /*bLinePost*/, /*bPre*/, /*bLinePost*/, /*bLoad*/)
oModel:SetRelation( "TMP_DETAIL", { }, ( oTblDetail:GetAlias() )->( IndexKey( 1 ) ) )

oModel:AddGrid( 	"TMP_DETG4C", "TMP_DETAIL", oStructG4C, /*bLinePre*/, /*bLinePost*/, /*bPre*/, /*bLinePost*/, /*bLoad*/)

/*oModel:SetRelation( 'TMP_DETG4C', { { 'G4C_NUMID'  , '(oTblDetail:GetAlias())->G3R_NUMID'       } , ;
								   { 'G4C_IDITEM' , '(oTblDetail:GetAlias())->G3R_IDITEM'        }  ,;
								   { 'G4C_NUMSEQ' , '(oTblDetail:GetAlias())->G3R_NUMSEQ'        }  ,;
								   { 'G4C_CLIFOR' , '2'        }  ,;
									{ 'G4C_FATCAR' , ' '       }  ,;
									{ 'G4C_CARTUR' , ' '       } }, G4C->( IndexKey( 1 ) ) )*/
							
oModel:SetRelation( 'TMP_DETG4C', { { 'G4C_NUMID'  , '(oTblDetail:GetAlias())->G3R_NUMID'       } , ;
								   { 'G4C_IDITEM' , '(oTblDetail:GetAlias())->G3R_IDITEM'        }  ,;
								   { 'G4C_NUMSEQ' , '(oTblDetail:GetAlias())->G3R_NUMSEQ'        }  ,;
								   { 'G4C_CLIFOR' , '2'        }  ,;
									{ 'G4C_FATCAR' , ' '       } }, G4C->( IndexKey( 1 ) ) )
									

oModel:GetModel("TMP_DETAIL"):SetNoInsertLine(.T.)
oModel:GetModel("TMP_DETAIL"):SetNoDeleteLine(.T.)

oModel:GetModel("TMP_DETG4C"):SetNoInsertLine(.T.)
oModel:GetModel("TMP_DETG4C"):SetNoDeleteLine(.T.)

oModel:GetModel( 'TMP_DETG4C'):SetOptional(.T.)

oModel:SetPrimaryKey({})
	
oMdlFake := oModel

Return oModel

/*/{Protheus.doc} TURA061E
ViewDef
@type Static Function
@author Simone Mie Sato Kakinoana
@since 07/03/2016
@version 12.1.8
/*/
Static Function ViewDef()

Local oModel   	:= FWLoadModel( "TURA061E" )
Local oView		:= FWFormView():New()
Local oStructCab	:= FWFormViewStruct():New()
Local oStructDet 	:= FWFormViewStruct():New()
Local oStructG4C 	:= FWFormStruct( 2, 'G4C' )

oView:SetModel( oModel )

AddFldCab( oStructCab, 2 )
AddFldDet( @oStructDet, 2,"DET" )
AddFldDet( @oStructG4C, 2,"G4C" )

AddGroups( oStructCab )

oView:AddField("VIEW_TMPM", oStructCab, "TMP_MASTER" )
oView:AddGrid( "VIEW_TMPD", oStructDet, "TMP_DETAIL" )
oView:AddGrid( "VIEW_G4C", oStructG4C, "TMP_DETG4C" )

oView:AddUserButton( STR0001, "CLIPS", {|| FwMsgRun(,{|| TA061EQry() },,STR0005 ) } )//"Pesquisar DR"###"Pesquisando documentos..."

oView:CreateHorizontalBox( "SUPERIOR"	, 30 )
oView:CreateHorizontalBox( "MEIO"	, 35 )
oView:CreateHorizontalBox( "INFERIOR"	, 35 )

oView:SetOwnerView( "VIEW_TMPM"	, "SUPERIOR" )
oView:SetOwnerView( "VIEW_TMPD"	, "MEIO" )
oView:SetOwnerView( "VIEW_G4C"	, "INFERIOR" )

Return oView

/*/{Protheus.doc} TA061ECrTgb
Cria arquivo de trabalho
@type Static Function
@author Simone Mie Sato Kakinoana
@since 07/03/2016
@version 12.1.8
/*/
Static Function TA061ECrTb()

	Local aStruHeader	:= {}
	Local aStruDetail	:= {}
	Local aDRFields	:= {"G3R_NUMID", "G3R_IDITEM", "G3R_NUMSEQ", "G3R_DOC", "G3R_CONJUG", "G3R_EMISS", "G3R_TARIFA", "G3R_TAXA", "G3R_EXTRAS", "G3R_VLCOMI", "G3R_TAXADU", "G3R_VLINCE", "G3R_TOTREC", "G3R_TPSEG","G3R_FORNEC","G3R_LOJA","A2_NOME","G3Q_POSTO","G3M_DESCR","G3Q_CLIENT","G3Q_LOJA","A1_NOME"}

	Local nX	:= 0

	AAdd(aStruHeader,{"TMP_XEMIDE"	,"D",08						,0})
	AAdd(aStruHeader,{"TMP_XEMIAT"	,"D",08						,0})
	AAdd(aStruHeader,{"TMP_XFORND"	,"C",TamSX3("A2_COD")[1]		,0})
	AAdd(aStruHeader,{"TMP_XLOJAD"	,"C",TamSX3("A2_LOJA")[1]	,0})
	AAdd(aStruHeader,{"TMP_XNOMFO"	,"C",TamSX3("A2_NOME")[1]	,0})
	AAdd(aStruHeader,{"TMP_XFORNA"	,"C",TamSX3("A2_COD")[1]		,0})
	AAdd(aStruHeader,{"TMP_XLOJAA"	,"C",TamSX3("A2_LOJA")[1]	,0})
	AAdd(aStruHeader,{"TMP_XNOMFA"	,"C",TamSX3("A2_NOME")[1]	,0})
	AAdd(aStruHeader,{"TMP_XPOSTD"	,"C",TamSX3("G3P_POSTO")[1]	,0})
	AAdd(aStruHeader,{"TMP_XNOMPD"	,"C",TamSX3("G3P_DESPST")[1],0})
	AAdd(aStruHeader,{"TMP_XPOSTA"	,"C",TamSX3("G3P_POSTO")[1]	,0})
	AAdd(aStruHeader,{"TMP_XNOMPA"	,"C",TamSX3("G3P_DESPST")[1],0})
	AAdd(aStruHeader,{"TMP_XCLIDE"	,"C",TamSX3("A1_COD")[1]		,0})
	AAdd(aStruHeader,{"TMP_XCLLJD"	,"C",TamSX3("A1_LOJA")[1]	,0})
	AAdd(aStruHeader,{"TMP_XCLNDE"	,"C",TamSX3("A1_NOME")[1]	,0})
	AAdd(aStruHeader,{"TMP_XCLIAT"	,"C",TamSX3("A1_COD")[1]		,0})
	AAdd(aStruHeader,{"TMP_XCLLJA"	,"C",TamSX3("A1_LOJA")[1]	,0})
	AAdd(aStruHeader,{"TMP_XCLINA"	,"C",TamSX3("A1_NOME")[1]	,0})
	AAdd(aStruHeader,{"TMP_XCANC"	,"L",1							,0})
	AAdd(aStruHeader,{"TMP_XDOC"	,"C",TamSX3("G3Q_DOC")[1]	,0})
	AAdd(aStruHeader,{"TMP_XCARDE"	,"C",TamSX3("G90_CODIGO")[1]	,0})
	AAdd(aStruHeader,{"TMP_XCARAT"	,"C",TamSX3("G90_CODIGO")[1]	,0})

	oTblHeader := FWTemporaryTable():New()

	oTblHeader:SetFields( aStruHeader )
	oTblHeader:AddIndex( "index1", {"TMP_XDOC"} )
	oTblHeader:Create()

	SX3->( dbSetOrder(2) )
	For nX := 1 to len(aDRFields)
		If SX3->( dbSeek( aDRFields[nX] ) )
			AAdd(aStruDetail,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		EndIf
	Next

	oTblDetail := FWTemporaryTable():New()

	oTblDetail:SetFields( aStruDetail )
	oTblDetail:AddIndex( "index1", {"G3R_NUMID", "G3R_IDITEM", "G3R_NUMSEQ", "G3R_DOC"} )
	oTblDetail:Create()
	

Return

/*/{Protheus.doc} TA061ECommit
Grava��o
@type Static Function
@author Simone Mie Sato Kakinoana
@since 11/03/2016
@version 12.1.8
/*/
Static Function T061ECommit(oModelPesq)

Local oMdlPesq		 		:= oModelPesq:GetModel("TMP_DETG4C")
Local oGridG4C				:= oModConc:GetModel('G4CDETAIL')
Local oTotFin				:= oModConc:GetModel('TOTFIN')

Local oModelG4CTab 		:= Nil
Local oModelG4CAux 		:= Nil

Local nLinAnt				:= 0 
Local nX					:= 0
Local nY					:= 0
Local nPosG4C				:= 0
Local nTotFin				:= oTotFin:GetValue("XXX_TOTFIN") 

Local lOk					:= .F.
Local lGrava				:= .F.

Local aAuxG4C				:= {}
Local aStruG4C				:= {} 
Local aAuxPesq				:= {}
Local aStruPesq			:= {} 


oGridG4C:SetNoInsertLine(.F.)

nLinAnt	:= oMdlPesq:GetLine()

aAuxG4C		:= oGridG4C:GetStruct()
aStruG4C	:= aAuxG4C:GetFields()

aAuxPesq	:= oMdlPesq:GetStruct()
aStruPesq	:= aAuxPesq:GetFields()


For nX := 1 To oMdlPesq:Length()

	oMdlPesq:GoLine(nX)	
	
	lOk := oMdlPesq:GetValue("G4C_OK")
	
	If lOk
		oGridG4C:AddLine()
		
		For nY := 1 to Len(aStruPesq)
			nPosG4C := ASCAN(aStruG4C,{|x| x[3]== aStruPesq[nY][3]})  
			If  nPosG4C > 0 
				If  Alltrim(aStruPesq[nY][3]) == "G4C_FATCAR"
					oGridG4C:SetValue( aStruPesq[nY][3], G90->G90_NUMFAT )
					lGrava	:= .T.
				Else
					oGridG4C:SetValue( aStruPesq[nY][3], oMdlPesq:GetValue(aStruPesq[nY][3]) )
				EndIf				
			EndIf
			
			If nY == Len(aStruPesq) .And. lGrava 
				oGridG4C:LoadValue("G4C_LEGEN1","BR_VERMELHO")				
				oGridG4C:LoadValue("G4C_OK",.F.)
				lGrava	:= .F.							
			EndIf				
		Next	
		If oGridG4C:GetValue("G4C_PAGREC") == "1"
   	       	nTotFin += oMdlPesq:GetValue("G4C_VALOR")
   	    ElseIf oGridG4C:GetValue("G4C_PAGREC") == "2"
   	      	nTotFin -= oMdlPesq:GetValue("G4C_VALOR")
   	    EndIf
   	    
   	    G4C->(DbSetOrder(2))
		If G4C->(DbSeek(xFilial("G4C")+oGridG4C:GetValue("G4C_IDIF")))
   	    	
  			oModelG4CTab 		:= FwLoadModel("TURA061X") 	
			oModelG4CTab		:SetOperation( MODEL_OPERATION_UPDATE )
			oModelG4CAux		:= oModelG4CTab:GetModel("G4C_MASTER")
			
			If oModelG4CTab:Activate()
				oModelG4CAux:SetValue("G4C_FATCAR",oGridG4C:GetValue("G4C_FATCAR"))
				If oModelG4CTab:VldData()
					FwFormCommit(oModelG4CTab)
				Else
					JurShowErro( oModelG4CTab:GetModel():GetErrorMessage())
				EndIf
				oModelG4CTab:DeActivate()
				oModelG4CTab:Destroy()
			EndIf
			
		EndIf   	    	
	Endif 

Next nX

oTotFin:LoadValue("XXX_TOTFIN",nTotFin)	
oMdlPesq:GoLine(nLinAnt)
oGridG4C:SetNoInsertLine(.T.)

Return

/*/{Protheus.doc} TA061EDestroy
Deleta objeto
@type Static Function
@author Simone Mie Sato Kakinoana
@since 07/03/2016
@version 12.1.8
/*/
Function TA061EDestroy()

	//Verifica se o objeto FwTemporaryTable est� instanciado
	If ValType( oTblHeader ) == "O"
		oTblHeader:Delete()
		FreeObj( oTblHeader )
	EndIf

	//Verifica se o objeto FwTemporaryTable est� instanciado
	If ValType( oTblDetail ) == "O"
		oTblDetail:Delete()
		FreeObj( oTblDetail )
	EndIf

	If ValType( oTblDetG4C ) == "O"
		oTblDetG4C:Delete()
		FreeObj( oTblDetG4C )
	EndIf

Return .T.

/*/{Protheus.doc} TA061EQry
Grava��o
@type Static Function
@author Simone Mie Sato Kakinoana
@since 07/03/2016
@version 12.1.8
/*/
Function TA061EQry()

Local cAlias 			:= GetNextAlias()
Local cFilterSA1		:= "% "
Local cFilterSA2		:= "% "
Local cFilterG3Q		:= "% "
Local cFilterG3R		:= "% "
Local cFilterG4C		:= "% "

Local cCartao			:= G90->G90_CODIGO
Local cNumId			:= ""
Local cIdItem			:= ""
Local cNumSeq			:= ""

Local lNewLine			:= .F.
Local lFirst			:= .T.
Local nX				:= 0
Local nY				:= 0 
Local oModel			:= FwModelActive()
Local oModelMaster	:= oModel:GetModel("TMP_MASTER")
Local oModelDetail	:= oModel:GetModel("TMP_DETAIL")
Local oModelG4C		:= oModel:GetModel("TMP_DETG4C")

Local aAux				:= {}
Local aStruG4C			:= {} 
	
If Empty(oModel:GetModel("TMP_MASTER"):GetValue("TMP_XEMIDE")) .Or. Empty(oModel:GetModel("TMP_MASTER"):GetValue("TMP_XEMIAT"))
	Help(,,"Tura038aQuery",,STR0006,1,0)	//"Informe os parametros obrigatorios antes de fazer a consulta de documentos de reserva."
	Return
EndIf

oModel:GetModel("TMP_DETAIL"):SetNoInsertLine(.F.)
oModel:GetModel("TMP_DETAIL"):SetNoDeleteLine(.F.)

oModel:GetModel("TMP_DETG4C"):SetNoInsertLine(.F.)
oModel:GetModel("TMP_DETG4C"):SetNoDeleteLine(.F.)

oModelDetail:ClearData()
oModelG4C:ClearData()

	//Filtros Where da tabela G3R
	If !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XNURVD") ) .And. !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XNURVA") )	
		cFilterG3R += " AND G3R_NUMID BETWEEN '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XNURVD") + "' AND '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XNURVA") + "'"
	ElseIf !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XNURVD") )
		cFilterG3R += " AND G3R_NUMID >= '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XNURVD") + "'"
	ElseIf !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XNURVA") )
		cFilterG3R += " AND G3R_NUMID <= '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XNURVA") + "'"
	EndIf

	If !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XIDRVD") ) .And. !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XIDRVA") )	
		cFilterG3R += " AND G3R_IDITEM BETWEEN '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XIDRVD") + "' AND '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XIDRVA") + "'"
	ElseIf !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XIDRVD") )
		cFilterG3R += " AND G3R_IDITEM >= '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XIDRVD") + "'"
	ElseIf !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XIDRVA") )
		cFilterG3R += " AND G3R_IDITEM <= '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XIDRVA") + "'"
	EndIf

	If !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XSERVD") ) .And. !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XSERVA") )	
		cFilterG3R += " AND G3R_NUMSEQ BETWEEN '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XSERVD") + "' AND '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XSERVA") + "'"
	ElseIf !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XSERVD") )
		cFilterG3R += " AND G3R_NUMSEQ >= '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XSERVD") + "'"
	ElseIf !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XSERVA") )
		cFilterG3R += " AND G3R_NUMSEQ <= '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XSERVA") + "'"
	EndIf	
	
 
	If !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XEMIDE") ) .And. !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XEMIAT") )	
		cFilterG3R += " AND G3R_EMISS BETWEEN '" + DToS(oModelMaster:GetValue("TMP_XEMIDE")) + "' AND '" + DToS(oModelMaster:GetValue("TMP_XEMIAT")) + "' "
	ElseIf !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XEMIDE") )
		cFilterG3R += " AND G3R_EMISS >= '" + DToS(oModelMaster:GetValue("TMP_XEMIDE")) + "'" 
	ElseIf !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XEMIAT") )
		cFilterG3R += " AND G3R_EMISS <= '" + DToS(oModelMaster:GetValue("TMP_XEMIAT")) + "'" 
	EndIf	
	
	If !Empty(oModelMaster:GetValue("TMP_XDOC"))
		cFilterG3R += " AND G3R_DOC LIKE '%" + AllTrim(oModelMaster:GetValue("TMP_XDOC")) + "%' "
	EndIf

	If !oModelMaster:GetValue("TMP_XCANC")
		cFilterG3R += " AND G3R_STATUS <> '4' "
	EndIf


	//Filtros Where da tabela SA2
	If !Empty(oModelMaster:GetValue("TMP_XFORND"))
		cFilterSA2 +=  " AND A2_COD >= '" + oModelMaster:GetValue("TMP_XFORND") + "'"
	EndIf

	If !Empty(oModelMaster:GetValue("TMP_XLOJAD"))
		cFilterSA2 += " AND A2_LOJA >= '" + oModelMaster:GetValue("TMP_XLOJAD") + "'"
	EndIf

	If !Empty(oModelMaster:GetValue("TMP_XFORNA"))
		cFilterSA2 += " AND A2_COD <= '" + oModelMaster:GetValue("TMP_XFORNA") + "'"
	EndIf

	If !Empty(oModelMaster:GetValue("TMP_XLOJAA"))
		cFilterSA2 += " AND A2_LOJA <= '" + oModelMaster:GetValue("TMP_XLOJAA") + "'"
	EndIf


	//Filtros Where da tabala G3Q
	If !Empty(oModelMaster:GetValue("TMP_XPOSTD"))
		cFilterG3Q += " AND G3Q_POSTO >= '" + oModelMaster:GetValue("TMP_XPOSTD") + "'"
	EndIf

	If !Empty(oModelMaster:GetValue("TMP_XPOSTA"))
		cFilterG3Q += " AND G3Q_POSTO <= '" + oModelMaster:GetValue("TMP_XPOSTA") + "'"
	EndIf

	//Filtros Where da tabela SA1
	If !Empty(oModelMaster:GetValue("TMP_XCLIDE"))
		cFilterSA1 += " AND A1_COD >= '" + oModelMaster:GetValue("TMP_XCLIDE") + "'"
	EndIf

	If !Empty(oModelMaster:GetValue("TMP_XCLLJD"))
		cFilterSA1 += " AND A1_LOJA >= '" + oModelMaster:GetValue("TMP_XCLLJD") + "'"
	EndIf

	If !Empty(oModelMaster:GetValue("TMP_XCLIAT"))
		cFilterSA1 += " AND A1_COD <= '" + oModelMaster:GetValue("TMP_XCLIAT") + "'"
	EndIf

	If !Empty(oModelMaster:GetValue("TMP_XCLLJA"))
		cFilterSA1 += " AND A1_LOJA <= '" + oModelMaster:GetValue("TMP_XCLLJA") + "'"
	EndIf

	//Filtros Where da tabala G4C
	If !Empty(oModelMaster:GetValue("TMP_XCARDE"))
		cFilterG4C += " AND G4C_CARTUR >= '" + oModelMaster:GetValue("TMP_XCARDE") + "'"
	EndIf

	If !Empty(oModelMaster:GetValue("TMP_XCARAT"))
		cFilterG4C += " AND G4C_CARTUR <= '" + oModelMaster:GetValue("TMP_XCARAT") + "'"
	EndIf


	cFilterSA1		+= " %"
	cFilterSA2		+= " %"
	cFilterG3Q		+= " %"
	cFilterG3R		+= " %"
	cFilterG4C		+= " %"

	BeginSql Alias cAlias
		Column G3R_EMISS as date

		SELECT	G3R_NUMID,
				G3R_IDITEM,
				G3R_NUMSEQ,
				G3R_DOC,
				G3R_CONJUG,
				G3R_EMISS,
				G3R_TARIFA,
				G3R_TAXA,
				G3R_EXTRAS,
				G3R_VLCOMI,
				G3R_TAXADU,
				G3R_VLINCE,
				G3R_TOTREC,
				G3R_TPSEG,
				G3R_FORNEC,
				G3R_LOJA,
				A2_NOME,
				G3Q_POSTO,
				G3M_DESCR,
				G3Q_CLIENT,
				G3Q_LOJA,
				A1_NOME
			FROM %Table:G3R% G3R
			INNER JOIN %Table:G3Q% G3Q
				ON G3Q.G3Q_NUMID = G3R.G3R_NUMID
					AND G3Q.G3Q_IDITEM = G3R.G3R_IDITEM
					AND G3Q.G3Q_NUMSEQ = G3R.G3R_NUMSEQ
					AND G3Q_FILIAL = %xFilial:G3Q%
					AND G3Q.%notDel%
					%Exp:cFilterG3Q%
			INNER JOIN %Table:G3M% G3M
				ON G3M.G3M_CODIGO = G3Q.G3Q_POSTO
					AND G3M_FILIAL = %xFilial:G3M%
					AND G3M.%notDel%
			INNER JOIN %Table:SA1% SA1
				ON SA1.A1_COD = G3Q.G3Q_CLIENT
					AND SA1.A1_LOJA = G3Q.G3Q_LOJA
					AND SA1.A1_FILIAL = %xFilial:SA1%
					AND SA1.%notDel%
					%Exp:cFilterSA1%
			INNER JOIN %Table:SA2% SA2
				ON SA2.A2_COD = G3R.G3R_FORNEC
					AND SA2.A2_LOJA = G3R.G3R_LOJA
					AND SA2.A2_FILIAL = %xFilial:SA2%
					AND SA2.%notDel%
					%Exp:cFilterSA2%				 
				WHERE G3R_FILIAL = %xFilial:G3R%
					AND G3R.%notDel%
				%Exp:cFilterG3R%
	EndSql

	While (cAlias)->( !EOF() )
		If lNewLine
			oModelDetail:AddLine()
		EndIf


		For nX := 1 to (cAlias)->( FCount() )

			oModelDetail:SetValue( (cAlias)->( FieldName(nX) ), (cAlias)->( FieldGet(nX) )  )

		Next

		lNewLine := .T.
		(cAlias)->( dbSkip() )
	EndDo
	
	lNewLine 	:= .F.
	aAux		:=  oModelG4C:GetStruct()
	aStruG4C	:= aAux:GetFields()
	cAliasG4C 	:= GetNextAlias()
	
	//Atualizar Grid dos itens financeiros.
	For nX := 1 To oModelDetail:Length()
	
		oModelDetail:GoLine(nX)
		
		cNumId		:= oModelDetail:GetValue("G3R_NUMID")
		cIdItem		:= oModelDetail:GetValue("G3R_IDITEM")
		cNumSeq		:= oModelDetail:GetValue("G3R_NUMSEQ")
		
		If Select(cAliasG4C) > 0  
			dbSelectArea(cAliasG4C)
			dbCloseArea()
		EndIF
		
		BeginSql Alias cAliasG4C
		
		SELECT *		
		FROM %Table:G4C% G4C
		WHERE G4C_FILIAL = %xFilial:G4C%
				AND G4C.%notDel%
				AND G4C_NUMID =%Exp:cNumID% 
				AND G4C_IDITEM =%Exp:cIdItem%
				AND G4C_NUMSEQ =%Exp:cNumSeq%		
				AND G4C_CLIFOR='2'	
				AND G4C_CARTUR = %Exp:cCartao% 
				AND G4C_FATCAR = ''
				AND G4C_STATUS <> '4'
				%Exp:cFilterG4C%	 	
		EndSql			
		
		
		While (cAliasG4C)->( !EOF() )
			If !lFirst 
				oModelG4C:AddLine()
			EndIf

			For nY := 1 to (cAliasG4C)->( FCount() )
				If ASCAN(aStruG4C,{|x| x[3]== (cAliasG4C)->( FieldName(nY))}) > 0 
					If (cAliasG4C)->( FieldName(nY)) == "G4C_EMISS"
						oModelG4C:SetValue( "G4C_EMISS", STOD((cAliasG4C)->( FieldGet(nY)) )  )					 
					Else
						oModelG4C:SetValue( (cAliasG4C)->( FieldName(nY) ), (cAliasG4C)->( FieldGet(nY) )  )
					EndIf
				EndIf
			Next nY
			
			(cAliasG4C)->( dbSkip() )
			lFirst := .F.
		EndDo
		lFirst	:= .T.
				
	Next nX

	(cAliasG4C)->(dbCloseArea())

	oModelDetail:GoLine(1)
	oModelG4C:GoLine(1)
	
	(cAlias)->(dbCloseArea())

	oModel:GetModel("TMP_DETAIL"):SetNoInsertLine(.T.)
	oModel:GetModel("TMP_DETAIL"):SetNoDeleteLine(.T.)

	oModel:GetModel("TMP_DETG4C"):SetNoInsertLine(.T.)
	oModel:GetModel("TMP_DETG4C"):SetNoDeleteLine(.T.)
	
Return


/*/{Protheus.doc} Ta061EG6J
Busca os registros a serem carregados na G6I e atualizados na G3R.
@author Simone Mie Sato Kakinoana
@since 07/03/2016
@version 12.1.8
/*/
Static Function Ta061EG6J(cFaturaG6H,cCodDoc)

Local cAliasQry := GetNextAlias()
Local lPosto	:= .F.
Local aRetorno	:= {}
	
	BeginSql Alias cAliasQry
		SELECT G3R_POSTOR,G3M_NIATA,G3R_NUMID,G3R_IDITEM,G3R_NUMSEQ,G6I_BILHET,G6I_FATURA,;
				G6I_TARIFA,G3R_TARIFA,G6I_TAXAS,G3R_TAXA,G3R_EXTRAS,G6I_VLRCOM,G3R_VLCOMI,;
				G3R_TAXADU,G6I_VLRINC,G3R_VLINCE
			FROM %Table:G6I% G6I
			INNER JOIN %table:G6H% G6H ON
			G6H_FILIAL = %xFilial:G6I% AND
			G6H_FATURA = G6I_FATURA AND
			G6H.%NotDel%
			INNER JOIN %Table:G3R% G3R ON
				G3R_FILIAL = %xFilial:G6I% AND
				G3R_DOC    = G6I_BILHET    AND
				G3R.%NotDel%
			INNER JOIN %Table:G3M% ON
				G3M_FILIAL = %xFilial:G3M% AND
				G3M_CODIGO   = G3R_POSTOR
				WHERE G6I_FATURA = %Exp:cFaturaG6H% AND G6I_BILHET = %Exp:cCodDoc% AND G6I.%NotDel%					
	EndSql
	
	
	While (cAliasQry)->(!Eof())
				
		If Len(aRetorno) == 0
			Aadd(aRetorno,{(cAliasQry)->G3R_POSTOR,(cAliasQry)->G3M_NIATA,(cAliasQry)->G3R_NUMID,;
				(cAliasQry)->G3R_IDITEM,(cAliasQry)->G3R_NUMSEQ,(cAliasQry)->G6I_BILHET,(cAliasQry)->G6I_FATURA,;
				(cAliasQry)->G6I_TARIFA,(cAliasQry)->G3R_TARIFA,(cAliasQry)->G6I_TAXAS,(cAliasQry)->G3R_TAXA,;
				(cAliasQry)->G3R_EXTRAS,(cAliasQry)->G6I_VLRCOM,(cAliasQry)->G3R_VLCOMI,(cAliasQry)->G3R_TAXADU,;
				(cAliasQry)->G6I_VLRINC,(cAliasQry)->G3R_VLINCE})
		Else		
			lPosto := Ta038ABPos(aRetorno,(cAliasQry)->G3R_IDITEM)
			
			If !lPosto
				Aadd(aRetorno,{(cAliasQry)->G3R_POSTOR,(cAliasQry)->G3M_NIATA,(cAliasQry)->G3R_NUMID,;
					(cAliasQry)->G3R_IDITEM,(cAliasQry)->G3R_NUMSEQ,(cAliasQry)->G6I_BILHET,(cAliasQry)->G6I_FATURA,;
					(cAliasQry)->G6I_TARIFA,(cAliasQry)->G3R_TARIFA,(cAliasQry)->G6I_TAXAS,(cAliasQry)->G3R_TAXA,;
					(cAliasQry)->G3R_EXTRAS,(cAliasQry)->G6I_VLRCOM,(cAliasQry)->G3R_VLCOMI,(cAliasQry)->G3R_TAXADU,;
					(cAliasQry)->G6I_VLRINC,(cAliasQry)->G3R_VLINCE})
			EndIf
		EndIf
	
	(cAliasQry)->(DbSkip())
	EndDo
	
	(cAliasQry)->(DbCloseArea())

Return aRetorno


Static Function AddFldCab( oStruct, nOpc )
	If nOpc == 1


		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0007		,; // [01] C Titulo do campo	//"Num RV De"
							"" 					,; // [02] C ToolTip do campo
							"TMP_XNURVD" 		,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							TamSX3("G3R_NUMID")[1] 		,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							{|| .T.}			,; // [08] B Code-block de valida��o When do campo
							Nil 				,; // [09] A Lista de valores permitido do campo
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							Nil					,; // [11] B Code-block de inicializacao do campo
							.F. 				,; // [12] L Indica se trata de um campo chave
							.F. 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual

		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0008			,; // [01] C Titulo do campo	//"ID RV De"	
							"" 					,; // [02] C ToolTip do campo
							"TMP_XIDRVD" 		,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							TamSX3("G3R_IDITEM")[1] 		,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							{|| .T.}			,; // [08] B Code-block de valida��o When do campo
							Nil 				,; // [09] A Lista de valores permitido do campo
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							Nil					,; // [11] B Code-block de inicializacao do campo
							.F. 				,; // [12] L Indica se trata de um campo chave
							.F. 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual


		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0009			,; // [01] C Titulo do campo	//"Seq RV De"
							"" 					,; // [02] C ToolTip do campo
							"TMP_XSERVD" 		,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							TamSX3("G3R_NUMSEQ")[1] 		,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							{|| .T.}			,; // [08] B Code-block de valida��o When do campo
							Nil 				,; // [09] A Lista de valores permitido do campo
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							Nil					,; // [11] B Code-block de inicializacao do campo
							.F. 				,; // [12] L Indica se trata de um campo chave
							.F. 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual

		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0010		,; // [01] C Titulo do campo	//"Num RV Ate"
							"" 					,; // [02] C ToolTip do campo
							"TMP_XNURVA" 		,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							TamSX3("G3R_NUMID")[1] 		,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							{|| .T.}			,; // [08] B Code-block de valida��o When do campo
							Nil 				,; // [09] A Lista de valores permitido do campo
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							Nil					,; // [11] B Code-block de inicializacao do campo
							.F. 				,; // [12] L Indica se trata de um campo chave
							.F. 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual

		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0011			,; // [01] C Titulo do campo	//"ID RV Ate"
							"" 					,; // [02] C ToolTip do campo
							"TMP_XIDRVA" 		,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							TamSX3("G3R_IDITEM")[1] 		,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							{|| .T.}			,; // [08] B Code-block de valida��o When do campo
							Nil 				,; // [09] A Lista de valores permitido do campo
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							Nil					,; // [11] B Code-block de inicializacao do campo
							.F. 				,; // [12] L Indica se trata de um campo chave
							.F. 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual


		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0012			,; // [01] C Titulo do campo	//"Seq RV Ate"
							"" 					,; // [02] C ToolTip do campo
							"TMP_XSERVA" 		,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							TamSX3("G3R_NUMSEQ")[1] 		,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							{|| .T.}			,; // [08] B Code-block de valida��o When do campo
							Nil 				,; // [09] A Lista de valores permitido do campo
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							Nil					,; // [11] B Code-block de inicializacao do campo
							.F. 				,; // [12] L Indica se trata de um campo chave
							.F. 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual


		oStruct:AddField(	STR0013,;//"Emissao De"
							"",;
							"TMP_XEMIDE",;
							"D",;
							08,;
							0,;
							{|| .T. },;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0014,;	//"Emissao Ate"
							"",;
							"TMP_XEMIAT",;
							"D",;
							08,;
							0,;
							{|| .T. },;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0015,;//"Fornecedor De"
							"",;
							"TMP_XFORND",;
							"C",;
							TamSX3("A2_COD")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0016,;//"Loja De"
							"",;
							"TMP_XLOJAD",;
							"C",;
							TamSX3("A2_LOJA")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0017,;//"Nome Fornecedor De"
							"",;
							"TMP_XNOMFO",;
							"C",;
							TamSX3("A2_NOME")[1],;
							0,;
							{|| .T. },;
							{|| .T. },;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0018,;//"Fornecedor Ate"
							"",;
							"TMP_XFORNA",;
							"C",;
							TamSX3("A2_COD")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0019,;//"Loja Ate"
							"",;
							"TMP_XLOJAA",;
							"C",;
							TamSX3("A2_LOJA")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0020,;//"Nome Fornecedor Ate"
							"",;
							"TMP_XNOMFA",;
							"C",;
							TamSX3("A2_NOME")[1],;
							0,;
							{|| .T. },;
							{|| .T. },;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0021,;//"Posto De"
							"",;
							"TMP_XPOSTD",;
							"C",;
							TamSX3("G3P_POSTO")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )


		oStruct:AddField(	STR0022,;//"Nome Posto De"
							"",;
							"TMP_XNOMPD",;
							"C",;
							TamSX3("G3P_DESPST")[1],;
							0,;
							{|| .T. },;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0023,;//"Posto Ate"
							"",;
							"TMP_XPOSTA",;
							"C",;
							TamSX3("G3P_POSTO")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0024,;//"Nome Posto Ate"
							"",;
							"TMP_XNOMPA",;
							"C",;
							TamSX3("G3P_DESPST")[1],;
							0,;
							{|| .T. },;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0025,;//"Cliente De"
							"",;
							"TMP_XCLIDE",;
							"C",;
							TamSX3("A1_COD")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0026,;//"Loja De"
							"",;
							"TMP_XCLLJD",;
							"C",;
							TamSX3("A1_LOJA")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0027,;//"Nome Cliente De"
							"",;
							"TMP_XCLNDE",;
							"C",;
							TamSX3("A1_NOME")[1],;
							0,;
							{|| .T. },;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0028,;//"Cliente Ate"
							"",;
							"TMP_XCLIAT",;
							"C",;
							TamSX3("A1_COD")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0029,;//"Loja Ate"
							"",;
							"TMP_XCLLJA",;
							"C",;
							TamSX3("A1_LOJA")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0030,;//"Nome Cliente Ate"
							"",;
							"TMP_XCLINA",;
							"C",;
							TamSX3("A1_NOME")[1],;
							0,;
							{|| .T. },;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0031,;//"Listar Cancelados"
							"",;
							"TMP_XCANC",;
							"L",;
							1,;
							0,;
							{|| .T. },;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0032,;//"Localizador/Bilhete"
							"",;
							"TMP_XDOC"	,;
							"C",;
							TamSX3("G3Q_DOC")[1],;
							0,;
							{|| .T. },;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )
							
							
			oStruct:AddField(	STR0033,;//"C�digo Cart�o De"
							"",;
							"TMP_XCARDE"	,;
							"C",;
							TamSX3("G90_CODIGO")[1],;
							0,;
							{|| .T. },;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

			oStruct:AddField(	STR0034,;//"C�digo Cart�o Ate"
							"",;
							"TMP_XCARAT"	,;
							"C",;
							TamSX3("G90_CODIGO")[1],;
							0,;
							{|| .T. },;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )					

												

	Else

	
		oStruct:AddField(	"TMP_XNURVD",;
							'02',;
							STR0007,;	//"Num RV De"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
							STR0007,;	//"Num RV De"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	"TMP_XIDRVD",;
							'03',;
							STR0008,;	//"ID RV De"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
							STR0008,;	//"ID RV De"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	"TMP_XSERVD",;
							'04',;
							STR0009,;//"Seq RV De"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
							STR0009,;//"Seq RV De"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil )
							
		oStruct:AddField(	"TMP_XNURVA",;
							'05',;
							STR0010,;//"Num RV Ate"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
							STR0010,;//"Num RV Ate"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	"TMP_XIDRVA",;
							'06',;
							STR0011,;//"ID RV Ate"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
							STR0011,;//"ID RV Ate"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	"TMP_XSERVA",;
							'07',;
							STR0012,; //"Seq RV Ate"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
							STR0012,;//"Seq RV Ate"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil )

							
		oStruct:AddField(	'TMP_XEMIDE',;
							'08',;
							STR0013,;//"Emissao De"
							STR0013,;//"Emissao De"
							{},;
							'D',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XEMIAT',;
							'09',;
							STR0014,;//"Emissao Ate"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
							STR0014,;//"Emissao Ate"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
							{},;
							'D',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XFORND',;
							'10',;
							STR0015,;//"Fornecedor De"
							STR0015,;//"Fornecedor De"
							{},;
							'C',;
							'',;
							Nil,;
							'SA2',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XLOJAD',;
							'11',;
							STR0016,;//"Loja De"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
							STR0016,;//"Loja De"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XNOMFO',;
							'12',;
							STR0017,;//"Nome Fornecedor De"
							STR0017,;//"Nome Fornecedor De"
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.F.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XFORNA',;
							'13',;
							STR0018,;//"Fornecedor Ate"
							STR0018,;//"Fornecedor Ate"
							{},;
							'C',;
							'',;
							Nil,;
							'SA2',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XLOJAA',;
							'14',;
							STR0019,;//"Loja Ate"
							STR0019,;//"Loja Ate"
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XNOMFA',;
							'15',;
							STR0020,;//"Nome Fornecedor Ate"
							STR0020,;//"Nome Fornecedor Ate"
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.F.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XPOSTD',;
							'16',;
							STR0021,;//"Posto De"
							STR0021,;//"Posto De"
							{},;
							'C',;
							'',;
							Nil,;
							'G3M',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XNOMPD',;
							'17',;
							STR0022,;//"Nome Posto De"
							STR0022,;//"Nome Posto De"
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.F.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XPOSTA',;
							'18',;
							STR0023,;//"Posto Ate"
							STR0023,;//"Posto Ate"
							{},;
							'C',;
							'',;
							Nil,;
							'G3M',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XNOMPA',;
							'19',;
							STR0024,;//"Nome Posto Ate"
							STR0024,;//"Nome Posto Ate"
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.F.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XCLIDE',;
							'20',;
							STR0025,;//"Cliente De"
							STR0025,;//"Cliente De"
							{},;
							'C',;
							'',;
							Nil,;
							'SA1',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XCLLJD',;
							'21',;
							STR0026,;//"Loja De"
							STR0026,;//"Loja De"
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XCLNDE',;
							'22',;
							STR0027,;//"Nome Cliente De"
							STR0027,;//"Nome Cliente De"
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.F.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XCLIAT',;
							'23',;
							STR0028,;//"Cliente Ate"
							STR0028,;//"Cliente Ate"
							{},;
							'C',;
							'',;
							Nil,;
							'SA1',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XCLLJA',;
							'24',;
							STR0029,;//"Loja Ate"
							STR0029,;//"Loja Ate"
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XCLINA',;
							'25',;
							STR0030,;//"Nome Cliente Ate"
							STR0030,;//"Nome Cliente Ate"
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.F.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XDOC',;
							'26',;
							STR0032,;//"Localizador/Bilhete"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
							STR0032,;//"Localizador/Bilhete"
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)						
	

		oStruct:AddField(	'TMP_XCANC',;
							'27',;
							STR0031,;//"Listar Cancelados"
							STR0031,;//"Listar Cancelados"
							{},;
							'L',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)
							
		oStruct:AddField(	'TMP_XCARDE',;
							'28',;
							STR0033,;//"C�digo Cart�o De"
							STR0033,;//"C�digo Cart�o De"
							{},;
							'C',;
							'',;
							Nil,;
							'G3D',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)
		
	oStruct:AddField(	'TMP_XCARAT',;
							'29',;
							STR0034,;//"C�digo Cart�o At�"
							STR0034,;//"C�digo Cart�o At�"
							{},;
							'C',;
							'',;
							Nil,;
							'G3D',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)
	
	EndIf

Return


/*/{Protheus.doc} AddFldDet
Adiciona os campos referenes a detalhes no cabe�alho no model/view
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function AddFldDet( oStructDet, nOpc, cTipo )

	Local aStruHis	:= {}
	Local nX			:= 0
	
	If cTipo == "DET"
		aStruHis	:= ( oTblDetail:GetAlias() )->( dbStruct() )
		SX3->( dbSetOrder(2) )

		//Adiciona os demais campos da tabela
		For nX := 1 to len( aStruHis )
			If SX3->( dbSeek( aStruHis[nX][1] ) )
	
				If nOpc == 1
					oStructDet:AddField( ; // Ord. Tipo Desc.
											X3Titulo() , ; // [01] C Titulo do campo
											"" 				, ; // [02] C ToolTip do campo
											AllTrim(SX3->X3_CAMPO) , ; // [03] C identificador (ID) do Field
											SX3->X3_TIPO 	, ; // [04] C Tipo do campo
											SX3->X3_TAMANHO 	, ; // [05] N Tamanho do campo
											SX3->X3_DECIMAL 	, ; // [06] N Decimal do campo
											NIL				, ; // [07] B Code-block de valida��o do campo
											NIL 			, ; // [08] B Code-block de valida��o When do campoz
											NIL 			, ; // [09] A Lista de valores permitido do campo
											NIL 			, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
											NIL				, ; // [11] B Code-block de inicializacao do campo
											NIL 			, ; // [12] L Indica se trata de um campo chave
											.F. 			, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
											.F. 			) // [14] L Indica se o campo � virtual
	
	
				ElseIf nOpc == 2
					oStructDet:AddField( ; // Ord. Tipo Desc.
											AllTrim(SX3->X3_CAMPO) 	, ; // [01] C Nome do Campo
											StrZero(nX, 2)			, ; // [02] C Ordem
											X3Titulo()	, ; // [03] C Titulo do campo
											X3Titulo()	, ; // [04] C Descri��o do campo
											NIL			, ; // [05] A Array com Help
											SX3->X3_TIPO 		, ; // [06] C Tipo do campo
											SX3->X3_PICTURE	, ; // [07] C Picture
											NIL 			, ; // [08] B Bloco de Picture Var
											"" 				, ; // [09] C Consulta F3
											.F. 			, ; // [10] L Indica se o campo � edit�vel
											NIL 			, ; // [11] C Pasta do campo
											NIL 			, ; // [12] C Agrupamento do campo
											NIL 			, ; // [13] A Lista de valores permitido do campo (Combo)
											NIL 			, ; // [14] N Tamanho M�ximo da maior op��o do combo
											NIL 			, ; // [15] C Inicializador de Browse
											.F. 			, ; // [16] L Indica se o campo � virtual
											NIL 			) 	// [17] C Picture Vari�vel
	
	
				EndIf
			EndIf
		Next
		
	Else
		//aStruHis	:= ( oTblDetG4C:GetAlias() )->( dbStruct() )
		//Adiciona o campo OK
		If nOpc == 1
			oStructDet:AddField( ; // Ord. Tipo Desc.
								""			, ; // [01] C Titulo do campo
								"" 			, ; // [02] C ToolTip do campo
								"G4C_OK"	, ; // [03] C identificador (ID) do Field
								"L"			, ; // [04] C Tipo do campo
								1			, ; // [05] N Tamanho do campo
								0			, ; // [06] N Decimal do campo
								NIL			, ; // [07] B Code-block de valida��o do campo
								NIL 		, ; // [08] B Code-block de valida��o When do campoz
								NIL 		, ; // [09] A Lista de valores permitido do campo
								NIL 		, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
								NIL			, ; // [11] B Code-block de inicializacao do campo
								NIL 		, ; // [12] L Indica se trata de um campo chave
								.T. 		, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
								.F. 		) // [14] L Indica se o campo � virtual

		oStructDet:AddField( ; // Ord. Tipo Desc.	
								""			, ; // [01] C Titulo do campo
								"" 			, ; // [02] C ToolTip do campo
								"G4C_NUMID"	, ; // [03] C identificador (ID) do Field
								"C"			, ; // [04] C Tipo do campo
								10			, ; // [05] N Tamanho do campo
								0			, ; // [06] N Decimal do campo
								NIL			, ; // [07] B Code-block de valida��o do campo
								NIL 		, ; // [08] B Code-block de valida��o When do campoz
								NIL 		, ; // [09] A Lista de valores permitido do campo
								NIL 		, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
								NIL			, ; // [11] B Code-block de inicializacao do campo
								NIL 		, ; // [12] L Indica se trata de um campo chave
								.T. 		, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
								.F. 		) // [14] L Indica se o campo � virtual
																
	oStructDet:AddField( ; // Ord. Tipo Desc.	
								""			, ; // [01] C Titulo do campo
								"" 			, ; // [02] C ToolTip do campo
								"G4C_IDITEM"	, ; // [03] C identificador (ID) do Field
								"C"			, ; // [04] C Tipo do campo
								4			, ; // [05] N Tamanho do campo
								0			, ; // [06] N Decimal do campo
								NIL			, ; // [07] B Code-block de valida��o do campo
								NIL 		, ; // [08] B Code-block de valida��o When do campoz
								NIL 		, ; // [09] A Lista de valores permitido do campo
								NIL 		, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
								NIL			, ; // [11] B Code-block de inicializacao do campo
								NIL 		, ; // [12] L Indica se trata de um campo chave
								.T. 		, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
								.F. 		) // [14] L Indica se o campo � virtual								
		Else
			oStructDet:AddField( ; // Ord. Tipo Desc.
								"G4C_OK" 	, ; // [01] C Nome do Campo
								"00"		, ; // [02] C Ordem
								""			, ; // [03] C Titulo do campo
								""	, 		; // [04] C Descri��o do campo
								NIL			, ; // [05] A Array com Help
								"L" 		, ; // [06] C Tipo do campo
								NIL			, ; // [07] C Picture
								NIL 		, ; // [08] B Bloco de Picture Var
								"" 			, ; // [09] C Consulta F3
								.T. 		, ; // [10] L Indica se o campo � edit�vel
								NIL 		, ; // [11] C Pasta do campo
								NIL 		, ; // [12] C Agrupamento do campo
								NIL 		, ; // [13] A Lista de valores permitido do campo (Combo)
								NIL 		, ; // [14] N Tamanho M�ximo da maior op��o do combo
								NIL 		, ; // [15] C Inicializador de Browse
								.F. 		, ; // [16] L Indica se o campo � virtual
								NIL 		) 	// [17] C Picture Vari�vel
								
		oStructDet:AddField( ; // Ord. Tipo Desc.
								"G4C_NUMID" 	, ; // [01] C Nome do Campo
								"01"		, ; // [02] C Ordem
								""			, ; // [03] C Titulo do campo
								""	, 		; // [04] C Descri��o do campo
								NIL			, ; // [05] A Array com Help
								"C" 		, ; // [06] C Tipo do campo
								NIL			, ; // [07] C Picture
								NIL 		, ; // [08] B Bloco de Picture Var
								"" 			, ; // [09] C Consulta F3
								.F. 		, ; // [10] L Indica se o campo � edit�vel
								NIL 		, ; // [11] C Pasta do campo
								NIL 		, ; // [12] C Agrupamento do campo
								NIL 		, ; // [13] A Lista de valores permitido do campo (Combo)
								NIL 		, ; // [14] N Tamanho M�ximo da maior op��o do combo
								NIL 		, ; // [15] C Inicializador de Browse
								.F. 		, ; // [16] L Indica se o campo � virtual
								NIL 		) 	// [17] C Picture Vari�vel
																
		oStructDet:AddField( ; // Ord. Tipo Desc.
								"G4C_IDITEM" 	, ; // [01] C Nome do Campo
								"02"		, ; // [02] C Ordem
								""			, ; // [03] C Titulo do campo
								""	, 		; // [04] C Descri��o do campo
								NIL			, ; // [05] A Array com Help
								"C" 		, ; // [06] C Tipo do campo
								NIL			, ; // [07] C Picture
								NIL 		, ; // [08] B Bloco de Picture Var
								"" 			, ; // [09] C Consulta F3
								.F. 		, ; // [10] L Indica se o campo � edit�vel
								NIL 		, ; // [11] C Pasta do campo
								NIL 		, ; // [12] C Agrupamento do campo
								NIL 		, ; // [13] A Lista de valores permitido do campo (Combo)
								NIL 		, ; // [14] N Tamanho M�ximo da maior op��o do combo
								NIL 		, ; // [15] C Inicializador de Browse
								.F. 		, ; // [16] L Indica se o campo � virtual
								NIL 		) 	// [17] C Picture Vari�vel								
	
		EndIf
			
	EndIf
	
Return
/*/{Protheus.doc} AddGatilhos
Adiciona os gatilhos
@type function
@author Simone Mie Sato Kakinoana
@since 07/03/2015
@version 12.1.7
/*/
Static Function AddGatilhos( oStruct )
	Local aAux := {}

	aAux := FwStruTrigger( 'TMP_XFORND', 'TMP_XLOJAD', '"  "', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XFORND', 'TMP_XLOJAD', 'SA2->A2_LOJA', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XLOJAD', 'TMP_XNOMFO', 'Posicione("SA2",1,xFilial("SA2")+FwFldGet("TMP_XFORND")+FwFldGet("TMP_XLOJAD"),"SA2->A2_NOME","")', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XFORNA', 'TMP_XLOJAA', '"  "', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XFORNA', 'TMP_XLOJAA', 'SA2->A2_LOJA', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XLOJAA', 'TMP_XNOMFA', 'Posicione("SA2",1,xFilial("SA2")+FwFldGet("TMP_XFORNA")+FwFldGet("TMP_XLOJAA"),"SA2->A2_NOME")', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XPOSTD', 'TMP_XNOMPD', 'Posicione("G3M",1,xFilial("G3M")+FwFldGet("TMP_XPOSTD"),"G3M->G3M_DESCR")', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XPOSTA', 'TMP_XNOMPA', 'Posicione("G3M",1,xFilial("G3M")+FwFldGet("TMP_XPOSTA"),"G3M->G3M_DESCR")', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XCLIDE', 'TMP_XCLLJD', '"  "', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XCLIDE', 'TMP_XCLLJD', 'SA1->A1_LOJA', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XCLLJD', 'TMP_XCLNDE', 'Posicione("SA1",1,xFilial("SA1")+FwFldGet("TMP_XCLIDE")+FwFldGet("TMP_XCLLJD"),"SA1->A1_NOME")', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XCLIAT', 'TMP_XCLLJA', '"  "', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XCLIAT', 'TMP_XCLLJA', 'SA1->A1_LOJA', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XCLLJA', 'TMP_XCLINA', 'Posicione("SA1",1,xFilial("SA1")+FwFldGet("TMP_XCLIAT")+FwFldGet("TMP_XCLLJA"),"SA1->A1_NOME")', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

Return

/*/{Protheus.doc} AddGroups
Adiciona os grupos
@type function
@author Simone Mie Sato Kakinoan
@since 07/03/2016
@version 12.1.8
/*/

Static Function AddGroups( oStruct )

	oStruct:AddGroup( "GRUPO01", "Cart�o"					, "", 1 )	
	oStruct:AddGroup( "GRUPO02", "Registro de Venda"		, "", 1 )
	oStruct:AddGroup( "GRUPO03", "Emiss�o"					, "", 1 )
	oStruct:AddGroup( "GRUPO04", "Fornecedor"				, "", 1 )
	oStruct:AddGroup( "GRUPO05", "Posto"					, "", 1 )
	oStruct:AddGroup( "GRUPO06", "Cliente"					, "", 1 )
	oStruct:AddGroup( "GRUPO07", "Bilhete/Cancelados", "", 1 )

	oStruct:SetProperty( "TMP_XCARDE" , MVC_VIEW_GROUP_NUMBER, "GRUPO01" )
	oStruct:SetProperty( "TMP_XCARAT" , MVC_VIEW_GROUP_NUMBER, "GRUPO01" )

	oStruct:SetProperty( "TMP_XNURVD" , MVC_VIEW_GROUP_NUMBER, "GRUPO02" )
	oStruct:SetProperty( "TMP_XIDRVD" , MVC_VIEW_GROUP_NUMBER, "GRUPO02" )
	oStruct:SetProperty( "TMP_XSERVD" , MVC_VIEW_GROUP_NUMBER, "GRUPO02" )	
	oStruct:SetProperty( "TMP_XNURVA" , MVC_VIEW_GROUP_NUMBER, "GRUPO02" )
	oStruct:SetProperty( "TMP_XIDRVA" , MVC_VIEW_GROUP_NUMBER, "GRUPO02" )
	oStruct:SetProperty( "TMP_XSERVA" , MVC_VIEW_GROUP_NUMBER, "GRUPO02" )	

	oStruct:SetProperty( "TMP_XEMIDE" , MVC_VIEW_GROUP_NUMBER, "GRUPO03" )
	oStruct:SetProperty( "TMP_XEMIAT" , MVC_VIEW_GROUP_NUMBER, "GRUPO03" )
	oStruct:SetProperty( "TMP_XFORND" , MVC_VIEW_GROUP_NUMBER, "GRUPO04" )
	oStruct:SetProperty( "TMP_XLOJAD" , MVC_VIEW_GROUP_NUMBER, "GRUPO04" )
	oStruct:SetProperty( "TMP_XNOMFO" , MVC_VIEW_GROUP_NUMBER, "GRUPO04" )
	oStruct:SetProperty( "TMP_XFORNA" , MVC_VIEW_GROUP_NUMBER, "GRUPO04" )
	oStruct:SetProperty( "TMP_XLOJAA" , MVC_VIEW_GROUP_NUMBER, "GRUPO04" )
	oStruct:SetProperty( "TMP_XNOMFA" , MVC_VIEW_GROUP_NUMBER, "GRUPO04" )
	oStruct:SetProperty( "TMP_XPOSTD" , MVC_VIEW_GROUP_NUMBER, "GRUPO05" )
	oStruct:SetProperty( "TMP_XNOMPD" , MVC_VIEW_GROUP_NUMBER, "GRUPO05" )
	oStruct:SetProperty( "TMP_XPOSTA" , MVC_VIEW_GROUP_NUMBER, "GRUPO05" )
	oStruct:SetProperty( "TMP_XNOMPA" , MVC_VIEW_GROUP_NUMBER, "GRUPO05" )
	oStruct:SetProperty( "TMP_XCLIDE" , MVC_VIEW_GROUP_NUMBER, "GRUPO06" )
	oStruct:SetProperty( "TMP_XCLLJD" , MVC_VIEW_GROUP_NUMBER, "GRUPO06" )
	oStruct:SetProperty( "TMP_XCLNDE" , MVC_VIEW_GROUP_NUMBER, "GRUPO06" )
	oStruct:SetProperty( "TMP_XCLIAT" , MVC_VIEW_GROUP_NUMBER, "GRUPO06" )
	oStruct:SetProperty( "TMP_XCLLJA" , MVC_VIEW_GROUP_NUMBER, "GRUPO06" )
	oStruct:SetProperty( "TMP_XCLINA" , MVC_VIEW_GROUP_NUMBER, "GRUPO06" )
	oStruct:SetProperty( "TMP_XCANC"  , MVC_VIEW_GROUP_NUMBER, "GRUPO07" )
	oStruct:SetProperty( "TMP_XDOC"   , MVC_VIEW_GROUP_NUMBER, "GRUPO07" )
	
	oStruct:SetProperty( "TMP_XSERVD" , MVC_VIEW_INSERTLINE, .T. ) 
	oStruct:SetProperty( "TMP_XNOMFO" , MVC_VIEW_INSERTLINE, .T. ) 
	oStruct:SetProperty( "TMP_XNOMPD" , MVC_VIEW_INSERTLINE, .T. ) 
	oStruct:SetProperty( "TMP_XCLNDE" , MVC_VIEW_INSERTLINE, .T. )
	 
Return

/*/{Protheus.doc} T061EVldd()
Valida se o cart�o escolhido pertene ao cart�o da concilia��o e se o c�digo de fornecedor � 
  guaal ao c�digo do fornecedor do cart�o.
N�o pode ser marcado, mas � exibido como informa��o para o usu�rio. 
@type function
@author Simone Mie Sato Kakinoan
@since 15/03/2016
@version 12.1.8
/*/
Function T061EVld()

Local oModelAtu			:= FwModelActive()
Local oMdlPesq		 		:= oModelAtu:GetModel("TMP_DETG4C")
Local lRet	:= .T. 

If oMdlPesq:GetValue("G4C_CARTUR") <> G90->G90_CODIGO
	lRet	:= .F.
	Help(" ",1,"T061ENOCARD",,STR0035,3,1) //"N�o � poss�vel selecionar um item financeiro que n�o pertence ao c�digo de cart�o da concilia��o."
Else
	lRet	:= .T.
Endif

If lRet
	If oMdlPesq:GetValue("G4C_CODIGO") <> G90->G90_CODFOR .Or. oMdlPesq:GetValue("G4C_LOJA") <> G90->G90_LJFOR 
		lRet	:= .F.
		Help(" ",1,"T061ENOFORN",,STR0036,3,1) //"N�o � poss�vel selecionar um c�digo/loja de fornecedor diferente do cart�o da concilia��o."
	Else
		lRet	:= .T.
	Endif
Endif

Return(lRet)