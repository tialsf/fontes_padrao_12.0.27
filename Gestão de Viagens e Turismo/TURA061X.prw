#Include "Totvs.ch"
#Include "FWMVCDef.ch"

/*/{Protheus.doc} ModelDef
Modelo de dados para grava��o da G4C
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Static Function ModelDef()
	Local oModel		:= Nil
	Local oStruG4C 	:= FWFormStruct( 1, "G4C", /*bAvalCampo*/, /*lViewUsado*/ )	
		
	oModel := MPFormModel():New( "TURA061X", /*bPreValidacao*/, /*bPosValidacao*/,/*bCommit*/, /*bCancel*/ )
	oModel:AddFields( "G4C_MASTER", /*cOwner*/, oStruG4C )
	
	oModel:SetPrimaryKey({})
	
Return oModel	
