#Include "TURA067.ch"
#Include "Totvs.ch"
#Include "FWMVCDef.ch"
#Include "FWEditPanel.ch" 

Static oTblHeader	:= nil //Tabela tempor�ria com os campos referente a pesquisa
Static oTblDetail	:= nil //Tabela tempor�ria com os campos referente aos DR's encontrados
Static nLinChk      := 1
//------------------------------------------------------------------------------
/*/{Protheus.doc} TURA067()
	Rotina de pequisas de RV

@since		10/06/2015
@version	P12
/*/
//------------------------------------------------------------------------------
Function TURA067()

Local aButtons      := {{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.T.,STR0001},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil}} //"Cancelar"
Local aStruHeader	:= {}
Local aStruDetail	:= {}
Local aDRFields	    := {}
Local nX	        := 0

Private INCLUI     	 := .F.
Private __cPassag    := ""
Private nTotTaxas    := 0
Private oTotTaxas    := Nil
Private nTotExtras   := 0
Private oTotExtras   := Nil
Private cEntAdic     := Space(2)
Private cDescAdic    := ""
Private aModelsIV    := {}
Private nValCliente  := 0
Private nValFornec   := 0
Private nValReceita  := 0
Private oValCliente  := Nil
Private oValFornec   := Nil
Private oValReceita  := Nil
Private nValAltAcor  := 0
Private lWhenUnid    := .T.
Private aDocumento   := {}
Private lHierarquia	:= .F.

SetKey (VK_F12,{|a,b| AcessaPerg("TURA041A",.T.)})	

aDRFields := {"G3Q_FILIAL", "G3Q_NUMID" , "G3Q_IDITEM", "G3Q_NUMSEQ", "G3Q_OPERAC", "G3Q_TPDOC" , "G3Q_DOC"   , ;
			  "G3Q_EMISS" , "G3Q_PROD"  , "G3Q_DESPRD", "G3Q_GRPPRD", "G3Q_DESGRP", "G3Q_CLIENT", "G3Q_LOJA"  , ;
			  "G3Q_NOME"  , "G3R_FORNEC", "G3R_LOJA"  , "G3R_FORREP", "G3R_LOJREP", "G3R_NOMREP", "G3T_CODPAX", ;
			  "G3S_NOME"  , "G3T_NOMPAX", "G3R_LOCGDS", "G3T_LOC"   , "G3T_IDINTE", "G3T_DTSAID", "G3T_DTCHEG", ;
			  "G3Q_FORMPG", "G3Q_DESCFP", "G3Q_ORDER" ,"G3Q_ITOS"   , "G3R_CONCIL", "G3R_IDCONF", "G4D_NSU" }

AAdd(aStruHeader,{"TMP_VAR01","C",TamSX3("G3R_LOCGDS")[1],TamSX3("G3R_LOCGDS")[2]})
AAdd(aStruHeader,{"TMP_VAR02","C",TamSX3("G3T_LOC"   )[1],TamSX3("G3T_LOC"   )[2]})
AAdd(aStruHeader,{"TMP_VAR03","C",TamSX3("G3Q_DOC"   )[1],TamSX3("G3Q_DOC"   )[2]})
AAdd(aStruHeader,{"TMP_VAR04","C",TamSX3("G3Q_DOC"   )[1],TamSX3("G3Q_DOC"   )[2]})
AAdd(aStruHeader,{"TMP_VAR05","C",TamSX3("G3Q_PROD"  )[1],TamSX3("G3Q_PROD"  )[2]})
AAdd(aStruHeader,{"TMP_VAR06","C",TamSX3("G3S_NOME"  )[1],TamSX3("G3S_NOME"  )[2]})
AAdd(aStruHeader,{"TMP_VAR07","C",TamSX3("G3Q_OPERAC")[1],TamSX3("G3Q_OPERAC")[2]})
AAdd(aStruHeader,{"TMP_VAR08","C",TamSX3("G3Q_CLIENT")[1],TamSX3("G3Q_CLIENT")[2]})
AAdd(aStruHeader,{"TMP_VAR09","C",TamSX3("G3Q_LOJA"  )[1],TamSX3("G3Q_LOJA"  )[2]})
AAdd(aStruHeader,{"TMP_VAR10","C",TamSX3("G3Q_NOME"  )[1],TamSX3("G3Q_NOME"  )[2]})
AAdd(aStruHeader,{"TMP_VAR11","C",TamSX3("G3R_FORNEC")[1],TamSX3("G3R_FORNEC")[2]})
AAdd(aStruHeader,{"TMP_VAR12","C",TamSX3("G3R_LOJA"  )[1],TamSX3("G3R_LOJA"  )[2]})
AAdd(aStruHeader,{"TMP_VAR13","C",TamSX3("G3R_NOME"  )[1],TamSX3("G3R_NOME"  )[2]})
AAdd(aStruHeader,{"TMP_VAR14","C",TamSX3("G3R_FORREP")[1],TamSX3("G3R_FORREP")[2]})
AAdd(aStruHeader,{"TMP_VAR15","C",TamSX3("G3R_LOJREP")[1],TamSX3("G3R_LOJREP")[2]})
AAdd(aStruHeader,{"TMP_VAR16","C",TamSX3("G3R_NOMREP")[1],TamSX3("G3R_NOMREP")[2]})
AAdd(aStruHeader,{"TMP_VAR17","D",8,0})
AAdd(aStruHeader,{"TMP_VAR18","D",8,0})
AAdd(aStruHeader,{"TMP_VAR19","D",8,0})
AAdd(aStruHeader,{"TMP_VAR20","D",8,0})
AAdd(aStruHeader,{"TMP_VAR21","D",8,0})
AAdd(aStruHeader,{"TMP_VAR22","D",8,0})
AAdd(aStruHeader,{"TMP_VAR24","C",TamSX3("G3Q_FORMPG")[1],TamSX3("G3Q_FORMPG")[2]})
AAdd(aStruHeader,{"TMP_VAR25","C",TamSX3("G3Q_DESCFP")[1],TamSX3("G3Q_DESCFP")[2]})
AAdd(aStruHeader,{"TMP_VAR26","C",TamSX3("G3Q_ORDER" )[1],TamSX3("G3Q_ORDER" )[2]})
AAdd(aStruHeader,{"TMP_VAR27","C",TamSX3("G3Q_ITOS"  )[1],TamSX3("G3Q_ITOS"  )[2]})
AAdd(aStruHeader,{"TMP_VAR28","C",TamSX3("G3Q_ORDER" )[1],TamSX3("G3Q_ORDER" )[2]})
AAdd(aStruHeader,{"TMP_VAR29","C",TamSX3("G3Q_ITOS"  )[1],TamSX3("G3Q_ITOS"  )[2]})
AAdd(aStruHeader,{"TMP_VAR30","C",TamSX3("G3R_IDCONF")[1],TamSX3("G3R_IDCONF")[2]})
AAdd(aStruHeader,{"TMP_VAR31","C",TamSX3("G4D_NSU")[1]   ,TamSX3("G4D_NSU")[2]})

oTblHeader := FWTemporaryTable():New()

oTblHeader:SetFields( aStruHeader )
oTblHeader:AddIndex( "index1", {"TMP_VAR02"} )
oTblHeader:Create()

AAdd(aStruDetail,{"TMP_ARQ","C",3,0})

SX3->( dbSetOrder(2) )
For nX := 1 to len(aDRFields)
	If SX3->( dbSeek( aDRFields[nX] ) )
		AAdd(aStruDetail,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
	EndIf
Next

oTblDetail := FWTemporaryTable():New()

oTblDetail:SetFields( aStruDetail )
oTblDetail:AddIndex( "index1", {"TMP_ARQ","G3Q_FILIAL","G3Q_NUMID" ,"G3Q_IDITEM","G3Q_NUMSEQ","G3Q_DOC"} )
oTblDetail:Create()

FWExecView(STR0002,"VIEWDEF.TURA067",4,,{|| .T.},,,aButtons,{|| TA67Fechar()}) == 0 //"Pressione <F5> para executar"

Set Key VK_F12 To

Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
	Fun��o respons�vel pela cria��o do modelo de dados.

@since		29/11/2015
@version	P12
/*/
//------------------------------------------------------------------------------
Static Function ModelDef()

Local oStructCab := FWFormModelStruct():New()
Local oStructDet := FWFormModelStruct():New()
Local aAux       := {}
Local aStruHis	 := Nil
Local aFields    := {}
Local nX         := 0
Local oModel     := Nil

If Isincallstack("TURA041")

	 aButtons       := {{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.T.,STR0001},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil}} //"Cancelar"
	 aStruHeader	:= {}
	 aStruDetail	:= {}
	 aDRFields	    := {}
	 nX	        	:= 0
	
	Private INCLUI     	 := .F.
	Private __cPassag    := ""
	Private nTotTaxas    := 0
	Private oTotTaxas    := Nil
	Private nTotExtras   := 0
	Private oTotExtras   := Nil
	Private cEntAdic     := Space(2)
	Private cDescAdic    := ""
	Private aModelsIV    := {}
	Private nValCliente  := 0
	Private nValFornec   := 0
	Private nValReceita  := 0
	Private oValCliente  := Nil
	Private oValFornec   := Nil
	Private oValReceita  := Nil
	Private nValAltAcor  := 0
	Private lWhenUnid    := .T.
	Private aDocumento   := {}
	Private lHierarquia	:= .F.
	
	aDRFields := {"G3Q_FILIAL", "G3Q_NUMID" , "G3Q_IDITEM", "G3Q_NUMSEQ", "G3Q_OPERAC", "G3Q_TPDOC" , "G3Q_DOC"   , ;
				  "G3Q_EMISS" , "G3Q_PROD"  , "G3Q_DESPRD", "G3Q_GRPPRD", "G3Q_DESGRP", "G3Q_CLIENT", "G3Q_LOJA"  , ;
				  "G3Q_NOME"  , "G3R_FORNEC", "G3R_LOJA"  , "G3R_FORREP", "G3R_LOJREP", "G3R_NOMREP", "G3T_CODPAX", ;
				  "G3S_NOME"  , "G3T_NOMPAX", "G3R_LOCGDS", "G3T_LOC"   , "G3T_IDINTE", "G3T_DTSAID", "G3T_DTCHEG", ;
				  "G3Q_FORMPG", "G3Q_DESCFP", "G3Q_ORDER" , "G3Q_ITOS"  , "G3R_CONCIL", "G3R_IDCONF", "G4D_NSU" }
	
	AAdd(aStruHeader,{"TMP_VAR01","C",TamSX3("G3R_LOCGDS")[1],TamSX3("G3R_LOCGDS")[2]})
	AAdd(aStruHeader,{"TMP_VAR02","C",TamSX3("G3T_LOC"   )[1],TamSX3("G3T_LOC"   )[2]})
	AAdd(aStruHeader,{"TMP_VAR03","C",TamSX3("G3Q_DOC"   )[1],TamSX3("G3Q_DOC"   )[2]})
	AAdd(aStruHeader,{"TMP_VAR04","C",TamSX3("G3Q_DOC"   )[1],TamSX3("G3Q_DOC"   )[2]})
	AAdd(aStruHeader,{"TMP_VAR05","C",TamSX3("G3Q_PROD"  )[1],TamSX3("G3Q_PROD"  )[2]})
	AAdd(aStruHeader,{"TMP_VAR06","C",TamSX3("G3S_NOME"  )[1],TamSX3("G3S_NOME"  )[2]})
	AAdd(aStruHeader,{"TMP_VAR07","C",TamSX3("G3Q_OPERAC")[1],TamSX3("G3Q_OPERAC")[2]})
	AAdd(aStruHeader,{"TMP_VAR08","C",TamSX3("G3Q_CLIENT")[1],TamSX3("G3Q_CLIENT")[2]})
	AAdd(aStruHeader,{"TMP_VAR09","C",TamSX3("G3Q_LOJA"  )[1],TamSX3("G3Q_LOJA"  )[2]})
	AAdd(aStruHeader,{"TMP_VAR10","C",TamSX3("G3Q_NOME"  )[1],TamSX3("G3Q_NOME"  )[2]})
	AAdd(aStruHeader,{"TMP_VAR11","C",TamSX3("G3R_FORNEC")[1],TamSX3("G3R_FORNEC")[2]})
	AAdd(aStruHeader,{"TMP_VAR12","C",TamSX3("G3R_LOJA"  )[1],TamSX3("G3R_LOJA"  )[2]})
	AAdd(aStruHeader,{"TMP_VAR13","C",TamSX3("G3R_NOME"  )[1],TamSX3("G3R_NOME"  )[2]})
	AAdd(aStruHeader,{"TMP_VAR14","C",TamSX3("G3R_FORREP")[1],TamSX3("G3R_FORREP")[2]})
	AAdd(aStruHeader,{"TMP_VAR15","C",TamSX3("G3R_LOJREP")[1],TamSX3("G3R_LOJREP")[2]})
	AAdd(aStruHeader,{"TMP_VAR16","C",TamSX3("G3R_NOMREP")[1],TamSX3("G3R_NOMREP")[2]})
	AAdd(aStruHeader,{"TMP_VAR17","D",8,0})
	AAdd(aStruHeader,{"TMP_VAR18","D",8,0})
	AAdd(aStruHeader,{"TMP_VAR19","D",8,0})
	AAdd(aStruHeader,{"TMP_VAR20","D",8,0})
	AAdd(aStruHeader,{"TMP_VAR21","D",8,0})
	AAdd(aStruHeader,{"TMP_VAR22","D",8,0})
	AAdd(aStruHeader,{"TMP_VAR24","C",TamSX3("G3Q_FORMPG")[1],TamSX3("G3Q_FORMPG")[2]})
	AAdd(aStruHeader,{"TMP_VAR25","C",TamSX3("G3Q_DESCFP")[1],TamSX3("G3Q_DESCFP")[2]})
	AAdd(aStruHeader,{"TMP_VAR26","C",TamSX3("G3Q_ORDER" )[1],TamSX3("G3Q_ORDER" )[2]})
	AAdd(aStruHeader,{"TMP_VAR27","C",TamSX3("G3Q_ITOS"  )[1],TamSX3("G3Q_ITOS"  )[2]})
	AAdd(aStruHeader,{"TMP_VAR28","C",TamSX3("G3Q_ORDER" )[1],TamSX3("G3Q_ORDER" )[2]})
	AAdd(aStruHeader,{"TMP_VAR29","C",TamSX3("G3Q_ITOS"  )[1],TamSX3("G3Q_ITOS"  )[2]})
	AAdd(aStruHeader,{"TMP_VAR30","C",TamSX3("G3R_IDCONF")[1],TamSX3("G3R_IDCONF")[2]})
	AAdd(aStruHeader,{"TMP_VAR31","C",TamSX3("G4D_NSU")[1],TamSX3("G4D_NSU")[2]})
	
	oTblHeader := FWTemporaryTable():New()
	
	oTblHeader:SetFields( aStruHeader )
	oTblHeader:AddIndex( "index1", {"TMP_VAR02"} )
	oTblHeader:Create()
	
	AAdd(aStruDetail,{"TMP_ARQ","C",3,0})
	
	SX3->( dbSetOrder(2) )
	For nX := 1 to len(aDRFields)
		If SX3->( dbSeek( aDRFields[nX] ) )
			AAdd(aStruDetail,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		EndIf
	Next
	
	oTblDetail := FWTemporaryTable():New()
	
	oTblDetail:SetFields( aStruDetail )
	oTblDetail:AddIndex( "index1", {"TMP_ARQ","G3Q_FILIAL","G3Q_NUMID" ,"G3Q_IDITEM","G3Q_NUMSEQ","G3Q_DOC"} )
	oTblDetail:Create()
	
	aStruHis	 := (oTblDetail:GetAlias())->(dbStruct())
Else
	aStruHis	 := (oTblDetail:GetAlias())->(dbStruct())
Endif


oStructCab:AddTable(oTblHeader:GetAlias(),,STR0003,{|| oTblHeader:GetRealName()}) //'Pesquisa'
oStructDet:AddTable(oTblDetail:GetAlias(),,STR0004,{|| oTblDetail:GetRealName()}) //'Documentos de reserva'   

oStructCab:AddField(STR0005,"","TMP_VAR01","C",TamSX3("G3R_LOCGDS")[1],TamSX3("G3R_LOCGDS")[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"Localizador GDS"
oStructCab:AddField(STR0006,"","TMP_VAR02","C",TamSX3("G3T_LOC"   )[1],TamSX3("G3T_LOC"   )[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"Localizador Cia. A�rea"
oStructCab:AddField(STR0007,"","TMP_VAR03","C",TamSX3("G3Q_DOC"   )[1],TamSX3("G3Q_DOC"   )[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"Numero do Voucher"	
oStructCab:AddField(STR0008,"","TMP_VAR04","C",TamSX3("G3Q_DOC"   )[1],TamSX3("G3Q_DOC"   )[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"Numero do Bilhete"	
oStructCab:AddField(STR0047,"","TMP_VAR30","C",TamSX3("G3R_IDCONF")[1],TamSX3("G3R_IDCONF")[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"Id. Confirma��o"	
oStructCab:AddField(STR0009,"","TMP_VAR05","C",TamSX3("G3Q_PROD"  )[1],TamSX3("G3Q_PROD"  )[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"Produto"
oStructCab:AddField(STR0010,"","TMP_VAR23","C",TamSX3("G3Q_GRPPRD")[1],TamSX3("G3Q_GRPPRD")[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"Grupo de Produto"
oStructCab:AddField(STR0011,"","TMP_VAR06","C",TamSX3("G3S_NOME"  )[1],TamSX3("G3S_NOME"  )[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"Nome do Passageiro"
oStructCab:AddField(STR0012,"","TMP_VAR07","C",TamSX3("G3Q_OPERAC")[1],TamSX3("G3Q_OPERAC")[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"Opera��o"	
oStructCab:AddField(STR0013,"","TMP_VAR08","C",TamSX3("G3Q_CLIENT")[1],TamSX3("G3Q_CLIENT")[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"C�digo do Cliente"	
oStructCab:AddField(STR0014,"","TMP_VAR09","C",TamSX3("G3Q_LOJA"  )[1],TamSX3("G3Q_LOJA"  )[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"Loja do Cliente"
oStructCab:AddField(STR0015,"","TMP_VAR10","C",TamSX3("G3Q_NOME"  )[1],TamSX3("G3Q_NOME"  )[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"Nome do Cliente"	
oStructCab:AddField(STR0016,"","TMP_VAR11","C",TamSX3("G3R_FORNEC")[1],TamSX3("G3R_FORNEC")[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"C�digo do Fornecedor"
oStructCab:AddField(STR0017,"","TMP_VAR12","C",TamSX3("G3R_LOJA"  )[1],TamSX3("G3R_LOJA"  )[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"Loja do Fornecedor"
oStructCab:AddField(STR0018,"","TMP_VAR13","C",TamSX3("G3R_NOME"  )[1],TamSX3("G3R_NOME"  )[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"Nome do Fornecedor"
oStructCab:AddField(STR0019,"","TMP_VAR14","C",TamSX3("G3R_FORREP")[1],TamSX3("G3R_FORREP")[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"C�digo Forn. Reporte"
oStructCab:AddField(STR0020,"","TMP_VAR15","C",TamSX3("G3R_LOJREP")[1],TamSX3("G3R_LOJREP")[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"Loja do Forn. Reporte"	
oStructCab:AddField(STR0021,"","TMP_VAR16","C",TamSX3("G3R_NOMREP")[1],TamSX3("G3R_NOMREP")[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"Nome do Forn. Reporte"
oStructCab:AddField(STR0022,"","TMP_VAR17","D",8					   ,0					  ,{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"Da Data de Emiss�o"
oStructCab:AddField(STR0023,"","TMP_VAR18","D",8					   ,0					  ,{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"At� a Data de Emiss�o"
oStructCab:AddField(STR0024,"","TMP_VAR19","D",8				  	   ,0					  ,{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"Da Data In"
oStructCab:AddField(STR0025,"","TMP_VAR20","D",8					   ,0					  ,{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"At� a Data In"	
oStructCab:AddField(STR0026,"","TMP_VAR21","D",8					   ,0					  ,{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"Da Data Out"
oStructCab:AddField(STR0027,"","TMP_VAR22","D",8					   ,0					  ,{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )  //"At� a Data Out"
oStructCab:AddField(STR0041,"","TMP_VAR24","C",TamSX3("G3Q_FORMPG")[1],TamSX3("G3Q_FORMPG")[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )   //"Forma Pagto."
oStructCab:AddField(STR0042,"","TMP_VAR25","C",TamSX3("G3Q_DESCFP")[1],TamSX3("G3Q_DESCFP")[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )   //"Desc. Forma Pagto."
oStructCab:AddField(STR0043,"","TMP_VAR26","C",TamSX3("G3Q_ORDER" )[1],TamSX3("G3Q_ORDER" )[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )   //"De OS"
oStructCab:AddField(STR0044,"","TMP_VAR27","C",TamSX3("G3Q_ITOS"  )[1],TamSX3("G3Q_ITOS"  )[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )   //'De Item OS?'
oStructCab:AddField(STR0045,"","TMP_VAR28","C",TamSX3("G3Q_ORDER" )[1],TamSX3("G3Q_ORDER" )[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )   //'At� ordem?'
oStructCab:AddField(STR0046,"","TMP_VAR29","C",TamSX3("G3Q_ITOS"  )[1],TamSX3("G3Q_ITOS"  )[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )   //'At� Item OS?'
oStructCab:AddField(STR0048,"","TMP_VAR31","C",TamSX3("G4D_NSU"   )[1],TamSX3("G4D_NSU"   )[2],{|| .T.},{|| .T.},Nil,.F.,Nil,.F.,.F.,.T. )   //'Apr. Cart�o'
SX3->( dbSetOrder(2) )

oStructDet:AddField("","","TMP_ARQ","C",3,0,Nil,NIL,NIL,NIL,NIL,NIL,.T.,.F.) 

//Adiciona os demais campos da tabela
For nX := 1 to Len( aStruHis )
	If SX3->( dbSeek( aStruHis[nX][1] ) )
		oStructDet:AddField(X3Titulo(),"" ,AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL,NIL,NIL,NIL,NIL,NIL,NIL,.F.,.F.)
	EndIf
Next

oModel := MPFormModel():New( "TURA067", /*bPreValidacao*/, /*bPosValidacao*/,/*bcommit*/, {|oModel| FWFormCancel(oModel),  TA067Destroy() } )

aAux := FwStruTrigger( 'TMP_VAR08', 'TMP_VAR09', 'Posicione("SA1",1,xFilial("SA1")+FwFldGet("TMP_VAR08"),"SA1->A1_LOJA","")', .F. )
oStructCab:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger( 'TMP_VAR08', 'TMP_VAR10', 'Posicione("SA1",1,xFilial("SA1")+FwFldGet("TMP_VAR08"),"SA1->A1_NOME","")', .F. )
oStructCab:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger( 'TMP_VAR09', 'TMP_VAR10', 'Posicione("SA1",1,xFilial("SA1")+FwFldGet("TMP_VAR08")+FwFldGet("TMP_VAR09"),"SA1->A1_NOME","")', .F. )
oStructCab:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger( 'TMP_VAR11', 'TMP_VAR12', 'Posicione("SA2",1,xFilial("SA2")+FwFldGet("TMP_VAR11"),"SA2->A2_LOJA","")', .F. )
oStructCab:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger( 'TMP_VAR11', 'TMP_VAR13', 'Posicione("SA2",1,xFilial("SA2")+FwFldGet("TMP_VAR11"),"SA2->A2_NOME","")', .F. )
oStructCab:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger( 'TMP_VAR12', 'TMP_VAR13', 'Posicione("SA2",1,xFilial("SA2")+FwFldGet("TMP_VAR11")+FwFldGet("TMP_VAR12"),"SA2->A2_NOME","")', .F. )
oStructCab:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger( 'TMP_VAR14', 'TMP_VAR15', 'Posicione("SA2",1,xFilial("SA2")+FwFldGet("TMP_VAR14"),"SA2->A2_LOJA","")', .F. )
oStructCab:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger( 'TMP_VAR14', 'TMP_VAR16', 'Posicione("SA2",1,xFilial("SA2")+FwFldGet("TMP_VAR14"),"SA2->A2_NOME","")', .F. )
oStructCab:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger( 'TMP_VAR15', 'TMP_VAR16', 'Posicione("SA2",1,xFilial("SA2")+FwFldGet("TMP_VAR14")+FwFldGet("TMP_VAR15"),"SA2->A2_NOME","")', .F. )
oStructCab:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger( 'TMP_VAR24', 'TMP_VAR25', 'Posicione("G3N",1,xFilial("G3N")+FwFldGet("TMP_VAR24"),"G3N->G3N_DESCR","")', .F. )
oStructCab:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger( 'TMP_VAR26', 'TMP_VAR28', 'FwFldGet("TMP_VAR26")', .F. )
oStructCab:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

oModel:AddFields( "TMP_MASTER", /*cOwner*/, oStructCab )

oModel:AddGrid("TMP_DETAIL", "TMP_MASTER", oStructDet, /*bLinePre*/, /*bLinePost*/, /*bPre*/, /*bLinePost*/, /*bLoad*/)
oModel:SetRelation( "TMP_DETAIL", { }, ( oTblDetail:GetAlias() )->( IndexKey( 1 ) ) )

oModel:GetModel("TMP_DETAIL"):SetNoInsertLine(.T.)
oModel:GetModel("TMP_DETAIL"):SetNoDeleteLine(.T.)

oModel:GetModel("TMP_MASTER"):SetOnlyQuery(.T.)
oModel:GetModel("TMP_DETAIL"):SetOnlyQuery(.T.)

oModel:GetModel("TMP_DETAIL"):SetOnlyView(.T.)
oModel:GetModel("TMP_DETAIL"):SetOptional(.T.)

oModel:SetPrimaryKey({})

oModel:SetCommit( {|oModel|  TA067Commit(oModel,4) } )

Return oModel

//------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef()
	Montagem da View do MVC

@since		10/06/2015
@version	P12
/*/
//------------------------------------------------------------------------------
Static Function ViewDef()

Local oModel   		:= FWLoadModel( "TURA067" )
Local oView			:= FWFormView():New()
Local oStructCab	:= FWFormViewStruct():New()
Local oStructDet 	:= FWFormViewStruct():New()
Local aStruHis	    := (oTblDetail:GetAlias())->(dbStruct())
Local aFields		:= {}
Local nX			:= 0

oView:SetModel( oModel )

oStructCab:AddField("TMP_VAR01","01",STR0005,STR0005,{},"C","",NIL,""    ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3R_LOCGDS
oStructCab:AddField("TMP_VAR02",'02',STR0006,STR0006,{},"C","",NIL,""    ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3T_LOC
oStructCab:AddField("TMP_VAR03",'03',STR0007,STR0007,{},"C","",NIL,""    ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3Q_DOC Quando G3Q_TPDOC == '1'   
oStructCab:AddField("TMP_VAR04",'04',STR0008,STR0008,{},"C","",NIL,""    ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3Q_DOC Quando G3Q_TPDOC == '2' OU '8'   
oStructCab:AddField("TMP_VAR30",'05',STR0047,STR0047,{},"C","",NIL,"" 	 ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3R_IDCONF //"Id. Confirma��o"
oStructCab:AddField("TMP_VAR05",'06',STR0009,STR0009,{},"C","",NIL,"SB1" ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3Q_PROD
oStructCab:AddField("TMP_VAR23",'07',STR0010,STR0010,{},"C","",NIL,"SBM" ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3Q_PROD
oStructCab:AddField("TMP_VAR06",'08',STR0011,STR0011,{},"C","",NIL,""    ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3S_NOME
oStructCab:AddField("TMP_VAR07","09",STR0012,STR0012,{},"C","",NIL,""    ,.T.,NIL,NIL,{STR0028,STR0029,STR0030,STR0031,"          "},NIL,NIL,.T.,NIL) //G3Q_OPERAC (1=Emissao;2=Reembolso;3=Reemiss�o;4=Servi�o Proprio)
oStructCab:AddField("TMP_VAR08",'10',STR0013,STR0013,{},"C","",NIL,"SA1" ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3Q_CLIENT
oStructCab:AddField("TMP_VAR09",'11',STR0014,STR0014,{},"C","",NIL,""    ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3Q_LOJA
oStructCab:AddField("TMP_VAR10",'12',STR0015,STR0015,{},"C","",NIL,""    ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) 
oStructCab:AddField("TMP_VAR11",'13',STR0016,STR0016,{},"C","",NIL,"G4TA",.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3R_FORNEC
oStructCab:AddField("TMP_VAR12","14",STR0017,STR0017,{},"C","",NIL,""    ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3R_LOJA
oStructCab:AddField("TMP_VAR13",'15',STR0018,STR0018,{},"C","",NIL,""    ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL)
oStructCab:AddField("TMP_VAR14",'16',STR0019,STR0019,{},"C","",NIL,"SA2A",.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3R_FORREP
oStructCab:AddField("TMP_VAR15",'17',STR0020,STR0020,{},"C","",NIL,""    ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3R_LOJREP
oStructCab:AddField("TMP_VAR16",'18',STR0021,STR0021,{},"C","",NIL,""    ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL)
oStructCab:AddField("TMP_VAR17","19",STR0022,STR0022,{},"D","",NIL,""    ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3Q_EMISS
oStructCab:AddField("TMP_VAR18",'20',STR0023,STR0023,{},"D","",NIL,""    ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3Q_EMISS
oStructCab:AddField("TMP_VAR19",'21',STR0024,STR0024,{},"D","",NIL,""    ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3T_DTSAID,   G3U_DTINI,   G3V_DTINI,   G3W_DTINI,   G3X_DTINI,   G3Y_DTINI,   G3Z_DTINI,   G40_DTINI,   G41_DTINI,   G43_DTINI
oStructCab:AddField("TMP_VAR20",'22',STR0025,STR0025,{},"D","",NIL,""    ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3T_DTSAID,   G3U_DTINI,   G3V_DTINI,   G3W_DTINI,   G3X_DTINI,   G3Y_DTINI,   G3Z_DTINI,   G40_DTINI,   G41_DTINI,   G43_DTINI
oStructCab:AddField("TMP_VAR21","23",STR0026,STR0026,{},"D","",NIL,""    ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3T_DTCHEG,   G3U_DTFIM,   G3V_DTFIM,   G3W_DTFIM,   G3X_DTFIM,   G3Y_DTFIM,   G3Z_DTFIM,   G40_DTFIM,   G41_DTFIM,   G43_DTFIM
oStructCab:AddField("TMP_VAR22",'24',STR0027,STR0027,{},"D","",NIL,""    ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3T_DTCHEG,   G3U_DTFIM,   G3V_DTFIM,   G3W_DTFIM,   G3X_DTFIM,   G3Y_DTFIM,   G3Z_DTFIM,   G40_DTFIM,   G41_DTFIM,   G43_DTFIM
																								 	      //AEREO         HOTEL        CARRO        RODOVIARIO   TREM         CRUZEIRO     PACOTE       TOUR         SEGURO       OUTROS
oStructCab:AddField("TMP_VAR24",'25',STR0041,STR0041,{},"C","",NIL,"G3N" ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3Q_FORMPG //"Forma Pagto."
oStructCab:AddField("TMP_VAR25",'26',STR0042,STR0042,{},"C","",NIL,"" 	 ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3Q_DESCFP //"Desc. Forma Pagto."

oStructCab:AddField("TMP_VAR26",'27',STR0043,STR0043,{},"C","",NIL,"" 	 ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3Q_DESCFP //'De OS'
oStructCab:AddField("TMP_VAR27",'28',STR0044,STR0044,{},"C","",NIL,"" 	 ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3Q_DESCFP //'De Item OS'
oStructCab:AddField("TMP_VAR28",'29',STR0045,STR0045,{},"C","",NIL,"" 	 ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3Q_DESCFP //'At� OS'
oStructCab:AddField("TMP_VAR29",'30',STR0046,STR0046,{},"C","",NIL,"" 	 ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G3Q_DESCFP //'At� Item OS'
oStructCab:AddField("TMP_VAR31",'31',STR0048,STR0048,{},"C","",NIL,"" 	 ,.T.,NIL,NIL,{},NIL,NIL,.T.,NIL) //G4D_NSU //'Apr. Cart�o'

SX3->( dbSetOrder(2) )

//Adiciona os demais campos da tabela
For nX := 1 to len( aStruHis )
	If SX3->( dbSeek( aStruHis[nX][1] ) )
		oStructDet:AddField(AllTrim(SX3->X3_CAMPO),StrZero(nX, 2),X3Titulo(),X3Titulo(),NIL,SX3->X3_TIPO,SX3->X3_PICTURE,NIL,"",.F.,NIL,NIL,NIL,NIL,NIL,.F.,NIL) 
	EndIf
Next

oStructCab:SetProperty( "TMP_VAR04" , MVC_VIEW_INSERTLINE, .T. )
oStructCab:SetProperty( "TMP_VAR07" , MVC_VIEW_INSERTLINE, .T. )
oStructCab:SetProperty( "TMP_VAR10" , MVC_VIEW_INSERTLINE, .T. )
oStructCab:SetProperty( "TMP_VAR13" , MVC_VIEW_INSERTLINE, .T. )
oStructCab:SetProperty( "TMP_VAR16" , MVC_VIEW_INSERTLINE, .T. )
oStructCab:SetProperty( "TMP_VAR18" , MVC_VIEW_INSERTLINE, .T. )
oStructCab:SetProperty( "TMP_VAR20" , MVC_VIEW_INSERTLINE, .T. )

oView:AddField("VIEW_TMPM", oStructCab, "TMP_MASTER" )
oView:AddGrid( "VIEW_TMPD", oStructDet, "TMP_DETAIL" )

oView:AddUserButton(STR0032,"CLIPS",{||FwMsgRun(,{||TA067Qry(), nLinChk := 0      },,STR0033 ) },,VK_F5 ) //'<F5> Pesquisar'###"Pesquisando itens de venda..."
oView:AddUserButton(STR0034,"CLIPS",{||FwMsgRun(,{||TA067Commit(oModel,4)},,STR0035 ) } ,,VK_F6) //"<F6> Alterar"#"Aguarde..."
oView:AddUserButton(STR0036,"CLIPS",{||FwMsgRun(,{||TA067Commit(oModel,2)},,STR0035 ) } ,,VK_F10) //"<F10> Visualizar"#"Aguarde..."

//STR0032 "<F5> Pesquisar"
//STR0034 "<F6> Alterar" -- (oModel,4)
//STR0036 "<F10> Visualizar" -- (oModel,2)

Pergunte("TURA041A",.F.)

If !Empty(MV_PAR01)
	TA41ViewOrder(oStructDet,"TURA041","TMP_DETAIL", MV_PAR01)
Endif

oView:CreateHorizontalBox( "SUPERIOR"	, 68 )
oView:CreateHorizontalBox( "INFERIOR"	, 32 )

oView:SetOwnerView( "VIEW_TMPM"	, "SUPERIOR" )
oView:SetOwnerView( "VIEW_TMPD"	, "INFERIOR" )

oView:showInsertMsg( .F. )

Return oView


//------------------------------------------------------------------------------
/*/{Protheus.doc} TA067Qry()
	Query que efetua o filtro dos RVs pesquisados

@since		10/06/2015
@version	P12
/*/
//------------------------------------------------------------------------------
Static Function TA067Qry()

Local cAliasQry		:= GetNextAlias()
Local cQuery        := ""
Local cSelect       := ""
Local cSelectSeg    := ""
Local cFrom         := ""
Local cFromSeg      := ""
Local cWhere        := ""
Local cWhereSeg     := ""
Local cFilterG3P	:= ""
Local cFilterG3Q	:= ""
Local cFilterG3R	:= ""
Local cFilterG3S	:= ""
Local cFilterG3T	:= ""
Local cFilterG3U	:= ""
Local cFilterG3V	:= ""
Local cFilterG3W	:= ""
Local cFilterG3X	:= ""
Local cFilterG3Y	:= ""
Local cFilterG3Z	:= ""
Local cFilterG40	:= ""
Local cFilterG41	:= ""
Local cFilterG42	:= ""
Local cFilterG43	:= ""
Local cFilterG4D    := ""
Local cFilterQry    := ""

Local cVarChk       := ""
Local cFieldChk     := ""
Local lNewLine		:= .F.
Local nX			:= 0
Local nR            := 0
Local oModel		:= FwModelActive()
Local oView 		:= FwViewActive()
Local oModelMaster	:= oModel:GetModel("TMP_MASTER")
Local oModelDetail	:= oModel:GetModel("TMP_DETAIL")
Local oStruct       := oModelMaster:GetStruct()
Local aCampos       := oStruct:GetFields()
Local aUsrDados     := {"","","","",""}
Local cPosto        := ""
Local cTpCorp       := ""
Local cTpEvento     := ""
Local cTpLazer      := ""
Local lSeg01        := .F.
Local lSeg02        := .F.
Local lSeg03        := .F.

TURA67HierAces(__cUserId,@aUsrDados)

cTpCorp   := aUsrDados[3]
cTpEvento := aUsrDados[4]
cTpLazer  := aUsrDados[5]

If cTpCorp == "1"
	cFilterG3P += "G3P.G3P_SEGNEG = '1' "
ElseIf cTpCorp == "2"
	cPosto    := TURA67Postos(aUsrDados[2],"1")
	If !Empty(cPosto)
		cFilterG3P += "(G3P.G3P_SEGNEG = '1' AND G3P.G3P_POSTO IN "+cPosto+") "
	Else
		lSeg01 := .T.
	EndIf
Else
	lSeg01 := .T.
EndIf

If cTpEvento == "1"
	If !Empty(cFilterG3P)
		cFilterG3P += " OR "
	EndIf 
	cFilterG3P += "G3P.G3P_SEGNEG = '2' "
ElseIf cTpEvento == "2"
	cPosto    := TURA67Postos(aUsrDados[2],"2")
	If !Empty(cPosto)
		If !Empty(cFilterG3P)
			cFilterG3P += " OR "
		EndIf 
		cFilterG3P += "(G3P.G3P_SEGNEG = '2' AND G3P.G3P_POSTO IN "+cPosto+") "
	Else
		lSeg02 := .T.
	EndIf
Else
	lSeg02 := .T.
EndIf

If cTpLazer == "1"
	If !Empty(cFilterG3P)
		cFilterG3P += " OR "
	EndIf 
	cFilterG3P += "G3P.G3P_SEGNEG = '3' "
ElseIf cTpLazer == "2"
	cPosto    := TURA67Postos(aUsrDados[2],"3")
	If !Empty(cPosto)
		If !Empty(cFilterG3P)
			cFilterG3P += " OR "
		EndIf 
		cFilterG3P += "(G3P.G3P_SEGNEG = '3' AND G3P.G3P_POSTO IN "+cPosto+") "
	Else
		lSeg03 := .T.
	EndIf
Else //If cTpLazer == "3"
	lSeg03 := .T.
EndIf

If !Empty(cFilterG3P)
	cFilterG3P := " ( "+cFilterG3P+" ) AND "
EndIf 

If lSeg01
	cFilterG3P += "G3P.G3P_SEGNEG <> '1' AND "
EndIf
If lSeg02
	cFilterG3P += "G3P.G3P_SEGNEG <> '2' AND "
EndIf
If lSeg03
	cFilterG3P += "G3P.G3P_SEGNEG <> '3' AND "
EndIf

oModel:GetModel("TMP_DETAIL"):SetNoInsertLine(.F.)
oModel:GetModel("TMP_DETAIL"):SetNoDeleteLine(.F.)
oModelDetail:ClearData()

For nx:=1 To Len(aCampos)

	cFieldChk := aCampos[nx,3]
	cVarChk   := oModel:GetModel("TMP_MASTER"):GetValue(cFieldChk)
	
	If !Empty(cVarChk)  
		If   cFieldChk == "TMP_VAR01" //G3R_LOCGDS
			cFilterG3R += "UPPER(G3R.G3R_LOCGDS) LIKE '%"+AllTrim(Upper(cVarChk))+"%' AND "
			
		ElseIf cFieldChk == "TMP_VAR02" //G3T_LOC
			cFilterQRY += "UPPER(SUBQRY.LOC) LIKE '%"+AllTrim(Upper(cVarChk))+"%' AND "
			
		ElseIf cFieldChk == "TMP_VAR03" //G3Q_DOC Quando G3Q_TPDOC == '1'
			cFilterG3Q += "UPPER(G3Q.G3Q_DOC) LIKE '%"+AllTrim(Upper(cVarChk))+"%' AND G3Q_TPDOC = '1' AND "
			
		ElseIf cFieldChk == "TMP_VAR04" //G3Q_DOC Quando G3Q_TPDOC == '2' OU '8'
			cFilterG3Q += "UPPER(G3Q.G3Q_DOC) LIKE '%"+AllTrim(Upper(cVarChk))+"%' AND G3Q_TPDOC IN ('2','8') AND "
			
		ElseIf cFieldChk == "TMP_VAR05" //G3Q_PROD
			cFilterG3Q += "G3Q.G3Q_PROD = '"+cVarChk+"' AND "
			
		ElseIf cFieldChk == "TMP_VAR06" //G3S_NOME
			cFilterG3S += "UPPER(G3S.G3S_NOME) LIKE '%"+AllTrim(Upper(cVarChk))+"%' AND "
			
		ElseIf cFieldChk == "TMP_VAR07" //G3Q_OPERAC (1=Emissao;2=Reembolso;3=Reemiss�o;4=Servi�o Proprio)
			cFilterG3Q += "G3Q.G3Q_OPERAC = '"+cVarChk+"' AND "
			
		ElseIf cFieldChk == "TMP_VAR08" //G3Q_CLIENT
			cFilterG3Q += "G3Q.G3Q_CLIENT = '"+cVarChk+"' AND "
			
		ElseIf cFieldChk == "TMP_VAR09" //G3Q_LOJA
			cFilterG3Q += "G3Q.G3Q_LOJA = '"+cVarChk+"' AND "
			
		ElseIf cFieldChk == "TMP_VAR10" //G3Q_NOME 
			cFilterG3Q += "UPPER(G3Q.G3Q_NOME) LIKE '%"+AllTrim(Upper(cVarChk))+"%' AND "
			
		ElseIf cFieldChk == "TMP_VAR11" //G3R_FORNEC
			cFilterG3R += "G3R.G3R_FORNEC = '"+cVarChk+"' AND "
			
		ElseIf cFieldChk == "TMP_VAR12" //G3R_LOJA
			cFilterG3R += "G3R.G3R_LOJA = '"+cVarChk+"' AND "
			
		ElseIf cFieldChk == "TMP_VAR14" //G3R_FORREP
			cFilterG3R += "G3R.G3R_FORREP = '"+cVarChk+"' AND "
			
		ElseIf cFieldChk == "TMP_VAR15" //G3R_LOJREP
			cFilterG3R += "G3R.G3R_LOJREP = '"+cVarChk+"' AND "
			
		ElseIf cFieldChk == "TMP_VAR16" //GER_NOMREP 
			cFilterG3R += "UPPER(G3R.G3R_NOMREP) LIKE '%"+AllTrim(Upper(cVarChk))+"%' AND "
			
		ElseIf cFieldChk == "TMP_VAR17" //G3Q_EMISS
			cFilterG3Q += "G3Q.G3Q_EMISS >= '"+DtoS(cVarChk)+"' AND "
			
		ElseIf cFieldChk == "TMP_VAR18" //G3Q_EMISS
			cFilterG3Q += "G3Q.G3Q_EMISS <= '"+DtoS(cVarChk)+"' AND "
			
		ElseIf cFieldChk == "TMP_VAR19" //G3T_DTSAID,   G3U_DTINI,   G3V_DTINI,   G3W_DTINI,   G3X_DTINI,   G3Y_DTINI,   G3Z_DTINI,   G40_DTINI,   G41_DTINI,   G43_DTINI
			cFilterQry += "DTINI >= '"+DtoS(cVarChk)+"' AND "
				
		ElseIf cFieldChk == "TMP_VAR20" //G3T_DTSAID,   G3U_DTINI,   G3V_DTINI,   G3W_DTINI,   G3X_DTINI,   G3Y_DTINI,   G3Z_DTINI,   G40_DTINI,   G41_DTINI,   G43_DTINI
			cFilterQry += "DTINI <= '"+DtoS(cVarChk)+"' AND "
			
		ElseIf cFieldChk == "TMP_VAR21" //G3T_DTCHEG,   G3U_DTFIM,   G3V_DTFIM,   G3W_DTFIM,   G3X_DTFIM,   G3Y_DTFIM,   G3Z_DTFIM,   G40_DTFIM,   G41_DTFIM,   G43_DTFIM
			cFilterQry += "DTFIM >= '"+DtoS(cVarChk)+"' AND "
			
		ElseIf cFieldChk == "TMP_VAR22" //G3T_DTCHEG,   G3U_DTFIM,   G3V_DTFIM,   G3W_DTFIM,   G3X_DTFIM,   G3Y_DTFIM,   G3Z_DTFIM,   G40_DTFIM,   G41_DTFIM,   G43_DTFIM
			cFilterQry += "DTFIM <= '"+DtoS(cVarChk)+"' AND "

		ElseIf cFieldChk == "TMP_VAR23" //G3T_DTCHEG,   G3U_DTFIM,   G3V_DTFIM,   G3W_DTFIM,   G3X_DTFIM,   G3Y_DTFIM,   G3Z_DTFIM,   G40_DTFIM,   G41_DTFIM,   G43_DTFIM
			cFilterG3Q += "G3Q.G3Q_GRPPRD = '"+cVarChk+"' AND "

		ElseIf cFieldChk == "TMP_VAR24" //G3Q_FORMPG
			cFilterG3Q += "G3Q.G3Q_FORMPG = '"+cVarChk+"' AND "

		ElseIf cFieldChk == "TMP_VAR26" //G3Q_ORDER --> DE OS
			cFilterG3Q += "G3Q.G3Q_ORDER >= '"+cVarChk+"' AND "

		ElseIf cFieldChk == "TMP_VAR27" //G3Q_ITOS --> DE ITEM OS
			cFilterG3Q += "G3Q.G3Q_ITOS >= '"+cVarChk+"' AND "

		ElseIf cFieldChk == "TMP_VAR28" //G3Q_ORDER --> AT� OS
			cFilterG3Q += "G3Q.G3Q_ORDER <= '"+cVarChk+"' AND "

		ElseIf cFieldChk == "TMP_VAR29" //G3Q_ITOS --> AT� ITEM OS
			cFilterG3Q += "G3Q.G3Q_ITOS <= '"+cVarChk+"' AND "

		ElseIf cFieldChk == "TMP_VAR30" //G3R_IDCONF
			cFilterG3R += "G3R.G3R_IDCONF = '"+cVarChk+"' AND "
		
		ElseIf cFieldChk == "TMP_VAR31" //G4D_NSU --> Apr. Cart�o
			cFilterG4D += "G4D_NSU = '"+cVarChk+"' AND " 
			
		EndIf 
	EndIf
Next

cFilterG3P	+= "G3P.D_E_L_E_T_ = ' ' "
cFilterG3Q	+= "G3Q.G3Q_CONINU = ' ' AND G3Q.D_E_L_E_T_ = ' ' "
cFilterG3R	+= "G3R.G3R_CONINU = ' ' AND G3R.D_E_L_E_T_ = ' ' "
cFilterG3S	+= "G3S.G3S_CONINU = ' ' AND G3S.D_E_L_E_T_ = ' ' "
cFilterG3T	+= "G3T.G3T_CONINU = ' ' AND G3T.D_E_L_E_T_ = ' ' "
cFilterG3U	+= "G3U.G3U_CONINU = ' ' AND G3U.D_E_L_E_T_ = ' ' "
cFilterG3V	+= "G3V.G3V_CONINU = ' ' AND G3V.D_E_L_E_T_ = ' ' "
cFilterG3W	+= "G3W.G3W_CONINU = ' ' AND G3W.D_E_L_E_T_ = ' ' "
cFilterG3X	+= "G3X.G3X_CONINU = ' ' AND G3X.D_E_L_E_T_ = ' ' "
cFilterG3Y	+= "G3Y.G3Y_CONINU = ' ' AND G3Y.D_E_L_E_T_ = ' ' "
cFilterG3Z	+= "G3Z.G3Z_CONINU = ' ' AND G3Z.D_E_L_E_T_ = ' ' "
cFilterG40	+= "G40.G40_CONINU = ' ' AND G40.D_E_L_E_T_ = ' ' "
cFilterG41	+= "G41.G41_CONINU = ' ' AND G41.D_E_L_E_T_ = ' ' "
cFilterG42	+= "G42.G42_CONINU = ' ' AND G42.D_E_L_E_T_ = ' ' "
cFilterG43	+= "G43.G43_CONINU = ' ' AND G43.D_E_L_E_T_ = ' ' "
cFilterG4D  += "G4D.G4D_CONINU = ' ' AND G4D.D_E_L_E_T_ = ' ' "


cFilterQry := Substr(cFilterQry,1,RAt("AND",cFilterQry)-1) 

cQuery := "SELECT DISTINCT * FROM ("
cQuery += "SELECT  "
cQuery += "G3Q.G3Q_FILIAL,G3Q.G3Q_NUMID,G3Q.G3Q_IDITEM,G3Q.G3Q_NUMSEQ,G3Q.G3Q_OPERAC,G3Q.G3Q_TPDOC ,G3Q.G3Q_DOC   , "
cQuery += "G3Q.G3Q_EMISS,G3Q.G3Q_CLIENT,G3Q.G3Q_LOJA  ,G3Q.G3Q_NOME  ,G3Q.G3Q_PROD  ,G3Q.G3Q_DESPRD,G3Q.G3Q_GRPPRD, "
cQuery += "G3Q.G3Q_DESGRP,G3R.G3R_FORNEC,G3R.G3R_LOJA ,G3R.G3R_FORREP,G3R.G3R_LOJREP,G3R.G3R_NOMREP,G3S.G3S_NOME  , "
cQuery += "G3R.G3R_LOCGDS,G3Q.G3Q_FORMPG,G3Q.G3Q_DESCFP,G3Q_ORDER,G3Q_ITOS,G3R.G3R_CONCIL,G3R.G3R_IDCONF,G4D_NSU,"
cQuery += "CASE  "
cQuery += "	WHEN G3T.G3T_LOC IS NOT NULL THEN G3T.G3T_LOC " 
cQuery += "	ELSE '  ' "
cQuery += "END "
cQuery += "AS LOC, "
cQuery += "CASE  "
cQuery += "	WHEN G3T.G3T_CODPAX IS NOT NULL THEN G3T.G3T_CODPAX " 
cQuery += "	WHEN G3U.G3U_CODPAX IS NOT NULL THEN G3U.G3U_CODPAX "
cQuery += "	WHEN G3V.G3V_CODPAX IS NOT NULL THEN G3V.G3V_CODPAX "
cQuery += "	WHEN G3W.G3W_CODPAX IS NOT NULL THEN G3W.G3W_CODPAX "
cQuery += "	WHEN G3X.G3X_CODPAX IS NOT NULL THEN G3X.G3X_CODPAX "
cQuery += "	WHEN G3Y.G3Y_CODPAX IS NOT NULL THEN G3Y.G3Y_CODPAX "
cQuery += "	WHEN G3Z.G3Z_CODPAX IS NOT NULL THEN G3Z.G3Z_CODPAX "
cQuery += "	WHEN G40.G40_CODPAX IS NOT NULL THEN G40.G40_CODPAX "
cQuery += "	WHEN G41.G41_CODPAX IS NOT NULL THEN G41.G41_CODPAX "
cQuery += "	WHEN G42.G42_CODPAX IS NOT NULL THEN G42.G42_CODPAX "
cQuery += "	WHEN G43.G43_CODPAX IS NOT NULL THEN G43.G43_CODPAX "
cQuery += "	ELSE '  ' "
cQuery += "END "
cQuery += "AS CODPAX, "
cQuery += "CASE "
cQuery += "	WHEN G3T.G3T_NOMPAX IS NOT NULL THEN G3T.G3T_NOMPAX "
cQuery += "	WHEN G3U.G3U_NOMPAX IS NOT NULL THEN G3U.G3U_NOMPAX "
cQuery += "	WHEN G3V.G3V_NOMPAX IS NOT NULL THEN G3V.G3V_NOMPAX "
cQuery += "	WHEN G3W.G3W_NOMPAX IS NOT NULL THEN G3W.G3W_NOMPAX "
cQuery += "	WHEN G3X.G3X_NOMPAX IS NOT NULL THEN G3X.G3X_NOMPAX "
cQuery += "	WHEN G3Y.G3Y_NOMPAX IS NOT NULL THEN G3Y.G3Y_NOMPAX " 
cQuery += "	WHEN G3Z.G3Z_NOMPAX IS NOT NULL THEN G3Z.G3Z_NOMPAX "
cQuery += "	WHEN G40.G40_NOMPAX IS NOT NULL THEN G40.G40_NOMPAX "
cQuery += "	WHEN G41.G41_NOMPAX IS NOT NULL THEN G41.G41_NOMPAX "
cQuery += "	WHEN G42.G42_NOMPAX IS NOT NULL THEN G42.G42_NOMPAX " 
cQuery += "	WHEN G43.G43_NOMPAX IS NOT NULL THEN G43.G43_NOMPAX "
cQuery += "	ELSE '  ' "
cQuery += "END "	
cQuery += "AS NOMPAX, "
cQuery += "CASE " 
cQuery += "	WHEN G3T.G3T_IDINTE IS NOT NULL THEN G3T.G3T_IDINTE "
cQuery += "	WHEN G3U.G3U_IDINTE IS NOT NULL THEN G3U.G3U_IDINTE "
cQuery += "	WHEN G3V.G3V_IDINTE IS NOT NULL THEN G3V.G3V_IDINTE "
cQuery += "	WHEN G3W.G3W_IDINTE IS NOT NULL THEN G3W.G3W_IDINTE "
cQuery += "	WHEN G3X.G3X_IDINTE IS NOT NULL THEN G3X.G3X_IDINTE "
cQuery += "	WHEN G3Y.G3Y_IDINTE IS NOT NULL THEN G3Y.G3Y_IDINTE "
cQuery += "	WHEN G3Z.G3Z_IDINTE IS NOT NULL THEN G3Z.G3Z_IDINTE "
cQuery += "	WHEN G40.G40_IDINTE IS NOT NULL THEN G40.G40_IDINTE "
cQuery += "	WHEN G41.G41_IDINTE IS NOT NULL THEN G41.G41_IDINTE "
cQuery += "	WHEN G42.G42_IDINTE IS NOT NULL THEN G42.G42_IDINTE "
cQuery += "	WHEN G43.G43_IDINTE IS NOT NULL THEN G43.G43_IDINTE "
cQuery += "	ELSE '  ' " 
cQuery += "END "	
cQuery += "AS IDINTE, "
cQuery += "CASE " 
cQuery += "	WHEN G3T.G3T_DTSAID IS NOT NULL THEN G3T.G3T_DTSAID "
cQuery += "	WHEN G3U.G3U_DTINI  IS NOT NULL THEN G3U.G3U_DTINI "
cQuery += "	WHEN G3V.G3V_DTINI  IS NOT NULL THEN G3V.G3V_DTINI "
cQuery += "	WHEN G3W.G3W_DTINI  IS NOT NULL THEN G3W.G3W_DTINI "
cQuery += "	WHEN G3X.G3X_DTINI  IS NOT NULL THEN G3X.G3X_DTINI "
cQuery += "	WHEN G3Y.G3Y_DTINI  IS NOT NULL THEN G3Y.G3Y_DTINI "
cQuery += "	WHEN G3Z.G3Z_DTINI  IS NOT NULL THEN G3Z.G3Z_DTINI "
cQuery += "	WHEN G40.G40_DTINI  IS NOT NULL THEN G40.G40_DTINI "
cQuery += "	WHEN G41.G41_DTINI  IS NOT NULL THEN G41.G41_DTINI "
cQuery += "	WHEN G43.G43_DTINI  IS NOT NULL THEN G43.G43_DTINI "
cQuery += "	ELSE '  ' " 
cQuery += "END "	
cQuery += "AS DTINI, "
cQuery += "CASE " 
cQuery += "	WHEN G3T.G3T_DTCHEG IS NOT NULL THEN G3T.G3T_DTCHEG "
cQuery += "	WHEN G3U.G3U_DTFIM  IS NOT NULL THEN G3U.G3U_DTFIM "
cQuery += "	WHEN G3V.G3V_DTFIM  IS NOT NULL THEN G3V.G3V_DTFIM "
cQuery += "	WHEN G3W.G3W_DTFIM  IS NOT NULL THEN G3W.G3W_DTFIM "
cQuery += "	WHEN G3X.G3X_DTFIM  IS NOT NULL THEN G3X.G3X_DTFIM "
cQuery += "	WHEN G3Y.G3Y_DTFIM  IS NOT NULL THEN G3Y.G3Y_DTFIM "
cQuery += "	WHEN G3Z.G3Z_DTFIM  IS NOT NULL THEN G3Z.G3Z_DTFIM "
cQuery += "	WHEN G40.G40_DTFIM  IS NOT NULL THEN G40.G40_DTFIM "
cQuery += "	WHEN G41.G41_DTFIM  IS NOT NULL THEN G41.G41_DTFIM "
cQuery += "	WHEN G43.G43_DTFIM  IS NOT NULL THEN G43.G43_DTFIM "
cQuery += "	ELSE '  ' " 
cQuery += "END "	
cQuery += "AS DTFIM "

cQuery += "FROM "
cQuery += RetSqlName("G3P")+" G3P " 

cQuery += "Inner Join "+RetSqlName("G3Q")+" G3Q ON "
cQuery += "G3Q.G3Q_FILIAL = G3P.G3P_FILIAL AND "
cQuery += "G3Q.G3Q_NUMID  = G3P.G3P_NUMID  AND "
cQuery += cFilterG3Q  

cQuery += "Inner Join "+RetSqlName("G3R")+" G3R ON "
cQuery += "G3R.G3R_FILIAL = G3Q.G3Q_FILIAL AND "
cQuery += "G3R.G3R_NUMID  = G3Q.G3Q_NUMID  AND " 
cQuery += "G3R.G3R_IDITEM = G3Q.G3Q_IDITEM AND " 
cQuery += "G3R.G3R_NUMSEQ = G3Q.G3Q_NUMSEQ AND  "
cQuery += cFilterG3R  
 
cQuery += "Inner Join "+RetSqlName("G3S")+" G3S ON "
cQuery += "G3S.G3S_FILIAL = G3Q.G3Q_FILIAL AND "
cQuery += "G3S.G3S_IDITEM = G3Q.G3Q_IDITEM AND "
cQuery += "G3S.G3S_NUMID  = G3Q.G3Q_NUMID  AND "
cQuery += "G3S.G3S_NUMSEQ = G3Q.G3Q_NUMSEQ AND  "
cQuery += cFilterG3S

cQuery += "Left Outer Join "+RetSqlName("G3T")+" G3T ON " 
cQuery += "G3T.G3T_FILIAL = G3Q.G3Q_FILIAL AND " 
cQuery += "G3T.G3T_NUMID  = G3Q.G3Q_NUMID  AND "
cQuery += "G3T.G3T_IDITEM = G3Q.G3Q_IDITEM AND " 
cQuery += "G3T.G3T_NUMSEQ = G3Q.G3Q_NUMSEQ AND " 
cQuery += "G3T.G3T_CODPAX = G3S.G3S_CODPAX AND " 
cQuery += cFilterG3T

cQuery += "Left Outer Join "+RetSqlName("G3U")+" G3U ON " 
cQuery += "G3U.G3U_FILIAL = G3Q.G3Q_FILIAL AND " 
cQuery += "G3U.G3U_NUMID  = G3Q.G3Q_NUMID  AND " 
cQuery += "G3U.G3U_IDITEM = G3Q.G3Q_IDITEM AND " 
cQuery += "G3U.G3U_NUMSEQ = G3Q.G3Q_NUMSEQ AND " 
cQuery += "G3U.G3U_CODPAX = G3S.G3S_CODPAX AND " 
cQuery += cFilterG3U

cQuery += "Left Outer Join "+RetSqlName("G3V")+" G3V ON " 
cQuery += "G3V.G3V_FILIAL = G3Q.G3Q_FILIAL AND " 
cQuery += "G3V.G3V_NUMID  = G3Q.G3Q_NUMID  AND "
cQuery += "G3V.G3V_IDITEM = G3Q.G3Q_IDITEM AND " 
cQuery += "G3V.G3V_NUMSEQ = G3Q.G3Q_NUMSEQ AND " 
cQuery += "G3V.G3V_CODPAX = G3S.G3S_CODPAX AND " 
cQuery += cFilterG3V

cQuery += "Left Outer Join "+RetSqlName("G3W")+" G3W ON " 
cQuery += "G3W.G3W_FILIAL = G3Q.G3Q_FILIAL AND " 
cQuery += "G3W.G3W_NUMID  = G3Q.G3Q_NUMID  AND " 
cQuery += "G3W.G3W_IDITEM = G3Q.G3Q_IDITEM AND " 
cQuery += "G3W.G3W_NUMSEQ = G3Q.G3Q_NUMSEQ AND " 
cQuery += "G3W.G3W_CODPAX = G3S.G3S_CODPAX AND " 
cQuery += cFilterG3W

cQuery += "Left Outer Join "+RetSqlName("G3X")+" G3X ON " 
cQuery += "G3X.G3X_FILIAL = G3Q.G3Q_FILIAL AND " 
cQuery += "G3X.G3X_NUMID  = G3Q.G3Q_NUMID  AND " 
cQuery += "G3X.G3X_IDITEM = G3Q.G3Q_IDITEM AND " 
cQuery += "G3X.G3X_NUMSEQ = G3Q.G3Q_NUMSEQ AND " 
cQuery += "G3X.G3X_CODPAX = G3S.G3S_CODPAX AND " 
cQuery += cFilterG3X

cQuery += "Left Outer Join "+RetSqlName("G3Y")+" G3Y ON " 
cQuery += "G3Y.G3Y_FILIAL = G3Q.G3Q_FILIAL AND " 
cQuery += "G3Y.G3Y_NUMID  = G3Q.G3Q_NUMID  AND " 
cQuery += "G3Y.G3Y_IDITEM = G3Q.G3Q_IDITEM AND " 
cQuery += "G3Y.G3Y_NUMSEQ = G3Q.G3Q_NUMSEQ AND " 
cQuery += "G3Y.G3Y_CODPAX = G3S.G3S_CODPAX AND " 
cQuery += cFilterG3Y

cQuery += "Left Outer Join "+RetSqlName("G3Z")+" G3Z ON " 
cQuery += "G3Z.G3Z_FILIAL = G3Q.G3Q_FILIAL AND " 
cQuery += "G3Z.G3Z_NUMID  = G3Q.G3Q_NUMID  AND " 
cQuery += "G3Z.G3Z_IDITEM = G3Q.G3Q_IDITEM AND " 
cQuery += "G3Z.G3Z_NUMSEQ = G3Q.G3Q_NUMSEQ AND " 
cQuery += "G3Z.G3Z_CODPAX = G3S.G3S_CODPAX AND " 
cQuery += cFilterG3Z

cQuery += "Left Outer Join "+RetSqlName("G40")+" G40 ON " 
cQuery += "G40.G40_FILIAL = G3Q.G3Q_FILIAL AND " 
cQuery += "G40.G40_NUMID  = G3Q.G3Q_NUMID  AND " 
cQuery += "G40.G40_IDITEM = G3Q.G3Q_IDITEM AND " 
cQuery += "G40.G40_NUMSEQ = G3Q.G3Q_NUMSEQ AND " 
cQuery += "G40.G40_CODPAX = G3S.G3S_CODPAX AND " 
cQuery += cFilterG40

cQuery += "Left Outer Join "+RetSqlName("G41")+" G41 ON " 
cQuery += "G41.G41_FILIAL = G3Q.G3Q_FILIAL AND " 
cQuery += "G41.G41_NUMID  = G3Q.G3Q_NUMID  AND " 
cQuery += "G41.G41_IDITEM = G3Q.G3Q_IDITEM AND " 
cQuery += "G41.G41_NUMSEQ = G3Q.G3Q_NUMSEQ AND " 
cQuery += "G41.G41_CODPAX = G3S.G3S_CODPAX AND " 
cQuery += cFilterG41

cQuery += "Left Outer Join "+RetSqlName("G42")+" G42 ON " 
cQuery += "G42.G42_FILIAL = G3Q.G3Q_FILIAL AND " 
cQuery += "G42.G42_NUMID  = G3Q.G3Q_NUMID  AND " 
cQuery += "G42.G42_IDITEM = G3Q.G3Q_IDITEM AND " 
cQuery += "G42.G42_NUMSEQ = G3Q.G3Q_NUMSEQ AND "
cQuery += "G42.G42_CODPAX = G3S.G3S_CODPAX AND " 
cQuery += cFilterG42

cQuery += "Left Outer Join "+RetSqlName("G43")+" G43 ON " 
cQuery += "G43.G43_FILIAL = G3Q.G3Q_FILIAL AND " 
cQuery += "G43.G43_NUMID  = G3Q.G3Q_NUMID  AND " 
cQuery += "G43.G43_IDITEM = G3Q.G3Q_IDITEM AND " 
cQuery += "G43.G43_NUMSEQ = G3Q.G3Q_NUMSEQ AND " 
cQuery += "G43.G43_CODPAX = G3S.G3S_CODPAX AND " 
cQuery += cFilterG43

cQuery += "Inner Join "+RetSqlName("G4D")+" G4D ON "
cQuery += "G4D.G4D_FILIAL = G3Q.G3Q_FILIAL AND "
cQuery += "G4D.G4D_NUMID  = G3Q.G3Q_NUMID  AND "
cQuery += "G4D.G4D_IDITEM = G3Q.G3Q_IDITEM AND "
cQuery += "G4D.G4D_NUMSEQ = G3Q.G3Q_NUMSEQ AND "
cQuery += cFilterG4D

cQuery += "WHERE " 
cQuery += cFilterG3P

cQuery += ") AS SUBQRY "

If ( !Empty(cFilterQRY) )
	cQuery += "WHERE "
	cQuery += cFilterQRY
EndIf	

cQuery += "ORDER BY SUBQRY.G3Q_FILIAL,SUBQRY.G3Q_NUMID,SUBQRY.G3Q_IDITEM,SUBQRY.G3Q_NUMSEQ "

dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery), cAliasQry, .F., .T.)

While (cAliasQry)->( !EOF() )
	
	If lNewLine
		oModelDetail:AddLine()
	EndIf
	
	For nr := 1 to (cAliasQry)->(FCount())
		If (cAliasQry)->(FieldName(nr)) == "TMP_ARQ"
			oModelDetail:SetValue("TMP_ARQ",(cAliasQry)->(FieldGet(nr)))
		ElseIf (cAliasQry)->(FieldName(nr)) == "LOC"
			oModelDetail:SetValue("G3T_LOC",(cAliasQry)->(FieldGet(nr)))
		ElseIf (cAliasQry)->(FieldName(nr)) == "CODPAX"
			oModelDetail:SetValue("G3T_CODPAX",(cAliasQry)->(FieldGet(nr)))
		ElseIf (cAliasQry)->(FieldName(nr)) == "NOMPAX"
			oModelDetail:SetValue("G3T_NOMPAX",(cAliasQry)->(FieldGet(nr)))
		ElseIf (cAliasQry)->(FieldName(nr)) == "IDINTE"
			oModelDetail:SetValue("G3T_IDINTE",(cAliasQry)->(FieldGet(nr)))
		ElseIf (cAliasQry)->(FieldName(nr)) == "DTINI"
			oModelDetail:SetValue("G3T_DTSAID",StoD((cAliasQry)->(FieldGet(nr))))
		ElseIf (cAliasQry)->(FieldName(nr)) == "DTFIM"
			oModelDetail:SetValue("G3T_DTCHEG",StoD((cAliasQry)->(FieldGet(nr))))
		ElseIf (cAliasQry)->(FieldName(nr)) == "G3Q_EMISS"
			oModelDetail:SetValue("G3Q_EMISS" ,StoD((cAliasQry)->(FieldGet(nr))))
		Else
			oModelDetail:SetValue((cAliasQry)->(FieldName(nr)),(cAliasQry)->(FieldGet(nr)))
		EndIf
	Next
	
	lNewLine := .T.
	
	(cAliasQry)->( dbSkip() )
EndDo

(cAliasQry)->(dbCloseArea())


oModelDetail:GoLine(1)
oModel:GetModel("TMP_DETAIL"):SetNoInsertLine(.T.)
oModel:GetModel("TMP_DETAIL"):SetNoDeleteLine(.T.)
oView:Refresh()

Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA067Commit
	Funcao respons�vel por chamar a tela do Registro de Vendas

@since		10/06/2015
@version	P12
/*/
//------------------------------------------------------------------------------
Static Function TA067Commit(oModel,nOpt)

Local aDadosUsr	:= {}

Local lMarcado 	:= .F.

Local cNumId   	:= ""
Local cIdItem  	:= ""
Local cNumSeq  	:= ""
Local cFilSeek	:= ""
Local cBkpFil	:= ""
	
Local oMdlAnt  	:= oModel

cFilSeek	:= oModel:GetModel("TMP_DETAIL"):GetValue("G3Q_FILIAL")
cNumId   	:= oModel:GetModel("TMP_DETAIL"):GetValue("G3Q_NUMID")
cIdItem  	:= oModel:GetModel("TMP_DETAIL"):GetValue("G3Q_IDITEM")
cNumSeq  	:= oModel:GetModel("TMP_DETAIL"):GetValue("G3Q_NUMSEQ")
	
If G3P->(dbSeek(PadR(cFilSeek,TamSx3("G3P_FILIAL")[1])+cNumId))
	
	cBkpFil := cFilAnt 
	cFilAnt := cFilSeek
		
	T34Segmento( Val( G3P->G3P_SEGNEG ) )
	TURA34HierAces(__cUserId,@aDadosUsr)
	
	If Empty(aDadosUsr[1])
		Help(,,"Tura34SemAcesso",,STR0040,1,0) //'Seu codigo de usuario nao esta relacionado com um vendedor ou agente. Nao sera possivel acessar essa rotina.'
	Else

		T34DadosUsr( aDadosUsr )
		
		FWExecView(Iif(nOpt==2,STR0038,STR0039),'TURA034',Iif(nOpt==2,MODEL_OPERATION_VIEW,MODEL_OPERATION_UPDATE),,{|| .T. }) //"VISUALIZAR"#"ALTERAR"
		
		T34Segmento( 0 )
		T34DadosUsr( {"","","",""} )

	EndIf
		
	cFilAnt := cBkpFil
		
EndIf

Return .T.

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA067Destroy
Fun��o para desalocar objetos instanciados
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
//------------------------------------------------------------------------------
Function TA067Destroy()

//Verifica se o objeto FwTemporaryTable est� instanciado
If ValType( oTblHeader ) == "O"
	oTblHeader:Delete()
	FreeObj( oTblHeader )
EndIf

//Verifica se o objeto FwTemporaryTable est� instanciado
If ValType( oTblDetail ) == "O"
	oTblDetail:Delete()
	FreeObj( oTblDetail )
EndIf


Return .T.

//------------------------------------------------------------------------------
/*/{Protheus.doc} TURA34HierAces
	Retorna a hierarquia de acesso do
	usuario logado

@param		Nenhum
@since		10/06/2015
@version	P12
/*/
//------------------------------------------------------------------------------
Static Function TURA67HierAces(cCodUsr,aUsrDados)

Local aAreaBkp := GetArea()
Local cAliasUsr:= GetNextAlias()

aUsrDados := {"","","","",""}

BeginSql Alias cAliasUsr
	SELECT
		SA3.A3_COD, G3H.G3H_CODAGE, G3H.G3H_NOMEAG, G3H.G3H_TPATCO, G3H.G3H_TPATEV, G3H.G3H_TPATLZ
	FROM
		%table:SA3% SA3, %table:G3H% G3H
	WHERE
		SA3.A3_FILIAL = %xFilial:SA3% AND SA3.%NotDel% AND
		SA3.A3_CODUSR = %Exp:cCodUsr% AND
		G3H.G3H_FILIAL = %xFilial:G3H% AND G3H.%NotDel% AND
		G3H.G3H_CODVEN = SA3.A3_COD
EndSql

If (cAliasUsr)->(!Eof())
	aUsrDados[1] := (cAliasUsr)->A3_COD
	aUsrDados[2] := (cAliasUsr)->G3H_CODAGE
	aUsrDados[3] := (cAliasUsr)->G3H_TPATCO
	aUsrDados[4] := (cAliasUsr)->G3H_TPATEV
	aUsrDados[5] := (cAliasUsr)->G3H_TPATLZ
EndIf
(cAliasUsr)->(dbCloseArea())

RestArea(aAreaBkp)

Return(Nil)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TURA34Postos
	Retorna em formato de ARRAY ou STRING para uso em filtro SQL os postos
	de atendimento relacionados ao tipo de Registro de Venda

@param		Nenhum
@since		10/06/2015
@version	P12
/*/
//------------------------------------------------------------------------------
Static Function TURA67Postos(cCodAgente,cSegmen)

Local aAreaBkp   := GetArea()
Local aPostos	:= {}

Local cAliasPos  := GetNextAlias()
Local cRetPos    := ""

BeginSql Alias cAliasPos

	SELECT 
		DISTINCT 
		G3I_POSTO
	FROM 
		%table:G3I% G3I
	WHERE 
	  G3I.G3I_SEGMEN = %Exp:cSegmen% AND 
	  G3I.G3I_CODAGE = %Exp:cCodAgente% AND 
	  G3I.%NotDel%
	ORDER BY
		G3I_POSTO
				  
EndSql

While (cAliasPos)->(!Eof())

	If ( aScan(aPostos, (cAliasPos)->G3I_POSTO ) == 0 )

		If Empty(cRetPos)
			cRetPos += "('"
		Else	
			cRetPos += "','"
		EndIf
		
		cRetPos += (cAliasPos)->G3I_POSTO 
	
		aAdd(aPostos, (cAliasPos)->G3I_POSTO )
	
	EndIf	
		
	(cAliasPos)->(dbSkip())
EndDo
(cAliasPos)->(dbCloseArea())

If !Empty(cRetPos)
	cRetPos += "')"
EndIf	

RestArea(aAreaBkp)

aSize(aPostos,0)
aPostos := Nil

Return(cRetPos)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA67Fechar

Fun��o executada atrav�s do bloco de Cancelamento da Tela. Configura a View
como se n�o tivesse sido alterada. O MVC TURA067 n�o persiste dados em banco.

@param		Nenhum
@since		01/12/2016
@version	P12
/*/
//------------------------------------------------------------------------------

Static Function TA67Fechar() 

Local oView	:= FwViewActive()
		
oView:SetModified(.f.)
	
Return(.t.)
