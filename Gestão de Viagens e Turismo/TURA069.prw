#Include 'Protheus.ch'
#INCLUDE 'FWMVCDEF.CH'
#Include 'TURA069.ch'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA069()

CADASTRO TAXA DE CAMBIO - SIGATUR

@sample 	TURA031()
@return                             
@author  	LUCAS.BRUSTOLIN
@since   	25/04/2016
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Function TURA069()

Local oBrowse := Nil

// Instanciamento da Classe de Browse
oBrowse := FWMBrowse():New()

// Defini��o da tabela do Browse
oBrowse:SetAlias('G9L')

// Titulo da Browse
oBrowse:SetDescription(STR0001)//'Taxa de Cambio'

// Ativa��o da Classe
oBrowse:Activate()

Return Nil


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef()

DEFINI��O DO MENU DO CADASTRO

@sample 	MenuDef()
@return                             
@author  	LUCAS.BRUSTOLIN
@since   	25/04/2016
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Static Function MenuDef()

Local aRotina := {}
ADD OPTION aRotina Title STR0002 	Action 'VIEWDEF.TURA069'	OPERATION 2 ACCESS 0 // 'Visualizar'
ADD OPTION aRotina Title STR0003 	Action 'VIEWDEF.TURA069'	OPERATION 3 ACCESS 0 // 'Incluir'
ADD OPTION aRotina Title STR0004	Action 'VIEWDEF.TURA069'	OPERATION 4 ACCESS 0 // 'Alterar'
ADD OPTION aRotina Title STR0005 	Action 'VIEWDEF.TURA069'	OPERATION 5 ACCESS 0 // 'Excluir'
ADD OPTION aRotina Title STR0006 	Action 'VIEWDEF.TURA069'	OPERATION 8 ACCESS 0 // 'Imprimir'

Return aRotina

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef()

DEFINI��O DO MODELO DE DADOS

@sample 	ModelDef()
@return                             
@author  	LUCAS.BRUSTOLIN
@since   	25/04/2016
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Static Function ModelDef()

// Cria a estrutura a ser usada no Modelo de Dados
Local oStruG9L := FWFormStruct( 1, 'G9L' )
// Modelo de dados que ser� constru�do
Local oModel	:= Nil

// Cria o objeto do Modelo de Dados
oModel:= MPFormModel():New("TURA069", /*bPreValidacao*/,{|oModel| TURA069TOK(oModel) } ,/*bCommit*/,/*bCancel*/)

// Adiciona ao modelo um componente de formul�rio
oModel:AddFields( 'G9LMASTER', /*cOwner*/, oStruG9L)
// Adiciona a descri��o do Modelo de Dados
oModel:SetDescription( STR0007 ) // 'Modelo de dados Taxa de Cambio'
// Adiciona a descri��o do Componente do Modelo de Dados 
oModel:GetModel( 'G9LMASTER' ):SetDescription( STR0001 ) // 'Taxa de Cambio'

oModel:SetPrimaryKey( {'G9L_FILIAL','G9L_DATA','G9L_MOEDA'} )

Return oModel

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef()

DEFINI��O DO MODELO DE VISUALIZA��O

@sample 	ViewDef()
@return                             
@author  	LUCAS.BRUSTOLIN
@since   	25/04/2016
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Static Function ViewDef()
// Cria um objeto de Modelo de dados baseado no ModelDef() do fonte informado
Local oModel := FWLoadModel( 'TURA069' )

// Cria a estrutura a ser usada na View
Local oStruG9L := FWFormStruct( 2, 'G9L' )
// Interface de visualiza��o constru�da
Local oView
// Cria o objeto de View
oView := FWFormView():New()
// Define qual o Modelo de dados ser� utilizado na View
oView:SetModel( oModel )
// Adiciona no nosso View um controle do tipo formul�rio
// (antiga Enchoice)
oView:AddField( 'VIEW_G9L', oStruG9L, 'G9LMASTER' )
// Criar um "box" horizontal para receber algum elemento da view
oView:CreateHorizontalBox( 'TELA' , 100 )
// Relaciona o identificador (ID) da View com o "box" para exibi��o
oView:SetOwnerView( 'VIEW_G9L', 'TELA' )

Return oView

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA069TOK()

REALIZA A VALIDA��O DO MODELO DE DADOS.
1. VALIDA DUPLICIDADE DE REGISTRO

@sample 	TURA069TOK()
@Param 	oModel = Modelo de dados
@return   	lRet = .T. OK 
@author  	Inova��o
@since   	25/04/2016
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Function TURA069TOK(oModel)

Local nOperation	:= oModel:GetOperation() 
Local lRet 	:= .T.


If nOperation ==	 MODEL_OPERATION_INSERT

	DbSelectArea('G9L')
	DbSetOrder(1)
	
	If DbSeek( xFilial('G9L') + DTOS(FwFldGet('G9L_DATA')) + FwFldGet('G9L_MOEDA')  )
		Help('TURA069', 1, STR0008, ,STR0009, 1, 0 ) // 'Aten��o'#'Taxa de Cambio j� cadastrada.'
		lRet := .F.
	EndIf

EndIf
	
Return (lRet)
                                                     

