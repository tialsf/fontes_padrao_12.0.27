#include 'PROTHEUS.CH'
#include 'TURA070.CH'
#include 'FWMVCDEF.CH'
#Include 'TBICONN.CH'
#Include 'DIRECTRY.CH'

#DEFINE CRLF Chr(13) + Chr(10)

/*/{Protheus.doc} TURA070
Wizard de Importa��o PNR
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     26/04/2016
/*/

Function TURA070(oModAnt,oViewAnt)

	Local aDirPNR  := {}
	Local nOption  := 0          //Pagina 1
	Local cOrigem  := Space(256) //Pagina 2
	Local nRadio   := 0          //Pagina 3
	Local oStepWiz := Nil
	Local o1stPage := Nil
	Local o2ndPage := Nil
	Local o3rdPage := Nil	
	Local o4thPage := Nil
	
	If GetRemoteType() == 5

		Help(,,"TURA070NOHTML",,STR0026,1,0)//"Fun��o n�o dispon�vel para SmartClient HTML."
		
		Return	
	
	EndIf
	
	oStepWiz := FWWizardControl():New(,{600,850}) //Instancia a classe FWWizardControl 
 
	oStepWiz:ActiveUISteps()
	
	//----------------------
	// Pagina 1               // ConstPage1
	//----------------------  
	o1stPage := oStepWiz:AddStep("1STSTEP",{|Panel| ConstPage1(Panel, @nOption, @nRadio, @aDirPNR, @cOrigem)}) // Adiciona um Step
	o1stPage:SetStepDescription(STR0005)//"Quantidade de arquivos"
	o1stPage:SetNextTitle(STR0009)//"Pr�ximo"
	o1stPage:SetNextAction({|| ValidPage1(@nOption, @nRadio, @aDirPNR, @cOrigem)}) // Define o bloco ao clicar no bot�o Pr�ximo
	o1stPage:SetCancelAction({|| .T.}) // Define o bloco ao clicar no bot�o Cancelar
	 
	//----------------------
	// Pagina 2
	//---------------------- 
	o2ndPage := oStepWiz:AddStep("2NDSTEP", {|Panel| ConstPage2(Panel, @nOption, @nRadio, @aDirPNR, @cOrigem)})
	o2ndPage:SetStepDescription(STR0006)//"Origem do(s) arquivo(s)" 
	o2ndPage:SetNextTitle(STR0009)//"Pr�ximo"
	o2ndPage:SetPrevTitle(STR0010)//"Anterior"
	o2ndPage:SetNextAction({|| ValidPage2(@nOption, @nRadio, @aDirPNR, @cOrigem)})
	o2ndPage:SetPrevAction({|| .T.}) 
	o2ndPage:SetCancelAction({|| .T.}) 
	 
	//----------------------
	// Pagina 3
	//----------------------
	o3rdPage := oStepWiz:AddStep("3RDSTEP", {|Panel|ConstPage3(Panel, @nOption, @nRadio, @aDirPNR, @cOrigem)})
	o3rdPage:SetStepDescription(STR0007)//"Transfer�ncia e Processamento"
	o3rdPage:SetNextTitle(STR0009)//"Pr�ximo"
	o3rdPage:SetPrevTitle(STR0010)//"Anterior"
	o3rdPage:SetNextAction({|| ValidPage3(@nOption, @nRadio, @aDirPNR, @cOrigem)})
	o3rdPage:SetPrevAction({||.T.})
	o3rdPage:SetCancelAction({||.T.})
	
	//----------------------
	// Pagina 4
	//----------------------
	o4thPage := oStepWiz:AddStep("4THSTEP", {|Panel|ConstPage4(Panel, @nOption, @nRadio, @aDirPNR, @cOrigem)})
	o4thPage:SetStepDescription(STR0008)//"Execu��o"
	o4thPage:SetNextTitle(STR0009)//"Pr�ximo"
	o4thPage:SetPrevTitle(STR0010)//"Anterior"
	o4thPage:SetNextAction({|| ValidPage4(@nOption, @nRadio, @aDirPNR, @cOrigem)})
	o4thPage:SetPrevAction({||.T.})
	o4thPage:SetCancelAction({||.T.})
	
	oStepWiz:Activate()

	oStepWiz:Destroy()
	
Return


/*/{Protheus.doc} CONSTPAGE1
Constru��o da p�gina 1 do Wizard
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     27/04/2016
/*/
Static Function ConstPage1(oPanel, nOption, nRadio, aDirPNR, cOrigem)
	
	//Pagina 1
	//Lote Sim/N�o
	Local oFont
	Local oSay
	Local aCombo  := {STR0017, STR0016} // "Sim", "N�o"
	
	oFont:= TFont():New(,,-25,.T.,.T.,,,,,)
	
	oSayLote :=      TSay():New(35, 15,  {|| STR0011},oPanel,,,,,,.T.,CLR_BLACK,)//"Importa��o em Lote" 
	oRadio1  := TRadMenu():New(35, 100, aCombo, {|u| IIf(PCount() == 0, nOption, nOption := u) }, oPanel,,,,,,,, 100, 12,,,, .T.)	
	
Return


/*/{Protheus.doc} CONSTPAGE2
Constru��o da p�gina 2 do Wizard
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     27/04/2016
/*/
Static Function ConstPage2(oPanel, nOption, nRadio, aDirPNR, cOrigem)
	
	//Pagina 2
	// Pasta / Arquivo
	
	//1=SIM (Lote)
	//2=N�O (N�o Lote, arquivo)

	Local oFont
	Local oSay

	oFont:= TFont():New(,,-25,.T.,.T.,,,,,)

	oSayFile1 :=    TSay():New(35, 15,  {|| STR0011},oPanel,,,,,,.T.,CLR_BLACK,)//"Importa��o em Lote" 
	oSayFile2 :=    TSay():New(35, 100, {|| IIf(nOption==2, STR0016, STR0017)},oPanel,,,,,,.T.,CLR_BLACK,) //"Arquivo �nico" //"Lote(Pasta)"

	oSayFile3 :=    TSay():New(60, 15,  {|| STR0018},oPanel,,,,,,.T.,CLR_BLACK,) //"Local de Origem:"
	oBtnLog  := TButton():New(60, 100, STR0013, oPanel,{||cOrigem := cGetFile('*.PNR*','',,,,IIf(nOption==2, 48, 176)) }, 60,20,,,.F.,.T.,.F.,,.F.,,,.F. ) //"Selecione"

Return


/*/{Protheus.doc} CONSTPAGE3
Constru��o da p�gina 3 do Wizard
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     27/04/2016
/*/
Static Function ConstPage3(oPanel, nOption, nRadio, aDirPNR, cOrigem)
	
	Local aRadius := {STR0003, STR0004} //"Apenas Transfere", "Transfere e Processa"
	
	oSayFile1 :=    TSay():New(35, 15,  {|| STR0011},oPanel,,,,,,.T.,CLR_BLACK,)//"Importa��o em Lote" 
	oSayFile2 :=    TSay():New(35, 100, {|| IIf(nOption==2, STR0016, STR0017)},oPanel,,,,,,.T.,CLR_BLACK,) //"Arquivo �nico" //"Lote(Pasta)"

	oSayFile3 :=    TSay():New(60, 15,  {|| STR0018},oPanel,,,,,,.T.,CLR_BLACK,) //"Local de Origem:"
	oSayFile4 :=    TSay():New(60, 100, {|| AllTrim(Upper(cOrigem))},oPanel,,,,,,.T.,CLR_BLACK,) //cOrigem
	
	oSayFile5 :=    TSay():New(85, 15,{|| STR0019},oPanel,,,,,,.T.,CLR_BLACK,) //"O que ser� feito:"
	oRadio   := TRadMenu():New(85, 100, aRadius, {|u| IIf(PCount() == 0, nRadio, nRadio := u) }, oPanel,,,,,,,, 100, 12,,,, .T.)     
	
Return


/*/{Protheus.doc} CONSTPAGE4
Constru��o da p�gina 4 do Wizard
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     27/04/2016
/*/
Static Function ConstPage4(oPanel, nOption, nRadio, aDirPNR, cOrigem)
	
	Local oFont
	Local oSay
	
	oFont:= TFont():New(,,-20,.T.,.T.,,,,,)
	
	oSayTop		:= TSay():New(10, 15, {|| STR0014},oPanel,,oFont,,,,.T.,CLR_BLACK,) //"Por favor, confira as defini��es escolhidas:"
	
	oSayBottom1	:= TSay():New(35, 15, {|| STR0015},oPanel,,,,,,.T.,CLR_BLACK,) //"Tipo de Sele��o:"
	oSayBottom2	:= TSay():New(35, 100,{|| IIf(nOption==2,STR0016, STR0017)},oPanel,,,,,,.T.,CLR_BLACK,) //"Arquivo �nico" //"Lote(Pasta)"
	 
	oSayBottom3	:= TSay():New(60, 15, {|| STR0018},oPanel,,,,,,.T.,CLR_BLACK,) //"Local de Origem:"
	oSayBottom4	:= TSay():New(60, 100,{|| AllTrim(Upper(cOrigem))},oPanel,,,,,,.T.,CLR_BLACK,) //cOrigem
	          
	oSayBottom5	:= TSay():New(85, 15, {|| STR0019},oPanel,,,,,,.T.,CLR_BLACK,) //"O que ser� feito:"           
	oSayBottom6	:= TSay():New(85, 100,{|| IIf(nRadio==1, STR0020, STR0021)},oPanel,,,,,,.T.,CLR_BLACK,) //"Apenas Transfer�ncia/Transfer�ncia seguida de Processamento"
	
Return


/*/{Protheus.doc} VALIDPAGE1
Valida��o da p�gina 1 do Wizard
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     27/04/2016
/*/
Static Function ValidPage1(nOption, nRadio, aDirPNR, cOrigem)

	Local lRetPage1 := .T.

 	If Empty(nOption)
 	
		Help(,,"TURA070LOTE",, STR0012,1,0)//"Escolha uma op��o. Sim ou N�o."
		
		lRetPage1 := .F. 		
 	
 	EndIf 

Return(lRetPage1)


/*/{Protheus.doc} VALIDPAGE2
Valida��o da p�gina 2 do Wizard
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     27/04/2016
/*/
Static Function ValidPage2(nOption, nRadio, aDirPNR, cOrigem)
	
	Local lRetPage2 := .T.
	Local cDrive
	Local cDiretorio
	Local cNome
	Local cExtensao

	
	If Empty(cOrigem)
		
		lRetPage2 := .F.
		
		If nOption == 1 // Lote
			
			Help(,,"TURA070SEMLOT",,STR0022,1,0)//"Selecione uma Pasta de Origem."
			
		Else
		
			Help(,,"TURA070SEMARQ",,STR0023,1,0)//"Selecione um arquivo."
		
		EndIf
	
	EndIf
	
	If nOption == 1 // Lote
	
		aDirPNR := Directory(cOrigem + "*.pnr*")
		
		If Len(aDirPNR) == 0 
		
			lRetPage2 := .F.
			
			Help(,,"TURA070ZEROARQ",,STR0024,1,0)//"O diretorio escolhido n�o possui arquivos PNR."
		
		EndIf
	
	Else
	
		SplitPath (cOrigem, @cDrive, @cDiretorio, @cNome, @cExtensao )
		
		If Empty(AllTrim(cDrive)) 
		
			Help(,,"TURA070DRIVE",,STR0027,1,0)//"O caminho (path) escolhido n�o possui drive especificado."
			
			lRetPage2 := .F.
		
		EndIf
		
		If Empty(AllTrim(cNome)) 
		
			Help(,,"TURA070NOME",,STR0028,1,0)//"Nenhum arquivo foi escolhido."
			
			lRetPage2 := .F.
		
		EndIf
		
		If Empty(AllTrim(cExtensao)) .or. Upper(AllTrim(cExtensao)) <> ".PNR"
		
			Help(,,"TURA070EXT",,STR0029,1,0)//"O arquivo escolhido n�o � um arquivo PNR."
			
			lRetPage2 := .F. 
		
		EndIf
		
	EndIf
	
Return(lRetPage2)


/*/{Protheus.doc} VALIDPAGE3
Valida��o da p�gina 3 do Wizard
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     27/04/2016
/*/
Static Function ValidPage3(nOption, nRadio, aDirPNR, cOrigem)
	
	Local lRetPage3 := .T.
	
	If nRadio <> 1 .and. nRadio <> 2
		
		lRetPage3 := .F.
		
		Help(,,"TURA070TRANSF",,STR0025,1,0)//"Escolha uma das op��es."
		
	EndIf
	
Return(lRetPage3)


/*/{Protheus.doc} VALIDPAGE4
Valida��o da p�gina 4 do Wizard
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     27/04/2016
/*/
Static Function ValidPage4(nOption, nRadio, aDirPNR, cOrigem)
	
	Local nStopHere
	Local cDrive
	Local cDiretorio
	Local cNome
	Local cExtensao
	Local nArq
	Local nTransf
	Local nQtdFail := 0
	Local nQtdoK   := 0
	Local aTransf  := {}
	Local cPath 	 := SuperGetMV("MV_DIRSABR", .F., "\sigatur\importacao\PNR\Sabre\", )
	
	SplitPath (cOrigem, @cDrive, @cDiretorio, @cNome, @cExtensao )
	
	If nOption == 2 // Arquivo
	
		AAdd(aDirPNR, {cNome + cExtensao, 100, Date(), Time(), ""  })
	
	EndIf
	
	For nArq := 1 To Len(aDirPNR)
	
		If CpyT2S( cDrive + cDiretorio + aDirPNR[nArq][1], cPath )
			
			AAdd(aTransf, .T.)
			nQtdOk   += 1
			
		Else
			
			AAdd(aTransf, .F.)
			nQtdFail += 1
			
		EndIf
	
	Next
	
	If nQtdOk == Len(aDirPNR)
	
		FwAlertSuccess(AllTrim(Str(nQtdOk)) + STR0030, STR0031) //" arquivos(s) transferido(s) com sucesso." //"Sucesso"
	
	Else
	
		FwAlertError(STR0032 + AllTrim(Str(nQtdFail)) + STR0033, STR0034) //"Falha na transfer�ncia de " //" arquivo(s)." //"Falha"
	
	EndIf
	
	If nRadio == 2 .and. nQtdOk > 0 
	
		Processa({|| CallTura48(@aDirPNR) }, STR0035,,.T.) //"Processando arquivos. Por Favor, aguarde."
	
	EndIf
	
Return(.T.)

Static Function CallTura48(aDirPNR)
	
	Local nFiles     := Len(aDirPNR)
	Local nProc      := 0
	Local aRetTura48 := {}
	Local nMsgTipo3  := 0
	Local nMsgTipo1  := 0
	
	ProcRegua(nFiles)

	For nProc := 1 To nFiles
		
		IncProc(STR0036 + AllTrim(Str(nProc)) + STR0037 + AllTrim(Str(nFiles)) ) //"Arquivo " //" de "
		
		aRetTura48 := {}
	
		aRetTura48 := TURA048("", aDirPNR[nProc][1] )
		
		If Len(aRetTura48) > 1
		
			If aRetTura48[1]
				nMsgTipo3 += 1
			EndIf
	
			If aRetTura48[2]
				nMsgTipo1 += 1
			EndIf
		
		EndIf
		
	Next
	
	If nMsgTipo3 > 0
	
		FwAlertError(STR0038 + AllTrim(Str(nMsgTipo3)) + STR0039, STR0034) //"Falha no processamento de " //" arquivo(s)." //"Falha"		
					
	EndIf
	
	If nMsgTipo1 > 0
		
		FwAlertSuccess(AllTrim(Str(nMsgTipo1)) + STR0040, STR0031)	//" arquivos(s) processado(s) com sucesso." //"Sucesso"
			
	EndIf

Return