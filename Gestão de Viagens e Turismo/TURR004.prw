#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"
#INCLUDE 'TOTVS.CH'
#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'FILEIO.CH'
#INCLUDE 'TURR004.CH'

#DEFINE CRLF Chr(13) + Chr(10)
#DEFINE DMPAPER_A4 9

/*/{Protheus.doc} TURR004
Faz a chamada da fun��o de impress�o atraves do bloco de processamento 
(Barra de Progresso).
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     01/03/2016
/*/
Function TURR004(oModAnt,oViewAnt)

Local lRet := .T.

//	 Inicializa a regua de processamento                                 
Processa( {|| lRet := TAR004ImpV() },STR0118,STR0119)//"Aguarde..."##"Buscando Protocolos e Vouchers."

If lRet
	MsgInfo(STR0117) //## Processamento concluido.
EndIf
	
Return(lRet)

/*/{Protheus.doc} TAR004ImpV
Fun��o para impress�o de Voucher
@author    Inova��o
@version   1.00
@since     30/06/2016
/*/
Static Function TAR004ImpV()

//-----------------------------------------------------------------------
// Declaracao de Variaveis                                             
//-----------------------------------------------------------------------
Local   oMdl034	  := Nil
Local   cOldArea   := Alias()
Local   aOldArea   := GetArea()
Local   aG3PArea   := G3P->(GetArea())
Local   aG3QArea   := G3Q->(GetArea())
Local   aG3RArea   := G3R->(GetArea())
Local   aG3SArea   := G3S->(GetArea())
Local   aG3TArea   := G3T->(GetArea())
Local   aG3UArea   := G3U->(GetArea())
Local   aG3VArea   := G3V->(GetArea())
Local   aG3ZArea   := G3Z->(GetArea())
Local   aG40Area   := G40->(GetArea())
Local   aG44Area   := G44->(GetArea())
Local   aG4DArea   := G4D->(GetArea())
Local   aSA1Area   := SA1->(GetArea())
Local   aSA2Area   := SA2->(GetArea())

Local   aG3QSeg    := {}
Local   nG3QSeg    := 0
Local   cG3QSeg    := ""
Local   lFirst     := .T.
Local   _lDisSetup := .F.
Local   lCancelRV  := .F. 
Local   cPathPDF   := ""
Local   cPrinter   := ""
Local   cFilePrint 
Local   cPathPrint  
Local   lServerRV  
Local   nFactorH   := 0   
Local   nFactorV   := 0
Local   nModResul  := 0
Local   nPageH     := 0 
Local   nPageW     := 0 
Local   nPaperSize := 0 
Local   oFontAtuRV 
Local   nQtdPage   := 0
Local   nQtdPrev   := 0
Local	 nTotReg	  := 0	

Private oPrintRV		:= Nil
Private oRVSetup		:= Nil
Private _cFilePDF := "TURR004.pdf"
Private _aFontes   := {"06", "06N", "07", "07N", "08", "08N", "09", "09N", "10", "10N", "11", "11N", "12", "12N", "13", "13N", "14", "14N", "15", "15N", "16", "16N", "17", "17N", "18", "18N"  }
Private _lShowPDF  := .T.
Private _aPassType  := {}
Private _aPassTrat  := {}
	
// SX1 - TURR004
		
// MV_PAR01 Do RV 
// MV_PAR02 Ate o RV
// MV_PAR03 Do Id Item
// MV_PAR04 Ate o Id Item
// MV_PAR05 Da Sequencia 
// MV_PAR06 Ate a Sequencia
// MV_PAR07 Da Data de Emiss�o
// MV_PAR08 Ate a Data de Emissao
// MV_PAR09 Do Fornecedor 
// MV_PAR10 Da Loja do Fornecedor
// MV_PAR11 Ate o Fornecedor
// MV_PAR12 Ate a a Loja do Fornecedor
// MV_PAR13 Do Cliente
// MV_PAR14 Da Loja do Cliente
// MV_PAR15 Ate o Cliente
// MV_PAR16 Ate a a Loja do Cliente
// MV_PAR17 (1=Aereo;2=Terrestre;3=Ambos
// MV_PAR18 N�mero do Cart�o

	//-- Exibe tela de pergunte apenas quando a rotina for 
	//-- chamada fora do R.V (Registro de Venda)
	If !IsInCallStack("TURA034") .And. !IsInCallStack("TURA067")
		If !Pergunte("TURR004", .T.)
			Help( ,, "TURR004CAN001",, STR0120, 1, 0 )	//Opera��o Cancelada!
			Return(.F.)
		EndIf
	Else				
		oMdl034 := FwModelActive()		
		If oMdl034:GetId() <> "TURA034"		
			Return .F.		
		EndIf
				
		Pergunte("TURR004", .F.)
				
		MV_PAR01 := oMdl034:GetModel('G3P_FIELDS'):GetValue('G3P_NUMID')
		MV_PAR02 := MV_PAR01
		MV_PAR03 := oMdl034:GetModel('G3Q_ITENS'):GetValue('G3Q_IDITEM')
		MV_PAR04 := MV_PAR03
		MV_PAR05 := oMdl034:GetModel('G3Q_ITENS'):GetValue('G3Q_NUMSEQ')
		MV_PAR06 := MV_PAR05
		MV_PAR07 := oMdl034:GetModel('G3Q_ITENS'):GetValue('G3Q_EMISS') - 365
		MV_PAR08 := oMdl034:GetModel('G3Q_ITENS'):GetValue('G3Q_EMISS') + 365
		MV_PAR09 := oMdl034:GetModel('G3R_ITENS'):GetValue('G3R_FORNEC')
		MV_PAR10 := oMdl034:GetModel('G3R_ITENS'):GetValue('G3R_LOJA')
		MV_PAR11 := MV_PAR09
		MV_PAR12 := MV_PAR10
		MV_PAR13 := oMdl034:GetModel('G3Q_ITENS'):GetValue('G3Q_CLIENT')
		MV_PAR14 := oMdl034:GetModel('G3Q_ITENS'):GetValue('G3Q_LOJA')
		MV_PAR15 := MV_PAR13
		MV_PAR16 := MV_PAR14
		MV_PAR17 := 3
		MV_PAR18 := ""			
	EndIf

	AAdd(aG3QSeg,   {"1", "AEREO"   })
	AAdd(aG3QSeg,   {"2", "HOTEL"   })
	AAdd(aG3QSeg,   {"3", "VEICULO" })
	AAdd(aG3QSeg,   {"8", "SEGURO"  })
	AAdd(aG3QSeg,   {"9", "TOUR"    })
	AAdd(aG3QSeg,   {"A", "PACOTE"  })
	                           
	AAdd(_aPassType, "ADT")
	AAdd(_aPassType, "CHD")
	AAdd(_aPassType, "INF")
	AAdd(_aPassType, "JUN")
	AAdd(_aPassType, "MIL")
	AAdd(_aPassType, "SEN")

	AAdd(_aPassTrat, "MR")
	AAdd(_aPassTrat, "MRS")
	AAdd(_aPassTrat, "MISS")
	AAdd(_aPassTrat, "MSTR")

	cQuery := "SELECT DISTINCT "
	cQuery += "G3P.R_E_C_N_O_ AS RECG3P, "
	cQuery += "G3Q.R_E_C_N_O_ AS RECG3Q, "
	cQuery += "G3R.R_E_C_N_O_ AS RECG3R,  "
	cQuery += "G4D.G4D_NUMCAR, "
  	cQuery += "G4D.G4D_NSU, "
  	cQuery += "G4D.G4D_CODBAN "	
	cQuery += "FROM "
	cQuery += RetSQLName("G3P") + " G3P, "
	cQuery += RetSQLName("G3R") + " G3R, "
	cQuery += RetSQLName("G3Q") + " G3Q "
	cQuery += "LEFT JOIN "+ RetSQLName("G4D") + " G4D "
	cQuery += "ON "		
	cQuery += "G4D.G4D_FILIAL = '"+ xFilial('G4D') +"' AND "  
	cQuery += "G4D.G4D_FILIAL = G3Q.G3Q_FILIAL AND "
	cQuery += "G4D.G4D_NUMID = G3Q.G3Q_NUMID AND "
	cQuery += "G4D.G4D_IDITEM = G3Q.G3Q_IDITEM AND "
	cQuery += "G4D.G4D_NUMSEQ = G3Q.G3Q_NUMSEQ AND "
	cQuery += "G4D.D_E_L_E_T_ =  ' ' 	"	
	cQuery += "WHERE "
		
	cQuery += "G3P_NUMID  = G3Q_NUMID  AND "
	cQuery += "G3P_NUMID  = G3R_NUMID  AND "
		
	cQuery += "G3P_NUMID  BETWEEN '" + MV_PAR01 + "' AND '" + MV_PAR02 + "' AND "                 // MV_PAR01 -- MV_PAR02

	cQuery += "G3Q_IDITEM BETWEEN '" + MV_PAR03 + "' AND '" + MV_PAR04 + "' AND "                 // MV_PAR03 // MV_PAR04

	cQuery += "G3Q_NUMSEQ BETWEEN '" + MV_PAR05 + "' AND '" + MV_PAR06 + "' AND "                 // MV_PAR05 // MV_PAR06
		
	cQuery += "G3P_EMISS  BETWEEN '" + DToS(MV_PAR07) + "'   AND '" + DToS(MV_PAR08) + "'   AND " // MV_PAR07 // MV_PAR08
		
	cQuery += "G3R_FORNEC BETWEEN '" + MV_PAR09 + "' AND '" + MV_PAR11 + "' AND "                 // MV_PAR09 // MV_PAR11
	cQuery += "G3R_LOJA   BETWEEN '" + MV_PAR10 + "' AND '" + MV_PAR12 + "' AND "                 // MV_PAR10 // MV_PAR12
		
	cQuery += "G3Q_CLIENT BETWEEN '" + MV_PAR13 + "' AND '" + MV_PAR15 + "' AND "                 // MV_PAR13 // MV_PAR15
	cQuery += "G3Q_LOJA   BETWEEN '" + MV_PAR14 + "' AND '" + MV_PAR16 + "' AND "                 // MV_PAR14 // MV_PAR16
	
	If MV_PAR17 == 1
		cQuery += "G3Q_TPSEG = '1' AND "
	ElseIf MV_PAR17 == 2
		cQuery += "G3Q_TPSEG <> '1' AND "
	ElseIf MV_PAR17 == 3
	cQuery += "G3Q_TPSEG  IN ('1','2','3','8','9','A') AND "
	EndIf
	
	cQuery += "G3Q_OPERAC IN ('1','3') AND "
	
	cQuery += "G3R_IDITEM = G3Q_IDITEM AND "
	cQuery += "G3R_NUMSEQ = G3Q_NUMSEQ AND "

	cQuery += "G3R_CONINU = '' AND "
	cQuery += "G3Q_CONINU = '' AND "
	
	cQuery += "G3Q_STATUS NOT IN ('3', '4') AND "
 		
	cQuery += "G3P.D_E_L_E_T_ = ' ' AND "
	cQuery += "G3Q.D_E_L_E_T_ = ' ' AND "
	cQuery += "G3R.D_E_L_E_T_ = ' ' AND "
		
	cQuery += "G3P_FILIAL = '" + xFilial("G3P") + "' AND "
	cQuery += "G3Q_FILIAL = '" + xFilial("G3Q") + "' AND "
	cQuery += "G3R_FILIAL = '" + xFilial("G3R") + "'     "
		
	If MV_PAR17 == 1 .And. !Empty(AllTrim(MV_PAR18)) 
		cQuery += "AND G4D.G4D_NUMCAR = '"+ MV_PAR18 +"' "
	EndIf
		
	cTMPAlias := GetNextAlias()
		
			
	DBUseArea(.T., "TOPCONN", TCGenQry(,, cQuery), cTMPAlias, .F., .T.)
	
	DBSelectArea(cTMPAlias)
	
	Count to nTotReg
	
	ProcRegua( nTotReg   )	
	
	(cTMPAlias)->( DbGoTop())
	
	If (cTMPAlias)->( !EOF() )	
			
		While (cTMPAlias)->( !EOF() )
		
			If !lCancelRV
							
				DBSelectArea("G3P")
				DBGoTo((cTMPAlias)->RECG3P)
					
				DBSelectArea("G3Q")
				DBGoTo((cTMPAlias)->RECG3Q)
					
				DBSelectArea("G3R")
				DBGoTo((cTMPAlias)->RECG3R)
				
				DBSelectArea("SA1")
				DBSetOrder(1)
				DBSeek(xFilial() + G3Q->G3Q_CLIENT + G3Q->G3Q_LOJA)
				
				DBSelectArea("SA2")
				DBSetOrder(1)
				DBSeek(xFilial() + G3R->G3R_FORNEC + G3R->G3R_LOJA)
				 
				DBSelectArea("G3S")
				DBSetOrder(1)
				DBSeek(G3Q->G3Q_FILIAL + G3Q->G3Q_NUMID + G3Q->G3Q_IDITEM + G3Q->G3Q_NUMSEQ)
				 
				cG3QSeg := ""
				
				nG3QSeg := AScan(aG3QSeg, {|x| x[1] == G3Q->G3Q_TPSEG })
				
				If nG3QSeg > 0	
					cG3QSeg := aG3QSeg[nG3QSeg][2]
				Else
					(cTMPAlias)->( DBSkip() )
					Loop
				EndIf
		
				_cFilePDF := "VOUCHER" + Upper(AllTrim(G3P->G3P_NUMID ) + "_" + G3Q->G3Q_IDITEM + "_" + G3Q->G3Q_NUMSEQ + "_" + cG3QSeg + ".pdf")		                  
				_cFileRel := StrTran(Upper(_cFilePDF), ".PDF", ".REL")
				
				If lFirst .or. (!lFirst .and. cPrinter == "PDF")    		
					
					oPrintRV := FWMsPrinter():New(_cFilePDF,; //_cFilePDF  Nome do arquivo de relat�rio a ser criado.
						6	,; //nDevice Tipos de Sa�da aceitos:IMP_SPOOL Envia para impressora.IMP_PDF Gera arquivo PDF � partir do relat�rio.Default � IMP_SPOOL
						.T.	,; //lAdjustToLegacy Se .T. recalcula as coordenadas para manter o legado de propor��es com a classe TMSPrinter. Default � .T.IMPORTANTE:
							,; //cPathInServer 	Diret�rio onde o arquivo de relat�rio ser� salvo
						_lDisSetup	,; //lDisabeSetup 	Se .T. n�o exibe a tela de Setup, ficando � cargo do programador definir quando e se ser� feita sua chamada. Default � .F.
						.F.,; //lTReport Indica que a classe foi chamada pelo TReport. Default � .F.
						,; //oPrintSetup Objeto FWPrintSetup instanciado pelo usu�rio
						cPrinter	,; //cPrinter Impressora destino "for�ada" pelo usu�rio. Default � ""
						,; //lServer  Indica impress�o via Server (.REL N�o ser� copiado para o Client). Default � .F.   
						.F.,; //lPDFAsPNG .T. Indica que ser� gerado o PDF no formato PNG. O Default � .T.
						,; //lRaw  	.T. indica impress�o RAW/PCL, enviando para o dispositivo de impress�o caracteres bin�rios(RAW) ou caracteres program�veis espec�ficos da impressora(PCL)
						.F.,; //lViewPDF  Quando o tipo de impress�o for PDF, define se arquivo ser� exibido ap�s a impress�o. O default � .T.
						 1 ) // nQtdCopy Define a quantidade de c�pias a serem impressas quando utilizado o metodo de impress�o igual a SPOOL.
									
				EndIf
				
				lCancelRV := oPrintRV:nModalResult == 2  
				
				If !lCancelRV					
					If oPrintRV:CPRINTER == "PDF" .and. lFirst
						cPrinter := "PDF"						
						cPathPDF   := oPrintRV:CPATHPDF
					EndIf
				
					oPrintRV:SetPortrait(.T.)
				
					If lFirst
						_lDisSetup := .T.						
						cPrinter   := oPrintRV:CPRINTER						
						cPathPrint := oPrintRV:CPATHPRINT						
						cPathPDF   := oPrintRV:CPATHPDF	
						cFilePrint := oPrintRV:CFILEPRINT						
						lServerRV  := oPrintRV:LSERVER						
						nFactorH   := oPrintRV:NFACTORHOR						
						nFactorV   := oPrintRV:NFACTORVERT						
						nModResul  := oPrintRV:NMODALRESULT						
						nPageH     := oPrintRV:NPAGEHEIGHT						
						nPageW     := oPrintRV:NPAGEWIDTH						
						nPaperSize := oPrintRV:NPAPERSIZE						
						oFontAtuRV := oPrintRV:OFONTATU						
					Else						
						oPrintRV:CPRINTER     := cPrinter   						
						oPrintRV:CPATHPRINT   := cPathPrint 	
						oPrintRV:CPATHPDF     := cPathPDF 
					EndIf
						
					oPrintRV:StartPage()									
					oPrintRV:SetResolution(72)			
					oPrintRV:SetPaperSize(DMPAPER_A4)					
					CreatPDF(@oPrintRV)					
					oPrintRV:EndPage()	
									
					If oPrintRV:CPRINTER == "PDF"
						oPrintRV:Preview()						
						nQtdPrev += 1	
					EndIf
				EndIf					
			EndIf						
			
			If lCancelRV
				Help( ,, "TURR004CAN002",, STR0120, 1, 0 )	//"Opera��o Cancelada!"
				Return(.F.)
			EndIf
							
			IncProc( STR0121 + Upper(AllTrim(G3P->G3P_NUMID)) ) //"Processando R.V "
			
			(cTMPAlias)->( DBSkip() )
			nQtdPage += 1 		
			lFirst := .F.		
		EndDo

		If nQtdPage > nQtdPrev .and. !lCancelRV						  
			oPrintRV:Preview()						
		EndIf
	
		DBSelectArea(cTMPAlias)
		DBCloseArea()

		If ValType(oPrintRV) == "O"
			FreeObj(oPrintRV)
			oPrintRV := Nil
		EndIf
	
	Else
		Help( ,, "TURR004PRO001",, STR0122, 1, 0 )//"Nenhum registro foi encontrado."
		Return(.F.)	
	EndIf
	
	
	RestArea(aG3PArea)
	RestArea(aG3QArea)
	RestArea(aG3RArea)
	RestArea(aG3SArea)
	RestArea(aG3TArea)
	RestArea(aG3UArea)
	RestArea(aG3VArea)
	RestArea(aG3ZArea)
	RestArea(aG40Area)
	RestArea(aG44Area)
	RestArea(aG4DArea)
	RestArea(aSA1Area)
	RestArea(aSA2Area)
		
	DBSelectArea(cOldArea)
	RestArea(aOldArea)
		
Return(.T.)
	
		                                  
/*/{Protheus.doc} CreatPDF
Fun��o para definicao das Fontes a serem utilizadas
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     05/04/2016
/*/
Static Function CreatPDF(oPrintRV)
		
Private oFont06    := TFontEx():New(oPrintRV, "Arial", 06, 06, .F., .T., .F.)
Private oFont06N   := TFontEx():New(oPrintRV, "Arial", 06, 06, .T., .T., .F.)
Private oFont07    := TFontEx():New(oPrintRV, "Arial", 07, 07, .F., .T., .F.)
Private oFont07N   := TFontEx():New(oPrintRV, "Arial", 07, 07, .T., .T., .F.)
Private oFont08    := TFontEx():New(oPrintRV, "Arial", 08, 08, .F., .T., .F.)
Private oFont08N   := TFontEx():New(oPrintRV, "Arial", 08, 08, .T., .T., .F.)
Private oFont09    := TFontEx():New(oPrintRV, "Arial", 09, 09, .F., .T., .F.)
Private oFont09N   := TFontEx():New(oPrintRV, "Arial", 09, 09, .T., .T., .F.)
Private oFont10    := TFontEx():New(oPrintRV, "Arial", 10, 10, .F., .T., .F.)
Private oFont10N   := TFontEx():New(oPrintRV, "Arial", 10, 10, .T., .T., .F.)
Private oFont11    := TFontEx():New(oPrintRV, "Arial", 11, 11, .F., .T., .F.)
Private oFont11N   := TFontEx():New(oPrintRV, "Arial", 11, 11, .T., .T., .F.)
Private oFont12    := TFontEx():New(oPrintRV, "Arial", 12, 12, .F., .T., .F.)
Private oFont12N   := TFontEx():New(oPrintRV, "Arial", 12, 12, .T., .T., .F.)
Private oFont13    := TFontEx():New(oPrintRV, "Arial", 13, 13, .F., .T., .F.)
Private oFont13N   := TFontEx():New(oPrintRV, "Arial", 13, 13, .T., .T., .F.)
Private oFont14    := TFontEx():New(oPrintRV, "Arial", 14, 14, .F., .T., .F.)
Private oFont14N   := TFontEx():New(oPrintRV, "Arial", 14, 14, .T., .T., .F.)
Private oFont15    := TFontEx():New(oPrintRV, "Arial", 15, 15, .F., .T., .F.)
Private oFont15N   := TFontEx():New(oPrintRV, "Arial", 15, 15, .T., .T., .F.)
Private oFont16    := TFontEx():New(oPrintRV, "Arial", 16, 16, .F., .T., .F.)
Private oFont16N   := TFontEx():New(oPrintRV, "Arial", 16, 16, .T., .T., .F.)
Private oFont17    := TFontEx():New(oPrintRV, "Arial", 17, 17, .F., .T., .F.)
Private oFont17N   := TFontEx():New(oPrintRV, "Arial", 17, 17, .T., .T., .F.)
Private oFont18    := TFontEx():New(oPrintRV, "Arial", 18, 18, .F., .T., .F.)
Private oFont18N   := TFontEx():New(oPrintRV, "Arial", 18, 18, .T., .T., .F.)
	                
GeraPDF(@oPrintRV)
		
Return(.T.)


/*/{Protheus.doc} GeraPDF
Fun��o para gera��o do PDF
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     05/04/2016
/*/
Static Function GeraPDF(oPrintRV)

Local nLin		 := 0100
Local aDadosG3M:= {}

// Com base nos dados do R.V Retorna dados do posto de atendimento 		
aDadosG3M := GetAdvFVal("G3M",{'G3M_DESCR','G3M_NIATA'}, FwxFilial("G3M") + G3Q->G3Q_POSTO,1 )

// CABECALHO  
nLin += 45                        
oPrintRV:Say( nLin , 0468,AllTrim(SubStr(SM0->M0_NOMECOM,1,60)),   oFont14N:oFont )
nLin += 45
oPrintRV:Say( nLin , 0468,AllTrim(SubStr(SM0->M0_ENDCOB,1,35)),   oFont12:oFont )
oPrintRV:Say( nLin, 1450, AllTrim(SubStr( SM0->M0_BAIRCOB,1,20)),   oFont12:oFont )
nLin += 45
oPrintRV:Say( nLin, 0468, AllTrim(SubStr(SM0->M0_CIDCOB,1,30)),   oFont12:oFont )
oPrintRV:Say( nLin, 1256, SM0->M0_ESTCOB,   oFont12:oFont )
oPrintRV:Say( nLin, 1450, OEMTOANSI(STR0001) , oFont12:oFont ) //"BRASIL"
oPrintRV:Say( nLin, 1713, STR0002  + TransForm(SM0->M0_CEPCOB, PesqPict("SA1", "A1_CEP")),   oFont12:oFont ) //"CEP "
nLin += 45
oPrintRV:Say( nLin, 0468, OEMTOANSI(STR0003) , oFont12:oFont ) //"FONE"
oPrintRV:Say( nLin, 0682, TransForm(SM0->M0_TEL, "@R (99) 9999-9999" ),   oFont12:oFont )
oPrintRV:Say( nLin, 1256, OEMTOANSI(STR0004) , oFont12:oFont ) //"FAX"
oPrintRV:Say( nLin, 1450, TransForm(SM0->M0_FAX, "@R (99) 9999-9999" ),   oFont12:oFont )
nLin += 45
oPrintRV:Say( nLin, 0468, OEMTOANSI(STR0005) , oFont12:oFont ) //"HomePage"
oPrintRV:Say( nLin, 0682, OEMTOANSI("www.alatur.com.br") , oFont12:oFont )
oPrintRV:Say( nLin, 1256, OEMTOANSI(STR0006) , oFont12:oFont ) //"E-mail"
oPrintRV:Say( nLin, 1450, OEMTOANSI("alatur@alatur.com.br") , oFont12:oFont )
nLin += 45
oPrintRV:Say( nLin, 0468, OEMTOANSI(STR0007) , oFont12:oFont ) //"CNPJ"
oPrintRV:Say( nLin, 0682, TransForm(SM0->M0_CGC, "@R 99.999.999/9999-99" ),   oFont12:oFont )
oPrintRV:Say( nLin, 1256, OEMTOANSI(STR0008) , oFont12:oFont ) //"Embratur"
oPrintRV:Say( nLin, 1450, IIf(Empty(AllTrim(GetMV("MV_TUREMBT"))), "MV_TUREMBT", AllTrim(GetMV("MV_TUREMBT")) ),   oFont12:oFont )
nLin += 45
oPrintRV:Say( nLin, 0468, OEMTOANSI(STR0123), oFont12:oFont ) //""Emp./Fil.:""
oPrintRV:Say( nLin, 0682, AllTrim(SM0->M0_CODIGO + ' - ' + AllTrim(SM0->M0_CODFIL) +' - ' + ; //--Codigo
							 AllTrim(SubStr(SM0->M0_NOME,1,30)) + ' - ' + AllTrim(SubStr(SM0->M0_FILIAL,1,30))),   oFont12:oFont )//--Descricao
nLin += 45
oPrintRV:Say( nLin, 0468, OEMTOANSI(STR0124) , oFont12:oFont ) //"Posto"
If !aScan(aDadosG3M, Nil) > 0
	oPrintRV:Say( nLin, 0682, AllTrim(SubStr(aDadosG3M[1],1,30)),   oFont12:oFont )
EndIf

oPrintRV:Say( nLin, 1256, OEMTOANSI(STR0009) , oFont12:oFont ) //"IATA"
If !aScan(aDadosG3M, Nil) > 0
	oPrintRV:Say( nLin, 1450, AllTrim(SubStr(aDadosG3M[2],1,15)),   oFont12:oFont )
EndIf

DBSelectArea("G44")
DBSetOrder(1)
DBSeek(G3Q->G3Q_FILIAL + G3Q->G3Q_NUMID + G3Q->G3Q_IDITEM + G3Q->G3Q_NUMSEQ)
	
&("PrintSeg" + G3Q->G3Q_TPSEG + "()")
	
Return(.T.)


/*/{Protheus.doc} PrintSeg1
Fun��o para impress�o de Protocolo A�reo
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     05/04/2016
/*/
Static Function PrintSeg1()


Local cChavG3Q := G3Q->G3Q_FILIAL + G3Q->G3Q_NUMID + G3Q->G3Q_IDITEM + G3Q->G3Q_NUMSEQ 
Local cChavG3T := G3Q->G3Q_FILIAL + G3Q->G3Q_NUMID + G3Q->G3Q_IDITEM + G3Q->G3Q_NUMSEQ
Local nI		 := 0
Local nLin		 := 0
Local nVezes	 := 0
Local nInicio	 := 1
Local cCiaAerea:= ""
Local cCartao	:= (cTMPAlias)->G4D_NUMCAR //Numero cartao 
Local cNSU		:= (cTMPAlias)->G4D_NSU   //Numero NSU 
Local cBanco 	:= (cTMPAlias)->G4D_CODBAN //Codigo banco
Local aDadosSA2:= {}

	//PROTOCOLO AEREO

	DBSelectArea("G3T")
	DBSetOrder(1)
	DBSeek(G3Q->G3Q_FILIAL + G3Q->G3Q_NUMID + G3Q->G3Q_IDITEM + G3Q->G3Q_NUMSEQ)
	          
	
	nLin := 0586
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0011) , oFont12N:oFont ) //"Protocolo Venda Cartao de Credito"
	oPrintRV:Say( nLin, 0850, SubStr(Posicione("SX5", 1, xFilial("SX5") + "L9" + cBanco, "SX5->X5_DESCRI"),1,15), oFont12:oFont )
	oPrintRV:Say( nLin, 1256, "CARD\NSU: "+ TRANSFORM( cCartao, X3Picture('G4D_NUMCAR')) + " \ " + TRANSFORM( cNSU, X3Picture('G4D_NSU')),   oFont12:oFont )
	nLin += 53 //0639
	
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0127) , oFont12N:oFont ) //"Registro de Venda"  
	oPrintRV:Say( nLin, 0571, TransForm(G3P->G3P_NUMID, PesqPict("G3P", "G3P_NUMID")), oFont12:oFont, 1000 )
	nLin += 53//0692
	
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0012) , oFont12:oFont ) //"Cliente"
	oPrintRV:Say( nLin, 0456, AllTrim(SA1->A1_COD +'/'+ SA1->A1_LOJA +' - '+ AllTrim(SubStr(SA1->A1_NOME,1,30))),   oFont12:oFont )
	nLin += 53 //0745
	
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0013) , oFont12:oFont )  //"Endereco"
	oPrintRV:Say( nLin, 0456, SubStr( AllTrim(SA1->A1_END),1,30),   oFont12:oFont )
	oPrintRV:Say( nLin, 1256, OEMTOANSI(STR0014) , oFont12:oFont ) //"Cidade"
	oPrintRV:Say( nLin, 1450, SubStr(AllTrim(SA1->A1_MUN),1,11),   oFont12:oFont )
	oPrintRV:Say( nLin, 1827, AllTrim(SA1->A1_EST),   oFont12:oFont )
	oPrintRV:Say( nLin, 1910, TransForm(SA1->A1_CEP, PesqPict("SA1", "A1_CEP")),   oFont12:oFont )
	nLin += 54 //0799
	
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0015) , oFont12:oFont ) //"Emissor"
	oPrintRV:Say( nLin, 0456, SubStr(AllTrim(Posicione("G3H", 1, xFilial("G3H") + G3P->G3P_AGENT, "G3H_NOMEAG")),1,30),   oFont12:oFont )
	oPrintRV:Say( nLin, 1256, OEMTOANSI(STR0016) , oFont12:oFont ) //"Emissao"
	oPrintRV:Say( nLin, 1450, DToc(G3P->G3P_EMISS),   oFont12:oFont )
	nLin += 53 //0852
	
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0017) , oFont12:oFont ) //"Forma Pagto"
	oPrintRV:Say( nLin, 0456, AllTrim(G3Q->G3Q_DESCFP),   oFont12:oFont )
	nLin += 54 //0906
	
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0125) , oFont12:oFont ) //"CNPJ\CPF"
	oPrintRV:Say( nLin, 0456, TransForm(SA1->A1_CGC, "@R 99.999.999/9999-99" ),   oFont12:oFont )	
	nLin += 53 //0959
	
	
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0018) , oFont12N:oFont ) //"Cia Aerea"
	oPrintRV:Say( nLin, 0900, OEMTOANSI(STR0019) , oFont12N:oFont ) //"Passageiro/C.Custo/Solicitante"
	nLin += 53//1012
	
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0020) , oFont12N:oFont ) //"Bilhete"
	oPrintRV:Say( nLin, 0900, OEMTOANSI(STR0021) , oFont12N:oFont ) //"Rota"
	nLin += 54//1066
	
	oPrintRV:Say( nLin, 0479, OEMTOANSI(STR0022) , oFont12N:oFont ) //"Cambio"
	oPrintRV:Say( nLin, 0708, OEMTOANSI(STR0023) , oFont12N:oFont ) //"Valor Base"
	oPrintRV:Say( nLin, 0970, OEMTOANSI(STR0024) , oFont12N:oFont ) //"Valor Nacional"
	oPrintRV:Say( nLin, 1350, OEMTOANSI(STR0025) , oFont12N:oFont ) //"Taxas"
	oPrintRV:Say( nLin, 1702, OEMTOANSI(STR0026) , oFont12N:oFont ) //"Desconto"
	oPrintRV:Say( nLin, 2055, OEMTOANSI(STR0027) , oFont12N:oFont ) //"Total"
	nLin += 53 //1119

	oPrintRV:Line( nLin, 0171, nLin, 2284,, )
	nLin += 53 //1172
	//-- CIA
	aDadosSA2:= GetAdvFVal("SA2",{'A2_COD','A2_LOJA','A2_NREDUZ'}, xFilial("SA2") + G3T->G3T_CODFOR + G3T->G3T_LOJAF,1 )
	//-- Valida Retorno dos dados do fornecedor
	If !aScan(aDadosSA2, Nil) > 0
		oPrintRV:Say( nLin, 0171, AllTrim(aDadosSA2[1] + " - " + aDadosSA2[2] + " - " + SubStr(aDadosSA2[3],1,20)),   oFont12:oFont )
	EndIf
	
	//-- BILHETE
	oPrintRV:Say( nLin+53 , 0171, AllTrim(G3Q->G3Q_DOC),   oFont12:oFont )
	
	//--- Lista os passageiros do v�o
	While ( G3S->G3S_FILIAL + G3S->G3S_NUMID + G3S->G3S_IDITEM + G3S->G3S_NUMSEQ == cChavG3Q )
	
		If  (nLin  >  (oPrintRV:NPAGEHEIGHT - 250) )
			oPrintRV:StartPage()
			nLin := 0159
		EndIf	 	
	
	
		oPrintRV:Say( nLin , 0900, AllTrim(G3S->G3S_NOMINV) + STR0028  + AllTrim(G3Q->G3Q_CCUSTO) + STR0029 + AllTrim(Posicione("SU5", 1, xFilial("SU5") + G3Q->G3Q_SOLIC, "U5_CONTAT")),   oFont12:oFont ) //" / C.Custo " //" / Solicitante: "
	
		nLin += 60	
					
		G3S->( DbSkip() )
	
	EndDo
	

	//-- Lista os aereos do documento	
	While ( G3T->G3T_FILIAL + G3T->G3T_NUMID + G3T->G3T_IDITEM + G3T->G3T_NUMSEQ == cChavG3T )

		If  (nLin + 120)  >  (oPrintRV:NPAGEHEIGHT - 250) 
			//For�a a inclus�o de uma nova p�gina 
			oPrintRV:StartPage()
			nLin := 0159
		EndIf	

		nLin += 60				
		oPrintRV:Say( nLin, 0900, IIF(G3R->G3R_DESTIN=="1",STR0030,STR0031) + " - " + AllTrim(G3T->G3T_TERORI) + " / " + AllTrim(G3T->G3T_TERDST),   oFont12:oFont ) //"Nacional" //"Internacional
		
		oPrintRV:Say( nLin, 1450, OEMTOANSI("N") , oFont12:oFont )	
		oPrintRV:Say( nLin, 1500, AllTrim(SubStr(Posicione("G3C", 1, xFilial("G3C") + G3T->G3T_CLASRV, "G3C_DESCR"),1,30)),   oFont12:oFont )
		
		nLin += 60
		oPrintRV:Say( nLin, 0900, STR0032 + DToC(G3T->G3T_DTSAID) + STR0033 + DToC(G3T->G3T_DTCHEG),   oFont12:oFont ) //"Data da Viagem: " //" ate "
	
	
		G3T->( DbSkip() )
	EndDo
	
	If  (nLin + 330)  >  (oPrintRV:NPAGEHEIGHT - 250) 
		//For�a a inclus�o de uma nova p�gina 
		oPrintRV:StartPage()
		nLin := 0159
	EndIf	 			
	
	//-------
	
	nLin += 60
	oPrintRV:Say( nLin, 0342, AllTrim(Posicione("G5T", 1, xFilial("G5T") + G3Q->G3Q_MOEDA , "G5T_SIMBOL")),   oFont12:oFont )
	
	oPrintRV:Say( nLin, 0479, AllTrim(TransForm(G44->G44_TXCAMB, PesqPict("G44","G44_TXCAMB"))),   oFont12:oFont )
	
	oPrintRV:Say( nLin, 0708, AllTrim(TransForm(G44->G44_TARBAS, PesqPict("G44","G44_TARBAS"))),   oFont12:oFont )
	
	oPrintRV:Say( nLin, 0970, AllTrim(TransForm(G44->G44_APLICA, PesqPict("G44","G44_APLICA"))),   oFont12:oFont )

	oPrintRV:Say( nLin, 1350, AllTrim(TransForm(G3R->G3R_TAXA + G3Q->G3Q_TAXADU, PesqPict("G3R","G3R_TAXA"))),   oFont12:oFont )

	oPrintRV:Say( nLin, 1702, AllTrim(TransForm(G3Q->G3Q_DESC, PesqPict("G3Q","G3Q_DESC"))),   oFont12:oFont )
	
	oPrintRV:Say( nLin, 2055, AllTrim(TransForm(G3Q->G3Q_VLRSER, PesqPict("G3Q","G3Q_VLRSER"))),   oFont12:oFont )
	
	nLin += 60
	oPrintRV:Line( nLin, 0171, nLin, 2284,, )
	
	nLin += 60
	oPrintRV:Say( nLin, 0593, OEMTOANSI(STR0034) , oFont12N:oFont ) //"Total Geral"
	
	oPrintRV:Say( nLin, 0970, AllTrim(TransForm(G44->G44_APLICA, PesqPict("G44","G44_APLICA"))),   oFont12N:oFont )
	
	oPrintRV:Say( nLin, 1350, AllTrim(TransForm(G3R->G3R_TAXA + G3Q->G3Q_TAXADU, PesqPict("G3R","G3R_TAXA"))),   oFont12N:oFont )
	
	oPrintRV:Say( nLin, 1702, AllTrim(TransForm(G3Q->G3Q_DESC, PesqPict("G3Q","G3Q_DESC"))),   oFont12N:oFont )
	
	oPrintRV:Say( nLin, 2055, AllTrim(TransForm(G3Q->G3Q_VLRSER, PesqPict("G3Q","G3Q_VLRSER"))),   oFont12N:oFont )
	
	
	nLin += 60
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0035) , oFont12:oFont ) //"Recebi(emos) os Servicos/Produtos (passagens e/ou voucher) acima relacionados na forma de pgto. descrita e reconheco(emos) como divida liquida"
	
	nLin += 30
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0036) , oFont12:oFont ) //"e certa a importancia constante dessa requisicao nos termos do art. 585 do codigo de processo civil."
	
	nLin += 60
	oPrintRV:Say( nLin, 0171, AllTrim(SubStr(SM0->M0_CIDCOB,1,30)),   oFont12:oFont )
	
	oPrintRV:Say( nLin, 1000, cValToChar(Day(dDataBase)) + STR0116 + MesExtenso(dDataBase) + STR0037 + cValToChar(Year(dDataBase)),   oFont12:oFont ) //" de "
	
	oPrintRV:Say( nLin, 1358, OEMTOANSI(STR0038 + "________________________________________") , oFont12:oFont ) //"Assinatura: "

Return


/*/{Protheus.doc} PrintSeg2
Fun��o para impress�o de Voucher de Hotel
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     05/04/2016
/*/
Static Function PrintSeg2() 

Local lFirstPage   := .T.
Local cChaveG3U    := ""
Local cMoeda	   := ""
Local cEndCob	   := ""
Local cSimbol 	   := ""
Local nLinNextPage := 0852
Local aMensag	   := {}
Local nOrig		   := 1
Local nI 		   := 1
Local nTarifa	   := 1
Local nResult      := 0 //Incluido Ronaldo 
Local cObs         := ""
Local nIX          := 1
Local nTotTaxa     := 0


	DBSelectArea("G3U")
	DBSetOrder(1)
	DBSeek(G3Q->G3Q_FILIAL + G3Q->G3Q_NUMID + G3Q->G3Q_IDITEM + G3Q->G3Q_NUMSEQ)
	
	cChaveG3U := G3Q->G3Q_FILIAL + G3Q->G3Q_NUMID + G3Q->G3Q_IDITEM + G3Q->G3Q_NUMSEQ

	cSimbol := AllTrim(Posicione("G5T", 1, xFilial("G5T") + G3Q->G3Q_MOEDA , "G5T_SIMBOL"))	

	Aadd(aMensag, {STR0040,'HOTEL VOUCHER '}) //""
	Aadd(aMensag, {STR0041,'To'}) //"To"
	Aadd(aMensag, {STR0042,'Phone'}) //"Phone"
	Aadd(aMensag, {STR0043,'District'}) //"District"
	Aadd(aMensag, {STR0044,STR0044}) //"Fax"
	Aadd(aMensag, {STR0045,'Zip Code'}) //"Zip Code"
	Aadd(aMensag, {STR0046,STR0046}) //"E-mail"
	Aadd(aMensag, {STR0047,'OUR REFERENCE'}) //"OUR REFERENCE"
	Aadd(aMensag, {STR0048,'Please provide the following services'}) //"Please provide the following services"
	Aadd(aMensag, {STR0049,'Guests'}) //"Guests"
	Aadd(aMensag, {STR0051,'Qtt'}) //"Qtt"
	Aadd(aMensag, {STR0050,'Apartament'}) //"Apartament"
	Aadd(aMensag, {STR0052,'Guest'}) //"Guest"
	Aadd(aMensag, {STR0053,'IN'}) //"Entrada"
	Aadd(aMensag, {STR0054,'OUT'}) //"Sa�da"
	Aadd(aMensag, {STR0055,'Nights'}) //"Nights"
	Aadd(aMensag, {STR0056,'Amount'}) //"Amount"
	Aadd(aMensag, {STR0057,'Rates Amount'}) //"Rates Amount"
	Aadd(aMensag, {STR0058,'Daily Rates Value'}) //"Daily Rates Value"
	Aadd(aMensag, {STR0059,'The values expressed in this voucher are in'}) //"The values expressed in this voucher are in"
	Aadd(aMensag, {STR0060,'Meal Plan'}) //"Meal Plan"
	Aadd(aMensag, {STR0061,'Payment Form'}) //"Payment Form"
	Aadd(aMensag, {STR0062,'Confirmation'}) //"Confirmation"
	Aadd(aMensag, {STR0063,'P.S.:'}) //"P.S.:"
	Aadd(aMensag, {STR0066,'Voucher generated in'}) //"Voucher generated in"
	Aadd(aMensag, {STR0067,'Signature: ________________________________________'}) //"Signature: ________________________________________"
	Aadd(aMensag, {STR0068,'Printer user'}) //"Printer user"
	Aadd(aMensag, {STR0069,'Issuing agent:'}) //"Agente emissor"
	Aadd(aMensag, {STR0070,'Customer'}) //"Customer"
		
	If G3P->G3P_DESTIN == "1"

		//-- Guarda o endereco de cobran�a com base no Segmento de Neg�cio	
		If !Empty(AllTrim(GetMV(AllTrim("MV_ENDCOB"+ G3P->G3P_SEGNEG))))
			cEndCob := GetMV(AllTrim("MV_ENDCOB"+ G3P->G3P_SEGNEG))
		EndIf
		
			
		// VOUCHER HOTEL NACIONAL
		oPrintRV:FWMSBAR("CODE128" /*cTypeBar*/,0014/*nRow*/ ,0002/*nCol*/, G3Q->G3Q_NUMID + G3Q->G3Q_IDITEM + G3Q->G3Q_NUMSEQ/*cCode*/,oPrintRV/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02/*nWidth*/,1.2/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,2/*nPFWidth*/,2/*nPFHeigth*/,.F./*lCmtr2Pix*/)
		oPrintRV:Say( 0586, 0171, OEMTOANSI(STR0039 + cEndCob ) , oFont12N:oFont ) // "Endereco de Cobranca: "
		oPrintRV:Say( 0810, 0285, G3Q->G3Q_NUMID + G3Q->G3Q_IDITEM + G3Q->G3Q_NUMSEQ,   oFont12:oFont )
		
		cMoeda	:= "R$"
		nOrig	:= 1
	Else	
		cMoeda	:=  cSimbol
		nOrig	:= 2
	EndIf
		
		nLin := 1172 

		While ( G3U->G3U_FILIAL + G3U->G3U_NUMID + G3U->G3U_IDITEM + G3U->G3U_NUMSEQ == cChaveG3U )

			If  ( lFirstPage .Or. ( (nLin + 450)  >  oPrintRV:NPAGEHEIGHT) )
			 
			 	If !lFirstPage
					oPrintRV:StartPage()
					nLinNextPage := 0159
				EndIf
				
				oPrintRV:Box( nLinNextPage, 0114, 2611, 2284,, )
				
				nLinNextPage += 45
				oPrintRV:Say( nLinNextPage, 0799, aMensag[1][nOrig] + G3P->G3P_NUMID,   oFont12N:oFont ) // "VOUCHER DE HOTEL - N� "		
				
				nLinNextPage += 43
				oPrintRV:Say( nLinNextPage, 0171, OEMTOANSI(aMensag[2][nOrig]) , oFont12:oFont ) //"Para"		
				oPrintRV:Say( nLinNextPage, 0285, AllTrim(SA2->A2_COD + " "+ SA2->A2_LOJA + " - " + SA2->A2_NREDUZ),   oFont12:oFont )
				
				
						
				oPrintRV:Say( nLinNextPage, 1484, OEMTOANSI(aMensag[3][nOrig]) , oFont12:oFont ) //"Fone"		
				oPrintRV:Say( nLinNextPage, 1633, "(" + AllTrim(SA2->A2_DDD) + ") " + ALLTRIM(TRANSFORM(SA2->A2_TEL,PesqPict('SA2','A2_TEL'))),   oFont12:oFont )		
				
				nLinNextPage += 43
				oPrintRV:Say( nLinNextPage, 0285, SubStr( AllTrim(SA2->A2_END),1, 30) ,   oFont12:oFont )		
				oPrintRV:Say( nLinNextPage, 0970, OEMTOANSI(aMensag[4][nOrig]) , oFont12:oFont ) //"Bairro"		
				oPrintRV:Say( nLinNextPage, 1100, SubStr(AllTrim(SA2->A2_BAIRRO),1,15),   oFont12:oFont )		
				oPrintRV:Say( nLinNextPage, 1484, OEMTOANSI(aMensag[5][nOrig]) , oFont12:oFont ) //"Fax"		
				oPrintRV:Say( nLinNextPage, 1633, "(" + AllTrim(SA2->A2_DDD) + ") " + ALLTRIM(TRANSFORM(SA2->A2_FAX,PesqPict('SA2','A2_FAX'))),   oFont12:oFont )		
				
				nLinNextPage += 45
				oPrintRV:Say( nLinNextPage, 0285, SubStr(AllTrim(SA2->A2_MUN),1,26) + " - " + SA2->A2_EST,   oFont12:oFont )	
				oPrintRV:Say( nLinNextPage, 0970, OEMTOANSI(aMensag[6][nOrig]) , oFont12:oFont ) //"CEP"		
				oPrintRV:Say( nLinNextPage, 1150, TransForm(SA2->A2_CEP, PesqPict("SA2", "A2_CEP")),   oFont12:oFont )		
				oPrintRV:Say( nLinNextPage, 1484, OEMTOANSI(STR0046) , oFont12:oFont ) //"E-mail"		
				oPrintRV:Say( nLinNextPage, 1633, Lower(AllTrim(SubStr(SA2->A2_EMAIL,1,32))),   oFont12:oFont )		
				
				nLinNextPage += 43
				oPrintRV:Say( nLinNextPage, 0285, OEMTOANSI(STR0007) , oFont12:oFont ) //"CNPJ"
				oPrintRV:Say( nLinNextPage, 0400, TransForm(SA2->A2_CGC, "@R 99.999.999/9999-99" ) , oFont12:oFont ) //"CNPJ"
				
				
				nLinNextPage += 43
				oPrintRV:Say( nLinNextPage, 0171, OEMTOANSI(aMensag[8][nOrig]) , oFont12N:oFont ) //"NOSSA REFERENCIA"		
				oPrintRV:Say( nLinNextPage, 0593, AllTrim(G3R->G3R_NUMID + G3R->G3R_IDITEM ),   oFont12N:oFont )					
				
				nLinNextPage += 43
				oPrintRV:Say( nLinNextPage, 0171, OEMTOANSI(aMensag[9][nOrig]) , oFont12:oFont ) //"Solicitamos os seguintes servicos"			
				
				lFirstPage := .F.
				nLin := nLinNextPage//1225
			EndIf	
			
			nLin += 43
			oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[10][nOrig]) , oFont12N:oFont ) //"Hospedes"	
			oPrintRV:Say( nLin, 0354, AllTrim(G3U->G3U_NOMPAX),   oFont12:oFont )
		
			nLin += 43
			
			oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[12][nOrig]) , oFont12N:oFont ) //"Apartamento"
			oPrintRV:Say( nLin, 0880, OEMTOANSI(aMensag[13][nOrig]) , oFont12N:oFont ) //"Pax"
			oPrintRV:Say( nLin, 0993, OEMTOANSI(aMensag[14][nOrig]) , oFont12N:oFont ) //"Entrada"
			oPrintRV:Say( nLin, 1221, OEMTOANSI(aMensag[15][nOrig]) , oFont12N:oFont ) //"Saida"
			oPrintRV:Say( nLin, 1421, OEMTOANSI(aMensag[11][nOrig]) , oFont12N:oFont ) //"Qtde"
			oPrintRV:Say( nLin, 1571, OEMTOANSI(aMensag[16][nOrig]) , oFont12N:oFont ) //"Diaria"
			oPrintRV:Say( nLin, 1700, OEMTOANSI(STR0126) , oFont12N:oFont ) //"Vlr. Diaria"
			oPrintRV:Say( nLin, 1900, OEMTOANSI("Vlr. Taxa") , oFont12N:oFont ) //"Vlr. Taxa" //Incluido Ronaldo
			oPrintRV:Say( nLin, 2155, OEMTOANSI(aMensag[17][nOrig]) , oFont12N:oFont ) //"Total"
		
			nLin += 43
			
			oPrintRV:Say( nLin, 0171, SubStr( AllTrim(Posicione("G4J", 1, xFilial("G4J") + G3U->G3U_CATPRO, "G4J_DESCR")),1,26),   oFont12:oFont )
			oPrintRV:Say( nLin, 0880, '1',   oFont12:oFont )
			oPrintRV:Say( nLin, 0993, StrZero(Day(G3U->G3U_DTINI), 2) + Upper(SubStr(CMonth(G3U->G3U_DTINI), 1, 3)),   oFont12:oFont )
			oPrintRV:Say( nLin, 1221, StrZero(Day(G3U->G3U_DTFIM), 2) + Upper(SubStr(CMonth(G3U->G3U_DTFIM), 1, 3)),   oFont12:oFont )
			oPrintRV:Say( nLin, 1421, AllTrim(Str(G3U->G3U_QTDPRD)),   oFont12:oFont )
			oPrintRV:Say( nLin, 1571, AllTrim(Str(G3U->G3U_QTDDIA)),   oFont12:oFont )
			
			oPrintRV:Say( nLin, 1650, OEMTOANSI(cMoeda) , oFont12:oFont )
			oPrintRV:Say( nLin, 1750, AllTrim(TransForm(G44->G44_VLRDIA , PesqPict("G44", "G44_APLICA"))),   oFont12:oFont )
			nTotTaxa := G3R->G3R_TAXA + G44->G44_PLALIM 		
			oPrintRV:Say( nLin, 1950, AllTrim(TransForm(nTotTaxa, PesqPict("G3R", "G3R_TAXA"))),   oFont12:oFont )//Incluido Ronaldo
						
			oPrintRV:Say( nLin, 2100, OEMTOANSI(cMoeda) , oFont12:oFont )
			nTarifa := ((G44->G44_VLRDIA * G3U->G3U_QTDDIA) * G3U->G3U_QTDPRD )
			nResult := nTarifa + G3R->G3R_TAXA + G44->G44_PLALIM 
			oPrintRV:Say( nLin, 2155, AllTrim(TransForm(nResult, PesqPict("G44", "G44_APLICA"))),   oFont12:oFont )
						
			nLin += 45
			oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0059) , oFont12:oFont ) //"Os valores expressos neste voucher estao em"
			oPrintRV:Say( nLin, 0993, OEMTOANSI("BRL (REAL)") , oFont12:oFont )
			
			nLin += 43
			oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[21][nOrig]) , oFont12:oFont ) //"Plano Alimentar"
			oPrintRV:Say( nLin, 0628, Upper(AllTrim(Tabela("AH", G3U->G3U_PLALIM, .F.))),   oFont12:oFont )
			
			nLin += 43
			oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[22][nOrig]) , oFont12:oFont ) //Forma de pago
			oPrintRV:Say( nLin, 0628, AllTrim(G3Q->G3Q_DESCFP),   oFont12:oFont )
			
			nLin += 43
			oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[23][nOrig]) , oFont12:oFont ) //"Confirmacao"
			oPrintRV:Say( nLin, 0628, AllTrim(G3R->G3R_FORMCF) + " " + AllTrim(G3R->G3R_IDCONF) + " " + IIf(!Empty(G3R->G3R_DTCONF), DToC(G3R->G3R_DTCONF), ""),   oFont12:oFont )
			
			nLin += 43
			
			oPrintRV:Say( nLin, 0171, SubStr( AllTrim(G3U->G3U_OBS),1, 117),   oFont12:oFont )
		
			nLin += 43
		
			G3U->( DbSkip() )
			
			If G3U->G3U_FILIAL + G3U->G3U_NUMID + G3U->G3U_IDITEM + G3U->G3U_NUMSEQ <> cChaveG3U
				oPrintRV:Line( nLin, 0171, nLin, 2226,, )
				nLin += 43
				oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[19][nOrig]) , oFont12N:oFont ) //"Total do Voucher"
				oPrintRV:Say( nLin, 1907, OEMTOANSI(cMoeda) , oFont12N:oFont )                                                                                      
				//oPrintRV:Say( nLin, 2055, AllTrim(TransForm(G44->G44_APLICA, PesqPict("G44", "G44_APLICA"))),   oFont12N:oFont )
				nResult := G44->G44_APLICA + G3R->G3R_TAXA + G44->G44_PLALIM
				oPrintRV:Say( nLin, 2155, AllTrim(TransForm(nResult, PesqPict("G44", "G44_APLICA"))),   oFont12N:oFont )//Incluido Ronaldo
			EndIf
		
		EndDo
		
		
		If  (  (nLin + 562 + (oPrintRV:NPAGEHEIGHT - 2611)  )  >  oPrintRV:NPAGEHEIGHT ) 
			oPrintRV:StartPage()
			nLin := 0159
			oPrintRV:Box( nLin, 0114, 2611, 2284,, )
			nLin += 43			
		EndIf
		
		//Incluido Ronaldo
		If !Empty(G3R->G3R_OBS)
			cObs := ALLTRIM(G3R->G3R_OBS)
		Else
			cObs := ALLTRIM(G3Q->G3Q_OBSFAT)
		EndIf
		If Len(cObs) > 0
			nLin += 65
			oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0063) , oFont12:oFont ) //"Observacoes"
	
	  		For nIX := 1 To Len(cObs) Step 100
				oPrintRV:Say( nLin, 0350, Substr(cObs,nIx,100) , oFont12:oFont )
				nLin += 43
			Next
		EndIf
		//Final da Inclus�o Ronaldo
		/*
		nLin += 43
		oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[24][nOrig]) , oFont12:oFont ) //"Observacoes"
		oPrintRV:Say( nLin, 0628, AllTrim(G3Q->G3Q_OBSFAT),   oFont12:oFont )
		
		nLin += 43
		oPrintRV:Say( nLin, 0171, AllTrim(G3R->G3R_OBS),   oFont12N:oFont )
		*///Comentado linhas acima para que o relatorio n�o estoure na impress�o
		If nOrig == 1
			nLin += 43
			oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0064) , oFont12:oFont ) //"Taxas de turismo e impostos locais n�o est�o inclusos no valor da di�ria. Os mesmos ser�o cobrados conforme legisla��o"
		
			nLin += 43		
			oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0065) , oFont12:oFont ) //"Desde 01/jan/06, as principais redes hoteleiras associadas ao FOHB, nao faturam mais despesas extras, somente diarias."
		Else
			nLin += 43
			oPrintRV:Say( nLin, 0171, OEMTOANSI('CANCELLATION WITHOUT PENALTIES UNTIL 24HRS BEFORE THE "IN" DATE') , oFont12N:oFont )
			
			nLin += 43
			oPrintRV:Say( nLin, 0171, AllTrim(G3Q->G3Q_OBSFAT),   oFont12:oFont )
		
			nLin += 43
			oPrintRV:Say( nLin, 0171, OEMTOANSI("") , oFont12:oFont )
			
			nLin += 43
			oPrintRV:Say( nLin, 0171, OEMTOANSI("(CANCELAMENTO SEM PENALIDADES ATE24 HORAS ANTES DATA DE ENTRADA)") , oFont12N:oFont )		
		EndIf
				
		nLin += 86
		oPrintRV:Say( nLin, 0171, AllTrim(SubStr(SM0->M0_CIDCOB,1,30)),   oFont12:oFont )
		oPrintRV:Say( nLin, 1000, cValToChar(Day(dDataBase)) + STR0116 + MesExtenso(dDataBase) + STR0116 + cValToChar(Year(dDataBase)),   oFont12:oFont ) // " de "
		oPrintRV:Say( nLin, 1598, OEMTOANSI(aMensag[25][nOrig]) , oFont12:oFont ) //"Voucher gerado em"
		oPrintRV:Say( nLin, 1964, DtoC(G3Q->G3Q_EMISS),   oFont12:oFont )
		
		nLin += 175
		oPrintRV:Say( nLin, 1358, OEMTOANSI(aMensag[26][nOrig]) , oFont12:oFont ) //"Assinatura: ________________________________________"
		
		nLin += 43
		oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[27][nOrig]) , oFont12:oFont ) //"Usuario de impressao"
		oPrintRV:Say( nLin, 0560, Upper(AllTrim(SubStr(UsrFullName(__cUserID),1,30))),   oFont12:oFont )
		
		oPrintRV:Say( nLin, 1358, OEMTOANSI(aMensag[28][nOrig]) , oFont12:oFont ) //"Agente emissor:"
		oPrintRV:Say( nLin, 1655, Upper(AllTrim(Posicione("G3H", 1, XFILIAL("G3H") + G3R->G3R_AGRESE, "G3H_NOMEAG"))),   oFont12:oFont )
				
		
		//-- Rodap�				
		oPrintRV:Say( 2665, 0171, OEMTOANSI(aMensag[29][nOrig]) , oFont12:oFont ) //"Cliente"
		oPrintRV:Say( 2665, 0342, AllTrim(SA1->A1_COD +'/'+ SA1->A1_LOJA +' - '+ SA1->A1_NOME),   oFont12:oFont )
		
		oPrintRV:Say( 2718, 0342, SubStr(AllTrim(SA1->A1_END),1,40),   oFont12:oFont )
		oPrintRV:Say( 2718, 1393, OEMTOANSI(aMensag[4][nOrig]) , oFont12:oFont ) //"Bairro"
		oPrintRV:Say( 2718, 1565, SubStr( AllTrim(SA1->A1_BAIRRO),1,37 ),   oFont12:oFont )
		
		oPrintRV:Say( 2771, 0342, SubStr( AllTrim(SA1->A1_MUN) + "-" + AllTrim(SA1->A1_EST),1,32 ),   oFont12:oFont )
		oPrintRV:Say( 2771, 1393, OEMTOANSI(aMensag[6][nOrig]) , oFont12:oFont ) //"CEP"
		oPrintRV:Say( 2771, 1565, TransForm(SA1->A1_CEP, PesqPict("SA1", "A1_CEP")),   oFont12:oFont )
	

Return


/*/{Protheus.doc} PrintSeg3
Fun��o para impress�o de Voucher de Veiculo
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     05/04/2016
/*/
Static Function PrintSeg3() 

Local cCatAut := ""
Local cTpAut  := ""
Local cTraDir := ""
Local cCombAr := ""
Local aObsCar := {}
Local aMensag	:= {}
Local cChaveG3V	:= ""
Local cRetDev		:= ""
Local cSimbol		:= ""
Local cGrpCar		:= ""
Local lFirstPage	:= .T.
Local nOrig			:= 1
Local nI			:= 1
Local nLimiteBox	:= 1
Local nLin			:= 1
Local nTarifa		:= 1  
Local nResult       := 0
Local cObs          := ""
Local nIX           := 1
	
DBSelectArea("G3V")
DBSetOrder(1)
DBSeek(G3Q->G3Q_FILIAL + G3Q->G3Q_NUMID + G3Q->G3Q_IDITEM + G3Q->G3Q_NUMSEQ)

	cChaveG3V := G3Q->G3Q_FILIAL + G3Q->G3Q_NUMID + G3Q->G3Q_IDITEM + G3Q->G3Q_NUMSEQ

	cSimbol := AllTrim(Posicione("G5T", 1, xFilial("G5T") + G3Q->G3Q_MOEDA , "G5T_SIMBOL"))	
	
	Aadd(aMensag, {STR0071,'CAR VOUCHER Nr '}) //"VOUCHER DE LOCACAO N� "
	Aadd(aMensag, {STR0041,'To'}) //"Para"
	Aadd(aMensag, {STR0042,'Phone'} ) //"Fone"
	Aadd(aMensag, {STR0043,'District'} ) //"Bairro"
	Aadd(aMensag, {STR0044,STR0044}) //"Fax"
	Aadd(aMensag, {STR0045,'Zip'} ) //"CEP"
	Aadd(aMensag, {STR0046,STR0046}) //"E-mail"
	Aadd(aMensag, {STR0047,'OUR REFERENCE'} ) //"NOSSA REFERENCIA"
	Aadd(aMensag, {STR0072,'Please provide the following services:'} ) //"Confirmamos os seguintes servicos:"
	Aadd(aMensag, {STR0073,'Guests:'} ) //"Passageiros"
	Aadd(aMensag, {STR0051,'Qtt'} ) //"Qtde"
	Aadd(aMensag, {STR0080,'Car Group'} ) //"Grupo Carro"
	Aadd(aMensag, {STR0074,STR0074}) //"IN"
	Aadd(aMensag, {STR0075,STR0075}) //"OUT"
	Aadd(aMensag, {STR0076,'Rate'}) //"Tarifa"
	Aadd(aMensag, {'R$','AllTrim(Posicione("G5T", 1, xFilial("G5T") + G44->G44_MOEDFO , "G5T_SIMBOL"))'} ) //"SIMBOL"
	Aadd(aMensag, {STR0077,'Rate Composition'} ) //"Composicao da Tarifa"
	Aadd(aMensag, {STR0078,' Dayly'} ) //" Diaria"
	Aadd(aMensag, {STR0079,'Pick Up / Drop Off'} ) //"Retirada / Devolucao"
	Aadd(aMensag, {STR0081,'Amount'} ) //"Total do Voucher"
	Aadd(aMensag, {STR0059,'The values expressed in this voucher are in'} ) //"Os valores expressos neste voucher estao em"
	Aadd(aMensag, {'BRL (REAL)','AllTrim(Posicione("G5T", 1, xFilial("G5T") + G44->G44_MOEDFO , "G5T_SIMBOL"))'} ) //"BRL (REAL)"
	Aadd(aMensag, {STR0061,'Form of Payment'} ) //"Forma de Pagamento"
	Aadd(aMensag, {STR0062,'Confirmation'} ) //"Confirmacao"
	Aadd(aMensag, {STR0063,'P.S.:'} ) //"Observacoes"
	Aadd(aMensag, {STR0082,}) //"FATURAR DIARIAS E TAXAS"
	Aadd(aMensag, {STR0083,'Taxes are subject to change in according local legislation.'} ) //"Taxas de servicos e/ou outras estao sujeitas a alteracao em funcao de legislacao"
	Aadd(aMensag, {STR0084,}) //"REGRAS PARA LOCACAO: O motorista deve ser maior de 21 anos, ter no minimo 2 anos de habilitacao e apresentar o documento"
	Aadd(aMensag, {STR0085,}) //"O pagamento relativo a esse voucher esta condicionado ao envio de sua copia junto a nota fiscal correspondente"	
	Aadd(aMensag, {STR0116,STR0116}) //" de "
	Aadd(aMensag, {STR0066,'Voucher generated in'} ) //"Voucher gerado em"
	Aadd(aMensag, {STR0067,'Signature: ________________________________________'} ) //"Assinatura: ________________________________________"
	Aadd(aMensag, {STR0068,'Printer User'} ) //"Usuario de impressao"
	Aadd(aMensag, {STR0086,'Agent'} ) //"Agente"
	Aadd(aMensag, {STR0070,'Customer'} ) //"Cliente"
	
	
	If G3P->G3P_DESTIN == "1"
		nOrig := 1
	Else
		nOrig  := 2
	EndIf
	
	nLimiteBox := 2611 - 30
		
	// 104 - VOUCHER CARRO NACIONAL   
	oPrintRV:Box( 0586, 0114, 2611, 2284,, )
	
	nLin := 0586
	nLin += 43
	oPrintRV:Say( nLin, 0799, aMensag[1,nOrig] + G3P->G3P_NUMID,   oFont12N:oFont ) //"VOUCHER DE LOCACAO N� "
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[2,nOrig]) , oFont12:oFont ) //"Para"
	oPrintRV:Say( nLin, 0285, AllTrim(SA2->A2_COD + " "+ SA2->A2_LOJA + " - " + SA2->A2_NREDUZ),   oFont12:oFont )
	oPrintRV:Say( nLin, 1484, OEMTOANSI(aMensag[3,nOrig]) , oFont12:oFont ) //"Fone"
	oPrintRV:Say( nLin, 1633, "(" + AllTrim(SA2->A2_DDD) + ") " + ALLTRIM(TRANSFORM(SA2->A2_FAX,PesqPict('SA2','A2_FAX'))),   oFont12:oFont )
	
	nLin += 43
	oPrintRV:Say( nLin, 0285, SubStr( AllTrim(SA2->A2_END),1, 27),   oFont12:oFont )
	oPrintRV:Say( nLin, 0913, OEMTOANSI(aMensag[4,nOrig]) , oFont12:oFont ) //"Bairro"
	oPrintRV:Say( nLin, 1027, AllTrim(SA2->A2_BAIRRO),   oFont12:oFont )
	oPrintRV:Say( nLin, 1484, OEMTOANSI(aMensag[5,nOrig]) , oFont12:oFont ) //"Fax"	
	oPrintRV:Say( nLin, 1633, "(" + AllTrim(SA2->A2_DDD) + ") " + ALLTRIM(TRANSFORM(SA2->A2_FAX,PesqPict('SA2','A2_FAX'))),   oFont12:oFont )
	
	nLin += 43
	oPrintRV:Say( nLin, 0285, AllTrim(SA2->A2_BAIRRO),   oFont12:oFont )	
	oPrintRV:Say( nLin, 0913, OEMTOANSI(aMensag[6,nOrig]) , oFont12:oFont ) //"CEP"	
	oPrintRV:Say( nLin, 1027, TransForm(SA2->A2_CEP, PesqPict("SA2", "A2_CEP")),   oFont12:oFont )	
	oPrintRV:Say( nLin, 1484, OEMTOANSI(aMensag[7,nOrig]) , oFont12:oFont ) //"E-mail"	
	oPrintRV:Say( nLin, 1633, Lower(AllTrim(SubStr(SA2->A2_EMAIL,1,32))),   oFont12:oFont )
	
	nLin += 43
	oPrintRV:Say( nLin, 0285, OEMTOANSI(STR0007) , oFont12:oFont ) //"CNPJ"
	oPrintRV:Say( nLin, 0400, TransForm(SA2->A2_CGC, "@R 99.999.999/9999-99" ) , oFont12:oFont ) //"CNPJ"	
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[8,nOrig]) , oFont12N:oFont ) //"NOSSA REFERENCIA"	
	oPrintRV:Say( nLin, 0593, AllTrim(G3P->G3P_NUMID),   oFont12N:oFont )
				
				
	While G3V->G3V_FILIAL + G3V->G3V_NUMID + G3V->G3V_IDITEM + G3V->G3V_NUMSEQ == cChaveG3V
					
		cCatAut := AllTrim(Posicione("G4Y", 1, xFilial("G4Y") + G3V->G3V_CATAUT, "G4Y_DESCR" ))
		cTpAut  := AllTrim(Posicione("G4Z", 1, xFilial("G4Z") + G3V->G3V_TPAUT,  "G4Z_DESCR" ))
		cTraDir := AllTrim(Posicione("G50", 1, xFilial("G50") + G3V->G3V_TRADIR, "G50_DESCR" ))
		cCombAr := AllTrim(Posicione("G51", 1, xFilial("G51") + G3V->G3V_COMBAR, "G51_DESCR" ))				
										
		If  ( (nLin + 429)  >  nLimiteBox ) 
			oPrintRV:StartPage()
			oPrintRV:Box( 0586, 0114, 2611, 2284,, )	
			nLin := 639 - 54 
		EndIf
			
		nLin += 43	
		oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[9,nOrig]) , oFont12:oFont ) //"Confirmamos os seguintes servicos:"
		
		nLin += 43
		oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[10,nOrig]) , oFont12N:oFont ) //"Passageiros"
		oPrintRV:Say( nLin, 0411, AllTrim(G3V->G3V_NOMPAX),   oFont12:oFont )
	
		nLin += 43
				
		oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[12,nOrig]) , oFont12N:oFont ) //"Grupo Carro"	
		oPrintRV:Say( nLin, 1200, OEMTOANSI(aMensag[11,nOrig]) , oFont12N:oFont ) //"Qtde"		
		oPrintRV:Say( nLin, 1400, OEMTOANSI(aMensag[18,nOrig]) , oFont12N:oFont ) //"Di�ria"				
		oPrintRV:Say( nLin, 1600, OEMTOANSI(STR0126) , oFont12N:oFont ) //"Vlr. Di�ria"
		oPrintRV:Say( nLin, 1850, OEMTOANSI("Vlr. Taxa") , oFont12N:oFont ) //"Vlr. taxa"  //Incluido Ronaldo
		oPrintRV:Say( nLin, 2050, OEMTOANSI(aMensag[15,nOrig]) , oFont12N:oFont ) //"Tarifa"
		
		//---------------------------------------------------------------------------------------------					
		nLin += 43
			
		cGrpCar := SubStr(AllTrim(cCatAut + ", " + cTpAut  + ", " + cTraDir + ", " + cCombAr),1,60)		
		oPrintRV:Say( nLin, 0171,cGrpCar,   oFont12:oFont )		
		oPrintRV:Say( nLin, 1200, AllTrim(Str(G3V->G3V_QTDPRD)),   oFont12:oFont )
		oPrintRV:Say( nLin, 1400, AllTrim(Str(G3V->G3V_QTDDIA)) + aMensag[18,nOrig] + IIf(G3V->G3V_QTDDIA > 1, "s", ""),   oFont12:oFont ) //" Diaria"
		
		oPrintRV:Say( nLin, 1650, AllTrim(TransForm(G44->G44_VLRDIA, PesqPict("G44", "G44_VLRDIA"))),   oFont12:oFont )
		nTarifa := ( (G44->G44_VLRDIA * G3V->G3V_QTDDIA ) * G3V->G3V_QTDPRD )//-- VLR DIARIA X QTD DIARIA X QTD PRODUTO
		
		oPrintRV:Say( nLin, 1900, AllTrim(TransForm(G3R->G3R_TAXA, PesqPict("G3R", "G3R_TAXA"))),   oFont12:oFont )//Incluido Ronaldo
		nResult := nTarifa + G3R->G3R_TAXA 		
		oPrintRV:Say( nLin, 2050, AllTrim(TransForm(nResult, PesqPict("G44", "G44_APLICA"))),   oFont12:oFont )
	
		nLin += 43
	
		oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0074) , oFont12N:oFont ) //"IN"
		oPrintRV:Say( nLin, 0493, OEMTOANSI(STR0075) , oFont12N:oFont ) //"OUT"
		oPrintRV:Say( nLin, 0700, OEMTOANSI(aMensag[19,nOrig]) , oFont12N:oFont ) //"Retirada / Devolucao"	
		
		nLin += 43		
		oPrintRV:Say( nLin, 0171, StrZero(Day(G3V->G3V_DTINI), 2) + Upper(SubStr(CMonth(G3V->G3V_DTINI), 1, 3)) + " " + TransForm(G3V->G3V_HRINI, PesqPict("G3V", "G3V_HRINI")),   oFont12:oFont )
		oPrintRV:Say( nLin, 0493, StrZero(Day(G3V->G3V_DTFIM), 2) + Upper(SubStr(CMonth(G3V->G3V_DTFIM), 1, 3)) + " " + TransForm(G3V->G3V_HRFIM, PesqPict("G3V", "G3V_HRFIM")),   oFont12:oFont )
		cRetDev := SubStr(AllTrim(Posicione("G5S", 1, xFilial("G5S") + G3V->G3V_CIDRET, "G5S_CIDADE")) + " / " + AllTrim(Posicione("G5S", 1, xFilial("G5S") + G3V->G3V_CIDDEV, "G5S_CIDADE") ),1,60)
		oPrintRV:Say( nLin, 700, cRetDev ,   oFont12:oFont )
							
		nLin += 86
		
		G3V->( DbSkip() )
		
		If G3V->G3V_FILIAL + G3V->G3V_NUMID + G3V->G3V_IDITEM + G3V->G3V_NUMSEQ <> cChaveG3V
			oPrintRV:Line( nLin, 0171, nLin, 2226,, )
			nLin += 43
			oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[20,nOrig]) , oFont12N:oFont ) //"Total do Voucher"			
			oPrintRV:Say( nLin, 1907, IIF(nOrig == 1, OEMTOANSI("R$"), OEMTOANSI(cSimbol)), oFont12N:oFont )//-- R$
			nResult := G44->G44_APLICA+G3R->G3R_TAXA //Incluido Ronaldo
			//oPrintRV:Say( nLin, 2000, AllTrim(TransForm(G44->G44_APLICA, PesqPict("G44", "G44_APLICA"))),   oFont12N:oFont )
			oPrintRV:Say( nLin, 2050, AllTrim(TransForm(nResult, PesqPict("G44", "G44_APLICA"))),   oFont12N:oFont )//Incluido Ronaldo
		EndIf
		
	EndDo
			
	If  (  (nLin + 900)  >  nLimiteBox ) 
		oPrintRV:StartPage()
		oPrintRV:Box( 0159, 0114, 2611, 2284,, )	
		nLin := 215  
	EndIf

	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[21,nOrig]) , oFont12:oFont ) //"Os valores expressos neste voucher estao em"
	oPrintRV:Say( nLin, 0993, OEMTOANSI("BRL (REAL)") , oFont12:oFont )
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[23,nOrig]) , oFont12:oFont ) //"Forma de Pagamento"
	oPrintRV:Say( nLin, 0628, AllTrim(G3Q->G3Q_DESCFP),   oFont12:oFont )
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[24,nOrig]) , oFont12:oFont ) //"Confirmacao"
	oPrintRV:Say( nLin, 0628, AllTrim(G3R->G3R_FORMCF) + " " + AllTrim(G3R->G3R_IDCONF) + " " + IIf(!Empty(G3R->G3R_DTCONF), DToC(G3R->G3R_DTCONF), ""),   oFont12:oFont )
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[25,nOrig]) , oFont12:oFont ) //"Observacoes"
	If nOrig == 1
		oPrintRV:Say( nLin, 0628, OEMTOANSI(STR0082) , oFont12:oFont ) //"FATURAR DIARIAS E TAXAS"
	EndIf
	
	//Incluido Ronaldo
	If !Empty(G3R->G3R_OBS)
		cObs := ALLTRIM(G3R->G3R_OBS)
	Else
		cObs := ALLTRIM(G3Q->G3Q_OBSFAT)
	EndIf
	If Len(cObs) > 0
		nLin += 43
		oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0063) , oFont12:oFont ) //"Observacoes"
	
		For nIX := 1 To Len(cObs) Step 100
			oPrintRV:Say( nLin, 0350, Substr(cObs,nIx,100) , oFont12:oFont )
			nLin += 43
		Next
	EndIf
	//Final da Inclus�o Ronaldo
		
	nLin += 107
	oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[27,nOrig]) , oFont12:oFont ) //"Taxas de servicos e/ou outras estao sujeitas a alteracao em funcao de legislacao"
	
	If nOrig == 1
		nLin += 43
		oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0084) , oFont12:oFont ) //"REGRAS PARA LOCACAO: O motorista deve ser maior de 21 anos, ter no minimo 2 anos de habilitacao e apresentar o documento"
	
		nLin += 43
		oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0085) , oFont12:oFont ) //"O pagamento relativo a esse voucher esta condicionado ao envio de sua copia junto a nota fiscal correspondente"
	EndIf
	
	
	nLin += 250
	oPrintRV:Say( nLin, 0171, AllTrim(SubStr(SM0->M0_CIDCOB,1,30)),   oFont12:oFont )
	oPrintRV:Say( nLin, 1000, cValToChar(Day(dDataBase)) + aMensag[30,nOrig] + MesExtenso(dDataBase) + STR0116 + cValToChar(Year(dDataBase)),   oFont12:oFont ) // " de "
	oPrintRV:Say( nLin, 1598, OEMTOANSI(aMensag[31,nOrig]) , oFont12:oFont ) //"Voucher gerado em"
	oPrintRV:Say( nLin, 1964, DtoC(G3Q->G3Q_EMISS),   oFont12:oFont )
	
	nLin += 213
	oPrintRV:Say( nLin, 1358, OEMTOANSI(aMensag[32,nOrig]) , oFont12:oFont ) //"Assinatura: ________________________________________"
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(aMensag[33,nOrig]) , oFont12:oFont ) //"Usuario de impressao"
	oPrintRV:Say( nLin, 0560, Upper(AllTrim(SubStr(UsrFullName(__cUserID),1,30))),   oFont12:oFont )
	oPrintRV:Say( nLin, 1358, OEMTOANSI(aMensag[34,nOrig]) , oFont12:oFont ) //"Agente"
	oPrintRV:Say( nLin, 1598, Upper(AllTrim(Posicione("G3H", 1, XFILIAL("G3H") + G3R->G3R_AGRESE, "G3H_NOMEAG"))),   oFont12:oFont )
	
	
	//-- Rodap� 
	oPrintRV:Say( 2665, 0171, OEMTOANSI(aMensag[35,nOrig]) , oFont12:oFont ) //"Cliente"
	oPrintRV:Say( 2665, 0342, AllTrim(SA1->A1_COD +'/'+ SA1->A1_LOJA +' - '+ SA1->A1_NOME),   oFont12:oFont )
	
	
	oPrintRV:Say( 2718, 0342, SubStr(AllTrim(SA1->A1_END),1,40),   oFont12:oFont )
	oPrintRV:Say( 2718, 1393, OEMTOANSI(aMensag[4,nOrig]) , oFont12:oFont ) //"Bairro"
	oPrintRV:Say( 2718, 1565, SubStr( AllTrim(SA1->A1_BAIRRO),1,30 ),   oFont12:oFont )

	oPrintRV:Say( 2771, 0342, SubStr(AllTrim(SA1->A1_MUN) + " - " + AllTrim(SA1->A1_EST),1,32),   oFont12:oFont )
	oPrintRV:Say( 2771, 1393, OEMTOANSI(aMensag[6,nOrig]) , oFont12:oFont ) //"CEP"
	oPrintRV:Say( 2771, 1565, TransForm(SA1->A1_CEP, PesqPict("SA1", "A1_CEP")),   oFont12:oFont )
		
Return


/*/{Protheus.doc} PrintSeg8
Fun��o para impress�o de Voucher de Seguro
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     05/04/2016
/*/
Static Function PrintSeg8()

Local cSimbol := AllTrim(Posicione("G5T", 1, xFilial("G5T") + G3Q->G3Q_MOEDA , "G5T_SIMBOL"))	
Local nLin 	:= 600

	// 107 - VOUCHER SEGURO           
	oPrintRV:Say( nLin, 0799, STR0087 + AllTrim(G3P->G3P_NUMID),   oFont12N:oFont ) //-- ORDEM DE SERVICO XXXXXXXXX
	
	nLin += 43
	oPrintRV:Say( nLin, 0000, OEMTOANSI("") , oFont12:oFont )
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0088) , oFont12N:oFont ) //"Data"
	oPrintRV:Say( nLin, 0285, DtoC(G3R->G3R_EMISS),   oFont12N:oFont )
	oPrintRV:Say( nLin, 1484, OEMTOANSI("PC") , oFont12N:oFont )
		
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0041) , oFont12:oFont ) //"Para"
	oPrintRV:Say( nLin, 0285, AllTrim(SA2->A2_NOME),   oFont12:oFont )
	oPrintRV:Say( nLin, 1484, OEMTOANSI(STR0042) , oFont12:oFont ) //"Fone"
	oPrintRV:Say( nLin, 1633, "(" + AllTrim(SA2->A2_DDD) + ") " + ALLTRIM(TRANSFORM(SA2->A2_TEL,PesqPict('SA2','A2_TEL'))),   oFont12:oFont )
	
	nLin += 43
	oPrintRV:Say( nLin, 0285, SubStr(AllTrim(SA2->A2_END),1, 28),   oFont12:oFont )
	oPrintRV:Say( nLin, 0913, OEMTOANSI(STR0043) , oFont12:oFont ) //"Bairro"
	oPrintRV:Say( nLin, 1027, AllTrim(SA2->A2_BAIRRO),   oFont12:oFont )
	oPrintRV:Say( nLin, 1484, OEMTOANSI(STR0044) , oFont12:oFont ) //"Fax"
	oPrintRV:Say( nLin, 1633, "(" + AllTrim(SA2->A2_DDD) + ") " + ALLTRIM(TRANSFORM(SA2->A2_FAX,PesqPict('SA2','A2_FAX'))),   oFont12:oFont )
	
	nLin += 45
	oPrintRV:Say( nLin, 0285, AllTrim(SA2->A2_MUN) + "-" + SA2->A2_EST,   oFont12:oFont )
	oPrintRV:Say( nLin, 0913, OEMTOANSI(STR0045) , oFont12:oFont ) //"CEP"
	oPrintRV:Say( nLin, 1027, TransForm(SA2->A2_CEP, PesqPict("SA2", "A2_CEP")),   oFont12:oFont )
	oPrintRV:Say( nLin, 1484, OEMTOANSI(STR0046) , oFont12:oFont ) //"E-mail"
	oPrintRV:Say( nLin, 1633, Lower(AllTrim(SubStr(SA2->A2_EMAIL,1,32))),   oFont12:oFont )
	
	nLin += 106
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0089) , oFont12:oFont ) //"Prezados Senhores,"
	
	nLin += 45
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0090) , oFont12:oFont ) //"Vimos pela presente solicitar a emissao das seguintes apolices de seguro para debito em nossa conta-corrente"
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0091) , oFont12N:oFont ) //"Passageiro"
	oPrintRV:Say( nLin, 1256, OEMTOANSI(STR0061) , oFont12N:oFont ) //"Forma de Pagamento"
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, AllTrim(G3S->G3S_NOMINV) + " " + IIf(Val(G3S->G3S_TPPAX)  > 0 .and. Val(G3S->G3S_TPPAX)  < 7 , _aPassType[Val(G3S->G3S_TPPAX )], "") + " " + IIf(Val(G3S->G3S_TRATAM) > 0 .and. Val(G3S->G3S_TRATAM) < 5 , _aPassTrat[Val(G3S->G3S_TRATAM)], ""),   oFont12:oFont )
	oPrintRV:Say( nLin, 1256, OEMTOANSI(STR0092) , oFont12:oFont ) //"Faturado"
	
	nLin += 43
	oPrintRV:Say( nLin, 0628, OEMTOANSI(STR0076) , oFont12:oFont ) //"Tarifa"
	oPrintRV:Say( nLin, 1713, OEMTOANSI(STR0093) , oFont12:oFont ) //"Taxas"
	
	nLin += 10
	oPrintRV:Line( nLin, 0171, nLin, 1199,, )
	oPrintRV:Line( nLin, 1256, nLin, 2226,, )
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0051) , oFont12N:oFont ) //"Qtde"	
	oPrintRV:Say( nLin, 0628, cSimbol,   oFont12N:oFont )	
	oPrintRV:Say( nLin, 1084, cSimbol,   oFont12N:oFont )	
	oPrintRV:Say( nLin, 1256, OEMTOANSI(STR0093) , oFont12N:oFont ) //"Taxas"	
	oPrintRV:Say( nLin, 1461, OEMTOANSI(STR0051) , oFont12N:oFont ) //"Qtde"	
	oPrintRV:Say( nLin, 1621, OEMTOANSI(STR0094) , oFont12N:oFont ) //"Moeda Base"	
	oPrintRV:Say( nLin, 1987, cSimbol,   oFont12N:oFont )	
	oPrintRV:Say( nLin, 2112, cSimbol,   oFont12N:oFont )	
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI("1") , oFont12:oFont )
	oPrintRV:Say( nLin, 0628, AllTrim(TransForm(G3Q->G3Q_TARIFA, PesqPict("G3Q","G3Q_TARIFA"))) , oFont12:oFont )	
	oPrintRV:Say( nLin, 1084, AllTrim(TransForm(G3Q->G3Q_TARIFA, PesqPict("G3Q","G3Q_TARIFA"))) , oFont12:oFont )	
	oPrintRV:Say( nLin, 1256, OEMTOANSI("1"),   oFont12:oFont )	
	oPrintRV:Say( nLin, 1837, AllTrim(TransForm(G3Q->G3Q_TAXA, PesqPict("G3Q","G3Q_TAXA"))), oFont12:oFont )	
	oPrintRV:Say( nLin, 2112, AllTrim(TransForm(G3Q->G3Q_TAXA, PesqPict("G3Q","G3Q_TAXA"))), oFont12:oFont )
			
	nLin += 107		
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0095) , oFont12N:oFont ) //"Total Tarifas"
	oPrintRV:Say( nLin, 1084, AllTrim(TransForm(G3Q->G3Q_TARIFA, PesqPict("G3Q","G3Q_TARIFA"))) , oFont12:oFont )
	oPrintRV:Say( nLin, 1256, OEMTOANSI(STR0096) , oFont12N:oFont ) //"Total Taxas"
	oPrintRV:Say( nLin, 2112, AllTrim(TransForm(G3Q->G3Q_TAXA, PesqPict("G3Q","G3Q_TAXA"))), oFont12:oFont )
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0022) , oFont12N:oFont ) //"Cambio"	
	oPrintRV:Say( nLin, 0628, AllTrim(TransForm(G3Q->G3Q_TXCAMB, PesqPict("G3Q","G3Q_TXCAMB"))) , oFont12:oFont )	
	oPrintRV:Say( nLin, 1256, OEMTOANSI(STR0034) , oFont12N:oFont ) //"Total Geral"	
	oPrintRV:Say( nLin, 1987, cSimbol,   oFont12N:oFont )	
	oPrintRV:Say( nLin, 2112, AllTrim(TransForm(G3Q->G3Q_VLRSER, PesqPict("G3Q","G3Q_VLRSER"))) , oFont12N:oFont )
	
	nLin += 107
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0062) , oFont12N:oFont ) //"Confirmacao"	
	oPrintRV:Say( nLin, 0628, AllTrim(G3R->G3R_FORMCF) + " " + ;
									AllTrim(G3R->G3R_IDCONF) + " " + ;
									IIf(!Empty(G3R->G3R_DTCONF), DToC(G3R->G3R_DTCONF), ""),   oFont12:oFont )
	
	nLin += 586
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0097) , oFont12:oFont ) //"Atenciosamente,"	
	
	nLin += 160
	oPrintRV:Say( nLin, 0171, SM0->M0_NOMECOM,   oFont12:oFont )	
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0098) , oFont12:oFont ) //"Emissor"	
	oPrintRV:Say( nLin, 0342, Upper(AllTrim(SubStr(UsrFullName(__cUserID),1,30))),   oFont12:oFont )
	
	nLin += 267
	oPrintRV:Say( nLin, 0285, OEMTOANSI("") , oFont12:oFont )

Return


/*/{Protheus.doc} PrintSeg9
Fun��o para impress�o de Voucher de Tour
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     05/04/2016
/*/
Static Function PrintSeg9()

Local cChaveG3S	:= ""
Local cChaveG40	:= ""
Local cSimbol		:= ""
Local nLimiteBox 	:= 1	
Local nLin			:= 1
Local nI			:= 1
Local aObsG40 	:= {}


DBSelectArea("G40")
DBSetOrder(1)
DBSeek(G3Q->G3Q_FILIAL + G3Q->G3Q_IDITEM + G3Q->G3Q_NUMID + G3Q->G3Q_NUMSEQ)
	
cChaveG3S := 	G3Q->G3Q_FILIAL + G3Q->G3Q_NUMID + G3Q->G3Q_IDITEM + G3Q->G3Q_NUMSEQ 
cChaveG40 := 	G3Q->G3Q_FILIAL + G3Q->G3Q_NUMID + G3Q->G3Q_IDITEM + G3Q->G3Q_NUMSEQ 
	
	aObsG40 := CabeMemo(G40->G40_DSTOUR, 2560, "oFont12:oFont")
	
	// VOUCHER TOUR             
	oPrintRV:Box( 0586, 0114, 2611, 2284,, )
	nLin := 0586
	
	nLin += 43	
	oPrintRV:Say( nLin, 0799, STR0099 + G3P->G3P_NUMID,   oFont12N:oFont ) //"VOUCHER DE TOUR N� "	
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0041) , oFont12:oFont ) //"Para"	
	oPrintRV:Say( nLin, 0285, AllTrim(SA2->A2_NOME),   oFont12:oFont )	
	oPrintRV:Say( nLin, 1484, OEMTOANSI(STR0042) , oFont12:oFont ) //"Fone"	
	oPrintRV:Say( nLin, 1633, "(" + AllTrim(SA2->A2_DDD) + ") " + ALLTRIM(TRANSFORM(SA2->A2_TEL,PesqPict('SA2','A2_TEL'))),   oFont12:oFont )
	
	nLin += 43
	oPrintRV:Say( nLin, 0285, SubStr( AllTrim(SA2->A2_END),1, 30),   oFont12:oFont )	
	oPrintRV:Say( nLin, 0913, OEMTOANSI(STR0043) , oFont12:oFont ) //"Bairro"	
	oPrintRV:Say( nLin, 1027, SubStr(AllTrim(SA2->A2_BAIRRO),1,13),   oFont12:oFont )	
	oPrintRV:Say( nLin, 1484, OEMTOANSI(STR0044) , oFont12:oFont ) //"Fax"	
	oPrintRV:Say( nLin, 1633, "(" + AllTrim(SA2->A2_DDD) + ") " + ALLTRIM(TRANSFORM(SA2->A2_FAX,PesqPict('SA2','A2_FAX'))),   oFont12:oFont )
	
	nLin += 43
	oPrintRV:Say( nLin, 0285, SubStr(AllTrim(SA2->A2_MUN),1,23) + "-" + SA2->A2_EST,   oFont12:oFont )	
	oPrintRV:Say( nLin, 0913, OEMTOANSI(STR0045) , oFont12:oFont ) //"CEP"	
	oPrintRV:Say( nLin, 1027, TransForm(SA2->A2_CEP, PesqPict("SA2", "A2_CEP")),   oFont12:oFont )	
	oPrintRV:Say( nLin, 1484, OEMTOANSI(STR0046) , oFont12:oFont ) //"E-mail"	
	oPrintRV:Say( nLin, 1633, Lower(AllTrim(SubStr(SA2->A2_EMAIL,1,32))),   oFont12:oFont )
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0047) , oFont12N:oFont ) //"NOSSA REFERENCIA"
	oPrintRV:Say( nLin, 0593, TransForm(G3P->G3P_NUMID, PesqPict("G3P","G3P_NUMID")),   oFont12N:oFont )
	
	nLin += 43	
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0100) , oFont12:oFont ) //"Solicitamos os seguintes servicos:"
	
	//-- Lista os passageiros do Tour
	
	nLin += 53
	
	nLimiteBox := 2611 - 30
	
	While G3S->G3S_FILIAL + G3S->G3S_NUMID + G3S->G3S_IDITEM + G3S->G3S_NUMSEQ == cChaveG3S	
	
		If  ( (nLin + 106  )  >  nLimiteBox ) 
			oPrintRV:StartPage()
			nLin := 0159
			oPrintRV:Box( nLin, 0114, 2611, 2284,, )	
			nLin += 43
		EndIf		
	
		oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0073) , oFont12N:oFont ) //"Passageiros"
		oPrintRV:Say( nLin, 0411, AllTrim(G3S->G3S_NOMINV) + " " + IIf(Val(G3S->G3S_TPPAX)  > 0 .and. Val(G3S->G3S_TPPAX)  < 7 , _aPassType[Val(G3S->G3S_TPPAX )], "") + " " + IIf(Val(G3S->G3S_TRATAM) > 0 .and. Val(G3S->G3S_TRATAM) < 5 , _aPassTrat[Val(G3S->G3S_TRATAM)], ""),   oFont12:oFont )

		nLin += 43
		
		
		G3S->( DbSkip() )
	
	EndDo
	
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0101) , oFont12N:oFont ) //"Tour"	
	oPrintRV:Say( nLin, 0814, OEMTOANSI(STR0074) , oFont12N:oFont ) //"IN"	
	oPrintRV:Say( nLin, 1076, OEMTOANSI(STR0075) , oFont12N:oFont ) //"OUT"	
	oPrintRV:Say( nLin, 1350, OEMTOANSI(STR0102) , oFont12N:oFont ) //"Origem"	
	oPrintRV:Say( nLin, 1779, OEMTOANSI(STR0103) , oFont12N:oFont ) //"Destino"	

	nLin += 45
	oPrintRV:Say( nLin, 2055, OEMTOANSI(STR0056) , oFont12N:oFont ) //"Total"
	
	nLin += 45
	
	While G40->G40_FILIAL + G40->G40_NUMID + G40->G40_IDITEM + G40->G40_NUMSEQ == cChaveG40
	
	
		If  ( (nLin + 53  )  >  nLimiteBox ) 
			oPrintRV:StartPage()
			nLin := 0159
			oPrintRV:Box( nLin, 0114, 2611, 2284,, )	
			nLin += 43
		EndIf	
	
	
		oPrintRV:Say( nLin, 0171, SubStr(AllTrim(G40->G40_NMTOUR),1,30),   oFont12:oFont )		
		oPrintRV:Say( nLin, 0814, StrZero(Day(G40->G40_DTINI), 2) + Upper(SubStr(CMonth(G40->G40_DTINI),1,3)) + " " + TransForm(G40->G40_HRINI, PesqPict("G40", "G40_HRINI")),   oFont12:oFont )
		oPrintRV:Say( nLin, 1076, StrZero(Day(G40->G40_DTFIM), 2) + Upper(SubStr(CMonth(G40->G40_DTFIM),1,3)) + " " + TransForm(G40->G40_HRFIM, PesqPict("G40", "G40_HRFIM")),   oFont12:oFont )
		oPrintRV:Say( nLin, 1350, SubStr(AllTrim(Posicione("G5S", 1, xFilial("G5S") + G40->G40_CIDEMB, "G5S_CIDADE")),1,19),   oFont12:oFont )		
		oPrintRV:Say( nLin, 1779, SubStr(AllTrim(Posicione("G5S", 1, xFilial("G5S") + G40->G40_CIDPRI, "G5S_CIDADE")),1,19),   oFont12:oFont )
	
		nLin += 43
		
		G40->( DbSkip() )

	EndDo			
		
	//-- Verifica limute no Box
	If  ( (nLin + 1046  )  >  nLimiteBox ) 
		oPrintRV:StartPage()
		nLin := 0159
		oPrintRV:Box( nLin, 0114, 2611, 2284,, )	
		nLin += 43
	EndIf		
		
		
	nLin += 150
	
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0106) , oFont12:oFont ) //"Total Diarias"		
	
	cSimbol := AllTrim(Posicione("G5T", 1, xFilial("G5T") + G44->G44_MOEDFO , "G5T_SIMBOL"))
	oPrintRV:Say( nLin, 1941, cSimbol,   oFont12:oFont )
	oPrintRV:Say( nLin, 2055, AllTrim(TransForm(G3Q->G3Q_VLRSER, PesqPict("G3Q","G3Q_VLRSER"))),   oFont12:oFont )
	
	nLin += 43
	oPrintRV:Line( nLin, 0171, nLin, 2226,, )

	nLin += 45
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0081) , oFont12N:oFont ) //"Total do Voucher"	
	oPrintRV:Say( nLin, 1941, cSimbol,   oFont12N:oFont )
	oPrintRV:Say( nLin, 2055, AllTrim(TransForm(G3Q->G3Q_VLRSER, PesqPict("G3Q","G3Q_VLRSER"))),   oFont12N:oFont )

	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0081) , oFont12:oFont )	
	oPrintRV:Say( nLin, 0993, cSimbol,   oFont12:oFont )

	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0062) , oFont12:oFont ) //"Confirmacao"
	oPrintRV:Say( nLin, 0513, AllTrim(G3R->G3R_FORMCF) + " 53+" + AllTrim(G3R->G3R_IDCONF) + " " + IIf(!Empty(G3R->G3R_DTCONF), DToC(G3R->G3R_DTCONF), ""),   oFont12:oFont )

	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0107) , oFont12:oFont ) //"Descricao do Tour"

	If Len(aObsG40) > 0
		oPrintRV:Say( nLin, 0513, aObsG40[1],   oFont12:oFont )
	EndIf

	If Len(aObsG40) > 1
		nLin += 45
		oPrintRV:Say( nLin, 0513, aObsG40[2],   oFont12:oFont )
	EndIf

	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0061) , oFont12:oFont ) //"Forma de Pagamento"	
	oPrintRV:Say( nLin, 0513, AllTrim(G3Q->G3Q_DESCFP),   oFont12:oFont )
	
	nLin += 117
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0108) , oFont12:oFont ) //"Taxas e impostos estao sujeitos a alteracoes em funcao de legislacao local."
	
	nLin += 106
	oPrintRV:Say( nLin, 0171, AllTrim(SubStr(SM0->M0_CIDCOB,1,30)),   oFont12:oFont )
	oPrintRV:Say( nLin, 1000, cValToChar(Day(dDataBase)) + STR0116 + MesExtenso(dDataBase) + STR0116 + cValToChar(Year(dDataBase)),   oFont12:oFont ) // " de "
	oPrintRV:Say( nLin, 1598, OEMTOANSI(STR0066) , oFont12:oFont ) //"Voucher gerado em"
	oPrintRV:Say( nLin, 1964, DtoC(G3Q->G3Q_EMISS),   oFont12:oFont )
	
	nLin += 213
	oPrintRV:Say( nLin, 1358, OEMTOANSI(STR0067) , oFont12:oFont ) //"Assinatura: ________________________________________"
	
	nLin += 45
	oPrintRV:Say( nLin, 0171, OEMTOANSI("") , oFont12:oFont )
	oPrintRV:Say( nLin, 0285, OEMTOANSI("") , oFont12:oFont )
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0068) , oFont12:oFont ) //"Usuario de impressao"
	oPrintRV:Say( nLin, 0560, Upper(AllTrim(SubStr(UsrFullName(__cUserID),1,30))),   oFont12:oFont )
	oPrintRV:Say( nLin, 1358, OEMTOANSI(STR0086) , oFont12:oFont ) //"Agente"
	oPrintRV:Say( nLin, 1598, AllTrim(Posicione("G3H", 1, XFILIAL("G3H") + G3R->G3R_AGRESE, "G3H_NOMEAG")),   oFont12:oFont )
		
	//-- Rodap�
	oPrintRV:Say( 2665, 0171, OEMTOANSI(STR0070) , oFont12:oFont ) //"Cliente"
	oPrintRV:Say( 2665, 0342, AllTrim(SA1->A1_COD +'/'+ SA1->A1_LOJA +' - '+ SA1->A1_NOME),   oFont12:oFont )
			
	oPrintRV:Say( 2718, 0342, AllTrim(SA1->A1_END),   oFont12:oFont )
	oPrintRV:Say( 2718, 1393, OEMTOANSI(STR0043) , oFont12:oFont ) //"Bairro"
	oPrintRV:Say( 2718, 1565, AllTrim(SA2->A2_BAIRRO),   oFont12:oFont )
	
	oPrintRV:Say( 2771, 0342, AllTrim(SA1->A1_MUN) + "-" + AllTrim(SA1->A1_EST),   oFont12:oFont )
	oPrintRV:Say( 2771, 1393, OEMTOANSI(STR0045) , oFont12:oFont ) //"CEP"
	oPrintRV:Say( 2771, 1565, TransForm(SA1->A1_CEP, PesqPict("SA1", "A1_CEP")),   oFont12:oFont )
	

Return


/*/{Protheus.doc} PrintSegA
Fun��o para impress�o de Voucher de Pacote
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     05/04/2016
/*/
Static Function PrintSegA() 
	
Local aObsG3Z := {}
Local cSimbol := ""
Local nLin		:= 1

DBSelectArea("G3Z")
DBSetOrder(1)
DBSeek(G3Q->G3Q_FILIAL + G3Q->G3Q_NUMID + G3Q->G3Q_IDITEM + G3Q->G3Q_NUMSEQ)
	
	// 106 - VOUCHER PACOTE           
	oPrintRV:Box( 0586, 0114, 2611, 2284,, )
	nLin := 0586
	
	nLin += 43
	oPrintRV:Say( nLin, 0799, STR0109 + G3P->G3P_NUMID,   oFont12N:oFont ) //"VOUCHER DE PACOTE N� "
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0041) , oFont12:oFont ) //"Para"	
	oPrintRV:Say( nLin, 0285, AllTrim(SA2->A2_NOME),   oFont12:oFont )	
	oPrintRV:Say( nLin, 1484, OEMTOANSI(STR0042) , oFont12:oFont ) //"Fone"	
	oPrintRV:Say( nLin, 1633, "(" + AllTrim(SA2->A2_DDD) + ") " + ALLTRIM(TRANSFORM(SA2->A2_TEL,PesqPict('SA2','A2_TEL'))),   oFont12:oFont )
	
	nLin += 43
	oPrintRV:Say( nLin, 0285, SubStr( AllTrim(SA2->A2_END),1, 30),   oFont12:oFont )	
	oPrintRV:Say( nLin, 0913, OEMTOANSI(STR0043) , oFont12:oFont ) //"Bairro"	
	oPrintRV:Say( nLin, 1027, AllTrim(SA2->A2_BAIRRO),   oFont12:oFont )	
	oPrintRV:Say( nLin, 1484, OEMTOANSI(STR0044) , oFont12:oFont ) //"Fax"	
	oPrintRV:Say( nLin, 1633, "(" + AllTrim(SA2->A2_DDD) + ") " + ALLTRIM(TRANSFORM(SA2->A2_FAX,PesqPict('SA2','A2_FAX'))),   oFont12:oFont )
	
	nLin += 43
	oPrintRV:Say( nLin, 0285, SubStr(AllTrim(SA2->A2_MUN),1,23) + "-" + SA2->A2_EST,   oFont12:oFont )	
	oPrintRV:Say( nLin, 0913, OEMTOANSI(STR0045) , oFont12:oFont ) //"CEP"	
	oPrintRV:Say( nLin, 1027, TransForm(SA2->A2_CEP, PesqPict("SA2", "A2_CEP")),   oFont12:oFont )	
	oPrintRV:Say( nLin, 1484, OEMTOANSI(STR0046) , oFont12:oFont ) //"E-mail"	
	oPrintRV:Say( nLin, 1633, Lower(AllTrim(SubStr(SA2->A2_EMAIL,1,32))),   oFont12:oFont )
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0047) , oFont12N:oFont ) //"NOSSA REFERENCIA"	
	oPrintRV:Say( nLin, 0593, AllTrim(G3P->G3P_NUMID),   oFont12N:oFont )
		
	nLin += 43	
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0110) , oFont12:oFont ) //"Solicitamos os seguintes servicos:"	
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0111) , oFont12:oFont ) //"Hospedes"	
	oPrintRV:Say( nLin, 0411, AllTrim(G3S->G3S_NOMINV),   oFont12N:oFont )
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0051) , oFont12:oFont ) //"Qtde"	
	oPrintRV:Say( nLin, 0342, OEMTOANSI(STR0112) , oFont12:oFont ) //"Pacote"	
	oPrintRV:Say( nLin, 0628, OEMTOANSI(STR0113) , oFont12:oFont ) //"Pax"	
	oPrintRV:Say( nLin, 0708, OEMTOANSI(STR0074) , oFont12:oFont ) //"IN"	
	oPrintRV:Say( nLin, 0936, OEMTOANSI(STR0075) , oFont12:oFont ) //"OUT"	
	oPrintRV:Say( nLin, 1164, OEMTOANSI(STR0114) , oFont12:oFont ) //"Cidade Embarque"	
	oPrintRV:Say( nLin, 1713, OEMTOANSI(STR0105) , oFont12:oFont ) //"Valor"	
	oPrintRV:Say( nLin, 2055, OEMTOANSI(STR0056) , oFont12:oFont ) //"Total"
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, AllTrim(Str(G3Z->G3Z_QTDPAC)),   oFont12N:oFont )

	aObsG3Z := CabeMemo(G3Z->G3Z_DESPAC, 350, "oFont12:oFont")
	
	If Len(aObsG3Z) > 0
		
		oPrintRV:Say( nLin, 0342, aObsG3Z[1],   oFont12N:oFont )
	
	EndIf
	
	
	oPrintRV:Say( nLin, 0628, OEMTOANSI("1") , oFont12N:oFont )
	oPrintRV:Say( nLin, 0708, StrZero(Day(G3Z->G3Z_DTINI), 2) + Upper(SubStr(CMonth(G3Z->G3Z_DTINI), 1, 3)) + " " + TransForm(G3Z->G3Z_HRINI, PesqPict("G3Z", "G3Z_HRINI")),   oFont12N:oFont )
	oPrintRV:Say( nLin, 0936, StrZero(Day(G3Z->G3Z_DTFIM), 2) + Upper(SubStr(CMonth(G3Z->G3Z_DTFIM), 1, 3)) + " " + TransForm(G3Z->G3Z_HRFIM, PesqPict("G3Z", "G3Z_HRFIM")),   oFont12N:oFont )
	oPrintRV:Say( nLin, 1164, AllTrim(Posicione("G5S", 1, xFilial("G5S") + G3Z->G3Z_CIDEMB, "G5S_CIDADE")),   oFont12N:oFont )
	cSimbol := AllTrim(Posicione("G5T", 1, xFilial("G5T") + G44->G44_MOEDFO , "G5T_SIMBOL"))
	oPrintRV:Say( nLin, 1598, cSimbol,   oFont12N:oFont )
	oPrintRV:Say( nLin, 1713, AllTrim(TransForm(G3Q->G3Q_VLRSER, PesqPict("G3Q", "G3Q_VLRSER"))),   oFont12N:oFont )
	oPrintRV:Say( nLin, 1941, cSimbol,   oFont12N:oFont )
	oPrintRV:Say( nLin, 2055, AllTrim(TransForm(G3Q->G3Q_VLRSER, PesqPict("G3Q", "G3Q_VLRSER"))),   oFont12N:oFont )
	
	nLin += 106
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0115) , oFont12:oFont ) //"Descricao Pacote"
	
	aObsG3Z := CabeMemo(G3Z->G3Z_DESPAC, 2200, "oFont12:oFont")
	
	If Len(aObsG3Z) > 0
		oPrintRV:Say( nLin, 0513, aObsG3Z[1],   oFont12:oFont )
	EndIf
	
	If Len(aObsG3Z) > 1
		nLin += 43
		oPrintRV:Say( nLin, 0513, aObsG3Z[2],   oFont12:oFont )
	EndIf
	
	If Len(aObsG3Z) > 2
		nLin += 43
		oPrintRV:Say( nLin, 0513, aObsG3Z[3],   oFont12:oFont )
	EndIf
	
	If Len(aObsG3Z) > 3
		nLin += 43
		oPrintRV:Say( nLin, 0513, aObsG3Z[4],   oFont12:oFont )
	EndIf
	
	If Len(aObsG3Z) > 4
		nLin += 43
		oPrintRV:Say( nLin, 0513, aObsG3Z[5],   oFont12:oFont )
	EndIf
	
	nLin += 107
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0106) , oFont12:oFont ) //"Total Diarias"	
	oPrintRV:Say( nLin, 1941, cSimbol,   oFont12:oFont )	
	oPrintRV:Say( nLin, 2055, AllTrim(TransForm(G3Q->G3Q_VLRSER, PesqPict("G3Q", "G3Q_VLRSER"))),   oFont12:oFont )
	
	nLin += 43
	oPrintRV:Line( nLin, 0171, nLin, 2226,, )
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0081) , oFont12N:oFont ) //"Total do Voucher"	
	oPrintRV:Say( nLin, 1941, cSimbol,   oFont12N:oFont )	
	oPrintRV:Say( nLin, 2055, AllTrim(TransForm(G3Q->G3Q_VLRSER, PesqPict("G3Q", "G3Q_VLRSER"))),   oFont12N:oFont )
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0062) , oFont12:oFont ) //"Confirmacao"
	oPrintRV:Say( nLin, 0513, AllTrim(G3R->G3R_FORMCF) + " " + AllTrim(G3R->G3R_IDCONF) + " " + IIf(!Empty(G3R->G3R_DTCONF), DToC(G3R->G3R_DTCONF), ""),   oFont12:oFont )
	
	nLin += 43
	oPrintRV:Say( nLin, 0171, OEMTOANSI(STR0063) , oFont12:oFont ) //"Observacoes"
	
	If Len(aObsG3Z) > 0
		oPrintRV:Say( nLin, 0513, aObsG3Z[1],   oFont12:oFont )
	EndIf
	
	If Len(aObsG3Z) > 1
		nLin += 43
		oPrintRV:Say( nLin, 0513, aObsG3Z[2],   oFont12:oFont )
	EndIf
	
	If Len(aObsG3Z) > 2
		nLin += 43
		oPrintRV:Say( 1811, 0513, aObsG3Z[3],   oFont12:oFont )
	EndIf
	
	If Len(aObsG3Z) > 3
		nLin += 43
		oPrintRV:Say( nLin, 0513, aObsG3Z[4],   oFont12:oFont )
	EndIf
	
	If Len(aObsG3Z) > 4
		nLin += 43
		oPrintRV:Say( 1864, 0513, aObsG3Z[5],   oFont12:oFont )
	EndIf
	
	oPrintRV:Say( 2132, 0171, OEMTOANSI(STR0108) , oFont12:oFont ) //"Taxas e impostos estao sujeitos a alteracoes em funcao de legislacao local."
	
	oPrintRV:Say( 2238, 0171, AllTrim(SubStr(SM0->M0_CIDCOB,1,30)),   oFont12:oFont )
	oPrintRV:Say( 2238, 1000, cValToChar(Day(dDataBase)) + STR0116 + MesExtenso(dDataBase) + STR0116 + cValToChar(Year(dDataBase)),   oFont12:oFont ) // " de "
	oPrintRV:Say( 2238, 1598, OEMTOANSI(STR0066) , oFont12:oFont ) //"Voucher gerado em"	
	oPrintRV:Say( 2238, 1964, DtoC(G3Q->G3Q_EMISS),   oFont12:oFont )
	
	oPrintRV:Say( 2451, 1358, OEMTOANSI(STR0067) , oFont12:oFont ) //"Assinatura: ________________________________________"
	
	oPrintRV:Say( 2505, 0171, OEMTOANSI(STR0068) , oFont12:oFont ) //"Usuario de impressao"	
	oPrintRV:Say( 2505, 0560, Upper(AllTrim(SubStr(UsrFullName(__cUserID),1,30))),   oFont12:oFont )	
	oPrintRV:Say( 2505, 1358, OEMTOANSI(STR0086) , oFont12:oFont ) //"Agente"	
	oPrintRV:Say( 2505, 1598, AllTrim(Posicione("G3H", 1, XFILIAL("G3H") + G3R->G3R_AGRESE, "G3H_NOMEAG")),   oFont12:oFont )
	
	
	//-- 	RODAP� 
	oPrintRV:Say( 2665, 0171, OEMTOANSI(STR0070) , oFont12:oFont ) //"Cliente"	
	oPrintRV:Say( 2665, 0342, AllTrim(SA1->A1_NOME),   oFont12:oFont )
	
	oPrintRV:Say( 2718, 0342, AllTrim(SA1->A1_END),   oFont12:oFont )	
	oPrintRV:Say( 2718, 0970, OEMTOANSI(STR0043) , oFont12:oFont ) //"Bairro"	
	oPrintRV:Say( 2718, 1142, AllTrim(SA2->A2_BAIRRO),   oFont12:oFont )
	
	oPrintRV:Say( 2771, 0342, AllTrim(SA1->A1_MUN) + "-" + AllTrim(SA1->A1_EST),   oFont12:oFont )	
	oPrintRV:Say( 2771, 0970, OEMTOANSI(STR0045) , oFont12:oFont ) //"CEP"	
	oPrintRV:Say( 2771, 1142, TransForm(SA1->A1_CEP, PesqPict("SA1", "A1_CEP")),   oFont12:oFont )

Return


/*/{Protheus.doc} CabeMemo
Fun��o para quebra de campo memo pelo tamanho e pelos retornos de linha
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     12/04/2016
/*/
Static Function CabeMemo(cOriObs, nTamPixels, cFont) 

	Local aRetObs      := {}
	Local cOBS         := Upper(cOriObs)
	Local nTamObs      := Len(cOBS)
	Local nPosObs      := 0
	Local cLinObs      := ""
	Local cCR          := Chr(13)
	Local cLF          := Chr(10)
	
	DEFAULT nTamPixels := 2560
	DEFAULT cFont      := "oFont12"
	
	For nPosObs := 1 To nTamObs
	
		If Len(aRetObs) == 5
		
			Exit
		
		EndIf
		
		cAtuCar := SubStr(cOBS, nPosObs, 1)
		
		If cAtuCar == cCR .or. cAtuCar == cLF
		
			If SubStr(cOBS, nPosObs + 1, 1) == cLF
		
				nPosObs += 1
		
				cAtuCar := SubStr(cOBS, nPosObs, 1)
				
			EndIf
			
			If !Empty(AllTrim(cLinObs))
			
				AAdd(aRetObs, StrTran(StrTran(AllTrim(cLinObs), cCR, "" ), cLF,"" ))
				
				cLinObs := ""
				
			EndIf
		
		EndIf
		
		If cAtuCar <> cCR .and. cAtuCar <> cLF .and. cAtuCar <> cCR + cLF
		
			cLinObs += cAtuCar
			
		EndIf
		
		If oPrintRV:GetTextWidth(AllTrim(cLinObs), &(cFont) ) > nTamPixels
			
			If !Empty(AllTrim(cLinObs))
				
				AAdd(aRetObs, SubStr(AllTrim(cLinObs), 1, Len(AllTrim(cLinObs))-1))
				
				If nPosObs > 0
					
					nPosObs -= 1
				
				EndIf
				
				cLinObs := ""
				
			EndIf
			
		EndIf
		 
	Next

Return(aRetObs)
