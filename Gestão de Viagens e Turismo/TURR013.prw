#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"
#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'TURR013.CH'

#DEFINE CRLF Chr(13) + Chr(10)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURR13DET
Fun��o para impress�o de Fatura
@author    Cleyton
@version   1.00
@since     01/05/2016
/*/
//------------------------------------------------------------------------------------------
Function TURR013(oPrint,nLin,cCliPDF,cLojaPDF,cPrefixo,cFatura,nPagina,lReimp,cFilFat)

Local oFont11    := TFontEx():New(oPrint, "Courier", 11, 11, .T., .T., .F.)
Local oFont11N   := TFontEx():New(oPrint, "Arial"  , 11, 11, .T., .T., .F.)
Local oFont14    := TFontEx():New(oPrint, "Courier", 14, 14, .T., .T., .F.)
Local oFont16N   := TFontEx():New(oPrint, "Arial"  , 16, 16, .T., .T., .F.)
Local cAliasEad  := GetNextAlias()
Local cAliasTot  := GetNextAlias()
Local cAliasG8E  := GetNextAlias()
Local cAliasNFS  := ""
Local aTpAgru    := {STR0004,STR0005,STR0006,STR0007}  //"Entidades Adicionais","Solicitantes","Grupo de Produto","Padrao do Cliente"
Local aTitFina   := {}
Local aTitFisc   := {}
Local aTot		 := {}
Local aObs		 := {}
Local aAux		 := {}
Local aAreaSM0	 := {}
Local aTotais    := {0,0,0,0,0,0}
Local cDscEntTot := ""
Local cComple    := ""
Local cTpVen     := ""
Local cCodEad    := ""
Local cEnvio     := ""
Local cEntTot    := ""
Local cCodBar    := ""
Local cG85NumRV  := ""
Local cTotAnt	 := ""
Local cCampo     := ""
Local cOrder	 := ""
Local cObs		 := ""
Local cObsAux    := ""
Local cPefixo    := ""
Local cFilAux    := ""
Local lFirst	 := .T.
Local lTotaliza  := .T.
Local lObsOk     := .F.
Local nTotEad    := 0
Local nX 		 := 0
Local nLenTot    := 0
Local nCount	 := 0
Local nPosIni	 := 1
Local nIndexPre  := 0
Local nTotal	 := 0
Local nI		 := 0
Local nLinIni	 := 0
Local nPgBreak   := oPrint:nPageHeight*0.90 //2300
Local cConinu    := Space(TamSx3("G4C_CONINU")[1])
Local aEmail     := {}
Local nE         := 1
Local nSomaMail  := 0
Local lExistFunc := Findfunction('U_FTBLCFIN')
Local cTpApur	 := '1'
Local cExpG6LG85 := TurExpFil('G6L','G6L','G85','G85')
Local lFuncDTVen := Findfunction('U_TurDtVen')

Private oFont12  := TFontEx():New(oPrint,"Courier",12, 12,.T.,.T.,.F.)
nPagina := 0

G3G->(dbSetOrder(1)) //G3G_FILIAL + G3G_CLIENT + G3G_LOJA + G3G_TIPO + G3G_ITEM
G3Q->(dbSetOrder(1)) //G3Q_FILIAL + G3Q_NUMID + G3Q_IDITEM + G3Q_NUMSEQ + G3Q_CONORI
G3R->(dbSetOrder(1)) //G3R_FILIAL + G3R_NUMID + G3R_IDITEM + G3R_NUMSEQ + G3R_CONORI
G3S->(dbSetOrder(1)) //G3S_FILIAL + G3S_NUMID + G3S_IDITEM + G3S_NUMSEQ + G3S_CODPAX + G3S_CONORI
G3T->(dbSetOrder(1)) //G3T_FILIAL + G3T_NUMID + G3T_IDITEM + G3T_NUMSEQ + G3T_CODPAX + G3T_ID + G3T_CONORI
G3U->(dbSetOrder(1)) //G3U_FILIAL + G3U_NUMID + G3U_IDITEM + G3U_NUMSEQ + G3U_CODPAX + G3U_ID + G3U_CONORI
G3V->(dbSetOrder(1)) //G3V_FILIAL + G3V_NUMID + G3V_IDITEM + G3V_NUMSEQ + G3V_CODPAX + G3V_ID + G3V_CONORI
G3W->(dbSetOrder(1)) //G3W_FILIAL + G3W_NUMID + G3W_IDITEM + G3W_NUMSEQ + G3W_CODPAX + G3W_ID + G3W_CONORI
G3X->(dbSetOrder(1)) //G3X_FILIAL + G3X_NUMID + G3X_IDITEM + G3X_NUMSEQ + G3X_CODPAX + G3X_ID + G3X_CONORI
G3Y->(dbSetOrder(1)) //G3Y_FILIAL + G3Y_NUMID + G3Y_IDITEM + G3Y_NUMSEQ + G3Y_CODPAX + G3Y_ID + G3Y_CONORI
G3Z->(dbSetOrder(1)) //G3Z_FILIAL + G3Z_NUMID + G3Z_IDITEM + G3Z_NUMSEQ + G3Z_CODPAX + G3Z_ID + G3Z_CONORI
G40->(dbSetOrder(1)) //G40_FILIAL + G40_IDITEM + G40_NUMID + G40_NUMSEQ + G40_CODPAX + G40_ID + G40_CONORI
G41->(dbSetOrder(1)) //G41_FILIAL + G41_IDITEM + G41_NUMID + G41_NUMSEQ + G41_CODPAX + G41_ID + G41_CONORI
G42->(dbSetOrder(1)) //G42_FILIAL + G42_NUMID + G42_IDITEM + G42_NUMSEQ + G42_CODPAX + G42_ID + G42_CONORI
G43->(dbSetOrder(1)) //G43_FILIAL + G43_NUMID + G43_IDITEM + G43_NUMSEQ + G43_CODPAX + G43_ID + G43_CONORI
G4L->(dbSetORder(1)) //G4L_FILIAL + G4L_CODIGO
G4M->(dbSetORder(1)) //G4M_FILIAL + G4M_CODIGO
G84->(dbSetOrder(1)) //G84_FILIAL + G84_PREFIX + G84_NUMFAT + G84_CLIENT + G84_LOJA
G8E->(dbSetOrder(1)) //G8E_FILIAL + G8E_SEQUEN + G8E_PREFIX + G8E_NUMFAT + G8E_TIPO + G8E_SERIE + G8E_NUMNF + G8E_IDDOC
SA1->(dbSetOrder(1)) //A1_FILIAL + A1_COD + A1_LOJA
SA2->(dbSetOrder(1)) //A2_FILIAL + A2_COD + A2_LOJA
SF2->(dbSetOrder(1)) //F2_FILIAL + F2_DOC + F2_SERIE + F2_CLIENTE + F2_LOJA
SU5->(dbSetOrder(1)) //F2_FILIAL + F2_DOC + F2_SERIE + F2_CLIENTE + F2_LOJA
SBM->(dbSetOrder(1)) //BM_FILIAL + BM_GRUPO
G3E->(dbSetOrder(1)) //G3E_FILIAL + G3E_CODIGO
G4E->(dbSetOrder(1)) 

SA1->(dbSeek(xFilial("SA1")+cCliPDF+cLojaPDF))

G84->(dbGoTop())
If G84->(dbSeek(cFilFat+cPrefixo+cFatura))
	
	G4L->(dbSeek(xFilial("G4L")+G84->G84_CMPCLI))
	G4M->(dbSeek(xFilial("G4M")+G4L->G4L_CODIGO))
	
	aEmail  := StrtoKarr(Posicione("G4P",1,xFilial("G4P")+G4L->G4L_CODIGO,"G4P_EMAIL"),";")

	cComple := G84->G84_CMPCLI
	cTpVen  := G84->G84_TPFAT
	
	nLin := TURR13CAB(@oPrint,@nPagina,lReimp)

	If Len(aEmail)> 1
		nSomaMail := 45 * Len(aEmail)
	Endif 
	//------------------------------------------------------------------------------------------
	// INFORMA��ES DO CLIENTE
	//------------------------------------------------------------------------------------------
	oPrint:Box( nLin, 0060, nLin+460+nSomaMail, 2284 )
	
	
	nLin+=25
	
	//Cliente
	oPrint:Say( nLin+=40, 0080, OEMTOANSI(STR0008)+":"		        			 				,oFont11N:oFont)  //"Nome"
	oPrint:Say( nLin	, 0344, OEMTOANSI(SUBSTR(SA1->A1_NOME,1,44))	        			 					,oFont12:oFont  )
	oPrint:Say( nLin	, 1132, OEMTOANSI(STR0009)+":"		        			 				,oFont11N:oFont)  //"C�digo"
	oPrint:Say( nLin	, 1396, OEMTOANSI(AllTrim(cCliPDF)+"-"+AllTrim(cLojaPDF))					,oFont12:oFont  )
	oPrint:Say( nLin+=45, 0080, OEMTOANSI(STR0010)+":"											,oFont11N:oFont)  //"Endere�o"
	oPrint:Say( nLin	, 0344, OEMTOANSI(SUBSTR(SA1->A1_END,1,44))											,oFont12:oFont  )
	oPrint:Say( nLin	, 1132, OEMTOANSI(STR0011)+":"											,oFont11N:oFont)  //"CEP"
	oPrint:Say( nLin	, 1396, OEMTOANSI(TransForm(SA1->A1_CEP, PesqPict("SA1", "A1_CEP")))	,oFont12:oFont  )
	oPrint:Say( nLin+=45, 0080, OEMTOANSI(STR0012)+":"											,oFont11N:oFont)  //"Complemento"
	oPrint:Say( nLin	, 0344, OEMTOANSI(SA1->A1_COMPLEM)										,oFont12:oFont  )
	oPrint:Say( nLin	, 1132, OEMTOANSI(STR0013)+":"											,oFont11N:oFont)  //"Bairro"
	oPrint:Say( nLin	, 1396, OEMTOANSI(SA1->A1_BAIRRO)										,oFont12:oFont  )
	oPrint:Say( nLin+=45, 0080, OEMTOANSI(STR0014)+":"		        		 					,oFont11N:oFont)  //"Cidade-UF"
	oPrint:Say( nLin	, 0344, OEMTOANSI(AllTrim(SA1->A1_MUN)+"-"+SA1->A1_EST)					,oFont12:oFont  )
	oPrint:Say( nLin	, 1132, OEMTOANSI(STR0015)+":"		        			 				,oFont11N:oFont)  //"Fone"
	oPrint:Say( nLin	, 1396, OEMTOANSI("("+ AllTrim(SA1->A1_DDD) +") " + ALLTRIM(TRANSFORM(SA1->A1_TEL,PesqPict('SA1','A1_TEL'))))    	,oFont12:oFont  )
	oPrint:Say( nLin+=45, 0080, OEMTOANSI(STR0016)+":"											,oFont11N:oFont)  //"CPF/CNPJ"
	oPrint:Say( nLin	, 0344, OEMTOANSI(TransForm(AllTrim(SA1->A1_CGC),PesqPict("SA1", "A1_CGC"))),oFont12:oFont  )
	oPrint:Say( nLin	, 1132, OEMTOANSI(STR0082)+":"								,oFont11N:oFont)  //"Inscr. Estadual"
	oPrint:Say( nLin	, 1396, OEMTOANSI(SA1->A1_INSCR) 										,oFont12:oFont  )
	oPrint:Line(nLin+=50, 0060, nLin, 2286,, )
	//Cobran�a
	oPrint:Say( nLin+=50, 0080, OEMTOANSI(STR0010)+":"										 	,oFont11N:oFont)  //"Endere�o"
	oPrint:Say( nLin	, 0344, SubStr(Posicione("G4P",1,xFilial("G4P")+G4L->G4L_CODIGO,"G4P_ENDER"),1,44)	,oFont12:oFont  )
	oPrint:Say( nLin	, 1132, OEMTOANSI(STR0011)+":"		        			 				,oFont11N:oFont)  //"CEP"
	oPrint:Say( nLin	, 1396, TransForm(Posicione("G4P",1,xFilial("G4P")+G4L->G4L_CODIGO,"G4P_CEP"), PesqPict("SA1", "A1_CEP"))		,oFont12:oFont  )
	oPrint:Say( nLin+=45, 0080, OEMTOANSI(STR0012)+":"		        	 						,oFont11N:oFont)  //"Complemento"
	oPrint:Say( nLin	, 0344, Posicione("G4P",1,xFilial("G4P")+G4L->G4L_CODIGO,"G4P_COMPL")	,oFont12:oFont  )
	oPrint:Say( nLin	, 1132, OEMTOANSI(STR0013)+":"		        			 				,oFont11N:oFont)  //"Bairro"
	oPrint:Say( nLin	, 1396, Posicione("G4P",1,xFilial("G4P")+G4L->G4L_CODIGO,"G4P_BAIRRO")	,oFont12:oFont  )
	oPrint:Say( nLin+=45, 0080, OEMTOANSI(STR0014)+":"											,oFont11N:oFont)  //"Cidade-UF"
	oPrint:Say( nLin	, 0344, AllTrim(Posicione('G5S', 1, XFilial('G5S')+Posicione("G4P",1,xFilial("G4P")+G4L->G4L_CODIGO,"G4P_MUNIC"), 'G5S_CIDADE'))+"-"+;
							Posicione("G4P",1,xFilial("G4P")+G4L->G4L_CODIGO,"G4P_UF")			,oFont12:oFont  )
	oPrint:Say( nLin	, 1132, OEMTOANSI(STR0020)+":"											,oFont11N:oFont)  //"Email"

	For nE := 1 to Len(aEmail)
		oPrint:Say( nLin	, 1396, aEmail[nE]	,oFont12:oFont  )
		nLin+=45
	Next 
	
	nLin+=5
	
	BeginSQL Alias cAliasG8E
	
		SELECT
			G8E.G8E_FILREF
			,G8E.G8E_TIPOTI
			,G8E.G8E_PFXTIT
			,G8E.G8E_NUMTIT
			,G8E.G8E_PARCEL
			,G8E.G8E_SEQUEN
			,SE1.E1_VALOR
			,SE1.E1_SALDO
			,SE1.E1_VENCTO
			,SE1.E1_VENCREA			
			,SE1.E1_PORCJUR
			,(SE1.E1_VALJUR+SE1.E1_ACRESC) ENCARGOS
			,SE1.*
		FROM
			%Table:G8E% G8E
		INNER JOIN
			%Table:SE1% SE1
		ON
			SE1.E1_FILIAL = G8E.G8E_FILIAL
			AND SE1.E1_PREFIXO = G8E.G8E_PFXTIT
			AND SE1.E1_NUM = G8E.G8E_NUMTIT
			AND SE1.E1_PARCELA = G8E.G8E_PARCEL
			AND SE1.E1_TIPO = G8E.G8E_TIPOTI
			AND SE1.E1_SALDO > 0
			AND SE1.%NotDel% 			
		WHERE	
				G8E.G8E_FILIAL = %xFilial:G8E%
				AND G8E.G8E_PREFIX = %Exp:cPrefixo%
			AND	G8E.G8E_NUMFAT = %Exp:cFatura%
				AND G8E.G8E_TIPO <> '2'
			AND	G8E.%NotDel%
		ORDER BY 
			G8E.G8E_SEQUEN DESC
	
	EndSQL
	
	(cAliasG8E)->(dbGoTop())
	
	While (cAliasG8E)->(!Eof())
	
		aAdd(aAux, 	(cAliasG8E)->G8E_FILREF)
		aAdd(aAux,	(cAliasG8E)->G8E_PFXTIT )
		aAdd(aAux,	(cAliasG8E)->G8E_NUMTIT )
		aAdd(aAux,	(cAliasG8E)->G8E_PARCEL )
		aAdd(aAux,	(cAliasG8E)->G8E_TIPOTI )
		aAdd(aAux,	Iif( Alltrim((cAliasG8E)->G8E_TIPOTI) == "NCC",(cAliasG8E)->E1_SALDO*(-1),(cAliasG8E)->E1_SALDO)	)
		aAdd(aAux,	IIF( !lFuncDTVen, SToD((cAliasG8E)->E1_VENCTO), U_TurDtVen(cAliasG8E) ) ) 
		aAdd(aAux,	(cAliasG8E)->E1_PORCJUR )
		aAdd(aAux,	(cAliasG8E)->ENCARGOS  )
		
		aAdd(aTitFina,aClone(aAux))
		aAux := {}
			
		(cAliasG8E)->(DbSkip())
		
	EndDo
	
	(cAliasG8E)->(dbCloseArea())
	
	//------------------------------------------------------------------------------------------
	// INFORMA��ES DO FINANCEIRO
	//------------------------------------------------------------------------------------------
	If lExistFunc
		
		nLinIni := nLin
		
		//PONTO DE ENTRADA PARA ALTERAR INFORMA��ES DO BLOCO DO FINANCEIRO	
		nLin := U_FTBLCFIN(oPrint, aTitFina, nLin)
		
		If ValType(nLin) != "N"
			nLin := nLinIni 
		EndIf
			
	ELSE
		If !Empty(aTitFina)
			
			If ( aScan(aTitFina,{|x| !(Empty(x[3]))}) > 0)
				aSort(aTitFina,,,{|x,y| x[3] < y[3]})
			Endif
			
			nLinIni := nLin
			
				oPrint:Box( nLin, 0060, nLin+75, 2284,, ) 
				nLin+=25
		
				oPrint:Say( nLin+=35, 0080, OEMTOANSI(STR0021)	,oFont11N:oFont)  //"Prefixo"
				oPrint:Say( nLin	, 0400, OEMTOANSI(STR0022)	,oFont11N:oFont)  //"Num. Titulo"
				oPrint:Say( nLin	, 0720, OEMTOANSI(STR0023)	,oFont11N:oFont)  //"Parcela"
				oPrint:Say( nLin	, 0900, OEMTOANSI(STR0024)	,oFont11N:oFont)  //"Valor"
				oPrint:Say( nLin	, 1200, OEMTOANSI(STR0025)	,oFont11N:oFont)  //"Vencimento"
				oPrint:Say( nLin	, 1620, OEMTOANSI(STR0026)	,oFont11N:oFont)  //"Juros(%)"
				oPrint:Say( nLin	, 1950, OEMTOANSI(STR0027)	,oFont11N:oFont)  //"Tx.Perman."
			
				nLin+=15
		
			For nI := 1 to Len(aTitFina)
		
					oPrint:Line(nLin, 0060, nLin+45, 0060,, )
					oPrint:Line(nLin, 2286, nLin+45, 2286,, )
					nLin+=45
			
					oPrint:Say( nLin 	, 0080, OEMTOANSI(aTitFina[nI,2]) 									,oFont12:oFont  )
					oPrint:Say( nLin	, 0400, OEMTOANSI(aTitFina[nI,3]) 									,oFont12:oFont  )
					oPrint:Say( nLin	, 0720, OEMTOANSI(aTitFina[nI,4]) 									,oFont12:oFont  )
					oPrint:Say( nLin	, 0850, Transform(aTitFina[nI,6],"@E 9,999,999.99")	,oFont12:oFont,,,,1  )
					oPrint:Say( nLin	, 1200, OEMTOANSI(Dtoc(aTitFina[nI,7]))							,oFont12:oFont  )
					oPrint:Say( nLin	, 1550, Transform(aTitFina[nI,8],"@E 9,999,999.99") 			,oFont12:oFont,,,,1  )
					oPrint:Say( nLin	, 1900, Transform(aTitFina[nI,9],"@E 9,999,999.99") 			,oFont12:oFont,,,,1  )
			
					If nI == Len(aTitFina)		
						oPrint:Line(nLin, 0060, nLin+25, 0060,, )
						oPrint:Line(nLin, 2286, nLin+25, 2286,, )
						nLin+=25		
						oPrint:Line(nLin, 0060, nLin, 2286,, )
						nLin+=25
					EndIf
		
				
				Next nI
		
		EndIf
	EndIf
	
	//------------------------------------------------------------------------------------------
	// NOTA FISCAL
	//------------------------------------------------------------------------------------------

	If cTpVen == '2' //Apura��es

		cAliasNFS	:= GetNextAlias()
		
		BeginSQL Alias cAliasNFS
		
			SELECT
				G8E.G8E_FILREF
  				,G8E.G8E_NUMNF
  				,G8E.G8E_SERIE
				,G8E.G8E_SEQUEN
				,SF2.F2_EMISSAO
				,SF2.F2_VALMERC
				,SF2.F2_VALIRRF
				,SF2.F2_VALINSS
				,SF2.F2_VALISS
				,SF2.F2_VALICM
				,SF2.F2_VALIPI
				,SF2.F2_VALCSLL
				,SF2.F2_VALPIS
				,SF2.F2_VALCOFI 
				,SF2.F2_VALFAT
			FROM
				%Table:G8E% G8E
			INNER JOIN
				%Table:SF2% SF2
			ON
				SF2.F2_FILIAL = G8E.G8E_FILREF AND
				SF2.F2_SERIE = G8E.G8E_SERIE AND
				SF2.F2_DOC = G8E.G8E_NUMNF AND
				SF2.F2_CLIENTE = G8E.G8E_CLIENT AND
				SF2.F2_LOJA = G8E.G8E_LOJA AND
				SF2.%NotDel% 			
			WHERE	
				G8E.G8E_FILIAL = %xFilial:G8E% AND
				G8E.G8E_PREFIX = %Exp:cPrefixo% AND
				G8E.G8E_NUMFAT = %Exp:cFatura% AND
				G8E.G8E_TIPO = '2' AND
				G8E.%NotDel%
			ORDER BY 
				G8E.G8E_SEQUEN DESC
		
		EndSQL
		
		(cAliasNFS)->(dbGoTop())
		
		While (cAliasNFS)->(!Eof())
		
			aAdd(aTitFisc,{	(cAliasNFS)->G8E_SERIE  ,; 
								(cAliasNFS)->G8E_NUMNF  ,; 
								SToD((cAliasNFS)->F2_EMISSAO) ,; 
								(cAliasNFS)->F2_VALMERC ,;
								(cAliasNFS)->F2_VALIRRF +;
								(cAliasNFS)->F2_VALINSS +;
								(cAliasNFS)->F2_VALISS  +;
								(cAliasNFS)->F2_VALICM  +;
								(cAliasNFS)->F2_VALIPI  +;
								(cAliasNFS)->F2_VALCSLL +;
								(cAliasNFS)->F2_VALPIS  +;
								(cAliasNFS)->F2_VALCOFI ,; 
								(cAliasNFS)->F2_VALFAT  })
				
			(cAliasNFS)->(DbSkip())
			
		EndDo
		
		(cAliasNFS)->(dbCloseArea())
		
		If !Empty(aTitFisc)
					
			oPrint:Box( nLin, 0060, nLin+75, 2284,, ) 
			nLin+=25
			
			oPrint:Say( nLin+=35, 0080, OEMTOANSI(STR0028)	,oFont11N:oFont ) //"S�rie"
			oPrint:Say( nLin, 0400, OEMTOANSI(STR0094)		,oFont11N:oFont ) //"RPS"
			oPrint:Say( nLin, 0850, OEMTOANSI(STR0029)	,oFont11N:oFont ) //"Emiss�o"
			oPrint:Say( nLin, 1200, OEMTOANSI(STR0030)	,oFont11N:oFont ) //"Valor Servi�o"
			oPrint:Say( nLin, 1620, OEMTOANSI(STR0031)	,oFont11N:oFont ) //"Impostos"
			oPrint:Say( nLin, 1950, OEMTOANSI(STR0032)	,oFont11N:oFont ) //"Total da NF"
			nLin+=15
			
			For nx := 1 To Len(aTitFisc)
							
				oPrint:Line(nLin, 0060, nLin+45, 0060,, )
				oPrint:Line(nLin, 2286, nLin+45, 2286,, )
				nLin+=45
				
				oPrint:Say( nLin, 0080, OEMTOANSI(aTitFisc[nx,1]) 				,oFont12:oFont  ) 
				oPrint:Say( nLin, 0400, OEMTOANSI(aTitFisc[nx,2]) 				,oFont12:oFont  ) 
				oPrint:Say( nLin, 0850, OEMTOANSI(DToC(aTitFisc[nx,3])) 		,oFont12:oFont  ) 
				oPrint:Say( nLin, 1200, Transform(aTitFisc[nx,4],"@E 9,999,999.99") 	,oFont12:oFont,,,,1  ) 
				oPrint:Say( nLin, 1550, Transform(aTitFisc[nx,5],"@E 9,999,999.99") 	,oFont12:oFont,,,,1  ) 
				oPrint:Say( nLin, 1900, Transform(aTitFisc[nx,6],"@E 9,999,999.99") 	,oFont12:oFont,,,,1  ) 
				
			
				If nX == Len(aTitFisc)		
					oPrint:Line(nLin, 0060, nLin+25, 0060,, )
					oPrint:Line(nLin, 2286, nLin+25, 2286,, )
					nLin+=25		
					oPrint:Line(nLin, 0060, nLin, 2286,, )
					nLin+=25
				EndIf
			
			Next
		EndIf
	EndIf

	//------------------------------------------------------------------------------------------
	// ENTIDADE TOTALIZADORA
	//------------------------------------------------------------------------------------------
	nTotEad := 0

	//Seleciona o Totalizador cadastrado para o cliente da fatura
	BeginSQL Alias cAliasEad

		SELECT	G67.G67_BASE
				, G67.G67_CODEAD
		FROM	%Table:G67% G67
		WHERE	G67.%NotDel%
		AND		G67_FILIAL = %xFilial:G67%
		AND		G67_CODIGO = %Exp:cComple%
		AND		G67_TIPO   = %Exp:cTpVen%
		AND		G67_TPAGRU = '2'

	EndSQL

	(cAliasEad)->(dbGoTop())

	If (cAliasEad)->(!Eof()) .And. !Empty((cAliasEad)->G67_BASE)
		Do Case
			Case (cAliasEad)->G67_BASE == "1" //1=Entidades Adicionais

				cDscEntTot := AllTrim((Posicione("G3E",1,xFilial("G3E")+(cAliasEad)->G67_CODEAD,"G3E_DESCR")))+": "

			Case (cAliasEad)->G67_BASE == "2" //2=Solicitantes

				cDscEntTot := STR0085 //"Solicitante: "
				cCampo := "SOLIC"

			Case (cAliasEad)->G67_BASE == "3" //3=Grupo de Produto

				cDscEntTot := STR0086 //"Grupo de Produto: "
				cCampo := "GRUPO"

			Case (cAliasEad)->G67_BASE == "4" //4=Filial de Venda

				cDscEntTot := STR0087 //"Filial de Venda: "
				cCampo := "FILREF"

		EndCase
	Else
		lTotaliza := .F.
	EndIf

	If lTotaliza .And.	(cAliasEad)->G67_BASE $ "2|3|4"
		cOrder   += " ORDER BY " + cCampo
	EndIf

	cOrder := '%'+cOrder+'%'

	If G84->G84_TPFAT == '4'
		BeginSql Alias cAliasTot
			SELECT FILREF+REGVEN+ITVEND ITEM
			 		, FILREF
			 		, REGVEN
			 		, ITVEND
			 		, GRUPO
			 		, SOLIC
			 		, MOEDA
			 		, CAMBIO
			 		, TPENT
			 		, ITENT
			 		, SUM(TARIFA) TARIFA
			 		, SUM(TAXA) TAXA
			 		, SUM(TAXAADM) TAXAADM
			 		, SUM(VALOR) TOTAL
			 		, SUM(MULTA) MULTA
			 		, SUM(REPASSE) REPASSE
			FROM (
				SELECT G85.G85_IDIF IDIF
						, G85.G85_FILREF FILREF
						, G85.G85_REGVEN REGVEN
						, G85.G85_ITVEND ITVEND
						, G85.G85_TPENT  TPENT
						, G85.G85_ITENT  ITENT
						, G4C.G4C_MOEDA MOEDA
						, (CASE WHEN (G4C_OPERAC = '2' AND G4C_PAGREC = '2') OR (G4C_OPERAC <> '2' AND G4C_PAGREC = '1') THEN G4C.G4C_TARIFA ELSE G4C.G4C_TARIFA*-1 END) TARIFA
						, (CASE WHEN (G4C_OPERAC = '2' AND G4C_PAGREC = '2') OR (G4C_OPERAC <> '2' AND G4C_PAGREC = '1') THEN G4C.G4C_TXORIG + G4C.G4C_EXTRA ELSE (G4C.G4C_TXORIG + G4C.G4C_EXTRA)*-1  END) TAXA
          				, G4C.G4C_TXCAMB CAMBIO
          				, G4C.G4C_TAXADU TAXAADM
						, (CASE WHEN (G4C_OPERAC = '2' AND G4C_PAGREC = '2') OR (G4C_OPERAC <> '2' AND G4C_PAGREC = '1') THEN G4C.G4C_VALOR ELSE G4C.G4C_VALOR*-1 END) VALOR
						, G85.G85_GRPPRD GRUPO
						, G85.G85_SOLIC SOLIC
						, (CASE WHEN (G4C_OPERAC = '2' AND G4C_PAGREC = '2') OR (G4C_OPERAC <> '2' AND G4C_PAGREC = '1') THEN (G4C.G4C_TXRORI + G4C.G4C_EXTRA)*-1 ELSE (G4C.G4C_TXRORI + G4C.G4C_EXTRA) END)MULTA
						, 0 REPASSE
				 FROM %Table:G85% G85
				 INNER JOIN %Table:G4C% G4C
				 ON G4C.G4C_FILREF = G85.G85_FILREF
				 AND G4C.G4C_IDIF = G85.G85_IDIF
				 AND G4C.G4C_NUMID = G85.G85_REGVEN
				 AND G4C.G4C_IDITEM = G85.G85_ITVEND
				 AND G4C.G4C_NUMSEQ = G85.G85_SEQIV
				 AND G4C.G4C_CONINU = %Exp:cConinu%
				 AND G4C.%NotDel%
				 WHERE G85.G85_FILIAL = %xFilial:G85%
				 AND G85.G85_PREFIX = %Exp:cPrefixo%
				 AND G85.G85_NUMFAT = %Exp:cFatura%
				 AND G85.G85_ITPRIN = ' '
				 AND G85.G85_CLASS <> 'V01'
				 AND G85.%NotDel%

				 UNION

				 SELECT G85.G85_IDIF IDIF
				 		 , G85.G85_FILREF FILREF
						 , G85.G85_REGVEN REGVEN
						 , G85.G85_ITVEND ITVEND
 						 , G85.G85_TPENT  TPENT
						 , G85.G85_ITENT  ITENT
						 , G4C.G4C_MOEDA MOEDA
						 , (CASE WHEN G4C.G4C_PAGREC = '2' THEN G4C.G4C_VALOR ELSE G4C.G4C_VALOR*-1 END)  TARIFA
						 , 0 TAXA
						 , G4C.G4C_TXCAMB CAMBIO
						 , 0 TAXAADM
						 , (CASE WHEN G4C.G4C_PAGREC = '2' THEN G4C.G4C_VALOR ELSE G4C.G4C_VALOR*-1 END) VALOR
						 , G85.G85_GRPPRD GRUPO
						 , G85.G85_SOLIC SOLIC
						 , 0 MULTA
						 , 0 REPASSE
				 FROM %Table:G85% G85
				 INNER JOIN %Table:G4C% G4C
				 ON G4C.G4C_FILREF = G85.G85_FILREF
				 AND G4C.G4C_IDIF = G85.G85_IDIF
				 AND G4C.G4C_NUMID = G85.G85_REGVEN
				 AND G4C.G4C_IDITEM = G85.G85_ITVEND
				 AND G4C.G4C_NUMSEQ = G85.G85_SEQIV
				 AND G4C.G4C_CONINU = %Exp:cConinu%
				 AND G4C.%NotDel%
				 WHERE G85.G85_FILIAL = %xFilial:G85%
				 AND G85.G85_PREFIX = %Exp:cPrefixo%
				 AND G85.G85_NUMFAT = %Exp:cFatura%
				 AND G85.G85_ITPRIN <> ' '
				 AND G85.G85_CLASS IN ('C08','C09')
				 AND G85.%NotDel%

				 UNION

				 SELECT G85.G85_IDIF IDIF
				 		 , G85.G85_FILREF FILREF
						 , G85.G85_REGVEN REGVEN
						 , G85.G85_ITVEND ITVEND
						 , G85.G85_TPENT  TPENT
						 , G85.G85_ITENT  ITENT
						 , G4C.G4C_MOEDA MOEDA
						 , (CASE WHEN G4C.G4C_PAGREC = '2' THEN G4C.G4C_VALOR ELSE G4C.G4C_VALOR*-1 END) TARIFA
						 , 0 TAXA
						 , G4C.G4C_TXCAMB CAMBIO
						 , 0 TAXAADM
						 , (CASE WHEN G4C.G4C_PAGREC = '2' THEN G4C.G4C_VALOR ELSE G4C.G4C_VALOR*-1 END) VALOR
						 , G85.G85_GRPPRD GRUPO
						 , G85.G85_SOLIC SOLIC
						 , 0 MULTA
						 , 0 REPASSE
				 FROM %Table:G85% G85
				 INNER JOIN %Table:G4C% G4C
				 ON G4C.G4C_FILREF = G85.G85_FILREF
				 AND G4C.G4C_IDIF = G85.G85_IDIF
				 AND G4C.G4C_NUMID = G85.G85_REGVEN
				 AND G4C.G4C_IDITEM = G85.G85_ITVEND
				 AND G4C.G4C_NUMSEQ = G85.G85_SEQIV
				 AND G4C.G4C_CONINU = %Exp:cConinu%
				 AND G4C.%NotDel%
				 WHERE G85.G85_FILIAL = %xFilial:G85%
				 AND G85.G85_PREFIX = %Exp:cPrefixo%
				 AND G85.G85_NUMFAT = %Exp:cFatura%
				 AND G85.G85_ITPRIN = ' '
				 AND G85.G85_CLASS = 'V01'
				 AND G85.%NotDel%

				 UNION

				 SELECT G85.G85_IDIF IDIF
				 		 , G85.G85_FILREF FILREF
						 , G85.G85_REGVEN REGVEN
						 , G85.G85_ITVEND ITVEND
						 , G85.G85_TPENT  TPENT
						 , G85.G85_ITENT  ITENT
						 , G4C.G4C_MOEDA MOEDA
						 , 0 TARIFA
						 , 0 TAXA
						 , G4C.G4C_TXCAMB CAMBIO
						 , (CASE WHEN G4C.G4C_PAGREC = '2' THEN G4C.G4C_VALOR ELSE G4C.G4C_VALOR*-1 END) TAXAADM
						 , (CASE WHEN G4C.G4C_PAGREC = '2' THEN G4C.G4C_VALOR ELSE G4C.G4C_VALOR*-1 END) VALOR
						 , G85.G85_GRPPRD GRUPO
						 , G85.G85_SOLIC SOLIC
						 , 0 MULTA
						 , 0 REPASSE
				 FROM %Table:G85% G85
				 INNER JOIN %Table:G4C% G4C
				 ON G4C.G4C_FILREF = G85.G85_FILREF
				 AND G4C.G4C_IDIF = G85.G85_IDIF
				 AND G4C.G4C_NUMID = G85.G85_REGVEN
				 AND G4C.G4C_IDITEM = G85.G85_ITVEND
				 AND G4C.G4C_NUMSEQ = G85.G85_SEQIV
				 AND G4C.G4C_CONINU = %Exp:cConinu%
				 AND G4C.%NotDel%
				 WHERE G85.G85_FILIAL = %xFilial:G85%
				 AND G85.G85_PREFIX = %Exp:cPrefixo%
				 AND G85.G85_NUMFAT = %Exp:cFatura%
				 AND G85.G85_ITPRIN <> ' '
				 AND G85.G85_CLASS NOT IN('C09','C08','V01')
				 AND G85.%NotDel%
				 
			 	UNION

				 SELECT G85.G85_IDIF IDIF
				 		 , G85.G85_FILREF FILREF
						 , G85.G85_REGVEN REGVEN
						 , G85.G85_ITVEND ITVEND
						 , G85.G85_TPENT  TPENT
						 , G85.G85_ITENT  ITENT
						 , G4C.G4C_MOEDA MOEDA
						 , 0 TARIFA
						 , 0 TAXA
						 , G4C.G4C_TXCAMB CAMBIO
						 , 0 TAXAADM
						 , (CASE WHEN G4C.G4C_PAGREC = '2' THEN G4C.G4C_VALOR *-1 ELSE G4C.G4C_VALOR END) VALOR
						 , G85.G85_GRPPRD GRUPO
						 , G85.G85_SOLIC SOLIC
						 , 0 MULTA
						 , (CASE WHEN G4C.G4C_PAGREC = '2' THEN G4C.G4C_VALOR ELSE G4C.G4C_VALOR*-1 END) REPASSE
				 FROM %Table:G85% G85
				 INNER JOIN %Table:G4C% G4C
				 ON G4C.G4C_FILREF = G85.G85_FILREF
				 AND G4C.G4C_IDIF = G85.G85_IDIF
				 AND G4C.G4C_NUMID = G85.G85_REGVEN
				 AND G4C.G4C_IDITEM = G85.G85_ITVEND
				 AND G4C.G4C_NUMSEQ = G85.G85_SEQIV
				 AND G4C.G4C_CONINU = %Exp:cConinu%
				 AND G4C.%NotDel%
				 WHERE G85.G85_FILIAL = %xFilial:G85%
				 AND G85.G85_PREFIX = %Exp:cPrefixo%
				 AND G85.G85_NUMFAT = %Exp:cFatura%
				 AND G85.G85_CLASS = 'C07' 
				 AND G85.%NotDel%				 
				 ) TMP
			GROUP BY FILREF, REGVEN, ITVEND, GRUPO, SOLIC, MOEDA, CAMBIO, TPENT, ITENT

			%Exp:cOrder%

		EndSql


	ElseIf G84->G84_TPFAT <> '2'

		BeginSql Alias cAliasTot
			SELECT FILREF+REGVEN+ITVEND ITEM
			 		, FILREF
			 		, REGVEN
			 		, ITVEND
			 		, GRUPO
			 		, SOLIC
			 		, MOEDA
			 		, CAMBIO
			 		, TPENT
			 		, ITENT
			 		, SUM(TARIFA) TARIFA
			 		, SUM(TAXA) TAXA
			 		, SUM(TAXAADM) TAXAADM
			 		, SUM(VALOR) TOTAL
			FROM (
				SELECT G85.G85_IDIF IDIF
						, G85.G85_FILREF FILREF
						, G85.G85_REGVEN REGVEN
						, G85.G85_ITVEND ITVEND
						, G85.G85_TPENT  TPENT
					    , G85.G85_ITENT  ITENT
						, G4C.G4C_MOEDA MOEDA
						, (CASE WHEN (G4C_OPERAC = '2' AND G4C_PAGREC = '1') OR (G4C_OPERAC <> '2' AND G4C_PAGREC = '2') THEN G4C.G4C_TARIFA ELSE G4C.G4C_TARIFA*-1 END) TARIFA
						, (CASE WHEN (G4C_OPERAC = '2' AND G4C_PAGREC = '1') OR (G4C_OPERAC <> '2' AND G4C_PAGREC = '2') THEN G4C.G4C_TAXA + G4C.G4C_EXTRA ELSE (G4C.G4C_TAXA + G4C.G4C_EXTRA)*-1 END )TAXA
						, G4C.G4C_TXCAMB CAMBIO
						, G4C.G4C_TAXADU TAXAADM
						, (CASE WHEN (G4C_OPERAC = '2' AND G4C_PAGREC = '1') OR (G4C_OPERAC <> '2' AND G4C_PAGREC = '2') THEN G4C.G4C_VALOR ELSE G4C.G4C_VALOR*-1 END) VALOR
						, G85.G85_GRPPRD GRUPO
						, G85.G85_SOLIC SOLIC
				 FROM %Table:G85% G85
				 INNER JOIN %Table:G4C% G4C
				 ON G4C.G4C_FILREF = G85.G85_FILREF
				 AND G4C.G4C_IDIF = G85.G85_IDIF
				 AND G4C.G4C_NUMID = G85.G85_REGVEN
				 AND G4C.G4C_IDITEM = G85.G85_ITVEND
				 AND G4C.G4C_NUMSEQ = G85.G85_SEQIV
				 AND G4C.G4C_CONINU = %Exp:cConinu%
				 AND G4C.%NotDel%
				 WHERE G85.G85_FILIAL = %xFilial:G85%
				 AND G85.G85_PREFIX = %Exp:cPrefixo%
				 AND G85.G85_NUMFAT = %Exp:cFatura%
				 AND G85.G85_ITPRIN = ' '
				 AND G85.G85_CLASS <> 'V01'
				 AND G85.%NotDel%

				 UNION

				 SELECT G85.G85_IDIF IDIF
				 		 , G85.G85_FILREF FILREF
						 , G85.G85_REGVEN REGVEN
						 , G85.G85_ITVEND ITVEND
						 , G85.G85_TPENT  TPENT
						 , G85.G85_ITENT  ITENT
						 , G4C.G4C_MOEDA MOEDA
						 , (CASE WHEN G4C.G4C_PAGREC = '2' THEN G4C.G4C_VALOR ELSE G4C.G4C_VALOR*-1 END) TARIFA
						 , 0 TAXA
						 , G4C.G4C_TXCAMB CAMBIO
						 , 0 TAXAADM
						 , (CASE WHEN G4C.G4C_PAGREC = '2' THEN G4C.G4C_VALOR ELSE G4C.G4C_VALOR*-1 END) VALOR
						 , G85.G85_GRPPRD GRUPO
						 , G85.G85_SOLIC SOLIC
				 FROM %Table:G85% G85
				 INNER JOIN %Table:G4C% G4C
				 ON G4C.G4C_FILREF = G85.G85_FILREF
				 AND G4C.G4C_IDIF = G85.G85_IDIF
				 AND G4C.G4C_NUMID = G85.G85_REGVEN
				 AND G4C.G4C_IDITEM = G85.G85_ITVEND
				 AND G4C.G4C_NUMSEQ = G85.G85_SEQIV
				 AND G4C.G4C_CONINU = %Exp:cConinu%
				 AND G4C.%NotDel%
				 WHERE G85.G85_FILIAL = %xFilial:G85%
				 AND G85.G85_PREFIX = %Exp:cPrefixo%
				 AND G85.G85_NUMFAT = %Exp:cFatura%
				 AND G85.G85_ITPRIN <> ' '
				 AND G85.G85_CLASS IN ('C08','C09')
				 AND G85.%NotDel%

				 UNION

				 SELECT G85.G85_IDIF IDIF
				 		 , G85.G85_FILREF FILREF
						 , G85.G85_REGVEN REGVEN
						 , G85.G85_ITVEND ITVEND
						 , G85.G85_TPENT  TPENT
						 , G85.G85_ITENT  ITENT
						 , G4C.G4C_MOEDA MOEDA
						 , (CASE WHEN G4C.G4C_PAGREC = '2' THEN G4C.G4C_VALOR ELSE G4C.G4C_VALOR*-1 END) TARIFA
						 , 0 TAXA
						 , G4C.G4C_TXCAMB CAMBIO
						 , 0 TAXAADM
						 , (CASE WHEN G4C.G4C_PAGREC = '2' THEN G4C.G4C_VALOR ELSE G4C.G4C_VALOR*-1 END) VALOR
						 , G85.G85_GRPPRD GRUPO
						 , G85.G85_SOLIC SOLIC
				 FROM %Table:G85% G85
				 INNER JOIN %Table:G4C% G4C
				 ON G4C.G4C_FILREF = G85.G85_FILREF
				 AND G4C.G4C_IDIF = G85.G85_IDIF
				 AND G4C.G4C_NUMID = G85.G85_REGVEN
				 AND G4C.G4C_IDITEM = G85.G85_ITVEND
				 AND G4C.G4C_NUMSEQ = G85.G85_SEQIV
				 AND G4C.G4C_CONINU = %Exp:cConinu%
				 AND G4C.%NotDel%
				 WHERE G85.G85_FILIAL = %xFilial:G85%
				 AND G85.G85_PREFIX = %Exp:cPrefixo%
				 AND G85.G85_NUMFAT = %Exp:cFatura%
				 AND G85.G85_ITPRIN = ' '
				 AND G85.G85_CLASS = 'V01'
				 AND G85.%NotDel%

				 UNION

				 SELECT G85.G85_IDIF IDIF
				 		 , G85.G85_FILREF FILREF
						 , G85.G85_REGVEN REGVEN
						 , G85.G85_ITVEND ITVEND
						 , G85.G85_TPENT  TPENT
						 , G85.G85_ITENT  ITENT
						 , G4C.G4C_MOEDA MOEDA
						 , 0 TARIFA
						 , 0 TAXA
						 , G4C.G4C_TXCAMB CAMBIO
						 , (CASE WHEN G4C.G4C_PAGREC = '2' THEN G4C.G4C_VALOR ELSE G4C.G4C_VALOR*-1 END) TAXAADM
						 , (CASE WHEN G4C.G4C_PAGREC = '2' THEN G4C.G4C_VALOR ELSE G4C.G4C_VALOR*-1 END) VALOR
						 , G85.G85_GRPPRD GRUPO
						 , G85.G85_SOLIC SOLIC
				 FROM %Table:G85% G85
				 INNER JOIN %Table:G4C% G4C
				 ON G4C.G4C_FILREF = G85.G85_FILREF
				 AND G4C.G4C_IDIF = G85.G85_IDIF
				 AND G4C.G4C_NUMID = G85.G85_REGVEN
				 AND G4C.G4C_IDITEM = G85.G85_ITVEND
				 AND G4C.G4C_NUMSEQ = G85.G85_SEQIV
				 AND G4C.G4C_CONINU = %Exp:cConinu%
				 AND G4C.%NotDel%
				 WHERE G85.G85_FILIAL = %xFilial:G85%
				 AND G85.G85_PREFIX = %Exp:cPrefixo%
				 AND G85.G85_NUMFAT = %Exp:cFatura%
				 AND G85.G85_ITPRIN <> ' '
				 AND G85.G85_CLASS NOT IN('C09','C08','V01')
				 AND G85.%NotDel%
				 ) TMP
			GROUP BY FILREF, REGVEN, ITVEND, GRUPO, SOLIC, MOEDA, CAMBIO, TPENT, ITENT

			%Exp:cOrder%

		EndSql


	Else

		BeginSql Alias cAliasTot
			SELECT G6L_TPAPUR 
			FROM %Table:G6L% G6L
			JOIN %Table:G85% G85 ON %Exp:cExpG6LG85%
									G85.G85_CODAPU = G6L.G6L_CODAPU
			WHERE G85.G85_FILIAL = %xFilial:G85%  AND 
			      G85.G85_PREFIX = %Exp:cPrefixo% AND
                  G85.G85_NUMFAT = %Exp:cFatura%  AND
                  G85.%NotDel% AND 
                  G6L.%NotDel%
		EndSql
		
		If (cAliasTot)->(!Eof())
			cTpApur := (cAliasTot)->G6L_TPAPUR
			(cAliasTot)->(dbCloseArea())
	
			If cTpApur == '1' 
				BeginSql Alias cAliasTot
					SELECT G85.G85_FILREF+G85.G85_CODAPU ITEM,
						   G85.G85_FILREF FILREF,
						   G85.G85_CODAPU APURACAO,
						   G85.G85_TPENT TPENT,
						   G85.G85_ITENT ITENT,
						   SUM((CASE WHEN G85.G85_PAGREC = '2' 
								  THEN G85.G85_VALOR
								  ELSE G85.G85_VALOR * -1
								END)) TOTAL
					FROM %Table:G85% G85
					INNER JOIN %Table:G81% G81 ON G81.G81_FILREF = G85.G85_FILREF AND 		
												  G81.G81_IDIFA  = G85.G85_IDIFA  AND 		
												  G81.%NotDel%
					WHERE G85.G85_FILIAL = %xFilial:G85%  AND 
						  G85.G85_PREFIX = %Exp:cPrefixo% AND
						  G85.G85_NUMFAT = %Exp:cFatura%  AND
						  G85.%NotDel%
					GROUP BY G85.G85_FILREF+G85.G85_CODAPU,
							 G85.G85_FILREF, 
							 G85.G85_CODAPU, 
							 G85.G85_TPENT, 
							 G85.G85_ITENT
				EndSql
			Else
				BeginSql Alias cAliasTot
					SELECT		G85.G85_FILREF+G85.G85_CODAPU+G85.G85_IDIFA ITEM,
								G85.G85_FILREF FILREF,
								G85.G85_CODAPU APURACAO,
								G85.G85_IDIFA IDIFA,
								G85.G85_CODPRD PRODUTO,
								(CASE
									WHEN G85.G85_PAGREC = '2' 
									THEN G85.G85_VALOR
									ELSE G85.G85_VALOR*-1
								END) TOTAL,
								G85.G85_TPENT TPENT,
								G85.G85_ITENT ITENT,
								G85.G85_ITEM ITEMF
					FROM 		%Table:G85% G85
					INNER JOIN 	%Table:G81% G81
					ON 			G81.G81_FILREF 	= G85.G85_FILREF
					AND 		G81.G81_IDIFA 	= G85.G85_IDIFA
					AND 		G81.%NotDel%
					WHERE 		G85.%NotDel%
					AND 		G85.G85_FILIAL	= %xFilial:G85%
					AND 		G85.G85_PREFIX 	= %Exp:cPrefixo%
					AND 		G85.G85_NUMFAT 	= %Exp:cFatura%
				EndSql
			EndIf
		EndIf
	EndIf
	
	(cAliasTot)->(dbGoTop())

	//Guarda em um array os itens da fatura
	While (cAliasTot)->(!Eof())
	
		If ( (cAliasTot)->(FieldPos("REPASSE")) > 0 )
			nTotal := TR013CalcSub((cAliasTot)->TOTAL,(cAliasTot)->REPASSE)
		Else
			nTotal := (cAliasTot)->TOTAL
		EndIf	
		
		If lTotaliza
			aAdd(aTot,{	If(	(cAliasEad)->G67_BASE <> "1",	(cAliasTot)->&(cCampo),;
							If(G84->G84_TPFAT <> '2',	DescEnt((cAliasTot)->ITEM,'01',(cAliasEad)->G67_CODEAD),;
							If((cAliasTot)->TPENT == (cAliasEad)->G67_CODEAD,(cAliasTot)->ITENT,""))),;
							(cAliasTot)->ITEM,;
						If(G84->G84_TPFAT <> '2',(cAliasTot)->CAMBIO,0),;
						If(G84->G84_TPFAT <> '2',(cAliasTot)->TARIFA,0),; //Tarifa Cliente
						If(G84->G84_TPFAT <> '2',(cAliasTot)->TAXA,0),;
						If(G84->G84_TPFAT <> '2',(cAliasTot)->TAXAADM,0),;
						nTotal,;
						If(G84->G84_TPFAT == '2',(cAliasTot)->TPENT,""),;
						If(G84->G84_TPFAT == '2',(cAliasTot)->ITENT,""),;
						If(G84->G84_TPFAT == '4',(cAliasTot)->MULTA,0),;
						If(G84->G84_TPFAT == '4',(cAliasTot)->REPASSE,0)})
		Else
			aAdd(aTot,{	"",;
						(cAliasTot)->ITEM,;
						If(G84->G84_TPFAT <> '2',(cAliasTot)->CAMBIO,0),;
						If(G84->G84_TPFAT <> '2',(cAliasTot)->TARIFA,0),; //Tarifa Cliente
						If(G84->G84_TPFAT <> '2',(cAliasTot)->TAXA,0),;
						If(G84->G84_TPFAT <> '2',(cAliasTot)->TAXAADM,0),;
						nTotal,;
						If(G84->G84_TPFAT == '2',(cAliasTot)->TPENT,""),;
						If(G84->G84_TPFAT == '2',(cAliasTot)->ITENT,""),;
						If(G84->G84_TPFAT == '4',(cAliasTot)->MULTA,0),;
						If(G84->G84_TPFAT == '4',(cAliasTot)->REPASSE,0)})
		EndIf
		(cAliasTot)->(dbSkip())
	EndDo

	(cAliasTot)->(dbCloseArea())

	If (cAliasEad)->G67_BASE == "1" //Entidade Adicional
		aTot := aSort(aTot,,,{ | x,y | y[1] > x[1] })
	EndIf

	For nX := 1 To Len(aTot)
		If lTotaliza //Se houver totalizador cadastrado
			If cTotAnt <> aTot[nX][1] .Or. lFirst

				cTotAnt := aTot[nX][1]
				lFirst	:= .F.
				//Identifica a descri��o dos totalizadores
				Do Case
					Case (cAliasEad)->G67_BASE == "1" //1=Entidades Adicionais
						If !Empty(aTot[nX][1])
							If 	G3G->(dbSeek(xFilial("G3G")+cCliPDF+cLojaPDF+(cAliasEad)->G67_CODEAD+aTot[nX][1]))
								cEntTot := AllTrim(G3G->G3G_ITEM) + " - " + AllTrim(G3G->G3G_DESCR)
							EndIf
						Else
							cEntTot := ""
						EndIf

					Case (cAliasEad)->G67_BASE == "2" //2=Solicitantes

						If SU5->(dbSeek(xFilial("SU5")+aTot[nX][1]))
							cEntTot := AllTrim(SU5->U5_CODCONT) + " - " + AllTrim(SU5->U5_CONTAT)
						EndIf

					Case (cAliasEad)->G67_BASE == "3" //3=Grupo de Produto

						If SBM->(dbSeek(xFilial("SBM")+aTot[nX][1]))
							cEntTot := AllTrim(SBM->BM_GRUPO) + " - " + AllTrim(SBM->BM_DESC)
						EndIf

					Case (cAliasEad)->G67_BASE == "4" //4=Filial de Venda

						aAreaSM0 := SM0->(GetArea())
						If SM0->(dbSeek(cEmpAnt+aTot[nX][1]))
							cEntTot := AllTrim(SM0->M0_CODFIL) + " - " + AllTrim(SM0->M0_NOMECOM)
						EndIf
						RestArea(aAreaSM0)
				EndCase

				//------------------------------------------------------------------------------------------
				// ENTIDADE TOTALIZADORA
				//------------------------------------------------------------------------------------------
				If nLin+75 > nPgBreak

					oPrint:EndPage()

					nLin := TURR13CAB(@oPrint,@nPagina,lReimp)

				EndIf

				oPrint:Box( nLin, 0060, nLin+75, 2284,, )
				nLin+=35

				oPrint:Say( nLin, 0080, SubStr(OEMTOANSI(cDscEntTot) +" - "+ OEMTOANSI(cEntTot),1,86)	,oFont12:oFont,, )
				
				If Len(OEMTOANSI(cDscEntTot) +" - "+ OEMTOANSI(cEntTot)) > 86	
					oPrint:Line(nLin, 0060, nLin+20, 0060,, )
					oPrint:Line(nLin, 2284, nLin+20, 2284,, )
					nLin+=20
					oPrint:Say( nLin, 0080,SubStr(OEMTOANSI(cDscEntTot) +" - "+ OEMTOANSI(cEntTot),87) 	,oFont12:oFont  )
				EndIf
				
				nLin+=50
			EndIf
		Else
			nLin+=25
		EndIf

		If G84->G84_TPFAT <> '2'
			G85->(dbSetOrder(2)) //G85_FILIAL + G85_PREFIX + G85_NUMFAT + G85_FILREF + G85_REGVEN + G85_ITVEND + G85_SEQIV
		Else
			G85->(dbSetOrder(3)) //G85_FILIAL + G85_PREFIX + G85_NUMFAT + G85_FILREF + G85_CODAPU + G85_IDIFA
		EndIf
		If G85->(dbSeek(xFilial("G85")+cPrefixo+cFatura+aTot[nX][2]))

			If G84->G84_TPFAT <> '2'
				G3Q->(dbSeek(xFilial("G3Q",G85->G85_FILREF)+G85->G85_REGVEN+G85->G85_ITVEND+G85->G85_SEQIV))
				G3R->(dbSeek(xFilial("G3R",G85->G85_FILREF)+G85->G85_REGVEN+G85->G85_ITVEND+G85->G85_SEQIV))
				SA1->(dbSeek(xFilial("SA1",G85->G85_FILREF)+G3Q->G3Q_CLIENT+G3Q->G3Q_LOJA))
				SA2->(dbSeek(xFilial("SA2",G85->G85_FILREF)+G3R->G3R_FORNEC+G3R->G3R_LOJA))
				SBM->(dbSeek(xFilial("SBM",G85->G85_FILREF)+G85->G85_GRPPRD))
			Else
				G81->(dbSeek(xFilial("G81",G85->G85_FILREF)+G85->G85_IDIFA))
				SA1->(dbSeek(xFilial("SA1",G85->G85_FILREF)+G81->G81_CLIENT+G81->G81_LOJA))
			EndIf

			SB1->(dbSeek(xFilial("SB1",G85->G85_FILREF)+G85->G85_CODPRD))

			//------------------------------------------------------------------------------------------
			// ITEM DE VENDA
			//------------------------------------------------------------------------------------------

			If aTot[nX][7] <> 0
				nLin := TURR13SEG(@oPrint,@nPagina,nLin,cDscEntTot,cEntTot,aTot[nX],cTpApur)
				nTotEad += aTot[nX][7]
				aTotais[1] += aTot[nX][4] //Tarifa Cliente
				aTotais[2] += aTot[nX][5] //Taxas
				aTotais[3] += aTot[nX][6] //Taxa Adm
				aTotais[4] += (aTot[nX][7] ) //Total
				aTotais[5] += aTot[nX][10] //Multas
				aTotais[6] += aTot[nX][11] //Abatim.
			EndIf
		EndIf

		If lTotaliza
			If Len(aTot) == nX .Or. aTot[nX][1] <> aTot[nX+1][1]
				//------------------------------------------------------------------------------------------
				// TOTAL ENTIDADE
				//------------------------------------------------------------------------------------------
				If nLin+100 > nPgBreak

					oPrint:EndPage()
					nLin := TURR13CAB(@oPrint,@nPagina,lReimp)

				EndIf

				oPrint:Box( nLin, 0060, nLin+100, 2284,, )
				nLin+=25
				
				oPrint:Say( nLin+=35, 0344, OEMTOANSI(STR0033)  					,oFont11N:oFont)  //Total
				
				oPrint:Say( nLin	, 0450, SubStr(OEMTOANSI(cDscEntTot) +" - "+ OEMTOANSI(cEntTot),1,86)	,oFont12:oFont,, )
				
				If Len(OEMTOANSI(cDscEntTot) +" - "+ OEMTOANSI(cEntTot)) > 86	
					oPrint:Line(nLin, 0060, nLin+20, 0060,, )
					oPrint:Line(nLin, 2284, nLin+20, 2284,, )
					nLin+=20
					oPrint:Say( nLin, 0450,SubStr(OEMTOANSI(cDscEntTot) +" - "+ OEMTOANSI(cEntTot),87) 	,oFont12:oFont  )
				EndIf
				
				
				oPrint:Say( nLin	, 2020, Transform(nTotEad,"@E 9,999,999.99") 	,oFont12:oFont,, )
				nTotEad := 0
				nLin+=65
			EndIf
		EndIf

	Next nX

	(cAliasEad)->(dbCloseArea())

	//------------------------------------------------------------------------------------------
	// TOTAL DA FATURA
	//------------------------------------------------------------------------------------------
	If nLin+145 > nPgBreak

		oPrint:EndPage()
		nLin := TURR13CAB(@oPrint,@nPagina,lReimp)

	EndIf

	oPrint:Box( nLin, 0060, nLin+145, 2284,, )
	nLin+=25

	nLin+=35
	
	If G84->G84_TPFAT == '4'
		oPrint:Say( nLin	,  800, OEMTOANSI(STR0034) 	 ,oFont11N:oFont)  //Tar.Nac.
		oPrint:Say( nLin	, 1100, OEMTOANSI(STR0035) 	 ,oFont11N:oFont)  //Taxas
		oPrint:Say( nLin	, 1370, OEMTOANSI(STR0036)   ,oFont11N:oFont)  //Taxa Adm.
		oPrint:Say( nLin	, 1670, OEMTOANSI(STR0091) 	 ,oFont11N:oFont)  //"Multa"
		oPrint:Say( nLin	, 1870, OEMTOANSI(STR0092) 	 ,oFont11N:oFont)  //"Repasse"
		oPrint:Say( nLin	, 2120, OEMTOANSI(STR0033)  ,oFont11N:oFont)  //Total
	ElseIf G84->G84_TPFAT <> '2'
		oPrint:Say( nLin	, 1100, OEMTOANSI(STR0034) 	,oFont11N:oFont)  //Tar.Nac.
		oPrint:Say( nLin	, 1400, OEMTOANSI(STR0035) 	,oFont11N:oFont)  //Taxas
		oPrint:Say( nLin	, 1670, OEMTOANSI(STR0036)  ,oFont11N:oFont)  //Taxa Adm.
		oPrint:Say( nLin	, 2120, OEMTOANSI(STR0033)  ,oFont11N:oFont)  //Total
	EndIf
	

	oPrint:Say( nLin+=45, 0400, OEMTOANSI(STR0037)+":"  			  ,oFont11N:oFont,, )
	If G84->G84_TPFAT == '4'
		oPrint:Say( nLin,  730, Transform(aTotais[1],"@E 9,999,999.99") ,oFont12:oFont,, )
		oPrint:Say( nLin, 1000, Transform(aTotais[2],"@E 9,999,999.99") ,oFont12:oFont,, )
		oPrint:Say( nLin, 1320, Transform(aTotais[3],"@E 9,999,999.99") ,oFont12:oFont,, )
		oPrint:Say( nLin, 1560, Transform(aTotais[5],"@E 9,999,999.99") ,oFont12:oFont,, )
		oPrint:Say( nLin, 1790, Transform(aTotais[6],"@E 9,999,999.99") ,oFont12:oFont,, )
		oPrint:Say( nLin, 2050, Transform(aTotais[4],"@E 9,999,999.99") ,oFont12:oFont,, )
	ElseIf G84->G84_TPFAT <> '2'
		oPrint:Say( nLin, 1000, Transform(aTotais[1],"@E 9,999,999.99") ,oFont12:oFont,, )
		oPrint:Say( nLin, 1320, Transform(aTotais[2],"@E 9,999,999.99") ,oFont12:oFont,, )
		oPrint:Say( nLin, 1600, Transform(aTotais[3],"@E 9,999,999.99") ,oFont12:oFont,, )
		oPrint:Say( nLin, 2050, Transform(aTotais[4],"@E 9,999,999.99") ,oFont12:oFont,, )
	Else
		oPrint:Say( nLin, 2050, Transform(aTotais[4],"@E 9,999,999.99") ,oFont12:oFont,, )
	EndIf

	nLin+=35

	//------------------------------------------------------------------------------------------
	// IMPRESSAO
	//------------------------------------------------------------------------------------------
	If nLin+100 > nPgBreak

		oPrint:EndPage()
		nLin := TURR13CAB(@oPrint,@nPagina,lReimp)

	EndIf

	oPrint:Box( nLin, 0060, nLin+100, 2284,, )
	nLin+=25

	oPrint:Say( nLin+=35, 0080, OEMTOANSI(STR0038)+":" 	 	,oFont11N:oFont) 
	oPrint:Say( nLin	, 0359, DtoC(dDataBase)		 		,oFont12:oFont,, )
	oPrint:Say( nLin	, 1132, OEMTOANSI(STR0039)+":" 	 	,oFont11N:oFont) 
	oPrint:Say( nLin	, 1411, SubStr(cUsuario,7,15)		,oFont12:oFont,, )

	nLin+=50

	If nLin > 2700

		oPrint:EndPage()

		nLin := TURR13CAB(@oPrint,@nPagina,lReimp)

	EndIf
	//------------------------------------------------------------------------------------------
	// CODIGO DE BARRAS
	//------------------------------------------------------------------------------------------
	cCodBar := StrTran(G84->(G84_PREFIX+G84_NUMFAT)," ","*")

	For nIndexPre:= 1 TO TAMSX3('G84_PREFIX')[1]
		cPefixo +=  PADL(ASC(SUBSTRING(G84->G84_PREFIX,nIndexPre,1)),3,'0')
	Next nIndexPre
	
	For nIndexPre:= 1 TO TAMSX3('G84_FILIAL')[1]
		cFilAux += PADL(ASC(SUBSTRING(G84->G84_FILIAL,nIndexPre,1)),3,'0')
	Next nIndexPre
	
	If nLin+167 > nPgBreak

		oPrint:EndPage()
		nLin := TURR13CAB(@oPrint,@nPagina,lReimp)

	EndIf

	oPrint:Box( nLin, 0060, nLin+167, 2284,, )
	nLin+=98

	oPrint:Code128c(nLin+53,0224,cFilAux+cPefixo+G84->G84_NUMFAT,45) 
	 
	oPrint:Say(nLin,1512,cCodBar,oFont16N:oFont,,)

	nLin+= 79

	//------------------------------------------------------------------------------------------
	// OBSERVA��O
	//------------------------------------------------------------------------------------------
	cObs := AllTrim(G84->G84_MSGOBS)
	aObs := TurBreakLine(cObs,oPrint,oFont12:oFont,oPrint:nPageWidth-50 ) //2224 � o tamanho em pixels da largura do box

	If !Empty(aObs)

		If nLin+145 > nPgBreak //?? VER AQUI PARA A QUEBRA DA PAGINA QUE ESTA DEIXANDO UM ESPA�O GRANDE - RADU

			oPrint:EndPage()
			nLin := TURR13CAB(@oPrint,@nPagina,lReimp)

		EndIf

		oPrint:Line(nLin, 0060, nLin	, 2286,, )
		oPrint:Line(nLin, 0060, nLin+70	, 0060,, )
		oPrint:Line(nLin, 2286, nLin+70	, 2286,, )
		nLin+=25

		oPrint:Say( nLin+35, 0080, OEMTOANSI(STR0040) 	,oFont11N:oFont) 
		nLin+=45

		For nX := 1 To Len(aObs)

			If nLin+70 > nPgBreak
				oPrint:Line(nLin, 0060, nLin	, 2286,, )
				oPrint:EndPage()
				nLin := TURR13CAB(@oPrint,@nPagina,lReimp)
				nLin+=25
				oPrint:Line(nLin, 0060, nLin, 2286,, )
			EndIf

			oPrint:Line(nLin, 0060, nLin+45, 0060,, )
			oPrint:Line(nLin, 2286, nLin+45, 2286,, )
			nLin+=45

			If ( At(chr(13) + chr(10),aObs[nX]) > 0 )
				oPrint:Say( nLin, 0080, "" 			,oFont12:oFont)
			Else
				oPrint:Say( nLin, 0080, aObs[nX] 	,oFont12:oFont)
			EndIf

		Next nX
		oPrint:Line(nLin, 0060, nLin+25	, 0060,, )
		oPrint:Line(nLin, 2286, nLin+25	, 2286,, )
		nLin+=25
		oPrint:Line(nLin, 0060, nLin	, 2286,, )
	EndIf

Else
	FwAlertError(STR0096 + xFilial("G84") + " # " + cFilAnt + " - " + cPrefixo + " - " + cFatura + " - " + cCliPDF + " - " + cLojaPDF + " - " + STR0102 + G84->(DBFilter()) + " - ", STR0097)               // "Fatura n�o encontrada! "     "Falha na impress�o da Fatura."		"FILTRO: "
EndIf

oPrint:EndPage()

Return(oPrint)


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURR13CAB
Fun��o para impress�o de Fatura
@author    Cleyton
@version   1.00
@since     01/05/2016
/*/
//------------------------------------------------------------------------------------------
Static Function TURR13CAB(oPrint,nPagina,lReimp)

Local oFont11N   := TFontEx():New(oPrint,"Arial",11, 11,.T.,.T.,.F.)
Local oFont12    := TFontEx():New(oPrint,"Courier",12, 12,.T.,.T.,.F.)
Local oFont14    := TFontEx():New(oPrint,"Arial",14, 14,.F.,.T.,.F.)
Local oFont14N    := TFontEx():New(oPrint,"Arial",14, 14,.T.,.T.,.F.)
Local oFont16N   := TFontEx():New(oPrint,"Arial",16, 16,.T.,.T.,.F.)

Local nLin       := 0
Local cEndCob	 := ""
Local cCidCob	 := ""
Local aAreaSM0	:= {}

Default lReimp := .F.

nPagina++

oPrint:StartPage()

If nPagina > 1
	//------------------------------------------------------------------------------------------
	// CABE�ALHO OUTRAS PAGINAS
	//------------------------------------------------------------------------------------------
	oPrint:Box( 0100 ,0060 ,0200 ,2284 )
	
	oPrint:Say( 0160, 0080, OEMTOANSI(STR0045)+":"		        									,oFont11N:oFont)  //"P�gina"
	oPrint:Say( 0160, 0344, OEMTOANSI(AllTrim(Str(nPagina)))   										,oFont12:oFont  )
	oPrint:Say( 0160, 0730, OEMTOANSI(STR0046)+":"		        									,oFont11N:oFont)  //"Pref. - Fatura"
	oPrint:Say( 0160, 0994, OEMTOANSI(AllTrim(G84->(G84_PREFIX))+" - "+AllTrim(G84->(G84_NUMFAT)))	,oFont12:oFont  )
	oPrint:Say( 0160, 1318, OEMTOANSI(STR0053)+":"		        									,oFont11N:oFont)  //"Filial"
	aAreaSM0 := SM0->(GetArea())
	If SM0->(dbSeek(cEmpAnt+G84->G84_FILIAL))
		oPrint:Say( 0160, 1482, OEMTOANSI(G84->G84_FILIAL) +" - "+	AllTrim(SM0->M0_NOMECOM)		,oFont12:oFont  )
	EndIf
	RestArea(aAreaSM0)

	nLin := 0225

Else
	//------------------------------------------------------------------------------------------
	// INFORMA��ES DA EMPRESA
	//------------------------------------------------------------------------------------------
	oPrint:Box( 		0100, 0060, 0470, 2284 )
	
	If !lReimp .Or. Empty(cFilAnt) .OR. Empty(SM0->M0_NOMECOM)
		SM0->(dbSeek(cEmpAnt+G84->G84_FILIAL))
	EndIf
	
	cEndCob := AllTrim(SM0->M0_ENDCOB) + If(!Empty(SM0->M0_COMPCOB)," - " + AllTrim(SM0->M0_COMPCOB), "") + " - " + AllTrim(SM0->M0_BAIRCOB)
	cCidCob := AllTrim(SM0->M0_CIDCOB) + " - " + AllTrim(SM0->M0_ESTCOB)  + " - " + OEMTOANSI(STR0051) + " - " + STR0011+": " + TransForm(SM0->M0_CEPCOB, PesqPict("SA1", "A1_CEP"))

	oPrint:Say( 0159, (1142-(Int(oPrint:GetTextWidth(AllTrim(SM0->M0_NOMECOM),oFont16N:oFont)/3))), SM0->M0_NOMECOM		,oFont16N:oFont)
	oPrint:Say( 0213, (1142-(Int(oPrint:GetTextWidth(AllTrim(cEndCob),oFont14:oFont)/3))), AllTrim(cEndCob)		,oFont14:oFont) // Endere�o
	oPrint:Say( 0266, (1142-(Int(oPrint:GetTextWidth(AllTrim(cCidCob),oFont14:oFont)/3))), AllTrim(cCidCob)		,oFont14:oFont)	// Cidade
	oPrint:Say( 0319, 0468, OEMTOANSI(STR0083)+":"																,oFont14N:oFont)  //"Fone"
	oPrint:Say( 0319, 0703, TransForm(SM0->M0_TEL, "@R (99) 9999-9999" )											,oFont14:oFont  )
	oPrint:Say( 0373, 0468, OEMTOANSI(STR0041)+":" 																,oFont14N:oFont)  //"HomePage"
	oPrint:Say( 0373, 0703, AllTrim(GetMV("MV_TURSITE"))														,oFont14:oFont  )
	oPrint:Say( 0319, 1256, OEMTOANSI(STR0020)+":"																,oFont14N:oFont)  //"E-mail"
	oPrint:Say( 0319, 1445, AllTrim(GetMV("MV_TURMAIL"))														,oFont14:oFont  )
	oPrint:Say( 0426, 0468, OEMTOANSI(STR0042)+":" 																,oFont14N:oFont)  //"CNPJ"
	oPrint:Say( 0426, 0703, TransForm(SM0->M0_CGC, "@R 99.999.999/9999-99" )									,oFont14:oFont 	)
	oPrint:Say( 0373, 1256, OEMTOANSI(STR0084+":")																,oFont14N:oFont)  //"Embratur"
	oPrint:Say( 0373, 1445, AllTrim(GetMV("MV_TUREMBT"))														,oFont14:oFont  )
	oPrint:Say( 0426, 1256, OEMTOANSI(STR0044)+":" 																,oFont14N:oFont)  //"IATA"
	oPrint:Say( 0426, 1445, AllTrim(Posicione("G3M",1,xFilial("G3M")+G4M->G4M_CODPOS,"G3M_NIATA" ))				,oFont14:oFont 	)

	//------------------------------------------------------------------------------------------
	// INFORMA��ES DA FATURA
	//------------------------------------------------------------------------------------------
	oPrint:Box( 		0480 ,0060 ,0640 ,2284 )
	//---------------------------------------------------------------------------------------------------------------------------------------
	oPrint:Say( 0545, 0080, OEMTOANSI(STR0045)+":"		        									,oFont11N:oFont)  //"P�gina"
	oPrint:Say( 0545, 0344, OEMTOANSI(AllTrim(Str(nPagina)))   										,oFont12:oFont  )
	//---------------------------------------------------------------------------------------------------------------------------------------
	oPrint:Say( 0545, 0420, OEMTOANSI(STR0046)+":"		        									,oFont11N:oFont)  //"Pref. - Fatura"
	oPrint:Say( 0545, 0684, OEMTOANSI(AllTrim(G84->(G84_PREFIX))+" - "+AllTrim(G84->(G84_NUMFAT)))	,oFont12:oFont  )
	oPrint:Say( 0590, 0420, OEMTOANSI(STR0049)+":"													,oFont11N:oFont)  //"Data Emiss�o"
	oPrint:Say( 0590, 0684, DTOC(G84->G84_EMISS) 	        											,oFont12:oFont  )
	//---------------------------------------------------------------------------------------------------------------------------------------
	oPrint:Say( 0545, 1020, OEMTOANSI(STR0053)+":"		        									,oFont11N:oFont)  //"Filial"
	aAreaSM0 := SM0->(GetArea())
	If SM0->(dbSeek(cEmpAnt+G84->G84_FILIAL))
		oPrint:Say( 0545, 1250, OEMTOANSI(G84->G84_FILIAL) +" - "+	AllTrim(SM0->M0_NOMECOM)		,oFont12:oFont  )
	EndIf
	RestArea(aAreaSM0)
	oPrint:Say( 0590, 1020, OEMTOANSI(STR0050)+":"		        									,oFont11N:oFont)  //"Moeda"
	oPrint:Say( 0590, 1250, AllTrim(Posicione("G5T",1,xFilial("G5T")+G84->G84_MOEDA,"G5T_SIMBOL" )),oFont12:oFont  )
	
	//---------------------------------------------------------------------------------------------------------------------------------------
	If G84->G84_TOTAL < 0
		oPrint:Say( 0545, 1700, OEMTOANSI(STR0047)													,oFont11N:oFont  ) //"(FATURA DE CREDITO)"
	EndIf


	nLin := 0665
EndIf

Return(nLin)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURR13SEG
Fun��o para impress�o de Fatura
@author    Cleyton
@version   1.00
@since     01/05/2016
/*/
//------------------------------------------------------------------------------------------
Static Function TURR13SEG(oPrint,nPagina,nLin,cDscAgru,cQuebra,aTot,cTpApur)

Local aArea		 	:= (GetArea())
Local aAreaSA2   	:= SA2->(GetArea())
Local aAreaG3S   	:= G3S->(GetArea())
Local oFont11    	:= TFontEx():New(oPrint, "Courier", 11, 11, .T., .T., .F.)
Local oFont11N   	:= TFontEx():New(oPrint, "Arial"  , 11, 11, .T., .T., .F.)
Local oFont12    	:= TFontEx():New(oPrint, "Courier", 12, 12, .T., .T., .F.)
Local cAliasEntA 	:= ""
Local nTamG4BTit	:= TamSx3("G4B_TITUL")[1]
Local cConinu   	:= Space(TamSx3("G4C_CONINU")[1])
Local cDatas        := ""
Local lQuebraData   := .T.
Local aTpProd       := TURGetComb('G3U_TPPROD', 1)
Local aCatBus       := TURGetComb('G3W_CATBUS', 1)
Local nLinPula      := 0
Local nPgBreak		:= oPrint:nPageHeight*0.90
Local lTURR013PRT	:= ExistBlock("TURR013PRT")
Local aObsG43       := {}
Local nG43l         := 1
Local cAliasAux     := ""
Local cAliasAer		:= ''
Local lSegAereo		:= .F.
Local cTrechos		:= ''
Local lServProp     := G3Q->G3Q_OPERAC == "4"

If lTURR013PRT	
	ExecBlock("TURR013PRT",.f.,.f.,{oPrint,nLin})
EndIf

If nLin+120 > nPgBreak
	oPrint:EndPage()
	nLin := TURR13CAB(@oPrint,@nPagina)
EndIf

oPrint:Box( nLin, 0060, nLin+170, 2284,, )

nLin+=35

oPrint:Say( nLin, 0080, OEMTOANSI(STR0053), oFont11N:oFont)  //"Filial"

If G84->G84_TPFAT <> '2'
	oPrint:Say( nLin, 0230, OEMTOANSI(STR0054), oFont11N:oFont)  //"RV"
	oPrint:Say( nLin, 0430, OEMTOANSI(STR0055), oFont11N:oFont)  //"IV"
	oPrint:Say( nLin, 0550, OEMTOANSI(STR0029), oFont11N:oFont)  //"Emiss�o"
	oPrint:Say( nLin, 0750, OEMTOANSI(STR0057), oFont11N:oFont)  //"Produto"
Else // Apura��o
	oPrint:Say( nLin, 0250, OEMTOANSI(STR0088), oFont11N:oFont)  //"Apura��o"
	oPrint:Say( nLin, 0630, OEMTOANSI(STR0029), oFont11N:oFont)  //"Emiss�o"
	If cTpApur == '1'
		oPrint:Say( nLin, 0860, OEMTOANSI(STR0075), oFont11N:oFont)  //"Descri��o"
	Else
		oPrint:Say( nLin, 0860, OEMTOANSI(STR0057), oFont11N:oFont)  //"Produto"
	EndIf
EndIf

If G84->G84_TPFAT == '4'
	oPrint:Say( nLin, 1140, OEMTOANSI(STR0058), oFont11N:oFont)  //"Tarifa"
	oPrint:Say( nLin, 1300, OEMTOANSI(STR0059), oFont11N:oFont)  //"Tx.Cambio"
	oPrint:Say( nLin, 1530, OEMTOANSI(STR0060), oFont11N:oFont)  //"Tar.Nac."
	oPrint:Say( nLin, 1750, OEMTOANSI(STR0035), oFont11N:oFont)  //"Taxas"
	oPrint:Say( nLin, 1890, OEMTOANSI(STR0036), oFont11N:oFont)  //"Taxa Adm."
	oPrint:Say( nLin, 2150, OEMTOANSI(STR0091), oFont11N:oFont)  //"Multa"
Else
	If G84->G84_TPFAT <> '2'
		oPrint:Say( nLin, 1140, OEMTOANSI(STR0058), oFont11N:oFont)  //"Tarifa"
		oPrint:Say( nLin, 1300, OEMTOANSI(STR0059), oFont11N:oFont)  //"Tx.Cambio"
		oPrint:Say( nLin, 1530, OEMTOANSI(STR0060), oFont11N:oFont)  //"Tar.Nac."
		oPrint:Say( nLin, 1750, OEMTOANSI(STR0035), oFont11N:oFont)  //"Taxas"
		oPrint:Say( nLin, 1890, OEMTOANSI(STR0036), oFont11N:oFont)  //"Taxa Adm."
	Else
		oPrint:Say( nLin, 1450, OEMTOANSI(STR0100), oFont11N:oFont)  //"Per�odo Inicial"
		oPrint:Say( nLin, 1700, OEMTOANSI(STR0101), oFont11N:oFont)  //"Per�odo Final"
	EndIf
		
	oPrint:Say( nLin, 2150, OEMTOANSI(STR0033), oFont11N:oFont)  //Total
	
EndIf

nLin+=60

oPrint:Say( nLin, 0080, OEMTOANSI(G85->G85_FILREF)	  , oFont11:oFont )
If G84->G84_TPFAT <> '2'
	oPrint:Say( nLin, 0220, OEMTOANSI(G85->G85_REGVEN), oFont11:oFont )
	oPrint:Say( nLin, 0430, OEMTOANSI(G85->G85_ITVEND), oFont11:oFont )
	oPrint:Say( nLin, 0550, DtoC(G85->G85_EMISSA)	  , oFont11:oFont )
Else
	oPrint:Say( nLin, 0250, OEMTOANSI(G85->G85_CODAPU), oFont11:oFont )
	oPrint:Say( nLin, 0630, DtoC(G85->G85_EMISSA)	  , oFont11:oFont )
EndIf

If G84->G84_TPFAT == '4'
	oPrint:Say( nLin, 0750, OEMTOANSI(SBM->BM_GRUPO)				 	 , oFont11:oFont )
	oPrint:Say( nLin, 1020, Transform(aTot[4]/aTot[3], "@E 9,999,999.99"), oFont11:oFont ) //Tarifa Base = Tarifa Cliente / Taxa Cambio
	oPrint:Say( nLin, 1250, Transform(aTot[3]		 , "@E 9,999,999.99"), oFont11:oFont ) //Tx.Cambio
	oPrint:Say( nLin, 1460, Transform(aTot[4]		 , "@E 9,999,999.99"), oFont11:oFont ) //Tarifa Cliente
	oPrint:Say( nLin, 1650, Transform(aTot[5]		 , "@E 9,999,999.99"), oFont11:oFont ) //Taxas
	oPrint:Say( nLin, 1840, Transform(aTot[6]		 , "@E 9,999,999.99"), oFont11:oFont ) //Taxa Adm
	oPrint:Say( nLin, 2050, Transform(aTot[10]		 , "@E 9,999,999.99"), oFont11:oFont ) //Multa

	nLin+=45
	oPrint:Say( nLin, 0080, OEMTOANSI(STR0093) + ":"		    , oFont11N:oFont )	//"Prod."
	oPrint:Say( nLin, 0250, OEMTOANSI(SBM->BM_DESC)		        , oFont11:oFont  )	//Descri��o do Grupo de Produtos
	oPrint:Say( nLin, 1820, OEMTOANSI(STR0033)			        , oFont11N:oFont ) 	//Total
	oPrint:Say( nLin, 2050, Transform(aTot[7],"@E 9,999,999.99"), oFont11:oFont  ) 	//Total

Else

	If G84->G84_TPFAT <> '2'
		oPrint:Say( nLin, 0750, OEMTOANSI(SBM->BM_GRUPO)                     , oFont11:oFont )
		oPrint:Say( nLin, 1020, Transform(aTot[4]/aTot[3], "@E 9,999,999.99"), oFont11:oFont ) //Tarifa Base = Tarifa Cliente / Taxa Cambio
		oPrint:Say( nLin, 1250, Transform(aTot[3]		 , "@E 9,999,999.99"), oFont11:oFont ) //Tx.Cambio
		oPrint:Say( nLin, 1460, Transform(aTot[4]		 , "@E 9,999,999.99"), oFont11:oFont ) //Tarifa Cliente
		oPrint:Say( nLin, 1650, Transform(aTot[5]		 , "@E 9,999,999.99"), oFont11:oFont ) //Taxas
		oPrint:Say( nLin, 1840, Transform(aTot[6]		 , "@E 9,999,999.99"), oFont11:oFont ) //Taxa Adm
		oPrint:Say( nLin, 2050, Transform(aTot[7]		 , "@E 9,999,999.99"), oFont11:oFont ) //Total
	Else
		DbSelectArea("G6L")
		G6L->(DbSetOrder(1))	// G6L_FILIAL+G6L_CODAPU
		G6L->(DbSeek(G81->G81_FILIAL + G81->G81_CODAPU))
		
		If cTpApur == '1'	
			oPrint:Say( nLin, 0860, IIF(aTot[7] > 0, STR0098, STR0099) + IIF(G81->G81_SEGNEG == "1", Upper(STR0001), IIF(G81->G81_SEGNEG == "2", Upper(STR0002), Upper(STR0003))), oFont11:oFont )		// "FEE "	// "REPASSE "	// "CORPORATIVO"	// "EVENTO"		// "LAZER"    
			oPrint:Say( nLin, 1450, OEMTOANSI(DtoC(G6L->G6L_DTINI)), oFont11:oFont)
			oPrint:Say( nLin, 1700, OEMTOANSI(DtoC(G6L->G6L_DTFIM)), oFont11:oFont)
			oPrint:Say( nLin, 2050, Transform(aTot[7], "@E 9,999,999.99"), oFont11:oFont ) //Total
		Else
			oPrint:Say( nLin, 0860, OEMTOANSI(G85->G85_CODPRD), oFont11:oFont )
			oPrint:Say( nLin, 1450, OEMTOANSI(DtoC(G6L->G6L_DTINI)), oFont11:oFont)
			oPrint:Say( nLin, 1700, OEMTOANSI(DtoC(G6L->G6L_DTFIM)), oFont11:oFont)
			oPrint:Say( nLin, 2050, Transform(aTot[7], "@E 9,999,999.99"), oFont11:oFont ) //Total
	
			nLin += 45
	
			oPrint:Say( nLin, 0080, OEMTOANSI(STR0075 + " " + STR0093) + ":", oFont11N:oFont ) //"Prod."
			oPrint:Say( nLin, 0320, OEMTOANSI(Posicione("SB1", 1, xFilial("SB1") + G85->G85_CODPRD, "B1_DESC")), oFont11:oFont )
		EndIf
		G6L->(DbCloseArea())
	EndIf
EndIf

nLin+=25

If G84->G84_TPFAT <> '2'
	SA2->(dbSeek(xFilial("SA2",G85->G85_FILREF)+G3R->(G3R_FORNEC+G3R_LOJA)))

	DbSelectArea("G3S")
	G3S->(DbSetOrder(1)) //G3S_FILIAL + G3S_NUMID + G3S_IDITEM + G3S_NUMSEQ + G3S_CODPAX + G3S_CONORI

	If G3S->(DbSeek(xFilial("G3S",G85->G85_FILREF)+G85->G85_REGVEN+G85->G85_ITVEND+G85->G85_SEQIV))

		While G3S->(!Eof()) .And. (G85->G85_REGVEN + G85->G85_ITVEND + G85->G85_SEQIV) == (G3S->G3S_NUMID + G3S->G3S_IDITEM + G3S->G3S_NUMSEQ)
			If Empty(G3S->G3S_CONINU)
				
				//------------------------------------------------------------------------------------------
				// INFORMA��ES DO REEMBOLSO
				//------------------------------------------------------------------------------------------
				If G3Q->G3Q_OPERAC = '2'
					cAliasAux := TR13DdsSeg('R', xFilial("G4E", G85->G85_FILREF), G85->G85_REGVEN, G85->G85_ITVEND, G85->G85_SEQIV)
					If (cAliasAux)->(!EOF())
						If nLin+295 > nPgBreak
							oPrint:EndPage()
							nLin := TURR13CAB(@oPrint,@nPagina)
						EndIf

						oPrint:Box( nLin, 0060, nLin+295, 2284,, )
						oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0057) + ":", oFont11N:oFont)  //"Produto"
						oPrint:Say( nLin	, 0600, SBM->BM_DESC			, oFont12:oFont )					
						oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0061) + ":", oFont11N:oFont)  //"PAX"
						oPrint:Say( nLin	, 0600, AllTrim(G3S->G3S_NOME)	, oFont12:oFont )
						oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0062) + ":", oFont11N:oFont)  //"Bilhete/Doc"
						oPrint:Say( nLin	, 0600, AllTrim(G3Q->G3Q_DOC) + IIF( !lServProp, " � " + AllTrim(SA2->A2_NREDUZ), "") + IIF(G3Q->G3Q_TPSEG == "1", " - " + R013Trecho(G3Q->G3Q_DOCORI), ""), oFont12:oFont)
						oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0095) + ":", oFont11N:oFont)  //"Data prev - Data Cred" //FALTA STRING FIXA
						oPrint:Say( nLin	, 0600, DtoC(SToD((cAliasAux)->G4E_DTPREV)) + " � " + DtoC(SToD((cAliasAux)->G4E_DTCRED)), oFont12:oFont)
						oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0064) + ":", oFont11N:oFont)  //"Solicitante"
						oPrint:Say( nLin	, 0600, G3Q->G3Q_NOMESO			, oFont12:oFont )
						oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0065) + ":", oFont11N:oFont)  //"Emissor"
						oPrint:Say( nLin	, 0600, IIF( !lServProp, G3R->G3R_NOMREP, "")			, oFont12:oFont )
					EndIf
					(cAliasAux)->(DbCloseArea())
				Else
					cAliasAux := TR13DdsSeg(G3Q->G3Q_TPSEG, xFilial("G4E", G85->G85_FILREF), G85->G85_REGVEN, G85->G85_ITVEND, G85->G85_SEQIV)
					If (cAliasAux)->(!EOF())				
						Do Case 
							//------------------------------------------------------------------------------------------
							// INFORMA��ES DO AEREO
							//------------------------------------------------------------------------------------------
							Case G3Q->G3Q_TPSEG = '1'
								If nLin+295 > nPgBreak
									oPrint:EndPage()
									nLin := TURR13CAB(@oPrint,@nPagina)
								EndIf
								cAliasAer	:= GetNextAlias()
								
								lSegAereo:= .F.
								If ItensArer(cAliasAer, xFilial("G3T",G85->G85_FILREF), G85->G85_REGVEN, G85->G85_ITVEND, G85->G85_SEQIV, G3S->G3S_CODPAX, @cDatas, @cTrechos)
									lSegAereo:= .T.
			 						oPrint:Box( nLin, 0060, nLin+320, 2284,, )				
														
									If nLin+320 > nPgBreak
										oPrint:EndPage()
										nLin := TURR13CAB(@oPrint,@nPagina)
										
										oPrint:Line(nLin, 		0060, nLin+350, 0060,, )
										oPrint:Line(nLin, 		2286, nLin+350, 2286,, )
										oPrint:Line(nLin+350,	0060, nLin+350, 2286,, )												
									EndIf
									oPrint:Say( nLin+=50	, 0344, OEMTOANSI(STR0057) + ":", oFont11N:oFont)  //"Produto"
									oPrint:Say( nLin		, 0600, SBM->BM_DESC			, oFont12:oFont )
									oPrint:Say( nLin+=40	, 0344, OEMTOANSI(STR0061) + ":", oFont11N:oFont)  //"PAX"
									oPrint:Say( nLin		, 0600, AllTrim(G3S->G3S_NOME)  , oFont12:oFont )
									oPrint:Say( nLin+=45	, 0344, OEMTOANSI(STR0062) + ":", oFont11N:oFont)  //"Bilhete/Doc"
									oPrint:Say( nLin		, 0600, AllTrim(G3Q->G3Q_DOC) + IIF( !lServProp, " � " + AllTrim(SA2->A2_NREDUZ), "") + " � " + cTrechos, oFont12:oFont)															
									oPrint:Say( nLin+=45	, 0344, OEMTOANSI(STR0063) + ":",oFont11N:oFont )  //"Data In-Out"
									oPrint:Say( nLin		, 0600, AllTrim(SubStr(cDatas, 1, 99)), oFont12:oFont)
									
									oPrint:Say( nLin+=45	, 0344, OEMTOANSI(STR0064) + ":", oFont11N:oFont)  //"Solicitante"
									oPrint:Say( nLin		, 0600, G3Q->G3Q_NOMESO         , oFont12:oFont )
									oPrint:Say( nLin+=45	, 0344, OEMTOANSI(STR0065) + ":", oFont11N:oFont)  //"Emissor"
									oPrint:Say( nLin		, 0600, IIF( !lServProp, G3R->G3R_NOMREP, "")         , oFont12:oFont )								
																	
								EndIf				
							
							//------------------------------------------------------------------------------------------
							// INFORMA��ES DO HOTEL
							//------------------------------------------------------------------------------------------
							Case G3Q->G3Q_TPSEG = '2'
								If nLin+420 > nPgBreak
									oPrint:EndPage()
									nLin := TURR13CAB(@oPrint,@nPagina)
								EndIf
			
								cDatas   := TR13DtInOut(G85->G85_PREFIX, G85->G85_NUMFAT, G85->G85_REGVEN, G85->G85_ITVEND, "%G3U_DTINI%", "%G3U_DTFIM%")
								nLinPula := (Round((Len(cDatas) / 99), 0) - 1) * 25
								oPrint:Box( nLin, 0060, nLin+420 + IIF(nLinPula > 0, nLinPula, 0), 2284,, )
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0057) + ":", oFont11N:oFont)  //"Produto"//FAZER A STRING
								oPrint:Say( nLin	, 0600, SBM->BM_DESC			, oFont12:oFont )
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0061) + ":", oFont11N:oFont)  //""PAX""
								oPrint:Say( nLin	, 0600, AllTrim(G3S->G3S_NOME)  , oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0068) + ":", oFont11N:oFont)  //"Voucher"
								oPrint:Say( nLin	, 0600, AllTrim(G3Q->G3Q_DOC) + IIF( !lServProp, " � " + AllTrim(SA2->A2_NREDUZ), "") + " � " + AllTrim((cAliasAux)->G3U_CIDHOT), oFont12:oFont)
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0063) + ":", oFont11N:oFont)  //"Data In-Out"
								oPrint:Say( nLin	, 0600, AllTrim(SubStr(cDatas, 1, 99)), oFont12:oFont)
								
								While lQuebraData
									If Len(cDatas) > 99
										nLin += 25
										cDatas := AllTrim(SubStr(cDatas, 100))
										oPrint:Say( nLin, 0600, AllTrim(SubStr(cDatas, 1, 99)), oFont12:oFont)
									Else
										lQuebraData := .F.	
									EndIf
								EndDo
								lQuebraData := .T.
			
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0069) + ":", oFont11N:oFont)  //"Di�rias"
								oPrint:Say( nLin	, 0600, AllTrim(Transform((cAliasAux)->G3U_QTDDIA, PesqPict("G3U", "G3U_QTDDIA"))), oFont12:oFont)
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0070) + ":", oFont11N:oFont)  //"Tipo Prod."
								oPrint:Say( nLin	, 0600, G3Q->G3Q_PROD + " � " + AllTrim(SB1->B1_DESC) + " (" + aTpProd[Val((cAliasAux)->G3U_TPPROD)] + ") ", oFont12:oFont)
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0071) + ":", oFont11N:oFont)  //"Quantidade"
								oPrint:Say( nLin	, 0600, AllTrim(Transform((cAliasAux)->G3U_QTDPRD, PesqPict("G3U", "G3U_QTDPRD"))), oFont12:oFont)
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0064) + ":", oFont11N:oFont)  //"Solicitante"
								oPrint:Say( nLin	, 0600, G3Q->G3Q_NOMESO			, oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0065) + ":", oFont11N:oFont)  //"Emissor"
								oPrint:Say( nLin	, 0600, IIF( !lServProp, G3R->G3R_NOMREP, "") 		, oFont12:oFont )
	
							//------------------------------------------------------------------------------------------
							// INFORMA��ES DO CARRO
							//------------------------------------------------------------------------------------------
							Case G3Q->G3Q_TPSEG = '3'
								If nLin+375 > nPgBreak
									oPrint:EndPage()
									nLin := TURR13CAB(@oPrint,@nPagina)
								EndIf
			
								cDatas   := TR13DtInOut(G85->G85_PREFIX, G85->G85_NUMFAT, G85->G85_REGVEN, G85->G85_ITVEND, "%G3V_DTINI%", "%G3V_DTFIM%")
								nLinPula := (Round((Len(cDatas) / 99), 0) - 1) * 25
								oPrint:Box( nLin, 0060, nLin+375 + IIF(nLinPula > 0, nLinPula, 0), 2284,, )
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0057) + ":", oFont11N:oFont)  //"Produto"
								oPrint:Say( nLin	, 0600, SBM->BM_DESC			, oFont12:oFont )
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0061) + ":", oFont11N:oFont)  //"PAX"
								oPrint:Say( nLin    , 0600, AllTrim(G3S->G3S_NOME)  , oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0068) + ":", oFont11N:oFont)  //"Voucher"
								oPrint:Say( nLin    , 0600, AllTrim(G3Q->G3Q_DOC) + IIF( !lServProp, " � " + AllTrim(SA2->A2_NREDUZ), "") + " � " + AllTrim((cAliasAux)->G3V_CIDRET), oFont12:oFont)
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0063) + ":", oFont11N:oFont)  //"Data In-Out"
								oPrint:Say( nLin	, 0600, AllTrim(SubStr(cDatas, 1, 99)), oFont12:oFont)
								
								While lQuebraData
									If Len(cDatas) > 99
										nLin += 25
										cDatas := AllTrim(SubStr(cDatas, 100))
										oPrint:Say( nLin, 0600, AllTrim(SubStr(cDatas, 1, 99)), oFont12:oFont)
									Else
										lQuebraData := .F.	
									EndIf
								EndDo
								lQuebraData := .T.
			
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0069) + ":", oFont11N:oFont)  //"Di�rias"
								oPrint:Say( nLin    , 0600, AllTrim(Transform((cAliasAux)->G3V_QTDDIA, PesqPict("G3V", "G3V_QTDDIA"))), oFont12:oFont)
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0071) + ":", oFont11N:oFont)  //"Quantidade"
								oPrint:Say( nLin    , 0600, AllTrim(Transform((cAliasAux)->G3V_QTDPRD, PesqPict("G3V", "G3V_QTDPRD"))), oFont12:oFont)
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0064) + ":", oFont11N:oFont)  //"Solicitante"
								oPrint:Say( nLin    , 0600, G3Q->G3Q_NOMESO			, oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0065) + ":", oFont11N:oFont)  //"Emissor"
								oPrint:Say( nLin    , 0600, IIF( !lServProp, G3R->G3R_NOMREP, "") 		, oFont12:oFont )
	
							//------------------------------------------------------------------------------------------
							// INFORMA��ES DO RODOVIARIO
							//------------------------------------------------------------------------------------------
							Case G3Q->G3Q_TPSEG = '4'
								If nLin+375 > nPgBreak
									oPrint:EndPage()
									nLin := TURR13CAB(@oPrint,@nPagina)
								EndIf
			
								cDatas   := TR13DtInOut(G85->G85_PREFIX, G85->G85_NUMFAT, G85->G85_REGVEN, G85->G85_ITVEND, "%G3W_DTINI%", "%G3W_DTFIM%")
								nLinPula := (Round((Len(cDatas) / 99), 0) - 1) * 25
								oPrint:Box( nLin, 0060, nLin+375 + IIF(nLinPula > 0, nLinPula, 0), 2284,, )
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0057) + ":"      , oFont11N:oFont)  //"Produto"
								oPrint:Say( nLin	, 0600, SBM->BM_DESC			      , oFont12:oFont )
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0061) + ":"      , oFont11N:oFont)  //"PAX"
								oPrint:Say( nLin    , 0600, AllTrim(G3S->G3S_NOME)        , oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0068) + ":"      , oFont11N:oFont)  //"Voucher"
								oPrint:Say( nLin    , 0600, AllTrim(G3Q->G3Q_DOC) + IIF( !lServProp, " � " + AllTrim(SA2->A2_NREDUZ), ""), oFont12:oFont)
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0072) + ":"      , oFont11N:oFont)  //"Itiner�rio"
								oPrint:Say( nLin    , 0600, AllTrim(Posicione("G5S", 1, xFilial("G5S") + (cAliasAux)->G3W_CIDEMB, "G5S_CIDADE")) + " / " + AllTrim(Posicione("G5S", 1, xFilial("G5S") + (cAliasAux)->G3W_CIDDES, "G5S_CIDADE")), oFont12:oFont) 
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0073) + ":"      , oFont11N:oFont)  //"Categoria"
								oPrint:Say( nLin    , 0600, aCatBus[Val((cAliasAux)->G3W_CATBUS)], oFont12:oFont)
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0063) + ":"      , oFont11N:oFont)  //"Data In-Out"
								oPrint:Say( nLin	, 0600, AllTrim(SubStr(cDatas, 1, 99)), oFont12:oFont )
								
								While lQuebraData
									If Len(cDatas) > 99
										nLin += 25
										cDatas := AllTrim(SubStr(cDatas, 100))
										oPrint:Say( nLin, 0600, AllTrim(SubStr(cDatas, 1, 99)), oFont12:oFont)
									Else
										lQuebraData := .F.	
									EndIf
								EndDo
								lQuebraData := .T.
			
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0064) + ":", oFont11N:oFont)  //"Solicitante"
								oPrint:Say( nLin    , 0600, G3Q->G3Q_NOMESO			, oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0065) + ":", oFont11N:oFont)  //"Emissor"
								oPrint:Say( nLin    , 0600, IIF( !lServProp, G3R->G3R_NOMREP, "") 		, oFont12:oFont )
	
							//------------------------------------------------------------------------------------------
							// INFORMA��ES DO TREM
							//------------------------------------------------------------------------------------------
							Case G3Q->G3Q_TPSEG = '6'
								If nLin+375 > nPgBreak
									oPrint:EndPage()
									nLin := TURR13CAB(@oPrint,@nPagina)
								EndIf
			
								cDatas   := TR13DtInOut(G85->G85_PREFIX, G85->G85_NUMFAT, G85->G85_REGVEN, G85->G85_ITVEND, "%G3X_DTINI%", "%G3X_DTFIM%")
								nLinPula := (Round((Len(cDatas) / 99), 0) - 1) * 25
								oPrint:Box( nLin, 0060, nLin+375 + IIF(nLinPula > 0, nLinPula, 0), 2284,, )
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0057) + ":", oFont11N:oFont)  //"Produto"
								oPrint:Say( nLin	, 0600, SBM->BM_DESC			, oFont12:oFont )
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0061) + ":", oFont11N:oFont)  //"PAX"
								oPrint:Say( nLin    , 0600, AllTrim(G3S->G3S_NOME)	, oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0068) + ":", oFont11N:oFont)  //"Voucher"
								oPrint:Say( nLin    , 0600, AllTrim(G3Q->G3Q_DOC) + IIF( !lServProp, " � " + AllTrim(SA2->A2_NREDUZ), ""), oFont12:oFont)
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0072) + ":", oFont11N:oFont)  //"Itiner�rio"
								oPrint:Say( nLin    , 0600, AllTrim((cAliasAux)->G3X_CIDEMB) + " / " + AllTrim((cAliasAux)->G3X_CIDDES), oFont12:oFont)
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0073) + ":", oFont11N:oFont)  //"Categoria"
								oPrint:Say( nLin    , 0600, AllTrim((cAliasAux)->G3X_CTTREM), oFont12:oFont)
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0063) + ":", oFont11N:oFont)  //"Data In-Out"
								oPrint:Say( nLin	, 0600, AllTrim(SubStr(cDatas, 1, 99)), oFont12:oFont)
								
								While lQuebraData
									If Len(cDatas) > 99
										nLin += 25
										cDatas := AllTrim(SubStr(cDatas, 100))
										oPrint:Say( nLin, 0600, AllTrim(SubStr(cDatas, 1, 99)), oFont12:oFont)
									Else
										lQuebraData := .F.	
									EndIf
								EndDo
								lQuebraData := .T.
			
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0064) + ":", oFont11N:oFont)  //"Solicitante"
								oPrint:Say( nLin    , 0600, G3Q->G3Q_NOMESO			, oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0065) + ":", oFont11N:oFont)  //"Emissor"
								oPrint:Say( nLin    , 0600, IIF( !lServProp, G3R->G3R_NOMREP, "") 		, oFont12:oFont )
	
							//------------------------------------------------------------------------------------------
							// INFORMA��ES DO CRUZEIRO
							//------------------------------------------------------------------------------------------
							Case G3Q->G3Q_TPSEG = '5'
								If nLin+420 > nPgBreak
									oPrint:EndPage()
									nLin := TURR13CAB(@oPrint,@nPagina)
								EndIf
			
								G3Y->(DbGoTo((cAliasAux)->R_E_C_N_O_))
								cDatas   := TR13DtInOut(G85->G85_PREFIX, G85->G85_NUMFAT, G85->G85_REGVEN, G85->G85_ITVEND, "%G3Y_DTINI%", "%G3Y_DTFIM%")
								nLinPula := (Round((Len(cDatas) / 99), 0) - 1) * 25
								oPrint:Box( nLin, 0060, nLin+420 + IIF(nLinPula > 0, nLinPula, 0), 2284,, )
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0057) + ":", oFont11N:oFont)  //"Produto"
								oPrint:Say( nLin	, 0600, SBM->BM_DESC			, oFont12:oFont )
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0061) + ":", oFont11N:oFont)  //"PAX"
								oPrint:Say( nLin    , 0600, AllTrim(G3S->G3S_NOME)  , oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0068) + ":", oFont11N:oFont)  //"Voucher"
								oPrint:Say( nLin    , 0600, AllTrim(G3Q->G3Q_DOC) + IIF( !lServProp, " � " + AllTrim(SA2->A2_NREDUZ), ""), oFont12:oFont)
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0074) + ":", oFont11N:oFont)  //"Navio"
								oPrint:Say( nLin    , 0600, (cAliasAux)->G3Y_NOMNAV , oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0075) + ":", oFont11N:oFont)  //"Descri��o"
								oPrint:Say( nLin    , 0600, SubStr(G3Y->G3Y_DESNAV, 01, 50), oFont12:oFont)
								oPrint:Say( nLin+=45, 0600, SubStr(G3Y->G3Y_DESNAV, 51, 50), oFont12:oFont) 			//Continua��o Descri��o
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0063) + ":", oFont11N:oFont)  //"Data In-Out"
								oPrint:Say( nLin	, 0600, AllTrim(SubStr(cDatas, 1, 99)), oFont12:oFont)
								
								While lQuebraData
									If Len(cDatas) > 99
										nLin += 25
										cDatas := AllTrim(SubStr(cDatas, 100))
										oPrint:Say( nLin, 0600, AllTrim(SubStr(cDatas, 1, 99)), oFont12:oFont)
									Else
										lQuebraData := .F.	
									EndIf
								EndDo
								lQuebraData := .T.
			
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0064) + ":", oFont11N:oFont)  //"Solicitante"
								oPrint:Say( nLin    , 0600, G3Q->G3Q_NOMESO			, oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0065) + ":", oFont11N:oFont)  //"Emissor"
								oPrint:Say( nLin    , 0600, IIF( !lServProp, G3R->G3R_NOMREP, "") 		, oFont12:oFont )
	
							//------------------------------------------------------------------------------------------
							// INFORMA��ES DO PACOTE
							//------------------------------------------------------------------------------------------
							Case G3Q->G3Q_TPSEG = 'A'
								If nLin+375 > nPgBreak
									oPrint:EndPage()
									nLin := TURR13CAB(@oPrint,@nPagina)
								EndIf
			
								G3Z->(DbGoTo((cAliasAux)->R_E_C_N_O_))
								cDatas   := TR13DtInOut(G85->G85_PREFIX, G85->G85_NUMFAT, G85->G85_REGVEN, G85->G85_ITVEND, "%G3Z_DTINI%", "%G3Z_DTFIM%")
								nLinPula := (Round((Len(cDatas) / 99), 0) - 1) * 25
								oPrint:Box( nLin, 0060, nLin+375 + IIF(nLinPula > 0, nLinPula, 0), 2284,, )
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0057) + ":", oFont11N:oFont)  //"Produto"
								oPrint:Say( nLin	, 0600, SBM->BM_DESC			, oFont12:oFont )	
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0061) + ":", oFont11N:oFont)  //""PAX""
								oPrint:Say( nLin    , 0600, AllTrim(G3S->G3S_NOME)	, oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0068) + ":", oFont11N:oFont)  //"Voucher"
								oPrint:Say( nLin    , 0600, AllTrim(G3Q->G3Q_DOC) + IIF( !lServProp, " � " + AllTrim(SA2->A2_NREDUZ), ""), oFont12:oFont)
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0075) + ":", oFont11N:oFont)  //"Descri��o"
								oPrint:Say( nLin    , 0600, SubStr(G3Z->G3Z_DESPAC, 01, 50),oFont12:oFont)
								oPrint:Say( nLin+=45, 0600, SubStr(G3Z->G3Z_DESPAC, 51, 50),oFont12:oFont)			//Continua��o Descri��o
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0063) + ":", oFont11N:oFont)  //"Data In-Out"
								oPrint:Say( nLin	, 0600, AllTrim(SubStr(cDatas, 1, 99)), oFont12:oFont)
								
								While lQuebraData
									If Len(cDatas) > 99
										nLin += 25
										cDatas := AllTrim(SubStr(cDatas, 100))
										oPrint:Say( nLin, 0600, AllTrim(SubStr(cDatas, 1, 99)), oFont12:oFont)
									Else
										lQuebraData := .F.	
									EndIf
								EndDo
								lQuebraData := .T.
			
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0064) + ":", oFont11N:oFont)  //"Solicitante"
								oPrint:Say( nLin    , 0600, G3Q->G3Q_NOMESO			, oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0065) + ":", oFont11N:oFont)  //"Emissor"
								oPrint:Say( nLin    , 0600, IIF( !lServProp, G3R->G3R_NOMREP, "") 		, oFont12:oFont )
	
							//------------------------------------------------------------------------------------------
							// INFORMA��ES DO TOUR
							//------------------------------------------------------------------------------------------
							Case G3Q->G3Q_TPSEG = '9'
								If nLin+330 > nPgBreak
									oPrint:EndPage()
									nLin := TURR13CAB(@oPrint,@nPagina)
								EndIf
			
								cDatas   := TR13DtInOut(G85->G85_PREFIX, G85->G85_NUMFAT, G85->G85_REGVEN, G85->G85_ITVEND, "%G40_DTINI%", "%G40_DTFIM%")
								nLinPula := (Round((Len(cDatas) / 99), 0) - 1) * 25
								oPrint:Box( nLin, 0060, nLin+330 + IIF(nLinPula > 0, nLinPula, 0), 2284,, )
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0057) + ":", oFont11N:oFont)  //"Produto"
								oPrint:Say( nLin	, 0600, SBM->BM_DESC			, oFont12:oFont )
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0061) + ":", oFont11N:oFont)  //"PAX"
								oPrint:Say( nLin    , 0600, AllTrim(G3S->G3S_NOME)	, oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0068) + ":", oFont11N:oFont)  //"Voucher"
								oPrint:Say( nLin    , 0600, AllTrim(G3Q->G3Q_DOC) + IIF( !lServProp, " � " + AllTrim(SA2->A2_NREDUZ), ""), oFont12:oFont)
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0076) + ":", oFont11N:oFont)  //"Nome Tour"
								oPrint:Say( nLin    , 0600, (cAliasAux)->G40_NMTOUR	, oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0063) + ":", oFont11N:oFont)  //"Data In-Out"
								oPrint:Say( nLin	, 0600, AllTrim(SubStr(cDatas, 1, 99)), oFont12:oFont)
								
								While lQuebraData
									If Len(cDatas) > 99
										nLin += 25
										cDatas := AllTrim(SubStr(cDatas, 100))
										oPrint:Say( nLin, 0600, AllTrim(SubStr(cDatas, 1, 99)), oFont12:oFont)
									Else
										lQuebraData := .F.	
									EndIf
								EndDo
								lQuebraData := .T.
			
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0064) + ":", oFont11N:oFont)  //"Solicitante"
								oPrint:Say( nLin    , 0600, G3Q->G3Q_NOMESO			, oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0065)+":"	, oFont11N:oFont)  //"Emissor"
								oPrint:Say( nLin    , 0600, IIF( !lServProp, G3R->G3R_NOMREP, "") 		, oFont12:oFont )
	
							//------------------------------------------------------------------------------------------
							// INFORMA��ES DO SEGURO
							//------------------------------------------------------------------------------------------
							Case G3Q->G3Q_TPSEG = '8'
								If nLin+375 > nPgBreak
									oPrint:EndPage()
									nLin := TURR13CAB(@oPrint,@nPagina)
								EndIf
			
								cDatas   := TR13DtInOut(G85->G85_PREFIX, G85->G85_NUMFAT, G85->G85_REGVEN, G85->G85_ITVEND, "%G41_DTINI%", "%G41_DTFIM%")
								nLinPula := (Round((Len(cDatas) / 99), 0) - 1) * 25
								oPrint:Box( nLin, 0060, nLin+375 + IIF(nLinPula > 0, nLinPula, 0), 2284,, )
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0057) + ":", oFont11N:oFont)  //"Produto"
								oPrint:Say( nLin	, 0600, SBM->BM_DESC			, oFont12:oFont )
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0061) + ":", oFont11N:oFont)  //"PAX"
								oPrint:Say( nLin    , 0600, AllTrim(G3S->G3S_NOME)	, oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0068) + ":", oFont11N:oFont)  //"Voucher"
								oPrint:Say( nLin    , 0600, AllTrim(G3Q->G3Q_DOC) + IIF( !lServProp, " � " + AllTrim(SA2->A2_NREDUZ), ""), oFont12:oFont)
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0077) + ":", oFont11N:oFont)  //"Nome Plano"
								oPrint:Say( nLin    , 0600, (cAliasAux)->G41_PLSEGU	, oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0078) + ":", oFont11N:oFont)  //"Apolice"
								oPrint:Say( nLin    , 0600, (cAliasAux)->G41_NUMAPO	, oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0063) + ":", oFont11N:oFont)  //"Data In-Out"
								oPrint:Say( nLin	, 0600, AllTrim(SubStr(cDatas, 1, 99)), oFont12:oFont)
								
								While lQuebraData
									If Len(cDatas) > 99
										nLin += 25
										cDatas := AllTrim(SubStr(cDatas, 100))
										oPrint:Say( nLin, 0600, AllTrim(SubStr(cDatas, 1, 99)), oFont12:oFont)
									Else
										lQuebraData := .F.	
									EndIf
								EndDo
								lQuebraData := .T.
			
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0064) + ":", oFont11N:oFont)  //"Solicitante"
								oPrint:Say( nLin    , 0600, G3Q->G3Q_NOMESO			, oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0065) + ":", oFont11N:oFont)  //"Emissor"
								oPrint:Say( nLin    , 0600, IIF( !lServProp, G3R->G3R_NOMREP, "") 		, oFont12:oFont )
	
							//------------------------------------------------------------------------------------------
							// INFORMA��ES DO VISTO
							//------------------------------------------------------------------------------------------
							Case G3Q->G3Q_TPSEG = '7'
								If nLin+330 > nPgBreak
									oPrint:EndPage()
									nLin := TURR13CAB(@oPrint,@nPagina)
								EndIf
			
								oPrint:Box( nLin, 0060, nLin+330, 2284,, )
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0057) + ":", oFont11N:oFont)  //"Produto"
								oPrint:Say( nLin	, 0600, SBM->BM_DESC			, oFont12:oFont )
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0061) + ":", oFont11N:oFont)  //"PAX"
								oPrint:Say( nLin    , 0600, AllTrim(G3S->G3S_NOME)	, oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0068) + ":", oFont11N:oFont) //"Voucher"
								oPrint:Say( nLin    , 0600, AllTrim(G3Q->G3Q_DOC) + IIF( !lServProp, " � " + AllTrim(SA2->A2_NREDUZ), ""), oFont12:oFont)
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0081) + ":", oFont11N:oFont) //"Tipo Visto"
								oPrint:Say( nLin    , 0600, AllTrim( (Posicione("G4F",1,xFilial("G4F")+(cAliasAux)->G42_TPVIST, "G4F_DESCR")) )	, oFont12:oFont )								
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0080) + ":", oFont11N:oFont) //"Pa�s"
								oPrint:Say( nLin    , 0600, AllTrim( (Posicione("SYA",1,xFilial("SYA")+(cAliasAux)->G42_PAIS, "YA_DESCR")) )	, oFont12:oFont )
								 
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0064) + ":", oFont11N:oFont) //"Solicitante"
								oPrint:Say( nLin    , 0600, G3Q->G3Q_NOMESO			, oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0065) + ":", oFont11N:oFont) //"Emissor"
								oPrint:Say( nLin    , 0600, IIF( !lServProp, G3R->G3R_NOMREP, "") 		, oFont12:oFont )
	
							//------------------------------------------------------------------------------------------
							// INFORMA��ES DO OUTROS
							//------------------------------------------------------------------------------------------
							Case G3Q->G3Q_TPSEG = 'B'
								If nLin+375 > nPgBreak
									oPrint:EndPage()
									nLin := TURR13CAB(@oPrint,@nPagina)
								EndIf
			
								G43->(DbGoTo((cAliasAux)->R_E_C_N_O_))
								cDatas   := TR13DtInOut(G85->G85_PREFIX, G85->G85_NUMFAT, G85->G85_REGVEN, G85->G85_ITVEND, "%G43_DTINI%", "%G43_DTFIM%")
								nLinPula := (Round((Len(cDatas) / 99), 0) - 1) * 25
								oPrint:Box( nLin, 0060, nLin+375 + IIF(nLinPula > 0, nLinPula, 0), 2284,, )
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0057) + ":", oFont11N:oFont)  //"Produto"
								oPrint:Say( nLin	, 0600, SBM->BM_DESC			, oFont12:oFont )
								oPrint:Say( nLin+=40, 0344, OEMTOANSI(STR0061) + ":", oFont11N:oFont)  //"PAX"
								oPrint:Say( nLin    , 0600, AllTrim(G3S->G3S_NOME)	, oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0068) + ":", oFont11N:oFont)  //"Voucher"
								oPrint:Say( nLin    , 0600, AllTrim(G3Q->G3Q_DOC)  + IIF( !lServProp, " � " + AllTrim(SA2->A2_NREDUZ), ""), oFont12:oFont)
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0075) + ":", oFont11N:oFont)  //"Descri��o"
								oPrint:Say( nLin    , 0600, AllTrim(G3Q->G3Q_PROD) + " � " + AllTrim(SB1->B1_DESC)  , oFont12:oFont)
								
								aObsG43 := CabeMemo(G43->G43_DSSERV, 7060, "oFont12:oFont", oPrint)
								If Len(aObsG43) > 0
									nLin+=45
									For nG43l := 1 to Len(aObsG43)
										oPrint:Say( nLin, 0600, aObsG43[nG43l],oFont12:oFont,, )//Continua��o Descri��o
										nLin+=45
									Next
								Endif
			
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0063) + ":", oFont11N:oFont)  //"Data In-Out"
								oPrint:Say( nLin	, 0600, AllTrim(SubStr(cDatas, 1, 99)), oFont12:oFont)
								
								While lQuebraData
									If Len(cDatas) > 99
										nLin += 25
										cDatas := AllTrim(SubStr(cDatas, 100))
										oPrint:Say( nLin, 0600, AllTrim(SubStr(cDatas, 1, 99)), oFont12:oFont)
									Else
										lQuebraData := .F.	
									EndIf
								EndDo
								lQuebraData := .T.
			
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0064) + ":", oFont11N:oFont)  //"Solicitante"
								oPrint:Say( nLin    , 0600, G3Q->G3Q_NOMESO			, oFont12:oFont )
								oPrint:Say( nLin+=45, 0344, OEMTOANSI(STR0065) + ":", oFont11N:oFont)  //"Emissor"
								oPrint:Say( nLin    , 0600, IIF( !lServProp, G3R->G3R_NOMREP, "") 		, oFont12:oFont )
						EndCase
					EndIf
					(cAliasAux)->(DbCloseArea())
				EndIf
				
				nLin+=25
				
				cAliasEntA := GetNextAlias()
				BeginSQL Alias cAliasEntA

					SELECT		G4B.G4B_TITUL
								,G4B.G4B_ITEM
								,G4B.G4B_DESENT
					FROM		%Table:G4B% G4B
					INNER JOIN 	%Table:G3F% G3F
					ON 			G3F.G3F_CODCLI 	= G4B.G4B_CLIENT
					AND 		G3F.G3F_LOJA 	= G4B.G4B_LOJA
					AND			G3F.G3F_TIPO 	= G4B.G4B_TPENT
					AND			G3F.%NotDel%
					WHERE		G4B.G4B_FILIAL 	= %Exp:G3S->G3S_FILIAL%
					AND			G4B.G4B_NUMID 	= %Exp:G3S->G3S_NUMID%
					AND			G4B.G4B_IDITEM 	= %Exp:G3S->G3S_IDITEM%
					AND			G4B.G4B_NUMSEQ 	= %Exp:G3S->G3S_NUMSEQ%
					AND			G4B.G4B_CODPAX 	= %Exp:G3S->G3S_CODPAX%
					AND			G3F.G3F_IMPFAT 	= '1'
					AND			G4B.G4B_CONINU 	= %Exp:cConinu%
					AND			G4B.%NotDel%

				EndSQL

				(cAliasEntA)->(dbGoTop())

				If (cAliasEntA)->(!Eof())
					If G3Q->G3Q_TPSEG <> '1' .OR. (G3Q->G3Q_TPSEG = '1' .AND. lSegAereo)
						While (cAliasEntA)->(!Eof())
	
							If nLin+45 > nPgBreak
								oPrint:Line(nLin, 0060, nLin, 2286,, )
								oPrint:EndPage()
								nLin := TURR13CAB(@oPrint,@nPagina)
								nLin+=25
								oPrint:Line(nLin, 0060, nLin, 2286,, )
							EndIf
	
							oPrint:Line(nLin, 0060, nLin+45, 0060,, )
							oPrint:Line(nLin, 2286, nLin+45, 2286,, )
							nLin+=45
							
							//Para fonte Courrier 12. Cabe 86 caracter de Item e Descri��o da entidade
							oPrint:Say( nLin, 0080, Replicate(" ",nTamG4BTit-Len(AllTrim((cAliasEntA)->G4B_TITUL)))+AllTrim((cAliasEntA)->G4B_TITUL)+":",oFont12:oFont  )
							oPrint:Say( nLin, 0810, SubStr(AllTrim((cAliasEntA)->G4B_ITEM)+" - "+ AllTrim((cAliasEntA)->G4B_DESENT),1,86) 	,oFont12:oFont  )
							
							If Len(AllTrim((cAliasEntA)->G4B_ITEM)+" - "+ AllTrim((cAliasEntA)->G4B_DESENT)) > 86	
								oPrint:Line(nLin, 0060, nLin+20, 0060,, )
								oPrint:Line(nLin, 2286, nLin+20, 2286,, )
								nLin+=20
								oPrint:Say( nLin, 0810, SubStr(AllTrim((cAliasEntA)->G4B_ITEM)+" - "+ AllTrim((cAliasEntA)->G4B_DESENT),87) 	,oFont12:oFont  )
							EndIf
	
	
							(cAliasEntA)->(DbSkip())
						EndDo
						oPrint:Line(nLin, 0060, nLin+25, 0060,, )
						oPrint:Line(nLin, 2286, nLin+25, 2286,, )
						nLin+=25
						oPrint:Line(nLin, 0060, nLin, 2286,, )
					EndIf
				EndIf

				(cAliasEntA)->(dbCloseArea())

			EndIf
			G3S->(DbSkip())
		EndDo
	EndIf
ElseIf !Empty(aTot[8]) .And. !Empty(aTot[9]) //Se for fatura de apura��o imprime somente uma entidade adicional
	If nLin+70 > nPgBreak

		oPrint:EndPage()
		nLin := TURR13CAB(@oPrint,@nPagina)

	EndIf

	oPrint:Box( nLin, 0060, nLin+70, 2284 )
	nLin+=45

	oPrint:Say( nLin, 0324, AllTrim(Posicione("G3E",1,xFilial("G3E")+aTot[8],"G3E_DESCR"))+":" 	,oFont11N:oFont  )
	oPrint:Say( nLin, 1112, AllTrim(aTot[9])+" - "+ AllTrim(Posicione("G3G",1,xFilial("G3G")+G81->G81_CLIENT+G81->G81_LOJA+aTot[8]+aTot[9],"G3G_DESCR")) ,oFont12:oFont  )
	nLin+=25

EndIf

RestArea(aAreaG3S)
RestArea(aAreaSA2)
nLin+=25

Return(nLin)


Static Function R013Trecho(cDocOrigem)
Local cTrecho := ''
Local cAliasG3Q := GetNextAlias()

	BeginSQL Alias cAliasG3Q

		SELECT     G3Q.G3Q_DOC, G3Q.G3Q_DOCORI,G3T.G3T_TERORI,G3T.G3T_TERDST
		FROM         %Table:G3T% G3T INNER JOIN %Table:G3Q% G3Q ON 
			G3T.G3T_FILIAL = G3Q.G3Q_FILIAL 
			AND G3T.G3T_NUMID = G3Q.G3Q_NUMID 
			AND G3T.G3T_IDITEM = G3Q.G3Q_IDITEM 
			AND G3T.G3T_NUMSEQ = G3Q.G3Q_NUMSEQ
		WHERE
			G3Q.G3Q_FILIAL 	= %xFilial:G3Q% AND
			G3Q.%NotDel% AND
			G3Q.G3Q_DOC = %Exp:cDocOrigem%
	EndSQL
	 
	(cAliasG3Q)->(dbGoTop())

	If (cAliasG3Q)->(!Eof())
		cTrecho := (cAliasG3Q)->G3T_TERORI +" - "+(cAliasG3Q)->G3T_TERDST
	EndIf

	(cAliasG3Q)->(dbCloseArea())


Return cTrecho

//-------------------------------------------------------------------
/*/{Protheus.doc} TR013CalcSub() 
Fun��o para efetuar o c�lculo de subtra��o do repasse do total da 
fatura, levando em considera��o os valores negativos

@author Fernando Radu Muscalu
@since�14/10/2016
@version 12.1.7
/*/
//-------------------------------------------------------------------
Static Function TR013CalcSub(nTotalFat,nRepasse)

Local nTotalPos		:= 0
Local nRepassePos	:= 0
Local nTotal		:= 0
		
If ( nTotalFat < 0 )
	nTotalPos := ( nTotalFat * (-1) )
Else
	nTotalPos := nTotalFat	
EndIf 	

If ( nRepasse < 0 )
	nRepassePos := ( nRepasse * (-1) )
Else
	nRepassePos := nRepasse	 
EndIf

If ( nTotalPos > nRepassePos )
	
	If ( nTotalFat < 0 .And. nRepasse > 0 )
		nTotal := nTotalPos - nRepassePos
		nTotal *= -1
	ElseIf ( nTotalFat < 0 .And. nRepasse < 0 )
		nTotal := nTotalPos + nRepassePos	
	 	nTotal *= -1
	Else
	 	nTotal := nTotalPos - nRepassePos
	Endif
	 	
ElseIf ( nRepassePos > nTotalPos )

	If ( nRepasse < 0 .And. nTotalFat > 0 )
		nTotal := nRepassePos - nTotalPos
		nTotal *= -1
	ElseIf ( nRepasse < 0 .And. nTotalFat < 0 )
		nTotal := nRepassePos + nTotalPos
		nTotal *= -1
	Else	
		nTotal := nRepassePos + nTotalPos
	Endif
		 
EndIf

Return(nTotal)		


//-------------------------------------------------------------------
/*/{Protheus.doc} DescEnt() 
Fun��o para consulta de Entidade Adicional utilizada
@author Totvs
@since�17/02/2017
@version 12.1.6
/*/
//-------------------------------------------------------------------
Static Function DescEnt(cFilIdItem,cSeq,cCodEAD)
Local cAliasG4B	:= GetNextAlias()
Local cFilialG4B	:= SUBSTR(cFilIdItem,1,TAMSX3('G4B_FILIAL')[1])
Local cID 			:= SUBSTR(cFilIdItem,TAMSX3('G4B_FILIAL')[1]+1,TAMSX3('G4B_NUMID')[1])
Local cItem		:= SUBSTR(cFilIdItem,TAMSX3('G4B_FILIAL')[1]+TAMSX3('G4B_NUMID')[1]+1,TAMSX3('G4B_IDITEM')[1])
Local cRet			:= ''
Local cConinu   	:= Space(TamSx3("G4C_CONINU")[1])

BeginSQL Alias cAliasG4B

	SELECT
		G4B_ITEM
	FROM
		%Table:G4B% G4B
	WHERE	
			G4B.G4B_FILIAL = %Exp:cFilialG4B%
			AND G4B.G4B_NUMID = %Exp:cID%
			AND G4B.G4B_IDITEM = %Exp:cItem%
			AND G4B.G4B_NUMSEQ = %Exp:cSeq%
			AND G4B.G4B_TPENT = %Exp:cCodEAD%
			AND G4B.G4B_CONINU = %Exp:cConinu%
		AND	G4B.%NotDel%

EndSQL

(cAliasG4B)->(dbGoTop())

If (cAliasG4B)->(!Eof())
		cRet := (cAliasG4B)->G4B_ITEM
EndIf

(cAliasG4B)->(dbCloseArea())


Return cRet

Static Function CabeMemo(cOriObs, nTamPixels, cFont, oPrint) 

	Local aRetObs      := {}
	Local cOBS         := Upper(cOriObs)
	Local nTamObs      := Len(cOBS)
	Local nPosObs      := 0
	Local cLinObs      := ""
	Local cCR          := Chr(13)
	Local cLF          := Chr(10)
	Local nQtdArray    := Ceiling(nTamObs/95)
	Local nArray       := 1
	DEFAULT nTamPixels := 7060
	DEFAULT cFont      := "oFont12"

	For nArray := 1 to nQtdArray

		For nPosObs := 1 To nTamObs

			If Len(aRetObs) == 5		
				Exit
			EndIf

			cAtuCar := SubStr(cOBS, nPosObs, 1)
	
			If cAtuCar == cCR .or. cAtuCar == cLF
				If SubStr(cOBS, nPosObs + 1, 1) == cLF
					nPosObs += 1
					cAtuCar := SubStr(cOBS, nPosObs, 1)
				EndIf

				If !Empty(AllTrim(cLinObs))			
					AAdd(aRetObs, StrTran(StrTran(AllTrim(cLinObs), cCR, "" ), cLF,"" ))				
					cLinObs := ""				
				EndIf
			EndIf

			If cAtuCar <> cCR .and. cAtuCar <> cLF .and. cAtuCar <> cCR + cLF
				cLinObs += cAtuCar
			EndIf

			If oPrint:GetTextWidth(AllTrim(cLinObs), &(cFont) ) > nTamPixels
				If !Empty(AllTrim(cLinObs))
					AAdd(aRetObs, SubStr(AllTrim(cLinObs), 1, Len(AllTrim(cLinObs))-1))
					If nPosObs > 0
						nPosObs -= 1
					EndIf
					cLinObs := ""
				EndIf
			EndIf

		Next

		If cLinObs <> "" 
			AAdd(aRetObs, SubStr(AllTrim(cLinObs), 1, Len(AllTrim(cLinObs))))
			Exit
		Endif 

	Next
Return(aRetObs)

//-------------------------------------------------------------------
/*/{Protheus.doc} TR13DtInOut() 
Fun��o para retornar as datas de IN/OUT dos IVs faturados
@author Totvs
@since�17/02/2017
@version 12.1.6
/*/
//-------------------------------------------------------------------
Static Function TR13DtInOut(cPrefix, cNumFat, cNumId, cIdItem, cDataIni, cDataFim)

Local aArea   := GetArea()
Local cAlias  := GetNextAlias()
Local cDatas  := ""
Local cConinu := Space(TamSx3("G4C_CONINU")[1])

BeginSQL Alias cAlias
	COLUMN INICIAL AS DATE
	COLUMN FINAL   AS DATE

	SELECT DISTINCT %Exp:cDataIni% INICIAL, %Exp:cDataFim% FINAL
	FROM %Table:G85% G85
	INNER JOIN %Table:G3Q% G3Q ON G3Q_FILIAL = G85_FILREF AND
								  G3Q_NUMID  = G85_REGVEN AND
								  G3Q_IDITEM = G85_ITVEND AND
								  G3Q_NUMSEQ = G85_SEQIV  AND
								  G3Q.%NotDel%
	LEFT JOIN %Table:G3T% G3T ON  G3T_FILIAL = G3Q_FILIAL AND
							      G3T_NUMID  = G3Q_NUMID  AND
							      G3T_IDITEM = G3Q_IDITEM AND
							      G3T_NUMSEQ = G3Q_NUMSEQ AND
								  G3T_CONINU = G3Q_CONINU AND
							      G3T.%NotDel%
	LEFT JOIN %Table:G3U% G3U ON  G3U_FILIAL = G3Q_FILIAL AND
							      G3U_NUMID  = G3Q_NUMID  AND
							      G3U_IDITEM = G3Q_IDITEM AND
							      G3U_NUMSEQ = G3Q_NUMSEQ AND
								  G3U_CONINU = G3Q_CONINU AND
							      G3U.%NotDel%
	LEFT JOIN %Table:G3V% G3V ON  G3V_FILIAL = G3Q_FILIAL AND
							      G3V_NUMID  = G3Q_NUMID  AND
							      G3V_IDITEM = G3Q_IDITEM AND
							      G3V_NUMSEQ = G3Q_NUMSEQ AND
								  G3V_CONINU = G3Q_CONINU AND
							      G3V.%NotDel%
	LEFT JOIN %Table:G3W% G3W ON  G3W_FILIAL = G3Q_FILIAL AND
							      G3W_NUMID  = G3Q_NUMID  AND
							      G3W_IDITEM = G3Q_IDITEM AND
							      G3W_NUMSEQ = G3Q_NUMSEQ AND
								  G3W_CONINU = G3Q_CONINU AND
							      G3W.%NotDel%
	LEFT JOIN %Table:G3X% G3X ON  G3X_FILIAL = G3Q_FILIAL AND
							      G3X_NUMID  = G3Q_NUMID  AND
							      G3X_IDITEM = G3Q_IDITEM AND
							      G3X_NUMSEQ = G3Q_NUMSEQ AND
								  G3X_CONINU = G3Q_CONINU AND
							      G3X.%NotDel%
	LEFT JOIN %Table:G3Y% G3Y ON  G3Y_FILIAL = G3Q_FILIAL AND
							      G3Y_NUMID  = G3Q_NUMID  AND
							      G3Y_IDITEM = G3Q_IDITEM AND
							      G3Y_NUMSEQ = G3Q_NUMSEQ AND
								  G3Y_CONINU = G3Q_CONINU AND
							      G3Y.%NotDel%
	LEFT JOIN %Table:G3Z% G3Z ON  G3Z_FILIAL = G3Q_FILIAL AND
							      G3Z_NUMID  = G3Q_NUMID  AND
							      G3Z_IDITEM = G3Q_IDITEM AND
							      G3Z_NUMSEQ = G3Q_NUMSEQ AND
								  G3Z_CONINU = G3Q_CONINU AND
							      G3Z.%NotDel%
	LEFT JOIN %Table:G40% G40 ON  G40_FILIAL = G3Q_FILIAL AND
							      G40_NUMID  = G3Q_NUMID  AND
							      G40_IDITEM = G3Q_IDITEM AND
							      G40_NUMSEQ = G3Q_NUMSEQ AND
								  G40_CONINU = G3Q_CONINU AND
							      G40.%NotDel%
	LEFT JOIN %Table:G41% G41 ON  G41_FILIAL = G3Q_FILIAL AND
							      G41_NUMID  = G3Q_NUMID  AND
							      G41_IDITEM = G3Q_IDITEM AND
							      G41_NUMSEQ = G3Q_NUMSEQ AND
								  G41_CONINU = G3Q_CONINU AND
							      G41.%NotDel%
	LEFT JOIN %Table:G43% G43 ON  G43_FILIAL = G3Q_FILIAL AND
							      G43_NUMID  = G3Q_NUMID  AND
							      G43_IDITEM = G3Q_IDITEM AND
							      G43_NUMSEQ = G3Q_NUMSEQ AND
								  G43_CONINU = G3Q_CONINU AND
							      G43.%NotDel%
	WHERE G85_FILIAL = %xFilial:G85% AND
	      G85_PREFIX = %Exp:cPrefix% AND
	      G85_NUMFAT = %Exp:cNumFat% AND
	      G85_REGVEN = %Exp:cNumId%  AND
	      G85_ITVEND = %Exp:cIdItem% AND
		  G3Q_CONINU = %Exp:cConinu% AND
	      G3Q_STATUS < '3' AND 
	      G3Q_ACERTO = '2' AND
	      G85.%NotDel% 
							 
EndSQL

While (cAlias)->(!EOF())
	cDatas += DtoC((cAlias)->INICIAL) + " - " + DtoC((cAlias)->FINAL) 
	(cAlias)->(DbSkip())
	If (cAlias)->(!EOF())
		cDatas += ", "
	EndIf
EndDo
(cAlias)->(DbCloseArea())

RestArea(aArea)

Return cDatas

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TR13DdsSeg

Fun��o que retorna Alias com dados do segmento a ser impresso

@sample 	TR13DdsSeg()
@param		cTpSeg  - String - c�digo do segmento
			cFilRv  - String - filial do RV 
			cNumID  - String - c�digo do RV
			cIdItem - String - c�digo do IV
			cNumSeq - String - c�digo sequencia do IV
@author 	Thiago Tavares
@since 		15/12/2017
@version 	12.1.17
/*/
//+----------------------------------------------------------------------------------------
Static Function TR13DdsSeg(cTpSeg, cFilRV, cNumID, cIdItem, cNumSeq)

Local cAliasQry := GetNextAlias() 
Local cQuery    := ''
Local aTabSeg   := {{'1', 'G3T'}, {'2', 'G3U'}, {'3', 'G3V'}, {'4', 'G3W'}, {'5', 'G3Y'}, {'6', 'G3X'}, {'7', 'G42'}, {'8', 'G41'}, {'9', 'G40'}, {'A', 'G3Z'}, {'B', 'G43'}, {'R', 'G4E'}}
Local nPos      := 0
Local cConinu   := Space(TamSx3("G4C_CONINU")[1])

If (nPos := aScan(aTabSeg, {|x| x[1] == cTpSeg}))
	cQuery := I18N("SELECT * FROM " + RetSQLName(aTabSeg[nPos][2]) + " WHERE #1_FILIAL = '" + cFilRV + "' AND #1_NUMID = '" + cNumID + "' AND #1_IDITEM = '" + cIdItem + "' AND #1_NUMSEQ = '" + cNumSeq + "' AND #1_CONINU = '" + cConinu + "' AND D_E_L_E_T_ <> '*'", {aTabSeg[nPos][2]})
	cQuery := ChangeQuery(cQuery)
	DBUseArea(.T., "TOPCONN", TCGenQry(, , cQuery), cAliasQry, .F., .T.)
	DbSelectArea(cAliasQry)
EndIf

Return cAliasQry



/*/{Protheus.doc} ItensArer
(long_description)
@type function
@author osmar.junior
@since 31/01/2018
@version 1.0
@param cFilialAux, character, (Descri��o do par�metro)
@param cREGVEN, character, (Descri��o do par�metro)
@param cITVEND, character, (Descri��o do par�metro)
@param cSEQIV, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function ItensArer( cAliasAer, cFilialAux, cREGVEN, cITVEND, cSEQIV,cPax, cDatas, cTrechos)
Local lRet := .F.
Local cDataFim := ''

	cDatas := ''
	cTrechos := ''
	
	BeginSql Alias cAliasAer
		column G3T_DTSAID as Date
		column G3T_DTCHEG as Date
		
		SELECT G3T_DTSAID,G3T_DTCHEG,G3T_TERORI,G3T_TERDST
		 FROM %Table:G3T%
		WHERE 
		G3T_FILIAL = %Exp:cFilialAux% AND
		G3T_NUMID  = %Exp:cREGVEN% AND
		G3T_IDITEM = %Exp:cITVEND% AND
		G3T_NUMSEQ = %Exp:cSEQIV% AND
		G3T_CODPAX = %Exp:cPax% AND
		G3T_CONINU = '' AND
		%notDel%
		ORDER BY G3T_FILIAL,G3T_NUMID,G3T_IDITEM,G3T_NUMSEQ,G3T_ID
	EndSql
		
	If (cAliasAer)->( !EOF() )
		lRet 	:= .T.
		cDatas 	:= 	DtoC((cAliasAer)->G3T_DTSAID)
		While (cAliasAer)->(!Eof())	
			cTrechos += AllTrim((cAliasAer)->G3T_TERORI) + "/" + AllTrim((cAliasAer)->G3T_TERDST)+ ' | '
			cDataFim := DtoC((cAliasAer)->G3T_DTCHEG)
			(cAliasAer)->(DbSkip()) 
		EndDo
		cDatas 		+= " - " + cDataFim	
        cTrechos 	:= SUBSTR(cTrechos, 1, RAT("|", cTrechos)-1)
					
	EndIf
	
	(cAliasAer)->( dbCloseArea() )	


Return lRet