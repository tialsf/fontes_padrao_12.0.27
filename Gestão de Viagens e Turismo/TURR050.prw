#Include 'Protheus.ch'
#include 'FILEIO.CH'
#INCLUDE 'TOTVS.CH'  
#INCLUDE 'TOPCONN.CH'
#INCLUDE 'REPORT.CH'
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TURR050.CH"

#Define SEC_FILIAL		1
#Define SEC_FORNECEDOR	2
#Define SEC_APURACAO	3
#Define SEC_SEGMENTO	4
#Define SEC_ACORDO		5
#Define SEC_ITENS_ACORD	6
#Define SEC_RESUMO		7

Static aTitle 	:= {}
Static nIdioma	:= 1	

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TURR050
Fun��o de usu�rio para a gera��o do relat�rio de demonstrativos de cobran�as. Este relat�rio
possui como objetivo apresentar as informa��es das apura��es de receitas dos fornecedores. 
Al�m disso, tamb�m envia por e-mail dos respectivos fornecedores, estes demonstrativos.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TURR050(cApuraDe, cApuraAte, cFornDe, cLjForDe, cFornAte, cLjForAte, lAglutina, lEnvMail, cEmailRem, cDirFile, lCompFor, lExcel, cFilApu)

Local lContinua		:= .f.
Local cApurAbert	:= '1'
Local cWarningMsg	:= STR0001//"N�o foi poss�vel extrair dados para apresentar o relat�rio. Verifique a parametriza��o e certifique-se que n�o equ�vocos."
Local bProcessa		:= {|x|	x := TR50QryRpt(cApuraDe,cApuraAte,cFornDe,cLjForDe,cFornAte,cLjForAte,lCompFor,cApurAbert,cFilApu),;
							(x)->(dbGotop()),; 
							Iif((x)->(!eof()),TR50ProcRpt(x,lAglutina,lEnvMail,cEmailRem,cDirFile,lCompFor,lExcel),FwAlertWarning(cWarningMsg,STR0002)),;//"Aten��o"
							(x)->(DbCloseArea()) }

Default cApuraDe  := ''
Default cApuraAte := '' 
Default cFornDe   := '' 
Default cLjForDe  := ''
Default cFornAte  := ''
Default cLjForAte := ''
Default lAglutina := .F.
Default lEnvMail  := .F.
Default cEmailRem := ''
Default cDirFile  := GetMv('MV_DIRAPU', , 'C:\TOTVS\')
Default lCompFor  := .T.
Default lExcel	  := .F.
Default cFilApu   := cFilAnt 

//--  Carrega array com os labels do relatorio
TURSetTitle(@aTitle)

If ( Empty(cApuraDe) .and. Empty(cApuraAte) )	
	
	lContinua := Pergunte(Padr("TUR50D",10)) 
	
	cApuraDe 	:= mv_par01
	cApuraAte	:= mv_par02
	cFornDe		:= mv_par03
	cLjForDe	:= mv_par04
	cFornAte	:= mv_par05
	cLjForAte	:= mv_par06
	lAglutina	:= mv_par07 == 1
	lEnvMail	:= mv_par08 == 1
	cEmailRem	:= Lower(Alltrim(mv_par09))
	cDirFile 	:= Alltrim(mv_par10)
	lCompFor	:= mv_par11 == 1
	cApurAbert := AllTrim( AllToChar(mv_par12) )
		
	If ( !lEnvMail )
		If ( Empty(cDirFile) )
			lContinua := .F.
			FwAlertError(STR0004,STR0003)	//"Erro na parametriza��o"//"Se a op��o de enviar e-mail for n�o, � obrigat�rio o preenchimento do diret�rio do arquivo."			
		Endif
	Endif	 
	
Else
	lContinua := !Empty(cApuraAte) .and. !Empty(cLjForAte) .and. !Empty(cFornAte)
Endif

If ( lContinua )
	Processa(bProcessa, STR0005, STR0006)//"Aguarde"//"Gerando apura��o..."
Endif	

Return()

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TR50QryRpt
Fun��o respons�vel por fazer a consulta na base de dados para compor as informa��es que
ser�o apresentadas no relat�rio, conforme filtro indicado por parametriza��o de usu�rios.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
		
Static Function TR50QryRpt(cApuraDe, cApuraAte,	cFornDe, cLjForDe, cFornAte, cLjForAte,lCompFor,cApurAbert,cFilApu)

Local cAliasG6L	:= GetNextAlias()
Local cComplFor	:= Iif(lCompFor,".T.",".F.")
Local cStatus	:= "% " 	
Local cFilG6L   := xFilial("G6L", cFilApu)
Local cFilG5T   := xFilial("G5T", cFilApu)
Local cFilG8B   := xFilial("G8B", cFilApu)

	If cApurAbert $ "1|2"
		cStatus += " AND G6L_STATUS = '"+ cApurAbert + "'" 
	EndIf

	cStatus += " %"

BeginSQL Alias cAliasG6L
	
	Column G6L_DTGERA as Date
	
	SELECT
		G6L_FILIAL,									// SE��O 1: DADOS DA FILIAL
		'TR50GetEmpFil("' + %Exp:cFilApu% + '","M0_NOME")' M0_NOME,
		'TR50GetEmpFil("' + %Exp:cFilApu% + '","M0_ENDCOB")' M0_ENDCOB,
		'TR50GetEmpFil("' + %Exp:cFilApu% + '","M0_CEPCOB")' M0_CEPCOB,
		'TR50GetEmpFil("' + %Exp:cFilApu% + '","M0_CIDCOB")' M0_CIDCOB,
		'TR50GetEmpFil("' + %Exp:cFilApu% + '","M0_TEL")' M0_TEL,
		G6L_FORNEC,									//SE��O 2: DADOS DO FORNECEDOR
		G6L_LJFORN,
		A2_NREDUZ,									
		'TR50GetCplForn("'+G6L_FORNEC+'","'+G6L_LJFORN+'","G4V_ENDER",'+ %Exp:cComplFor% + ')' 	G4V_ENDER,
		'TR50GetCplForn("'+G6L_FORNEC+'","'+G6L_LJFORN+'","G4V_CEP",'+ %Exp:cComplFor% + ')' 	G4V_CEP,
		'TR50GetCplForn("'+G6L_FORNEC+'","'+G6L_LJFORN+'","G4V_BAIRRO",'+ %Exp:cComplFor% + ')'	G4V_BAIRRO,
		'TR50GetCplForn("'+G6L_FORNEC+'","'+G6L_LJFORN+'","G4V_UF",'+ %Exp:cComplFor% + ')' 	G4V_UF,
		'TR50GetCplForn("'+G6L_FORNEC+'","'+G6L_LJFORN+'","G4V_NOMMUN",'+ %Exp:cComplFor% + ')' G4V_NOMMUN,
		A2_TEL,
		G6L_CODAPU,									//SE��O 3: ACORDOS
		G6L_DTGERA,
		G6L_STATUS,
		(
			CASE
				WHEN G6L_STATUS = '1' THEN 'Em aberto'
				WHEN G6L_STATUS = '2' THEN 'Liberado'
			END
		) DS_STATG6L,
		G6L_TOTACD,
		G6L_MOEDA,
		(
			SELECT
				G5T_DESCR
			FROM
				%Table:G5T% G5T
			WHERE
				G5T_FILIAL = %Exp:cFilG5T%
				AND G5T_CODIGO = G6L_MOEDA
				AND G5T.%NotDel%
		) DSC_MOEDA,
		G6M_SEGNEG,
		(
			CASE
				WHEN G6M_SEGNEG = '1' THEN 'Corporativo'
				WHEN G6M_SEGNEG = '2' THEN 'Eventos'
				WHEN G6M_SEGNEG = '3' THEN 'Lazer'
			END
		) DSC_SEGNEG,
		G6M_TIPOAC,									//SE��O 3.1: SEGMENTO DE NEG�CIO + MODALIDADE DE ACORDO
		(
			SELECT
				G8B_DESCRI
			FROM
				%Table:G8B% G8B
			WHERE
				G8B_FILIAL = %Exp:cFilG8B%
				AND G8B_CLIFOR = '2'
				AND G8B_CODIGO = G6M_TIPOAC
				AND G8B.%NotDel%
		) DSC_TIPOAC,
		(
			SELECT 
				SUM(G6M_VLACD) 
			FROM 
				%Table:G6M% G6M2 
			WHERE
				G6M_FILIAL = G6L_FILIAL
				AND G6M_CODAPU = G6L_CODAPU
				AND G6M2.%NotDel%
		) G6M_VLACD,
		G6M_PERDES,
		G6M_VLDESC,
		G6M_PERCTX,
		G6M_VLTXAD,
		(
			SELECT 
				SUM(G6M_TOTAL) 
			FROM 
				%Table:G6M% G6M3 
			WHERE
				G6M_FILIAL = G6L_FILIAL
				AND G6M_CODAPU = G6L_CODAPU
				AND G6M3.%NotDel%
		) G6M_TOTAL,
		G6O_CODACD,									//SE��O 3.2: ACORDOS
		G6O_CODREV,
		G6O_BASAPU,
		G6O_VLBASE,
		G6O_TIPOVL,
		(
			CASE
				WHEN G6O_TIPOVL = '1' THEN 'Porcentagem'
				WHEN G6O_TIPOVL= '2' THEN 'Valor Fixo'
			END
		) DSC_TIPOVL,
		G6O_PERCEN,
		G6O_VALOR,
		G6O_VAPLIC,
		G6O_STATUS,
		(
			CASE
				WHEN G6O_STATUS = '1' THEN 'Ativo'
				WHEN G6O_STATUS = '2' THEN 'Cancelado'
				WHEN G6O_STATUS = '3' THEN 'Fora de vigencia'
			END
		) DS_STATG6O,
		G4C_NUMID,									//SE��O 3.3: ITENS DO ACORDO
		G4C_IDITEM,
		G4C_NUMSEQ,
		G4C_PAGREC,
		(
			CASE
				WHEN G4C_PAGREC = '1' THEN 'A pagar'
				WHEN G4C_PAGREC = '2' THEN 'A receber'
				WHEN G4C_PAGREC = '3' THEN 'Demonstrativo'
			END
		) DSC_PAGREC,
		G4C_TIPO,
		(
			CASE
				WHEN G4C_TIPO = '1' THEN 'Venda'
				WHEN G4C_TIPO = '2' THEN 'Reembolso'
				WHEN G4C_TIPO = '3' THEN 'Receita'
				WHEN G4C_TIPO = '4' THEN 'Abat. Receita'
				WHEN G4C_TIPO = '5' THEN 'Breakage'
			END
		) DS_TIPOG4C,
		G4C_VALOR,
		G4C_DESTIN,
		(
			CASE
				WHEN G4C_DESTIN = '1' THEN 'Nacional'
				WHEN G4C_DESTIN = '2' THEN 'Internacional'
			END
		) DSC_DESTIN,
		G4C_STATUS,
		(
			CASE
				WHEN G4C_STATUS = '1' THEN 'Ag. Liberacao'
				WHEN G4C_STATUS = '2' THEN 'Ag. Apuracao'
				WHEN G4C_STATUS = '3' THEN 'Liberado'
				WHEN G4C_STATUS = '4' THEN 'Finalizado'			
			END
		) DS_STATG4C
	FROM
		%Table:G6L% G6L
	INNER JOIN
		%Table:SA2% SA2
	ON
		A2_FILIAL = %XFilial:SA2%
		AND A2_COD = G6L_FORNEC
		AND A2_LOJA = G6L_LJFORN
		AND SA2.%NotDel%
	INNER JOIN
		%Table:G6M% G6M
	ON
		G6L_FILIAL = G6M_FILIAL
		AND G6L_CODAPU = G6M_CODAPU
		AND G6M.%NotDel%
	INNER JOIN
		%Table:G6O% G6O
	ON
		G6M_FILIAL = G6O_FILIAL
		AND G6M_FILREF = G6O_FILREF
		AND G6M_CODAPU = G6O_CODAPU
		AND G6M_TIPOAC = G6O_TIPOAC
		AND G6O.%NotDel%
	INNER JOIN
		%Table:G4C% G4C
	ON
		G4C_FILIAL = G6O_FILREF
		AND G6L_CODAPU = G4C_CODAPU
		AND G6L_FORNEC = G4C_CODIGO
		AND G6L_LJFORN = G4C_LOJA
		AND G6L_GRPPRO = G4C_GRPPRD
		AND G6L_MOEDA = G4C_MOEDA
		AND G6M_SEGNEG = G4C_SEGNEG
		AND G6O_CODACD = G4C_NUMACD
		AND G6O_TIPOAC = G4C_CLASS
		AND G4C_CLIFOR = '2'										//Cliente ou fornecedor (1 ou 2, respectivamente)
		AND G4C_TIPO = '3'											//Tipo de Item Financeiro (1=Venda;2=Reembolso;3=Receita;4=Abat. Receita;5=Breakage), fixado em '3'
		AND G4C_APLICA <> '' 
		AND G4C.%NotDel%
	INNER JOIN
		%Table:G48% G48
	ON
		G4C_FILIAL = G48_FILIAL
		AND G4C_NUMID = G48_NUMID
		AND G4C_IDITEM = G48_IDITEM
		AND G4C_APLICA = G48_APLICA
		AND G4C_CLIFOR = G48_CLIFOR
		AND G48_CODAPU = G6L_CODAPU
		AND G48_CODREC = G6O_CODREV
		AND G48_MARK = 'T'										//Filtra somente os itens que foram marcados
		AND G48.%NotDel%			
	WHERE
		G6L_FILIAL = %Exp:cFilG6L%
		AND G6L_CODAPU BETWEEN %Exp:cApuraDe% AND %Exp:cApuraAte%	// Parametros mv_par01 e mv_par02, respectivamente
		AND G6L_FORNEC BETWEEN %Exp:cFornDe% AND %Exp:cFornAte% 	// Parametros mv_par03 e mv_par05, respectivamente
		AND G6L_LJFORN BETWEEN %Exp:cLjForDe%  AND %Exp:cLjForAte%	// Parametros mv_par04 e mv_par06, respectivamente
		AND G6L_TPAPUR = '3'										// Tipo de apura��o (1-Clientes,2-Metas,3-Fornecedor) - fixado em '3'
		%Exp:cStatus%
		AND G6L.%NotDel%
	ORDER BY
		G6L_FORNEC,
		G6L_LJFORN,
		G6L_CODAPU,
		G6M_SEGNEG,
		G6M_TIPOAC,
		G6O_CODACD,
		G6O_CODREV,
		G4C_NUMID, 
		G4C_IDITEM,
		G4C_NUMSEQ

EndSQL

Return(cAliasG6L)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TR50ProcRpt
Esta fun��o ir� varrer o ResultSet da query que captou os dados do relat�rio, para efetuar
a impress�o de acordo com um layout definido.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------

Static Function TR50ProcRpt(cAliasG6L, lAglutina, lEnvMail, cEmailRem, cDirFile, lCompFor, lExcel)

Local oMailServer   := Nil
Local cFornece		:= ""
Local cLjForne		:= ""
Local cApuracao 	:= ""
Local cDirApu   	:= "\data\"	//GetSrvProfString("RootPath", "")//Alltrim(cDirFile)
Local cEmail		:= ""		//"fernando.radu@gmail.com"
Local cArqBody		:= GetSrvProfString("StartPath", "") + "\Turismo\TR050cBody.html"
Local cBody			:= ""
Local cDemonstrativo:= ""
Local cEnviado		:= ""
Local nHandle	    := 0
Local nLength	    := 0
Local lNewRpt	    := .T.
Local lSendMail	    := lEnvMail
Local aAnexos	    := {}
Local aRetorno      := {}
Local bPrint	    := {|oRpt| ReportPrint(cAliasG6L,oRpt,@lNewRpt,lAglutina,@cEmail,lCompFor)}	
Local lFornExt	    := Posicione('SA2',1,xFilial('SA2') + (cAliasG6L)->G6L_FORNEC +(cAliasG6L)->G6L_LJFORN,"A2_EST") == "EX"

Default cDirFile := GetMv("MV_DIRAPUR",,"c:\totvs")	

oMailServer := TMailManager():New()
aRetorno    := TXSmtpConn(@oMailServer)

//-- 2 = Ingl�s, 1 = Portugues
nIdioma := IIF(lFornExt,2,1)

While ( (cAliasG6L)->(!Eof()) )
	
	cDemonstrativo := ""
	
	If ( lNewRpt )
		
		lSendMail		:= lEnvMail
		cApuracao 		:= (cAliasG6L)->G6L_CODAPU
		cDemonstrativo 	:= "1" //Demonstrativo de Cobran�a impresso		
		cEmail 			:= ""
		lUpdApura		:= (cAliasG6L)->G6L_STATUS == "1"
		
		oReport := TReport():New('TURR050', STR0007, , bPrint, STR0008, .T. /*lLandscape*/, /*uTotalText*/, /*lTotalInLine*/, /*cPageTText*/, /*lPageTInLine*/, /*lTPageBreak*/, /*nColSpace*/)//"Demonstrativo de Cobran�a - Fornecedor"//"Gera Relat�rio das Apura��es de Receita de Fornecedor"
		SetSections(oReport)
		
		If ( lSendMail ) .And. aRetorno[1]
			
			//o envio de e-mail definido no complemento do fornecedor � priorit�rio sobre o que fora parametrizado pelo usu�rio como envia
			lSendMail	:= Alltrim(TR50GetCplForn((cAliasG6L)->G6L_FORNEC,(cAliasG6L)->G6L_LJFORN,"G4V_ENVIO",lCompFor))
			
			If ( ValType(lSendMail) == "C" .and. Empty(lSendMail) )
				lSendMail := lEnvMail
			Else
				lSendMail := lSendMail == "1"	
			Endif
				
			oReport:SetPreview(.F.)
			oReport:SetDevice(4)
			oReport:cFile := Alltrim(cDirApu) + STR0009 + AllTrim((cAliasG6L)->(G6L_FORNEC+G6L_LJFORN)+dtos(dDataBase))+ StrTran(Time(),":","") + ".xls"//"Dem_Cobranca_"
			//cDirApu := Alltrim(oReport:cFile)
			
			aAnexos := {oReport:cFile}
			
			oReport:Print(.F.)
			
			If ( !Empty(cEmail) )
				
				If ( File(cArqBody) )
				
					nHandle := FOpen(cArqBody, FO_READWRITE)
					nLength := FSeek(nHandle, 0, 2)
					
					FSeek(nHandle, 0)
					FRead(nHandle, @cBody, nLength)
					
				EndIf
			
				cBody := StrTran(DecodeUTF8(cBody), '!Filial!', AllTrim(FWFilialName()))
				
				If ( lSendMail .And. Len(aAnexos) > 0 )
					
					TURXEnvio(cEmailRem, cEmail, "", cEmailRem, STR0077, cBody, aAnexos, oMailServer) // "Demonstrativo de Cobran�a"
					
					cEnviado :=	"1" // 1=Enviado
					
				Else
					cEnviado :=	"2" //2=Nao enviado (nao aceita envio)	
				Endif
			
			Else
				cEnviado := "3"		//3=Nao enviado (e-mail nao cadastrado) 
			Endif	
			
			CpyS2T(oReport:cFile,cDirFile)
							
		Else
			
			If ( !lExcel ) 
				oReport:PrintDialog()
			Else
				oReport:SetPreview(.F.)
				oReport:SetDevice(4)
				oReport:cFile := Alltrim(cDirApu) + STR0009 + AllTrim((cAliasG6L)->(G6L_FORNEC+G6L_LJFORN)+dtos(dDataBase))+ StrTran(Time(),":","") + ".xls"	//"Dem_Cobranca_"
				oReport:Print(.F.)
				
				MakeDir(cDirFile)
				CpyS2T(oReport:cFile,cDirFile)
			Endif
				
		Endif	
		
		//Atualiza os dados do cabe�alho do Demonstrativo - G6L
		If ( lUpdApura .And. !Empty(cDemonstrativo) )
			MsgRun(STR0013, STR0014, {|| TA50DemApura("TURA050",cApuracao,{cDemonstrativo,cEnviado,cEmailRem,cEmail})} )				//"Atualizando Apura��o..."//"Atualiza��o"
		Endif
				
	Endif	
	
EndDo

Return()

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} SetSections
Fun��o que define as se��es e c�luas do relat�rio.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------

Static Function SetSections(oReport)

Local oSecCab
Local oSecCabL1
Local oSecCabL2
Local oSecCabL3
Local oSecFor
Local oSecForL1
Local oSecForL2
Local oSecForL3
Local oSecApu
Local oSecSeg
Local oSecAco
Local oSecItm
Local oSecRes

Local aSecCab	:= TR50CellCollection(SEC_FILIAL)		//SE��O 1: DADOS DA FILIAL
Local aSecFor	:= TR50CellCollection(SEC_FORNECEDOR)	//SE��O 2: DADOS DO FORNECEDOR
Local aSecApu	:= TR50CellCollection(SEC_APURACAO)		//SE��O 3: APURA��O
Local aSecSeg	:= TR50CellCollection(SEC_SEGMENTO)		//SE��O 4: SEGMENTO DE NEG�CIO + MODALIDADE DE ACORDO
Local aSecAco	:= TR50CellCollection(SEC_ACORDO)		//SE��O 5: ACORDOS
Local aSecItm	:= TR50CellCollection(SEC_ITENS_ACORD)	//SE��O 6: ITENS DO ACORDO
Local aSecRes	:= TR50CellCollection(SEC_RESUMO)		//SE��O 7: RESUMO DA APURA��O

Local nI		:= 0
Local nX		:= 0

//Defini��o das Se��es do Relat�rio - instanciando os objetos - In�cio
//Ser�o Tr�s Se��es para a defini��o dos dados da Filial (Ag�ncia) 
oSecCabL1 := TRSection():New(oReport, "FILIAL_1", {'SM0','G6L'})	//SE��O 1.a
oSecCabL2 := TRSection():New(oReport, "FILIAL_2", {'SM0','G6L'})	//SE��O 1.b
oSecCabL3 := TRSection():New(oReport, "FILIAL_3", {'SM0','G6L'})	//SE��O 1.b

//Ser�o Tr�s Se��es para a defini��o dos dados do Fornecedor
oSecForL1 := TRSection():New(oSecCabL3, "FORNECE_1", {'SA2','G4V'})	//SE��O 2.a
oSecForL2 := TRSection():New(oSecCabL3, "FORNECE_2", {'SA2','G4V'})	//SE��O 2.b
oSecForL3 := TRSection():New(oSecCabL3, "FORNECE_3", {'SA2','G4V'})	//SE��O 2.c

oSecApu := TRSection():New(oSecForL3, STR0015, 'G6L')				//SE��O 3//"Apura��o"
oSecApu:SetLeftMargin(3) 
 
oSecSeg := TRSection():New(oSecApu, STR0016, 'G6M')	//SE��O 4//"Segmento de Neg�cio"
oSecSeg:SetLeftMargin(6)

oSecAco := TRSection():New(oSecSeg, STR0017, 'G6O')				//SE��O 5//"Acordo"
oSecAco:SetLeftMargin(9)

oSecItm := TRSection():New(oSecAco, STR0018, 'G4C')		//SE��O 6	//"Items do Acordo"
oSecItm:SetLeftMargin(12)

oSecRes := TRSection():New(oReport, STR0019, 'G6M')	//SE��O 7//"Resumo da Apura��o"

//Defini��o das Se��es do Relat�rio - instanciando os objetos - Fim

//Defini��o das C�lulas das se��es - in�cio
//C�lulas da Se��o 1 - In�cio
For nI := 1 to Len(aSecCab)
	
	If (nI == 1)
		oSecCab := oSecCabL1
	ElseIf (nI == 2)
		oSecCab := oSecCabL2
	Else
		oSecCab := oSecCabL3
	Endif		
	
	For nX := 1 To len(aSecCab[nI])
		TRCell():New(oSecCab, aSecCab[nI][nX,1], aSecCab[nI][nX,2], aSecCab[nI][nX,3], aSecCab[nI][nX,4],; 
						aSecCab[nI][nX,5],,,aSecCab[nI][nX,6])
	Next nX
	
Next nI	
//C�lulas da Se��o 1 - Fim

//C�lulas da Se��o 2 - In�cio
For nI := 1 to Len(aSecFor)
	
	If (nI == 1)
		oSecFor := oSecForL1
	ElseIf (nI == 2)
		oSecFor := oSecForL2
	Else
		oSecFor := oSecForL3
	Endif
	
	For nX := 1 To Len(aSecFor[nI])
		TRCell():New(oSecFor, aSecFor[nI][nX,1], aSecFor[nI][nX,2], aSecFor[nI][nX,3], aSecFor[nI][nX,4],; 
						aSecFor[nI][nX,5],,,aSecFor[nI][nX,6])
	Next nX
		
Next nI
//C�lulas da Se��o 2 - Fim

//C�lulas da Se��o 3 - In�cio
For nI := 1 to Len(aSecApu)
	TRCell():New(oSecApu, aSecApu[nI,1], aSecApu[nI,2], aSecApu[nI,3], aSecApu[nI,4],;
					aSecApu[nI,5],,,aSecApu[nI,6])
Next nI
//C�lulas da Se��o 3 - Fim

//C�lulas da Se��o 4 - In�cio
For nI := 1 to Len(aSecSeg)
	TRCell():New(oSecSeg, aSecSeg[nI,1], aSecSeg[nI,2], aSecSeg[nI,3], aSecSeg[nI,4],;
					aSecSeg[nI,5],,,aSecSeg[nI,6])
Next nI
//C�lulas da Se��o 4 - Fim

//C�lulas da Se��o 5 - In�cio
For nI := 1 to Len(aSecAco)
	TRCell():New(oSecAco, aSecAco[nI,1], aSecAco[nI,2], aSecAco[nI,3], aSecAco[nI,4],;
					aSecAco[nI,5],,,aSecAco[nI,6])
Next nI
//C�lulas da Se��o 5 - Fim

//C�lulas da Se��o 6 - In�cio
For nI := 1 to Len(aSecItm)
	TRCell():New(oSecItm, aSecItm[nI,1], aSecItm[nI,2], aSecItm[nI,3], aSecItm[nI,4],;
					aSecItm[nI,5],,,aSecItm[nI,6])	
Next nI
//C�lulas da Se��o 6 - Fim

//C�lulas da Se��o 7 - In�cio
For nI := 1 to Len(aSecRes)
	TRCell():New(oSecRes, aSecRes[nI,1], aSecRes[nI,2], aSecRes[nI,3], aSecRes[nI,4],;
					aSecRes[nI,5],,,aSecRes[nI,6])	
	
Next nI
//C�lulas da Se��o 7 - Fim
//Defini��o das C�lulas das se��es - fim

Return()

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TR50GetEmpFil
Fun��o que retorna informa��o (cCampo) da filial de acordo com o par�metro de Filial, 
cFilSeek passado.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TR50GetEmpFil(cFilSeek, cCampo)

Local xRet     := ""
Local aAreaSM0 := {}

If !Empty(cFilSeek)
	If ( Alltrim(cFilSeek) <> Alltrim(SM0->M0_CODFIL) )
		
		aAreaSM0 := SM0->(GetArea())
		
		SM0->(DbGoTop())
		
		While SM0->(!Eof())
			
			If ( Alltrim(cFilSeek) == Alltrim(SM0->M0_CODFIL) )
				xRet := SM0->&(cCampo)
				Exit
			Endif
				
			SM0->(DbSkip())
			
		EndDo
		
		RestArea(aAreaSM0)
		
	Else
		xRet := Alltrim(SM0->&(cCampo))	
	Endif	 
EndIf

Return(xRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TR50GetCplForn
Fun��o que retorna informa��es do complemento de fornecedor. Para isto � verificado atrav�s
dos par�metros cFornece e cLoja, qual o fornecedor. o par�metro cCampo, informa qual � o campo 
desejado e, lCompFor, indica se a informa��o ser� proveniente, realmente do cadastro de 
complemento de fornecedores, ou (no caso de .f.) do pr�prio cadastro de fornecedores.

Importante: Se determinado campo buscado na tabela de complemento de fornecedor, possuir 
dado em branco, ent�o ser� retornado conte�do de um campo correspondente do pr�prio
cadastro de fornecedores (SA2).

@type 		Function
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------

Function TR50GetCplForn(cFornece,cLoja,cCampo,lCompFor)

Local xRet

Local aDePara	:= {}

If ( lCompFor )

	If ( cCampo == "G4V_NOMMUN" )
		
		xRet := Alltrim(Posicione("G4V", 1, XFilial("G4V")+Padr(cFornece,TamSx3("G4V_FORNEC")[1])+cLoja, "G4V_MUNIC"))
		
		If ( !Empty(xRet) )	
			xRet := Posicione("CC2",3,XFilial("CC2")+xRet,"CC2_MUN")
		Endif
			
	Else
		xRet := Alltrim(Posicione("G4V", 1, XFilial("G4V")+Padr(cFornece,TamSx3("G4V_FORNEC")[1])+cLoja, cCampo))
	Endif

Else
	xRet := ""
Endif

If ( Empty(xRet) )
	
	aDePara := {{"G4V_ENDER","A2_END"},;
				{"G4V_CEP","A2_CEP"},;
				{"G4V_BAIRRO", "A2_BAIRRO"},;
				{"G4V_UF","A2_EST"},;
				{"G4V_NOMMUN","A2_MUN"},;
				{"G4V_EMAIL","A2_EMAIL"},;
				{"G4V_ENVIO",""}} //talvez pegar do campo A2_FORNEMA (Forn. mailing)
	
	aAreaSA2 := GetArea()
	
	SA2->(DbSetOrder(1)) //A2_FILIAL + A2_COD + A2_LOJA
	
	If ( SA2->(DbSeek(XFilial("SA2") + Padr(cFornece,TamSx3("G4V_FORNEC")[1])+cLoja)) )
		
		nP := aScan(aDePara,{|x| Alltrim(Upper(x[1])) == Alltrim(Upper(cCampo)) })
		
		If ( nP > 0 )
		
			SX3->(DbSetOrder(2)) //X3_CAMPO
		
			If ( SX3->(DbSeek(aDePara[nP,2])) )
				xRet := SA2->&(aDePara[nP,2])
			Else
				xRet := aDePara[nP,2]	
			Endif
					
		Endif
				
	Endif
	
	RestArea(aAreaSA2)
	
Endif

Return(xRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TR50CellCollection
Esta fun��o retorna o array que ir� servir de base das se��es do relat�rio, de acordo com
a informa��o do par�metro nSection

@type 		Function
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TR50CellCollection(nSection)

Local aAux		:= {}
Local aRet		:= {}
Local aTamanhos	:= {}

Do Case
Case ( nSection == SEC_FILIAL )	// SE��O 1: DADOS DA FILIAL
	
	//Nome da C�lula, Alias da Tabela, Nome da Coluna, Picture, Tamanho
	
	aAux := { 	{"LBL_FILIAL","G6L","Filial","",Len("Filial:")+5,"LEFT"},;	//Label "Filial:"									
				{"M0_NOME","SM0","Content","",60,"LEFT"}}					//Conte�do de "Filial:" - C�digo e Nome da Filial [XX-XXXXXXXXXXXXXXX]
	
	aAdd(aRet, aClone(aAux))	//Se��o 1.a: A primeira linha do Cabe�alho
					
	aAux := {	{"LBL_ENDER","SM0","Endereco","",Len(STR0021)+5,"LEFT"},;	//Label Endereco//"Endere�o:"
				{"M0_ENDCOB","SM0","ContEnd","",55,"LEFT"},;				//Conte�do de "Endere�o:" - Av. XXXXXXXXXXXXXXXX, 99 
				{"LBL_CEP","SM0","CEP","",Len(STR0023)+5,"LEFT"},;			//Label "CEP"	//"CEP:"
				{"M0_CEPCOB","SM0","ContCep","",14,"LEFT"},;				//Conte�do de "CEP:" - 99999-999
				{"LBL_CIDADE","SM0","Cidade","",Len(STR0025)+5,"LEFT"},;	//Label "Cidade"	//"Cidade:"
				{"M0_CIDCOB","SM0","ContCid","",35,"LEFT"}}					//Conte�do de "Cidade:" - XXXXXXXXXX
	
	aAdd(aRet, aClone(aAux))	//Se��o 1.b: Segunda linha do Cabe�alho
				
	aAux := {	{"LBL_TEL","SM0","Telefone","",Len(STR0027)+5,"LEFT"},;		//Label "Telefone" //"Telefone:"
				{"M0_TEL","SM0","ContTel","",20,"LEFT"}}						//Conte�do de "Telefone:" - 99 9 9999-9999			
					
	aAdd(aRet, aClone(aAux))	//Se��o 1.c: Terceira linha do Cabe�alho
	
Case ( nSection == SEC_FORNECEDOR )	//SE��O 2: DADOS DO FORNECEDOR
	
	aAux := {	{"LBL_NOME","SA2","Fornecedor","",Len(STR0029)+5,"LEFT"},;	//Label "Fornecedor:"
				{"A2_NREDUZ","SA2","ContForn","",80,"LEFT"}}				//Conte�do de "Fornecedor:" - C�digo/Loja - Nome [999999/99 - XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX] 
	
	aAdd(aRet, aClone(aAux))	//Se��o 2.a: Primeira linha do Fornecedor				
	
	aAux := {	{"LBL_ENDER","G4V","Endereco","",Len(STR0021)+5,"LEFT"},;		//Label 	//"Endere�o:"
				{"G4V_ENDER","G4V","ContEnd","",70,"LEFT"},;					//Conte�do de "Endere�o:" - Av. XXXXXXXXXXXXXXXX, 99
				{"LBL_CEP","G4V","CEP","",Len(STR0023)+5,"LEFT"},;				//Label //"CEP:"
				{"G4V_CEP","G4V","ContCep","",14,"LEFT"},;						//Conte�do de "CEP:" - 99999-999
				{"LBL_BAIRRO","G4V","Bairro","",Len(STR0035)+5,"LEFT"},;		//Label //"Bairro:"
				{"G4V_BAIRRO","G4V","ContBai","",20,"LEFT"}}					//Conte�do de "Bairro:" - XXXXXXXXXXXXXXX
	
	aAdd(aRet, aClone(aAux))	//Se��o 2.b: Segunda linha do Fornecedor
					
	aAux := {	{"LBL_UF","G4V","UF","",Len(STR0037)+5,"LEFT"},;			//Label //"UF:"
				{"G4V_UF","G4V","ContUF","",7,"LEFT"},;						//Conte�do de "UF:" - XX
				{"LBL_CIDADE","G4V","Municipio","",Len(STR0039)+5,"LEFT"},;	//Label //"Munic�pio:"
				{"G4V_NOMMUN","G4V","ContMun","",40,"LEFT"},;				//Conte�do de "Munic�pio:" - XXXXXXXXXXXXXXXXXXX
				{"LBL_TEL","SA2","Telefone","",Len(STR0027)+5,"LEFT"},;		//Label //"Telefone:"
				{"A2_TEL","SA2","ContTel","",14,"LEFT"}}					//Conte�do de "Telefone:" - 99 9 9999-9999
					
	aAdd(aRet, aClone(aAux))	//Se��o 2.c: Segunda linha do Fornecedor
	
Case ( nSection == SEC_APURACAO )	//SE��O 3: Apura��o
	
	
	aRet := {	{"G6L_CODAPU","G6L",TurGetTitle("STR0015",nIdioma),"",TamSx3("G6L_CODAPU")[1]+10,"LEFT"},;		//
				{"G6L_DTGERA","G6L",TurGetTitle("STR0043",nIdioma),"",20,"LEFT"},;//"Dt. Gera��o"
				{"DS_STATG6L","G6L",TurGetTitle("STR0044",nIdioma),"",15,"LEFT"},;//"Status"
				{"DSC_MOEDA","G6L",TurGetTitle("STR0045",nIdioma),"",10,"LEFT"},;//"Moeda"
				{"G6L_TOTACD","G6L",TurGetTitle("STR0046",nIdioma),PesqPict("G6L","G6L_TOTACD"),20,"RIGHT"}}//"Total"
	
	
Case ( nSection == SEC_SEGMENTO )	//SE��O 4: SEGMENTO DE NEG�CIO + MODALIDADE DE ACORDO
	
	aRet := {	{"G6M_SEGNEG","G6M",TurGetTitle("STR0047",nIdioma),"",TamSX3("G6M_SEGNEG")[1]+5,"LEFT"},;//"Cod."
				{"DSC_SEGNEG","G6M",TurGetTitle("STR0048",nIdioma),"",20,"LEFT"},;//"Segmento"
				{"DSC_TIPOAC","G6M",TurGetTitle("STR0049",nIdioma),"",20,"LEFT"}}//"Tipo Acordo"
			
Case ( nSection == SEC_ACORDO )	//SE��O 5: ACORDOS
	
	
	aAdd(aTamanhos,TamSx3("G6O_CODACD")[1]+10)
	aAdd(aTamanhos,TamSx3("G6O_CODREV")[1]+10)
	aAdd(aTamanhos,Len(Alltrim(PesqPict("G6O","G6O_VLBASE"))))
	aAdd(aTamanhos,15)
	aAdd(aTamanhos,Len(Alltrim(PesqPict("G6O","G6O_PERCEN")))+10)
	aAdd(aTamanhos,Len(Alltrim(PesqPict("G6O","G6O_VALOR")))+10)
	aAdd(aTamanhos,Len(Alltrim(PesqPict("G6O","G6O_VAPLIC")))+10)
	aAdd(aTamanhos,15)
	
	aRet	:= {	{"G6O_CODACD",	"G6O",TurGetTitle("STR0050",nIdioma),		"",								aTamanhos[1],"LEFT"},;									//"Nro Acordo"
					{"G6O_CODREV",	"G6O",TurGetTitle("STR0051",nIdioma),		"",								aTamanhos[2],"LEFT"},;//"Revis�o"
					{"G6O_VLBASE",	"G6O",TurGetTitle("STR0052",nIdioma),		PesqPict("G6O","G6O_VLBASE"),	aTamanhos[3],"RIGHT"},;//"Vlr. Base"
					{"DSC_TIPOVL",	"G6O",TurGetTitle("STR0053",nIdioma),		"",								aTamanhos[4],"LEFT"},;//"Tipo Valor"
					{"G6O_PERCEN",	"G6O",TurGetTitle("STR0054",nIdioma),		PesqPict("G6O","G6O_PERCEN"),	aTamanhos[5],"RIGHT"},;//"% Acordo"
					{"G6O_VALOR",	"G6O",TurGetTitle("STR0055",nIdioma),		PesqPict("G6O","G6O_VALOR"),	aTamanhos[6],"RIGHT"},;//"Valor Fixo"
					{"G6O_VAPLIC",	"G6O",TurGetTitle("STR0056",nIdioma),	PesqPict("G6O","G6O_VAPLIC"),	aTamanhos[7],"RIGHT"},;//"Vlr. Aplicado"
					{"DS_STATG6O",	"G6O",TurGetTitle("STR0044",nIdioma),			"",								aTamanhos[8],"LEFT"}}//"Status"
	
Case ( nSection == SEC_ITENS_ACORD )	//SE��O 6: ITENS DO ACORDO			
 	
	aRet := {	{"G4C_NUMID","G4C",TurGetTitle("STR0058",nIdioma),"",TamSX3("G4C_NUMID")[1]+5,"LEFT"},;									//"Reg. Venda"
				{"G4C_IDITEM","G4C",TurGetTitle("STR0059",nIdioma),"",TamSX3("G4C_IDITEM")[1]+10,"LEFT"},;//"Item Venda"
				{"G4C_NUMSEQ","G4C",TurGetTitle("STR0060",nIdioma),"",TamSX3("G4C_NUMSEQ")[1]+5,"LEFT"},;//"Seq."
				{"DSC_PAGREC","",TurGetTitle("STR0061",nIdioma),"",20,"LEFT"},;//"Origem"
				{"DS_TIPOG4C","",TurGetTitle("STR0062",nIdioma),"",10,"LEFT"},;//"Tipo"
				{"G4C_VALOR","G4C",TurGetTitle("STR0063",nIdioma),PesqPict("G4C","G4C_VALOR"),TamSX3("G4C_VALOR")[1]+TamSX3("G4C_VALOR")[2]+15,"RIGHT"},;//"Valor"
				{"DSC_DESTIN","",TurGetTitle("STR0064",nIdioma),"",25,"LEFT"},;//"Destino"
				{"DS_STATG4C","",TurGetTitle("STR0044",nIdioma),"",20,"LEFT"}}//"Status"
			
Case ( nSection == SEC_RESUMO )	//SE��O 7: RESUMO DA APURA��O
	
	aRet := {	{"G6M_SEGNEG","G6M",TurGetTitle("STR0048",nIdioma),"",TamSX3("G6M_SEGNEG")[1],"LEFT"},;//"Segmento"
				{"DSC_SEGNEG","G6M",TurGetTitle("STR0067",nIdioma),"",20,"LEFT"},;//"Descri��o"
				{"DSC_TIPOAC","G6M",TurGetTitle("STR0049",nIdioma),"",20,"LEFT"},;//"Tipo Acordo"
				{"G6M_VLACD" ,"G6M",TurGetTitle("STR0069",nIdioma),PesqPict("G6M","G6M_VLACD") ,TamSX3("G6M_VLACD" )[1]+TamSX3("G6M_VLACD" )[2],"RIGHT"},;//"Vlr. Apurado"
				{"G6M_PERDES","G6M",TurGetTitle("STR0070",nIdioma),PesqPict("G6M","G6M_PERDES"),TamSX3("G6M_PERDES")[1]+TamSX3("G6M_PERDES")[2],"RIGHT"},;//"% Desconto"
				{"G6M_VLDESC","G6M",TurGetTitle("STR0071",nIdioma),PesqPict("G6M","G6M_VLDESC"),TamSX3("G6M_VLDESC")[1]+TamSX3("G6M_VLDESC")[2],"RIGHT"},;//"Vlr Desconto"
				{"G6M_PERCTX","G6M",TurGetTitle("STR0072",nIdioma),PesqPict("G6M","G6M_PERCTX"),TamSX3("G6M_PERCTX")[1]+TamSX3("G6M_PERCTX")[2],"RIGHT"},;//"% Adicional"
				{"G6M_VLTXAD","G6M",TurGetTitle("STR0073",nIdioma),PesqPict("G6M","G6M_VLTXAD"),TamSX3("G6M_VLTXAD")[1]+TamSX3("G6M_VLTXAD")[2],"RIGHT"},;//"Vlr Acidional"
				{"G6M_TOTAL" ,"G6M",TurGetTitle("STR0046",nIdioma),PesqPict("G6M","G6M_TOTAL") ,TamSX3("G6M_TOTAL" )[1]+TamSX3("G6M_TOTAL" )[2],"RIGHT"}}//"Total"
		
EndCase

Return(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TR50PutValues
Fun��o que atualiza o conte�do das c�lulas de uma determinada se��o.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TR50PutValues(cAliasG6L, xSection, nSecCell)

Local nI := 0
Local nX := 0
Local nP := 0

Local cConteudo	:= ""

Local aCells 	:= TR50CellCollection(nSecCell)

If ( Valtype(xSection) == "O" )
	
	For nI := 1 to Len(aCells)
		
		If ( ValType((cAliasG6L)->&(aCells[nI,1])) == "C" )
		
			nP := At(")",(cAliasG6L)->&(aCells[nI,1]))
			
			If ( nP > 0 )
			
				cConteudo := SubStr((cAliasG6L)->&(aCells[nI,1]),1,nP)
				
				If ( FindFunction(cConteudo) )
					cConteudo := &(cConteudo)
				Endif
			Else
				cConteudo := (cAliasG6L)->&(aCells[nI,1])	
			Endif
		
		Else
			cConteudo := (cAliasG6L)->&(aCells[nI,1])
		Endif
		
		xSection:Cell(aCells[nI,1]):SetValue(cConteudo)
		
	Next nI
	
	xSection:PrintLine()
	
Else
	
	For nX := 1 to Len(xSection)		
		
		For nI := 1 to Len(aCells[nX])
			
			If ( "LBL_" $ aCells[nX][nI,1] )
				cConteudo := RetLabelContent(aCells[nX][nI,1])
			Else
				
				If ( ValType((cAliasG6L)->&(aCells[nX][nI,1])) == "C" )
				
					nP := At(")",(cAliasG6L)->&(aCells[nX][nI,1]) )
			
					If ( nP > 0 )
					
						cConteudo := SubStr((cAliasG6L)->&(aCells[nX][nI,1]),1,nP)
						
						If ( FindFunction(cConteudo) )
							cConteudo := &(cConteudo)
						Endif
					
					Else
						cConteudo := (cAliasG6L)->&(aCells[nX][nI,1])
					Endif
				
				Else
					cConteudo := (cAliasG6L)->&(aCells[nX][nI,1])
				Endif
				
			Endif
			
			xSection[nX]:Cell(aCells[nX][nI,1]):SetValue(cConteudo)

		Next nI
		
		xSection[nX]:PrintLine()
			
	Next nX
	
Endif

Return()

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} ReportPrint
Fun��o executada pelo bloco de impress�o da classe tReport.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function ReportPrint(cAliasG6L, oReport, lBreak, lAglutina, cEmail,lCompFor)

Local oSecCabL1	:= oReport:Section(1)														//SE��O 1.a: DADOS DA FILIAL: Linha 1
Local oSecCabL2	:= oReport:Section(2)														//SE��O 1.a: DADOS DA FILIAL: Linha 2
Local oSecCabL3	:= oReport:Section(3)														//SE��O 1.a: DADOS DA FILIAL: Linha 3
Local oSecForL1	:= oReport:Section(3):Section(1)											//SE��O 2: DADOS DO FORNECEDOR: Linha 1
Local oSecForL2	:= oReport:Section(3):Section(2)											//SE��O 2: DADOS DO FORNECEDOR: Linha 2
Local oSecForL3	:= oReport:Section(3):Section(3)											//SE��O 2: DADOS DO FORNECEDOR: Linha 3
Local oSecApu	:= oReport:Section(3):Section(3):Section(1)									//SE��O 3: APURA��O
Local oSecSeg	:= oReport:Section(3):Section(3):Section(1):Section(1)						//SE��O 4: SEGMENTO DE NEG�CIO + MODALIDADE DE ACORDO
Local oSecAco	:= oReport:Section(3):Section(3):Section(1):Section(1):Section(1)			//SE��O 5: ACORDOS
Local oSecItm	:= oReport:Section(3):Section(3):Section(1):Section(1):Section(1):Section(1)//SE��O 6: ITENS DO ACORDO
Local oSecRes	:= oReport:Section(4)														//SE��O 7: RESUMO DA APURA��O
Local oFont     := TFont():New("Arial", 15, 10, , .T., , , , .T., .F.)
Local lBrkFornec:= .T.
Local lBrkApura	:= .T.
Local lBrkSeg	:= .T.
Local lBrkAcord	:= .T.
Local lFirstItm	:= .T.

Local aCells	:= {}
Local aResumo	:= {}
Local aAux		:= {}

Local nI 	:= 0
Local nX	:= 0

lBreak := .f.

While ( (cAliasG6L)->(!Eof()) .and. !lBreak)
	
	oReport:StartPage()

	If ( Empty(cEmail) )
		cEmail	:= Lower(TR50GetCplForn((cAliasG6L)->G6L_FORNEC,(cAliasG6L)->G6L_LJFORN,"G4V_EMAIL",lCompFor))
	Endif
		
	//Quebra de Fornecedor
	If ( lBrkFornec )
		
		//Omite Impress�o do cabe�alho das c�luas da Se��o de Fornecedores
		oSecCabL1:SetHeaderSection(.F.)
		oSecCabL2:SetHeaderSection(.F.)
		oSecCabL3:SetHeaderSection(.F.)
		
		//Impressado dos Dados da Filial - In�cio
		oSecCabL1:Init()
		oSecCabL2:Init()
		oSecCabL3:Init()
		
		TR50PutValues(cAliasG6L, {oSecCabL1,oSecCabL2,oSecCabL3}, SEC_FILIAL)		
		oReport:ThinLine()
		//Impressado dos Dados da Filial - Fim
		
		//Omite Impress�o do cabe�alho das c�luas da Se��o de Fornecedores
		oSecForL1:SetHeaderSection(.F.)
		oSecForL2:SetHeaderSection(.F.)
		oSecForL3:SetHeaderSection(.F.)
		
		//Impressado dos Dados do Fornecedor - In�cio
		oSecForL1:Init()
		oSecForL2:Init()
		oSecForL3:Init()
		
		TR50PutValues(cAliasG6L, {oSecForL1,oSecForL2,oSecForL3}, SEC_FORNECEDOR)
		oReport:ThinLine()
		//Impressado dos Dados do Fornecedor - Fim
		
		lBrkFornec := .F.	
		
	Endif
	
	//Quebra de Apuracao
	If ( lBrkApura )
		
		oSecApu:Init()
		TR50PutValues(cAliasG6L, oSecApu, SEC_APURACAO)
		//oReport:ThinLine()
		
		lBrkApura := .F.
		
	Endif
	
	//Quebra de Segmento+Modalidade (Tipo de Acordo)
	If ( lBrkSeg )
		
		oSecSeg:Init()
		TR50PutValues(cAliasG6L, oSecSeg, SEC_SEGMENTO)
		//oReport:ThinLine()
		
		lBrkSeg := .F.
		
	Endif
	
	//Quebra de Acordo
	If ( lBrkAcord )
		
		oSecAco:Init()
		TR50PutValues(cAliasG6L, oSecAco, SEC_ACORDO)
		//oReport:ThinLine()
		
		//Monta o Array de Resumo
		If ( aScan(aResumo, {|x| Alltrim(x[1]+x[2]) == (cAliasG6L)->( Alltrim(G6M_SEGNEG+G6M_TIPOAC) )}) == 0 )
			
			aCells 	:= TR50CellCollection(SEC_RESUMO)
			
			For nI := 1 to Len(aCells)
				aAdd(aAux, {(cAliasG6L)->(aCells[nI,1]),(cAliasG6L)->&(aCells[nI,1])})		
			Next nI
			
			aAdd(aResumo, {(cAliasG6L)->G6M_SEGNEG, (cAliasG6L)->G6M_TIPOAC,aClone(aAux)})
							
			aAux := {}				
		Endif
	
		lBrkAcord := .F.
		
	Endif
	
	//Impressao dos Itens
	If ( lFirstItm )
		oSecItm:Init()
		lFirstItm := .F.
	Endif
		
	TR50PutValues(cAliasG6L, oSecItm, SEC_ITENS_ACORD)
	//Fim da Impress�o dos Itens
				
	//Atualiza as vari�veis antes de mudar de registro
	cFornece	:= (cAliasG6L)->G6L_FORNEC
	cLjForne	:= (cAliasG6L)->G6L_LJFORN
	cApuracao	:= (cAliasG6L)->G6L_CODAPU	
	cSegmento	:= (cAliasG6L)->G6M_SEGNEG + (cAliasG6L)->G6M_TIPOAC
	cAcordo		:= (cAliasG6L)->G6O_CODACD + (cAliasG6L)->G6O_CODREV
	
	(cAliasG6L)->(DbSkip())
	
	//Confer�ncias das quebras do Relat�rio - In�cio
	//Checa se h� necessidade de Gerar um Novo Relat�rio
	lBreak 	:= (cAliasG6L)->(IIf(!lAglutina, cFornece+cLjForne <> G6L_FORNEC+G6L_LJFORN, cFornece <> G6L_FORNEC))
	
	//Confere quais as quebras que irao ocorrer na proxima iteracao
	lBrkFornec	:= lBreak .or. cLjForne <> (cAliasG6L)->G6L_LJFORN 
	lBrkApura	:= lBrkFornec .or. cApuracao <> (cAliasG6L)->G6L_CODAPU 
	lBrkSeg		:= lBrkApura .or. cSegmento <> (cAliasG6L)->G6M_SEGNEG + (cAliasG6L)->G6M_TIPOAC
	lBrkAcord	:= lBrkSeg .or. cAcordo	<> (cAliasG6L)->G6O_CODACD + (cAliasG6L)->G6O_CODREV
	
	//Verifica se h� quebra de Acordo+Revis�o para fechar a se��o correspondente
	If ( lBrkAcord )
		lFirstItm := .T.	//Inicializa o primeiro item do novo acordo
		oSecItm:Finish()
		oSecAco:Finish()
	Endif	
	
	//Verifica se h� quebra de Segmento+Tipo Acordo para fechar a se��o correspondente
	If ( lBrkSeg )
		oSecSeg:Finish()
	Endif	
	
	//Verifica se h� quebra de Apura��o para fechar a se��o correspondente
	If ( lBrkApura )
		
		oSecApu:Finish()
		
		//Imprime Resumo da Apura��o
		oSecRes:Init()
		
		For nI := 1 To Len(aResumo)
			
			For nX := 1 to Len(aResumo[nI,3])
				oSecRes:Cell(aResumo[nI,3][nX,1]):SetValue(aResumo[nI,3][nX,2])
			Next nX
			
			oSecRes:PrintLine()
				
		Next nI
		
		oSecRes:Finish()
		oReport:SkipLine()
		oReport:FatLine()
					
	Endif
	
	//Verifica se h� quebra de Fornecedor para fechar a se��o correspondente
	If ( lBrkFornec )
		
		oSecCabL3:Finish()
		oSecForL3:Finish()
				
	Endif
	//Confer�ncias das quebras do Relat�rio - Fim
	
	//Caso seja necess�rio gerar novo relat�rio, logo � preciso enviar o e-mail ao fornecedor ou pelo menos separar o arquivo impresso do fornecedor
	If ( lBreak )
		//Chamada da Gera��o do E-mail ou separa��o do Arquivo impresso - Tudo depende de lEnvMail
		//Incluir tamb�m toda a parte de encerramento do objeto de impressao
		oReport:EndPage()
		Exit
	Endif
	
EndDo

Return()

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} RetLabelContent
Alguns campos que n�o existem no dicion�rio de dados, mas que s�o utilizados no relat�rio
possuem um t�tulo identificador. Esta fun��o retorna tal t�tulo.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------

Static Function RetLabelContent(cField)

Local cRet		:= ""

Local nP		:= 0

Local aLabels 	:= { 	{"LBL_FILIAL","Filial:"},;									
						{"LBL_ENDER",TurGetTitle("STR0021",nIdioma)},;//"Endere�o:"
						{"LBL_CEP",TurGetTitle("STR0023",nIdioma)},;//"CEP:"
						{"LBL_CIDADE",TurGetTitle("STR0025",nIdioma)},;//"Cidade:"
						{"LBL_TEL",TurGetTitle("STR0027",nIdioma)},;//"Telefone:"
						{"LBL_NOME",TurGetTitle("STR0029",nIdioma)},;//"Fornecedor:"
						{"LBL_BAIRRO",TurGetTitle("STR0035",nIdioma)},;//"Bairro:"
						{"LBL_UF",TurGetTitle("STR0037",nIdioma)}}//"UF:"
				
nP := aScan(aLabels,{|x| Alltrim(x[1]) == Alltrim(cField)})

If ( nP > 0 )
	cRet := aLabels[nP,2]
Endif

Return(cRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TurGetTitle
Retorna o titulo com base no STR e idioma informado no parametro.
@author 	Inova��o
@since 		20/07/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TurGetTitle(cStr,nIdioma)

Local cTitle	:= ""
Local nPos		:= 0
	
	If ( nPos := aScan( aTitle, { |X|  X[3] == cStr } ) ) > 0	
		//-- Idioma = 1 (Portugues), Idioma = 2 (Ingl�s) ;
		cTitle := aTitle[nPos][nIdioma]
	EndIf
	
	
Return(cTitle)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TurGetTitle
Alimenta array statico com os labels  do relatorio.
@author 	Inova��o
@since 		20/07/2016
@version 	12.1.7
/*/
//+---------------------------------------------------------------------------------------
Static Function TURSetTitle(aTitle)
	
	aAdd(aTitle,{'Filial', "Sucursal","STR0014"})
	aAdd(aTitle,{STR0015, "Calculation","STR0015"})
	aAdd(aTitle,{STR0016, "Business Segment","STR0016"})
	aAdd(aTitle,{STR0017, "Agreement","STR0017"})
	aAdd(aTitle,{STR0018, "Agreement Items","STR0018"})
	aAdd(aTitle,{STR0019, "Calculation Summary","STR0019"})
	aAdd(aTitle,{STR0020, "Address","STR0020"})
	aAdd(aTitle,{STR0021, "Address:","STR0021"})
	aAdd(aTitle,{STR0022, "Postal Code","STR0022"})
	aAdd(aTitle,{STR0023, "Postal Code:","STR0023"})
	aAdd(aTitle,{STR0024, "City","STR0024"})
	aAdd(aTitle,{STR0025, "City:","STR0025"})
	aAdd(aTitle,{STR0026, "Phone Number","STR0026"})
	aAdd(aTitle,{STR0027, "Phone Number:","STR0027"})
	aAdd(aTitle,{STR0028, "Supplier","STR0028"})
	aAdd(aTitle,{STR0029, "Supplier:","STR0029"})
	aAdd(aTitle,{STR0030, "Address","STR0030"})
	aAdd(aTitle,{STR0032, "Postal Code","STR0032"})
	aAdd(aTitle,{STR0034, "District","STR0034"})
	aAdd(aTitle,{STR0035, "District:","STR0035"})
	aAdd(aTitle,{STR0036, "State","STR0036"})
	aAdd(aTitle,{STR0037, "State:","STR0037"})
	aAdd(aTitle,{STR0038, "City","STR0038"})
	aAdd(aTitle,{STR0039, "City:","STR0039"})
	aAdd(aTitle,{STR0040, "Phone Number","STR0040"})
	aAdd(aTitle,{STR0043, "Dt. Generation","STR0043"})
	aAdd(aTitle,{STR0044, "Status","STR0044"})
	aAdd(aTitle,{STR0045, "Currency","STR0045"})
	aAdd(aTitle,{STR0046, "Total","STR0046"})
	aAdd(aTitle,{STR0047, "Code","STR0047"})
	aAdd(aTitle,{STR0048, "Segment","STR0048"})
	aAdd(aTitle,{STR0049, "Agreement Type","STR0049"})
	aAdd(aTitle,{STR0050, "Agreement No.","STR0050"})
	aAdd(aTitle,{STR0051, "Review","STR0051"})
	aAdd(aTitle,{STR0052, "Vl. Base","STR0052"})
	aAdd(aTitle,{STR0053, "Value Type","STR0053"})
	aAdd(aTitle,{STR0054, "Agreement %","STR0054"})
	aAdd(aTitle,{STR0055, "Fixed Value","STR0055"})
	aAdd(aTitle,{STR0056, "Vl. Applied","STR0056"})
	aAdd(aTitle,{STR0058, "Rec. Sales","STR0058"})
	aAdd(aTitle,{STR0059, "Sales Item","STR0059"})
	aAdd(aTitle,{STR0060, "Seq.","STR0060"})
	aAdd(aTitle,{STR0061, "Origin","STR0061"})
	aAdd(aTitle,{STR0062, "Type","STR0062"})
	aAdd(aTitle,{STR0063, "Value","STR0063"})
	aAdd(aTitle,{STR0064, "Destination","STR0064"})
	aAdd(aTitle,{STR0067, "Description","STR0067"})
	aAdd(aTitle,{STR0069, "Vl. Calculated","STR0069"})
	aAdd(aTitle,{STR0070, "Discount %","STR0070"})
	aAdd(aTitle,{STR0071, "Discount Vl","STR0071"})
	aAdd(aTitle,{STR0072, "%Bonus","STR0072"})
	aAdd(aTitle,{STR0073, "Bonus Vl.","STR0073"})

Return()


