#Include 'PROTHEUS.CH'
#include 'FILEIO.CH'
#INCLUDE 'TOPCONN.CH'
#INCLUDE 'TURR050.CH'

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TURR050A -> U_TURR050A

Função de usuário para a geração e envio por e-mail do demonstrativos de cobranças. 

@sample 	TURR050A(cApuraDe, cApuraAte, cFornDe, cLjForDe, cFornAte, cLjForAte, lAglutina, lEnvMail, cEmailRem, lCompFor)
@param 		cApuraDe  - String - código da apuração DE
			cApuraAte - String - código da apuração ATE
			cFornDe   - String - código de fornecedor DE
			cLjForDe  - String - loja de fornecedor DE
			cFornAte  - String - código de fornecedor ATE
			cLjForAte - String - loja de fornecedor ATE 
			lAglutina - Lógico - quebra do demonstrativo por Fornecedor(.T.) ou por Fornecedor+Loja (.F.)
			lEnvMail  - Lógico - indica se o demosntrativo deverá se enviado por e-mail para o fornecedor? (.T. sim; .F. não)
			cEmailRem - String - endereço de e-mail do remetente
			cDirFile  - String - caminho para salvar o demonstrativo
			lComplFor - Lógico - determina que informações do fornecedor serão retiradas do complemento (.T. sim; .F. não)
@return   
@author    Thiago Tavares
@since		02/08/2016
@version	P12.1.13

/*/
//+----------------------------------------------------------------------------------------
User Function TURR050A(cApuraDe, cApuraAte, cFornDe, cLjForDe, cFornAte, cLjForAte, lAglutina, lEnvMail, cEmailRem, cDirFile, lCompFor, cFilApu)

Local lContinua   := .F.
Local cApurAbert  := '1'
Local cAliasG6L   := ''

Default cApuraDe  := ''
Default cApuraAte := '' 
Default cFornDe   := '' 
Default cLjForDe  := ''
Default cFornAte  := ''
Default cLjForAte := ''
Default lAglutina := .F.
Default lEnvMail  := .F.
Default cEmailRem := ''
Default cDirFile  := GetMv('MV_DIRAPU', , 'C:\TOTVS\')
Default lCompFor  := .T.
Default cFilApu   := cFilAnt 

If Empty(cApuraDe) .And. Empty(cApuraAte)	
	lContinua  := Pergunte(PadR('TUR50D', 10)) 
	cApuraDe   := MV_PAR01
	cApuraAte  := MV_PAR02
	cFornDe    := MV_PAR03
	cLjForDe   := MV_PAR04
	cFornAte   := IIF(!Empty(MV_PAR05), MV_PAR05, Replicate("Z", TamSX3('A2_COD' )[1]))
	cLjForAte  := IIF(!Empty(MV_PAR06), MV_PAR06, Replicate("Z", TamSX3('A2_LOJA')[1]))
	lAglutina  := MV_PAR07 == 1
	lEnvMail   := MV_PAR08 == 1
	cEmailRem  := Lower(Alltrim(MV_PAR09))
	cDirFile   := IIF(!Empty(Alltrim(MV_PAR10)), Alltrim(MV_PAR10), GetMv('MV_DIRAPUR', , 'C:\TOTVS\')) 
	lCompFor   := MV_PAR11 == 1
	cApurAbert := AllTrim(AllToChar(MV_PAR12))
Else
	lContinua  := !Empty(cApuraAte) .And. !Empty(cLjForAte) .And. !Empty(cFornAte)
EndIf

If lContinua
	// validando o caminho informado para gravar o demonstrativo
	If AllTrim(Upper(cDirFile)) == 'C:' .Or. AllTrim(Upper(cDirFile)) == 'C:\'
		Help( , ,'TURR050A', , STR0082, 1, 0) // "Não é possível gravar o Demonstrativo de Cobrança no diretório informado." 
	Else 
		If SubStr(cDirFile, Len(cDirFile), 1) != '\'
			cDirFile += '\'
		EndIf 		   
		MakeDir(cDirFile)
		MakeDir(cDirFile + 'Log')

		cAliasG6L := TR050QRYRPT(cApuraDe, cApuraAte, cFornDe, cLjForDe, cFornAte, cLjForAte, lCompFor, cApurAbert, lAglutina, cFilApu)
		(cAliasG6L)->(DbGoTop())
		If (cAliasG6L)->(!EOF())
			FwMsgRun( , {|oSelf| TA050PROCRPT(cAliasG6L, lAglutina, lEnvMail, cEmailRem, cDirFile, lCompFor, oSelf)}, STR0005, I18N(IIF(!lEnvMail, STR0078, STR0093), {'0%'}))	// "Aguarde"  // "Gerando e enviando o(s) demonstrativo(s)... "
		Else
			FwAlertWarning(STR0074, STR0002) //   "Não foi possível extrair dados para apresentar o Demonstrativo de Cobrança. Verifique a parametrização e tente novamente."   "Atenção"
		EndIf
		(cAliasG6L)->(DbCloseArea())
	EndIf
EndIf	

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TR050QRYRPT

Função responsável por fazer a consulta na base de dados para compor as informações que
serão apresentadas no demonstrativo conforme filtro indicado por parametrização de usuários

@sample 	TR050QRYRPT(cApuraDe, cApuraAte, cFornDe, cLjForDe, cFornAte, cLjForAte, lCompFor, cApurAbert, lAglutina)
@param 		cApuraDe   - String - código da apuração DE
			cApuraAte  - String - código da apuração ATE
			cFornDe    - String - código de fornecedor DE
			cLjForDe   - String - loja de fornecedor DE
			cFornAte   - String - código de fornecedor ATE
			cLjForAte  - String - loja de fornecedor ATE 
			lComplFor  - Lógico - determina que informações do fornecedor serão retiradas do complemento (.T. sim; .F. não)  
			cApurAbert - String - indica o status da apuração 1-Em Aberto / 2-Liberado
			lAglutina  - Lógico - quebra do demonstrativo por Fornecedor(.T.) ou por Fornecedor+Loja (.F.) 
			cFilApu    - String - filial da apuração
@return		cAliasG6L  - String - Alias da query    
@author 	Thiago Tavares
@since 		02/08/2016
@version 	12.1.13
/*/
//+----------------------------------------------------------------------------------------
Static Function TR050QRYRPT(cApuraDe, cApuraAte, cFornDe, cLjForDe, cFornAte, cLjForAte, lCompFor, cApurAbert, lAglutina, cFilApu)

Local cAliasG6L  := GetNextAlias()
Local cWhere	 := "%" 
Local cFilG6L    := xFilial("G6L", cFilApu)

If ( cApurAbert $ '1|2' )
	cWhere += " AND G6L_STATUS = '" + cApurAbert + "' "	
Endif

If ( lAglutina )
	cWhere += " AND G6L_LJFORN BETWEEN '" + cLjForDe + "' AND '" + cLjForAte + "' "	
EndIf

cWhere += " %"

BeginSQL Alias cAliasG6L
	
	Column G6L_DTGERA as Date
	
	SELECT
		 'TR050GETEMPFIL("' + G3R_FILIAL + '", "M0_NOMECOM")' M0_NOMECOM
		,'TR050GETEMPFIL("' + G3R_FILIAL + '", "M0_ENDCOB" )' M0_ENDCOB
		,'TR050GETEMPFIL("' + G3R_FILIAL + '", "M0_COMPCOB")' M0_COMPCOB
		,'TR050GETEMPFIL("' + G3R_FILIAL + '", "M0_BAIRCOB")' M0_BAIRCOB
		,'TR050GETEMPFIL("' + G3R_FILIAL + '", "M0_CEPCOB" )' M0_CEPCOB
		,'TR050GETEMPFIL("' + G3R_FILIAL + '", "M0_CIDCOB" )' M0_CIDCOB
		,'TR050GETEMPFIL("' + G3R_FILIAL + '", "M0_ESTCOB" )' M0_ESTCOB
		,'TR050GETEMPFIL("' + G3R_FILIAL + '", "M0_CODMUN" )' M0_CODMUN
		,'TR050GETEMPFIL("' + G3R_FILIAL + '", "M0_TEL"    )' M0_TEL
		,G6L_FORNEC
		,G6L_LJFORN
		,(RTRIM(G6L_FORNEC) + ' - ' + G6L_LJFORN) AS A2_COD
		,A2_NREDUZ
		,'TR050GETCPLFORN("' + G6L_FORNEC + '", "' + G6L_LJFORN + '", "G4V_ENDER" , .T.)' G4V_ENDER
		,'TR050GETCPLFORN("' + G6L_FORNEC + '", "' + G6L_LJFORN + '", "G4V_BAIRRO", .T.)' G4V_BAIRRO
		,'TR050GETCPLFORN("' + G6L_FORNEC + '", "' + G6L_LJFORN + '", "G4V_CEP"   , .T.)' G4V_CEP
		,'TR050GETCPLFORN("' + G6L_FORNEC + '", "' + G6L_LJFORN + '", "G4V_UF"    , .T.)' G4V_UF
		,'TR050GETCPLFORN("' + G6L_FORNEC + '", "' + G6L_LJFORN + '", "G4V_NOMMUN", .T.)' G4V_NOMMUN
		,'TR050GETCPLFORN("' + G6L_FORNEC + '", "' + G6L_LJFORN + '", "G4V_PAIS"  , .T.)' G4V_PAIS
		,'TR050GETCPLFORN("' + G6L_FORNEC + '", "' + G6L_LJFORN + '", "G4V_PAIS"  , .T.)' G4V_FONE
		,G3S_NOMINV
		,G6L_FILIAL
		,G6L_CODAPU
		,G6L_GRPPRO
		,G6L_DTINI
		,G6L_DTFIM
		,G6L_TOTAL
		,G6O_CODACD
		,G48_MOEDA
		,G48_VLACD
		,G44_MOEDFO
		,G44_TARBAS
		,G44_TXCAMB
		,G3R_FILIAL
		,G3R_NUMID
		,G3R_IDITEM
		,G3R_NUMSEQ
		,G3R_EMISS
		,G3R_DOC
		,G3R_IDCONF
		,(
			SELECT 
				G3M_NIATA
			FROM 
				%Table:G3M% G3M
			WHERE 
				G3M_FILIAL = G3R_FILIAL
				AND G3M_CODIGO = G3R_POSTOR
				AND G3M.%NotDel%
			) G3M_NIATA
		,G8B_TIPO
	FROM
		%Table:G6L% G6L
	INNER JOIN
		%Table:G6M% G6M
	ON
		G6M_FILIAL = G6L_FILIAL
		AND G6M_CODAPU = G6L_CODAPU
		AND  G6M.%NotDel%
	INNER JOIN
		%Table:G6O% G6O
	ON
		G6O_FILIAL = G6M_FILIAL
		AND G6O_FILREF = G6M_FILREF
		AND G6O_CODAPU = G6M_CODAPU
		AND G6O_TIPOAC = G6M_TIPOAC
		AND G6O.%NotDel%
	INNER JOIN
		%Table:G48% G48
	ON
		G48_FILAPU = G6O_FILIAL
		AND G48_FILREF = G6O_FILREF
		AND G48_CODAPU = G6O_CODAPU
		AND G48_CODACD = G6O_CODACD
		AND G48_CODREC = G6O_CODREV
		AND G48_CLASS = G6O_TIPOAC
		AND G48_CLIFOR = '2'
		AND G48.%NotDel%
	INNER JOIN 
		%Table:G44% G44 
	ON 
		G44_FILIAL = G48_FILIAL
		AND G44_NUMID = G48_NUMID
		AND G44_IDITEM = G48_IDITEM
		AND G44_NUMSEQ = G48_NUMSEQ
		AND G44_CONINU = ' '
		AND G44.%NotDel%
	INNER JOIN 
		%Table:G3R% G3R 
	ON 
		G3R_FILIAL = G48_FILIAL
		AND G3R_NUMID = G48_NUMID
		AND G3R_IDITEM = G48_IDITEM 
		AND G3R_NUMSEQ = G48_NUMSEQ
		AND G3R_CONINU = ' '
		AND G3R.%NotDel%
	INNER JOIN 
		%Table:G3S% G3S 
	ON 
		G3S_FILIAL = G3R_FILIAL
		AND G3S_NUMID = G3R_NUMID
		AND G3S_IDITEM = G3R_IDITEM
		AND G3S_NUMSEQ = G3R_NUMSEQ
		AND G3S_CONINU = ' '
		AND G3S.%NotDel%
	INNER JOIN 
		%Table:G8B% G8B 
	ON 
		G8B_FILIAL = %XFilial:G8B%
		AND G8B_CODIGO = G6M_TIPOAC
		AND G8B.%NotDel%
	INNER JOIN 
		%Table:SA2% SA2 
	ON 
		A2_FILIAL = %XFilial:SA2%
		AND A2_COD = G6L_FORNEC
		AND A2_LOJA = G6L_LJFORN
		AND SA2.%NotDel%
	WHERE
		G6L_FILIAL = %Exp:cFilG6L%
		AND G6L_CODAPU BETWEEN %Exp:cApuraDe% AND %Exp:cApuraAte%
		AND G6L_FORNEC BETWEEN %Exp:cFornDe% AND %Exp:cFornAte% 
		AND G6L_TPAPUR = '3'
		AND G6L.%NotDel% %Exp:cWhere%
	ORDER BY G6L_FORNEC, G6L_LJFORN, G6L_CODAPU, G6M_SEGNEG, G6M_TIPOAC, G6O_CODACD, G6O_CODREV, G48_NUMID, G48_IDITEM, G48_NUMSEQ

EndSQL

Return cAliasG6L

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050PROCRPT

Esta função irá varrer o ResultSet da query que captou os dados do relatório e irá gerar 
o demonstrativo de cobrança para envio ao fornecedor

@sample 	TA050PROCRPT(cAliasG6L, lAglutina, lEnvMail, cEmailRem, cDirFile, lCompFor)
@param 		cAliasG6L - String - ResultSet da query 
			lAglutina - Lógico - quebra do demonstrativo por Fornecedor(.T.) ou por Fornecedor+Loja (.F.)
			lEnvMail  - Lógico - indica se o demosntrativo deverá se enviado por e-mail para o fornecedor? (.T. sim; .F. não)
			cEmailRem - String - endereço de e-mail do remetente
			cDirFile  - String - caminho para salvar o demonstrativo
			lComplFor - Lógico - determina que informações do fornecedor serão retiradas do complemento (.T. sim; .F. não)  
@return
@type 		Function
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA050PROCRPT(cAliasG6L, lAglutina, lEnvMail, cEmailRem, cDirFile, lCompFor, oMsgRun)

Local cArqFil     := IIf(Empty((cAliasG6L)->G6L_FILIAL),'','_' + AllTrim((cAliasG6L)->G6L_FILIAL))
Local oMailServer := Nil
Local cEmail      := ''
Local cBody       := ''
Local cBodyAux    := ''
Local cEnviado    := ''
Local cCodApu     := ''
Local cFornAux    := ''
Local cLojaAux    := ''
Local cNomeAux    := ''
Local cItens      := ''
Local cConteudo   := ''
Local cHtmlArq    := ''
Local cApuracoes  := ''
Local cCaminho    := ''
Local cLogEnvio   := ''
Local cNome       := ''
Local cFornEst    := ''
Local cFornTipo   := ''
Local cTpSeg      := ''
Local cArqBody    := ''
Local cStartPath  := GetSrvProfString('StartPath', '')
Local cPaisDesc   := ''
Local cSimbol     := ''
Local cMsgServer  := ''
Local cCodBRL     := Posicione('G5T', 4, xFilial('G5T') + 'BRL', 'G5T_CODIGO')
Local cCodUSD     := Posicione('G5T', 4, xFilial('G5T') + 'USD', 'G5T_CODIGO')
Local cArqLog     := cDirFile + 'Log\Fornecedor_ApuRec_Envio_' + FwTimeStamp() + '.log'
Local aDatas      := {}
Local aAnexos     := {cStartPath + 'Turismo\logo_cliente'+cArqFil+'.png'}
Local aApuracoes  := {}
Local aRetorno    := {}
Local nHandle     := 0
Local nLength     := 0
Local nX          := 0
Local nPos        := 0
Local nTotal      := 0
Local nCount      := 0
Local nEmail	  := 0
Local nNumEnvio   := SuperGetMv('MV_NUMENV', .F., 10)
Local nNoites     := 0
Local nTxCamb     := 1 
Local nTxUSD      := 1
Local nSinal      := 1
Local nTarBas	  := 0 //valor da tarifa base
Local nVlrAco	  := 0 //valor do acordo aplicado
Local nPercAtu    := 0	
Local nTotReg     := (cAliasG6L)->(ScopeCount())
Local lAbrir      := .F.
Local lSendMail   := .F.
Local lG4VEnvio   := .F.
Local lInter      := .F.
Local lAtuMsg	  := ValType(oMsgRun) == 'O'
Local aTrocas     := {'M0_NOMECOM', 'M0_ENDCOB', 'M0_COMPCOB', 'M0_BAIRCOB', 'M0_CEPCOB' , 'M0_CIDCOB' , 'M0_ESTCOB', 'M0_TEL'  , 'A2_COD', ;
                      'A2_NREDUZ' , 'G4V_ENDER', 'G4V_BAIRRO', 'G4V_CEP'   , 'G4V_BAIRRO', 'G4V_NOMMUN', 'G4V_UF'   , 'G4V_PAIS', 'G4V_FONE'}

If lEnvMail
	oMailServer := TMailManager():New()
	aRetorno    := TXSmtpConn(@oMailServer)
	If (Len(aRetorno) .And. !aRetorno[1]) 
		cLogEnvio += aRetorno[3] + "-" + StrTran(StrTran(aRetorno[2], chr(13),''), chr(10), '') + " - " + TxDtStamp(dDataBase, .T., .F.) + Chr(13) + Chr(10)
	EndIf
EndIf

(cAliasG6L)->(DbGoTop())

If !File( cStartPath + 'Turismo\TR050AcBody'+cArqFil+'.html' ) .Or. !File( cStartPath + 'Turismo\TR050BcBody'+cArqFil+'.html' ) 
	Help( , ,'TA050PROCRPT', , STR0081, 1, 0) // "Não foi possível encontrar o arquivo para compor o Demonstrativo de Cobrança. Por favor, contate o administrador do sistema."	 		

Else
	While (cAliasG6L)->(!EOF())
		cFornAux  := (cAliasG6L)->G6L_FORNEC
		cLojaAux  := (cAliasG6L)->G6L_LJFORN
		cNomeAux  := (cAliasG6L)->A2_NREDUZ
		lInter    := "EX" $ cFornAux .And. cLojaAux == "00EX" 
		cSimbol   := IIF(lInter, 'USD', 'BRL')		
		cFornEst  := Posicione('SA2', 1, xFilial('SA2') + cFornAux + cLojaAux, 'A2_EST')
		cFornTipo := Posicione('G4S', 1, xFilial('G4S') + cFornAux + cLojaAux, 'G4S_TIPO')		// 2-HOTEL / 3-CARRO
		cTpSeg    := Posicione('SBM', 1, xFilial('SBM') + (cAliasG6L)->G6L_GRPPRO, 'BM_TPSEGP')
		lG4VEnvio := TR050GETCPLFORN(cFornAux, cLojaAux, 'G4V_ENVIO', .T.) == '1'
		cNome     := '(' + AllTrim(cFornAux) + '/' + AllTrim(cLojaAux) + ') ' + AllTrim(cNomeAux)
		cEmail    := Lower(AllTrim(TR050GETCPLFORN(cFornAux, cLojaAux, 'G4V_EMAIL', lCompFor)))

		// validando se o fornecedor é internacional
		// caso afirmativo, utilizar o corpo do demonstrativo em ingles	
		If cFornEst == 'EX'
			cArqBody := cStartPath + 'Turismo\TR050BcBody' + cArqFil + '.html'
		Else
			cArqBody := cStartPath + 'Turismo\TR050AcBody' + cArqFil + '.html'
		EndIf

		nHandle := FOpen(cArqBody, FO_READWRITE)
		nLength := FSeek(nHandle, 0, 2)
		FSeek(nHandle, 0)
		FRead(nHandle, @cBodyAux, nLength)
		FClose(nHandle)

		cBody      := cBodyAux 
		cItens     := ''
		cApuracoes := ''
		nTotal     := 0
	
		cBody := StrTran(cBody, 'logo_cliente.png', 'logo_cliente' + cArqFil + '.png')
		
		// preenchendo os dados do cabecalho e do fornecedor
		For nX := 1 To Len(aTrocas)
			If ValType((cAliasG6L)->&(aTrocas[nX])) == 'C'
				If (nPos := At(')', (cAliasG6L)->&(aTrocas[nX]))) > 0
					cConteudo := SubStr((cAliasG6L)->&(aTrocas[nX]), 1 , nPos)
					If (FindFunction(cConteudo))
						cConteudo := &(cConteudo)
					Endif
				Else
					cConteudo := (cAliasG6L)->&(aTrocas[nX])
				Endif
			Else
				cConteudo := (cAliasG6L)->&(aTrocas[nX])
			Endif
			
			If ValType(cConteudo) == 'C'
				cBody := StrTran(cBody, '#' + aTrocas[nX] + '#', AllTrim(cConteudo))
			EndIf
		Next nX
					
		If ( !Empty((cAliasG6L)->&(G4V_PAIS)) )
			cPaisDesc := AllTrim(Posicione('SYA', 1, xFilial('SYA') + (cAliasG6L)->&(G4V_PAIS), 'YA_DESCR'))
		Else
			cPaisDesc := ""
		EndIf		
								
		cBody := StrTran(cBody, '#G4V_NMPAIS#', TurFrmtHtm(cPaisDesc))
		cBody := StrTran(cBody, '#DATAATUAL#' , IIF(cFornEst == 'EX', Month2Str(DATE()) + "/" + Day2Str(DATE()) + "/" + Year2Str(DATE()), DToC(DATE())))
	
		While (cAliasG6L)->(!EOF()) .And. (cFornAux == (cAliasG6L)->G6L_FORNEC .And. (cLojaAux == (cAliasG6L)->G6L_LJFORN .Or. lAglutina))
			cCodApu := (cAliasG6L)->G6L_CODAPU
			nSinal  := IIF((cAliasG6L)->G8B_TIPO == '1', 1, -1)
		
			// pegando a data de inicio e fim de acordo com o tipo de segmento do grupo de produto da apuracao
			aDatas  := TR050RETDTS(cTpSeg, (cAliasG6L)->G3R_FILIAL, (cAliasG6L)->G3R_NUMID, (cAliasG6L)->G3R_IDITEM, (cAliasG6L)->G3R_NUMSEQ)
		
			cItens += '<tr cellspacing="0" cellpadding="0" bgcolor="' + IIf(nCount % 2 == 0, '#D3D3D3', '#FFFFFF') + '">'
			cItens += 		'<td><font size="2px">' + (cAliasG6L)->G3S_NOMINV + '</font></td>'        
			cItens += 		'<td><font size="2px">' + (cAliasG6L)->(G3R_FILIAL + '.' + G3R_NUMID + '.' + G3R_IDITEM + '.' + G3R_NUMSEQ) + '</font></td>'        
			cItens += 		'<td><font size="2px">' + (cAliasG6L)->G3R_IDCONF + '</font></td>'     
			cItens += 		'<td><font size="2px">' + (cAliasG6L)->G3M_NIATA + '</font></td>'      

			nVlrAco := (cAliasG6L)->G48_VLACD
			nTarBas := (cAliasG6L)->G44_TARBAS
			
			// como encontrei alguns RVs com taxa de câmbio na tarifa entre moedas diferentes de BRL 
			// optei por utilizar sempre a taxa de cambio informada na tabela G9L ao invés de pegar a taxa de cambio da aba tarifa
			// faço isso porque a lógica utilizada para conversão primeiro converte pra BRL e depois pra USD 
			// só irei ler a taxa de cambio da aba tarifa caso a taxa de cambio desse dia informada na tabela G9L seja igual a 0 ou 1
			// poderá acontecer de a taxa de cambio da tabela G9L e da aba tarifa sejam iguais a 0 ou 1 e o RV ter moedas diferentes de BRL
			// nesse caso não há o que o fazer e o usuário deverá corrigir a moeda diretamente no RV
			nTxCamb := Posicione('G9L', 2, xFilial('G9L') + (cAliasG6L)->G44_MOEDFO + (cAliasG6L)->G3R_EMISS, 'G9L_TAXA')
			
			If lInter
				// taxa de cambio USD referente ao dia  
				nTxUSD := Posicione('G9L', 2, xFilial('G9L') + cCodUSD + (cAliasG6L)->G3R_EMISS, 'G9L_TAXA')

				// convertando valor da TARIFA
				If (cAliasG6L)->G44_MOEDFO != cCodUSD
					If (cAliasG6L)->G44_MOEDFO == cCodBRL
						// se MOEDA do fornecedor na TARIFA for BRL precisa converter pra USD
						nTarBas := ((cAliasG6L)->G44_TARBAS / nTxUSD) * nSinal
					Else
						// se MOEDA do fornecedor na TARIFA NÃO for BRL precisa converter primeiro pra BRL e depois pra USD
						nTarBas := (((cAliasG6L)->G44_TARBAS * nTxCamb) / nTxUSD) * nSinal
					EndIf
				EndIf

				// convertando valor do ACORDO
				If (cAliasG6L)->G48_MOEDA != cCodUSD
					If (cAliasG6L)->G48_MOEDA == cCodBRL
						// se MOEDA do fornecedor no ACORDO for BRL precisa converter pra USD
						nVlrAco := ((cAliasG6L)->G48_VLACD / nTxUSD) * nSinal
					Else
						nVlrAco := (((cAliasG6L)->G48_VLACD * nTxCamb) / nTxUSD) * nSinal
					EndIf	
				EndIf

			// se não for internacional e a moeda for diferente de BRL precisa converter pra BRL
			Else
				If (cAliasG6L)->G44_MOEDFO != cCodBRL
					nTarBas := ((cAliasG6L)->G44_TARBAS * nTxCamb) * nSinal
				EndIf
				
				If (cAliasG6L)->G48_MOEDA != cCodBRL
					nVlrAco := ((cAliasG6L)->G48_VLACD  * nTxCamb) * nSinal
				EndIf	
			EndIf									

			If cFornEst == 'EX'
				cItens += 	'<td><font size="2px">' + SubStr(aDatas[1], 5, 2) + '/' + SubStr(aDatas[1], 7, 2) + '/' + SubStr(aDatas[1], 1, 4) + '</font></td>' 
				cItens += 	'<td><font size="2px">' + SubStr(aDatas[2], 5, 2) + '/' + SubStr(aDatas[2], 7, 2) + '/' + SubStr(aDatas[2], 1, 4) + '</font></td>'
				cItens += 	'<td align="center"><font size="2px">' + IIF(DateDiffDay(SToD(aDatas[1]), SToD(aDatas[2])) == 0, '1', AllTrim(Str(DateDiffDay(SToD(aDatas[1]), SToD(aDatas[2]))))) + '</font></td>'     
				cItens += 	'<td align="right"><font size="2px"> ' + AllTrim(Transform(nTarBas, '@E 999,999,999.99')) + '</font></td>'
				cItens += 	'<td align="right"><font size="2px"> ' + AllTrim(Transform(nVlrAco, '@E 999,999,999.99')) + '</font></td>'
				cItens += 	'<td align="center"><font size="2px">' + cSimbol + '</font></td>'
			Else
				cItens += '<td><font size="2px">' + SubStr(aDatas[1], 7, 2) + '/' + SubStr(aDatas[1], 5, 2) + '/' + SubStr(aDatas[1], 1, 4) + '</font></td>' 
				cItens += '<td><font size="2px">' + SubStr(aDatas[2], 7, 2) + '/' + SubStr(aDatas[2], 5, 2) + '/' + SubStr(aDatas[2], 1, 4) + '</font></td>'
				cItens += '<td align="center"><font size="2px">' + IIF(DateDiffDay(SToD(aDatas[1]), SToD(aDatas[2])) == 0, '1', AllTrim(Str(DateDiffDay(SToD(aDatas[1]), SToD(aDatas[2]))))) + '</font></td>'     
				cItens += '<td align="center"><font size="2px">' + cSimbol + '</font></td>'     
				cItens += '<td align="right"><font size="2px"> ' + AllTrim(Transform(nTarBas, '@E 999,999,999.99')) + '</font></td>'     
				cItens += '<td align="right"><font size="2px"> ' + AllTrim(Transform(nVlrAco, '@E 999,999,999.99')) + '</font></td>'
			EndIf
			
			cItens += '</tr>'
			nTotal += nVlrAco//(cAliasG6L)->G48_VLACD / nTxCamb (cAliasG6L)->G6L_TOTAL
			nCount++	
			(cAliasG6L)->(DbSkip())
			If lAtuMsg 
				T050AtuMsg(oMsgRun, @nPercAtu, nCount, nTotReg, IIF(!lEnvMail, STR0078, STR0093))
			EndIf			

			If (cAliasG6L)->(Eof()) .Or. cCodApu <> (cAliasG6L)->G6L_CODAPU
				cApuracoes += cCodApu + IIF((cAliasG6L)->(!EOF()) .And. (cFornAux == (cAliasG6L)->G6L_FORNEC .And. (cLojaAux == (cAliasG6L)->G6L_LJFORN .Or. lAglutina)), ', ', '')
				aAdd(aApuracoes, {cCodApu})
			EndIf
		EndDo 
	
		cBody := StrTran(cBody, '#CODAPU#', cApuracoes)
		cBody := StrTran(cBody, '#TOTAL#', AllTrim(Transform(nTotal, '@E 999,999,999.99')))
		cBody := StrTran(cBody, '#ITENS_DE_VENDA#', cItens)

		// no Pergunte (X1) não foi habilitado o envio
		If !lEnvMail		 
			cEnviado  := '2'
			cLogEnvio := STR0091 + " - " + TxDtStamp(dDataBase, .T., .F.) + Chr(13) + Chr(10)		// "Demonstrativo(s) não enviado(s) - Parâmetro de envio de e-mail não habilitado ao gerar as apurações e/ou demonstrativos."			

		// no Pergunte (X1) foi habilitado o envio
		ElseIf lEnvMail .And. lG4VEnvio
			
			// se o email não foi informado ou não é valido
			If Empty(cEmail) .Or. !TXVldEmail(cEmail)
				cEnviado  := '3'
				cLogEnvio += I18N(STR0092, {cNome, cApuracoes}) + Chr(13) + Chr(10)			// "Demonstrativo(s) não enviado(s) - #1 - #2 - No cadastro do Complemento do Fornecedor não foi informado e-mail para envio do demonstrativo ou o e-mail informado não é válido."

			// email informado e valido
			ElseIf !Empty(cEmail) .And. TXVldEmail(cEmail)
				aRetorno := {}
				aRetorno := TURXEnvio(cEmailRem, cEmail, "", cEmailRem, STR0077, cBody, aAnexos, oMailServer) // "Demonstrativo de Cobrança"
				nEmail ++
				If aRetorno[1]		 
					cEnviado  := '1' 			
					cLogEnvio += I18N(STR0084, {cNome, cApuracoes, cEmail, TxDtStamp(dDataBase, .T., .F.)}) + Chr(13) + Chr(10)		// "Demonstrativo(s) enviado(s) com sucesso - #1 - #2 (#3 - #4)"
				Else				
					cEnviado   := '3'
					cMsgServer := aRetorno[3] + "-" + StrTran(StrTran(aRetorno[2], chr(13),''), chr(10), '')
					cLogEnvio  += I18N(STR0085, {cNome, cApuracoes, cMsgServer, cEmail, TxDtStamp(dDataBase, .T., .F.)}) + Chr(13) + Chr(10)		// "Demonstrativo(s) não enviado(s) - #1 - #2 - #3 (#4 - #5)"
				EndIf
				If nEmail > nNumEnvio
					oMailServer:SMTPDisconnect()
					TXSmtpConn(oMailServer)
					nEmail := 0
				EndIf
			EndIf
		
		// no Pergunte (X1) foi habilitado o envio, mas o fornecedor ñ permite
		ElseIf lEnvMail .And. !lG4VEnvio 
			cEnviado  := '2'
			cLogEnvio += I18N(STR0086, {cNome, cApuracoes}) + Chr(13) + Chr(10)		// "Demonstrativo(s) não enviado(s) - #1 - #2 - O Envio Eletrônico do demonstrativo não está habilitado no cadastro de Complemento do Fornecedor."
		EndIf

		// atualizando as apurações
		For nX := 1 To Len(aApuracoes) 
			TA50DemApura('TURA050', aApuracoes[nX][1], {'1', cEnviado, IIF(lEnvMail, cEmailRem, ''), cEmail})
		Next nX

		// gravando o arquivo HTML
		CpyS2T(cStartPath + 'Turismo\logo_cliente'+cArqFil+'.png', cDirFile)
		cHtmlArq := cDirFile + 'FOR_' + AllTrim(cFornAux) + '_' + IIF(!lAglutina, cLojaAux + '_', '') + FwTimeStamp() + cArqFil +'.html'
		If (nHandle := FCreate(cHtmlArq)) == -1
			cLogEnvio += I18N(STR0088 + " - " + TxDtStamp(dDataBase, .T., .F.) + Chr(13) + Chr(10), {cNome, cDirFile, cApuracoes})		// "Erro ao tentar criar o demonstrativo do fornecedor #1 no diretório #2. (#3)" 
  		Else
    		FSeek(nHandle, 0, FS_END)         
    		FWrite(nHandle, cBody, Len(cBody)) 
    		FClose(nHandle)
		Endif

		If (cAliasG6L)->(!EOF())
			aApuracoes := {}
			cFornAux   := (cAliasG6L)->G6L_FORNEC
			cLojaAux   := (cAliasG6L)->G6L_LJFORN
			cNomeAux   := (cAliasG6L)->A2_NREDUZ
		
			cFornEst   := Posicione('SA2', 1, xFilial('SA2') + cFornAux + cLojaAux, 'A2_EST')
			cFornTipo  := Posicione('G4S', 1, xFilial('G4S') + cFornAux + cLojaAux, 'G4S_TIPO')		// 2-HOTEL / 3-CARRO
			cTpSeg     := Posicione('SBM', 1, xFilial('SBM') + (cAliasG6L)->G6L_GRPPRO, 'BM_TPSEGP')
		
			lG4VEnvio  := TR050GETCPLFORN(cFornAux, cLojaAux, 'G4V_ENVIO', .T.) == '1'
			cNome      := '(' + AllTrim(cFornAux) + '/' + AllTrim(cLojaAux) + ') ' + AllTrim(cNomeAux)
			cEmail     := Lower(AllTrim(TR050GETCPLFORN(cFornAux, cLojaAux, 'G4V_EMAIL', lCompFor)))
		EndIf
	EndDo

	If lEnvMail
		oMailServer:SmtpDisconnect()     
		oMailServer := Nil
	EndIf
	
	// gravando o arquivo com o log de envio das apurações
	nHandle := FCreate(cArqLog)
	FSeek(nHandle, 0, FS_END)         
	FWrite(nHandle, cLogEnvio, Len(cLogEnvio)) 
	FClose(nHandle)
	
	If FwAlertYesNo(STR0087, STR0002) 			// "Processo finalizado. Deseja ver o LOG de envio do(s) Demonstrativo(s) de Cobrança?" 		"Atenção"			
		ShellExecute('Open', cArqLog, '', GetTempPath(), 1)
	EndIf

EndIf

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TR050GETEMPFIL

Função que retorna informações da filial que está realizando a cobrança 

@sample 	TR050GETEMPFIL(cFilSeek, cCampo)
@param 		cFilSeek - String - código da filial 
			cCampo   - String - nome do campo que terá o valor retornado
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.13
/*/
//+----------------------------------------------------------------------------------------
Static Function TR050GETEMPFIL(cFilSeek, cCampo)

Local xRet
Local aAreaSM0 := {}

If !Empty(cFilSeek)
	If Alltrim(cFilSeek) <> Alltrim(SM0->M0_CODFIL)
		aAreaSM0 := SM0->(GetArea())
		SM0->(DbGoTop())
		While SM0->(!EOF())
			If Alltrim(cFilSeek) == Alltrim(SM0->M0_CODFIL)
				xRet := SM0->&(cCampo)
				Exit
			EndIf
			SM0->(DbSkip())
		EndDo
		RestArea(aAreaSM0)
	Else
		xRet := Alltrim(SM0->&(cCampo))	
	EndIf	 
EndIf 

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TR050GETCPLFORN

Função que retorna informações fornecedor

@sample 	TR050GETCPLFORN(cForn, cLoja, cCampo, lCompFor)
@param		cForn     - String - código de fornecedor DE
			cLoja     - String - loja de fornecedor DE
			cCampo    - String - código de fornecedor ATE
			lComplFor - Lógico - determina que informações do fornecedor serão retiradas do complemento (.T. sim; .F. não)  
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.13
/*/
//+----------------------------------------------------------------------------------------
Static Function TR050GETCPLFORN(cForn, cLoja, cCampo, lCompFor)

Local nPos    := 0
Local aArea   := GetArea()		//de			//para
Local aDePara := {{'G4V_ENDER' , 'A2_END'   }, ;
				  {'G4V_CEP'   , 'A2_CEP'   }, ;
				  {'G4V_BAIRRO', 'A2_BAIRRO'}, ;
				  {'G4V_UF'    , 'A2_EST'   }, ;
				  {'G4V_NOMMUN', 'A2_MUN'   }, ;
				  {'G4V_EMAIL' , 'A2_EMAIL' }, ;
				  {'G4V_FONE'  , 'A2_TEL'   }, ;
				  {'G4V_ENVIO' , ''         }, ;
				  {'G4V_PAIS'  , 'A2_PAIS'  }} 
Local xRet := ''

If lCompFor
	If cCampo == 'G4V_NOMMUN'
		If !Empty((xRet := Alltrim(Posicione('G4V', 1, xFilial('G4V') + PadR(cForn, TamSX3('G4V_FORNEC')[1]) + PadR(cLoja, TamSX3('G4V_LOJA')[1]), 'G4V_MUNIC'))))	
			xRet := AllTrim(CC2->(GetAdvFVal('CC2', 'CC2_MUN', xFilial('CC2') + xRet, 3, '')))
		EndIf
	ElseIf cCampo == 'G4V_FONE'
		xRet := Alltrim(G4S->(GetAdvFVal('G4S', 'G4S_FONE', xFilial('G4S') + PadR(cForn, TamSX3('G4S_FORNEC')[1]) + PadR(cLoja, TamSX3('G4S_LOJA')[1]), 1, '')))	
	Else
		xRet := AllTrim(G4V->(GetAdvFVal('G4V', cCampo, xFilial('G4V') + PadR(cForn, TamSX3('G4V_FORNEC')[1]) + PadR(cLoja, TamSX3('G4V_LOJA')[1]), 1, '')))
	EndIf
EndIf

If Empty(xRet)
	aArea := GetArea()
	DbSelectArea('SA2')
	SA2->(DbSetOrder(1)) 	// A2_FILIAL + A2_COD + A2_LOJA
	If SA2->(DbSeek(xFilial('SA2') + PadR(cForn, TamSX3('G4V_FORNEC')[1]) + cLoja))
		If (nPos := aScan(aDePara, {|x| Alltrim(Upper(x[1])) == Alltrim(Upper(cCampo))})) > 0
			SX3->(DbSetOrder(2)) //X3_CAMPO
			If SX3->(DbSeek(aDePara[nPos, 2]))
				If aDePara[nPos, 2] != 'A2_TEL'
					xRet := AllTrim(SA2->&(aDePara[nPos, 2]))
				Else
					xRet := AllTrim(SA2->A2_DDI) + '(' + AllTrim(SA2->A2_DDD) + ')' + AllTrim(SA2->A2_TEL) 
				EndIf
			Else
				xRet := AllTrim(aDePara[nPos, 2])	
			EndIf
		EndIf
	EndIf
	SA2->(DbCloseArea())
EndIf

RestArea(aArea)

Return xRet  

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TR050RETDTS

Função que retorna a data de início e fim do respectivo segmento

@sample 	TR050RETDTS(cTpSeg)
@param		cTpSeg - String - código do segmento
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.13
/*/
//+----------------------------------------------------------------------------------------
Static Function TR050RETDTS(cTpSeg, cFilRV, cNumID, cIdItem, cNumSeq)

Local cAliasQry := GetNextAlias() 
Local cQuery    := ''
Local aTabSeg   := {{'1', 'G3T', 'G3T_DTSAID', 'G3T_DTCHEG'}, ;
                    {'2', 'G3U', 'G3U_DTINI' , 'G3U_DTFIM' }, ;
                    {'3', 'G3V', 'G3V_DTINI' , 'G3V_DTFIM' }, ;
                    {'4', 'G3W', 'G3W_DTINI' , 'G3W_DTFIM' }, ;
                    {'5', 'G3Y', 'G3Y_DTINI' , 'G3Y_DTFIM' }, ;
                    {'6', 'G3X', 'G3X_DTINI' , 'G3X_DTFIM' }, ;
                    {'7', 'G3Q', 'G3Q_EMISS' , 'G3Q_EMISS' }, ;
                    {'8', 'G41', 'G41_DTINI' , 'G41_DTFIM' }, ;
                    {'9', 'G40', 'G40_DTINI' , 'G40_DTFIM' }, ;
                    {'A', 'G3Z', 'G3Z_DTINI' , 'G3Z_DTFIM' }, ;
                    {'B', 'G43', 'G43_DTINI' , 'G43_DTFIM' }}
Local cDataIni := ''	
Local cDataFim := ''
Local nPos     := 0

If (nPos := aScan(aTabSeg, {|x| x[1] == cTpSeg})) > 0
	cQuery := I18N("SELECT #1, #2 FROM " + RetSQLName(aTabSeg[nPos][2]) + " WHERE #3_FILIAL = '" + cFilRV + "' AND #3_NUMID = '" + cNumID + "' AND #3_IDITEM = '" + cIdItem + "' AND #3_NUMSEQ = '" + cNumSeq + "' AND D_E_L_E_T_ <> '*'", {aTabSeg[nPos][3], aTabSeg[nPos][4], aTabSeg[nPos][2]})
	cQuery := ChangeQuery(cQuery)
	DBUseArea(.T., "TOPCONN", TCGenQry(, , cQuery), cAliasQry, .F., .T.)
	DbSelectARea(cAliasQry)
	cDataIni := (cAliasQry)->&(aTabSeg[nPos][3])
	cDataFim := (cAliasQry)->&(aTabSeg[nPos][4])
	(cAliasQry)->(DBCloseArea())
EndIf

Return {cDataIni, cDataFim}

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA050AtuMsg

Função para atualização da mensagem de processamento informando o percentual já processado

@type 		function
@author 	Anderson Toledo
@since 		14/09/2016
/*/
//+----------------------------------------------------------------------------------------
Static Function T050AtuMsg(oMessage, nPercAtu, nItemAtu, nTotal, cMsg)

Local nPerc := Int((nItemAtu * 100) / nTotal)

Default	cMsg := ""

If nPerc >= nPercAtu
	oMessage:SetText(I18N(cMsg, {cValToChar( nPerc ) + '%'}))
	ProcessMessages()
	
	//Atualiza o próximo % de atualização da mensagem, sempre de 5% em 5%
	nPercAtu := nPerc + Round(100 / nTotal, 0)
EndIf

Return