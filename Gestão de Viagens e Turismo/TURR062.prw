#Include 'Protheus.ch'
#Include 'TURR062.ch'

/*/{Protheus.doc} TURR062
Relat�rio Cont�bil Turismo

@author paulo.barbosa
@since 06/10/2017
@version 1.0
/*/
Function TURR062()
Local cPerg      := "TURR062"
Local cPath      := ""
Local nHdl       := 0
Local oDlg 

If Pergunte(cPerg,.T.)
	If !(MV_PAR03 == 4 .And. MV_PAR04 == 4)
		oReport := ReportDef()
		oReport:nDevice := 4 //Impress�o em Excel
		oReport:nExcelPrintType := 2 //Impress�o em formato de tabela
		oReport:lXlsTable := .T. //Tipo planilha
		oReport:nFontBody := 11 //define tamanho da fonte do relat�rio
		oReport:cFontBody := "Calibri" //define fonte do relat�rio
		oReport:cFile := STR0048 + DtoS(MV_PAR01) + "_" + StrTran(time(),":","") + ".xml"
		//oReport:cXlsFile := STR0048 + DtoS(MV_PAR01) + "_" + StrTran(time(),":","") + ".xls"
		//oReport:cDir:='C:\temp'
		//oReport:nEnvironment := 2
		
		oReport:PrintDialog()		
		
		//O TREPORT cria o relat�rio na pasta TEMP, portanto precisamos copiar o arquivo para
		//a pasta selecionada pelo usu�rio
		//FwMsgRun(,{||VerifArq(oReport)}, STR0049, STR0050) //"Gerando arquivo"###"Feche o relat�rio para continuar"
		//FwMsgRun(, "Salvando arquivo", )
		//If !Empty(MV_PAR05) .And. FILE(GetTempPath() + oReport:cFile)
			//Copiar arquivo apenas ap�s o usu�rio fechar.
			//Para isso tentamos abrir o arquivo em modo exclusivo e fechamos assim que conseguimos
	
			//cPath := SetPath(AllTrim(MV_PAR05))
			//__CopyFile(GetTempPath() + oReport:cFile, cPath + oReport:cFile)
		//EndIf
	Else
		MsgAlert(STR0051) //"N�o � poss�vel gerar o relat�rio com as op��es selecionadas"
	EndIf
EndIf

Return

/*/{Protheus.doc} ReportDef
Defini��o do objeto do relat�rio personaliz�vel e das se��es que ser�o utilizadas.

@author paulo.barbosa
@since 04/07/2017
@version 1.0
/*/
Static Function ReportDef(cPerg)
Local oReport
Local oSec1
Local oSec2
Local oSec3
Local oSec4

oReport:= TReport():New("TURR062", STR0001, Nil, {|oReport| ReportPrint(oReport, cPerg)}, STR0001, , , , , .F.) //TODO Definir cPerg e STR0001 - Titulo do relatorio

oSec1:= TRSection():New(oReport, STR0002, /*aAlias*/)//"Provis�o a Receber"
oSec1:SetTotalInLine(.F.)

TRCell():New(oSec1, "cFilial"   , /*cAlias*/, STR0004, /*cPicture*/, TamSX3("G4C_FILIAL")[1], /*lPixel*/, {|| FILIAL}, "LEFT") //"Filial"
TRCell():New(oSec1, "cRV"       , /*cAlias*/, STR0005, /*cPicture*/, TamSX3("G4C_NUMID")[1] , /*lPixel*/, {|| RV}, "LEFT") //"RV"
TRCell():New(oSec1, "cItem"     , /*cAlias*/, STR0041, /*cPicture*/, TamSX3("G4C_IDITEM")[1], /*lPixel*/, {|| ITEM}, "LEFT") //"Item"
TRCell():New(oSec1, "cSeq"      , /*cAlias*/, STR0006, /*cPicture*/, TamSX3("G4C_NUMSEQ")[1], /*lPixel*/, {|| SEQ}, "LEFT") //"Seq."
TRCell():New(oSec1, "cOperacao" , /*cAlias*/, STR0007, /*cPicture*/, 25                     , /*lPixel*/, {|| OPERACAO}, "LEFT") //"Opera��o"
TRCell():New(oSec1, "cClassi"   , /*cAlias*/, STR0008, /*cPicture*/, TamSX3("G4C_CLASS")[1] , /*lPixel*/, {|| CLASSIFICACAO}, "LEFT") //"Classifica��o"
TRCell():New(oSec1, "cCodAco"   , /*cAlias*/, STR0009, /*cPicture*/, TamSX3("G4C_NUMACD")[1], /*lPixel*/, {|| COD_ACORDO}, "LEFT") //"C�d. Acordo"
TRCell():New(oSec1, "cDescrAco" , /*cAlias*/, STR0010, /*cPicture*/, TamSX3("G4W_DESCRI")[1], /*lPixel*/, {|| DESC_ACORD}, "LEFT") //"Descri��o Acordo"
TRCell():New(oSec1, "cCobranca" , /*cAlias*/, STR0011, /*cPicture*/, 15                     , /*lPixel*/, {|| COBRANCA}, "LEFT") //"Cobran�a"
TRCell():New(oSec1, "cCodCli"   , /*cAlias*/, STR0012, /*cPicture*/, TamSX3("G3Q_CLIENT")[1], /*lPixel*/, {|| COD_CLIENTE}, "LEFT") //"C�d. Cliente"
TRCell():New(oSec1, "cLojCli"   , /*cAlias*/, STR0043, /*cPicture*/, TamSX3("G3Q_LOJA")[1]  , /*lPixel*/, {|| LOJA_CLIENTE}, "LEFT") //"Loja Cliente"
TRCell():New(oSec1, "cNomeCli"  , /*cAlias*/, STR0013, /*cPicture*/, TamSX3("A1_NOME")[1]   , /*lPixel*/, {|| NOME_CLIENTE}, "LEFT") //"Nome Cliente"
TRCell():New(oSec1, "cGrpProd"  , /*cAlias*/, STR0014, /*cPicture*/, TamSX3("G4C_GRPPRD")[1], /*lPixel*/, {|| GRP_PROD}, "LEFT") //"Grp. Produto"
TRCell():New(oSec1, "cCodForPrd", /*cAlias*/, STR0015, /*cPicture*/, TamSX3("G3R_FORNEC")[1], /*lPixel*/, {|| COD_FORN_PROD}, "LEFT") //"C�d. Forn. Prod."
TRCell():New(oSec1, "cLojForPrd", /*cAlias*/, STR0044, /*cPicture*/, TamSX3("G3R_LOJA")[1]  , /*lPixel*/, {|| LOJA_FORN_PRO}, "LEFT") //"Loja Forn. Prod."
TRCell():New(oSec1, "cNomForPrd", /*cAlias*/, STR0016, /*cPicture*/, TamSX3("A2_NOME")[1]   , /*lPixel*/, {|| NOME_FORN_PROD}, "LEFT") //"Nome Forn. Prod."
TRCell():New(oSec1, "cTipoDoc"  , /*cAlias*/, STR0017, /*cPicture*/, 15                     , /*lPixel*/, {|| TIPO_DOC}, "LEFT") //"Tipo Doc."
//TRCell():New(oSec1, "cDocumento", /*cAlias*/, STR0018, /*cPicture*/, TamSX3("G4C_DOC")[1]   , /*lPixel*/, {|| }, "LEFT") //"Documento"
TRCell():New(oSec1, "cDataEmis" , /*cAlias*/, STR0019, /*cPicture*/, 8                      , /*lPixel*/, {|| DATA_EMISSAO}, "LEFT") //"Data Emiss�o"
TRCell():New(oSec1, "cDataInclu", /*cAlias*/, STR0020, /*cPicture*/, 8                      , /*lPixel*/, {|| DATA_INCLUSAO}, "LEFT") //"Data Inclus�o"
TRCell():New(oSec1, "cCodFOP"   , /*cAlias*/, STR0021, /*cPicture*/, TamSX3("G3Q_FORMPG")[1], /*lPixel*/, {|| COD_FOP}, "LEFT") //"C�d. FOP"
TRCell():New(oSec1, "cDescriFOP", /*cAlias*/, STR0022, /*cPicture*/, TamSX3("G3N_DESCR")[1] , /*lPixel*/, {|| DESCRICAO_FOP}, "LEFT") //"Descri��o FOP"
TRCell():New(oSec1, "cTipo", /*cAlias*/, "Tipo", /*cPicture*/, TamSX3("G3N_DESCR")[1] , /*lPixel*/, {|| TIPO}, "LEFT") //"TIPO"
TRCell():New(oSec1, "cMoeda"    , /*cAlias*/, STR0042, /*cPicture*/, TamSX3("G5T_DESCR")[1] , /*lPixel*/, {|| MOEDA}, "LEFT") //"Moeda"
TRCell():New(oSec1, "cVlrServ"  , /*cAlias*/, STR0047, "@E 99,999,999,999.99", TamSX3("G4C_VALOR")[1] , /*lPixel*/, {|| VALOR}, "LEFT") //"Valor"
TRCell():New(oSec1, "cTpTarifa" , /*cAlias*/, STR0026, /*cPicture*/, 15                     , /*lPixel*/, {|| TIPO_TARIFA}, "LEFT") //"Tipo Tarifa"
TRCell():New(oSec1, "cDestino"  , /*cAlias*/, STR0027, /*cPicture*/, 15                     , /*lPixel*/, {|| DESTINO}, "LEFT") //"Destino"
TRCell():New(oSec1, "cSistOri"  , /*cAlias*/, STR0028, /*cPicture*/, TamSX3("G4K_DESCR")[1] , /*lPixel*/, {|| SISTEMA_ORIGEM}, "LEFT") //"Sistema Origem"
TRCell():New(oSec1, "cCodForRep", /*cAlias*/, STR0029, /*cPicture*/, TamSX3("G3R_FORREP")[1], /*lPixel*/, {|| FORNEC_REP}, "LEFT") //"Fornecedor Report"
TRCell():New(oSec1, "cLojForRep", /*cAlias*/, STR0045, /*cPicture*/, TamSX3("G3R_LOJREP")[1], /*lPixel*/, {|| LOJA_FORN_REP}, "LEFT") //"Loja Forn. Rep."
TRCell():New(oSec1, "cNomForRep", /*cAlias*/, STR0030, /*cPicture*/, TamSX3("A2_NOME")[1]   , /*lPixel*/, {|| NOME_FORN_REP}, "LEFT") //"Nome Forn. Report"
TRCell():New(oSec1, "cCliFor"   , /*cAlias*/, STR0031, /*cPicture*/, 10                     , /*lPixel*/, {|| CLI_FOR}, "LEFT") //"Cliente / Fornec."
TRCell():New(oSec1, "cCodEntFin", /*cAlias*/, STR0032, /*cPicture*/, TamSX3("G4C_CODIGO")[1], /*lPixel*/, {|| COD_ENT_FIN}, "LEFT") //"Cod. Ent. Financ."
TRCell():New(oSec1, "cLojForRep", /*cAlias*/, STR0046, /*cPicture*/, TamSX3("G4C_LOJA")[1]  , /*lPixel*/, {|| LOJA_ENT_FIN}, "LEFT") //"Loja Ent. Financ."
TRCell():New(oSec1, "cNomEntFin", /*cAlias*/, STR0033, /*cPicture*/, TamSX3("A1_NOME")[1]   , /*lPixel*/, {|| NOME_ENT_FIN}, "LEFT") //"Nome Ent. Financ."
TRCell():New(oSec1, "cStatus", /*cAlias*/, "STATUS", /*cPicture*/, 15  , /*lPixel*/, {|| STATUS}, "LEFT") //"STATUS"
TRCell():New(oSec1, "cDiverg"   , /*cAlias*/, STR0034, /*cPicture*/, 25                     , /*lPixel*/, {|| DIVERGENCIA}, "LEFT") //"Diverg�ncia"
TRCell():New(oSec1, "cApuracao"   , /*cAlias*/, "Apura��o", /*cPicture*/, 10                     , /*lPixel*/, {|| APURACAO}, "LEFT") //"APURACAO"
TRCell():New(oSec1, "cFilApur"   , /*cAlias*/, "Filial Apur", /*cPicture*/, 10                     , /*lPixel*/, {|| FILIAL_APURACAO}, "LEFT") //"FILIAL APURACAO"
TRCell():New(oSec1, "cStatusApur"   , /*cAlias*/, "Status Apur", /*cPicture*/, 15                     , /*lPixel*/, {|| STATUS_APURACAO}, "LEFT") //"STATUS APURACAO"
TRCell():New(oSec1, "cVlrApur"  , /*cAlias*/, "Total Apur", "@E 99,999,999,999.99", TamSX3("G81_VALOR")[1] , /*lPixel*/, {|| VALOR_TOT_APURACAO}, "LEFT") //"Valor Total Apur."

oSec2:= TRSection():New(oReport, STR0035, /*aAlias*/)//"Provis�o a Pagar"
oSec2:SetTotalInLine(.F.)

TRCell():New(oSec2, "cFilial"   , /*cAlias*/, STR0004, /*cPicture*/, TamSX3("G4C_FILIAL")[1], /*lPixel*/, {|| FILIAL}, "LEFT") //"Filial"
TRCell():New(oSec2, "cRV"       , /*cAlias*/, STR0005, /*cPicture*/, TamSX3("G4C_NUMID")[1] , /*lPixel*/, {|| RV}, "LEFT") //"RV"
TRCell():New(oSec2, "cItem"     , /*cAlias*/, STR0041, /*cPicture*/, TamSX3("G4C_IDITEM")[1], /*lPixel*/, {|| ITEM}, "LEFT") //"Item"
TRCell():New(oSec2, "cSeq"      , /*cAlias*/, STR0006, /*cPicture*/, TamSX3("G4C_NUMSEQ")[1], /*lPixel*/, {|| SEQ}, "LEFT") //"Seq."
TRCell():New(oSec2, "cOperacao" , /*cAlias*/, STR0007, /*cPicture*/, 25                     , /*lPixel*/, {|| OPERACAO}, "LEFT") //"Opera��o"
TRCell():New(oSec2, "cClassi"   , /*cAlias*/, STR0008, /*cPicture*/, TamSX3("G4C_CLASS")[1] , /*lPixel*/, {|| CLASSIFICACAO}, "LEFT") //"Classifica��o"
TRCell():New(oSec2, "cCodAco"   , /*cAlias*/, STR0009, /*cPicture*/, TamSX3("G4C_NUMACD")[1], /*lPixel*/, {|| COD_ACORDO}, "LEFT") //"C�d. Acordo"
TRCell():New(oSec2, "cDescrAco" , /*cAlias*/, STR0010, /*cPicture*/, TamSX3("G4W_DESCRI")[1], /*lPixel*/, {|| DESC_ACORD}, "LEFT") //"Descri��o Acordo"
TRCell():New(oSec2, "cCobranca" , /*cAlias*/, STR0011, /*cPicture*/, 15                     , /*lPixel*/, {|| COBRANCA}, "LEFT") //"Cobran�a"
TRCell():New(oSec2, "cCodCli"   , /*cAlias*/, STR0012, /*cPicture*/, TamSX3("G3Q_CLIENT")[1], /*lPixel*/, {|| COD_CLIENTE}, "LEFT") //"C�d. Cliente"
TRCell():New(oSec2, "cLojCli"   , /*cAlias*/, STR0043, /*cPicture*/, TamSX3("G3Q_LOJA")[1]  , /*lPixel*/, {|| LOJA_CLIENTE}, "LEFT") //"Loja Cliente"
TRCell():New(oSec2, "cNomeCli"  , /*cAlias*/, STR0013, /*cPicture*/, TamSX3("A1_NOME")[1]   , /*lPixel*/, {|| NOME_CLIENTE}, "LEFT") //"Nome Cliente"
TRCell():New(oSec2, "cGrpProd"  , /*cAlias*/, STR0014, /*cPicture*/, TamSX3("G4C_GRPPRD")[1], /*lPixel*/, {|| GRP_PROD}, "LEFT") //"Grp. Produto"
TRCell():New(oSec2, "cCodForPrd", /*cAlias*/, STR0015, /*cPicture*/, TamSX3("G3R_FORNEC")[1], /*lPixel*/, {|| COD_FORN_PROD}, "LEFT") //"C�d. Forn. Prod."
TRCell():New(oSec2, "cLojForPrd", /*cAlias*/, STR0044, /*cPicture*/, TamSX3("G3R_LOJA")[1]  , /*lPixel*/, {|| LOJA_FORN_PRO}, "LEFT") //"Loja Forn. Prod."
TRCell():New(oSec2, "cNomForPrd", /*cAlias*/, STR0016, /*cPicture*/, TamSX3("A2_NOME")[1]   , /*lPixel*/, {|| NOME_FORN_PROD}, "LEFT") //"Nome Forn. Prod."
TRCell():New(oSec2, "cTipoDoc"  , /*cAlias*/, STR0017, /*cPicture*/, 15                     , /*lPixel*/, {|| TIPO_DOC}, "LEFT") //"Tipo Doc."
//TRCell():New(oSec2, "cDocumento", /*cAlias*/, STR0018, /*cPicture*/, TamSX3("G4C_DOC")[1]   , /*lPixel*/, {|| }, "LEFT") //"Documento"
TRCell():New(oSec2, "cDataEmis" , /*cAlias*/, STR0019, /*cPicture*/, 8                      , /*lPixel*/, {|| DATA_EMISSAO}, "LEFT") //"Data Emiss�o"
TRCell():New(oSec2, "cDataInclu", /*cAlias*/, STR0020, /*cPicture*/, 8                      , /*lPixel*/, {|| DATA_INCLUSAO}, "LEFT") //"Data Inclus�o"
TRCell():New(oSec2, "cCodFOP"   , /*cAlias*/, STR0021, /*cPicture*/, TamSX3("G3Q_FORMPG")[1], /*lPixel*/, {|| COD_FOP}, "LEFT") //"C�d. FOP"
TRCell():New(oSec2, "cDescriFOP", /*cAlias*/, STR0022, /*cPicture*/, TamSX3("G3N_DESCR")[1] , /*lPixel*/, {|| DESCRICAO_FOP}, "LEFT") //"Descri��o FOP"
TRCell():New(oSec2, "cTipo", /*cAlias*/, "Tipo", /*cPicture*/, TamSX3("G3N_DESCR")[1] , /*lPixel*/, {|| TIPO}, "LEFT") //"TIPO"
TRCell():New(oSec2, "cMoeda"    , /*cAlias*/, STR0042, /*cPicture*/, TamSX3("G5T_DESCR")[1] , /*lPixel*/, {|| MOEDA}, "LEFT") //"Moeda"
TRCell():New(oSec2, "cVlrServ"  , /*cAlias*/, STR0047, "@E 99,999,999,999.99", TamSX3("G4C_VALOR")[1] , /*lPixel*/, {|| VALOR}, "LEFT") //"Valor"
TRCell():New(oSec2, "cTpTarifa" , /*cAlias*/, STR0026, /*cPicture*/, 15                     , /*lPixel*/, {|| TIPO_TARIFA}, "LEFT") //"Tipo Tarifa"
TRCell():New(oSec2, "cDestino"  , /*cAlias*/, STR0027, /*cPicture*/, 15                     , /*lPixel*/, {|| DESTINO}, "LEFT") //"Destino"
TRCell():New(oSec2, "cSistOri"  , /*cAlias*/, STR0028, /*cPicture*/, TamSX3("G4K_DESCR")[1] , /*lPixel*/, {|| SISTEMA_ORIGEM}, "LEFT") //"Sistema Origem"
TRCell():New(oSec2, "cCodForRep", /*cAlias*/, STR0029, /*cPicture*/, TamSX3("G3R_FORREP")[1], /*lPixel*/, {|| FORNEC_REP}, "LEFT") //"Fornecedor Report"
TRCell():New(oSec2, "cLojForRep", /*cAlias*/, STR0045, /*cPicture*/, TamSX3("G3R_LOJREP")[1], /*lPixel*/, {|| LOJA_FORN_REP}, "LEFT") //"Loja Forn. Rep."
TRCell():New(oSec2, "cNomForRep", /*cAlias*/, STR0030, /*cPicture*/, TamSX3("A2_NOME")[1]   , /*lPixel*/, {|| NOME_FORN_REP}, "LEFT") //"Nome Forn. Report"
TRCell():New(oSec2, "cCliFor"   , /*cAlias*/, STR0031, /*cPicture*/, 10                     , /*lPixel*/, {|| CLI_FOR}, "LEFT") //"Cliente / Fornec."
TRCell():New(oSec2, "cCodEntFin", /*cAlias*/, STR0032, /*cPicture*/, TamSX3("G4C_CODIGO")[1], /*lPixel*/, {|| COD_ENT_FIN}, "LEFT") //"Cod. Ent. Financ."
TRCell():New(oSec2, "cLojForRep", /*cAlias*/, STR0046, /*cPicture*/, TamSX3("G4C_LOJA")[1]  , /*lPixel*/, {|| LOJA_ENT_FIN}, "LEFT") //"Loja Ent. Financ."
TRCell():New(oSec2, "cStatus", /*cAlias*/, "STATUS", /*cPicture*/, 15  , /*lPixel*/, {|| STATUS}, "LEFT") //"STATUS"
TRCell():New(oSec2, "cNomEntFin", /*cAlias*/, STR0033, /*cPicture*/, TamSX3("A1_NOME")[1]   , /*lPixel*/, {|| NOME_ENT_FIN}, "LEFT") //"Nome Ent. Financ."
TRCell():New(oSec2, "cStatus", /*cAlias*/, "STATUS", /*cPicture*/, 15  , /*lPixel*/, {|| STATUS}, "LEFT") //"STATUS"
TRCell():New(oSec2, "cDiverg"   , /*cAlias*/, STR0034, /*cPicture*/, 25                     , /*lPixel*/, {|| DIVERGENCIA}, "LEFT") //"Diverg�ncia"
TRCell():New(oSec2, "cApuracao"   , /*cAlias*/, "Apura��o", /*cPicture*/, 10                     , /*lPixel*/, {|| APURACAO}, "LEFT") //"APURACAO"
TRCell():New(oSec2, "cFilApur"   , /*cAlias*/, "Filial Apur", /*cPicture*/, 10                     , /*lPixel*/, {|| FILIAL_APURACAO}, "LEFT") //"FILIAL APURACAO"
TRCell():New(oSec2, "cStatusApur"   , /*cAlias*/, "Status Apur", /*cPicture*/, 15                     , /*lPixel*/, {|| STATUS_APURACAO}, "LEFT") //"STATUS APURACAO"
TRCell():New(oSec2, "cVlrApur"  , /*cAlias*/, "Total Apur", "@E 99,999,999,999.99", TamSX3("G81_VALOR")[1] , /*lPixel*/, {|| VALOR_TOT_APURACAO}, "LEFT") //"Valor Total Apur."

oSec3:= TRSection():New(oReport, STR0036, /*aAlias*/)//"Efetivo a Receber"
oSec3:SetTotalInLine(.F.)

TRCell():New(oSec3, "cFilial"   , /*cAlias*/, STR0004, /*cPicture*/, TamSX3("G4C_FILIAL")[1], /*lPixel*/, {|| FILIAL}, "LEFT") //"Filial"
TRCell():New(oSec3, "cRV"       , /*cAlias*/, STR0005, /*cPicture*/, TamSX3("G4C_NUMID")[1] , /*lPixel*/, {|| RV}, "LEFT") //"RV"
TRCell():New(oSec3, "cItem"     , /*cAlias*/, STR0041, /*cPicture*/, TamSX3("G4C_IDITEM")[1], /*lPixel*/, {|| ITEM}, "LEFT") //"Item"
TRCell():New(oSec3, "cSeq"      , /*cAlias*/, STR0006, /*cPicture*/, TamSX3("G4C_NUMSEQ")[1], /*lPixel*/, {|| SEQ}, "LEFT") //"Seq."
TRCell():New(oSec3, "cOperacao" , /*cAlias*/, STR0007, /*cPicture*/, 25                     , /*lPixel*/, {|| OPERACAO}, "LEFT") //"Opera��o"
TRCell():New(oSec3, "cClassi"   , /*cAlias*/, STR0008, /*cPicture*/, TamSX3("G4C_CLASS")[1] , /*lPixel*/, {|| CLASSIFICACAO}, "LEFT") //"Classifica��o"
TRCell():New(oSec3, "cCodAco"   , /*cAlias*/, STR0009, /*cPicture*/, TamSX3("G4C_NUMACD")[1], /*lPixel*/, {|| COD_ACORDO}, "LEFT") //"C�d. Acordo"
TRCell():New(oSec3, "cDescrAco" , /*cAlias*/, STR0010, /*cPicture*/, TamSX3("G4W_DESCRI")[1], /*lPixel*/, {|| DESC_ACORD}, "LEFT") //"Descri��o Acordo"
TRCell():New(oSec3, "cCobranca" , /*cAlias*/, STR0011, /*cPicture*/, 15                     , /*lPixel*/, {|| COBRANCA}, "LEFT") //"Cobran�a"
TRCell():New(oSec3, "cCodCli"   , /*cAlias*/, STR0012, /*cPicture*/, TamSX3("G3Q_CLIENT")[1], /*lPixel*/, {|| COD_CLIENTE}, "LEFT") //"C�d. Cliente"
TRCell():New(oSec3, "cLojCli"   , /*cAlias*/, STR0043, /*cPicture*/, TamSX3("G3Q_LOJA")[1]  , /*lPixel*/, {|| LOJA_CLIENTE}, "LEFT") //"Loja Cliente"
TRCell():New(oSec3, "cNomeCli"  , /*cAlias*/, STR0013, /*cPicture*/, TamSX3("A1_NOME")[1]   , /*lPixel*/, {|| NOME_CLIENTE}, "LEFT") //"Nome Cliente"
TRCell():New(oSec3, "cGrpProd"  , /*cAlias*/, STR0014, /*cPicture*/, TamSX3("G4C_GRPPRD")[1], /*lPixel*/, {|| GRP_PROD}, "LEFT") //"Grp. Produto"
TRCell():New(oSec3, "cCodForPrd", /*cAlias*/, STR0015, /*cPicture*/, TamSX3("G3R_FORNEC")[1], /*lPixel*/, {|| COD_FORN_PROD}, "LEFT") //"C�d. Forn. Prod."
TRCell():New(oSec3, "cLojForPrd", /*cAlias*/, STR0044, /*cPicture*/, TamSX3("G3R_LOJA")[1]  , /*lPixel*/, {|| LOJA_FORN_PRO}, "LEFT") //"Loja Forn. Prod."
TRCell():New(oSec3, "cNomForPrd", /*cAlias*/, STR0016, /*cPicture*/, TamSX3("A2_NOME")[1]   , /*lPixel*/, {|| NOME_FORN_PROD}, "LEFT") //"Nome Forn. Prod."
TRCell():New(oSec3, "cTipoDoc"  , /*cAlias*/, STR0017, /*cPicture*/, 15                     , /*lPixel*/, {|| TIPO_DOC}, "LEFT") //"Tipo Doc."
//TRCell():New(oSec3, "cDocumento", /*cAlias*/, STR0018, /*cPicture*/, TamSX3("G4C_DOC")[1]   , /*lPixel*/, {|| }, "LEFT") //"Documento"
TRCell():New(oSec3, "cDataEmis" , /*cAlias*/, STR0019, /*cPicture*/, 8                      , /*lPixel*/, {|| DATA_EMISSAO}, "LEFT") //"Data Emiss�o"
TRCell():New(oSec3, "cDataInclu", /*cAlias*/, STR0020, /*cPicture*/, 8                      , /*lPixel*/, {|| DATA_INCLUSAO}, "LEFT") //"Data Inclus�o"
TRCell():New(oSec3, "cCodFOP"   , /*cAlias*/, STR0021, /*cPicture*/, TamSX3("G3Q_FORMPG")[1], /*lPixel*/, {|| COD_FOP}, "LEFT") //"C�d. FOP"
TRCell():New(oSec3, "cDescriFOP", /*cAlias*/, STR0022, /*cPicture*/, TamSX3("G3N_DESCR")[1] , /*lPixel*/, {|| DESCRICAO_FOP}, "LEFT") //"Descri��o FOP"
TRCell():New(oSec3, "cTipo", /*cAlias*/, "Tipo", /*cPicture*/, TamSX3("G3N_DESCR")[1] , /*lPixel*/, {|| TIPO}, "LEFT") //"TIPO"
TRCell():New(oSec3, "cMoeda"    , /*cAlias*/, STR0042, /*cPicture*/, TamSX3("G5T_DESCR")[1] , /*lPixel*/, {|| MOEDA}, "LEFT") //"Moeda"
TRCell():New(oSec3, "cVlrServ"  , /*cAlias*/, STR0047, "@E 99,999,999,999.99", TamSX3("G4C_VALOR")[1] , /*lPixel*/, {|| VALOR}, "LEFT") //"Valor"
TRCell():New(oSec3, "cTpTarifa" , /*cAlias*/, STR0026, /*cPicture*/, 15                     , /*lPixel*/, {|| TIPO_TARIFA}, "LEFT") //"Tipo Tarifa"
TRCell():New(oSec3, "cDestino"  , /*cAlias*/, STR0027, /*cPicture*/, 15                     , /*lPixel*/, {|| DESTINO}, "LEFT") //"Destino"
TRCell():New(oSec3, "cSistOri"  , /*cAlias*/, STR0028, /*cPicture*/, TamSX3("G4K_DESCR")[1] , /*lPixel*/, {|| SISTEMA_ORIGEM}, "LEFT") //"Sistema Origem"
TRCell():New(oSec3, "cCodForRep", /*cAlias*/, STR0029, /*cPicture*/, TamSX3("G3R_FORREP")[1], /*lPixel*/, {|| FORNEC_REP}, "LEFT") //"Fornecedor Report"
TRCell():New(oSec3, "cLojForRep", /*cAlias*/, STR0045, /*cPicture*/, TamSX3("G3R_LOJREP")[1], /*lPixel*/, {|| LOJA_FORN_REP}, "LEFT") //"Loja Forn. Rep."
TRCell():New(oSec3, "cNomForRep", /*cAlias*/, STR0030, /*cPicture*/, TamSX3("A2_NOME")[1]   , /*lPixel*/, {|| NOME_FORN_REP}, "LEFT") //"Nome Forn. Report"
TRCell():New(oSec3, "cCliFor"   , /*cAlias*/, STR0031, /*cPicture*/, 10                     , /*lPixel*/, {|| CLI_FOR}, "LEFT") //"Cliente / Fornec."
TRCell():New(oSec3, "cCodEntFin", /*cAlias*/, STR0032, /*cPicture*/, TamSX3("G4C_CODIGO")[1], /*lPixel*/, {|| COD_ENT_FIN}, "LEFT") //"Cod. Ent. Financ."
TRCell():New(oSec3, "cLojForRep", /*cAlias*/, STR0046, /*cPicture*/, TamSX3("G4C_LOJA")[1]  , /*lPixel*/, {|| LOJA_ENT_FIN}, "LEFT") //"Loja Ent. Financ."
TRCell():New(oSec3, "cNomEntFin", /*cAlias*/, STR0033, /*cPicture*/, TamSX3("A1_NOME")[1]   , /*lPixel*/, {|| NOME_ENT_FIN}, "LEFT") //"Nome Ent. Financ."
TRCell():New(oSec3, "cStatus", /*cAlias*/, "STATUS", /*cPicture*/, 15  , /*lPixel*/, {|| STATUS}, "LEFT") //"STATUS"
TRCell():New(oSec3, "cDocEfe"   , /*cAlias*/, STR0037, /*cPicture*/, 10                     , /*lPixel*/, {|| DOC_EFETIVACAO}, "LEFT") //"Doc. Efetiva��o"
TRCell():New(oSec3, "cNumDocEfe", /*cAlias*/, STR0038, /*cPicture*/, 10                     , /*lPixel*/, {|| NR_DOC}, "LEFT") //"Nr. Documento"
TRCell():New(oSec3, "cDatDocEfe", /*cAlias*/, STR0039, /*cPicture*/, 10                     , /*lPixel*/, {|| DATA_DOC}, "LEFT") //"Data Documento"
TRCell():New(oSec3, "cDiverg"   , /*cAlias*/, STR0034, /*cPicture*/, 25                     , /*lPixel*/, {|| DIVERGENCIA}, "LEFT") //"Diverg�ncia"
TRCell():New(oSec3, "cApuracao"   , /*cAlias*/, "Apura��o", /*cPicture*/, 10                     , /*lPixel*/, {|| APURACAO}, "LEFT") //"APURACAO"
TRCell():New(oSec3, "cFilApur"   , /*cAlias*/, "Filial Apur", /*cPicture*/, 10                     , /*lPixel*/, {|| FILIAL_APURACAO}, "LEFT") //"FILIAL APURACAO"
TRCell():New(oSec3, "cStatusApur"   , /*cAlias*/, "Status Apur", /*cPicture*/, 15                     , /*lPixel*/, {|| STATUS_APURACAO}, "LEFT") //"STATUS APURACAO"
TRCell():New(oSec3, "cVlrApur"  , /*cAlias*/, "Total Apur", "@E 99,999,999,999.99", TamSX3("G81_VALOR")[1] , /*lPixel*/, {|| VALOR_TOT_APURACAO}, "LEFT") //"Valor Total Apur."

oSec4:= TRSection():New(oReport, STR0040, /*aAlias*/)//"Efetivo a Pagar"
oSec4:SetTotalInLine(.F.)

TRCell():New(oSec4, "cFilial"   , /*cAlias*/, STR0004, /*cPicture*/, TamSX3("G4C_FILIAL")[1], /*lPixel*/, {|| FILIAL}, "LEFT") //"Filial"
TRCell():New(oSec4, "cRV"       , /*cAlias*/, STR0005, /*cPicture*/, TamSX3("G4C_NUMID")[1] , /*lPixel*/, {|| RV}, "LEFT") //"RV"
TRCell():New(oSec4, "cItem"     , /*cAlias*/, STR0041, /*cPicture*/, TamSX3("G4C_IDITEM")[1], /*lPixel*/, {|| ITEM}, "LEFT") //"Item"
TRCell():New(oSec4, "cSeq"      , /*cAlias*/, STR0006, /*cPicture*/, TamSX3("G4C_NUMSEQ")[1], /*lPixel*/, {|| SEQ}, "LEFT") //"Seq."
TRCell():New(oSec4, "cOperacao" , /*cAlias*/, STR0007, /*cPicture*/, 25                     , /*lPixel*/, {|| OPERACAO}, "LEFT") //"Opera��o"
TRCell():New(oSec4, "cClassi"   , /*cAlias*/, STR0008, /*cPicture*/, TamSX3("G4C_CLASS")[1] , /*lPixel*/, {|| CLASSIFICACAO}, "LEFT") //"Classifica��o"
TRCell():New(oSec4, "cCodAco"   , /*cAlias*/, STR0009, /*cPicture*/, TamSX3("G4C_NUMACD")[1], /*lPixel*/, {|| COD_ACORDO}, "LEFT") //"C�d. Acordo"
TRCell():New(oSec4, "cDescrAco" , /*cAlias*/, STR0010, /*cPicture*/, TamSX3("G4W_DESCRI")[1], /*lPixel*/, {|| DESC_ACORD}, "LEFT") //"Descri��o Acordo"
TRCell():New(oSec4, "cCobranca" , /*cAlias*/, STR0011, /*cPicture*/, 15                     , /*lPixel*/, {|| COBRANCA}, "LEFT") //"Cobran�a"
TRCell():New(oSec4, "cCodCli"   , /*cAlias*/, STR0012, /*cPicture*/, TamSX3("G3Q_CLIENT")[1], /*lPixel*/, {|| COD_CLIENTE}, "LEFT") //"C�d. Cliente"
TRCell():New(oSec4, "cLojCli"   , /*cAlias*/, STR0043, /*cPicture*/, TamSX3("G3Q_LOJA")[1]  , /*lPixel*/, {|| LOJA_CLIENTE}, "LEFT") //"Loja Cliente"
TRCell():New(oSec4, "cNomeCli"  , /*cAlias*/, STR0013, /*cPicture*/, TamSX3("A1_NOME")[1]   , /*lPixel*/, {|| NOME_CLIENTE}, "LEFT") //"Nome Cliente"
TRCell():New(oSec4, "cGrpProd"  , /*cAlias*/, STR0014, /*cPicture*/, TamSX3("G4C_GRPPRD")[1], /*lPixel*/, {|| GRP_PROD}, "LEFT") //"Grp. Produto"
TRCell():New(oSec4, "cCodForPrd", /*cAlias*/, STR0015, /*cPicture*/, TamSX3("G3R_FORNEC")[1], /*lPixel*/, {|| COD_FORN_PROD}, "LEFT") //"C�d. Forn. Prod."
TRCell():New(oSec4, "cLojForPrd", /*cAlias*/, STR0044, /*cPicture*/, TamSX3("G3R_LOJA")[1]  , /*lPixel*/, {|| LOJA_FORN_PRO}, "LEFT") //"Loja Forn. Prod."
TRCell():New(oSec4, "cNomForPrd", /*cAlias*/, STR0016, /*cPicture*/, TamSX3("A2_NOME")[1]   , /*lPixel*/, {|| NOME_FORN_PROD}, "LEFT") //"Nome Forn. Prod."
TRCell():New(oSec4, "cTipoDoc"  , /*cAlias*/, STR0017, /*cPicture*/, 15                     , /*lPixel*/, {|| TIPO_DOC}, "LEFT") //"Tipo Doc."
//TRCell():New(oSec4, "cDocumento", /*cAlias*/, STR0018, /*cPicture*/, TamSX3("G4C_DOC")[1]   , /*lPixel*/, {|| }, "LEFT") //"Documento"
TRCell():New(oSec4, "cDataEmis" , /*cAlias*/, STR0019, /*cPicture*/, 8                      , /*lPixel*/, {|| DATA_EMISSAO}, "LEFT") //"Data Emiss�o"
TRCell():New(oSec4, "cDataInclu", /*cAlias*/, STR0020, /*cPicture*/, 8                      , /*lPixel*/, {|| DATA_INCLUSAO}, "LEFT") //"Data Inclus�o"
TRCell():New(oSec4, "cCodFOP"   , /*cAlias*/, STR0021, /*cPicture*/, TamSX3("G3Q_FORMPG")[1], /*lPixel*/, {|| COD_FOP}, "LEFT") //"C�d. FOP"
TRCell():New(oSec4, "cDescriFOP", /*cAlias*/, STR0022, /*cPicture*/, TamSX3("G3N_DESCR")[1] , /*lPixel*/, {|| DESCRICAO_FOP}, "LEFT") //"Descri��o FOP"
TRCell():New(oSec4, "cTipo", /*cAlias*/, "Tipo", /*cPicture*/, TamSX3("G3N_DESCR")[1] , /*lPixel*/, {|| TIPO}, "LEFT") //"TIPO"
TRCell():New(oSec4, "cMoeda"    , /*cAlias*/, STR0042, /*cPicture*/, TamSX3("G5T_DESCR")[1] , /*lPixel*/, {|| MOEDA}, "LEFT") //"Moeda"
TRCell():New(oSec4, "cVlrServ"  , /*cAlias*/, STR0047, "@E 99,999,999,999.99", TamSX3("G4C_VALOR")[1] , /*lPixel*/, {|| VALOR}, "LEFT") //"Valor"
TRCell():New(oSec4, "cTpTarifa" , /*cAlias*/, STR0026, /*cPicture*/, 15                     , /*lPixel*/, {|| TIPO_TARIFA}, "LEFT") //"Tipo Tarifa"
TRCell():New(oSec4, "cDestino"  , /*cAlias*/, STR0027, /*cPicture*/, 15                     , /*lPixel*/, {|| DESTINO}, "LEFT") //"Destino"
TRCell():New(oSec4, "cSistOri"  , /*cAlias*/, STR0028, /*cPicture*/, TamSX3("G4K_DESCR")[1] , /*lPixel*/, {|| SISTEMA_ORIGEM}, "LEFT") //"Sistema Origem"
TRCell():New(oSec4, "cCodForRep", /*cAlias*/, STR0029, /*cPicture*/, TamSX3("G3R_FORREP")[1], /*lPixel*/, {|| FORNEC_REP}, "LEFT") //"Fornecedor Report"
TRCell():New(oSec4, "cLojForRep", /*cAlias*/, STR0045, /*cPicture*/, TamSX3("G3R_LOJREP")[1], /*lPixel*/, {|| LOJA_FORN_REP}, "LEFT") //"Loja Forn. Rep."
TRCell():New(oSec4, "cNomForRep", /*cAlias*/, STR0030, /*cPicture*/, TamSX3("A2_NOME")[1]   , /*lPixel*/, {|| NOME_FORN_REP}, "LEFT") //"Nome Forn. Report"
TRCell():New(oSec4, "cCliFor"   , /*cAlias*/, STR0031, /*cPicture*/, 10                     , /*lPixel*/, {|| CLI_FOR}, "LEFT") //"Cliente / Fornec."
TRCell():New(oSec4, "cCodEntFin", /*cAlias*/, STR0032, /*cPicture*/, TamSX3("G4C_CODIGO")[1], /*lPixel*/, {|| COD_ENT_FIN}, "LEFT") //"Cod. Ent. Financ."
TRCell():New(oSec4, "cLojForRep", /*cAlias*/, STR0046, /*cPicture*/, TamSX3("G4C_LOJA")[1]  , /*lPixel*/, {|| LOJA_ENT_FIN}, "LEFT") //"Loja Ent. Financ."
TRCell():New(oSec4, "cNomEntFin", /*cAlias*/, STR0033, /*cPicture*/, TamSX3("A1_NOME")[1]   , /*lPixel*/, {|| NOME_ENT_FIN}, "LEFT") //"Nome Ent. Financ."
TRCell():New(oSec4, "cStatus", /*cAlias*/, "STATUS", /*cPicture*/, 15  , /*lPixel*/, {|| STATUS}, "LEFT") //"STATUS"
TRCell():New(oSec4, "cDocEfe"   , /*cAlias*/, STR0037, /*cPicture*/, 10                     , /*lPixel*/, {|| DOC_EFETIVACAO}, "LEFT") //"Doc. Efetiva��o"
TRCell():New(oSec4, "cNumDocEfe", /*cAlias*/, STR0038, /*cPicture*/, 10                     , /*lPixel*/, {|| NR_DOC}, "LEFT") //"Nr. Documento"
TRCell():New(oSec4, "cDatDocEfe", /*cAlias*/, STR0039, /*cPicture*/, 10                     , /*lPixel*/, {|| DATA_DOC}, "LEFT") //"Data Documento"
TRCell():New(oSec4, "cDiverg"   , /*cAlias*/, STR0034, /*cPicture*/, 25                     , /*lPixel*/, {|| DIVERGENCIA}, "LEFT") //"Diverg�ncia"
TRCell():New(oSec4, "cApuracao"   , /*cAlias*/, "Apura��o", /*cPicture*/, 10                     , /*lPixel*/, {|| APURACAO}, "LEFT") //"APURACAO"
TRCell():New(oSec4, "cFilApur"   , /*cAlias*/, "Filial Apur", /*cPicture*/, 10                     , /*lPixel*/, {|| FILIAL_APURACAO}, "LEFT") //"FILIAL APURACAO"
TRCell():New(oSec4, "cStatusApur"   , /*cAlias*/, "Status Apur", /*cPicture*/, 15                     , /*lPixel*/, {|| STATUS_APURACAO}, "LEFT") //"STATUS APURACAO"
TRCell():New(oSec4, "cVlrApur"  , /*cAlias*/, "Total Apur", "@E 99,999,999,999.99", TamSX3("G81_VALOR")[1] , /*lPixel*/, {|| VALOR_TOT_APURACAO}, "LEFT") //"Valor Total Apur." 

Return oReport

/*/{Protheus.doc} ReportPrint
Imprime o relat�rio definido pelo usu�rio de acordo com as se��es criadas na fun��o ReportDef.

@author paulo.barbosa
@since 04/07/2017
@version 1.0
/*/

Static Function ReportPrint(oReport, cPerg)
Local oSec1      := oReport:Section(1)
Local oSec2      := oReport:Section(2)
Local oSec3      := oReport:Section(3)
Local oSec4      := oReport:Section(4)
Local aAliasAux  := QueryCtb(MV_PAR01,MV_PAR02,MV_PAR03,MV_PAR04)

//Impress�o das provis�es
If MV_PAR03 != 4
	DbSelectArea(aAliasAux[1])
	While !oReport:Cancel() .And. (aAliasAux[1])->(!EOF())
		If AllTrim(Classif_Contabil) == 'Prov. A Receber'
			oSec1:Init()
			oSec1:PrintLine()
			oSec1:Finish()
		ElseIf AllTrim(Classif_Contabil) == 'Prov. A Pagar'
			oSec2:Init()
			oSec2:PrintLine()
			oSec2:Finish()
		EndIf
		(aAliasAux[1])->(DbSkip())
	EndDo
EndIf

//Impress�o dos efetivos
If MV_PAR04 != 4
	DbSelectArea(aAliasAux[2])
	While !oReport:Cancel() .And. (aAliasAux[2])->(!EOF())
		If AllTrim(Classif_Contabil) == 'Efet. A Receber'
			oSec3:Init()
			oSec3:PrintLine()
				oSec3:Finish()
		ElseIf AllTrim(Classif_Contabil) == 'Efet. A Pagar'
			oSec4:Init()
			oSec4:PrintLine()
			oSec4:Finish()
		EndIf
		(aAliasAux[2])->(DbSkip())
	EndDo
EndIf

Return

/*/{Protheus.doc} QueryCtb
(long_description)
@type function
@author osmar.junior
@since 11/10/2017
@version 1.0
@param ${param}, ${param_type}, ${param_descr}
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function QueryCtb(dDTRef, nTipo, nProv, nEfet)
Local cAliasProv := GetNextAlias()
Local cAliasEfet := GetNextAlias()
Local cExpG5VG48	:= cExpFil('G5V','G5V','G48','G48')
Local cExpG4WG48	:= cExpFil('G4W','G4W','G48','G48')
Local cExpSA1G3Q	:= cExpFil('SA1','SA1','G3Q','G3Q')
Local cExpSA1G4C	:= cExpFil('SA1','SA1G4C')
Local cExpSA2G4C	:= cExpFil('SA2','SA2G4C')
Local cExpSA2G3R	:= cExpFil('SA2','SA2','G3R','G3R')
Local cExpSA2REP	:= cExpFil('SA2','SA2REP','G3R','G3R')
Local cExpG3NG3Q	:= cExpFil('G3N','G3N','G3Q','G3Q')
Local cExpG4KG3R	:= cExpFil('G4K','G4K','G3Q','G3Q')
Local cExpG6I	:= cExpFil('G6I')
Local cExpG5T	:= cExpFil('G5T') 
  
Local cExpTipo		:= ''
Local cExpTpPrv		:= ''
Local cExpTpEfet		:= ''
Local cExpProv		:= "%"
Local cExpEfet		:= "%"

Local cQueryProv	:= ''
Local cQueryEfet	:= ''

Local cExpDtEmi	:= '%' + SuperGetMV( "MV_EMISG6I" , .F. /*lHelp*/, 'G6I.G6I_EMISSA' /*cPadrao*/) + '%'

cExpProv += " G4C.G4C_DTINC <= '" + DTOS(dDTRef) + "'" 
cExpEfet += " G4C.G4C_DTINC <= '" + DTOS(dDTRef) + "'" 

//Tipo: Interm/Acord/Ambos
Do Case
	Case nTipo == 1	//Intermedia��o
	cExpTipo += " AND (G4C.G4C_CLASS = ' ') "	
Case nTipo == 2		//Acordos
	cExpTipo += " AND (G4C.G4C_CLASS <> ' ') " 
Case nTipo == 3		//Ambos
	cExpTipo += "" 	
EndCase	

cExpProv += cExpTipo
cExpEfet += cExpTipo

//Provis�o: A Receber/A Pagar/Ambos/Nenhuma
Do Case
	Case nProv == 1		//A Receber
	cExpTpPrv += " AND G4C.G4C_PAGREC = '2' "	
Case nProv == 2		//A Pagar
	cExpTpPrv += " AND G4C.G4C_PAGREC = '1' "
Case nProv == 3		//Ambos
	cExpTpPrv += ""
EndCase	

//Efetivo: A Receber/A Pagar/Ambos/Nenhum
Do Case
	Case nEfet == 1		//A Receber
	cExpTpEfet += " AND G4C.G4C_PAGREC = '2' " 
Case nEfet == 2		//A Pagar
	cExpTpEfet += " AND G4C.G4C_PAGREC = '1' "
Case nEfet == 3		//Ambos
	cExpTpEfet += "" 
EndCase

cExpProv += cExpTpPrv
cExpEfet += cExpTpEfet

cExpProv += '%'
cExpEfet += '%'


//Efetivo
If nEfet <> 4 //4-N�o Apresentar
	
	BeginSql Alias cAliasEfet
	
		//Column G8C_DTEFET AS Date 
		//Column G6I_EMISSA AS Date 
		Column G84_EMISS  AS Date
		Column G4C_EMISS  AS Date
		Column G4C_DTINC  AS Date 
	
	SELECT * FROM (		
	SELECT	
	G4C.G4C_IDIF 'IDIF',
	CASE  
		WHEN G4C.G4C_PAGREC = '2' THEN 'Efet. A Receber' 
		WHEN G4C.G4C_PAGREC = '1' THEN 'Efet. A Pagar'
	END 'CLASSIF_CONTABIL',		
	G4C.G4C_FILIAL 'FILIAL',
	G4C.G4C_NUMID 'RV',
	G4C.G4C_IDITEM 'ITEM', 
	G4C.G4C_NUMSEQ 'SEQ', 
	CASE G4C.G4C_OPERAC
		 WHEN '1' THEN 'Emissao'
		 WHEN '2' THEN 'Reembolso'
		 WHEN '3' THEN 'Reemissao'
		 WHEN '4' THEN 'Venda Servicos Proprios'
	END 'OPERACAO',		 
	G4C.G4C_CLASS 'CLASSIFICACAO', 
	G4C.G4C_NUMACD 'COD_ACORDO', 
	CASE 
		WHEN G4C.G4C_CLASS = 'V01' THEN 'Servi�o Pr�prio '
		WHEN G4C.G4C_CLASS <> '' AND G4C.G4C_CLIFOR = '1' THEN G5V_DESCRI 
		WHEN G4C.G4C_CLASS <> '' AND G4C.G4C_CLIFOR = '2' THEN G4W_DESCRI 
		ELSE ' '
	END 'DESC_ACORD', 
	CASE WHEN G48.G48_COMSER = '2' THEN 'Na Apura��o' ELSE 'Na Fatura' END 'COBRANCA',  
	G3Q.G3Q_CLIENT 'COD_CLIENTE',
	G3Q.G3Q_LOJA 'LOJA_CLIENTE', 
	SA1.A1_NOME 'NOME_CLIENTE',
	G4C.G4C_GRPPRD 'GRP_PROD',
	G3R.G3R_FORNEC 'COD_FORN_PROD',
	G3R.G3R_LOJA 'LOJA_FORN_PRO',
	SA2.A2_NOME 'NOME_FORN_PROD',
	CASE G3Q.G3Q_TPDOC 
		WHEN '1' THEN 'Voucher' 
		WHEN '2' THEN 'Bilhete' 
		WHEN '3' THEN 'Localizador' 
		WHEN '4' THEN 'Doc Adicional' 
		WHEN '5' THEN 'ADM'
		WHEN '6' THEN 'ACM'
		WHEN '7' THEN 'EMD'
		WHEN '8' THEN 'MCO'
	END 'TIPO_DOC',
	G4C.G4C_EMISS 'DATA_EMISSAO',
	G4C.G4C_DTINC 'DATA_INCLUSAO', 
	G3Q.G3Q_FORMPG 'COD_FOP',	
	G3N.G3N_DESCR 'DESCRICAO_FOP',				
	CASE  
		WHEN G4C.G4C_TIPO = '1' THEN 'Intermedia��o' 
		WHEN G4C.G4C_TIPO = '2' THEN 'Reembolso' 
		WHEN G4C.G4C_TIPO = '3' AND G4C.G4C_CLIFOR = '1' THEN 'Credito Acordo Cliente' 
		WHEN G4C.G4C_TIPO = '3' AND G4C.G4C_CLIFOR = '2' THEN 'Credito Acordo Fornecedor'
		WHEN G4C.G4C_TIPO = '4' AND G4C.G4C_CLIFOR = '1' THEN 'Debito Acordo Cliente' 
		WHEN G4C.G4C_TIPO = '4' AND G4C.G4C_CLIFOR = '2' THEN 'Debito Acordo Fornecedor'
		WHEN G4C.G4C_TIPO = '5' THEN 'Breakage'
	END 'TIPO',
	G5T.G5T_DESCR 'MOEDA',
	G4C.G4C_VALOR 'VALOR',				
	CASE 
		WHEN G4C.G4C_OPERAC = '2' THEN 
			(CASE WHEN G4E_TPTAR = '1' THEN 'Net' ELSE 'Comissionado' END)
		ELSE 
			(CASE WHEN G44_TPTAR = '1' THEN 'Net' ELSE 'Comissionado' END)
	 END 'TIPO_TARIFA',
	CASE WHEN G4C.G4C_DESTIN = '1' THEN 'Nacional' ELSE 'Internacional' END 'DESTINO',
	G4K.G4K_DESCR 'SISTEMA_ORIGEM',
	G3R.G3R_FORREP 'FORNEC_REP',
	G3R.G3R_LOJREP 'LOJA_FORN_REP',
	SA2REP.A2_NOME 'NOME_FORN_REP' ,
	CASE WHEN G4C.G4C_CLIFOR = '1' THEN 'Cliente' ELSE 'Fornecedor' END 'CLI_FOR',
	G4C.G4C_CODIGO 'COD_ENT_FIN',
	G4C.G4C_LOJA 'LOJA_ENT_FIN',
	CASE WHEN G4C.G4C_CLIFOR = '1' THEN SA1G4C.A1_NOME ELSE SA2G4C.A2_NOME END 'NOME_ENT_FIN',
	CASE  
		WHEN G4C.G4C_STATUS = '1' THEN 'Ag. Liberacao' 
		WHEN G4C.G4C_STATUS = '2' THEN 'Ag. Apuracao' 
		WHEN G4C.G4C_STATUS = '3' THEN 'Liberado' 
		WHEN G4C.G4C_STATUS = '4' THEN 'Finalizado'
	END 'STATUS',
	CASE 					
		WHEN G4C.G4C_CONORI <> ' ' AND G4CESP.G4C_IDIF IS NULL THEN 'Efetivo sem Provis�o'
		ELSE ' '	
	END 'DIVERGENCIA',
	G48.G48_CODAPU 'APURACAO',
	G48.G48_FILAPU 'FILIAL_APURACAO',
	CASE  
		WHEN G81.G81_STATUS = '1' THEN 'Liberado' 
		WHEN G81.G81_STATUS = '2' THEN 'Finalizado' 	
		 WHEN G81.G81_STATUS IS NULL and G84.G84_TPFAT IS NULL AND G48.G48_CODAPU IS NOT NULL AND G48.G48_COMSER = '2' AND G48.G48_CODAPU <> ' ' THEN 'Ag. Libera��o'  
	END 'STATUS_APURACAO', 
	G6L.G6L_TOTAL 'VALOR_TOT_APURACAO' ,
	CASE  
		WHEN G84.G84_TPFAT = '1' THEN 'Fatura de Vendas' 
		WHEN G84APU.G84_TPFAT = '2' THEN 'Fatura de Apura��es' 
		WHEN G84.G84_TPFAT = '3' THEN 'Aprova��o de Breakage' 
		WHEN G84.G84_TPFAT = '4' THEN 'Fatura de Cr�dito'	
		WHEN G6I_CONCIL <> ' ' THEN 'Concilia��o A�rea'
		WHEN G8C_CONCIL <> ' ' THEN 'Concilia��o Terrestre'			
	END 'DOC_EFETIVACAO',
	CASE  
		WHEN G84.G84_TPFAT IN ( '1','3','4' ) THEN G84.G84_NUMFAT 
		WHEN G84APU.G84_TPFAT = '2' THEN G84APU.G84_NUMFAT  			
		WHEN G6I_CONCIL <> ' ' THEN G6I.G6I_CONCIL
		WHEN G8C_CONCIL <> ' ' THEN G8C.G8C_CONCIL			
	END 'NR_DOC',
	CASE  
		WHEN G84.G84_TPFAT IN ( '1','3','4' ) THEN G84.G84_EMISS  
		WHEN G84APU.G84_TPFAT = '2' THEN G84APU.G84_EMISS   			
		WHEN G6I_CONCIL <> ' ' THEN %Exp:cExpDtEmi%
		WHEN G8C_CONCIL <> ' ' THEN G8C.G8C_DTEFET			
	END 'DATA_DOC'		
	         		                                                                                                          
	FROM %Table:G4C% G4C 
	
	INNER JOIN %Table:G3Q% G3Q ON
		G4C.G4C_FILIAL = G3Q.G3Q_FILIAL AND
		G4C.G4C_NUMID  = G3Q.G3Q_NUMID AND
		G4C.G4C_IDITEM = G3Q.G3Q_IDITEM AND
		G4C.G4C_NUMSEQ = G3Q.G3Q_NUMSEQ AND
		G4C.G4C_CONINU = G3Q.G3Q_CONINU AND
		G4C.G4C_CONORI = G3Q.G3Q_CONORI	AND
		G3Q.%notDel%			
	INNER JOIN %Table:G3R% G3R ON   
		G4C.G4C_FILIAL = G3R.G3R_FILIAL AND
		G4C.G4C_NUMID  = G3R.G3R_NUMID AND
		G4C.G4C_IDITEM = G3R.G3R_IDITEM AND
		G4C.G4C_NUMSEQ = G3R.G3R_NUMSEQ AND		
		G4C.G4C_CONORI = G3R.G3R_CONORI	AND
		G3R.%notDel%	
	LEFT JOIN %Table:G44% G44 ON
		G4C.G4C_OPERAC <> '2' AND
		G4C.G4C_FILIAL = G44.G44_FILIAL AND
		G4C.G4C_NUMID  = G44.G44_NUMID AND
		G4C.G4C_IDITEM = G44.G44_IDITEM AND
		G4C.G4C_NUMSEQ = G44.G44_NUMSEQ AND
		G4C.G4C_CONINU = G44.G44_CONINU AND
		G4C.G4C_CONORI = G44.G44_CONORI AND
		G44.%notDel%	
	LEFT JOIN %Table:G4E% G4E ON
		G4C.G4C_OPERAC = '2' AND
		G4C.G4C_FILIAL = G4E.G4E_FILIAL AND
		G4C.G4C_NUMID  = G4E.G4E_NUMID AND
		G4C.G4C_IDITEM = G4E.G4E_IDITEM AND
		G4C.G4C_NUMSEQ = G4E.G4E_NUMSEQ AND
		G4C.G4C_CONINU = G4E.G4E_CONINU AND
		G4C.G4C_CONORI = G4E.G4E_CONORI AND
		G4E.%notDel%	
	LEFT JOIN %Table:SA1% SA1 ON 
		%Exp:cExpSA1G3Q%
		G3Q.G3Q_CLIENT = SA1.A1_COD AND
		G3Q.G3Q_LOJA = SA1.A1_LOJA AND
		SA1.%notDel%	
	LEFT JOIN %Table:G4K% G4K ON
		%Exp:cExpG4KG3R%
		G3R.G3R_SISORI = G4K.G4K_CODIGO AND
		G4K.%notDel%	 	
	LEFT JOIN %Table:SA2% SA2 ON 
		%Exp:cExpSA2G3R%
		G3R.G3R_FORNEC = SA2.A2_COD AND 
		G3R.G3R_LOJA = SA2.A2_LOJA AND
		SA2.%notDel%	 
	LEFT JOIN %Table:SA2% SA2REP ON 
		%Exp:cExpSA2REP%
		G3R.G3R_FORREP = SA2REP.A2_COD AND 
		G3R.G3R_LOJREP = SA2REP.A2_LOJA AND
		SA2REP.%notDel%	 	
	LEFT JOIN %Table:G3N% G3N ON 
		%Exp:cExpG3NG3Q%
		G3Q.G3Q_FORMPG = G3N.G3N_CODIGO AND
		G3N.%notDel%					
	LEFT JOIN %Table:SA1% SA1G4C ON
		%Exp:cExpSA1G4C%				
		G4C.G4C_CODIGO = SA1G4C.A1_COD AND
		G4C.G4C_LOJA = SA1G4C.A1_LOJA AND
		SA1G4C.%notDel%	
	LEFT JOIN %Table:SA2% SA2G4C ON
		%Exp:cExpSA2G4C%				
		G4C.G4C_CODIGO = SA2G4C.A2_COD AND
		G4C.G4C_LOJA = SA2G4C.A2_LOJA AND
		SA2G4C.%notDel%		
	LEFT JOIN %Table:G84% G84 ON
		G4C.G4C_CLIFOR = '1' AND
		G4C.G4C_FILFAT = G84.G84_FILIAL AND				
		G4C.G4C_PREFIX = G84.G84_PREFIX AND
		G4C.G4C_NUMFAT = G84.G84_NUMFAT AND
		G84.%notDel%		
	LEFT JOIN %Table:G48% G48 ON
		G4C.G4C_FILIAL = G48.G48_FILIAL AND
		G4C.G4C_NUMID  = G48.G48_NUMID AND
		G4C.G4C_IDITEM = G48.G48_IDITEM AND
		G4C.G4C_NUMSEQ = G48.G48_NUMSEQ AND
		G4C.G4C_CLIFOR = G48.G48_CLIFOR AND
		G4C.G4C_CLASS  = G48.G48_CLASS AND
		G4C.G4C_NUMACD = G48.G48_CODACD AND
		G4C.G4C_APLICA = G48.G48_APLICA AND
		G4C.G4C_CONINU = G48.G48_CONINU AND 
		G4C.G4C_CONORI = G48.G48_CONORI AND 
		G48.%notDel%	
	LEFT JOIN %Table:G81% G81 ON			
		G81.G81_FILIAL = G48.G48_FILAPU AND
		G81.G81_CODAPU = G48.G48_CODAPU AND
		G81.G81_FILREF = G48.G48_FILIAL AND
		G81.G81_CLASS = G48.G48_CLASS AND 
		G81.%notDel%
	LEFT JOIN %Table:G6L% G6L ON	
		G6L_FILIAL=G81_FILIAL AND
		G6L_CODAPU=G81_CODAPU AND	
		G6L.%notDel%	
	LEFT JOIN %Table:G84APU% G84APU ON			
		G81.G81_FILFAT = G84APU.G84_FILIAL AND
		G81.G81_PREFIX = G84APU.G84_PREFIX AND
		G81.G81_NUMFAT = G84APU.G84_NUMFAT	AND
		G84APU.%notDel%	
	LEFT JOIN %Table:G6I% G6I ON
		%Exp:cExpG6I%				
		G4C.G4C_CLIFOR = '2' AND
		G3Q.G3Q_TPSEG = '1' AND
		(G4C.G4C_CLASS = '' OR (G4C.G4C_CLASS <> ' ' AND G4C.G4C_STATUS = '4' AND G4C.G4C_CODAPU = ' ')) AND
		G4C.G4C_NUMID  = G6I.G6I_NUMID AND
		G4C.G4C_IDITEM = G6I.G6I_IDITEM AND
		G4C.G4C_NUMSEQ = G6I.G6I_NUMSEQ AND
		G6I.G6I_SITUAC = '3' AND
		G6I.%notDel%	
	LEFT JOIN %Table:G8C% G8C ON
		G4C.G4C_CLIFOR = '2' AND
		G3Q.G3Q_TPSEG <> '1' AND
		(G4C.G4C_CLASS = '' OR (G4C.G4C_CLASS <> ' ' AND G4C.G4C_STATUS = '4' AND G4C.G4C_CODAPU = ' ')) AND
		G8C.G8C_FILIAL = G3R.G3R_FILCON AND
		G8C.G8C_CONCIL = G3R.G3R_CONCIL	 AND
		G8C.%notDel%	
	LEFT JOIN %Table:G4C% G4CESP ON G4C.G4C_CONORI <> ' ' AND
		G4CESP.G4C_FILIAL = G4C.G4C_FILIAL AND
		G4CESP.G4C_NUMID = G4C.G4C_NUMID AND
		G4CESP.G4C_IDITEM = G4C.G4C_IDITEM AND
		G4CESP.G4C_NUMSEQ = G4C.G4C_NUMSEQ AND
		G4CESP.G4C_CLIFOR = G4C.G4C_CLIFOR AND
		G4CESP.G4C_CLASS = G4C.G4C_CLASS AND
		G4CESP.G4C_NUMACD = G4C.G4C_NUMACD AND
		G4CESP.G4C_CONINU = G4C.G4C_CONORI AND
		G4CESP.G4C_APLICA = G4C.G4C_APLICA AND
		G4CESP.G4C_ENTAD  = G4C.G4C_ENTAD AND 
		G4CESP.G4C_ITRAT = G4C.G4C_ITRAT AND
		G4CESP.%notDel%	
	LEFT JOIN %Table:G5T% G5T ON
		%Exp:cExpG5T%
		G4C.G4C_MOEDA = G5T.G5T_CODIGO AND
		G5T.%notDel%
	LEFT JOIN %Table:G5V% G5V ON 
		%Exp:cExpG5VG48%
		G48.G48_CLIFOR = '1' AND
		G48.G48_CODACD = G5V.G5V_CODACO AND
		G48.G48_CLASS = G5V.G5V_TPFEE AND
		G48.G48_CODREC = G5V.G5V_CODREV AND
		G5V.%notDel%	
	LEFT JOIN %Table:G4W% G4W ON 
		%Exp:cExpG4WG48%
		G48.G48_CLIFOR = '2' AND
		G48.G48_CODACD = G4W.G4W_CODACO AND 
		G48.G48_CLASS = G4W.G4W_CLASSI AND
		G48.G48_CODREC = G4W.G4W_CODREV AND
		G4W.%notDel%		
	WHERE 
		G4C.%notDel% AND									 
		G4C.G4C_CONINU = ' ' AND									 
		%Exp:cExpEfet%    
	) EFETIVO	
		WHERE DATA_DOC <= %Exp:DTOS(dDTRef)%
	
	EndSql
	
	
	If (cAliasEfet)->(!EOF())//While (cAliasAux)->(!EOF())
		cQueryEfet :=GetLastQuery()[2]
		MEMOWRITE('QueryContabilEfet.sql',cQueryEfet)
		//Alert('Gerado arquivo.')
	End//Do
	
EndIf
	
//Provis�o	
If 	nProv <> 4 // 4-N�o Apresentar

	BeginSql Alias cAliasProv
	
		//Column G8C_DTEFET AS Date 
		//Column G6I_EMISSA AS Date 
		Column G84_EMISS  AS Date
		Column G4C_EMISS  AS Date
		Column G4C_DTINC  AS Date 
	
	SELECT * FROM (		
	SELECT	
	G4C.G4C_IDIF 'IDIF',
	CASE  
		WHEN  G4C.G4C_PAGREC = '2' THEN 'Prov. A Receber' 
		WHEN  G4C.G4C_PAGREC = '1' THEN 'Prov. A Pagar' 
	END 'CLASSIF_CONTABIL',		
	G4C.G4C_FILIAL 'FILIAL',
	G4C.G4C_NUMID 'RV',
	G4C.G4C_IDITEM 'ITEM', 
	G4C.G4C_NUMSEQ 'SEQ', 
	CASE G4C.G4C_OPERAC
		 WHEN '1' THEN 'Emissao'
		 WHEN '2' THEN 'Reembolso'
		 WHEN '3' THEN 'Reemissao'
		 WHEN '4' THEN 'Venda Servicos Proprios'
	END 'OPERACAO',		 
	G4C.G4C_CLASS 'CLASSIFICACAO', 
	G4C.G4C_NUMACD 'COD_ACORDO', 
	CASE 
		WHEN G4C.G4C_CLASS = 'V01' THEN 'Servi�o Pr�prio '
		WHEN G4C.G4C_CLASS <> '' AND G4C.G4C_CLIFOR = '1' THEN G5V_DESCRI 
		WHEN G4C.G4C_CLASS <> '' AND G4C.G4C_CLIFOR = '2' THEN G4W_DESCRI 
		ELSE ' '
	END 'DESC_ACORD', 
	CASE WHEN G48.G48_COMSER = '2' THEN 'Na Apura��o' ELSE 'Na Fatura' END 'COBRANCA',  
	G3Q.G3Q_CLIENT 'COD_CLIENTE',
	G3Q.G3Q_LOJA 'LOJA_CLIENTE', 
	SA1.A1_NOME 'NOME_CLIENTE',
	G4C.G4C_GRPPRD 'GRP_PROD',
	G3R.G3R_FORNEC 'COD_FORN_PROD',
	G3R.G3R_LOJA 'LOJA_FORN_PRO',
	SA2.A2_NOME 'NOME_FORN_PROD',
	CASE G3Q.G3Q_TPDOC 
		WHEN '1' THEN 'Voucher' 
		WHEN '2' THEN 'Bilhete' 
		WHEN '3' THEN 'Localizador' 
		WHEN '4' THEN 'Doc Adicional' 
		WHEN '5' THEN 'ADM'
		WHEN '6' THEN 'ACM'
		WHEN '7' THEN 'EMD'
		WHEN '8' THEN 'MCO'
	END 'TIPO_DOC',
	G4C.G4C_EMISS 'DATA_EMISSAO',
	G4C.G4C_DTINC 'DATA_INCLUSAO', 
	G3Q.G3Q_FORMPG 'COD_FOP',	
	G3N.G3N_DESCR 'DESCRICAO_FOP',				
	CASE  
		WHEN G4C.G4C_TIPO = '1' THEN 'Intermedia��o' 
		WHEN G4C.G4C_TIPO = '2' THEN 'Reembolso' 
		WHEN G4C.G4C_TIPO = '3' AND G4C.G4C_CLIFOR = '1' THEN 'Credito Acordo Cliente' 
		WHEN G4C.G4C_TIPO = '3' AND G4C.G4C_CLIFOR = '2' THEN 'Credito Acordo Fornecedor'
		WHEN G4C.G4C_TIPO = '4' AND G4C.G4C_CLIFOR = '1' THEN 'Debito Acordo Cliente' 
		WHEN G4C.G4C_TIPO = '4' AND G4C.G4C_CLIFOR = '2' THEN 'Debito Acordo Fornecedor'
		WHEN G4C.G4C_TIPO = '5' THEN 'Breakage'
	END 'TIPO',
	G5T.G5T_DESCR 'MOEDA',
	G4C.G4C_VALOR 'VALOR',				
	CASE 
		WHEN G4C.G4C_OPERAC = '2' THEN 
			(CASE WHEN G4E_TPTAR = '1' THEN 'Net' ELSE 'Comissionado' END)
		ELSE 
			(CASE WHEN G44_TPTAR = '1' THEN 'Net' ELSE 'Comissionado' END)
	 END 'TIPO_TARIFA',
	CASE WHEN G4C.G4C_DESTIN = '1' THEN 'Nacional' ELSE 'Internacional' END 'DESTINO',
	G4K.G4K_DESCR 'SISTEMA_ORIGEM',
	G3R.G3R_FORREP 'FORNEC_REP',
	G3R.G3R_LOJREP 'LOJA_FORN_REP',
	SA2REP.A2_NOME 'NOME_FORN_REP' ,
	CASE WHEN G4C.G4C_CLIFOR = '1' THEN 'Cliente' ELSE 'Fornecedor' END 'CLI_FOR',
	G4C.G4C_CODIGO 'COD_ENT_FIN',
	G4C.G4C_LOJA 'LOJA_ENT_FIN',
	CASE WHEN G4C.G4C_CLIFOR = '1' THEN SA1G4C.A1_NOME ELSE SA2G4C.A2_NOME END 'NOME_ENT_FIN',
	CASE  
		WHEN G4C.G4C_STATUS = '1' THEN 'Ag. Liberacao' 
		WHEN G4C.G4C_STATUS = '2' THEN 'Ag. Apuracao' 
		WHEN G4C.G4C_STATUS = '3' THEN 'Liberado' 
		WHEN G4C.G4C_STATUS = '4' THEN 'Finalizado'
	END 'STATUS',	
	CASE 					
		WHEN G4C.G4C_CONINU <> ' ' AND G4CPSE.G4C_IDIF IS NULL THEN 'Provis�o sem Efetivo'
		ELSE ' '	
	END 'DIVERGENCIA',
	G48.G48_CODAPU 'APURACAO',
	G48.G48_FILAPU 'FILIAL_APURACAO',
	CASE   
		WHEN G81.G81_STATUS = '1' THEN 'Liberado' 
		WHEN G81.G81_STATUS = '2' THEN 'Finalizado' 	
		 WHEN G81.G81_STATUS IS NULL and G84.G84_TPFAT IS NULL AND G48.G48_CODAPU IS NOT NULL AND G48.G48_COMSER = '2' AND G48.G48_CODAPU <> ' ' THEN 'Ag. Libera��o'   
	END 'STATUS_APURACAO',
	G6L.G6L_TOTAL 'VALOR_TOT_APURACAO' ,
	CASE  
		WHEN G84.G84_TPFAT = '1' THEN 'Fatura de Vendas' 
		WHEN G84APU.G84_TPFAT = '2' THEN 'Fatura de Apura��es' 
		WHEN G84.G84_TPFAT = '3' THEN 'Aprova��o de Breakage' 
		WHEN G84.G84_TPFAT = '4' THEN 'Fatura de Cr�dito'	
		WHEN G6I_CONCIL <> ' ' THEN 'Concilia��o A�rea'
		WHEN G8C_CONCIL <> ' ' THEN 'Concilia��o Terrestre'			
	END 'DOC_EFETICAVAO',
	CASE  
		WHEN G84.G84_TPFAT IN ( '1','3','4' ) THEN G84.G84_NUMFAT 
		WHEN G84APU.G84_TPFAT = '2' THEN G84APU.G84_NUMFAT  			
		WHEN G6I_CONCIL <> ' ' THEN G6I.G6I_CONCIL
		WHEN G8C_CONCIL <> ' ' THEN G8C.G8C_CONCIL			
	END 'NR_DOC',
	CASE  
		WHEN G84.G84_TPFAT IN ( '1','3','4' ) THEN G84.G84_EMISS  
		WHEN G84APU.G84_TPFAT = '2' THEN G84APU.G84_EMISS   			
		WHEN G6I_CONCIL <> ' ' THEN %Exp:cExpDtEmi%
		WHEN G8C_CONCIL <> ' ' THEN G8C.G8C_DTEFET			
	END 'DATA_DOC',
	G4CPSE.G4C_FILFAT 'FILIAL_FAT_REF',
	G4CPSE.G4C_NUMFAT 'NUM_FAT_REF',
	G4CPSE.G4C_PREFIX 'PREFIX_FAT_REF',
	G84AUX.G84_EMISS 'EMISSAO_FAT_REF',
	G84AUX.G84_TOTAL 'TOTAL_FAT_REF'
				         		                                                                                                          
	FROM %Table:G4C% G4C  
	
	INNER JOIN %Table:G3Q% G3Q ON
		G4C.G4C_FILIAL = G3Q.G3Q_FILIAL AND
		G4C.G4C_NUMID = G3Q.G3Q_NUMID AND
		G4C.G4C_IDITEM	= G3Q.G3Q_IDITEM AND
		G4C.G4C_NUMSEQ = G3Q.G3Q_NUMSEQ AND
		G4C.G4C_CONINU = G3Q.G3Q_CONINU AND
		G4C.G4C_CONORI = G3Q.G3Q_CONORI	AND
		G3Q.%notDel%			
	INNER JOIN %Table:G3R% G3R ON   
		G4C.G4C_FILIAL = G3R.G3R_FILIAL AND
		G4C.G4C_NUMID  = G3R.G3R_NUMID AND
		G4C.G4C_IDITEM = G3R.G3R_IDITEM AND
		G4C.G4C_NUMSEQ = G3R.G3R_NUMSEQ AND		
		G4C.G4C_CONORI = G3R.G3R_CONORI	AND
		G3R.%notDel%	
	LEFT JOIN %Table:G44% G44 ON
		G4C.G4C_OPERAC <> '2' AND
		G4C.G4C_FILIAL = G44.G44_FILIAL AND
		G4C.G4C_NUMID  = G44.G44_NUMID AND
		G4C.G4C_IDITEM = G44.G44_IDITEM AND
		G4C.G4C_NUMSEQ = G44.G44_NUMSEQ AND
		G4C.G4C_CONINU = G44.G44_CONINU AND
		G4C.G4C_CONORI = G44.G44_CONORI AND
		G44.%notDel%	
	LEFT JOIN %Table:G4E% G4E ON
		G4C.G4C_OPERAC = '2' AND
		G4C.G4C_FILIAL = G4E.G4E_FILIAL AND
		G4C.G4C_NUMID  = G4E.G4E_NUMID AND
		G4C.G4C_IDITEM = G4E.G4E_IDITEM AND
		G4C.G4C_NUMSEQ = G4E.G4E_NUMSEQ AND
		G4C.G4C_CONINU = G4E.G4E_CONINU AND
		G4C.G4C_CONORI = G4E.G4E_CONORI AND
		G4E.%notDel%	
	LEFT JOIN %Table:SA1% SA1 ON 
		%Exp:cExpSA1G3Q%
		G3Q.G3Q_CLIENT = SA1.A1_COD AND
		G3Q.G3Q_LOJA = SA1.A1_LOJA AND
		SA1.%notDel%	
	LEFT JOIN %Table:G4K% G4K ON
		%Exp:cExpG4KG3R%
		G3R.G3R_SISORI = G4K.G4K_CODIGO AND
		G4K.%notDel%	 	
	LEFT JOIN %Table:SA2% SA2 ON 
		%Exp:cExpSA2G3R%
		G3R.G3R_FORNEC = SA2.A2_COD AND 
		G3R.G3R_LOJA = SA2.A2_LOJA AND
		SA2.%notDel%	 
	LEFT JOIN %Table:SA2% SA2REP ON 
		%Exp:cExpSA2REP%
		G3R.G3R_FORREP = SA2REP.A2_COD AND 
		G3R.G3R_LOJREP = SA2REP.A2_LOJA AND
		SA2REP.%notDel%	 	
	LEFT JOIN %Table:G3N% G3N ON 
		%Exp:cExpG3NG3Q%
		G3Q.G3Q_FORMPG = G3N.G3N_CODIGO AND
		G3N.%notDel%					
	LEFT JOIN %Table:SA1% SA1G4C ON
		%Exp:cExpSA1G4C%				
		G4C.G4C_CODIGO = SA1G4C.A1_COD AND
		G4C.G4C_LOJA = SA1G4C.A1_LOJA AND
		SA1G4C.%notDel%	
	LEFT JOIN %Table:SA2% SA2G4C ON
		%Exp:cExpSA2G4C%				
		G4C.G4C_CODIGO = SA2G4C.A2_COD AND
		G4C.G4C_LOJA = SA2G4C.A2_LOJA AND
		SA2G4C.%notDel%		
	LEFT JOIN %Table:G84% G84 ON
		G4C.G4C_CLIFOR = '1' AND
		G4C.G4C_FILFAT = G84.G84_FILIAL AND				
		G4C.G4C_PREFIX = G84.G84_PREFIX AND
		G4C.G4C_NUMFAT = G84.G84_NUMFAT AND
		G84.%notDel%		
	LEFT JOIN %Table:G48% G48 ON
		G4C.G4C_FILIAL = G48.G48_FILIAL AND
		G4C.G4C_NUMID  = G48.G48_NUMID AND
		G4C.G4C_IDITEM = G48.G48_IDITEM AND
		G4C.G4C_NUMSEQ = G48.G48_NUMSEQ AND
		G4C.G4C_CLIFOR = G48.G48_CLIFOR AND
		G4C.G4C_CLASS  = G48.G48_CLASS AND
		G4C.G4C_NUMACD = G48.G48_CODACD AND
		G4C.G4C_APLICA = G48.G48_APLICA AND
		G4C.G4C_CONINU = G48.G48_CONINU AND 
		G4C.G4C_CONORI = G48.G48_CONORI AND 
		G48.%notDel%	
	LEFT JOIN %Table:G81% G81 ON			
		G81.G81_FILIAL = G48.G48_FILAPU AND
		G81.G81_CODAPU = G48.G48_CODAPU AND
		G81.G81_FILREF = G48.G48_FILIAL AND
		G81.G81_CLASS = G48.G48_CLASS AND 
		G81.%notDel%
	LEFT JOIN %Table:G6L% G6L ON	
		G6L_FILIAL=G81_FILIAL AND
		G6L_CODAPU=G81_CODAPU AND	
		G6L.%notDel%		
	LEFT JOIN %Table:G84APU% G84APU ON			
		G81.G81_FILFAT = G84APU.G84_FILIAL AND
		G81.G81_PREFIX = G84APU.G84_PREFIX AND
		G81.G81_NUMFAT = G84APU.G84_NUMFAT	AND
		G84APU.%notDel%	
	LEFT JOIN %Table:G6I% G6I ON
		%Exp:cExpG6I%				
		G4C.G4C_CLIFOR = '2' AND
		G3Q.G3Q_TPSEG = '1' AND
		(G4C.G4C_CLASS = '' OR (G4C.G4C_CLASS <> ' ' AND G4C.G4C_STATUS = '4' AND G4C.G4C_CODAPU = ' ')) AND
		G4C.G4C_NUMID  = G6I.G6I_NUMID AND
		G4C.G4C_IDITEM = G6I.G6I_IDITEM AND
		G4C.G4C_NUMSEQ = G6I.G6I_NUMSEQ AND
		G6I.G6I_SITUAC = '3' AND
		G6I.%notDel%	
	LEFT JOIN %Table:G8C% G8C ON
		G4C.G4C_CLIFOR = '2' AND
		G3Q.G3Q_TPSEG <> '1' AND
		(G4C.G4C_CLASS = '' OR (G4C.G4C_CLASS <> ' ' AND G4C.G4C_STATUS = '4' AND G4C.G4C_CODAPU = ' ')) AND
		G8C.G8C_FILIAL = G3R.G3R_FILCON AND
		G8C.G8C_CONCIL = G3R.G3R_CONCIL	 AND
		G8C.%notDel%	
	LEFT JOIN %Table:G4C% G4CPSE ON G4C.G4C_CONINU <> ' ' AND 
		G4CPSE.G4C_FILIAL = G4C.G4C_FILIAL AND
		G4CPSE.G4C_NUMID = G4C.G4C_NUMID AND
		G4CPSE.G4C_IDITEM = G4C.G4C_IDITEM AND
		G4CPSE.G4C_NUMSEQ = G4C.G4C_NUMSEQ AND
		G4CPSE.G4C_CLIFOR = G4C.G4C_CLIFOR AND
		G4CPSE.G4C_CLASS = G4C.G4C_CLASS AND
		G4CPSE.G4C_NUMACD = G4C.G4C_NUMACD AND
		G4CPSE.G4C_CONORI = G4C.G4C_CONINU AND
		G4CPSE.G4C_ENTAD = G4C.G4C_ENTAD AND 
		G4CPSE.G4C_ITRAT = G4C.G4C_ITRAT AND
		G4CPSE.%notDel%	
	LEFT JOIN %Table:G5T% G5T ON
		%Exp:cExpG5T%
		G4C.G4C_MOEDA = G5T.G5T_CODIGO AND
		G5T.%notDel%
	LEFT JOIN %Table:G5V% G5V ON 
		%Exp:cExpG5VG48%
		G48.G48_CLIFOR = '1' AND
		G48.G48_CODACD = G5V.G5V_CODACO AND
		G48.G48_CLASS = G5V.G5V_TPFEE AND
		G48.G48_CODREC = G5V.G5V_CODREV AND
		G5V.%notDel%	
	LEFT JOIN %Table:G4W% G4W ON
		%Exp:cExpG4WG48%
		G48.G48_CLIFOR = '2' AND
		G48.G48_CODACD = G4W.G4W_CODACO AND 
		G48.G48_CLASS = G4W.G4W_CLASSI AND
		G48.G48_CODREC = G4W.G4W_CODREV AND
		G4W.%notDel%	
	LEFT JOIN %Table:G84% G84AUX ON
		G4CPSE.G4C_CLIFOR = '1' AND
		G4CPSE.G4C_FILFAT = G84AUX.G84_FILIAL AND				
		G4CPSE.G4C_PREFIX = G84AUX.G84_PREFIX AND
		G4CPSE.G4C_NUMFAT = G84AUX.G84_NUMFAT AND
		G4CPSE.%notDel%		
	WHERE 
		G4C.%notDel% AND
		G4C.G4C_CONORI = ' ' AND									 
		%Exp:cExpProv%   
	) PROVISAO	 
	  
	  
	EndSql
	
	//retirar WHERE DATA_DOC <= %Exp:DTOS(dDTRef)%
	
	If (cAliasProv)->(!EOF())//While (cAliasAux)->(!EOF())
		cQueryProv := GetLastQuery()[2]
		MEMOWRITE('QueryContabilProv.sql',cQueryProv)
		//Alert('Gerado arquivo.')
	End//Do
	
EndIf


 
Return {cAliasProv, cAliasEfet}

Static Function cExpFil(cTabela,cAliasTab,cTabBase,cAliasTabRef)
Local cCompE		:= FWModeAccess(cTabela,1)
Local cCompU		:= FWModeAccess(cTabela,2)
Local cCompF		:= FWModeAccess(cTabela,3)
Local nTam			:= FWSizeFilial()
Local cRet			:= '%%'

Default cAliasTab 		:= cTabela
Default cTabBase 		:= 'G4C'
Default cAliasTabRef 	:= 'G4C'

If cCompE=='C' //Empresa Compartilhada
	nTam -= Len(FWSM0Layout(,1))
EndIf

If cCompU=='C' //Unidade Compartilhada
	nTam -= Len(FWSM0Layout(,2))
EndIf

If cCompF=='C' //Filial Compartilhada
	nTam -= Len(FWSM0Layout(,3))
EndIf

If nTam > 0
	cRet := "%SUBSTRING("+cAliasTab+"."+PrefixoCpo(cTabela)+"_FILIAL,1,"+ALLTRIM(STR(nTam))+") = SUBSTRING("+cAliasTabRef+"."+cTabBase+"_FILIAL,1,"+ALLTRIM(STR(nTam))+") AND%"
EndIf

Return cRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} SetPath()
Trata o caminho do arquivo caso ele seja digitado incorretamente pelo usu�rio

@param    cPath, string, Cont�m o caminho onde o arquivo ser� salvo
@return   cPath, string, Cont�m o caminho onde o arquivo ser� salvo ap�s ser tratado pela fun��o
@author   paulo.barbosa
@since	  13/10/2017
@version  1.0
/*/
//------------------------------------------------------------------------------
Static Function SetPath(cPath)
If !(SubSTR(cPath,len(cPath),1) $ "/\")
	cPath += "\"
EndIf
Return cPath

//------------------------------------------------------------------------------
/*/{Protheus.doc} VerifArq()
Verifica se o arquivo gerado pelo TREPORT j� foi fechado pelo usu�rio

@author   paulo.barbosa
@since	  19/10/2017
@version  1.0
/*/
//------------------------------------------------------------------------------
Static Function VerifArq(oReport)
Local lRet       := .F.
Local nHdl       := 0

While !lRet
	//Sleep antes para evitar abrir o arquivo antes do proprio TREPORT
	sleep(1000)
	If ( nHdl := FOPEN(GetTempPath() + oReport:cFile, 16) ) != -1
		lRet := .T.
	EndIf
EndDo
FCLOSE(nHdl)

Return lRet