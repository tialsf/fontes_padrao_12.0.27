#INCLUDE "TURXCLASSI.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE 'FWMVCDEF.CH'

//==================================================================================================================
/*{Protheus.doc}TURXCLASSI()

INSERE REGISTROS NA TABELA G8B (Classifica��o de Acordos)

@sample 	TURXCLASS1()
@return  	lRet                       
@author  	Fanny Mieko Suzuki
@since   	12/08/2015
@version  	P12
*/
//==================================================================================================================

Function TURXCLASSI()

Local lRet			:= .T.
Local aArea		:= GetArea()
Local nX			:= 0
Local cQuery    	:= ""
Local aCargaG8B	:= {	{"C01", STR0006, "1", "1", "06", "G3Q_FEE"   , "1"},; // Transaction
						 	{"C02", STR0007, "1", "1", "00", ""          , "2"},; // Flat
							{"C03", STR0008, "1", "1", "00", ""          , "2"},; // Management
							{"C04", STR0009, "1", "1", "00", ""          , "2"},; // Sucess
							{"C05", STR0010, "1", "1", "06", "G3Q_FEE"   , "2"},; // Combo-Transaction
							{"C06", STR0011, "1", "1", "06", "G3Q_TAXAAG", "1"},; // Taxa Ag�ncia
							{"C07", STR0012, "1", "2", "04", "G3Q_REPASS", "1"},; // Repasse
							{"C08", STR0013, "1", "2", "04", "G3Q_DESC"  , "1"},; // Desconto
							{"C09", STR0014, "1", "1", "01", "G3Q_MARKUP", "1"},; // Markup
							{"C10", STR0028, "1", "1", "06", "G3Q_TAXAAG", "1"},; // Taxa Reembolso
							{"C11", STR0020, "1", "1", "01", "G3Q_TAXADU", "1"},; // Taxa DU
							{"C12", STR0029, "1", "1", "06", "G3Q_TAXAAG", "1"},; // Cr�dito N�o Tribut�vel
							{"V01", STR0018, "1", "1", "00", ""          , "2"},; // Servi�o Pr�prio "
							{"F01", STR0015, "2", "1", "03", "G3R_VLCOMI", "1"},; // Comiss�o
							{"F02", STR0016, "2", "1", "04", "G3R_VLINCE", "1"},; // Incentivo Fixo
							{"F03", STR0019, "2", "2", "02", "G3R_TXFORN", "1"},; // Taxa Fornec.
							{"M01", STR0017, "2", "1", "00", ""          , "2"},; // Meta (Volume Vendas)	
							{"M02", STR0024, "2", "1", "00", ""          , "2"},; // Meta (Transa��o)	
							{"M03", STR0025, "2", "1", "00", ""          , "2"},; // Meta (Market Share)	
							{"M04", STR0026, "2", "1", "00", ""          , "2"}}  // Meta (Crescimento)			

    
//Monta array da consulta
dbSelectArea("G8B")
G8B->(dbSetOrder(1))
For nX := 1 To Len(aCargaG8B)
	If !G8B->(dbSeek(xFilial("G8B") + aCargaG8B[nX][1]))
		RecLock("G8B", .T.)
		G8B->G8B_FILIAL 	:= xFilial("G8B")
		G8B->G8B_CODIGO 	:= aCargaG8B[nX][1]
		G8B->G8B_DESCRI 	:= aCargaG8B[nX][2]
		G8B->G8B_CLIFOR 	:= aCargaG8B[nX][3]
		G8B->G8B_TIPO 	:= aCargaG8B[nX][4]
		G8B->G8B_NIVEL 	:= aCargaG8B[nX][5]
		G8B->G8B_DESTIN 	:= aCargaG8B[nX][6]
		G8B->G8B_APLCRV	:= aCargaG8B[nX][7]
		G8B->(MsUnlock())  
	ElseIf Empty(G8B->G8B_APLCRV)
		RecLock("G8B", .F.)
		G8B->G8B_APLCRV := aCargaG8B[nX][7]
		G8B->(MsUnlock())  
	EndIf
Next nX
G8B->(dbCloseArea())

RestArea( aArea )

Return lRet




