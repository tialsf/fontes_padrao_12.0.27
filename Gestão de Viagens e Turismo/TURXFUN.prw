#INCLUDE 'TOTVS.CH'
#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'TURXFUN.CH'

#DEFINE TP_LOGIC_RET	1 // (.T. , .F.)
#DEFINE TP_CHAR1_RET	2 // ("1" = Sim, "2" = N�o"
#DEFINE TP_CHAR2_RET	3 // ("SIM",N�O") 
#DEFINE TP_CHAR3_RET	4 // ("TRUE","FALSE")
#DEFINE TP_CHAR4_RET	5 // ("VERDADEIRO","FALSO")
#DEFINE TP_NUMB_RET 	6 // ( 1 = SIM, 2 = N�O)

Static cFilAuxAnt	:= ""
Static cFilAuxFat	:= ""
Static cFilPst 		:= ""
Static cCodPst 		:= ""
Static cTurApura    := ""
Static cTurRetPath	:= ""	//Diret�rio retonardo pega fun��o cGetFile(..) em Consulta Espec�fica (F3)
Static cTurNaturez	:= "" 	//Natureza utilizada na gera��o da NFS

Static aTurTables	:= {}
Static aTurMultFil	:= {}
Static aTurSChrHtm	:= {} 
Static aTurPxlsStr	:= {}
Static aSetCpos		:= {}
Static aTblCods     := {{'AC8', 'AC8_CODCON'}, ; 	// CONTACTRELATIONSHIP (CONTATO X ENTIDADE)
				        {'ACY', 'ACY_GRPVEN'}, ; 	// COSTUMERGROUP (GRUPO DE CLIENTES)
				        {'AGT', 'AGT_CODIGO'}, ; 	// CORPORATEGROUP (GRUPO DE SOCIETARIOS)
				        {'G3A', 'G3A_CODIGO'}, ; 	// TRAVELRATE (CADASTRO TAXAS)
				        {'G3B', 'G3B_CODIGO'}, ; 	// PASSENGERTERMINAL (TERMINAL DE PASSAGEIROS)
				        {'G3C', 'G3C_CODIGO'}, ; 	// SERVICECLASS (CLASSE DE SERVICOS)
				        {'G3D', 'G3D_CODIGO'}, ; 	// AGENCYCREDITCARD (CARTOES DE CREDITO TURISMO)
				        {'G3E', 'G3E_CODIGO'}, ; 	// ADDITIONALENTITYTYPE (TIPOS DE ENTIDADES)
				        {'G3F', 'G3F_TIPO'  }, ; 	// ADDITIONALENTITY (ENTIDADE ADICIONAL X CLIENTE)
				        {'G3G', 'G3G_ITEM'  }, ; 	// ADDITIONALENTITY (ENTIDADE ADICIONAL X CLIENTE X ITEM)
				        {'G3H', 'G3H_CODAGE'}, ; 	// TRAVELAGENT (AGENTES DE VIAGENS)
				        {'G3J', 'G3J_CODIGO'}, ; 	// CUSTOMERCREDITCARD (CARTOES DE CREDITO DO CLIENTE)
				        {'G3M', 'G3M_CODIGO'}, ; 	// SERVICESTATION (CADASTRO DE POSTO DE ATENDIMENTO)
				        {'G3N', 'G3N_CODIGO'}, ; 	// TRAVELPAYMENTTERM (FORMAS DE PAGAMENTO)
				        {'G3O', 'G3O_ITEM'  }, ; 	// TRAVELPAYMENTTERM (FORMAS DE PAGAMENTO X GRUPOS DE PRODUTO)
				        {'G3P', 'G3P_NUMID' }, ; 	// SALEREGISTRY (REGISTRO DE VENDA - CABE�ALHO)
				        {'G3Q', 'G3Q_IDITEM'}, ; 	// SALEREGISTRY (REGISTRO DE VENDA - ITEM DE VENDA)
				        {'G3R', 'G3R_IDITEM'}, ; 	// SALEREGISTRY (REGISTRO DE VENDA - DOCUMENTO DE RESERVA)
				        {'G3S', 'G3S_CODPAX'}, ; 	// SALEREGISTRY (REGISTRO DE VENDA - PASSAGEIRO)
				        {'G3T', 'G3T_ID'    }, ; 	// SALEREGISTRY (REGISTRO DE VENDA - AEREO)
				        {'G3U', 'G3U_ID'    }, ; 	// SALEREGISTRY (REGISTRO DE VENDA - HOTEL)
				        {'G3V', 'G3V_ID'    }, ; 	// SALEREGISTRY (REGISTRO DE VENDA - CARRO)
				        {'G3W', 'G3W_ID'    }, ; 	// SALEREGISTRY (REGISTRO DE VENDA - RODOVIARIO)
				        {'G3X', 'G3X_ID'    }, ; 	// SALEREGISTRY (REGISTRO DE VENDA - TREM)
				        {'G3Y', 'G3Y_ID'    }, ; 	// SALEREGISTRY (REGISTRO DE VENDA - CRUZEIRO)
				        {'G3Z', 'G3Z_ID'    }, ; 	// SALEREGISTRY (REGISTRO DE VENDA - PACOTE)
				        {'G40', 'G40_ID'    }, ; 	// SALEREGISTRY (REGISTRO DE VENDA - TOUR)
				        {'G41', 'G41_ID'    }, ; 	// SALEREGISTRY (REGISTRO DE VENDA - SEGURO)
				        {'G42', 'G42_ID'    }, ; 	// SALEREGISTRY (REGISTRO DE VENDA - VISTO)
				        {'G43', 'G43_ID'    }, ; 	// SALEREGISTRY (REGISTRO DE VENDA - OUTROS)
				        {'G45', 'G45_SEQ'   }, ;	// SALEREGISTRY (REGISTRO DE VENDA - TARIFAS COTADAS)
				        {'G46', 'G46_SEQTAX'}, ; 	// SALEREGISTRY (REGISTRO DE VENDA - TAXAS)
				        {'G47', 'G47_SEQ'   }, ; 	// SALEREGISTRY (REGISTRO DE VENDA - EXTRAS)
				        {'G49', 'G49_SEQ'   }, ; 	// SALEREGISTRY (REGISTRO DE VENDA - IMPOSTOS)
				        {'G4D', 'G4D_SEQ'   }, ; 	// SALEREGISTRY (REGISTRO DE VENDA - DADOS DO CARTAO)
				        {'G4F', 'G4F_CODIGO'}, ; 	// VISATYPE (TIPOS DE VISTO)
				        {'G4G', 'G4G_CODIGO'}, ; 	// INSURANCEPLAN (PLANOS DE SEGUROS)
				        {'G4H', 'G4H_CODIGO'}, ; 	// SHIP (NAVIOS)
				        {'G4I', 'G4I_CODIGO'}, ; 	// CABINTYPE (TIPOS DE CABINE)
				        {'G4J', 'G4J_CODIGO'}, ; 	// APARTMENTTYPE (TIPOS DE APARTAMENTO)
				        {'G4K', 'G4K_CODIGO'}, ; 	// FRONTSYSTEM (SISTEMAS DE ORIGEM)
				        {'G4L', 'G4L_CODIGO'}, ; 	// TRAVELCUSTOMER (CADASTRO DE COMPLEMENTO DE CLIENTE)
				        {'G4R', 'G4R_FORNEC'}, ; 	// TRAVELVENDOR (CADASTRO DE COMPLEMENTO DE FORNECEDOR)
				        {'G4Y', 'G4Y_CODIGO'}, ; 	// VEHICLECATEGORY (CATEGORIAS DE VEICULOS)
				        {'G4Z', 'G4Z_CODIGO'}, ; 	// VEHICLETYPE (CADASTRO DE TIPOS DE VEICULO)
				        {'G50', 'G50_CODIGO'}, ; 	// DIRECTIONTYPE (TRANSMISSAO/DIRECAOVEICULO)
				        {'G51', 'G51_CODIGO'}, ; 	// FUELTYPE (TIPOS DE COMBUSTIVEL/AR
				        {'G5M', 'G5M_CODIGO'}, ; 	// VENDORGROUP (GRUPOS DE FORNECEDORES)
				        {'G5O', 'G5O_CODIGO'}, ; 	// BROADCASTTYPE (TIPO EMISSAO)
				        {'G5R', 'G5R_CODIGO'}, ; 	// TAX (CADASTRO DE IMPOSTOS TURISMO)
				        {'G5S', 'G5S_CODIGO'}, ; 	// CITY (CADASTRO DE CIDADES TURISMO)
				        {'G5T', 'G5T_CODIGO'}, ; 	// CURRENCY (CADASTRO DE MOEDAS TURISMO)
				        {'G8M', 'G8M_CODIGO'}, ; 	// ROADLINESTRETCH (CADASTRO DE LINHAS E TRECHOS)
				        {'G8N', 'G8N_CODIGO'}, ; 	// ROADLINESTRETCH (CADASTRO DE LINHAS E TRECHOS - ITEM)
				        {'G8O', 'G8O_CODIGO'}, ; 	// BROKERSYSTEM (SISTEMAS DE BROKER)
				        {'G8P', 'G8P_CODIGO'}, ; 	// REFUNDREASON (MOTIVO DE REEMBOLSO)
				        {'G8Q', 'G8Q_CODIGO'}, ;	// AGENCYCREDITCARD (CARTOES DE CREDITO TURISMO - CLASSIFICA��O DE CART�ES)
				        {'SA1', 'A1_COD'    }, ; 	// CUSTOMERVENDOR (CLIENTES)
				        {'SA2', 'A2_COD'    }, ; 	// CUSTOMERVENDOR (FORNECEDORES)
				        {'SB1', 'B1_COD'    }, ; 	// ITEM (PRODUTOS)
				        {'SBM', 'BM_GRUPO'  }, ; 	// FAMILY (GRUPO DE PRODUTOS)
				        {'SU5', 'U5_CODCONT'}} 		// CONTACT (CONTATOS)

Static _TURDIRRET   := ""
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TRF3SX3
Fun��o utilizada que exibe uma tela com os dados de campos na SX3 de um Alias informado
para se ser utilizado em pesquisas padr�es espec�ficas.

@sample	TRF3SX3(cAlias)
@param 		cAlias - String com o Alias da Entidade que se deseja exibir os campos para sele��o
@return	lRet - Vari�vel l�gica, indica se a opera��o ocorreu com sucesso (.T.) ou n�o (.F.) 
@author	Elton Teodoro Alves
@since		13/05/2015
@version 	P12.1.5
/*/
//------------------------------------------------------------------------------------------
Function TRF3SX3(cAlias)

Local	aCpos	:= {}  //Array com os dados
Local	aRet	:= {}  //Array do retorno da opcao selecionada
Local	oDlg	:= Nil //Objeto Janela
Local	oLbx	:= Nil //Objeto List box
Local	cTitulo	:= STR0004  // Campos do sitema
Local	lRet	:= .F.
Local	aArea	:= GetArea()

DbSelectArea( "SX3" )
SX3->( DbSetOrder( 1 ) )
SX3->( DbSeek( cAlias ) )

Do While ! SX3->( Eof() ) .And. X3_ARQUIVO == cAlias
	
	If X3USO( SX3->X3_USADO ) .And. SX3->X3_CONTEXT <> "V"
		aAdd( aCpos, { SX3->X3_CAMPO, X3Titulo() , X3Descric() } )
	EndIf
	
	SX3->( DbSkip() )
	
End Do

If Len( aCpos ) > 0
	
	DEFINE MSDIALOG oDlg TITLE cTitulo FROM 0 , 0 TO 240 , 500 PIXEL
	
	@ 10 , 10 LISTBOX oLbx FIELDS HEADER STR0001 , STR0002 , STR0003  SIZE 230 , 95 OF oDlg PIXEL // CAMPO , TITULO , DESCRICAO
	
	oLbx:SetArray( aCpos )
	oLbx:bLine		:= { || { aCpos[oLbx:nAt, 1], aCpos[oLbx:nAt, 2], aCpos[oLbx:nAt, 3] } }
	oLbx:bLDblClick	:= { || { oDlg:End(), aRet := { oLbx:aArray[oLbx:nAt, 1], oLbx:aArray[oLbx:nAt, 2], oLbx:aArray[oLbx:nAt,3] } } }

	DEFINE SBUTTON FROM 107 , 213 TYPE 1 ACTION ( oDlg:End(), aRet := { oLbx:aArray[oLbx:nAt, 1], oLbx:aArray[oLbx:nAt, 2], oLbx:aArray[oLbx:nAt, 3] } ) ENABLE OF oDlg
	ACTIVATE MSDIALOG oDlg CENTER
	
	If Len( aRet ) > 0
		lRet :=	.T.
		SX3->( dbSetOrder( 2 ) )
		SX3->( dbSeek( aRet[ 1 ] ) )
	End If

End If

RestArea( aArea )

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TRX3TITDSC

Fun��o utilizada para retornar o t�tulo ou a descri��o de um campo
esta fun��o tem a finalidade de ser utilizada em inicializadores de campo e/ou browser

@sample	TRX3TitDsc( cCampo , nTipo)
@param 		cCampo - String com o Nome t�cnico do campo
			nTipo - Inteiro com o Tipo de Retorno desejado -> 1 = Titulo do Campo , 2 = Descricao do Campo
@return 	cRet - String com o T�tulo ou Descri��o do campo
@author 	Elton Teodoro Alves
@since 		13/05/2015
@version 	P12.1.5
/*/
//------------------------------------------------------------------------------------------
Function TRX3TitDsc( cCampo , nTipo)
	
Local cRet    := ''
Local aArea   := GetArea()
Default nTipo := 1

DbSelectArea( 'SX3' )
DbSetOrder( 2 )

If DbSeek( cCampo )
	If nTipo == 1
		cRet := X3Titulo()
	Else
		cRet := X3Descric()
	End If
End If

RestArea( aArea )

Return cRet

/*/{Protheus.doc} TRX2Name
Fun��o que retorna a descri��o da tabela
@author elton.alves
@since 16/09/2015
@version 1.0
@param cAlias, Caracter, Alias da tabela
@return Caracter, Nome da tabela
/*/
Function TRX2Name( cAlias )
	
	Local cRet := ''
	
	If SX2->( DbSeek( cAlias ) )
		
		cRet := Capital( X2Nome() )
		
	End If
	
Return cRet
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURXMAKEID

Fun��o que retorna o InternalId da Entidade de acordo com o valor interno gerado

@sample 	TURAMakeId(cId, cAlias, cEmp, cFil)
@param		cId - String com o C�digo da Entidade
			cAlias - String com o Alias da Entidade
			cEmp - String com o C�digo da Empresa da integra��o  
			cFil - String com o C�digo da Filial da integra��o, se vier vazio pega a filial logada  
@return   	cRet - String com a Chave completa da Entidade                           
@author	Jacomo Lisa
@since		20/09/2015
@version	P12.1.7
/*/
//------------------------------------------------------------------------------------------
Function TURXMakeId(cId, cAlias, cEmp, cFil)

Local   cRet := ""
Default cEmp := cEmpAnt
Default cFil := xFilial(cAlias)
If !Empty(cId)
	cRet := RTrim(cEmp) + '|' + RTrim(cFil) + '|' + RTrim(cId)
Endif

Return cRet
	
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURXRETID

Fun��o que retorna o InternalId da Entidade de acordo com o valor externo recebido

@sample 	TURXRETID(cMarca, cAlias, cCampo, cExtID, cIntID, nOrdem)
@param		cMarca - String com a Marca da integra��o
			cAlias - String com o Alias da Entidade
			cCampo - String com o nome do Campo que ser� verificado no DE/PARA 
			cExtId - String com a Chave EXTERNA da entidade 
			cIntId - String com a Chave INTERNA da entidade 
			nOrdem - Inteiro onde � informada a posi��o do Array que dever� ser retornada
@return   	aRet - Array com os valores que comp�em a chave da entidade                          
@author	Jacomo Lisa
@since		20/09/2015
@version	P12.1.7
/*/
//------------------------------------------------------------------------------------------
Function TURXRetId(cMarca, cAlias, cCampo, cExtID, cIntID, nOrdem, cErro)

Local aRet		 := NIL
Default nOrdem := 0
cErro		:= ""
cIntID := AllTrim(CFGA070INT(cMarca, cAlias, cCampo, cExtID))
If !Empty(cIntID)
	If nOrdem > 0
		aRet := Alltrim(Separa(cIntID, '|')[nOrdem])
	Else
		aRet := Separa(cIntID, '|' )
	EndIf
Else
	cErro := Alltrim(TRX2Name( cAlias ))+': '+cExtID + STR0039//' N�o encontrado no D�/Para'
Endif

Return aRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURXLOGIC

Fun��o que retorna o respectivo valor de campos utilizados como L�GICO / BOOLEANO

@sample 	TURXLogic(xValue, nTypeRet)
@param		xValue - Vari�vel que pode ser String/L�gica/Inteiro com valor a ser comparado
			nTypeRet - Inteiro que informa o tipo de retorno  
@return   	xRet                           
@author	Jacomo Lisa
@since		20/09/2015
@version	P12.1.7
/*/
//------------------------------------------------------------------------------------------
Function TURXLogic(xValue, nTypeRet)

Local xRet

If ValType(xValue) == "C"
	If upper(xValue) == "SIM" .Or. xValue == "1" .Or. upper(xValue) == "TRUE" .Or. upper(xValue) == "VERDADEIRO"
		if nTypeRet == TP_LOGIC_RET
			xRet := .T.
		ElseIf nTypeRet == TP_CHAR1_RET
			xRet := "1"
		ElseIf nTypeRet == TP_CHAR2_RET
			xRet := "SIM"
		ElseIf nTypeRet == TP_CHAR3_RET
			xRet := "TRUE"
		ElseIf nTypeRet == TP_CHAR4_RET
			xRet := "VERDADEIRO"
		Else
			xRet := 1
		Endif
	Else
		if nTypeRet == TP_LOGIC_RET
			xRet := .F.
		ElseIf nTypeRet == TP_CHAR1_RET
			xRet := "2"
		ElseIf nTypeRet == TP_CHAR2_RET
			xRet := "N�O"
		ElseIf nTypeRet == TP_CHAR3_RET
			xRet := "FALSE"
		ElseIf nTypeRet == TP_CHAR4_RET
			xRet := "FALSO"
		Else
			xRet := 2
		Endif
	Endif
	
ElseIf ValType(xValue) == "L"
	IF xValue
		if nTypeRet == TP_LOGIC_RET
			xRet := .T.
		ElseIf nTypeRet == TP_CHAR1_RET
			xRet := "1"
		ElseIf nTypeRet == TP_CHAR2_RET
			xRet := "SIM"
		ElseIf nTypeRet == TP_CHAR3_RET
			xRet := "TRUE"
		ElseIf nTypeRet == TP_CHAR4_RET
			xRet := "VERDADEIRO"
		Else
			xRet := 1
		Endif
	Else
		if nTypeRet == TP_LOGIC_RET
			xRet := .F.
		ElseIf nTypeRet == TP_CHAR1_RET
			xRet := "2"
		ElseIf nTypeRet == TP_CHAR2_RET
			xRet := "N�O"
		ElseIf nTypeRet == TP_CHAR3_RET
			xRet := "FALSE"
		ElseIf nTypeRet == TP_CHAR4_RET
			xRet := "FALSO"
		Else
			xRet := 2
		Endif
	Endif

ElseIf ValType(xValue) == "N"
	IF xValue == 1 
		if nTypeRet == TP_LOGIC_RET
			xRet := .T.
		ElseIf nTypeRet == TP_CHAR1_RET
			xRet := "1"
		ElseIf nTypeRet == TP_CHAR2_RET
			xRet := "SIM"
		ElseIf nTypeRet == TP_CHAR3_RET
			xRet := "TRUE"
		ElseIf nTypeRet == TP_CHAR4_RET
			xRet := "VERDADEIRO"
		Else
			xRet := 1
		Endif
	Else
		if nTypeRet == TP_LOGIC_RET
			xRet := .F.
		ElseIf nTypeRet == TP_CHAR1_RET
			xRet := "2"
		ElseIf nTypeRet == TP_CHAR2_RET
			xRet := "N�O"
		ElseIf nTypeRet == TP_CHAR3_RET
			xRet := "FALSE"
		ElseIf nTypeRet == TP_CHAR4_RET
			xRet := "FALSO"
		Else
			xRet := 2
		Endif
	Endif
Endif

Return xRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURXCPFOR

Fun��o utilizada no inicializador padr�o dos campos ???_FORNEC e ???_LOJA
para trazer o C�digo/Loja do Fornecedor do registro posicionado quando for a opera��o de COPIAR. 

@sample 	TURXCPFOR(cCampo)
@param		cAlias - String - nome da tabela 		
			cCampo - String - nome campo que ter� o valor copiado
@return 	cRet - String - conte�do do campo  
@author 	Thiago Tavares
@since		03/11/2015
@version	P12.1.7
/*/
//------------------------------------------------------------------------------------------
Function TURXCPFOR(cAlias, cCampo)
	
Local cRet   := ''
local oModel := FwModelActive()

If oModel:IsCopy()
	cRet := (cAlias)->&(cCampo)
EndIf

Return cRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TXDTSTAMP

Fun��o que retorna o respectivo valor de campos utilizados como L�GICO / BOOLEANO

@sample 	TxDtStamp(xInfo, lTipo, lOnlyDate)
@param		xInfo		- Vari�vel que pode ser String/L�gica/Inteiro com valor a ser comparado
			lTipo		- Se .T. xRet := Formato "AAAA-MM-DDt00:00:00-03:00", Se .F. xRet := "DD/MM/AAAA"
			lOnlyDate	- Define se o retorno ser� s� com Data ou Junto com o Horario (Apenas para lTipo = .T.)
@return   	xRet		- Se lTipo = .T. xRet := Formato "AAAA-MM-DDt00:00:00-03:00", Se .F. xRet := "DD/MM/AAAA"
@author	Jacomo Lisa
@since		11/11/2015
@version	P12
/*/
//------------------------------------------------------------------------------------------
Function TxDtStamp(xInfo, lTipo, lOnlyDate)

Local xRet  := ""
Local nType := 3
Local cTime := Time()

Default lTipo     := .T.
Default xInfo     := Date()
Default lOnlyDate := .T.

//Tipo = .T. -> Funcionalidade normal do TimeStampo. Retorno no formato "AAAA-MM-DDt00:00:00-03:00"
If !Empty(xInfo)
	If lTipo
	   
	   If ValType(xInfo) == "C"
	       xInfo := cToD(xInfo)
	   EndIf
	
	   xRet := FWTimeStamp( nType, xInfo, cTime )
	   
	   If lOnlyDate // Tratamento para retornar somente "AAAA-MM-DD"
	      xRet := SubStr(xRet , 1, At("T", xRet)-1 )
	   EndIf
	
	//Tipo = .F. -> Inverte o formato de TimeStamp para data Normal. Retorna uma data   
	Else
	   
	   //Pegando a data:
	   If At("T", xInfo) > 0
	      xInfo := SubStr(xInfo , 1, At("T", xInfo)-1 ) //"2011-12-20T00:00:00-03:00" -> "2011-12-20"
	   EndIf
	   
	   //Retirando os '-':
	   xInfo := StrTransf(xInfo,"-","") //"2011-12-20" -> "20111220"
	   
	   //Formatando a data
	   xRet := SToD(xInfo) //"20111220" -> "20/12/2011"
	   
	EndIf
Else
	xRet := If(!lTipo,SToD(xInfo),"")
EndIf

Return xRet

/*/{Protheus.doc} TRGeraParcelas
Retona o array com as parcelas calculadas.
@author Elton Teodoro Alves
@since 20/07/2015
@version 1.0
@param dData, Data, Data de Fechamento
@param nValor, Numero, Valor da Fatura
@param nQtdPar, Numero, Quantidade de Parcelas
@param nQtdDia, Numero, Quantidade de dias de intervala entre as parcelas
@param nQtDiaT, Numero, Quantidade de dias de transsito a ser acrescentado na primeira parcela
@param cContag, Caractere, Indica o tipo de contagem 1=Dias Corridos / 2=Dias �teis
@param cFeriad, Caractere, Indica o tipo de tratamento quando vencimento � feriado 1=Mant�m / 2=Antecipa / 3=Posterga
@param cIncCnt, Caracter, Indica o in�cio da contagem do prazo da primeira parcela 1=Fechamento / 2=Fora Semana / 3=For a M�s
@Param aDiasFech, Array, Cont�m os dias aptos para fechamento
@param aExcecoes, Array, Cont�m a lista de exce��es de datas de vencimento
@return Array Retorna lista de array�s de duas posi��es que indicam as parcelas geradas, onde a primeira posi��o � a data de vencimento da parcela e a outra o valor.
/*/
Function TRGeraParcelas( dData, nValor, nQtdPar, nQtdDia, nQtDiaT, cContag, cFeriad, cIncCnt, aDiasFech, aExcecoes )
	
	Local aRet      := {}
	Local aParcelas := {}
	Local lIsFat    := .F.
	Local aDow      := { 'DIADOM', 'DIASEG', 'DIATER', 'DIAQUA', 'DIAQUA', 'DIASEX', 'DIASAB' }
	Local nVlrParc  := nValor / nQtdPar
	Local nVlrDif   := 0
	Local dDtCont   := cTod('  /  /  ')
	Local dDtVenc   := cTod('  /  /  ')
	Local nX        := 0
	
	// Calcula o valor das parcelas
	If SuperGetMv( 'MV_ARRDPRC',.F.,"1" ) = '1'
		
		nVlrParc := Round( nVlrParc, 2 )
		
	Else
		
		nVlrParc := NoRound( nVlrParc, 2 )
		
	EndIf
	
	// Calcula se h� diferen�a dpo somat�rio das parcelas com o valor total de faturamento
	nVlrDif := nValor - ( nVlrParc * nQtdPar )
	
	//+--------------------------------------------+
	//| Verificando se data � apta para fechamento |
	//+--------------------------------------------+
	
	// Verifica se o dia � uma das datas Flegadas
	lIsFat	:= aDiasFech[ aScan( aDiasFech, { | X | X[ 1 ] == 'G65_DIAM' + cValToChar( Day( dData ) ) } ) ][ 2 ]
	
	// Verifica se � o �ltimo dia do m�s e se este est� flegado
	lIsFat	:=	( aDiasFech[ aScan( aDiasFech, { | X | X[ 1 ] == 'G65_UDIA'  } ) ][ 2 ] ) .Or. lIsFat
	
	// Verifica se � um dia da semana flegado
	lIsFat	:=	( aDiasFech[ aScan( aDiasFech, { | X | X[ 1 ] == 'G65_' + aDow[ Dow( dData ) ] } ) ][ 2 ] ) .Or. lIsFat
	
	aAdd( aRet, lIsFat )
	
	// Verifica a data do in�cio da contagem
	If cIncCnt == '1' .Or. Empty(cIncCnt) 	// Fechamento
		
		dDtCont := dData
		
	ElseIf cIncCnt == '2' // Fora da Semana
		
		dDtCont := dData + ( 8 - Dow( dData ) )
		
	Else // Fora do M�s
		
		dDtCont := DaySum( LastDate( dData ), 1 )
		
	EndIf
	
	// Calcula primeiro vencimento
	dDtVenc := TRSomaDias( dDtCont, nQtdDia + nQtDiaT, cContag, cFeriad, aExcecoes)
	
	For nX := 1 To nQtdPar
		
		If SuperGetMv( 'MV_DIFPARC',.F.,"1"  ) == '1' .And. nX == 1 // Verifica se adciona diferen�a de soma de parcelas na primeira parcela
			
			aAdd( aParcelas, { dDtVenc, nVlrParc + nVlrDif } )
			
		ElseIf SuperGetMv( 'MV_DIFPARC',.F.,"1" ) == '2' .And. nX == nQtdPar  // Verifica se adciona diferen�a de soma de parcelas na �ltima parcela
			
			aAdd( aParcelas, { dDtVenc, nVlrParc + nVlrDif } )
			
		Else
			
			aAdd( aParcelas, { dDtVenc, nVlrParc } )
			
		EndIf
		
		dDtVenc := TRSomaDias( dDtVenc, nQtdDia , cContag, cFeriad, aExcecoes )
		
	Next nX
	
	aAdd( aRet, aParcelas )
	
Return aRet

/*/{Protheus.doc} TRSomaDias
Calcula da data final de um intervalo somando uma quantidade de dias e considerandos ser�o corridos ou dias �teis, tamb�m trata as exce��es e se o dia � um feriado
@author Elton Teodoro Alves
@since 20/07/2015
@version 1.0
@param dData, Data, Data de Fechamento
@param nDias, Numero, Quantidade de dias de intervalo entre as datas
@param cContag, Caractere, Indica o tipo de contagem 1=Dias Corridos / 2=Dias �teis
@param cFeriad, Caractere, Indica o tipo de tratamento quando vencimento � feriado 1=Mant�m / 2=Antecipa / 3=Posterga
@param aExcecoes, Array, Cont�m a lista de exce��es de datas de vencimento
@return Data Retorna data calcula a fim do intervalo
/*/
Function TRSomaDias( dData, nDias, cContag, cFeriad, aExcecoes )
	
	Local dRet := dData
	Local aDow := { 'DIADOM', 'DIASEG', 'DIATER', 'DIAQUA', 'DIAQUI', 'DIASEX', 'DIASAB' }
	Local nX   := 1
	
	If cContag == '1' // Considera dias corridos
		
		dRet := DaySum( dRet, nDias )
		
	ELse
		// Considera dias  �teis
		Do While nX <= nDias
			
			// Verifica se a data n�o � uma Sexta-Feira, S�bado ou Feriado e salta incremento.
			If !( Dow( dRet ) == 6 .Or. Dow( dRet ) == 7 .Or. TRFeriado( dRet ) )
				
				nX++
				
			EndIf
			
			dRet := DaySum( dRet, 1 )
			
		EndDo
		
	EndIf
	
	// Trata as exece�oes definidas, se � um dia do m�s e/ou dia da semana permitido para vencimento.
	Do While aExcecoes[ aScan( aExcecoes, { | X | X[ 1 ] == 'G66_DIAM' + cValToChar( Day( dRet ) ) } )][ 2 ] .Or.;
			aExcecoes[ aScan( aExcecoes, { | X | X[ 1 ] == 'G66_' + aDow[ Dow( dRet ) ] } ) ][ 2 ]
		
		dRet := DaySum( dRet, 1 )
		
	EndDo
	
	//Trata se dia � feriado conforme definido em G66_FERIAD
	If ! cFeriad == '1' // Mant�m
		
		Do While TRFeriado( dRet )
			
			If cFeriad == '2' // Antecipa
				
				dRet := DaySub( dRet, 1 )
				
			Else // Posterga
				
				dRet := DaySum( dRet, 1 )
				
			EndIf
			
		EndDo
		
	EndIF
	
Return dRet

/*/{Protheus.doc} TRFeriado
Verifica se uma data � ou n�o feriado
@author Elton Teodoro Alves
@since 20/07/2015
@version 1.0
@param dData, Data, Data a ser verificada
@return L�gico Indica se a data � um feriado
/*/
Function TRFeriado( dData )
	
	Local lRet  := .F.
	Local aArea := GetArea()
	Local cDia  := Day2Str( dData)
	Local cMes  := Month2Str( dData)
	
	DbSelectArea( 'SP3' )
	
	DbSetOrder( 1 ) // P3_FILIAL+Dtos(P3_DATA)
	
	lRet := DbSeek( xFilial( 'SP3' ) + dToS( dData ) )
	
	DbSetOrder( 2 ) // P3_FILIAL+P3_MESDIA+P3_FIXO
	
	lRet := DbSeek( xFilial( 'SP3' ) + cMes + cDia + 'S' ) .Or. lRet
	
	RestArea( aArea )
	
Return lRet

/*/{Protheus.doc} TRExistAp
Verifica se a revis�o de um acordo de meta foi utilizado em uma apura��o
@author elton.alves
@since 01/09/2015
@version 1.0
@param cCodAco, Caractere, C�digo do acordo de metas
@param cCodRev, Caractere, Revis�o do acordo de mestas
@Param cTipo, Caractere, Tipo de Apura��o
@return L�gico, Indica se a revis�o foi utilizada ou em uma revis�o
/*/
Function TRExistAp( cCodAco, cCodRev, cTipo )
	
	Local lRet := .F.
	
Return lret

/*/{Protheus.doc} TURAX01
Realiza pesquisa no cadastro de complemento de cliente e verifica se o cliente deve ser fechado na data desejada, retorna o c�digo do complemento 
do cliente e os segmentos de neg�cio
@author rogerio.melonio
@since 05/02/2016
@version 1.0
@param cCliCod, Caractere, C�digo do cliente
@param cCliLoja, Caractere, Loja do Cliente
@Param lValParam, Logico, Flag para indicar se considera ou n�o os par�metros de fechamento (aplica��o e tipo de fechamento)
@Param cAplica, Caractere, Aplica��o (1=Fat. Venda ; 2=Apura��o)
@Param dDataRef, Data, Data de refer�ncia de fechamento
@Param lCorp, Logico, Flag para indicar se considera ou n�o segmento Corporativo
@Param lEvento, Logico, Flag para indicar se considera ou n�o segmento Eventos
@Param lLazer, Logico, Flag para indicar se considera ou n�o segmento Lazer
@return array de 4 posi��es, sendo:
@[1] C�digo do Complemento de Cliente
@[2] Considera��o do Grupo de Produtos, se Todos (.F.) ou Lisatados (.T.) 
@[3] Array com rela��o dos grupos selecionados ou vazio se considera todos
@[4] String com os segmentos que o Complemento do Cliente foi configurado
/*/
Function TURAX01(cCliCod, cCliLoja , lValParam , cAplica ,dDataRef, lCorp, lEvento, lLazer, cGrupoPrd )
Local aArea 	:= GetArea()
Local cSegmento := ""
Local aFecha 	:= {}

Default dDataRef		:= dDataBase
Default cGrupoPrd		:= "" 

//Pesquisa os complementos do cliente correspondente ao segmento selecionado
dbSelectArea("G4L")
G4L->(dbSetOrder(2))
If G4L->( dbSeek(xFilial("G4L")+cCliCod+cCliLoja) )
	While G4L->(!Eof()) .And. (xFilial("G4L")+cCliCod+cCliLoja == G4L->(G4L_FILIAL+G4L_CLIENT+G4L_LOJA))  
		If 	(lEvento .And. G4L->G4L_EVENTO) .Or. ;
			(lLazer  .And. G4L->G4L_LAZER) .Or. ;
			(lCorp .And. G4L->G4L_CORP)
			cSegmento := IIf(G4L->G4L_CORP,"1","")+IIf(G4L->G4L_EVENTO,"2","")+IIf(G4L->G4L_LAZER,"3","")
			If lValParam
				//Fun��o que avalia Parametros de fechamento
				TURAX02(@aFecha,G4L->G4L_CODIGO,cSegmento,cAplica,dDataRef,cGrupoPrd)
			Else
				aAdd(aFecha,{ G4L->G4L_CODIGO,.T.,"",cSegmento } )
			EndIf
		EndIf
		G4L->(DbSkip())
	EndDo
EndIf 

RestArea(aArea)

Return aFecha


/*/{Protheus.doc} TURAX02
Fun��o que verifica se o cliente deve ser fechado na data desejada considerando Segmento e Grupo de Produto
@author    Jos� Domingos Caldana Jr
@version   1.00
@since     30/09/2015
/*/
Function TURAX02(aFecha,cCodCompl,cSegmento,cAplica,dDataRef,cGrupoPrd)
Local aArea 		:= GetArea()
Local lRet			:= .F.
Local lGrupo 		:= .F.
Local lDiaFech	:= .F. 

Default aFecha		:= {}
Default dDataRef		:= dDataBase
Default cGrupoPrd		:= "" 

dbSelectArea("G64")
G64->(dbSetOrder(1))
G64->(dbSeek(xFilial("G64") + cCodCompl ))
While !G64->(Eof()) .And. G64->(G64_FILIAL+G64_CODIGO) == xFilial("G64")+cCodCompl
	
	// Avalia se o parametro de Fechamento (Tabela G64) est� de acordo com a aplica��o (cAplica) 
	If G64->G64_APLICA == cAplica
       
       // Se existe dia para fechamento, avaliar considera��o do Grupo (G64_RELGRP)
		// 1=Listados ; 2=Todos
		If G64->G64_RELGRP <> "2" 
			dbSelectArea("G68")
			G68->(dbSetOrder(1))
			G68->(dbSeek( xFilial("G68") + cCodCompl + G64->G64_ITEM ))
			While !G68->(Eof()) .And. G68->(G68_FILIAL+G68_CODIGO+G68_ITEM) == xFilial("G68")+cCodCompl+G64->G64_ITEM 
				If G68->G68_CODGRP == cGrupoPrd
					lGrupo := .T.
					Exit
				EndIf
				G68->(dbSkip())
			EndDo
		Else
			lGrupo := .T.	
		Endif 
       
       //Avalia o dia de fechamento    	
		lDiaFech := TURAX03(G64->G64_FILIAL,cCodCompl,G64->G64_ITEM,G64->G64_TPFECH,dDataRef)
		
		If lGrupo .And. lDiaFech
			Aadd(aFecha,{ cCodCompl , ( G64->G64_RELGRP == "1" ), cGrupoPrd , cSegmento } ) 
		Endif
		
	Endif
	G64->(dbSkip())
EndDo

RestArea(aArea)

Return lRet


/*/{Protheus.doc} TURAX03
Fun��o que verifica se o cliente deve ser fechado na data desejada considerando Segmento e Grupo de Produto
@author    Jos� Domingos Caldana Jr
@version   1.00
@since     30/09/2015
/*/
Function TURAX03(cFilPesq,cCodCompl,cItem,cTpFech,dDataRef)
Local aArea		:= GetArea()
Local lRet 		:= .F.
Local nDiaSem		:= Dow(dDataRef) // 0 = vazia, 1 = Domingo, ..... , Sabado = 7 
Local aDow			:= { 2, 3, 4, 5, 6, 7, 1 }
Local nDow			:= aScan( aDow, { |x| x == nDiaSem } )

Default cFilPesq := xFilial("G65")

dbSelectArea("G65")
G65->(dbSetOrder(1))

// De Acordo com o Tipo de Fechamento (G64_TPFECH) avalia se deve fechar o cliente no dia (dDataRef)
// 1=Diario ; 2=Semanal ; 3=Decendial ; 4=Quinzenal ; 5=Mensal
If G65->(dbSeek( cFilPesq + cCodCompl + cItem ))
	// Se fechamento semanal verifica se a data de refer�ncia cai em dia de fechamento 
	If cTpFech == "2" .And. nDiaSem > 0
		lRet := ( Substr(G65->G65_DIASEM,nDow,1) == "S" )
	Else // Demais tipos de fechamento verifica se a data esta configurada na tabela G65
		lRet := (Substr(G65->G65_DIAMES,Day(dDataRef),1) == "S")
		// Verifica se UD esta marcada
		If !lRet .and. Substr(G65->G65_DIAMES,32,1) == "S"
			lRet := ( dDataRef == LastDay(dDataRef) )
		Endif
	Endif			
Endif

RestArea(aArea)

Return lRet


/*/{Protheus.doc} TURXFndG4L
Fun��o para obten��o do C�digo do Complemento do Cliente ou Filial do Posto 
a partir do C�digo do Cliente + Loja + Segmento 
@author    Marcelo Cardoso Barbosa
@version   1.00
@since     30/09/2015
/*/

Function TURXFndG4L(cCodCli, cLojCli, cSegNeg, cFilCli, cRet)

Local aArea     := GetArea()	
Local cAliasG4L := GetNextAlias()
Local cQuery    := ""
Local cFldG4L   := ""
Local cFldRet   := ""
Local cOldArea  := Alias()

Default cRet    := "1" // 1-Codigo do Complemento; 2-Codigo da Filial do Posto
Default cFilCli := xFilial("G4L") 

Do Case // Permite tratar Corportativo por 1 ou C, Evento por 2 ou E e Lazer por 3 ou L
	Case cSegNeg $ "1C" // Caso seja Corporativo 
		cFldG4L := "%G4L_CORP%"
		
	Case cSegNeg $ "2E" // Caso seja Evento
		cFldG4L := "%G4L_EVENTO%"
		
	Case cSegNeg $ "3L" // Caso seja Lazer
		cFldG4L := "%G4L_LAZER%"
EndCase

cFldRet := IIf(cRet == "1", "%G4L_CODIGO%", "%G4M_FILPOS%") 

BeginSql Alias cAliasG4L
	SELECT %Exp:cFldRet%, G4L_LOJA, G4L_COMPAR
	FROM %Table:G4L% G4L
	INNER JOIN %Table:G4M% G4M ON G4M_FILIAL = %xFilial:G4L% AND G4M_CODIGO = G4L_CODIGO AND G4M.%NotDel%
	WHERE G4L_FILIAL = %xFilial:G4L% AND
	      G4L_CLIENT = %Exp:cCodCli% AND
	      G4L_LOJA   = %Exp:cLojCli% AND
	      %Exp:cFldG4L% = 'T' AND 
	      G4L.%NotDel%
EndSql

If (cAliasG4L)->(EOF())
	(cAliasG4L)->(DbCloseArea())
	BeginSql Alias cAliasG4L
		SELECT %Exp:cFldRet%, G4L_LOJA, G4L_COMPAR
		FROM %Table:G4L% G4L
		INNER JOIN %Table:G4M% G4M ON G4M_FILIAL = %xFilial:G4L% AND G4M_CODIGO = G4L_CODIGO AND G4M.%NotDel%
		WHERE G4L_FILIAL = %xFilial:G4L% AND
		      G4L_CLIENT = %Exp:cCodCli% AND
		      G4L_COMPAR = '1' AND
		      %Exp:cFldG4L% = 'T' AND 
		      G4L.%NotDel%
	EndSql
EndIf

If (cAliasG4L)->(!EOF())
	cRet := (cAliasG4L)->&(StrTran(cFldRet, '%', ''))
EndIf
(cAliasG4L)->(DbCloseArea())

RestArea(aArea) 

Return(cRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TURXCndPag
Fun��o que retorna a Condi��o de Pagamento do Cliente 

@type 		Function
@param		cCliente, caracter - c�digo do cliente
			cLoja, caracter - loja do cliente
			cSegNeg, caracter - segmento de neg�gio
			cGrpCod, caracter - c�digo do grupo de produto
			cAplica, caracter - indica se retorna a condi��o para 1-Faturamento ou 2-Apura��o
@author 	Thiago Tavares
@since 		03/03/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TURXCndPag(cCliente, cLoja, cSegNeg, cGrpCod, cAplica)

Local aArea    := GetArea()
Local cCodCpl  := TURXFndG4L(cCliente, cLoja, cSegNeg, , '1')	// retorna o c�digo do complemento do cliente
Local cCondPag := '' 

dbSelectArea("G4L")
G4L->(DbSetOrder(1))		// G4L_FILIAL+G4L_CODIGO
G64->(DbSetOrder(2))		// G64_CODIGO+G64_APLICA

If G4L->(dbSeek(xFilial("G4L") + cCodCpl))						// posicionando no complemento do cliente
	If G64->(DbSeek(xFilial("G64") + cCodCpl + cAplica))  	// posicionando no fechamento 
		If cAplica == '2'		// apura��o 
			cCondPag := G64->G64_CONDPG 
		Else					// faturamento
			If G64->G64_RELGRP == '2'	// todos os grupos
				cCondPag := G64->G64_CONDPG 
			Else							// validando o grupo
				G68->(DbSetOrder(2))		// G68_FILIAL+G68_CODIGO+G68_ITEM+G68_CODGRP
				If G68->(DbSeek(xFilial("G68") + cCodCpl + G64->G64_ITEM + cGrpCod))
					cCondPag := G64->G64_CONDPG	  
				EndIf
			EndIf
		EndIf
	EndIf		
EndIf 

RestArea(aArea)

Return cCondPag

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TURXEmptyGrid
Fun��o que retorna se um grid est� vazio.

@since 		09/03/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TURXEmptyGrid(oModel)
Local nI
Local aRows := FwSaveRows()
Local lRet	:= .T.

For nI := 1 To oModel:Length()
	oModel:GoLine(nI)
	If !oModel:isDeleted() .AND. oModel:isModified()
		lRet := .F.
		Exit
	EndIf
Next nI

FwRestRows(aRows)
Return lRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TURXEnvMail

Funcao generica para envio de e-mail utilizando as configuracoes de E-mail/Proxy
especificados atraves do Configurador.

@sample   TURXEnvMail( cFrom, cTo, cCc, cBcc, cSubject, cBody, aAnexos )

@param	  ExpC1 - Rementente
		  ExpC2 - Destinatario
		  ExpC3 - Copia
		  ExpC4 - Copia Oculta
		  ExpC5 - Assunto
		  ExpC6 - Mensagem / Texto
		  ExpC7 - Anexos

@return  lRet

@author  Thiago Tavares 
@since	  19/05/2013
@version 11.90
/*/
//------------------------------------------------------------------------------
Function TURXEnvMail(cFrom, cTo, cCc, cBcc, cSubject, cBody, aAnexos) 

Local oMailServer 	:= Nil
Local lRetorno    	:= .F.
Local nSMTPPort   	:= SuperGetMV("MV_PORSMTP", .F., 25 )  	// PORTA SMTP
Local cSMTPAddr   	:= SuperGetMV("MV_RELSERV", .F., "" )  	// ENDERECO SMTP
Local cUser       	:= SuperGetMV("MV_RELACNT", .F., "" )  	// USUARIO PARA AUTENTICACAO SMTP
Local cPass       	:= SuperGetMV("MV_RELPSW" , .F., "" )  	// SENHA PARA AUTENTICA SMTP
Local cUserAut    	:= SuperGetMV("MV_RELAUSR", .F., "" )
Local lAutentica  	:= SuperGetMV("MV_RELAUTH", .F., .F.) 	// VERIFICAR A NECESSIDADE DE AUTENTICACAO
Local nSMTPTime   	:= SuperGetMV("MV_RELTIME", .F., 120) 	// TIMEOUT PARA A CONEXAO
Local lSSL        	:= SuperGetMV("MV_RELSSL" , .F., .F.)  	// VERIFICA O USO DE SSL
Local lTLS        	:= SuperGetMV("MV_RELTLS" , .F., .F.)  	// VERIFICA O USO DE TLS
Local nError      	:= 0											//CONTROLE DE ERRO.
Local nAnexos     	:= 0
Local nPos			:= 0
Local nDomain		:= 0
Local cMsgErro      := ""
 	
Default cFrom     := ""
Default cTo       := ""
Default cCc       := "" 
Default cBcc      := ""
Default cSubject  := ""
Default cBody     := ""

If ( nPos := AT(":", cSMTPAddr) ) > 0
	cSMTPAddr := SubStr( cSMTPAddr,1,nPos-1 )	
EndIf

oMailServer := TMailManager():New()

// Usa SSL, TLS ou nenhum na inicializacao
oMailServer:SetUseSSL(lSSL)
oMailServer:SetUseTLS(lTLS)

// Inicializacao do objeto de Email
If nError == 0
	nError := oMailServer:Init("", cSMTPAddr, cUser, cPass, 0, nSMTPPort)
	If nError <> 0
		cMsgErro := STR0040 + oMailServer:getErrorString(nError) //"Falha ao conectar: "
	EndIf
Endif

// Define o Timeout SMTP
If (nError == 0 .AND. oMailServer:SetSMTPTimeout(nSMTPTime) <> 0)
	nError := 1
	cMsgErro := STR0041   //"Falha ao definir timeout"
EndIf

// Conecta ao servidor
If nError == 0
	nError := oMailServer:SmtpConnect()
	If nError <> 0
		cMsgErro := STR0040 + oMailServer:getErrorString(nError)   //"Falha ao conectar: "
		oMailServer:SMTPDisconnect()
	EndIf
EndIf

// Realiza autenticacao no servidor
If nError == 0 .AND. lAutentica
	nError := oMailServer:SmtpAuth(cUserAut,cPass)
	If nError != 0
		nDomain := At("@",cUserAut)
		If ( nDomain > 0 )
			nError := oMailServer:SmtpAuth(LTrim(Subs(cUserAut,1,nDomain-1)),cPass)
		EndIf
	EndIf
	
	If nError <> 0
		cMsgErro := STR0042 + oMailServer:getErrorString(nError)   //"Falha ao autenticar: "
		oMailServer:SMTPDisconnect()
	EndIf
EndIf

lRetorno := (nError == 0) 

If ( lRetorno )

	oMessage:= TMailMessage():New()
	oMessage:Clear()
	oMessage:cFrom    := cUser
	oMessage:cTo      := cTo
	oMessage:cCc      := cCc
	oMessage:cBcc     := cBcc
	oMessage:cSubject := cSubject
	oMessage:cBody    := cBody
	oMessage:cReplyTo := IIF(IsInCallStack('TURA066'), cUser, cFrom)
       
	For nAnexos := 1 To Len(aAnexos)
		
		nError := oMessage:AttachFile(aAnexos[nAnexos])
		
		oMessage:AddAttHTag('Content-ID: <ID_conf_' + aAnexos[nAnexos] + '>')
		
		If ( nError < 0 )
			lRetorno := .f.
			cMsgErro := STR0043		//"N�o foi poss�vel anexar arquivo. O envio da mensagem ser� abortado."
			Exit
		Endif
		
    Next nAnexos
      
    If ( lRetorno )  
    	
    	nError := oMessage:Send(oMailServer)
    	
    	If nError <> 0
			cMsgErro := STR0044 + oMailServer:GetErrorString(nError)   //"Falha ao enviar o e-mail: "
		EndIf
		
		lRetorno := nError == 0
		
    Endif	
	
	oMailServer:SmtpDisconnect()     

EndIf

Return aClone({lRetorno, cMsgErro})

Function ConPadG3M(nType)

Local aButtons		:= {}
Local aIndice	 	:= {}
Local aRetorno		:= {}
Local cQry		 	:= ""          
Local lRet 			:= .F.
Local oLookUp		:= nil
Local cAlias 		:= If(Select('G3I')> 0, 'G3I','G4M')

cQry := " SELECT  G3M_FILIAL, G3M_CODIGO, G3M_DESCR"
cQry += " FROM " + RetSqlName("G3M")
cQry += " WHERE D_E_L_E_T_ = ' ' AND G3M_FILIAL LIKE ('"+ RTRIM(xFilial(cAlias))+ "%')"

oLookUp := TurXLookUp():New( cQry, {"G3M_CODIGO","G3M_FILIAL"})
                                                       
oLookUp:AddIndice( "Filial+Codigo", "G3M_FILIAL+G3M_CODIGO" 	)


If oLookUp:Execute()
	lRet := .T.
	aRetorno	:= oLookUp:GetReturn()
	If nType == 1 
		cCodPst	:= aRetorno[1]
		cFilPst	:= aRetorno[2]
	Else
		cFilPst	:= aRetorno[2]
	Endif	
EndIf   

Return lRet                          

Function TxSetFilAnt(cFil)
	cFilAuxAnt := cFil
Return

Function TxGetFilAnt()
Return cFilAuxAnt

Function TxSetFilFat(cFil)
	cFilAuxFat := cFil
Return 

Function TxGetFilFat()
Return cFilAuxFat

Function TxGetFil()
Return cFilPst

Function TxGetPosto()
Return cCodPst

Function TXVALPOSTO(cFilPst,cCodPst,cEnt)
Local lRet		:= .T.
Default cFilPst := ""
Default cCodPst := ""
Default cEnt	:= 'G3I'
If (!Empty(cFilPst) .Or. !Empty(cCodPst)) 
	If cEnt = 'G3I'   
		cFilPst := FWFLDGET('G3I_FILPST')
		cCodPst := FWFLDGET('G3I_POSTO')
	Elseif cEnt = 'G4M' 
		cFilPst := FWFLDGET('G4M_FILPOS')
		cCodPst := FWFLDGET('G4M_CODPOS')
	Endif	
Endif
If !Empty(cCodPst) .and. !Empty(cFilPst) .and. !G3M->(DBSEEK(cFilPst+cCodPst))
	lRet := .F.
	Help(" ",1,"REGNOIS")
Endif

Return lRet 
//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TURXVLDG6O

Fun��o que verifica se o acordo + revis�o est� em apura��o 

@type 		Function
@author 	Thiago Tavares
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TURXVLDG6O(cCodAco, cRevisao, cTipo)

Local aArea := GetArea()
Local lRet  := .F.

DbSelectArea("G6O")
G6O->(DbSetOrder(2))		// G6O_FILIAL+G6O_CODACD+G6O_CODREV+G6O_TIPOAC
If G6O->(DbSeek(xFilial("G6O") + cCodAco + cRevisao + cTipo))
	lRet := .T.
EndIf
G6O->(DbCloseArea())

RestArea(aArea)

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TURXLibApura
Fun��o de libera��o das apura��es de receitas por meta, de clientes e de fornecedores

@type 		Function
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------

Function TURXLibApura()

Local cTableSeek	:= ""
Local cFieldSeek	:= ""
Local cCliFor		:= ""
Local cLoja			:= ""
Local cNome			:= ""
Local cTipoApura	:= ""
Local cIdModel		:= ""

Local lRet			:= .t.

Local bProcess

Do Case

Case ( FwIsInCallStack("TURA040") )	

	cTableSeek	:= "SA1"
	cFieldSeek	:= "A1_NOME"
	cCliFor		:= G6L->G6L_CLIENT
	cLoja		:= G6L->G6L_LOJA
	cTipoApura	:= STR0005	//"do cliente:"
	
	bProcess 	:= {|| StaticCall(TURA040,TA040LibP)}

Case ( FwIsInCallStack("TURA043") )

	cTableSeek	:= "SA1"
	cFieldSeek	:= "A1_NOME"
	cCliFor		:= G6L->G6L_CLIENT
	cLoja		:= G6L->G6L_LOJA
	cTipoApura	:= STR0005	//"do cliente:"
	
	bProcess 	:= {|| StaticCall(TURA043,TA043ProcLib)}
	
Case ( FwIsInCallStack("TURA050") )

	cTableSeek	:= "SA1"
	cFieldSeek	:= "A1_NOME"
	cCliFor		:= G6L->G6L_CLIENT
	cLoja		:= G6L->G6L_LOJA
	cTipoApura	:= STR0006	//"do fornecedor:"
	
	bProcess 	:= {|| lRet := StaticCall(TURA050,TA050LibP)}
	
EndCase

// validando se o fornecedor possui c�digo como cliente
If (FwIsInCallStack("TURA043") .Or. FwIsInCallStack("TURA050")) .And. (Empty(cCliFor) .Or. Empty(cLoja))
	aSA2 := ClienteFor(G6L->G6L_FORNEC, G6L->G6L_LJFORN)
	If Len(aSA2) == 0 .Or. Empty(cCliFor) .Or. Empty(cLoja) 	
		FwAlertHelp(STR0053, STR0054)		// "Aten��o"		"Favor verificar o cadastro do Fornecedor, pois ele n�o possui seu c�digo como cliente cadastrado."
		lRet := .F. 
	Else
		G6L->G6L_CLIENT := aSA2[2] 
		G6L->G6L_LOJA   := aSA2[3] 
	EndIf
EndIf

If lRet
	cNome := Posicione(cTableSeek, 1, XFilial(cTableSeek) + cCliFor + cLoja, cFieldSeek )
	
	If ( G6L->G6L_STATUS <> '1' )
		
		lRet := .f.
		
		FwAlertHelp(STR0007,STR0008) //"Apura��o j� liberada, n�o � permitido uma nova libera��o." //"Escolha apura��es que estejam com o status igual a 'Aguardando Libera��o'"
		
	Else
	
		If ( FwIsInCallStack("TURA040") )
			cIdModel := "TURA040"
		ElseIf ( FwIsInCallStack("TURA043") )
			cIdModel := "TURA043"
		ElseIf ( FwIsInCallStack("TURA050") )
			cIdModel := "TURA050"
		EndIf		
	
		lRet := TurChkItApur(cIdModel)
		
		If ( lRet .And. !CtbValiDt(,G6L->G6L_DTGERA,,,,{"TUR001"},) )
			lRet	:= .F.
		EndIf	
	
		If ( lRet )
		
			If ( FwAlertYesNo(I18N(STR0009 + cTipoApura + " #2 #3", {Alltrim(G6L->G6L_CODAPU), cCliFor, Alltrim(cNome)})) ) //"Confirma a libera��o da apura��o: #1 " 
				FwMsgRun( , bProcess , , STR0010) // "Liberando Apura��o..."
			EndIf
			
		EndIf		
				
	EndIf
	
EndIf

Return(lRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TURXEstApura
Fun��o de estorno da libera��o das apura��es de receitas por meta, de clientes e de fornecedores

@type 		Function
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
 
Function TURXEstApura()

Local cTableSeek	:= ""
Local cFieldSeek	:= ""
Local cCliFor		:= ""
Local cLoja			:= ""
Local cNome			:= ""
Local cTipoApura	:= ""

Local lContinua		:= .t.

Local lRet			:= .t.

Local bProcess

Do Case

Case ( FwIsInCallStack("TURA040") )	

	cTableSeek	:= "SA1"
	cFieldSeek	:= "A1_NOME"
	cCliFor		:= G6L->G6L_CLIENT
	cLoja		:= G6L->G6L_LOJA
	cTipoApura	:= STR0005	//"do cliente:"
	
	bProcess 	:= {|| StaticCall(TURA040,TA040EstL)}
	
Case ( FwIsInCallStack("TURA043") )

	cTableSeek	:= "SA1"
	cFieldSeek	:= "A1_NOME"
	cCliFor		:= G6L->G6L_CLIENT
	cLoja		:= G6L->G6L_LOJA
	cTipoApura	:= STR0005	//"do cliente:"
	
	bProcess 	:= {|| StaticCall(TURA043,TA043ProcEst) }
	
Case ( FwIsInCallStack("TURA050") )

	cTableSeek	:= "SA1"
	cFieldSeek	:= "A1_NOME"
	cCliFor		:= G6L->G6L_CLIENT
	cLoja		:= G6L->G6L_LOJA
	cTipoApura	:= STR0006	//"do fornecedor:"
	
	bProcess 	:= {|| StaticCall(TURA050,TA050EstL)}
	
EndCase

cNome := Posicione(cTableSeek, 1, XFilial(cTableSeek) + cCliFor + cLoja, cFieldSeek )

lContinua	:= TURXVlIF(G6L->G6L_CODAPU)

If ( lContinua )

	//Verifica se a apura��o de metas foi conciliada e aprovada. N�o pode permitir o estorno, caso tenha sido aprovado.
	If ( FwIsInCallStack("TURA043") )
		lContinua := !TA43ConMtApv(G6L->G6L_CODAPU,G6L->G6L_FILIAL)
	EndIf
	
	If ( lContinua )
	
		If ( G6L->G6L_STATUS <> "2" )
			lRet := .f.
			FwAlertHelp(STR0011,STR0012)	//"Apura��o n�o foi liberada, n�o � permitido realizar estorno."	//"Escolha apura��es que estejam com o status igual a 'Apura��o em Aberto'"
		Else


			If !CtbValiDt(,G6L->G6L_DTGERA,,,,{"TUR001"},)
				lRet	:= .F.
			EndIf
			
			If lRet	
	
				If ( FwAlertYesNo(I18N(STR0013 + cTipoApura + " #2 #3 ?", {Alltrim(G6L->G6L_CODAPU), cCliFor, Alltrim(cNome)})) )//"Confirma o estorno da libera��o da apura��o: #1 " 
					FwMsgRun( , bProcess , , STR0014) // "Estornando Libera��o da Apura��o..."
				EndIf
	
			EndIf
	
		EndIf
	
	Else
		lRet := .f.
		FwAlertHelp(STR0066,STR0067)//"Apura��o de Metas j� possui Concilia��o Aprovada!"#"Verifique esta apura��o de metas atrav�s da Concilia��o de Metas"		
	EndIf

Else 
	lRet := .f.
	If !FwIsInCallStack('TA040SelApu')
		FwAlertHelp(STR0015,STR0016)	//"Apura��o n�o pode ser estornada."	//"Esta apura��o possui itens financeiros que foram finalizados."
	EndIf
Endif

Return(lRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TURXVlIF
Fun��o respons�vel pela valida��o dos IF's (G81), quando for estornar a libera��o. N�o
pode existir item finalizado.

@type 		Function
@author 	Thiago Tavares
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TURXVlIF(cCodApu)

Local aArea     := GetArea()
Local cAliasG81 := GetNextAlias()
Local lRet      := .T.

//+------------------------------------------------
// Seleciona Itens do Financeiros
//+------------------------------------------------
BeginSql Alias cAliasG81
	SELECT	G81_STATUS
	FROM %Table:G81% G81
	WHERE G81_FILIAL = %xFilial:G81% AND
	      G81_CODAPU = %Exp:cCodApu% AND 
	      G81.%NotDel%
EndSql

While (cAliasG81)->(!EOF())

	If (cAliasG81)->G81_STATUS == '2' // Finalizado
		lRet := .F.
		Exit		
	EndIf
	
	(cAliasG81)->(DbSkip())

EndDo

(cAliasG81)->(DbCloseArea())

RestArea(aArea)

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TurCtOnApu() 
Rotina de contabiliza��o da libera��o da apura��o

@type 		Function
@author 	Simone Mie Sato Kakinoana
@since 		14/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TurCtOnApu(lOnline,lMostrLanc,lAglutLanc,aCtbG81A,aCtbG81B,cOrigem)

Local aSaveArea	:= GetArea()

Local aFlagCTB 	:= {}

Local cPadT40    	:= "T40"             
Local cPadT42    	:= "T42"
Local cPadT44    	:= "T44"             
Local cLoteTur	 	:= LoteCont("TUR")

Local cCodPrd		:= ""
Local cCodTes		:= ""

Local lPadT40	  	:= VerPadrao(cPadT40)
Local lPadT42	  	:= VerPadrao(cPadT42)
Local lPadT44	  	:= VerPadrao(cPadT44)
Local lUsaFlag			:= SuperGetMV( "MV_CTBFLAG" , .T. /*lHelp*/, .F. /*cPadrao*/)

Local nA			:= 0
Local nB			:= 0
Local nRecG6A		:= 0
Local nPosReg		:= 0 

Private cArquivo  	:= ""
Private nHdlPrv		:= 0
Private nTotal	 		:= 0
Private LanceiCtb		:= .F.

nHdlPrv := HeadProva(cLoteTur,cOrigem,Substr(cUsuario,7,6),@cArquivo)

//Posicionar no G81
For nA := 1 To Len(aCtbG81A)
	DbSelectArea("G81")
	G81->(DbSetOrder(1))
	If G81->(DbSeek(xFilial("G81")+aCtbG81A[nA]))
		If G81->G81_LA <> 'S'
		
			SA1->(DbSetOrder(1))
			SA1->(DbSeek(xFilial("SA1")+G6L->G6L_CLIENT+G6L->G6L_LOJA))
			
			nRecG6A := TurSeekG6A(G81->G81_CLASS, G81->G81_SEGNEG)
			If nRecG6A > 0 
				G6A->(dbGoto(nRecG6A))
			EndIf
			
			If G81->G81_TIPO == "1"
				If lPadT40
					If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil 
						aAdd( aFlagCTB, {"G81_LA", "S", "G81", G81->( Recno() ), 0, 0, 0} )
					Endif
				
					nTotal += DetProva(nHdlPrv,cPadT40,cOrigem,cLoteTur,,,,,,,,@aFlagCTB)
					
					If LanceiCtb // Vem do DetProva
						If !lUsaFlag
							RecLock("G81")
							G81->G81_LA    := "S"
							G81->(MsUnlock())
						EndIf
					ElseIf lUsaFlag
						If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == G81->(Recno()) }))>0
							aFlagCTB := Adel(aFlagCTB,nPosReg)
							aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
						Endif
					EndIf
					
				EndIf
			ElseIf G81->G81_TIPO == "2"
				If lPadT42
					If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil 
						aAdd( aFlagCTB, {"G81_LA", "S", "G81", G81->( Recno() ), 0, 0, 0} )
					Endif

					nTotal += DetProva(nHdlPrv,cPadT42,cOrigem,cLoteTur,,,,,,,,@aFlagCTB)
					
					If LanceiCtb // Vem do DetProva
						If !lUsaFlag
							RecLock("G81")
							G81->G81_LA    := "S"
							G81->(MsUnlock())
						EndIf
					ElseIf lUsaFlag
						If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == G81->(Recno()) }))>0
							aFlagCTB := Adel(aFlagCTB,nPosReg)
							aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
						Endif
					EndIf
					
				EndIf
			EndIf
		Endif
	EndIf
Next nA					
	
For nB := 1 To Len(aCtbG81B)
	DbSelectarea("G81")
	G81->(DbSetOrder(1))
	If G81->(DbSeek(xFilial("G81")+aCtbG81B[nB]))
		If G81->G81_LA <> 'S'
		
			SA1->(DbSetOrder(1))
			SA1->(DbSeek(xFilial("SA1")+G6L->G6L_CLIENT+G6L->G6L_LOJA))

			nRecG6A := TurSeekG6A(G81->G81_CLASS, G81->G81_SEGNEG)
			If nRecG6A > 0 
				G6A->(dbGoto(nRecG6A))
			EndIf
			
			If G81->G81_TIPO == "1"
				If lPadT40
					If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil 
						aAdd( aFlagCTB, {"G81_LA", "S", "G81", G81->( Recno() ), 0, 0, 0} )
					Endif
				
					nTotal += DetProva(nHdlPrv,cPadT40,cOrigem,cLoteTur,,,,,,,,@aFlagCTB)
					
					If LanceiCtb // Vem do DetProva
						If !lUsaFlag
							RecLock("G81")
							G81->G81_LA    := "S"
							G81->(MsUnlock())
						EndIf
					ElseIf lUsaFlag
						If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == G81->(Recno()) }))>0
							aFlagCTB := Adel(aFlagCTB,nPosReg)
							aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
						Endif
					EndIf
					
				EndIf
			ElseIf G81->G81_TIPO == "2"
				If lPadT42
					If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil 
						aAdd( aFlagCTB, {"G81_LA", "S", "G81", G81->( Recno() ), 0, 0, 0} )
					Endif
				
					nTotal += DetProva(nHdlPrv,cPadT42,cOrigem,cLoteTur,,,,,,,,@aFlagCTB)
					
					If LanceiCtb // Vem do DetProva
						If !lUsaFlag
							RecLock("G81")
							G81->G81_LA    := "S"
							G81->(MsUnlock())
						EndIf
					ElseIf lUsaFlag
						If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == G81->(Recno()) }))>0
							aFlagCTB := Adel(aFlagCTB,nPosReg)
							aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
						Endif
					EndIf
					
				EndIf
			Endif
		EndIf
	EndIf	
Next			

//Contabiliza por total => G6L		
If lPadT44
	If G6L->G6L_LA <> 'S'
			
		SA1->(DbSetOrder(1))
		SA1->(DbSeek(xFilial("SA1")+G6L->G6L_CLIENT+G6L->G6L_LOJA))			

		nRecG6A := TurSeekG6A(G81->G81_CLASS, G81->G81_SEGNEG)
		If nRecG6A > 0 
			G6A->(dbGoto(nRecG6A))
		EndIf
	
		If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil 
			aAdd( aFlagCTB, {"G6L_LA", "S", "G6L", G6L->( Recno() ), 0, 0, 0} )
		Endif		
		
		DbselectArea("G6L")		
		nTotal += DetProva(nHdlPrv,cPadT44,cOrigem,cLoteTur,,,,,,,,@aFlagCTB)

		If LanceiCtb // Vem do DetProva
			If !lUsaFlag
				RecLock("G6L")
				G6L->G6L_LA    := "S"
				G6L->(MsUnlock())
			EndIf
		ElseIf lUsaFlag
			If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == G6L->(Recno()) }))>0
				aFlagCTB := Adel(aFlagCTB,nPosReg)
				aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
			Endif
		EndIf
		
	EndIf
EndIf

If nHdlPrv > 0 .And. nTotal > 0
	RodaProva(nHdlPrv,nTotal)
	cA100Incl(cArquivo, nHdlPrv, 3, cLoteTur, lMostrLanc, lAglutLanc,,,,@aFlagCTB,,)
	cArquivo	:= ""
	nHdlPrv		:= 0
	nTotal		:= 0
Endif

aFlagCTB := {}

RestArea(aSaveArea)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TurCtOnEAp() 
Rotina de contabiliza��o do Estorno da libera��o da apura��o

@type 		Function
@author 	Simone Mie Sato Kakinoana
@since 		14/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TurCtOnEAp(oModel,lOnline,lMostrLanc,lAglutLanc,aCtbG81A,aCtbG81B,cOrigem)

Local aSaveArea	:= GetArea()

Local aFlagCTB 	:= {}

Local cPadT41    	:= "T41"             
Local cPadT43    	:= "T43"
Local cPadT45    	:= "T45"             
Local cLoteTur	 	:= LoteCont("TUR")

Local lPadT41	  	:= VerPadrao(cPadT41)
Local lPadT43	  	:= VerPadrao(cPadT43)
Local lPadT45	  	:= VerPadrao(cPadT45)
Local lUsaFlag			:= SuperGetMV( "MV_CTBFLAG" , .T. /*lHelp*/, .F. /*cPadrao*/)

Local nA			:= 0
Local nB			:= 0
Local nRecG6A		:= 0 
Local nPosReg			:= 0

Private cArquivo  	:= ""
Private nHdlPrv		:= 0
Private nTotal	 		:= 0
Private LanceiCtb		:= .F.

nHdlPrv := HeadProva(cLoteTur,cOrigem,Substr(cUsuario,7,6),@cArquivo)

//Posicionar no G81
For nA := 1 To Len(aCtbG81A)
	DbSelectArea("G81")
	G81->(DbSetOrder(1))
	If G81->(DbSeek(xFilial("G81")+aCtbG81A[nA]))
	
		SA1->(DbSetOrder(1))
		SA1->(DbSeek(xFilial("SA1")+G6L->G6L_CLIENT+G6L->G6L_LOJA))
	
		nRecG6A := TurSeekG6A(G81->G81_CLASS, G81->G81_SEGNEG)
		If nRecG6A > 0 
			G6A->(dbGoto(nRecG6A))
		EndIf
		
		If G81->G81_LA == 'S'
			If G81->G81_TIPO == "1"
				If lPadT41
					If lUsaFlag
						aAdd(aFlagCTB,{"G81_LA"," ","G81",G81->(Recno()),0,0,0})
					EndIf
				
					nTotal += DetProva(nHdlPrv,cPadT41,cOrigem,cLoteTur,,,,,,,,@aFlagCTB)
					
					If LanceiCtb // Vem do DetProva
						If !lUsaFlag
							RecLock("G81")
							G81->G81_LA    := " "
							G81->(MsUnlock())
						EndIf
					ElseIf lUsaFlag
						If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == G81->(Recno()) }))>0
							aFlagCTB := Adel(aFlagCTB,nPosReg)
							aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
						Endif
					EndIf		
					
				EndIf
			ElseIf G81->G81_TIPO == "2"
				If lPadT43
					If G81->G81_LA == 'S'
						If lUsaFlag
							aAdd(aFlagCTB,{"G81_LA"," ","G81",G81->(Recno()),0,0,0})
						EndIf
						
						nTotal += DetProva(nHdlPrv,cPadT43,cOrigem,cLoteTur,,,,,,,,@aFlagCTB)
						
						If LanceiCtb // Vem do DetProva
							If !lUsaFlag
								RecLock("G81")
								G81->G81_LA    := " "
								G81->(MsUnlock())
							EndIf
						ElseIf lUsaFlag
							If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == G81->(Recno()) }))>0
								aFlagCTB := Adel(aFlagCTB,nPosReg)
								aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
							Endif
						EndIf		
						
					EndIf
				EndIf
			EndIf
		Endif
	EndIf
Next nA					
	
For nB := 1 To Len(aCtbG81B)
	DbSelectarea("G81")
	G81->(DbSetOrder(1))
	If G81->(DbSeek(xFilial("G81")+aCtbG81B[nB]))
		
		SA1->(DbSetOrder(1))
		SA1->(DbSeek(xFilial("SA1")+G6L->G6L_CLIENT+G6L->G6L_LOJA))

		nRecG6A := TurSeekG6A(G81->G81_CLASS, G81->G81_SEGNEG)
		If nRecG6A > 0 
			G6A->(dbGoto(nRecG6A))
		EndIf
	
		If G81->G81_LA == 'S'
			If G81->G81_TIPO == "1"
				If lPadT41
					If lUsaFlag
						aAdd(aFlagCTB,{"G81_LA"," ","G81",G81->(Recno()),0,0,0})
					EndIf
			
					nTotal += DetProva(nHdlPrv,cPadT41,cOrigem,cLoteTur,,,,,,,,@aFlagCTB)

					If LanceiCtb // Vem do DetProva
						If !lUsaFlag
							RecLock("G81")
							G81->G81_LA    := " "
							G81->(MsUnlock())
						EndIf
					ElseIf lUsaFlag
						If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == G81->(Recno()) }))>0
							aFlagCTB := Adel(aFlagCTB,nPosReg)
							aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
						Endif
					EndIf		
					
				EndIf
			ElseIf G81->G81_TIPO == "2"
				If lPadT43
					If lUsaFlag
						aAdd(aFlagCTB,{"G81_LA"," ","G81",G81->(Recno()),0,0,0})
					EndIf
				
					nTotal += DetProva(nHdlPrv,cPadT43,cOrigem,cLoteTur,,,,,,,,@aFlagCTB)
					
					If LanceiCtb // Vem do DetProva
						If !lUsaFlag
							RecLock("G81")
							G81->G81_LA    := " "
							G81->(MsUnlock())
						EndIf
					ElseIf lUsaFlag
						If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == G81->(Recno()) }))>0
							aFlagCTB := Adel(aFlagCTB,nPosReg)
							aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
						Endif
					EndIf		
					
				EndIf
			Endif
		EndIf
	EndIf	
Next			

//Contabiliza por total => G6L		
If lPadT45
	If G6L->G6L_LA == 'S'
		If lUsaFlag
			aAdd(aFlagCTB,{"G6L_LA"," ","G6L",G6L->(Recno()),0,0,0})
		EndIf
		DbselectArea("G6L")		
		nTotal += DetProva(nHdlPrv,cPadT45,cOrigem,cLoteTur)

		If LanceiCtb // Vem do DetProva
			If !lUsaFlag
				RecLock("G6L")
				G6L->G6L_LA    := " "
				G6L->(MsUnlock())
			EndIf
		ElseIf lUsaFlag
			If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == G6L->(Recno()) }))>0
				aFlagCTB := Adel(aFlagCTB,nPosReg)
				aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
			Endif
		EndIf		
		
	EndIf
EndIf

If nHdlPrv > 0 .And. nTotal > 0
	RodaProva(nHdlPrv,nTotal)
	cA100Incl(cArquivo, nHdlPrv, 3, cLoteTur, lMostrLanc, lAglutLanc,,,,@aFlagCTB,,)
	cArquivo	:= ""
	nHdlPrv		:= 0
	nTotal		:= 0
Endif

aFlagCTB := {}

RestArea(aSaveArea)

Return



//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TurSeekG6A() 
Posiciona no G6A, buscando pelo tipo de acordo e segmento de neg�cio. 

@type 		Function
@author 	Simone Mie Sato Kakinoana
@since 		18/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TurSeekG6A(cTipoAc, cSegNeg)

Local aArea     := GetArea()
Local cAliasG6A := GetNextAlias()
Local nRecG6A	  := 0 	
Local cWhere    := ''

DEFAULT cSegNeg	:= ""

If !Empty(cSegNeg)
	Do Case
		Case cSegNeg == '1' // Corporativo
			cWhere := "% AND G6A_CORP = 'T' %"
		Case cSegNeg == '2' // Evento
			cWhere := "% AND G6A_EVENTO = 'T' %"
		Case cSegNeg == '3' // Lazer
			cWhere := "% AND G6A_LAZER = 'T' %"
	EndCase
Else
	cWhere := "% %"
EndIf

BeginSql Alias cAliasG6A
	SELECT	R_E_C_N_O_ RECNO
	FROM %Table:G6A% G6A
	WHERE G6A_CDCLAS = %Exp:cTipoAc% AND 
	      G6A_FILIAL = %xFilial:G6A% AND 
	      G6A.%notDel% 
	      %Exp:cWhere%
EndSql

nRecG6A := (cAliasG6A)->RECNO

(cAliasG6A)->(DbCloseArea())

RestArea(aArea)

Return(nRecG6A)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TurApuraBrw
Fun��o utilizada para carregar as colunas.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		03/05/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Function TurApuraBrw(oBrowse)

Local nOrdCod := 0 
Local nOrdLoj := 0
Local nOrdNom := 0

If ( ValType(oBrowse) == "O" .And. Alltrim(Upper(oBrowse:ClassName())) == "FWMBROWSE" )

	If ( FWIsInCallStack("TURA043") .OR. FWIsInCallStack("TURA050") )
	
		nOrdCod := oBrowse:GetColById("G6L_CLIENT"):GetOrder()
		nOrdLoj := oBrowse:GetColById("G6L_LOJA"):GetOrder()
		nOrdNom := oBrowse:GetColById("G6L_NOME"):GetOrder()
		
		oBrowse:DelColumn(nOrdCod)
		oBrowse:DelColumn(nOrdLoj)
		oBrowse:DelColumn(nOrdNom)
	
	Else	
	
		nOrdCod := oBrowse:GetColById("G6L_FORNEC"):GetOrder()
		nOrdLoj := oBrowse:GetColById("G6L_LJFORN"):GetOrder()
		nOrdNom := oBrowse:GetColById("G6L_NOMFOR"):GetOrder()
		
		oBrowse:DelColumn(nOrdCod)
		oBrowse:DelColumn(nOrdLoj)
		oBrowse:DelColumn(nOrdNom)
		
	Endif
	
	oBrowse:UpdateBrowse()
	
Endif

Return(.t.)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TurMultFiliais
Fun��o respons�vel por verificar se dever� ser mostrada a a tela de filiais.
 
@type 		Function
@author 	Fernando Radu Muscalu
@since 		03/05/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------

Function TurMultFiliais(aFiliais)

Local aTabMod	:= {}
Local aAux		:= TurGetTables()
Local lGestao	:= FWSizeFilial() > 2 
Local lRet := .t.

Default aFiliais := {}

lRet := TurChkModo(aAux)

If ( lRet )
		
	aTabMod := TurRetModoTab(aAux)
	
	If  aTabMod[1,2,1] == "C" .and. ( !lGestao .or. ( lGestao .AND. aTabMod[1,2,2] == "C" .And. aTabMod[1,2,3] == "C" ) )
		aFiliais := {XFilial(aAux[1])}
	Else
		TurSelFil()
		aFiliais := TurArrMultFil()
	Endif
	
	lRet := Len(aFiliais) > 0
	
Endif

Return(lRet)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TurF3Filiais
Fun��o respons�vel por montar a interface de sele��o de filiais. Ela tamb�m retorna as 
filiais que foram selecionadas em uma string.
 
@type 		Function
@author 	Fernando Radu Muscalu
@since 		03/05/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Function TurSelFil(lCurrentGrp, lShow, cAlias)

Local cAuxStr	:= ""
Local nI		:= 0

Default lCurrentGrp	:= .F.
Default lShow		:= .t.
Default cAlias 		:= "G6L" 	//Tabela Principal de Apura��o

If ( lShow )
	aTurMultFil := AdmGetFil(,lCurrentGrp,cAlias)
Else
	
	If ( Len(aTurMultFil) == 0 )
		aTurMultFil := TurGetFil(!lCurrentGrp)	//AdmGetFil(,.f.,aTabChkModo[1])
	Endif
		
Endif

Return()


//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TurArrMultFil
Fun��o que retorna o conte�do do array est�tico aTurMultFil. Se passado o par�metro, o array
retornado ser� um vetor vazio.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		03/05/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Function TurArrMultFil(lReset)

Default lReset := .f.

If ( lReset )
	aTurMultFil := {}
Endif

Return(aTurMultFil)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TurFilFilter
Fun��o que efetua o filtro de filiais, para uso de multifilial. A principio foi implementado 
para filtro em query

@type 		Function
@author 	Fernando Radu Muscalu
@since 		03/05/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Function TurFilFilter(cAlias,lRetInQry)

Local cRet		:= ""
Local cAtual	:= ""

Local aTabMod	:= {}
Local aFilInfo	:= FWArrFilAtu()

Local nI		:= 0
Local nEmp		:= Len(aFilInfo[13])
Local nUng		:= Len(aFilInfo[14])
Local nFil		:= Len(aFilInfo[15])

Default lRetInQry := .t.

If ( Len(aTurMultFil) > 0 )
	 
	 aTabMod := TurRetModoTab({cAlias})
	 	
 	If ( lRetInQry )
 		
 		If ( aTabMod[1,2,1] == "E" .AND. aTabMod[1,2,2] == "C" .And. aTabMod[1,2,3] == "C" )
 			 
 			If ( Len(aTurMultFil) > 0 )
 				
 				cAtual := Substr(aTurMultFil[1],1,nEmp)
 				
 				cRet := "("
	 			cRet += PrefixoCpo(cAlias) + "_FILIAL LIKE '" + cAtual + "%'" 
	 			
	 			For nI := 1 to Len(aTurMultFil)
	 				
	 				If ( cAtual <> Substr(aTurMultFil[nI],1,nEmp) )
	 					cRet += " OR "
	 					cRet += PrefixoCpo(cAlias) + "_FILIAL LIKE '" + Substr(aTurMultFil[nI],1,nEmp) + "%'"
	 				Endif
	 				
	 				cAtual := Substr(aTurMultFil[nI],1,nEmp)
	 				
	 			Next nI
	 				
	 			cRet += ")"
 			
 			Endif
 			
 		ElseIf ( aTabMod[1,2,1] == "E" .AND. aTabMod[1,2,2] == "E" .And. aTabMod[1,2,3] == "C" )
 		
 			If ( Len(aTurMultFil) > 0 )
 				
 				cAtual := Substr(aTurMultFil[1],1,nEmp+nUng)
 				
 				cRet := "("
	 			cRet += PrefixoCpo(cAlias) + "_FILIAL LIKE '" + cAtual + "%'" 
	 			
	 			For nI := 1 to Len(aTurMultFil)
	 				
	 				If ( cAtual <> Substr(aTurMultFil[nI],1,nEmp+nUng) )
	 					cRet += " OR "
	 					cRet += PrefixoCpo(cAlias) + "_FILIAL LIKE '" + Substr(aTurMultFil[nI],1,nEmp+nUng) + "%'"
	 				Endif
	 				
	 				cAtual := Substr(aTurMultFil[nI],1,nEmp+nUng)
	 				
	 			Next nI
	 				
	 			cRet += ")"
 			
 			Endif 
 			
 		ElseIf ( aTabMod[1,2,1] == "E" .AND. aTabMod[1,2,2] == "E" .And. aTabMod[1,2,3] == "E" )
 		
 			If ( Len(aTurMultFil) > 0 )
 				
 				cAtual := Substr(aTurMultFil[1],1,nEmp+nUng+nFil)
 				
 				cRet := "("
	 			cRet += PrefixoCpo(cAlias) + "_FILIAL LIKE '" + cAtual  + "%'" 
	 			
	 			For nI := 1 to Len(aTurMultFil)
	 				
	 				If ( cAtual <> Substr(aTurMultFil[nI],1,nEmp+nUng+nFil) )
	 					cRet += " OR "
	 					cRet += PrefixoCpo(cAlias) + "_FILIAL LIKE '" + Substr(aTurMultFil[nI],1,nEmp+nUng+nFil) + "%'"
	 				Endif
	 				
	 				cAtual := Substr(aTurMultFil[nI],1,nEmp+nUng+nFil)
	 				
	 			Next nI
	 				
	 			cRet += ")"
 			
 			Endif 
 		
 		Else
	 		cRet := PrefixoCpo(cAlias) + "_FILIAL = '" + XFilial(cAlias) + "'" 
	 	Endif
 		
 	Endif
 	 
Else //Retorna filtro da Filial Corrente
	
	If ( lRetInQry )
		cRet := PrefixoCpo(cAlias) + "_FILIAL = '" + XFilial(cAlias) + "'" 
	Else
		cRet := PrefixoCpo(cAlias) + "_FILIAL == '" + XFilial(cAlias) + "'"
	Endif
		
Endif

Return(cRet)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TurChkModo
Fun��o que valida o modo de acesso das tabelas que foram passadas pelo par�metro aTables.

Caso seja passado o par�metro cModo (ou "E" ou "C"), ent�o ser� avaliado se algum dos modos
de acesso das tabelas de aTable � igual ou diferente ao conte�do de cModo. 

Do contr�rio, ao omitir o par�metro cModo, ser� avaliado entre as tabelas de aTable, se todos
os modos de acesso s�o iguais.

S�o os modos de acesso: Por empresa, por unidade de neg�cio ou por filial.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		09/05/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------

Function TurChkModo(aTables,cModo)

Local cTables		:= ""
Local cTextModo		:= ""
Local cMsg			:= ""

Local nI			:= 0

Local aAux 			:= TurRetModoTab(aTables)

Local lRet			:= .t.

Default cModo		:= ""

If ( Empty(cModo) )

	aEval(aAux, {|x| cTables += x[1] + ", " })
	
	cMsg += "Tabelas " + Substr(cTables,1,Rat(",",cTables)-1) + "."
	
	For nI := 1 To Len(aAux)-1
	
		If ( !fCompArray(aAux[nI,2], aAux[nI+1,2]) )
			
			lRet := .f.
			
			cMsg += STR0055		// " Estas Tabelas n�o possuem modos de acesso "
			cMsg += STR0056		// "(Empresa, Unidade de Neg�cio e Filial que sejam compartilhados ou exclusivos) equivalentes. "
			cMsg += STR0057		// "Entrem em contato com o administrador do sistema."
			
			FwAlertHelp(STR0058, cMsg)		// "Modos de acesso das tabelas divergem."
			Exit
			
		Endif
		
	Next nI	

Else
	
	For nI := 1 to Len(aAux)
		
		If ( aScan(aAux[nI,2], Upper(cModo)) == 0 )
			
			If ( cModo == "E" )
				cTextModo := STR0059		// "Exclusivo"
			Else
				cTextModo := STR0060		// "Compartilhado"
			Endif
			
			cMsg := I18N(STR0061, {aAux[nI,1]})		// "A Tabela #1 possui modo de Acesso "
			cMsg += STR0062 + cTextModo					// "- tanto Empresa, Unidade de neg�cio quanto Filial - que difere (ao menos um) de "  
			
			FwAlertHelp(STR0063, cMsg)					// "Modos de acesso das tabelas divergem."
			
			lRet := .f.
			
			Exit
			
		Endif
			
	Next nI
	
Endif	

Return(lRet)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TurRetModoTab
Fun��o que retorna os modos de acesso de tabelas passadas pelo par�metro aAliases.

Os modos de acesso s�o: Por empresa, por unidade de neg�cio ou por filial.

O array retornado � aModoComp e ele � da seguinte forma:
	aModoComp[n,1]: Alias da Tabela em quest�o -> � proveniente da tabela de Aliases
	aModoComp[n,2]: Array com os modos de Acesso da tabela de aModoComp[n,1]
		aModoComp[n,3][1]: Modo de Acesso Empresa ("E" - Exclusiva, "C" - Compartilhada)
		aModoComp[n,3][2]: Modo de Acesso Unidade de Neg�cio ("E" - Exclusiva, "C" - Compartilhada)
		aModoComp[n,3][3]: Modo de Acesso Filial ("E" - Exclusiva, "C" - Compartilhada)
		
@type 		Function
@author 	Fernando Radu Muscalu
@since 		09/05/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Function TurRetModoTab(aAliases)

Local nI		:= 0

Local aAux		:= {}
Local aModoComp	:= {}

Local lGestao	:= FWSizeFilial() > 2		// Indica se usa Gestao Corporativa
Local lExclusivo:= .F.

Default aAliases	:= {}

For nI := 1 to Len(aAliases)
	
	If ( lGestao )
	
		aAdd(aAux, FWModeAccess(aAliases[nI],1) )	//Modo de Acesso Empresa
		aAdd(aAux, FWModeAccess(aAliases[nI],2) )	//Modo de Acesso Unidade de Neg�cio
		aAdd(aAux, FWModeAccess(aAliases[nI],3) )	//Modo de Acesso Filial
		
	Else
	
		aAdd(aAux, IIf(!Empty(xFilial(aAliases[nI])),"E","C"))
		
	Endif
	
	aAdd(aModoComp, {aAliases[nI], AClone(aAux)})
	aAux := {}
	
Next nI

Return(aModoComp)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TurSetTables

Fun��o respons�vel por carregar o array est�tico aTurTables com todas as tabelas do m�dulo
SIGATUR 

@type 		Function
@author 	Fernando Radu Muscalu
@since 		02/06/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Function TurSetTables(lRefaz)

Local nDel		:= 0
Local nI		:= 0

Local aAreaSX2	:= SX2->(GetArea())

Default lRefaz := .f.

If ( Len(aTurTables) == 0 .or. lRefaz )

	aTurTables := {	"G3A","G3B","G3C","G3D","G3E","G3F","G3G","G3H","G3I","G3J",;
					"G3K","G3L","G3M","G3N","G3O","G3P","G3Q","G3R","G3S","G3T",;
					"G3U","G3V","G3W","G3X","G3Y","G3Z","G40","G41","G42","G43",;
					"G44","G45","G46","G47","G48","G49","G4A","G4B","G4C","G4D",;
					"G4E","G4F","G4G","G4H","G4I","G4J","G4K","G4L","G4M","G4N",;
					"G4O","G4P","G4Q","G4R","G4S","G4T","G4U","G4V","G4W","G4X",;
					"G4Y","G4Z","G50","G51","G5M","G5N","G5O","G5P","G5Q","G5R",;
					"G5S","G5T","G5U","G5V","G5W","G5X","G5Y","G5Z","G60","G61",;
					"G62","G63","G64","G65","G66","G67","G68","G69","G6A","G6B",;
					"G6C","G6D","G6E","G6F","G6G","G6H","G6I","G6J","G6K","G6L",;
					"G6M","G6N","G6O","G6P","G80","G81","G82","G83","G84","G85",;
					"G86","G87","G88","G89","G8A","G8B","G8C","G8D","G8E","G8F",;
					"G8G","G8H","G8I","G8J","G8K","G8L","G8M","G8N","G8O","G8P",;
					"G8Q","G8R","G8S","G8T","G8U","G8V","G8W","G8X","G8Y","G8Z",;
					"G90","G91","G92","G93","G9E","G9F","G9G","G9H","G9I","G9J",;
					"G9K","G9L","G9M","G9N"}
	
	SX2->(DbSetOrder(1))

	For nI := 1 to Len(aTurTables)
		
		If ( aTurTables[nI] <> Nil .And. !SX2->(DbSeek(aTurTables[nI])) )
			aDel(aTurTables,nI)
			nDel++
		Endif
		
	Next nI
	
	If ( nDel > 0 )
		aSize(aTurTables, Len(aTurTables) - nDel)
	Endif
	
	RestArea(aAreaSX2)

Endif

Return()

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TurGetTables

Fun��o respons�vel por retornar o array est�tico aTurTables. 
@type 		Function
@author 	Fernando Radu Muscalu
@since 		02/06/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------

Function TurGetTables(cProcesso)

Local nI	:= 0
Local nDel	:= 0

Local aRetTables	:= {}

Default cProcesso := "TURA040|TURA043|TURA050"

TurSetTables()

If ( cProcesso $ "TURA040|TURA043|TURA050" )
	
	aRetTables := {"G6L","G6M","G6O","G6N","G6P","G80","G81","G82"}
	
	For nI := 1 to Len(aRetTables)
		
		If ( aScan(aTurTables, aRetTables[nI] ) == 0 )
			ADel(aRetTables,nI)
			nDel++
		Endif
		
	Next nI	
	
ElseIf ( Empty(cProcesso) )
	aRetTables := AClone(aTurTables)
Endif	

If ( nDel > 0 )
	ASize(aRetTables, Len(aRetTables) - nDel)
Endif

Return(aRetTables)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TURGetComb
Converte o combo de um campo em array

@type 		Function
@author 	jose Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------

Function TURGetComb(cCampo,nOpcao)
Local aCombo 		:= {}
Local cCombo 		:= {}
Local aCombSprin	:= {}
Local aArea	 	:= GetArea()
Local aAreaSX3 	:= SX3->(GetArea())
Local nX			:= 0

Default nOpcao 	:= 1

SX3->(dbSetOrder(2))
If SX3->(MsSeek(cCampo))
	cCombo := SX3->( X3CBox() )
	aCombo := StrTokArr ( cCombo , ';' )
	For nx := 1 To Len(aCombo)
		aAdd(aCombSprin, StrTokArr(aCombo[nX],'=')) 
	Next
EndIf

RestArea(aAreaSX3)
RestArea(aArea)

Do Case
	Case nOpcao == 1 
		Return aCombo
	Case nOpcao == 2 
		Return aCombSprin
EndCase 

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TURArr2In
Converte um array na express�o de IN

@type 		Function
@author 	Jose Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------

Function TURArr2In(aAux)

Local cCombIn		:= ""
Local nX			:= 0
		
For nx := 1 To Len(aAux)
	If nX == 1
		cCombIn := "('"+aAux[nX]+"'"
	Else
		cCombIn += ",'"+aAux[nX]+"'"
	EndIf
Next

If Len(aAux) > 0 
	cCombIn := cCombIn+")"
EndIf

Return cCombIn


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TurTamSXG
Converte um array na express�o de IN

@type 		Function
@author 	Jose Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TurTamSXG(cGrupo,nTamPad)

Local nSize 	:= 0
Local aFilInfo	:= FWArrFilAtu()
Local aArea		:= GetArea()

DbSelectArea("SXG")
SXG->(DbSetOrder(1))

If SXG->(DbSeek(cGrupo))
	nSize := (aFilInfo[8])
Else
	nSize := nTamPad
Endif

RestArea(aArea)

Return nSize


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TurRngFil
Converte sele��o de filiais na express�o de IN

@type 		Function
@author 	Jose Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TurRngFil( xSelFil , cAlias )
Local cRetorno 		:= ""
Local aArea			:= GetArea()
Local nX				:= 0
Local aStruct 		:= {}
Local nTamFil 		:= TurTamSXG("033",2)  			// Grupo ; tamanho padrao
Local lGestao			:= FWSizeFilial() > 2		// Indica se usa Gestao Corporativa
Local aModoComp 		:= {}
Local lExclusivo 		:= .F.
Local cAtualxFil		:= ""

If lGestao
	aAdd(aModoComp, FWModeAccess(cAlias,1) )
	aAdd(aModoComp, FWModeAccess(cAlias,2) )
	aAdd(aModoComp, FWModeAccess(cAlias,3) )
	lExclusivo := Ascan(aModoComp, 'E') > 0
Else
	dbSelectArea(cAlias)
	lExclusivo := !Empty(xFilial(cAlias))
EndIf

If Valtype(xSelFil) == "A"
	If lExclusivo //cAlias em modo exclusivo
		If Len(xSelFil) > 1
			If lGestao
				cFilCpy := cFilAnt
				For nX := 1 to Len(xSelFil)
					cFilAnt := xSelFil[nX]
					dbSelectArea(cAlias)
					cAtualxFil := xFilial(cAlias)
					cRetorno += cAtualxFil + "|" 	
				Next nX                      
				cRetorno := Left(cRetorno, Len(cRetorno) - 1 )
				cRetorno := "IN " + FormatIn(cRetorno,"|")
				cFilAnt := cFilCpy
			Else
				For nX := 1 to Len(xSelFil)
					cRetorno += xSelFil[nX] + "|" 	
				Next nX                      
				cRetorno := Left(cRetorno, Len(cRetorno) - 1 )
				cRetorno := "IN " + FormatIn(cRetorno,"|") 
			EndIf
		Else
			If lGestao
				cFilCpy := cFilAnt
				If Len(xSelFil) > 0
					cFilAnt := xSelFil[1]
				EndIf
				dbSelectArea(cAlias)
				cAtualxFil := xFilial(cAlias)
				cRetorno := " = '" + cAtualxFil + "' " 				
				cFilAnt := cFilCpy
			Else
				If Len(xSelFil) > 0
					cRetorno := " = '" + xSelFil[1] + "' " 	
				Else
					cRetorno := " = '" + xFilial(cAlias) + "' " 	
				EndIf
			EndIf
		EndIf
	Else
		//cAlias em modo compartilhado	
		dbSelectArea(cAlias)
		cRetorno := " = '" + xFilial(cAlias) + "' " 	
	EndIf	
Else
	cRetorno := " = '" + xFilial(cAlias,xSelFil) + "' "
EndIf          

RestArea(aArea)
	
Return cRetorno


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TLBoxOper
Listbox para sele��o das Opera��es

@type 		Function
@author 	Jose Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TLBoxOper()

Local cVar     := Nil
Local oDlg     := Nil
Local lMark    := .F.
Local cTitulo  := STR0023 //"Sele��o de Opera��es"
Local oOk      := LoadBitmap( GetResources(), "LBOK" )   //CHECKED    //LBOK  //LBTIK
Local oNo      := LoadBitmap( GetResources(), "LBNO" ) //UNCHECKED  //LBNO
Local oChk     := Nil
Local oLbx     := Nil
Local aVetor   := {}
Local lChk     := .F.
Local aDescOp  := TURGetComb("G3Q_OPERAC",2)

//+-------------------------------------+
//| Carrega o vetor conforme a condicao |
//+-------------------------------------+
aAdd( aVetor, { lMark, "1", aDescOp[Ascan(aDescOp,{ |x| x[1] == "1" })][2] })
aAdd( aVetor, { lMark, "3", aDescOp[Ascan(aDescOp,{ |x| x[1] == "3" })][2] })
aAdd( aVetor, { lMark, "4", aDescOp[Ascan(aDescOp,{ |x| x[1] == "4" })][2] })

DEFINE MSDIALOG oDlg TITLE cTitulo FROM 0,0 TO 240,500 PIXEL
   
@ 10,10 LISTBOX oLbx VAR cVar FIELDS HEADER " ", TURX3Title("G3Q_OPERAC"), STR0003 ; //"Descri��o"
   SIZE 230,095 OF oDlg PIXEL ON dblClick(aVetor[oLbx:nAt,1] := !aVetor[oLbx:nAt,1],oLbx:Refresh())

oLbx:SetArray( aVetor )
oLbx:bLine := {|| {Iif(aVetor[oLbx:nAt,1],oOk,oNo),;
                       aVetor[oLbx:nAt,2],;
                       aVetor[oLbx:nAt,3]}}
	 
//+----------------------------------------------------------------
//| Para marcar e desmarcar todos existem duas op�oes, acompanhe...
//+----------------------------------------------------------------
//| Chamando uma funcao pr�pria
//+----------------------------------------------------------------
@ 110,10 CHECKBOX oChk VAR lChk PROMPT STR0027+"/"+STR0028 SIZE 60,007 PIXEL OF oDlg; //Marca/Desmarca
          ON CLICK(Iif(lChk,TLbxMarca(@oLbx, @aVetor, lChk),TLbxMarca(@oLbx, @aVetor, lChk)))


DEFINE SBUTTON FROM 107,213 TYPE 1 ACTION oDlg:End() ENABLE OF oDlg

ACTIVATE MSDIALOG oDlg CENTER

Return aVetor

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TLbxMarca
Fun��o para marca��o e desmarca��o de todos os itens da listbox

@type 		Function
@author 	Jose Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TLbxMarca(oLbx, aVetor, lMarca)
Local nX := 0

For nX := 1 To Len(aVetor)
   aVetor[nX][1] := lMarca
Next nX

oLbx:Refresh()

Return()

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TurFormFil
Fun��o para posicionar na primeira filial ou unidade de neg�cio existente.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		24/05/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TurFormFil(cFilToForm)

Local aFilInfo	:= FWArrFilAtu()

Local cEmp 		:= ""
Local cUng 		:= ""
Local cFil 		:= ""
Local cRet		:= ""

Local nEmp		:= Len(aFilInfo[13])
Local nUNg		:= Len(aFilInfo[14])
Local nFil		:= Len(aFilInfo[15])
Local nRec		:= 0

cEmp := Substr(cFilToForm,1,nEmp)
cUng := Substr(cFilToForm,nEmp+1,nUNg)
cFil := Substr(cFilToForm,nEmp+nUNg+1,nFil)

nRec := SM0->(Recno())

SM0->(DbGoTop())

If ( !Empty(cEmp) .And. (Empty(cUNg)) .Or. Empty(cFil) )

	While SM0->(!Eof())
		
		If ( SM0->M0_CODIGO == cEmp )
			
			If ( !Empty(cUNg))
			
				If ( Empty(cFil) )
					cRet := Alltrim(SM0->M0_CODFIL)
					Exit
				Endif
			
			Else
				cRet := Alltrim(SM0->M0_CODFIL)
				Exit
			Endif
			
		Endif
		
		SM0->(DbSkip())
	
	EndDo

Else
	cRet := cFilToForm
Endif

SM0->(DbGoto(nRec))

Return(cRet)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA50ExistX1
Fun��o utilizada para saber se existe uma grupo de perguntas

@type 		Function
@author 	Fernando Radu Muscalu
@since 		01/06/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Function TURExistX1(cGrupoPerg)

Local lRet	:= .f.

Local aAreaX1	:= SX1->(GetArea())

lRet := SX1->(DbSeek(cGrupoPerg))

RestArea(aAreaX1)

Return(lRet)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TxGetNdXml()
Fun��o utilizada para validar e pegar o valor da Tag.

@type 		Function
@author 	Jacomo Lisa
@since 	16/05/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Function TxGetNdXml(oXml,cNode,oModel,cCampo,xVal )
Local lRet	:= .T.
Default xVal:= nil 
If oXml:XPathHasNode( cNode ) .or. "INTERNALID" $ UPPER(cNode)     
	If Valtype(xVal) == 'U'
		xVal := TxRetVal(oXml:XPathGetNodeValue(cNode),cCampo)
	Endif
	lRet	:= TxSetVal(oModel,cCampo,xVal) 
Endif

Return lRet
//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TxSetVal()
Fun��o utilizada para validar e carregar os valores no campo.

@type 		Function
@author 	Jacomo Lisa
@since 	16/05/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------

Function TxSetVal(oModel,cCampo,xValue)
Local lRet := .T.
If ValType(aSetCpos) <> "A" .or. Len(aSetCpos) == 0 
	aSetCpos := {'G3Q_CLIENT','G3R_FORNEC','G3R_FORREP','G3T_CODFOR','G3T_LOJAF','G3T_DTSAID','G3T_HRINI' ,;
				'G3U_DTINI','G3U_HRINI','G3V_DTINI','G3V_HRINI','G3W_LINHA','G3W_DTINI','G3W_HRINI',;
				'G3X_DTINI','G3X_HRINI','G3X_HRFIM','G3Y_DTINI','G3Y_HRINI','G3Z_DTINI','G3Z_HRINI',;
				'G40_DTINI','G40_HRINI','G41_DTINI','G43_DTINI','G43_DTFIM' }
Endif
If oModel:GetValue(cCampo) <> xValue 
	If ascan(aSetCpos,{|x| x == Alltrim(cCampo) }) > 0  
		TI034Ajust(oModel,cCampo)
	Endif
	lRet := oModel:SetValue(cCampo,xValue)
Endif

Return lRet
//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TxSetNode()
Fun��o utilizada para validar e carregar os valores no campo.

@type 		Function
@author 	Jacomo Lisa
@since 	16/05/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Function TxSetNode(cNode,cValue,lForce)
Local cRet := ""
Default cValue := ""
Default lForce := .F.
If !Empty(cValue) 
	cRet := '<'+Alltrim(cNode)+'>' + Rtrim(cValue) +'</'+Alltrim(cNode)+'>'
ElseIf lForce
   cRet := '<'+Alltrim(cNode)+'/>' 
Endif

Return cRet
//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TxRetVal()
Fun��o utilizada para retornar valores validos para o campo

@type 		Function
@author 	Jacomo Lisa
@since 	16/05/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Function TxRetVal(xVal,cCampo)
Local xRet := nil
SX3->(DBSETORDER(2))
If SX3->(DBSEEK(cCampo))
	If SX3->X3_TIPO == 'C'
		xRet := Padr(Alltrim( xVal ),SX3->X3_TAMANHO  )	
	ElseIf SX3->X3_TIPO == 'D'
		xRet := TxDtStamp(Alltrim(xVal),.F.)
	ElseIf SX3->X3_TIPO == 'N'
		xRet := Val(Alltrim(xVal))
	ElseIf SX3->X3_TIPO == 'L'
		xRet := TURXLogic(Alltrim(xVal), TP_LOGIC_RET) 
	ElseIf SX3->X3_TIPO == 'M'
		xRet := Alltrim( xVal )	
	ENDIF
Endif
Return xRet

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TxValCpo()
Fun��o utilizada para validar se o campo existe na estrutura

@type 		Function
@author 	Jacomo Lisa
@since 	16/05/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Function TxValCpo(aEstru,cCampo)
Return ascan(aEstru,{|x| x[3] == Alltrim(cCampo) }) > 0  


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TLBoxMoed
Listbox para sele��o das Moedas

@type 		Function
@author 	Jose Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TLBoxMoed()

Local cVar     := Nil
Local oDlg     := Nil
Local lMark    := .F.
Local cTitulo  := STR0026 //"Sele��o de Moedas"
Local oOk      := LoadBitmap( GetResources(), "LBOK" ) //CHECKED    //LBOK  //LBTIK
Local oNo      := LoadBitmap( GetResources(), "LBNO" ) //UNCHECKED  //LBNO
Local oChk     := Nil
Local lChk		 := .F.
Local oLbx 	 := Nil
Local aVetor	 := {}

//+-------------------------------------+
//| Carrega o vetor conforme a condicao |
//+-------------------------------------+
DbSelectarea("G5T")
G5T->(DbSetOrder(1))
G5T->(DbGoTop())
While G5T->(!Eof())
	aAdd( aVetor, { lMark, G5T->G5T_CODIGO, G5T->G5T_DESCR })
	G5T->(DbSkip())
EndDo

DEFINE MSDIALOG oDlg TITLE cTitulo FROM 0,0 TO 240,500 PIXEL
   
@ 10,10 LISTBOX oLbx VAR cVar FIELDS HEADER " ", TURX3Title("G5T_CODIGO"), TURX3Title("G5T_DESCR") ;
   SIZE 230,095 OF oDlg PIXEL ON dblClick(aVetor[oLbx:nAt,1] := !aVetor[oLbx:nAt,1],oLbx:Refresh())

oLbx:SetArray( aVetor )
oLbx:bLine := {|| {Iif(aVetor[oLbx:nAt,1],oOk,oNo),;
                       aVetor[oLbx:nAt,2],;
                       aVetor[oLbx:nAt,3]}}
	 
//+----------------------------------------------------------------
//| Para marcar e desmarcar todos existem duas op�oes, acompanhe...
//+----------------------------------------------------------------
//| Chamando uma funcao pr�pria
//+----------------------------------------------------------------
@ 110,10 CHECKBOX oChk VAR lChk PROMPT STR0027+"/"+STR0028 SIZE 60,007 PIXEL OF oDlg; //Marca/Desmarca
          ON CLICK(Iif(lChk,TLbxMarca(@oLbx, @aVetor, lChk),TLbxMarca(@oLbx, @aVetor, lChk)))

DEFINE SBUTTON FROM 107,213 TYPE 1 ACTION oDlg:End() ENABLE OF oDlg

ACTIVATE MSDIALOG oDlg CENTER

Return aVetor


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TURX3Title
Fun��o para resgatar a descri��o do campo do X3

@type 		Function
@author 	Jose Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TURX3Title(cCampo)

Local aArea	:= GetArea()
Local cTitulo	:= ""

Default cCampo := "#"

DbSelectArea("SX3")
SX3->(DbSetOrder(2))
If SX3->(DbSeek(cCampo))
   cTitulo := X3Titulo()
EndIf

RestArea(aArea)

Return cTitulo

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURxMkDir()

Cria��o de diret�rio conforme Empresa, Unidade e Filial

@sample 	TURA065()
@return     oView                       
@author  	Servicos
@since   	05/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Function TURxMkDir(lPathServer,lServerMirror)

Local aFilAtu   	:= FWArrFilAtu()
Local aPath			:= {}

Local cCaminho  	:= AllTrim(SuperGetMV("MV_TURDIR",,""))
Local cPathServer	:= ""
Local cDirEmp   	:= TRXSTRTRAN(aFilAtu[3])
Local cDirUni   	:= TRXSTRTRAN(aFilAtu[4])
Local cDirFil   	:= TRXSTRTRAN(aFilAtu[5])
Local cRetorno		:= ""

Local lInServer		:= .f.
Local lRet			:= .t.

Local nI			:= 0

Default lPathServer		:= .F.	
Default lServerMirror 	:= .T.

If ( !Empty(cCaminho) )
	
	If ( Substr(cCaminho,Len(cCaminho),1) == "\" )
		cCaminho := Substr(cCaminho,1,Len(cCaminho)-1)
	Endif

Else
	lInServer := .t.		
Endif

If !Empty(cDirEmp)
	cCaminho += "\"+cDirEmp	
EndIf

If !Empty(cDirUni)	
	cCaminho += "\"+cDirUni	
EndIf

If !Empty(cDirFil)
	cCaminho += "\"+cDirFil
EndIf

If ( !lInServer .And. FWMakeDir(cCaminho) )
	
	If ( lServerMirror )
	
		If ( At(":\",cCaminho) > 0 )
			cPathServer := SubStr(cCaminho,At(":\",cCaminho)+1)
		Else
			cPathServer := cCaminho
		Endif
		
		lRet := FWMakeDir(cPathServer)
		
	Endif

Else	

	If ( lInServer)
		lRet := FWMakeDir(cCaminho) 
	Endif
	
Endif

If ( !lRet )
	cCaminho := ""
Endif 

If ( lPathServer )
	cRetorno := cPathServer
Else
	cRetorno := cCaminho
EndIf

Return(cRetorno)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TURXG8U

Fun��o respons�vel por retornar um array com as linhas do layout GDS Sabre 

@sample	TURXG8U(cGDS, cVersao)
@param 		cGDS    : String - c�digo do GDS (1-Sabre / 2-Amadeus)
			cVersao: String - c�digo da Versao do arquivo que est� sendo integrado 
@return 	aG8U: Array - lista com os registros da tabela G8U para o c�digo GDS informado  
@author    Thiago Tavares
@since     16/05/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TURXG8U(cGDS, cVersao)

Local aArea   := GetArea()
Local nx      := 0
Local aG8U    := {} 
Local aLinhas := {}

DbSelectArea('G8U')
G8U->(DbSetOrder(2))		// G8U_FILIAL+G8U_VERSAO+G8U_GDS+G8U_SEQUEN
If G8U->(DbSeek(xFilial('G8U') + PadR(cVersao, TamSx3('G8U_VERSAO')[1], ' ') + cGDS))
	While G8U->(!Eof())
		If G8U->G8U_ATIVO == '1'
			aLinhas := {}
			aAdd(aLinhas, {'G8U_GDS'   , G8U->G8U_GDS   })
			aAdd(aLinhas, {'G8U_VERSAO', G8U->G8U_VERSAO})
			aAdd(aLinhas, {'G8U_SEQUEN', G8U->G8U_SEQUEN})
			aAdd(aLinhas, {'G8U_DADO'  , G8U->G8U_DADO  })
			aAdd(aLinhas, {'G8U_TABELA', G8U->G8U_TABELA})
			aAdd(aLinhas, {'G8U_CAMPO' , G8U->G8U_CAMPO })
			aAdd(aLinhas, {'G8U_VARIAV', G8U->G8U_VARIAV})
			aAdd(aLinhas, {'G8U_CONTEX', G8U->G8U_CONTEX})
			aAdd(aLinhas, {'G8U_BLOCO' , G8U->G8U_BLOCO })
			aAdd(aLinhas, {'G8U_INICIO', G8U->G8U_INICIO})
			aAdd(aLinhas, {'G8U_TAMANH', G8U->G8U_TAMANH})
			aAdd(aLinhas, {'G8U_TAG'   , G8U->G8U_TAG   })
			aAdd(aLinhas, {'G8U_TAGMUL', G8U->G8U_TAGMUL})
			aAdd(aLinhas, {'G8U_PASSAG', G8U->G8U_PASSAG})
			aAdd(aLinhas, {'G8U_SEPARA', G8U->G8U_SEPARA})
			aAdd(aLinhas, {'G8U_POSICA', G8U->G8U_POSICA})
			aAdd(aLinhas, {'G8U_FREAD' , G8U->G8U_FREAD })
			aAdd(aLinhas, {'G8U_SEEK'  , G8U->G8U_SEEK  })
			aAdd(aLinhas, {'G8U_ALIAS' , G8U->G8U_ALIAS })
			aAdd(aLinhas, {'G8U_INDICE', G8U->G8U_INDICE})
			aAdd(aLinhas, {'G8U_CHAVE' , G8U->G8U_CHAVE })
			aAdd(aLinhas, {'G8U_RETURN', G8U->G8U_RETURN})
			aAdd(aLinhas, {'G8U_FPOS'  , G8U->G8U_FPOS  })
			aAdd(aLinhas, {'G8U_TIPO'  , G8U->G8U_TIPO  })
			aAdd(aLinhas, {'G8U_SUBINI', G8U->G8U_SUBINI})
			aAdd(aLinhas, {'G8U_SUBTAM', G8U->G8U_SUBTAM})
			aAdd(aLinhas, {'G8U_FFORM' , G8U->G8U_FFORM })
			aAdd(aLinhas, {'G8U_WHEN'  , G8U->G8U_WHEN  })
			aAdd(aLinhas, {'G8U_MACFIX', G8U->G8U_MACFIX})
			aAdd(aLinhas, {'G8U_INCREM', G8U->G8U_INCREM})
			aAdd(aLinhas, {'G8U_OBRIGA', G8U->G8U_OBRIGA})
			aAdd(aG8U, aClone(aLinhas))
		EndIf	
		G8U->(DbSkip())
	EndDo
EndIf
G8U->(DbCloseArea())

RestArea(aArea)

Return aG8U

//------------------------------------------------------------------------------
/*/{Protheus.doc} TURXG8UDET

Fun��o respons�vel por retornar um array com os dados de um linha do layout GDS  

@sample	TURXG8UDET(aG8U, cDado, cFind)
@param 		aG8U: Array - dados da tabela Layout GDS
			cDado: String - indica se procura por 1-Campo ou 2-Vari�vel
			cFind: String - nome da vari�vel ou do campo que ter� os dados retornado
			cWhen: String - texto a ser validado no campo G8U_WHEN
@return 	aG8U: Array - lista com os registros da tabela G8U para o c�digo GDS informado  
@author    Thiago Tavares
@since     16/05/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TURXG8UDET(aG8U, cDado, cFind, cWhen)

Local nX      := 0
Local nPos    := aScan(aG8U[1], {|x| x[1] == 'G8U_CAMPO'})	// 1-Campo
Local nPosW   := aScan(aG8U[1], {|x| x[1] == 'G8U_WHEN' })
Local aLinha  := Nil

Default cWhen := '' 

If cDado == '2'
	nPos := aScan(aG8U[1], {|x| x[1] == 'G8U_VARIAV'})  
EndIf

For nX := 1 To Len(aG8U)
	If AllTrim(aG8U[nX][nPos][2]) == cFind .And. IIf(Empty(cWhen), .T., cWhen $ AllTrim(aG8U[nX][nPosW][2])) 
		aLinha := aClone(aG8U[nX])
		Exit
	EndIf
Next nX

Return aLinha


/*/{Protheus.doc} TurGetNatur
Utilizada na mata461.prx para obter a natureza das NF de sa�da do m�dulo de Turismo
@type function
@author Anderson Toledo
@since 17/06/2016
@version 1.0
/*/
Function TurGetNatur()
Return cTurNaturez

/*/{Protheus.doc} TurSetNatur
Utilizada para atualizar o valor da vari�vel static cTurNaturez
@type function
@author Anderson Toledo
@since 17/06/2016
@version 1.0
/*/
Function TurSetNatur( cNaturez )
	cTurNaturez := cNaturez
Return	


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TURCToL
Fun��o para converter Caracter em L�gico 

@type 		Function
@author 	Jose Domingos Caldana Jr
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------

Function TURCTOL(cConteudo)

Local lRet := Nil

If Len(cConteudo) == 1 
	If cConteudo == "T"
		lRet := .T.
	ElseIf cConteudo == "F"
		lRet := .F.
	EndIf 
EndIf

Return lRet



/*/{Protheus.doc} TURxGetVls
Fun��o que serializa todos os dados de um modelo e seus submodelos em um vetor
@type function
@author Anderson Toledo
@since 16/03/2016
@version 1.0
@param cIdCopy, character, Id do modelo que ser� serializado
@param lSoAtual, booleano, Se for um grid, indica que dever� ser copiado apenas a linha atual
@param aNoCopy, array, Vetor com ID's que n�o ser�o copiados
@param aLines, array de 2 posi��es, na 1� posi��o � o ID's do Model e 2� posi��o as linhas que devem ser copiadas
@param aCond, array de 2 posi��es, na 1� posi��o � o ID's do Model e 2� posi��o um vetor onde a primeira posi��o [1] � o campo para compara��o e a segunda posi��o o codeblock a ser executado, o valor do campo ser� passado via parametro ex.: { { 'G4CA_ITENS', { { 'G4C_STATUS', {|x| x == '1'}, { 'G4C_TPCONC', {|x| x == '2'} } } } }  
@return array, Vetor com todos os dados do modelo e submodelos serializados
/*/
Function TURxGetVls( cIdCopy, lSoAtual, aNoCopy, aLines, oModel, aNoAddLine, aCond )

Local aRetValues	:= {cIdCopy,{}}
Local aModelStruct 	:= {}
Local aModelValues	:= {{},.T.}
Local aDependency		:= {}
Local aValuesDep		:= {}
Local lCond			:= .T.
Local nX			:= 0
Local nY			:= 0
Local nJ			:= 0
Local nPos			:= 0
Local oModelAux	:= nil
Default lSoAtual 	:= .F.
Default aNoCopy		:= {}
Default aLines		:= {}		
Default oModel 		:= FwModelActive()
Default aNoAddLine	:= {}
Default aCond		:= {}

oModelAux	:= oModel:GetModel( cIdCopy )

If aScan( aNoCopy, {|x| x == cIdCopy} ) > 0
	Return aRetValues
EndIf 

aDependency 	:= oModel:GetDependency( cIdCopy )
aModelStruct 	:= oModelAux:GetStruct():GetFields()

If !lSoAtual .And. oModelAux:ClassName() == "FWFORMGRID"
	For nX := 1 to oModelAux:Length()
		
		//Tratamento para copia de linhas especificas
		If Len(aLines) > 0 										//Se mandar array em branco, copia todas as linhas
			nPos := Ascan( aLines, {|x| x[1] == cIdCopy} )		//Procura se tem tratamento de linhas para o model em quest�o
			If nPos > 0											//Se achar o model no array, s� copia as linhas que existe
				If Ascan( aLines[nPos,2], {|y| y == nX} ) <= 0
					Loop
				EndIf
			EndIf
		EndIf
		
		
		oModelAux:GoLine( nX )
		
		//Tratamento para c�pia com condi��o
		If len( aCond ) > 0
			nPos := Ascan( aCond, {|x| x[1] == cIdCopy } )		//Procura se tem tratamento de condi��o para o model em quest�o
			
			If nPos > 0											
				 lCond	:= .T.
				For nY := 1  to len(aCond[nPos,2])				
					If !Eval(  aCond[nPos,2,nY,2], oModelAux:GetValue( aCond[nPos,2,nY,1] ) ) //Via macroexecu��o, executa a condi��o
						lCond	:= .F.
						Exit
					EndIf
				Next
				
				If !lCond
					Loop
				EndIf
			EndIf
		EndIf	
		
		
		

		aModelValues	:= {{},.T.}
		aValuesDep		:= {}
		
		
		
		If ( oModelAux:ClassName() == "FWFORMGRID" .And. !oModelAux:IsEmpty() .And. !oModelAux:IsDeleted() ) .Or. oModelAux:ClassName() == "FWFORMFIELDS"

			//Verifica se ser� adicionada uma nova linha ou ser� alterada a linha corrente
			If nX == 1
				aModelValues[2] := IIF( aScan( aNoAddLine, {|x| x == cIdCopy } ) > 0, .F., .T. ) //Indica se ser� adicionada uma nova linha
			Else
				aModelValues[2] := .T.
			EndIf

			For nY := 1 to len( aModelStruct )
				aAdd( aModelValues[1], { AllTrim( aModelStruct[nY][3] ), oModelAux:GetValue( aModelStruct[nY][3] ) } )
			Next

			For nJ := 1 to len( aDependency )
				aAdd( aValuesDep, TURxGetVls( aDependency[nJ][2], .F., aNoCopy, aLines, oModel, aNoAddLine, aCond)  )
			Next

			aAdd( aRetValues[2], {aModelValues, aValuesDep} )
			
		EndIf
		
	Next

Else

	If ( oModelAux:ClassName() == "FWFORMGRID" .And. !oModelAux:IsEmpty() .And. !oModelAux:IsDeleted() ) .Or. oModelAux:ClassName() == "FWFORMFIELDS"
		aModelValues[2] := IIF( aScan( aNoAddLine, {|x| x == cIdCopy } ) > 0, .F., .T. ) //Indica se ser� adicionada uma nova linha
		
		For nX := 1 to len( aModelStruct )
			aAdd( aModelValues[1], { AllTrim( aModelStruct[nX][3] ), oModelAux:GetValue( aModelStruct[nX][3] ) } )
		Next
	

		For nJ := 1 to len( aDependency )
			aAdd( aValuesDep, TURxGetVls( aDependency[nJ][2], .F., aNoCopy, aLines, oModel, aNoAddLine, aCond)  )
		Next

		aAdd( aRetValues[2], {aModelValues, aValuesDep} )
	
	EndIf
	
EndIf


Return aRetValues


/*/{Protheus.doc} TURxSetVs
Fun��o para apendar ao modelo os dados seriados pela fun��o TURxGetVls
@type function
@author Anderson Toledo
@since 16/03/2016
@version 1.0
@param aCopyValues, array, vetor com valores serializados pela fun��o TURxGetVls
/*/
Function TURxSetVs( aCopyValues, aNoCampos, oModel )

Local nX 		:= 0
Local nY 		:= 0
Local oObj		:= nil
Local lRet		:= .T.

Default aNoCampos	:= {}
Default oModel 	:= FwModelActive()

If len(aCopyValues) >= 2
	For nX := 1 To Len( aCopyValues[2] )
		If Len( aCopyValues[2][nX][1] ) > 0
			oObj := oModel:GetModel( aCopyValues[1] )
	
			If ValType( oObj ) == 'O'
				If oObj:ClassName() == "FWFORMGRID"
					lRet := TURxLoadV(oModel,aCopyValues[1],aCopyValues[2][nX][1][1], aCopyValues[2][nX][1][2], aNoCampos )
				Else
					lRet := TURxLoadV(oModel,aCopyValues[1],aCopyValues[2][nX][1][1], .F., aNoCampos )
				EndIf
	
				If lRet
					For nY := 1 To Len( aCopyValues[2][nX][2] )
						lRet := TURxSetVs( aCopyValues[2][nX][2][nY] , aNoCampos, oModel)
					Next
				EndIf
			EndIf
		EndIf
	Next
EndIf

Return lRet



/*/{Protheus.doc} TURxLoadV
Realiza o LoadValue no modelo de dados informado com as informa��es passadas via parametro
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Function TURxLoadV(oMdlMaster,cIdModel,aDados,lLinha,aNoCampos)
Local lRet		:= .T.
Local nX		:= 0
Local nPos		:= 0

Local oModel 	  := oMdlMaster:GetModel(cIdModel)
Local lCanInsLine := !(IIF(oModel:ClassName() == "FWFORMGRID", oModel:CanInsertLine(), .F.))	
Local oStru	      := oModel:GetStruct()

Default lLinha	:= .F.
Default aNoCampos := {}

IIF(oModel:ClassName() == "FWFORMGRID", oModel:SetNoInsertLine( .F. ), )

If ( lLinha .And. !oModel:IsEmpty() ) .Or. ( oModel:ClassName() == 'FWFORMGRID' .And. oModel:IsDeleted() ) 

	If oModel:Length() == oModel:AddLine()
		Return .F.
	EndIf

EndIf

For nX := 1 To Len( aDados )
	
	// Verifica se os campos passados existem na estrutura do modelo
	If oStru:HasField( aDados[nX][1] )
		nPos := Ascan( aNoCampos, aDados[nX][1] )	//Verifica se o campo � um dos que n�o deve ser copiado
		If nPos <= 0
			If (FwIsInCallStack("TURXACMN") .Or. FwIsInCallStack("TA039AcCan") .Or. FwIsInCallStack("TA039VerAc") .Or. FwIsInCallStack("TA39IGerAc")) .And. "G48" $ cIdModel .And. AllTrim(aDados[nX][1]) $ "G48_APLICA|G48_COMSER|G48_CODAPU|G4C_CODAPU"
				Do Case
					Case AllTrim(aDados[nX][1]) == "G48_APLICA"
						lRet := oMdlMaster:LoadValue(cIdModel, aDados[nX][1], StrZero(T034NxtApl(oMdlMaster:GetModel(cIdModel), '1', Val(aDados[nX][2])), TamSx3("G48_APLICA")[1]))  

					Case AllTrim(aDados[nX][1]) == "G48_COMSER"
						lRet := oMdlMaster:LoadValue(cIdModel, aDados[nX][1], "1")
					
					Case AllTrim(aDados[nX][1]) $ "G48_CODAPU|G4C_CODAPU"
						lRet := oMdlMaster:LoadValue(cIdModel, aDados[nX][1], "")  
				EndCase

				If !lRet  
					Exit
				EndIf

			ElseIf !(lRet := oMdlMaster:LoadValue(cIdModel, aDados[nX][1], aDados[nX][2]))
				Exit
			EndIf
		EndIf
	EndIf
Next

IIF(oModel:ClassName() == "FWFORMGRID", oModel:SetNoInsertLine( lCanInsLine ), )

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TURSearch
fun��o de busca que dever� substituir os DBSeek das tabelas que possuem o campo CONINU 

@type 		Function
@author 	Fernando Radu Muscalu
@since 		01/07/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------

Function TURSearch(cAlias,nInd,cChave)

Local cFilInutil	:= "" 
Local cIndexKey		:= (cAlias)->(IndexKey(nInd)) 
Local cWhere		:= ""
Local cFrom			:= ""
Local cNxtAlias		:= ""

Local nI			:= 0
Local nPIni			:= 0
Local nLenKey		:= 0

Local aIndex		:= {}
Local aAux			:= {}

Local lRet 			:= .F.

If ( !Empty(cIndexKey) )
	
	aAux := Separa(cIndexKey,"+")
	
	If ( Len(aAux) > 0 )
		
		nPIni := 1
		
		cWhere := "%"
		
		For nI := 1 to Len(aAux)
			
			nLenKey := TamSx3(Alltrim(aAux[nI]))[1]
			 
			//por hora tratando os campos de �ndice que s�o caracteres
			If ( nI < Len(aAux) )
				cWhere += Alltrim(aAux[nI]) + " = '" + SubStr(cChave,nPIni,nLenKey) + "' "
				cWhere += " AND "
			Else
				
				If ( !Empty(SubStr(cChave,nPIni,nLenKey)) )
					cWhere += Alltrim(aAux[nI]) + " = '" + SubStr(cChave,nPIni,nLenKey) + "' "					
				Else				
					cWhere := SubStr(cWhere,1,Rat("AND",cWhere)-3)
				Endif
					
			Endif
			
			nPIni += nLenKey
			
		Next nI
		
		cWhere += "%" 
		
	Endif
	
Endif

//Pesquisa se a tabela (da variavel cAlias) possui o campo CONINU - > inutiliza��o de registro
If ( (cAlias)->( FieldPos(PrefixoCpo(cAlias)+"_CONINU") ) > 0 )
	
	cFilInutil 	:= "%" + PrefixoCpo(cAlias)+"_CONINU = '' AND D_E_L_E_T_ = '' %"
	cFrom 		:= "%" + RetSQLName(cAlias) + "%"
	cNxtAlias	:= GetNextAlias()
	
	BeginSQL Alias cNxtAlias
		
		SELECT
			R_E_C_N_O_ RECNRO
		FROM
			%Exp:cFrom%
		WHERE
			%Exp:cWhere% AND 
			%Exp:cFilInutil% 
			
	EndSQL
	
	If ( Select(cNxtAlias) > 0 )
		(cAlias)->(DbGoTo((cNxtAlias)->RECNRO))
		lRet := .T.
	Endif
	
	(cNxtAlias)->(DbCloseArea())
			
Else
	
	(cAlias)->(DbSetOrder(nInd))
	lRet := (cAlias)->(DbSeek(cChave)) 
		
Endif

Return(lRet)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TURXVLDPNR

Fun��o que cria verifica se o arquivo j� foi processado

@sample	TURXVLDPNR(cFilePNR)
@param		cFilePNR: String - nome do arquivo 
@return	lRet: L�gico - se verdadeiro arquivo j� processado
@author    Thiago Tavares
@since     26/06/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TURXVLDPNR(cFilePNR)

Local aArea     := GetArea()
Local cAliasG8V := GetNextAlias()
Local lRet      := .F.

BeginSql Alias cAliasG8V
	SELECT G8V_SEQUEN
	FROM %Table:G8V% G8V
	WHERE G8V_ARQUIV = %Exp:cFilePNR% AND
	      G8V_TIPO = '1' AND
	      G8V.%notDel%
EndSql

If (cAliasG8V)->(!EOF())
	lRet := .T.
EndIf
(cAliasG8V)->(DbCloseArea())

RestArea(aArea)

Return lRet


//------------------------------------------------------------------------------
/*/{Protheus.doc} TURXACMN

Fun��o para cria��o de acerto manual

@sample	TURXACMN(oModel)
@param		oModel: Modelo que ser� utilizado 
@author    Osmar Cioni Estevam Junior
@since     01/08/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TURXACMN(oModel)
Local aCopyValues	:= Nil
Local cSeqG3Q		:= ''
Local aNoCampos	:= {}
Local cSeqOrig := oModel:GetValue( "G3R_ITENS", "G3R_NUMSEQ" )
Local cSeqParc := oModel:GetValue( "G3R_ITENS", "G3R_SEQPRC" )

If oModel:GetModel( "G3Q_ITENS" ):GetValue("G3Q_ACERTO") == '1' //N�o permitido para um Acerto (1)

	Help( , , "TURXACMN", , STR0017, 1, 0) // "N�o � permitido gerar acerto de um acerto."
	
ElseIf oModel:GetModel( "G3Q_ITENS" ):GetValue("G3Q_TPDOC") $ '4|5|6' //N�o permitido para DOC/ADM/ACM	
	
	Help( , , "TURXACMN", , STR0018, 1, 0) // "N�o � permitido gerar acerto de DOC/ADM/ACM."

ElseIf oModel:GetModel( "G3Q_ITENS" ):GetValue("G3Q_OPERAC") == '4' //N�o permitido para Venda de Serv.Proprio	
	
	Help( , , "TURXACMN", , STR0019, 1, 0) // "N�o � permitido gerar acerto de Servi�o Pr�prio."
	
ElseIf oModel:GetModel( "G3Q_ITENS" ):GetValue("G3Q_STATUS") $ '3|4' //PARCIALIDADE: 3-Cancelado 4-Particionado
  
	Help( , , "TURXACMN", , STR0020, 1, 0) // "N�o � permitido gerar acerto de item que foi particionado ou cancelado."

ElseIf TURXIVCOMA(oModel,3) //Verifica se o IV j� possui algum acerto
 	Help( , , "TURXACMN", , STR0021, 1, 0) // "Este item j� possui acerto."
 	
ElseIf !Empty( oModel:GetModel( "G3R_ITENS" ):GetValue("G3R_CONCIL") )
 	TurHelp( STR0078 ,; //"O documento de reserva se encontra em concilia��o."
 			 STR0079 ,; //"Retire o documento de reserva da concilia��o antes de prosseguir."
 			 "ACERTORVINCONC"  )
 	
Else
	
	aCopyValues	:=	TURxGetVls( "G3Q_ITENS", .T., {"G44_ITENS"/*TARIFAS*/,"G46_ITENS"/*TAXAS*/,"G47_ITENS"/*EXTRAS*/,"G48_ITENS"/*ACORDOS*/,"G49_ITENS"/*IMPOSTOS*/,"G4E_ITENS"/*REEMBOLSOS*/,"G4CA_ITENS"/*ACORDOS-CLI*/,"G4CB_ITENS"/*ACORDOS-FOR*/,"G9KA_ITENS"/*DEM-CLI*/,"G9KB_ITENS"/*DEM-FOR*/} )
		
	If TURxSetVs( aCopyValues, aNoCampos )
	
		cSeqG3Q := TA042MaxSq( oModel )
		TA042SetRec( "G3Q_ITENS", {{"_NUMSEQ",cSeqG3Q}}, , .F.  )
		
		oModel:SetValue( "G3R_ITENS", "G3R_VLFIM",	0 ) 
		oModel:SetValue( "G3R_ITENS", "G3R_TOTREC",	0 )
		oModel:SetValue( "G3R_ITENS", "G3R_TARIFA",	0 )
		oModel:SetValue( "G3R_ITENS", "G3R_TAXA",		0 )  
		oModel:SetValue( "G3R_ITENS", "G3R_EXTRAS",	0 )
		oModel:SetValue( "G3R_ITENS", "G3R_VLRIMP",	0 )
		oModel:SetValue( "G3R_ITENS", "G3R_TXREE",	0 ) 
		oModel:SetValue( "G3R_ITENS", "G3R_VLRSER",	0 )
		oModel:SetValue( "G3R_ITENS", "G3R_TAXADU",	0 )
		oModel:SetValue( "G3R_ITENS", "G3R_TXFORN",	0 )
		oModel:SetValue( "G3R_ITENS", "G3R_VLCOMI",	0 )
		oModel:SetValue( "G3R_ITENS", "G3R_VLINCE",	0 )	
		oModel:SetValue( "G3R_ITENS", "G3R_CONCIL",	'')	
		
		If !Empty(cSeqOrig)
			oModel:SetValue( "G3R_ITENS", "G3R_SEQACR"	, cSeqOrig )
		EndIF
		
		oModel:SetValue( "G3Q_ITENS", "G3Q_ATUIF",	'2' ) 		//Atualiza IF (1=Apenas Cliente;2=Cliente e Fornecedor)
			
		oModel:SetValue( "G3Q_ITENS", "G3Q_ACERTO",	"1" ) 		//Sim	
		oModel:SetValue( "G3R_ITENS", "G3R_ACERTO",	"1" ) 		//Sim
		oModel:SetValue( "G3Q_ITENS", "G3Q_STATUS",	"1" ) 		//Pendente de Faturamento
		oModel:SetValue( "G3R_ITENS", "G3R_STATUS",	"1" )		//Em Aberto	
			
		//Refaz os itens financeiros - recalcula os acordos
		Tur34ItFin(oModel,oModel:GetModel( "G3Q_ITENS" ):GetValue("G3Q_ATUIF"),.F.,oModel:GetModel( "G3Q_ITENS" ):GetValue("G3Q_ATUIFA"))	
		T34AtuDMFI(oModel,.F.)			
	EndIf
EndIf

Return


/*/{Protheus.doc} TA042MaxSq
Fun��o para retornar a pr�xima sequ�ncia de um documento de reserva
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param oModel, objeto, Objeto do ModelDef instanciado
/*/
Function TA042MaxSq( oModel )
	Local aLines		:= FwSaveRows()
	Local cSeq 		:= ""
	Local cTmpAlias	:= GetNextAlias()
	Local nX			:= 1
	Local oModelG3R	:= oModel:GetModel( "G3R_ITENS" )
	Local oModelG3Q	:= oModel:GetModel( "G3Q_ITENS" )
	Local cNumId      := oModelG3Q:GetValue( "G3Q_NUMID" )
	Local cIdItem		:= oModelG3Q:GetValue( "G3Q_IDITEM" )
	//a consulta ao banco de dados serve para garantir que nenhum outro usu�rio gerou uma nova sequ�ncia em outra concilia��o para o mesmo item de venda
	BeginSql Alias cTmpAlias
		SELECT MAX(G3Q_NUMSEQ) G3Q_NUMSEQ
			FROM %Table:G3Q%
			WHERE G3Q_FILIAL = %xFilial:G3Q%
				AND G3Q_NUMID = %Exp:oModelG3Q:GetValue( "G3Q_NUMID" )%
				AND G3Q_IDITEM = %Exp:oModelG3Q:GetValue( "G3Q_IDITEM" )%
				AND %notDel%

	EndSql

	cSeqG3Q := ( cTmpAlias )->G3Q_NUMSEQ

	//Verifica na concilia��o atual se n�o houve novas sequ�ncias ainda n�o comitadas
	For nX := 1 to oModelG3R:Length()
		oModelG3R:GoLine( nX )

		If oModelG3R:GetValue( "G3R_NUMID" ) == cNumId .AND. oModelG3R:GetValue( "G3R_IDITEM" )  == cIdItem .And. oModelG3R:GetValue( "G3R_NUMSEQ" ) > cSeqG3Q
			cSeqG3Q := oModelG3R:GetValue( "G3R_NUMSEQ" )
		EndIf
	Next

	FwRestRows( aLines )

Return Soma1( cSeqG3Q )


/*/{Protheus.doc} TA042SetRec
Fun��o para alterar todas as ocorrencias de um campo de forma recursiva
@type function
@author Anderson Toledo
@since 16/03/2016
@version 1.0
@param cIdModel, character, Id do modelo de onde ser� realizada a recurs�o em seus submodelos
@param aLoadValues, array, vetor com informa��es para o loadvalue
@param aSetValues, array, vetor com informa��es para o setvalue
@param lAllLines, booleano, Se for grid, indica que dever� ser percorrida todas as linhas
/*/
Function TA042SetRec( cIdModel, aLoadValues, aSetValues, lAllLines  )
	Local aDependency 	:= {}
	Local cTable			:= ""
	Local oModel			:= FwModelActive()
	Local oModelAux		:= oModel:GetModel( cIdModel )
	Local nX				:= 0
	Local nY				:= 0

	DEFAULT aLoadValues	:= {}
	DEFAULT aSetValues	:= {}
	DEFAULT lAllLines		:= .F.

	aDependency 	:= oModel:GetDependency( cIdModel )
	cTable			:= oModelAux:GetStruct():GetTable()[1]


	If lAllLines .And. oModelAux:ClassName() == "FWFORMGRID"
		For nX := 1 to oModelAux:Length()
			oModelAux:GoLine( nX )

			For nY := 1 to len( aSetValues )
				If oModelAux:HasField( cTable + aSetValues[nY][1] )
					If ( oModelAux:ClassName() == "FWFORMGRID" .And. !oModelAux:IsEmpty() ) .Or. oModelAux:ClassName() == "FWFORMFIELDS"
						oModelAux:SetValue( cTable + aLoadValues[nY][1], aSetValues[nY][2] )
					EndIf
				EndIf
			Next

			For nY := 1 to len( aLoadValues )
				If oModelAux:HasField( cTable + aLoadValues[nY][1] )
					If ( oModelAux:ClassName() == "FWFORMGRID" .And. !oModelAux:IsEmpty() ) .Or. oModelAux:ClassName() == "FWFORMFIELDS"
						oModelAux:LoadValue( cTable + aLoadValues[nY][1], aLoadValues[nY][2] )
					EndIf
				EndIf
			Next

			For nY := 1 to len( aDependency )
				TA042SetRec( aDependency[nY][2], aLoadValues, aSetValues, .T.  )
			Next

		Next

	Else

		For nY := 1 to len( aSetValues )
			If oModelAux:HasField( cTable + aSetValues[nY][1] )
				If ( oModelAux:ClassName() == "FWFORMGRID" .And. !oModelAux:IsEmpty() ) .Or. oModelAux:ClassName() == "FWFORMFIELDS"
					oModelAux:SetValue( cTable + aLoadValues[nY][1], aSetValues[nY][2] )
				EndIf
			EndIf
		Next

		For nY := 1 to len( aLoadValues )
			If oModelAux:HasField( cTable + aLoadValues[nY][1] )
				If ( oModelAux:ClassName() == "FWFORMGRID" .And. !oModelAux:IsEmpty() ) .Or. oModelAux:ClassName() == "FWFORMFIELDS"
					oModelAux:LoadValue( cTable + aLoadValues[nY][1], aLoadValues[nY][2] )
				EndIf
			EndIf
		Next

		For nY := 1 to len( aDependency )
			TA042SetRec( aDependency[nY][2], aLoadValues, aSetValues, .T.  )
		Next

	EndIf

Return .T.


/*/{Protheus.doc} TURXCONEFE
Fun��o para verificar se uma concilia��o esta efetivada
@type function
@author Osmar Cioni Estevam Junior
@since 02/08/2016
@version 1.0
@param C�digo da concilia��o
/*/

Function TURXCONEFE(cConcil)
Local lConcEfet:= .F.

DbSelectArea("G6J")
DbSetOrder(1)		//G6J_FILIAL+G6J_CONCIL+G6J_NRIATA+G6J_CPIATA
If G6J->( DbSeek( xFilial("G6J")+cConcil))
	If	G6J->G6J_EFETIV == '1'		//EFETIVADA = 1=Sim;2=Nao
		lConcEfet := .T.
	EndIF

EndIf                                                                                                                   

Return lConcEfet




/*/{Protheus.doc} TURXSLDIF
Fun��o que verifica o saldo do cliente a pagar
@type function
@author osmar.junior
@since 04/08/2016
@version 1.0
@param cCodCli, character, (C�digo do Cliente)
@param cLoja, character, (Loja do Cliente)
@param cSegTur, character, (Segmento - Corporativo/Evento/Lazer)
@return $nSaldo, $Valor do saldo
/*/
Function TURXSLDIF(cCodCli,cLoja,cSegTur)
Local nSaldo		:= 0
Local cAliasAux 	:= GetNextAlias()
Local cCONINU		:= SPACE(TAMSX3('G4C_CONINU')[1])
Local cExpLoja	:= '%%'


If !Empty(cLoja)
	cExpLoja := "%G4C_LOJA ='"+cLoja+"' AND%"
EndIF

BeginSQL Alias cAliasAux
	SELECT SUM(CASE WHEN G4C_PAGREC = '1' THEN (G4C_VALOR*-1) ELSE G4C_VALOR END) SALDO 
	FROM %Table:G4C% G4C
	WHERE 
	G4C_FILIAL =	%XFilial:G4C% AND
	G4C_CLIFOR =	'1' AND
	G4C_CODIGO =	%Exp:cCodCli%	 AND
	%Exp:cExpLoja%
	G4C_SEGNEG =	%Exp:cSegTur%	 AND
	G4C_CONINU =	%Exp:cCONINU% AND
	G4C.%NotDel%

EndSQL
                                                                                                                 
If !( (cAliasAux)->(Eof()) )

	nSaldo:= (cAliasAux)->SALDO
		
Endif	

(cAliasAux)->(DbCloseArea())

Return nSaldo



/*/{Protheus.doc} TRXSTRTRAN
(long_description)
@type function
@author osmar.junior
@since 05/08/2016
@version 1.0
@param cString, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
FUNCTION TRXSTRTRAN(cString)
Local cStringAux := ''

cStringAux:= STRTRAN(cString, ' ','_')

Return cStringAux



/*/{Protheus.doc} TURXIVCOMA
Fun��o que verifica se pode Cancelar/Estornar um IV
@type function
@author osmar.junior
@since 09/08/2016
@version 1.0
@param oModel, objeto, (Modelo completo a partir do IV)
@param nTipo, num�rico, (Tipo de verifica��o: 1-Gerar Acerto/Cancela Item / 2-Estornar Cancelamento / 3-Verifica acerto)
@return ${L�gigo}, ${Indica se � permitido seu cancelamento/estorno de cancelamento}
/*/
Function TURXIVCOMA(oModel,nTipo)
Local lValid :=		.F.
Local cNumID :=		oModel:GetValue( "G3Q_ITENS", "G3Q_NUMID" )
Local cIdItem :=		oModel:GetValue( "G3Q_ITENS", "G3Q_IDITEM" )

If nTipo == 1
	If oModel:GetModel( "G3Q_ITENS" ):SeekLine({;
														{"G3Q_NUMID",cNumID},;
														{"G3Q_IDITEM",cIdItem},;
														{"G3Q_NUMSEQ",'02'},;	
														{"G3Q_STATUS",'1'},;													
														{"G3Q_ACERTO",'1'}})
														
		lValid := .T.
	EndIf

ElseIf nTipo == 2 .AND. oModel:GetValue( "G3Q_ITENS", "G3Q_ACERTO" ) == '1'
	If oModel:GetModel( "G3Q_ITENS" ):SeekLine({;
														{"G3Q_NUMID",cNumID},;
														{"G3Q_IDITEM",cIdItem},;
														{"G3Q_NUMSEQ",'01'},;													
														{"G3Q_STATUS",'3'}})
																																								
		lValid := .T.
	EndIf
ElseIf nTipo == 3
	If oModel:GetModel( "G3Q_ITENS" ):SeekLine({;
														{"G3Q_NUMID",cNumID},;
														{"G3Q_IDITEM",cIdItem},;
														{"G3Q_NUMSEQ",'02'},;																												
														{"G3Q_ACERTO",'1'}})
														
		lValid := .T.
	EndIf
	
EndIf
												
Return lValid

/*/{Protheus.doc} TUR34VLDSO
Valida o solicitante do item de venda.
@type function
@author Enaldo Cardoso Junior
@since 11/08/2016
@version 1.0
@return ${L�gigo}
/*/
Function TUR34VLDSO(lSolici)

Local aArea		:= GetArea()
Local oModel		:= FwModelActive()
Local lRet			:= .T.
Local cContato	:= ""
Local cAliasAC8	:= GetNextAlias()
Local cCodEntCt	:= ''
Default lSolici	:= .T.

If lSolici
	cContato	:= oModel:GetModel('G3Q_ITENS'):GETVALUE('G3Q_SOLIC')
Else
	cContato	:= oModel:GetModel('G3S_ITENS'):GETVALUE('G3S_CONTAT')
Endif
If SU5->(DbSeek(xFilial('SU5')+cContato)) 
	
	If lSolici .AND. SU5->U5_SOLICTE == "2"
		lRet := .F.
		Help(,,"TUR34VLDSO1",,STR0022,1,0)
	Else
		cCodEntCt := PADR(FWFLDGET('G3Q_CLIENT')+FWFLDGET('G3Q_LOJA'),TAMSX3('AC8_CODENT')[1])
		
		BeginSql Alias cAliasAC8
		SELECT COUNT(*) QTD
		FROM %Table:AC8% AC8
		WHERE AC8_FILIAL = %xFilial:AC8% AND
			  AC8_ENTIDA = 'SA1' AND
			  AC8_CODCON = %Exp:cContato% AND
			  AC8_CODENT = %Exp:cCodEntCt% AND
			  AC8.%NotDel%
		EndSql

		If (cAliasAC8)->QTD == 0
			lRet := .F.
			Help(,,"TUR34VLDSO2",,STR0038,1,0) // "O Contato n�o est� vinculado para esse Cliente"
		EndIf
		(cAliasAC8)->(DbCloseArea())
	EndIf
Endif

RestArea(aArea)

Return lRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TURxCond

Fun��o para montagem de parcelas conforme condi��o de pagamento do Turismo

@sample	TURxCond()
@author    Jos� Domingos Caldana Jr
@since     11/08/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------

Function TURxCond(nValor,cCondPgto,dData)
		
Local aArea		:= GetArea()
Local aParcelas	:= {}
		
DbSelectArea("SE4")
SE4->(DbSetOrder(1))
If SE4->(DbSeek(xFilial('SE4')+cCondPgto))
	If SE4->E4_TIPO == '9'
		aParcelas := TA009Cond( cCondPgto, nValor, dData )
	Else
		aParcelas := Condicao( nValor, cCondPgto,,dData  )
	EndIf
EndIf

RestArea(aArea)

Return aParcelas

//------------------------------------------------------------------------------
/*/{Protheus.doc} TURValCond

Valida as condi��o de pagamento do Turismo

@sample	TURValCond()
@author    Jos� Domingos Caldana Jr
@since     11/08/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------

Function TURValCond(cCondPgto)

Local aArea	:= GetArea()
Local lRet 	:= .T.

DbSelectArea("SE4")
SE4->(DbSetOrder(1))
If SE4->(DbSeek( xFilial('SE4')+ cCondPgto ) )		
	If SE4->E4_TIPO == '9'
		If SuperGetMV("MV_INTTUR",,.F.)
			DbSelectArea("G66")
			G66->(DbSetOrder(1)) 
			If !G66->(DbSeek( xFilial('G66')+cCondPgto))
				Help(,,"TURValCond_TP9",,STR0024,1,0)			 //"Condi��o de pagamento do Tipo 9 sem o cadastro de complemento."
				lRet :=  .F.
			EndIf
		Else
			Help(,,"TUR_INTTUR",,STR0049, 1, 0,,,,,,{STR0050}) //"Par�metro de integra��o com M�dulo Turismo (MV_INTTUR) desativado." //"Ative o par�metro para utilizar esta condi��o de pagamento."
			lRet :=  .F.		
		EndIf
	EndIf
Else
	Help(,,"TURValCond",,STR0025,1,0)			 //"Condi��o de pagamento n�o cadastrada."
	lRet :=  .F.	
EndIf

RestArea(aArea)

Return lRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurGetErrorMsg

Fun��o respons�vel para retornar em string o erro que ocorre no MVC, valida��es 
dentro do Modelo de dados.

@sample	TurGetErrorMsg()
@author    Fernando Radu Muscalu
@since     16/08/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TurGetErrorMsg(oModel)

Local cErrorMessage	:= ""
Local aErro 		:= oModel:GetErrorMessage()

cErrorMessage := STR0029 + " [" + AllTrim(AllToChar( aErro[1] )) + "]" + chr(13)	//"Id do formul�rio de origem: "
cErrorMessage += STR0030 + " [" + AllTrim(AllToChar( aErro[2] )) + "]" + chr(13)	//"Id do campo de origem: "
cErrorMessage += STR0031 + " [" + AllTrim(AllToChar( aErro[3] )) + "]" + chr(13)	//"Id do formul�rio de erro: "
cErrorMessage += STR0032 + " [" + AllTrim(AllToChar( aErro[4] )) + "]" + chr(13)	//"Id do campo de erro: "
cErrorMessage += STR0033 + " [" + AllTrim(AllToChar( aErro[5] )) + "]" + chr(13)	//"Id do erro: "
cErrorMessage += STR0034 + " [" + AllTrim(AllToChar( aErro[6] )) + "]" + chr(13)	//"Mensagem do erro: "
cErrorMessage += STR0035 + " [" + AllTrim(AllToChar( aErro[7] )) + "]" + chr(13)	//"Mensagem da solu��o: "
cErrorMessage += STR0036 + " [" + AllTrim(AllToChar( aErro[8] )) + "]" + chr(13)	//"Valor atribu�do: "
cErrorMessage += STR0037 + " [" + AllTrim(AllToChar( aErro[9] )) + "]"				//"Valor anterior: "

Return(cErrorMessage)

/*/{Protheus.doc} TUR34AC8
Filtra o solicitante referente a entidade.
@type function
@author Enaldo Cardoso Junior
@since 18/08/2016
@version 1.0
@return ${caracter}
/*/
Function TUR34AC8()

Local cQuery  := ""
Local cFilter := ""

cFilter := FwFldGet("G3Q_CLIENT")+FwFldGet("G3Q_LOJA")
 
cQuery := "@AC8_CODENT = ('"+cFilter+"') AND AC8_ENTIDA = 'SA1' "

Return cQuery

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurxFil

Fun��o que ir� gerar um Filtro para consulta padr�o

@sample	TurxFil(cConPad)
@param		cConPad - String, nome da consulta para qual ser� criado o filtro
@return	cFilter - String, query do filtro 
@author    Jacomo Lisa
@since     06/10/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TurxFil(cConPad)

Local oModel  := FwModelActive()
Local oView   := Nil
Local cQuery  := ""
Local cWhere  := ""

If ValType(oModel) == "O"
	Do Case 
		Case cConPad == 'G3C1'
			cQuery  := "@G3C_CODFOR+G3C_LJFOR = '" + oModel:GetValue('G3T_ITENS', 'G3T_CODFOR') + oModel:GetValue('G3T_ITENS', 'G3T_LOJAF') + "'" 
	
		Case cConPad == 'G3G'
			cQuery  := "@G3G_CLIENT = ('" + oModel:GetValue('G3Q_ITENS', 'G3Q_CLIENT') + "') AND G3G_LOJA = ('" + oModel:GetValue('G4B_ITENS', 'G4B_LOJA') + "') AND G3G_TIPO = ('" + oModel:GetValue('G4B_ITENS', 'G4B_TPENT') + "') "
	
		Case cConPad == 'G3G2'
			cQuery  := "@G3G_CLIENT = ('" + oModel:GetValue('G3Q_ITENS', 'G3Q_CLIENT') + "') AND G3G_LOJA = ('" + oModel:GetValue('G4B_ITENS', 'G4B_LOJA') + "') AND G3G_TIPO = ('" + oModel:GetValue('G4A_ITENS', 'G4A_TPENT') + "') "
			
		Case cConPad == 'G3NG3Q'
			If oModel:GetValue('G3P_FIELDS', 'G3P_SEGNEG') == '1'
				cWhere := "BM_CORP = 'T'"
			ElseIf oModel:GetValue('G3P_FIELDS', 'G3P_SEGNEG') == '2'		 
				cWhere := "BM_EVENTO = 'T'"
			Else
				cWhere := "BM_LAZER = 'T'"
			EndIf
			
			cQuery  := "@G3N_CODIGO IN (" + ;
								"SELECT G3K_CODFOP " + ;
								"FROM " + RetSQLName("SBM") + " SBM " + ;
								"INNER JOIN " + RetSQLName("G3K") + " G3K ON G3K_FILIAL = '"+xFilial('G3K')+ "' AND G3K_CODGRP = BM_GRUPO AND G3K.D_E_L_E_T_ = '' " + ;
								"WHERE BM_FILIAL = '" + xFilial("SBM") + "' AND BM_GRUPO = '" + oModel:GetValue('G3Q_ITENS', 'G3Q_GRPPRD') + "' AND " + cWhere + ;
						") "
		
		Case cConPad == 'G4G'
			cQuery  := "@G4H_FORNEC+G4H_LOJA = '" + oModel:GetValue('G3R_ITENS', 'G3R_FORNEC') + oModel:GetValue('G3R_ITENS', 'G3R_LOJA') + "'" 
		
		Case cConPad == 'G4H'
			cQuery  := "@G4H_FORNEC+G4H_LOJA = '" + oModel:GetValue('G3R_ITENS', 'G3R_FORNEC') + oModel:GetValue('G3R_ITENS', 'G3R_LOJA') + "'" 
		
		Case cConPad == 'G8M'
			cQuery  := "@G8M_CODFOR+G8M_LJFOR = '" + oModel:GetValue('G3R_ITENS', 'G3R_FORNEC') + oModel:GetValue('G3R_ITENS', 'G3R_LOJA') + "'" 
		
		Case cConPad == 'G8N'
			cQuery  := "@G8N_CODLIN = '" + oModel:GetValue('G3W_ITENS', 'G3W_LINHA') + "'" 
		
		Case cConPad == 'SA1G4A'
			cQuery  := "@A1_COD = '" + oModel:GetValue('G3Q_ITENS', 'G3Q_CLIENT') + "' AND A1_LOJA IN ( " + ;
								"SELECT G3F_LOJA FROM " + RetSqlName("G3F") + " G3F " + ;
								"WHERE G3F.D_E_L_E_T_ <> '*' AND G3F_CODCLI = '" + oModel:GetValue('G3Q_ITENS', 'G3Q_CLIENT') + "' AND G3F_MSBLQL <> '1' " + ;
					 	") "
		
		Case cConPad == 'SA2PRD'
			cQuery  := "@A2_COD+A2_LOJA IN (" + ;
								"SELECT G4T_FORNEC+G4T_LOJA " + ;
								"FROM " + RetSQLName("G4T") + " G4T " + ;
								"WHERE G4T_FILIAL = '" + xFilial("G4T") + "' AND G4T_GRUPO = '" + oModel:GetValue('G3Q_ITENS', 'G3Q_GRPPRD') + "'" + ;
						") "
			
		Case cConPad == 'SA2RPT'
			cQuery  := "@A2_COD+A2_LOJA IN (" + ;
								"SELECT G4S_FORNEC+G4S_LOJA " + ;
								"FROM " + RetSQLName("G4S") + " G4S " + ;
								"WHERE G4S_FILIAL = '" + xFilial("G4S") + "' AND G4S_REPORT = '1'" + ;
						") "
	
		Case cConPad == 'G3GG80'
			oView   := FwViewActive()
			cQuery  := "@G3G_CLIENT+G3G_LOJA+G3G_TIPO = '" + oModel:GetValue('G6L_MASTER', 'G6L_CLIENT') + oModel:GetValue(IIF(oView:GetFolderActive('F_ALL',2)[1] == 1, 'G80A_DETAIL', 'G80B_DETAIL'), 'G80_LOJA') + oModel:GetValue(IIF(oView:GetFolderActive('F_ALL',2)[1] == 1, 'G80A_DETAIL', 'G80B_DETAIL'), 'G80_TPENT')
		
		Case cConPad == 'G3FG67'
			cQuery  := "@G3F_CODCLI = '" + oModel:GetValue('G4L_MASTER', 'G4L_CLIENT') + "' AND G3F_LOJA = '" + oModel:GetValue('G4L_MASTER', 'G4L_LOJA') + "' "
		
		Case cConPad == 'G3FCLI'
			cQuery  := "@G3F_CODCLI = '" + oModel:GetValue('G3Q_ITENS', 'G3Q_CLIENT') + "' AND G3F_LOJA = '" + oModel:GetValue('G3Q_ITENS', 'G3Q_LOJA') + "' "
		
		
				
	EndCase	
EndIf

Return cQuery

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurFileFromPath

Esta fun��o tem por objetivo retornar o nome do arquivo e extens�o contidos em uma
string com o caminho do diret�rio.

@sample	TurFileFromPath()
@author    Fernando Radu Muscalu
@since     19/08/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TurFileFromPath(cPath)

Local cDirFile	:= Alltrim(cPath)
Local cRetFile	:= ""

Local nSlash	:= 0 

If ( RAt("\",cDirFile) > 0 )
	nSlash := RAt("\",cDirFile)
Else
	nSlash := RAt("/",cDirFile)
Endif

If ( nSlash > 0 )
	cRetFile := Substr(cDirFile,nSlash+1)
EndIf

Return(cRetFile) 

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurCpyT2S

Fun��o que efetua a copia dos arquivos, caminhos que est�o no array, do Host 
(maquina local) para o servidor. Esta fun��o retornar� um novo array com os
diret�rio e arquivos que foram copiados.

@sample	TurCpyT2S()
@author    Fernando Radu Muscalu
@since     19/08/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TurCpyT2S(aFiles,cPath)

Local nI		:= 0

Local cAux		:= ""

Local aCopied	:= {}

Default cPath	:= TURxMkDir(.T.)

If ( Len(aFiles) > 0 )

	If ( Empty(cPath) )
		aCopied := aClone(aFiles)
	Else
		
		For nI := 1 to Len(aFiles)
			
			cAux := StrTran(aFiles[nI],cPath)
			
			If ( At(":\",cAux) > 0 )
				cAux := SubStr(cAux,At(":\",cAux)+1)
			Endif 
			
			If ( At(".PDF",Upper(cAux)) > 0 )
				cAux := SubStr(cAux,1,Rat("\",cAux)-1)
			Endif
			
			cAux := cPath + cAux
			
			If ( FWMakeDir(cAux) )
			
				If ( CpyT2S(aFiles[nI],cAux) )
					aAdd(aCopied,cAux+"\"+TurFileFromPath(aFiles[nI]))
				Endif
			
			Endif
			
		Next nI
	
	Endif

Endif

Return(aCopied)


//------------------------------------------------------------------------------
/*/{Protheus.doc} TURXMOEDAS

Fun��o que retorna as cotacoes e nome das moedas segundo arquivo SM2

@sample	TURXMOEDAS()
@param		
@return	aMoedas: Array - com as moedas e as respectivas taxas de cambio
@author    Thiago Tavares
@since     26/08/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TURXMOEDAS()

Local nMoedas := MoedFin()
Local nX      := 0
Local cMoeda	:= ''
Local aMoedas := {}

For nX	:=	1 To nMoedas
	cMoeda	:=	Str(nX, IIF(nX <= 9, 1, 2))
	If !Empty(GetMv("MV_MOEDA" + cMoeda))
		aAdd(aMoedas, {GetMv("MV_MOEDA" + cMoeda), RecMoeda(dDataBase, nX), PesqPict('SM2', 'M2_MOEDA'+ cMoeda)})
	Else
		Exit
	Endif
Next nX

Return aMoedas

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurBreakLine

Fun��o Respons�vel por retornar um array com a quebra de linhas a partir
de um texto a ser impresso por objetos das classes FWMSPrinter TMSPrinter.

A fun��o ir� considerar a largura (ou comprimento) limite e fonte em que o texto
est� vinculado.

@sample	TurBreakLine(cTexto,oFWPrt,oFont,nLimLnPxl)
@param		
@return	aBrkLines: Array - com as linhas quebradas
@author    Fernando Radu Muscalu
@since     08/09/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TurBreakLine(cTexto,oFWPrt,oFont,nLimLnPxl)

Local nI		:= 0

Local aRoot		:= Separa(cTexto,chr(13) + chr(10))
Local aBrkLines	:= {}

Default nLimLnPxl 	:= oFWPrt:nPageWidth
Default oFont		:= oFWPrt:oFontAtu

For nI := 1 to Len(aRoot)
	
	If ( !Empty(aRoot[nI]) )
	
		If ( oFWPrt:GetTextWidth(aRoot[nI],oFont) >= nLimLnPxl )
			
			RecursionBrkLine(aRoot[nI],oFWPrt,oFont,nLimLnPxl,aBrkLines)
			
		Else
			aAdd(aBrkLines,aRoot[nI])
		Endif
	
	Else
		AAdd(aBrkLines,chr(13) + chr(10))
	Endif
	
Next nI

Return(aBrkLines)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurVldNat

Valida as Naturezas do Turismo (Analiticas de Uso Livre)

@sample	TurVldNat()
@author    Jos� Domingos Caldana Jr
@since     11/08/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TurVldNat(cCampo, lHelp, cCmpHelp)

Local lRet		:= .T.
Local aArea	:= GetArea()

Default cCampo	:= &( ReadVar() )
Default lHelp		:= .T.
Default cCmpHelp	:= ""

If !Empty( cCampo )
	DbSelectArea( "SED" )
	SED->( DbSetOrder( 1 ) )
	If SED->( DbSeek( xFilial( "SED" ) + cCampo ) )

		If SED->ED_TIPO <> '2'
			If lHelp
				Help( " ", 1, "NATUREZ2",, cCmpHelp + STR0046 , 1, 0 ) // "Verifique se a natureza informada est� classificada corretamente. Apenas naturezas do titpo anal�tico ser�o aceitas para este processo.!"
			EndIf
			lRet	:= .F.
		EndIf

		If lRet .And. !Empty( SED->ED_USO ) .And. SED->ED_USO <> '0'
			If lHelp
				Help( " ", 1, "USONAT",, cCmpHelp + STR0047 , 1, 0 ) // "A natureza n�o poder� ser usada nesta rotina!"
			EndIf
			lRet	:= .F.
		EndIf
	Else
		If lHelp
			Help( " ", 1, "NATNAOENC",, cCmpHelp + STR0048 , 1, 0 ) // "A natureza informada n�o foi encontrada."
		EndIf
		lRet	:= .F.
	EndIf
EndIf

RestArea( aArea )

Return lRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} RecursionBrkLine

Fun��o recursiva auxiliar a fun��o TurBreakLine(..) para continuar quebrando as
linhas enquanto estas n�o respeitarem o tamanho limite definido por nLimChar

@sample	RecursionBrkLine(cTexto,oPrint,oFont,nLimChar,aBrkLines)
@param		
@return	nil
@author    Fernando Radu Muscalu
@since     08/09/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------

Static Function RecursionBrkLine(cTexto,oPrint,oFont,nLimChar,aBrkLines)

Local cAuxTxt	:= ""
Local cAuxTxt2	:= ""

Local nCount 	:= 0
Local nPF		:= 0

Local lFinish	:= .f.
		
cAuxTxt2 := cTexto //vir� preenchido no param cTexto2

While ( !lFinish )
	
	nPF := At(Space(1),cAuxTxt2)
	
	If ( nPF > 0 )
	
		cAuxTxt		+= SubStr(cAuxTxt2,1,nPF)
		cAuxTxt2	:= LTrim(SubStr(cAuxTxt2,nPF))
		
	Else
		
		If ( Empty(cAuxTxt) .or. oPrint:GetTextWidth(cAuxTxt,oFont) > nLimChar )
		
			nPF := TurBrkPxlStr(oPrint,oFont,nLimChar)
			
			cAuxTxt	+= SubStr(cAuxTxt2,1,nPF)
			cAuxTxt2:= LTrim(SubStr(cAuxTxt2,nPF))
			
		Else
			lFinish := .T.
		Endif
				
	Endif	
	
	If ( !lFinish )
		lFinish :=  !(oPrint:GetTextWidth(cAuxTxt,oFont) <= nLimChar)
	Endif	
	 
EndDo

aAdd(aBrkLines,cAuxTxt)

If ( oPrint:GetTextWidth(cAuxTxt2,oFont) > nLimChar )
	RecursionBrkLine(cAuxTxt2,oPrint,oFont,nLimChar,aBrkLines)
Else
	aAdd(aBrkLines,cAuxTxt2)	
Endif 

Return()

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurBrkPxlStr

Fun��o que retorna a quantidade de caracteres que poder� ser impressa por linha,
respeitando o limite de comprimento definido por nPxlWidth.

@sample	TurBrkPxlStr(oFwPrt,oFont,nPxlWidth)
@param		
@return	nMaxChars: Num�rico. N�mero de Caracteres a ser efetuada a quebra da linha 
impressa
@author    Fernando Radu Muscalu
@since     08/09/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------

Function TurBrkPxlStr(oFwPrt,oFont,nPxlWidth)

Local nMaxPxl 	:= TurMaxPxlStr(oFwPrt,oFont)
Local nMaxChars	:= 0

nMaxChars := Int(nPxlWidth/nMaxPxl) 

Return(nMaxChars)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurMaxPxlStr

Fun��o que dever� retornar o maior pixel de um determinado caracter dependendo da 
fonte e do dispositivo de impress�o.

Esta fun��o tamb�m � respons�vel por montar uma matriz AscII com os tamanhos em 
pixels dependendo da fonte.
 
N�o s�o utilizados todos os caracteres poss�veis da tabela AscII, somente os que 
mais se adequam em textos. S�o eles:

Caractere	Decimal	Hexadecimal	Bin�rio
(Espa�o)	32	20	0010 0000
!	33	21	0010 0001
"	34	22	0010 0010
#	35	23	0010 0011
$	36	24	0010 0100
%	37	25	0010 0101
&	38	26	0010 0110
'	39	27	0010 0111
(	40	28	0010 1000
)	41	29	0010 1001
*	42	2A	0010 1010
+	43	2B	0010 1011
,	44	2C	0010 1100
-	45	2D	0010 1101
.	46	2E	0010 1110
/	47	2F	0010 FFFF
0	48	30	0011 0000
1	49	31	0011 0001
2	50	32	0011 0010
3	51	33	0011 0011
4	52	34	0011 0100
5	53	35	0011 0101
6	54	36	0011 0110
7	55	37	0011 0111
8	56	38	0011 1000
9	57	39	0011 1001
:	58	3A	0011 1010
;	59	3B	0011 1011
<	60	3C	0011 1100
=	61	3D	0011 1101
>	62	3E	0011 1110
?	63	3F	0011 1111
@	64	40	0100 0000
A	65	41	0100 0001
B	66	42	0100 0010
C	67	43	0100 0011
D	68	44	0100 0100
E	69	45	0100 0101
F	70	46	0100 0110
G	71	47	0100 0111
H	72	48	0100 1000
I	73	49	0100 1001
J	74	4A	0100 1010
K	75	4B	0100 1011
L	76	4C	0100 1100
M	77	4D	0100 1101
N	78	4E	0100 1110
O	79	4F	0100 1111
P	80	50	0101 0000
Q	81	51	0101 0001
R	82	52	0101 0010
S	83	53	0101 0011
T	84	54	0101 0100
U	85	55	0101 0101
V	86	56	0101 0110
W	87	57	0101 0111
X	88	58	0101 1000
Y	89	59	0101 1001
Z	90	5A	0101 1010
[	91	5B	0101 1011
\	92	5C	0101 1100
]	93	5D	0101 1101
^	94	5E	0101 1110
_	95	5F	0101 1111
`	96	60	0110 0000
a	97	61	0110 0001
b	98	62	0110 0010
c	99	63	0110 0011
d	100	64	0110 0100
e	101	65	0110 0101
f	102	66	0110 0110
g	103	67	0110 0111
h	104	68	0110 1000
i	105	69	0110 1001
j	106	6A	0110 1010
k	107	6B	0110 1011
l	108	6C	0110 1100
m	109	6D	0110 1101
n	110	6E	0110 1110
o	111	6F	0110 1111
p	112	70	0111 0000
q	113	71	0111 0001
r	114	72	0111 0010
s	115	73	0111 0011
t	116	74	0111 0100
u	117	75	0111 0101
v	118	76	0111 0110
w	119	77	0111 0111
x	120	78	0111 1000
y	121	79	0111 1001
z	122	7A	0111 1010
{	123	7B	0111 1011
|	124	7C	0111 1100
}	125	7D	0111 1101
~	126	7E	0111 1110 
 
@sample	TurMaxPxlStr(oFwPrt,oFont,lRefaz)
@param		
@return	nMaxChars: Num�rico. N�mero de Caracteres a ser efetuada a quebra da linha 
impressa
@author    Fernando Radu Muscalu
@since     08/09/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------

Function TurMaxPxlStr(oFwPrt,oFont,lRefaz)

Local nMaxPixel	:= 0
Local nI		:= 0

Local aPixels	:= {}
Default lRefaz	:= .f.

If ( lRefaz .Or. Len(aTurPxlsStr) == 0 .Or. (aTurPxlsStr[1] <> oFont:Name .And. aTurPxlsStr[2] <> oFont:nWidth) ) 

	For nI := 32 to 126 //Percorre todos os caracteres da tabela ASCII mais utilizados em escrita 
		
		If ( nI <> 34 ) //34 � aspas (") ent�o ser� ignorado para n�o dar problema durante a execu��o
			AAdd(aPixels, {chr(nI),oFwPrt:GetTextWidth(chr(nI),oFont)})
		Endif				
		
	Next nI
	
	//Ordena do maior pixel para o menor 	
	aSort(aPixels,,,{|x,y| x[2] > y[2]})
	
	aTurPxlsStr := {oFont:Name,oFont:nWidth,aPixels}

Else
	aSort(aTurPxlsStr[3],,,{|x,y| x[2] > y[2]})
EndIf

nMaxPixel := aTurPxlsStr[3][1,2] 

Return(nMaxPixel)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurGetPxlStr

Fun��o que retorna array aTuPxlsStr

@sample	TurGetPxlStr()
@param		
@return	aTurPxlsStr: Array. Array que armazena o nome da fonte, tamanho e dentre os
caracteres mais utilizados o tamanho (em pixel) de cada um deles.
 	aTurPxlsStr[1] - Nome da Fonte	(Ex: Arial)
 	aTurPxlsStr[2] - Tamanho da Fonte (Ex: 12)
 	aTurPxlsStr[3] - Array
 		aTurPxlsStr[3][n,1] - Caractere
 		aTurPxlsStr[3][n,2] - Tamanho em Pixels
 		
@author    Fernando Radu Muscalu
@since     08/09/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------

Function TurGetPxlStr()

Return(aTurPxlsStr)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurFrmtHtm

Fun��o respons�vel por tornar o texto proveniente do par�metro em um formato
amig�vel para Html

@sample	TurFrmtHtm(cTexto,aPopChars)
@param
	cTexto: String. Texto a ser convertido
	aPopChars: Array. Vetor contendo os principais (populares) caracteres utilizado 
	em Portugu�s, Ingl�s e Espanhol		
@return	cRetTxt: String. Texto convertido 
 		
@author    Fernando Radu Muscalu
@since     17/09/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TurFrmtHtm(cTexto,aPopChars) 

Local aPopChr	:= {}

Local nI	:= 0
Local nP	:= 0

Local cRetTxt	:= cTexto

Default aPopChars := -1

If ( Valtype(aPopChars) == "N" )

	aPopChr := {	"�","�","�","�","�","�","�","�",;
					"�","�","�","�","�","�","�","�",;
					"�","�","�","�","�","�","�","�",;
					"�","�","�","�","�","�","�"}

Else
	aPopChr := aClone(aPopChars)
Endif
	
//Monta o array com os caracteres especiais que podem ser utilizados em HTML
TurHtmlChars()

For nI := 1 to Len(aPopChr)
	
	If ( At(aPopChr[nI],cRetTxt) > 0 )
		
		nP := aScan(aTurSChrHtm,{|x| Alltrim(x[1]) == Alltrim(aPopChr[nI])})
		
		If ( nP > 0 )
			cRetTxt := StrTran(cRetTxt,aPopChr[nI],aTurSChrHtm[nP,2])
		Endif
			
	Endif
	
Next nI

Return(cRetTxt)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurHtmlChars

Esta fun��o carrega o array est�tico aTurSChrHtm, se estiver vazio, com os 
caracteres especiais e o seu codigo em formato html para convers�o

@sample	TurHtmlChars()
@param
			
@return	aTurSChrHtm: Array. Vetor com os caracteres especiais e seu respectivo
c�digo html 
 		
@author    Fernando Radu Muscalu
@since     17/09/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TurHtmlChars()

Local aChars	:= {}

If ( Len(aTurSChrHtm) == 0 )

	aAdd(aChars,{'�','&Aacute;'})		
	aAdd(aChars,{'�','&aacute;'})
	aAdd(aChars,{'�','&egrave;'})
	aAdd(aChars,{'�','&Ograve;'})
	aAdd(aChars,{'�','&ccedil;'})
	aAdd(aChars,{'�','&Acirc;'})
	aAdd(aChars,{'�','&Euml;'})
	aAdd(aChars,{'�','&ograve;'})
	aAdd(aChars,{'�','&acirc;'})
	aAdd(aChars,{'�','&euml;'})
	aAdd(aChars,{'�','&Oslash;'})
	aAdd(aChars,{'�','&Ntilde;'})
	aAdd(aChars,{'�','&Agrave;'})
	aAdd(aChars,{'�','&ETH;'})
	aAdd(aChars,{'�','&oslash;'})
	aAdd(aChars,{'�','&ntilde;'})
	aAdd(aChars,{'�','&agrave;'})
	aAdd(aChars,{'�','&eth;'})
	aAdd(aChars,{'�','&Otilde;'})
	aAdd(aChars,{'�','&Aring;'})
	aAdd(aChars,{'�','&otilde;'})
	aAdd(aChars,{'�','&Yacute;'})
	aAdd(aChars,{'�','&aring;'})
	aAdd(aChars,{'�','&Iacute;'})
	aAdd(aChars,{'�','&Ouml;'})
	aAdd(aChars,{'�','&yacute;'})
	aAdd(aChars,{'�','&Atilde;'})
	aAdd(aChars,{'�','&iacute;'})
	aAdd(aChars,{'�','&ouml;'})
	aAdd(aChars,{'�','&atilde;'})
	aAdd(aChars,{'�','&Icirc;'})
	aAdd(aChars,{'"','&quot;'})
	aAdd(aChars,{'�','&Auml;'})
	aAdd(aChars,{'�','&icirc;'})
	aAdd(aChars,{'�','&Uacute;'})
	aAdd(aChars,{'<','&lt;'})
	aAdd(aChars,{'�','&auml;'})
	aAdd(aChars,{'�','&Igrave;'})
	aAdd(aChars,{'�','&uacute;'})
	aAdd(aChars,{'>','&gt;'})
	aAdd(aChars,{'�','&AElig;'})
	aAdd(aChars,{'�','&igrave;'})
	aAdd(aChars,{'�','&Ucirc;'})
	aAdd(aChars,{'&','&amp;'})
	aAdd(aChars,{'�','&aelig;'})
	aAdd(aChars,{'�','&Iuml;'})
	aAdd(aChars,{'�','&ucirc;'})
	aAdd(aChars,{'�','&iuml;'})
	aAdd(aChars,{'�','&Ugrave;'})
	aAdd(aChars,{'�','&reg;'})
	aAdd(aChars,{'�','&Egrave;'})
	aAdd(aChars,{'�','&Eacute;'})
	aAdd(aChars,{'�','&ugrave;'})
	aAdd(aChars,{'�','&copy;'})
	aAdd(aChars,{'�','&eacute;'})
	aAdd(aChars,{'�','&Oacute;'})
	aAdd(aChars,{'�','&Uuml;'})
	aAdd(aChars,{'�','&THORN;'})
	aAdd(aChars,{'�','&Ecirc;'})
	aAdd(aChars,{'�','&oacute;'})
	aAdd(aChars,{'�','&uuml;'})
	aAdd(aChars,{'�','&thorn;'})
	aAdd(aChars,{'�','&ecirc;'})
	aAdd(aChars,{'�','&Ocirc;'})
	aAdd(aChars,{'�','&szlig;'})
	aAdd(aChars,{'�','&ocirc;'})
	aAdd(aChars,{'�','&Ccedil;'})
	aAdd(aChars,{'�','&iexcl;'})
	aAdd(aChars,{'�','&iquest;'})
	
	aTurSChrHtm := aClone(aChars)

Endif

Return(aTurSChrHtm)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurSetVarRmt()

Fun��o Respons�vel por criar vari�veis no host (Totvs Smart Client) 
@sample	TurHtmlChars()
@param
			
@return	aTurSChrHtm: Array. Vetor com os caracteres especiais e seu respectivo
c�digo html 
 		
@author    Fernando Radu Muscalu
@since     22/09/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TurSetVarRmt(cIdVar,aListVars)

Local cNewId	:= cIdVar
Local cExec		:= ""

Local nI	:= 0

If ( Len(aListVars) > 0 )

	cExec := "__SaveRmt('" + Alltrim(Upper(cNewId)) + "',"
	
	For nI := 1 to Len(aListVars) 
		
		If ( nI < Len(aListVars) .Or. nI < 10 )
			
			If ( ValType(aListVars[nI]) == "C" )
				cExec += "'" + TurCast(aListVars[nI],"C") + "',"
			Else
				cExec += TurCast(aListVars[nI],"C") + ","
			EndIf
					
		ElseIf ( nI == Len(aListVars) .Or. nI == 10 )
			
			If ( ValType(aListVars[nI]) == "C" )
				cExec += "'" + TurCast(aListVars[nI],"C") + "')"
			Else
				cExec += TurCast(aListVars[nI],"C") + ")"
			EndIf
			
			&(cExec)
		
		Else	
		
		Endif
			
	Next nI

Endif

Return()

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurSetVarRmt()

Fun��o Respons�vel por criar vari�veis no host (Totvs Smart Client) 
@sample	TurHtmlChars()
@param
			
@return	aTurSChrHtm: Array. Vetor com os caracteres especiais e seu respectivo
c�digo html 
 		
@author    Fernando Radu Muscalu
@since     22/09/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TurCast(xConvFrom,cConvTo,lFormData)

Local xConverted

Default lFormData := .t.

Do Case
Case ( cConvTo == "C" )

	If ( ValType(xConvFrom) == "C" )
		xConverted := xConvFrom
	ElseIf ( ValType(xConvFrom) == "N" )
		xConverted := cValToChar(xConvFrom)
	ElseIf ( ValType(xConvFrom) == "D" .and. lFormData)
		xConverted := DToC(xConvFrom)
	ElseIf ( ValType(xConvFrom) == "D" .and. !lFormData)
		xConverted := DToS(xConvFrom)	
	ElseIf ( ValType(xConvFrom) == "L" )
		xConverted := IIf (xConvFrom,".T.",".F.")
	EndIf

Case ( cConvTo == "N" )
	
	If ( ValType(xConvFrom) == "C" )
		xConverted := Val(xConvFrom)
	ElseIf ( ValType(xConvFrom) == "N" )
		xConverted := xConvFrom
	ElseIf ( ValType(xConvFrom) == "L" )
		cRetConv := IIf (xConvFrom,1,0)
	EndIf
	
Case ( cConvTo == "D" )
	
	If ( ValType(xConvFrom) == "D" )
		xConverted := xConvFrom
	ElseIf ( ValType(xConvFrom) == "C" .and. lFormData)
		xConverted := CToD(xConvFrom)
	ElseIf ( ValType(xConvFrom) == "C" .and. !lFormData)
		xConverted := SToD(xConvFrom)
	EndIf
		
Case ( cConvTo == "L" )
	
	If ( ValType(xConvFrom) == "C" )
		xConverted := IIf (Upper(xConvFrom) == ".T.",.T.,.F.)
	ElseIf ( ValType(xConvFrom) == "N" )
		xConverted := IIf (Upper(xConvFrom) == 1,.T.,.F.)
	ElseIf ( ValType(xConvFrom) == "L" )
		xConverted := xConvFrom	
	EndIf
	
EndCase

Return(xConverted)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurRemChEsp()

Remove os caracteres invalidos da string
@sample	TurRemChEsp(cString,cExcecao)
@param  cString  caracteres a serem tratados
@param  cExcecao caracteres especiais que ser�o exce��es
			
@return	 cNewStr - String sem os caracteres especiais
 		
@author    Lucas Brustolin
@since     22/09/2016
@version   12.1.13
/*/
//------------------------------------------------------------------------------
Function TurRemChEsp( cString, cExcecao )

Local cChar		:= ""
Local cNewStr		:= ""
Local nSizeStr	:= 0
Local nI			:= 0

Default cString := ""
Default cExcecao:= ""

//transforma em maiuscula e substitui caracteres com acentos
cString := AllTrim( FwNoAccent(cString) )

//tamanho da string
nSizeStr := Len( cString )

For nI := 1 to nSizeStr
	cChar := SubStr(cString, nI, 1)
	If cChar $ "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnoprqstuvwxyz0123456789" + cExcecao
		cNewStr += cChar
	EndIf
Next

Return cNewStr 

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurChkItApur()

Fun��o Respons�vel por efetuar valida��o antes da libera��o das apura��es
  
@sample	TurChkItApur()
@param
	cIdModel	-> Caractere. Nome do Modelo de dados
			
@return	lRet	-> L�gico. Vetor com os caracteres especiais e seu respectivo
c�digo html 
 		
@author    Fernando Radu Muscalu
@since     22/09/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------

Function TurChkItApur(cIdModel)

Local oModel

Local cIdSubMdl	:= ""
Local cMarked	:= ""

Local nI		:= 0
Local nX		:= 0
Local nNotEmpty	:= 0

Local lRet		:= .F.

If ( cIdModel $ "TURA040|TURA050" )
	cIdSubMdl := "G48A_DETAIL"
	cMarked := Iif(cIdModel == "TURA040", "G48_OK", "G48_MARK")
ElseIf ( cIdModel == "TURA043" )
	cIdSubMdl := "G6N_DETAIL"
	cMarked := "G6N_OK"	
Endif
	
oModel	:= FWLoadModel(cIdModel)
oModel:SetOperation(MODEL_OPERATION_VIEW)
oModel:Activate()

For nI := 1 to oModel:GetModel("G6M_DETAIL"):Length()
	
	If ( !oModel:GetModel("G6M_DETAIL"):IsDeleted(nI) )
		
		oModel:GetModel("G6M_DETAIL"):GoLine(nI)
		
		For nX := 1 to oModel:GetModel("G6O_DETAIL"):Length()
	
			If ( !oModel:GetModel("G6O_DETAIL"):IsDeleted(nX) )
				
				oModel:GetModel("G6O_DETAIL"):GoLine(nX) 
				
				If oModel:GetValue("G6O_DETAIL", "G6O_TIPOAC") $ 'C02|C03|C04' .Or. !( oModel:GetModel(cIdSubMdl):IsEmpty() .Or. !oModel:GetModel(cIdSubMdl):SeekLine({{cMarked,.t.}}) )
					nNotEmpty++ 	
				Endif
							
			EndIf	
		
		Next nX
		
		If ( nNotEmpty > 0 )
			lRet := .t.
			Exit
		EndIf
		
	Endif
	
Next nI

If !lRet .And. cIdModel == 'TURA040'
	For nX := 1 to oModel:GetModel('G82_DETAIL'):Length()
		If !oModel:GetModel('G82_DETAIL'):IsDeleted(nX)
			oModel:GetModel('G82_DETAIL'):GoLine(nX) 
			If !(oModel:GetModel('G48B_DETAIL'):IsEmpty() .Or. !oModel:GetModel('G48B_DETAIL'):SeekLine({{cMarked, .T.}}))
				nNotEmpty++ 	
			Endif
		EndIf

		If nNotEmpty > 0
			lRet := .T.
			Exit
		EndIf
	Next nX
EndIf

oModel:DeActivate()
	
If !lRet .And. !FwIsInCallStack('TA040SelApu')
	FwAlertInfo(STR0051,STR0052) // "N�o h� itens de acordos aplicados para serem liberados. Verifique a apura��o."#"Sem Itens"
EndIf	
	
Return(lRet)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurGetFil()

Esta fun��o retorna as filiais para um Array unidimensional. Poder�o ser 
retornadas: todas as filiais, somente as filiais do grupo de empresas corrente
ou a filial corrente.
 
@sample	TurGetFil(lAll,lCurrent)
@param
	lAll	-> L�gico. Se .t., ser�o carregadas todas as filiais. Caso seja .F., somente
			as filiais do grupo de empresas corrente.
	lCurrent-> L�gico. Se .t., ser� carregada, somente a filial corrente.		
			
@return	aFiliais	-> Array. Vetor unidimensional com as filiais.
			aFiliais[n]	-> Caractere. C�digo da Filial. 
 		
@author    Fernando Radu Muscalu
@since     28/09/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------

Function TurGetFil(lAll,lCurrent)

Local aFilInfo	:= FWArrFilAtu()
Local aFiliais	:= {}

Local cEmp 		:= ""
Local cUng 		:= ""
Local cFil 		:= ""
Local cRet		:= ""

Local nEmp		:= Len(aFilInfo[13])
Local nUNg		:= Len(aFilInfo[14])
Local nFil		:= Len(aFilInfo[15])
Local nRec		:= 0

Default lAll 		:= .T.
Default lCurrent	:= .F.

If ( lCurrent )
	aFiliais := {cFilAnt}
Else

	nRec := SM0->(Recno())

	SM0->(DbGoTop())
	
	While ( SM0->(!Eof()) )
		
		aAdd(aFiliais, Substr(SM0->M0_CODFIL,1,aFilInfo[8]))
		
		SM0->(DbSkip())
		
		If ( !lAll )
		
			If ( Substr(cFilAnt,1,nEmp) <> SubStr(SM0->M0_CODFIL,1,nEmp) )
				Exit
			Endif
			
		Endif
		
	EndDo
	
Endif

Return(aFiliais)


//------------------------------------------------------------------------------
/*/{Protheus.doc} TurAtuStRV

Atualiza Status do RV

@sample	TurAtuStRV()
@author    Jos� Domingos Caldana Jr
@since     11/08/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------

Function TurAtuStRV( cFilRv, cNumID, cIDItem, cNumSeq, cConinu, cAtuIVouDR, lFinalizou )
	
Local aArea		:= GetArea()
Local cAliasIFIV 	:= ""
Local cAliasIFDR 	:= ""
Local cAliasRV	:= ""
Local lDRAberto	:= .T.
Local lIVAberto	:= .T.
Local lErro		:= .F.
Local cId		:= ''

If lFinalizou	
	
	If cAtuIVouDR $ "1|3"  
	
		cAliasIFIV 	:= GetNextAlias()
	
		BeginSql Alias cAliasIFIV
			SELECT G4C_IDIF
			FROM %table:G4C% G4C
			WHERE G4C.G4C_FILIAL = %Exp:cFilRv% 
			AND G4C.G4C_NUMID = %Exp:cNumID% 
			AND G4C.G4C_IDITEM = %Exp:cIDItem%
			AND G4C.G4C_NUMSEQ = %Exp:cNumSeq%
			AND G4C.G4C_CLIFOR = '1'
			AND G4C.G4C_STATUS <> '4'
			AND G4C.G4C_CONINU = %Exp:cConinu%
			AND G4C.%NotDel% 
		EndSql
	
		//N�o tem item financeiro (G4C) em aberto no IV
		If (cAliasIFIV)->(Eof())
			
			lIVAberto := .F. 
			
			//Atualiza Status do IV (G3Q) para Finalizado
			dbSelectArea("G3Q")
			G3Q->(dbSetOrder(1))
			If G3Q->(dbSeek(cFilRv+cNumID+cIDItem+cNumSeq+cConinu))
				
				If !(G3Q->G3Q_STATUS $ '3|4') //Não atualiza para Cancelado ou Particionado
					If SoftLock("G3Q")
						G3Q->G3Q_STATUS := "2" //Finalizado
						G3Q->(MsUnLock())
					Else
						lErro := .T.					
						DisarmTransaction()
						cId := cFilRv+'-'+cNumID+'-'+cIDItem+'-'+cNumSeq+'-'+cConinu
						Help(" ",1,"TurAtuStRV",,STR0074+cId,1,0)		//"Item de Venda bloqueado (G3Q) : "
						T45Log( '1', '', "TurAtuStRV "+cFilRv+cNumID)
						T45Log( '2', '', STR0074+cId)
						T45Log( '3', '', '')
						Break
					EndIf
				EndIf
			EndIf	
			
		EndIf
	
		(cAliasIFIV)->(dbCloseArea())
	
	EndIf
	
	
	If cAtuIVouDR $ "2|3" .AND. !lErro  
	
		cAliasIFDR 	:= GetNextAlias()
		
		BeginSql Alias cAliasIFDR
			SELECT G4C_IDIF
			FROM %table:G4C% G4C
			WHERE G4C.G4C_FILIAL = %Exp:cFilRv% 
			AND G4C.G4C_NUMID = %Exp:cNumID% 
			AND G4C.G4C_IDITEM = %Exp:cIDItem%
			AND G4C.G4C_NUMSEQ = %Exp:cNumSeq%
			AND G4C.G4C_CLIFOR = '2'
			AND G4C.G4C_STATUS <> '4'
			AND G4C.G4C_CONINU = %Exp:cConinu%
			AND G4C.%NotDel% 
		EndSql
	
		//N�o tem item financeiro (G4C) em aberto no IV
		If (cAliasIFDR)->(Eof())
		
			lDRAberto := .F. 
		
			//Atualiza Status do DR (G3R) para Finalizado
			dbSelectArea("G3R")
			G3R->(dbSetOrder(1))
			If G3R->(dbSeek(cFilRv+cNumID+cIDItem+cNumSeq+cConinu))
				
				If !(G3R->G3R_STATUS $ '2|4') //Não atualiza para Cancelado ou Particionado
					If SoftLock("G3R")
						G3R->G3R_STATUS := "3" //Finalizado
						G3R->(MsUnLock())
					Else
						lErro := .T.					
						DisarmTransaction()
						cId := cFilRv+'-'+cNumID+'-'+cIDItem+'-'+cNumSeq+'-'+cConinu
						Help(" ",1,"TurAtuStRV",,STR0075+cId,1,0)		//"Documento de Reserva bloqueado (G3R) : "
						T45Log( '1', '', "TurAtuStRV "+cFilRv+cNumID)
						T45Log( '2', '', STR0075+cId)
						T45Log( '3', '', '')
						Break
					EndIf	
				EndIf
			EndIf	
		
		EndIf
	
		(cAliasIFDR)->(dbCloseArea())
	
	EndIf
	
		
	cAliasRV 	:= GetNextAlias()

	BeginSql Alias cAliasRV
		SELECT G3Q_NUMID NUMID
		FROM %table:G3Q% G3Q
		WHERE G3Q.G3Q_FILIAL = %Exp:cFilRv% 
		AND G3Q.G3Q_NUMID = %Exp:cNumID% 
		AND G3Q.G3Q_STATUS = '1' //Pendente
		AND G3Q.G3Q_CONINU = %Exp:cConinu%
		AND G3Q.%NotDel% 
		UNION
		SELECT G3R_NUMID NUMID
		FROM %table:G3R% G3R
		WHERE G3R.G3R_FILIAL = %Exp:cFilRv% 
		AND G3R.G3R_NUMID = %Exp:cNumID% 
		AND G3R.G3R_STATUS = '1' //Pendente
		AND G3R.G3R_CONINU = %Exp:cConinu%
		AND G3R.%NotDel% 
	EndSql

	//N�o tem IV (G3Q) e nem DR (G3R) em aberto no RV
	If (cAliasRV)->(Eof())	
		
		//Atualiza Status do RV (G3P) para Finalizado
		dbSelectArea("G3P")
		G3P->(dbSetOrder(1))
		If G3P->(dbSeek(cFilRv+cNumID))
			RecLock("G3P",.F.)
				G3P->G3P_STATUS := "2" //Finalizado
			G3P->(MsUnLock())
		EndIf							
	EndIf

	(cAliasRV)->(dbCloseArea())
		
Else

	If cAtuIVouDR $ "1|3"
	
		//Atualiza status do IV (G3Q) 
		dbSelectArea("G3Q")
		G3Q->(dbSetOrder(1))
		If G3Q->(dbSeek(xFilial("G3Q",cFilRv)+cNumID+cIDItem+cNumSeq+cConinu))
			If G3Q->G3Q_STATUS == '2'
				If SoftLock("G3Q")
					G3Q->G3Q_STATUS := "1" //Fat. Pendente
					G3Q->(MsUnLock())
				Else
					lErro := .T.					
					DisarmTransaction()
					cId := xFilial("G3Q",cFilRv)+'-'+cNumID+'-'+cIDItem+'-'+cNumSeq+'-'+cConinu
					Help(" ",1,"TurAtuStRV",,STR0074+cId,1,0)		//"Item de Venda bloqueado (G3Q) : "
					T45Log( '1', '', "TurAtuStRV "+xFilial("G3Q",cFilRv)+cNumID)
					T45Log( '2', '', STR0074+cId)
					T45Log( '3', '', '')
					Break
				EndIf		
			EndIf
		EndIf		

	EndIf
	
	If cAtuIVouDR $ "2|3" .AND. !lErro  
	
		//Atualiza status do DR (G3R) 
		dbSelectArea("G3R")
		G3R->(dbSetOrder(1))
		If G3R->(dbSeek(xFilial("G3R",cFilRv)+cNumID+cIDItem+cNumSeq+cConinu))
			If G3R->G3R_STATUS == '3'
				If SoftLock("G3R")
					G3R->G3R_STATUS := "1" //Fat. Pendente
					G3R->(MsUnLock())
				Else
					lErro := .T.					
					DisarmTransaction()
					cId := xFilial("G3R",cFilRv)+'-'+cNumID+'-'+cIDItem+'-'+cNumSeq+'-'+cConinu
					Help(" ",1,"TurAtuStRV",,STR0075+cId,1,0)		//"Documento de Reserva bloqueado (G3R) : "
					T45Log( '1', '', "TurAtuStRV "+xFilial("G3R",cFilRv)+cNumID)
					T45Log( '2', '', STR0075+cId)
					T45Log( '3', '', '')
					Break
				EndIf		
			EndIf
		EndIf		

	EndIf
	
	//Atualiza Status do RV (G3P) para Finalizado
	dbSelectArea("G3P")
	G3P->(dbSetOrder(1))
	If G3P->(dbSeek(xFilial("G3P",cFilRv)+cNumID)) .And. G3P->G3P_STATUS == '2'
		RecLock("G3P",.F.)
			G3P->G3P_STATUS := "1" //Aberto
		G3P->(MsUnLock())
	EndIf
	
EndIf

RestArea(aArea)

Return .T.

//------------------------------------------------------------------------------
/*/{Protheus.doc} TURChkFin()

Esta fun��o retorna as filiais para um Array unidimensional. Poder�o ser 
retornadas: todas as filiais, somente as filiais do grupo de empresas corrente
ou a filial corrente.
 
@sample	TURChkFin(cAlias)
@param
	cAlias	-> Caractere. Alias da Tabela
			
@return	lRet	-> L�gico. .t. encontrou a fatura do Turismo; .f. n�o encontrou
			 		
@author    Fernando Radu Muscalu
@since     04/10/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TURChkFin(cAlias)

Local cFilSeek		:= ""
Local cNxtAlias		:= GetNextAlias()
Local cSearchKey 	:= &(TurKeyTitulo(cAlias))

Local lRet		:= .F.

If ( !FwIsInCallStack("TA45Cancel") .AND.  !FwIsInCallStack("TA45RunCan"))
	
	//Retorna o Conte�do referente a Filial Original, contida na chave
	cFilSeek := Substr(cSearchKey,1,TamSx3(PrefixoCpo(cAlias)+"_FILORIG")[1])
	
	//Busca pela chave da liquida��o
	cChave	:= FinGrvFK7("SE1",cSearchKey)
	
	BEGINSQL Alias cNxtAlias
	
		SELECT
			DISTINCT
			1 EXISTE
		FROM
			%Table:FO1% FO1
		INNER JOIN
			%Table:FO0% FO0
		ON
			FO0_FILIAL = FO1_FILIAL AND
			FO0_PROCES = FO1_PROCES	AND
			FO0_VERSAO = FO1_VERSAO AND
			SUBSTRING(FO0_ORIGEM,1,3) = 'TUR' AND
			FO0.%NotDel%
		WHERE
			FO1_FILIAL = %Exp:cFilSeek% AND
			FO1_IDDOC = %Exp:cChave% AND
			FO1.%NotDel%		
		
		UNION ALL

		SELECT 
			DISTINCT 1 EXISTE 
		FROM  
			%Table:G8E% G8E
		WHERE
			G8E_FILIAL = %Exp:cFilSeek% AND
			G8E_IDDOC = %Exp:cChave% AND
			G8E.%NotDel%
		
	EndSQL
	
	lRet := (cNxtAlias)->EXISTE
	
	(cNxtAlias)->(DbCloseArea())
	
Endif

Return(lRet)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurApuraF3

Fun��o que cria objeto da tela de consulta. Ela � utilizada na consulta espec�fica (SXB) 
G6LFIL.

S�o listadas as apura��es de todas as filiais. 
 
@sample	TurApuraF3()
@param			
@return	.t.
			 		
@author    Fernando Radu Muscalu
@since     06/10/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------

Function TurApuraF3() 

Local cQry		:= ""

Local aRetorno 	:= {}

Local oLookUp

cQry := "SELECT	" + CHR(13)
cQry += "	G6L_FILIAL,	" + CHR(13)
cQry += "	G6L_CODAPU,	" + CHR(13)
cQry += "	G6L_TOTAL,	" + CHR(13)
cQry += "	G6L_DTGERA,	" + CHR(13)
cQry += "	G6L_FORNEC,	" + CHR(13)
cQry += "	G6L_LJFORN,	" + CHR(13)
cQry += "	G6L_CLIENT	" + CHR(13)
cQry += "FROM " + CHR(13)
cQry += "	" + RetSQLName("G6L") + " G6L " + CHR(13)
cQry += "WHERE " + CHR(13)
cQry += "	G6L.D_E_L_E_T_ = ''	AND " + CHR(13)
cQry += "	G6L_STATUS = '2'	" + CHR(13)	//Somente as apura��es que foram liberadas
cQry += "ORDER BY	" + CHR(13)
cQry += "	G6L_FILIAL,	" + CHR(13)
cQry += "	G6L_CODAPU	"

oLookUp := TurXLookUp():New( cQry, {"G6L_CODAPU"})
                                                       
oLookUp:AddIndice( STR0068, "G6L_FILIAL+G6L_CODAPU" 	)//"Filial+Cod. Apura��o"

If ( oLookUp:Execute() )
	cTurApura	:= oLookUp:GetReturn()[1]		
EndIf   

Return(.t.)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurGetApura

Fun��o retorna o conte�do da vari�vel est�tica cTurApura. Esta fun��o � utilizada 
no retorno da consulta espec�fica (SXB) G6LFIL.
 
@sample	TurGetApura()
@param			
@return	cTurApura	-> Caractere. C�digo da apura��o que foi selecionada na 
		pesquisa.
			 		
@author    Fernando Radu Muscalu
@since     06/10/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------

Function TurGetApura()
Return(cTurApura)

/*/{Protheus.doc} ClienteFor
//Retorna array com as informa��es de cliente atrelado ao fornecedor
@author osmar.junior
@since 21/07/2016
@version undefined
@param cFornecedor, characters, descricao
@param cLojaFornec, characters, descricao
@type function
/*/
Function ClienteFor(cFornecedor,cLojaFornec)
Local aCliente := SA2->(GetAdvFVal("SA2", {"A2_NOME","A2_CLIENTE","A2_LOJCLI"}, xFilial('SA2') + cFornecedor + cLojaFornec, 1, {}))

Return aCliente

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TurClearApu
Fun��o respons�vel para atualizar os registros de G4C e G48 para efetuar a desvinvula��o
da apura��o. Nos casos dos acordos de classifica��o Transaction e Combo, ser� efetuado a
exclus�o (l�gica) dos registros

@type 		Function
@author 	Fernando Radu Muscalu
@since 		19/10/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TurClearApu(cFilApu,cApuracao,cCalled)

Local cUpdate	:= ""
Local cWhere	:= ""
Local cNxtAlias	:= ""
Local cExpWhere	:= ""

Local nValFim	:= 0
Local nFee 		:= 0
Local nTotalRec	:= 0

Local aAreaG3Q	:= {}
Local aAreaG48	:= {}

Local lRet		:= .T.

cWhere += "	G48_FILAPU = '" + cFilApu + "' AND " + CHR(13)
cWhere += "	G48_CODAPU = '" + cApuracao + "' AND " + CHR(13)
cWhere += "	G48_CONINU = '" + Space(TamSx3("G48_CONINU")[1]) + "' " + CHR(13)

//Desvincula o c�digo da apura��o do item financeiro
cUpdate := "UPDATE " + CHR(13)
cUpdate += "	" + RetSQLName("G4C") + CHR(13)
cUpdate += "SET " + CHR(13)
cUpdate += "	G4C_CODAPU = '' " + CHR(13)
cUpdate += "FROM " + CHR(13)
cUpdate += "	" + RetSQLName("G48") + " G48 " + CHR(13)
cUpdate += "INNER JOIN " + CHR(13)
cUpdate += "	" + RetSQLName("G4C") + " G4C " + CHR(13)
cUpdate += "ON " + CHR(13)
cUpdate += "	G4C_FILIAL = G48_FILIAL AND " + CHR(13)
cUpdate += "	G4C_NUMID = G48_NUMID AND " + CHR(13)
cUpdate += "	G4C_IDITEM = G48_IDITEM AND " + CHR(13)
cUpdate += "	G4C_NUMSEQ = G48_NUMSEQ AND " + CHR(13)
cUpdate += "	G4C_APLICA = G48_APLICA AND " + CHR(13)
cUpdate += "	G4C_NUMACD = G48_CODACD AND " + CHR(13)
cUpdate += "	G4C_CODAPU = G48_CODAPU AND " + CHR(13)
cUpdate += "	G4C.D_E_L_E_T_ = '' " + CHR(13)
cUpdate += "WHERE " + CHR(13)
cUpdate += cWhere + " AND " + CHR(13)

If ( cCalled == "TURA040" )	//Apura��o de Clientes
	cUpdate += "	G48_CLIFOR = '1' AND " + CHR(13)	
Else						//Apura��o de Fornecedores
	cUpdate += "	G48_CLIFOR = '2' AND " + CHR(13)
EndIf

cUpdate += "	G48.D_E_L_E_T_ = '' " + CHR(13)

If ( TcSqlExec(cUpdate) < 0 )
	lRet := .f.
	FwAlertHelp(STR0064, TcSQLError()) //"N�o foi poss�vel desvincular a apura��o dos itens financeiros."
EndIf

If ( lRet )
	
	//Apura��o de Clientes
	If ( Alltrim(Upper(cCalled)) == "TURA040" )
		
		aAreaG3Q := G3Q->(GetArea())
		aAreaG48 := G48->(GetArea())
		
		//Exclus�o dos Acordos Aplicados de Transaction e Combo (C01 e C05)
		cNxtAlias := GetNextAlias()
		
		cExpWhere	:= "%" + cWhere + "%"	
		
		BeginSQL Alias cNxtAlias
			
			SELECT
				G3Q.R_E_C_N_O_ G3Q_RECNO,
				G48.R_E_C_N_O_ G48_RECNO
			FROM
				%Table:G48% G48
			INNER JOIN
				%Table:G3Q% G3Q
			ON
				G3Q_FILIAL = G48_FILIAL AND
				G3Q_NUMID = G48_NUMID AND
				G3Q_IDITEM = G48_IDITEM AND
				G3Q_NUMSEQ = G48_NUMSEQ AND
				G3Q.%NotDel%
			WHERE
				%Exp:cExpWhere% AND
				G48_CLASS IN ('C01','C05') AND
				G48_CLIFOR = '1' AND
				G48_APUGER = '1' AND 
				G48.%NotDel%
			ORDER BY
				G3Q_FILIAL,
				G3Q_NUMID,
				G3Q_IDITEM,
				G3Q_NUMSEQ,
				G48_CLASS	
		
		EndSQL
		
		While ( (cNxtAlias)->(!EoF()) )
			
			G3Q->(DbGoto((cNxtAlias)->G3Q_RECNO))
			G48->(DbGoto((cNxtAlias)->G48_RECNO))
			
			nFee 		:= G3Q->G3Q_FEE - G48->G48_VLACD
			nTotalRec	:= G3Q->G3Q_TOTREC - G48->G48_VLACD
			
			If ( G3Q->G3Q_OPERAC <> "2" )
				nValFim		:= G3Q->G3Q_VLFIM - G48->G48_VLACD
			Else
				nValFim		:= G3Q->G3Q_VLFIM + G48->G48_VLACD
			Endif	
			
			RecLock("G3Q", .F.)
				
				G3Q->G3Q_FEE := nFee
				G3Q->G3Q_TOTREC := nTotalRec
				G3Q->G3Q_VLFIM := nValFim
				 
			G3Q->(MsUnlock())

			RecLock("G48", .F.)
				G48->(DbDelete())
			G48->(MsUnlock())
				 			
			(cNxtAlias)->(DbSkip())
			
		EndDo
		
		(cNxtAlias)->(DbCloseArea())
		
		RestArea(aAreaG3Q)
		RestArea(aAreaG48)
		
	Endif
	
	If ( lRet )
		
		//Apura��o de Clientes
		If ( Alltrim(Upper(cCalled)) == "TURA040" )
		
			//Atualiza os acordos aplicados, desvinculando a apura��o, inclusive os acordos de servi�os pr�prios que foram apurados
			cUpdate := "UPDATE " + CHR(13)
			cUpdate += "	" + RetSQLName("G48") + CHR(13)
			cUpdate += "SET " + CHR(13)
			cUpdate += "	G48_FILAPU = '', " + CHR(13)
			cUpdate += "	G48_CODAPU = '', " + CHR(13)
			cUpdate += "	G48_OK = 'F' " + CHR(13)
			cUpdate += "WHERE " + CHR(13)
			cUpdate += cWhere + " AND " + CHR(13)
			cUpdate += "	G48_CLIFOR = '1' AND " + CHR(13)
			cUpdate += "	D_E_L_E_T_ = '' "
		
		Else	//Apura��o de Fornecedores
		
			//Atualiza os acordos aplicados, desvinculando a apura��o, inclusive os acordos de servi�os pr�prios que foram apurados
			cUpdate := "UPDATE " + CHR(13)
			cUpdate += "	" + RetSQLName("G48") + CHR(13)
			cUpdate += "SET " + CHR(13)
			cUpdate += "	G48_FILAPU = '', " + CHR(13)
			cUpdate += "	G48_CODAPU = '', " + CHR(13)
			cUpdate += "	G48_MARK = 'F' " + CHR(13)
			cUpdate += "WHERE " + CHR(13)
			cUpdate += cWhere + " AND " + CHR(13)
			cUpdate += "	G48_CLIFOR = '2' AND " + CHR(13)
			cUpdate += "	D_E_L_E_T_ = '' "
			
		EndIf
			
	EndIf 
	
	If ( TcSqlExec(cUpdate) < 0 )
		lRet := .f.
		FwAlertHelp(STR0065, TcSQLError())	//"N�o foi poss�vel desvincular a apura��o dos acordos aplicados."
	EndIf

EndIf

Return(lRet)
//------------------------------------------------------------------------------
/*/{Protheus.doc} TurKeyTitulo()

Esta fun��o retorna string com os campos que formam a chave do campo FK7_CHAVE
 
@sample	TurKeyTitulo(cAlias)
@param
	cAlias	-> Caractere. Alias da Tabela
			
@return	cChave	-> Caractere. String com os Campos sepaprados por Pipe (|)
			 		
@author    Fernando Radu Muscalu
@since     20/10/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TurKeyTitulo(cAlias)

Local cChave	:= ""

If ( cAlias == "SE1" )
	
	cChave := cAlias + "->E1_FILORIG + '|'"
	cChave += " + " + cAlias + "->E1_PREFIXO + '|'"
	cChave += " + " + cAlias + "->E1_NUM + '|'"
	cChave += " + " + cAlias + "->E1_PARCELA + '|'"
	cChave += " + " + cAlias + "->E1_TIPO + '|'"
	cChave += " + " + cAlias + "->E1_CLIENTE + '|'"
	cChave += " + " + cAlias + "->E1_LOJA"
	
Else

	cChave := cAlias + "->E2_FILORIG + '|'"
	cChave += " + " + cAlias + "->E2_PREFIXO + '|'"
	cChave += " + " + cAlias + "->E2_NUM + '|'"
	cChave += " + " + cAlias + "->E2_PARCELA + '|'"
	cChave += " + " + cAlias + "->E2_TIPO + '|'"
	cChave += " + " + cAlias + "->E2_FORNECE + '|'"
	cChave += " + " + cAlias + "->E2_LOJA"
	
Endif

Return(cChave)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TxDestroy

Fun��o para eliminar variaveis da memoria.
 
@sample	TxDestroy(xObj)
@param		xObj	
@return	nulo
			 		
@author    Jacomo Lisa
@since     20/10/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------

Function TxDestroy(xObj)
Local n1 := 0
Local cType := ValType(xObj)
Default xObj := nil
If cType == "O"
	FreeObj(xObj)
	xObj := nil
	DelClassIntf()
ElseIf cType == "A"
	/*
	For n1:= 1 to Len(xObj)
		If ValType(xObj[n1]) == "A" .and. Len(xObj[n1]) > 0
			TxDestroy(xObj[n1])
		Else
			xObj[n1]:= nil
		Endif
	Next
	*/
	aSize(xObj,0)
	xObj := Nil
Else
	xObj := nil
Endif
Return()
//------------------------------------------------------------------------------
/*/{Protheus.doc} TxTraceLog

Gera Log quando utilizado FwTraceLog = 1.
 
@sample	TxTraceLog(cObs)
@param		oObj	
@return	nulo
			 		
@author    Jacomo Lisa
@since     26/10/2016
@version 	12.1.13
/*/
//------------------------------------------------------------------------------
Function TxTraceLog(cObs,cThreadLog,cTimeIni,cTimeMid,cRotina)
Local cMsgLog		:= ""
Local lFwTrace	:= If(GetSrvProfString('FWTRACELOG','2') == '1',.T.,.F.)
Local cTimeFim     := Time()
Default cObs		:= ""
Default cThreadLog := "Thr"+cValToChar(Randomize(0,100000))+"_"+ cValToChar( threadid() )
Default cTimeIni   := Time()
DeFault cTimeMid   := 0
Default cRotina    := ""
If lFwTrace
	
	cMsgLog := "--------------------------------" + Chr(13)+ Chr(10)
	cMsgLog += STR0069 + PROCNAME(1)+ STR0070 + cValToChar(PROCLINE(2))+ Chr(13)+ Chr(10)	//"Fun��o:"#" Linha: "
	cMsgLog += STR0071 + FWTimeStamp(2) + Chr(13)+ Chr(10)									//"Data/Hora: "
	cMsgLog += "Thread: " +cThreadLog+ Chr(13)+ Chr(10)
	cMsgLog += STR0072 + ElapTime ( cTimeIni, cTimeFim)+ Chr(13)+ Chr(10)					//"Tempo Total: "
	
	If !Empty( cTimeMid )
		cMsgLog += STR0073 + cRotina + ": " + ElapTime ( cTimeMid, cTimeFim)+ Chr(13)+ Chr(10)	//"Tempo da rotina "
	Endif
	If !Empty(cObs)
		cMsgLog += cObs+ Chr(13)+ Chr(10)
	Endif
	cMsgLog += "--------------------------------"+ Chr(13)+ Chr(10)
	
	ShowInfMem()
�
Endif
Conout(cMsgLog)
Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurF3Solic

Fun��o para filtro da consulta padr�o SU5RV
 
@sample	TurF3Solic()

@return	nulo
			 		
@author    	Fernando Radu Muscalu
@since     	07/11/2016
@version	12.1.13
/*/
//------------------------------------------------------------------------------
Function TurF3Solic()

Local cRetFil	:= ""

Local oModel	:= FwModelActive()

If ( oModel:GetId() == "TURA034" )

	cRetFil := "@U5_CODCONT IN "
	cRetFil += "( SELECT AC8_CODCON FROM " + RetSQLName("AC8") + " AC8 " 
	cRetFil += " WHERE AC8_FILIAL = U5_FILIAL "			
	cRetFil += " AND AC8_ENTIDA = 'SA1' "
	cRetFil += " AND AC8_CODENT = '" + oModel:GetModel("G3Q_ITENS"):GetValue("G3Q_CLIENT") + oModel:GetModel("G3Q_ITENS"):GetValue("G3Q_LOJA") + "' "
	cRetFil += " AND AC8.D_E_L_E_T_ = '')"
	 
EndIf

Return(cRetFil)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TURXGETEMPFIL

Fun��o que retorna a informa��o de um campo de uma filial do grupo de empresas

@type 		Function
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TurXGetEmpFil(cFilSeek, cCampo)

Local xRet
Local aAreaSM0 := {}
Default cCampo := "M0_FILIAL"

If (cCampo =="M0_FILIAL" )
	xRet := TurGetNameFil(cFilSeek)
Else

	If Alltrim(cFilSeek) <> Alltrim(SM0->M0_CODFIL) 
		
		aAreaSM0 := SM0->(GetArea())
	 	
		SM0->(DbGoTop())
	 	
		While SM0->(!Eof())
	  	
			If ( Alltrim(cFilSeek) == Alltrim(SM0->M0_CODFIL) )
				xRet := SM0->&(cCampo)
				Exit
			Endif
	  	
			SM0->(DbSkip())
		EndDo
	 	
		RestArea(aAreaSM0)
	Else
		xRet := Alltrim(SM0->&(cCampo))	
	Endif	 

Endif

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TurGetNameFil

Fun��o que retorna o nome da filial

@type 		Function
@author 	Fernando Radu Muscalu
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TurGetNameFil(cFilSeek)

Local cRet := ""

Local aFilInfo	:= FWArrFilAtu(,cFilSeek)

If ( Len(Alltrim(cFilSeek)) <> Len(Alltrim(aFilInfo[2])) )
	cRet := aFilInfo[17]
Else
	cRet := aFilInfo[07]
Endif 

Return(cRet)


//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TurOrdViewStruct
Fun��o respons�vel por adicionar campos, inexistentes no dicion�rio de dados da tabela, na 
estrutura do Model/View.

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/04/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Function TurOrdViewStruct(cAlias, oStruct, aNewOrder)

Local cOrdem 	:= ""

Local nI		:= 0

Default aNewOrder := {}

//Ordens Pr�-definidas para as apura��es de Cliente e Fornecedor
If ( FwIsInCallStack("TURA040") .Or. FwIsInCallStack("TURA050") )
	
	If ( cAlias == "G48" )
	
		If ( aScan(aNewOrder,{|x| Alltrim(x[1]) == "G48_CODPRD"}) == 0 )
			aAdd(aNewOrder,{"G48_CODPRD","G48_DSCPRD"})
		Endif
		
		If ( aScan(aNewOrder,{|x| Alltrim(x[1]) == "G48_CLASS"}) == 0 )
			aAdd(aNewOrder,{"G48_CLASS","G48_DSCLAS"})
		Endif	
		
		If ( aScan(aNewOrder,{|x| Alltrim(x[1]) == "G48_BASECL"}) == 0 )
			aAdd(aNewOrder,{"G48_BASECL","G48_NBASEC"})
		Endif
		
		If ( aScan(aNewOrder,{|x| Alltrim(x[1]) == "G48_MOEDA"}) == 0 )
			aAdd(aNewOrder,{"G48_MOEDA","G48_DSCMOE"})
		Endif
	
	EndIf
	//Ordens Pr�-definidas para as apura��es de Cliente e Fornecedor
	If ( cAlias == "G81" )
	
		If ( aScan(aNewOrder,{|x| Alltrim(x[1]) == "G81_CLASS"}) == 0 )
			aAdd(aNewOrder,{"G81_CLASS","G81_DSCLAS"})
		Endif	
		
		If ( aScan(aNewOrder,{|x| Alltrim(x[1]) == "G81_MOEDA"}) == 0 )
			aAdd(aNewOrder,{"G81_MOEDA","G81_DSCMOE"})
		Endif
		
		If ( aScan(aNewOrder,{|x| Alltrim(x[1]) == "G81_LOJA"}) == 0 )
			aAdd(aNewOrder,{"G81_LOJA","G81_DSCCLI"})
		Endif
		
		If ( aScan(aNewOrder,{|x| Alltrim(x[1]) == "G81_CONDPG"}) == 0 )
			aAdd(aNewOrder,{"G81_CONDPG","G81_DCNDPG"})
		Endif
		
		If ( aScan(aNewOrder,{|x| Alltrim(x[1]) == "G81_CODPRD"}) == 0 )
			aAdd(aNewOrder,{"G81_CODPRD","G81_DSCPRD"})
		Endif
		
	EndIf

EndIf
		
For nI := 1 to Len(aNewOrder)
	
	If ( oStruct:HasField(aNewOrder[nI,1]) .And. oStruct:HasField(aNewOrder[nI,2]) )  
	
		cOrdem := oStruct:GetProperty(aNewOrder[nI,1], MVC_VIEW_ORDEM)
	
		TurOrdStruct(oStruct,StrZero(++Val(cOrdem),2),aNewOrder[nI,2])
	
	EndIf
	
Next nI

Return()

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurOrdStruct

Fun��o para Ordena��o de Campos da Estrutura de um submodelo da view (FWFormView)

@Params:	oStrView, Objeto, Obj instanciado da classe FwFormStruct
			cNewOrder, Caractere, Nova Ordem definida
			cField, Caractere, Campo que passa a ter a nova ordem	 
 
@sample	TurOrdStruct(oStrView,cNewOrder,cField)

@return	nulo
			 		
@author    	Fernando Radu Muscalu
@since     	16/11/2016
@version	12.1.13
/*/
//------------------------------------------------------------------------------

Function TurOrdStruct(oStrView,cNewOrder,cField)

Local cNext		:= ""

Local nI		:= 0

Local aFldStr	:= oStrView:GetFields()

nI := aScan(aFldStr,{|x| Alltrim(x[2]) == Alltrim(cNewOrder) })

If ( nI > 0 )
	
	oStrView:SetProperty(cField, MVC_VIEW_ORDEM, cNewOrder)
	
	cNext := StrZero(++Val(aFldStr[nI,2]),2)
	TurOrdStruct(oStrView,cNext,aFldStr[nI,1])	

Else
	oStrView:SetProperty(cField, MVC_VIEW_ORDEM, cNewOrder)
Endif

Return()

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurRetRevAcd

Fun��o que verifica se existe uma determinada revis�o solicitada para o Acordo
especificado no par�metro

@Params:	cCodAcd, Caractere, C�digo do Acordo requisitado
			cFrom, Caractere, Qual � o acordo (C - Cliente; F - Fornecedor)
			cSpecific, Caractere, Localiza a revisao especificada. 
			lLast, L�gico, Busca a �ltima revis�o (.t. - sim; .f. - primeira)

@return:	cRevisao, Caractere, Revis�o localizada

@sample	cRevisao := TurRetRevAcd(cCodAcd,cFrom,cSpecific,lLast)
			 		
@author    	Fernando Radu Muscalu
@since     	16/11/2016
@version	12.1.13
/*/
//------------------------------------------------------------------------------
Function TurRetRevAcd(cCodAcd,cFrom,cSpecific,lLast)

Local lRet		:= .T.

Local cRevisao	:= ""
Local cField	:= ""
Local cTable	:= ""
Local cWhere	:= ""

Local aAreaG5V	:= {}
Local aAreaG4W	:= {}

Default cSpecific	:= ""
Default lLast 		:= .T.

If ( !Empty(cSpecific) )
	
	If ( cFrom == "C" )
		
		aAreaG5V := G5V->(GetArea())
		
		G5V->(DbSetOrder(1))	//G5V_FILIAL+G5V_CODACO+G5V_CODREV
		
		If ( G5V->(DbSeek(xFilial("G5V")+Padr(cCodAcd,TamSX3("G5V_CODACO")[1])+cRevAcd)) )
			cRevisao := G5V->G5V_CODREV
		EndIf
		
		RestArea(aAreaG5V)

	Else

		aAreaG4W := G4W->(GetArea())
		
		G4W->(DbSetOrder(1))	//G4W_FILIAL+G4W_CODACO+G4W_CODREV+G4W_MSBLQL
		
		If ( G4W->(DbSeek(xFilial("G4W")+Padr(cCodAcd,TamSX3("G4W_CODACO")[1])+cRevAcd)) )
			cRevisao := G4W->G4W_CODREV
		EndIf
		
		RestArea(aAreaG4W)

	EndIf

Else
 	
 	If ( cFrom == "C" )
		
		cField := "G5V_CODREV"
		cTable := "G5V"
		
	Else
	
		cField := "G4W_CODREV"
		cTable := "G4W"
		
	EndIf
	
	If ( lLast )
		cField := "% MAX("+cField+") %"
	Else
		cField := "% MIN("+cField+") %"	
	Endif
	
	cWhere := "% " + cTable + "_FILIAL = '" + XFilial(cTable) + "' AND " 
	cWhere += cTable + "_CODACO = '" + cCodAcd + "' AND "
	cWhere += cTable + ".D_E_L_E_T_ = '' %"
	
	cTable := "% " + RetSQLName(cTable) + " " + cTable + " %"
	
	cNxtAlias := GetNextAlias()
	
	BeginSQL Alias cNxtAlias
		
		SELECT
			%Exp:cField% CODREV
		FROM
			%Exp:cTable%
		WHERE				
			%Exp:cWhere%
			
	EndSQL
	
	cRevisao := (cNxtAlias)->CODREV
	
	(cNxtAlias)->(DbCloseArea())
	
Endif

Return(cRevisao)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurGetFile

Fun��o para utilizar em Consulta Espec�fica (SXB) que busca arquivo dentro dos 
diret�rios do computado ou servidor

@return:	.T.
@sample	lRet := TurGetFile() (retorno l�gico obrigat�rio para a consulta espec�fica
			 		
@author    	Fernando Radu Muscalu
@since     	18/11/2016
@version	12.1.13
/*/
//------------------------------------------------------------------------------
Function TurGetFile()

cTurRetPath := cGetFile(,"Localizar Arquivo",0,"",.T.,GETF_RETDIRECTORY+GETF_LOCALHARD) 

Return(.t.)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurRetPath()

Recuperar o conte�do da vari�vel est�tica cTurRetPath

@return:	cTurRetPath, Caractere, Vari�vel est�tica que cont�m o diret�rio
@sample	cRevisao := TurGetFile()
			 		
@author    	Fernando Radu Muscalu
@since     	18/11/2016
@version	12.1.13
/*/
//------------------------------------------------------------------------------
Function TurRetPath()

Return(cTurRetPath)

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} TurLoadSubMdl

Fun��o para montagem do bloco de Carga dos submodelos (fields ou grids) de modelos 
de dados (MVC - ModelDef)

@Params:	oStruct:	Objeto. Objeto da classe FwFormModelStruct
			cTipoSub:	Caractere. Tipo de SubModelo - "F" - Fields; "G" - Grid
			cTable:		Caractere. Nome do Alias da Tabela que dever� carregar os dados 
			bWhile:		Bloco. Bloco de condi��o do la�o While, para a varredura dos 
						registros da tabela de cTable
			nRecNumber:	Num�rico. N�mero do Recno da tabela de cTable
			aInits:		Array. Array com os campos e seus inicializadores padr�es, caso exista.
				aInits[n,1]: Caractere. Nome do Campo
				aInits[n,2]: Bloco. Bloco do Inicializador Padr�o
			lNoRecRet:	L�gico. Retornar� o recno no array aReg? .t. Sim; .f. N�o

@sample	aReg := TurLoadSubMdl(oStruct,cTipoSub,cTable,bWhile,nRecNumber,aInits,lNotRecRet)

@return	aReg:	Array. Array de retorno do bloco de carga ou do Field ou do Grid
			 		
@author    	Fernando Radu Muscalu
@since     	21/12/2016
@version	12.1.13
/*/
//------------------------------------------------------------------------------------------------

Function TurLoadSubMdl(oStruct,cTipoSub,cTable,bWhile,nRecNumber,aInits,lNotRecRet)

Local aAreaSX3	:= {}
Local aFldStr	:= oStruct:GetFields()
Local aData		:= {}
Local aReg		:= {}

Local nI		:= 0
Local nP		:= 0
Local nRec		:= 0

Default bWhile		:= Iif(cTipoSub == "F", {||.t.}, {|| (cTable)->(!Eof()) })
Default nRecNumber	:= 0
Default aInits		:= {}
Default lNotRecRet	:= .T.

If ( nRecNumber > 0 )
	nRec := nRecNumber
Else
	
	If ( !Empty(cTable) )
		nRec := (cTable)->(Recno())
	EndIf	

EndIf

While ( Eval(bWhile) )

	For nI := 1 to Len(aFldStr)
		
		nP := aScan(aInits, {|x| Alltrim(Upper(x[1])) == Alltrim(Upper(aFldStr[nI,3]))})
		
		If ( nP > 0 )
			aAdd(aData,Eval(aInits[nP,2]))
		Else
			
			If ( (cTable)->(FieldPos(aFldStr[nI,3])) > 0 )		
				aAdd(aData,(cTable)->&(aFldStr[nI,3]))
			Else
				
				Do Case 
				Case ( aFldStr[nI,4] == "C" )
					aAdd(aData,"")
				Case ( aFldStr[nI,4] == "N" )
					aAdd(aData,0)
				Case ( aFldStr[nI,4] == "D" )
					aAdd(aData,StoD(""))
				Case ( aFldStr[nI,4] == "L" )
					aAdd(aData,.F.)
				End Case
					
			EndIf	 
				
		Endif
		
	Next nI
	
	If ( cTipoSub == "F" )	//Field
		
		aAdd(aReg,aData)
		
		If ( lNotRecRet )
			aAdd(aReg,nRec)
		EndIf	
		
		bWhile := {||.f.}
	
	ElseIf ( cTipoSub == "G" )	//Grid
		
		aAdd(aReg,{nRec,aData})
		
		(cTable)->(DbSkip())
		
	EndIf
	
	aData := {}
	
EndDo

Return(aReg)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TXVldEmail

Fun��o utilizada para validar o(s) e-mail(s) informado(s) como remetente nas rotinas 
- Apura��o de Receita de Cliente
- Apura��o de Receita de Fornecedor
- Envio Eletr�nico de Fatura

@sample		TXVldEmail()
@param 		cFrom - String com o(s) e-mail(s) 
@return 	lRet - L�gico informando se valido ou n�o
@author 	Thiago Tavares
@since 		21/03/2017
@version 	P12.1.14
/*/
//------------------------------------------------------------------------------------------
Function TXVldEmail(cFrom)

Local lRet    := .T. 
Local aEmails := {}
Local nX      := 0

If At(';', cFrom) > 0
	aEmails := Separa(cFrom, ';')
	For nX := 1 To Len(aEmails)
		If !Empty(aEmails[nX]) .And. !IsEmail(aEmails[nX])
			lRet := .F.
			Exit
		EndIf
	Next nX
ElseIf !IsEmail(cFrom)
	lRet := .F. 
EndIf

Return lRet



/*/{Protheus.doc} TurXFech
Fun��o que retorna o paremetro de fechamento do Cliente, conforme complemento e Grupo de Produto
@author    Jos� Domingos Caldana Jr
@version   1.00
@since     01/06/2017
/*/
Function TurXFech(cFilCompl,cCodCompl,cTpFech,cGrupoPrd)

Local cAliasG64	:= GetNextAlias()
Local aRet			:= {}

BeginSql Alias cAliasG64
	SELECT G64_FILIAL FILIAL, G64_CODIGO CODIGO, G64_ITEM ITEM, G64_TPFECH TPFECH, G64_CONDPG CONDPG  
	FROM %Table:G64% G641 
	WHERE G641.G64_FILIAL = %Exp:cFilCompl%
	AND G641.G64_CODIGO = %Exp:cCodCompl%
	AND G641.G64_APLICA = %Exp:cTpFech%
	AND G641.G64_RELGRP = '2'
	AND G641.%NotDel%
	UNION
	SELECT G64_FILIAL FILIAL,G64_CODIGO CODIGO, G64_ITEM ITEM, G64_TPFECH TPFECH, G64_CONDPG CONDPG  
	FROM %Table:G64% G642
	INNER JOIN %Table:G68% G68 
	ON G68.G68_FILIAL = G642.G64_FILIAL
	AND G68.G68_CODIGO = G642.G64_CODIGO
	AND G68.G68_ITEM = G642.G64_ITEM
	AND G68.G68_CODGRP = %Exp:cGrupoPrd%
	AND G68.%NotDel%
	WHERE G642.G64_FILIAL = %Exp:cFilCompl%
	AND G642.G64_CODIGO = %Exp:cCodCompl%
	AND G642.G64_APLICA = %Exp:cTpFech%
	AND G642.G64_RELGRP = '1'
	AND G642.%NotDel%
EndSql

If (cAliasG64)->(!EOF())
	
	aRet := {(cAliasG64)->(FILIAL),(cAliasG64)->(CODIGO),(cAliasG64)->(ITEM),(cAliasG64)->(TPFECH),(cAliasG64)->(CONDPG)}

EndIf

(cAliasG64)->(DbCloseArea())

Return aRet

/*/{Protheus.doc} TurHelp
Fun��o para encapsular a chamada da fun��o Help
@author    Anderson Toledo
@version   1.00
@since     27/06/2017
@param cProblema, String, String contendo o problema a ser apresentado no Help
@param cSolucao, String, String contendo a solu��o a ser apresentado no Help
@param cTitulo, String, T�tulo a ser apresentado no Help

/*/
Function TurHelp( cProblema, cSolucao, cTitulo )
	DEFAULT cProblema	:= ""
	DEFAULT cSolucao	:= ""
	DEFAULT cTitulo		:= ""

	Help(,,cTitulo,,cProblema,1,0,,,,,,{cSolucao})
Return



//------------------------------------------------------------------------------
/*/{Protheus.doc} TXSmtpConn

Funcao generica para envio de e-mail com conexao ao SMTP na fun��o de chamada

@sample   TXSmtpConn(oMailServer)
@param	  ExpC1 - Objeto TMailMessage
@return   {lRet, nError}
@author   Thiago Tavares 
@since	  04/07/2017
@version  1.0
/*/
//------------------------------------------------------------------------------
Function TXSmtpConn(oMailServer) 

Local nSMTPPort   := SuperGetMV("MV_PORSMTP", .F., 25 )  	// PORTA SMTP
Local cSMTPAddr   := SuperGetMV("MV_RELSERV", .F., "" )  	// ENDERECO SMTP
Local cUser       := SuperGetMV("MV_RELACNT", .F., "" )  	// USUARIO PARA AUTENTICACAO SMTP
Local cPass       := SuperGetMV("MV_RELPSW" , .F., "" )  	// SENHA PARA AUTENTICA SMTP
Local cUserAut    := SuperGetMV("MV_RELAUSR", .F., "" )
Local lAutentica  := SuperGetMV("MV_RELAUTH", .F., .F.) 	// VERIFICAR A NECESSIDADE DE AUTENTICACAO
Local nSMTPTime   := SuperGetMV("MV_RELTIME", .F., 120) 	// TIMEOUT PARA A CONEXAO
Local lSSL        := SuperGetMV("MV_RELSSL" , .F., .F.)  	// VERIFICA O USO DE SSL
Local lTLS        := SuperGetMV("MV_RELTLS" , .F., .F.)  	// VERIFICA O USO DE TLS
Local nError      := 0
Local nPos        := 0
Local cMsgErro    := ''

// Usa SSL, TLS ou nenhum na inicializacao
oMailServer:SetUseSSL(lSSL)
oMailServer:SetUseTLS(lTLS)

// Inicializacao do objeto de Email
If (nPos := AT(":", cSMTPAddr)) > 0
	cSMTPAddr := SubStr(cSMTPAddr, 1, nPos - 1)	
EndIf

If (nError := oMailServer:Init("", cSMTPAddr, cUser, cPass, 0, nSMTPPort)) != 0
	cMsgErro := I18N(STR0040, {oMailServer:getErrorString(nError)}) + Chr(13) + Chr(10) //"Falha ao conectar: #1"
EndIf

// Define o Timeout SMTP
If nError == 0 .AND. oMailServer:SetSMTPTimeout(nSMTPTime) != 0
	nError   := 1
	cMsgErro := STR0041 + Chr(13) + Chr(10)   //"Falha ao definir tempo de espera de envio."
EndIf

// Conecta ao servidor
If nError == 0
	If (nError := oMailServer:SmtpConnect()) != 0
		cMsgErro := I18N(STR0040, {oMailServer:getErrorString(nError)}) + Chr(13) + Chr(10)   //"Falha ao conectar: #1"
		oMailServer:SMTPDisconnect()
	EndIf
EndIf

// Realiza autenticacao no servidor
If nError == 0 .AND. lAutentica
	If (nError := oMailServer:SmtpAuth(cUserAut, cPass)) != 0
		If (nDomain := At("@", cUserAut)) > 0
			If (nError := oMailServer:SmtpAuth(LTrim(Subs(cUserAut, 1, nDomain - 1)), cPass)) != 0
				cMsgErro := I18N(STR0042, {oMailServer:getErrorString(nError)}) + Chr(13) + Chr(10)   //"Falha ao autenticar: #1"
				oMailServer:SMTPDisconnect()
			EndIf
		EndIf
	EndIf
EndIf

Return aClone({nError == 0, cMsgErro, AllTrim(Str(nError))})

//------------------------------------------------------------------------------
/*/{Protheus.doc} TURXEnvio

Funcao generica para envio de e-mail com conexao ao SMTP na fun��o de chamada

@sample   TURXEnvio( cFrom, cTo, cCc, cBcc, cSubject, cBody, aAnexos, oMailServer )

@param	  ExpC1 - Rementente
		  ExpC2 - Destinatario
		  ExpC3 - Copia
		  ExpC4 - Copia Oculta
		  ExpC5 - Assunto
		  ExpC6 - Mensagem / Texto
		  ExpC7 - Anexos
		  ExcC8 - Objeto TMailMessage

@return  lRet

@author   Thiago Tavares 
@since	  03/07/2017
@version  1.0
/*/
//------------------------------------------------------------------------------
Function TURXEnvio(cFrom, cTo, cCc, cBcc, cSubject, cBody, aAnexos, oMailServer) 

Local oMessage 	 := TMailMessage():New()
Local nError     := 0
Local nAnexos    := 0
Local cMsgErro   := ""
 	
Default cFrom    := ""
Default cTo      := ""
Default cCc      := "" 
Default cBcc     := ""
Default cSubject := ""
Default cBody    := ""

oMessage:Clear()
oMessage:cFrom    := SuperGetMV("MV_RELACNT", .F., "")
oMessage:cTo      := cTo
oMessage:cCc      := cCc
oMessage:cBcc     := cBcc
oMessage:cSubject := cSubject
oMessage:cBody    := cBody
oMessage:cReplyTo := IIF(IsInCallStack('TURA066'), cUser, cFrom)
       
For nAnexos := 1 To Len(aAnexos)
	nError := oMessage:AttachFile(aAnexos[nAnexos])
	oMessage:AddAttHTag('Content-ID: <ID_conf_' + aAnexos[nAnexos] + '>')
	If nError != 0
		cMsgErro := STR0043 + Chr(13) + Chr(10)		//"N�o foi poss�vel anexar arquivo. O envio do e-mail ser� abortado."
		Exit
	Endif
Next nAnexos
      
If nError == 0
  	If (nError := oMessage:Send(oMailServer)) != 0
		// validando se erro de conexao
		If (nError := oMailServer:SmtpConnect()) != 0
			aRetorno := TXSmtpConn(@oMailServer)
			If (Len(aRetorno) .And. !aRetorno[1])
				cMsgErro += aRetorno[2] + Chr(13) + Chr(10)
			Else
				If (lRetorno := (nError := oMessage:Send(oMailServer)) <> 0)
					cMsgErro := oMailServer:GetErrorString(nError) + Chr(13) + Chr(10)
				EndIf
			EndIf
		Else
			If (lRetorno := (nError := oMessage:Send(oMailServer)) <> 0)
				cMsgErro := oMailServer:GetErrorString(nError) + Chr(13) + Chr(10)
			EndIf
		EndIf
	EndIf
EndIf

oMessage := Nil

Return aClone({nError == 0, cMsgErro, AllTrim(Str(nError))})


Function TurTpAcd( nOpc, lSql )
	Local aAcordos	:= {}
	Local cAlias 	:= GetNextAlias()
	Local nPos		:= 0

	If nOpc == 1
		BeginSql Alias cAlias
			 SELECT G8B_DESTIN, G8B_CODIGO
			 	FROM %Table:G8B% G8B
				WHERE G8B_DESTIN <> ' '
					AND G8B_MSBLQL <> '1'
					AND G8B_FILIAL = %xFilial:G8B%
					AND G8B.%notDel%	
		EndSql
		
		While (cAlias)->( !EOF() )
			nPos := aScan( aAcordos, {|x| x[1] == AllTrim( (cAlias)->G8B_DESTIN )  } )
		
			If nPos > 0
				If lSql
					aAcordos[nPos][2] += ",'" + AllTrim( (cAlias)->G8B_CODIGO ) + "'"
				Else
					aAdd( aAcordos[nPos][2], AllTrim( (cAlias)->G8B_CODIGO ) )
				EndIf
				
			Else
				If lSql
					aAdd( aAcordos, { AllTrim( (cAlias)->G8B_DESTIN ),;	
										"'" + AllTrim( (cAlias)->G8B_CODIGO ) + "'" } )
				Else
					aAdd( aAcordos, { AllTrim( (cAlias)->G8B_DESTIN ),{ AllTrim( (cAlias)->G8B_CODIGO ) } } )
				EndIf
				
			EndIf
		
		
			(cAlias)->( dbSkip() )
		EndDo
		
		(cAlias)->( dbCloseArea() )
	EndIf

Return aAcordos

//------------------------------------------------------------------------------
/*/{Protheus.doc} TXVldChvExt

Funcao que valida a duplicidade da chave externa enviada na integra��o

@sample   TXVldChvExt(cTable, cTblAlias, cChvExt)

@param	  ExpC1 - Tabela 
		  ExpC2 - Alias
		  ExpC3 - Chave Externa

@return  lRet

@author   Thiago Tavares 
@since	  06/09/2017
@version  1.0
/*/
//------------------------------------------------------------------------------
Function TXVldChvExt(cTable, cTblAlias, cChvExt)

Local aArea    := GetArea()
Local lRet     := .F. 
Local cChvInt  := ""
Local nPos     := aScan(aTblCods, {|x| x[1] == CtblAlias})
Local cCampo   := IIF(nPos > 0, aTblCods[nPos][2] + Space(10 - Len(aTblCods[nPos][2])), Space(10))
Local cMarca   := "PROTHEUS" + Space(15 - Len("PROTHEUS"))

DbSelectArea('XXF')
XXF->(DbSetOrder(1))	// XXF_REFER+XXF_TABLE+XXF_ALIAS+XXF_FIELD+XXF_EXTVAL+XXF_INTVAL
If XXF->(DbSeek(cMarca + cTable + cTblAlias + cCampo + cChvExt))
	lRet    := .T.
	cChvInt := AllTrim(CFGA070INT(cMarca, cTblAlias, cCampo, cChvExt))
EndIf
XXF->(DbCloseArea())

RestArea(aArea)

Return aClone({lRet, cChvInt})

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurSetDir()

Funcao que chama CGETFILE para definir caminho de pasta de arquivos.
Fun��o criada para ser utilizada em consulta padr�o em perguntes.

@return  lRet, .T. Se um caminho foi definido.

@author   paulo.barbosa
@since	  13/10/2017
@version  1.0
/*/
//------------------------------------------------------------------------------
Function TurSetDir()
_TURDIRRET := cGetFile(, STR0076, 0, "", .T., GETF_RETDIRECTORY + GETF_LOCALHARD + GETF_NETWORKDRIVE, .T., .T.)
Return !EMPTY(_TURDIRRET)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TurRetDir()

Complemento da fun��o TurSetDir(), usada para retornar o caminho definido.

@return  _TURDIRRET, Contendo o caminho selecionado pelo usu�rio na fun��o TurSetDir()

@author   paulo.barbosa
@since	  13/10/2017
@version  1.0
/*/
//------------------------------------------------------------------------------
Function TurRetDir()
Return _TURDIRRET


//------------------------------------------------------------------------------
/*/{Protheus.doc} TurGetNum()

Verifica se o Numero retornado pelo GETSXENUM j� existe no BD, se existir d� rollback e pega outro

@return  numero
/*/
//------------------------------------------------------------------------------
Function TurGetNum(cTable,cCampo,cNumOld)
Local cNum 		:= ""
Local cAliasNum 	:= GetNextAlias()
Local cTbl 		:= "%"+cTable+alltrim(cEmpAnt)+"0%"
Local cFld 		:= ""
Local cCampoFil 	:= PrefixoCpo(cTable)+"_FILIAL" 
Local lJatem		:= .F.
Default cNumOld 	:= ""

cNum := GetSXENum(cTable,cCampo)                                          

cFld := "%"+cCampoFil+" = '"+xFilial(cTable)+"' AND "+cCampo+" = '"+cNum+"'%"

BeginSql Alias cAliasNum
	SELECT %exp:cCampoFil% 
	FROM %exp:cTbl% TBL
	WHERE 
	%exp:cFld%
	AND TBL.%NotDel%
EndSql

lJatem := !(cAliasNum)->(EOF())

(cAliasNum)->(DbCloseArea())

If cNumOld == cNum
	If !isBlind() 
		FwAlertHelp(STR0077)
	EndIf
ElseIf lJatem
	ConfirmSX8()
	cNum := TurGetNum(cTable,cCampo,cNum)	
EndIf

Return cNum

//------------------------------------------------------------------------------
/*/{Protheus.doc} TURXNIL()

Fun��o utilizada para destruir vari�veis/objetos 

@param 		xVariavel - array ou objeto a ser destru�do
@return  	
@author   	thiago.tavares
@since	  	21/11/2017
@version  	1.0
/*/
//------------------------------------------------------------------------------
Function TURXNIL(xVariavel)

Local nX := 0

If xVariavel != Nil
	If ValType(xVariavel) == "A"
		For nX := Len(xVariavel) To 1 Step -1
			If ValType(xVariavel[nX]) == "A" .And. Len(xVariavel[nX]) > 1
				TURXNIL(xVariavel[nX])
			Else
				aSize(xVariavel, 0)
				Exit
			EndIf
		Next nX
	EndIf

	xVariavel := Nil
EndIf

Return .T.


/*/{Protheus.doc} TurRetFil()

Fun��o que retorna o c�digo da filial completa para atribuir ao cFilAnt, a partir do conteudo _FILIAL de uma tabela
Esta fun��o pode ser utilizada no retorno de Query, onde precisa setar a filial do registro antes de editar um model por exemplo
pois ela avalia o compartilhamento da tabela e retorna uma filial v�lida  

@param 		cTalela - Alias da tabela (Caracter)
@param 		cCodFil - Conte�do do campo _FILIAL 
@return  	
@author   	Jose Domingos Caldana Jr
@since	  	21/11/2017
@version  	1.0
/*/
Function TurRetFil(cTabela,cCodFil)
Local cCompE		:= FWModeAccess(cTabela,1)
Local cCompU		:= FWModeAccess(cTabela,2)
Local cCompF		:= FWModeAccess(cTabela,3)
Local cRetFil		:= ""
Local aArrFil		:= {} 


If cCompE=="E" .And. cCompU=='E' .And. cCompF=='E'
	cRetFil := cCodFil
Else
	If cCompE=="E" .And. cCompU=='E' .And. cCompF=='C' 
		aArrFil := FWAllFilial(SubStr(cCodFil,1,Len(FWSM0Layout(,1))),SubStr(cCodFil,Len(FWSM0Layout(,1))+1,Len(FWSM0Layout(,2))),,.F.)
	
	ElseIf cCompE=="E" .And. cCompU=='C' .And. cCompF=='C' 
		aArrFil := FWAllFilial(SubStr(cCodFil,1,Len(FWSM0Layout(,1))),,,.F.)
	
	ElseIf cCompE=="C" .And. cCompU=='C' .And. cCompF=='C' 
		aArrFil := FWAllFilial(,,,.F.)
	
	EndIf
	
	If Len(aArrFil) > 0
		cRetFil := aArrFil[1]	
	EndIf
EndIf

Return cRetFil

Function TurExpFil(cTabela,cAliasTab,cTabBase,cAliasTabRef)
Local cCompE		:= FWModeAccess(cTabela,1)
Local cCompU		:= FWModeAccess(cTabela,2)
Local cCompF		:= FWModeAccess(cTabela,3)
Local nTam			:= FWSizeFilial()
Local cRet			:= '%%'

If cCompE=='C' //Empresa Compartilhada
	nTam -= Len(FWSM0Layout(,1))
EndIf

If cCompU=='C' //Unidade Compartilhada
	nTam -= Len(FWSM0Layout(,2))
EndIf

If cCompF=='C' //Filial Compartilhada
	nTam -= Len(FWSM0Layout(,3))
EndIf

If nTam > 0
	cRet := "%SUBSTRING("+cAliasTab+"."+PrefixoCpo(cTabela)+"_FILIAL,1,"+ALLTRIM(STR(nTam))+") = SUBSTRING("+cAliasTabRef+"."+cTabBase+"_FILIAL,1,"+ALLTRIM(STR(nTam))+") AND%"
EndIf

Return cRet