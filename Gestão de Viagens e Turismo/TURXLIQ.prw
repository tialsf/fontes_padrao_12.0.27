#Include 'Protheus.ch'
#Include "FWMVCDEF.CH"
#Include "TURXLIQ.ch"

#DEFINE OPER_LIQUIDAR  16

//------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TurLiqAut
Rotina autom�tica de Liqui��o

@param	nOpc 		   Identifica Inclus�o ou Exclus�o do processo
@param	dDateProc	   Data do Processo da Liquida��o
@param	aBaixar		   Informa��es dos t�tulos a serem baixados
@param	aParcelas	   Informa��es das parcelas a gerar
@param	cOrigin		   Origem da Liquida��o
@param	cErrorMessage  Mensagem de erro em caso de falha no processo (MVC)

@author	Mauricio Pequim Jr
@version 12.1.13
@since	19/07/2016
@return	aRetorno Array contendo os campos da chave primaria da natureza e o seu internalid.
@sample	exemplo de retorno - {.T., {'Empresa', 'xFilial', 'Codigo Processo','Vers�o'},InternalId}
/*/									//   01          02         03             04 
//-------------------------------------------------------------------------------------------------
Function TurLiqAut(cPrefix, cNumTit, cTipo, cNatureza, nOpc, dDateProc, aBaixar, aParcelas, cOrigin, cErrorMessage )

Local oModel	:= NIL
Local oFO0		:= NIL
Local oFO1		:= NIL
Local oFO2		:= NIL
Local cLastParc	:= ""
Local cNaturez	:= ""
Local cVersao 	:= '0001'	
Local cNomeCli	:= ''
Local cCliente	:= ''
Local cLoja		:= ''
Local cChaveTit := ""
Local cChaveFK7 := ""

Local nMoeda	:= 1
Local nTxMulta	:= 0
Local nTxJuros	:= 0
Local nTxJurGer	:= 0
Local nCount	:= 0
Local nTamArr	:= 0	
Local nValBxAnt	:= 0 
Local nLinAtu  	:= 0
Local nTotLiq	:= 0
Local nTotNeg	:= 0
Local nTotAbImp	:= 0
Local nTotAbat	:= 0
Local nAbat		:= 0
Local nValAcres	:= 0
Local nValMul	:= 0
Local nValJuros	:= 0
Local nValDesc	:= 0
Local nValDecre	:= 0
Local nI		:= 0

Local lRet		:= .T.

Private cLote		:= LoteCont("FIN")	//vari�vel utilizada dentro da fun��o F460ACommit (fonte FINA460A) 

Default dDateProc := dDatabase
Default cOrigin	  := "TURA045A"

Private lOpcAuto  := .T.	//vari�vel utilizada dentro da fun��o F460VLDFO2(..)
Private cMarca		:= Nil
//Inclus�o
If nOpc == 3

	//Carrego as perguntas do processo de liquida��o
	pergunte("AFI460",.F.)
	
	_nOper := OPER_LIQUIDAR
	
	cLastParc	:= PadR(SuperGetMV('MV_1DUP'  ,.F.," "  ), TamSX3("E1_PARCELA")[1] )
	
	If lRet
	
		cNLiquid	:= F460NumLiq()
		
		lAuto := .T.
		
		DbSelectArea('FO0') //Cabe�alhO Liquida��o 
		DbSelectArea('FO1') //Titulos Geradores.
		DbSelectArea('FO2') //Parcelas da Liquida��o.	
		
		oModel := FWLoadModel("FINA460A")			//Carrega estrutura do model
		oModel:SetOperation(�MODEL_OPERATION_INSERT�)�//Define opera��o de inclusao
		oModel:Activate()							//Ativa o model	
		
		oFO0 := oModel:GetModel('MASTERFO0')
		oFO1 := oModel:GetModel('TITSELFO1')
		oFO2 := oModel:GetModel('TITGERFO2')
		
		//O primeiro cliente das parcelas a serem geradas ser� o cliente do processo
		cCliente	:= aParcelas[1][3]
		cLoja		:= aParcelas[1][4]
		cNomeCli	:= Posicione("SA1",1,xFilial("SA1") + cCliente + cLoja, "A1_NOME")
		
		//-------------------------------------------------------------------------------------------------------
		//Carga da FO0 - Cabe�alho da Liquida��o
		//-------------------------------------------------------------------------------------------------------
		cProcess := FINIDPROC("FO0","FO0_PROCES",cVersao)
			
		oFO0:LoadValue("FO0_PROCES"	, cProcess	)
		oFO0:LoadValue("FO0_VERSAO"	, cVersao	)
		oFO0:LoadValue("FO0_RAZAO"	, cNomeCli	)
		oFO0:LoadValue("FO0_CLIENT"	, cCliente	)
		oFO0:LoadValue("FO0_LOJA"	, cLoja		)
		oFO0:LoadValue("FO0_TIPO"	, cTipo		)
		oFO0:LoadValue("FO0_NATURE"	, cNatureza	)
		oFO0:LoadValue("FO0_MOEDA"	, nMoeda	)
		oFO0:LoadValue("FO0_DATA"	, dDateProc	)
		oFO0:LoadValue("FO0_DTVALI"	, dDateProc	) 
		oFO0:LoadValue("FO0_TXJUR"	, nTxJuros	)
		oFO0:LoadValue("FO0_TXMUL"	, nTxMulta	)
		oFO0:LoadValue("FO0_TXJRG"	, nTxJurGer	)
		oFO0:LoadValue("FO0_NUMLIQ"	, cNLiquid	)
		oFO0:LoadValue("FO0_ORIGEM"	, cOrigin	)
		oFO0:LoadValue("FO0_EFETIVA", "1"	)
		
		//ver ao final do arquivo, mapa do array aBaixar
		nTamArr := Len(aBaixar)
		
		For nCount := 1 To nTamArr
		
			nRecno		:= aBaixar[nCount,01]
			nValor		:= aBaixar[nCount,02]			
			nJuros		:= aBaixar[nCount,03]
			nDesco		:= aBaixar[nCount,04]
			nValMul		:= aBaixar[nCount,05]
			nValAcres	:= aBaixar[nCount,06] + aBaixar[nCount,07]
			nValDecre	:= aBaixar[nCount,08]
			
			//nAbat		:= aBaixar[nCount,05]	//n�o usa
			
			SE1->(dbGoTo(nRecno))
			
			nValBxAnt := SE1->E1_VALOR - SE1->E1_SALDO
			nTotAbImp := 0
			nTotAbat  := SumAbatRec(SE1->E1_PREFIXO,SE1->E1_NUM,SE1->E1_PARCELA,SE1->E1_MOEDA,"S",dDateProc,@nTotAbImp)
			nAbat	  := nTotAbat - nTotAbImp  
			
			cChaveTit := SE1->E1_FILORIG +"|"+ SE1->E1_PREFIXO +"|"+ SE1->E1_NUM +"|"+ SE1->E1_PARCELA +"|"+ SE1->E1_TIPO +"|"+ SE1->E1_CLIENTE +"|"+ SE1->E1_LOJA
			cChaveFK7 := FINGRVFK7("SE1", cChaveTit, SE1->E1_FILORIG)
					
			If !oFO1:IsEmpty()
				oFO1:AddLine()
			EndIf
			
			oFO1:LoadValue("FO1_MARK"	, .T.				)
			oFO1:LoadValue("FO1_PROCES"	, oFO0:GetValue("FO0_PROCES")	)
			oFO1:LoadValue("FO1_VERSAO"	, oFO0:GetValue("FO0_VERSAO")	)
			oFO1:LoadValue("FO1_FILORI"	, SE1->E1_FILORIG	)
			oFO1:LoadValue("FO1_PREFIX"	, SE1->E1_PREFIXO	)
			oFO1:LoadValue("FO1_NUM"	, SE1->E1_NUM		)
			oFO1:LoadValue("FO1_PARCEL"	, SE1->E1_PARCELA	)
			oFO1:LoadValue("FO1_TIPO"	, SE1->E1_TIPO		)
			oFO1:LoadValue("FO1_CLIENT"	, SE1->E1_CLIENTE	)
			oFO1:LoadValue("FO1_LOJA"	, SE1->E1_LOJA		)
			oFO1:LoadValue("FO1_NATURE"	, SE1->E1_NATUREZ	)
			oFO1:LoadValue("FO1_IDDOC"	, cChaveFK7			)
			oFO1:LoadValue("FO1_MOEDA"	, SE1->E1_MOEDA		)
			oFO1:LoadValue("FO1_TXMOED"	, SE1->E1_TXMOEDA	)
			oFO1:LoadValue("FO1_EMIS"	, SE1->E1_EMISSAO	)
			oFO1:LoadValue("FO1_VENCTO"	, SE1->E1_VENCTO	)
			oFO1:LoadValue("FO1_VENCRE"	, SE1->E1_VENCREA	)
			oFO1:LoadValue("FO1_SALDO"	, SE1->E1_SALDO		)
			oFO1:LoadValue("FO1_BAIXA"	, SE1->E1_BAIXA		)
			oFO1:LoadValue("FO1_VLBAIX"	, nValBxAnt			)
			oFO1:LoadValue("FO1_HIST"	, SE1->E1_HIST		)
			oFO1:LoadValue("FO1_TXJUR"	, 0					)
			oFO1:LoadValue("FO1_VLDIA"	, 0					)
			oFO1:LoadValue("FO1_VLJUR"	, nJuros			)
			oFO1:LoadValue("FO1_TXMUL"	, 0					)
			oFO1:LoadValue("FO1_VLMUL"	, nValMul			)
			oFO1:LoadValue("FO1_DESCON"	, nDesco			)
			oFO1:LoadValue("FO1_VLABT"	, nAbat				)
			oFO1:LoadValue("FO1_ACRESC"	, nValAcres			)
			oFO1:LoadValue("FO1_DECRES"	, nValDecre			)
			oFO1:LoadValue("FO1_VALCVT"	, 0					)
			oFO1:LoadValue("FO1_TOTAL"	, nValor			)
			
			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			//Aqui Grava se SE1 para o caso do Acrescimo e decrecimo , quando for cancelado efetuar a baixa completa//
			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			RecLock("SE1",.F.)
			
				SE1->E1_ACRESC	:= nValAcres
				SE1->E1_DECRESC := nValDecre
						
			SE1->(MsUnlock())
			
			//Totalizador de valor a liquidar
			nTotLiq += nValor
			
		Next nCount
		
		//ver coment�rio ao final sobre o mapa de aParcelas		
		nTamArr := Len(aParcelas)
		
		For nCount := 1 To nTamArr		
			
			If !oFO2:IsEmpty()
				oFO2:AddLine()
			EndIf
			
			///////////////////////////////////////////////////////////////////
			//Aqui eu fa�o a utiliza��o dos valores informado no XML pela RM //
			// Valida��o para saber se o cara enviou a chave completa caso   //
			// ele envia uma chave faltando deixa para execAuto recusar
			///////////////////////////////////////////////////////////////////
			If(!Empty(aParcelas[nCount,05]) .Or.  !Empty(aParcelas[nCount,07]) .Or. !Empty(aParcelas[nCount,06]))
				
				cPrefix  := ""
				cNaturez := ""
				cNumTit  := ""
				
				cPrefix  := aParcelas[nCount,05]
				cNaturez := aParcelas[nCount,07]
				cNumTit  := aParcelas[nCount,06]
				
				If(SE1->(MsSeek(xFilial("SE1")+cPrefix+cNumTit+cLastParc+cTipo)))
				
					lRet := .F.
					cErrorMessage := STR0001	//"Titulo Informado na nova parcela ja existe"
					Exit
				
				EndIf
				
			EndIf
			
			oFO2:LoadValue("FO2_IDSIM" ,FWUUIDV4() ) //Chave ID tabela FK1.
			oFO2:LoadValue("FO2_PROCES",oFO0:GetValue("FO0_PROCES")) //Processo
			oFO2:LoadValue("FO2_VERSAO",oFO0:GetValue("FO0_VERSAO")) //Vers�o

			oFO2:LoadValue("FO2_PREFIX", cPrefix)
			
			//Gero numero da Parcela
			nLinAtu := oFO2:GetLine()
	
			oFO2:GoLine(nLinAtu)
	
			oFO2:LoadValue("FO2_NUM"   , cNumTit)
			oFO2:LoadValue("FO2_PARCEL", aParcelas[nCount,08])		// parcela

			oFO2:LoadValue("FO2_VENCTO", aParcelas[nCount,02])		// data vencto
			oFO2:LoadValue("FO2_VALOR" , aParcelas[nCount,01])		// valor da parcela
			
			oFO2:LoadValue("FO2_TXJUR" , 0 )
			oFO2:LoadValue("FO2_VLJUR" , 0 )

			nValParc := aParcelas[nCount,1] + oFO2:GetValue("FO2_VLJUR")
			
			oFO2:LoadValue("FO2_VLPARC", nValParc)
			oFO2:LoadValue("FO2_TOTAL" , nValParc) //valor total negociado

			nTotNeg += nValParc
			
			///////////////////////////////////////////////
			// Grava os Valores acessorios na tabela FKD //
			///////////////////////////////////////////////
			cChaveTit := FWxFilial( "SE1" ) + "|" + cPrefix + "|" + cNumTit + "|" + cLastParc + "|" + cTipo + "|" + cCliente + "|" + cLoja
			cChaveFK7 := FINGRVFK7( "SE1", cChaveTit )
			
		Next nCount
		
		oFO0:LoadValue("FO0_VLRLIQ"	, nTotLiq )
		oFO0:LoadValue("FO0_VLRNEG"	, nTotNeg )

		If oModel:VldData()
			If oModel:CommitData()
				cErrorMessage := ""	
			Else
				lRet := .F.
				cErrorMessage := 'FINI460MOD - ' 
				cErrorMessage += cValToChar(oModel:GetErrorMessage()[4]) + ' - ' 
				cErrorMessage += cValToChar(oModel:GetErrorMessage()[6]) + ' - ' 
				cErrorMessage += cValToChar(oModel:GetErrorMessage()[8])	
			Endif
		Else
			lRet := .F.
			cErrorMessage := 'FINI460MOD - ' 
			cErrorMessage += cValToChar(oModel:GetErrorMessage()[4]) + ' - ' 
			cErrorMessage += cValToChar(oModel:GetErrorMessage()[6]) + ' - ' 
			cErrorMessage += cValToChar(oModel:GetErrorMessage()[8])	
		EndIf	
		oModel:DeActivate()
		oModel:Destroy()
		oModel:= Nil
	Endif	
Endif
		
Return lRet		

/*
	MAPA DO ARRAY aBaixar
		
		//-------------------------------------------------------------------------------------------------------
		//Usado para efetuar a carga da FO1 - Titulos a Baixar
		//-------------------------------------------------------------------------------------------------------
		//Estrutura aBaixar
		//[01] Recno SE1
		//[02] Valor Baixa
		//[03] Valor Juros
		//[04] Valor Desconto
		//[05] Valor Acrescimo
		//[06] Valor Multa
		//[07] Valor de Acrescimo
		//[08] Valor de Decrescimo
	
	MAPA DO ARRAY aParcelas
		
		//-------------------------------------------------------------------------------------------------------
		//Usado para efetuar a carga da FO2 - Titulos a Gerar
		//-------------------------------------------------------------------------------------------------------
		//Estrutura aParcelas
		//[01] Valor da Parcela
		//[02] Data de Vencimento
		//[03] Cliente
		//[04] Loja 
		//[05] Prefixo do Titulo
		//[06] Numero do Titulo
		//[07] Natureza do Titulo
		//[08] Parcela do Titulo
	
*/