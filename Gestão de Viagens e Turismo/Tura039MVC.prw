#Include "TOTVS.CH" 
#Include "FWMVCDEF.CH"
#Include "TURA039MVC.CH"

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tura39Conc
Fun��o para concilia��o a�rea, utilizando model reduzido

@type function
@author Anderson Toledo
@since 04/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Tura39Conc(nOpc)

Local aButtons := {{.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.T., STR0001}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}} 
			       		 
//A variavel aTpAcd � private por quest�o de performance, pois � utilizada na carga dos totalizadores da concilia��o	//A variavel aTpAcd � private por quest�o de performance, pois � utilizada na carga dos totalizadores 
//e � executada a cada load de linha via OnDemand  
Private aTpAcd := TurTpAcd(1, .T.)

//As FWTemporaryTable s�o privates pois s�o usadas pelo ModelDef e ViewDef, n�o s�o utilizadas vari�veis static
//pelo alto consumo de mem�ria
Private oTmpTotG3R	:= Nil
Private oTmpTotG3Q	:= Nil

nOpc := IIF(nOpc == 1, MODEL_OPERATION_VIEW, MODEL_OPERATION_UPDATE)

//Cria��o da tabela tempor�ria
oTmpTotG3R := FWTemporaryTable():New() 
oTmpTotG3R:SetFields(T39ViewStruct(T39MVCFields(1)))
oTmpTotG3R:AddIndex("index1", {"G3R_FILIAL", "G3R_NUMID", "G3R_IDITEM"})
oTmpTotG3R:Create()

oTmpTotG3Q := FWTemporaryTable():New() 
oTmpTotG3Q:SetFields(T39ViewStruct(T39MVCFields(2)))
oTmpTotG3Q:AddIndex("index1", {"G3Q_FILIAL", "G3Q_NUMID", "G3Q_IDITEM"})
oTmpTotG3Q:Create()
	
FWExecView(STR0002, "TURA039MVC", nOpc, , {|| .T.}, , , aButtons)	//"Concilia��o A�rea"

oTmpTotG3R:Delete()
oTmpTotG3Q:Delete()
oTmpTotG3R := Nil
oTmpTotG3Q := Nil
	
Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados reduzido para concilia��o a�rea

@type function
@author Anderson Toledo
@since 04/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel   	 := MPFormModel():New("TURA039MVC", /*bPreValidacao*/, /*bPosValid*/, /*bCommit*/, /*bCancel*/)
Local oStruG6I 	 := FWFormStruct(1, "G6I", /*bAvalCampo*/, .F.) 
Local oStrTotG3R := FWFormModelStruct():New()
Local oStrTotG3Q := FWFormModelStruct():New()
Local lExistTot	 := Type("oTmpTotG3R") == "O" .And. Type("oTmpTotG3Q") == "O"
Local bLoadG6J	 := {|oModel| T39ViewLoadGrid(oModel, 0)}
Local bLoadG3R	 := {|oGrid|  T39ViewLoadGrid(oGrid , 1)}
Local bLoadG3Q	 := {|oGrid|  T39ViewLoadGrid(oGrid , 2)}
Local bFormG3R   := {|oModel| oModel:GetValue("TOTAL_ITENS_G3R","G3R_TARIFA") + oModel:GetValue("TOTAL_ITENS_G3R", "G3R_TAXA") + oModel:GetValue("TOTAL_ITENS_G3R","G3R_TXREE") + ; 
	                          oModel:GetValue("TOTAL_ITENS_G3R","G3R_TAXADU") + oModel:GetValue("TOTAL_ITENS_G3R","G3R_VLCOMI")	+ oModel:GetValue("TOTAL_ITENS_G3R","G3R_VLINCE") + ;
	                          oModel:GetValue("TOTAL_ITENS_G3R","G3R_TXFORN")}
Local bFormG3Q   := {|oModel| oModel:GetValue("TOTAL_ITENS_G3Q","G3Q_TARIFA") + oModel:GetValue("TOTAL_ITENS_G3Q","G3Q_MARKUP")	+ oModel:GetValue("TOTAL_ITENS_G3Q","G3Q_TAXA") + ; 
							  oModel:GetValue("TOTAL_ITENS_G3Q","G3Q_EXTRA") + oModel:GetValue("TOTAL_ITENS_G3Q","G3Q_TAXADU") + oModel:GetValue("TOTAL_ITENS_G3Q","G3Q_DESC") + ;
							  oModel:GetValue("TOTAL_ITENS_G3Q","G3Q_TAXAAG") + oModel:GetValue("TOTAL_ITENS_G3Q","G3Q_FEE") + oModel:GetValue("TOTAL_ITENS_G3Q","G3Q_REPASS")}

If lExistTot
	//Cria��o da estrutura da tabela de totalizadores
	oStrTotG3R:AddTable(oTmpTotG3R:GetAlias(), , STR0003, {|| oTmpTotG3R:GetRealName()}) //"Total"
	oStrTotG3Q:AddTable(oTmpTotG3Q:GetAlias(), , STR0003, {|| oTmpTotG3Q:GetRealName()}) //"Total"
	
	T39MVCFld(oStrTotG3R, 1, 1)
	T39MVCFld(oStrTotG3Q, 1, 2)
EndIf

//Cria��o dos campos virtuais da G6I
T39VtFldM(oStruG6I)
oStruG6I:SetProperty("*", MODEL_FIELD_WHEN, {|| .F.})
oStruG6I:SetProperty("TMP_OK", MODEL_FIELD_WHEN, {|| .T.})
oStruG6I:SetProperty("G6I_SITUAC", MODEL_FIELD_WHEN, {|| .T.})

// Adiciona a descricao do Modelo de Dados
oModel:SetDescription(STR0002) //"Concilia��o A�rea"

// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
oModel:AddFields("G6J_MASTER", /*cOwner*/, FWFormStruct(1, "G6J", /*bAvalCampo*/, .F.), /*bPreValidacao*/, /*bPosValidacao*/, bLoadG6J)
oModel:GetModel("G6J_MASTER"):SetDescription(STR0002) //"Concilia��o A�rea"
oModel:GetModel("G6J_MASTER"):SetOnlyView(.T.)		
oModel:GetModel("G6J_MASTER"):SetOnlyQuery(.T.)

// Itens da Fatura 
oModel:AddGrid("G6I_ITENS", "G6J_MASTER", oStruG6I, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G6I_ITENS", {{"G6I_FILIAL", "xFilial('G6J')"}, {"G6I_CONCIL", "G6J_CONCIL"}}, G6I->(IndexKey(7)))
oModel:GetModel("G6I_ITENS"):SetOptional(.T.)
oModel:GetModel("G6I_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G6I_ITENS"):SetNoInsertLine(.T.)
oModel:GetModel("G6I_ITENS"):SetNoDeleteLine(.T.)
oModel:GetModel("G6I_ITENS"):SetDescription(STR0004) //"Itens da Fatura (Bilhetes)"

// Documento de reserva
oModel:AddGrid("G3R_ITENS", "G6I_ITENS", FWFormStruct(1, "G3R", /*bAvalCampo*/, .F.), /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G3R_ITENS", {{"G3R_FILCON", "G6I_FILIAL"}, {"G3R_CONCIL", "G6I_CONCIL"}, {"G3R_MSFIL", "G6I_FILDR"}, {"G3R_NUMID", "G6I_NUMID"}, {"G3R_IDITEM", "G6I_IDITEM"}}, G3R->(IndexKey(2)))
oModel:GetModel("G3R_ITENS"):SetOptional(.T.)
oModel:GetModel("G3R_ITENS"):SetOnlyView(.T.)		
oModel:GetModel("G3R_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G3R_ITENS"):SetDescription(STR0005) //"Documentos de Reserva"

// Item de Venda (G3Q_ITENS ser� somente para consulta, este modelo est� ligado diretamente a G6I ao inv�s de G3R
oModel:AddGrid("G3Q_ITENS", "G3R_ITENS", FWFormStruct(1, "G3Q", /*bAvalCampo*/, .F.), /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G3Q_ITENS", {{"G3Q_FILIAL", "G6I_FILDR"}, {"G3Q_NUMID", "G6I_NUMID"}, {"G3Q_IDITEM", "G6I_IDITEM"}}, G3Q->(IndexKey(1)))
oModel:GetModel("G3Q_ITENS"):SetOptional(.T.)
oModel:GetModel("G3Q_ITENS"):SetOnlyView(.T.)		
oModel:GetModel("G3Q_ITENS"):SetOnlyQuery(.T.)

If lExistTot
	oModel:AddGrid("TOTAL_ITENS_G3R", "G6I_ITENS", oStrTotG3R, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, bLoadG3R)
	oModel:SetRelation("TOTAL_ITENS_G3R", {{"G3R_FILCON", "G6I_FILIAL"}, {"G3R_CONCIL", "G6I_CONCIL"}, {"G3R_MSFIL", "G6I_FILDR"}, {"G3R_NUMID", "G6I_NUMID"}, {"G3R_IDITEM", "G6I_IDITEM"}}, "G3R_FILIAL+G3R_NUMID+G3R_IDITEM")
	oModel:GetModel("TOTAL_ITENS_G3R"):SetOptional(.T.)
	oModel:GetModel("TOTAL_ITENS_G3R"):SetNoInsertLine(.T.)
	oModel:GetModel("TOTAL_ITENS_G3R"):SetNoDeleteLine(.T.)
	oModel:GetModel("TOTAL_ITENS_G3R"):SetOnlyQuery(.T.)
	
	oModel:AddCalc("VLRLIQ_G3R", "G6I_ITENS", "TOTAL_ITENS_G3R", "G3R_TARIFA", "XXX_VLRLIQ", "FORMULA", {||.T.} /*bCondition*/, /*bInitValue*/, STR0006 /*cTitle*/, bFormG3R /*bFormula*/) //"Valor L�quido"

	//Por utilizar uma tabela tempor�ria com o resultado de uma query, possui campos da G3Q e G3R
	oModel:AddGrid("TOTAL_ITENS_G3Q", "G6I_ITENS", oStrTotG3Q, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, bLoadG3Q)
	oModel:SetRelation("TOTAL_ITENS_G3Q", {{"G3R_FILCON", "G6I_FILIAL"}, {"G3R_CONCIL", "G6I_CONCIL"}, {"G3R_MSFIL", "G6I_FILDR"}, {"G3Q_NUMID", "G6I_NUMID"}, {"G3Q_IDITEM", "G6I_IDITEM"}}, "G3Q_FILIAL+G3Q_NUMID+G3Q_IDITEM")
	oModel:GetModel("TOTAL_ITENS_G3Q"):SetOptional(.T.)
	oModel:GetModel("TOTAL_ITENS_G3Q"):SetNoInsertLine(.T.)
	oModel:GetModel("TOTAL_ITENS_G3Q"):SetNoDeleteLine(.T.)
	oModel:GetModel("TOTAL_ITENS_G3Q"):SetOnlyQuery(.T.)

	oModel:AddCalc("VLRLIQ_G3Q", "G6I_ITENS", "TOTAL_ITENS_G3Q", "G3Q_TARIFA", "XXX_VLRLIQ", "FORMULA", {||.T.} /*bCondition*/, /*bInitValue*/, STR0006 /*cTitle*/, bFormG3Q /*bFormula*/) //"Valor L�quido"
EndIf
				
oModel:SetOnDemand(.T.)

oModel:SetVldActivate({|oModel| Tura039Act(oModel)})
	
Return oModel

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Vis�o reduzida para concilia��o a�rea

@type function
@author Anderson Toledo
@since 04/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ViewDef()

Local oView	      := FWFormView():New()
Local oModel  	  := FWLoadModel("TURA039MVC")
Local oStrCalcG3R := Nil
Local oStrCalcG3Q := Nil
Local oStruG6I    := FWFormStruct(2, "G6I")
Local oStruG3R    := FWFormStruct(2, "G3R")
Local oStrTotG3R  := FWFormViewStruct():New()
Local oStrTotG3Q  := FWFormViewStruct():New()
Local lExistTot	  := Type("oTmpTotG3R") == "O" .And. Type("oTmpTotG3Q") == "O"

//Cria��o dos campos virtuais da G6I
T39VtFldV(oStruG6I)

If lExistTot
	oStrCalcG3R := FWCalcStruct(oModel:GetModel("VLRLIQ_G3R"))
	oStrCalcG3Q := FWCalcStruct(oModel:GetModel("VLRLIQ_G3Q"))

	//Cria��o dos campos virtuais de totalizadores
	T39MVCFld(oStrTotG3R, 2, 1)
	T39MVCFld(oStrTotG3Q, 2, 2)
EndIf

TA41ViewOrder(oStruG3R, "TURA039MVC", "G3R_ITENS")

// Define qual o Modelo de dados ser� utilizado
oView:SetModel(oModel)

oView:AddUserButton(STR0007, STR0007, {|oModel| Tur039Des(), Tur39Refresh()}, , , {MODEL_OPERATION_INSERT, MODEL_OPERATION_UPDATE}) 						//"Desassociar"###"Desassociar"
oView:AddUserButton(STR0008, STR0008, {|oModel| Tur039Apr()}                , , , {MODEL_OPERATION_INSERT, MODEL_OPERATION_UPDATE}) 						//"Aprovar/Desaprovar"###"Aprovar/Desaprovar"
oView:AddUserButton(STR0009, STR0009, {|| T039VMrAll()}                     , , , {MODEL_OPERATION_INSERT, MODEL_OPERATION_UPDATE}) 						//"Marcar todos"###"Marcar todos"
oView:AddUserButton(STR0010, STR0010, {|| T039AltRV(), Tur39Refresh()}      , , , {MODEL_OPERATION_INSERT, MODEL_OPERATION_UPDATE}) 						//"Alterar RV"###"Alterar RV" 
oView:AddUserButton(STR0011, STR0011, {|| T039VisRV(), Tur39Refresh()}      , , , {MODEL_OPERATION_VIEW  , MODEL_OPERATION_INSERT, MODEL_OPERATION_UPDATE})	//"Visualizar RV"###"Visualizar RV" 
oView:AddUserButton(STR0012, STR0012, {|| TA39AltFOP(), Tur39Refresh()}     , , , {MODEL_OPERATION_INSERT, MODEL_OPERATION_UPDATE}) 						//"Alt. FOP/Moeda"###"Alt. FOP/Moeda"
oView:AddUserButton(STR0013, STR0013, {|| TA39CAlFOP(), Tur39Refresh()}     , , , {MODEL_OPERATION_INSERT, MODEL_OPERATION_UPDATE}) 						//"Canc. Alt. FOP/Moeda"###"Canc. Alt. FOP/Moeda"
oView:AddUserButton(STR0022, STR0022, {|| TURA039K(), Tur39Refresh()}       , , , {MODEL_OPERATION_INSERT, MODEL_OPERATION_UPDATE}) 						//"Naturezas"###"Naturezas"
oView:AddUserButton(STR0014, STR0014, {|| Tur039Leg()}) 																									//"Legendas"###"Legendas"

oView:AddField("VIEW_G6J", FWFormStruct(2, "G6J"), "G6J_MASTER")

oView:AddGrid("VIEW_G6I", oStruG6I, "G6I_ITENS")
oView:EnableTitleView("VIEW_G6I", STR0004) //"Itens da Fatura(Bilhetes)"

oView:AddGrid("VIEW_G3R", oStruG3R, "G3R_ITENS")
oView:EnableTitleView("VIEW_G3R", STR0005) //"Documentos de Reserva"
	
oView:AddGrid("VIEW_G3Q", FWFormStruct(2, "G3Q"), "G3Q_ITENS")
oView:EnableTitleView("VIEW_G3Q", STR0023) // "Item de venda"

If lExistTot
	oView:AddGrid("VIEW_TOTAL_G3R", oStrTotG3R, "TOTAL_ITENS_G3R")
	oView:EnableTitleView("VIEW_TOTAL_G3R", IIF(G6J->G6J_TIPO == "2", STR0028, STR0015)) // "Totais a receber" ### "Totais a pagar"

	oView:AddField("VIEW_CALC_G3R", oStrCalcG3R, "VLRLIQ_G3R")
	oView:EnableTitleView("VIEW_CALC_G3R", STR0024) //"Fornecedor"

	oView:AddGrid("VIEW_TOTAL_G3Q", oStrTotG3Q, "TOTAL_ITENS_G3Q")
	oView:EnableTitleView("VIEW_TOTAL_G3Q", IIF(G6J->G6J_TIPO == "2", STR0028, STR0015)) // "Totais a receber" ### "Totais a pagar"

	oView:AddField("VIEW_CALC_G3Q", oStrCalcG3Q, "VLRLIQ_G3Q")
	oView:EnableTitleView("VIEW_CALC_G3Q", STR0025) //"Cliente"
EndIf

// Divis�o Horizontal
oView:CreateHorizontalBox("ALL", 100)
oView:CreateFolder("FOLDER_ALL", "ALL")

// Pastas
oView:AddSheet("FOLDER_ALL", "ABA_ALL_CABEC", STR0017) //"Concilia��o" 
oView:AddSheet("FOLDER_ALL", "ABA_ALL_ITENS", STR0018) //"Itens Concilia��o" 

oView:CreateHorizontalBox("BOX_G6J"	, 100, , , "FOLDER_ALL", "ABA_ALL_CABEC")
oView:CreateHorizontalBox("BOX_G6I"	, 040, , , "FOLDER_ALL", "ABA_ALL_ITENS")
oView:CreateHorizontalBox("BOX_DOWN", 060, , , "FOLDER_ALL", "ABA_ALL_ITENS")

oView:CreateVerticalBox("BOX_DOWN_RV"	, 100, "BOX_DOWN", .F., "FOLDER_ALL", "ABA_ALL_ITENS")
oView:CreateVerticalBox("BOX_DOWN_TOTAL", 150, "BOX_DOWN", .T., "FOLDER_ALL", "ABA_ALL_ITENS")

oView:CreateHorizontalBox("BOX_CALC_G3R", 50, "BOX_DOWN_TOTAL", .F., "FOLDER_ALL", "ABA_ALL_ITENS")
oView:CreateHorizontalBox("BOX_CALC_G3Q", 50, "BOX_DOWN_TOTAL", .F., "FOLDER_ALL", "ABA_ALL_ITENS")

oView:CreateFolder("FOLDER_RV", "BOX_DOWN_RV")
oView:AddSheet("FOLDER_RV", "SHEET_G3R", STR0026) //"Documento de reserva"
oView:CreateHorizontalBox("BOX_G3R"	     , 060, , , "FOLDER_RV", "SHEET_G3R")
oView:CreateHorizontalBox("BOX_TOTAL_G3R", 040, , , "FOLDER_RV", "SHEET_G3R")

oView:AddSheet("FOLDER_RV", "SHEET_G3Q", STR0027) //"Item de venda"
oView:CreateHorizontalBox("BOX_G3Q"	     , 060, , , "FOLDER_RV", "SHEET_G3Q")
oView:CreateHorizontalBox("BOX_TOTAL_G3Q", 040, , , "FOLDER_RV", "SHEET_G3Q")

oView:SetOwnerView("VIEW_G6J", "BOX_G6J")
oView:SetOwnerView("VIEW_G6I", "BOX_G6I")
oView:SetOwnerView("VIEW_G3R", "BOX_G3R")
oView:SetOwnerView("VIEW_G3Q", "BOX_G3Q")

If lExistTot
	oView:SetOwnerView("VIEW_TOTAL_G3R", "BOX_TOTAL_G3R")
	oView:SetOwnerView("VIEW_TOTAL_G3Q", "BOX_TOTAL_G3Q")
	oView:SetOwnerView("VIEW_CALC_G3R" , "BOX_CALC_G3R")
	oView:SetOwnerView("VIEW_CALC_G3Q" , "BOX_CALC_G3Q")
EndIf

oView:SetAfterViewActivate({|| T39MVCActivate()}) 
oView:SetViewProperty("VIEW_G6I", "GRIDDOUBLECLICK", {{|oGrid, cField, nLineGrid, nLineModel| Ta039DbClk(oGrid, cField, nLineGrid, nLineModel)}})
	
Return oView

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39MVCActivate
Fun��o para manipular o modelo, evitar mensagem que n�o houve altera��o ao clicar no bot�o OK.

@type function
@author Anderson Toledo
@since 04/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39MVCActivate()

Local oModel := FwModelActive()
Local oView  := FwViewActive()
	
oView:SetModified(.T.)
oModel:lModify := .T.

Return .T.

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39ViewStruct
Retorna a estrutura dos campos utilizados na tabela tempor�ria de total

@type function
@author Anderson Toledo
@since 04/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39ViewStruct(aFields)

Local aArea := GetArea()
Local aAux  := {}
Local nX    := 0

SX3->(DbSetOrder(2))	// X3_CAMPO
For nX := 1 To Len(aFields)
	If SX3->(DbSeek(aFields[nX][1])) 	
		aAdd(aAux, {AllTrim(aFields[nX][1]), TamSX3(aFields[nX][1])[3], TamSX3(aFields[nX][1])[1], TamSX3(aFields[nX][1])[2]})
	EndIf
Next nX

RestArea(aArea)

Return aAux

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39MVCFld
Adiciona os campos da tabela tempor�ria ao metadados do model e view

@type function
@author Anderson Toledo
@since 04/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39MVCFld(oStr, nOpc, nTipo)

Local aArea      := GetArea()
Local aCBox		 := {}
Local aStruTotal := T39MVCFields(nTipo)
Local nX		 := 0

SX3->(DbSetOrder(2))	// X3_CAMPO
For nX := 1 To Len(aStruTotal)
	If SX3->(DbSeek(aStruTotal[nX][1])) 	
		If nOpc == 1 .Or. aStruTotal[nX][2]
			If nOpc == 1
				oStr:AddField(GetSX3Cache(aStruTotal[nX][1], "X3_TITULO"), ; 	// [01] C Titulo do campo
							  GetSX3Cache(aStruTotal[nX][1], "X3_DESCRIC"), ; 	// [02] C ToolTip do campo
							  AllTrim(aStruTotal[nX][1]), ; 					// [03] C identificador (ID) do Field
							  TamSX3(aStruTotal[nX][1])[3], ; 					// [04] C Tipo do campo
							  TamSX3(aStruTotal[nX][1])[1], ; 					// [05] N Tamanho do campo
							  TamSX3(aStruTotal[nX][1])[2], ; 					// [06] N Decimal do campo
							  Nil, ; 											// [07] B Code-block de valida��o do campo
							  Nil, ; 											// [08] B Code-block de valida��o When do campoz
							  Nil, ; 											// [09] A Lista de valores permitido do campo
							  Nil, ; 											// [10] L Indica se o campo tem preenchimento obrigat�rio
							  Nil, ; 											// [11] B Code-block de inicializacao do campo
							  Nil, ; 											// [12] L Indica se trata de um campo chave
							  .F., ; 											// [13] L Indica se o campo pode receber valor em uma opera��o de update.
							  .F.)   											// [14] L Indica se o campo � virtual
	
			ElseIf nOpc == 2 
				If !Empty(X3CBox())
					aCBox := Separa(X3CBox(), ";")
				Else
					aCBox := Nil
				Endif
				
				oStr:AddField(AllTrim(aStruTotal[nX][1]), ; 						// [01] C Nome do Campo
							  StrZero(nX, 2), ;         							// [02] C Ordem
							  GetSX3Cache(aStruTotal[nX][1], "X3_TITULO"), ; 		// [03] C Titulo do campo
							  GetSX3Cache(aStruTotal[nX][1], "X3_DESCRIC"), ; 		// [04] C Descri��o do campo
							  Nil, ; 												// [05] A Array com Help
							  TamSX3(aStruTotal[nX][1])[3], ; 						// [06] C Tipo do campo
							  GetSX3Cache(aStruTotal[nX][1], "X3_PICTURE"), ; 		// [07] C Picture
							  Nil, ; 												// [08] B Bloco de Picture Var
							  "", ;  												// [09] C Consulta F3
							  .F., ; 												// [10] L Indica se o campo � edit�vel
							  Nil, ; 												// [11] C Pasta do campo
							  Nil, ; 												// [12] C Agrupamento do campo
							  aCBox, ;  											// [13] A Lista de valores permitido do campo (Combo)
							  Nil, ; 												// [14] N Tamanho M�ximo da maior op��o do combo
							  Nil, ; 												// [15] C Inicializador de Browse
							  .F., ; 												// [16] L Indica se o campo � virtual
							  Nil) 	 												// [17] C Picture Vari�vel
			EndIf
		EndIf
	EndIf
Next nX

RestArea(aArea)

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39ViewLoadGrid

Fun��o para a carga de dados da tabela tempor�ria de totalizadores
@type function
@author Anderson Toledo
@since 04/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39ViewLoadGrid(oObj, nOpc)

Local aArea     := GetArea()
Local oModel 	:= Nil
Local oMdlAux	:= Nil	
Local cAliasTMP	:= ""

If nOpc == 0
	T39TMPG3R(G6J->G6J_FILIAL, G6J->G6J_CONCIL)
	T39TMPG3Q(G6J->G6J_FILIAL, G6J->G6J_CONCIL)
	Return FWLoadByAlias(oObj, "G6J", RetSqlName("G6J"), /*Recno*/, /*lCopy*/, .F.)
	
ElseIf nOpc == 1
	cAliasTMP := oTmpTotG3R:GetAlias()
	oModel 	  := FwModelActive()
	oMdlAux	  := oModel:GetModel("G6I_ITENS")	
		
	(cAliasTMP)->(DbSetOrder(1))
	If (cAliasTMP)->(DbSeek(oMdlAux:GetValue("G6I_FILDR") + oMdlAux:GetValue("G6I_NUMID") + oMdlAux:GetValue("G6I_IDITEM")))
		Return {{(cAliasTMP)->(Recno()), {(cAliasTMP)->G3R_FILIAL, ; 
			                              (cAliasTMP)->G3R_NUMID, ; 
			                              (cAliasTMP)->G3R_IDITEM, ;
										  (cAliasTMP)->G3R_DOC, ;
										  (cAliasTMP)->G3R_TARIFA, ;
										  (cAliasTMP)->G3R_TAXA, ;
										  (cAliasTMP)->G3R_TXREE, ;
										  (cAliasTMP)->G3R_TAXADU, ;
										  (cAliasTMP)->G3R_VLCOMI, ;
										  (cAliasTMP)->G3R_VLINCE, ;
										  (cAliasTMP)->G3R_TXFORN}}}
	EndIf
Else
	cAliasTMP := oTmpTotG3Q:GetAlias()
	oModel    := FwModelActive()
	oMdlAux   := oModel:GetModel("G6I_ITENS")	
	
	(cAliasTMP)->(DbSetOrder(1))
	If (cAliasTMP)->(DbSeek(oMdlAux:GetValue("G6I_FILDR") + oMdlAux:GetValue("G6I_NUMID") + oMdlAux:GetValue("G6I_IDITEM")))
		Return {{(cAliasTMP)->(Recno()), {(cAliasTMP)->G3R_FILCON, ; 
			                              (cAliasTMP)->G3R_CONCIL, ;
										  (cAliasTMP)->G3R_MSFIL, ;
										  (cAliasTMP)->G3Q_FILIAL, ;
										  (cAliasTMP)->G3Q_NUMID, ;
										  (cAliasTMP)->G3Q_IDITEM, ;
										  (cAliasTMP)->G3Q_DOC, ;
										  (cAliasTMP)->G3Q_TARIFA, ;
										  (cAliasTMP)->G3Q_MARKUP, ;
										  (cAliasTMP)->G3Q_PRCBAS, ;
										  (cAliasTMP)->G3Q_TAXA, ;
										  (cAliasTMP)->G3Q_EXTRA, ;
										  (cAliasTMP)->G3Q_TAXADU, ;
										  (cAliasTMP)->G3Q_VLRSER, ;
										  (cAliasTMP)->G3Q_DESC, ;
										  (cAliasTMP)->G3Q_PRECAG, ;
										  (cAliasTMP)->G3Q_TAXAAG, ;
										  (cAliasTMP)->G3Q_FEE, ;
										  (cAliasTMP)->G3Q_REPASS}}}
	EndIf
EndIf

RestArea(aArea)
	
Return {}

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39MVCFields
Retorna vetor com os campos do metadados: 
	posi��o[1] Nome do campo
	posi��o[2] Valor l�gico que indica se o campo pertence a View

@type function
@author Anderson Toledo
@since 04/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39MVCFields(nOpc)

Local aFields := {}

If nOpc == 1
	aFields := {{"G3R_FILIAL", .F.}, {"G3R_NUMID", .F.}, {"G3R_IDITEM", .F.}, {"G3R_DOC", .T.}, {"G3R_TARIFA", .T.}, {"G3R_TAXA", .T.}, {"G3R_TXREE", .T.}, {"G3R_TAXADU", .T.}, {"G3R_VLCOMI", .T.}, {"G3R_VLINCE", .T.}, {"G3R_TXFORN", .T.}}
Else
	aFields := {{"G3R_FILCON", .F.}, {"G3R_CONCIL", .F.}, {"G3R_MSFIL", .F.}, {"G3Q_FILIAL", .F.}, {"G3Q_NUMID", .F.}, {"G3Q_IDITEM", .F.}, {"G3Q_DOC", .T.}, {"G3Q_TARIFA", .T.}, {"G3Q_MARKUP", .T.}, {"G3Q_PRCBAS", .T.}, {"G3Q_TAXA", .T.}, {"G3Q_EXTRA", .T.}, {"G3Q_TAXADU", .T.}, {"G3Q_VLRSER", .T.} , {"G3Q_DESC", .T.}, {"G3Q_PRECAG", .T.}, {"G3Q_TAXAAG", .T.}, {"G3Q_FEE", .T.}, {"G3Q_REPASS", .T.}}
EndIf					
	
Return aFields

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39SQLAC
Fun��o para totalizar os itens de acordo com a classifica��o

@type function
@author Anderson Toledo
@since 04/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39SQLAC(cField, cTpAcd)

Local cAux 	  := ""
Local cSqlAcd := "' '"
Local nPos	  := 0

If !Empty(cTpAcd)
	If (nPos := aScan(aTpAcd, {|x| x[1] == cTpAcd})) > 0
		cSqlAcd := aTpAcd[nPos][2]
	EndIf
	
	If G6J->G6J_TIPO == "2"
		cAux := 	"SUM((CASE WHEN G4C_PAGREC = '2' AND G4C_CLASS IN (" + cSqlAcd + ") THEN " + cField + " "
		cAux += 			  "WHEN G4C_PAGREC = '1' AND G4C_CLASS IN (" + cSqlAcd + ") THEN " + cField + " * -1 ELSE 0 "
		cAux += 		 "END)) "
	Else
		cAux := 	"SUM((CASE WHEN G4C_PAGREC = '2' AND G4C_CLASS IN (" + cSqlAcd + ") THEN " + cField + " * -1"
		cAux += 			  "WHEN G4C_PAGREC = '1' AND G4C_CLASS IN (" + cSqlAcd + ") THEN " + cField + " ELSE 0 "
		cAux += 		 "END)) "
	EndIf
Else
	If cField == "G4C_TXRORI" 
		cAux := 	"SUM((CASE WHEN (G4C_OPERAC = '2' AND G4C_PAGREC = '1') OR (G4C_OPERAC <> '2' AND G4C_PAGREC = '2') THEN " + cField + "  ELSE " + cField + " * -1 END)) "	
	Else
		cAux := 	"SUM((CASE WHEN (G4C_OPERAC = '2' AND G4C_PAGREC = '1') OR (G4C_OPERAC <> '2' AND G4C_PAGREC = '2') THEN " + cField + " * -1 ELSE " + cField + " END)) "	
	EndIf
EndIf
	
Return cAux

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39VtFldM
Fun��o para cria��o dos campos virtuais referente ao Model

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39VtFldM(oStruG6I)

Local aAux	:= {}
Local bInit := {|| IIF(G6I->G6I_SITUAC == "1", "BR_VERMELHO", IIF(G6I->G6I_SITUAC == "2", "BR_AMARELO", IIF(G6I->G6I_SITUAC $ "45", "BR_LARANJA", "BR_VERDE")))}

oStruG6I:AddField("", ; 		// [01] C Titulo do campo
				  "", ; 		// [02] C ToolTip do campo
				  "TMP_OK", ; 	// [03] C identificador (ID) do Field
				  "L", ; 		// [04] C Tipo do campo
				  1, ; 			// [05] N Tamanho do campo
				  0, ; 			// [06] N Decimal do campo
				  Nil, ; 		// [07] B Code-block de valida��o do campo
				  Nil, ; 		// [08] B Code-block de valida��o When do campoz
				  Nil, ; 		// [09] A Lista de valores permitido do campo
				  Nil, ; 		// [10] L Indica se o campo tem preenchimento obrigat�rio
				  Nil, ; 		// [11] B Code-block de inicializacao do campo
				  .F., ; 		// [12] L Indica se trata de um campo chave
				  .F., ; 		// [13] L Indica se o campo pode receber valor em uma opera��o de update.
				  .F.)   		// [14] L Indica se o campo � virtual

oStruG6I:AddField("", ; 			// [01] C Titulo do campo
				  "", ; 			// [02] C ToolTip do campo
				  "G6I_LEGASS", ; 	// [03] C identificador (ID) do Field
				  "BT", ; 			// [04] C Tipo do campo
				  1, ; 	  			// [05] N Tamanho do campo
				  0, ;    			// [06] N Decimal do campo
				  {|| .T. }, ; 		// [07] B Code-block de valida��o do campo
				  Nil, ; 			// [08] B Code-block de valida��o When do campoz
				  Nil, ; 			// [09] A Lista de valores permitido do campo
				  .F., ; 			// [10] L Indica se o campo tem preenchimento obrigat�rio
				  bInit, ;  		// [11] B Code-block de inicializacao do campo
				  Nil, ; 			// [12] L Indica se trata de um campo chave
				  .T., ; 			// [13] L Indica se o campo pode receber valor em uma opera��o de update.
				  .T.)   			// [14] L Indica se o campo � virtual
Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39VtFldV
Fun��o para cria��o dos campos virtuais referente a View

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39VtFldV(oStruG6I)

oStruG6I:AddField("TMP_OK", ; 		// [01] C Nome do Campo
				  "00", ; 	  		// [02] C Ordem
				  "", ;   	  		// [03] C Titulo do campo
				  "", ;   	  		// [04] C Descri��o do campo
				  Nil, ;  	  		// [05] A Array com Help
				  "L", ;  	  		// [06] C Tipo do campo
				  Nil, ;  	  		// [07] C Picture
				  Nil, ;  	  		// [08] B Bloco de Picture Var
				  "", ;   	  		// [09] C Consulta F3
				  .T., ;  	  		// [10] L Indica se o campo � edit�vel
				  Nil, ;  	  		// [11] C Pasta do campo
				  Nil, ;  	  		// [12] C Agrupamento do campo
				  Nil, ;  	  		// [13] A Lista de valores permitido do campo (Combo)
				  Nil, ;  	  		// [14] N Tamanho M�ximo da maior op��o do combo
				  Nil, ;  	  		// [15] C Inicializador de Browse
				  .T., ;  	  		// [16] L Indica se o campo � virtual
				  Nil) 	  	  		// [17] C Picture Vari�vel

oStruG6I:AddField("G6I_LEGASS", ; 		// [01] C Nome do Campo
				   "01", ; 				// [02] C Ordem
				   "", ;   				// [03] C Titulo do campo
				   "", ;   				// [04] C Descri��o do campo
				   {}, ;   				// [05] A Array com Help
				   "BT", ; 				// [06] C Tipo do campo
				   "", ;  				// [07] C Picture
				   Nil, ; 				// [08] B Bloco de Picture Var
				   Nil, ; 				// [09] C Consulta F3
				   .F., ; 				// [10] L Indica se o campo � edit�vel
				   "", ;  				// [11] C Pasta do campo
				   Nil, ; 				// [12] C Agrupamento do campo
				   Nil, ; 				// [13] A Lista de valores permitido do caadampo (Combo)
				   Nil, ; 				// [14] N Tamanho M�ximo da maior op��o do combo
				   Nil, ; 				// [15] C Inicializador de Browse
				   .T., ; 				// [16] L Indica se o campo � virtual
				   Nil)   				// [17] C Picture Vari�vel
Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Ta039DbClk
Fun��o para visualizar as legendas quando clicado com duplo click no campo de legenda

@type function
@author Jacomo Lisa 
@since 19/01/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function Ta039DbClk(oGrid, cField, nLineGrid, nLineModel)

If cField == "G6I_LEGASS"
	Tur039Leg()
ElseIf cField == "G6I_BILHET" .Or. cField == "G6I_LOCALI"	
	CopyToClipboard(oGrid:GetModel():GetValue(cField))
Endif

Return .T. 

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tur39Refresh
Atualiza a view

@type function
@author Jacomo Lisa 
@since 19/01/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Tur39Refresh()

Local oView	 := FwViewActive()
Local oModel := oView:GetModel()

oModel:DeActivate()
oModel:Activate()
			
FWMsgRun( , {|| oView:Refresh()}, , STR0031)	// "Aguarde enquanto o processo est� sendo finalizado."

Return 

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tura039Act
Seta como alterado o model para quando clicar no bot�o fechar n�o tenha problema caso n�o tenha alterado nada na tela.

@type Function
@author Anderson Toledo
@since 29/01/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Tura039Act(oModel)

Local nOpc := oModel:GetOperation()
Local lRet := .T.

If nOpc == MODEL_OPERATION_UPDATE
	If G6J->G6J_EFETIV == "1" .And. !FwIsInCallStack("TA039DEfe")
		TurHelp(STR0019, STR0021, "TURMVCACT1") //"Concilia��o j� efetivada, altera��o n�o permitida."###"Estorne a efetiva��o para prosseguir."
		lRet := .F.
	Else
		oModel:lModify := .T.
	EndIf

ElseIf nOpc == MODEL_OPERATION_DELETE
	If G6J->G6J_EFETIV = "1"
		TurHelp(STR0020, STR0021, "TURMVCACT2") //"Concilia��o j� efetivada, exclus�o n�o permitida."###"Estorne a efetiva��o para prosseguir."
		lRet := .F.
	EndIf
EndIf

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39TMPG3R
Fun��o para adicionar informa��es na tabela tempor�ria referente aos totais do documento de reserva

@type Function
@author Anderson Toledo
@since 10/10/2017
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T39TMPG3R(cFilCon, cConcil)

Local cSts := ""

(oTmpTotG3R:GetAlias())->(__dbZap())

cSts := "INSERT INTO " + oTmpTotG3R:GetRealName() + " (G3R_FILIAL, G3R_NUMID, G3R_IDITEM, G3R_DOC, G3R_TARIFA, G3R_TAXA, G3R_TXREE, G3R_TAXADU, G3R_VLCOMI, G3R_VLINCE, G3R_TXFORN)"
cSts += 	" SELECT G3R_FILIAL, G3R_NUMID, G3R_IDITEM, G3R_DOC,"  
cSts +=				T39SQLAC("G4C_TARIFA", "")             + ","
cSts +=				T39SQLAC("(G4C_TAXA + G4C_EXTRA)", "") + ","
cSts +=				T39SQLAC("G4C_TXRORI", "") 	           + ","
cSts +=				T39SQLAC("G4C_VALOR", "G3Q_TAXADU")    + ","
cSts +=				T39SQLAC("G4C_VALOR", "G3R_VLCOMI")    + ","
cSts +=				T39SQLAC("G4C_VALOR", "G3R_VLINCE")    + ","
cSts +=				T39SQLAC("G4C_VALOR", "G3R_TXFORN")    + " "
cSts += 	" FROM "+ RetSqlName("G3R") + " G3R"
cSts += 	" INNER JOIN " + RetSqlName("G6I") + " G6I ON G6I_CONCIL = G3R_CONCIL AND G6I_FILIAL = G3R_FILCON AND G6I_NUMID = G3R_NUMID AND G6I_IDITEM = G3R_IDITEM AND " + IIF(TCSrvType() == "AS/400", "G6I.@DELETED@ = ' '", "G6I.D_E_L_E_T_ = ' '")
cSts +=		" INNER JOIN " + RetSqlName("G4C") + " G4C ON G4C_FILIAL = G3R_FILIAL AND G4C_NUMID = G3R_NUMID AND G4C_IDITEM = G3R_IDITEM AND G4C_NUMSEQ = G3R_NUMSEQ"
cSts +=				 	" AND ((G4C_CODIGO = G3R_FORREP AND G4C_LOJA = G3R_LOJREP) OR (G4C_CODIGO = G3R_FORNEC AND G4C_LOJA = G3R_LOJA)) "
cSts += 				" AND (G4C_STATUS = '1' OR G4C_TPCONC = '3') AND G4C_CODAPU = ' ' AND G4C_CLIFOR = '2' AND " + IIF(TCSrvType() == "AS/400", "G4C.@DELETED@ = ' '", "G4C.D_E_L_E_T_ = ' '")
cSts += 	" WHERE G3R_CONCIL = '" + cConcil + "' AND G3R_FILCON = '" + cFilCon + "' AND " + IIF(TCSrvType() == "AS/400", "G3R.@DELETED@ = ' '", "G3R.D_E_L_E_T_ = ' '")
cSts +=		" GROUP BY G3R_FILIAL, G3R_NUMID, G3R_IDITEM, G3R_DOC "
	
Return TcSqlExec(cSts) > 0

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39TMPG3Q
Fun��o para adicionar informa��es na tabela tempor�ria referente aos totais do item de venda

@type Function
@author Anderson Toledo
@since 10/10/2017
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T39TMPG3Q(cFilCon, cConcil)

Local cSts := ""

(oTmpTotG3Q:GetAlias())->(__dbZap())
	
//Na f�rmula de campos de som�t�ria, todos os valores s�o somados pois os campos de abatimento j� est�o com valor negativo	
cSts := "INSERT INTO " + oTmpTotG3Q:GetRealName() + " (G3R_FILCON,G3R_CONCIL,G3R_MSFIL,G3Q_FILIAL,G3Q_NUMID,G3Q_IDITEM,G3Q_DOC,G3Q_TARIFA,G3Q_MARKUP,G3Q_PRCBAS,G3Q_TAXA,G3Q_EXTRA,G3Q_TAXADU,G3Q_VLRSER,G3Q_DESC,G3Q_PRECAG,G3Q_TAXAAG,G3Q_FEE,G3Q_REPASS)"
cSts += 	" SELECT G3R_FILCON,G3R_CONCIL,G3R_MSFIL,G3Q_FILIAL,G3Q_NUMID,G3Q_IDITEM,G3Q_DOC,G3Q_TARIFA,G3Q_MARKUP,(G3Q_TARIFA + G3Q_MARKUP) G3Q_PRCBAS,G3Q_TAXA,G3Q_EXTRA,G3Q_TAXADU,
cSts += 		  " (G3Q_TARIFA + G3Q_MARKUP + G3Q_TAXA + G3Q_EXTRA + G3Q_TAXADU) G3Q_VLRSER,G3Q_DESC,(G3Q_TARIFA + G3Q_MARKUP + G3Q_TAXA + G3Q_EXTRA + G3Q_TAXADU + G3Q_DESC) G3Q_PRECAG,G3Q_TAXAAG,G3Q_FEE,G3Q_REPASS"
cSts +=		" FROM (SELECT G3R_FILCON,G3R_CONCIL,G3R_MSFIL,G3Q_FILIAL,G3Q_NUMID,G3Q_IDITEM,G3Q_DOC,"
cSts += 				   T39SQLAC("G4C_TARIFA", ""          ) + " G3Q_TARIFA,"
cSts += 				   T39SQLAC("G4C_VALOR" , "G3Q_MARKUP") + " G3Q_MARKUP,"
cSts += 				   T39SQLAC("G4C_TAXA"	, ""          ) + " G3Q_TAXA,"
cSts += 				   T39SQLAC("G4C_EXTRA" , ""          ) + " G3Q_EXTRA,"
cSts += 				   T39SQLAC("G4C_VALOR" , "G3Q_TAXADU") + " G3Q_TAXADU,"
cSts += 				   T39SQLAC("G4C_VALOR" , "G3Q_DESC"  ) + " G3Q_DESC,"
cSts += 				   T39SQLAC("G4C_VALOR" , "G3Q_TAXAAG") + " G3Q_TAXAAG,"
cSts += 				   T39SQLAC("G4C_VALOR" , "G3Q_FEE"   ) + " G3Q_FEE,"
cSts += 				   T39SQLAC("G4C_VALOR" , "G3Q_REPASS") + " G3Q_REPASS"
cSts += 			" FROM " + RetSqlName("G3R") + " G3R"
cSts += 			" INNER JOIN " + RetSqlName("G6I") + " G6I ON G6I_FILIAL = G3R_FILCON AND G6I_NUMID = G3R_NUMID AND G6I_IDITEM = G3R_IDITEM AND G6I_CONCIL = G3R_CONCIL AND " + IIF(TCSrvType() == "AS/400", "G6I.@DELETED@ = ' '", "G6I.D_E_L_E_T_ = ' '")
cSts +=				" INNER JOIN " + RetSqlName("G3Q") + " G3Q ON G3Q_FILIAL = G3R_FILIAL AND G3Q_NUMID = G3R_NUMID AND G3Q_IDITEM = G3R_IDITEM AND G3Q_NUMSEQ = G3R_NUMSEQ AND " + IIF(TCSrvType() == "AS/400", "G3Q.@DELETED@ = ' '", "G3Q.D_E_L_E_T_ = ' '")
cSts +=				" INNER JOIN " + RetSqlName("G4C") + " G4C ON G4C_FILIAL = G3Q_FILIAL AND G4C_NUMID = G3Q_NUMID AND G4C_IDITEM = G3Q_IDITEM AND G4C_NUMSEQ = G3Q_NUMSEQ AND G4C_CLIFOR = '1' AND " + IIF(TCSrvType() == "AS/400", "G4C.@DELETED@ = ' '", "G4C.D_E_L_E_T_ = ' '")
cSts += 			" WHERE G3R_FILCON = '" + cFilCon + "' AND G3R_CONCIL = '" + cConcil + "' AND " + IIF(TCSrvType() == "AS/400", "G3R.@DELETED@ = ' '", "G3R.D_E_L_E_T_ = ' '")
cSts +=				" GROUP BY G3R_FILCON,G3R_CONCIL,G3R_MSFIL,G3Q_FILIAL,G3Q_NUMID,G3Q_IDITEM,G3Q_DOC "
cSts +=		") TRB"
	
Return TcSqlExec(cSts) > 0