#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'TURA051.CH'
 
Function TURA051A()
 
Local aArea     := GetArea()
Local aListaCpo := {}
Local aReg      := {}
Local aListReg  := {}
Local nCpoG6K   := 0
Local nPosReg   := 0
Local nX        := 0 
Local cAtrib    := '' 
Local cDE       := ''
 
aAdd(aListaCpo, 'G6K_ATRIB' ) 
aAdd(aListaCpo, 'G6K_DESCRI') 
aAdd(aListaCpo, 'G6K_CDDE'  ) 
aAdd(aListaCpo, 'G6K_DSDE'  ) 
aAdd(aListaCpo, 'G6K_CDPARA') 
aAdd(aListaCpo, 'G6K_DSPARA') 

DbSelectArea('SX3') 
DbSetOrder(2) 
For nCpoG6K := 1 To Len(aListaCpo) 
	If !DbSeek(aListaCpo[nCpoG6K]) 
		Help( , , 'TURA051CAMPO', , I18N(STR0007, {aListaCpo[nCpoG6K]}), 1, 0) // "Campo #1 n�o encontrado." 
		Return 
	EndIf 
Next nCpoG6K
 
Begin Transaction
 
 	// SEGMENTO DE NEG�CIO
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000001'})
	aAdd(aReg, {'G6K_DESCRI', STR0011 })			// "SEGMENTO NEG�CIO (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '1'     })
	aAdd(aReg, {'G6K_DSDE'  , STR0012 })			// "CORPORATIVO"
	aAdd(aReg, {'G6K_CDPARA', 'C'     })
	aAdd(aReg, {'G6K_DSPARA', STR0012 })			// "CORPORATIVO"
	aAdd(aListReg, aReg)
 
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000001'})
	aAdd(aReg, {'G6K_DESCRI', STR0011 })			// "SEGMENTO NEG�CIO (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '2'     })
	aAdd(aReg, {'G6K_DSDE'  , STR0013 })			// "EVENTO"
	aAdd(aReg, {'G6K_CDPARA', 'E'     })
	aAdd(aReg, {'G6K_DSPARA', STR0013 })			// "EVENTO"
	aAdd(aListReg, aReg)
 
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000001'})
	aAdd(aReg, {'G6K_DESCRI', STR0011 })			// "SEGMENTO NEG�CIO (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '3'     })
	aAdd(aReg, {'G6K_DSDE'  , STR0014 })			// "LAZER"
	aAdd(aReg, {'G6K_CDPARA', 'L'     })
	aAdd(aReg, {'G6K_DSPARA', STR0014 })			// "LAZER"
	aAdd(aListReg, aReg)
 
 	// SEGMENTO DE PRODUTO
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000002'})
	aAdd(aReg, {'G6K_DESCRI', STR0015 })			// "SEGMENTO DE PRODUTO (PROTHEUS -> SABRE)"          
	aAdd(aReg, {'G6K_CDDE'  , '1'     })
	aAdd(aReg, {'G6K_DSDE'  , STR0016 })			// "A�REO"
	aAdd(aReg, {'G6K_CDPARA', 'AIR'   })
	aAdd(aReg, {'G6K_DSPARA', STR0016 })			// "A�REO"
	aAdd(aListReg, aReg)
 
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000002'})
	aAdd(aReg, {'G6K_DESCRI', STR0015 })			// "SEGMENTO DE PRODUTO (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '2'     })
	aAdd(aReg, {'G6K_DSDE'  , STR0018 })			// "HOTEL"
	aAdd(aReg, {'G6K_CDPARA', 'HHT'   })
	aAdd(aReg, {'G6K_DSPARA', STR0018 })			// "HOTEL"
	aAdd(aListReg, aReg)
 
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000002'})
	aAdd(aReg, {'G6K_DESCRI', STR0015 })			// "SEGMENTO DE PRODUTO (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '2'     })
	aAdd(aReg, {'G6K_DSDE'  , STR0018 })			// "HOTEL"
	aAdd(aReg, {'G6K_CDPARA', 'HTL'   })
	aAdd(aReg, {'G6K_DSPARA', STR0018 })			// "HOTEL"
	aAdd(aListReg, aReg)
 
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000002'})
	aAdd(aReg, {'G6K_DESCRI', STR0015 })			// "SEGMENTO DE PRODUTO (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '2'     })
	aAdd(aReg, {'G6K_DSDE'  , STR0018 })			// "HOTEL"
	aAdd(aReg, {'G6K_CDPARA', 'HHL'   })
	aAdd(aReg, {'G6K_DSPARA', STR0018 })			// "HOTEL"
	aAdd(aListReg, aReg)
 
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000002'})
	aAdd(aReg, {'G6K_DESCRI', STR0015 })			// "SEGMENTO DE PRODUTO (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '3'     })
	aAdd(aReg, {'G6K_DSDE'  , STR0019 })			// "CARRO"
	aAdd(aReg, {'G6K_CDPARA', 'CAR'   })
	aAdd(aReg, {'G6K_DSPARA', STR0019 })			// "CARRO"
	aAdd(aListReg, aReg)
 
	// C�DIGO DO PRODUTO
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000003'})
	aAdd(aReg, {'G6K_DESCRI', STR0020 })			// "C�DIGO DO PRODUTO (SABRE -> PROTHEUS)"
	aAdd(aReg, {'G6K_CDDE'  , 'AIR'   })
	aAdd(aReg, {'G6K_DSDE'  , STR0016 })			// "A�REO"
	aAdd(aReg, {'G6K_CDPARA', ''      })
	aAdd(aReg, {'G6K_DSPARA', STR0016 })			// "A�REO"
	aAdd(aListReg, aReg)

	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000003'})
	aAdd(aReg, {'G6K_DESCRI', STR0020 })			// "C�DIGO DO PRODUTO (SABRE -> PROTHEUS)"
	aAdd(aReg, {'G6K_CDDE'  , 'HHT'   })
	aAdd(aReg, {'G6K_DSDE'  , STR0018 })			// "HOTEL"
	aAdd(aReg, {'G6K_CDPARA', ''      })
	aAdd(aReg, {'G6K_DSPARA', STR0018 })			// "HOTEL"
	aAdd(aListReg, aReg)

	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000003'})
	aAdd(aReg, {'G6K_DESCRI', STR0020 })			// "C�DIGO DO PRODUTO (SABRE -> PROTHEUS)"
	aAdd(aReg, {'G6K_CDDE'  , 'HTL'   })
	aAdd(aReg, {'G6K_DSDE'  , STR0018 })			// "HOTEL"
	aAdd(aReg, {'G6K_CDPARA', ''      })
	aAdd(aReg, {'G6K_DSPARA', STR0018 })			// "HOTEL"
	aAdd(aListReg, aReg)

	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000003'})
	aAdd(aReg, {'G6K_DESCRI', STR0020 })			// "C�DIGO DO PRODUTO (SABRE -> PROTHEUS)"
	aAdd(aReg, {'G6K_CDDE'  , 'HHL'   })
	aAdd(aReg, {'G6K_DSDE'  , STR0018 })			// "HOTEL"
	aAdd(aReg, {'G6K_CDPARA', ''      })
	aAdd(aReg, {'G6K_DSPARA', STR0018 })			// "HOTEL"
	aAdd(aListReg, aReg)

	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000003'})
	aAdd(aReg, {'G6K_DESCRI', STR0020 })			// "C�DIGO DO PRODUTO (SABRE -> PROTHEUS)"
	aAdd(aReg, {'G6K_CDDE'  , 'CAR'   })
	aAdd(aReg, {'G6K_DSDE'  , STR0019 })			// "CARRO"
	aAdd(aReg, {'G6K_CDPARA', ''      })
	aAdd(aReg, {'G6K_DSPARA', STR0019 })			// "CARRO"
	aAdd(aListReg, aReg)
	
	// TIPO DE PASSAGEIRO
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000004'})
	aAdd(aReg, {'G6K_DESCRI', STR0017 })			// "TIPO DE PASSAGEIRO (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '1'     })
	aAdd(aReg, {'G6K_DSDE'  , STR0021 })			// "ADULTO"
	aAdd(aReg, {'G6K_CDPARA', 'ADT'   })
	aAdd(aReg, {'G6K_DSPARA', STR0021 })			// "ADULTO"
	aAdd(aListReg, aReg)
 
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000004'})
	aAdd(aReg, {'G6K_DESCRI', STR0017 })			// "TIPO DE PASSAGEIRO (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '2'     })
	aAdd(aReg, {'G6K_DSDE'  , STR0022 })			// "CRIANCA"
	aAdd(aReg, {'G6K_CDPARA', 'CHD'   })
	aAdd(aReg, {'G6K_DSPARA', STR0022 })			// "CRIANCA"
	aAdd(aListReg, aReg)
 
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000004'})
	aAdd(aReg, {'G6K_DESCRI', STR0017 })			// "TIPO DE PASSAGEIRO (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '3'     })
	aAdd(aReg, {'G6K_DSDE'  , STR0023 })			// "INFANTIL / BEBE"
	aAdd(aReg, {'G6K_CDPARA', 'INF'   })
	aAdd(aReg, {'G6K_DSPARA', STR0023 })			// "INFANTIL / BEBE"
	aAdd(aListReg, aReg)
 
 	aReg := {}
 	aAdd(aReg, {'G6K_ATRIB' , '000004'})
	aAdd(aReg, {'G6K_DESCRI', STR0017 })			// "TIPO DE PASSAGEIRO (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '4'     })
	aAdd(aReg, {'G6K_DSDE'  , STR0024 })			// "JUNIOR"
	aAdd(aReg, {'G6K_CDPARA', 'JUN'   })
	aAdd(aReg, {'G6K_DSPARA', STR0024 })			// "JUNIOR"
	aAdd(aListReg, aReg)

	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000004'})
	aAdd(aReg, {'G6K_DESCRI', STR0017 })			// "TIPO DE PASSAGEIRO (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '5'     })
	aAdd(aReg, {'G6K_DSDE'  , STR0025 })			// "MILITAR"
	aAdd(aReg, {'G6K_CDPARA', 'MIL'   })
	aAdd(aReg, {'G6K_DSPARA', STR0025 })			// "MILITAR"
	aAdd(aListReg, aReg)
 
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000004'})
	aAdd(aReg, {'G6K_DESCRI', STR0017 })			// "TIPO DE PASSAGEIRO (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '6'     })
	aAdd(aReg, {'G6K_DSDE'  , STR0026 })			// "SENIOR"
	aAdd(aReg, {'G6K_CDPARA', 'SEN'   })
	aAdd(aReg, {'G6K_DSPARA', STR0026 })			// "SENIOR"
	aAdd(aListReg, aReg)
 
 	// FORMA DE TRATAMENTO
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000005'})
	aAdd(aReg, {'G6K_DESCRI', STR0027 })			// "FORMA DE TRATAMENO (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '1'     })
	aAdd(aReg, {'G6K_DSDE'  , 'MR'    })
	aAdd(aReg, {'G6K_CDPARA', 'MR'    })
	aAdd(aReg, {'G6K_DSPARA', 'MR'    })
	aAdd(aListReg, aReg)
 
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000005'})
	aAdd(aReg, {'G6K_DESCRI', STR0027 })			// "FORMA DE TRATAMENO (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '2'     })
	aAdd(aReg, {'G6K_DSDE'  , 'MRS'   })
	aAdd(aReg, {'G6K_CDPARA', 'MRS'   })
	aAdd(aReg, {'G6K_DSPARA', 'MRS'   })
	aAdd(aListReg, aReg)
 
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000005'})
	aAdd(aReg, {'G6K_DESCRI', STR0027 })			// "FORMA DE TRATAMENO (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '3'     })
	aAdd(aReg, {'G6K_DSDE'  , 'MISS'  })
	aAdd(aReg, {'G6K_CDPARA', 'MISS'  })
	aAdd(aReg, {'G6K_DSPARA', 'MISS'  })
	aAdd(aListReg, aReg)
 
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000005'})
	aAdd(aReg, {'G6K_DESCRI', STR0027 })			// "FORMA DE TRATAMENO (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '4'     })
	aAdd(aReg, {'G6K_DSDE'  , 'MSTR'  })
	aAdd(aReg, {'G6K_CDPARA', 'MSTR'  })
	aAdd(aReg, {'G6K_DSPARA', 'MSTR'  })
	aAdd(aListReg, aReg)

 	// BANDEIRA DO CART�O
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000006'})
	aAdd(aReg, {'G6K_DESCRI', STR0028 })			// "BANDEIRA DO CART�O (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '04'    })
	aAdd(aReg, {'G6K_DSDE'  , 'VISA'  })
	aAdd(aReg, {'G6K_CDPARA', 'VI'    })
	aAdd(aReg, {'G6K_DSPARA', 'VISA'  })
	aAdd(aListReg, aReg)

	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000006'})
	aAdd(aReg, {'G6K_DESCRI', STR0028 })			// "BANDEIRA DO CART�O (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '06'    })
	aAdd(aReg, {'G6K_DSDE'  , 'AMEX'  })
	aAdd(aReg, {'G6K_CDPARA', 'AX'    })
	aAdd(aReg, {'G6K_DSPARA', 'AMEX'  })
	aAdd(aListReg, aReg)

	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000006'})
	aAdd(aReg, {'G6K_DESCRI', STR0028 })			// "BANDEIRA DO CART�O (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '07'    })
	aAdd(aReg, {'G6K_DSDE'  , 'SOLLO' })
	aAdd(aReg, {'G6K_CDPARA', 'SO'    })
	aAdd(aReg, {'G6K_DSPARA', 'SOLLO' })
	aAdd(aListReg, aReg)

	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000006' })
	aAdd(aReg, {'G6K_DESCRI', STR0028  })			// "BANDEIRA DO CART�O (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '08'     })
	aAdd(aReg, {'G6K_DSDE'  , 'DINNERS'})
	aAdd(aReg, {'G6K_CDPARA', 'DI'     })
	aAdd(aReg, {'G6K_DSPARA', 'DINNERS'})
	aAdd(aListReg, aReg)

	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000006'    })
	aAdd(aReg, {'G6K_DESCRI', STR0028     })		// "BANDEIRA DO CART�O (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '51'        })
	aAdd(aReg, {'G6K_DSDE'  , 'HIPPERCARD'})
	aAdd(aReg, {'G6K_CDPARA', 'HC'        })
	aAdd(aReg, {'G6K_DSPARA', 'HIPPERCARD'})
	aAdd(aListReg, aReg)

	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000006'    })
	aAdd(aReg, {'G6K_DESCRI', STR0028     })		// "BANDEIRA DO CART�O (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '56'        })
	aAdd(aReg, {'G6K_DSDE'  , 'MASTERCARD'})
	aAdd(aReg, {'G6K_CDPARA', 'CA'        })
	aAdd(aReg, {'G6K_DSPARA', 'MASTERCARD'})
	aAdd(aListReg, aReg)

 	// GRUPO DE PRODUTO
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000007'})
	aAdd(aReg, {'G6K_DESCRI', STR0029 })			// "GRUPO DE PRODUTO (SABRE -> PROTHEUS)"
	aAdd(aReg, {'G6K_CDDE'  , 'AIR'   })
	aAdd(aReg, {'G6K_DSDE'  , STR0016 })			// "A�REO"
	aAdd(aReg, {'G6K_CDPARA', ''      })
	aAdd(aReg, {'G6K_DSPARA', STR0016 })			// "A�REO"
	aAdd(aListReg, aReg)

	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000007'})
	aAdd(aReg, {'G6K_DESCRI', STR0029 })			// "GRUPO DE PRODUTO (SABRE -> PROTHEUS)"
	aAdd(aReg, {'G6K_CDDE'  , 'HHT'   })
	aAdd(aReg, {'G6K_DSDE'  , STR0018 })			// "HOTEL"
	aAdd(aReg, {'G6K_CDPARA', ''      })
	aAdd(aReg, {'G6K_DSPARA', STR0018 })			// "HOTEL"
	aAdd(aListReg, aReg)

	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000007'})
	aAdd(aReg, {'G6K_DESCRI', STR0029 })			// "GRUPO DE PRODUTO (SABRE -> PROTHEUS)"
	aAdd(aReg, {'G6K_CDDE'  , 'HTL'   })
	aAdd(aReg, {'G6K_DSDE'  , STR0018 })			// "HOTEL"
	aAdd(aReg, {'G6K_CDPARA', ''      })
	aAdd(aReg, {'G6K_DSPARA', STR0018 })			// "HOTEL"
	aAdd(aListReg, aReg)

	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000007'})
	aAdd(aReg, {'G6K_DESCRI', STR0029 })			// "GRUPO DE PRODUTO (SABRE -> PROTHEUS)"
	aAdd(aReg, {'G6K_CDDE'  , 'HHL'   })
	aAdd(aReg, {'G6K_DSDE'  , STR0018 })			// "HOTEL"
	aAdd(aReg, {'G6K_CDPARA', ''      })
	aAdd(aReg, {'G6K_DSPARA', STR0018 })			// "HOTEL"
	aAdd(aListReg, aReg)

	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000007'})
	aAdd(aReg, {'G6K_DESCRI', STR0029 })			// "GRUPO DE PRODUTO (SABRE -> PROTHEUS)"
	aAdd(aReg, {'G6K_CDDE'  , 'CAR'   })
	aAdd(aReg, {'G6K_DSDE'  , STR0019 })			// "CARRO"
	aAdd(aReg, {'G6K_CDPARA', ''      })
	aAdd(aReg, {'G6K_DSPARA', STR0019 })			// "CARRO"
	aAdd(aListReg, aReg)

	// TIPO DE EMISSAO
	aReg := {}
	aAdd(aReg, {'G6K_ATRIB' , '000008' })
	aAdd(aReg, {'G6K_DESCRI', STR0031  })			// "TIPO DE EMISS�O (PROTHEUS -> SABRE)"
	aAdd(aReg, {'G6K_CDDE'  , '01'     })
	aAdd(aReg, {'G6K_DSDE'  , 'OFFILNE'})			
	aAdd(aReg, {'G6K_CDPARA', '01'     })
	aAdd(aReg, {'G6K_DSPARA', 'OFFLINE'})			
	aAdd(aListReg, aReg)

	// ENTIDADE ADICIONAL 
	DbSelectArea('G3E') 
	G3E->(DbSetOrder(1))			// G3E_FILIAL+G3E_CODIGO 
 	While G3E->(!EOF())
		aReg := {}
		aAdd(aReg, {'G6K_ATRIB' , '000009'       })
		aAdd(aReg, {'G6K_DESCRI', STR0030        })			// "ENTIDADE ADICIONAL (PROTHEUS -> SABRE)"
		aAdd(aReg, {'G6K_CDDE'  , G3E->G3E_CODIGO})
		aAdd(aReg, {'G6K_DSDE'  , G3E->G3E_DESCR })			
		aAdd(aReg, {'G6K_CDPARA', ''             })
		aAdd(aReg, {'G6K_DSPARA', G3E->G3E_DESCR })			
		aAdd(aListReg, aReg)
		G3E->(DbSkip())
 	EndDo
	G3E->(DbCloseArea())  
 
	DbSelectArea('G6K') 
	G6K->(DbSetOrder(1))			// G6K_FILIAL+G6K_ATRIB+G6K_CDDE+G6K_CDPARA 
	For nX := 1 To Len(aListReg)
		cAtrib := PadR(aListReg[nX][aScan(aListReg[nX], {|x| x[1] == 'G6K_ATRIB'})][2] + Space(TamSX3('G6K_ATRIB')[1]), TamSX3('G6K_ATRIB')[1])
		cDE    := PadR(aListReg[nX][aScan(aListReg[nX], {|x| x[1] == 'G6K_CDDE'})][2] + Space(TamSX3('G6K_CDDE')[1]), TamSX3('G6K_CDDE')[1])

		If !DbSeek(xFilial('G6K') + cAtrib + cDE) 
			RecLock('G6K', .T.) 
			G6K->G6K_FILIAL := xFilial('G6K')  
			For nPosReg := 1 To Len(aListReg[nX])  
		   		If (nFieldPos := FieldPos(aListReg[nX][nPosReg][1])) <> 0 
		      		If aListReg[nX][nPosReg][1] == 'G6K_OBS' 
		         		FieldPut(nFieldPos, StrTran(aListReg[nX][nPosReg][2], '/\', CRLF) ) 
		      		Else 
		         		FieldPut(nFieldPos, aListReg[nX][nPosReg][2] ) 
		      		EndIf 
		   		EndIf
		   	Next nPosReg
		   	G6K->(MsUnLock())   
		EndIf	 
	Next nX
	G6K->(DbCloseArea())  
 
End Transaction

RestArea(aArea)

Return
