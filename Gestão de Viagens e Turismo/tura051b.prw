#Include 'Protheus.ch'
 
Function TURA051B()
 
Local aReg      := {} 
Local aListaCpo := {} 
Local _nCpoG6K  := 0 
Local _nPosReg  := 0 
 
DBSelectArea('G6K') 
DBSetOrder(1) 
 
AAdd(aListaCpo, 'G6K_ATRIB') 
AAdd(aListaCpo, 'G6K_DESCRI') 
AAdd(aListaCpo, 'G6K_CDDE') 
AAdd(aListaCpo, 'G6K_DSDE') 
AAdd(aListaCpo, 'G6K_CDPARA') 
AAdd(aListaCpo, 'G6K_DSPARA') 
 
For _nCpoG6K := 1 To Len(aListaCpo) 
 
DBSelectArea('SX3') 
DBSetOrder(2) 
 
If !DBSeek(aListaCpo[_nCpoG6K]) 
Help(,,'TURA051CAMPO',, 'Campo ' + aListaCpo[_nCpoG6K] + ' nao encontrado.' ,1,0) 
Return 
EndIf 
 
Next
 
Begin Transaction 
 
 
aReg := {}
 
AAdd(aReg, {'G6K_ATRIB', '000013'  })
AAdd(aReg, {'G6K_DESCRI', 'FATURA CART�O CTA - TIPO DO ITEM'  })
AAdd(aReg, {'G6K_CDDE', '1'  })
AAdd(aReg, {'G6K_DSDE', 'DEBITO'  })
AAdd(aReg, {'G6K_CDPARA', 'D'  })
AAdd(aReg, {'G6K_DSPARA', 'DEBITO'  })
 
DBSelectArea('G6K') 
DBSetOrder(1) 
If !DBSeek(xFilial('G6K') + '000013' + '1              ' + 'D              '   ) 
RecLock('G6K', .T.) 
 
G6K->G6K_FILIAL := xFilial('G6K')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G6K_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G6K->(MSUnLock())  
 
EndIf 
 
 
aReg := {}
 
AAdd(aReg, {'G6K_ATRIB', '000013'  })
AAdd(aReg, {'G6K_DESCRI', 'FATURA CART�O CTA - TIPO DO ITEM'  })
AAdd(aReg, {'G6K_CDDE', '2'  })
AAdd(aReg, {'G6K_DSDE', 'CREDITO'  })
AAdd(aReg, {'G6K_CDPARA', 'C'  })
AAdd(aReg, {'G6K_DSPARA', 'CREDITO'  })
 
DBSelectArea('G6K') 
DBSetOrder(1) 
If !DBSeek(xFilial('G6K') + '000013' + '2              ' + 'C              '   ) 
RecLock('G6K', .T.) 
 
G6K->G6K_FILIAL := xFilial('G6K')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G6K_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G6K->(MSUnLock())  
 
EndIf 
 
End Transaction 
Return 
