#Include 'Protheus.ch'
 
Function TURA057A()
 
Local aReg      := {} 
Local aListaCpo := {} 
Local _nCpoG8U  := 0 
Local _nPosReg  := 0 
 
DBSelectArea('G8U') 
DBSetOrder(1) 
 
If DBSeek(xFilial('G8U') + '1') 
 
Return 
 
EndIf 
 
AAdd(aListaCpo, 'G8U_GDS') 
AAdd(aListaCpo, 'G8U_VERSAO') 
AAdd(aListaCpo, 'G8U_SEQUEN') 
AAdd(aListaCpo, 'G8U_ATIVO') 
AAdd(aListaCpo, 'G8U_DADO') 
AAdd(aListaCpo, 'G8U_TABELA') 
AAdd(aListaCpo, 'G8U_CAMPO') 
AAdd(aListaCpo, 'G8U_VARIAV') 
AAdd(aListaCpo, 'G8U_CONTEX') 
AAdd(aListaCpo, 'G8U_BLOCO') 
AAdd(aListaCpo, 'G8U_TRECHO') 
AAdd(aListaCpo, 'G8U_INICIO') 
AAdd(aListaCpo, 'G8U_TAMANH') 
AAdd(aListaCpo, 'G8U_TAG') 
AAdd(aListaCpo, 'G8U_TAGMUL') 
AAdd(aListaCpo, 'G8U_PASSAG') 
AAdd(aListaCpo, 'G8U_SEPARA') 
AAdd(aListaCpo, 'G8U_POSICA') 
AAdd(aListaCpo, 'G8U_FREAD') 
AAdd(aListaCpo, 'G8U_SEEK') 
AAdd(aListaCpo, 'G8U_ALIAS') 
AAdd(aListaCpo, 'G8U_INDICE') 
AAdd(aListaCpo, 'G8U_CHAVE') 
AAdd(aListaCpo, 'G8U_RETURN') 
AAdd(aListaCpo, 'G8U_FPOS') 
AAdd(aListaCpo, 'G8U_TIPO') 
AAdd(aListaCpo, 'G8U_SUBINI') 
AAdd(aListaCpo, 'G8U_SUBTAM') 
AAdd(aListaCpo, 'G8U_FFORM') 
AAdd(aListaCpo, 'G8U_WHEN') 
AAdd(aListaCpo, 'G8U_OBS') 
AAdd(aListaCpo, 'G8U_MACFIX') 
AAdd(aListaCpo, 'G8U_INCREM') 
AAdd(aListaCpo, 'G8U_OBRIGA') 
 
For _nCpoG8U := 1 To Len(aListaCpo) 
 
DBSelectArea('SX3') 
DBSetOrder(2) 
 
If !DBSeek(aListaCpo[_nCpoG8U]) 
Help(,,'TURA057CAMPO',, 'Campo ' + aListaCpo[_nCpoG8U] + ' nao encontrado.' ,1,0) 
Return 
EndIf 
 
Next
 
Begin Transaction 
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '000'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_VARIAV', 'CIU0TYP'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_BLOCO', 'M0'  })
AAdd(aReg, {'G8U_TRECHO', 'IU0TYP'  })
AAdd(aReg, {'G8U_INICIO', 14  })
AAdd(aReg, {'G8U_TAMANH', 1  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(CVARSAB000 == '5', '1', '2')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_OBS', 'N�o se deve excluir a declara��o desta vari�vel, nem alterar o seu nome. Caso o crit�rio para defini��o de que o PNR se trate de cancelamento seja verdadeiro, neste exemplo, (5) atribui-se o valor 1-Sim, do contr�rio 2-N�o. N�o alterar a sequencia 000 deste item.'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '001'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_VARIAV', 'CVERSION'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_BLOCO', 'M0'  })
AAdd(aReg, {'G8U_TRECHO', 'IU0VER'  })
AAdd(aReg, {'G8U_INICIO', 15  })
AAdd(aReg, {'G8U_TAMANH', 2  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '002'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_VARIAV', 'CLOCALIZA'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_BLOCO', 'M0'  })
AAdd(aReg, {'G8U_INICIO', 27  })
AAdd(aReg, {'G8U_TAMANH', 14  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_OBS', 'N�o alterar a sequencia 000 deste item.'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '003'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_VARIAV', 'CCODCLI'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'CL.'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_FFORM", "PADR(CVARSAB003, TAMSX3('G3P_CLIENT')[1], ' ')"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '1'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '004'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_VARIAV', 'CCODLOJA'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'LJ.'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_FFORM", "PADR(CVARSAB004, TAMSX3('G3P_LOJA')[1], ' ')"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '1'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '005'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_VARIAV', 'CSEGNEG'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'SN.'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '1'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '006'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_VARIAV', 'CFILIALRV'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_BLOCO', 'M0'  })
AAdd(aReg, {'G8U_TRECHO', 'IU0IS4'  })
AAdd(aReg, {'G8U_INICIO', 127  })
AAdd(aReg, {'G8U_TAMANH', 5  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G3M'  })
AAdd(aReg, {'G8U_INDICE', 5  })
AAdd(aReg, {'G8U_CHAVE', 'CVARSAB006'  })
AAdd(aReg, {'G8U_RETURN', 'G3M_FILIAL'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '007'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_VARIAV', 'DDTEMISS'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_BLOCO', 'M0'  })
AAdd(aReg, {'G8U_TRECHO', 'IU0PDT'  })
AAdd(aReg, {'G8U_INICIO', 117  })
AAdd(aReg, {'G8U_TAMANH', 5  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'TA048DTEMIS(ALLTRIM(CVARSAB007))'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '3'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '008'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_VARIAV', 'CCODSABRE'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G8O'  })
AAdd(aReg, {'G8U_INDICE', 2  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G8O') + 'SABRE'"  })
AAdd(aReg, {'G8U_RETURN', 'G8O_CODIGO'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '009'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_VARIAV', 'CBASTAR'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_BLOCO', 'M4'  })
AAdd(aReg, {'G8U_TRECHO', 'IU4FBS'  })
AAdd(aReg, {'G8U_INICIO', 25  })
AAdd(aReg, {'G8U_TAMANH', 13  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 1  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '009'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_VARIAV', 'CIDTAX1'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_BLOCO', 'M2'  })
AAdd(aReg, {'G8U_TRECHO', 'IU2ID1'  })
AAdd(aReg, {'G8U_INICIO', 54  })
AAdd(aReg, {'G8U_TAMANH', 2  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '00A'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_VARIAV', 'NVLRTAX1'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_BLOCO', 'M2'  })
AAdd(aReg, {'G8U_TRECHO', 'IU2TX1'  })
AAdd(aReg, {'G8U_INICIO', 47  })
AAdd(aReg, {'G8U_TAMANH', 7  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '00B'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_VARIAV', 'CIDTAX2'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_BLOCO', 'M2'  })
AAdd(aReg, {'G8U_TRECHO', 'IU2ID2'  })
AAdd(aReg, {'G8U_INICIO', 64  })
AAdd(aReg, {'G8U_TAMANH', 2  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '00C'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_VARIAV', 'NVLRTAX2'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_BLOCO', 'M2'  })
AAdd(aReg, {'G8U_TRECHO', 'IU2TX2'  })
AAdd(aReg, {'G8U_INICIO', 57  })
AAdd(aReg, {'G8U_TAMANH', 7  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '00D'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_VARIAV', 'CIDTAX3'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_BLOCO', 'M2'  })
AAdd(aReg, {'G8U_TRECHO', 'IU2ID3'  })
AAdd(aReg, {'G8U_INICIO', 74  })
AAdd(aReg, {'G8U_TAMANH', 2  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '00E'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_VARIAV', 'NVLRTAX3'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_BLOCO', 'M2'  })
AAdd(aReg, {'G8U_TRECHO', 'IU2TX3'  })
AAdd(aReg, {'G8U_INICIO', 67  })
AAdd(aReg, {'G8U_TAMANH', 7  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '010'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3P'  })
AAdd(aReg, {'G8U_CAMPO', 'G3P_FILIAL'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'CFILIALRV'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '011'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3P'  })
AAdd(aReg, {'G8U_CAMPO', 'G3P_FILIAL'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "XFILIAL('G3P')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_WHEN', 'EMPTY(ALLTRIM(CVARSAB010))'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '012'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3P'  })
AAdd(aReg, {'G8U_CAMPO', 'G3P_CLIENT'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'CCODCLI'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '013'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3P'  })
AAdd(aReg, {'G8U_CAMPO', 'G3P_LOJA'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'CCODLOJA'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '014'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3P'  })
AAdd(aReg, {'G8U_CAMPO', 'G3P_CLICMP'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "TURXFNDG4L(CCODCLI, CCODLOJA, CSEGNEG,, '1')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '015'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3P'  })
AAdd(aReg, {'G8U_CAMPO', 'G3P_SEGNEG'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G6K'  })
AAdd(aReg, {'G8U_INDICE', 2  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G6K') + '000001' + CSEGNEG"  })
AAdd(aReg, {'G8U_RETURN', 'G6K_CDDE'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '015'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3P'  })
AAdd(aReg, {'G8U_CAMPO', 'G3P_ANO'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'STR(YEAR(DDTEMISS))'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '016'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3P'  })
AAdd(aReg, {'G8U_CAMPO', 'G3P_EMISS'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'DDTEMISS'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '3'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '017'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3P'  })
AAdd(aReg, {'G8U_CAMPO', 'G3P_DESTIN'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'TA048GETDST(APNR, AG8U, NIL)'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '018'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3P'  })
AAdd(aReg, {'G8U_CAMPO', 'G3P_DESCRI'  })
AAdd(aReg, {'G8U_CONTEX', '1'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "'INSERIDO VIA IMPORTACAO DO ARQUIVO ' + ALLTRIM(CARQUIVO) + ' EM ' + DTOC(DATE()) + ' AS ' + TIME() + 'H     ' + XFILIAL('SA2')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '020'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_IDITEM'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '1'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '021'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_OPERAC'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) == 'AIR', TA048OPERAC(ALISTPAX[1])[1], '1')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '022'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_TPDOC'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) == 'AIR', TA048OPERAC(ALISTPAX[1])[3], '1')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '023'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_DOC'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M5'  })
AAdd(aReg, {'G8U_TRECHO', 'IU5VR1'  })
AAdd(aReg, {'G8U_INICIO', 12  })
AAdd(aReg, {'G8U_TAMANH', 10  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '024'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_DOCORI'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) == 'AIR', TA048OPERAC(ALISTPAX[1])[2], '')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_FFORM', 'IIF(!EMPTY(CVARSAB024), CVARSAB024, CDOCORI)'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '025'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_CONJUG'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M2'  })
AAdd(aReg, {'G8U_TRECHO', 'IU2DNO'  })
AAdd(aReg, {'G8U_INICIO', 244  })
AAdd(aReg, {'G8U_TAMANH', 1  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '1'  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) == 'AIR', TA048QTSEG(APNR, AG8U, 'AIR', CVARSAB025)[2], 0)"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '026'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_EMISS'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'DDTEMISS'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '3'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '027'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_TPEMIS'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G6K'  })
AAdd(aReg, {'G8U_INDICE', 1  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G6K') + '000008' + '01'"  })
AAdd(aReg, {'G8U_RETURN', 'G6K_CDPARA'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '028'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_CLIENT'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'CCODCLI'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '029'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_LOJA'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'CCODLOJA'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '02A'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_PROD'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3PC2'  })
AAdd(aReg, {'G8U_INICIO', 15  })
AAdd(aReg, {'G8U_TAMANH', 3  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G6K'  })
AAdd(aReg, {'G8U_INDICE', 1  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G6K') + '000003' + CVARSAB02A"  })
AAdd(aReg, {'G8U_RETURN', 'G6K_CDPARA'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '02B'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_FORMPG'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'FOP.'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '1'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '02C'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_FORMPG'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'FOP.'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) $ 'HHT|HTL|HHL'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '1'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '02D'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_FORMPG'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'FOP.'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'CAR'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '1'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '02E'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_MOEDCL'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G5T'  })
AAdd(aReg, {'G8U_INDICE', 4  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G5T') + 'BRL'"  })
AAdd(aReg, {'G8U_RETURN', 'G5T_CODIGO'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '02F'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_ACORDO'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', '1'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '02G'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_SOLIC'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'SL.'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'TA048SOLIC(CVARSAB02G, CCODCLI, CCODLOJA)'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_WHEN', '!EMPTY(CVARSAB02G)'  })
AAdd(aReg, {'G8U_OBS', 'Trecho inicialmente configurado para utilizar:/\Bloco M9/\TAG SL./\Porem esta tag estava informando o NOME do solicitante./\Foi criada a tag SOL. para informar o c�digo SU5 do solicitante.'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '02H'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_NOMESO'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(!EMPTY(CVARSAB02G), ALLTRIM(POSICIONE('SU5', 1, XFILIAL('SU5') + CVARSAB02G, 'U5_CONTAT')), '')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_WHEN', '!EMPTY(CVARSAB02G)'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '02I'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_DTSOLI'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'DS.'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '3'  })
AAdd(aReg, {"G8U_FFORM", "CTOD(SUBSTR(CVARSAB02H, 7, 2) + '/' + SUBSTR(CVARSAB02H, 5, 2) + '/' + SUBSTR(CVARSAB02H, 1, 4))"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '02J'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_ORDER'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'OS.'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '02L'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_ITOS'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'OSI.'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '02M'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3Q'  })
AAdd(aReg, {'G8U_CAMPO', 'G3Q_TPSEG'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3PC2'  })
AAdd(aReg, {'G8U_INICIO', 15  })
AAdd(aReg, {'G8U_TAMANH', 3  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G6K'  })
AAdd(aReg, {'G8U_INDICE', 2  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G6K') + '000002' + CVARSAB02M"  })
AAdd(aReg, {'G8U_RETURN', 'G6K_CDDE'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '030'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_EMISS'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'DDTEMISS'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '3'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '031'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_FORNEC'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3CAR'  })
AAdd(aReg, {'G8U_INICIO', 59  })
AAdd(aReg, {'G8U_TAMANH', 2  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G4S'  })
AAdd(aReg, {'G8U_INDICE', 3  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G4S') + CVARSAB031"  })
AAdd(aReg, {'G8U_RETURN', 'G4S_FORNEC'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_OBS', 'No campo CHAVE foi informado:/\/\XFILIAL("G8L") + "000001" + CVARSAB046/\/\A segunda posi��o "000001" refere-se ao codigo GDS do Sabe cadastrado na tabela G8O.'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '032'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_LOJA'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G4S'  })
AAdd(aReg, {'G8U_INDICE', 3  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G4S') + CVARSAB031"  })
AAdd(aReg, {'G8U_RETURN', 'G4S_LOJA'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_OBS', 'No campo CHAVE foi informado:/\/\XFILIAL("G8L") + "000001" + CVARSAB046/\/\A segunda posi��o "000001" refere-se ao codigo GDS do Sabe cadastrado na tabela G8O.'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '033'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_FORNEC'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3TDG'  })
AAdd(aReg, {'G8U_INICIO', 55  })
AAdd(aReg, {'G8U_TAMANH', 6  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G8L'  })
AAdd(aReg, {'G8U_INDICE', 2  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G8L') + CCODSABRE + ALLTRIM(CVARSAB033)"  })
AAdd(aReg, {'G8U_RETURN', 'G8L_FORNEC'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'HHL'"  })
AAdd(aReg, {'G8U_OBS', 'No campo CHAVE foi informado:/\/\XFILIAL("G8L") + "000001" + CVARSAB047/\/\A segunda posi��o "000001" refere-se ao codigo GDS do Sabe cadastrado na tabela G8O.'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '034'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_LOJA'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G8L'  })
AAdd(aReg, {'G8U_INDICE', 2  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G8L') + CCODSABRE + ALLTRIM(CVARSAB033)"  })
AAdd(aReg, {'G8U_RETURN', 'G8L_LOJA'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'HHL'"  })
AAdd(aReg, {'G8U_OBS', 'No campo CHAVE foi informado:/\/\XFILIAL("G8L") + "000001" + CVARSAB047/\/\A segunda posi��o "000001" refere-se ao codigo GDS do Sabe cadastrado na tabela G8O.'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '035'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_FORNEC'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3ITT'  })
AAdd(aReg, {'G8U_INICIO', 61  })
AAdd(aReg, {'G8U_TAMANH', 32  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) == 'HHL', TA048FNPRD(ALLTRIM(CVARSAB035), 'A2_COD'), '')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'HHL' .AND. EMPTY(POSICIONE('SA2', 1, XFILIAL('SA2') + CVARSAB033 + CVARSAB034, 'A2_COD')) .AND. !EMPTY(CVARSAB035)"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '036'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_LOJA'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3ITT'  })
AAdd(aReg, {'G8U_INICIO', 61  })
AAdd(aReg, {'G8U_TAMANH', 32  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) == 'HHL', TA048FNPRD(ALLTRIM(CVARSAB036), 'A2_LOJA'), '')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'HHL' .AND. EMPTY(POSICIONE('SA2', 1, XFILIAL('SA2') + CVARSAB033 + CVARSAB034, 'A2_LOJA')) .AND. !EMPTY(CVARSAB035)"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '037'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_FORNEC'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 3  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G8L'  })
AAdd(aReg, {'G8U_INDICE', 2  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G8L') + CCODSABRE + ALLTRIM(CVARSAB035)"  })
AAdd(aReg, {'G8U_RETURN', 'G8L_FORNEC'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) $ 'HHT|HTL'"  })
AAdd(aReg, {'G8U_OBS', 'No campo CHAVE foi informado:/\/\XFILIAL("G8L") + "000001" + CVARSAB04A/\/\A segunda posi��o "000001" refere-se ao codigo GDS do Sabe cadastrado na tabela G8O.'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '038'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_LOJA'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G8L'  })
AAdd(aReg, {'G8U_INDICE', 2  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G8L') + CCODSABRE + ALLTRIM(CVARSAB035)"  })
AAdd(aReg, {'G8U_RETURN', 'G8L_LOJA'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) $ 'HHT|HTL'"  })
AAdd(aReg, {'G8U_OBS', 'Defini��o fixa tempor�ria'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '039'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_FORNEC'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 3  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) $ 'HHT|HTL', TA048FNPRD(ALLTRIM(CVARSAB039), 'A2_COD'), '')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) $ 'HHT|HTL' .AND. EMPTY(POSICIONE('SA2', 1, XFILIAL('SA2') + CVARSAB037 + CVARSAB038, 'A2_COD')) .AND. !EMPTY(CVARSAB039)"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '03A'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_LOJA'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 3  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) $ 'HHT|HTL', TA048FNPRD(ALLTRIM(CVARSAB03A), 'A2_LOJA'), '')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) $ 'HHT|HTL' .AND. EMPTY(POSICIONE('SA2', 1, XFILIAL('SA2') + CVARSAB037 + CVARSAB038, 'A2_LOJA')) .AND. !EMPTY(CVARSAB039)"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '03B'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_VARIAV', 'CABREV'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3PTY'  })
AAdd(aReg, {'G8U_INICIO', 19  })
AAdd(aReg, {'G8U_TAMANH', 2  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '03C'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_FORNEC'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) == 'CAR', TA048FNCAR(CVARSAB03C, CVARSAB03B), '')"  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G8L'  })
AAdd(aReg, {'G8U_INDICE', 2  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G8L') + CCODSABRE + ALLTRIM(CVARSAB03C)"  })
AAdd(aReg, {'G8U_RETURN', 'G8L_FORNEC'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'CAR'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '03D'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_LOJA'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G8L'  })
AAdd(aReg, {'G8U_INDICE', 2  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G8L') + CCODSABRE + ALLTRIM(CVARSAB03C)"  })
AAdd(aReg, {'G8U_RETURN', 'G8L_LOJA'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'CAR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '03E'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_POSTOR'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M0'  })
AAdd(aReg, {'G8U_TRECHO', 'IU0PCC'  })
AAdd(aReg, {'G8U_INICIO', 89  })
AAdd(aReg, {'G8U_TAMANH', 5  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G3M'  })
AAdd(aReg, {'G8U_INDICE', 5  })
AAdd(aReg, {'G8U_CHAVE', 'CVARSAB03E'  })
AAdd(aReg, {'G8U_RETURN', 'G3M_CODIGO'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '03F'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_PCC'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M0'  })
AAdd(aReg, {'G8U_TRECHO', 'IU0PCC'  })
AAdd(aReg, {'G8U_INICIO', 89  })
AAdd(aReg, {'G8U_TAMANH', 5  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G3M'  })
AAdd(aReg, {'G8U_INDICE', 5  })
AAdd(aReg, {'G8U_CHAVE', 'CVARSAB03F'  })
AAdd(aReg, {'G8U_RETURN', 'G3M_PCC'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '03G'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_MOEDA'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G5T'  })
AAdd(aReg, {'G8U_INDICE', 4  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G5T') + 'BRL'"  })
AAdd(aReg, {'G8U_RETURN', 'G5T_CODIGO'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '03H'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_AGEMIS'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M0'  })
AAdd(aReg, {'G8U_TRECHO', 'IU0IS3'  })
AAdd(aReg, {'G8U_INICIO', 134  })
AAdd(aReg, {'G8U_TAMANH', 3  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G3H'  })
AAdd(aReg, {'G8U_INDICE', 4  })
AAdd(aReg, {'G8U_CHAVE', 'CVARSAB03H'  })
AAdd(aReg, {'G8U_RETURN', 'G3H_CODAGE'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '03I'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_AGRESE'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M0'  })
AAdd(aReg, {'G8U_TRECHO', 'IU0IAG'  })
AAdd(aReg, {'G8U_INICIO', 96  })
AAdd(aReg, {'G8U_TAMANH', 3  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G3H'  })
AAdd(aReg, {'G8U_INDICE', 4  })
AAdd(aReg, {'G8U_CHAVE', 'CVARSAB03I'  })
AAdd(aReg, {'G8U_RETURN', 'G3H_CODAGE'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '03J'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_SISORI'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G4K'  })
AAdd(aReg, {'G8U_INDICE', 2  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G4K') + 'SABRE'"  })
AAdd(aReg, {'G8U_RETURN', 'G4K_CODIGO'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_OBS', 'No campo Expr Leitura foi informado o "001" como sendo o C�digo do Sabre. Na tabela G4K, origem do dado, o campo c�digo possui 6 posi��es. J� no G3R_SISORI consta apenas 3 posi��es. Assim sendo os codigos foram gravados com 3 posi��es.'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '03K'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_LOCGDS'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M0'  })
AAdd(aReg, {'G8U_TRECHO', 'IU0PNR'  })
AAdd(aReg, {'G8U_INICIO', 54  })
AAdd(aReg, {'G8U_TAMANH', 8  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '03L'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_FORMCF'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'GDS SABRE'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_OBS', 'Forma de confirma��o, meio pelo qual a confirma��o foi feita.'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '03M'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_IDCONF'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M2'  })
AAdd(aReg, {'G8U_TRECHO', 'IU2TCN'  })
AAdd(aReg, {'G8U_INICIO', 8  })
AAdd(aReg, {'G8U_TAMANH', 11  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "!EMPTY(CVARSAB03M) .AND. ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '03N'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3R'  })
AAdd(aReg, {'G8U_CAMPO', 'G3R_IDCONF'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3CFN'  })
AAdd(aReg, {'G8U_INICIO', 21  })
AAdd(aReg, {'G8U_TAMANH', 15  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "!EMPTY(CVARSAB03N) .AND. ALLTRIM(CPNRPROD) != 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '040'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3S'  })
AAdd(aReg, {'G8U_CAMPO', 'G3S_CODPAX'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "T034SETSTC({{'__CPASSAG', STRZERO(NATUPASS, TAMSX3('G3S_CODPAX')[1])}})"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_FFORM", "STRZERO(NATUPASS, TAMSX3('G3S_CODPAX')[1])"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '041'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3S'  })
AAdd(aReg, {'G8U_CAMPO', 'G3S_TPPAX'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M2'  })
AAdd(aReg, {'G8U_TRECHO', 'IU2PTY'  })
AAdd(aReg, {'G8U_INICIO', 5  })
AAdd(aReg, {'G8U_TAMANH', 3  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) == 'AIR', POSICIONE('G6K', 2, XFILIAL('G6K') + '000004' + CVARSAB041, 'G6K_CDDE'), '1')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '042'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3S'  })
AAdd(aReg, {'G8U_CAMPO', 'G3S_TRATAM'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M1'  })
AAdd(aReg, {'G8U_TRECHO', 'IU1PNM'  })
AAdd(aReg, {'G8U_INICIO', 5  })
AAdd(aReg, {'G8U_TAMANH', 64  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'TA048PAXNM(CVARSAB042, 1)'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '043'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3S'  })
AAdd(aReg, {'G8U_CAMPO', 'G3S_PRINOM'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M1'  })
AAdd(aReg, {'G8U_TRECHO', 'IU1PNM'  })
AAdd(aReg, {'G8U_INICIO', 5  })
AAdd(aReg, {'G8U_TAMANH', 64  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'TA048PAXNM(CVARSAB043, 2)'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '044'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3S'  })
AAdd(aReg, {'G8U_CAMPO', 'G3S_ULTNOM'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M1'  })
AAdd(aReg, {'G8U_TRECHO', 'IU1PNM'  })
AAdd(aReg, {'G8U_INICIO', 5  })
AAdd(aReg, {'G8U_TAMANH', 64  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'TA048PAXNM(CVARSAB044, 3)'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '045'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3S'  })
AAdd(aReg, {'G8U_CAMPO', 'G3S_NOME'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M1'  })
AAdd(aReg, {'G8U_TRECHO', 'IU1PNM'  })
AAdd(aReg, {'G8U_INICIO', 5  })
AAdd(aReg, {'G8U_TAMANH', 64  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'TA048PAXNM(CVARSAB045, 4)'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '046'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3S'  })
AAdd(aReg, {'G8U_CAMPO', 'G3S_NOMINV'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M1'  })
AAdd(aReg, {'G8U_TRECHO', 'IU1PNM'  })
AAdd(aReg, {'G8U_INICIO', 5  })
AAdd(aReg, {'G8U_TAMANH', 64  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'TA048PAXNM(CVARSAB046, 5)'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '047'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3S'  })
AAdd(aReg, {'G8U_CAMPO', 'G3S_SEXO'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', '1'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CVARSAB042) $ '1|2'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '048'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3S'  })
AAdd(aReg, {'G8U_CAMPO', 'G3S_SEXO'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CVARSAB042) $ '3|4'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '050'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3T'  })
AAdd(aReg, {'G8U_CAMPO', 'G3T_CODPAX'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "T034SETSTC({{'__CPASSAG', STRZERO(NATUPASS, TAMSX3('G3S_CODPAX')[1])}})"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_FFORM", "STRZERO(NATUPASS, TAMSX3('G3T_CODPAX')[1])"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '051'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3T'  })
AAdd(aReg, {'G8U_CAMPO', 'G3T_IDINTE'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3ITN'  })
AAdd(aReg, {'G8U_INICIO', 3  })
AAdd(aReg, {'G8U_TAMANH', 2  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '052'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3T'  })
AAdd(aReg, {'G8U_CAMPO', 'G3T_LOC'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3OAL'  })
AAdd(aReg, {'G8U_INICIO', 240  })
AAdd(aReg, {'G8U_TAMANH', 8  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '053'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3T'  })
AAdd(aReg, {'G8U_CAMPO', 'G3T_ABCIA'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3CAR'  })
AAdd(aReg, {'G8U_INICIO', 59  })
AAdd(aReg, {'G8U_TAMANH', 2  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '054'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3T'  })
AAdd(aReg, {'G8U_CAMPO', 'G3T_CODFOR'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3CAR'  })
AAdd(aReg, {'G8U_INICIO', 59  })
AAdd(aReg, {'G8U_TAMANH', 2  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G4S'  })
AAdd(aReg, {'G8U_INDICE', 3  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G4S') + CVARSAB054"  })
AAdd(aReg, {'G8U_RETURN', 'G4S_FORNEC'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '055'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3T'  })
AAdd(aReg, {'G8U_CAMPO', 'G3T_LOJAF'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3CAR'  })
AAdd(aReg, {'G8U_INICIO', 59  })
AAdd(aReg, {'G8U_TAMANH', 2  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G4S'  })
AAdd(aReg, {'G8U_INDICE', 3  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G4S') + CVARSAB055"  })
AAdd(aReg, {'G8U_RETURN', 'G4S_LOJA'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '056'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3T'  })
AAdd(aReg, {'G8U_CAMPO', 'G3T_TERORI'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3DCC'  })
AAdd(aReg, {'G8U_INICIO', 19  })
AAdd(aReg, {'G8U_TAMANH', 3  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '057'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3T'  })
AAdd(aReg, {'G8U_CAMPO', 'G3T_TERDST'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3ACC'  })
AAdd(aReg, {'G8U_INICIO', 39  })
AAdd(aReg, {'G8U_TAMANH', 3  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '058'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3T'  })
AAdd(aReg, {'G8U_CAMPO', 'G3T_CLASRV'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3CLS'  })
AAdd(aReg, {'G8U_INICIO', 66  })
AAdd(aReg, {'G8U_TAMANH', 2  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '059'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3T'  })
AAdd(aReg, {'G8U_CAMPO', 'G3T_AERONA'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3EQP'  })
AAdd(aReg, {'G8U_INICIO', 113  })
AAdd(aReg, {'G8U_TAMANH', 3  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '05A'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3T'  })
AAdd(aReg, {'G8U_CAMPO', 'G3T_NRVOO'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3FLT'  })
AAdd(aReg, {'G8U_INICIO', 61  })
AAdd(aReg, {'G8U_TAMANH', 5  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '05B'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3T'  })
AAdd(aReg, {'G8U_CAMPO', 'G3T_DTSAID'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3DDT'  })
AAdd(aReg, {'G8U_INICIO', 10  })
AAdd(aReg, {'G8U_TAMANH', 5  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'TA048DTSAB(CVARSAB05B, DDTEMISS)'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_FFORM', 'CTOD(CVARSAB05B)'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '05C'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3T'  })
AAdd(aReg, {'G8U_CAMPO', 'G3T_HRINI'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3DTM'  })
AAdd(aReg, {'G8U_INICIO', 68  })
AAdd(aReg, {'G8U_TAMANH', 4  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "SUBSTR(CVARSAB05C,1,2) + ':' + SUBSTR(CVARSAB05C,3, 2)"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '05D'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3T'  })
AAdd(aReg, {'G8U_CAMPO', 'G3T_DTCHEG'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3DCH'  })
AAdd(aReg, {'G8U_INICIO', 91  })
AAdd(aReg, {'G8U_TAMANH', 1  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'CTOD(CVARSAB05B) + VAL(CVARSAB05D)'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '3'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '05E'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3T'  })
AAdd(aReg, {'G8U_CAMPO', 'G3T_HRFIM'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3ATM'  })
AAdd(aReg, {'G8U_INICIO', 73  })
AAdd(aReg, {'G8U_TAMANH', 4  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "SUBSTR(CVARSAB05E,1,2) + ':' + SUBSTR(CVARSAB05E,3, 2)"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '05F'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3T'  })
AAdd(aReg, {'G8U_CAMPO', 'G3T_QTDESC'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3NOS'  })
AAdd(aReg, {'G8U_INICIO', 92  })
AAdd(aReg, {'G8U_TAMANH', 1  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '05G'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3T'  })
AAdd(aReg, {'G8U_CAMPO', 'G3T_GDS'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'CCODSABRE'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '05H'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3T'  })
AAdd(aReg, {'G8U_CAMPO', 'G3T_BASTAR'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M4'  })
AAdd(aReg, {'G8U_TRECHO', 'IU4FBS'  })
AAdd(aReg, {'G8U_INICIO', 25  })
AAdd(aReg, {'G8U_TAMANH', 13  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 1  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_FFORM', 'IIF(!EMPTY(CVARSAB05H), CVARSAB05H, CBASTAR)'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '05I'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3T'  })
AAdd(aReg, {'G8U_CAMPO', 'G3T_ID'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '1'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '060'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3U'  })
AAdd(aReg, {'G8U_CAMPO', 'G3U_CODPAX'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "T034SETSTC({{'__CPASSAG', STRZERO(NATUPASS, TAMSX3('G3S_CODPAX')[1])}})"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_FFORM", "STRZERO(NATUPASS, TAMSX3('G3U_CODPAX')[1])"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '061'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3U'  })
AAdd(aReg, {'G8U_CAMPO', 'G3U_IDINTE'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3ITN'  })
AAdd(aReg, {'G8U_INICIO', 3  })
AAdd(aReg, {'G8U_TAMANH', 2  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '062'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3U'  })
AAdd(aReg, {'G8U_CAMPO', 'G3U_TPPROD'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', '1'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '063'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3U'  })
AAdd(aReg, {'G8U_CAMPO', 'G3U_DTINI'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3DDT'  })
AAdd(aReg, {'G8U_INICIO', 10  })
AAdd(aReg, {'G8U_TAMANH', 5  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'TA048DTSAB(CVARSAB063, DDTEMISS)'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_FFORM', 'CTOD(CVARSAB063)'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '064'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3U'  })
AAdd(aReg, {'G8U_CAMPO', 'G3U_HRINI'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', '12:00'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '065'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3U'  })
AAdd(aReg, {'G8U_CAMPO', 'G3U_DTFIM'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 2  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) $ 'HHT|HTL', TA048DTSAB(STRTRAN(CVARSAB065, 'OUT', ''), DDTEMISS), '')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_FFORM', 'CTOD(CVARSAB065)'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) $ 'HHT|HTL'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '066'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3U'  })
AAdd(aReg, {'G8U_CAMPO', 'G3U_DTFIM'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3OUT'  })
AAdd(aReg, {'G8U_INICIO', 45  })
AAdd(aReg, {'G8U_TAMANH', 5  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) == 'HHL', TA048DTSAB(CVARSAB066, DDTEMISS), '')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_FFORM', 'CTOD(CVARSAB066)'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'HHL'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '067'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3U'  })
AAdd(aReg, {'G8U_CAMPO', 'G3U_HRFIM'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', '12:00'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '068'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3U'  })
AAdd(aReg, {'G8U_CAMPO', 'G3U_CIDHOT'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 1  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G5S'  })
AAdd(aReg, {'G8U_INDICE', 3  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G5S') + CVARSAB068"  })
AAdd(aReg, {'G8U_RETURN', 'G5S_CIDADE'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) $ 'HHT|HTL'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '069'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3U'  })
AAdd(aReg, {'G8U_CAMPO', 'G3U_CIDHOT'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3CAC'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 3  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {"G8U_FPOS", "POSICIONE('G5S', 1, XFILIAL('G5S') + POSICIONE('G3B', 2, XFILIAL('G3B') + CVARSAB069, 'G3B_CODMUN'), 'G5S_CIDADE')"  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'HHL'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '06A'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_TABELA', 'G3U'  })
AAdd(aReg, {'G8U_VARIAV', 'NQTDDIA'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'ALLTRIM(STR(IIF(!EMPTY(CVARSAB065), CTOD(CVARSAB065) - CTOD(CVARSAB063), CTOD(CVARSAB066) - CTOD(CVARSAB063))))'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_FFORM", "IIF(CVARSAB06A == '0', '1', CVARSAB06A)"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '06B'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3U'  })
AAdd(aReg, {'G8U_CAMPO', 'G3U_QTDDIA'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'NQTDDIA'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '06C'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3U'  })
AAdd(aReg, {'G8U_CAMPO', 'G3U_QTDPRD'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3NRM'  })
AAdd(aReg, {'G8U_INICIO', 93  })
AAdd(aReg, {'G8U_TAMANH', 1  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'HHL'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '06D'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3U'  })
AAdd(aReg, {'G8U_CAMPO', 'G3U_QTDPRD'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3PTY'  })
AAdd(aReg, {'G8U_INICIO', 19  })
AAdd(aReg, {'G8U_TAMANH', 2  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) $ 'HHT|HTL'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '06E'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3U'  })
AAdd(aReg, {'G8U_CAMPO', 'G3U_CODAPO'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3NRM'  })
AAdd(aReg, {'G8U_INICIO', 94  })
AAdd(aReg, {'G8U_TAMANH', 7  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'HHL'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '06F'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3U'  })
AAdd(aReg, {'G8U_CAMPO', 'G3U_CODAPO'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 4  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) $ 'HHT|HTL'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '06G'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3U'  })
AAdd(aReg, {'G8U_CAMPO', 'G3U_BROKER'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'CCODSABRE'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '070'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3V'  })
AAdd(aReg, {'G8U_CAMPO', 'G3V_CODPAX'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "T034SETSTC({{'__CPASSAG', STRZERO(NATUPASS, TAMSX3('G3S_CODPAX')[1])}})"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_FFORM", "STRZERO(NATUPASS, TAMSX3('G3V_CODPAX')[1])"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '071'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3V'  })
AAdd(aReg, {'G8U_CAMPO', 'G3V_IDINTE'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3ITN'  })
AAdd(aReg, {'G8U_INICIO', 3  })
AAdd(aReg, {'G8U_TAMANH', 2  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '072'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3V'  })
AAdd(aReg, {'G8U_CAMPO', 'G3V_CIDRET'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 1  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G5S'  })
AAdd(aReg, {'G8U_INDICE', 3  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G5S') + CVARSAB072"  })
AAdd(aReg, {'G8U_RETURN', 'G5S_CODIGO'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_OBS', 'Campo requer defini��o de como ser� feito o De/Para entre o conteudo informado no Sabre e o cadastro de cidades no Protheus'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '073'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3V'  })
AAdd(aReg, {'G8U_CAMPO', 'G3V_LOCRET'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'LOCADORA'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_OBS', 'Campo n�o informado no Sabre'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '074'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3V'  })
AAdd(aReg, {'G8U_CAMPO', 'G3V_CIDDEV'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 1  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G5S'  })
AAdd(aReg, {'G8U_INDICE', 3  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G5S') + CVARSAB074"  })
AAdd(aReg, {'G8U_RETURN', 'G5S_CODIGO'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_OBS', 'Campo n�o definido no Sabre. Recomenda-se utilizar o conteudo atribuido ao campo G3V_CIDRET'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '075'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3V'  })
AAdd(aReg, {'G8U_CAMPO', 'G3V_LOCDEV'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'LOCADORA'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_OBS', 'Informa��o n�o contemplada no Sabre.'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '076'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3V'  })
AAdd(aReg, {'G8U_CAMPO', 'G3V_QTDPRD'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3CCN'  })
AAdd(aReg, {'G8U_INICIO', 18  })
AAdd(aReg, {'G8U_TAMANH', 1  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '077'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3V'  })
AAdd(aReg, {'G8U_CAMPO', 'G3V_CATAUT'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 3  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_SUBINI', 1  })
AAdd(aReg, {'G8U_SUBTAM', 1  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '078'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3V'  })
AAdd(aReg, {'G8U_CAMPO', 'G3V_TPAUT'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 3  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_SUBINI', 2  })
AAdd(aReg, {'G8U_SUBTAM', 1  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '079'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3V'  })
AAdd(aReg, {'G8U_CAMPO', 'G3V_TRADIR'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 3  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_SUBINI', 3  })
AAdd(aReg, {'G8U_SUBTAM', 1  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '07A'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3V'  })
AAdd(aReg, {'G8U_CAMPO', 'G3V_COMBAR'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 3  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_SUBINI', 4  })
AAdd(aReg, {'G8U_SUBTAM', 1  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '07B'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3V'  })
AAdd(aReg, {'G8U_CAMPO', 'G3V_GRPAUT'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 3  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_SUBINI', 1  })
AAdd(aReg, {'G8U_SUBTAM', 4  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '07C'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3V'  })
AAdd(aReg, {'G8U_CAMPO', 'G3V_DTINI'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3DDT'  })
AAdd(aReg, {'G8U_INICIO', 10  })
AAdd(aReg, {'G8U_TAMANH', 5  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'TA048DTSAB(CVARSAB07C, DDTEMISS)'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_FFORM', 'CTOD(CVARSAB07C)'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '07D'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3V'  })
AAdd(aReg, {'G8U_CAMPO', 'G3V_HRINI'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "TA048HRSAB(CVARSAB07D, 'CAR', 'ARR-')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_OBS', 'Campo n�o informado no layout Sabre'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '07E'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3V'  })
AAdd(aReg, {'G8U_CAMPO', 'G3V_DTFIM'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 2  })
AAdd(aReg, {'G8U_FREAD', 'TA048DTSAB(CVARSAB07E, DDTEMISS)'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_FFORM', 'CTOD(CVARSAB07E)'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '07F'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3V'  })
AAdd(aReg, {'G8U_CAMPO', 'G3V_HRFIM'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "TA048HRSAB(CVARSAB07F, 'CAR', 'RET-')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_OBS', 'Campo n�o informado no layout Sabre'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '07G'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_TABELA', 'G3V'  })
AAdd(aReg, {'G8U_VARIAV', 'NQTDDIA'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'ALLTRIM(STR(CTOD(CVARSAB07E) - CTOD(CVARSAB07C)))'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_FFORM", "IIF(CVARSAB07G == '0', '1', CVARSAB07G)"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '07H'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3V'  })
AAdd(aReg, {'G8U_CAMPO', 'G3V_QTDDIA'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'NQTDDIA'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '07I'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G3V'  })
AAdd(aReg, {'G8U_CAMPO', 'G3V_BROKER'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'CCODSABRE'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '080'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_BASTAR'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'CBASTAR'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '081'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_BASTAR'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) != 'AIR' .OR. (EMPTY(CVARSAB081) .AND. ALLTRIM(CPNRPROD) == 'AIR')"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '1'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '082'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_TPTAR'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_OBS', 'Campo informado como Fixo com cont�udo 2'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '083'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_MOEDFO'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M2'  })
AAdd(aReg, {'G8U_TRECHO', 'IU2FCC'  })
AAdd(aReg, {'G8U_INICIO', 35  })
AAdd(aReg, {'G8U_TAMANH', 3  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G5T'  })
AAdd(aReg, {'G8U_INDICE', 4  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G5T') + CVARSAB083"  })
AAdd(aReg, {'G8U_RETURN', 'G5T_CODIGO'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '084'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_MOEDFO'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 104  })
AAdd(aReg, {'G8U_TAMANH', 891  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 1  })
AAdd(aReg, {'G8U_FREAD', 'SUBSTR(CVARSAB084, LEN(CVARSAB084)-2, LEN(CVARSAB084))'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G5T'  })
AAdd(aReg, {'G8U_INDICE', 4  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G5T') + CVARSAB084"  })
AAdd(aReg, {'G8U_RETURN', 'G5T_CODIGO'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'HHL'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '085'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_MOEDFO'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 6  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G5T'  })
AAdd(aReg, {'G8U_INDICE', 4  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G5T') + CVARSAB085"  })
AAdd(aReg, {'G8U_RETURN', 'G5T_CODIGO'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) $ 'HHT|HTL'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '086'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_MOEDFO'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 5  })
AAdd(aReg, {'G8U_FREAD', 'SUBSTR(CVARSAB086, LEN(CVARSAB086)-2, LEN(CVARSAB086))'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G5T'  })
AAdd(aReg, {'G8U_INDICE', 4  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G5T') + CVARSAB086"  })
AAdd(aReg, {'G8U_RETURN', 'G5T_CODIGO'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) $ 'HHT|HTL' .AND. !EMPTY(CVARSAB086)"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '087'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_MOEDFO'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR2'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) == 'CAR', TA048MOEDA(CVARSAB087, 1), '')"  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G5T'  })
AAdd(aReg, {'G8U_INDICE', 4  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G5T') + CVARSAB087"  })
AAdd(aReg, {'G8U_RETURN', 'G5T_CODIGO'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'CAR'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '088'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_VARIAV', 'NVALOR01'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M2'  })
AAdd(aReg, {'G8U_TRECHO', 'IU2FAR'  })
AAdd(aReg, {'G8U_INICIO', 38  })
AAdd(aReg, {'G8U_TAMANH', 8  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '089'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_VARIAV', 'NVALOR01'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 104  })
AAdd(aReg, {'G8U_TAMANH', 891  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 1  })
AAdd(aReg, {'G8U_FREAD', 'VAL(SUBSTR(CVARSAB089, 1, LEN(CVARSAB089)-3))'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'HHL'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '08A'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_VARIAV', 'NVALOR01'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 5  })
AAdd(aReg, {'G8U_FREAD', 'VAL(SUBSTR(CVARSAB08A, 1, LEN(CVARSAB08A)-3))'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) $ 'HHT|HTL'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '08B'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_VARIAV', 'NVALOR01'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) == 'CAR', VAL(TA048MOEDA(CVARSAB08B, 2)), 0)"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'CAR'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '08C'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_VARIAV', 'NVALOR02'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M2'  })
AAdd(aReg, {'G8U_TRECHO', 'IU2EFR'  })
AAdd(aReg, {'G8U_INICIO', 120  })
AAdd(aReg, {'G8U_TAMANH', 8  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '08D'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '2'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_VARIAV', 'NVALOR02'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 104  })
AAdd(aReg, {'G8U_TAMANH', 891  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 2  })
AAdd(aReg, {'G8U_FREAD', 'VAL(SUBSTR(CVARSAB08D, 1, LEN(CVARSAB08D)-3)) * NQTDDIA'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'HHL'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '08E'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_VLRDIA'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'NVALOR01'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) != 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '08F'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_TARBAS'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'NVALOR01'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '08G'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_TXCAMB'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'IIF(NVALOR02 > 0, NVALOR02 / NVALOR01, 1)'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '08H'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_TXCAMB'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'IIF(NVALOR01 > 0, NVALOR02 / NVALOR01, 1)'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'HHL'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '08I'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_TXCAMB'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) $ 'HHT|HTL', VAL(TA048MOEDA(IIF(!EMPTY(CVARSAB086), CVARSAB086, CVARSAB085), 3)), 1)"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) $ 'HHT|HTL'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '08J'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_TXCAMB'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) == 'CAR', VAL(TA048MOEDA(CVARSAB087, 3)), 1)"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'CAR'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '08K'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_APLICA'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M5'  })
AAdd(aReg, {'G8U_TRECHO', 'IU5VR1'  })
AAdd(aReg, {'G8U_INICIO', 7  })
AAdd(aReg, {'G8U_TAMANH', 985  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 3  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '08L'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_MENOR'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'MT.'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '08M'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_MAIOR'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'TM.'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '08N'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_CHEIA'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'TC.'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '08O'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_PUBLIC'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'TB.'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '08P'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_OPORTU'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'TO.'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '08Q'  })
AAdd(aReg, {'G8U_ATIVO', '2'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_EARLY'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'TER.'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) $ 'HHT|HTL|HHL'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '08R'  })
AAdd(aReg, {'G8U_ATIVO', '2'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_LATE'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'TLA.'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) $ 'HHT|HTL|HHL'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '08S'  })
AAdd(aReg, {'G8U_ATIVO', '2'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G44'  })
AAdd(aReg, {'G8U_CAMPO', 'G44_PLALIM'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'TPL.'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) $ 'HHT|HTL|HHL'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '090'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G46'  })
AAdd(aReg, {'G8U_CAMPO', 'G46_SEQTAX'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '1'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '091'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G46'  })
AAdd(aReg, {'G8U_CAMPO', 'G46_EMISS'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'DDTEMISS'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '3'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '092'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G46'  })
AAdd(aReg, {'G8U_CAMPO', 'G46_CODTX'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G3A'  })
AAdd(aReg, {'G8U_INDICE', 1  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G3A') + PADR('BR', TAMSX3('G3A_CODIGO')[1], ' ')"  })
AAdd(aReg, {'G8U_RETURN', 'G3A_CODIGO'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_OBS', '� utilizada a fun��o TURA048G46 para gerar registros de Taxas para uma segunda e uma terceira taxas, considerando os codigos de taxas que constam no arquivo nas posi��es 64 e 74.'  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '093'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G46'  })
AAdd(aReg, {'G8U_CAMPO', 'G46_MOEDFO'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G5T'  })
AAdd(aReg, {'G8U_INDICE', 4  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G5T') + 'BRL'"  })
AAdd(aReg, {'G8U_RETURN', 'G5T_CODIGO'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '094'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G46'  })
AAdd(aReg, {'G8U_CAMPO', 'G46_TXCAMB'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', '1'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '095'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G46'  })
AAdd(aReg, {'G8U_CAMPO', 'G46_VLBASE'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M5'  })
AAdd(aReg, {'G8U_TRECHO', 'IU5VR1'  })
AAdd(aReg, {'G8U_INICIO', 7  })
AAdd(aReg, {'G8U_TAMANH', 985  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 4  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '100'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G47'  })
AAdd(aReg, {'G8U_CAMPO', 'G47_SEQ'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'CAR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '1'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '101'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G47'  })
AAdd(aReg, {'G8U_CAMPO', 'G47_EMISS'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'DDTEMISS'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '3'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'CAR'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '102'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G47'  })
AAdd(aReg, {'G8U_CAMPO', 'G47_EXTRA'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'HORAS EXCEDENTES'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'CAR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '103'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G47'  })
AAdd(aReg, {'G8U_CAMPO', 'G47_MOEDFO'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) == 'CAR', TA048MOEDA(CVARSAB103, 1), '')"  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G5T'  })
AAdd(aReg, {'G8U_INDICE', 4  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G5T') + CVARSAB103"  })
AAdd(aReg, {'G8U_RETURN', 'G5T_CODIGO'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'CAR'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '104'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G47'  })
AAdd(aReg, {'G8U_CAMPO', 'G47_QUANT'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', '1'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'CAR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '105'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G47'  })
AAdd(aReg, {'G8U_CAMPO', 'G47_VLUNIT'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M3'  })
AAdd(aReg, {'G8U_TRECHO', 'IU3VR4'  })
AAdd(aReg, {'G8U_INICIO', 36  })
AAdd(aReg, {'G8U_TAMANH', 956  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) == 'CAR', VAL(TA048MOEDA(CVARSAB105, 4)), 0)"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'CAR'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '106'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G47'  })
AAdd(aReg, {'G8U_CAMPO', 'G47_TXCAMB'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) == 'CAR', VAL(TA048MOEDA(CVARSAB103, 3)), 1)"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {"G8U_WHEN", "ALLTRIM(CPNRPROD) == 'CAR'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '110'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G4A'  })
AAdd(aReg, {'G8U_CAMPO', 'G4A_TPENT'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'RT.'  })
AAdd(aReg, {'G8U_TAGMUL', '1'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(!EMPTY(CVARSAB110), STRTOKARR2(CVARSAB110, '.', .F.)[1], '')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_WHEN', '!EMPTY(CVARSAB110)'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '111'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G4A'  })
AAdd(aReg, {'G8U_CAMPO', 'G4A_LOJA'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'RT.'  })
AAdd(aReg, {'G8U_TAGMUL', '1'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'CCODLOJA'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_WHEN', '!EMPTY(CVARSAB110)'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '112'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G4A'  })
AAdd(aReg, {'G8U_CAMPO', 'G4A_ITEM'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'RT.'  })
AAdd(aReg, {'G8U_TAGMUL', '1'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(!EMPTY(CVARSAB112), STRTOKARR2(CVARSAB112, '.', .F.)[2], '')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {'G8U_WHEN', '!EMPTY(CVARSAB110)'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '113'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G4A'  })
AAdd(aReg, {'G8U_CAMPO', 'G4A_PERRAT'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M9'  })
AAdd(aReg, {'G8U_TAG', 'RT.'  })
AAdd(aReg, {'G8U_TAGMUL', '1'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {"G8U_FREAD", "IIF(!EMPTY(CVARSAB113), STRTOKARR2(CVARSAB113, '.', .F.)[3], '')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '2'  })
AAdd(aReg, {'G8U_WHEN', '!EMPTY(CVARSAB110)'  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '120'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G4D'  })
AAdd(aReg, {'G8U_CAMPO', 'G4D_NUMCAR'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M5'  })
AAdd(aReg, {'G8U_TRECHO', 'IU5VR1'  })
AAdd(aReg, {'G8U_INICIO', 7  })
AAdd(aReg, {'G8U_TAMANH', 985  })
AAdd(aReg, {'G8U_TAG', 'CC'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 6  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_FFORM", "PADR(SUBSTR(CVARSAB120, 3, LEN(CVARSAB120)), 16, ' ')"  })
AAdd(aReg, {"G8U_WHEN", "!EMPTY(CVARSAB120) .AND. ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '121'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G4D'  })
AAdd(aReg, {'G8U_CAMPO', 'G4D_DTOPER'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_FREAD', 'DDTEMISS'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '3'  })
AAdd(aReg, {"G8U_WHEN", "!EMPTY(CVARSAB120) .AND. ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '122'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G4D'  })
AAdd(aReg, {'G8U_CAMPO', 'G4D_NSU'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M2'  })
AAdd(aReg, {'G8U_TRECHO', 'IU2ATH'  })
AAdd(aReg, {'G8U_INICIO', 208  })
AAdd(aReg, {'G8U_TAMANH', 9  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "!EMPTY(CVARSAB120) .AND. ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '123'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G4D'  })
AAdd(aReg, {'G8U_CAMPO', 'G4D_CODBAN'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '1'  })
AAdd(aReg, {'G8U_ALIAS', 'G6K'  })
AAdd(aReg, {'G8U_INDICE', 2  })
AAdd(aReg, {"G8U_CHAVE", "XFILIAL('G6K') + '000006' + SUBSTR(CVARSAB120, 1, 2)"  })
AAdd(aReg, {'G8U_RETURN', 'G6K_CDDE'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "!EMPTY(CVARSAB120) .AND. ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '124'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G4D'  })
AAdd(aReg, {'G8U_CAMPO', 'G4D_TITULA'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_BLOCO', 'M5'  })
AAdd(aReg, {'G8U_TRECHO', 'IU5VR1'  })
AAdd(aReg, {'G8U_INICIO', 7  })
AAdd(aReg, {'G8U_TAMANH', 985  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEPARA', '/'  })
AAdd(aReg, {'G8U_POSICA', 6  })
AAdd(aReg, {"G8U_FREAD", "IIF(ALLTRIM(CPNRPROD) == 'AIR', TA048PAXNM(CVARSAB124, 6), '')"  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "!EMPTY(CVARSAB120) .AND. ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '1'  })
AAdd(aReg, {'G8U_INCREM', '2'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
 
aReg := {}
 
AAdd(aReg, {'G8U_GDS', '1'  })
AAdd(aReg, {'G8U_VERSAO', '17'  })
AAdd(aReg, {'G8U_SEQUEN', '125'  })
AAdd(aReg, {'G8U_ATIVO', '1'  })
AAdd(aReg, {'G8U_DADO', '1'  })
AAdd(aReg, {'G8U_TABELA', 'G4D'  })
AAdd(aReg, {'G8U_CAMPO', 'G4D_SEQ'  })
AAdd(aReg, {'G8U_CONTEX', '2'  })
AAdd(aReg, {'G8U_TAGMUL', '2'  })
AAdd(aReg, {'G8U_PASSAG', '2'  })
AAdd(aReg, {'G8U_SEEK', '2'  })
AAdd(aReg, {'G8U_TIPO', '1'  })
AAdd(aReg, {"G8U_WHEN", "!EMPTY(CVARSAB120) .AND. ALLTRIM(CPNRPROD) == 'AIR'"  })
AAdd(aReg, {'G8U_MACFIX', '2'  })
AAdd(aReg, {'G8U_INCREM', '1'  })
AAdd(aReg, {'G8U_OBRIGA', '2'  })
 
RecLock('G8U', .T.) 
 
G8U->G8U_FILIAL := xFilial('G8U')  
 
For _nPosReg := 1 To Len(aReg)  
 
   nFieldPos := FieldPos(aReg[_nPosReg][1])  
 
   If nFieldPos <> 0 
      If aReg[_nPosReg][1] == 'G8U_OBS' 
         FieldPut(nFieldPos, StrTran(aReg[_nPosReg][2], '/\', CRLF) ) 
      Else 
         FieldPut(nFieldPos, aReg[_nPosReg][2] ) 
      EndIf 
   EndIf  
 
Next  
 
G8U->(MSUnLock())  
 
End Transaction 
Return 
