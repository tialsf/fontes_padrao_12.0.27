#Include "protheus.ch"

/*/{Protheus.doc} FWDLEXLIST
    Fun��o resp�nsavel por devolver uma lista de exten��es autorizadas para download no tWebEngine.

    ------------------------------------ IMPORTANTE ----------------------------------------------
    
            Essa fonte � de uso exclusivo do FRAMEWORK, qualquer altera��o deve ser alinhada,
            caso contr�rio ser�o desfeitas

    ----------------------------------------------------------------------------------------------

    @type  Function
    @return aWhitelist array com as exten��es de arquivos permitidas para download. 
    
    @see (https://tdn.totvs.com/display/PROT/AdDLExList)

/*/
Function FwDLExList() 

Local aWhitelist as Array
Local aNewItens as Array
Local cItem as Character
Local nX as Numeric

aWhitelist := {}
aNewItens := {}

AAdd(aWhitelist,"xls")
AAdd(aWhitelist,"xlsx")
AAdd(aWhitelist,"pdf")
AAdd(aWhitelist,"csv")
AAdd(aWhitelist,"txt")
AAdd(aWhitelist,"doc")
AAdd(aWhitelist,"docx")
AAdd(aWhitelist,"xml")

// Ponto de entrada para inclus�o de novas exten��es. 
If ExistBlock("AdDLExList")
   aNewItens := ExecBlock("AdDLExList",.F.,.F.,{aWhitelist})
    For nX:= 1 to Len(aNewItens)
        cItem := LOWER(aNewItens[nX]) 
        If ASCAN(aWhitelist, cItem ) == 0
            AAdd(aWhitelist, cItem )
        EndIf 
   Next
EndIf

Return aWhitelist