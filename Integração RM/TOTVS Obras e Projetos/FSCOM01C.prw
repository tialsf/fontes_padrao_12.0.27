#Include "RESTFUL.CH"

#Include "TOTVS.CH"

#Include "Protheus.ch"
#Include "RwMake.ch"
#Include "TopConn.ch"
#Include "FWAdapterEAI.ch"
#Include "COLORS.CH"
#Include "TBICONN.CH"
#Include "COMMON.CH"
#Include "XMLXFUN.CH"
#Include "fileio.ch"

#Include "FSCOM01C.CH"

#DEFINE  TAB  CHR ( 13 ) + CHR ( 10 )

WSRESTFUL GETRESIDUES DESCRIPTION STR0012

WSDATA page AS INTEGER
WSDATA pageSize AS INTEGER

WSDATA sourceApp AS STRING
WSDATA companyId AS STRING
WSDATA branchId AS STRING

WSDATA movementType AS STRING

WSDATA movementInternalId AS STRING

WSMETHOD GET    DESCRIPTION STR0012 WSSYNTAX "/GETRESIDUES || /GETRESIDUES/{id}"

END WSRESTFUL

WSMETHOD GET WSRECEIVE page, pageSize, sourceApp, companyId, branchId, movementType, movementInternalId WSSERVICE GETRESIDUES
	Local lMetodo := .F.

	Local cTempMov := ""
	Local lNew := .F.
	Local nTotal := 0
	Local nPages
	Local nPSize
	Local cMarca
	Local cEmpre
	Local cBranc
	Local lMovementType
	Local lMovementInternalId
	
	local itemnum := ''
	Local cPdCVer          := RTrim(PmsMsgUVer('ORDER',                  'MATA120')) //Vers�o do Pedido de Compra
	Local cPrdVer		   := RTrim(PmsMsgUVer('ITEM','MATA010')) //Vers�o do Produto
	Local cSCoVer			:= RTrim(PmsMsgUVer('REQUEST',			'MATA110')) //Vers�o da Solicita��o de Compra
	Local cSAoVer			:= RTrim(PmsMsgUVer('REQUEST',			'MATA105')) //Vers�o da Solicita��o de Armazem
	
	Local cComp := ''
	Local nLenComp := 0
	Local lCompact := .F.

	local jsreturn
	local jsdata
	local strJs
	
	Local aArrayMovimRet := {}
	Local aArrayItemMovim := {}
	
	Private bError         := { |e| oError := e, Break(e) }
	Private bErrorBlock    := ErrorBlock( bError )
	Private oError

	DEFAULT ::page 	 			     := 0
	DEFAULT ::pageSize 	 			 := 0
	DEFAULT ::sourceApp 	 		 := ''
	DEFAULT ::companyId 	 		 := ''
	DEFAULT ::branchId               := ''
	DEFAULT ::movementType           := ''
	DEFAULT ::movementInternalId 	 := ''
	
	BEGIN SEQUENCE

		nPages := ::page
		nPSize := ::pageSize
		cMarca := ::sourceApp
		cEmpre := ::companyId
		cBranc := ::branchId
		lMovementType := ::movementType
		lMovementInternalId     := ::movementInternalId

		if(Empty (cMarca))
			SetRestFault(400, STR0006)
			Return (lMetodo)
		endif
		
		aEmpre := FWEAIEMPFIL(cEmpre, cBranc, cMarca)

		If Len (aEmpre) < 2
			SetRestFault(400, STR0001 + cEmpre + STR0002 + cBranc + "' " + STR0005 + cMarca +"!")
			Return (lMetodo)
		EndIf

		If Len (aEmpre) > 1

			RESET ENVIRONMENT

			RPCSetType(3)

			PREPARE ENVIRONMENT EMPRESA aEmpre[1] FILIAL aEmpre[2] TABLES "SC7", "SC1", "SCP", "DHN" MODULO "COM"

		EndIf

		fSetErrorHandler(STR0007)

		If TcGetDB() == "ORACLE"
			cBanco := "ORACLE"
		Else
			cBanco := "MSSQL"
		EndIf

		fResetErrorHandler()

		If ! fMandInfor(nPages,nPSize,cMarca,cEmpre,cBranc, lMovementType, lMovementInternalId)

			Return (lMetodo)

		EndIf

		If ! fReadSQL(cBranc, cMarca, lMovementType, lMovementInternalId, nPages, nPSize, cBanco)

			Return (lMetodo)

		EndIf

		/*
		RETORNO REST
		RETORNO REST
		*/
		
		fSetErrorHandler(STR0008)

		// define o tipo de retorno do m�todo
		::SetContentType("application/json")

		jsreturn := JsonObject():new()

		jsreturn['page']  := 1

		jsdata := JsonObject():new()

		jsreturn['pageSize'] := 1
		
		lNew := .F.
		
		nTotal := 0
		
		DBSelectArea("DAD")
		DBGoTop()
		Do While DAD->( !EOF() )

			nTotal += 0

			jsMovim := JsonObject():new()

			jsMovim['companyInternalId'] := cEmpAnt+'|'+RTrim(DAD->FILIAL)
			//jsMovim['documentType'] := DAD->ENTIDADE
			
			//jsMovim['recNo'] := DAD->RECNO

			If !Empty ( DAD->ITEMNUMBER )
				if(DAD->TYPE == 0)
					itemnum := IntSCoExt(/*cEmpresa*/, /*Filial*/, RTrim(DAD->NUMSC), RTrim(DAD->ITEMNUMBER), cSCoVer)[2]
					
					jsMovim['itemNumber'] := itemnum
				elseif(DAD->TYPE == 1)
					itemnum := IntPdCExt(/*Empresa*/, /*Filial*/, RTrim(DAD->NUMPC), RTrim(DAD->ITEMNUMBER), cPdCVer)[2]
					
					jsMovim['itemNumber'] := itemnum
				else
					itemnum := IntSArExt(/*Empresa*/, /*Filial*/, RTrim(DAD->NUMSA), RTrim(DAD->ITEMNUMBER), SToD(DAD->DATASA), cSAoVer)[2]
					
					
					jsMovim['itemNumber'] := itemnum
				endif
			EndIf
			
			
			If !Empty ( DAD->TYPE )
				jsMovim['type'] := DAD->TYPE
			EndIf

			If !Empty ( DAD->NUMSC )
				jsMovim['documentNumberSC'] := IntSCoExt(/*cEmpresa*/, /*Filial*/, RTrim(DAD->NUMSC), Nil, cSCoVer)[2]
			EndIf
			
			If !Empty ( DAD->NUMSA )
				jsMovim['documentNumberSA'] := IntSArExt(/*cEmpresa*/, /*Filial*/, RTrim(DAD->NUMSA), Nil, cSAoVer)[2]
			EndIf

			If !Empty ( DAD->NUMPC )
				jsMovim['documentNumberPC'] := IntPdCExt(/*Empresa*/, /*Filial*/, RTrim(DAD->NUMPC), Nil, cPdCVer)[2]
			EndIf

			If !Empty ( DAD->QUANTSC )
				jsMovim['quantitySC'] := DAD->QUANTSC
			EndIf

			If !Empty ( DAD->QUANTPC )
				jsMovim['quantityPC'] := DAD->QUANTPC
			EndIf

			If !Empty ( DAD->RESIDUOSC )
				jsMovim['residuoSC'] := RTrim(DAD->RESIDUOSC)
			else
				jsMovim['residuoSC'] := ""
			EndIf

			If !Empty ( DAD->RESIDUOPC )
				jsMovim['residuoPC'] := RTrim(DAD->RESIDUOPC)
			else
				jsMovim['residuoPC'] := ""
			EndIf

			If !Empty ( DAD->UNIT )
				jsMovim['unitofMeasureInternalId'] := IntUndExt(/*cEmpresa*/, /*cFilial*/, DAD->UNIT)[2]
			EndIf
			
			//internalid do item
			If !Empty ( DAD->ITEMNUMBER )
				jsMovim['itemInternalId'] := IntProExt(cEmpAnt,xFilial('SB1'),DAD->PRODUTO,cPrdVer)[2]
			EndIf
			
			If !Empty ( DAD->QUJESC )
				jsMovim['qujeSC'] := DAD->QUJESC
			EndIf

			If !Empty ( DAD->QUJEPC )
				jsMovim['qujePC'] := DAD->QUJEPC
			EndIf
			
			//vERIFICAR SE PRECISA BUSCAR SC8 COTA��O
			If !Empty ( DAD->COTACAOSC )
				jsMovim['cotacaoSC'] := cEmpAnt + '|' + xFilial("SC7") + '|' + RTrim(DAD->COTACAOSC)
			EndIf

			If !Empty ( DAD->APROVSC )
				jsMovim['aprovSC'] := RTrim(DAD->APROVSC)
			EndIf

			If !Empty ( DAD->APROVPC )
				jsMovim['aprovPC'] := RTrim(DAD->APROVPC)
			EndIf
			
			If !Empty ( DAD->DATASA )
				jsMovim['DataSA'] := SToD(DAD->DATASA)
			EndIf

			AAdd(aArrayMovimRet, jsMovim )

			//nCurReg := DAD->ITEMNUMBER

			DBSelectArea("DAD")
			DBSkip()

		EndDo

		jsdata['movAssignments'] := aArrayMovimRet

		//jsreturn['total']  := nTotReg
		//jsreturn['hasNext']     := Iif(nCurReg >= nTotReg,.F.,.T.)

		jsreturn['data'] := jsdata

		strJs := FWJsonSerialize(jsreturn,.T.,.T.)

		//If Type("::GetHeader('Accept-Encoding')") != "U"  .and. 'GZIP' $ Upper(::GetHeader('Accept-Encoding') )
		//	lCompact := .T.
		//EndIf

		If(lCompact)
			::SetHeader('Content-Encoding','gzip')
			GzStrComp(strJs, @cComp, @nLenComp )
		Else
			cComp := strJs
		Endif

		::SetResponse(cComp)

		/*
		RETORNO REST
		RETORNO REST
		*/

		fResetErrorHandler()

		lMetodo := .T.

		RECOVER
		ErrorBlock(bErrorBlock)
		SetRestFault(400, STR0011 + TAB + oError:Description)
		lMetodo := .F.
		Return (lMetodo)

	END SEQUENCE

	ErrorBlock( bErrorBlock )

Return (lMetodo )

/*
/*{Protheus.doc} fMandInfor
Function fMandInfor
@Uso    Verifica os campos obrigat�rios no recebimento da mensagem REST
@Autor  Daniel de Paulo e Silva - TOTVS
@param  Campos recebidos da mensagem REST nPages,nPSize,cMarca,cEmpre, lMovementType, lMovementInternalId
@return	.T. -> Processo validado ; .F. -> Processo Interrompido

*/

Static Function fMandInfor(nPages,nPSize,cMarca,cEmpre,cBranc,cMovementType,cMovementInternalId)
	***************************************************************************************
	**
	**
	*****

	Local lPost := .F.
	Local cNum  := " "

	If nPages <= 0
		SetRestFault(400, STR0003 + "nPage" + STR0004)

	EndIf

	If nPSize <= 0
		SetRestFault(400, STR0003 + "pageSize" + STR0004)
		Return (lPost)
	EndIf

	If Empty(cMarca)
		SetRestFault(400, STR0003 + "sourceApp" + STR0004)
		Return (lPost)
	EndIf

	If Empty(cEmpre)
		SetRestFault(400, STR0003 + "companyId" + STR0004)
		Return (lPost)
	EndIf

	If Empty(cBranc)
		SetRestFault(400, STR0003 + "branchId" + STR0004)
		Return (lPost)
	EndIf

	If Empty(cMovementType)
		SetRestFault(400, STR0003 + "movementType" + STR0004)
		Return (lPost)
	EndIf

	If Empty(cMovementInternalId)
		SetRestFault(400, STR0003 + "movementInternalId" + STR0004)
		Return (lPost)
	EndIf
	
	if cMovementType == "0"
		cNum := RTrim(CFGA070Int(cMarca, "SC1", "C1_NUM", cMovementInternalId))
				
		If Empty(cNum)
			SetRestFault(400, STR0013 + " '" + cMovementInternalId + "'")
			Return (lPost)
		EndIf
	ELSEIF cMovementType == "1"
		cNum := RTrim(CFGA070Int(cMarca, "SC7", "C7_NUM", cMovementInternalId))
				
		If Empty(cNum)
			SetRestFault(400, STR0014 + " '" + cMovementInternalId + "'")
			Return (lPost)
		EndIf
	ELSEIF cMovementType == "2"
		cNum := RTrim(CFGA070Int(cMarca, "SCP", "CP_NUM", cMovementInternalId))
				
		If Empty(cNum)
			SetRestFault(400, "SOLICITA��O DE ARMAZEM " + " '" + cMovementInternalId + "'")
			Return (lPost)
		EndIf
	else
		lPost := .F.
		
	EndIf
	
	
	lPost := .T.

Return (lPost)

/*
{Protheus.doc} fReadSQL
Function fReadSQL
@Uso    Prepara o arquivo de trabalho montado na query
@param  Marca, Tipo Movimento, InternalId Movimento, ItemInternalId
@return	Nenhum

@Autor  Daniel de Paulo e Silva - TOTVS
*/

Static Function fReadSQL(cBranch, cMarca, cMovementType,cMovementInternalId, nPages, nPSize, cBanco)
	Local sNomeTabelaSC := " "
	Local sNomeTabelaPC := " "
	Local sNomeTabelaDHN := " "
	Local sNomeTabelaSP := " "
	Local nHandle := 0
	
	Local cTemp := " "
	Local cNum := " "
	Private cQry := " "

	fSetErrorHandler(STR0009)

	DBSelectArea("SC7")
	DBSetOrder(1)
	DBSeek(xFilial("SC7"))

	DBSelectArea("SC1")
	DBSetOrder(1)
	DBSeek(xFilial("SC1"))

	If ! Empty(cMovementType)
		If 	cMovementType == "0"
			sNomeTabelaSC := "SC1"
			sNomeTabelaPC := "SC7"
			
			cTemp := RTrim(CFGA070Int(cMarca, "SC1", "C1_NUM", cMovementInternalId))
			
			if(!Empty(cTemp))
				cBranch = AllTrim(Separa(cTemp,"|")[2])
				cNum = AllTrim(Separa(cTemp,"|")[3])
			endif
			
			cQry += "SELECT 0 TYPE, SOL.C1_ITEM AS ITEMNUMBER, SOL.C1_NUM AS NUMSC,'' AS NUMSA,'' DATASA, SOL.C1_QUANT AS QUANTSC, SOL.C1_RESIDUO AS RESIDUOSC, SOL.C1_QUJE AS QUJESC, " + TAB
            cQry += "                 SOL.C1_COTACAO AS COTACAOSC, SOL.C1_APROV AS APROVSC, " + TAB
            cQry += "                 coalesce(PED.C7_NUM, ' ') AS NUMPC, coalesce(PED.C7_QUANT,0) AS QUANTPC, coalesce(PED.C7_RESIDUO,' ') AS RESIDUOPC, coalesce(PED.C7_QUJE,0) AS QUJEPC, " + TAB
            cQry += "                 coalesce(PED.C7_APROV, ' ') AS APROVPC, SOL.C1_FILIAL FILIAL, C1_UM UNIT, C1_PRODUTO PRODUTO " + TAB
            cQry += "            FROM " + RetSqlName(sNomeTabelaSC) + " SOL (NOLOCK) " + TAB
            cQry += "                  LEFT JOIN " + RetSqlName(sNomeTabelaPC) + "  PED (NOLOCK) " + TAB
            cQry += "                    ON PED.C7_FILIAL = '"+xFilial("SC7")+"' AND SOL.C1_FILIAL = '"+xFilial("SC1")+"' " + TAB
            cQry += "                   AND PED.C7_NUMSC = SOL.C1_NUM " + TAB
            cQry += "                   AND PED.C7_ITEMSC = SOL.C1_ITEM " + TAB
            cQry += "                   AND PED.D_E_L_E_T_ <> '*' " + TAB
            cQry += "       WHERE " + TAB
            
            cQry += "          SOL.C1_NUM = '"+cNum+"' AND " + TAB 
            cQry += "          SOL.C1_FILIAL = '"+cBranch+"' AND " + TAB 
            
            //cQry += "            AND SOL.C1_ITEM LIKE '%' || :NUMITEMSC " + TAB
            cQry += "             SOL.D_E_L_E_T_ <> '*' " + TAB
            
            //EXISTS RetSqlName("AFG")? 
		ELSEIF (cMovementType == "1")
			sNomeTabelaSC := "SC1"
			sNomeTabelaPC := "SC7"
			
			cTemp := RTrim(CFGA070Int(cMarca, "SC7", "C7_NUM", cMovementInternalId))
			
			if(!Empty(cTemp))
				cBranch = AllTrim(Separa(cTemp,"|")[2])
				cNum = AllTrim(Separa(cTemp,"|")[3])
			endif
			
			cQry += "SELECT 1 TYPE, PED.C7_ITEM AS ITEMNUMBER, SOL.C1_NUM AS NUMSC,'' AS NUMSA,'' DATASA, SOL.C1_QUANT AS QUANTSC, SOL.C1_RESIDUO AS RESIDUOSC, SOL.C1_QUJE AS QUJESC,  " + TAB
            cQry += "             SOL.C1_COTACAO AS COTACAOSC, SOL.C1_APROV AS APROVSC, " + TAB
            cQry += "             coalesce(PED.C7_NUM, ' ') AS NUMPC, coalesce(PED.C7_QUANT,0) AS QUANTPC, coalesce(PED.C7_RESIDUO,' ') AS RESIDUOPC, coalesce(PED.C7_QUJE,0) AS QUJEPC,  " + TAB
            cQry += "             coalesce(PED.C7_APROV, ' ') AS APROVPC, PED.C7_FILIAL FILIAL, C7_UM UNIT, C7_PRODUTO PRODUTO " + TAB
            cQry += "        FROM " + RetSqlName(sNomeTabelaPC) + " PED (NOLOCK) " + TAB
            cQry += "               LEFT JOIN " + RetSqlName(sNomeTabelaSC) + " SOL (NOLOCK) " + TAB
            cQry += "                 ON 1 = 2 " + TAB
            cQry += "       WHERE " + TAB
            
            cQry += "          PED.C7_NUM = '"+cNum+"' AND " + TAB 
            cQry += "          PED.C7_FILIAL = '"+cBranch+"' AND " + TAB 
            
            //cQry += "         AND PED.C7_ITEM LIKE '%' + :NUMITEMPC " + TAB
            cQry += "          PED.D_E_L_E_T_ <> '*' " + TAB
            
            //EXISTS? RetSqlName("AJ7")
		ELSEIF (cMovementType == "2") // Busca pela solicita��o de armazem
		
		    DBSelectArea("SCP")
		    DBSetOrder(1)
		    DBSeek(xFilial("SCP"))
		
		    DBSelectArea("DHN")
		    DBSetOrder(1)
		    DBSeek(xFilial("DHN"))
		    
			sNomeTabelaSP := "SCP"
			sNomeTabelaSC := "SC1"
			sNomeTabelaPC := "SC7"
			sNomeTabelaDHN := "DHN"
			
			cTemp := RTrim(CFGA070Int(cMarca, "SCP", "CP_NUM", cMovementInternalId))
			
			if(!Empty(cTemp))
				cBranch = AllTrim(Separa(cTemp,"|")[2])
				cNum = AllTrim(Separa(cTemp,"|")[3])
			endif
			
			cQry += "SELECT 2 TYPE, SCP.CP_ITEM AS ITEMNUMBER, SOL.C1_NUM AS NUMSC, SCP.CP_NUM AS NUMSA,SCP.CP_EMISSAO DATASA, " + TAB
			cQry += "               (SCP.CP_QUJE + SOL.C1_QUANT) AS QUANTSC, SOL.C1_RESIDUO AS RESIDUOSC, " + TAB 
			cQry += "               (SCP.CP_QUJE + SOL.C1_QUJE) AS QUJESC, " + TAB
            cQry += "                 SOL.C1_COTACAO AS COTACAOSC, SOL.C1_APROV AS APROVSC, " + TAB
            cQry += "                 coalesce(PED.C7_NUM, ' ') AS NUMPC, coalesce(PED.C7_QUANT,0) AS QUANTPC, coalesce(PED.C7_RESIDUO,' ') AS RESIDUOPC, coalesce(PED.C7_QUJE,0) AS QUJEPC, " + TAB
            cQry += "                 coalesce(PED.C7_APROV, ' ') AS APROVPC, SOL.C1_FILIAL FILIAL, C1_UM UNIT, C1_PRODUTO PRODUTO " + TAB
            cQry += "            FROM " + RetSqlName(sNomeTabelaSP) + " SCP (NOLOCK) " + TAB
            cQry += "         	    JOIN " + RetSqlName(sNomeTabelaDHN) + " RELAC ON " + TAB
			cQry += "         			RELAC.DHN_DOCORI = SCP.CP_NUM AND RELAC.DHN_ITORI = SCP.CP_ITEM " + TAB
			cQry += "            		AND RELAC.D_E_L_E_T_ <> '*' " + TAB
			cQry += "            		AND RELAC.DHN_FILORI = '"+xFilial("SCP")+"' " + TAB
			cQry += "               LEFT JOIN "+RetSqlName(sNomeTabelaSC)+"  SOL (NOLOCK)  " + TAB
            cQry += "                 ON SOL.C1_FILIAL = '"+xFilial("SC1")+"' AND " + TAB
			cQry += "            		 RELAC.DHN_DOCDES = SOL.C1_NUM  " + TAB
			cQry += "            		AND RELAC.DHN_ITDES = SOL.C1_ITEM " + TAB
			cQry += "            		AND SCP.D_E_L_E_T_ <> '*' AND RELAC.DHN_FILDES = '"+xFilial("SC1")+"' " + TAB
            cQry += "               LEFT JOIN "+RetSqlName(sNomeTabelaPC)+"  PED (NOLOCK)  " + TAB
            cQry += "                 ON PED.C7_FILIAL = '"+xFilial("SC7")+"' AND SOL.C1_FILIAL = '"+xFilial("SC1")+"' AND " + TAB
            cQry += "            	PED.C7_NUMSC = SOL.C1_NUM  " + TAB
            cQry += "               AND PED.C7_ITEMSC = SOL.C1_ITEM  " + TAB
            cQry += "              	AND PED.D_E_L_E_T_ <> '*'          " + TAB
            
            cQry += "        WHERE  SCP.CP_NUM = '"+cNum+"' AND " + TAB 
            cQry += "          SCP.CP_FILIAL = '"+cBranch+"' AND " + TAB 
            
            //cQry += "         AND PED.C7_ITEM LIKE '%' + :NUMITEMPC " + TAB
            cQry += "          SCP.D_E_L_E_T_ <> '*' " + TAB
            
		EndIf
	EndIf
	
	cQry = ChangeQuery(cQry)

	fCloseDados("DAD")

	dbUseArea( .T., "TOPCONN", TcGenQry(,,cQry), "DAD", .T., .T. )

	fResetErrorHandler()

	**
	Return (.T.)
	**

/*
{Protheus.doc} fCloseDados
Function fCloseDados
@Uso    Fecha um arquivo de trabalho
@param  Arquivo de trabalho
@return	Nenhum

@Autor  Wesley Alves Pereira - TOTVS
*/

Static Function fCloseDados (cDados)

	fSetErrorHandler(STR0010)

	If Select(cDados) > 0
		dbSelectArea(cDados)
		dbCloseArea()
	EndIf

	fResetErrorHandler()

Return (.T.)

/*
{Protheus.doc} fSetErrorHandler
Function fSetErro
@Uso    Seta c�digo e mensagem de erro
@param  Objeto de erro
@return	Nenhum

@Autor  Lucas Peixoto Sepe - TOTVS
*/
Static Function fSetErrorHandler(cTitle)
	bError  := { |e| oError := e , oError:Description := cTitle + TAB + oError:Description, Break(e) }
	bErrorBlock    := ErrorBlock( bError )
Return(.T.)

/*
{Protheus.doc} fResetErrorHandler
Function fSetErro
@Uso    Seta c�digo e mensagem de erro
@param  Objeto de erro
@return	Nenhum

@Autor  Lucas Peixoto Sepe - TOTVS
*/
Static Function fResetErrorHandler(cTitle)
	bError  := { |e| oError := e , Break(e) }
	bErrorBlock    := ErrorBlock( bError )
Return(.T.)