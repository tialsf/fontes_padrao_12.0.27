#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'PARMTYPE.CH'

#DEFINE tb        Chr( 9 )           // Caracter de Tabulacao
#DEFINE SIMPLES   "' + SIMPLES + '"  // Caracter Aspas Simples
#DEFINE DUPLAS    "' + DUPLAS + '"   // Caracter Aspas Duplas


//-------------------------------------------------------------------
/*/{Protheus.doc} JURROTAGP
Rotina para gerar fonte de update para as Agrupamento de Campos do SIGAJURI

@author Ernani Forastieri
@since 01/05/09
@version 1.0
/*/
//-------------------------------------------------------------------
Function JURROTAGP()
	Local   cNomeProg := 'JURCRIAAGP'
	Local   cEmpJob  := cEmpAnt
	Local   cFilJob  := cFilAnt
	Local   aSay     := {}
	Local   aButton  := {}
	Local   nOpc     := 0
	Local   cDesc5   := ''
	Local   cDirProg := '\system\'
	Local   lOk      := .T.
	Local   cMsg     := 'Processamento cancelado.'
	Local   cTitulo  := ''

	aAdd( aSay, 'Este rotina ira fazer a geracao do programa de update ' + cNomeProg )
	aAdd( aSay, 'este fonte deve ser compilado e gerado um patch para envio a clientes.' )
	aAdd( aSay, 'Ap�s a sua execu��o no cliente as tabelas NVX/NUX/NUY de Agrupamento de Campos' )
	aAdd( aSay, 'ser�o preenchidas,'             )

	aAdd( aButton, { 1, .T., { || nOpc := 1, FechaBatch() } } )
	aAdd( aButton, { 2, .T., { || FechaBatch()            } } )

	FormBatch( cTitulo, aSay, aButton )

	If nOpc <> 1
		Return NIL
	EndIf

	nHdl      := FCreate( Alltrim( cDirProg ) + Alltrim( cNomeProg ) + '.prw' )

	If nHdl < 0
		ApMsgStop( 'N�o foi poss�vel criar o arquivo de UPDATE.', 'ATEN��O' )
		Return .F.
	EndIf

	If Gera( nHdl, cNomeProg )
		ApMsgInfo( 'Gera��o terminada.', 'ATEN��O' )
	Else
		ApMsgStop( 'Gera��o terminada com problemas.', 'ATEN��O' )
	EndIf

	FClose( nHdl )

Return NIL


//-------------------------------------------------------------------
/*/{Protheus.doc} Gera
Rotina Auxiliar para gerar fonte de update para as Agrupamento de Campos do SIGAJURI

@author Ernani Forastieri
@since 01/05/09
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function Gera( nHdl, cNomeProg )
	Local nI := 0

	GrvLinha( nHdl, "#INCLUDE 'PROTHEUS.CH'")
	GrvLinha( nHdl, "#INCLUDE 'PARMTYPE.CH'")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "#DEFINE SIMPLES Chr( 39 )"  )
	GrvLinha( nHdl, "#DEFINE DUPLAS  Chr( 34 )"  )
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "//-------------------------------------------------------------------")
	GrvLinha( nHdl, "/*/{Protheus.doc} " + cNomeProg + "")
	GrvLinha( nHdl, "Cria��o de Agrupamento de Campos SIGAJURI")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "@since 28/04/09")
	GrvLinha( nHdl, "@version 1.0")
	GrvLinha( nHdl, "/*/")
	GrvLinha( nHdl, "//-------------------------------------------------------------------")
	GrvLinha( nHdl, "User Function " + cNomeProg + "()")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "Local lAlone := ( Select ( 'SM0' ) == 0 )")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "If 	lAlone")
	GrvLinha( nHdl, "	MsApp():New('SIGAJURI')")
	GrvLinha( nHdl, "	oApp:lMenu      := .F.")
	GrvLinha( nHdl, "	oApp:lShortCut	:= .F.")
	GrvLinha( nHdl, "	oApp:cInternet  := NIL")
	GrvLinha( nHdl, "	oApp:cModDesc	:= 'Cria��o de Agrupamento de Campos SIGAJURI'")
	GrvLinha( nHdl, "	oApp:bMainInit  := { || Abertura( ,,{} ), JurCrAgp( lAlone ) }")
	GrvLinha( nHdl, "	SetFunName( 'JURCRIAAGP' )")
	GrvLinha( nHdl, "	PtSetTheme( 'OCEAN'      )")
	GrvLinha( nHdl, "	oApp:CreateEnv()")
	GrvLinha( nHdl, "	oApp:RunApp(.T.)")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "Else")
	GrvLinha( nHdl, "	JurCrAgp( lAlone )")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "EndIf")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "Return NIL")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "//-------------------------------------------------------------------")
	GrvLinha( nHdl, "/*/{Protheus.doc} JurCrAgp")
	GrvLinha( nHdl, "Funcao Auxiliar para Cria��o de Agrupamento de Campos SIGAJURI")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "@since 28/04/09")
	GrvLinha( nHdl, "@version 1.0")
	GrvLinha( nHdl, "/*/")
	GrvLinha( nHdl, "//-------------------------------------------------------------------")
	GrvLinha( nHdl, "Function JurCrAgp( lAlone )")
	GrvLinha( nHdl, "Local   aSay     := {}")
	GrvLinha( nHdl, "Local   aButton  := {}")
	GrvLinha( nHdl, "Local   nOpc     := 0")
	GrvLinha( nHdl, "Local   lOk      := .T.")
	GrvLinha( nHdl, "Local   cMsg     := 'Processamento cancelado.'")
	GrvLinha( nHdl, "Local   cTitulo  := 'Cria��o de Agrupamento de Campos SIGAJURI'")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "aAdd( aSay, 'Esta rotina ir� efetuar a cria��o dos Agrupamento de Campos padr�o' )")
	GrvLinha( nHdl, "aAdd( aSay, 'utilizados no m�dulo Jur�dico (SIGAJURI).'  )")
	GrvLinha( nHdl, "aAdd( aSay, ''  )")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "aAdd( aButton, { 1, .T., { || nOpc := 1, FechaBatch() } } )")
	GrvLinha( nHdl, "aAdd( aButton, { 2, .T., { || FechaBatch()            } } )")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "FormBatch( cTitulo, aSay, aButton )")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "If nOpc <> 1")
	GrvLinha( nHdl, "	If lAlone")
	GrvLinha( nHdl, "		Final()")
	GrvLinha( nHdl, "	EndIf")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "	Return NIL")
	GrvLinha( nHdl, "EndIf")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "Processa( { || lOk := RunProcNVX( cTitulo ) } , 'Aguarde', 'Processando...', .F. )")
	GrvLinha( nHdl, "Processa( { || lOk := RunProcNUX( cTitulo ) } , 'Aguarde', 'Processando...', .F. )")
	GrvLinha( nHdl, "Processa( { || lOk := RunProcNUY( cTitulo ) } , 'Aguarde', 'Processando...', .F. )")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "If lOk")
	GrvLinha( nHdl, "	cMsg := 'Processamento terminado.'")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "Else")
	GrvLinha( nHdl, "	cMsg := 'Processamento terminados com problemas.'")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "EndIf")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "If lAlone")
	GrvLinha( nHdl, "	Final()")
	GrvLinha( nHdl, "EndIf")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "Return NIL")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "//-------------------------------------------------------------------")
	GrvLinha( nHdl, "/*/{Protheus.doc} RunProcNVX")
	GrvLinha( nHdl, "Funcao Auxiliar para Cria��o de Agrupamento de Campos SIGAJURI")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "@since 28/04/09")
	GrvLinha( nHdl, "@version 1.0")
	GrvLinha( nHdl, "/*/")
	GrvLinha( nHdl, "//-------------------------------------------------------------------")
	GrvLinha( nHdl, "Static Function  RunProcNVX( cTitulo )")
	GrvLinha( nHdl, "Local aStruct  := {}")
	GrvLinha( nHdl, "Local aDados  := {}")
	GrvLinha( nHdl, "Local aChave  := {}")
	GrvLinha( nHdl, "Local cChave  := ''")
	GrvLinha( nHdl, "Local nReg    := 0")
	GrvLinha( nHdl, "Local nI      := 0")
	GrvLinha( nHdl, "Local lRet    := .T.")
	GrvLinha( nHdl, "Local cAlias  := 'NVX'")
	GrvLinha( nHdl, "")

	GrvLinha( nHdl, "aChave := StrToArray( '" + StrTran( AllTrim( "NVX_FILIAL+NVX_TABELA+NVX_FUNCAO" ), ' ' , '' ) + "', '+' )")
	GrvLinha( nHdl, "")

	aEstrut := NVX->( dbStruct( ) )
	aEval( aEstrut, { |aX| GrvLinha( nHdl, "aAdd( aStruct, '" + AllTrim( aX[1] ) + "' )") } )
	GrvLinha( nHdl, "")

	NVX->( dbGoTop() )
	While !NVX->( EOF() )
		cLinha := "aAdd( aDados, {"

		For nI := 1 To Len( aEstrut )
			xDado := NVX->( FieldGet( FieldPos( aEstrut[nI][1] ) ) )
			If     aEstrut[nI][2] == 'C'
				//cLinha += TrocaAspas( xDado, .T., !aEstrut[nI][1] $ SX2->X2_UNICO  )
				cLinha += TrocaAspas( xDado, .T., .F. )
			ElseIf aEstrut[nI][2] == 'N'
				cLinha +=  AllTrim( Str( xDado ) )
			ElseIf  aEstrut[nI][2] == 'D'
				cLinha += "CToD('" + DToC( xDado )  + "')"
			Else
				cLinha += "'" + AllToChar( xDado ) + "'"
			EndIf
			cLinha += ','
		Next
		cLinha := Left( cLinha, Len( cLinha ) - 1 ) + '} )'

		GrvLinha( nHdl, cLinha )
		NVX->( dbSkip() )
	End
	GrvLinha( nHdl, "")


	GrvLinha( nHdl, "ProcRegua(  Len( aDados ) )")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "For nReg := 1 To Len( aDados )")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "	IncProc()")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "	cChave := ''")
	GrvLinha( nHdl, "	For nI := 1 To Len( aChave )")
	GrvLinha( nHdl, "		cChave += aDados[nReg][aScan( aStruct, { |x| x == aChave[nI] } ) ]")
	GrvLinha( nHdl, "	Next")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "	(cAlias)->( dbSetOrder( 1 ) )")
	GrvLinha( nHdl, "	RecLock( cAlias, !(cAlias)->( dbSeek ( cChave ) ) )")
	GrvLinha( nHdl, "	For nI := 1 To Len( aStruct )")
	GrvLinha( nHdl, "		(cAlias)->( FieldPut( FieldPos( aStruct[nI] ), aDados[nReg][nI] ) )")
	GrvLinha( nHdl, "	Next")
	GrvLinha( nHdl, "	MsUnLock()")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "Next")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "Return lRet")
	GrvLinha( nHdl, "")


	///// NUX
	GrvLinha( nHdl, "//-------------------------------------------------------------------")
	GrvLinha( nHdl, "/*/{Protheus.doc} RunProcNUX")
	GrvLinha( nHdl, "Funcao Auxiliar para Cria��o de Agrupamento de Campos SIGAJURI")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "@since 28/04/09")
	GrvLinha( nHdl, "@version 1.0")
	GrvLinha( nHdl, "/*/")
	GrvLinha( nHdl, "//-------------------------------------------------------------------")
	GrvLinha( nHdl, "Static Function  RunProcNUX( cTitulo )")
	GrvLinha( nHdl, "Local aStruct  := {}")
	GrvLinha( nHdl, "Local aDados  := {}")
	GrvLinha( nHdl, "Local aChave  := {}")
	GrvLinha( nHdl, "Local cChave  := ''")
	GrvLinha( nHdl, "Local nReg    := 0")
	GrvLinha( nHdl, "Local nI      := 0")
	GrvLinha( nHdl, "Local lRet    := .T.")
	GrvLinha( nHdl, "Local cAlias  := 'NUX'")
	GrvLinha( nHdl, "")

	GrvLinha( nHdl, "aChave := StrToArray( '" + StrTran( AllTrim( "NUX_FILIAL+NUX_TABELA+NUX_FUNCAO+NUX_CODGRP" ), ' ' , '' ) + "', '+' )")
	GrvLinha( nHdl, "")

	aEstrut := NUX->( dbStruct( ) )
	aEval( aEstrut, { |aX| GrvLinha( nHdl, "aAdd( aStruct, '" + AllTrim( aX[1] ) + "' )") } )
	GrvLinha( nHdl, "")

	NUX->( dbGoTop() )
	While !NUX->( EOF() )
		cLinha := "aAdd( aDados, {"

		For nI := 1 To Len( aEstrut )
			xDado := NUX->( FieldGet( FieldPos( aEstrut[nI][1] ) ) )
			If     aEstrut[nI][2] == 'C'
				//cLinha += TrocaAspas( xDado, .T., !aEstrut[nI][1] $ SX2->X2_UNICO  )
				cLinha += TrocaAspas( xDado, .T., .F. )
			ElseIf aEstrut[nI][2] == 'N'
				cLinha +=  AllTrim( Str( xDado ) )
			ElseIf  aEstrut[nI][2] == 'D'
				cLinha += "CToD('" + DToC( xDado )  + "')"
			Else
				cLinha += "'" + AllToChar( xDado ) + "'"
			EndIf
			cLinha += ','
		Next
		cLinha := Left( cLinha, Len( cLinha ) - 1 ) + '} )'

		GrvLinha( nHdl, cLinha )
		NUX->( dbSkip() )
	End
	GrvLinha( nHdl, "")

	GrvLinha( nHdl, "ProcRegua(  Len( aDados ) )")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "For nReg := 1 To Len( aDados )")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "	IncProc()")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "	cChave := ''")
	GrvLinha( nHdl, "	For nI := 1 To Len( aChave )")
	GrvLinha( nHdl, "		cChave += aDados[nReg][aScan( aStruct, { |x| x == aChave[nI] } ) ]")
	GrvLinha( nHdl, "	Next")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "	(cAlias)->( dbSetOrder( 1 ) )")
	GrvLinha( nHdl, "	RecLock( cAlias, !(cAlias)->( dbSeek ( cChave ) ) )")
	GrvLinha( nHdl, "	For nI := 1 To Len( aStruct )")
	GrvLinha( nHdl, "		(cAlias)->( FieldPut( FieldPos( aStruct[nI] ), aDados[nReg][nI] ) )")
	GrvLinha( nHdl, "	Next")
	GrvLinha( nHdl, "	MsUnLock()")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "Next")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "Return lRet")
	GrvLinha( nHdl, "")



	///// NUY
	GrvLinha( nHdl, "//-------------------------------------------------------------------")
	GrvLinha( nHdl, "/*/{Protheus.doc} RunProcNUY")
	GrvLinha( nHdl, "Funcao Auxiliar para Cria��o de Agrupamento de Campos SIGAJURI")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "@since 28/04/09")
	GrvLinha( nHdl, "@version 1.0")
	GrvLinha( nHdl, "/*/")
	GrvLinha( nHdl, "//-------------------------------------------------------------------")
	GrvLinha( nHdl, "Static Function  RunProcNUY( cTitulo )")
	GrvLinha( nHdl, "Local aStruct  := {}")
	GrvLinha( nHdl, "Local aDados  := {}")
	GrvLinha( nHdl, "Local aChave  := {}")
	GrvLinha( nHdl, "Local cChave  := ''")
	GrvLinha( nHdl, "Local nReg    := 0")
	GrvLinha( nHdl, "Local nI      := 0")
	GrvLinha( nHdl, "Local lRet    := .T.")
	GrvLinha( nHdl, "Local cAlias  := 'NUY'")
	GrvLinha( nHdl, "")

	GrvLinha( nHdl, "aChave := StrToArray( '" + StrTran( AllTrim( "NUY_FILIAL+NUY_TABELA+NUY_FUNCAO+NUY_CODGRP+NUY_CAMPO" ), ' ' , '' ) + "', '+' )")
	GrvLinha( nHdl, "")

	aEstrut := NUY->( dbStruct( ) )
	aEval( aEstrut, { |aX| GrvLinha( nHdl, "aAdd( aStruct, '" + AllTrim( aX[1] ) + "' )") } )
	GrvLinha( nHdl, "")

	NUY->( dbGoTop() )
	While !NUY->( EOF() )
		cLinha := "aAdd( aDados, {"

		For nI := 1 To Len( aEstrut )
			xDado := NUY->( FieldGet( FieldPos( aEstrut[nI][1] ) ) )
			If     aEstrut[nI][2] == 'C'
				cLinha += TrocaAspas( xDado, .T., .F. )
			ElseIf aEstrut[nI][2] == 'N'
				cLinha +=  AllTrim( Str( xDado ) )
			ElseIf  aEstrut[nI][2] == 'D'
				cLinha += "CToD('" + DToC( xDado )  + "')"
			Else
				cLinha += "'" + AllToChar( xDado ) + "'"
			EndIf
			cLinha += ','
		Next
		cLinha := Left( cLinha, Len( cLinha ) - 1 ) + '} )'

		GrvLinha( nHdl, cLinha )
		NUY->( dbSkip() )
	End
	GrvLinha( nHdl, "")


	GrvLinha( nHdl, "ProcRegua(  Len( aDados ) )")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "For nReg := 1 To Len( aDados )")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "	IncProc()")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "	cChave := ''")
	GrvLinha( nHdl, "	For nI := 1 To Len( aChave )")
	GrvLinha( nHdl, "		cChave += aDados[nReg][aScan( aStruct, { |x| x == aChave[nI] } ) ]")
	GrvLinha( nHdl, "	Next")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "	(cAlias)->( dbSetOrder( 1 ) )")
	GrvLinha( nHdl, "	RecLock( cAlias, !(cAlias)->( dbSeek ( cChave ) ) )")
	GrvLinha( nHdl, "	For nI := 1 To Len( aStruct )")
	GrvLinha( nHdl, "		(cAlias)->( FieldPut( FieldPos( aStruct[nI] ), aDados[nReg][nI] ) )")
	GrvLinha( nHdl, "	Next")
	GrvLinha( nHdl, "	MsUnLock()")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "Next")
	GrvLinha( nHdl, "")
	GrvLinha( nHdl, "Return lRet")
	GrvLinha( nHdl, "")


Return .T.


//-------------------------------------------------------------------
/*/{Protheus.doc} GrvLinha
Grava a linha no arquivo de update

@author Ernani Forastieri
@since 01/05/09
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function GrvLinha( nHdl, cLinha )
	Local nTam := 0

	cLinha := RTrim( cLinha )
	cLinha += CRLF

	nTam   := Len( cLinha )

	If FWrite( nHdl, cLinha, nTam ) <> nTam
		ApMsgStop( 'Erro na grava��o da linha do arquivo do fonte.', 'ATEN��O')
	EndIf

Return NIL


//-------------------------------------------------------------------
/*/{Protheus.doc} TrocaAspas
Trata as aspas simples e duplas de strings

@author Ernani Forastieri
@since 01/05/09
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function TrocaAspas( cTexto, lPoeAspas, lAlTrim,cCharAsp )
	Local   cRet        := ''
	Local   nPosSimples := 0
	Local   nPosDuplas  := 0

	Default cRet        := ''
	Default cCharAsp    := "'"
	Default lPoeAspas   := .T.

	cRet := cTexto

	nPosSimples := At( "'", cRet )
	nPosDuplas  := At( '"', cRet )

	If     nPosSimples == 0
		cCharAsp := "'"

	ElseIf nPosSimples > 0 .AND. nPosDuplas == 0
		//cRet     := StrTran( cRet, "'", '"' )
		cCharAsp := '"'

	ElseIf nPosSimples > 0 .AND. nPosDuplas >  0
		cRet := StrTran( cRet, "'", SIMPLES )
		cRet := StrTran( cRet, '"', DUPLAS )

	EndIf

Return IIf( lPoeAspas, cCharAsp, '' ) + IIf( lAlTrim, AllTrim( cRet ), cRet ) + IIf( lPoeAspas, cCharAsp, '' )