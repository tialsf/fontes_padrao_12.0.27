#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"

Static lWSTLegal := .F.

//-------------------------------------------------------------------
/*/{Protheus.doc} JuriRstMd
Publica��o dos modelos que devem ficar dispon�veis no REST.
Classe que herda da FwRestModel
@since 26/06/2019
/*/
//-------------------------------------------------------------------
Class JurModRest From FwRestModel
	Data qryOrder AS STRING
	Method Activate()
	Method DeActivate()
	Method SetFilter(cFilter)
	Method Seek(cPk)

EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} Activate()
M�todo para ativar a classe
@since 26/06/2019
/*/
//-------------------------------------------------------------------
Method Activate() Class JurModRest
Local oReqBody   := Nil
Local cVerbo     := self:GetHttpHeader("_METHOD_")
local cPath      := self:GetHttpHeader("_PATH_")
Local cCajuri    := ''
local cBody      := ''
Local cRotina    := ''
Local cCmpBdy    := ''
Local cPDigitada := ''
Local aQryString := {}
Local lAcesso    := .T.
Local nOpc       := 2
Local nI         := 0
Local nIndex     := 0

public cTipoAsJ
public c162TipoAs
	nModulo := 76
	lWSTLegal := .T.
	lAcesso   := _Super:Activate()

	self:qryOrder := self:GetHttpHeader("orderBy")

	If '/FWMODEL/JURA095' $ cPath

		cBody:= self:GetHttpHeader("_BODY_")

		If FWJsonDeserialize(cBody,@oReqBody)
			 If cVerbo == "POST" .OR. cVerbo == "PUT"
				For nI := 1 To Len(oReqBody:MODELS[1]:FIELDS)
					cCmpBdy := oReqBody:MODELS[1]:FIELDS[nI]:ID
					If cCmpBdy == "NSZ_TIPOAS"
						cTipoAsJ   := oReqBody:MODELS[1]:FIELDS[nI]:VALUE
						c162TipoAs := cTipoAsJ
						Exit
					EndIf 
				Next nI
			Else
				If ('cajuri' $ cBody)
					cCajuri:= oReqBody:cajuri
					cTipoAsJ := JurGetDados("NSZ",1,xFilial("NSZ") + cCajuri, "NSZ_TIPOAS")
					c162TipoAs := cTipoAsJ
				Else
					cTipoAsJ := "001"
					c162TipoAs := "001"
				EndIf
			EndIf
		Else
			If(at("JURA095/",cPath)>0)
				cCajuri    := SUBSTR(cPath, at("JURA095/",cPath)+8)
				cCajuri    := SUBSTR(cCajuri, 0, IIf(at("/",cCajuri)>0, at("/",cCajuri)-1,len(cCajuri)))
				cCajuri    := DECODE64( cCajuri )
				cTipoAsJ   := JurGetDados("NSZ",1,cCajuri, "NSZ_TIPOAS")
				c162TipoAs := cTipoAsJ
			EndIf
		EndIf
	EndIf

	Do case
		Case cVerbo == 'GET'
			public INCLUI   := .F.
			public ALTERA   := .F.
		Case cVerbo == 'POST'
			public INCLUI   := .T.
			public ALTERA   := .F.
			nOpc := 3
		Case cVerbo == 'PUT'
			public INCLUI   := .F.
			public ALTERA   := .T.
			nOpc := 4
		Case cVerbo == 'DELETE'
			public INCLUI   := .F.
			public ALTERA   := .F.
			nOpc := 5
	End Case

	Do case
		Case IsInPath(cPath, "TJurAnexo") //Anexos
			cRotina   := '03'
		Case IsInPath(cPath, "JURA100")   //Andamentos
			cRotina   := '04'
		Case IsInPath(cPath, "JURA106")   //Follow-ups
			cRotina   := '05'
		Case IsInPath(cPath, "JURA094")   //Objetos
			cRotina   := '06'
		Case IsInPath(cPath, "JURA270")   //Pedidos
			cRotina   := '06'
		Case IsInPath(cPath, "JURA098")   //Garantias
			cRotina   := '07'
		Case IsInPath(cPath, "JURA099")   //Despesas
			cRotina   := '08'
		Case IsInPath(cPath, "JURA095")   //Processos
			cRotina   := '14'
		Case IsInPath(cPath, "JURA260")   //Liminares
			cRotina   := '20'
		Case IsInPath(cPath, "JURA269")   //Favoritos
			Self:SetFilter("O0V_USER='" + __CUSERID + "' ")
		Case IsInPath(cPath, "JCLIDEP")
			Self:SetFilter("A1_FILIAL='" + xFilial("SA1") + "'")
		Case IsInPath(cPath, "JURA132")
			Self:SetFilter("A2_FILIAL='" + xFilial("SA2") + "'")
		Case IsInPath(cPath, "JBANFIN")
			Self:SetFilter("A6_FILIAL='" + xFilial("SA6") + "'")
		Case IsInPath(cPath, "JNATFIN")
			Self:SetFilter("ED_FILIAL='" + xFilial("SED") + "'")
		Case IsInPath(cPath, "JCONPGFIN")
			Self:SetFilter("E4_FILIAL='" + xFilial("SE4") + "'")
		Case IsInPath(cPath, "JPRODUTO")
			Self:SetFilter("B1_FILIAL='" + xFilial("SB1") + "'")
		Case IsInPath(cPath, "JCCUSTO")
			Self:SetFilter("CTT_FILIAL='" + xFilial("CTT") + "'")
		Case IsInPath(cPath, "JURA159")   // Participantes
			Self:SetFilter("RD0_FILIAL='" + xFilial("RD0") + "'")
		Case IsInPath(cPath, "JURA276")   // Modelos de Exporta��o
			Self:SetFilter("NQ5_FILIAL='" + xFilial("NQ5") + "'")
		Case IsInPath(cPath, "JURA279")   // Distribui��o
			If (nIndex := Ascan(SELF:GetQueryString(), {|x| x[1] == 'PALAVRACHAVE'})) > 0
				aQryString := SELF:GetQueryString()
				cPDigitada := aQryString[nIndex][2]
				FilPChave(cPDigitada, Self)
			EndIf
	End Case

	if !Empty(cRotina)
		lAcesso := JA162AcTL(cRotina,nOpc)
	Endif

	if !lAcesso
		SetRestFault(403,"Acesso negado")
		ConOut("Sem permiss�o para " + cVerbo + " em " + Substr(cPath,Rat("/",cPath)+1))
		self:ClearModel()
	Endif


	Self:lActivate := lAcesso

Return lAcesso

//-------------------------------------------------------------------
/*/{Protheus.doc} FilPChave()
Monta o filtro para pesquisar a palavra chave.

@param cPDigitada - Palavra que o usu�rio deseja filtrar.
@param oFwMdlRest - Objeto do FWModelRest

@since 07/07/2020
/*/
//-------------------------------------------------------------------
Static Function FilPChave(cPDigitada, oFwMdlRest)
Local aAux      := {}
Local cPalChave := ''
Local cFiltro   := ''
Local nCont     := 1

Default cPDigitada := ''
Default oFwMdlRest := Nil

	aAux := StrToKarr( cPDigitada, "|")
	
	//Palavras digitadas
	For nCont := 1 To Len(aAux)
		cPalChave := AllTrim( Upper(aAux[nCont]) )
		
		If !Empty(cPalChave)
			cFiltro := "  ( NZZ_ESCRI  LIKE '%" + cPalChave + "%'"
			cFiltro += " OR NZZ_TERMO  LIKE '%" + cPalChave + "%'"
			cFiltro += " OR NZZ_TRIBUN LIKE '%" + cPalChave + "%'"
			cFiltro += " OR NZZ_OCORRE LIKE '%" + cPalChave + "%'"
			cFiltro += " OR NZZ_REU    LIKE '%" + cPalChave + "%'"
			cFiltro += " OR NZZ_AUTOR  LIKE '%" + cPalChave + "%'"
			cFiltro += " OR NZZ_FORUM  LIKE '%" + cPalChave + "%'"
			cFiltro += " OR NZZ_VARA   LIKE '%" + cPalChave + "%'"
			cFiltro += " OR NZZ_NUMPRO LIKE '%" + cPalChave + "%'"
			cFiltro += " OR NZZ_CIDADE LIKE '%" + cPalChave + "%'"
			cFiltro += " OR NZZ_ESTADO LIKE '%" + cPalChave + "%'"
			cFiltro += " OR NZZ_ADVOGA LIKE '%" + cPalChave + "%')"
			
			oFwMdlRest:SetFilter(cFiltro)
		EndIf
	Next nCont

Return cFiltro

//-------------------------------------------------------------------
/*/{Protheus.doc} DeActivate()
M�todo para desativar a classe
@since 26/06/2019
/*/
//-------------------------------------------------------------------
Method DeActivate() Class JurModRest

	Self:cFilter := ""
	If lWSTLegal
		lWSTLegal := .F.
	EndIf

Return _Super:DeActivate()

//-------------------------------------------------------------------
/*/{Protheus.doc} SetFilter(cFilter)
Monta os filtros extra
@since 30/09/2019
/*/
//-------------------------------------------------------------------
Method SetFilter(cFilter)  Class JurModRest
Local cFiltExi := ""
	If (self:cFilter != Nil)
		cFiltExi := Self:cFilter
	EndIf

	If !Empty(cFiltExi)
		cFiltExi += " AND "
	Endif
	cFiltExi += cFilter

	Self:cFilter := cFiltExi
Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} IsInPath(cPath, cFonte)
Verifica se o Endpoint (cFonte) est� na requisi��o (cPath)

@Param cPath - Path completo da requisi��o
@Param cFonte - Nome do fonte a ser verificado

@since 30/09/2019
/*/
//-------------------------------------------------------------------
Static Function IsInPath(cPath, cFonte)
Default cPath  := ""
Default cFonte := ""
Return  cFonte $ cPath

//-------------------------------------------------------------------
/*/{Protheus.doc}  GetWhereQryAlias(cTable)
Adiciona a seguran�a padr�o de acesso as filiais do sistema
@since 20/02/2020
/*/
//-------------------------------------------------------------------

Static Function GetWhereQryAlias(cTable)
Local cWhere AS CHARACTER
Local cUsrFilFilter AS CHARACTER

    cWhere := " WHERE D_E_L_E_T_ = ' '"

    //Introduz a seguran�a padr�o de acesso as filiais do sistema
    If (cTable)->(FieldPos(PrefixoCpo(cTable)+"_FILIAL")) > 0
        cUsrFilFilter := FWSQLUsrFilial(cTable)
        If !Empty(cUsrFilFilter)
            cWhere += " AND " + cUsrFilFilter
        EndIf
    EndIf

Return cWhere

//-------------------------------------------------------------------
/*/{Protheus.doc}  Seek(cPK)
Adiciona uma ordena��o
@since 20/02/2020
/*/
//-------------------------------------------------------------------

Method Seek(cPK) Class JurModRest
Local lRet AS LOGICAL
Local cQry AS CHARACTER
Local cPkFilter AS CHARACTER
Local cQuery AS CHARACTER
Local lHasPK AS LOGICAL

lRet   := .F.
lHasPK := !Empty(cPK)

	If self:HasAlias()
		cQuery := "SELECT R_E_C_N_O_"

		cQuery += " FROM "+ RetSqlName(self:cAlias)
		cQuery += GetWhereQryAlias(self:cAlias)
		If lHasPK
			cPkFilter := FWAToS(self:oModel:GetPrimaryKey(),"||") + "=?"

			If (self:cAlias)->(FieldPos(PrefixoCpo(self:cAlias)+"_FILIAL")) > 0
				cPkFilter := PrefixoCpo(self:cAlias)+"_FILIAL||" + cPkFilter
			EndIf
			cQuery += " AND " + cPkFilter
		Endif

		If !Empty(self:cFilter)
			cQuery += " AND ( " + self:cFilter + " ) "
		EndIf
		
		If !Empty(self:qryOrder)
			cQuery += self:qryOrder
		EndIf

		oStatement := FWPreparedStatement():New(cQuery)
		If lHasPK
			oStatement:SetString(1, cPK)
		EndIf
		cQuery := oStatement:getFixQuery()
		cQuery := ChangeQuery(cQuery)

		MPSysOpenQuery(cQuery, @self:cQryAlias)

		oStatement:Destroy()
		FwFreeObj(oStatement)

		If self:lDebug .And. self:GetQSValue("showQuery") == "true"
			i18nConOut("[FWRESTMODELOBJECT] Query: #1#2", {CRLF, cQry})
		EndIf

		If !(self:cQryAlias)->(Eof())
			(self:cAlias)->(dbGoTo((self:cQryAlias)->R_E_C_N_O_))
			lRet := !(self:cAlias)->(Eof())
		EndIf
	Endif

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JModRst
Publica��o dos modelos que s�o disponibilizados no REST
@since 26/06/2019
/*/
//-------------------------------------------------------------------
Function JModRst()

Return lWSTLegal

	PUBLISH MODEL REST NAME JPRODUTO  SOURCE MATA010 RESOURCE OBJECT JurModRest    //-- Produto - MATA010
	PUBLISH MODEL REST NAME JRATEIO   SOURCE CTBA120 RESOURCE OBJECT JurModRest    //-- Rateio - CTBA120
	PUBLISH MODEL REST NAME JCRGS     SOURCE GPEA370 RESOURCE OBJECT JurModRest    //-- Cargo de Funcion�rios - GPEA370
	PUBLISH MODEL REST NAME JCLIDEP   SOURCE JURA148 RESOURCE OBJECT JurModRest    //-- Clientes - JURA148
	PUBLISH MODEL REST NAME JBANFIN   SOURCE MATA070 RESOURCE OBJECT JurModRest    //-- Banco - MATA070
	PUBLISH MODEL REST NAME JNATFIN   SOURCE FINA010 RESOURCE OBJECT JurModRest    //-- Natureza - FINA010
	PUBLISH MODEL REST NAME JCONPGFIN SOURCE MATA360 RESOURCE OBJECT JurModRest    //-- Condi��o de Pagamento - MATA360 (SE4)
	PUBLISH MODEL REST NAME JCCUSTO   SOURCE CTBA030 RESOURCE OBJECT JurModRest    //-- Centro de custo - CTBA030

	/* Publica��o dos modelos que s�o disponibilizados no REST */
	PUBLISH MODEL REST NAME JURA001 SOURCE JURA001 RESOURCE OBJECT JurModRest     //-- Natureza - JURA001
	PUBLISH MODEL REST NAME JURA003 SOURCE JURA003 RESOURCE OBJECT JurModRest     //-- Cadastro de relat�rios - JURA003
	PUBLISH MODEL REST NAME JURA004 SOURCE JURA004 RESOURCE OBJECT JurModRest     //-- Objeto/ Assunto - JURA004
	PUBLISH MODEL REST NAME JURA005 SOURCE JURA005 RESOURCE OBJECT JurModRest     //-- Comarcas - JURA005
	PUBLISH MODEL REST NAME JURA006 SOURCE JURA006 RESOURCE OBJECT JurModRest     //-- Comarcas - JURA005
	PUBLISH MODEL REST NAME JURA009 SOURCE JURA009 RESOURCE OBJECT JurModRest     //-- tipo de envolvimento - JURA009
	PUBLISH MODEL REST NAME JURA011 SOURCE JURA011 RESOURCE OBJECT JurModRest     //-- Fase processual - JURA011
	PUBLISH MODEL REST NAME JURA013 SOURCE JURA013 RESOURCE OBJECT JurModRest     //-- Motivo de encerramento - JURA013
	PUBLISH MODEL REST NAME JURA014 SOURCE JURA014 RESOURCE OBJECT JurModRest     //-- Configura��o de Relat�rio - JURA014
	PUBLISH MODEL REST NAME JURA016 SOURCE JURA016 RESOURCE OBJECT JurModRest     //-- Prepostos - JURA016
	PUBLISH MODEL REST NAME JURA017 SOURCE JURA017 RESOURCE OBJECT JurModRest     //-- Resultados de Follow-ups - JURA017
	PUBLISH MODEL REST NAME JURA021 SOURCE JURA021 RESOURCE OBJECT JurModRest     //-- Tipos de Follow-ups - JURA021
	PUBLISH MODEL REST NAME JURA022 SOURCE JURA022 RESOURCE OBJECT JurModRest     //-- Tipo de A��o - JURA022
	PUBLISH MODEL REST NAME JURA024 SOURCE JURA024 RESOURCE OBJECT JurModRest     //-- Tipos de Garantia - JURA024
	PUBLISH MODEL REST NAME JURA025 SOURCE JURA025 RESOURCE OBJECT JurModRest     //-- Motivo de altera��o - JURA025
	PUBLISH MODEL REST NAME JURA051 SOURCE JURA051 RESOURCE OBJECT JurModRest     //-- Ato processual - JURA051
	PUBLISH MODEL REST NAME JURA052 SOURCE JURA052 RESOURCE OBJECT JurModRest     //-- Cargo - JURA052
	PUBLISH MODEL REST NAME JURA061 SOURCE JURA061 RESOURCE OBJECT JurModRest     //-- Formas de Corre��o - JURA061
	PUBLISH MODEL REST NAME JURA085 SOURCE JURA085 RESOURCE OBJECT JurModRest     //-- Tipo de Objeto - JURA085
	PUBLISH MODEL REST NAME JURA087 SOURCE JURA087 RESOURCE OBJECT JurModRest     //-- Tipo de Despesa - JURA087
	PUBLISH MODEL REST NAME JURA093 SOURCE JURA093 RESOURCE OBJECT JurModRest     //-- Justificativa de altera��o de correspondente - JURA093
	PUBLISH MODEL REST NAME JURA094 SOURCE JURA094 RESOURCE OBJECT JurModRest     //-- Objetos/Pedidos - JURA094
	PUBLISH MODEL REST NAME JURA095 SOURCE JURA095 RESOURCE OBJECT JurModRest     //-- Processo - JURA095
	PUBLISH MODEL REST NAME JURA098 SOURCE JURA098 RESOURCE OBJECT JurModRest     //-- Garantias - JURA098
	PUBLISH MODEL REST NAME JURA099 SOURCE JURA099 RESOURCE OBJECT JurModRest     //-- Despesas - JURA099
	PUBLISH MODEL REST NAME JURA100 SOURCE JURA100 RESOURCE OBJECT JurModRest     //-- Andamentos - JURA100
	PUBLISH MODEL REST NAME JURA106 SOURCE JURA106 RESOURCE OBJECT JurModRest     //-- Follow-ups - JURA106
	PUBLISH MODEL REST NAME JURA107 SOURCE JURA107 RESOURCE OBJECT JurModRest     //-- Local de trabalho - JURA107
	PUBLISH MODEL REST NAME JURA132 SOURCE JURA132 RESOURCE OBJECT JurModRest     //-- Escrit�rio Correspondente/ Fornecedor (SA2) - JURA132
	PUBLISH MODEL REST NAME JURA133 SOURCE JURA133 RESOURCE OBJECT JurModRest     //-- Tipos de Aditivos de Contratos (NXZ) - JURA133
	PUBLISH MODEL REST NAME JURA134 SOURCE JURA134 RESOURCE OBJECT JurModRest     //-- Tipo de Contrato - JURA134
	PUBLISH MODEL REST NAME JURA158 SOURCE JURA158 RESOURCE OBJECT JurModRest     //-- Assuntos jur�dicos - JURA158
	PUBLISH MODEL REST NAME JURA163 SOURCE JURA163 RESOURCE OBJECT JurModRest     //-- Relaciona Pesquisa / Assunto jur�dico x Usu�rio - JURA163
	PUBLISH MODEL REST NAME JURA166 SOURCE JURA166 RESOURCE OBJECT JurModRest     //-- Justificativa - JURA166
	PUBLISH MODEL REST NAME JURA184 SOURCE JURA184 RESOURCE OBJECT JurModRest     //-- Parte Contr�ria - JURA184
	PUBLISH MODEL REST NAME JURA218 SOURCE JURA218 RESOURCE OBJECT JurModRest     //-- Usu�rios x Grupo - JURA218
	PUBLISH MODEL REST NAME JURA258 SOURCE JURA258 RESOURCE OBJECT JurModRest     //-- Redutores - JURA258
	PUBLISH MODEL REST NAME JURA269 SOURCE JURA269 RESOURCE OBJECT JurModRest     //-- Favoritos - JURA269
	PUBLISH MODEL REST NAME JURA270 SOURCE JURA270 RESOURCE OBJECT JurModRest     //-- Verbas Por Pedidos - JURA270
	PUBLISH MODEL REST NAME JURA276 SOURCE JURA276 RESOURCE OBJECT JurModRest     //-- Modelos de Configura��o para Exporta��o - JURA276
	PUBLISH MODEL REST NAME JURA018 SOURCE JURA018 RESOURCE OBJECT JurModRest     //-- Rito - JURA018
	PUBLISH MODEL REST NAME JURA279 SOURCE JURA279 RESOURCE OBJECT JurModRest     //-- Distribui��o - JURA279
	PUBLISH MODEL REST NAME JURA280 SOURCE JURA280 RESOURCE OBJECT JurModRest     //-- Notifica��es - JURA280
