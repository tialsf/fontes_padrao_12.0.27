#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOTVS.CH"
#INCLUDE "JURRESTFUN.CH"
#INCLUDE "RESTFUL.CH"

WSRESTFUL JurRESTFun DESCRIPTION STR0011 // "Fun��es do PFS"
	WSDATA LEGALDESK  AS STRING

	WSMETHOD POST WoTsCreate DESCRIPTION STR0012 PATH "wo-ts" // "Wo em lote de Timesheet"

END WSRESTFUL

//-------------------------------------------------------------------
/*/{Protheus.doc} POST WoTsCreate
Fun��o POST para executar a��es conforme par�metro passado na URL.

@author bruno.ritter
@since 25/04/2017
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD POST WoTsCreate HEADERPARAM LEGALDESK WSRESTFUL JurRESTFun
Local lRet       := .T.
Local cTpBody    := ""
Local cAccept    := Self:GetAccept()
Local aRetFun    := {.T.,{0,""},""}
Local lLegalDesk := .F.

If !Empty(SELF:LEGALDESK)
	lLegalDesk := Upper(SELF:LEGALDESK) == "TRUE"
EndIf

If lLegalDesk
	JurSetLD(lLegalDesk)

	// Identificando o tipo do Body
	If !Empty(cAccept) .AND. ValType(cAccept) == "C"
		cTpBody := Upper(Substr(cAccept, At("/",cAccept)+1))
	EndIf

	If Empty(cTpBody) .OR. (cTpBody != "JSON" .AND. cTpBody != "XML")
		SetRestFault(400, EncodeUTF8(STR0006)) // "N�o foi poss�vel ler o Body, � apenas aceito JSON ou XML"
		lRet := .F.
	EndIf

	// Executando a fun��o
	If lRet
		aRetFun := JurRtWoTs(Self, cTpBody)
	EndIf

	If !aRetFun[1]
		If Len(aRetFun[2]) > 1
			SetRestFault(aRetFun[2][1], EncodeUTF8(aRetFun[2][2]), , aRetFun[2][1], EncodeUTF8(aRetFun[2][2]))
		Else
			SetRestFault(500, EncodeUTF8(STR0004)) //"Erro n�o identificado"
		EndIf
		lRet := .F.
	Else
		Self:SetResponse(aRetFun[3])
	EndIf
Else
	lRet := .F.
EndIf

JurSetLD(.F.)

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JurRtWoTs(oWsRestFul, cTpBody)
Fun��o para executar WO em lote de Time Sheets.

@param oWsRestFul   - Objeto do Rest 'WSRESTFUL'
@param cTpBody      - Tipo do Body (JSON, XML)

@Return - aRet - aRet[1]Retorno se foi executado a fun��o de WO em Lote
				   aRet[2][1] C�digo de erro HTTP
				   aRet[2][2] Mensagem de erro
				   aRet[3] Retorno em XML ou JSON, dependedo do Body que foi enviado

@author bruno.ritter
@since 25/04/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function JurRtWoTs (oWsRestFul, cTpBody)
Local aRet      := {.T.,{0,""},""}
Local aRetFun   := {}
Local aCodTs    := {}
Local cCodMotv  := ""
Local cCodPart  := ""
Local cMsgWo    := ""
Local nI        := 1
Local nY        := 1
Local cErro     := ""
Local cAviso    := ""
Local oJson     := Nil
Local oXml      := Nil
Local oXmlParam := Nil
Local oXmlACdTs := Nil
Local cResp     := ''
Local cCodTS    := ""
Local cCodWo    := ""
Local cObs      := ""
Local nLenRetF  := 0
Local cContent  := ""

//------------------------------------------------------
// Leitura do Body
//------------------------------------------------------
cContent  := oWsRestFul:GetContent()

If !Empty(cContent)
	cContent :=  EncodeUtf8(cContent)

	// XML
	If cTpBody == "XML"

		oXml := XmlParser(cContent, "_",@cErro,@cAviso)
		If oXml <> Nil
			oXmlParam := XmlChildEx(oXml, "_PARAMETROS")
			If oXmlParam <> Nil

				// C�digo de Time Sheet
				oXmlACdTs := XmlChildEx(oXmlParam, "_ACODTS")
				If oXmlACdTs <> Nil .AND. XmlChildEx(oXmlACdTs, "_CCODTS") <> Nil
					If (ValType(oXmlACdTs:_CCODTS) == "A")
						For nI := 1 To Len(oXmlACdTs:_CCODTS)
							Aadd(aCodTs, oXmlACdTs:_CCODTS[nI]:TEXT)
						Next nI
					Else
						Aadd(aCodTs,  oXmlACdTs:_CCODTS:TEXT)
					EndIf
				EndIf

				// C�digo do Motivo de WO
				If XmlChildEx(oXmlParam, "_CCODMOTV") <> Nil
					cCodMotv := DecodeUTF8(oXmlParam:_CCODMOTV:TEXT)
				Else
					cCodMotv := ""
				EndIf

				// C�digo do Participante do WO
				If XmlChildEx(oXmlParam, "_CCODPART") <> Nil
					cCodPart := DecodeUTF8(oXmlParam:_CCODPART:TEXT)
				Else
					cCodPart := ""
				EndIf

				// Observa��o do WO
				If XmlChildEx(oXmlParam, "_CMSGWO") <> Nil
					cMsgWo := (oXmlParam:_CMSGWO:TEXT)
				Else
					cMsgWo := ""
				EndIf
			Else
				aRet[1]    := .F.
				aRet[2][1] := 400
				aRet[2][2] := STR0005 // "N�o foi poss�vel encontrar TAG de 'PARAMETROS' no XML"
			EndIf
		Else
			aRet[1]    := .F.
			aRet[2][1] := 400
			aRet[2][2] := STR0006 // "N�o foi poss�vel ler o Body, � apenas aceito JSON ou XML"
		EndIF
	EndIf

	//JSON
	If cTpBody == "JSON"
		oJson := JsonObject():New()
		oJson:fromJson(cContent)
		If ValType(oJson) == "J"
			aCodTs   := Iif(oJson['aCodTs'] <> Nil, oJson['aCodTs'], {})
			cCodMotv := Iif(oJson['cCodMotv'] <> Nil, DecodeUTF8(oJson['cCodMotv']), "")
			cCodPart := Iif(oJson['cCodPart'] <> Nil, DecodeUTF8(oJson['cCodPart']), "")
			cMsgWo   := Iif(oJson['cMsgWo'] <> Nil, DecodeUTF8(oJson['cMsgWo']), "")
		Else
			aRet[1]    := .F.
			aRet[2][1] := 400
			aRet[2][2] := STR0006 // "N�o foi poss�vel ler o Body, � apenas aceito JSON ou XML"
		EndIf
	EndIf

Else
	aRet[1]    := .F.
	aRet[2][1] := 400
	aRet[2][2] := STR0006 // "N�o foi poss�vel ler o Body, � apenas aceito JSON ou XML"
EndIf


//------------------------------------------------------
// Valida��o b�sica
//------------------------------------------------------
If aRet[1]
	// Observa��o do WO
	If aRet[1] .AND. Empty(cMsgWo)
		aRet[1]    := .F.
		aRet[2][1] := 400
		aRet[2][2] := STR0007 // "A observa��o do WO 'cMsgWo' � um campo obrigat�rio!"
	EndIf

	// C�digo do Participante do WO
	If aRet[1] .AND. (Empty(cCodPart) .OR. !ExistCpo("RD0",cCodPart,1))
		aRet[1]    := .F.
		aRet[2][1] := 400
		aRet[2][2] := STR0008 // "O c�digo de participante 'cCodPart' est� inv�lido!"
	EndIf

	// C�digo do Motivo de WO
	If aRet[1] .AND. (Empty(cCodMotv) .OR. !ExistCpo("NXV",cCodMotv,1))
		aRet[1]    := .F.
		aRet[2][1] := 400
		aRet[2][2] := STR0009 // "O c�digo do motivo de WO 'cCodMotv' est� inv�lido!"
	EndIf

	// C�digo de Time Sheet
	If aRet[1] .AND. Len(aCodTs) < 1
		aRet[1]    := .F.
		aRet[2][1] := 400
		aRet[2][2] := STR0010 // "� obrigat�rio informar ao menos um Time Sheet 'aCodTs'/'cCodTs'!"
	EndIf
EndIf

//------------------------------------------------------
// Execu��o da Fun��o
//------------------------------------------------------
If aRet[1]
	aRetFun  := Ja145WoTs(aCodTs, cCodMotv, cMsgWo, cCodPart)

	//------------------------------------------------------
	// Preparar o Retorno
	//------------------------------------------------------
	nLenRetF := Len(aRetFun)
	If cTpBody == "XML" // Criando XML do retorno
		oWsRestFul:SetContentType("application/xml")
		cResp := "<?xml version='1.0' encoding='UTF-8'?>"
		cResp += "<totvs_total>"+AllTrim(Str(nLenRetF))+"</totvs_total>"
		cResp += "<hasNext>false</hasNext>"
		cResp += "<result>"
		For nY := 1 To nLenRetF
			cCodTS := Iif( Len(aRetFun[nY]) > 0, EncodeUTF8(aRetFun[nY][1]), "" )
			cCodWo := Iif( Len(aRetFun[nY]) > 1, EncodeUTF8(aRetFun[nY][2]), "" )
			cObs   := Iif( Len(aRetFun[nY]) > 2, EncodeUTF8(aRetFun[nY][3]), "" )

			cResp += "<return>"
			cResp +=    "<codigoTS>"+cCodTS+"</codigoTS>"
			cResp +=    "<codigoWO>"+cCodWo+"</codigoWO>"
			cResp +=    "<obs>"+cObs+"</obs>"
			cResp += "</return>"
		Next nY
		cResp += "</result>"

	Else // Criando JSON do retorno
		oWsRestFul:SetContentType("application/json")

		cResp := '{'
		cResp += '"totvs_total":'+AllTrim(Str(nLenRetF))+','
		cResp += '"hasNext":false,'
		cResp += '"return":['

		For nY := 1 To nLenRetF
			cCodTS := Iif( Len(aRetFun[nY]) > 0, EncodeUTF8(aRetFun[nY][1]), "" )
			cCodWo := Iif( Len(aRetFun[nY]) > 1, EncodeUTF8(aRetFun[nY][2]), "" )
			cObs   := Iif( Len(aRetFun[nY]) > 2, EncodeUTF8(aRetFun[nY][3]), "" )
			cResp += '{'
			cResp +=    '"codigoTS":"'+cCodTS+'",'
			cResp +=    '"codigoWO":"'+cCodWo+'",'
			cResp +=    '"obs":"'+cObs+'"'
			cResp += '}'
			cResp += Iif(nY == nLenRetF, ']',',')
		Next nY
		cResp +=  '}'

	EndIf
	aRet[3] := cResp
EndIf

Return aRet
