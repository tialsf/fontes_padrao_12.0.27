#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDef.ch"
//#INCLUDE "JurLog.CH"

#DEFINE TYPE_DEFAULT		1
#DEFINE TYPE_DBF				2
#DEFINE TYPE_MSG				3

Static lDbfCreated := .F.

//-------------------------------------------------------------------
/*/{Protheus.doc} JurLog
CLASS JurLog

@author Felipe Bonvicini Conti
@since 04/03/13
@version 1.0
-------------------------------------------------------------------*/
Function __JurLog() // Function Dummy
	ApMsgInfo( 'JurLog -> Utilizar Classe ao inves da funcao' )
Return NIL

CLASS JurLog From FWSerialize

	DATA cText
	DATA nType // 1 - Tela de Log Padr�o / 2 - Log extendido / X - MsgInfo()
	DATA cAlias

	METHOD New(cText, nType) CONSTRUCTOR
	METHOD SetText(cText)
	METHOD GetText()
	METHOD SetType(nType)
	METHOD GetType()
	METHOD ShowLog()
	METHOD Destroy()

ENDCLASS

METHOD New(cText, nType) Class JurLog

	Self:cText := "" // Isto serve para zerar a propiedade antes de acrescentar texto a mesma.
	Self:SetText(cText)
	Self:SetType(nType)

	Self:cAlias := ""

Return Self


METHOD SetText(cText) Class JurLog
Default cText := ""

	If Valtype(cText) == "C"

		Do Case
			Case Self:GetType() == TYPE_DEFAULT
				Self:cText += cText + CRLF

			Case Self:GetType() == TYPE_DBF
			
				If lDbfCreated
		
					RecLock(Self:cAlias, .T.)
					(Self:cAlias)->XML := cText
					(Self:cAlias)->(MsUnlock())
					(Self:cAlias)->(dbCommitAll())

					Self:cText := cText

				EndIf

			End Case			

	EndIf

Return (Self:cText == cText)

METHOD GetText() Class JurLog
Local cRet := ""

	Do Case
		Case Self:GetType() == TYPE_DBF
		
			If lDbfCreated

				(Self:cAlias)->(dbGoTop())
				While !(Self:cAlias)->(Eof())
					cRet += (Self:cAlias)->XML + CRLF
					(Self:cAlias)->(dbSkip())
				End
				
			EndIf

		OtherWise
			cRet := Self:cText
		
		End Case	

Return cRet


METHOD SetType(nType) Class JurLog

Default nType := TYPE_DEFAULT

	If Valtype(nType) == "N"
		Self:nType := nType
		
		If Self:GetType() == TYPE_DBF .And. !lDbfCreated // Dbf
			lDbfCreated := JLogNewDbf(@Self:cAlias)
		EndIf
		
	EndIf

Return (Self:nType == nType)

METHOD GetType() Class JurLog
Return Self:nType


METHOD ShowLog() Class JurLog

	Do Case 
		Case Self:GetType() == TYPE_DEFAULT // Tela de Log padr�o
			AutoGrLog(Self:GetText())
			MostraErro()

		Case Self:GetType() == TYPE_DBF // Mostra Log gravado em dbf.
			JLogShow(Self:cAlias)

		OtherWise // MsgInfo
			MsgInfo(Self:GetText())

	End Case

Return Nil

METHOD Destroy() Class JurLog
Local aArea := GetArea()
Local nI

	If Self:GetType() == TYPE_DBF // Mostra Log gravado em dbf.

		If FILE(Self:cAlias+GetDbExtension())

			If Select(Self:cAlias)==0
				DbUseArea(.T.,"DBFCDX",Self:cAlias,Self:cAlias,.T.,.F.)
				If NetErr()
					ConOut("Problema ao abrir tabela temporaria")
				EndIf
			EndIF
	
			(Self:cAlias)->(dbCloseArea())
			For nI := 1 to 10000
				FErase(Self:cAlias+".fpt")
				If FILE(Self:cAlias+GetDbExtension())

					If FCLOSE( FOpen(Self:cAlias+GetDbExtension() , 264 ))
						If FErase(Self:cAlias+GetDbExtension()) == -1
							ConOut('JurLog: Falha na dele��o do Arquivo ( FError'+str(ferror(),4)+') - ' + allTrim(Str(nI)) + " / ID: " + GetThredId())
						Else
							ConOut("JurLog: Killed! / Self:cAlias == "+Self:cAlias+" / ID: " + GetThredId() + "( FError"+str(ferror(),4)+")")
							Exit
						EndIf
					EndIF

				Else
					Exit
				EndIf
				Sleep(10000)
			Next

		EndIF

	EndIf

	RestArea(aArea)

	FreeObj(Self)

Return Nil

Static Function JLogNewDbf(cAlias)
Local aStruct      := {}
Local cDbExtension := GetDbExtension()
	
	If !FILE(cAlias + cDbExtension)

		cAlias := GetFileTMP(__cUserID) 

		Aadd( aStruct, {"XML" ,"M",10,0}) // Este campo por ser MEMO, deve ser o ultimo campo!!!! (Por causa do CTREE) - Felipe Conti
	
		dbCreate( cAlias + cDbExtension, aStruct, "DBFCDX")

	EndIf

Return Select(cAlias) > 0

Static Function GetFileTMP(cCodUser)
Local cRet := AllTrim(UPPER("JurLog" + cCodUser + StrZero(Randomize( Randomize(01,45),Randomize(46,99) ), 2)))

	While FILE(cRet+GetDbExtension())
		cRet := AllTrim(UPPER("JurLog" + cCodUser + StrZero(Randomize( Randomize(01,45),Randomize(46,99) ), 2)))
	End

Return cRet

Static Function JLogShow(cAlias)

	

Return Nil

Static Function GetThredId()
Return cValToChar(ThreadID())