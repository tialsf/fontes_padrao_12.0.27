object frmCorretorOrtografico: TfrmCorretorOrtografico
  Left = 220
  Top = 183
  Width = 601
  Height = 430
  Caption = 'Corretor Ortogr'#225'fico'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object tlbPrincipal: TToolBar
    Left = 0
    Top = 0
    Width = 593
    Height = 23
    AutoSize = True
    ButtonHeight = 21
    ButtonWidth = 45
    Caption = 'tlbPrincipal'
    Flat = True
    ParentShowHint = False
    ShowCaptions = True
    ShowHint = True
    TabOrder = 0
    object btnVerificar: TToolButton
      Left = 0
      Top = 0
      Caption = 'Verificar'
      ImageIndex = 0
      OnClick = btnVerificarClick
    end
    object ToolButton2: TToolButton
      Left = 45
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object btnFechar: TToolButton
      Left = 53
      Top = 0
      Caption = 'Fechar'
      ImageIndex = 1
      OnClick = btnFecharClick
    end
  end
  object stbPrincipal: TStatusBar
    Left = 0
    Top = 377
    Width = 593
    Height = 19
    Panels = <>
  end
  object mmPrincipal: TRichEdit
    Left = 0
    Top = 23
    Width = 593
    Height = 354
    Align = alClient
    TabOrder = 2
  end
  object Spell: TAddictAutoLiveSpell
    ConfigStorage = csRegistry
    ConfigID = '%UserName%'
    ConfigFilename = '%AppDir%\Spell.cfg'
    ConfigRegistryKey = 'Software\Addictive Software\%AppName%'
    ConfigDictionaryDir.Strings = (
      '%AppDir%')
    ConfigAvailableOptions = [soLiveSpelling, soLiveCorrect, soUpcase, soNumbers, soPrimaryOnly, soRepeated, soDUalCaps]
    ConfigUseMSWordCustom = True
    ConfigDefaultMain.Strings = (
      'American.adm')
    ConfigDefaultActiveCustom = '%ConfigID%.adu'
    ConfigDefaultOptions = [soLiveSpelling, soLiveCorrect, soInternet, soAbbreviations, soRepeated, soDUalCaps]
    ConfigDefaultUseMSWordCustom = True
    SuggestionsAutoReplace = False
    SuggestionsLearning = True
    SuggestionsLearningDict = '%AppDir%\%UserName%_sp.adl'
    LiveSpelling = True
    LiveCorrect = True
    QuoteChars = '>'
    DialogInitialPos = ipLastUserPos
    DialogSelectionAvoid = saAvoid
    DialogShowImmediate = False
    DialogShowModal = False
    EndMessage = emExceptCancel
    EndCursorPosition = epOriginal
    EndMessageWordCount = False
    MaxUndo = -1
    MaxSuggestions = -1
    KeepDictionariesActive = False
    SynchronousCheck = True
    UseHourglassCursor = True
    CommandsVisible = [sdcIgnore, sdcIgnoreAll, sdcChange, sdcChangeAll, sdcAdd, sdcAutoCorrect, sdcUndo, sdcHelp, sdcCancel, sdcOptions, sdcCustomDictionary, sdcCustomDictionaries, sdcConfigOK, sdcAddedEdit, sdcAutoCorrectEdit, sdcExcludedEdit, sdcInternalEdit, sdcMainDictFolderBrowse, sdcResetDefaults]
    CommandsEnabled = [sdcIgnore, sdcIgnoreAll, sdcChange, sdcChangeAll, sdcAdd, sdcAutoCorrect, sdcUndo, sdcHelp, sdcCancel, sdcOptions, sdcCustomDictionary, sdcCustomDictionaries, sdcConfigOK, sdcAddedEdit, sdcAutoCorrectEdit, sdcExcludedEdit, sdcInternalEdit, sdcMainDictFolderBrowse, sdcResetDefaults]
    PhoneticSuggestions = True
    PhoneticMaxDistance = 4
    PhoneticDivisor = 2
    PhoneticDepth = 2
    MappingAutoReplace = True
    UseExcludeWords = True
    UseAutoCorrectFirst = False
    RecheckReplacedWords = True
    ResumeFromLastPosition = True
    AllowedCases = cmInitialCapsOrUpcase
    UILanguage = ltBrPort
    UIType = suiDialog
    UILanguageFontControls.Charset = DEFAULT_CHARSET
    UILanguageFontControls.Color = clWindowText
    UILanguageFontControls.Height = -11
    UILanguageFontControls.Name = 'MS Sans Serif'
    UILanguageFontControls.Style = []
    UILanguageFontText.Charset = DEFAULT_CHARSET
    UILanguageFontText.Color = clWindowText
    UILanguageFontText.Height = -11
    UILanguageFontText.Name = 'MS Sans Serif'
    UILanguageFontText.Style = []
    UILanguageUseFonts = False
    DoubleBuffered = True
    LiveSpellingColor = clRed
    LiveSpellingReadOnly = False
    LiveControl = mmPrincipal
    LiveMenuOptions = [spDialog, spAutoCorrect, spChangeAll, spAdd, spIgnoreAll, spIgnore, spReplace]
    LiveSpellingOptions = True
    AutoSpellWhenFocused = False
    AutoSpellOnlyFocusedControl = False
    Left = 104
    Top = 47
  end
end
