unit CorretorOrtograficaoF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ToolWin, ad3SpellBase, ad3Spell,
  ad3LiveBase, ad3LiveAutospell;

type
  TfrmCorretorOrtografico = class(TForm)
    tlbPrincipal: TToolBar;
    stbPrincipal: TStatusBar;
    mmPrincipal: TRichEdit;
    btnVerificar: TToolButton;
    ToolButton2: TToolButton;
    btnFechar: TToolButton;
    Spell: TAddictAutoLiveSpell;
    procedure btnFecharClick(Sender: TObject);
    procedure btnVerificarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCorretorOrtografico: TfrmCorretorOrtografico;

implementation

{$R *.dfm}

procedure TfrmCorretorOrtografico.btnFecharClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TfrmCorretorOrtografico.btnVerificarClick(Sender: TObject);
begin
  Spell.CheckWinControl(mmPrincipal,ctSmart);
end;

end.
