library SIGAADDICT;

uses
  SysUtils,
  Classes,
  Dialogs,
  ad3SpellBase in '..\Repositorio\FrameWork\Componentes\Addict3\ad3spellbase.pas',
  ad3ParserBase in '..\Repositorio\FrameWork\Componentes\Addict3\ad3ParserBase.pas',
  ad3MainDictionary in '..\Repositorio\FrameWork\Componentes\Addict3\ad3MainDictionary.pas',
  ad3Util in '..\Repositorio\FrameWork\Componentes\Addict3\ad3Util.pas',
  ad3PhoneticsMap in '..\Repositorio\FrameWork\Componentes\Addict3\ad3PhoneticsMap.pas',
  ad3CustomDictionary in '..\Repositorio\FrameWork\Componentes\Addict3\ad3CustomDictionary.pas',
  ad3Ignore in '..\Repositorio\FrameWork\Componentes\Addict3\ad3Ignore.pas',
  ad3ParseEngine in '..\Repositorio\FrameWork\Componentes\Addict3\ad3ParseEngine.pas',
  ad3Configuration in '..\Repositorio\FrameWork\Componentes\Addict3\ad3Configuration.pas',
  ad3Suggestions in '..\Repositorio\FrameWork\Componentes\Addict3\ad3Suggestions.pas',
  ad3ConfigurationDialogCtrl in '..\Repositorio\FrameWork\Componentes\Addict3\ad3ConfigurationDialogCtrl.pas',
  ad3SpellLanguages in '..\Repositorio\FrameWork\Componentes\Addict3\ad3SpellLanguages.pas',
  ad3WinAPIParser in '..\Repositorio\FrameWork\Componentes\Addict3\ad3WinAPIParser.pas',
  ad3StringParser in '..\Repositorio\FrameWork\Componentes\Addict3\ad3StringParser.pas',
  ad3Spell in '..\Repositorio\FrameWork\Componentes\Addict3\ad3Spell.pas',
  ad3ConfigurationDialog in '..\Repositorio\FrameWork\Componentes\Addict3\ad3ConfigurationDialog.pas' {SpellCheckConfigDialog},
  ad3CustomDictionariesDialog in '..\Repositorio\FrameWork\Componentes\Addict3\ad3CustomDictionariesDialog.pas' {CustomDictionariesDialog},
  ad3NewCustomDictionaryDialog in '..\Repositorio\FrameWork\Componentes\Addict3\ad3NewCustomDictionaryDialog.pas' {NewCustomDictionary},
  ad3CustomDictionaryEditDialog in '..\Repositorio\FrameWork\Componentes\Addict3\ad3CustomDictionaryEditDialog.pas' {CustomDictionaryEditDialog},
  ad3SpellDialog in '..\Repositorio\FrameWork\Componentes\Addict3\ad3SpellDialog.pas' {SpellDialog},
  ad3LiveBase in '..\Repositorio\FrameWork\Componentes\Addict3\ad3LiveBase.pas',
  ad3LiveAutospell in '..\Repositorio\FrameWork\Componentes\Addict3\ad3LiveAutospell.pas',
  ad3RichEditSubclass in '..\Repositorio\FrameWork\Componentes\Addict3\ad3RichEditSubclass.pas',
  ad3LiveRichEdit in '..\Repositorio\FrameWork\Componentes\Addict3\ad3LiveRichEdit.pas',
  CorretorOrtograficaoF in 'CorretorOrtograficaoF.pas' {frmCorretorOrtografico};

{$R *.res}

{
ExecInClientDLL - Rotina p�blica da biblioteca para interface com outras aplica��es (Protheus).
                  Atrav�s destas rotina se tem acesso as demais por meio de parametros
}

function CorretorOrtografico(plcTexto: String): String;
begin
  Result := plcTexto;
  frmCorretorOrtografico := TfrmCorretorOrtografico.Create(nil);
  try
    frmCorretorOrtografico.mmPrincipal.Lines.Text := plcTexto;
    if frmCorretorOrtografico.ShowModal = 1 then //mrOk = 1
    begin
      Result := frmCorretorOrtografico.mmPrincipal.Lines.GetText;
    end;
  finally
    FreeAndNil(frmCorretorOrtografico);
  end;
end;

{
Utilizado para fazer interface com o Protheus
Parametros
aFuncID - C�digo da fun��o que deve ser executada (Obrigat�rio)
aParams - Lista de parametros para a procedure que ser� chamada (N�o obrigat�rio)
aBuff - Complemento dos param�tros (N�o obrigat�rio)
aBuffSize - Tamanho do Buffer de complemento (aBuff) (N�o obrigat�rio)
Retorno
aBuff - pode ser utilizado para retornar valores para o Protheus
}
function ExecInClientDLL(aFuncID: Integer;
                         aParams: PChar;
                         aBuff: PChar;
                         aBuffSize: Integer ): integer; stdcall;
var
  vDoc : String;
begin
  Result := -1;
  try
    if aFuncID = 1 then  //Chama tela do corretor ortografico
    begin
      vDoc := trim(aParams);
      vDoc := CorretorOrtografico(vDoc);
      strCopy(aBuff, PChar(vDoc));
      if trim(vDoc) <> trim(aParams) then
      begin
        Result := 1;
      end
      else
        Result := 0;
    end;
  except
    on e: exception do
      MessageDlg('Erro: '+e.Message, mtInformation,[mbOk], 0);
  end;
end;

exports ExecInClientDLL;

begin
end.
