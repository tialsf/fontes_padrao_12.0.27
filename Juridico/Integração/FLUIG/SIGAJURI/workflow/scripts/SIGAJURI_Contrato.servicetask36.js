function servicetask36(attempt, message) {
var constraints = new Array();
var numSolic = getValue("WKNumProces");
var sAssLista = hAPI.getCardValue("sAssLista");
var cdDocAsign = hAPI.getCardValue("cdDocAsign")
var cErrorMessage = ""
	log.info("*** ServiceTask Contrato : Inicio Valida Assinatura! Tentativa:" + attempt);
	// Chama o dataSet de upload manual para sincronizar com a Vertsign
	DatasetFactory.getDataset("ds_upload_vertsign_manual", null, null, null);
	
	log.info("*** Contrato Valida Assinatura: Numero da Solicitação: " + numSolic);
	log.info("*** Contrato Valida Assinatura: Lista de Assinadores: " + sAssLista);
	log.info("*** Contrato Valida Assinatura: Código Documento:" + cdDocAsign);
	
	var dsFormAux = null;
	var constraints = new Array();
	var dataHoje = new Date()
	var fields = new Array("documentPK.documentId","documentPK.version","parentDocumentId","documentDescription", "statusAssinatura");
	var horaEnv = dataHoje.toLocaleTimeString('pt-BR')
	var dataEnv = dataHoje.toLocaleDateString('pt-BR');
	var lastVersion = -1;
	
	log.info("*** beforeStateEntry Contrato: Enviando documento para VertSign. CodAnexo[" + cdDocAsign + "]");
	constraints.push(DatasetFactory.createConstraint("codArquivo", cdDocAsign, cdDocAsign, ConstraintType.MUST));
	
	try {	
		dsFormAux = DatasetFactory.getDataset("ds_form_aux_vertsign", fields, constraints, null);
		
		if (dsFormAux.rowsCount > 0){
			
			if (dsFormAux.getValue(0,"statusAssinatura") == "Assinado"){
				return true;
			} else {
				if (dsFormAux.getValue(0,"statusAssinatura") == "Pendente Assinatura"){
					cErrorMessage = "O documento ainda não foi assinado por todos os responsáveis. Status: "  + dsFormAux.getValue(0,"statusAssinatura");
				} else {
					cErrorMessage = "O documento foi recusado por um dos responsáveis. Status: "  + dsFormAux.getValue(0,"statusAssinatura");
				}
				
				throw cErrorMessage;
			}
		} else {
			cErrorMessage = "O status do Documento é: Pendente Assinatura."
			throw cErrorMessage;
		}
		
	}catch(e){
		log.error("*** ServiceTask Contrato: " + cErrorMessage);
		throw cErrorMessage
	}
	log.info("*** ServiceTask Contrato : Fim Valida Assinatura!");
}