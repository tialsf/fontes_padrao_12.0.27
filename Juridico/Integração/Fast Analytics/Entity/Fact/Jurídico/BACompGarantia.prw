#INCLUDE "BADEFINITION.CH"

NEW ENTITY COMPGARANTIA

//-------------------------------------------------------------------
/*/{Protheus.doc} COMPGARANTIA
Visualiza a compara��o de Garantias entre marcas

@since   01/02/2018
/*/
//-------------------------------------------------------------------
Class BACompGarantia from BAEntity
	Method Setup( ) CONSTRUCTOR
	Method BuildQuery( )
EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} Setup
Construtor padr�o.

@since   01/02/2018
/*/
//-------------------------------------------------------------------
Method Setup( ) Class BACompGarantia
	_Super:Setup("AuditoriaCompGarantia", FACT, "O0H")

	
	//---------------------------------------------------------
	// Define que a extra��o da entidade ser� por m�s
	//---------------------------------------------------------
	_Super:SetTpExtr( BYMONTH )
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} BuildQuery
Constr�i a query da entidade.

@return aQuery, array, Retorna as consultas da entidade por empresa.

@since   01/02/2018
/*/
//-------------------------------------------------------------------
Method BuildQuery( ) Class BACompGarantia
	Local cQuery := ""
	
	cQuery := "SELECT BK_FILIAL " +;
	                ",BK_PROCESSO_JUR " +;
	                ",<<KEY_O0E_M1.O0E_MARCA>> AS MARCA1 " +;
	                ",<<KEY_O0E_M2.O0E_MARCA>> AS MARCA2 " +;
	                ",O0H_DATA " +;
	                ",O0H_VALOR " +;
	                ",O0H_VLRATU " +;
	                ",O0H_VJPROV " +;
	                ",O0H_VCPROV " +;
	                ",O0H_LEVANT " +;
	                ",O0H_COD " +;
	                ",O0H_EMBREC " +;
	                ",BK_FORMA_CORRECAO " +;
	                ",BK_TIPO_GARANTIA " +;
	                ",MOVIMENTACAO " +;
              "FROM ( SELECT A.BK_FILIAL BK_FILIAL, " +;
	                        "COALESCE(A.BK_PROCESSO_JUR,B.BK_PROCESSO_JUR) BK_PROCESSO_JUR, " +;
	                        "COALESCE(A.BK_MARCA,M.MARCA1) MARCA1, " +;
	                        "B.BK_MARCA MARCA2, " +;
	                        "COALESCE(B.O0H_VALOR,0) - COALESCE(A.O0H_VALOR,0) O0H_VALOR, " +;
	                        "COALESCE(B.O0H_VLRATU,0) - COALESCE(A.O0H_VLRATU,0) O0H_VLRATU, " +;
	                        "COALESCE(B.O0H_VJPROV,0) - COALESCE(A.O0H_VJPROV,0) O0H_VJPROV, " +;
	                        "COALESCE(B.O0H_VCPROV,0) - COALESCE(A.O0H_VCPROV,0) O0H_VCPROV, " +;
	                        "COALESCE(B.O0H_LEVANT,0) - COALESCE(A.O0H_LEVANT,0) O0H_LEVANT, " +;
	                        "B.O0H_DATA, " +;
	                        "B.BK_TIPO_GARANTIA, " +;
	                        "B.O0H_COD, " +;
	                        "B.O0H_EMBREC, " +;
	                        "B.BK_FORMA_CORRECAO, " +;
	                        "CASE " +;
	                          "WHEN COALESCE(B.O0H_VLRATU,0) = A.O0H_VLRATU AND COALESCE(B.O0H_LEVANT,0) = A.O0H_LEVANT THEN 'Sem Alteracao' " +;
	                          "WHEN COALESCE(NULLIF(COALESCE(B.O0H_VLRATU,0),0),B.O0H_VALOR) - B.O0H_LEVANT <= 0 THEN 'Baixa total' " +;
	                          "WHEN COALESCE(B.O0H_LEVANT,0) > 0 AND B.O0H_VLRATU - B.O0H_LEVANT > 0 THEN 'Baixa parcial' " +;
	                          "WHEN COALESCE(A.O0H_COD,' ') = ' ' AND B.O0H_VALOR > 0 THEN 'Adicao de valor' " +;
	                          "WHEN B.O0H_VCPROV > A.O0H_VCPROV OR B.O0H_VJPROV > A.O0H_VJPROV THEN 'Correcao monetaria' " +;
	                          "ELSE '0' " +;
	                        "END MOVIMENTACAO " +;
	                 "FROM ( SELECT DISTINCT A1.O0E_MARCA MARCA1 " +;
	                                       ",B1.O0E_MARCA MARCA2 " +;
	                        "FROM (SELECT O0E_MARCA " +;
	                              "FROM <<O0E_COMPANY>> " +; 
	                              "WHERE D_E_L_E_T_ =' ') A1, " +;
	                             "(SELECT O0E_MARCA " +;
	                              "FROM <<O0E_COMPANY>> " +;
	                              "WHERE D_E_L_E_T_ =' ') B1 " +;
	                        "WHERE B1.O0E_MARCA > A1.O0E_MARCA " +;
	                        dateFilter() +;
	                        " ) M " +;
	                        "RIGHT JOIN ( " +;
	                        "SELECT <<KEY_FILIAL_O0H_FILPRO>> AS BK_FILIAL, " +; 
	                               "<<KEY_NSZ_O0H_FILPRO+O0H_CAJURI>> AS BK_PROCESSO_JUR, " +;
	                               "<<KEY_NQW_O0H_CTPGAR>> AS BK_TIPO_GARANTIA, " +;
	                               "<<KEY_NW7_O0H_CCOMON>> AS BK_FORMA_CORRECAO, " +;
	                               "O0H_MARCA BK_MARCA, " +;
	                               "COALESCE(NULLIF(O0H_DATA,''),'19010101') O0H_DATA, " +;
	                               "O0H_VALOR, " +;
	                               "O0H_VLRATU, " +;
	                               "O0H_VJPROV, " +;
	                               "O0H_VCPROV, " +;
	                               "O0H_LEVANT, " +;
	                               "O0H_COD, " +;
	                               "O0H_EMBREC " +;
	                        "FROM <<O0H_COMPANY>> O0H LEFT JOIN <<NQW_COMPANY>> NQW ON O0H_CTPGAR = NQW_COD " +;
	                                                                              "AND NQW.D_E_L_E_T_ = ' ' " +;
	                        "WHERE O0H.D_E_L_E_T_ = ' ') A ON (A.BK_MARCA = M.MARCA1) " +;
	                        "RIGHT JOIN ( " +;
	                        "SELECT <<KEY_FILIAL_O0H_FILPRO>> AS BK_FILIAL, " +; 
	                               "<<KEY_NSZ_O0H_FILPRO+O0H_CAJURI>> AS BK_PROCESSO_JUR, " +;
	                               "<<KEY_NQW_O0H_CTPGAR>> AS BK_TIPO_GARANTIA, " +;
	                               "<<KEY_NW7_O0H_CCOMON>> AS BK_FORMA_CORRECAO, " +;
	                               "O0H_MARCA BK_MARCA, " +;
	                               "COALESCE(NULLIF(O0H_DATA,''),'19010101') O0H_DATA, " +;
	                               "O0H_VALOR, " +;
	                               "O0H_VLRATU, " +;
	                               "O0H_VJPROV, " +;
	                               "O0H_VCPROV, " +;
	                               "O0H_LEVANT, " +;
	                               "O0H_COD, " +;
	                               "O0H_EMBREC " +;
	                        "FROM <<O0H_COMPANY>> O0H LEFT JOIN <<NQW_COMPANY>> NQW ON O0H_CTPGAR = NQW_COD " +;   
	                                                                              "AND NQW.D_E_L_E_T_ = ' ' " +;
	                        "WHERE O0H.D_E_L_E_T_ = ' ') B ON (A.BK_PROCESSO_JUR = B.BK_PROCESSO_JUR " +;
	                                                      "AND A.O0H_COD = B.O0H_COD " +;
	                                                      "AND B.BK_MARCA = M.MARCA2) " +;
              "UNION " +;
              "SELECT <<KEY_FILIAL_O0H_FILPRO>> AS BK_FILIAL, " +; 
                     "<<KEY_NSZ_O0H_FILPRO+O0H_CAJURI>> AS BK_PROCESSO_JUR, " +;
                     "M.MARCA1, " +;
                     "M.MARCA2, " +;
                     "COALESCE(C.O0H_VALOR,0) O0H_VALOR, " +;
                     "COALESCE(C.O0H_VLRATU,0) O0H_VLRATU, " +;
                     "COALESCE(C.O0H_VJPROV,0) O0H_VJPROV, " +;
                     "COALESCE(C.O0H_VCPROV,0) O0H_VCPROV, " +;
                     "COALESCE(C.O0H_LEVANT,0) O0H_LEVANT, " +;
                     "COALESCE(NULLIF(C.O0H_DATA,''),'19010101') O0H_DATA, " +;
                     "<<KEY_NQW_O0H_CTPGAR>> AS BK_TIPO_GARANTIA, " +;
                     "C.O0H_COD, " +;
                     "C.O0H_EMBREC, " +;
                     "<<KEY_NW7_O0H_CCOMON>> AS BK_FORMA_CORRECAO, " +;
                     "CASE " +;
                       "WHEN COALESCE(NULLIF(COALESCE(C.O0H_VLRATU,0),0),C.O0H_VALOR) - COALESCE(C.O0H_LEVANT,0) <= 0 THEN 'Novo Baixa total' " +;
                       "WHEN COALESCE(C.O0H_LEVANT,0) > 0 AND COALESCE(NULLIF(C.O0H_VLRATU,0),C.O0H_VALOR) - COALESCE(C.O0H_LEVANT,0) > 0 THEN 'Novo Baixa parcial' " +;
                       "WHEN COALESCE(C.O0H_VALOR,0) > 0 THEN 'Adicao de valor' " +;
                       "ELSE '0' " +;
                     "END MOVIMENTACAO " +;
              "FROM ( SELECT DISTINCT A1.O0E_MARCA MARCA1 " +;
                                    ",B1.O0E_MARCA MARCA2 " +;
                     "FROM ( SELECT O0E_MARCA " +;
                            "FROM <<O0E_COMPANY>> " +; 
                            "WHERE D_E_L_E_T_ =' ') A1, " +;
                          "( SELECT O0E_MARCA " +;
                            "FROM <<O0E_COMPANY>> " +; 
                            "WHERE D_E_L_E_T_ =' ') B1 " +;
                     "WHERE B1.O0E_MARCA > A1.O0E_MARCA " +;
                     dateFilter() +;
                     ") M JOIN <<O0H_COMPANY>> C ON C.O0H_MARCA = M.MARCA2 " +; 
                                                "AND NOT EXISTS ( SELECT 1 " +;
                                                                 "FROM <<O0H_COMPANY>> " +;
                                                                 "WHERE O0H_MARCA = M.MARCA1 " +; 
                                                                   "AND O0H_COD = C.O0H_COD " +;
                                                                   "AND O0H_FILPRO = C.O0H_FILPRO) " +; 
	                ") F JOIN <<O0E_COMPANY>> M1 ON (F.MARCA1 = M1.O0E_MARCA) " +;
	                    "JOIN <<O0E_COMPANY>> M2 ON (F.MARCA2 = M2.O0E_MARCA) " +;
              "WHERE F.MARCA1 IS NOT NULL AND F.MARCA2 IS NOT NULL " +;
              " <<AND_XFILIAL_O0H_FILIAL>> "

Return cQuery

//-------------------------------------------------------------------
/*/{Protheus.doc} dateFilter
Cria o filtro de data

@return cReturn - Filtro de Datas

@since   06/02/2018
/*/
//-------------------------------------------------------------------
Static Function dateFilter()
Local cReturn := ""
Local cDatabase  := Upper( SGDB() )
	
	If "ORACLE" $ cDatabase
		cReturn := "AND SUBSTRING(B1.O0E_MARCA,1,6) = SUBSTR(<<FINAL_DATE>>,1,6) "
		cReturn += "AND SUBSTRING(A1.O0E_MARCA,1,6) IN ( " 
		cReturn +=     "to_char(add_months(to_date(B1.O0E_MARCA,'yyyymmdd'),-1),'yyyymm'), "
		cReturn +=     "to_char(add_months(to_date(B1.O0E_MARCA,'yyyymmdd'),-2),'yyyymm'), " 
		cReturn +=     "to_char(add_months(to_date(B1.O0E_MARCA,'yyyymmdd'),-3),'yyyymm'), " 
		cReturn +=     "to_char(add_months(to_date(B1.O0E_MARCA,'yyyymmdd'),-6),'yyyymm'), " 
		cReturn +=     "to_char(add_months(to_date(B1.O0E_MARCA,'yyyymmdd'),-11),'yyyymm'), "
		cReturn +=     "to_char(add_months(to_date(B1.O0E_MARCA,'yyyymmdd'),-12),'yyyymm')) "
	 Elseif "POSTGRES" $ cDatabase
		cReturn := "AND SUBSTRING(B1.O0E_MARCA,1,6) = SUBSTRING(<<FINAL_DATE>>,1,6) "
		cReturn += "AND SUBSTRING(A1.O0E_MARCA,1,6) IN ( "
		cReturn +=     "TO_CHAR(TO_DATE(B1.O0E_MARCA,'YYYYMMDD') - 'INTERVAL 1 MONTH','YYYYMM'), "
		cReturn +=     "TO_CHAR(TO_DATE(B1.O0E_MARCA,'YYYYMMDD') - 'INTERVAL 2 MONTH','YYYYMM'), "
		cReturn +=     "TO_CHAR(TO_DATE(B1.O0E_MARCA,'YYYYMMDD') - 'INTERVAL 3 MONTH','YYYYMM'), "
		cReturn +=     "TO_CHAR(TO_DATE(B1.O0E_MARCA,'YYYYMMDD') - 'INTERVAL 6 MONTH','YYYYMM'), "
		cReturn +=     "TO_CHAR(TO_DATE(B1.O0E_MARCA,'YYYYMMDD') - 'INTERVAL 11 MONTH','YYYYMM'), "
		cReturn +=     "TO_CHAR(TO_DATE(B1.O0E_MARCA,'YYYYMMDD') - 'INTERVAL 12 MONTH','YYYYMM')) "
	Else 
		cReturn := "AND LEFT(B1.O0E_MARCA,6) = LEFT(<<FINAL_DATE>>,6) "
		cReturn += "AND LEFT(A1.O0E_MARCA,6) IN ( "
		cReturn +=      "CONVERT(VARCHAR(6),dateadd(month,-1,CONVERT(DATE,B1.O0E_MARCA,112)),112), " 
		cReturn +=      "CONVERT(VARCHAR(6),dateadd(month,-2,CONVERT(DATE,B1.O0E_MARCA,112)),112), "
		cReturn +=      "CONVERT(VARCHAR(6),dateadd(month,-3,CONVERT(DATE,B1.O0E_MARCA,112)),112), "
		cReturn +=      "CONVERT(VARCHAR(6),dateadd(month,-6,CONVERT(DATE,B1.O0E_MARCA,112)),112), "
		cReturn +=      "CONVERT(VARCHAR(6),dateadd(month,-11,CONVERT(DATE,B1.O0E_MARCA,112)),112), "
		cReturn +=      "CONVERT(VARCHAR(6),dateadd(month,-12,CONVERT(DATE,B1.O0E_MARCA,112)),112)) "
	EndIf
Return cReturn