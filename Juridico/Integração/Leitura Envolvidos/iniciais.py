import spacy # https://spacy.io/
import re
import io

nlp = spacy.load('pt')

aPatternName = ["casad","solteir","desempregad","brasileir","divorciad","nascid","portador"]
aPatternEmail = ["endereço eletrônico","e-mail","email"]
aPatternCpf = ["CPF","CPF/MF"]
aPatternCnpj = ["CNPJ"]
aPatternCnj = ["autos do processo","processo número","processo","número do processo"]

regexName = r"^([^,]+[A-Z])"
regexEmail = r"[\w\.-]+@[\w\.-]+"
regexCPF = r"\d{3}\.\d{3}\.\d{3}\-\d{2}"
regexCNPJ = r"\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}"
regexCNJ =  r"\d{7}\-\d{2}\.\d{4}\.\d{1}\.\d{2}\.\d{4}"

# Função para encontrar os padrões no texto
def findPattern(data):
        global CNJ
        global nome
        global email
        global CPF
        global CNPJ
        global strCNPJ
        global aCNPJ
        nome = ''
        email = ''
        CPF = ''
        CNPJ = ''
        CNJ = ''
        linhas = ''
        linhaAtual = ''
        aCNPJ = []
        strCNPJ = ''
        newData = io.StringIO(data) # Converte o conteúdo do body para string
        texto = newData.readlines()
        i = 0

        # Procura os padrões passados no aPatternPer
        for linhas in texto:
            # Procura as palavras chaves
            if any(i in linhas for i in aPatternName):
                nL = texto.index(linhas)
                linhaAtual = texto[nL]
                matches = re.finditer(regexName, linhaAtual, re.DOTALL)

                # Realiza os matchs do Regex 
                for matchNum, match in enumerate(matches):
                    nome = match.group()
                    linhaAtual = ''

                # Se não achou no Regex, busca pela entidade
                if nome == '':
                    linhaAtual = nlp(texto[nL])

                    # Verifica palavra por palavra para encontrar a entidade
                    for word in linhaAtual.ents:
                        if word.label_ == 'PER':
                            nome = word.text
                            linhaAtual = ''
                        elif word.label_ == 'MISC' and nome == '':
                            nome = word.text
                            linhaAtual = ''    
                if nome != '':
                     break

        # Procura os padrões passados no aPatternEmail
        for linhas in texto:
            # Busca a palavra chave
            if any(i in linhas for i in aPatternEmail):
                nL = texto.index(linhas)
                linhaAtual = texto[nL]
                startLine = nL - 6
                endLine = nL + 2
                linhas = texto[startLine:endLine]
                linhas = ''.join(linhas)

                # Se o nome for encontrado nas linhas selecionadas (startLine:endLine)
                if nome in linhas and nome != '':
                    matches = re.finditer(regexEmail, linhas, re.DOTALL)

                    # Verifica o Regex
                    for matchNum, match in enumerate(matches):
                        email = match.group()
                        linhaAtual = ''
                    if email != '':
                        break

        # Procura os padrões passados no aPatternCpf
        for linhas in texto:
            # Verifica se encontra o pattern do CPF
            if any(i in linhas for i in aPatternCpf):
                nL = texto.index(linhas)
                linhaAtual = texto[nL]
                startLine = nL - 5
                endLine = nL + 4
                linhas = texto[startLine:endLine]
                linhas = ''.join(linhas)
            
            # Se o nome for encontrado nas linhas selecionadas (startLine:endLine)
            if nome in linhas and nome != '':
                matches = re.finditer(regexCPF, linhas, re.DOTALL)

                for matchNum, match in enumerate(matches):
                    CPF = match.group()
                    linhaAtual = ''
                    if CPF != '':
                        break

        # Procura no texto o padrão de numeração CNPJ
        for linhas in texto:
            matches = re.finditer(regexCNPJ, linhas, re.DOTALL)

            for matchNum, match in enumerate(matches):
                i+=1
                CNPJ = '{"id":'+str(i)+' , "CNPJ":"' + match.group() + '"}'
                if CNPJ not in aCNPJ:
                    aCNPJ.append(CNPJ)
                    linhaAtual = ''
                    strCNPJ = ', '.join(aCNPJ) # Transforma o array aCNPJ em string para ser aceito no JSON

        # Procura os padrões passados no aPatternCnj
        for linhas in texto:
            if any(i in linhas for i in aPatternCnj):
                nL = texto.index(linhas)
                linhaAtual = texto[nL]
                matches = re.finditer(regexCNJ, linhaAtual, re.DOTALL)
                
                for matchNum, match in enumerate(matches):
                    CNJ = match.group()
                    linhaAtual = ''

                    if CNJ != '':
                        break

        # Monta a estrutura do JSON
        oJson = '{"autor":"'+ nome +'", "cpf":"'+ CPF +'", "email":"'+ email +'","polopassivo":['+ strCNPJ +'],"processo":"'+ CNJ +'"}'
        return oJson
