export const weekdayBR = [
    { id: '0', name: 'Domingo', shortName: 'Dom' },
    { id: '1', name: 'Segunda', shortName: 'Seg'  },
    { id: '2', name: 'Terça', shortName: 'Ter'  },
    { id: '3', name: 'Quarta', shortName: 'Qua'  },
    { id: '4', name: 'Quinta', shortName: 'Qui'  },
    { id: '5', name: 'Sexta', shortName: 'Sex'  },
    { id: '6', name: 'Sabado', shortName: 'Sab'  },
  ];
