import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { formatDate } from '@angular/common';
import { trigger, transition, style, animate } from '@angular/animations';

import { EventData } from './interface/event-data';
import { yearsLabel } from './locale/years';
import { mounthsHU } from './locale/monthsHU';
import { weekdayHU } from './locale/weekdaysHU';
import { mounthsEN } from './locale/monthsEN';
import { weekdayEN } from './locale/weekdaysEN';
import { mounthsBR } from './locale/monthsBR';
import { weekdayBR } from './locale/weekdaysBR';

@Component({
	selector: 'ngx-event-calendar',
	templateUrl: './ngx-event-calendar.component.html',
	styleUrls: ['./ngx-event-calendar.component.scss'],
	animations: [
		trigger('enterAnimation', [
			transition(':enter', [
				style({ transform: 'translateX(100%)', opacity: 0 }),
				animate('250ms', style({ transform: 'translateX(0)', opacity: 1 })),
			]),
			transition(':leave', [
				style({ transform: 'translateX(0)', opacity: 1 }),
				animate('250ms', style({ transform: 'translateX(100%)', opacity: 0 })),
			]),
		]),
	],
})
export class NgxEventCalendarComponent implements OnInit {
	isSmall = false;

	today;
	currentMonth;
	currentYear;
	firstDay;
	daysInMonth;
	daysInLastMonth;
	actDay;
	lastMonth;
	actMonth;
	months: any;
	weekdays: any;
	years = [];
	actFullDate;
	actDate: any;
	arrTest = [];
	arrCalendar = [];
	eventsData: any;
	actYear;
	showChangeDate = false;
	btnAddShow: boolean;

	@Input() dataSource: EventData[];
	@Input() showAddButton: boolean;
	@Input() language: string;
	@Output() changeMonth = new EventEmitter();
	@Output() dayEvents = new EventEmitter();
	@Output() newEvent = new EventEmitter();

	constructor() {
		this.today = new Date();
		this.currentMonth = this.today.getMonth();
		this.currentYear = this.today.getFullYear();
		this.years = yearsLabel;
	}

	ngOnInit() {
		this.actFullDate = formatDate(new Date(), 'yyyy. MMMM. dd', 'en');
		this.actDate = formatDate(new Date(), 'MMMM/yyyy', 'pt-BR');
		this.actDay = formatDate(new Date(), 'dd', 'en');
		this.actMonth = formatDate(new Date(), 'MM', 'en');
		this.actYear = formatDate(new Date(), 'yyyy', 'en');
		this.eventsData = this.dataSource;
		this.btnAddShow = this.showAddButton;
	}

	ngAfterViewInit(): void {
		setTimeout(() => {
			const height = document.getElementById('cont').offsetHeight;
			const width = document.getElementById('cont').offsetWidth;
			// TODO: if small only show badges not all the events

			if (height <= 350 || width <= 750) {
				this.isSmall = true;
			} else {
				this.isSmall = false;
			}
		}, 1000);
	}

	ngOnChanges() {
		this.eventsData = this.dataSource;
		this.changeLanguage();
		this.createCalendar();
	}

	/**
	 * Função responsável por renderizar o painel de atividade,
	 * conforme resolução do usuário.
	 */
	@HostListener('window:resize', ['$event'])
	onResize() {
		const height = document.getElementById('cont').offsetHeight;
		const width = document.getElementById('cont').offsetWidth;
		if (height <= 350 || width <= 750) {
			this.isSmall = true;
		} else {
			this.isSmall = false;
		}
	}

	/**
	 * Função responsável por montar o calendário
	 */
	createCalendar() {
		this.arrTest = [];
		this.arrCalendar = [];
		this.firstDay = new Date(this.currentYear, this.currentMonth).getUTCDay();
		this.daysInMonth = this.getDaysInMonth(this.currentMonth, this.currentYear);
		this.daysInLastMonth = this.getDaysInMonth(this.currentMonth - 1, this.currentYear);
		const lmd = this.daysInLastMonth - (this.firstDay - 1);

		// Last month days
		for (let index = lmd; index <= this.daysInLastMonth; index++) {
			this.arrTest.push({
				day: index,
				month: this.currentMonth - 1,
				year: this.currentYear,
			});
		}

		// Actual month
		for (let index = 1; index <= this.daysInMonth; index++) {
			const filterEvents = this.eventsData.filter(event => {
				return (
					event.startDate <=
						new Date(this.currentYear, this.currentMonth, index + 1).getTime() &&
					event.endDate >= new Date(this.currentYear, this.currentMonth, index).getTime()
				);
			});

			//Sorted events by date
			const arrSortedEventsByDate = filterEvents.sort((a: any, b: any) => {
				return a.startDate - b.startDate;
			});

			this.arrTest.push({
				day: index,
				month: this.currentMonth,
				year: this.currentYear,
				events: arrSortedEventsByDate,
			});
		}

		for (let i = this.arrTest.length, j = 1; i < 42; i++, j++) {
			this.arrTest.push({
				day: j,
				month: this.currentMonth + 1,
				year: this.currentYear,
			});
		}

		for (let i = 0; i < 6; i++) {
			const arrWeek = this.arrTest.splice(0, 7);
			this.arrCalendar.push(arrWeek);
		}
	}

	/**
	 * Função responsável por obter o ultimo dia do mês
	 * @param iMonth - Mês
	 * @param iYear  - Ano
	 */
	getDaysInMonth(iMonth, iYear) {
		return 32 - new Date(iYear, iMonth, 32).getDate();
	}

	/**
	 * Função que volta para o mês anterior
	 */
	previousMonthButtonClick() {
		if (this.currentMonth === 0) {
			this.currentYear -= 1;
			this.currentMonth = 11;
		} else {
			this.currentMonth -= 1;
		}

		this.actDate = this.creatActMonthYear();

		this.createCalendar();
		this.addEventMonth();
	}

	/**
	 * Função que avança para o proximo mês
	 */
	nextMonthButtonClick() {
		if (this.currentMonth === 11) {
			this.currentYear += 1;
			this.currentMonth = 0;
		} else {
			this.currentMonth += 1;
		}

		this.actDate = this.creatActMonthYear();
		this.createCalendar();
		this.addEventMonth();
	}

	/**
	 * Função que envia informações dos eventos do dia selecionado pelo usuário
	 * @param event - Estrutura de informação de eventos do dia selecionado.
	 */
	openDialog(event) {
		this.dayEvents.next(event);
	}

	/**
	 * Função que define a linguagem do calendário.
	 */
	changeLanguage() {
		if (!this.language) {
			this.language = 'hu';
			this.months = mounthsHU;
			this.weekdays = weekdayHU;
		}
		if (this.language === 'en') {
			this.months = mounthsEN;
			this.weekdays = weekdayEN;
		} else if (this.language === 'br') {
			this.months = mounthsBR;
			this.weekdays = weekdayBR;
		} else {
			this.months = mounthsHU;
			this.weekdays = weekdayHU;
		}
	}

	/**
	 * Função que movimento o calendário para o ano selecionado.
	 * @param event - Ano
	 */
	onYearChange(event) {
		this.currentYear = event;

		this.actDate = this.creatActMonthYear();
		this.createCalendar();
		this.addEventMonth();
	}

	/**
	 * Função que movimento o calendário para o mês selecionado.
	 * @param event - mês
	 */
	onMonthChange(event) {
		this.currentMonth = event;

		this.actDate = this.creatActMonthYear();

		this.createCalendar();
		this.addEventMonth();
	}

	/**
	 * Função que define o titulo do calendário (Mês/Ano)
	 */
	creatActMonthYear() {
		const actDate = formatDate(
			new Date(this.currentYear, this.currentMonth),
			'MMMM/yyyy',
			'pt-BR'
		);

		return actDate;
	}

	/**
	 * Função que disponibiliza ao usuário criar novos eventos no calendário para o dia selecionado.
	 */
	addEventClicked() {
		const testMessage = `${this.currentYear}-${this.currentMonth}-${this.actDay}`;
		this.newEvent.next(testMessage);
	}

	/**
	 * Função responsável por informar o período que o calendário se encontra.
	 */
	addEventMonth() {
		this.changeMonth.next(
			new Date(
				this.currentYear,
				this.currentMonth,
				this.getDaysInMonth(this.currentMonth, this.currentYear)
			)
		);
	}
}
