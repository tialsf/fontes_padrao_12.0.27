import { NgModule } from '@angular/core';
import { NgxEventCalendarComponent } from './ngx-event-calendar.component';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PoModule, PoComponentsModule } from '@po-ui/ng-components';

@NgModule({
	declarations: [NgxEventCalendarComponent],
	imports: [
		FlexLayoutModule,
		CommonModule,
		MatButtonModule,
		MatSelectModule,
		MatFormFieldModule,
		MatInputModule,
		MatIconModule,
		FormsModule,
		PoModule,
		PoComponentsModule
	],
	exports: [NgxEventCalendarComponent]
})
export class NgxEventCalendarModule { }
