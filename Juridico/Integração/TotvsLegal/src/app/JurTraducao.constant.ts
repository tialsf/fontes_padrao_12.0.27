import { PoI18nConfig } from '@po-ui/ng-components';
import { favoritosPt } from './favoritos/favoritos-pt';
import { homePt } from './home/home-pt';
import { provisaoPt } from './provisao/provisao-pt';
import { acompanhamentoPt } from './acompanhamento/acompanhamento-pt';
import { menuPt } from './menu/menu-pt';
import { jurigenericsPt } from './services/jurigenerics-pt';
import { processoPt } from './processo/processo-pt';
import { andamentosPt } from './andamentos/andamentos-pt';
import { anexosPt } from './anexos/anexos-pt';
import { detAndamentosPt } from './andamentos/det-andamento/detAndamento-pt';
import { garantiasPt } from './processo/garantias/garantias-pt';
import { detGarantiasPt } from './processo/garantias/det-garantia/detGarantia-pt';
import { detPedidosPt } from './processo/pedidos/det-pedido/detPedido-pt';
import { tarefasPt } from './tarefas/tarefas-pt';
import { detTarefaPt } from './tarefas/det-tarefa/detTarefa-pt';
import { pedidosPt } from './processo/pedidos/pedidos-pt';
import { despesasPt } from './despesas/despesas-pt';
import { detDespesasPt } from './despesas/det-despesa/detDespesas-pt';
import { detLevantamentosPt } from './processo/garantias/det-garantia/det-levantamento/detLevantamento-pt';
import { detProcessoPt } from './processo/det-processo/det-processo-pt';
import { PesqAvanPt } from './pesq-avancada/pesq-avancada-pt';
import { loginPt } from './login/login-pt';
import { ProtheusServicePt } from './services/protheus-service-pt';
import { historicoPt } from './historico/historico-pt';
import { JustificativaPt } from './shared/jur-justif-modal/justificativa-pt';
import { homeContratosPt } from './home-contratos/homeContratos-pt';
import { contratoPt } from './contrato/contrato-pt';
import { detContratoPt } from './contrato/det-contrato/det-contrato-pt';
import { altProcessoPt } from './processo/alt-processo/alt-processo-pt';
import { detTipoContratoPt } from './cadastros/tipo-contrato/det-tipocontrato/det-tipocontrato-pt';
import { tipoContratoPt } from './cadastros/tipo-contrato/tipo-contrato-pt';
import { SLAJuridicoPt } from './sla-juridico/sla-juridico-pt';
import { PageCadastrosPt } from './cadastros/page-cadastros/page-cadastros-pt';
import { aditivosPt } from './contrato/aditivos/aditivos-pt';
import { detAditivoPt } from './contrato/aditivos/det-aditivos/det-aditivo-pt';
import { unidadePt } from './cadastros/unidades/unidades-pt';
import { detUnidadePt } from './cadastros/unidades/det-unidade/det-unidade-pt';
import { usuariosPt } from './usuarios/usuarios/usuarios-pt';
import { detUsuarioPt } from './usuarios/usuarios/det-usuario/det-usuario-pt';
import { gruposUsuarioPt } from './usuarios/usuarios/grupos-usuario/grupos-usuario-pt';
import { credenciadosPt } from './cadastros/credenciados/credenciados-pt';
import { detCredenciadoPt } from './cadastros/credenciados/det-credenciado/det-credenciado-pt';
import { InstanciaServicePt } from './services/instancia-service-pt';
import { userConfigPT } from './usuarios/user-config/user-config-pt';
import { distribuicaoPt } from './distribuicao/distribuicao-pt';
import { detDistribuicaoPt } from './distribuicao/det-distribuicao/det-distribuicao-pt';
import { detGrupoUsuariosPt } from './usuarios/usuarios/grupos-usuario/det-grupos-usuarios/det-grupo-usuario-pt';
import { atividadesPT } from './atividades/atividade-pt';
import { modImportacaoPt } from './distribuicao/mod-importacao/mod-importacao-pt';

//- Classe de configuração de tradução - poI18n
export const i18nConfig: PoI18nConfig = {
	//-- Default
	//-- Caso não seja passado nenhum parametro para o GetLiterals(), utilizará o default
	default: {
		language: 'pt-BR',
		context: 'general',
		cache: true,
	},

	//-- Contextos
	contexts: {
		/*general: {
			'pt-BR': generalPt,
			//'en-US': generalEn
		},*/
		login: {
			'pt-BR': loginPt,
		},

		acomp: {
			'pt-BR': acompanhamentoPt,
		},

		fav: {
			'pt-BR': favoritosPt,
		},

		home: {
			'pt-BR': homePt,
		},

		prov: {
			'pt-BR': provisaoPt,
		},
		menu: {
			'pt-BR': menuPt,
		},
		jurigenerics: {
			'pt-BR': jurigenericsPt,
		},
		protheusService: {
			'pt-BR': ProtheusServicePt,
		},
		processo: {
			'pt-BR': processoPt,
		},
		cadastro: {
			'pt-BR': detProcessoPt,
		},
		andamentos: {
			'pt-BR': andamentosPt,
		},
		anexos: {
			'pt-BR': anexosPt,
		},
		detAndamentos: {
			'pt-BR': detAndamentosPt,
		},
		garantias: {
			'pt-BR': garantiasPt,
		},
		detGarantias: {
			'pt-BR': detGarantiasPt,
		},
		tarefas: {
			'pt-BR': tarefasPt,
		},
		detPedidos: {
			'pt-BR': detPedidosPt,
		},
		detTarefa: {
			'pt-BR': detTarefaPt,
		},
		pedidos: {
			'pt-BR': pedidosPt,
		},
		despesas: {
			'pt-BR': despesasPt,
		},
		detDespesa: {
			'pt-BR': detDespesasPt,
		},
		detLevantamentos: {
			'pt-BR': detLevantamentosPt,
		},
		pesqAvan: {
			'pt-BR': PesqAvanPt,
		},
		jurJustifModal: {
			'pt-BR': JustificativaPt,
		},
		historico: {
			'pt-BR': historicoPt,
		},

		homeContratos: {
			'pt-BR': homeContratosPt,
		},
		contrato: {
			'pt-BR': contratoPt,
		},
		contratoCadastro: {
			'pt-BR': detContratoPt,
		},
		altProcesso: {
			'pt-BR': altProcessoPt,
		},
		detTipocontrato: {
			'pt-BR': detTipoContratoPt,
		},
		tipoContrato: {
			'pt-BR': tipoContratoPt,
		},
		slaJur: {
			'pt-BR': SLAJuridicoPt,
		},
		pageCadastros: {
			'pt-BR': PageCadastrosPt,
		},
		aditivos: {
			'pt-BR': aditivosPt,
		},
		detAditivo: {
			'pt-BR': detAditivoPt,
		},
		unidade: {
			'pt-BR': unidadePt,
		},
		detUnidade: {
			'pt-BR': detUnidadePt,
		},
		usuarios: {
			'pt-BR': usuariosPt,
		},
		detUsuario: {
			'pt-BR': detUsuarioPt,
		},
		gruposUsuario: {
			'pt-BR': gruposUsuarioPt,
		},
		credenciados: {
			'pt-BR': credenciadosPt,
		},
		detcredenciado: {
			'pt-BR': detCredenciadoPt,
		},
		instanciaService: {
			'pt-BR': InstanciaServicePt,
		},
		userConfig: {
			'pt-BR': userConfigPT,
		},
		distribuicao: {
			'pt-BR': distribuicaoPt,
		},
		detDistribuicao: {
			'pt-BR': detDistribuicaoPt,
		},
		detGrupoUsuarios: {
			'pt-BR': detGrupoUsuariosPt,
		},
		atividades: {
			'pt-BR': atividadesPT,
		},
		modImportacao: {
			'pt-BR': modImportacaoPt,
		},
	},
};
