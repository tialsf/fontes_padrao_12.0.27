/**
* Arquivo de constantes para tradução de Acompanhamento de Processos - Português
*/


//-- Constantes para títulos das colunas do Grid de Acompanhamento de Processos 
export const gridAcomp = {
	
	colProcesso:     "Processos",
	colMovimentacao: "Movimentação",
	colData:         "Data",
	colValorProv:    "Valor da provisão (R$)",
	colContrato:     "Contratos",
	colValorContr:   "Valor do contrato (R$)",
}


//-- Constantes para o breadcrumb de Acompanhamento de Processos 
export const breadcrumbAcomp = {

	brHome:          "Meus processos",
	brAcomp:         "Acompanhamento",
	brHomeContratos: "Meus contratos",
}


//-- Constantes Gerais para o componente de Acompanhamento de Processos
export const acompanhamentoPt = {	
	
	cntPesquisaTittle:    "Acompanhamento",
	acompanhamento:       "Acompanhamento de processos",
	processosEmAndamento: "Processos em andamento",
	verAndamentos:        "Ver os processos em andamento",
	novosProcessos:       "Novos processos",
	verprocessosNovos:    "Ver os processos novos",
	processosEncerrados:  "Processos encerrados",
	verEncerrados:        "Ver os processos encerrados",	
	pesquisarProcesso:    "Pesquisar processo",
	pesq:                 "Pesquisar",
	anterior:             "<< Anterior",
	proximo:              "Próximo >>",
	finalPagina:          "Ops! Não há mais registros para serem exibidos.",
	// Contratos
	acompanhamentoContr:  "Acompanhamento de contratos",
	contratosEmAndamento: "Contratos vigentes",
	verAndamentosContr:   "Ver os contratos vigentes",
	novosContratos:       "Novos contratos",
	verContratosNovos:    "Ver os novos contratos",
	contratosEncerrados:  "Contratos encerrados",
	verEncerradosContr:   "Ver os contratos encerrados",	
	pesquisarContrato:    "Pesquisar contrato",
	gridAcompanhamento: gridAcomp,
	breadcrumbAc: breadcrumbAcomp

}


