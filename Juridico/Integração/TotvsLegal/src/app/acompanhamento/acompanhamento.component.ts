import { Component, OnInit } from '@angular/core';
import { PoBreadcrumb, PoCheckboxGroupOption, PoTableColumn, PoI18nService, PoDialogConfirmLiterals, PoDialogService  } from '@po-ui/ng-components';
import { LegalprocessService } from '../services/legalprocess.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JurigenericsService } from '../services/jurigenerics.service';
import { JurconsultasService } from '../services/jurconsultas.service';
import { TLUserInfo } from '../services/authlogin.service';
import { UserConfig } from 'src/app/usuarios/user-config/user-config.component'

@Component({
	selector: 'app-acompanhamento',
	templateUrl: './acompanhamento.component.html',
	styleUrls: ['./acompanhamento.component.css']
})

export class AcompanhamentoComponent implements OnInit {
	columns:                     Array<PoTableColumn>;
	listaProcesso:               Array<string> = [];
	pagina:                      string = "1";
	pesquisa:                    string = '';
	codPro:                      string = '';
	filial:                      string = '';
	assJur:                      string = '';
	strAnomesAtu:                string = '';
	situacao:                    string = '';
	acompanhamentoHTML:          string = '';
	emAndamentoHTML:             string = '';
	novosHTML:                   string = '';
	encerradosHTML:              string = '';
	verEmAndamentoHTML:          string = '';
	verNovosHTML:                string = '';
	verEncerradosHTML:           string = '';
	pesquisarHTML:               string = '';
	linkHome:                    string = '';
	breadcrumbHome:              string = '';
	codUsuario:                  string = '';
	hasPage:                     boolean = true;
	isHideLoadingAcompanhamento: boolean = false;
	tPrimaryAnd:                 boolean = false;
	tPrimaryEnc:                 boolean = false;
	tPrimaryNov:                 boolean = false;
	isContrato:                  boolean = false;
	isDisabledBtnPrev:           boolean = true;
	isDisabledBtnNxt:            boolean = true;
	lastPage:                    number = 1;
	page:                        number;
	qtdAndamento:                number = 0;
	qtdNovosProcessos:           number = 0;
	qtdEncerrado:                number = 0;	
	inscricao:                   Subscription;
	tFormatAnd                   = 'po-button-link';
	tFormatEnc                   = 'po-button-link';
	tFormatNov                   = 'po-button-link';
	litAcomp:                    any;
	literalsConfirm:             PoDialogConfirmLiterals;
	dtAnoMesAtu                  = new Date();

	public breadcrumb: PoBreadcrumb = {
		items: [
		],
	};	

	public readonly propertiesOptions: Array<PoCheckboxGroupOption> = [
		{ label: 'Hide text overflow', value: 'hideTextOverflow' },
		{ label: 'Sort', value: 'sort' },
		{ label: 'Striped', value: 'striped' },
		{ label: 'Show more disabled', value: 'showMoreDisabled' },
		{ label: 'Hide detail', value: 'hideDetail' },
		{ label: 'Loading', value: 'loading' }
	];

	colAcomp: Array<PoTableColumn> = [
		{
			property: 'caso',
			color: 'color-01',
			type: 'link',

			action: (value,row) => {
				if(this.isContrato){
					this.openConsulta(row.branch,row.id);
				}else{
					this.openProcesso(row.processBranch,row.processId);
				}
			},
			width: '50%'
		},

		{
			property: 'movimentacao',
			color: 'color-01',
			type: 'link',

			action: (value,row) => {
				if(this.isContrato){
					this.openConsulta(row.branch,row.id);
				}else{
					this.openProcesso(row.processBranch,row.processId);
				}
			},
			width: '20%'
		},
		{
			property: 'data',
			color: 'color-01',
			type: 'link',

			action: (value,row) => {
				if(this.isContrato){
					this.openConsulta(row.branch,row.id);
				}else{
					this.openProcesso(row.processBranch,row.processId);
				}
			},
			width: '10%'
		},
		
		{ property: 'atualizedprovision', type: 'number', format: '1.2-5', width: '20%' }
	];


	constructor(
		private legalprocess: LegalprocessService,
		private route: ActivatedRoute,
		private thfAlert: PoDialogService,
		private thfI18nService: PoI18nService,
		private JuriGenerics: JurigenericsService,
		private jurconsultas: JurconsultasService,
		private userInfo: TLUserInfo) {

		//-- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'acomp' no arquivo JurTraducao.			
		thfI18nService.getLiterals({ context: 'acomp', language: thfI18nService.getLanguage() })
			.subscribe((litAcomp) => {
				this.litAcomp = litAcomp;

				// Lê os parametros da rota
				this.inscricao = this.route.queryParams.subscribe((queryParams: any) => {
					this.pagina = queryParams.page
					if(queryParams.assJur != undefined){
						this.assJur = queryParams.assJur
					}
				})
				if(this.assJur == '006'){ //contratos
					this.isContrato = true;
					this.linkHome = '/homeContratos';
					this.breadcrumbHome = this.litAcomp.breadcrumbAc.brHomeContratos;
				}else{
					this.linkHome = '/home';
					this.breadcrumbHome = this.litAcomp.breadcrumbAc.brHome;;
				}
				
				//-- seta as constantes para tradução dos títulos do Breadcrumb de Acompanhamento - acompanhamento-pt.ts
				this.breadcrumb.items = [
					{ label: this.breadcrumbHome  , link: this.linkHome },
					{ label: this.litAcomp.breadcrumbAc.brAcomp}
				]

				//-- seta as constantes para tradução do titulo das colunas do Grid de Acompanhamento - acompanhamento-pt.ts
				if(this.isContrato){
					this.colAcomp[0].label = this.litAcomp.gridAcompanhamento.colContrato;
					this.colAcomp[3].label = this.litAcomp.gridAcompanhamento.colValorContr;
				}else{
					this.colAcomp[0].label = this.litAcomp.gridAcompanhamento.colProcesso;
					this.colAcomp[3].label = this.litAcomp.gridAcompanhamento.colValorProv;
				}
				this.colAcomp[1].label = this.litAcomp.gridAcompanhamento.colMovimentacao;
				this.colAcomp[2].label = this.litAcomp.gridAcompanhamento.colData;

				//-- seta as constantes para tradução dos titulos e labels do hmtl que variam de acordo com o tipo de assunto jurídico
				if(this.isContrato){
					this.acompanhamentoHTML = this.litAcomp.acompanhamentoContr;
					this.emAndamentoHTML    = this.litAcomp.contratosEmAndamento;
					this.novosHTML          = this.litAcomp.novosContratos;
					this.encerradosHTML     = this.litAcomp.contratosEncerrados;
					this.verEmAndamentoHTML = this.litAcomp.verAndamentosContr;
					this.verNovosHTML       = this.litAcomp.verContratosNovos;
					this.verEncerradosHTML  = this.litAcomp.verEncerradosContr;
					this.pesquisarHTML      = this.litAcomp.pesquisarContrato;
				}else{
					this.acompanhamentoHTML = this.litAcomp.acompanhamento;
					this.emAndamentoHTML    = this.litAcomp.processosEmAndamento;
					this.novosHTML          = this.litAcomp.novosProcessos;
					this.encerradosHTML     = this.litAcomp.processosEncerrados;
					this.verEmAndamentoHTML = this.litAcomp.verAndamentos;
					this.verNovosHTML       = this.litAcomp.verprocessosNovos;
					this.verEncerradosHTML  = this.litAcomp.verEncerrados;
					this.pesquisarHTML      = this.litAcomp.pesquisarProcesso;
				}
			});
	}

	ngOnInit() {
		this.pesquisa = "";
		this.columns = this.colAcomp

		if (this.isContrato) { //contratos
			this.getTotaisAcomp();
			if (this.pagina == '1') { //em Andamento
				this.situacao = '1' //em Aberto
			} else if (this.pagina == '3') { //encerrados
				this.situacao = '2' //encerrado
			}
			this.toogle();
		} else {
			this.getInfoUser();
		}

		this.strAnomesAtu = this.dtAnoMesAtu.getFullYear().toString() + this.JuriGenerics.buildMonthDayStr((this.dtAnoMesAtu.getMonth() + 1).toString()) + this.JuriGenerics.buildMonthDayStr(this.dtAnoMesAtu.getDate().toString());
		this.strAnomesAtu = this.JuriGenerics.makeDate(this.strAnomesAtu,"MMMM de yyyy");
	}

	ngOnDestroy() {
		this.inscricao.unsubscribe();
	}

	/**
	 * Função responsável por obter dados do usuário logado.
	 */
	getInfoUser() {
		this.codUsuario = this.userInfo.getCodUsuario();

		if (this.JuriGenerics.changeUndefinedToEmpty(this.codUsuario) == ""){
			setTimeout(() => { this.getInfoUser() }, 500);
		} else {
			this.getStock();
			this.toogle();
		}
	}
	
	/**
	 * Traz a listagem de processos
	 */
	getListProcess() {
		let cfgUsuario: Array<UserConfig> = [];
		let nIndex:     number            = -1;
		let tpAssJur:   Array<string>     = [];

		if ( this.JuriGenerics.changeUndefinedToEmpty(window.localStorage.getItem('userConfig')) != '') {
			cfgUsuario = JSON.parse(window.localStorage.getItem('userConfig'));
			nIndex     = cfgUsuario.findIndex(x => x.userId == this.codUsuario);
			tpAssJur   = nIndex >= 0 ? cfgUsuario[nIndex].filtAssJur : [];
		}

		this.legalprocess.restore();
		this.legalprocess.setOrderProvision("2");
		this.legalprocess.setPageSize('20');
		this.legalprocess.setPage(this.page.toString());
		this.legalprocess.setTypeFilter(this.pagina);
		this.legalprocess.setFilter('tipoAssJur', tpAssJur.toString()); // Informar o assunto jurídico que deseja filtrar

		this.isHideLoadingAcompanhamento = false;
		this.isDisabledBtnNxt            = true;
		this.isDisabledBtnPrev           = true;

		if (this.pesquisa != '') {
			this.legalprocess.setSearchKey(this.pesquisa.toString())
		} else {
			this.legalprocess.setSearchKey("")
		}

		this.legalprocess.get('process', 'Lista de Processos').subscribe((data) => {
			this.hasPage = (data.hasNext == "true" ? true : false);

			if (this.hasPage) {
				this.isDisabledBtnNxt = false;
			}
			if (this.page > 1) {
				this.isDisabledBtnPrev = false;
			}

			if (data.processes != '') {
				data.processes.forEach(item => {
					item.caso = item.matter[0].description;
					item.movimentacao = item.lasthistory[0].title;
					item.data = this.JuriGenerics.makeDate(item.lasthistory[0].date,"dd/mm/yyyy");
				});

				this.listaProcesso = data.processes;
				this.isHideLoadingAcompanhamento = true;
			} else {
				this.isHideLoadingAcompanhamento = true; 

			}
		})
	}

	/**
	 * Busca dados para a próxima tela.
	 */
	showMore(buttonId: string) {
		this.addPage(buttonId);
	}

	/**
	 * Controle de paginação
	 */
	addPage(buttonId: string) {
		this.page = this.lastPage;
		
		if (buttonId == "b") {
			if (this.hasPage) {
				this.lastPage = this.page++
			}
		}else{
			if (this.page >= 2) {
				this.lastPage = this.page--
			}
		}
		
		if(this.isContrato){
			this.getListConsultas();
		}else{
			this.getListProcess();
		}
		this.lastPage = this.page;
	}

	/**
	 * Pesquisa Rápida
	 */
	search() {
		this.toogle();
		if(this.isContrato){
			this.getListConsultas();
		}else{
			this.getListProcess();
		}
	}

	/**
	 * Traz a quantidade de processos das widgets de acompanhamento
	 */
	getStock() {
		let cfgUsuario: Array<UserConfig> = [];
		let nIndex:     number            = -1;
		let tpAssJur:   Array<string>     = [];

		if ( this.JuriGenerics.changeUndefinedToEmpty(window.localStorage.getItem('userConfig')) != '') {
			cfgUsuario = JSON.parse(window.localStorage.getItem('userConfig'));
			nIndex     = cfgUsuario.findIndex(x => x.userId == this.codUsuario);
			tpAssJur   = nIndex >= 0 ? cfgUsuario[nIndex].filtAssJur : [];
		}

		//Processo em andamento
		this.legalprocess.restore();
		this.legalprocess.setTypeFilter('1');
		this.legalprocess.setPageSize('0');
		this.legalprocess.setFilter('tipoAssJur', tpAssJur.toString()); // Informar o assunto jurídico que deseja filtrar
		this.legalprocess.get('process', 'Acompanhamento - Proc. Andamento').subscribe((data) => {
			this.qtdAndamento = data.length.toString();
		})

		//Casos Novos
		this.legalprocess.restore();
		this.legalprocess.setTypeFilter('2');
		this.legalprocess.setPageSize('0');
		this.legalprocess.setFilter('tipoAssJur', tpAssJur.toString()); // Informar o assunto jurídico que deseja filtrar
		this.legalprocess.get('process', 'Acompanhamento - Novos Processo').subscribe((data) => {
			this.qtdNovosProcessos = data.length.toString();
		})

		//Encerrados no mês
		this.legalprocess.restore();
		this.legalprocess.setTypeFilter('3');
		this.legalprocess.setPageSize('0');
		this.legalprocess.setFilter('tipoAssJur', tpAssJur.toString()); // Informar o assunto jurídico que deseja filtrar
		this.legalprocess.get('process', 'Acompanhamento - Proc. Encerrados').subscribe((data) => {
			this.qtdEncerrado = data.length.toString();
		})
	}

	//Router
	open(tela) {
		this.situacao = ""
		switch(tela) {
			case "2": 
				this.pagina = "1";
				this.situacao = "1"
				break;

			case "3": 
				this.pagina = "2";
				break;

			case "4": 
				this.pagina = "3";
				this.situacao = "2"
				break;

		}

		this.pesquisa = '';
		this.toogle();
	}

	/**
	 * Destaca o filtro para a tela de acompanhamento
	 */
	toogle() {
		this.tPrimaryAnd = false;
		this.tPrimaryEnc = false;
		this.tPrimaryNov = false;
		this.tFormatAnd = 'po-button-link';
		this.tFormatEnc = 'po-button-link';
		this.tFormatNov = 'po-button-link';

		switch(this.pagina){
			case  "1":
				this.tPrimaryAnd = true;
				this.tFormatAnd = 'po-text-center';
				break;

			case  "2":
				this.tPrimaryNov = true;
				this.tFormatNov = 'po-text-center';
				break;

			case "3":
				this.tPrimaryEnc = true;
				this.tFormatEnc = 'po-text-center';
				break;

		}

		this.page = 1;
		if(this.isContrato){
			this.getListConsultas();
		}else{
			this.getListProcess();
		}
	}

	/**
	 * Alert para o final da página
	 */
	alertAcomp() {
		let titulo: string = '';
		if(this.isContrato){
			titulo = this.litAcomp.acompanhamentoContr
		}else{
			titulo = this.litAcomp.acompanhamento
		}
		this.thfAlert.alert({
			title: titulo,
			message: this.litAcomp.finalPagina,
			ok: () => ""
		});

	}

	
	/**
	 * Abre a tela de Resumo de Processos
	 * @param filPro 
	 * @param codPro NSZ_COD
	 */
	openProcesso(filPro: string, codPro: string){
		window.open('processo/' + encodeURIComponent(btoa(filPro)) +
			"/" + encodeURIComponent(btoa(codPro)));
	}

	/**
	 * Abre a tela de Resumo 
	 * @param filial 
	 * @param codigo NSZ_COD
	 */
	openConsulta(filial: string, codigo: string){
		window.open('contrato/' + encodeURIComponent(btoa(filial)) +
			'/' + encodeURIComponent(btoa(codigo))
		);
	}

	/**
	 * método GET (ListContratos) WS REST JurConsultas
	 * consulta os contratos, seus vencimentos, andamentos e valores
	 */
	getTotaisAcomp(){
		this.jurconsultas.restore();
		this.jurconsultas.setParam('assJur', this.assJur);
		this.jurconsultas.setParam('situac', '1');
		this.jurconsultas.setParam('tpFilter', '1');
		this.jurconsultas.get('contracts','getAcompanhamentos').subscribe((data) => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data.contracts) != ''){
				this.qtdAndamento = data.length;
			};
		});

		this.jurconsultas.restore();
		this.jurconsultas.setParam('assJur', this.assJur);
		this.jurconsultas.setParam('tpFilter', '2');
		this.jurconsultas.get('contracts','getAcompanhamentos').subscribe((data) => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data.contracts) != ''){
				this.qtdNovosProcessos = data.length;
			};
		});

		this.jurconsultas.restore();
		this.jurconsultas.setParam('assJur', this.assJur);
		this.jurconsultas.setParam('situac', '2');
		this.jurconsultas.setParam('tpFilter', '3');
		this.jurconsultas.get('contracts','getAcompanhamentos').subscribe((data) => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data.contracts) != ''){
				this.qtdEncerrado = data.length;
			};
		});
	}

	/**
	 * método GET (ListContratos) WS REST JurConsultas
	 * consulta os contratos, seus vencimentos, andamentos e valores
	 */
	getListConsultas(){
		this.listaProcesso = [];
		this.jurconsultas.restore();
		this.jurconsultas.setParam('assJur'  , this.assJur);
		this.jurconsultas.setParam('situac'  , this.situacao);
		this.jurconsultas.setParam('tpFilter', this.pagina);
		this.jurconsultas.setParam('pageSize', '20');
		this.jurconsultas.setParam('page'    , this.page.toString());
		this.isHideLoadingAcompanhamento = false;
		this.isDisabledBtnNxt = true;
		this.isDisabledBtnPrev = true;
		if (this.pesquisa != '') {			
			this.jurconsultas.setParam('searchKey', this.pesquisa.toString());
		}

		this.jurconsultas.get('contracts','getAcompanhamentos').subscribe((data) => {
			this.hasPage = (data.hasNext == "true" ? true : false);
			if(this.hasPage){
				this.isDisabledBtnNxt = false;
			}
			if(this.page > 1){
				this.isDisabledBtnPrev = false;
			}
			if(this.JuriGenerics.changeUndefinedToEmpty(data.contracts) != ''){
				data.contracts.forEach(item => {
					item.caso               = item.description;
					item.movimentacao       = item.moviment;
					item.data               = this.JuriGenerics.makeDate(item.movDate,"dd/mm/yyyy");
					item.atualizedprovision = item.value;
				})
				this.listaProcesso = data.contracts;
				this.isHideLoadingAcompanhamento = true;
			}else{
				this.isHideLoadingAcompanhamento = true;
			};
		})
	}
}
