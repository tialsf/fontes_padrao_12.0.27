/**
* Arquivo de constantes para tradução da página de andamentos
*/

//-- Constantes para o bread crumb
export const breadCrumb = {
	home:         'Meus processos',
	processo:     'Meu processo',
	andamentos:   'Andamentos',
	historico:    'Histórico',
	contrato:     'Meu contrato',
	homeContrato: 'Meus contratos',
	instancia:    'Instância'
}

//-- Constantes para grid de andamentos
export const gridAndamentos = {
	colData: 'Data',
	colAto:  'Ato',
	colFase: 'Fase',
	colTeor: 'Teor'
}

//-- Constantes principais
export const andamentosPt = {
	btnNovoAnd:   'Novo andamento',
	btnNovoHis:   'Novo histórico',
	pesquisarAnd: 'Pesquisar',
	btnPesquisar: 'Pesquisar',
	tituloAnd:    'Andamentos',
	tituloHis:    'Histórico',
	btnProximo:   'Proximo',
	btnAnterior:  'Anterior',
	carregando:   'Carregando andamentos...',
	gridAnd:      gridAndamentos,
	breadCrumb:   breadCrumb
}