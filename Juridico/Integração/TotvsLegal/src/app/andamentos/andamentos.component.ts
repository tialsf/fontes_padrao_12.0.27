import { JurigenericsService } from '../services/jurigenerics.service';
import { Component, OnInit} from '@angular/core';
import { PoBreadcrumb, PoNotificationService, PoTableColumn, PoI18nService } from '@po-ui/ng-components';
import { LegalprocessService } from '../services/legalprocess.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RoutingService } from 'src/app/services/routing.service';
import { FwmodelService } from 'src/app/services/fwmodel.service';
import { adaptorFilterParam, FwmodelAdaptorService } from 'src/app/services/fwmodel-adaptor.service';
import { InstanciaService, RetDadosInstancia } from '../services/instancia.service';

class Andamento{
	codAndamento: string;
	data: string;
	ato: string;
	fase: string;
	descricao: string;
	editaAndamento: Array<string>;
}

@Component({
	selector: 'app-andamentos',
	templateUrl: './andamentos.component.html',
	styleUrls: ['./andamentos.component.css']
})

export class AndamentosComponent implements OnInit {

	listAndamentos: Array<Andamento> = [];
	litAnd: any;
	
	inscricao: any;
	cajuri:  string;
	codInst: string;
	searchKey: string           = '';
	chaveProcesso: string       = '';
	filial: string              = '';
	cajuriTitulo: string        = '';
	perfil: string              = '/processo/';
	breadcrumbPerfil: string    = ''
	breadcrumbHome: string      = ''
	linkHome: string            = ''
	tituloPagina: string        = ''
	btnNovoLabel: string        = ''
	
	nPageSize: number            = 20;
	nRegIndex: number            = 1;
	nTotalAndamento: number      = 0;
	nNextPage: number            = 1;

	isHideLoadingAndamentos: boolean = false;
	isShowMoreDisabled: boolean      = false;
	mostraWidgetAndamentos: boolean  = true;
	isProcesso: boolean              = false;

	// Colunas da widget de Andamentos
	colAndamentos: Array<PoTableColumn> = [
		{ property: 'data', label: 'Data', type: 'date'},
		{ property: 'ato', label: 'Ato'},
		{ property: 'fase', label: 'Fase'},
		{ property: 'descricao', label: 'Teor'},
		{ property: 'editaAndamento', label: ' ', type: 'icon', width: '10%',
			icons: [
				{ value: 'editarAnd', icon: 'po-icon po-icon-edit', color: '#29b6c5', tooltip: 'Editar', action:(value)=>{
					this.callDetail(value);
				},
			 },
				{value: 'anexos', icon: 'po-icon po-icon-document-filled', color: '#29b6c5', tooltip: 'Anexos', action:(value)=>{
					this.callAnexos(value);
					},
				}
			] 
		}
	];

	// Adapter do Ato Processual
	adaptorParamAtoProcessual: adaptorFilterParam = new adaptorFilterParam(
		"JURA051", "NRO_TIPO!='1' AND NRO_TIPO!='2'", "NRO_DESC", "NRO_DESC", "", "NRO_COD", "", "Combo de ato"
		);
	/**
	 * estrutura de navegação da URL atual
	 */
	public breadcrumb: PoBreadcrumb = {
		items: [
		],
	};

	constructor(
		protected thfNotification: PoNotificationService,
		protected thfI18nService: PoI18nService,
		public fwModelAdaptorService: FwmodelAdaptorService,
		private fwmodel: FwmodelService,
		private legalprocess: LegalprocessService,
		private JuriGenerics: JurigenericsService,
		private route: ActivatedRoute,
		private routingState: RoutingService,
		private router: Router,
		private instanciaService: InstanciaService
	) { 
		this.routingState.loadRouting();

		if(this.router.url.indexOf('/contrato/')>-1){
			this.filial = this.route.snapshot.paramMap.get('filContrato');
			window.sessionStorage.setItem('filial', atob(this.filial));
		
			this.chaveProcesso = this.route.snapshot.paramMap.get('codContrato');
			this.cajuri = atob(decodeURIComponent(this.chaveProcesso));
			this.isProcesso = false;
		}else{
			this.filial = this.route.snapshot.paramMap.get('filPro');
			window.sessionStorage.setItem('filial', atob(this.filial));

			this.chaveProcesso = this.route.snapshot.paramMap.get('codPro');
			this.cajuri = atob(decodeURIComponent(this.chaveProcesso));
			this.isProcesso = true;
			
			if(this.route.snapshot.queryParamMap.get('codInst') != undefined){
				this.codInst = atob(this.route.snapshot.queryParamMap.get('codInst'))
			}
		}

		//-- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'andamentos' no arquivo JurTraducao.
		thfI18nService.getLiterals({ context: 'andamentos', language: thfI18nService.getLanguage() })
		.subscribe((litAnd) => {
			this.litAnd = litAnd;
			let breadCrumbAndLabel = ''

			//Verifica se é um processo ou contrato
			if (this.isProcesso){
				this.perfil = '/processo/';
				this.breadcrumbPerfil = this.litAnd.breadCrumb.processo
				this.breadcrumbHome   = this.litAnd.breadCrumb.home
				this.tituloPagina     = this.litAnd.tituloAnd
				this.btnNovoLabel     = this.litAnd.btnNovoAnd
				breadCrumbAndLabel    = this.litAnd.breadCrumb.andamentos
				this.linkHome         = '/home'
			}else{
				this.perfil = '/contrato/';
				this.breadcrumbPerfil = this.litAnd.breadCrumb.contrato
				this.breadcrumbHome   = this.litAnd.breadCrumb.homeContrato
				this.tituloPagina     = this.litAnd.tituloHis
				this.btnNovoLabel     = this.litAnd.btnNovoHis
				breadCrumbAndLabel    = this.litAnd.breadCrumb.historico
				this.linkHome         = '/homeContratos'
			}

			if(this.codInst){
			
				this.breadcrumb.items = [
					{ label: this.breadcrumbHome  , link: this.linkHome },
					{ label: this.breadcrumbPerfil, link: this.perfil + this.filial + '/' + this.chaveProcesso},
					{
						label: breadCrumbAndLabel,
						action: () => {
							this.codInst = null
							this.router.navigate([], {
								queryParams: { codInst: null },
								queryParamsHandling: 'merge',
							});
							setTimeout(() => {window.location.reload()}, 200);
						}
					},
					{ label: this.litAnd.breadCrumb.instancia}
				];

			} else {
				this.breadcrumb.items = [
					{ label: this.breadcrumbHome  , link: this.linkHome },
					{ label: this.breadcrumbPerfil, link: this.perfil + this.filial + '/' + this.chaveProcesso},
					{ label: breadCrumbAndLabel}
				];

			}

			//-- seta as constantes para tradução do titulo das colunas da widget de andamentos
			this.colAndamentos[0].label = this.litAnd.gridAnd.colData;
			this.colAndamentos[1].label = this.litAnd.gridAnd.colAto;
			this.colAndamentos[2].label = this.litAnd.gridAnd.colFase;
			this.colAndamentos[3].label = this.litAnd.gridAnd.colTeor;

			//-- Adiciona o código do cajuri no título Meu processo + área
			this.cajuriTitulo = this.cajuri.replace(new RegExp('^0*', 'g'), '')
		})
	}
	
	ngOnInit() {
		this.searchKey = ""
		this.getInfoProcesso();
		this.getAndamentos();
		this.getInfoInstancia();
	}

	/**
	 * Busca as informações do processo
	 */
	getInfoProcesso(){
		this.fwmodel.restore()
		this.fwmodel.setModelo("JURA095")
		this.fwmodel.setFilter("NSZ_FILIAL='" + atob(this.filial) + "'");
		this.fwmodel.setFilter("NSZ_COD='" + this.cajuri + "'")
		this.fwmodel.setCampoVirtual(true)
		this.fwmodel.get("Informação do Processo").subscribe(data=>{
			let descResumoBreadCrumb = []
			if (this.isProcesso){
				descResumoBreadCrumb = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields,"NSZ_DAREAJ")
			}else{
				descResumoBreadCrumb = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields,"NSZ_DESCON")
			}
			if (data.resources != ""){
				this.breadcrumb.items[1].label = this.breadcrumbPerfil + " " + descResumoBreadCrumb + " (" + this.cajuriTitulo + ")"
			}
		})
	}

	/**
	 * método GET (ListAndamentos)
	 * consome o WS REST de integração com LegalProcess
	 */
	getAndamentos() {
		this.isHideLoadingAndamentos = false;
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA100");
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.setPageSize(this.nPageSize.toString());
		this.fwmodel.setPage(this.nRegIndex.toString());
		this.fwmodel.setFilter("NT4_FILIAL = '" + atob(this.filial) + "'" );
		this.fwmodel.setFilter("NT4_CAJURI = '" + this.cajuri + "'");

		if(this.codInst != undefined){
			this.fwmodel.setFilter("NT4_CINSTA = '" + this.codInst + "'");
		}

		this.fwmodel.setHeaderParam('orderBy', ' ORDER BY NT4_DTANDA DESC ');

		if (this.searchKey != '' && this.searchKey != undefined) {
			this.fwmodel.setSearchKey("NT4_CATO = '" + this.searchKey + "'");
		}
		if (this.nRegIndex == 1){
			this.listAndamentos = [];
		}

		this.fwmodel.get("Lista de Andamentos").subscribe((data) => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != '') {
				data.resources.forEach(item => {
					let itemAndamento = new Andamento();
					itemAndamento.codAndamento   = this.JuriGenerics.findValueByName(item.models[0].fields, "NT4_COD");
					itemAndamento.data           = this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(item.models[0].fields, "NT4_DTANDA"), "yyyy-mm-dd");
					itemAndamento.ato            = this.JuriGenerics.findValueByName(item.models[0].fields, "NT4_DATO");
					itemAndamento.fase           = this.JuriGenerics.findValueByName(item.models[0].fields, "NT4_DFASE");
					itemAndamento.descricao      = this.JuriGenerics.findValueByName(item.models[0].fields, "NT4_DESC");
					itemAndamento.editaAndamento = ['editarAnd'];

					if (this.JuriGenerics.findValueByName(item.models[0].fields, 'NT4__TEMANX') == '01') {
						itemAndamento.editaAndamento = ['editarAnd','anexos'];
					}
					this.listAndamentos.push(itemAndamento);
				})
			}

			this.setShowMoreAndamento(data.total);
			this.isHideLoadingAndamentos = true;
		})
	}

	/**
	 * Incrementa o index de paginação da tela e
	 * Seta se o botão de carregar mais continua habilitado ou não
	 * @param total Total de andamentos
	 */
	setShowMoreAndamento(total: number) {

		this.nTotalAndamento = total;
		if(this.nRegIndex == 1){
			this.nRegIndex = this.nPageSize + 1;
		}else{
			this.nRegIndex += this.nPageSize;
		}
		
		if (this.nRegIndex <= total) {
			this.isShowMoreDisabled = false;
		}else{
			this.isShowMoreDisabled = true;
		};
	}
	
	/**
	 * Redireciona para o path da tela de detalhes do andamento
	 * @param value 
	 */
	callDetail(value ?: any) {
		if ( value == undefined){
			this.router.navigate(["./cadastro"], { relativeTo: this.route, queryParamsHandling: 'merge' })
		} else {
			this.router.navigate(["./" + btoa(value.codAndamento)], { relativeTo: this.route, queryParamsHandling: 'merge' })
		}
	}

	/**
	* ação do botão Pesquisa
	*/	  
	search() {
		this.nRegIndex = 1;
		this.getAndamentos();
	}

	/**
	 * Abre a tela de anexos
	 */
	callAnexos(value?: any){
		this.router.navigate(['./' + btoa(value.codAndamento) + '/anexos'], { relativeTo: this.route, queryParamsHandling: "merge" });
	}

	/**
	 * Ação do botão Carregar mais resultados
	 * Se for clicado pela 3a vez carrega o restante dos dados, independente da quantidade
	 */
	showMore() {

		this.nNextPage ++
		//se for clicado pela 3a vez carrega o restante dos dados
		if(this.nNextPage == 3){
			this.nPageSize = this.nTotalAndamento - this.listAndamentos.length
		}

		this.isShowMoreDisabled = true;
		this.getAndamentos();
	}

	/**
	 * Função responsavel pelo preenchimento das informações no breadcrumb de instancia
	 */
	getInfoInstancia() {
		if (this.codInst) {
			this.instanciaService
				.getDadosInstancia(this.filial + this.chaveProcesso, this.codInst)
				.subscribe(
					(retDadosInst: RetDadosInstancia) => {
						let descInst: string = retDadosInst.instancia
							+ ' - ' + retDadosInst.tpAcao 
							+ ' - ' + retDadosInst.nrProcesso;
						
						this.breadcrumb.items[
							this.breadcrumb.items.findIndex(i => i.label == this.litAnd.breadCrumb.instancia)
						].label = descInst
					}
				)
		}
	}
}
