import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AndamentosRoutingModule } from './andamentos-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AndamentosRoutingModule
  ]
})
export class AndamentosModule { }
