import { Injectable } from '@angular/core';
import { FwmodelService } from 'src/app/services/fwmodel.service';
import { JurigenericsService } from '../services/jurigenerics.service';


class Andamento{
	codAndamento: string;
	data: string;
	ato: string;
	fase: string;
	descricao: string;
	editaAndamento: Array<string>;
}

@Injectable({
  providedIn: 'root'
})
export class AndamentosService {

	isHideLoadingAndamentos: boolean = true;
	listAndamentos: Array<Andamento> = []
	mostraWidgetAndamentos: boolean = true;

	constructor(		
		private fwmodel: FwmodelService,
		private JuriGenerics: JurigenericsService,
	) { }

	/**
	 * método GET (ListAndamentos)
	 * consome o WS REST de integração com LegalProcess
	 */
	async getAndamentosResumo(filial, cajuri) {
		this.isHideLoadingAndamentos = false;
		this.listAndamentos = [];
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA100");
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.setPageSize('3');

		this.fwmodel.setFilter("NT4_FILIAL = '" + atob(filial) + "'" );
		this.fwmodel.setFilter("NT4_CAJURI = '" + cajuri + "'");
		this.fwmodel.setHeaderParam('orderBy', ' ORDER BY NT4_DTANDA DESC');

		this.fwmodel.get("Lista de Andamentos").subscribe((data) => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != '') {
				data.resources.forEach(item => {
					let itemAndamento = new Andamento();
					itemAndamento.codAndamento   = this.JuriGenerics.findValueByName(item.models[0].fields, "NT4_COD");
					itemAndamento.data           = this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(item.models[0].fields, "NT4_DTANDA"), "dd/mm/yyyy");
					itemAndamento.ato            = this.JuriGenerics.findValueByName(item.models[0].fields, "NT4_DATO");
					itemAndamento.fase           = this.JuriGenerics.findValueByName(item.models[0].fields, "NT4_DFASE");
					itemAndamento.descricao      = this.JuriGenerics.findValueByName(item.models[0].fields, "NT4_DESC");
					itemAndamento.editaAndamento = ['editarAnd'];
					this.listAndamentos.push(itemAndamento);
				})
			}
			this.isHideLoadingAndamentos = true;
		}, err => {
			if(err.message == 403) {
				this.mostraWidgetAndamentos = false
			}
		})
		return [this.listAndamentos, this.mostraWidgetAndamentos]
	}

}
