export class Andamento {
	data: string = '';
	ato: string = '';
	fase: string = '';
	instancia: string = '';
	teor: string = '';
	dataInclusao: string = '';
	dataAlteracao: string = '';
}
