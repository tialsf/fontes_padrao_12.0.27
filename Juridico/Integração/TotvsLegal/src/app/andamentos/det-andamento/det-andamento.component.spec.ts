import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetAndamentoComponent } from './det-andamento.component';

describe('DetAndamentoComponent', () => {
  let component: DetAndamentoComponent;
  let fixture: ComponentFixture<DetAndamentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetAndamentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetAndamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
