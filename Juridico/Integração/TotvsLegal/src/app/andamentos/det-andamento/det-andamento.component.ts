import { Component, OnInit, ViewChild } from '@angular/core';
import { FwmodelService } from 'src/app/services/fwmodel.service';
import { LegalprocessService, SysProtheusParam, sysParamStruct } from 'src/app/services/legalprocess.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Andamento } from 'src/app/andamentos/det-andamento/andamento';
import { TLUserInfo } from 'src/app/services/authlogin.service';
import { JurigenericsService, JurUTF8 } from 'src/app/services/jurigenerics.service';
import { Router, ActivatedRoute } from '@angular/router';
import {
	PoNotificationService, PoModalAction, PoModalComponent, PoI18nService,
	PoBreadcrumb, PoTableColumn, PoUploadComponent
} from '@po-ui/ng-components';
import { Subscription } from 'rxjs';
import { RoutingService } from 'src/app/services/routing.service';
import { adaptorFilterParam, FwmodelAdaptorService } from 'src/app/services/fwmodel-adaptor.service';
import { JurJustifModalComponent } from 'src/app/shared/jur-justif-modal/jur-justif-modal.component';
import { InstanciaService, RetDadosInstancia } from 'src/app/services/instancia.service';
import { AtoAdapterService, FaseAdapterService } from 'src/app/services/combos-fwmodel-adapter.service'

class DadosStatus {
	codigo: string;
	descricao: string;
	tipo: string;
}

class DataCombo {
	value: string;
	label: string;
}

class GridTarefa {
	data: string;
	hora: string;
	codTipo: string;
	tipo: string;
	descricao: string;
	status: string;
	codResultado: string;
	resultado: string;
	tarefaEdit: Array<string>;
	responsavel: string;
	codTarefa: string;
	pk: string;
}

@Component({
	selector: 'app-det-andamento',
	templateUrl: './det-andamento.component.html',
	styleUrls: ['./det-andamento.component.css']
})
export class DetAndamentoComponent implements OnInit {
	litDetAnd: any;
	formAnd: FormGroup;
	inscricao: Subscription;
	dadosStatus: DadosStatus = new DadosStatus();
	paramsProtheus: sysParamStruct[] = [];

	filtroAto:        string = "NRO_TIPO!='1' AND NRO_TIPO!='2'";
	codPrazo: string         = '';
	titleDet: string         = '';
	codAnd: string           = '';
	pk: string               = '';
	cajuri: string           = '';
	descAnd: string          = '';
	descTipo: string         = '';
	statusAtual: string      = '';
	newStatus: string        = '';
	pkFup: string            = '';
	chaveProcesso: string    = '';
	filial: string           = '';
	cajuriTitulo: string     = '';
	perfil: string           = '/processo/';
	breadcrumbPerfil: string = ''
	breadcrumbHome: string   = ''
	linkHome: string         = ''
	tituloPrazo: string      = ''
    btnExcluir: string       = '' 
    btnIncluir: string       = '' 
    btnAlterar: string       = '' 
	codInst: string;

	isHideLoadingDetAnd: boolean   = false;
	isHideLoadingInclusao: boolean = false;
	isHideLoadingDelete: boolean   = false;
	bOperacaoInclusao: boolean     = true;
	disableInclusaoPrazo: boolean  = false;
	isHideLoadingTarefas: boolean  = false;
	isProcEncerrado: boolean       = false;
	isProcesso: boolean            = false;
	isDisabledInst: boolean;

	instancias: Array<DataCombo>   = [];
	listTarefas: Array<GridTarefa> = [];
	status: Array<DadosStatus>     = [];

	uploadCount: number        = 0;
	uploadSuccessCount: number = 0;

	// Ações do modal de confirmação de exclusão(Andamento)
	fechar: PoModalAction = {
		action: () => {
			this.thfModal.close();
		},
		label: 'Fechar',
	};

	excluir: PoModalAction = {
		action: () => {
			this.excluir.loading = true;
			this.deleteVerify();
		},
		label: 'Excluir',
	};

	// ações do modal de confirmação de exclusão (Garantia)
	fecharConcluiModal: PoModalAction = {
		action: () => {
			this.newStatus = '';
			this.concluiModal.close();
		},
		label: ' ',
	};

	putConcluirTarefa: PoModalAction = {
		action: () => { this.verifyBaixa(); },
		label: ' '
	};

	/**
	 *  Colunas da widget de Tarefas 
	 * */
	colTarefas: Array<PoTableColumn> = [
		{ property: 'data', label: 'Data', width: '10%' },
		{ property: 'hora', label: 'Hora', width: '10%' },
		{ property: 'tipo', label: 'Tipo', width: '15%' },
		{
			property: 'status', type: 'label', width: '10%',
			labels: [
				{ value: '1', color: 'color-08', label: 'Pendente' },
				{ value: '2', color: 'color-11', label: 'Concluído' },
				{ value: '3', color: 'color-01', label: 'Cancelado' },
				{ value: '4', color: 'color-06', label: 'Em Aprovação' },
				{ value: '5', color: 'color-09', label: 'Em Andamento' },
				{ value: '6', color: 'color-07', label: 'Pendente' }  /* vermelho quando a tarefa está pendente e atrasada */
			]
		},
		{ property: 'responsavel', label: 'Responsável', width: '20%' },
		{ property: 'descricao', label: 'Descrição', width: '41%' },
		{
			property: 'tarefaEdit', label: ' ', width: '8%', type: 'icon',
			icons: [
				{	value: 'tarefa'  , icon: 'po-icon-edit', color: '#29b6c5'  , tooltip: 'Editar', 
					action: (value) => {
						this.updateTarefa(value);
					},
				},
				{	value: 'concluir', icon: 'po-icon-ok'  , color: '#color-07', tooltip: 'Concluir', 
					action: (value) => {
						this.openConcluiTarefa(value);
					},
				}
			]
		}
	];

	/**
	 * estrutura de navegação da URL atual
	 */
	public breadcrumb: PoBreadcrumb = {
		items: [
		],
	};

	// Adaptor do status de followup
	adaptorStatusFup: adaptorFilterParam = new adaptorFilterParam(
		"JURA017", "", "NQN_DESC", "NQN_DESC", "NQN_DESC", "NQN_COD", "", "Combo de Status do Prazo"
		);

	@ViewChild('docUpload') docUpload: PoUploadComponent;
	@ViewChild('docUploadNTA') docUploadNTA: PoUploadComponent;
	@ViewChild('modalAnd', { static: true }) thfModal: PoModalComponent;
	@ViewChild('modJustif', { static: true }) modalJustif: JurJustifModalComponent;

	// Modal para concluir tarefa
	@ViewChild('concluiTarefa', { static: true }) concluiModal: PoModalComponent;

	constructor(protected thfNotification: PoNotificationService,
		protected thfI18nService: PoI18nService,
		public fwModelAdaptorService: FwmodelAdaptorService,
		private fwmodel: FwmodelService,
		private legalprocess: LegalprocessService,
		private formBuilder: FormBuilder,
		private userInfo: TLUserInfo,
		private JuriGenerics: JurigenericsService,
		private router: Router,
		private route: ActivatedRoute,
		private sysProtheusParam: SysProtheusParam,
		private routingState: RoutingService,
		private instanciaService: InstanciaService,
		public  atoAdapterService: AtoAdapterService,
		public  faseAdapterService: FaseAdapterService
	) {	

		// Setups de adapters
		this.atoAdapterService.setup(new adaptorFilterParam("JURA051", "NRO_TIPO!='1' AND NRO_TIPO!='2'", "NRO_DESC", "NRO_DESC", "", "NRO_COD", "", "Combo de ato"))
		this.faseAdapterService.setup(new adaptorFilterParam("JURA011", "", "NQG_DESC", "NQG_DESC", "NQG_DESC", "NQG_COD", "", "Combo de fase"))

		routingState.loadRouting();

		if (this.route.snapshot.paramMap.get("codAnd") != undefined) {
			this.codAnd = atob(decodeURIComponent(this.route.snapshot.paramMap.get("codAnd"))); // cancelar inclusão de prazo
		}

		if (this.route.snapshot.queryParamMap.get("codInst") != undefined) {
			this.codInst = atob(decodeURIComponent(this.route.snapshot.queryParamMap.get("codInst")));
		}

		if(this.router.url.indexOf('/contrato/')>-1){
			this.filial = this.route.snapshot.paramMap.get('filContrato');
			window.sessionStorage.setItem('filial', atob(this.filial));
		
			this.chaveProcesso = this.route.snapshot.paramMap.get('codContrato');
			this.cajuri = atob(decodeURIComponent(this.chaveProcesso));
			this.isProcesso = false;
		}else{
			this.filial = this.route.snapshot.paramMap.get('filPro');
			window.sessionStorage.setItem('filial', atob(this.filial));

			this.chaveProcesso = this.route.snapshot.paramMap.get('codPro');
			this.cajuri = atob(decodeURIComponent(this.chaveProcesso));
			this.isProcesso = true;
		}

		// Chamada do serviço poI18n para buscar as constantes a partir do contexto 'andamentos' no arquivo JurTraducao.
		thfI18nService
			.getLiterals({ context: 'detAndamentos', language: thfI18nService.getLanguage() })
			.subscribe((litAnd) => {
				let titleDetNovo = '';
				let titleDetAltera = '';
				let breadCrumbAndamentos = '';
				this.litDetAnd     = litAnd;
				this.fechar.label  = this.litDetAnd.btnModalFechar;
				this.excluir.label = this.litDetAnd.btnModalExcluir;

				//-- Adiciona o código do cajuri no título Meu processo + área
				this.cajuriTitulo = this.cajuri.replace(new RegExp('^0*', 'g'), '')

					//Verifica se é um processo ou contrato
				if (this.isProcesso){
					this.perfil = '/processo/';
					this.breadcrumbPerfil = this.litDetAnd.breadCrumb.processo;
					this.breadcrumbHome   = this.litDetAnd.breadCrumb.home;
					titleDetNovo          = this.litDetAnd.tituloNovoAnd;
					titleDetAltera        = this.litDetAnd.tituloAlterarAnd;
					this.tituloPrazo      = this.litDetAnd.tituloPrazo;
					this.btnExcluir       = this.litDetAnd.btnExcluirAnd;
					this.btnIncluir       = this.litDetAnd.btnIncluirAnd;
					this.btnAlterar       = this.litDetAnd.btnAlterarAnd;
					breadCrumbAndamentos  = this.litDetAnd.breadCrumb.andamentos;
					this.linkHome         = '/home'
				}else{
					this.perfil = '/contrato/';
					this.breadcrumbPerfil = this.litDetAnd.breadCrumb.contrato
					this.breadcrumbHome   = this.litDetAnd.breadCrumb.homeContrato
					titleDetNovo          = this.litDetAnd.tituloNovoHis;
					titleDetAltera        = this.litDetAnd.tituloAlterarHis; 
					this.tituloPrazo      = this.litDetAnd.tituloPrazoHis;
					this.btnExcluir       = this.litDetAnd.btnExcluirHis;
					this.btnIncluir       = this.litDetAnd.btnIncluirHis;
					this.btnAlterar       = this.litDetAnd.btnAlterarHis;
					breadCrumbAndamentos  = this.litDetAnd.breadCrumb.historico;
					this.linkHome         = '/homeContratos'
				}
				this.breadcrumb.items = []
				this.breadcrumb.items.push({ label: this.breadcrumbHome, link: this.linkHome})
				this.breadcrumb.items.push({ label: this.breadcrumbPerfil, link: this.perfil + this.filial + '/' + this.chaveProcesso })
				this.breadcrumb.items.push({ label: breadCrumbAndamentos , link: this.perfil + this.filial + '/' + this.chaveProcesso+'/andamento' })
				
				if (this.hasInstancia()) {
					
					this.breadcrumb.items.push({
						label: this.litDetAnd.breadCrumb.instancia,
						action: () => {
							this.router
								.navigate(
									[this.perfil + this.filial + '/' + this.chaveProcesso+'/andamento'],
									{queryParamsHandling: 'merge'}
								);
						}
					})
				}

				if (this.JuriGenerics.changeUndefinedToEmpty(this.codAnd ) == '' ) {
					this.breadcrumb.items.push({ label: titleDetNovo })
					this.titleDet = titleDetNovo;
				} else {
					this.breadcrumb.items.push({ label: this.descAnd })
					this.titleDet = titleDetAltera;
					this.bOperacaoInclusao = false;
				}

				// seta as constantes para tradução do titulo das colunas do Grid de Tarefas
				this.colTarefas[0].label = this.litDetAnd.gridTarefas.colunaData;
				this.colTarefas[1].label = this.litDetAnd.gridTarefas.colunaHora;
				this.colTarefas[2].label = this.litDetAnd.gridTarefas.colunaTipo;
				this.colTarefas[3].label = this.litDetAnd.gridTarefas.colunaStatus;

				this.colTarefas[3].labels[0].label = this.litDetAnd.gridTarefas.labelPendente;
				this.colTarefas[3].labels[1].label = this.litDetAnd.gridTarefas.labelConcluido;
				this.colTarefas[3].labels[2].label = this.litDetAnd.gridTarefas.labelCancelado;
				this.colTarefas[3].labels[3].label = this.litDetAnd.gridTarefas.labelEmAprov;
				this.colTarefas[3].labels[4].label = this.litDetAnd.gridTarefas.labelEmAnd;
				this.colTarefas[3].labels[5].label = this.litDetAnd.gridTarefas.labelPendente;

				this.colTarefas[4].label = this.litDetAnd.gridTarefas.colunaResponsavel;
				this.colTarefas[5].label = this.litDetAnd.gridTarefas.colunaDescricao;

				this.fecharConcluiModal.label = this.litDetAnd.modalConcluirPt.modFechar;
				this.putConcluirTarefa.label = this.litDetAnd.modalConcluirPt.modConcluir;

			});
		this.isDisabledInst = !this.isProcesso || this.hasInstancia()
	}

	ngOnInit() {
		this.getInfoProcesso();
		this.getAndamentos();
		this.getTarefas();
		this.getInfoInstancia();

		this.disableInclusaoPrazo = (this.JuriGenerics.changeUndefinedToEmpty(this.codAnd) == '');
		this.createForm(new Andamento());
		
		// Busca os parâmetros do Protheus
		this.sysProtheusParam.getParamByArray(["MV_JTVENJF"]).subscribe((params) => {
			this.paramsProtheus = params;
		});
	}

	/**
	 * Busca as informações do processo
	 */
	getInfoProcesso() {
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA095");
		this.fwmodel.setFilter("NSZ_FILIAL='" + atob(this.filial) + "'");
		this.fwmodel.setFilter("NSZ_COD='" + this.cajuri + "'");
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.get("getInfoProcesso").subscribe(data => {
			let descResumoBreadCrumb = []
			if (this.isProcesso){
				descResumoBreadCrumb = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NSZ_DAREAJ")
			}else{
				descResumoBreadCrumb = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields,"NSZ_DESCON")
			}
			if (data.resources != "") {
				this.breadcrumb.items[1].label = this.breadcrumbPerfil + " "
					+ descResumoBreadCrumb
						+ " (" + this.cajuriTitulo + ")"
				this.isProcEncerrado = (this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NSZ_SITUAC") == "2");
			}
		});
	}

	/**
	 * Busca os andamentos
	 */
	getAndamentos() {
		if (this.JuriGenerics.changeUndefinedToEmpty(this.codAnd) == '') {
			if(this.isProcesso){
				this.getInstancia(this.codInst);
			};
			this.isHideLoadingDetAnd = true;

			this.formAnd = this.formBuilder.group({
				dataAndamento: [new Date()]
			});
		} else {

			this.fwmodel.restore();
			this.fwmodel.setModelo("JURA100");
			this.fwmodel.setFilter("NT4_COD='" + this.codAnd + "'");
			this.fwmodel.setFilter("NT4_FILIAL='" + atob(this.filial) + "'");
			this.fwmodel.setCampoVirtual(true);
			this.isHideLoadingDetAnd = false;

			this.fwmodel.get("Busca de dados do Andamento").subscribe((data) => {
				if (data.count == 0) {
					this.routingState.navigateToPreviousPage(btoa(this.codAnd), { queryParamsHandling: 'merge' })
					this.thfNotification.error(this.isProcesso ? this.litDetAnd.naoEncontrado : this.litDetAnd.naoEncontradoHis);
				} else {
					this.pk = data.resources[0].pk;
					this.cajuri = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT4_CAJURI");
					this.descAnd = this.JuriGenerics.makeDate(this.JuriGenerics
						.findValueByName(data.resources[0].models[0].fields, "NT4_DTANDA"), "dd/mm/yyyy");

					this.getInstancia(this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT4_CINSTA"));
					
					if(this.isProcesso){
						this.formAnd.patchValue({
							fase: this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT4_CFASE")
						});
					};
					this.formAnd.patchValue({
						teor: this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT4_DESC"),
						dataAndamento: this.JuriGenerics.makeDate(this.JuriGenerics
							.findValueByName(data.resources[0].models[0].fields, "NT4_DTANDA"), "yyyy-mm-dd"),
						ato: this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT4_CATO"),
						dataInclusao: this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT4_DTINCL")
					});

					this.breadcrumb.items[this.breadcrumb.items.length-1].label = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT4_DATO") + ' - ' +
					this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT4_DTANDA"), "dd/mm/yyyy");

					this.isHideLoadingDetAnd = true;
					return this.pk;
				}
			});
		}
	}

	// Responsável por remover um andamento
	delAndamento() {
		this.excluir.loading = true;
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA100");
		this.fwmodel.setFilter("NT4_COD='" + this.codAnd + "'");
		this.fwmodel.setFilter("NT4_FILIAL='" + atob(this.filial) + "'");

		this.fwmodel.delete(this.pk, "Exclusao de andamento").subscribe((data) => {
			if (data == true) {
				this.thfNotification.success(this.isProcesso ? this.litDetAnd.msgExclusao : this.litDetAnd.msgExclusaoHis);
				this.formAnd.reset(new Andamento());
				this.excluir.loading = false;
				this.routingState.navigateToPreviousPage(btoa(this.codAnd), { queryParamsHandling: 'merge' })
			} else {
				this.excluir.loading = false;
			}
		});
	}

	// Responsável por obter as instâncias do processo
	getInstancia(cCodAtu?: string) {
		this.legalprocess.restore();
		this.legalprocess.setCodProc(this.cajuri);
		this.legalprocess.getDetProcesso().subscribe((data) => {
			this.cajuri = data.processes[0].processId;
			if (data.processes != '') {
				data.processes[0].instance.forEach(item => {
					let dadosInsta = new DataCombo();
					dadosInsta = {
						label: item.displayName + ' - ' + item.processNumber,
						value: item.id
					};
					this.instancias.push(dadosInsta);
				});
			}
			if (cCodAtu != undefined) {
				this.formAnd.patchValue({
					instancia: cCodAtu
				});
			}
		});
	}

	/**
	* Responsável por obter os dados inputados no formulário de inclusão
	* de andamentos para serem usados após o submit
	*/
	createForm(andamento: Andamento) {
		if(this.isProcesso){
			this.formAnd = this.formBuilder.group({
				dataAndamento: [new Date(), Validators.compose([Validators.required])],
				ato: [andamento.ato],
				fase: [andamento.fase],
				instancia: [andamento.instancia],
				teor: [andamento.teor, Validators.compose([Validators.required])],
				dataInclusao: [""]
			});
		}else{
			this.formAnd = this.formBuilder.group({
				dataAndamento: [new Date(), Validators.compose([Validators.required])],
				ato: [andamento.ato],
				teor: [andamento.teor, Validators.compose([Validators.required])],
				dataInclusao: [""]
			});
		}
	}

	/**
	 * Função de inicio das validações e Commit.
	 */
	confirmVerify() {
		if (this.beforeSubmit()) {
			this.submitAnd();
		}
	}

	/**
 * Função de inicio das validações e Commit.
 */
	deleteVerify() {
		if (this.beforeDelete()) {
			this.delAndamento();
		}
	}

	/**
	 * Pré-validação.
	 */
	beforeSubmit() {
		let isSubmitable: boolean = false;
		if (this.formAnd.valid) {
			isSubmitable = true;
		}

		// Validação para apresentar o Modal de Justificativa, caso o processo esteja encerrado
		if (this.isProcEncerrado && this.sysProtheusParam.getParamValueByName(this.paramsProtheus, "MV_JTVENJF") == "1") {
			isSubmitable = false;
			this.modalJustif.openModal(this.cajuri,
				() => {
					if (this.modalJustif.isOk) {
						this.submitAnd();
					}
				});
		} else {
			isSubmitable = true;
		}

		return isSubmitable;
	}

	beforeDelete() {
		let isSubmitable: boolean = false;
		if (this.formAnd.valid) {
			isSubmitable = true;
		}

		// Validação para apresentar o Modal de Justificativa, caso o processo esteja encerrado
		if (this.isProcEncerrado && this.sysProtheusParam.getParamValueByName(this.paramsProtheus, "MV_JTVENJF") == "1") {
			isSubmitable = false;
			this.modalJustif.openModal(this.cajuri,
				() => {
					if (this.modalJustif.isOk) {
						this.delAndamento();
					}
				});
		} else {
			isSubmitable = true;
		}

		return isSubmitable;
	}

	/**
	* Obtem os dados do formulário no momento da inclusão e passa como parâmetro
	* para o postAndamento. Esses dados serão usados no body da requisisão
	* para realizar a inclusão através do FWModel.
	*/
	submitAnd() {
		this.isHideLoadingInclusao = true;
		if (this.bOperacaoInclusao) {
			this.insertAndamento();
		} else {
			this.updateAndamento();
		}
	}
	insertAndamento(){

		const cBody = this.bodyAndamento(this.formAnd.value, this.cajuri,this.userInfo.getCodUsuario())
		
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA100");
		this.fwmodel.setFilter("'NT4_CAJURI = '" + this.cajuri + "'");
		this.fwmodel.setFilter("'NT4_FILIAL = '" + atob(this.filial) + "'");
		this.fwmodel.post(cBody, "Gravação do andamento").subscribe((response) => {
			if (response != '') {
				this.codAnd = this.JuriGenerics.findValueByName(response.models[0].fields, "NT4_COD");
				this.thfNotification.success(this.isProcesso ? this.litDetAnd.msgInclusao : this.litDetAnd.msgInclusaoHis);

				this.submitNavigate();

				this.isHideLoadingInclusao = false;
			} else {
				this.isHideLoadingInclusao = false;
			}
		});
	}

	updateAndamento(){

		const cBody = this.bodyAndamento(this.formAnd.value, this.cajuri,this.userInfo.getCodUsuario())
		
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA100");
		this.fwmodel.setFilter("NT4_COD='" + this.codAnd + "'");
		this.fwmodel.setFilter("NT4_FILIAL='" + atob(this.filial) + "'");
		this.fwmodel.put(this.pk, cBody, "Atualização do andamento").subscribe((response) => {
			if (response != '') {
				this.codAnd = this.JuriGenerics.findValueByName(response.models[0].fields, "NT4_COD");
				this.thfNotification.success(this.isProcesso ? this.litDetAnd.msgAlteracao : this.litDetAnd.msgAlteracaoHis);
				this.submitNavigate();
				this.isHideLoadingInclusao = false;
			} else {
				this.isHideLoadingInclusao = false;
			}
		});

	}

	/**
	 * Cria o body de andamente de acordo com o perfil
	 * @param formAnd - Formulário de Andamento
	 * @param cajuri  - Código do assunto jurídico
	 * @param user    - Usuário logado
	 */
	bodyAndamento(formAnd: any, cajuri: string, user: string) {
		if(this.isProcesso){
			const bodyReq: Atualiza = {
				id: "JURA100",
				operation: this.bOperacaoInclusao ? 3 : 4,
				models: [
					{
						id: "NT4MASTER", modeltype: "FIELDS", fields: [
							{ id: "NT4_DESC", order: 1, value: JurUTF8.encode(formAnd.teor) },
							{ id: "NT4_CAJURI", order: 2, value: cajuri },
							{ id: "NT4_DTANDA", order: 3, value: formAnd.dataAndamento.replace(/-/g, '') },
							{ id: "NT4_CATO", order: 4, value: formAnd.ato },
							{ id: "NT4_CFASE", order: 5, value: formAnd.fase },
							{ id: "NT4_DTINCL", order: 6, value: this.JuriGenerics.makeDateProtheus() },
							{ id: "NT4_DTALTE", order: 7, value: this.JuriGenerics.makeDateProtheus() },
							{ id: "NT4_USUALT", order: 8, value: user },
							{ id: "NT4_CINSTA", order: 9, value: formAnd.instancia }
						]
					}
				]
			}; // Body da requisição
			return JSON.stringify(bodyReq);
		}else{
			const bodyReq: historicoAndamento = {
				id: "JURA100",
				operation: this.bOperacaoInclusao ? 3 : 4,
				models: [
					{
						id: "NT4MASTER", modeltype: "FIELDS", fields: [
							{ id: "NT4_DESC", order: 1, value: JurUTF8.encode(formAnd.teor) },
							{ id: "NT4_CAJURI", order: 2, value: cajuri },
							{ id: "NT4_DTANDA", order: 3, value: formAnd.dataAndamento.replace(/-/g, '') },
							{ id: "NT4_CATO", order: 4, value: formAnd.ato },
							{ id: "NT4_DTINCL", order: 6, value: this.JuriGenerics.makeDateProtheus() },
							{ id: "NT4_DTALTE", order: 7, value: this.JuriGenerics.makeDateProtheus() },
							{ id: "NT4_USUALT", order: 8, value: user }
						]
					}
				]
			}; // Body da requisição
			return JSON.stringify(bodyReq);
		}
	}

	/**
	 * Verifica se há documentos, se tiver ele faz o upload dos arquivos, caso contrario redireciona para a pagina anterior
	 */
	submitNavigate() {
		// Valida se há documentos a serem enviados
		if (this.docUpload.hasOwnProperty('currentFiles') && this.docUpload.currentFiles.length > 0) {
			this.sendDocs();
		} else {
			this.routingState.navigateToPreviousPage(btoa(this.codAnd), { queryParamsHandling: 'merge' })
		}
	}

	/**
	 * Retorna para a tela anterior
	 */
	onCancel() {
		this.routingState.navigateToPreviousPage(btoa(this.codAnd), { queryParamsHandling: 'merge' })
	}

	/**
	 * Função que verifica se existe o parametro de instancia
	 */
	hasInstancia(){
		return this.codInst != undefined && this.codInst != null && this.codInst != ''
	}

	/**
	 * Responsável por trazer os Resultados de Follow-ups
	 */
	async getResultado() {
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA017");
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.get("Busca do resultado do Prazo").subscribe((data) => {
			data.resources.forEach(itensStatus => {
				this.dadosStatus = new DadosStatus();
				this.dadosStatus.codigo = this.JuriGenerics.findValueByName(itensStatus.models[0].fields, "NQN_COD");
				this.dadosStatus.descricao = this.JuriGenerics.findValueByName(itensStatus.models[0].fields, "NQN_DESC");
				this.dadosStatus.tipo = this.JuriGenerics.findValueByName(itensStatus.models[0].fields, "NQN_TIPO");
				this.status.push(this.dadosStatus);
			});
		});
		return this.status;
	}

	/**
	 * Responsável por trazer as tarefas do processo
	 */
	getTarefas() {
		this.isHideLoadingTarefas = false;

		if (this.JuriGenerics.changeUndefinedToEmpty(this.codAnd) != '') {
			this.getResultado().then(() => {
				this.listTarefas = new Array<GridTarefa>();
				this.fwmodel.restore();
				this.fwmodel.setModelo("JURA106");
				this.fwmodel.setCampoVazio(true);
				this.fwmodel.setCampoVirtual(true);
				this.fwmodel.setFirstLevel(false);

				this.fwmodel.setFilter("NTA_FILIAL = '" + atob(this.filial) + "'");
				this.fwmodel.setFilter("NTA_CAJURI = '" + this.cajuri + "' ");
				this.fwmodel.setFilter("NTA_CANDAM = '" + this.codAnd + "' ");

				this.fwmodel.get("Busca de prazos do andamento").subscribe((data) => {
					data.resources.forEach(itensTarefas => {
						let responsaveis: string = "";
						const itemGridTarefa: GridTarefa = new GridTarefa();
						let indexStatus: number = -1;
						let formatHora: string = "";

						itemGridTarefa.pk = itensTarefas.pk;
						itemGridTarefa.codTarefa = this.JuriGenerics.findValueByName(itensTarefas.models[0].fields, "NTA_COD");
						itemGridTarefa.data = this.JuriGenerics.makeDate(this.JuriGenerics
							.findValueByName(itensTarefas.models[0].fields, "NTA_DTFLWP"), "dd/mm/yyyy");
						itemGridTarefa.codTipo = this.JuriGenerics.findValueByName(itensTarefas.models[0].fields, "NTA_CTIPO");
						itemGridTarefa.tipo = this.JuriGenerics.findValueByName(itensTarefas.models[0].fields, "NTA_DTIPO");
						itemGridTarefa.descricao = this.JuriGenerics.findValueByName(itensTarefas.models[0].fields, "NTA_DESC");
						itemGridTarefa.codResultado = this.JuriGenerics.findValueByName(itensTarefas.models[0].fields, "NTA_CRESUL");
						itemGridTarefa.resultado = this.JuriGenerics.findValueByName(itensTarefas.models[0].fields, "NTA_DRESUL");

						if (this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(itensTarefas.models[0].fields, "NTA_HORA")) != "") {
							formatHora = this.JuriGenerics.findValueByName(itensTarefas.models[0].fields, "NTA_HORA");
							formatHora = formatHora.substring(0, 2) + ':' + formatHora.substring(2, 4);
						}
						itemGridTarefa.hora = formatHora;
						indexStatus = this.status.findIndex(x => x.codigo == itemGridTarefa.codResultado);

						if (indexStatus > -1) {
							if (this.status[indexStatus].tipo == "1" && itemGridTarefa.data <= 
							this.JuriGenerics.makeDate(this.JuriGenerics.makeDateProtheus(), 'dd/mm/yyyy')) {
								itemGridTarefa.status = "6";
							} else {
								itemGridTarefa.status = this.status[indexStatus].tipo;
							}
						}

						// Pega os responsáveis
						this.JuriGenerics.findModelItensByName(itensTarefas.models[0].models, 'NTEDETAIL').forEach(
							itemModelo => {
								responsaveis += this.JuriGenerics.findValueByName(itemModelo.fields, "NTE_SIGLA") + ' / ';
							}
						);

						if (responsaveis != "") {
							itemGridTarefa.responsavel = responsaveis.substring(0, responsaveis.length - 2);
						};
						
						itemGridTarefa.tarefaEdit = ['tarefa', 'concluir'];

						this.listTarefas.push(itemGridTarefa);
					});
					this.isHideLoadingTarefas = true;
				});
			});
		} else {
			this.isHideLoadingTarefas = true;
		}
	}

	/**
	 * Chama a tela de anexos
	 */
	callAnexos() {
		this.router.navigate(["./anexos"], { relativeTo: this.route, queryParamsHandling: "merge" });
	}

	/**
	 * Realiza o envio dos documentos
	 */
	sendDocs() {
		if (this.docUpload.currentFiles.length > 0) {
			this.docUpload.sendFiles();
		}
	}

	/**
	 * Carrega as informações para o POST do upload de arquivos
	 * @param file 
	 */
	uploadFiles(file) {
		file.data = {
			cajuri: this.cajuri.toString(),
			entidade: "NT4",
			codEntidade: this.codAnd.toString()
		};
	}

	/**
	 * Trata para redirecionar após concluir todos os anexos
	 */
	uploadSucess() {
		this.uploadSuccessCount++;

		if (this.uploadSuccessCount == this.docUpload.currentFiles.length) {
			this.routingState.navigateToPreviousPage(btoa(this.codAnd), { queryParamsHandling: 'merge' })
		}
	}

	/**
	 * Chama o SelectFiles do po-upload
	 */
	getDocuments() {
		this.docUpload.selectFiles();
	}

	/**
	 * Chama a tela de cadastro de tarefas
	 */
	createTarefa() {
		this.router.navigate(["./tarefa/cadastro"], { relativeTo: this.route, queryParamsHandling: 'merge' });
	}

	/**
	 * Abre a Det-tarefa em modo de Update
	 */
	updateTarefa(value?: GridTarefa) {
		if (value == undefined) {
			this.router.navigate(["./tarefa/cadastro"], { relativeTo: this.route, queryParamsHandling: 'merge' });
		} else {
			this.router.navigate(["./tarefa/" + btoa(value.codTarefa)], { relativeTo: this.route, queryParamsHandling: 'merge' });
		}
	}

	/**
	 * Chama o modal de conclusão do fup
	 * @param value 
	 */
	openConcluiTarefa(value?: GridTarefa) {
		this.descTipo = value.tipo;
		this.statusAtual = value.resultado;
		this.pkFup = value.pk;
		this.codPrazo = value.codTarefa;
		this.concluiModal.open();
	}

	/**
	 * Realiza a baixa do Fup
	 */
	baixTask() {
		this.concluiModal.primaryAction.loading = true;
		if (this.newStatus != '' && this.newStatus != undefined) {
			this.fwmodel.restore();
			this.fwmodel.setModelo('JURA106');
			this.fwmodel.put(this.pkFup, this.bodybaixaTask(), "Atualização do prazo").subscribe((data) =>	 {
				if (data == "") {
					this.concluiModal.primaryAction.loading = false;
				} else {
					//Valida se há documentos a serem enviados
					if (this.docUploadNTA.hasOwnProperty('currentFiles') && this.docUploadNTA.currentFiles.length > 0) {
						this.uploadCount = this.docUploadNTA.currentFiles.length;
						this.sendDocsNTA();
					} else {
						this.concluiModal.primaryAction.loading = false;
						this.thfNotification.success(this.litDetAnd.modalConcluirPt.modSucesso);
						this.newStatus = '';
						this.concluiModal.close();
						this.getTarefas();
						this.getAndamentos();
					}
				}
			});
		} else {
			this.concluiModal.primaryAction.loading = false;
			this.thfNotification.warning(this.litDetAnd.modalConcluirPt.modSemStatus);
		}

	}

	/**
	 * Body para conclusão do Fup
	 */
	bodybaixaTask() {
		const bodyReq: ConcluiFup = {
			id: "JURA106",
			operation: 4,
			models: [
				{
					id: "NTAMASTER", modeltype: "FIELDS", fields: [
						{ id: "NTA_CRESUL", order: 1, value: this.newStatus }
					]
				}
			]
		}; // Body da requisição

		return JSON.stringify(bodyReq);
	}

	/**
	 * Realiza o envio dos documentos de prazos
	 */
	sendDocsNTA() {
		if (this.docUploadNTA.currentFiles.length > 0) {
			this.docUploadNTA.sendFiles();
		}
	}

	/**
	 * Chama o SelectFiles do po-upload de prazos
	 */
	getDocumentsNTA() {
		this.docUploadNTA.selectFiles();
	}

	/**
	 * Carrega as informações para o POST do upload de arquivos de prazos
	 * @param file 
	 */
	uploadFilesNTA(file) {
		file.data = {
			cajuri: this.cajuri.toString(),
			entidade: "NTA",
			codEntidade: this.codPrazo.toString()
		};
	}
	/**
	 * Trata para redirecionar após concluir todos os anexos de prazos
	 */
	uploadSucessNTA() {
		this.uploadSuccessCount++;

		if (this.uploadSuccessCount == this.docUploadNTA.currentFiles.length) {
			this.thfNotification.success(this.litDetAnd.modalConcluirPt.modSucesso);
			this.concluiModal.close();
			this.docUploadNTA.clear();
			this.getTarefas();
			this.concluiModal.primaryAction.loading = false;
		}
	}

	verifyBaixa() {
		if (this.beforeSubmitBaixa()) {
			this.baixTask();
		}
	}


	beforeSubmitBaixa() {
		let isSubmitable: boolean = false;

		isSubmitable = true;
		// Validação para apresentar o Modal de Justificativa, caso o processo esteja encerrado
		if (isSubmitable) {
			if (this.isProcEncerrado && this.sysProtheusParam.getParamValueByName(this.paramsProtheus, "MV_JTVENJF") == "1") {
				isSubmitable = false;
				this.modalJustif.openModal(this.cajuri,
					() => {
						if (this.modalJustif.isOk) {
							this.baixTask();
						}
					});
			} else {
				isSubmitable = true;
			}
		}

		return isSubmitable;
	}
	
	/**
	 * Função responsavel pelo preenchimento das informações no breadcrumb de instancia
	 */
	getInfoInstancia() {
		if (this.codInst) {
			this.instanciaService
				.getDadosInstancia(this.filial + this.chaveProcesso, this.codInst)
				.subscribe(
					(retDadosInst: RetDadosInstancia) => {
						let descInst: string = retDadosInst.instancia
							+ ' - ' + retDadosInst.tpAcao 
							+ ' - ' + retDadosInst.nrProcesso;
							
						this.breadcrumb.items[
							this.breadcrumb.items.findIndex(i => i.label == this.litDetAnd.breadCrumb.instancia)
						].label = descInst
					}
				)

		}
	}
}

/**
 * Estrutura para Post e Put de Andamento
 */
interface Atualiza {
	id: string;
	operation: number;
	models: [{
		id: string, modeltype: string, fields: [
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string }]
	}];
}

/**
 * Estrutura para Post e Put de Andamento/historico - Cotratos
 */
interface historicoAndamento {
	id: string;
	operation: number;
	models: [{
		id: string, modeltype: string, fields: [
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string }]
	}];
}

/**
 * Estrutura para conclusão de fup
 */
interface ConcluiFup {
	id: string;
	operation: number;
	models: [{
		id: string, modeltype: string, fields: [
			{ id: string, order: number, value: string }
		]
	}];
}
