/**
* Arquivo de constantes para tradução da página detalhe do andamento
*/

//-- Constantes para títulos das colunas do Grid de Tarefas
const gridTarefas = {
	
	colunaTipo:        "Tipo",
	colunaData:        "Data", 
	colunaHora:        "Hora", 
	colunaStatus:      "Status",
	colunaResponsavel: "Responsável",
	colunaDescricao:   "Descrição",
	labelPendente:     "Pendente",
	labelConcluido:    "Concluído",
	labelCancelado:    "Cancelado",
	labelEmAprov:      "Em Aprovação",
	labelEmAnd:        "Em Andamento"
}

const modalExclusao = {
	msgExclusao: "Deseja excluir esse registro?",
	titleExclusao: "Atenção!"
}

//-- Constantes para o bread crumb
const breadCrumb = {
	home:             'Meus processos',
	processo:         'Meu processo',
	andamentos:       'Andamentos',
	historico:        'Histórico',
	contrato:         'Meu contrato',
	homeContrato:     'Meus contratos',
	instancia:        'Instância'
}

const modalConcluirPt = {
	modFechar:      "Fechar",
	modConcluir:    "Concluir",
	modSucesso:     "Compromisso baixado com sucesso",
	modTitle:       "Concluir Compromisso",
	modCompromisso: "Compromisso",
	modStatusAtual: "Status Atual",
	modNovoStatus:  "Novo Status",
	modSemStatus:   "Favor Informar o Novo Status."

}
//-- Constantes principais
export const detAndamentosPt = {
	tituloNovoAnd:    'Novo andamento',
	tituloNovoHis:    'Novo histórico',
	tituloAlterarAnd: 'Alterar andamento',
	tituloAlterarHis: 'Alterar histórico',
	tituloPrazo:      'Prazos a cumprir deste andamento',	
	tituloPrazoHis:   'Prazos a cumprir deste histórico',
	inputData:        'Data',
	inputAto:         'Ato',
	inputFase:        'Fase',
	inputInsta:       'Instância',
	inputTeor:        'Teor',
	carregando:       'Carregando...',
	loadTarefa:       "Carregando tarefas",
	btnPrazo:         'Incluir prazo',
	btnExcluirAnd:    'Excluir andamento',
	btnIncluirAnd:    'Incluir andamento',
	btnAlterarAnd:    'Alterar andamento',
	btnExcluirHis:    'Excluir histórico',
	btnIncluirHis:    'Incluir histórico',
	btnAlterarHis:    'Alterar histórico',
	btnSelecionar:    'Selecionar Arquivo',
	btnCancelar:      'Cancelar',
	btnModalFechar:   'Cancelar',
	btnModalExcluir:  'Excluir',
	msgInclusao:      'Andamento incluído com sucesso!',
	msgAlteracao:     'Andamento atualizado com sucesso!',
	msgExclusao:      'Andamento excluído com sucesso!',
	msgInclusaoHis:   'Histórico incluído com sucesso!',
	msgAlteracaoHis:  'Histórico atualizado com sucesso!',
	msgExclusaoHis:   'Histórico excluído com sucesso!',
	naoEncontrado:    'Esse andamento não foi localizado na base ou pode ter sido excluido.',
	naoEncontradoHis: 'Esse histórico não foi localizado na base ou pode ter sido excluido.',
	breadCrumb:       breadCrumb,
	gridTarefas:      gridTarefas,
	modal:            modalExclusao,
	modalConcluirPt:  modalConcluirPt
}