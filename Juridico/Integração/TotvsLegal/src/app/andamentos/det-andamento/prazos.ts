export class Prazos {
	dataPrazo:   string = '';
	atoPrazo:    string = '';
	tipoPrazo:   string = '';
	statusPrazo: string = '';
	respPrazo:   string = '';
	escPrazo:    string = '';
	prepPrazo:   string = '';
	descPrazo:   string = '';
}
