const anxProcesso = {
	title:  "Anexos do processo",
	list:   "Processos",
	detail: "processo"
}

const anxAndamento = {
	title:  "Anexos do andamento",
	list:   "Andamentos",
	detail: "andamento"
}

const anxTarefas = {
	title:  "Anexos da tarefa",
	list:   "Tarefas",
	detail: "tarefa"
}

const anxGarantia = {
	title:  "Anexos da garantia",
	list:   "Garantias",
	detail: "garantia"
}

const anxDespesa = {
	title:  "Anexos da despesa",
	list:   "Despesas",
	detail: "despesa"
}

const anxPedido = {
	title:  "Anexos da pedido",
	list:   "Pedidos",
	detail: "pedido"
}

const tarefaWithAnd = {
	subNivelList:   "Prazos",
	subNivelDetail: "prazo",
	title:          "Anexos do prazo",
	list:           "Andamentos",
	detail:         "andamento"
}

//-- Constantes para o breadcrumb de Acompanhamento de Processos 
const breadcrumbAnexos = {

	brHomeProc:        "Meus processos",
	brProcesso:        "Meu processo",
	brHomeCont:        "Meus contratos",
	brContrato:        "Meu contrato",
	brAndamento:       "Andamento",
	brAndamentos:      "Andamentos",
	brAnexosAndamento: "Anexos do Andamento",
	brDespesa:         "Despesa",
	brDespesas:        "Despesas",
	brAnexosDespesa:   "Anexos da despesa",
	brGarantia:        "Garantia",
	brInstancia:       "Instância",
	garantia:          anxGarantia,
	despesa:           anxDespesa,
	tarefa:            anxTarefas,
	pedido:            anxPedido,
	andamento:         anxAndamento,
	processo:          anxProcesso,
	tarefaWithAnd:     tarefaWithAnd

}


//-- Constantes para os botões na tela de anexos 
const botoesAnexos = {
	incluir:      "Incluir anexo",
	download:     "Fazer download",
	excluir:      "Excluir anexo",
	pesquisar:    "Pesquisar",
	pesquisarAnx: "Pesquisar anexo"

}
//-- Constantes para as informações exibidas em tela
const infosAnexos = {
	infoFormato: "Formatos adotados: .PDF",
	infoLimite:  "Limite de tamanho por arquivo: 200 MB",
	confirmExc:  "Tem certeza que deseja excluir os anexos abaixo : ",
	atentionExc: "ATENÇÃO:",
	listExc:     "Todos os anexos selecionados serão excluidos.",
	dragFiles:   "Arraste os arquivos aqui para fazer upload",
	dropFiles:   "Solte os arquivos aqui para fazer upload",

}
const colunaAnx = {
	nome:  "Nome",
	tipo:  "Tipo",
	local: "Local",
	data:  "Data de inclusão"

}
//-- Constantes Gerais para o componente de Anexos do Processo
export const anexosPt = {	
	
	anexosProc:      "Anexos do processo",
	anexosCont:      "Anexos do contrato",
	anexosAnd:       "Anexos do andamento",
	excluirAnx:      "Excluir anexos?",
	loadAnx:         "Carregando anexos",
	breadcrumbAnx:   breadcrumbAnexos,
	btnAnexos:       botoesAnexos,
	infoAnexos:      infosAnexos,
	colAnexo:        colunaAnx,
	notifyExclusion: "Para excluir o arquivo {0} utilize a rotina de {1} ",
	successExc:      "Documento {0} foi excluído com sucesso",
}