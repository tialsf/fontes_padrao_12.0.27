import { Component, OnInit, ViewChild } from '@angular/core';
import { 
	PoBreadcrumb, 
	PoNotificationService, 
	PoTableComponent,
	PoTableColumn, 
	PoI18nService,   
	PoModalAction,
	PoModalComponent,
	PoI18nPipe,
	PoUploadLiterals,
	PoUploadComponent
} from '@po-ui/ng-components';
import { ActivatedRoute, Router } from '@angular/router';
import { LegalprocessService } from '../services/legalprocess.service';
import { Subscription } from 'rxjs';
import { FwmodelService } from '../services/fwmodel.service';
import { JurigenericsService, JurBase64 } from '../services/jurigenerics.service';
import { InstanciaService, RetDadosInstancia } from '../services/instancia.service';

class fields{
	nomeCampo: string;
	tipo: string;
	constructor(nomeCampo: string, tipoCampo: string){
		this.nomeCampo = nomeCampo
		this.tipo = tipoCampo
	}
}

class breadCrumbStruct{
	entidade: string;
	codigo: string;
	urlPath: string;
}

class Attachments{
	number:       string;
	codeEntity:   string;
	extension:    string;
	nameEntity:   string;
	codeDocument: string;
	nameDocument: string;
	entity:       string;
	dateInsert:   string;
}

@Component({
  selector: 'app-anexos',
  templateUrl: './anexos.component.html',
  styleUrls: ['./anexos.component.css']
})
export class AnexosComponent implements OnInit {
	
	[x: string]:        any;
	litAnexos:          any;
	fileUrl:            any;
  
	codInst:            string;
	titleAnex:          string = '';
	codAnd:             string = '';
	cajuri:             string = '';
	area:               string = '';
	searchKey:          string = '';
	codDoc:             string = '';
	entidade:           string = '';
	nomeRotaEntidade:   string = '';
	codEntidade:        string = '';
	dragndropAnexos:    string = '';
	chaveAssJur:        string = '';
	filial:             string = '';
	currentURL:         string = '';
	anexosTitle:        string = '';
	homeTitle:          string = '';
	resumoTitle:        string = '';
	cajuriTitulo:       string = '';
	linkHome:           string = '';

	selectedAnx:        Array<string>        = [];
	subNiveis:          breadCrumbStruct[]   = [];
	listAnx:            Array<any>;
	selectedItems:      Array<any>;
	anexos:             Array<string>;
	columnAnx:          Array<PoTableColumn>;
	inscricao:          Subscription;
	attachmentLiterals: PoUploadLiterals;

	uploadSuccessCount: number = 0;
	
	isHideLoadingAnexos: boolean = false;	

	primaryActAnexo: PoModalAction = {
		label:'Excluir',
		action: () => {
			this.confirmDel(this.selectedItems)
			this.thfModal.close();
		}
	};
	secondaryActAnexo: PoModalAction = {
		label:'Cancelar',
		action: () => {
			this.thfModal.close();
		}
	};

 	/**
	 * estrutura de navegação da URL atual
	 */
	public breadcrumb: PoBreadcrumb = {
		items: [
		],
	};

	@ViewChild(PoTableComponent, { static: true }) thfTable: PoTableComponent;
	@ViewChild(PoModalComponent, { static: true }) thfModal: PoModalComponent;
	@ViewChild(PoUploadComponent, { static: true }) poUpload: PoUploadComponent;

	/**
	 * colunas da tabela de valores
	 */
	ValuesColumns: Array<PoTableColumn>  = [
		{ property: 'nameDocument', label: ' ', width: '25%'},
		{ property: 'extension' ,   label: ' ', width: '25%'},
		{ property: 'nameEntity',   label: ' ', width: '25%'},
		{ property: 'dateInsert',   label: ' ', width: '25%', type: 'date'},
	];

	constructor(
		public thfNotification: PoNotificationService,
		protected router: Router,
		protected thfI18nService: PoI18nService,
		private legalprocess: LegalprocessService,
		private fwModel: FwmodelService,
		private route: ActivatedRoute,
		private JuriGenerics: JurigenericsService,
		private thfI18nPipe: PoI18nPipe,
		private instanciaService: InstanciaService
	) { 
		let childBreadCrumb: any;
		let urlPathChild: string = "";

		if(this.router.url.indexOf('/contrato/')>-1){			
			this.filial = this.route.snapshot.paramMap.get('filContrato');
			this.chaveAssJur = this.route.snapshot.paramMap.get('codContrato');
			this.currentURL = '/contrato/';
		}else{
			this.filial = this.route.snapshot.paramMap.get('filPro');
			this.chaveAssJur = this.route.snapshot.paramMap.get('codPro');
			this.currentURL = '/processo/';
		}		
		window.sessionStorage.setItem('filial', atob(this.filial));

		let codAnd = this.route.snapshot.paramMap.get('codAnd');
		let codDes = this.route.snapshot.paramMap.get('codDesp');
		let codGar = this.route.snapshot.paramMap.get('codGar');
		let codPed = this.route.snapshot.paramMap.get('codPedido');
		let codTar = this.route.snapshot.paramMap.get('codTar');

		if (this.route.snapshot.queryParamMap.get('codInst') != undefined) {
			this.codInst = atob(decodeURIComponent(this.route.snapshot.queryParamMap.get('codInst')));
		}

		this.cajuri = this.chaveAssJur;
		
		// Define qual é a entidade 
		if (codTar != undefined && codAnd != undefined){
			let subNivel = new breadCrumbStruct()

			this.entidade = 'NTA'
			this.codEntidade = codTar,
			urlPathChild = 'tarefa'
			
			subNivel.codigo = codAnd
			subNivel.entidade = "NT4"
			subNivel.urlPath = "andamento"
			this.subNiveis.push(subNivel)

		} else if(codAnd != undefined){
			this.entidade = "NT4"
			this.codEntidade = codAnd
			urlPathChild = "andamento"
		} else if (codDes != undefined) {
			this.entidade = "NT3"
			this.codEntidade = codDes
			urlPathChild = "despesa"
		} else if (codGar != undefined) {
			this.entidade = "NT2"
			this.codEntidade = codGar
			urlPathChild = "garantia"
		} else if (codPed != undefined){
			this.entidade = "NSY"
			this.codEntidade = codPed
			urlPathChild = "pedido"
		} else if (codTar != undefined){
			this.entidade = "NTA"
			this.codEntidade = codTar
			urlPathChild = "tarefa"
		} else {
			this.entidade = "NSZ"
			this.codEntidade = this.cajuri
		}

		this.cajuri = atob(decodeURIComponent(this.cajuri))
		this.codEntidade = atob(decodeURIComponent(this.codEntidade))
        //-- Adiciona o código do cajuri no título Meu processo + área
        this.cajuriTitulo = this.cajuri.replace(new RegExp('^0*', 'g'), '')

		//-- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'anexos' no arquivo JurTraducao.			
		thfI18nService.getLiterals( { context: 'anexos', language: thfI18nService.getLanguage() } )
		.subscribe((litAnexos) => {
			this.litAnexos = litAnexos;

			if(this.router.url.indexOf('/contrato/')>-1){			
				this.anexosTitle = this.litAnexos.anexosCont;
				this.homeTitle   = this.litAnexos.breadcrumbAnx.brHomeCont;
				this.resumoTitle = this.litAnexos.breadcrumbAnx.brContrato;
				this.linkHome    = '/homeContratos'
			}else{
				this.anexosTitle = this.litAnexos.anexosProc;
				this.homeTitle   = this.litAnexos.breadcrumbAnx.brHomeProc;
				this.resumoTitle = this.litAnexos.breadcrumbAnx.brProcesso;	
				this.linkHome    = '/home'
			}
			
			//-- seta as constantes para tradução do titulo das colunas da tabela de anexos
			this.ValuesColumns[0].label = this.litAnexos.colAnexo.nome;
			this.ValuesColumns[1].label = this.litAnexos.colAnexo.tipo;
			this.ValuesColumns[2].label = this.litAnexos.colAnexo.local;
			this.ValuesColumns[3].label = this.litAnexos.colAnexo.data;
			
			// Verifica qual é o Literals a ser utilizado a partir da Entidade 
			switch (this.entidade) {
				case "NSZ":
					this.breadcrumb.items = [
						{ label: this.homeTitle   , link: this.linkHome },
						{ label: this.resumoTitle , link: this.currentURL + this.filial + "/" + this.chaveAssJur},
						{ label: this.anexosTitle }
					];
					this.titleAnex = this.anexosTitle;
					break;
				case "NT4":
					childBreadCrumb = this.litAnexos.breadcrumbAnx.andamento;
					break;
				case "NT3":
					childBreadCrumb = this.litAnexos.breadcrumbAnx.despesa;
					break;
				case "NT2":
					childBreadCrumb = this.litAnexos.breadcrumbAnx.garantia;
					break;
				case "NSY":
					childBreadCrumb = this.litAnexos.breadcrumbAnx.pedido;
					break;
				case "NTA":
						if (this.subNiveis.length > 0){
							childBreadCrumb = this.litAnexos.breadcrumbAnx.tarefaWithAnd;
						} else {
							childBreadCrumb = this.litAnexos.breadcrumbAnx.tarefa;
						}
					break;
				default:
					break;
			}

			if (this.entidade != "NSZ"){
				if (this.subNiveis.length > 0){
					this.breadcrumb.items = [
						{ label: this.homeTitle                , link: '/' },
						{ label: this.resumoTitle              , link: this.currentURL + this.filial + '/' + this.chaveAssJur},
						{ label: childBreadCrumb.list          , link: this.currentURL + this.filial + '/' + this.chaveAssJur + '/' + this.subNiveis[0].urlPath}
					]

					if(this.codInst != undefined){
						this.breadcrumb.items = [...this.breadcrumb.items,
							{
								label: this.litAnexos.breadcrumbAnx.brInstancia,
								action: () => {
									this.router
										.navigate(
											[ this.currentURL + this.filial + '/' + this.chaveAssJur + '/' + this.subNiveis[0].urlPath ], 
											{queryParamsHandling: 'merge'}
										);
								}
							},
							{
								label: childBreadCrumb.detail,
								action: () => {
									this.router
										.navigate(
											[this.currentURL + this.filial + '/' + this.chaveAssJur + '/' + this.subNiveis[0].urlPath + '/' + this.subNiveis[0].codigo], 
											{queryParamsHandling: 'merge'}
										);
								}
							},
							{
								label:  childBreadCrumb.subNivelDetail,
								action: () => {
									this.router
										.navigate(
											[this.currentURL + this.filial + '/' + this.chaveAssJur + '/' + this.subNiveis[0].urlPath + '/' + this.subNiveis[0].codigo + '/' +urlPathChild + '/' + btoa(this.codEntidade)], 
											{queryParamsHandling: 'merge'}
										);
								}
							}
						]
				
					} else {
						this.breadcrumb.items = [...this.breadcrumb.items,
							{label: childBreadCrumb.detail, link:this.currentURL + this.filial + '/' + this.chaveAssJur + '/' + this.subNiveis[0].urlPath + '/' + this.subNiveis[0].codigo},
							{label: childBreadCrumb.subNivelDetail, link: this.currentURL + this.filial + '/' + this.chaveAssJur + '/' + this.subNiveis[0].urlPath + '/' + this.subNiveis[0].codigo + '/' +urlPathChild + '/' + btoa(this.codEntidade)}
						]
					}
					
				} else {
					this.breadcrumb.items = [
						{ label: this.homeTitle        , link: '/' },
						{ label: this.resumoTitle      , link: this.currentURL + this.filial + '/' + this.chaveAssJur},
						{ label: childBreadCrumb.list  , link: this.currentURL + this.filial + '/' + this.chaveAssJur  + '/' + urlPathChild}
					]

					if(this.codInst != undefined){
						this.breadcrumb.items = [...this.breadcrumb.items,
							{
								label: this.litAnexos.breadcrumbAnx.brInstancia,
								action: () => {
									this.router
										.navigate(
											[this.currentURL + this.filial + '/' + this.chaveAssJur  + '/' + urlPathChild], 
											{queryParamsHandling: 'merge'}
										);
								}
							},
							{
								label: childBreadCrumb.detail,
								action: () => {
									this.router
										.navigate(
											[this.currentURL + this.filial + '/' + this.chaveAssJur  + '/' + urlPathChild + '/' + btoa(this.codEntidade)], 
											{queryParamsHandling: 'merge'}
										);
								}
							}
						]
					} else {
						this.breadcrumb.items = [...this.breadcrumb.items,
							{ label: childBreadCrumb.detail, link: this.currentURL + this.filial + '/' + this.chaveAssJur  + '/' + urlPathChild + '/' + btoa(this.codEntidade)},
						];
					}
					
				}
				
				this.breadcrumb.items = [...this.breadcrumb.items, { label: childBreadCrumb.title }]
				this.titleAnex = childBreadCrumb.title;
			}

			this.attachmentLiterals = {
				dragFilesHere: this.litAnexos.infoAnexos.dragFiles,
				dropFilesHere: this.litAnexos.infoAnexos.dropFiles,
				selectFilesOnComputer: ''
			};
		});	
	}

	ngOnInit() {
		this.searchKey = ""
		this.columnAnx = this.ValuesColumns;
		this.getDocs();
		this.getInfoBreadCrumb(1,"NSZ");
		this.getInfoInstancia();
		
		let order: number = this.codInst != undefined ? 4 : 3
		
		if (this.subNiveis.length > 0){
			this.getInfoBreadCrumb(order, this.subNiveis[0].entidade, atob(this.subNiveis[0].codigo));
			this.getInfoBreadCrumb(order+1);
		} else if (this.entidade != "NSZ"){
			this.getInfoBreadCrumb(order);
		}
	}

	/**
	 * Busca as informações para colocar os valores dinamicos no Breadcrumb
	 * @param breadCrumbIndex 
	 * @param entidade 
	 */
	getInfoBreadCrumb(breadCrumbIndex:number = 1,entidade: string = this.entidade, codFiltro: string = this.codEntidade): void{
		let modelo:                string   = "";
		let descricaoBreadCrumb:   string   = "";
		let fieldFilter:           string   = "";
		let defaultBreadCrumbText: string   = "";
		let afterBreadCrumbText:   string   = "";
		let camposDescr:           fields[] = [];
		// Define campos a serem concatenados
		switch (entidade) {
			case "NSZ":
				modelo = "JURA095"
				fieldFilter = "NSZ_COD"
				if(this.router.url.indexOf('/contrato/')>-1){	
					camposDescr.push(new fields("NSZ_DESCON","C"))
				}else{
					camposDescr.push(new fields("NSZ_DAREAJ","C"))
				}
				codFiltro = this.cajuri
				defaultBreadCrumbText = this.resumoTitle + " "
				afterBreadCrumbText = " ("+this.cajuriTitulo+")"
				break;
			case "NT3":
				modelo = "JURA099"
				fieldFilter = "NT3_COD"
				camposDescr.push(new fields("NT3_DTPDES","C"))
				camposDescr.push(new fields("NT3_DATA","D"))
				break;
			case "NT2":
				modelo = "JURA098"
				fieldFilter = "NT2_COD"
				camposDescr.push(new fields("NT2_DTPGAR","C"))
				camposDescr.push(new fields("NT2_DATA","D"))
				break;
			case "NT4":
				modelo = "JURA100"
				fieldFilter = "NT4_COD"
				camposDescr.push(new fields("NT4_DATO","C"))
				camposDescr.push(new fields("NT4_DTANDA","D"))
				break;
			case "NTA":
				modelo = "JURA106"
				fieldFilter = "NTA_COD"
				camposDescr.push(new fields("NTA_DTIPO","C"))
				camposDescr.push(new fields("NTA_DTFLWP","D"))
				break;
			case "NSY":
				modelo = "JURA094"
				fieldFilter = "NSY_COD"
				camposDescr.push(new fields("NSY_DPEVLR","C"))
				camposDescr.push(new fields("NSY_PEDATA","D"))
			default:
				modelo = ""
				break;
		}

		this.fwModel.restore();
		this.fwModel.setModelo(modelo);
		this.fwModel.setFilter(fieldFilter + " = '" + codFiltro + "' ");
		this.fwModel.setCampoVirtual(true);
		this.fwModel.get("getInfo" + entidade).subscribe(data => {
			if (data.resources != "") {
				let arrFields = data.resources[0].models[0].fields;
				camposDescr.forEach(campo => {
					if (campo.tipo == "D"){
						descricaoBreadCrumb += this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(arrFields, campo.nomeCampo), "dd/mm/yyyy") + " - ";
					} else {
						descricaoBreadCrumb += this.JuriGenerics.findValueByName(arrFields, campo.nomeCampo) + " - ";
					}
				});
			}
			if (descricaoBreadCrumb != "") {
				descricaoBreadCrumb = descricaoBreadCrumb.substring(0, descricaoBreadCrumb.length - 3);
			}
			this.breadcrumb.items[breadCrumbIndex].label = defaultBreadCrumbText + descricaoBreadCrumb + afterBreadCrumbText
		})
	}

  	/**
	* Busca todos os anexos de todas as entidades relacionadas ao processo selecionado
	*/
	getDocs (){
		this.isHideLoadingAnexos = false;
		this.listAnx = [];
		
		this.legalprocess.restore();
		this.legalprocess.setCodProc(this.cajuri); 
		
		if (this.entidade != "NSZ"){
			this.legalprocess.setFilter("codEntidade", this.codEntidade)
		}

		if(this.searchKey != ''){
			this.legalprocess.setSearchKey(this.searchKey.toString()) //conteúdo pesquisado nos campos NUM_DOC /NUM_EXTENS
		}
		this.legalprocess.get("process/" + this.cajuri + "/docs/" + this.entidade).subscribe((data) => {
			if (data.attachments != '' && data.attachments != undefined){
				data.attachments.forEach(data =>{
					let docs: Attachments = new Attachments();

					docs.number       = data.number;
					docs.nameEntity   = data.nameEntity;
					docs.codeDocument = data.codeDocument;
					docs.codeEntity   = data.codeEntity;
					docs.entity       = data.entity;
					docs.extension    = data.extension;
					docs.nameDocument = data.nameDocument;
					docs.dateInsert   = this.JuriGenerics.makeDate(data.dateInsert,"yyyy-mm-dd");

					this.listAnx.push(docs)
				})
			}else{
				this.listAnx = [];
			}	
			this.isHideLoadingAnexos = true; 
		});
		return this.listAnx;
	}

	/**
	* ação do botão Pesquisa
	*/	  
	search(event: any) {
		//13 = ao pressionar o enter
		if (event.keyCode == 13){
			this.getDocs();
		}
	}

	/**
	 * Chama o modal para a exclusão
	 */
	deleteAnx(){
		this.selectedItems = [];
		this.selectedItems = this.thfTable.getSelectedRows();

		if (this.selectedItems.length > 0) {
			this.anexos = this.itemsSelec(this.selectedItems);
			this.thfModal.open()
		}
	}

	/**
	 * Verifica e concatena os anexos a serem excluidos
	 * @param selectedItems 
	 */
	confirmDel(selectedItems: Array<any>) {
		this.codDoc = '';
		var exclusionMsg: any = "";
		selectedItems.forEach(item => {
			//validar se a entidade principal é a mesma do anexo a ser excluido
			if (item.entity == this.entidade){
				this.codDoc += item.codeDocument + ','
			}else{
				exclusionMsg = this.thfI18nPipe.transform(this.litAnexos.notifyExclusion, [item.nameDocument, item.nameEntity]);
				this.thfNotification.error(exclusionMsg.toString());
			}
		});
		
		if (this.codDoc != ''){
			this.codDoc = this.codDoc.substring(0,this.codDoc.length -1 )
			this.deleteAttachments(this.codDoc);		
		}
	}

	/**
	 * Retorna os itens selecionados
	 * @param selectedItems 
	 */
	itemsSelec(selectedItems: Array<any>) {
		this.selectedAnx = [];
		selectedItems.forEach(item => {
			this.selectedAnx.push(item.nameDocument + item.extension);
		});
		return this.selectedAnx
	}

	/**
	 * Realiza a operação de Exclusão dos Anexos
	 * @param codAnx 
	 */
	deleteAttachments(codAnx){
		this.legalprocess.restore();
		this.legalprocess.setCodNUM(codAnx);
		this.legalprocess.setCodProc(this.cajuri)
		this.legalprocess.setFilter('nomeEnt',this.entidade)
		this.legalprocess.setFilter('codEntidade', this.codEntidade)
		this.legalprocess.deleteDocs().subscribe((data) => {
			if (data.hasOwnProperty("attachments") && (data.attachments.length > 0)){
				data.attachments.forEach(item => {
					this.thfNotification.success(this.thfI18nPipe.transform(this.litAnexos.successExc, [item.nameDocument]) );
				});
			};
			this.getDocs()	
		});
	} 
	
	/**
	 * Responsável pela ação do botão download
	 */
	downloadSelectedAttachments(){
		this.selectedItems = [];
		this.selectedItems = this.thfTable.getSelectedRows();
		if (this.selectedItems.length > 0) {
			this.selectedItems.forEach(item => {
				this.confirmDownloadSelected(item.codeDocument);	
			});
		}
	}

	/**
	 * Responsável por percorrer cada codigo dos itens selecionados 
	 * @param codAnx 
	 */
	confirmDownloadSelected(codAnx){
		this.legalprocess.restore();
		this.legalprocess.setCodNUM(codAnx);
		this.legalprocess.setEntidade(this.entidade);
		this.legalprocess.setCodEntidade(this.codEntidade);
		this.legalprocess.downloadFileAttached().subscribe((fileData) => {
			if(fileData.attachments != '' ) {
				fileData.attachments.forEach(item => {
					if(item.filedata != ""){
						var blob = JurBase64.toUTF8(item.filedata, 'application/octet-stream');
						item.fileUrl = window.URL.createObjectURL(blob);
					}else{
						item.fileUrl = item.fileurl;
					}
					var fileLink = document.createElement('a');
					fileLink.href = item.fileUrl;
					fileLink.download = item.namefile; //força o nome do download
					fileLink.target = "_blank"; //força a abertura em uma nova página
					document.body.appendChild(fileLink);
					fileLink.click(); //gatilha o evento do click
				});
			}
		});
	}

	/**
	 * carrega as informações para o POST do upload de arquivos
	 * @param file 
	 */
	uploadFiles(file){
		file.data = {cajuri: this.cajuri.toString(),
					entidade: this.entidade.toString(),
					codEntidade: this.codEntidade.toString()};
	}

	/**
	 * recarrega os anexos após o upload do arquivo
	 */
	uploadSucess(){
		this.uploadSuccessCount++;
		if (this.uploadSuccessCount == this.poUpload.currentFiles.length){
			this.poUpload.clear();
			this.getDocs(); 
		}
	}

	/**
	 * seleciona arquivos para upload
	 */
	incluirAnx(){
		this.poUpload.selectFiles()
	}
	
	/**
	 * Função responsavel pelo preenchimento das informações no breadcrumb de instancia
	 */
	getInfoInstancia() {
		if (this.codInst) {
			this.instanciaService
				.getDadosInstancia(this.filial + this.chaveAssJur, this.codInst)
				.subscribe(
					(retDadosInst: RetDadosInstancia) => {
						let descInst: string = retDadosInst.instancia
							+ ' - ' + retDadosInst.tpAcao 
							+ ' - ' + retDadosInst.nrProcesso;
							
						this.breadcrumb.items[
							this.breadcrumb.items.findIndex(i => i.label == this.litAnexos.breadcrumbAnx.brInstancia)
						].label = descInst
					}
				)

		}
	}
}
