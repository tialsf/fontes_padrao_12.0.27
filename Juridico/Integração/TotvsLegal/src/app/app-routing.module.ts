import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ChartsModule } from 'ng2-charts';

const routes: Routes = [
	{
		path: '',
		loadChildren: () => import('./menu/menu.module').then(m => m.MenuModule),
	},
	{
		path: 'login',
		component: LoginComponent,
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes), ChartsModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
