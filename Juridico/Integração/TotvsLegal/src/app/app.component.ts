import { Component } from '@angular/core';

/**
 * Classe inicial do sistema.
 */
@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent{

}
