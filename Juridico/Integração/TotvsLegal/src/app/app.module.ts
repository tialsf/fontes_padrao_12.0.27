import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PoModule, PoI18nConfig, PoI18nService, PoI18nModule } from '@po-ui/ng-components';
import { PoComponentsModule } from "@po-ui/ng-templates";
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TLUserInfo, AuthloginService } from "./services/authlogin.service";
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './services/auth-guard.service';
import { i18nConfig } from './JurTraducao.constant';
import { UploadInterceptor } from './upload.interceptor';
import { SharedModule } from "./shared/shared.module";
// Localização
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';

registerLocaleData(localePt, 'pt');

@NgModule({
	declarations: [
		AppComponent,
		LoginComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		AppRoutingModule,
		PoModule,
		PoComponentsModule,
		HttpClientModule,
		FormsModule,
		SharedModule,
		PoI18nModule.config(i18nConfig)
	],
	providers: [AuthloginService,TLUserInfo,AuthGuardService, Title,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: UploadInterceptor,
			multi: true,
		},
		{
			provide: LOCALE_ID,
			useValue: 'pt-BR'
		}],
	bootstrap: [AppComponent]
})


export class AppModule { }
