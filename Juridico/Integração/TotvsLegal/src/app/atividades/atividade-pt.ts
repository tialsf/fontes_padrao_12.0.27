const breadcrumb = {
	home: 'Home',
	atividades: 'Painel de atividades',
};

const filtro = {
	disponivel: 'Filtros disponíveis: ',
	myActivity: 'Minhas tarefas',
	myProcFav: 'Meus processos favoritos',
	myTeam: 'Minha equipe',
	myProcess: 'Processos que cuido',
};

const modal = {
	fechar: 'Fechar',
	titulo: 'Atividades - {0} de {1} de {2}',
	openTarefa: 'Ir para tarefa',
	atividades: 'Atividades',
	hora: 'Hora marcada',
	semAtivid: 'Não há nenhuma atividade',
};

const status = {
	pendAtraso: 'Pendente em atraso',
	pendAndam: 'Pendente em andamento',
	reagendado: 'Reagendado',
	cancelado: 'Cancelado',
	efetuado: 'Efetuado',
	emAprov: 'Em aprovação',
	emAndam: 'Em andamento',
	legenda: 'Legenda',
};

/**
 * Constantes principais
 */
export const atividadesPT = {
	breadcrumb,
	filtro,
	modal,
	status,
	loading: 'Carregando atividades...',
};
