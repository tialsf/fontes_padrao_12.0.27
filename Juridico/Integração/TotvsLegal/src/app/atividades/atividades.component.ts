import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { LegaltaskService } from '../services/legaltask.service';
import { JurigenericsService } from '../services/jurigenerics.service';
import {
	PoModalAction,
	PoModalComponent,
	PoListViewAction,
	PoTreeViewItem,
	PoBreadcrumb,
	PoI18nService,
	PoI18nPipe,
} from '@po-ui/ng-components';
import { JurconsultasService } from '../services/jurconsultas.service';
import { IntervaloDataCalend, EventsDay, Atividade, CoresStatusAtiv } from './atividades';
import { isNullOrUndefined } from 'util';

@Component({
	selector: 'app-atividades',
	templateUrl: './atividades.component.html',
	styleUrls: ['./atividades.component.css'],
})
export class AtividadesComponent implements OnInit {
	litAtividades;
	breadcrumb: PoBreadcrumb;

	tituloMdlAtividades: string = '';
	styleCalendar: string = '';

	nHeightWidget: number = 0;

	hideLoadingAtividades: boolean = true;
	toggleLegenda: boolean = true;
	classSizeLg: boolean = true;
	classSizeSm: boolean = false;

	dateCalendar: Date = new Date();

	listAtividades: Array<Atividade> = [];
	listItensResult: Array<Atividade> = [];
	actions: Array<PoListViewAction> = [];
	listMembrosEquipe: Array<PoTreeViewItem> = [];

	listStatusFup: Array<CoresStatusAtiv> = [
		{
			tipoSituac: 'EFETUADO',
			desc: '',
			corCalendar: '#56b96b',
			corPortinari: 'color-10', // verde
		},
		{
			tipoSituac: 'PENDENTE EM ATRASO',
			desc: '',
			corCalendar: '#c64840',
			corPortinari: 'color-07', // vermelho
		},
		{
			tipoSituac: 'CANCELADO',
			desc: '',
			corCalendar: '#ab43c8',
			corPortinari: 'color-05', // roxo
		},
		{
			tipoSituac: 'REAGENDADO',
			desc: '',
			corCalendar: '#abc249',
			corPortinari: 'color-09', // amarelo
		},
		{
			tipoSituac: 'PENDENTE EM ANDAMENTO',
			desc: '',
			corCalendar: '#5843c8',
			corPortinari: 'color-04', // roxo escuro
		},
		{
			tipoSituac: 'EM APROVACAO',
			desc: '',
			corCalendar: '#0c9abe',
			corPortinari: 'color-01', // azul
		},
		{
			tipoSituac: 'EM ANDAMENTO',
			desc: '',
			corCalendar: '#ea9b3e',
			corPortinari: 'color-08', // laranja
		},
	];

	itemsTreeView: Array<PoTreeViewItem> = [
		{
			label: '',
			value: 'MINHAS TAREFAS',
			selected: true,
		},
		{
			label: '',
			value: 'FAVORITOS',
			selected: false,
		},
		{
			label: '',
			value: 'MINHA EQUIPE',
			selected: false,
			expanded: true,
			subItems: [],
		},
		{
			label: '',
			value: 'PROCESSOS QUE CUIDO',
			selected: false,
		},
	];

	@ViewChild('mdlAtividades', { static: true }) mdlListaAtividades: PoModalComponent;

	// ações do modal de fechar
	mdlAtiviBtnFechar: PoModalAction = {
		action: () => {
			this.tituloMdlAtividades = '';
			this.mdlListaAtividades.close();
		},
		label: '',
	};

	constructor(
		private legaltask: LegaltaskService,
		private jurGenerics: JurigenericsService,
		private poI18nPipe: PoI18nPipe,
		private poI18nService: PoI18nService,
		private jurConsultas: JurconsultasService
	) {
		// Traduções
		this.poI18nService
			.getLiterals({ context: 'atividades', language: poI18nService.getLanguage() })
			.subscribe(litAtiv => {
				this.litAtividades = litAtiv;

				this.onResize();
				this.setActions();
				this.setBreadCrumb();
				this.setTraducao();
			});
	}

	ngOnInit() {
		this.listAtividades = [];
		this.getMembrosEquipe();
		this.refreshTask('date');
	}

	/**
	 * Função responsável por traduzir as labels
	 */
	private setTraducao() {
		// Tradução dea label do modal
		this.mdlAtiviBtnFechar.label = this.litAtividades.modal.fechar;

		// Tradução de label da tree view
		this.itemsTreeView[0].label = this.litAtividades.filtro.myActivity;
		this.itemsTreeView[1].label = this.litAtividades.filtro.myProcFav;
		this.itemsTreeView[2].label = this.litAtividades.filtro.myTeam;
		this.itemsTreeView[3].label = this.litAtividades.filtro.myProcess;

		// tradução de label das actions do list view
		this.actions[0].label = this.litAtividades.modal.openTarefa;

		// tradução legenda
		this.listStatusFup[0].desc = this.litAtividades.status.efetuado;
		this.listStatusFup[1].desc = this.litAtividades.status.pendAtraso;
		this.listStatusFup[2].desc = this.litAtividades.status.cancelado;
		this.listStatusFup[3].desc = this.litAtividades.status.reagendado;
		this.listStatusFup[4].desc = this.litAtividades.status.pendAndam;
		this.listStatusFup[5].desc = this.litAtividades.status.emAprov;
		this.listStatusFup[6].desc = this.litAtividades.status.emAndam;
	}

	/**
	 * Método responsável pela criação do BreadCrumb
	 */
	private setBreadCrumb() {
		this.breadcrumb = {
			items: [
				{ label: this.litAtividades.breadcrumb.home, link: '/' },
				{ label: this.litAtividades.breadcrumb.atividades },
			],
		};
	}

	/**
	 * Método responsável pela montagem das ações utilizadas no po-list-view
	 */
	private setActions() {
		this.actions = [
			{
				label: '',
				type: 'link',
				icon: 'po-icon-edit',
				action: (item: Atividade) => this.openTarefa(item),
			},
		];
	}

	/**
	 * Função responsável por renderizar o painel de atividade,
	 * conforme resolução do usuário.
	 */
	@HostListener('window:resize', ['$event'])
	onResize() {
		const nHeightTotal: number = window.innerHeight;
		const nWidthTotal: number = window.innerWidth;
		this.styleCalendar = 'height: ' + (nHeightTotal - 200).toString() + 'px;';

		if (nHeightTotal <= 700 || nWidthTotal <= 1100) {
			this.nHeightWidget = nHeightTotal - 160;
			this.classSizeSm = true;
			this.classSizeLg = false;
		} else {
			this.nHeightWidget = nHeightTotal - 180;
			this.classSizeLg = true;
			this.classSizeSm = false;
		}
	}

	/**
	 * Função responsável por apresentar a lista de atividades do dia selecionado.
	 * @param event - Informações que o calendário retorna para o dia selecionado.
	 */
	selectDay(event: EventsDay) {
		if (event.events) {
			this.tituloMdlAtividades = this.poI18nPipe.transform(this.litAtividades.modal.titulo, [
				event.day,
				this.jurGenerics.getStrMonth(event.month + 1),
				event.year,
			]);
			this.listItensResult = event.events;
		}
		if (this.tituloMdlAtividades) {
			this.mdlListaAtividades.open();
		}
	}

	/**
	 * Função responsável por buscar as tarefas por período
	 * @param dataInicial - Data inicial de busca.
	 * @param dataFinal   - Data final de busca.
	 * @param filterFup   - Fitro da treeview a ser aplicada.
	 */
	async getTarefas(
		dataInicial: string = '',
		dataFinal: string = '',
		filterFup: string = ''
	): Promise<Array<Atividade>> {
		const listAtividades: Array<Atividade> = [];
		this.hideLoadingAtividades = false;

		this.legaltask.restore();
		this.legaltask.setFilter('pageSize', '300000');
		this.legaltask.setFilter('dataIni', dataInicial);
		this.legaltask.setFilter('dataFim', dataFinal);
		this.legaltask.setFilter('filterFup', filterFup);
		this.legaltask.setFilter('lWSTLegal', 'true');
		const data = await this.legaltask.get('fup', 'Busca Tarefas').toPromise();

		if (this.jurGenerics.changeUndefinedToEmpty(data.data) !== '') {
			data.data.forEach(item => {
				const itemTarefa = {} as Atividade;

				const horaFup: string = item.hora === ':' ? '00:01' : item.hora;
				const descFup: string =
					item.desc.length >= 50 ? item.desc.substring(0, 200) + ' ...' : item.desc;

				itemTarefa.id = listAtividades.length + 1;
				itemTarefa.title = item.tipofu[0].descri;
				if (item.hora === ':') {
					itemTarefa.hasHour = false;
				} else {
					itemTarefa.hasHour = true;
				}

				itemTarefa.descFup = descFup;
				itemTarefa.startDate = new Date(
					this.jurGenerics.makeDate(item.dtfup, 'yyyy-mm-dd') + 'T' + horaFup
				);
				itemTarefa.endDate = new Date(
					this.jurGenerics.makeDate(item.dtfup, 'yyyy-mm-dd') + 'T' + horaFup
				);
				itemTarefa.createdAt = new Date(
					this.jurGenerics.makeDate(item.dtfup, 'yyyy-mm-dd') + 'T' + horaFup
				);

				itemTarefa.filial = this.jurGenerics.changeUndefinedToEmpty(item.filial);
				itemTarefa.cajuri = item.cajuri;
				itemTarefa.codigo = this.jurGenerics.changeUndefinedToEmpty(item.idFu);
				itemTarefa.status = item.result;
				itemTarefa.descStatus = item.descresul;
				itemTarefa.situacProc = item.situacao;

				const iStatusFup: number = this.listStatusFup.findIndex(
					x =>
						x.tipoSituac ===
						this.getStatusAtiv(item.tipoStatus, item.dtfup.toString(), item.reagendado)
				);
				itemTarefa.color = this.listStatusFup[iStatusFup].corCalendar;
				itemTarefa.corTagName = this.listStatusFup[iStatusFup].corPortinari;

				itemTarefa.styleModal = 'background-color: ' + itemTarefa.color + ';';

				itemTarefa.horaFup = horaFup;
				itemTarefa.dataFup = this.jurGenerics.makeDate(item.dtfup, 'dd/mm/yyyy');
				itemTarefa.minhasTarefas = filterFup === 'MINHAS TAREFAS';
				itemTarefa.favoritos = filterFup === 'FAVORITOS';
				itemTarefa.minhaEquipe = filterFup === 'MINHA EQUIPE';
				itemTarefa.procCuido = filterFup === 'PROCESSOS QUE CUIDO';
				const listFilterFup: Array<string> = [
					'MINHAS TAREFAS',
					'FAVORITOS',
					'MINHA EQUIPE',
					'PROCESSOS QUE CUIDO',
				].filter(val => {
					return val === filterFup;
				});
				if (!listFilterFup.length || listFilterFup[0] === 'MINHA EQUIPE') {
					if (listFilterFup[0] === 'MINHA EQUIPE') {
						itemTarefa.membroEquipe = item.codPart;
					} else {
						itemTarefa.membroEquipe = filterFup;
					}
					const iResp: number = item.resp.findIndex(
						x => x.codPart === itemTarefa.membroEquipe
					);
					itemTarefa.responsavel = item.resp[iResp].nome;
					itemTarefa.desc = item.resp[iResp].sigla;
				} else {
					itemTarefa.membroEquipe = '';
					const iResp: number = item.resp.findIndex(x => x.codPart === item.codPart);
					itemTarefa.responsavel = item.resp[iResp].nome;
					itemTarefa.desc = item.resp[iResp].sigla;
				}

				if (item.usrLogged) {
					itemTarefa.iconTag = 'po-icon-user';
				}
				itemTarefa.tipoAsjJur =
					item.tipoAsjOrig.trim() === '' ? item.tipoasj : item.tipoAsjOrig;

				if (!listFilterFup.length || listFilterFup[0] === 'MINHA EQUIPE') {
					if (this.validMembroEquipe(itemTarefa.membroEquipe)) {
						listAtividades.push(itemTarefa);
					}
				} else {
					listAtividades.push(itemTarefa);
				}
				this.hideLoadingAtividades = true;
			});
			this.hideLoadingAtividades = true;
		} else {
			this.hideLoadingAtividades = true;
		}
		return listAtividades;
	}

	/**
	 * Função que faz abertura da tarefa.
	 * @param infoFup - Estrutura de informação da atividade selecionada.
	 */
	openTarefa(infoFup: Atividade) {
		const filial: string = infoFup.filial;
		const codProc: string = infoFup.cajuri;
		const codFup: string = infoFup.codigo;
		const assJur: string = infoFup.tipoAsjJur === '001' ? 'processo/' : 'contrato/';

		window.open(
			assJur +
				encodeURIComponent(btoa(filial)) +
				'/' +
				encodeURIComponent(btoa(codProc)) +
				'/tarefa/' +
				encodeURIComponent(btoa(codFup))
		);
	}

	/**
	 * Ação ao alterar a data do calendário
	 * @param changeData data alterada do calendário
	 */
	changeDataCalend(changeData?: Date) {
		this.dateCalendar = changeData;
		this.refreshTask('date');
	}

	/**
	 * Função responsável por determinar o período de busca das tarefas,
	 * conforme o mês que consta no calendário.
	 */
	refreshTask(change: string, changeFiltro?: PoTreeViewItem) {
		// Marca como des/selecionado o item
		if (changeFiltro !== undefined) {
			const idTree = this.itemsTreeView.findIndex(x => x.value === changeFiltro.value);

			if (idTree > -1) {
				this.itemsTreeView[idTree].selected = changeFiltro.selected;
				if (idTree === 2) {
					this.itemsTreeView[idTree].subItems.forEach(item => {
						item.selected = changeFiltro.selected;
					});
				}
			} else {
				this.itemsTreeView[2].selected = false;
				if (
					this.itemsTreeView[2].subItems.filter(x => x.selected === changeFiltro.selected)
						.length ===
					this.itemsTreeView[2].subItems.length - 1
				) {
					this.itemsTreeView[2].selected = changeFiltro.selected;
				}
				const idTreeEquipe = this.itemsTreeView[2].subItems.findIndex(
					x => x.value === changeFiltro.value
				);
				this.itemsTreeView[2].subItems[idTreeEquipe].selected = changeFiltro.selected;
			}
		}
		this.changeFilter(change, changeFiltro);
	}

	/**
	 * Remove ou adiciona atividades ao calendário de acordo com o filtro
	 * @param change tipo de alteração date|filterRemove|filterAdd
	 * @param changeFiltro item da treeview clicado
	 */
	changeFilter(change: string, changeFiltro?: PoTreeViewItem) {
		if (change === 'filterRemove') {
			this.removeFilter(changeFiltro);
		} else {
			this.addFilter(changeFiltro, change);
		}
	}

	/**
	 * Remove atividades do calendário
	 * @param changeFiltro item da treeview clicado
	 */
	removeFilter(changeFiltro: PoTreeViewItem) {
		this.listAtividades = this.listAtividades.filter(ativ => {
			return !this.validRemove(ativ, changeFiltro.value.toString());
		});
	}

	/**
	 * Valida se a atividade também pertence a outro filtro antes de exclui-la
	 * @param atividade atividade a ser removida
	 * @param filtro filtro desmarcado
	 */
	validRemove(atividade: Atividade, filtro: string) {
		let canRemove: boolean = false;
		switch (filtro) {
			case 'MINHAS TAREFAS':
				atividade.minhasTarefas = false;
				canRemove =
					!atividade.favoritos &&
					!atividade.minhaEquipe &&
					!atividade.procCuido &&
					atividade.membroEquipe === '';
				break;
			case 'FAVORITOS':
				atividade.favoritos = false;
				canRemove =
					!atividade.minhasTarefas &&
					!atividade.minhaEquipe &&
					!atividade.procCuido &&
					atividade.membroEquipe === '';
				break;
			case 'MINHA EQUIPE':
				atividade.minhaEquipe = false;
				atividade.membroEquipe = '';
				canRemove =
					!atividade.favoritos && !atividade.minhasTarefas && !atividade.procCuido;
				break;
			case 'PROCESSOS QUE CUIDO':
				atividade.procCuido = false;
				canRemove =
					!atividade.favoritos &&
					!atividade.minhaEquipe &&
					!atividade.minhasTarefas &&
					atividade.membroEquipe === '';
				break;
			default:
				if (atividade.membroEquipe === filtro) {
					atividade.membroEquipe = '';
					atividade.minhaEquipe = false;
					canRemove = !atividade.favoritos && !atividade.procCuido;
				}
				break;
		}

		return canRemove;
	}

	/**
	 * Adiciona atividades do calendário
	 * @param changeFiltro item da treeview clicado
	 * @param change tipo de alteração filterRemove|filterAdd
	 */
	addFilter(changeFiltro: PoTreeViewItem, change: string) {
		let filtro: string = '';

		filtro = changeFiltro === undefined ? 'MINHAS TAREFAS' : changeFiltro.value.toString();

		if (change === 'date') {
			this.listAtividades = [];
			this.itemsTreeView.forEach(item => {
				if (item.selected) {
					this.callGetTarefas(item.value.toString());
				}
				if (item.value === 'MINHA EQUIPE' && !item.selected) {
					item.subItems.forEach(subItem => {
						if (subItem.selected) {
							this.callGetTarefas(subItem.value.toString());
						}
					});
				}
			});
		} else {
			this.callGetTarefas(filtro);
		}
	}

	/**
	 * Chama a função que obtém as atividades de acordo com o filtro
	 * @param filtro filtro marcado
	 */
	callGetTarefas(filtro: string) {
		const dataCalend: IntervaloDataCalend = this.intervaloDataCalend();
		this.getTarefas(dataCalend.dataIni, dataCalend.dataFin, filtro).then(atividades => {
			this.validaIncListAtividades(atividades, filtro);
		});
	}
	/**
	 * Valida duplicação de atividades
	 * @param atividades fups a serem incluídos no calendário
	 * @param filtro filtro marcado
	 */
	validaIncListAtividades(atividades: Array<Atividade>, filtro: string) {
		atividades.forEach(ativ => {
			const i: number = this.listAtividades.findIndex(x => x.codigo === ativ.codigo);
			if (i === -1) {
				this.listAtividades = this.listAtividades.concat([ativ]);
			} else {
				this.marcaAtividadeTpFiltro(filtro, i, ativ);
			}
		});
	}

	/**
	 * Realiza a marcação de filtro das atividades que já existem no calendário
	 * @param filtro filtro marcado
	 * @param indexAtiv indice da atividade a ser marcada
	 * @param atividade conteúdo a ser inserido na atividade
	 */
	marcaAtividadeTpFiltro(filtro: string, indexAtiv: number, atividade: Atividade) {
		switch (filtro) {
			case 'MINHAS TAREFAS':
				this.listAtividades[indexAtiv].minhasTarefas = true;
				break;
			case 'FAVORITOS':
				this.listAtividades[indexAtiv].favoritos = true;
				break;
			case 'MINHA EQUIPE':
				if (
					this.validMembroEquipe(
						atividade.membroEquipe,
						this.listAtividades[indexAtiv]
					) &&
					!this.listAtividades[indexAtiv].minhasTarefas
				) {
					this.listAtividades[indexAtiv].minhaEquipe = true;
					this.listAtividades[indexAtiv].membroEquipe = atividade.membroEquipe;
				}
				break;
			case 'PROCESSOS QUE CUIDO':
				this.listAtividades[indexAtiv].procCuido = true;
				break;
			default:
				if (
					this.validMembroEquipe(
						atividade.membroEquipe,
						this.listAtividades[indexAtiv]
					) &&
					!this.listAtividades[indexAtiv].minhasTarefas
				) {
					this.listAtividades[indexAtiv].membroEquipe = atividade.membroEquipe;
				}
				break;
		}
	}

	/**
	 * Valida se o responsável da tarefa faz parte da equipe
	 * @param membro código do participante
	 * @param atividade atividade a ser incluída no calendário
	 */
	validMembroEquipe(membro: string, atividade?: Atividade) {
		let canAttribute: boolean = false;
		if (isNullOrUndefined(atividade)) {
			if (this.itemsTreeView[2].subItems.findIndex(x => x.value === membro) > -1) {
				canAttribute = true;
			}
		} else {
			if (
				atividade.membroEquipe === '' &&
				this.itemsTreeView[2].subItems.findIndex(x => x.value === membro) > -1
			) {
				canAttribute = true;
			}
		}
		return canAttribute;
	}

	/**
	 * Identifica o status da situação do Follow-up
	 * @param tipoStatus NQN_TIPO
	 * @param dtfup data do Follow-up
	 * @param reagendado NTA_REAGEN
	 */
	getStatusAtiv(tipoStatus: string, dtfup: string, reagendado: boolean) {
		let status: string;
		const hoje: number = new Date().getDate();
		const dataFup: number = new Date(
			this.jurGenerics.makeDate(dtfup, 'yyyy-mm-dd') + 'T00:00:01'
		).getDate();

		switch (tipoStatus) {
			case '1':
				if (dataFup < hoje) {
					status = 'PENDENTE EM ATRASO';
				} else if (dataFup >= hoje && !reagendado) {
					status = 'PENDENTE EM ANDAMENTO';
				} else if (dataFup >= hoje && reagendado) {
					status = 'REAGENDADO';
				}
				break;
			case '2':
				status = 'EFETUADO';
				break;
			case '3':
				status = 'CANCELADO';
				break;
			case '4':
				status = 'EM APROVACAO';
				break;
			default:
				status = 'EM ANDAMENTO';
				break;
		}

		return status;
	}

	/**
	 * GET Jurconsultas equipe
	 * Obtém os membros da equipe do usuário logado
	 */
	async getMembrosEquipe() {
		this.jurConsultas.restore();
		const data = await this.jurConsultas
			.get('teamMembers', 'Obtém os membros da equipe')
			.toPromise();

		if (this.jurGenerics.changeUndefinedToEmpty(data) !== '') {
			data.membros.forEach(member => {
				const membro = {} as PoTreeViewItem;
				membro.label = member.nome;
				membro.value = member.codigo;
				membro.selected = false;
				this.addTreeViewSubitem(membro, this.itemsTreeView[2]);
			});
		}
	}

	/**
	 * Adiciona subitens à TreeView
	 * @param treeViewItem subitem a ser adicionado
	 * @param parent item pai que receberá os subitens
	 */
	addTreeViewSubitem(treeViewItem: PoTreeViewItem, parent: PoTreeViewItem) {
		treeViewItem.selected = false;

		const treeViewItemClone = { ...treeViewItem };
		const treeViewItemNode = parent;

		treeViewItemNode.subItems = [...treeViewItemNode.subItems, treeViewItemClone];

		this.itemsTreeView = [].concat(this.itemsTreeView);
	}

	/**
	 * Retorna as datas inicial e final do calendário formatadas
	 */
	intervaloDataCalend(): IntervaloDataCalend {
		const dataFin: string =
			this.dateCalendar.getFullYear().toString() +
			(this.dateCalendar.getMonth() + 1).toString().padStart(2, '0') +
			(
				32 -
				new Date(
					this.dateCalendar.getFullYear(),
					this.dateCalendar.getMonth(),
					32
				).getDate()
			).toString();

		const dataIni: string =
			this.dateCalendar.getFullYear().toString() +
			(this.dateCalendar.getMonth() + 1).toString().padStart(2, '0') +
			'01';

		return { dataIni, dataFin };
	}
}
