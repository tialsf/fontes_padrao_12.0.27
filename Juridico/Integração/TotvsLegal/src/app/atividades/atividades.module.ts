import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { PoModule } from '@po-ui/ng-components';
import { UploadInterceptor } from '../upload.interceptor';
import { AtividadesComponent } from './atividades.component';
import { NgxEventCalendarModule } from 'projects/ngx-event-calendar/src/public-api';
import { SharedModule } from '../shared/shared.module';
import { AtividadesRoutingModule } from './atividades-routing.module';

@NgModule({
	declarations: [AtividadesComponent],
	imports: [
		CommonModule,
		PoModule,
		FormsModule,
		ReactiveFormsModule,
		NgxEventCalendarModule,
		SharedModule,
		AtividadesRoutingModule,
	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: UploadInterceptor,
			multi: true,
		},
	],
})
export class AtividadesModule {}
