export interface IntervaloDataCalend {
	dataIni: string;
	dataFin: string;
}

export interface EventsDay {
	day: string;
	month: string;
	year: string;

	events: Array<Atividade>;
}

export interface Atividade {
	id: number;

	title: string;
	desc?: string;
	createdBy?: string;
	color?: string;
	filial: string;
	cajuri: string;
	codigo: string;
	status: string;
	descStatus: string;
	situacProc: string;
	descFup: string;
	horaFup: string;
	dataFup: string;
	corTagName: string;
	responsavel: string;
	tipoAsjJur: string;
	membroEquipe: string;
	filtroOrigPart: string;
	iconTag: string;
	styleModal: string;

	hasHour: boolean;
	minhasTarefas: boolean;
	favoritos: boolean;
	minhaEquipe: boolean;
	procCuido: boolean;
	
	startDate: Date;
	endDate: Date;
	createdAt?: Date;
}

export interface CoresStatusAtiv {
	tipoSituac: string;
	desc: string;
	corPortinari: string;
	corCalendar: string;
}
