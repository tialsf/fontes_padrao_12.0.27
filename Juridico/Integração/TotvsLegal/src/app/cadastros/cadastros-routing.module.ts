import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageCadastrosComponent } from './page-cadastros/page-cadastros.component';
import { DetTipoContratoComponent } from './tipo-contrato/det-tipocontrato/det-tipocontrato.component';
import { TipoContratoComponent } from './tipo-contrato/tipo-contrato.component';
import { UnidadesComponent } from './unidades/unidades.component';
import { DetUnidadeComponent } from './unidades/det-unidade/det-unidade.component';
import { CredenciadosComponent } from './credenciados/credenciados.component';
import { DetCredenciadoComponent } from './credenciados/det-credenciado/det-credenciado.component';

const routes: Routes = [
	{
		path: '',
		component: PageCadastrosComponent,

	},
	{
		path: 'tipocontrato',
		children: [
			{
				path:'',
				pathMatch:'full',
				component: TipoContratoComponent
			},
			{
				path: 'novo',
				component: DetTipoContratoComponent
			},
			{
				path: ':codTpContr',
				component: DetTipoContratoComponent
			}
		]
	},
	{
		path: 'unidades',
		children: [
			{
				path:'',
				pathMatch:'full',
				component: UnidadesComponent
			},
			{
				path: 'novo', 
				component: DetUnidadeComponent
			},
			{
				path: ':codUnidade/:loja',
				component: DetUnidadeComponent
			}
		]
	},
	{
		path: 'credenciados',
		children: [
			{
				path:'',
				pathMatch:'full',
				component: CredenciadosComponent
			},
			{
				path: 'novo',
				component: DetCredenciadoComponent
			},
			{
				path: ':codCredenciado/:loja',
				component: DetCredenciadoComponent
	}
		]
	},
	{
		path: 'fornecedores',
		children: [
			{
				path:'',
				pathMatch:'full',
				component: CredenciadosComponent
			},
			{
				path: 'novo',
				component: DetCredenciadoComponent
			},
			{
				path: ':codCredenciado/:loja',
				component: DetCredenciadoComponent
			}
		]
	}

];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class CadastrosRoutingModule { }
