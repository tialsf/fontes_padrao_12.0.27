import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PoModule } from '@po-ui/ng-components';
import { SharedModule } from '../shared/shared.module';
import { CadastrosRoutingModule } from './cadastros-routing.module';
import { PageCadastrosComponent, FilterByDescription } from './page-cadastros/page-cadastros.component';
import { TipoContratoComponent } from './tipo-contrato/tipo-contrato.component';
import { DetTipoContratoComponent } from './tipo-contrato/det-tipocontrato/det-tipocontrato.component';
import { UploadInterceptor } from '../upload.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { UnidadesComponent } from './unidades/unidades.component';
import { DetUnidadeComponent } from './unidades/det-unidade/det-unidade.component';
import { CredenciadosComponent } from './credenciados/credenciados.component';
import { DetCredenciadoComponent } from './credenciados/det-credenciado/det-credenciado.component';

@NgModule({
	declarations: [
		PageCadastrosComponent,
		TipoContratoComponent,
		DetTipoContratoComponent,
		FilterByDescription,
		UnidadesComponent,
		DetUnidadeComponent,
		CredenciadosComponent,
		DetCredenciadoComponent
	],
	imports: [
		CommonModule,
		CadastrosRoutingModule, 
		PoModule,
		FormsModule,
		ReactiveFormsModule,
		SharedModule
	],
	providers: [{
		provide: HTTP_INTERCEPTORS,
		useClass: UploadInterceptor,
		multi: true,
	}]

})
export class CadastrosModule { }
