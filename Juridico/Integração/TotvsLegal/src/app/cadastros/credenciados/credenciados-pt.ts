const breadCrumb = {
	home        : 'Home',
	cadastros   : 'Cadastros',
	titulo      : 'Credenciados',
	tituloForn  : 'Fornecedores',
}
const gridCredenciados ={
	codigo      : 'Código',
	razaoSocial : 'Razão social',
	nomeFantasia: 'Nome fantasia',
	cgc         : 'CPF/CNPJ',
	status      : 'Status',
	ativo       : 'Ativo',
	inativo     : 'Inativo',
	descricao   : 'Editar'
}

//-- Constantes principais
export const credenciadosPt = {
	btnNovoCredenciado : 'Novo credenciado',
	btnNovoFornecedor  : 'Novo fornecedor',
	pesquisar          : 'Pesquisar',
	loadCred           : 'Carregando...',
	titulo             : 'Credenciados',
	breadCrumb         : breadCrumb,
	grid               : gridCredenciados
}