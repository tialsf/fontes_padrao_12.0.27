import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PoTableColumn, PoBreadcrumb, PoI18nService } from '@po-ui/ng-components';
import { RoutingService } from 'src/app/services/routing.service';
import { FwmodelService } from 'src/app/services/fwmodel.service';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { adaptorFilterParam, FwmodelAdaptorService } from 'src/app/services/fwmodel-adaptor.service';

/**
 * Classe para o retorno da lista de credenciados
 */
class Credenciados{
	codigo      : string;
	loja        : string;
	razaoSocial : string;
	nomeFantasia: string;
	cgc         : string;
	status      : string;
	editaCredenciado: Array<string>;
}

@Component({
	selector: 'app-credenciados',
	templateUrl: './credenciados.component.html',
	styleUrls: ['./credenciados.component.css']
})

export class CredenciadosComponent implements OnInit {
	searchKey          : string               = '';
	lblTitulo          : string               = '';
	lblBtnNovo         : string               = '';
	linkCredFor        : string               = '';
	nPageSize          : number               = 20;
	nRegIndex          : number               = 1;
	nTotalTarefas      : number               = 0;
	nNextPage          : number               = 1;
	columns            : Array<PoTableColumn> ;
	listCredenciados   : Array<Credenciados>  = [] ;
	isShowMoreDisabled : boolean              = false;
	isHideLoadingGrig  : boolean              = false;
	isFornecedor       : boolean              = false;
	litCredenciados    : any;
	
	public breadcrumb: PoBreadcrumb = {	items: [] };

	/**
	 * Serviço de busca de escritório credenciados
	 */
	adaptorParamCredenciados: adaptorFilterParam;
	

	/**
	 * colunas da tabela 
	 */
	credenciadosColumns: Array<PoTableColumn> = [
		{ property: 'codigo'},
		{ property: 'razaoSocial'},
		{ property: 'nomeFantasia'},
		{ property: 'cgc'},
		{ property: 'status', type: 'label',
			labels: [
				{ value: '2', color: 'color-11', label: 'Inativo' },
				{ value: '1', color: 'color-07', label: 'Ativo'   } 
			]
		},
		{ property: 'editaCredenciado', label: ' ', type: 'icon', width: '5%',
			 icons: [
				{ value: 'editaCredenciado', icon: 'po-icon po-icon-edit', color: '#29b6c5', tooltip: 'Editar', 
					action:(value, row)=>{ this.callCredenciados(value) } 
				} 
			] 
		}
	];

	
	constructor(private router: Router,
		private route: ActivatedRoute,
		private routerState: RoutingService,
		private poI18nService: PoI18nService,
		private fwmodel: FwmodelService,
		private JuriGenerics: JurigenericsService,
		public fwModelAdaptorService: FwmodelAdaptorService,) 
		{
			if(this.router.url.toString().indexOf('fornecedores') > 0){
				this.isFornecedor = true;
				this.adaptorParamCredenciados = new adaptorFilterParam(
					"JURA132", "A2_MJURIDI <> '1'", "A2_NOME", "A2_NOME,A2_COD,A2_LOJA", "A2_NOME [A2_COD/A2_LOJA]", "A2_COD,A2_LOJA", "A2_CODA2_LOJA", "Get Credenciados");
			
			} else {
				this.adaptorParamCredenciados = new adaptorFilterParam(
					"JURA132", "A2_MJURIDI = '1'", "A2_NOME", "A2_NOME,A2_COD,A2_LOJA", "A2_NOME [A2_COD/A2_LOJA]", "A2_COD,A2_LOJA", "A2_CODA2_LOJA", "Get Credenciados");
			}
			this.routerState.loadRouting();

		//-- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'prov' no arquivo JurTraducao.
		poI18nService
			.getLiterals( { context: 'credenciados', language: poI18nService.getLanguage() } )
			.subscribe(
				(litCredenciados) => {
					this.litCredenciados = litCredenciados;
		
					//-- seta as constantes para tradução do titulo das colunas do Grid de Provisões
					this.credenciadosColumns[0].label = this.litCredenciados.grid.codigo;
					this.credenciadosColumns[1].label = this.litCredenciados.grid.razaoSocial;
					this.credenciadosColumns[2].label = this.litCredenciados.grid.nomeFantasia;
					this.credenciadosColumns[3].label = this.litCredenciados.grid.cgc;
					this.credenciadosColumns[4].label = this.litCredenciados.grid.status;
					this.credenciadosColumns[4].labels[0].label  = this.litCredenciados.grid.ativo;
					this.credenciadosColumns[4].labels[1].label  = this.litCredenciados.grid.inativo;
					this.credenciadosColumns[5].icons[0].tooltip = this.litCredenciados.grid.descricao;

					if(this.isFornecedor){
						this.lblTitulo = this.litCredenciados.breadCrumb.tituloForn;
						this.linkCredFor = '/Cadastros/fornecedores';
						this.lblBtnNovo = this.litCredenciados.btnNovoFornecedor
					} else {
						this.lblTitulo = this.litCredenciados.breadCrumb.titulo;
						this.linkCredFor = '/Cadastros/credenciados';
						this.lblBtnNovo = this.litCredenciados.btnNovoCredenciado
					}


					this.breadcrumb = {
						items: [
							{ label: this.litCredenciados.breadCrumb.home, link: '/' },
							{ label: this.litCredenciados.breadCrumb.cadastros, link: '/Cadastros' },
							{ label: this.lblTitulo , link: this.linkCredFor }
						]
					}

				}
			);	

		 }

	ngOnInit() {
		this.columns = this.credenciadosColumns;
		this.searchKey = "";
		this.getCredenciados();
	}

	/**
	 * Método responsavel pela criação do breadcrumb
	 */
	private setBreadCrumb() {
		this.breadcrumb = {
			items: [
				{ label: this.litCredenciados.breadCrumb.home, link: '/' },
				{ label: this.litCredenciados.breadCrumb.cadastros, link: '/Cadastros' },
				{ label: this.litCredenciados.breadCrumb.titulo, link: '/Cadastros/credenciados' }
			]
		}
	}

	/**
	 * Lista de Credenciados/Clientes
	 */
	getCredenciados(){

		this.isHideLoadingGrig = false;
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA132");
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.setPageSize(this.nPageSize.toString());
		this.fwmodel.setPage(this.nRegIndex.toString());
		this.fwmodel.setFirstLevel(false);
		
		if(this.isFornecedor){
			this.fwmodel.setFilter("A2_MJURIDI <> '1' ")
		} else {
			this.fwmodel.setFilter("A2_MJURIDI = '1' ")
		}
		this.fwmodel.setHeaderParam('orderBy', ' ORDER BY A2_COD DESC '); // Mostra no topo da lista o último registro cadastrado

		//Se for pesquisa, filtra pelo codigo e loja da credenciado
		if (this.searchKey != '' && this.searchKey != undefined) {
			let nQtdCaracter = this.searchKey.length
			this.nRegIndex = 1
			this.fwmodel.setSearchKey("A2_COD = '" + this.searchKey.substr(0, nQtdCaracter-2 ) + "' " );
			this.fwmodel.setSearchKey("A2_LOJA = '" + this.searchKey.substr( nQtdCaracter-2 ) + "' " );
		}

		//Se for uma nova pesquisa, limpa o listCredenciados
		if (this.nRegIndex == 1){
			this.listCredenciados = [];
		}

		this.fwmodel.get("Lista de Credenciados").subscribe(data=>{
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != '') {
				data.resources.forEach(item => {
					let itemCredenciados = new Credenciados();

					itemCredenciados.codigo       = this.JuriGenerics.findValueByName(item.models[0].fields, "A2_COD");
					itemCredenciados.loja         = this.JuriGenerics.findValueByName(item.models[0].fields, "A2_LOJA");
					itemCredenciados.razaoSocial  = this.JuriGenerics.findValueByName(item.models[0].fields, "A2_NOME");
					itemCredenciados.nomeFantasia = this.JuriGenerics.findValueByName(item.models[0].fields, "A2_NREDUZ")
					itemCredenciados.cgc          = this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(item.models[0].fields, "A2_CGC"));
					itemCredenciados.status       = this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(item.models[0].fields, "A2_MSBLQL"));
					itemCredenciados.editaCredenciado = ['editaCredenciado'];

					this.listCredenciados.push(itemCredenciados);
				})
			};
			
			this.setShowMore(data.total);
			this.isHideLoadingGrig = true;
		})
	}

	/**
	 * Responsável por chamar a tela de edição de credenciado 
	 * @param codCredenciado - Se tiver preenchido sabemos que é alteração
	 */
	callCredenciados(codCredenciado: any){
		if(codCredenciado == undefined || codCredenciado == "" )
		{
			this.router.navigate(["./novo"], {relativeTo: this.route})
		}else{
			this.router.navigate(["./"+ btoa(codCredenciado.codigo) + "/"+ btoa(codCredenciado.loja)], {relativeTo: this.route})
		}
	}

	/**
	* Ação do botão Pesquisa
	*/	  
	search() {

		this.nRegIndex = 1;
		this.getCredenciados();
	}

	/**
	 * Incrementa o index de paginação da tela e
	 * define se o botão de carregar mais continua habilitado ou não
	 * @param total Total de Credenciados
	 */
	setShowMore(total: number) {
		this.nTotalTarefas = total;
		if(this.nRegIndex == 1){
			this.nRegIndex = this.nPageSize + 1;
		}else{
			this.nRegIndex += this.nPageSize;
		}

		if (this.nRegIndex <= total) {
			this.isShowMoreDisabled = false;
		}else{
			this.isShowMoreDisabled = true;
		};
	}

	/**
	 * Ação do botão Carregar mais resultados
	 * Se for clicado pela 3a vez carrega o restante dos dados, independente da quantidade
	 */
	showMore() {
		this.nNextPage ++
		//se for clicado pela 3a vez carrega o restante dos dados
		if(this.nNextPage == 3){
			this.nPageSize = this.nTotalTarefas - this.listCredenciados.length;
		}
		this.isShowMoreDisabled = true;
		this.getCredenciados();
	}
}
