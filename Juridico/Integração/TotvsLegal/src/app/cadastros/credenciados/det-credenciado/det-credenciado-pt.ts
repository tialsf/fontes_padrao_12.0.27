/**
* Arquivo de constantes para tradução da página detalhe de Credenciado
*/

const breadCrumb = {
	credenciados    : 'Credenciados',
	fornecedores    : 'Fornecedores', 
	novaCredenciado : 'Novo credenciado', 
	tituloAlterar   : 'Alterar credenciado',
	home            : 'Home',
	cadastros       : 'Cadastros'
}
const modal ={
	titleExclusao   : 'Atenção!',
	exclusao        : 'O credenciado será excluído. Deseja prosseguir?',
	btnExcluir      : 'Excluir',
	btnFechar       : 'Fechar',
	btnSim          : 'Sim',
	pergIncCont     : 'Deseja incluir',
	pergIncCont2    : 'como um novo contato?',
	incContato      : 'Incluir Contato',
	nome            : 'Nome',
	tipoPessoa      : 'Tipo Pessoa',
	confirmar       : "Confirmar",
	cancelar        : "Cancelar",
	validacao       : "O campo nome do contato deve ser preenchido. Verifique!"
}
const opcaoFixa = {
	pessoaFisica    : 'Física ',
	pessoaJuridi    : 'Jurídica ',
	ativo           : 'Ativo',
	inativo         : 'Inativo'
}

const form = {
	codigo          : 'Código',
	loja            : 'Loja',
	razaoSocial     : 'Razão social',
	nomeFantasia    : 'Nome fantasia',
	tpPessoa        : 'Tipo pessoa',
	cgc             : 'CNPJ/CPF',
	tipo            : 'Tipo',
	email           : 'E-mail',
	estado          : 'Estado',
	cidade          : 'Cidade',
	telefone        : 'Telefone',
	endereco        : 'Endereço',
	cep             : 'CEP',
	contato         : 'Contato do escritório',
	contatoFonec    : "Contato do fornecedor",
	status          : 'Status',
}

const botao = {
	btnCancelar     : 'Cancelar',
	btnExcluir      : 'Excluir',
	btnSalvar       : 'Salvar',
}
const mensagem = {
	msgInclusao     : 'Credenciado incluído com sucesso!',
	msgAlteracao    : 'Credenciado atualizado com sucesso!',
	msgExclusao     : 'Credenciado excluído com sucesso!',
	msgValid        : 'Há campos obrigatórios não preenchidos, verifique!',
	naoEncontrada   : 'O credenciado não foi encontrado!'
}

//-- Constantes principais
export const detCredenciadoPt = {
	tituloNovo       : 'Novo credenciado',
	tituloNovoFornec : 'Novo fornecedor',
	tituloAlterar    : 'Alterar credenciado',
	tituloAlterarFor : 'Alterar fornecedor',
	breadCrumb       : breadCrumb,
	msg              : mensagem,
	btn              : botao,
	list             : opcaoFixa,
	form             : form,
	modal            : modal
}