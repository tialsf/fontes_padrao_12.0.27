import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetCredenciadoComponent } from './det-credenciado.component';

describe(' DetCredenciadoComponent', () => {
  let component:  DetCredenciadoComponent;
  let fixture: ComponentFixture< DetCredenciadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [  DetCredenciadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent( DetCredenciadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
