import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PoBreadcrumb, PoI18nService, PoRadioGroupOption, PoComboComponent, PoNotificationService, PoModalComponent, PoModalAction } from '@po-ui/ng-components';
import { RoutingService } from 'src/app/services/routing.service';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { FwmodelService, FwModelBody, FwModelBodyModels, FwModelBodyItems } from 'src/app/services/fwmodel.service';
import { adaptorFilterParam, FwmodelAdaptorService } from 'src/app/services/fwmodel-adaptor.service';
import { LegalprocessService } from 'src/app/services/legalprocess.service';
import { CredenciadoDados, ContatoDados } from './det-credenciado';
import { CidadeAdapterService } from 'src/app/services/combos-fwmodel-adapter.service';

/**
 * Estrutura padrão para alimentação de combos.
 *
 * @class ComboValue
 */
class ComboValue {
	label: string = '';
	value: string = '';
}

@Component({
	selector: 'app-det-credenciado',
	templateUrl: './det-credenciado.component.html',
	styleUrls: ['./det-credenciado.component.css']
})

export class DetCredenciadoComponent implements OnInit {
	litDetCredenciado         : any;
	formCredenciado           : FormGroup;
	formContato               : FormGroup;
	estado                    : string  = "";
	cidade                    : string  = "";
	codCredenciado            : string  = "";
	lojaCredenciado           : string  = "";
	pkCredenciado             : string  = "";
	titleDet                  : string  = "";
	lblBtnSalvar              : string  = "";
	lblBtnCancelar            : string  = "";
	lblBtnExcluir             : string  = "";
	lblNomeContato            : string  = "";
	lblCredFor                : string  = "";
	linkCredFor               : string  = "";
	nomeContato               : string  = "";
	juridico                  : string  = "1";
	maskCpfCnpj               : string  = "99.999.999/9999-99";  
	isOperacaoInclusao        : boolean = false;
	isDisableMunicipio        : boolean = true;
	isHideLoadingCredenciado  : boolean = false;
	isLoadingBtnsave          : boolean = false;
	isFornecedor              : boolean = false;

	readonly listTpPessoa: Array<PoRadioGroupOption> = [
		{ label: 'Física'   , value: 'F' },
		{ label: 'Juridica' , value: 'J' }
	]

	readonly listStatus: Array<PoRadioGroupOption> = [
		{ label: 'Ativo',    value: '2' },
		{ label: 'Inativo',  value: '1' }
	]

	readonly listEstados: Array<ComboValue> = [
		{ value: 'AC', label: 'ACRE'                },
		{ value: 'AL', label: 'ALAGOAS'             },
		{ value: 'AM', label: 'AMAZONAS'            },
		{ value: 'AP', label: 'AMAPA'               },
		{ value: 'BA', label: 'BAHIA'               },
		{ value: 'CE', label: 'CEARA'               },
		{ value: 'DF', label: 'DISTRITO FEDERAL'    },
		{ value: 'ES', label: 'ESPIRITO SANTO'      },
		{ value: 'EX', label: 'ESTRANGEIRO'         },
		{ value: 'GO', label: 'GOIAS'               },
		{ value: 'MA', label: 'MARANHAO'            },
		{ value: 'MG', label: 'MINAS GERAIS'        },
		{ value: 'MS', label: 'MATO GROSSO DO SUL'  },
		{ value: 'MT', label: 'MATO GROSSO'         },
		{ value: 'PA', label: 'PARA'                },
		{ value: 'PB', label: 'PARAIBA'             },
		{ value: 'PE', label: 'PERNAMBUCO'          },
		{ value: 'PI', label: 'PIAUI'               },
		{ value: 'PR', label: 'PARANA'              },
		{ value: 'RJ', label: 'RIO DE JANEIRO'      },
		{ value: 'RN', label: 'RIO GRANDE DO NORTE' },
		{ value: 'RO', label: 'RONDONIA'            },
		{ value: 'RR', label: 'RORAIMA'             },
		{ value: 'RS', label: 'RIO GRANDE DO SUL'   },
		{ value: 'SC', label: 'SANTA CATARINA'      },
		{ value: 'SE', label: 'SERGIPE'             },
		{ value: 'SP', label: 'SAO PAULO'           },
		{ value: 'TO', label: 'TOCANTINS'           }
		
	];

	public breadcrumb: PoBreadcrumb = {
		items: []
	};

	// Campos dinâmicos
	@ViewChild('contatoEscritorio') cmbContatoEscritorio: PoComboComponent;
	@ViewChild('municipio') cmbMunicipio        : PoComboComponent;
	@ViewChild('tipoPessoa'        , { static: true  }) radioTipoPessoa     : PoRadioGroupOption;
	@ViewChild('status'            , { static: true  }) radioStatus         : PoRadioGroupOption;

	//Modal de exclusão de credenciado
	@ViewChild('excluiCredenciado' , { static: true  }) excluiModal         : PoModalComponent;
		fecharExc: PoModalAction = {
			action: () => {
				this.excluiModal.close();
			},
			label: 'Fechar',
		};
		
		excluirExc: PoModalAction = {
			action: () => {
				this.excluirCredenciado();
			},
			label: 'Excluir',
		};

	/**
	 * Modal valida se sinclui contato
	 *
	 * @type {PoModalComponent}
	 * @memberof DetCredenciadoComponent
	 */
	@ViewChild('confirmIncCont'   ,{ static: true  }) modalConfirmIncl: PoModalComponent;
		fecharContato: PoModalAction = {
			action: () => { this.modalConfirmIncl.close();}, 
			label: '' };
		
		incluirContato: PoModalAction = { 
			action: () => { 
				this.modalConfirmIncl.close(); 
				this.modalIncCont.open(); }, 
			label: '', 
		};

	/**
	 *Modal de inclusão de novo contato
	 *
	 * @type {PoModalComponent}
	 * @memberof DetCredenciadoComponent
	 */
	@ViewChild('modalIncCont'     ,{ static: true  }) modalIncCont:   PoModalComponent;
		cancelar: PoModalAction = {
			action: () => {	this.modalIncCont.close();},
			label: ''};
		
		confirmar: PoModalAction = {
			action: () => { this.postContato(); },
			label: ''
		};

	/**
	 * Construtor da classe
	 * Seta os literais
	 */
	constructor(private poI18nService: PoI18nService,
		private router: Router,
		private routingState: RoutingService,
		private route: ActivatedRoute,
		private formBuilder: FormBuilder,
		private JuriGenerics: JurigenericsService,
		private fwmodel: FwmodelService,
		public  poNotification: PoNotificationService,
		public  fwModelAdaptorService: FwmodelAdaptorService,
		private legalprocess: LegalprocessService,
		public cidadeAdapterService: CidadeAdapterService
	) {

		//setup dos adapters
		this.fwModelAdaptorService.setup(new adaptorFilterParam("JURA232","","U5_CONTAT","U5_CONTAT","","U5_CODCONT","","getContato"))
		this.cidadeAdapterService.setup(new adaptorFilterParam("JMUNICIPIO", "", "CC2_MUN", "CC2_MUN", "CC2_MUN", "CC2_CODMUN", "", "Get Municipio"))

		if (this.route.snapshot.paramMap.get("codCredenciado") != undefined) {
			this.codCredenciado = atob(decodeURIComponent(this.route.snapshot.paramMap.get("codCredenciado"))); 
		}

		if (this.route.snapshot.paramMap.get("loja") != undefined) {
			this.lojaCredenciado = atob(decodeURIComponent(this.route.snapshot.paramMap.get("loja"))); 
		}

		if(this.router.url.toString().indexOf('fornecedores') > 0){
			this.isFornecedor = true;
			this.juridico = '2';
		}

		// Traduções
		poI18nService.getLiterals({ 
			context: 'detcredenciado', 
			language: poI18nService.getLanguage() 
		}).subscribe((litDetCredenciado) => {
			this.litDetCredenciado = litDetCredenciado;
			this.lblBtnExcluir = this.litDetCredenciado.btn.btnExcluir;
			this.fecharExc.label = this.litDetCredenciado.modal.btnFechar;
			this.excluirExc.label = this.litDetCredenciado.modal.btnExcluir;
			this.fecharContato.label = this.litDetCredenciado.modal.btnFechar;
			this.incluirContato.label = this.litDetCredenciado.modal.btnSim;
			this.confirmar.label = this.litDetCredenciado.modal.confirmar
			this.cancelar.label = this.litDetCredenciado.modal.cancelar
			this.listTpPessoa[0].label = this.litDetCredenciado.list.pessoaFisica;
			this.listTpPessoa[1].label = this.litDetCredenciado.list.pessoaJuridi;
			this.listStatus[0].label = this.litDetCredenciado.list.ativo;
			this.listStatus[1].label = this.litDetCredenciado.list.inativo;

			if(this.isFornecedor){
				this.lblNomeContato = this.litDetCredenciado.form.contatoFonec;
				this.lblCredFor = this.litDetCredenciado.breadCrumb.fornecedores;
				this.linkCredFor = '/Cadastros/fornecedores'
			} else {
				this.lblNomeContato = this.litDetCredenciado.form.contato;
				this.lblCredFor = this.litDetCredenciado.breadCrumb.credenciados;
				this.linkCredFor = '/Cadastros/credenciados'
			}

			this.breadcrumb.items = [
				{ label: this.litDetCredenciado.breadCrumb.home          , link: '/' },
				{ label: this.litDetCredenciado.breadCrumb.cadastros     , link: '/Cadastros' },
				{ label: this.lblCredFor                                 , link: this.linkCredFor },
				{ label: this.titleDet }
			];
			

			
			//Novo
			if (this.codCredenciado == '' ) {
				this.isOperacaoInclusao = true;
				this.lblBtnSalvar = this.litDetCredenciado.btn.btnSalvar;

				if(this.isFornecedor){
					this.titleDet     = this.litDetCredenciado.tituloNovoFornec;
				} else {
					this.titleDet     = this.litDetCredenciado.tituloNovo;
				}



			//Atualização	
			} else {
				this.lblBtnSalvar = this.litDetCredenciado.btn.btnSalvar;

				if(this.isFornecedor){
					this.titleDet     = this.litDetCredenciado.tituloAlterarFor;
				} else {
					this.titleDet     = this.litDetCredenciado.tituloAlterar
				}

			}
		});
	}

	/**
	 * Inicializador da classe
	 * Carrega os formulários
	 *
	 * @memberof DetCredenciadoComponent
	 */
	ngOnInit() {
		this.createFormContato(new ContatoDados());
		this.createFormCredenciado(new CredenciadoDados());
		this.getCredenciado();
	}

	/**
	 * Cria o form de Contato
	 *
	 * @param {ContatoDados} contato EEstrutura de dados do form
	 * @memberof DetCredenciadoComponent
	 */
	createFormContato(contato: ContatoDados){
		this.formContato = this.formBuilder.group({
			nomeContato:  [contato.nome, Validators.compose([Validators.required])],
			cpfContato:   [contato.cpf],
			emailContato: [contato.email]
		})
	}

	/**
	 * Cria o formulário de credenciados
	 *
	 * @param {CredenciadoDados} credenciado Estrutura de dados do form
	 * @memberof DetCredenciadoComponent
	 */
	createFormCredenciado(credenciado: CredenciadoDados) {
		this.formCredenciado = this.formBuilder.group({
			codigo:       [credenciado.codigo],
			loja:         [credenciado.loja],
			razaoSocial:  [credenciado.razaoSocial,  Validators.compose([Validators.required]) ],
			tpPessoa:     [credenciado.tpPessoa,     Validators.compose([Validators.required]) ],
			cgc:          [credenciado.cgc],
			nomeFantasia: [credenciado.nomeFantasia, Validators.compose([Validators.required]) ],
			status:       [credenciado.status,       Validators.compose([Validators.required]) ],
			email:        [credenciado.email ],
			telefone:     [credenciado.telefone ],
			endereco:     [credenciado.endereco,     Validators.compose([Validators.required]) ],
			estado:       [credenciado.estado,       Validators.compose([Validators.required]) ],
			cidade:       [credenciado.cidade,       Validators.compose([Validators.required]) ],
			cep:          [credenciado.cep],
			contato:      [credenciado.contato,      Validators.compose([Validators.required]) ]
		
		});
	}

	/**
	 * Busca os dados do credenciado 
	 *
	 * @memberof DetCredenciadoComponent
	 */
	getCredenciado(){
		this.isHideLoadingCredenciado = false;
		if (this.JuriGenerics.changeUndefinedToEmpty(this.codCredenciado) == '') {

			this.formCredenciado.patchValue({
				tpPessoa: "J",
				status:   "2"
			})

			this.isHideLoadingCredenciado = true;
			
		}else{
			this.fwmodel.restore();
			this.fwmodel.setModelo("JURA132");
			this.fwmodel.setFilter("A2_COD ='" + this.codCredenciado + "'");
			this.fwmodel.setFilter("A2_LOJA ='" + this.lojaCredenciado + "'");
			this.fwmodel.setCampoVirtual(true);
			this.fwmodel.setFirstLevel(false);
			this.fwmodel.get("Busca credenciado").subscribe(data => {

				if (data.count === 0) {
					this.poNotification.error(this.litDetCredenciado.msg.naoEncontrada);
				} else {
					this.pkCredenciado = data.resources[0].pk;
					this.setmaskCpfCnpj(this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "A2_TIPO"   ))
					this.formCredenciado.patchValue({
						tpPessoa:     this.JuriGenerics.findValueByName(data.resources[0].models[0].fields                   , "A2_TIPO"   )
					});

					// Aguarda carregar a máscara do campo
					setTimeout(() => {
						this.formCredenciado.patchValue({
							cgc: this.JuriGenerics.findValueByName(data.resources[0].models[0].fields                        , "A2_CGC"    )
						})
					}, 200);
	
					this.formCredenciado.patchValue({
						codigo:       this.JuriGenerics.findValueByName(data.resources[0].models[0].fields                   , "A2_COD"    ),
						loja:         this.JuriGenerics.findValueByName(data.resources[0].models[0].fields                   , "A2_LOJA"   ),
						razaoSocial:  this.JuriGenerics.findValueByName(data.resources[0].models[0].fields                   , "A2_NOME"   ),
						nomeFantasia: this.JuriGenerics.findValueByName(data.resources[0].models[0].fields                   , "A2_NREDUZ" ),
						status:       this.JuriGenerics.findValueByName(data.resources[0].models[0].fields                   , "A2_MSBLQL" ),
						email:        this.JuriGenerics.findValueByName(data.resources[0].models[0].fields                   , "A2_EMAIL"  ),
						telefone:     this.JuriGenerics.findValueByName(data.resources[0].models[0].fields                   , "A2_TEL"    ),
						endereco:     this.JuriGenerics.findValueByName(data.resources[0].models[0].fields                   , "A2_END"    ),
						estado:       this.JuriGenerics.findValueByName(data.resources[0].models[0].fields                   , "A2_EST"    ),
						cidade:       this.JuriGenerics.findValueByName(data.resources[0].models[0].fields                   , "A2_COD_MUN"),
						cep:          this.JuriGenerics.findValueByName(data.resources[0].models[0].fields                   , "A2_CEP"    ),
						contato:      this.JuriGenerics.findValueByName(data.resources[0].models[0].models[0].items[0].fields, "AC8_CODCON")
					})
				}
				this.isHideLoadingCredenciado = true;
			});
		}
	}

	/**
	 *Seta o filtro de cidades de acordo com o estado selecionado
	 *
	 * @param {string} estado  estado selecionado
	 * @memberof DetCredenciadoComponent
	 */
	setFilterCidade(estado: string){
		this.cidade = '';
 
		if (this.JuriGenerics.changeUndefinedToEmpty(estado) != '') {
			estado    = "CC2_EST = '" + estado + "'"
			this.cidadeAdapterService.setFiltroP(estado);
			this.isDisableMunicipio    = false;
		} else {
			this.isDisableMunicipio = true;
		}
	}

	/**
	 * Define qual máscara será usada no campo CNPJ/CPF
	 *
	 * @param {string} tipo Tipo de pesoa ("F"ísica ou "J"urídica)
	 * @memberof DetCredenciadoComponent
	 */
	setmaskCpfCnpj(tipo: string){
		this.formCredenciado.patchValue({ cgc: ""});

		if(tipo == "F"){
			this.maskCpfCnpj = "999.999.999-99"
		} else{
			this.maskCpfCnpj = "99.999.999/9999-99"
		}
	}

	/**
	 * Cancela operação de inclusão ou alteração
	 *
	 * @memberof DetCredenciadoComponent
	 */
	onCancel() {
		this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codCredenciado))]);
	}

	/**
	 * Valida o tipo de operação para persistir os dados do formulário 
	 *
	 * @memberof DetCredenciadoComponent
	 */
	submitCredenciado() {

		if(this.formCredenciado.valid){
			this.isLoadingBtnsave = true;
			this.isHideLoadingCredenciado = false;

			if(this.isOperacaoInclusao){
				this.insertCredenciado()
			}else{
				this.updateCredenciado()
			}
		} else{
			this.poNotification.warning(this.litDetCredenciado.msg.msgValid);
		}
		
	}

	/**
	 * Faz o insert de um novo credenciado
	 *
	 * @memberof DetCredenciadoComponent
	 */
	insertCredenciado(){

		//Busca o código sequêncial
		this.isLoadingBtnsave = true;
		this.legalprocess.restore();
		this.legalprocess.setSearchKey('SA2')
		this.legalprocess.get("getSequencial","Busca o proximo sequencial disponivel").subscribe(data => {
			if (data != '') {
				//fazer a carga do campo codigo e a loja
				this.formCredenciado.patchValue({
					codigo: data.sequencial,
					loja:   '01'
				})

				// Faz a requisição de insert
				this.fwmodel.restore();
				this.fwmodel.setFirstLevel(false)
				this.fwmodel.setModelo('JURA132');
				this.fwmodel.post(this.bodyCredenciado(),"Inclusão de Credenciado").subscribe((data) => {
					if (data != '') {
						this.isLoadingBtnsave = false;
						this.router.navigate([this.routingState.getPreviousUrl(this.codCredenciado)])
						this.poNotification.success(this.litDetCredenciado.msg.msgInclusao);
					} else {
						this.isLoadingBtnsave = false;
						this.isHideLoadingCredenciado = true;
					}
				});
			}else{
				this.isLoadingBtnsave = false;
				this.isHideLoadingCredenciado = true;
			}
		})
	}

	/**
	 *Faz o update de um credenciado
	 *
	 * @memberof DetCredenciadoComponent
	 */
	updateCredenciado(){
		this.isLoadingBtnsave = true;
		this.fwmodel.restore();
		this.fwmodel.setFirstLevel(false)
		this.fwmodel.setModelo('JURA132');
		this.fwmodel.setFilter("A2_COD='" + this.codCredenciado + "'");
		this.fwmodel.setFilter("A2_LOJA='" + this.lojaCredenciado + "'");
		this.fwmodel.put(this.pkCredenciado, this.bodyCredenciado(),"Atualiza Credenciado").subscribe((data) => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data) != '') {
				this.isLoadingBtnsave = false;
				this.router.navigate([this.routingState.getPreviousUrl(this.codCredenciado)])
				this.poNotification.success(this.litDetCredenciado.msg.msgAlteracao);

			} else {
				this.isHideLoadingCredenciado = true;
				this.isLoadingBtnsave = false;
			}
		});
	}

	/**
	 * Responsável por excluir a credenciado
	 */
	excluirCredenciado() {
		this.excluiModal.primaryAction.loading = true;
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA132');
		this.fwmodel.delete(this.pkCredenciado, "Deleta o credenciado Credenciado").subscribe((data) => {
			if (data != '') {
				this.isLoadingBtnsave = false;
				this.router.navigate([this.routingState.getPreviousUrl(this.codCredenciado)])
				this.poNotification.success(this.litDetCredenciado.msg.msgExclusao);
				this.excluiModal.primaryAction.loading = false;
			} else {
				this.isLoadingBtnsave = false;
				this.excluiModal.primaryAction.loading = false;
			}
		});
	}
	
	/**
	 * Monta o body do credenciado
	 *
	 * @returns Objeto JSON da requisição
	 * @memberof DetCredenciadoComponent
	 */
	bodyCredenciado(){
		let fwModelBody: FwModelBody = new FwModelBody('JURA132',3, this.pkCredenciado);
		let gridContato: FwModelBodyModels;
		let itemContato: FwModelBodyItems;
		let formCredenciado = this.formCredenciado.value;
	
		//-- MODEL SA2 "FORNECEDOR" 
		fwModelBody.addModel("SA2MASTER","FIELDS",1)
		fwModelBody.models[0].addField("A2_COD"     , formCredenciado.codigo       , 0, "C")
		fwModelBody.models[0].addField("A2_LOJA"    , formCredenciado.loja         , 0, "C")
		fwModelBody.models[0].addField("A2_MJURIDI" , this.juridico                , 0, "C")
		fwModelBody.models[0].addField("A2_NOME"    , formCredenciado.razaoSocial  , 0, "C")
		fwModelBody.models[0].addField("A2_TIPO"    , formCredenciado.tpPessoa     , 0, "C")
		fwModelBody.models[0].addField("A2_CGC"     , formCredenciado.cgc          , 0, "C")
		fwModelBody.models[0].addField("A2_NREDUZ"  , formCredenciado.nomeFantasia , 0, "C")
		fwModelBody.models[0].addField("A2_MSBLQL"  , formCredenciado.status       , 0, "C")
		fwModelBody.models[0].addField("A2_EMAIL"   , formCredenciado.email        , 0, "C")
		fwModelBody.models[0].addField("A2_TEL"     , formCredenciado.telefone     , 0, "C")
		fwModelBody.models[0].addField("A2_END"     , formCredenciado.endereco     , 0, "C")
		fwModelBody.models[0].addField("A2_EST"     , formCredenciado.estado       , 0, "C")
		fwModelBody.models[0].addField("A2_COD_MUN" , formCredenciado.cidade       , 0, "C")
		fwModelBody.models[0].addField("A2_CEP"     , formCredenciado.cep          , 0, "C")

		//-- MODEL AC8 "CONTATO"
		gridContato = fwModelBody.models[0].addModel("AC8DETAIL", "GRID", 0)
		itemContato = gridContato.addItems(1 , 0)
		itemContato.addField("AC8_CODCON", formCredenciado.contato      ,"C") 
		return fwModelBody.serialize();
	}

	/**
	 * Verifica se o nome digitado está presente no retorno do combo, 
	 * se não estiver,apresenta um modal para inclusão.
	 * @param event - obtem dados do evento html do componente.
	 */
	vldContato(event){

		if (event.keyCode == 9 || event.keyCode == 13) {
			//verifica se o nome não esta presente no combo
			if (this.cmbContatoEscritorio.selectedOption == undefined ||
				(this.cmbContatoEscritorio.selectedOption.label != this.cmbContatoEscritorio.previousSearchValue ) ){

				this.nomeContato = this.cmbContatoEscritorio.previousSearchValue//informação digitada no nome
				this.modalConfirmIncl.open();
				this.formContato.patchValue({
					nomeContato: this.nomeContato
				})
			}
		}
	}

	/**
	 * Efetua o post para inclusão de um novo contato
	 *
	 * @memberof DetCredenciadoComponent
	 */
	postContato(){

		if(this.formContato.valid){
			
			this.fwmodel.restore();
			this.fwmodel.setModelo('JURA232');
			this.fwmodel.post(this.bodyContato(),'postContato').subscribe((data) =>{
				if (this.JuriGenerics.changeUndefinedToEmpty(data) != ""){
			
					this.modalIncCont.close();
					this.formContato.reset();
					this.formCredenciado.patchValue({
						contato: this.JuriGenerics.findValueByName(data.models[0].fields, "U5_CODCONT")
					})
				}
			})
		
		} else {
			this.poNotification.warning(this.litDetCredenciado.modal.validacao);
		}
	}


	bodyContato(){
		let value = this.formContato.value
		let fwModelBody: FwModelBody = new FwModelBody("JURA232",3,undefined,"SU5MASTER")

		fwModelBody.models[0].addField("U5_CONTAT" ,value.nomeContato)
		fwModelBody.models[0].addField("U5_CPF"    ,value.cpf  )
		fwModelBody.models[0].addField("U5_EMAIL"  ,value.emailContato          )

		return 	fwModelBody.serialize()
	}


	


}
