const page = {
	title:"Gerenciar cadastros básicos",
	searchPlaceHolder: "Buscar rotina pelo nome ou descrição..."
}

const btn = {
	novoRegistro: "Novo Registro",
	mostrarTodos: "Mostrar Todos"
}

const breadcrumb = {
	home: "Home",
	cadastros: "Cadastros"
}

const actions = {
	novoRegistro: "Novo registro",
	mostrarTodos: "Mostrar todos"
}

const itens = {
	nomeTipoContratos: 'Tipo de contratos',
	descTipoContratos: 'Rotina responsável pelo cadastro e manutenção dos tipos de contratos e suas minutas',
	nomeUnidades     : 'Unidades',
	descUnidades     : 'Rotina responsável pelo cadastro e manutenção das unidades',
	titleCredenciados: 'Escritórios credenciados',
	descCredenciados : 'Rotina responsável pelo cadastro e manutenção dos escritórios credenciados',
	titleFornecedores: 'Fornecedores',
	desFornecedores  : 'Rotina responsável pelo cadastro e manutenção dos fornecedores'

}

export const PageCadastrosPt = {
	page,
	btn,
	breadcrumb,
	actions,
	itens
}
