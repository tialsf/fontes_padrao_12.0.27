import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageCadastrosComponent } from './page-cadastros.component';

describe('PageCadastrosComponent', () => {
  let component: PageCadastrosComponent;
  let fixture: ComponentFixture<PageCadastrosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageCadastrosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageCadastrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
