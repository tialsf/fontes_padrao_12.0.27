import { Component, Pipe, PipeTransform, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { PoListViewAction, PoBreadcrumb, PoI18nService } from '@po-ui/ng-components';

@Component({
	selector: 'app-page-cadastros',
	templateUrl: './page-cadastros.component.html',
	styleUrls: ['./page-cadastros.component.css']
})
export class PageCadastrosComponent {
	litPageCadastros;
	breadcrumb: PoBreadcrumb;
	actions: Array<PoListViewAction>;
	itemList: Array<itemList>;

	@Input() filter: string = ''

	/**
	 * Método contrutor do componente
	 * @param router NgModule que disponibiliza a nevegação da url
	 * @param ActivatedRoute Rota ativa do componente
	 * @param poI18nService Serviço de literais do I18N
	 */
	constructor(
		private router: Router,
		private ActivatedRoute: ActivatedRoute,
		private poI18nService: PoI18nService
	) {
		this.poI18nService.getLiterals({	
				context: 'pageCadastros', 
				language: poI18nService.getLanguage() 
			}).subscribe(
				(litPageCadastros) => {
					this.litPageCadastros = litPageCadastros
					this.setBreadCrumb()
					this.setActions()
					this.setItemsList()
				}
			)
	}

	/**
	 * Método responsavel pela criação do BreadCrumb conforme as literais cadastradas
	 */
	private setBreadCrumb() {
		this.breadcrumb = {
			items: [
				{ label: this.litPageCadastros.breadcrumb.home, link: '/' },
				{ label: this.litPageCadastros.breadcrumb.cadastros, link: '/Cadastros' }
			]
		}

	}
	/**
	 * Método responsavel pela montagem das ações utilizadas no po-list-view
	 */
	private setActions() {
		this.actions = [
			{
				label: this.litPageCadastros.actions.novoRegistro,
				type: 'link', icon: 'po-icon-plus',
				action: (item: itemList) => this.navigate(item.new)
			},
			{
				label: this.litPageCadastros.actions.mostrarTodos,
				type: 'link', icon: 'po-icon po-icon-list',
				action: (item: itemList) => this.navigate(item.showAll)
			}
		]
	}
	/**
	 * método resposavel pela definição da listagem das rotinas do po-list-view
	 */
	private setItemsList() {
		this.itemList = [
			{
				name: this.litPageCadastros.itens.nomeTipoContratos,
				descricao: this.litPageCadastros.itens.descTipoContratos,
				new: './tipocontrato/novo',
				showAll: './tipocontrato'
			},
			{
				name: this.litPageCadastros.itens.nomeUnidades,
				descricao: this.litPageCadastros.itens.descUnidades,
				new: './unidades/novo',
				showAll: './unidades'
			},
			{
				name: this.litPageCadastros.itens.titleCredenciados,
				descricao: this.litPageCadastros.itens.descCredenciados,
				new: './credenciados/novo',
				showAll: './credenciados'
			},
			{
				name: this.litPageCadastros.itens.titleFornecedores,
				descricao: this.litPageCadastros.itens.desFornecedores,
				new: './fornecedores/novo',
				showAll: './fornecedores'
			}
		]

	}
	/**
	 * Método resposavel pela navegação da url conforme rotina selecionada no po-list-view
	 * @param path string contendo o dados para navegação das rotinas
	 */
	private navigate(path: string) {
		this.router.navigate([path], { relativeTo: this.ActivatedRoute })
	}


}
/**
 * Estrutura da lista de itens
 * @interface itemList
 */
interface itemList {
	name: string,
	descricao: string,
	new: string,
	showAll: string
}

/**
 * Pipe criado para realizar o filtro das rotinas conforme o usuário digita no input
 */
@Pipe({ name: 'filterByDescription' })
export class FilterByDescription implements PipeTransform {
	transform(itemList: Array<itemList>, description: string = '') {
		description = description
			.trim()
			.toLowerCase();

		if (description) {
			return itemList.filter(
				item => item.name.toLowerCase().includes(description)
					|| item.descricao.toLowerCase().includes(description)
			)
		} else {
			return itemList
		}
	}
}