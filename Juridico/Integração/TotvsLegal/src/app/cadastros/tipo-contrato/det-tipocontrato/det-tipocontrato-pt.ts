/**
* Arquivo de constantes para tradução da página detalhe do Tipo de contrato
*/

const modalExclusao = {
	msgExclusao: "Deseja excluir esse registro?",
	titleExclusao: "Atenção!"
}

const helpCampos = {
	motCancela: "Motivos de encerramento que será registrado no contrato, caso o fluxo do fluig seja cancelado",
	minutas: "Escolha uma minuta que será o padrão para este tipo de contrato para a geração automática no fluxo de contratos dentro do fluig"
}

const breadCrumb = {
	tipoContrato  : 'Tipo de Contrato',
	novoTipo      : 'Novo Tipo', 
	tituloAlterar : 'Alterar tipo',

}


//-- Constantes principais
export const detTipoContratoPt = {
	tiposContrato    : "Tipos de Contrato",
	tituloNovo       : 'Novo tipo de contrato',
	tituloAlterar    : 'Alterar tipo de contrato',
	tipoContrato     : 'Tipo de contrato',
	motCancela       : 'Motivo de cancelamento',
	minuta           : 'Modelo de minuta',
	btnBaixar        : 'Baixar minuta',
	novaMinuta       : 'Nova Minuta',
	descMinuta       : 'Descrição da minuta',
	btnCancelar      : 'Cancelar',
	btnExcluir       : 'Excluir',
	btnSalvar        : 'Salvar',
	msgInclusao      : 'Tipo incluído com sucesso!',
	msgAlteracao     : 'Tipo atualizado com sucesso!',
	msgExclusao      : 'Tipo excluído com sucesso!',
	msgSucess        : "Arquivo gerado com sucesso!",
	alteraMinuta     : "Atualizar Minuta",
	exclusao         : "O tipo de contrato será excluído. Deseja Prosseguir?",
	help: helpCampos,
	breadCrumb       : breadCrumb,
	modal            : modalExclusao
}