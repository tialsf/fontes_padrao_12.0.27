import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetTipoContratoComponent } from './det-tipocontrato.component';

describe('DetTipoContratoComponent', () => {
  let component: DetTipoContratoComponent;
  let fixture: ComponentFixture<DetTipoContratoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetTipoContratoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetTipoContratoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
