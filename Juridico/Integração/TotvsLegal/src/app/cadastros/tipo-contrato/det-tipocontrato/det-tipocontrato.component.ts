import { Component, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { PoUploadComponent, PoUploadFileRestrictions, PoNotificationService, PoBreadcrumb, PoI18nService
		,PoModalAction, PoModalComponent, } from '@po-ui/ng-components';
import { adaptorFilterParam, FwmodelAdaptorService } from 'src/app/services/fwmodel-adaptor.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FwmodelService } from 'src/app/services/fwmodel.service';
import { JurigenericsService, JurUTF8, JurBase64 } from 'src/app/services/jurigenerics.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TipoContratoDados } from './det-tipocontrato';
import { JurconsultasService } from 'src/app/services/jurconsultas.service';
import { RoutingService } from 'src/app/services/routing.service';


@Component({
	selector: 'app-tipocontrato',
	templateUrl: './det-tipocontrato.component.html',
	styleUrls: ['./det-tipocontrato.component.css']
})


export class DetTipoContratoComponent implements OnInit {
	litDetTpContrato     : any;
	nomeConfig           : string     = "";
	codConfig            : string     = "";
	url                  : string     = "/juri/JURCONSULTAS/uploadDot";
	toggleNovaMinuta     : boolean    = false;
	formTpContrato       : FormGroup;
	pkTpContrato         : string     = "";
	codTpContrato        : string     = "";
	loadingTpContrato    : boolean    = true;
	loadingBtnsave       : boolean    = false;
	opercao              : string     = "I";
	lblBtnSalvar         : string     = "";
	lblBtnCancelar       : string     = "";
	lblBtnExcluir        : string     = "";
	titleDet             : string     = "";
	breadcrumbHome       : string     = "";
	linkHome             : string     = "";
	bOperacaoInclusao    : boolean    = false;
	operacaoMinuta       : boolean    = false;
	lblOperacao          : string     = "";
	bBaixaMinuta         : boolean    = true;
	descMinuta           : string     = "";
	bHasFileSelected     : boolean    = false;
	bFileSent            : boolean    = false;
	descTipoContrato     : string     = "";


	public breadcrumb: PoBreadcrumb = {
		items: []
	};


	// Ações do modal de confirmação de exclusão
	fechar: PoModalAction = {
		action: () => {
			this.excluiModal.close();
		},
		label: 'Fechar',
	};
	
	excluir: PoModalAction = {
		action: () => {
			this.deleteVerify();
		},
		label: 'Excluir',
	};



	adaptorParamTpContrato: adaptorFilterParam = new adaptorFilterParam(
		"JURA134", "", "NY0_DESC", "NY0_DESC", "", "NY0_COD", "", "Combo Tipo Contrato"
		);

	adaptorParamMotCancela: adaptorFilterParam = new adaptorFilterParam(
		"JURA013", "", "NQI_DESC", "NQI_DESC", "", "NQI_COD", "", "Combo Tipo Contrato"
		);


	adaptorParamMinuta: adaptorFilterParam = new adaptorFilterParam(
		"JURA014", "", "NQY_DESC", "NQY_DESC", "", "NQY_COD", "", "Combo Configuração de Minuta"
		);


	//Modal para exclusão de tipo de contrato
	@ViewChild('excluiTpContrato', { static: true }) excluiModal: PoModalComponent;

	// Restrições de arquivos para upload
	fileRestrinctions:PoUploadFileRestrictions = {allowedExtensions:[ ".dot"]};
	@ViewChild('docUpload') docUpload: PoUploadComponent;
	

	constructor(
		public fwModelAdaptorService: FwmodelAdaptorService,
		private fwmodel: FwmodelService,
		private formBuilder: FormBuilder,
		public  poNotification: PoNotificationService,
		private JuriGenerics: JurigenericsService,
		private router: Router,
		private routingState: RoutingService,
		private route: ActivatedRoute,
		private poI18nService: PoI18nService,
		private jurConsultas: JurconsultasService,
	) { 

		if (this.route.snapshot.paramMap.get("codTpContr") != undefined) {
			this.codTpContrato = atob(decodeURIComponent(this.route.snapshot.paramMap.get("codTpContr"))); 
		}

		// Traduções
		poI18nService
			.getLiterals({ context: 'detTipocontrato', language: poI18nService.getLanguage() })
			.subscribe(
				(litDetTpContrato) => {

					this.litDetTpContrato = litDetTpContrato;
				
					this.lblBtnExcluir = this.litDetTpContrato.btnExcluir;

					this.lblOperacao = this.litDetTpContrato.novaMinuta;
					
					//Novo
					if (this.codTpContrato == '' ) {
						this.bOperacaoInclusao = true;
						this.titleDet = this.litDetTpContrato.tituloNovo;
						this.lblBtnSalvar = this.litDetTpContrato.btnSalvar;

					//Atualização	
					} else {
						this.titleDet = this.litDetTpContrato.tituloAlterar;
						this.lblBtnSalvar = this.litDetTpContrato.btnSalvar;
					}

					this.breadcrumb.items = [
						{ label: 'Home'  , link: '/' },
						{ label: 'Cadastros'  , link: '/Cadastros' },
						{ label: this.litDetTpContrato.breadCrumb.tipoContrato, link: '/Cadastros/tipocontrato' },
						{ label: this.titleDet }
					];

				}
			);
	}

	ngOnInit() { 
		this.createFormTpContrato(new TipoContratoDados());
		this.getTpContrato();
	}

	/**
	 * Responsável por obter os dados inputados
	 * no formulário de inclusão de Tipo de Contrato
	 */
	createFormTpContrato(contrato: TipoContratoDados) {
		this.formTpContrato = this.formBuilder.group({
			tipoContrato: [contrato.tipoContrato, Validators.compose([Validators.required])],
			motCancela:   [contrato.motCancela],
			minuta:       [contrato.minuta ],
			descMinuta:   [contrato.descMinuta ]
		});
	}



	/**
	 * Busca a descrição da minuta pelo código
	 * @parm codido da configuração de minuta
	 */
	getDescMinuta(cod: string){

		if((cod != undefined) && (cod != '')){

			this.fwmodel.restore();
			this.fwmodel.setModelo("JURA014");
			this.fwmodel.setFilter("NQY_COD='" + cod + "'");

			this.fwmodel.get("Busca de Tipos de Contrato").subscribe(data => {
				if(data != '' && data != undefined){
					this.descMinuta = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NQY_DESC");

				}
			});
		}
	}


	/**
	 * Responsável por obter os dados do tipo de contrato
	 */
	getTpContrato(){

		this.bBaixaMinuta = true;

		if(this.codTpContrato != ""){
		
			this.fwmodel.restore();
			this.fwmodel.setModelo("JURA134");
			this.fwmodel.setCampoVirtual(true);
			this.fwmodel.setFirstLevel(false);
			this.fwmodel.setFilter("NY0_COD='" + this.codTpContrato + "'");

			this.fwmodel.get("Busca de Tipos de Contrato").subscribe(data => {

				
				if(data != '' && data != undefined){
					this.descTipoContrato = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NY0_DESC")
					this.pkTpContrato = data.resources[0].pk;
					this.descMinuta = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NY0_DESREL")
					this.formTpContrato.patchValue({
						tipoContrato: this.descTipoContrato,
						motCancela:   this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NY0_CMOCAN"),
						minuta:       this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NY0_CODREL")
						
					});
				}

				this.breadcrumb.items[this.breadcrumb.items.length -1].label = this.descTipoContrato
				this.changeMinuta();
			});
			
		}
	}


	/**
	 * Função de inicio das validações e Commit.
	 */
	confirmVerify(){
		if (this.formTpContrato.valid){
			this.submitTpContrato()
		}
	}


	/**
	 * Obtem os dados do formulário no momento da inclusão e passa como parâmetro
	 * para o post. Esses dados serão usados no body da requisição
	 * para realizar a inclusão através do FWModel.
	 */
	submitTpContrato() {
		this.loadingTpContrato = true;
		this.loadingBtnsave = true;

		if(this.pkTpContrato == "" ){
			this.insertTpContrato()
		}else{
			this.updateTpContrato()
		}
	}

	/**
	 * Faz o insert de um novo tipo de contrato
	 */
	insertTpContrato(){

		this.loadingTpContrato = true;
		this.loadingBtnsave = true;
		
		if (this.docUpload.hasOwnProperty('currentFiles') &&(this.JuriGenerics.changeUndefinedToEmpty(this.docUpload.currentFiles[0].name) != "" && !(this.bFileSent))) {
			this.submitDot();
		} else{
			this.fwmodel.restore();
			this.fwmodel.setFirstLevel(false)
			this.fwmodel.setModelo('JURA134');

			this.fwmodel.post(this.bodyTpContrato(),"Insere tipo de contrato").subscribe((data) => {
				if (data != '') {
					this.codTpContrato = this.JuriGenerics.findValueByName(data.models[0].fields, "NY0_COD")
					this.loadingTpContrato = false;
					this.loadingBtnsave = false;
					this.router.navigate([this.routingState.getPreviousUrl('/')], { relativeTo : this.route })
					this.poNotification.success(this.litDetTpContrato.msgInclusao);

				} else {
					this.loadingTpContrato = false;
					this.loadingBtnsave = false;
				}
			});
	
		}
		return true;
	}

	/**
	 * Altera a Tipo de contrato
	 *
	*/
	updateTpContrato(){

		this.loadingTpContrato = true;
		this.loadingBtnsave = true;
	
		if (this.docUpload.hasOwnProperty('currentFiles') &&(this.JuriGenerics.changeUndefinedToEmpty(this.docUpload.currentFiles[0].name) != "" && !(this.bFileSent))) {
			this.submitDot();
		} else{
			this.fwmodel.restore();
			this.fwmodel.setFirstLevel(false)
			this.fwmodel.setModelo('JURA134');

			this.fwmodel.put(this.pkTpContrato,this.bodyTpContrato(),"Altera tipo de contrato").subscribe((data) => {
				if (data != '') {
					this.codTpContrato = this.JuriGenerics.findValueByName(data.models[0].fields, "NY0_COD")
					this.loadingTpContrato = false;
					this.loadingBtnsave = false;
					this.router.navigate([this.routingState.getPreviousUrl('/')], { relativeTo : this.route })
					this.poNotification.success(this.litDetTpContrato.msgAlteracao);

				} else {
					this.loadingTpContrato = false;
					this.loadingBtnsave = false;
				}
			});
	
		}
		return true;
	}

	/**
	 * Monta o body 
	 */
	bodyTpContrato(){
		let bodyAtualiza: StructTpContrato = {
			pk: "",
			id: "JURA134", 
			operation: 4, 
			models: [{
				id: "NY0MASTER", 
				modeltype: "FIELDS",
				fields: [
					{ id: "NY0_DESC", order: 1, value: JurUTF8.encode(this.JuriGenerics.changeUndefinedToEmpty(this.formTpContrato.value.tipoContrato)) },
					{ id: "NY0_CODREL", order: 2, value: JurUTF8.encode(this.JuriGenerics.changeUndefinedToEmpty(this.formTpContrato.value.minuta)) },
					{ id: "NY0_CMOCAN",  order: 3, value: JurUTF8.encode(this.JuriGenerics.changeUndefinedToEmpty(this.formTpContrato.value.motCancela)) }
				]
			}]

		}


		return JSON.stringify(bodyAtualiza)
	}


	/**
	 * Envia o arquivo .DOT para leitura de variáveis e criação da configuração no JURA014
	 */
	submitDot(){
		let tOk: boolean = false;

		if (this.JuriGenerics.changeUndefinedToEmpty(this.docUpload.currentFiles[0].name) != "") {

			this.docUpload.url = this.url;

			if (this.operacaoMinuta ){
				this.docUpload.url += "?nOpc=3";

			} else {
				this.docUpload.url += "?nOpc=4";
			};

			this.docUpload.url +="&nomeConfig=" + encodeURIComponent(this.docUpload.currentFiles[0].name.toUpperCase().replace('.DOT',''));
			this.docUpload.url +="&codConfig=" + encodeURIComponent(this.JuriGenerics.changeUndefinedToEmpty(this.formTpContrato.value.minuta))
			this.docUpload.sendFiles();


		}
	}





	/**
	 * Ação executada em caso de sucesso processamento do .DOT 
	 * @param event código e mensagem de Sucesso
	 */
	uploadSuccess(event:EventEmitter <any>){
		let response: any;

		response = event;
		this.formTpContrato.value.minuta = response.body.codConfig;
		this.bFileSent = true;
		
		if(this.pkTpContrato == ""){
			this.insertTpContrato();
		} else {
			this.updateTpContrato();
		}
		
	}
	

	/**
	 * Ação executada em caso de falha processamento do .DOT 
	 * @param event código e mensagem de erro
	 */
	uploadError(event:EventEmitter <any>){
		let response: any;

		response = event;
		this.loadingBtnsave = false;
		this.poNotification.error(response.error.errorMessage.toString());
	}


	/**
	 * Função de validação e delete.
	 */
	deleteVerify(){
		if (this.beforeDelete()){
			this.delTpContrato()
		}
	}


	/**
	 * Validação pré-delete
	 */
	beforeDelete(){
		let isSubmitable: boolean = false;

		isSubmitable = this.formTpContrato.valid;
	
		return isSubmitable
	}


	/**
	 * Responsavel por remover um tipo de contrato
	 */
	delTpContrato() {  
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA134');
		this.fwmodel.delete(this.pkTpContrato, "Deleta tipo de contrato").subscribe(data=>{
			if (data == true){
				this.poNotification.success(this.litDetTpContrato.msgExclusao);
				this.excluiModal.close();
				this.router.navigate([this.routingState.getPreviousUrl('/')], { relativeTo : this.route })
			}
		})
	}
	/**
	 * Redireciona para a página principal de tipo de contrato caso o usuário selecione o botão de "Cancelar"
	 */
	onCancel(){
		this.router.navigate([this.routingState.getPreviousUrl('/')], { relativeTo : this.route })
	}

	/**
	 * Faz o download do arquivo .DOT cadastrado
	 */
	baixaMinuta(){
		let codigoMinuta = this.formTpContrato.value.minuta

		this.jurConsultas.restore();
		this.jurConsultas.get("downloadFile/"+ codigoMinuta , "Baixa Modelo de minuta").subscribe(fileData => {
			if (this.JuriGenerics.changeUndefinedToEmpty(fileData.downloadMinuta) != '' ) {
				fileData.downloadMinuta.forEach(item => {
					var blob = JurBase64.toUTF8(item.filedata, 'application/octet-stream');
					var fileLink = document.createElement('a');

					fileLink.href = window.URL.createObjectURL(blob);;
					item.namefile = this.descMinuta + '.dot' 
					fileLink.download = item.namefile; //força o nome do download
					fileLink.target = "_blank"; //força a abertura em uma nova página
					document.body.appendChild(fileLink);
					fileLink.click(); //gatilha o evento do click
					this.poNotification.success(this.litDetTpContrato.msgSucess)
				
				})
			}
		});

	}


	/**
	 * Alterna a opoeração do upload entre inclusão e alteração
	 */
	changeMinuta(){

		if(this.formTpContrato.value.minuta == "" || this.formTpContrato.value.minuta == undefined ){
			this.operacaoMinuta = true;
			this.lblOperacao = this.litDetTpContrato.novaMinuta;
			this.bBaixaMinuta = true; 

		} else{
			this.operacaoMinuta = false;
			this.lblOperacao = this.litDetTpContrato.alteraMinuta;
			this.bBaixaMinuta = false;
		}

	}

	onChange($event){
		this.descMinuta = $event.target.value;
		this.formTpContrato.value.descMinuta = this.descMinuta
	}

	novaMinuta(){
		this.selectFile();
		this.operacaoMinuta = true;

	}

	atualizaMinuta(){
		this.selectFile();
		this.operacaoMinuta = false;

	}

	/**
	 * Chama a função para seleção de arquivo para importação de petição inicial
	 */
	selectFile(){

			this.docUpload.selectFiles();
	}


	changeBtn(){
		this.toggleNovaMinuta = true;

		if(this.formTpContrato.value.minuta == "" || this.formTpContrato.value.minuta == undefined ){
			this.bBaixaMinuta = true; 
		}else {
			this.bBaixaMinuta = false;
		}


		if(this.formTpContrato.value.minuta == "" || this.formTpContrato.value.minuta == undefined ){
			this.operacaoMinuta = true;
		} else {
			this.operacaoMinuta = !this.operacaoMinuta;
		};

		if( this.operacaoMinuta ) {
			this.lblOperacao = this.litDetTpContrato.novaMinuta; 
		} else {
			this.lblOperacao = this.litDetTpContrato.alteraMinuta;
		}
	}

}


/**
 *  Constroi a estrutura que sera utilizada para montar o body
 */
interface StructTpContrato {
	id        : string ;
	operation : number ;
	pk        : string ;

	models: [{ 
		id: string, 
		modeltype: string, 
		fields: [
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string }
		]
	}]
}