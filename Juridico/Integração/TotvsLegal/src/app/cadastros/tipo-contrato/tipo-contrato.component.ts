import { Component, OnInit } from '@angular/core';
import { PoBreadcrumb, PoTableColumn } from '@po-ui/ng-components'
import { FwmodelService } from 'src/app/services/fwmodel.service';
import { JurigenericsService } from '../../services/jurigenerics.service';
import { PoI18nService } from '@po-ui/ng-components';
import { ActivatedRoute, Router } from '@angular/router';
import { RoutingService } from '../../services/routing.service';
import { adaptorFilterParam, FwmodelAdaptorService } from '../../services/fwmodel-adaptor.service';


/**
 * Interface para tipo de contrato
 */
class TipoContrato{
	codigo: string;
	descricao: string;
	motCancel: string;
	editaTpContrato: Array<string>;
}


@Component({
	selector: 'app-tipo-contrato',
	templateUrl: './tipo-contrato.component.html',
	styleUrls: ['./tipo-contrato.component.css']
})

export class TipoContratoComponent implements OnInit {
	columns            : Array<PoTableColumn> ;
	tpContratoList     : Array<any> = [] ;
	litTipoContrato    : any;
	codPro             : string     = ''
	filial             : string     = '';
	searchKey          : string     = '' ;
	btnNxtDisable      : boolean    = false ;
	btnPreDisable      : boolean    = false ;
	hasData            : boolean    = true ;
	isHideLoadingGrig  : boolean    = false;
	isShowMoreDisabled : boolean    = false;  
	length             : number     = 0 ;
	maxPage            : number     = 1 ;
	page               : number     = 0 ;
	pageSize           : number     = 20;
	regIndex           : number     = 1;

	breadcrumb: PoBreadcrumb;

	// Adapter do tipo de Contrato
	adaptorParamTipoContrato: adaptorFilterParam = new adaptorFilterParam(
		"JURA134", "", "NY0_DESC", "NY0_DESC", "NY0_DESC", "NY0_COD", "", "Combo de tipo de contrato");

	constructor(
		private poI18nService: PoI18nService,
		private fwmodel: FwmodelService,
		public fwModelAdaptorService: FwmodelAdaptorService,
		private JuriGenerics: JurigenericsService,
		private router: Router,
		private route: ActivatedRoute,
		private routerState: RoutingService,
	) {
		this.routerState.loadRouting();

		//-- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'prov' no arquivo JurTraducao.
		poI18nService
			.getLiterals( { context: 'tipoContrato', language: poI18nService.getLanguage() } )
			.subscribe(
				(litTipoContrato) => {
					this.litTipoContrato = litTipoContrato;
		
					//-- seta as constantes para tradução do titulo das colunas do Grid de Provisões
					this.TpContratoColumns[0].label = this.litTipoContrato.colTipo; 
					this.TpContratoColumns[1].label = this.litTipoContrato.colmot; 
					this.setBreadCrumb()
				}
			);	
	}

	ngOnInit() {
		this.page = 1;
		this.searchKey = ""
		this.columns = this.TpContratoColumns;
		this.getTpContrato();
	}

	/**
	 * método responsavel pela criação do breadcrumb conforme as literais criadas
	 */
	private setBreadCrumb() {
		this.breadcrumb = {
			items: [
				{ label: this.litTipoContrato.home, link: '/' },
				{ label: this.litTipoContrato.cadastros, link: '/Cadastros' },
				{ label: this.litTipoContrato.titulo, link: '/Cadastros/tipocontrato' }
			]
		}

	}
	/**
	 * colunas da tabela 
	 */
	TpContratoColumns: Array<PoTableColumn> = [
		{ property: 'descricao'},
		{ property: 'motCancel'},
		{ property: 'editaTpContrato', label: ' ', type: 'icon', width: '5%',
			 icons: [ { value: 'editaContrato', icon: 'po-icon po-icon-edit', color: '#29b6c5', tooltip: 'Editar', 
						action:(value, row)=>{ this.callTipoContrato(value) } } ] 
		}
	];
		
	/**
	 * Lista os tipos de contratos 
	 */
	getTpContrato (){

		this.isHideLoadingGrig = false;
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA134");
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.setPageSize(this.pageSize.toString());
		this.fwmodel.setPage(this.regIndex.toString());

		//Se for pesquisa, filtra pela descrição do tipo de contrato
		if (this.searchKey != '' && this.searchKey != undefined) {
			this.fwmodel.setSearchKey("NY0_COD = '" + this.searchKey + "' " );
		}

		if (this.regIndex == 1){
			this.tpContratoList = [];
		}


		this.fwmodel.get("Lista de Tipos de Contrato").subscribe(data=>{
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != '') {
				data.resources.forEach(item => {
					let itemTpContrato = new TipoContrato();

					itemTpContrato.descricao       = this.JuriGenerics.findValueByName(item.models[0].fields, "NY0_DESC");
					itemTpContrato.motCancel       = this.JuriGenerics.findValueByName(item.models[0].fields, "NY0_DMOCAN");
					itemTpContrato.editaTpContrato = ['editaContrato'];
					itemTpContrato.codigo          = this.JuriGenerics.findValueByName(item.models[0].fields, "NY0_COD");

					this.tpContratoList.push(itemTpContrato);
				})
			};
			this.setShowMoreregistros(data.total);
			this.isHideLoadingGrig = true;
		
		})
		return true;
	}

	/**
	 * Incrementa o index de paginação da tela
	 * @param total Total de registros
	 */
	setShowMoreregistros(total: number) {

		this.regIndex += this.pageSize;

		if (this.regIndex <= total) {
			this.isShowMoreDisabled = false;
		}else{
			this.isShowMoreDisabled = true;
		};
	}
	/**
	 * Função responsável pela chamada da inclusão ou manutenção do tipo de contrato
	 * @param codTpContrato - código do tipo de contrato, caso não informado será redirecionado para uma inclusão
	 */
	callTipoContrato(codTpContrato: any){
		if(codTpContrato == undefined || codTpContrato == "" )
		{
			this.router.navigate(["./novo"], {relativeTo: this.route})
		}else{
			this.router.navigate(["./"+ btoa(codTpContrato.codigo)], {relativeTo: this.route})
		}
	}


	/**
	* ação do botão Pesquisa
	*/	  
	search() {

		this.regIndex = 1;
		this.getTpContrato()
		
	}

	/**
	 * Ação do botão Carregar mais resultados
	 */
	showMore() {
		this.isShowMoreDisabled = true;
		this.getTpContrato();
	}


}
