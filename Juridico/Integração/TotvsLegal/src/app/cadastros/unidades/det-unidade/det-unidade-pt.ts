/**
* Arquivo de constantes para tradução da página detalhe de Unidade
*/

const breadCrumb = {
	unidades  :     'Unidades',
	novaUnidade :   'Nova unidade', 
	tituloAlterar : 'Alterar unidade',
	home:           'Home',
	cadastros:      'Cadastros'
}
const modal ={
	titleExclusao:  'Atenção!',
	exclusao :      'A unidade será excluída. Deseja prosseguir?',
	btnExcluir:     'Excluir',
	btnFechar:      'Fechar'
}
const opcaoFixa = {
	pessoaFisica:  'Física ',
	pessoaJuridi:  'Jurídica ',
	consumidor:    'Consumidor final',
	produtor:      'Produtor rural',
	revendedor:    'Revendedor',
	solidario:     'Solidário' ,
	exportacao:    'Exportação',
	ativo:         'Ativo',
	inativo:       'Inativo'
}

const form = {
	codigo :       'Código',
	loja :         'Loja',
	razaoSocial :  'Razão social',
	nomeFantasia : 'Nome fantasia',
	tpPessoa :     'Tipo pessoa',
	cgc :          'CNPJ/CPF',
	tipo :         'Tipo',
	email :        'E-mail',
	estado :       'Estado',
	cidade :       'Cidade',
	telefone :     'Telefone',
	endereco :     'Endereço',
	cep :          'CEP',
	socioResp :    'Gestor do jurídico',
	idioma :       'Idioma',
	status :       'Status',
	nire :         'Nire',
	observacao :   'Observação',
}

const botao = {
	btnCancelar : 'Cancelar',
	btnExcluir :  'Excluir',
	btnSalvar :   'Salvar',
}
const mensagem = {
	msgInclusao :  'Unidade incluída com sucesso!',
	msgAlteracao : 'Unidade atualizada com sucesso!',
	msgExclusao :  'Unidade excluída com sucesso!',
	
	naoEncontrada: 'A unidade não foi encontrada!'
}

//-- Constantes principais
export const detUnidadePt = {
	tituloNovo :    'Nova unidade',
	tituloAlterar : 'Alterar unidade',
	breadCrumb :     breadCrumb,
	msg:             mensagem,
	btn:             botao,
	list:            opcaoFixa,
	form:            form,
	modal:           modal
}