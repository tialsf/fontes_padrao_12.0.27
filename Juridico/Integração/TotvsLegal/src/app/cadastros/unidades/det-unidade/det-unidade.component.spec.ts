import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetUnidadeComponent } from './det-unidade.component';

describe('DetUnidadeComponent', () => {
  let component: DetUnidadeComponent;
  let fixture: ComponentFixture<DetUnidadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetUnidadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetUnidadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
