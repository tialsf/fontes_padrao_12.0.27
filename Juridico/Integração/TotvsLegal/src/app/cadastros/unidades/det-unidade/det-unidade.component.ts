import { Component, OnInit, ViewChild } from '@angular/core';
import { PoBreadcrumb, PoI18nService, PoRadioGroupOption, PoComboComponent, PoNotificationService, PoModalComponent, PoModalAction, PoComboFilter } from '@po-ui/ng-components';
import { Router, ActivatedRoute } from '@angular/router';
import { RoutingService } from 'src/app/services/routing.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UnidadeDados } from './det-unidade';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { FwmodelService, FwModelBody, FwModelBodyModels } from 'src/app/services/fwmodel.service';
import { adaptorFilterParam, FwmodelAdaptorService } from 'src/app/services/fwmodel-adaptor.service';
import { LegalprocessService } from 'src/app/services/legalprocess.service';
import { CidadeAdapterService } from 'src/app/services/combos-fwmodel-adapter.service'

/**
 * Classe Padrão para utilizar nos retornos de serviços.
 */
class Padrao {
	label: string = '';
	value: string = '';
}

@Component({
	selector: 'app-det-unidade',
	templateUrl: './det-unidade.component.html',
	styleUrls: ['./det-unidade.component.css']
})
export class DetUnidadeComponent implements OnInit {
	litDetUnidade  : any;
	formUnidade    : FormGroup;

	codUnidade:     string = "";
	lojaUnidade:    string = "";
	pkUnidade:      string = "";
	titleDet:       string = "";
	lblBtnSalvar:   string = "";
	lblBtnCancelar: string = "";
	lblBtnExcluir:  string = "";
	estado:         string = '';
	cidade:         string = '';
	filtroSocio:    string = "RD0_TPJUR='1'";

	bOperacaoInclusao : boolean    = false;
	isDisableMunicipio : boolean   = true;
	isHideLoadingUnidade : boolean = false;
	loadingBtnsave : boolean       = false;

	readonly listTpPessoa: Array<PoRadioGroupOption> = [
		{ label: 'Física'   , value: 'F' },
		{ label: 'Juridica' , value: 'J' }
	]

	listTipo: Array<Padrao> = [
		{ label: 'Consumidor final', value: 'F' },
		{ label: 'Produtor rural',   value: 'L' },
		{ label: 'Revendedor',       value: 'R' },
		{ label: 'Solidário',        value: 'S' },
		{ label: 'Exportacao',       value: 'X' }
	]
	readonly listStatus: Array<PoRadioGroupOption> = [
		{ label: 'Ativo',    value: '1' },
		{ label: 'Inativo',  value: '2' }
	]

	readonly listEstados: Array<Padrao> = [
		{ value: 'AC', label: 'ACRE'                },
		{ value: 'AL', label: 'ALAGOAS'             },
		{ value: 'AM', label: 'AMAZONAS'            },
		{ value: 'AP', label: 'AMAPA'               },
		{ value: 'BA', label: 'BAHIA'               },
		{ value: 'CE', label: 'CEARA'               },
		{ value: 'DF', label: 'DISTRITO FEDERAL'    },
		{ value: 'ES', label: 'ESPIRITO SANTO'      },
		{ value: 'EX', label: 'ESTRANGEIRO'         },
		{ value: 'GO', label: 'GOIAS'               },
		{ value: 'MA', label: 'MARANHAO'            },
		{ value: 'MG', label: 'MINAS GERAIS'        },
		{ value: 'MS', label: 'MATO GROSSO DO SUL'  },
		{ value: 'MT', label: 'MATO GROSSO'         },
		{ value: 'PA', label: 'PARA'                },
		{ value: 'PB', label: 'PARAIBA'             },
		{ value: 'PE', label: 'PERNAMBUCO'          },
		{ value: 'PI', label: 'PIAUI'               },
		{ value: 'PR', label: 'PARANA'              },
		{ value: 'RJ', label: 'RIO DE JANEIRO'      },
		{ value: 'RN', label: 'RIO GRANDE DO NORTE' },
		{ value: 'RO', label: 'RONDONIA'            },
		{ value: 'RR', label: 'RORAIMA'             },
		{ value: 'RS', label: 'RIO GRANDE DO SUL'   },
		{ value: 'SC', label: 'SANTA CATARINA'      },
		{ value: 'SE', label: 'SERGIPE'             },
		{ value: 'SP', label: 'SAO PAULO'           },
		{ value: 'TO', label: 'TOCANTINS'           },
		
	];

	public breadcrumb: PoBreadcrumb = {
		items: []
	};

	// Ações do modal de confirmação de exclusão
	fechar: PoModalAction = {
		action: () => {
			this.excluiModal.close();
		},
		label: 'Fechar',
	};
	
	excluir: PoModalAction = {
		action: () => {
			this.excluirUnidade();
		},
		label: 'Excluir',
	};

	@ViewChild('tipoPessoa'   , { static: true  }) radioTipoPessoa: PoRadioGroupOption;
	@ViewChild('status'       , { static: true  }) radioStatus:     PoRadioGroupOption;
	@ViewChild('excluiUnidade', { static: true  }) excluiModal:     PoModalComponent;
	@ViewChild('tipo')                             cmbTipo:         PoComboComponent;

	constructor(private poI18nService: PoI18nService,
		private router: Router,
		private routingState: RoutingService,
		private route: ActivatedRoute,
		private formBuilder: FormBuilder,
		private JuriGenerics: JurigenericsService,
		private fwmodel: FwmodelService,
		public  poNotification: PoNotificationService,
		public  fwModelAdaptorService: FwmodelAdaptorService,
		private legalprocess: LegalprocessService,
		public cidadeAdapterService: CidadeAdapterService) {
		
		// Setups de inicialização de adapters
		this.fwModelAdaptorService.setup(new adaptorFilterParam("JURA159", "RD0_TPJUR='1'", "RD0_NOME", "RD0_NOME", "", "RD0_SIGLA", "", "Advogado Responsável", undefined, undefined, undefined, { bGetAllLevels: true, bUseEmptyField: true, bUseVirtualField: true, arrSetFields: [] }))
		this.cidadeAdapterService.setup(new adaptorFilterParam("JMUNICIPIO", "", "CC2_MUN", "CC2_MUN", "CC2_MUN", "CC2_CODMUN", "", "Get Municipio") );

		if (this.route.snapshot.paramMap.get("codUnidade") != undefined) {
			this.codUnidade = atob(decodeURIComponent(this.route.snapshot.paramMap.get("codUnidade"))); 
		}
		if (this.route.snapshot.paramMap.get("loja") != undefined) {
			this.lojaUnidade = atob(decodeURIComponent(this.route.snapshot.paramMap.get("loja"))); 
		}
		// Traduções
		poI18nService
			.getLiterals({ context: 'detUnidade', language: poI18nService.getLanguage() })
			.subscribe(
				(litDetUnidade) => {

					this.litDetUnidade = litDetUnidade;
				
					this.lblBtnExcluir = this.litDetUnidade.btn.btnExcluir;
					
					//Novo
					if (this.codUnidade == '' ) {
						this.bOperacaoInclusao = true;
						this.titleDet     = this.litDetUnidade.tituloNovo;
						this.lblBtnSalvar = this.litDetUnidade.btnSalvar;

					//Atualização	
					} else {
						this.titleDet     = this.litDetUnidade.tituloAlterar;
						this.lblBtnSalvar = this.litDetUnidade.btnSalvar;
					}

					this.breadcrumb.items = [
						{ label: this.litDetUnidade.breadCrumb.home      , link: '/' },
						{ label: this.litDetUnidade.breadCrumb.cadastros , link: '/Cadastros' },
						{ label: this.litDetUnidade.breadCrumb.unidades  , link: '/Cadastros/unidades' },
						{ label: this.titleDet }
					];

					//-- seta as constantes para tradução do conteúdo do componente Tipo de Pessoa
					this.listTpPessoa[0].label = this.litDetUnidade.list.pessoaFisica;
					this.listTpPessoa[1].label = this.litDetUnidade.list.pessoaJuridi;

					//-- seta as constantes para tradução do conteúdo do componente Status
					this.listStatus[0].label = this.litDetUnidade.list.ativo;
					this.listStatus[1].label = this.litDetUnidade.list.inativo;

					//-- seta as constantes para tradução do conteúdo do componente Tipo de cliente
					this.listTipo[0].label = this.litDetUnidade.list.consumidor;
					this.listTipo[1].label = this.litDetUnidade.list.produtor;
					this.listTipo[2].label = this.litDetUnidade.list.revendedor;
					this.listTipo[3].label = this.litDetUnidade.list.solidario;
					this.listTipo[4].label = this.litDetUnidade.list.exportacao;

					this.fechar.label = this.litDetUnidade.modal.btnFechar;
					this.excluir.label = this.litDetUnidade.modal.btnExcluir;
				}
			);

	 }

	ngOnInit() {
		this.createFormUnidade(new UnidadeDados());
		this.getUnidade();
		
	}

	/**
	 * Responsável por obter os dados inputados
	 * no formulário de inclusão de Unidade
	 */
	createFormUnidade(unidade: UnidadeDados) {
		this.formUnidade = this.formBuilder.group({
			codigo:       [unidade.codigo, Validators.compose([Validators.required])],
			loja:         [unidade.loja, Validators.compose([Validators.required])],
			razaoSocial:  [unidade.razaoSocial, Validators.compose([Validators.required]) ],
			nomeFantasia: [unidade.nomeFantasia, Validators.compose([Validators.required]) ],
			tpPessoa:     [unidade.tpPessoa],
			cgc:          [unidade.cgc],
			tipo:         [unidade.tipo, Validators.compose([Validators.required]) ],
			email:        [unidade.email ],
			estado:       [unidade.estado, Validators.compose([Validators.required])],
			cidade:       [unidade.cidade, Validators.compose([Validators.required])],
			telefone:     [unidade.telefone ],
			endereco:     [unidade.endereco, Validators.compose([Validators.required]) ],
			cep:          [unidade.cep],
			socioResp:    [unidade.socioResp, Validators.compose([Validators.required])],
			status:       [unidade.status, Validators.compose([Validators.required]) ],
			nire:         [unidade.nire ],
			observacao:   [unidade.observacao ]
		});
	}

	/**
	 * Busca a unidade se for em modo de alteração, se não, busca o próx sequencial disponivel para a tabela
	 */
	getUnidade(){
		this.isHideLoadingUnidade = false;
		if (this.JuriGenerics.changeUndefinedToEmpty(this.codUnidade) == '') {
			this.legalprocess.restore();
			this.legalprocess.get("getSequencial","Busca o proximo sequencial disponivel").subscribe(data => {
				if (data != '') {
					//fazer a carga do campo codigo e a loja
					this.formUnidade.patchValue({
						codigo: data.sequencial,
						loja:   '01'
					})
				}
				this.isHideLoadingUnidade = true;
			})
			
		}else{
			this.fwmodel.restore();
			this.fwmodel.setModelo("JCLIDEP");
			this.fwmodel.setFilter("A1_COD ='" + this.codUnidade + "'");
			this.fwmodel.setFilter("A1_LOJA ='" + this.lojaUnidade + "'");
			this.fwmodel.setCampoVirtual(true);
			this.fwmodel.setFirstLevel(false);
			this.fwmodel.get("Busca unidade").subscribe(data => {
				if (data.count === 0) {
					this.poNotification.error(this.litDetUnidade.msg.naoEncontrada);
					this.isHideLoadingUnidade = true;
				} else {
					this.pkUnidade = data.resources[0].pk;

					this.formUnidade.patchValue({
						codigo:       this.JuriGenerics.findValueByName(data.resources[0].models[0].fields          , "A1_COD"    ),
						loja:         this.JuriGenerics.findValueByName(data.resources[0].models[0].fields          , "A1_LOJA"   ),
						razaoSocial:  this.JuriGenerics.findValueByName(data.resources[0].models[0].fields          , "A1_NOME"   ),
						nomeFantasia: this.JuriGenerics.findValueByName(data.resources[0].models[0].fields          , "A1_NREDUZ" ),
						cgc:          this.JuriGenerics.findValueByName(data.resources[0].models[0].fields          , "A1_CGC"    ),
						tpPessoa:     this.JuriGenerics.findValueByName(data.resources[0].models[0].fields          , "A1_PESSOA" ),
						tipo:         this.JuriGenerics.findValueByName(data.resources[0].models[0].fields          , "A1_TIPO"   ),
						email:        this.JuriGenerics.findValueByName(data.resources[0].models[0].fields          , "A1_NOME"   ),
						estado:       this.JuriGenerics.findValueByName(data.resources[0].models[0].fields          , "A1_EST"    ),
						cidade:       this.JuriGenerics.findValueByName(data.resources[0].models[0].fields          , "A1_COD_MUN"),
						telefone:     this.JuriGenerics.findValueByName(data.resources[0].models[0].fields          , "A1_TEL"    ),
						endereco:     this.JuriGenerics.findValueByName(data.resources[0].models[0].fields          , "A1_END"    ),
						bairro:       this.JuriGenerics.findValueByName(data.resources[0].models[0].fields          , "A1_BAIRRO" ),
						cep:          this.JuriGenerics.findValueByName(data.resources[0].models[0].fields          , "A1_CEP"    ),
						socioResp:    this.JuriGenerics.findValueByName(data.resources[0].models[0].models[0].fields, "NUH_SIGLA" ),
						status:       this.JuriGenerics.findValueByName(data.resources[0].models[0].models[0].fields, "NUH_ATIVO" ),
						nire:         this.JuriGenerics.findValueByName(data.resources[0].models[0].models[0].fields, "NUH_NIRE"  ),
						observacao:   this.JuriGenerics.findValueByName(data.resources[0].models[0].models[0].fields, "NUH_OBSCAD"),
					});
					this.isHideLoadingUnidade = true;

				}
				this.isHideLoadingUnidade = true;
			});
			
		}
		
	}

	/**
	 * Seta o estado para filtrar as cidades de acordo com o estado
	 * @param estado
	 */
	setEstado(estado: string){
		this.cidade = '';

		if (this.JuriGenerics.changeUndefinedToEmpty(estado) != '') {
			estado    = "CC2_EST = '" + estado + "'"
			this.cidadeAdapterService.setFiltroP(estado);
			this.isDisableMunicipio    = false;
		} else {
			this.isDisableMunicipio = true;
		}
		this.isHideLoadingUnidade = true;
	}

	/**
	 * Cancela operação
	 */
	onCancel() {
		this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codUnidade))]);
	}

	/**
	 * Obtem os dados do formulário no momento da inclusão e passa como parâmetro
	 * para o post. Esses dados serão usados no body da requisição
	 * para realizar a inclusão através do FWModel.
	 */
	submitUnidade() {
		this.loadingBtnsave = true;
		this.isHideLoadingUnidade = false;
		if(this.bOperacaoInclusao){
			this.insertUnidade()
		}else{
			this.updateUnidade()
		}
		
	}
	/**
	 * Faz o insert de uma nova unidade
	 */
	insertUnidade(){

		this.loadingBtnsave = true;
		this.fwmodel.restore();
		this.fwmodel.setFirstLevel(false)
		this.fwmodel.setModelo('JURA148');

		this.fwmodel.post(this.bodyUnidade(),"Inclusão da Unidade").subscribe((data) => {
			if (data != '') {
				this.loadingBtnsave = false;
				this.router.navigate([this.routingState.getPreviousUrl(this.codUnidade)])
				this.poNotification.success(this.litDetUnidade.msg.msgInclusao);
			} else {
				this.loadingBtnsave = false;
				this.isHideLoadingUnidade = true;
			}
		});
	}

	/**
	 * Faz o update de uma unidade
	 */
	updateUnidade(){
		this.loadingBtnsave = true;

		this.fwmodel.restore();
		this.fwmodel.setFirstLevel(false)
		this.fwmodel.setModelo('JURA148');
		this.fwmodel.setFilter("A1_COD='" + this.codUnidade + "'");
		this.fwmodel.setFilter("A1_LOJA='" + this.lojaUnidade + "'");

		this.fwmodel.put(this.pkUnidade, this.bodyUnidade(),"Atualiza Unidade").subscribe((data) => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data) != '') {
				this.loadingBtnsave = false;
				this.router.navigate([this.routingState.getPreviousUrl(this.codUnidade)])
				this.poNotification.success(this.litDetUnidade.msg.msgAlteracao);

			} else {
				this.isHideLoadingUnidade = true;
				this.loadingBtnsave = false;
			}
		});
	
		
		return true;
	}
	/**
	 * Responsável por excluir a unidade
	 */
	excluirUnidade() {
		this.excluiModal.primaryAction.loading = true;
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA148');
		this.fwmodel.delete(this.pkUnidade, "Deleta a unidade Unidade").subscribe((data) => {
			if (data != '') {
				this.loadingBtnsave = false;
				this.router.navigate([this.routingState.getPreviousUrl(this.codUnidade)])
				this.poNotification.success(this.litDetUnidade.msg.msgExclusao);
				this.excluiModal.primaryAction.loading = false;
			} else {
				this.loadingBtnsave = false;
				this.excluiModal.primaryAction.loading = false;
			}
		});
	}
	
	/**
	 * Monta o body 
	 */
	bodyUnidade(){
		let fwModelBody: FwModelBody = new FwModelBody('JURA148',3, this.pkUnidade);
		let fwModelBodyNUH: FwModelBodyModels;
		let formUnidade = this.formUnidade.value;

		//-- MODEL SA1 
		fwModelBody.addModel("SA1MASTER","FIELDS",1)
		fwModelBody.models[0].addField("A1_COD"     , formUnidade.codigo       , 0, "C")
		fwModelBody.models[0].addField("A1_LOJA"    , formUnidade.loja         , 0, "C")
		fwModelBody.models[0].addField("A1_NOME"    , formUnidade.razaoSocial  , 0, "C")
		fwModelBody.models[0].addField("A1_NREDUZ"  , formUnidade.nomeFantasia , 0, "C")
		fwModelBody.models[0].addField("A1_END"     , formUnidade.endereco     , 0, "C")
		fwModelBody.models[0].addField("A1_TIPO"    , formUnidade.tipo         , 0, "C")
		fwModelBody.models[0].addField("A1_EST"     , formUnidade.estado       , 0, "C")
		fwModelBody.models[0].addField("A1_COD_MUN" , formUnidade.cidade       , 0, "C")
		fwModelBody.models[0].addField("A1_CGC"     , formUnidade.cgc          , 0, "C")
		fwModelBody.models[0].addField("A1_PESSOA"  , formUnidade.tpPessoa     , 0, "C")
		fwModelBody.models[0].addField("A1_CEP"     , formUnidade.cep          , 0, "C")
		fwModelBody.models[0].addField("A1_TEL"     , formUnidade.telefone     , 0, "C")
		fwModelBody.models[0].addField("A1_EMAIL"   , formUnidade.email        , 0, "C")

		//-- MODEL NUH 
		fwModelBodyNUH = fwModelBody.models[0].addModel("NUHMASTER", "FIELDS", 1)
		fwModelBodyNUH.addField("NUH_SIGLA" , formUnidade.socioResp , 0, "C")
		fwModelBodyNUH.addField("NUH_ATIVO" , formUnidade.status    , 0, "C")
		fwModelBodyNUH.addField("NUH_NIRE"  , formUnidade.nire      , 0, "C")
		fwModelBodyNUH.addField("NUH_OBSCAD", formUnidade.observacao, 0, "C")
		fwModelBodyNUH.addField("NUH_CIDIO" , '01'                  , 0, "C")

		return fwModelBody.serialize();
	}


}
