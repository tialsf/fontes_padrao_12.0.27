export class UnidadeDados {
	codigo: string       = '';
	loja: string         = '';
	razaoSocial: string  = '';
	nomeFantasia: string = '';
	tpPessoa: string     = '';
	cgc: string          = '';
	tipo: string         = '';
	email: string        = '';
	estado: string       = '';
	cidade: string       = '';
	telefone: string     = '';
	endereco: string     = '';
	cep: string          = '';
	socioResp: string    = '';
	idioma: string       = '';
	status: string       = '';
	nire: string         = '';
	observacao: string   = '';

}