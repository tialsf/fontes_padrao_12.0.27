const breadCrumb = {
	home        : 'Home',
	cadastros   : 'Cadastros',
	titulo      : 'Unidades',
}
const gridUnidades ={
	codigo      : 'Código',
	razaoSocial : 'Razão social',
	nomeFantasia: 'Nome fantasia',
	cgc         : 'CPF/CNPJ',
	status      : 'Status',
	ativo       : 'Ativo',
	inativo     : 'Inativo',
	descricao   : 'Editar'
}

//-- Constantes principais
export const unidadePt = {
	btnNovaUnidade : 'Nova unidade',
	pesquisar      : 'Pesquisar',
	loadProv       : 'Carregando...',
	titulo         : 'Unidades',
	breadCrumb     : breadCrumb,
	grid           : gridUnidades
}