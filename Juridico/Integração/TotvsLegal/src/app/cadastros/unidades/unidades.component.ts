import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PoTableColumn, PoBreadcrumb, PoI18nService } from '@po-ui/ng-components';
import { RoutingService } from 'src/app/services/routing.service';
import { FwmodelService } from 'src/app/services/fwmodel.service';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { adaptorFilterParam, FwmodelAdaptorService } from 'src/app/services/fwmodel-adaptor.service';


/**
 * Classe para o retorno da lista de unidades
 */
class Unidades{
	codigo      : string;
	loja        : string;
	razaoSocial : string;
	nomeFantasia: string;
	cgc         : string;
	status      : string;
	editaUnidade: Array<string>;
}


@Component({
	selector: 'app-unidades',
	templateUrl: './unidades.component.html',
	styleUrls: ['./unidades.component.css']
})
export class UnidadesComponent implements OnInit {

	searchKey: string          = '';

	nPageSize: number          = 20;
	nRegIndex: number          = 1;
	nTotalTarefas: number      = 0;
	nNextPage: number          = 1;

	columns            : Array<PoTableColumn> ;
	listUnidades       : Array<Unidades> = [] ;
	
	isShowMoreDisabled: boolean   = false;
	isHideLoadingGrig: boolean    = false;

	litUnidades        : any;
	
	/**
	 * estrutura de navegação da URL atual
	 */
	public breadcrumb: PoBreadcrumb = {
		items: [],
	};

	adaptorParamUnidades: adaptorFilterParam = new adaptorFilterParam(
		"JCLIDEP", "", "A1_NOME", "A1_NOME,A1_COD,A1_LOJA", "A1_NOME [A1_COD/A1_LOJA]", "A1_COD,A1_LOJA", "A1_CODA1_LOJA", "Get Unidades");
	/**
	 * colunas da tabela 
	 */
	unidadesColumns: Array<PoTableColumn> = [
		{ property: 'codigo'},
		{ property: 'razaoSocial'},
		{ property: 'nomeFantasia'},
		{ property: 'cgc'},
		{
			property: 'status', type: 'label',
			labels: [
				{ value: '1', color: 'color-11', label: 'Ativo' },
				{ value: '2', color: 'color-07', label: 'Inativo' } // vermelho quando a tarefa está pendente e atrasada
			]
		},
		{ property: 'editaUnidade', label: ' ', type: 'icon', width: '5%',
			 icons: [ { value: 'editaUnidade', icon: 'po-icon po-icon-edit', color: '#29b6c5', tooltip: 'Editar', 
						action:(value, row)=>{ this.callUnidades(value) } } ] 
		}
	];

	
	constructor(private router: Router,
		private route: ActivatedRoute,
		private routerState: RoutingService,
		private poI18nService: PoI18nService,
		private fwmodel: FwmodelService,
		private JuriGenerics: JurigenericsService,
		public fwModelAdaptorService: FwmodelAdaptorService,
	) {
			
		this.routerState.loadRouting();

		//-- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'prov' no arquivo JurTraducao.
		poI18nService
			.getLiterals( { context: 'unidade', language: poI18nService.getLanguage() } )
			.subscribe(
				(litUnidades) => {
					this.litUnidades = litUnidades;
		
					//-- seta as constantes para tradução do titulo das colunas do Grid de Provisões
					this.unidadesColumns[0].label = this.litUnidades.grid.codigo;
					this.unidadesColumns[1].label = this.litUnidades.grid.razaoSocial;
					this.unidadesColumns[2].label = this.litUnidades.grid.nomeFantasia;
					this.unidadesColumns[3].label = this.litUnidades.grid.cgc;
					this.unidadesColumns[4].label = this.litUnidades.grid.status;
					this.unidadesColumns[4].labels[0].label  = this.litUnidades.grid.ativo;
					this.unidadesColumns[4].labels[1].label  = this.litUnidades.grid.inativo;
					this.unidadesColumns[5].icons[0].tooltip = this.litUnidades.grid.descricao;
					this.setBreadCrumb();
				}
			);	

		 }

	ngOnInit() {
		this.columns = this.unidadesColumns;
		this.searchKey = "";
		this.getUnidades();
	}


	/**
	 * Lista de Unidades/Clientes
	 */
	getUnidades(){

		this.isHideLoadingGrig = false;
		this.fwmodel.restore();
		this.fwmodel.setModelo("JCLIDEP");
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.setPageSize(this.nPageSize.toString());
		this.fwmodel.setPage(this.nRegIndex.toString());
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.setHeaderParam('orderBy', ' ORDER BY A1_COD DESC ');
		//Se for pesquisa, filtra pelo codigo e loja da unidade
		if (this.searchKey != '' && this.searchKey != undefined) {
			let nQtdCaracter = this.searchKey.length
			this.nRegIndex = 1
			this.fwmodel.setSearchKey("A1_COD = '" + this.searchKey.substr(0, nQtdCaracter-2 ) + "' " );
			this.fwmodel.setSearchKey("A1_LOJA = '" + this.searchKey.substr( nQtdCaracter-2 ) + "' " );
		}

		if (this.nRegIndex == 1){
			this.listUnidades = [];
		}


		this.fwmodel.get("Lista de Unidades/Clientes").subscribe(data=>{
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != '') {
				data.resources.forEach(item => {
					let itemUnidades = new Unidades();

					itemUnidades.codigo       = this.JuriGenerics.findValueByName(item.models[0].fields, "A1_COD");
					itemUnidades.loja         = this.JuriGenerics.findValueByName(item.models[0].fields, "A1_LOJA");
					itemUnidades.razaoSocial  = this.JuriGenerics.findValueByName(item.models[0].fields, "A1_NOME");
					itemUnidades.nomeFantasia = this.JuriGenerics.findValueByName(item.models[0].fields, "A1_NREDUZ")
					itemUnidades.cgc          = this.JuriGenerics.findValueByName(item.models[0].fields, "A1_CGC")
					itemUnidades.status       = this.JuriGenerics.findValueByName(item.models[0].models[0].fields, "NUH_ATIVO")
					itemUnidades.editaUnidade = ['editaUnidade'];

					this.listUnidades.push(itemUnidades);
				})
			};
			this.setShowMore(data.total);
			this.isHideLoadingGrig = true;
		})
	}
	/**
	 * método responsavel pela criação do breadcrumb conforme as literais criadas
	 */
	private setBreadCrumb() {
		this.breadcrumb = {
			items: [
				{ label: this.litUnidades.breadCrumb.home, link: '/' },
				{ label: this.litUnidades.breadCrumb.cadastros, link: '/Cadastros' },
				{ label: this.litUnidades.breadCrumb.titulo, link: '/Cadastros/unidades' }
			]
		}
	}
	/**
	 * Responsável por chamar a tela de det unidade em modo de inclusão ou alteração
	 * @param codUnidade - Se tiver preenchido sabemos que é alteração
	 */
	callUnidades(codUnidade: any){
		if(codUnidade == undefined || codUnidade == "" )
		{
			this.router.navigate(["./novo"], {relativeTo: this.route})
		}else{
			this.router.navigate(["./"+ btoa(codUnidade.codigo) + "/"+ btoa(codUnidade.loja)], {relativeTo: this.route})
		}
	}

	/**
	* ação do botão Pesquisa
	*/	  
	search() {

		this.nRegIndex = 1;
		this.getUnidades();
		
	}
	/**
	 * Incrementa o index de paginação da tela e
	 * Seta se o botão de carregar mais continua habilitado ou não
	 * @param total Total de Unidades/Clientes
	 */
	setShowMore(total: number) {
		this.nTotalTarefas = total;
		if(this.nRegIndex == 1){
			this.nRegIndex = this.nPageSize + 1;
		}else{
			this.nRegIndex += this.nPageSize;
		}

		if (this.nRegIndex <= total) {
			this.isShowMoreDisabled = false;
		}else{
			this.isShowMoreDisabled = true;
		};
	}

	/**
	 * Ação do botão Carregar mais resultados
	 * Se for clicado pela 3a vez carrega o restante dos dados, independente da quantidade
	 */
	showMore() {
		this.nNextPage ++
		//se for clicado pela 3a vez carrega o restante dos dados
		if(this.nNextPage == 3){
			this.nPageSize = this.nTotalTarefas - this.listUnidades.length;
		}
		this.isShowMoreDisabled = true;
		this.getUnidades();
	}


}
