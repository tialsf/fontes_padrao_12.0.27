//-- Traduções Aditivos - Portugues

export const gridAditivos = {
tipo:   'Tipo',
inicio: 'Início da Vigência',
fim:    'Fim da Vigência',
valor:  'Valor',
editar: 'Editar'
}

export const breadCrumb = {
	home:         'Meus processos',
	contrato:     'Meu contrato',
	homeContrato: 'Meus contratos',
	aditivos:     'Aditivos'
}

export const aditivosPt = {
	titleAditivos: 'Aditivos',
	loading:       'Carregando Aditivos',
	novoAditivo:   'Novo Aditivo',
	tipo:          'Tipo',
	inicio:        'Início da Vigência',
	fim:           'Fim da Vigência',
	valor:         'Valor',
	pesquisa:      'Pesquisar tipo de aditivo',
	breadCrumb,
	gridAditivos

}