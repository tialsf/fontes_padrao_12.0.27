import { Component, OnInit, ViewChild } from '@angular/core';
import { PoTableColumn, PoBreadcrumb, PoI18nService, PoComboComponent } from '@po-ui/ng-components';
import { RoutingService } from 'src/app/services/routing.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { FwmodelService } from 'src/app/services/fwmodel.service';
import { adaptorFilterParam, FwmodelAdaptorService } from 'src/app/services/fwmodel-adaptor.service';

class Aditivos{
	codigo:      string = ''
	id:          number = 0;
	tipo:        string = '';
	inicio:      string = '';
	fim:         string = '';
	valor:       string = '';
	btnEditAdit: string = '';
}

@Component({
  selector: 'app-aditivos',
  templateUrl: './aditivos.component.html',
  styleUrls: ['./aditivos.component.css']
})
export class AditivosComponent implements OnInit {

	litAditivos:           any;
	filial:                string = '';
	chaveContrato:         string = '';
	cajuri:                string = '';
	pk:                    string = '';
	breadcrumbPerfil:      string = '';
	breadcrumbHome:        string = '';
	linkHome:              string = '';
	cajuriTitulo:          string = '';
	chaveProcesso:         string = '';
	searchKey:             string = '';
	perfil:                string = '/contrato/';
	isShowMoreDisabled:    boolean = false;
	isHideLoadingAditivos: boolean = false;
	listAditivos:          Array<Aditivos> = [];

	/**
	 * estrutura de navegação da URL atual
	 */
	public breadcrumb: PoBreadcrumb = {
		items: [],
	};

	/**
	 *  Colunas da widget de Aditivos
	**/
	colAditivos: Array<PoTableColumn> = [
		{ property: 'tipo',        label: " ",               width: '40%' },
		{ property: 'inicio',      label: " ", width: '15%', type: 'date' },
		{ property: 'fim',         label: " ", width: '15%', type: 'date' },
		{ property: 'valor',       label: " ", width: '15%', type: 'number', format: '1.2-5' },
		{ property: 'btnEditAdit', label: " ", width: '10%', type: 'icon',
			icons: [
				{ value: 'editAditivo', icon: 'po-icon-edit', color: '#29b6c5', tooltip: ' ', action: (value)=>{
					this.callAditivo(value);
					}, 
				}
			]
		
		}
	]

	// Adapter do tipo de Aditivo
	adaptorParamTipoAditivo = new adaptorFilterParam("JURA133", "", "NXZ_DESC", "NXZ_DESC", "NXZ_DESC", "NXZ_COD", "", "Combo de tipo de aditivos");

	@ViewChild('comboPesquisa') cmbPesquisa: PoComboComponent

	constructor(
		private fwModel: FwmodelService,
		protected thfI18nService: PoI18nService,
		public  fwModelAdaptorService: FwmodelAdaptorService,
		private   routingState: RoutingService,
		private   route: ActivatedRoute,
		private   JuriGenerics: JurigenericsService,
		private   router: Router
		) {

		this.routingState.loadRouting();
		
		this.filial = this.route.snapshot.paramMap.get('filContrato');
		window.sessionStorage.setItem('filial', atob(this.filial));
	
		this.chaveProcesso = this.route.snapshot.paramMap.get('codContrato');
		this.cajuri = atob(decodeURIComponent(this.chaveProcesso));

		//-- Adiciona o código do cajuri no título Meu contrato + desc contrato
		this.cajuriTitulo = this.cajuri.replace(new RegExp('^0*', 'g'), '')
		
		//-- Traduções
		thfI18nService.getLiterals({ context: 'aditivos', language: thfI18nService.getLanguage() }).subscribe((litAditivos) => {
			this.litAditivos = litAditivos;

			this.perfil = '/contrato/';
			this.breadcrumbPerfil = this.litAditivos.breadCrumb.contrato;
			this.breadcrumbHome   = this.litAditivos.breadCrumb.homeContrato;
			this.linkHome         = '/homeContratos';
			
			this.breadcrumb.items = [
				{ label: this.breadcrumbHome  , link: this.linkHome },
				{ label: this.breadcrumbPerfil, link: this.perfil + this.filial + '/' + this.chaveProcesso },
				{ label: this.litAditivos.breadCrumb.aditivos }
			];

			this.colAditivos[0].label = this.litAditivos.gridAditivos.tipo;
			this.colAditivos[1].label = this.litAditivos.gridAditivos.inicio;
			this.colAditivos[2].label = this.litAditivos.gridAditivos.fim;
			this.colAditivos[3].label = this.litAditivos.gridAditivos.valor;
			this.colAditivos[4].icons[0].tooltip = this.litAditivos.gridAditivos.editar;


		});
	}

		
	ngOnInit() {
		this.searchKey = ""
		this.getInfoProcesso();
		this.getAditivos();
	}
	/**
	 * Busca as informações do processo
	 */
	getInfoProcesso(){
		this.fwModel.restore()
		this.fwModel.setModelo("JURA095")
		this.fwModel.setFilter("NSZ_COD='" + this.cajuri + "'")
		this.fwModel.setFilter("NSZ_FILIAL='" + atob(this.filial) + "'")
		this.fwModel.setCampoVirtual(true)
		this.fwModel.get("getInfoProcesso").subscribe(data=>{
			let descResumoBreadCrumb: string = ''
			if (data.resources != ""){
			
				descResumoBreadCrumb = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields,"NSZ_DESCON");
				descResumoBreadCrumb = this.JuriGenerics.changeUndefinedToEmpty(descResumoBreadCrumb);
		
				this.breadcrumb.items[1].label = this.breadcrumbPerfil + ' ' 
					+ descResumoBreadCrumb 
						+ " (" + this.cajuriTitulo + ")"
			}
		})
	}

	/**
	 * Faz o GET de Aditivos do resumo do contrato
	 */
	 getAditivos() {

		this.listAditivos = [];
		this.isHideLoadingAditivos = false;
		
		this.fwModel.restore();
		this.fwModel.setModelo("JURA095/" + btoa( atob(this.filial) + this.cajuri) );
		this.fwModel.setCampoVirtual(true);
		this.fwModel.setFirstLevel(false);
		this.fwModel.setHeaderParam('orderBy', ' ORDER BY NSZ_DTADIT DESC');

		this.fwModel.get("Busca aditivos").subscribe((data) => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data) != '' && data.models[0].models[1].items != undefined ){
				this.JuriGenerics.findModelItensByName(data.models[0].models, 'NXYDETAIL').forEach( aditivo => {

					let isSearch: boolean = this.searchKey == this.JuriGenerics.findValueByName(aditivo.fields, "NXY_CTIPO");

					//-- Se é pesquisa filtra pelo tipo do searchkey
					if (this.JuriGenerics.changeUndefinedToEmpty(this.searchKey) == '' || isSearch) {
						let itemAditivo = new Aditivos();
						itemAditivo.codigo = this.JuriGenerics.findValueByName(aditivo.fields, "NXY_COD");
						itemAditivo.tipo        = this.JuriGenerics.findValueByName(aditivo.fields, "NXY_DTIPO");
						itemAditivo.inicio      = this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(aditivo.fields, "NXY_DTINVI"), "yyyy-mm-dd");
						itemAditivo.fim         = this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(aditivo.fields, "NXY_DTTMVI"), "yyyy-mm-dd");
						itemAditivo.valor       = this.JuriGenerics.findValueByName(aditivo.fields, "NXY_VLADIT");
						itemAditivo.id          = aditivo.id;
						itemAditivo.btnEditAdit = 'editAditivo';
						
						this.listAditivos.push(itemAditivo);
					}
				});
			
				this.isHideLoadingAditivos = true;

				//-- Ordena os aditivos por data de inicio de vigencia em ordem decrescente
				this.listAditivos = this.JuriGenerics.orderByDate(this.listAditivos, "inicio");
			}else{
				this.isHideLoadingAditivos = true;
			}
			this.isHideLoadingAditivos = true;
		})
		
	}

	/**
	 * Abre a Det-despesa em modo de Update ou inclusão
	 * @param value 
	 */
	callAditivo(value ?: Aditivos) {
		if(value == undefined){
			this.router.navigate(["./cadastro"], {relativeTo: this.route})
		}else{
			this.router.navigate(["./" + btoa(value.codigo)], {relativeTo: this.route})
		}
	}

}
