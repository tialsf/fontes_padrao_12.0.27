
const form = {
	detalhe: 'Detalhamento aditivo',
	dtAssinatura: 'Data assinatura',
	objeto: 'Objeto',
	tipo: 'Tipo',
	numeroContrato: 'Número contrato',
	iniVigencia: 'Início vigência',
	fimVigencia: 'Fim vigência',
	dtValor: 'Data correção',
	moeda: 'Moeda',
	valor:'Valor',
	fCorrecao: 'Forma correção',
}

const botao = {
	btnCancelar : 'Cancelar',
	btnExcluir :  'Excluir',
	btnSalvar :   'Salvar',
}

const breadCrumb = {
	aditivo  :     'Aditivo',
	novoAditivo:   'Novo aditivo', 
	tituloAlterar : 'Alterar aditivo',
	cadastros:      'Cadastros',
	home:         'Meus processos',
	contrato:     'Meu contrato',
	homeContrato: 'Meus contratos',
	aditivos:     'Aditivos',
	cadastro:     'Cadastro'
}

const modal ={
	titleExclusao:  'Atenção!',
	exclusao :      'O aditivo será excluído. Deseja prosseguir?',
	btnExcluir:     'Excluir',
	btnFechar:      'Fechar'
}
const mensagem = {
	msgInclusao :  'Aditivo incluído com sucesso!',
	msgAlteracao : 'Aditivo atualizado com sucesso!',
	msgExclusao :  'Aditivo excluído com sucesso!',
	naoEncontrada: 'O Aditivo não foi encontrado!',
	exclSucess: 'Aditivo excluído com sucesso!',
	cposObrigat: 'O formulário não é valido! Favor preencher campos obrigatórios.'
}
//-- Constantes principais
export const detAditivoPt = {
	tituloNovo :    'Novo aditivo',
	tituloAlterar : 'Alterar aditivo',
	carregando:     'Carregando...',
	form: form,
	btn: botao,
	breadCrumb :     breadCrumb,
	modal:          modal,
	msg:            mensagem
}