import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetAditivosComponent } from './det-aditivos.component';

describe('DetAditivosComponent', () => {
  let component: DetAditivosComponent;
  let fixture: ComponentFixture<DetAditivosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetAditivosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetAditivosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
