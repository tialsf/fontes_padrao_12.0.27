import { Component, OnInit, ViewChild } from '@angular/core';
import { PoI18nService, PoBreadcrumb, PoModalAction, PoModalComponent, PoNotificationService } from '@po-ui/ng-components';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AditivosDados } from './det-aditivos';
import { adaptorFilterParam, FwmodelAdaptorService } from 'src/app/services/fwmodel-adaptor.service';
import { FwmodelService, FwModelBody, FwModelBodyModels, FwModelBodyItems } from 'src/app/services/fwmodel.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RoutingService } from 'src/app/services/routing.service';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { LegalprocessService } from 'src/app/services/legalprocess.service';

@Component({
	selector: 'app-det-aditivos',
	templateUrl: './det-aditivos.component.html',
	styleUrls: ['./det-aditivos.component.css']
})
export class DetAditivosComponent implements OnInit {

	litDetAditivo:         any;
	formAditivo:           FormGroup
	titleDet:              string = "";
	codAditivo:            string = "";
	pk:                    string = '';
	filial:                string = '';
	cajuri:                string = '';
	dataAditivo:           string = ''
	perfil:                string = '/contrato/';
	breadcrumbPerfil:      string = '';
	breadcrumbHome:        string = '';
	linkHome:              string = '';
	chaveProcesso:         string = '';
	cajuriTitulo:          string = '';
	descResumoBreadCrumb:  string = '';
	descTipoAditivo:       string = '';
	listItensAditivos:     Array<AditivosDados> = [];
	listAditivos:          Array<AditivosDados> = [];
	nLengthAditivo:        number = 0;
	isProcesso:            boolean = false;
	isLoadingBtnsave:      boolean = false;
	isOperacaoInclusao:    boolean = false;
	isEditaAditivo:        boolean = false;
	isLoadingAditivo:      boolean = false;
	
	public breadcrumb: PoBreadcrumb = {
		items: []
	};

	// Ações do modal de exclusão
	fechar: PoModalAction = {
		action: () => {
			this.excluiModal.close();
		}, label: ' ',
	};
	
	excluir: PoModalAction = {
		action: () => {
			this.onDeleteAditivo();
			this.excluir.loading = true;
		}, label: ' ', 
	};

	//Adaptor de tipo de aditivo
	adaptorParamTipo = new adaptorFilterParam("JURA133", "", "NXZ_DESC", "NXZ_DESC", "", "NXZ_COD", "", "Tipo de aditivo");

	// Adaptor da forma de correção
	adaptorParamFCorrecao: adaptorFilterParam = new adaptorFilterParam("JURA061", "NW7_VISIV='1'", "NW7_DESC", "NW7_DESC", "", "NW7_COD", "", "getFormaCorrecao");

	@ViewChild('excluiAditivo', { static: true  }) excluiModal: PoModalComponent;
	
	constructor(private poI18nService: PoI18nService,
		private legalprocess:          LegalprocessService,
		private formBuilder:           FormBuilder,
		public  fwModelAdaptorService: FwmodelAdaptorService,
		private fwmodel:               FwmodelService,
		public  poNotification:        PoNotificationService,
		private route:                 ActivatedRoute,
		private router:                Router,
		private   routingState:        RoutingService,
		private   JuriGenerics:        JurigenericsService) {
			
			this.routingState.loadRouting();
			this.filial = this.route.snapshot.paramMap.get('filContrato');
			window.sessionStorage.setItem('filial', atob(this.filial));
		
			this.chaveProcesso = this.route.snapshot.paramMap.get('codContrato');
			this.cajuri = atob(decodeURIComponent(this.chaveProcesso));
			this.isProcesso = false;

			//-- Adiciona o código do cajuri no título Meu processo + área
			this.cajuriTitulo = this.cajuri.replace(new RegExp('^0*', 'g'), '')

			//pega o codigo do aditivo para fazer a alteração
			if (this.JuriGenerics.changeUndefinedToEmpty(this.route.snapshot.paramMap.get("codAditivo") ) != "") {
				this.codAditivo = atob(decodeURIComponent(this.route.snapshot.paramMap.get('codAditivo')));
			}
				
			// Traduções
			poI18nService
				.getLiterals({ context: 'detAditivo', language: poI18nService.getLanguage() })
				.subscribe( (litDetAditivo) => {

					this.litDetAditivo = litDetAditivo;

					//Novo
					if (this.codAditivo == '' ) {
					this.isOperacaoInclusao = true;
						this.titleDet     = this.litDetAditivo.tituloNovo;

					//Atualização
					} else {
						this.titleDet     = this.litDetAditivo.tituloAlterar;
					}

					//-- seta as constantes para tradução do breadcrumb
					this.perfil = '/contrato/';
					this.breadcrumbPerfil = this.litDetAditivo.breadCrumb.contrato;
					this.breadcrumbHome   = this.litDetAditivo.breadCrumb.homeContrato;
					this.linkHome         = '/homeContratos';
					
					this.breadcrumb.items = [
						{ label: this.breadcrumbHome  , link: this.linkHome },
						{ label: this.breadcrumbPerfil, link: this.perfil + this.filial + '/' + this.chaveProcesso },
						{ label: this.litDetAditivo.breadCrumb.aditivos, link: this.perfil + this.filial + '/' + this.chaveProcesso + '/aditivos' },
						{ label: this.titleDet }
					];

					//-- seta as constantes para tradução do titulo dos botões da modal de Exclusao
					this.fechar.label  = this.litDetAditivo.modal.btnFechar;
					this.excluir.label = this.litDetAditivo.modal.btnExcluir;
				}
			);
	 }

	ngOnInit() {
		this.getInfoContrato();
		this.createFormAditivos(new AditivosDados)
	}

	/**
	 * Responsável por obter os dados inputados
	 * no formulário de inclusão de Aditivos
	 */
	createFormAditivos(aditivo: AditivosDados) {
		let row: number = -1;
		this.formAditivo = this.formBuilder.group({
			rowId:          [row],
			codigo:         [aditivo.codigo],
			detalhe:        [aditivo.detalhe],
			objeto:         [aditivo.objeto ],
			numeroContrato: [aditivo.numeroContrato],
			dtValor:        [aditivo.dtValor],
			fCorrecao:      [aditivo.fCorrecao],
			tipo:           [aditivo.tipo,         Validators.compose([Validators.required])],
			iniVigencia:    [aditivo.iniVigencia,  Validators.compose([Validators.required])],
			fimVigencia:    [aditivo.fimVigencia,  Validators.compose([Validators.required])],
			dtAssinatura:   [aditivo.dtAssinatura, Validators.compose([Validators.required])],
			valor:          [aditivo.valor,        Validators.compose([Validators.required])],
		
		});
	}

	/**
	 * método GET (JURA095) WS JURMODREST
	 * Busca informaçoes necessárias para incluir/alterar aditivo
	 */
	getInfoContrato(){
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA095/"+btoa(atob(this.filial)+this.cajuri));
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.setCampoVirtual(true);

		this.fwmodel.get('getDetailProcess').subscribe((data) => {
			if(	this.JuriGenerics.changeUndefinedToEmpty(data) != ''){
				this.descResumoBreadCrumb = this.JuriGenerics.findValueByName(data.models[0].fields,"NSZ_DESCON")
				this.pk = data.pk;
				this.dataAditivo = this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(data.models[0].fields, 'NSZ_DTADIT'));

				// Adiciona a descricao do contrato no BreadCrumb
				this.descResumoBreadCrumb = this.JuriGenerics.changeUndefinedToEmpty(this.descResumoBreadCrumb);
				this.breadcrumb.items[1].label = this.breadcrumbPerfil + ' ' 
					+ this.descResumoBreadCrumb + " (" + this.cajuriTitulo + ")"

					for(var nDetail = 0; nDetail < data.models[0].models.length; nDetail++){
						if(data.models[0].models[nDetail].id == "NXYDETAIL"){
							this.getListAditivos(this.JuriGenerics.findModelItensByName(data.models[0].models, 'NXYDETAIL'));

							if(data.models[0].models[nDetail].items != undefined){
								this.nLengthAditivo = data.models[0].models[nDetail].items.length
							}
						}
					}
				
				this.isLoadingAditivo = true;
			}
		})
	}

	/**
	 * Pega todos os aditivos cadastrados no contrato, incluindo esses dados em um array
	 * e se tiver em modo de alteração, pega os dados da linha para atualizar o formulário com seus dados
	 * @param data 
	 */
	getListAditivos(data: any){
		this.listItensAditivos = [];
		this.isLoadingAditivo    = false;

		if(this.JuriGenerics.changeUndefinedToEmpty(data) != ''){
			data.forEach(item => {
				let itemAditivo = new AditivosDados;
				let codAditivo  =  this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(item.fields, "NXY_COD"));

				if(codAditivo != undefined && codAditivo != ''){
					itemAditivo.codigo         = codAditivo
				}
				
				itemAditivo.rowId          =  item.id;
				itemAditivo.detalhe        =  this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(item.fields, "NXY_ADITIV"));
				itemAditivo.dtAssinatura   =  this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(item.fields, "NXY_DTASSI"));
				itemAditivo.tipo           =  this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(item.fields, "NXY_CTIPO"));
				itemAditivo.objeto         =  this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(item.fields, "NXY_OBJETO"));
				itemAditivo.numeroContrato =  this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(item.fields, "NXY_NUMCON"));
				itemAditivo.iniVigencia    =  this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(item.fields, "NXY_DTINVI"));
				itemAditivo.fimVigencia    =  this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(item.fields, "NXY_DTTMVI"));
				itemAditivo.dtValor        =  this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(item.fields, "NXY_DTVLAD"));
				itemAditivo.fCorrecao      =  this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(item.fields, "NXY_CFCORR"));
				itemAditivo.valor          =  this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(item.fields, "NXY_VLADIT"));

				this.listItensAditivos.push(itemAditivo);
				//Se for alteração e o codigo do aditivo for igual ao codigo da linha atual, carrega os dados no form
				if(!this.isOperacaoInclusao && this.codAditivo == codAditivo) {
					this.descTipoAditivo = this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(item.fields, "NXY_DTIPO"));
					this.breadcrumb.items[3].label += " " + this.descTipoAditivo // Adiciona a descricao do tipo do adivito no BreadCrumb
					this.carregaDadosForm(itemAditivo)
				}else{
					this.isLoadingAditivo = true;
				}
			})
		}else{
			this.isLoadingAditivo = true;
		}
	}

	/**
	 * Preenche o Form com os dados do registro a ser alterado
	 * @param row linha a ser alterada
	 */
	carregaDadosForm(dados: AditivosDados){
		this.isEditaAditivo       = true;
	
		this.formAditivo.patchValue({
			detalhe:         dados.detalhe,
			dtAssinatura:    this.JuriGenerics.makeDate(this.JuriGenerics.changeUndefinedToEmpty(dados.dtAssinatura), "yyyy-mm-dd"),
			objeto:          dados.objeto,
			tipo:            dados.tipo,
			numeroContrato : dados.numeroContrato,
			iniVigencia:     this.JuriGenerics.makeDate(this.JuriGenerics.changeUndefinedToEmpty(dados.iniVigencia), "yyyy-mm-dd"),
			fimVigencia:     this.JuriGenerics.makeDate(this.JuriGenerics.changeUndefinedToEmpty(dados.fimVigencia), "yyyy-mm-dd"),
			rowId:           dados.rowId,
			dtValor:         this.JuriGenerics.makeDate(this.JuriGenerics.changeUndefinedToEmpty(dados.dtValor), "yyyy-mm-dd"),
			valor:           dados.valor,
			fCorrecao:       dados.fCorrecao,
		})
		this.isLoadingAditivo = true;
	}

	/**
	 * Salva as alterações do aditivo
	 */
	salvarAditivo(){
		let aditivo: AditivosDados

		if (this.formAditivo.valid) {
			aditivo = new AditivosDados();

			aditivo.codigo         = this.formAditivo.value.codigo;
			aditivo.detalhe        = this.formAditivo.value.detalhe;
			aditivo.dtAssinatura   = this.formAditivo.value.dtAssinatura;
			aditivo.objeto         = this.formAditivo.value.objeto;
			aditivo.tipo           = this.formAditivo.value.tipo;
			aditivo.numeroContrato = this.formAditivo.value.numeroContrato;
			aditivo.iniVigencia    = this.formAditivo.value.iniVigencia;
			aditivo.fimVigencia    = this.formAditivo.value.fimVigencia;
			aditivo.dtValor        = this.formAditivo.value.dtValor;
			aditivo.fCorrecao      = this.formAditivo.value.fCorrecao;
			aditivo.valor          = this.formAditivo.value.valor;
			aditivo.rowId          = this.formAditivo.value.rowId;

			if (this.formAditivo.value.rowId >= 0){
				let indexReg = this.listItensAditivos.findIndex(x => x.rowId == this.formAditivo.value.rowId);
				this.listItensAditivos[indexReg] = aditivo;
			} else {
				aditivo.rowId = this.listItensAditivos.length+1
				this.listItensAditivos.push(aditivo)
			}
			//Efetua a inclusão ou alteração do registro
			this.submitAditivo(aditivo.rowId);
		}else{
			this.poNotification.error(this.litDetAditivo.msg.cposObrigat);
		}
	}

	/**
	 * //Efetua a inclusão ou alteração do registro
	 * @param id linha a ser incluida ou alterada
	 * @param isExclusao Indica se é operação de exclusão
	 */
	submitAditivo(id: number, isExclusao?: boolean){
		let rowId = id;
		let codNXY: string = "";
		let cBody:  string = "";

		//-- Tratamento para o campo NXY_COD
		if(this.JuriGenerics.changeUndefinedToEmpty(this.codAditivo) == '' ){
			this.legalprocess.restore();
			this.legalprocess.get("getSeqNXY","Busca o proximo sequencial disponivel").subscribe( (data) => {
				if( this.JuriGenerics.changeUndefinedToEmpty(data) != '' ){
					codNXY = data.seq;
					cBody = this.setBodyAditivo(rowId, codNXY );
					this.putAditivo(id, isExclusao, cBody,  rowId);
				}
			});

		}else {

			cBody = this.setBodyAditivo(rowId, this.codAditivo )
			this.putAditivo(id, isExclusao, cBody,  rowId);
		}
	}

	/**
	 * Realiza a inclusão / alteração / exclusão de um aditivo (NXY)
	 * @param id linha a ser incluida ou alterada
	 * @param isExclusao Indica se é operação de exclusão
	 * @param cBody Body da requisição
	 * @param rowId id do registro
	 */
	putAditivo(id: number, isExclusao: boolean = false, cBody: string,  rowId: number ){

		if(this.formAditivo.valid || isExclusao){
			this.isLoadingBtnsave = true;
			this.fwmodel.restore();
			this.fwmodel.setFirstLevel(false)
			this.fwmodel.setModelo('JURA095');

			this.fwmodel.put(this.pk, cBody, "Submit de Aditivo").subscribe((data) => {
				if (this.JuriGenerics.changeUndefinedToEmpty(data)  != '') {
					if(isExclusao){
						setTimeout(() => { 
							this.listItensAditivos.splice(id, 1);
							this.excluiModal.close();
							this.poNotification.success(this.litDetAditivo.msg.exclSucess);
							this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codAditivo))], { relativeTo: this.route });
						}, 500);
					}else{
						let detailNXY= this.JuriGenerics.findModelItensByName(data.models[0].models, 'NXYDETAIL')
						this.isLoadingBtnsave = false;
						this.codAditivo = this.JuriGenerics.findValueByName(detailNXY[rowId-1].fields, "NXY_COD");
						this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codAditivo))], { relativeTo: this.route });
						this.getAditivos()
						if(this.isOperacaoInclusao){
							this.poNotification.success(this.litDetAditivo.msg.msgInclusao);
						} else {
							this.poNotification.success(this.litDetAditivo.msg.msgAlteracao);
						}
					}
				} else {
					this.isLoadingBtnsave = false;
				}
			});
		}else{
			this.poNotification.error(this.litDetAditivo.msg.cposObrigat);
		}
	}

	/**
	 * Função que faz a montagem do body do cadastro de contrato.
	 * @param id - id do registro
	 * @param codNXY - codigo do registro NXY_COD
	 */
	setBodyAditivo(id: number, codNXY ?: string) {

		let fwModelBody: FwModelBody = new FwModelBody('JURA095', 4, this.pk);
		let gridAditivo: FwModelBodyModels;
	
		//-- NSZ
		fwModelBody.addModel("NSZMASTER", "FIELDS").addField("NSZ_COD", this.cajuri, 2)
		fwModelBody.models[0].addField("NSZ_TIPOAS", '006' ,0,"C")
		if(this.dataAditivo == ''){
			fwModelBody.models[0].addField("NSZ_DTADIT", this.formAditivo.value.dtAssinatura.replace(/-/g,''), 0, 'C') 
		}

		//-- Aditivos - NXY
		gridAditivo = fwModelBody.models[0].addModel("NXYDETAIL", "GRID", 1)

		this.listItensAditivos.forEach(items=>{
			let itemAditivo: FwModelBodyItems;

			itemAditivo = gridAditivo.addItems(0 , items.deleted)

			if(id == items.rowId){

				if(this.JuriGenerics.changeUndefinedToEmpty(codNXY) != ''){
					itemAditivo.addField("NXY_COD", codNXY, "C");
				}

				itemAditivo.addField("NXY_CAJURI", this.cajuri                                                              ,"C")
				itemAditivo.addField("NXY_ADITIV", items.detalhe.replace(/[\u0026]/g,"&amp;")                               ,"C")
				itemAditivo.addField("NXY_DTASSI", items.dtAssinatura.replace(/-/g,'')                                      ,"C")
				itemAditivo.addField("NXY_OBJETO", items.objeto.replace(/[\u0026]/g,"&amp;")                                ,"C")
				itemAditivo.addField("NXY_CTIPO" , items.tipo                                                               ,"C")
				itemAditivo.addField("NXY_NUMCON", items.numeroContrato.replace(/[\u0026]/g,"&amp;")                        ,"C")
				itemAditivo.addField("NXY_DTINVI", items.iniVigencia.replace(/-/g,'')                                       ,"C")
				itemAditivo.addField("NXY_DTTMVI", items.fimVigencia.replace(/-/g,'')                                       ,"C")
				itemAditivo.addField("NXY_DTVLAD", this.JuriGenerics.changeUndefinedToEmpty(items.dtValor).replace(/-/g,'') ,"C")
				itemAditivo.addField("NXY_CMOADI", '01'                                                                     ,"C")
				itemAditivo.addField("NXY_CFCORR", items.fCorrecao                                                          ,"C")
				itemAditivo.addField("NXY_VLADIT", items.valor                                                              ,"C")
			}
		});
		return fwModelBody.serialize();
	}

	/**
	 * Faz o GET de Aditivos do resumo do contrato
	 */
	getAditivos() {
		this.listAditivos = [];
		let qtdAditivos: number = 0;
		this.isLoadingAditivo = false;
		
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA095/" + btoa( atob(this.filial) + this.cajuri) );
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.setPageSize('3');
		this.fwmodel.setHeaderParam('orderBy', ' ORDER BY NSZ_DTADIT DESC');

		this.fwmodel.get("Busca aditivos").subscribe((data) => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data) != ''){

				if( this.JuriGenerics.findModelItensByName(data.models[0].models, 'NXYDETAIL') != undefined){
					this.JuriGenerics.findModelItensByName(data.models[0].models, 'NXYDETAIL').forEach( aditivo => {
						let itemAditivo = new AditivosDados();
						itemAditivo.codigo      = this.JuriGenerics.findValueByName(aditivo.fields, "NXY_COD");
						itemAditivo.tipo        = this.JuriGenerics.findValueByName(aditivo.fields, "NXY_DTIPO");
						itemAditivo.iniVigencia      = this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(aditivo.fields, "NXY_DTINVI"), "dd/mm/yyyy");
						itemAditivo.fimVigencia         = this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(aditivo.fields, "NXY_DTTMVI"), "dd/mm/yyyy");
						itemAditivo.valor       = this.JuriGenerics.findValueByName(aditivo.fields, "NXY_VLADIT");
					
						this.listAditivos.push(itemAditivo);
					});
					this.isLoadingAditivo = true;
					qtdAditivos = this.listAditivos.length
					
					//-- Tratamento para apresentar somente os 3 aditivos mais recentes no resumo do contrato
					if( qtdAditivos >= 3 ){
						this.listAditivos.splice(3, this.listAditivos.length)
					}
				}else{
					this.isLoadingAditivo = true;
				}
			}else{
				this.isLoadingAditivo = true;
			}
		})
	}

	/**
	 * Cancela operação
	 */
	onCancel() {
		this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codAditivo))]);
	}

	/**
	 * Ação no botão de Exclusão de Aditivo
	 */
	onDeleteAditivo(){
		
		let id: number = this.formAditivo.value.rowId

		if(this.codAditivo){
			this.listItensAditivos[id-1].deleted = 1
			this.submitAditivo(id, true);
		}
		
	}
}
