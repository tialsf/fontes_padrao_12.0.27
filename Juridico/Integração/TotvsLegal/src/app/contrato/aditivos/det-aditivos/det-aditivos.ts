export class AditivosDados {
	codigo: string         = '';
	rowId: number          = 0;
	detalhe: string        = '';
	dtAssinatura: string   = '';
	objeto: string         = '';
	tipo: string           = '';
	numeroContrato: string = '';
	iniVigencia: string    = '';
	fimVigencia: string    = '';
	dtValor: string        = '';
	moeda: string          = '';
	valor: string          = '';
	fCorrecao: string      = '';
	deleted: 0|1           = 0;
}