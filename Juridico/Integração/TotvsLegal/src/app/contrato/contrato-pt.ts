/**
* Arquivo de constantes para tradução da tela de contrato (resumo)
*/

//--  Constantes para o compartilhamento do contrato
const share = {
	shareWhatsApp: "Acesse o TOTVS Jurídico - Contrato: "
}

//--  Constantes para o breadcrumb de Contrato
const breadcrumbContrato = {	
	brHome: "Meus contratos",
	brContrato: "Meu contrato"
}

//--  Constantes para a widget de resumo do contrato
const resumoContrato = {	
	unidade:        "Unidade",
	envolvidos:     "Envolvidos",
	advogadoResp:   "Advogado responsável",
	condicaoPgto:   "Condição pagamento",
	renovacaoAuto:  "Renovação automática?",
	numeroContrato: "Número contrato",
	dataEntrada:    "Data de Entrada",
	solicitante:    "Solicitante",
	areaSolic:      "Área solicitante",
	situacao:       "Situação",
	vigente:        "Vigente",
	encerrado:      "Encerrado"
}

//--  Constantes para a widget de valores
const valores = {	
	valores:           "Valores",	
	carregando:        "Carregando valores...",
	// Colunas 
	valorContrato:     "Valor contrato (R$)",
	inicioVigencis:    "Início vigência",
	fimVigencis:       "Fim vigência",
	correcaoMonetaria: "Correção monetária",
	valorAtual:        "Valor atualizado (R$)",
}

//--  Constantes para a widget de objeto do contrato
const objeto = {	
	objetoTitle:       "Objeto do contrato",
	carregando:        "Carregando objeto do contrato..."
}

//--  Constantes para a widget de observacoes do contrato
const observacoes = {	
	observacoesTitle:  "Observações",
	carregando:        "Carregando observações..."
}

//--  Constantes para a widget de encerramento
const encerramento = {	
	encerramento:     "Encerramento",
	loadEnc:          "Carregando encerramento...",
	dataEnc:          "Data encerramento",
	motivo:           "Motivo",
	usuario:          "Usuário",
	detalhe:          "Detalhe",
	btnEnc:           "Encerrar contrato"
}

//-- Constantes para tradução da widget de Tarefas
const tarefas = {	
	colunaData:        "Data", 
	colunaTipo:        "Tipo",
	colunaStatus:      "Status",
	colunaResponsavel: "Responsável",
	btnIncluirTarefa:  "Incluir nova tarefa",
	btnVerTodasTarefa: "Ver todos",
	tituloTarefas:     "Prazos e Tarefas",
	loadTarefas:       "Carregando Prazos e Tarefas"
}

//-- Constantes para tradução da widget de Despesas
const gridDespesa = {
	tipoDespesa:       "Despesa",
	dataDespesa:       "Data",
	valorDespesa:      "Valor",
	edtDespesa:        " "
}

const despesasProc = {
	btnVerTodos:        "Ver todos",
	btnIncluirNovoDesp: "Incluir nova despesa",
	tituloDespesas:     "Despesas",
	loadDespesas:       "Carregando despesas",
	gridDespesa:        gridDespesa
}

//-- Constantes para tradução do compartilhamento do contrato
const compartilharResumo = {
	whatsapp:  "Link via WhatsApp",
	pdf:       "Baixar PDF",
	msgInfo:   "O resumo do processo será gerado em segundo plano, assim que o processo for concluído você será notificado.",
	title:     "Compartilhar resumo",
	msgSucess: "Resumo em PDF gerado com sucesso!",
	msgErro:   "Erro ao gerar PDF"
}

const aditivos = {
	tituloAditivos: 'Aditivos',
	incluirNovo: 'Incluir novo aditivo',
	carregando: 'Carregando Aditivos'
}

const gridAditivos = {
	tipo:   'Tipo',
	inicio: 'Início Vigência',
	fim:    'Fim Vigência',
	valor:  'Valor',
	editar: 'Editar'
}

//-- Constantes para tradução da widget de historico/andamentos
const historico = {
	data:        "Data",
	movimento:   "Movimento",
	fase:        "Fase",
	descricao:   "Descrição", 
	tituloHist:  "Histórico",
	verTodos:    "Ver todos",
	incluirNovo: "Incluir novo",
	carregando:  "Carregando histórico"
}
//-- Constantes principais
export const contratoPt = {	
	anexos:             "Anexos",
	WFFluig:            "Workflow FLUIG",
	carregando:         "Carregando Informações...",
	alertLinkFluig:     'Não foi possível acessar o link da solicitação no Fluig. Verifique os parâmetros de integração.',
	naoEncontrado:      "Este contrato não foi localizado na base ou pode ter sido excluído.",
	minimizar:          "Minimizar",
	maximizar:          "Maximizar",	
	alterado:           "Contrato alterado com sucesso.",
	encerrado:          "Contrato encerrado com sucesso.",
	sim:                "Sim",
	nao:                "Não",
	breadcrumbContrato: breadcrumbContrato,
	resumoContrato:     resumoContrato,
	valores:            valores,
	objeto:             objeto,
	observacoes:        observacoes,
	encerramento:       encerramento,
	tarefas:            tarefas,
	despesas:           despesasProc,
	compartilhar:       compartilharResumo,
	share:              share,
	historico:          historico,
	aditivos,
	gridAditivos
}
