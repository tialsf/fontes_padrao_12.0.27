import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContratoComponent } from './contrato.component';
import { AnexosComponent } from '../anexos/anexos.component';
import { TarefasComponent } from '../tarefas/tarefas.component';
import { DetTarefaComponent } from '../tarefas/det-tarefa/det-tarefa.component';
import { DespesasComponent } from '../despesas/despesas.component';
import { DetDespesaComponent } from '../despesas/det-despesa/det-despesa.component';
import { HistoricoComponent } from '../historico/historico.component';
import { AndamentosComponent } from '../andamentos/andamentos.component';
import { DetAndamentoComponent } from '../andamentos/det-andamento/det-andamento.component';
import { DetContratoComponent } from './det-contrato/det-contrato.component';
import { AditivosComponent } from './aditivos/aditivos.component';
import { DetAditivosComponent } from './aditivos/det-aditivos/det-aditivos.component';

const routes: Routes = [
	{
		path: '',
		component: ContratoComponent
	},
	{
		path: 'cadastro',
		component: DetContratoComponent
	},
	{
		path: ':filContrato/:codContrato',
		children: [
			{
				path: '',
				pathMatch: 'full',
				component: ContratoComponent

			},
			{
				path: 'andamento',
				children: [
					{
						path: '',
						pathMatch: 'full',
						component: AndamentosComponent
					},
					{
						path: 'cadastro',
						component: DetAndamentoComponent
					},
					{
						path: ':codAnd',
						children: [
							{
								path: '',
								pathMatch: 'full',
								component: DetAndamentoComponent,
							},
							{
								path: 'anexos',
								component: AnexosComponent
							},
							{
								path: 'tarefa',
								redirectTo: ''
							},
							{
								path: 'tarefa/cadastro',
								component: DetTarefaComponent,
							},
							{
								path: 'tarefa/:codTar',
								children: [
									{
										path: '',
										pathMatch: 'full',
										component: DetTarefaComponent
									},
									{
										path: 'anexos',
										component: AnexosComponent
									}

								]
							}	
						]
					}
				]
			},
			{
				path: 'tarefa',
				children: [
					{
						path: '',
						pathMatch: 'full',
						component: TarefasComponent
					},
					{
						path: 'cadastro',
						component: DetTarefaComponent
					},
					{
						path: ':codTar',
						children: [
							{
								path: '',
								pathMatch: 'full',
								component: DetTarefaComponent,
							},
							{
								path: 'anexos',
								component: AnexosComponent
							}
						]
					}
				]
			},
			{
				path: 'despesa',
				children: [
					{
						path: '',
						pathMatch: 'full',
						component: DespesasComponent
					},
					{
						path: 'cadastro',
						component: DetDespesaComponent
					},
					{
						path: ':codDesp',
						children: [
							{
								path: '',
								pathMatch: 'full',
								component: DetDespesaComponent,
							},
							{
								path: 'anexos',
								component: AnexosComponent
							}
						]
					}
				]
			},
			{
				path: 'anexos',
				component: AnexosComponent
			},
			{
				path: 'historico',
				component: HistoricoComponent
			},
			{
				path: 'aditivos',
				children: [
					{
						path: '',
						pathMatch: 'full',
						component: AditivosComponent
					},
					{
						path: 'cadastro',
						component: DetAditivosComponent
					},
					{
						path: ':codAditivo',
						children: [
							{
								path: '',
								pathMatch: 'full',
								component: DetAditivosComponent,
							},
							
						]
					}
				]
			}
			
		]
	}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContratoRoutingModule { }
