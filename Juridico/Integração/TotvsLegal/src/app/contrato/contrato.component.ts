import { Component, OnInit, ViewChild } from '@angular/core';
import { PoBreadcrumbItem, 
		PoDynamicViewField, 
		PoNotificationService, 
		PoTableColumn, 
		PoI18nService, 
		PoPageAction, 
		PoModalAction, 
		PoRadioGroupComponent, 
		PoModalComponent,
} from '@po-ui/ng-components';
import { TLUserInfo } from "../services/authlogin.service";
import { RoutingService } from '../services/routing.service';
import { JurigenericsService, JurBase64 } from '../services/jurigenerics.service';
import { LegalprocessService } from '../services/legalprocess.service';
import { FwmodelService, FwModelBody } from '../services/fwmodel.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Title }     from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { JurconsultasService } from '../services/jurconsultas.service';
import { TarefasService } from '../tarefas/tarefas.service';
import { DespesasService } from '../despesas/despesas.service';
import { AndamentosService } from '../andamentos/andamentos.service';
class ResumoContrato{
	unidade:        string;
	envolvidos:     string;
	advogadoResp:   string;
	condicaoPgto:   string;
	renovacaoAuto:  string;
	numeroContrato: string;
	dataEntrada:    string;  
	solicitante:    string;
	areaSolic:      string;
	situacao:       string;
}

class ValoresContrato{
	valorContrato:     number;
	inicioVigencia:    string;
	fimVigencia:       string;
	correcaoMonetaria: string;
	valorAtual:        number;
}

class Combo{
	value: string;
	label: string;
}
class Tarefa{
	codTarefa: string;
	data: string;
	tipo: string;
	status: string;
	responsavel: string;
	tarefaEdit: string;
}
class Despesa{
	codDespesa: string;
	tipoDesp: string;
	valor: number;
	data: string;
	btnEditDesp: Array<string>;
}
class Encerramento {
	dataEnc: string = '';
	motivo:  string = '';
	usuario: string = '';
	detalhe: string = '';
}
class Historico{
	codAndamento: string;
	data: string;
	ato: string;
	descricao: string;
	editaAndamento: string;
}

class Aditivos{
	codigo:      string = '';
	tipo:        string = '';
	inicio:      string = '';
	fim:         string = '';
	valor:       string = '';
	btnEditAdit: string = '';
}

@Component({
	selector: 'app-contrato',
	templateUrl: './contrato.component.html',
	styleUrls: ['./contrato.component.css']
})

export class ContratoComponent implements OnInit {
	
	litContrato:               any;
	breadcrumbProperties:      object;
	breadcrumbItem:            PoBreadcrumbItem;
	breadcrumb:                Array<PoBreadcrumbItem> ;
	breadcrumbParams:          object  = {};
	DadosContrato:             object  = {};
	filial:                    string  = '';
	chaveContrato:             string  = '';
	cajuri:                    string  = '';
	cajuriTitulo:              string  = '';
	tpContr:                   string  = '';
	codSituac:                 string  = '';
	pk:                        string  = '';
	objetoContrato:            string  = '';
	observacoes:               string  = '';
	urlWf:                     string  = '';	
	urlPastaFluig:             string  = '';
	opcaoCompartilhar:         string  = '';
	tipoAssuntoJuridico:       string  = '';
	envolvidos:                string  = '';

	valoresContrato:           Array<ValoresContrato>  = [];
	listMotivos:               Array<Combo>            = [];
	listTarefas:               Array<Tarefa>           = [];
	listDespesa:               Array<Despesa>          = [];
	listHistorico:             Array<Historico>        = [];
	listAditivos:              Array<Aditivos>         = [];

	isToggleAditivos:          boolean = true;
	toggleTarefas:             boolean = true;
	toggleDespesas:            boolean = true;
	toggleEncerra:             boolean = false;
	toggleObserv:              boolean = true;
	toggleObjeto:              boolean = true;
	togglehistorico:           boolean = true;
	mostraWidgetTarefas:       boolean = true;
	mostraWidgetDespesas:      boolean = true;
	mostraWidgetHistorico:     boolean = true;
	isMostraWidgetAditivos:    boolean = true;
	isHideLoadingContrato:     boolean = false;
	isHideLoadingTarefas:      boolean = false;
	isHideLoadingDespesas:     boolean = false;
	isHideLoadingValores:      boolean = false;
	isHideLoadingObjeto:       boolean = false;
	isHideLoadingObserv:       boolean = false;
	isHideLoadingEncerramento: boolean = false;
	isEncerrado:               boolean = false;
	isHideLoadingHistorico:    boolean = false;
	isHideLoadingAditivos:     boolean = false;
	
	formEncerramento:          FormGroup;

	// Colunas da widget de Andamentos
	colHistorico: Array<PoTableColumn> = [
		{ property: 'data', label: 'Data'},
		{ property: 'ato', label: 'Ato'},
		{ property: 'descricao', label: 'Teor'},
		{ property: 'editaAndamento', label: ' ', type: 'icon', width: "4%",
		action: (value,row)=>{
			this.callHitorico(value);
			}, 
		icons: [{ value: 'editarAnd', icon: 'po-icon po-icon-edit', color: '#29b6c5' }] 
		}
	];
	/**
	 *  Colunas da widget de Tarefas 
	 * */
	colTarefas: Array<PoTableColumn> = [
		{ property: 'data', label: 'Data' },
		{ property: 'tipo', label: 'Tipo' },
		{ property: 'status', label: 'Status' },
		{ property: 'responsavel', label: 'Responsável' },
		{ property: 'tarefaEdit', label: ' ', type: 'icon',
			action: (value)=>{
				this.callTarefa(value);},
			icons: [{ value: 'tarefa', icon: 'po-icon po-icon-edit', color: '#29b6c5' }]
		}
	];
	/**
	 *  Colunas da widget de Despesas 
	 * */
	colDespesas: Array<PoTableColumn> = [
		{ property: 'data', label: "Data" },
		{ property: 'tipoDesp', label: "Tipo" },
		{ property: 'valor', label: "Valor", type: 'number', format: '1.2-5'},
		{ property: 'btnEditDesp', label: ' ', type: 'icon',
			icons: [{ value:' ', icon: 'po-icon po-icon-edit', color: '#29b6c5' }],
			action: (value, row)=>{
				this.callDespesa(value);
			}
		}
	];

	// Colunas da widget de detalhes do processo
	ResumoContrato: Array<PoDynamicViewField> = [
		{ property: 'unidade'       , gridColumns: 3 },
		{ property: 'envolvidos'    , gridColumns: 3 },
		{ property: 'advogadoResp'  , gridColumns: 3 },
		{ property: 'condicaoPgto'  , gridColumns: 3 },
		{ property: 'renovacaoAuto' , gridColumns: 3 },
		{ property: 'numeroContrato', gridColumns: 3 },
		{ property: 'dataEntrada'   , gridColumns: 3 },
		{ property: 'solicitante'   , gridColumns: 3 },
		{ property: 'areaSolic'     , gridColumns: 3 },
		{ property: 'situacao'      , gridColumns: 3 , tag: true }
	];

	/**
	* colunas da tabela de valores
	*/
	colValores: Array<PoTableColumn>  = [
		{ property: 'valorContrato'     , type: 'number', format: '1.2-5', width: '20%'},
		{ property: 'inicioVigencia'    , type: 'string', format: '1.2-5', width: '20%'},
		{ property: 'fimVigencia'       , type: 'string', format: '1.2-5', width: '20%'},
		{ property: 'correcaoMonetaria' , type: 'string', format: '1.2-5', width: '20%'},
		{ property: 'valorAtual'        , type: 'number', format: '1.2-5', width: '20%'}
	];

	/**
	 *  Colunas da widget de Aditivos 
	 * */
	colAditivos: Array<PoTableColumn> = [
		{ property: 'tipo',        label: " " },
		{ property: 'inicio',      label: " ", type: 'date' },
		{ property: 'fim',         label: " ", type: 'date' },
		{ property: 'valor',       label: " ", type: 'number', format: '1.2-5' },
		{ property: 'btnEditAdit', label: " ", type: 'icon'  , 
			icons: [
				{ value: 'editAditivo', icon: 'po-icon-edit', color: '#29b6c5', tooltip: ' ', 
					action: (value)=>{ this.callAditivo(value); },
				}
			]
		}
	];

	/**
	 *  Ações do botão Compartilhar 
	 * */
	listCompResumo: Array<Combo> =[
		{value: '1', label: 'Link via whatsApp'},
		{value: '2', label: 'baixar PDF'}
	]

	// Botões posicionados ao lado direito do título "Meu Processo"
	pageActions: Array<PoPageAction> = [
		{ label: '', icon: 'po-icon po-icon-share', action: this.openModal.bind(this)},
		{ label: 'Histórico', action: this.openHist.bind(this)}
	];

	fechar: PoModalAction = {
		action: () => {
			this.modalCompartilhar.close();
			this.radio.value = '';
			this.opcaoCompartilhar = '';
		},
		label: 'Fechar'
	};
	
	confirmar: PoModalAction = {
		action: () => {
			this.compResumoOpc();
		},
		label: 'Confirmar'
	};

	@ViewChild('compartilhar', { static: true }) modalCompartilhar: PoModalComponent;
	@ViewChild(PoRadioGroupComponent, { static: true }) radio: PoRadioGroupComponent;

	constructor(		
		poI18nService: PoI18nService,
		private   userInfo: TLUserInfo,
		private   routerState: RoutingService,
		private   route: ActivatedRoute,
		private   jurconsultas: JurconsultasService,
		private   JuriGenerics: JurigenericsService,
		private   router: Router,
		private   titleService: Title,
		public    poNotification: PoNotificationService,
		private   fwmodel: FwmodelService,
		private   formBuilder: FormBuilder,
		private   tarefas : TarefasService,
		private   despesas: DespesasService,
		private   legalprocess: LegalprocessService,
		public    thfNotification: PoNotificationService,
		private   andamentos: AndamentosService
	) { 
		this.routerState.loadRouting();
		
		this.filial = this.route.snapshot.paramMap.get('filContrato');
		window.sessionStorage.setItem('filial', atob(this.filial));
		
		this.chaveContrato = this.route.snapshot.paramMap.get('codContrato');
		this.cajuri = atob(decodeURIComponent(this.chaveContrato));

		//-- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'processo' no arquivo JurTraducao.
		poI18nService.getLiterals({
			context: 'contrato', language: poI18nService.getLanguage()
		}).subscribe((litContrato) => {			
			this.litContrato = litContrato;

			//-- seta as constantes para tradução do titulo dos campos de detalhe do processo
			this.ResumoContrato[0].label = this.litContrato.resumoContrato.unidade;       
			this.ResumoContrato[1].label = this.litContrato.resumoContrato.envolvidos;    
			this.ResumoContrato[2].label = this.litContrato.resumoContrato.advogadoResp;  
			this.ResumoContrato[3].label = this.litContrato.resumoContrato.condicaoPgto;  
			this.ResumoContrato[4].label = this.litContrato.resumoContrato.renovacaoAuto; 
			this.ResumoContrato[5].label = this.litContrato.resumoContrato.numeroContrato;
			this.ResumoContrato[6].label = this.litContrato.resumoContrato.dataEntrada;   
			this.ResumoContrato[7].label = this.litContrato.resumoContrato.solicitante;   
			this.ResumoContrato[8].label = this.litContrato.resumoContrato.areaSolic;     
			this.ResumoContrato[9].label = this.litContrato.resumoContrato.situacao;      

			//-- seta as constantes para tradução do titulo das colunas da widget de tarefas
			this.colTarefas[0].label = this.litContrato.tarefas.colunaData;
			this.colTarefas[1].label = this.litContrato.tarefas.colunaTipo;
			this.colTarefas[2].label = this.litContrato.tarefas.colunaStatus;
			this.colTarefas[3].label = this.litContrato.tarefas.colunaResponsavel;

			//-- Adiciona o código do cajuri no título Meu processo + área
			this.cajuriTitulo = this.cajuri.replace(new RegExp('^0*', 'g'), '')

			//-- seta as constantes para tradução do titulo dos campos da widget de valores
			this.colValores[0].label = this.litContrato.valores.valorContrato;    
			this.colValores[1].label = this.litContrato.valores.inicioVigencis;   
			this.colValores[2].label = this.litContrato.valores.fimVigencis;      
			this.colValores[3].label = this.litContrato.valores.correcaoMonetaria;
			this.colValores[4].label = this.litContrato.valores.valorAtual;       

			//-- Widget despesas
			this.colDespesas[0].label = this.litContrato.despesas.gridDespesa.tipoDespesa
			this.colDespesas[1].label = this.litContrato.despesas.gridDespesa.dataDespesa
			this.colDespesas[2].label = this.litContrato.despesas.gridDespesa.valorDespesa
			this.colDespesas[3].label = this.litContrato.despesas.gridDespesa.edtDespesa

			//-- Tradução das colunas de aditivos
			this.colAditivos[0].label = this.litContrato.gridAditivos.tipo;
			this.colAditivos[1].label = this.litContrato.gridAditivos.inicio;
			this.colAditivos[2].label = this.litContrato.gridAditivos.fim;
			this.colAditivos[3].label = this.litContrato.gridAditivos.valor;
			this.colAditivos[4].icons[0].tooltip = this.litContrato.gridAditivos.editar;

			//-- seta as constantes para tradução do titulo das colunas da widget de andamentos
			this.colHistorico[0].label = this.litContrato.historico.data;
			this.colHistorico[1].label = this.litContrato.historico.movimento;
			this.colHistorico[2].label = this.litContrato.historico.descricao;
		})
	}
	ngOnInit() {
		
		// Seta as propriedades do breadcrumb 
		this.breadcrumbProperties = this.breadcrumbSetProperties()
		this.getDetailContract();
		this.createForm(new Encerramento);
		this.getInfoEncerrado();
		this.getTarefas();
		this.getDespesas();
		this.getHistorico();
		this.getChave();
		this.getAditivos();
	}

	/**
	 * propriedades do breadcrbumb
	 */
	breadcrumbSetProperties(){
		this.breadcrumbSetParams();
		/**
		 * estrutura de navegação da URL atual
		 */
		this.breadcrumb = [
			{	label: this.litContrato.breadcrumbContrato.brHome, link: '/homeContratos' },
			{	label: this.litContrato.breadcrumbContrato.brContrato, 
				link:  '/contrato/' + atob(this.filial) + '/' + atob(this.chaveContrato) 
			}
		];

		var properties =  {
			favorite: '/juri/JURLEGALPROCESS/favorite/',
			items: this.breadcrumb,
			params: this.breadcrumbParams
		}
		return properties
	}

	/**
	 * seta os parametros para montagem do breadcrumb
	 */
	breadcrumbSetParams(){
		if (this.userInfo.getCodUsuario() == ""){
			setTimeout(() => {this.breadcrumbSetParams()}, 1000);
		} else {		
			var params = {
				userId: this.userInfo.getCodUsuario()
			}
			Object.assign(this.breadcrumbParams, params )
		}
	}
	/**
	 * método GET (DetailContrato) WS REST JurConsultas
	 * detalhes do contrato
	 */
	getDetailContract(){
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA095/" + btoa( atob(this.filial) + this.cajuri) );
		this.fwmodel.setCampoVirtual(true);

		this.fwmodel.get('getDetailContract').subscribe((data) => {

			if(this.JuriGenerics.changeUndefinedToEmpty(data) != ''){
				// Informações do resumo do contrato
				const resumo = new ResumoContrato();
				const dataContract = data.models[0].fields;
				resumo.unidade        = this.JuriGenerics.findValueByName(dataContract, 'NSZ_DCLIEN');
				resumo.envolvidos     = this.JuriGenerics.findValueByName(dataContract, 'NSZ_PATIVO') +'/'+ 
				this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(dataContract, 'NSZ_PPASSI'));

				resumo.advogadoResp   = this.JuriGenerics.findValueByName(dataContract, 'NSZ_DPART1');
				resumo.condicaoPgto   = this.JuriGenerics.changeUndefinedToEmpty(
											 this.JuriGenerics.findValueByName(dataContract, 'NSZ_FPGTO')
										).substring(0,100);
				if(this.JuriGenerics.findValueByName(dataContract, 'NSZ_RENOVA') == '1'){
					resumo.renovacaoAuto = this.litContrato.sim;
				}else{
					resumo.renovacaoAuto = this.litContrato.nao;
				}	
				resumo.numeroContrato = this.JuriGenerics.findValueByName(dataContract, 'NSZ_NUMCON');
				resumo.dataEntrada    = this.JuriGenerics.makeDate(
											this.JuriGenerics.findValueByName(dataContract, 'NSZ_DTINCL')
										, "dd/mm/yyyy");
				resumo.solicitante    = this.JuriGenerics.findValueByName(dataContract, 'NSZ_SOLICI');
				resumo.areaSolic      = this.JuriGenerics.findValueByName(dataContract, 'NSZ_DAREAJ');
				this.codSituac        = this.JuriGenerics.findValueByName(dataContract, 'NSZ_SITUAC');
				// Verifica se o contrato está em andamento ou encerrado e altera a cor das tags
				if (this.codSituac == '1') {
					this.ResumoContrato[9].color = 'color-03'
					resumo.situacao = this.litContrato.resumoContrato.vigente;
				} else {
					this.ResumoContrato[9].color = 'color-07'
					resumo.situacao = this.litContrato.resumoContrato.encerrado;
					this.isEncerrado = true;
				}
				this.DadosContrato = resumo;

				// Informações da widget de valores
				const valores = new ValoresContrato();
				valores.valorContrato     = this.JuriGenerics.changeUndefinedToZero(
												this.JuriGenerics.findValueByName(dataContract, 'NSZ_VLCONT')
											);
				valores.inicioVigencia    = this.JuriGenerics.makeDate(
												this.JuriGenerics.findValueByName(dataContract, 'NSZ_DTINVI')
											, "dd/mm/yyyy");
				valores.fimVigencia       = this.JuriGenerics.makeDate(
												this.JuriGenerics.findValueByName(dataContract, 'NSZ_DTTMVI')
											, "dd/mm/yyyy");
				valores.correcaoMonetaria = this.JuriGenerics.findValueByName(dataContract, 'NSZ_DFCORR');
				valores.valorAtual        = this.JuriGenerics.changeUndefinedToZero(
												this.JuriGenerics.findValueByName(dataContract, 'NSZ_VACONT')
											);
				this.valoresContrato.push(valores);

				// Código do Workflow do Fluig
				this.urlWf = this.JuriGenerics.findValueByName(dataContract, 'NSZ_CODWF');

				// Informação da widget de Objeto do contrato
				this.objetoContrato = this.JuriGenerics.findValueByName(dataContract, 'NSZ_DETALH');

				// Informação da widget de Observações
				this.observacoes = this.JuriGenerics.findValueByName(dataContract, 'NSZ_OBSERV');

				this.tpContr = this.JuriGenerics.changeUndefinedToEmpty( this.JuriGenerics.findValueByName(dataContract, 'NSZ_DESCON') );

				// Adiciona o código do cajuri no BreadCrumb
				this.breadcrumb[1].label = 	this.litContrato.breadcrumbContrato.brContrato + 
											" " + this.tpContr + " ("+this.cajuriTitulo+")" 
				this.titleService.setTitle(resumo.envolvidos);
				
				this.tipoAssuntoJuridico = this.JuriGenerics.findValueByName(dataContract, 'NSZ_TIPOAS');
				this.envolvidos = resumo.envolvidos;
				//this.urlPastaFluig  = this.JuriGenerics.changeUndefinedToEmpty(data.fluigUrl);
			} else {
				this.poNotification.error(this.litContrato.naoEncontrado);
				this.router.navigate(['/'], { relativeTo: this.route })
			};
			this.isHideLoadingContrato = true;
			this.isHideLoadingValores = true;
			this.isHideLoadingObjeto = true;
			this.isHideLoadingObserv = true;
		});
	}	
	
	/**
	* Abre a tela de anexos
	*/
	openAttachments() {
		this.jurconsultas.restore();
		this.jurconsultas.get('anexosFluig/'+this.cajuri,'Contratos - openAttachments').subscribe((data) => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data.url) != ''){
				window.open(data.url, '_blank')
			} else {
				this.router.navigate(["./anexos"], { relativeTo: this.route })
			};
		});
	}

	/**
	 * Abre o link do workflow no fluig
	 */
	openWFFluig(){
		if(this.JuriGenerics.changeUndefinedToEmpty(this.urlWf) != ''){
			this.jurconsultas.restore();
			this.jurconsultas.setParam('codWF', this.urlWf);
			this.jurconsultas.get('urlFluig','openWFFluig').subscribe((data) => {
				if(this.JuriGenerics.changeUndefinedToEmpty(data.urlFluig) !== ''){
					window.open(data.urlFluig);
				};
			});
		}else{
			this.poNotification.error(this.litContrato.alertLinkFluig);
		}
	}

	/**
	 * Responsável por obter a lista de motivo de encerramento
	 * @param CodMotivo 
	 */
	getMotivos(CodMotivo?: string) {
		
		this.fwmodel.restore();
		this.listMotivos = [];
		this.fwmodel.setModelo("JURA013");
		this.fwmodel.get("Motivo de Encerramento").subscribe((data) => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data) !== ""){
				data.resources.forEach(item => {
					const motivoEncerramento = new Combo();
					motivoEncerramento.label = this.JuriGenerics.findValueByName(item.models[0].fields,"NQI_DESC");
					motivoEncerramento.value = this.JuriGenerics.findValueByName(item.models[0].fields,"NQI_COD");

					this.listMotivos.push(motivoEncerramento);
				});
			}

			if (CodMotivo != undefined){
				this.formEncerramento.patchValue({
					motivo: CodMotivo
				})
			}
		})
	};

	/**
	* Responsável por obter os dados inputados no formulário de inclusão
	* de garantias para serem usados após o submit
	*/
	createForm(encerramento: Encerramento) {
		if (this.userInfo.getNomeUsuario() == "" || this.codSituac == ""){
			setTimeout(() => {this.createForm(encerramento)}, 500);
		} else {
			this.formEncerramento = this.formBuilder.group({
				dataEncerramento: [null, Validators.compose([Validators.required])],
				motivo: [encerramento.motivo, Validators.compose([Validators.required])],
				usuario : [this.userInfo.getNomeUsuario(),Validators.compose([Validators.required])],
				detalhe: [encerramento.detalhe, Validators.compose([Validators.required])]
			})
		}
	}	

	/**
	 * Trás as informações de encerramento, caso esteja em andamento, sugere a data e informa o usuário logado
	 */
	getInfoEncerrado(){

		if (this.userInfo.getNomeUsuario() == "" || this.codSituac == ""){
			setTimeout(() => {this.getInfoEncerrado()}, 500);
		} else {
			
			if( this.codSituac == '2' || 
				(this.codSituac == '1' && this.formEncerramento.value.motivo != '') ){
				this.toggleEncerra = true;
				this.fwmodel.restore();
				this.fwmodel.setModelo("JURA095"+ btoa( atob(this.filial) + this.cajuri) );
				this.fwmodel.setFields(["NSZ_DTENCE", "NSZ_CMOENC", "NSZ_DETENC", "NSZ_USUENC"]);

				//seta informações de encerramento no formulário
				this.fwmodel.get('getInfoEncerrado').subscribe((data) => {
					this.formEncerramento.patchValue({
						usuario: this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NSZ_USUENC"),
					});
					this.pk = data.resources[0].pk; 
					this.getdataEnce(this.JuriGenerics.makeDate(
						this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NSZ_DTENCE")
					, "yyyy-mm-dd"));
					this.getMotivos(this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NSZ_CMOENC"));
					this.getDetalhe(this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NSZ_DETENC"));
					this.isHideLoadingEncerramento = true;
								
				});
			}else if(this.codSituac == '1'){
				this.getMotivos();
				this.isHideLoadingEncerramento = true;
			}
		}
	}

	/**
	 * Trás no formulário o detalhe em caso de processo já encerrado
	 * @param detalhe 
	 */
	getDetalhe(detalhe?: string){

		if(detalhe != undefined){
			this.formEncerramento.patchValue({
				detalhe: detalhe
			})
		}

	}

	/**
	 * Responsável por trazer a data de encerramento, 
	 * caso esteja em andamento é sugerido a data atual 
	 * @param dataEncerramento 
	 */
	getdataEnce(dataEncerramento?: string){
		this.isHideLoadingEncerramento = false;
		if(dataEncerramento != undefined){
		this.formEncerramento.patchValue({
				dataEncerramento: dataEncerramento
			})
		}
		this.isHideLoadingEncerramento = true;
	}

	/**
	 * Efetua a alteração do processo, encerrando com as informações passadas no formulário
	 */
	onClickBtnEnc(){
		let fwModelBody: FwModelBody
		let user = this.userInfo.getNomeUsuario();

		this.isHideLoadingEncerramento = false;
		
		fwModelBody = new FwModelBody('JURA095', 4, this.pk,"NSZMASTER");

		fwModelBody.cajuri = this.cajuri

		fwModelBody.models[0].addField("NSZ_SITUAC", "2"                                          , 0, "C" )
		fwModelBody.models[0].addField("NSZ_DTENCE", this.formEncerramento.value.dataEncerramento , 0, "D" )
		fwModelBody.models[0].addField("NSZ_CMOENC", this.formEncerramento.value.motivo           , 0, "C" )
		fwModelBody.models[0].addField("NSZ_DETENC", this.formEncerramento.value.detalhe          , 0, "C" )
		fwModelBody.models[0].addField("NSZ_USUENC", user                                         , 0, "C" )

		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA095");
		this.fwmodel.setFilter("NSZ_COD='" + this.cajuri + "' ");
		this.fwmodel.setFilter("NSZ_FILIAL='" + atob(this.filial) + "' ");
		this.fwmodel.setChave(this.pk);

		this.fwmodel
			.put(this.pk,fwModelBody.serialize(),"Encerramento de Contrato")
			.subscribe((data)=>{
				if(this.JuriGenerics.changeUndefinedToEmpty(data) !== ''){
					if (this.codSituac == '2'){
						this.poNotification.success(this.litContrato.encerrado);
					}
					//carrega informações 
					this.getDetailContract();
					this.getInfoEncerrado();
				}else{
					this.isHideLoadingEncerramento = true;
				}
			});

	}
	/**
	 * Faz o get de Follow-ups de resumo
	 */
	async getTarefas() {
		this.isHideLoadingTarefas = false;
		//busca as tarefas e seta com a variavel dadas mostraWidgetTarefas de acordo com o retorno da função 
		this.tarefas.getTarefasResumo(this.cajuri, this.filial).then( listatarefas =>{
			
			//tipado com any, pois há retorno diferentes [0] - array<Tarefas> [1] - boolean
			let infoTarefas: Array<any> = listatarefas;

			this.listTarefas          = infoTarefas[0];
			this.isHideLoadingTarefas = true
			this.mostraWidgetTarefas  = infoTarefas[1]
			
			})
	}

	/**
	 * Faz o get de Despesas de resumo
	 */
	async getDespesas() {
		this.isHideLoadingDespesas = false;
		//busca as despesas e seta com a variavel dadas mostraWidgetDespesas de acordo com o retorno da função 
		this.despesas.getDespesasResumo(this.filial, this.cajuri).then( listaDespesas =>{
			//tipado com any, pois há retorno diferentes [0] - array<Despesa> [1] - boolean
			let infoDespesas: Array<any> = listaDespesas;

			this.listDespesa           = infoDespesas[0];
			this.isHideLoadingDespesas = true
			this.mostraWidgetDespesas  = infoDespesas[1]
			
		})
			
	}

	/**
	  * Abre a Det-tarefa em modo de Update ou inclusão
	  * @param value 
	 */
	callTarefa(value ?: Tarefa) {
		if ( value == undefined){
			this.router.navigate(["./tarefa/cadastro"], { relativeTo: this.route })
		} else {
			this.router.navigate(["./tarefa/"+ btoa(value.codTarefa)], {relativeTo: this.route})
		}
	}

	/**
	 * Abre lista de tarefas cadastradas
	 */
	openTarefas() {
		this.router.navigate(["./tarefa"], { relativeTo: this.route })
	}

	/**
	 * Abre a Det-despesa em modo de Update ou inclusão
	 * @param value 
	 */
	callDespesa(value ?: any) {
		if(value == undefined){
			this.router.navigate(["./despesa/cadastro"], {relativeTo: this.route})
		}else{
			this.router.navigate(["./despesa/" + btoa(value.codDespesa)], {relativeTo: this.route})
		}
	}

	/**
	 * Abre lista de despesas cadastradas
	 */
	openDespesas(){
		this.router.navigate(["./despesa"], {relativeTo: this.route})
	}

	/**
	 * Abre a tela de histórico
	 */
	openHist(){
		this.router.navigate(["./historico"], {relativeTo: this.route})
	}

	/**
	 * Abre a modal com as opções para compartilhar resumo
	 */
	openModal(){
		this.radio.value       = '';
		this.opcaoCompartilhar = '';
		this.modalCompartilhar.primaryAction.disabled = false;
		this.modalCompartilhar.open();
	}

	/**
	 * Responsável por setar a opção selecionada para a tela de compartilhar resumo
	 * opc 1 = Enviar link via WhatsApp
	 * opc 2 = Baixar PDF
	 * @param event 
	 */
	setOpcao(event){
		this.opcaoCompartilhar = event
	}

	/**
	 * Responsável por verificar a opção selecionada, para fazer sua determinada função
	 */
	compResumoOpc(){
		let opc = this.opcaoCompartilhar;

		if (opc == '1'){
			this.shareWhatsApp();
		}else{
			this.modalCompartilhar.primaryAction.disabled = true;
			this.exportPDF();
		}

	}
	
	/**
	 * Função que chama a API do WhatsApp para compartilhar a URL do processo
	 */
	shareWhatsApp(){
		let localUrl = encodeURIComponent(window.location.href)
		let textoAcesso = this.litContrato.share.shareWhatsApp + this.envolvidos + " "
		window.open("https://web.whatsapp.com/send?text=" + textoAcesso + localUrl, '_blank')
	}

	/**
	 * Exporta o resumo do processo em PDF
	 */
	exportPDF(){
		this.legalprocess.restore();
		this.legalprocess.setFilter("codFil", atob(this.filial))
		this.legalprocess.setFilter('tipoAssjur', this.tipoAssuntoJuridico);
		this.legalprocess.get("exportpdf/"+ this.cajuri, "Gera relatório em PDF").subscribe(fileData => {
			if (this.JuriGenerics.changeUndefinedToEmpty(fileData.export) != '' ) {
				fileData.export.forEach(item => {
					var blob = JurBase64.toUTF8(item.filedata, 'application/octet-stream');
					var fileLink = document.createElement('a');

					fileLink.href = window.URL.createObjectURL(blob);;
					fileLink.download = item.namefile; //força o nome do download
					fileLink.target = "_blank"; //força a abertura em uma nova página
					document.body.appendChild(fileLink);
					fileLink.click(); //gatilha o evento do click
					this.thfNotification.success(this.litContrato.compartilhar.msgSucess)
					this.modalCompartilhar.close();
					this.radio.value = '';
					this.opcaoCompartilhar = '';
					this.modalCompartilhar.primaryAction.disabled = false;
				})
			}
		});
	}
	/**
	 * Abre a Det-andamento em modo de Update ou inclusão (historico)
	 * @param value 
	 */
	callHitorico(value ?: any) {
		if(value == undefined){
			this.router.navigate(["./andamento/cadastro"], { relativeTo: this.route })
		}else{
			this.router.navigate(["./andamento/" + btoa(value.codAndamento)], { relativeTo: this.route })
		}
	}

	/**
	 *  Rota para acessar todos os historico/andamentos do contrato.
	 */
	openHistorico() {
		this.router.navigate(["./andamento"], { relativeTo: this.route })
	}
	/**
	 * Faz o get de historico/andamentos de resumo
	 */
	async getHistorico() {
		this.isHideLoadingHistorico = false;
		//busca as despesas e seta com a variavel dadas mostraWidgetDespesas de acordo com o retorno da função 
		this.andamentos.getAndamentosResumo(this.filial, this.cajuri).then( listaHistorico =>{
			//tipado com any, pois há retorno diferentes [0] - array<Despesa> [1] - boolean
			let infoHistorico: Array<any> = listaHistorico;

			this.listHistorico          = infoHistorico[0];
			this.isHideLoadingHistorico = true
			this.mostraWidgetHistorico  = infoHistorico[1]
			
		})
			
	}
		
	/**
	 * metodo responsável por trazer a chave (pk) do registro
	 */
	getChave(){
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA095/" + btoa( atob(this.filial) + this.cajuri) );

		this.fwmodel.get('Busca a pk do registro - Get processo').subscribe((data) => {
			this.pk = data.pk;
		});
	}

	/**
	 * Abre lista de aditivos cadastrados
	 */
	openAditivos() {
		this.router.navigate(["./aditivos"], { relativeTo: this.route })
	}

	/**
	 * Abre o det- aditivo em modo de inclusão
	 * @param value 
	 */
	callAditivo(value ?: Aditivos) {
		if(value == undefined){
			this.router.navigate(["./aditivos/cadastro"], {relativeTo: this.route})
		}else{
			this.router.navigate(["./aditivos/" + btoa(value.codigo)], {relativeTo: this.route})
		}
	}

	/**
	 * Faz o GET de Aditivos do resumo do contrato
	 */
	getAditivos() {
		this.listAditivos = [];
		let qtdAditivos: number = 0;
		let modelNXY: Aditivos;
		this.isHideLoadingAditivos = false;
		
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA095/" + btoa( atob(this.filial) + this.cajuri) );
		this.fwmodel.setCampoVazio(true);
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.setPageSize('3');
		this.fwmodel.setHeaderParam('orderBy', ' ORDER BY NSZ_DTADIT DESC');

		this.fwmodel.get("Busca aditivos").subscribe((data) => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data) != '' && data.models[0].models[1].items != undefined ){

				if( this.JuriGenerics.findModelItensByName(data.models[0].models, 'NXYDETAIL') != undefined ){
					this.JuriGenerics.findModelItensByName(data.models[0].models, 'NXYDETAIL').forEach( aditivo => {
						let itemAditivo = new Aditivos();
						itemAditivo.codigo      = this.JuriGenerics.findValueByName(aditivo.fields, "NXY_COD");
						itemAditivo.tipo        = this.JuriGenerics.findValueByName(aditivo.fields, "NXY_DTIPO");
						itemAditivo.inicio      = this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(aditivo.fields, "NXY_DTINVI"), "yyyy-mm-dd");
						itemAditivo.fim         = this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(aditivo.fields, "NXY_DTTMVI"), "yyyy-mm-dd");
						itemAditivo.valor       = this.JuriGenerics.findValueByName(aditivo.fields, "NXY_VLADIT");
						itemAditivo.btnEditAdit = 'editAditivo'
					
						this.listAditivos.push(itemAditivo);
					});
					this.isHideLoadingAditivos = true;
	
					//-- Ordena os aditivos por data de inicio de vigencia em ordem decrescente
					this.listAditivos = this.JuriGenerics.orderByDate(this.listAditivos, "inicio");
					qtdAditivos = this.listAditivos.length
					
					//-- Tratamento para apresentar somente os 3 aditivos mais recentes no resumo do contrato
					if( qtdAditivos >= 3 ){
						this.listAditivos.splice(3, this.listAditivos.length)
					}
					
				}else{
					this.isHideLoadingAditivos = true;
				}

			}else{
				this.isHideLoadingAditivos = true;
			}
		})
	}
}
