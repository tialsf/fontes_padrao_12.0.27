import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PoModule } from '@po-ui/ng-components';
import { PoTemplatesModule } from '@po-ui/ng-templates';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "../shared/shared.module";
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { UploadInterceptor } from '../upload.interceptor';

import { MenuModule } from '../menu/menu.module'
import { ContratoRoutingModule } from './contrato-routing.module';
import { ContratoComponent } from './contrato.component';
import { AndamentosService } from '../andamentos/andamentos.service';
import { TarefasService } from '../tarefas/tarefas.service';
import { DespesasService } from '../despesas/despesas.service';
import { DetContratoComponent } from './det-contrato/det-contrato.component';
import { AditivosComponent } from './aditivos/aditivos.component';
import { DetAditivosComponent } from './aditivos/det-aditivos/det-aditivos.component';

@NgModule({
	declarations: [
		ContratoComponent,
		DetContratoComponent,
		AditivosComponent,
		DetAditivosComponent
	],
	imports: [
		CommonModule,
		ContratoRoutingModule,
		PoModule,
		PoTemplatesModule,
		FormsModule,
		ReactiveFormsModule,
		SharedModule,
		MenuModule
	],
	providers: [
		TarefasService,
		DespesasService,
		AndamentosService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: UploadInterceptor,
			multi: true
		},
	]
})
export class ContratoModule { }
