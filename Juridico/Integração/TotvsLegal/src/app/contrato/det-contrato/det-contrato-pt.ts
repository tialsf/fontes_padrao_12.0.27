const breadcrumb = {
	home: 'Meus contratos',
	preCadastro: 'Pré-cadastro'
}

const dadosComponente = {
	cliente:        'Cliente',
	fornecedor:     'Fornecedor',
	outros:         'Outros',
	tpPessoaFisica: 'Física',
	tpPessoaJuri:   'Jurídica',
	contratante:    'Contratante',
	contratado:     'Contratado'
}

const envolvido = {
	principal:        'Principal',
	tpEnvolvimento:   'Tipo do Envolvimento',
	origem:           'Origem',
	nomeEnvolvido:    'Nome do Envolvido',
	proxStepDetalhe:  'Incluir detalhes',
	sucessIncEnvol:   'Envolvido incluído com sucesso.',
	sucessAltEnvol:   'Envolvido alterado com sucesso.',
	naoLocaliEnvol:   'Não foi possível localizar o tipo de envolvido Contratante e/ou Contratado.',
	erroValidEnvol:   'Não foi possível incluir envolvido. Verifique os campos obrigatórios.',
	erroVldPrinEnvol: 'Já foi selecionado um envolvido como principal para "{0}", favor verificar.',
	erroVldNaoPrinci: 'Não foi definido o envolvido principal para {0}, favor verificar.',
	naoExisteEnvol:   'Não foi informado nenhum envolvidos neste contrato, favor verificar.'
}

const detalhes = {
	dataEntrada:      'Data de entrada',
	areaSolicitante:  'Area solicitante',
	unidade:          'Unidade',
	tipoContrato:     'Tipo de contrato',
	numeroContrato:   'Identificação do contrato',
	advogado:         'Advogado responsável',
	solicitante:      'Solicitante',
	dtIniVigencia:    'Início da vigência',
	dtFimVigencia:    'Fim da vigência',
	renovaAutoma:     'Renovação automática',
	vlrContrato:      'Valor do contrato',
	formaCorrecao:    'Forma de correção',
	formaPagamento:   'Forma de pagamento',
	objetivoLabel:    'Objeto do contrato',
	objetivoHolder:   'Detalhe o objeto do contrato',
	limiteHelp:       '(Limite de 250 caracteres)',
	observacaoLabel:  'Observações',
	observacaoHolder: 'Detalhe as observações do contrato',
	erroValidDetalhe: 'Não foi possível incluir o contrato. Verifique os campos obrigatórios de detalhes.',
	voltarStepEnvol:  'Voltar para envolvidos',
	erroVldDtVigen:   'Data inicial da vigência não pode ser maior que a data final, favor verifique.'
}

const modalOutros = {
	incluirOutros:   'Incluir envolvido',
	subTitulo1:      'Deseja incluir "',
	subTitulo2:      '" como novo envolvido?',
	nome:            'Nome',
	tpPessoa:        'Tipo pessoa',
	email:           'E-mail',
	incPartiSucesso: 'Envolvido {0} foi incluído(a) com sucesso!',
	errorValid:      'Preencha os campos de nome e tipo pessoa.'
}

export const detContratoPt ={
	title:           'Pré-cadastro de Contratos',
	cancelar:        'Cancelar',
	simboloMoeda:    'R$',
	incluiEnvolvido: 'Incluir envolvido',
	alteraEnvolvido: 'Alterar Envolvido',
	concluir:        'Concluir',
	limpar:          'Limpar',
	confirmar:       'Confirmar',
	incluir:         'Incluir',
	loadPagInic:     'Carregando...',
	sim:             'Sim',
	nao:             'Não',
	fechar:          'Fechar',
	cpfCnpj:         'CPF/CNPJ',
	tooltipTpEnv:    'Você deve informar pelo menos um contratante e um contratado!',
	tooltipPrinci:   'Necessário ter um envolvido como principal para o contratado e contratante!',
	envolvido:       envolvido,
	modalOutros:     modalOutros,
	breadcrumb:      breadcrumb,
	dadosComponente: dadosComponente,
	detalhes:        detalhes
}