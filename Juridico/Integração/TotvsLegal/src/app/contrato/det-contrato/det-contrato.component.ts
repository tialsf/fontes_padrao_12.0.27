
import { Router, ActivatedRoute } from '@angular/router';
import { 
	FwmodelService, 
	FwModelBody,
	FwModelBodyModels, 
	FwModelBodyItems } from 'src/app/services/fwmodel.service';
import { JurigenericsService, JurUTF8 } from 'src/app/services/jurigenerics.service';
import { LegalprocessService } from 'src/app/services/legalprocess.service';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
	PoTableColumn,
	PoRadioGroupOption,
	PoComboComponent,
	PoModalComponent,
	PoModalAction, 
	PoNotificationService,
	PoBreadcrumb,
	PoI18nService,
	PoI18nPipe,
	PoStepperComponent
} from '@po-ui/ng-components';
import {
	FwmodelAdaptorService,
	adaptorFilterParam,
	adaptorReturnStruct
} from 'src/app/services/fwmodel-adaptor.service';
import { protheusParam } from 'src/app/services/authlogin.service';
import { ItemEnvolvido, CamposOutros, DetalhesContrato } from './det-contrato';
import { NomeEnvolvidoAdapterService } from 'src/app/services/combos-fwmodel-adapter.service'

/**
 * Classe do Cadastro do Tipo de Envolvimento.
 */
class TipoEnvolvimento{
	nome:        string = '';
	poloAtivo:   string = '';
	poloPassivo: string = '';
}

/**
 * Classe Padrão para utilizar nos retornos de serviços.
 */
class Padrao {
	label: string = '';
	value: string = '';
}

/**
 * Classe do Grid de Envolvidos.
 */
class GridEnvolvido {
	origem:      string        = '';
	codEntidade: string|number = '';
	nomeEnv:     string        = '';
	codCliente:  string        = '';
	ljCliente:   string        = '';
	codFornece:  string        = '';
	ljFornece:   string        = '';
	tipoPessoa:  string        = '';
	cpfcnpj:     string        = '';
	principal:   string        = '';
	polo:        string        = '';
	codTpEnv:    string        = '';
	tpEnvolvido: string        = '';
	editarEnv:   string        = 'editarEnvolvido';
	excluirEnv:  string        = 'excluirEnvolvido';
	id:          number        = 0;
}

@Component({
	selector: 'app-det-contrato',
	templateUrl: './det-contrato.component.html',
	styleUrls: ['./det-contrato.component.css']
})
export class DetContratoComponent implements OnInit {
	litDetCont: any;
	formEnvolvido: FormGroup;
	formContrato: FormGroup;
	formParteCont: FormGroup;

	nomePartCont: string        = '';
	btnIncAltEnvolvido: string  = '';
	btnPrincipalOn: string      = '';
	btnPrincipalOff: string     = '';
	origem:          string     = '';

	nNextIdEnvolvido: number    = -1;

	isDisableCpfCnpj: boolean    = false;
	isLoadIncluContrato: boolean = true;
	isLoadingContrato: boolean   = true;

	listItemsEnv: Array<GridEnvolvido> = [];
	listTpEnvModel: Array<Padrao>      = [];
	params: protheusParam []           = [];

	listEntOrigem: Array<Padrao> = [
		{ label: 'Cliente'    , value: 'SA1' },
		{ label: 'Fornecedor' , value: 'SA2' },
		{ label: 'Outros'     , value: 'NZ2' }
	]

	listTpPessoa: Array<Padrao> = [
		{label:'Fisica'   , value: '1'},
		{label:'Juridica' , value: '2'}
	]

	public breadcrumb: PoBreadcrumb = {
		items: []
	};
	
	readonly listTpEnvolvidos: Array<PoRadioGroupOption> = [
		{ label: 'Contratante', value: 'Contratante' },
		{ label: 'Contratado' , value: 'Contratado' }
	]

	adaptorParamArea:       adaptorFilterParam = new adaptorFilterParam("JURA038","NRB_ATIVO='1' AND NRB_FLUIG='1'","NRB_DESC","NRB_DESC","","NRB_COD","","Area Solicitante")
	adaptorParamUnidade:    adaptorFilterParam = new adaptorFilterParam("JURA148","","A1_NOME","A1_NOME,A1_COD,A1_LOJA,A1_CGC","A1_NOME [ A1_CGC ]","A1_COD,A1_LOJA","","Unidades",undefined,undefined,undefined,{bGetAllLevels: false, bUseEmptyField: true, bUseVirtualField: true, arrSetFields: []})
	adaptorParamTpContrato: adaptorFilterParam = new adaptorFilterParam("JURA134","","NY0_DESC","NY0_DESC","","NY0_COD","","Tipo de Contrato")
	adaptorParamFormCorrec: adaptorFilterParam = new adaptorFilterParam("JURA061","NW7_VISIV='1'","NW7_DESC","NW7_DESC","","NW7_COD","","Forma de Correção")
	adaptorParamRespons:    adaptorFilterParam = new adaptorFilterParam("JURA159","RD0_TPJUR='1'","RD0_NOME","RD0_NOME","","RD0_SIGLA","","Advogado Responsável",undefined,undefined,undefined,{bGetAllLevels: true, bUseEmptyField: true, bUseVirtualField: true, arrSetFields: []})

	colEnvolvidos: Array<PoTableColumn> = [
		{ property: 'tpEnvolvido',  width: '30%' },
		{ property: 'nomeEnv', width: '30%'},
		{ property: 'principal',  width: '30%'},
		{ property: 'cpfcnpj',  width: '30%' },

		{ property: 'editarEnv', label: ' ', type: 'icon', width: '5%',
			action: (row) => {
				this.formEnvolvido.reset()
				this.alteraEnvolvido(row)
			},
		icons: [{ value: 'editarEnvolvido', icon: 'po-icon po-icon-edit', color: '#29b6c5' }] },

		{ property: 'excluirEnv', label: ' ', type: 'icon', width: '5%',
			 action: (row) => this.excluiEnvolvido(row),
		icons: [{ value: 'excluirEnvolvido', icon: 'po-icon po-icon-close', color: '#29b6c5' }] }
	];

	// fechar do modal de confirmação da Parte Contrária.
	fechar: PoModalAction = {
		action: () => {
			this.modalConfirmIncl.close();
		},
		label: 'Fechar'
	};
	
	incluir: PoModalAction = {
		action: () => {
			this.modalConfirmIncl.close();
			this.modalIncPtCont.open();
			
		},
		label: 'Sim',
	};
	cancelar: PoModalAction = {
		action: () => {
			this.modalIncPtCont.close();
			this.formParteCont.reset();
		},
		label: 'Cancelar'
	};
	
	confirmar: PoModalAction = {
		action: () => {
			this.setComboNomeOrigem(this.formEnvolvido.value.origem, '')
			this.commitOutros();
		},
		label: 'Confirmar'
	};

	@ViewChild('nomeEnvolvido') cmbEnvolvidoNome: PoComboComponent;
	@ViewChild('envolvidoOrigem') cmbTipoEnvolvido: PoComboComponent;
	@ViewChild('incPartContraria' , { static: true  }) modalIncPtCont:   PoModalComponent;
	@ViewChild('validaIncPartCon' , { static: true  }) modalConfirmIncl: PoModalComponent;
	@ViewChild('tipoEnvolvido'    , { static: true  }) radioTipoEnv:     PoRadioGroupOption;
	@ViewChild('comboUnidade'     , { static: true  }) cmbUnidade:       PoComboComponent;

	constructor(
		private router:                Router,
		private poI18nPipe:            PoI18nPipe,
		private formBuilder:           FormBuilder,
		protected poI18nService:       PoI18nService,
		private route:                 ActivatedRoute,
		private fwmodel:               FwmodelService,
		private legalprocess:          LegalprocessService,
		private JuriGenerics:          JurigenericsService,
		public  poNotification:        PoNotificationService,
		public  fwModelAdaptorService: FwmodelAdaptorService,
		public  nomeEnvAdapterService: NomeEnvolvidoAdapterService
	) {
		this.getTiposEnvolvidos();

		//-- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'contratoCadastro' no arquivo JurTraducao.
		poI18nService.getLiterals({ context: 'contratoCadastro', language: poI18nService.getLanguage() }).subscribe((litCont) => {
			this.litDetCont = litCont;

			//-- seta as constantes para tradução do titulo da BreadCrumb
			this.breadcrumb.items =[
				{ label: this.litDetCont.breadcrumb.home, link: '/homeContratos' },
				{ label: this.litDetCont.breadcrumb.preCadastro}
			];

			//-- seta as constantes para traduçãodo conteúdo do componente Tipo de Pessoa
			this.listTpPessoa[0].label = this.litDetCont.dadosComponente.tpPessoaFisica;
			this.listTpPessoa[1].label = this.litDetCont.dadosComponente.tpPessoaJuri;

			//-- seta as constantes para traduçãodo conteúdo do componente Principal
			this.btnPrincipalOn  = this.litDetCont.sim;
			this.btnPrincipalOff = this.litDetCont.nao;

			//-- seta as constantes para tradução do conteúdo do componente Origem
			this.listEntOrigem[0].label = this.litDetCont.dadosComponente.cliente;
			this.listEntOrigem[1].label = this.litDetCont.dadosComponente.fornecedor;
			this.listEntOrigem[2].label = this.litDetCont.dadosComponente.outros;

			//-- seta as constantes para tradução do conteúdo do componente Tipo de Envolvimento
			this.listTpEnvolvidos[0].label = this.litDetCont.dadosComponente.contratante;
			this.listTpEnvolvidos[1].label = this.litDetCont.dadosComponente.contratado;
			
			//-- seta as constantes para tradução do titulo das colunas do Grid de Envolvidos
			this.colEnvolvidos[0].label = this.litDetCont.envolvido.tpEnvolvimento;
			this.colEnvolvidos[1].label = this.litDetCont.envolvido.nomeEnvolvido;
			this.colEnvolvidos[2].label = this.litDetCont.envolvido.principal;
			this.colEnvolvidos[3].label = this.litDetCont.cpfCnpj;

			//-- seta as constantes para tradução do titulo dos botões da modal
			this.cancelar.label  = this.litDetCont.cancelar;
			this.confirmar.label = this.litDetCont.confirmar;
			this.incluir.label   = this.litDetCont.sim;
			this.fechar.label    = this.litDetCont.fechar;
		});
	}

	ngOnInit() {
		this.createFormEnvolv(new ItemEnvolvido);
		this.createFormPtCont(new CamposOutros);
		this.createFormDetalhe(new DetalhesContrato);
		this.btnIncAltEnvolvido = this.litDetCont.incluiEnvolvido;
	}

	/**
	 * Função responsável pela ação de voltar para home de contratos
	 */
	callHome(){
		this.router.navigate(['/homeContratos'],{relativeTo: this.route})
	}

	/**
	* Responsável por inicializar o Formulário de cadastro do Envolvido
	* @param envolvido - Informar a classe do formulário de Envolvidos.
	*/
	createFormEnvolv(envolvido: ItemEnvolvido){
		let nRow: number = -1;

		this.formEnvolvido = this.formBuilder.group({
			tipoEnvolvido: [envolvido.tipoEnvolvido , Validators.compose([Validators.required])],
			nomeEnvolvido: [envolvido.nomeEnvolvido , Validators.compose([Validators.required])],
			origem:        [envolvido.origem        , Validators.compose([Validators.required])],
			principal:     [envolvido.principal    ],
			cpfcnpj:       [envolvido.cpfcnpj      ],
			rowId:         [nRow                   ]
		})
	}

	/**
	* Responsável por inicializar o Formulário de cadastro do Detalhe de contrato
	* @param detContrato - Informar a classe do formulário de Detalhe.
	*/
	createFormDetalhe(detContrato: DetalhesContrato){
		this.formContrato = this.formBuilder.group({
			areaSolicitante: [detContrato.areaSolicitante , Validators.compose([Validators.required])],
			unidade:         [detContrato.unidade         , Validators.compose([Validators.required])],
			tipoContrato:    [detContrato.tipoContrato    , Validators.compose([Validators.required])],
			numContrato:     [detContrato.numContrato     , Validators.compose([Validators.required])],
			responsavel:     [detContrato.responsavel     , Validators.compose([Validators.required])],
			solicitante:     [detContrato.solicitante     , Validators.compose([Validators.required])],
			iniVigencia:     [detContrato.iniVigencia     , Validators.compose([Validators.required])],
			fimVigencia:     [detContrato.fimVigencia     , Validators.compose([Validators.required])],
			renovaAuto:      [detContrato.renovaAuto      , Validators.compose([Validators.required])],
			dataEntrada:     [new Date()                  , Validators.compose([Validators.required])],
			objContrato:     [detContrato.objContrato    ],
			fCorrecao:       [detContrato.fCorrecao      ],
			observacoes:     [detContrato.observacoes    ],
			vlrContrato:     [detContrato.vlrContrato    ],
			moedaVlCaus:     [detContrato.moeda          ],
			formPagamento:   [detContrato.formPagamento  ]
		})
	}

	/**
	* Responsável por inicializar o Formulário de cadastro de uma nova parte contrário
	* @param ptContraria - Informar a classe do formulário do cadastro da parte contrária.
	*/
	createFormPtCont(ptContraria: CamposOutros){
		this.formParteCont = this.formBuilder.group({
			tpPessoa:        [ptContraria.tpPessoa        , Validators.compose([Validators.required])],
			nomePtContraria: [ptContraria.nomePtContraria , Validators.compose([Validators.required])],
			cpfcnpjPtCont:   [ptContraria.cpfcnpjPtCont],
			email:           [ptContraria.email]
		})
	}

	/**
	 * Limpa o form após realizar a operação de um envolvido
	 */
	clrFormEnvolvido(){
		this.formEnvolvido.reset();
		this.btnIncAltEnvolvido = this.litDetCont.incluiEnvolvido;
		this.createFormEnvolv(new ItemEnvolvido());
	}

	/**
	 * Trás o filtro de acordo com a entidade origem selecionada
	 * (SA1 / SA2/ NZ2)
	 * @param entidade - tabela de pesquisa do nome selecionada.
	 * @param codEntidade - responsável por gatilhar o nome do envolvido.
	 */
	setComboNomeOrigem(entidade: string = "", nomeEnvolvido?: string | number ){
		if (entidade == ""){
			this.formEnvolvido.patchValue({
				nomeEnvolvido: '',
				cpfcnpj: '',
			})
		} else {
			switch (entidade) {
				case "SA1":
					this.nomeEnvAdapterService.setup(new adaptorFilterParam("JCLIDEP","","A1_NOME,A1_CGC","A1_NOME,A1_COD,A1_LOJA","A1_NOME [A1_COD/A1_LOJA]","A1_COD,A1_LOJA","A1_CODA1_LOJA","getClienteEnvolvido",undefined,undefined,undefined,{bGetAllLevels: false, bUseEmptyField: false, bUseVirtualField: false, arrSetFields: ["A1_COD","A1_LOJA","A1_CGC","A1_NOME"]},"A1_CGC"));
					break;
				case "SA2":
					this.nomeEnvAdapterService.setup(new adaptorFilterParam("JURA132","","A2_NOME,A2_CGC","A2_NOME,A2_COD,A2_LOJA","A2_NOME [A2_COD/A2_LOJA]","A2_COD,A2_LOJA","A2_CODA2_LOJA","getFornecedorEnvolvido",undefined,undefined,undefined,{bGetAllLevels:false,bUseVirtualField:true,bUseEmptyField:false,arrSetFields:[]},"A2_CGC"))
					break;
				case "NZ2":
					this.nomeEnvAdapterService.setup(new adaptorFilterParam("JURA184","","NZ2_NOME,NZ2_CGC","NZ2_NOME","","NZ2_COD","","getParteContrariaEnvolvido",undefined,undefined,undefined,{bGetAllLevels:false,bUseVirtualField:true,bUseEmptyField:false,arrSetFields:[]},"NZ2_CGC"))
					break;
			}
		}
		
		this.formEnvolvido.patchValue({
			nomeEnvolvido: nomeEnvolvido
		})
	}

	/**
	 * Seta os campos de data e moeda da causa, caso o campo de valor esteja preenchido
	 * @param vlrContrato - valor do contrato preenchido no formulário.
	 */
	setCampoMoeda(vlrContrato: string){
		if(this.JuriGenerics.changeUndefinedToEmpty(vlrContrato) != ''){
			this.legalprocess.restore();
			this.legalprocess.get("tlprocess/sysParam/MV_JCMOPRO").subscribe((data)=>{
			let param = new protheusParam;
			
			param.id = data.sysParam.name;
			param.value = data.sysParam.value;
			
			this.params.push(param);

			this.formContrato.patchValue({
		 		moedaVlCaus: this.JuriGenerics.findValueByName(this.params,'MV_JCMOPRO')
		 	});
		});
		} else {
			this.formContrato.patchValue({
				moedaVlCaus: ''
			});
		}
	}

	/**
	 * Preenche e Habilita o campo  CPF / CNPJ após selecionar a entidade
	 */
	incluiCpfCnpj(){
		var codCGC: string = "";
		if (this.cmbTipoEnvolvido.selectedOption != undefined){
			let tipoEntidade = this.cmbTipoEnvolvido.selectedOption.value

			if (this.cmbEnvolvidoNome.selectedOption != undefined){
				let fieldsExtra: adaptorReturnStruct = <adaptorReturnStruct>this.cmbEnvolvidoNome.selectedOption
				
				switch (tipoEntidade) {
					case "SA1":
						codCGC  = this.JuriGenerics.findValueByName(fieldsExtra.getExtras(),"A1_CGC")
						break;
					case "SA2":
						codCGC  = this.JuriGenerics.findValueByName(fieldsExtra.getExtras(),"A1_CGC")
						break;
					case "NZ2":
						codCGC  = this.JuriGenerics.findValueByName(fieldsExtra.getExtras(),"NZ2_CGC")
						break;
				}
			}
		}

		//-- Ao alterar um envolvido, verifica se o usuário havia digitado um cpf manualmente, senão, seta o cpf do cadastro da entidade.
		if(this.JuriGenerics.changeUndefinedToEmpty(codCGC) != ''){
				this.formEnvolvido.patchValue({
					cpfcnpj: codCGC
				});

				this.isDisableCpfCnpj = true;
		} else {
			this.formEnvolvido.patchValue({
				cpfcnpj: ''
			});
			this.isDisableCpfCnpj = false;
		}
	}

	/**
	 * Verifica se a entidade selecionada é de parte contrária, e verifica se o nome digitado está presente no retorno 
	 * do combo, se não tiver, é apresentado um modal para inclusão, verificação é acionada assim que se sai do campo
	 * com TAB ou ENTER
	 * @param event - obtem dados do evento html do componente.
	 */
	validaOutros(event: KeyboardEvent){
		if (event.keyCode == 9 || event.keyCode == 13) {
			if(this.cmbTipoEnvolvido.selectedOption != undefined){
				if(this.cmbTipoEnvolvido.selectedOption.value == "NZ2"){//verifica que a entidade selecionada é parte contrária 
					if (this.cmbEnvolvidoNome.selectedOption == undefined ||
						this.cmbEnvolvidoNome.selectedOption.label != this.cmbEnvolvidoNome.previousSearchValue){//verifica se o nome não esta presente no combo
						this.nomePartCont = this.cmbEnvolvidoNome.previousSearchValue//informação digitada no nome
						this.modalConfirmIncl.open();
						//carrega o campo nome do cadastro de parte contrária, com o valor digitado no envolvido
						this.formParteCont.patchValue({
							nomePtContraria: this.nomePartCont
						})
					}
				}
			}
		}
	}

	/**
	 * Função responsável por validar e gravar a nova parte contrário na tabela NZ2 ou rotina JURA184.
	 */
	commitOutros() {
		if(this.beforeSubOut()){

			let body: string = this.bodyPostOutros(this.formParteCont.value, "");
			this.fwmodel.restore();
			this.fwmodel.setModelo('JURA184');
			this.fwmodel.post(body,'Inclusão de Envolvidos (Outros)').subscribe((data) =>{
				if (this.JuriGenerics.changeUndefinedToEmpty(data) != ""){
					var codNZ2  = this.JuriGenerics.findValueByName(data.models[0].fields, "NZ2_COD");
					var cpfcnpj = this.JuriGenerics.findValueByName(data.models[0].fields, "NZ2_CGC");
					var nomeNZ2 = this.JuriGenerics.findValueByName(data.models[0].fields, "NZ2_NOME");
					
					this.poNotification.success(this.poI18nPipe.transform(this.litDetCont.modalOutros.incPartiSucesso, nomeNZ2));
					
					this.modalIncPtCont.close();
					this.formParteCont.reset();

					this.formEnvolvido.patchValue({
						nomeEnvolvido: codNZ2,
						cpfcnpj: cpfcnpj
					})
				}
			})
		}
	}

	/**
	 * Função que faz validação de dados para grava uma parte contrária
	 */
	beforeSubOut(){
		let bOk:boolean = this.formParteCont.valid;//valida os campos obrigatórios
		if (!bOk){
			this.poNotification.error(this.litDetCont.modalOutros.errorValid);
		}
		return bOk;
	}

	/**
	 * Função responsável pela montagem do body da parte contrária.
	 */
	bodyPostOutros(parteCont: CamposOutros, pk: string){
		
		let fwModel: FwModelBody = new FwModelBody('JURA184', 3, pk, "NZ2MASTER");

		fwModel.models[0].addField("NZ2_NOME",  parteCont.nomePtContraria, 0, "C")
		fwModel.models[0].addField("NZ2_TIPOP", parteCont.tpPessoa,        0, "C")
		fwModel.models[0].addField("NZ2_CGC",   parteCont.cpfcnpjPtCont,   0, "C")
		fwModel.models[0].addField("NZ2_EMAIL", parteCont.email,           0, "C")

		return fwModel.serialize();
	}

	/**
	 * Função responsável por incluir um envolvido no Grid de envolvido.
	 */
	incluiEnvolvido(){
		let envolvido:   GridEnvolvido = new GridEnvolvido();
		let bRet:            Boolean   = true;
		let principalForm:   string    = this.formEnvolvido.value.principal;
		let tpEnvolvidoForm: string    = this.formEnvolvido.value.tipoEnvolvido;

		if (principalForm) {
			principalForm = this.litDetCont.sim;
		} else {
			principalForm = this.litDetCont.nao;
		}

		if(this.formEnvolvido.valid){
			if (principalForm == this.litDetCont.sim) {
				bRet = this.listItemsEnv.findIndex(x => x.principal == this.litDetCont.sim && x.tpEnvolvido == tpEnvolvidoForm) == -1;
				
				if (!bRet) {
					this.poNotification.error(this.poI18nPipe.transform(this.litDetCont.envolvido.erroVldPrinEnvol, [tpEnvolvidoForm]));
				}
			}

			if (bRet) {
				let nI:            number = 0;
				let nQtdCaract:    number = 0;
				let codTpEnvModel: string = '';

				envolvido.principal   = principalForm;
				envolvido.tpEnvolvido = tpEnvolvidoForm;
				envolvido.cpfcnpj     = this.formEnvolvido.value.cpfcnpj;
				envolvido.origem      = this.formEnvolvido.value.origem;
				envolvido.nomeEnv     = this.cmbEnvolvidoNome.selectedOption.label;
				envolvido.codEntidade = this.cmbEnvolvidoNome.selectedOption.value;
				
				for (nI; nI < this.listTpEnvModel.length; nI++) {
					if (this.listTpEnvModel[nI].label == envolvido.tpEnvolvido ) {
						codTpEnvModel = this.listTpEnvModel[nI].value;
					}
				}

				envolvido.codTpEnv = codTpEnvModel;

				if (envolvido.origem.indexOf('SA1') > -1) {
					envolvido.codCliente = this.JuriGenerics.getValueByOption(this.cmbEnvolvidoNome,"A1_COD");
					envolvido.ljCliente  = this.JuriGenerics.getValueByOption(this.cmbEnvolvidoNome,"A1_LOJA");
				}

				if (envolvido.origem.indexOf('SA2') > -1) {
					envolvido.codFornece = this.JuriGenerics.getValueByOption(this.cmbEnvolvidoNome,"A2_COD");
					envolvido.ljFornece  = this.JuriGenerics.getValueByOption(this.cmbEnvolvidoNome,"A2_LOJA");
				}

				if (envolvido.tpEnvolvido == this.litDetCont.dadosComponente.contratante ) {
					envolvido.polo = '1'; //Polo Ativo
				} else {
					envolvido.polo = '2'; //Polo Passivo
				}

				//-- Verifica o tipo de pessoa - 1=Física / 2=Jurídica
				if(this.JuriGenerics.changeUndefinedToEmpty(this.formEnvolvido.value.cpfcnpj) != ''){
					nQtdCaract = this.formEnvolvido.value.cpfcnpj.trim().length;

					//-- Conta quantidade de caracteres para verificar se é CPF ou CNPJ
					if(nQtdCaract <= 11){
						envolvido.tipoPessoa = '1'
					} else {
						envolvido.tipoPessoa = '2'
					}
				}else{
					envolvido.tipoPessoa = '1'
				}

				if (this.formEnvolvido.value.rowId >= 0){
					let indexReg = this.listItemsEnv.findIndex(x => x.id == this.formEnvolvido.value.rowId)
					envolvido.id = this.formEnvolvido.value.rowId;
					this.listItemsEnv[indexReg] = envolvido
				} else {
					this.nNextIdEnvolvido++;
					envolvido.id = this.nNextIdEnvolvido;

					this.listItemsEnv.push(envolvido);
				}

				if(this.btnIncAltEnvolvido != this.litDetCont.alteraEnvolvido){
					this.poNotification.success(this.litDetCont.envolvido.sucessIncEnvol);
				} else {
					this.poNotification.success(this.litDetCont.envolvido.sucessAltEnvol)
				}
				
				//-- Limpa o Form após incluir os dados no GRID
				this.clrFormEnvolvido();
			}
		} else {
			this.poNotification.error(this.litDetCont.envolvido.erroValidEnvol);
		}
	}

	/**
	 * Função que tem objetivo de realizar a alteração de envolvido.
	 * @param row - recebe a linha do grid de envolvido que será alterado.
	 */
	alteraEnvolvido(row: GridEnvolvido){
		let envolPrincipal: boolean;
		this.setComboNomeOrigem(row.origem,row.codEntidade)
		
		if (row.principal == this.litDetCont.sim) {
			envolPrincipal = true;
		} else {
			envolPrincipal = false;
		}
	
		this.formEnvolvido.patchValue({
			principal:     envolPrincipal,
			tipoEnvolvido: row.tpEnvolvido,
			rowId:         row.id,
			origem:        row.origem,
			cpfcnpj:       row.cpfcnpj
		})
		
		this.btnIncAltEnvolvido = this.litDetCont.alteraEnvolvido;
	}

	/**
	 * Função que obtem o código do tipo de envolvimento na tabela NQA.
	 */
	getTiposEnvolvidos () {
		this.isLoadingContrato = false;
		this.fwmodel.restore();
		this.fwmodel.setPageSize("1000")
		this.fwmodel.setModelo("JURA009");
		this.fwmodel.get('Cadastros do Tipo Envolvido').subscribe((data) => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data) != '') {
				data.resources.forEach(item => {
					let tipoEnv = new Padrao();
					let litContratante: string = this.litDetCont.dadosComponente.contratante;
					let litContratado:  string = this.litDetCont.dadosComponente.contratado;
	
					let itemValue = this.JuriGenerics.findValueByName(item.models[0].fields,"NQA_COD");
					let itemLabel = this.JuriGenerics.findValueByName(item.models[0].fields,"NQA_DESC");
	
					if (itemLabel.trim() == litContratante || itemLabel.trim() == litContratado) {
						tipoEnv.label = itemLabel;
						tipoEnv.value = itemValue;
						this.listTpEnvModel.push(tipoEnv);
						this.isLoadingContrato = true;
					} else{
						this.isLoadingContrato = true;
					}
				})
			} else {
				this.isLoadingContrato = true;
			}

			let listIncTpEnvol: Array<TipoEnvolvimento> = [
				{nome: 'Contratado'  , poloAtivo: '2', poloPassivo: '1'},
				{nome: 'Contratante' , poloAtivo: '1', poloPassivo: '2'}
			]
	
			if (this.listTpEnvModel.findIndex( x=> x.label == this.litDetCont.dadosComponente.contratante) == -1) {
				this.incluiTpEnvol(listIncTpEnvol[0]);
			}
	
			if (this.listTpEnvModel.findIndex( x=> x.label == this.litDetCont.dadosComponente.contratado) == -1) {
				this.incluiTpEnvol(listIncTpEnvol[1]);
			}
		})
	}

    /**
     * Função responsável por cadastrar o tipo de envolvimento na tabela NQA,
     * caso não tenha cadastrado o tipo de envolvimento 'Contratado' e 'Contratante'.
     * @param listIncTpEnvol - Lista do tipo de envolvimento que será cadastrado.
     */
    incluiTpEnvol(listIncTpEnvol: TipoEnvolvimento) {
        let body: string = this.bodyPostTpEnv(listIncTpEnvol)
        this.isLoadingContrato = false;
        this.fwmodel.restore();
        this.fwmodel.setModelo('JURA009');
        this.fwmodel.post(body,'Inclusão de Tipo de Envolvimento').subscribe((data) =>{
            if (this.JuriGenerics.changeUndefinedToEmpty(data) != ""){
                let tipoEnv = new Padrao();

                tipoEnv.label = this.JuriGenerics.findValueByName(data.models[0].fields,"NQA_DESC");
                tipoEnv.value = this.JuriGenerics.findValueByName(data.models[0].fields,"NQA_COD");

                this.isLoadingContrato = true;
                this.listTpEnvModel.push(tipoEnv);
            } else {
                this.isLoadingContrato = true;
            }
        })
    }

    /**
     * Função que faz a montagem do body do cadastro do tipo de envolvimento.
     * @param listIncTpEnvol - Lista do tipo de envolvimento que será cadastrado.
     */
    bodyPostTpEnv(tpEnvolvimento: TipoEnvolvimento) {

        let fwModel: FwModelBody = new FwModelBody("JURA009", 3, "", "NQAMASTER")

        fwModel.models[0].addField("NQA_DESC",   tpEnvolvimento.nome, 0, "C")
        fwModel.models[0].addField("NQA_POLOAT", tpEnvolvimento.poloAtivo, 0, "C")
        fwModel.models[0].addField("NQA_POLOPA", tpEnvolvimento.poloPassivo, 0, "C")

        return fwModel.serialize();
	}
	
	/**
	 * Exclui um envolvido do grid
	 * @param row - linha selecionada no grid de envolvidos
	 */
	excluiEnvolvido(row: any) {
		for( var i = 0; i < this.listItemsEnv.length; i++){ 
			if ( this.listItemsEnv[i].id == row.id) {
				this.listItemsEnv.splice(i, 1);
			}
		}
		this.clrFormEnvolvido();
		this.poNotification.success("Envolvido excluido com sucesso!");
	}

	/**
	 * Chama o método POST para realizar a gravação do contrato.
	 */
	submitContrato(){
		this.isLoadIncluContrato = false;
		let bRet = this.formContrato.valid;

		if (bRet){
			let bRet = this.vldDtVigencia();
			if (bRet) {
				bRet = this.beforeSubmit(undefined,true);
				if (bRet){
					this.fwmodel.restore();
					this.fwmodel.setModelo("JURA095");
					this.fwmodel.post(this.setbodyContratos(),'Cadastro do Contrato').subscribe((data) =>{
						if(data != ''){
							var codContrato: string = this.JuriGenerics.findValueByName(data.models[0].fields, "NSZ_COD")
							var filContrato: string = window.localStorage.getItem('filial')
							this.listItemsEnv = [];
							
							this.openContrato(filContrato, codContrato)
							this.isLoadIncluContrato = true;
						} else {
							this.isLoadIncluContrato = true;
						}

						this.isLoadIncluContrato = true;
					})
				}
			} else {
				this.isLoadIncluContrato = true;
			}
		} else {
			this.isLoadIncluContrato = true;
			this.poNotification.error(this.litDetCont.detalhes.erroValidDetalhe)
		}
	}

	/**
	 * Função responsável por validar se a data final é menor que a data inicial da vigência.
	 */
	vldDtVigencia() {
		let dtIniVigencia: string = this.formContrato.value.iniVigencia;
		let dtFimVigencia: string = this.formContrato.value.fimVigencia;
		let bRet: boolean         = true;

		if (dtFimVigencia < dtIniVigencia) {
			this.poNotification.error(this.litDetCont.detalhes.erroVldDtVigen);
			bRet = false;
		}

		return bRet;
	}

	/**
	 * Função responsável pela abertura do resumo do contrato.
	 * @param filContrato - Filial em que o contrato foi cadastrado.
	 * @param codContrato - Código do contrato que será aberto.
	 */
	openContrato(filContrato: string, codContrato: string){
		this.router.navigate(['./contrato/' + btoa(filContrato) +
										"/" + btoa(codContrato)])
	}

	/**
	 * Função responsável por validar se existe envolvidos que não foram definidos como principais.
	 * @param stepper       - html nos envia a classe stepper para poder passar para o próximo step.
	 * @param bBeforeSubmit - Esta validação é na ação do botão de cadastro do contrato (false) ou
	 *                        botão de ir para o step de detalhe (true).
	 */
	beforeSubmit(stepper: PoStepperComponent, bBeforeSubmit: boolean) {
		let bRet: boolean = true;
		let contratado: string = this.litDetCont.dadosComponente.contratado;
		let contratante: string = this.litDetCont.dadosComponente.contratante;
		let msgErro: string = '';
				
		if (this.listItemsEnv.findIndex(x => x.principal == this.litDetCont.sim && x.tpEnvolvido == contratado) == -1) {
			if (this.listItemsEnv.findIndex(x => x.principal == this.litDetCont.nao && x.tpEnvolvido == contratado) > -1) {
				bRet     = false;
				msgErro += contratado + ', '
			}
		}

		if (this.listItemsEnv.findIndex(x => x.principal == this.litDetCont.sim && x.tpEnvolvido == contratante) == -1) {
			if (this.listItemsEnv.findIndex(x => (x.principal == this.litDetCont.nao) && x.tpEnvolvido == contratante) > -1) {
				bRet = false;
				msgErro += contratante + ', '
			}
		}

		if (msgErro.indexOf(contratado) == -1 && this.listItemsEnv.findIndex(x => x.tpEnvolvido == contratado) == -1) {
			bRet = false;
			msgErro += contratado + ', '
		}

		if (msgErro.indexOf(contratante) == -1 && this.listItemsEnv.findIndex(x => x.tpEnvolvido == contratante) == -1) {
			bRet = false;
			msgErro += contratante + ', '
		}

		if (bRet && !bBeforeSubmit) {
			stepper.next();
		} else {
			if (!bRet) {
				this.poNotification.error(this.poI18nPipe.transform(this.litDetCont.envolvido.erroVldNaoPrinci, [msgErro.substring(0,msgErro.length-2)]));
			}
		}

		if (!bRet && bBeforeSubmit) {
			this.isLoadIncluContrato = true;
		}

		return bRet
	}

	/**
	 * Função que faz a montagem do body do cadastro de contrato.
	 */
	setbodyContratos() {
		let nCount:     number = 0;
		let nRenoAuto:  string = '2';

		
		let fwModelBody: FwModelBody = new FwModelBody('JURA095', 3, '');
		let gridEnvolvidos: FwModelBodyModels;
		let itemEnvolvidos: FwModelBodyItems;
		let formContrato = this.formContrato.value;
		let envolvidos   = this.listItemsEnv;

		// Tratamento para renovação automatica
		if (formContrato.renovaAuto) {
			nRenoAuto = '1';
		}
		
		//-- NSZ
		fwModelBody.addModel("NSZMASTER","FIELDS",1)
		fwModelBody.models[0].addField("NSZ_DTENTR", formContrato.dataEntrada.replace(/-/g,'')                     , 0, 'C') //-- NSZ_DTENTR - Data de Entrada
		fwModelBody.models[0].addField("NSZ_CAREAJ", formContrato.areaSolicitante                                  , 0, 'C') //-- NSZ_CAREAJ - Codigo da área Jurídica / Area Solicitante
		fwModelBody.models[0].addField("NSZ_CCLIEN", this.JuriGenerics.getValueByOption(this.cmbUnidade,"A1_COD")  , 0, 'C') //-- NSZ_CCLIEN - Cod. Cliente
		fwModelBody.models[0].addField("NSZ_LCLIEN", this.JuriGenerics.getValueByOption(this.cmbUnidade,"A1_LOJA") , 0, 'C') //-- NSZ_LCLIEN - Loja Cliente
		fwModelBody.models[0].addField("NSZ_CODCON", formContrato.tipoContrato                                     , 0, 'C') //-- NSZ_CODCON - Código do contrato / Tipo de Contrato
		fwModelBody.models[0].addField("NSZ_NUMCON", formContrato.numContrato                                      , 0, 'C') //-- NSZ_NUMCON - Numero do Contrato
		fwModelBody.models[0].addField("NSZ_SIGLA1", formContrato.responsavel                                      , 0, 'C') //-- NSZ_SIGLA1 - Sigla do participante / Advogado Responsavel
		fwModelBody.models[0].addField("NSZ_SOLICI", formContrato.solicitante                                      , 0, 'C') //-- NSZ_SOLICI - Solicitante
		fwModelBody.models[0].addField("NSZ_DTINVI", formContrato.iniVigencia.replace(/-/g,'')                     , 0, 'C') //-- NSZ_DTINVI - Data Inicio Vigencia
		fwModelBody.models[0].addField("NSZ_DTTMVI", formContrato.fimVigencia.replace(/-/g,'')                     , 0, 'C') //-- NSZ_DTTMVI - Data Fim Vigencia
		fwModelBody.models[0].addField("NSZ_RENOVA", nRenoAuto                                                     , 0, 'C') //-- NSZ_RENOVA - Renovação Automatica
		fwModelBody.models[0].addField("NSZ_VLCONT", formContrato.vlrContrato.toString()                           , 0, 'C') //-- NSZ_VLCONT - Valor do Contrato
		fwModelBody.models[0].addField("NSZ_CFCORR", formContrato.fCorrecao                                        , 0, 'C') //-- NSZ_CFCORR - Forma de Correção
		fwModelBody.models[0].addField("NSZ_FPGTO" , JurUTF8.encode(formContrato.formPagamento)                    , 0, 'C') //-- NSZ_FPGTO  - Forma de Pgto
		fwModelBody.models[0].addField("NSZ_DETALH", formContrato.objContrato                                      , 0, 'C') //-- NSZ_DETALH - Detalhamento / Objetivo do Contrato
		fwModelBody.models[0].addField("NSZ_OBSERV", formContrato.observacoes                                      , 0, 'C') //-- NSZ_OBSERV - Observações 
		fwModelBody.models[0].addField("NSZ_TIPOAS", "006"                                                         , 0, 'C') //-- NSZ_TIPOAS - Tipo de Assunto

		//-- Envolvidos - NT9
		gridEnvolvidos = fwModelBody.models[0].addModel("NT9DETAIL", "GRID", 0)

		// Tratamento para setar os valores de Envolvidos (NT9)
		for (nCount; nCount < envolvidos.length; nCount++) {
			let nPrincipal: string = '2';
			// Tratamento para o campo de principal
			if (envolvidos[nCount].principal == this.litDetCont.sim) {
				nPrincipal = '1';
			}

			itemEnvolvidos = gridEnvolvidos.addItems(nCount+1 , 0)

			itemEnvolvidos.addField("NT9_ENTIDA", envolvidos[nCount].origem      ,"C") //-- NT9_ENTIDA - Entidade (SA1/SA2/NZ2)
			itemEnvolvidos.addField("NT9_CODENT", envolvidos[nCount].codEntidade ,"C") //-- NT9_CODENT - Código da Entidade
			itemEnvolvidos.addField("NT9_PRINCI", nPrincipal                     ,"C") //-- NT9_PRINCI - Principal (1-Sim / 2-Não)
			itemEnvolvidos.addField("NT9_TIPOEN", envolvidos[nCount].polo        ,"C") //-- NT9_TIPOEN - Tipo de Envolvimento
			itemEnvolvidos.addField("NT9_CTPENV", envolvidos[nCount].codTpEnv    ,"C") //-- NT9_CTPENV - Cod. Tipo de Envolvimento
			itemEnvolvidos.addField("NT9_TIPOP" , envolvidos[nCount].tipoPessoa  ,"C") //-- NT9_TIPOP  - Tipo de Pessoa (1-Fisica/2-Juridica)
			//- Valida CPF / CNPJ e Tipo de Pessoa
			if(envolvidos[nCount].cpfcnpj != '' && envolvidos[nCount].cpfcnpj != undefined){
				itemEnvolvidos.addField("NT9_CGC" , envolvidos[nCount].cpfcnpj  ,"C"); //-- NT9_CGC    - CPF/CNPJ
			}
			itemEnvolvidos.addField("NT9_CEMPCL", envolvidos[nCount].codCliente  ,"C") //-- NT9_CEMPCL - Cod. Cliente
			itemEnvolvidos.addField("NT9_LOJACL", envolvidos[nCount].ljCliente   ,"C") //-- NT9_LOJACL - Loja Cliente
			itemEnvolvidos.addField("NT9_CFORNE", envolvidos[nCount].codFornece  ,"C") //-- NT9_LOJACL - Loja Cliente
			itemEnvolvidos.addField("NT9_LFORNE", envolvidos[nCount].ljFornece   ,"C") //-- NT9_LOJACL - Loja Cliente

		}
		
		return fwModelBody.serialize();
	}
}
