/**
 * export classe do Formulário de Envolvidos.
 */
export class ItemEnvolvido {
	tipoEnvolvido: string  = 'Contratante';
	nomeEnvolvido: string  = '';
	origem:        boolean = false;
	principal:     string  = '';
	cpfcnpj:       string  = '';
}

/**
 * export classe do Formulário de Outros.
 */
export class CamposOutros{
    nomePtContraria: string = '';
    tpPessoa:        string = '';
    cpfcnpjPtCont:   string = '';
    email:           string = '';
}

/**
 * export classe do formulário de Detalhe de contrato.
 */
export class DetalhesContrato {
	vlrContrato:     string  = '';
	dataEntrada:     string  = '';
	areaSolicitante: string  = '';
	unidade:         string  = '';
	tipoContrato:    string  = '';
	numContrato:     string  = '';
	responsavel:     string  = '';
	solicitante:     string  = '';
	iniVigencia:     string  = '';
	fimVigencia:     string  = '';
	fCorrecao:       string  = '';
	formPagamento:   string  = '';
	objContrato:     string  = '';
	observacoes:     string  = '';
	moeda:           string  = '';
	renovaAuto:      boolean = false;
}