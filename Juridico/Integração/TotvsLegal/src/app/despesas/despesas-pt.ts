export const breadCrumb = {
	home: "Meus processos",
	processo: "Meu processo",
	despesas: "Despesas",
	contrato: 'Meu contrato',
	homeContrato: 'Meus contratos'
}

export const gridDespesas = {
	tipo: "Tipo",
	data: "Data",
	valor: "Valor"
}

export const despesasPt = {
	tituloDes: "Despesas",
	btnNovoDes: "Nova despesa",
	loadDes: "Carregando despesa",
	btnPesquisar: "Pesquisar",
	breadcrumb: breadCrumb,
	grid: gridDespesas
}
