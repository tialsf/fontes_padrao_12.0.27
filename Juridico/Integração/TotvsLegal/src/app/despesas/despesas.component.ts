import { Component, OnInit } from '@angular/core';
import { PoTableColumn, PoI18nService, PoBreadcrumb } from '@po-ui/ng-components';
import { FwmodelService } from 'src/app/services/fwmodel.service';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { RoutingService } from 'src/app/services/routing.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FwmodelAdaptorService, adaptorFilterParam } from 'src/app/services/fwmodel-adaptor.service';

class Despesa{
	codDespesa: string;
	tipoDesp: string;
	valor: number;
	data: string;
	btnEditDesp: Array<string>;
}
class DataCombo{
	value: string;
	label: string;
}

@Component({
	selector: 'app-despesas',
	templateUrl: './despesas.component.html',
	styleUrls: ['./despesas.component.css']
})
export class DespesasComponent implements OnInit {

	despesaList: Array<Despesa>    = []   ;
	litDes: any                           ;
	tipoDespesa: DataCombo;
	isHideLoadingDespesa: boolean  = true ;
	listDespesaResumo: Array<Despesa> = [];
	mostraWidgetDespesas: boolean     = true;
	isProcesso: boolean               = false;

	pageSize: number               = 300  ;
	page: number                   = 1    ;
	cajuri: string           = '';
	searchKey: string        = '';
	chaveProcesso: string          = ''   ;
	filial: string                 = ''   ;
    cajuriTitulo: string           = ''   ;
	perfil: string           = '/processo/';
	breadcrumbPerfil: string = ''
	breadcrumbHome: string   = ''
	linkHome: string         = ''
    

	colDespesa: Array<PoTableColumn> = [
		{ property: 'data', label: "Data" },
		{ property: 'tipoDesp', label: "Tipo" },
		{ property: 'valor', label: "Valor", type: 'number', format: '1.2-5'},
		{ property: 'btnEditDesp', label: ' ', type: 'icon',
			icons: [
				{ value: 'editDesp', icon: 'po-icon-edit', color: '#29b6c5', tooltip: 'Editar', action: (value)=>{
					this.callDetailDespesa(value);
					}, 
				},
				{ value: 'anexos', icon: 'po-icon po-icon-document-filled', color: '#color-07', tooltip: 'Anexos', action: (value)=>{
					this.callAnexos(value);
					},  
				}
			]
		}
	]

	adaptorParamTipoDespesa: adaptorFilterParam = new adaptorFilterParam("JURA087","","NSR_DESC","NSR_DESC","","NSR_COD","","getTipoDespesa")
	
	breadcrumb:PoBreadcrumb = {
		items: [

		]
	}

	constructor(public fwModelAdaptorService: FwmodelAdaptorService,
		private fwModel: FwmodelService,
		private JuriGenerics: JurigenericsService,
		private routingState: RoutingService,
		private router: Router,
		private route: ActivatedRoute,
		protected poI18nService: PoI18nService) 
	{ 
		this.routingState.loadRouting();

		if(this.router.url.indexOf('/contrato/')>-1){
			this.filial = this.route.snapshot.paramMap.get('filContrato');
			window.sessionStorage.setItem('filial', atob(this.filial));
		
			this.chaveProcesso = this.route.snapshot.paramMap.get('codContrato');
			this.cajuri = atob(decodeURIComponent(this.chaveProcesso));
			this.isProcesso = false;
		}else{
			this.filial = this.route.snapshot.paramMap.get('filPro');
			window.sessionStorage.setItem('filial', atob(this.filial));
		
			this.chaveProcesso = this.route.snapshot.paramMap.get('codPro');
			this.cajuri = atob(decodeURIComponent(this.chaveProcesso));
			this.isProcesso = true;
		}
        
        //-- Adiciona o código do cajuri no título Meu processo + área
        this.cajuriTitulo = this.cajuri.replace(new RegExp('^0*', 'g'), '')

		poI18nService.getLiterals({ context: 'despesas', language: poI18nService.getLanguage() }).subscribe((litDes) => {
			this.litDes = litDes;

			//Verifica se é um processo ou contrato
			if (this.isProcesso){
				this.perfil = '/processo/';
				this.breadcrumbPerfil = this.litDes.breadcrumb.processo
				this.breadcrumbHome   = this.litDes.breadcrumb.home
				this.linkHome         = '/home'
			}else{
				this.perfil = '/contrato/';
				this.breadcrumbPerfil = this.litDes.breadcrumb.contrato
				this.breadcrumbHome   = this.litDes.breadcrumb.homeContrato
				this.linkHome         = '/homeContratos'
			}
			this.breadcrumb.items = [
				{ label: this.breadcrumbHome, link: this.linkHome},
				{ label: this.breadcrumbPerfil, link: this.perfil + this.filial + '/' + this.chaveProcesso},
				{ label: this.litDes.breadcrumb.despesas }
			];

			this.colDespesa[0].label = this.litDes.grid.data
			this.colDespesa[1].label = this.litDes.grid.tipo
			this.colDespesa[2].label = this.litDes.grid.valor

		});
	}

	ngOnInit() {
		this.getDespesas();
		this.getInfoProcesso();
	}

	/**
	 * Busca as despesas
	 */
	getDespesas(){
		this.isHideLoadingDespesa = false;
		this.fwModel.restore();
		this.fwModel.setModelo("JURA099");
		this.fwModel.setFilter("NT3_CAJURI ='" + this.cajuri + "'");
		this.fwModel.setFilter("NT3_FILIAL ='" + atob(this.filial) + "'");
		this.fwModel.setCampoVirtual(true);
		this.fwModel.setPage(this.page.toString());
		this.fwModel.setPageSize(this.pageSize.toString());
		
		if (this.searchKey != "" && this.searchKey != undefined){
			this.fwModel.setSearchKey("NT3_CTPDES='" + this.searchKey + "'")
		}
		this.fwModel.get("getListDespesa").subscribe(data =>{
			this.despesaList = [];
			if (data.resources != ""){
				data.resources.forEach( item =>{
					let itemDespesa = new Despesa();
					itemDespesa.codDespesa = this.JuriGenerics.findValueByName(item.models[0].fields, "NT3_COD")
					itemDespesa.tipoDesp = this.JuriGenerics.findValueByName(item.models[0].fields, "NT3_DTPDES")
					itemDespesa.data  = this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(item.models[0].fields, "NT3_DATA"), "dd/mm/yyyy")
                    itemDespesa.valor = this.JuriGenerics.findValueByName(item.models[0].fields, "NT3_VALOR")
                    itemDespesa.btnEditDesp = ['editDesp']

                    if (this.JuriGenerics.findValueByName(item.models[0].fields, 'NT3__TEMANX') == '01') {
						itemDespesa.btnEditDesp = ['editDesp','anexos'];
					}

					this.despesaList.push(itemDespesa)
				})
			}
			this.isHideLoadingDespesa = true;
		})
	}

	/**
	 * Busca as informações do processo
	 */
	getInfoProcesso(){
		this.fwModel.restore()
		this.fwModel.setModelo("JURA095")
		this.fwModel.setFilter("NSZ_COD='" + this.cajuri + "'")
		this.fwModel.setFilter("NSZ_FILIAL='" + atob(this.filial) + "'")
		this.fwModel.setCampoVirtual(true)
		this.fwModel.get("getInfoProcesso").subscribe(data=>{
			let descResumoBreadCrumb = []
			if (this.isProcesso){
				descResumoBreadCrumb = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields,"NSZ_DAREAJ")
			}else{
				descResumoBreadCrumb = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields,"NSZ_DESCON")
			}
			if (data.resources != ""){
				this.breadcrumb.items[1].label = this.breadcrumbPerfil + ' ' 
					+ descResumoBreadCrumb 
						+ " (" + this.cajuriTitulo + ")"
			}
		})
	}
	/**
	 * Redirecionamento de rota
	 * @param value 
	 */
	callDetailDespesa(value ?: Despesa){
		if ( value == undefined){
			this.router.navigate(["./cadastro"], {relativeTo: this.route})
		} else {
			this.router.navigate(["./" + btoa(value.codDespesa)], {relativeTo: this.route})
		}
	}

	/**
	 * Abre a tela de anexos
	*/
	callAnexos(value?: any){
		this.router.navigate(['./' + btoa(value.codDespesa) + '/anexos'], { relativeTo: this.route });
	}

}
