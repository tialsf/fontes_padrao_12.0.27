import { Injectable } from '@angular/core';
import { FwmodelService } from 'src/app/services/fwmodel.service';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';

class Despesa{
	codDespesa: string;
	tipoDesp: string;
	valor: number;
	data: string;
	btnEditDesp: Array<string>;
}

@Injectable({
	providedIn: 'root'
})
export class DespesasService {

	isHideLoadingDespesa: boolean        = true;
	mostraWidgetDespesas: boolean        = true;
	listDespesaResumo:    Array<Despesa> = [];

	constructor(		
		private fwModel: FwmodelService,
		private JuriGenerics: JurigenericsService,
	) { }
	
	/**
	 * get para widgets componentizadas de despesas
	 * @param filial 
	 * @param cajuri 
	 */
	async getDespesasResumo( filial, cajuri ){
		this.isHideLoadingDespesa = false;
		this.listDespesaResumo = []
		this.fwModel.restore();
		this.fwModel.setModelo("JURA099");
		this.fwModel.setPageSize('3');
		this.fwModel.setFilter("NT3_FILIAL='" + atob(filial) + "' ");
		this.fwModel.setFilter("NT3_CAJURI='" + cajuri + "' ");
		this.fwModel.setCampoVirtual(true);
		this.fwModel.get("Lista de Despesa").subscribe(data=>{
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != ''){
				data.resources.forEach(item => {
					let itemDespesa = new Despesa();
					itemDespesa.codDespesa  = this.JuriGenerics.findValueByName(item.models[0].fields, "NT3_COD");
					itemDespesa.tipoDesp    = this.JuriGenerics.findValueByName(item.models[0].fields, "NT3_DTPDES");
					itemDespesa.data        = this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(item.models[0].fields, "NT3_DATA"), "dd/mm/yyyy");
					itemDespesa.valor       = this.JuriGenerics.findValueByName(item.models[0].fields, "NT3_VALOR");
					itemDespesa.btnEditDesp = [" "];

					this.listDespesaResumo.push(itemDespesa);
				})
			}
			this.isHideLoadingDespesa = true;
		}, err => {
			if(err.message == 403) {
				this.mostraWidgetDespesas = false
			}
		})
		return [this.listDespesaResumo, this.mostraWidgetDespesas]
	}
}
