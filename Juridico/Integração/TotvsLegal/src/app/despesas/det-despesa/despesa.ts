export class Despesa{
	codDespesa: string;
	tipo: string; //Combo
	data: string; //DatePicker
	moeda: string; //Combo
	valor: string; //Input
	descricao: string;//TextArea
	intFinPrefixo: string;//Input
	intFinDataFinanc: string;//DatePicker
	intFinNatureza: string;//Combo
	intFinTitulo: string;//Combo
	intFinFornecedor: string;//Combo
	intFinCondPagto: string;//Combo
	intFinProduto: string;//Combo
	intFinFiliDest: string;//Combo
	intFinRateio: string;//Combo
}