import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetDespesaComponent } from './det-despesa.component';

describe('DetDespesaComponent', () => {
  let component: DetDespesaComponent;
  let fixture: ComponentFixture<DetDespesaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetDespesaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetDespesaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
