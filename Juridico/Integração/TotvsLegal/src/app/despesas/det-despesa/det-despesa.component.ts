import { Component, OnInit, ViewChild } from '@angular/core';
import { FwmodelService } from 'src/app/services/fwmodel.service';
import {
	FwmodelAdaptorService,
	adaptorFilterParam,
	FwModelParam,
	adaptorReturnStruct,
	FwGetFilDesAdapterService
} from 'src/app/services/fwmodel-adaptor.service';
import { JurigenericsService, JurUTF8 } from 'src/app/services/jurigenerics.service';
import { RoutingService } from 'src/app/services/routing.service';
import { Router, ActivatedRoute } from '@angular/router';
import {
	PoI18nService,
	PoNotificationService,
	PoBreadcrumb,
	PoModalComponent,
	PoModalAction,
	PoUploadComponent,
	PoComboComponent
} from '@po-ui/ng-components';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Despesa } from './despesa';
import { protheusParam } from 'src/app/services/authlogin.service';
import { LegalprocessService, SysProtheusParam, sysParamStruct } from 'src/app/services/legalprocess.service';
import { JurJustifModalComponent } from 'src/app/shared/jur-justif-modal/jur-justif-modal.component';
import { TpDespesaAdapterService } from 'src/app/services/combos-fwmodel-adapter.service'
import { MoedaAdapterService } from 'src/app/services/combos-fwmodel-adapter.service'
import { NaturezaAdapterService } from 'src/app/services/combos-fwmodel-adapter.service'
import { FornecedorAdapterService } from 'src/app/services/combos-fwmodel-adapter.service'
import { CondPagAdapterService } from 'src/app/services/combos-fwmodel-adapter.service'
import { ProdutoAdapterService } from 'src/app/services/combos-fwmodel-adapter.service'
import { RateioAdapterService } from 'src/app/services/combos-fwmodel-adapter.service'

class DataCombo {
	value: string;
	label: string;
}

@Component({
	selector: 'app-det-despesa',
	templateUrl: './det-despesa.component.html',
	styleUrls: ['./det-despesa.component.css']
})
export class DetDespesaComponent implements OnInit {
	litDetDes: any;
	formDespesa: FormGroup;

	cajuri: string                  = "";
	pkDesp: string                  = "";
	codDesp: string                 = "";
	titleDet: string                = "";
	descDesp: string                = "";
	chaveProcesso: string           = "";
	filial: string                  = "";
	btnIncluirLabel: string         = "";
	empresaLogada: string           = window.localStorage.getItem('codEmp');
    cajuriTitulo: string            = "";
	perfil: string                  = '/processo/';
	breadcrumbPerfil: string        = ''
	breadcrumbHome: string          = ''
	linkHome: string                = ''

	uploadSuccessCount: number = 0;
	sysParams: Array<sysParamStruct> = [];
	arrOpcIntegraFinanceiro: Array<DataCombo> = [];
	lsIntFinTitulo: Array<DataCombo> = [];

	btnExcluirDisabled: boolean = true;
	btnVerAnexoEnabled: boolean = false;
	bIntegraFinanceiro: boolean = false;
	bValidContabil: boolean = false;
	bLoadingIncGar: boolean = false;
	isHideLoadingDespesa: boolean = false;
	bDisableIntegraCTB: boolean = false;
	isProcEncerrado: boolean = false;
	isProcesso: boolean           = false;

	public breadcrumb: PoBreadcrumb = {
		items: [
		],
	};

	// ações do modal de confirmação de exclusão (Garantia)
	fechar: PoModalAction = {
		action: () => {
			this.modalExcluir.close();
		},
		label: 'Fechar',
	};

	excluir: PoModalAction = {
		action: () => {
			this.excluir.loading = true;
			this.deleteVerify();
		},
		label: 'Excluir',
	};

	@ViewChild('excluiDespesa') modalExcluir: PoModalComponent;
	@ViewChild('docUpload') docUpload: PoUploadComponent;
	@ViewChild('tipoDespesa') listTipoDespesa: PoComboComponent;
	@ViewChild('modJustif', { static: true }) modalJustif: JurJustifModalComponent;
	@ViewChild('cmbFilialDest', { static: true }) cmbFilDestino: PoComboComponent;
	@ViewChild('cmbProduto') cmbProdDesp: PoComboComponent;
	@ViewChild('cmbNatureza') cmbNatDesp: PoComboComponent;
	@ViewChild('cmbTitulo') cmbTituloDesp: PoComboComponent;
	@ViewChild('cmbCondPagamento') cmbCondPagtoDesp: PoComboComponent;
	@ViewChild('cmbRateio') cmbRatDesp: PoComboComponent;
	@ViewChild('cmbFornecedor') cmbFornDesp: PoComboComponent;

	constructor(public fwModelAdaptorService: FwmodelAdaptorService,
				protected poI18nService: PoI18nService,
				private fwModel: FwmodelService,
				private poNotification: PoNotificationService,
				private legalprocess: LegalprocessService,
				private JuriGenerics: JurigenericsService,
				private routingState: RoutingService,
				private router: Router,
				private route: ActivatedRoute,
				private formBuilder: FormBuilder,
				private sysProtheusParam: SysProtheusParam,
				public  filiaisAdapterService: FwGetFilDesAdapterService,
				public  tpDespesaAdapterService: TpDespesaAdapterService,
				public  moedaAdapterService:      MoedaAdapterService,
				public  naturezaAdapterService:   NaturezaAdapterService,
				public  fornecedorAdapterService: FornecedorAdapterService,
				public  condPagAdapterService:    CondPagAdapterService,
				public  produtoAdapterService:    ProdutoAdapterService,
				public  rateioAdapterService:     RateioAdapterService
	) {

		// Setups adapters
		this.tpDespesaAdapterService.setup(new adaptorFilterParam("JURA087", "", "NSR_DESC", "NSR_DESC", "", "NSR_COD", "", "getTipoDespesa",undefined, undefined, undefined, new FwModelParam(false, true), "NSR_INTCTB"))
		this.moedaAdapterService.setup(new adaptorFilterParam("JMOEDA", "", "CTO_DESC", "CTO_DESC", "", "CTO_MOEDA", "", "getMoeda", undefined, undefined, { useTenantId: true, empresa: window.localStorage.getItem("codEmp"), filial: window.localStorage.getItem("filLog"), campoFiltro: "CTO_FILIAL", tenantOnHeader: false }))
		this.naturezaAdapterService.setup(new adaptorFilterParam("JNATFIN", "", "ED_DESCRIC", "ED_DESCRIC", "", "ED_CODIGO", "", "GetNatureza", undefined, undefined, { useTenantId: true, empresa: window.localStorage.getItem("codEmp"), filial: window.localStorage.getItem('filLog'), campoFiltro: "", tenantOnHeader: true }))
		this.fornecedorAdapterService.setup(new adaptorFilterParam("JURA132", "", "A2_NOME", "A2_COD,A2_LOJA,A2_NOME", "A2_NOME - [A2_COD/A2_LOJA]","A2_COD,A2_LOJA", "", "GetFornecedor", undefined, undefined, { useTenantId: true, empresa: window.localStorage.getItem("codEmp"), filial: window.localStorage.getItem('filLog'), campoFiltro: "", tenantOnHeader: true }))
		this.condPagAdapterService.setup(new adaptorFilterParam("JCONPGFIN", "", "E4_DESCRI", "E4_DESCRI", "", "E4_CODIGO", "", "getCondicaoPagto", undefined, undefined, { useTenantId: true, empresa: window.localStorage.getItem("codEmp"), filial: window.localStorage.getItem('filLog'), campoFiltro: "", tenantOnHeader: true }))
		this.produtoAdapterService.setup(new adaptorFilterParam("JPRODUTO", "", "B1_DESC", "B1_DESC", "", "B1_COD", "", "getProduto", undefined, undefined, { useTenantId: true, empresa: window.localStorage.getItem("codEmp"), filial: window.localStorage.getItem('filLog'), campoFiltro: "", tenantOnHeader: true}))
		this.rateioAdapterService.setup(new adaptorFilterParam("JRATEIO", "", "CTJ_DESC", "CTJ_DESC", "", "CTJ_RATEIO", "", "getRateio", undefined, undefined, {useTenantId: true, empresa: window.localStorage.getItem("codEmp"), filial: window.localStorage.getItem('filLog'), campoFiltro: "", tenantOnHeader: true }))

		this.arrOpcIntegraFinanceiro = [{ label: "Sim", value: "1" }, { label: "Não", value: "2" }];

		routingState.loadRouting();

		if (this.route.snapshot.paramMap.get("codDesp") != undefined) {
			this.codDesp = atob(decodeURIComponent(route.snapshot.paramMap.get("codDesp")));
		}

		if(this.router.url.indexOf('/contrato/')>-1){
			this.filial = this.route.snapshot.paramMap.get('filContrato');
			window.sessionStorage.setItem('filial', atob(this.filial));
		
			this.chaveProcesso = this.route.snapshot.paramMap.get('codContrato');
			this.cajuri = atob(decodeURIComponent(this.chaveProcesso));
			this.isProcesso = false;
		}else{
			this.filial = this.route.snapshot.paramMap.get('filPro');
			window.sessionStorage.setItem('filial', atob(this.filial));

			this.chaveProcesso = this.route.snapshot.paramMap.get('codPro');
			this.cajuri = atob(decodeURIComponent(this.chaveProcesso));
			this.isProcesso = true;
		}
        
        //-- Adiciona o código do cajuri no título Meu processo + área
        this.cajuriTitulo = this.cajuri.replace(new RegExp('^0*', 'g'), '')

		// Chamada do serviço poI18n para buscar as constantes a partir do contexto 'garantias' no arquivo JurTraducao.
		poI18nService.getLiterals({ context: 'detDespesa', language: poI18nService.getLanguage() })
			.subscribe((litDes) => {
				this.litDetDes = litDes;

				//Verifica se é um processo ou contrato
				if (this.isProcesso){
					this.perfil = '/processo/';
					this.breadcrumbPerfil = this.litDetDes.breadCrumb.processo
					this.breadcrumbHome   = this.litDetDes.breadCrumb.home
					this.linkHome         = '/home'
				}else{
					this.perfil = '/contrato/';
					this.breadcrumbPerfil = this.litDetDes.breadCrumb.contrato
					this.breadcrumbHome   = this.litDetDes.breadCrumb.homeContrato
					this.linkHome         = '/homeContratos'
				}

				if (this.JuriGenerics.changeUndefinedToEmpty(this.codDesp) == "") {
					this.breadcrumb.items = [
						{ label: this.breadcrumbHome  , link: this.linkHome },
						{ label: this.breadcrumbPerfil, link: this.perfil + this.filial + '/' + this.chaveProcesso },
						{ label: this.litDetDes.breadCrumb.despesas, link: this.perfil + this.filial + '/' + this.chaveProcesso + '/despesa' },
						{ label: this.litDetDes.tituloDespesa }
					];
					this.titleDet = this.litDetDes.titleNovaDespesa;
					this.btnIncluirLabel = this.litDetDes.btnIncluir;
					this.btnVerAnexoEnabled = true;
				} else {
					this.breadcrumb.items = [
						{ label: this.breadcrumbHome  , link: this.linkHome },
						{ label: this.breadcrumbPerfil, link: this.perfil + this.filial + '/' + this.chaveProcesso },
						{ label: this.litDetDes.breadCrumb.despesas, link: this.perfil + this.filial + '/' + this.chaveProcesso + '/despesa' },
						{ label: this.descDesp }
					];
					this.titleDet = this.litDetDes.titleAlterarDespesa;
					this.btnIncluirLabel = this.litDetDes.btnAlterar;
					this.btnExcluirDisabled = false;
				}
			});
	}

	ngOnInit() {
		const frmDespesa: Despesa = new Despesa();

		this.createForm(frmDespesa);
		this.getInfoProcesso();

		// Busca os parâmetros do Protheus
		this.sysProtheusParam.getParamByArray(["MV_JTVENJF", "MV_JINTVAL", "MV_JALCADA"]).subscribe((params) => {
			this.sysParams = params;
			this.getDespesas();
			if (this.sysProtheusParam.getParamValueByName(params, 'MV_JINTVAL') == "1") {
				this.bIntegraFinanceiro = true;
				this.insertIntegFin(new Despesa());
			}
		});
	}

	/**
	 * Cria o FormBuilder da Despesa
	 * @param despesa - Entidade da despesa
	 */
	createForm(despesa: Despesa) {
		this.formDespesa = this.formBuilder.group({
			tipo: [despesa.tipo, Validators.compose([Validators.required])],
			data: [despesa.data, Validators.compose([Validators.required])],
			moeda: [despesa.moeda, Validators.compose([Validators.required])],
			valor: [despesa.valor, Validators.compose([Validators.required])],
			descricao: [despesa.descricao]
		});
	}

	/**
	 * Adiciona os controles da Integração Financeira
	 * @param frmGarantia
	 */
	insertIntegFin(frmDespesa: Despesa) {
		this.formDespesa.addControl('intFinPrefixo', new FormControl(frmDespesa.intFinPrefixo));
		this.formDespesa.addControl('intFinDataFinanc', new FormControl(frmDespesa.intFinDataFinanc));
		this.formDespesa.addControl('intFinNatureza', new FormControl(frmDespesa.intFinNatureza));
		this.formDespesa.addControl('intFinTitulo', new FormControl(frmDespesa.intFinTitulo));
		this.formDespesa.addControl('intFinFornecedor', new FormControl(frmDespesa.intFinFornecedor));
		this.formDespesa.addControl('intFinCondPagto', new FormControl(frmDespesa.intFinCondPagto));
		this.formDespesa.addControl('intFinProduto', new FormControl(frmDespesa.intFinProduto));
		this.formDespesa.addControl('intFinFiliDest', new FormControl(frmDespesa.intFinFiliDest));
		this.formDespesa.addControl('intFinRateio', new FormControl(frmDespesa.intFinRateio));
	}

	/**
	 * Busca as informações do processo
	 */
	getInfoProcesso() {
		this.fwModel.restore();
		this.fwModel.setModelo("JURA095");
		this.fwModel.setFilter("NSZ_FILIAL='" + atob(this.filial) + "'");
		this.fwModel.setFilter("NSZ_COD='" + this.cajuri + "'");
		this.fwModel.setCampoVirtual(true);
		this.fwModel.get("getInfoProcesso").subscribe(data => {
			let descResumoBreadCrumb = []
			if (this.isProcesso){
				descResumoBreadCrumb = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields,"NSZ_DAREAJ")
			}else{
				descResumoBreadCrumb = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields,"NSZ_DESCON")
			}
			if (data.resources != "") {
				this.breadcrumb.items[1].label = this.breadcrumbPerfil + ' '
					+ descResumoBreadCrumb
						+ " (" + this.cajuriTitulo + ")";
				this.isProcEncerrado = (this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NSZ_SITUAC") == "2");
			}
		});
	}

	/**
	 * Busca os dados da Despesa
	 */
	getDespesas() {
		this.isHideLoadingDespesa = false;
		// Caso seja de cadastro, busca os dados do combo somente
		if (this.JuriGenerics.changeUndefinedToEmpty(this.codDesp) == "") {
			this.getTitulo();
			this.isHideLoadingDespesa = true;
		} else {
			this.fwModel.restore();
			this.fwModel.setModelo("JURA099");
			this.fwModel.setFilter("NT3_CAJURI='" + this.cajuri + "'");
			this.fwModel.setFilter("NT3_COD='" + this.codDesp + "' ");
			this.fwModel.setFilter("NT3_FILIAL='" + atob(this.filial) + "' ");
			this.fwModel.setCampoVirtual(true);
			this.fwModel.get("getDespesa").subscribe(data => {
				if (data.count == 0) {
					this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codDesp))], { relativeTo: this.route });
					this.poNotification.error("Despesa não encontrada");
				} else {
					const resources = data.resources[0];
					const arrFields = resources.models[0].fields;

					this.pkDesp = resources.pk;

					// Inclusão dos campos
					this.formDespesa.patchValue({
						valor: this.JuriGenerics.findValueByName(arrFields, "NT3_VALOR"),
						moeda: this.JuriGenerics.findValueByName(arrFields, "NT3_CMOEDA"),
						tipo: this.JuriGenerics.findValueByName(arrFields, "NT3_CTPDES"),
						descricao: this.JuriGenerics.findValueByName(arrFields, "NT3_DESC"),
						data: this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(arrFields, "NT3_DATA"), "yyyy-mm-dd")
					});

					// Parte de Integração Financeira
					if (this.bIntegraFinanceiro) {
						this.formDespesa.patchValue({
							intFinPrefixo: this.JuriGenerics.findValueByName(arrFields, "NT3_PREFIX"),
							intFinFiliDest: this.JuriGenerics.findValueByName(arrFields, "NT3_FILDES"),
							intFinDataFinanc: this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(arrFields, "NT3_DTFIN"), "yyyy-mm-dd")
						});
						
						this.getTitulo(this.JuriGenerics.findValueByName(arrFields, "NT3_CTIPOT"));
						
						const filialDest: string = this.JuriGenerics.findValueByName(arrFields, "NT3_FILDES");
						if (this.JuriGenerics.changeUndefinedToEmpty(filialDest) != "") {
							this.setFilialDest(filialDest).then(() => {
								this.patchIntFin(arrFields)
							});
						} else {
							this.patchIntFin(arrFields)
						}
					}

					this.breadcrumb.items[3].label = this.JuriGenerics.findValueByName(arrFields, "NT3_DTPDES") + " - " +
													 this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(arrFields, "NT3_DATA"), "dd/mm/yyyy");
				}
				this.isHideLoadingDespesa = true;
			});
		}
	}
	/**
	 * Realiza o Patch value dos campos dependentes da Filial de Destino
	 * @param arrFields 
	 */
	patchIntFin(arrFields){
		this.formDespesa.patchValue({
			intFinNatureza: this.JuriGenerics.findValueByName(arrFields, "NT3_CNATUT"),
			intFinFornecedor: this.JuriGenerics.findConcatValueByMultiNames(arrFields, "NT3_CFORNT,NT3_LFORNT"),
			intFinCondPagto: this.JuriGenerics.findValueByName(arrFields, "NT3_CONDPG"),
			intFinProduto: this.JuriGenerics.findValueByName(arrFields, "NT3_PRODUT"),
			intFinRateio: this.JuriGenerics.findValueByName(arrFields, "NT3_CRATEI"),
		});
	}

	/**
	 * Define se haverá validação de contábil ou não
	 * @param event - Valor do item posicionado no combo
	 */
	setValidContabil(event: any) {
		if (event != undefined) {
			const fieldsExtra: adaptorReturnStruct = <adaptorReturnStruct> this.listTipoDespesa.selectedOption;
			this.bValidContabil = this.JuriGenerics.findValueByName(fieldsExtra.getExtras(), "NSR_INTCTB") == "1";
			// se a integração estiver ativa, houver validação de cotábil e for alteração, desabilita os campos para edição
			this.bDisableIntegraCTB = this.bValidContabil && this.bIntegraFinanceiro && this.pkDesp != "";
		} else {
			this.bValidContabil = false;
		}
	}

	/**
	 * Delete da despesa
	 */
	delDespesa() {
		this.fwModel.restore();
		this.fwModel.setModelo("JURA099");
		this.fwModel.delete(this.pkDesp, "deleteDespesa").subscribe(data => {
			if (data == true) {
				this.poNotification.success(this.litDetDes.msgExclusao);
				this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codDesp))], { relativeTo: this.route });
			}
		});
	}

	/**
	 * Cancela a ação e volta para a pagina anterior
	 */
	onCancel() {
		this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codDesp))], { relativeTo: this.route });
	}

	/**
	 * Função de inicio das validações e Commit.
	 */
	confirmVerify() {
		if (this.beforeSubmit(this.formDespesa.value)) {
			this.submitDesp();
		}
	}

	/**
	 * Função de validação e delete.
	 */
	deleteVerify() {
		if (this.beforeDelete(this.formDespesa.value)) {
			this.delDespesa();
		}
	}
	/**
	 * Realiza o submit da despesa
	 */
	submitDesp() {
		const despesa = this.formDespesa.value;
		let body: string;

		window.sessionStorage.setItem('filial', atob(this.filial));

		this.bLoadingIncGar = true;
		this.fwModel.restore();
		this.fwModel.setModelo("JURA099");

		if (this.bIntegraFinanceiro) {
			body = this.despBodyIntegra(despesa);
		} else {
			body = this.despBody(despesa);
		}

		if (this.pkDesp === "") {
			this.fwModel.post(body, 'postDespesa').subscribe((data) => {
				if (data != '') {
					this.codDesp = this.JuriGenerics.findValueByName(data.models[0].fields, "NT3_COD");
					this.poNotification.success(this.litDetDes.msgInclusaoSucesso);

					if (this.docUpload.hasOwnProperty('currentFiles') && this.docUpload.currentFiles.length > 0) {
						this.sendDocs();
					} else {
						this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codDesp))], { relativeTo: this.route });
					}
				}
				this.bLoadingIncGar = false;
			});
		} else {
			this.fwModel.put(this.pkDesp, body, 'putDespesa').subscribe((data) => {
				if (data != '') {
					this.codDesp = this.JuriGenerics.findValueByName(data.models[0].fields, "NT3_COD");
					this.poNotification.success(this.litDetDes.msgAlteracaoSucesso);

					if (this.docUpload.hasOwnProperty('currentFiles') && this.docUpload.currentFiles.length > 0) {
						this.sendDocs();
					} else {
						this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codDesp))], { relativeTo: this.route });
					}
				}
				this.bLoadingIncGar = false;
			});
		}
	}

	/**
	 * Pré-validação para o Submit
	 * @param frmDespesa 
	 */
	beforeSubmit(frmDespesa: Despesa) {
		let isSubmitable: boolean = this.formDespesa.valid;

		if (isSubmitable) {
			if (this.bIntegraFinanceiro && this.bValidContabil) {
				isSubmitable = (frmDespesa.intFinTitulo != undefined && frmDespesa.intFinTitulo != "") &&
					(frmDespesa.intFinFornecedor != undefined && frmDespesa.intFinFornecedor != "") &&
					(frmDespesa.intFinNatureza != undefined && frmDespesa.intFinNatureza != "");
				if (isSubmitable) {
					if (this.sysProtheusParam.getParamValueByName(this.sysParams, 'MV_JALCADA') == "1") {
						isSubmitable = (frmDespesa.intFinCondPagto != undefined && frmDespesa.intFinCondPagto != "") &&
							(frmDespesa.intFinProduto != undefined && frmDespesa.intFinProduto != "");
						if (!isSubmitable) {
							this.poNotification.error(this.litDetDes.msgError.frmDespContWithAlcada);
						}
					} else {
						isSubmitable = (frmDespesa.intFinPrefixo != undefined && frmDespesa.intFinPrefixo != "");

						if (!isSubmitable) {
							this.poNotification.error(this.litDetDes.msgError.frmDespContWithoutAlcada);
						}
					}
				} else {
					this.poNotification.error(this.litDetDes.msgError.frmDespWithInteg);
				}
			}
		} else {
			this.poNotification.error(this.litDetDes.msgError.frmDespWithoutInteg);
		}

		if (isSubmitable) {
			// Validação para apresentar o Modal de Justificativa, caso o processo esteja encerrado
			if (this.isProcEncerrado && this.sysProtheusParam.getParamValueByName(this.sysParams, "MV_JTVENJF") == "1") {
				isSubmitable = false;
				this.modalJustif.openModal(this.cajuri,
					() => {
						if (this.modalJustif.isOk) {
							this.submitDesp();
						}
					});
			} else {
				isSubmitable = true;
			}
		}
		return isSubmitable;
	}

	/**
	 * Pré-validação para o delete
	 * @param frmDespesa 
	 */
	beforeDelete(frmDespesa: Despesa) {
		let isSubmitable: boolean = this.formDespesa.valid;

		if (isSubmitable) {
			// Validação para apresentar o Modal de Justificativa, caso o processo esteja encerrado
			if (this.isProcEncerrado && this.sysProtheusParam.getParamValueByName(this.sysParams, "MV_JTVENJF") == "1") {
				isSubmitable = false;
				this.modalJustif.openModal(this.cajuri,
					() => {
						if (this.modalJustif.isOk) {
							this.delDespesa();
						}
					});
			} else {
				isSubmitable = true;
			}
		}
		return isSubmitable;
	}

	/**
	 * Chama o SelectFiles do po-upload
	 */
	getDocuments() {
		this.docUpload.selectFiles();
	}

	/**
	 * Chama a tela de anexos
	 */
	callAnexos() {
		this.router.navigate(["./anexos"], { relativeTo: this.route });
	}
	/**
	 * Realiza o envio dos documentos
	 */
	sendDocs() {
		if (this.docUpload.currentFiles.length > 0) {
			this.docUpload.sendFiles();
		}
	}
	/**
	 * Carrega as informações para o POST do upload de arquivos
	 * @param file
	 */
	uploadFiles(file) {
		file.data = {
			cajuri: this.cajuri.toString(),
			entidade: "NT3",
			codEntidade: this.codDesp.toString()
		};
	}

	/**
	 * Trata para redirecionar após concluir todos os anexos
	 */
	uploadSucess() {
		this.uploadSuccessCount++;

		if (this.uploadSuccessCount == this.docUpload.currentFiles.length) {
			this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codDesp))], { relativeTo: this.route });
		}
	}

	/**
	 * Monta o body para o put de despesas com a Integração Ativa
	 * @param despesa dados do formulário
	 */
	despBodyIntegra(despesa: Despesa) {
		let operation: number;
		let descricao: string;

		if (this.pkDesp === "") {
			operation = 1;
		} else {
			operation = 4;
		}

		if (despesa.descricao != null) {
			descricao = JurUTF8.encode(despesa.descricao);
		} else {
			descricao = '';
		}

		const bodyAtualiza: atualizaIntegra = {
			id: "JURA099", operation: operation, models: [
				{
					id: "NT3MASTER", modeltype: "FIELDS", fields: [
						{ id: "NT3_CAJURI" , order:  2, value: this.cajuri                                                       },
						{ id: "NT3_FILDES" , order:  4, value: this.JuriGenerics.changeUndefinedToEmpty(despesa.intFinFiliDest)  },
						{ id: "NT3_CTPDES" , order:  5, value: despesa.tipo                                                      },
						{ id: "NT3_DATA"   , order:  7, value: despesa.data.replace(/-/g, '')                                    },
						{ id: "NT3_CMOEDA" , order:  8, value: despesa.moeda                                                     },
						{ id: "NT3_VALOR"  , order: 10, value: despesa.valor.toString()                                          },
						{ id: "NT3_DESC"   , order: 13, value: descricao                                                         },
						{ id: "NT3_PREFIX" , order: 24, value: this.JuriGenerics.changeUndefinedToEmpty(despesa.intFinPrefixo)   },
						{ id: "NT3_CNATUT" , order: 25, value: this.JuriGenerics.changeUndefinedToEmpty(despesa.intFinNatureza)  },
						{ id: "NT3_CTIPOT" , order: 26, value: this.JuriGenerics.changeUndefinedToEmpty(despesa.intFinTitulo)    },
						{ id: "NT3_CFORNT" , order: 27, value: this.JuriGenerics.getValueByOption(this.cmbFornDesp,"A2_COD")     },
						{ id: "NT3_LFORNT" , order: 28, value: this.JuriGenerics.getValueByOption(this.cmbFornDesp,"A2_LOJA")    },
						{ id: "NT3_CONDPG" , order: 30, value: this.JuriGenerics.changeUndefinedToEmpty(despesa.intFinCondPagto) },
						{ id: "NT3_PRODUT" , order: 31, value: this.JuriGenerics.changeUndefinedToEmpty(despesa.intFinProduto)   },
						{ id: "NT3_CRATEI" , order: 12, value: this.JuriGenerics.changeUndefinedToEmpty(despesa.intFinRateio)    }
					]
				}
			]
		};
		return JSON.stringify(bodyAtualiza);
	}

	/**
	 * Monta o body para o put de despesas com a Integração Inativa
	 * @param despesa dados do formulário
	 */
	despBody(despesa: Despesa) {
		let operation: number;
		let descricao: string;

		if (this.pkDesp === "") {
			operation = 1;
		} else {
			operation = 4;
		}

		if (despesa.descricao != null) {
			descricao = JurUTF8.encode(despesa.descricao);
		} else {
			descricao = '';
		}

		const bodyAtualiza: Atualiza = {
			id: "JURA099", operation: operation, models: [
				{
					id: "NT3MASTER", modeltype: "FIELDS", fields: [
						{ id: "NT3_CAJURI", order: 1, value: this.cajuri },
						{ id: "NT3_CTPDES", order: 2, value: despesa.tipo },
						{ id: "NT3_DATA", order: 3, value: despesa.data.replace(/-/g, '') },
						{ id: "NT3_CMOEDA", order: 4, value: despesa.moeda },
						{ id: "NT3_VALOR", order: 5, value: despesa.valor.toString() },
						{ id: "NT3_DESC", order: 15, value: descricao }]
				}
			]
		};
		return JSON.stringify(bodyAtualiza);
	}

	/**
	 * Busca os titulos na SX5
	 * @param codTitulo - Código do Titulo posicionado
	 */
	getTitulo(codTitulo?: string) {
		this.legalprocess.restore();
		this.legalprocess.get('tlprocess/tabGen/05', 'Título').subscribe((data) => {
			if (data.result != '') {
				data.result.forEach(item => {
					let dadosTitulo = new DataCombo();
					dadosTitulo = {
						value: item.chave.trim(),
						label: item.descricao
					};
					this.lsIntFinTitulo.push(dadosTitulo);

				});
				if (codTitulo != undefined) {
					this.formDespesa.patchValue({
						intFinTitulo: codTitulo

					});
				}
			}
		});
	}

	/**
	 * Seta a filial e empresa para o filtro de fornecedor e natureza de acordo com a filial destino
	 * @param filial de destino selecionada no combo
	 */
	async setFilialDest(filial: string = "") {
		let cmbProdDespAdaptor      = <adaptorFilterParam>this.cmbProdDesp.filterParams;
		let cmbNatDespAdaptor       = <adaptorFilterParam>this.cmbNatDesp.filterParams;
		let cmbFornDespAdaptor      = <adaptorFilterParam>this.cmbFornDesp.filterParams;
		let cmbCondPagtoDespAdaptor = <adaptorFilterParam>this.cmbCondPagtoDesp.filterParams;
		let cmbRatDespAdaptor       = <adaptorFilterParam>this.cmbRatDesp.filterParams;
		
		if (this.JuriGenerics.changeUndefinedToEmpty(filial) == ""){
			filial = window.localStorage.getItem('filLog')
		}

		// Definindo a filial para o Produto
		cmbProdDespAdaptor.setTenantFilial(filial);
		this.cmbProdDesp.filterParams = cmbProdDespAdaptor;
		this.cmbProdDesp.updateSelectedValue(undefined);

		// Definindo a filial para a Natureza
		cmbNatDespAdaptor.setTenantFilial(filial);
		this.cmbNatDesp.filterParams = cmbNatDespAdaptor;
		this.cmbNatDesp.updateSelectedValue(undefined);

		// Definindo a filial para o Fornecedor
		cmbFornDespAdaptor.setTenantFilial(filial);
		this.cmbFornDesp.filterParams = cmbFornDespAdaptor;
		this.cmbFornDesp.updateSelectedValue(undefined);

		// Definindo a filial para a Condição de Pagamento
		cmbCondPagtoDespAdaptor.setTenantFilial(filial);
		this.cmbCondPagtoDesp.filterParams = cmbCondPagtoDespAdaptor;
		this.cmbCondPagtoDesp.updateSelectedValue(undefined);

		// Definindo a filial para o Rateio
		cmbRatDespAdaptor.setTenantFilial(filial);
		this.cmbRatDesp.filterParams = cmbRatDespAdaptor;
		this.cmbRatDesp.updateSelectedValue(undefined);
	}
}

/**
 * objeto do body da JURA099 SEM integração financeira
 */
interface Atualiza {
	id: string;
	operation: number;
	models: [{
		id: string, modeltype: string, fields: [
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string }]
	}];
}

/**
 * objeto do body da JURA099 COM integração financeira
 */
interface atualizaIntegra {
	id: string;
	operation: number;
	models: [{
		id: string, modeltype: string, fields: [
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string }]
	}];
}