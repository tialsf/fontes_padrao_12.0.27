const integraFinanceiro ={
	integracaoFinanceira: "Integração financeira",
	prefixo: "Prefixo",
	intFinDataFinanc: "Data financeiro",
	natureza: "Natureza",
	Titulo: "Titulo",
	fornecedor: "Fornecedor",
	CondPagto: "Condição de pagto",
	Produto: "Produto",
	FiliDest: "Filial destino",
	Rateio: "Rateio"

}
const validaSubmit = {
	error: "Preencha os campos corretamente"
}
const breadCrumb ={
	home: "Meus processos",
	processo: "Meu processo",
	despesas: "Despesas",
	contrato: 'Meu contrato',
	homeContrato: 'Meus contratos'
}

const msgError= {
	frmDespWithInteg: "Preencha os campos de titulo, fornecedor e natureza.",
	frmDespWithoutInteg: "Preencha os campos de tipo, data, moeda e valor.",
	frmDespContWithAlcada: "Preencha os campos de condição de pagamento e produto.",
	frmDespContWithoutAlcada: "Preencha o campo de prefixo."
}

export const detDespesasPt = {
	inputTipo: "Tipo",
	inputData: "Data",
	inputMoeda: "Moeda ",
	inputValor: "Valor",
	inputDesc: "Descrição",
	tituloDespesa: "Nova despesa",
	novaDes: "Incluir",
	loadDes: "Carregando despesa",
	btnCancelar: "Cancelar",
	btnExcluir: "Excluir despesa",
	btnIncluir: "Incluir despesa",
	btnAlterar: "Alterar despesa",
	titleNovaDespesa: "Nova despesa",
	titleAlterarDespesa: "Alterar despesa",
	msgInclusaoSucesso: "Despesa criada com sucesso!",
	msgAlteracaoSucesso: "Despesa alterada com sucesso!",
	confirmExclusao: "Tem certeza que deseja excluir esta despesa?",
	msgExclusao: "Despesa excluida com sucesso!",
	verTodosAnexos: "Ver todos os anexos",
	selecionarArquivo: "Selecionar arquivo",
	integraFinanceiro: integraFinanceiro,
	breadCrumb: breadCrumb,
	validaSubmit: validaSubmit,
	msgError: msgError
}
