const breadCrumb = {
	myProcess: 'Meus processos',
	painelDist: 'Painel de distribuições',
};

const fieldsDist = {
	status: 'Status',
	dtDist: 'Data distribuição',
	dtRece: 'Data recebimento',
	numProc: 'Número processo',
	advogado: 'Advogado',
	reu: 'Réu',
	autor: 'Autor',
	ocorre: 'Ocorrência',
	valor: 'Valor (R$)',
	tribunal: 'Tribunal',
	forum: 'Forum',
	vara: 'Vara',
	cidade: 'Cidade',
	estado: 'Estado',
	dtAudi: 'Data audiência',
	hrAudi: 'Hora audiência',
	termo: 'Termo',
	diviDist: 'Informações da distribuição',
};

const status = {
	pendImporta: 'Pendente de importação',
	pendComplem: 'Pendente de complemento',
	importada: 'Importada',
	excluida: 'Excluída',
	erroImportacao: 'Erro de importação',
	processando: 'Processando',
};

export const detDistribuicaoPt = {
	fieldsDist,
	status,
	breadCrumb,
	detDist: 'Detalhes da distribuição',
	docPeticio: 'Documentos peticionados',
	voltar: 'Voltar',
	loading: 'Carregando informações',
};
