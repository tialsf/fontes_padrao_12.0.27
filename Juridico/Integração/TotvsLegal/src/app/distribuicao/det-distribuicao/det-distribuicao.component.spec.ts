import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetDistribuicaoComponent } from './det-distribuicao.component';

describe('DetDistribuicaoComponent', () => {
	let component: DetDistribuicaoComponent;
	let fixture: ComponentFixture<DetDistribuicaoComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [DetDistribuicaoComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DetDistribuicaoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
