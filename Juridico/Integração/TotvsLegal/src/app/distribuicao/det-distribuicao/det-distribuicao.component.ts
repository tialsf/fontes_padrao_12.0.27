import { Component, OnInit, PipeTransform, Pipe } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { PoDynamicViewField, PoI18nService, PoBreadcrumb } from '@po-ui/ng-components';

import { DetDistribuicao } from './det-distribuicao';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { FwmodelService } from 'src/app/services/fwmodel.service';

class Status {
	label: string = '';
	value: string = '';
	color: string = '';
}

@Component({
	selector: 'app-det-distribuicao',
	templateUrl: './det-distribuicao.component.html',
	styleUrls: ['./det-distribuicao.component.css'],
})
export class DetDistribuicaoComponent implements OnInit {
	litDetDist;
	breadCrumb: PoBreadcrumb;

	linkDoc: string = '';
	styleDetDist: string = '';
	pk: string = this.route.snapshot.paramMap.get('pk');

	hideLoadingDetDist: boolean = true;
	hasDocumento: boolean = false;

	nHeightMonitor: number = 0;
	nHeightPdfMonitor: number = 0;

	valuesDistribuicao: DetDistribuicao = new DetDistribuicao();

	fieldsDistribuicao: Array<PoDynamicViewField> = [
		{ property: 'status', tag: true, color: 'color-11' },
		{ property: 'dtDistribuicao', type: 'date' },
		{ property: 'dtRecebimento', type: 'date' },
		{
			property: 'erroImportacao',
			gridColumns: 12,
			visible: false,
		},
		{ property: 'numProcesso', gridColumns: 4 },
		{ property: 'advoga', gridColumns: 4 },
		{ property: 'reu' },
		{ property: 'autor', gridColumns: 4 },
		{ property: 'ocorrencia', gridColumns: 4 },
		{ property: 'valor', type: 'currency' },
		{ property: 'tribunal', gridColumns: 4 },
		{ property: 'forum', gridColumns: 4 },
		{ property: 'vara' },
		{ property: 'cidade', gridColumns: 4 },
		{ property: 'estado', gridColumns: 4 },
		{ property: 'dtAudiencia', type: 'date' },
		{ property: 'hrAudiencia', gridColumns: 4 },
		{ property: 'termo' },
	];

	/**
	 * Lista de status de distribuições
	 */
	listStatus: Array<Status> = [
		{ label: '', value: '1', color: 'color-08' },
		{ label: '', value: '2', color: 'color-11' },
		{ label: '', value: '3', color: 'color-07' },
		{ label: '', value: '4', color: 'color-07' },
		{ label: '', value: '5', color: 'color-02' },
	];

	constructor(
		private route: ActivatedRoute,
		private juriGenerics: JurigenericsService,
		private fwModel: FwmodelService,
		private router: Router,
		private poI18nService: PoI18nService
	) {
		// -- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'distribuicao' no arquivo JurTraducao.
		poI18nService
			.getLiterals({ context: 'detDistribuicao', language: poI18nService.getLanguage() })
			.subscribe(litDetDistribuicao => {
				this.litDetDist = litDetDistribuicao;
				this.setBreadCrumb();
				this.onHeightTela();

				// Tradução das labels e dividers do detalhe da distribuição
				this.fieldsDistribuicao.find(
					x => x.property === 'status'
				).label = this.litDetDist.fieldsDist.status;
				this.fieldsDistribuicao[1].label = this.litDetDist.fieldsDist.dtDist;
				this.fieldsDistribuicao[2].label = this.litDetDist.fieldsDist.dtRece;
				this.fieldsDistribuicao[3].label = ' ';
				this.fieldsDistribuicao[4].divider = this.litDetDist.fieldsDist.diviDist;
				this.fieldsDistribuicao[4].label = this.litDetDist.fieldsDist.numProc;
				this.fieldsDistribuicao[5].label = this.litDetDist.fieldsDist.advogado;
				this.fieldsDistribuicao[6].label = this.litDetDist.fieldsDist.reu;
				this.fieldsDistribuicao[7].label = this.litDetDist.fieldsDist.autor;
				this.fieldsDistribuicao[8].label = this.litDetDist.fieldsDist.ocorre;
				this.fieldsDistribuicao[9].label = this.litDetDist.fieldsDist.valor;
				this.fieldsDistribuicao[10].label = this.litDetDist.fieldsDist.tribunal;
				this.fieldsDistribuicao[11].label = this.litDetDist.fieldsDist.forum;
				this.fieldsDistribuicao[12].label = this.litDetDist.fieldsDist.vara;
				this.fieldsDistribuicao[13].label = this.litDetDist.fieldsDist.cidade;
				this.fieldsDistribuicao[14].label = this.litDetDist.fieldsDist.estado;
				this.fieldsDistribuicao[15].label = this.litDetDist.fieldsDist.dtAudi;
				this.fieldsDistribuicao[16].label = this.litDetDist.fieldsDist.hrAudi;
				this.fieldsDistribuicao[17].label = this.litDetDist.fieldsDist.termo;

				// Tradução das Labels da lista de status das distribuições
				this.listStatus[0].label = this.litDetDist.status.pendImporta;
				this.listStatus[1].label = this.litDetDist.status.importada;
				this.listStatus[2].label = this.litDetDist.status.excluida;
				this.listStatus[3].label = this.litDetDist.status.erroImportacao;
				this.listStatus[4].label = this.litDetDist.status.processando;
			});
	}

	ngOnInit() {
		this.getDetDistribuição();
	}

	/**
	 * método responsavel pela criação do breadcrumb
	 */
	private setBreadCrumb() {
		this.breadCrumb = {
			items: [
				{ label: this.litDetDist.breadCrumb.myProcess, link: '/' },
				{ label: this.litDetDist.breadCrumb.painelDist, link: '/Distribuicao' },
				{ label: this.litDetDist.detDist },
			],
		};
	}

	/**
	 * função responsável pelo retorno de um observable contendo os dados de tipos de assuntos jurídicos
	 */
	getDetDistribuição() {
		this.hideLoadingDetDist = false;
		this.fwModel.restore();
		this.fwModel.setModelo('JURA279/' + this.pk);
		this.fwModel.get('Busca detalhes da Distribuição').subscribe(detDistribuicao => {
			const infoDetDistribuicao: DetDistribuicao = new DetDistribuicao();

			if (this.juriGenerics.changeUndefinedToEmpty(detDistribuicao) !== '') {
				infoDetDistribuicao.advoga = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_ADVOGA'
				);
				infoDetDistribuicao.autor = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_AUTOR'
				);
				infoDetDistribuicao.cajuri = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_CAJURI'
				);
				infoDetDistribuicao.cidade = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_CIDADE'
				);
				infoDetDistribuicao.escritorio = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_ESCRI'
				);
				infoDetDistribuicao.estado = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_ESTADO'
				);
				infoDetDistribuicao.forum = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_FORUM'
				);
				infoDetDistribuicao.hrAudiencia = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_HRAUDI'
				);
				infoDetDistribuicao.login = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_LOGIN'
				);
				infoDetDistribuicao.numProcesso = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_NUMPRO'
				);
				infoDetDistribuicao.ocorrencia = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_OCORRE'
				);
				infoDetDistribuicao.reu = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_REU'
				);
				infoDetDistribuicao.termo = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_TERMO'
				);
				infoDetDistribuicao.tribunal = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_TRIBUN'
				);
				infoDetDistribuicao.valor = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_VALOR'
				);
				infoDetDistribuicao.vara = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_VARA'
				);

				infoDetDistribuicao.status = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_STATUS'
				);
				infoDetDistribuicao.status = this.listStatus.find(
					x => x.value === infoDetDistribuicao.status
				).label;

				infoDetDistribuicao.dtAudiencia = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_DTAUDI'
				);
				infoDetDistribuicao.dtDistribuicao = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_DTDIST'
				);
				infoDetDistribuicao.dtRecebimento = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_DTREC'
				);

				infoDetDistribuicao.dtAudiencia = this.juriGenerics.makeDate(
					infoDetDistribuicao.dtAudiencia,
					'yyyy-mm-dd'
				);
				infoDetDistribuicao.dtDistribuicao = this.juriGenerics.makeDate(
					infoDetDistribuicao.dtDistribuicao,
					'yyyy-mm-dd'
				);
				infoDetDistribuicao.dtRecebimento = this.juriGenerics.makeDate(
					infoDetDistribuicao.dtRecebimento,
					'yyyy-mm-dd'
				);

				this.linkDoc = this.juriGenerics.findValueByName(
					detDistribuicao.models[0].fields,
					'NZZ_LINK'
				);

				infoDetDistribuicao.erroImportacao = this.juriGenerics.changeUndefinedToEmpty(
					this.juriGenerics.findValueByName(detDistribuicao.models[0].fields, 'NZZ_ERRO')
				);

				if (
					infoDetDistribuicao.status === this.litDetDist.status.pendImporta &&
					infoDetDistribuicao.erroImportacao !== ''
				) {
					infoDetDistribuicao.status = this.listStatus.find(x => x.value === '4').label;
					this.fieldsDistribuicao[3].visible = true; // deixa o campo de Erro visivel

					this.fieldsDistribuicao[0].order = 3; // altera a ordem da coluna de status

					this.fieldsDistribuicao[0].gridColumns = 3;

					this.fieldsDistribuicao[1].order = 1;
					this.fieldsDistribuicao[1].gridColumns = 6;

					this.fieldsDistribuicao[2].order = 2;
					this.fieldsDistribuicao[2].gridColumns = 6;
				}

				this.fieldsDistribuicao[0].color = this.listStatus.find(
					x => x.label === infoDetDistribuicao.status
				).color; // altera a cor da tag do status

				if (this.juriGenerics.changeUndefinedToEmpty(this.linkDoc) !== '') {
					this.hasDocumento = true;
				}

				if (this.juriGenerics.changeUndefinedToEmpty(infoDetDistribuicao.autor) !== '') {
					this.breadCrumb.items[2].label = infoDetDistribuicao.autor + ' / ';
				} else {
					this.breadCrumb.items[2].label = '';
				}

				if (this.juriGenerics.changeUndefinedToEmpty(infoDetDistribuicao.reu) !== '') {
					this.breadCrumb.items[2].label += infoDetDistribuicao.reu + ' / ';
				}

				if (
					this.juriGenerics.changeUndefinedToEmpty(infoDetDistribuicao.numProcesso) !== ''
				) {
					this.breadCrumb.items[2].label += infoDetDistribuicao.numProcesso;
				}
			}

			this.hideLoadingDetDist = true;
			this.valuesDistribuicao = infoDetDistribuicao;
		});
	}

	/**
	 * Função responsável por retornar para o painel de distribuição
	 */
	onCancel() {
		this.router.navigate(['/Distribuicao'], { relativeTo: this.route });
	}

	/**
	 * Função responsável por redimencionar a pagina de acordo com a resolução.
	 */
	onHeightTela() {
		if (window.innerHeight > 660) {
			this.nHeightMonitor = window.innerHeight - 211;
			this.nHeightPdfMonitor = this.nHeightMonitor - 90;
		} else {
			this.nHeightMonitor = window.innerHeight - 191;
			this.nHeightPdfMonitor = this.nHeightMonitor - 74;
		}

		this.styleDetDist = 'height: ' + this.nHeightMonitor + '; border: solid 1px;';
	}
}

@Pipe({
	name: 'safe',
})
export class SafePipe implements PipeTransform {
	constructor(private sanitizer: DomSanitizer) {}

	transform(url) {
		return this.sanitizer.bypassSecurityTrustResourceUrl(url);
	}
}
