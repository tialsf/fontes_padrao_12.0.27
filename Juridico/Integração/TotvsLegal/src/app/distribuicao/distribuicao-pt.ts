/**
 * Constantes para o bread crumb
 */
export const breadCrumb = {
	home: 'Meus processos',
	distribuicao: 'Painel de distribuições',
};

/**
 * Constantes para o tabela de Distribuições
 */
export const tableDistri = {
	processo: 'Processo',
	autor: 'Autor',
	reu: 'Réu',
	dtdistrib: 'Distribuição',
	dtReceb: 'Recebido',
	ocorrencia: 'Ocorrência',
	vlrCausa: 'Valor causa',
	dtProxAudi: 'Audiência',
	openProcess: 'Abrir processo',
	maisDetalhes: 'Mais detalhes',
	semFiltro: 'Nenhum filtro foi selecionado.',
	sucExcluir: 'Distribuição {0} foi excluída com sucesso!',
	erroExcluir: 'Não foi possível excluir a distribuição {0}!',
	vldExclusao: 'Não é permitido realizar a exclusão da distribuição {0}, pois seu status é {1}.',
};

export const status = {
	pendImporta: 'Pendente de importação',
	pendComplem: 'Pendente de complemento',
	importada: 'Importada',
	excluida: 'Excluída',
	erroImportacao: 'Erro de importação',
	processando: 'Processando',
};

export const filtroDistri = {
	palavraChave: 'Filtro por palavra chave',
	pChaveHolder: 'Informe a palavra chave para ser pesquisada',
	status: 'Status',
	atalhoData: 'Período de recebimento',
	dataInicial: 'Data inicial',
	dataFinal: 'Data final',
	adicDisclaimer: 'Adicionar filtro',
	tooltipPeriodo: 'Já possui filtro para períodos!',
};

export const atalhoData = {
	hoje: 'Hoje',
	nestaSemana: 'Esta semana',
	semanaPassada: 'Semana passada',
	nesteMes: 'Neste mês',
	mesPassado: 'Mês passado',
	maisAntigo: 'Mais antigos',
	escolher: 'Escolher Período',
};

export const disclaimer = {
	periodo: 'Período de {0} até {1}',
	apartir: 'Período a partir de {0}',
	ateDia: 'Período até {0}',
	palavraChave: 'Palavra Chave: {0}',
	status: 'Status: {0}',
};

/** Constantes para o Painel de Distribuição */
export const distribuicaoPt = {
	breadCrumb,
	tableDistri,
	status,
	filtroDistri,
	atalhoData,
	disclaimer,
	excluir: 'Excluir',
	loading: 'Carregando distribuições',
	importar: 'Importar',
	qtdDistribuicao: ' registros localizados',
	aplicarModelo: ' Aplicar modelo',
};
