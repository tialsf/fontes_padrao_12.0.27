import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetDistribuicaoComponent } from './det-distribuicao/det-distribuicao.component';
import { DistribuicaoComponent } from './distribuicao.component';
import { ModImportacaoComponent } from './mod-importacao/mod-importacao.component';

const routes: Routes = [
	{
		path: '',
		children: [
			{
				path: '',
				pathMatch: 'full',
				component: DistribuicaoComponent,
			},
			{
				path: 'detalhe/:pk',
				component: DetDistribuicaoComponent,
			},
			{
				path: 'importacao',
				component: ModImportacaoComponent,
			},
		],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class DistribuicaoRoutingModule {}
