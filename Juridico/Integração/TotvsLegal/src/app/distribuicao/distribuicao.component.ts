import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FiltroDistribuicao } from './distribuicao';
import { JurigenericsService } from '../services/jurigenerics.service';
import { FwmodelService, FwModelBody } from '../services/fwmodel.service';
import {
	PoBreadcrumb,
	PoSelectOption,
	PoI18nService,
	PoDisclaimer,
	PoTableColumn,
	PoI18nPipe,
	PoMultiselectOption,
	PoNotificationService,
} from '@po-ui/ng-components';

/**
 * Estrutura da tabela da listagem de distribuições
 */
class Distribuicao {
	pk: string = '';
	cajuri: string = '';
	processo: string = '';
	autor: string = '';
	reu: string = '';
	dtDistrib: string = '';
	dtReceb: string = '';
	ocorrencia: string = '';
	vlrCausa: string = '';
	dtProxAudi: string = '';
	status: string = '';
	erroImportacao: string = '';
	distriEdit: Array<string> = [];
	selected: boolean = false;
}

/**
 * Estrutura de index dos status que estão filtrados.
 */
class Status {
	value: string = '';
	count: number = -1;
}

/**
 * Estrutura para o Disclaimer
 */
class DisclaimerItemStruct implements PoDisclaimer {
	label: string = '';
	field: string = '';
	value: number | string | Array<string> = [];
}

@Component({
	selector: 'app-distribuicao',
	templateUrl: './distribuicao.component.html',
	styleUrls: ['./distribuicao.component.css'],
})
export class DistribuicaoComponent implements OnInit {
	litDistribuicao;

	formFiltrar: FormGroup;
	breadCrumb: PoBreadcrumb;

	tooltipAtalhoData: string = '';

	isDisableBtnAdicao: boolean = true;
	isDisableBtnExclui: boolean = true;
	hasFiltro: boolean = true;
	isDisableBtnAtalho: boolean = false;
	hasHidePeriodo: boolean = false;
	hideLoadingDistr: boolean = false;
	isDisabledShowMore: boolean = false;
	canImport: boolean = false;

	nTotalDistribuicao: number = 0;
	nQtdDistribuicao: number = 0;
	nHeightMonitor: number = 0;
	nRegIndex: number = 1;
	nNextPage: number = 1;
	nPageSize: number = 5;

	listDistribuicao: Array<Distribuicao> = [];
	disclaimers: Array<DisclaimerItemStruct> = [];

	/**
	 *  Colunas da tabela de Distribuição
	 */
	colDistribuicoes: Array<PoTableColumn> = [
		{ property: 'processo', label: '', width: '15%' },
		{ property: 'autor', label: '', width: '15%' },
		{ property: 'reu', label: '', width: '15%' },
		{ property: 'dtDistrib', label: '', type: 'date', width: '8%' },
		{ property: 'dtReceb', label: '', type: 'date', width: '8%' },
		{ property: 'ocorrencia', label: '', width: '15%' },
		{ property: 'vlrCausa', label: '', type: 'number', format: '1.2-5', width: '7%' },
		{ property: 'dtProxAudi', label: '', type: 'date', width: '8%' },
		{
			property: 'status',
			type: 'label',
			width: '13%',
			labels: [
				{ value: '1', color: 'color-08', label: '' },
				{ value: '2', color: 'color-11', label: '' },
				{ value: '3', color: 'color-07', label: '' },
				{ value: '4', color: 'color-07', label: '' },
				{ value: '5', color: 'color-02', label: '' },
			],
		},
		{
			property: 'distriEdit',
			label: ' ',
			type: 'icon',
			width: '6%',
			icons: [
				{
					value: 'excluir',
					icon: 'po-icon po-icon-delete',
					color: '#color-07',
					tooltip: 'Excluir',
					action: value => {
						this.deleteDistr(value);
					},
				},
				{
					value: 'openProcesso',
					icon: 'po-icon po-icon-menu-open',
					color: '#color-07',
					tooltip: 'Abrir processo',
					action: value => {
						this.openProcesso(value.cajuri);
					},
				},
				{
					value: 'verDetalhes',
					icon: 'po-icon po-icon-plus-circle',
					color: '#color-07',
					tooltip: 'Mais detalhes',
					action: value => {
						this.verDetalhes(value.pk);
					},
				},
			],
		},
	];

	/**
	 * Atalho que mostra períodos de datas
	 */
	listAtalhoData: Array<PoSelectOption> = [
		{ label: '', value: '1' },
		{ label: '', value: '2' },
		{ label: '', value: '3' },
		{ label: '', value: '4' },
		{ label: '', value: '5' },
		{ label: '', value: '6' },
		{ label: '', value: '7' },
	];

	/**
	 * Lista de status de distribuições
	 */
	listStatus: Array<PoMultiselectOption> = [
		{ label: 'Pendente de importação', value: '1' },
		{ label: 'Importada', value: '2' },
		{ label: 'Excluída', value: '3' },
		{ label: 'Erro de Importação', value: '4' },
		{ label: 'Processando', value: '5' },
	];

	constructor(
		private formBuilder: FormBuilder,
		private juriGenerics: JurigenericsService,
		private poI18nPipe: PoI18nPipe,
		private poNotification: PoNotificationService,
		private fwModel: FwmodelService,
		private route: ActivatedRoute,
		private router: Router,
		private poI18nService: PoI18nService
	) {
		// -- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'distribuicao' no arquivo JurTraducao.
		this.poI18nService
			.getLiterals({ context: 'distribuicao', language: poI18nService.getLanguage() })
			.subscribe(litDistribuicao => {
				this.litDistribuicao = litDistribuicao;
				this.setBreadCrumb();
				this.onHeightTela(true);

				// tradução da tabela de distribuição
				this.colDistribuicoes[0].label = this.litDistribuicao.tableDistri.processo;
				this.colDistribuicoes[1].label = this.litDistribuicao.tableDistri.autor;
				this.colDistribuicoes[2].label = this.litDistribuicao.tableDistri.reu;
				this.colDistribuicoes[3].label = this.litDistribuicao.tableDistri.dtdistrib;
				this.colDistribuicoes[4].label = this.litDistribuicao.tableDistri.dtReceb;
				this.colDistribuicoes[5].label = this.litDistribuicao.tableDistri.ocorrencia;
				this.colDistribuicoes[6].label = this.litDistribuicao.tableDistri.vlrCausa;
				this.colDistribuicoes[7].label = this.litDistribuicao.tableDistri.dtProxAudi;
				this.colDistribuicoes[8].labels[0].label = this.litDistribuicao.status.pendImporta;
				this.colDistribuicoes[8].labels[1].label = this.litDistribuicao.status.importada;
				this.colDistribuicoes[8].labels[2].label = this.litDistribuicao.status.excluida;
				this.colDistribuicoes[8].labels[3].label = this.litDistribuicao.status.erroImportacao;
				this.colDistribuicoes[8].labels[4].label = this.litDistribuicao.status.processando;
				this.colDistribuicoes[9].icons[0].tooltip = this.litDistribuicao.excluir;
				this.colDistribuicoes[9].icons[1].tooltip = this.litDistribuicao.tableDistri.openProcess;
				this.colDistribuicoes[9].icons[2].tooltip = this.litDistribuicao.tableDistri.maisDetalhes;

				// tradução da lista de atalhos de períodos de datas
				this.listAtalhoData[0].label = this.litDistribuicao.atalhoData.hoje;
				this.listAtalhoData[1].label = this.litDistribuicao.atalhoData.nestaSemana;
				this.listAtalhoData[2].label = this.litDistribuicao.atalhoData.semanaPassada;
				this.listAtalhoData[3].label = this.litDistribuicao.atalhoData.nesteMes;
				this.listAtalhoData[4].label = this.litDistribuicao.atalhoData.mesPassado;
				this.listAtalhoData[5].label = this.litDistribuicao.atalhoData.maisAntigo;
				this.listAtalhoData[6].label = this.litDistribuicao.atalhoData.escolher;

				// tradução da lista de status
				this.listStatus[0].label = this.litDistribuicao.status.pendImporta;
				this.listStatus[1].label = this.litDistribuicao.status.importada;
				this.listStatus[2].label = this.litDistribuicao.status.excluida;
				this.listStatus[3].label = this.litDistribuicao.status.erroImportacao;
				this.listStatus[4].label = this.litDistribuicao.status.processando;
			});
	}

	ngOnInit() {
		this.defaultDisclaimer();
		this.createFormFiltro(new FiltroDistribuicao());
	}

	/**
	 * Criação do formulário de filtro das distribuições
	 * @param filtroDistribuicao - estrutura do formulário de filtro
	 */
	createFormFiltro(filtroDistribuicao: FiltroDistribuicao) {
		this.formFiltrar = this.formBuilder.group({
			palavraChave: [filtroDistribuicao.palavraChave],
			atalhoData: [filtroDistribuicao.atalhoData],
			dataInicial: [filtroDistribuicao.dataInicial],
			dataFinal: [filtroDistribuicao.dataFinal],
			status: [filtroDistribuicao.status],
		});
	}

	/**
	 * método responsavel pela criação do breadcrumb conforme as literais criadas
	 */
	private setBreadCrumb() {
		this.breadCrumb = {
			items: [
				{ label: this.litDistribuicao.breadCrumb.home, link: '/' },
				{ label: this.litDistribuicao.breadCrumb.distribuicao, link: '/Distribuicao' },
			],
		};
	}

	/**
	 * Sugestão de data, conforme o atalho selecionado no HTML.
	 */
	getSugestData() {
		let dataHoje: Date = new Date();
		let atalhoSelec: string = this.formFiltrar.value.atalhoData;
		let dSugeDtIni: string = '';
		let dSugeDtFin: string = '';

		this.hasHidePeriodo = false;

		switch (atalhoSelec) {
			case '1': // Hoje
				dSugeDtIni = this.juriGenerics.makeDateProtheus();
				dSugeDtIni = this.juriGenerics.makeDate(dSugeDtIni, 'yyyy-mm-dd');

				dSugeDtFin = this.juriGenerics.makeDateProtheus();
				dSugeDtFin = this.juriGenerics.makeDate(dSugeDtFin, 'yyyy-mm-dd');
				break;

			case '2': // Esta Semana
				dSugeDtIni =
					dataHoje.getFullYear() + ('0' + (dataHoje.getMonth() + 1)) + dataHoje.getDate();
				dSugeDtIni = dataHoje.setDate(dataHoje.getDate() - dataHoje.getDay()).toString();
				dSugeDtIni = dataHoje.toISOString().substring(0, 10);

				dSugeDtFin =
					dataHoje.getFullYear() + ('0' + (dataHoje.getMonth() + 1)) + dataHoje.getDate();
				dSugeDtFin = dataHoje
					.setDate(dataHoje.getDate() - dataHoje.getDay() + 6)
					.toString();
				dSugeDtFin = dataHoje.toISOString().substring(0, 10);
				break;

			case '3': // Semana passada
				dSugeDtIni =
					dataHoje.getFullYear() + ('0' + (dataHoje.getMonth() + 1)) + dataHoje.getDate();
				dSugeDtIni = dataHoje
					.setDate(dataHoje.getDate() - dataHoje.getDay() - 7)
					.toString();
				dSugeDtIni = dataHoje.toISOString().substring(0, 10);

				dSugeDtFin =
					dataHoje.getFullYear() + ('0' + (dataHoje.getMonth() + 1)) + dataHoje.getDate();
				dSugeDtFin = dataHoje
					.setDate(dataHoje.getDate() - dataHoje.getDay() + 6)
					.toString();
				dSugeDtFin = dataHoje.toISOString().substring(0, 10);
				break;

			case '4': // Neste Mês
				dSugeDtIni =
					dataHoje.getFullYear().toString() +
					('0' + (dataHoje.getMonth() + 1).toString()).substring(
						('0' + (dataHoje.getMonth() + 1).toString()).length - 2
					) +
					'01';
				dSugeDtIni = this.juriGenerics.makeDate(dSugeDtIni, 'yyyy-mm-dd');

				dSugeDtFin = dataHoje.setMonth(dataHoje.getMonth() + 1).toString();
				dSugeDtFin = dataHoje.setDate(dataHoje.getDate() - dataHoje.getDate()).toString();
				dSugeDtFin = dataHoje.toISOString().substring(0, 10);
				break;

			case '5': // Mês Passado
				dSugeDtIni =
					dataHoje.getFullYear().toString() +
					('0' + dataHoje.getMonth().toString()).substring(
						('0' + dataHoje.getMonth().toString()).length - 2
					) +
					'01';
				dSugeDtIni = this.juriGenerics.makeDate(dSugeDtIni, 'yyyy-mm-dd');

				dSugeDtFin = dataHoje.setMonth(dataHoje.getMonth()).toString();
				dSugeDtFin = dataHoje.setDate(dataHoje.getDate() - dataHoje.getDate()).toString();
				dSugeDtFin = dataHoje.toISOString().substring(0, 10);
				break;

			case '6': // Mais antigos
				dSugeDtIni = '1990-01-01';

				dSugeDtFin = this.juriGenerics.makeDateProtheus();
				dSugeDtFin = this.juriGenerics.makeDate(dSugeDtFin, 'yyyy-mm-dd');
				break;

			case '7': // Escolher período
				this.hasHidePeriodo = true;
				break;
		}

		this.formFiltrar.patchValue({
			dataInicial: dSugeDtIni,
			dataFinal: dSugeDtFin,
		});

		this.adicionaDisclaimer();
	}

	/**
	 * Função responsável por adicionar o filtro selecionado no disclaimer
	 */
	adicionaDisclaimer() {
		let formulario: FiltroDistribuicao = this.formFiltrar.value;
		let label: string = '';

		if (
			this.juriGenerics.changeUndefinedToEmpty(formulario.dataInicial) != '' ||
			this.juriGenerics.changeUndefinedToEmpty(formulario.dataFinal) != ''
		) {
			let disclaimerItem: DisclaimerItemStruct = new DisclaimerItemStruct();
			let dataIniForm: string = this.juriGenerics
				.changeUndefinedToEmpty(formulario.dataInicial)
				.replace(/-/g, '');
			let dataFinForm: string = this.juriGenerics
				.changeUndefinedToEmpty(formulario.dataFinal)
				.replace(/-/g, '');
			let dataIni: string = '';
			let dataFin: string = '';

			if (dataIniForm != '' && dataFinForm != '') {
				dataIni = this.juriGenerics.makeDate(dataIniForm, 'dd/mm/yyyy');
				dataFin = this.juriGenerics.makeDate(dataFinForm, 'dd/mm/yyyy');

				label = this.poI18nPipe.transform(this.litDistribuicao.disclaimer.periodo, [
					dataIni,
					dataFin,
				]);
			} else if (dataIniForm != '' && dataFinForm == '') {
				dataIni = this.juriGenerics.makeDate(dataIniForm, 'dd/mm/yyyy');

				label = this.poI18nPipe.transform(this.litDistribuicao.disclaimer.apartir, [
					dataIni,
				]);
			} else if (dataIniForm == '' && dataFinForm != '') {
				dataFin = this.juriGenerics.makeDate(dataFinForm, 'dd/mm/yyyy');

				label = this.poI18nPipe.transform(this.litDistribuicao.disclaimer.ateDia, [
					dataFin,
				]);
			}

			disclaimerItem.label = label;
			disclaimerItem.field = 'NZZ_DTREC';
			disclaimerItem.value = [dataIniForm, dataFinForm];
			this.disclaimers = [...this.disclaimers, disclaimerItem];
		}

		if (this.juriGenerics.changeUndefinedToEmpty(formulario.palavraChave) != '') {
			let palavraChave: string = this.juriGenerics.changeUndefinedToEmpty(
				formulario.palavraChave
			);
			let disclaimerItem: DisclaimerItemStruct = new DisclaimerItemStruct();

			if (this.disclaimers.findIndex(x => x.value[0] == palavraChave) == -1) {
				label = this.poI18nPipe.transform(this.litDistribuicao.disclaimer.palavraChave, [
					palavraChave,
				]);

				disclaimerItem.label = label;
				disclaimerItem.field = 'palavraChave';
				disclaimerItem.value = palavraChave;
				this.disclaimers = [...this.disclaimers, disclaimerItem];
			}
		}

		if (formulario.status.length > 0) {
			let status: Array<string> = formulario.status;

			status.forEach(item => {
				if (this.disclaimers.findIndex(x => x.value == item) == -1) {
					let disclaimerItem: DisclaimerItemStruct = new DisclaimerItemStruct();
					let nIndex: number = this.listStatus.findIndex(x => x.value == item);

					label = this.poI18nPipe.transform(this.litDistribuicao.disclaimer.status, [
						this.listStatus[nIndex].label,
					]);

					disclaimerItem.label = label;
					disclaimerItem.field = 'NZZ_STATUS';
					disclaimerItem.value = item;
					this.disclaimers = [...this.disclaimers, disclaimerItem];
				}
			});
		}
	}

	/**
	 * disparada quando a lista de disclaimers é modificada
	 * atualiza as distribuição da tabela
	 */
	changeFilters() {
		let formStatus: Array<string> = this.formFiltrar.value.status;
		let listAux: Array<string> = [];
		this.isDisableBtnAtalho = false;
		this.listDistribuicao = [];
		this.tooltipAtalhoData = '';
		this.nRegIndex = 1;
		this.nNextPage = 1;

		this.getDistribuicao();

		if (this.disclaimers.findIndex(x => x.field == 'NZZ_DTREC') > -1) {
			this.isDisableBtnAtalho = true;
			this.hasHidePeriodo = false;
			this.tooltipAtalhoData = this.litDistribuicao.filtroDistri.tooltipPeriodo;
		}

		this.formFiltrar.patchValue({
			palavraChave: '',
			atalhoData: '',
			dataInicial: '',
			dataFinal: '',
		});

		if (this.disclaimers.length == 0) {
			this.formFiltrar.patchValue({ status: '' });
		}

		this.disclaimers.forEach(item => {
			if (item.field == 'NZZ_STATUS') {
				listAux.push(item.value[0]);
			}
		});

		if (listAux.length < formStatus.length) {
			this.formFiltrar.patchValue({
				status: listAux,
			});
		}
	}

	/**
	 * Função responsável por buscar as distribuições
	 */
	getDistribuicao(isLimpaListDistri: boolean = false) {
		this.nQtdDistribuicao = 0;
		this.hideLoadingDistr = false;
		this.isDisableBtnExclui = true;
		let isRet: boolean = false;

		this.fwModel.restore();
		this.fwModel.setModelo('JURA279');
		this.fwModel.setPageSize(this.nPageSize.toString());
		this.fwModel.setPage(this.nRegIndex.toString());

		isRet = this.setFilter();

		if (isRet) {
			this.fwModel.get('Busca distribuicoes').subscribe(data => {
				if (this.nRegIndex == 1 || isLimpaListDistri) {
					this.listDistribuicao = [];
				}

				if (data.total > 0) {
					data.resources.forEach(item => {
						let itemDistr = new Distribuicao();
						itemDistr.pk = item.pk;
						itemDistr.cajuri = this.juriGenerics.findValueByName(
							item.models[0].fields,
							'NZZ_CAJURI'
						);
						itemDistr.processo = this.juriGenerics.findValueByName(
							item.models[0].fields,
							'NZZ_NUMPRO'
						);
						itemDistr.autor = this.juriGenerics.findValueByName(
							item.models[0].fields,
							'NZZ_AUTOR'
						);
						itemDistr.reu = this.juriGenerics.findValueByName(
							item.models[0].fields,
							'NZZ_REU'
						);
						itemDistr.dtDistrib = this.juriGenerics.makeDate(
							this.juriGenerics.findValueByName(item.models[0].fields, 'NZZ_DTDIST'),
							'yyyy-mm-dd'
						);
						itemDistr.dtReceb = this.juriGenerics.makeDate(
							this.juriGenerics.findValueByName(item.models[0].fields, 'NZZ_DTREC'),
							'yyyy-mm-dd'
						);
						itemDistr.ocorrencia = this.juriGenerics.findValueByName(
							item.models[0].fields,
							'NZZ_OCORRE'
						);
						itemDistr.vlrCausa = this.juriGenerics.findValueByName(
							item.models[0].fields,
							'NZZ_VALOR'
						);
						itemDistr.dtProxAudi = this.juriGenerics.makeDate(
							this.juriGenerics.findValueByName(item.models[0].fields, 'NZZ_DTAUDI'),
							'yyyy-mm-dd'
						);

						itemDistr.erroImportacao = this.juriGenerics.changeUndefinedToEmpty(
							this.juriGenerics.findValueByName(item.models[0].fields, 'NZZ_ERRO')
						);

						itemDistr.status = this.juriGenerics.findValueByName(
							item.models[0].fields,
							'NZZ_STATUS'
						);

						if (itemDistr.status === '1' && itemDistr.erroImportacao !== '') {
							itemDistr.status = '4';
						}

						itemDistr.distriEdit = ['verDetalhes'];

						if (itemDistr.status == '2') {
							itemDistr.distriEdit.push('openProcesso');
						} else if (itemDistr.status != '3') {
							itemDistr.distriEdit.push('excluir');
						}

						this.listDistribuicao.push(itemDistr);
					});

					this.nQtdDistribuicao = data.total;

					if (
						this.listDistribuicao.findIndex(x => x.status == '1' || x.status == '4') >
						-1
					) {
						this.isDisableBtnExclui = false;
					}
				}

				this.setShowMoreAndamento(data.total);
				this.hideLoadingDistr = true;
			});
		} else {
			this.hideLoadingDistr = true;
			this.listDistribuicao = [];
			this.nQtdDistribuicao = 0;
		}
	}

	/**
	 * Função que seta filtros do disclaimer na requisição da JURA279.
	 */
	setFilter() {
		let isRet: boolean = false;
		this.hasFiltro = false;

		if (this.disclaimers.length > 0) {
			let palavraChave: string = '';
			let status: string = '';
			let periodo: string = '';

			this.disclaimers.forEach(item => {
				switch (item.field) {
					case 'palavraChave':
						palavraChave += palavraChave === '' ? item.value : '|' + item.value;
						break;

					case 'NZZ_STATUS':
						if (item.value === '4') {
							status +=
								status === ''
									? "((NZZ_ERRO > '' and NZZ_STATUS='1') "
									: " OR (NZZ_ERRO > '' and NZZ_STATUS='1') ";
						} else {
							status +=
								status === ''
									? "(NZZ_STATUS = '" + item.value + "'"
									: " OR NZZ_STATUS = '" + item.value + "'";
						}
						break;

					case 'NZZ_DTREC':
						if (item.value[0] != '' && item.value[1] != '') {
							periodo +=
								" NZZ_DTREC >= '" +
								item.value[0] +
								"'" +
								" AND NZZ_DTREC <= '" +
								item.value[1] +
								"'";
						} else if (item.value[0] != '' && item.value[1] == '') {
							periodo += " NZZ_DTREC >= '" + item.value[0] + "'";
						} else if (item.value[0] == '' && item.value[1] != '') {
							periodo += " NZZ_DTREC <= '" + item.value[1] + "'";
						}
						break;
				}
			});

			if (palavraChave != '') {
				this.fwModel.setQueryParam('palavraChave', palavraChave);
			}

			if (status != '') {
				this.fwModel.setFilter(status + ')');
			}

			if (periodo != '') {
				this.fwModel.setFilter(periodo);
			}

			isRet = true;
			this.hasFiltro = true;
		}

		return isRet;
	}

	/**
	 * Valid que habilita o botão de adicionar filtro dos períodos.
	 */
	validBtnAdicFiltro() {
		let formulario: FiltroDistribuicao = this.formFiltrar.value;
		this.isDisableBtnAdicao = true;

		if (
			this.juriGenerics.changeUndefinedToEmpty(formulario.dataInicial) != '' ||
			this.juriGenerics.changeUndefinedToEmpty(formulario.dataFinal) != ''
		) {
			this.isDisableBtnAdicao = false;
		}

		return this.isDisableBtnAdicao;
	}

	/**
	 * Função que faz remoção e adição dos status no disclaimer.
	 */
	validStatus() {
		let formStatus: Array<string> = this.formFiltrar.value.status;
		let listAux: Array<Status> = [];
		let nCount: number = 0;
		let nIndex: number = -1;

		if (formStatus.length > 0) {
			this.disclaimers.forEach(item => {
				if (item.field == 'NZZ_STATUS') {
					let discStatus: Status = new Status();

					discStatus.value = item.value[0];
					discStatus.count = nCount;
					listAux.push(discStatus);
				}
				nCount++;
			});

			nCount = 0;
			if (listAux.length > formStatus.length) {
				listAux.forEach(dados => {
					nIndex = formStatus.findIndex(x => x == dados.value);
					if (nIndex == -1) {
						nCount = dados.count;
						this.disclaimers.splice(nCount, 1);
					}
				});
			} else {
				this.adicionaDisclaimer();
			}
		} else {
			nIndex = this.disclaimers.findIndex(x => x.field == 'NZZ_STATUS');

			if (nIndex > -1) {
				this.disclaimers.splice(nIndex, 1);
			} else {
				this.adicionaDisclaimer();
			}
		}
	}

	/**
	 * Incrementa o index de paginação da tabela de distribuição
	 * Seta se o botão de carregar mais continua habilitado ou não
	 * @param total Total de distribuição
	 */
	setShowMoreAndamento(total: number) {
		this.nTotalDistribuicao = total;

		if (this.nRegIndex == 1) {
			this.nRegIndex = this.nPageSize + 1;
		} else {
			this.nRegIndex += this.nPageSize;
		}

		if (this.nRegIndex <= total) {
			this.isDisabledShowMore = false;
		} else {
			this.isDisabledShowMore = true;
		}
	}

	/**
	 * Ação do botão Carregar mais resultados
	 * Se for clicado pela 3a vez, carrega o restante dos dados, independente da quantidade
	 */
	showMore() {
		this.nNextPage++;
		//se for clicado pela 3a vez carrega o restante dos dados
		if (this.nNextPage == 3) {
			this.nPageSize = this.nTotalDistribuicao - this.listDistribuicao.length;
		}

		this.isDisabledShowMore = true;
		this.getDistribuicao();
	}

	/**
	 * Função que faz abertura do processo
	 * @param codPro - Código interno do processo
	 */
	openProcesso(codPro: string) {
		let filPro: string = window.localStorage.getItem('filial');

		if (filPro == null) {
			filPro = window.localStorage.getItem('filLog');
		}

		window.open(
			'/processo/' + encodeURIComponent(btoa(filPro)) + '/' + encodeURIComponent(btoa(codPro))
		);
	}

	/**
	 * Função que faz a abertura de mais detalhes
	 * @param pk - Primary Key das distribuição
	 */
	verDetalhes(pk: string) {
		this.router.navigate(['./detalhe/' + pk], { relativeTo: this.route });
	}

	/**
	 * Função que adiciona o default do disclaimer.
	 */
	defaultDisclaimer() {
		let disclaimerItem: DisclaimerItemStruct = new DisclaimerItemStruct();
		let label: string = '';

		label = this.poI18nPipe.transform(this.litDistribuicao.disclaimer.status, [
			this.listStatus[0].label,
		]);

		disclaimerItem.label = label;
		disclaimerItem.field = 'NZZ_STATUS';
		disclaimerItem.value = '1';
		this.disclaimers = [...this.disclaimers, disclaimerItem];
	}

	/**
	 * Responsável por excluir distribuição
	 * @param value - Conteúdo da distribuição selecionada na tabela.
	 */
	deleteDistr(value?: Distribuicao) {
		let msgError: string = '';
		let nIndex: number = -1;
		let labelStatus: string = '';
		this.nRegIndex = 1;
		this.nNextPage = 1;
		this.fwModel.restore();
		this.fwModel.setModelo('JURA279');

		//Quando vem da ação do botão para excluir os selecionados
		if (value == undefined) {
			if (this.listDistribuicao.length > 0) {
				this.listDistribuicao.forEach(item => {
					if (item['$selected']) {
						if (item.status == '1') {
							this.fwModel
								.put(item.pk, this.bodyDelete(item.pk), 'Exclusão de distribuicao')
								.subscribe(data => {
									if (this.juriGenerics.changeUndefinedToEmpty(data) != '') {
										msgError = this.poI18nPipe.transform(
											this.litDistribuicao.tableDistri.sucExcluir,
											[item.processo]
										);

										this.poNotification.success(msgError);
									} else {
										msgError = this.poI18nPipe.transform(
											this.litDistribuicao.tableDistri.erroExcluir,
											[item.processo]
										);
										this.poNotification.error(msgError);
									}
								});
						} else {
							nIndex = this.listStatus.findIndex(x => x.value == item.status);
							labelStatus = this.listStatus[nIndex].label;
							msgError = this.poI18nPipe.transform(
								this.litDistribuicao.tableDistri.vldExclusao,
								[item.processo, labelStatus]
							);

							this.poNotification.error(msgError);
						}

						this.getDistribuicao(true);
					}
				});
			}
		} else {
			//Quando é acionado pelo icone da linha posicionada
			if (value.status == '1') {
				this.fwModel
					.put(value.pk, this.bodyDelete(value.pk), 'Exclusão de distribuicao')
					.subscribe(data => {
						if (this.juriGenerics.changeUndefinedToEmpty(data) != '') {
							msgError = this.poI18nPipe.transform(
								this.litDistribuicao.tableDistri.sucExcluir,
								[value.processo]
							);

							this.poNotification.success(msgError);
						} else {
							this.poNotification.error(this.litDistribuicao.tableDistri.erroExcluir);
						}
						this.getDistribuicao(true);
					});
			} else {
				nIndex = this.listStatus.findIndex(x => x.value === value.status);
				labelStatus = this.listStatus[nIndex].label;
				msgError = this.poI18nPipe.transform(this.litDistribuicao.tableDistri.vldExclusao, [
					value.processo,
					labelStatus,
				]);

				this.poNotification.error(msgError);
				this.getDistribuicao(true);
			}
		}
	}

	/**
	 * Monta o body para o put de distribuicao - Exclusão
	 * @param pk - Chave unica da distribuição
	 */
	bodyDelete(pk: string) {
		let fwModelBody: FwModelBody = new FwModelBody('JURA279', 4, pk);

		fwModelBody.addModel('NZZMASTER', 'FIELDS', 1);
		fwModelBody.models[0].addField('NZZ_STATUS', '3', 0, 'C');

		return fwModelBody.serialize();
	}

	/**
	 * Função responsável por redimencionar a pagina de acordo com a resolução.
	 */
	onHeightTela(isPageSize: boolean = false) {
		this.nHeightMonitor = window.innerHeight;

		if (this.nHeightMonitor > 660) {
			this.nHeightMonitor -= 435;

			if (isPageSize) {
				this.nPageSize = 8;
			}
		} else {
			this.nHeightMonitor -= 390;
		}
	}

	/**
	 * Envia os registros selecionados para importação
	 *
	 */
	importarSelecionados() {
		const listImport: Array<Array<string>> = [];

		this.listDistribuicao.forEach(item => {
			if (item.selected) {
				listImport.push([item.pk, item.processo]);
			}
		});

		this.router.navigateByUrl('/Distribuicao/importacao', {
			state: listImport,
		});
	}

	/**
	 * Marca registro como selecionados
	 *
	 * @param row - Linha do grid.
	 */
	markReg(row: any) {
		this.canImport = true;

		this.listDistribuicao.forEach(item => {
			if (item.pk === row.pk) {
				item.selected = true;

				if (item.status !== '1' && item.status !== '4') {
					this.canImport = false;
				}
			}
		});
	}

	/**
	 * Marca registro como não selecionados
	 *
	 * @param row - Linha do grid.
	 */
	unmarkReg(row: any) {
		this.canImport = false;

		this.listDistribuicao.forEach(item => {
			if (item.pk === row.pk) {
				item.selected = false;
			}

			if (item.selected && (item.status === '1' || item.status === '4')) {
				this.canImport = true;
			}
		});
	}

	/**
	 * Marca todos os registros como selecionados
	 *
	 */
	markAllReg() {
		this.canImport = true;

		this.listDistribuicao.forEach(item => {
			item.selected = true;

			if (item.status !== '1' && item.status !== '4') {
				this.canImport = false;
			}
		});
	}

	/**
	 * Marca todos os registros como não selecionados
	 *
	 */
	unmarkAllReg() {
		this.canImport = false;
		this.listDistribuicao.forEach(item => {
			item.selected = false;

			if (item.selected && (item.status === '1' || item.status === '4')) {
				this.canImport = true;
			}
		});
	}
}
