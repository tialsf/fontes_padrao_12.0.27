import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { PoModule } from '@po-ui/ng-components';
import { SharedModule } from '../shared/shared.module';
import { UploadInterceptor } from '../upload.interceptor';
import { DetDistribuicaoComponent, SafePipe } from './det-distribuicao/det-distribuicao.component';
import { DistribuicaoRoutingModule } from './distribuicao-routing.module';
import { DistribuicaoComponent } from './distribuicao.component';
import { HttpClientModule } from '@angular/common/http';
import { ModImportacaoComponent } from './mod-importacao/mod-importacao.component';

@NgModule({
	declarations: [
		DetDistribuicaoComponent,
		DistribuicaoComponent,
		ModImportacaoComponent,
		SafePipe,
	],
	imports: [
		CommonModule,
		DistribuicaoRoutingModule,
		PoModule,
		FormsModule,
		ReactiveFormsModule,
		SharedModule,
		HttpClientModule,
	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: UploadInterceptor,
			multi: true,
		},
	],
})
export class DistribuicaoModule {}
