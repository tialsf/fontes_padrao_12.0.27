import { modais } from '../../processo/alt-processo/alt-processo-pt';
import { PoComboOption } from '@po-ui/ng-components';

const breadcrumbItem = [
	{ label: 'Home', link: '/' },
	{ label: 'Distribuições', link: '/Distribuicao' },
	{ label: 'Importação' },
];

const breadcrumb = {
	item: breadcrumbItem,
};

const buttons = {
	back: 'Voltar',
	save: 'Salvar',
	import: 'Importar',
};

const fields = {
	assuntoJuridico: 'Assunto jurídico',
	modelo: 'Selecionar modelo',
	tipoAudiencia: 'Tipo de audiência',
	tipoAudienciaTooltip:
		'Informe o tipo que será utilizado para a criação automática do follow-up de audiência',
	respAudiencia: 'Responsável audiência',
	respAudienciaTooltip:
		'Informe o responsável que será utilizado para a criação automática do follow-up de audiência',
	dataEntrada: 'Data de entrada',
	areaJuridica: 'Área',
	centroCusto: 'Centro de Custo',
	unidade: 'Unidade',
	escritorio: 'Escritório credenciado',
	gerente: 'Gerente',
	responsavel: 'Responsável',
	estagiario: 'Estágiario',
	natureza: 'Natureza',
	tipoAcao: 'Tipo de ação',
	assunto: 'Assunto/Objeto',
	rito: 'Rito',
	detalhamento: 'Detalhamento',
	detalhamentoHelp: '(limite de 250 caracteres)',
	detalhamentoPlaceHolder: 'Detalhes do processo',
	instancia: 'Instância',
	comarca: 'Comarca',
	foro: 'Foro',
	vara: 'Vara',
	valorCausa: 'Valor da causa',
	moedaBR: 'R$',
	formaCorrecao: 'Forma de correção',
};

const listInstancia: Array<PoComboOption> = [
	{ label: '1a Instância', value: '1' },
	{ label: '2a Instância', value: '2' },
	{ label: 'Tribunal Superior', value: '3' },
];

export const modImportacaoPt = {
	title: 'Importação de distribuição',
	loading: 'Carregando...',
	msgNotStarted:
		'Para iniciar o cadastro, é necessário informar os dados acima.' +
		' Podendo aplicar um modelo pré-cadastrado',
	msgErroImport: 'Há campo obrigatório que não foi preenchido. Verifique!',
	alertTile: 'Importação de distribuições',
	alertMsg:
		'A importação será executada em segundo plano. ' +
		'Você receberá uma notificação avisando o término do processamento.',
	breadcrumb,
	buttons,
	fields,
	modais,
	listInstancia,
};
