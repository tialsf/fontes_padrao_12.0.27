import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModImportacaoComponent } from './mod-importacao.component';

describe('ModImportacaoComponent', () => {
  let component: ModImportacaoComponent;
  let fixture: ComponentFixture<ModImportacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModImportacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModImportacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
