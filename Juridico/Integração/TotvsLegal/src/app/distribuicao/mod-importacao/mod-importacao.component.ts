import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
	PoI18nService,
	PoBreadcrumb,
	PoNotificationService,
	PoDynamicFormField,
	PoComboOption,
	PoDialogService,
} from '@po-ui/ng-components';
import {
	FwmodelAdaptorService,
	adaptorReturnStruct,
} from 'src/app/services/fwmodel-adaptor.service';
import { LegalprocessService } from 'src/app/services/legalprocess.service';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { JurconsultasService, ExtraField } from 'src/app/services/jurconsultas.service';
import { GetDetModelsAdapterService } from 'src/app/services/jurconsultas-adapter.service';
import {
	lpGetDistribuicaoAdapterService,
	lpGetForoAdapterService,
	lpGetVaraAdapterService,
} from 'src/app/services/legalprocess-adapter.service';
import {
	TpTarefaService,
	ParticipanteAdapterService,
	NomeEnvolvidoAdapterService,
	NaturezaJuridicaAdapterService,
	ComarcasAdapterService,
	TpAcaoAdapterService,
	UnidadeAdapterService,
	AreaJuridicaAdapterService,
	CustoAdapterService,
	EscritorioAdapterService,
	AssuntoAdapterService,
	RitoAdapterService,
	FCorrecaoAdapterService,
} from 'src/app/services/combos-fwmodel-adapter.service';
import { ModImportacaoForm, BodyPost } from './mod-importacao.struct';
import { SelectOptions } from 'src/app/processo/alt-processo/alt-processo.component';

@Component({
	selector: 'app-mod-importacao',
	templateUrl: './mod-importacao.component.html',
	styleUrls: ['./mod-importacao.component.css'],
})
export class ModImportacaoComponent implements OnInit {
	formProcesso: FormGroup;
	litModImport: any;

	isDisableSelTpAssJur: boolean = true;
	disabletpAcao: boolean = true;
	isLoadingCadastro: boolean = true;
	lIniciar: boolean = true;
	isHideLoadCmpAdic: boolean = true;
	hasExtraFields: boolean = false;
	bLoadingIncProcess: boolean = false;
	isDisableForo: boolean = true;
	isDisableVara: boolean = true;

	listExtraFields: Array<ExtraField> = [];
	listTipoAssunto: Array<SelectOptions> = [];
	listDistribuicao: Array<Array<string>> = [];
	listDetModelo: Array<Array<string | number>> = [];

	listInstancia: Array<PoComboOption> = [];

	oValores: object = {};

	public breadcrumb: PoBreadcrumb = { items: [] };

	constructor(
		private router: Router,
		private formBuilder: FormBuilder,
		protected poI18nService: PoI18nService,
		private poNotification: PoNotificationService,
		private poDialog: PoDialogService,
		private legalprocess: LegalprocessService,
		private JuriGenerics: JurigenericsService,
		private jurConsultas: JurconsultasService,
		public getDetModelsAdapter: GetDetModelsAdapterService,
		public comarcasAdapterService: ComarcasAdapterService,
		public fwModelAdaptorService: FwmodelAdaptorService,
		public distrAdaptorService: lpGetDistribuicaoAdapterService,
		public participanteAdapterService: ParticipanteAdapterService,
		public tpTarefaService: TpTarefaService,
		public nomeEnvAdapterService: NomeEnvolvidoAdapterService,
		public naturezaAdapterService: NaturezaJuridicaAdapterService,
		public tipoAcaoAdapterService: TpAcaoAdapterService,
		public unidadeAdapterService: UnidadeAdapterService,
		public areaJuridicaAdapterSerive: AreaJuridicaAdapterService,
		public custoAdapterService: CustoAdapterService,
		public escritorioAdapterService: EscritorioAdapterService,
		public assuntoAdapterService: AssuntoAdapterService,
		public ritoAdapterService: RitoAdapterService,
		public fCorrecaoAdapterService: FCorrecaoAdapterService,
		public foroAdapterService: lpGetForoAdapterService,
		public varaAdapterService: lpGetVaraAdapterService
	) {
		if (this.router.getCurrentNavigation().extras.hasOwnProperty('state')) {
			this.router.getCurrentNavigation().extras.state.forEach(element => {
				this.listDistribuicao.push(element);
			});
		} else {
			this.router.navigateByUrl('/Distribuicao', {});
		}

		/**
		 * Chamada do serviço poI18n para buscar as constantes a partir do contexto 'processo' no arquivo JurTraducao.
		 */
		poI18nService
			.getLiterals({
				context: 'modImportacao',
				language: poI18nService.getLanguage(),
			})
			.subscribe(litModImport => {
				this.litModImport = litModImport;

				this.listInstancia = this.litModImport.listInstancia;

				/**
				 * seta as constantes para tradução do titulo da BreadCrumb
				 */
				this.breadcrumb.items = this.litModImport.breadcrumb.item;
			});
	}

	ngOnInit() {
		this.lIniciar = false;
		this.getTipoAssuntoJuridico();
		this.createForm(new ModImportacaoForm());
		this.loadExtraFields();
	}

	/**
	 * Responsável por  inicializar os Formulários de cadastro de Processo
	 * @param modImp estrutura de campos do formulario de importacao
	 */
	createForm(modImp: ModImportacaoForm) {
		this.formProcesso = this.formBuilder.group({
			/**
			 * Campos complementares
			 */
			assuntoJuridico: [modImp.assuntoJuridico, Validators.compose([Validators.required])],
			modelo: [modImp.modelo],
			tipoAudiencia: [modImp.tipoAudiencia],
			respAudiencia: [modImp.respAudiencia],
			/**
			 * Processo
			 */
			areaJuridica: [modImp.areaJuridica, Validators.compose([Validators.required])],
			centroCusto: [modImp.centroCusto],
			unidade: [modImp.unidade, Validators.compose([Validators.required])],
			escritorio: [modImp.escritorio],
			gerente: [modImp.gerente, Validators.compose([Validators.required])],
			responsavel: [modImp.responsavel],
			estagiario: [modImp.estagiario],
			natureza: [modImp.natureza, Validators.compose([Validators.required])],
			tipoAcao: [modImp.tipoAcao, Validators.compose([Validators.required])],
			assunto: [modImp.assunto],
			rito: [modImp.rito],
			detalhamento: [modImp.detalhamento],
			instancia: [modImp.instancia],
			comarca: [modImp.comarca],
			foro: [modImp.foro],
			vara: [modImp.vara],
			formaCorrecao: [modImp.formaCorrecao],
		});
	}

	/**
	 * Busca os dados da distribuição selecionada pelo usuário e seta as informações nos Forms
	 * @param value - Código do modelo selecionado
	 */
	getDadosModelo(value: string) {
		this.isLoadingCadastro = false;

		if (value !== undefined) {
			/**
			 * GET para buscar dados do modelo
			 */
			this.jurConsultas.restore();
			this.jurConsultas.setParam('searchKey', value);
			this.jurConsultas.get('getTemplate', 'Obtem os campos do modelo').subscribe(data => {
				this.listDetModelo = [];

				if (data.length > 0) {
					this.listDetModelo = data.detModel;
					const comarca: string = this.getFieldValue('NUQ_CCOMAR') as string;
					const foro: string = this.getFieldValue('NUQ_CLOC2N') as string;
					const natureza: string = this.getFieldValue('NUQ_CNATUR') as string;

					this.foroAdapterService.setComarca(comarca);
					this.varaAdapterService.setComarca(comarca);
					this.varaAdapterService.SetForo(foro);

					this.formProcesso.patchValue({
						natureza,
						comarca,
					});

					setTimeout(() => {
						this.formProcesso.patchValue({
							areaJuridica: this.getFieldValue('NSZ_CAREAJ'),
							centroCusto: this.getFieldValue('NSZ_CCUSTO'),
							unidade:
								this.getFieldValue('NSZ_CCLIEN') +
								this.JuriGenerics.separador +
								this.getFieldValue('NSZ_LCLIEN'),
							escritorio:
								this.getFieldValue('NUQ_CCORRE') +
								this.JuriGenerics.separador +
								this.getFieldValue('NUQ_LCORRE'),
							gerente: this.getFieldValue('NSZ_SIGLA1'),
							responsavel: this.getFieldValue('NSZ_SIGLA2'),
							estagiario: this.getFieldValue('NSZ_SIGLA3'),
							tipoAcao: this.getFieldValue('NUQ_CTIPAC'),
							assunto: this.getFieldValue('NSZ_COBJET'),
							rito: this.getFieldValue('NSZ_CRITO'),
							detalhamento: this.getFieldValue('NSZ_DETALH'),
							instancia: this.getFieldValue('NUQ_INSTAN'),
							foro,
							vara: this.getFieldValue('NUQ_CLOC3N'),
							formaCorrecao: this.getFieldValue('NSZ_CFCORR'),
						});
					}, 600);

					this.listExtraFields.forEach(field => {
						this.oValores = Object.assign(this.oValores, {
							[field.property]: this.getFieldValue(field.property),
						});
					});

					this.isLoadingCadastro = true;
				}
			});
		} else {
			this.isLoadingCadastro = true;
		}
	}

	/**
	 * Busca os assuntos juridicos vinculados ao usuario, e que tenham instancia (NUQ)
	 */
	getTipoAssuntoJuridico() {
		this.legalprocess.restore();
		this.legalprocess
			.get('listTpAssuntoJur', 'Busca os assuntos juridicos ')
			.subscribe(data => {
				if (this.JuriGenerics.changeUndefinedToEmpty(data.TpAssJur) !== '') {
					data.TpAssJur.forEach(tipoAssJur => {
						const assuntosJuridicos = new SelectOptions();

						assuntosJuridicos.value = tipoAssJur.value;
						assuntosJuridicos.label = tipoAssJur.label;

						this.listTipoAssunto.push(assuntosJuridicos);
					});
				}
			});
	}
	/**
	 * Verifica se o assunto foi selecionado, se sim, permite que continue a inclusão, se não, barra qualquer ação
	 * @param tpAssJuri assunto selecionado
	 */

	setTipoAssuntoJur(tpAssJuri: string) {
		window.sessionStorage.setItem('assuntoJuri', tpAssJuri);

		if (this.JuriGenerics.changeUndefinedToEmpty(tpAssJuri) !== '') {
			this.isDisableSelTpAssJur = false;
			this.lIniciar = true;
		} else {
			this.isDisableSelTpAssJur = true;
		}
	}

	/**
	 * Função que faz a criação dos campos no dynamic form.
	 */
	loadExtraFields() {
		const listCamposDet: Array<string> = [
			'NSZ_CAREAJ',
			'NSZ_CCUSTO',
			'NSZ_CCLIEN',
			'NSZ_LCLIEN',
			'NUQ_CCORRE',
			'NUQ_LCORRE',
			'NSZ_SIGLA1',
			'NSZ_SIGLA2',
			'NSZ_SIGLA3',
			'NUQ_CNATUR',
			'NUQ_CTIPAC',
			'NSZ_COBJET',
			'NSZ_CRITO',
			'NSZ_DETALH',
			'NUQ_INSTAN',
			'NUQ_CCOMAR',
			'NUQ_CLOC2N',
			'NUQ_CLOC3N',
			'NSZ_CFCORR',
		];

		this.isHideLoadCmpAdic = false;
		this.listExtraFields = [];

		this.jurConsultas.getExtraFields(listCamposDet).subscribe((list: Array<ExtraField>) => {
			if (list.length) {
				this.hasExtraFields = true;
			}
			this.listExtraFields = list;
			this.isHideLoadCmpAdic = true;
		});
	}

	/*
	 * Chama GET do Legal Process que retorna a lista de FOROS
	 */
	buscaListaForos(codComarca: string) {
		this.foroAdapterService.setComarca(codComarca);

		if (this.JuriGenerics.changeUndefinedToEmpty(codComarca) === '') {
			this.formProcesso.patchValue({ foro: '', vara: '' });
			this.isDisableForo = true;
			this.isDisableVara = true;
		} else {
			this.formProcesso.patchValue({ foro: this.formProcesso.value.foro });
		}

		if (this.JuriGenerics.changeUndefinedToEmpty(this.formProcesso.value.comarca) !== '') {
			this.isDisableForo = false;
		}
	}

	/*
	 * Chama GET do Legal Process que retorna a lista de varas
	 */
	buscaListaVaras(codForo: string) {
		const codComarca: string = this.formProcesso.value.comarca;

		this.varaAdapterService.setComarca(codComarca);
		this.varaAdapterService.SetForo(codForo);

		if (this.JuriGenerics.changeUndefinedToEmpty(codForo) === '') {
			this.formProcesso.patchValue({ vara: '' });
		} else {
			this.formProcesso.patchValue({ vara: this.formProcesso.value.vara });
		}

		if (this.JuriGenerics.changeUndefinedToEmpty(this.formProcesso.value.comarca) !== '') {
			this.isDisableForo = false;
		}

		if (this.JuriGenerics.changeUndefinedToEmpty(this.formProcesso.value.foro) !== '') {
			this.isDisableVara = false;
		}
	}

	/**
	 * Chama a função para filtrar o tipo de ação de acordo com a natureza selecionada
	 */
	filtraTipoAcao(selectedOption: adaptorReturnStruct) {
		this.formProcesso.patchValue({ tipoAcao: '' });
		this.disabletpAcao = selectedOption === undefined || selectedOption === null;

		if (selectedOption !== undefined && selectedOption !== null) {
			const origem: string = this.JuriGenerics.findValueByName(
				selectedOption.getExtras(),
				'NQ1_TIPO'
			);
			this.tipoAcaoAdapterService.setFiltroP(`NQU_ORIGEM = '${origem}'`);
		}
	}

	/**
	 * Chama o método POST para importar a distribuição
	 */
	submitImportacao() {
		if (this.beforeSubmitProcess()) {
			const body: string = this.GetBodyPost();

			this.isLoadingCadastro = false;

			this.legalprocess.restore();
			this.legalprocess
				.post('distribuicoes/', body, 'importar distribuicoes')
				.subscribe(data => {
					if (data !== undefined || data.result === 'Processando') {
						this.poDialog.alert({
							title: this.litModImport.alertTile,
							message: this.litModImport.alertMsg,
							ok: () => this.router.navigateByUrl('/Distribuicao', {}),
						});
						this.isLoadingCadastro = true;
					} else {
						this.isLoadingCadastro = true;
					}
				});
		}
	}

	/**
	 * Responsável por fazer validações antes de comitar o processo
	 */
	beforeSubmitProcess(): boolean {
		let bRet: boolean = true;

		if (!this.formProcesso.valid) {
			bRet = false;
			this.poNotification.error(this.litModImport.msgErroImport);
		}

		return bRet;
	}

	/**
	 * Monta o body que será utilizado na importação das distribuições
	 */
	GetBodyPost(): string {
		const form = this.formProcesso.value;
		const bodyPost: BodyPost = {
			tipoAssunto: form.assuntoJuridico,
			tipoAudiencia: form.tipoAudiencia,
			respAudiencia: form.respAudiencia,
			distribuicoes: this.listDistribuicao,
			detModelo: [],
		};

		this.setFieldValue('NSZ_CAREAJ', form.areaJuridica);
		this.setFieldValue('NSZ_CCUSTO', form.centroCusto);
		this.setFieldValue(
			'NSZ_CCLIEN',
			this.formProcesso.value.unidade.substring(
				0,
				this.formProcesso.value.unidade.indexOf(this.JuriGenerics.separador)
			)
		);
		this.setFieldValue(
			'NSZ_LCLIEN',
			this.formProcesso.value.unidade.substring(
				this.formProcesso.value.unidade.indexOf(this.JuriGenerics.separador) +
					this.JuriGenerics.separador.length
			)
		);
		this.setFieldValue(
			'NUQ_CCORRE',
			this.formProcesso.value.escritorio.substring(
				0,
				this.formProcesso.value.escritorio.indexOf(this.JuriGenerics.separador)
			)
		);
		this.setFieldValue(
			'NUQ_LCORRE',
			this.formProcesso.value.escritorio.substring(
				this.formProcesso.value.escritorio.indexOf(this.JuriGenerics.separador) +
					this.JuriGenerics.separador.length
			)
		);

		this.setFieldValue('NSZ_SIGLA1', form.gerente);
		this.setFieldValue('NSZ_SIGLA2', form.responsavel);
		this.setFieldValue('NSZ_SIGLA3', form.estagiario);
		this.setFieldValue('NUQ_CNATUR', form.natureza);
		this.setFieldValue('NUQ_CTIPAC', form.tipoAcao);
		this.setFieldValue('NSZ_COBJET', form.assunto);
		this.setFieldValue('NSZ_CRITO', form.rito);
		this.setFieldValue('NSZ_DETALH', form.detalhamento);
		this.setFieldValue('NUQ_INSTAN', form.instancia);
		this.setFieldValue('NUQ_CCOMAR', form.comarca);
		this.setFieldValue('NUQ_CLOC2N', form.foro);
		this.setFieldValue('NUQ_CLOC3N', form.vara);
		this.setFieldValue('NSZ_CFCORR', form.formaCorrecao);

		/**
		 * Adiciona os campos extras no body
		 */

		// tslint:disable-next-line: forin -- comentário necessário para o tslint ignorar o bloco abaixo
		for (const campos in this.oValores) {
			const nPosCampoExt: number = this.listExtraFields.findIndex(x => x.property === campos);
			if (nPosCampoExt > -1) {
				const cTypeForm: string = this.listExtraFields[nPosCampoExt].type;

				let value: string | number;

				if (cTypeForm === 'boolean') {
					value = this.oValores[campos] ? 'T' : 'F';
				} else {
					value = this.oValores[campos];
				}

				this.setFieldValue(campos, value, cTypeForm);
			}
		}

		bodyPost.detModelo = this.listDetModelo;

		return JSON.stringify(bodyPost);
	}

	/**
	 * Busca o campo e formata o valor de acordo com o tipo
	 * @param field campo
	 * @param type tipo
	 */
	getFieldValue(field: string = '', type: string = 'C') {
		let fieldValue: string | number = type === 'C' ? '' : 0;
		let index: number = 0;

		index = this.listDetModelo.findIndex(x => x[0] === field);
		if (index > -1) {
			fieldValue = this.listDetModelo[index][2];
		}
		if (type === 'D') {
			fieldValue = this.JuriGenerics.makeDate(fieldValue as string, 'yyyy-mm-dd');
		} else if (type === 'N') {
			fieldValue = parseFloat(fieldValue as string);
		}

		return fieldValue;
	}

	/**
	 * Seta o dado de acordo com o tipo
	 * @param field asd
	 * @param value asd
	 * @param type asd
	 */
	setFieldValue(field: string = '', value: string | number = '', type: string = 'C') {
		let index: number = 0;

		const model: string =
			field.substring(0, 3) === 'NSZ' ? 'NSZMASTER' : `${field.substring(0, 3)}DETAIL`;

		if (type === 'D') {
			value = (value as string).replace(/-/g, '');
		} else if (type === 'N') {
			value = parseInt(value as string);
		}

		index = this.listDetModelo.findIndex(x => x[0] === field);
		if (index > -1) {
			this.listDetModelo[index][2] = value;
		} else {
			const detModelo = [field, type, value, model];
			this.listDetModelo.push(detModelo);
		}
	}

	/**
	 * Redireciona para a página Home caso o usuário selecione o botão de 'Cancelar'
	 */
	goBack() {
		this.router.navigateByUrl('/Distribuicao');
	}
}
