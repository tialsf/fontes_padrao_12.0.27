export class ModImportacaoForm {
	assuntoJuridico: string = '';
	modelo: string = '';
	tipoAudiencia: string = '';
	respAudiencia: string = '';
	areaJuridica: string = '';
	centroCusto: string = '';
	unidade: string = '';
	escritorio: string = '';
	gerente: string = '';
	responsavel: string = '';
	estagiario: string = '';
	natureza: string = '';
	tipoAcao: string = '';
	assunto: string = '';
	rito: string = '';
	detalhamento: string = '';
	instancia: string = '';
	comarca: string = '';
	foro: string = '';
	vara: string = '';
	formaCorrecao: string = '';
}

export interface BodyPost {
	tipoAssunto: string;
	tipoAudiencia: string;
	respAudiencia: string;
	distribuicoes: Array<Array<string>>;
	detModelo: Array<Array<string | number>>;
}
