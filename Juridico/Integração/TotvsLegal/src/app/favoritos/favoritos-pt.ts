/**
* Arquivo de constantes para tradução de Favoritos - Português
*/

//-- Constantes para títulos das colunas do Grid de Favoritos
export const gridFavoritos = {
	
	colunaProcesso: "Processos",
	colunaMovimentacao: "Movimentações",
	colunaData: "Data",
	colunaFav: " "
}


//-- Constantes para o breadcrumb de Favoritos
export const breadcrumbFavoritos = {

	brHome: "Meus processos",
	brFav: "Meus favoritos",
	brHomeContratos: "Meus Contratos",
	brHomeConsultivo: "Minhas Consultas"
}


//-- Constantes para o componente de Favoritos
export const favoritosPt = {	
	
	loadFav: "Carregando Favoritos",
	favoritos: "Meus favoritos",
	RemoverFav: "Deseja remover favorito?",
	favExcluido: "Favorito excluído com sucesso.",
	anterior: "<<  Anterior",
	proximo: "Próximo  >>",
	finalPagina: "Ops! Não há mais registros para serem exibidos.",
	gridFavorito: gridFavoritos,
	breadcrumbFav: breadcrumbFavoritos
}



