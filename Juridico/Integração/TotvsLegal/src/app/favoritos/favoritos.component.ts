import { Component, OnInit } from '@angular/core';
import { FwmodelService } from '../services/fwmodel.service';
import { JurigenericsService } from '../services/jurigenerics.service';
import { PoI18nService } from '@po-ui/ng-components';
import { PoDialogConfirmLiterals, PoDialogService } from '@po-ui/ng-components';
import { PoTableColumn, PoNotificationService, PoBreadcrumb } from '@po-ui/ng-components';
import { TLUserInfo } from "../services/authlogin.service";
import { RoutingService } from '../services/routing.service';
import { Router } from '@angular/router';
import { LegalprocessService } from '../services/legalprocess.service';

class ItemFavorito {
	processo:     string;
	movimentacao: string;
	date:         string;
	filial:       string;
	fav:          string;
	cajuri:       string;
	tpAssunto:    string;
}

@Component({
	selector: 'app-favoritos',
	templateUrl: './favoritos.component.html',
	styleUrls: ['./favoritos.component.css']
})
export class FavoritosComponent implements OnInit {

	page: number = 0;
	searchKey: string = '';
	hasData: boolean = true;
	length: number = 0;
	pageSize: number = 20;
	maxPage: number = 1;
	btnPreDisable: boolean = false;
	btnNxtDisable: boolean = false;
	isHideLoadingTabFavoritos: boolean = false;
	litFav: any;
	dateStr: string;
	literalsConfirm: PoDialogConfirmLiterals;
	message: string;
	title: string;
	dialogMethod: string;
	literals: string;
	actionOptions: Array<string> = ["confirm", "cancel"];
	hasPage: boolean = true;
	lastPage: number = 1;
	filial: string = '';
	codPro: string = '';
	tpAss: string ='';

	colFavoritos: Array<PoTableColumn> = [
		{
			property: 'processo',
			color: 'color-01',
			type: 'link',
			action: (value, row) => {
				this.filial = row.filial;

				/** Proteção feita para ambientes compartilhado, com isso, a abertura do processo
				*   é feita respeitando o tamanho do campo filial.
				*   O item da 'filial' do localStorage é inserida na função MostraFilial da Home.
				*/
				if (row.filial == "" && window.localStorage.getItem('filial').trim() == "") {
					this.filial = window.localStorage.getItem('filial');
				}

				this.openProcesso(this.filial, row.cajuri);
			}
		},
		{ property: 'movimentacao', label: ' ',
			type: 'link',
			action: (value, row) => {
				this.filial = row.filial;

				/** Proteção feita para ambientes compartilhado, com isso, a abertura do processo
				*   é feita respeitando o tamanho do campo filial.
				*   O item da 'filial' do localStorage é inserida na função MostraFilial da Home.
				*/
				if (row.filial == "" && window.localStorage.getItem('filial').trim() == "") {
					this.filial = window.localStorage.getItem('filial');
				}

				this.openProcesso(this.filial, row.cajuri);
			} 
		},
		{ property: 'date', label: ' ' ,
			type: 'link',
			action: (value, row) => {
				this.filial = row.filial;

				/** Proteção feita para ambientes compartilhado, com isso, a abertura do processo
				*   é feita respeitando o tamanho do campo filial.
				*   O item da 'filial' do localStorage é inserida na função MostraFilial da Home.
				*/
				if (row.filial == "" && window.localStorage.getItem('filial').trim() == "") {
					this.filial = window.localStorage.getItem('filial');
				}

				this.openProcesso(this.filial, row.cajuri);
			}
		},
		{ property: 'fav', label: ' ', type: 'icon', action: this.confirmDel.bind(this), icons: [{ value: 'favorito', icon: 'po-icon-star-filled', color: '#0d729c' }] }
	];

	listFavoritos: Array<ItemFavorito> = [];

	public breadcrumb: PoBreadcrumb;

	constructor(
		private fwmodel: FwmodelService,
		public thfNotification: PoNotificationService,
		private userInfo: TLUserInfo,
		private thfAlert: PoDialogService,
		private thfI18nService: PoI18nService,
		private legalprocess: LegalprocessService,
		private JuriGenerics: JurigenericsService,
		private routingState: RoutingService,
		private router: Router) 
	{
		this.routingState.loadRouting();
		//-- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'fav' no arquivo JurTraducao.			
		thfI18nService.getLiterals({ context: 'fav', language: thfI18nService.getLanguage() })
			.subscribe((litFav) => {
				this.litFav = litFav;

				//-- seta as constantes para tradução do titulo das colunas do Grid de Favoritos
				this.colFavoritos[0].label = this.litFav.gridFavorito.colunaProcesso;
				this.colFavoritos[1].label = this.litFav.gridFavorito.colunaMovimentacao;
				this.colFavoritos[2].label = this.litFav.gridFavorito.colunaData;
				this.colFavoritos[3].label = this.litFav.gridFavorito.colunaFav;

				//-- seta as constantes para tradução do titulo das colunas do Grid de Provisões
				switch (this.router.url) {
					case "/processos/favoritos":
						this.breadcrumb= { items: [
											{ label: ' ', link: '/home' },
											{ label: ' ' }]	
									};

						this.breadcrumb.items[0].label = this.litFav.breadcrumbFav.brHome;
						break;
					case "/contratos/favoritos":
						this.breadcrumb= { items: [
											{ label: ' ', link: '/homeContratos' },
											{ label: ' ' }]	
						};

						this.breadcrumb.items[0].label = this.litFav.breadcrumbFav.brHomeContratos;
						break;

					case "/consultivo/favoritos":
						this.breadcrumb= { items: [
											{ label: ' ', link: '/homeConsultivo' },
											{ label: ' ' }]	
										};
						this.breadcrumb.items[0].label = this.litFav.breadcrumbFav.brHomeConsultivo;
						break;
				}

				this.breadcrumb.items[1].label = this.litFav.breadcrumbFav.brFav;

				switch (this.router.url) {
					case "/processos/favoritos":
						this.tpAss = "001";
						break;

					case "/contratos/favoritos":
						this.tpAss = "006";
						break;

					case "/consultivo/favoritos":
						this.tpAss = "001";
						break;
				}
		});
	}

	ngOnInit() {
		this.page = 1;
		this.searchKey = ""
		this.getFavoritos(this.tpAss);
	}

	openProcesso(filPro : string, codPro: string){
		window.open('processo/' + encodeURIComponent(btoa(filPro)) + "/" + encodeURIComponent(btoa(codPro)));
	}

	//Responsável por trazer os FAVORITOS de cordo com o tipo de assunto jurídico
	getFavoritos(tpAssunto) {
		this.listFavoritos = [];
		this.legalprocess.restore();
		this.legalprocess.setFilter("tipoAssjur", tpAssunto)
		this.legalprocess.setPageSize('5');
		this.isHideLoadingTabFavoritos = false;
		
		this.legalprocess.get("listFavorite","Lista de Favoritos").subscribe(data => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data.listFavorite) != ''){
				data.listFavorite.forEach(item => {
					const favoritos = new ItemFavorito;

					favoritos.date         = this.JuriGenerics.makeDate( this.JuriGenerics.changeUndefinedToEmpty( item.date), "dd/mm/yyyy");
					favoritos.movimentacao = item.movimentacao;
					favoritos.cajuri       = item.cajuri;
					favoritos.fav          = item.fav;
					favoritos.filial       = item.filial;
					favoritos.processo     = item.partes;
					favoritos.tpAssunto    = tpAssunto;

					this.listFavoritos.push(favoritos)

					this.isHideLoadingTabFavoritos = true;

				});
			} else { 
				this.isHideLoadingTabFavoritos = true;
			}

			this.setMaxPage();
		})
	}

	//-- Dialog Confirm de Favoritos
	confirmDel(row) {
		this.thfAlert.confirm({
			literals: this.literalsConfirm,
			title: this.litFav.favoritos,
			message: this.litFav.RemoverFav,
			confirm: () => this.delFavoritos(row)
		});
	}

	//- Responsável por remover um processo favoritado
	delFavoritos(row) {
		let pkFavorito: string = '';
		let tpAssunto:  string = row.tpAssunto;
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA269");
		this.fwmodel.setFilter("O0V_FILCAJ = '" + row.filial + "'");
		this.fwmodel.setFilter("O0V_CAJURI = '" + row.cajuri + "'");
		this.fwmodel.get("Busca tarefas").subscribe((data) => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != '') {
				pkFavorito = data.resources[0].pk
			
				this.fwmodel.deleteFavoritos(pkFavorito).subscribe((data) => {
					if (data) {
						this.thfNotification.success(this.litFav.favExcluido);
						this.getFavoritos(tpAssunto);
					}
				});
			}
		})
	}


	/**
	* ação dos botões de paginação Anterior/ Próximo
	*/
	showMore(buttonId: string){
		this.movePage(buttonId);
	}

	

	/**
	* ação dos botões Anterior/ Próximo
	*/
	movePage(buttonId: string) {

		this.setMaxPage();
		if (buttonId == "next" && this.page < this.maxPage) {
			this.page++;
			this.pagination();
		} else if (buttonId == "previous" && this.page >= 2) {
			this.page--;
			this.pagination();
		}

		this.pagination();
	}

	/**
	* habilita/ desabilita os botões de paginação
	*/
	pagination() {

		this.btnPreDisable = false;
		this.btnNxtDisable = false;

		if (this.page == this.maxPage) {
			this.btnNxtDisable = true;
		}
		if (this.page == 1) {
			this.btnPreDisable = true;
		}
	}

	/**
	* calcula a quantidade máxima de páginas 
	*/
	setMaxPage() {

		this.maxPage = this.length / this.pageSize
		if ((this.maxPage - Math.floor(this.maxPage)) > 0) {
			this.maxPage = Math.floor(this.maxPage) + 1;
		}

		this.pagination();
	}

	//-- Alert para o final da página
	alertFav() {
		this.thfAlert.alert({
			title: this.litFav.favoritos,
			message: this.litFav.finalPagina,
			ok: () => ""
		});

	}

	
}
