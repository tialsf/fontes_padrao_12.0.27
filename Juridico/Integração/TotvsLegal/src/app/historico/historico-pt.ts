/**
* Arquivo de constantes para tradução da tela de Histórico de Alterações
*/

//--  Constantes para o breadcrumb de Processo
const breadcrumb = {
	homeProcesso:  "Meus processos",
	processo:      "Meu processo",
	historico:     "Histórico de alterações",
	contrato:      'Meu contrato',
	homeContrato:  'Meus contratos'
}

const formHistorico = {
	dataInicial :          "Data Inicial", 
	dataFinal :            "Data Final",
	usuarios :             "Usuários",
	campo :                "Campo"
}

const listViewHistorico = {
	data:            "Data",
	hora:            "Hora",
	campo:           "Campo",
	valorAntes:      "Valor antes",
	valorDepois:     "Valor depois",
	usuario:         "Usuário",
	nomeCampo:       "Título Campo",
	descricao:       "Descrição",
	naoHaDetalhes:   "não há detalhes a serem exibidos",
	altProcesso:     "Alteração de processo",
	altEncerramento: "Alteração de processo encerrado",
	altContrato:     "Alteração de contrato",
	altContratoEnc:  "Alteração de contrato encerrado",
	altCorresp:      "Alteração de correpondente"
}

//-- Constantes principais
export const historicoPt = {
	bread :                breadcrumb,
	formHist :             formHistorico,
	listView :             listViewHistorico,
	btnConsultar :         "Consultar",
	titResultado :         "Resultado de pesquisa",
	loadResult:            "Carregando histórico",
	erroValidData:         "A data inicial precisa ser maior do que a data final."
}