import { Component, OnInit, ViewChild} from '@angular/core';
import { 
	PoComboComponent,
	PoBreadcrumb, 
	PoNotificationService, 
	PoI18nService
} from '@po-ui/ng-components';
import { FwmodelService } from 'src/app/services/fwmodel.service';
import { LegalprocessService } from 'src/app/services/legalprocess.service';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RoutingService } from 'src/app/services/routing.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { 
	HistoricoAdapterService,
	histAdapterFilterParam 
} from 'src/app/services/historico-adapter.service';

class HistoricoResultStruct {
	tipo:string;
	title?: string;
	user : string;
	date : string ;
	time : string ;
	field ?: string;
	fieldName ?: string;
	oldData ?: string;
	newData ?: string;
	description?:string;
}

class Usuarios {
	value: string;
	label: string;
}

@Component({
  selector: 'app-historico',
  templateUrl: './historico.component.html',
  styleUrls: ['./historico.component.css']
})
export class HistoricoComponent implements OnInit {

	litHistorico: any;
	chaveProcesso: string         = '';
	filial: string                = '';
	cajuri: string                = '';
	filialCajuri: string          = '';
	usuario: string               = '';
	perfil: string                = '/processo/';
	breadcrumbPerfil: string      = ''
	breadcrumbHome: string        = ''
	linkHome: string              = ''
	cajuriTitulo: string          = ''
	isAuditTrail: boolean         = false;
	isHideLoadingHist: boolean    = true;
	isProcesso: boolean           = false;
	isDisableUsuarios: boolean    = true;

	listUsuarios: Array<Usuarios> = []
	formHistorico: FormGroup;
	histAdapterParam: histAdapterFilterParam;
	listItensResult: Array<HistoricoResultStruct> = [];

	/**
	 * estrutura de navegação da URL atual
	 */
	public breadcrumb: PoBreadcrumb = {
		items: [
		{ label: ' ', link: '/' },
		{ label: ' ' }
		],
	};

	@ViewChild("cmbCampo"   ,{ static: true }) cmbCampoMonitorado : PoComboComponent;

	constructor(		
		protected thfI18nService: PoI18nService,
		protected thfNotification: PoNotificationService,
		public legalprocess: LegalprocessService,
		public historicoAdapter: HistoricoAdapterService,
		private JuriGenerics: JurigenericsService,
		private fwmodel: FwmodelService,
		private route: ActivatedRoute,
		private routingState: RoutingService,
		private formBuilder: FormBuilder,
		private router: Router,
	) { 
		this.routingState.loadRouting();

		if(this.router.url.indexOf('/contrato/')>-1){
			this.filial = this.route.snapshot.paramMap.get('filContrato');		
			this.chaveProcesso = this.route.snapshot.paramMap.get('codContrato');			
			this.isProcesso = false;
		}else{
			this.filial = this.route.snapshot.paramMap.get('filPro');				
			this.chaveProcesso = this.route.snapshot.paramMap.get('codPro');
			this.isProcesso = true;
		}

		this.cajuri =        atob(decodeURIComponent(this.chaveProcesso));
		this.filialCajuri =  atob(decodeURIComponent(this.filial));
		window.sessionStorage.setItem('filial', atob(this.filial));	

		// Chamada do serviço poI18n para buscar as constantes a partir do contexto 
		// 'historico' no arquivo JurTraducao.
		thfI18nService.getLiterals({ 
			context: 'historico', 
			language: thfI18nService.getLanguage() 
		}).subscribe((litHist) => {
				this.litHistorico = litHist;

			//-- Adiciona o código do cajuri no título Meu processo + área
				this.cajuriTitulo = this.cajuri.replace(new RegExp('^0*', 'g'), '')

				//Verifica se é um processo ou contrato
				if (this.isProcesso){
					this.perfil = '/processo/';
					this.breadcrumbPerfil = this.litHistorico.bread.processo
					this.breadcrumbHome   = this.litHistorico.bread.homeProcesso
					this.linkHome         = '/home'
				}else{
					this.perfil = '/contrato/';
					this.breadcrumbPerfil = this.litHistorico.bread.contrato
					this.breadcrumbHome   = this.litHistorico.bread.homeContrato
					this.linkHome         = '/homeContratos'
				}

				this.breadcrumb.items =[
					{ label: this.breadcrumbHome, link: this.linkHome},
					{ label: this.breadcrumbPerfil, 
						link: this.perfil + this.filial + '/' + this.chaveProcesso 
					},
					{ label: this.litHistorico.bread.historico}
				];
			})
	}

	ngOnInit() {
		this.createFormHistorico(new Historico());
		this.getInfoProcesso();
		this.getTableLog();
		this.getDadosHist();
	}

	/**
	 * Limpa a lista do resultado da pesquisa
	 */
	restore(){
		this.listItensResult = [];
	}

	/**
	 * Responsável por obter os dados inputados
	 * no formulário de pesquisa do Histórico de alterações
	 * @param historico classe Historico
	 ```
	dataInicial : string;
	dataFinal : string;
	usuario : Array<string>     = [];
	campoProcesso : string      = '';
	 */
	createFormHistorico(historico: Historico) {
		this.formHistorico = this.formBuilder.group({
			dataInicial:   [historico.dataInicial   ],
			dataFinal:     [historico.dataFinal     ],
			usuario:       [historico.usuario       ],
			campoProcesso: [historico.campoProcesso ],
		});
	}

	/**
	 * Busca as informações do processo
	 */
	getInfoProcesso() {
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA095');
		this.fwmodel.setFilter("NSZ_COD = '" + this.cajuri + "'");
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.get('getInfoProcesso').subscribe(data => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != '') {
				let descResumoBreadCrumb = []
				if (this.isProcesso){
					descResumoBreadCrumb = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields,"NSZ_DAREAJ")
				}else{
					descResumoBreadCrumb = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields,"NSZ_DESCON")
				}
				this.breadcrumb.items[1].label = this.breadcrumbPerfil + ' ' +
				descResumoBreadCrumb + " (" + this.cajuriTitulo + ")";
			}
		});
	}

	/**
	 * Busca as informações dos Usuarios
	 */
	getHistUsuarios() {
		this.listUsuarios = [];
		if (!this.isAuditTrail) {
			this.legalprocess.restore();
			this.legalprocess.get('process/' + this.filialCajuri + this.cajuri + '/historico', 'Obtem lista de usuários').subscribe(data => {
				if (this.JuriGenerics.changeUndefinedToEmpty(data.listusr) !== '') {
					data.listusr.forEach(itemUser => {
						const itemUsuario = new Usuarios();
						itemUsuario.label = itemUser.user;
						itemUsuario.value = itemUser.user;
						this.listUsuarios.push(itemUsuario);
					});
				}
			});
		} else {
			this.listItensResult.forEach(itemUser => {
				const itemUsuario = new Usuarios();
				itemUsuario.label = itemUser.user;
				itemUsuario.value = itemUser.user;
				this.listUsuarios.push(itemUsuario);
			});
		}
	}

	/**
	 * retorna se a consulta será realizada nas tabelas do Embedded Audit Trail (NSZT10_TTAT_LOG )
	 * ou no Historico de alterações (O0X)
	 */
	getTableLog(){
		this.legalprocess.restore();
		this.legalprocess.get("process/"+ this.filialCajuri + this.cajuri +"/historico","Lista de Filial").
			subscribe(data => {
				if (this.JuriGenerics.changeUndefinedToEmpty(data.log) != '') {
					this.isAuditTrail = data.log;
				}
			});
	};

	/**
	 * Função executada na ação do botão Consultar
	 */
	getFilteredHist(){
		
		this.restore();
		this.isHideLoadingHist = false;
		const dadosHist = this.formHistorico.value;
		this.legalprocess.restore();

		if(dadosHist.dataInicial > dadosHist.dataFinal){
			this.thfNotification.error(this.litHistorico.erroValidData);
		}


		//-- Filtros 
		this.legalprocess.setFilter('dataIni'  ,
			this.JuriGenerics.changeUndefinedToEmpty(dadosHist.dataInicial)
		);
		this.legalprocess.setFilter('dataFinal',
			this.JuriGenerics.changeUndefinedToEmpty(dadosHist.dataFinal)
		);
		this.legalprocess.setFilter('usuario'  ,
			this.JuriGenerics.changeUndefinedToEmpty(dadosHist.usuario)
		);
		this.legalprocess.setFilter('campo'    ,
			this.JuriGenerics.changeUndefinedToEmpty(dadosHist.campoProcesso)
		);

		let filial = '';
		if (this.filialCajuri.trim() !== '') {
			filial = this.filialCajuri.trim();
		} else {
			filial = this.filialCajuri;
        }
		//--GET
		this.legalprocess
			.get("process/" + filial + this.cajuri + "/historico", "getFilteredHist")
			.subscribe(
				data => {
					if (this.JuriGenerics.changeUndefinedToEmpty(data.log) != '') {
						this.restore();
						data.log.forEach(
							(item: HistoricoResultStruct) => {
								item.date = this.JuriGenerics.makeDate(item.date.replace("-", '').replace('-', ''), "dd/mm/yyyy");
								switch (item.tipo) {
									case "1":
										item.title = this.isProcesso ? this.litHistorico.listView.altProcesso : this.litHistorico.listView.altContrato
										break;
								
									case "2":
										item.title = this.isProcesso ? this.litHistorico.listView.altEncerramento : this.litHistorico.listView.altContratoEnc
										break;
								
									case "3":
										item.title = this.litHistorico.listView.altCorresp 
										break;
								
									default:
										item.title = this.isProcesso ? this.litHistorico.listView.altProcesso : this.litHistorico.listView.altContrato
										break;
								}
								this.listItensResult.push(item);
							}
						);
					}
					this.isHideLoadingHist = true;
					this.getHistUsuarios();
					this.isDisableUsuarios = false;
				}
			);

		return this.listItensResult;
	}

	/**
	 * Alimenta os parâmetros do historico-adapter
	 */
	getDadosHist(){
		const dadosHist = this.formHistorico.value;
		this.cmbCampoMonitorado.filterParams = new histAdapterFilterParam
		(
			this.filialCajuri + this.cajuri,
			this.JuriGenerics.changeUndefinedToEmpty(dadosHist.dataInicial), 
			this.JuriGenerics.changeUndefinedToEmpty(dadosHist.dataFinal), 
			this.JuriGenerics.changeUndefinedToEmpty(dadosHist.usuario)
		)
	}

}

export class Historico {
	dataInicial : string;
	dataFinal : string;
	usuario : Array<string>     = [];
	campoProcesso : string      = '';
}
