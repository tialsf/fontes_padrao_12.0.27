import { Component, OnInit, ViewChild } from '@angular/core';
import {
	PoI18nService,
	PoTableColumn,
	PoDialogService,
	PoDialogConfirmLiterals,
	PoNotificationService,
	PoModalComponent,
	PoUploadComponent,
	PoModalAction,
	PoComboComponent
} from '@po-ui/ng-components';
import { FwmodelService, FwModelBody } from '../services/fwmodel.service';
import { JurigenericsService, JurBase64 } from '../services/jurigenerics.service';
import { Title } from '@angular/platform-browser';
import { LegaltaskService } from '../services/legaltask.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JurconsultasService } from '../services/jurconsultas.service';
import { JurconsultasAdapterService } from 'src/app/services/jurconsultas-adapter.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LegalprocessService, sysParamStruct, SysProtheusParam } from '../services/legalprocess.service';
import { lpGetFiliaisAdapterService  } from 'src/app/services/legalprocess-adapter.service';
import { JurJustifModalComponent } from '../shared/jur-justif-modal/jur-justif-modal.component';
import { adaptorFilterParam, FwmodelAdaptorService } from '../services/fwmodel-adaptor.service';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';

class TarefasVencer {
	filial:     string;
	cajuri:     string;
	codTarefa:  string;
	codProcess: string;
	processo:   string;
	tipo:       string;
	data:       string;
	hora:       string;
	status:     string;
	descSatus:  string;
	situacProc: string;
	tarefaEdit: Array<string>;
	descricao:  string;
	assJur:     string;
}

class ContratosVencer {
	filial:   string;
	contrato: string;
	renova:   string;
	valor:    number;
	dataVenc: string;
	cajuri:   string;
}

class ConcluiTarefa {
	tipo:        string = '';
	statusAtual: string = '';
	novoStatus:  string = '';
    codTar:      string = '';
	cajuri:      string = '';
	filial:      string = ''; 
	descricao:   string = '';
}

class ItemFavorito {
	partes:       string;
	movimentacao: string;
	date:         string;
	filial:       string;
	fav:          string;
	cajuri:       string;
}

@Component({
  selector: 'app-home-contratos',
  templateUrl: './home-contratos.component.html',
  styleUrls: ['./home-contratos.component.css']
})
export class HomeContratosComponent implements OnInit {

	litHomeContratos:          any;
	literalsConfirm:           PoDialogConfirmLiterals;

	bFilial:                  boolean;
	isHideLoadingFavoritos:   boolean = false;
	isHideLoadingVencimentos: boolean = false;
	isHideLoadingTarefas:     boolean = false;
	isProcEncerrado:          boolean = false;
	isHideLoadingAndamentos:  boolean = false;
	isHideLoadingNovos:       boolean = false;
	isHideLoadingEncerrados:  boolean = false;
	isHideLoadingGraph:       boolean = false;
	isHideLoadingSLA:         boolean = true;
	isShowMoreDisabled:       boolean = true;
	
	filial:                    string                = '';
	assuntoJuridico:           string                = '';
	searchContract:            string                = ''; //parâmetros pesquisa rápida de processos
	pkTarefa:                  string                = '';
	anoMes:                    string                = '';
	listFavoritos:             Array<any>            = [];
	listTarefas:               Array<any>            = [];	
	listVencimentos:           Array<any>            = [];
	sysParams:                 Array<sysParamStruct> = []
	nQtdeTarefasVencer:        number                = 0;
	nQtdShowTarefa:            number                = 30;
	uploadCount:               number                = 0;
	uploadSuccessCount:        number                = 0;
	nEmAndamento:              number                = 0;
	nNovos:                    number                = 0;
	nEncerrados:               number                = 0;
	nSLADias:                  number                = 0;
	formConcluiComp:           FormGroup;	
	formFiliais:               FormGroup;
	dtAnoMesAtu = new Date();

	//-- Propriedades para o Grafico de Contratos
	public barChartType: ChartType = 'bar';
	public barChartData: ChartDataSets[] = [{data:[]}];
	public barChartLabels: string[] = [];
	public barChartOptions: ChartOptions = {
		responsive: true,
		// -- We use this empty as placeholders for dynamic theming.
		scales: { 
			xAxes: [{
				ticks: { fontColor: 'black'},
				gridLines: { color: 'rgba(40, 3, 252, 0.1)' }
			}], 
			yAxes: [{
				ticks: { fontColor: 'black', beginAtZero: true},
				gridLines: { color: 'rgba(40, 3, 252, 0.1)' }
			}] 
		},
		plugins: { 
			datalabes: { 
				anchor: 'end',
				align: 'end'
			} 
		},
		legend: {display:false}
	};

	// Colunas widget de favoritos
	colFavoritos: Array<PoTableColumn> = [
		{	property: 'partes', color: 'color-01', type: 'link',
			action: (value, row) => {
				this.filial = row.filial;
				/** Proteção feita para ambientes compartilhado, com isso, a abertura do processo
				*   é feita respeitando o tamanho do campo filial.
				*   O item da 'filial' do localStorage é inserida na função MostraFilial da Home.
				*/
				if (row.filial == "" && window.localStorage.getItem('filial').trim() == "") {
					this.filial = window.localStorage.getItem('filial');
				}
				this.openContrato(this.filial, row.cajuri);
			}
		},
		{	property: 'movimentacao', color: 'color-01', type: 'link',
			action: (value, row) => {
				this.filial = row.filial;
				if (row.filial == "" && window.localStorage.getItem('filial').trim() == "") {
					this.filial = window.localStorage.getItem('filial');
				}
				this.openContrato(this.filial, row.cajuri);
			}
		},
		{	property: 'date', color: 'color-01', type: 'link', 
			action: (value, row) => {
				this.filial = row.filial;
				if (row.filial == "" && window.localStorage.getItem('filial').trim() == "") {
					this.filial = window.localStorage.getItem('filial');
				}
				this.openContrato(this.filial, row.cajuri);
			}
		},
		{	property: 'fav', type: 'icon', label: ' ' ,
			action: this.confirmDel.bind(this), 
			icons: [{ 
				value: 'favorito', icon: 'po-icon-star-filled', color: '#0d729c' 
			}] },
	];

	// Colunas widget de provisões
	colVencimentos: Array<PoTableColumn> = [
		{	property: 'contrato' , type: 'link'  , color: 'color-01', width: '50%',
			action: (value, row) => {
				this.filial = row.filial;
				if(row.filial === undefined && window.localStorage.getItem('filial').trim() == ''){
					this.filial = window.localStorage.getItem('filial');
				}
				this.openContrato(this.filial, row.cajuri);
			}
		},
		{ 	property: 'renova'   , type: 'string', format: '1.2-5'  , width: '20%'},
		{ 	property: 'valor'    , type: 'number', format: '1.2-5'  , width: '10%'},
		{ 	property: 'dataVenc' , type: 'link'  , color: 'color-01', width: '2	0%',
			action: (value, row) => {
				this.filial = row.filial;
				if(row.filial === undefined && window.localStorage.getItem('filial').trim() == ''){
					this.filial = window.localStorage.getItem('filial');
				}
				this.openContrato(this.filial, row.cajuri);
			}
		}
	];

	// Colunas widget de tarefas
	colTarefas: Array<PoTableColumn>  = [
		{	property: 'processo', color: 'color-01', type: 'link',
			action: (value, row) => {
				this.filial = row.filial;
				if (row.filial == undefined && window.localStorage.getItem('filial').trim() == "") {
					this.filial = window.localStorage.getItem('filial');
				}
				
				if(row.assJur == '006' ){
					this.openContrato(this.filial, row.cajuri);
				} else {
					this.openProcesso(this.filial, row.cajuri);
				}
			}
		},
		{	property: 'tipo' },
		{	property: 'data', type:"date" },
		{	property: 'hora' },
		{	property: 'tarefaEdit', width: '16%', type: 'icon', label: ' ',
			icons: [
				{	value: 'tarefa'  , icon: 'po-icon-edit', color: '#29b6c5'  , tooltip: 'Editar', 
					action: (value) => {
						this.updateTarefa(value);
					},
				},
				{	value: 'concluir', icon: 'po-icon-ok'  , color: '#color-07', tooltip: 'Concluir', 
					action: (value) => {
						this.openConcluiTarefa(value);
					},
				},
				{	value: 'anexos'  , icon: 'po-icon po-icon-document-filled', color: '#color-07', tooltip: 'Anexos', 
					action: (value) => {
						this.callAnexos(value);
					}
				}
			]
		},

	];
	
	// ações do modal de seleção de filiais
	cancelar: PoModalAction = {
		action: () => { 
			this.modalFiliais.close();
		},
		label: 'Cancelar',
	};

	salvar: PoModalAction = {
		action: () => { this.salvarFilial(); },
		label: 'Salvar',
	};

	// Ações do modal de concluir compromisso
	putConcluirTarefa: PoModalAction = {
		action: () => {
			if (this.JuriGenerics.changeUndefinedToEmpty(this.formConcluiComp.value.novoStatus) != '') {
				this.verifyBaixa();
			} else {
				this.poNotification.warning(this.litHomeContratos.tarefas.modSemStatus);
			}
		},
		label: ' ',
	};

	fechar: PoModalAction = {
		action: () => {
			this.ConcluiModal.close();
		},
		label: ' ',
	};

	// Modais
	@ViewChild('concluiTarefa', { static: true  }) ConcluiModal: PoModalComponent;
	@ViewChild('docUpload',     { static: false }) docUpload:    PoUploadComponent;
	@ViewChild('modJustif',     { static: true  }) modalJustif:  JurJustifModalComponent;
	@ViewChild('comboFilial',   { static: true  }) cmbFilial:    PoComboComponent;
	@ViewChild('filiais',       { static: true  }) modalFiliais: PoModalComponent;

	// Adaptor do status de followup
	adaptorStatusFup: adaptorFilterParam = new adaptorFilterParam("JURA017", "", "NQN_DESC", "NQN_DESC", "NQN_DESC", "NQN_COD", "", "getStatusFup")

	constructor(
		protected poI18nService: PoI18nService,
		private   poDialogService: PoDialogService,
		private   fwmodel: FwmodelService,
		public    poNotification: PoNotificationService,
		private   JuriGenerics: JurigenericsService,
		private   titleService: Title,
		private   legaltask: LegaltaskService,
		private   legalprocess: LegalprocessService,
		private   route: ActivatedRoute,
		private   router: Router,
		private   jurconsultas: JurconsultasService,
		public    jurconsultasAdapter: JurconsultasAdapterService,
		private   sysProtheusParam: SysProtheusParam,
		private   formBuilder: FormBuilder,
		public    fwModelAdaptorService: FwmodelAdaptorService,
		public    filiaisAdapterService : lpGetFiliaisAdapterService
		) {
		
		// Tipo de assunto jurídico Contratos
		this.assuntoJuridico = '006';
		
		// Valida se o usuário possui mais de uma filial ou uma filial,
		// ou então se a base é compartilhada.
		this.mostraFilial();

		//-- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'menu' no arquivo JurTraducao.
		poI18nService.getLiterals({ context: 'homeContratos', language: poI18nService.getLanguage() })
		.subscribe((litHomeContratos) => {
			this.litHomeContratos = litHomeContratos;
			//-- seta as constantes para tradução do titulo das colunas do Grid de Favoritos
			this.colFavoritos[0].label = this.litHomeContratos.favoritos.colunaContrato;
			this.colFavoritos[1].label = this.litHomeContratos.favoritos.colunaMovimentacao;
			this.colFavoritos[2].label = this.litHomeContratos.favoritos.colunaData;
			
			//-- seta as constantes para tradução do titulo das colunas do Grid de Provisões
			this.colVencimentos[0].label = this.litHomeContratos.vencimentos.colunaContrato;
			this.colVencimentos[1].label = this.litHomeContratos.vencimentos.colunaRenova; 
			this.colVencimentos[2].label = this.litHomeContratos.vencimentos.colunaValor;  
			this.colVencimentos[3].label = this.litHomeContratos.vencimentos.colunaData;   
			
			//-- seta as constantes para tradução do titulo das colunas do Grid de Tarefas
			this.colTarefas[0].label = this.litHomeContratos.tarefas.colunaContrato;
			this.colTarefas[1].label = this.litHomeContratos.tarefas.colunaTipo;
			this.colTarefas[2].label = this.litHomeContratos.tarefas.colunaData;
			this.colTarefas[3].label = this.litHomeContratos.tarefas.colunaHora;

			//-- seta as constantes dos modais
			this.fechar.label            = this.litHomeContratos.tarefas.modFechar;
			this.putConcluirTarefa.label = this.litHomeContratos.tarefas.modConcluir;
			this.cancelar.label          = this.litHomeContratos.cancelar;
			this.salvar.label            = this.litHomeContratos.salvar;
		});
	}

	ngOnInit() {
		this.titleService.setTitle(this.litHomeContratos.titulo);
		this.getFavoritos();
		this.getTarefas();
		this.getContratosVenc();
		this.createFormConcluiComp(new ConcluiTarefa);
		this.createFormFiliais();
		// Busca os parâmetros do Protheus
		this.sysProtheusParam.getParamByArray(["MV_JTVENJF"]).subscribe((params) => {
			this.sysParams = params;
		})
		// Monta a string do ano/mês corrente de acompanhamentos
		this.anoMes = 	this.dtAnoMesAtu.getFullYear().toString() +
						this.JuriGenerics.buildMonthDayStr((this.dtAnoMesAtu.getMonth() + 1).toString()) +
						this.JuriGenerics.buildMonthDayStr(this.dtAnoMesAtu.getDate().toString());
		this.anoMes = this.JuriGenerics.makeDate(this.anoMes,"MMMM de yyyy");
		this.getAcompanhamentos();
		this.getChartInfo();
		this.getSLAJuridico();
	}

	/**
	* Responsável por remover favoritado
	*/
	confirmDel(row) {

		this.poDialogService.confirm({
			literals: this.literalsConfirm,
			title: this.litHomeContratos.favoritos.favoritos,
			message: this.litHomeContratos.favoritos.RemoverFav,
			confirm: () => this.delFavoritos(row)
		});
	}

	/**
	 * Responsável por remover um processo favoritado
	 */
	delFavoritos(row) {
		let pkFavorito: string = '';
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA269");
		this.fwmodel.setFilter("O0V_FILCAJ = '" + row.filial + "'");
		this.fwmodel.setFilter("O0V_CAJURI = '" + row.cajuri + "'");
		this.fwmodel.get("Busca tarefas").subscribe((data) => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != '') {
				pkFavorito = data.resources[0].pk
			
				this.fwmodel.deleteFavoritos(pkFavorito).subscribe((data) => {
					if (data) {
						this.poNotification.success(this.litHomeContratos.favoritos.favExcluido);
						this.getFavoritos();
					}
				});
			}
		})
	}

	/**
	 * Responsável por trazer os processos FAVORITOS
	 */
	getFavoritos() {
		this.listFavoritos = [];
		this.legalprocess.restore();
		this.legalprocess.setFilter("tipoAssjur", "006")
		this.legalprocess.setPageSize('5');
		this.isHideLoadingFavoritos = false;
		
		this.legalprocess.get("listFavorite","Lista de Favoritos").subscribe(data => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data.listFavorite) != ''){
				data.listFavorite.forEach(item => {
					const favoritos = new ItemFavorito;
					
					favoritos.date         = this.JuriGenerics.makeDate( this.JuriGenerics.changeUndefinedToEmpty( item.date), "dd/mm/yyyy");
					favoritos.movimentacao = item.movimentacao;
					favoritos.cajuri       = item.cajuri;
					favoritos.fav          = item.fav;
					favoritos.filial       = item.filial;
					favoritos.partes       = item.partes;
					
					this.listFavoritos.push(favoritos);
			});
		}
			this.isHideLoadingFavoritos = true;
		})
	}

	/**
	 * Faz o get de Follow-ups através do legaltask
	 */
	getTarefas() {
		let dataIni: Date = new Date()
		let dataFim: Date = new Date()

		dataIni.setMonth(dataIni.getMonth()-3)
		dataFim.setDate(dataIni.getDate()+7)

		this.legaltask.restore();
		this.isHideLoadingTarefas = false;
		this.legaltask.setFilter('pageSize',this.nQtdShowTarefa.toString());
		this.legaltask.setFilter('dataIni',dataIni.toJSON().substr(0,10).replace(/-/g,'')) 
		this.legaltask.setFilter('dataFim',dataFim.toJSON().substr(0,10).replace(/-/g,''))
		this.legaltask.setFilter('status','1'); 
		this.legaltask.setFilter('lWSTLegal', 'true');
		this.listTarefas = [];
		this.legaltask.get('fup','Busca Tarefas').subscribe((data) => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.data) != ''){
				data.data.forEach(item => {
					let itemTarefa = new TarefasVencer();
					itemTarefa.filial     = this.JuriGenerics.changeUndefinedToEmpty(item.filial);
					itemTarefa.cajuri     = item.cajuri
					itemTarefa.codTarefa  = this.JuriGenerics.changeUndefinedToEmpty(item.idFu);
					itemTarefa.codProcess = this.JuriGenerics.changeUndefinedToEmpty(item.cajuri);
					itemTarefa.processo   = this.JuriGenerics.changeUndefinedToEmpty(item.autor) + 
											" - " + this.JuriGenerics.changeUndefinedToEmpty(item.reu);
					itemTarefa.tipo       = item.tipofu[0].descri;
					itemTarefa.status     = item.result;
					itemTarefa.descSatus  = item.descresul;
					itemTarefa.hora       = item.hora;
					itemTarefa.data       = this.JuriGenerics.makeDate(item.dtfup,"yyyy-mm-dd");
					itemTarefa.situacProc = item.situacao;
					itemTarefa.tarefaEdit = ['tarefa', 'concluir'];
					itemTarefa.descricao = item.desc;
					itemTarefa.assJur     = item.tipoAsjOrig.trim() == '' ? item.tipoasj : item.tipoAsjOrig;

					if (item.documentos.length > 0) {
						itemTarefa.tarefaEdit = ['tarefa', 'concluir', 'anexos'];
					}
					this.listTarefas.push(itemTarefa);
				})
				this.nQtdeTarefasVencer = data.length;
				this.isShowMoreDisabled = data.hasNext === "false";
			}
			this.isHideLoadingTarefas = true;
		})
	}

	/**
	 * Função atribuida ao botão de mostrar mais tarefas pendentes
	 */
	showMoreTarefas(){
		if (this.nQtdShowTarefa === 60){
			this.nQtdShowTarefa = this.nQtdeTarefasVencer
		} else {
			this.nQtdShowTarefa += 30
		}
		this.getTarefas()
	}
	
	/**
	 * Abre a Det-tarefa em modo de Update
	 */
	updateTarefa(value ?: TarefasVencer) {
		if ( value != undefined){
			window.open( (value.assJur == "006" ? 'contrato/' : 'processo/')  + encodeURIComponent(btoa(value.filial)) + '/' + encodeURIComponent(btoa(value.codProcess)) + 
						'/tarefa/' + encodeURIComponent(btoa(value.codTarefa))
			);
		}
	}

	/**
	 * método GET (ListContratos) WS REST JurConsultas
	 * consulta os contratos, seus vencimentos e valores
	 */
	getContratosVenc(){
		this.jurconsultas.restore();
		this.jurconsultas.setParam('pageSize', '5');
		this.jurconsultas.setParam('assJur', '006');
		this.jurconsultas.setParam('situac', '1')

		this.jurconsultas.get('contracts','getContratosVenc').subscribe((data) => {
			this.isHideLoadingVencimentos = false;
			if(this.JuriGenerics.changeUndefinedToEmpty(data.contracts) != ''){
				data.contracts.forEach(contract => {
					const itemContrato = new ContratosVencer();
					itemContrato.contrato = contract.description;
					if(contract.renew === "1"){
						itemContrato.renova = this.litHomeContratos.vencimentos.sim;
					}else{
						itemContrato.renova = this.litHomeContratos.vencimentos.nao;
					}
					itemContrato.valor = contract.value;
					itemContrato.dataVenc = this.JuriGenerics.makeDate(contract.due, 'dd/mm/yyyy');
					itemContrato.cajuri = contract.id;
					itemContrato.filial = contract.branch;

					this.listVencimentos.push(itemContrato);
				});
			};
			this.isHideLoadingVencimentos = true;
		});
	};

	/**
	 * Ação do onSelect da pesquisa rápida de processos
	 */
	onSelectSearchedContract() {
		if(this.JuriGenerics.changeUndefinedToEmpty(this.searchContract) != '' ){
			this.openContrato(atob(this.searchContract).substring(0 , atob(this.searchContract).indexOf("/")),
								atob(this.searchContract).substring(atob(this.searchContract).indexOf("/") + 1 ),
			);
		}
	}
		/**
	 * Função responsável por realizar Download do relatório através do navegador
	 * em Excel
	 */
	exportarExcel(){
		this.legaltask.restore();
		this.legaltask.setFilter('status','1') 
		this.legaltask.setFilter('periFilt','5') //Filtro para os proximos 7 dias
		this.legaltask.get('exportTarefa','Exportação de tarefas a vencer').subscribe((fileData)=>{
			if (this.JuriGenerics.changeUndefinedToEmpty(fileData.export) != '' ) {
				fileData.export.forEach(item => {
					var blob = JurBase64.toUTF8(item.filedata, 'application/octet-stream');
					var fileLink = document.createElement('a');

					fileLink.href = window.URL.createObjectURL(blob);;
					fileLink.download = item.namefile; //força o nome do download
					fileLink.target = "_blank"; //força a abertura em uma nova página
					document.body.appendChild(fileLink);
					fileLink.click(); //gatilha o evento do click
					this.poNotification.success(this.litHomeContratos.tarefas.msgExportaSucess)
				})
			} else {
				this.poNotification.error(this.litHomeContratos.tarefas.msgExportaError);
			}
		})

	}
	/**
	 * Função responsável por realizar Download do relatório através do navegador,
	 * relatorio de pauta de compromisso - PDF
	 */
	exportarPDF(){
		this.legaltask.restore();
		this.legaltask.setFilter('periFilt','5') //Filtro para os proximos 7 dias
		this.legaltask.setFilter('status','1') //Pendentes
		this.legaltask.get('exportPauta','Relatório de pauta - PDF').subscribe((fileData)=>{
			if (this.JuriGenerics.changeUndefinedToEmpty(fileData.pauta) != '' ) {
				fileData.pauta.forEach(item => {
					var blob = JurBase64.toUTF8(item.filedata, 'application/octet-stream');
					var fileLink = document.createElement('a');

					fileLink.href = window.URL.createObjectURL(blob);;
					fileLink.download = item.namefile; //força o nome do download
					fileLink.target = "_blank"; //força a abertura em uma nova página
					document.body.appendChild(fileLink);
					fileLink.click(); //gatilha o evento do click
					this.poNotification.success(this.litHomeContratos.tarefas.msgExportaSucess)
				})
			} else {
				this.poNotification.error(this.litHomeContratos.tarefas.msgExportaError);
			}
		})
	}

	/**
	 * rota de navegação
	 */
	openFavoritos(){
		this.router.navigate(["contratos/favoritos"])
	}

	/**
	 * Abre a tela de cadastro do contrato
	 */
	callNewContrato(){
		this.router.navigate(["contrato/cadastro"])
	}

	openAcompEmAndamento(){
		this.router.navigate(['acompanhamento'], { queryParams: { page: '1' , assJur: '006'} })
	}

	openAcompNovos(){
		this.router.navigate(['acompanhamento'], { queryParams: { page: '2' , assJur: '006'} })
	}

	openAcompaEncerrados(){
		this.router.navigate(['acompanhamento'], { queryParams: { page: '3' , assJur: '006'} })
	}

	openSLA(){
		this.router.navigate(['slaJuridico'])
	}

	/**
	 * Abre a tela de anexos
	 */
	callAnexos(value?: any) {
		if ( value != undefined){

			window.open( (value.assJur == "006" ? 'contrato/' : 'processo/') + encodeURIComponent(btoa(value.filial)) + '/' + encodeURIComponent(btoa(value.codProcess)) + 
						'/tarefa/' + encodeURIComponent(btoa(value.codTarefa)) + '/anexos');
		}
	}

	openContrato(filContrato: string, codContrato: string){
		window.open('contrato/' + encodeURIComponent(btoa(filContrato)) +
			'/' + encodeURIComponent(btoa(codContrato))
		);
	}

	openProcesso(filContrato: string, codContrato: string){
		window.open('processo/' + encodeURIComponent(btoa(filContrato)) +
			'/' + encodeURIComponent(btoa(codContrato))
		);
	}

	/**
	 * Abre modal para concluir a tarefa
	 * @param value 
	 */
	openConcluiTarefa(value?: TarefasVencer) {

		this.formConcluiComp.patchValue({
			tipo: value.tipo,
			statusAtual: value.descSatus,
			codTar: value.codTarefa,
			cajuri: value.codProcess,
			filial: value.filial,
			situacao: value.situacProc,
			novoStatus: '',
			descricao: value.descricao

		})
		this.getPKTarefa(value)
		this.ConcluiModal.open();
	}

	/**
	 * Busca a pk da tarefa que está sendo concluida para a alteração ser feita via modelo
	 * @param value dados da linha selecionada
	 */
	getPKTarefa(value?: TarefasVencer) {
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA106");
		this.fwmodel.setFilter("NTA_CAJURI = '" + value.codProcess + "'");
		this.fwmodel.setFilter("NTA_COD = '" + value.codTarefa + "'");
		this.fwmodel.setFilter("NTA_FILIAL = '" + value.filial + "'");
		this.fwmodel.get("Busca tarefas").subscribe((data) => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != '') {
				this.pkTarefa = data.resources[0].pk
			}
		})
		if(value.situacProc == '2'){
			this.isProcEncerrado = true;
		}

	}

	/**
	 * validações antes do commit
	 */
	verifyBaixa() {
		if (this.beforeSubmitBaixa()) {
			this.baixaTask();
		}
	}

		/**
	 * Verifica se o processo esta encerrado se sim, apresenta a modal de justificativa
	 * @param value 
	 */
	beforeSubmitBaixa(value?: TarefasVencer) {
		let isSubmitable: boolean = false;

		isSubmitable = this.formConcluiComp.valid;
		if (isSubmitable) {
	
			if (this.isProcEncerrado && this.sysProtheusParam.getParamValueByName(this.sysParams, "MV_JTVENJF") == "1") {
				isSubmitable = false;
				this.modalJustif.openModal(this.formConcluiComp.value.cajuri.toString(),
					() => {
						if (this.modalJustif.isOk) {
							this.baixaTask();
						}
					});
			} else {
				isSubmitable = true;
			}
		}
		return isSubmitable
	}

	/**
	 * Baixa a tarefa
	 */
	baixaTask() {
		this.ConcluiModal.primaryAction.loading = true;
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA106');
		this.fwmodel.put(this.pkTarefa, this.bodybaixaTask()).subscribe((data) => {
			if (data == "") {
				this.poNotification.error(this.litHomeContratos.tarefas.modErro);
			} else {
				//Valida se há documentos a serem enviados
				if (this.docUpload.hasOwnProperty('currentFiles') && this.docUpload.currentFiles.length > 0) {
					this.uploadCount = this.docUpload.currentFiles.length;
					this.sendDocs();
				} else {
					this.poNotification.success(this.litHomeContratos.tarefas.modSucesso);
					this.ConcluiModal.close();
					this.getTarefas();
				}
			}
			this.ConcluiModal.primaryAction.loading = false;
		})
	}	
	
	/**
	* Monta o body para o put de tarefas
	*/
	bodybaixaTask() {
		const fwModelBody: FwModelBody = new FwModelBody("JURA106",4,this.pkTarefa,"NTAMASTER")

		fwModelBody.models[0].addField("NTA_CRESUL", this.formConcluiComp.value.novoStatus, undefined, "C")
		fwModelBody.models[0].addField("NTA_DESC"  , this.formConcluiComp.value.descricao , undefined, "C")

		return fwModelBody.serialize()
	}

	/**
	 * Realiza o envio dos documentos
	 */
	sendDocs() {
		if (this.docUpload.currentFiles.length > 0) {
			this.docUpload.sendFiles();
		}
	}

	/**
	 * form para concluir tarefa pendente
	 * @param concl 
	 */
	createFormConcluiComp(concl: ConcluiTarefa) {

		this.formConcluiComp = this.formBuilder.group({
			tipo:        [concl.tipo],
			statusAtual: [concl.statusAtual],
			novoStatus:  [concl.novoStatus, Validators.compose([Validators.required])],
			codTar:      [concl.codTar],
			cajuri:      [concl.cajuri],
			filial:      [concl.filial],
			descricao:   [concl.descricao]
		})
	}

	/**
	 * Chama o SelectFiles do po-upload
	 */
	getDocuments() {
		this.docUpload.selectFiles()
	}

	/**
	 * Carrega as informações para o POST do upload de arquivos
	 * @param file
	 */
	uploadFiles(file) {
		file.data = {
			cajuri: this.formConcluiComp.value.cajuri.toString(),
			entidade: "NTA",
			codEntidade: this.formConcluiComp.value.codTar.toString()
		};
	}

	/**
	 * Trata para redirecionar após concluir todos os anexos
	 */
	uploadSucess() {
		this.uploadSuccessCount++;

		if (this.uploadSuccessCount == this.docUpload.currentFiles.length) {
			this.poNotification.success(this.litHomeContratos.tarefas.modSucesso);
			this.ConcluiModal.close();
			this.docUpload.clear();
			this.getTarefas();
		}
		this.ConcluiModal.primaryAction.loading = false;
	}

	/**
	 * Cria o form para seleção de Filiais
	 */
	createFormFiliais(){
		this.formFiliais = this.formBuilder.group({
			filialCont:    ["", Validators.compose([Validators.required])]
		});
		this.cmbFilial.selectedValue = window.localStorage.getItem('filial');
	}

	/**
	 * Responsável por solicitar ao usuário a filial do cadastro do processo
	 */
	verificaFiliaisDoUsuario(){
		if(this.bFilial == undefined){
			setTimeout(() => { this.verificaFiliaisDoUsuario(); }, 500);
		} else {
			if (this.bFilial) {
				this.modalFiliais.open();
				this.formFiliais.patchValue({ filialCont: window.localStorage.getItem('filial') });
			} else {
				this.callNewContrato();
			}
		}
	}

	/**
	 * Responsável por armazenar a filial selecionada
	 */
	salvarFilial(){
		window.localStorage.setItem('filial', this.cmbFilial.selectedOption.value.toString() );
		window.sessionStorage.setItem('filial', this.cmbFilial.selectedOption.value.toString() );
		this.modalFiliais.close();
		this.callNewContrato();
	}

	/**
	 * Valida se a filial será apresentada.
	 */
	mostraFilial(){
		this.legalprocess.restore()
		this.legalprocess.get("listFiliaisUser","Lista de Filial").subscribe(itemFiliais => {
			if (itemFiliais.length > 1){
				this.bFilial = true;
			} else {
				if (itemFiliais.length == 1) {
					window.localStorage.setItem('filial', itemFiliais.User[0].value.toString() );
				}

				this.bFilial = false;
			}

			return itemFiliais
		});
	}

	/**
	 * método GET (ListContratos) WS REST JurConsultas
	 * consulta os contratos, seus vencimentos, andamentos e valores
	 */
	getAcompanhamentos(){
		this.jurconsultas.restore();
		this.jurconsultas.setParam('assJur', '006');
		this.jurconsultas.setParam('situac', '1');
		this.jurconsultas.setParam('tpFilter', '1');
		this.isHideLoadingAndamentos = false;
		this.jurconsultas.get('contracts','getAcompanhamentos').subscribe((data) => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data.contracts) != ''){
				this.nEmAndamento = data.length;
			};
			this.isHideLoadingAndamentos = true;
		});

		this.jurconsultas.restore();
		this.jurconsultas.setParam('assJur', '006');
		this.jurconsultas.setParam('tpFilter', '2');
		this.isHideLoadingNovos = false;
		this.jurconsultas.get('contracts','getAcompanhamentos').subscribe((data) => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data.contracts) != ''){
				this.nNovos = data.length;
			};
			this.isHideLoadingNovos = true;
		});

		this.jurconsultas.restore();
		this.jurconsultas.setParam('assJur', '006');
		this.jurconsultas.setParam('situac', '2');
		this.jurconsultas.setParam('tpFilter', '3');
		this.isHideLoadingEncerrados = false;
		this.jurconsultas.get('contracts','getAcompanhamentos').subscribe((data) => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data.contracts) != ''){
				this.nEncerrados = data.length;
			};
			this.isHideLoadingEncerrados = true;
		});
	}

	/**
	 * método GET (graficoInfoContratos) WS REST JurConsultas
	 * consulta os valores dos contratos em andamento por tipo
	 */
	getChartInfo(){
		this.jurconsultas.restore();
		this.isHideLoadingGraph = false;
		this.jurconsultas.get('contractChartData','getChartInfo').subscribe((data) => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data.tipos) != ''){
				let valor: number[] = [];
				const colorList: string[] = 
					[	'rgba(255, 99, 132, ',
						'rgba(54, 162, 235, ',
						'rgba(255, 206, 86, ',
						'rgba(75, 192, 192, ',
						'rgba(153, 102, 255,',
						'rgba(255, 159, 64, '
					];
				let count: number = 0;
				let backGroundColor: string[] = [];
				let borderColor: string[] = [];
				let hoverBackgroundColor: string[] = [];
				let hoverBorderColor: string[] = [];
				data.tipos.forEach(item => {
					this.barChartLabels.push(item.name);
					valor.push(item.value);
					backGroundColor.push(colorList[count]+'0.2)');
					borderColor.push(colorList[count]+'1)');
					hoverBackgroundColor.push(colorList[count]+'0.5)')
					hoverBorderColor.push(colorList[count]+'1)')
					count++;
					if(count >= 6){
						count = 0;
					}
				});
				this.barChartData = [
					{data: valor,
					backgroundColor: backGroundColor,
					borderColor: borderColor,
					hoverBackgroundColor: hoverBackgroundColor,
					hoverBorderColor: hoverBorderColor,
					borderWidth: 1,
					label: this.litHomeContratos.grafico.labelDataSet}
				]
			};
			this.isHideLoadingGraph = true;
		})
	}

	/**
	 * método GET (slaDias) WS REST JurConsultas
	 * consulta o SLA em dias
	 */
	getSLAJuridico(){
		this.isHideLoadingSLA = false;
		this.jurconsultas.restore();
		this.jurconsultas.setParam('assJur', '006');
		this.jurconsultas.get('slaDias','getSLAJuridico').subscribe((data) => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data.dias) != ''){
				this.nSLADias = data.dias;
			}
			this.isHideLoadingSLA = true;
		})
	}

	/**
	 * Função responsável por fazer a abertura do painel de atividades
	 */
	openPainelTarefa(){
		window.open("PainelAtividades")
	}
}