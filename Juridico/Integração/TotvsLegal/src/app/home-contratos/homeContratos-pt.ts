/**
* Arquivo de constantes para tradução da Home de Contratos - Português
*/

//-- Constantes para a widget de Favoritos
export const favoritos = {
	
	meusFav:              "Meus Favoritos",
	verFav:               "Ver todos os meus favoritos",
	loadFav:              "Carregando Favoritos",
	favoritos:            "Favoritos",
	favExcluido:          "Favorito excluído com sucesso.",
	editar:               "Editar",
	concluir:             "Consluir",
	anexos:               "Anexos",
	RemoverFav:            "Deseja remover o contrato de favoritos?",
	// Colunas 
	colunaContrato:       "Contrato",
	colunaMovimentacao:   "Movimentações",
	colunaData:           "Data"

}

//-- Constantes para a widget de Provisões
export const vencimentos = {

	vencimentos:          "Próximos vencimentos de contratos",
	loadVenc:             "Carregando Vencimentos",
	sim:                  "Sim",
	nao:                  "Não",
	// Colunas
	colunaContrato:       "Contrato",
	colunaRenova:         "Renovação automática?",
	colunaValor:          "Valor (R$)",
	colunaData:           "Data"

}

//-- Constantes para a widget de Tarefas
export const tarefas = {	
	
	tarefasVencer:        "Minhas tarefas pendentes (7 dias)",
	painelAtividades:     "Painel de atividades",
	exportarExcel:        "Exportar para excel",
	exportarPDF:          "Exportar para PDF",
	msgExportaSucess:     "Exportação realizada com sucesso!",
	msgExportaError:      "Não foi possível exportar o relatório!",
	// Modal concluiTarefa
	modSucesso:           "Compromisso baixado com sucesso",
	modConcluirTitle:     "Concluir Compromisso",
	modCompromisso:       "Compromisso",
	modStatusAtual:       "Status Atual",
	modNovoStatus:        "Novo Status",
	modSemStatus:         "Favor Informar o Novo Status.",
	modFechar:            "Fechar",
	modConcluir:          "Concluir",
	modDescricao:         "Descrição",
	// Colunas
	colunaContrato:       "Contrato",
	colunaTipo:           "Tipo",
	colunaData:           "Data",
	colunaHora:           "Hora"

}

//-- Constantes para as widgets de acompanhamento de contratos
export const acompanhamento = {
	acompanhamento:       "Acompanhamento de contratos",
	contratosEmAndamento: "Contratos vigentes",
	contratosNovos:       "Novos contratos",
	contratosEncerrados:  "Contratos encerrados",
	verEmAndamento:       "Ver os contratos vigentes",
	verNovos:             "Ver os novos contratos",
	verEncerrados:        "Ver os contratos encerrados",
	loadEmAndamento:      "Carregando contratos vigentes",
	loadNovos:            "Carregando novos contratos",
	loadEncerrados:       "Carregando contratos encerrados",
	slaJuridico:          "SLA Jurídico",
	loadSLA:              "Carregando SLA Jurídico",
	diasSLA:              "Dias",
	verSLA:               "Ver mais detalhes"
}

export const grafico = {
	titulo:               "Contratos vigentes por tipo",
	descricao:            "Valor dos contratos vigentes por tipo",
	load:                 "Carregando informações...",
	labelDataSet:         "Valor total de contratos (R$)"
}

export const homeContratosPt = {

	criarNovoContrato:    "Criar novo contrato",
	toolTipPesqRapida:    "Utilize na pesquisa rápida : \nO número do contrato, o nome dos envolvidos, o código de workflow ou o código do assunto jurídico.",
	pesqContrato:         "Pesquisar contratos",
	loadTarPendente:      "Carregando tarefas pendendes",
	titulo:               "TOTVS JURÍDICO",
	btnSelecionar:        "Selecionar Arquivo",
	tituloModalFil:       "Seleção de Filial!",
	selecionaFilial:      "Selecione uma filial para o cadastro do contrato.",
	salvar:               "Salvar",
	cancelar:             "Cancelar",
	filial:               "Filial",
	favoritos:            favoritos,
	vencimentos:          vencimentos,
	tarefas:              tarefas,
	acompanhamento:       acompanhamento,
	grafico:              grafico
}
