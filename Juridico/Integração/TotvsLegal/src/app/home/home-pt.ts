/**
* Arquivo de constantes para tradução da Home - Português
*/

//-- Constantes para títulos das colunas do Grid de Favoritos / Provisão  / Acompanhamento de Processos
export const gridsHome = {

	colunaProcesso:     "Processo",
	colunaMovimentacao: "Movimentações",
	colunaData:         "Data",
	colunaFav:          " ",
	colValorProv:       "Valor da provisão (R$) ",
}

//-- Constantes para a legenda do grafico de Acompanhamento de Processos
export const graphicAcomp = {

	legAnd:        "Em andamento",
	legNovos:      "Casos novos",
	legEncerrados: "Casos encerrados"
}

//-- Constantes para o modal de Tarefas
export const modalPt = {
	modFechar:      "Fechar",
	modConcluir:    "Concluir",
	modErro:        "Não foi possível baixar o compromisso.",
	modSucesso:     "Compromisso baixado com sucesso",
	modTitle:       "Concluir compromisso",
	modCompromisso: "Compromisso",
	modStatusAtual: "Status atual",
	modNovoStatus:  "Novo status",
	modSemStatus:   "Favor informar o novo status.",
	modDescricao:   "Descrição"

}

//-- Constantes para tradução da Home
export const homePt = {
	
	loadFav:              "Carregando favoritos",
	loadProv:             "Carregando provisões",
	loadAnd:              "Carregando andamentos",
	loadNovos:            "Carregando novos processos",
	loadEncerrados:       "Carregando processos encerrados",
	loadGrafico:          "Carregando gráfico",
	favoritos:            "Favoritos",
	RemoverFav:           "Deseja remover favorito?",
	favExcluido:          "Favorito excluído com sucesso.",
	pesqProcesso:         "Pesquisar processos",
	pesqAvancada:         "Pesquisa avançada",
	toolTipPesqRapida:    "Utilize na pesquisa rápida : \nO número do processo, o nome dos envolvidos, ou o código do assunto jurídico.",
	criarNovoProcesso:    "Criar novo processo",
	meusFav:              "Meus favoritos",
	verFav:               "Ver todos os meus favoritos",
	provisoes:            "Provisão dos processos",
	verProv:              "Ver todas as provisões",
	processosEmAndamento: "Processos em andamento",
	verAndamentos:        "Ver os processos em andamento",
	novosProcessos:       "Novos processos",
	verprocessosNovos:    "Ver os processos novos",
	processosEncerrados:  "Processos encerrados",
	verEncerrados:        "Ver os processos encerrados",
	demonstrativo:        "Demonstrativo da evolução de processos nos últimos meses.",
	acomp:                "Acompanhamento dos processos",
	evolucao:             "Evolução dos processos",
	anterior:             "<<  Anterior",
	proximo:              "Próximo  >>",
	selecionaFilial:      "Selecione uma filial para o cadastro de processos.",
	tituloModalFil:       "Seleção de filial!",
	filial:               "Filial",
	btnSelecionar:        "Selecionar arquivo",
	msgExportaSucess:     "Exportação realizada com sucesso!",
	msgExportaError:      "Não foi possível exportar o relatório!",
	exportarExcel:        "Exportar para excel",
	exportarPDF:          "Exportar para PDF",
	tarefasVencer:        "Minhas tarefas pendentes (7 dias)",
	painelAtividades:     "Painel de atividades",
	loadTarPendente:      "Carregando tarefas pendendes",
	distribuicoes:        "Distribuições em análise",
	verDistribuicoes:     "Ver mais detalhes",
	loadDistribuicao:     "Carregando distribuições recebidas",
	pendentes:            "Pendentes",
	gridsH: gridsHome,
	graphic: graphicAcomp,
	modalPt: modalPt,
	titulo: "TOTVS JURÍDICO"
   }   
