import { Component, OnInit, ViewChild  } from '@angular/core';
import { LegalprocessService, sysParamStruct, SysProtheusParam } from '../services/legalprocess.service';
import { JurigenericsService, JurBase64 } from '../services/jurigenerics.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PoDialogConfirmLiterals, PoDialogService, PoModalComponent, PoModalAction, PoComboComponent, PoUploadComponent, PoTableColumn, PoNotificationService, PoI18nService } from '@po-ui/ng-components';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FwmodelService, FwModelBody } from '../services/fwmodel.service';
import { ConcluiTarefa} from './home';
import { SearchprocessAdapterService } from 'src/app/services/searchprocess-adapter.service';
import { lpGetFiliaisAdapterService  } from 'src/app/services/legalprocess-adapter.service';
import { LegaltaskService } from '../services/legaltask.service';
import { JurJustifModalComponent } from '../shared/jur-justif-modal/jur-justif-modal.component';
import { adaptorFilterParam, FwmodelAdaptorService } from '../services/fwmodel-adaptor.service';
import { Title } from '@angular/platform-browser';
import { TLUserInfo } from '../services/authlogin.service';
import { UserConfig } from 'src/app/usuarios/user-config/user-config.component'
import { ChartDataSets } from 'chart.js';

/** Classe do Grafico de Evolução
 * @field mesano: Ano mes
 * @field emAndamento: Quantidade em Andamento no Ano Mes
 * @field casosNovos: Quantidade de Casos novos no Ano Mes
 * @field casosEncerados: Quantidade de Casos encerrados no Ano Mes
 */
class EvolutionGraph {
	mesano: string;
	emAndamento: string;
	casosNovos: string;
	casosEncerrados: string;

}

class TarefasVencer {
	filial: string;
	cajuri: string;
	codTarefa: string;
	codProcess: string;
	processo: string;
	tipo: string;
	data: string;
	hora: string;
	status: string;
	descSatus: string;
	situacProc: string;
	tarefaEdit: Array<string>;
	descricao: string;
	assJur:string;
}

class ItemFavorito {
	partes:       string;
	movimentacao: string;
	date:         string;
	filial:       string;
	fav:          string;
	cajuri:       string;
}

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
	formConcluiComp: FormGroup;
	formFiliais:     FormGroup;

	dtAnoMesAtu: Date = new Date();

	bFilial:                 boolean;//inicia como undefined
	isHideLoadingFavoritos:  boolean = false;
	isHideLoadingProvisoes:  boolean = false;
	isHideLoadingAndamentos: boolean = false;
	isHideLoadingNovos:      boolean = false;
	isHideLoadingEncerrados: boolean = false;
	hideLoadingdDistr:       boolean = false;
	isHideLoadingGraph:      boolean = false;
	isHideLoadingTarefas:    boolean = false;
	isProcEncerrado:         boolean = false;
	isShowMoreDisabled:      boolean = true;

	pkTarefa:      string = '';
	strAnomesAtu:  string = '';
	descResult:    string = '';
	empresaLogada: string = '';
	filial:        string = '';
	codPro:        string = '';
	codUsuario:    string = '';
	searchProcess: string = ''; //parâmetros pesquisa rápida de processos

	litHome: any;
	literalsConfirm: PoDialogConfirmLiterals;
	
	qtdAndamento:       number = 0;
	qtdNovosProcessos:  number = 0;
	qtdEncerrado:       number = 0;
	nQtdDistribuicao:   number = 0;
	uploadCount:        number = 0;
	uploadSuccessCount: number = 0;
	nQtdeTarefasVencer: number = 0;
	nQtdShowTarefa:     number = 30
	
	provisionList: Array<any>            = [];
	listFavoritos: Array<ItemFavorito>   = [];
	items:         Array<any>            = [];
	filiaisCombo:  Array<string>         = [];
	cmbFiliais:    Array<struct>         = [];
	sysParams:     Array<sysParamStruct> = []
	listTarefas:   Array<TarefasVencer>  = []
	
	clone:      ChartDataSets[] = [{data:[]}];
	cloneMonth: string[]        = []

	colTarefas: Array<PoTableColumn>  = [
			{ property: 'processo', label: 'Processo', color: 'color-01',
				type: 'link',
				action: (value,row) => {
					this.filial = row.filial;
					/** Proteção feita para ambientes compartilhado, com isso, a abertura do processo
					*   é feita respeitando o tamanho do campo filial.
					*   O item da 'filial' do localStorage é inserida na função MostraFilial da Home.
					*/
					if (row.filial == undefined && window.localStorage.getItem('filial').trim() == "") {
						this.filial = window.localStorage.getItem('filial');
					}
					
					if(row.assJur == '006' ){
						this.openContrato(this.filial, row.cajuri);
					} else {
						this.openProcesso(this.filial, row.cajuri);
					}
				}
			},
			{ property: 'tipo', label: 'Tipo' },
			{ property: 'data', label: 'Data',type:"date" },
			{ property: 'hora', label: 'Hora' },
			{ property: 'tarefaEdit', label: ' ', width: '16%', type: 'icon',
			icons: [
				{
					value: 'tarefa', icon: 'po-icon-edit', color: '#29b6c5', tooltip: 'Editar', action: (value) => {
						this.updateTarefa(value);
					},
				},
				{
					value: 'concluir', icon: 'po-icon-ok', color: '#color-07', tooltip: 'Concluir', action: (value) => {
						this.openConcluiTarefa(value);
					},
				},
				{
					value: 'anexos', icon: 'po-icon po-icon-document-filled', color: '#color-07', tooltip: 'Anexos', action: (value) => {
						this.callAnexos(value);
					}
				}
			]
		},

	];
	colFavoritos: Array<PoTableColumn> = [
		{
			property: 'partes',
			color: 'color-01',
			type: 'link',
			action: (value,row) => {
				this.filial = row.filial;

				/** Proteção feita para ambientes compartilhado, com isso, a abertura do processo
				*   é feita respeitando o tamanho do campo filial.
				*   O item da 'filial' do localStorage é inserida na função MostraFilial da Home.
				*/
				if (row.filial == "" && window.localStorage.getItem('filial').trim() == "") {
					this.filial = window.localStorage.getItem('filial');
				}

				this.openProcesso(this.filial, row.cajuri);
			}
		},
		{ 
			property: 'movimentacao',
			color: 'color-01',
			type: 'link',
			action: (value,row) => {
				this.filial = row.filial;

				/** Proteção feita para ambientes compartilhado, com isso, a abertura do processo
				*   é feita respeitando o tamanho do campo filial.
				*   O item da 'filial' do localStorage é inserida na função MostraFilial da Home.
				*/
				if (row.filial == "" && window.localStorage.getItem('filial').trim() == "") {
					this.filial = window.localStorage.getItem('filial');
				}

				this.openProcesso(this.filial, row.cajuri);
			}
		},
		{ 
			property: 'date', label: ' ' ,
			color: 'color-01',
			type: 'link',
			action: (value,row) => {
				this.filial = row.filial;

				/** Proteção feita para ambientes compartilhado, com isso, a abertura do processo
				*   é feita respeitando o tamanho do campo filial.
				*   O item da 'filial' do localStorage é inserida na função MostraFilial da Home.
				*/
				if (row.filial == "" && window.localStorage.getItem('filial').trim() == "") {
					this.filial = window.localStorage.getItem('filial');
				}

				this.openProcesso(this.filial, row.cajuri);
			}
		},
		{ property: 'fav', label: ' ', type: 'icon', action: this.confirmDel.bind(this), icons: [{ value: 'favorito', icon: 'po-icon-star-filled', color: '#0d729c' }] },
	];

	colsProvisao: Array<PoTableColumn> = [
		{ property: 'caso', 
			color: 'color-01',
			type: 'link',
			action: (value, row) => {
				this.openProcesso(row.processBranch, row.processId);
			}
		},
		{ property: 'atualizedprovision', type: 'number', format: '1.2-5' }
	];

	// ações do modal de seleção de filiais
	cancelar: PoModalAction = {
		action: () => { 
			this.modalFiliais.close();
		},
		label: 'Cancelar',
	};

	salvar: PoModalAction = {
		action: () => { this.salvarFilial(); },
		label: 'Salvar',
	};

	// ações do modal de concluir compromisso
	fechar: PoModalAction = {
		action: () => {
			this.ConcluiModal.close();
		},
		label: ' ',
	};
	putConcluirTarefa: PoModalAction = {
		action: () => {
			if (this.JuriGenerics.changeUndefinedToEmpty(this.formConcluiComp.value.novoStatus) != '') {
				this.verifyBaixa();
			} else {
				this.poNotification.warning(this.litHome.modalPt.modSemStatus);
			}
		},
		label: ' ',
	};


	//Modal para concluir tarefa
	@ViewChild('concluiTarefa', { static: true  }) ConcluiModal: PoModalComponent;
	@ViewChild('modJustif',     { static: true  }) modalJustif:  JurJustifModalComponent;
	@ViewChild('filiais',       { static: true  }) modalFiliais: PoModalComponent;
	@ViewChild('comboFilial',   { static: true  }) cmbFilial:    PoComboComponent;
	@ViewChild('docUpload') docUpload:    PoUploadComponent;


	// Adaptor do status de followup
	adaptorStatusFup: adaptorFilterParam = new adaptorFilterParam("JURA017", "", "NQN_DESC", "NQN_DESC", "NQN_DESC", "NQN_COD", "", "getStatusFup")

	constructor(
		protected thfI18nService: PoI18nService,
		public poNotification: PoNotificationService,
		public searchprocess: SearchprocessAdapterService,
		private legalprocess: LegalprocessService,
		private fwmodel: FwmodelService,
		private router: Router,
		private route: ActivatedRoute,
		private thfAlert: PoDialogService,
		private JuriGenerics: JurigenericsService,
		private formBuilder: FormBuilder,
		private sysProtheusParam: SysProtheusParam,
		private legaltask: LegaltaskService,
		private userInfo: TLUserInfo,
		public filiaisAdapterService: lpGetFiliaisAdapterService,
		public fwModelAdaptorService: FwmodelAdaptorService,
		protected notification: PoNotificationService,
		private titleService: Title) {
		
		this.mostraFilial();
		
		//-- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'home' no arquivo JurTraducao.
		thfI18nService.getLiterals({ context: 'home', language: thfI18nService.getLanguage() })
			.subscribe((litHome) => {
				this.litHome = litHome;

				//-- seta as constantes para tradução do titulo das colunas do Grid de Favoritos
				this.colFavoritos[0].label = this.litHome.gridsH.colunaProcesso;
				this.colFavoritos[1].label = this.litHome.gridsH.colunaMovimentacao;
				this.colFavoritos[2].label = this.litHome.gridsH.colunaData;
				this.colFavoritos[3].label = this.litHome.gridsH.colunaFav;

				//-- seta as constantes para tradução do titulo das colunas do Grid de Provisões
				this.colsProvisao[0].label = this.litHome.gridsH.colunaProcesso;
				this.colsProvisao[1].label = this.litHome.gridsH.colValorProv;

				//-- seta as constantes para tradução das strings no gráfico Acompanhamento de Processos
				this.graphEvoLines[0].label = this.litHome.graphic.legAnd;
				this.graphEvoLines[1].label = this.litHome.graphic.legNovos;
				this.graphEvoLines[2].label = this.litHome.graphic.legEncerrados;

				this.fechar.label = this.litHome.modalPt.modFechar;
				this.putConcluirTarefa.label = this.litHome.modalPt.modConcluir;
			});
	}


	/** Propriedade das linhas do Gráfico
	 *  https://www.npmjs.com/package/ng2-charts
	 */
	public graphEvoLineProp: Array<any> = [{
		// Em Andamento - Azul Escuro #0d729c
		backgroundColor: 'rgba(13, 114, 156, 0)',
		borderColor: 'rgba(13, 114, 156, 1)',
		pointBackgroundColor: 'rgba(255, 255, 255, 1)',
		pointBorderColor: 'rgba(13, 114, 156, 1)',
		pointHoverBackgroundColor: 'rgba(13, 114, 156, 1)',
		pointHoverBorderColor: 'rgba(148,159,177,0.8)'
	},
	{ // Casos Novos - Azul Claro #29b6c5
		backgroundColor: 'rgba(41, 182, 197, 0)',
		borderColor: 'rgba(41, 182, 197, 1)',
		pointBackgroundColor: 'rgba(255, 255, 255, 1)',
		pointBorderColor: 'rgba(41, 182, 197, 1)',
		pointHoverBackgroundColor: 'rgba(41, 182, 197, 1)',
		pointHoverBorderColor: 'rgba(41, 182, 197 ,1)'
	},
	{ // Casos Encerrados - Laranja #ea9b3e
		backgroundColor: 'rgba(234, 155, 62, 0)',
		borderColor: 'rgba(234, 155, 62, 1)',
		pointBackgroundColor: 'rgba(255, 255, 255, 1)',
		pointBorderColor: 'rgba(234, 155, 62, 1)',
		pointHoverBackgroundColor: 'rgba(234, 155, 62, 1)',
		pointHoverBorderColor: 'rgba(148,159,177,0.8)'
	}
	];

	ngOnInit() {
		this.titleService.setTitle(this.litHome.titulo)
		this.legalprocess.restore();
		this.strAnomesAtu = this.dtAnoMesAtu.getFullYear().toString() + this.JuriGenerics.buildMonthDayStr((this.dtAnoMesAtu.getMonth() + 1).toString()) + this.JuriGenerics.buildMonthDayStr(this.dtAnoMesAtu.getDate().toString());
		this.strAnomesAtu = this.JuriGenerics.makeDate(this.strAnomesAtu,"MMMM de yyyy");

		this.getFavoritos();
		this.createFormFiliais();
		this.getTarefas();
		this.createFormConcluiComp(new ConcluiTarefa);
		// Busca os parâmetros do Protheus
		this.sysProtheusParam.getParamByArray(["MV_JTVENJF"]).subscribe((params) => {
			this.sysParams = params;
		})

		// Copia a estrutura
		this.clone      = JSON.parse(JSON.stringify(this.graphEvoLines))
		this.cloneMonth = JSON.parse(JSON.stringify(this.graphEvoFooter));

		this.getInfoUser();
	}

	/**
	 * Função responsável por obter dados do usuário logado.
	 */
	getInfoUser() {
		this.codUsuario = this.userInfo.getCodUsuario();

		if (this.JuriGenerics.changeUndefinedToEmpty(this.codUsuario) == ""){
			setTimeout(() => {
				this.getInfoUser();
			}, 500);
		} else {
			this.getGraphEvo();
			this.getProvision();
			this.getStock();
		}
	}

	/**
	 * método GET (ListProcess) de processos
	 * consome o WS REST de integração com LegalProcess
	 */
	getProvision() {
		let cfgUsuario: Array<UserConfig> = [];
		let nIndex:     number            = -1;
		let tpAssJur:   Array<string>     = [];

		if ( this.JuriGenerics.changeUndefinedToEmpty(window.localStorage.getItem('userConfig')) != '') {
			cfgUsuario = JSON.parse(window.localStorage.getItem('userConfig'));
			nIndex     = cfgUsuario.findIndex(x => x.userId == this.codUsuario);
			tpAssJur   = nIndex >= 0 ? cfgUsuario[nIndex].filtAssJur : [];
		}

		this.legalprocess.restore();
		this.legalprocess.setOrderProvision("1");                       // ordenação da query (1 = "ORDER BY NSZ.NSZ_VAPROV DESC")
		this.legalprocess.setPageSize('5');                             // quantidade de itens por página
		this.legalprocess.setTypeFilter('1');                           // processo em andamento
		this.legalprocess.setSearchKey("")                              // conteúdo pesquisado nos campos NT9_NOME/ NUQ_NUMPRO/ NSZ_DETALH
		this.legalprocess.setFilter('tipoAssJur', tpAssJur.toString()); // Informar o assunto jurídico que deseja filtrar
		this.legalprocess.get('process', 'Lista de Provisão').subscribe((data) => {
			this.isHideLoadingProvisoes = false;
			if (data.processes != '') {
				//obtém o nome do caso contido no sub array dos processos retornados
				data.processes.forEach(item => {
					item.caso = item.matter[0].description;
					item['link'] = '/processo?p=' + item.processId;
				});

				this.provisionList = data.processes;
				this.isHideLoadingProvisoes = true;
			}
			this.isHideLoadingProvisoes = true;
		})
	}

	/**
	 * Responsável por trazer os processos FAVORITOS
	 */
	getFavoritos() {
		this.listFavoritos = [];
		this.legalprocess.restore();
		this.legalprocess.setFilter("tipoAssjur", "001")
		this.legalprocess.setPageSize('5');
		this.isHideLoadingFavoritos = false;
		
		this.legalprocess.get("listFavorite","Lista de Favoritos").subscribe(data => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data.listFavorite) != ''){
				data.listFavorite.forEach(item => {
					const favoritos = new ItemFavorito;
					
					favoritos.date         = this.JuriGenerics.makeDate( this.JuriGenerics.changeUndefinedToEmpty( item.date), "dd/mm/yyyy");
					favoritos.movimentacao = item.movimentacao;
					favoritos.cajuri       = item.cajuri;
					favoritos.fav          = item.fav;
					favoritos.filial       = item.filial;
					favoritos.partes       = item.partes;
					
					this.listFavoritos.push(favoritos);
			});
		}
			this.isHideLoadingFavoritos = true;
		})
	}

	/**
	* Responsável por remover favoritado
	*/
	//-- Dialog Confirm de Favoritos
	confirmDel(row) {

		this.thfAlert.confirm({
			literals: this.literalsConfirm,
			title: this.litHome.favoritos,
			message: this.litHome.RemoverFav,
			confirm: () => this.delFavoritos(row)
		});
	}

	//- Responsável por remover um processo favoritado
	delFavoritos(row) {
		let pkFavorito: string = '';
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA269");
		this.fwmodel.setFilter("O0V_FILCAJ = '" + row.filial + "'");
		this.fwmodel.setFilter("O0V_CAJURI = '" + row.cajuri + "'");
		this.fwmodel.get("Busca tarefas").subscribe((data) => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != '') {
				pkFavorito = data.resources[0].pk
			
				this.fwmodel.deleteFavoritos(pkFavorito).subscribe((data) => {
					if (data) {
						this.poNotification.success(this.litHome.favExcluido);
						this.getFavoritos();
					}
				});
			}
		})
	}
	
	/**
	 * rota de navegação
	 */
	openProvisao(){
		this.router.navigate(["provisao"])
	}

	openAcompanhamentoAndamento(){
		this.router.navigate(["acompanhamento"], { queryParams: { page: "1" } })
	}

	openAcompanhamentoNovosProcessos(){
		this.router.navigate(["acompanhamento"], { queryParams: { page: "2" } })
	}

	openAcompanhamentoEncerrados(){
		this.router.navigate(["acompanhamento"], { queryParams: { page: "3" } })
	}

	openDistribuicoes(){
		this.router.navigate(["Distribuicao"])
	}

	openFavoritos(){
		this.router.navigate(["processos/favoritos"])
	}

	openProcesso(filPro: string, codPro: string){
		window.open('processo/' + encodeURIComponent(btoa(filPro)) + "/" + encodeURIComponent(btoa(codPro)));
	}

	openContrato(filPro: string, codPro: string){
		window.open('contrato/' + encodeURIComponent(btoa(filPro)) + "/" + encodeURIComponent(btoa(codPro)));
	}

	callNewProcess(){
		this.router.navigate(["processo/cadastro"])
	}

	openPesqAvancada(){
		this.router.navigate(["pesquisa"])
	}

	// Acompanhamento
	// Processos em Andamento
	getStock() {
		let cfgUsuario: Array<UserConfig> = [];
		let nIndex:     number            = -1;
		let tpAssJur:   Array<string>     = [];

		if ( this.JuriGenerics.changeUndefinedToEmpty(window.localStorage.getItem('userConfig')) != '') {
			cfgUsuario = JSON.parse(window.localStorage.getItem('userConfig'));
			nIndex     = cfgUsuario.findIndex(x => x.userId == this.codUsuario);
			tpAssJur   = nIndex >= 0 ? cfgUsuario[nIndex].filtAssJur : [];
		}

		//Processo em andamento
		this.legalprocess.restore();
		this.legalprocess.setTypeFilter('1');
		this.legalprocess.setPageSize('0');
		this.legalprocess.setFilter('tipoAssJur', tpAssJur.toString()); // Informar o assunto jurídico que deseja filtrar
		this.isHideLoadingAndamentos = false;
		this.legalprocess.get('process', 'Acompanhamento - Proc. Andamento').subscribe((data) => {
			this.qtdAndamento = data.length.toString();
			this.isHideLoadingAndamentos = true;
		})

		//Casos Novos
		this.legalprocess.restore();
		this.legalprocess.setTypeFilter('2');
		this.legalprocess.setPageSize('0');
		this.legalprocess.setFilter('tipoAssJur', tpAssJur.toString()); // Informar o assunto jurídico que deseja filtrar
		this.isHideLoadingNovos = false;
		this.legalprocess.get('process', 'Acompanhamento - Novos Processo').subscribe((data) => {
			this.qtdNovosProcessos = data.length.toString();
			this.isHideLoadingNovos = true;
		})

		//Encerrados no mês
		this.legalprocess.restore();
		this.legalprocess.setTypeFilter('3');
		this.legalprocess.setPageSize('0');
		this.legalprocess.setFilter('tipoAssJur', tpAssJur.toString()); // Informar o assunto jurídico que deseja filtrar
		this.isHideLoadingEncerrados = false;
		this.legalprocess.get('process', 'Acompanhamento - Proc. Encerrados').subscribe((data) => {
			this.qtdEncerrado = data.length.toString();
			this.isHideLoadingEncerrados = true;
		})

		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA279");
		this.fwmodel.setFilter("NZZ_STATUS = '1'");
		this.hideLoadingdDistr = false;
		this.fwmodel.get("Busca distribuicoes recebidas").subscribe((data) => {
			if ( data.total > 0 ) {
				this.nQtdDistribuicao = data.total;
				this.hideLoadingdDistr = true;
			} else {
				this.hideLoadingdDistr = true;
			}
		})
	}

	//----------------------
	// Evolução do Processo
	dadosGrafico: EvolutionGraph[] = new Array();

	//Dados Gráfico
	// Linha do Gráfico
	public graphEvoLines: ChartDataSets[] = [
		{ data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: ' ' },
		{ data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: ' ' },
		{ data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: ' ' }
	];

	// Rodapé do Gráfico
	public graphEvoFooter: Array<any> = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];//this.strAnomesAtu;

	public graphEvoShowLegend: boolean = true; // Demonstra se vai ter a legenda ou não
	public graphEvoTipo: string = 'line';  // Tipo do Gráfico
	public graphEvoOptions: any = {
		responsive: true,
		elements: {
			line:
			{
				tension: 0
			},
			point:
			{
				radius: 7,
				borderWidth: 2,
				hitRadius: 5,
				hoverRadius: 10,
				hoverBorderWidth: 2
			}
		},
		legend: { position: 'bottom' }
	};

	// funções do gráfico
	getGraphEvo() {
		let cfgUsuario: Array<UserConfig> = [];
		let nIndex:     number            = -1;
		let tpAssJur:   Array<string>     = [];

		if ( this.JuriGenerics.changeUndefinedToEmpty(window.localStorage.getItem('userConfig')) != '') {
			cfgUsuario = JSON.parse(window.localStorage.getItem('userConfig'));
			nIndex     = cfgUsuario.findIndex(x => x.userId == this.codUsuario);
			tpAssJur   = nIndex >= 0 ? cfgUsuario[nIndex].filtAssJur : [];
		}
		
		this.legalprocess.restore();
		this.legalprocess.setFilter('tipoAssJur', tpAssJur.toString()); // Informar o assunto jurídico que deseja filtrar
		this.isHideLoadingGraph = false;
		// Faz o subscribe para montar o Chart
		this.legalprocess.get('evolution', 'Evolução dos Processos').subscribe((data) => {
			var count = 0;
			if (this.JuriGenerics.changeUndefinedToEmpty(data.evolution) != '') {
				// Loop
				data.evolution.forEach(item => {

					// Instância da classe
					var graphLine = new EvolutionGraph();
					graphLine.emAndamento = item.andamento;
					graphLine.casosEncerrados = item.encerrados;
					graphLine.casosNovos = item.novos;
					graphLine.mesano = item.mesano;

					// Inclui no Array de retorno
					this.dadosGrafico.push(graphLine);

					// Seta os valores no Array do Grafico
					this.clone[0].data[count] = parseInt(graphLine.emAndamento);
					this.clone[1].data[count] = parseInt(graphLine.casosNovos);
					this.clone[2].data[count] = parseInt(graphLine.casosEncerrados);

					// Rodapé do Gráfico
					this.cloneMonth[count] = this.JuriGenerics.makeDate(graphLine.mesano, "MM/yyyy");
					count++;

				});
			}

			// Retorna os valores para as variáveis usadas nos graficos
			this.graphEvoLines = this.clone;
			this.graphEvoFooter = this.cloneMonth;
			this.isHideLoadingGraph = true;
		});

		return this.dadosGrafico;
	}

	/**
	 * Ação do onSelect da pesquisa rápida de processos
	 */
	onSelectSearchedProcess() {
		if(this.searchProcess != '' && this.searchProcess != undefined){
			this.openProcesso(atob(this.searchProcess).substring(0 , atob(this.searchProcess).indexOf("/")),
							 atob(this.searchProcess).substring(atob(this.searchProcess).indexOf("/") + 1 ),
			);
		}
	}

	/**
	 * Responsável por solicitar ao usuário a filial do cadastro do processo
	 */
	verificaFiliaisDoUsuario(){
		if(this.bFilial == undefined){
			setTimeout(() => { this.verificaFiliaisDoUsuario(); }, 500);
		} else {
			if (this.bFilial) {
				this.modalFiliais.open();
				this.formFiliais.patchValue({ filialProc: window.localStorage.getItem('filial') });
			} else {
				this.callNewProcess();
			}
		}

	}

	/**
	 * Cria o form para seleção de Filiais
	 */
	createFormFiliais(){
		this.formFiliais = this.formBuilder.group({
			filialProc:    ["", Validators.compose([Validators.required])]
		});
		this.cmbFilial.selectedValue = window.localStorage.getItem('filial');
	}

	/**
	 * Responsável por armazenar a filial selecionada
	 */
	salvarFilial(){
		window.localStorage.setItem('filial', this.cmbFilial.selectedOption.value.toString() );
		window.sessionStorage.setItem('filial', this.cmbFilial.selectedOption.value.toString() );
		this.modalFiliais.close();
		this.callNewProcess();
	}

	/**
	 * Valida se a filial será apresentada.
	 */
	mostraFilial(){
		this.legalprocess.restore()
		this.legalprocess.get("listFiliaisUser","Lista de Filial").subscribe(itemFiliais => {
			if (itemFiliais.length > 1){
				this.bFilial = true;
			} else {
				if (itemFiliais.length == 1) {
					window.localStorage.setItem('filial', itemFiliais.User[0].value.toString() );
				}

				this.bFilial = false;
			}
			return itemFiliais
		});
	}

	/**
	 * Faz o get de Follow-ups através do legaltask
	*/
	getTarefas() {
		let dataIni: Date = new Date()
		let dataFim: Date = new Date()

		dataIni.setMonth(dataIni.getMonth()-3)
		dataFim.setDate(dataIni.getDate()+7)

		this.legaltask.restore();
		this.isHideLoadingTarefas = false;
		this.legaltask.setFilter('pageSize',this.nQtdShowTarefa.toString());
		this.legaltask.setFilter('dataIni',dataIni.toJSON().substr(0,10).replace(/-/g,'')) 
		this.legaltask.setFilter('dataFim',dataFim.toJSON().substr(0,10).replace(/-/g,''))
		this.legaltask.setFilter('status','1');
		this.legaltask.setFilter('lWSTLegal', 'true');
		this.listTarefas = [];
		this.legaltask.get('fup','Busca Tarefas').subscribe((data) => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.data) != ''){
				data.data.forEach(item => {
					let itemTarefa = new TarefasVencer();
					itemTarefa.filial     = this.JuriGenerics.changeUndefinedToEmpty(item.filial);
					itemTarefa.cajuri     = item.cajuri
					itemTarefa.codTarefa  = this.JuriGenerics.changeUndefinedToEmpty(item.idFu);
					itemTarefa.codProcess = this.JuriGenerics.changeUndefinedToEmpty(item.cajuri);
					itemTarefa.processo   = this.JuriGenerics.changeUndefinedToEmpty(item.autor) + " - " + this.JuriGenerics.changeUndefinedToEmpty(item.reu);
					itemTarefa.tipo       = item.tipofu[0].descri;
					itemTarefa.status     = item.result;
					itemTarefa.descSatus  = item.descresul;
					itemTarefa.hora       = item.hora;
					itemTarefa.data       = this.JuriGenerics.makeDate(item.dtfup,"yyyy-mm-dd");
					itemTarefa.situacProc = item.situacao;
					itemTarefa.tarefaEdit = ['tarefa', 'concluir'];
					itemTarefa.descricao  = item.desc
					itemTarefa.assJur     = item.tipoAsjOrig.trim() == '' ? item.tipoasj : item.tipoAsjOrig;

					if (item.documentos.length > 0) {
						itemTarefa.tarefaEdit = ['tarefa', 'concluir', 'anexos'];
					}
					this.listTarefas.push(itemTarefa);
				})
				this.nQtdeTarefasVencer = data.length;
				this.isShowMoreDisabled = data.hasNext === "false";
			}
			this.isHideLoadingTarefas = true;
		})
	}

	/**
	 * Função atribuida ao botão de mostrar mais tarefas pendentes
	 */
	showMoreTarefas(){
		if (this.nQtdShowTarefa === 60){
			this.nQtdShowTarefa = this.nQtdeTarefasVencer
		} else {
			this.nQtdShowTarefa += 30
		}
		this.getTarefas()
	}
	/**
	 * Abre a Det-tarefa em modo de Update
	 */
	updateTarefa(value ?: TarefasVencer) {
		if ( value != undefined){
			window.open( (value.assJur == "006" ? 'contrato/' : 'processo/') + encodeURIComponent(btoa(value.filial)) + '/' + encodeURIComponent(btoa(value.codProcess)) + 
						'/tarefa/' + encodeURIComponent(btoa(value.codTarefa))
			);
		}
	}
	
	/**
	 * Abre modal para concluir a tarefa
	 * @param value 
	 */
	openConcluiTarefa(value?: TarefasVencer) {

		this.formConcluiComp.patchValue({
			tipo: value.tipo,
			statusAtual: value.descSatus,
			codTar: value.codTarefa,
			cajuri: value.codProcess,
			filial: value.filial,
			situacao: value.situacProc,
			novoStatus: '',
			descricao: value.descricao
		})
		this.getPKTarefa(value)
		this.ConcluiModal.open();
	}
	
	/**
	 * form para concluir tarefa pendente
	 * @param concl 
	 */
	createFormConcluiComp(concl: ConcluiTarefa) {

		this.formConcluiComp = this.formBuilder.group({
			tipo:        [concl.tipo],
			statusAtual: [concl.statusAtual],
			novoStatus:  [concl.novoStatus, Validators.compose([Validators.required])],
			codTar:      [concl.codTar],
			cajuri:      [concl.cajuri],
			filial:      [concl.filial],
			descricao:   [concl.descricao]
		})
	}

	/**
	 * validações antes do commit
	 */
	verifyBaixa() {
		if (this.beforeSubmitBaixa()) {
			this.baixTask();
		}
	}

	/**
	 * Verifica se o processo esta encerrado se sim, apresenta a modal de justificativa
	 * @param value 
	 */
	beforeSubmitBaixa(value?: TarefasVencer) {
		let isSubmitable: boolean = false;

		isSubmitable = this.formConcluiComp.valid;
		if (isSubmitable) {
	
			if (this.isProcEncerrado && this.sysProtheusParam.getParamValueByName(this.sysParams, "MV_JTVENJF") == "1") {
				isSubmitable = false;
				this.modalJustif.openModal(this.formConcluiComp.value.cajuri.toString(),
					() => {
						if (this.modalJustif.isOk) {
							this.baixTask();
						}
					});
			} else {
				isSubmitable = true;
			}

		}
		return isSubmitable
	}
	/**
	 * Busca a pk da tarefa que está sendo concluida para a alteração ser feita via modelo
	 * @param value dados da linha selecionada
	 */
	getPKTarefa(value?: TarefasVencer) {
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA106");
		this.fwmodel.setFilter("NTA_CAJURI = '" + value.codProcess + "'");
		this.fwmodel.setFilter("NTA_COD = '" + value.codTarefa + "'");
		this.fwmodel.setFilter("NTA_FILIAL = '" + value.filial + "'");
		this.fwmodel.get("Busca tarefas").subscribe((data) => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != '') {
				this.pkTarefa = data.resources[0].pk
			}
		})
		if(value.situacProc == '2'){
			this.isProcEncerrado = true;
		}

	}

	/**
	 * Baixa a tarefa
	 */
	baixTask() {
		this.ConcluiModal.primaryAction.loading = true;
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA106');
		this.fwmodel.put(this.pkTarefa, this.bodybaixaTask()).subscribe((data) => {
			if (data == "") {
				this.poNotification.error(this.litHome.modalPt.modErro);
			} else {
				//Valida se há documentos a serem enviados
				if (this.docUpload.hasOwnProperty('currentFiles') && this.docUpload.currentFiles.length > 0) {
					this.uploadCount = this.docUpload.currentFiles.length;
					this.sendDocs();
				} else {
					this.poNotification.success(this.litHome.modalPt.modSucesso);
					this.ConcluiModal.close();
					this.getTarefas();
				}
			}
			this.ConcluiModal.primaryAction.loading = false;
		})
	}

	/**
	 * Monta o body para o put de tarefas
	 */
	bodybaixaTask() {
		const fwModelBody: FwModelBody = new FwModelBody("JURA106",4,this.pkTarefa,"NTAMASTER")

		fwModelBody.models[0].addField("NTA_CRESUL", this.formConcluiComp.value.novoStatus, undefined, "C")
		fwModelBody.models[0].addField("NTA_DESC"  , this.formConcluiComp.value.descricao , undefined, "C")

		return fwModelBody.serialize()
	}
	/**
	 * Abre a tela de anexos
	 */
	callAnexos(value?: any) {
		if ( value != undefined){

			window.open((value.assJur == "006" ? 'contrato/' : 'processo/') + encodeURIComponent(btoa(value.filial)) + '/' + encodeURIComponent(btoa(value.codProcess)) + 
						'/tarefa/' + encodeURIComponent(btoa(value.codTarefa)) + '/anexos');
		}
	}
	/**
	 * Realiza o envio dos documentos
	 */
	sendDocs() {
		if (this.docUpload.currentFiles.length > 0) {
			this.docUpload.sendFiles();
		}
	}
	/**
	 * Chama o SelectFiles do po-upload
	 */
	getDocuments() {
		this.docUpload.selectFiles()
	}
	/**
	 * Carrega as informações para o POST do upload de arquivos
	 * @param file
	 */
	uploadFiles(file) {
		file.data = {
			cajuri: this.formConcluiComp.value.cajuri.toString(),
			entidade: "NTA",
			codEntidade: this.formConcluiComp.value.codTar.toString()
		};
	}
	/**
	 * Trata para redirecionar após concluir todos os anexos
	 */
	uploadSucess() {
		this.uploadSuccessCount++;

		if (this.uploadSuccessCount == this.docUpload.currentFiles.length) {
			this.poNotification.success(this.litHome.modalPt.modSucesso);
			this.ConcluiModal.close();
			this.docUpload.clear();
			this.getTarefas();
		}
		this.ConcluiModal.primaryAction.loading = false;
	}
	/**
	 * Função responsável por realizar Download do relatório através do navegador
	 * em Excel
	 */
	exportarExcel(){
		this.legaltask.restore();
		this.legaltask.setFilter('status','1') 
		this.legaltask.setFilter('periFilt','5') //Filtro para os proximos 7 dias
		this.legaltask.get('exportTarefa','Exportação de tarefas a vencer').subscribe((fileData)=>{
			if (this.JuriGenerics.changeUndefinedToEmpty(fileData.export) != '' ) {
				fileData.export.forEach(item => {
					var blob = JurBase64.toUTF8(item.filedata, 'application/octet-stream');
					var fileLink = document.createElement('a');

					fileLink.href = window.URL.createObjectURL(blob);;
					fileLink.download = item.namefile; //força o nome do download
					fileLink.target = "_blank"; //força a abertura em uma nova página
					document.body.appendChild(fileLink);
					fileLink.click(); //gatilha o evento do click
					this.notification.success(this.litHome.msgExportaSucess)
				})
			} else {
				this.notification.error(this.litHome.msgExportaError);
			}
		})

	}
	/**
	 * Função responsável por realizar Download do relatório através do navegador,
	 * relatorio de pauta de compromisso - PDF
	 */
	exportarPDF(){
		this.legaltask.restore();
		this.legaltask.setFilter('periFilt','5') //Filtro para os proximos 7 dias
		this.legaltask.setFilter('status','1') //Pendentes
		this.legaltask.get('exportPauta','Relatório de pauta - PDF').subscribe((fileData)=>{
			if (this.JuriGenerics.changeUndefinedToEmpty(fileData.pauta) != '' ) {
				fileData.pauta.forEach(item => {
					var blob = JurBase64.toUTF8(item.filedata, 'application/octet-stream');
					var fileLink = document.createElement('a');

					fileLink.href = window.URL.createObjectURL(blob);;
					fileLink.download = item.namefile; //força o nome do download
					fileLink.target = "_blank"; //força a abertura em uma nova página
					document.body.appendChild(fileLink);
					fileLink.click(); //gatilha o evento do click
					this.notification.success(this.litHome.msgExportaSucess)
				})
			} else {
				this.notification.error(this.litHome.msgExportaError);
			}
		})
	}

	/**
	 * Função responsável por fazer a abertura do painel de atividades
	 */
	openPainelTarefa(){
		window.open("PainelAtividades")
	}
	
}

class struct{
	label: string;
	value: string;
}