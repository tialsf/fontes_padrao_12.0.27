export class ConcluiTarefa {
	tipo:        string = '';
	statusAtual: string = '';
	novoStatus:  string = '';
    codTar:      string = '';
	cajuri:      string = '';
	filial:      string = '';
	descricao:   string = '';
}