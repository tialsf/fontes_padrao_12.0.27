const loginCustomLiterals = {
	title: 'Boas Vindas à TOTVS Jurídico',
	loginErrorPattern: 'Login obrigatório',
	loginHint: 'Caso não possua usuário entre em contato com o administrador do sistema',
	loginLabel: 'Insira seu usuário',
	loginPlaceholder: 'Insira seu usuário de acesso',
	passwordErrorPattern: 'Senha obrigatória',
	passwordLabel: 'Insira sua senha',
	passwordPlaceholder: 'Insira sua senha de acesso',
	customFieldErrorPattern: 'Campo customizado inválido',
	customFieldPlaceholder: 'Por favor insira um valor',
	rememberUser: 'Logar automaticamente',
	rememberUserHint: 'Esta opção pode ser desabilitada nas configurações do sistema',
	submitLabel: 'Entrar',
	forgotPassword: 'Esqueceu sua senha?',
}

export const loginPt = {
	customLiterals: loginCustomLiterals
}


