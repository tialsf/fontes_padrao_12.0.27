import { Component, ViewChild } from '@angular/core';
import { PoPageLogin, PoPageLoginComponent } from '@po-ui/ng-templates';
import { AuthloginService, TLUserInfo } from "../services/authlogin.service";
import { GrantTypeEnum, JurigenericsService } from '../services/jurigenerics.service';
import { Router } from '@angular/router';
import { PoModalPasswordRecoveryType, PoPageLoginLiterals, PoPageLoginRecovery } from '@po-ui/ng-templates';
import { PoI18nService } from '@po-ui/ng-components';
import { RoutingService } from '../services/routing.service';
import { LegalprocessService } from '../services/legalprocess.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})

/**
 * Classe de Login.
 */
export class LoginComponent {
	
	customLiterals: PoPageLoginLiterals;
	litLogin: any;
	literals: string;
	useSessionStorage = false;
	bLoading: boolean = false;

	passwordRecovery: PoPageLoginRecovery = {
		url: 'https://thf.totvs.com.br/sample/api/users',
		type: PoModalPasswordRecoveryType.Email,
	};

	@ViewChild("login") frmLogin: PoPageLoginComponent;
	constructor(
		protected thfI18nService: PoI18nService,
		private authLogin: AuthloginService, 
		private routingService: RoutingService,
		private userInfo: TLUserInfo,
		private router: Router,
		private legalprocess: LegalprocessService,
		private juriGenerics: JurigenericsService
		) {
			
			//-- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'menu' no arquivo JurTraducao.			
			thfI18nService.getLiterals( { context: 'login', language: thfI18nService.getLanguage() } ).subscribe((literals) => {
				this.litLogin = literals;
				this.customLiterals = this.litLogin.customLiterals
			});
	}

	/**
	 * Realiza a autenticação e cria o Storage para a Autenticação
	 * @param loginData - Pagina do Login
	 */
	loginSubmit(loginData: PoPageLogin){
		
		let userConfig = window.localStorage.getItem("userConfig")
		
		window.localStorage.clear()

		if(userConfig != null){
			window.localStorage.setItem("userConfig",userConfig)
		}


		this.bLoading = true
		this.useSessionStorage = loginData.rememberUser;

		this.authLogin.restore();
		this.authLogin.setUserLogin(loginData.login);
		this.authLogin.setPasswordLogin(loginData.password);
		this.authLogin.setGrantType(GrantTypeEnum.password);

		// Requisição para autenticação
		this.authLogin.getToken().subscribe(data=> {
			if ('access_token' in data){
				this.createSessionStorage(data);
				
				let redirect = this.routingService.getPreviousUrl('login');
				this.insertInfoPerfil(redirect);
				this.legalprocess.restore();
				this.legalprocess.get('empresaLogada', 'Busca Empresa').subscribe((data) => {
					if (this.juriGenerics.changeUndefinedToEmpty(data.codEmp) != ""){
						window.localStorage.setItem('codEmp',data.codEmp);
					}

					if (this.juriGenerics.changeUndefinedToEmpty(data.filLog) != ""){
						window.localStorage.setItem('filLog',data.filLog);
					}
				})
				this.bLoading = false
			} else {
				this.bLoading = false
			}

		}, ()=>{
			this.bLoading = false
		});
	}

	/**
	 * Método para criação do Session Storage
	 * @param token 
	 */
	createSessionStorage(token: any){
		window.localStorage.setItem('TLToken',token.access_token);
		window.localStorage.setItem('TLTokenRefresh',token.refresh_token);
		window.localStorage.setItem('TLAuthType',token.token_type);
		window.localStorage.setItem('rememberUser',this.useSessionStorage.toString());
		window.localStorage.setItem('lastUse', Date.now().toString() );		
		window.localStorage.setItem('assuntoJuri', '');//para guardar o ultimo assunto cadastrado pelo usuario
	};

	/**
	 * Busca os dados do usuário 
	 * @param urlDest - Url destino
	 */
	insertInfoPerfil(urlDest: any){
		this.userInfo.restore();

		this.userInfo.getUserId().subscribe(data=>{
			this.userInfo.setCodUsuario(data.userID);
			
			this.userInfo.restore();
			if (this.userInfo.getCodUsuario() != undefined && this.userInfo.getCodUsuario() != '') { 
				this.userInfo.getUserInfo().subscribe(data=>{
					this.userInfo.setEmailUsuario(data.emails[0].value);
					this.userInfo.setNomeUsuario(data.displayName);
					this.router.navigateByUrl(urlDest);
				});
			}
		});
	}
}