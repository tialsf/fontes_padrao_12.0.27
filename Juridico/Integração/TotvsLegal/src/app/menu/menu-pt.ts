/**
* Arquivo de constantes para tradução de Menu - Português
*/

//-- Constantes para Menu 
export const menuPrincipal = {
	meusProcessos: "Meus Processos",
	processos:     "Processos",
	relatorios:    "Relatórios",
	ajuda:         "Ajuda",
	contratos:     "Contratos",
	meusContratos: "Meus Contratos",
	tipoContrato:  "Tipo de Contrato",
	cadastros:     "Cadastros",
	usuarios:      "Usuários",
	exit:          "Sair",
	prefUsuario:   "Preferências do usuário"
}

//-- Constantes para Menu principal
export const menuPt = {	
	menuP: menuPrincipal
}
