import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { ProvisaoComponent } from '../provisao/provisao.component';
import { FavoritosComponent } from '../favoritos/favoritos.component';
import { AcompanhamentoComponent } from '../acompanhamento/acompanhamento.component';
import { PesqAvancadaComponent } from '../pesq-avancada/pesq-avancada.component';
import { MenuComponent } from './menu.component';
import { HomeContratosComponent } from '../home-contratos/home-contratos.component';
import { SlaJuridicoComponent } from '../sla-juridico/sla-juridico.component';
import { AtividadesComponent } from '../atividades/atividades.component';

const routes: Routes = [
	{
		path: '',
		component: MenuComponent,
		children: [
			{
				path: '',
				component: HomeComponent
			},
			{
				path: 'home',
				component: HomeComponent
			},
			{
				path: 'provisao',
				component: ProvisaoComponent
			},
			{
				path: 'processos/favoritos',
				component: FavoritosComponent
			},
			{
				path: 'contratos/favoritos',
				component: FavoritosComponent
			},
			{
				path: 'acompanhamento',
				component: AcompanhamentoComponent
			},
			{
				path: 'pesquisa',
				component: PesqAvancadaComponent
			},
			{
				path: 'processo',
				loadChildren: () => import('../processo/processo.module').then(m => m.ProcessoModule)
			},
			{
				path: 'homeContratos',
				component: HomeContratosComponent
			},
			{
				path: 'slaJuridico',
				component: SlaJuridicoComponent
			},
			{
				path: 'contrato',
				loadChildren: () => import('../contrato/contrato.module').then(m => m.ContratoModule)
			},
			{
				path: 'Cadastros',
				loadChildren: () => import('../cadastros/cadastros.module').then(m => m.CadastrosModule)
			},
			{
				path: 'Usuarios',
				loadChildren: () => import('../usuarios/usuarios.module').then(m => m.UsuariosModule)
			},
			{
				path: 'Distribuicao',
				loadChildren: () => import('../distribuicao/distribuicao.module').then(m => m.DistribuicaoModule)
			},
			{
				path: 'PainelAtividades',
				loadChildren: () => import('../atividades/atividades.module').then(m => m.AtividadesModule)
			}
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class MenuRoutingModule { }
