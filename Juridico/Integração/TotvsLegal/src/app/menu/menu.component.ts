import { Component, OnInit } from '@angular/core';
import { Router, UrlSegment } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { PoToolbarProfile, PoToolbarAction, PoI18nService } from '@po-ui/ng-components';
import { TLUserInfo, AuthloginService } from '../services/authlogin.service';
import { NotificationService } from '../services/notification.service';
import { NotificationAction } from '../shared/structs/notification-action.interface';

interface LinkDocRefencia {
	rotina: string;
	url: string;
	firstChild?: string;
}

@Component({
	selector: 'app-menu',
	templateUrl: './menu.component.html',
	styleUrls: ['./menu.component.css'],
})
export class MenuComponent implements OnInit {
	litMenu: any;
	title = 'TOTVS JURÍDICO'; // Titulo do produto

	listDocRef: Array<LinkDocRefencia> = [
		{ rotina: '/', url: 'https://tdn.totvs.com/x/YpgIHw' }, // Link: TOTVS Legal - Meus Processos
		{ rotina: 'home', url: 'https://tdn.totvs.com/x/YpgIHw' }, // Link: TOTVS Legal - Meus Processos
		{ rotina: 'homeContratos', url: 'https://tdn.totvs.com/x/2qVvI' }, // Link: TOTVS Legal - Meus Contratos
		{ rotina: 'processo', url: 'https://tdn.totvs.com/x/YpgIHw' }, // Link: TOTVS Legal - Meus Processos
		{ rotina: 'altProcesso', url: 'https://tdn.totvs.com/x/Nx2PI' }, // Link: TOTVS Legal - Detalhes do processo
		{ rotina: 'contrato', url: 'https://tdn.totvs.com/x/2qVvI' }, // Link: TOTVS Legal - Meus Contratos
		{ rotina: 'Cadastros', url: 'https://tdn.totvs.com/x/EBfNI' }, // Link: Totvs Legal - Gerenciar Cadastros Básicos
		{ rotina: 'anexos', url: 'https://tdn.totvs.com/x/Hwt2Hw' }, // Link: TOTVS Legal - Anexos
		{ rotina: 'andamento', url: 'https://tdn.totvs.com/x/sjFXHw' }, // Link: TOTVS Legal - Andamentos
		{ rotina: 'tarefa', url: 'https://tdn.totvs.com/x/BeCVI' }, // Link: TOTVS Legal - Prazos e tarefas
		{ rotina: 'garantia', url: 'https://tdn.totvs.com/x/kpcXHw' }, // Link: TOTVS Legal - Garantias
		{ rotina: 'levantamento', url: 'https://tdn.totvs.com/x/kpcXHw' }, // Link: TOTVS Legal - Garantias
		{ rotina: 'despesa', url: 'https://tdn.totvs.com/x/E1pXHw' }, // Link: TOTVS Legal - Despesas
		{ rotina: 'pedido', url: 'https://tdn.totvs.com/x/OKgIHw' }, // Link: TOTVS Legal - Pedidos
		{ rotina: 'provisao', url: 'https://tdn.totvs.com/x/YpgIHw' }, // Link: TOTVS Legal - Meus Processos
		{ rotina: 'pesquisa', url: 'https://tdn.totvs.com/x/pg5XHw' }, // Link: TOTVS Legal - Pesquisa Avançada
		{ rotina: 'historico', url: 'https://tdn.totvs.com/x/P2zNI' }, // Link: TOTVS Legal - Histórico de alteração
		{ rotina: 'favoritos', url: 'https://tdn.totvs.com/x/DxfyHg' }, // Link: Totvs Legal - Favoritos
		{ rotina: 'tipocontrato', url: 'https://tdn.totvs.com/x/90aPI' }, // Link: Totvs Legal - Tipos de Contratos
		{ rotina: 'slaJuridico', url: 'https://tdn.totvs.com/x/B5WvI' }, // Link: TOTVS Legal - SLA Jurídico
		{ rotina: 'unidades', url: 'https://tdn.totvs.com/x/22HNI' }, // Link: TOTVS Legal - Unidades
		{ rotina: 'Usuarios', url: 'https://tdn.totvs.com/x/Gj-oI' }, // Link: TOTVS Legal - Gerenciar Usuários
		{ rotina: 'perfil', url: 'https://tdn.totvs.com/x/MUDoI' }, // Link: TOTVS Legal - Perfil do Usuário
		{ rotina: 'grupos', url: 'https://tdn.totvs.com/x/aUDoI' }, // Link: TOTVS Legal - Gerenciar Grupos de Usuários
		{ rotina: 'grupo', url: 'https://tdn.totvs.com/x/NNIpIQ' }, // Link: TOTVS Legal - Grupos de usuários
		{ rotina: 'credenciados', url: 'https://tdn.totvs.com/x/x3LoI' }, // Link: Totvs Legal - Gerenciar Cadastro de escritórios credenciados
		{ rotina: 'fornecedores', url: 'https://tdn.totvs.com/x/6nroI' }, // Link: Totvs Legal - Gerenciar Cadastro de fornecedores
		{ rotina: 'Distribuicao', url: 'https://tdn.totvs.com/x/YbgYIQ' }, // Link: TOTVS Jurídico - Painel de Distribuição
		{ rotina: 'PainelAtividades', url: 'https://tdn.totvs.com/x/BhM4IQ' }, // Link: TOTVS Jurídico - Painel de atividades

		// Link: TOTVS Legal - Criar novo processo
		{ firstChild: 'processo', rotina: 'cadastro', url: 'https://tdn.totvs.com/x/VQUAHw' },
		// Link: TOTVS Legal - Acompanhamento/Evolução
		{ firstChild: 'processo', rotina: 'acompanhamento', url: 'https://tdn.totvs.com/x/EQCaHw' },
		// Link: TOTVS Legal - Criar novo contrato
		{ firstChild: 'contrato', rotina: 'cadastro', url: 'https://tdn.totvs.com/x/_Jx-I' },
		// Link: TOTVS Legal - Acompanhamento/Valores por tipo de contrato
		{ firstChild: 'contrato', rotina: 'acompanhamento', url: 'https://tdn.totvs.com/x/u_WFI' },
		// Link: TOTVS Jurídico - Preferência de Usuário
		{ firstChild: 'Usuarios', rotina: 'config', url: 'https://tdn.totvs.com/x/60kPIQ' },
		// Link: TOTVS Jurídico - Preferência de Usuário
		{ firstChild: 'Distribuicao', rotina: 'importacao', url: 'https://tdn.totvs.com/x/FTlZIQ' },
	];

	toolbarActions: Array<PoToolbarAction> = [
		{ label: 'Ajuda', icon: 'po-icon-help', action: item => this.mostraDocReferencia() },
		{
			label: 'Cadastros',
			icon: 'po-icon-settings',
			separator: true,
			action: item => this.navigate('Cadastros'),
		},
		{
			label: 'Usuários',
			icon: 'po-icon-users',
			separator: true,
			action: item => this.navigate('Usuarios'),
		},
	];

	// Perfil do usuário
	profileUser: PoToolbarProfile = {
		avatar: '',
		title: this.infoUser.getNomeUsuario(),
		subtitle: this.infoUser.getEmailUsuario(),
	};

	/**
	 * Criação dos itens para o menu do Perfil
	 */
	profileActions: Array<PoToolbarAction> = [
		{
			icon: 'po-icon-settings',
			label: 'Preferências de usuário',
			action: item => this.navigate('/Usuarios/config'),
		},
		{ icon: 'po-icon-exit', label: 'Exit', action: item => this.logoff(), type: 'danger' },
	];

	// Menus
	menus = [
		{ label: ' ', link: '/home', icon: 'po-icon-archive', shortLabel: ' ' },
		{ label: ' ', link: '/homeContratos', icon: 'po-icon-news', shortLabel: ' ' },
	];

	/**
	 * Variável que alimenta a lista de notificações do Menu!
	 */
	listNotifications: BehaviorSubject<Array<NotificationAction>>;

	constructor(
		private infoUser: TLUserInfo,
		private thfI18nService: PoI18nService,
		private router: Router,
		private authloginService: AuthloginService,
		protected notificationService: NotificationService
	) {
		// -- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'menu' no arquivo JurTraducao.
		this.thfI18nService
			.getLiterals({ context: 'menu', language: this.thfI18nService.getLanguage() })
			.subscribe(litMenu => {
				this.litMenu = litMenu;

				// -- seta as constantes para tradução do Menu - menu-pt.ts
				this.menus[0].label = this.litMenu.menuP.meusProcessos;
				this.menus[0].shortLabel = this.litMenu.menuP.processos;

				this.menus[1].label = this.litMenu.menuP.meusContratos;
				this.menus[1].shortLabel = this.litMenu.menuP.contratos;

				// -- seta as constantes para tradução do Menu no action
				this.toolbarActions[0].label = this.litMenu.menuP.ajuda;
				this.toolbarActions[1].label = this.litMenu.menuP.cadastros;
				this.toolbarActions[2].label = this.litMenu.menuP.usuarios;

				// -- seta as constantes para tradução do Profile Action
				this.profileActions[0].label = this.litMenu.menuP.prefUsuario;
				this.profileActions[1].label = this.litMenu.menuP.exit;
			});
		this.listNotifications = this.notificationService.subListNotify;
	}

	ngOnInit(): void {
		this.setDadosUser();
		this.keepLogged();

		// Inicializa a busca das notificações
		this.notificationService.start();
	}

	/**
	 * Função responsavel pelo preenchimentos dos dados de usuários
	 */
	setDadosUser() {
		if (this.infoUser.getCodUsuario() == undefined || this.infoUser.getCodUsuario() == '') {
			this.infoUser.restore();

			this.infoUser.getUserId().subscribe(idReturn => {
				this.infoUser.setCodUsuario(idReturn.userID);
				this.infoUser.restore();
				this.infoUser.getUserInfo().subscribe(user => {
					this.infoUser.setEmailUsuario(user.emails[0].value);
					this.infoUser.setNomeUsuario(user.displayName);

					// Perfil do usuário
					this.profileUser = {
						avatar: '',
						title: this.infoUser.getNomeUsuario(),
						subtitle: this.infoUser.getEmailUsuario(),
					};
				});
			});
		}
	}

	/**
	 * Renova o token a cada 50 minutos
	 *
	 * @memberof MenuComponent
	 */
	keepLogged() {
		setTimeout(() => {
			this.authloginService.isLogged();
			this.keepLogged();
		}, 3005000);
	}

	/**
	 * Logoff dos usuários
	 */
	logoff() {
		this.notificationService.subscription.unsubscribe();
		this.listNotifications.next([]);

		this.infoUser.clearUser();
		this.router.navigate(['/login']);
	}

	/**
	 * Função responsável por realizar a abertura do documento de refência da rotina.
	 */
	mostraDocReferencia() {
		let children = this.router.parseUrl(this.router.url).root.children['primary'];
		let rotas: Array<UrlSegment> =
			children != undefined ? children.segments : [{ path: '/' } as UrlSegment];
		let firstChild = rotas[0].path;
		let urlDocRef: string = '';

		for (let index = rotas.length - 1; index > -1; index--) {
			urlDocRef = this.linkDocRef(rotas[index].path, firstChild);

			if (urlDocRef != '') {
				break;
			}
		}

		window.open(urlDocRef != '' ? urlDocRef : this.linkDocRef('/'), '_blank');
	}

	/**
	 * Localiza o link do documento de referência da rotina na listDocRef
	 * @param rotina - nome da rotina que está aberta no totvs legal
	 */
	linkDocRef(rotina: string, firstChild?: string) {
		if (rotina == 'acompanhamento') {
			if (this.router.url.includes('&assJur=006')) {
				firstChild = 'contrato';
			} else {
				firstChild = 'processo';
			}
		}
		let index = this.listDocRef.findIndex(x => {
			if (x.hasOwnProperty('firstChild')) {
				return x.firstChild == firstChild && x.rotina == rotina;
			} else {
				return x.rotina == rotina;
			}
		});
		return index > -1 ? this.listDocRef[index].url : '';
	}

	/**
	 * Função responsável para mostrar a quantidade de notificações não lidas
	 */
	getNotificationNumber() {
		return this.listNotifications.getValue().filter(not => not.type === 'danger').length;
	}

	/**
	 * Método responsavel pelo redirecionamento da rota conforme a ação selecionada
	 * @param newRoute string contendo a url que será redirecionada
	 */
	navigate(newRoute: string) {
		this.router.navigate([newRoute]);
	}
}
