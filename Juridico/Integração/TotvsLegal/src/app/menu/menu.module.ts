import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuRoutingModule } from './menu-routing.module';
import { HomeComponent } from '../home/home.component';
import { PoModule } from '@po-ui/ng-components';
import { ProvisaoComponent } from '../provisao/provisao.component';
import { AcompanhamentoComponent } from '../acompanhamento/acompanhamento.component';
import { PesqAvancadaComponent } from '../pesq-avancada/pesq-avancada.component';
import { FavoritosComponent } from '../favoritos/favoritos.component';
import { ChartsModule } from 'ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MenuComponent } from './menu.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { UploadInterceptor } from '../upload.interceptor';
import { SharedModule } from "../shared/shared.module";
import { HomeContratosComponent } from '../home-contratos/home-contratos.component';

import { AnexosComponent } from '../anexos/anexos.component';
import { TarefasComponent } from '../tarefas/tarefas.component';
import { DetTarefaComponent } from '../tarefas/det-tarefa/det-tarefa.component';
import { DespesasComponent } from '../despesas/despesas.component';
import { DetDespesaComponent } from '../despesas/det-despesa/det-despesa.component';
import { HistoricoComponent } from '../historico/historico.component';
import { AndamentosComponent } from '../andamentos/andamentos.component';
import { DetAndamentoComponent } from '../andamentos/det-andamento/det-andamento.component';
import { SlaJuridicoComponent } from '../sla-juridico/sla-juridico.component';
import { CadastrosModule } from '../cadastros/cadastros.module';

@NgModule({
	declarations: [
		MenuComponent,
		HomeComponent,
		ProvisaoComponent,
		AcompanhamentoComponent,
		PesqAvancadaComponent,
		FavoritosComponent,
		HomeContratosComponent,	
		AnexosComponent,
		TarefasComponent,
		DetTarefaComponent,
		DespesasComponent,
		DetDespesaComponent,
		HistoricoComponent,
		AndamentosComponent,
		DetAndamentoComponent,
		SlaJuridicoComponent
	],
	
	imports: [
		CommonModule,
		MenuRoutingModule,
		ChartsModule,
		PoModule,
		FormsModule,
		ReactiveFormsModule,
		SharedModule,
		CadastrosModule
	],
	providers: [{
		provide: HTTP_INTERCEPTORS,
		useClass: UploadInterceptor,
		multi: true,
	}],
})
export class MenuModule { }
