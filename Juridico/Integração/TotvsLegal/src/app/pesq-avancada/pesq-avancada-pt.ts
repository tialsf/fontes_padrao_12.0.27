/**
* Arquivo de constantes para tradução da tela de Pesquisa Avançada
*/

//--  Constantes para o breadcrumb de Processo
const breadcrumb = {
	meusProcessos : "Meus processos",
	pesqAva       : "Pesquisa em processos",
}

const atalhoData = {
	hoje      : "Hoje", 
	semanaAtu : "Esta semana",
	semanaAnt : "Semana passada",
	mesAtual  : "Neste mês",
	mesAnte   : "Mês passado"
}

const colPesquisa = {
	nomeProc : "Nome do processo",
	dataProv : "Data de provisão", 
	vlrProv  : "Valor de provisão (R$)",
	prognost : "Prognóstico",
	processFilial: "Filial"
} 

const formPesqAvan = {
	filtSalv       : "Filtros salvos ", 
	expRelat       : "Exportar relatório",
	salvaFil       : "Salvar filtro",
	campoProc      : "Campo de processo",
	atalhoData     : "Atalho", 
	dataInicio     : "Data inicial",
	dataFinal      : "Data final",
	adicFiltro     : "Adicionar filtro",
	seleciona      : "Selecione a opção para pesquisa",
	texto          : "Digite o termo para ser pesquisado",
	valor          : "Informe o valor para ser pesquisado",
	mosResultDisc  : "Apresentando resultados filtrados por:"
}

//-- Constantes principais
export const PesqAvanPt = {
	bread              : breadcrumb,
	formPesqAvan       : formPesqAvan,
	colPesquisa        : colPesquisa,
	atalhoData         : atalhoData,
	nenhumFiltroSelec  : "Para consultar os processos selecione 1 ou mais itens de controle de processo.",
	loadPesqAvan       : "Carregando",
	loadExport         : "Exportando relatório",
	tituloPesqAvan     : "Pesquisa em processos",
	anterior           : "<<  Anterior",
	proximo            : "Próximo  >>",
	procLocalizados    : " registros localizados",
	msgExportaSucess   : "Exportação realizada com sucesso!",
	msgExportaError    : "Não foi possível exportar o relatório!",
	placeHoldCmb       : "Modelos de Exportação",
	switchLabel        : "Exportar todos os processos",
	toolTipTodosProc   : "Ao ligar esta opção, serão exportados todos os processos da base. A exportação pode ser demorada.",
	atention           : "Atenção!!"
}