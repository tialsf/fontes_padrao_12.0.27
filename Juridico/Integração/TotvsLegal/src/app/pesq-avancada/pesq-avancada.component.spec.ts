import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesqAvancadaComponent } from './pesq-avancada.component';

describe('PesqAvancadaComponent', () => {
  let component: PesqAvancadaComponent;
  let fixture: ComponentFixture<PesqAvancadaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesqAvancadaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesqAvancadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
