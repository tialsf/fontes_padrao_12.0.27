import { Component, OnInit, ViewChild } from '@angular/core';
import {
	PoSelectOption,
	PoDisclaimer,
	PoTableColumn,
	PoBreadcrumb,
	PoNotificationService,
	PoI18nService,
	PoComboComponent,
	PoSelectComponent,
	PoDialogService
} from '@po-ui/ng-components';
import { JurigenericsService, JurBase64 } from '../services/jurigenerics.service';
import { LegalprocessService } from '../services/legalprocess.service';
import { PesqAvancadaAdapterService, LpComboPesqReturnStruct, f3AdapterFilterParam, f3FieldsAdapter } from '../services/pesqAvancada-adapter.service';
import { FwmodelAdaptorService, adaptorFilterParam } from 'src/app/services/fwmodel-adaptor.service';
import { FwmodelService } from '../services/fwmodel.service';
import { TLUserInfo } from '../services/authlogin.service';
import { UserConfig } from '../usuarios/user-config/user-config.component';

@Component({
  selector: 'app-pesq-avancada',
  templateUrl: './pesq-avancada.component.html',
  styleUrls: ['./pesq-avancada.component.css']
})
export class PesqAvancadaComponent implements OnInit {
	litDtPqAvan           : any;
	titleDet              : string;
	tipoCampoSelecionado  : string;
	campoProcesso         : string;
	comboPesquisa         : string;
	f3Value               : string;
	memoValue             : string;
	textValue             : string;
	filial                : string ='';
	codPro                : string = '';
	campoSelecionadoValue : string | number;
	dataInicio            : string;
	dataFinal             : string;
	atalhoData            : string;
	exportacao            : string;
	numberValue           : number;
	page                  : number = 0;
	nResultPesquisa       : number = 0;
	maxPage               : number = 1;
	pageSize              : number = 10;
	HasDisclaimer         : boolean = false;
	hasData               : boolean = false;
	btnPreDisable         : boolean = false;
	btnNxtDisable         : boolean = false;
	isHideLoadingExporta  : boolean = false;
	isHideLoadingPesqAvan : boolean = false;
	bFilial               : boolean = false;
	bAddDisclaimer        : boolean = true;
	bDisableAddFilter     : boolean = true;
	lDisabledExp          : boolean = true;
	hasModeloExportacao   : boolean = false;
	isNoFilter            : boolean = false;
	isSegundoPlano        : boolean = true;
	columns               : Array<PoTableColumn>;
	f3AdapterParam        : f3AdapterFilterParam;
	listDadosCombo        : Array<comboStruct>          = [];
	listResultPesq        : Array<itemGridProcesso>     = [];
	disclaimers           : Array<disclaimerItemStruct> = [];
	listUserConfig        : Array<UserConfig>           = [];
	listAtaData           : Array<PoSelectOption>       = [
		{ label: " --- "          , value: '1' },
		{ label: "Hoje"           , value: '2' },
		{ label: "Esta semana"    , value: '3' },
		{ label: "Semana passada" , value: '4' },
		{ label: "Neste mês"      , value: '5' },
		{ label: "Mês passado"    , value: '6' }
	];

	colPesquisa : Array<PoTableColumn> = [
		{ property: 'processFilial' , width: '100px', label : 'Filial', type : 'link',
			action: (value, row) => {
				this.openProcesso(row.processFilial, row.processId);
			}
		},
		
		{ property: 'nomeProc' , label : 'Nome do processo', type : 'link',
			action: (value, row) => {
				this.openProcesso(row.processFilial, row.processId);
			}
		},
		
		{ property: 'dataProv' , width: '200px', label: 'Data de provisão', type : 'link',
			action: (value, row) => {
				this.openProcesso(row.processFilial, row.processId);
			}
		},

		{ property: 'prognostico' , width: '200px', label: 'Prognóstico', type : 'link',
			action: (value, row) => {
				this.openProcesso(row.processFilial, row.processId);
			}
		},

		{ property: 'valorProv' , width: '200px', label: 'Valor de provisão (R$)' , type : 'number', format: '1.2-5'}
	];

	public readonly tipoExportacao: Array<object> = [
		{ label: 'Excel', action: this.exportaRelatorio.bind(this) }
	];

	
	openProcesso(filPro: string, codPro: string){
		window.open('/processo/' +  encodeURIComponent(btoa(filPro)) + "/" +  encodeURIComponent(btoa(codPro)));
	}
	
	/**
	 * estrutura de navegação da URL atual
	 */
	public breadcrumb: PoBreadcrumb = {
		items: [
			{ label: ' ', link: '/' },
			{ label: ' ' }
		],
	};

	//-- Adapter para Combo de Lista de Modelos de Exportação
	adaptorParamModelosExp: adaptorFilterParam;

	@ViewChild("cmbSelectCampo",{ static: true  }) cmbCampos  : PoComboComponent;
	@ViewChild("cmbF3List") cmbCampoF3 : PoComboComponent;
	@ViewChild("cmbCombo"      ,{ static: true  }) cmbComboSel: PoComboComponent;
	@ViewChild("selSugestData" ,{ static: true  }) selSugData : PoSelectComponent;

	constructor(
		public    pesqAvancadaAdapter:   PesqAvancadaAdapterService,
		public    fwModelAdaptorService: FwmodelAdaptorService,
		public    fwmodel:               FwmodelService,
		public    f3Adapter:             f3FieldsAdapter,
		private   legalprocess:          LegalprocessService,
		private   JuriGenerics:          JurigenericsService,
		protected thfI18nService:        PoI18nService,
		protected notification:          PoNotificationService,
		private   userInfo:              TLUserInfo,
		private   poDialog:              PoDialogService
	) { 
		//-- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'pesquisa avançada' no arquivo JurTraducao.
		thfI18nService.getLiterals({ context: 'pesqAvan', language: thfI18nService.getLanguage() })
		.subscribe((litPesq) => {
			this.litDtPqAvan = litPesq;

			this.breadcrumb.items =[
				{ label: this.litDtPqAvan.bread.meusProcessos, link: '/' },
				{ label: this.litDtPqAvan.tituloPesqAvan}
			];
			this.titleDet = this.litDtPqAvan.tituloPesqAvan;
			
			//-- seta as constantes para tradução do atalho de Datas
			this.listAtaData[1].label = this.litDtPqAvan.atalhoData.hoje;
			this.listAtaData[2].label = this.litDtPqAvan.atalhoData.semanaAtu;
			this.listAtaData[3].label = this.litDtPqAvan.atalhoData.semanaAnt;
			this.listAtaData[4].label = this.litDtPqAvan.atalhoData.mesAtual;
			this.listAtaData[5].label = this.litDtPqAvan.atalhoData.mesAnte;
			
			//-- seta as constantes para tradução da table de Pesquisa
			this.colPesquisa[0].label = this.litDtPqAvan.colPesquisa.processFilial;
			this.colPesquisa[1].label = this.litDtPqAvan.colPesquisa.nomeProc;
			this.colPesquisa[2].label = this.litDtPqAvan.colPesquisa.dataProv;
			this.colPesquisa[3].label = this.litDtPqAvan.colPesquisa.prognost;
			this.colPesquisa[4].label = this.litDtPqAvan.colPesquisa.vlrProv;
		})
		if(window.localStorage.getItem('userConfig') != null){
			this.listUserConfig = JSON.parse(window.localStorage.getItem('userConfig'));
		}

		this.adapterModelExp();
		this.mostraColunaFilial();
		this.getUserConfig();
	}

	ngOnInit() {
		this.restaurar();
		this.pagination();
		this.getModelosExport();
		this.page                 = 1;
		this.HasDisclaimer        = false;
		this.columns              = this.colPesquisa;
		this.isHideLoadingExporta = true;
		this.lDisabledExp         = true;
		this.exportacao           = '';
	}

	/**
	* Adiciona o filtro no disclaimer e apresenta a Label na view do campo filtrado.
	*/
	addDisclaimer() {
		let campoSelec = this.campoProcesso;
		var disclaimer : disclaimerItemStruct =  this.getLabelDisclamer(campoSelec);

		if (this.disclaimers.findIndex( item => item.value == disclaimer.value && item.field == disclaimer.field) == -1){
			this.disclaimers = [...this.disclaimers, disclaimer];
		}

		if ( this.disclaimers.length > 0) {
			this.HasDisclaimer = true;
		} else {
			this.HasDisclaimer = false;
			this.bAddDisclaimer = true;
		}
		this.restaurar();
	}

	/**
	 * Manipula se o campo de "Adicionar Filtro" está habilitado ou não
	 */
	getVldButtonDisclaimer() {
		switch (this.tipoCampoSelecionado) {
			case "D":
				this.bDisableAddFilter = (this.dataInicio == "" || this.dataFinal == "")
				break;
			case "COMBO":
				this.bDisableAddFilter = (this.cmbComboSel.selectedOption == undefined)
				break;
			case "F3":
				this.bDisableAddFilter = (this.cmbCampoF3.selectedOption == undefined)
				break;
			case "N":
				this.bDisableAddFilter = false;
				break;
			default:
				this.bDisableAddFilter = (this.JuriGenerics.changeUndefinedToEmpty(<string>this.campoSelecionadoValue) == "")
				break;
		}
	}

	/**
	 * Retorna o Label que será apresentado na view e estrutura os dados para adicionar 
	 * no body do serviços de pesquisa avançada.
	 * @param campoSelec - Campo a ser manipulado
	 */
	getLabelDisclamer(campoSelec ?: string): disclaimerItemStruct{
		let itemSelecionado: LpComboPesqReturnStruct = (<LpComboPesqReturnStruct> this.cmbCampos.selectedOption)
		let disclaimerItem = new disclaimerItemStruct();
		let content    : any;
		let tpCampo    = "";
		let where      = "";
		let label      = "";

		if (this.campoProcesso != undefined && this.campoProcesso != "") {
			tpCampo    = itemSelecionado.type
			where      = itemSelecionado.codigo
			label      = itemSelecionado.label

			switch (tpCampo) {
				case "D":
					let dataIniForm = this.dataInicio;
					let dataFinForm = this.dataFinal;
					
					let dataIni  = this.JuriGenerics.makeDate(dataIniForm.replace("-",'').replace('-',''),"dd/mm/yyyy");
					let dataFin  = this.JuriGenerics.makeDate(dataFinForm.replace('-','').replace('-',''),"dd/mm/yyyy");
	
					label   += ": De " + dataIni + " até " + dataFin;
					content = [this.dataInicio, this.dataFinal];
					break;
	
				case "C":
					label += ": " + this.campoSelecionadoValue;
					content = this.campoSelecionadoValue;
					break;
	
				case "M":
					label += ": " + this.campoSelecionadoValue;
					content = this.campoSelecionadoValue;
					break;
	
				case "COMBO":
					label += ": " + this.cmbComboSel.selectedOption.label
					content = this.cmbComboSel.selectedOption.value
					break;
	
				case "F3":
					label += ": " + this.cmbCampoF3.selectedOption.label
					content = this.cmbCampoF3.selectedOption.value
					break;
	
				case "N":
					label = label + ": " + this.campoSelecionadoValue;
					content = this.campoSelecionadoValue;
					break;
			}

			// Setando os valores no objeto
			disclaimerItem.$id = campoSelec + content
			disclaimerItem.value = content
			disclaimerItem.type = tpCampo
			disclaimerItem.condition = where
			disclaimerItem.label = label
			disclaimerItem.field = campoSelec
		}
		return disclaimerItem;
	}

	/**
	 * Habilita o campo a partir do tipo de Campo que foi selecionado no Select Principal
	 */
	getDadosPesq(){
		let itemSelecionado: LpComboPesqReturnStruct = (<LpComboPesqReturnStruct> this.cmbCampos.selectedOption)
		if (itemSelecionado != undefined){
			this.tipoCampoSelecionado = itemSelecionado.type
			switch (this.tipoCampoSelecionado) {
				case "D":
					this.selSugData.writeValue("");
					this.dataFinal = ""
					this.dataInicio = ""
					this.getVldButtonDisclaimer();
					break;
				case "C":
					this.campoSelecionadoValue = ""
					break;
				case "M":
					this.campoSelecionadoValue = ""
					break;
				case "COMBO":
					this.listDadosCombo = []
					this.cmbComboSel.writeValue("")
					itemSelecionado.comboOptions.forEach(item=>{
						let newItemList = new comboStruct()
						newItemList.value = item[0]
						newItemList.label = item[1]

						this.listDadosCombo.push(newItemList);
					})
					break;
				case "F3":
					this.cmbCampoF3.filterService = this.f3Adapter;
					this.cmbCampoF3.filterParams = new f3AdapterFilterParam(itemSelecionado.f3fields)
					this.cmbCampoF3.writeValue("")
					break;
				case "N":
					this.campoSelecionadoValue = 0
					this.bDisableAddFilter = false
					break;
			}
		} else {
			this.tipoCampoSelecionado = undefined
			this.bDisableAddFilter    = true
			this.lDisabledExp         = true;
			this.exportacao           = "";
		}
	}

	/**
	 * Sugestão de data, conforme o atalho selecionado no HTML.
	 */	
	getSugestData(){
		let atalhoSelec = this.atalhoData;
		let dataHoje    = new Date ();
		let sugeDtIni   : any;
		let sugeDtFin   : any;

		switch (atalhoSelec) {
			case "1": //Vazio
				this.dataInicio = sugeDtIni;
				this.dataFinal  = sugeDtFin;
				break;
			
			case "2": //Hoje
				sugeDtIni = this.JuriGenerics.makeDateProtheus();
				sugeDtIni = this.JuriGenerics.makeDate(sugeDtIni,"yyyy-mm-dd");
				sugeDtFin = this.JuriGenerics.makeDateProtheus();
				sugeDtFin = this.JuriGenerics.makeDate(sugeDtFin,"yyyy-mm-dd");

				this.dataInicio = sugeDtIni;
				this.dataFinal  = sugeDtFin;
				break;
			
			case "3": //Esta Semana
				
				sugeDtIni = dataHoje.getFullYear()+("0"+(dataHoje.getMonth()+1))+dataHoje.getDate();
				sugeDtIni = dataHoje.setDate(dataHoje.getDate()-dataHoje.getDay());
				sugeDtIni = dataHoje.toISOString().substring(0,10);

				sugeDtFin = dataHoje.getFullYear()+("0"+(dataHoje.getMonth()+1))+dataHoje.getDate();
				sugeDtFin = dataHoje.setDate(dataHoje.getDate()-dataHoje.getDay()+6);
				sugeDtFin = dataHoje.toISOString().substring(0,10);

				this.dataInicio = sugeDtIni;
				this.dataFinal  = sugeDtFin;
				break;

			case "4": // Semana passada

				sugeDtIni = dataHoje.getFullYear()+("0"+(dataHoje.getMonth()+1))+dataHoje.getDate();
				sugeDtIni = dataHoje.setDate(dataHoje.getDate()-dataHoje.getDay()-7);
				sugeDtIni = dataHoje.toISOString().substring(0,10);

				sugeDtFin = dataHoje.getFullYear()+("0"+(dataHoje.getMonth()+1))+dataHoje.getDate();
				sugeDtFin = dataHoje.setDate(dataHoje.getDate()-dataHoje.getDay()+6);
				sugeDtFin = dataHoje.toISOString().substring(0,10);
	
				this.dataInicio = sugeDtIni;
				this.dataFinal  = sugeDtFin;
				break;

			case "5": //Neste Mês
				sugeDtIni = dataHoje.getFullYear().toString()+("0"+(dataHoje.getMonth()+1).toString()).substring(("0"+(dataHoje.getMonth()+1).toString()).length-2)+"01";
				sugeDtIni = this.JuriGenerics.makeDate(sugeDtIni,"yyyy-mm-dd");

				sugeDtFin = dataHoje.setMonth(dataHoje.getMonth()+1);
				sugeDtFin = dataHoje.setDate(dataHoje.getDate()-dataHoje.getDate());
				sugeDtFin = dataHoje.toISOString().substring(0,10);

				this.dataInicio = sugeDtIni;
				this.dataFinal  = sugeDtFin;
				break;

			case "6": // Mês Passado
				sugeDtIni = dataHoje.getFullYear().toString()+("0"+(dataHoje.getMonth()).toString()).substring(("0"+(dataHoje.getMonth()).toString()).length-2)+"01";
				sugeDtIni = this.JuriGenerics.makeDate(sugeDtIni,"yyyy-mm-dd");

				sugeDtFin = dataHoje.setMonth(dataHoje.getMonth());
				sugeDtFin = dataHoje.setDate(dataHoje.getDate()-dataHoje.getDate());
				sugeDtFin = dataHoje.toISOString().substring(0,10);
				
				this.dataInicio = sugeDtIni;
				this.dataFinal  = sugeDtFin;
				break;
		}

		this.getVldButtonDisclaimer();
	}

	/**
	 * realiza a requisição POST enviando o conteúdo do disclaimer e 
	 * retornando o conteúdo da tabela de processos.
	 * 
	 * @param filter array de objetos disclaimer
	 */
	postFilteredProcesses(filter: Array<PoDisclaimer>){
		this.isHideLoadingPesqAvan = false;
		this.legalprocess.restore();
		this.legalprocess.setPage(this.page.toString()) ; //número da página
		this.legalprocess.setPageSize(this.pageSize.toString()); //quantidade de itens por página
		let body = this.filterBody(filter);

		this.legalprocess.post("setfilter",body).subscribe((data)=>{
			this.nResultPesquisa = data.length; //total de processos retornados pelo WS
			if (data.processes != '' && data.processes != undefined) {

				let arrProcessos: itemGridProcesso [] = []
				data.processes.forEach(process => {
					
					let processo: itemGridProcesso = new itemGridProcesso();
					let valorProvisao = 0;

					if (this.JuriGenerics.changeUndefinedToEmpty(process.provisionAmount) != ""){
						valorProvisao = process.provisionAmount
					}
					
					processo.nomeProc      = process.caseTitle;
					processo.dataProv      = this.JuriGenerics.makeDate(process.provisionDate,"dd/mm/yyyy");
					processo.valorProv     = valorProvisao;
					processo.prognostico   = process.prognostic;
					processo.processId     = process.processId;
					processo.processFilial = process.processFilial;

					arrProcessos.push(processo);

				});
				this.listResultPesq = arrProcessos
				this.hasData        = true;
				this.lDisabledExp   = false;
			} else {
				this.listResultPesq = [];
				this.hasData        = false;
				this.lDisabledExp   = true;
				this.exportacao     = "";
			}
			this.isHideLoadingPesqAvan = true;
			this.setMaxPage();
		});
		return this.hasData;
	}

	/**
	* monta o body para a requisição POST 
	* @param filter array de objetos disclaimer
	* @param bExport boolean responsável por validar a quantidade de registros que retorna (Total ou paginação).
	*/
	filterBody(filter: Array<PoDisclaimer>, bExport?: boolean) {
		let count: number = 0;
		var body: string = '{"filters": [';
		filter.forEach((element) => {
			count++;
			body += JSON.stringify(element) + ', ';
		});

		if (count > 0) {
			body = body.substring(0, body.length - 2);
		}

		body += '], "page": ' + this.page + ', ';
		body += '"pageSize": ' + this.pageSize + ', ';
		body += ' "count": ' + count + ', ';

		if (bExport != undefined) {
			body += ' "export": "true" ,';
			body += ' "expCount": ' + this.nResultPesquisa + ' ,';
			body += ' "isNoFilter": ' + this.isNoFilter + '}';
		} else {
			body += ' "export":  "false" ,';
			body += ' "expCount": 0 ,';
			body += ' "isNoFilter": ' + this.isNoFilter + '}';
		}

		return body;
	}

	/**
	 * disparada quando a lista de disclaimers é modificada
	 * atualiza os processos da tabela
	 */
	changeFilters() {
		this.postFilteredProcesses(this.disclaimers);
		
		if ( this.disclaimers.length > 0) {
			this.HasDisclaimer  = true;
		} else {
			this.HasDisclaimer  = false;
			this.bAddDisclaimer = true;
			this.lDisabledExp   = true;
			this.page           = 1;
			this.exportacao     = "";
		}
	}

	/**
	 * Restaura os valores de todos componentes. 
	 */
	restaurar(){
		this.campoProcesso = "";
		this.tipoCampoSelecionado = "";
		this.bDisableAddFilter = true;

	}

	/**
	 * ação dos botões de paginação Anterior/ Próximo
	 */
	showMore(buttonId: string){
		this.movePage(buttonId);
	}

	/**
	 * ação dos botões Anterior/ Próximo
	 */
	movePage(buttonId: string) {
		this.setMaxPage();

		if(buttonId == "next" && this.page < this.maxPage){
			this.page ++;
			this.pagination();
		}else if(buttonId == "previous" && this.page >= 2){
			this.page --;
			this.pagination();
		}

		this.pagination();
		
		if ( this.postFilteredProcesses(this.disclaimers) ){
			this.columns = this.colPesquisa;
		}
	}
		
	/**
	* ação do botão Pesquisa
	*/
	search() {
		this.page = 1;
		this.postFilteredProcesses(this.disclaimers);
	}
	
	/**
	 * habilita/ desabilita os botões de paginação
	 */
	pagination() {
		this.btnPreDisable = false;
		this.btnNxtDisable = false;

		if(this.page == this.maxPage){
			this.btnNxtDisable = true;
		}
		if(this.page == 1){
			this.btnPreDisable = true;
		}
		
		if (this.listResultPesq.length == 0) {
			this.btnPreDisable = true;
			this.btnNxtDisable = true;
			this.lDisabledExp  = true;
			this.exportacao    = "";
		}
	}

	/**
	 * calcula a quantidade máxima de páginas 
	 */
	setMaxPage(){

		this.maxPage = this.nResultPesquisa/this.pageSize
		if ((this.maxPage - Math.floor(this.maxPage)) > 0){
			this.maxPage = Math.floor(this.maxPage) + 1;
		}

		this.pagination();
	  }	

	/**
	 * Função responsável por gerar a exportação do relatório solicitado pelo usuário
	 */
	exportaRelatorio(cCodModelo: string){

		if(this.JuriGenerics.changeUndefinedToEmpty(cCodModelo) != ''){
			let listProcessExport: Array<itemGridProcesso> = [];
			let Exportbdy = this.filterBody(this.isNoFilter ? [] : this.disclaimers, true);

			this.isHideLoadingExporta = false;
			this.legalprocess.restore();
			this.legalprocess.setPage(this.page.toString()) ; //número da página
			this.legalprocess.setPageSize(this.pageSize.toString()); //quantidade de itens por página

			this.legalprocess.post("setfilter",Exportbdy).subscribe((data)=>{

				this.nResultPesquisa = data.length; //total de processos retornados pelo WS
				if (this.JuriGenerics.changeUndefinedToEmpty(data.processes) != '') {

					let processExport: Array<itemGridProcesso> = [];

					data.processes.forEach(process => {
						let processos:     itemGridProcesso = new itemGridProcesso();

						processos.processFilial = process.processFilial;
						processos.processId     = process.processId;

						processExport.push(processos);
					});
					listProcessExport = processExport;
					this.downloadExport(listProcessExport, cCodModelo);
				}
			});
		}
	}

	/**
	 * Função responsável por realizar Download do relatório através do navegador
	 * @param listProcessExport - Array que contém lista de processos que serão exportados
	 */
	downloadExport(listProcessExport: Array<itemGridProcesso>, cCodModelo: string ){
		//-- Download
		if (listProcessExport.length > 0 ) {
			this.legalprocess.restore();
			let body = this.bodyExportacao(listProcessExport, cCodModelo);
			this.legalprocess
				.post('exportPesquisa', body)
				.subscribe((fileData) => {
					if (fileData.operation === "Notification") {
						this.poDialog
							.alert({
								title: this.litDtPqAvan.atention,
								message: fileData.message
							})
					} else {
						if (this.JuriGenerics.changeUndefinedToEmpty(fileData.export) != '' ) {
							fileData.export.forEach(item => {
								if (item.filedata != "") {
									var blob = JurBase64.toUTF8(item.filedata, 'application/octet-stream');
									item.fileUrl = window.URL.createObjectURL(blob);
								} else {
									item.fileUrl = item.fileurl;
								}
							
								var fileLink = document.createElement('a');
								fileLink.href = item.fileUrl;
								fileLink.download = item.namefile; //força o nome do download
								fileLink.target = "_blank"; //força a abertura em uma nova página
								document.body.appendChild(fileLink);
								fileLink.click(); //gatilha o evento do click
								this.notification.success(this.litDtPqAvan.msgExportaSucess)
							});
						} else {
							this.notification.error(this.litDtPqAvan.msgExportaError);
						}
					}
					this.isHideLoadingExporta = true;
				});
		}
	}

	/**
	 * Função responsável por montar body da chamada do serviço 'exportPesquisa'
	 * @param listProcessExport - Array que contém lista de processos que serão exportados
	 */
	bodyExportacao(listProcessExport: Array<itemGridProcesso>, cCodModelo: string ){
		let listaProcessos : ListProcess = { count: 0, listProcess:[], modeloExp: "", background: true };

		listProcessExport.forEach( item => {
			listaProcessos.listProcess.push({"ProcessFilial":item.processFilial, "ProcessId":item.processId});
		});

		listaProcessos.count     = this.nResultPesquisa;
		listaProcessos.modeloExp = this.JuriGenerics.changeUndefinedToEmpty(cCodModelo);
		
		listaProcessos.background = this.isNoFilter ? this.isNoFilter : this.isSegundoPlano;

		return 	JSON.stringify(listaProcessos);
	}

	/**
	 * Valida se a filial será apresentada.
	 */
	mostraColunaFilial(){
		this.legalprocess.restore()
		this.legalprocess.get("listFiliaisUser","Lista de Filial").subscribe(data => {
			if (data.length > 0){
				if (data.User[0].value.trim() == ""){
					this.bFilial = false;
				} else {
					this.bFilial = true;
				}
			} else {
				this.bFilial = false;
			}
		
			if (this.bFilial == false) {
				this.colPesquisa.shift();
			}
		});
	}

	/**
	 * Busca os modelos de exportação selecionado
	 */
	getModelosExport(){
		this.isHideLoadingExporta = false;
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA276");
		this.fwmodel.get("Lista de Modelos de exportação personalizada").subscribe(data => {
			if (data.total > 0) {
				this.hasModeloExportacao  = true;
				this.isHideLoadingExporta = true;
			} else {
				this.isHideLoadingExporta = true;
			}
		})
	}
	/**
	 * Função responsável pela criação do adapter de modelos de exportação,
	 * sendo eles publico ou conforme o código usuário logado
	 */
	adapterModelExp() {
		if (this.userInfo.getCodUsuario() == '') {
			setTimeout(() => {
				this.adapterModelExp();
			}, 500);
		} else {
			//-- Adapter para Combo de Lista de Modelos de Exportação
			this.adaptorParamModelosExp = new adaptorFilterParam("JURA276","(NQ5_TIPO='2' OR (NQ5_TIPO = '1' AND NQ5_USER = '" + this.userInfo.getCodUsuario()  + "')) ","NQ5_DESC","NQ5_DESC","","NQ5_COD","","Modelos de Exportação")
			
		}
	}

	/**
	 * Função responsavel para pegar a configuração do usuário 
	 */
	getUserConfig(){
		let codUser: string = this.userInfo.getCodUsuario()
		let nIndex:  number = 0 

		if (codUser == '') {
			setTimeout(() => {
				this.getUserConfig();
			}, 500);
		} else {
			nIndex = this.listUserConfig.findIndex(x=> x.userId == codUser);
			this.isSegundoPlano = nIndex > -1 ? this.listUserConfig[nIndex].pesqAvanSegPlano : true;
		}
	}

	/**
	* Ação do switch Exportar todos os processos
	*/
	switchActionsTodosProcessos() {
		this.lDisabledExp = !this.isNoFilter;
		if (this.isNoFilter) {
			this.campoProcesso = "";
		}
	}
}

/**
 * Estrutura padrão do Combo
 */
class comboStruct{
	value: string;
	label: string;
}

/**
 * Estrutura do Body do serviço 'exportPesquisa'
 */
interface ListProcess {
	count?: number;
	listProcess?: Array<any>;
	modeloExp?: string;
	background: boolean
}

/**
 * Estrutura para o Disclaimer
 */
class disclaimerItemStruct implements PoDisclaimer{
	$id:string;
	label: string;
	value: string;
	field: string;
	type: string;
	condition: string;
}

/**
 * Estrutura do Item do Grid de Processo
 */
class itemGridProcesso {
	nomeProc: string;
	dataProv: string;
	valorProv: number;
	prognostico: string;
	processId: string;
	processFilial: string;
}