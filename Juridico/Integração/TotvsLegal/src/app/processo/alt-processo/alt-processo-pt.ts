//-- Constantes para o bread crumb
export const breadCrumb = {
	home:     "Meus processos",
	processo: "Meu processo"
}

export const detalhes = {
	stepperDetalhe:   "Detalhes",
	vlrCausa:         "Valor da causa",
	simboloMoeda:     "R$",
	dataValor:        "Data da causa",
	formaCorrecao:    "Forma de correção",
	detObjeto:        "Detalhamento do objeto",
	detObjetoHolder:  "Detalhe o objeto",
	observacoes:      "Observações",
	observacaoHolder: "Adicione uma observação ao processo",
	limiteHelp:       "Limite de 250 caracteres",
	rito:             "Rito",
	centroCusto:      "Centro de custo",
	partic1:          "Gerente",
	partic2:          "Advogado responsável",
	partic3:          "Estagiário",
	assunto:          "Assunto",
	unidade:          "Unidade",
	erroValidDetalhe: "Não foi possível alterar os detalhes do processo. Verifique os campos obrigatórios.",
	btnCancelar:      "Cancelar",
	btnAltProc:       "Alterar processo",
	successAlt:       "Processo alterado com sucesso.",
	area:             "Área"
}

export const envolvidos = {
	stepperEnvolvido: 'Envolvidos',
	titExcEnvolvido:  'Exclusão de Envolvido',
	verExclEnv:       'O envolvido "',
	verExclEnv2:      '" será excluído. Confirma a operação?',
	polo:             'Polo',
	tipoEnvolvimento: 'Tipo de envolvimento',
	principal:        'Principal',
	entidade:         'Entidade',
	nome:             'Nome',
	cpf:              'CPF/CNPJ',
	cargo:            'Cargo',
	localTrabalho:    'Local de trabalho',
	dataAdmissao:     'Data de admissão',
	dataDemissao:     'Data de demissão',
	limpaForm:        'Limpar formulário',
	incluir:          'Incluir envolvido',
	alterar:          'Alterar envolvido',
	sucExcEnvolvido:  'Envolvido excluído com sucesso',
	sucAltEnvolvido:  'Envolvido alterado com  Sucesso',
	errFormIncEnvol:  'Não foi possível incluir envolvido. Verifique os campos obrigatórios!',
	errFormAltEnvol:  'Não foi possível alterar envolvido. Verifique os campos obrigatórios!',
	novoEnvolvido:    'Novo envolvido',
	fisica:           'Fisica',
	juridica:         'Jurídica'
}

export const instancia = {
	stepperInstancia: "Instâncias",
	verExclInsta:     'A instância "',
	verExclInsta2:    '" será excluída. Confirma a operação?',
	titExcInstancia:  "Exclusão da Instância",
	limpaForm:        "Limpar formulário",
	tooltipTipoAcao:  "O campo é habilitado, quando a natureza for preenchida!",
	salvar:           "Salvar instância",
	dataDistribuicao: "Data de distribuição",
	novaInstancia:    "Nova instância",
	natureza:         "Natureza",
	tipoAcao:         "Tipo de ação",
	digiteProcesso:   "Digite o numero do processo",
	numeroProcesso:   "Número do processo",
	instancia:        "Instância",
	instanciaOrigem:  "Instância origem",
	incluir:          "Incluir instância",
	alterar:          "Alterar instância",
	comarca:          "Comarca",
	foro:             "Foro",
	vara:             "Vara",
	processoOrigem:   "Processo origem?",
	escritorio:       "Escritório credenciado",
	dataEncInst:      "Data de encerramento",
	statusInstancia:  "Status Instância",
	errFormIncInsta:  "Não foi possível incluir instância. Verifique os campos obrigatórios!",
	errFormAltInsta:  "Não foi possível alterar instância. Verifique os campos obrigatórios!",
	sucAltInstancia:  "Alteração de instância feita com sucesso!",
	sucIncInstancia:  "Inclusão de instância feita com sucesso!",
	sucExcInstancia:  "Exclusão de instância feita com sucesso!",
	erroExcInst:      "Não foi possível realizar exclusão. É necessário ter ao menos uma instância origem !",
	erroExcInstPai:   "Não é possível excluir o registro. Esta instância possui vínculo com o processo {0} ."
}

export const camposAdic = {
	titExtraFields: 'Campos adicionais',
	btnSalvarAlt:   'Salvar alteração'
}

const listInstancia = {
	primeira:         "1ª Instância",
	segunda:          "2ª Instância",
	tribunalSuperior: "Tribunal superior"
}

const listPolos = {
	poloAtivo:              "Polo ativo",
	poloPassivo:            "Polo passivo",
	terceiroInteressado:    "Terceiro interessado",
	sociedade:              "Sociedade envolvida",
	participacaoSocietaria: "Participação societária",
	administracao:          "Administração"
}

const listEntidadeOrigem = {
	cliente:        "Cliente",
	contato:        "Contato",
	fornecedor:     "Fornecedor",
	funcionario:    "Funcionário",
	parteContraria: "Parte contrária"
}

const listSimNao = {
	sim: "Sim",
	nao: "Não"
}

const listTpPessoa = {
	fisica:   "Fisica",
	juridica: "Juridica"
}

const colEnvolvidos = {
	nome:       "Nome",
	polo:       "Polo",
	tipoEnvolv: "Tipo de envolvimento",
	principal:  "Principal?"
}

const colInstancia = {
	instancia:      "Instância",
	atual:          "Origem?",
	seq:            "Seq.",
	processo:       "Processo",
	localizacao:    "Localização",
	correspondente: "Correspondente",
	status1:        "Instância em andamento",
	status2:        "Instância encerrada",
	tipoAcao:       "Tipo de ação"
}

export const parteContraria = {
	verInclPtCont:     'Deseja incluir',
	verInclPtCont2:    'como parte contrária?',
	partContraria:     'Parte Contrária',
	verExclPtCont:     'A parte contrária "',
	verExclPtCont2:    '" será excluída. Confirma a operação?',
	sucesIncPartC:     'Parte Incluída com sucesso',
	msgCamposObgrigat: 'Há campos obrigatórios não preenchidos. Verifique!',
	nome:              'Nome',
	tipoPessoa:        'Tipo Pessoa',
	cgc:               'CNPJ/CPF',
	email:             'E-mail'
}

export const ListEntOrigem = {
	cliente:        "Cliente",
	contato:        "Contato",
	fornecedor:     "Fornecedor",
	funcionario:    "Funcionário",
	parteContrária: "Parte contrária",
}

export const modais = {
	confirmar:        "Confirmar",
	fechar:           "Fechar",
	cancelar:         "Cancelar",
	numProcInvalido:  "Número do processo inválido",
	invalidCNJ:       "O número do processo digitado está fora do padrão CNJ. Deseja continuar?"
}

export const altProcessoPt = {
	titleDet:           "Detalhes do processo",
	loadGernerico:      "Carregando...",
	editar:             "Editar",
	excluir:            "Excluir",
	verAndamentos:      "Ver andamentos",
	sim:                "Sim",
	nao:                "Não",
	breadCrumb:         breadCrumb,
	detalhes:           detalhes,
	envolvidos:         envolvidos,
	instancia:          instancia,
	listInstancia:      listInstancia,
	listPolos:          listPolos,
	listEntidadeOrigem: listEntidadeOrigem,
	listSimNao:         listSimNao,
	listTpPessoa:       listTpPessoa,
	colEnvolvidos:      colEnvolvidos,
	ListEntOrigem:      ListEntOrigem,
	camposAdic:         camposAdic,
	parteContraria,
	colInstancia,
	modais
}

