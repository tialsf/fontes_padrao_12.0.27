import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltProcessoComponent } from './alt-processo.component';

describe('AltProcessoComponent', () => {
  let component: AltProcessoComponent;
  let fixture: ComponentFixture<AltProcessoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltProcessoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltProcessoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
