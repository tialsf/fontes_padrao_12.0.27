import { Component, OnInit, ViewChild } from '@angular/core';
import { RoutingService } from 'src/app/services/routing.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FwmodelService } from 'src/app/services/fwmodel.service';
import { JurigenericsService, JurUTF8 } from 'src/app/services/jurigenerics.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
	FwmodelAdaptorService,
	adaptorFilterParam,
	adaptorReturnStruct,
} from 'src/app/services/fwmodel-adaptor.service';
import {
	LegalprocessService,
	SysProtheusParam,
	sysParamStruct,
} from 'src/app/services/legalprocess.service';
import { protheusParam } from 'src/app/services/authlogin.service';
import {
	JurJustifModalComponent,
	DadosInstancia,
} from 'src/app/shared/jur-justif-modal/jur-justif-modal.component';
import { ItemEnvolvido, CamposParteContraria } from '../det-processo/det-processo';
import { BuscaInstanciasOrigens } from './alt-processo.service';
import { Title } from '@angular/platform-browser';
import {
	lpGetFuncionarioAdapterService,
	lpGetForoAdapterService,
	lpFuncionarioReturnStruct,
	lpGetVaraAdapterService,
} from 'src/app/services/legalprocess-adapter.service';
import { Detalhes, GridEnvolvido, GridInstancia, Select, BodyValidNumPro } from './alt-processo';
import {
	PoI18nService,
	PoBreadcrumb,
	PoTableColumn,
	PoComboComponent,
	PoNotificationService,
	PoModalComponent,
	PoDynamicFormField,
	PoModalAction,
	PoSelectOption,
	PoStepperComponent,
	PoI18nPipe,
} from '@po-ui/ng-components';
import { FwModelBody, FwModelBodyModels, FwModelBodyItems } from 'src/app/services/fwmodel.service';
import { JurconsultasService, ExtraField } from 'src/app/services/jurconsultas.service';
import { NomeEnvolvidoAdapterService } from 'src/app/services/combos-fwmodel-adapter.service';
import { UnidadeAdapterService } from 'src/app/services/combos-fwmodel-adapter.service';
import { AreaJuridicaAdapterService } from 'src/app/services/combos-fwmodel-adapter.service';
import { ParticipanteAdapterService } from 'src/app/services/combos-fwmodel-adapter.service';
import { AssuntoAdapterService } from 'src/app/services/combos-fwmodel-adapter.service';
import { RitoAdapterService } from 'src/app/services/combos-fwmodel-adapter.service';
import { CustoAdapterService } from 'src/app/services/combos-fwmodel-adapter.service';
import { FCorrecaoAdapterService } from 'src/app/services/combos-fwmodel-adapter.service';
import { TpEnvolvAdapterService } from 'src/app/services/combos-fwmodel-adapter.service';
import { TpAcaoAdapterService } from 'src/app/services/combos-fwmodel-adapter.service';
import { ComarcasAdapterService } from 'src/app/services/combos-fwmodel-adapter.service';
import { EscritorioAdapterService } from 'src/app/services/combos-fwmodel-adapter.service';
import { LocalTrabAdapterService } from 'src/app/services/combos-fwmodel-adapter.service';
import { CargoAdapterService } from 'src/app/services/combos-fwmodel-adapter.service';

/**
 * Classe de estrutura para uso nos combos
 */
export class SelectOptions {
	value: string = '';
	label: string = '';
}

@Component({
	selector: 'app-alt-processo',
	templateUrl: './alt-processo.component.html',
	styleUrls: ['./alt-processo.component.css'],
})
export class AltProcessoComponent implements OnInit {

	// Detalhe
	litAltProcesso: any;
	formAltProcesso: FormGroup;
	tituloPagina: string = '';
	filial: string = '';
	chaveProcesso: string = '';
	cajuri: string = '';
	pk: string = '';
	cajuriTitulo: string = '';
	classSwitch: string = '';
	classInstancia: string = '';
	classDtDistr: string = '';
	codInstSeqPri: string = '';
	unidade: string = '';
	area: string = '';
	partic1: string = '';
	partic2: string = '';
	partic3: string = '';
	assunto: string = '';
	rito: string = '';
	centroCusto: string = '';
	fCorrecao: string = '';
	envolvimento: string = '';
	nome: string = '';
	cargo: string = '';
	localtrab: string = '';
	instanciaOri: string = '';
	tipoAcao: string = '';
	comarca: string = '';
	foro: string = '';
	vara: string = '';
	correspondente: string = '';
	filtroTipoEnv: string = '';
	filtroPartic: string = "RD0_TPJUR='1'";
	filtroFCorrecao: string = "NW7_VISIV='1'";
	filtroEscritorio: string = "A2_MJURIDI='1'";
	isLoadingDetalhes: boolean = false;
	isLoadingAlteraProcesso: boolean = false;
	isProcEncerrado: boolean = false;
	isDisableInsOri: boolean = true;
	params: protheusParam[] = [];
	paramsProtheus: sysParamStruct[] = [];

	@ViewChild('comboEscritorio', { static: false }) cmbEscritorio: PoComboComponent;
	@ViewChild('comboUnidade', { static: true }) cmbUnidade: PoComboComponent;
	@ViewChild('modJustif', { static: true }) modalJustif: JurJustifModalComponent;
	@ViewChild('modCorresp', { static: false }) modalCorresp: JurJustifModalComponent;

	// Envolvidos 
	filterTpEnvo: string = '';
	FilterCodLoja: string = '';
	nomePartCont: string = '';
	btnSalvarEnvolvido: string = '';
	nomeEnvolvido: string = '';
	nNextIdEnvolvido: number = -1;
	isDisableLocTrabalho: boolean = true;
	isDisabletpEnvol: boolean = true;
	isDisableCpfCnpj: boolean = true;
	isLoadingCadastro: boolean = true;
	isEditaEnvolvido: boolean = false;
	itemsEnvolv: Array<GridEnvolvido> = [];
	listEnvolvidos: GridEnvolvido;
	formEnvolvido: FormGroup;
	formParteCont: FormGroup;

	listInstancia: Array<SelectOptions> = [
		{ label: '', value: '1' },
		{ label: '', value: '2' },
		{ label: '', value: '3' },
	];

	listPolos: Array<SelectOptions> = [
		{ label: '', value: '1' },
		{ label: '', value: '2' },
		{ label: '', value: '3' },
		{ label: '', value: '4' },
		{ label: '', value: '5' },
		{ label: '', value: '6' },
	];

	ListEntOrigem: Array<SelectOptions> = [
		{ label: '', value: 'SA1' },
		{ label: '', value: 'SU5' },
		{ label: '', value: 'SA2' },
		{ label: '', value: 'SRA' },
		{ label: '', value: 'NZ2' },
	];

	listPrincipal: Array<SelectOptions> = [
		{ label: '', value: '1' },
		{ label: '', value: '2' },
	];

	listTpPessoa: Array<SelectOptions> = [
		{ label: '', value: '1' },
		{ label: '', value: '2' },
	];

	// Grid Envolvidos
	colEnvolvidos: Array<PoTableColumn> = [
		{ label: '', property: 'nome', width: '30%' },
		{ label: '', property: 'descTipoEnv', width: '20%' },
		{ label: '', property: 'poloDescri', width: '20%' },
		{ label: '', property: 'poloPrincipal', width: '35%' },
		{
			label: ' ',
			property: 'btnEditEnv',
			width: '5%',
			type: 'icon',
			icons: [
				{
					value: 'editarEnv',
					icon: 'po-icon po-icon-edit',
					color: '#29b6c5',
					tooltip: '',
					action: row => {
						this.isEditaEnvolvido = true;
						setTimeout(() => {
							this.formEnvolvido.reset();
							this.alteraEnvolvido(row);
						}, 500);
					},
				},
				{
					value: 'excluirEnv',
					icon: 'po-icon po-icon-close',
					color: '#29b6c5',
					tooltip: '',
					action: row => {
						this.listEnvolvidos = row;
						this.nomeEnvolvido = row.nome;
						this.modalExcEnvo.open();
					},
				},
			],
		},
	];

	// Campos dinâmicos
	@ViewChild('envolvidoPolo', { static: false }) cmbEnvolvidoPolo: PoComboComponent;
	@ViewChild('envolvidoTipo', { static: false }) cmbEnvolvidoTipo: PoComboComponent;
	@ViewChild('envolvidoNome', { static: false }) cmbEnvolvidoNome: PoComboComponent;
	@ViewChild('tipoEnvolvido', { static: false }) cmbTipoEnvolvido: PoComboComponent;

	// Modais
	@ViewChild('confirmInclPtC', { static: true }) modalConfirmIncl: PoModalComponent;
	fechar: PoModalAction = {
		action: () => {
			this.modalConfirmIncl.close();
		},
		label: '',
	};
	incluir: PoModalAction = {
		action: () => {
			this.modalConfirmIncl.close();
			this.modalIncPtCont.open();
		},
		label: '',
	};

	@ViewChild('modalInclPtC', { static: true }) modalIncPtCont: PoModalComponent;
	cancelar: PoModalAction = {
		action: () => {
			this.modalIncPtCont.close();
		},
		label: '',
	};
	confirmar: PoModalAction = {
		action: () => {
			this.setComboNomeEntidade(this.formEnvolvido.value.entidade, '');
			this.postPartCont();
		},
		label: '',
	};

	@ViewChild('confirmExclEnv', { static: true }) modalExcEnvo: PoModalComponent;
	closeEnv: PoModalAction = {
		action: () => {
			this.modalExcEnvo.close();
		},
		label: '',
	};
	excluirEnv: PoModalAction = {
		action: () => {
			this.excluiEnvolvido(this.listEnvolvidos);
		},
		label: '',
	};

	// Instância
	formInstancia: FormGroup;
	listItemInstaExc: GridInstancia;
	btnSalvarInstancia: string = '';
	nomeInstancia: string = '';
	correAtual: string = '';
	isUpdateCorresp: boolean = false;
	isEditaInstancia: boolean = false;
	isFromDePara: boolean = false;
	isDisabletpAcao: boolean = true;
	isDisableVara: boolean = true;
	isDisableForo: boolean = true;
	listNaturezas: Array<Select> = [];
	listForo: Array<SelectOptions> = [];
	listVara: Array<SelectOptions> = [];
	listTpAcao: Array<Select> = [];
	itemsInstancia: Array<GridInstancia> = [];
	listAuxItemsInst: Array<GridInstancia> = [];

	// Grid Instancias
	colInstancia: Array<PoTableColumn> = [
		{
			property: 'staInstancia',
			label: ' ',
			type: 'label',
			width: '3%',
			labels: [
				{ value: '1', color: 'color-11', label: '' },
				{ value: '2', color: 'color-07', label: '' },
			],
		},
		{ property: 'sequencia', label: '', width: '1%' },
		{ property: 'descInstancia', label: '', width: '10%' },
		{ property: 'origem', label: '', width: '5%' },
		{ property: 'processo', label: '', width: '15%' },
		{ property: 'localizacao', label: '', width: '25%' },
		{ property: 'desctipoAcao', label: '', width: '12%' },
		{ property: 'descCorresp', label: '', width: '23%' },

		{
			label: ' ',
			property: 'btnEditIns',
			width: '2%',
			type: 'icon',
			icons: [
				{
					value: 'callAndamentos',
					icon: 'po-icon po-icon-history',
					color: '#29b6c5',
					tooltip: '',
					action: (row: GridInstancia) => {
						this.callAndamentos(row.cod);
					},
				},
				{
					value: 'editarInstancia',
					icon: 'po-icon po-icon-edit',
					color: '#29b6c5',
					tooltip: '',
					action: row => {
						this.isEditaInstancia = true;
						this.alteraInstancia(row);
					},
				},
				{
					value: 'excluirInstancia',
					icon: 'po-icon po-icon-close',
					color: '#29b6c5',
					tooltip: '',
					action: row => {
						this.nomeInstancia = row.descInstancia;
						this.listItemInstaExc = row;
						this.modalExcInsta.open();
					},
				},
			],
		},
	];

	@ViewChild('tipoAcao', { static: false }) cmbTipoAcao: PoComboComponent;
	@ViewChild('comboForo', { static: false }) cmbForo: PoComboComponent;
	@ViewChild('comboVara', { static: false }) cmbVara: PoComboComponent;
	@ViewChild('instaOrigem', { static: true }) cmbInstOrigem: PoComboComponent;

	// Modal
	@ViewChild('modalValidaCNJ', { static: true }) modalValidCNJ: PoModalComponent;
	continueValidCnj: PoModalAction = {
		action: () => {
			this.modalValidCNJ.close();
		},
		label: '',
	};

	cancelValidCnj: PoModalAction = {
		action: () => {
			this.formInstancia.patchValue({
				processo: '',
			});

			this.modalValidCNJ.close();
		},
		label: '',
	};

	@ViewChild('confirmExclInsta', { static: true }) modalExcInsta: PoModalComponent;

	closeInsta: PoModalAction = {
		action: () => {
			this.modalExcInsta.close();
		},
		label: '',
	};

	excluirInsta: PoModalAction = {
		action: () => {
			this.excluiInstancia(this.listItemInstaExc);
		},
		label: '',
	};

	// Campos Adicionais
	hasExtraFields: boolean = true;
	isHideLoadCmpAdic: boolean = true;
	listExtraFields: Array<PoDynamicFormField> = [];
	listAuxExtFi: Array<PoDynamicFormField> = [];
	oValores: object = {};

	listCamposDet: Array<string> = [
		'NSZ_CCLIEN',
		'NSZ_LCLIEN',
		'NSZ_TIPOAS',
		'NSZ_CAREAJ',
		'NSZ_SIGLA1',
		'NSZ_SIGLA2',
		'NSZ_SIGLA3',
		'NSZ_CRITO',
		'NSZ_CCUSTO',
		'NSZ_COBJET',
		'NSZ_DETALH',
		'NSZ_OBSERV',
		'NSZ_VLCAUS',
		'NSZ_DTCAUS',
		'NSZ_CFCORR',
		'NSZ_CMOCAU',
	];

	@ViewChild('stepper', { static: true }) statusStepper: PoStepperComponent;

	/**
	 * estrutura de navegação da URL atual
	 */
	public breadcrumb: PoBreadcrumb = {
		items: [],
	};

	constructor(
		private titleService: Title,
		protected poI18nService: PoI18nService,
		private routingState: RoutingService,
		private route: ActivatedRoute,
		private fwmodel: FwmodelService,
		private JuriGenerics: JurigenericsService,
		private formBuilder: FormBuilder,
		public fwModelAdaptorService: FwmodelAdaptorService,
		private router: Router,
		private legalprocess: LegalprocessService,
		private poNotification: PoNotificationService,
		private sysProtheusParam: SysProtheusParam,
		public funcionarioAdapterService: lpGetFuncionarioAdapterService,
		public foroAdapterService: lpGetForoAdapterService,
		public varaAdapterService: lpGetVaraAdapterService,
		public buscaInstanciasOrigens: BuscaInstanciasOrigens,
		private jurConsultas: JurconsultasService,
		private thfI18nPipe: PoI18nPipe,
		public nomeEnvAdapterService: NomeEnvolvidoAdapterService,
		public unidadeAdapterService: UnidadeAdapterService,
		public areaJurAdapterService: AreaJuridicaAdapterService,
		public participAdapterService: ParticipanteAdapterService,
		public assuntoAdapterService: AssuntoAdapterService,
		public ritoAdapterService: RitoAdapterService,
		public custoAdapterService: CustoAdapterService,
		public fCorrecaoAdapterService: FCorrecaoAdapterService,
		public tpEnvolvAdapterService: TpEnvolvAdapterService,
		public tpAcaoAdapterService: TpAcaoAdapterService,
		public comarcasAdapterService: ComarcasAdapterService,
		public escritorioAdapterService: EscritorioAdapterService,
		public localTrabAdapterService: LocalTrabAdapterService,
		public cargosAdapterService: CargoAdapterService
	) {
		this.routingState.loadRouting();
		this.filial = this.route.snapshot.paramMap.get('filPro');
		window.sessionStorage.setItem('filial', atob(this.filial));
		this.chaveProcesso = this.route.snapshot.paramMap.get('codPro');
		this.cajuri = atob(decodeURIComponent(this.chaveProcesso));

		// Adiciona o código do cajuri no título Meu processo + área 
		this.cajuriTitulo = this.cajuri.replace(new RegExp('^0*', 'g'), '');

		// setups dos objetos de adapter 
		this.unidadeAdapterService.setup(
			new adaptorFilterParam(
				'JURA148',
				'',
				'A1_NOME,A1_CGC',
				'A1_NOME,A1_COD,A1_LOJA,A1_CGC',
				'A1_NOME [ A1_CGC ]',
				'A1_COD,A1_LOJA',
				'',
				'Unidades',
				undefined,
				undefined,
				undefined,
				{
					bGetAllLevels: false,
					bUseEmptyField: false,
					bUseVirtualField: false,
					arrSetFields: ['A1_COD', 'A1_LOJA', 'A1_CGC', 'A1_NOME'],
				}
			)
		);
		this.areaJurAdapterService.setup(
			new adaptorFilterParam(
				'JURA038',
				'',
				'NRB_DESC',
				'NRB_DESC',
				'',
				'NRB_COD',
				'',
				'GetArea'
			)
		);
		this.participAdapterService.setup(
			new adaptorFilterParam(
				'JURA159',
				"RD0_TPJUR='1'",
				'RD0_NOME',
				'RD0_NOME',
				'',
				'RD0_SIGLA',
				'',
				'Advogado Responsável',
				undefined,
				undefined,
				undefined,
				{
					bGetAllLevels: true,
					bUseEmptyField: true,
					bUseVirtualField: true,
					arrSetFields: [],
				}
			)
		);
		this.assuntoAdapterService.setup(
			new adaptorFilterParam(
				'JURA004',
				'',
				'NQ4_DESC',
				'NQ4_DESC',
				'',
				'NQ4_COD',
				'',
				'GetAssunto'
			)
		);
		this.ritoAdapterService.setup(
			new adaptorFilterParam('JURA018', '', 'NQO_DESC', 'NQO_DESC', '', 'NQO_COD', '', 'Rito')
		);
		this.custoAdapterService.setup(
			new adaptorFilterParam(
				'JCCUSTO',
				'',
				'CTT_DESC01',
				'CTT_DESC01',
				'',
				'CTT_CUSTO',
				'',
				'Centro de Custo'
			)
		);
		this.fCorrecaoAdapterService.setup(
			new adaptorFilterParam(
				'JURA061',
				"NW7_VISIV='1'",
				'NW7_DESC',
				'NW7_DESC',
				'',
				'NW7_COD',
				'',
				'Forma de Correção'
			)
		);
		this.tpEnvolvAdapterService.setup(
			new adaptorFilterParam(
				'JURA009',
				'',
				'NQA_DESC',
				'NQA_DESC',
				'',
				'NQA_COD',
				'',
				'GetTipoEnvolvimento',
				undefined,
				undefined,
				undefined,
				{
					bGetAllLevels: false,
					bUseEmptyField: false,
					bUseVirtualField: false,
					arrSetFields: [],
				}
			)
		);
		this.tpAcaoAdapterService.setup(
			new adaptorFilterParam(
				'JURA022',
				'',
				'NQU_DESC',
				'NQU_DESC',
				'',
				'NQU_COD',
				'',
				'Tipo de Ação'
			)
		);
		this.comarcasAdapterService.setup(
			new adaptorFilterParam(
				'JURA005',
				'',
				'NQ6_DESC',
				'NQ6_DESC',
				'',
				'NQ6_COD',
				'',
				'GetComarca',
				undefined,
				undefined,
				undefined,
				{
					bGetAllLevels: true,
					bUseEmptyField: false,
					bUseVirtualField: false,
					arrSetFields: [],
				}
			)
		);
		this.escritorioAdapterService.setup(
			new adaptorFilterParam(
				'JURA132',
				"A2_MJURIDI='1'",
				'A2_NOME,A2_CGC',
				'A2_NOME',
				'',
				'A2_COD,A2_LOJA',
				'',
				'getEscritorio',
				undefined,
				undefined,
				undefined,
				{
					bGetAllLevels: true,
					bUseEmptyField: false,
					bUseVirtualField: false,
					arrSetFields: [],
				}
			)
		);
		this.cargosAdapterService.setup(
			new adaptorFilterParam(
				'JCRGS',
				'',
				'Q3_DESCSUM',
				'Q3_DESCSUM',
				'',
				'Q3_CARGO',
				'',
				'GetCargos',
				undefined,
				undefined,
				undefined,
				{
					bGetAllLevels: false,
					bUseEmptyField: true,
					bUseVirtualField: true,
					arrSetFields: [],
				}
			)
		);
		this.localTrabAdapterService.setup(
			new adaptorFilterParam(
				'JURA107',
				'',
				'NTB_DESC',
				'NTB_DESC',
				'',
				'NTB_COD',
				'',
				'GetLocalTrabalho'
			)
		);

		
		// Chamada do serviço poI18n para buscar as constantes a partir do contexto 'altProcesso'
		// no arquivo JurTraducao.
		poI18nService
			.getLiterals({
				context: 'altProcesso',
				language: poI18nService.getLanguage(),
			})
			.subscribe(litAltProcesso => {
				this.litAltProcesso = litAltProcesso;
				this.tituloPagina = this.litAltProcesso.titleDet;

				// Instancias
				this.listInstancia[0].label = this.litAltProcesso.listInstancia.primeira;
				this.listInstancia[1].label = this.litAltProcesso.listInstancia.segunda;
				this.listInstancia[2].label = this.litAltProcesso.listInstancia.tribunalSuperior;

				this.listPolos[0].label = this.litAltProcesso.listPolos.poloAtivo;
				this.listPolos[1].label = this.litAltProcesso.listPolos.poloPassivo;
				this.listPolos[2].label = this.litAltProcesso.listPolos.terceiroInteressado;
				this.listPolos[3].label = this.litAltProcesso.listPolos.sociedade;
				this.listPolos[4].label = this.litAltProcesso.listPolos.participacaoSocietaria;
				this.listPolos[5].label = this.litAltProcesso.listPolos.administracao;

				this.ListEntOrigem[0].label = this.litAltProcesso.listEntidadeOrigem.cliente;
				this.ListEntOrigem[1].label = this.litAltProcesso.listEntidadeOrigem.contato;
				this.ListEntOrigem[2].label = this.litAltProcesso.listEntidadeOrigem.fornecedor;
				this.ListEntOrigem[3].label = this.litAltProcesso.listEntidadeOrigem.funcionario;
				this.ListEntOrigem[4].label = this.litAltProcesso.listEntidadeOrigem.parteContraria;

				this.listPrincipal[0].label = this.litAltProcesso.listSimNao.sim;
				this.listPrincipal[1].label = this.litAltProcesso.listSimNao.nao;

				this.listTpPessoa[0].label = this.litAltProcesso.listTpPessoa.fisica;
				this.listTpPessoa[1].label = this.litAltProcesso.listTpPessoa.juridica;

				this.colEnvolvidos[0].label = this.litAltProcesso.colEnvolvidos.nome;
				this.colEnvolvidos[1].label = this.litAltProcesso.colEnvolvidos.tipoEnvolv;
				this.colEnvolvidos[2].label = this.litAltProcesso.colEnvolvidos.polo;
				this.colEnvolvidos[3].label = this.litAltProcesso.colEnvolvidos.principal;
				this.colEnvolvidos[4].icons[0].tooltip = this.litAltProcesso.editar;
				this.colEnvolvidos[4].icons[1].tooltip = this.litAltProcesso.excluir;

				// Intancias
				this.colInstancia[0].labels[0].tooltip = this.litAltProcesso.colInstancia.status1;
				this.colInstancia[0].labels[1].tooltip = this.litAltProcesso.colInstancia.status2;
				this.colInstancia[1].label = this.litAltProcesso.colInstancia.seq;
				this.colInstancia[2].label = this.litAltProcesso.colInstancia.instancia;
				this.colInstancia[3].label = this.litAltProcesso.colInstancia.atual;
				this.colInstancia[4].label = this.litAltProcesso.colInstancia.processo;
				this.colInstancia[5].label = this.litAltProcesso.colInstancia.localizacao;
				this.colInstancia[6].label = this.litAltProcesso.colInstancia.tipoAcao;
				this.colInstancia[7].label = this.litAltProcesso.colInstancia.correspondente;
				this.colInstancia[8].icons[0].tooltip = this.litAltProcesso.verAndamentos;
				this.colInstancia[8].icons[1].tooltip = this.litAltProcesso.editar;
				this.colInstancia[8].icons[2].tooltip = this.litAltProcesso.excluir;

				// Modais
				this.fechar.label = this.litAltProcesso.modais.fechar;
				this.incluir.label = this.litAltProcesso.listSimNao.sim;
				this.cancelar.label = this.litAltProcesso.modais.cancelar;
				this.confirmar.label = this.litAltProcesso.modais.confirmar;
				this.closeEnv.label = this.litAltProcesso.modais.cancelar;
				this.excluirEnv.label = this.litAltProcesso.modais.confirmar;
				this.closeInsta.label = this.litAltProcesso.modais.cancelar;
				this.excluirInsta.label = this.litAltProcesso.modais.confirmar;
				this.continueValidCnj.label = this.litAltProcesso.listSimNao.sim;
				this.cancelValidCnj.label = this.litAltProcesso.modais.cancelar;

				this.breadcrumb.items = [
					{ label: this.litAltProcesso.breadCrumb.home, link: '/home' },
					{
						label: this.litAltProcesso.breadCrumb.processo,
						link: '/processo/' + this.filial + '/' + this.chaveProcesso,
					},
					{ label: this.litAltProcesso.titleDet },
				];
			});
	}

	ngOnInit() {
		// Detalhes
		this.createFormDetalhe(new Detalhes());

		// Busca os parâmetros do Protheus */
		this.sysProtheusParam.getParamByArray(['MV_JTVENJF']).subscribe(params => {
			this.paramsProtheus = params;
		});

		// Envolvidos
		this.createFormPtCont(new CamposParteContraria());
		this.createFormEnvolv(new ItemEnvolvido());
		this.getInfoProcess();
		this.isDisableLocTrabalho = true;

		// Instância
		this.getNatureza();
		this.createFormInstancia(new GridInstancia());
	}

	/**
	 * Responsável por inicializar o Formulário de Detalhes do processo
	 * @param altProcesso - Informar a classe do formulário de Detalhe.
	 */
	createFormDetalhe(altProcesso: Detalhes) {
		this.formAltProcesso = this.formBuilder.group({
			vlrCausa: [altProcesso.vlrCausa],
			dataVlr: [altProcesso.dataVlr],
			fCorrecao: [altProcesso.fCorrecao],
			detObjeto: [altProcesso.detObjeto],
			observacao: [altProcesso.observacao],
			rito: [altProcesso.rito],
			partic1: [altProcesso.partic1, Validators.compose([Validators.required])],
			partic2: [altProcesso.partic2],
			partic3: [altProcesso.partic3],
			assunto: [altProcesso.assunto],
			unidade: [altProcesso.unidade, Validators.compose([Validators.required])],
			centroCusto: [altProcesso.centroCusto],
			moedaVlCaus: [altProcesso.moedaVlCaus],
			area: [altProcesso.area, Validators.compose([Validators.required])],
			tpAsj: [altProcesso.tpAsj],
		});
	}

	/**
	 * método GET (JURA095) WS JURMODREST
	 * detalhes do processo
	 */
	getDetailProcess() {
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA095/' + btoa(atob(this.filial) + this.cajuri));
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.setCampoVirtual(true);

		this.fwmodel.get('getDetailProcess').subscribe(data => {
			this.isLoadingDetalhes = false;
			if (this.JuriGenerics.changeUndefinedToEmpty(data) != '') {
				let situcacao: string;
				const dataProcess = data.models[0].fields;

				// Verifica se trouxe os Submodelos e roda a requisição até obtero sub
				if (data.models[0].hasOwnProperty('models')) {
					this.breadcrumb.items[1].label =
						this.litAltProcesso.breadCrumb.processo +
						' ' +
						this.JuriGenerics.findValueByName(data.models[0].fields, 'NSZ_DAREAJ') +
						' (' +
						this.cajuriTitulo +
						')';
					this.formAltProcesso.patchValue({
						unidade:
							this.JuriGenerics.changeUndefinedToEmpty(
								this.JuriGenerics.findValueByName(dataProcess, 'NSZ_CCLIEN')
							) +
							this.JuriGenerics.separador +
							this.JuriGenerics.changeUndefinedToEmpty(
								this.JuriGenerics.findValueByName(dataProcess, 'NSZ_LCLIEN')
							),
						area: this.JuriGenerics.changeUndefinedToEmpty(
							this.JuriGenerics.findValueByName(dataProcess, 'NSZ_CAREAJ')
						),
						partic1: this.JuriGenerics.changeUndefinedToEmpty(
							this.JuriGenerics.findValueByName(dataProcess, 'NSZ_SIGLA1')
						),
						partic2: this.JuriGenerics.changeUndefinedToEmpty(
							this.JuriGenerics.findValueByName(dataProcess, 'NSZ_SIGLA2')
						),
						partic3: this.JuriGenerics.changeUndefinedToEmpty(
							this.JuriGenerics.findValueByName(dataProcess, 'NSZ_SIGLA3')
						),
						rito: this.JuriGenerics.changeUndefinedToEmpty(
							this.JuriGenerics.findValueByName(dataProcess, 'NSZ_CRITO')
						),
						centroCusto: this.JuriGenerics.changeUndefinedToEmpty(
							this.JuriGenerics.findValueByName(dataProcess, 'NSZ_CCUSTO')
						),
						assunto: this.JuriGenerics.changeUndefinedToEmpty(
							this.JuriGenerics.findValueByName(dataProcess, 'NSZ_COBJET')
						),
						detObjeto: this.JuriGenerics.changeUndefinedToEmpty(
							this.JuriGenerics.findValueByName(dataProcess, 'NSZ_DETALH')
						),
						observacao: this.JuriGenerics.changeUndefinedToEmpty(
							this.JuriGenerics.findValueByName(dataProcess, 'NSZ_OBSERV')
						),
						vlrCausa: this.JuriGenerics.changeUndefinedToZero(
							this.JuriGenerics.findValueByName(dataProcess, 'NSZ_VLCAUS')
						),
						dataVlr: this.JuriGenerics.makeDate(
							this.JuriGenerics.changeUndefinedToEmpty(
								this.JuriGenerics.findValueByName(dataProcess, 'NSZ_DTCAUS')
							),
							'yyyy-mm-dd'
						),
						fCorrecao: this.JuriGenerics.changeUndefinedToEmpty(
							this.JuriGenerics.findValueByName(dataProcess, 'NSZ_CFCORR')
						),
						cajuri: this.JuriGenerics.findValueByName(dataProcess, 'NSZ_COD'),
						tpAsj: this.JuriGenerics.findValueByName(dataProcess, 'NSZ_TIPOAS'),
					});
					this.pk = data.pk;
					situcacao = this.JuriGenerics.changeUndefinedToEmpty(
						this.JuriGenerics.findValueByName(dataProcess, 'NSZ_SITUAC')
					);
					this.isProcEncerrado = situcacao == '2';

					this.gravaListaEnvolvidos(
						this.JuriGenerics.findModelItensByName(data.models[0].models, 'NT9DETAIL')
					);
					this.listInstancias(
						this.JuriGenerics.findModelItensByName(data.models[0].models, 'NUQDETAIL')
					);

					this.loadExtraFields(dataProcess);
				} else {
					setTimeout(() => {
						this.getDetailProcess();
					}, 500);
				}
			} else {
				this.router.navigate(['/'], { relativeTo: this.route });
			}
			this.isLoadingDetalhes = true;
		});
	}

	/**
	 * Seta os campos de data e moeda da causa, caso o campo de valor esteja preenchido
	 * @param vlrCausa - valor dda causa preenchida no formulário.
	 */
	setCampoMoeda(vlrCausa: string) {
		if (this.JuriGenerics.changeUndefinedToEmpty(vlrCausa) != '') {
			this.legalprocess.restore();
			this.legalprocess.get('tlprocess/sysParam/MV_JCMOPRO').subscribe(data => {
				const param = new protheusParam();

				param.id = data.sysParam.name;
				param.value = data.sysParam.value;

				this.params.push(param);

				this.formAltProcesso.patchValue({
					moedaVlCaus: this.JuriGenerics.findValueByName(this.params, 'MV_JCMOPRO'),
				});
			});
		} else {
			this.formAltProcesso.patchValue({
				moedaVlCaus: '',
			});
		}
	}

	/**
	 * Verifica se o processo esta encerrado se sim, apresenta a modal de justificativa
	 */
	beforeSubmit() {
		let isSubmitable: boolean = this.formAltProcesso.valid;

		if (isSubmitable) {
			this.isLoadingAlteraProcesso = true;
			// Tratamento para justificativa do encerramento
			if (this.isProcEncerrado) {
				if (
					this.sysProtheusParam.getParamValueByName(this.paramsProtheus, 'MV_JTVENJF') ==
					'1'
				) {
					isSubmitable = false;
					this.modalJustif.openModal(this.cajuri, () => {
						this.submitDetalhes();
					});
				} else {
					isSubmitable = true;
					this.isLoadingAlteraProcesso = false;
				}
			} else {
				this.submitDetalhes();
			}
		} else {
			this.poNotification.error(this.litAltProcesso.detalhes.erroValidDetalhe);
		}

		return isSubmitable;
	}

	/**
	 * Chama o método PUT para realizar a gravação dos detalhes.
	 */
	submitDetalhes() {
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA095');
		this.fwmodel
			.put(this.pk, this.setBodyDetalhes(), 'Alteração do processo')
			.subscribe(data => {
				if (data != '') {
					this.poNotification.success(this.litAltProcesso.detalhes.successAlt);
				}
				this.isLoadingAlteraProcesso = false;
			});
	}

	/**
	 * Função que faz a montagem do body dos detralhes do processo.
	 */
	setBodyDetalhes() {
		const value = this.formAltProcesso.value;
		let fwModelBody: FwModelBody;

		fwModelBody = new FwModelBody('JURA095', 4, this.pk, 'NSZMASTER');

		fwModelBody.models[0].addField(
			'NSZ_CCLIEN',
			this.JuriGenerics.getValueByOption(this.cmbUnidade, 'A1_COD'),
			0,
			'C'
		);
		fwModelBody.models[0].addField(
			'NSZ_LCLIEN',
			this.JuriGenerics.getValueByOption(this.cmbUnidade, 'A1_LOJA'),
			0,
			'C'
		);
		fwModelBody.models[0].addField('NSZ_TIPOAS', this.formAltProcesso.value.tpAsj, 0, 'C');
		fwModelBody.models[0].addField('NSZ_CAREAJ', value.area, 0, 'C');
		fwModelBody.models[0].addField('NSZ_SIGLA1', value.partic1, 0, 'C');
		fwModelBody.models[0].addField('NSZ_SIGLA2', value.partic2, 0, 'C');
		fwModelBody.models[0].addField('NSZ_SIGLA3', value.partic3, 0, 'C');
		fwModelBody.models[0].addField('NSZ_CRITO', value.rito, 0, 'C');
		fwModelBody.models[0].addField('NSZ_CCUSTO', value.centroCusto, 0, 'C');
		fwModelBody.models[0].addField('NSZ_COBJET', value.assunto, 0, 'C');
		fwModelBody.models[0].addField('NSZ_DETALH', value.detObjeto, 0, 'C');
		fwModelBody.models[0].addField('NSZ_OBSERV', value.observacao, 0, 'C');
		fwModelBody.models[0].addField('NSZ_VLCAUS', value.vlrCausa, 0, 'C');
		fwModelBody.models[0].addField('NSZ_DTCAUS', value.dataVlr, 0, 'D');
		fwModelBody.models[0].addField('NSZ_CFCORR', value.fCorrecao, 0, 'C');
		fwModelBody.models[0].addField('NSZ_CMOCAU', value.moedaVlCaus, 0, 'C');

		// Adiciona os campos extras no body
		// tslint:disable-next-line: forin -- comentário necessário para ignorar o fonir
		for (const campos in this.oValores) {
			const nPosCampoExt: number = this.listAuxExtFi.findIndex(x => x.property == campos);
			if (nPosCampoExt > -1) {
				const cTypeForm: string = this.listAuxExtFi[nPosCampoExt].type;
				let cTypeBody: 'C' | 'N' | 'D' | 'L' = 'C';
				let value: string | number = this.oValores[campos];

				switch (cTypeForm) {
					case 'date':
						cTypeBody = 'D';
						break;
					case 'boolean':
						if (value) {
							value = 'T';
						} else {
							value = 'F';
						}
						break;
				}

				fwModelBody.models[0].addField(campos, value, 0, cTypeBody);
			}
		}
		return fwModelBody.serialize();
	}

	/**
	 * Retorna para a tela anterior
	 */
	onCancel() {
		this.router.navigate([this.routingState.getPreviousUrl()], { relativeTo: this.route });
	}

	// ENVOLVIDOS

	/**
	 * Cria formEnvolvido
	 * @param envolvido Estrutura do form
	 */
	createFormEnvolv(envolvido: ItemEnvolvido) {
		const row: number = -1;
		// Envolvido
		this.formEnvolvido = this.formBuilder.group({
			polo: [envolvido.polo, Validators.compose([Validators.required])],
			tipoEnvol: [envolvido.tipoEnvol, Validators.compose([Validators.required])],
			principal: [envolvido.principal, Validators.compose([Validators.required])],
			entidade: [envolvido.entidade, Validators.compose([Validators.required])],
			cpfcnpj: [envolvido.cpfcnpj],
			nome: [envolvido.nome, Validators.compose([Validators.required])],
			cargo: [envolvido.cargo],
			localtrab: [envolvido.localtrab],
			dataAdm: [envolvido.dataAdm],
			dataDem: [envolvido.dataDem],
			rowId: [row],
			cod: [envolvido.cod],
		});
	}

	/**
	 * form parte contrária
	 * @param ptContraria - Estrutura do formulário da Parte Contrária.
	 */
	createFormPtCont(ptContraria: CamposParteContraria) {
		this.formParteCont = this.formBuilder.group({
			nomePtContraria: [
				ptContraria.nomePtContraria,
				Validators.compose([Validators.required]),
			],
			tpPessoa: [ptContraria.tpPessoa, Validators.compose([Validators.required])],
			cpfcnpjPtCont: [ptContraria.cpfcnpjPtCont],
			email: [ptContraria.email],
		});
	}

	/**
	 * Cria um novo envolvido
	 */
	novoEnvolvido() {
		this.btnSalvarEnvolvido = this.litAltProcesso.envolvidos.incluir;
		this.isEditaEnvolvido = true;
		setTimeout(() => {
			this.formEnvolvido.reset();
			this.clearEnvolvidos();
		}, 500);
	}

	/**
	 * Limpa formEnvolvido
	 */
	clearEnvolvidos() {
		this.createFormEnvolv(new ItemEnvolvido());
	}

	/**
	 * Função ativada ao selecionar o polo, definindo qual será o filtro
	 * para o tipo de envolvimento
	 * @param polo - Polo selecionado no formulário de envolvidos.
	 */
	filterTpEnvol(polo: string) {
		this.isDisabletpEnvol = polo == undefined;
		this.formEnvolvido.patchValue({ tipoEnvol: '' });

		this.filtroTipoEnv = this.makeFilterTpEnvol();
		this.tpEnvolvAdapterService.setFiltroP(this.filtroTipoEnv);
	}

	/**
	 * Define o filtro do tipo de envolvido
	 */
	makeFilterTpEnvol() {
		const polo: string = this.formEnvolvido.value.polo;
		let filter: string = '';

		switch (polo) {
			case '1':
				filter = "NQA_POLOAT = '1'";
				break;
			case '2':
				filter = "NQA_POLOPA = '1'";
				break;
			case '3':
				filter = "NQA_TERCIN = '1'";
				break;
			case '4':
				filter = "NQA_SOCIED = '1'";
				break;
			case '5':
				filter = "NQA_PARTIC = '1'";
				break;
			case '6':
				filter = "NQA_ADMINI = '1'";
				break;
		}
		return filter;
	}

	/**
	 * Preenche e Habilita o campo  CPF / CNPJ após selecionar a entidade
	 */
	incluiCpfCnpj() {
		let codCGC: string = '';
		if (this.cmbTipoEnvolvido.selectedOption != undefined) {
			const tipoEntidade = this.cmbTipoEnvolvido.selectedOption.value;

			if (tipoEntidade == 'SRA') {
				if (this.cmbEnvolvidoNome.selectedOption != undefined) {
					codCGC = (<lpFuncionarioReturnStruct>this.cmbEnvolvidoNome.selectedOption).cgc;
				}
			} else {
				if (this.cmbEnvolvidoNome.selectedOption != undefined) {
					const fieldsExtra: adaptorReturnStruct = <adaptorReturnStruct>(
						this.cmbEnvolvidoNome.selectedOption
					);

					switch (tipoEntidade) {
						case 'SA1':
							codCGC = this.JuriGenerics.findValueByName(
								fieldsExtra.getExtras(),
								'A1_CGC'
							);
							break;
						case 'SA2':
							codCGC = this.JuriGenerics.findValueByName(
								fieldsExtra.getExtras(),
								'A2_CGC'
							);
							break;
						case 'SU5':
							codCGC = this.JuriGenerics.findValueByName(
								fieldsExtra.getExtras(),
								'U5_CPF'
							);
							break;
						case 'NZ2':
							codCGC = this.JuriGenerics.findValueByName(
								fieldsExtra.getExtras(),
								'NZ2_CGC'
							);
							break;
					}
				}
			}
			// Filtra o Local de Trabalho de acordo com Cliente e Loja selecionado pelo usuário
			if (this.cmbEnvolvidoNome.selectedOption != undefined) {
				let codCliLj: string = '';
				if (
					this.formEnvolvido.value.entidade === 'SA1' &&
					this.JuriGenerics.changeUndefinedToEmpty(
						this.cmbEnvolvidoNome.selectedOption.value.toString()
					) != ''
				) {
					codCliLj =
						"'" +
						this.JuriGenerics.changeUndefinedToEmpty(
							this.cmbEnvolvidoNome.selectedOption.value.toString()
						) +
						"'";
				}
				this.itemsEnvolv.forEach(items => {
					if (items.entidade === 'SA1' && items.polo === '2') {
						if (codCliLj != '') {
							codCliLj += ", '" + items.codEntidade + "'";
						} else {
							codCliLj += "'" + items.codEntidade + "'";
						}
					}
				});

				if (codCliLj != undefined) {
					if (codCliLj === '') {
						codCliLj = "''";
					}
					this.FilterCodLoja = 'NTB_CCLIEN||NTB_LOJACL IN (' + codCliLj + ')';
					this.localTrabAdapterService.setFiltroP(this.FilterCodLoja);
					this.isDisableLocTrabalho = false;
				}
			} else {
				this.isDisableLocTrabalho = true;
			}
		}

		// Ao alterar um envolvido, verifica se o usuário havia digitado um cpf manualmente, senão, seta o cpf do cadastro da entidade.
		if (codCGC != '' && codCGC != undefined) {
			this.formEnvolvido.patchValue({
				cpfcnpj: codCGC,
			});
			this.isDisableCpfCnpj = true;
		}
		this.isDisableCpfCnpj = false;
	}

	/**
	 * Verifica se a entidade selecionada é de parte contrária, e verifica se o nome digitado está presente no retorno
	 * do combo, se não tiver, é apresentado um modal para inclusão, verificação é acionada assim que se sai do campo
	 * com TAB ou ENTER
	 * @param event - obtem dados do evento html do componente.
	 */
	vldParteCont(event) {
		if (event.keyCode == 9 || event.keyCode == 13) {
			if (this.cmbTipoEnvolvido.selectedOption != undefined) {
				if (this.cmbTipoEnvolvido.selectedOption.value == 'NZ2') {
					// verifica que a entidade selecionada é parte contrária
					if (
						this.cmbEnvolvidoNome.selectedOption == undefined ||
						(this.cmbEnvolvidoNome.selectedOption.label != '' &&
							this.cmbEnvolvidoNome.selectedOption.label !=
								this.cmbEnvolvidoNome
									.previousSearchValue) // verifica se o nome não esta presente no combo
					) {
						this.nomePartCont = this.cmbEnvolvidoNome.previousSearchValue; // informação digitada no nome 
						this.modalConfirmIncl.open();
						// carrega o campo nome do cadastro de parte contrária, com o valor digitado no envolvido
						this.formParteCont.patchValue({
							nomePtContraria: this.nomePartCont,
						});
					}
				}
			}
		}
	}

	/**
	 * Responsável por efetuar a inclusão da parte contrária com as informações setadas no bodyPutPartCon
	 */
	postPartCont() {
		if (this.beforeSubPtC()) {
			this.fwmodel.restore();
			this.fwmodel.setModelo('JURA184');
			this.fwmodel.post(this.bodyPutPartCon(), 'postParContraria').subscribe(data => {
				if (this.JuriGenerics.changeUndefinedToEmpty(data) != '') {
					const codNZ2 = this.JuriGenerics.findValueByName(
						data.models[0].fields,
						'NZ2_COD'
					);
					const cpfcnpj = this.JuriGenerics.findValueByName(
						data.models[0].fields,
						'NZ2_CGC'
					);

					this.poNotification.success(this.litAltProcesso.parteContraria.sucesIncPartC);
					this.modalIncPtCont.close();
					this.formParteCont.reset();
					this.formEnvolvido.patchValue({
						nome: codNZ2,
						cpfcnpj,
					});
				}
			});
		}
	}

	/**
	 * Monta o body no formato de JSON e seta os valores que estão no body
	 */
	bodyPutPartCon() {
		const value = this.formParteCont.value;
		const fwModelBody: FwModelBody = new FwModelBody('JURA184', 3, undefined, 'NZ2MASTER');

		fwModelBody.models[0].addField('NZ2_NOME', value.nomePtContraria);
		fwModelBody.models[0].addField('NZ2_TIPOP', value.tpPessoa);
		fwModelBody.models[0].addField('NZ2_CGC', value.cpfcnpjPtCont);
		fwModelBody.models[0].addField('NZ2_EMAIL', value.email);

		return fwModelBody.serialize();
	}

	/**
	 * Validação dos campos antes de fazer o submit
	 */
	beforeSubPtC() {
		const isOk: boolean = this.formParteCont.valid; // valida os campos obrigatórios

		if (!isOk) {
			this.poNotification.error(this.litAltProcesso.parteContraria.msgCamposObgrigat);
		}
		return isOk;
	}

	/**
	 * Preenche o Form com os dados do registro a ser alterado
	 * @param row linha a ser alterada
	 */
	alteraEnvolvido(row: GridEnvolvido) {
		this.btnSalvarEnvolvido = this.litAltProcesso.envolvidos.alterar;
		this.isEditaEnvolvido = true;
		this.isLoadingCadastro = false;
		this.setComboNomeEntidade(row.entidade, row.codEntidade);

		this.formEnvolvido.patchValue({
			polo: row.polo,
			principal: row.principal,
			cargo: row.cargo,
			localtrab: row.localTrabalho,
			dataAdm: row.dataAdm,
			dataDem: row.dataDem,
			entidade: row.entidade,
			rowId: row.id,
			tipoEnvol: row.codTipoEnv,
			cpfcnpj: row.cpfcnpj,
			codTipoEnv: row.codTipoEnv,
			cod: row.cod,
			cajuri: row.cajuri,
		});

		this.isLoadingCadastro = true;
	}

	/**
	 * Traz o filtro de acordo com a entidade origem selecionada
	 * (SA1 / SA2/ SU5/ NZ2/ SRA)
	 * @param entidade - Entidade do envolvido.
	 * @param codEntidade - Código do envolvido.
	 */
	setComboNomeEntidade(entidade: string = '', nomeEnvolvido?: string) {
		if (entidade == '') {
			this.formEnvolvido.patchValue({
				nome: '',
				cpfcnpj: '',
				localtrab: '',
			});
		} else {
			switch (entidade) {
				case 'SA1':
					this.nomeEnvAdapterService.setup(
						new adaptorFilterParam(
							'JCLIDEP',
							'',
							'A1_NOME,A1_CGC',
							'A1_NOME,A1_COD,A1_LOJA',
							'A1_NOME [A1_COD/A1_LOJA]',
							'A1_COD,A1_LOJA',
							'A1_CODA1_LOJA',
							'getClienteEnvolvido',
							undefined,
							undefined,
							undefined,
							{
								bGetAllLevels: false,
								bUseVirtualField: false,
								bUseEmptyField: false,
								arrSetFields: ['A1_COD', 'A1_LOJA', 'A1_CGC', 'A1_NOME'],
							},
							'A1_CGC'
						)
					);
					break;
				case 'SA2':
					this.nomeEnvAdapterService.setup(
						new adaptorFilterParam(
							'JURA132',
							'',
							'A2_NOME,A2_CGC',
							'A2_NOME,A2_COD,A2_LOJA',
							'A2_NOME [A2_COD/A2_LOJA]',
							'A2_COD,A2_LOJA',
							'A2_CODA2_LOJA',
							'getFornecedorEnvolvido',
							undefined,
							undefined,
							undefined,
							{
								bGetAllLevels: false,
								bUseVirtualField: true,
								bUseEmptyField: false,
								arrSetFields: [],
							},
							'A2_CGC'
						)
					);
					break;
				case 'SU5':
					this.nomeEnvAdapterService.setup(
						new adaptorFilterParam(
							'JURA232',
							'',
							'U5_CONTAT,U5_CPF',
							'U5_CONTAT',
							'',
							'U5_CODCONT',
							'',
							'getContatoEnvolvido',
							undefined,
							undefined,
							undefined,
							{
								bGetAllLevels: false,
								bUseVirtualField: true,
								bUseEmptyField: false,
								arrSetFields: [],
							},
							'U5_CPF'
						)
					);
					break;
				case 'NZ2':
					this.nomeEnvAdapterService.setup(
						new adaptorFilterParam(
							'JURA184',
							'',
							'NZ2_NOME,NZ2_CGC',
							'NZ2_NOME',
							'',
							'NZ2_COD',
							'',
							'getParteContrariaEnvolvido',
							undefined,
							undefined,
							undefined,
							{
								bGetAllLevels: false,
								bUseVirtualField: true,
								bUseEmptyField: false,
								arrSetFields: [],
							},
							'NZ2_CGC'
						)
					);
					break;
			}
		}

		this.formEnvolvido.patchValue({ nome: nomeEnvolvido });
	}

	/**
	 * Lista os envolvidos no grid
	 * @param data - Dados dos envolvidos do processos.
	 */
	gravaListaEnvolvidos(data: any) {
		this.itemsEnvolv = [];

		data.forEach(item => {
			const itemEnvolvido = new GridEnvolvido();

			itemEnvolvido.nome = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NT9_NOME')
			);
			itemEnvolvido.descTipoEnv = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NT9_DTPENV')
			);
			itemEnvolvido.polo = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NT9_TIPOEN')
			);
			itemEnvolvido.poloDescri = this.listPolos[
				parseInt(
					this.JuriGenerics.changeUndefinedToEmpty(
						this.JuriGenerics.findValueByName(item.fields, 'NT9_TIPOEN')
					)
				) - 1
			].label;
			itemEnvolvido.cargo = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NT9_CCRGDP')
			);
			itemEnvolvido.localTrabalho = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NT9_CLOCTR')
			);
			itemEnvolvido.dataAdm = this.JuriGenerics.makeDate(
				this.JuriGenerics.changeUndefinedToEmpty(
					this.JuriGenerics.findValueByName(item.fields, 'NT9_DTADM')
				),
				'yyyy-mm-dd'
			);
			itemEnvolvido.dataDem = this.JuriGenerics.makeDate(
				this.JuriGenerics.changeUndefinedToEmpty(
					this.JuriGenerics.findValueByName(item.fields, 'NT9_DTDEMI')
				),
				'yyyy-mm-dd'
			);
			itemEnvolvido.entidade = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NT9_ENTIDA')
			);
			itemEnvolvido.codEntidade = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NT9_CODENT')
			);
			itemEnvolvido.codTipoEnv = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NT9_CTPENV')
			);
			itemEnvolvido.cpfcnpj = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NT9_CGC')
			);
			itemEnvolvido.cajuri = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NT9_CAJURI')
			);
			itemEnvolvido.cod = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NT9_COD')
			);
			itemEnvolvido.id = item.id;
			itemEnvolvido.btnEditEnv = ['editarEnv', 'excluirEnv'];

			itemEnvolvido.principal = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NT9_PRINCI')
			);

			if (itemEnvolvido.principal == '1') {
				itemEnvolvido.poloPrincipal = this.litAltProcesso.listSimNao.sim;
			} else {
				itemEnvolvido.poloPrincipal = this.litAltProcesso.listSimNao.nao;
			}

			this.itemsEnvolv.push(itemEnvolvido);
		});
	}

	/**
	 * Função responsável por cancelar a operação de inclusão ou alteração do envolvido.
	 */
	cancelEnvolvido() {
		this.getDetailProcess();
		this.clearEnvolvidos();
		this.isEditaEnvolvido = false;
	}

	/**
	 * Salva as alterações dos envolvidos
	 */
	salvarEnvolvido() {
		let envolvido: GridEnvolvido;

		if (this.formEnvolvido.valid) {
			envolvido = new GridEnvolvido();

			envolvido.codEntidade = this.formEnvolvido.value.nome;
			envolvido.cpfcnpj = this.formEnvolvido.value.cpfcnpj;
			envolvido.entidade = this.formEnvolvido.value.entidade;
			envolvido.principal = this.formEnvolvido.value.principal;
			envolvido.cargo = this.formEnvolvido.value.cargo;
			envolvido.localTrabalho = this.formEnvolvido.value.localtrab;
			envolvido.dataAdm = this.formEnvolvido.value.dataAdm;
			envolvido.dataDem = this.formEnvolvido.value.dataDem;
			envolvido.codTipoEnv = this.formEnvolvido.value.tipoEnvol;
			envolvido.polo = this.cmbEnvolvidoPolo.selectedOption.value.toString();
			envolvido.poloDescri = this.cmbEnvolvidoPolo.selectedOption.label;
			envolvido.descTipoEnv = this.cmbEnvolvidoTipo.selectedOption.label;
			envolvido.nome = this.cmbEnvolvidoNome.selectedOption.label;
			envolvido.cajuri = this.cajuri;
			envolvido.cod = this.formEnvolvido.value.cod;
			envolvido.id = this.formEnvolvido.value.rowId;

			if (this.formEnvolvido.value.rowId >= 0) {
				const indexReg = this.itemsEnvolv.findIndex(
					x => x.id == this.formEnvolvido.value.rowId
				);
				this.itemsEnvolv[indexReg] = envolvido;
			} else {
				envolvido.id = this.itemsEnvolv.length + 1;
				this.itemsEnvolv.push(envolvido);
			}

			// Validação para preencher justificativa de alteração de processo encerrado 
			if (this.isProcEncerrado) {
				this.modalJustif.openModal(this.cajuri, () => {
					this.submitEnvolvido(envolvido.id);
				});
			} else {
				this.submitEnvolvido(envolvido.id);
			}
		} else {
			if (this.btnSalvarEnvolvido == this.litAltProcesso.instancia.incluir) {
				this.poNotification.error(this.litAltProcesso.instancia.errFormIncInsta);
			} else {
				this.poNotification.error(this.litAltProcesso.instancia.errFormAltInsta);
			}
		}
	}

	/**
	 * Chama o método PUT para realizar a gravação do envolvido.
	 * @param id - Id do Envolvido.
	 */
	submitEnvolvido(id: number, isDelete: boolean = false) {
		const body = this.setBodyEnvolvido(id);
		this.isLoadingDetalhes = false;
		this.isLoadingCadastro = false;

		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA095');
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.put(this.pk, body, 'Alterar envolvidos do processo').subscribe(data => {
			if (data != '') {
				this.poNotification.success(this.litAltProcesso.envolvidos.sucAltEnvolvido);
				this.isEditaEnvolvido = false;

				// Limpa o Form após incluir os dados no GRID 
				this.clearEnvolvidos();
				this.getDetailProcess();
			} else {
				if (!isDelete) {
					this.itemsEnvolv.splice(this.itemsEnvolv.length - 1, 1);
				}
			}

			this.isLoadingAlteraProcesso = false;
			this.isLoadingDetalhes = true;
		});
	}

	/**
	 * Monta o body com os dados que serão gravados na inclusão do processo
	 * @param id - Id do envolvido.
	 */
	setBodyEnvolvido(id: number) {
		const fwModelBody: FwModelBody = new FwModelBody('JURA095', 4, this.pk);
		let gridEnvolvidos: FwModelBodyModels;

		fwModelBody.addModel('NSZMASTER', 'FIELDS').addField('NSZ_COD', this.cajuri, 2);
		fwModelBody.models[0].addField('NSZ_TIPOAS', this.formAltProcesso.value.tpAsj, 0, 'C');
		gridEnvolvidos = fwModelBody.models[0].addModel('NT9DETAIL', 'GRID', 1);

		this.itemsEnvolv.forEach(items => {
			let itemEnvolvidos: FwModelBodyItems;

			itemEnvolvidos = gridEnvolvidos.addItems(0, items.deleted);
			if (id == items.id) {
				if (items.cod != undefined && items.cod != '') {
					itemEnvolvidos.addField('NT9_COD', items.cod, 'C');
				}
				itemEnvolvidos.addField('NT9_CAJURI', this.cajuri, 'C');
				itemEnvolvidos.addField('NT9_ENTIDA', items.entidade, 'C');
				itemEnvolvidos.addField('NT9_CODENT', items.codEntidade, 'C');
				itemEnvolvidos.addField('NT9_PRINCI', items.principal, 'C');
				itemEnvolvidos.addField('NT9_TIPOEN', items.polo, 'C');
				itemEnvolvidos.addField('NT9_CTPENV', items.codTipoEnv, 'C');

				// Valida CPF / CNPJ e Tipo de Pessoa
				if (items.cpfcnpj != '' && items.cpfcnpj != undefined) {
					itemEnvolvidos.addField('NT9_CGC', items.cpfcnpj, 'C');
				}
				// Se há Local de Trabalho, deve preencher os campos de Cliente (NT9_CEMPCL) e Loja Cliente (NT9_LOJACL)
				if (items.localTrabalho != '' && items.localTrabalho != undefined) {
					items.codEntidade = this.JuriGenerics.changeUndefinedToEmpty(items.codEntidade);

					itemEnvolvidos.addField('NT9_CLOCTR', items.localTrabalho, 'C');
					itemEnvolvidos.addField(
						'NT9_CEMPCL',
						items.codEntidade.substring(0, items.codEntidade.indexOf('-')),
						'C'
					);
					itemEnvolvidos.addField(
						'NT9_LOJACL',
						items.codEntidade.substring(items.codEntidade.indexOf('-') + 1),
						'C'
					);
				}
				itemEnvolvidos.addField('NT9_CCRGDP', items.cargo, 'C');
				itemEnvolvidos.addField('NT9_DTADM', items.dataAdm.replace(/-/g, ''), 'C');
				itemEnvolvidos.addField('NT9_DTDEMI', items.dataDem.replace(/-/g, ''), 'C');
			}
		});
		return fwModelBody.serialize();
	}

	/**
	 * Exclui o envolvido
	 * @param row - Dados da linha selecionada no grid de envolvidos..
	 */
	excluiEnvolvido(row: GridEnvolvido) {
		this.isLoadingDetalhes = false;
		this.isLoadingCadastro = false;

		row.deleted = 1;
		this.submitEnvolvido(row.id, true);
		this.modalExcEnvo.close();
		this.getDetailProcess();
	}

	/**
	 * Exclui a instância
	 * @param row - Dados da linha selecionada no grid da instância.
	 */
	excluiInstancia(row: GridInstancia) {
		let codChild: string = '';
		let msgExclInstPai: string = '';

		// Varre o grid para saber se a linha que esta sendo excluída possui instancias filhas
		for (let nX = 0; nX < this.itemsInstancia.length; nX++) {
			if (this.itemsInstancia[nX].instanciaOri == row.cod) {
				codChild = this.itemsInstancia[nX].processo;
			}
		}

		if (this.JuriGenerics.changeUndefinedToEmpty(codChild) != '') {
			msgExclInstPai = this.thfI18nPipe.transform(
				this.litAltProcesso.instancia.erroExcInstPai,
				[codChild]
			);
			this.poNotification.error(msgExclInstPai.toString());
		} else {
			this.isLoadingDetalhes = false;
			this.isLoadingCadastro = false;
			row.deleted = 1;
			this.submitInstancia(row.id, true);
		}
		this.modalExcInsta.close();
	}

	/**
	 * Lista as instâncias no grid
	 * @param data - array com dados da da instância
	 */
	listInstancias(data: any) {
		this.itemsInstancia = [];

		data.forEach(item => {
			const itemInstancia = new GridInstancia();
			const isAtual: boolean =
				this.JuriGenerics.changeUndefinedToEmpty(
					this.JuriGenerics.findValueByName(item.fields, 'NUQ_INSATU')
				) == '1';

			itemInstancia.id = item.id;
			itemInstancia.cajuri = this.cajuri;
			itemInstancia.cod = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NUQ_COD')
			);
			itemInstancia.instancia = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NUQ_INSTAN')
			);
			itemInstancia.instanciaOri = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NUQ_CINSTP')
			);
			itemInstancia.descInstancia = this.JuriGenerics.changeUndefinedToEmpty(
				this.listInstancia[this.JuriGenerics.findValueByName(item.fields, 'NUQ_INSTAN') - 1]
					.label
			);
			itemInstancia.natureza = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NUQ_CNATUR')
			);
			itemInstancia.descNatureza = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NUQ_DNATUR')
			);
			itemInstancia.processo = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NUQ_NUMPRO')
			);
			itemInstancia.atual = isAtual;

			if (itemInstancia.atual) {
				itemInstancia.origem = this.litAltProcesso.listSimNao.sim;
			} else {
				itemInstancia.origem = this.litAltProcesso.listSimNao.nao;
			}

			itemInstancia.dtDistrib = this.JuriGenerics.makeDate(
				this.JuriGenerics.changeUndefinedToEmpty(
					this.JuriGenerics.findValueByName(item.fields, 'NUQ_DTDIST')
				),
				'yyyy-mm-dd'
			);

			itemInstancia.dtEncerra = this.JuriGenerics.makeDate(
				this.JuriGenerics.changeUndefinedToEmpty(
					this.JuriGenerics.findValueByName(item.fields, 'NUQ_DTENC')
				),
				'yyyy-mm-dd'
			);

			itemInstancia.tipoAcao = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NUQ_CTIPAC')
			);
			itemInstancia.desctipoAcao = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NUQ_DTIPAC')
			);
			itemInstancia.comarca = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NUQ_CCOMAR')
			);
			itemInstancia.descComarca = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NUQ_DCOMAR')
			);
			itemInstancia.uf = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NUQ_ESTADO')
			);
			itemInstancia.foro = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NUQ_CLOC2N')
			);
			itemInstancia.descForo = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NUQ_DLOC2N')
			);
			itemInstancia.vara = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NUQ_CLOC3N')
			);
			itemInstancia.descVara = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NUQ_DLOC3N')
			);
			itemInstancia.correspondente = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NUQ_CCORRE')
			);
			itemInstancia.lojaCorresp = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NUQ_LCORRE')
			);
			itemInstancia.descCorresp = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NUQ_DCORRE')
			);
			itemInstancia.btnEditIns = ['callAndamentos', 'editarInstancia', 'excluirInstancia'];
			itemInstancia.localizacao = itemInstancia.descComarca + ' / ' + itemInstancia.descForo;
			itemInstancia.staInstancia = this.JuriGenerics.changeUndefinedToEmpty(
				this.JuriGenerics.findValueByName(item.fields, 'NUQ_STATUS')
			);

			if (this.JuriGenerics.changeUndefinedToEmpty(itemInstancia.descVara) != '') {
				itemInstancia.localizacao += ' / ' + itemInstancia.descVara;
			}

			this.itemsInstancia.push(itemInstancia);
		});

		this.getSequenciaInstancia();
	}

	/**
	 * Responsável por inicializar o Formulário de Instancia
	 * @param altInstancia - Informar a classe do formulário de Instancia.
	 */
	createFormInstancia(altInstancia: GridInstancia) {
		const row: number = -1;

		this.formInstancia = this.formBuilder.group({
			id: [altInstancia.id],
			cajuri: [altInstancia.cajuri],
			cod: [altInstancia.cod],
			instanciaOri: [altInstancia.instanciaOri],
			dtDistrib: [altInstancia.dtDistrib],
			dtEncerra: [altInstancia.dtEncerra],
			status: [altInstancia.status],
			tipoAcao: [altInstancia.tipoAcao],
			comarca: [altInstancia.comarca, Validators.compose([Validators.required])],
			uf: [altInstancia.uf],
			foro: [altInstancia.foro, Validators.compose([Validators.required])],
			vara: [altInstancia.vara],
			correspondente: [altInstancia.correspondente],
			rowId: [row],
			instancia: [altInstancia.instancia, Validators.compose([Validators.required])],
			atual: [altInstancia.atual, Validators.compose([Validators.required])],
			natureza: [altInstancia.natureza, Validators.compose([Validators.required])],
			processo: [altInstancia.processo, Validators.compose([Validators.required])],
		});
	}

	/**
	 * Preenche o Form com os dados do registro a ser alterado
	 * @param row linha a ser alterada
	 */
	alteraInstancia(row: GridInstancia) {
		this.btnSalvarInstancia = this.litAltProcesso.instancia.alterar;
		this.isLoadingDetalhes = false;
		this.isLoadingCadastro = false;
		this.isEditaInstancia = true;
		this.buscaInstanciasOrigens.setFilters(this.pk, row.cod);
		this.buscaInstanciasOrigens.setLiteralLabel(this.listInstancia);

		if (row.atual) {
			this.isDisableInsOri = false;
			this.classSwitch = 'po-lg-4';
			this.classInstancia = 'po-lg-4';
			this.classDtDistr = 'po-lg-4';
		} else {
			this.isDisableInsOri = true;
			this.classSwitch = 'po-lg-2';
			this.classInstancia = 'po-lg-3';
			this.classDtDistr = 'po-lg-3';
		}

		this.foroAdapterService.setComarca(row.comarca);
		this.varaAdapterService.SetForo(row.foro);
		this.varaAdapterService.setComarca(row.comarca);
		this.varaAdapterService.SetForo(row.foro);

		setTimeout(() => {
			this.formInstancia.reset();

			this.formInstancia.patchValue({
				cajuri: row.cajuri,
				cod: row.cod,
				instancia: row.instancia,
				atual: row.atual,
				instanciaOri: row.instanciaOri,
				natureza: row.natureza,
				processo: row.processo,
				dtDistrib: row.dtDistrib,
				dtEncerra: row.dtEncerra,
				status: row.status,
				tipoAcao: row.tipoAcao,
				comarca: row.comarca,
				uf: row.uf,
				foro: row.foro,
				vara: row.vara,
				correspondente: row.correspondente,
				rowId: row.id,
				id: row.id,
			});

			// Guarda o correspondente atual
			this.correAtual = row.correspondente + row.lojaCorresp;
		}, 500);

		this.isLoadingCadastro = true;
		this.isLoadingDetalhes = true;
	}

	/**
	 * Retorna a lista de naturezas
	 */
	getNatureza() {
		this.fwmodel.restore();
		this.fwmodel.setPageSize('99');
		this.fwmodel.setModelo('JURA001');
		this.fwmodel.get('getNatureza').subscribe(data => {
			data.resources.forEach(item => {
				const naturezas = new Select();
				naturezas.label = this.JuriGenerics.findValueByName(
					item.models[0].fields,
					'NQ1_DESC'
				);
				naturezas.value = this.JuriGenerics.findValueByName(
					item.models[0].fields,
					'NQ1_COD'
				);
				naturezas.extra = this.JuriGenerics.findValueByName(
					item.models[0].fields,
					'NQ1_TIPO'
				);
				this.listNaturezas.push(naturezas);
			});
		});
	}

	/**
	 * Chama a função para filtrar o tipo de ação de acordo com a natureza selecionada
	 * @param natureza - Natureza selecionada no formulário da instância.
	 */
	chooseNat(natureza: string) {
		let origemNatureza: string = '';
		let filterQry: string = '';
		this.listTpAcao = [];

		if (this.JuriGenerics.changeUndefinedToEmpty(this.formInstancia.value.natureza) == '') {
			this.isDisabletpAcao = true;
		}

		if (this.JuriGenerics.changeUndefinedToEmpty(natureza) != '') {
			origemNatureza = this.listNaturezas.find(x => x.value == natureza).extra;
			filterQry = "NQU_ORIGEM = '" + origemNatureza + "'";
			this.isEditaInstancia = true;
			this.isDisabletpAcao = false;
		} else {
			this.formInstancia.patchValue({ tipoAcao: '' });
		}

		this.tpAcaoAdapterService.setFiltroP(filterQry);
	}

	/**
	 * Retorna a lista de tipos de ação
	 * @param origemNatureza - Natureza selecionado no formulário da instância.
	 */
	getTipoAcao(origemNatureza?: string) {
		let filterQry: string = '';

		if (this.JuriGenerics.changeUndefinedToEmpty(origemNatureza) != '') {
			filterQry = "NQU_ORIGEM = '" + origemNatureza + "'";
		}

		this.tpAcaoAdapterService.setFiltroP(filterQry);
	}

	/**
	 * Preenche a comarca/ foro/ vara com os dados do cadastro de/para (O00) de acordo com os 7 útimos números do processo.
	 * Caso o parâmetro MV_JNUMCNJ do tipo de ssunto jurídico estiver ligado e a natureza validar CNJ (NQ1_VALCNJ = 1),
	 * valida se é um número CNJ válido.
	 */
	validaNumPro() {
		const dadosFormProc = this.formInstancia.value;
		if (dadosFormProc.numeroPro != '' && dadosFormProc.natureza != undefined) {
			const body = this.getBodyValidCNJ();
			this.legalprocess
				.post('validNumPro', body, 'Validação CNJ do Nº Processo.')
				.subscribe(data => {
					this.formInstancia.patchValue({
						comarca: data.comarca,
						foro: data.foro,
						vara: data.vara,
					});

					this.isFromDePara = true;

					if (!data.valid && data.continue) {
						this.modalValidCNJ.open();
					} else if (!data.valid && data.message != '') {
						this.poNotification.error(data.message);
					}
				});
		}
	}

	/**
	 * Chama GET do Legal Process que retorna a lista de FOROS
	 */
	buscaListaForos(codComarca: string) {
		this.foroAdapterService.setComarca(codComarca);

		if (this.JuriGenerics.changeUndefinedToEmpty(codComarca) == '') {
			this.formInstancia.patchValue({ foro: '', vara: '' });
			this.isDisableForo = true;
			this.isDisableVara = true;
		} else {
			this.formInstancia.patchValue({ foro: this.formInstancia.value.foro });
		}

		if (this.JuriGenerics.changeUndefinedToEmpty(this.formInstancia.value.comarca) != '') {
			this.isDisableForo = false;
		}
	}

	/**
	 * Cria o body de request do POST validNumPro, informando:
	 * - Tipo de assunto jurídico;
	 * - Natureza;
	 * - Número do processo.
	 */
	getBodyValidCNJ() {
		const body = new BodyValidNumPro();

		body.tpAssunto = this.formAltProcesso.value.tpAsj;
		body.natureza = this.formInstancia.value.natureza;
		body.numPro = this.formInstancia.value.processo;

		return JSON.stringify(body);
	}

	/**
	 * Chama GET do Legal Process que retorna a lista de FOROS
	 * @param comarca - Comarca selecionada no formulário da instância.
	 * @param foro    - Foro selecionado no formulário da instância.
	 */
	buscaListaVaras(codForo: string) {
		const codComarca: string = this.formInstancia.value.comarca;

		this.varaAdapterService.setComarca(codComarca);
		this.varaAdapterService.SetForo(codForo);

		if (this.JuriGenerics.changeUndefinedToEmpty(codForo) == '') {
			this.formInstancia.patchValue({ vara: '' });
		} else {
			this.formInstancia.patchValue({ vara: this.formInstancia.value.vara });
		}

		if (this.JuriGenerics.changeUndefinedToEmpty(this.formInstancia.value.comarca) != '') {
			this.isDisableForo = false;
		}

		if (this.JuriGenerics.changeUndefinedToEmpty(this.formInstancia.value.foro) != '') {
			this.isDisableVara = false;
		}
	}

	/**
	 * Salva as alterações das instâncias
	 */
	salvarInstancia() {
		let instancia: GridInstancia;
		const cAtual: string = this.JuriGenerics.changeUndefinedToEmpty(this.correAtual);

		// Validação para justificativa quando o correspondente for alterado
		if (
			cAtual != '' &&
			cAtual !=
				this.JuriGenerics.getValueByOption(this.cmbEscritorio, 'A2_COD') +
					this.JuriGenerics.getValueByOption(this.cmbEscritorio, 'A2_LOJA')
		) {
			this.isUpdateCorresp = true;
		} else {
			this.isUpdateCorresp = false;
		}

		if (this.formInstancia.valid) {
			instancia = new GridInstancia();

			instancia.id = this.formInstancia.value.id;
			instancia.cajuri = this.formInstancia.value.cajuri;
			instancia.cod = this.formInstancia.value.cod;
			instancia.instancia = this.formInstancia.value.instancia;
			instancia.instanciaOri = this.formInstancia.value.instanciaOri;
			instancia.atual = this.formInstancia.value.atual;
			instancia.natureza = this.formInstancia.value.natureza;
			instancia.processo = this.formInstancia.value.processo;
			instancia.dtDistrib = this.formInstancia.value.dtDistrib;
			instancia.dtEncerra = this.formInstancia.value.dtEncerra;
			instancia.tipoAcao = this.formInstancia.value.tipoAcao;
			instancia.comarca = this.formInstancia.value.comarca;
			instancia.foro = this.formInstancia.value.foro;
			instancia.vara = this.formInstancia.value.vara;
			instancia.correspondente = this.formInstancia.value.correspondente;
			instancia.lojaCorresp = this.JuriGenerics.getValueByOption(
				this.cmbEscritorio,
				'A2_LOJA'
			);

			if (this.formInstancia.value.rowId >= 0) {
				const indexReg = this.itemsInstancia.findIndex(
					x => x.id == this.formInstancia.value.rowId
				);
				this.itemsInstancia[indexReg] = instancia;
			} else {
				instancia.id = this.itemsInstancia.length + 1;
				this.itemsInstancia.push(instancia);
			}

			// Tratamento para apresentar a modal de justificativa de alteração de correspondente / Alteração de Processo Encerrado
			if (this.isUpdateCorresp) {
				let descInst: string = '';
				const dadosInstancia: DadosInstancia = new DadosInstancia();
				const codCorresp = this.JuriGenerics.getValueByOption(this.cmbEscritorio, 'A2_COD');
				const lojaCorresp = this.JuriGenerics.getValueByOption(
					this.cmbEscritorio,
					'A2_LOJA'
				);

				// Preenche a descrição da instancia
				if (instancia.instancia == '1') {
					descInst = this.litAltProcesso.listInstancia.primeira;
				} else if (instancia.instancia == '2') {
					descInst = this.litAltProcesso.listInstancia.segunda;
				} else if (instancia.instancia == '3') {
					descInst = this.litAltProcesso.listInstancia.tribunalSuperior;
				}

				// Preenche as informações para a modal de justificativa de alteração de correspondente
				dadosInstancia.instancia = instancia.instancia;
				dadosInstancia.descInstancia = descInst;
				dadosInstancia.correspondente = this.cmbEscritorio.selectedOption.label;
				dadosInstancia.motivo = '';
				dadosInstancia.codCorresp = codCorresp;
				dadosInstancia.lojaCorresp = lojaCorresp;

				this.modalCorresp.openModalCorresp(this.cajuri, dadosInstancia, () => {
					// Verifica se o processo esta encerrado para chamar tela de justificativa de encerramento 
					if (this.isProcEncerrado) {
						this.modalJustif.openModal(this.cajuri, () => {
							this.salvaJustifCorresp();
							this.submitInstancia(instancia.id);
						});
					} else {
						this.salvaJustifCorresp();
						this.submitInstancia(instancia.id);
					}
				});
			} else if (this.isProcEncerrado) {
				this.modalJustif.openModal(this.cajuri, () => {
					this.submitInstancia(instancia.id);
				});
			} else {
				this.submitInstancia(instancia.id);
			}
		} else {
			if (this.btnSalvarInstancia == this.litAltProcesso.instancia.incluir) {
				this.poNotification.error(this.litAltProcesso.instancia.errFormIncInsta);
			} else {
				this.poNotification.error(this.litAltProcesso.instancia.errFormAltInsta);
			}
		}
	}

	/**
	 * Chama o método PUT para realizar a gravação da instância.
	 * @param id - Id da instância.
	 */
	submitInstancia(id: number, isDeleteInst: boolean = false) {
		const body = this.setBodyInstancia(id);
		let codInstAtu: string = '';

		if (isDeleteInst) {
			const nPosCodAtu: number = this.itemsInstancia.findIndex(x => x.id == id);
			if (nPosCodAtu > -1) {
				codInstAtu = this.itemsInstancia[nPosCodAtu].cod;
			}
		}

		this.isLoadingDetalhes = false;
		this.isLoadingCadastro = false;

		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA095');
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.put(this.pk, body, 'Alterar Instancias do processo').subscribe(data => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.models) != '') {
				// Limpa o Form após incluir os dados no GRID
				this.clearInstancias();

				if (isDeleteInst) {
					const nPosDadosNUQ: number = data.models[0].models.findIndex(
						x => x.id == 'NUQDETAIL'
					);
					let isRet: boolean = true;

					data.models[0].models[nPosDadosNUQ].items.forEach(dados => {
						if (
							this.JuriGenerics.findValueByName(dados.fields, 'NUQ_COD') == codInstAtu
						) {
							isRet = false;
						}
					});

					if (isRet) {
						this.poNotification.success(this.litAltProcesso.instancia.sucExcInstancia);
					} else {
						this.poNotification.error(this.litAltProcesso.instancia.erroExcInst);
					}
				} else if (this.btnSalvarInstancia == this.litAltProcesso.instancia.incluir) {
					this.poNotification.success(this.litAltProcesso.instancia.sucIncInstancia);
				} else {
					this.poNotification.success(this.litAltProcesso.instancia.sucAltInstancia);
				}

				this.isEditaInstancia = false;
				this.getDetailProcess();
			} else {
				this.itemsInstancia.splice(this.itemsInstancia.length - 1, 1);
			}

			this.isLoadingAlteraProcesso = false;
			this.isLoadingDetalhes = true;
		});
	}

	/**
	 * Monta o body com os dados que serão gravados na alteração da Instância
	 * @param id - Id da instância.
	 */
	setBodyInstancia(id: number) {
		this.listAuxItemsInst = this.itemsInstancia;
		const fwModelBody: FwModelBody = new FwModelBody('JURA095', 4, this.pk);
		let gridInstancias: FwModelBodyModels;

		fwModelBody.addModel('NSZMASTER', 'FIELDS').addField('NSZ_COD', this.cajuri, 2);
		fwModelBody.models[0].addField('NSZ_TIPOAS', this.formAltProcesso.value.tpAsj, 0, 'C');

		gridInstancias = fwModelBody.models[0].addModel('NUQDETAIL', 'GRID', 1);

		this.sortAuxItemsInst();

		this.listAuxItemsInst.forEach(items => {
			let itemInstancias: FwModelBodyItems;

			if (this.JuriGenerics.changeUndefinedToEmpty(id.toString()) != '') {
				itemInstancias = gridInstancias.addItems(items.id, items.deleted);
			} else {
				itemInstancias = gridInstancias.addItems(0, items.deleted);
			}

			if (id == items.id) {
				if (items.cod != undefined && items.cod != '') {
					itemInstancias.addField('NUQ_COD', items.cod, 'C');
				}

				itemInstancias.addField('NUQ_CAJURI', this.cajuri, 'C');
				itemInstancias.addField('NUQ_INSTAN', items.instancia, 'C');
				itemInstancias.addField('NUQ_CINSTP', items.instanciaOri, 'C');
				itemInstancias.addField('NUQ_INSATU', items.atual ? '1' : '2', 'C');
				itemInstancias.addField('NUQ_CNATUR', items.natureza, 'C');
				itemInstancias.addField('NUQ_NUMPRO', items.processo, 'C');
				itemInstancias.addField('NUQ_CTIPAC', items.tipoAcao, 'C');
				itemInstancias.addField('NUQ_CCOMAR', items.comarca, 'C');
				itemInstancias.addField('NUQ_CLOC2N', items.foro, 'C');
				itemInstancias.addField('NUQ_CLOC3N', items.vara, 'C');
				itemInstancias.addField('NUQ_CCORRE', items.correspondente, 'C');
				itemInstancias.addField('NUQ_LCORRE', items.lojaCorresp, 'C');
				itemInstancias.addField('NUQ_DTDIST', items.dtDistrib, 'D');
				itemInstancias.addField('NUQ_DTENC', items.dtEncerra, 'D');
			}
		});
		return fwModelBody.serialize();
	}

	/**
	 * Limpa formInstância
	 */
	clearInstancias() {
		this.formInstancia.reset();
		this.createFormInstancia(new GridInstancia());
	}

	/**
	 * Ação ao trocar de step
	 * @param stepper - Stepper atual
	 */
	changeStep(stepper, isOnInit: boolean = true) {
		const lit = this.litAltProcesso;
		this.clearInstancias();
		this.clearEnvolvidos();
		this.isEditaEnvolvido = false;
		this.isEditaInstancia = false;

		if (isOnInit) {
			this.getDetailProcess();
		}

		switch (stepper.currentActiveStep.label) {
			case lit.detalhes.stepperDetalhe:
				this.tituloPagina = lit.titleDet;
				stepper.poSteps._results[2]._status = 'default';
				break;
			case lit.envolvidos.stepperEnvolvido:
				this.tituloPagina = lit.titleDet + ' - ' + lit.envolvidos.stepperEnvolvido;
				break;
			case lit.instancia.stepperInstancia:
				this.tituloPagina = lit.titleDet + ' - ' + lit.instancia.stepperInstancia;
				break;
			case lit.camposAdic.titExtraFields:
				this.tituloPagina = lit.titleDet + ' - ' + lit.camposAdic.titExtraFields;
				break;
		}

		// Mantém todos os steps habilitados
		stepper.poSteps._results.forEach(item => {
			if (item._status == 'disabled') {
				item._status = 'default';
			}
		});
	}

	/**
	 * Cria um novo envolvido
	 */
	novaInstancia() {
		this.btnSalvarInstancia = this.litAltProcesso.instancia.incluir;
		this.isEditaInstancia = true;
		this.isDisableVara = true;
		this.isDisableForo = true;
		this.correAtual = '';
		this.isDisableInsOri = true;
		this.classSwitch = 'po-lg-2';
		this.classInstancia = 'po-lg-3';
		this.classDtDistr = 'po-lg-3';

		this.buscaInstanciasOrigens.setFilters(this.pk, this.formInstancia.value.cod);
		this.buscaInstanciasOrigens.setLiteralLabel(this.listInstancia);

		setTimeout(() => {
			this.getTipoAcao();
			this.clearInstancias();
		}, 500);
	}

	/**
	 * Cancela a alteração de Instância
	 */
	cancelInstancia() {
		this.getDetailProcess();
		this.clearInstancias();
		this.isEditaInstancia = false;
		this.isDisabletpAcao = true;
	}

	/**
	 * Obtém os envolvidos para o titulo do browser
	 */
	getInfoProcess() {
		this.legalprocess.restore();
		this.legalprocess.setCodProc(this.cajuri); // define o processo para obter detalhes
		this.legalprocess
			.get('tlprocess/detail/' + this.cajuri, 'Informações do Processo')
			.subscribe(data => {
				if (data.processes != '') {
					this.titleService.setTitle(data.processes[0].matter[0].description);
				}
			});
	}

	/**
	 * Faz a requisição de post para a justificativa do correspondente
	 */
	salvaJustifCorresp() {
		const cBodyCorresp = window.sessionStorage.getItem('Corresp');

		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA093');

		this.fwmodel
			.post(cBodyCorresp, 'Inclusão de Justificativa de Alt. de Correspondente')
			.subscribe(data => {
				if (this.JuriGenerics.changeUndefinedToEmpty(data) != '') {
					this.poNotification.success('Justificativa salva com sucesso!');
				}
			});
	}

	/**
	 * Função que redireciona a tela para a listagem de andamentos com filtro da instância.
	 * @param codInst - Código da Instância
	 */
	callAndamentos(codInst: string) {
		const url: string =
			'/processo/' +
			encodeURIComponent(this.filial) +
			'/' +
			encodeURIComponent(this.chaveProcesso) +
			'/andamento?codInst=' +
			encodeURIComponent(btoa(codInst));
		window.open(url);
	}

	/**
	 * Função responsável por habilitar o campo de Instância Origem
	 */
	habilitaInstOri() {
		if (this.formInstancia.value.atual) {
			this.isDisableInsOri = false;
			this.classSwitch = 'po-lg-4';
			this.classInstancia = 'po-lg-4';
			this.classDtDistr = 'po-lg-4';

			this.buscaInstanciasOrigens.setFilters(this.pk, this.formInstancia.value.cod);
			this.buscaInstanciasOrigens.setLiteralLabel(this.listInstancia);
		} else {
			this.isDisableInsOri = true;
			this.classSwitch = 'po-lg-2';
			this.classInstancia = 'po-lg-3';
			this.classDtDistr = 'po-lg-3';
		}
	}

	/**
	 * Função responsável por obter as sequencias das instâncias.
	 */
	getSequenciaInstancia() {
		let nSeqPrimary: number = 0;

		this.itemsInstancia.forEach(item => {
			if (this.JuriGenerics.changeUndefinedToEmpty(item.instanciaOri) == '') {
				nSeqPrimary++;
				item.sequencia = nSeqPrimary.toString();
			} else {
				let isRet: boolean = false;
				let nSeqSec: number = 0;

				item.sequencia = this.seqRegistroAnt(item.instanciaOri) + '.';

				this.itemsInstancia.forEach(dados => {
					if (
						dados.sequencia == item.sequencia &&
						dados.sequencia.indexOf(item.sequencia) < 0
					) {
						dados.sequencia += nSeqSec;
						nSeqSec++;
					} else if (
						dados.sequencia.indexOf(item.sequencia) > -1 &&
						item.sequencia.length + 1 >= dados.sequencia.length
					) {
						nSeqSec++;
						isRet = true;
					}
				});

				if (isRet) {
					item.sequencia += nSeqSec;
				}
			}
		});

		this.sortItemsInstancia();
	}

	/**
	 * Função que retorna a sequencia da Instância Origem anterior.
	 * @param instOri - Instância Atual.
	 */
	seqRegistroAnt(instOri: string = '') {
		let cReturn: string = '';
		let nPosRegInst: number = -1;
		nPosRegInst = this.itemsInstancia.findIndex(x => x.cod == instOri);

		if (nPosRegInst > -1) {
			cReturn = this.JuriGenerics.changeUndefinedToEmpty(
				this.itemsInstancia[nPosRegInst].sequencia
			);
		}

		return cReturn;
	}

	/**
	 * Função responsável por ordenar a tabela de instância pela sequencia
	 */
	sortItemsInstancia() {
		this.itemsInstancia.sort((a, b) => {
			const valueA = a.sequencia.replace('.', '');
			const valueB = b.sequencia.replace('.', '');

			if (valueB < valueA) {
				return 1;
			}

			if (valueB > valueA) {
				return -1;
			}

			return 0;
		});
	}

	/**
	 * Função responsável por ordenar a lista auxiliar da tabela de instância
	 * pelo Id da instância.
	 */
	sortAuxItemsInst() {
		this.listAuxItemsInst.sort((a, b) => {
			const valueA = a.id.toString();
			const valueB = b.id.toString();

			if (valueB < valueA) {
				return 1;
			}

			if (valueB > valueA) {
				return -1;
			}

			return 0;
		});
	}

	/**
	 * Função que faz a criação dos campos no dynamic form.
	 * @param oDadosProcess - Objeto com as informações do processo
	 */
	loadExtraFields(oDadosProcess: object) {
		this.isHideLoadCmpAdic = false;
		this.listExtraFields = [];

		this.jurConsultas
			.getExtraFields(this.listCamposDet)
			.subscribe((list: Array<ExtraField>) => {
				this.hasExtraFields = false;
				this.listAuxExtFi = [];

				list.forEach(item => {
					if (item.agrupamento !== '005') {
						this.listAuxExtFi.push(item);
						this.hasExtraFields = true;
					}
				});

				this.listExtraFields = this.listAuxExtFi;
				this.loadValores(this.listAuxExtFi, oDadosProcess);
				this.isHideLoadCmpAdic = true;

				setTimeout(() => {
					this.changeStep(this.statusStepper, false);
				}, 1000);
			});
	}

	/**
	 * Função responsável por retornar ao dynamic form os dados que estão no cadastro do processo.
	 * @param listExtFields - Lista de Fields Extras
	 * @param oDataProcess  - Objeto com as informações do processo
	 */
	loadValores(listExtFields: Array<PoDynamicFormField> = [], oDataProcess: object) {
		let valor: string | number | boolean = '';
		let oValoresAdd: object = [];

		listExtFields.forEach(extField => {
			for (const campos in oDataProcess) {
				if (extField.property == oDataProcess[campos].id) {
					if (extField.type == 'date') {
						valor = this.JuriGenerics.makeDate(
							this.JuriGenerics.changeUndefinedToEmpty(oDataProcess[campos].value),
							'yyyy-mm-dd'
						);
					} else if (extField.type == 'boolean') {
						if (oDataProcess[campos].value == 'T') {
							valor = true;
						} else {
							valor = false;
						}
					} else {
						if (oDataProcess[campos].value != undefined) {
							valor = oDataProcess[campos].value;
						} else {
							valor = '';
						}
					}

					// Adiciona valor ao campo
					oValoresAdd = { [extField.property]: valor };
					this.oValores = Object.assign(this.oValores, oValoresAdd);
				}
			}
		});
	}
}
