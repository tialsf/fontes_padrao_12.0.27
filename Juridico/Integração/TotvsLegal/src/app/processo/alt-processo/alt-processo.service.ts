import { Injectable } from '@angular/core';
import { PoComboFilter, PoComboOption } from '@po-ui/ng-components';
import { Observable } from 'rxjs';
import { FwmodelService } from 'src/app/services/fwmodel.service';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { map } from 'rxjs/operators';
import { SelectOptions } from './alt-processo.component'

/**
 *  Busca a Listas de Instâncias cadastradas no processo.
 */
@Injectable({
	providedIn: 'root'
})
export class BuscaInstanciasOrigens implements PoComboFilter{
	pk:               string               = '';
	codInstAtual:     string               = '';
	listLitInstancia: Array<SelectOptions> = [];

	constructor(
		private fwmodel:      FwmodelService,
		private juriGenerics: JurigenericsService,
	) { }
	
	/**
	 * Função que recebe os filtros para realizar a requisição.
	 * @param pk - Filial + Código do Cajuri em Base 64
	 * @param codInstAtual - Cód. da Instância Atual.
	 */
	setFilters(pk: string = '', codInstAtual: string = '') {
		this.pk = pk;
		this.codInstAtual = codInstAtual;
	}

	/**
	 * Função que obtem as labels do cadastro básicos das Instância.
	 * @param listLiteralsInsta - Lista das instância, label e value.
	 */
	setLiteralLabel(listLiteralsInsta: Array<SelectOptions>){
		this.listLitInstancia = listLiteralsInsta;
	}

	getFilteredData(valor): Observable<Array<AdapterReturnStruct>>{
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA095/' + this.pk);
		this.fwmodel.setCampoVirtual(true);
		return this.fwmodel.get('Busca Instâncias').pipe(map(
			(data) => {
				let listItensReturn: AdapterReturnStruct[] = [];
				let nPosSubModel:    number                = -1;

				if (this.juriGenerics.changeUndefinedToEmpty(data.models[0].models) != '') {
					
					nPosSubModel = data.models[0].models.findIndex(x => x.id == 'NUQDETAIL');
					data.models[0].models[nPosSubModel].items.forEach(item => {

						let itemReturn            = new AdapterReturnStruct();
						let isValidValor: boolean = true;
						let codInstancia: string  = this.juriGenerics.findValueByName(item.fields, 'NUQ_COD');
						let numPro:       string  = this.juriGenerics.findValueByName(item.fields, 'NUQ_NUMPRO');
						let tipoAcao:     string  = this.juriGenerics.findValueByName(item.fields, 'NUQ_DTIPAC');
						
						if (this.juriGenerics.changeUndefinedToEmpty(valor.value) != '') {
							isValidValor = numPro.toUpperCase().indexOf(valor.value.toUpperCase()) > -1 ||
										   tipoAcao.toUpperCase().indexOf(valor.value.toUpperCase()) > -1;
						}

						if (codInstancia != this.codInstAtual && isValidValor ) {

							itemReturn.value = codInstancia;

							switch (this.juriGenerics.findValueByName(item.fields, 'NUQ_INSTAN')) {
								case '1':
									itemReturn.label = this.listLitInstancia[0].label + ' - ';
									break;

								case '2':
									itemReturn.label = this.listLitInstancia[1].label + ' - ';
									break;

								case '3':
									itemReturn.label = this.listLitInstancia[2].label + ' - ';
									break;
							}

							itemReturn.label += numPro + ' - ' + tipoAcao;
							listItensReturn.push(itemReturn);
						}
					});
				}

				return listItensReturn;
			}
		))
	}

	getObjectByValue(valor: string): Observable<AdapterReturnStruct>{
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA095/' + this.pk);
		this.fwmodel.setCampoVirtual(true);
		return this.fwmodel.get('Busca Instâncias').pipe(map(
			(data: any) => {
				let itemReturn:    AdapterReturnStruct = new AdapterReturnStruct;
				let nPosSubModel:  number              = -1;
				let codInstOrigem: string              = '';
				let codInstancia:  string              = '';

				if (this.juriGenerics.changeUndefinedToEmpty(data.models[0].models) != '') {
					nPosSubModel = data.models[0].models.findIndex(x => x.id == 'NUQDETAIL');

					data.models[0].models[nPosSubModel].items.forEach(item => {
						codInstancia = this.juriGenerics.findValueByName(item.fields, 'NUQ_COD');
						
						if (codInstancia == this.codInstAtual) {
							codInstOrigem = this.juriGenerics.findValueByName(item.fields, 'NUQ_CINSTP')
						}
					});

					if (this.juriGenerics.changeUndefinedToEmpty(codInstOrigem) != '') {
						data.models[0].models[nPosSubModel].items.forEach(item => {
							if (codInstOrigem == this.juriGenerics.findValueByName(item.fields, 'NUQ_COD')) {
								switch (this.juriGenerics.findValueByName(item.fields, 'NUQ_INSTAN')) {
									case '1':
										itemReturn.label = '1ª Instância - ';
										break;

									case '2':
										itemReturn.label = '2ª Instância - ';
										break;

									case '3':
										itemReturn.label = 'Tribunal superior - ';
										break;
								}
								itemReturn.label += this.juriGenerics.findValueByName(item.fields, 'NUQ_NUMPRO') + ' - ';
								itemReturn.label += this.juriGenerics.findValueByName(item.fields, 'NUQ_DTIPAC');
								itemReturn.value  = this.juriGenerics.findValueByName(item.fields, 'NUQ_COD');
							}
						});
					}
				}
			 	return itemReturn;
			}
		));
	}
}

/**
 * Estrutura de classe para o adapter de alt-processo.
 */
export class AdapterReturnStruct implements PoComboOption{
	label: string;
	value: string;
}