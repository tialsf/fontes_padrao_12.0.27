/**
 * classe do formulário de Detalhe.
 */
export class Detalhes {
	vlrCausa:    string = '';
	dataVlr:     string = '';
	fCorrecao:   string = '';
	detObjeto:   string = '';
	observacao:  string = '';
	rito:        string = '';
	partic1:     string = '';
	partic2:     string = '';
	partic3:     string = '';
	assunto:     string = '';
	unidade:     string = '';
	centroCusto: string = '';
	moedaVlCaus: string = '';
	area:        string = '';
	cajuri:      string = '';
	altProcesso: string = '';
	tpAsj:       String = '';
}

//Classe do Grid Envolvidos
export class GridEnvolvido {
	editarEnv:     string       = 'editarEnvolvido';
	excluirEnv:    string       = 'excluirEnvolvido';
	id:            number       = 0;
	cajuri:        string       = '';
	cod:           string       = '';
	codEntidade:   string       = '';
	nome:          string       = '';
	polo:          string       = '';
	poloDescri:    string       = '';
	descTipoEnv:   string       = '';
	cpfcnpj:       string       = '';
	tipoPessoa:    string       = '';
	entidade:      string       = '';
	principal:     string       = '';
	poloPrincipal: string       = '';
	cargo:         string       = '';
	localTrabalho: string       = '';
	dataAdm:       string       = '';
	dataDem:       string       = '';
	codTipoEnv:    string       = '';
	deleted:          0|1       = 0;
	btnEditEnv:   Array<string> = [];
}

//Classe do Grid Instâncias
export class GridInstancia {
	editarInstancia:  string        = 'editarInstancia';
	excluirInstancia: string        = 'excluirInstancia';
	callAndamentos:   string        = 'callAndamentos';
	atual:            boolean       = false;
	sequencia:        string        = '';
	status:           string        = '';
	id:               number        = 0;
	cajuri:           string        = '';
	cod:              string        = '';
	instancia:        string        = '';
	instanciaOri:     string        = '';
	descInstancia:    string        = '';
	staInstancia:     string        = '1';
	origem:           string        = '';
	localizacao:      string        = '';
	natureza:         string        = '';
	descNatureza:     string        = '';
	processo:         string        = '';
	dtDistrib:        string        = '';
	dtEncerra:        string        = '';
	tipoAcao:         string        = '';
	desctipoAcao:     string        = '';
	comarca:          string        = '';
	descComarca:      string        = '';
	uf:               string        = '';
	foro:             string        = '';
	vara:             string        = '';
	correspondente:   string        = '';
	lojaCorresp:      string        = '';
	descForo:         string        = '';
	descVara:         string        = '';
	descCorresp:      string        = '';
	deleted:             0|1        = 0;
	btnEditIns:       Array<string> = [];
}


export class Select {
	value:    string;
	label:    string;
	extra:    string;
}

export class BodyValidNumPro {
	tpAssunto : string            = '';
	natureza : string             = '';
	numPro : string               = '';
}

