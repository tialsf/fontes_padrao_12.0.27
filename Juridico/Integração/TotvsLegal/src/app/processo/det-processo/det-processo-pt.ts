const helps = {
	procDetalheLimiteCaracter: '(limite de 250 caracteres)',
};

const messages = {
	errAddEnvolvido: 'Não foi possível incluir envolvido. Verifique os campos obrigatórios.',
	sucGravaProcesso: 'Processo incluido com sucesso.',
	errCamposObrigProcesso:
		'Não foi possível incluir o processo. Verifique os campos obrigatórios.',
	errEnvolvidosSemPrincipal:
		'Há envolvidos definidos para os seguintes polos: {0}. Favor definir o envolvido principal.',
	errEnvolvidosMaisPrincipal:
		'Há mais de um envolvido principal para os seguintes polos: {0}. Favor verificar.',
	sucAddEnvolvido: 'Envolvido incluído com sucesso.',
	sucAltEnvolvido: 'Envolvido alterado com sucesso.',
	sucExcEnvolvido: 'Envolvido excluído com sucesso.',
	errUpdDistribuicao: 'Não foi possível atualizar a distribuição.',
	errBaixaDocs: 'Não foi possível realizar a baixa dos documentos da distribuição.',
	sucBaixaDocs: 'O(s) documento(s) do processo {0} foram baixado(s) com sucesso.',
	sucesIncPartC: 'Parte contrária incluída com sucesso!',
	verInclPtCont: 'Deseja incluir',
	verInclPtCont2: 'como parte contrária ?',
	msgCamposObgrigat: 'Preencha os campos de nome e tipo pessoa.',
	selecioneAssJur: 'Para continuar, Selecione o Assunto juridico',
	invalidCNJ: 'O número do processo digitado está fora do padrão CNJ. Deseja continuar?',
};

const fieldsProcesso = {
	importarPeticaoInicial: 'Importar petição inicial',
	selectDistribuicao: 'Selecione uma distribuição',
	numeroProcesso: 'Número do processo',
	processoDistr: 'Processo',
	dataEntrada: 'Data de entrada',
	dataDistribuicao: 'Data de distribuição',
	areaJuridica: 'Área',
	unidade: 'Unidade',
	ccusto: 'Centro de Custo',
	escritorio: 'Escritório credenciado ',
	responsavel: 'Responsável',
	natureza: 'Natureza',
	tipoAcao: 'Tipo de ação',
	assunto: 'Assunto/Objeto',
	detalhe: 'Detalhamento',
	instancia: 'Instância',
	comarca: 'Comarca',
	foro: 'Foro',
	vara: 'Vara',
	valorCausa: 'Valor da causa',
	formaCorrecao: 'Forma de correção',
	assuntoJuridico: 'Assunto jurídico',
	tooltipDistr:
		'Utilize na pesquisa: \n O número do processo, o nome dos envolvidos ou o tribunal.',
	rito: 'Rito',
};

const fieldsEnvolvido = {
	polo: 'Polo',
	tipoEnvolvimento: 'Tipo de envolvimento',
	principal: 'Principal',
	entidade: 'Entidade',
	nomeEntidade: 'Nome',
	cgc: 'CPF/CNPJ',
	cargo: 'Cargo',
	localTrabalho: 'Local de trabalho',
	dataAdmissao: 'Data de admissão',
	dataDemissao: 'Data de demissão',
};

const placeHolder = {
	comboProcesso: 'Selecione uma distribuição',
	digiteProcesso: 'Digite o numero do processo',
	detalheProcesso: 'Detalhes do processo',
	moedaBR: 'R$',
};

const loading = {
	generico: 'Carregando...',
};

const colsEnvolvido = {
	nome: 'Nome',
	polo: 'Polo',
	tipoEnvolvimento: 'Tipo de envolvimento',
};

const breadcrumb = {
	home: 'Meus processos',
	preCadastro: 'Pré-cadastro',
};

const listInstancia = {
	primeira: '1ª Instância',
	segunda: '2ª Instância',
	tribunalSuperior: 'Tribunal superior',
};

const listPolos = {
	poloAtivo: 'Polo ativo',
	poloPassivo: 'Polo passivo',
	terceiroInteressado: 'Terceiro interessado',
	sociedade: 'Sociedade envolvida',
	participacaoSocietaria: 'Participação societária',
	administracao: 'Administração',
};

const listEntidadeOrigem = {
	cliente: 'Cliente',
	contato: 'Contato',
	fornecedor: 'Fornecedor',
	funcionario: 'Funcionário',
	parteContraria: 'Parte contrária',
};

const listSimNao = {
	sim: 'Sim',
	nao: 'Não',
};

const lists = {
	listInstancia: listInstancia,
	listPolos: listPolos,
	listEntidadeOrigem: listEntidadeOrigem,
	listSimNao: listSimNao,
};

const fieldsParteContraria = {
	nome: 'Nome',
	tipo: 'Tipo Pessoa',
	cgc: 'CNPJ/CPF',
	email: 'E-mail',
};

export const detProcessoPt = {
	title: 'Pré-cadastro de processo',
	selecionarArquivo: 'Selecionar arquivo',
	iniciarPreCad: 'Iniciar pré-cadastro',
	msgNotIniciado:
		'Para cadastrar o processo é necessário identificar os dados acima. Com a possibilidade de importar um arquivo previamente preenchido.',
	stepperEnvolvido: 'Envolvidos',
	stepperDetalhe: 'Detalhe',
	incluirEnvolvido: 'Incluir envolvido',
	alterarEnvolvido: 'Alterar Envolvido',
	incluirDetalhes: 'Incluir detalhes',
	limparForm: 'Limpar',
	voltarEnvolvido: 'Voltar para envolvidos',
	incluiCamposAdic: 'Incluir campos adicionais',
	voltarDetalhe: 'Voltar para detalhe',
	titExtraFields: 'Campos adicionais',
	partContraria: 'Nova parte contrária',
	concluir: 'Concluir',
	cancelar: 'Cancelar',
	numProcInvalido: 'Número do processo inválido',
	confirmar: 'Confirmar',
	messages: messages,
	help: helps,
	lists: lists,
	listSimNao: listSimNao,
	breadcrumb: breadcrumb,
	placeHolder: placeHolder,
	fieldsProcesso: fieldsProcesso,
	fieldsEnvolvido: fieldsEnvolvido,
	colsEnvolvido: colsEnvolvido,
	loading: loading,
	fieldsPartCon: fieldsParteContraria,
};
