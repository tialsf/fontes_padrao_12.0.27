import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetProcessoComponent } from './det-processo.component';

describe('DetProcessoComponent', () => {
	let component: DetProcessoComponent;
	let fixture: ComponentFixture<DetProcessoComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [DetProcessoComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DetProcessoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
