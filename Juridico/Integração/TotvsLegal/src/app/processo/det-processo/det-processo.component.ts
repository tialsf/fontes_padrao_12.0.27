import { Component, OnInit, ViewChild } from '@angular/core';
import { RoutingService } from 'src/app/services/routing.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DetProcesso, ItemEnvolvido, CamposParteContraria } from './det-processo';
import {
	FwmodelService,
	FwModelBody,
	FwModelBodyModels,
	FwModelBodyItems,
} from 'src/app/services/fwmodel.service';
import { LegalprocessService } from 'src/app/services/legalprocess.service';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { Router, ActivatedRoute } from '@angular/router';
import {
	adaptorFilterParam,
	FwmodelAdaptorService,
	adaptorReturnStruct,
} from 'src/app/services/fwmodel-adaptor.service';
import { protheusParam } from 'src/app/services/authlogin.service';
import {
	lpGetFuncionarioAdapterService,
	lpFuncionarioReturnStruct,
	lpGetDistribuicaoAdapterService,
	lpDistrReturnStruct,
} from 'src/app/services/legalprocess-adapter.service';
import {
	PoI18nService,
	PoBreadcrumb,
	PoTableColumn,
	PoComboComponent,
	PoNotificationService,
	PoUploadComponent,
	PoI18nPipe,
	PoModalComponent,
	PoModalAction,
	PoSelectOption,
	PoDynamicFormField,
	PoStepperComponent,
} from '@po-ui/ng-components';
import { JurconsultasService } from 'src/app/services/jurconsultas.service';
import {
	NomeEnvolvidoAdapterService,
	TpEnvolvAdapterService,
	LocalTrabAdapterService,
	AreaJuridicaAdapterService,
	UnidadeAdapterService,
	CustoAdapterService,
	EscritorioAdapterService,
	ParticipanteAdapterService,
	AssuntoAdapterService,
	TpAcaoAdapterService,
	RitoAdapterService,
	ComarcasAdapterService,
	FCorrecaoAdapterService,
	CargoAdapterService,
} from 'src/app/services/combos-fwmodel-adapter.service';

class Select {
	value: string;
	label: string;
	cpf_cnpj: string;
}

class Natureza {
	value: string;
	label: string;
	tpNatureza: string;
}
class OptionsComboSelect {
	value: string;
	label: string;
}

class GridEnvolvido {
	codEntidade: string = '';
	nome: string = '';
	polo: string = '';
	poloDescri: string = '';
	descTipoEnv: string = '';
	cpfcnpj: string = '';
	tipoPessoa: string = '';
	entidade: string = '';
	principal: string = '';
	cargo: string = '';
	localTrabalho: string = '';
	dataAdm: string = '';
	dataDem: string = '';
	codTipoEnv: string = '';
	editarEnv: string = 'editarEnvolvido';
	excluirEnv: string = 'excluirEnvolvido';
	id: number = 0;
}

class BodyValidNumPro {
	tpAssunto: string = '';
	natureza: string = '';
	numPro: string = '';
}

@Component({
	selector: 'app-det-processo',
	templateUrl: './det-processo.component.html',
	styleUrls: ['./det-processo.component.css'],
})
export class DetProcessoComponent implements OnInit {
	formProcesso: FormGroup;
	formEnvolvido: FormGroup;
	formParteCont: FormGroup;
	litDetPro: any;

	key: string = '';
	filterTpEnvo: string = '';
	titleBtnEnvol: string = '';
	FilterCodLoja: string = '';
	nomePartCont: string = '';
	codDistribuicao: string = '';
	btnSalvarDetalhe: string = '';

	isDisableSelTpAssJur: boolean = true;
	disableVara: boolean = true;
	disableForo: boolean = true;
	disabletpEnvol: boolean = true;
	disabletpAcao: boolean = true;
	lDisableCpfCnpj: boolean = true;
	isDisableLocTrabalho: boolean = true;
	lpre_cadastro: boolean = true;
	isLoadingCadastro: boolean = true;
	numpro: boolean = true;
	lIniciar: boolean = true;
	isHideLoadCmpAdic: boolean = true;
	hasExtraFields: boolean = false;
	bLoadingIncProcess: boolean = false;
	lUploadDisabled: boolean = false;
	uploadedResume: boolean = false;
	isDistribuicao: boolean = false;
	isFromDePara: boolean = false;

	nCurLinhaEnvolvido: number = -1;
	nNextIdEnvolvido: number = -1;

	oValores: object = {};
	dadosDistribuicao: any = [];
	maxDate: Date;

	listEntidade: Array<any> = [];
	listTpAcao: Array<Select> = [];
	listComarca: Array<Select> = [];
	listForo: Array<any> = [];
	listVara: Array<any> = [];
	listProcessos: Array<Select> = [];
	listNaturezas: Array<Natureza> = [];
	listTipoAssunto: Array<OptionsComboSelect> = [];
	listExtraFields: Array<PoDynamicFormField> = [];

	params: protheusParam[] = [];

	listInstancia: Array<any> = [
		{ label: '1a Instância', value: '1' },
		{ label: '2a Instância', value: '2' },
		{ label: 'Tribunal Superior', value: '3' },
	];

	listPolos: Array<any> = [
		{ label: 'Polo Ativo', value: '1' },
		{ label: 'Polo Passivo', value: '2' },
		{ label: 'Terceiro Interessado', value: '3' },
		{ label: 'Sociedade Envolvida', value: '4' },
		{ label: 'Participacao Societaria', value: '5' },
		{ label: 'Administracao', value: '6' },
	];

	ListEntOrigem: Array<any> = [
		{ label: 'Cliente', value: 'SA1' },
		{ label: 'Contato', value: 'SU5' },
		{ label: 'Fornecedor', value: 'SA2' },
		{ label: 'Funcionário', value: 'SRA' },
		{ label: 'Parte contrária', value: 'NZ2' },
	];

	listPrincipal: Array<any> = [
		{ label: 'Sim', value: '1' },
		{ label: 'Não', value: '2' },
	];

	listTpPessoa: Array<any> = [
		{ label: 'Fisica', value: '1' },
		{ label: 'Juridica', value: '2' },
	];

	listCamposDet: Array<string> = [
		'NSZ_CCLIEN',
		'NSZ_LCLIEN',
		'NSZ_TIPOAS',
		'NSZ_CAREAJ',
		'NSZ_SIGLA1',
		'NSZ_DTENTR',
		'NSZ_COBJET',
		'NSZ_DETALH',
		'NSZ_CFCORR',
		'NSZ_DTCAUS',
		'NSZ_CMOCAU',
		'NSZ_VLCAUS',
		'NSZ_CCUSTO',
		'NSZ_CRITO',
	];

	itemsEnvolv: Array<GridEnvolvido> = [];

	colEnvolvidos: Array<PoTableColumn> = [
		{ property: 'nome', width: '30%' },
		{ property: 'poloDescri', width: '30%' },
		{ property: 'descTipoEnv', width: '30%' },

		{
			property: 'editarEnv',
			label: ' ',
			type: 'icon',
			width: '5%',
			action: row => {
				this.formEnvolvido.reset();
				this.alteraEnvolvido(row);
			},
			icons: [{ value: 'editarEnvolvido', icon: 'po-icon po-icon-edit', color: '#29b6c5' }],
		},

		{
			property: 'excluirEnv',
			label: ' ',
			type: 'icon',
			width: '5%',
			action: row => this.excluiEnvolvido(row),
			icons: [{ value: 'excluirEnvolvido', icon: 'po-icon po-icon-close', color: '#29b6c5' }],
		},
	];

	fechar: PoModalAction = {
		action: () => {
			this.modalConfirmIncl.close();
		},
		label: 'Fechar',
	};

	incluir: PoModalAction = {
		action: () => {
			this.modalConfirmIncl.close();
			this.modalIncPtCont.open();
		},
		label: 'Sim',
	};
	cancelar: PoModalAction = {
		action: () => {
			this.modalIncPtCont.close();
		},
		label: 'Cancelar',
	};

	confirmar: PoModalAction = {
		action: () => {
			this.setComboNomeEntidade(this.formEnvolvido.value.entidade, '');
			this.postPartCont();
		},
		label: 'Confirmar',
	};

	continueValidCnj: PoModalAction = {
		action: () => {
			this.modalValidCNJ.close();
		},
		label: '',
	};

	cancelValidCnj: PoModalAction = {
		action: () => {
			this.resetNumPro();
		},
		label: '',
	};

	/**
	 * estrutura de navegação da URL atual
	 */
	public breadcrumb: PoBreadcrumb = { items: [] };
	@ViewChild('confirmInclPtC', { static: true }) modalConfirmIncl: PoModalComponent;
	@ViewChild('modalInclPtC', { static: true }) modalIncPtCont: PoModalComponent;
	@ViewChild('modalValidaCNJ', { static: true }) modalValidCNJ: PoModalComponent;
	@ViewChild('upload', { static: true }) upload: PoUploadComponent;
	@ViewChild('comboEnvolvido') cmbEnvolvido: PoComboComponent;
	@ViewChild('comboProcessos') cmbProcessos: PoComboComponent;
	@ViewChild('envolvidoTipo') cmbEnvolvidoTipo: PoComboComponent;
	@ViewChild('envolvidoPolo') cmbEnvolvidoPolo: PoComboComponent;
	@ViewChild('envolvidoNome') cmbEnvolvidoNome: PoComboComponent;
	@ViewChild('tipoEnvolvido') cmbTipoEnvolvido: PoComboComponent;
	@ViewChild('comboUnidade') cmbUnidade: PoComboComponent;
	@ViewChild('comboEscritorio') cmbEscritorio: PoComboComponent;
	@ViewChild('comboTipoAcao') cmbTipoAcao: PoComboComponent;

	constructor(
		protected thfI18nService: PoI18nService,
		private router: Router,
		private route: ActivatedRoute,
		private fwmodel: FwmodelService,
		private routingState: RoutingService,
		private legalprocess: LegalprocessService,
		public fwModelAdaptorService: FwmodelAdaptorService,
		public funcionarioAdapterService: lpGetFuncionarioAdapterService,
		private JuriGenerics: JurigenericsService,
		public poNotification: PoNotificationService,
		private poI18nPipe: PoI18nPipe,
		private formBuilder: FormBuilder,
		public distrAdaptorService: lpGetDistribuicaoAdapterService,
		private jurConsultas: JurconsultasService,
		public nomeEnvAdapterService: NomeEnvolvidoAdapterService,
		public tpEnvolvAdapterService: TpEnvolvAdapterService,
		public localTrabAdapterService: LocalTrabAdapterService,
		public areaJurAdapterService: AreaJuridicaAdapterService,
		public unidadeAdapterService: UnidadeAdapterService,
		public custoAdapterService: CustoAdapterService,
		public escritorioAdapterService: EscritorioAdapterService,
		public participAdapterService: ParticipanteAdapterService,
		public assuntoAdapterService: AssuntoAdapterService,
		public tpAcaoAdapterService: TpAcaoAdapterService,
		public ritoAdapterService: RitoAdapterService,
		public comarcasAdapterService: ComarcasAdapterService,
		public fCorrecaoAdapterService: FCorrecaoAdapterService,
		public cargoAdapterService: CargoAdapterService
	) {
		this.routingState.loadRouting();

		//-- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'processo' no arquivo JurTraducao.
		thfI18nService
			.getLiterals({ context: 'cadastro', language: thfI18nService.getLanguage() })
			.subscribe(litProc => {
				this.litDetPro = litProc;

				//-- Tradução do botão concluir do Step Detalhe
				this.btnSalvarDetalhe = this.litDetPro.concluir;

				//-- seta as constantes para tradução do titulo da BreadCrumb
				this.breadcrumb.items = [
					{ label: this.litDetPro.breadcrumb.home, link: '/' },
					{ label: this.litDetPro.breadcrumb.preCadastro },
				];

				//-- seta as constantes para tradução do titulo das colunas do Grid de Garantias
				this.colEnvolvidos[0].label = this.litDetPro.colsEnvolvido.nome;
				this.colEnvolvidos[1].label = this.litDetPro.colsEnvolvido.polo;
				this.colEnvolvidos[2].label = this.litDetPro.colsEnvolvido.tipoEnvolvimento;

				//-- seta as constantes para tradução do titulo dos botões da modal valida CNJ
				this.cancelValidCnj.label = this.litDetPro.cancelar;
				this.continueValidCnj.label = this.litDetPro.confirmar;
			});
	}

	ngOnInit() {
		this.createForm(new DetProcesso());
		this.createFormEnvolv(new ItemEnvolvido());
		this.createFormPtCont(new CamposParteContraria());
		this.getNatureza();
		this.getTipoAssuntoJuridico();
		this.loadExtraFields();
		this.isDisableLocTrabalho = true;
		this.lIniciar = false;
		this.key = '';
		this.maxDate = new Date();
		this.titleBtnEnvol = this.litDetPro.incluirEnvolvido;
		this.isDistribuicao = false;
	}

	/**
	 * Responsável por  inicializar os Formulários de cadastro de Processo
	 */
	createForm(processo: DetProcesso) {
		this.formProcesso = this.formBuilder.group({
			//-- Instância
			dataDistrib: [processo.dataDistrib],
			natureza: [processo.natureza, Validators.compose([Validators.required])],
			tipoAcao: [processo.tipoAcao, Validators.compose([Validators.required])],
			instancia: [processo.instancia, Validators.compose([Validators.required])],
			comarca: [processo.comarca, Validators.compose([Validators.required])],
			foro: [processo.foro, Validators.compose([Validators.required])],
			vara: [processo.vara],
			numeroPro: [processo.numeroPro, Validators.compose([Validators.required])],
			distrRec: [processo.distrRec],

			//-- Processo
			dataEntrada: [new Date()],
			unidade: [processo.unidade, Validators.compose([Validators.required])],
			area: [processo.area, Validators.compose([Validators.required])],
			escritorio: [processo.escritorio],
			responsavel: [processo.responsavel, Validators.compose([Validators.required])],
			centroCusto: [processo.centroCusto],
			assunto: [processo.assunto],
			rito: [processo.rito],
			detAssunto: [processo.detAssunto],
			dataVlCau: [processo.dataCausa],
			moedaVlCaus: [processo.moeda],
			vlrCausa: [processo.vlrCausa],
			fCorrecao: [processo.fCorrecao],
			assuntoJuri: [processo.assuntoJuri, Validators.compose([Validators.required])],
		});
	}

	createFormEnvolv(envolvido: ItemEnvolvido) {
		let row: number = -1;
		//envolvido
		this.formEnvolvido = this.formBuilder.group({
			polo: [envolvido.polo, Validators.compose([Validators.required])],
			tipoEnvol: [envolvido.tipoEnvol, Validators.compose([Validators.required])],
			principal: [envolvido.principal, Validators.compose([Validators.required])],
			entidade: [envolvido.entidade, Validators.compose([Validators.required])],
			cpfcnpj: [envolvido.cpfcnpj],
			nome: [envolvido.nome, Validators.compose([Validators.required])],
			cargo: [envolvido.cargo],
			localtrab: [envolvido.localtrab],
			dataAdm: [envolvido.dataAdm],
			dataDem: [envolvido.dataDem],
			rowId: [row],
		});
	}

	//form parte contrária
	createFormPtCont(ptContraria: CamposParteContraria) {
		this.formParteCont = this.formBuilder.group({
			nomePtContraria: [
				ptContraria.nomePtContraria,
				Validators.compose([Validators.required]),
			],
			tpPessoa: [ptContraria.tpPessoa, Validators.compose([Validators.required])],
			cpfcnpjPtCont: [ptContraria.cpfcnpjPtCont],
			email: [ptContraria.email],
		});
	}

	getNatureza() {
		this.fwmodel.restore();
		this.fwmodel.setPageSize('99');
		this.fwmodel.setModelo('JURA001');
		this.fwmodel.get('getNatureza').subscribe(data => {
			data.resources.forEach(item => {
				let naturezas = new Natureza();
				naturezas.label = this.JuriGenerics.findValueByName(
					item.models[0].fields,
					'NQ1_DESC'
				);
				naturezas.value = this.JuriGenerics.findValueByName(
					item.models[0].fields,
					'NQ1_COD'
				);
				naturezas.tpNatureza = this.JuriGenerics.findValueByName(
					item.models[0].fields,
					'NQ1_TIPO'
				);
				this.listNaturezas.push(naturezas);
			});
		});
	}

	getComarca() {
		this.fwmodel.restore();
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.setPageSize('99');
		this.fwmodel.setModelo('JURA005');
		this.fwmodel.get().subscribe(data => {
			data.resources.forEach(item => {
				let comarcas = new Select();
				comarcas.label = this.JuriGenerics.findValueByName(
					item.models[0].fields,
					'NQ6_DESC'
				);
				comarcas.value = this.JuriGenerics.findValueByName(
					item.models[0].fields,
					'NQ6_COD'
				);

				this.listComarca.push(comarcas);
			});
		});
	}

	/*
	 * Chama GET do Legal Process que retorna a lista de FOROS
	 */
	buscaListaForos(codComarca: string) {
		this.listForo = [];

		// Tratamento para alteração da Comarca
		if (
			this.JuriGenerics.changeUndefinedToEmpty(codComarca) == '' ||
			(this.formProcesso.value.foro != '' &&
				this.formProcesso.value.foro != undefined &&
				!this.isFromDePara)
		) {
			this.formProcesso.patchValue({ foro: '' });
			this.formProcesso.patchValue({ vara: '' });
			this.disableForo = true;
			this.disableVara = true;
		} else {
			this.legalprocess.restore();
			this.legalprocess
				.get('comarca/' + codComarca + '/foros', 'getForos')
				.subscribe(data => {
					if (this.JuriGenerics.changeUndefinedToEmpty(data.foro) != '') {
						data.foro.forEach(item => {
							if (item.value != '') {
								this.listForo.push(item);
							}
						});
						if (this.isFromDePara) {
							this.formProcesso.patchValue({
								foro: this.formProcesso.value.foro,
							});
						}
					}
				});
			this.disableForo = false;

			if (this.JuriGenerics.changeUndefinedToEmpty(this.formProcesso.value.foro) != '') {
				this.disableVara = false;
			}
		}
	}

	/*
	 * Chama GET do Legal Process que retorna a lista de FOROS
	 */
	buscaListaVaras(codForo: string) {
		let codComarca = this.formProcesso.value.comarca;
		this.disableVara = false;
		this.listVara = [];

		// Tratamento para alteração do Foro
		if (
			this.JuriGenerics.changeUndefinedToEmpty(codForo) == '' ||
			(this.formProcesso.value.vara != '' &&
				this.formProcesso.value.vara != undefined &&
				!this.isFromDePara)
		) {
			this.formProcesso.patchValue({ vara: '' });
			this.disableVara = true;
		} else {
			this.legalprocess.restore();
			this.legalprocess
				.get('comarca/' + codComarca + '/foros/' + codForo + '/varas', 'getVaras')
				.subscribe(data => {
					if (this.JuriGenerics.changeUndefinedToEmpty(data.vara) != '') {
						data.vara.forEach(item => {
							if (this.JuriGenerics.changeUndefinedToEmpty(item.value) != '') {
								this.listVara.push(item);
							}
						});

						if (this.isFromDePara) {
							this.formProcesso.patchValue({
								vara: this.formProcesso.value.vara,
							});
						}
					}
				});
			this.disableVara = false;
		}
	}

	/**
	 * Chama a função para filtrar o tipo de ação de acordo com a natureza selecionada
	 */
	chooseNat(natureza: string) {
		let origemNatureza: string = '';
		let filterQry: string = '';

		this.disabletpAcao = true;

		this.formProcesso.patchValue({ tipoAcao: '' });

		if (this.JuriGenerics.changeUndefinedToEmpty(natureza) != '') {
			this.disabletpAcao = false;

			origemNatureza = this.listNaturezas.find(x => x.value == natureza).tpNatureza;
			filterQry = "NQU_ORIGEM = '" + origemNatureza + "'";

			if (this.formProcesso.value.numeroPro != '') {
				this.validaNumPro();
			}
		}

		this.tpAcaoAdapterService.setFiltroP(filterQry);
	}

	/**
	 * Seta os campos de data e moeda da causa, caso o campo de valor esteja preenchido
	 */
	setCamposVlCausa(valorCausa: string) {
		if (valorCausa != '' && valorCausa != undefined) {
			let dataDistr = this.formProcesso.value.dataDistrib;
			let dataVlCausa = this.formProcesso.value.dataEntrada;
			this.getMoeda(); //seta a moeda de acordo com o parametro MV_JCMOPRO
			if (this.JuriGenerics.changeUndefinedToEmpty(dataDistr) != '') {
				dataVlCausa = dataDistr;
			}

			this.formProcesso.patchValue({
				dataVlCau: dataVlCausa,
			});
		} else {
			this.formProcesso.patchValue({
				dataVlCau: '',
				moedaVlCaus: '',
			});
		}
	}

	/**
	 * Busca a moeda cadastrada no parametro MV_JCMOPRO
	 */
	getMoeda() {
		this.legalprocess.restore();
		this.legalprocess.get('tlprocess/sysParam/MV_JCMOPRO').subscribe(data => {
			let param = new protheusParam();
			param.id = data.sysParam.name;
			param.value = data.sysParam.value;
			this.params.push(param);

			this.formProcesso.patchValue({
				moedaVlCaus: this.JuriGenerics.findValueByName(this.params, 'MV_JCMOPRO'),
			});
		});
	}

	/**
	 * Trata os dados recebidos da Leitura de Iniciais e inclui no Grid de Envolvidos
	 */
	retornoJson(response) {
		let envolvidos = response.body.data[0].envolvidos;
		let auxCount: number = 0;
		let qtdCaract: string = '';

		this.lpre_cadastro = false;
		this.numpro = true;
		this.isLoadingCadastro = false;

		if (this.itemsEnvolv.length > 0) {
			auxCount = this.itemsEnvolv.length;
			this.lUploadDisabled = true;
			this.lpre_cadastro = true;
		} else {
			this.formProcesso.patchValue({
				numeroPro: response.body.data[0].numprocesso,
			});
			this.validaNumPro();
			this.itemsEnvolv = [];
			this.lUploadDisabled = false;
			this.lpre_cadastro = false;
		}

		if (envolvidos != undefined) {
			for (let count = 0; count < envolvidos.length; count++) {
				let envolvido: GridEnvolvido = new GridEnvolvido();

				envolvido.nome = envolvidos[count].NT9_NOMEEN;
				envolvido.codEntidade = envolvidos[count].NT9_CODENT;
				envolvido.polo = envolvidos[count].NT9_TIPOEN;
				envolvido.poloDescri = this.listPolos.find(
					x => x.value == envolvidos[count].NT9_TIPOEN
				).label;
				envolvido.codTipoEnv = envolvidos[count].NT9_CTPENV;
				envolvido.descTipoEnv = envolvidos[count].NT9_DESCEN;
				envolvido.entidade = envolvidos[count].NT9_ENTIDA;
				envolvido.principal = envolvidos[count].NT9_PRINCI;
				envolvido.cpfcnpj = envolvidos[count].NT9_DOCENV;

				//-- Verifica o tipo de pessoa - 1=Física / 2=Jurídica
				if (
					envolvidos[count].NT9_DOCENVC != '' &&
					envolvidos[count].NT9_DOCENV != undefined
				) {
					qtdCaract = envolvidos[count].NT9_DOCENV.trim();

					//-- Conta quantidade de caracteres para verificar se é CPF ou CNPJ
					if (qtdCaract.length <= 11) {
						envolvido.tipoPessoa = '1';
					} else {
						envolvido.tipoPessoa = '2';
					}
				}

				envolvido.id = count;

				if (envolvido.polo == '2' && envolvido.principal == '1') {
					this.formProcesso.value.unidade = envolvido.codEntidade;
				}

				this.itemsEnvolv.push(envolvido);
			}
		}

		this.upload.clear();
		this.lIniciar = true;
		this.upload.disabled = true;
		this.lUploadDisabled = true;
		this.lpre_cadastro = true;

		this.isLoadingCadastro = true;
	}

	/**
	 * Chama a função para seleção de arquivo para importação de petição inicial
	 */
	selectFile() {
		this.listProcessos = [];
		if (this.isDisableSelTpAssJur) {
			this.lUploadDisabled = false;
			this.poNotification.error(this.litDetPro.messages.selecioneAssJur);
		} else {
			this.upload.selectFiles();
		}
	}

	/**
	 * Redireciona para a página Home caso o usuário selecione o botão de "Cancelar"
	 */
	callHome() {
		this.router.navigate(['/'], { relativeTo: this.route });
	}

	/**
	 * Desabilita o botão primário "Iniciar pré-cadastro"
	 */
	preCadastro() {
		if (this.isDisableSelTpAssJur) {
			this.poNotification.error(this.litDetPro.messages.selecioneAssJur);
		} else {
			this.lpre_cadastro = true;
			this.lIniciar = true;
		}
	}

	/**
	 * Busca os dados da distribuição selecionada pelo usuário e seta as informações nos Forms
	 */
	getDadosNZZ(numProcesso: string) {
		let dtDistr: string = '';
		let qtdCaract: string = '';

		this.dadosDistribuicao = [];
		this.itemsEnvolv = [];
		this.isLoadingCadastro = false;

		if (numProcesso != undefined) {
			//Verifica se selecionou um assunto juridico

			if (this.dadosDistribuicao[0] == undefined) {
				this.legalprocess.restore();
				//-- GET para buscar infos da NZZ
				this.legalprocess.get('distr/cod/' + numProcesso).subscribe(data => {
					this.dadosDistribuicao.push(data[0]);

					if (this.dadosDistribuicao[0] != undefined) {
						//guarda o código da distribuição para realizar a atualização do status
						this.codDistribuicao = numProcesso;

						//-- Comarca, Unidade e numero do processo
						this.formProcesso.patchValue({
							unidade:
								this.dadosDistribuicao[0].NSZ_CCLIEN +
								'-' +
								this.dadosDistribuicao[0].NSZ_LCLIEN,
							numeroPro: (<lpDistrReturnStruct>this.cmbProcessos.selectedOption)
								.numpro,
						});
						this.validaNumPro();

						//-- Tipo de Ação
						if (this.dadosDistribuicao[0].NUQ_CTIPAC != '') {
							this.formProcesso.patchValue({ tipoAcao: '' });
						}

						dtDistr = this.JuriGenerics.makeDate(
							this.dadosDistribuicao[0].NUQ_DTDIST,
							'yyyy-mm-dd'
						);
						//-- Data de Entrada e Data de Distribuição
						this.formProcesso.patchValue({
							dataDistrib: dtDistr,
							dataEntrada: new Date(),
						});

						//-- Seta os Dados no Grid de Envolvidos - NT9
						for (let nX = 0; nX < this.dadosDistribuicao[0].Envolvidos.length; nX++) {
							let envolvido: GridEnvolvido = new GridEnvolvido();

							(envolvido.nome = this.dadosDistribuicao[0].Envolvidos[nX].NT9_NOMEEN),
								(envolvido.codEntidade = this.dadosDistribuicao[0].Envolvidos[
									nX
								].NT9_CODENT);
							envolvido.polo = this.dadosDistribuicao[0].Envolvidos[nX].NT9_TIPOEN;
							envolvido.poloDescri = this.listPolos.find(
								x => x.value == this.dadosDistribuicao[0].Envolvidos[nX].NT9_TIPOEN
							).label;
							envolvido.codTipoEnv = this.dadosDistribuicao[0].Envolvidos[
								nX
							].NT9_CTPENV;
							envolvido.descTipoEnv = this.dadosDistribuicao[0].Envolvidos[
								nX
							].NT9_DESCEN;
							envolvido.entidade = this.dadosDistribuicao[0].Envolvidos[
								nX
							].NT9_ENTIDA;
							envolvido.principal = this.dadosDistribuicao[0].Envolvidos[
								nX
							].NT9_PRINCI;
							envolvido.cpfcnpj = this.dadosDistribuicao[0].Envolvidos[nX].NT9_CGC;
							envolvido.id = nX;

							//-- Verifica o tipo de pessoa - 1=Física / 2=Jurídica
							if (
								this.dadosDistribuicao[0].Envolvidos[nX].NT9_CGC != '' &&
								this.dadosDistribuicao[0].Envolvidos[nX].NT9_CGC != undefined
							) {
								qtdCaract = this.dadosDistribuicao[0].Envolvidos[nX].NT9_CGC.trim();

								//-- Conta quantidade de caracteres para verificar se é CPF ou CNPJ
								if (qtdCaract.length <= 11) {
									envolvido.tipoPessoa = '1';
								} else {
									envolvido.tipoPessoa = '2';
								}
							}

							this.itemsEnvolv.push(envolvido);
							this.nNextIdEnvolvido = nX + 1;
						}
						this.lIniciar = true;
						this.lpre_cadastro = true;
						this.isLoadingCadastro = true;
						this.lUploadDisabled = true;
						this.isDistribuicao = true;
					}
				});
			}
		} else {
			this.isLoadingCadastro = true;
			this.lpre_cadastro = false;
			this.lUploadDisabled = false;
			this.isDistribuicao = false;

			this.formEnvolvido.reset();
			this.formProcesso.reset();
		}
	}

	clearEnvolvidos() {
		this.formEnvolvido.reset();
		this.titleBtnEnvol = this.litDetPro.incluirEnvolvido;
		this.createFormEnvolv(new ItemEnvolvido());
	}

	/**
	 * Função ativada ao selecionar o polo, definindo qual será o filtro
	 * para o tipo de envolvimento
	 */
	filterTpEnvol(polo: string) {
		this.disabletpEnvol = polo == undefined;

		if (this.JuriGenerics.changeUndefinedToEmpty(polo) == '') {
			this.formEnvolvido.patchValue({ tipoEnvol: '' });
		}

		polo = this.makeFilterTpEnvol(polo);
		this.tpEnvolvAdapterService.setFiltroP(polo);
	}

	makeFilterTpEnvol(polo: string) {
		//let polo: string = this.formEnvolvido.value.polo;
		let filter: string = '';

		if (this.JuriGenerics.changeUndefinedToEmpty(polo) != '') {
			switch (polo) {
				case '1':
					filter = "NQA_POLOAT = '1'";
					break;
				case '2':
					filter = "NQA_POLOPA = '1'";
					break;
				case '3':
					filter = "NQA_TERCIN = '1'";
					break;
				case '4':
					filter = "NQA_SOCIED = '1'";
					break;
				case '5':
					filter = "NQA_PARTIC = '1'";
					break;
				case '6':
					filter = "NQA_ADMINI = '1'";
					break;
			}
		}

		return filter;
	}

	addEnvolv() {
		let envolvido: GridEnvolvido = new GridEnvolvido();
		let qtdCaract: string = '';
		envolvido.tipoPessoa = '1';

		if (this.formEnvolvido.valid) {
			envolvido.codEntidade = this.formEnvolvido.value.nome;
			envolvido.cpfcnpj = this.formEnvolvido.value.cpfcnpj;
			envolvido.entidade = this.formEnvolvido.value.entidade;
			envolvido.principal = this.formEnvolvido.value.principal;
			envolvido.cargo = this.formEnvolvido.value.cargo;
			envolvido.localTrabalho = this.formEnvolvido.value.localtrab;
			envolvido.dataAdm = this.formEnvolvido.value.dataAdm;
			envolvido.dataDem = this.formEnvolvido.value.dataDem;
			envolvido.codTipoEnv = this.formEnvolvido.value.tipoEnvol;

			envolvido.polo = this.cmbEnvolvidoPolo.selectedOption.value.toString();
			envolvido.poloDescri = this.cmbEnvolvidoPolo.selectedOption.label;
			envolvido.descTipoEnv = this.cmbEnvolvidoTipo.selectedOption.label;
			envolvido.nome = this.cmbEnvolvidoNome.selectedOption.label;

			if (this.formEnvolvido.value.rowId >= 0) {
				let indexReg = this.itemsEnvolv.findIndex(
					x => x.id == this.formEnvolvido.value.rowId
				);
				this.itemsEnvolv[indexReg] = envolvido;
			} else {
				this.nNextIdEnvolvido++;
				envolvido.id = this.nNextIdEnvolvido;

				this.itemsEnvolv.push(envolvido);
			}

			//-- Verifica o tipo de pessoa - 1=Física / 2=Jurídica
			if (
				this.formEnvolvido.value.cpfcnpj != '' &&
				this.formEnvolvido.value.cpfcnpj != undefined
			) {
				qtdCaract = this.formEnvolvido.value.cpfcnpj.trim();

				//-- Conta quantidade de caracteres para verificar se é CPF ou CNPJ
				if (qtdCaract.length <= 11) {
					envolvido.tipoPessoa = '1';
				} else {
					envolvido.tipoPessoa = '2';
				}
			}
			if (this.titleBtnEnvol == this.litDetPro.incluirEnvolvido) {
				this.poNotification.success(this.litDetPro.messages.sucAddEnvolvido);
			} else {
				this.poNotification.success(this.litDetPro.messages.sucAltEnvolvido);
			}
			//-- Limpa o Form após incluir os dados no GRID
			this.clearEnvolvidos();
		} else {
			this.poNotification.error(this.litDetPro.messages.errAddEnvolvido);
		}
	}

	alteraEnvolvido(row: GridEnvolvido) {
		this.isLoadingCadastro = false;
		this.tpEnvolvAdapterService.setFiltroP(this.makeFilterTpEnvol(row.polo));
		this.setComboNomeEntidade(row.entidade, row.codEntidade);

		this.formEnvolvido.patchValue({
			polo: row.polo,
			principal: row.principal,
			cargo: row.cargo,
			localtrab: row.localTrabalho,
			dataAdm: row.dataAdm,
			dataDem: row.dataDem,
			entidade: row.entidade,
			rowId: row.id,
			tipoEnvol: row.codTipoEnv,
			cpfcnpj: row.cpfcnpj,
			codTipoEnv: row.codTipoEnv,
		});
		this.titleBtnEnvol = this.litDetPro.alterarEnvolvido;
		this.isLoadingCadastro = true;
	}

	excluiEnvolvido(row: any) {
		for (var i = 0; i < this.itemsEnvolv.length; i++) {
			if (this.itemsEnvolv[i].id == row.id) {
				this.itemsEnvolv.splice(i, 1);
			}
		}
		this.clearEnvolvidos();
		this.poNotification.success(this.litDetPro.messages.sucExcEnvolvido);
	}

	/**
	 * Trás o filtro de acordo com a entidade origem selecionada
	 * (SA1 / SA2/ SU5/ NZ2/ SRA)
	 * @param entidade
	 * @param codEntidade
	 */
	setComboNomeEntidade(entidade: string = '', nomeEnvolvido?: string) {
		if (entidade == '') {
			this.formEnvolvido.patchValue({
				nome: '',
				cpfcnpj: '',
				localtrab: '',
			});
		} else {
			switch (entidade) {
				case 'SA1':
					this.nomeEnvAdapterService.setup(
						new adaptorFilterParam(
							'JCLIDEP',
							'',
							'A1_NOME,A1_CGC',
							'A1_NOME,A1_COD,A1_LOJA',
							'A1_NOME [A1_COD/A1_LOJA]',
							'A1_COD,A1_LOJA',
							'A1_CODA1_LOJA',
							'getClienteEnvolvido',
							undefined,
							undefined,
							undefined,
							{
								bGetAllLevels: false,
								bUseEmptyField: false,
								bUseVirtualField: false,
								arrSetFields: ['A1_COD', 'A1_LOJA', 'A1_CGC', 'A1_NOME'],
							},
							'A1_CGC'
						)
					);
					break;
				case 'SA2':
					this.nomeEnvAdapterService.setup(
						new adaptorFilterParam(
							'JURA132',
							'',
							'A2_NOME,A2_CGC',
							'A2_NOME,A2_COD,A2_LOJA',
							'A2_NOME [A2_COD/A2_LOJA]',
							'A2_COD,A2_LOJA',
							'A2_CODA2_LOJA',
							'getFornecedorEnvolvido',
							undefined,
							undefined,
							undefined,
							{
								bGetAllLevels: false,
								bUseVirtualField: true,
								bUseEmptyField: false,
								arrSetFields: [],
							},
							'A2_CGC'
						)
					);
					break;
				case 'SU5':
					this.nomeEnvAdapterService.setup(
						new adaptorFilterParam(
							'JURA232',
							'',
							'U5_CONTAT,U5_CPF',
							'U5_CONTAT',
							'',
							'U5_CODCONT',
							'',
							'getContatoEnvolvido',
							undefined,
							undefined,
							undefined,
							{
								bGetAllLevels: false,
								bUseVirtualField: true,
								bUseEmptyField: false,
								arrSetFields: [],
							},
							'U5_CPF'
						)
					);
					break;
				case 'NZ2':
					this.nomeEnvAdapterService.setup(
						new adaptorFilterParam(
							'JURA184',
							'',
							'NZ2_NOME,NZ2_CGC',
							'NZ2_NOME',
							'',
							'NZ2_COD',
							'',
							'getParteContrariaEnvolvido',
							undefined,
							undefined,
							undefined,
							{
								bGetAllLevels: false,
								bUseVirtualField: true,
								bUseEmptyField: false,
								arrSetFields: [],
							},
							'NZ2_CGC'
						)
					);
					break;
			}
			this.formEnvolvido.patchValue({
				nome: nomeEnvolvido,
			});
		}
	}

	/**
	 * Preenche e Habilita o campo  CPF / CNPJ após selecionar a entidade
	 */
	incluiCpfCnpj() {
		var codCGC: string = '';
		if (this.cmbTipoEnvolvido.selectedOption != undefined) {
			let tipoEntidade = this.cmbTipoEnvolvido.selectedOption.value;

			if (tipoEntidade == 'SRA') {
				if (this.cmbEnvolvidoNome.selectedOption != undefined) {
					codCGC = (<lpFuncionarioReturnStruct>this.cmbEnvolvidoNome.selectedOption).cgc;
				}
			} else {
				if (this.cmbEnvolvidoNome.selectedOption != undefined) {
					let fieldsExtra: adaptorReturnStruct = <adaptorReturnStruct>(
						this.cmbEnvolvidoNome.selectedOption
					);

					switch (tipoEntidade) {
						case 'SA1':
							codCGC = this.JuriGenerics.findValueByName(
								fieldsExtra.getExtras(),
								'A1_CGC'
							);
							break;
						case 'SA2':
							codCGC = this.JuriGenerics.findValueByName(
								fieldsExtra.getExtras(),
								'A1_CGC'
							);
							break;
						case 'SU5':
							codCGC = this.JuriGenerics.findValueByName(
								fieldsExtra.getExtras(),
								'U5_CPF'
							);
							break;
						case 'NZ2':
							codCGC = this.JuriGenerics.findValueByName(
								fieldsExtra.getExtras(),
								'NZ2_CGC'
							);
							break;
					}
				}
			}
			//-- Filtra o Local de Trabalho de acordo com Cliente e Loja selecionado pelo usuário
			if (this.cmbEnvolvidoNome.selectedOption != undefined) {
				let codCliLj: string = '';
				if (
					this.formEnvolvido.value.entidade === 'SA1' &&
					this.JuriGenerics.changeUndefinedToEmpty(
						this.cmbEnvolvidoNome.selectedOption.value.toString()
					) != ''
				) {
					codCliLj =
						"'" +
						this.JuriGenerics.changeUndefinedToEmpty(
							this.cmbEnvolvidoNome.selectedOption.value.toString()
						) +
						"'";
				}
				this.itemsEnvolv.forEach(items => {
					if (items.entidade === 'SA1' && items.polo === '2') {
						if (codCliLj != '') {
							codCliLj += ", '" + items.codEntidade + "'";
						} else {
							codCliLj += "'" + items.codEntidade + "'";
						}
					}
				});

				if (codCliLj != undefined) {
					if (codCliLj === '') {
						codCliLj = "''";
					}
					this.FilterCodLoja = 'NTB_CCLIEN||NTB_LOJACL IN (' + codCliLj + ')';
					this.localTrabAdapterService.setFiltroP(this.FilterCodLoja);
					this.isDisableLocTrabalho = false;
				}
			} else {
				this.isDisableLocTrabalho = true;
				this.formEnvolvido.patchValue({ localtrab: '' });
			}
		}

		//-- Ao alterar um envolvido, verifica se o usuário havia digitado um cpf manualmente, senão, seta o cpf do cadastro da entidade.

		if (codCGC != '' && codCGC != undefined) {
			this.formEnvolvido.patchValue({
				cpfcnpj: codCGC,
			});
			this.lDisableCpfCnpj = true;
		}
		this.lDisableCpfCnpj = false;
	}

	/**
	 * Chama o método POST para realizar a gravação do processo.
	 */
	submitProcess(stepper: PoStepperComponent) {
		this.isLoadingCadastro = false;
		this.bLoadingIncProcess = true;
		let processo = this.formProcesso.value;
		let arrEnvolvido = this.itemsEnvolv;

		let bRet = this.formProcesso.valid;
		if (bRet) {
			bRet = this.beforeSubmit(processo, arrEnvolvido);
			if (bRet) {
				if (this.hasExtraFields && stepper != undefined) {
					stepper.next();
					this.isLoadingCadastro = true;
					this.bLoadingIncProcess = false;
				} else {
					this.fwmodel.restore();
					this.fwmodel.setModelo('JURA095');
					this.fwmodel.post(this.setBodyProcess(), 'postProcess').subscribe(data => {
						window.localStorage.setItem('assuntoJuri', processo.assuntoJuri); //seta o localStorage para assunto juridico
						if (data != '') {
							var codPro = this.JuriGenerics.findValueByName(
								data.models[0].fields,
								'NSZ_COD'
							);
							var filPro = btoa(window.localStorage.getItem('filial'));
							this.itemsEnvolv = [];
							this.poNotification.success(this.litDetPro.messages.sucGravaProcesso);
							this.isLoadingCadastro = true;
							this.bLoadingIncProcess = false;

							this.router.navigate(['./processo/' + filPro + '/' + btoa(codPro)]);
							if (this.isDistribuicao) {
								//verifica se foi carregado por distribuição
								this.updateStatusDist(codPro);
							}
						} else {
							this.isLoadingCadastro = true;
							this.bLoadingIncProcess = false;
						}
						this.bLoadingIncProcess = false;
					});
				}
			}
		} else {
			this.poNotification.error(this.litDetPro.messages.errCamposObrigProcesso);
			this.isLoadingCadastro = true;
			this.bLoadingIncProcess = false;
		}
	}
	/**
	 * Responsável por fazer validações antes de comitar o processo
	 * @param processo form do processo
	 * @param arrEnvolvido array com os envolvidos cadastrados no processo
	 */
	beforeSubmit(processo: DetProcesso, arrEnvolvido: GridEnvolvido[]): boolean {
		let concatPolosIrregulares: string = '';
		let qtdPoloAtivo: number = 0;
		let qtdPoloPassivo: number = 0;
		let qtdTerceiroInteressado: number = 0;
		let qtdSociedadeEnvolvida: number = 0;
		let qtdParticipacaoSociet: number = 0;
		let qtdAdministracao: number = 0;
		let bRet: boolean = true;

		//-- Validação para Polos
		this.listPolos.forEach(polo => {
			if (arrEnvolvido.findIndex(x => x.polo == polo.value && x.principal == '1') > -1) {
				bRet = true;
			} else {
				if (arrEnvolvido.findIndex(x => x.polo == polo.value && x.principal == '2') > -1) {
					bRet = false;
				} else {
					bRet = true;
				}

				if (!bRet) {
					concatPolosIrregulares += polo.label + ',';
				}
			}
		});

		if (concatPolosIrregulares.length > 0) {
			bRet = false;
			this.poNotification.error(
				this.poI18nPipe.transform(this.litDetPro.messages.errEnvolvidosSemPrincipal, [
					concatPolosIrregulares.substring(0, concatPolosIrregulares.length - 1),
				])
			);
			this.isLoadingCadastro = true;
			this.bLoadingIncProcess = false;
		} else {
			concatPolosIrregulares = '';
			arrEnvolvido.forEach(envolvido => {
				if (envolvido.principal == '1') {
					switch (envolvido.polo) {
						case '1':
							qtdPoloAtivo++;
							if (qtdPoloAtivo == 2) {
								concatPolosIrregulares +=
									this.listPolos.find(x => x.value == '1').label + ',';
							}
							break;
						case '2':
							qtdPoloPassivo++;
							if (qtdPoloPassivo == 2) {
								concatPolosIrregulares +=
									this.listPolos.find(x => x.value == '2').label + ',';
							}
							break;
						case '3':
							qtdTerceiroInteressado++;
							if (qtdTerceiroInteressado == 2) {
								concatPolosIrregulares +=
									this.listPolos.find(x => x.value == '3').label + ',';
							}
							break;
						case '4':
							qtdSociedadeEnvolvida++;
							if (qtdSociedadeEnvolvida == 2) {
								concatPolosIrregulares +=
									this.listPolos.find(x => x.value == '4').label + ',';
							}
							break;
						case '5':
							qtdParticipacaoSociet++;
							if (qtdParticipacaoSociet == 2) {
								concatPolosIrregulares +=
									this.listPolos.find(x => x.value == '5').label + ',';
							}
							break;
						case '6':
							qtdAdministracao++;
							if (qtdAdministracao == 2) {
								concatPolosIrregulares +=
									this.listPolos.find(x => x.value == '6').label + ',';
							}
							break;
						default:
							break;
					}
				}
			});
			if (concatPolosIrregulares.length > 0) {
				this.poNotification.error(
					this.poI18nPipe.transform(this.litDetPro.messages.errEnvolvidosMaisPrincipal, [
						concatPolosIrregulares.substring(0, concatPolosIrregulares.length - 1),
					])
				);
				bRet = false;
				this.isLoadingCadastro = true;
				this.bLoadingIncProcess = false;
			}
		}
		return bRet;
	}

	/**
	 * Monta o body com os dados que serão gravados na inclusão do processo
	 */
	setBodyProcess() {
		let fwModelBody: FwModelBody = new FwModelBody('JURA095', 3, '');
		let gridInstancias: FwModelBodyModels;
		let itemInstancias: FwModelBodyItems;
		let gridEnvolvidos: FwModelBodyModels;
		let itemEnvolvidos: FwModelBodyItems;
		let envolvidos = this.itemsEnvolv;
		let formProcesso = this.formProcesso.value;

		//-- Processos - NSZ
		fwModelBody.addModel('NSZMASTER', 'FIELDS', 1);
		fwModelBody.models[0].addField(
			'NSZ_CCLIEN',
			this.JuriGenerics.getValueByOption(this.cmbUnidade, 'A1_COD'),
			0,
			'C'
		);
		fwModelBody.models[0].addField(
			'NSZ_LCLIEN',
			this.JuriGenerics.getValueByOption(this.cmbUnidade, 'A1_LOJA'),
			0,
			'C'
		);
		fwModelBody.models[0].addField('NSZ_TIPOAS', formProcesso.assuntoJuri, 0, 'C');
		fwModelBody.models[0].addField('NSZ_CAREAJ', formProcesso.area, 0, 'C');
		fwModelBody.models[0].addField('NSZ_SIGLA1', formProcesso.responsavel, 0, 'C');
		fwModelBody.models[0].addField('NSZ_DTENTR', formProcesso.dataEntrada, 0, 'D');
		fwModelBody.models[0].addField('NSZ_COBJET', formProcesso.assunto, 0, 'C');
		fwModelBody.models[0].addField('NSZ_DETALH', formProcesso.detAssunto, 0, 'C');
		fwModelBody.models[0].addField('NSZ_CFCORR', formProcesso.fCorrecao, 0, 'C');
		fwModelBody.models[0].addField('NSZ_DTCAUS', formProcesso.dataVlCau, 0, 'D');
		fwModelBody.models[0].addField('NSZ_CMOCAU', formProcesso.moedaVlCaus, 0, 'C');
		fwModelBody.models[0].addField('NSZ_VLCAUS', formProcesso.vlrCausa, 0, 'C');
		fwModelBody.models[0].addField('NSZ_CCUSTO', formProcesso.centroCusto, 0, 'C');
		fwModelBody.models[0].addField('NSZ_CRITO', formProcesso.rito, 0, 'C');

		//Adiciona os campos extras no body
		for (var campos in this.oValores) {
			let nPosCampoExt: number = this.listExtraFields.findIndex(x => x.property == campos);
			if (nPosCampoExt > -1) {
				let cTypeForm: string = this.listExtraFields[nPosCampoExt].type;
				let cTypeBody: 'C' | 'N' | 'D' | 'L' = 'C';
				let value: string | number = this.oValores[campos];

				switch (cTypeForm) {
					case 'date':
						cTypeBody = 'D';
						break;
					case 'boolean':
						if (value) {
							value = 'T';
						} else {
							value = 'F';
						}
						break;
				}

				fwModelBody.models[0].addField(campos, value, 0, cTypeBody);
			}
		}

		//-- Instâncias - NUQ
		gridInstancias = fwModelBody.models[0].addModel('NUQDETAIL', 'GRID', 0);
		itemInstancias = gridInstancias.addItems();
		itemInstancias.addField('NUQ_INSTAN', formProcesso.instancia, 'C');
		itemInstancias.addField('NUQ_INSATU', '1', 'C');
		itemInstancias.addField('NUQ_CNATUR', formProcesso.natureza, 'C');
		itemInstancias.addField(
			'NUQ_NUMPRO',
			formProcesso.numeroPro.replace(/[\u0026]/g, '&amp;'),
			'C'
		);
		itemInstancias.addField('NUQ_CTIPAC', formProcesso.tipoAcao, 'C');
		itemInstancias.addField('NUQ_CCOMAR', formProcesso.comarca, 'C');
		itemInstancias.addField('NUQ_CLOC2N', formProcesso.foro, 'C');
		itemInstancias.addField('NUQ_CLOC3N', formProcesso.vara, 'C');
		itemInstancias.addField(
			'NUQ_CCORRE',
			this.JuriGenerics.getValueByOption(this.cmbEscritorio, 'A2_COD'),
			'C'
		);
		itemInstancias.addField(
			'NUQ_LCORRE',
			this.JuriGenerics.getValueByOption(this.cmbEscritorio, 'A2_LOJA'),
			'C'
		);
		itemInstancias.addField('NUQ_DTDIST', formProcesso.dataDistrib, 'D');

		//-- Envolvidos - NT9
		gridEnvolvidos = fwModelBody.models[0].addModel('NT9DETAIL', 'GRID', 0);

		for (let count = 0; count < envolvidos.length; count++) {
			itemEnvolvidos = gridEnvolvidos.addItems(count + 1, 0);
			itemEnvolvidos.addField('NT9_ENTIDA', envolvidos[count].entidade, 'C');
			itemEnvolvidos.addField('NT9_CODENT', envolvidos[count].codEntidade, 'C');
			itemEnvolvidos.addField('NT9_PRINCI', envolvidos[count].principal, 'C');
			itemEnvolvidos.addField('NT9_TIPOEN', envolvidos[count].polo, 'C');
			itemEnvolvidos.addField('NT9_CTPENV', envolvidos[count].codTipoEnv, 'C');
			itemEnvolvidos.addField('NT9_TIPOP', envolvidos[count].tipoPessoa, 'C');

			//- Valida CPF / CNPJ e Tipo de Pessoa
			if (envolvidos[count].cpfcnpj != '' && envolvidos[count].cpfcnpj != undefined) {
				itemEnvolvidos.addField('NT9_CGC', envolvidos[count].cpfcnpj, 'C');
			}
			//-- Se há Local de Trabalho, deve preencher os campos de Cliente (NT9_CEMPCL) e Loja Cliente (NT9_LOJACL)
			if (
				envolvidos[count].localTrabalho != '' &&
				envolvidos[count].localTrabalho != undefined
			) {
				itemEnvolvidos.addField('NT9_CLOCTR', envolvidos[count].localTrabalho, 'C');
				itemEnvolvidos.addField(
					'NT9_CEMPCL',
					this.JuriGenerics.changeUndefinedToEmpty(
						envolvidos[count].codEntidade
					).substring(
						0,
						this.JuriGenerics.changeUndefinedToEmpty(
							envolvidos[count].codEntidade
						).indexOf('-')
					),
					'C'
				);
				itemEnvolvidos.addField(
					'NT9_LOJACL',
					this.JuriGenerics.changeUndefinedToEmpty(
						envolvidos[count].codEntidade
					).substring(
						this.JuriGenerics.changeUndefinedToEmpty(
							envolvidos[count].codEntidade
						).indexOf('-') + 1
					),
					'C'
				);
				itemEnvolvidos.addField('NT9_CLOCTR', envolvidos[count].localTrabalho, 'C');
			}
			itemEnvolvidos.addField('NT9_CCRGDP', envolvidos[count].cargo, 'C');
			itemEnvolvidos.addField('NT9_DTADM', envolvidos[count].dataAdm, 'D');
			itemEnvolvidos.addField('NT9_DTDEMI', envolvidos[count].dataDem, 'D');
		}

		return fwModelBody.serialize();
	}

	/**
	 * atualiza o status da distibuição, atribui o cajuri e faz a baixa dos documentos da distribuição
	 * @param cajuri código do processo criado
	 */
	updateStatusDist(cajuri: string) {
		this.legalprocess.restore();
		this.legalprocess
			.put('distr/cod/' + this.codDistribuicao + '/proc/' + cajuri, '', 'updateStatusDist')
			.subscribe(data => {
				if (data == '' || data == undefined) {
					this.poNotification.error(this.litDetPro.messages.errUpdDistribuicao);
				} else if (data.doc === true) {
					//se há documentos para baixar
					if (data.messages === '') {
						let sucBaixaMsg = this.poI18nPipe.transform(
							this.litDetPro.messages.sucBaixaDocs,
							[cajuri]
						);
						this.poNotification.success(sucBaixaMsg);
					} else if (data.messages != '') {
						this.poNotification.error(
							this.litDetPro.messages.errBaixaDocs + data.messages.toString()
						);
					}
				}
			});
	}

	/**
	 * Verifica se a entidade selecionada é de parte contrária, e verifica se o nome digitado está presente no retorno
	 * do combo, se não tiver, é apresentado um modal para inclusão, verificação é acionada assim que se sai do campo
	 * com TAB ou ENTER
	 * @param event
	 */
	vldParteCont(event) {
		if (event.keyCode == 9 || event.keyCode == 13) {
			if (this.cmbTipoEnvolvido.selectedOption != undefined) {
				if (this.cmbTipoEnvolvido.selectedOption.value == 'NZ2') {
					//verifica que a entidade selecionada é parte contrária
					if (
						this.cmbEnvolvidoNome.selectedOption == undefined ||
						this.cmbEnvolvidoNome.selectedOption.label !=
							this.cmbEnvolvidoNome.previousSearchValue
					) {
						//verifica se o nome não esta presente no combo
						this.nomePartCont = this.cmbEnvolvidoNome.previousSearchValue; //informação digitada no nome
						this.modalConfirmIncl.open();
						//carrega o campo nome do cadastro de parte contrária, com o valor digitado no envolvido
						this.formParteCont.patchValue({
							nomePtContraria: this.nomePartCont,
						});
					}
				}
			}
		}
	}

	/**
	 * Responsável por efetuar a inclusão da parte contrária com as informações setadas no bodyPutPartCon
	 */
	postPartCont() {
		if (this.beforeSubPtC()) {
			let body: string = this.bodyPutPartCon(this.formParteCont.value, '');
			this.fwmodel.restore();
			this.fwmodel.setModelo('JURA184');
			this.fwmodel.post(body, 'postParContraria').subscribe(data => {
				if (this.JuriGenerics.changeUndefinedToEmpty(data) != '') {
					var codNZ2 = this.JuriGenerics.findValueByName(
						data.models[0].fields,
						'NZ2_COD'
					);
					var cpfcnpj = this.JuriGenerics.findValueByName(
						data.models[0].fields,
						'NZ2_CGC'
					);

					this.poNotification.success(this.litDetPro.messages.sucesIncPartC);
					this.modalIncPtCont.close();
					this.formParteCont.reset();
					this.formEnvolvido.patchValue({
						nome: codNZ2,
						cpfcnpj: cpfcnpj,
					});
				}
			});
		}
	}

	/**
	 *  função responsavel pela montagem do body da inclusao de parte contrária
	 * @param parteCont - formulário da parte contrária
	 * @param pk        - primary key do pedido
	 */
	bodyPutPartCon(parteCont: CamposParteContraria, pk: string) {
		let fwModel: FwModelBody = new FwModelBody('JURA184', 3, pk, 'NZ2MASTER');

		fwModel.models[0].addField('NZ2_NOME', parteCont.nomePtContraria, 0, 'C');
		fwModel.models[0].addField('NZ2_TIPOP', parteCont.tpPessoa, 0, 'C');
		fwModel.models[0].addField('NZ2_CGC', parteCont.cpfcnpjPtCont, 0, 'C');
		fwModel.models[0].addField('NZ2_EMAIL', parteCont.email, 0, 'C');

		return fwModel.serialize();
	}

	/**
	 * Validação dos campos antes de fazer o submit
	 */
	beforeSubPtC() {
		let bOk: boolean = this.formParteCont.valid; //valida os campos obrigatórios
		if (!bOk) {
			this.poNotification.error(this.litDetPro.messages.msgCamposObgrigat);
		}
		return bOk;
	}
	/**
	 * Busca os assuntos juridicos vinculados ao usuario, e que tenham instancia (NUQ)
	 */
	getTipoAssuntoJuridico() {
		this.lUploadDisabled = true;
		this.lpre_cadastro = true;
		let assuntoJuridico = window.localStorage.getItem('assuntoJuri');
		this.legalprocess.restore();
		this.legalprocess
			.get('listTpAssuntoJur', 'Busca os assuntos juridicos ')
			.subscribe(data => {
				if (this.JuriGenerics.changeUndefinedToEmpty(data.TpAssJur) != '') {
					data.TpAssJur.forEach(tipoAssJur => {
						let assuntosJuridicos = new OptionsComboSelect();

						assuntosJuridicos.value = tipoAssJur.value;
						assuntosJuridicos.label = tipoAssJur.label;

						this.listTipoAssunto.push(assuntosJuridicos);
					});
					if (this.JuriGenerics.changeUndefinedToEmpty(assuntoJuridico) != '') {
						this.formProcesso.patchValue({
							assuntoJuri: assuntoJuridico,
						});
						this.setTipoAssuntoJur(assuntoJuridico);
					}
				}
			});
	}
	/**
	 * Verifica se o assunto foi selecionado, se sim, permite que continue a inclusão, se não, barra qualquer ação
	 * @param tpAssJuri assunto selecionado
	 */
	setTipoAssuntoJur(tpAssJuri: string) {
		if (this.JuriGenerics.changeUndefinedToEmpty(tpAssJuri) != '') {
			this.isDisableSelTpAssJur = false;
			this.lUploadDisabled = false;
			this.lpre_cadastro = false;
		} else {
			this.isDisableSelTpAssJur = true;
			this.lUploadDisabled = true;
			this.lpre_cadastro = true;
		}
	}

	/**
	 * Preenche a comarca/ foro/ vara com os dados do cadastro de/para (O00) de acordo com os 7 útimos números do processo.
	 * Caso o parâmetro MV_JNUMCNJ do tipo de ssunto jurídico estiver ligado e a natureza validar CNJ (NQ1_VALCNJ = 1),
	 * valida se é um número CNJ válido.
	 */
	validaNumPro() {
		const dadosFormProc = this.formProcesso.value;
		if (dadosFormProc.numeroPro != '' && dadosFormProc.natureza != undefined) {
			let body = this.getBodyValidCNJ();
			this.legalprocess.post('validNumPro', body).subscribe(data => {
				this.formProcesso.patchValue({
					comarca: data.comarca,
					foro: data.foro,
					vara: data.vara,
				});
				this.isFromDePara = true;
				if (!data.valid && data.continue) {
					this.modalValidCNJ.open();
				} else if (!data.valid && data.message != '') {
					this.poNotification.error(data.message);
				}
			});
		}
	}

	/**
	 * Cria o body de request do POST validNumPro, informando:
	 * - Tipo de assunto jurídico;
	 * - Natureza;
	 * - Número do processo.
	 */
	getBodyValidCNJ() {
		const dadosFormProc = this.formProcesso.value;
		let body = new BodyValidNumPro();

		body.tpAssunto = dadosFormProc.assuntoJuri;
		body.natureza = dadosFormProc.natureza;
		body.numPro = dadosFormProc.numeroPro;

		return JSON.stringify(body);
	}

	/**
	 * Limpa o campo Número do processo
	 */
	resetNumPro() {
		this.formProcesso.patchValue({
			numeroPro: '',
		});
		this.modalValidCNJ.close();
	}

	/**
	 * Função que faz a criação dos campos no dynamic form.
	 */
	loadExtraFields() {
		this.isHideLoadCmpAdic = false;
		this.listExtraFields = [];

		this.jurConsultas.restore();
		this.jurConsultas.get('extraFields', 'Obtem os campos adicionais').subscribe(data => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.campos) != '') {
				data.campos.forEach(item => {
					let isNotFieldDet: boolean =
						this.listCamposDet.findIndex(x => x == item.campo) == -1;

					if (isNotFieldDet) {
						let cType: string;
						let listOptions: Array<PoSelectOption>;
						let booleanTrue: string;
						let booleanFalse: string;

						this.hasExtraFields = true;
						this.btnSalvarDetalhe = this.litDetPro.incluiCamposAdic;

						switch (item.tipo) {
							case 'N':
								cType = 'currency';
								break;
							case 'D':
								cType = 'date';
								break;
							case 'CB':
								listOptions = item.opcoes;
								break;
							case 'L':
								cType = 'boolean';
								booleanTrue = this.litDetPro.listSimNao.sim;
								booleanFalse = this.litDetPro.listSimNao.nao;
								break;
						}

						this.listExtraFields.push({
							property: item.campo,
							label: item.titulo,
							type: cType,
							options: listOptions,
							gridColumns: 4,
							gridSmColumns: 12,
							booleanTrue,
							booleanFalse,
						});
					}
				});
			}

			this.isHideLoadCmpAdic = true;
		});
	}

	/**
	 * Ação ao trocar de step
	 * @param stepper - Stepper atual
	 */
	changeStep(stepper) {
		// Mantém todos os steps habilitados
		stepper.poSteps._results.forEach(item => {
			if (item._status == 'disabled') {
				item._status = 'default';
			}
		});
	}
}
