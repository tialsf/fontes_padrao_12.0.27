export class DetProcesso {
	dataEntrada: string = '';
	dataDistrib: string = '';
	unidade: string = '';
	area: string = '';
	escritorio: string = '';
	centroCusto: string = '';
	responsavel: string = '';
	natureza: string = '';
	tipoAcao: string = '';
	assunto: string = '';
	detAssunto: string = '';
	instancia: string = '';
	comarca: string = '';
	foro: string = '';
	vara: string = '';
	vlrCausa: string = '';
	fCorrecao: string = '';
	numeroPro: string = '';
	moeda: string = '';
	dataCausa: string = '';
	distrRec: string = '';
	assuntoJuri: string = '';
	rito: string = '';
}

export class ItemEnvolvido {
	polo: string = '';
	tipoEnvol: string = '';
	principal: string = '';
	entidade: string = '';
	cpfcnpj: string = '';
	nome: string = '';
	cargo: string = '';
	localtrab: string = '';
	dataAdm: string = '';
	dataDem: string = '';
	cod: string = '';
}

export class CamposParteContraria {
	nomePtContraria: string = '';
	tpPessoa: string = '';
	cpfcnpjPtCont: string = '';
	email: string = '';
}
