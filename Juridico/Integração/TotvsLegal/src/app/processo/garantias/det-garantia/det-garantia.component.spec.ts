import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetGarantiaComponent } from './det-garantia.component';

describe('DetGarantiaComponent', () => {
  let component: DetGarantiaComponent;
  let fixture: ComponentFixture<DetGarantiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetGarantiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetGarantiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
