import { Component, OnInit, ViewChild } from '@angular/core';
import { PoModalComponent, PoI18nService, PoBreadcrumb, PoNotificationService, PoModalAction, PoTableColumn, PoUploadComponent, PoComboComponent } from '@po-ui/ng-components';
import { RoutingService } from 'src/app/services/routing.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Garantia } from './garantia';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { FwmodelService } from 'src/app/services/fwmodel.service';
import { LegalprocessService, sysParamStruct, SysProtheusParam } from 'src/app/services/legalprocess.service';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { FwmodelAdaptorService, adaptorFilterParam, FwGetFilDesAdapterService } from 'src/app/services/fwmodel-adaptor.service';
import { JurJustifModalComponent } from 'src/app/shared/jur-justif-modal/jur-justif-modal.component';
import { FCorrecaoAdapterService } from 'src/app/services/combos-fwmodel-adapter.service';
import { TpGarAdapterService } from 'src/app/services/combos-fwmodel-adapter.service';
import { NaturezaAdapterService } from 'src/app/services/combos-fwmodel-adapter.service'
import { FornecedorAdapterService } from 'src/app/services/combos-fwmodel-adapter.service'
import { RateioAdapterService } from 'src/app/services/combos-fwmodel-adapter.service'
import { BancoAdapterService } from 'src/app/services/combos-fwmodel-adapter.service'

class DadoCombo {
	value: string;
	label: string;
}
class DadosTableLev{
	codLev: string;
	tipo  : string;
	data  : string;
	respo : string;
	valor : string;
	lev   : string;
}

class dadosGrupoAprovador {
	filial  : string;
	value   : string;
	codResp : string;
	label   : string;
	minLimit: number;
	maxLimit: number;
}

@Component({
  selector: 'app-det-garantia',
  templateUrl: './det-garantia.component.html',
  styleUrls: ['./det-garantia.component.css']
})
export class DetGarantiaComponent implements OnInit {
	litDetGar : any;
	formGarantia : FormGroup;

	titleDet : string;
	pk : string            = '';
	descGar : string       = '';
	cajuri : string        = '';
	codGar : string        = '';
	chaveProcesso : string = '';
	filial : string        = '';
	lblBtnSalvar : string  = '';
	filtroCorrecao: string = "NW7_VISIV='1'";
	filtroTpGar   : string = "NQW_TIPO='1'";
	empresaLogada : string = window.localStorage.getItem('codEmp');

	uploadCount : number        = 0;
	uploadSuccessCount : number = 0;

	disableInclusaoLev : boolean         = false;
	bOperacaoInclusao : boolean          = false;
	bIntegraFinanceiro : boolean         = false;
	bShowGroupIntFinanc : boolean        = false;
	bLoadingIncGar : boolean             = false;
	isHideLoadingLevantamentos : boolean = false;
	isHideLoadingGarantia : boolean      = false;
	isProcEncerrado: boolean             = false;
	
	sysParams               : Array<sysParamStruct>      = []
	lsIntFinTitulo          : Array<DadoCombo>           = [];
	arrOpcIntegraFinanceiro : Array<DadoCombo>           = [];
	listEnvolvido           : Array<DadoCombo>           = [];
	levantamentos           : Array<DadosTableLev>       = [];
	lsIntFinGrupoAprovacao  : Array<dadosGrupoAprovador> = [];

	// ações do modal de confirmação de exclusão (Garantia)
	fechar: PoModalAction = {
		action: () => {
			this.thfModal.close();
			this.excluir.loading = false;
		},
		label: 'Fechar',
	};
	
	excluir: PoModalAction = {
		action: () => {
			this.excluir.loading = true;
			this.deleteVerify();
		},
		label: 'Excluir',
	};
	
	/**
	 * Colunas dos levantamentos
	 */
	colLevantamentos: Array<PoTableColumn> = [
		{ property: 'tipo', label: 'Tipo' },
		{ property: 'data', label: 'Data' },
		{ property: 'respo', label: 'Responsável' },
		{ property: 'valor', label: 'Valor (R$)', type: 'number', format: '1.2-5' },
		{ property: 'lev', label: ' ', type: 'icon', width: '4%' ,
			action: (value)=>{
				this.openLevantamento(value);
			}, 
		icons: [{ value: 'editarLev', icon: 'po-icon po-icon-edit', color: '#29b6c5' }] 
		}
	];
	
	/**
	 * estrutura de navegação da URL atual
	 */
	public breadcrumb: PoBreadcrumb = {
		items: [
		],
	};
	
	@ViewChild('excluiGarantia' , { static: true  } ) thfModal          : PoModalComponent;
	@ViewChild('docUpload') docUpload         : PoUploadComponent;
	@ViewChild('modJustif'      , { static: true  } ) modalJustif       : JurJustifModalComponent;
	@ViewChild('cmbFornecedor') cmbFornecedorGar  : PoComboComponent;
	@ViewChild('cmbNatureza') cmbNaturezaGar    : PoComboComponent;
	@ViewChild('cmbGrupoAprov') cmbGrupoAprovGar  : PoComboComponent;
	@ViewChild('cmbBancoAgencia') cmbBancoAgenciaGar: PoComboComponent;

	constructor(
		public fwModelAdaptorService : FwmodelAdaptorService,
		public thfNotification       : PoNotificationService,
		public filiaisAdapterService : FwGetFilDesAdapterService,
		private legalprocess         : LegalprocessService,
		private route                : ActivatedRoute,
		private routingState         : RoutingService,
		private formBuilder          : FormBuilder,
		private fwmodel              : FwmodelService,
		private router               : Router,
		private sysProtheusParam     : SysProtheusParam,
		private JuriGenerics         : JurigenericsService,
		protected thfI18nService     : PoI18nService,
		public correcaoAdapterService: FCorrecaoAdapterService,
		public tpGarAdapterService   : TpGarAdapterService,
		public naturezaAdapterService: NaturezaAdapterService,
		public fornecedorAdapterService:FornecedorAdapterService,
		public rateioAdapterService  : RateioAdapterService,
		public bancoAdapterService   : BancoAdapterService
	) {

		// Setups de adapters
		this.correcaoAdapterService.setup(new adaptorFilterParam("JURA061", "NW7_VISIV='1'", "NW7_DESC", "NW7_DESC", "", "NW7_COD", "", "getFormaCorrecao"))
		this.tpGarAdapterService.setup(new adaptorFilterParam("JURA024", "NQW_TIPO='1'", "NQW_DESC", "NQW_DESC", "", "NQW_COD", "", "getTipoGarantia"))
		this.naturezaAdapterService.setup(new adaptorFilterParam("JNATFIN", "", "ED_DESCRIC", "ED_DESCRIC", "", "ED_CODIGO", "", "GetNatureza", undefined, undefined, { useTenantId: true, empresa: window.localStorage.getItem("codEmp"), filial: window.localStorage.getItem('filLog'), campoFiltro: "", tenantOnHeader: true }))
		this.fornecedorAdapterService.setup( new adaptorFilterParam("JURA132", "", "A2_NOME", "A2_COD,A2_LOJA,A2_NOME", "A2_NOME - [A2_COD/A2_LOJA]", "A2_COD,A2_LOJA", "", "GetFornecedor", undefined, undefined, { useTenantId: true, empresa: window.localStorage.getItem("codEmp"), filial: window.localStorage.getItem('filLog'), campoFiltro: "", tenantOnHeader: true }))
		this.rateioAdapterService.setup(new adaptorFilterParam("JTABRATEIO", "", "OH6_DESCRI", "OH6_DESCRI", "", "OH6_CODIGO", "", "getRateio"))
		this.bancoAdapterService.setup(new adaptorFilterParam("JBANFIN", "", "A6_AGENCIA,A6_NUMCON", "A6_AGENCIA,A6_NUMCON", "A6_AGENCIA\\A6_NUMCON", "A6_COD,A6_AGENCIA,A6_NUMCON", "", "getBanco", undefined, undefined, { useTenantId: true, empresa: window.localStorage.getItem("codEmp"), filial: window.localStorage.getItem('filLog'), campoFiltro: "", tenantOnHeader: true }))

		this.arrOpcIntegraFinanceiro = [{ label: 'Sim', value: '1' }, { label: 'Não', value: '2' }];
		routingState.loadRouting();
		
		if (this.route.snapshot.paramMap.get("codGar") != undefined) {
			this.codGar = atob(decodeURIComponent(this.route.snapshot.paramMap.get("codGar")));
		}

		this.filial = this.route.snapshot.paramMap.get('filPro');
		window.sessionStorage.setItem('filial', atob(this.filial));
		
		this.chaveProcesso = this.route.snapshot.paramMap.get('codPro');

		this.cajuri = atob(decodeURIComponent(this.chaveProcesso));

		this.disableInclusaoLev = (this.codGar == "");

		// Chamada do serviço poI18n para buscar as constantes a partir do contexto 'garantias' no arquivo JurTraducao.
		thfI18nService.getLiterals({ context: 'detGarantias', language: thfI18nService.getLanguage() })
		.subscribe((litGar) => {
			this.litDetGar = litGar;

			this.colLevantamentos[0].label = this.litDetGar.levantamentos.tipo;
			this.colLevantamentos[1].label = this.litDetGar.levantamentos.data;
			this.colLevantamentos[2].label = this.litDetGar.levantamentos.responsavel;
			this.colLevantamentos[3].label = this.litDetGar.levantamentos.valor;

			// Nova Garantia
			if (this.JuriGenerics.changeUndefinedToEmpty( this.codGar) == '') {
				this.titleDet = this.litDetGar.tituloNovaGar;
				this.lblBtnSalvar = this.litDetGar.btnIncluirGarantia;
				this.bOperacaoInclusao = true;

				this.breadcrumb.items = [
					{ label: this.litDetGar.bread.home, link: '/' },
					{ label: this.litDetGar.bread.processo, link: '/processo/' + this.filial + '/' + this.chaveProcesso},
					{ label: this.litDetGar.bread.garantias, link: '/processo/' + this.filial + '/' + this.chaveProcesso + '/garantia'},
					{ label: this.litDetGar.tituloNovaGar}
				];
				this.titleDet = this.litDetGar.tituloNovaGar;
			
			// Atualização
			} else {
				this.titleDet = this.litDetGar.tituloAlterarGar;
				this.lblBtnSalvar = this.litDetGar.btnSalvarGarantia;

				this.breadcrumb.items = [
					{ label: this.litDetGar.bread.home, link: '/' },
					{ label: this.litDetGar.bread.processo, link: '/processo/' + this.filial + '/' + this.chaveProcesso},
					{ label: this.litDetGar.bread.garantias, link: '/processo/' + this.filial + '/' + this.chaveProcesso + '/garantia'},
					{ label: this.descGar}
				];
				this.titleDet = this.litDetGar.tituloAlterarGar;
			}
		});
	}

	ngOnInit() {
		const frmGarantia: Garantia = new Garantia();
		this.getInfoProcesso();
		this.createForm(frmGarantia);
		this.habilitaCampos()
		
		// Busca os parâmetros do Protheus
		this.sysProtheusParam.getParamByArray(["MV_JINTVAL","MV_JTVENJF"]).subscribe((params)=>{
			this.sysParams = params;

			if (this.sysProtheusParam.getParamValueByName(params,"MV_JINTVAL") == '1'){
				this.bShowGroupIntFinanc = true;
				this.insertIntegFin(frmGarantia);
			}

			this.getGarantias();
			this.getLevantamentos();
		})
	}

	/**
	 * Adiciona os controles da Integração Financeira
	 * @param frmGarantia 
	 */
	insertIntegFin(frmGarantia: Garantia){
		this.formGarantia.addControl('integraFinanceiro' , new FormControl(frmGarantia.integraFinanceiro));
		this.formGarantia.addControl('intFinFiliDest'    , new FormControl(frmGarantia.intFinFiliDest));
		this.formGarantia.addControl('intFinPrefixo'     , new FormControl(frmGarantia.intFinPrefixo));
		this.formGarantia.addControl('intFinNatureza'    , new FormControl(frmGarantia.intFinNatureza));
		this.formGarantia.addControl('intFinTitulo'      , new FormControl(frmGarantia.intFinTitulo));
		this.formGarantia.addControl('intFinFornecedor'  , new FormControl(frmGarantia.intFinFornecedor));
		this.formGarantia.addControl('intFinBancoAgencia', new FormControl(frmGarantia.intFinBancoAgencia));
		this.formGarantia.addControl('intFinGrupoAprovac', new FormControl(frmGarantia.intFinGrupoAprovac));
	}
	
	
	/**
	* Responsável por obter os dados inputados no formulário de inclusão
	* de garantias para serem usados após o submit
	*/
	createForm(garantia: Garantia) {
		// revisar após o ajuste do uso de formcontrol no thf-datapicker
		this.formGarantia = this.formBuilder.group({
			tipo         : [ garantia.tipo, Validators.compose([Validators.required])],
			dataGarantia : [ new Date(), Validators.compose([Validators.required])],
			envolvido    : [ garantia.envolvido],
			correcao     : [ garantia.correcao],
			valor        : [ garantia.valor, Validators.compose([Validators.required])],
			levantamento : [ garantia.levantamento],
			sJuiza       : [ garantia.sJuiza],
		});
	}

	/**
	 * Busca as informações do Processo
	 */
	getInfoProcesso() {
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA095');
		this.fwmodel.setFilter("NSZ_COD='" + this.cajuri + "' ");
		this.fwmodel.setFilter("NSZ_FILIAL='" + atob(this.filial) + "'"); 
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.get("getInfoProcesso").subscribe(data => {
			if (data.resources != "") {
				this.breadcrumb.items[1].label = this.litDetGar.bread.meuProcesso + " " + this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NSZ_DAREAJ");
				this.isProcEncerrado = (this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NSZ_SITUAC") == "2")
			}
		});
	}

	/**
	 * Responsável por obter os dados de garantias
	 */
	getGarantias() {
		this.isHideLoadingGarantia = false;
		if (this.JuriGenerics.changeUndefinedToEmpty(this.codGar) == "") {
			this.getEnvolvidos();
			this.getGrupoAprovacao();
			this.getTitulo();
			this.bOperacaoInclusao = true;
			this.isHideLoadingGarantia = true;
		} else {
			this.fwmodel.restore();
			this.fwmodel.setModelo("JURA098");
			this.fwmodel.setCampoVirtual(true);
			this.fwmodel.setFilter("NT2_CAJURI='" + this.cajuri + "'"); 
			this.fwmodel.setFilter("NT2_FILIAL='" + atob(this.filial) + "'"); 
			this.fwmodel.setFilter("NT2_COD='" + this.codGar + "'" );
			
			this.fwmodel.get('Obtem dados da Garantia').subscribe((data) => {
				if (data.count == 0) {
					this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codGar))], { relativeTo: this.route });
					this.thfNotification.error(this.litDetGar.naoEncontrado);
				} else {
					let dadosGar = data.resources[0].models[0].fields;
					

					this.pk = data.resources[0].pk;
					this.cajuri = this.JuriGenerics.findValueByName(dadosGar, 'NT2_CAJURI');
					this.descGar = this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(dadosGar, "NT2_DATA"), "dd/mm/yyyy");
					
					this.getEnvolvidos(this.JuriGenerics.findValueByName(dadosGar, "NT2_CENVOL"));
					this.getGrupoAprovacao(this.JuriGenerics.findValueByName(dadosGar, "NT2_CGRUAP"));
					this.getTitulo(this.JuriGenerics.findValueByName(dadosGar, "NT2_CTIPOT"));

					this.formGarantia.patchValue({
						correcao     : [this.JuriGenerics.findValueByName(dadosGar, "NT2_CCOMON")],
						tipo         : [this.JuriGenerics.findValueByName(dadosGar, "NT2_CTPGAR")],
						valor        : [this.JuriGenerics.findValueByName(dadosGar, 'NT2_VALOR') ],
						sJuiza       : [this.JuriGenerics.findValueByName(dadosGar, "NT2__GASJA")],
						levantamento : [this.JuriGenerics.findValueByName(dadosGar, "NT2__LEVSD")],
						dataGarantia : this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(dadosGar, "NT2_DATA"), "yyyy-mm-dd")
					});
					
					// Parte de Integração Financeira
					if (this.sysProtheusParam.getParamValueByName( this.sysParams, "MV_JINTVAL") == '1') {
						let filialDest: string = this.JuriGenerics.findValueByName(dadosGar, "NT2_FILDES");

						if (this.JuriGenerics.changeUndefinedToEmpty(filialDest) != "") {
							this.setFilialDest(filialDest);
						}
					
						this.formGarantia.patchValue({
							integraFinanceiro  : this.JuriGenerics.findValueByName(dadosGar, "NT2_INTFIN"),
							intFinFiliDest     : this.JuriGenerics.findValueByName(dadosGar, "NT2_FILDES"),
							intFinFornecedor   : this.JuriGenerics.findValueByName(dadosGar, "NT2_CFORNT") + this.JuriGenerics.separador + this.JuriGenerics.findValueByName(dadosGar, "NT2_LFORNT"),
							intFinBancoAgencia : this.JuriGenerics.findValueByName(dadosGar, "NT2_CBANCO") + this.JuriGenerics.separador + this.JuriGenerics.findValueByName(dadosGar, "NT2_CAGENC") +  this.JuriGenerics.separador + this.JuriGenerics.findValueByName(dadosGar, "NT2_CCONTA"),
							intFinNatureza     : this.JuriGenerics.findValueByName(dadosGar, "NT2_CNATUT"),
							intFinPrefixo      : this.JuriGenerics.findValueByName(dadosGar, "NT2_PREFIX"),
							intFinTitulo       : this.JuriGenerics.findValueByName(dadosGar, "NT2_CTIPOT"),
							intFinGrupoAprovac : this.JuriGenerics.findValueByName(dadosGar, "NT2_CGRUAP"),
						});
						this.habilitaCampos();
					}
					this.breadcrumb.items[3].label = this.JuriGenerics.findValueByName(dadosGar, "NT2_DTPGAR") + ' - ' + this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(dadosGar, "NT2_DATA"), "dd/mm/yyyy");
					this.isHideLoadingGarantia = true;

					return this.pk;
				}
			});
		}
	}
	
	/**
	 * Responsável por obter a lista de envolvidos
	 * @param cCodEnvo 
	 */
	getEnvolvidos(cCodEnvo?: string) {
		
		this.legalprocess.restore();
		this.legalprocess.get('process/' +this.cajuri,"Lista de Envolvidos").subscribe((data) => {
			if (data.processes != '') {
				data.processes[0].party.forEach(item => {

					const dadosEnvolvidos = new DadoCombo();
					dadosEnvolvidos.label = item.name,
						dadosEnvolvidos.value = item.id;

					this.listEnvolvido.push(dadosEnvolvidos);
				});
				
			}
			if (cCodEnvo != undefined) {
				this.formGarantia.patchValue({
					envolvido: [cCodEnvo]
				});
			}
		});
	}

	/**
	 * Busca os grupos de aprovação
	 * @param codGrupoAprov - Código do Grupo de Aprovação posicionado
	 */
	getGrupoAprovacao(codGrupoAprov?: string) {
		this.legalprocess.restore();
		this.legalprocess.get('tlprocess/grupoAprv','Grupo de Aprovação').subscribe((data) => {
			if (data.groupApprover != '') {
				data.groupApprover.forEach(item => {
					let dadosGrupo = new dadosGrupoAprovador();
					dadosGrupo = {
						filial: item.filial,
						value: item.id,
						label: item.approver,
						minLimit: item.minLimit,
						maxLimit: item.maxLimit,
						codResp: item.codResp
					};
					this.lsIntFinGrupoAprovacao.push(dadosGrupo);
				});

				if (codGrupoAprov != undefined) {
					this.formGarantia.patchValue({
						intFinGrupoAprovac: codGrupoAprov
					});
				}
			}
		});
	}

	/**
	 * Busca os titulos na SX5
	 * @param codTitulo - Código do Titulo posicionado
	 */
	getTitulo(codTitulo?: string) {
		this.legalprocess.restore();
		this.legalprocess.get('tlprocess/tabGen/05','Título').subscribe((data) => {
			if (data.result != '') {
				data.result.forEach(item => {
					let dadosTitulo = new DadoCombo();
					dadosTitulo = {
						value: item.chave.trim(),
						label: item.descricao
					};
					this.lsIntFinTitulo.push(dadosTitulo);
				});
				if (codTitulo != undefined) {
					this.formGarantia.patchValue({
						intFinTitulo: codTitulo
					});
				}
			}
		});
	}	

	/**
	 * Função de inicio das validações e Commit.
	 */
	confirmVerify(){
		if (this.beforeSubmit()){
			this.submitGar()
		}
	}

	/**
	 * Função validação e delete.
	 */
	deleteVerify(){
		if (this.beforeDelete()){
			this.delGarantia()
		}
	}

	/**
	 * Pré-validação.
	 */
	beforeSubmit(){
		let isSubmitable: boolean = false;
		isSubmitable = this.formGarantia.valid;
		
		if (isSubmitable){
			const garantia = this.formGarantia.value;
			if (garantia.integraFinanceiro == '1') {
				isSubmitable = ((garantia.intFinPrefixo != undefined && garantia.intFinPrefixo != '') &&
					(garantia.intFinFornecedor != undefined && garantia.intFinFornecedor != '') &&
					(garantia.intFinNatureza != undefined && garantia.intFinNatureza != '') &&
					(garantia.intFinTitulo != undefined && garantia.intFinTitulo != ''));

				if (!isSubmitable) {
					this.thfNotification.error(this.litDetGar.messages.msgCamposIntegFinanceiro);
				}
			} else {
				isSubmitable = true
			}
		} else {
			isSubmitable = false
			this.thfNotification.error(this.litDetGar.messages.msgCamposPadrao);
		}

		// Validação para apresentar o Modal de Justificativa, caso o processo esteja encerrado
		if (isSubmitable){
			if (this.isProcEncerrado && this.sysProtheusParam.getParamValueByName(this.sysParams,"MV_JTVENJF") == "1"){
				isSubmitable = false;
				this.modalJustif.openModal(this.cajuri,
					()=>{
						if (this.modalJustif.isOk){
							this.submitGar()
						}
					});
			} else {
				isSubmitable = true;
			}
		}

		return isSubmitable
	}

		/**
	 * Pré-validação.
	 */
	beforeDelete(){
		let isSubmitable: boolean = false;
		isSubmitable = this.formGarantia.valid;

		// Validação para apresentar o Modal de Justificativa, caso o processo esteja encerrado
		if (isSubmitable){
			if (this.isProcEncerrado && this.sysProtheusParam.getParamValueByName(this.sysParams,"MV_JTVENJF") == "1"){
				isSubmitable = false;
				this.modalJustif.openModal(this.cajuri,
					()=>{
						if (this.modalJustif.isOk){
							this.delGarantia()
						}
					});
			} else {
				isSubmitable = true;
			}
		}

		return isSubmitable
	}
	
	/**
	* Obtem os dados do formulário no momento da inclusão e passa como parâmetro
	* para o postGarantia. Esses dados serão usados no body da requisisão
	* para realizar a inclusão através do FWModel.
	*/
	submitGar() {
		window.sessionStorage.setItem('filial', atob(this.filial));

		if (this.JuriGenerics.changeUndefinedToEmpty(this.codGar) == "") {
			this.insertGarantia();
		} else {
			this.updateGarantia();
		}
	}
	
	insertGarantia() {

		const cBody = this.bodyPostGar(this.formGarantia.value);
		this.bLoadingIncGar = true;

		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA098');
		this.fwmodel.post(cBody, 'Inclusão de Garantia').subscribe((data) => {
			if (data != '') {
				this.codGar = this.JuriGenerics.findValueByName(data.models[0].fields, "NT2_COD");
				this.thfNotification.success(this.litDetGar.msgInclusao);
				this.formGarantia.reset(new Garantia());
				
				// Valida se há documentos a serem enviados
				if (this.docUpload.hasOwnProperty('currentFiles') && this.docUpload.currentFiles.length > 0) {
					this.uploadCount = this.docUpload.currentFiles.length;
					this.sendDocs();
				} else {
					this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codGar))], { relativeTo: this.route });
				}
			}
			this.bLoadingIncGar = false;
		});

	}

	updateGarantia() {

		this.bLoadingIncGar = true;

		const dadosGarantia = this.formGarantia.value;

		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA098');
		this.fwmodel.put(this.pk, this.bodyPutGar(dadosGarantia), 'Alteração da Garantia').subscribe((data) => {
			if (data == '') {
				this.bLoadingIncGar = false;
			} else {
				this.codGar = this.JuriGenerics.findValueByName(data.models[0].fields, "NT2_COD");
				this.thfNotification.success(this.litDetGar.sucessUpdate);
				this.formGarantia.reset(new Garantia());
					
				// Valida se há documentos a serem enviados
				if (this.docUpload.hasOwnProperty('currentFiles') && this.docUpload.currentFiles.length > 0) {
					this.uploadCount = this.docUpload.currentFiles.length;
					this.sendDocs();
				} else {
					this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codGar))], { relativeTo: this.route });
				}
				this.bLoadingIncGar = false;
			}
		});
	}
	
	/**
	 * Cancela operação
	 */
	onCancel() {
		this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codGar))], { relativeTo: this.route });
	}

	/**
	 * Responsavel por remover uma garantia
	 */
	delGarantia() {
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA098');

		this.fwmodel.delete(this.pk, 'Deleção da Garantia').subscribe((data) => {
			if (data == true) {
				this.thfNotification.success(this.litDetGar.msgExclusao);
				this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codGar))], { relativeTo: this.route });
			} else {
				this.excluir.loading = false;
			}
		});
	}

	// Responsável por trazer os levantamentos
	getLevantamentos() {
		this.isHideLoadingLevantamentos = false;
		this.levantamentos = [];
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA098');
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.setFilter("NT2_CAJURI='" + this.cajuri + "'");
		this.fwmodel.setFilter("NT2_FILIAL='" + atob(this.filial) + "'");
		this.fwmodel.setFilter("NT2_MOVFIN='2'")
		this.fwmodel.setFilter("NT2_CGARAN='" + this.codGar + "'");

		this.fwmodel.get('Obter levantamentos').subscribe((data) => {
			if (data.resources != '') {
				data.resources.forEach(item => {
					let itemLev = new DadosTableLev();
						itemLev.codLev = this.JuriGenerics.findValueByName(item.models[0].fields, 'NT2_COD')
						itemLev.tipo  = this.JuriGenerics.findValueByName(item.models[0].fields, "NT2_DTPGAR"),
						itemLev.data  = this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(item.models[0].fields, "NT2_DATA"), "dd/mm/yyyy"),
						itemLev.respo = this.JuriGenerics.findValueByName(item.models[0].fields, "NT2_DENVOL"),
						itemLev.valor = this.JuriGenerics.findValueByName(item.models[0].fields, "NT2_VALOR"),
						itemLev.lev   = 'editarLev'
					
						this.levantamentos.push(itemLev);
				});
			}
			this.isHideLoadingLevantamentos = true;
		});
	}

	habilitaCampos() {
		this.bIntegraFinanceiro = !(this.formGarantia.value.integraFinanceiro == "1");
	}

	/**
	* Monta o body para o post de garantias com/sem integração
	* @param garantia dados do formulário
	*/
	bodyPostGar(formGarantia: Garantia) {
		if (this.JuriGenerics.changeUndefinedToEmpty(formGarantia.integraFinanceiro) !== '1') {
			const bodyInsereGar: InsereGar = {
				id: 'JURA098', operation: 1, models: [
					{
						id: 'NT2MASTER', modeltype: 'FIELDS', fields: [
							{ id: 'NT2_CAJURI', order: 2, value: this.cajuri },
							{ id: 'NT2_CTPGAR', order: 4, value: formGarantia.tipo },
							{ id: 'NT2_CCOMON', order: 6, value: formGarantia.correcao },
							{ id: 'NT2_MOVFIN', order: 8, value: '1' },
							{ id: 'NT2_DATA', order: 9, value: formGarantia.dataGarantia.replace(/-/g, '') },
							{ id: 'NT2_CMOEDA', order: 10, value: '01' },
							{ id: 'NT2_VALOR', order: 16, value: formGarantia.valor.toString() },
							{ id: 'NT2_CENVOL', order: 21, value: formGarantia.envolvido },
							{ id: 'NT2_INTFIN', order: 20, value: '2' },
							{ id: 'NT2_DTULAT', order: 12, value: this.JuriGenerics.makeDateProtheus() }
						]
					}
				]
			};
			return JSON.stringify(bodyInsereGar);

		} else {
			const bodyInsereGar: InsereGarIntegra = {
				id: 'JURA098', operation: 1, models: [
					{
						id: 'NT2MASTER', modeltype: 'FIELDS', fields: [
							{ id: 'NT2_FILDES', order: 2, value: formGarantia.intFinFiliDest},
							{ id: 'NT2_MOVFIN', order: 3, value: '1' },
							{ id: 'NT2_CAJURI', order: 4, value: this.cajuri },
							{ id: 'NT2_CTPGAR', order: 6, value: formGarantia.tipo },
							{ id: 'NT2_CCOMON', order: 8, value: formGarantia.correcao },
							{ id: 'NT2_DATA', order: 10, value: formGarantia.dataGarantia.replace(/-/g, '') },
							{ id: 'NT2_CMOEDA', order: 12, value: '01' },
							{ id: 'NT2_VALOR', order: 18, value: formGarantia.valor.toString() },
							{ id: 'NT2_CENVOL', order: 27, value: formGarantia.envolvido },
							// Campos da integração financeira
							{ id: 'NT2_DTULAT', order: 14, value: this.JuriGenerics.makeDateProtheus() },
							{ id: 'NT2_PREFIX', order: 52, value: formGarantia.intFinPrefixo },
							{ id: 'NT2_CNATUT', order: 53, value: formGarantia.intFinNatureza },
							{ id: 'NT2_CTIPOT', order: 54, value: formGarantia.intFinTitulo },
							{ id: 'NT2_CFORNT', order: 55, value: this.JuriGenerics.getValueByOption(this.cmbFornecedorGar,"A2_COD") },
							{ id: 'NT2_LFORNT', order: 56, value: this.JuriGenerics.getValueByOption(this.cmbFornecedorGar,"A2_LOJA") },
							{ id: 'NT2_CGRUAP', order: 58, value: formGarantia.intFinGrupoAprovac },
							{ id: 'NT2_CBANCO', order: 59, value: this.JuriGenerics.getValueByOption(this.cmbBancoAgenciaGar,"A6_COD") },
							{ id: 'NT2_CAGENC', order: 60, value: this.JuriGenerics.getValueByOption(this.cmbBancoAgenciaGar,"A6_AGENCIA") },
							{ id: 'NT2_CCONTA', order: 61, value: this.JuriGenerics.getValueByOption(this.cmbBancoAgenciaGar,"A6_NUMCON") },
							{ id: 'NT2_INTFIN', order: 65, value: formGarantia.integraFinanceiro }
							
						]
					}
				]
			};

			return JSON.stringify(bodyInsereGar);
		}
	}
	
	/**
	 * Monta o body para o put de garantias com/sem integração
	 * @param garantia dados do formulário
	 */
	bodyPutGar(garantia?: Garantia) {
		if (this.JuriGenerics.changeUndefinedToEmpty(garantia.integraFinanceiro) !== '1') {
			const bodyAtualiza: Atualiza = {
				id: 'JURA098', operation: 4, models: [
					{
						id: 'NT2MASTER', modeltype: 'FIELDS', fields:
							[
								{ id: 'NT2_CAJURI', order: 1, value: '' },
								{ id: 'NT2_CTPGAR', order: 2, value: '' },
								{ id: 'NT2_CCOMON', order: 3, value: '' },
								{ id: 'NT2_DATA'  , order: 4, value: '' },
								{ id: 'NT2_VALOR' , order: 5, value: '' },
								{ id: 'NT2_CENVOL', order: 6, value: '' }
							]
					}
				]
			};

			// Campos do Model Master (NT2)
			bodyAtualiza.models[0].fields[0].value = this.cajuri;
			bodyAtualiza.models[0].fields[1].value = garantia.tipo;
			bodyAtualiza.models[0].fields[2].value = garantia.correcao.toString();
			bodyAtualiza.models[0].fields[3].value = garantia.dataGarantia.replace(/-/g, '');
			bodyAtualiza.models[0].fields[4].value = garantia.valor.toString();
			bodyAtualiza.models[0].fields[5].value = garantia.envolvido.toString();

			return JSON.stringify(bodyAtualiza);

		} else {
			const bodyAtualiza: AtualizaIntegra = {
				id: 'JURA098', operation: 4, models: [
					{
						id: 'NT2MASTER', modeltype: 'FIELDS', fields: [
							{ id: 'NT2_FILDES', order: 2, value: ''  },
							{ id: 'NT2_CAJURI', order: 4, value: ''  },
							{ id: 'NT2_CTPGAR', order: 6, value: ''  },
							{ id: 'NT2_CCOMON', order: 8, value: ''  },
							{ id: 'NT2_DATA'  , order: 10, value: '' },
							{ id: 'NT2_DTULAT', order: 14, value: '' },
							{ id: 'NT2_VALOR' , order: 18, value: '' },
							{ id: 'NT2_CENVOL', order: 27, value: '' },
							// Campos da integração financeira
							{ id: 'NT2_PREFIX', order: 52, value: '' },
							{ id: 'NT2_CNATUT', order: 53, value: '' },
							{ id: 'NT2_CTIPOT', order: 54, value: '' },
							{ id: 'NT2_CFORNT', order: 55, value: '' },
							{ id: 'NT2_LFORNT', order: 56, value: '' },
							{ id: 'NT2_CBANCO', order: 59, value: '' },
							{ id: 'NT2_CAGENC', order: 60, value: '' },
							{ id: 'NT2_CCONTA', order: 61, value: '' },
							{ id: 'NT2_CGRUAP', order: 58, value: '' },
							{ id: 'NT2_INTFIN', order: 65, value: '' }]
					}
				]
			};

			// Campos do Model Master (NT2)
			bodyAtualiza.models[0].fields[0].value = this.JuriGenerics.changeUndefinedToEmpty( garantia.intFinFiliDest);
			bodyAtualiza.models[0].fields[1].value = this.cajuri;
			bodyAtualiza.models[0].fields[2].value = garantia.tipo;
			bodyAtualiza.models[0].fields[3].value = garantia.correcao.toString();
			bodyAtualiza.models[0].fields[4].value = garantia.dataGarantia.replace(/-/g, '');
			bodyAtualiza.models[0].fields[5].value = this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.makeDateProtheus());
			bodyAtualiza.models[0].fields[6].value = garantia.valor.toString();
			bodyAtualiza.models[0].fields[7].value = garantia.envolvido.toString();
			
			// Campos da integração financeira
			bodyAtualiza.models[0].fields[8].value = this.JuriGenerics.changeUndefinedToEmpty(garantia.intFinPrefixo);
			bodyAtualiza.models[0].fields[9].value = this.JuriGenerics.changeUndefinedToEmpty(garantia.intFinNatureza);
			bodyAtualiza.models[0].fields[10].value = this.JuriGenerics.changeUndefinedToEmpty(garantia.intFinTitulo);
			bodyAtualiza.models[0].fields[11].value = this.JuriGenerics.getValueByOption(this.cmbFornecedorGar,"A2_COD");
			bodyAtualiza.models[0].fields[12].value = this.JuriGenerics.getValueByOption(this.cmbFornecedorGar,"A2_LOJA");
			bodyAtualiza.models[0].fields[13].value = this.JuriGenerics.getValueByOption(this.cmbBancoAgenciaGar,"A6_COD");	
			bodyAtualiza.models[0].fields[14].value = this.JuriGenerics.getValueByOption(this.cmbBancoAgenciaGar,"A6_AGENCIA");
			bodyAtualiza.models[0].fields[15].value = this.JuriGenerics.getValueByOption(this.cmbBancoAgenciaGar,"A6_NUMCON");
			bodyAtualiza.models[0].fields[16].value = this.JuriGenerics.changeUndefinedToEmpty(garantia.intFinGrupoAprovac);
			bodyAtualiza.models[0].fields[17].value = this.JuriGenerics.changeUndefinedToEmpty(garantia.integraFinanceiro);

			return JSON.stringify(bodyAtualiza);
		}
	}

	callAnexos() {
		this.router.navigate(['./anexos'], { relativeTo: this.route });
	}
	/**
	 * Realiza o envio dos documentos
	 */
	sendDocs() {
		if (this.docUpload.currentFiles.length > 0) {
			this.docUpload.sendFiles();
		}
	}
	/**
	 * Carrega as informações para o POST do upload de arquivos
	 * @param file 
	 */
	uploadFiles(file) {
		file.data = {
					cajuri: this.cajuri.toString(),
					entidade: 'NT2',
					codEntidade: this.codGar.toString()
				};
	}

	/**
	 * Trata para redirecionar após concluir todos os anexos
	 */
	uploadSucess() {
		this.uploadSuccessCount++;

		if (this.uploadSuccessCount == this.docUpload.currentFiles.length) {
			this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codGar))], { relativeTo: this.route });
		}
	}

	/**
	 * Chama o SelectFiles do po-upload
	 */
	getDocuments() {
		this.docUpload.selectFiles();
	}

	/**
	 * Abre o Det-levantamento em modo de Update ou inclusão
	 */
	openLevantamento(value ?: DadosTableLev) {
		if ( value == undefined){
			this.router.navigate(["levantamento/cadastro"], {relativeTo: this.route})
		} else {
			this.router.navigate(["levantamento/"+ btoa(value.codLev)], {relativeTo: this.route})
		}
	}

	/**
	 * Seta a filial e empresa para o filtro de fornecedor e natureza de acordo com a filial destino
	 * @param filial selecionada no select 
	 */
	setFilialDest(filial: string = ""){
		let cmbBancoAgeAdaptor = <adaptorFilterParam>this.cmbBancoAgenciaGar.filterParams;
		let cmbFornecedorAdaptor = <adaptorFilterParam>this.cmbFornecedorGar.filterParams;
		let cmbNaturezaAdaptor = <adaptorFilterParam>this.cmbNaturezaGar.filterParams;

		if (this.JuriGenerics.changeUndefinedToEmpty(filial) == "") {
			filial = window.localStorage.getItem('filLog')
		}

		// Definindo a filial para o Banco Agencia
		cmbBancoAgeAdaptor.setTenantFilial(filial);
		this.cmbBancoAgenciaGar.filterParams = cmbBancoAgeAdaptor;
		this.cmbBancoAgenciaGar.updateSelectedValue(undefined);

		// Definindo a filial para o Fornecedor
		cmbFornecedorAdaptor.setTenantFilial(filial);
		this.cmbFornecedorGar.filterParams = cmbFornecedorAdaptor;
		this.cmbFornecedorGar.updateSelectedValue(undefined);
		
		// Definindo a filial para a Natureza
		cmbNaturezaAdaptor.setTenantFilial(filial);
		this.cmbNaturezaGar.filterParams = cmbNaturezaAdaptor;
		this.cmbNaturezaGar.updateSelectedValue(undefined);

	}
}

/**
* objeto do body da JURA098 SEM integração financeira
*/
interface Atualiza {
	id: string;
	operation: number;
	models: [{
		id: string, modeltype: string, fields: [
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string }],
	}];
}

/**
 * objeto do body da JURA098 COM integração financeira
 */
interface AtualizaIntegra {
	id: string;
	operation: number;
	models: [{
		id: string, modeltype: string, fields: [
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string }]
	}];
}
/**
* objeto do body da JURA098 SEM integração financeira
*/
interface InsereGar {
	id: string;
	operation: number;
	models: [{
		id: string, modeltype: string, fields: [
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string }],
	}];
}

/**
 * objeto do body da JURA098 COM integração financeira
 */
interface InsereGarIntegra {
	id: string;
	operation: number;
	models: [{
		id: string, modeltype: string, fields: [
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string }]
	}];
}
