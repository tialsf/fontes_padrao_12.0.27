import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetLevantamentoComponent } from './det-levantamento.component';

describe('DetLevantamentoComponent', () => {
  let component: DetLevantamentoComponent;
  let fixture: ComponentFixture<DetLevantamentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetLevantamentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetLevantamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
