import { Component, OnInit, ViewChild } from '@angular/core';
import { PoI18nService, PoBreadcrumb, PoNotificationService, PoModalComponent, PoModalAction} from '@po-ui/ng-components';
import { RoutingService } from 'src/app/services/routing.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Levantamento } from './levantamento';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FwmodelService } from 'src/app/services/fwmodel.service';
import { LegalprocessService, sysParamStruct, SysProtheusParam } from 'src/app/services/legalprocess.service';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { protheusParam } from 'src/app/services/authlogin.service';
import { FwmodelAdaptorService, adaptorFilterParam } from 'src/app/services/fwmodel-adaptor.service';
import { JurJustifModalComponent } from 'src/app/shared/jur-justif-modal/jur-justif-modal.component';

class dataCombo{
	value: string;
	label: string;
}

@Component({
  selector: 'app-det-levantamento',
  templateUrl: './det-levantamento.component.html',
  styleUrls: ['./det-levantamento.component.css']
})
export class DetLevantamentoComponent implements OnInit {
	litDetLev: any;
	dtLev: any;
	disableInclusaoLev: boolean         = false;
	bShowGroupIntFinanc: boolean        = false;
	bOperacaoInclusao: boolean          = true;
	bLoadingBtnSalvar: boolean          = true;
	isHideLoadingLevantamento : boolean = false;
	isProcEncerrado: boolean            = false;

	formLev: FormGroup;
	saldoGaranAtu: number = 0.0;
	vlrLevTotal:   number = 0.0;
	listEnvolvido: Array<dataCombo> = [];
	sysParams: Array<sysParamStruct> = [];

	pk : string           = '';
	cajuri: string        = '';
	codGar: string        = '';
	codLev: string        = '';
	descLev: string       = '';
	chaveProcesso: string = '';
	lblBtnSalvar : string = '';
	filial: string        = '';
	titleDet: string;
	

	/**
	 * estrutura de navegação da URL atual
	 */
	public breadcrumb: PoBreadcrumb = {
		items: [
		],
	};
	// ações do modal de confirmação de exclusão (Garantia)
	fechar: PoModalAction = {
		action: () => {
		  this.thfModal.close();
			this.excluir.loading = false;
		},
		label: 'Fechar',
	};
	
	excluir: PoModalAction = {
		action: () => {
			this.excluir.loading = true;
		  this.deleteVerify();
		},
		label: 'Excluir',
	};
	
	adaptorParamTipoLevant: adaptorFilterParam = new adaptorFilterParam("JURA024","NQW_TIPO='2'","NQW_DESC","NQW_DESC","","NQW_COD","","Busca tipo levantamento");

	@ViewChild('excluiLevantamento', { static: true }) thfModal: PoModalComponent;
	@ViewChild('modJustif', { static: true }) modalJustif: JurJustifModalComponent;
	constructor(private legalprocess: LegalprocessService,
		private route: ActivatedRoute,
		private routingState: RoutingService,
		private formBuilder: FormBuilder,
		private fwmodel: FwmodelService,
		private JuriGenerics: JurigenericsService,
		private sysProtheusParam: SysProtheusParam,
		protected thfI18nService: PoI18nService,
		public thfNotification: PoNotificationService,
		public fwModelAdaptorService: FwmodelAdaptorService,
		private router: Router) {
			routingState.loadRouting();

			if (this.JuriGenerics.changeUndefinedToEmpty(this.route.snapshot.paramMap.get("codGar"))) {
				this.codGar = atob(decodeURIComponent(this.route.snapshot.paramMap.get("codGar")));
			}

			if (this.JuriGenerics.changeUndefinedToEmpty(this.route.snapshot.paramMap.get("codLev"))) {
				this.codLev = atob(decodeURIComponent(this.route.snapshot.paramMap.get("codLev")));
			}

			this.filial = this.route.snapshot.paramMap.get('filPro');
			window.sessionStorage.setItem('filial', atob(this.filial));
			
			this.chaveProcesso = this.route.snapshot.paramMap.get('codPro');
			this.cajuri = atob(decodeURIComponent(this.chaveProcesso));

			//-- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'levantamentos' no arquivo JurTraducao.
			thfI18nService.getLiterals({ context: 'detLevantamentos', language: thfI18nService.getLanguage() })
			.subscribe((litLev) => {
				this.litDetLev = litLev;

				if (this.JuriGenerics.changeUndefinedToEmpty(this.codLev) == ""){
					this.lblBtnSalvar = this.litDetLev.titleBtn.incluir
					this.breadcrumb.items =[
						{ label: this.litDetLev.bread.home, link: '/' },
						{ label: this.litDetLev.bread.processo, link: '/processo/' + this.filial + '/' + this.chaveProcesso},
						{ label: this.litDetLev.bread.garantias, link: '/processo/' + this.filial + '/' + this.chaveProcesso + '/garantia'},
						{ label: this.litDetLev.bread.garantia, link: '/processo/' + this.filial + '/' + this.chaveProcesso + '/garantia/' + btoa(this.codGar)},
						{ label: this.litDetLev.bread.novoLev}
					];
					this.titleDet = this.litDetLev.bread.novoLev;
				}else{
					this.breadcrumb.items = [
						{ label: this.litDetLev.bread.home, link: '/' },
						{ label: this.litDetLev.bread.processo, link: '/processo/' + this.filial + '/' + this.chaveProcesso},
						{ label: this.litDetLev.bread.garantias, link: '/processo/' + this.filial + '/' + this.chaveProcesso + '/garantia'},
						{ label: this.litDetLev.bread.garantia, link: '/processo/' + this.filial + '/' + this.chaveProcesso + '/garantia/' + btoa(this.codGar)},
						{ label: this.descLev}
					];
					this.lblBtnSalvar = this.litDetLev.titleBtn.alterar
					this.titleDet = this.litDetLev.tituloAlterarLev;
				}

			})

	}

	ngOnInit() {
		var frmLevantamento: Levantamento = new Levantamento();
		this.restore();
		this.getInfoProcesso();
		this.getInfoGarantia();
		this.createFormLev(frmLevantamento);

		// Busca os parâmetros do Protheus
		this.sysProtheusParam.getParamByArray(["MV_JTVENJF","MV_JINTVAL"]).subscribe((params)=>{
			this.sysParams = params;

			if (this.JuriGenerics.findValueByName(this.sysParams,'MV_JINTVAL') == "1"){
				this.bShowGroupIntFinanc = true;
			}
			this.getLevantamento();			
		})
	}

	/**
	 * Cria o formBuilder de Levantamento
	 * @param levantamento 
	 */
	createFormLev(levantamento: Levantamento){
		this.formLev = this.formBuilder.group({
			tipoLev:          [ levantamento.tipoLev, Validators.compose([Validators.required]) ],
			dataValor:        [ this.dtLev, Validators.compose([Validators.required]) ],
			envolvido:        [ levantamento.envolvido ],
			vlrLevPrincipal:  [ levantamento.vlrLevPrincipal, Validators.compose([Validators.required]) ],
			vlrLevCorrecao:   [ levantamento.vlrLevCorrecao ],
			vlrLevJuros:      [ levantamento.vlrLevJuros ],
			vlrLevAjCorrecao: [ levantamento.vlrLevAjCorrecao ],
			vlrLevAjJuros:    [ levantamento.vlrLevAjJuros ],
			saldo:            [ this.saldoGaranAtu ],
			vlrLevTotal:      [ this.vlrLevTotal ]
		})
	}

	/**
	 * Busca as informações do Processo
	 */
	getInfoProcesso(){
		this.fwmodel.restore()
		this.fwmodel.setModelo("JURA095")
		this.fwmodel.setFilter("NSZ_FILIAL='" + atob(this.filial) + "'");
		this.fwmodel.setFilter("NSZ_COD='" + this.cajuri + "' ")
		this.fwmodel.setCampoVirtual(true)
		this.fwmodel.get("getInfoProcesso").subscribe(data=>{
			if (data.resources != ""){
				this.breadcrumb.items[1].label = this.litDetLev.bread.processo + " " + this.JuriGenerics.findValueByName(data.resources[0].models[0].fields,"NSZ_DAREAJ")
				this.isProcEncerrado = (this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NSZ_SITUAC") == "2")
			}
		})
	}

	/**
	 * Busca as informações da Garantia
	 */
	getInfoGarantia(){
		this.isHideLoadingLevantamento = false;
		this.fwmodel.restore()
		this.fwmodel.setModelo("JURA098");
		this.fwmodel.setFilter("NT2_CAJURI='" + this.cajuri + "'");
		this.fwmodel.setFilter("NT2_COD = '" + this.codGar + "'");
		this.fwmodel.setCampoVirtual(true)
		this.fwmodel.get("getInfoGarantia").subscribe(data=>{
			if (data.resources != "" && data.resources != undefined){
				this.breadcrumb.items[3].label = this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT2_DATA"), "dd/mm/yyyy")
				this.getTipoGar(this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT2_CTPGAR"))
				this.saldoGaranAtu = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT2__GASJA")
			}
			if(this.JuriGenerics.changeUndefinedToZero(this.saldoGaranAtu) > 0 ){
				this.formLev.patchValue({
					saldo: this.saldoGaranAtu
				})
	
			}
			this.isHideLoadingLevantamento = true;
		})

	}

	/**
	 * Responsável por obter a lista dos tipos de garantias
	 * @param cCodTipo código do tipo de garantias
	 */
	getTipoGar(cCodTipo ?: string) {
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA024");
		this.fwmodel.setFilter("NQW_TIPO='1'")
		this.fwmodel.get('Tipo de Garantia').subscribe((data) => {
			if(data != '' && data != undefined){
				data.resources.forEach(item => {
					if (this.JuriGenerics.findValueByName(item.models[0].fields, "NQW_COD") == cCodTipo) {
						let descTpGar = this.JuriGenerics.findValueByName(item.models[0].fields, "NQW_DESC");
						this.breadcrumb.items[3].label = descTpGar + ' - ' + this.breadcrumb.items[3].label ;
					}
				})
			}
		})
	}

	/**
	 * Cancela operação
	 */
	onCancel(){
		this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codGar))],{relativeTo: this.route})
	}

	/**
	 * Restaura as variáveis
	 */ 
	restore(){
		this.dtLev='';
	}

	/**
	 * Responsável por obter a lista dos tipos de levantamento
	 * @param cCodEnvo 
	 */
	getEnvolvidos(cCodEnvo ?: string) {
		
		this.legalprocess.restore();
		this.legalprocess.get('process/' +this.cajuri,"Lista de Envolvidos").subscribe((data) => {
			if(data != '' && data != undefined){
				data.processes[0].party.forEach(item => {

					let dadosEnvolvidos = new dataCombo();
					dadosEnvolvidos.label= item.name,
					dadosEnvolvidos.value= item.id
					
					this.listEnvolvido.push(dadosEnvolvidos)
				})
				
			}
			if (cCodEnvo != undefined){
				this.formLev.patchValue({
					envolvido: [cCodEnvo]
				})
			}
		});
	}

	bodyLevantamento(codGar: string, levantamento: Levantamento){
		let dataLev = levantamento.dataValor.replace(/-/g, '');
		let intFin: string= "2";
		//para integração financeira
		// if (this.bShowGroupIntFinanc){
		// 	intFin = "1"
		// }
		const bodyInsereLev: StructLev = {
			id: 'JURA098', operation: 1, models: [
				{
					id: 'NT2MASTER', modeltype: 'FIELDS', fields: [
						{ id: 'NT2_CAJURI', order: 1, value: this.cajuri },
						{ id: 'NT2_CTPGAR', order: 2, value: levantamento.tipoLev },
						{ id: 'NT2_DATA'  , order: 3, value: dataLev },
						{ id: 'NT2__LEVTP', order: 4, value: '1' },
						{ id: 'NT2_PRAZO' , order: 5, value: '2' },
						{ id: 'NT2_CENVOL', order: 6, value: levantamento.envolvido },
						{ id: 'NT2_CMOEDA', order: 7, value: '01' },
						{ id: 'NT2_CGARAN', order: 8, value: codGar},
						{ id: 'NT2__VALOR', order: 9, value: this.JuriGenerics.changeUndefinedToZero(levantamento.vlrLevPrincipal).toString() },
						{ id: 'NT2__VCPRO', order: 10, value: this.JuriGenerics.changeUndefinedToZero(levantamento.vlrLevCorrecao).toString() },
						{ id: 'NT2__VJPRO', order: 11, value: this.JuriGenerics.changeUndefinedToZero(levantamento.vlrLevJuros).toString() },
						{ id: 'NT2_AJUCOR', order: 12, value: this.JuriGenerics.changeUndefinedToZero(levantamento.vlrLevAjCorrecao).toString() },
						{ id: 'NT2_AJUJUR', order: 13, value: this.JuriGenerics.changeUndefinedToZero(levantamento.vlrLevAjJuros).toString() },
						{ id: 'NT2_INTFIN', order: 14, value: intFin}
						
					]
				}
			]
		};
		return JSON.stringify(bodyInsereLev);
	}
	/**
	 * Faz a soma dos valores para trazer o valor total do levantamento a ser feito
	 * '+' ao lado do valor o transforma em number
	 */
	atualizaVlrLev(){
		let form = this.formLev.value
		this.vlrLevTotal = +form.vlrLevPrincipal + +form.vlrLevAjCorrecao + +form.vlrLevAjJuros +
						   +form.vlrLevCorrecao + +form.vlrLevJuros

		this.formLev.patchValue({
			vlrLevTotal: this.vlrLevTotal
		})
	}

	/**
	 * Busca dados do levantamento
	 */
	getLevantamento(){
		this.isHideLoadingLevantamento = false;
		if (this.JuriGenerics.changeUndefinedToEmpty(this.codLev) == "") {
			this.getEnvolvidos();
			this.bOperacaoInclusao = true;
			this.isHideLoadingLevantamento = true;
			this.bLoadingBtnSalvar = false;
		} else {
			this.fwmodel.restore();
			this.fwmodel.setModelo("JURA098");
			this.fwmodel.setCampoVirtual(true);
			this.fwmodel.setFilter("NT2_CAJURI='" + this.cajuri + "'"); 
			this.fwmodel.setFilter("NT2_FILIAL='" + atob(this.filial) + "'"); 
			this.fwmodel.setFilter("NT2_COD='" + this.codLev + "'" );
			
			this.fwmodel.get('Obtem dados do levantamento').subscribe((data) => {
				if (data.count == 0) {
					this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codGar))], { relativeTo: this.route });
					this.thfNotification.error(this.litDetLev.naoEncontrado);
				} else {
					this.pk = data.resources[0].pk;
					this.cajuri = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, 'NT2_CAJURI');
					
					this.getEnvolvidos(this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT2_CENVOL"));
					
					this.formLev.patchValue({
						tipoLev:         [this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT2_CTPGAR")],
						dataValor:       this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT2_DATA"),"yyyy-mm-dd"),
						vlrLevPrincipal: [this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT2__VALOR")],
						vlrLevCorrecao:  [this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT2__VCPRO")],
						vlrLevJuros:     [this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT2__VJPRO")],
						vlrLevAjCorrecao:[this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT2_AJUCOR")],
						vlrLevAjJuros:   [this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT2_AJUJUR")],
						saldo:           [this.saldoGaranAtu]			
					});
					this.breadcrumb.items[4].label = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT2_DTPGAR") + ' - ' + this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NT2_DATA"), "dd/mm/yyyy");
					this.atualizaVlrLev();	
					this.isHideLoadingLevantamento = true;
					this.bOperacaoInclusao = false;
					this.bLoadingBtnSalvar = false;
				}
			});
		}

	}

	/**
	 * Função de inicio das validações e Commit.
	 */
	confirmVerify(){
		if (this.beforeSubmit()){
			this.submitLevantamento()
		}
	}

	/**
	 * Função de validação e delete.
	 */
	deleteVerify(){
		if (this.beforeDelete()){
			this.deleteLev()
		}
	}
	
	/**
	 * Pré-validação.
	 */
	beforeSubmit(){
		let isSubmitable: boolean = false;
		isSubmitable = this.formLev.valid
		
		// Validação para apresentar o Modal de Justificativa, caso o processo esteja encerrado
		if (this.isProcEncerrado && this.sysProtheusParam.getParamValueByName(this.sysParams,"MV_JTVENJF") == "1"){
			isSubmitable = false;
			this.modalJustif.openModal(this.cajuri,
				()=>{
					if (this.modalJustif.isOk){
						this.submitLevantamento()
					}
				});
		} else {
			isSubmitable = true;
		}

		return isSubmitable
	}

	/**
	 * Pré-validação.
	 */
	beforeDelete(){
		let isSubmitable: boolean = false;
		isSubmitable = this.formLev.valid
		
		// Validação para apresentar o Modal de Justificativa, caso o processo esteja encerrado
		if (this.isProcEncerrado && this.sysProtheusParam.getParamValueByName(this.sysParams,"MV_JTVENJF") == "1"){
			isSubmitable = false;
			this.modalJustif.openModal(this.cajuri,
				()=>{
					if (this.modalJustif.isOk){
						this.deleteLev()
					}
				});
		} else {
			isSubmitable = true;
		}

		return isSubmitable
	}

	/**
	* Obtem os dados do formulário no momento da inclusão e passa como parâmetro
	* para o postGarantia. Esses dados serão usados no body da requisisão
	* para realizar a inclusão através do FWModel.
	*/
	submitLevantamento() {
		this.bLoadingBtnSalvar = true;
		if (this.JuriGenerics.changeUndefinedToEmpty(this.codLev) == "") {
			this.insertLevantamento();
		} else {
			this.updateLevantamento();
		}
	}

	insertLevantamento(){
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA098');
		this.fwmodel.post(this.bodyLevantamento(this.codGar,this.formLev.value),"Inclusão de Levantamento").subscribe((data) => {
			if ( this.JuriGenerics.changeUndefinedToEmpty(data) != '') {
				if (data.pk.length > 0) {
					this.thfNotification.success(this.litDetLev.msg.sucessIncl);
					this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codGar))], { relativeTo: this.route });
				}
			}
			this.bLoadingBtnSalvar = false;
		});
	}

	updateLevantamento() {

		this.bLoadingBtnSalvar = true;

		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA098');
		this.fwmodel.put(this.pk, this.bodyLevantamento(this.codGar,this.formLev.value), 'Alteração do Levantamento').subscribe((data) => {
			if ( this.JuriGenerics.changeUndefinedToEmpty(data) != '') {
				this.thfNotification.success(this.litDetLev.msg.sucessUpd);
				this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codGar))], { relativeTo: this.route });
				
			}
			this.bLoadingBtnSalvar = false;
		});
	}
	/**
	 * Responsavel por remover um levantamento 
	 */
	deleteLev() {
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA098');

		this.fwmodel.delete(this.pk, 'Deleção do levantamento').subscribe((data) => {
			if (data == true) {
				this.thfModal.close();
				this.thfNotification.success(this.litDetLev.msg.sucessExcl);
				this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codGar))], { relativeTo: this.route });
				
			} else {
				this.excluir.loading = false;
			}
		});
	}

}
/**
* objeto do body da JURA098 SEM integração financeira
*/
interface StructLev {
	id: string;
	operation: number;
	models: [{
		id: string, modeltype: string, fields: [
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string },
			{ id: string, order: number, value: string }],
	}];
}