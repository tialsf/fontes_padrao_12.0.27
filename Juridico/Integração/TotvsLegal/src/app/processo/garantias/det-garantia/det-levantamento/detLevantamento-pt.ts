/**
* Arquivo de constantes para tradução da página detalhe do levantamento
*/

//-- Constantes para o bread crumb
const breadCrumb = {
	home     : "Meus processos",
	processo : "Meu processo",
	garantias: "Garantias",
	garantia : "Garantia",
	novoLev  : "Novo levantamento",
	levantamento: "Levantamento"
}

const tituloBotoes = {
	incluir : "Incluir levantamento",
	alterar : "Alterar levantamento",
	excluir : "Excluir levantamento",
	cancelar: "Cancelar",
}

const messages ={
	sucessIncl: "Levantamento incluído com sucesso!",
	sucessExcl: "Levantamento excluído com sucesso!",
	sucessUpd : 'Levantamento alterado com sucesso!',
}

//-- Constantes principais
export const detLevantamentosPt = {
	tituloLev  : "Levantamentos",
	tipo       : "Tipo",
	data       : "Data",
	responsavel: "Responsável",
	valor      : "Valor (R$)",
	btnNovoLev : "Novo levantamento",
	loadLev    : "Carregando levantamento",
	novoLev    : 'Novo levantamento',
	tipLev     : 'Tipo',
	dtLev      : 'Data',
	envLev     : 'Envolvido',
	vlrLev     : 'Valor (R$)',
	vlrCorreLev: 'Valor de Correção (R$)',
	vlrJurosLev: 'Valor de Juros (R$)',
	vlrAjuCorre: 'Ajuste Correção (R$)',
	vlrAjJur   : 'Ajuste Juros (R$)',
	saldoDisp  : 'Saldo disponível',
	vlrLevAtu  : 'Valor levantamento',
	atencao    : "Atenção!",
	tituloAlterarLev: "Alterar levantamento",
	confirmExclusao : "Tem certeza que deseja excluir este levantamento?",
	msg: messages,
	bread: breadCrumb,
	titleBtn: tituloBotoes
}