//--- Modelo de Dados para inclusão de Levantamento

export class Levantamento {
	tipoLev :         string = '';
	dataValor :       string = '';
    envolvido:        string = '';    
    vlrLevPrincipal:  number;
    vlrLevCorrecao:   number;
	vlrLevJuros:      number;
	vlrLevAjCorrecao: number;
	vlrLevAjJuros:    number;
	saldo:            number;
	vlrLevTotal:      number;

}
