/**
* Arquivo de constantes para tradução da página detalhe do andamento
*/
const componentMessages = {
	msgCamposIntegFinanceiro: "Preencha os campos prefixo, fornecedor, natureza e titulo.",
	msgCamposPadrao: "Preencha os campos de tipo, data e valor."
}
//-- Constantes para o bread crumb
const breadCrumb = {
	home: "Meus processos",
	processo: "Processo",
	garantias: "Garantias",
	novaGarantia: "Nova garantia",
	meuProcesso: "Meu processo"
}

//-- Constantes para o levantamento
const detLevantamentos = {
	tituloLev: "Levantamentos",
	tipo: "Tipo",
	data: "Data",
	responsavel: "Responsável",
	valor: "Valor (R$)",
	btnNovoLev: "Novo levantamento",
	loadLev: "Carregando levantamentos"
}

//-- Constantes para o Integração com Financeiro
const integraFinaneiro = {
    integrafinanceiro : "Integra Financeiro",
    integracaofinanceira: "Integração Financeira",
    prefixo: "Prefixo",
    natureza: "Natureza",
    titulo: "Titulo",
    grpaprov: "Grupo de Aprovação",
    bancoagencia: "Banco/Agência",
	fornecedor: "Fornecedor",
	FiliDest: "Filial destino"
}
//-- Constantes principais
export const detGarantiasPt = {
	tituloNovaGar: "Nova garantia",
	tituloAlterarGar: "Alterar garantia",
	inputTipo: "Tipo",
	inputData: "Data",
	inputEnvolvidos: "Envolvidos",
	inputCorrecao: "Correção",
	inputValor: "Valor (R$)",
	inputLevantamento: "Levantamento (R$)",
	inputSJuizo: "Saldo em juízo (R$)",
	msgInclusao: "Garantia incluída com sucesso!",
	msgExclusao: "Garantia excluída com sucesso!",
	confirmExclusao: "Tem certeza que deseja excluir esta garantia?",
	verTodosAnexos: "Ver todos os anexos",
	selecionarArquivo: "Selecionar arquivo",
	btnIncluirGarantia:    'Incluir garantia',
	btnSalvarGarantia:     'Salvar garantia',
	btnExcluir: "Excluir garantia",
	btnCancelar: "Cancelar",
	novoLev:     'Novo levantamento',
	tipLev:      'Tipo:',
	dtLev:       'Data:',
	envLev:      'Envolvido:',
	vlrLev:      'Valor (R$)',
	vlrCorreLev: 'Valor de Correção (R$)',
	vlrJurosLev: 'Valor de Juros (R$)',
	vlrAjuCorre: 'Ajuste Correção (R$)',
	vlrAjJur:    'Ajuste Juros (R$)',
	sucessLev:   'Levantamento incluído com Sucesso!',
	errorLev:    'Não foi possível incluir o levantamento. Verifique as informações dos campos obrigatórios e/ou as informações deste do Processo.',
	sucessUpdate: 'Registro alterado com Sucesso!',
	loadGar:      'Carregando garantia',
	bread: breadCrumb,
	levantamentos: detLevantamentos,
	integraFinaneiro: integraFinaneiro,
	messages: componentMessages
}