export class Garantia {
	tipo              : string = '';
	dataGarantia      : string = '';
	tipoMov           : string = '';
	envolvido         : string = '';
	correcao          : string = '';
	valor             : string = '';
	levantamento      : string = '';
	sJuiza            : string = '';
	integraFinanceiro : string = '2';
	intFinPrefixo     : string = '';
	intFinNatureza    : string = '';
	intFinTitulo      : string = '';
	intFinBancoAgencia: string = '';
	intFinFornecedor  : string = '';
	intFinGrupoAprovac: string = '';
	intFinFiliDest    : string = '';
}