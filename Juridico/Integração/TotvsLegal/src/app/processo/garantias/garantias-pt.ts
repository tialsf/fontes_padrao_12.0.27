/**
* Arquivo de constantes para tradução de Garantias - Português
*/

//-- Constantes para títulos das colunas do Grid de Garantias
export const gridGarantias = {
	
	colunaTipo: 			"Tipo",
	colunaData: 			"Data", 
	colunaValor: 			"Valor (R$)",
	colunaLevantamento:		"Levantamento (R$)",
	colunaSaldoEmJuizo: 	"Saldo em juízo (R$)",
	colunaTipoMovimentacao: "Tipo de movimentação"

}


//-- Constantes para o bread crumb
export const breadCrumb = {
	home: 'Meus processos',
	processo: 'Meu processo',
	garantias: 'Garantias'
}


//-- Constantes para o componente de Garantias
export const garantiasPt = {
		
	btnPesquisar: 	 "Pesquisar",
	btnNovoGar: 	 "Nova garantia",
	loadGar: 		 "Carregando garantias",
	tituloGar: 		 "Garantias",
	RemoverGar: 	 "Deseja remover garantia?",
	garExcluido: 	 "Garantia excluído com sucesso.",
	btnAnterior: 	 "<<  Anterior",
	btnProximo: 	 "Próximo  >>",
	finalPagina:	 "Ops! Não há mais registros para serem exibidos.",
	WgtTotDepositos: "Total de depositos",
	WgtTotLevant:    "Total de levantamentos",
	WgtTotSalJuizo:  "Saldo total em juízo",
	gridGarantia: gridGarantias,
	breadCrumb: breadCrumb
}



