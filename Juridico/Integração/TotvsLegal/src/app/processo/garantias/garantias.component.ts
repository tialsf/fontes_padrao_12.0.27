import { Component, OnInit } from '@angular/core';
import { PoTableColumn, PoDialogConfirmLiterals, PoBreadcrumb, PoNotificationService, PoI18nService } from '@po-ui/ng-components';
import { FwmodelService } from 'src/app/services/fwmodel.service';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RoutingService } from 'src/app/services/routing.service';
import { FormGroup } from '@angular/forms';
import { adaptorFilterParam, FwmodelAdaptorService } from 'src/app/services/fwmodel-adaptor.service';

class DadosTipo {
	value: string;
	label: string;
}
class Garantias{
	codGar: string;
	tipo : string;
	data : string;
	valor: string;
	levantamento: string;
	saldoemjuizo: string;
	gar         : Array<string>;

}

@Component({
	selector: 'app-garantias',
	templateUrl: './garantias.component.html',
	styleUrls: ['./garantias.component.css']
})
export class GarantiasComponent implements OnInit {

	cajuri: string = '';
	page: number = 0;
	searchKey: string = '';
	hasData: boolean = true;
	length: number = 0;
	pageSize: number = 300;
	maxPage: number = 1;
	btnPreDisable: boolean = false;
	btnNxtDisable: boolean = false;
	isHideLoadingGarantias: boolean = false;
	litGar: any;
	dateStr: string;
	literalsConfirm: PoDialogConfirmLiterals;
	message: string;
	title: string;
	dialogMethod: string;
	literals: string;
	actionOptions: Array<string> = ['confirm', 'cancel'];
	hasPage: boolean = true;
	lastPage: number = 1;
	totalDepositos: number = 0;
	totalLevantamentos: number = 0;
	saldoTotalJuizo: number = 0;
	dadosTipo: DadosTipo = new DadosTipo();
	descGar: string = '';
	tipos: Array<any> = [];
	formA: FormGroup;
	event: string;
	chaveProcesso: string = '';
	filial: string = '';
	garantiasList: Array<Garantias>;

	colGarantias: Array<PoTableColumn> = [
		{ property: 'tipo', label: ' ' },
		{ property: 'data', label: 'Data' },
		{ property: 'valor', label: '', type: 'number', format: '1.2-5' },
		{ property: 'levantamento', label: 'Levantamento (R$)', type: 'number', format: '1.2-5' },
		{ property: 'saldoemjuizo', label: 'Saldo em juízo (R$)', type: 'number', format: '1.2-5' },
		{ property: 'gar', label: ' ', type: 'icon',
			icons: [
				{ value: 'garantia', icon: 'po-icon po-icon-edit', color: '#29b6c5', tooltip: 'Editar', action:(value)=>{
					this.callDetailGarantia(value);
					},
				},
				{ value: 'anexos', icon: 'po-icon po-icon-document-filled', color: '#29b6c5', tooltip: 'Anexos', action:(value)=>{
					this.callAnexos(value);
					},
				}
			]
		}
	];

	// Adapter do Tipo de Garantia
	adaptorParamTipoGarantia:
	adaptorFilterParam = new adaptorFilterParam('JURA024', "NQW_TIPO='1'", 'NQW_DESC', 'NQW_DESC', '', 'NQW_COD', '', 'getTipoGarantia');

	/**
	 * estrutura de navegação da URL atual
	 */
	public breadcrumb: PoBreadcrumb = {
		items: [ ]
	};

	constructor(
		protected thfI18nService: PoI18nService,
		protected thfNotification: PoNotificationService,
		public fwModelAdaptorService: FwmodelAdaptorService,
		private JuriGenerics: JurigenericsService,
		private fwmodel: FwmodelService,
		private route: ActivatedRoute,
		private routingState: RoutingService,
		private router: Router) {

		this.routingState.loadRouting();

		this.filial = this.route.snapshot.paramMap.get('filPro');
		window.sessionStorage.setItem('filial', atob(this.filial));
		
		this.chaveProcesso = this.route.snapshot.paramMap.get('codPro');
		this.cajuri = atob(decodeURIComponent(this.chaveProcesso));

		// Chamada do serviço poI18n para buscar as constantes a partir do contexto 'gar' no arquivo JurTraducao.

		thfI18nService.getLiterals({ context: 'garantias', language: thfI18nService.getLanguage() })
			.subscribe((litGar) => {

				this.litGar = litGar;

				this.breadcrumb.items = [
					{ label: this.litGar.breadCrumb.home, link: '/' },
					{ label: this.litGar.breadCrumb.processo, link: '/processo/' + this.filial + '/' + this.chaveProcesso },
					{ label: this.litGar.breadCrumb.garantias }
				];

				// seta as constantes para tradução do titulo das colunas do Grid de Garantias
				this.colGarantias[0].label = this.litGar.gridGarantia.colunaTipo;
				this.colGarantias[1].label = this.litGar.gridGarantia.colunaData;
				this.colGarantias[2].label = this.litGar.gridGarantia.colunaValor;
				this.colGarantias[3].label = this.litGar.gridGarantia.colunaLevantamento;
				this.colGarantias[4].label = this.litGar.gridGarantia.colunaSaldoEmJuizo;

			});
	}

	ngOnInit() {
		this.getGarantias();
		this.getTipoGar();
		this.getInfoProcesso();
		this.searchKey = '';
	}

	/**
 * Busca as informações do processo
 */
	getInfoProcesso() {
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA095');
		this.fwmodel.setFilter("NSZ_COD = '" + this.cajuri + "'");
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.get('getInfoProcesso').subscribe(data => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != '') {
				this.breadcrumb.items[1].label = this.litGar.breadCrumb.processo + ' ' +
				this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, 'NSZ_DAREAJ');
			}
		});
	}

	/**
	 * Busca as garantias para a tela
	 */
	getGarantias() {
		this.isHideLoadingGarantias = false;
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA098');
		this.fwmodel.setCampoVazio(true);
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.setPageSize(this.pageSize.toString()); // quantidade de itens por página

		this.fwmodel.setFilter("NT2_FILIAL = '" + atob(this.filial) + "'" );
		this.fwmodel.setFilter("NT2_CAJURI = '" + this.cajuri + "'");
		this.fwmodel.setFilter("NT2_MOVFIN = '1'");

		if (this.searchKey != '' && this.searchKey != undefined) {
			this.fwmodel.setSearchKey("NT2_CTPGAR = '" + this.searchKey + "'");
		}
		this.garantiasList = [];
		this.fwmodel.get('Lista de Garantias').subscribe((data) => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != '') {
				data.resources.forEach(item => {
					let itemGar = new Garantias();
					itemGar.codGar = this.JuriGenerics.findValueByName(item.models[0].fields, 'NT2_COD');
					itemGar.tipo = this.JuriGenerics.findValueByName(item.models[0].fields, 'NT2_DTPGAR');
					itemGar.valor = this.JuriGenerics.findValueByName(item.models[0].fields, 'NT2_VALOR');
					itemGar.levantamento = this.JuriGenerics.findValueByName(item.models[0].fields, 'NT2__LEVSD');
					itemGar.saldoemjuizo = this.JuriGenerics.findValueByName(item.models[0].fields, 'NT2__GASJ');
					itemGar.data = this.JuriGenerics.makeDate(this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(item.models[0].fields, 'NT2_DATA')), 'dd/mm/yyyy');
					itemGar.gar = ['garantia'];

					if (this.JuriGenerics.findValueByName(item.models[0].fields, 'NT2__TEMANX') == '01') {
						itemGar.gar = ['garantia','anexos'];
					}

					this.garantiasList.push(itemGar);
				});
			}
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != '') {
				this.totalDepositos = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, 'NT2_SGARA');
				this.totalLevantamentos = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, 'NT2_SAL');
				this.saldoTotalJuizo = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, 'NT2_SJUIZA');
			}

			
			this.isHideLoadingGarantias = true;

		});

	}

	/**
	 * Responsável por obter a lista dos tipos de garantias para preencher o combo
	 */
	getTipoGar() {
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA024');
		this.fwmodel.setFilter("NQW_TIPO = '1'");

		this.fwmodel.get('Busca tipo de Garantia').subscribe((data) => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != '') {
				data.resources.forEach(item => {

					this.dadosTipo = new DadosTipo();
					this.dadosTipo.label = item.models[0].fields[1].value;
					this.dadosTipo.value = item.models[0].fields[0].value;

					this.tipos.push(this.dadosTipo);
				});
			}
		});
	}

	/**
	 * Responsável por filtrar a garantia de acordo com o tipo
	 *
	 */
	filtraTipo() {
		this.getGarantias();
	}

	/**
	 * Realiza o detalhe da Garantia
	 * @param value
	 */
	callDetailGarantia(value?: any) {
		if (value == undefined) {
			this.router.navigate(['./cadastro'], { relativeTo: this.route });
		} else {
			this.router.navigate(['./' + btoa(value.codGar)], { relativeTo: this.route });
		}
	}

	/**
	 * Abre a tela de anexos
	 */
	callAnexos(value?: any){
		this.router.navigate(['./' + btoa(value.codGar) + '/anexos'], { relativeTo: this.route });
	}
}
