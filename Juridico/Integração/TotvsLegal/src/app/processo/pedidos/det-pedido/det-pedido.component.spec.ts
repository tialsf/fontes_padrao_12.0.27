import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetPedidoComponent } from './det-pedido.component';

describe('DetPedidoComponent', () => {
  let component: DetPedidoComponent;
  let fixture: ComponentFixture<DetPedidoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetPedidoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetPedidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});