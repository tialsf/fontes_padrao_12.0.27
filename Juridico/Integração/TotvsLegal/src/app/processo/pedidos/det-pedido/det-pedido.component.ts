import { Component, OnInit, ViewChild } from '@angular/core';
import {
	PoBreadcrumb,
	PoNotificationService,
	PoModalAction,
	PoI18nService,
	PoModalComponent,
	PoUploadComponent,
	PoI18nPipe,
	PoInputComponent,
} from '@po-ui/ng-components';
import {
	FwmodelService,
	FwModelBody,
	FwModelBodyModels,
	FwModelBodyItems,
} from 'src/app/services/fwmodel.service';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RoutingService } from 'src/app/services/routing.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Pedido } from './pedido';
import {
	adaptorFilterParam,
	FwmodelAdaptorService,
} from 'src/app/services/fwmodel-adaptor.service';
import { flatMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { JurJustifModalComponent } from 'src/app/shared/jur-justif-modal/jur-justif-modal.component';
import { SysProtheusParam, sysParamStruct } from 'src/app/services/legalprocess.service';
import { HttpParams } from '@angular/common/http';
import { TpPedidoAdapterService } from 'src/app/services/combos-fwmodel-adapter.service';
import { FCorrecaoAdapterService } from 'src/app/services/combos-fwmodel-adapter.service';

enum tipoTratamentoMigr {
	valorPedidoZerado = 1,
	valorASerDistribuido = 2,
}

@Component({
	selector: 'app-det-pedido',
	templateUrl: './det-pedido.component.html',
	styleUrls: ['./det-pedido.component.css'],
})
export class DetPedidoComponent implements OnInit {
	private valorPossivelOriginal: number = 0;
	private valorPedidoOriginal: number = 0;
	private valorTotalPrognosticos: number = 0;
	private arrPedidosMigracao: string[] = [];
	private nQtdPedidosParaExcluidos: number = 0;
	private cTipoTratamentoMigracao: tipoTratamentoMigr = tipoTratamentoMigr.valorASerDistribuido;
	formPedido: FormGroup;

	litDetPed: any;
	dataPedidoVlr: any = '';
	titleDet: string;
	pk: string = '';
	cajuri: string = '';
	codPed: string = '';
	codO0W: string = '';
	codTipPedido: string = '';
	filtroCorrecao: string = "NW7_VISIV='1'";
	btnIncluirLabel: string = 'Incluir pedido';
	prognostico: string = '';
	chaveProcesso: string = '';
	filial: string = '';
	cajuriTitulo: string = '';

	nIndiceRegistro: number = 0;
	vlrPossivelAtu: number;
	uploadCount: number = 0;

	aQtdPedidos: Array<any> = [];
	listCorrecao: Array<any> = [];
	sysParams: Array<sysParamStruct> = [];

	isProcEncerrado: boolean = false;
	isHideLoadingPedido: boolean = false;
	isHideLoadingInclusao: boolean = false;
	isHideLoadingExclusao: boolean = false;
	isHideVlrRedutor: boolean = false;
	isCorrMultaDisabled: boolean = false;
	isMinimizeDisabled: boolean = false;
	isClearBtnDisabled: boolean = true;
	btnIncluirEnabled: boolean = false;
	btnExcluirDisabled: boolean = true;
	bHasValorDistribuir: boolean = false;
	disabledMulta: boolean = true;
	mostraAvancado: boolean = true;
	toggleAvancado: boolean = false;

	// ações do modal de confirmação de exclusão (Pedido)
	fechar: PoModalAction = {
		action: () => {
			this.thfModal.close();
		},
		label: 'Fechar',
	};

	excluir: PoModalAction = {
		action: () => {
			this.excluir.loading = true;
			this.deleteVerify();
		},
		label: 'Excluir',
	};

	limpar: PoModalAction = {
		action: () => {
			this.onClearAvanc();
		},
		label: 'Limpar',
	};

	/**
	 * estrutura de navegação da URL atual
	 */
	public breadcrumb: PoBreadcrumb = {
		items: [],
	};

	@ViewChild(PoModalComponent, { static: true }) thfModal: PoModalComponent;
	@ViewChild('modalClearAvc', { static: true }) clearModal: PoModalComponent;
	@ViewChild('docUpload') docUpload: PoUploadComponent;
	@ViewChild('valorPedido') inpValorPedido: PoInputComponent;
	@ViewChild('modJustif', { static: true })
	modalJustif: JurJustifModalComponent;

	constructor(
		public fwModelAdaptorService: FwmodelAdaptorService,
		public poI18nPipe: PoI18nPipe,
		protected thfI18nService: PoI18nService,
		private fwmodel: FwmodelService,
		private formBuilder: FormBuilder,
		private thfNotification: PoNotificationService,
		private route: ActivatedRoute,
		private JuriGenerics: JurigenericsService,
		private routingState: RoutingService,
		private sysProtheusParam: SysProtheusParam,
		private router: Router,
		public tpPedidoAdapterService: TpPedidoAdapterService,
		public fCorrecaoAdapterService: FCorrecaoAdapterService
	) {
		// Setup de adapter
		this.tpPedidoAdapterService.setup(
			new adaptorFilterParam(
				'JURA085',
				'',
				'NSP_DESC',
				'NSP_DESC',
				'',
				'NSP_COD',
				'',
				'getTipoPedido',
				undefined,
				new HttpParams().append('fields', 'NSP_COD,NSP_DESC')
			)
		);
		this.fCorrecaoAdapterService.setup(
			new adaptorFilterParam(
				'JURA061',
				"NW7_VISIV='1'",
				'NW7_DESC',
				'NW7_DESC',
				'',
				'NW7_COD',
				'',
				'getFormaCorrecao',
				undefined,
				new HttpParams().append('fields', 'NW7_COD,NW7_DESC')
			)
		);

		this.routingState.loadRouting();

		if (this.route.snapshot.paramMap.get('codTipPedido') != undefined) {
			this.codTipPedido = atob(
				decodeURIComponent(this.route.snapshot.paramMap.get('codTipPedido'))
			);
		}

		if (this.route.snapshot.paramMap.get('codPedido') != undefined) {
			this.codPed = atob(decodeURIComponent(this.route.snapshot.paramMap.get('codPedido')));
		}

		this.filial = this.route.snapshot.paramMap.get('filPro');
		window.sessionStorage.setItem('filial', atob(this.filial));

		this.chaveProcesso = this.route.snapshot.paramMap.get('codPro');

		this.cajuri = atob(decodeURIComponent(this.chaveProcesso));
		/** Chamada do serviço poI18n para buscar as constantes a partir do contexto 'pedidos' no arquivo JurTraducao.*/
		thfI18nService
			.getLiterals({ context: 'detPedidos', language: thfI18nService.getLanguage() })
			.subscribe(litPed => {
				this.litDetPed = litPed;

				if (this.JuriGenerics.changeUndefinedToEmpty(this.codTipPedido) != '') {
					this.breadcrumb.items = [
						{ label: this.litDetPed.bread.home, link: '/' },
						{
							label: this.litDetPed.bread.processo,
							link: '/processo/' + this.filial + '/' + this.chaveProcesso,
						},
						{
							label: this.litDetPed.bread.pedidos,
							link: '/processo/' + this.filial + '/' + this.chaveProcesso + '/pedido',
						},
						{ label: this.litDetPed.bread.migracaoPedido },
					];
					this.titleDet = this.litDetPed.tituloMigracaoPed;
					this.btnIncluirLabel = this.litDetPed.formPed.btnConfirmar;
				} else if (this.JuriGenerics.changeUndefinedToEmpty(this.codPed) == '') {
					this.breadcrumb.items = [
						{ label: this.litDetPed.bread.home, link: '/' },
						{
							label: this.litDetPed.bread.processo,
							link: '/processo/' + this.filial + '/' + this.chaveProcesso,
						},
						{
							label: this.litDetPed.bread.pedidos,
							link: '/processo/' + this.filial + '/' + this.chaveProcesso + '/pedido',
						},
						{ label: this.litDetPed.tituloNovoPed },
					];
					this.titleDet = this.litDetPed.tituloNovoPed;
					this.btnIncluirLabel = this.litDetPed.formPed.btnIncluir;
				} else {
					this.breadcrumb.items = [
						{ label: this.litDetPed.bread.home, link: '/' },
						{
							label: this.litDetPed.bread.processo,
							link: '/processo/' + this.filial + '/' + this.chaveProcesso,
						},
						{
							label: this.litDetPed.bread.pedidos,
							link: '/processo/' + this.filial + '/' + this.chaveProcesso + '/pedido',
						},
						{ label: this.litDetPed.bread.pedido },
					];
					this.titleDet = this.litDetPed.tituloAlterarPed;
					this.btnIncluirLabel = this.litDetPed.formPed.btnAlterar;
					this.btnExcluirDisabled = false;
				}
				/** Adiciona o código do cajuri no título Meu processo + área */
				this.cajuriTitulo = this.cajuri.replace(new RegExp('^0*', 'g'), '');
			});
	}

	ngOnInit() {
		this.createForm(new Pedido());
		if (this.JuriGenerics.changeUndefinedToEmpty(this.codTipPedido) != '') {
			this.getInfoPreMigracao();
			this.getNSY();
		} else {
			this.getPedido();
		}

		this.sysProtheusParam.getParamByArray(['MV_JTVENJF']).subscribe(params => {
			this.sysParams = params;
		});

		this.getInfoProcesso();
		this.getConfigRedutores();
	}

	/**
	 * Busca as informações do processo
	 */
	getInfoProcesso() {
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA095');
		this.fwmodel.setFilter("NSZ_FILIAL='" + atob(this.filial) + "'");
		this.fwmodel.setFilter("NSZ_COD='" + this.cajuri + "'");
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.get('getInfoProcesso').subscribe(data => {
			if (data.resources != '') {
				this.breadcrumb.items[1].label =
					this.litDetPed.bread.processo +
					' ' +
					this.JuriGenerics.findValueByName(
						data.resources[0].models[0].fields,
						'NSZ_DAREAJ'
					) +
					' (' +
					this.cajuriTitulo +
					')';
				this.isProcEncerrado =
					this.JuriGenerics.findValueByName(
						data.resources[0].models[0].fields,
						'NSZ_SITUAC'
					) == '2';
			}
		});
	}
	/**
	 * Responsável pelo cálculo do valor do possível
	 */
	async atualizaVlrPossivel() {
		let prognPedido: string = '';
		this.vlrPossivelAtu =
			this.JuriGenerics.changeUndefinedToZero(this.formPedido.value.vlrpedido) -
			this.JuriGenerics.changeUndefinedToZero(this.formPedido.value.vlrprovavel) -
			this.JuriGenerics.changeUndefinedToZero(this.formPedido.value.vlrremoto) -
			this.JuriGenerics.changeUndefinedToZero(this.formPedido.value.vlrincontroverso);

		prognPedido = this.getTipoPrognosticoByValue(
			this.formPedido.value.vlrprovavel,
			this.vlrPossivelAtu,
			this.formPedido.value.vlrremoto,
			this.formPedido.value.vlrincontroverso
		);

		this.valorTotalPrognosticos =
			this.valorPossivelOriginal +
			this.JuriGenerics.changeUndefinedToZero(this.formPedido.value.vlrprovavel) +
			this.JuriGenerics.changeUndefinedToZero(this.formPedido.value.vlrremoto) +
			this.JuriGenerics.changeUndefinedToZero(this.formPedido.value.vlrincontroverso);
		this.formPedido.patchValue({
			vlrpossivel: this.vlrPossivelAtu,
			prognostico: prognPedido,
			vlrDistribuidoOrig: this.valorTotalPrognosticos,
		});
	}

	showDistriFields() {
		this.bHasValorDistribuir = this.vlrPossivelAtu != this.valorPossivelOriginal;
		if (this.valorPedidoOriginal == 0) {
			this.cTipoTratamentoMigracao = tipoTratamentoMigr.valorPedidoZerado;
		} else {
			this.cTipoTratamentoMigracao = tipoTratamentoMigr.valorASerDistribuido;
		}
	}

	getTipoPrognosticoByValue(
		valorProvavel: number = 0,
		valorPossivel: number = 0,
		valorRemoto: number = 0,
		valorIncontroverso: number = 0
	): string {
		let prognPedido: string = '';
		switch (true) {
			case valorProvavel > 0:
				prognPedido = '01';
				break;
			case valorPossivel > 0:
				prognPedido = '02';
				break;
			case valorRemoto > 0:
				prognPedido = '03';
				break;
			case valorIncontroverso > 0:
				prognPedido = '04';
				break;
			default:
				prognPedido = '00';
				break;
		}

		return this.getTipoPrognosticoByCode(prognPedido);
	}

	getTipoPrognosticoByCode(codTipoPrognostico: string = ''): string {
		let descPrognostico: string = '';
		switch (codTipoPrognostico) {
			case '01':
				descPrognostico = this.litDetPed.prognostico.provavel;
				break;
			case '02':
				descPrognostico = this.litDetPed.prognostico.possivel;
				break;
			case '03':
				descPrognostico = this.litDetPed.prognostico.remoto;
				break;
			case '04':
				descPrognostico = this.litDetPed.prognostico.incontroverso;
				break;
			default:
				descPrognostico = this.litDetPed.prognostico.semPrognostico;
				break;
		}
		return descPrognostico;
	}

	/**
	 * Responsável por obter os dados inputados no formulário de inclusão
	 * de pedido para serem usados após o submit
	 */
	createForm(pedido: Pedido) {
		this.formPedido = this.formBuilder.group({
			tpPedido: [pedido.tpPedido, Validators.compose([Validators.required])],
			dataPedidoVlr: [pedido.dataPedidoVlr, Validators.compose([Validators.required])],
			correcao: [pedido.correcao, Validators.compose([Validators.required])],
			prognostico: [pedido.prognostico],
			vlrpedido: [pedido.vlrpedido],
			vlrprovavel: [pedido.vlrprovavel],
			vlrpossivel: [pedido.vlrpossivel],
			vlrremoto: [pedido.vlrremoto],
			vlrincontroverso: [pedido.vlrincontroverso],
			vlrPossivelOrig: [pedido.vlrPossivelOrig],
			vlrDistribuidoOrig: [pedido.vlrDistribuidoOrig],
			dtJuros: [pedido.dtJuros],
			dtMulta: [pedido.dtMulta],
			percMulta: [pedido.percMulta],
			detalhamento: [pedido.detalhamento],
			atualizaRedutor: [pedido.atualizaRedutor],
			vlrRedutor: [pedido.vlrRedutor],
			percEncargos: [pedido.percEncargos],
			percHonorarios: [pedido.percHonorarios],
			percMultaAvc: [pedido.percMultaAvc],
			correcaoMultaAvc: [pedido.correcaoMultaAvc],
			atualizaMultaAvc: [pedido.atualizaMultaAvc],
		});
	}

	/**
	 * Requisição do protheus para obter os pedidos cadastrados e pk do processo
	 */
	getPedido(bSetValuesForm: boolean = true) {
		this.isHideLoadingPedido = false;
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA270');
		this.fwmodel.setFilter("NSZ_COD='" + this.cajuri + "'");
		this.fwmodel.setCampoVazio(true);
		this.fwmodel.setFilter("NSZ_FILIAL='" + atob(this.filial) + "'");
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.setCampoVirtual(true);

		this.fwmodel.get('Busca pedido').subscribe(data => {
			this.pk = data.resources[0].pk;
			this.aQtdPedidos = data.resources[0].models[0].models[0].items;
			var pedidoSelecionado: any;
			if (this.aQtdPedidos == undefined || this.aQtdPedidos.length == 0) {
				if (bSetValuesForm && this.JuriGenerics.changeUndefinedToEmpty(this.codPed) != '') {
					this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codPed))], {
						relativeTo: this.route,
					});
					this.thfNotification.error(this.litDetPed.naoEncontrado);
				} else {
					this.isHideLoadingPedido = true;
				}
			} else {
				if (bSetValuesForm) {
					if (this.JuriGenerics.changeUndefinedToEmpty(this.codPed) != '') {
						let i = -1;
						do {
							i++;
							pedidoSelecionado = this.aQtdPedidos[i];
						} while (
							this.aQtdPedidos[i] != undefined &&
							this.codPed !=
								this.JuriGenerics.findValueByName(
									this.aQtdPedidos[i].fields,
									'O0W_COD'
								)
						);
						this.nIndiceRegistro = i;
						this.codO0W = this.JuriGenerics.findValueByName(
							pedidoSelecionado.fields,
							'O0W_COD'
						);
						this.formPedido.patchValue({
							tpPedido: this.JuriGenerics.findValueByName(
								pedidoSelecionado.fields,
								'O0W_CTPPED'
							),
							dataPedidoVlr: this.JuriGenerics.makeDate(
								this.JuriGenerics.findValueByName(
									pedidoSelecionado.fields,
									'O0W_DATPED'
								),
								'yyyy-mm-dd'
							),
							correcao: this.JuriGenerics.findValueByName(
								pedidoSelecionado.fields,
								'O0W_CFRCOR'
							),
							prognostico: this.JuriGenerics.findValueByName(
								pedidoSelecionado.fields,
								'O0W_PROGNO'
							),
							vlrpedido: this.JuriGenerics.findValueByName(
								pedidoSelecionado.fields,
								'O0W_VPEDID'
							),
							vlrprovavel: this.JuriGenerics.changeUndefinedToZero(
								this.JuriGenerics.findValueByName(
									pedidoSelecionado.fields,
									'O0W_VPROVA'
								)
							),
							vlrpossivel: this.JuriGenerics.changeUndefinedToZero(
								this.JuriGenerics.findValueByName(
									pedidoSelecionado.fields,
									'O0W_VPOSSI'
								)
							),
							vlrremoto: this.JuriGenerics.changeUndefinedToZero(
								this.JuriGenerics.findValueByName(
									pedidoSelecionado.fields,
									'O0W_VREMOT'
								)
							),
							vlrincontroverso: this.JuriGenerics.changeUndefinedToZero(
								this.JuriGenerics.findValueByName(
									pedidoSelecionado.fields,
									'O0W_VINCON'
								)
							),
							dtJuros: this.JuriGenerics.makeDate(
								this.JuriGenerics.findValueByName(
									pedidoSelecionado.fields,
									'O0W_DTJURO'
								),
								'yyyy-mm-dd'
							),
							dtMulta: this.JuriGenerics.makeDate(
								this.JuriGenerics.findValueByName(
									pedidoSelecionado.fields,
									'O0W_DTMULT'
								),
								'yyyy-mm-dd'
							),
							percMulta: this.JuriGenerics.changeUndefinedToZero(
								this.JuriGenerics.findValueByName(
									pedidoSelecionado.fields,
									'O0W_PERMUL'
								)
							),
							percEncargos: this.JuriGenerics.changeUndefinedToZero(
								this.JuriGenerics.findValueByName(
									pedidoSelecionado.fields,
									'O0W_PERENC'
								)
							),
							detalhamento: this.JuriGenerics.changeUndefinedToEmpty(
								this.JuriGenerics.findValueByName(
									pedidoSelecionado.fields,
									'O0W_DETALH'
								)
							),
							vlrRedutor: this.JuriGenerics.changeUndefinedToEmpty(
								this.JuriGenerics.findValueByName(
									pedidoSelecionado.fields,
									'O0W_VLREDU'
								)
							),
							percHonorarios: this.JuriGenerics.changeUndefinedToZero(
								this.JuriGenerics.findValueByName(
									pedidoSelecionado.fields,
									'O0W_PERHON'
								)
							),
							percMultaAvc: this.JuriGenerics.changeUndefinedToZero(
								this.JuriGenerics.findValueByName(
									pedidoSelecionado.fields,
									'O0W_MULTRI'
								)
							),
							correcaoMultaAvc: this.JuriGenerics.changeUndefinedToEmpty(
								this.JuriGenerics.findValueByName(
									pedidoSelecionado.fields,
									'O0W_CFCMUL'
								)
							),
						});
						if (
							this.JuriGenerics.changeUndefinedToEmpty(
								this.JuriGenerics.findValueByName(
									pedidoSelecionado.fields,
									'O0W_REDUT'
								)
							) == '1'
						) {
							this.formPedido.patchValue({
								atualizaRedutor: true,
							});
						} else {
							this.formPedido.patchValue({
								atualizaRedutor: false,
							});
						}

						if (
							this.JuriGenerics.changeUndefinedToEmpty(
								this.JuriGenerics.findValueByName(
									pedidoSelecionado.fields,
									'O0W_CFCMUL'
								)
							) != ''
						) {
							this.formPedido.patchValue({ atualizaMultaAvc: true });
							this.isCorrMultaDisabled = false;
						} else {
							this.formPedido.patchValue({ atualizaMultaAvc: false });
							this.isCorrMultaDisabled = true;
						}

						this.breadcrumb.items[3].label =
							this.JuriGenerics.findValueByName(
								pedidoSelecionado.fields,
								'O0W_DTPPED'
							) +
							' - ' +
							this.JuriGenerics.makeDate(
								this.JuriGenerics.findValueByName(
									pedidoSelecionado.fields,
									'O0W_DATPED'
								),
								'dd/mm/yyyy'
							);
						this.isHideLoadingPedido = true;
						this.btnIncluirEnabled = false;
						this.avancadoAct();
					} else {
						this.isHideLoadingPedido = true;

						this.formPedido.patchValue({
							prognostico: this.litDetPed.prognostico.semPrognostico,
						});
					}
				} else {
					this.isHideLoadingPedido = true;
				}
			}
		});
	}

	/**
	 * Responsável por obter a lista de formas de correção
	 * @param CodCorrecao
	 */
	getFormasCorrecao(CodCorrecao?: string) {
		var formula: string = '';

		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA061');
		this.fwmodel.setFilter("NW7_COD='" + CodCorrecao + "'");
		this.fwmodel.get('obtem lista com formas de correção').subscribe(data => {
			if (data.resources != '') {
				formula = data.resources[0].models[0].fields[2].value;
			}
		});
		return formula;
	}
	/**
	 * Verifica se a forma de correção selecionada tem multa, para que seja habilitado os campos de data e % de multa
	 * @param correcao
	 */
	checkMulta(correcao: string) {
		this.disabledMulta = true;
		let formula = this.getFormasCorrecao(correcao);

		if (
			(correcao === '30' || correcao === '09' || formula.indexOf('#VLRMULTA') != -1) &&
			!this.toggleAvancado
		) {
			this.disabledMulta = false;
		} else {
			this.formPedido.patchValue({
				dtMulta: '',
				percMulta: 0,
			});
		}
	}

	/**
	 * Função de inicio das validações e Commit.
	 */
	confirmVerify() {
		if (this.beforeSubmit()) {
			this.submitPedido();
		}
	}

	/**
	 * Função de inicio das validações e Commit.
	 */
	deleteVerify() {
		if (this.beforeDelete()) {
			this.onDeletePed();
		}
	}

	/**
	 * Obtem os dados do formulário no momento da inclusão e passa como parâmetro
	 * para o postPedido. Esses dados serão usados no body da requisisão
	 * para realizar a inclusão através do FWModel.
	 */
	submitPedido() {
		this.isHideLoadingInclusao = true;

		if (this.vlrPossivelAtu < 0) {
			this.thfNotification.error(this.litDetPed.msgErroVlrNegativo);
			this.isHideLoadingInclusao = false;
		} else {
			let body: string = this.bodyPutPedido(
				this.formPedido.value,
				this.pk,
				this.cajuri,
				this.aQtdPedidos
			);

			this.fwmodel.restore();
			this.fwmodel.setModelo('JURA270');
			this.fwmodel.setFilter("NSZ_COD = '" + this.cajuri + "'");
			this.fwmodel.setFilter("NSZ_FILIAL = '" + atob(this.filial) + "'");
			this.fwmodel.setCampoVirtual(true);
			this.fwmodel.setFirstLevel(false);
			this.fwmodel.put(this.pk, body).subscribe(data => {
				if (this.JuriGenerics.changeUndefinedToEmpty(data) == '') {
					this.isHideLoadingInclusao = false;
				} else {
					if (this.JuriGenerics.changeUndefinedToEmpty(this.codPed) == '') {
						this.thfNotification.success(this.litDetPed.msgInclusao);
					} else {
						this.thfNotification.success(this.litDetPed.msgAlteracao);
					}

					if (this.arrPedidosMigracao.length > 0) {
						this.deleteNSYbyTipoPedido();
					} else {
						this.isHideLoadingInclusao = false;
						this.router.navigate([this.routingState.getPreviousUrl('pedido')], {
							relativeTo: this.route,
						});
					}
				}
			});
		}
	}

	/**
	 * Pré-validação
	 */
	beforeSubmit(): boolean {
		let isSubmitable: boolean = true;
		let cErrorMsg: string = '';
		let frmPedido = this.formPedido.value;
		isSubmitable = this.formPedido.valid;

		if (!isSubmitable) {
			if (this.formPedido.value.vlrpedido <= 0) {
				cErrorMsg = this.litDetPed.error.valorNegativo;
			} else if (
				this.JuriGenerics.changeUndefinedToEmpty(this.formPedido.value.tpPedido) == '' ||
				this.JuriGenerics.changeUndefinedToEmpty(this.formPedido.value.dataPedidoVlr) ==
					'' ||
				this.JuriGenerics.changeUndefinedToEmpty(this.formPedido.value.correcao) == ''
			) {
				cErrorMsg = this.litDetPed.error.camposObrigatorios;
			} else {
				cErrorMsg = this.litDetPed.error.erroGenerico;
			}
		} else {
			if (
				frmPedido.vlrpedido !=
				this.JuriGenerics.changeUndefinedToZero(frmPedido.vlrpossivel) +
					this.JuriGenerics.changeUndefinedToZero(frmPedido.vlrprovavel) +
					this.JuriGenerics.changeUndefinedToZero(frmPedido.vlrremoto) +
					this.JuriGenerics.changeUndefinedToZero(frmPedido.vlrincontroverso)
			) {
				cErrorMsg = this.litDetPed.error.somaPrognostico;
			}
		}

		if (cErrorMsg != '') {
			this.thfNotification.error(cErrorMsg);
			isSubmitable = false;
		}

		// Validação para apresentar o Modal de Justificativa, caso o processo esteja encerrado
		if (isSubmitable) {
			if (
				this.isProcEncerrado &&
				this.sysProtheusParam.getParamValueByName(this.sysParams, 'MV_JTVENJF') == '1'
			) {
				isSubmitable = false;
				this.modalJustif.openModal(this.cajuri, () => {
					if (this.modalJustif.isOk) {
						this.submitPedido();
					}
				});
			} else {
				isSubmitable = true;
			}
		}
		return isSubmitable;
	}

	/**
	 * Pré-validação
	 */
	beforeDelete(): boolean {
		let isSubmitable: boolean = true;
		isSubmitable = this.formPedido.valid;

		// Validação para apresentar o Modal de Justificativa, caso o processo esteja encerrado
		if (isSubmitable) {
			if (
				this.isProcEncerrado &&
				this.sysProtheusParam.getParamValueByName(this.sysParams, 'MV_JTVENJF') == '1'
			) {
				isSubmitable = false;
				this.modalJustif.openModal(this.cajuri, () => {
					if (this.modalJustif.isOk) {
						this.onDeletePed();
					}
				});
			} else {
				isSubmitable = true;
			}
		}
		return isSubmitable;
	}

	/**
	 * Obtem os dados do formulário no momento da inclusão e passa como parâmetro
	 * para o postPedido. Esses dados serão usados no body da requisisão
	 * para realizar a inclusão através do FWModel.
	 */
	onDeletePed() {
		this.excluir.loading = true;
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA270');
		this.fwmodel.setFilter("NSZ_COD = '" + this.cajuri + "'");
		this.fwmodel.setFilter("NSZ_FILIAL = '" + atob(this.filial) + "'");
		this.fwmodel
			.put(this.pk, this.bodyDeletePedido(this.formPedido.value, this.pk, this.cajuri))
			.subscribe(data => {
				if (data != '') {
					this.thfNotification.success(this.litDetPed.msgExclusao);
					this.formPedido.reset(new Pedido());
					this.excluir.loading = false;
					this.router.navigate([this.routingState.getPreviousUrl('pedido')], {
						relativeTo: this.route,
					});
				}
				this.isHideLoadingExclusao = false;
			});
	}

	/**
	 * Chama o SelectFiles do po-upload
	 */
	getDocuments() {
		this.docUpload.selectFiles();
	}
	/**
	 * Chama a tela de anexos
	 */
	callAnexos() {
		this.router.navigate(['./anexos'], { relativeTo: this.route });
	}
	/**
	 * Realiza o envio dos documentos
	 */
	sendDocs() {
		if (this.docUpload.currentFiles.length > 0) {
			this.docUpload.sendFiles();
		}
	}
	/**
	 * Carrega as informações para o POST do upload de arquivos
	 * @param file
	 */
	uploadFiles(file) {
		file.data = {
			cajuri: this.cajuri.toString(),
			entidade: 'NSY',
			codEntidade: this.codPed.toString(),
		};
		this.uploadCount++;
	}

	/**
	 * Trata para redirecionar após concluir todos os anexos
	 */
	uploadSucess() {
		this.uploadCount--;

		if (this.uploadCount == 0) {
			this.router.navigate([this.routingState.getPreviousUrl(btoa(this.codPed))], {
				relativeTo: this.route,
			});
		}
	}

	/**
	 * Restaura as variáveis
	 */

	restore() {
		this.dataPedidoVlr = '';
	}

	/**
	 * Cancela operação
	 */
	onCancel() {
		this.router.navigate([this.routingState.getPreviousUrl()], { relativeTo: this.route });
	}

	/**
	 * Body do Delete do Pedido
	 * @param pedido - Pedido
	 * @param pk - PK do Processo
	 * @param cajuri - Código do Assunto Jurídico
	 */
	bodyDeletePedido(pedido: Pedido, pk: string, cajuri: string) {
		let fwModelBody: FwModelBody = new FwModelBody('JURA270', 4, pk);
		let gridPedidos: FwModelBodyModels;
		let itemPedido: FwModelBodyItems;

		fwModelBody.addModel('NSZMASTER', 'FIELDS', 1).addField('NSZ_COD', cajuri, 2);

		gridPedidos = fwModelBody.models[0].addModel('O0WDETAIL', 'GRID', 1);

		for (let i = 0; i < this.aQtdPedidos.length; i++) {
			if (
				this.JuriGenerics.findValueByName(this.aQtdPedidos[i].fields, 'O0W_COD') !=
				this.codPed
			) {
				gridPedidos.addItems();
			} else {
				break;
			}
		}
		itemPedido = gridPedidos.addItems(0, 1);
		itemPedido.addField('O0W_CAJURI', cajuri, 'C');
		itemPedido.addField('O0W_DATPED', pedido.dataPedidoVlr, 'D');
		itemPedido.addField('O0W_CTPPED', pedido.tpPedido, 'C');
		itemPedido.addField('O0W_CFRCOR', pedido.correcao, 'C');

		return fwModelBody.serialize();
	}

	/**
	 *  função responsavel pela montagem do body do pedido
	 * @param pedido - formulário  do pedido
	 * @param pk - primary key do pedido
	 * @param cajuri - código do processo
	 * @param aQtdPedidos - lista de pedidos vinculados ao processo
	 */
	bodyPutPedido(pedido: Pedido, pk: string, cajuri: string, aQtdPedidos: Array<any> = []) {
		let fwModelBody: FwModelBody = new FwModelBody('JURA270', 4, pk);
		let gridPedidos: FwModelBodyModels;
		let itemPedido: FwModelBodyItems;

		fwModelBody.addModel('NSZMASTER', 'FIELDS').addField('NSZ_COD', cajuri, 2);

		gridPedidos = fwModelBody.models[0].addModel('O0WDETAIL', 'GRID', 1);

		if (aQtdPedidos.length) {
			for (let i = 0; i < aQtdPedidos.length; i++) {
				const codO0WLoop = this.JuriGenerics.findValueByName(
					aQtdPedidos[i].fields,
					'O0W_COD'
				);
				if (codO0WLoop != this.codO0W) {
					gridPedidos.addItems(0, 0);
				} else {
					break;
				}
			}
		} else {
			gridPedidos.addItems(0, 1);
		}

		itemPedido = gridPedidos.addItems();
		itemPedido.addField('O0W_CAJURI', cajuri, 'C');
		itemPedido.addField('O0W_DATPED', pedido.dataPedidoVlr, 'D');
		itemPedido.addField('O0W_CTPPED', pedido.tpPedido, 'C');
		itemPedido.addField('O0W_CFRCOR', pedido.correcao, 'C');
		itemPedido.addField('O0W_VPEDID', pedido.vlrpedido, 'C');
		itemPedido.addField('O0W_VPROVA', pedido.vlrprovavel, 'C');
		itemPedido.addField('O0W_VREMOT', pedido.vlrremoto, 'C');
		itemPedido.addField('O0W_VINCON', pedido.vlrincontroverso, 'C');
		itemPedido.addField('O0W_DTJURO', pedido.dtJuros, 'D');
		itemPedido.addField('O0W_DTMULT', pedido.dtMulta, 'D');
		itemPedido.addField('O0W_PERMUL', pedido.percMulta, 'C');

		/**
		 * por motivos indefinidos, esse é o unico campo que não faz a gravação do & corretamente, por isso é feita a conversão aqui.
		 */
		itemPedido.addField('O0W_DETALH', pedido.detalhamento.replace(/[\u0026]/g, '&amp;'), 'C');
		if (pedido.atualizaRedutor) {
			itemPedido.addField('O0W_REDUT', '1', 'C');
		} else {
			itemPedido.addField('O0W_REDUT', '2', 'C');
		}

		itemPedido.addField('O0W_MULTRI', pedido.percMultaAvc, 'C');
		itemPedido.addField('O0W_CFCMUL', pedido.correcaoMultaAvc, 'C');
		itemPedido.addField('O0W_PERENC', pedido.percEncargos, 'C');
		itemPedido.addField('O0W_PERHON', pedido.percHonorarios, 'C');

		return fwModelBody.serialize();
	}

	/**
	 * Busca o pedido
	 */
	getNSY() {
		let dataPedido: string = '';
		let correcao: string = '';
		let codPrognostico: string = '';
		let tipoPrognostico: string = '';
		let valorProvavel: number = 0;
		let valorPossivel: number = 0;
		let valorRemoto: number = 0;
		let valorIncontroverso: number = 0;
		let codsPedido: string = window.sessionStorage.getItem('TLPedidosMigracao');
		let filtroPedidos: string = '';
		this.getPedido(false);
		if (this.JuriGenerics.changeUndefinedToEmpty(codsPedido) != '') {
			let arrPedidos: string[] = JSON.parse(codsPedido);
			arrPedidos.forEach(pedido => {
				filtroPedidos += "'" + pedido + "',";
			});
			filtroPedidos = filtroPedidos.substr(0, filtroPedidos.length - 1);
		}

		this.aQtdPedidos = undefined;
		this.isHideLoadingPedido = false;
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA094');
		this.fwmodel.setFilter("NSY_CAJURI='" + this.cajuri + "'and NSY_CVERBA=' '");

		if (filtroPedidos.length > 0) {
			this.fwmodel.setFilter('NSY_COD IN (' + filtroPedidos + ')');
			window.sessionStorage.setItem('TLPedidosMigracao', '');
		} else {
			this.fwmodel.setFilter("NSY_CPEVLR='" + this.codTipPedido + "'");
		}

		this.fwmodel.setCampoVazio(true);
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.setPageSize('1000');
		this.fwmodel
			.get('Busca dos Pedidos - Migração')
			.pipe(
				flatMap(data => {
					if (data.resources != '' && data.resources != undefined) {
						data.resources.map(item => {
							this.arrPedidosMigracao.push(
								this.JuriGenerics.findValueByName(item.models[0].fields, 'NSY_COD')
							);

							//Obtem os valores do pedido posicionado
							codPrognostico = this.JuriGenerics.findValueByName(
								item.models[0].fields,
								'NSY_CPROG'
							);
							correcao = correcao
								? correcao
								: this.JuriGenerics.findValueByName(
										item.models[0].fields,
										'NSY_CFCORC'
								  );

							if (
								this.JuriGenerics.changeUndefinedToEmpty(
									this.JuriGenerics.findValueByName(
										item.models[0].fields,
										'NSY_DTCONT'
									)
								) == ''
							) {
								dataPedido = dataPedido
									? dataPedido
									: this.JuriGenerics.findValueByName(
											item.models[0].fields,
											'NSY_PEDATA'
									  );
							} else {
								dataPedido = dataPedido
									? dataPedido
									: this.JuriGenerics.findValueByName(
											item.models[0].fields,
											'NSY_DTCONT'
									  );
							}

							if (codPrognostico != '') {
								this.fwmodel.restore();
								this.fwmodel.setModelo('JURA006');
								this.fwmodel.setFilter("NQ7_COD='" + codPrognostico + "'");

								return this.fwmodel.get('getNSYToO0WFormat').subscribe(result => {
									if (
										this.JuriGenerics.changeUndefinedToEmpty(
											result.resources
										) != ''
									) {
										let valorAtual: number = 0;
										let valorPedido: number = 0;
										tipoPrognostico = this.JuriGenerics.findValueByName(
											result.resources[0].models[0].fields,
											'NQ7_TIPO'
										); //busca o tipo do prognóstico setado
										switch (tipoPrognostico) {
											case '1':
												valorAtual = this.formPedido.value.vlrprovavel;
												valorAtual += this.JuriGenerics.changeUndefinedToZero(
													this.JuriGenerics.findValueByName(
														item.models[0].fields,
														'NSY_VLCONT'
													)
												);
												valorPedido = this.JuriGenerics.changeUndefinedToZero(
													this.JuriGenerics.findValueByName(
														item.models[0].fields,
														'NSY_PEVLR'
													)
												);
												if (valorPedido > 0) {
													this.formPedido.patchValue({
														vlrpedido: valorPedido,
													});
												}
												this.formPedido.patchValue({
													vlrprovavel: valorAtual,
												});
												break;
											case '2':
												this.valorPossivelOriginal += this.JuriGenerics.changeUndefinedToZero(
													this.JuriGenerics.findValueByName(
														item.models[0].fields,
														'NSY_VLCONT'
													)
												);
												this.valorPedidoOriginal = this.JuriGenerics.changeUndefinedToZero(
													this.JuriGenerics.findValueByName(
														item.models[0].fields,
														'NSY_PEVLR'
													)
												);

												this.formPedido.patchValue({
													vlrpedido: this.valorPedidoOriginal,
													vlrPossivelOrig: this.valorPossivelOriginal,
												});
												break;
											case '3':
												valorAtual = this.formPedido.value.vlrremoto;
												valorAtual += this.JuriGenerics.changeUndefinedToZero(
													this.JuriGenerics.findValueByName(
														item.models[0].fields,
														'NSY_VLCONT'
													)
												);
												valorPedido = this.JuriGenerics.changeUndefinedToZero(
													this.JuriGenerics.findValueByName(
														item.models[0].fields,
														'NSY_PEVLR'
													)
												);
												if (valorPedido > 0) {
													this.formPedido.patchValue({
														vlrpedido: valorPedido,
													});
												}
												this.formPedido.patchValue({
													vlrremoto: valorAtual,
												});
												break;
											case '4':
												valorAtual = this.formPedido.value.vlrincontroverso;
												valorAtual += this.JuriGenerics.changeUndefinedToZero(
													this.JuriGenerics.findValueByName(
														item.models[0].fields,
														'NSY_VLCONT'
													)
												);
												valorPedido = this.JuriGenerics.changeUndefinedToZero(
													this.JuriGenerics.findValueByName(
														item.models[0].fields,
														'NSY_PEVLR'
													)
												);
												if (valorPedido > 0) {
													this.formPedido.patchValue({
														vlrpedido: valorPedido,
													});
												}
												this.formPedido.patchValue({
													vlrincontroverso: valorAtual,
												});
												break;
										}

										if (
											this.JuriGenerics.changeUndefinedToEmpty(
												this.formPedido.value.dataPedidoVlr
											) == ''
										) {
											this.formPedido.patchValue({
												dataPedidoVlr: this.JuriGenerics.makeDate(
													dataPedido,
													'yyyy-mm-dd'
												),
											});
										}
										this.formPedido.patchValue({
											dtJuros: this.JuriGenerics.makeDate(
												this.JuriGenerics.findValueByName(
													item.models[0].fields,
													'NSY_DTJURC'
												),
												'yyyy-mm-dd'
											),
											dtMulta: this.JuriGenerics.makeDate(
												this.JuriGenerics.findValueByName(
													item.models[0].fields,
													'NSY_DTMULC'
												),
												'yyyy-mm-dd'
											),
											percMulta: this.JuriGenerics.changeUndefinedToZero(
												this.JuriGenerics.findValueByName(
													item.models[0].fields,
													'NSY_PERMUC'
												)
											),
										});
										this.atualizaVlrPossivel().then(() =>
											this.showDistriFields()
										);
									}
								});
							}
						});
					}
					return of({
						nome: valorProvavel + valorPossivel + valorRemoto + valorIncontroverso,
					});
				})
			)
			.toPromise()
			.then(() => {
				this.formPedido.patchValue({
					tpPedido: this.codTipPedido,
					correcao,
				});
				this.isHideLoadingPedido = true;
			});
	}

	async deleteNSYbyTipoPedido() {
		this.arrPedidosMigracao.forEach(pedido => {
			this.fwmodel.restore();
			this.fwmodel.setModelo('JURA094');
			this.fwmodel.setFilter("NSY_COD='" + pedido + "' and NSY_CAJURI='" + this.cajuri + "'");
			this.fwmodel.get('getPedidoMigracaoForDelete').subscribe(result => {
				if (result.resources != '') {
					let pkPedido = result.resources[0].pk;

					this.fwmodel.restore();
					this.fwmodel.setModelo('JURA094');
					this.fwmodel.delete(pkPedido, 'deletePedidoMigracao').subscribe(() => {
						this.nQtdPedidosParaExcluidos++;
						if (this.nQtdPedidosParaExcluidos == this.arrPedidosMigracao.length) {
							this.router.navigate(
								[this.routingState.getPreviousUrl(btoa(this.codPed))],
								{ relativeTo: this.route }
							);
						}
					});
				}
			});
		});
	}

	getInfoPreMigracao() {
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA270');
		this.fwmodel.setFilter("NSZ_COD='" + this.cajuri + "'");
		this.fwmodel.setFilter("NSZ_FILIAL='" + atob(this.filial) + "'");
		this.fwmodel.get('Processo para Migração').subscribe(result => {
			if (result.resources != '') {
				this.pk = result.resources[0].pk;
			}
		});
	}

	// Limpa e desabilita os campos de multa quando expandir a sessão avançada
	desabilitaMulta() {
		let form = this.formPedido.value;

		if (this.toggleAvancado == true) {
			this.formPedido.patchValue({
				percMultaAvc: form.percMulta,
				correcaoMultaAvc: form.correcao,
				atualizaMultaAvc: true,
			});
			this.isCorrMultaDisabled = false;
			this.formPedido.patchValue({
				percMulta: '',
				dtMulta: '',
			});
			this.disabledMulta = true;
			this.avancadoAct();
		} else if (
			this.JuriGenerics.changeUndefinedToZero(form.percMultaAvc) === 0 &&
			this.JuriGenerics.changeUndefinedToZero(form.percHonorarios) === 0 &&
			this.JuriGenerics.changeUndefinedToZero(form.percEncargos) === 0 &&
			this.JuriGenerics.changeUndefinedToEmpty(form.correcaoMultaAvc) == ''
		) {
			this.checkMulta(form.correcao);
		}
	}

	// Preenche o campo data de juros com o mesmo valor do campo data de correção
	preencheDataJuros() {
		this.formPedido.patchValue({
			dtJuros: this.formPedido.value.dataPedidoVlr,
		});
	}

	/**
	 * Ação do switch
	 * Limpa e desabilita o campo de forma de correção
	 * @param status True ou False
	 */
	clearFormaCorr(status) {
		if (status) {
			this.isCorrMultaDisabled = false;
		} else {
			this.formPedido.patchValue({
				correcaoMultaAvc: '',
			});
			this.isCorrMultaDisabled = true;
		}
	}

	/**
	 * Ações a serem tomadas quando os campos do Avançado estiverem preenchidos
	 */
	avancadoAct() {
		const form = this.formPedido;
		this.isMinimizeDisabled = false;
		this.isClearBtnDisabled = true;
		if (
			this.JuriGenerics.changeUndefinedToZero(form.value.percMultaAvc) !== 0 ||
			this.JuriGenerics.changeUndefinedToZero(form.value.percEncargos) !== 0 ||
			this.JuriGenerics.changeUndefinedToZero(form.value.percHonorarios) !== 0 ||
			this.JuriGenerics.changeUndefinedToEmpty(form.value.correcaoMultaAvc) != ''
		) {
			this.toggleAvancado = true;
			this.isMinimizeDisabled = true;
			this.isClearBtnDisabled = false;
		}
	}

	/**
	 * Ação do botão limpar
	 */
	onClearAvanc() {
		this.formPedido.patchValue({
			percMultaAvc: 0,
			percEncargos: 0,
			percHonorarios: 0,
			correcaoMultaAvc: '',
		});
		this.avancadoAct();
		this.clearModal.close();
	}
	/**
	 * Verifica se há configurações de redutores cadastradas
	 * se não houver, as informaçoes de redutor são ocultadas
	 */
	getConfigRedutores() {
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA258');
		this.fwmodel.get('Busca registros na tabela de redutores').subscribe(data => {
			if (data !== undefined && data !== '' && data.total === 0) {
				this.isHideVlrRedutor = true;
			}
		});
	}
}
