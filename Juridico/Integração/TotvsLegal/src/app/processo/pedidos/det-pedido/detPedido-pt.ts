const errorMsg = {
	valorDivergente:
		'Há valor a ser distribuido entre os prognósticos. Distribua a diferença nos prognósticos Provável, Remoto e Incontroverso.',
	camposObrigatorios: 'Preencha os campos de Tipo do Pedido, data do valor e Forma de correção',
	valorNegativo: 'O valor do pedido deve ser maior que zero!',
	somaPrognostico: 'A soma dos prognósticos deve ser igual ao valor do pedido!',
	erroGenerico: 'Não foi possível realizar a gravação do registro. Verifique!',
};

const migracaoMsg = {
	cabecalho:
		'Identificamos que o valor do pedido é de R$ {0}, mas há uma diferença entre o prognóstico Possivel e o valor do Pedido. Essa diferença deve ser distribuida entre os prognósticos Provável, Remoto e Incontroverso.',
	valorPossivelOrig: 'Valor possível original: R$ {0}',
};

const breadcrumb = {
	home: 'Meus processos',
	processo: 'Meu processo',
	pedidos: 'Pedido',
	pedido: '',
	migracaoPedido: '',
};

const progStatus = {
	provavel: 'Provável',
	possivel: 'Possível',
	remoto: 'Remoto',
	incontroverso: 'Incontroverso',
	semPrognostico: 'Sem prognóstico',
};

const formPedidos = {
	tpPedido: 'Pedido',
	dataPedidoVlr: 'Data de correção',
	correcao: 'Forma de correção',
	prognostico: 'Prognóstico',
	fundProg: 'Prognóstico',
	vlrprov: 'Valor de provisão (R$)',
	vlrprovatu: 'Valor da provisão atualizado (R$)',
	vlrpedido: 'Valor do pedido (R$)',
	vlrprovavel: 'Valor provável (R$)',
	vlrpossivel: 'Valor possível (R$)',
	vlrremoto: 'Valor remoto (R$)',
	vlrRedutor: 'Valor com redutor (R$)',
	vlrincontroverso: 'Valor incontroverso (R$)',
	btnCancelar: 'Cancelar',
	btnExcluir: 'Excluir pedido',
	btnIncluir: 'Incluir pedido',
	btnAlterar: 'Alterar pedido',
	btnModalExc: 'Excluir',
	btnModalFec: 'Fechar',
	btnConfirmar: 'Confirmar',
	btnClearAvc: 'Limpar',
	tituloModal: 'Atenção!',
	confirmExclusao: 'Tem certeza que deseja excluir este pedido?',
	confirmClearAvc: 'Tem certeza que deseja limpar os percentuais de multa?',
	vlrMigracao: 'Valores da migração',
	detalhes: 'Detalhes',
	vlrPossivelOri: 'Valor possível original',
	vlrDistri: 'Valor total já distribuido',
	valorNegativo: 'O valor tem de ser maior que 0!',
	dataJuros: 'Data juros',
	percMulta: '% Multa',
	tootipMulta:
		'Este campo sempre calcula a multa sobre o valor atualizado! É ativado de acordo com a forma de correção. Se necessário, use o botão avançado.',
	dataMulta: 'Data multa',
	percEncargos: '% Encargos',
	percHonorarios: '% Honorários',
	avancado: 'Avançado',
	maximizar: 'MAXIMIZAR',
	minimizar: 'MINIMIZAR',
	atuMulta: 'Atualiza multa?',
	atuEncargos: 'Atualiza encargos?',
	atuHonorarios: 'Atualiza honorários?',
	sim: 'Sim',
	nao: 'Não',
	correcaoMultaAvc: 'Forma de correção multa',
	inputDetalhamento: 'Detalhamento',
	redutor: 'Aplicar redutor ?',
};
/**
 * Constantes principais
 */
export const detPedidosPt = {
	bread: breadcrumb,
	formPed: formPedidos,
	prognostico: progStatus,
	error: errorMsg,
	migracao: migracaoMsg,
	tituloNovoPed: 'Novo pedido',
	tituloAlterarPed: 'Alterar pedido',
	tituloMigracaoPed: 'Atualização de pedido',
	msgInclusao: 'Pedido incluído com sucesso!',
	msgExclusao: 'Pedido excluído com sucesso!',
	msgAlteracao: 'Pedido alterado com sucesso!',
	msgErroVlrNegativo: 'O valor possível não pode ser negativo',
	carregando: 'Carregando Pedido',
};
