const msgModal = {
	msgAjustes:
		'Verificamos que seu cadastro de pedido está desatualizado e/ou precisa de ajustes, deseja atualizar ? ',
	atualizacao: 'Atualização de pedidos',
	pedido: 'Pedidos',
	msgGridPed: 'Os seguintes registros foram encontrados para o pedido: ',
	alertMigracao:
		'Foram encontrados pedidos com data e/ou prognósticos diferentes. Verifique os dados antes de salvar.',
	selecionarPedidos: 'Selecione os pedidos para atualização:',
	alertConfPriLinha: 'Os pedidos apresentados são do mesmo pedido?',
	alertConfSegLinha:
		'Caso não seja, você deve clicar em "Não" para indicar corretamente as linhas de cada pedido.',
	alertLinkFluig:
		'Não foi possível acessar o link da Solicitação de Aprovação. Verifique os parâmetros de integração.',
};

const breadCrumb = {
	home: 'Meus processos',
	processo: 'Meu processo',
	pedidos: 'Pedidos',
};
/**
 * Constantes para os totalizadores
 */
const totaisPedidos = {
	totalProvavel: 'Total provável',
	totalProvavelAtu: 'Total provável atualizado',
	totalPossivel: 'Total possível (R$)',
	totalPossivelAtu: 'Total possível atualizado (R$)',
	totalRemoto: 'Total remoto (R$)',
	totalRemotoAtu: 'Total remoto atualizado (R$)',
};
/**
 * Constantes para o grid
 */
const gridPedidos = {
	pedido: 'Pedido',
	dataValor: 'Data correção',
	provisao: 'Provável (R$)',
	provisaoAtu: 'Provável atualizado (R$)',
	prognostico: 'Prognóstico',
	vlrPedido: 'Valor pedido (R$)',
	vlrAtuPed: 'Valor atualizado (R$)',
	btnNovoPedido: 'Novo pedido',
	btnAlteraEmLote: 'Alterar em lote',
	aguardaAprov: 'Aguardando aprovação',
	quantidadeReg: 'Quantidade de registros',
	vlrRedutor: 'Valor com redutor (R$)',
};

const button = {
	lembrar: 'Lembrar mais tarde',
	nao: 'Não',
	sim: 'Sim',
	avancar: 'Avançar',
	cancelar: 'Cancelar',
};

const gridMigracao = {
	data: 'Data do pedido',
	progn: 'Prognóstico',
	valorPedido: 'Valor',
	valorContig: 'Valor contigência',
};

/**
 * Constantes principais
 */
export const pedidosPt = {
	tituloPrincipal: 'Resumo de pedidos',
	tituloSecundario: 'Pedidos solicitados',
	totais: totaisPedidos,
	grid: gridPedidos,
	breadCrumb,
	modal: msgModal,
	button,
	gridMigracao,
};
