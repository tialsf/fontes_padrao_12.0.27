import { Component, OnInit, ViewChild } from '@angular/core';
import {
	PoBreadcrumb,
	PoNotificationService,
	PoTableColumn,
	PoI18nService,
	PoModalComponent,
	PoModalAction,
	PoSelectComponent,
	PoTableComponent,
} from '@po-ui/ng-components';

import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { FwmodelService } from 'src/app/services/fwmodel.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RoutingService } from 'src/app/services/routing.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Pedido } from './det-pedido/pedido';
import { LegalprocessService } from 'src/app/services/legalprocess.service';

class tipoPedido {
	value: string;
	label: string;
	dataPedido: string;
	valorPedido: number = 0;
	valorContig: number = 0;
	prognostico: string;
	descPrognos: string;
	codPedido: string;
	editarPedido: Array<string>;
	codWf: string;
	urlWf: string;
	valorRedutor: number = 0;
}

class gridMigrPedido {
	data: string;
	codigoPedido: string;
	prognostico: string;
	valorPedido: number;
	valorContig: number;
}

@Component({
	selector: 'app-pedidos',
	templateUrl: './pedidos.component.html',
	styleUrls: ['./pedidos.component.css'],
})
export class PedidosComponent implements OnInit {
	[x: string]: any;
	litPedidos: any;
	formMigracao: FormGroup;
	dadosPedidos = {};
	private arrPedidosInfo: tipoPedido[] = [];
	listPedidosMigracao: gridMigrPedido[] = [];

	cajuri: string = '';
	totalProvavel: string = '';
	totalProvavelAtu: string = '';
	searchKey: string = '';
	tpPedido: string = '';
	msgMigracao: string = '';
	chaveProcesso: string = '';
	filial: string = '';
	cajuriTitulo: string = '';
	qtdPedidos: string = '';

	pedidos: Array<any> = [];
	totaisPedidos: Array<any> = [];
	listPedidos: Array<tipoPedido> = [];

	isHideLoadingPedidos: boolean = false;

	/**
	 *  Colunas dos saldos totais
	 */
	colTotaisPedidos: Array<PoTableColumn> = [
		{
			property: 'totalPossivel',
			label: 'Total possível (R$)',
			type: 'number',
			format: '1.2-5',
		},
		{
			property: 'totalPossivelAtu',
			label: 'Total possível atualizado (R$)',
			type: 'number',
			format: '1.2-5',
		},
		{
			property: 'totalRemoto',
			label: 'Total remoto (R$)',
			type: 'number',
			format: '1.2-5',
		},
		{
			property: 'totalRemotoAtu',
			label: 'Total remoto atualizado (R$)',
			type: 'number',
			format: '1.2-5',
		},
		{
			property: 'totalIncontroverso',
			label: 'Total Incontroverso (R$)',
			type: 'number',
			format: '1.2-5',
		},
		{
			property: 'totalIncontroversoAtu',
			label: 'Total Incontroverso atualizado (R$)',
			type: 'number',
			format: '1.2-5',
		},
	];

	/**
	 * Colunas do Grid de Migração de pedidos
	 */
	colMigracaoPedido: Array<PoTableColumn> = [
		{ property: 'data', label: 'Data do pedido' },
		{ property: 'prognostico', label: 'Prognóstico' },
		{
			property: 'valorPedido',
			label: 'Valor',
			type: 'number',
			format: '1.2-5',
		},
		{
			property: 'valorContig',
			label: 'Valor Contigência',
			type: 'number',
			format: '1.2-5',
		},
	];

	/**
	 *  Colunas da grid de pedidos
	 */
	colPedidos = [
		{ property: 'pedido', label: 'Pedido' },
		{ property: 'dtValor', label: 'Data valor' },
		{
			property: 'vlrProvisao',
			label: 'Valor provisão (R$)',
			type: 'number',
			format: '1.2-5',
		},
		{
			property: 'vlrProvisaoAtu',
			label: 'Valor provisão atual (R$)',
			type: 'number',
			format: '1.2-5',
		},
		{ property: 'prognostico', label: 'Prognóstico' },
		{
			property: 'vlrPedido',
			label: 'Valor pedido (R$)',
			type: 'number',
			format: '1.2-5',
		},
		{
			property: 'vlrAtuPed',
			label: 'Valor atualizado (R$)',
			type: 'number',
			format: '1.2-5',
		},
		{
			property: 'valorRedutor',
			label: 'Valor redutor (R$)',
			type: 'number',
			format: '1.2-5',
		},
		{
			property: 'editarPedido',
			label: ' ',
			type: 'icon',
			icons: [
				{
					value: 'pedido',
					icon: 'po-icon po-icon-edit',
					color: '#29b6c5',
					action: value => {
						this.callPedido(value.codPedido);
					},
				},
				{
					value: 'status',
					icon: 'po-icon po-icon-warning',
					tooltip: 'Aguardando aprovação',
					color: '#29b6c5',
					action: value => {
						this.fluigSolicLink(value.urlWf);
					},
				},
			],
		},
	];

	/**
	 * estrutura de navegação da URL atual
	 */
	public breadcrumb: PoBreadcrumb = {
		items: [],
	};

	fechar: PoModalAction = {
		action: () => {
			this.modalConfirmMigra.close();
		},
		label: 'Não',
	};
	migrar: PoModalAction = {
		action: () => {
			this.modalNSYFromO0W.open();
			this.modalConfirmMigra.close();
		},
		label: 'Sim',
	};
	cancelar: PoModalAction = {
		action: () => {
			this.modalNSYFromO0W.close();
		},
		label: 'Cancelar',
	};
	avancar: PoModalAction = {
		action: () => {
			this.callDetPedidos(true);
		},
		label: 'Avançar',
	};

	confirmaPedido: PoModalAction = {
		label: 'Sim',
		disabled: true,
		action: () => {
			this.callDetPedidos(false);
		},
	};
	checkPedido: PoModalAction = {
		label: 'Não',
		disabled: true,
		action: () => {
			this.showCheck();
		},
	};
	@ViewChild('confirmMigracaoNSY', { static: true }) modalConfirmMigra: PoModalComponent;
	@ViewChild('modalMigracaoNSY', { static: true }) modalNSYFromO0W: PoModalComponent;
	@ViewChild('listPedMigr', { static: true }) cmbPedidoMigracao: PoSelectComponent;
	@ViewChild('tblPedidosMigracao', { static: true }) tblMigraPedido: PoTableComponent;
	@ViewChild('gridPedidos', { static: true }) gridPedidos: PoTableComponent;
	// Rota para a tela de cadastro de pedidos
	createPedido() {
		this.router.navigate(['./cadastro'], { relativeTo: this.route });
	}

	constructor(
		protected thfI18nService: PoI18nService,
		public notification: PoNotificationService,
		private fwmodel: FwmodelService,
		private route: ActivatedRoute,
		private JuriGenerics: JurigenericsService,
		private routingState: RoutingService,
		private router: Router,
		private formBuilder: FormBuilder,
		private legalprocess: LegalprocessService
	) {
		this.routingState.loadRouting();

		this.filial = this.route.snapshot.paramMap.get('filPro');
		window.sessionStorage.setItem('filial', atob(this.filial));

		this.chaveProcesso = this.route.snapshot.paramMap.get('codPro');
		this.cajuri = atob(decodeURIComponent(this.chaveProcesso));

		thfI18nService
			.getLiterals({ context: 'pedidos', language: thfI18nService.getLanguage() })
			.subscribe(litPedidos => {
				this.litPedidos = litPedidos;

				this.breadcrumb.items = [
					{ label: this.litPedidos.breadCrumb.home, link: '/' },
					{
						label: this.litPedidos.breadCrumb.processo,
						link: '/processo/' + this.filial + '/' + this.chaveProcesso,
					},
					{ label: this.litPedidos.breadCrumb.pedidos },
				];
				/**
				 * Adiciona o código do cajuri no título Meu processo + área
				 */
				this.cajuriTitulo = this.cajuri.replace(new RegExp('^0*', 'g'), '');

				/**
				 * seta as constantes para tradução do titulo dos totalizadores
				 */
				this.colTotaisPedidos[0].label = this.litPedidos.totais.totalPossivel;
				this.colTotaisPedidos[1].label = this.litPedidos.totais.totalPossivelAtu;
				this.colTotaisPedidos[2].label = this.litPedidos.totais.totalRemoto;
				this.colTotaisPedidos[3].label = this.litPedidos.totais.totalRemotoAtu;

				/**
				 * seta as constantes para tradução do titulo das colunas do Grid de Pedidos
				 */
				this.colPedidos[0].label = this.litPedidos.grid.pedido;
				this.colPedidos[1].label = this.litPedidos.grid.dataValor;
				this.colPedidos[2].label = this.litPedidos.grid.provisao;
				this.colPedidos[3].label = this.litPedidos.grid.provisaoAtu;
				this.colPedidos[4].label = this.litPedidos.grid.prognostico;
				this.colPedidos[5].label = this.litPedidos.grid.vlrPedido;
				this.colPedidos[6].label = this.litPedidos.grid.vlrAtuPed;
				this.colPedidos[7].label = this.litPedidos.grid.vlrRedutor;
				this.colPedidos[8].icons[1].tooltip = this.litPedidos.grid.aguardaAprov;

				this.fechar.label = this.litPedidos.button.lembrar;
				this.migrar.label = this.litPedidos.button.sim;
				this.cancelar.label = this.litPedidos.button.cancelar;
				this.avancar.label = this.litPedidos.button.avancar;
				this.checkPedido.label = this.litPedidos.button.nao;
				this.confirmaPedido.label = this.litPedidos.button.sim;

				this.colMigracaoPedido[0].label = this.litPedidos.gridMigracao.data;
				this.colMigracaoPedido[1].label = this.litPedidos.gridMigracao.progn;
				this.colMigracaoPedido[2].label = this.litPedidos.gridMigracao.valorPedido;
				this.colMigracaoPedido[3].label = this.litPedidos.gridMigracao.valorContig;

				this.msgMigracao = this.litPedidos.modal.msgGridPed;
			});
	}

	ngOnInit() {
		this.getPedidos();
		this.getInfoProcesso();
		this.searchKey = '';
		this.checksPedidos();
		this.createFormMigracao(new Pedido());
		this.getConfigRedutores();
	}

	/**
	 * Busca os pedidos do processo
	 */
	getPedidos() {
		this.isHideLoadingPedidos = false;
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA270');
		this.fwmodel.setFilter("NSZ_COD='" + this.cajuri + "'");
		this.fwmodel.setFilter("NSZ_FILIAL='" + atob(this.filial) + "'");
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.setCampoVirtual(true);

		this.fwmodel.get('Busca Pedido').subscribe(data => {
			if (data !== '' && data.resources[0].models[0].models[0].items !== undefined) {
				this.totalProvavel = this.JuriGenerics.findValueByName(
					data.resources[0].models[0].fields,
					'NSZ_TOTPRO'
				);
				this.totalProvavelAtu = this.JuriGenerics.findValueByName(
					data.resources[0].models[0].fields,
					'NSZ_TATPRO'
				);
				this.totaisPedidos = [
					{
						totalPossivel: this.JuriGenerics.findValueByName(
							data.resources[0].models[0].fields,
							'NSZ_TOTPOS'
						),
						totalPossivelAtu: this.JuriGenerics.findValueByName(
							data.resources[0].models[0].fields,
							'NSZ_TATPOS'
						),
						totalRemoto: this.JuriGenerics.findValueByName(
							data.resources[0].models[0].fields,
							'NSZ_TOTREM'
						),
						totalRemotoAtu: this.JuriGenerics.findValueByName(
							data.resources[0].models[0].fields,
							'NSZ_TATREM'
						),
						totalIncontroverso: this.JuriGenerics.findValueByName(
							data.resources[0].models[0].fields,
							'NSZ_TOTINC'
						),
						totalIncontroversoAtu: this.JuriGenerics.findValueByName(
							data.resources[0].models[0].fields,
							'NSZ_TATINC'
						),
					},
				];
				let indice: number = 1;
				this.qtdPedidos = data.resources[0].models[0].models[0].items.length.toString();
				data.resources[0].models[0].models[0].items.forEach(item => {
					this.dadosPedidos = {
						codPedido: this.JuriGenerics.findValueByName(item.fields, 'O0W_COD'),
						indice: indice++,
						pedido: this.JuriGenerics.findValueByName(item.fields, 'O0W_DTPPED'),
						dtValor: this.JuriGenerics.makeDate(
							this.JuriGenerics.findValueByName(item.fields, 'O0W_DATPED'),
							'dd/mm/yyyy'
						),
						vlrProvisao: this.JuriGenerics.findValueByName(item.fields, 'O0W_VPROVA'),
						vlrProvisaoAtu: this.JuriGenerics.findValueByName(
							item.fields,
							'O0W_VATPRO'
						),
						prognostico: this.JuriGenerics.findValueByName(item.fields, 'O0W_PROGNO'),
						vlrPedido: this.JuriGenerics.findValueByName(item.fields, 'O0W_VPEDID'),
						vlrAtuPed: this.JuriGenerics.findValueByName(item.fields, 'O0W_VATPED'),
						codWf: this.JuriGenerics.findValueByName(item.fields, 'O0W_CODWF'),
						valorRedutor: this.JuriGenerics.findValueByName(item.fields, 'O0W_VLREDU'),
						urlWf: this.JuriGenerics.findValueByName(item.fields, 'O0W_URLFLG'),
						editarPedido: ['pedido'],
					};
					this.pedidos.push(this.dadosPedidos);
				});
				this.getStatus();
			}
			this.isHideLoadingPedidos = true;
		});
	}

	/**
	 * Busca as informações do processo
	 */
	getInfoProcesso() {
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA095');
		this.fwmodel.setFilter("NSZ_COD='" + this.cajuri + "'");
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.get('getInfoProcesso').subscribe(data => {
			if (data.resources !== '') {
				this.breadcrumb.items[1].label =
					this.litPedidos.breadCrumb.processo +
					' ' +
					this.JuriGenerics.findValueByName(
						data.resources[0].models[0].fields,
						'NSZ_DAREAJ'
					) +
					' (' +
					this.cajuriTitulo +
					')';
			}
		});
	}

	/**
	 * Verifica se há pedidos a serem migrados
	 */
	checksPedidos() {
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA094');
		this.fwmodel.setFilter("NSY_CAJURI='" + this.cajuri + "' and NSY_CVERBA = ' ' ");
		this.fwmodel.setPageSize('100');
		this.fwmodel.setCampoVazio(true);
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.get('getPedidosNSY').subscribe(data => {
			// Verifica se há campos vazios
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != '') {
				data.resources.forEach(item => {
					let itemPedido = new tipoPedido();
					itemPedido.value = this.JuriGenerics.findValueByName(
						item.models[0].fields,
						'NSY_CPEVLR'
					);
					itemPedido.label = this.JuriGenerics.findValueByName(
						item.models[0].fields,
						'NSY_DPEVLR'
					);
					itemPedido.dataPedido = this.JuriGenerics.findValueByName(
						item.models[0].fields,
						'NSY_DTCONT'
					);
					itemPedido.prognostico = this.JuriGenerics.findValueByName(
						item.models[0].fields,
						'NSY_CPROG'
					);
					itemPedido.descPrognos = this.JuriGenerics.findValueByName(
						item.models[0].fields,
						'NSY_DPROG'
					);
					itemPedido.valorPedido = this.JuriGenerics.findValueByName(
						item.models[0].fields,
						'NSY_PEVLR'
					);
					itemPedido.valorContig = this.JuriGenerics.findValueByName(
						item.models[0].fields,
						'NSY_VLCONT'
					);
					itemPedido.codPedido = this.JuriGenerics.findValueByName(
						item.models[0].fields,
						'NSY_COD'
					);

					if (this.JuriGenerics.changeUndefinedToEmpty(itemPedido.dataPedido) === '') {
						itemPedido.dataPedido = this.JuriGenerics.findValueByName(
							item.models[0].fields,
							'NSY_PEDATA'
						);
					}

					if (itemPedido.valorContig > 0 || itemPedido.valorPedido > 0) {
						this.listPedidos.push(itemPedido);
					}
					this.arrPedidosInfo.push(itemPedido);
				});
				this.modalConfirmMigra.open();
			}
		});
	}

	/**
	 * Preenche a tabela do Tipo de Pedido
	 */
	callTabMigracao() {
		// Zera os valores anteriores
		const arrPedidoSelecionado: tipoPedido[] = this.arrPedidosInfo.filter(
			x =>
				x.value === this.cmbPedidoMigracao.selectedValue &&
				(x.valorPedido > 0 || x.valorContig > 0)
		);
		window.sessionStorage.setItem('TLPedidosMigracao', '');
		this.tblMigraPedido.checkbox = false;
		this.msgMigracao = this.litPedidos.modal.msgGridPed;
		this.listPedidosMigracao = [];

		// Incluir as linhas dos Grids
		arrPedidoSelecionado.forEach(pedido => {
			const itemGrid: gridMigrPedido = new gridMigrPedido();
			itemGrid.data = this.JuriGenerics.makeDate(pedido.dataPedido, 'dd/mm/yyyy');
			itemGrid.prognostico = pedido.descPrognos;
			itemGrid.valorPedido = pedido.valorPedido;
			itemGrid.valorContig = pedido.valorContig;
			itemGrid.codigoPedido = pedido.codPedido;

			this.listPedidosMigracao.push(itemGrid);
		});

		// Habilita os botoões caso tenha linhas no Grid
		this.modalNSYFromO0W.primaryAction = this.confirmaPedido;
		this.modalNSYFromO0W.secondaryAction = this.checkPedido;

		this.modalNSYFromO0W.primaryAction.disabled = this.listPedidosMigracao.length === 0;
		this.modalNSYFromO0W.secondaryAction.disabled = this.listPedidosMigracao.length === 0;
	}

	/**
	 * Form da migração
	 * @param pedidos dados do form
	 */
	createFormMigracao(pedidos: Pedido) {
		this.formMigracao = this.formBuilder.group({
			tpPedido: [pedidos.tpPedido, Validators.compose([Validators.required])],
		});
	}

	/**
	 * Se for seleção com Checkbox, joga os pedidos em um Array no SessionStorage
	 * @param bGetItensCheck se o item esta selecionado
	 */
	callDetPedidos(bGetItensCheck: boolean = false) {
		const tipoPedido = this.formMigracao.value.tpPedido;
		if (bGetItensCheck) {
			// Pega os pedidos selecionados para concatenar
			const itensSelecionados = this.tblMigraPedido.getSelectedRows();
			const arrCodPedidos: string[] = [];

			// Incluir no array de pedidos
			itensSelecionados.forEach(item => {
				arrCodPedidos.push(item.codigoPedido);
			});
			window.sessionStorage.setItem('TLPedidosMigracao', JSON.stringify(arrCodPedidos));
		}

		this.router.navigate(['./m/' + btoa(tipoPedido)], { relativeTo: this.route });
	}

	/**
	 * Mostra o Checkbox e troca os botões do Modal
	 */
	showCheck() {
		this.tblMigraPedido.checkbox = true;
		this.modalNSYFromO0W.primaryAction = this.avancar;
		this.modalNSYFromO0W.secondaryAction = undefined;
		this.msgMigracao = this.litPedidos.modal.selecionarPedidos;
	}

	callPedido(dadosLinha?: string) {
		this.router.navigate(['./' + btoa(dadosLinha)], { relativeTo: this.route });
	}

	/**
	 * Obtém o status da solicitação no fluig
	 */
	getStatus() {
		this.pedidos.forEach(element => {
			if (this.JuriGenerics.changeUndefinedToEmpty(element.codWf) !== '') {
				this.legalprocess.restore();
				this.legalprocess
					.get('solicitation/' + element.codWf, 'getStatus')
					.subscribe(data => {
						if (data.status !== ' ' && data.status) {
							element.editarPedido = ['pedido', 'status'];
						}
					});
			}
		});
	}

	/**
	 * Redireciona para o link da solicitação no Fluig
	 * @param urlWf URL do Workflow Fluig
	 */
	fluigSolicLink(urlWf) {
		console.log(urlWf);
		if (urlWf !== '') {
			window.open(urlWf);
		} else {
			this.notification.error(this.litPedidos.modal.alertLinkFluig);
		}
	}

	/**
	 * Verifica se há configurações de redutores cadastradas
	 * se não houver, as informaçoes de redutor são ocultadas
	 */
	getConfigRedutores() {
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA258');
		this.fwmodel.get('Busca registros na tabela de redutores').subscribe(data => {
			if (data !== undefined && data !== '' && data.total === 0) {
				/** Retira a coluna de valor de valor de redutor da tabela */
				this.gridPedidos.mainColumns.splice(7, 1);
				this.colPedidos = this.gridValores.mainColumns;
			}
		});
	}
}
