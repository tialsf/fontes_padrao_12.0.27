/**
* Arquivo de constantes para tradução da tela de processos(resumo)
*/

//--  Constantes para o compartilhamento do processo
const share = {
	
	shareWhatsApp: "Acesse o TOTVS Jurídico - Processo: "
}

//--  Constantes para o breadcrumb de Processo
const breadcrumbProcesso = {
	
	brHome: "Meus processos",
	brProcesso: "Meu processo"
}

//--  Constantes para a tabela de valores
const tableValues = {
	
	envolvido:  "Envolvido (R$)",
	provisao:   "Provisão (R$)",
	acordo:     "Alçada acordo (R$)",
	juizo:      "Saldo em juízo (R$)",
	deposito:   "Total depósitos (R$)",
	despesa:    "Total despesas (R$)",
	carregando: "Carregando Valores",
	causa:      "Causa (R$)"
}

//--  Constantes para a tabela de valores
const tableContingencia = {
	
	provavel:      "Total provável (R$)",
	possivel:      "Total possível (R$)",
	remoto:        "Total remoto (R$)",
	incontroverso: "Total incontroverso (R$)",
	carregando:    "Carregando contingência"
}
//-- Constantes para os campos de detalhe
const camposDetalhe = {

	escritorio:  'Escritorio',
	envolvidos:  'Envolvidos',  
	instancia:   'Instância',
	localizacao: 'Localização' ,
	numprocesso: 'Numero do Processo',
	dataEntrada: 'Data de Entrada',
	responsavel: 'Advogado Responsável',
	assunto:     'Assunto',
	situacao:    'Situação'
}

//-- Constantes para tradução da widget de andamentos
const andamentosProc = {
	data:        "Data",
	ato:         "Ato",
	fase:        "Fase",
	descricao:   "Descrição", 
	tituloAnd:   "Andamentos",
	verTodosAnd: "Ver todos",
	incluirAnd:  "Incluir novo andamento",
	carregando:  "Carregando Andamentos"
}

//-- Constantes para títulos das colunas do Grid de Garantias
const gridGarantias = {	
	colunaData:        "Data", 
	colunaTipo:        "Tipo",
	colunaValor:       "Valor (R$)",
	colunaSaldoAtu:    "Saldo atualizado (R$)"
}

//-- Constantes para tradução da widget de Tarefas
const tarefas = {	
	colunaData:        "Data", 
	colunaTipo:        "Tipo",
	colunaStatus:      "Status",
	colunaResponsavel: "Responsável",
	btnIncluirTarefa:  "Incluir nova tarefa",
	btnVerTodasTarefa: "Ver todos",
	tituloTarefas:     "Prazos e Tarefas",
	loadTarefas:       "Carregando Prazos e Tarefas"

}
//-- Constantes para tradução da widget de encerramento
const encerramento = {
	encerramento: "Encerramento",
	dataEnc:      "Data",
	motivo:       "Motivo",
	usuario:      "Usuário",
	detalhe:      "Detalhamento",
	encerrar:     "Encerrar processo",
	erroVldEnc:   'Existem campos obrigatórios não preenchidos, favor verifique.'
}

//-- Constantes para tradução da widget de Pedido
const pedidosProc = {
	pedido: "Pedido",
	dataValor: "Data valor",
	provisao: "Provisão (R$)",
	provisaoAtu: "Provisão atualizado (R$)",
	prognostico: "Prognóstico",
	vlrPedido: "Valor pedido (R$)",
	tituloPedidos: "Pedidos",
	btnVerTodos: "Ver todos",
	btnIncluirNovoPed: "Incluir novo pedido",
	loadPedidos: "Carregando pedidos",
	aguardaAprov: "Aguardando aprovação",
	alertLinkFluig: 'Não foi possível acessar o link da Solicitação de Aprovação. Verifique os parâmetros de integração.'
}
const gridDespesa = {
	tipoDespesa: "Despesa",
	dataDespesa: "Data",
	valorDespesa: "Valor",
	edtDespesa: " "
}
const despesasProc = {
	btnVerTodos: "Ver todos",
	btnIncluirNovoDesp: "Incluir nova despesa",
	tituloDespesas: "Despesas",
	loadDespesas: "Carregando despesas",
	gridDespesa: gridDespesa
}

const compartilharResumo = {
	whatsapp:  "Link via WhatsApp",
	pdf:       "Baixar PDF",
	msgInfo:   "O resumo do processo será gerado em segundo plano, assim que o processo for concluído você será notificado.",
	title:     "Compartilhar resumo",
	msgSucess: "Resumo em PDF gerado com sucesso!",
	msgErro:   "Erro ao gerar PDF"
}
const outrasOpcoes = {
	historico: "Histórico de alterações deste processo"
}

//-- Constantes para tradução de mensagens de erros
const erro = {
}

const listSimNao = {
	sim: "Sim",
	nao: "Não"
}

//-- Constantes principais
export const processoPt = {
	meuProcesso:    "Meu processo ",
	favExcluido:    "Processo Desfavoritado.",
	favoritar:      "Processo Favoritado.",
	anexos:         "Anexos",
	detalhes:       "Ver Detalhes",
	valores:        "Valores",
	contingencia:   "Contingência",
	carregando:     "Carregando Informações...",
	loadGar:        "Carregando Garantias",
	loadEnc:        "Carregando Encerramento",
	tituloGar:      "Garantias",
	btnIncluirGar:  "Incluir nova garantia",
	btnVerTodasGar: "Ver todos",
	minimizar:      "Minimizar",
	maximizar:      "Maximizar",
	naoEncontrado:  "Esse processo não foi localizado na base ou pode ter sido excluído.",
	alterado:       "Processo alterado com sucesso.",
	encerrado:      "Processo encerrado com sucesso.",
	btnEncerrar:    "Encerrar processo",
	btnReabrir:     "Reabrir processo",
	breadcrumbProc: breadcrumbProcesso,
	camposDet:      camposDetalhe,
	valuesTable:    tableValues,
	andamentos:     andamentosProc,
	gridGarantia:	gridGarantias,
	tarefas:        tarefas,
	encerramento:   encerramento,
	pedidos:        pedidosProc,
	despesas:       despesasProc,
	msgErro:        erro,
	outrasOpcoes:   outrasOpcoes,
	btnHistorico:   "Histórico",
	share:          share,
	compartilhar:   compartilharResumo,
	listSimNao:     listSimNao,
	tableConting:   tableContingencia
}