import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProcessoComponent } from './processo.component';
import { AndamentosComponent } from '../andamentos/andamentos.component';
import { DetAndamentoComponent } from '../andamentos/det-andamento/det-andamento.component';
import { AnexosComponent } from '../anexos/anexos.component';
import { GarantiasComponent } from './garantias/garantias.component';
import { DetGarantiaComponent } from './garantias/det-garantia/det-garantia.component';
import { TarefasComponent } from '../tarefas/tarefas.component';
import { DetTarefaComponent } from '../tarefas/det-tarefa/det-tarefa.component';
import { DetPedidoComponent } from './pedidos/det-pedido/det-pedido.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { DetProcessoComponent } from './det-processo/det-processo.component';
import { DespesasComponent } from '../despesas/despesas.component';
import { DetDespesaComponent } from '../despesas/det-despesa/det-despesa.component';
import { DetLevantamentoComponent } from './garantias/det-garantia/det-levantamento/det-levantamento.component';
import { HistoricoComponent } from '../historico/historico.component';
import { AltProcessoComponent } from './alt-processo/alt-processo.component';

const routes: Routes = [
	{
		path: '',
		component: ProcessoComponent
	},
	{
		path: 'cadastro',
		component: DetProcessoComponent
	},
	{
		path: ':filPro/:codPro',
		children: [
			{
				path: '',
				pathMatch: 'full',
				component: ProcessoComponent

			},
			{
				path: 'andamento',
				children: [
					{
						path: '',
						pathMatch: 'full',
						component: AndamentosComponent
					},
					{
						path: 'cadastro',
						component: DetAndamentoComponent
					},
					{
						path: ':codAnd',
						children: [
							{
								path: '',
								pathMatch: 'full',
								component: DetAndamentoComponent,
							},
							{
								path: 'anexos',
								component: AnexosComponent
							},
							{
								path: 'tarefa',
								redirectTo: ''
							},
							{
								path: 'tarefa/cadastro',
								component: DetTarefaComponent,
							},
							{
								path: 'tarefa/:codTar',
								children: [
									{
										path: '',
										pathMatch: 'full',
										component: DetTarefaComponent
									},
									{
										path: 'anexos',
										component: AnexosComponent
									}

								]
							}	
						]
					}
				]
			},
			{
				path: 'garantia',
				children: [
					{
						path: '',
						pathMatch: 'full',
						component: GarantiasComponent
					},
					{
						path: 'cadastro',
						component: DetGarantiaComponent
					},
					{
						path: ':codGar',
						children: [
							{
								path: '',
								pathMatch: 'full',
								component: DetGarantiaComponent,
							},
							{
								path: 'anexos',
								component: AnexosComponent
							},
							{
								path: 'levantamento',
								redirectTo: ''
							},
							{
								path: 'levantamento/cadastro',
								component: DetLevantamentoComponent,
							},
							{
								path: 'levantamento/:codLev',
								component: DetLevantamentoComponent
							}
						]
					}
				]
			},
			{
				path: 'tarefa',
				children: [
					{
						path: '',
						pathMatch: 'full',
						component: TarefasComponent
					},
					{
						path: 'cadastro',
						component: DetTarefaComponent
					},
					{
						path: ':codTar',
						children: [
							{
								path: '',
								pathMatch: 'full',
								component: DetTarefaComponent,
							},
							{
								path: 'anexos',
								component: AnexosComponent
							}
						]
					}
				]
			},
			{
				path: 'pedido',
				children: [
					{
						path: '',
						pathMatch: 'full',
						component: PedidosComponent
					},
					{
						path: 'm/:codTipPedido',
						component: DetPedidoComponent
					},
					{
						path: 'cadastro',
						component: DetPedidoComponent
					},
					{
						path: ':codPedido',
						children: [
							{
								path: '',
								pathMatch: 'full',
								component: DetPedidoComponent,
							},
							{
								path: 'anexos',
								component: AnexosComponent
							}
						]
					}
				]
			},
			{
				path: 'despesa',
				children: [
					{
						path: '',
						pathMatch: 'full',
						component: DespesasComponent
					},
					{
						path: 'cadastro',
						component: DetDespesaComponent
					},
					{
						path: ':codDesp',
						children: [
							{
								path: '',
								pathMatch: 'full',
								component: DetDespesaComponent,
							},
							{
								path: 'anexos',
								component: AnexosComponent
							}
						]
					}
				]
			},
			{
				path: 'anexos',
				component: AnexosComponent
			},
			{
				path: 'historico',
				component: HistoricoComponent
			},
			{
				path: 'altProcesso',
				component: AltProcessoComponent
			},
		]
	},
	
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ProcessoRoutingModule { }
