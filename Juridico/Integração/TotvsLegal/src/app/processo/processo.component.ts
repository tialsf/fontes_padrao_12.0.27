import { JurigenericsService, JurBase64 } from '../services/jurigenerics.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { LegalprocessService } from '../services/legalprocess.service';
import { FwmodelService, FwModelBody, FwModelBodyModels } from '../services/fwmodel.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TLUserInfo } from "../services/authlogin.service";
import { Title }     from '@angular/platform-browser';
import { RoutingService } from '../services/routing.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Processo } from './processo';
import { TarefasService } from '../tarefas/tarefas.service';
import { DespesasService } from '../despesas/despesas.service';
import { AndamentosService } from '../andamentos/andamentos.service';
import { JurconsultasService  } from 'src/app/services/jurconsultas.service'
import { JurJustifModalComponent } from 'src/app/shared/jur-justif-modal/jur-justif-modal.component';
import {
	PoBreadcrumbItem,
	PoDynamicViewField,
	PoNotificationService,
	PoTableColumn,
	PoI18nService,
	PoPageAction,
	PoModalAction,
	PoRadioGroupComponent,
	PoModalComponent,
	PoSelectOption,
	PoDynamicFormField,
	PoTableComponent
} from '@po-ui/ng-components';

class motivoEncerramento{
	value: string;
	label: string;
}

class Compartilhar{
	value: string;
	label: string;
}

class Despesa{
	codDespesa:  string;
	tipoDesp:    string;
	valor:       number;
	data:        string;
	btnEditDesp: Array<string>;
}

class Tarefa{
	codTarefa:   string;
	data:        string;
	tipo:        string;
	status:      string;
	responsavel: string;
	tarefaEdit:  string;
}

class Andamento{
	codAndamento:   string;
	data:           string;
	ato:            string;
	fase:           string;
	descricao:      string;
	editaAndamento: string;
}

class Pedido{
	pedido:         string;
	dtValor:        string;
	vlrProvisao:    string;
	vlrProvisaoAtu: string;
	prognostico:    string;
	vlrPedido:      string;
	codIndice:      string;
	codPedido:      string;
	codWf:          string;
	urlWf:          string;
	editarPedido:   Array<string>;
}

class Valores{
	atualizedCause :    string;
	atualizedinvolved:  string;
	atualizedprovision: string;
	balanceInCourt:     string;
	balanceguarantees:  string;
	balanceExpenses:    string;
}

@Component({
	selector: 'app-processo',
	templateUrl: './processo.component.html',
	styleUrls: ['./processo.component.css']
})
export class ProcessoComponent implements OnInit {
	formEncerramento:          FormGroup;
	litProcesso:               any;
	breadcrumbItem:            PoBreadcrumbItem;
	breadcrumb:                Array<PoBreadcrumbItem> ;
	breadcrumbProperties:      object;
	breadcrumbParams:          object                    = {};
	oValores:                  object                    = {};
	DadosProcessoSticky                                  = {};
	
	dateStr:                   string                    = '';
	opcaoCompartilhar:         string                    = '';
	tipoAssuntoJuridico:       string                    = '';
	cajuri:                    string                    = '';
	cajuriTitulo:              string                    = '';
	chaveProcesso:             string                    = '';
	filial:                    string                    = '';
	area:                      string                    = '';
	envolvidos:                string                    = '';
	btnSituacaoProcess:        string                    = '';
	classDetEncerramento:      string                    = 'po-lg-8 po-mr-4 po-mb-8 form-group';
	classBtnSalvarSituac:      string                    = 'po-pull-right po-mt-4 po-mr-1';
	codSituac:                 string                    = '';
	pk:                        string                    = '';
	user:                      string                    = '';
	dtEncerramento:            string                    = '';

	coluna:                    Array<PoTableColumn>      = [];
	listValores:               Array<any>                = [];
	listContingencia:          Array<any>                = [];
	garantias:                 Array<any>                = [];
	garantiasList:             Array<any>                = [];
	listMotivos:               Array<motivoEncerramento> = [];
	listDespesa:               Array<Despesa>            = [];
	listTarefas:               Array<Tarefa>             = [];
	listAndamentos:            Array<Andamento>          = [];
	listPedidos:               Array<Pedido>             = [];
	listExtraFields:           Array<PoDynamicFormField> = [];
	listAuxExtField:           Array<PoDynamicFormField> = [];
	motivoEncerramento:        motivoEncerramento        = new motivoEncerramento;
	
	isHideLoadingAndamentos:   boolean                   = false;
	isHideLoadingGarantias:    boolean                   = false;
	isHideLoadingProcesso:     boolean                   = false;
	isHideLoadingValores:      boolean                   = false;
	isHideLoadingContingencia: boolean                   = false;
	isHideLoadingTarefas:      boolean                   = false;
	isHideLoadingEncerramento: boolean                   = false;
	isHideLoadingPedidos:      boolean                   = false;
	isHideLoadingDespesas:     boolean                   = false;
	toggleEncerra:             boolean                   = false;
	isEncerrado:               boolean                   = false;
	hasData:                   boolean                   = true;
	mostraWidgetAndamentos:    boolean                   = true;
	mostraWidgetGarantias:     boolean                   = true;
	mostraWidgetTarefas:       boolean                   = true;
	mostraWidgetPedidos:       boolean                   = true;
	mostraWidgetDespesas:      boolean                   = true;
	toggleAndamentos:          boolean                   = true;
	toggleGarantias:           boolean                   = true;
	toggleTarefas:             boolean                   = true;
	toggleDespesas:            boolean                   = true;
	togglePedidos:             boolean                   = true;
	mostraWidgetContingencia:  boolean                   = true;

	DadosProcesso = {
		escritorio:     '',
		envolvidos:     '',
		instancia:      '',
		localizacao:    '',
		numeroprocesso: '',
		dataEntrada:    '',
		responsavel:    '',
		assunto:        '',
		situacao:       '',
		urlPastaFluig:  ''
	};

	/**
	* colunas da tabela de valores
	*/
	colGridValores: Array<PoTableColumn>  = [
		{ property: 'atualizedCause'     , label: '', type: 'number', format: '1.2-5'},
		{ property: 'atualizedinvolved'  , label: '', type: 'number', format: '1.2-5'},
		{ property: 'atualizedprovision' , label: '', type: 'number', format: '1.2-5'},
		{ property: 'balanceInCourt'     , label: '', type: 'number', format: '1.2-5'},
		{ property: 'balanceguarantees'  , label: '', type: 'number', format: '1.2-5'},
		{ property: 'balanceExpenses'    , label: '', type: 'number', format: '1.2-5'}
	];
	/**
	* colunas da tabela de contingencia
	*/
	colGridContingencia: Array<PoTableColumn>  = [
		{ property: 'totalProvavel'      , label: '', type: 'number', format: '1.2-5', width: '25%'},
		{ property: 'totalPossivel'      , label: '', type: 'number', format: '1.2-5', width: '25%'},
		{ property: 'totalRemoto'        , label: '', type: 'number', format: '1.2-5', width: '25%'},
		{ property: 'totalIncontroverso' , label: '', type: 'number', format: '1.2-5', width: '25%' },
	];
	/**
	 * Colunas da widget de detalhes do processo
	 */
	camposProcesso: Array<PoDynamicViewField> = [
		{ property: 'escritorio'    , gridColumns: 3 },
		{ property: 'envolvidos'    , gridColumns: 3 },
		{ property: 'instancia'     , gridColumns: 3 },
		{ property: 'localizacao'   , gridColumns: 3 },
		{ property: 'numeroprocesso', gridColumns: 3 },
		{ property: 'dataEntrada'   , gridColumns: 3 },
		{ property: 'responsavel'   , gridColumns: 3 },
		{ property: 'assunto'       , tag: true, gridColumns: 2, color: 'color-01' },
		{ property: 'situacao'      , tag: true, gridColumns: 1 }
	];

	// Colunas da widget de detalhes do processo fixo
	CamposProcessoSticky: Array<PoDynamicViewField> = [
		{ property: 'escritorio'    ,  gridColumns: 3 },
		{ property: 'envolvidos'    ,  gridColumns: 3 },
		{ property: 'numeroprocesso', gridColumns: 3 },
		{ property: 'localizacao'   , gridColumns: 3 }
	];

	// Colunas da widget de Andamentos
	colAndamentos: Array<PoTableColumn> = [
		{ property: 'data'          , label: 'Data'},
		{ property: 'ato'           , label: 'Ato'},
		{ property: 'fase'          , label: 'Fase'},
		{ property: 'descricao'     , label: 'Teor'},
		{ property: 'editaAndamento', label: ' ', type: 'icon', width: "4%",
		action: (value,row)=>{
			this.callAndamento(value);
		 }, 
		icons: [{ value: 'editarAnd', icon: 'po-icon po-icon-edit', color: '#29b6c5' }] 
		}
	];

	// Colunas da widget de Garantias
	colGarantias: Array<PoTableColumn> = [
		{ property: 'data', label: 'Data' },
		{ property: 'tipo', label: 'Tipo' },
		{ property: 'valor', label: '', type: 'number', format: '1.2-5' },
		{ property: 'saldoatu', label: 'Saldo atualizado (R$)', type: 'number', format: '1.2-5' },
		{
			property: 'gar', label: ' ', type: 'icon',
			action: (value, row) => {
				this.callGarantia(value);
			},
			icons: [{ value: 'garantia', icon: 'po-icon po-icon-edit', color: '#29b6c5' }]
		}
	];

	/**
	 *  Colunas da widget de Tarefas 
	 * */
	colTarefas: Array<PoTableColumn> = [
		{ property: 'data'       , label: 'Data' },
		{ property: 'tipo'       , label: 'Tipo' },
		{ property: 'status'     , label: 'Status' },
		{ property: 'responsavel', label: 'Responsável' },
		{ property: 'tarefaEdit' , label: ' ', type: 'icon',
			action: (value,row)=>{
				this.updateTarefa(value);},
			icons: [{ value: 'tarefa', icon: 'po-icon po-icon-edit', color: '#29b6c5' }]
		}
	];
	
	/**
	 *  Colunas da widget de Pedidos
	 * */
	colPedidos: Array<PoTableColumn> = [
		{ property: 'pedido'        , label: 'Pedido' },
		{ property: 'dtValor'       , label: 'Data valor' },
		{ property: 'vlrProvisao'   , label: 'Valor provisão (R$)'           , type: 'number', format: '1.2-5' },
		{ property: 'vlrProvisaoAtu', label: 'Valor provisão atualizado (R$)', type: 'number', format: '1.2-5' },
		{ property: 'prognostico'   , label: 'Prognóstico' },
		{ property: 'vlrPedido'     , label: 'Valor pedido (R$)'             , type: 'number', format: '1.2-5' },
		{ property: 'editarPedido'  , label: ' ', type: 'icon', 
			icons: [{ value: 'pedido', icon: 'po-icon po-icon-edit', color: '#29b6c5',
						action: (value)=>{
							this.callPedido(value);
						}
					},
					{ value: 'status', icon: 'po-icon po-icon-warning', tooltip: 'Aguardando aprovação', color: '#29b6c5',
						action: (value)=>{
							this.fluigSolicLink(value.urlWf);
						}
					}]
		}
	];

	
	listCompResumo: Array<Compartilhar> =[
		{value: '1', label: 'Link via whatsApp'},
		{value: '2', label: 'baixar PDF'}
	];
	/**
	 *  Colunas da widget de despesas
	 * */
	colDespesas: Array<PoTableColumn> = [
		{ property: 'data'       , label: "Data" },
		{ property: 'tipoDesp'   , label: "Tipo" },
		{ property: 'valor'      , label: "Valor", type: 'number', format: '1.2-5'},
		{ property: 'btnEditDesp', label: ' ', type: 'icon',
			icons: [{ 
				value:' ', icon: 'po-icon po-icon-edit', color: '#29b6c5' 
			}],
			action: (value, row)=>{
				this.callDespesa(value);
			}
		}
	];

	// Botões posicionados ao lado direito do título "Meu Processo"
	pageActions: Array<PoPageAction> = [
		{ label: ''         , icon: 'po-icon po-icon-share', action: this.openModal.bind(this)},
		{ label: 'Histórico', action: this.openHist.bind(this)}
	];

	fechar: PoModalAction = {
		action: () => {
			this.modalCompartilhar.close();
			this.radio.value = '';
			this.opcaoCompartilhar = '';
		},
		label: 'Fechar'
	};
	
	confirmar: PoModalAction = {
		action: () => {
			this.compResumoOpc();
		},
		label: 'Confirmar'
	};

	@ViewChild('compartilhar'        , { static: true })  modalCompartilhar: PoModalComponent;
	@ViewChild(PoRadioGroupComponent , { static: true })  radio:             PoRadioGroupComponent;
	@ViewChild('modJustif'           , { static: true  }) modalJustif:       JurJustifModalComponent;
	@ViewChild('gridValores'         , { static: true  }) gridValores:       PoTableComponent;
	/**
	 * construtor da classe
	 */
	constructor(
		private legalprocess:   LegalprocessService,
		private fwmodel:        FwmodelService,
		private route:          ActivatedRoute,
		private userInfo:       TLUserInfo,
		private JuriGenerics:   JurigenericsService,
		public thfNotification: PoNotificationService,
		private titleService:   Title,
		thfI18nService:         PoI18nService,
		private routerState:    RoutingService,
		private router:         Router,
		private formBuilder:    FormBuilder,
		private tarefas:        TarefasService,
		private despesas:       DespesasService,
		private andamentos:     AndamentosService,
		private jurConsultas:   JurconsultasService
	)
	{
		this.routerState.loadRouting();
		
		this.filial = this.route.snapshot.paramMap.get('filPro');
		window.sessionStorage.setItem('filial', atob(this.filial));
		
		this.chaveProcesso = this.route.snapshot.paramMap.get('codPro');
		this.cajuri = atob(decodeURIComponent(this.chaveProcesso));		

		//-- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'processo' no arquivo JurTraducao.
		thfI18nService.getLiterals({
			context: 'processo',
			language: thfI18nService.getLanguage()
		}).subscribe((litProc) => {
			this.litProcesso = litProc;

			//-- tradução do label do botão de encerrar ou reabrir processo.
			this.btnSituacaoProcess = this.litProcesso.btnEncerrar;

			//-- seta as constantes para tradução do titulo dos campos de detalhe do processo
			this.camposProcesso[0].label = this.litProcesso.camposDet.escritorio;
			this.camposProcesso[1].label = this.litProcesso.camposDet.envolvidos;
			this.camposProcesso[2].label = this.litProcesso.camposDet.instancia;
			this.camposProcesso[3].label = this.litProcesso.camposDet.localizacao;
			this.camposProcesso[4].label = this.litProcesso.camposDet.numprocesso;
			this.camposProcesso[5].label = this.litProcesso.camposDet.dataEntrada;
			this.camposProcesso[6].label = this.litProcesso.camposDet.responsavel;
			this.camposProcesso[7].label = this.litProcesso.camposDet.assunto;
			this.camposProcesso[8].label = this.litProcesso.camposDet.situacao;

			//-- seta as constantes para tradução do titulo das colunas da tabela de valores
			this.colGridValores[0].label = this.litProcesso.valuesTable.causa;
			this.colGridValores[1].label = this.litProcesso.valuesTable.envolvido;
			this.colGridValores[2].label = this.litProcesso.valuesTable.provisao;
			this.colGridValores[3].label = this.litProcesso.valuesTable.juizo;
			this.colGridValores[4].label = this.litProcesso.valuesTable.deposito;
			this.colGridValores[5].label = this.litProcesso.valuesTable.despesa;

			//-- seta as constantes para tradução do titulo das colunas da tabela de contingencia
			this.colGridContingencia[0].label = this.litProcesso.tableConting.provavel;
			this.colGridContingencia[1].label = this.litProcesso.tableConting.possivel;
			this.colGridContingencia[2].label = this.litProcesso.tableConting.remoto;
			this.colGridContingencia[3].label = this.litProcesso.tableConting.incontroverso;

			//-- seta as constantes para tradução do titulo das colunas da widget de andamentos
			this.colAndamentos[0].label = this.litProcesso.andamentos.data;
			this.colAndamentos[1].label = this.litProcesso.andamentos.ato;
			this.colAndamentos[2].label = this.litProcesso.andamentos.fase;
			this.colAndamentos[3].label = this.litProcesso.andamentos.descricao;

			//-- seta as constantes para tradução do titulo das colunas da widget de garantia
			this.colGarantias[0].label = this.litProcesso.gridGarantia.colunaData;
			this.colGarantias[1].label = this.litProcesso.gridGarantia.colunaTipo;
			this.colGarantias[2].label = this.litProcesso.gridGarantia.colunaValor;
			this.colGarantias[3].label = this.litProcesso.gridGarantia.colunaSaldoAtu;

			//-- seta as constantes para tradução do titulo das colunas da widget de tarefas
			this.colTarefas[0].label = this.litProcesso.tarefas.colunaData;
			this.colTarefas[1].label = this.litProcesso.tarefas.colunaTipo;
			this.colTarefas[2].label = this.litProcesso.tarefas.colunaStatus;
			this.colTarefas[3].label = this.litProcesso.tarefas.colunaResponsavel;
				
			//-- seta as constantes para tradução do titulo das colunas da widget de pedidos
			this.colPedidos[0].label = this.litProcesso.pedidos.pedido;
			this.colPedidos[1].label = this.litProcesso.pedidos.dataValor;
			this.colPedidos[2].label = this.litProcesso.pedidos.provisao;
			this.colPedidos[3].label = this.litProcesso.pedidos.provisaoAtu;
			this.colPedidos[4].label = this.litProcesso.pedidos.prognostico;
			this.colPedidos[5].label = this.litProcesso.pedidos.vlrPedido;
			this.colPedidos[6].icons[1].tooltip = this.litProcesso.pedidos.aguardaAprov;

			//-- Widget despesas
			this.colDespesas[0].label = this.litProcesso.despesas.gridDespesa.tipoDespesa
			this.colDespesas[1].label = this.litProcesso.despesas.gridDespesa.dataDespesa
			this.colDespesas[2].label = this.litProcesso.despesas.gridDespesa.valorDespesa
			this.colDespesas[3].label = this.litProcesso.despesas.gridDespesa.edtDespesa

			//-- seta as constantes para tradução do titulo dos campos de detalhe do processo fixo
			this.CamposProcessoSticky[0].label = this.litProcesso.camposDet.escritorio;
			this.CamposProcessoSticky[1].label = this.litProcesso.camposDet.envolvidos;
			this.CamposProcessoSticky[2].label = this.litProcesso.camposDet.numprocesso;
			this.CamposProcessoSticky[3].label = this.litProcesso.camposDet.localizacao;

			//-- Tradução para os botões posicionados ao lado direito do título "Meu Processo"
			this.pageActions[1].label = this.litProcesso.btnHistorico

			//-- Adiciona o código do cajuri no título Meu processo + área
			this.cajuriTitulo = this.cajuri.replace(new RegExp('^0*', 'g'), '')
			//-- Titulo das opçoes de compartilhamento
			this.listCompResumo[0].label = this.litProcesso.compartilhar.whatsapp;
			this.listCompResumo[1].label = this.litProcesso.compartilhar.pdf;

			});
	}

	ngOnInit() {
		this.getDetailProcess();
		this.coluna = this.colGridValores;
		this.getContingencia();
		this.getAndamentos();
		this.getGarantias();
		this.getTarefas();
		this.getPedidos();
		this.getDespesas();
			
		this.createForm(new Processo);
		this.getInfoEncerrado();
		this.getChave();

		// Responsável por capturar o evento de scroll
		document.getElementsByName("processPage")[0].addEventListener('scroll', this.exibeDetProcFixo, true);

		// Seta as propriedades do breadcrumb 
		this.breadcrumbProperties = this.breadcrumbSetProperties()
	}
	
	/**
	* Responsável por obter os dados inputados no formulário de inclusão
	* de garantias para serem usados após o submit
	*/
	createForm(processo: Processo) {
		if (this.userInfo.getNomeUsuario() == "" || this.codSituac == ""){
			setTimeout(() => {this.createForm(processo)}, 500);
		} else {
			// revisar após o ajuste do uso de formcontrol no thf-datapicker
			this.formEncerramento = this.formBuilder.group({
				dataEncerramento: [null, Validators.compose([Validators.required])],
				motivo: [processo.motivo, Validators.compose([Validators.required])],
				usuario : [this.userInfo.getNomeUsuario(),Validators.compose([Validators.required])],
				detalhe: [processo.detalhe, Validators.compose([Validators.required])]
			});
		}
	}

	/**
	 * método GET (getDetailProcess) de processos
	 * consome o WS REST de integração com LegalProcess
	 */
	getDetailProcess() {
		this.legalprocess.restore();
		this.isHideLoadingProcesso = false;
		this.isHideLoadingValores = false;
		this.legalprocess.setCodProc(this.cajuri); //define o processo para obter detalhes
		this.legalprocess.setSaldoNT2('1')
		this.legalprocess.get('tlprocess/detail/' + this.cajuri, "Detalhe do Processo" ).subscribe((data) => {
			if (data.processes != '') {
				let valores: Valores = new Valores();
				data.processes.forEach(item => {
					
					this.envolvidos = item.matter[0].description
					this.DadosProcesso = {
						escritorio:     item.office,
						envolvidos:     this.JuriGenerics.changeUndefinedToEmpty(item.author) + "/" + this.JuriGenerics.changeUndefinedToEmpty(item.reu),
						instancia:      item.instance[0].displayName,
						localizacao:    item.instance[0].local + ', ' + item.instance[0].districtCourt + ', ' + item.instance[0].branch, //'Com. Penha, Juizado Cível, Vara Unica',
						numeroprocesso: this.JuriGenerics.decodeHtmlEntities(item.instance[0].processNumber),
						dataEntrada:    this.JuriGenerics.makeDate(item.entryDate, "dd/mm/yyyy"),
						responsavel:    item.staff[1].name,
						assunto:        item.subject,
						situacao:       item.status[0].description,
						urlPastaFluig:  item.folderFluig[0].url
					};

					/* Pega os dados para a widget de valores*/
					valores.atualizedCause     = item.atualizedCause;
					valores.atualizedinvolved  = item.atualizedinvolved;
					valores.atualizedprovision = item.atualizedprovision;
					valores.balanceInCourt     = item.balanceInCourt;
					valores.balanceguarantees  = item.balanceguarantees
					valores.balanceExpenses    = item.balanceExpenses

					this.listValores.push(valores)

					this.DadosProcessoSticky = {
						escritorio:     item.office,
						envolvidos:     this.JuriGenerics.changeUndefinedToEmpty(item.author) + "/" + this.JuriGenerics.changeUndefinedToEmpty(item.reu),
						numeroprocesso: item.instance[0].processNumber,
						localizacao:    item.instance[0].local + ', ' + item.instance[0].districtCourt + ', ' + item.instance[0].branch,
					}

					this.codSituac = item.status[0].code;
					this.area      = item.area[0].description;

					if ( this.breadcrumb[1].label.indexOf(this.cajuriTitulo) == -1 ) {
						this.breadcrumb[1].label += " " + this.area + " (" + this.cajuriTitulo + ")" // Adiciona o código do cajuri no BreadCrumb
					}
					
					this.titleService.setTitle(this.envolvidos);

					// Verifica se o processo está em andamento ou encerrado e altera a cor das tags
					if (item.status[0].code == '1') {
						this.isEncerrado             = false;
						this.camposProcesso[8].color = 'color-03';
						this.btnSituacaoProcess      = this.litProcesso.btnEncerrar;
					} else {
						this.camposProcesso[8].color = 'color-07';
						this.isEncerrado             = true;
						this.btnSituacaoProcess      = this.litProcesso.btnReabrir;
					}

					this.tipoAssuntoJuridico = item.assJur;
				})

			} else {
				this.hasData = false
				this.thfNotification.error(this.litProcesso.naoEncontrado);
				this.router.navigate(['/'], { relativeTo: this.route })
			};
			this.isHideLoadingProcesso = true;
			this.isHideLoadingValores  = true;
		});
		return this.hasData;
	}
	
	/**
	 * método GET (ListAndamentos)
	 * consome o WS REST de integração com LegalProcess
	 */
	getAndamentos() {
		this.isHideLoadingAndamentos = false;
		
		//busca as despesas e seta com a variavel dadas mostraWidgetDespesas de acordo com o retorno da função 
		this.andamentos.getAndamentosResumo(this.filial, this.cajuri).then( listaAndamentos =>{
			//tipado com any, pois há retorno diferentes [0] - array<Despesa> [1] - boolean
			let infoAndamentos: Array<any> = listaAndamentos;

			this.listAndamentos          = infoAndamentos[0];
			this.isHideLoadingAndamentos = true
			this.mostraWidgetAndamentos  = infoAndamentos[1]
			
		})
	}


	callAndamento(value?: any) {
		this.router.navigate(["./andamento/" + btoa(value.codAndamento)], { relativeTo: this.route })
	}

	createAndamento() {
		this.router.navigate(["./andamento/cadastro"], { relativeTo: this.route })
	}

	createGarantia() {
		this.router.navigate(["./garantia/cadastro"], { relativeTo: this.route })
	}

	createPedido(){
		this.router.navigate(["./pedido/cadastro"], {relativeTo: this.route})
	}

	/**
	 *  Rota provisória para acessar todos os andamentos do processo.
	 */
	openAndamentos() {
		this.router.navigate(["./andamento"], { relativeTo: this.route })
	}

	/**
	 * Abre a tela de anexos
	 */
	openAttachments() {
		if (this.DadosProcesso.urlPastaFluig != undefined){
			window.open(this.DadosProcesso.urlPastaFluig, '_blank')
		} else {
			this.router.navigate(["./anexos"], { relativeTo: this.route })
		}
	}
	
	/**
	 * Redireciona para a tela de alteração do processo
	 */
	openAltProcesso(){
		this.router.navigate(["./altProcesso"], { relativeTo: this.route })
	}

	/**
	 * Responsável por trazer os processos GARANTIAS
	 */
	getGarantias() {
		this.isHideLoadingGarantias = false;
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA098");
		this.fwmodel.setCampoVazio(true);
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.setPageSize('3');

		this.fwmodel.setFilter("NT2_FILIAL = '" + atob(this.filial) + "'" );
		this.fwmodel.setFilter("NT2_CAJURI = '" + this.cajuri + "'");
		this.fwmodel.setFilter("NT2_MOVFIN = '1'");


		this.fwmodel.get("Lista de garantias").subscribe((data) => {
			if (data != "") {
				data.resources.forEach(itensGarantias => {
					if (data.resources != "") {
						itensGarantias.models[0].fields.forEach(itemFields => {
							switch (itemFields.id) {
								case "NT2_DATA":
									//-- Tratamento para data
									this.dateStr = itemFields.value;
									if (this.dateStr == null) {
										itensGarantias.data = ''
									}
									else {
										itensGarantias.data = this.JuriGenerics.makeDate(this.dateStr, "dd/mm/yyyy")
									}
									itensGarantias.data;
									break;
								case "NT2_DTPGAR": itensGarantias.tipo = itemFields.value;
									break;
								case "NT2_VALOR": itensGarantias.valor = itemFields.value;
									break;
								case "NT2__GASJA": itensGarantias.saldoatu = itemFields.value;
									break;
							}
							itensGarantias.gar = 'garantia'
						})
					}
				});
			}
			this.garantiasList = data.resources;
			this.isHideLoadingGarantias = true;
		}, err => {
			if(err.message == 403) {
				this.mostraWidgetGarantias = false
			}
		})
	}

	/**
	 * Rota provisória para acessar todos os Garantias do processo.
	 */
	openGarantias() {
		this.router.navigate(["./garantia"], { relativeTo: this.route })
	}

	callGarantia(value?: any) {
		this.router.navigate(["./garantia/" + btoa(this.JuriGenerics.findValueByName(value.models[0].fields, "NT2_COD"))], { relativeTo: this.route })
	}

	/**
	 * Faz o get de Follow-ups de resumo
	 */
	async getTarefas() {
		this.isHideLoadingTarefas = false;

		//busca as tarefas e seta com as variaveis dadas na função 
		this.tarefas.getTarefasResumo(this.cajuri, this.filial).then( listatarefas =>{
			
			//tipado com any, pois há retorno diferentes [0] - array<Tarefas> [1] - boolean
			let infoTarefas: Array<any> = listatarefas;

			this.listTarefas          = infoTarefas[0];
			this.isHideLoadingTarefas = true
			this.mostraWidgetTarefas  = infoTarefas[1]
			
		})
	}

	openTarefas() {
		this.router.navigate(["./tarefa"], { relativeTo: this.route })
	}

	/**
	 * Responsável por obter a lista de motivo de encerramento
	 * @param CodMotivo 
	 */
	getMotivos(CodMotivo?: string) {
		
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA013");
		this.fwmodel.get("Motivo de Encerramento").subscribe((data) => {
			data.resources.forEach(item => {
				this.motivoEncerramento = new motivoEncerramento();
				this.motivoEncerramento.label = this.JuriGenerics.findValueByName(item.models[0].fields,"NQI_DESC");
				this.motivoEncerramento.value = this.JuriGenerics.findValueByName(item.models[0].fields,"NQI_COD");

				this.listMotivos.push(this.motivoEncerramento);
			})

			if (CodMotivo != undefined){
				this.formEncerramento.patchValue({
					motivo: CodMotivo
				})
			}
		})
	}
	/**
	 * Responsável por trazer a data de encerramento, caso esteja em andamento é sugerido a data atual 
	 * @param dataEncerramento 
	 */
	getdataEnce(dataEncerramento?: string){
		this.isHideLoadingEncerramento = false;

		if (dataEncerramento != undefined) {
			this.formEncerramento.patchValue({
				dataEncerramento: dataEncerramento
			})
		}

		this.isHideLoadingEncerramento = true;
	}

	/**
	 * Trás no formulário o detalhe em caso de processo já encerrado
	 * @param detalhe 
	 */
	getDetalhe(detalhe?: string){
		if(detalhe != undefined && this.formEncerramento != undefined){
			this.formEncerramento.patchValue({
				detalhe: detalhe
			})
		}

	}
	/**
	 * Trás as informações de encerramento, caso esteja em andamento, sugere a data e informa o usuário logado
	 */
	getInfoEncerrado(){
		// Verifica se o código do usuário foi preenchido; Essa linha só funciona pois o Menu é responsável por buscar os dados do usuário.
		if (this.userInfo.getNomeUsuario() == "" || this.codSituac == ""){
			setTimeout(() => {this.getInfoEncerrado()}, 500);
		} else {
			if( this.codSituac == '2' ||  (this.codSituac == '1' && 
				this.JuriGenerics.changeUndefinedToEmpty(this.formEncerramento.value.motivo) != '') ){
				this.toggleEncerra = true;
				this.fwmodel.restore();
				this.fwmodel.setModelo("JURA095/" + this.filial + btoa(this.cajuri));

				//seta informações de encerramento no formulário
				this.fwmodel.get('Busca informações de encerramento do processo').subscribe((data) => {
					let dataInfo = data.models[0].fields;
					
					this.formEncerramento.patchValue({
						usuario: this.JuriGenerics.findValueByName(dataInfo, "NSZ_USUENC"),
					})

					this.pk = data.pk;
					this.getdataEnce(this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(dataInfo, "NSZ_DTENCE"), "yyyy-mm-dd"));
					this.getMotivos(this.JuriGenerics.findValueByName(dataInfo, "NSZ_CMOENC"));
					this.getDetalhe(this.JuriGenerics.findValueByName(dataInfo, "NSZ_DETENC"));
					this.loadExtraFields(dataInfo);
					this.isHideLoadingEncerramento = true;
				});
			} else if (this.codSituac == '1') {
				this.getMotivos();
				this.isHideLoadingEncerramento = true;
			}
		}
	}

	/**
	 * metodo responsável por trazer a chave (pk) do registro
	 */
	getChave(){
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA095/" + this.filial + btoa(this.cajuri));

		this.fwmodel.get('Busca a pk do registro - Get processo').subscribe((data) => {
			this.pk = data.pk;

			if (this.JuriGenerics.findValueByName(data.models[0].fields, "NSZ_SITUAC") == '1') {
				this.isHideLoadingEncerramento = false;
				this.getDetalhe(this.JuriGenerics.findValueByName(data.models[0].fields, "NSZ_DETENC"))
				this.loadExtraFields(data.models[0].fields);
			}
		});
	}

	/**
	 * Função responsável por validar se o processo está encerrado ou não
	 * para que seja realizar a ação de encerrar ou reabrir um processo.
	 */
	beforeSubmitEnc() {
		let situacProc: string  = '2';
		let isRet:      boolean = this.formEncerramento.valid;

		if (isRet) {
			if (this.isEncerrado) {
				this.modalJustif.openModal(this.cajuri, ()=>{
					if (this.modalJustif.isOk){
						situacProc     = '1';
						
						this.formEncerramento.patchValue({ 
							motivo: '',
							dataEncerramento: ''
						});

						this.onEncChange(situacProc);
					}
				});
			} else {
				this.onEncChange(situacProc);
			}
		} else {
			this.thfNotification.error(this.litProcesso.encerramento.erroVldEnc);
		}
	}
	
	/**
	 * Efetua a alteração do processo, encerrando com as informações passadas no formulário
	 */
	onEncChange(situacProc: string){
		let fwModelBody: FwModelBody;
		let dtEnc:       string        = this.formEncerramento.value.dataEncerramento;
		this.isHideLoadingEncerramento = false;
		this.user                      = this.userInfo.getNomeUsuario();
		
		fwModelBody        = new FwModelBody('JURA095', 4, this.pk,"NSZMASTER");
		fwModelBody.cajuri = this.cajuri;

		fwModelBody.models[0].addField("NSZ_SITUAC", situacProc                          , 0, "C" )
		fwModelBody.models[0].addField("NSZ_DTENCE", dtEnc                               , 0, "D" )
		fwModelBody.models[0].addField("NSZ_CMOENC", this.formEncerramento.value.motivo  , 0, "C" )
		fwModelBody.models[0].addField("NSZ_DETENC", this.formEncerramento.value.detalhe , 0, "C" )
		fwModelBody.models[0].addField("NSZ_USUENC", this.user                           , 0, "C" )

		//Adiciona os campos extras no body
		for (var campos in this.oValores) {
			let nPosCampoExt: number = this.listAuxExtField.findIndex(x => x.property == campos);
			if (nPosCampoExt > -1) {
				let cTypeForm: string = this.listAuxExtField[nPosCampoExt].type;
				let cTypeBody: 'C' | 'N' | 'D' | 'L' = 'C';
				let value: string | number = this.oValores[campos];

				switch (cTypeForm) {
					case 'date':
						cTypeBody = 'D';
						break;
					case 'boolean':
						if (value) {
							value = 'T'
						} else {
							value = 'F'
						}
						break;
				}

				fwModelBody.models[0].addField(campos, value, 0, cTypeBody);
			}
		}

		if (situacProc == '1') {
			this.formEncerramento.reset();
		}

		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA095");
		this.fwmodel
			.put(this.pk,fwModelBody.serialize(),"Encerramento do Processo")
			.subscribe((data)=>{
				if (data != '') {
					let situac: string = this.JuriGenerics.findValueByName(data.models[0].fields, "NSZ_SITUAC");

					if (situac == '2') {
						this.thfNotification.success(this.litProcesso.encerrado);
						this.isEncerrado        = false;
						this.toggleEncerra      = true;
						this.btnSituacaoProcess = this.litProcesso.btnEncerrar;
					} else {
						this.thfNotification.success(this.litProcesso.alterado);
						this.isEncerrado        = true;
						this.toggleEncerra      = false;
						this.btnSituacaoProcess = this.litProcesso.btnReabrir;
					}

					//carrega informações
					this.getDetailProcess();
					this.getInfoEncerrado();
				} else {
					this.isHideLoadingEncerramento = true;
				}
			})
	}

	createTarefa() {
		this.router.navigate(["./tarefa/cadastro"], { relativeTo: this.route })
	}
		
	// Rota provisória para acessar todos os pedidos do processo.
	openPedidos(){
		this.router.navigate(["./pedido"], {relativeTo: this.route})
	}	
	
	callPedido(dadosLinha ?: any) {
		this.router.navigate(["./pedido/" + btoa(dadosLinha.codPedido)], {relativeTo: this.route})
	}
	
	/**
	* Requisição do protheus para obter os pedidos cadastrados
	*/
	getPedidos(){
		this.isHideLoadingPedidos = false;
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA270");
		this.fwmodel.setFilter("NSZ_COD='" + this.cajuri + "' ");
		this.fwmodel.setFilter("NSZ_FILIAL='" + atob(this.filial) + "' ");
		this.fwmodel.setCampoVazio(true);
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.setCampoVirtual(true);

		this.fwmodel.get('Lista de Pedido').subscribe((data) => {
			if (data != '' && data.resources[0].models[0].models[0].items != undefined){
				let aQtdPedidos = data.resources[0].models[0].models[0].items
				var min = 0
				if (aQtdPedidos.length >= 4) {
					var min = aQtdPedidos.length - 3
				}
				aQtdPedidos.slice(min,aQtdPedidos.length).forEach(item => {
					let itemPedido = new Pedido();
					itemPedido.pedido         = this.JuriGenerics.findValueByName(item.fields, "O0W_DTPPED"),
					itemPedido.dtValor        = this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(item.fields, "O0W_DATPED"), "dd/mm/yyyy"),
					itemPedido.vlrProvisao    = this.JuriGenerics.findValueByName(item.fields, "O0W_VPROVA"),
					itemPedido.vlrProvisaoAtu = this.JuriGenerics.findValueByName(item.fields, "O0W_VATPRO"),
					itemPedido.prognostico    = this.JuriGenerics.findValueByName(item.fields, "O0W_PROGNO"),
					itemPedido.vlrPedido      = this.JuriGenerics.findValueByName(item.fields, "O0W_VPEDID"),
					itemPedido.codIndice      = item.id,
					itemPedido.codPedido      = this.JuriGenerics.findValueByName(item.fields, "O0W_COD"),
					itemPedido.codWf          = this.JuriGenerics.findValueByName(item.fields, "O0W_CODWF"),
					itemPedido.urlWf          = this.JuriGenerics.findValueByName(item.fields, "O0W_URLFLG"),
					itemPedido.editarPedido   = ['pedido']
					this.listPedidos.push(itemPedido)
				})
				this.getStatus();
			}	
			this.isHideLoadingPedidos = true;
		}, err => {
			if(err.message == 403) {
				this.mostraWidgetPedidos = false
			}
		})
	}

	// Função responsável por exibir os detalhes do processo quando o usuário scrollar a tela
	exibeDetProcFixo() {
		let poPage = document.getElementsByClassName('po-page-content')
		let element = document.getElementById('StickyWidget');

		if (poPage[0].scrollTop >= 303) {
			element.classList.remove('hide');
		} else {
			element.classList.add('hide');
		}
	}

	breadcrumbSetProperties(){
		this.breadcrumbSetParams();
		/**
		 * estrutura de navegação da URL atual
		 */
		this.breadcrumb = [
			{label: this.litProcesso.breadcrumbProc.brHome, link: '/' },
			{label: this.litProcesso.breadcrumbProc.brProcesso, link:  '/processo/' + atob(this.filial) + '/' + atob(this.chaveProcesso) }
		];

		var properties =  {
			favorite: '/juri/JURLEGALPROCESS/favorite/',
			items: this.breadcrumb,
			params: this.breadcrumbParams
		}
		return properties
	}

	breadcrumbSetParams(){
		if (this.userInfo.getCodUsuario() == ""){
			setTimeout(() => {this.breadcrumbSetParams()}, 1000);
		} else {		
			var params = {
				userId: this.userInfo.getCodUsuario()
			}
			Object.assign(this.breadcrumbParams, params )
		}
	}
	
	callDespesa(dadosLinha ?: any) {
		this.router.navigate(["./despesa/" + btoa(dadosLinha.codDespesa)], {relativeTo: this.route})
	}
	/**
	 * Faz o get de Despesas de resumo
	 */
	async getDespesas(){
		this.isHideLoadingDespesas = false;

		//busca as tarefas e seta com as variaveis dadas na função 
		this.despesas.getDespesasResumo(this.filial, this.cajuri).then( listaDespesas =>{
			//tipado com any, pois há retorno diferentes [0] - array<Tarefas> [1] - boolean
			let infoDespesas: Array<any> = listaDespesas;

			this.listDespesa           = infoDespesas[0];
			this.isHideLoadingDespesas = true
			this.mostraWidgetDespesas  = infoDespesas[1]
			
		})
		
	}

	openDespesas(){
		this.router.navigate(["./despesa"], {relativeTo: this.route})
	}

	createDespesas(){
		this.router.navigate(["./despesa/cadastro"], {relativeTo: this.route})
	}	

	/**
	 * Abre a Det-tarefa em modo de Update
	 */
	updateTarefa(value ?: Tarefa) {
		if ( value == undefined){
			this.router.navigate(["./tarefa/cadastro"], { relativeTo: this.route })
		} else {
			this.router.navigate(["./tarefa/"+ btoa(value.codTarefa)], {relativeTo: this.route})
		}
	}

	openHist(){
		this.router.navigate(["./historico"], {relativeTo: this.route})
	}

	// Função que chama a API do WhatsApp para compartilhar a URL do processo
	shareWhatsApp() {
		let localUrl = encodeURIComponent(window.location.href)
		let textoAcesso = this.litProcesso.share.shareWhatsApp + this.envolvidos + " "
		window.open("https://web.whatsapp.com/send?text=" + textoAcesso + localUrl, '_blank')
	}

	/**
	 * Obtém o status da solicitação no fluig
	 */
	getStatus() {
		this.listPedidos.forEach(element => {
			if(element.codWf != ' '){
				this.legalprocess.restore();
				this.legalprocess.get('solicitation/'+ element.codWf, 'getStatus').subscribe((data) => {
					if(data.status != ' ' && data.status){
						element.editarPedido = ['pedido', 'status'];
					};
				});

			};
		});
	}
		/**
	 * Exporta o resumo do processo em PDF
	 */
	exportPDF(){
		this.legalprocess.restore();
		this.legalprocess.setFilter("codFil", atob(this.filial))
		this.legalprocess.setFilter('tipoAssjur', this.tipoAssuntoJuridico);
		this.legalprocess.get("exportpdf/"+ this.cajuri, "Gera relatório em PDF").subscribe(fileData => {
			if (this.JuriGenerics.changeUndefinedToEmpty(fileData.export) != '' ) {
				fileData.export.forEach(item => {
					var blob = JurBase64.toUTF8(item.filedata, 'application/octet-stream');
					var fileLink = document.createElement('a');

					fileLink.href = window.URL.createObjectURL(blob);;
					fileLink.download = item.namefile; //força o nome do download
					fileLink.target = "_blank"; //força a abertura em uma nova página
					document.body.appendChild(fileLink);
					fileLink.click(); //gatilha o evento do click
					this.thfNotification.success(this.litProcesso.compartilhar.msgSucess)
					this.modalCompartilhar.close();
					this.radio.value = '';
					this.opcaoCompartilhar = '';
					this.modalCompartilhar.primaryAction.disabled = false;
				})
			}
		});

		
	}

	/**
	 * Responsável por setar a opção selecionada para a tela de compartilhar resumo
	 * opc 1 = Enviar link via WhatsApp
	 * opc 2 = Baixar PDF
	 * @param event 
	 */
	setOpcao(event){
		this.opcaoCompartilhar = event
	}

	/**
	 * Abre a modal com as opções para compartilhar resumo
	 */
	openModal(){
		this.radio.value       = '';
		this.opcaoCompartilhar = '';
		this.modalCompartilhar.primaryAction.disabled = false;
		this.modalCompartilhar.open();
	}

	/**
	 * Responsável por verificar a opção selecionada, para fazer sua determinada função
	 */
	compResumoOpc(){
		let opc = this.opcaoCompartilhar;

		if (opc == '1'){
			this.shareWhatsApp();
		}else{
			this.modalCompartilhar.primaryAction.disabled = true;
			this.exportPDF();
		}

	}
	
	/**
	 * Redireciona para o link da solicitação no Fluig
	 * @param urlWf URL do Workflow Fluig
	 */
	fluigSolicLink(urlWf){
		if (urlWf != '') {
			window.open(urlWf)
		} else {
			this.thfNotification.error(this.litProcesso.pedido.alertLinkFluig);
		}
	}

	/**
	 * Função que faz a criação dos campos no dynamic form.
	 * @param oDadosProcess - Objeto com as informações do processo
	 */
	loadExtraFields(oDadosProcess: object) {
		this.listAuxExtField = [];
		this.listExtraFields = [];
		this.jurConsultas.restore();
		this.jurConsultas.get('extraFields','Obtem os campos adicionais').subscribe(data => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.campos) != '') {
				data.campos.forEach(item => {
					if (item.agrupamento == '005') {
						let cType:        string;
						let listOptions:  Array<PoSelectOption>;
						let booleanTrue:  string;
						let booleanFalse: string;

						this.classDetEncerramento = 'po-lg-12 po-mr-4 po-mb-12 form-group';
						this.classBtnSalvarSituac = 'po-pull-right po-mt-1 po-mr-1'

						switch (item.tipo) {
							case 'N':
								cType = 'currency';
								break;
							case 'D':
								cType = 'date';
								break;
							case 'CB':
								listOptions = item.opcoes;
								break;
							case 'L':
								cType        = 'boolean';
								booleanTrue  = this.litProcesso.listSimNao.sim;
								booleanFalse = this.litProcesso.listSimNao.nao;
								break;
						}

						this.listAuxExtField.push({
							property:      item.campo,
							label:         item.titulo,
							type:          cType,
							options:       listOptions,
							gridColumns:   3,
							gridSmColumns: 12,
							booleanTrue,
							booleanFalse
						});
					}
				});

				this.listExtraFields = this.listAuxExtField;
				this.loadValores(this.listExtraFields, oDadosProcess);
			} else {
				this.isHideLoadingEncerramento = true;
			}
		});
	}

	/**
	 * Função responsável por retornar ao dynamic form os dados que estão no cadastro do processo.
	 * @param listExtFields - Lista de Fields Extras
	 * @param oDataProcess  - Objeto com as informações do processo
	 */
	loadValores(listExtFields: Array<PoDynamicFormField> = [], oDataProcess: object){
		let valor:       string | number | boolean = '';
		let oValoresAdd: object                    = [];

		listExtFields.forEach(extField => {
			for (var campos in oDataProcess) {
				if ( extField.property == oDataProcess[campos].id ) {
					if (extField.type == 'date') {
						valor = this.JuriGenerics.makeDate(this.JuriGenerics.changeUndefinedToEmpty(oDataProcess[campos].value), "yyyy-mm-dd");
					} else if (extField.type == 'boolean') {
						if (oDataProcess[campos].value == 'T') {
							valor = true;
						} else {
							valor = false;
						}
					} else {
						if(oDataProcess[campos].value  != undefined) {
							valor = oDataProcess[campos].value;
						} else {
							valor = '';
						};
					}
				
					// Adiciona valor ao campo
					oValoresAdd = { [extField.property]: valor }
					this.oValores = Object.assign(this.oValores, oValoresAdd)
				}
			}
		});

		this.isHideLoadingEncerramento = true;
	}

	
	/**
	 * Busca os valores de contingencia do processo
	 */
	getContingencia(){
		this.isHideLoadingContingencia = false;
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA270");
		this.fwmodel.setFilter("NSZ_COD='" + this.cajuri + "'");
		this.fwmodel.setFilter("NSZ_FILIAL='" + atob(this.filial) + "'");
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.setCampoVirtual(true);
		
		this.fwmodel.get('Busca valor de contingencia').subscribe((data) => {
			
			if(data != '' && data.resources[0].models[0].models[0].items != undefined){
				let hasFields           = data.resources[0].models[0].fields;
				let nProvavelAtualizado = this.JuriGenerics.findValueByName(hasFields, "NSZ_TATPRO");
				let nPossivelAtualizado = this.JuriGenerics.findValueByName(hasFields, "NSZ_TATPOS");
				let nRemotoAtualizado   = this.JuriGenerics.findValueByName(hasFields, "NSZ_TATREM");
				let nIncontAtualizado   = this.JuriGenerics.findValueByName(hasFields, "NSZ_TATINC");
				let nTotalProvavel      = 0;
				let nTotalPossivel      = 0;
				let nTotalRemoto        = 0;
				let nTotalIncontroverso = 0;

				/**
				 * Verifica se os valores atualizaveis estão preenchidos, senão pega o valor original
				 */
				if(nProvavelAtualizado > 0){
					nTotalProvavel = nProvavelAtualizado;
				}else{
					nTotalProvavel = this.JuriGenerics.findValueByName(hasFields, "NSZ_TOTPRO")
				}

				if(nPossivelAtualizado > 0){
					nTotalPossivel = nPossivelAtualizado;
				}else{
					nTotalPossivel = this.JuriGenerics.findValueByName(hasFields, "NSZ_TOTPOS")
				}

				if(nRemotoAtualizado > 0){
					nTotalRemoto = nRemotoAtualizado;
				}else{
					nTotalRemoto = this.JuriGenerics.findValueByName(hasFields, "NSZ_TOTREM")
				}
				if(nIncontAtualizado > 0){
					nTotalIncontroverso = nIncontAtualizado;
				}else{
					nTotalIncontroverso = this.JuriGenerics.findValueByName(hasFields, "NSZ_TOTINC")
				}
				
				this.listContingencia = [{
					totalProvavel:      nTotalProvavel,
					totalPossivel:      nTotalPossivel,
					totalRemoto:        nTotalRemoto,
					totalIncontroverso: nTotalIncontroverso
				}]
				//Retira a coluna de valor de provisão da tabela
				this.gridValores.mainColumns.splice(2,1);
				this.colGridValores = this.gridValores.mainColumns;

			}else{
				this.mostraWidgetContingencia = false;
			}

		this.isHideLoadingContingencia = true;

		}, err => {
			if(err.message == 403) {
				this.mostraWidgetContingencia = false
			}
		})
	}
}