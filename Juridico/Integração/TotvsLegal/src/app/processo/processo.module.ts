import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PoModule } from '@po-ui/ng-components';
import { PoTemplatesModule } from '@po-ui/ng-templates';
import { ProcessoRoutingModule } from './processo-routing.module';
import { ProcessoComponent } from './processo.component';
import { GarantiasComponent } from './garantias/garantias.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DetGarantiaComponent } from './garantias/det-garantia/det-garantia.component';
import { DetPedidoComponent } from './pedidos/det-pedido/det-pedido.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { UploadInterceptor } from '../upload.interceptor';
import { PedidosComponent } from './pedidos/pedidos.component';
import { DetProcessoComponent } from './det-processo/det-processo.component';
import { DetLevantamentoComponent } from './garantias/det-garantia/det-levantamento/det-levantamento.component';
import { SharedModule } from "../shared/shared.module";
import { MenuModule } from '../menu/menu.module'
import { AndamentosService } from '../andamentos/andamentos.service';
import { TarefasService } from '../tarefas/tarefas.service';
import { DespesasService } from '../despesas/despesas.service';
import { AltProcessoComponent } from './alt-processo/alt-processo.component';

// Localização
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
registerLocaleData(localePt, 'pt');

@NgModule({
	declarations: [
		ProcessoComponent,
		GarantiasComponent,
		DetGarantiaComponent,
		DetPedidoComponent,
		PedidosComponent,
		DetProcessoComponent,
		DetLevantamentoComponent,
		AltProcessoComponent
	],
	imports: [
		CommonModule,
		ProcessoRoutingModule,
		PoModule,
		PoTemplatesModule,
		FormsModule,	
		ReactiveFormsModule,
		SharedModule,
		MenuModule
	],
	providers: [{
		provide: HTTP_INTERCEPTORS,
		useClass: UploadInterceptor,
		multi: true
	},
	TarefasService,
	DespesasService,
	AndamentosService,
	{
		provide: LOCALE_ID,
		useValue: 'pt-BR'
	}]
})
export class ProcessoModule { }
