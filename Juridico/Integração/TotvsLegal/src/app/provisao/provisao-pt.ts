/**
* Arquivo de constantes para tradução de Provisões - Português
*/


//-- Constantes para o breadcrumb de Favoritos
export const breadcrumbProvisao = {

	brHome: "Meus processos",
	brProv: "Provisões de Processos"
}


//-- Constantes para títulos das colunas do Grid de Provisões
export const provisaoPt = {
	
    loadProv:"Carregando Provisões",
    provisoes: "Provisões",
    colunaProcesso: "Processos",
    colValorProv: "Valor da provisão ($)",
    pesqProcesso: "Pesquisar Processo",
    pesq: "Pesquisar",
    anterior: "<<  Anterior",
	proximo: "Próximo  >>",
    brProvisao: breadcrumbProvisao
}
