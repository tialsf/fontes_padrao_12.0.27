import { 
	Component, 
	OnInit 
} from '@angular/core';
import { 
	PoBreadcrumb,
	PoTableColumn 
} from '@po-ui/ng-components'
import { LegalprocessService } from '../services/legalprocess.service';
import { PoI18nService } from '@po-ui/ng-components';
import { TLUserInfo } from '../services/authlogin.service';
import { UserConfig } from 'src/app/usuarios/user-config/user-config.component'
import { JurigenericsService } from '../services/jurigenerics.service';

@Component({
	selector: 'app-provisao',
	templateUrl: './provisao.component.html',
	styleUrls: ['./provisao.component.css']
})

export class ProvisaoComponent implements OnInit {
	/**
	 * Variaveis para a classe
	 */
	litProvisao: any;
	columns:                   Array<PoTableColumn>;
	provisionList:             Array<any> = [];
	searchKey:                 string     = '';
	filial:                    string     = '';
	codPro:                    string     = '';
	codUsuario:                string     = '';
	length:                    number     = 0;
	page:                      number     = 0;
	pageSize:                  number     = 20;
	maxPage :                  number     = 1;
	hasData:                   boolean    = true;
	btnPreDisable :            boolean    = false;
	btnNxtDisable :            boolean    = false;
	isHideLoadingTabProvisoes: boolean    = false;

	/**
	 * estrutura de navegação da URL atual
	 */
	public breadcrumb: PoBreadcrumb = {
		items: [
			{ label: ' ', link: '/' },
			{ label: ' ' }
		]
	};

	/**
	 * construtor da classe 
	 */
	constructor(
		private legalprocess: LegalprocessService,
		private thfI18nService: PoI18nService,
		private juriGenerics: JurigenericsService,
		private userInfo: TLUserInfo
		) {
			//-- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'prov' no arquivo JurTraducao.			
			thfI18nService.getLiterals( { context: 'prov', language: thfI18nService.getLanguage() } )
				.subscribe((litProvisao) => {
				this.litProvisao = litProvisao;
	
				//-- seta as constantes para tradução do titulo das colunas do Grid de Provisões
				this.ProvisonColumns[0].label = this.litProvisao.colunaProcesso;
				this.ProvisonColumns[1].label = this.litProvisao.colValorProv;

				//-- seta as constantes para tradução dos títulos do Breadcrumb de Provisões
				this.breadcrumb.items[0].label = this.litProvisao.brProvisao.brHome;
				this.breadcrumb.items[1].label = this.litProvisao.brProvisao.brProv;
			});
		}

	ngOnInit() {
		this.page = 1;
		this.searchKey = "";
		this.columns = this.ProvisonColumns;
		this.getInfoUser();
	}
	
	/**
	 * Função responsável por obter dados do usuário logado.
	 */
	getInfoUser() {
		this.codUsuario = this.userInfo.getCodUsuario();

		if (this.juriGenerics.changeUndefinedToEmpty(this.codUsuario) == ""){
			setTimeout(() => {
				this.getInfoUser();
			}, 500);
		} else {
			this.getProvision();
		}
	}

	/**
	 * método GET (ListProcess) de processos
	 * consome o WS REST de integração com LegalProcess 
	 */
	getProvision () {
		let cfgUsuario: Array<UserConfig> = [];
		let nIndex:     number            = -1;
		let tpAssJur:   Array<string>     = [];

		if ( this.juriGenerics.changeUndefinedToEmpty(window.localStorage.getItem('userConfig')) != '') {
			cfgUsuario = JSON.parse(window.localStorage.getItem('userConfig'));
			nIndex     = cfgUsuario.findIndex(x => x.userId == this.codUsuario);
			tpAssJur   = nIndex >= 0 ? cfgUsuario[nIndex].filtAssJur : [];
		}
		
		this.legalprocess.restore();
		this.legalprocess.setOrderProvision("1");                       // ordenação da query (1 = "ORDER BY NSZ.NSZ_VAPROV DESC")
		this.legalprocess.setPageSize(this.pageSize.toString());        // quantidade de itens por página
		this.legalprocess.setTypeFilter('1');                           // processo em andamento
		this.legalprocess.setPage(this.page.toString()) ;               // número da página
		this.legalprocess.setFilter('tipoAssJur', tpAssJur.toString()); // Informar o assunto jurídico que deseja filtrar
		this.isHideLoadingTabProvisoes = false;

		if(this.searchKey != ''){
			this.legalprocess.setSearchKey(this.searchKey.toString()) //conteúdo pesquisado nos campos NT9_NOME/ NUQ_NUMPRO/ NSZ_DETALH
		}
		
		this.legalprocess.get('process', 'Lista de Provisão').subscribe((data) => {
			this.length = data.length; //total de processos retornados pelo WS

			if (data.processes != '') {
				data.processes.forEach(item => {
					item.caso = item.matter[0].description;
				});

				this.isHideLoadingTabProvisoes = true;
				this.provisionList = data.processes;
			} else {
				this.isHideLoadingTabProvisoes = true;
				this.hasData = false;
			}

			this.setMaxPage();
		})

		return this.hasData;
	}

	/**
	 * colunas da tabela 
	 */
	ProvisonColumns: Array<PoTableColumn> = [
		{	property: 'caso',
			color: 'color-01',
			type: 'link',
			action: (value, row) => {
				this.openProcesso(row.processBranch, row.processId);
			}
	 	},
		{ property: 'atualizedprovision',label: ' ', type: 'number', format: '1.2-5'}
	];

	openProcesso(filPro: string, codPro: string){
		window.open('/processo/' +  encodeURIComponent(btoa(filPro)) + "/" +  encodeURIComponent(btoa(codPro)));
	}

	/**
	 * ação dos botões de paginação Anterior/ Próximo
	 */
	showMore(buttonId: string){
		this.movePage(buttonId);
	}

	/**
	 * ação dos botões Anterior/ Próximo
	 */
	movePage(buttonId: string) {
		this.setMaxPage();
		if(buttonId == "next" && this.page < this.maxPage){
			this.page ++;
			this.pagination();
		}else if(buttonId == "previous" && this.page >= 2){
			this.page --;
			this.pagination();
		}

		this.pagination();

		if (this.getProvision()){
			this.columns = this.ProvisonColumns;
		}
	}

	/**
	 * ação do botão Pesquisa
	 */
	search() {
		this.page = 1;
		this.getProvision();

	}
	
	/**
	 * habilita/ desabilita os botões de paginação
	 */
	pagination() {
		this.btnPreDisable = false;
		this.btnNxtDisable = false;

		if (this.length > 0) {
			if(this.page == this.maxPage){
				this.btnNxtDisable = true;
			}
			if(this.page == 1){
				this.btnPreDisable = true;
			}
		} else {
			this.btnPreDisable = true;
			this.btnNxtDisable = true;
		}
	}

	/**
	 * calcula a quantidade máxima de páginas 
	 */
	setMaxPage(){

		this.maxPage = this.length/this.pageSize
		if ((this.maxPage - Math.floor(this.maxPage)) > 0){
			this.maxPage = Math.floor(this.maxPage) + 1;
		}

		this.pagination();
	}


}
