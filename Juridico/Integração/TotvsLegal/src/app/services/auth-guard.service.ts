import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

/**
 * Classe para validar se o usuário foi autenticado ou não.
 * Caso não tenha sido, irá redirecionar o usuário para a tela de Login
 */
@Injectable({
	providedIn: 'root'
})
export class AuthGuardService implements CanActivate{
	
	constructor() {}
	
	/**
	 * Função utilizada para verificar se o Login foi realizado
	 * @param next   - Snapshot da Rota ativa
	 * @param state  - Estado do Snapshot
	 */
	canActivate(next: ActivatedRouteSnapshot,
	state: RouterStateSnapshot): boolean {
		
		return true
	}
	/**
	 * Função utilizada para verificar se tem acesso aos filhos
	 * @param next   - Snapshot da Rota ativa
	 * @param state  - Estado do Snapshot
	 */
	canActivateChild(state: RouterStateSnapshot): boolean {
		
		return true
	}
}
