import { Injectable } from '@angular/core';
import { protheusService } from './protheus.service';
import { JurigenericsService } from '../services/jurigenerics.service';
import { PoNotificationService } from '@po-ui/ng-components';

const authEndPoint = "api/oauth2/v1/token";
const userIdEndPoint = "Users/GetUserId";
const userInfoEndPoint = "Users/";
const sysParamEndPoint = "api/framework/v1/systemParameters/"
const frameEndPoint = "api/framework/"

@Injectable({
	providedIn:'root'
})
export class TLUserInfo{
	private nomeUsuario = "";
	private emailUsuario = "";
	private codUsuario = "";
	
	constructor(
		private protheusService: protheusService,
		private JuriGenerics: JurigenericsService) { }
	
	/*******************/
	// Setters/Getters //
	/*******************/
	setCodUsuario(codUsuario: string){
		this.codUsuario = codUsuario;
	}
	
	setNomeUsuario(nomeUsuario: string){
		this.nomeUsuario = nomeUsuario;
	}

	setEmailUsuario(emailUsuario: string){
		this.emailUsuario = emailUsuario;
	}	

	getCodUsuario(){
		return this.codUsuario;
	}

	getNomeUsuario(){
		return this.nomeUsuario;
	}

	getEmailUsuario(){
		return this.emailUsuario;
	}

	/** Restaura os valores iniciais */
	restore(){
		this.protheusService.useAuthorization(true);
		this.protheusService.restore();
		this.protheusService.setOptionsHeaders('X-Totvs-No-Error', 'true') // Usado para remover o retorno padrão
	}
	
	/**
	 * Busca o ID do usuário
	 * Endpoint: "Users/GetUserId"
	 */
	getUserId(){
		return this.protheusService.get(userIdEndPoint, 'getUserId');
	}

	/**
	 * Busca os dados do usuário
	 * Endpoint: "Users/"
	 */
	getUserInfo(){
		return this.protheusService.get(userInfoEndPoint + this.getCodUsuario(), 'getUserInfo');
	}

	/**
	 * Limpa o usuário logado
	 */
	clearUser(){
		this.JuriGenerics.clearStorage();
		this.codUsuario = "";
		this.emailUsuario = "";
		this.nomeUsuario = "";
	}

}

/**
 * Classe para realizar a autenticação do Login
 * Endpoint: "api/oauth2/v1/token"
 */
@Injectable({
	providedIn:'root'
})
export class AuthloginService {
	redirectUrl: string = "/";
	isLoggedIn = false;
	
	constructor(private protheusService: protheusService, private userInfo: TLUserInfo, private thfNotification : PoNotificationService,
		 private juriGenerics: JurigenericsService) { 
	}
	
	/** Restaura os valores iniciais */
	restore(){
		this.protheusService.useAuthorization(false);
		this.protheusService.restore();
		this.protheusService.setOptionsHeaders('X-Totvs-No-Error', 'true') // Usado para remover o retorno padrão
	}
	
	/** Setters */
	setUserLogin(strLogin: string){
		this.protheusService.setOptionsParams("username", strLogin);
	}
	
	setPasswordLogin(strPass: string){
		this.protheusService.setOptionsParams("password", strPass);
	}
	
	setGrantType(type: string){
		this.protheusService.setOptionsParams("grant_type",type);
	}

	setRefreshToken(token: string){
		this.protheusService.setOptionsParams("refresh_token",token);
	}
	
	/**
	 * Realiza a requisição do Post
	 */
	getToken(){
		return this.protheusService.post(authEndPoint, 'getToken');
	}

	/**
	 * Realiza a requisição de refresh do token
	 */
	getTokenRefresh(){
		return this.protheusService.post(authEndPoint, 'getToken');
	}

	/**
	 * Verifica se o usuário está autenticado. Senão redireciona para a tela de login
	 */
	isLogged(){
		var isLogged:boolean = true;
		let diaLogin = new Date(parseInt(window.localStorage.getItem('lastUse')));
		let hoje     = new Date()

		if ("TLToken" in window.localStorage) {
			if (window.localStorage.getItem("TLToken") == undefined){
				isLogged = false;
			}

			if(isLogged && 
				( ( Date.now() - parseInt(window.localStorage.getItem('lastUse') )) > 300000 ) && // se tiver mais de 50 minutos
				(hoje.getDate() == diaLogin.getDate() ) ) { // Se for o mesmo dia renova o token
				 
				this.setRefreshToken(window.localStorage.getItem('TLTokenRefresh'));
				this.setGrantType('refresh_token');
				this.getToken().subscribe(data=> {
					if ('access_token' in data){
						this.updateSessionStorage(data);
						this.updateInfoPerfil();
					}
				});
			}

			if (isLogged && this.juriGenerics.changeUndefinedToEmpty(this.userInfo.getCodUsuario()) == "") {
				isLogged = false;
			}
		} else {
			isLogged = false;
		}
		return isLogged;
	}
	
	/** Atualiza informações do usuário*/
	updateInfoPerfil(){
		this.userInfo.restore();
		this.userInfo.getUserId().subscribe(data=>{
			this.userInfo.setCodUsuario(data.userID);
			
			this.userInfo.restore();
			if (this.userInfo.getCodUsuario() != undefined && this.userInfo.getCodUsuario() != '') { 
				this.userInfo.getUserInfo().subscribe(data=>{
					this.userInfo.setEmailUsuario(data.emails[0].value);
					this.userInfo.setNomeUsuario(data.displayName);
				}, errorUserInfo=>{
				});
			}
		},errorGetId=>{
			
		});
	}

	/**
	 * Atualiza o Session Storage
	 * @param token 
	 */
	updateSessionStorage(token: any){
		window.localStorage.setItem('TLToken',token.access_token);
		window.localStorage.setItem('TLTokenRefresh',token.refresh_token);
		window.localStorage.setItem('TLAuthType',token.token_type);
		window.localStorage.setItem('lastUse', Date.now().toString() );
		
	};
}


/**
 * Classe para as API's do Protheus fora das API's do Juridico
 * Endpoint: "api/framework/v1/systemParameters/"
 * @listens systemparameters
 */
@Injectable({
	providedIn:'root'
})
export class protheusAPI{
	private codParam: string;

	constructor(
		private protheusService: protheusService) { }
		

	setMv(codParam: string){
		this.codParam = codParam;
	}

	restore(){
		this.protheusService.restore();
	}

	getParam(){
		return this.protheusService.get(sysParamEndPoint + this.codParam, 'getParam');
	}
}

export class protheusParam{
	id: string;
	value: any;
}

/**
 * Classe para consulta à API do Frame
 * Endpoint: "api/framework/"
 */
@Injectable({
	providedIn:'root'
})
export class frameApi{
	private method: string = "";

	constructor(
		private protheusService: protheusService,
	){ }

	/**
	 * Restaura os Headers e os Params
	 */
	restore(){
		this.protheusService.restore();
	}

	// seta a quantidade de itens por página
	setPageSize(pagesize: string){
		this.protheusService.setOptionsParams("pagesize", pagesize);
	}

	/**
	 * Define o valor do QueryParam passado
	 * @param cNameParam - QueryParam a ser definido
	 * @param value - Valor a ser inserido no QueryParam
	 */
	setFilter(cNameParam: string, value: string){
		this.protheusService.setOptionsParams(cNameParam, value);
	}

	/**
	 * Define o método a ser executado
	 * @param metodo 
	 */
	setMethod(metodo: string){
		this.method = metodo
	}

	executeGet(){
		return this.protheusService.get(frameEndPoint + this.method, 'get' + this.method)
	}
}

/**
 * Classe na estrutura da Empresa/Filial. Contem campos de Label e Value por conta da necessidade de incluir em Combos
 */
export class EmpresaFilial{
	filial : string;
	empresa: string;
	label  : string = "";
	value  : string = "";
}
