import { Injectable } from '@angular/core';
import { FwmodelAdaptorService, adaptorFilterParam } from './fwmodel-adaptor.service';
import { FwmodelService } from './fwmodel.service';
import { JurigenericsService } from './jurigenerics.service';

/**
 * Área Jurídica
 */
@Injectable({
	providedIn: 'root',
})
export class AreaJuridicaAdapterService extends FwmodelAdaptorService {
	adapter: adaptorFilterParam = new adaptorFilterParam(
		'JURA038',
		"NRB_ATIVO='1'",
		'NRB_DESC',
		'NRB_DESC',
		'',
		'NRB_COD',
		'',
		'GetArea',
		undefined,
		undefined,
		undefined,
		{
			bGetAllLevels: true,
			bUseEmptyField: false,
			bUseVirtualField: false,
			arrSetFields: ['NRB_COD', 'NRB_DESC'],
		}
	);

	constructor(fwModelService: FwmodelService, juriGenerics: JurigenericsService) {
		super(fwModelService, juriGenerics);
		this.setup(this.adapter);
	}
}

/**
 * Assunto Jurídico
 */
@Injectable({
	providedIn: 'root',
})
export class AssuntoAdapterService extends FwmodelAdaptorService {
	adapter: adaptorFilterParam = new adaptorFilterParam(
		'JURA004',
		'',
		'NQ4_DESC',
		'NQ4_DESC',
		'',
		'NQ4_COD',
		'',
		'GetAssunto',
		undefined,
		undefined,
		undefined,
		{
			bGetAllLevels: true,
			bUseEmptyField: false,
			bUseVirtualField: false,
			arrSetFields: ['NQ4_COD', 'NQ4_DESC'],
		}
	);

	constructor(fwModelService: FwmodelService, juriGenerics: JurigenericsService) {
		super(fwModelService, juriGenerics);
		this.setup(this.adapter);
	}
}

/**
 * Ato Processual
 */
@Injectable({
	providedIn: 'root',
})
export class AtoAdapterService extends FwmodelAdaptorService {}

/**
 * Banco
 */
@Injectable({
	providedIn: 'root',
})
export class BancoAdapterService extends FwmodelAdaptorService {}

/**
 * Cargo
 */
@Injectable({
	providedIn: 'root',
})
export class CargoAdapterService extends FwmodelAdaptorService {
	adapter: adaptorFilterParam = new adaptorFilterParam(
		'JCRGS',
		'',
		'Q3_DESCSUM',
		'Q3_DESCSUM',
		'',
		'Q3_CARGO',
		'',
		'Get Cargos',
		undefined,
		undefined,
		undefined,
		{
			bGetAllLevels: false,
			bUseEmptyField: true,
			bUseVirtualField: true,
			arrSetFields: [],
		}
	);
	constructor(fwModelService: FwmodelService, juriGenerics: JurigenericsService) {
		super(fwModelService, juriGenerics);
		this.setup(this.adapter);
	}
}

/**
 * Centro de Custo
 */
@Injectable({
	providedIn: 'root',
})
export class CustoAdapterService extends FwmodelAdaptorService {
	adapter: adaptorFilterParam = new adaptorFilterParam(
		'JCCUSTO',
		'',
		'CTT_DESC01',
		'CTT_DESC01',
		'',
		'CTT_CUSTO',
		'',
		'Centro de Custo',
		undefined,
		undefined,
		undefined,
		{
			bGetAllLevels: true,
			bUseEmptyField: false,
			bUseVirtualField: false,
			arrSetFields: ['CTT_CUSTO', 'CTT_DESC01'],
		}
	);
	constructor(fwModelService: FwmodelService, juriGenerics: JurigenericsService) {
		super(fwModelService, juriGenerics);
		this.setup(this.adapter);
	}
}

/**
 * Cidade / Município
 */
@Injectable({
	providedIn: 'root',
})
export class CidadeAdapterService extends FwmodelAdaptorService {}

/**
 * Comarcas
 */
@Injectable({
	providedIn: 'root',
})
export class ComarcasAdapterService extends FwmodelAdaptorService {
	adapter: adaptorFilterParam = new adaptorFilterParam(
		'JURA005',
		'',
		'NQ6_DESC',
		'NQ6_DESC',
		'',
		'NQ6_COD',
		'',
		'GetComarca',
		undefined,
		undefined,
		undefined,
		{
			bGetAllLevels: true,
			bUseEmptyField: false,
			bUseVirtualField: false,
			arrSetFields: [],
		}
	);

	constructor(fwModelService: FwmodelService, juriGenerics: JurigenericsService) {
		super(fwModelService, juriGenerics);
		this.setup(this.adapter);
	}
}

/**
 * Cond Pagamento
 */
@Injectable({
	providedIn: 'root',
})
export class CondPagAdapterService extends FwmodelAdaptorService {}

/**
 * Escritorio Correspondente
 */
@Injectable({
	providedIn: 'root',
})
export class EscritorioAdapterService extends FwmodelAdaptorService {
	adapter: adaptorFilterParam = new adaptorFilterParam(
		'JURA132',
		"A2_MJURIDI='1'",
		'A2_NOME,A2_CGC',
		'A2_NOME',
		'',
		'A2_COD,A2_LOJA',
		'',
		'getEscritorio',
		undefined,
		undefined,
		undefined,
		{
			bGetAllLevels: true,
			bUseEmptyField: false,
			bUseVirtualField: false,
			arrSetFields: ['A2_COD', 'A2_LOJA', 'A2_NOME', 'A2_CGC'],
		}
	);
	constructor(fwModelService: FwmodelService, juriGenerics: JurigenericsService) {
		super(fwModelService, juriGenerics);
		this.setup(this.adapter);
	}
}

/**
 * Fase Processual
 */
@Injectable({
	providedIn: 'root',
})
export class FaseAdapterService extends FwmodelAdaptorService {}

/**
 * Formas de Correção
 */
@Injectable({
	providedIn: 'root',
})
export class FCorrecaoAdapterService extends FwmodelAdaptorService {
	adapter: adaptorFilterParam = new adaptorFilterParam(
		'JURA061',
		"NW7_VISIV='1'",
		'NW7_DESC',
		'NW7_DESC',
		'',
		'NW7_COD',
		'',
		'getFormaCorrecao',
		undefined,
		undefined,
		undefined,
		{
			bGetAllLevels: true,
			bUseEmptyField: false,
			bUseVirtualField: false,
			arrSetFields: ['NW7_COD', 'NW7_DESC'],
		}
	);
	constructor(fwModelService: FwmodelService, juriGenerics: JurigenericsService) {
		super(fwModelService, juriGenerics);
		this.setup(this.adapter);
	}
}

/**
 * Fornecedor
 */
@Injectable({
	providedIn: 'root',
})
export class FornecedorAdapterService extends FwmodelAdaptorService {}

/**
 * Locais de Trabalho
 */
@Injectable({
	providedIn: 'root',
})
export class LocalTrabAdapterService extends FwmodelAdaptorService {
	adapter: adaptorFilterParam = new adaptorFilterParam(
		'JURA107',
		'',
		'NTB_DESC',
		'NTB_DESC',
		'',
		'NTB_COD',
		'',
		'GetLocalTrabalho'
	);
	constructor(fwModelService: FwmodelService, juriGenerics: JurigenericsService) {
		super(fwModelService, juriGenerics);
		this.setup(this.adapter);
	}
}

/**
 * Moeda
 */
@Injectable({
	providedIn: 'root',
})
export class MoedaAdapterService extends FwmodelAdaptorService {}

/**
 * Natureza Financeira
 */
@Injectable({
	providedIn: 'root',
})
export class NaturezaAdapterService extends FwmodelAdaptorService {}

/**
 * Nome Envolvido
 */
@Injectable({
	providedIn: 'root',
})
export class NomeEnvolvidoAdapterService extends FwmodelAdaptorService {}

/**
 * Participantes / Responsável
 */
@Injectable({
	providedIn: 'root',
})
export class ParticipanteAdapterService extends FwmodelAdaptorService {
	adapter: adaptorFilterParam = new adaptorFilterParam(
		'JURA159',
		"RD0_TPJUR = '1' AND RD0_SIGLA <> '     '",
		'RD0_NOME',
		'RD0_NOME',
		'RD0_NOME',
		'RD0_SIGLA',
		'',
		'Busca responsavel',
		undefined,
		undefined,
		undefined,
		{
			bGetAllLevels: true,
			bUseEmptyField: true,
			bUseVirtualField: true,
			arrSetFields: ['RD0_SIGLA', 'RD0_NOME'],
		}
	);
	constructor(fwModelService: FwmodelService, juriGenerics: JurigenericsService) {
		super(fwModelService, juriGenerics);
		this.setup(this.adapter);
	}
}

/**
 * Preposto FUPs
 */
@Injectable({
	providedIn: 'root',
})
export class PrepostoAdapterService extends FwmodelAdaptorService {}

/**
 * Produto
 */
@Injectable({
	providedIn: 'root',
})
export class ProdutoAdapterService extends FwmodelAdaptorService {}

/**
 * Rateio
 */
@Injectable({
	providedIn: 'root',
})
export class RateioAdapterService extends FwmodelAdaptorService {}

/**
 * Ritos
 */
@Injectable({
	providedIn: 'root',
})
export class RitoAdapterService extends FwmodelAdaptorService {
	adapter: adaptorFilterParam = new adaptorFilterParam(
		'JURA018',
		'',
		'NQO_DESC',
		'NQO_DESC',
		'',
		'NQO_COD',
		'',
		'Busca Rito',
		undefined,
		undefined,
		undefined,
		{
			bGetAllLevels: true,
			bUseEmptyField: true,
			bUseVirtualField: true,
			arrSetFields: ['NQO_COD', 'NQO_DESC'],
		}
	);
	constructor(fwModelService: FwmodelService, juriGenerics: JurigenericsService) {
		super(fwModelService, juriGenerics);
		this.setup(this.adapter);
	}
}

/**
 * Status Tarefa
 */
@Injectable({
	providedIn: 'root',
})
export class StatusTarefaService extends FwmodelAdaptorService {}

/**
 * Tipo de Ação
 */
@Injectable({
	providedIn: 'root',
})
export class TpAcaoAdapterService extends FwmodelAdaptorService {
	adapter: adaptorFilterParam = new adaptorFilterParam(
		'JURA022',
		'',
		'NQU_DESC',
		'NQU_DESC',
		'',
		'NQU_COD',
		'',
		'Tipo de Ação',
		undefined,
		undefined,
		undefined,
		{
			bGetAllLevels: false,
			bUseVirtualField: true,
			bUseEmptyField: false,
			arrSetFields: ['NQU_COD', 'NQU_DESC'],
		}
	);
	constructor(fwModelService: FwmodelService, juriGenerics: JurigenericsService) {
		super(fwModelService, juriGenerics);
		this.setup(this.adapter);
	}
}

/**
 * Tipo de Despesa
 */
@Injectable({
	providedIn: 'root',
})
export class TpDespesaAdapterService extends FwmodelAdaptorService {}

/**
 * Tipo de Envolvimento
 */
@Injectable({
	providedIn: 'root',
})
export class TpEnvolvAdapterService extends FwmodelAdaptorService {
	adapter: adaptorFilterParam = new adaptorFilterParam(
		'JURA009',
		'',
		'NQA_DESC',
		'NQA_DESC',
		'',
		'NQA_COD',
		'',
		'GetTipoEnvolvimento',
		undefined,
		undefined,
		undefined,
		{
			bGetAllLevels: false,
			bUseVirtualField: true,
			bUseEmptyField: false,
			arrSetFields: [],
		}
	);
	constructor(fwModelService: FwmodelService, juriGenerics: JurigenericsService) {
		super(fwModelService, juriGenerics);
		this.setup(this.adapter);
	}
}

/**
 * Tipo de Garantia
 */
@Injectable({
	providedIn: 'root',
})
export class TpGarAdapterService extends FwmodelAdaptorService {}

/**
 * Tipo de Pedido
 */
@Injectable({
	providedIn: 'root',
})
export class TpPedidoAdapterService extends FwmodelAdaptorService {}

/**
 * Tipo de Tarefa / Prazo
 */
@Injectable({ providedIn: 'root' })
export class TpTarefaService extends FwmodelAdaptorService {
	adapter: adaptorFilterParam = new adaptorFilterParam(
		'JURA021',
		"NQS_TIPO in (' ','2')",
		'NQS_DESC',
		'NQS_DESC',
		'NQS_DESC',
		'NQS_COD',
		'',
		'Busca tipo',
		undefined,
		undefined,
		undefined,
		{
			bGetAllLevels: false,
			bUseEmptyField: false,
			bUseVirtualField: false,
			arrSetFields: ['NQS_COD', 'NQS_DESC'],
		}
	);
	constructor(fwModelService: FwmodelService, juriGenerics: JurigenericsService) {
		super(fwModelService, juriGenerics);
		this.setup(this.adapter);
	}
}

/**
 * Unidades
 */
@Injectable({
	providedIn: 'root',
})
export class UnidadeAdapterService extends FwmodelAdaptorService {
	adapter: adaptorFilterParam = new adaptorFilterParam(
		'JURA148',
		'',
		'A1_NOME,A1_CGC',
		'A1_NOME,A1_COD,A1_LOJA,A1_CGC',
		'A1_NOME [ A1_CGC ]',
		'A1_COD,A1_LOJA',
		'',
		'Unidades',
		undefined,
		undefined,
		undefined,
		{
			bGetAllLevels: false,
			bUseEmptyField: false,
			bUseVirtualField: false,
			arrSetFields: ['A1_COD', 'A1_LOJA', 'A1_CGC', 'A1_NOME'],
		}
	);
	constructor(fwModelService: FwmodelService, juriGenerics: JurigenericsService) {
		super(fwModelService, juriGenerics);
		this.setup(this.adapter);
	}
}

/**
 * Natureza Juridica
 */
@Injectable({ providedIn: 'root' })
export class NaturezaJuridicaAdapterService extends FwmodelAdaptorService {
	adapter: adaptorFilterParam = new adaptorFilterParam(
		'JURA001',
		'',
		'NQ1_DESC',
		'NQ1_DESC',
		'',
		'NQ1_COD',
		'',
		'Busca naturezas juridicas',
		undefined,
		undefined,
		undefined,
		{
			bGetAllLevels: false,
			bUseVirtualField: true,
			bUseEmptyField: false,
			arrSetFields: ['NQ1_COD', 'NQ1_DESC', 'NQ1_TIPO'],
		},
		'NQ1_TIPO'
	);
	constructor(fwModelService: FwmodelService, juriGenerics: JurigenericsService) {
		super(fwModelService, juriGenerics);
		this.setup(this.adapter);
	}
}

/**
 * Natureza Juridica
 */
@Injectable({ providedIn: 'root' })
export class TipoAcaoAdapterService extends FwmodelAdaptorService {}
