import { TestBed } from '@angular/core/testing';

import { FwmodelAdaptorService } from './fwmodel-adaptor.service';

describe('FwmodelAdaptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FwmodelAdaptorService = TestBed.get(FwmodelAdaptorService);
    expect(service).toBeTruthy();
  });
});
