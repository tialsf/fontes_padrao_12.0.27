import { Injectable } from '@angular/core';
import { FwmodelService } from './fwmodel.service';
import { JurigenericsService } from './jurigenerics.service';
import { Observable } from 'rxjs';
import { PoComboOption, PoComboFilter } from '@po-ui/ng-components';
import { map } from 'rxjs/operators';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { frameApi } from 'src/app/services/authlogin.service';
import { defaultReturnStruct } from './legalprocess-adapter.service';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root',
})
export class FwmodelAdaptorService implements PoComboFilter {
	public filterParam: adaptorFilterParam = null;
	public filtroP: string = '';
	public funcionario: string = '';

	/**
	 * Define o filtro que será utilizado
	 * quando o filterParam for string
	 */
	setFiltroP(filtroP) {
		this.filtroP = filtroP;
	}

	/**
	 * Adapter do FWModel Service
	 * @param fwmodelService
	 * @param juriGenerics
	 */

	constructor(public fwmodelService: FwmodelService, public juriGenerics: JurigenericsService) {}

	setup(adapterFilterParam) {
		this.filterParam = adapterFilterParam;
	}

	/**
	 * Função executada no momento em que for digitado ou selecionado o combo
	 * @param valorAtu - Valor digitado atual
	 * @param filterParam - Parâmetros fixos da requisição
	 */
	getFilteredData(
		valorAtu: any,
		filtroData?: adaptorFilterParam | string
	): Observable<Array<adaptorReturnStruct>> {
		this.fwmodelService.restore();
		let filtro: string = '';
		let filtroLike: string[] = [];

		if (this.filtroP !== '') {
			filtro += this.filtroP;
		} else if (filtroData !== undefined) {
			if (typeof filtroData !== 'string') {
				this.filterParam = filtroData;
				// Inclui o filtro padrão, se o mesmo foi definido
				if (this.filterParam.filtroPadrao() !== '') {
					filtro += this.filterParam.filtroPadrao() + ' ';
				}
			} else {
				filtro += filtroData;
			}
		} else {
			// Inclui o filtro padrão, se o mesmo foi definido
			if (this.filterParam.filtroPadrao() !== '') {
				filtro += this.filterParam.filtroPadrao() + ' ';
			}
		}

		this.fwmodelService.setModelo(this.filterParam.modelo());

		// Inclui o filtro de Empresa, caso seja necessário. Desde que o parâmetro seja true e o campo seja informado
		if (
			this.filterParam.useTenantId() &&
			this.juriGenerics.changeUndefinedToEmpty(this.filterParam.campoFilial()) != '' &&
			!this.filterParam.getTenantUseOnHeader()
		) {
			if (filtro != '') {
				filtro += 'and ';
			}

			filtro += this.filterParam.campoFilial() + "='" + this.filterParam.tenantId() + "'";
		}

		// Inclui o filtro do valor do combo
		if (valorAtu.value != '') {
			filtroLike = this.filterParam.campoFiltro().split(',');
			if (filtro != '') {
				filtro += 'and ';
			}

			filtro += '(';
			filtroLike.forEach(itemFiltLike => {
				filtro +=
					'UPPER(' +
					itemFiltLike +
					") LIKE '%" +
					valorAtu.value.toString().toUpperCase() +
					"%'or ";
			});

			filtro = filtro.substr(0, filtro.length - 3);
			filtro += ')';
		}

		// Se houve filtro informado, insere no FWModelService
		if (filtro != '') {
			filtro = filtro.trim();
			this.fwmodelService.setFilter(filtro);
		}

		if (this.filterParam.fwModelParams().bGetAllLevels) {
			this.fwmodelService.setFirstLevel(false);
		}

		if (this.filterParam.fwModelParams().bUseEmptyField) {
			this.fwmodelService.setCampoVazio(true);
		}

		if (this.filterParam.fwModelParams().bUseVirtualField) {
			this.fwmodelService.setCampoVirtual(true);
		}

		if (this.filterParam.fwModelParams().arrSetFields.length > 0) {
			this.fwmodelService.setFields(this.filterParam.fwModelParams().arrSetFields);
		}

		// Inclui os Headers extras, a parte do que já está no FWModel
		if (this.filterParam.getHeader() != undefined) {
			if (this.filterParam.getHeader().keys().length > 0) {
				this.filterParam
					.getHeader()
					.keys()
					.forEach(item => {
						this.fwmodelService.setHeaderParam(
							item,
							this.filterParam.getHeader().get(item)
						);
					});
			}
		}

		if (this.filterParam.useTenantId() && this.filterParam.getTenantUseOnHeader()) {
			this.fwmodelService.setHeaderParam('tenantid', this.filterParam.getTenantForHeader());
		}

		// Inclui os Options extras, a parte do que já está no FWModel
		if (this.filterParam.getParam() != undefined) {
			if (this.filterParam.getParam().keys().length > 0) {
				this.filterParam
					.getParam()
					.keys()
					.forEach(item => {
						this.fwmodelService.setQueryParam(
							item,
							this.filterParam.getParam().get(item)
						);
					});
			}
		}

		return this.fwmodelService.get(this.filterParam.operacao()).pipe(
			map((response: any) => {
				let listItensReturn: adaptorReturnStruct[] = [];
				if (this.juriGenerics.changeUndefinedToEmpty(response.resources) != '') {
					response.resources.forEach(modelos => {
						let itemReturn: adaptorReturnStruct = new adaptorReturnStruct();
						let aCampos: string[] = this.filterParam.campoValor().split(',');

						itemReturn.label = this.juriGenerics.findConcatValueByMultiNames(
							modelos.models[0].fields,
							this.filterParam.campoLabel(),
							this.juriGenerics.separador,
							this.filterParam.formatoLabel()
						);
						itemReturn.value = this.juriGenerics.findConcatValueByMultiNames(
							modelos.models[0].fields,
							this.filterParam.campoValor(),
							this.juriGenerics.separador,
							this.filterParam.formatoValor()
						);
						itemReturn.setExtras(
							this.makeExtras(
								modelos.models[0].fields,
								this.filterParam.filtrosExtras()
							)
						);

						/**
						 * Varre os campos que devem ser utilizados para concatenar valores para o value do combo
						 */
						aCampos.forEach(item => {
							let aSetExtras: extraFields = new extraFields();
							aSetExtras.id = item;
							aSetExtras.value = this.juriGenerics.findValueByName(
								modelos.models[0].fields,
								item
							);

							itemReturn.campos.push(aSetExtras);
						});

						listItensReturn.push(itemReturn);
					});
				}

				return listItensReturn;
			})
		);
	}

	/**
	 * Função de Carga. Ela é ativada na Carga da tela, quando é informado um valor ao FormControl.
	 * A função filtra pelo `valueField` os valores que retornam do campo.
	 * @param value - Valor a ser filtrado
	 * @param filterParam - Parâmetros fixos da requisição
	 *
	 * @example
	 * ```
	 * value="001-0001-00000001
	 * filterParam.valueField="A6_COD,A6_AGENCIA,A6_NUMCON"
	 *
	 * filtro="A6_COD=001 and A6_AGENCIA=0001 and A6_NUMCON=00000001"
	 * ```
	 */
	getObjectByValue(value: string | number): Observable<adaptorReturnStruct> {
		const filterParam = this.filterParam;
		this.fwmodelService.restore();
		this.fwmodelService.setModelo(filterParam.modelo());
		let filtro: string = '';
		const lstValue = value.toString().split(this.juriGenerics.separador);
		const lstChave = filterParam.campoValor().split(',');

		/**
		 * Se o valor do campo for concatenado sem ter o separador,
		 * ele concatena os campos de valor com || e joga o resultado setado na variável `value`
		 */
		if (
			filterParam.formatoValor() != '' &&
			filterParam.formatoValor().indexOf('-') == -1 &&
			filterParam.campoValor().indexOf(',') > -1
		) {
			filtro = filterParam.campoValor().replace(',', '||') + "= '" + value.toString() + "' ";
		} else {
			// Loop para criar o filtro padrão com o valor atual
			for (let index = 0; index < lstValue.length; index++) {
				filtro += lstChave[index] + "= '" + lstValue[index] + "'and ";
			}

			if (filtro != '') {
				filtro = filtro.substr(0, filtro.length - 4);
			}
		}

		// Inclui o filtro padrão, se o mesmo foi definido
		if (this.filtroP != '') {
			if (filtro != '') {
				filtro += 'and ';
			}
			filtro += this.filtroP + ' ';
		}

		// Inclui o filtro de Empresa, caso seja necessário. Desde que o parâmetro seja true e o campo seja informado
		if (
			filterParam.useTenantId() &&
			this.juriGenerics.changeUndefinedToEmpty(filterParam.campoFilial()) != '' &&
			!filterParam.getTenantUseOnHeader()
		) {
			if (filtro != '') {
				filtro += 'and ';
			}

			filtro += filterParam.campoFilial() + "='" + filterParam.tenantId() + "'";
		}

		// Se houve filtro informado, insere no FWModelService
		if (filtro != '') {
			this.fwmodelService.setFilter(filtro);
		}

		if (filterParam.fwModelParams().bGetAllLevels) {
			this.fwmodelService.setFirstLevel(false);
		}

		if (filterParam.fwModelParams().bUseEmptyField) {
			this.fwmodelService.setCampoVazio(true);
		}

		if (filterParam.fwModelParams().bUseVirtualField) {
			this.fwmodelService.setCampoVirtual(true);
		}

		if (filterParam.fwModelParams().arrSetFields.length > 0) {
			this.fwmodelService.setFields(filterParam.fwModelParams().arrSetFields);
		}

		// Inclui os Headers extras, a parte do que já está no FWModel
		if (filterParam.getHeader() != undefined) {
			if (filterParam.getHeader().keys().length > 0) {
				filterParam
					.getHeader()
					.keys()
					.forEach(item => {
						this.fwmodelService.setHeaderParam(item, filterParam.getHeader().get(item));
					});
			}
		}

		if (filterParam.useTenantId() && filterParam.getTenantUseOnHeader()) {
			this.fwmodelService.setHeaderParam('tenantid', filterParam.getTenantForHeader());
		}

		// Inclui os Options extras, a parte do que já está no FWModel
		if (filterParam.getParam() != undefined) {
			if (filterParam.getParam().keys().length > 0) {
				filterParam
					.getParam()
					.keys()
					.forEach(item => {
						this.fwmodelService.setQueryParam(item, filterParam.getParam().get(item));
					});
			}
		}

		return this.fwmodelService.get(filterParam.operacao()).pipe(
			map((response: any) => {
				let itemReturn: adaptorReturnStruct = new adaptorReturnStruct();
				let aCampos: string[] = filterParam.campoValor().split(',');

				if (this.juriGenerics.changeUndefinedToEmpty(response.resources) != '') {
					response.resources.forEach(modelos => {
						itemReturn.label = this.juriGenerics.findConcatValueByMultiNames(
							modelos.models[0].fields,
							filterParam.campoLabel(),
							this.juriGenerics.separador,
							filterParam.formatoLabel()
						);
						itemReturn.value = this.juriGenerics.findConcatValueByMultiNames(
							modelos.models[0].fields,
							filterParam.campoValor(),
							this.juriGenerics.separador,
							filterParam.formatoValor()
						);
						itemReturn.setExtras(
							this.makeExtras(modelos.models[0].fields, filterParam.filtrosExtras())
						);

						/**
						 * Varre os campos que devem ser utilizados para concatenar valores para o value do combo
						 */
						aCampos.forEach(item => {
							const aSetExtras: extraFields = new extraFields();
							aSetExtras.id = item;
							aSetExtras.value = this.juriGenerics.findValueByName(
								modelos.models[0].fields,
								item
							);

							itemReturn.campos.push(aSetExtras);
						});
					});
				}
				return itemReturn;
			})
		);
	}

	/**
	 * Gera o array de campos extras
	 * @param fields - Array resultante do JuriGenerics
	 * @param camposExtras - Array dos campos definidos no ExtraFilter do adaptorFilterParam
	 *
	 * @returns Array na estrutura `id` e `value`. Para buscar com o FindValueByName do JuriGenerics
	 */
	makeExtras(fields: any, camposExtras: string[]): extraFields[] {
		let listFields: extraFields[] = [];
		camposExtras.forEach(campo => {
			let valor: extraFields = new extraFields();
			valor.id = campo;
			valor.value = this.juriGenerics.changeUndefinedToEmpty(
				this.juriGenerics.findValueByName(fields, campo)
			);

			listFields.push(valor);
		});
		return listFields;
	}
}

/**
 * Estrutura de Retorno do Adaptor. Fixado para não haver manipulação ou inclusão de novos elementos.
 * Sem que o desenvolvedor tenha de alterar aqui.
 * 
 * Ao alterar essa classe, verificar os locais aonde é utilizado o adaptor.
 * 
 * @implements PoComboOption
 * @notes Para usar o extras é necessário fazer um Cast da propriedade conforme demonstrado abaixo:
 * @example 
 * ```
 * setValidContabil(event: any){
 *		if (event != undefined){
 *			let fieldsExtra: adaptorReturnStruct = <adaptorReturnStruct>this.listTipoDespesa.selectedOption
 *			this.bValidContabil = this.JuriGenerics.findValueByName(fieldsExtra.getExtras(),"NSR_INTCTB")
 *		} else {
 *			this.bValidContabil = false
 *		}
 *	}
	```
	- No caso é recebido o `$event` do `p-change` do combo de Tipo de Despesa. E o `selectedOption` é do tipo `PoComboOption`.
	Como o `adaptorReturnStruct` herda dele, temos de fazer um Cast para acessar a propriedade "Extras"
 */
export class adaptorReturnStruct implements PoComboOption {
	label: string;
	value: string;
	extras: extraFields[] = [];
	campos: extraFields[] = [];

	setExtras(extraFieldsArray: extraFields[]) {
		this.extras = extraFieldsArray;
	}
	getExtras() {
		return this.extras;
	}
}

class extraFields {
	id: string;
	value: string;
}

/**
 * Classe para os parâmetros que serão passados ao Adaptor
 *
 */
export class adaptorFilterParam {
	private nomeModelo: string;
	private nomeOperacao: string;
	private campoFilter: string;
	private valueField: string;
	private labelField: string;
	private formatLabel: string;
	private defaultFilter: string;
	private queryParam: HttpParams;
	private headerParam: HttpHeaders;
	private filterTenantId: TenantId;
	private fwModelFilters: FwModelParam;
	private formatValue: string;
	private extraFields: string;

	/**
	 * Estrutura do Adaptor para montagem da requisição
	 *
	 * @param modelo - Modelo a ser utilizado
	 * @param defaultFilter - Filtro padrão
	 * @param campoFilter - Campo utilizado para fazer o filtro dinâmico. `É filtrado pelo valor digitado`
	 * @param labelField - Campos a serem usados na montagem do Label de retorno. Valores podem ser concatenados.
	 * @param formatLabel - Formato do label a ser apresentado
	 * @param valueField - Campos a serem usados na montagem do Valor de retorno. Valores podem ser concatenados.
	 * @param nomeOperacao - Nome da operação a ser executada
	 * @param headerParam - Headers adicionais
	 * @param queryParam - Options adicionais
	 * @param tenantId - TenantId {useTenantId: true, empresa: "T1", filial: "D MG 01 ", campoFiltro: "A6_FILIAL"}
	 * @param extraFields - Campos extras para o retorno do Adaptor. Valores concatenados por ",".
	 *
	 * @example
	 * ```
	 * adaptorParamBanco: adaptorFilterParam = new adaptorFilterParam("JBANCO","","A6_AGENCIA,A6_NUMCON","A6_AGENCIA,A6_NUMCON","A6_AGENCIA\\A6_NUMCON","A6_COD,A6_AGENCIA,A6_NUMCON","getBanco",undefined,undefined,{useTenantId: true, empresa: "T1", filial: "D MG 01 ", campoFiltro: "A6_FILIAL"})
	 *
	 * adaptorParamTitulo: adaptorFilterParam = new adaptorFilterParam("JURA195","","X5_DESCRI","X5_DESCRI","","X5_CHAVE","","getTitulo",undefined, new HttpParams().append("SX5TAB","05"))
	 *
	 * ```
	 */
	constructor(
		modelo: string,
		defaultFilter: string = '',
		campoFilter: string,
		labelField: string,
		formatLabel: string = '',
		valueField: string = '',
		formatValue: string = '',
		nomeOperacao: string,
		headerParam?: HttpHeaders,
		queryParam?: HttpParams,
		tenantId?: TenantId,
		fwParams?: FwModelParam,
		extraFields?: string
	) {
		this.nomeModelo = modelo;
		this.campoFilter = campoFilter;
		this.nomeOperacao = nomeOperacao;
		this.valueField = valueField;
		this.labelField = labelField;
		this.defaultFilter = defaultFilter;
		this.formatLabel = formatLabel;
		this.headerParam = headerParam;
		this.queryParam = queryParam;
		this.formatValue = formatValue;

		if (extraFields == undefined) {
			this.extraFields = '';
		} else {
			this.extraFields = extraFields;
		}

		if (tenantId == undefined) {
			this.filterTenantId = new TenantId(false, 'T1', 'D MG 01 ');
		} else {
			this.filterTenantId = tenantId;
		}

		if (fwParams == undefined) {
			this.fwModelFilters = new FwModelParam();
		} else {
			this.fwModelFilters = fwParams;
		}
	}

	/**
	 * Define o novo filtro padrão para o Adapter
	 * @param newFilter - Novo filtro, irá substituir o antigo
	 */
	setDefaultFilter(newFilter: string) {
		this.defaultFilter = newFilter;
	}

	/**
	 * Define o novo Header padrão para o Adapter
	 * @param newHeader - Novo Header. Irá substituir o antigo
	 */
	setHeaderParam(newHeader: HttpHeaders) {
		this.headerParam = newHeader;
	}

	/**
	 * Define o novo Query padrão para o Adapter
	 * @param newQuery - Novo QueryParam. Irá substituir o antigo
	 */
	setQueryParam(newQuery: HttpParams) {
		this.queryParam = newQuery;
	}

	setTenantFilial(tenantFili: string) {
		this.filterTenantId.filial = tenantFili;
	}

	/**
	 * Retorna o nome do Modelo
	 */
	modelo(): string {
		return this.nomeModelo;
	}

	/**
	 * Retorna o Filtro padrão
	 */
	filtroPadrao(): string {
		return this.defaultFilter;
	}

	/**
	 * Retonar o nome da Operação
	 */
	operacao(): string {
		return this.nomeOperacao;
	}

	/**
	 * Retorna os campos de filtro
	 */
	campoFiltro(): string {
		return this.campoFilter;
	}

	/**
	 * Retorna os campos de valor
	 */
	campoValor(): string {
		return this.valueField;
	}

	/**
	 * Retorna o formato do Valor
	 */
	formatoValor(): string {
		return this.formatValue;
	}

	/**
	 * Retonar os campos que compõem o label
	 */
	campoLabel(): string {
		return this.labelField;
	}

	/**
	 * Formato do Label, se deseja incluir
	 */
	formatoLabel(): string {
		return this.formatLabel;
	}

	/**
	 * Retonar os Headers Adicionais
	 */
	getHeader(): HttpHeaders {
		return this.headerParam;
	}

	/**
	 * Retorna os Options adicionais
	 */
	getParam(): HttpParams {
		return this.queryParam;
	}

	/**
	 * Retorna se vai utilizar o fitro de Filial
	 */
	useTenantId(): boolean {
		return this.filterTenantId.useTenantId;
	}

	getTenantUseOnHeader(): boolean {
		return this.filterTenantId.tenantOnHeader;
	}

	getTenantForHeader(): string {
		let tenantId: string = this.filterTenantId.empresa;

		if (this.filterTenantId.filial != undefined && this.filterTenantId.filial != '') {
			tenantId += ',' + this.filterTenantId.filial;
		}

		return tenantId;
	}

	/**
	 * Campo a ser filtrado
	 */
	campoFilial(): string {
		return this.filterTenantId.campoFiltro;
	}

	/**
	 * Valor da Filial para filtro
	 */
	tenantId(): string {
		return this.filterTenantId.filial;
	}

	fwModelParams(): FwModelParam {
		return this.fwModelFilters;
	}

	filtrosExtras(): string[] {
		return this.extraFields.split(',');
	}
}

/**
 * Classe do TenantId
 */
export class TenantId {
	useTenantId: boolean;
	empresa: string = 'T1';
	filial: string = 'D MG 01 ';
	campoFiltro: string;
	tenantOnHeader: boolean = false;

	/**
	 * Construtor do TenantID
	 * @param useTenantIdFilter - Boolean - Indica se vai usar o filtro de TenantId
	 * @param codEmpresa - Código da Empresa
	 * @param codFilial - Código da Filial para filtro
	 * @param campoFiltro - Campo de Filial para ser filtrado
	 */
	constructor(
		useTenantIdFilter: boolean,
		codEmpresa: string,
		codFilial: string,
		campoFiltro?: string,
		useOnHeader?: boolean
	) {
		this.useTenantId = useTenantIdFilter;
		this.empresa = codEmpresa;
		this.filial = codFilial;
		this.campoFiltro = campoFiltro;
		this.tenantOnHeader = useOnHeader;
	}
}

/**
 * Classe para os parâmetros do FWModel. Ativa os sets disponiveis no FWModel
 */
export class FwModelParam {
	bUseEmptyField: boolean = true;
	bUseVirtualField: boolean = false;
	bGetAllLevels: boolean = false;
	arrSetFields: string[] = [];

	/**
	 * Constructor do FWModelParam
	 * @param getEmptyFields - Utiliza o OptionParam de EmptyFields?
	 * @param getVirtualFields - Utiliza o OptionParam de VirtualFields?
	 * @param getAllLevels - Utiliza o OptionParams de FirstLevel?
	 * @param fieldsArrays - Array de campos a serem retornados
	 */
	constructor(
		getEmptyFields: boolean = true,
		getVirtualFields: boolean = false,
		getAllLevels: boolean = false,
		fieldsArrays: string[] = []
	) {
		this.bUseEmptyField = getEmptyFields;
		this.bUseVirtualField = getVirtualFields;
		this.bGetAllLevels = getAllLevels;
		this.arrSetFields = fieldsArrays;
	}
}

@Injectable({
	providedIn: 'root',
})

/**
 *  Busca a lista de filiais que o usuário possui acesso
 */
export class FwGetFilDesAdapterService implements PoComboFilter {
	constructor(
		private juriGenerics: JurigenericsService,
		private router: Router,
		private frameApi: frameApi
	) {}

	/**
	 *  Busca a lista de filiais que o usuário possui acesso de acordo com o conteúdo digitado
	 */
	getFilteredData(valor): Observable<Array<PoComboOption>> {
		let listItensReturn: defaultReturnStruct[] = [];

		this.frameApi.restore();
		this.frameApi.setPageSize('1000');
		this.frameApi.setFilter('EnterpriseGroup', window.localStorage.getItem('codEmp'));
		this.frameApi.setMethod('environment/v1/branches');
		return this.frameApi.executeGet().pipe(
			map((data: any) => {
				if (data.items != '') {
					data.items.forEach(filial => {
						let filiais = new defaultReturnStruct();

						filiais.value = filial.Code;
						filiais.label = filial.Description;

						if (this.juriGenerics.changeUndefinedToEmpty(valor) != '') {
							if (
								filial.Description.toUpperCase().match(valor.value.toUpperCase()) !=
								null
							) {
								listItensReturn.push(filiais);
							}
						} else {
							listItensReturn.push(filiais);
						}
					});
				}
				return listItensReturn;
			})
		);
	}

	/**
	 *  Busca a filial selecionada de aocrdo com o código
	 */
	getObjectByValue(valor: string): Observable<defaultReturnStruct> {
		this.frameApi.restore();
		this.frameApi.setPageSize('1000');
		this.frameApi.setFilter('Code', valor);
		this.frameApi.setMethod('environment/v1/branches');

		return this.frameApi.executeGet().pipe(
			map((data: any) => {
				let filiais = new defaultReturnStruct();
				if (data.items != '') {
					data.items.forEach(filial => {
						filiais.value = filial.Code;
						filiais.label = filial.Description;
					});
				}
				return filiais;
			})
		);
	}
}
