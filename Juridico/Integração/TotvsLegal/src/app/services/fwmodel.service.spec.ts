import { TestBed } from '@angular/core/testing';

import { FwmodelService } from './fwmodel.service';

describe('FwmodelService', () => {
	beforeEach(() => TestBed.configureTestingModule({}));
	
	it('should be created', () => {
		const service: FwmodelService = TestBed.get(FwmodelService);
		expect(service).toBeTruthy();
	});
});
