import { Injectable } from '@angular/core';
import { protheusService } from './protheus.service';
import { JurigenericsService, JurUTF8 } from './jurigenerics.service';

const endpoint = "FWMODEL/";

@Injectable({
	providedIn: 'root'
})
export class FwmodelService {

	private cModelo   : string;  //--Modelo
	private cBody     : string;  //--Corpo
	private chave     : string;
	
	/**
	 * Construtor do FWModelService
	 * @param protheusService
	 */
	constructor(private protheusService: protheusService, private JuriGenerics: JurigenericsService) {
	}
	
	/**
	 * Define o modelo
	 * @param cModelo - Nome do Modelo
	 */
	setModelo(cModelo: string) {
		this.cModelo = cModelo;
	}

	/**
	 * Define a PK
	 * @param chave - Código da PK 
	 */
	setChave(chave: string){
		this.chave = chave;
	}
	/**
	 * Insere novos itens ao HeaderParams
	 * @param paramName - Nome do Parâmetro
	 * @param value - Valor
	 */
	setHeaderParam(paramName: string, value: any){
		this.protheusService.setOptionsHeaders(paramName, value);
	}

	/**
	 * Retorna o valor do Header passado por parâmetro
	 * @param paramName - Header solicitado
	 */
	getHeaderValue(paramName: string): string{
		return this.protheusService.getHeaderValue(paramName);
	}

	/**
	 * Inclui novos itens ao OptionParams
	 * @param paramName - Nome do Parâmetro
	 * @param value - Valor
	 */
	setQueryParam(paramName: string, value: any){
		this.protheusService.setOptionsParams(paramName, value);
	}

	/**
	 * Retorna o valor do Query passao por parâmetro
	 * @param paramName 
	 */
	getQueryParam(paramName: string): string{
		return this.protheusService.getParamValue(paramName);
	}

	/**
	 * Retorna os campos Vazios. FIELDEMPTY
	 * @param lStatus 
	 */
	setCampoVazio(lStatus: Boolean = true) {
		//Traz campos vazios por padrão
		this.protheusService.setOptionsParams("FIELDEMPTY", lStatus.toString());
	}

	/**
	 * Inclui os campos virtuais no retorno da requisição. FIELDVIRTUAL
	 * @param lStatus 
	 */
	setCampoVirtual(lStatus: Boolean) {
		this.protheusService.setOptionsParams("FIELDVIRTUAL", lStatus.toString());
	}

	/**
	 * Quantidade de itens na pagina. COUNT
	 * @param pagesize - Quantidade de itens por pagina
	 */
	setPageSize(pagesize: string) {
		this.protheusService.setOptionsParams("COUNT", pagesize);
	}

	/**
	 * Pagina a ser mostrada. STARTINDEX
	 * @param page - numero da pagina
	 */
	setPage(page: string) {
		this.protheusService.setOptionsParams("STARTINDEX", page);
	}

	/**
	 * Inclui filtro na requisição. FILTER
	 * @param query - Filtro a ser inserido
	 */
	setFilter(query: string) {
		let queryExist = this.getQueryParam("FILTER");
		if (queryExist != "" && queryExist != undefined){
			query = queryExist + " AND " + query;
		}
		this.protheusService.setOptionsParams("FILTER", query);
	}

	/**
	 * Inclui o filtro por palavra chave
	 * @param query - Filtro a ser feito
	 */
	setSearchKey(query: string){
		let queryExist = this.getQueryParam("FILTER");
		if (queryExist != "" && queryExist != undefined){
			query = queryExist + " AND " + query;
		}
		this.protheusService.setOptionsParams("FILTER", query);
	}
	/**
	 * Retorna somente o Primeiro nivel? FIRSTLEVEL
	 * @param lStatus - Se `true` irá retornar somente o modelo principal. Se `false` retornará todos os modelos
	 */
	setFirstLevel(lStatus: Boolean) {
		this.protheusService.setOptionsParams("FIRSTLEVEL", lStatus.toString());
	}

	/**
	 * Restaura as configurações padrões do ProtheusService
	 */
	restore() {
		this.protheusService.restore();
		this.setCampoVazio(true);
	}

	/**
	 * Informa os campos que devem ser retornados. FIELDS
	 * @param campos - Campos a serem definidos
	 */
	setFields(campos?: Array<string>){
		this.protheusService.setOptionsParams("FIELDS", campos.toString() );
	}

	/**
	 * Executa a Requisição HTTP GET
	 * @param nameOperation - Nome da Operação
	 */
	get(nameOperation: string = "GetFWModel") {
		return this.protheusService.get(endpoint + this.cModelo, nameOperation);
	}

	/**
	 * Executa a requisição HTTP DELETE
	 * @param codPk  - PK do registro a ser deletado
	 * @param nameOperation  - Nome da operação
	 */
	delete(codPk: string, nameOperation: string = "deleteFWModel"){

		return this.protheusService.delete(endpoint + this.cModelo + '/' + codPk, nameOperation);
	}

	/**
	 * Executa a requisição HTTP PUT
	 * @param codPrimaryKey - Código PK do registro a ser atualizado
	 * @param bodyReq - Body da requisição
	 * @param nameOperation - Nome da operação
	 */
	put(codPrimaryKey:string, bodyReq: string, nameOperation: string = "putFWModel"){

		this.protheusService.setBody(bodyReq);
		return this.protheusService.put(endpoint + this.cModelo + '/' + codPrimaryKey, nameOperation);
	}

	/**
	 * Executa a requisição HTTP POST
	 * @param cBody - Body da requisição
	 * @param nameOperation - Nome da operação
	 */
	post(cBody: string, nameOperation: string = "postFWModel"){
		
		this.protheusService.setBody(cBody);
		return this.protheusService.post(endpoint + this.cModelo, nameOperation);
	}

	/**
	 * Função de consumo do serviço Rest FWModel
	 * GET
	 */
	getFavoritos() {
		return this.protheusService.get(endpoint + this.cModelo, 'Lista de Favoritos');
	}

	/**
	 * Função de consumo do serviço Rest FWModel
	 * GET
	 */
	getGarantias() {
		return this.protheusService.get(endpoint + this.cModelo, 'Lista de Garantias');
	}

	/**
	 * DELETE
	 */
	deleteFavoritos(id: string) {
		return this.protheusService.delete(endpoint + this.cModelo + "/" + id, 'Exclusão do Favorito');
	}

	/* POST Torna o processo favorito*/
	postFavorito(cajuri: string, usuario: string) {
		this.protheusService.setBody(this.favBody(cajuri, usuario));
		return this.protheusService.post(endpoint + this.cModelo, 'Incluir Favorito');
	}

	favBody(cajuri: string, user: string) {
		this.cBody = '{"id": "JURA269", "operation": 1, "pk": "", ';
		this.cBody += '"models": [ { "id": "O0VMASTER", "modeltype": "FIELDS", ';
		this.cBody += ' "fields": [ { "id": "O0V_CAJURI", "order": 3, "value": "' + cajuri + '" },';
		this.cBody += ' { "id": "O0V_USER", "order": 4, "value": "' + user + '" }] ';
		this.cBody +=         ' } ]';
		this.cBody += '}';

		return this.cBody
	}

	/**
	 * Retorna os Andamentos
	 */
	getAndamentos() {
		return this.protheusService.get(endpoint + this.cModelo, 'Lista de Andamentos');
	}

	/**
	* DELETE Garantia
	*/
	deleteGarantia(id: string){
		return this.protheusService.delete(endpoint + this.cModelo + "/" + id, 'Exclusão da Garantia');
	}

	/**
	 * Retorna os Levantamentos
	 */
	getLevantamentos() {
		return this.protheusService.get(endpoint + this.cModelo, 'Lista de Levantamentos');
	}

}


/** @type modelOperation - informe qual a operação que deseja executar */
type modelOperation = 1 | 3 | 4 | 5

/** @type modelType - informe se o modelo será do tipo Field ou Grid */
type modelType = "FIELDS" | "GRID";

/** @type booleanNumber - informa se deverá ser verdadeiro = 1 ou falso= 0*/
type booleanNumber = 0 | 1;

/**
 * classe especializada na montagem do body da requisição de put/post do fwmodel
 */
export class FwModelBody {
	/** @indice Definido um indice para classe, onde possa ser inclusa qualquer propriedade caso necessário*/
	[x: string]: any;

	models: Array<FwModelBodyModels>;

	/**
	 *  metodo construtor da classe FwModelBody
	 * @param id - Nome do modelo a ser usado
	 * @param operation - operação realizada conforme o protheus
	 * @param pk - primary key do registro
	 * @param mainModel - informa qual é o modelo principal do body
	 */
	constructor(
		private id: string,
		private operation?: modelOperation,
		private pk?: string,
		mainModel?:string
	) {
		if (typeof mainModel != "undefined"){
			this.addModel(mainModel)
		}
	}

	/**
	 * método responsavel pela inclusão de modelo
	 * @param id - id do modelo a ser adicionado
	 * @param modeltype - tipo de modelo, sendo ele Field ou uma Grid
	 * @param optional -- Informa se o modelo será opcional
	 * @returns FwModelBodyModels
	 */
	addModel(
		id: string,
		modeltype: modelType = "FIELDS",
		optional?: booleanNumber
	): FwModelBodyModels {
		let model: FwModelBodyModels
		if (typeof this.models == "undefined") {
			this.models = []
		}

		model = new FwModelBodyModels(id, modeltype, optional);

		this.models.push(model)

		return model
	}

	/**
	 * metodo responsavel pela serialização do objeto em string
	 * @returns objeto serializado em string
	 */
	serialize(): string {
		return JSON.stringify(this)
	}
}
/**
 * Classe responsavel pela definição do modelo do body
 */
export class FwModelBodyModels {

	fields?: Array<FwModelBodyFields>
	items?: Array<FwModelBodyItems>;
	struct?: Array<any>;
	models?: Array<FwModelBodyModels>

	/**
	 * método responsavel pela definição do modelo do body
	 * @param id - nome do modelo a ser utilizado
	 * @param modeltype - definição do tipo de modelo, sendo ele um Field ou um Grid
	 * @param optional - define se o modelo será opicional ou não
	 */
	constructor(
		private id: string,
		private modeltype: modelType = "FIELDS",
		private optional?: booleanNumber
	) { }

	/**
	 * Método responsavel pela adição de campos no modelo
	 * @param id - Id do campo a ser inserido
	 * @param value - valor do campo
	 * @param order - ordem da qual o campo deverá ser executado, caso não informado ou definido como 0, será utilizado a sequencia de inclusão
	 * @param type - informe o tipo de variavel
	 * @returns FwModelBodyFields
	 */
	addField(
		id: string,
		value: string | number,
		order?: number,
		type?: "C" | "N" | "D" | "L"
	): FwModelBodyFields {
		let field: FwModelBodyFields

		if (typeof this.fields == "undefined") {
			this.fields = []
		}

		field = new FwModelBodyFields(id)
		field.setValue(value, type);
		field.setOrder((typeof order == "undefined" || order == 0) ? this.fields.length + 1 : order);

		this.fields.push(field)

		return field
	}
	/**
	 * método responsavel pela adição de itens
	 * @param id - numero sequencial do item, caso não informado ou informado 0, será utilizado o numero sequencial conforme adicionado
	 * @param deleted informa se é deletado ou não
	 * @returns FwModelBodyItems
	 */
	addItems(
		id: number = 0,
		deleted: booleanNumber = 0
	): FwModelBodyItems {
		let item: FwModelBodyItems;

		if (typeof this.items == "undefined") {
			this.items = [];
		}

		if (id == 0) {
			id = this.items.length + 1
		}

		item = new FwModelBodyItems(id, deleted);
		this.items.push(item);

		return item;
	}
	/**
	 * método responsavel pela inserção de um modelo
	 * @param id id do modelo a ser inserido
	 * @param modeltype tipo do modelo, sendo Field ou Grid
	 * @param optional define se será opicional
	 * @returns FwModelBodyModels
	 */
	addModel(
		id: string,
		modeltype: modelType = "FIELDS",
		optional?: booleanNumber
	): FwModelBodyModels {
		let model: FwModelBodyModels

		if (typeof this.models == "undefined") {
			this.models = []
		}

		model = new FwModelBodyModels(id, modeltype, optional);
		this.models.push(model)
		return model
	}
}


/**
 * classe reponsavel pela definição da estrutura de itens
 */
export class FwModelBodyItems {

	fields: Array<FwModelBodyFields> = []

	/**
	 * metodo construtor da classe de itens do body
	 * @param id - informe o id sequencial do registro
	 * @param deleted informa se o registro deverá ser excluido ou não
	 */
	constructor(
		private id: number = 0,
		private deleted: booleanNumber = 0
	) { }

	/**
	 * método responsavel pela inserção dos campos no modelo
	 * @param id - informe o nome do campo
	 * @param value - informe o valor do campo
	 * @param type - informe o tipo de campo que deverá ser convertido
	 * @returns FwModelBodyFields
	 */
	addField(
		id: string,
		value: string | number,
		type?: "C" | "N" | "D" | "L"
	): FwModelBodyFields {
		let field = new FwModelBodyFields(id)

		field.setValue(value, type);

		this.fields.push(field)

		return field
	}
}

/**
 * classe responsavel pela definição de fields do modelo/item
 */
export class FwModelBodyFields {
	/**
	 * método construtor da classe de fields
	 * @param id - nome do campo a ser criado
	 * @param value conteudo do campo
	 * @param order informa a ordem que o campo deverá ser executado
	 */
	constructor(
		private id: string,
		private value?: string | number | boolean,
		private order?: number
	) { }

	/**
	 * método responsavel pelo preenchimento do conteudo do campo
	 * @param value - valor do campo a ser preenchido
	 * @param type - tipo a ser atribuido/convertido
	 */
	setValue(
		value: string | number | boolean,
		type: string = ""
	) {
		if (typeof value == "undefined" || value == null) {
			if (type == "N") {
				value = 0
			} else {
				value = ""
			}
		} else if (typeof value == "string") {
			if (type == "D") {
				value = value.replace(/-/g, '')
			}
			
		} else if (typeof value == "number" && type == "C") {
			value = value.toString()

		} else if (typeof value == "boolean" && type == "C") {
			value = value.toString()

		} else if (typeof value == "boolean" && type == "N") {
			value = value ? 1 : 0
		}
		this.value = value
	}
	/**
	 * método responsavel pelo preenchimento da propriedade de ordem
	 * @param order - informe o numero da ordem do campo
	 */
	setOrder(order: number) {
		if (typeof order != "undefined") {
			this.order = order
		}
	}
}
