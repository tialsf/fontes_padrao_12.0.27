import { Injectable } from '@angular/core';
import { LegalprocessService } from './legalprocess.service';
import { Observable } from 'rxjs';
import { PoComboOption, PoComboFilter } from '@po-ui/ng-components';
import { map } from 'rxjs/operators';
import { JurigenericsService } from './jurigenerics.service';

@Injectable({
	providedIn: 'root'
})
export class HistoricoAdapterService implements PoComboFilter{

	constructor(
		private legalprocess: LegalprocessService,
		private juriGenerics: JurigenericsService,
	) { }
	
	/**
	 * Função executada no momento em que for digitado ou selecionado o combo
	 * @param valorAtu - Valor digitado atual
	 */
	getFilteredData(valorAtu, filterParams: histAdapterFilterParam): Observable<Array<PoComboOption>>{
		this.legalprocess.restore();
		this.legalprocess.setFilter('dataIni'  ,filterParams.params[1]);
		this.legalprocess.setFilter('dataFinal',filterParams.params[2]);
		this.legalprocess.setFilter('usuario'  ,filterParams.params[3]);
		this.legalprocess.setSearchKey(valorAtu.value);

		return this.legalprocess.get("process/"+filterParams.params[0]+"/historico").pipe(map((data: any) => {
			let listItensReturn: adaptorReturnStruct[] = [];
			if (this.juriGenerics.changeUndefinedToEmpty(data.log) != '') {
				data.log.forEach(item => {
					if(listItensReturn.indexOf(item.field) === -1){
						let itemReturn = new adaptorReturnStruct();
						itemReturn.label = item.fieldName;
						itemReturn.value = item.field;
						listItensReturn.push(itemReturn);
					};
				});
				return listItensReturn;
			};
		}))
	};

	/**
	 * Função responsável a realizar a consulta quando um Valor é definido para o campo que utiliza o Adapter
	 * @param value - Valor setado
	 */
	getObjectByValue(valorAtu, filterParams: histAdapterFilterParam): Observable<PoComboOption>{
		return new Observable<PoComboOption>();
	};
}

/**
 * Estrutura de Retorno do Adaptor. Fixado para não haver manipulação ou inclusão de novos elementos
 * Sem que o desenvolvedor tenha de alterar aqui.
 * @
 * Ao alterar essa classe, verificar os locais aonde é utilizado o adaptor.
 */
export class adaptorReturnStruct implements PoComboOption{
	value: string;
	label: string;
}

/**
 * Campos Parametro do adapter de historico de alterações
 */
export class histAdapterFilterParam {
	params : Array<string> = [];
	constructor(cajuri : string, dataInicial ?: string, dataFinal ?: string, usuario ?: string){
		this.params = [cajuri, dataInicial, dataFinal, usuario];
	}
	
	getParams(){
		return this.params;
	}
}
