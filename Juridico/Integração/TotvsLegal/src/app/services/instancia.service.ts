import { Injectable } from '@angular/core';
import { FwmodelService } from './fwmodel.service';
import { PoI18nService } from '@po-ui/ng-components';
import { JurigenericsService } from './jurigenerics.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

/**
 * Serviço para busca de informação de instância
 */
@Injectable({
	providedIn: 'root'
})
export class InstanciaService {
	litInstancia: any
	pkProcesso:   string        = ""
	codInst:      string        = ""
	fields:       Array<string> = [
		"NUQ_COD",
		"NUQ_INSTAN",
		"NUQ_DTIPAC",
		"NUQ_NUMPRO"
	]

	constructor(
		private fwModel: FwmodelService,
		private poi18nService: PoI18nService,
		private juriGenerics: JurigenericsService
	) {
		this.poi18nService
			.getLiterals({ context: 'instanciaService', language: poi18nService.getLanguage() })
			.subscribe(
				(litInstancia) => {
					this.litInstancia = litInstancia
				}
			)
	}

	/**
	 * Busca dados de uma determinada instância
	 * @param pkProcesso - Chave unica do processo
	 * @param codInst    - Código da instância
	 */
	getDadosInstancia(pkProcesso: string, codInst: string): Observable<RetDadosInstancia> {
		this.fwModel.restore();
		this.fwModel.setModelo('JURA095/' + pkProcesso);
		this.fwModel.setCampoVirtual(true);
		this.fwModel.setFirstLevel(false);
		this.fwModel.setFields(this.fields);

		return this.fwModel
			.get('Busca Instâncias')
			.pipe(
				map(
					(data) => {
						let retDadosInstancia: RetDadosInstancia = new RetDadosInstancia()
						let listIntancias = data.models[0].models[
							data.models[0].models.findIndex(x => x.id == 'NUQDETAIL')
						].items

						listIntancias.forEach(inst => {

							if (this.juriGenerics.findValueByName(inst.fields, "NUQ_COD") === codInst) {
								
								retDadosInstancia.cod = this.juriGenerics.changeUndefinedToEmpty(this.juriGenerics.findValueByName(inst.fields, "NUQ_COD"));
								retDadosInstancia.instancia = this.getDesctInstancia(
									this.juriGenerics.changeUndefinedToEmpty(
										this.juriGenerics.findValueByName(inst.fields, "NUQ_INSTAN")
									)
								);
								retDadosInstancia.tpAcao = this.juriGenerics.changeUndefinedToEmpty(this.juriGenerics.findValueByName(inst.fields, "NUQ_DTIPAC"));
								retDadosInstancia.nrProcesso = this.juriGenerics.changeUndefinedToEmpty(this.juriGenerics.findValueByName(inst.fields, "NUQ_NUMPRO"));
							}
						});

						return retDadosInstancia
					}
				)
			)
	}

	/**
	 * Retorna descrição da instância
	 * @param value - Valor do campo NUQ_INSTAN
	 */
	getDesctInstancia(value: string = '') {
		switch (value) {
			case '1':
				return this.litInstancia.primeira;

			case '2':
				return this.litInstancia.segunda;

			case '3':
				return this.litInstancia.tribunalSuperior;

			default:
				return '';
		}
	}
}

/**
 * Classe de estrutura para o serviço de instância
 */
export class RetDadosInstancia {
	cod:        string = "";
	instancia:  string = "";
	tpAcao:     string = "";
	nrProcesso: string = "";
}