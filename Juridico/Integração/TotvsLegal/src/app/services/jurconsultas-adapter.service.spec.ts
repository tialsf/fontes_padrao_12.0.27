import { TestBed } from '@angular/core/testing';

import { JurconsultasAdapterService } from './jurconsultas-adapter.service';

describe('JurconsultasAdapterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JurconsultasAdapterService = TestBed.get(JurconsultasAdapterService);
    expect(service).toBeTruthy();
  });
});
