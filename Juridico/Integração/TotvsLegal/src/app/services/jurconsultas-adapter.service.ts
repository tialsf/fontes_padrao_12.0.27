import { Injectable } from '@angular/core';
import { JurconsultasService } from '../services/jurconsultas.service';
import { Observable } from 'rxjs';
import { PoComboOption, PoComboFilter } from '@po-ui/ng-components';
import { map } from 'rxjs/operators';
import { JurigenericsService } from '../services/jurigenerics.service';

@Injectable({
	providedIn: 'root',
})
export class JurconsultasAdapterService implements PoComboFilter {
	constructor(
		private jurconsultas: JurconsultasService,
		private JuriGenerics: JurigenericsService
	) {}

	/**
	 * Função executada no momento em que for digitado ou selecionado o combo
	 * @param valorAtu - Valor digitado atual
	 */
	getFilteredData(valorAtu): Observable<Array<PoComboOption>> {
		this.jurconsultas.restore();
		this.jurconsultas.setParam('searchKey', valorAtu.value);
		this.jurconsultas.setParam('pageSize', '10');
		this.jurconsultas.setParam('assJur', '006');

		return this.jurconsultas.get('contracts', 'getContractsAdapter').pipe(
			map((data: any) => {
				let listItensReturn: adaptorReturnStruct[] = [];
				if (this.JuriGenerics.changeUndefinedToEmpty(data.contracts) != '') {
					data.contracts.forEach(contract => {
						let itemReturn = new adaptorReturnStruct();
						itemReturn.label =
							contract.id.replace(new RegExp('^0*', 'g'), '') +
							' - ' +
							contract.description;
						if (data.hasMoreBranch && contract.branch != '') {
							itemReturn.label += ' - ' + contract.branch;
						}
						itemReturn.value = btoa(contract.branch + '/' + contract.id.toString());

						listItensReturn.push(itemReturn);
					});
				}
				return listItensReturn;
			})
		);
	}

	getObjectByValue(value: string): Observable<PoComboOption> {
		this.jurconsultas.restore();
		this.jurconsultas.setParam('searchKey', value);
		this.jurconsultas.setParam('pageSize', '10');
		this.jurconsultas.setParam('assJur', '006');

		return this.jurconsultas.get('contracts', 'getContractsAdapter').pipe(
			map((data: any) => {
				let itemReturn = new adaptorReturnStruct();
				if (this.JuriGenerics.changeUndefinedToEmpty(data.contracts) != '') {
					data.contracts.forEach(contract => {
						itemReturn.label = contract.description + ' - ' + contract.id;
						itemReturn.value = contract.id;
					});
				} else {
					this.getObjectByValue(value);
				}

				return itemReturn;
			})
		);
	}
}

@Injectable({
	providedIn: 'root',
})
export class GetDetModelsAdapterService implements PoComboFilter {
	constructor(
		private jurconsultas: JurconsultasService,
		private JuriGenerics: JurigenericsService
	) {}

	/**
	 * Função executada no momento em que for digitado ou selecionado o combo
	 * @param value - Valor digitado atual
	 */
	getFilteredData(value): Observable<Array<PoComboOption>> {
		let codAssJuridico: string = '';

		codAssJuridico = window.sessionStorage.getItem('assuntoJuri');
		this.jurconsultas.restore();
		this.jurconsultas.setParam('assjur', codAssJuridico);

		if (this.JuriGenerics.changeUndefinedToEmpty(value.value) != '') {
			this.jurconsultas.setParam('filtraMod', value.value);
		}

		return this.jurconsultas.get('getTemplate', 'getTemplateAdapter').pipe(
			map((data: any) => {
				const listItensReturn: Array<PoComboOption> = [];
				if (this.JuriGenerics.changeUndefinedToEmpty(data.listModels) != '') {
					data.listModels.forEach(item => {
						const itemReturn: PoComboOption = {
							label: item.descModel + ' - ' + item.codModel,
							value: item.codModel,
						};

						listItensReturn.push(itemReturn);
					});
				}
				return listItensReturn;
			})
		);
	}

	getObjectByValue(value: string): Observable<PoComboOption> {
		this.jurconsultas.restore();
		this.jurconsultas.setParam('searchKey', value);

		return this.jurconsultas.get('getTemplate', 'getTemplateAdapter').pipe(
			map((data: any) => {
				const itemReturn: PoComboOption = { label: '', value: '' };
				if (this.JuriGenerics.changeUndefinedToEmpty(data.listModels) != '') {
					data.listModels.forEach(item => {
						itemReturn.label = item.descModel + ' - ' + item.codModel;
						itemReturn.value = item.codModel;
					});
				}

				return itemReturn;
			})
		);
	}
}

/**
 * Estrutura de Retorno do Adaptor. Fixado para não haver manipulação ou inclusão de novos elementos
 * Sem que o desenvolvedor tenha de alterar aqui.
 * @
 * Ao alterar essa classe, verificar os locais aonde é utilizado o adaptor.
 */
export class adaptorReturnStruct implements PoComboOption {
	value: string;
	label: string;
}
