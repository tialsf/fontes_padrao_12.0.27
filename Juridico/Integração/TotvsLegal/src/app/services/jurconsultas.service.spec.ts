import { TestBed } from '@angular/core/testing';

import { JurconsultasService } from './jurconsultas.service';

describe('JurconsultasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JurconsultasService = TestBed.get(JurconsultasService);
    expect(service).toBeTruthy();
  });
});
