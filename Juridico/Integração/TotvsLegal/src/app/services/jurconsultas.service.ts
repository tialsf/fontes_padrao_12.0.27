import { Injectable } from '@angular/core';
import { protheusService } from './protheus.service';
import { PoDynamicFormField, PoSelectOption } from '@po-ui/ng-components';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { JurigenericsService } from './jurigenerics.service';

const endpoint = 'JURCONSULTAS/';

export interface ExtraField extends PoDynamicFormField {
	agrupamento: string;
}

@Injectable({
	providedIn: 'root',
})
export class JurconsultasService {
	constructor(
		private protheusService: protheusService,
		private juriGenerics: JurigenericsService
	) {}

	/**
	 * Define o valor do QueryParam passado
	 * @param cNameParam - QueryParam a ser definido
	 * @param value - Valor a ser inserido no QueryParam
	 */
	setParam(cNameParam: string, value: string) {
		this.protheusService.setOptionsParams(cNameParam, value);
	}

	/**
	 * Restaura o serviço Protheus
	 */

	restore() {
		this.protheusService.restore();
	}

	/**
	 * Realiza a requisição HTTP GET
	 * @param pathUrl - Path a ser requisitado
	 * @param nameOperation - Nome da operação
	 */
	get(pathUrl: string, nameOperation: string = 'getJurConsultas') {
		return this.protheusService.get(endpoint + pathUrl, nameOperation);
	}

	/**
	 * Realiza a busca dos campos adicionais que sejam customizados ou obrigatórios que sejam diferentes da lista passada
	 * @param listIgnoredFields - Lista de campos que deverão ser ignorados na busca
	 * @param alias - Alias a ser buscado
	 * @param assJur - Assunto jurídico a ser buscado
	 */
	getExtraFields(
		listIgnoredFields: Array<string> = [],
		alias: string = 'NSZ',
		assJur: string = '001'
	): Observable<Array<ExtraField>> {
		this.protheusService.restore();
		this.protheusService.setOptionsParams('searchKey', alias);
		this.protheusService.setOptionsParams('assJur', assJur);

		return this.protheusService
			.get(endpoint + 'extraFields', 'Obtem os campos adicionais')
			.pipe(
				map(data => {
					const listFields: Array<ExtraField> = [];
					if (this.juriGenerics.changeUndefinedToEmpty(data.campos) !== '') {
						data.campos.forEach(item => {
							const isNotFieldDet: boolean =
								listIgnoredFields.findIndex(x => x === item.campo) === -1;

							if (isNotFieldDet) {
								let cType: string;
								let listOptions: Array<PoSelectOption>;
								let booleanTrue: string;
								let booleanFalse: string;

								switch (item.tipo) {
									case 'N':
										cType = 'currency';
										break;
									case 'D':
										cType = 'date';
										break;
									case 'CB':
										listOptions = item.opcoes;
										break;
									case 'L':
										cType = 'boolean';
										booleanTrue = 'sim';
										booleanFalse = 'nao';
										break;
								}

								listFields.push({
									property: item.campo,
									label: item.titulo,
									type: cType,
									options: listOptions,
									gridColumns: 4,
									gridSmColumns: 12,
									booleanTrue,
									booleanFalse,
									agrupamento: item.agrupamento,
								});
							}
						});
					}
					return listFields;
				})
			);
	}
}
