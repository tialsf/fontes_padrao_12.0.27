/**
* Arquivo de constantes para tradução do service JurGenerics - Português
*/

//-- Constantes para os meses por extenso (getStrMonth)
export const mesesExtenso = {
	Janeiro: "Janeiro",
	Fevereiro: "Fevereiro",
	Marco: "Marco",
	Abril: "Abril",
	Maio: "Maio",
	Junho: "Junho",
	Julho: "Julho",
	Agosto: "Agosto",
	Setembro: "Setembro",
	Outubro: "Outubro",
	Novembro: "Novembro",
	Dezembro: "Dezembro"
}

//-- Constantes para os meses abreviados (getStrMonth)
export const mesesAbreviados = {
	Jan: "Jan",
	Fev: "Fev",
	Mar: "Mar",
	Abr: "Abr",
	Mai: "Mai",
	Jun: "Jun",
	Jul: "Jul",
	Ago: "Ago",
	Set: "Set",
	Out: "Out",
	Nov: "Nov",
	Dez: "Dez"
}

//-- Constantes principais
export const jurigenericsPt = {
	meses: mesesExtenso,
	mesesA: mesesAbreviados
}
