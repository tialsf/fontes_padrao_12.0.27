import { TestBed } from '@angular/core/testing';

import { JurigenericsService } from './jurigenerics.service';

describe('JurigenericsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JurigenericsService = TestBed.get(JurigenericsService);
    expect(service).toBeTruthy();
  });
});
