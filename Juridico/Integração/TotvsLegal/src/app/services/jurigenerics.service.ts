import { Injectable } from '@angular/core';
import { PoI18nService } from '@po-ui/ng-components';

@Injectable({
	providedIn: 'root'
})
export class JurigenericsService {

	litJurGenerics: any;
	arrMonthAbrevi  = ["Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set","Out","Nov","Dez"];
	arrMonthExtenso = ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"];
	
	/**
	 * constante para definir qual será o separador utilizado na concatenação dos campos
	 */
	separador:string = "<separador>";
	
	constructor(protected thfI18nService: PoI18nService) {

		//-- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'jurigenerics' no arquivo JurTraducao.			
		thfI18nService.getLiterals( { context: 'jurigenerics', language: thfI18nService.getLanguage() } )
		.subscribe((litJurGenerics) => {
			this.litJurGenerics = litJurGenerics;

			// Seta as constantes para a tradução dos meses do gráfico de forma abreviada
			this.arrMonthAbrevi =  [this.litJurGenerics.mesesA.Jan,
									this.litJurGenerics.mesesA.Fev,
									this.litJurGenerics.mesesA.Mar,
									this.litJurGenerics.mesesA.Abr,
									this.litJurGenerics.mesesA.Mai,
									this.litJurGenerics.mesesA.Jun,
									this.litJurGenerics.mesesA.Jul,
									this.litJurGenerics.mesesA.Ago,
									this.litJurGenerics.mesesA.Set,
									this.litJurGenerics.mesesA.Out,
									this.litJurGenerics.mesesA.Nov,
									this.litJurGenerics.mesesA.Dez];

			// Seta as constantes para a tradução dos meses do gráfico de forma extensa				
			this.arrMonthExtenso = [this.litJurGenerics.meses.Janeiro,
									this.litJurGenerics.meses.Fevereiro,
									this.litJurGenerics.meses.Marco,
									this.litJurGenerics.meses.Abril,
									this.litJurGenerics.meses.Maio,
									this.litJurGenerics.meses.Junho,
									this.litJurGenerics.meses.Julho,
									this.litJurGenerics.meses.Agosto,
									this.litJurGenerics.meses.Setembro,
									this.litJurGenerics.meses.Outubro,
									this.litJurGenerics.meses.Novembro,
									this.litJurGenerics.meses.Dezembro];

		});						
	}

	/** Função para subtrair as datas e retornar um valor numérico
	* O retorno independe da ordem de envio, sempre retorna um valor positivo
	* @param dateIni - Data a ser subtraida
	* @param dateFim - Se estiver indefinida, será o dia de hoje
	* 
	* @example subtractDates("20190101","20190129") - Return será 28
	*  subtractDates("20190129","20190101") - Return será 28
	*/
	subtractDates(dateIni: Date, dateFim?:Date):number{
		var qtdDiasDiff: number;
		var dataInicial: string;
		var dataFinal: string;
		
		if (dateFim == undefined){
			dateFim = new Date();
		}
		
		// Tratamento da data inicial
		dataInicial = dateIni.getFullYear().toString();
		dataInicial += this.buildMonthDayStr(dateIni.getMonth().toString());
		dataInicial += this.buildMonthDayStr(dateIni.getDate().toString());
		
		// Tratamento da data Final
		dataFinal = dateFim.getFullYear().toString();
		dataFinal += this.buildMonthDayStr(dateFim.getMonth().toString());
		dataFinal += this.buildMonthDayStr(dateFim.getDate().toString());
		
		// Subtração utilizando o abs. O Abs transforma o resultado em positivo.
		qtdDiasDiff = Math.abs(parseInt(dataInicial)-(parseInt(dataFinal)));
		return qtdDiasDiff;
	}
	
	/** Inclui um 0 no começo. 
	* @param monthDay - String do Dia/Mês para incluir o "0";
	* 
	* @example monthDay = "1", return será "01";
	* monthDay = "10", return será "10";
	*/
	buildMonthDayStr(monthDay: String){
		var strMonthDay: String;
		strMonthDay = "0" + monthDay;
		
		return strMonthDay.substr(strMonthDay.length-2)
	}
	
	/**
	 * Busca a descrição do mês
	 * @param strNumMonth - Numero do mês (1-12)
	 * @param bAbrevi - Verifica se é a forma abreviada ou não
	 */
	getStrMonth(strNumMonth: string, bAbrevi?: boolean){
		var arrMonth: Array<string>;
		var strMonth: string;
		
		if (bAbrevi == undefined){
			bAbrevi = false;
		}
		
		if (bAbrevi){
			arrMonth = this.arrMonthAbrevi;
		} else {
			arrMonth = this.arrMonthExtenso;
		}
		
		strMonth = arrMonth[parseInt(strNumMonth)-1];
		
		return strMonth;
	}

	/** Monta a data no formato mm-dd-yyyy para utilizar como Data
	* @param strDate - Recebe a data no formato Protheus (yyyymmdd)
	* @param formato - Formato da mascara. No default será considerado o formato ("mm-dd-yyyy"). Padrão do JavaScript.
	* @param separador - Separador a ser trocado da mascara, caso seja diferente do que está na mascara 
	* 
	* @example Para utiizar a data "20190129" provinda do Protheus a data precisa estar no formato mm-dd-yyyy
	* makeDate("20190129")  = 01-29-2019
	* makeDate("20190129","dd/mm/yyyy")  = 29/01/2019
	* makeDate("20190129",,"_")  = 01_29_2019
	* 
	*/
	makeDate(strDate: string, formato?: string, separador?: string): string{
		var dataFinal: string;
		var dia, mes, ano: string;
		var strMes: string;
		
		if (this.changeUndefinedToEmpty(strDate) != ""){
			if (separador == undefined){
				separador = "-";
			}
			
			if (formato == undefined){
				formato = "mm-dd-yyyy"
			}
			
			dia = strDate.substring(6,8);
			mes = strDate.substring(4,6);
			ano = strDate.substring(0,4);
			
			if (formato.indexOf("MMMM") > -1 ) {
				strMes = this.getStrMonth(mes);
				formato = formato.replace("MMMM","MM");
			} else if (formato.indexOf("MM") > -1){
				strMes = this.getStrMonth(mes,true);
			}
			
			dataFinal = formato.replace(/dd/g,dia).replace(/MM/g,strMes).replace(/mm/g,mes).replace(/yyyy/g,ano).replace(/-/g,separador);
		}else {
			dataFinal = ""
		}
		
		return dataFinal
	}
	
	/**
	 * Transforma um new Date(): Tue Jul 30 2019 11:17:52 GMT-0300
	 * em formato Protheus "20190730"
	 */
	makeDateProtheus() {
		var hoje: any;
		hoje = new Date();
		var dd = String(hoje.getDate()).padStart(2, '0');
		var mm = String(hoje.getMonth() + 1).padStart(2, '0'); //Janeiro = 0!
		var yyyy = hoje.getFullYear();

		hoje = yyyy + mm + dd;

		return hoje
	}
	
	/**
	 * Função utilizada para adicionar ou subtrair dias, meses ou anos de uma data
	 * 
	 * @param date        - Data que será feito o cálculo (esperado no formato DD/MM/YYYY)
	 * @param tipoAddData - Tipo que será cálculado, 'D' = Dia, 'M' = Mês ou 'Y'|'A' = Ano
	 * @param nQtd        - Define a quantidade a ser adicionada ou subtraída
	 */
	jurAddDate( dDate ?: string , tipoAddData: string = 'D', nQtd: number = 0 ) {
		let nDia: number;
		let nMes: number;
		let nAno: number;
		
		if (this.changeUndefinedToEmpty(dDate) == '') {
			let dataAtual: Date = new Date();
			dDate = dataAtual.toLocaleDateString('pt-BR');
		}
		
		// Formato de Data esperado DD/MM/YYYY
		nDia = parseInt(dDate.substring(0,2));
		nMes = parseInt(dDate.substring(3,5));
		nAno = parseInt(dDate.substring(6));

		switch (tipoAddData.toUpperCase()) {
			case 'D':
				break;
			case 'M':
				let nI: number = 0
				for (nI; nI < Math.abs(nQtd); nI++) {
					if (nQtd > 0) {
						nMes++;
					} else {
						nMes--;
					}

					if (nMes == 13) {
						nMes = 1;
						nAno++;
					}
					if (nMes == 0) {
						nMes = 12;
						nAno--;
					}
				}
				break;
			case 'Y' || 'A':
				nAno = nAno + nQtd;
				break;
		}

		return nDia.toString().padStart(2, '0') + nMes.toString().padStart(2, '0') + nAno.toString().padStart(4, '0');
	}

	/**
	 * Busca os dados do Submodelo. Podendo trazer somente o array de Itens ou retornar toda a Estrutura do modelo passado
	 * @param aDados - Array de dados até a parte em que define o modelo
	 * @param cNomeModelo - Nome do Modelo a ser retornado
	 * @param bGetOnlyItens - Verifica se tem de retornar somente os itens. Caso não, irá retornar a Estrutura e o Tipo do Modelo. Default: True.
	 * @example
	 * ```
	 * aDados[0].id = "NTEDETAIL"
	 * aDados[0].modeltype = "GRID"
	 * aDados[0].struct = [Array de campos do modelo]
	 * aDados[0].items = [Array de linhas do Grid]
	 * 
	 * findModelItensByName(aDados, "NTEDETAIL", true) // Irá retornar os `items` em formato de Array
	 * findModelItensByName(aDados, "NTEDETAIL", false) // Irá retornar todo o objeto com `id`, `modeltype`,`struct` e `items`. Tambem como array
	 * findModelItensByName(aDados, "NZKDETAIL") // Como em `aDados` não tem o modelo especificado, irá retornar `undefined`
	 * ```
	 * 
	 * @returns `Array com os dados` ou `Undefined`
	 */
	findModelItensByName(aDados: Array<any>, cNomeModelo: string, bGetOnlyItens: boolean = true): Array<any> | undefined{
		let aModel = aDados.find( x=> x.id == cNomeModelo)

		if (aModel != undefined){
			if (bGetOnlyItens){
				return aModel.items
			} else {
				return aModel
			}
		}
	}
	
	/**
	 * Busca dados a partir de um array. Se não encontrar, retorna `undefined` 
	 * @param aDados - Array de dados
	 * @param cCampo - Campo a ser encontrado
	 * @example 
	 * ```
	 * aDados[0].id = "NSZ_COD"
	 * aDados[0].order = 1
	 * aDados[0].value = "0000000111"
	 * aDados[1].id = "NSZ_TIPOAS"
	 * aDados[1].order = 2
	 * aDados[1].value = "001"
	 * 
	 * this.findValueByName(aDados, "NSZ_COD") // Retorna "0000000111"
	 * this.findValueByName(aDados, "NSZ_TIPOAS") // Retorna "001"
	 * ```
	 */
	findValueByName(aDados: Array<any>, cCampo: string){
		var aEncontrado = aDados.find(x => x.id == cCampo);

		if (aEncontrado != undefined){
			if (typeof(aEncontrado.value) == 'string'){
				return this.decodeHtmlEntities(aEncontrado.value);
			} else {				
				return aEncontrado.value;
			}
		}
	}

	/**
	 * converte a string codificada como entidade HTML (palavras reservadas do HMTL)
	 * em seus caracteres correspondentes 
	 * @param encodedString - string codificada como entidade HTML
	 */
	decodeHtmlEntities(encodedString) {
		let decodedHtmlEntities: string = encodedString;
		decodedHtmlEntities = (decodedHtmlEntities).replace(/&quot;/gm,'"');
		decodedHtmlEntities = (decodedHtmlEntities).replace(/&amp;/gm ,'&');
		decodedHtmlEntities = (decodedHtmlEntities).replace(/&#39;/gm ,"'");
		decodedHtmlEntities = (decodedHtmlEntities).replace(/&gt;/gm  ,'>');
		decodedHtmlEntities = (decodedHtmlEntities).replace(/&lt;/gm  ,'<');
		decodedHtmlEntities = (decodedHtmlEntities).replace(/&#60;/gm ,'<');
		decodedHtmlEntities = (decodedHtmlEntities).replace(/&#62;/gm ,'>');
		decodedHtmlEntities = (decodedHtmlEntities).replace(/&#34;/gm ,'"');

		return decodedHtmlEntities
	}

	/**
	 * Busca dados a partir de um Array. Concatena campos
	 * @param aDados - Array de dados. 
	 * @param cCampos - Campos que deseja retornar. Separados por ","
	 * @param cSeparator - Separador para as colunas
	 * @param cFormato - Opcional - Formato a ser retornado
	 * @example 
	 * ```
	 * aDados[0].id = "NSZ_COD"
	 * aDados[0].order = 1
	 * aDados[0].value = "0000000111"
	 * aDados[1].id = "NSZ_TIPOAS"
	 * aDados[1].order = 2
	 * aDados[1].value = "001"
	 * 
	 * this.findValueByName(aDados, "NSZ_COD,NSZ_TIPOAS","-","[NSZ_COD] - NSZ_TIPOAS") // Retorna "[0000000111] - 001"
	 * this.findValueByName(aDados, "NSZ_COD,NSZ_TIPOAS","-")                          // Retorna "0000000111-001"
	 * ```
	 */
	findConcatValueByMultiNames(aDados: Array<any>, cCampos: string, cSeparator: string = this.separador, cFormato?: string): string{
		let valorRetorno: String = ""
		let aCampos: string[] = cCampos.split(",")

		// Verifica se há formato já definido
		if (cFormato != undefined && cFormato != ""){
			valorRetorno = cFormato;
		}

		aCampos.forEach(campo=>{
			let valor = this.findValueByName(aDados,campo);

			if (valor == undefined){
				valor = '';
			}

			if (valor != undefined){
				// Troca o valor do campo pelo valor retornado
				if (cFormato != undefined && cFormato != ""){
					valorRetorno = valorRetorno.replace(campo,valor)
				} else {
					// Sem o formato, concatena usando o separador definido
					valorRetorno += this.findValueByName(aDados,campo);
					valorRetorno += cSeparator
				}
			}
		})

		// Remova caracteres a mais por conta do Separador, se utilizou o separador
		if (cFormato != undefined && cFormato != ""){
			return valorRetorno.substring(0,valorRetorno.length);
		} else {
			return valorRetorno.substring(0,valorRetorno.length-cSeparator.length);
		}
	}

	/**
	 * Monta o request
	 */
	getRequestAuth(){
		return this.changeUndefinedToEmpty(window.localStorage.getItem('TLAuthType')) + ' ' + this.changeUndefinedToEmpty(window.localStorage.getItem('TLToken'))
	}

	/**
	 * Limpa o storage
	 */
	clearStorage(){
		let userConfig = window.localStorage.getItem("userConfig")
		
		window.sessionStorage.clear();
		window.localStorage.clear()

		if(userConfig != null){
			window.localStorage.setItem("userConfig",userConfig)
		}
	}

	/**
	 * Altera o valor `undefined` para vazio
	 * @param value - Valor a ser validado
	 * @returns O valor passado ou vazio `("")`
	 */
	changeUndefinedToEmpty(value: string | undefined): string{
		if (value == undefined){
			value = "";
		}
		return value;
	}

	/**
	 * Altera o valor `undefined` para `0`
	 * @param value - Valor a ser validado
	 * @returns O valor passado ou vazio `(0)`
	 */
	changeUndefinedToZero(value: number | undefined): number{
		if (value == undefined){
			value = 0;
		}

		if (typeof(value) == "string"){
			value = parseFloat(value)
		}
		return value;
	}
	/**
	 *  ordena a lista pela data de forma descrescente 
	 */
	orderByDate(list: Array<any>, nomeColuna: string) {
		
		list.sort((a, b) => { 
			if (a[nomeColuna].toString() < b[nomeColuna].toString() ) return 1;
			else if (a[nomeColuna].toString()  > b[nomeColuna].toString() ) return -1;
			else return 0;
		});
		return list;
	}	

	/**
	* Retorna a propriedade de valor de um campo extra, de um determinado combo
	* @param comboComponent - Componente combo
	* @param field - string com campo
	* @returns valor do campo extra
	*/
	getValueByOption(comboComponent, field){
		let retorno: string = "";

		if (comboComponent.selectedOption == undefined || field == undefined ) {
			retorno = "";
		} else {
			retorno = this.changeUndefinedToEmpty( comboComponent.selectedOption.campos.find(element => element.id == field).value )
		}
		return retorno
	}
}

/**
* Enum para a requisição do Authentication
*/
export enum GrantTypeEnum{
	password = "password",
	refresh = "refresh_token"
}

/**
 * Classe de Encode e Decode UTF-8
 */
export class JurUTF8{
	/**
	 * Encoda a string passada para UTF-8
	 * @param decodedString - String a ser Encodada
	 */
	static encode(decodedString) {
		return decodedString;
	}

	/**
	 * Decoda a string passada de UTF-8
	 * @param encodedString - String a ser Decodada
	 */
	static decode(encodedString) {
		for (var a, b, i = -1, l = (encodedString = encodedString.split("")).length, o = String.fromCharCode, c = "charCodeAt"; ++i < l;
			((a = encodedString[i][c](0)) & 0x80) &&
			(encodedString[i] = (a & 0xfc) == 0xc0 && ((b = encodedString[i + 1][c](0)) & 0xc0) == 0x80 ?
				o(((a & 0x03) << 6) + (b & 0x3f)) : o(128), encodedString[++i] = "")
		);

		return encodedString.join("");
	}
}
/**
 * Classe de conversão 
 */
export class JurBase64{

	/**
	 * converte o arquivo de base64 pra utf8
	 * @param base64Data conteúdo em base64 a ser convertido
	 * @param contentType tipo do conteúdo a ser setado para o blob
	 */
	static toUTF8(base64Data, contentType) {
		contentType = contentType || '';
		var sliceSize = 1024;
		var byteCharacters = atob(base64Data);
		var bytesLength = byteCharacters.length;
		var slicesCount = Math.ceil(bytesLength / sliceSize);
		var byteArrays = new Array(slicesCount);
	
		for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
			var begin = sliceIndex * sliceSize;
			var end = Math.min(begin + sliceSize, bytesLength);
	
			var bytes = new Array(end - begin);
			for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
				bytes[i] = byteCharacters[offset].charCodeAt(0);
			}
			byteArrays[sliceIndex] = new Uint8Array(bytes);
		}
		return new Blob(byteArrays, { type: contentType });
	}

}

