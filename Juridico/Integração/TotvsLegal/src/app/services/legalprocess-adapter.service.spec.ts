import { TestBed } from '@angular/core/testing';

import { lpGetFuncionarioAdapterService } from './legalprocess-adapter.service';

describe('LegalprocessAdapterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: lpGetFuncionarioAdapterService = TestBed.get(lpGetFuncionarioAdapterService);
    expect(service).toBeTruthy();
  });
});
