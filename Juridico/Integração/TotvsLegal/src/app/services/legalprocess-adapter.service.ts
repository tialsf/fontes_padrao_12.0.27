import { Injectable } from '@angular/core';
import { LegalprocessService } from './legalprocess.service';
import { PoComboFilter, PoComboOption } from '@po-ui/ng-components';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { JurigenericsService } from './jurigenerics.service';

@Injectable({
	providedIn: 'root'
})
export class lpGetFuncionarioAdapterService implements PoComboFilter{
	constructor(
		private legalprocess: LegalprocessService,
		private juriGenerics: JurigenericsService
	) { }

	getFilteredData(valor): Observable<Array<PoComboOption>>{
		this.legalprocess.restore();

		if (this.juriGenerics.changeUndefinedToEmpty(valor.value) != ""){
			this.legalprocess.setNomeFunc(valor.value);
		}
		
		this.legalprocess.setPageSize('10');

		return this.legalprocess.get("funcionarios","getFuncionarios").pipe(map(
			(data: any) => {
				
				let listItensReturn: lpFuncionarioReturnStruct[] = [];
				if (data.funcionarios != '') {
					data.funcionarios.forEach(item => {
						let itemReturn = new lpFuncionarioReturnStruct();
						itemReturn.label = item.nome
						itemReturn.value = item.filial.toString() + item.matricula.toString()
						itemReturn.cgc   = item.cpf

						listItensReturn.push(itemReturn);
					});
				}
				return listItensReturn;
			}
		))
	}

	getObjectByValue(valor: string): Observable<PoComboOption>{
		this.legalprocess.restore();
		this.legalprocess.setSearchKey(valor);
		this.legalprocess.setPageSize('1');

		return this.legalprocess.get("funcionarios", "getLegalProcess").pipe(map(
			(data: any) => {

				let itemReturn = new lpFuncionarioReturnStruct();
				if (data.funcionarios != ''  && data.funcionarios != undefined) {
					data.funcionarios.forEach(item => {
						itemReturn.label = item.nome
						itemReturn.value = item.filial.toString() + item.matricula.toString()
						itemReturn.cgc   = item.cpf
					});
				}
			 	return itemReturn;
			}
		))
	}

}

@Injectable({
	providedIn: 'root'
})
/**
 *  Busca a lista de filiais que o usuário possui acesso
 */
export class lpGetFiliaisAdapterService implements PoComboFilter{
	constructor(
		private legalprocess: LegalprocessService,
		private juriGenerics: JurigenericsService
	) { }

	/**
 	*  Busca a lista de filiais que o usuário possui acesso de acordo com o conteúdo digitado
 	*/
	getFilteredData(valor): Observable<Array<PoComboOption>>{
		this.legalprocess.restore();

		return this.legalprocess.get("listFiliaisUser","Lista de Filial").pipe(map(
			(data: any) => {
				
				let listItensReturn: defaultReturnStruct[] = [];
				if (data.User != '') {
					data.User.forEach(filial => {
						let filiais = new defaultReturnStruct();
						filiais.value = filial.value
						filiais.label = filial.label

						listItensReturn.push(filiais);
					});
				}
				return listItensReturn;
			}
		))
	}

	/**
 	*  Busca a filial selecionada de aocrdo com o código
 	*/
	getObjectByValue(valor: string): Observable<defaultReturnStruct>{
		this.legalprocess.restore();
		this.legalprocess.setFilter('codFil',valor);
		let filiais = new defaultReturnStruct();

		return this.legalprocess.get("listFiliaisUser", "listFiliaisUser").pipe(map(
			(data: any) => {
				if (this.juriGenerics.changeUndefinedToEmpty(data.User) != '') {
					data.User.forEach(filial => {
						filiais.value = filial.value
						filiais.label = filial.label
					});
				}
			 	return filiais;
			}
		))
	}

}

/**
 *  Busca a lista de filiais que o usuário possui acesso
 */
@Injectable({
	providedIn: 'root'
})
export class lpGetForoAdapterService implements PoComboFilter {
	codComarca: string = '';

	constructor(
		private legalprocess: LegalprocessService,
		private juriGenerics: JurigenericsService
	) { }

	/**
	 * Informa a Comarca para buscar o foro
	 * @param comarca - Informa a comarca.
	 */
	setComarca(comarca: string){
		this.codComarca = comarca;
	}

	/**
 	*  Busca a lista de filiais que o usuário possui acesso de acordo com o conteúdo digitado
 	*/
	getFilteredData(valor): Observable<Array<PoComboOption>>{
		this.legalprocess.restore();
		
		if (this.juriGenerics.changeUndefinedToEmpty(valor.value) != ""){
			this.legalprocess.setSearchKey(valor.value);
		}

		return this.legalprocess.get('comarca/' + this.codComarca + '/foros', 'getForos').pipe(map(
			(data: any) => {
				
				let listItensReturn: defaultReturnStruct[] = [];
				if (data.foro != '') {
					data.foro.forEach(item => {
						let foro = new defaultReturnStruct();
						foro.value = item.value
						foro.label = item.label

						listItensReturn.push(foro);
					});
				}
				return listItensReturn;
			}
		))
	}

	/**
 	*  Busca a filial selecionada de acordo com o código
 	*/
	getObjectByValue(valor: string): Observable<PoComboOption>{
		this.legalprocess.restore();
		this.legalprocess.setBuscaInfo(valor);
		let itemForo = new defaultReturnStruct();

		return this.legalprocess.get('comarca/' + this.codComarca + '/foros', 'getForos').pipe(map(
			(data: any) => {
				if (data.foro != '' && data.foro != undefined) {
					data.foro.forEach(item => {
						itemForo.value = item.value
						itemForo.label = item.label
					});
				}
				return itemForo;
			}
		))
	}
}

/**
 *  Busca a lista de filiais que o usuário possui acesso
 */
@Injectable({
	providedIn: 'root'
})
export class lpGetVaraAdapterService implements PoComboFilter {
	codComarca: string = '';
	codForo: string    = '';

	constructor(
		private legalprocess: LegalprocessService,
		private juriGenerics: JurigenericsService
	) { }
	
	/**
	 * Informa a Comarca para buscar o foro
	 * @param comarca - Informa a comarca.
	 */
	setComarca(comarca: string){
		this.codComarca = comarca;
	}

	/**
	 * Informa o foro para buscar a vara.
	 * @param foro - Informa o foro.
	 */
	SetForo(foro: string) {
		this.codForo = foro;
	}

	/**
 	*  Busca a lista de filiais que o usuário possui acesso de acordo com o conteúdo digitado
 	*/
	getFilteredData(valor): Observable<Array<PoComboOption>>{
		this.legalprocess.restore();
		
		if (this.juriGenerics.changeUndefinedToEmpty(valor.value) != ""){
			this.legalprocess.setSearchKey(valor.value);
		}

		return this.legalprocess.get('comarca/' + this.codComarca + '/foros/' + this.codForo + '/varas', 'getVaras').pipe(map(
			(data: any) => {
				
				let listItensReturn: defaultReturnStruct[] = [];
				if (data.vara != '') {
					data.vara.forEach(item => {
						let vara = new defaultReturnStruct();
						vara.value = item.value
						vara.label = item.label

						listItensReturn.push(vara);
					});
				}
				return listItensReturn;
			}
		))
	}

	/**
 	*  Busca a filial selecionada de aocrdo com o código
 	*/
	getObjectByValue(valor: string): Observable<PoComboOption>{
		this.legalprocess.restore();
		this.legalprocess.setBuscaInfo(valor);
		let itemVara = new defaultReturnStruct();

		return this.legalprocess.get('comarca/' + this.codComarca + '/foros/' + this.codForo + '/varas', 'getVaras').pipe(map(
			(data: any) => {
				if (data.vara != '') {
					data.vara.forEach(item => {
						itemVara.value = item.value
						itemVara.label = item.label
					});
				}
				return itemVara;
			}
		))
	}
}

/**
 *  Busca a lista de filiais que o usuário possui acesso
 */
@Injectable({
	providedIn: 'root'
})
export class lpGetDistribuicaoAdapterService implements PoComboFilter{
	constructor(
		private legalprocess: LegalprocessService,
		private juriGenerics: JurigenericsService
	) { }

	getFilteredData(valor): Observable<Array<PoComboOption>>{
		this.legalprocess.restore();
		this.legalprocess.setPageSize("99");

		if (this.juriGenerics.changeUndefinedToEmpty(valor.value) != ""){
			this.legalprocess.setSearchKey(valor.value);
		}

		return this.legalprocess.get('distr/list/1','Lista de Distribuições').pipe(map(
			(data: any) => {
				
				let listItensReturn: lpDistrReturnStruct[] = [];
				if (data != '') {
					data.data[0].distribuicoes.forEach(item => {
						let itemReturn = new lpDistrReturnStruct();
						itemReturn.label = item.descricao;
						itemReturn.value = item.codigo;
						itemReturn.numpro= item.NZZ_NUMPRO;
						

						listItensReturn.push(itemReturn);
					});
				}
				return listItensReturn;
			}
		))
	}

	getObjectByValue(valor: string): Observable<PoComboOption>{
		this.legalprocess.restore();
		this.legalprocess.setSearchKey(valor);
		this.legalprocess.setPageSize('1');
		return this.legalprocess.get('distr/list/1','Lista de Distribuições').pipe(map(
			(data: any) => {

				let itemReturn = new lpDistrReturnStruct();
				if (data != ''  && data != undefined) {
					data.distribuicoes.forEach(item => {
						itemReturn.label = item.descricao;
						itemReturn.value = item.codigo;
						itemReturn.numpro= item.NZZ_NUMPRO;
					});
				}
			 	return itemReturn;
			}
		))
	}

}

/**
 * Estrutura de Retorno do Adaptor. Fixado para não haver manipulação ou inclusão de novos elementos
 * Sem que o desenvolvedor tenha de alterar aqui.
 * @
 * Ao alterar essa classe, verificar os locais aonde é utilizado o adaptor.
 */
export class defaultReturnStruct implements PoComboOption{
	value: string;
	label: string;
}

export class lpFuncionarioReturnStruct implements PoComboOption{
	value: string;
	label: string;
	cgc: string
}
export class lpDistrReturnStruct implements PoComboOption{
	value: string;
	label: string;
	numpro: string
}
