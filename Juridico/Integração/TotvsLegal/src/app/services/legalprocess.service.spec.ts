import { TestBed } from '@angular/core/testing';

import { LegalprocessService } from './legalprocess.service';

describe('LegalprocessService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LegalprocessService = TestBed.get(LegalprocessService);
    expect(service).toBeTruthy();
  });
});
