import { Injectable } from '@angular/core';
import { protheusService } from './protheus.service';
import { Observable, of, from, forkJoin, concat } from 'rxjs';
import { map, flatMap, concatMap, mergeMap, reduce } from 'rxjs/operators';

const endpoint = "JURLEGALPROCESS/";

@Injectable({
	providedIn: 'root'
})
export class LegalprocessService {

	cajuri: string = '';
	entidade: string = '';
	codDoc: string= '';

	// Construtor
	constructor(private protheusService: protheusService) {}

	/**
	 * Define o valor do QueryParam passado
	 * @param cNameParam - QueryParam a ser definido
	 * @param value - Valor a ser inserido no QueryParam
	 */
	setFilter(cNameParam: string, value: string){
		this.protheusService.setOptionsParams(cNameParam, value);
	}

	/**
	 * seta a ordenação da query
	 * 1 - Por valor de provisão (" ORDER BY NSZ.NSZ_VAPROV DESC ")
	 * 2 - " ORDER BY ULTAND.NT4_DTANDA DESC, NSZ.NSZ_VAPROV DESC "
	 * 3 - " ORDER BY NSZ.NSZ_COD ""
	 */
	setOrderProvision(queryOrder: string){
		this.protheusService.setOptionsParams("qryOrder", queryOrder);
	}

	// seta a quantidade de itens por página
	setPageSize(pagesize: string){
		this.protheusService.setOptionsParams("pageSize", pagesize);
	}

	// seta o número da página
	setPage(page: string){
		this.protheusService.setOptionsParams("page", page);
	}

	/**
	 * seta o filtro a ser aplicado na query
	 * 1 - Processos em andamento
	 * 2 - Processos Cadastrados no mês corrente
	 * 3 - Processos Encerrados no mês corrente
	 */
	setTypeFilter(tpFilter: string){
		this.protheusService.setOptionsParams("tpFilter", tpFilter);
	}

	// seta o conteúdo a ser pesquisado nos campos NT9_NOME/ NUQ_NUMPRO/ NSZ_DETALH
	setSearchKey(searchKey: string){
		this.protheusService.setOptionsParams("searchKey", searchKey);
	}

	// seta o conteúdo a ser filtrado no retorno das requisições.
	setBuscaInfo(buscaInfo: string){
		this.protheusService.setOptionsParams("buscaInfo", buscaInfo);
	}

	// seta o nome do funcionário para busca
	setNomeFunc(nomeFunc: string){
		this.protheusService.setOptionsParams("nomeFunc", nomeFunc);
	}

	// seta o parametro para mostrar o saldo da garantia
	setSaldoNT2(saldoNT2: string){
		this.protheusService.setOptionsParams("saldoNT2", saldoNT2);
	}

	// seta a quantidade de caracteres que serão exibidos no campo NT4_DESC
	setQtdCarac(qtdCarac: string){
		this.protheusService.setOptionsParams("qtdCarac", qtdCarac);
	}
	
	restore(){
		this.protheusService.restore();
	}

	// seta o conteúdo a ser pesquisado nos campos NT9_NOME/ NUQ_NUMPRO/ NSZ_DETALH
	setCodProc(CodProc: string){
		this.cajuri = CodProc;
	}

	// seta o conteúdo a ser pesquisado nos campos NT9_NOME/ NUQ_NUMPRO/ NSZ_DETALH
	setEntidade(entidade: string){
		this.protheusService.setOptionsParams("nomeEnt", entidade);
	}
	/**
	 * seta o valor do código na propriedade codDoc
	 * @param codDoc Código do documento referente à NUM
	 */
	setCodNUM(codDoc: string){
		this.codDoc = codDoc;
	}

	/**
	 * Seta o conteudo na propriedade de código da entidade
	 * @param codEntidade Informe o código da entidade que será buscada
	 */
	setCodEntidade(codEntidade: string){
		this.protheusService.setOptionsParams("codEntidade", codEntidade);
	}

	/**
	 * Função genérica para setar o conteudo da propriedade
	 * @param codParam Informe o parametro à ser setado
	 * @param param Informe o conteudo do parametro
	 */
	setParam(codParam: string, param: string) {
		this.protheusService.setOptionsParams(codParam, param);
	}

	/**
	 * Retorna os processos para a pesquisa rápida
	 */
	getSearchProcess() {
		return this.protheusService.get(endpoint + 'searchProcess', 'SearchProcess');
	}

	/**
	 * Retorna os andamentos de um processo
	 */
	getAndamentos(cajuri: string) {
		return this.protheusService.get(endpoint + 'process/' +cajuri+ '/andamentos', 'Lista de Andamentos');
	}

	/**
	 * Retorna os detalhes do processo para a tela de resumo
	 */
	getDetProcesso() {
		return this.protheusService.get(endpoint + 'tlprocess/detail/' +this.cajuri, 'Detalhe do Processo');
	}
	
	deleteDocs() {
		return this.protheusService.delete(endpoint + 'process/' + this.cajuri + '/docs/'+ this.codDoc , 'Exclusão do Documento')
	}

	downloadFileAttached() {
		return this.protheusService.get(endpoint + 'anexo/'+ this.codDoc, 'Download do Documento');
	}

	/**
	 * Realiza a requisição HTTP GET 
	 * @param pathUrl - Path a ser requisitado
	 * @param nameOperation - Nome da operação
	 */
	get(pathUrl: string,nameOperation: string = "getLegalProcess"){
		return this.protheusService.get(endpoint + pathUrl, nameOperation)
	}

	/**
	 * Realiza a requisição HTTP POST 
	 * @param pathUrl - Path a ser requisitado
	 * @param nameOperation - Nome da operação
	 */
	post(pathUrl: string,bodyReq: string,nameOperation: string = "postLegalProcess"){
		this.protheusService.setBody(bodyReq);
		return this.protheusService.post(endpoint + pathUrl, nameOperation)
	}

	/**
	 * Realiza a requisição HTTP DELETE
	 * @param pathUrl - Path a ser requisitado
	 * @param nameOperation - Operação
	 */
	delete(pathUrl: string, nameOperation: string = "deleteLegalProcess"){
		return this.protheusService.delete(endpoint + pathUrl, nameOperation);
	}

	/**
	 * Realiza a requisição HTTP PUT 
	 * @param pathUrl - Path a ser requisitado
	 * @param nameOperation - Nome da operação
	 */
	put(pathUrl:string, bodyReq: string, nameOperation: string = "putLegalProcess"){
		this.protheusService.setBody(bodyReq);
		return this.protheusService.put(endpoint + pathUrl, nameOperation);
	}

	async getAsync(pathUrl: string, nameOperation: string = "getAsyncLegalProcess"){
		return await this.protheusService.get(endpoint + pathUrl, nameOperation).toPromise()
	}
}

/**
 * Classe para consulta de parâmetros e funções para consulta
 */
@Injectable({
	providedIn: 'root'
})
export class SysProtheusParam {
	private sysParamEndPoint = "tlprocess/sysParam/"
	constructor(private legalprocess: LegalprocessService){}

	/**
	 * Busca os parâmetros, podendo recebe mais de um através de um Array com os nomes dos parâmetros
	 * @param arrParams - Array com o nome dos parâmetros.
	 *
	 * @returns Retorna um Array com os parâmetros na ordem passada. Se não encontrou, retorna o valor em branco
	 * @note Para utilizar, deve-se realizar um `Subcribe` e caso seja necessário algum processamento inicial da tela
	 * chamar as funções dentro do `Subscribe`
	 * @example
	 * ```
	 * this.SysProtheusParam.getParamByArray(["MV_JTVENJF", "MV_JAJUENC","MV_JDOCUME"]).subscribe(console.log)
	 * --- Console ---
	 * 0: sysParamStruct {name: "MV_JTVENJF", value: "1"}
	 * 1: sysParamStruct {name: "MV_JDOCUME", value: "2"}
	 * 2: sysParamStruct {name: "MV_JAJUENC", value: " "}
	 * ---------------
	 * ```
	 */
	getParamByArray(arrParams: Array<string>): Observable<sysParamStruct[]>{
		// O from transforma o array em Observable
		return from(arrParams).pipe(
			// O flatMap segura o retorno para somente retornar após passar todos os itens
			flatMap((value: any) => {
				return this.getParamValue(value).pipe(map(
					value=> [value]
				));
			}),
			// Concate as respostas 
			// Acc - Array atual | response - Item a ser adicionado
			reduce((acc, response) => {
				return acc.concat(response)
			})
		)
	}

	/**
	 * Busca o parâmetro passado. Se não encontrar, irá informar o resultado como Branco ('')
	 * @param nomeParam - Nome do Parâmetro a ser pesquisado
	 * 
	 * @returns `string` - Retorna o valor do parâmetro
	 */
	getParamValue(nomeParam: string): Observable<sysParamStruct>{
		this.legalprocess.restore()
		return this.legalprocess.get(this.sysParamEndPoint + nomeParam).pipe(
			map((result)=>{
					let paramValue: sysParamStruct = new sysParamStruct();
					paramValue.name = result.sysParam.name
					paramValue.value = result.sysParam.value
					return paramValue
				}
			)
		)
	}

	/**
	 * Busca o valor do Parâmetro dentro do Array de Parâmetros
	 * @param arrParams - Array com os parâmetros no formato `sysParamStruct`.
	 * @param nomeParam - Nome do Parâmetro
	 * 
	 * @returns - Valor do Parâmetro solicitado. Se não encontrado, retorna `undefined`.
	 */
	getParamValueByName(arrParams: sysParamStruct[], nomeParam: string): string | undefined{
		let valor: sysParamStruct = arrParams.find(x => x.name == nomeParam )
		let retorno: string = undefined

		if (valor != undefined){
			retorno = valor.value
		}

		return retorno
	}
}

export class sysParamStruct{
	name: string;
	value: string;
}