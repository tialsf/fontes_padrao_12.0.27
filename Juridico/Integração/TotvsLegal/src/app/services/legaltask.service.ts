import { Injectable } from '@angular/core';
import { protheusService } from './protheus.service';

const endpoint = "JURFWREST/";

@Injectable({
	providedIn: 'root'
})
export class LegaltaskService {

	// Construtor
	constructor(private protheusService: protheusService) {}

	/**
	 * Define o valor do QueryParam passado
	 * @param cNameParam - QueryParam a ser definido
	 * @param value - Valor a ser inserido no QueryParam
	 */
	setFilter(cNameParam: string, value: string){
		this.protheusService.setOptionsParams(cNameParam, value);
	}

	/**
	 * Restaura os valores iniciais
	 */
	restore(){
		this.protheusService.restore();
	}

	/**
	 * Realiza a requisição HTTP GET 
	 * @param pathUrl - Path a ser requisitado
	 * @param nameOperation - Nome da operação
	 */
	get(pathUrl: string,nameOperation: string = "getLegaltask"){
		return this.protheusService.get(endpoint + pathUrl, nameOperation)
	}

	/**
	 * Realiza a requisição HTTP POST 
	 * @param pathUrl - Path a ser requisitado
	 * @param nameOperation - Nome da operação
	 */
	post(pathUrl: string,bodyReq: string,nameOperation: string = "postLegaltask"){
		this.protheusService.setBody(bodyReq);
		return this.protheusService.post(endpoint + pathUrl, nameOperation)
	}

	/**
	 * Realiza a requisição HTTP DELETE
	 * @param pathUrl - Path a ser requisitado
	 * @param nameOperation - Operação
	 */
	delete(pathUrl: string, nameOperation: string = "deleteLegaltask"){
		return this.protheusService.delete(endpoint + pathUrl, nameOperation);
	}

	/**
	 * Realiza a requisição HTTP PUT 
	 * @param pathUrl - Path a ser requisitado
	 * @param nameOperation - Nome da operação
	 */
	put(pathUrl:string, bodyReq: string, nameOperation: string = "putLegaltask"){
		this.protheusService.setBody(bodyReq);
		return this.protheusService.put(endpoint + pathUrl, nameOperation);
	}

	async getAsync(pathUrl: string, nameOperation: string = "getAsyncLegaltask"){
		return await this.protheusService.get(endpoint + pathUrl, nameOperation).toPromise()
	}

}
