import { Injectable, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject, interval, Subscription } from 'rxjs';
import { FwmodelService, FwModelBody } from './fwmodel.service';
import { JurigenericsService, JurBase64 } from './jurigenerics.service';
import { TLUserInfo } from './authlogin.service';
import { LegalprocessService } from './legalprocess.service';
import { Router } from '@angular/router';
import { NotificationAction, ActionType } from '../shared/structs/notification-action.interface';

@Injectable({
	providedIn: 'root',
})
export class NotificationService implements OnDestroy {
	protected codUser: string = '';

	subListNotify: BehaviorSubject<Array<NotificationAction>> = new BehaviorSubject([]);
	intervalList: Observable<number> = interval(10000);
	subscription: Subscription;

	constructor(
		protected fwModel: FwmodelService,
		protected juriGenerics: JurigenericsService,
		protected infoUser: TLUserInfo,
		protected legalProcess: LegalprocessService,
		private router: Router
	) {}

	/**
	 * Método responsável pela destruição do behaviourSubject
	 */
	ngOnDestroy(): void {
		this.subscription.unsubscribe();
		this.subListNotify.next([]);
		this.subListNotify.unsubscribe();
	}

	/**
	 * Função responsavel pela inicialziação dos intervalos de busca de notificações
	 */
	start() {
		this.subscription = this.intervalList.subscribe(x => this.getNotifications());
	}

	/**
	 * Função responsavel pela busca e preenchimento da variavel de lista de notificações
	 * @param codUser Código do usuário para realizar a busca de notificações
	 */
	getNotifications(): void {
		this.codUser = this.infoUser.getCodUsuario();

		if (this.codUser === '') {
			setTimeout(x => this.getNotifications(), 500);
		} else {
			this.fwModel.restore();
			this.fwModel.setModelo('JURA280');
			this.fwModel.setFilter(`O12_CODUSR='${this.codUser}'`);
			this.fwModel.setHeaderParam(
				'orderBy',
				' ORDER BY O12_LIDO,O12_DATA DESC,O12_HORA DESC'
			);
			this.fwModel.get('Busca notificações').subscribe(resp => {
				const listNotifActions: Array<NotificationAction> = [];

				this.subListNotify.next([]);

				if (this.juriGenerics.changeUndefinedToEmpty(resp.resources) !== '') {
					resp.resources.forEach(i => {
						let tpAcao: ActionType;
						switch (
							this.juriGenerics.findValueByName(i.models[0].fields, 'O12_TPACAO')
						) {
							case '2':
								tpAcao = 'redirect';
								break;
							case '3':
								tpAcao = 'download';
								break;

							default:
								tpAcao = 'notify';
								break;
						}

						listNotifActions.push({
							pk: this.juriGenerics.decodeHtmlEntities(i.pk),
							user: this.juriGenerics.findValueByName(
								i.models[0].fields,
								'O12_CODUSR'
							),
							label: this.juriGenerics.findValueByName(
								i.models[0].fields,
								'O12_TITULO'
							),
							icon:
								'po-icon-' +
								this.juriGenerics.findValueByName(i.models[0].fields, 'O12_ICONE'),
							type:
								this.juriGenerics.findValueByName(
									i.models[0].fields,
									'O12_LIDO'
								) === 'T'
									? 'default'
									: 'danger',
							action: item => this.onClickNotification(item),
							actionType: tpAcao,
							param: this.juriGenerics.findValueByName(
								i.models[0].fields,
								'O12_PARAM'
							),
						});
					});

					this.subListNotify.next(listNotifActions);
				}
			});
		}
	}
	/**
	 * Função responsavel pela busca e download do arquivo informado na notificação
	 * @param param Parmetro indicando o caminho do arquivo
	 */
	downloadFile(param: string) {
		this.legalProcess.setParam('pathArq', param);
		this.legalProcess.get('downloadFile').subscribe(fileData => {
			if (fileData.attachments !== '') {
				fileData.attachments.forEach(item => {
					if (item.filedata !== '') {
						const blob = JurBase64.toUTF8(item.filedata, 'application/octet-stream');
						item.fileUrl = window.URL.createObjectURL(blob);
					} else {
						item.fileUrl = item.fileurl;
					}
					const fileLink = document.createElement('a');
					fileLink.href = item.fileUrl;
					fileLink.download = item.namefile; // força o nome do download
					fileLink.target = '_blank'; // força a abertura em uma nova página
					document.body.appendChild(fileLink);
					fileLink.click(); // gatilha o evento do click
				});
			}
		});
	}

	/**
	 * Função que faz ação do clique da notificação "Tarefa Atualizada!"
	 * @param item todos dados da notificação clicada.
	 */
	onClickNotification(item: NotificationAction) {
		if (item.actionType === 'download') {
			this.downloadFile(item.param);
		} else if (item.actionType === 'redirect') {
			this.router.navigateByUrl(encodeURIComponent(item.param).replace(/%2F/g, '/'));
		}

		if (item.type === 'danger') {
			this.setAsReaded(item, this.codUser).subscribe(resp => {
				this.getNotifications();
			});
		}
	}

	/**
	 * Função responsável pela marcação de lido da notificação
	 * @param item registro da notificação selecionada
	 * @param user código do usuário
	 */
	setAsReaded(item: NotificationAction, user: string): Observable<FwmodelService> {
		const fwModelBody: FwModelBody = new FwModelBody('JURA280', 4, item.pk, 'O12MASTER');

		fwModelBody.models[0].addField('O12_LIDO', 'T', 0, 'C');

		this.fwModel.restore();
		this.fwModel.setModelo('JURA280');

		return this.fwModel.put(item.pk, fwModelBody.serialize(), 'Marcar como lido');
	}
}
