import { TestBed } from '@angular/core/testing';

import { PesqAvancadaAdapterService } from './pesqAvancada-adapter.service';

describe('PesqAvancadaAdapterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PesqAvancadaAdapterService = TestBed.get(PesqAvancadaAdapterService);
    expect(service).toBeTruthy();
  });
});
