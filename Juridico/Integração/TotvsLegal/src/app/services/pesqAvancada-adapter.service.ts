import { Injectable } from '@angular/core';
import { LegalprocessService } from './legalprocess.service';
import { Observable } from 'rxjs';
import { PoComboOption, PoComboFilter } from '@po-ui/ng-components';
import { map } from 'rxjs/operators';
import { JurigenericsService } from './jurigenerics.service';

@Injectable({
	providedIn: 'root'
})
export class PesqAvancadaAdapterService implements PoComboFilter{
	listItensReturn: LpComboPesqReturnStruct[] = [];

	constructor(
		private legalprocess: LegalprocessService,
		private juriGenerics: JurigenericsService,
	) { }
	
	/**
	 * Função executada no momento em que for digitado ou selecionado o combo
	 * @param valorAtu - Valor digitado atual
	 */
	getFilteredData(valorAtu): Observable<Array<LpComboPesqReturnStruct>>{
		this.legalprocess.restore();
		this.listItensReturn = []
		this.legalprocess.setSearchKey(valorAtu.value);

		return this.legalprocess.get("fields").pipe(map(
			(data: any) => {
				
				if (this.juriGenerics.changeUndefinedToEmpty(data.result) != '') {
					data.result.forEach(item => {

						let itemReturn = new LpComboPesqReturnStruct();

						itemReturn.label    = item.title;
						itemReturn.value    = item.field;
						itemReturn.codigo   = item.codigo;
						itemReturn.type     = item.type;

						switch (itemReturn.type) {
							case "F3":
								itemReturn.f3fields = item.f3fields;
								break;

							case "COMBO":
								itemReturn.comboOptions = item.comboOptions;
							break;
						}
						if (itemReturn.type == "F3") {
						}

						this.listItensReturn.push(itemReturn);
					});
				}
				return this.listItensReturn;
			}
		))
	};

	/**
	 * Função responsável a realizar a consulta quando um Valor é definido para o campo que utiliza o Adapter
	 * @param value - Valor setado
	 */
	getObjectByValue(value: string): Observable<LpComboPesqReturnStruct>{
		return new Observable<LpComboPesqReturnStruct>();
	};
}

@Injectable({
	providedIn: 'root'
})
export class f3FieldsAdapter implements PoComboFilter{
	
	constructor(
		private legalprocess: LegalprocessService
	) { }

	/**
	 * Função executada no momento em que for digitado ou selecionado o combo
	 * @param valorDigitado - Valor digitado
	 * @param filterParams  - Parâmetros para a requisição
	 */
	getFilteredData(valorDigitado, filterParams: f3AdapterFilterParam): Observable<PoComboOption[]> {
		let arrF3Values: lpF3ListReturnStruct[] = [];
		this.legalprocess.restore();
		this.legalprocess.setSearchKey(valorDigitado.value.toString().toUpperCase())

		return this.legalprocess.get("f3list/" + filterParams.getFieldName()).pipe(map(
			(data) => {
				if (data.f3Options != ""){
					data.f3Options.forEach(item =>{
						let f3ItemOption = new lpF3ListReturnStruct();
						f3ItemOption.label = item.label
						f3ItemOption.value = item.value

						arrF3Values.push(f3ItemOption)
					})
				}
				return arrF3Values;
			}
		))
	}	
	
	/**
	 * Função responsável a realizar a consulta quando um Valor é definido para o campo que utiliza o Adapter
	 * @param value - Valor setado
	 * @param filterParams - Parâmetros do Adapter
	 */
	getObjectByValue(value: string, filterParams: f3AdapterFilterParam): Observable<PoComboOption> {
		return new Observable<PoComboOption>();
	}
}

/**
 * Campos Parametro do adapter para campos F3
 */
export class f3AdapterFilterParam {
	fieldName: string = ""
	constructor(fieldName: string){
		this.fieldName = fieldName;
	}
	
	getFieldName(){
		return this.fieldName;
	}
}

/**
 * Estrutura modelo para o retono do Adapter de Campos F3
 */
export class lpF3ListReturnStruct implements PoComboOption{
	value: string;
	label: string;
}


/**
 * Estrutura de Retorno do Adaptor. Fixado para não haver manipulação ou inclusão de novos elementos
 * Sem que o desenvolvedor tenha de alterar aqui.
 * @
 * Ao alterar essa classe, verificar os locais aonde é utilizado o adaptor.
 */
export class LpComboPesqReturnStruct implements PoComboOption{
	value    : string;
	label    : string;
	type     : string;
	codigo   : string;
	f3fields : string;
	comboOptions :Array<any>;
}
