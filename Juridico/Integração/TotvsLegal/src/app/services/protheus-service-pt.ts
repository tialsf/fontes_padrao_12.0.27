const modelError = {
	msgErro: "Mensagem de erro:",
	idPrograma: "Id submodelo erro:",
	idErro: "Id erro:",
	detTecnico: "Detalhes tecnicos:"
}

const httpError = {
	error401Expirado: "Sua sessão expirou. Por gentileza, faça login novamente.",
	error403: "Você não possui permissão para acessar a rotina",
	error4xx: "Erro na autenticação. Favor verificar o usuário e senha!",
	error5xx: "Erro inesperado. Favor verificar o serviço do Protheus!",
	erroIniNotific: "Erro ao realizar "
}

export const ProtheusServicePt = {
	httpError: httpError,
	modelError: modelError
}
