import { TestBed } from '@angular/core/testing';

import { protheusService } from './protheus.service';

describe('ProtheusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: protheusService = TestBed.get(protheusService);
    expect(service).toBeTruthy();
  });
}); 
