import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { PoNotificationService, PoI18nService } from '@po-ui/ng-components';
import { JurigenericsService } from '../services/jurigenerics.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RoutingService } from './routing.service';

const host = "/juri/"

/**
 * Classe para centralizar as chamadas do Protheus
 */

@Injectable({
	providedIn: 'root'
})
export class protheusService {

	private bAuth: boolean = true;
	private bShowErrNotification: boolean = true;
	private litProtheusService: any;

	/**
	* header padrão
	*/
	private optionsGetHeader = new HttpHeaders({
		'Content-Type': 'application/json;text/plain'
	});

	/**
	* params padrão
	*/
	private optionsGetParams = new HttpParams({
	});

	private cBody: string ='{}';

	/**
	* construtor da classe
	*/
	constructor(private http: HttpClient,
		private thfNotification : PoNotificationService,
		private router: Router,
		private routingService: RoutingService,
		private JuriGenerics: JurigenericsService,
		protected poI18nService: PoI18nService,
		private route: ActivatedRoute,) 
	{
		poI18nService.getLiterals( { context: 'protheusService', language: poI18nService.getLanguage()}).subscribe((litPrtService)=>{
			this.litProtheusService = litPrtService;
		})

		if (this.bAuth){
			this.setOptionsHeaders('Authorization',this.JuriGenerics.getRequestAuth())
		}
	}

	/**
	 * Seta a utilização do Authorization
	 * @param useAuth - Define se será utilizada a Autenticação
	 */
	useAuthorization(useAuth: boolean = true){
		this.bAuth = useAuth
	}
	
	/**
	 * Mostra a notificação de Erro?
	 * @param showNotify - Define se a mensagem será demonstrada ou não
	 */
	showErrorNotification(showNotify: boolean = false){
		this.bShowErrNotification = showNotify
	}

	/**
	* constroi um novo corpo URL acrescentando ao parametro o valor fornecido
	* @param param
	* @param value
	*/
	setOptionsParams(param: string, value: string) {
		this.optionsGetParams = this.optionsGetParams.set(param, value);
	}

	/**
	* acrescenta um novo valor de cabeçalho ao conjunto já existente
	* @param name
	* @param value
	*/
	setOptionsHeaders(name: string, value:string | string[]) {
		this.optionsGetHeader = this.optionsGetHeader.set(name, value);
	}

	/**
	 * Retornar o valor do Header passado por parâmetro
	 * @param paramName - Header solicitado
	 */
	getHeaderValue(paramName: string): string | null{
		return this.optionsGetHeader.get(paramName);
	}

	/**
	 * Retorna o valor do QueryParam passado por parâmetro
	 * @param paramName - QueryParam solicitado
	 */
	getParamValue(paramName: string): string | null{
		return this.optionsGetParams.get(paramName);
	}

	/**
	* limpa o params
	* */
	clearParams(){
		this.optionsGetParams = new HttpParams();
	}

	/**
	* limpa o headers
	* */
	clearHeader(){
		this.optionsGetHeader = new HttpHeaders({
			'Content-Type': 'application/json;text/plain'
		});

		this.setOptionsHeaders('Authorization',this.JuriGenerics.getRequestAuth());
		this.setOptionsHeaders('X-PO-No-Error', "true");
	}

	/**
	* restaura os valores iniciais
	*/
	restore(){
		this.bAuth = true;
		this.bShowErrNotification = true;
		this.cBody = "";

		this.clearParams();
		this.clearHeader();
	}

	/**
	 * Define o Body da requisição
	 * @param cBody - Corpo da requisição
	 */
	setBody(cBody){
		this.cBody = cBody;
	}

	/**
	* metodo GET
	* consome os serviços REST do Protheus
	* @param endpoint
	*/
	get(endpoint, operation, optHeader ?: HttpHeaders,  optParams ?: HttpParams) {
		if ( optHeader != undefined ){
			this.optionsGetHeader = optHeader;
		}

		if ( optParams != undefined){
			this.optionsGetParams = optParams;
		}

		return this.http.get<any>(host + endpoint, {
			headers: this.optionsGetHeader,
			params: this.optionsGetParams
		}).pipe(
			tap(),
			catchError(this.handleError(operation, []))
		);
	};

	/**
	* metodo POST
	* consome os serviços REST do Protheus
	* @param endpoint
	*/
	post(endpoint, operation, optHeader ?: HttpHeaders, optParams ?: HttpParams  ) {
		if ( optHeader != undefined ){
			this.optionsGetHeader = optHeader;
		}

		if ( optParams != undefined){
			this.optionsGetParams = optParams;
		}

		return this.http.post<any>(host + endpoint, this.cBody, {
			headers: this.optionsGetHeader,
			params: this.optionsGetParams
		}).pipe(
			tap(),
			catchError(this.handleError(operation, []))
		);
	};
	
    /**
    * metodo put
    * consome os serviços REST do Protheus
    * @param endpoint
    */
   put(endpoint, operation, optHeader ?: HttpHeaders, optParams ?: HttpParams ) {
		if ( optHeader != undefined ){
			this.optionsGetHeader = optHeader;
		}

		if ( optParams != undefined){
			this.optionsGetParams = optParams;
		}

		return this.http.put<any>(host + endpoint, this.cBody, {
			headers: this.optionsGetHeader,
			params: this.optionsGetParams
		}).pipe(
			tap(),
			catchError(this.handleError(operation, []))
		);
	};
	/**
	* metodo DELETE
	* consome os serviços REST do Protheus
	* @param endpoint
	*/
	delete(endpoint, operation) {
		return this.http.delete<any>(host + endpoint, {
			headers: this.optionsGetHeader,
			params: this.optionsGetParams
		}).pipe(
			tap(),
			catchError(this.handleError(operation, []))
		);
	};

	/**
	* Tratamento de erro na requisição
	*/
	private handleError<T>(operation : string = "Erro ", result?: T) {
		return (error: any): Observable<T> => {
			var msgError    : string   = "";
			var msgResp     : any      = "";
			var statusError : string   = "";

			// Proteção para obter mensagem de error do response e o code do erro
			if (this.JuriGenerics.changeUndefinedToEmpty(error.error) != "") {
				if (this.JuriGenerics.changeUndefinedToEmpty(error.error.errorCode) != "") {
					statusError = error.error.errorCode; // Status do erro para quando vier do FWModel
				} else if (this.JuriGenerics.changeUndefinedToEmpty(error.error.code) != "") {
					statusError = error.error.code; // Status do erro para quando vier da autenticação não autorizada
				} else {
					statusError = error.status;
				}
				
				//Response do protheus
				if (this.JuriGenerics.changeUndefinedToEmpty(error.error.errorMessage) != "") {
					msgResp = this.trataMsgErro(error.error.errorMessage)
					
				} else if (this.JuriGenerics.changeUndefinedToEmpty(error.error.message) != "") {
					msgResp = error.error.message;
				}
			} else {
				statusError = error.status;
			}

			// 401 - Erro de Não Autenticação
			if (statusError == "401") {
				// Verifica se está na tela de Login. Se estiver, não mostra a
				if (this.router.url != "/login") { 
					this.router.navigate(['/login']);
					msgError = this.litProtheusService.httpError.error401Expirado;
				} else {
					if (msgResp.indexOf("invalid_grant") != -1) {
						msgError = this.litProtheusService.httpError.error4xx;
					}
				}
			// 403 - Acesso não autorizado
			} else if (statusError == "403"){
				//Faz a validação da URL
				let bProcesso: boolean = error.url.match("JURA095") != null
				
				msgError =  this.litProtheusService.httpError.error403;
				if (this.router.url.match(/[/]/g).length > 3 || bProcesso) {
					this.thfNotification.error(msgError);
					if (bProcesso) {
						this.router.navigate(['/home']);
					} else {
						this.router.navigate([this.routingService.getPreviousUrl()]);
					}
				}
				throw new Error(statusError);
			} else {
				if (msgResp != "") {
					msgError = this.litProtheusService.httpError.erroIniNotific + operation + ": " + msgResp; // Quando tiver response de error, será apresentado ao usuário a operação + response
				} else {
					msgError = this.litProtheusService.httpError.error5xx; // Quando não tiver response de error, será apresentado ao usuário que o servidor do protheus está com problema.
					this.router.navigate(['/login']);
				}
			}

			if (this.bShowErrNotification && msgError != ""){
				this.thfNotification.error(msgError);
			}

			return of(result as T);
		};
	}

	/**
	* trataMsgErro
	* Responsável por receber a mensagem de erro do modelo e trata-la de uma maneira legível
	* @param mensagem
	*/
	trataMsgErro(mensagem) {
		let response:   string = ''
		let match:      Array<Object> = []
		let msgErro:    string = ''
		let idPrograma: string = ''
		let idErro:     string = ''
		let detTecnico: string = ''
		let msgFinal:   string = ''

		// Variáveis que contém a expressão regular a ser pesquisada
		let reMsgErro = new RegExp(this.litProtheusService.modelError.msgErro, 'g')
		let reIdPrograma = new RegExp(this.litProtheusService.modelError.idPrograma, 'g')
		let reIdErro = new RegExp(this.litProtheusService.modelError.idErro, 'g')
		let reDetTecnico = new RegExp(this.litProtheusService.modelError.detTecnico, 'g')

		response = mensagem.split('\n') // Transforma a mensagem recebida em objeto

		// Pesquisa a expressão regular em cada linha e obtem seu conteúdo depois do caractere ":"
		for (var i = 0; i < response.length; i++) {
			match = response[i].match(reMsgErro)
			if (match != null) {
				msgErro = match + response[i].substr(response[i].indexOf(":") + 1)
				msgErro = msgErro.replace('[','').replace(']','')
			}
		}

		for (var i = 0; i < response.length; i++) {
			match = response[i].match(reIdPrograma)
			if (match != null) {
				idPrograma = response[i].substr(response[i].indexOf(":") + 1)
				idPrograma = idPrograma.replace('[','').replace(']','')
			}
		}

		for (var i = 0; i < response.length; i++) {
			match = response[i].match(reIdErro)
			if (match != null) {
				idErro = response[i].substr(response[i].indexOf(":") + 1)
				idErro = idErro.replace('[','').replace(']','')
			}
		}

		for (var i = 0; i < response.length; i++) {
			match = response[i].match(reDetTecnico)
			if (match != null) {
				i + 1
				detTecnico = response[i].substr(response[i].indexOf(":") + 1)
				detTecnico = ' / ' +  detTecnico.replace('[','').replace(']','')
			} else {
				detTecnico = ''
			}
		}

		msgFinal = idPrograma + ' / ' + idErro  + ' / ' + msgErro + detTecnico

		return msgFinal
	}

}
