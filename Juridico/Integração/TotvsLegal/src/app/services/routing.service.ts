import { Injectable } from '@angular/core';
import { filter } from 'rxjs/operators';
import {
	Router,
	NavigationEnd,
	UrlSegmentGroup,
	UrlSegment,
	UrlTree,
	PRIMARY_OUTLET,
	ActivatedRoute,
	NavigationExtras
} from '@angular/router';

@Injectable({
	providedIn: 'root'
})

export class RoutingService {
	private history = [];

	constructor(
		private router: Router,
		private route: ActivatedRoute
	) {}

	/**
	 * Inclui a URL atual no histórico para buscar a navegação
	 */
	public loadRouting(): void {
		this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(
			({urlAfterRedirects}: NavigationEnd) => 
		{
			// Verifica se já está inputado a url como ultima url navegada. se for, não inclui para evitar duplicidade
			if (this.history[this.history.length - 1] != urlAfterRedirects){
				this.history = [...this.history, urlAfterRedirects];
			}

		});
	}

	/**
	 * Retorna todo o histórico de navegação
	 */
	public getHistory(): string[] {
		return this.history;
	}

	/**
	 * Busca a URL anterior. Caso não encontre a URL anterior. Pega a URL atual
	 * removendo o código passado por parâmetro
	 * @param cCodPagAtual  - Código a ser removido
	 */
	public getPreviousUrl(cCodPagAtual?: string, cancelAction: boolean = true): string {
		var retHist: string = this.history[this.history.length - 2] 
		if ( retHist == undefined) {
			if (this.history != undefined && this.history.length > 0){
				// Remove o código da pagina da URL
				if (cCodPagAtual == 'pedido'){
					retHist = this.history[this.history.length - 1].substr(0,this.history[this.history.length - 1].lastIndexOf("/"))
				} else {
					retHist = this.history[this.history.length - 1].toString().replace('/' + cCodPagAtual,'');
				}
	
				// Caso seja uma pagina de Cadastro
				if (retHist.indexOf("cadastro") > 0){
					retHist = this.history[this.history.length - 1].toString().replace('/cadastro','');
				}
			}

			// Se não tiver URL para retornar, manda pra Home
			if (retHist == undefined || retHist == "" ){
				retHist = '/';
			}

			
		} else {
			if (cancelAction){
				//segmenta a URL corrente
				var tree: UrlTree = this.router.parseUrl(this.history[this.history.length - 1])
				var urlSegmentGroup: UrlSegmentGroup = tree.root.children[PRIMARY_OUTLET];
				var urlSegment: UrlSegment[] = urlSegmentGroup.segments
				//se a URL à retornar for filha da URL atual (o ultimo segmento da URL corrente está contida na URL a ser redirecionada)
				if(retHist.indexOf(urlSegment[urlSegment.length -1].path +"/") != -1){
					retHist = "";
					//regride nos segmentos da URL corrente
					for(let count = 0; count < urlSegment.length-1; count ++){
						retHist += "/" + urlSegment[count].path;
					}
					//retira do histórico a URL atual e a filha
					this.history.pop();
					this.history.pop();
				}else{
					this.history.pop();
				}
			}
		}

		if (retHist === this.router.url) {
			retHist = this.getPreviousRoute(cCodPagAtual)
		}

		if (cCodPagAtual != 'login'){
			 retHist = decodeURIComponent(retHist);
		}

		return retHist;
	}

	/**
	 * função responsavel para retornar a url da ultima rota ou da ultima pagina acessada
	 * @param currentPage verifica a ultima pagina visitada e ignora essa rota
	 */
	public getPreviousRoute(currentPage: string = '') {
		const urlPreviousRoute: Array<string> = []
		let url = this.router.url.replace(currentPage,'')
		let children = this.router.parseUrl(url).root.children["primary"]
		let rotas: Array<UrlSegment> = children != undefined ? children.segments : [{ path: '/' } as UrlSegment]

		for (let i = 0; i < rotas.length -1; i++) {
			urlPreviousRoute.push(rotas[i].path);
		}

		return urlPreviousRoute.join('/')
	}
	
	/** Função responsavel pela navegação da pagina conforme a ultima pagina/rota acessada
	 * @param currentPage verifica a ultima pagina visitada e ignora essa rota
	 * @param extras parametros do tipo NavigationExtras que serão enviados no método navigate
	 */
	public navigateToPreviousPage(currentPage: string, extras?: NavigationExtras): void {
		let url: string = ''

		if (this.route.snapshot.queryParamMap.keys.length) {
			url = this.getPreviousRoute(currentPage)
		} else {
			url = this.getPreviousUrl(currentPage)
		}

		this.router.navigate([url],extras)
	}
}