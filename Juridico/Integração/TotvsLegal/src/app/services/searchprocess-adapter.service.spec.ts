import { TestBed } from '@angular/core/testing';

import { SearchprocessAdapterService } from './searchprocess-adapter.service';

describe('SearchprocessAdapterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SearchprocessAdapterService = TestBed.get(SearchprocessAdapterService);
    expect(service).toBeTruthy();
  });
});
