import { Injectable } from '@angular/core';
import { LegalprocessService } from './legalprocess.service';
import { Observable } from 'rxjs';
import { PoComboOption, PoComboFilter } from '@po-ui/ng-components';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})

export class SearchprocessAdapterService implements PoComboFilter{

	constructor(
		private legalprocess: LegalprocessService
	) { }

	/**
	 * Função executada no momento em que for digitado ou selecionado o combo
	 * @param valorAtu - Valor digitado atual
	 */
	getFilteredData(valorAtu): Observable<Array<PoComboOption>>{
		this.legalprocess.restore();
		this.legalprocess.setSearchKey(valorAtu.value);
		this.legalprocess.setPageSize('10');

		return this.legalprocess.getSearchProcess().pipe(map(
			(data: any) => {
				
				let listItensReturn: adaptorReturnStruct[] = [];
				if (data.processes != '') {
					data.processes.forEach(item => {
						let itemReturn = new adaptorReturnStruct();
						itemReturn.label  = 
							item.processId.toString().replace(new RegExp('^0*', 'g'), '') + 
							" - " + item.description;
						if (data.hasMoreBranch && item.processBranch != '') {
							itemReturn.label += " - " + item.processBranch ;
						}
						itemReturn.value = btoa(item.processBranch + "/" + item.processId.toString())
						listItensReturn.push(itemReturn);
					});
				}
				return listItensReturn;
			}
		))
	};
	
	getObjectByValue(value: string): Observable<PoComboOption>{
		this.legalprocess.restore();
		this.legalprocess.setSearchKey(value);
		this.legalprocess.setPageSize('10');

		return this.legalprocess.getSearchProcess().pipe(map(
			(data: any) => {

				let itemReturn = new adaptorReturnStruct();
				if (data.processes != ''){
					data.processes.forEach(item => {
						itemReturn.label = item.description +" - "+ item.processId.toString();
						itemReturn.value = item.processId.toString();
					});
				} else {
					this.getObjectByValue(value);
				}

				return itemReturn;
			}
		))
	};

}

/**
 * Estrutura de Retorno do Adaptor. Fixado para não haver manipulação ou inclusão de novos elementos
 * Sem que o desenvolvedor tenha de alterar aqui.
 * @
 * Ao alterar essa classe, verificar os locais aonde é utilizado o adaptor.
 */
export class adaptorReturnStruct implements PoComboOption{
	value: string;
	label: string;
}
