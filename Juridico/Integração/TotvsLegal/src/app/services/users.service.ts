import { Injectable } from '@angular/core';
import { protheusService } from './protheus.service';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';

const endpoint = "users";

@Injectable({
	providedIn: 'root'
})
export class UsersService {

	constructor(
		private protheusService: protheusService,
		private juriGenerics: JurigenericsService
	) { }

	// Parâmetros

	/**
	 * Indica se o get deve retornar o usuário admin
	 * @param value (default: "false")
	 */
	setShowAdmin(value: string){
		this.protheusService.setOptionsParams("showAdmin", value);
	}

	/**
	 * Indica quantos usuários deverão ser retornados pelo método
	 * @param value quantidade de usuários (default: todos)
	 */
	setCount(value: string){
		this.protheusService.setOptionsParams("count", value);
	}

	/**
	 * Indica a partir de qual usuário encontrado deverá ocorrer o retorno
	 * @param value start index (default: "1")
	 */
	setStartIndex(value: string){
		this.protheusService.setOptionsParams("startIndex", value);
	}

	/**
	 * Indica quais atributos do jSon devem ser retornados. Os atributos devem ser separados por ','
	 * @param value Exemplo: "id,name" (default: todos os atributos)
	 */
	setAttributes(value: string){
		this.protheusService.setOptionsParams("attributes", value);
	}

	/**
	 * Retorna o id do usuário atualmente logado pelo serviço REST no Protheus.
	 * @param nameOperation - Nome da operação
	 */
	getUserId( nameOperation: string = 'getUserId' ){
		return this.protheusService.get(endpoint + '/GetUserId', nameOperation);
	}

	/**
	 * Para recuperar um usuário conhecido, os clientes enviam requisições GET. 
	 * Se o usuário existir o servidor responde com o código de estado 200 e inclui o resultado 
	 * no corpo da resposta. 
	 * Também é possível listar os usuários do sistema, omitindo o envio do pathParam {userId}.
	 * @param userId        - (Opcional) Código do usuário 
	 * @param nameOperation - Nome da Operação
	 */
	get(userId?: string, nameOperation: string = "GetUsers") {
		let id: string = this.juriGenerics.changeUndefinedToEmpty(userId);
		let endpointUrl: string = endpoint+(id !== "" ? "/"+id : "" );
		
		return this.protheusService.get(endpointUrl, nameOperation);
	}

	/**
	 * Método utilizado para bloquear um usuário existente. O usuário é bloqueado, e todos os itens amarrados 
	 * ao seu registro (grupos, vínculo funcional, etc) são desassociados.
	 * @param userId        - Código do usuário
	 * @param nameOperation - Nome da operação
	 */
	delete(userId: string, nameOperation: string = "deleteFWModel"){

		return this.protheusService.delete(endpoint + "/" + userId, nameOperation);
	}

	/**
	 * Método utilizado para atualizar um usuário existente, quando bem sucedida, 
	 * o código de resposta 201 (created).
	 * @param userId  - Código do usuário
	 * @param bodyReq - Body da requisição
	 * @param nameOperation - Nome da operação
	 */
	put(userId:string, bodyReq: string, nameOperation: string = "putFWModel"){

		this.protheusService.setBody(bodyReq);
		return this.protheusService.put(endpoint + "/" + userId, nameOperation);
	}

	/**
	 * Cria novos usuários no sistema devolvendo na requisição, quando bem sucedida, 
	 * o código de resposta 201 (created).
	 * @param userId    - Código do usuário
	 * @param bodyReq   - Body da requisição
	 * @param operation - (Opcional) Valores aceitos: activate e deactivate. Indica se o usuário será ativado no 
	 * 					  sistema (activate) ou se o usuário será bloqueado via SAML (deactivate). 
	 * 					  Se este parâmetro não for informado, um novo usuário será criado.
	 * @param nameOperation - Nome da operação
	 */
	post(userId:string, cBody?: string, operation?: string, nameOperation: string = "postFWModel"){
		let endpointUrl : string = endpoint + "/" + userId
		if(this.juriGenerics.changeUndefinedToEmpty(cBody) !== ""){
			this.protheusService.setBody(cBody);
		}

		if(this.juriGenerics.changeUndefinedToEmpty(operation) !== ""){
			endpointUrl = endpointUrl + "/" + operation
		}
		return this.protheusService.post(endpointUrl, nameOperation);
	}

	/**
	 * Restaura as configurações padrões do ProtheusService
	 */
	restore() {
		this.protheusService.restore();
	}


}
