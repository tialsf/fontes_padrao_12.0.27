import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JurJustifModalComponent } from './jur-justif-modal.component';

describe('JurJustifModalComponent', () => {
  let component: JurJustifModalComponent;
  let fixture: ComponentFixture<JurJustifModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JurJustifModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JurJustifModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
