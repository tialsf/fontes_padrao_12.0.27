import { Component, OnInit, ViewChild } from '@angular/core';
import { PoModalComponent, PoModalAction, PoNotificationService, PoI18nService } from '@po-ui/ng-components';
import { Justificativa } from './justificativa';
import { FwmodelService } from 'src/app/services/fwmodel.service';
import { TLUserInfo } from 'src/app/services/authlogin.service';
import { FwmodelAdaptorService, adaptorFilterParam } from 'src/app/services/fwmodel-adaptor.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { JurigenericsService, JurUTF8 } from 'src/app/services/jurigenerics.service';
import { FwModelBody, FwModelBodyModels, FwModelBodyItems } from 'src/app/services/fwmodel.service';
import { modelJustifCorresp } from './justifCorresp.model';

export class DadosInstancia {
	instancia:      string;
	descInstancia:  string;
	motivo:         string;
	correspondente: string;
	codCorresp:     string;
	lojaCorresp:    string;
}

@Component({
  selector: 'jur-justif-modal',
  templateUrl: './jur-justif-modal.component.html',
  styleUrls: ['./jur-justif-modal.component.css']
})
export class JurJustifModalComponent implements OnInit {
	public litJustif: any
	public isOk: boolean = false;
	public isActive: boolean = false;
	public funcConfirm: Function;
	public funcCancel: Function;
	public formJustificativa: FormGroup;
	public formJustifCorresp: FormGroup;
	public codGar: string;
	public cajuri: string;
	public isLoadingConfirm: boolean = false;
		
	// Adaptor do Tipo de Justificativa
	public adaptorParamJustifMotivo: adaptorFilterParam = new adaptorFilterParam("JURA025", "NQX_CLMTAL = '1'", "NQX_DESC", "NQX_DESC", "", "NQX_COD", "", "getJustifMotivo");
	
	public actionConfirm: PoModalAction = {
		label: "Salvar",
		loading: this.isLoadingConfirm,
		action: () => {	
			this.insertJustificativa()
		},
	};

	public actionCancel: PoModalAction = {
		label: "Cancelar",
		action: () => {
			if (this.funcCancel != undefined){
				this.funcCancel()
			}

			this.modalJustif.close()
		 }
	}

	public confirmJustifCorresp: PoModalAction = {
		label: " ", // Salvar
		loading: this.isLoadingConfirm,
		action: () => {
			let cBodyCorresp = this.bodyPostJustifCorresp(this.formJustifCorresp.value);
			window.sessionStorage.setItem('Corresp', cBodyCorresp);
			this.insertJustifCorresp(this.formJustifCorresp.value);
		}, 
	};

	public cancelJustifCorresp: PoModalAction = {
		label: " ",  // Cancelar
		action: () => {
			this.modalCorresp.close()
		 }
	}

	@ViewChild('jurJustif') modalJustif  : PoModalComponent;
	@ViewChild('justifCorresp') modalCorresp : PoModalComponent;

	constructor(private fwmodel: FwmodelService,
				private userInfo: TLUserInfo,
				private juriGenerics: JurigenericsService,
				private poNotification: PoNotificationService,
				private formBuilder: FormBuilder,
				protected thfI18nService: PoI18nService,
				public fwModelAdaptorService: FwmodelAdaptorService) {
		
		thfI18nService.getLiterals({ context: 'jurJustifModal', language: thfI18nService.getLanguage() }).subscribe(
			(litJurJustModal) => {
				this.litJustif = litJurJustModal

				this.confirmJustifCorresp.label = this.litJustif.btnSalvarJustificativa
				this.cancelJustifCorresp.label  = this.litJustif.btnCancelar
				this.actionConfirm.label = this.litJustif.btnSalvarJustificativa
				this.actionCancel.label = this.litJustif.btnCancelar
			}
		);
	}
	
	ngOnInit() {
		this.createFormJustificativa(new Justificativa());
		this.createFormJustifCorresp(new modelJustifCorresp());
	}

	/**
	* Responsável por abrir a modal de Justificativa de Alteração em processo encerrado
	*/
	async openModal(codAssJur:string, funcConfirm?: Function, funcCancel?: Function){
		this.modalJustif.open();

		this.createFormJustificativa(new Justificativa())
		this.actionConfirm.loading = false
		this.actionCancel.loading = false
		this.isActive = true;

		this.cajuri = codAssJur
		if (funcConfirm != undefined){
			this.funcConfirm = () => { funcConfirm(); this.modalJustif.close()} // Inclui a função passada e inclui o fechamento do modelo
		}

		if (funcCancel != undefined){
			this.funcCancel = funcCancel
		}
	}

	/**
	* POST da inclusão de Justificativa de Alteração em processo encerrado
	*/
	async insertJustificativa() {
		this.actionConfirm.loading = true
		const cBody = this.bodyPostJustificativa(this.formJustificativa.value);

		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA166');
		this.fwmodel.post(cBody, 'Inclusão de Justificativa').subscribe((data) => {
			if (data != '') {
				this.codGar = this.juriGenerics.findValueByName(data.models[0].fields, "NT2_COD");
				this.isOk = true;
				this.poNotification.success(this.litJustif.msgInclusao);
				
				if (this.funcConfirm != undefined){
					this.funcConfirm()
				} else {
					this.modalJustif.close()
				}
			}
			this.actionConfirm.loading = false;
			return this.isOk;
		});

	}

	/**
	* Responsável por abrir a modal de Justificativa de Alteração de correspondente
	*/
	async openModalCorresp(codAssJur:string, dadosInstancia: DadosInstancia,  funcConfirm?: Function, funcCancel?: Function){
		this.modalCorresp.open();
		this.createFormJustifCorresp(new modelJustifCorresp())

		if(dadosInstancia != undefined && this.juriGenerics.changeUndefinedToEmpty(dadosInstancia.correspondente) != '' ){
			this.formJustifCorresp.patchValue({
				codInstancia: dadosInstancia.instancia,
				instancia:    dadosInstancia.descInstancia,
				descCorresp:  dadosInstancia.correspondente,
				motivo:       dadosInstancia.motivo,
				codCorresp:   dadosInstancia.codCorresp,
				lojaCorresp:  dadosInstancia.lojaCorresp,
			});
		}

		this.actionConfirm.loading = false
		this.actionCancel.loading = false
		this.isActive = true;
		this.cajuri = codAssJur

		if (funcConfirm != undefined){
			this.funcConfirm = () => { funcConfirm(); this.modalCorresp.close()} // Inclui a função passada e inclui o fechamento do modelo
		}

		if (funcCancel != undefined){
			this.funcCancel = funcCancel
		}
	}

	/**
	* Responsável por configurar o botão confirmar e fechar a modal de justificativa de alteração de correspondente
	*/
	async insertJustifCorresp(dadosFormCorresp: DadosInstancia) {

		if(dadosFormCorresp.motivo != ''){

			this.actionConfirm.loading = true
			this.isOk = true;

			if (this.funcConfirm != undefined){
				this.funcConfirm()
			} else {
				this.modalCorresp.close()
			}
			this.actionConfirm.loading = false;

		}else{
			this.poNotification.error(this.litJustif.msgCampoMotivo)
		}
		
		return this.isOk;
	}

	/**
	* Responsável por obter os dados inputados no formulário de inclusão
	* da justificativa para serem usados após o submit
	*/
   createFormJustificativa(justificativa: Justificativa) {
		this.formJustificativa = this.formBuilder.group({
			codMotivo  : [ justificativa.codMotivo, Validators.compose([Validators.required])],
			justificativa : [ justificativa.justificativa, Validators.compose([Validators.required])],
		});
	}

	/**
	* Monta o body para o post de justificativa de alteração de processo encerrado
	*/
	bodyPostJustificativa(formJustificativa: Justificativa) {
		let fwModelBody: FwModelBody = new FwModelBody('JURA166', 3);
		let modelNUV: FwModelBodyModels =  fwModelBody.addModel("NUVMASTER")

		modelNUV.addField( 'NUV_CAJURI', this.cajuri );
		modelNUV.addField( 'NUV_USUALT', this.userInfo.getNomeUsuario() );
		modelNUV.addField( 'NUV_CMOTIV', formJustificativa.codMotivo );
		modelNUV.addField( 'NUV_JUSTIF', JurUTF8.encode(formJustificativa.justificativa) );
		modelNUV.addField( 'NUV_CLMTAL', '1' );

		return JSON.stringify(fwModelBody);
	}

	/**
	* Responsável por obter os dados inputados no formulário de inclusão
	* da justificativa de correspondente para serem usados após o submit
	*/
	createFormJustifCorresp(motivoCorresp: modelJustifCorresp) {
		
		this.formJustifCorresp = this.formBuilder.group({
			codInstancia: [ motivoCorresp.codInstancia],
			instancia   : [ motivoCorresp.instancia],
			descCorresp : [ motivoCorresp.descCorresp], 
			motivo      : [ motivoCorresp.motivo,  Validators.compose([Validators.required])],
			codCorresp  : [ motivoCorresp.codCorresp],
			lojaCorresp : [ motivoCorresp.lojaCorresp]
		});
	}
	
	/**
	* Monta o body para o post de justificativa de Correspondente
	*/
	bodyPostJustifCorresp(formJustifCorresp: modelJustifCorresp){
		let fwModelBody: FwModelBody = new FwModelBody('JURA093', 3);
		let modelNTC: FwModelBodyModels =  fwModelBody.addModel("NTCMASTER")

		modelNTC.addField('NTC_CAJURI', this.cajuri);
		modelNTC.addField('NTC_INSTAN', formJustifCorresp.codInstancia);
		modelNTC.addField('NTC_CCORES', formJustifCorresp.codCorresp);
		modelNTC.addField('NTC_LCORRE', formJustifCorresp.lojaCorresp);
		modelNTC.addField('NTC_MOTIVO', formJustifCorresp.motivo);

		return JSON.stringify(fwModelBody);
	}
}