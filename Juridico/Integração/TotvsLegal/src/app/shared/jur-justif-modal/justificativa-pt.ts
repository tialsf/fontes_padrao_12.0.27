export const JustificativaPt = {
	titulo: "Justificativa para alteração de processos encerrados",
	cmbMotivo: "Motivo",
	inputJustificativa: "Justificativa",
	msgInclusao: "Justificativa para alteração de processo encerrado, incluída com sucesso!",
	btnSalvarJustificativa: 'Salvar justificativa',
	btnCancelar: "Cancelar",
	alteracaoCorresp: "Justificativa para alteração de correspondente",
	instancia: "Instância",
	correspondente: "Correspondente",
	motivo: "Motivo",
	msgCampoMotivo: "É necessário preencher o campo de Motivo para salvar as alterações."
	}
