export class Justificativa {
	filial:          string = '';
	codJustif:       string = '';
	classifMotivAlt: string = '';
	cajuri:          string = '';
	usuarioAlter:    string = '';
	dataAlter:       string = '';
	codMotivo:       string = '';
	descMotivo:      string = '';
	justificativa:   string = '';
	dataOrdenacao:   string = '';
}