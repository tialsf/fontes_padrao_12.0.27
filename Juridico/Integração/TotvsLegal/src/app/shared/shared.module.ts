import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PoModule, PoComponentsModule } from '@po-ui/ng-components';
import { JurJustifModalComponent } from './jur-justif-modal/jur-justif-modal.component';

@NgModule({
	declarations: [
		JurJustifModalComponent
	],
	exports: [
		JurJustifModalComponent
	],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		PoModule,
		PoComponentsModule
	]
})
export class SharedModule { }
