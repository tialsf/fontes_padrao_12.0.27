import { PoToolbarAction } from '@po-ui/ng-components';

export type ActionType = 'notify' | 'redirect' | 'download';

/**
 * Interface contendo a extrutura da lista de notificações
 */
export interface NotificationAction extends PoToolbarAction {
	pk: string;
	user: string;
	param?: string;
	actionType?: ActionType;
}
