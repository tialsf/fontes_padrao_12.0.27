export const breadcrumb ={
	brHome:   "Meus contratos",
	brSLAJur: "Indicadores de SLA"
}

export const processaWF = {
	infoProcessar: "Existem {0} workflows finalizados que ainda não foram pocessados para calcular o SLA. ",
	infoAgora:     "Deseja processar agora?",
	btnProcessar:  "Processar",
	progressBar:   "Processando SLA jurídico...",
	carregaInfo:   "Estamos verificando, aguarde...",
	progressIncos: "Processando consultas com Workflows inconsistentes..."
}

export const modal ={
	msgAlerta:      "O SLA jurídico será processado em segundo plano, e ao término do processamento, o gráfico será atualizado.",
	titleProcessar: "Atenção!",
	cancelar:       "Cancelar",
	ok:             "Ok",
	msgIgnorarWf:   "Foram encontrados {0} registros que não possuem workflow correspondente no Fluig.",
	confirmIgnora:  " Deseja ignorar estes registros?"
}

export const graficoAreaSoli = {
	titulo:       "Análise de SLA por área solicitante",
	descricao:    "Tempo gasto em horas, de resposta do Jurídico em interações das solicitações de contrato por área solicitante, mês, nos últimos 6 meses.",
	titFiltro:    "Se necessário, utilize o campo abaixo para filtrar o gráfico:",
	labelDataSet: "Horas"
}

export const graficoFluxo = {
	titulo:       "Análise de SLA por fluxos do jurídico",
	descricao:    "Tempo gasto em horas de cada fluxo que o departamento jurídico executou nas solicitações de contrato, por tipo de contrato.",
	titFiltro:    "Se necessário, utilize os campos abaixo para filtrar o gráfico:",
	labelDataSet: "Horas"
}

export const graficoSLA = {
	titulo:       "Análise de SLA do jurídico",
	descricao:    "Tempo médio (dias), de resposta do Jurídico em interações das solicitações de contrato, por mês, nos últimos 12 meses.",
	labelDataSet: "Dias"
}

export const SLAJuridicoPt = {
	descricaoSLA: "Indicadores de SLA",
	loadWF:       "Carregando informações...",
	sim:          "Sim",
	titBtnFiltro: "Filtrar",
	titTpCont:    "Tipo de contrato",
	titAreaSolic: "Área solicitante",
	titDtInicial: "Data inicial",
	titDtFinal:   "Data final",
	nao:          "Não",
	infoErro:     "Alguns dos workflows não puderam ser processados. Verifique...",
	breadcrumb:   breadcrumb,
	processaWF:   processaWF,
	modal,
	graficoFluxo,
	graficoAreaSoli,
	graficoSLA
}