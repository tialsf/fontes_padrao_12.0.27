import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlaJuridicoComponent } from './sla-juridico.component';

describe('SlaJuridicoComponent', () => {
  let component: SlaJuridicoComponent;
  let fixture: ComponentFixture<SlaJuridicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlaJuridicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlaJuridicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
