import { Component, OnInit, ViewChild } from '@angular/core';
import { PoI18nService,
	PoBreadcrumb,
	PoI18nPipe,
	PoNotificationService,
	PoModalComponent,
	PoModalAction } from '@po-ui/ng-components';
import { ChartType, ChartDataSets, ChartOptions } from 'chart.js';
import { JurconsultasService } from '../services/jurconsultas.service';
import { JurigenericsService } from '../services/jurigenerics.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FiltroGraphFluxo, FiltroGraphArea } from './sla-juridico';
import { FwmodelService } from '../services/fwmodel.service';

class Historico {
	filial:      string;
	codConsulta: string;
}

class GraficosPadrao {
	label: string  = '';
	value: string  = '';
	indice: number = -1;
}

@Component({
	selector: 'app-sla-juridico',
	templateUrl: './sla-juridico.component.html',
	styleUrls: ['./sla-juridico.component.css']
})
export class SlaJuridicoComponent implements OnInit {
	litSLA:                 any;
	formFluxoDept:          FormGroup;
	formAreaSolic:          FormGroup;
	
	slaJuridico:            string           = '';
	processaWF:             string           = '';
	confirmaProcWF:         string           = '';
	msgIgnorarWf:           string           = '';
	isHideLoadGraphSLA:     boolean          = true;
	isHideLoadGraphFlx:     boolean          = true;
	isHideLoadGraphArea:    boolean          = true;
	isDisabledProcessar:    boolean          = false;
	isProcessou:            boolean          = false;
	isShowProgressBar:      boolean          = false;
	isDisabledBtnProcessar: boolean          = false;
	isNext:                 boolean          = false;
	isProcessErr:           boolean          = false;
	isShowProgBarIncons:    boolean          = false;
	nProgressTotal:         number           = 0;
	nProgressProcHist:      number           = 0;
	nProgressWFIncons:      number           = 0;
	nPagina:                number           = 1;
	nProcessErr:            number           = 0;
	nInconsErr:             number           = 0;
	listConsultasProcessar: Array<Historico> = [];
	listPeriodosSLA:        Array<string>    = [];
	listPeriodosArea:       Array<string>    = [];
	listTpContrato:         Array<GraficosPadrao>    = [];
	listAreaSolic:          Array<GraficosPadrao>    = [];
	listProcessErr:         Array<Historico> = [];

	//-- Propriedades para o Grafico do SLA Jurídico
	public barChartSLAType:   ChartType       = 'line';
	public barChartSLALegend: boolean         = false;
	public barChartSLALabels: string[]        = [];
	public barChartSLAData:   ChartDataSets[] = [{data:[]}];
	public barChartSLAOptions: ChartOptions   = {
		responsive: true,
		elements: {
			line: { tension: 0 },
			point: {
				radius: 7,
				borderWidth: 2,
				hitRadius: 5,
				hoverRadius: 10,
				hoverBorderWidth: 2
			}
		}
	};

	//-- Propriedades para o Gráfico dos Fluxos do Jurídico 
	public barChartFluxoType:    ChartType       = 'bar';
	public barChartFluxoLegend:  boolean         = false;
	public barChartFluxoLabels:  string[]        = [];
	public barChartFluxoData:    ChartDataSets[] = [{data:[]}];
	public barChartFluxoOptions: ChartOptions    = {
		responsive: true,
		scales: {
			xAxes: [{
				stacked: true,
			}],
			yAxes: [{
				stacked: true
			}]
		}
	};

	// Gráfico de Horas por área
	public barChartAreaLegend: boolean         = false;
	public barChartAreaLabels: string[]        = [];
	public barChartAreaData:   ChartDataSets[] = [{data:[]}];
	public barChartAreaOptions: ChartOptions   = {
		responsive: true,
		elements: {
			line: { tension: 0 },
			point: {
				radius: 7,
				borderWidth: 2,
				hitRadius: 5,
				hoverRadius: 10,
				hoverBorderWidth: 2
			}
		}
	};

	// BreadCrumb
	public breadcrumb: PoBreadcrumb = {
		items: [],
	};

	// Ações do modal de Processamento
	cancelar: PoModalAction = {
		action: () => {
			this.poModal.close();
		},
		label: 'Cancelar',
	};
	processar: PoModalAction = {
		action: () => {
			this.poModal.close();
			this.processarHistorico();
		},
		label: 'Ok',
	};
	

	// Ações do modal de ignorar WF inconsistentes no Fluig
	nao: PoModalAction = {
		action: () => {
			this.modalIgnoraWF.close();
		},
		label: 'Não',
	};
	sim: PoModalAction = {
		action: () => {
			this.modalIgnoraWF.close();
			this.processarWFIncons();
		},
		label: 'Sim',
	};

	@ViewChild('modalIgnoraWFIncons', { static: true }) modalIgnoraWF: PoModalComponent;
	@ViewChild('modalProcessarHist' , { static: true }) poModal:       PoModalComponent;

	constructor (
			private poI18nService:  PoI18nService,
			private jurConsultas:   JurconsultasService,
			private JuriGenerics:   JurigenericsService,
			private formBuilder:    FormBuilder,
			private fwmodel:        FwmodelService,
			private poI18nPipe:     PoI18nPipe,
			public  poNotification: PoNotificationService,
		) {
		//-- Chamada do serviço poI18n para buscar as constantes a partir do contexto 'slaJur' no arquivo JurTraducao.
		poI18nService.getLiterals({ context: 'slaJur', language: poI18nService.getLanguage() })
			.subscribe((litSLA) => {
				this.litSLA = litSLA;

				this.slaJuridico    = this.litSLA.descricaoSLA;

				//-- seta as constantes para tradução dos títulos do Breadcrumb de Acompanhamento - acompanhamento-pt.ts
				this.breadcrumb.items = [
					{ label: this.litSLA.breadcrumb.brHome, link: '/homeContratos' },
					{ label: this.litSLA.breadcrumb.brSLAJur}
				]
				//-- seta as constantes para tradução dos botões do modal de confirmação
				this.cancelar.label  = this.litSLA.modal.cancelar;
				this.processar.label = this.litSLA.modal.ok;
				this.sim.label       = this.litSLA.sim;
				this.nao.label       = this.litSLA.nao;
			})
	}

	ngOnInit() {
		this.getConsultasAProcessar();
		this.getTpContratoFilter();
		this.getTpContratoSLA();
		this.getAreaSolicFilter();
		this.getAreaSolicGraph();
		this.getChartFluxJur();
		this.createFormFluxoDept(new FiltroGraphFluxo());
		this.createFormAreaSolic(new FiltroGraphArea());
	}

	/**
	 * Responsável por obter os dados inputados
	 * no formulário de filtros do gráfico de fluxo do jurídico
	 * @param fluxoJuridico classe FiltroGraphFluxo
	 */
	createFormFluxoDept(fluxoJuridico: FiltroGraphFluxo) {
		this.formFluxoDept = this.formBuilder.group({
			tpContrato:   [fluxoJuridico.tpContrato  ],
			areaSolic:    [fluxoJuridico.areaSolic   ],
			dataInicial:  [fluxoJuridico.dataInicial ],
			dataFinal:    [fluxoJuridico.dataFinal   ]
		});
	}

	/**
	 * Responsável por obter os dados inputados
	 * no formulário de filtros do gráfico da Área Solicitante
	 * @param areaSolic classe FiltroGraphArea
	 */
	createFormAreaSolic(areaSolic: FiltroGraphArea) {
		this.formAreaSolic = this.formBuilder.group({
			tpContrato: [areaSolic.tpContrato]
		});
	}

	/**
	 * Função responsável por obter o conteúdo do campo de filtro
	 * Área do Solicitante.
	 */
	getAreaSolicFilter() {
		this.listAreaSolic = [];

		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA038");
		this.fwmodel.setFilter("NRB_ATIVO='1' AND NRB_FLUIG='1'");
		this.fwmodel.get('Get Área Solitante (Filtro)').subscribe((data) => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != '') {
				data.resources.forEach(item => {
					const areaSolic = new GraficosPadrao;
					areaSolic.label = this.JuriGenerics.findValueByName(item.models[0].fields, "NRB_DESC");
					areaSolic.value = this.JuriGenerics.findValueByName(item.models[0].fields, "NRB_COD");
					this.listAreaSolic.push( areaSolic );
				});
			}
		})
	}
	
	/**
	 * Função responsável por obter o conteúdo do campo de filtro
	 * Tipo de Contrato.
	 */
	getTpContratoFilter() {
		this.listTpContrato = [];
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA134");
		this.fwmodel.get('Get Tipo de Contrato (Filtro)').subscribe((data) => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != '') {
				data.resources.forEach(item => {
					const tipoContrato  = new GraficosPadrao;
					tipoContrato.label  = this.JuriGenerics.findValueByName(item.models[0].fields, "NY0_DESC");
					tipoContrato.value  = this.JuriGenerics.findValueByName(item.models[0].fields, "NY0_COD");
					this.listTpContrato.push( tipoContrato );
				});
			}
		})
	}

	/**
	 * Função responsável para alimentar o gráfico 
	 * do fluxo do dept. jurídico
	 * @param isFiltro - Chamada desta função, vem do filtrar?
	 */
	getChartFluxJur(isFiltro: boolean = false) {
		let dtIni: string       = '';
		let dtFin: string       = '';
		this.isHideLoadGraphFlx = false;

		this.jurConsultas.restore();
		this.jurConsultas.setParam('assJur', '006');

		if (isFiltro) {
			let formFluxo = this.formFluxoDept.value

			if (this.JuriGenerics.changeUndefinedToEmpty(formFluxo.dataInicial) != '') {
				dtIni = this.JuriGenerics.makeDate(formFluxo.dataInicial.replace(/-/g,''),"yyyymmdd");
			}

			if (this.JuriGenerics.changeUndefinedToEmpty(formFluxo.dataFinal) != '') {
				dtFin = this.JuriGenerics.makeDate(formFluxo.dataFinal.replace(/-/g,''),"yyyymmdd");
			}

			this.jurConsultas.setParam('dtInicial', dtIni);
			this.jurConsultas.setParam('dtFinal', dtFin);
			this.jurConsultas.setParam('tpContrato', formFluxo.tpContrato);
			this.jurConsultas.setParam('areaSolic', formFluxo.areaSolic);
		}

		this.jurConsultas.get('slaChartFluxo','Busca de dados do Fluxo do Jurídico').subscribe( data => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.tpContrato) != '') {
				let nAuxTpContrato: number        = 0;
				let listAuxTpCont: Array<GraficosPadrao>  = [];
				this.barChartFluxoData            = [];
				this.barChartFluxoLabels          = [];
				this.barChartFluxoLegend          = true;
				this.barChartFluxoOptions.legend  = { display: true, position: 'bottom' }
				
				data.tpContrato.forEach(item => {
					const tipoContrato  = new GraficosPadrao;
					tipoContrato.label  = item.descTpCont;
					tipoContrato.value  = item.codTpCont;
					tipoContrato.indice = nAuxTpContrato
					listAuxTpCont.push( tipoContrato );

					nAuxTpContrato ++;
					this.barChartFluxoLabels.push(item.descTpCont);
				});

				if (this.JuriGenerics.changeUndefinedToEmpty(data.atividades) != '') {
					let index: number = 0;
					let listData: number[] = [];

					for (index; index < nAuxTpContrato; index++) {
						listData.push(0)
					}

					data.atividades.forEach(ativ => {
						this.barChartFluxoData.push(
							{
								data: [listData],
								label: ativ.descAtividade
							}
						);
					})

					if (this.JuriGenerics.changeUndefinedToEmpty(data.graphFluxoJur) != '') {
						let nPosDtFluxo:   number = -1;
						let nPosLblTpCont: number = -1;
	
						data.graphFluxoJur.forEach(item => {
							nPosDtFluxo = this.barChartFluxoData.findIndex(x => x.label == item.descFluxo);
							nPosLblTpCont = listAuxTpCont.findIndex(x => x.label == item.descTpCont);
							
							if (nPosDtFluxo > -1 && nPosLblTpCont > -1) {
								this.barChartFluxoData[nPosDtFluxo].data[listAuxTpCont[nPosLblTpCont].indice] = item.horas;
							}
						});
						
						this.isHideLoadGraphFlx = true;
					} else {
						this.isHideLoadGraphFlx = true;
					}
				} else {
					this.barChartFluxoLegend = false;
					this.isHideLoadGraphFlx  = true;
					this.barChartFluxoLabels = [];
					this.barChartFluxoData   = [{data:[]}];
				}
			} else {
				this.barChartFluxoLegend = false;
				this.isHideLoadGraphFlx  = true;
				this.barChartFluxoLabels = [];
				this.barChartFluxoData   = [{data:[]}];
			}
		})
	}

	/**
	 * Função que obtem os últimos 6 meses para montagem do gráfico
	 * da área do solicitante
	 */
	getLabelMesesArea() {
		this.barChartAreaLabels = [];
		this.listPeriodosArea   = [];
		let periodo:    string  = '';
		let nIntervalo: number  = 5;

		while (nIntervalo >= 0) {
			periodo = this.JuriGenerics.jurAddDate( '' , 'M' , ( nIntervalo * -1) );
			let mes: string = periodo.substring(2,4);
			let ano: string = periodo.substring(4);
			periodo         = periodo.substring(2);

			this.listPeriodosArea.push(periodo);
			this.barChartAreaLabels.push(this.JuriGenerics.getStrMonth(mes, true) + "/" + ano);
			nIntervalo --;
		}
	}
	
	/**
	 * Função responsável por criar as linhas do gráfico
	 * da Área solicitante.
	 * 
	 * @param isFiltro - Chamada desta função, vem do filtrar?
	 */
	getAreaSolicGraph(isFiltro: boolean = false) {
		this.isHideLoadGraphArea = false;
		this.getLabelMesesArea();
		this.jurConsultas.restore();

		this.jurConsultas.setParam('assJur', '006');

		if (isFiltro) {
			let formArea = this.formAreaSolic.value
			this.jurConsultas.setParam('tpContrato', formArea.tpContrato);
		}

		this.jurConsultas.get('slaAreaSolic','Get Tipo de Contrato SLA').subscribe( data => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.areaSolicSLA) != '') {
				this.barChartAreaData           = [];
				this.barChartAreaLegend         = true;
				this.barChartAreaOptions.legend = { display: true, position: 'bottom' }

				data.areaSolicSLA.forEach(item => {
					this.barChartAreaData.push(
						{
							data: [0,0,0,0,0,0],
							label: item.desAreaSolic,
							backgroundColor: 'rgba(0, 0, 0, 0)'
						}
					);
				});

				if (!isFiltro) {
					this.getChartAreaInfo();
				}
			} else {
				this.barChartAreaLegend  = false;
				this.barChartAreaData    = [{data:[]}];
				this.isHideLoadGraphArea = true;
			}
		});
	};

	/**
	 * Função responsável por alimentar o gráfico da Área Solicitante
	 * @param isFiltro - Chamada desta função, vem do filtrar?
	 */
	getChartAreaInfo(isFiltro: boolean = false) {
		let nPosAreaSolic: number = -1;
		
		if (isFiltro) {
			let formArea = this.formAreaSolic.value;

			this.getAreaSolicGraph(true);
			this.jurConsultas.restore();
			this.jurConsultas.setParam('assJur', '006');
			this.jurConsultas.setParam('tpContrato', formArea.tpContrato);
		} else {
			this.jurConsultas.restore();
			this.jurConsultas.setParam('assJur', '006');
		}

		this.jurConsultas.get('slaChartArea','Get Grafico SLA').subscribe((data) => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data.graphAreaSolic) != ''){
				this.barChartAreaLabels = []
				let nCount: number     = 0;
				data.graphAreaSolic.forEach(item => {
					let mes: string = this.listPeriodosArea[nCount].substring(0,2);
					let ano: string = this.listPeriodosArea[nCount].substring(2);
	
					this.barChartAreaLabels.push(this.JuriGenerics.getStrMonth(mes, true) + "/" + ano);

					item[this.listPeriodosArea[nCount]].forEach(dados => {
						nPosAreaSolic = this.barChartAreaData.findIndex(x => x.label == dados.descAreaSolic);
						if (nPosAreaSolic > -1) {
							this.barChartAreaData[nPosAreaSolic].data[nCount] = dados.horas;
						}
					});

					nCount++;
				});
				this.isHideLoadGraphArea = true;
			} else {
				this.isHideLoadGraphArea = true;
			}
		});
	}

	/**
	 * Função responsável por cadastrar as linhas de tipo de contrato
	 */
	async getTpContratoSLA () {
		this.getLabelMesesSLA();

		this.jurConsultas.restore();
		this.jurConsultas.setParam('assJur', '006');
		let data = await this.jurConsultas.get('slaTpContr','Get Tipo de Contrato SLA').toPromise();
		if (this.JuriGenerics.changeUndefinedToEmpty(data.tpContratoSLA) != '') {
			this.barChartSLAData           = [];
			this.barChartSLALegend         = true;
			this.isHideLoadGraphSLA        = false;
			this.barChartSLAOptions.legend = { display: true, position: 'bottom' }

			data.tpContratoSLA.forEach(item => {
				this.barChartSLAData.push(
					{
						data: [0,0,0,0,0,0,0,0,0,0,0,0],
						label: item.tpContrato,
						backgroundColor: 'rgba(0, 0, 0, 0)'
					}
				);
			});
			this.getChartSLAInfo();
		}
	};

	/**
	 * Função que obtem os últimos 12 meses para montagem do gráfico do SLA
	 */
	getLabelMesesSLA() {
		this.barChartSLALabels = [];
		this.listPeriodosSLA      = [];
		let periodo:    string = '';
		let nIntervalo: number = 11;

		while (nIntervalo >= 0) {
			periodo = this.JuriGenerics.jurAddDate( '' , 'M' , ( nIntervalo * -1) );
			let mes: string = periodo.substring(2,4);
			let ano: string = periodo.substring(4);
			periodo         = periodo.substring(2);

			this.listPeriodosSLA.push(periodo);
			this.barChartSLALabels.push(this.JuriGenerics.getStrMonth(mes, true) + "/" + ano);
			nIntervalo --;
		}
	}

	/**
	 * método GET (grafSLA) WS REST JurConsultas
	 * consulta o SLA Jurídico em dias dos últimos 12 meses
	 */
	getChartSLAInfo() {
		let nPosTpContrato: number = -1;
		this.jurConsultas.restore();
		this.jurConsultas.setParam('assJur', '006');
		this.jurConsultas.get('slaChartData','Get Grafico SLA').subscribe((data) => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data.sla) != ''){
				this.barChartSLALabels = []
				let nCount: number     = 0;
				data.sla.forEach(item => {
					let mes: string = this.listPeriodosSLA[nCount].substring(0,2);
					let ano: string = this.listPeriodosSLA[nCount].substring(2);
	
					this.barChartSLALabels.push(this.JuriGenerics.getStrMonth(mes, true) + "/" + ano);

					item[this.listPeriodosSLA[nCount]].forEach(dados => {
						nPosTpContrato = this.barChartSLAData.findIndex(x => x.label == dados.tipoContrato);
						if (nPosTpContrato > -1) {
							let num: number = dados.dias;
							this.barChartSLAData[nPosTpContrato].data[nCount] = num;
						}
					});

					nCount++;
				});
				this.isHideLoadGraphSLA = true;
			}
		});
	}

	/**
	 * Busca a quantidade de workflows que foram finalizados e não foram processados,
	 * para calcular o SLA
	 */
	getConsultasAProcessar(){
		this.isDisabledProcessar = false;
		// Monta a frase de quantidade de consultas a serem processadas
		this.processaWF = this.poI18nPipe.transform(
							this.litSLA.processaWF.infoProcessar,
							[this.listConsultasProcessar.length]
						) + this.litSLA.processaWF.carregaInfo
		this.isDisabledBtnProcessar = true;

		this.getHistoryWF().then(() => {
			if (this.isNext){
				this.getConsultasAProcessar();
			}else{
				this.processaWF = this.poI18nPipe.transform(
									this.litSLA.processaWF.infoProcessar,
									[this.listConsultasProcessar.length]
								) + this.litSLA.processaWF.infoAgora
				this.isDisabledBtnProcessar = false
				this.nProgressTotal = this.listConsultasProcessar.length;
				this.restoreBusca();
			}
		})
		
	}

	/**
	 * método GET (getWfSLAJur) WS REST JurConsultas
	 * Busca de forma paginada as consultas finalizadas que ainda não foram processadas, 
	 * para o calculo de SLA.
	 */
	async getHistoryWF(){
		this.jurConsultas.restore();
		this.jurConsultas.setParam('assJur'  , '006');
		this.jurConsultas.setParam('page'    , this.nPagina.toString());
		this.jurConsultas.setParam('pageSize', '10');

		let data  = await this.jurConsultas
						.get('getHistoryWF',
							'Get de consultas que foram finalizadas e não foram processadas')
						.toPromise()
		if (this.JuriGenerics.changeUndefinedToEmpty(data)!= '') {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.getWorkflow) != '') {
				data.getWorkflow.forEach(item => {
					let dadosHist = new Historico();
					dadosHist = {
						filial: item.filcon,
						codConsulta: item.codigo
					}
					this.listConsultasProcessar.push(dadosHist);
				})
			}
			this.isNext = (data.hasNext == 'true' ? true : false)
			// Se ainda houverem consultas a serem verificadas
			if (this.isNext) {
				this.nPagina++
			} else {
				this.nPagina = 0
			}
			if (!this.isNext && this.listConsultasProcessar.length == 0) {
				this.isDisabledProcessar = true;
			}

			if (this.nProcessErr > 0 && !this.isNext) {
				this.modalIgnoraWF.open();
				this.isProcessErr = true;
				this.msgIgnorarWf = this.poI18nPipe.transform(
										this.litSLA.modal.msgIgnorarWf,
										[this.listProcessErr.length]);
			}
		} else {
			this.isNext = false;
		}
	}

	/**
	 * Restaura as variáveis de busca das consultas finalizadas que ainda não foram processadas
	 */
	restoreBusca(){
		this.nPagina = 0;
		this.isNext = false;
	}

	/**
	 * Responsável por processar os WF que não estão com o histórico gravados
	 */
	processarHistorico(){
		let nCount = 0;
		this.isDisabledBtnProcessar = true;
		this.isShowProgressBar = true;
		this.listProcessErr = [];
		this.nProcessErr = 0;
		this.listConsultasProcessar.forEach(item => {
			
			if (this.JuriGenerics.changeUndefinedToEmpty(item.filial) != '' &&
				 this.JuriGenerics.changeUndefinedToEmpty(item.codConsulta) != ''){
				
				this.processHist(item.filial, item.codConsulta).then(() => {
					nCount ++
					//Valor processado da barra de progresso
					this.nProgressProcHist = Math.round(((this.nProgressTotal - (this.nProgressTotal - nCount)) * 100) / this.nProgressTotal) 

					if (nCount == this.nProgressTotal){
						this.isShowProgressBar = false;
						this.restoreListConsultas();
						this.getTpContratoSLA();
						this.getAreaSolicGraph();
						this.getChartFluxJur();
					}
				})
			}
		});

	}

	/**
	 * Limpa a lista de consultas e faz a verificação novamente
	 */
	restoreListConsultas(){
		this.listConsultasProcessar = [];
		this.getConsultasAProcessar();
	}
	
	/**
	 * Realiza o processamento do histórico 
	 * @param filial      filial da consulta
	 * @param codConsulta cajuri
	 */
	async processHist(filial, codConsulta){
		this.jurConsultas.restore();
		this.jurConsultas.setParam('filConsulta', filial);
		this.jurConsultas.setParam('cajuri', codConsulta);

		//Assume a filial da consulta em que será incluido o histórico
		window.sessionStorage.setItem('filial', filial);
		
		let data = await this.jurConsultas.get('processHistory','Processa historico').toPromise()
		if(this.JuriGenerics.changeUndefinedToEmpty(data.historicoWF) != ''){
			if (!data.lRet){
				this.nProcessErr ++;
				let dadosErro = new Historico();
				dadosErro = {
					filial: filial,
					codConsulta: codConsulta
				}
				this.listProcessErr.push(dadosErro);
			}
		}
	}

	/**
	 * Inicializa a gravação das consultas que possuem o Workflow inconsistente no Fluig 
	 */
	processarWFIncons(){
		let nTotal = this.listConsultasProcessar.length;
		let nCount = 0;
		this.isShowProgBarIncons = true;

		this.listConsultasProcessar.forEach(item => {

			if (this.JuriGenerics.changeUndefinedToEmpty(item.filial) != '' &&
				this.JuriGenerics.changeUndefinedToEmpty(item.codConsulta) != ''){
				this.gravaWFIncons(item.filial, item.codConsulta).then(() => {
					nCount ++
					//Valor processado da barra de progresso
					this.nProgressWFIncons = Math.round(((nTotal - (nTotal - nCount)) * 100) / nTotal)

					if (nCount == this.nProgressTotal){
						this.isShowProgBarIncons = false;
						this.isProcessErr = false;
						this.restoreListConsultas();
						if(this.nInconsErr > 0){
							this.poNotification.information(this.litSLA.infoErro);
						}
					}
				})
			}
		})
	}

	/**
	 * Realiza a gravação das consultas que possuem o Workflow inconsistente no Fluig,
	 * zeradas na tabela de SLA Jurídico
	 * @param filial      filial da consulta
	 * @param codConsulta cajuri
	 */
	async gravaWFIncons(filial, codConsulta){
		this.jurConsultas.restore();
		this.jurConsultas.setParam('filConsulta', filial);
		this.jurConsultas.setParam('cajuri', codConsulta);
		
		//Assume a filial da consulta em que será incluido o histórico
		window.sessionStorage.setItem('filial', filial);
		
		let data = await this.jurConsultas.get('procWFInconsis',
												'Processa as consultas que possuem o Workflow inconsistente')
											.toPromise()
		if(this.JuriGenerics.changeUndefinedToEmpty(data.WFInconsistente) != ''){
			if (!data.lRet){
				this.nInconsErr++;
			} else {
				this.nProcessErr--;
			}
		}
	}
}