/**
 * export classe do Formulário de Filtros do SLA.
 */
export class FiltroGraphFluxo {
	tpContrato:   string = '';
	areaSolic:    string = '';
	dataInicial:  string = '';
	dataFinal:    string = '';
}

/**
 * export classe do Formulário de Filtros do SLA.
 */
export class FiltroGraphArea {
	tpContrato:   string = '';
}