import { Component, OnInit, ViewChild } from '@angular/core';
import { FwmodelService, FwModelBody } from 'src/app/services/fwmodel.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Tarefa } from './det-tarefa';
import { TLUserInfo } from 'src/app/services/authlogin.service';
import { JurigenericsService, JurUTF8 } from 'src/app/services/jurigenerics.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PoNotificationService, PoModalAction, PoModalComponent, PoI18nService, PoBreadcrumb, PoUploadComponent } from '@po-ui/ng-components';
import { RoutingService } from 'src/app/services/routing.service';
import { FwmodelAdaptorService, adaptorFilterParam, adaptorReturnStruct } from 'src/app/services/fwmodel-adaptor.service';
import { JurJustifModalComponent } from 'src/app/shared/jur-justif-modal/jur-justif-modal.component';
import { SysProtheusParam, sysParamStruct } from 'src/app/services/legalprocess.service';
import { InstanciaService, RetDadosInstancia } from 'src/app/services/instancia.service';
import { TpTarefaService } from 'src/app/services/combos-fwmodel-adapter.service'
import { ParticipanteAdapterService } from 'src/app/services/combos-fwmodel-adapter.service'
import { PrepostoAdapterService } from 'src/app/services/combos-fwmodel-adapter.service'
import { StatusTarefaService } from 'src/app/services/combos-fwmodel-adapter.service'

class DadosPart {
	id: string;
	value: string;
	label: string;
}

@Component({
	selector: 'app-det-tarefa',
	templateUrl: './det-tarefa.component.html',
	styleUrls: ['./det-tarefa.component.css']
})
export class DetTarefaComponent implements OnInit {

	litDetTarefa: any;
	dadosResp: any;
	DadosPart: DadosPart = new DadosPart();
	callbackOk: `Function`;
	formTarefa: FormGroup;

	codInst: string;
	titleDet: string = '';
	sigla: string = '';
	cPart: string = '';
	cajuri: string = '';
	codAnd: string = '';
	pkAnd: string = '';
	pkFup: string = '';
	codTar: string = '';
	descAnd: string = '';
	lblBtnSalvar: string = '';
	lblBtnExcluir: string = '';
	descTipo: string = '';
	chaveProcesso: string = '';
	filial: string = '';
	perfil: string = '/processo/';
	breadcrumbPerfil: string = ''
	breadcrumbHome: string = ''
	linkHome: string = ''
	cajuriTitulo: string = ''
	filtroResp: string = "RD0_TPJUR = '1' AND RD0_SIGLA <> ''"

	participantes: Array<DadosPart> = [];
	sysParams: Array<sysParamStruct> = []

	uploadSuccessCount: number = 0;
	uploadCount: number = 0;

	bOperacaoInclusao: boolean = false;
	isHideLoadP: boolean = false;
	loadingTarefa: boolean = false;
	isProcEncerrado: boolean = false;
	isProcesso: boolean = false;
	canChangeDesc: boolean = false;

	public breadcrumb: PoBreadcrumb = {
		items: []
	};

	// ações do modal de confirmação de exclusão (Garantia)
	fechar: PoModalAction = {
		action: () => {
			this.thfModal.close();
		},
		label: 'Fechar',
	};

	excluir: PoModalAction = {
		action: () => {
			this.deleteVerify();
		},
		label: 'Excluir',
	};

	@ViewChild('excluiTarefa', { static: true }) thfModal: PoModalComponent;
	@ViewChild('docUpload') docUpload: PoUploadComponent;
	@ViewChild('modJustif', { static: true }) modalJustif: JurJustifModalComponent;

	constructor(
		private fwmodel: FwmodelService,
		private formBuilder: FormBuilder,
		private userInfo: TLUserInfo,
		private JuriGenerics: JurigenericsService,
		public poNotification: PoNotificationService,
		private route: ActivatedRoute,
		private routingState: RoutingService,
		private thfI18nService: PoI18nService,
		private router: Router,
		private sysProtheusParam: SysProtheusParam,
		public fwModelAdaptorService: FwmodelAdaptorService,
		private instanciaService: InstanciaService,
		public tpTarefaService: TpTarefaService,
		public participanteAdapterService: ParticipanteAdapterService,
		public prepostoAdapterService: PrepostoAdapterService,
		public statusTarefaService: StatusTarefaService
	) {

		// Setup de adapters
		this.tpTarefaService.setup(new adaptorFilterParam("JURA021", "", "NQS_DESC", "NQS_DESC", "NQS_DESC", "NQS_COD", "", "Busca tipo", undefined, undefined, undefined, { bGetAllLevels: false, bUseEmptyField: false, bUseVirtualField: false, arrSetFields: ["NQS_COD", "NQS_DESC", "NQS_DESPAD"] }, "NQS_DESPAD"));
		this.participanteAdapterService.setup(new adaptorFilterParam("JURA159", "RD0_TPJUR = '1' AND RD0_SIGLA <> ''", "RD0_NOME", "RD0_NOME", "RD0_NOME", "RD0_SIGLA", "", "Busca responsavel"));
		this.prepostoAdapterService.setup(new adaptorFilterParam("JURA016", "", "NQM_DESC", "NQM_DESC", "NQM_DESC", "NQM_COD", "", "Busca  preposto"));
		this.statusTarefaService.setup(new adaptorFilterParam("JURA017", "", "NQN_DESC", "NQN_DESC", "NQN_DESC", "NQN_COD", "", "Busca status"))

		routingState.loadRouting();

		if (this.JuriGenerics.changeUndefinedToEmpty(this.route.snapshot.paramMap.get("codAnd")) != "") {
			this.codAnd = atob(decodeURIComponent(this.route.snapshot.paramMap.get("codAnd")));
		}

		if (this.JuriGenerics.changeUndefinedToEmpty(this.route.snapshot.paramMap.get("codTar")) != "") {
			this.codTar = atob(decodeURIComponent(this.route.snapshot.paramMap.get('codTar')));
		}

		if (this.route.snapshot.queryParamMap.get("codInst") != undefined) {
			this.codInst = atob(decodeURIComponent(this.route.snapshot.queryParamMap.get("codInst")));
		}


		if (this.router.url.indexOf('/contrato/') > -1) {
			this.filial = this.route.snapshot.paramMap.get('filContrato');
			window.sessionStorage.setItem('filial', atob(this.filial));

			this.chaveProcesso = this.route.snapshot.paramMap.get('codContrato');
			this.cajuri = atob(decodeURIComponent(this.chaveProcesso));
			this.isProcesso = false;
		} else {
			this.filial = this.route.snapshot.paramMap.get('filPro');
			window.sessionStorage.setItem('filial', atob(this.filial));

			this.chaveProcesso = this.route.snapshot.paramMap.get('codPro');
			this.cajuri = atob(decodeURIComponent(this.chaveProcesso));
			this.isProcesso = true;
		}

		// Traduções
		thfI18nService.getLiterals({ context: 'detTarefa', language: thfI18nService.getLanguage() })
			.subscribe((litDetTarefa) => {
				this.litDetTarefa = litDetTarefa;
				const lAndamento = document.location.pathname.search('andamento');

				//-- Adiciona o código do cajuri no título Meu processo + área
				this.cajuriTitulo = this.cajuri.replace(new RegExp('^0*', 'g'), '')

				//Verifica se é um processo ou contrato
				if (this.isProcesso) {
					this.perfil = '/processo/';
					this.breadcrumbPerfil = this.litDetTarefa.breadCrumb.processo
					this.breadcrumbHome = this.litDetTarefa.breadCrumb.home
					this.linkHome = '/home'
				} else {
					this.perfil = '/contrato/';
					this.breadcrumbPerfil = this.litDetTarefa.breadCrumb.contrato
					this.breadcrumbHome = this.litDetTarefa.breadCrumb.homeContrato
					this.linkHome = '/homeContratos'
				}

				this.breadcrumb.items = [
					{ label: this.breadcrumbHome, link: this.linkHome },
					{ label: this.breadcrumbPerfil, link: this.perfil + this.filial + '/' + this.chaveProcesso }
				]

				//Quando não tem andamento é tarefa
				if (lAndamento < 0) {
					this.lblBtnExcluir = this.litDetTarefa.btnExcluirTarefa;

					//Nova tarefa
					if (this.codTar.length < 5) {
						this.titleDet = this.litDetTarefa.tituloNovaTarefa;
						this.lblBtnSalvar = this.litDetTarefa.btnIncluirTarefa;
						this.bOperacaoInclusao = true;

						this.breadcrumb.items = [... this.breadcrumb.items,
						{ label: this.litDetTarefa.breadCrumb.tarefas, link: this.perfil + this.filial + '/' + this.chaveProcesso + '/tarefa' },
						{ label: this.titleDet }
						];
						//Atualização	
					} else {
						this.titleDet = this.litDetTarefa.tituloAlterarTarefa;
						this.lblBtnSalvar = this.litDetTarefa.btnSalvarTarefa;

						this.breadcrumb.items = [... this.breadcrumb.items,
						{ label: this.litDetTarefa.breadCrumb.tarefas, link: this.perfil + this.filial + '/' + this.chaveProcesso + '/tarefa' },
						{ label: this.titleDet }
						];

					}

					//Verifica se é Prazo
				} else {
					this.lblBtnExcluir = this.litDetTarefa.btnExcluirPrazo;

					this.breadcrumb.items.push({ label: this.litDetTarefa.breadCrumb.andamentos, link: this.perfil + this.filial + '/' + this.chaveProcesso + '/andamento/' })

					if (this.hasInstancia()) {
						this.breadcrumb.items = [...this.breadcrumb.items,
						{
							label: this.litDetTarefa.breadCrumb.andamentos.instancia,
							action: () => {
								this.router
									.navigate(
										[this.perfil + this.filial + '/' + this.chaveProcesso + '/andamento/'],
										{ queryParamsHandling: 'merge' }
									);
							}
						},
						{
							label: this.litDetTarefa.breadCrumb.andamento,
							action: () => {
								this.router
									.navigate(
										[this.perfil + this.filial + '/' + this.chaveProcesso + '/andamento/' + btoa(this.codAnd)],
										{ queryParamsHandling: 'merge' }
									);
							}
						}
						];
					} else {
						this.breadcrumb.items.push({ label: this.litDetTarefa.breadCrumb.andamento, link: this.perfil + this.filial + '/' + this.chaveProcesso + '/andamento/' + btoa(this.codAnd) })
					}

					// Novo Prazo
					if (this.codTar.length < 5) {
						this.titleDet = this.litDetTarefa.tituloNovoPrazo;
						this.lblBtnSalvar = this.litDetTarefa.btnIncluirPrazo;
						this.bOperacaoInclusao = true;

						// Alterar Prazo
					} else {
						this.titleDet = this.litDetTarefa.tituloAlterarPrazo;
						this.lblBtnSalvar = this.litDetTarefa.btnSalvarPrazo;
					}
					this.breadcrumb.items.push({ label: this.titleDet })
				}
			});
	}


	ngOnInit() {
		this.canChangeDesc = false;

		this.createFormPrazos(new Tarefa());
		this.getResponsavel();
		this.openTarefa();
		this.getInfoProcesso();
		this.getInfoInstancia();

		if (this.codAnd) {
			this.getAndamentos();
		}

		//se tiver tarefa busca os dados 
		if (this.codTar.length > 5) {
			this.getTarefa();
		} else { //se inclusão, deixa o status como pendente
			this.canChangeDesc = true;
			this.setTarefaPendente()
		}

		// Busca os parâmetros do Protheus
		this.sysProtheusParam.getParamByArray(["MV_JTVENJF"]).subscribe((params) => {
			this.sysParams = params;
		})

	}

	/**
	 * Responsável por obter os dados inputados
	 * no formulário de inclusão de Tarefa
	 */
	createFormPrazos(prazos: Tarefa) {
		this.formTarefa = this.formBuilder.group({
			dataPrazo: [new Date(), Validators.compose([Validators.required])],
			tipoPrazo: [prazos.tipoPrazo, Validators.compose([Validators.required])],
			statusPrazo: [prazos.statusPrazo, Validators.compose([Validators.required])],
			respPrazo: [prazos.respPrazo, Validators.compose([Validators.required])],
			prepPrazo: [prazos.prepPrazo],
			descPrazo: [prazos.descPrazo],
			codAndamento: [prazos.codAndamento],
			hora: [prazos.hora]
		});
	}

	/**
	 *  Responsável por trazer os Participantes / Resposáveis de Follow-ups
	 */
	getResponsavel() {
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA159');
		this.fwmodel.setFilter("RD0_TPJUR = '1'");
		this.fwmodel.setFilter("RD0_SIGLA <> ''");
		this.fwmodel.setPageSize('999');
		this.fwmodel.get('Busca de participantes').subscribe((data) => {
			data.resources.forEach(itensPart => {

				this.DadosPart = new DadosPart();
				this.DadosPart.id = this.JuriGenerics.findValueByName(itensPart.models[0].fields, 'RD0_CODIGO');
				this.DadosPart.value = this.JuriGenerics.findValueByName(itensPart.models[0].fields, 'RD0_SIGLA');
				this.DadosPart.label = this.JuriGenerics.findValueByName(itensPart.models[0].fields, 'RD0_NOME');
				this.participantes.push(this.DadosPart);
			});
		});
	}


	/**
		*  Responsável por carregar os combos para a Tarefa
		*/
	async openTarefa(func?: `Function`) {
		this.formTarefa.patchValue({ dataPrazo: new Date() });
		this.getResponsavel(); // Responsavel / Participante do Follow Up

		if (func != undefined) {
			this.callbackOk = func;
		}
	}

	/**
	 * Função de inicio das validações e Commit.
	 */
	confirmVerify() {
		if (this.beforeSubmit()) {
			this.submitPrazo()
		}
	}

	/**
	 * Função de validação e delete.
	 */
	deleteVerify() {
		if (this.beforeDelete()) {
			this.delTarefa()
		}
	}

	beforeSubmit() {
		let isSubmitable: boolean = false;
		isSubmitable = this.formTarefa.valid;

		// Validação para apresentar o Modal de Justificativa, caso o processo esteja encerrado
		if (isSubmitable) {
			if (this.isProcEncerrado && this.sysProtheusParam.getParamValueByName(this.sysParams, "MV_JTVENJF") == "1") {
				isSubmitable = false;
				this.modalJustif.openModal(this.cajuri,
					() => {
						if (this.modalJustif.isOk) {
							this.submitPrazo()
						}
					});
			} else {
				isSubmitable = true;
			}
		}

		return isSubmitable
	}

	beforeDelete() {
		let isSubmitable: boolean = false;
		isSubmitable = this.formTarefa.valid;

		// Validação para apresentar o Modal de Justificativa, caso o processo esteja encerrado
		if (isSubmitable) {
			if (this.isProcEncerrado && this.sysProtheusParam.getParamValueByName(this.sysParams, "MV_JTVENJF") == "1") {
				isSubmitable = false;
				this.modalJustif.openModal(this.cajuri,
					() => {
						if (this.modalJustif.isOk) {
							this.delTarefa()
						}
					});
			} else {
				isSubmitable = true;
			}
		}

		return isSubmitable
	}

	/**
	 * Obtem os dados do formulário no momento da inclusão e passa como parâmetro
	 * para o postPrazo. Esses dados serão usados no body da requisição
	 * para realizar a inclusão através do FWModel.
	 */
	submitPrazo() {
		this.loadingTarefa = true;
		if (this.codTar.length < 5) {
			this.insertTarefa()
		} else {
			this.updateTarefa()
		}
	}

	/**
	 * Cancela operação
	 */
	onCancel() {
		this.routingState.navigateToPreviousPage(btoa(this.codTar), { queryParamsHandling: 'merge' })
	}

	/**
		* Busca as informações do processo
		*/
	getInfoProcesso() {
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA095');
		this.fwmodel.setFilter("NSZ_FILIAL='" + atob(this.filial) + "'");
		this.fwmodel.setFilter("NSZ_COD='" + this.cajuri + "'");
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.get('getInfoProcesso').subscribe(data => {
			let descResumoBreadCrumb = []
			if (this.isProcesso) {
				descResumoBreadCrumb = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NSZ_DAREAJ")
			} else {
				descResumoBreadCrumb = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NSZ_DESCON")
			}
			if (data.resources != '') {
				this.breadcrumb.items[1].label = this.breadcrumbPerfil + ' '
					+ descResumoBreadCrumb + " (" + this.cajuriTitulo + ")";
				this.isProcEncerrado = (this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NSZ_SITUAC") == "2")
			}
		});
	}

	/**
	 * Responsavel por remover uma tarefa
	 */
	delTarefa() {
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA106');
		this.fwmodel
			.delete(this.pkFup, "Deleta tarefa")
			.subscribe(data => {
				if (data == true) {
					this.poNotification.success(this.litDetTarefa.msgExclusao);
					this.routingState.navigateToPreviousPage(encodeURIComponent(btoa(this.codTar)), { queryParamsHandling: "merge" })
					this.thfModal.close();
				}
			})
	}

	/**
	 * Responsável por obter os dados do andamento
	 */
	getAndamentos() {
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA100');
		this.fwmodel.setFilter("NT4_COD='" + this.codAnd + "'");
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.get("Busca Andamentos").subscribe((data) => {
			if (data.count === 0) {
				this.poNotification.error(this.litDetTarefa.naoEncontrado);
			} else {
				this.pkAnd = data.resources[0].pk;
				this.cajuri = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, 'NT4_CAJURI');
				this.descAnd = this.JuriGenerics
					.makeDate(this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, 'NT4_DTANDA'), 'dd/mm/yyyy');

				this.breadcrumb.items[
					this.breadcrumb.items.findIndex(i => i.label == this.litDetTarefa.breadCrumb.andamento)
				].label = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, 'NT4_DATO') +
				' - ' + this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, 'NT4_DTANDA'), 'dd/mm/yyyy');
				return this.pkAnd;
			}
		});
	}

	/**
	 * Responsável por obter os dados da tarefa
	 */
	getTarefa() {
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA106');
		this.fwmodel.setFilter("NTA_COD='" + this.codTar + "'");
		this.fwmodel.setFilter("NTA_CAJURI='" + this.cajuri + "'");
		this.fwmodel.setFilter("NTA_FILIAL='" + atob(this.filial) + "'");

		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.get("Busca de Tarefas").subscribe(data => {
			if (data.count === 0) {
				this.poNotification.error(this.litDetTarefa.naoEncontrado);
			} else {
				this.pkFup = data.resources[0].pk;
				this.descTipo = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NTA_DTIPO")
				this.codAnd = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NTA_CANDAM")

				this.formTarefa.patchValue({ tipoPrazo: this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NTA_CTIPO") }, { emitEvent: false })

				this.formTarefa.patchValue({
					dataPrazo: this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NTA_DTFLWP"), "yyyy-mm-dd"),
					statusPrazo: this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NTA_CRESUL"),
					respPrazo: this.JuriGenerics.findValueByName(data.resources[0].models[0].models[0].items[0].fields, "NTE_SIGLA"),
					prepPrazo: this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NTA_CPREPO"),
					descPrazo: this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NTA_DESC"),
					hora: this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NTA_HORA")
				})

				setTimeout(() => {
					this.canChangeDesc = true
				}, 2000);

				return;
			}
		});
	}

	/**
	 * Faz o insert de uma nova tarefa/Prazo
	 */
	insertTarefa() {
		if (this.userInfo.getNomeUsuario() === '') {
			setTimeout(() => { this.submitPrazo(); }, 500);
		} else {
			if (this.beforeTarefa()) {
				this.isHideLoadP = true;
				this.fwmodel.restore();
				this.fwmodel.setFirstLevel(false)
				this.fwmodel.setModelo('JURA106');
				this.fwmodel.setFilter("NTA_CAJURI='" + this.cajuri + "'");
				this.fwmodel.setFilter("NTA_FILIAL='" + atob(this.filial) + "'");

				this.dadosResp = this.participantes.find(x => x.value === this.formTarefa.value.respPrazo);
				this.cPart = this.dadosResp.id;
				this.sigla = this.dadosResp.value;

				this.fwmodel.post(this.bodyTask(3), "Insere tarefa").subscribe((data) => {
					if (data != '') {
						this.isHideLoadP = false;
						this.codTar = this.JuriGenerics.findValueByName(data.models[0].fields, "NTA_COD")

						// Valida se há documentos a serem enviados
						if (this.docUpload.hasOwnProperty('currentFiles') && this.docUpload.currentFiles.length > 0) {
							this.uploadCount = this.docUpload.currentFiles.length;
							this.sendDocs();
						} else {
							this.poNotification.success(this.litDetTarefa.sucessPrazo);
							this.routingState.navigateToPreviousPage(btoa(this.codTar), { queryParamsHandling: "merge" })
						}

					} else {
						this.isHideLoadP = false;
						this.loadingTarefa = false;
					}
				});
			} else {
				this.loadingTarefa = false;
			}
		}
		return true;
	}

	/**
	 * Altera a tarefa
	 */
	updateTarefa() {
		if (this.userInfo.getNomeUsuario() === '') {
			setTimeout(() => { this.submitPrazo(); }, 500);
		} else {
			this.isHideLoadP = true;

			this.dadosResp = this.participantes.find(x => x.value === this.formTarefa.value.respPrazo);
			this.cPart = this.dadosResp.id;
			this.sigla = this.dadosResp.value;

			this.fwmodel.restore();
			this.fwmodel.setModelo('JURA106');
			this.fwmodel.put(this.pkFup, this.bodyTask(4), "Atualiza tarefa").subscribe((data) => {
				if (data == "") {
					this.isHideLoadP = false;
				} else {

					this.isHideLoadP = false;
					//Valida se há documentos a serem enviados
					if (this.docUpload.hasOwnProperty('currentFiles') && this.docUpload.currentFiles.length > 0) {
						this.uploadCount = this.docUpload.currentFiles.length;
						this.sendDocs();
					} else {
						this.poNotification.success(this.litDetTarefa.sucessUpdate);
						this.routingState.navigateToPreviousPage(btoa(this.codTar), { queryParamsHandling: 'merge' })
					}
				}
				this.loadingTarefa = false;
			})
		}
	}

	/**
	 * Monta o body de tarefas
	 */
	bodyTask(operation: 3 | 4) {
		const fwModelBody: FwModelBody = new FwModelBody("JURA106", operation, this.pkFup, "NTAMASTER")

		fwModelBody.models[0].addField("NTA_CAJURI", this.cajuri, undefined, "C")
		fwModelBody.models[0].addField("NTA_DTFLWP", this.formTarefa.value.dataPrazo, undefined, "D")
		fwModelBody.models[0].addField("NTA_CTIPO", this.formTarefa.value.tipoPrazo, undefined, "C")
		fwModelBody.models[0].addField("NTA_CRESUL", this.formTarefa.value.statusPrazo, undefined, "C")
		fwModelBody.models[0].addField("NTA_CPREPO", this.formTarefa.value.prepPrazo, undefined, "C")
		fwModelBody.models[0].addField("NTA_DESC", this.formTarefa.value.descPrazo, undefined, "C")
		fwModelBody.models[0].addField("NTA_CANDAM", this.formTarefa.value.codAnd, undefined, "C")
		fwModelBody.models[0].addField("NTA_HORA", this.formTarefa.value.hora, undefined, "C")


		fwModelBody.models[0]
			.addModel("NTEDETAIL", "GRID")
			.addItems(1, 0)
			.addField("NTE_SIGLA", this.sigla, "C")

		return fwModelBody.serialize()

	}
	/**
	 * Abre a tela de anexos
	 */
	callAnexos() {
		this.router.navigate(["./anexos"], { relativeTo: this.route, queryParamsHandling: "merge" })
	}
	/**
	 * Realiza o envio dos documentos
	 */
	sendDocs() {
		if (this.docUpload.currentFiles.length > 0) {
			this.docUpload.sendFiles();
		}
	}

	/**
	 * Chama o SelectFiles do po-upload
	 */
	getDocuments() {
		this.docUpload.selectFiles()
	}
	/**
	 * Carrega as informações para o POST do upload de arquivos
	 * @param file 
	 */
	uploadFiles(file) {
		file.data = {
			cajuri: this.cajuri.toString(),
			entidade: "NTA",
			codEntidade: this.codTar.toString()
		};
	}

	/**
	 * Trata para redirecionar após concluir todos os anexos
	 */
	uploadSucess() {
		this.uploadSuccessCount++;

		if (this.uploadSuccessCount == this.docUpload.currentFiles.length) {
			if (this.bOperacaoInclusao) {
				this.poNotification.success(this.litDetTarefa.sucessPrazo);
			} else {
				this.poNotification.success(this.litDetTarefa.sucessUpdate);
			}
			this.routingState.navigateToPreviousPage(btoa(this.codTar), { queryParamsHandling: 'merge' })
		}
	}

	/**
	 * Validação dos campos antes de fazer o submit
	 */
	beforeTarefa() {
		let bOk: boolean = this.formTarefa.valid;//valida os campos obrigatórios
		if (!bOk) {
			this.poNotification.error(this.litDetTarefa.camposObrig);
		}
		return bOk;
	}

	/**
	 * Função que verifica se existe o parametro de instancia
	 */
	hasInstancia() {
		return this.codInst != undefined && this.codInst != null && this.codInst != ''
	}

	/**
	 * Função responsavel pelo preenchimento das informações no breadcrumb de instancia
	 */
	getInfoInstancia() {
		if (this.hasInstancia()) {
			this.instanciaService
				.getDadosInstancia(this.filial + this.chaveProcesso, this.codInst)
				.subscribe(
					(retDadosInst: RetDadosInstancia) => {
						let descInst: string = retDadosInst.instancia
							+ ' - ' + retDadosInst.tpAcao
							+ ' - ' + retDadosInst.nrProcesso;

						this.breadcrumb.items[
							this.breadcrumb.items.findIndex(i => i.label == this.litDetTarefa.breadCrumb.andamentos.instancia)
						].label = descInst
					}
				)

		}
	}

	/**
	 * Função responsavel por setar o status da tarefa que seja do tipo pendente
	 */
	setTarefaPendente() {
		this.fwmodel.restore()
		this.fwmodel.setModelo('JURA017')
		this.fwmodel.setFilter(" NQN_TIPO = '1' ")
		this.fwmodel.setPageSize("1")
		this.fwmodel.setFields(["NQN_COD"])
		this.fwmodel
			.get("Buca tipo tarefa pendente")
			.subscribe(
				(data)=>{
					if (data.count > 0) {
						this.formTarefa.patchValue({ statusPrazo: data.resources[0].models[0].fields[0].value })
					}
				}
			)
		
	}

	/**
	 * Evento de alteração do campo de tipo de tarefa
	 * @param selectedOption Opção selecionada do componente de combo
	 */
	getDetailTarefa(selectedOption: adaptorReturnStruct) {

		if (selectedOption != null && selectedOption != undefined) {
			const desc: string = this.JuriGenerics.findValueByName(selectedOption.getExtras(), "NQS_DESPAD");
			if (this.canChangeDesc && !(desc === "")) {
				this.formTarefa.patchValue({ descPrazo: desc })
			}
		}

	}
}