export class Tarefa {
	dataPrazo:   string = '';
	tipoPrazo:   string = '';
	statusPrazo: string = '';
	respPrazo:   string = '';
	prepPrazo:   string = '';
	descPrazo:   string = '';
	codAndamento: string = '';
	hora: string = '';
}
