/**
* Arquivo de constantes para tradução da tela de Prazos
*/

//-- Constantes para o bread crumb
const breadCrumb = {
	home:         'Meus processos',
	processo:     'Meu processo',
    tarefas:      'Tarefas',
    andamentos:   'Andamentos',
	andamento:    'Andamento',
	contrato:     'Meu contrato',
	homeContrato: 'Meus contratos',
	instancia:    'Instância'
}

//-- Constantes principais
export const detTarefaPt = {

	tituloNovaTarefa:    'Nova Tarefa',
	tituloNovoPrazo:     'Novo Prazo',    
	tituloAlterarTarefa: 'Alterar Tarefa',
	tituloAlterarPrazo:  'Alterar Prazo',
	dataP:               'Data',
	atoP:                'Ato Processual',
	hora:                'Hora',
	tipoP:               'Tipo',
	statusP:             'Status',
	prepostoP:           'Preposto',
	descricaoP:          'Descrição',
	escritorioP:         'Escritório',
	responsavelP:        'Responsável',
	btnIncluirTarefa:    'Incluir Tarefa',
	btnSalvarTarefa:     'Alterar Tarefa',
	btnExcluirTarefa:    'Excluir Tarefa',
	btnIncluirPrazo:     'Incluir Prazo',
	btnSalvarPrazo:      'Alterar Prazo',
	btnExcluirPrazo:     'Excluir Prazo',
	btnCancelar:         'Cancelar',
	naoEncontrado:       'Esse andamento não foi localizado na base ou pode ter sido excluido.',
	sucessPrazo:         'Registro incluído com sucesso!',
	sucessUpdate:        'Registro alterado com sucesso!',
	oCompromisso:        'O compromisso "',
	confirmExclusao:     '" será excluído. Confirma Operação?',
	msgExclusao:         'Registro Excluído com sucesso!',
	camposObrig:         'Não foi possível incluir o registro. Favor verificar se os campos foram devidamente preenchidos!',
	anexos:              'Ver Todos os Anexos',
	selecionaArq:        'Selecionar Arquivo', 

	breadCrumb: breadCrumb

}