
//-- Constantes para títulos das colunas do Grid de Tarefas
export const gridTarefas = {
	
	colunaTipo: 		"Tipo",
	colunaData: 		"Data", 
	colunaHora: 		"Hora",
	colunaStatus: 		"Status",
	colunaResponsavel:	"Responsável",
    colunaDescricao: 	"Descrição",
    labelPendente:      "Pendente",
    labelConcluido:     "Concluído",
    labelCancelado:     "Cancelado",
    labelEmAprov:       "Em Aprovação",
    labelEmAnd:         "Em Andamento"

}

//-- Constantes para o bread crumb
export const breadCrumb = {
	home: 'Meus processos',
	processo: 'Meu processo',
	tarefas: 'Tarefas',
	contrato: 'Meu contrato',
	homeContrato: 'Meus contratos'
}

//-- Constantes para o modal de Tarefas
export const modalPt = {
	modFechar:      "Fechar",
	modConcluir:    "Concluir",
	modErro:        "Não Foi Possível baixar o compromisso.",
	modSucesso:     "Compromisso baixado com sucesso",
	modTitle:       "Concluir Compromisso",
	modCompromisso: "Compromisso",
	modStatusAtual: "Status Atual",
	modNovoStatus:  "Novo Status",
	modSemStatus:   "Favor Informar o Novo Status.",
	modDescricao:   "Descrição"

}

//-- Constantes para o componente de Tarefas
export const tarefasPt = {

    btnNovaTarefa: 	"Nova tarefa",
	loadTarefa: 	"Carregando tarefas",
	tituloTarefa: 	"Tarefas",
	btnSelecionar:  "Selecionar Arquivo",
    gridTarefas:    gridTarefas,
	breadCrumb:     breadCrumb,
	modalPt:        modalPt
}
