import { Subscription } from 'rxjs/internal/Subscription';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PoI18nService, PoModalComponent, PoModalAction, PoUploadComponent } from '@po-ui/ng-components';
import { PoTableColumn, PoDialogConfirmLiterals, PoBreadcrumb, PoNotificationService } from '@po-ui/ng-components';

import { RoutingService } from 'src/app/services/routing.service';
import { FwmodelService, FwModelBody } from 'src/app/services/fwmodel.service';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { FwmodelAdaptorService, adaptorFilterParam } from 'src/app/services/fwmodel-adaptor.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConcluiComprom } from './tarefa';
import { sysParamStruct, SysProtheusParam } from 'src/app/services/legalprocess.service';
import { JurJustifModalComponent } from 'src/app/shared/jur-justif-modal/jur-justif-modal.component';

class dadosStatus {
	codigo: string;
	descricao: string;
	tipo: string;
}

class dadosTipo {
	value: string;
	label: string;
}

class TarefaDet{
	codTarefa:   string;
	data:        string;
	tipo:        string;
	status:      string;
	responsavel: string;
	tarefaEdit:  string;
}

class Tarefa {
	pk: string;
	codTarefa: string;
	data: string;
	tipo: string;
	status: string;
	descStatus: string;
	responsavel: string;
	descricao: string;
	tarefaEdit: Array<string>;
	hora: string;
}

@Component({
	selector: 'app-tarefas',
	templateUrl: './tarefas.component.html',
	styleUrls: ['./tarefas.component.css']
})

export class TarefasComponent implements OnInit {

	dadosStatus: dadosStatus = new dadosStatus();
	dadosTipo: dadosTipo     = new dadosTipo();
	inscricao: Subscription;
	literalsConfirm: PoDialogConfirmLiterals;
	litTarefas: any;
	formConcluiComp: FormGroup;
	resultado: any;
	cajuri: string           = '';
	descTipo: string         = '';
	pkFup: string            = '';
	searchKey: string        = '';
	chaveProcesso: string    = '';
	filial: string           = '';
	statusAtual: string      = '';
	perfil: string           = '/processo/';
	breadcrumbPerfil: string = ''
	breadcrumbHome: string   = ''
	linkHome: string         = ''
	cajuriTitulo: string     = ''

	uploadSuccessCount: number = 0;
	uploadCount: number        = 0;
	nPageSize: number          = 20;
	nRegIndex: number          = 1;
	nTotalTarefas: number      = 0;
	nNextPage: number          = 1;

	status: Array<any>               = [];
	tarefasList: Array<Tarefa>       = [];
	tipos: Array<any>                = [];
	listTarefasDet: Array<TarefaDet> = [];
	sysParams: Array<sysParamStruct> = [];

	isProcEncerrado: boolean      = false;
	isHideLoadingTarefas: boolean = false;
	mostraWidgetTarefas: boolean  = true;
	isProcesso: boolean           = false;
	isShowMoreDisabled: boolean   = false;
	
	/**
	 *  Colunas da widget de Tarefas
	 * */
	colTarefas: Array<PoTableColumn> = [
		{ property: 'data', label: 'Data', type:"date", width: '10%' },
		{ property: 'hora', label: 'Hora', width: '10%' },
		{ property: 'tipo', label: 'Tipo', width: '15%' },
		{
			property: 'status', type: 'label', width: '10%',
			labels: [
				{ value: '1', color: 'color-08', label: 'Pendente' },
				{ value: '2', color: 'color-11', label: 'Concluído' },
				{ value: '3', color: 'color-01', label: 'Cancelado' },
				{ value: '4', color: 'color-06', label: 'Em Aprovação' },
				{ value: '5', color: 'color-09', label: 'Em Andamento' },
				{ value: '6', color: 'color-07', label: 'Pendente' } // vermelho quando a tarefa está pendente e atrasada
			]
		},
		{ property: 'responsavel', label: 'Responsável', width: '20%' },
		{ property: 'descricao', label: 'Descrição', width: '31%' },
		{
			property: 'tarefaEdit', label: ' ', width: '10%', type: 'icon',
			icons: [
				{
					value: 'tarefa', icon: 'po-icon-edit', color: '#29b6c5', tooltip: 'Editar', action: (value) => {
						this.updateTarefa(value);
					},
				},
				{
					value: 'Concluir', icon: 'po-icon-ok', color: '#color-07', tooltip: 'Concluir', action: (value) => {
						this.openConcluiTarefa(value);
					},
				},
				{
					value: 'anexos', icon: 'po-icon po-icon-document-filled', color: '#color-07', tooltip: 'Anexos', action: (value) => {
						this.callAnexos(value);
					}
				}
			]
		},
	];

	// Adapter tipo FUP
	adaptorTipoFup: adaptorFilterParam = new adaptorFilterParam(
		"JURA021", "", "NQS_DESC", "NQS_DESC", "NQS_DESC", "NQS_COD", "", "Busca tipo"
	);

	/**
	 * estrutura de navegação da URL atual
	 */
	public breadcrumb: PoBreadcrumb = {
		items: [],
	};

	// Modal para concluir tarefa
	@ViewChild(PoModalComponent, { static: true }) ConcluiModal: PoModalComponent;
	@ViewChild('modJustif', { static: true }) modalJustif: JurJustifModalComponent;
	@ViewChild('docUpload') docUpload: PoUploadComponent;

	// ações do modal de confirmação de exclusão (Garantia)
	fechar: PoModalAction = {
		action: () => {
			this.ConcluiModal.close();
		},
		label: ' ',
	};

	putConcluirTarefa: PoModalAction = {
		action: () => {
			if (this.JuriGenerics.changeUndefinedToEmpty(this.formConcluiComp.value.novoStatus) != '') {
				this.verifyBaixa();
			} else {
				this.thfNotification.warning(this.litTarefas.modalPt.modSemStatus)
			}
		},
		label: ' ',
	};

	// Adaptor do status de followup
	adaptorStatusFup: adaptorFilterParam = new adaptorFilterParam("JURA017", "", "NQN_DESC", "NQN_DESC", "NQN_DESC", "NQN_COD", "", "getStatusFup")

	constructor(
		protected thfI18nService: PoI18nService,
		protected thfNotification: PoNotificationService,
		private JuriGenerics: JurigenericsService,
		private route: ActivatedRoute,
		private fwmodel: FwmodelService,
		private routingState: RoutingService,
		private router: Router,
		private sysProtheusParam: SysProtheusParam,
		private formBuilder: FormBuilder,
		public fwModelAdaptorService: FwmodelAdaptorService) {
		this.routingState.loadRouting();

		if(this.router.url.indexOf('/contrato/')>-1){
			this.filial = this.route.snapshot.paramMap.get('filContrato');
			window.sessionStorage.setItem('filial', atob(this.filial));
		
			this.chaveProcesso = this.route.snapshot.paramMap.get('codContrato');
			this.cajuri = atob(decodeURIComponent(this.chaveProcesso));
			this.isProcesso = false;
		}else{
			this.filial = this.route.snapshot.paramMap.get('filPro');
			window.sessionStorage.setItem('filial', atob(this.filial));

			this.chaveProcesso = this.route.snapshot.paramMap.get('codPro');
			this.cajuri = atob(decodeURIComponent(this.chaveProcesso));
			this.isProcesso = true;
		}

		thfI18nService.getLiterals({ context: 'tarefas', language: thfI18nService.getLanguage() }).subscribe((litTarefas) => {
			this.litTarefas = litTarefas;

			//-- Adiciona o código do cajuri no título Meu processo + área
			this.cajuriTitulo = this.cajuri.replace(new RegExp('^0*', 'g'), '')

			//Verifica se é um processo ou contrato
			if (this.isProcesso){
				this.perfil = '/processo/';
				this.breadcrumbPerfil = this.litTarefas.breadCrumb.processo
				this.breadcrumbHome   = this.litTarefas.breadCrumb.home
				this.linkHome         = '/home'
			}else{
				this.perfil = '/contrato/';
				this.breadcrumbPerfil = this.litTarefas.breadCrumb.contrato
				this.breadcrumbHome   = this.litTarefas.breadCrumb.homeContrato
				this.linkHome         = '/homeContratos'
			}
			
			this.breadcrumb.items = [
				{ label: this.breadcrumbHome  , link: this.linkHome },
				{ label: this.breadcrumbPerfil, link: this.perfil + this.filial + '/' + this.chaveProcesso },
				{ label: this.litTarefas.breadCrumb.tarefas }
			];

			// seta as constantes para tradução do titulo das colunas do Grid de Tarefas
			this.colTarefas[0].label = this.litTarefas.gridTarefas.colunaData;
			this.colTarefas[1].label = this.litTarefas.gridTarefas.colunaHora;
			this.colTarefas[2].label = this.litTarefas.gridTarefas.colunaTipo;
			this.colTarefas[3].label = this.litTarefas.gridTarefas.colunaStatus;
			this.colTarefas[3].labels[0].label = this.litTarefas.gridTarefas.labelPendente;
			this.colTarefas[3].labels[1].label = this.litTarefas.gridTarefas.labelConcluido;
			this.colTarefas[3].labels[2].label = this.litTarefas.gridTarefas.labelCancelado;
			this.colTarefas[3].labels[3].label = this.litTarefas.gridTarefas.labelEmAprov;
			this.colTarefas[3].labels[4].label = this.litTarefas.gridTarefas.labelEmAnd;
			this.colTarefas[3].labels[5].label = this.litTarefas.gridTarefas.labelPendente;
			this.colTarefas[4].label = this.litTarefas.gridTarefas.colunaResponsavel;
			this.colTarefas[5].label = this.litTarefas.gridTarefas.colunaDescricao;
			this.fechar.label = this.litTarefas.modalPt.modFechar;
			this.putConcluirTarefa.label = this.litTarefas.modalPt.modConcluir;
		});
	}


	ngOnInit() {
		this.getTarefas(this.cajuri, this.filial);
		this.getTipoTarefa();
		this.getInfoProcesso();
		this.searchKey = "";
		this.createFormConcluiComp(new ConcluiComprom());
		// Busca os parâmetros do Protheus
		this.sysProtheusParam.getParamByArray(["MV_JTVENJF"]).subscribe((params) => {
			this.sysParams = params;
		})
	}

	/**
	 * Busca as informações do processo
	 */
	getInfoProcesso() {
		this.fwmodel.restore()
		this.fwmodel.setModelo("JURA095")
		this.fwmodel.setFilter("NSZ_FILIAL = '" + atob(this.filial) + "'");
		this.fwmodel.setFilter("NSZ_COD='" + this.cajuri + "'")
		this.fwmodel.setCampoVirtual(true)
		this.fwmodel.get("getInfoProcesso").subscribe(data => {
			let descResumoBreadCrumb = []
			if (this.isProcesso){
				descResumoBreadCrumb = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields,"NSZ_DAREAJ")
			}else{
				descResumoBreadCrumb = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields,"NSZ_DESCON")
			}
			if (data.resources != "") {
				this.breadcrumb.items[1].label = 	this.breadcrumbPerfil + ' ' + 
													descResumoBreadCrumb + " (" + this.cajuriTitulo + ")";
				this.isProcEncerrado = (this.JuriGenerics.findValueByName(data.resources[0].models[0].fields, "NSZ_SITUAC") == "2")
			}
		})
	}

	/**
	 * Responsável por trazer as tarefas do processo
	 */
	getTarefas(cajuri, filial) {
		let responsaveis: string = '';

		this.getResultado();
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA106");
		this.fwmodel.setCampoVazio(true);
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.setPageSize(this.nPageSize.toString());
		this.fwmodel.setPage(this.nRegIndex.toString());
		this.fwmodel.setFilter("NTA_CAJURI = '" + cajuri + "'");
		this.fwmodel.setFilter("NTA_FILIAL = '" + atob(filial) + "'");
		this.fwmodel.setHeaderParam('orderBy', ' ORDER BY NTA_DTFLWP DESC');

		if (this.searchKey != '' && this.searchKey != undefined) {
			this.fwmodel.setSearchKey("NTA_CTIPO = '" + this.searchKey + "'");
		}
		if (this.nRegIndex == 1){
			this.tarefasList = [];
		}

		this.isHideLoadingTarefas = false;
		this.fwmodel.get("Busca tarefas").subscribe((data) => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != '') {
				data.resources.forEach(item => {
					let itemTarefa = new Tarefa();
					let formatHora: string = "";
					if (this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(item.models[0].fields, "NTA_HORA")) != "") {
						formatHora = this.JuriGenerics.findValueByName(item.models[0].fields, "NTA_HORA")
						formatHora = formatHora.substring(0, 2) + ':' + formatHora.substring(2, 4)
					}
					itemTarefa.pk        = item.pk
					itemTarefa.codTarefa = this.JuriGenerics.findValueByName(item.models[0].fields, "NTA_COD")
					itemTarefa.tipo      = this.JuriGenerics.findValueByName(item.models[0].fields, "NTA_DTIPO")
					itemTarefa.descricao = this.JuriGenerics.findValueByName(item.models[0].fields, "NTA_DESC")
					itemTarefa.hora      = formatHora
					itemTarefa.data      = this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(item.models[0].fields, "NTA_DTFLWP"), "yyyy-mm-dd")
					/* Valida o status */
					this.status.forEach(itemStatus => {
						if (itemStatus.codigo === this.JuriGenerics.findValueByName(item.models[0].fields, "NTA_CRESUL")) {
							if ((itemTarefa.data) < this.JuriGenerics.makeDate(this.JuriGenerics.makeDateProtheus(), "dd/mm/yyyy") &&
								itemStatus.tipo === "1") {
								itemTarefa.status = "6";
							} else {
								itemTarefa.status = itemStatus.tipo;
							}
						}
					});

					itemTarefa.descStatus = this.JuriGenerics.findValueByName(item.models[0].fields, "NTA_DRESUL")
					itemTarefa.tarefaEdit = ['tarefa', 'Concluir']
					/* concatena o responsaveis  */
					item.models[0].models[0].items.forEach(itemResponsavel => {
						if (responsaveis != '') {
							responsaveis = responsaveis.concat(' / ' + this.JuriGenerics.findValueByName(itemResponsavel.fields, "NTE_SIGLA"))
						} else {
							responsaveis = itemResponsavel.fields[1].value
						}
					});

					if (this.JuriGenerics.findValueByName(item.models[0].fields, 'NTA__TEMANX') == '01') {
						itemTarefa.tarefaEdit = ['tarefa', 'Concluir', 'anexos'];
					}

					itemTarefa.responsavel = responsaveis;
					responsaveis = "";
					this.tarefasList.push(itemTarefa);

				});
			}
			this.setShowMoreTarefas(data.total);
			this.isHideLoadingTarefas = true;
		})
	}

	/**
	 * Incrementa o index de paginação da tela e
	 * Seta se o botão de carregar mais continua habilitado ou não
	 * @param total Total de tarefas
	 */
	setShowMoreTarefas(total: number) {
		this.nTotalTarefas = total;
		if(this.nRegIndex == 1){
			this.nRegIndex = this.nPageSize + 1;
		}else{
			this.nRegIndex += this.nPageSize;
		}

		if (this.nRegIndex <= total) {
			this.isShowMoreDisabled = false;
		}else{
			this.isShowMoreDisabled = true;
		};
	}

	/**
	 *  Responsável por trazer os Resultados de Follow-ups 
	 */
	getResultado() {

		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA017");
		this.fwmodel.setCampoVazio(true);
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.get('Busca resultados').subscribe((data) => {
			data.resources.forEach(itensStatus => {
				this.dadosStatus = new dadosStatus();
				this.dadosStatus.codigo = this.JuriGenerics.findValueByName(itensStatus.models[0].fields, "NQN_COD")
				this.dadosStatus.descricao = this.JuriGenerics.findValueByName(itensStatus.models[0].fields, "NQN_DESC")
				this.dadosStatus.tipo = this.JuriGenerics.findValueByName(itensStatus.models[0].fields, "NQN_TIPO")
				this.status.push(this.dadosStatus)
			});
		});
		return this.status
	}

	/**
	 * Responsável por filtrar a tarefa de acordo com o tipo
	 *
	 */
	filtraTipo() {
		this.nRegIndex = 1;
		this.getTarefas(this.cajuri, this.filial);
	}

	/**
	 * Responsável por obter a lista dos tipos de Follow-up para preencher o combo
	 *
	 */
	getTipoTarefa() {

		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA021");
		this.fwmodel.get("Busca tipo de tarefa").subscribe((data) => {
			data.resources.forEach(item => {
				this.dadosTipo = new dadosTipo();
				this.dadosTipo.label = this.JuriGenerics.findValueByName(item.models[0].fields, "NQS_DESC")
				this.dadosTipo.value = this.JuriGenerics.findValueByName(item.models[0].fields, "NQS_COD")
				this.tipos.push(this.dadosTipo);
			})
		})
	}

	/**
	 * Abre a Det-tarefa em modo de Insert
	 */
	createTarefa() {
		this.router.navigate(["./cadastro"], { relativeTo: this.route })
	}

	/**
	 * Abre a Det-tarefa em modo de Update
	 */
	updateTarefa(value?: Tarefa) {
		if (value == undefined) {
			this.router.navigate(["./cadastro"], { relativeTo: this.route })
		} else {
			this.router.navigate(["./" + btoa(value.codTarefa)], { relativeTo: this.route })
		}
	}

	openConcluiTarefa(value?: Tarefa) {
		this.formConcluiComp.patchValue({
			tipo: value.tipo,
			statusAtual: value.descStatus,
			codTar: value.codTarefa,
			novoStatus: '',
			descricao: value.descricao
		})
		this.pkFup = value.pk;

		this.ConcluiModal.open();
	}

	verifyBaixa() {
		if (this.beforeSubmitBaixa()) {
			this.baixTask();
		}
	}

	beforeSubmitBaixa() {
		let isSubmitable: boolean = false;

		isSubmitable = this.formConcluiComp.valid;
		/* Validação para apresentar o Modal de Justificativa, caso o processo esteja encerrado */
		if (isSubmitable) {
			if (this.isProcEncerrado && this.sysProtheusParam.getParamValueByName(this.sysParams, "MV_JTVENJF") == "1") {
				isSubmitable = false;
				this.modalJustif.openModal(this.cajuri,
					() => {
						if (this.modalJustif.isOk) {
							this.baixTask();
						}
					});
			} else {
				isSubmitable = true;
			}
		}

		return isSubmitable
	}

	/**
	 * Baixa a tarefa
	 */
	baixTask() {
		this.ConcluiModal.primaryAction.loading = true;
		this.fwmodel.restore();
		this.fwmodel.setModelo('JURA106');
		this.fwmodel.put(this.pkFup, this.bodybaixaTask()).subscribe((data) => {
			if (data == "") {
				this.thfNotification.error(this.litTarefas.modalPt.modErro);
				this.ConcluiModal.primaryAction.loading = false;
			} else {
				//Valida se há documentos a serem enviados
				if (this.docUpload.hasOwnProperty('currentFiles') && this.docUpload.currentFiles.length > 0) {
					this.uploadCount = this.docUpload.currentFiles.length;
					this.sendDocs();
				} else {
					this.ConcluiModal.primaryAction.loading = false;
					this.thfNotification.success(this.litTarefas.modalPt.modSucesso);
					this.ConcluiModal.close();
					this.nRegIndex = 1;
					this.getTarefas(this.cajuri, this.filial);
				}
			}
		})
	}

	/**
	 * Monta o body para o put de tarefas
	 */
	bodybaixaTask() {
		const fwModelBody: FwModelBody = new FwModelBody("JURA106",4,this.pkFup,"NTAMASTER")

		fwModelBody.models[0].addField("NTA_CRESUL", this.formConcluiComp.value.novoStatus, undefined, "C")
		fwModelBody.models[0].addField("NTA_DESC"  , this.formConcluiComp.value.descricao , undefined, "C")

		return fwModelBody.serialize()
	}
	// form parte contrária
	createFormConcluiComp(concl: ConcluiComprom) {

		this.formConcluiComp = this.formBuilder.group({
			tipo:        [concl.tipo],
			statusAtual: [concl.statusAtual],
			novoStatus:  [concl.novoStatus, Validators.compose([Validators.required])],
			codTar:      [concl.codtar],
			descricao:   [concl.descricao]
		})
	}

	/**
	 * Realiza o envio dos documentos
	 */
	sendDocs() {
		if (this.docUpload.currentFiles.length > 0) {
			this.docUpload.sendFiles();
		}
	}

	/**
	 * Chama o SelectFiles do po-upload
	 */
	getDocuments() {
		this.docUpload.selectFiles()
	}

	/**
	 * Carrega as informações para o POST do upload de arquivos
	 * @param file
	 */
	uploadFiles(file) {
		file.data = {
			cajuri: this.cajuri.toString(),
			entidade: "NTA",
			codEntidade: this.formConcluiComp.value.codTar.toString()
		};
	}

	/**
	 * Trata para redirecionar após concluir todos os anexos
	 */
	uploadSucess() {
		this.uploadSuccessCount++;

		if (this.uploadSuccessCount == this.docUpload.currentFiles.length) {
			this.thfNotification.success(this.litTarefas.modalPt.modSucesso);
			this.ConcluiModal.close();
			this.docUpload.clear();
			this.nRegIndex = 1;
			this.getTarefas(this.cajuri, this.filial);
			this.ConcluiModal.primaryAction.loading = false;
		}
	}

	/**
	 * Abre a tela de anexos
	 */
	callAnexos(value?: any) {
		this.router.navigate(['./' + btoa(value.codTarefa) + '/anexos'], { relativeTo: this.route });
	}

	/**
	 * Ação do botão Carregar mais resultados
	 * Se for clicado pela 3a vez carrega o restante dos dados, independente da quantidade
	 */
	showMore() {
		this.nNextPage ++
		//se for clicado pela 3a vez carrega o restante dos dados
		if(this.nNextPage == 3){
			this.nPageSize = this.nTotalTarefas - this.tarefasList.length
		}
		this.isShowMoreDisabled = true;
		this.getTarefas(this.cajuri, this.filial);
	}

}