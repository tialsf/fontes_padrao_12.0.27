import { Injectable } from '@angular/core';
import { FwmodelService } from 'src/app/services/fwmodel.service';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';

class TarefaDet{
	codTarefa:   string;
	data:        string;
	tipo:        string;
	status:      string;
	responsavel: string;
	tarefaEdit:  string;
}

class Tarefa {
	pk: string;
	codTarefa: string;
	data: string;
	tipo: string;
	status: string;
	descStatus: string;
	responsavel: string;
	descricao: string;
	tarefaEdit: Array<string>;
	hora: string;
}

@Injectable({
  providedIn: 'root'
})
export class TarefasService {

	isHideLoadingTarefas: boolean          = true;
	mostraWidgetTarefas:  boolean          = true;
	listTarefasDet:       Array<TarefaDet> = [];
	tarefasList:          Array<Tarefa>    = [];

	constructor(
		private fwmodel: FwmodelService,
		private JuriGenerics: JurigenericsService,
	) { }

	/**
	 * Get Tarefas para utilizar no resumo do processo, utilizando o FWModel
	 * @param cajuri 
	 * @param filial 
	 */
	async getTarefasResumo(cajuri, filial) {
		let responsaveis: string = '';
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA106");
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.setPageSize('3');
		this.fwmodel.setFilter("NTA_FILIAL = '" + atob(filial) + "'");
		this.fwmodel.setFilter("NTA_CAJURI = '" + cajuri + "'");
		this.fwmodel.setHeaderParam('orderBy', ' ORDER BY NTA_DTFLWP DESC');
		this.isHideLoadingTarefas = false;
		this.listTarefasDet = []
		this.fwmodel.get("Busca tarefas").subscribe((data) => {
			if (this.JuriGenerics.changeUndefinedToEmpty(data.resources) != ''){
				data.resources.forEach(item => {
					let itemTarefa = new TarefaDet();
					itemTarefa.codTarefa  = this.JuriGenerics.findValueByName(item.models[0].fields, "NTA_COD")
					itemTarefa.data       = this.JuriGenerics.makeDate(this.JuriGenerics.findValueByName(item.models[0].fields, "NTA_DTFLWP"), "dd/mm/yyyy")
					itemTarefa.tipo       = this.JuriGenerics.findValueByName(item.models[0].fields, "NTA_DTIPO")
					itemTarefa.status     = this.JuriGenerics.findValueByName(item.models[0].fields, "NTA_DRESUL")
					itemTarefa.tarefaEdit = 'tarefa'
					item.models[0].models[0].items.forEach(itemResponsavel => {
						if (responsaveis != '') {
							responsaveis = responsaveis.concat(' / ' + this.JuriGenerics.findValueByName(itemResponsavel.fields, "NTE_SIGLA"))
						} else {
							responsaveis = itemResponsavel.fields[1].value
						}
					});
					itemTarefa.responsavel = responsaveis; 
					responsaveis = '';
					this.listTarefasDet.push(itemTarefa);
				})
				this.tarefasList = this.JuriGenerics.orderByDate(this.tarefasList, "data");
			}
			this.isHideLoadingTarefas = true;
		}, err => {
			if(err.message == 403) {
				this.mostraWidgetTarefas = false
			}
		})
		return [this.listTarefasDet, this.mostraWidgetTarefas]
	}
}
