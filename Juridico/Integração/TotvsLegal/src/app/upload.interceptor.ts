import { Injectable, NgModule } from '@angular/core';
import { Observable } from 'rxjs';
import {
	HttpEvent,
	HttpInterceptor,
	HttpHandler,
	HttpRequest,
} from '@angular/common/http';
import { JurigenericsService } from './services/jurigenerics.service';
import { Router } from '@angular/router';
import { NotificationService } from './services/notification.service';


@Injectable({
	providedIn: "root"
})

export class UploadInterceptor implements HttpInterceptor {

	constructor(
		private JuriGenerics:  JurigenericsService,
		private notifyService: NotificationService,
		private router:        Router
	) { }

	intercept(
		request: HttpRequest<any>,
		next: HttpHandler,
	): Observable<HttpEvent<any>> {
		return next.handle(this.addAuthenticationToken(request));
	}

	private addAuthenticationToken(request: HttpRequest<any>): HttpRequest<any> {
		let tenantIdValue = this.JuriGenerics.changeUndefinedToEmpty(window.localStorage.getItem('codEmp'));
		let filProc       = this.JuriGenerics.changeUndefinedToEmpty(window.sessionStorage.getItem('filial'));
		
		if ( this.router.url == '/login' && request.url.indexOf('/JURA280') > -1 ) {
			this.notifyService.subscription.unsubscribe();
		}

		if (this.JuriGenerics.changeUndefinedToEmpty(window.localStorage.getItem('filLog')) != ""){
			if (filProc != ""){
				tenantIdValue += ',' + filProc
			} else {
				tenantIdValue += ',' + window.localStorage.getItem('filLog');
			}
		}

		if ((request.url.indexOf("/juri/") > -1) && !request.headers.has("tenantid")){
			request = request.clone({
				headers: request.headers.set('tenantid', tenantIdValue)
			});
		}

		if ((!request.headers.has("Authorization")) && (request.url.indexOf("/juri/")>-1)){
			return request.clone({
				headers: request.headers.set('Authorization',this.JuriGenerics.getRequestAuth())
			});
		}else{
			return request;
		}
	}  
}
