// Constantes de tradução do breadcrumb
const breadcrumb = {
	home: "Home",
	preferencias: "Preferências de usuário"
}

// Constantes utilizadas na pagina
const page = {
	title: "Preferências de usuário",
	searchPlaceHolder: "Digite o tópico ou a preferencia que deseja buscar",
	searchNotFound: "Não foram encontrados nenhum resultado para essa busca",

}

//Constantes utilizdas em botões
const button = {
	btnSave: "Salvar",
	saveNotification: "Preferências salvas com sucesso!",
	btnBack: "Voltar"
}

const accordFilAssJur = {
	label: "Tela inicial de Processos",
	fieldAssJurLabel: "Escolha os assuntos jurídicos que serão levados em consideração no ranking provisão e no painel de acompanhamento de processos"
}

const accordPesqAvan = {
	label: "Pesquisa Avançada",
	fieldSegundoPlano: "Receber a notificação quando tiver pronto para download? O relatório será gerado em segundo plano."
}

const accordion = {
	accordFilAssJur,
	accordPesqAvan
}

// Constantes principais
export const userConfigPT = {
	breadcrumb,
	page,
	button,
	accordion,
	sim: "Sim",
	nao: "Não"
}