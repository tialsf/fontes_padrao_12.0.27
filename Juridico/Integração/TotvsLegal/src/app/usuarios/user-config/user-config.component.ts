import { Component, OnInit, Input, Pipe, PipeTransform } from '@angular/core';
import { PoBreadcrumb, PoCheckboxGroupOption, PoDynamicFormField, PoNotificationService } from '@po-ui/ng-components';
import { UserConfigService } from './user-config.service';
import { RoutingService } from 'src/app/services/routing.service';
import { TLUserInfo } from 'src/app/services/authlogin.service';

@Component({
	selector: 'app-user-config',
	templateUrl: './user-config.component.html',
	styleUrls: ['./user-config.component.css']
})
export class UserConfigComponent implements OnInit {

	@Input() filter:  string                       = '';
	breadcrumb:       PoBreadcrumb;
	litUserConfig:    any;
	codUser:          string                       = "";
	hasListAccordion: boolean                      = false;
	userConfig:       Array<UserConfig>            = [];
	accordionItems:   Array<AccordionItem>         = [];
	listOptions:      Array<PoCheckboxGroupOption> = [];


	constructor(
		private userConfigService: UserConfigService,
		private routingState:      RoutingService,
		private userInfo:          TLUserInfo,
		public  poNotification:    PoNotificationService
	) {

		this.routingState.loadRouting();
		
		this.getLiterais();

		this.setListOptions();
		
		if(window.localStorage.getItem('userConfig') != null){
			this.userConfig = JSON.parse(window.localStorage.getItem('userConfig'));
		}

		
	}
	/**
	 * Responsável pela inicialização do componente
	 */
	ngOnInit() {
		this.userInfo
			.getUserId()
			.subscribe(resp => { this.codUser = resp.userID });

		setTimeout(() => {
			this.createAccordion();
		}, 1000);
	}
	/**
	 * getLiterais - Responsável pela obtenção das literais do componente
	 */
	getLiterais() {
		this.userConfigService
			.getLiterals()
			.subscribe((lit) => {
				this.litUserConfig = lit
				this.createBreadCrumb()
			});
	}
	/**
	 * createBreadCrumb - Responsável pela criação do breadcrumb junto das literais
	 */
	createBreadCrumb() {
		this.breadcrumb = {
			items: [
				{ label: this.litUserConfig.breadcrumb.home         , link: '/' },
				{ label: this.litUserConfig.breadcrumb.preferencias }
			]
		};
	}

	/**
	 * createAccordion - Reponsavel pela criação dos arcodeões definidos na pagina
	 */
	createAccordion() {
		let filtAssJur: Array<string> = this.getFiltAssJur();
		let pesqAvanSegPlano: boolean = this.getPesqAvanSegPlano();
		this.hasListAccordion         = true;
		this.accordionItems           = [
			...this.accordionItems,
			{
				label: this.litUserConfig.accordion.accordFilAssJur.label,
				fields: [
					{
						property:      "filtAssJur",
						label:         this.litUserConfig.accordion.accordFilAssJur.fieldAssJurLabel,
						optionsMulti:  true,
						options:       this.listOptions,
						gridLgColumns: 6,
						gridXlColumns: 6
					}
				],
				value: { filtAssJur }
			},
			{
				label: this.litUserConfig.accordion.accordPesqAvan.label,
				fields: [
					{
						property:      "pesqAvanSegPlano",
						label:         this.litUserConfig.accordion.accordPesqAvan.fieldSegundoPlano,
						type:          "boolean",
						booleanTrue:   this.litUserConfig.sim,
						booleanFalse:  this.litUserConfig.nao
						
					}
				],
				value: { pesqAvanSegPlano }
			}
		];

	}

	/**
	 * setListOptions - Responsavel pela busca e o preenchimento da lista do filtro de assunto jurídico
	 */
	setListOptions() {
		this.userConfigService
			.getListTpAssJur()
			.subscribe(
				(list: Array<PoCheckboxGroupOption>) => {
					this.listOptions = list
				}
			)
	}

	/**
	 * getFiltAssJur - Retorna os dados definidos no localstorage referente ao campo de filtro de assunto jurídico
	 */
	getFiltAssJur(): Array<string> {
		let nIndex: number = this.userConfig.findIndex(x=> x.userId == this.codUser);
		return nIndex > -1 ? this.userConfig[nIndex].filtAssJur : [];
	}
	
	/**
	 * getPesqAvanSegPlano - Retorna os dados definidos no localstorage referente ao campo de gera em segundo plano na tela de pesquisa avançada
	 */
	getPesqAvanSegPlano(){
		let nIndex: number = this.userConfig.findIndex(x=> x.userId == this.codUser);
		return nIndex > -1 ? this.userConfig[nIndex].pesqAvanSegPlano : true;
	}

	/**
	 * salvarConfig - Responsável pela gravação do localstorage, não é feito o retorno para ultima pagina
	 */
	salvarConfig() {
		let nIndex: number = this.userConfig.findIndex(x => x.userId == this.codUser)

		if (nIndex >= 0) {
			this.userConfig[nIndex].filtAssJur       = this.accordionItems[0].value["filtAssJur"]
			this.userConfig[nIndex].pesqAvanSegPlano = this.accordionItems[1].value["pesqAvanSegPlano"]
		} else {
			let newConfig: UserConfig = {
				userId:           this.codUser,
				filtAssJur:       this.accordionItems[0].value["filtAssJur"],
				pesqAvanSegPlano: this.accordionItems[1].value["pesqAvanSegPlano"]
			}

			this.userConfig.push(newConfig)
		}

		window.localStorage.setItem('userConfig', JSON.stringify(this.userConfig));

		this.poNotification.success(this.litUserConfig.button.saveNotification)
	}

	/**
	 * sairConfig - Realiza o retorno da ultima pagina visitada
	 */
	sairConfig() {
		this.routingState.navigateToPreviousPage('Usuarios/config');
	}
}

/**
 * interface do acordeão
 */
interface AccordionItem {
	label:  string
	fields: Array<PoDynamicFormField>
	value?: object
}

/**
 * interface da configuração setada no localstorage
 */
export interface UserConfig {
	userId:            string
	filtAssJur?:       Array<string>
	pesqAvanSegPlano?: boolean
}


/**
 * Pipe criado para realizar o filtro dos acordeões conforme o usuário digita no input de filtro
 */
@Pipe({ name: 'filterAccordionByDescription' })
export class FilterAccordionByDescription implements PipeTransform {

	transform(
		itemList: Array<AccordionItem>, 
		description: string = '', 
		userConfig: UserConfigComponent
	) {

		let retList: Array<AccordionItem> = itemList;

		userConfig.hasListAccordion = true;

		description = description
			.trim()
			.toLowerCase();

		if (description) {
			retList = itemList
				.filter((item) => {
					return item.label.toLowerCase().includes(description)
						|| (item.fields.findIndex(x => x.label.includes(description)) > -1);
				});
		}

		if (retList.length == 0) {
			userConfig.hasListAccordion = false;
		}

		return retList;

	}
}


/**
 * Pipe criado para realizar o filtro dos campos conforme o usuário digita no input de filtro
 */

@Pipe({ name: 'filterFieldByDescription' })
export class FilterFieldByDescription implements PipeTransform {
	transform(
		itemList: Array<PoDynamicFormField>, 
		description: string = '', 
		accordionItem: AccordionItem
	) {
		description = description
			.trim()
			.toLowerCase();

		if (description) {

			if (itemList.findIndex(x => x.label.toLowerCase().includes(description)) > -1) {
				return itemList.filter((item) => {
					let hasFound: boolean = item.label.toLowerCase().includes(description)

					return hasFound
				})
			} else if (accordionItem.label.toLowerCase().includes(description)) {
				return itemList
			} else {
				return []
			}

		} else {
			return itemList
		}
	}
}


