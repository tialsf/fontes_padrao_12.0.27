import { Injectable } from '@angular/core';
import { PoI18nService, PoCheckboxGroupOption } from '@po-ui/ng-components';
import { Observable } from 'rxjs';
import { LegalprocessService } from 'src/app/services/legalprocess.service';
import { map } from 'rxjs/operators';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';

@Injectable({
	providedIn: 'root'
})
export class UserConfigService {

	constructor(
		private poI18nService: PoI18nService,
		private legalprocess: LegalprocessService,
		private juriGenercs: JurigenericsService
	) { }

	/**
	 * função resposável pela busca das literias da configuração de usuário
	 */
	getLiterals(): Observable<object> {
		return this.poI18nService
			.getLiterals({ context: 'userConfig', language: this.poI18nService.getLanguage() })
	}


	/**
	 * função responsável pelo retorno de um observable contendo os dados de tipos de assuntos jurídicos
	 */
	getListTpAssJur(): Observable<Array<PoCheckboxGroupOption>> {
		this.legalprocess.restore();
		return this.legalprocess
			.get('listTpAssuntoJur', 'Busca os assuntos juridicos ')
			.pipe(map(
				(resp) => {
					let listTpAssJurOpt: Array<PoCheckboxGroupOption> = []

					if (this.juriGenercs.changeUndefinedToEmpty(resp.TpAssJur) != '') {
						listTpAssJurOpt = resp.TpAssJur
					}
					return listTpAssJurOpt
				}
			))
	}

}
