import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { DetUsuarioComponent } from './usuarios/det-usuario/det-usuario.component'
import { GruposUsuarioComponent } from './usuarios/grupos-usuario/grupos-usuario.component'
import { UserConfigComponent } from './user-config/user-config.component';
import { DetGruposUsuariosComponent } from './usuarios/grupos-usuario/det-grupos-usuarios/det-grupos-usuarios.component';

const routes: Routes = [
	{
		path: '',
		component: UsuariosComponent
	},
	{
		path: 'perfil/:codParticipante',
		children: [
			{
				path: '',
				pathMatch: 'full',
				component: DetUsuarioComponent
			},
			{
				path: 'grupos/gerenciar',
					children: [
						{
							path: '',
							pathMatch: 'full',
							component: GruposUsuarioComponent
						},
						{
							path: 'grupo/:filial/:codigo' ,
							component: DetGruposUsuariosComponent
						},
						{
							path: 'grupo/cadastro',
							component: DetGruposUsuariosComponent
						}
					]
			},
			{
				path: 'grupos/associar',
					children: [
						{
							path: '',
							pathMatch: 'full',
							component: GruposUsuarioComponent
						},
						{
							path: 'grupo/:filial/:codigo',
							component: DetGruposUsuariosComponent
						},
						{
							path: 'grupo/cadastro',
							component: DetGruposUsuariosComponent
						}
					]
			},
		]
	},
	{
		path: 'config',
		component: UserConfigComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class UsuariosRoutingModule { }
