import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { PoModule } from '@po-ui/ng-components';
import { SharedModule } from '../shared/shared.module';
import { UploadInterceptor } from '../upload.interceptor';
import { UsuariosRoutingModule } from './usuarios-routing.module';
import { UsuariosComponent, FilterUserByName } from './usuarios/usuarios.component';
import { DetUsuarioComponent } from './usuarios/det-usuario/det-usuario.component';
import { GruposUsuarioComponent, FilterGroupByName } from './usuarios/grupos-usuario/grupos-usuario.component';
import { UserConfigComponent, FilterAccordionByDescription, FilterFieldByDescription } from './user-config/user-config.component';
import { DetGruposUsuariosComponent } from './usuarios/grupos-usuario/det-grupos-usuarios/det-grupos-usuarios.component';


@NgModule({
	declarations: [
		UsuariosComponent,
		FilterUserByName,
		DetUsuarioComponent,
		GruposUsuarioComponent,
		FilterGroupByName,
		FilterAccordionByDescription,
		FilterFieldByDescription,
		UserConfigComponent, 
		DetGruposUsuariosComponent
	],
	imports: [
		CommonModule,
		UsuariosRoutingModule,
		PoModule,
		FormsModule,
		ReactiveFormsModule,
		SharedModule
	],
	providers: [{
		provide: HTTP_INTERCEPTORS,
		useClass: UploadInterceptor,
		multi: true,
	}]
})
export class UsuariosModule { }
