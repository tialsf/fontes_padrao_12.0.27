// Constantes de tradução do breadcrumb
const breadcrumb = {
	home:     "Home",
	usuarios: "Gerenciar usuários",
	perfil:   "Perfil"
}

// Constantes de tradução da widget de grupos associados
const grupos = {
	title:     "GRUPOS ASSOCIADOS",
	gerenciar: "Gerenciar",
	associar:  "Associar",
	errorNoUser: "O cadastro do usuário está incompleto. Não é possível acessar o gerenciamento de grupos. Verifique."
}

// Constantes de tradução do forma de edição do participante
const edit = {
	nome:        "Nome",
	tipo:        "Tipo",
	dtAdm:       "Data de Admissão",
	email:       "E-mail",
	codUsr:      "Usuário",
	status:      "Status",
	sigla:       "Sigla",
	apelido:     "Apelido",
	nOAB:        "Número OAB",
	successUpd:  "Dados do usuário alterados com sucesso.",
	interno:     "Interno",
	externo:     "Externo",
	errorCmpReq: "Existem campos obrigatórios não preenchidos. Verifique."
}

// Constantes de tradução da widget do cabeçalho do perfil
const wdgPerfil = {
	loading:   "Carregando...",
	dtAdm:     "Data de admissão",
	tpUsuario: "Tipo de usuário",
	altSenha:  "Alterar senha",
	ativar:    "Ativar usuário",
	desativar: "Desativar usuário"
}

// Constantes de tradução dos botões da tela
const btn = {
	cancelar: "Cancelar",
	salvar:   "Salvar",
	editar:   "Atualizar usuário",
	altSenha: "Alterar senha"
}

// Constantes de tradução da modal de alteração de senha
const modAltSenha = {
	login:           "Login usuário",
    nova:            "Nova senha",
	confirmar:       "Confirmar nova senha",
	title:           "Alterar senha",
	successAltSenha: "Senha alterada com sucesso.",
	errorConftSenha: "As senhas não são iguais.",
	confirmAlt:      "Alterar senha",
	errorPermissao:  "Não foi possível alterar a senha. Procure o administrador para realizar a alteração.",
	errorDesativado: "Não é possível alterar a senha de um(a) usuário(a) desativado(a).",
	errorNoUser:     "O cadastro do usuário está incompleto. Verifique."
}

// Constantes de tradução da modal de ativar/ desativar usuários	
const modAtivDesdativ = {
	msgConfirmAtiv:   "Tem certeza que deseja ativar o(a) usuário(a) {1}?",	
	msgConfirmDestiv: "Tem certeza que deseja destivar o(a) usuário(a) {1}?",
	successOper:      "Operação realizada com sucesso.",
	ativar:           "Ativar",
	desativar:        "Desativar",
	errorPermissao:   "Não foi possível realizar a operação. Procure o administrador."
}

//-- Constantes principais
export const detUsuarioPt = {
	breadcrumb,
	grupos,
	edit,
	btn,
	wdgPerfil,
	modAltSenha,
	modAtivDesdativ,
	cancelar:   "Cancelar",
	carregando: "Carregando..."
}
