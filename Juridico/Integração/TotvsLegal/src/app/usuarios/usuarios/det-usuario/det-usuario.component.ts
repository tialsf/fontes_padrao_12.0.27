import { Component, OnInit, ViewChild } from '@angular/core';
import { 
	PoBreadcrumb, 
	PoI18nService, 
	PoRadioGroupOption, 
	PoNotificationService, 
	PoModalComponent, 
	PoModalAction, 
	PoI18nPipe
} from '@po-ui/ng-components';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { FwmodelService, FwModelBody, FwModelBodyModels } from 'src/app/services/fwmodel.service';
import { FwmodelAdaptorService } from 'src/app/services/fwmodel-adaptor.service';
import { JurconsultasService } from 'src/app/services/jurconsultas.service';
import { UsersService } from 'src/app/services/users.service';

// Dados do participante
export class participante {
	nome:      string = '';
	tipo:      string = '';
	dtAdm:     string = '';
	email:     string = '';
	codUsr:    string = '';
	status:    string = '';
	sigla:     string = '';
	apelido:   string = '';
	nOAB:      string = '';
}

// Dados para alteração de senha
export class altSenha {
	login:     string = '';
	nova:      string = '';
	confirmar: string = '';
}

// Dados combo
class combo{
	value: string;
	label: string;
}

@Component({
	selector: 'app-det-usuario',
	templateUrl: './det-usuario.component.html',
	styleUrls: ['./det-usuario.component.css']
})
export class DetUsuarioComponent implements OnInit {
	litDetUsuario:        any;
	breadcrumb:           PoBreadcrumb;
	formPartic:           FormGroup;
	formAltSenha:         FormGroup;
	perfilData:           participante;
	codParticipante:      string          = '';
	altStatus:            string          = '';
	msgAtivDesativUsr:    string          = '';
	titleAtivDesativUsr:  string          = '';
	pkPartic:             string          = "";
	perfilDataStatus:     string          = "";
	perfilDataTipo:       string          = "";
	perfilDataDtAdm:      string          = "";
	codUsr:               string          = "";
	filial:               string          = "";
	indexGetUsr:          number          = 1;
	listAtiv:             Array<string>   = [];
	listUsers:            Array<combo>    = [];
	isHideLoadGrupos:     boolean         = true;
	isEditPart:           boolean         = false;
	isDisabledEdit:       boolean         = false;
	isHideLoadingInfoUsr: boolean         = false;
	loadingSalvar:        boolean         = false;
	nGrupos:              number          = 0;
	dataPartic:           object          = {};

	// Tipo de participante
	readonly listTpPartic: Array<PoRadioGroupOption> = [
		{ label: 'Interno',  value: '1' },
		{ label: 'Externo',  value: '2' }
	]

	// Ações modal altera senha
	// Cancelar
	cancel: PoModalAction = {
		action: () => {
			this.modalAltSenha.close();
			this.modalAtivDesativUsr.close();
		},
		label: ''
	};

	// Alterar senha
	confirmAlt: PoModalAction = {
		action: () => {
			this.preValidNovaSenha();
		},
		label: ''
	};

	// Ativar/ Desativar usuário
	confirmAtivDesativUsr: PoModalAction = {
		action: () => {
			this.ativDesativUsr();
		},
		label: ''
	};

	// ViewChilds
	@ViewChild('modAlteraSenha'   , { static: true }) modalAltSenha      : PoModalComponent;
	@ViewChild('modAtivDesativUsr', { static: true }) modalAtivDesativUsr: PoModalComponent;

	constructor(
		private poI18nService: PoI18nService,
		private router: Router,
		private route: ActivatedRoute,
		private formBuilder: FormBuilder,
		private JuriGenerics: JurigenericsService,
		private fwmodel: FwmodelService,
		public  poNotification: PoNotificationService,
		public  fwModelAdaptorService: FwmodelAdaptorService,
		private jurconsultas: JurconsultasService,
		private usersService: UsersService,
		private poI18nPipe: PoI18nPipe
	) { 
		if (this.route.snapshot.paramMap.get("codParticipante") != undefined) {
			this.codParticipante = atob(decodeURIComponent(this.route.snapshot.paramMap.get("codParticipante")));
		}
		// Traduções
		this.poI18nService
			.getLiterals({ context: 'detUsuario', language: poI18nService.getLanguage() })
			.subscribe(
				(litDetUsuario) => {
					this.litDetUsuario = litDetUsuario;
					this.setBreadCrumb();

					// Lista de strings Ativar/ Desativar
					this.listAtiv = [
						"",
						this.litDetUsuario.wdgPerfil.ativar,
						this.litDetUsuario.wdgPerfil.desativar
					]

					// Tradução modais
					this.cancel.label     = this.litDetUsuario.cancelar;
					this.confirmAlt.label = this.litDetUsuario.modAltSenha.confirmAlt;

					// Tradução lista tipo de participante
					this.listTpPartic[0].label = this.litDetUsuario.edit.interno;
					this.listTpPartic[1].label = this.litDetUsuario.edit.externo;
				}
			)
	}

	/**
	 * Método responsável pela criação do BreadCrumb conforme as literais cadastradas
	 */
	private setBreadCrumb() {
		this.breadcrumb = {
			items: [
				{ label: this.litDetUsuario.breadcrumb.home    , link: '/' },
				{ label: this.litDetUsuario.breadcrumb.usuarios, link: '/Usuarios' },
				{ label: this.litDetUsuario.breadcrumb.perfil  }
			]
		}

	}

	ngOnInit() {
		this.getInfoUser();
		this.createFormPartic(new participante());
		this.createFormAltSenha(new altSenha());
	}

	/**
	 * Obtém os dados do usuário
	 */
	getInfoUser(){
		this.isDisabledEdit = false;
		this.isHideLoadingInfoUsr = false;
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA159");
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.setFilter("RD0_CODIGO = '"+this.codParticipante+"'");

		this.fwmodel.get('Busca informações participante').subscribe((data) => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data) != ''){
				this.dataPartic = data.resources[0]; // Guarda o response da requisição inicial
				let participante = data.resources[0].models[0].fields;
				let status = (
								this.JuriGenerics.changeUndefinedToEmpty(
									this.JuriGenerics.findValueByName(participante,"RD0_MSBLQL")
								) == "" ? 
								"1" : 
								this.JuriGenerics.findValueByName(participante,"RD0_MSBLQL")
							);

				// Dados formatados para o cabeçalho do perfil
				this.perfilDataStatus = ( status == "2" ? "Ativo" : "Inativo" );

				// realiza o patchValue do form de Participantes
				this.formPatchValue(data.resources[0]);

				this.codUsr = this.JuriGenerics.findValueByName(participante,"RD0_USER"  )

				// Busca a qtde de grupos do usuário
				this.getUserGroups();

				// Pk Participante
				this.pkPartic = data.resources[0].pk;
				// Altera a label do breadcrumb pra o nome do usuário
				this.breadcrumb.items[2].label = this.formPartic.value.nome;
				// label do botão ativar/ desativar usuário
				this.altStatus = this.listAtiv[+status];

				this.isHideLoadingInfoUsr = true;
			}else{
				this.isHideLoadingInfoUsr = true;
			}
		})
	}

	/**
	 * Faz o patchValue do form de Participantes
	 * @param data retorno da requisição
	 */
	formPatchValue(data){
		let participante = data.models[0].fields;
		let complPartic  = data.models[0].models[0].fields;
		let dtAdm = this.JuriGenerics.findValueByName(participante,"RD0_DTADMI");
		let tipo = this.JuriGenerics.findValueByName(participante,"RD0_TIPO");
		let status = (
						this.JuriGenerics.changeUndefinedToEmpty(
							this.JuriGenerics.findValueByName(participante,"RD0_MSBLQL")
						) == "" ? 
						"1" : 
						this.JuriGenerics.findValueByName(participante,"RD0_MSBLQL")
					);

		// Atribui valores para o form de participantes
		this.formPartic.patchValue({
			nome:    this.JuriGenerics.findValueByName(participante,"RD0_NOME"  ),
			tipo:    this.JuriGenerics.findValueByName(participante,"RD0_TIPO"  ),
			dtAdm:   this.JuriGenerics.makeDate(dtAdm, "yyyy-mm-dd"),
			email:   this.JuriGenerics.findValueByName(participante,"RD0_EMAIL" ),
			codUsr:  this.JuriGenerics.findValueByName(participante,"RD0_USER"  ),
			status:  status,
			sigla:   this.JuriGenerics.findValueByName(participante,"RD0_SIGLA" ),
			apelido: this.JuriGenerics.findValueByName(complPartic ,"NUR_APELI" ),
			nOAB:    this.JuriGenerics.findValueByName(complPartic ,"NUR_OAB"   )
		})
		this.perfilDataTipo   = ( tipo == "1" ? "Interno" : "Externo" ); 
		this.perfilDataStatus = ( status == "2" ? "Ativo" : "Inativo" );
		this.perfilDataDtAdm  = this.JuriGenerics.makeDate(dtAdm, "dd/mm/yyyy");

		return true
	}

	/**
	 * Cria o form de participantes
	 * @param participante Estrutura do form
	 */
	createFormPartic(participante: participante){
		
		this.formPartic = this.formBuilder.group({
			nome:      [participante.nome      , Validators.compose([Validators.required])],
			tipo:      [participante.tipo      ],
			dtAdm:     [participante.dtAdm     ],
			email:     [participante.email     ],
			codUsr:    [participante.codUsr    ],
			status:    [participante.status    ],
			sigla:     [participante.sigla     , Validators.compose([Validators.required])],
			apelido:   [participante.apelido   ],
			nOAB:      [participante.nOAB      ]
		})
	}


	/**
	 * Obtém a quantidade de grupos aos quais o participante pertence
	 */
	getUserGroups(){
		
		this.isHideLoadGrupos = false;
		if(this.JuriGenerics.changeUndefinedToEmpty(this.codUsr) !== ""){
			this.isHideLoadGrupos = false;
			this.jurconsultas.restore();
			this.jurconsultas.setParam('partic',this.codParticipante);
			this.jurconsultas.get('userGroups','getUserGroups').subscribe((data) => {
				if(this.JuriGenerics.changeUndefinedToEmpty(data) != ''){
					this.nGrupos = data.length;
					this.isHideLoadGrupos = true;
				}else{
					this.isHideLoadGrupos = true;
				}
			});
		}else{
			this.isHideLoadGrupos = true;
		}
	}

	/**
	 * Altera os dados do participante
	 */
	updatePartic(){
		this.loadingSalvar = true;
		let isSubmitable: boolean =  this.formPartic.valid;
		
		if(isSubmitable){
			this.fwmodel.restore();
			this.fwmodel.setModelo('JURA159');
	
			this.fwmodel.put(this.pkPartic, this.bodyPutPartic(),"Atualiza participante").subscribe((data) => {
				this.dataPartic = data; // Guarda o response da requisição do put
				if (this.JuriGenerics.changeUndefinedToEmpty(data) != '') {
					this.isEditPart = false;

					this.formPartic.reset();
					this.poNotification.success(this.litDetUsuario.edit.successUpd);
					this.loadingSalvar = false;
					this.isDisabledEdit = false;
					// Realiza o patchValue do form de Participantes
					this.formPatchValue(data);

				}else{
					this.loadingSalvar = false;
				}
			});
		}else{
			this.poNotification.error(this.litDetUsuario.edit.errorCmpReq);
			this.loadingSalvar = false;
		}
	}

	/**
	 * Monta o body de alteração do participante
	 */
	bodyPutPartic() {
		let formValue = this.formPartic.value;
		let fwModelBody:    FwModelBody = new FwModelBody('JURA159',4, this.pkPartic, 'RD0MASTER');
		let fwModelBodyNUR: FwModelBodyModels;
		let nOAB: string = formValue.nOAB;

		if(this.JuriGenerics.changeUndefinedToEmpty(nOAB) == ""){
			nOAB = "-"
		};

		fwModelBody.models[0].addField("RD0_NOME"  ,formValue.nome    ,0,"C")
		fwModelBody.models[0].addField("RD0_TIPO"  ,formValue.tipo    ,0,"C")
		fwModelBody.models[0].addField("RD0_DTADMI",formValue.dtAdm   ,0,"D")
		fwModelBody.models[0].addField("RD0_EMAIL" ,formValue.email   ,0,"C")
		fwModelBody.models[0].addField("RD0_USER"  ,formValue.codUsr  ,0,"C")
		fwModelBody.models[0].addField("RD0_MSBLQL",formValue.status  ,0,"C")
		fwModelBody.models[0].addField("RD0_SIGLA" ,formValue.sigla   ,0,"C")
		
		//-- Complemento participante (NUR) 
		fwModelBodyNUR = fwModelBody.models[0].addModel("NURDETAIL", "FIELDS", 1)
		fwModelBodyNUR.addField("NUR_APELI" ,formValue.apelido ,0,"C")
		fwModelBodyNUR.addField("NUR_OAB"   ,nOAB              ,0,"C")

		return fwModelBody.serialize()
	}

	/**
	 * Abre a tela de grupos
	 * @param acao Ação ao abrir a tela de grupos gerenciar/ associar
	 * 
	 */
	openGrupos(acao: string){

		if(this.JuriGenerics.changeUndefinedToEmpty(this.codUsr) !== ""){
			this.router.navigate(["./grupos/"+acao], {relativeTo: this.route})
		}else{
			this.poNotification.error(this.litDetUsuario.grupos.errorNoUser)
		}
	}

	/**
	 * Abre o form de alteração do participante
	 */
	openEdit(){
		this.isEditPart = true;
		this.isDisabledEdit = true;
	}

	/**
	 * Fecha o form de alteração do participante
	 */
	closeEdit(){
		this.isEditPart = false;
		this.isDisabledEdit = false;
		this.formPatchValue(this.dataPartic);
	}

	/**
	 * Abre a modal de alteração de senha
	 */
	openAlteraSenha(){
		let codUsr: string = this.formPartic.value.codUsr;
		
		if(this.JuriGenerics.changeUndefinedToEmpty(codUsr) !== ""){
			if(this.formPartic.value.status == "2"){
				this.usersService.restore();
				this.usersService.get(codUsr).subscribe((data)=>{
	
					this.modalAltSenha.open();
					
					// seta o login do participante
					this.formAltSenha.patchValue({
						login: data.userName
					});
				});
			}else{
				this.poNotification.error(this.litDetUsuario.modAltSenha.errorDesativado);
			}
		}else{
			this.poNotification.error(this.litDetUsuario.modAltSenha.errorNoUser);
		}
		
	};

	/**
	 * Cria o form de alteração de senha
	 * @param altSenha Estrutura do form
	 */
	createFormAltSenha(altSenha: altSenha){
		
		this.formAltSenha = this.formBuilder.group({
			login:     [altSenha.login     ],
			nova:      [altSenha.nova      , Validators.compose([Validators.required])],
			confirmar: [altSenha.confirmar , Validators.compose([Validators.required])]
		})
	}

	/**
	 * Faz a gravação da nova senha
	 */
	submitNovaSenha(){
		let formPartic : participante = this.formPartic.value;
		let formAltSenha : altSenha = this.formAltSenha.value;
		let body = this.bodyPutSenhaUsr();
		this.usersService.restore();
		this.usersService.put(formPartic.codUsr, body, 'Alteração de senha').subscribe((data)=>{
			if(this.JuriGenerics.changeUndefinedToEmpty(data.password) === formAltSenha.nova){
				this.poNotification.success(this.litDetUsuario.modAltSenha.successAltSenha);
				this.modalAltSenha.close();
			}else if(this.JuriGenerics.changeUndefinedToEmpty(data.errorCode) === '403'){
				this.poNotification.error(this.litDetUsuario.modAltSenha.errorPermissao);
			}
		});
	}

	/**
	 * Monta o body da alteração de senha
	 */
	bodyPutSenhaUsr(){
		let formPartic : participante = this.formPartic.value;
		let formAltSenha : altSenha = this.formAltSenha.value;

		const bodyReq : AtualizaSenha = {
			password: formAltSenha.nova ,
			emails: [
				{
					value: formPartic.email, primary: true
				}
			]
		}; // Body da requisição
		return JSON.stringify(bodyReq);
	}

	/**
	 * Verifica se a nova senha e a confirmação, estão iguais
	 */
	preValidNovaSenha(){
		let formAltSenha : altSenha = this.formAltSenha.value;

		if( this.formAltSenha.valid ) {
			// Valid se a confirmação da senha está correta
			if( formAltSenha.nova === formAltSenha.confirmar ) {
				this.submitNovaSenha();
			} else {
				this.poNotification.error(this.litDetUsuario.modAltSenha.errorConftSenha)
			}
		}
	}

	/**
	 * Abre a modal de confirmação de ativação/ desativação de usuário
	 */
	openAtivDesativUsr(){

		if(this.JuriGenerics.changeUndefinedToEmpty(this.codUsr) !== ""){
			let formPartic : participante = this.formPartic.value
			let msgConfirm = (formPartic.status == "1" ? this.litDetUsuario.modAtivDesdativ.msgConfirmAtiv : this.litDetUsuario.modAtivDesdativ.msgConfirmDestiv );
			this.titleAtivDesativUsr = this.listAtiv[+formPartic.status];

			this.confirmAtivDesativUsr.label = this.listAtiv[+formPartic.status];

			this.modalAtivDesativUsr.open();
			// Mensagem de confirmação de ativação/ desativação de usuário
			this.msgAtivDesativUsr = this.poI18nPipe.transform(msgConfirm, [formPartic.nome]);
		}else{
			this.poNotification.error(this.litDetUsuario.modAltSenha.errorNoUser);
		}
	}

	/**
	 * Faz a requisição para ativar/ desativar usuários e altera o campo NZY_MSBLQL
	 */
	ativDesativUsr(){
		let usrId : string = this.formPartic.value.codUsr;
		let operation : string = (this.formPartic.value.status == "1" ? "activate" : "deactivate");
		let altStatus : string = (this.formPartic.value.status == "1" ? "2" : "1");

		this.usersService.restore();
		this.usersService.post(usrId,"",operation).subscribe((data)=>{
			if(this.JuriGenerics.changeUndefinedToEmpty(data.message).indexOf('fulfilled')>-1){
				this.poNotification.success(this.litDetUsuario.modAtivDesdativ.successOper);
				this.formPartic.patchValue({
					status: altStatus
				})
				this.modalAtivDesativUsr.close();
				this.updatePartic();
			}else if(this.JuriGenerics.changeUndefinedToEmpty(data.errorCode) === '403'){
				this.poNotification.error(this.litDetUsuario.modAtivDesdativ.errorPermissao);
			}
		})
	}

	/**
	 * Obtém a lista de usuários do sistema
	 */
	async getUsers(){
		this.usersService.restore();
		this.usersService.setCount("50");
		this.usersService.setStartIndex(this.indexGetUsr.toString());
		this.usersService.setAttributes("id,name");

		let data = await this.usersService.get().toPromise();

		if(this.JuriGenerics.changeUndefinedToEmpty(data) !== ""){
			for (let i = 0; i < data.resources.length; i++) {
				const user = new combo();
			
				user.label = data.resources[i].name.formatted;
				user.value = data.resources[i].id;

				this.listUsers.push(user)
				// Se for o ultimo registro do for
				if(i+1 == data.resources.length){
					this.indexGetUsr += 50 // Seta o proximo indexStart

					if(this.indexGetUsr > data.totalResults){
						return true // Termina a busca de usuários
					}else{
						this.getUsers(); // Faz o get novamente
					}
				}

			};
		}
		
	}


}

/**
 * Estrutura para Post e Put de Andamento
 */
interface AtualizaSenha {
	password: string;
	emails: [{
		value: string, primary: boolean
	}];
}