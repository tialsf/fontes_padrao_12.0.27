// Constantes de tradução do breadcrumb
const breadCrumb = {
	home:      "Home",
	usuarios:  "Gerenciar usuários",
	perfil:    "Perfil",
	gruposGer: "Gerenciar grupos",
	gruposAss: "Associar grupos",
	altGrupo:  "Alterar grupo de usuários",
	novoGrupo: "Novo grupo de usuários"

}

const form = {
	nome:           "Nome",
	tpAcesso:       "Tipo de acesso",
	assJuridicos:   "Assuntos jurídicos relacionados",
	escritorioCred: "Escritório credenciado",
	participante:   "Participantes"
}

const radioGroup = {
	interno:        "Interno",
	correspondente: "Credenciado",
	unidade:        "Unidade"
}

const wdgCredenciado = {
	titulo: "Configuração acesso - Credenciado",
	descricao: "Para o tipo de acesso crendenciado, é preciso definir qual o escritório credenciado pertinente ao grupo. Os integrantes só terão acesso aos processos que estiverem sob responsabilidade deste credenciado."
}

const participantes = {
	nomePart:      'Nome participante',
	nomeUser:      'Nome usuário',
	tootipRemover: 'Remover',
	existsPartici: 'O participante {0}, já faz parte deste grupo.'
}

const botoes = {
	cancelar:         "Cancelar",
	excluir:          "Excluir",
	salvar:           "Salvar",
	novoParticipante: "Incluir participante"
}

const notifications = {
	cmpObrigat:   "Existem campos obrigatórios não preenchidos. Verifique!",
	alterado:     "Grupo alterado com sucesso.",
	incluido:     "Grupo incluido com sucesso",
	excluido:     "Grupo excluído com sucesso",
	vldUsuAssJur: "O(s) usuário(s) que foi(ram) adicionado(s) neste grupo, possui(em) vínculo com os assuntos jurídicos: {0}!",
	vldUsuTpAces:  "O(s) usuário(s) que foi(ram) adicionado(s) neste grupo, já possui(em) vínculo com o tipo de acesso: {0}!",
	msgUltPartic: "Não é permitido remover este participante, pois é necessário ter ao menos um participante no grupo.",
	errPosGrupo:  "Não foi possível incluir o grupo. Verifique.",
	errEscCred:   "Informe um escritório credenciado."
}

// Variáveis principais
export const detGrupoUsuariosPt = {
	carregando: "Carregando",
	breadCrumb,
	form,
	radioGroup,
	wdgCredenciado,
	participantes,
	botoes,
	notifications
}