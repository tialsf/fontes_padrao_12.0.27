import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetGruposUsuariosComponent } from './det-grupos-usuarios.component';

describe('DetGruposUsuariosComponent', () => {
  let component: DetGruposUsuariosComponent;
  let fixture: ComponentFixture<DetGruposUsuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetGruposUsuariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetGruposUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
