import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import {
	PoI18nService,
	PoBreadcrumb,
	PoTableColumn,
	PoCheckboxGroupOption,
	PoComboComponent,
	PoNotificationService,
	PoI18nPipe,
	PoRadioGroupOption
} from '@po-ui/ng-components';
import { DetGruposUsuarios } from './det-grupos-usuarios';
import { 
	adaptorFilterParam, 
	FwmodelAdaptorService 
} from 'src/app/services/fwmodel-adaptor.service';
import { RoutingService } from 'src/app/services/routing.service';
import { JurconsultasService } from 'src/app/services/jurconsultas.service';
import { 
	FwmodelService, 
	FwModelBody, 
	FwModelBodyModels, 
	FwModelBodyItems 
} from 'src/app/services/fwmodel.service';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { ParticipanteAdapterService, EscritorioAdapterService } from 'src/app/services/combos-fwmodel-adapter.service'

type booleanNumber = 0 | 1;

class Participantes {
	id: number = 0;
	
	codigoUser: string = '';
	codigoPart: string = '';
	nomeUsr:    string = '';
	nomePart:   string = '';
	nomeGrupo:  string = '';
	original:   booleanNumber = 1;
	isDeleted:  booleanNumber = 0;
	editUser: Array<string> = ['removeUser'];
}

@Component({
	selector: 'app-det-grupos-usuarios',
	templateUrl: './det-grupos-usuarios.component.html',
	styleUrls: ['./det-grupos-usuarios.component.css']
})
export class DetGruposUsuariosComponent implements OnInit {

	litDetGrupoUsu: any;
	breadCrumb: PoBreadcrumb;
	formGrupo:  FormGroup;

	filialGrupo:      string = '';
	codigoGrupo:      string = '';
	codParticipante:  string = '';
	nomeParticipante: string = '';
	codUsr:           string = '';
	nomeUsr:          string = '';
	pageTitle:        string = '';
	pkGrupo:          string = '';
	tpAcessoOriginal: string = '';
	filtroPart:       string = "RD0_TPJUR = '1' AND RD0_USER <> ''";
	filtroEsc:        string = "A2_MJURIDI='1'";

	nUsuOriginais: number = 0;

	isAssociar:                 boolean = true;
	isDisabledAdicParticipante: boolean = true;
	isNewGroup:                 boolean = true;
	isHiddenLoadGrupo:          boolean = true;
	isLoadingBtnSave:           boolean = false;
	hasAssJuridico:             boolean = false;
	isLoadingBtnDelete:         boolean = false;
	showEscCred:                boolean = false;
	isBtnExcDisabled:           boolean = true;

	listAssJuridicos:    Array<PoCheckboxGroupOption> = [];
	listParticipantes:   Array<Participantes>         = [];
	listPartDeleted:     Array<Participantes>         = [];
	listAssJurOriginais: Array<AssJuridicoOriginal>   = [];
	
	// 1-interno 3-correspondente 2-unidade
	listTpAcesso:     Array<PoRadioGroupOption> = [
		{ value: '1' , label: '' },
		{ value: '3' , label: '' }
	]
	
	colParticipantes: Array<PoTableColumn> = [
		{ property: 'nomePart' },
		{ property: 'nomeUsr' , width: '15%' },
		{ property: 'editUser', label: ' ', width: '6%', type: 'icon',
			icons: [
				{ value: 'removeUser', icon: 'po-icon po-icon-delete', color: '#29b6c5', tooltip: '',
					action: (row)=>{
						this.excluirParticipante(row.codigoUser);
					},
				}
			]
		}
	]

	@ViewChild('comboParticipantes') cmbParticipante: PoComboComponent;

	constructor(
		private router:                     Router,
		private route:                      ActivatedRoute,
		private routingService:             RoutingService,
		private poI18nService:              PoI18nService,
		private poI18nPipe:                 PoI18nPipe,
		private fwModelService:             FwmodelService,
		private jurGenerics:                JurigenericsService,
		private jurconsultas:               JurconsultasService,
		private formBuilder:                FormBuilder,
		public  fwModelAdaptorService:      FwmodelAdaptorService,
		public  poNotification:             PoNotificationService,
		public  participanteAdapterService: ParticipanteAdapterService,
		public  escritorioAdapterService:   EscritorioAdapterService
	) {
		
		// Setups de Adapters de fwmodel
		this.participanteAdapterService.setup(new adaptorFilterParam("JURA159","RD0_TPJUR = '1' AND RD0_USER <> ''","RD0_NOME,RD0_CIC,RD0_SIGLA","RD0_NOME,RD0_CODIGO,RD0_USER,RD0_CIC","RD0_NOME [ RD0_CIC ]","RD0_CODIGO,RD0_USER,RD0_USRNAM,RD0_NOME","","Participantes",undefined,undefined,undefined,{	bGetAllLevels: false,	bUseEmptyField: false,	bUseVirtualField: true,	arrSetFields: ["RD0_NOME","RD0_CODIGO","RD0_USER","RD0_CIC","RD0_USRNAM","RD0_SIGLA"]}))
		this.escritorioAdapterService.setup(new adaptorFilterParam("JURA132","A2_MJURIDI='1'","A2_NOME,A2_CGC","A2_NOME","","A2_COD,A2_LOJA","","getEscritorio",undefined,undefined,undefined,{	bGetAllLevels:true,	bUseEmptyField:false,	bUseVirtualField:false,	arrSetFields:[]}))

		this.routingService.loadRouting();

		this.getVariaveisRota();

		this.poI18nService.getLiterals({ 
			context: 'detGrupoUsuarios', 
			language: poI18nService.getLanguage() 
		})
		.subscribe(
			(litDetGrupoUsu) => {
				this.litDetGrupoUsu = litDetGrupoUsu;
				this.setBreadCrumb();
				this.traducaoGrupo();
			}
		);
	}

	/**
	 * obtém as variáveis de rota
	 */
	getVariaveisRota(){
		if (this.route.snapshot.paramMap.get("codParticipante") != undefined) {
			this.codParticipante = atob(decodeURIComponent(
				this.route.snapshot.paramMap.get("codParticipante")
				));
		}

		if (this.route.snapshot.paramMap.get('filial') != undefined) {
			this.filialGrupo = atob(this.route.snapshot.paramMap.get('filial'));
		}
		
		if (this.route.snapshot.paramMap.get('codigo') != undefined) {
			this.codigoGrupo = atob(this.route.snapshot.paramMap.get('codigo'));
		}

		// Verifica se é o cadastro de um novo grupo
		this.isNewGroup = ( this.router.url.indexOf('/cadastro') > -1 ? true : false )

		// Obtém da rota se a tela é referente à gerenciar ou associar grupos
		this.isAssociar = (this.router.url.indexOf('/associar')>-1 ? true : false )
	}

	/**
	 * Seta os itens do breadCrumb
	 */
	setBreadCrumb(){
		let grupos : string;
		let path : string = this.isAssociar ? 'associar' : 'gerenciar'

		grupos = (this.isAssociar ? 
			this.litDetGrupoUsu.breadCrumb.gruposAss : 
			this.litDetGrupoUsu.breadCrumb.gruposGer)

		this.pageTitle = this.isNewGroup ? 
			this.litDetGrupoUsu.breadCrumb.novoGrupo : 
			this.litDetGrupoUsu.breadCrumb.altGrupo

		this.breadCrumb = {
			items: [
				{ 
					label: this.litDetGrupoUsu.breadCrumb.home, 
					link: '/'
				},
				{ 
					label: this.litDetGrupoUsu.breadCrumb.usuarios, 
					link: '/Usuarios'
				},
				{ 
					label: this.litDetGrupoUsu.breadCrumb.perfil, 
					link: '/Usuarios/perfil/'+btoa(this.codParticipante) 
				},
				{ 
					label: grupos, 
					link: '/Usuarios/perfil/'+btoa(this.codParticipante)+
					'/grupos/'+path
				},
				{ 
					label: this.pageTitle
				},
			]
		}
	}

	/**
	 * Tradução da pagina dos grupos
	 */
	traducaoGrupo(): void {
		this.listTpAcesso[0].label = this.litDetGrupoUsu.radioGroup.interno;
		this.listTpAcesso[1].label = this.litDetGrupoUsu.radioGroup.correspondente;

		this.colParticipantes[0].label            = this.litDetGrupoUsu.participantes.nomePart;
		this.colParticipantes[1].label            = this.litDetGrupoUsu.participantes.nomeUser;
		this.colParticipantes[2].icons[0].tooltip = this.litDetGrupoUsu.participantes.tootipRemover;
	}

	ngOnInit() {
		this.getInfoUser();
		this.createFormGrupo(new DetGruposUsuarios);
		this.setListAssJuridicos();
	}

	/**
	* Obtém os dados do participante
	*/
	getInfoUser(): void {
		this.fwModelService.restore();
		this.fwModelService.setModelo("JURA159");
		this.fwModelService.setCampoVirtual(true);
		this.fwModelService.setFilter("RD0_CODIGO='" + this.codParticipante + "'");

		this.fwModelService.get('getInfoUser').subscribe((data) => {
			if(this.jurGenerics.changeUndefinedToEmpty(data.resources[0]) != ''){
				this.nomeParticipante = this.jurGenerics.findValueByName(data.resources[0].models[0].fields,"RD0_NOME");
				this.breadCrumb.items[2].label = this.nomeParticipante;
					
				this.codUsr  = this.jurGenerics.findValueByName(data.resources[0].models[0].fields,"RD0_USER");
				this.nomeUsr = this.jurGenerics.findValueByName(data.resources[0].models[0].fields,"RD0_USRNAM");
				
				if (this.isNewGroup)
					this.defaultParticipante();
			}
		})
	}	

	/**
	 * Adiciona o participante do perfil no novo grupo
	 */
	defaultParticipante() : void {
		this.listParticipantes = [];

		let participante: Participantes = new Participantes();

		participante.id         = 1;
		participante.codigoPart = this.codParticipante;
		participante.nomePart   = this.nomeParticipante;
		participante.codigoUser = this.codUsr;
		participante.nomeUsr    = this.nomeUsr;
		participante.original   = 0;
		participante.isDeleted  = 0;

		this.listParticipantes.push(participante);
	}

	/**
	 * Cria o formulário do grupo de usuários
	 */
	createFormGrupo(grupoStructure: DetGruposUsuarios): void {
		this.formGrupo = this.formBuilder.group({
			nome:           [grupoStructure.nome    , [Validators.required]],
			tpAcesso:       [grupoStructure.tpAcesso, [Validators.required]],
			assJuridicos:   [grupoStructure.assJuridicos                   ],
			escritorioCred: [grupoStructure.escritorioCred                 ],
			participante:   [grupoStructure.participantes                  ]
		})
	};

	/**
	 * Obtém os tipos de assuntos jurídicos cadastrados
	 */
	async setListAssJuridicos() {
		this.isHiddenLoadGrupo = false;

		let listAux: PoCheckboxGroupOption[] = [];
		this.fwModelService.restore();
		this.fwModelService.setModelo('JURA158');

		let data = await this.fwModelService.get('getAssJuridicos').toPromise();
		
		if (this.jurGenerics.changeUndefinedToEmpty(data.resources) != '') {
			data.resources.forEach( assJur => {
				let fields = assJur.models[0].fields;
				const assJuridico : PoCheckboxGroupOption = {
					value: this.jurGenerics.findValueByName(fields, 'NYB_COD'),
					label: this.jurGenerics.findValueByName(fields, 'NYB_DESC')
				}
				listAux.push(assJuridico);
			} );

			this.listAssJuridicos = listAux;
			if (this.isNewGroup) {
				this.isHiddenLoadGrupo = true;
				this.isBtnExcDisabled = true;
			} else {
				this.getAssJurGrupo();
				this.getInfoGrupo();
			}
		}
	}

	/**
	 * Obtém os assuntos jurídicos do grupo
	 */
	getAssJurGrupo() : void {
		let listAssuJur:    Array<string> = [];
		let listAssuJurOri: Array<AssJuridicoOriginal> = [];
		let escritorioCred: string = ''

		this.fwModelService.restore();
		this.fwModelService.setModelo("JURA163");
		this.fwModelService.setFilter("NVK_CGRUP='" + this.codigoGrupo + "'");
		this.fwModelService.get('Relacionamento grupo x Assunto jurídico')
			.subscribe(data => {
				if ( this.jurGenerics.changeUndefinedToEmpty(data.resources) != '' ) {
					data.resources.forEach(item => {
						listAssuJur.push(
							this.jurGenerics.findValueByName(
								item.models[0].fields,
								"NVK_CASJUR"
							)
						);
						listAssuJurOri.push( { 
							codigo: this.jurGenerics.findValueByName(
								item.models[0].fields,
								"NVK_CASJUR"
							),
							pk: item.pk
						} );
						escritorioCred = this.jurGenerics.findValueByName(
							item.models[0].fields,
							"NVK_CCORR"
						) + '<separador>' +
						this.jurGenerics.findValueByName(
							item.models[0].fields,
							"NVK_CLOJA"
						)
					} );

					this.formGrupo.patchValue({
						assJuridicos: listAssuJur,
						escritorioCred: escritorioCred
					});
					this.showEscCredenciado();

					listAssuJurOri.forEach(item => {
						this.listAssJurOriginais.push(item)
					})
					this.isHiddenLoadGrupo = true;
					this.isBtnExcDisabled = false;
					this.hasAssJuridico = true;
				} else {
					this.isHiddenLoadGrupo = true;
					this.isBtnExcDisabled = false;
					this.showEscCredenciado();
				}
			});
	}

	/**
	 * Função responsável por obter informações do grupo de usuário
	 */
	getInfoGrupo() : void {
		let form: DetGruposUsuarios;

		this.fwModelService.restore();
		this.fwModelService.setModelo('JURA218/' + btoa(this.filialGrupo) + btoa(this.codigoGrupo));
		this.fwModelService.setCampoVirtual(true);
		this.fwModelService.setFirstLevel(false);
		
		this.fwModelService.get('Busca informação do grupo').subscribe((data) => {

			if ( this.jurGenerics.changeUndefinedToEmpty(data.models) != '' ) {
				let nomeGrupo : string = 
					this.jurGenerics.findValueByName(data.models[0].fields,'NZX_DESC');
				let tpAcesso : string = this.jurGenerics.findValueByName(data.models[0].fields,'NZX_TIPOA');
				this.pkGrupo = data.pk;
				this.breadCrumb.items[4].label = nomeGrupo;

				this.formGrupo.patchValue({
					nome: nomeGrupo,
					tpAcesso: tpAcesso
				})
				
				form = this.formGrupo.value;
				
				this.nUsuOriginais = data.models[0].models[0].items.length;
				data.models[0].models[0].items.forEach(element => {
					this.getInfoParticipante(
						this.jurGenerics.findValueByName(element.fields,"NZY_CUSER"),
						this.jurGenerics.findValueByName(element.fields,"NZY_DUSER"),
						element.id
					);
				});

				this.tpAcessoOriginal = tpAcesso;

				this.showEscCredenciado()
			}
		})
	}


	/** 
	 * Obtém as informações do participante 
	 * */
	getInfoParticipante(codUsr: string, descUsr: string, id: number) : void {
		let nIndex: number = -1;

		this.fwModelService.restore();
		this.fwModelService.setModelo("JURA159");
		this.fwModelService.setCampoVirtual(true);
		this.fwModelService.setFilter("RD0_USER='" + codUsr + "'");
		this.fwModelService.get('Busca nome e código participante').subscribe(data => {
			if (data.total > 0) {
				data.resources.forEach(participantes => {
					if (this.listParticipantes.findIndex(
						x => x.codigoPart == this.jurGenerics.findValueByName(participantes.models[0].fields,"RD0_CODIGO")) == -1
					) {
						nIndex = this.listParticipantes.findIndex(x => x.codigoUser == codUsr);

						if (nIndex > -1) {
							this.listParticipantes[nIndex].nomePart   += 
								', ' + 
								this.jurGenerics.findValueByName(
									participantes.models[0].fields,"RD0_NOME"
								);
							this.listParticipantes[nIndex].codigoPart += 
								', ' + 
								this.jurGenerics.findValueByName(
									participantes.models[0].fields,"RD0_CODIGO"
								);
						} else {
							let participante: Participantes = new Participantes;

							participante.codigoUser = codUsr;
							participante.nomePart   = this.jurGenerics.findValueByName(participantes.models[0].fields,"RD0_NOME");
							participante.codigoPart = this.jurGenerics.findValueByName(participantes.models[0].fields,"RD0_CODIGO");
							participante.nomeUsr    = descUsr;
							participante.id         = id;
							participante.original   = 1;
							participante.isDeleted  = 0;
		
							this.listParticipantes.push(participante);
						}
					}
				});
			} else {
				if ( this.jurGenerics.changeUndefinedToEmpty(codUsr) != ''  ) {
					let participante: Participantes = new Participantes;

					participante.codigoUser = codUsr;
					participante.nomePart   = '-';
					participante.nomeUsr    = descUsr;
					participante.id         = id;
					participante.original   = 1;
					participante.isDeleted  = 0;

					this.listParticipantes.push(participante);
				}
			};
		});
	}

	/**
	 * Ação do botão de adicionar participantes na tabela 
	 */
	novoParticipante() : void {
		let participante: Participantes = new Participantes;
		let nIndex:       number        = -1;
		let nIndexDelet:  number        = -1;

		participante.codigoUser = this.jurGenerics.getValueByOption(this.cmbParticipante,"RD0_USER");
		participante.codigoPart = this.jurGenerics.getValueByOption(this.cmbParticipante,"RD0_CODIGO");
		participante.nomePart   = this.jurGenerics.getValueByOption(this.cmbParticipante,"RD0_NOME");
		participante.nomeUsr    = this.jurGenerics.getValueByOption(this.cmbParticipante,"RD0_USRNAM");
		participante.id         = ++this.nUsuOriginais;
		participante.original   = 0;
		participante.isDeleted  = 0;

		// Se não encontrar o participante na lista
		if ( this.listParticipantes.findIndex(x => x.codigoPart.indexOf(participante.codigoPart) > -1) == -1 ) {
			// Procura o novo participante nos excluídos
			if (( nIndexDelet = this.listPartDeleted.findIndex(x => x.codigoUser == participante.codigoUser) ) > -1 ) {
				if (!(this.listPartDeleted[nIndexDelet].isDeleted = 0)) { // desmarca como deletado
					// retorna para o array de participante
					this.listParticipantes.push(this.listPartDeleted.find(x => x.codigoUser == participante.codigoUser));
					this.listPartDeleted.splice(nIndexDelet,1); // retira do array de part deletados
				};
			} else {
				if (( nIndex = this.listParticipantes.findIndex(x => x.codigoUser == participante.codigoUser) ) > -1) {
					this.listParticipantes[nIndex].nomePart   += ', ' + participante.nomePart;
					this.listParticipantes[nIndex].codigoPart += ', ' + participante.codigoPart;
				} else {
					this.listParticipantes.push(participante);
				}
			}

			this.formGrupo.patchValue({ participante: '' });
			this.isDisabledAdicParticipante = true;
		} else {
			this.poNotification.error(
				this.poI18nPipe.transform(
					this.litDetGrupoUsu.participantes.existsPartici,
					[participante.nomePart]
				)
			);
		}
	}

	/**
	 * Exclui a linha do participante
	 * @param codigoUser código do usuário
	 */
	excluirParticipante(codigoUser: string) {
		if (this.listParticipantes.length > 1) {
			let nIndex = this.listParticipantes.findIndex(x => x.codigoUser == codigoUser);
			if (this.listParticipantes[nIndex].isDeleted = 1) {
				this.listPartDeleted.push(this.listParticipantes.find(x => x.codigoUser == codigoUser));
				this.listParticipantes.splice(nIndex,1);
			};
		} else {
			this.poNotification.error(
				this.litDetGrupoUsu.notifications.msgUltPartic
			)
		}
	}

	/**
	 * Valida se o participante já existe na lista
	 */
	vldAdicionaParticipante() {
		this.isDisabledAdicParticipante = true;

		if (this.jurGenerics.changeUndefinedToEmpty(this.formGrupo.value.participante) != '') {
			this.isDisabledAdicParticipante = false;
		}
	}

	/**
	 * Faz a gravação das alterações do grupo
	 */
	async submitGrupo(){
		this.isLoadingBtnSave   = true;
		let hasAssJur : boolean = false;

		hasAssJur = 
			this.formGrupo.value.assJuridicos.length > 0 || 
			this.listAssJurOriginais.length > 0
		
		try {
			const isValid = await this.vldUserAssJuridico();
			if (isValid) {
				if (this.formGrupo.valid && 
				this.listParticipantes.length) {
					if (this.isNewGroup) {
						try {
							const codigo = await this.postGrupo();
							if (hasAssJur) {
								this.submitUsuXPesq(codigo);
								this.isLoadingBtnSave = false;
							} else {
								this.poNotification.success(this.litDetGrupoUsu.notifications.incluido);
								this.previousURL();
								this.isLoadingBtnSave = false;
							}
						} catch (error) {
							this.poNotification.error(error);
							this.isLoadingBtnSave = false;
						}
					} else {
						try {
							await this.putGrupo();
							if (hasAssJur) {
								this.submitUsuXPesq(this.codigoGrupo);
								this.isLoadingBtnSave = false;
							} else {
								this.poNotification.success(this.litDetGrupoUsu.notifications.incluido);
								this.previousURL();
								this.isLoadingBtnSave = false;
							}
						} catch (error) {
							this.isLoadingBtnSave = false;
							this.poNotification.error(error);
						}
					}
				} else {
					this.poNotification.error(this.litDetGrupoUsu.notifications.cmpObrigat);
					this.isLoadingBtnSave = false;
				}
			} else {
				this.isLoadingBtnSave = false;
			}
		} catch (error) {
			this.poNotification.error(error);
			this.isLoadingBtnSave = false;
		}
	}

	/**
	 * Retorna para a tela anterior
	 */
	previousURL(){
		this.router.navigate([
			this.routingService.getPreviousUrl(this.codigoGrupo)
		], { relativeTo: this.route });
	}

	/**
	 * Inclui grupos de usuários
	 * Post JURA218 - grupos de usuários
	 */
	async postGrupo(): Promise<string> {
		let canPost : boolean = this.validaEscCred();

		if (canPost) {
			let cBody : string = this.montaBodyPostGrupo();
			let codGrupo : string = ''

			this.fwModelService.restore();
			this.fwModelService.setModelo('JURA218');
			
			let data = await this.fwModelService
				.post(cBody, 'Grava grupos').toPromise();
			
			if(this.jurGenerics.changeUndefinedToEmpty(data) != ''){
				return codGrupo = this.jurGenerics.findValueByName(
					data.models[0].fields, 'NZX_COD'
				);
			} else {
				throw Error (this.litDetGrupoUsu.notifications.errPosGrupo);
			}
		} else {
			throw Error (this.litDetGrupoUsu.notifications.errEscCred);
		}
	}	

	/**
	 * Valida o preenchimento do campo Escritório credenciado
	 */
	validaEscCred() : boolean {
		let formValue = this.formGrupo.value;
		let canPost : boolean = false;

		if (formValue.tpAcesso == '3' && formValue.assJuridicos.length) {
			return formValue.escritorioCred != ''
		} else {
			return canPost = true;
		}
	}

	/**
	 * Monta o body POST JURA218 - grupos de usuários
	 */
	montaBodyPostGrupo() {
		let form : DetGruposUsuarios = this.formGrupo.value;
		let fwModelBodyNZX: FwModelBody = new FwModelBody('JURA218',3,undefined,'NZXMASTER');
		let fwModelBodyNZY: FwModelBodyModels;
		let itemNZY:        FwModelBodyItems;

		//-- Grupo (NZX)
		fwModelBodyNZX.models[0].addField('NZX_DESC' ,form.nome);
		fwModelBodyNZX.models[0].addField('NZX_TIPOA',form.tpAcesso);

		//-- Usuário X Grupo (NZY) 
		fwModelBodyNZY = fwModelBodyNZX.models[0].addModel("NZYDETAIL", "GRID", 1)

		for (let i = 0; i < this.listParticipantes.length; i++) {
			itemNZY = fwModelBodyNZY.addItems(0,0)
			itemNZY.addField("NZY_CUSER" , this.listParticipantes[i].codigoUser ,"C")
		}

		return fwModelBodyNZX.serialize()
	}

	/**
	 * Altera o grupo
	 * Post JURA218 - grupos de usuários
	 */
	async putGrupo() : Promise<boolean> {
		let cBody : string = this.montaBodyPutGrupo();
		let canPost : boolean = this.validaEscCred();

		if (canPost) {
			this.fwModelService.restore();
			this.fwModelService.setModelo('JURA218');
			
			const data = await this.fwModelService
				.put(this.pkGrupo ,cBody, 'Altera grupo')
				.toPromise();

			if (this.jurGenerics.changeUndefinedToEmpty(data) != '') {
				return data.models[0].models[0].items.length == this.listParticipantes
			} else {
				throw Error (this.litDetGrupoUsu.notifications.errPutGrupo)
			}
		} else {
			throw Error (this.litDetGrupoUsu.notifications.errEscCred);
		}
	}

	/**
	 * Monta o body PUT JURA218 - grupos de usuários
	 */
	montaBodyPutGrupo(){
		let form :       DetGruposUsuarios    = this.formGrupo.value;
		let listSorted : Array<Participantes> = [];
		
		let fwModelBodyNZX: FwModelBody = new FwModelBody('JURA218',4,this.pkGrupo,'NZXMASTER');
		let fwModelBodyNZY: FwModelBodyModels;
		let itemNZY:        FwModelBodyItems;

		//-- Grupo (NZX)
		fwModelBodyNZX.models[0].addField('NZX_COD'  ,this.codigoGrupo);
		fwModelBodyNZX.models[0].addField('NZX_DESC' ,form.nome);

		if (!this.hasAssJuridico)
			fwModelBodyNZX.models[0].addField('NZX_TIPOA',form.tpAcesso);

		//-- Usuário X Grupo (NZY) 
		fwModelBodyNZY = fwModelBodyNZX.models[0].addModel("NZYDETAIL", "GRID", 1)

		listSorted = this.listParticipantes; // clona a lista de participantes
		listSorted = listSorted.concat(this.listPartDeleted); // faz o merge com a lista de deletados
		listSorted = listSorted.sort((a,b) => a.id - b.id); // ordena por id (NZY)
					
		for (let i = 0; i < listSorted.length; i++) {
			let addBody : boolean = !(!listSorted[i].original && listSorted[i].isDeleted);
			if ( addBody ) {
				itemNZY = fwModelBodyNZY.addItems(0,listSorted[i].isDeleted)
				if (listSorted[i].isDeleted || !listSorted[i].original) {
					itemNZY.addField("NZY_CGRUP" , this.codigoGrupo         ,"C")
					itemNZY.addField("NZY_CUSER" , listSorted[i].codigoUser ,"C")
				}
			}
		}

		return fwModelBodyNZX.serialize()
	}

	/**
	 * Faz a gravação dos Usuarios X Pesquisa
	 * @param codGrupo código do grupo
	 */
	async submitUsuXPesq(codGrupo, excGrupo: boolean = false){
		let assJur: Array<string> = this.formGrupo.value.assJuridicos;
		let listAssJurInc: Array<string> = [];
		let listAssJurExc: Array<string> = [];

		if (excGrupo) {
			// Exclui assuntos juridicos 
			for (let i = 0; i < this.listAssJurOriginais.length; i++) {
				if (assJur.findIndex(x => x == this.listAssJurOriginais[i].codigo ) == -1 ) {

					listAssJurExc.push(this.listAssJurOriginais[i].pk);
				}
				if (i + 1 == this.listAssJurOriginais.length){
					if (listAssJurExc.length)
						await this.deleteUsrXPesq(listAssJurExc);
				}
			}
		} else {
			listAssJurInc = this.getNovosAssJur();
			if (listAssJurInc.length)
				await this.postUsrXPesq(listAssJurInc, codGrupo);
			// Exclui assuntos juridicos 
			for (let i = 0; i < this.listAssJurOriginais.length; i++) {
				if (assJur.findIndex(x => x == this.listAssJurOriginais[i].codigo ) == -1 ) {

					listAssJurExc.push(this.listAssJurOriginais[i].pk);
				}
				if (i + 1 == this.listAssJurOriginais.length){
					if (listAssJurExc.length)
						await this.deleteUsrXPesq(listAssJurExc);
				}
			}
			
			this.poNotification.success(this.litDetGrupoUsu.notifications.incluido);
			this.previousURL();
		}
	}

	/**
	 * Vincula assuntos jurídicos aos grupos de usuários
	 * Post JURA 163 - Usuários X Pesquisa
	 */
	async postUsrXPesq(listAssJuridico : Array<string>, codGrupo : string) {

		let listResult: Array<string> = [];

		this.fwModelService.restore();
		this.fwModelService.setModelo('JURA163');

		const promises = listAssJuridico.map( async (assJur) => 
			await this.fwModelService
			.post(this.montaBodyNVK(assJur, codGrupo), 'Grava grupo X assunto juridico')
			.toPromise()
		);

		await Promise.all(promises).then(res => {
			res.forEach(element => {
				listResult.push(this.jurGenerics.findValueByName( 
						element.models[0].fields, 
						'NVK_CASJUR' 
					) 
				) 
			});
		})
	}

	/**
	 * Monta o body do JURA163 - usuários X pesquisa
	 */
	montaBodyNVK(assJuridico : string, codGrupo : string) : string {
		let fwMdlBody: FwModelBody = new FwModelBody('JURA163',3,undefined,'NVKMASTER');
		let form : DetGruposUsuarios = this.formGrupo.value;
		
		fwMdlBody.models[0].addField('NVK_CGRUP' ,codGrupo);
		fwMdlBody.models[0].addField('NVK_CASJUR',assJuridico);
		fwMdlBody.models[0].addField('NVK_CCORR' ,form.escritorioCred.substr(0,6));
		fwMdlBody.models[0].addField('NVK_CLOJA' ,form.escritorioCred.substr(17,2));

		return fwMdlBody.serialize()
	}

	/**
	 * Deleta o vinculo do grupo com os assuntos juridicos
	 * Delete JURA163 - usuário X pesquisa
	 */
	async deleteUsrXPesq( listPk : Array<string> ) {
		let listReturn : Array<boolean> = [];

		this.fwModelService.restore();
		this.fwModelService.setModelo('JURA163');

		const promises = listPk.map(async (pk) => 
			await this.fwModelService
			.delete(pk, 'Deleta grupo X assunto juridico')
			.toPromise()
		);

		await Promise.all(promises).then(res => {
			res.forEach(element => {
				listReturn.push(element) 
			});
		})
	}

	/**
	 * Habilita/ desabilita o campo de escritorio credenciado
	 */
	showEscCredenciado(): void {
		
		this.showEscCred = false;
		if (this.formGrupo.value.assJuridicos.length && 
			this.formGrupo.value.tpAcesso == '3'){
			this.showEscCred = true
		}
	}

	/**
	 * Busca os novos assuntos juridicos a serem vinculados
	 */
	getNovosAssJur() {
		let listAssJurInc: Array<string> = [];
		let assJur:        Array<string> = this.formGrupo.value.assJuridicos;
		let i:             number        = 0;
		
		for (i = 0; i < assJur.length; i++) {
			if (assJur[i] != undefined &&
				this.listAssJurOriginais.findIndex(x => x.codigo == assJur[i]) == -1 ) {
					listAssJurInc.push(assJur[i]);
			}
		}

		return listAssJurInc;
	}

	/**
	 * Busca os novos participantes do grupo
	 */
	getNovosParticipantes() : Array<Participantes> {
		let listNovsPart : Array<Participantes> = [];
		this.listParticipantes.forEach(part => {
			if (!part.original) {
				listNovsPart.push(part);
			}
		})
		return listNovsPart
	}

	/**
	 * Faz a pré-validação do assunto jurídico e do grupo de acesso
	 */
	async vldUserAssJuridico(){

	let nCount:        number        = 0;
	let msg:           string        = '';
	let paramUsrGrupo: string        = '';
	let paramAssJur:   string        = '';
	let formTpAcesso:  string        = this.formGrupo.value.tpAcesso;
	let listAssJurInc: Array<string> = [];

	if (this.getNovosAssJur().length) {
		this.listParticipantes.forEach(info => {
			paramUsrGrupo += nCount > 0 ? ',' + info.codigoUser : info.codigoUser;
			nCount++;
		});
		listAssJurInc = this.getNovosAssJur();
	} else {
		if (this.tpAcessoOriginal != formTpAcesso) {
			this.listParticipantes.forEach(info => {
				paramUsrGrupo += nCount > 0 ? ',' + info.codigoUser : info.codigoUser;
				nCount++;
			});
		} else {
			if (this.getNovosParticipantes().length) { 
				this.getNovosParticipantes().forEach(info => {
					paramUsrGrupo += nCount > 0 ? ',' + info.codigoUser : info.codigoUser;
					nCount++;
				});
				listAssJurInc = this.formGrupo.value.assJuridicos
			}
		}
	}

	nCount = 0;
	listAssJurInc.forEach( assunto => {
		if ( this.jurGenerics.changeUndefinedToEmpty(assunto) != '') {
			paramAssJur += nCount > 0 ? ',' + assunto : assunto;
			nCount++;
		}
	});

	this.jurconsultas.restore();
	this.jurconsultas.setParam('usersGrupo', paramUsrGrupo);
	this.jurconsultas.setParam('assJur', paramAssJur);
	this.jurconsultas.setParam('codGrupo', this.codigoGrupo);
	this.jurconsultas.setParam('tpAcesso', formTpAcesso);

	const data = await this.jurconsultas
		.get('vldUsuxAssJur','Usuário x Assunto Jurídico')
		.toPromise()
		let vldPart: Array<string> = [];
		let tipoAcessOk: boolean   = false;
		let nIndex: number = -1;

		if (data.length > 0) {
			data.UserxAssJur.forEach(element => {
				tipoAcessOk = false;

				if (element.tipoAcessOk) {
					tipoAcessOk = true;

					if ((nIndex = vldPart.findIndex(x => x.indexOf(element.nomeUser) > -1)) > -1) {
						vldPart[nIndex] += ', ' + element.assJuridico;
					} else {
						vldPart.push(element.nomeUser + '(' + element.assJuridico);
					}
				} else {
					throw Error (this.poI18nPipe.transform(
						this.litDetGrupoUsu.notifications.vldUsuTpAces,
						[element.msgTpAcesso]
					));
				}
			} );
			
			if (tipoAcessOk) {
				nCount = 0;
				vldPart.forEach( items => {
					items += ')';
					msg   += nCount == 0 ? items : ', ' + items;
					nCount++;
				});
				
				throw Error (this.poI18nPipe.transform(
					this.litDetGrupoUsu.notifications.vldUsuAssJur,
					[msg]
				));
			}

			this.isLoadingBtnSave = false;
		} else {
			return true
		}
	}

	/**
	 * Chama a deleção do grupo
	 */
	async submitDeleteGrupo() {
		this.isLoadingBtnDelete = true;
		this.formGrupo.patchValue({
			assJuridicos: []
		})
		try {
			await this.submitUsuXPesq(this.codigoGrupo, true);
			try {
				await this.deleteGrupo();
				this.poNotification.success(this.litDetGrupoUsu.notifications.excluido);
				this.previousURL();
			} catch (error) {
				this.poNotification.error(error);
			}
			this.isLoadingBtnDelete = true;
		} catch (error) {
			this.poNotification.error(error);
			this.isLoadingBtnDelete = true;
		}
	}

	/**
	 * Deleta o grupo de usuários
	 */
	async deleteGrupo() {
		this.fwModelService.restore();
		this.fwModelService.setModelo('JURA218');
		const data = await this.fwModelService.delete(this.pkGrupo, 'Deleta grupo de usuários')
		.toPromise()

		if(data)
			return data
		throw new Error("Deu ruim delete");
	}
}

interface AssJuridicoOriginal {
	codigo: string;
	pk: string;
}