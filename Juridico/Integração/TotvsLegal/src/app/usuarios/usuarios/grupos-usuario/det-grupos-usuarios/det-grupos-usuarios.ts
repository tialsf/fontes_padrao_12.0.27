import { PoCheckboxGroupOption, PoComboOption } from '@po-ui/ng-components';

export class DetGruposUsuarios {
	nome:           string = '';
	tpAcesso:       string = '';
	escritorioCred: string = '';

	assJuridicos:  Array<PoCheckboxGroupOption> = [];
	participantes: Array<PoComboOption>         = [];
}