// Constantes de tradução da lista de acessos dos grupos
const acessos = {
	matriz:          "Interno",
	clientes:        "Unidade",
	correspondentes: "Credenciado"
}

// Constantes de tradução do breadcrumb
const breadcrumb = {
	home:      "Home",
	usuarios:  "Gerenciar usuários",
	perfil:    "Perfil",
	gruposGer: "Gerenciar grupos",
	gruposAss: "Associar grupos",
	
}

const notifications = {
	vldUsuAssJur: "O(s) usuário(s) que foi(ram) adicionado(s) neste grupo, possui(em) vínculo com os assuntos jurídicos: {0}!",
	vldUsuTpAces:  "O(s) usuário(s) que foi(ram) adicionado(s) neste grupo, já possui(em) vínculo com o tipo de acesso: {0}!",
}

// Variáveis principais
export const gruposUsuarioPt = {
	associar:          "Associar",
	desassociar:       "Desassociar",
	editar:            "Editar",
	tpAcesso:          "Tipo de acesso",
	qtdUsr:            "Quantidade de usuários",
	assJur:            "Assuntos jurídicos",
	carregando:        "Carregando grupos...",
	btnNovoGrupo:      "Criar novo grupo",
	searchPlaceHolder: "Pesquisar grupos pelo nome",
	errorNoAddAss:     "Não foi possível associar o grupo. Tente novamente.",
	errorNoAddDes:     "Não foi possível desassociar o grupo. Tente novamente.",
	breadcrumb,
	acessos,
	notifications
}