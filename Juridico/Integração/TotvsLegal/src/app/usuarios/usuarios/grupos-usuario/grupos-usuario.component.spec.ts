import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GruposUsuarioComponent } from './grupos-usuario.component';

describe('GruposUsuarioComponent', () => {
  let component: GruposUsuarioComponent;
  let fixture: ComponentFixture<GruposUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GruposUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GruposUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
