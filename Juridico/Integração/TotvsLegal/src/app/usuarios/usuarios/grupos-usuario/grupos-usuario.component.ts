import { Component, OnInit, Input, Pipe, PipeTransform, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { 
	PoListViewAction, 
	PoBreadcrumb, 
	PoI18nService, 
	PoDynamicViewField, 
	PoNotificationService,
	PoI18nPipe
} from '@po-ui/ng-components';
import { 
	FwmodelService, 
	FwModelBody, 
	FwModelBodyModels, 
	FwModelBodyItems 
} from 'src/app/services/fwmodel.service';
import { JurigenericsService } from 'src/app/services/jurigenerics.service';
import { JurconsultasService } from 'src/app/services/jurconsultas.service';

/** @type booleanNumber - informa se deverá ser verdadeiro = 1 ou falso= 0*/
type booleanNumber = 0 | 1;

@Component({
	selector: 'app-grupos-usuario',
	templateUrl: './grupos-usuario.component.html',
	styleUrls: ['./grupos-usuario.component.css']
})
export class GruposUsuarioComponent implements OnInit {

	@Input() filter: string = ''
	@Output() nameUsr: string = ''
	
	// Varáveis
	litGruposUsu :       any;
	breadcrumb :         PoBreadcrumb;
	codParticipante:     string                  = '';
	pageTitle:           string                  = '';
	codUsr:              string                  = '';
	listLabelAcessos:    Array<string>           = [];
	actions:             Array<PoListViewAction> = [];
	listGrupos:          Array<grupos>           = [];
	listNZY:             Array<any>              = [];
	isHideLoadingGrupos: boolean                 = false;
	isAssociar:          boolean                 = true;

	fields: Array<PoDynamicViewField> = [
		{ property: 'acesso'     , label: '', gridColumns: 3 },
		{ property: 'qtd'        , label: '', gridColumns: 3 },
		{ property: 'assJur'     , label: '', gridColumns: 6 }
	];

	/**
	 * Método contrutor do componente
	 */
	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private poI18nService: PoI18nService,
		private poI18nPipe: PoI18nPipe,
		private fwmodel: FwmodelService,
		private JuriGenerics: JurigenericsService,
		private jurconsultas: JurconsultasService,
		private PoNotification: PoNotificationService
	) { 
		if (this.route.snapshot.paramMap.get("codParticipante") != undefined) {
			this.codParticipante = atob(decodeURIComponent(this.route.snapshot.paramMap.get("codParticipante")));
		}

		// Obtém da rota se a tela é referente à gerenciar ou associar grupos
		this.isAssociar = (this.router.url.indexOf('/associar')>-1 ? true : false )

		// Traduções
		this.poI18nService
		.getLiterals({ context: 'gruposUsuario', language: poI18nService.getLanguage() })
		.subscribe(
			(litGruposUsu) => {
				this.litGruposUsu = litGruposUsu;
				this.setBreadCrumb();
				this.setActions();

				// Traduções dos acessos
				this.listLabelAcessos = [
					"",
					this.litGruposUsu.acessos.matriz,
					this.litGruposUsu.acessos.clientes,
					this.litGruposUsu.acessos.correspondentes
				];

				// Trudução das labels dos campos da list view
				this.fields[0].label = this.litGruposUsu.tpAcesso;
				this.fields[1].label = this.litGruposUsu.qtdUsr;
				this.fields[2].label = this.litGruposUsu.assJur;

				// Título da página de acordo com a ação da página
				this.pageTitle = (this.isAssociar ? this.litGruposUsu.breadcrumb.gruposAss : this.litGruposUsu.breadcrumb.gruposGer)

			}
		)
	}

	ngOnInit() {
		this.getUserGroups();
		this.getInfoUser();
	}

	/**
	 * Método responsável pela criação do BreadCrumb 
	 */
	private setBreadCrumb() {
		let grupos : string;
		grupos = (this.isAssociar ? this.litGruposUsu.breadcrumb.gruposAss : this.litGruposUsu.breadcrumb.gruposGer)
		this.breadcrumb = {
			items: [
				{ label: this.litGruposUsu.breadcrumb.home    , link: '/'         },
				{ label: this.litGruposUsu.breadcrumb.usuarios, link: '/Usuarios' },
				{ label: this.litGruposUsu.breadcrumb.perfil  , link: '/Usuarios/perfil/'+btoa(this.codParticipante) },
				{ label: grupos  }
			]
		}
	}

	/**
	 * Método responsável pela montagem das ações utilizadas no po-list-view
	 */
	private setActions() {
		this.actions = [
			{
				label:    (this.isAssociar ? this.litGruposUsu.associar : this.litGruposUsu.desassociar) ,
				type:     'link', icon: 'po-icon-plus',
				action:   (item: grupos) => this.getPkGrupo(item.codigo),
				disabled: (item: grupos) => this.unicoUsr(item.qtd)
			},
			{
				label:    this.litGruposUsu.editar ,
				type:     'link', icon: 'po-icon-edit',
				action:   (item: grupos) => this.openGrupo(item.filial, item.codigo)
			}	
		]
	}

	/**
	 * Abre a tela do grupo de usuários
	 * @param filial filial do grupo
	 * @param codigo código do grupo
	 */
	openGrupo(filial?: string, codigo?: string){
		if( this.JuriGenerics.changeUndefinedToEmpty(codigo) != '' ){
			this.router.navigate(
				['./grupo/' + btoa(filial) + '/' + btoa(codigo) ],
				{relativeTo: this.route}
			);
		} else {
			this.router.navigate(
				['./grupo/cadastro']
				, {relativeTo: this.route}
			);
		}
	}


	/**
	 * Obtém os grupos do participante
	 */
	getUserGroups(){
		let acao: string;
		this.listGrupos = [];
		this.isHideLoadingGrupos = false;
		acao = (this.isAssociar ? "associar" : "desassociar")

		this.jurconsultas.restore();
		this.jurconsultas.setParam('partic' ,this.codParticipante);
		this.jurconsultas.setParam('acaoGrp',acao);
		this.isHideLoadingGrupos = false;

		this.jurconsultas.get('userGroups','Busca os grupos do participante').subscribe((data) => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data) != ''){
				data.grupos.forEach(element => {
					const grupo = {} as grupos;

					grupo.filial    = element.filial;
					grupo.codigo    = element.codigo;
					grupo.nome      = element.descricao;
					grupo.acesso    = this.listLabelAcessos[+element.acesso];
					grupo.qtd       = element.qtdUsr
					grupo.assJur    = element.tpAssJur;
					grupo.codAssJur = element.codAssJur.split("/");
					
					this.listGrupos.push(grupo);
				});
				this.isHideLoadingGrupos = true;
			}else{
				this.isHideLoadingGrupos = true;
			}
		});
		
	}	
	
	/**
	* Obtém os dados do participante
	*/
	getInfoUser(){
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA159");
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.setFilter("RD0_CODIGO='" + this.codParticipante + "'")

		this.fwmodel.get('getInfoUser').subscribe((data) => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data.resources[0]) != ''){
				this.breadcrumb.items[2].label = 
					this.JuriGenerics.findValueByName(data.resources[0].models[0].fields,"RD0_NOME")
				this.codUsr = this.JuriGenerics.findValueByName(data.resources[0].models[0].fields,"RD0_USER")
			}
		})
	}

	/**
	 * Verifica se é o único usuário do grupo
	 * @param qtdUsr quantidade de usuarios do grupo
	 */
	unicoUsr(qtdUsr: number){
		if(!this.isAssociar){
			return (qtdUsr > 1 ? false : true)
		}
	}

	/**
	 * Obtém a pk do grupo a ser associado/ desassociado
	 * @param codGrupo Código do grupo
	 */
	async getPkGrupo(codGrupo: string){
		let tpAcesso : string = '';
		let listAssJuridicos : Array<string> = [];

		this.fwmodel.restore();
		this.fwmodel.setFirstLevel(false)
		this.fwmodel.setModelo("JURA218");
		this.fwmodel.setFilter("NZX_COD='"+ codGrupo +"'");

		const data = await this.fwmodel.get('getInfoUser').toPromise();
		// Armazena os usuários vinculados ao grupo
		this.listNZY = data.resources[0].models[0].models[0].items;
		tpAcesso = this.JuriGenerics.findValueByName(
			data.resources[0].models[0].fields, 
			'NZX_TIPOA'
		);

		if (this.isAssociar) {
			listAssJuridicos = this.listGrupos.find(x => x.codigo == codGrupo).codAssJur;
			try {
				const isValid = 
					await this.vldUserAssJuridico(tpAcesso, codGrupo, listAssJuridicos);
				if (isValid)
					this.alteraGrupo(data.resources[0].pk, codGrupo);
			} catch (error) {
				this.PoNotification.error(error);
			}
		} else {
			this.alteraGrupo(data.resources[0].pk, codGrupo);
		}
		
	}
	
	/**
	 * Faz a pré-validação do assunto jurídico e do grupo de acesso
	 * @param tpAcesso Tipo de acesso do grupo
	 * @param codGrupo Código do grupo
	 * @param listAssuJur Array de assuntos jurídicos associados ao grupo
	 */
	async vldUserAssJuridico(tpAcesso : string, codGrupo : string, listAssuJur : Array<string>){
		let paramAssJur : string = '';
		let nCount      : number = 0;
		let msg         : string = '';

		listAssuJur.forEach( assunto => {
			if ( this.JuriGenerics.changeUndefinedToEmpty(assunto) != '') {
				paramAssJur += nCount > 0 ? ',' + assunto : assunto;
				nCount++;
			}
		});

		this.jurconsultas.restore();
		this.jurconsultas.setParam('usersGrupo', this.codUsr);
		this.jurconsultas.setParam('assJur'    , paramAssJur);
		this.jurconsultas.setParam('codGrupo'  , codGrupo);
		this.jurconsultas.setParam('tpAcesso'  , tpAcesso);
	
		const data = await this.jurconsultas
			.get('vldUsuxAssJur','Usuário x Assunto Jurídico')
			.toPromise()
			let vldPart: Array<string> = [];
			let tipoAcessOk: boolean   = false;
			let nIndex: number = -1;

		if (data.length > 0) {
			data.UserxAssJur.forEach(element => {
				tipoAcessOk = false;

				if (element.tipoAcessOk) {
					tipoAcessOk = true;

					if ((nIndex = vldPart.findIndex(x => x.indexOf(element.nomeUser) > -1)) > -1) {
						vldPart[nIndex] += ', ' + element.assJuridico;
					} else {
						vldPart.push(element.nomeUser + '(' + element.assJuridico);
					}
				} else {
					throw Error (this.poI18nPipe.transform(
						this.litGruposUsu.notifications.vldUsuTpAces,
						[element.msgTpAcesso]
					));
				}
			} );
			
			if (tipoAcessOk) {
				nCount = 0;
				vldPart.forEach( items => {
					items += ')';
					msg   += nCount == 0 ? items : ', ' + items;
					nCount++;
				});
				
				throw Error (this.poI18nPipe.transform(
					this.litGruposUsu.notifications.vldUsuAssJur,
					[msg]
				));
			}
		} else {
			return true
		}
	}


	/**
	 * Associa/ Desassocia usuário do grupo
	 * @param pk       pk do grupo a ser associado/ desassociado
	 * @param codGrupo código do grupo
	 */
	alteraGrupo(pk: string, codGrupo: string){
		
		this.isHideLoadingGrupos = false;

		this.fwmodel.restore();
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.setModelo('JURA218');

		let body = this.bodyPutGrupo(pk, codGrupo);
		let nCount: number = 0;

		this.fwmodel.put(pk, body, "Associa/ Desassocia usuário").subscribe((data) => {
			this.isHideLoadingGrupos = true;
			if (data != '') {
				let achouUsr: boolean = true;
				let retorno =  data.models[0].models[0].items;
				
				for (let i = 0; i < retorno.length; i++) {
					// Busca o usuário associado/ desassociado no grupo
					achouUsr = (this.JuriGenerics.findValueByName(retorno[i].fields,"NZY_CUSER") == this.codUsr ? true : false )
					if(i+1 == retorno.length || achouUsr){
						if(this.isAssociar){
							// Se encontrou o usuário (adicionou), atualiza a lista de grupos a Associar
							if(achouUsr){
								this.getUserGroups();
								break;
							}else{
								this.PoNotification.error(this.litGruposUsu.errorNoAddAss)
							}
						}else{
							// Se não encontrou o usuário (excluiu), atualiza a lista de grupos a Gerenciar
							if(!achouUsr){
								this.getUserGroups();
								break;
							}else{
								this.PoNotification.error(this.litGruposUsu.errorNoAddDes)
							}
						}
					}
				};

			}else{
				this.isHideLoadingGrupos = true;
			}
		});
	}

	/**
	 * Monta o body de exclusão de usuários do grupo
	 * @param pk       pk do grupo
	 * @param codGrupo código do grupo
	 */
	bodyPutGrupo(pk: string, codGrupo: string) {
		let fwModelBody:    FwModelBody = new FwModelBody('JURA218',4, pk);
		let fwModelBodyNZY: FwModelBodyModels;
		let itemNZY:        FwModelBodyItems;
		let deleted:        booleanNumber = (this.isAssociar ? 0 : 1)

		fwModelBody.addModel("NZXMASTER","FIELDS",1).addField("NZX_COD",codGrupo,2)
		//-- Usuário X Grupo (NZY) 
		fwModelBodyNZY = fwModelBody.models[0].addModel("NZYDETAIL", "GRID", 1)

		if(this.listNZY.length){
			// Busca o id da linha do usuário a ser desassociado (para exclusão) ou o último id da grid (para inclusão)
			for (let i = 0; i < this.listNZY.length; i++) {
				if (this.JuriGenerics.findValueByName(this.listNZY[i].fields,"NZY_CUSER") != this.codUsr ){
					fwModelBodyNZY.addItems(0,0)
				} else {
					break
				}
			}
		}

		itemNZY = fwModelBodyNZY.addItems(0,deleted)
		itemNZY.addField("NZY_CGRUP" , codGrupo    ,"C")
		itemNZY.addField("NZY_CUSER" , this.codUsr ,"C")

		return fwModelBody.serialize()
	}
}

// Classe da lista de usuários
interface grupos{
	codigo: string;
	nome:   string;
	qtd:    number;
	acesso: string;
	assJur: string;
	filial: string;
	codAssJur: Array<string>;
}

/**
 * Pipe criado para realizar o filtro das rotinas conforme o usuário digita no input
 * @param name Nome do Pipe
 */
@Pipe({ name: 'filterGroupByName' })
export class FilterGroupByName implements PipeTransform {
	transform(listUsuarios: Array<grupos>, description: string = '') {
		description = description
			.trim()
			.toLowerCase();
		if (description) {
			// Filtra o nome do grupo conforme o conteúdo digitado
			return listUsuarios.filter(
				item => item.nome.toLowerCase().includes(description)
			)
		} else {
			return listUsuarios
		}
	}
}
