
// Constantes de tradução do breadcrumba
const breadcrumb = {
	home:     "Home",
	usuarios: "Gerenciar usuários"
}

// Constantes de tradução da list view
const listview = {
	actAddGrupos: "Adicionar grupos",
	interno:      "Interno",
	externo:      "Externo",
	ativo:        "Ativo",
	inativo:      "Inativo"
	
}

// Constantes principais
export const usuariosPt = {
	carregando:        "Carregando usuários...",
	searchPlaceHolder: "Pesquisar usuário pelo nome ou e-mail ",
	breadcrumb,
	listview
}