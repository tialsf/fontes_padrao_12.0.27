import { Component, OnInit, Input, Pipe, PipeTransform } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PoListViewAction, PoBreadcrumb, PoI18nService, PoDynamicViewField } from '@po-ui/ng-components';
import { FwmodelService } from '../../services/fwmodel.service';
import { JurigenericsService } from '../../services/jurigenerics.service';

// Classe da lista de usuários
class usuarios{
	codigo: string;
	nome:   string;
	tipo:   string;
	ativo:  string;
	status: string;
	email:  string;
	sigla:  string;
	type:   string;
}

@Component({
	selector: 'app-usuarios',
	templateUrl: './usuarios.component.html',
	styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

	// Variáveis
	litUsuarios:        any;
	breadcrumb:         PoBreadcrumb;
	actions:            Array<PoListViewAction>;
	listUsuarios:       Array<usuarios> = [];
	isHideLoadingUsers: boolean         = true;

	fields: Array<PoDynamicViewField> = [
		{ property: 'email', label: '', gridColumns: 4 },
		{ property: 'tipo' , label: '', gridColumns: 4 },
		{ property: 'ativo', label: '', gridColumns: 4 , tag: true }
	];

	@Input() filter: string = ''

	/**
	 * Método contrutor do componente
	 */
	constructor(
		private router:        Router,
		private route:         ActivatedRoute,
		private poI18nService: PoI18nService,
		private fwmodel:       FwmodelService,
		private JuriGenerics:  JurigenericsService
	) { 
		// Traduções
		this.poI18nService
			.getLiterals({ context: 'usuarios', language: poI18nService.getLanguage() })
			.subscribe(
				(litUsuarios) => {
					this.litUsuarios = litUsuarios;
					this.setBreadCrumb();
					this.setActions();
				}
			)
	}

	ngOnInit() {
		this.getParticipantes();
	}

	/**
	 * Método responsável pela criação do BreadCrumb 
	 */
	private setBreadCrumb() {
		this.breadcrumb = {
			items: [
				{ label: this.litUsuarios.breadcrumb.home    , link: '/' },
				{ label: this.litUsuarios.breadcrumb.usuarios, link: '/Usuarios' }
			]
		}
	}

	/**
	 * Método responsável pela montagem das ações utilizadas no po-list-view
	 */
	private setActions() {
		this.actions = [
			{
				label:    this.litUsuarios.listview.actAddGrupos ,
				type:     'link', icon: 'po-icon-plus',
				action:   (item: usuarios) => this.openGrupos(item.codigo),
				disabled: (item: usuarios) => this.isDeactive(item.status) 
			}
		]
	}

	/**
	 * Obtém a lista de participantes
	 */
	getParticipantes(){
		this.fwmodel.restore();
		this.fwmodel.setModelo("JURA159");
		this.fwmodel.setCampoVirtual(true);
		this.fwmodel.setFirstLevel(false);
		this.fwmodel.setFilter("RD0_TPJUR = '1' "); // Participantes do jurídico
		this.fwmodel.setPageSize("1000");

		this.isHideLoadingUsers = false;

		this.fwmodel.get('Busca participantes').subscribe((data) => {
			if(this.JuriGenerics.changeUndefinedToEmpty(data) != ''){
				data.resources.forEach(item => {
					let participante = new usuarios();
					let tipo:  string = this.JuriGenerics.findValueByName(item.models[0].fields,"RD0_TIPO"  );
					let ativo: string = this.JuriGenerics.findValueByName(item.models[0].fields,"RD0_MSBLQL");
					let sigla: string = this.JuriGenerics.changeUndefinedToEmpty(this.JuriGenerics.findValueByName(item.models[0].fields,"RD0_SIGLA" ));
					let nome:  string = this.JuriGenerics.findValueByName(item.models[0].fields,"RD0_NOME"   );

					participante.codigo = this.JuriGenerics.findValueByName(item.models[0].fields,"RD0_CODIGO" );
					participante.nome   = nome + (sigla !== "" ? " (" + sigla + ")" : "");
					participante.email  = this.JuriGenerics.findValueByName(item.models[0].fields,"RD0_EMAIL"  );
					participante.tipo   = ( tipo == "1" ? this.litUsuarios.listview.interno : this.litUsuarios.listview.externo);
					participante.status = ativo
					participante.ativo  = ( ativo == "2" ? this.litUsuarios.listview.ativo : this.litUsuarios.listview.inativo);
					participante.type   = ( status == "2" ? 'success' : 'danger' );
					
					this.listUsuarios.push(participante);
				});
				this.isHideLoadingUsers = true;

			}else{
				this.isHideLoadingUsers = true;
			}
		})
	}

	/**
	 * Abre a página do perfil do usuário
	 * @param usuario código do participante
	 */
	openPerfil(usuario){
		this.router.navigate(["./perfil/"+btoa(usuario.codigo)], {relativeTo: this.route})
	}

	/**
	 * Abre a tela de grupos
	 * @param codPartic código do participante
	 * 
	 */
	openGrupos(codPartic: string){
		this.router.navigate(["./perfil/"+btoa(codPartic)+"/grupos/associar"], {relativeTo: this.route})
	}

	/**
	 * Retorna se deve ou não habilitar a associação de grupos, conforme o status do partcipante
	 * @param item status do participante
	 */
	isDeactive(item: string){
		return item == "1" ? true : false
	}

}


/**
 * Pipe criado para realizar o filtro das rotinas conforme o usuário digita no input
 * @param name Nome do Pipe
 */
@Pipe({ name: 'filterUserByName' })
export class FilterUserByName implements PipeTransform {
	transform(listUsuarios: Array<usuarios>, description: string = '') {
		description = description
			.trim()
			.toLowerCase();

		if (description) {
			// Filtra o nome e e-mail conforme o conteúdo digitado
			return listUsuarios.filter(
				item => item.nome.toLowerCase().includes(description) ||
				(item.email != '' && item.email != undefined ? 
					item.email.toLowerCase().includes(description) : false)
			)
		} else {
			return listUsuarios
		}
	}
}