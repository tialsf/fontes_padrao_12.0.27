#include "protheus.ch" 
#INCLUDE "PARMTYPE.CH"
 
Function FUConsumirWS()  
Local oXML 
    // Criando o objeto Web Service
    _oWSTeste := WSServiceTOTVS():New()
    _oWSTeste:RecuperarPublicacao('TESTE','123')       
    // Mostrar o resultado
    oXML := _oWSTeste:oWSRecuperarPublicacaoResult
Return 

User Function TesteCTS()
  FUConsumirWS()
Return Nil  