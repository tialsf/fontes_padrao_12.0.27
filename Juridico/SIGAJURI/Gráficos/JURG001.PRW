#INCLUDE "JURG001.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH"
#INCLUDE "MSGRAPHI.CH"

// Vetores dos totais mensais
#DEFINE MESES_MESANO  1
#DEFINE MESES_MESANOD 2
#DEFINE MESES_TOTAL   3


//------------------------------------------------------------------------
/*/{Protheus.doc} JURG001
Grafico de processos abertos e encerrados m�s a m�s

@author Ernani Forastieri
@since 05/11/2013
@version P11
/*/
//------------------------------------------------------------------------
Function JURG001( oOwner )
Local aButton := {}
Local aSay    := {}
Local lOk     := .F.

// Mensagens de Tela Inicial
aAdd( aSay, STR0004 ) //Esta rotina ir� gerar um grafico m�s a m�s,
aAdd( aSay, STR0005 ) //referente aos processos abertos e encerrados.
//aAdd( aSay, cDesc3 )
//aAdd( aSay, cDesc4 )
//aAdd( aSay, cDesc5 )
//aAdd( aSay, cDesc6 )
//aAdd( aSay, cDesc7 )

// Botoes Tela Inicial
aAdd(  aButton, {  5, .T., { || Pergunte( 'JURG001', .T. ) } } )
aAdd(  aButton, {  1, .T., { || lOk := .T., FechaBatch() } } )
aAdd(  aButton, {  2, .T., { || lOk := .F., FechaBatch() } } )

dbSelectArea( 'NSZ' )

FormBatch( STR0006,  aSay,  aButton ) //Gr�fico de processos abertos e encerrados m�s a m�s


 //<- Valida��o do c�digo do assunto juridico ->
IF VCodAssJur(MV_PAR05)
 
	If !lOk .OR. !ApMsgYesNo( STR0007 ) //Confirma gera��o do gr�fico ?
		Return NIL
	EndIf
	
	MsgRun( STR0008, '', { || Processa( oOwner ) } ) //Gerando gr�fico, aguarde...
Else
	//JurMsgErro("Aten��o!"+CRLF+CRLF+"Verificar o c�digo do assunto jur�dico na tela de param�tros."+CRLF +"Valor inv�lido.")
	JurMsgErro( STR0001 + CRLF+CRLF + STR0002 + CRLF + STR0003 )
EndIf

MsgRun( STR0008, '', { || Processa( oOwner ) } )

Return NIL


//------------------------------------------------------------------------
/*/{Protheus.doc} Processa
Processamento dos dados

@author Ernani Forastieri
@since 05/11/2013
@version P11
/*/
//------------------------------------------------------------------------
Static Function Processa( oOwner )
//Local aAbrMes     := { 'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez' }
Local aArea       := GetArea()
Local aMesesAbe   := {}
Local aMesesEnc   := {}
Local cAlias      := ''
Local cCampo      := ''
Local cLit        := ''
Local cQuery      := ''
Local dAux 		  := CtoD( '' )
Local dDataRet    := CtoD( '' )
Local dFimPeriodo := CtoD( '' )
Local dIniPeriodo := CtoD( '' )
Local lEntIncl    := .T.
Local nMeses      := 0
Local nPos        := 0
Local nTipoGraf   := 0
Local nX          := 0


Pergunte( 'JURG001', .F. )

nMeses      := MV_PAR01
dDataRet    := CtoD( '15/' + StrZero( MV_PAR02, 2 ) + '/' + StrZero( MV_PAR03, 4 ) )
dIniPeriodo := FirstDay( MonthSub( dDataRet, nMeses ) )
dFimPeriodo := LastDay( dDataRet )
lEntIncl    := ( MV_PAR04 == 1 )
cAssunto    := MV_PAR05
nTipoGraf   := MV_PAR06

//
// Prepara vetor dos meses
//
dAux :=  dDataRet
For nX := nMeses To 1 Step -1
	//aAdd( aMesesAbe,  { SubStr( DToS( dAux ), 1, 6 ), aAbrMes[Month(dAux)]+'/'+ SubString(StrZero(Year(dAux),4),3,2), 0 } )
	aAdd( aMesesAbe,  { SubStr( DToS( dAux ), 1, 6 ), SubStr(MesExtenso(Month(dAux)),1,3)+'/'+ SubString(StrZero(Year(dAux),4),3,2), 0 } )
	dAux := MonthSub( dAux, 1 )
Next
aMesesEnc := aClone( aMesesAbe)


//
// Obtem os dados abertura
//
If lEntIncl
	cCampo := 'NSZ_DTENTR'
	cLit   := STR0009 //Data de Entrada
Else
	cCampo := 'NSZ_DTINCL'
	cLit   := STR0010 //Data de Inclus�o
EndIf

cQuery := ""
cQuery += "SELECT MESANO, COUNT( * ) TOTAL FROM ("
cQuery += "SELECT SUBSTRING(" + cCampo +",1,6) MESANO FROM " + RetSQLName( 'NSZ'  )+ " "
cQuery += "WHERE NSZ_FILIAL = '" + xFilial( 'NSZ' ) + "' "
cQuery +=   "AND " + cCampo +" BETWEEN '" + DToS( dIniPeriodo ) + "' AND '" + DToS( dFimPeriodo ) + "' "
cQuery +=   "AND NSZ_TIPOAS = '" + cAssunto + "' "
cQuery +=   "AND D_E_L_E_T_ = ' ' ) AAAAMM "
cQuery +=   "GROUP BY MESANO "

cQuery := ChangeQuery( cQuery )


cAlias := GetNextAlias()
dbUseArea( .T., "TOPCONN", TcGenQry( ,, cQuery ) , cAlias, .T., .T. )

While !(cAlias)->( EOF() )

	If ( nPos := aScan( aMesesAbe, { |aX| aX[MESES_MESANO] == (cAlias)->MESANO } ) ) > 0
		aMesesAbe[nPos][MESES_TOTAL] := (cAlias)->TOTAL
	EndIf

	(cAlias)->( dbSkip() )
End


(cAlias)->( dbCloseArea() )

aSort( aMesesAbe, , , { |aX,aY| aX[MESES_MESANO] < aY[MESES_MESANO] } )



//
// Obtem os dados encerramentos
//
cQuery := ""
cQuery += "SELECT MESANO, COUNT( * ) TOTAL FROM ("
cQuery += "SELECT SUBSTRING(NSZ_DTENCE,1,6) MESANO FROM " + RetSQLName( 'NSZ'  )+ " "
cQuery += "WHERE NSZ_FILIAL = '" + xFilial( 'NSZ' ) + "' "
cQuery +=   "AND NSZ_TIPOAS = '" + cAssunto + "' "
cQuery +=   "AND NSZ_SITUAC = '2' "
cQuery +=   "AND NSZ_DTENCE BETWEEN '" + DToS( dIniPeriodo ) + "' AND '" + DToS( dFimPeriodo ) + "' "
cQuery +=   "AND D_E_L_E_T_ = ' ' ) AAAAMM "
cQuery +=   "GROUP BY MESANO "
cQuery := ChangeQuery( cQuery )

dbUseArea( .T., "TOPCONN", TcGenQry( ,, cQuery ) , cAlias, .T., .T. )

While !(cAlias)->( EOF() )

	If ( nPos := aScan( aMesesEnc, { |aX| aX[MESES_MESANO] == (cAlias)->MESANO } ) ) > 0
		aMesesEnc[nPos][MESES_TOTAL] := (cAlias)->TOTAL
	EndIf

	(cAlias)->( dbSkip() )
End

(cAlias)->( dbCloseArea() )

aSort( aMesesEnc, , , { |aX,aY| aX[MESES_MESANO] < aY[MESES_MESANO] } )

dbSelectArea( 'NSZ' )

Grafico( oOwner, aMesesAbe, aMesesEnc, nTipoGraf, cLit, cAssunto )

RestArea( aArea )

Return NIL


//------------------------------------------------------------------------
/*/ { Protheus.doc } Grafico
Geracao do Grafico

@author Ernani Forastieri
@since 05/11/2013
@version P11
/*/
//------------------------------------------------------------------------
Static Function Grafico( oOwner, aMesesAbe, aMesesEnc, nTipoGraf, cLit, cAssunto )
Local aCoors  := {}
Local nMax    := 0
Local nSerie1 := 0
Local nSerie2 := 0
Local nX      := 0
//Local oDlg
Local oGrafico
Local oBtn
Local oDlgRel

If oOwner == NIL
	oOwner := oMainWnd
EndIf

aCoors := FWGetDialogSize( oOwner )

//Define Dialog oDlg Title "Gr�fico de processos em aberto m�s a m�s" FROM aCoors[1], aCoors[2] To aCoors[3], aCoors[4] STYLE nOR( WS_VISIBLE, WS_POPUP ) Of oOwner Pixel
Define Dialog oDlgRel Title STR0006 FROM aCoors[1], aCoors[2] To aCoors[3], aCoors[4] Of oOwner Pixel

oGrafico := TMSGraphic():New( aCoors[1], aCoors[2], oDlgRel,,,, aCoors[4]/2, aCoors[3]/2 )
oGrafico:SetMargins( 5, 5, 5, 5 )
oGrafico:SetTitle( STR0011 + JURGETDADOS( "NYB",1,xFILIAL('NYB') + cAssunto, "NYB_DESC"), STR0012 + aMesesAbe[1][MESES_MESANOD] + STR0013 + aTail( aMesesAbe)[MESES_MESANOD] + STR0014 + cLit, CLR_HBLUE, A_CENTER, GRP_TITLE )

If nTipoGraf == 2
	nMax := 0
	aEval( aMesesAbe, { |aX| nMax := Max( aX[MESES_TOTAL], nMax ) } )
	aEval( aMesesEnc, { |aX| nMax := Max( aX[MESES_TOTAL], nMax ) } )

	//nMax := ( Val( Left( Alltrim( Str( nMax ) ), 1 ) ) + 1 ) * ( 10 ^ ( Len( Alltrim( Str( nMax ) ) ) - 1 ) )

	//
	// Limites do Grafico de Linha
	//
	oGrafico:SetRangeY( 0, nMax )
EndIf


//
// Serie das aberturas
//
nSerie1 := oGrafico:CreateSerie( IIf( nTipoGraf == 1, GRP_BAR, GRP_LINE ) , STR0015, 0, .T. ) //Abertos

For nX := 1 To Len( aMesesAbe)
	oGrafico:Add( nSerie1, aMesesAbe[nX][MESES_TOTAL], aMesesAbe[nX][MESES_MESANOD], IIf( nTipoGraf == 1, RGB(205, 215, 235), RGB(50, 80, 120) ) )
Next


//
// Serie dos encerramentos
//

nSerie2 := oGrafico:CreateSerie( IIf( nTipoGraf == 1, GRP_BAR, GRP_LINE ) , STR0016, 0, .T. ) //Encerrados

For nX := 1 To Len( aMesesEnc )
	oGrafico:Add( nSerie2, aMesesEnc[nX][MESES_TOTAL], aMesesEnc[nX][MESES_MESANOD], IIf( nTipoGraf == 1, RGB(50, 80, 120), CLR_RED ) )
Next

// Legenda
oGrafico:SetLegenProp( GRP_SCRBOTTOM, CLR_LIGHTGRAY, GRP_VALUES , .T. )
// Tira efeito 3D
oGrafico:l3D := .F.

//
// Botao dummy para evitar problemas de foco
//
@ -150, -150 Button oBtn Prompt 'ZE' Action oDlgRel:End() Of oDlgRel
oBtn:SetFocus()

Activate Dialog oDlgRel Centered

Return NIL


//--------------------------------------------------------------------
/*/{Protheus.doc} JurEntre
Fun��o Generica para validar esta entre 2 limites

@author TOTVS Protheus
@since  12/11/2013
@version 1.0
/*/
//--------------------------------------------------------------------
User Function JurEntre( xInicial, xFinal, cCampo )
Local lRet := .T.

If ValType( cCampo ) = "U"
	cCampo := &( ReadVar() )
EndIf

If ( cCampo < xInicial ) .OR. ( cCampo > xFinal )
	Help( "", 1, "Help",, I18N( STR0017, { AllTrim( AllToChar( xInicial ) ) , AllTrim( AllToChar( xFinal ) ) , } ) , 1, 1 )
	lRet := .F.
EndIf

Return lRet


//--------------------------------------------------------------------
/*/{Protheus.doc} VCodAssJur
Valida��o do c�digo do assunto jur�dico 

@Param cTmpCod -> C�digo do assunto a ser verificado

@author Rafael Rezende Costa
@since  17/12/2014

@version 1.0
/*/
//--------------------------------------------------------------------
Static Function VCodAssJur(cTmpCod)
Local lRet := .F.

	If !EMPTY(cTmpCod) .AND. JurGetDados('NYB', 1, xFilial('NYB') + cTmpCod, 'NYB_COD') <> " "
		lRet := .T.
	EndIf
	
Return lRet
