#INCLUDE "JURA055.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH" 

Static cTipoAs := ''

//-------------------------------------------
//Cabe�alho dos perfis Contrato e Procura��es
//-------------------------------------------
#DEFINE CAMPOSCAB 'NSZ_COD|NSZ_PATIVO|NSZ_PPASSI|NSZ_DSITUA|'

//-------------------------------------------
//Cabe�alho do perfil Societ�rio             
//-------------------------------------------
#DEFINE CMPCABSOC 'NSZ_COD|NSZ_SOCIED|'

//-------------------------------------------------------------------
/*/{Protheus.doc} JURA055
Cadastro Gen�rico para os assuntos juridicos 
05 - Consultivo / 06 - Contratos / 07 - Procura��es / 08 - Societ�rio

@author Claudio Donizete de Souza
@since 14/10/09
@version 1.0
/*/
//-------------------------------------------------------------------
Function JURA055()
Local oBrowse

oBrowse    := FWMBrowse():New()
oBrowse:SetAlias( "NSZ" )
oBrowse:SetLocate()
oBrowse:SetDescription(STR0001) //"Cadastro de Consultas"
oBrowse:DisableDetails()   
oBrowse:SetCacheView(.F.)
JurSetLeg( oBrowse, "NSZ" )
oBrowse:Activate()

Return NIL

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menu Funcional

@return aRotina - Estrutura
[n,1] Nome a aparecer no cabecalho
[n,2] Nome da Rotina associada
[n,3] Reservado
[n,4] Tipo de Transa��o a ser efetuada:
1 - Pesquisa e Posiciona em um Banco de Dados
2 - Simplesmente Mostra os Campos
3 - Inclui registros no Bancos de Dados
4 - Altera o registro corrente
5 - Remove o registro corrente do Banco de Dados
6 - Altera��o sem inclus�o de registros
7 - C�pia
8 - Imprimir
[n,5] Nivel de acesso
[n,6] Habilita Menu Funcional

@author Claudio Donizete de Souza
@since 14/10/09
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
Local aRotina := {}

aAdd( aRotina, { STR0002, "PesqBrw"        , 0, 1, 0, .T. } )  //"Pesquisar"
aAdd( aRotina, { STR0003, "VIEWDEF.JURA055", 0, 2, 0, NIL } ) //"Visualizar"
aAdd( aRotina, { STR0004, "VIEWDEF.JURA055", 0, 3, 0, NIL } ) //"Incluir"
aAdd( aRotina, { STR0005, "VIEWDEF.JURA055", 0, 4, 0, NIL } ) //"Alterar"
aAdd( aRotina, { STR0006, "VIEWDEF.JURA055", 0, 5, 0, NIL } ) //"Excluir"

Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
View de dados de Assuntos Juridicos - Consultivo

@author Claudio Donizete de Souza
@since 14/10/09
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
Local oView
Local oModel     := FWLoadModel( "JURA055" )
Local oStruct    := FWFormStruct( 2, "NSZ", { | cCampo | JUR055CPO(cCampo, xFilial('NSZ'), NSZ->NSZ_COD) } ) 
Local oStructNT9 := FWFormStruct( 2, "NT9", { | cCampo | JUR055CPO(cCampo, xFilial('NT9'), NSZ->NSZ_COD) } )
Local oStructNUQ := FWFormStruct( 2, "NUQ", { | cCampo | JUR055CPO(cCampo, xFilial('NUQ'), NSZ->NSZ_COD) } )
Local oStruct2   := Nil                        
Local cGrpRest   := JurGrpRest()

//If cTipoAs == '08' 
//	oStruct2 := FWFormStruct( 2, "NSZ", { | cCampo | AllTrim( cCampo ) + '|' $ CMPCABSOC } ) 
//Else
	oStruct2 := FWFormStruct( 2, "NSZ", { | cCampo | AllTrim( cCampo ) + '|' $ CAMPOSCAB } ) 
//EndIf

oStructNT9:RemoveField( "NT9_CAJURI" )      
oStructNUQ:RemoveField( "NUQ_CAJURI" )

oStruct2:aFolders := {}
oStruct2:RemoveField( "NSZ_COD" )

oView := FWFormView():New()
oView:SetModel( oModel )

//-------------------------------------------------
//Estrutura de telas para Consultivo 
//-------------------------------------------------
If cTipoAs == '05' 
	oView:AddField( "JURA055_VIEW", oStruct , "NSZMASTER"  )
		
	oView:CreateFolder("FOLDER_01")   
	oView:AddSheet("FOLDER_01", "ABA_01_01", STR0013 )
		
	oView:createHorizontalBox("BOX_01_F01_A01", 100,,,"FOLDER_01","ABA_01_01")
		
	oView:SetOwnerView( "JURA055_VIEW", "BOX_01_F01_A01"  )
	
//-------------------------------------------------
//Estrutura de telas para Contrato 
//-------------------------------------------------	
ElseIf cTipoAs == '06' 
	oView:AddField( "JURA055_VIEW"   , oStruct   , "NSZMASTER"  )
	oView:AddField( "JURA055_VIEW2"  , oStruct2  , "NSZMASTER2" )
	oView:AddGrid(  "JURA055_GRIDNUQ", oStructNUQ, "NUQDETAIL"  )
	oView:AddGrid(  "JURA055_GRIDNT9", oStructNT9, "NT9DETAIL"  )
	
	oView:CreateFolder("FOLDER_01")
	oView:AddSheet("FOLDER_01", "ABA_01_01", STR0013 )
	oView:AddSheet("FOLDER_01", "ABA_01_02", STR0021 )
	oView:AddSheet("FOLDER_01", "ABA_01_03", STR0014 )

	oView:createHorizontalBox("BOX_01_F01_A011", 10,,,"FOLDER_01","ABA_01_01")
	oView:createHorizontalBox("BOX_01_F01_A01" , 90,,,"FOLDER_01","ABA_01_01")
	oView:createHorizontalBox("BOX_01_F01_A02" ,100,,,"FOLDER_01","ABA_01_02")
	oView:createHorizontalBox("BOX_01_F01_A03" ,100,,,"FOLDER_01","ABA_01_03") 

	oView:SetOwnerView( "JURA055_VIEW"   , "BOX_01_F01_A01"  )
	oView:SetOwnerView( "JURA055_VIEW2"  , "BOX_01_F01_A011" )
	oView:SetOwnerView( "JURA055_GRIDNUQ", "BOX_01_F01_A02"  )	
	oView:SetOwnerView( "JURA055_GRIDNT9", "BOX_01_F01_A03"  )
			
Else
	oView:AddField( "JURA055_VIEW"   , oStruct   , "NSZMASTER"  )
	oView:AddField( "JURA055_VIEW2"  , oStruct2  , "NSZMASTER2" )
	If cTipoAs == '08' 
		oView:AddGrid(  "JURA055_GRIDNUQ", oStructNUQ, "NUQDETAIL"  )
	EndIf
	oView:AddGrid(  "JURA055_GRIDNT9", oStructNT9, "NT9DETAIL"  )

	oView:CreateFolder("FOLDER_01")
	oView:AddSheet("FOLDER_01", "ABA_01_01", STR0013 )
	If cTipoAs == '08'
		oView:AddSheet("FOLDER_01", "ABA_01_02", STR0023 ) 
		oView:AddSheet("FOLDER_01", "ABA_01_03", STR0022 )
	Else
		oView:AddSheet("FOLDER_01", "ABA_01_02", STR0014 )	
    EndIf
	oView:createHorizontalBox("BOX_01_F01_A011", 10,,,"FOLDER_01","ABA_01_01")
	oView:createHorizontalBox("BOX_01_F01_A01" , 90,,,"FOLDER_01","ABA_01_01")
	oView:createHorizontalBox("BOX_01_F01_A02" ,100,,,"FOLDER_01","ABA_01_02")
	If cTipoAs == '08'
		oView:createHorizontalBox("BOX_01_F01_A03" ,100,,,"FOLDER_01","ABA_01_03")
	EndIf
	oView:SetOwnerView( "JURA055_VIEW"   , "BOX_01_F01_A01"  )
	oView:SetOwnerView( "JURA055_VIEW2"  , "BOX_01_F01_A011" )
	If cTipoAs == '08'
		oView:SetOwnerView( "JURA055_GRIDNUQ", "BOX_01_F01_A02"  )
		oView:SetOwnerView( "JURA055_GRIDNT9", "BOX_01_F01_A03"  )	
	Else
		oView:SetOwnerView( "JURA055_GRIDNT9", "BOX_01_F01_A02"  )
	EndIf
EndIf
          
oView:EnableControlBar( .T. )
If JA162AcRst('01') .And. cTipoAs == '08'
	oView:AddUserButton( STR0024, "APTIMG32", { | oView | oModelSAVE := FWModelActive(), MenuPop(NSZ->NSZ_CPRORI, NSZ->NSZ_COD,oView), FWModelActive(oModelSAVE) } )
EndIf
If JA162AcRst('03')
	oView:AddUserButton( STR0010, "CLIPS",    { | oView | JURANEXDOC("NSZ","NSZMASTER","NSZ_COD","NSZ_COD") } )
EndIf	
If JA162AcRst('04')
	oView:AddUserButton( STR0011, "PCOIMG32", { | oView | oModelSAVE := FWModelActive(), JURA100(NSZ->NSZ_COD), FWModelActive(oModelSAVE) } )
EndIf
If JA162AcRst('05')	
	oView:AddUserButton( STR0012, "AGENDA",   { | oView | oModelSAVE := FWModelActive(), JURA106(NSZ->NSZ_COD), FWModelActive(oModelSAVE) } )
EndIf	

If !Empty(cGrpRest)
	oView:SetViewProperty("*","DISABLELOOKUP")
Endif

Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados de Assuntos Juridicos

@author Claudio Donizete de Souza
@since 14/10/09
@version 1.0

@obs NSZMASTER - Dados do Assuntos Juridicos

/*/
//-------------------------------------------------------------------
Static Function Modeldef()
Local oModel     := NIL
Local oStructNT9 := FWFormStruct( 1, "NT9")
Local oStruct    := FWFormStruct( 1, "NSZ")      
Local oStructNUQ := FWFormStruct( 1, "NUQ" )
Local oStruct2   := Nil

//If cTipoAs == '08' 
//	oStruct2 := FWFormStruct( 1, "NSZ", { | cCampo | AllTrim( cCampo ) + '|' $ CMPCABSOC } ) 
//Else
	oStruct2 := FWFormStruct( 1, "NSZ", { | cCampo | AllTrim( cCampo ) + '|' $ CAMPOSCAB } ) 
//EndIf

//*********************************************************************************************
//Altera��o de propriedade do campo NSZ_COD para n�o incluir registro em branco
//*********************************************************************************************
nAux := aScan( oStruct2:aFields, {  |aX| PadR( aX[3], 10 ) == PadR( 'NSZ_COD', 10 ) } )
//obrigat�rio?
oStruct2:aFields[nAux][10] := .F.
//inicializador padr�o
oStruct2:aFields[nAux][11] := {|A,B,C| FWINITCPO(A,B,C), LRETORNO := IF(!INCLUI,NSZ->NSZ_COD,''),FWCLOSECPO(A,B,C,.T.),LRETORNO } 
// virtual?
oStruct2:aFields[nAux][14] := .T.    

oStructNT9:RemoveField( "NT9_CAJURI" )      
oStructNUQ:RemoveField( "NUQ_CAJURI" )

//-----------------------------------------
//Monta o modelo do formul�rio
//-----------------------------------------
oModel:= MPFormModel():New( "JURA055", /*Pre-Validacao*/, {|oX| JA055TOK(oX)}/*Pos-Validacao*/, /*Commit*/,/*Cancel*/)
oModel:AddFields( "NSZMASTER", NIL, oStruct , /*Pre-Validacao*/, /*Pos-Validacao*/ )   

//--------------------------------------
//Estrutura para Contratos, Procura��es e Societ�rio
//--------------------------------------
If cTipoAs == '06' .Or. cTipoAs == '07' .Or. cTipoAs == '08'
	oModel:AddFields( "NSZMASTER2", "NSZMASTER", oStruct2, /*Pre-Validacao*/, /*Pos-Validacao*/ )
	oModel:AddGrid( "NT9DETAIL", "NSZMASTER" /*cOwner*/, oStructNT9, /*bLinePre*/, /*bLinePost*/,/*bPre*/, /*bPost*/ )
	oModel:GetModel( "NT9DETAIL" ):SetDescription( STR0014 ) // "Envolvidos"
	If cTipoAs == '08'                                                        
		oModel:GetModel( "NT9DETAIL" ):SetDescription( STR0022 ) // "Adm/Part. Acion�ria"
	EndIf
	oModel:GetModel( "NT9DETAIL" ):SetUniqueLine( { "NT9_COD" } )
	oModel:SetRelation( "NT9DETAIL", { { "NT9_FILIAL", "XFILIAL('NT9')" }, { "NT9_CAJURI", "NSZ_COD" } }, NT9->( IndexKey( 1 ) ) )
    JurSetRules( oModel, "NT9DETAIL",, "NT9" )
    oModel:SetOnlyQuery( "NSZMASTER2", .T. )    

    If cTipoAs =='06'
		oModel:AddGrid( "NUQDETAIL", "NSZMASTER" /*cOwner*/, oStructNUQ, /*bLinePre*/, /*bLinePost*/,/*bPre*/, /*bPost*/ )    
		oModel:GetModel( "NUQDETAIL" ):SetDescription( STR0021 ) // "Aditivo"
		oModel:GetModel( "NUQDETAIL" ):SetUniqueLine( { "NUQ_COD" } )
		oModel:SetRelation( "NUQDETAIL", { { "NUQ_FILIAL", "XFILIAL('NUQ')" }, { "NUQ_CAJURI", "NSZ_COD" } }, NUQ->( IndexKey( 1 ) ) )
		JurSetRules( oModel, "NUQDETAIL",, "NUQ" )
    EndIf
    
    If cTipoAs == '08'
		oModel:AddGrid( "NUQDETAIL", "NSZMASTER" /*cOwner*/, oStructNUQ, /*bLinePre*/, /*bLinePost*/,/*bPre*/, /*bPost*/ )    
		oModel:GetModel( "NUQDETAIL" ):SetDescription( STR0023 ) // "Unidades"
		oModel:GetModel( "NUQDETAIL" ):SetUniqueLine( { "NUQ_COD" } )
		oModel:SetRelation( "NUQDETAIL", { { "NUQ_FILIAL", "XFILIAL('NUQ')" }, { "NUQ_CAJURI", "NSZ_COD" } }, NUQ->( IndexKey( 1 ) ) )
		JurSetRules( oModel, "NUQDETAIL",, "NUQ" )
    EndIf
    
EndIf

JurSetRules( oModel, "NSZMASTER",, "NSZ" )

If cTipoAs == '05'
	oModel:SetDescription( STR0007 )   //Consultivo
	oModel:GetModel( "NSZMASTER" ):SetDescription( STR0007 )
ElseIf cTipoAs == '06'
	oModel:SetDescription( STR0016 )   //Contratos
	oModel:GetModel( "NSZMASTER" ):SetDescription( STR0016 )
ElseIf cTipoAs == '07'
	oModel:SetDescription( STR0017 )  //Procura��es
	oModel:GetModel( "NSZMASTER" ):SetDescription( STR0017 )
ElseIf cTipoAs == '08'
	oModel:SetDescription( STR0015 )  //Societ�rio
	oModel:GetModel( "NSZMASTER" ):SetDescription( STR0015 ) 		
EndIf  

Return oModel

//-------------------------------------------------------------------
/*/{Protheus.doc} JUR055CPO
Campos a serem exibidos na consulta

@param  cCampo  	Campo a ser verificado	
        cFil      Filial da tabela SUZ
        cAssJur   Codigo do assunto juridico
@Return lRet	 	.T. - O Campo pode ser exibido
                  .F. - O campo n�o pode ser exibido
                  
@author Claudio Donizete de Souza
@since 14/10/09
@version 1.0

@obs Campos obrigatorios ser�o exibidos independentes de configura��o

/*/
//-------------------------------------------------------------------
Static Function JUR055CPO(cCampo, cFil, cAssJur)
Local lRet       := .F.
Local aArea      := GetArea()
Local cTipoAjCPO := ""
Local cAliasTrb
                                   
If IsInCallStack( "JURA162" ) .AND. INCLUI

  cTipoAjCPO := cTipoAj

Elseif NSZ->(dbSeek(xFilial("NSZ") + cAssJur))              

  cTipoAjCPO := NSZ->NSZ_TIPOAS     
  
Endif  

NUZ->( dbSetOrder( 1 ) ) 

If x3Obrigat(cCampo)
	lRet := .T.
Else
	cAliasTrb := GetNextAlias()
	BeginSql Alias cAliasTrb
		SELECT NUZ_FILIAL 
        FROM %table:NUZ%
       WHERE NUZ_FILIAL = %exp:cFil%
             AND NUZ_CAMPO = %exp:cCampo%
             AND NUZ_CTAJUR = %exp:cTipoAjCPO%
             AND %notDel%
	EndSql
	lRet := (cAliasTrb)->(!Eof())
    (cAliasTrb)->( dbCloseArea() )  
Endif	

RestArea( aArea )    

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JA055TOK
Valida informa��es ao salvar.

@param 	oModel  	Model a ser verificado	
@Return lRet	 	.T./.F. As informa��es s�o v�lidas ou n�o

@author Juliana Iwayama Velho
@since 04/11/09
@version 1.0
/*/
//-------------------------------------------------------------------
Function JA055TOK(oModel)
Local lRet      := .T.
Local nOpc      := oModel:GetOperation()

If nOpc == 3 .Or. nOpc == 4    
	
	lRet := JURA95ENC()       
	
	If lRet .And. JA095CAut() .And. Empty(oModel:GetValue('NSZMASTER','NSZ_NUMCAS'))
	  JurMsgErro(STR0018)
	  lRet := .F.	  
	Endif
					
	if lRet
		lRet := JA095VLD() //Valida��o de grupo, cliente, loja e caso
	Endif
	
	if lRet
		lRet := JURA95DTEN()//Valida��o de data de encerramento
	Endif
		
	if lRet .And. (cTipoAs == '06' .Or. cTipoAs == '08')
		lRet := JA095ENVOL() //Valida��o do grid de envolvidos
	Endif
		
	If lRet
		lRet := JURA95VCAU() // Valida��o campos de causa
	EndIf
	
	If lRet
		lRet := JURA95VHIS()// Valida��o campos de hist�rico
	EndIf
	
	If lRet .And. cTipoAs == '08' //Valida��o campos de capital
		lRet :=	JA055VCAPT()
		If lRet
			lRet := JA095IncOk(oModel:GetValue("NSZMASTER","NSZ_COD"))
		EndIf	
	EndIf
	
	If lRet .And. (cTipoAs == '06' .Or. cTipoAs == '07' .Or. cTipoAs == '08')
		lRet := JURA105TOK(oModel)		
	EndIf  	                                                                       
		
EndIf            

If nOpc > 2 .And. lRet
	
	lRet := JURSITPROC(oModel:GetValue('NSZMASTER',"NSZ_COD"), 'MV_JTVENPR',oModel:IsFieldUpdated('NSZMASTER','NSZ_SITUAC'))
	
	If nOpc == 5 .And. lRet
		
		lRet := JurExcAnex ('NSZ',oModel:GetValue("NSZMASTER","NSZ_COD"))
		
	EndIf
			
EndIf

If lRet .And. oModel:lModify
	MsgAlert('Altera��o realizada com sucesso!','Alterar')
Endif 

Return lRet         

//-------------------------------------------------------------------
/*/{Protheus.doc} JA055SetAJ
Atualiza o tipo de assunto jur�dico

@param 	cTipo  	C�digo do tipo de assunto jur�dico

@author Juliana Iwayama Velho
@since 22/03/10
@version 1.0
/*/
//-------------------------------------------------------------------
Function JA055SetAJ(cTipo)
	cTipoAs := cTipo
Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} JA055NSW
Filtra consulta padr�o de subclasse por classe

@Return cRet	 	Comando para filtro
@#JA055NSW()

@author Juliana Iwayama Velho
@since 23/03/10
@version 1.0
/*/
//-------------------------------------------------------------------
Function JA055NSW()
Local cRet := "@#@#"
Local aArea    := GetArea()
Local cCclassNSW := ''
Local oModel   := Nil
Local oCmpPesq := Nil
Local nCt      := 0

If IsPesquisa()
	oCmpPesq := J162CmpPes()
	For nCt := 1 To Len(oCmpPesq)
		If oCmpPesq[nCt]:CNomeCampo == 'NSZ_CCLASS'
			cCclassNSW := oCmpPesq[nCt]:Valor
			If Len(cCclassNSW) > 4
				cCclassNSW := StrTran(cCclassNSW, ";", "','")
				cCclassNSW := LEFT(cCclassNSW, RAT(",", cCclassNSW) - 2)
			Endif 
			Exit
		Endif
	Next
	cRet := Iif(!Empty(cCclassNSW),"@#NSW->NSW_CCLASS IN ('" + cCclassNSW + "')",'')
ElseIf !Empty(FwFldGet('NSZ_CCLASS'))
	cRet := "@#NSW->NSW_CCLASS == '" + FwFldGet('NSZ_CCLASS') + "'@#" 
Endif

Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JA055NSWOK
Valida se o campo de subclasse est� vinculado ao classe

@Return lRet	 	.T./.F. As informa��es s�o v�lidas ou n�o
@sample

@author Juliana Iwayama Velho
@since 23/03/10
@version 1.0
/*/
//-------------------------------------------------------------------
Function JA055NSWOK()
Local lRet     := .F.
Local aArea    := GetArea()
Local aAreaNSW := NSW->( GetArea() )
Local oModel   := FWModelActive()
Local cClasse  := oModel:GetValue("NSZMASTER","NSZ_CCLASS")
Local cSubClas := oModel:GetValue("NSZMASTER","NSZ_CSUBCL")

If !Empty(cSubClas)
	
	NSW->( dbSetOrder( 1 ) )
	NSW->( dbSeek( xFilial( 'NSW' ) + cSubClas ) )
	
	While !NSW->( EOF() ) .AND. xFilial( 'NSW' ) + cSubClas == NSW->NSW_FILIAL + NSW->NSW_COD
		If cClasse == NSW->NSW_CCLASS
			lRet := .T.
		Endif
		NSW->( dbSkip() )
	End
	
	If !lRet
		JurMsgErro(STR0019+RetTitle("NSZ_CSUBCL"))
		lRet := .F.
	EndIf       
	
EndIf

RestArea( aAreaNSW )
RestArea( aArea )

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JA055VCAPT
Valida��o o preenchimento dos campos de valor capital

@Return lRet	 	.T./.F. As informa��es s�o v�lidas ou n�o

@author Juliana Iwayama Velho
@since 24/03/10
@version 1.0
/*/
//-------------------------------------------------------------------
Function JA055VCAPT()
Local lRet     := .T.
Local aArea    := GetArea()

If (!Empty(FwFldGet('NSZ_DTCAPI')) .Or. !Empty(FwFldGet('NSZ_CMOCAP')) .Or. !Empty(FwFldGet('NSZ_VLCAPI')) .Or. !Empty(FwFldGet('NSZ_VLACAO'))) .And.;
	!(!Empty(FwFldGet('NSZ_DTCAPI')) .And. !Empty(FwFldGet('NSZ_CMOCAP')) .And. !Empty(FwFldGet('NSZ_VLCAPI')) .And. !Empty(FwFldGet('NSZ_VLACAO')))
	JurMsgErro(STR0020)
	lRet:= .F.
Else
	lRet := JurVinMoe(FwFldGet('NSZ_DTCAPI'), FwFldGet('NSZ_CMOCAP'))
EndIf

RestArea( aArea )

Return lRet