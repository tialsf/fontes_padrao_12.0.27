#INCLUDE "JURA106B.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} JURA106B
Follow-ups

@author Juliana Iwayama Velho
@since 06/10/09
@version 1.0
/*/

//--------------------------------------------------------------------
/*/{Protheus.doc} JA106GFWIU
Rotina para inclus�o de follow-ups por interven��o do usu�rio a
partir do follow-up padr�o

@param  cAssJur  - C�digo do assunto jur�dico
@param  cCodFw   - C�digo do follow-up
@param  dDtFw    - Data do follow-up
@param  cTipoFw  - Tipo do follow-up
@param  cFwPai   - C�digo do follow-up pai
@param lDtProxEv - Se a data vem do campo prox evento

@author Juliana Iwayama Velho
@since 06/10/09
@version 1.0

@obs NTAMASTER - Dados do Follow-ups
@obs NTEDETAIL - Respons�veis

/*/
//-------------------------------------------------------------------
Function JA106GFWIU(cAssJur, cCodFw, dDtFw , cTipoFw, cFwPai, lDtProxEv)
Local aArea      := GetArea()
Local aAreaNRT   := NRT->( GetArea() )
Local aAreaNVD   := NVD->( GetArea() )
Local cMVBlqFer  := SuperGetMV('MV_JBLQFER',, '2')
Local dNvDtFw    := ctod('')
Local cDescr     := NRT->NRT_DESC

Default lDtProxEv  := .F.

IF Empty(NRT->NRT_DATAT ) .And. Empty( NRT->NRT_QTDED )

	dNvDtFw := JA106GERAF(dDtFw,,NRT->NRT_DATAT,NRT->NRT_QTDED, lDtProxEv)

Else

	If NRT->NRT_DATAT == '2'
		dNvDtFw := dDtFw + NRT->NRT_QTDED
	ElseIf NRT->NRT_DATAT == '1'
		dNvDtFw := dDtFw - NRT->NRT_QTDED
	EndIf

EndIf

If !Empty ( dNvDtFw )
	
	//-------------------------------------------------------------------------
	//Caso o parametro para bloqueio de final de semana e feriado esteja ativo
	//-------------------------------------------------------------------------
	If cMVBlqFer == '1'
		
		dNvDtFw := DataValida( dNvDtFw )
		
	EndIf

	//-------------------------------------------------------------------------	
	//seta vari�veis est�ticas para utilizar na inicializa��o padr�o dos campos
	//campo em branco � o antigo advogado correspondente
	//-------------------------------------------------------------------------

	If Empty(cDescr)
		cDescr := Iif(Empty(NTA->NTA_DESC),NT4->NT4_DESC,NTA->NTA_DESC)
	EndIf

	JURSETXVAR( { NRT->NRT_CTIPOF, dNvDtFw, NRT->NRT_HORAF, NRT->NRT_DURACA, NRT->NRT_CPREPO, ;
				'', NRT->NRT_CRESUL, NRT->NRT_CSUATO, NRT->NRT_CFASE, NRT->NRT_SUGDES, cDescr,;
				NRT->NRT_COD, cAssJur, cCodFw } )
	
	FWExecView(STR0001,'JURA106',3,,{||.T.}) //"Incluir"
	
	JURCLEXVAR()
	
EndIf

RestArea( aAreaNRT )
RestArea( aAreaNVD )
RestArea( aArea )

Return nil

//-------------------------------------------------------------------
/*/{Protheus.doc} JA106GERAF
Rotina para abrir a tela de gerar novo follow-up

@param dData   - Data do follow-up
@param aCoord  - Coordenadas do tamanho da tela
@param cTpData - Tipo de Calculo.
		[1] - Retroativo
		[2] - Futuro
@param nQtdeDias - Quantidades a se calculado
@param lDtProxEv - Se a data vem do campo prox evento, se sim, n�o calcula as datas 

@return dRet        Nova data

@author Juliana Iwayama Velho
@since 07/10/09
@version 1.0

/*/
//-------------------------------------------------------------------
Function JA106GERAF(dData, aCoord, cTipoData, nQtdeDias, lDtProxEv)
Local aArea     := GetArea()
Local dRet      := ctod('')
Local dDataFw   := ctod('')
Local aItems    := {'1=Retroativa','2=Futura'}
Local oDlg, oBtnOk, oBtnCan
Local oGetQtdeDias,oSayQtdeDias,oGetData,oSayData,oCmbTipoData,oSayTipoData,oSayData2
Local oPnlTop, oPnlMid, oPnlMidR, oPnlMidL,oPnlBtn

ParamType 1 Var aCoord  As Array Optional Default { 0, 0, 160, 240 }

DEFAULT nQtdeDias := 0
DEFAULT cTipoData := '2'
Default lDtProxEv := .F.

dDataFw   := JUR106DTFU(cTipoData, dData, nQtdeDias, lDtProxEv)
dRet      := dDataFw    //Se nao atribuir aqui e nada for mudado no dialogo ir� sair em branco.

Define MsDialog oDlg FROM aCoord[1], aCoord[2] To aCoord[3], aCoord[4] Title STR0002 Pixel Of oMainWnd //"Gera Novo Follow-up"

oPnlTop       := tPanel():New(0,0,'',oDlg,,,,,,0,30)
oPnlMid       := tPanel():New(0,0,'',oDlg,,,,,,0,0)
oPnlBtn       := tPanel():New(0,0,'',oDlg,,,,,,0,20)
oPnlTop:Align := CONTROL_ALIGN_TOP
oPnlMid:Align := CONTROL_ALIGN_ALLCLIENT
oPnlBtn:Align := CONTROL_ALIGN_BOTTOM

oPnlMidR 	  := tPanel():New(0,0,'',oPnlMid,,,,,,0,0)
oPnlMidL 	  := tPanel():New(0,0,'',oPnlMid,,,,,,60,0)
oPnlMidR:Align:= CONTROL_ALIGN_ALLCLIENT
oPnlMidL:Align:= CONTROL_ALIGN_LEFT

oPnlBtnR      := tPanel():New(0,0,'',oPnlBtn,,,,,,0,0)
oPnlBtnL      := tPanel():New(0,0,'',oPnlBtn,,,,,,40,0)
oPnlBtnR:Align:= CONTROL_ALIGN_ALLCLIENT
oPnlBtnL:Align:= CONTROL_ALIGN_LEFT

oSayTipoData := tSay():New(01,03,{||STR0003},oPnlTop,,,,,,.T.,,,50,10) //"Tipo Data"
oSayTipoData:lWordWrap   := .T.
oSayTipoData:lTransparent:= .T.

oCmbTipoData := TComboBox():New(10,03,{|u|if(PCount()>0,cTipoData:=u,cTipoData)},;
aItems,50,20,oPnlTop,,{||dRet:= JUR106DTFU(cTipoData, dData, nQtdeDias, lDtProxEv),;
oSayData2:SetText(dRet)/*A��o*/},,,,.T.,,,,,,,,,'cTipoData')

oSayQtdeDias := tSay():New(01,03,{||STR0004},oPnlMidL,,,,,,.T.,,,50,10) //"Num de Dias"
oSayQtdeDias:lWordWrap   := .T.
oSayQtdeDias:lTransparent:= .T.

oGetQtdeDias := TGet():New( 10,03,{|u|if(PCount()>0,nQtdeDias:=u,nQtdeDias)},oPnlMidL,30,10,"999",;
{||dRet:= JUR106DTFU(cTipoData, dData, nQtdeDias, lDtProxEv),;
oSayData2:SetText(dRet)},0,,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F.,,'nQtdeDias',,,, )

oSayData := tSay():New(01,03,{||STR0005},oPnlMidR,,,,,,.T.,,,50,10) //"Data Follow-up"
oSayData:lWordWrap   := .T.
oSayData:lTransparent:= .T.

oSayData2 := tSay():New(10,03,{||dDataFw},oPnlMidR,,,,,,.T.,,,50,10)
oSayData2:lWordWrap   := .F.

@ 03,03 Button oBtnOk  Prompt STR0008  Size 30,10 Pixel Of oPnlBtnL Action ( oDlg:End() ) //'Ok'
@ 03,03 Button oBtnCan Prompt STR0006  Size 30,10 Pixel Of oPnlBtnR Action ( If( ApMsgYesNo(STR0007) ,; //"Cancelar" "Deseja realmente cancelar a inclus�o deste follow-up?"
( dRet := ctod(''), oDlg:End() ), .F.) )

Activate MsDialog oDlg Centered

RestArea( aArea )

Return dRet
