#INCLUDE "JURA113.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH" 

//-------------------------------------------------------------------
/*/{Protheus.doc} JURA113
Campos Integra��o Jur�dica
@author Cl�vis Eduardo Teixeira
@since 15/12/09
@version 1.0
/*/
//-------------------------------------------------------------------
Function JURA113()
Local oBrowse

oBrowse := FWMBrowse():New()
oBrowse:SetDescription( STR0007 )
oBrowse:SetAlias( "NV6" )
oBrowse:SetLocate()
JurSetLeg( oBrowse, "NV6" )
JurSetBSize( oBrowse )
oBrowse:Activate()

Return NIL

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menu Funcional

@return aRotina - Estrutura
[n,1] Nome a aparecer no cabecalho
[n,2] Nome da Rotina associada
[n,3] Reservado
[n,4] Tipo de Transa��o a ser efetuada:
1 - Pesquisa e Posiciona em um Banco de Dados
2 - Simplesmente Mostra os Campos
3 - Inclui registros no Bancos de Dados
4 - Altera o registro corrente
5 - Remove o registro corrente do Banco de Dados
6 - Altera��o sem inclus�o de registros
7 - C�pia
8 - Imprimir
[n,5] Nivel de acesso
[n,6] Habilita Menu Funcional

@author Juliana Iwayama Velho
@since 28/04/09
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
Local aRotina := {}

aAdd( aRotina, { STR0001, "PesqBrw"        , 0, 1, 0, .T. } ) // "Pesquisar"
aAdd( aRotina, { STR0002, "VIEWDEF.JURA113", 0, 2, 0, NIL } ) // "Visualizar"
aAdd( aRotina, { STR0003, "VIEWDEF.JURA113", 0, 3, 0, NIL } ) // "Incluir"
aAdd( aRotina, { STR0004, "VIEWDEF.JURA113", 0, 4, 0, NIL } ) // "Alterar"
aAdd( aRotina, { STR0005, "VIEWDEF.JURA113", 0, 5, 0, NIL } ) // "Excluir"
aAdd( aRotina, { STR0006, "VIEWDEF.JURA113", 0, 8, 0, NIL } ) // "Imprimir"
aAdd( aRotina, { STR0013, "JA113CONFG"     , 0, 3, 0, NIL } ) // "Config. Inicial"
                                              	
Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
View de dados de Campos Integra��o Jur�dica

@author Cl�vis Eduardo Teixeira
@since 15/12/09
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
Local oModel  := FWLoadModel( "JURA113" )
Local oStructNV6
Local oStructNV7
Local oStructNV8
Local oView

oStructNV6 := FWFormStruct( 2, "NV6" )
oStructNV7 := FWFormStruct( 2, "NV7" )
oStructNV8 := FWFormStruct( 2, "NV8" )

oStructNV7:RemoveField( "NV7_CINTJU" )
oStructNV8:RemoveField( "NV8_CADBAS" )

JurSetAgrp( 'NV6',, oStructNV6 )

oView := FWFormView():New()
oView:SetModel( oModel )
oView:AddField( "JURA113_TABELA", oStructNV6, "NV6MASTER"  )   
oView:AddGrid ( "JURA113_RELACI", oStructNV7, "NV7DETAIL"  )
oView:AddGrid ( "JURA113_CAMPOS", oStructNV8, "NV8DETAIL"  )
                                                   
oView:CreateHorizontalBox( "FORMTABELA" , 20 )
oView:CreateHorizontalBox( "FORMRELACI" , 40 )
oView:CreateHorizontalBox( "FORMCAMPOS" , 40 )

oView:SetOwnerView( "NV6MASTER" , "FORMTABELA" )
oView:SetOwnerView( "NV7DETAIL" , "FORMRELACI" )
oView:SetOwnerView( "NV8DETAIL" , "FORMCAMPOS" )

oView:AddIncrementField( "NV7DETAIL" , "NV7_CODIGO" )	
oView:AddIncrementField( "NV8DETAIL" , "NV8_CADBAS" )   

oView:SetUseCursor( .T. )
oView:EnableControlBar( .T. )
                                  
//oView:SetNoInsertLine( "NV7DETAIL" )
//oView:SetNoUpdateLine( "NV7DETAIL" )

Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados de Campos Integra��o Jur�dica

@author Cl�vis Eduardo Teixeira
@since 15/12/09
@version 1.0

@obs NV8MASTER - Dados dos Campos Integra��o Jur�dica

/*/
//-------------------------------------------------------------------
Static Function Modeldef()                                             

Local oStructNV6 := NIL
Local oStructNV7 := NIL
Local oStructNV8 := NIL
Local oModel     := NIL

//-----------------------------------------
//Monta a estrutura do formul�rio com base no dicion�rio de dados
//-----------------------------------------
oStructNV6 := FWFormStruct(1,"NV6")
oStructNV7 := FWFormStruct(1,"NV7")
oStructNV8 := FWFormStruct(1,"NV8")

oStructNV7:RemoveField( "NV7_CINTJU" )
oStructNV8:RemoveField( "NV8_CADBAS" )

//-----------------------------------------
//Monta o modelo do formul�rio
//-----------------------------------------
oModel:= MPFormModel():New( "JURA113", /*Pre-Validacao*/, {|oX|JA113TOK(oX)}/*Pos-Validacao*/, /*Commit*/,/*Cancel*/)

oModel:AddFields( "NV6MASTER", /*cOwner*/, oStructNV6,/*Pre-Validacao*/,/*Pos-Validacao*/)
oModel:GetModel( "NV6MASTER" ):SetDescription( STR0008 ) 

oModel:AddGrid( "NV7DETAIL", "NV6MASTER" /*cOwner*/, oStructNV7, /*bLinePre*/, /*bLinePost*/,/*bPre*/, /*bPost*/ )
oModel:GetModel( "NV7DETAIL"  ):SetDescription( STR0009 )
oModel:SetRelation( "NV7DETAIL", { { "NV7_FILIAL", "XFILIAL('NV6')" }, { "NV7_CINTJU", "NV6_COD" } }, NV7->( IndexKey( 1 ) ) )

oModel:AddGrid( "NV8DETAIL", "NV7DETAIL" /*cOwner*/, oStructNV8, /*bLinePre*/, /*bLinePost*/,/*bPre*/, /*bPost*/ )
oModel:SetRelation( "NV8DETAIL", { { "NV8_FILIAL", "XFILIAL('NV7')" }, { "NV8_CADBAS", "NV7_CODIGO" } }, NV8->( IndexKey( 1 ) ) ) 

oModel:GetModel( "NV7DETAIL" ):SetUniqueLine( { "NV7_CODIGO" } )
 
oModel:GetModel( "NV7DETAIL" ):SetNoUpdateLine(.T.)
oModel:GetModel( "NV7DETAIL" ):SetNoInsertLine(.T.)  
oModel:GetModel( "NV7DETAIL" ):SetNoDeleteLine(.T.)     

oModel:GetModel( "NV8DETAIL" ):SetUniqueLine({'NV8_VLRORI'})
                                                                  
JurSetRules( oModel, "NV6MASTER",, 'NV6' )
JurSetRules( oModel, "NV7DETAIL",, 'NV7' )
JurSetRules( oModel, "NV8DETAIL",, 'NV8' )  

oModel:SetOptional( "NV8DETAIL" , .T. )

Return oModel

//-------------------------------------------------------------------
/*/{Protheus.doc} JA113TOK
Valida informa��es ao salvar

@param 	oModel  	Model a ser verificado
@Return lRet	 	.T./.F. As informa��es s�o v�lidas ou n�o

@author Cl�vis Eduardo Teixeira
@since 15/12/09
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function JA113TOK(oModel)
Local lRet     := .T.
Local aArea    := GetArea()
Local aAreaNV6 := NV6->( GetArea())
Local nOpc     := oModel:GetOperation()  
Local cClien   :=	oModel:GetValue("NV6MASTER","NV6_CCLIEN")
Local clClien  := oModel:GetValue("NV6MASTER","NV6_LOJA")
Local cRotina  := oModel:GetValue("NV6MASTER","NV6_ROTINA")    

	If nOpc == 3
	
		If NV6->(dbSeek(xFilial('NV6') +cClien +clClien +cRotina)) 
		  lRet := .F.                         
		  JurMsgErro(STR0010)	//"S� � permitida uma configura��o de Web-Service por cliente, loja e rotina. Favor verificar!"
		Endif	       
	
	Endif        
	
	If nOpc == 4
	
		If NV6->(dbSeek(xFilial('NV6') +cClien +clClien +cRotina)) .And. (oModel:IsFieldUpdated("NV6MASTER","NV6_CCLIEN") ;
			.Or. oModel:IsFieldUpdated("NV6MASTER","NV6_LOJA") .Or. oModel:IsFieldUpdated("NV6MASTER","NV6_ROTINA"))
		  	lRet := .F.                         
		  	JurMsgErro(STR0010)	//"S� � permitida uma configura��o de Web-Service por cliente, loja e rotina. Favor verificar!"
		Endif	       
	
	Endif 	
	
Return lRet
              
//-------------------------------------------------------------------
/*/{Protheus.doc} JA113NCAMP
Preenche os campos padr�o na tela.
@Return cRet	 	Filtro de campos
@author Cl�vis Eduardo Teixeira
@since 15/12/09
@version 1.0
/*/
//-------------------------------------------------------------------
Function JA113NCAMP(cRotina)   
Local oModel    := FWModelActive() 
Local oModelNV7 := oModel:GetModel('NV7DETAIL') 
Local nOper     := oModel:GetOperation()   
Local aCampos   := JA113CadBase(cRotina) 
Local oView 		:= FWViewActive()   
Local aSaveLines := FWSaveRows()
Local nI       

Local lRet := .T.
	
	if nOper == 3 .Or. nOper == 4   
	 
		if oModelNV7:GetQtdLine() > 0 
		
			oModelNV7:SetNoUpdateLine(.F.)
			oModelNV7:SetNoInsertLine(.F.)  
			oModelNV7:SetNoDeleteLine(.F.)
		
			For nI := 1 To oModelNV7:GetQtdLine()
				oModelNV7:GoLine(nI)
				If !oModelNV7:IsDeleted()
					oModelNV7:DeleteLine()
				EndIf
			Next	 
			oModelNV7:AddLine()
			oModelNV7:DeleteLine()  	
			oModelNV7:DeActivate()
			oModelNV7:Activate() 	  
		Endif
		
		For nI := 1 to Len(aCampos) 
			oModelNV7:AddLine() 
		  oModelNV7:SetValue('NV7_CODIGO', GetSxENum("NV7","NV7_CODIGO"))
		  oModelNV7:SetValue('NV7_TABELA',aCampos[nI][1])
		  oModelNV7:SetValue('NV7_CADBAS',aCampos[nI][2])
		Next  

		oModelNV7:GoLine( 1 )
		aCampos := {}
		
		oModelNV7:SetNoUpdateLine(.T.)
		oModelNV7:SetNoInsertLine(.T.)  
		oModelNV7:SetNoDeleteLine(.T.)			
	Endif 
                 
	FWRestRows( aSaveLines )

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JA113CadBase
Verifica se o campo digitado � permitido a configura��o

@Return lRet	 	.T./.F. As informa��es s�o v�lidas ou n�o

@author Cl�vis Eduardo Teixeira
@since 15/12/09
@version 1.0

/*/
//-------------------------------------------------------------------
Function JA113CadBase(cRotina)
Local aCadBase := {}
                           
if cRotina == '1'
  aAdd(aCadBase, {'SA1', 'Clientes'})
// aAdd(aCadBase, {'NVE', 'Caso'})
	aAdd(aCadBase, {'NST', 'Grupo de Cliente'})
	aAdd(aCadBase, {'NS7', 'Escrit�rio'})
	aAdd(aCadBase, {'NRB', '�rea'})
	aAdd(aCadBase, {'NRL', 'Sub-�rea'})
//	aAdd(aCadBase, {'SU5', 'S�cio - Advogado - Estagi�rio'})
	aAdd(aCadBase, {'NQ0', 'Rito'})
	aAdd(aCadBase, {'NQ4', 'Objeto'})
	aAdd(aCadBase, {'NQ7', 'Progn�stico'})
//	aAdd(aCadBase, {'CTO', 'Moeda'})
	aAdd(aCadBase, {'NW7', 'Forma de Corre��o Monet�ria'})
	aAdd(aCadBase, {'NQI', 'Motivo de Encerramento'})
	aAdd(aCadBase, {'NQ1', 'Natureza Jur�dica'})
	aAdd(aCadBase, {'NQU', 'Tipo de A��o'})
	aAdd(aCadBase, {'NQ6', 'Comarca'})
	aAdd(aCadBase, {'NQC', 'Foro'})
	aAdd(aCadBase, {'NQE', 'Vara'})
	aAdd(aCadBase, {'SA2', 'Correspondente'})
	aAdd(aCadBase, {'NRS', 'Advogado Parte Contraria'})
	aAdd(aCadBase, {'NQQ', 'Decis�o'})
	aAdd(aCadBase, {'NQA', 'Tipo de Envolvimento'})
	aAdd(aCadBase, {'NRP', 'Cargo'})
	aAdd(aCadBase, {'NS6', 'Fun��o'})
	aAdd(aCadBase, {'NTB', 'Local de Trabalho'})
//	aAdd(aCadBase, {'Q3',  'Cargo Participante'})
//	aAdd(aCadBase, {'RJ',  'Fun��o Participante'})
Elseif cRotina == '2'
	aAdd(aCadBase, {'NRO', 'Ato Processual'})
	aAdd(aCadBase, {'NQ6', 'Fase Processual'})
	aAdd(aCadBase, {'NQB', 'Especialidade'})
	aAdd(aCadBase, {'NQL', 'Especialidade'})
	aAdd(aCadBase, {'NQH', 'Juiz'})
Elseif cRotina == '3'
	aAdd(aCadBase, {'NQS', 'Tipos de Follow-Up'})
	aAdd(aCadBase, {'NQM', 'Preposto'})
	aAdd(aCadBase, {'NQN', 'Resultado Follow-Up'})
	aAdd(aCadBase, {'NRT', 'Modelo Follow-Up'})
Elseif cRotina == '4'
	aAdd(aCadBase, {'NQW', 'Tipo de Garantia'})
Elseif cRotina == '5'	
	aAdd(aCadBase, {'NSR', 'Despesa Jur�dica'})
Elseif cRotina == '6' 
	aAdd(aCadBase, {'NSP', 'Tipo de Valores'})
Elseif cRotina == '7'
	aAdd(aCadBase, {'NSQ', 'Tipo de Contrato'})
Endif

Return aCadBase		 

//-------------------------------------------------------------------
/*/{Protheus.doc} JA113VFILT
Filtro da consulta padr�o de campos do De/Para
@Return cRet	 	Contem o filtro que o programa deve realizar
@author Cl�vis Eduardo Teixeira
@since 22/12/09
@version 1.0                                             
/*/
//-------------------------------------------------------------------
Function JA113Filt()
Local cRet := ""    
	
	  If !Empty(FwFldGet('NV7_TABELA')) 
			cRet := FwFldGet('NV7_TABELA')
		Endif	
	
Return cRet