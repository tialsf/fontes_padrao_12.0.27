#INCLUDE "JURA136.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} JURA136
Contas a Pagar desdobramento

@author Clovis E. Teixeira dos Santos
@since 11/06/09
@version 1.0
/*/
//-------------------------------------------------------------------
Function JURA136()
Local oBrowse

oBrowse := FWMBrowse():New()
oBrowse:SetDescription( STR0007 )
oBrowse:SetAlias( "NU6" )
oBrowse:SetLocate()
//oBrowse:DisableDetails()
JurSetLeg( oBrowse, "NU6" )
JurSetBSize( oBrowse )
oBrowse:Activate()

Return NIL


//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menu Funcional

@return aRotina - Estrutura
[n,1] Nome a aparecer no cabecalho
[n,2] Nome da Rotina associada
[n,3] Reservado
[n,4] Tipo de Transa��o a ser efetuada:
1 - Pesquisa e Posiciona em um Banco de Dados
2 - Simplesmente Mostra os Campos
3 - Inclui registros no Bancos de Dados
4 - Altera o registro corrente
5 - Remove o registro corrente do Banco de Dados
6 - Altera��o sem inclus�o de registros
7 - C�pia
8 - Imprimir
[n,5] Nivel de acesso
[n,6] Habilita Menu Funcional

@author Clovis E. Teixeira dos Santos
@since 11/06/09
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
Local aRotina := {}

aAdd( aRotina, { STR0001, "PesqBrw"        , 0, 1, 0, .T. } ) // "Pesquisar"
aAdd( aRotina, { STR0002, "VIEWDEF.JURA136", 0, 2, 0, NIL } ) // "Visualizar"
aAdd( aRotina, { STR0003, "VIEWDEF.JURA136", 0, 3, 0, NIL } ) // "Incluir"
aAdd( aRotina, { STR0004, "VIEWDEF.JURA136", 0, 4, 0, NIL } ) // "Alterar"
aAdd( aRotina, { STR0005, "VIEWDEF.JURA136", 0, 5, 0, NIL } ) // "Excluir"

Return aRotina


//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
View de dados de Contas a Pagar desdobramento

@author Clovis E. Teixeira dos Santos
@since 11/06/09
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()

Local oModel := FwLoadModel( "JURA136" )
Local oStructNU6
Local oStructNU4
Local oView

//--------------------------------------------------------------
//Montagem da interface via dicionario de dados
//--------------------------------------------------------------
oStructNU6 := FWFormStruct( 2, "NU6" )
oStructNU4 := FWFormStruct( 2, "NU4" )
oStructNU4:RemoveField( "NU4_COD" )
oStructNU4:RemoveField( "NU4_DESC" )

//--------------------------------------------------------------
//Montagem do View normal se Container
//--------------------------------------------------------------
JurSetAgrp( 'NU6',, oStructNU6 )

oView := FWFormView():New()
oView:SetModel( oModel )
oView:AddField( "JURA136_VIEW", oStructNU6, "NU6MASTER" )
oView:AddGrid(  "JURA136_GRID", oStructNU4 , "NU4DETAIL"  )
oView:AddIncrementField( "JURA136_GRID", "NU4_ITEM"  )	
oView:CreateHorizontalBox( "FORMFIELD", 20 )
oView:CreateHorizontalBox( "GRID"     , 80 )
oView:SetOwnerView( "JURA136_VIEW", "FORMFIELD" )
oView:SetOwnerView( "JURA136_GRID", "GRID"      )
oView:SetUseCursor( .T. )
oView:SetDescription( STR0007 ) // "Contas a Pagar desdobramento"
oView:EnableControlBar( .T. )
Return oView
	

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados de Contas a Pagar desdobramento

@author Clovis E. Teixeira dos Santos
@since 11/06/09
@version 1.0

@obs NU6MASTER - Cabecalho Contas a Pagar desdobramento / NU4DETAIL - Itens Contas a Pagar desdobramento
/*/
//-------------------------------------------------------------------
Static Function ModelDef()

Local oStructNU6 := NIL
Local oStructNU4 := NIL
Local oModel     := NIL

//-----------------------------------------
//Monta a estrutura do formul�rio com base no dicion�rio de dados
//-----------------------------------------
oStructNU6 := FWFormStruct(1,"NU6")
oStructNU4 := FWFormStruct(1,"NU4")
oStructNU4:RemoveField( "NU4_COD" )
oStructNU4:RemoveField( "NU4_DESC" )


******************************************************************************************
**   A T E N C A O
******************************************************************************************
**
**  Verificar nomes reais dos campos "NU4_COD", "NU4_DESC" e "NU4_ITEM"
**  ajustar o fontes e retirar este comentario....
**
******************************************************************************************
******************************************************************************************


//-----------------------------------------
//Monta o modelo do formul�rio
//-----------------------------------------
oModel:= MpFormModel():New( "JURA136", /*Pre-Validacao*/, /*Pos-Validacao*/ )
oModel:AddFields( "NU6MASTER", /*cOwner*/, oStructNU6, /*Pre-Validacao*/,/*Pos-Validacao*/)
oModel:SetDescription( STR0008 ) // "Modelo de Dados da Contas a Pagar desdobramento"
oModel:GetModel( "NU6MASTER" ):SetDescription( STR0009 ) // "Cabecalho Contas a Pagar desdobramento"
oModel:AddGrid( "NU4DETAIL", "NU6MASTER" /*cOwner*/, oStructNU4, /*bLinePre*/, /*bLinePost*/,/*bPre*/, /*bPost*/ )
oModel:GetModel( "NU4DETAIL" ):SetUniqueLine( { "NU4_ITEM" } )
oModel:SetRelation( "NU4DETAIL", { { "NU4_FILIAL", "XFILIAL('NU4')" }, { "NU4_CODTAB", "NU6_CODTAB" } }, NU4->( IndexKey( 1 ) ) )
oModel:GetModel( "NU4DETAIL" ):SetDescription( STR0010 ) // "Itens Contas a Pagar desdobramento"
JurSetRules( oModel, "NU6MASTER",, "NU6",, "JURA136" )
JurSetRules( oModel, "NU4MASTER" ,, "NU4" ,, "JURA136" )


Return oModel
