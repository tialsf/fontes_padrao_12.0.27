#INCLUDE "PROTHEUS.CH"
#INCLUDE "APWEBSRV.CH"
#INCLUDE "JURA228A.CH"

//-----------------------------------------------------------------------------------------------------------
//	CUIDADO QUANDO FOR GERAR ESTE WSCLIENT, PORQUE AL�M DO WSCLIENT EXISTE FUN��ES NO FIM DO FONTE
//-----------------------------------------------------------------------------------------------------------

/* ===============================================================================
WSDL Location    http://juridico.totvsbpo.com.br:8082/WSDISTRIBUICOES.apw?WSDL
Gerado em        10/17/16 14:26:49
Observa��es      C�digo-Fonte gerado por ADVPL WSDL Client 1.120703
                 Altera��es neste arquivo podem causar funcionamento incorreto
                 e ser�o perdidas caso o c�digo-fonte seja gerado novamente.
=============================================================================== */

User Function _XNSMWLD ; Return  // "dummy" function - Internal Use 

/* -------------------------------------------------------------------------------
WSDL Service JURA228A - Servi�o de valida��o de acesso as distribui��es Totvs 
------------------------------------------------------------------------------- */

WSCLIENT JURA228A

	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD RESET
	WSMETHOD CLONE
	WSMETHOD MTDISTRIBUICOES

	WSDATA   _URL                      AS String
	WSDATA   _HEADOUT                  AS Array of String
	WSDATA   _COOKIES                  AS Array of String
	WSDATA   cUSUARIO                  AS string
	WSDATA   cSENHA                    AS string
	WSDATA   oWSMTDISTRIBUICOESRESULT  AS WSDISTRIBUICOES_ARRAYOFSTRUACESSODIS

ENDWSCLIENT

WSMETHOD NEW WSCLIENT JURA228A
::Init()
If !FindFunction("XMLCHILDEX")
	UserException("O C�digo-Fonte Client atual requer os execut�veis do Protheus Build [7.00.131227A-20160928 NG] ou superior. Atualize o Protheus ou gere o C�digo-Fonte novamente utilizando o Build atual.")
EndIf
Return Self

WSMETHOD INIT WSCLIENT JURA228A
	::oWSMTDISTRIBUICOESRESULT := WSDISTRIBUICOES_ARRAYOFSTRUACESSODIS():New()
Return

WSMETHOD RESET WSCLIENT JURA228A
	::cUSUARIO           := NIL 
	::cSENHA             := NIL 
	::oWSMTDISTRIBUICOESRESULT := NIL 
	::Init()
Return

WSMETHOD CLONE WSCLIENT JURA228A
Local oClone := JURA228A():New()
	oClone:_URL          := ::_URL 
	oClone:cUSUARIO      := ::cUSUARIO
	oClone:cSENHA        := ::cSENHA
	oClone:oWSMTDISTRIBUICOESRESULT :=  IIF(::oWSMTDISTRIBUICOESRESULT = NIL , NIL ,::oWSMTDISTRIBUICOESRESULT:Clone() )
Return oClone

// WSDL Method MTDISTRIBUICOES of Service JURA228A

WSMETHOD MTDISTRIBUICOES WSSEND cUSUARIO,cSENHA WSRECEIVE oWSMTDISTRIBUICOESRESULT WSCLIENT JURA228A
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<MTDISTRIBUICOES xmlns="http://juridico.totvsbpo.com.br:8082/">'
cSoap += WSSoapValue("USUARIO", ::cUSUARIO, cUSUARIO , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("SENHA", ::cSENHA, cSENHA , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</MTDISTRIBUICOES>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://juridico.totvsbpo.com.br:8082/MTDISTRIBUICOES",; 
	"DOCUMENT","http://juridico.totvsbpo.com.br:8082/",,"1.031217",; 
	"http://juridico.totvsbpo.com.br:8082/WSDISTRIBUICOES.apw")

::Init()
::oWSMTDISTRIBUICOESRESULT:SoapRecv( WSAdvValue( oXmlRet,"_MTDISTRIBUICOESRESPONSE:_MTDISTRIBUICOESRESULT","ARRAYOFSTRUACESSODIS",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.


// WSDL Data Structure ARRAYOFSTRUACESSODIS

WSSTRUCT WSDISTRIBUICOES_ARRAYOFSTRUACESSODIS
	WSDATA   oWSSTRUACESSODIS          AS WSDISTRIBUICOES_STRUACESSODIS OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT WSDISTRIBUICOES_ARRAYOFSTRUACESSODIS
	::Init()
Return Self

WSMETHOD INIT WSCLIENT WSDISTRIBUICOES_ARRAYOFSTRUACESSODIS
	::oWSSTRUACESSODIS     := {} // Array Of  WSDISTRIBUICOES_STRUACESSODIS():New()
Return

WSMETHOD CLONE WSCLIENT WSDISTRIBUICOES_ARRAYOFSTRUACESSODIS
	Local oClone := WSDISTRIBUICOES_ARRAYOFSTRUACESSODIS():NEW()
	oClone:oWSSTRUACESSODIS := NIL
	If ::oWSSTRUACESSODIS <> NIL 
		oClone:oWSSTRUACESSODIS := {}
		aEval( ::oWSSTRUACESSODIS , { |x| aadd( oClone:oWSSTRUACESSODIS , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT WSDISTRIBUICOES_ARRAYOFSTRUACESSODIS
	Local nRElem1, oNodes1, nTElem1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNodes1 :=  WSAdvValue( oResponse,"_STRUACESSODIS","STRUACESSODIS",{},NIL,.T.,"O",NIL,NIL) 
	nTElem1 := len(oNodes1)
	For nRElem1 := 1 to nTElem1 
		If !WSIsNilNode( oNodes1[nRElem1] )
			aadd(::oWSSTRUACESSODIS , WSDISTRIBUICOES_STRUACESSODIS():New() )
			::oWSSTRUACESSODIS[len(::oWSSTRUACESSODIS)]:SoapRecv(oNodes1[nRElem1])
		Endif
	Next
Return

// WSDL Data Structure STRUACESSODIS

WSSTRUCT WSDISTRIBUICOES_STRUACESSODIS
	WSDATA   cCODESCRITORIO            AS string
	WSDATA   cNOMERELACIONAL           AS string
	WSDATA   cURL                      AS string
	WSDATA   cTOKEN			           AS string
	WSDATA   cAGRUPADOR		           AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT WSDISTRIBUICOES_STRUACESSODIS
	::Init()
Return Self

WSMETHOD INIT WSCLIENT WSDISTRIBUICOES_STRUACESSODIS
Return

WSMETHOD CLONE WSCLIENT WSDISTRIBUICOES_STRUACESSODIS
	Local oClone := WSDISTRIBUICOES_STRUACESSODIS():NEW()
	oClone:cCODESCRITORIO       := ::cCODESCRITORIO
	oClone:cNOMERELACIONAL      := ::cNOMERELACIONAL
	oClone:cURL                 := ::cURL
	oClone:cTOKEN               := ::cTOKEN
	oClone:cAGRUPADOR           := ::cAGRUPADOR
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT WSDISTRIBUICOES_STRUACESSODIS
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cCODESCRITORIO     :=  WSAdvValue( oResponse,"_CODESCRITORIO","string",NIL,"Property cCODESCRITORIO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cNOMERELACIONAL    :=  WSAdvValue( oResponse,"_NOMERELACIONAL","string",NIL,"Property cNOMERELACIONAL as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cURL               :=  WSAdvValue( oResponse,"_URL","string",NIL,"Property cURL as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTOKEN             :=  WSAdvValue( oResponse,"_TOKEN","string",NIL,"Property cTOKEN as s:string on SOAP Response not found.",NIL,"S",NIL,NIL)
	::cAGRUPADOR         :=  WSAdvValue( oResponse,"_AGRUPADOR","string",NIL,"Property cAGRUPADOR as s:string on SOAP Response not found.",NIL,"S",NIL,NIL)
Return
//-------------------------------------------------------------------
//	FIM DO CLIENTE WEBSERVICES
//-------------------------------------------------------------------

//-------------------------------------------------------------------
/*/{Protheus.doc} J228AcDiTo()
Fun��o que valida o cliente junto a Totvs, para ver se ele est� habilitado
para distribui��es totvs.

@return  aDados
@author  Rafael Tenorio da Costa
@since 	 06/10/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Function J228AcDiTo()

	Local cUsuario	:= SuperGetMV('MV_JINDUSR', , "")
	Local cSenha   	:= SuperGetMV('MV_JINDPSW', , "")
	Local lRetorno	:= .F.
	Local oWS		:= Nil
	Local aDados 	:= {}
	Local nLogin	:= 0
	Local aAcessos	:= {}
	
	oWS := JURA228A():New()
	
	oWS:cUSUARIO := cUsuario
	oWS:cSENHA   := cSenha
	
	If oWS:MTDISTRIBUICOES()
		If oWS:oWsMtDistribuicoesResult <> Nil
													
			aDados	 := oWs:oWsMtDistribuicoesResult:oWsStruAcessoDis
			lRetorno := .T.
			
			For nLogin:=1 To Len(aDados)
				Aadd(aAcessos, {	aDados[nLogin]:cNomeRelacional	,;
									aDados[nLogin]:cCodEscritorio	,;
									aDados[nLogin]:cToken			,;
									aDados[nLogin]:cAgrupador		,;
									aDados[nLogin]:cUrl				} )
			Next nLogin
		EndIf
	EndIf
	
	If !lRetorno
		ConOut( STR0007 + GetWSCError() )		//"Erro ao validar servi�o de monitoramento TOTVS - (MTDISTRIBUICOES): "
	EndIf
	
	FwFreeObj(aDados)
	FwFreeObj(oWS)
	
Return aAcessos

//-------------------------------------------------------------------
/*/{Protheus.doc} J228CfDiTo
Fun��o para dar baixa\confirmar as distribuicoes recebidas da Totvs(Vista)
Uso JURA172.

@param 	 cUrl 		- Url para acesso a confima��o de distribui��es Totvs
@param	 aID 		- IDs da distribui��o que ser� confirmada
@return  lRet   	- Define se a distribui��o foi confirmada com sucesso
@author	 Rafael Tenorio da Costa
@since	 06/10/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Function J228CfDiTo(cUrl, aID)

	Local nC        := 0
	Local lRet      := .F.
	Local cPrxServer:= Alltrim( FWSFPolice("COMUNICATION", "USR_PROXYIP") )	
	Local lProxy   	:= (cPrxServer == "T")
	Local nPrxPort  := Val( FWSFPolice("COMUNICATION", "USR_PROXYPORT") )
	Local cPrxUser  := Alltrim( FWSFPolice("COMUNICATION", "USR_PROXYLOGON") )
	Local cPrxPass  := Alltrim( FWSFPolice("COMUNICATION", "USR_PROXYPSW") )
	Local oWsdl     := Nil
	Local cMensagem := ''
	Local xRet      := ''
	Local cMsg      := ''
	Local oXml      := Nil
	Local cErro     := ""
	Local cAviso    := ""
	Local aSimple	:= {}	
	Local nPos		:= 0
	Local cBaixado	:= "1"		//Informa a vista que ser� mesmo baixado, a vista n�o ira retornar mais esta distribui��o
	Local nID		:= 0
	
	Begin Sequence
	
	  	//Cria o objeto da classe TWsdlManager
	��  oWsdl := TWsdlManager():New()
	���
	� 	//Configura o proxy, se necess�rio.
	    If lProxy
	        oWsdl:SetProxy( cPrxServer, nPrxPort )
	        oWsdl:SetCredentials( cPrxUser, cPrxPass )
	    EndIf
	
	� 	//Faz o parse de uma URL
		cUrl := IIF( At("?WSDL", Upper(cUrl)) == 0, cUrl + "?WSDL", cUrl)
	��  If  !( lRet := oWsdl:ParseURL(cUrl) )
	        cMensagem := STR0001 + oWsdl:cError		//"Problema para configurar a URL do webservice de distribui��o da TOTVS: "
	        Break
	    EndIf
	
	� 	//Define a opera��o
	��  If  !( lRet := oWsdl:SetOperation("ConfirmaDistribuicaoEnviada") )
	        cMensagem := STR0002 + oWsdl:cError		//"Problema para configurar o m�todo do webservice de distribui��o da TOTVS: "
	        Break
	    EndIf
	
	��	//Alterada a localiza��o pois o wsdl traz o endere�o como localhost
		oWsdl:cLocation := StrTran(cUrl, "?WSDL", "")

		//Retona os elementos para preenchimento	
	    aSimple := oWsdl:SimpleInput()
	    
	    For nID:=1 To Len(aID)
	    
			nPos := aScan( aSimple, {|x| x[2] == "ID"} )
	        If !( lRet := oWsdl:SetValue(aSimple[nPos][1], aID[nID]) )
	            cMensagem := STR0003 + oWsdl:cError		//"Problema para configurar os valores da tags : "
	            Break
	        EndIf
	        
	        nPos := aScan( aSimple, {|x| x[2] == "baixado"} )
	        If !( lRet := oWsdl:SetValue(aSimple[nPos][1], cBaixado) )
	            cMensagem := STR0003 + oWsdl:cError		//"Problema para configurar os valores da tags : "
	            Break
	        EndIf
	        
			//Pega a mensagem que sera enviada para Web Service
		    cMsg := oWsdl:GetSoapMsg()
		
		    //Envia a mensagem SOAP ao servidor
		    xRet := oWsdl:SendSoapMsg(cMsg)
		
		  	//Pega a mensagem de resposta
		    xRet := oWsdl:GetSoapResponse()
		  	
		  	//Obtem somente Result Tag do XML de retorno  
		    nC   := At('<ConfirmaDistribuicaoEnviadaResult>', xRet)
		    xRet := SubStr(xRet, nC, Len(xRet))
		    nC   := At('</ConfirmaDistribuicaoEnviadaResult>', xRet) + 38
		    xRet := Left(xRet, nC)
		
		  	//Gera o objeto do Result Tag  
		    oXml := XmlParser(xRet, "_", @cErro, @cAviso)
		     
		    If Empty(oXml)
		        cMensagem := STR0004 + oWsdl:cError		//"Problema no retorno da baixa de distribui��o TOTVS: "
		        Break
		    EndIf
		
		  	//Verifica se distribui��o foi baixada
		    If !( lRet := (oXml:_ConfirmaDistribuicaoEnviadaResult:TEXT == 'true') )
		    	cMensagem := I18n(STR0006, {aID[nID], oXml:_ConfirmaDistribuicaoEnviadaResult:TEXT})	//"Distribui��o ID #1 n�o foi baixada. Retorno WS: #2"
		    	Exit
		    EndIf
	    
	    Next nID
	
	End Sequence
	
	If !Empty(cMensagem)
	    ConOut("J228CfDiTo: " + STR0005 + cMensagem)		//"Erro ao dar baixa na distribui��o TOTVS: "
	    lRet := .F.
	EndIf
	
	//Limpa a mem�ria
	FWFreeObj(oXml)
	FWFreeObj(oWsdl)
	xRet := ""
	cMsg := ""

Return lRet