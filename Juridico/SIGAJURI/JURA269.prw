#INCLUDE "JURA269.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} JURA269
TOTV Legal - Favoritos

@since 24/06/2019
/*/
//-------------------------------------------------------------------
Function JURA269()

Local oBrowse

	oBrowse := FWMBrowse():New()
	oBrowse:SetDescription( STR0001) //-- Favoritos
	oBrowse:SetAlias( "O0V" )
	oBrowse:SetMenuDef( 'JURA269' )
	oBrowse:SetLocate()
	oBrowse:Activate()

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menu Funcional

@return aRotina - Estrutura

@since 24/06/2019
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
Local aRotina := {}

aAdd( aRotina, { STR0002, "PesqBrw"        , 0, 1, 0, .T. } ) // "Pesquisar"
aAdd( aRotina, { STR0003, "VIEWDEF.JURA269", 0, 2, 0, NIL } ) // "Visualizar"
aAdd( aRotina, { STR0004, "VIEWDEF.JURA269", 0, 3, 0, NIL } ) // "Incluir"
aAdd( aRotina, { STR0005, "VIEWDEF.JURA269", 0, 4, 0, NIL } ) // "Alterar"
aAdd( aRotina, { STR0006, "VIEWDEF.JURA269", 0, 5, 0, NIL } ) // "Excluir"
aAdd( aRotina, { STR0007, "VIEWDEF.JURA269", 0, 8, 0, NIL } ) // "Imprimir"

Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados de Processos Favoritos.

@since 24/06/2019
/*/
//-------------------------------------------------------------------
Static Function Modeldef()
Local oModel     := NIL
Local oStructO0V := FWFormStruct( 1, "O0V" )

	oModel:= MPFormModel():New( "JURA269", /*Pre-Validacao*/, /*Pos-Validacao*/, /*Commit*/,/*Cancel*/)

	oModel:SetDescription( STR0008 ) //-- "Cadastro de Favoritos"

	oModel:AddFields( "O0VMASTER", NIL, oStructO0V, /*Pre-Validacao*/, /*Pos-Validacao*/ )

	oModel:GetModel( "O0VMASTER" ):SetDescription( STR0008 ) //-- "Cadastro de Favoritos"

	JurSetRules( oModel, 'O0VMASTER',, 'O0V' )

	oModel:SetPrimaryKey( { "O0V_FILCAJ", "O0V_CAJURI", "O0V_USER"  }  )

Return oModel

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
View de dados de Favoritos.

@since 24/06/2019
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
Local oView      := Nil
Local oModel     := FWLoadModel( "JURA269" )
Local oStructO0V := FWFormStruct( 2, "O0V" )

	oView := FWFormView():New()
	oView:SetModel( oModel )
	oView:AddField( "O0V_VIEW", oStructO0V, "O0VMASTER"  )

	oView:CreateHorizontalBox( "SUPERIOR", 100 )
	oView:SetDescription( STR0008 ) //-- "Cadastro de Favoritos"

Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} J269QryFav()
Fun��o retorna Polos Ativo e Passivo Concatenados, Ato Processual e
Data do �timo Andamento para a funcionalidade de Favoritos

@param   cFilCaj - Filial do Assunto Juridico
@param   cCajuri - Assunto Juridico
@return  aRet    - Dados filtrados na query
		 aRet[1] - Filial Cajuri
		 aRet[2] - Assunto Juridico / Cajuri
		 aRet[3] - Polo Ativo
		 aRet[4] - Polo Passivo
		 aRet[5] - Ato Processual
		 aRet[6] - Data do �ltimo Andamento

@since 25/06/2019
/*/
//-------------------------------------------------------------------
Function J269QryFav( cFilCaj, cCajuri )

Local cPartes := ""
Local cQry    := ""
Local aRet    := {}
Local cAlias  := GetNextAlias()

	cQry := " SELECT NSZ_FILIAL, "
	cQry += 	   " NSZ_COD, "
	cQry += 	   " NSZ_PATIVO, "
	cQry += 	   " NSZ_PPASSI, "
	cQry += 	   " NRO001.NRO_DESC   AS ATO, "
	cQry += 	   " NT4001.NT4_DTANDA DATA_AND "
	cQry += " FROM " + RetSqlName("NSZ") + " NSZ "
	cQry += 	   " LEFT JOIN (SELECT SUB4.NT4_FILIAL, "
	cQry += 						 " SUB4.NT4_COD, "
	cQry += 						 " SUB4.NT4_CAJURI, "
	cQry += 						 " SUB4.NT4_CATO, "
	cQry += 						 " SUB4.NT4_DTANDA "
	cQry += 				  " FROM " + RetSqlName("NT4") + " SUB4 "
	cQry += 						 " LEFT JOIN (SELECT MAX(SUB2.NT4_COD) CODIGO "
	cQry += 									" FROM " + RetSqlName("NT4") + " SUB2 "
	cQry += 										   " INNER JOIN (SELECT SUB1.NT4_FILIAL, "
	cQry += 															  " SUB1.NT4_CAJURI, "
	cQry += 															  " MAX(SUB1.NT4_DTANDA) AS DTAND "
	cQry += 													  " FROM  " + RetSqlName("NT4") + " SUB1 "
	cQry += 													  " WHERE  SUB1.D_E_L_E_T_ = ' ' "
	cQry += 													  " GROUP  BY SUB1.NT4_FILIAL, "
	cQry += 																" SUB1.NT4_CAJURI) NT4002 ON SUB2.NT4_FILIAL = NT4002.NT4_FILIAL "
	cQry += 																						" AND SUB2.NT4_CAJURI = NT4002.NT4_CAJURI "
	cQry += 																						" AND SUB2.NT4_DTANDA = NT4002.DTAND) SUB3 ON SUB4.NT4_COD = SUB3.CODIGO) NT4001 ON NSZ_FILIAL = NT4001.NT4_FILIAL "
	cQry += 																																										" AND NSZ_COD = NT4001.NT4_CAJURI "
	cQry += 		" LEFT JOIN " + RetSqlName("NRO") + " NRO001 ON NT4001.NT4_CATO = NRO001.NRO_COD "
	cQry += " WHERE NSZ.D_E_L_E_T_ = ' ' "
	cQry +=   " AND NSZ_FILIAL = '" + cFilCaj + "' "
	cQry +=   " AND NSZ_COD = '" + cCajuri + "' "
	cQry += " ORDER  BY NT4001.NT4_DTANDA DESC "

	cQry := ChangeQuery(cQry)
	dbUseArea(.T., 'TOPCONN', TcGenQry( ,, cQry ) , cAlias, .T., .F.)

	While !(cAlias)->( EOF() )

		aAdd(aRet, (cAlias)->NSZ_FILIAL)    //-- Filial do Assunto Juridico
		aAdd(aRet, (cAlias)->NSZ_COD)       //-- C�digo do Assunto Juridico
		aAdd(aRet, (cAlias)->NSZ_PATIVO)    //-- Polo Ativo
		aAdd(aRet, (cAlias)->NSZ_PPASSI)    //-- Polo Passivo
		aAdd(aRet, (cAlias)->ATO)           //-- Ato Processual (Movimenta��es)
		aAdd(aRet, (cAlias)->DATA_AND)      //-- Data do utimo Andamento

		(cAlias)->( dbSkip() )
	End

Return aRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J269RFav
Verifica qual informa��o deve retornar em cada campo.

aRet - Dados filtrados pela fun��o J269QryFav()
	 aRet[1] - Filial Cajuri
	 aRet[2] - Assunto Juridico / Cajuri
	 aRet[3] - Polo Ativo
	 aRet[4] - Polo Passivo
	 aRet[5] - Ato Processual
	 aRet[6] - Data do �ltimo Andamento
@return cConteud - Conte�do string para os campos de Partes, Movimenta��o
		dData    - Conte�do date para o campo de Data do ultimo andamento

@since 25/06/2019
/*/
//-------------------------------------------------------------------
Function J269RFav( )

Local cConteud  := ""
Local cPAtivo   := ""
Local cPPassivo := ""
Local cAto      := ""
Local dData     := ""
Local cCampo    := ""
Local cFilCaj   := ""
Local cCajuri   := ""
Local aRet      := {}

//-- guarda a filial do cajuri e o Cajuri
cFilCaj := O0V->O0V_FILCAJ
cCajuri := O0V->O0V_CAJURI

//-- Busca o conteudo de cada campo
aRet := J269QryFav( cFilCaj, cCajuri )

cCampo := ReadVar()

If Len(aRet) > 0
	If ( 'O0V_PARTES' $ cCampo )
		//-- T�tulo do caso
		cPAtivo   := aRet[3]
		cPPassivo := aRet[4]

		//-- Concatena Polo Ativo x Polo Passivo
		cConteud := AllTrim( cPAtivo ) + " x " + AllTrim( cPPassivo )

	ElseIf ( 'O0V_MOVIME' $ cCampo )
		//-- Ato Processual
		cConteud := aRet[5]

	ElseIf ( 'O0V_DATA' $ cCampo )
		//-- Data do ultimo Andamento
		dData    := SToD( aRet[6] )
	EndIf
EndIf

Return IIF( ('O0V_DATA' $ cCampo), dData, cConteud   )

//-------------------------------------------------------------------
/*/{Protheus.doc} JFilExist(cFilPro)
Verifica se a filial existe

@Param cFilPro - Filial do Processo

@Return lExist - L�gico .T./.F.

@since 28/10/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Function JFilExist(cFilPro)
Local lExist := .F.

	lExist := FWFilExist(,cFilPro)

return lExist