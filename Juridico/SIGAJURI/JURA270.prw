#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "JURA270.CH"
//--------------------------------- ----------------------------------
/*/{Protheus.doc} JURA270
Verbas por Pedidos

@author Willian.Kazahaya
@since 07/08/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Function JURA270(cProcesso, cFilFiltro)
	Local aArea     := GetArea()
	Local aAreaNSZ  := NSZ->( GetArea() )

	Default cFilFiltro := xFilial("O0W")

	NSZ->(DbSetOrder(1))
	NSZ->(DbSeek(cFilFiltro + cProcesso))

	nRet := FWExecView(STR0001, "JURA270", 4, , , , , , , , , )

	RestArea(aAreaNSZ)
	RestArea(aArea)
Return NIL

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
View de dados da Solicita��o de documentos

@author Willian.Kazahaya
@since 05/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
	Local oView
	Local oModel  	:= FWLoadModel( "JURA270" )
	Local oStructNSZ := Nil
	Local oStructO0W := Nil
	Local oStructNSY := Nil
	Local aNSZ := {}
	Local aO0W := {'O0W_FILIAL','O0W_DATPED','O0W_CTPPED','O0W_DTPPED','O0W_PROGNO',;
		'O0W_CFRCOR','O0W_DFRCOR','O0W_VPEDID','O0W_VPROVA','O0W_VPOSSI',;
		'O0W_VREMOT','O0W_VINCON','O0W_VATPED','O0W_VATPRO','O0W_VATPOS',;
		'O0W_VATREM','O0W_VATINC','O0W_DTJURO','O0W_PERMUL','O0W_CODWF'}
	Local aNSY := { 'NSY_FILIAL','NSY_CAJURI','NSY_COD'   ,'NSY_DPEVLR','NSY_DPROG' ,;
		'NSY_VLCONT','NSY_VLCONA','NSY_DTJURC','NSY_DTMULC','NSY_PERMUC',;
		'NSY_CFCOR1','NSY_V1VLR' ,'NSY_CCORP1','NSY_CJURP1','NSY_MULAT1',; //1� inst�ncia
	'NSY_V1VLRA',;
		'NSY_CFCOR2','NSY_V2VLR' ,'NSY_CCORP2','NSY_CJURP2','NSY_MULAT2',; //2� inst�ncia
	'NSY_V2VLRA','NSY_CFMUL2','NSY_VLRMU2','NSY_CCORM2','NSY_CJURM2',;
		'NSY_MUATU2',;
		'NSY_CFCORT','NSY_TRVLR' ,'NSY_CCORPT','NSY_CJURPT','NSY_VLRMUT',; //Tribunal
	'NSY_TRVLRA','NSY_VLRMT' ,'NSY_CCORMT','NSY_CJURMT','NSY_MUATT' }

//-- Campos Valores Tributarios
	DbSelectAre("O0W")

	If ColumnPos('O0W_MULTRI') > 0
		aAdd( aO0W, 'O0W_MULTRI' )
	EndIf

	If ColumnPos('O0W_CFCMUL') > 0
		aAdd( aO0W, 'O0W_CFCMUL' )
	EndIf

	If ColumnPos('O0W_PERENC') > 0
		aAdd( aO0W, 'O0W_PERENC' )
	EndIf

	If ColumnPos('O0W_PERHON') > 0
		aAdd( aO0W, 'O0W_PERHON' )
	EndIf

	If ColumnPos('O0W_DTMULT') > 0
		aAdd( aO0W, 'O0W_DTMULT' )
	EndIf

	If ColumnPos('O0W_DETALH') > 0
		aAdd( aO0W, 'O0W_DETALH' )
	EndIf

	If ColumnPos('O0W_REDUT') > 0
		aAdd( aO0W, 'O0W_REDUT' )
		aAdd( aO0W, 'O0W_VLREDU' )
	EndIf
	oStructNSZ := FWFormStruct( 2, "NSZ", {|x| aScan(aNSZ,AllTrim(x))> 0 } )
	oStructO0W := FWFormStruct( 2, "O0W", {|x| aScan(aO0W,AllTrim(x))> 0 } )
	oStructNSY := FWFormStruct( 2, "NSY", {|x| aScan(aNSY,AllTrim(x))> 0 } )

	oStructNSZ:AddField( ;
		"NSZ_AVLCAS"       , ;         // [01] Campo
	"01"               , ;         // [02] Ordem
	STR0002            , ;         // [03] Titulo
	STR0002            , ;         // [04] Descricao
	NIL                , ;         // [05] Help
	"GET"              , ;         // [06] Tipo do campo   COMBO, Get ou CHECK
	"@!"               , ;         // [07] Picture
	, ;         // [08] Bloco de picture Var
	, ;         // [09] Chave para ser usado no LooKUp
	.T.                , ;         // [10] Logico dizendo se o campo pode ser alterado
	'1'                , ;         // [11] Chave para ser usado no LooKUp
	'1'                  )         //

	oStructNSZ:AddField( ;
		"NSZ_TOTPED"       , ;         // [01] Campo
	"02"               , ;         // [02] Ordem
	STR0003            , ;         // [03] Titulo
	STR0003            , ;         // [04] Descricao
	NIL                , ;         // [05] Help
	"GET"              , ;         // [06] Tipo do campo   COMBO, Get ou CHECK
	"@E 999,999,999.99", ;         // [07] Picture
	, ;         // [08] Bloco de picture Var
	, ;         // [09] Chave para ser usado no LooKUp
	.T.                , ;         // [10] Logico dizendo se o campo pode ser alterado
	'1'                , ;         // [11] Chave para ser usado no LooKUp
	'1'                  )         // [10] Logico dizendo se o campo pode ser alterado

	oStructNSZ:AddField( ;
		"NSZ_TATPED"       , ;         // [01] Campo
	"03"               , ;         // [02] Ordem
	STR0009            , ;         // [03] Titulo
	STR0009            , ;         // [04] Descricao
	NIL                , ;         // [05] Help
	"GET"              , ;         // [06] Tipo do campo   COMBO, Get ou CHECK
	"@E 999,999,999.99", ;         // [07] Picture
	, ;         // [08] Bloco de picture Var
	, ;         // [09] Chave para ser usado no LooKUp
	.T.                , ;         // [10] Logico dizendo se o campo pode ser alterado
	'1'                , ;         // [11] Chave para ser usado no LooKUp
	'1'                  )         // [10] Logico dizendo se o campo pode ser alterado

	oStructNSZ:AddField( ;
		"NSZ_TOTPRO"       , ;         // [01] Campo
	"04"               , ;         // [02] Ordem
	STR0004            , ;         // [03] Titulo
	STR0004            , ;         // [04] Descricao
	NIL                , ;         // [05] Help
	"GET"              , ;         // [06] Tipo do campo   COMBO, Get ou CHECK
	"@E 999,999,999.99", ;         // [07] Picture
	, ;         // [08] Bloco de picture Var
	, ;         // [09] Chave para ser usado no LooKUp
	.T.                , ;         // [10] Logico dizendo se o campo pode ser alterado
	'1'                , ;         // [11] Chave para ser usado no LooKUp
	'1'                  )         // [10] Logico dizendo se o campo pode ser alterado

	oStructNSZ:AddField( ;
		"NSZ_TATPRO"       , ;         // [01] Campo
	"05"               , ;         // [02] Ordem
	STR0010            , ;         // [03] Titulo
	STR0010            , ;         // [04] Descricao
	NIL                , ;         // [05] Help
	"GET"              , ;         // [06] Tipo do campo   COMBO, Get ou CHECK
	"@E 999,999,999.99", ;         // [07] Picture
	, ;         // [08] Bloco de picture Var
	, ;         // [09] Chave para ser usado no LooKUp
	.T.                , ;         // [10] Logico dizendo se o campo pode ser alterado
	'1'                , ;         // [11] Chave para ser usado no LooKUp
	'1'                  )         // [10] Logico dizendo se o campo pode ser alterado

	oStructNSZ:AddField( ;
		"NSZ_TOTPOS"       , ;         // [01] Campo
	"06"               , ;         // [02] Ordem
	STR0005            , ;         // [03] Titulo
	STR0005            , ;         // [04] Descricao
	NIL                , ;         // [05] Help
	"GET"              , ;         // [06] Tipo do campo   COMBO, Get ou CHECK
	"@E 999,999,999.99", ;         // [07] Picture
	, ;         // [08] Bloco de picture Var
	, ;         // [09] Chave para ser usado no LooKUp
	.T.                , ;         // [10] Logico dizendo se o campo pode ser alterado
	'1'                , ;         // [11] Chave para ser usado no LooKUp
	'1'                  )         // [10] Logico dizendo se o campo pode ser alterado

	oStructNSZ:AddField( ;
		"NSZ_TATPOS"       , ;         // [01] Campo
	"07"               , ;         // [02] Ordem
	STR0011            , ;         // [03] Titulo
	STR0011            , ;         // [04] Descricao
	NIL                , ;         // [05] Help
	"GET"              , ;         // [06] Tipo do campo   COMBO, Get ou CHECK
	"@E 999,999,999.99", ;         // [07] Picture
	, ;         // [08] Bloco de picture Var
	, ;         // [09] Chave para ser usado no LooKUp
	.T.                , ;         // [10] Logico dizendo se o campo pode ser alterado
	'1'                , ;         // [11] Chave para ser usado no LooKUp
	'1'                  )         // [10] Logico dizendo se o campo pode ser alterado

	oStructNSZ:AddField( ;
		"NSZ_TOTREM"       , ;         // [01] Campo
	"08"               , ;         // [02] Ordem
	STR0006            , ;         // [03] Titulo
	STR0006            , ;         // [04] Descricao
	NIL                , ;         // [05] Help
	"GET"              , ;         // [06] Tipo do campo   COMBO, Get ou CHECK
	"@E 999,999,999.99", ;         // [07] Picture
	, ;         // [08] Bloco de picture Var
	, ;         // [09] Chave para ser usado no LooKUp
	.T.                , ;         // [10] Logico dizendo se o campo pode ser alterado
	'1'                , ;         // [11] Chave para ser usado no LooKUp
	'1'                  )         // [10] Logico dizendo se o campo pode ser alterado

	oStructNSZ:AddField( ;
		"NSZ_TATREM"       , ;         // [01] Campo
	"09"               , ;         // [02] Ordem
	STR0012            , ;         // [03] Titulo
	STR0012            , ;         // [04] Descricao
	NIL                , ;         // [05] Help
	"GET"              , ;         // [06] Tipo do campo   COMBO, Get ou CHECK
	"@E 999,999,999.99", ;         // [07] Picture
	, ;         // [08] Bloco de picture Var
	, ;         // [09] Chave para ser usado no LooKUp
	.T.                , ;         // [10] Logico dizendo se o campo pode ser alterado
	'1'                , ;         // [11] Chave para ser usado no LooKUp
	'1'                  )         // [10] Logico dizendo se o campo pode ser alterado

	oStructNSZ:AddField( ;
		"NSZ_TOTINC"       , ;         // [01] Campo
	"10"               , ;         // [02] Ordem
	STR0007            , ;         // [03] Titulo
	STR0007            , ;         // [04] Descricao
	NIL                , ;         // [05] Help
	"GET"              , ;         // [06] Tipo do campo   COMBO, Get ou CHECK
	"@E 999,999,999.99", ;         // [07] Picture
	, ;         // [08] Bloco de picture Var
	, ;         // [09] Chave para ser usado no LooKUp
	.T.                , ;         // [10] Logico dizendo se o campo pode ser alterado
	'1'                , ;         // [11] Chave para ser usado no LooKUp
	'1'                  )         // [10] Logico dizendo se o campo pode ser alterado


	oStructNSZ:AddField( ;
		"NSZ_TATINC"       , ;         // [01] Campo
	"11"               , ;         // [02] Ordem
	STR0013            , ;         // [03] Titulo
	STR0013            , ;         // [04] Descricao
	NIL                , ;         // [05] Help
	"GET"              , ;         // [06] Tipo do campo   COMBO, Get ou CHECK
	"@E 999,999,999.99", ;         // [07] Picture
	, ;         // [08] Bloco de picture Var
	, ;         // [09] Chave para ser usado no LooKUp
	.T.                , ;         // [10] Logico dizendo se o campo pode ser alterado
	'1'                , ;         // [11] Chave para ser usado no LooKUp
	'1'                  )         // [10] Logico dizendo se o campo pode ser alterado


	oView := FWFormView():New()

	oView:SetModel( oModel )

	oView:AddField( "NSZMASTER" , oStructNSZ, "NSZMASTER"  )
	oView:AddGrid(  "O0WDETAIL" , oStructO0W, "O0WDETAIL" )
	oView:AddGrid(  "NSYDETAIL" , oStructNSY, "NSYDETAIL" )

	oView:CreateHorizontalBox( "FORMRESUM" , 30 )
	oView:CreateHorizontalBox( "GRIDVALOR" , 35 )
	oView:CreateHorizontalBox( "GRIDOBJET" , 35 )

	oView:SetOwnerView( "NSZMASTER" , "FORMRESUM" )
	oView:SetOwnerView( "O0WDETAIL" , "GRIDVALOR" )
	oView:SetOwnerView( "NSYDETAIL" , "GRIDOBJET" )

	oView:SetUseCursor( .T. )
	oView:EnableControlBar( .T. )
Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados da Solicita��o de documentos

@author Willian.Kazahaya
@since 05/02/2018
@version 1.0

@obs O0MMASTER - Dados da Solicita��o de documentos
@obs O0NDETAIL - Documentos da Solicita��o
/*/
//-------------------------------------------------------------------
Static Function Modeldef()
	Local oModel     := NIL
	Local oStructNSZ := NIL
	Local oStructO0W := NIL
	Local oStructNSY := NIL
	Local aNSZ := { 'NSZ_FILIAL','NSZ_COD','NSZ_TIPOAS'}
	Local aO0W := { 'O0W_FILIAL','O0W_COD'   ,'O0W_DATPED','O0W_CTPPED','O0W_DTPPED',;
		'O0W_PROGNO','O0W_CFRCOR','O0W_DFRCOR','O0W_VPEDID','O0W_VPROVA',;
		'O0W_VPOSSI','O0W_VREMOT','O0W_VINCON','O0W_VATPED','O0W_VATPRO',;
		'O0W_VATPOS','O0W_VATREM','O0W_VATINC','O0W_DTJURO','O0W_DTMULT',;
		'O0W_PERMUL','O0W_CODWF', 'O0W_REDUT', 'O0W_VLREDU', 'O0W_CAJURI' }
	Local aNSY := { 'NSY_FILIAL','NSY_CAJURI','NSY_COD'   ,'NSY_CPEVLR','NSY_DPEVLR',;//Detalhe
	'NSY_CPROG' ,'NSY_DPROG' ,'NSY_CINSTA','NSY_PEINVL',;
		'NSY_CCOMON','NSY_PEDATA','NSY_DTJURO','NSY_CMOPED','NSY_PEVLR' ,; //Objeto
	'NSY_DTMULT','NSY_PERMUL',;
		'NSY_CFCORC','NSY_DTCONT','NSY_DTJURC','NSY_INECON','NSY_CMOCON',; //Conting�ncia
	'NSY_DTMULC','NSY_PERMUC','NSY_VLCONT','NSY_CCORPC','NSY_CJURPC',;
		'NSY_MULATC','NSY_VLCONA',;
		'NSY_CFCOR1','NSY_V1DATA','NSY_DTJUR1','NSY_CMOIN1','NSY_DTMUL1','NSY_PERMU1',; //1� inst�ncia
	'NSY_V1INVL','NSY_V1VLR' ,'NSY_CCORP1','NSY_CJURP1','NSY_MULAT1','NSY_V1VLRA',;
		'NSY_CFCOR2','NSY_V2DATA','NSY_CMOIN2','NSY_DTMUL2','NSY_PERMU2',; //2� inst�ncia
	'NSY_CFMUL2','NSY_CMOEM2','NSY_VLRMU2','NSY_DTMUT2','NSY_DTINC2',;
		'NSY_CCORP2','NSY_CJURP2','NSY_MULAT2','NSY_V2VLRA','NSY_CCORM2',;
		'NSY_CJURM2','NSY_MUATU2','NSY_V2INVL','NSY_DTJUR2','NSY_V2VLR' ,;
		'NSY_CFCORT','NSY_TRDATA','NSY_CMOTRI','NSY_DTMUTR','NSY_PERMUT',; //Tribunal
	'NSY_CFMULT','NSY_CMOEMT','NSY_VLRMT' ,'NSY_DTMUTT','NSY_DTINCT',;
		'NSY_CCORPT','NSY_CJURPT','NSY_VLRMUT','NSY_TRVLRA',;
		'NSY_CCORMT','NSY_CJURMT','NSY_MUATT' ,'NSY_TRINVL',;
		'NSY_DTJURT','NSY_TRVLR' ,;
		'NSY_CVERBA', 'NSY_REDUT'} //Outros

	//-- Campos Valores Tribut�rios
	DbSelectAre("O0W")

	If ColumnPos('O0W_MULTRI') > 0
		aAdd( aO0W, 'O0W_MULTRI' )
	EndIf

	If ColumnPos('O0W_CFCMUL') > 0
		aAdd( aO0W, 'O0W_CFCMUL' )
	EndIf

	If ColumnPos('O0W_PERENC') > 0
		aAdd( aO0W, 'O0W_PERENC' )
	EndIf

	If ColumnPos('O0W_PERHON') > 0
		aAdd( aO0W, 'O0W_PERHON' )
	EndIf

	If ColumnPos('O0W_DTMULT') > 0
		aAdd( aO0W, 'O0W_DTMULT' )
	EndIf

	If ColumnPos('O0W_DETALH') > 0
		aAdd( aO0W, 'O0W_DETALH' )
	EndIf

	//-------------------------------------------------------------------------
	//Monta a estrutura do formul�rio com base no dicion�rio de dados usando
	//somente os campos dos Arrays
	//-------------------------------------------------------------------------
	oStructNSZ  := FWFormStruct( 1, "NSZ", {|x| aScan(aNSZ,AllTrim(x))> 0 } )
	oStructO0W  := FWFormStruct( 1, "O0W", {|x| aScan(aO0W,AllTrim(x))> 0 } )
	oStructNSY  := FWFormStruct( 1, "NSY", {|x| aScan(aNSY,AllTrim(x))> 0 } )

	oStructNSZ:AddField(   ;
		STR0002          , ;               // [01] Titulo do campo
	""		         , ;               // [02] ToolTip do campo
	"NSZ_AVLCAS"     , ;               // [03] Id do Field
	"C"              , ;               // [04] Tipo do campo
	50               , ;               // [05] Tamanho do campo
	0                , ;               // [06] Decimal do campo
	, ;                                // [07] Code-block de valida��o do campo
	{|| .F.}         , ;               // [08] Code-block de valida��o When do campo
	, ;                                // [09] Lista de valores permitido do campo
	.F.               ,;               // [10] Indica se o campo tem preenchimento obrigat�rio   ]
	{|| CalcAvalCS(NSZ->NSZ_COD)  },;  // [11] Bloco de c�digo de inicializa��o do campo
	, ;                                // [12] Indica se trata-se de um campo chave.
	, ;                                // [13] Indica se o campo n�o pode receber valor em uma opera��o de update.
	.T.)                               // [14] Indica se o campo � virtual.


	oStructNSZ:AddField(;
		STR0003          ,;                  // [01] Titulo do campo
	""		         ,;                  // [02] ToolTip do campo
	"NSZ_TOTPED"     ,;                  // [03] Id do Field
	"N"              ,;                  // [04] Tipo do campo
	12               ,;                  // [05] Tamanho do campo
	2                ,;                  // [06] Decimal do campo
	, ;                                  // [07] Code-block de valida��o do campo
	{|| .F.}         ,;                  // [08] Code-block de valida��o When do campo
	, ;                                  // [09] Lista de valores permitido do campo
	.F.                 ,;               // [10] Indica se o campo tem preenchimento obrigat�rio   ]
	{|| CalcValTot('00',NSZ->NSZ_COD)},; // [11] Bloco de c�digo de inicializa��o do campo
	, ;                                  // [12] Indica se trata-se de um campo chave.
	, ;                                  // [13] Indica se o campo n�o pode receber valor em uma opera��o de update.
	.T.)                                 // [14] Indica se o campo � virtual.
	oStructNSZ:AddField(;
		STR0009          ,;                  // [01] Titulo do campo
	""		         ,;                  // [02] ToolTip do campo
	"NSZ_TATPED"     ,;                  // [03] Id do Field
	"N"              ,;                  // [04] Tipo do campo
	12               ,;                  // [05] Tamanho do campo
	2                ,;                  // [06] Decimal do campo
	, ;                                  // [07] Code-block de valida��o do campo
	{|| .F.}         ,;                  // [08] Code-block de valida��o When do campo
	, ;                                  // [09] Lista de valores permitido do campo
	.F.                 ,;               // [10] Indica se o campo tem preenchimento obrigat�rio   ]
	{|| CalcValTot('05',NSZ->NSZ_COD)},; // [11] Bloco de c�digo de inicializa��o do campo
	, ;                                  // [12] Indica se trata-se de um campo chave.
	, ;                                  // [13] Indica se o campo n�o pode receber valor em uma opera��o de update.
	.T.)                                 // [14] Indica se o campo � virtual.

	oStructNSZ:AddField(;
		STR0004          ,;               // [01] Titulo do campo
	""		         ,;               // [02] ToolTip do campo
	"NSZ_TOTPRO"     ,;               // [03] Id do Field
	"N"              ,;               // [04] Tipo do campo
	12               ,;               // [05] Tamanho do campo
	2                ,;               // [06] Decimal do campo
	, ;                                // [07] Code-block de valida��o do campo
	{|| .F.}         ,;               // [08] Code-block de valida��o When do campo
	, ;                                // [09] Lista de valores permitido do campo
	.F.               ,;               // [10] Indica se o campo tem preenchimento obrigat�rio   ]
	{|| CalcValTot('01',NSZ->NSZ_COD)},; // [11] Bloco de c�digo de inicializa��o do campo
	, ;                                  // [12] Indica se trata-se de um campo chave.
	, ;                                  // [13] Indica se o campo n�o pode receber valor em uma opera��o de update.
	.T.)                                 // [14] Indica se o campo � virtual.

	oStructNSZ:AddField(;
		STR0010          ,;               // [01] Titulo do campo
	""		         ,;               // [02] ToolTip do campo
	"NSZ_TATPRO"     ,;               // [03] Id do Field
	"N"              ,;               // [04] Tipo do campo
	12               ,;               // [05] Tamanho do campo
	2                ,;               // [06] Decimal do campo
	, ;                                // [07] Code-block de valida��o do campo
	{|| .F.}         ,;               // [08] Code-block de valida��o When do campo
	, ;                                // [09] Lista de valores permitido do campo
	.F.               ,;               // [10] Indica se o campo tem preenchimento obrigat�rio   ]
	{|| CalcValTot('06',NSZ->NSZ_COD)},; // [11] Bloco de c�digo de inicializa��o do campo
	, ;                                  // [12] Indica se trata-se de um campo chave.
	, ;                                  // [13] Indica se o campo n�o pode receber valor em uma opera��o de update.
	.T.)                                 // [14] Indica se o campo � virtual.

	oStructNSZ:AddField(;
		STR0005          ,;               // [01] Titulo do campo
	""		         ,;               // [02] ToolTip do campo
	"NSZ_TOTPOS"     ,;               // [03] Id do Field
	"N"              ,;               // [04] Tipo do campo
	12               ,;               // [05] Tamanho do campo
	2                ,;               // [06] Decimal do campo
	, ;                                // [07] Code-block de valida��o do campo
	{|| .F.}         ,;               // [08] Code-block de valida��o When do campo
	, ;                                // [09] Lista de valores permitido do campo
	.F.               ,;               // [10] Indica se o campo tem preenchimento obrigat�rio   ]
	{|| CalcValTot('02',NSZ->NSZ_COD)},; // [11] Bloco de c�digo de inicializa��o do campo
	, ;                                  // [12] Indica se trata-se de um campo chave.
	, ;                                  // [13] Indica se o campo n�o pode receber valor em uma opera��o de update.
	.T.)                                 // [14] Indica se o campo � virtual.
	oStructNSZ:AddField(;
		STR0011          ,;               // [01] Titulo do campo
	""		         ,;               // [02] ToolTip do campo
	"NSZ_TATPOS"     ,;               // [03] Id do Field
	"N"              ,;               // [04] Tipo do campo
	12               ,;               // [05] Tamanho do campo
	2                ,;               // [06] Decimal do campo
	, ;                                // [07] Code-block de valida��o do campo
	{|| .F.}         ,;               // [08] Code-block de valida��o When do campo
	, ;                                // [09] Lista de valores permitido do campo
	.F.               ,;               // [10] Indica se o campo tem preenchimento obrigat�rio   ]
	{|| CalcValTot('07',NSZ->NSZ_COD)},; // [11] Bloco de c�digo de inicializa��o do campo
	, ;                                  // [12] Indica se trata-se de um campo chave.
	, ;                                  // [13] Indica se o campo n�o pode receber valor em uma opera��o de update.
	.T.)                                 // [14] Indica se o campo � virtual.

	oStructNSZ:AddField(;
		STR0006          ,;               // [01] Titulo do campo
	""		         ,;               // [02] ToolTip do campo
	"NSZ_TOTREM"     ,;               // [03] Id do Field
	"N"              ,;               // [04] Tipo do campo
	12               ,;               // [05] Tamanho do campo
	2                ,;               // [06] Decimal do campo
	, ;                                // [07] Code-block de valida��o do campo
	{|| .F.}         ,;               // [08] Code-block de valida��o When do campo
	, ;                                // [09] Lista de valores permitido do campo
	.F.               ,;               // [10] Indica se o campo tem preenchimento obrigat�rio   ]
	{|| CalcValTot('03',NSZ->NSZ_COD)},; // [11] Bloco de c�digo de inicializa��o do campo
	, ;                                  // [12] Indica se trata-se de um campo chave.
	, ;                                  // [13] Indica se o campo n�o pode receber valor em uma opera��o de update.
	.T.)                                 // [14] Indica se o campo � virtual.
	oStructNSZ:AddField(;
		STR0012          ,;               // [01] Titulo do campo
	""		         ,;               // [02] ToolTip do campo
	"NSZ_TATREM"     ,;               // [03] Id do Field
	"N"              ,;               // [04] Tipo do campo
	12               ,;               // [05] Tamanho do campo
	2                ,;               // [06] Decimal do campo
	, ;                                // [07] Code-block de valida��o do campo
	{|| .F.}         ,;               // [08] Code-block de valida��o When do campo
	, ;                                // [09] Lista de valores permitido do campo
	.F.               ,;               // [10] Indica se o campo tem preenchimento obrigat�rio   ]
	{|| CalcValTot('08',NSZ->NSZ_COD)},; // [11] Bloco de c�digo de inicializa��o do campo
	, ;                                  // [12] Indica se trata-se de um campo chave.
	, ;                                  // [13] Indica se o campo n�o pode receber valor em uma opera��o de update.
	.T.)                                 // [14] Indica se o campo � virtual.

	oStructNSZ:AddField(;
		STR0007          ,;                  // [01] Titulo do campo
	""		         ,;                  // [02] ToolTip do campo
	"NSZ_TOTINC"     ,;                  // [03] Id do Field
	"N"              ,;                  // [04] Tipo do campo
	12               ,;                  // [05] Tamanho do campo
	2                ,;                  // [06] Decimal do campo
	, ;                                  // [07] Code-block de valida��o do campo
	{|| .F.}         ,;                  // [08] Code-block de valida��o When do campo
	, ;                                  // [09] Lista de valores permitido do campo
	.F.               ,;                 // [10] Indica se o campo tem preenchimento obrigat�rio   ]
	{|| CalcValTot('04',NSZ->NSZ_COD)},; // [11] Bloco de c�digo de inicializa��o do campo
	, ;                                  // [12] Indica se trata-se de um campo chave.
	, ;                                  // [13] Indica se o campo n�o pode receber valor em uma opera��o de update.
	.T.)                                 // [14] Indica se o campo � virtual.
	oStructNSZ:AddField(;
		STR0013          ,;               // [01] Titulo do campo
	""		         ,;               // [02] ToolTip do campo
	"NSZ_TATINC"     ,;               // [03] Id do Field
	"N"              ,;               // [04] Tipo do campo
	12               ,;               // [05] Tamanho do campo
	2                ,;               // [06] Decimal do campo
	, ;                                // [07] Code-block de valida��o do campo
	{|| .F.}         ,;               // [08] Code-block de valida��o When do campo
	, ;                                // [09] Lista de valores permitido do campo
	.F.               ,;               // [10] Indica se o campo tem preenchimento obrigat�rio   ]
	{|| CalcValTot('09',NSZ->NSZ_COD)},; // [11] Bloco de c�digo de inicializa��o do campo
	, ;                                  // [12] Indica se trata-se de um campo chave.
	, ;                                  // [13] Indica se o campo n�o pode receber valor em uma opera��o de update.
	.T.)                                 // [14] Indica se o campo � virtual.

	oStructO0W:AddField(;
		"URL Fluig"      ,;               // [01] Titulo do campo
	""		         ,;               // [02] ToolTip do campo
	"O0W_URLFLG"     ,;               // [03] Id do Field
	"C"              ,;               // [04] Tipo do campo
	254              ,;               // [05] Tamanho do campo
	0                ,;               // [06] Decimal do campo
	, ;                               // [07] Code-block de valida��o do campo
	{|| .F.}         ,;               // [08] Code-block de valida��o When do campo
	, ;                               // [09] Lista de valores permitido do campo
	.F.              ,;               // [10] Indica se o campo tem preenchimento obrigat�rio   ]
	{|| JMontaURL(Iif(O0W->( FieldPos("O0W_CODWF") ) > 0 , O0W->O0W_CODWF, ""))},;  // [11] Bloco de c�digo de inicializa��o do campo
	, ;                               // [12] Indica se trata-se de um campo chave.
	, ;                               // [13] Indica se o campo n�o pode receber valor em uma opera��o de update.
	.T.)                              // [14] Indica se o campo � virtual.

	// Inclui as valida��es nos campos. N�o est� no Dicion�rio pois usa elementos do Modelo
	oStructO0W:SetProperty('O0W_CTPPED',MODEL_FIELD_VALID,{|oModelAtu,cField,nValAtu,nLinha,nValAnt| ExistCpo('NSP', FwFldGet("O0W_CTPPED"),1).AND.JCallJ094(oModel,cField,nLinha,nValAtu,nValAnt)})
	oStructO0W:SetProperty('O0W_DATPED',MODEL_FIELD_VALID,{|oModelAtu,cField,nValAtu,nLinha,nValAnt| JCallJ094(oModel,cField,nLinha,nValAtu,nValAnt)})
	oStructO0W:SetProperty('O0W_DTJURO',MODEL_FIELD_VALID,{|oModelAtu,cField,nValAtu,nLinha,nValAnt| JCallJ094(oModel,cField,nLinha,nValAtu,nValAnt)})
	oStructO0W:SetProperty('O0W_DTMULT',MODEL_FIELD_VALID,{|oModelAtu,cField,nValAtu,nLinha,nValAnt| JCallJ094(oModel,cField,nLinha,nValAtu,nValAnt)})
	oStructO0W:SetProperty('O0W_PERMUL',MODEL_FIELD_VALID,{|oModelAtu,cField,nValAtu,nLinha,nValAnt| JCallJ094(oModel,cField,nLinha,nValAtu,nValAnt)})
	oStructO0W:SetProperty('O0W_CFRCOR',MODEL_FIELD_VALID,{|oModelAtu,cField,nValAtu,nLinha,nValAnt| JCallJ094(oModel,cField,nLinha,nValAtu,nValAnt)})
	oStructO0W:SetProperty('O0W_VPEDID',MODEL_FIELD_VALID,{|oModelAtu,cField,nValAtu,nLinha,nValAnt| JCallJ094(oModel,cField,nLinha,nValAtu,nValAnt), JUpdTotal('00',oModel,       ,      , oModelAtu)})
	oStructO0W:SetProperty('O0W_VPROVA',MODEL_FIELD_VALID,{|oModelAtu,cField,nValAtu,nLinha,nValAnt| JCallJ094(oModel,cField,nLinha,nValAtu,nValAnt), JUpdTotal('01',oModel,nValAtu,nLinha, oModelAtu)})
	oStructO0W:SetProperty('O0W_VPOSSI',MODEL_FIELD_VALID,{|oModelAtu,cField,nValAtu,nLinha,nValAnt| JCallJ094(oModel,cField,nLinha,nValAtu,nValAnt), JUpdTotal('02',oModel,nValAtu,nLinha, oModelAtu)})
	oStructO0W:SetProperty('O0W_VREMOT',MODEL_FIELD_VALID,{|oModelAtu,cField,nValAtu,nLinha,nValAnt| JCallJ094(oModel,cField,nLinha,nValAtu,nValAnt), JUpdTotal('03',oModel,nValAtu,nLinha, oModelAtu)})
	oStructO0W:SetProperty('O0W_VINCON',MODEL_FIELD_VALID,{|oModelAtu,cField,nValAtu,nLinha,nValAnt| JCallJ094(oModel,cField,nLinha,nValAtu,nValAnt), JUpdTotal('04',oModel,nValAtu,nLinha, oModelAtu)})

	//-- Campos Valores Tribut�rios
	DbSelectArea("O0W")
	If ColumnPos('O0W_MULTRI') > 0
		oStructO0W:SetProperty('O0W_MULTRI',MODEL_FIELD_VALID,{|oModelAtu,cField,nValAtu,nLinha,nValAnt| JCallJ094(oModel,cField,nLinha,nValAtu,nValAnt)})
	EndIf

	If ColumnPos('O0W_CFCMUL') > 0
		oStructO0W:SetProperty('O0W_CFCMUL',MODEL_FIELD_VALID,{|oModelAtu,cField,nValAtu,nLinha,nValAnt| (Vazio().OR.ExistCpo('NW7', FwFldGet("O0W_CFCMUL"),1)).AND.JCallJ094(oModel,cField,nLinha,nValAtu,nValAnt)})
	EndIf

	If ColumnPos('O0W_PERENC') > 0
		oStructO0W:SetProperty('O0W_PERENC',MODEL_FIELD_VALID,{|oModelAtu,cField,nValAtu,nLinha,nValAnt| JCallJ094(oModel,cField,nLinha,nValAtu,nValAnt)})
	EndIf

	If ColumnPos('O0W_PERHON') > 0
		oStructO0W:SetProperty('O0W_PERHON',MODEL_FIELD_VALID,{|oModelAtu,cField,nValAtu,nLinha,nValAnt| JCallJ094(oModel,cField,nLinha,nValAtu,nValAnt)})
	EndIf

	If ColumnPos('O0W_REDUT') > 0
		oStructO0W:SetProperty('O0W_REDUT',MODEL_FIELD_VALID,{|oModelAtu,cField,nValAtu,nLinha,nValAnt| JCallJ094(oModel,cField,nLinha,nValAtu,nValAnt)})
	EndIf


	oStructO0W:SetProperty('O0W_VPOSSI',MODEL_FIELD_WHEN, {|| .F.})
	oStructO0W:SetProperty('O0W_VATPED',MODEL_FIELD_WHEN, {|| .F.})
	oStructO0W:SetProperty('O0W_VATPOS',MODEL_FIELD_WHEN, {|| .F.})
	oStructO0W:SetProperty('O0W_VATPRO',MODEL_FIELD_WHEN, {|| .F.})
	oStructO0W:SetProperty('O0W_VATREM',MODEL_FIELD_WHEN, {|| .F.})
	oStructO0W:SetProperty('O0W_VATINC',MODEL_FIELD_WHEN, {|| .F.})

	// Remo��o das Valida��es pois est�o centralizadas nos Modelos padr�o
	oStructNSY:SetProperty('NSY_CPEVLR',MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty('NSY_CINSTA',MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty('NSY_CFCORC',MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty('NSY_CMOCON',MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty('NSY_DTJURC',MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty('NSY_DTMULC',MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty('NSY_PERMUC',MODEL_FIELD_VALID,{|| .T. })

	oStructNSY:SetProperty("NSY_CFCOR1",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_V1DATA",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_CMOIN1",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_DTMUL1",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_PERMU1",MODEL_FIELD_VALID,{|| .T. })

	oStructNSY:SetProperty("NSY_CFCOR2",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_V2DATA",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_CMOIN2",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_DTMUL2",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_PERMU2",MODEL_FIELD_VALID,{|| .T. })

	oStructNSY:SetProperty("NSY_CFCORT",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_TRDATA",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_CMOTRI",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_DTMUTR",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_PERMUT",MODEL_FIELD_VALID,{|| .T. })


	oStructNSY:SetProperty("NSY_CFMUL2",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_CFMULT",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_CMOEM2",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_CMOEMT",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_VLRMU2",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_VLRMT" ,MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_DTMUT2",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_DTMUTT",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_DTINC2",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty("NSY_DTINCT",MODEL_FIELD_VALID,{|| .T. })
	oStructNSY:SetProperty('NSY_REDUT' ,MODEL_FIELD_VALID,{|| .T. })

	oStructNSY:SetProperty('NSY_CFMUL2',MODEL_FIELD_WHEN, {|| .T.})
	oStructNSY:SetProperty('NSY_CMOEM2',MODEL_FIELD_WHEN, {|| .T.})
	oStructNSY:SetProperty('NSY_VLRMU2',MODEL_FIELD_WHEN, {|| .T.})
	oStructNSY:SetProperty('NSY_DTMUT2',MODEL_FIELD_WHEN, {|| .T.})
	oStructNSY:SetProperty('NSY_DTINC2',MODEL_FIELD_WHEN, {|| .T.})

	oStructNSY:SetProperty('NSY_CFMULT',MODEL_FIELD_WHEN, {|| .T.})
	oStructNSY:SetProperty('NSY_CMOEMT',MODEL_FIELD_WHEN, {|| .T.})
	oStructNSY:SetProperty('NSY_VLRMT' ,MODEL_FIELD_WHEN, {|| .T.})
	oStructNSY:SetProperty('NSY_DTMUTT',MODEL_FIELD_WHEN, {|| .T.})
	oStructNSY:SetProperty('NSY_DTINCT',MODEL_FIELD_WHEN, {|| .T.})

	oStructO0W:RemoveField( "NSY_CAJURI" )
	oStructNSY:RemoveField( "NSY_CVERBA" )

	//-------------------------------------------------------------------------
	//Monta o modelo do formul�rio
	//-------------------------------------------------------------------------
	// Parte do resumo
	oModel:= MPFormModel():New( "JURA270", /*Pre-Validacao*/, {|oModel| J270TOK(oModel)}/*Pos-Validacao*/, {|oModel| J270Commit(oModel)}/*Commit*/, /*Cancel*/)
	oModel:SetDescription( STR0001 )
	oModel:AddFields( "NSZMASTER", /*cOwner*/, oStructNSZ, /*bPre-Pre-Validacao*/,/*Pos-Validacao*/, /*Carregamento*/)

	// Grid da Verba
	oModel:AddGrid( "O0WDETAIL", "NSZMASTER" /*cOwner*/, oStructO0W, /*bLinePre*/, /*bLinePost*/,/*bPre*/, /*bPost*/)
	oModel:GetModel( 'O0WDETAIL' ):SetUniqueLine({"O0W_COD"})
	oModel:SetRelation( "O0WDETAIL", { { "O0W_FILIAL", "xFilial('O0W')" }, { "O0W_CAJURI", "NSZ_COD" } }, O0W->( IndexKey( 1 ) ) )
	oModel:SetOptional( 'O0WDETAIL' , .T. )

	// Grid dos valores
	oModel:AddGrid( "NSYDETAIL", "O0WDETAIL" /*cOwner*/, oStructNSY, /*bLinePre*/, /*bLinePost*/,/*bPre*/, /*bPost*/ )
	oModel:GetModel( 'NSYDETAIL' ):SetUniqueLine({"NSY_COD"})
	oModel:SetRelation( "NSYDETAIL", { { "NSY_FILIAL", "xFilial('O0W')" }, { "NSY_CVERBA", "O0W_COD"}, { "NSY_CAJURI", "NSZ_COD"} }, NSY->( IndexKey( 1 ) ) )
	oModel:GetModel( 'NSYDETAIL' ):SetDelAllLine( .F. )
	oModel:GetModel( 'NSYDETAIL' ):SetNoInsertLine(.F.)
	oModel:SetOptional( 'NSYDETAIL' , .F. )

	oModel:GetModel( "NSZMASTER" ):SetDescription( 'Objetos e Valores' )
	oModel:GetModel( "O0WDETAIL" ):SetDescription( 'Verbas' )
	oModel:GetModel( "NSYDETAIL" ):SetDescription( 'Valores' )

Return oModel

//-------------------------------------------------------------------
/*/{Protheus.doc} J270Commit
Valida informa��es Commit

@param  oModel Model a ser verificado
@Return lRet   .T./.F. As informa��es s�o v�lidas ou n�o
@author Willian.Kazahaya
@since 05/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function J270Commit(oModel)
	Local aArea      := GetArea()
	Local aAreaNSZ   := NSZ->( GetArea() )
	Local cFilProc   := xFilial("NSZ")
	Local cProcesso  := oModel:GetValue("NSZMASTER", "NSZ_COD")
	Local cPrognost  := ""
	Local nVlrPro    := 0
	Local nVlrProAtu := 0
	Local aVlEnvolvi := {}
	Local dData      := Date()
//C�digo da moeda utilizada nos valores envolvidos\provis�o no processo quando estes valores vierem dos objetos
	Local cMoeCod    := SuperGetMv("MV_JCMOPRO", .F., "01")
	Local oMdl95     := Nil
	Local lRet       := .F.
	Local aVlrPro    := {}
	Local nVCProAtu  := 0
	Local nVJProAtu  := 0
	Local oModelO0W  := oModel:GetModel("O0WDETAIL")
	Local lAtuRedut  := .F.

	DbSelectArea("O0W")
	If ColumnPos('O0W_REDUT') > 0
		lAtuRedut := .T.
	EndIf
	O0W->(DbCloseArea())

	Private cTipoAsJ   := oModel:GetValue("NSZMASTER", "NSZ_TIPOAS")
	Private C162TipoAs := cTipoAsJ

	//Realiza a Grava��o do Model
	Begin Transaction
		lRet := FwFormCommit( oModel )


		If lRet

			//Atualiza Valores
			JURCORVLRS('NSY', cProcesso, , , )
			JAtuValO0W(cProcesso)
			If lAtuRedut
				J270AtuRed(oModelO0W)
			EndIf

			//Busca progn�stico dos objetos
			cPrognost  := J94ProgObj(cFilProc, cProcesso)

			//Busca valor provavel
			nVlrPro    := JA094VlDis(cProcesso, "1", .F.)

			//Busca valor provavel atualizado
			aVlrPro := JA094VlDis(cProcesso, "1", .T.,,.T.)
			nVlrProAtu := aVlrPro[1][1] // Valor atualizado
			nVCProAtu  := aVlrPro[1][2] // Valor de corre��o Atualizado
			nVJProAtu  := aVlrPro[1][3] // Valor de Juros Atualizado

			oMdl95 := FWLoadModel("JURA095")
			oMdl95:SetOperation(4)
			oMdl95:Activate()

			DbSelectArea("NSZ")
			NSZ->( DbSetOrder(1) )  //-- NSZ_FILIAL + NSZ_COD

			//-- Atualiza as informa��es nos campos do processo - NSZ
			If NSZ->( DbSeek(cFilProc + cProcesso) )
				//Busca valores envolvidos
				aVlEnvolvi := JA094VlEnv(cProcesso, cFilProc)

				If !Empty(cPrognost)
					oMdl95:LoadValue("NSZMASTER", "NSZ_CPROGN", cPrognost)
				EndIf

				oMdl95:LoadValue("NSZMASTER", "NSZ_VLPROV", nVlrPro   )
				oMdl95:LoadValue("NSZMASTER", "NSZ_VAPROV", nVlrProAtu )
				oMdl95:LoadValue("NSZMASTER", "NSZ_VCPROV", nVCProAtu   )
				oMdl95:LoadValue("NSZMASTER", "NSZ_VJPROV", nVJProAtu   )

				If (nVlrPro ) > 0
					oMdl95:LoadValue("NSZMASTER", "NSZ_DTPROV", dData    )
					oMdl95:LoadValue("NSZMASTER", "NSZ_CMOPRO", cMoeCod  )
				Else
					oMdl95:LoadValue("NSZMASTER", "NSZ_DTPROV", CtoD("") )
					oMdl95:LoadValue("NSZMASTER", "NSZ_CMOPRO", "" )
				EndIf

				oMdl95:LoadValue("NSZMASTER", "NSZ_VLENVO", aVlEnvolvi[1][1] )
				oMdl95:LoadValue("NSZMASTER", "NSZ_VAENVO", aVlEnvolvi[1][2] )
				oMdl95:LoadValue("NSZMASTER", "NSZ_VCENVO", aVlEnvolvi[1][3] )
				oMdl95:LoadValue("NSZMASTER", "NSZ_VJENVO", aVlEnvolvi[1][4] )

				If aVlEnvolvi[1][1] > 0
					oMdl95:LoadValue("NSZMASTER", "NSZ_DTENVO", dData     )
					oMdl95:LoadValue("NSZMASTER", "NSZ_CMOENV", cMoeCod   )
				Else
					oMdl95:LoadValue("NSZMASTER", "NSZ_DTENVO", CtoD("")  )
					oMdl95:LoadValue("NSZMASTER", "NSZ_CMOENV", "" )
				EndIf
			EndIf

			lRet := oMdl95:VldData()

			If lRet
				lRet := oMdl95:CommitData()
			EndIf

			If !lRet
				oModel:aErrorMessage := oMdl95:aErrorMessage
				JurMsgErro(oModel:aErrorMessage[6],STR0015,oModel:aErrorMessage[7])
				DisarmTransaction()
			EndIf
		EndIf
	End Transaction

	RestArea(aAreaNSZ)
	RestArea(aArea)
Return(lRet)

//-------------------------------------------------------------------
/*/{Protheus.doc} JAtuValO0W(cCajuri)
Atualiza��o de Valores na O0W

@param  cCajuri - Assunto Juridico

@Return lRet    - .T./.F. As informa��es s�o v�lidas ou n�o

@author Willian.Kazahaya
@since 20/08/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Function JAtuValO0W(cCajuri)
	Local aArea      := GetArea()
	Local aAreaO0W   := O0W->(GetArea())
	Local cQuery     := ""
	Local cQrySel    := ""
	Local cQryFrm    := ""
	Local cQryWhr    := ""
	Local cQryOrd    := ""
	Local cAlias     := ""
	Local cChaveAnt  := ""
	Local cChaveAtu  := ""
	Local nValorProv := 0
	Local nValorPoss := 0
	Local nValorRemo := 0
	Local nValorInco := 0
	Local nMulta     := 0
	Local nValorAtu  := 0
	Local aValores   := {}
	Local nX         := 0
	Local nAtuPro    := 0
	Local nAtuPos    := 0
	Local nAtuRem    := 0
	Local nAtuInc    := 0
	Local nAtuPed    := 0
	Local lGrava     := .F.
	Local lGuardaVlr := .F.

	cQrySel += "SELECT NSY.NSY_COD, "
	cQrySel +=        "NSY.NSY_CVERBA, "
	cQrySel +=        "NSY.NSY_CPEVLR, "
	cQrySel +=        "NSY.NSY_CPROG, "
	cQrySel +=        "NSY.NSY_VLCONT, "
	cQrySel +=        "NSY.NSY_DTCONT, "
	cQrySel +=        "NSY.NSY_VLCONA, "
	cQrySel +=        "NSP.NSP_DESC, "
	cQrySel +=        "NSP.NSP_TIPO, "
	cQrySel +=        "NQ7.NQ7_TIPO, "
	cQrySel +=        "NSY.NSY_TRVLRA, " // Honor�rios Atualizados
	cQrySel +=        "NSY.NSY_TRVLR, "  // Honor�rios
	cQrySel +=        "NSY.NSY_MUATT, "  // Honor�rios sobre a Multa Atualizada
	cQrySel +=        "NSY.NSY_VLRMT, "  // Honor�rios sobre a Multa
	cQrySel +=        "NSY.NSY_V2VLRA, " // Encargos Atualizados
	cQrySel +=        "NSY.NSY_V2VLR, "  // Encargos
	cQrySel +=        "NSY.NSY_MUATU2, " // Encargos sobre a Multa Atualizada
	cQrySel +=        "NSY.NSY_VLRMU2, " // Encargos sobre a Multa
	cQrySel +=        "NSY.NSY_V1VLRA, " // Multa Atualizada
	cQrySel +=        "NSY.NSY_V1VLR "   // Multa
	cQryFrm += "FROM " + RetSqlName("NSY") + " NSY "
	cQryFrm += "INNER JOIN " + RetSqlName("NSP") + " NSP "
	cQryFrm +=       "ON (NSP.NSP_COD = NSY.NSY_CPEVLR "
	cQryFrm +=       "AND NSP.D_E_L_E_T_ = ' ') "
	cQryFrm += "INNER JOIN " + RetSqlName("NQ7") + " NQ7 "
	cQryFrm +=       "ON (NQ7.NQ7_COD = NSY.NSY_CPROG "
	cQryFrm +=       "AND NQ7.D_E_L_E_T_ = ' ') "
	cQryWhr += "WHERE NSY_CAJURI = '" + cCajuri + "' "
	cQryWhr +=  " AND NSY_FILIAL = '" + xFilial("NSY") + "' "
	cQryWhr +=  " AND NSY_CVERBA <> ' ' "
	cQryWhr +=  " AND NSY.D_E_L_E_T_ = ' ' "
	cQryOrd += "ORDER BY NSY.NSY_CVERBA, NSY.NSY_CPROG"

	cQuery := ChangeQuery(cQrySel + cQryFrm + cQryWhr + cQryOrd)

	cAlias := GetNextAlias()
	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )

	If (cAlias)->(!Eof())

		While (cAlias)->(!Eof())
			// Atualiza a chave atual
			cChaveAtu :=  cCajuri + (cAlias)->NSY_CVERBA + (cAlias)->NSY_CPEVLR
			nMulta := (cAlias)->NSY_VLCONA
			// Honor�rios
			nValorAtu := Iif((cAlias)->NSY_TRVLRA > 0, (cAlias)->NSY_TRVLRA, (cAlias)->NSY_TRVLR)
			// Honor�rios sobre a Multa
			nValorAtu += Iif((cAlias)->NSY_MUATT > 0, (cAlias)->NSY_MUATT, (cAlias)->NSY_VLRMT)
			// Encargos
			nValorAtu += Iif((cAlias)->NSY_V2VLRA > 0, (cAlias)->NSY_V2VLRA, (cAlias)->NSY_V2VLR)
			// Encargos sobre a Multa
			nValorAtu += Iif((cAlias)->NSY_MUATU2 > 0, (cAlias)->NSY_MUATU2, (cAlias)->NSY_VLRMU2)
			// Multa
			nValorAtu += Iif((cAlias)->NSY_V1VLRA > 0, (cAlias)->NSY_V1VLRA, (cAlias)->NSY_V1VLR)

			Do Case
			Case (cAlias)->NQ7_TIPO == '1'
				nValorProv := nMulta + nValorAtu
				aAdd(aValores,{cChaveAtu,'1',nValorProv})

			Case (cAlias)->NQ7_TIPO == '2'
				nValorPoss := nMulta + nValorAtu
				aAdd(aValores,{cChaveAtu,'2',nValorPoss})

			Case (cAlias)->NQ7_TIPO == '3'
				nValorRemo := nMulta + nValorAtu
				aAdd(aValores,{cChaveAtu,'3',nValorRemo})

			OtherWise
				nValorInco := nMulta + nValorAtu
				aAdd(aValores,{cChaveAtu,'4',nValorInco})
			End Case
			(cAlias)->(dbSkip())
		EndDo

		If Len(aValores) > 0
			DBSelectArea("O0W")
			O0W->( DbSetOrder(3) ) //O0W_FILIAL + O0W_CAJURI + O0W_COD + O0W_CTPPED
			For nX := 1 To Len(aValores)
				If nX == 1 .Or. aValores[nX][1] == cChaveAnt
					lGuardaVlr := .T.
					//Se for o �timo valor, grava
					If nX == Len(aValores)
						Do CASE
						Case aValores[nX][2] == '1'
							nAtuPro := aValores[nX][3]
						Case aValores[nX][2] == '2'
							nAtuPos := aValores[nX][3]
						Case aValores[nX][2] == '3'
							nAtuRem := aValores[nX][3]
						OtherWise
							nAtuInc := aValores[nX][3]
						End CASE
						lGrava := .T.
					EndIf
				Else
					lGrava := .T.
					lGuardaVlr := .T.
				EndIf

				If lGrava
					If O0W->( DbSeek(xFilial("O0W") + cChaveAnt))
						//somat�ria dos progn�sticos atualizados
						nAtuPed := nAtuPro + nAtuPos + nAtuRem + nAtuInc
						RecLock("O0W", .F.)
						// Atualiza os campos de valores atualizados
						O0W->O0W_VATPRO := nAtuPro
						O0W->O0W_VATPOS := nAtuPos
						O0W->O0W_VATREM := nAtuRem
						O0W->O0W_VATINC := nAtuInc
						O0W->O0W_VATPED := nAtuPed
						O0W->( MsUnLock() )
					EndIf
					lGrava := .F.
				EndIf

				If lGuardaVlr
					Do CASE
					Case aValores[nX][2] == '1'
						nAtuPro := aValores[nX][3]
					Case aValores[nX][2] == '2'
						nAtuPos := aValores[nX][3]
					Case aValores[nX][2] == '3'
						nAtuRem := aValores[nX][3]
					OtherWise
						nAtuInc := aValores[nX][3]
					End CASE
					lGuardaVlr := .F.
				EndIf

				cChaveAnt := aValores[nX][1]
			Next nX
			O0W->(DbCloseArea())
		EndIf
	EndIf

	(cAlias)->(DbCloseArea())
	RestArea(aAreaO0W)
	RestArea(aArea)

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} JA270CAJUR()
Busca o Cajuri posicionado

@Return cCajuri - C�d. do Assunto Jur��dico

@author Willian.Kazahaya
@since 05/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Function JA270CAJUR()
	Local cCajuri := ''

	If  !( Empty(NSZ->NSZ_COD) )
		cCajuri := NSZ->NSZ_COD
	Else
		cCajuri := M->NSZ_COD
	EndIf

Return cCajuri

//-------------------------------------------------------------------
/*/{Protheus.doc} J270WhenCJ()
When do C�d. de Assunto Juridico

@Return lRet - Se o campo est� habilitado ou n�o

@author Willian.Kazahaya
@since 05/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Function J270WhenCJ()
Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} J270WnTpPd()
When do C�d. do Tipo de Pedido

@Return lRet - Se o campo est� habilitado ou n�o

@author Willian.Kazahaya
@since 05/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Function J270WnTpPd()
Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} J270ConNSP()
Consulta padr�o do Tipo de Pedido

@author Willian.Kazahaya
@since 05/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Function J270ConNSP(cCajuri)
	Local cQuery     := ""
	Local lRet       := .F.
	Local nResult    := 0
	Local oModel     := FwModelActive()
	Local aPesq      := {"NSP_COD","NSP_DESC"}

	If Empty(cCajuri)
		cCajuri := oModel:GetValue("NSZMASTER","NSZ_COD")
	EndIf

	cQuery += " SELECT NSP_COD, NSP_DESC, NSP.R_E_C_N_O_ NSPRECNO "
	cQuery += " FROM "+RetSqlName("NSP")+" NSP"
	cQuery += " WHERE NSP_FILIAL = '"+xFilial("NSP")+"'"
	cQuery += " AND NSP.D_E_L_E_T_ = ' '"

	cQuery := ChangeQuery(cQuery, .F.)

	nResult := JurF3SXB("NSP", aPesq,, .F., .F.,, cQuery)
	lRet := nResult > 0

	If lRet
		DbSelectArea("NSP")
		NSP->(dbgoTo(nResult))
	EndIf
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JQueryO0W()
Consulta da Tabela de Cabe�alho de Pedidos

@Param cCajuri - C�d. do Assunto Juridico
@Param cTipPedido - C�d. do Tipo de Pedido

@Return cQuery - Query filtrando o Assunto Juridico e o Tipo de Pedido

@author Willian.Kazahaya
@since 05/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function JQueryO0W(cCajuri, cTipPedido)
	Local cQuery  := ""
	Local cQrySel := ""
	Local cQryFrm := ""
	Local cQryWhr := ""

	Default cTipPedido := ""

	cQrySel := "SELECT O0W_FILIAL "
	cQrySel +=      " ,O0W_CAJURI "
	cQrySel +=      " ,O0W_DATPED "
	cQrySel +=      " ,O0W_CTPPED "
	cQrySel +=      " ,O0W_VPEDID "
	cQrySel +=      " ,O0W_VPROVA "
	cQrySel +=      " ,O0W_VPOSSI "
	cQrySel +=      " ,O0W_VREMOT "
	cQrySel +=      " ,O0W_VINCON "
	cQryFrm := " FROM " + RetSqlName("O0W")
	cQryWhr := " WHERE O0W_CAJURI = '" + cCajuri + "' "
	cQryWhr +=  " AND O0W_FILIAL = '" + xFilial("O0W") + "' "
	cQryWhr +=   " AND D_E_L_E_T_ = ' ' "

	If !Empty(cTipPedido)
		cQryWhr += " AND O0W_CTPPED = '" + cTipPedido + "'"
	EndIf

	cQuery := cQrySel + cQryFrm + cQryWhr

	cQuery := ChangeQuery(cQuery)

Return cQuery

//-------------------------------------------------------------------
/*/{Protheus.doc} CalcAvalCS()
Calcula a avalia��o do Progn�stico

@Param cCajuri - Assunto Juridico
@Param cTipPedido - Tipo de Pedido

@Return cAvalCaso - Avalia��o do Caso

@author Willian.Kazahaya
@since 05/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function CalcAvalCS(cCajuri,cTipPedido)
	Local cAvalCaso  := ""
	Local cAlias     := ""
	Local cQuery     := ""
	Local nValProvav := 0
	Local nValPossiv := 0
	Local nValRemoto := 0
	Local nValIncont := 0

	Default cCajuri    := NSZ->NSZ_CAJURI
	Default cTipPedido := ""

	cQuery := JQueryO0W(cCajuri,cTipPedido)

	cAlias := GetNextAlias()
	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )

	While (cAlias)->(!Eof())

		nValProvav += (cAlias)->O0W_VPROVA
		nValPossiv += (cAlias)->O0W_VPOSSI
		nValRemoto += (cAlias)->O0W_VREMOT
		nValIncont += (cAlias)->O0W_VINCON

		(cAlias)->(dbSkip())
	End

	(cAlias)->(DbCloseArea())

	cAvalCaso := GetAvlCaso(nValProvav,nValPossiv,nValRemoto,nValIncont)

Return cAvalCaso

//-------------------------------------------------------------------
/*/{Protheus.doc} CalcValTot()
Calcula o Valor Total dos Pedidos

@Param cCajuri - Assunto Juridico
@Param cTipPedido - Tipo de Pedido

@Return nVlrTot - Valor total dos Pedidos

@author Willian.Kazahaya
@since 05/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function CalcValTot(cTipo, cCajuri)
	Local nVlrTot   := 0
	Local cQuery    := ""
	Local cQrySel   := ""
	Local cQryFrm   := ""
	Local cQryWhr   := ""
	Local cCmpValor := ""
	Local cAlias    := ""

	Default cTipo   := "00"
	Default cCajuri := NSZ->NSZ_COD

	Do Case
		//1=Prov�vel;2=Poss��vel;3=Remoto;4=Incontroverso
	Case cTipo == "01"
		cCmpValor := "O0W_VPROVA"
	Case cTipo == "02"
		cCmpValor := "O0W_VPOSSI"
	Case cTipo == "03"
		cCmpValor := "O0W_VREMOT"
	Case cTipo == "04"
		cCmpValor := "O0W_VINCON"
	Case cTipo == "05"
		cCmpValor := "O0W_VATPED"
	Case cTipo == "06"
		cCmpValor := "O0W_VATPRO"
	Case cTipo == "07"
		cCmpValor := "O0W_VATPOS"
	Case cTipo == "08"
		cCmpValor := "O0W_VATREM"
	Case cTipo == "09"
		cCmpValor := "O0W_VATINC"
	OtherWise // "00"
		cCmpValor := "O0W_VPEDID"
	End Case

	// Query da Tabela de Cabe�alho sumarizand pela Coluna a Ser Validada
	cQrySel := " SELECT O0W_CAJURI "
	cQrySel +=       " ,O0W_CTPPED "
	cQrySel +=       " ," + cCmpValor + " VALOR "

	cQryFrm := " FROM " + RetSqlName("O0W")

	cQryWhr := " WHERE O0W_CAJURI = '" + cCajuri + "' "
	cQryWhr +=  " AND O0W_FILIAL = '" + xFilial("O0W") + "' "
	cQryWhr +=   " AND D_E_L_E_T_ = ' '"

	cQuery := ChangeQuery(cQrySel + cQryFrm + cQryWhr)

	cAlias := GetNextAlias()
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.F.,.F.)

	While (cAlias)->(!Eof())
		nVlrTot += (cAlias)->VALOR
		(cAlias)->(dbSkip())
	End

	(cAlias)->( DbCloseArea() )
Return nVlrTot

//-------------------------------------------------------------------
/*/{Protheus.doc} JUpdTotal()
Atualiza os valores dos Campos Virtuais

@Param cTipo - Campo a ser atualizado
@Param cTipPedido - Tipo de Pedido

@Return cAvalCaso - Avalia��o do Caso

@author Willian.Kazahaya
@since 05/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function JUpdTotal(cTipo,oModel, nValAtu, nLinAtu, oModelLin)
	Local nValProvav := 0
	Local nValPossiv := 0
	Local nValRemoto := 0
	Local nValIncont := 0
	Local nValPedido := 0
	Local cAvalCaso  := ""
	Local nI         := 0
	Local oModelO0W  := oModel:GetModel("O0WDETAIL")
	Local cCajuri    := oModel:GetValue("NSZMASTER", "NSZ_COD")
	Local aMultas    := {}

	If oModelLin == NIL
		oModelLin := oModel:GetModel("O0WDETAIL")
	EndIf

	//Seta o prognostico da verba alterada
	oModelLin:SetValue("O0W_PROGNO",GetAvlCaso(oModelLin:GetValue("O0W_VPROVA"),;
		oModelLin:GetValue("O0W_VPOSSI"),;
		oModelLin:GetValue("O0W_VREMOT"),;
		oModelLin:GetValue("O0W_VINCON")))

	// Loop pegando somente as linhas v�lidas
	For nI := 1 to oModelO0W:Length()
		if !oModelO0W:isDeleted(nI)
			nValProvav += oModelO0W:GetValue("O0W_VPROVA",nI)
			nValPossiv += oModelO0W:GetValue("O0W_VPOSSI",nI)
			nValRemoto += oModelO0W:GetValue("O0W_VREMOT",nI)
			nValIncont += oModelO0W:GetValue("O0W_VINCON",nI)
			nValPedido += oModelO0W:GetValue("O0W_VPEDID",nI)
		Endif
	Next
	If J270OpcVerba(oModelO0W:GetValue('O0W_COD'))  == 3
		aMultas := JA270VlrMu(cCajuri)
	EndIf

	// Verifica qual campo totalizador tem de ser atualizado
	Do Case
	Case cTipo == "01"
		oModel:LoadValue("NSZMASTER","NSZ_TOTPRO", nValProvav + Iif(Len(aMultas) > 0, aMultas[1][2], 0))
	Case cTipo == "02"
		oModel:LoadValue("NSZMASTER","NSZ_TOTPOS", nValPossiv + Iif(Len(aMultas) > 0, aMultas[2][2], 0))
	Case cTipo == "03"
		oModel:LoadValue("NSZMASTER","NSZ_TOTREM", nValRemoto + Iif(Len(aMultas) > 0, aMultas[3][2], 0))
	Case cTipo == "04"
		oModel:LoadValue("NSZMASTER","NSZ_TOTINC", nValIncont + Iif(Len(aMultas) > 0, aMultas[4][2], 0))
	OtherWise
		oModel:LoadValue("NSZMASTER","NSZ_TOTPED", nValPedido)
	End Case

	// Busca o Progn�stico do Processo
	cAvalCaso := GetAvlCaso(nValProvav,nValPossiv,nValRemoto,nValIncont)

	oModel:LoadValue("NSZMASTER","NSZ_AVLCAS",cAvalCaso)

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} GetAvlCaso()
Tratamento para calcular o Progn�stico do Caso

@Param nValProvav - Valor Provavel
@Param nValPossiv - Valor Poss��vel
@Param nValRemoto - Valor Remoto
@Param nValIncont - Valor Incontroverso

@Return cProgno - Progn�stico a ser retornado

@author Willian.Kazahaya
@since 05/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function GetAvlCaso(nValProvav, nValPossiv, nValRemoto, nValIncont)
	Local aTipoAval := {STR0004, STR0005, STR0007, STR0006, STR0008}
	// "Provavel", "Poss�vel", "Incontroverso", "Remoto", "Sem Progn�stico"
	Local cProgno := aTipoAval[5]

	Do Case
	Case (nValProvav > 0 )// Prov�vel
		cProgno := aTipoAval[1]
	Case (nValPossiv > 0 )// Poss�vel
		cProgno := aTipoAval[2]
	Case (nValIncont > 0) // Incontroverso
		cProgno := aTipoAval[3]
	Case (nValRemoto > 0) // Remoto
		cProgno := aTipoAval[4]
	OtherWise // Sem Progn�stico
		cProgno := aTipoAval[5]
	End Case

Return cProgno

//-------------------------------------------------------------------
/*/{Protheus.doc} JCallJ094(oModel,cField,nLinha,nValAtu,nValAnt)()
Criar e atualizar o NSYDETAIL com os dados inputados na O0W

@Param oModel - Modelo atual
@Param cField - Nome do Campo Alterado
@Param nLinha - Numero da Linha alterada
@Param nValAtu - Valor Atual
@Param nValAnt - Valor anterior

@Return cProgno - Progn�stico a ser retornado

@author Willian.Kazahaya
@since 05/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function JCallJ094(oModel,cField,nLinha,nValAtu,nValAnt, oModelWil)
	Local oModelO0W  := oModel:GetModel("O0WDETAIL")
	Local oModelNSY  := oModel:GetModel("NSYDETAIL")
	Local cTipoPed   := oModelO0W:GetValue("O0W_CTPPED")
	Local cCajuri    := oModel:GetValue("NSZMASTER", "NSZ_COD")
	Local cDatPed    := oModelO0W:GetValue("O0W_DATPED")
	Local cInstan    := JurGetDados('NUQ', 2, xFilial('NSZ') + cCajuri + '1',{"NUQ_COD"})
	Local aTipProg   := {'1','2','3','4'}
	Local cMoeda     := SuperGetMv("MV_JCMOPRO", .F., "01")
	Local nValTotPed := 0
	Local nValorPed  := 0
	Local nI         := 0
	Local nLinePoss  := 0
	Local nValProvav := 0
	Local nValPossiv := 0
	Local nValRemoto := 0
	Local nValIncont := 0
	Local nCount     := 0
	Local lRet       := .T.
	Local lAdicionar := .F.
	Local cCodNSY    := ""
	Local cTipoProg  := ""
	Local nGridNsy   := 0
	Local nMulta     := 0
	Local nEncargo   := 0
	Local nMultaEnc  := 0
	Local nHonorario := 0
	Local nMultaHon  := 0
	Local lCposTrib  := .F.
	Local lAtuRedut  := .F.

	//-- Valida se possui os campos da O0W para valores tribut�rios
	DbSelectArea("O0W")
	If ColumnPos('O0W_MULTRI') > 0 ;         //-- Percentual de multa tribu
		.AND. ColumnPos('O0W_CFCMUL') > 0 ;  //-- Forma de correcao multa
		.AND. ColumnPos('O0W_PERENC') > 0 ;  //-- Percentual de encargos
		.AND. ColumnPos('O0W_PERHON') > 0 ;  //-- Percentual de honorario

		lCposTrib := .T.
	EndIf
	If ColumnPos('O0W_REDUT') > 0
		lAtuRedut := .T.
	EndIf

	If !IsInCallStack("JUpdValO0W")
		// Se a Data estiver vazia, coloca a data de hoje
		If Empty(cDatPed)
			oModelO0W:SetValue("O0W_DATPED",Date())
			cDatPed := oModelO0W:GetValue("O0W_DATPED")
		EndIf

		// Verifica qual o Tipo de Progn�stico a ser atualizado
		Do Case
		Case (cField == "O0W_VPROVA")
			cTipoProg := "1"
		Case (cField == "O0W_VPOSSI")
			cTipoProg := "2"
		Case (cField == "O0W_VREMOT")
			cTipoProg := "3"
		Case (cField == "O0W_VINCON")
			cTipoProg := "4"
		OtherWise
			cTipoProg := "0"
		End Case

		// Valor de Pedido
		nValTotPed := oModelO0W:GetValue("O0W_VPEDID")

		// Verifica se � uma inclus�o de uma nova Verba
		lAdicionar := oModelNSY:Length(.T.) == 1
		oModelNSY:SetNoInsertLine(.F.)

		If lAdicionar
			nCount := Len(aTipProg)
		Else
			nCount := oModelNSY:Length()
		EndIf

		nGridNsy := oModelNSY:GetLine()
		// Loop
		For nI := 1 To nCount

			// Se for a inclus�o, Seta os valores dos objetos
			If lAdicionar
				// Somente ir� jogar o valor no Progn. Possivel, caso contr�rio � Zero
				If aTipProg[nI] == '2'
					nValorPed := nValTotPed
				Else
					nValorPed := 0
				EndIf

				cCodNSY := GetSXENum("NSY","NSY_COD",xFilial("NSY")+ cCajuri ,1)

				oModelNSY:SetValue("NSY_FILIAL", xFilial("NSY")                  )
				oModelNSY:SetValue("NSY_COD"   , cCodNSY                         )
				oModelNSY:LoadValue("NSY_CAJURI", cCajuri                        )
				oModelNSY:SetValue("NSY_CPEVLR", cTipoPed                        )
				oModelNSY:SetValue("NSY_CPROG" , JGetProgn(aTipProg[nI])[1]      )
				oModelNSY:SetValue("NSY_CINSTA", cInstan                         )
				oModelNSY:SetValue("NSY_INECON", "2"                             )
				oModelNSY:SetValue("NSY_DTCONT", cDatPed                         )
				oModelNSY:SetValue("NSY_VLCONT", nValorPed                       )
				oModelNSY:SetValue("NSY_CMOCON", cMoeda                          )
				oModelNSY:SetValue("NSY_CFCORC", oModelO0W:GetValue("O0W_CFRCOR"))

				// Se ainda houver outro, adiciona uma nova linha
				If nI < Len(aTipProg)
					oModelNSY:AddLine()
				EndIf
				ConfirmSX8()
			Else
				// Caso haja mais de 1 linha, � considerado altera��o.
				oModelNSY:GoLine(nI)
				cCodTipPrg := JGetProgn(,oModelNSY:GetValue("NSY_CPROG"))[2]

				If !oModelNSY:IsDeleted()
					// Verifica se o Progn�stico � do tipo "Provavel" para ser Atualizado depois
					If cCodTipPrg == '2'
						nLinePoss := nI
						// Verifica se a linha � do Tipo que est� validando
					ElseIf cCodTipPrg == cTipoProg
						oModelNSY:LoadValue("NSY_VLCONT", nValAtu)
						If nValAtu == 0
							oModelNSY:LoadValue("NSY_CJURPC", nValAtu)
							oModelNSY:LoadValue("NSY_VLCONA", nValAtu)
						EndIf
						nValTotPed := nValTotPed - nValAtu
					Else
						nValTotPed := nValTotPed - oModelNSY:GetValue("NSY_VLCONT")
					EndIf
					oModelNSY:LoadValue("NSY_DTCONT",cDatPed)
				EndIf
			EndIf
			If lAtuRedut
				oModelNSY:SetValue("NSY_REDUT" , oModelO0W:GetValue("O0W_REDUT"))
			EndIf
		Next

		// Caso n�o seja a inclus�o, pega o valor calculado e atualiza na linha do Possivel
		If !lAdicionar
			oModelNSY:GoLine(nLinePoss)
			oModelNSY:LoadValue("NSY_PEVLR" , oModelO0W:GetValue("O0W_VPEDID"))
			oModelNSY:LoadValue("NSY_CCOMON",oModelO0W:GetValue("O0W_CFRCOR"))
			oModelNSY:LoadValue("NSY_PEINVL",'2')
			oModelNSY:LoadValue("NSY_CMOPED",cMoeda)
			oModelNSY:LoadValue("NSY_PEDATA",cDatPed)
			oModelNSY:LoadValue("NSY_DTJURO",oModelO0W:GetValue("O0W_DTJURO"))
			oModelNSY:LoadValue("NSY_DTCONT",cDatPed)
			lRet := oModelNSY:LoadValue("NSY_VLCONT", nValTotPed)
		EndIf

		oModelNSY:GoLine(nGridNsy)

		nGridNsy := oModelNSY:GetLine()
		For nI := 1 To oModelNSY:Length()
			oModelNSY:GoLine(nI)
			If !oModelNSY:IsDeleted()
				cCodTipPrg := JGetProgn(,oModelNSY:GetValue("NSY_CPROG"))[2]

				Do Case
				Case cCodTipPrg == "1"
					nValProvav += oModelNSY:GetValue("NSY_VLCONT")
				Case cCodTipPrg == "2"
					nValPossiv += oModelNSY:GetValue("NSY_VLCONT")//Valor do pedido?
				Case cCodTipPrg == "3"
					nValRemoto += oModelNSY:GetValue("NSY_VLCONT")
				Case cCodTipPrg == "4"
					nValIncont += oModelNSY:GetValue("NSY_VLCONT")
				End Case

				oModelNSY:SetValue("NSY_CPEVLR" , cTipoPed                        )
				oModelNSY:SetValue("NSY_CFCORC" , oModelO0W:GetValue("O0W_CFRCOR"))
				oModelNSY:SetValue("NSY_DTJURC" , oModelO0W:GetValue("O0W_DTJURO"))
				oModelNSY:LoadValue("NSY_DTMULC", oModelO0W:GetValue("O0W_DTMULT"))
				oModelNSY:LoadValue("NSY_PERMUC", oModelO0W:GetValue("O0W_PERMUL"))

				//-- Valida se possui os campos da O0W para valores tribut�rios, para atualizar os campos necessarios da NSY
				If lCposTrib

					If oModelO0W:isFieldUpdate("O0W_CFCMUL")//se alterar o campo de forma de corre��o, limpa os totalizadores da nsy
						oModelNSY:ClearField("NSY_CCORP1")
						oModelNSY:ClearField("NSY_CJURP1")
						oModelNSY:ClearField("NSY_MULAT1")
						oModelNSY:ClearField("NSY_V1VLRA")
					EndIf


					If Empty(oModelO0W:GetValue("O0W_MULTRI"))
						oModelNSY:ClearField("NSY_CFCOR1")
						oModelNSY:ClearField("NSY_V1DATA")
						oModelNSY:ClearField("NSY_DTJUR1")
						oModelNSY:ClearField("NSY_CMOIN1")
						oModelNSY:ClearField("NSY_DTMUL1")
						oModelNSY:ClearField("NSY_PERMU1")
						oModelNSY:ClearField("NSY_CCORP1")
						oModelNSY:ClearField("NSY_CJURP1")
						oModelNSY:ClearField("NSY_MULAT1")
						oModelNSY:ClearField("NSY_V1VLRA")
						oModelNSY:ClearField("NSY_V1VLR")
					Else
						//1� inst�ncia
						oModelNSY:SetValue("NSY_CFCOR1", oModelO0W:GetValue("O0W_CFCMUL"))
						oModelNSY:SetValue("NSY_V1DATA", oModelO0W:GetValue("O0W_DATPED"))
						oModelNSY:SetValue("NSY_DTJUR1", oModelO0W:GetValue("O0W_DATPED"))
						oModelNSY:SetValue("NSY_CMOIN1", cMoeda)
						If JA094VlMult(oModelNSY:GetValue('NSY_CFCOR1'))
							oModelNSY:SetValue("NSY_DTMUL1", oModelO0W:GetValue("O0W_DATPED"))
							oModelNSY:SetValue("NSY_PERMU1", "0")
						EndIf

						nMulta := oModelNSY:GetValue("NSY_VLCONT") * oModelO0W:GetValue("O0W_MULTRI")/100

						oModelNSY:LoadValue("NSY_V1VLR", nMulta)

						// Limpa os valores valores preenchidos pela corre��o
						oModelNSY:ClearField("NSY_CCORP1", nMulta)
						oModelNSY:ClearField("NSY_CJURP1", nMulta)
						oModelNSY:ClearField("NSY_MULAT1", nMulta)
						oModelNSY:ClearField("NSY_V1VLRA", nMulta)

					EndIf
					//2� instancia
					If Empty(oModelO0W:GetValue("O0W_PERENC"))
						oModelNSY:ClearField("NSY_CFCOR2")
						oModelNSY:ClearField("NSY_V2DATA")
						oModelNSY:ClearField("NSY_DTJUR2")
						oModelNSY:ClearField("NSY_CMOIN2")
						oModelNSY:ClearField("NSY_DTMUL2")
						oModelNSY:ClearField("NSY_PERMU2")
						oModelNSY:ClearField("NSY_V2VLR")
						oModelNSY:ClearField("NSY_CCORP2")
						oModelNSY:ClearField("NSY_CJURP2")
						oModelNSY:ClearField("NSY_MULAT2")
						oModelNSY:ClearField("NSY_V2VLRA")
						oModelNSY:ClearField("NSY_CFMUL2")
						oModelNSY:ClearField("NSY_DTMUT2")
						oModelNSY:ClearField("NSY_DTINC2")
						oModelNSY:ClearField("NSY_CMOEM2")
						oModelNSY:ClearField("NSY_VLRMU2")
						oModelNSY:ClearField("NSY_CCORM2")
						oModelNSY:ClearField("NSY_CJURM2")
						oModelNSY:ClearField("NSY_MUATU2")
					Else
						oModelNSY:SetValue("NSY_CFCOR2", oModelO0W:GetValue("O0W_CFRCOR"))
						oModelNSY:SetValue("NSY_V2DATA", oModelO0W:GetValue("O0W_DATPED"))
						oModelNSY:SetValue("NSY_DTJUR2", oModelO0W:GetValue("O0W_DATPED"))
						oModelNSY:SetValue("NSY_CMOIN2", cMoeda)
						If JA094VlMult(oModelNSY:GetValue('NSY_CFCOR2'))
							oModelNSY:SetValue("NSY_DTMUL2", oModelO0W:GetValue("O0W_DATPED"))
							oModelNSY:SetValue("NSY_PERMU2", "0")
						EndIf

						nEncargo := oModelNSY:GetValue("NSY_VLCONT") * oModelO0W:GetValue("O0W_PERENC")/100

						oModelNSY:SetValue("NSY_V2VLR", nEncargo)

						// Limpa os valores valores preenchidos pela corre��o
						oModelNSY:ClearField("NSY_CCORP2", nEncargo)
						oModelNSY:ClearField("NSY_CJURP2", nEncargo)
						oModelNSY:ClearField("NSY_MULAT2", nEncargo)
						oModelNSY:ClearField("NSY_V2VLRA", nEncargo)

						oModelNSY:SetValue("NSY_CFMUL2", oModelO0W:GetValue("O0W_CFCMUL"))
						oModelNSY:SetValue("NSY_DTMUT2", oModelO0W:GetValue("O0W_DATPED"))//Data Multa
						oModelNSY:SetValue("NSY_DTINC2", oModelO0W:GetValue("O0W_DATPED"))//Data Incidencia
						oModelNSY:SetValue("NSY_CMOEM2", cMoeda)

						nMultaEnc := oModelNSY:GetValue("NSY_V1VLR") * oModelO0W:GetValue("O0W_PERENC")/100

						oModelNSY:SetValue("NSY_VLRMU2", nMultaEnc)

						// Limpa os valores valores preenchidos pela corre��o
						oModelNSY:ClearField("NSY_CCORM2", nMultaEnc)
						oModelNSY:ClearField("NSY_CJURM2", nMultaEnc)
						oModelNSY:ClearField("NSY_MUATU2", nMultaEnc)
					EndIf
					//Tribunal Superior
					If Empty(oModelO0W:GetValue("O0W_PERHON"))
						oModelNSY:ClearField("NSY_CFCORT")
						oModelNSY:ClearField("NSY_TRDATA")
						oModelNSY:ClearField("NSY_DTJURT")
						oModelNSY:ClearField("NSY_CMOTRI")
						oModelNSY:ClearField("NSY_DTMUTR")
						oModelNSY:ClearField("NSY_PERMUT")
						oModelNSY:ClearField("NSY_TRVLR")
						oModelNSY:ClearField("NSY_CCORPT")
						oModelNSY:ClearField("NSY_CJURPT")
						oModelNSY:ClearField("NSY_VLRMUT")
						oModelNSY:ClearField("NSY_TRVLRA")
						oModelNSY:ClearField("NSY_CFMULT")
						oModelNSY:ClearField("NSY_DTMUTT")
						oModelNSY:ClearField("NSY_DTINCT")
						oModelNSY:ClearField("NSY_CMOEMT")
						oModelNSY:ClearField("NSY_VLRMT")
						oModelNSY:ClearField("NSY_CCORMT")
						oModelNSY:ClearField("NSY_CJURMT")
						oModelNSY:ClearField("NSY_MUATT")
					Else
						oModelNSY:SetValue("NSY_CFCORT", oModelO0W:GetValue("O0W_CFRCOR"))
						oModelNSY:SetValue("NSY_TRDATA", oModelO0W:GetValue("O0W_DATPED"))
						oModelNSY:SetValue("NSY_DTJURT", oModelO0W:GetValue("O0W_DATPED"))
						oModelNSY:SetValue("NSY_CMOTRI", cMoeda)
						If JA094VlMult(oModelNSY:GetValue('NSY_CFCORT'))
							oModelNSY:SetValue("NSY_DTMUTR", oModelO0W:GetValue("O0W_DATPED"))
							oModelNSY:SetValue("NSY_PERMUT", "0")
						EndIf

						nHonorario := oModelNSY:GetValue("NSY_VLCONT") * oModelO0W:GetValue("O0W_PERHON")/100

						oModelNSY:SetValue("NSY_TRVLR", nHonorario)

						// Limpa os valores valores preenchidos pela corre��o
						oModelNSY:ClearField("NSY_CCORPT", nHonorario)
						oModelNSY:ClearField("NSY_CJURPT", nHonorario)
						oModelNSY:ClearField("NSY_VLRMUT", nHonorario)
						oModelNSY:ClearField("NSY_TRVLRA", nHonorario)

						oModelNSY:SetValue("NSY_CFMULT", oModelO0W:GetValue("O0W_CFCMUL"))
						oModelNSY:SetValue("NSY_DTMUTT", oModelO0W:GetValue("O0W_DATPED"))//Data Multa
						oModelNSY:SetValue("NSY_DTINCT", oModelO0W:GetValue("O0W_DATPED"))//Data Incidencia
						oModelNSY:SetValue("NSY_CMOEMT", cMoeda)
						nMultaHon := oModelNSY:GetValue("NSY_V1VLR") * oModelO0W:GetValue("O0W_PERHON")/100
						oModelNSY:SetValue("NSY_VLRMT" , nMultaHon)

						// Limpa os valores valores preenchidos pela corre��o
						oModelNSY:ClearField("NSY_CCORMT", nMultaHon)
						oModelNSY:ClearField("NSY_CJURMT", nMultaHon)
						oModelNSY:ClearField("NSY_MUATT" , nMultaHon)
					EndIf
					oModelNSY:SetValue("NSY_V1INVL", '2')
					oModelNSY:SetValue("NSY_V2INVL", '2')
					oModelNSY:SetValue("NSY_TRINVL", '2')
				EndIf
			EndIf
			If lAtuRedut
				oModelNSY:SetValue("NSY_REDUT" , oModelO0W:GetValue("O0W_REDUT"))
			EndIF
		Next

		oModelO0W:LoadValue("O0W_VPROVA",nValProvav)
		oModelO0W:LoadValue("O0W_VPOSSI",nValPossiv)
		oModelO0W:LoadValue("O0W_VREMOT",nValRemoto)
		oModelO0W:LoadValue("O0W_VINCON",nValIncont)

		oModelNSY:SetNoInsertLine(.T.)
		oModelNSY:GoLine(nGridNsy)
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JGetProgn(cTipProg, cCodProg)
Criar e atualizar o NSYDETAIL com os dados inputados na O0W

@Param cTipProg - Tipo do Progn�stico
@Param cCodProg - C�digo do Progn�stico

@Return aRet[1] - C�digo do Progn�stico
			[2] - Tipo do Progn�stico

@author Willian.Kazahaya
@since 05/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function JGetProgn(cTipProg, cCodProg)
	Local cAlias  := ""
	Local cQrySel := ""
	Local cQryFrm := ""
	Local cQryWhr := ""

	Default cCodProg := ""
	Default cTipProg := ""

	cQrySel := " SELECT NQ7_COD "
	cQrySel +=        ",NQ7_DESC "
	cQrySel +=        ",NQ7_TIPO "

	cQryFrm := " FROM " + RetSqlName("NQ7") + " NQ7 "
	cQryWhr := " WHERE D_E_L_E_T_ = ' ' "
	cQryWhr +=  " AND NQ7_FILIAL = '" + xFilial("NQ7") + "' "

	If !Empty(cTipProg)
		cQryWhr += " AND NQ7_TIPO = '" + cTipProg + "'"
	EndIf

	If !Empty(cCodProg)
		cQryWhr += " AND NQ7_COD = '" + cCodProg + "'"
	EndIf

	cQuery := ChangeQuery(cQrySel + cQryFrm + cQryWhr)

	cAlias := GetNextAlias()
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.F.,.F.)

	If (cAlias)->(!Eof())
		cCodProg := (cAlias)->NQ7_COD
		cTipProg := (cAlias)->NQ7_TIPO
	End

	(cAlias)->( DbCloseArea() )

Return {cCodProg, cTipProg}

//-------------------------------------------------------------------
/*/{Protheus.doc} VldProgno()
(Valida se temos prog�sticos cadastrados para os 4 tipos "Prov�vel", "Poss�vel", 
"Remoto" e "Incontroverso" )
@type  Static Function
@author Ronaldo.Goncalves
@since 03/01/2020
@version 1.0
@return lRet
/*/
//-------------------------------------------------------------------
Static Function VldProgno()
	Local aArea     := GetArea()
	Local cAliasNQ7 := GetNextAlias()
	Local cQuery    := ''
	Local nI        := 1
	Local lRet      := .T.

	cQuery := "SELECT NQ7_TIPO "
	cQuery +=  " FROM " + RetSqlName("NQ7") + " NQ7 "
	cQuery += " WHERE NQ7.NQ7_FILIAL = '" + xFilial("NQ7") + "' "
	cQuery +=   " AND  D_E_L_E_T_ = ' ' "
	cQuery +=" GROUP BY NQ7_TIPO "
	cQuery +=" ORDER BY NQ7_TIPO "

	cQuery := ChangeQuery(cQuery)

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasNQ7,.F.,.F.)

	While lRet .And. (!(cAliasNQ7)->(EoF()))

		If ((cAliasNQ7)-> NQ7_TIPO) != cValToChar(nI)
			lRet := .F.
		EndIf

		(cAliasNQ7)->(DbSkip())
		nI++
	EndDo

	If nI < 5
		JurMsgErro(STR0014,STR0015,STR0016) //Erro: N�o foi poss��vel cadastrar o pedido, pois, o cadastro de progn�sticos est� incompleto.
		lRet := .F.
	EndIf

	(cAliasNQ7)->(DbCloseArea())
	RestArea(aArea)

Return lret

//-------------------------------------------------------------------
/*/{Protheus.doc} J270TOK(oModel)
Pr�-valida��o do modelo

@param 	oModel  	Model a ser verificado
@Return lRet	 	.T./.F. As informa��es s�o v�lidas ou n�o
@author Willian.Kazahaya
@since 02/10/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function J270TOK(oModel)
	Local lRet         := .F.
	Local oModelO0W    := oModel:GetModel("O0WDETAIL")
	Local nJ270Opc     := 4
	Local nI           := 0
	Local aArea        := GetArea()
	Local aAreaO0W     := O0W->(GetArea())
	Local cFluig       := SuperGetMV('MV_JFLUIGA',,'2')
	Local lFluig       := .F.

	lRet := JUpdTotal("00",oModel)
	//Valida cadastro de progn�sticos
	If lRet
		lRet := VldProgno()
	EndIf

	If lRet
		DbSelectArea("O0W")
		O0W->( DbSetOrder(1) )	//O0W_FILIAL+O0W_COD
		For nI := 1 to oModelO0W:Length()
			oModelO0W:GoLine(nI)
			//Posiciona na verba
			O0W->(DbSeek(xFilial("O0W")+oModelO0W:GetValue('O0W_COD'),.T.))
			//Seta a opera��o da O0W (Inc, Alt ou Exc)
			If oModelO0W:IsDeleted(nI)
				nJ270Opc := 5
			Else
				//Se existem Objetos com o c�digo da verba, trata-se como Altera��o (4)
				nJ270Opc := J270OpcVerba(oModelO0W:GetValue('O0W_COD'))
			EndIf
			If oModelO0W:IsUpdated() .And. oModelO0W:GetValue('O0W_VPEDID') == 0
				lRet := .F.
				JurMsgErro(	STR0043 ) //"Informe um valor v�lido para o pedido."
				Exit
			EndIf
			If cFluig == '1'
				If NQS->( FieldPos('NQS_TAPROV') ) > 0
					//Verifica se o Tipo de aprova��o � 6 - Objeto
					lFluig := !Empty(Posicione("NQS", 3, xFilial("NQS") + "6", "NQS_COD" )) .And. ;
						Posicione("NQS", 3, xFilial("NQS") + "6", "NQS_FLUIG" ) == "1" //NQS_FILIAL + NQS_TAPROV
				Else
					lFluig = .T.
				EndIf
			EndIf
			//Aprova��o de Objeto
			If lFluig .And. !IsInCallStack("JA106ConfNZK") .And. O0W->( FieldPos("O0W_CODWF") ) > 0
				If !J270FFluig(oModel, oModelO0W, nJ270Opc)
					lRet := .F.
					Exit
				EndIf
			EndIf
		Next
	EndIf

	RestArea(aAreaO0W)
	RestArea(aArea)

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J270FFluig
Valida as altera��es nos campos de valor para ajustar o hist�rico 
conforme necessidade.

@param 	oModel   Modelo de dados
@param 	cTabela  Tabela que est� sendo alterada

@since 08/01/2020
/*/
//-------------------------------------------------------------------
Function J270FFluig(oModel, oModelO0W, nJ270Opc)

	Local lRet       := .T.
	Local lNZKInDic  := FWAliasInDic("NZK") //Verifica se existe a tabela NZK no Dicion�rio
	Local aDadFwApv  := {}
	Local cTipFwApv  := "6"
	Local cCodWf     := oModelO0W:GetValue("O0W_CODWF")
	Local nValorDif  := ""

	//*******************************************************************************
	// Gera follow-up e tarefas de follow-up para aprovacao no fluig quando nao for
	// uma aprovacao do fluig
	//*******************************************************************************
	If lNZKInDic .And. (nJ270Opc == MODEL_OPERATION_DELETE .Or. ;
			oModelO0W:IsFieldUpdated("O0W_VPEDID") .Or. ;
			oModelO0W:IsFieldUpdated("O0W_VPROVA") .Or. ;
			oModelO0W:IsFieldUpdated("O0W_VINCON") .Or. ;
			oModelO0W:IsFieldUpdated("O0W_CFRCOR") .Or. ;
			oModelO0W:IsFieldUpdated("O0W_DATPED") .Or. ;
			oModelO0W:IsFieldUpdated("O0W_DTJURO") )

		//Verifica se ja existe tarefa de follow-up em aprovacao
		If !Empty(cCodWf) .And. J94FTarFw(xFilial("O0W"), cCodWf, "1|5|6", "4")
			JurMsgErro(	STR0017 ) //"J� existe follow-up para aprova��o pendente. N�o ser� poss��vel prosseguir com a altera��o."
			lRet := .F.
		Else
			//Verifica se alterou valor da provisao
			If oModelO0W:GetValue("O0W_VPEDID") <> O0W->O0W_VPEDID  .Or. ;
					oModelO0W:GetValue("O0W_VPROVA") <> O0W->O0W_VPROVA .Or. ;
					oModelO0W:GetValue("O0W_CFRCOR") <> O0W->O0W_CFRCOR .Or. ;
					oModelO0W:GetValue("O0W_DATPED") <> O0W->O0W_DATPED .Or. ;
					oModelO0W:GetValue("O0W_DTJURO") <> O0W->O0W_DTJURO

				//Verifica se existe algum resultado de follow-up com o tipo 4=Em Aprovacao
				If lRet .And. Empty( JurGetDados("NQN", 3, xFilial("NQN") + "4", "NQN_COD") ) //NQN_FILIAL + NQN_TIPO
					JurMsgErro( STR0018 ) //"N�o existe resultado de follow-up com o tipo 4=Em Aprovacao cadastrado. Verifique o cadastro de resultados do follow-up!"
					lRet := .F.
				EndIf

				If lRet .And. (nJ270Opc == MODEL_OPERATION_UPDATE .OR. ;
						nJ270Opc == MODEL_OPERATION_INSERT)

					//Carrega as altera��es que ser�o feitas quando for aprovada
					//a alteracao do objeto
					Aadd( aDadFwApv, {"O0W_PROGNO" , oModelO0W:GetValue("O0W_PROGNO") } )
					Aadd( aDadFwApv, {"O0W_VPEDID",  oModelO0W:GetValue("O0W_VPEDID") } )
					Aadd( aDadFwApv, {"O0W_VPROVA",  oModelO0W:GetValue("O0W_VPROVA") } )
					Aadd( aDadFwApv, {"O0W_VPOSSI",  oModelO0W:GetValue("O0W_VPOSSI") } )
					Aadd( aDadFwApv, {"O0W_VREMOT",  oModelO0W:GetValue("O0W_VREMOT") } )
					Aadd( aDadFwApv, {"O0W_VINCON" , oModelO0W:GetValue("O0W_VINCON") } )
					Aadd( aDadFwApv, {"O0W_CFRCOR" , oModelO0W:GetValue("O0W_CFRCOR") } )
					Aadd( aDadFwApv, {"O0W_DATPED" , oModelO0W:GetValue("O0W_DATPED") } )
					Aadd( aDadFwApv, {"O0W_DTJURO" , oModelO0W:GetValue("O0W_DTJURO") } )

					If nJ270Opc == MODEL_OPERATION_INSERT
						nValorDif := oModelO0W:GetValue("O0W_VPEDID")
					Else
						nValorDif := oModelO0W:GetValue("O0W_VPEDID") - O0W->O0W_VPEDID
					EndIf
					//Diferen�a de valor que ser� aprovada
					Aadd( aDadFwApv, {"PROV_O0W", nValorDif} )

					//Volta os dados da verba antes de alterar
					If nJ270Opc == MODEL_OPERATION_UPDATE
						oModelO0W:SetValue( "O0W_VPEDID", O0W->O0W_VPEDID )
						oModelO0W:SetValue( "O0W_VPROVA", O0W->O0W_VPROVA )
						oModelO0W:SetValue( "O0W_VREMOT", O0W->O0W_VREMOT )
						oModelO0W:SetValue( "O0W_VINCON", O0W->O0W_VINCON )
						oModelO0W:SetValue( "O0W_CFRCOR", O0W->O0W_CFRCOR )
						oModelO0W:SetValue( "O0W_DATPED", O0W->O0W_DATPED )
						oModelO0W:SetValue( "O0W_DTJURO", O0W->O0W_DTJURO )
					Else //Limpa os dados da verba antes de incluir
						oModelO0W:SetValue( "O0W_VPEDID" , 0 )
						oModelO0W:SetValue( "O0W_VPROVA" , 0 )
						oModelO0W:SetValue( "O0W_VREMOT" , 0 )
						oModelO0W:SetValue( "O0W_VINCON" , 0 )
						oModelO0W:ClearField( "O0W_CFRCOR")
						oModelO0W:LoadValue( "O0W_DATPED" , Date() )
						oModelO0W:ClearField( "O0W_DTJURO")
					EndIf

					//Gera follow-up de aprovacao de Valor de Provisao
					Processa( {| | lRet := J270FFwApv(oModel:GetValue("NSZMASTER", "NSZ_COD"), ;
						aDadFwApv, cTipFwApv, oModel, oModelO0W, ;
						nJ270Opc)},;
						STR0019, "")	//"Gerando aprova��o no Fluig"
				EndIf
			EndIf
		EndIf
	EndIf

Return lRet


//-------------------------------------------------------------------
/*/{Protheus.doc} J270FFwApv
Gera follow-up de aprovacao
Uso geral.

@param 	cProcesso     C�digo do processo
@param 	aCampos       Campos e valores a serem aprovados
@param 	cTipoFwApr    Tipo de Fup de aprova��o
@param 	oModel        Modelo de dados NSZMASTER
@param 	oModelO0W     Modelo de dados O0WDETAIL
@param 	nJ270Opc      Opera��o 3 = Inclus�o, 4 = Altera��o, 5 = Exclus�o

@return	aCampos - Campos que seram gravados na NZK
@since 08/01/2020
/*/
//-------------------------------------------------------------------
Function J270FFwApv(cProcesso, aCampos, cTipoFwApr, oModel, oModelO0W, nJ270Opc)

	Local aArea      := GetArea()
	Local aAreaNTA   := NTA->( GetArea() )
	Local lRet       := .T.
	Local oModelFw   := Nil
	Local aTipoFw    := JurGetDados("NQS", 3, xFilial("NQS") + cTipoFwApr, {"NQS_COD", "NQS_DPRAZO"} )	//NQS_FILIAL + NQS_TAPROV	1=Alteracao Valor Provisao 2=Aprovacao de despesas 3=Aprovacao de Garantias 4=Aprovacao de Levantamento 5=Encerramento
	Local cTipoFw    := ""
	Local nDiaPrazo  := 0
	Local cResultFw  := JurGetDados("NQN", 3, xFilial("NQN") + "4", "NQN_COD") //NQN_FILIAL + NQN_TIPO		4=Em Aprovacao
	Local cPart      := JurUsuario(__cUserId)
	Local cSigla     := JurGetDados("RD0", 1, xFilial("RD0") + cPart, "RD0_SIGLA") //RD0_FILIAL + RD0_CODIGO
	Local aNTA       := {}
	Local aNTE       := {}
	Local aNZK       := {}
	Local aNZM       := {}
	Local aAux       := {}
	Local nCont      := 0
	Local nReg       := 0
	Local cConteudo  := ""
	Local aErroNTA   := {}
	Local cNQNTipoF  := ""  //Vari�vel que vai guardar o tipo do resultado ap�s incluir o WF no FLUIG
	Local cCodWF     := oModelO0W:GetValue("O0W_CODWF")
	Local lPendente  := !Empty(cCodWf) .And. J94FTarFw(xFilial("O0W"), cCodWf, "6", "1")
	Local nOpcFw     := 3
	Local cDesc      := ""
	Local nPosValO0W := Ascan(aCampos,{|x| x[1] == "PROV_O0W"})
	Local nValorO0W  := aCampos[nPosValO0W][2] //Valor que ser� enviado para aprova��o
	Local cProgAtual := ""
	Local nPosProgAp := Ascan(aCampos, {|x| x[1] == "O0W_PROGNO"})
	Local cProgAprov := AllTrim(aCampos[nPosProgAp][2]) //Descri��o do grupo de aprova��o
	Local nVlrPedAtu := 0 //Valor atual do pedido
	Local nPosPedApr := Ascan(aCampos, {|x| x[1] == "O0W_VPEDID"})
	Local nVlrPedApr := aCampos[nPosPedApr][2]//Valor a aprovar do pedido
	Local nVlrPrvAtu := 0 //Valor atual prov�vel
	Local nPosPrvApr := Ascan(aCampos, {|x| x[1] == "O0W_VPROVA"})
	Local nVlrPrvApr := aCampos[nPosPrvApr][2]//Valor a aprovar prov�vel
	Local nVlrPssAtu := 0 //Valor atual poss�vel
	Local nPosPssApr := Ascan(aCampos, {|x| x[1] == "O0W_VPOSSI"})
	Local nVlrPssApr := aCampos[nPosPssApr][2]//Valor a aprovar poss�vel
	Local nVlrRemAtu := 0 //Valor atual remoto
	Local nPosRemApr := Ascan(aCampos, {|x| x[1] == "O0W_VREMOT"})
	Local nVlrRemApr := aCampos[nPosRemApr][2]//Valor a aprovar remoto
	Local nVlrIncAtu := 0 //Valor atual incontroverso
	Local nPosIncApr := Ascan(aCampos, {|x| x[1] == "O0W_VINCON"})
	Local nVlrIncApr := aCampos[nPosIncApr][2]//Valor a aprovar incontroverso
	Local dDtPedAtu := "" //Data atual do pedido
	Local nDtPedApr := Ascan(aCampos, {|x| x[1] == "O0W_DATPED"})
	Local dDtPedApr := aCampos[nDtPedApr][2]//Data a aprovar do pedido
	Local dDtJuroAtu := "" //Data atual de juros
	Local nDtJuroApr := Ascan(aCampos, {|x| x[1] == "O0W_DTJURO"})
	Local dDtJuroApr := aCampos[nDtJuroApr][2]//Data a aprovar  de juros
	Local cForCorAtu := "" //Forma de corre��o atual
	Local nForCorApr := Ascan(aCampos, {|x| x[1] == "O0W_CFRCOR"})
	Local cForCorApr := cValToChar(aCampos[nForCorApr][2])//Forma de corre��o a aprovar
	Local dDataFup   := DataValida(Date(),.T.)

	ProcRegua(0)
	IncProc()
	IncProc()

	//Carerga follow-up
	If !Empty(aTipoFw)
		cTipoFw		:= aTipoFw[1]
		nDiaPrazo	:= Val( aTipoFw[2] ) //Verificar se o campo NQS_DPRAZO sera mesmo caracter
	EndIf

	//Carrega campos atuais
	If nJ270Opc == MODEL_OPERATION_UPDATE
		cProgAtual := O0W->O0W_PROGNO
		nVlrPedAtu := O0W->O0W_VPEDID
		nVlrPrvAtu := O0W->O0W_VPROVA
		nVlrPssAtu := O0W->O0W_VPOSSI
		nVlrRemAtu := O0W->O0W_VREMOT
		nVlrIncAtu := O0W->O0W_VINCON
		dDtPedAtu :=  O0W->O0W_DATPED
		dDtJuroAtu := O0W->O0W_DTJURO
		cForCorAtu := O0W->O0W_CFRCOR
	EndIf

	cDesc := STR0020 + JurGetDados("NSP", 1, xFilial("NSP") + oModelO0W:GetValue("O0W_CTPPED"), "NSP_DESC") //"Aprova��o de Altera��o no Pedido: "

	cDesc += CRLF + STR0021 + AllTrim( Transform(nValorO0W , "@E 99,999,999,999.99") ) //"Valor para aprova��o: "

	cDesc += CRLF + STR0022 + cProgAtual //"Progn�stico atual: "
	cDesc += CRLF + STR0023 + cProgAprov //"Progn�stico ap�s aprova��o: "

	cDesc += CRLF + STR0037 + if(!Empty(dDtPedAtu),DToC(dDtPedAtu),"") //"Data atual do pedido: "
	cDesc += CRLF + STR0038 + DToC(dDtPedApr) //"Data do pedido ap�s aprova��o: "

	cDesc += CRLF + STR0039 + JurGetDados("NW7", 1, xFilial("NW7") + cForCorAtu, "NW7_DESC")//"Forma de corre��o atual: "
	cDesc += CRLF + STR0040 + JurGetDados("NW7", 1, xFilial("NW7") + cForCorApr, "NW7_DESC") //"Forma de corre��o ap�s aprova��o: "

	cDesc += CRLF + STR0024 + AllTrim( Transform(nVlrPedAtu, "@E 99,999,999,999.99") ) //"Valor do pedido atual: "
	cDesc += CRLF + STR0025 + AllTrim( Transform(nVlrPedApr, "@E 99,999,999,999.99") ) //"Valor do pedido ap�s aprova��o: "

	cDesc += CRLF + STR0026 + AllTrim( Transform(nVlrPrvAtu, "@E 99,999,999,999.99") ) //"Valor prov�vel atual: "
	cDesc += CRLF + STR0027 + AllTrim( Transform(nVlrPrvApr, "@E 99,999,999,999.99") ) //"Valor prov�vel ap�s aprova��o: "

	cDesc += CRLF + STR0028 + AllTrim( Transform(nVlrPssAtu, "@E 99,999,999,999.99") ) //"Valor poss�vel atual: "
	cDesc += CRLF + STR0029 + AllTrim( Transform(nVlrPssApr, "@E 99,999,999,999.99") ) //"Valor poss�vel ap�s aprova��o: "

	cDesc += CRLF + STR0030 + AllTrim( Transform(nVlrRemAtu, "@E 99,999,999,999.99") ) //"Valor remoto atual: "
	cDesc += CRLF + STR0031 + AllTrim( Transform(nVlrRemApr, "@E 99,999,999,999.99") ) //"Valor remoto ap�s aprova��o: "

	cDesc += CRLF + STR0032 + AllTrim( Transform(nVlrIncAtu, "@E 99,999,999,999.99") ) //"Valor incontroverso atual: "
	cDesc += CRLF + STR0033 + AllTrim( Transform(nVlrIncApr, "@E 99,999,999,999.99") ) //"Valor incontroverso ap�s aprova��o: "

	cDesc += CRLF + STR0041 + if(!Empty(dDtJuroAtu),DToC(dDtJuroAtu),"") //"Data de juros atual: "
	cDesc += CRLF + STR0042 + if(!Empty(dDtJuroApr),DToC(dDtJuroApr),"") //"Data de juros ap�s aprova��o: "

	//Ja existe follow-up pendente e j� esta posicionado
	If lPendente
		nOpcFw    := 4
		cResultFw := JurGetDados("NQN", 3, xFilial("NQN") + "2", "NQN_COD") //NQN_FILIAL + NQN_TIPO 2=Concluido
		cDesc     := AllTrim(cDesc) + CRLF + Replicate("-", 5) + CRLF + AllTrim(NTA->NTA_DESC)

		aAdd(aNZM, {"NZM_CODWF"	, AllTrim(NTA->NTA_CODWF)} )
		aAdd(aNZM, {"NZM_CAMPO"	, "sObsExecutor"         } )
		aAdd(aNZM, {"NZM_CSTEP"	, "16"                   } )
		aAdd(aNZM, {"NZM_STATUS", "2"                    } )
	EndIf

	Aadd(aNTA, {"NTA_CAJURI", cProcesso         } )
	Aadd(aNTA, {"NTA_CTIPO" , cTipoFw           } )
	Aadd(aNTA, {"NTA_DTFLWP", dDataFup          } )
	Aadd(aNTA, {"NTA_CRESUL", cResultFw         } )
	Aadd(aNTA, {"NTA__VALOR", Abs( nValorO0W )  } )
	Aadd(aNTA, {"NTA_DESC"  , cDesc             } )

	//Carerga participante
	Aadd(aNTE, {"NTE_SIGLA", cSigla} )
	Aadd(aNTE, {"NTE_CPART", cPart } )

	//Carrega Tarefas do Follow-up
	For nCont := 1 To Len( aCampos )

		If !aCampos[nCont][1] $ "PROV_O0W/O0W_VPOSSI/O0W_PROGNO" // n�o � poss��vel atualizar o valor poss�vel

			Do Case
			Case ValType( aCampos[nCont][2] ) == "D"
				cConteudo := DtoS( aCampos[nCont][2] )
			Case ValType( aCampos[nCont][2] ) == "N"
				cConteudo := cValToChar( aCampos[nCont][2] )
			OtherWise
				cConteudo := aCampos[nCont][2]
			End Case

			aAux := {}
			Aadd(aAux, {"NZK_STATUS", "1"               } ) //1=Em Aprovacao
			Aadd(aAux, {"NZK_FONTE"	, "JURA270"         } )
			Aadd(aAux, {"NZK_MODELO", "O0WDETAIL"       } )
			Aadd(aAux, {"NZK_CAMPO" , aCampos[nCont][1] } )
			Aadd(aAux, {"NZK_VALOR" , cConteudo         } )
			Aadd(aAux, {"NZK_CHAVE" , xFilial("O0W") + ;
				oModelO0W:GetValue("O0W_COD") } ) //O0W_FILIAL+O0W_COD

			Aadd( aNZK, aAux )
		EndIf
	Next nCont

	//Prepara follow-up para inclusao
	oModelFw := FWLoadModel("JURA106")
	oModelFw:SetOperation(nOpcFw)
	oModelFw:Activate()

	//Atualiza follow-up
	For nCont:=1 To Len( aNTA )

		If aNTA[nCont][1] == "NTA_CAJURI"

			If nOpcFw == 3
				oModelFw:LoadValue("NTAMASTER", aNTA[nCont][1], aNTA[nCont][2])
			EndIf

			Loop
		EndIf

		If aNTA[nCont][1] == "NTA_CRESUL"

			If nOpcFw == 4
				oModelFw:LoadValue("NTAMASTER", aNTA[nCont][1], aNTA[nCont][2])
			Else
				If !( oModelFw:SetValue("NTAMASTER", aNTA[nCont][1], aNTA[nCont][2]) )
					lRet := .F.
					Exit
				EndIf
			EndIf
		Else
			If !( oModelFw:SetValue("NTAMASTER", aNTA[nCont][1], aNTA[nCont][2]) )
				lRet := .F.
				Exit
			EndIf
		EndIf
	Next nCont

	If lRet

		If nOpcFw == 3 //Somente se for uma inclus�o
			//Atualiza participante
			For nCont:=1 To Len( aNTE )
				If !( oModelFw:SetValue("NTEDETAIL", aNTE[nCont][1], aNTE[nCont][2]) )
					lRet := .F.
					Exit
				EndIf
			Next nCont
		EndIf

		If nOpcFw == 4 //Somente se for uma altera��o
			//Atualiza participante
			For nCont:=1 To Len( aNZM )
				If !( oModelFw:SetValue("NZMDETAIL", aNZM[nCont][1], aNZM[nCont][2]) )
					lRet := .F.
					Exit
				EndIf
			Next nCont
		EndIf

		If lRet

			//Atualiza tarefas do follow-up
			For nReg:=1 To Len( aNZK )

				If nReg > 1
					oModelFw:GetModel("NZKDETAIL"):AddLine()
				EndIf

				For nCont:=1 To Len( aNZK[nReg] )
					If !( oModelFw:SetValue("NZKDETAIL", aNZK[nReg][nCont][1], aNZK[nReg][nCont][2]) )
						lRet := .F.
						Exit
					EndIf
				Next nCont
			Next nReg

			//Inclui follow-up
			If lRet
				If ( lRet := oModelFw:VldData() )
					lRet := oModelFw:CommitData()
				EndIf
			EndIf
		EndIf
	EndIf

	If lRet

		cCodWF := oModelFw:GetValue("NTAMASTER", "NTA_CODWF")

		//valida se o follow-up est� conclu�do ou em aprova��o
		cNQNTipoF := JurGetDados('NQN',1,xFilial('NQN')+oModelFw:GetValue("NTAMASTER","NTA_CRESUL"),"NQN_TIPO")

		if (cNQNTipoF == "2")
			//Volta os valores pois o FW foi conclu��do.
			For nCont := 1 to Len(aCampos)
				If aCampos[nCont][1] != "PROV_O0W"
					oModelO0W:LoadValue(aCampos[nCont][1],aCampos[nCont][2])
				Endif
			Next
		Else
			//Exibe mensagem de aprova��o
			ApMsgInfo(	STR0034 + CRLF + CRLF +; //"Aprova��o enviada para o FLUIG." *****
			STR0035 , ProcName(0) ) //"Os dados alterados ser�o atualizados quando a aprova��o for conclu�da." *****
		Endif
	Else
		aErroNTA := oModelFw:GetErrorMessage()
	EndIf

	oModelFw:DeActivate()
	oModelFw:Destroy()

	FWModelActive( oModel )
	oModel:Activate()

	If lRet
		oModelO0W:LoadValue("O0W_CODWF", cCodWF)
	Else
		//Seta erro no modelo atual para retornar mensagem
		If Len(aErroNTA) > 0
			oModel:SetErrorMessage(aErroNTA[1], aErroNTA[2], aErroNTA[3], aErroNTA[4] , aErroNTA[5],;
				STR0036 + CRLF + ; //"N�o foi poss��vel inclui�r o follow-up de aprova��o. Verifique!"
			aErroNTA[6], aErroNTA[7], /*xValue*/ , /*xOldValue*/ )
		EndIf
	EndIf

	RestArea(aAreaNTA)
	RestArea(aArea)

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J270OpcVerba(cVerba)
Verifica se existem objetos para a verba.

@param 	cVerba      C�digo da verba
@Return lRet        .T./.F. Existe ou n�o objetos para a verba

@since 10/01/2020
/*/
//-------------------------------------------------------------------
Static Function J270OpcVerba(cVerba)
	Local nRet       := 3
	Local cQuery     := ""
	Local cAlias     := ""

	cQuery += "SELECT 1 FROM " + RetSqlName("NSY")
	cQuery += "WHERE NSY_CVERBA = '" + cVerba + "' "
	cQuery += "AND NSY_FILIAL = '" + xFilial("NSY") + "' "
	cQuery += "AND D_E_L_E_T_ = ' ' "

	cQuery := ChangeQuery(cQuery)

	cAlias := GetNextAlias()
	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )

	//Se existirem Objetos para a verba
	If (cAlias)->(!Eof())
		nRet := 4 //Altera��o
	EndIf

	(cAlias)->(DbCloseArea())

Return nRet


//-------------------------------------------------------------------
/*/{Protheus.doc} JA270VlrMu
Retorna a soma do valor de multa, encargo e honorario de todos os objetos vinculado ao processo, de um progn�stico e uma 
verba especifica

@param cProcesso - Numero do processo

@return nMultaTot - total da soma de multa, encargos e honor�rios

@since 03/02/2020
/*/
//-------------------------------------------------------------------
Function JA270VlrMu(cProcesso)

	Local aArea     := GetArea()
	Local cLista    := GetNextAlias()
	Local nMultaTot := 0
	Local cQuery    := ""
	Local aMultas   := {}

	cQuery += "SELECT ISNULL(SUM(( CASE "
	cQuery +=                       "WHEN NSY_TRVLRA > 0 THEN NSY_TRVLRA "
	cQuery +=                       "ELSE NSY_TRVLR "
	cQuery +=                     "END )), 0) HONORARIO, "
	cQuery +=        "ISNULL(SUM(( CASE "
	cQuery +=                       "WHEN NSY_MUATT > 0 THEN NSY_MUATT "
	cQuery +=                       "ELSE NSY_VLRMT "
	cQuery +=                     "END )), 0) MULTA_HONORARIO, "
	cQuery +=        "ISNULL(SUM(( CASE "
	cQuery +=                       "WHEN NSY_V2VLRA > 0 THEN NSY_V2VLRA "
	cQuery +=                       "ELSE NSY_V2VLR "
	cQuery +=                     "END )), 0) ENCARGO, "
	cQuery +=        "ISNULL(SUM(( CASE "
	cQuery +=                       "WHEN NSY_MUATU2 > 0 THEN NSY_MUATU2 "
	cQuery +=                       "ELSE NSY_VLRMU2 "
	cQuery +=                     "END )), 0) MULTA_ENCARGO, "
	cQuery +=        "ISNULL(SUM(( CASE "
	cQuery +=                       "WHEN NSY_V1VLRA > 0 THEN NSY_V1VLRA "
	cQuery +=                       "ELSE NSY_V1VLR "
	cQuery +=                     "END )), 0) MULTA, "
	cQuery +=        "NQ7_TIPO"
	cQuery += "FROM "+RetSqlname('NSY')+" NSY "
	cQuery += "INNER JOIN "+RetSqlname('NQ7')+" NQ7 "
	cQuery +=       "ON NQ7_FILIAL = '" + xFilial('NQ7') + "' "
	cQuery +=       "AND NSY_CPROG = NQ7_COD "
	cQuery += "WHERE  NSY_FILIAL = '" + xFilial('NSY') + "' "
	cQuery +=    "AND NSY_CAJURI = '" + cProcesso + "' "
	cQuery +=    "AND NSY_CVERBA <> ' ' "
	cQuery +=    "AND NSY.D_E_L_E_T_ = ' ' "
	cQuery +=    "AND NQ7.D_E_L_E_T_ = ' ' "
	cQuery +=    " GROUP BY NQ7_TIPO "

	cQuery := ChangeQuery(cQuery)

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery ), cLista,.T.,.T.)

	If (cLista)->(!Eof())
		While (cLista)->(!Eof())
			nMultaTot += (cLista)->HONORARIO + (cLista)->MULTA_HONORARIO
			nMultaTot += (cLista)->ENCARGO + (cLista)->MULTA_ENCARGO
			nMultaTot += (cLista)->MULTA
			aAdd(aMultas,{(cLista)->NQ7_TIPO, nMultaTot})
			nMultaTot := 0
			(cLista)->(dbSkip())
		EndDo
	EndIf

	(cLista)->( dbcloseArea() )
	RestArea( aArea )

Return aMultas

//-------------------------------------------------------------------
/*/{Protheus.doc} JMontaURL(cCodWf)
Monta a URL de acesso ao Workflow no fluig.

@param 	cCodWf      C�digo do Workflow

@Return cUrlRet     URL do workflow

@since 14/01/2020
/*/
//-------------------------------------------------------------------
Static Function JMontaURL(cCodWf)
	Local cUrlRet    := ""
	Local cUrl       := StrTran(AllTrim(JFlgUrl()), '/webdesk/', '')
	Local cEmpresa   := AllTrim(SuperGetMV('MV_ECMEMP' ,,""))

	If Empty(cEmpresa)
		cEmpresa := AllTrim(SuperGetMV('MV_ECMEMP2' ,,""))
	EndIf

	If !Empty(cUrl) .And. !Empty(cCodWf) .And. !Empty(cEmpresa)
		cUrlRet := cUrl
		cUrlRet += '/portal/p/'
		cUrlRet += cEmpresa
		cUrlRet += '/pageworkflowview?app_ecm_workflowview_detailsProcessInstanceID='
		cUrlRet += cCodWf
	EndIf

Return cUrlRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J270AtuRed(oModelO0W)
Atualiza o campo de valor de redutor de acordo com o prognostico de cada linha da o0w

@param oModelO0W      Modelo detailO0w
@Return Nil
@since 17/08/2020
/*/
//-------------------------------------------------------------------

Static Function J270AtuRed(oModelO0W)
	Local aArea    := GetArea()
	Local aAreaO0W := O0W->(GetArea())
	Local cAlias   := GetNextAlias()
	Local cProgno  := ""
	Local cVerba   := ""
	Local cCajuri  := oModelO0W:GetValue("O0W_CAJURI")
	Local cQuery   := ""
	Local nI       := 0
	Local aProg    := {}
	Local nPosProg := 0

	aAdd( aProg, {STR0004, '1'} )
	aAdd( aProg, {STR0005, '2'} )
	aAdd( aProg, {STR0007, '3'} )
	aAdd( aProg, {STR0006, '4'} )
	

	DbSelectArea("O0W")
	For nI := 1 to oModelO0W:Length()
		if !oModelO0W:isDeleted(nI)
			oModelO0W:GoLine(nI)
			
			nPosProg := aScan(aProg, {|x| x[1] == AllTrim(oModelO0W:GetValue("O0W_PROGNO",nI))})
			cProgno  := JGetProgn(aProg[nPosProg][2])[1]
			cVerba   := oModelO0W:GetValue("O0W_COD",nI)

			cQuery := " SELECT NSY_VLREDU FROM "+ RetSQLName('NSY')
			cQuery += " WHERE NSY_CVERBA = '"+ cVerba +"'"
			cQuery +=       " AND NSY_CAJURI = '" + cCajuri + "'"
			cQuery +=       " AND NSY_CPROG = '" + cProgno + "'"
			cQuery +=       " AND D_E_L_E_T_ = ''"

			cQuery := ChangeQuery(cQuery)

			DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )

			If (cAlias)->(!Eof())
				O0W->( DbSetOrder(3) ) // O0W_FILIAL+O0W_CAJURI+O0W_COD+O0W_CTPPED
				If O0W->( DbSeek(xFilial("O0W") + cCajuri + cVerba))
					RecLock("O0W", .F.)
					// Atualiza o campo de valor de redutor
					O0W->O0W_VLREDU := (cAlias)->NSY_VLREDU
					O0W->( MsUnLock() )
				EndIf
			EndIf
			(cAlias)->(DbCloseArea())
		Endif
	Next
	O0W->(DbCloseArea())


	RestArea(aArea)
	RestArea(aAreaO0W)

Return Nil
