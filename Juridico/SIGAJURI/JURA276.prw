#Include "JURA276.ch"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH"
#INCLUDE "FWMVCDEF.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} JURA276
Modelos de Exportação Personalizada

@since 23/06/2020
/*/
//-------------------------------------------------------------------
Function JURA276()

Local oBrowse

oBrowse := FWMBrowse():New()
oBrowse:SetDescription( STR0001 ) //"Modelos de Exportação Personalizada"
oBrowse:SetAlias( "NQ5" )
oBrowse:SetLocate()
JurSetBSize( oBrowse )

oBrowse:Activate()

Return NIL

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menu Funcional

@return aRotina - Estrutura
@since 23/06/2020
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
Local aRotina := {}

aAdd( aRotina, { STR0002, "PesqBrw"        , 0, 1, 0, .T. } ) //"Pesquisar"
aAdd( aRotina, { STR0003, "VIEWDEF.JURA276", 0, 2, 0, NIL } ) //"Visualizar"
aAdd( aRotina, { STR0004, "VIEWDEF.JURA276", 0, 3, 0, NIL } ) //"Incluir"
aAdd( aRotina, { STR0005, "VIEWDEF.JURA276", 0, 4, 0, NIL } ) //"Alterar"
aAdd( aRotina, { STR0006, "VIEWDEF.JURA276", 0, 5, 0, NIL } ) //"Excluir"

Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados 

@since 23/06/2020
/*/
//-------------------------------------------------------------------
Static Function Modeldef()
Local oModel     := NIL
Local oStructNQ5 := FWFormStruct( 1, "NQ5" )
Local oStructNQ8 := FWFormStruct( 1, "NQ8" )

	oModel:= MPFormModel():New( "JURA276", /*Pre-Validacao*/, /*Pos-Validacao*/, /*Commit*/,/*Cancel*/)
	oModel:SetDescription( STR0007 ) // "Modelo de Dados de Exportação Personalizada"
	oModel:AddFields( "NQ5MASTER", NIL, oStructNQ5, /*Pre-Validacao*/, /*Pos-Validacao*/ )

	//-- Grid dos campos da configuração do modelo de Exportação
	oModel:AddGrid( "NQ8DETAIL", "NQ5MASTER" /*cOwner*/, oStructNQ8, /*bLinePre*/, /*bLinePost*/,/*bPre*/, /*bPost*/)
	oModel:SetRelation( "NQ8DETAIL", { { "NQ8_FILIAL", "xFilial('NQ8')" }, { "NQ8_CCONFG", "NQ5_COD" } }, NQ8->( IndexKey( 1 ) ) )
	oModel:SetOptional( 'NQ8DETAIL' , .T. )

	JurSetRules( oModel, 'NQ5MASTER',, 'NQ5' )

Return oModel
