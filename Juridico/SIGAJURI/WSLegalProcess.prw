#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "WSLEGALPROCESS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FILEIO.CH"
#INCLUDE "PARMTYPE.CH"
#INCLUDE "SHELL.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} WSLegalProcess
M�todos WS do Jur�dico para integra��o com o LegalProcess.

@author SIGAJURI
@since 11/03/17
@version 1.0

/*/
//-------------------------------------------------------------------

WSRESTFUL JURLEGALPROCESS DESCRIPTION STR0001 //"WS de Integra��o com LegalProcess"

	WSDATA page        AS INTEGER
	WSDATA pageSize    AS INTEGER
	WSDATA codProc     AS STRING
	WSDATA searchKey   AS STRING
	WSDATA qryorder    AS STRING
	WSDATA tpFilter    AS STRING
	WSDATA saldoNT2    AS STRING
	WSDATA nomeEnt     AS STRING
	WSDATA codEntidade AS STRING
	WSDATA codDoc      AS STRING
	WSDATA qtdCarac    AS INTEGER // Quantidade de caracteres que ser�o exibidos no campo NT4_DESC
	WSDATA codTabela   AS STRING
	WSDATA chaveTab    AS STRING
	WSDATA codParam    AS STRING
	WSDATA url         AS STRING
	WSDATA campof3     AS STRING
	WSDATA codComarca  AS STRING
	WSDATA codForo	   AS STRING
	WSDATA codDistr    AS STRING
	WSDATA tipoDistr   AS STRING
	WSDATA codFil      AS STRING
	WSDATA dataIni     AS STRING
	WSDATA dataFinal   AS STRING
	WSDATA campo       AS STRING
	WSDATA codWf       AS STRING
	WSDATA tipoAssjur  AS STRING
	WSDATA nomeFunc    AS STRING
	WSDATA buscaInfo   AS STRING
	WSDATA pathArq     AS STRING
	WSDATA usuario     AS ARRAY

	// M�todos GET
	WSMETHOD GET    ListProcess     DESCRIPTION STR0002  PATH "process"                                    PRODUCES APPLICATION_JSON //"Listagem de Processos"
	WSMETHOD GET    DetailProcess   DESCRIPTION STR0003  PATH "process/{codProc}"                          PRODUCES APPLICATION_JSON //"Detalhe do Processo"
	WSMETHOD GET    ListAreas       DESCRIPTION STR0004  PATH "area"                                       PRODUCES APPLICATION_JSON //"Listagem de �reas"
	WSMETHOD GET    ListFup         DESCRIPTION STR0013  PATH "process/{codProc}/fups"                     PRODUCES APPLICATION_JSON //"Listagem de Followups"
	WSMETHOD GET    StockEvolution  DESCRIPTION STR0013  PATH "evolution"                                  PRODUCES APPLICATION_JSON //"Evolu��o do Estoque de a��es"
	WSMETHOD GET    SearchProcess   DESCRIPTION STR0019  PATH "searchProcess"                              PRODUCES APPLICATION_JSON //"Pesquida r�pida de Processos"
	WSMETHOD GET    ListAndamentos  DESCRIPTION STR0019  PATH "process/{codProc}/andamentos"               PRODUCES APPLICATION_JSON //"Listagem de Andamentos"
	WSMETHOD GET    ListDoc         DESCRIPTION STR0020  PATH "process/{codProc}/docs/{nomeEnt}"           PRODUCES APPLICATION_JSON //"Lista docs"
	WSMETHOD GET    DwnlFile        DESCRIPTION STR0021  PATH "downloadFile"                               PRODUCES APPLICATION_JSON //"Download de arquivos"
	WSMETHOD GET    DownloadFile    DESCRIPTION STR0021  PATH "anexo/{codDoc}"                             PRODUCES APPLICATION_JSON //"Download de arquivos"
	WSMETHOD GET    DetalheProcesso DESCRIPTION STR0003  PATH "tlprocess/detail/{codProc}"                 PRODUCES APPLICATION_JSON //"Informa��es Resumidas do Processo"       PRODUCES APPLICATION_JSON //"Pesquida r�pida de Processos"
	WSMETHOD GET    GrpAprovNT2     DESCRIPTION STR0026  PATH "tlprocess/grupoAprv"                        PRODUCES APPLICATION_JSON //"Busca o grupo de Aprova��o da Garantia"
	WSMETHOD GET    TabGenerica     DESCRIPTION STR0027  PATH "tlprocess/tabGen/{codTabela}"               PRODUCES APPLICATION_JSON //"Busca nas tabelas gen�ricas (SX5)"
	WSMETHOD GET    SysParam        DESCRIPTION STR0028  PATH "tlprocess/sysParam/{codParam}"              PRODUCES APPLICATION_JSON //"Consulta de Par�metros do sistema"
	WSMETHOD GET    Favorite        DESCRIPTION STR0029  PATH "favorite"                                   PRODUCES APPLICATION_JSON //"Busca os processos favoritos"
	WSMETHOD GET    ListFields      DESCRIPTION STR0031  PATH "fields"                                     PRODUCES APPLICATION_JSON //"Listagem de Campos para Pesquisa Avan�ada"
	WSMETHOD GET    GetListF3       DESCRIPTION STR0032  PATH "f3list/{campof3}"                           PRODUCES APPLICATION_JSON //"Listagem de �tens para campo tabelado - F3"
	WSMETHOD GET    SrchForo        DESCRIPTION STR0034  PATH "comarca/{codComarca}/foros"                 PRODUCES APPLICATION_JSON //"Busca todas as varas de um Foro"
	WSMETHOD GET    LtFuncionarios  DESCRIPTION STR0036  PATH "funcionarios"                               PRODUCES APPLICATION_JSON //"Listagem de Funcion�rios"
	WSMETHOD GET    DtRecebidas     DESCRIPTION STR0037  PATH "distr/list/{tipoDistr}"                     PRODUCES APPLICATION_JSON //"Distribui��es / Recebidas"
	WSMETHOD GET    J219Distr       DESCRIPTION STR0038  PATH "distr/cod/{codDistr}"                       PRODUCES APPLICATION_JSON //"Informa��es de uma distribui��o"
	WSMETHOD GET    SrchVara        DESCRIPTION STR0035  PATH "comarca/{codComarca}/foros/{codForo}/varas" PRODUCES APPLICATION_JSON //"Busca todas as varas de um Foro"
	WSMETHOD GET    EmpresaLogada   DESCRIPTION STR0044  PATH "empresaLogada"                              PRODUCES APPLICATION_JSON //"Busca a Empresa Logada"
	WSMETHOD GET    FilUserList     DESCRIPTION STR0045  PATH "listFiliaisUser"                            PRODUCES APPLICATION_JSON // " Busca a lista de filiais que o usu�rio tem acesso"
	WSMETHOD GET    TpAssuntoJur    DESCRIPTION STR0046  PATH "listTpAssuntoJur"                           PRODUCES APPLICATION_JSON // "Busca assuntos juridicos que contenha inst�ncia, vinculados ao usuario "
	WSMETHOD GET    Historico       DESCRIPTION STR0047  PATH "process/{codProc}/historico"                PRODUCES APPLICATION_JSON // "Hist�rico de Altera��es"
	WSMETHOD GET    StatusSolic     DESCRIPTION STR0049  PATH "solicitation/{codWf}"                       PRODUCES APPLICATION_JSON // "Busca o status da solicita��o no Fluig"
	WSMETHOD GET    ExportPDF       DESCRIPTION STR0050  PATH "exportpdf/{codProc}"                        PRODUCES APPLICATION_JSON // "Exporta resumo do processo em PDF"
	WSMETHOD GET    ListFavorite    DESCRIPTION STR0029  PATH "listFavorite"                               PRODUCES APPLICATION_JSON // "Lista os favoritos do processo."
	WSMETHOD GET    getSequencial   DESCRIPTION STR0053  PATH "getSequencial"                              PRODUCES APPLICATION_JSON // "Busca o proximo sequencial disponivel na tabela SA1"
	WSMETHOD GET    SeqNXY          DESCRIPTION STR0054  PATH "getSeqNXY"                                  PRODUCES APPLICATION_JSON // "Busca o proximo sequencial disponivel na tabela SA1"

	// M�todos POST
	WSMETHOD POST   Favorite        DESCRIPTION STR0030  PATH "favorite"       PRODUCES APPLICATION_JSON //"Inclui/ exclui os processos da tabela de favoritos"
	WSMETHOD POST   UploadFile      DESCRIPTION STR0025  PATH "anexo"          PRODUCES APPLICATION_JSON //"Upload de arquivos"
	WSMETHOD POST   leInicial       DESCRIPTION STR0039  PATH "inicial"        PRODUCES APPLICATION_JSON //"Leitura de Iniciais"
	WSMETHOD POST   SetFilter       DESCRIPTION STR0025  PATH "setfilter"      PRODUCES APPLICATION_JSON
	WSMETHOD POST   RltPesq         DESCRIPTION STR0040  PATH "exportPesquisa" PRODUCES APPLICATION_JSON //"Realiza a exporta��o da Pesquisa Avan�ada"
	WSMETHOD POST   VldCNJ          DESCRIPTION STR0048  PATH "validNumPro"    PRODUCES APPLICATION_JSON //"Valida o n�mero CNJ e busca comarca, foro e vara no cadastro De/Para de comarcas"
	WSMETHOD POST   distribuicoes   DESCRIPTION STR0058  PATH "distribuicoes"  PRODUCES APPLICATION_JSON //"Realiza a importa��o em lote de distribui��es"

	// M�todos PUT
	WSMETHOD PUT    UpdateDistr     DESCRIPTION STR0041  PATH "distr/cod/{codDistr}/proc/{codProc}" PRODUCES APPLICATION_JSON //"Atualiza��o da distribui��o"

	// M�todos DELETE
	WSMETHOD DELETE DeleteDoc       DESCRIPTION STR0022  PATH "process/{codProc}/docs/{codDoc}" PRODUCES APPLICATION_JSON //"Deleta anexos"
END WSRESTFUL

//-------------------------------------------------------------------
/*/{Protheus.doc} _JURFWREST

@author SIGAJURI
@since 11/03/17
@version 1.0
/*/
//-------------------------------------------------------------------

Function _JURLEGALP
Return


//-------------------------------------------------------------------
/*/{Protheus.doc} JGetNqnDes(nCodNqn)
Retorna a descri��o do status do tipo de resultado

@param nCodNqn - C�digo da NQN

@Return cDesNqn - Descritivo da Nqn

@author Willian Kazahaya
@since 08/12/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function JGetNqnDes(nCodNqn)
	Local cDesNqn := ""
	//1=Pendente;2=Conclu�do;3=Cancelado;4=Em Aprova��o;5=Em aprova��o
	Do Case
	Case nCodNqn == "1"
		cDesNqn := STR0014
	Case nCodNqn == "2"
		cDesNqn := STR0015
	Case nCodNqn == "3"
		cDesNqn := STR0016
	Case nCodNqn == "4"
		cDesNqn := STR0017
	Case nCodNqn == "5"
		cDesNqn := STR0018
	End Case

Return cDesNqn

//-------------------------------------------------------------------
/*/{Protheus.doc} JGetInsDes(nInsta)
Retorna a descri��o da Inst�ncia

@param nInsta - Numero da Inst�ncia

@Return cDesInsta - Descri��o da inst�ncia

@author Willian Kazahaya
@since 23/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function JGetInsDes(nInsta)
	Local cDesInsta := ""

	// 1=1� Inst�ncia;2=2� Inst�ncia;3=Tribunal Superior
	Do Case
	Case nInsta == "1"
		cDesInsta := STR0010
	Case nInsta == "2"
		cDesInsta := STR0011
	Case nInsta == "3"
		cDesInsta := STR0012
	End Case
Return cDesInsta

//-------------------------------------------------------------------
/*/{Protheus.doc} JConvUTF8(cValue)
Formata o valor em UTF8 e retira os espa�os

@param nInsta - Numero da Inst�ncia

@Return cDesInsta - Descri��o da inst�ncia

@author Willian Kazahaya
@since 23/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function JConvUTF8(cValue)
	Local cReturn := ""
	cReturn := EncodeUTF8(Alltrim(cValue))
Return cReturn


//-------------------------------------------------------------------
/*/{Protheus.doc} GET ListProcess
Listagem de Processos

@author Willian Yoshiaki Kazahaya
@since 17/10/17
@version 1.0

@param nPage      - Numero da p�gina
@param nPageSize  - Quantidade de itens na p�gina
@param cSearchKey - C�digo do processo (cajuri)
@param cQryOrder  - Order by da query ("1 - Ordena decrescente por Vlr. Provis�o , 2 - Ordena decrescente por Dt. Ult. Andamento)
@param tpFilter   - Filtra a query (1 = Processos em Andamento, 2 = Casos Novos, 3 = Encerrados)
@param cTpAssJur  - Qual � o tipo de assunto jur�dico que deseja filtrar?

@example [Sem Opcional] GET -> http://127.0.0.1:12173/rest/JURLEGALPROCESS/process

/*/
//-------------------------------------------------------------------
WSMETHOD GET ListProcess WSRECEIVE page, pageSize, searchKey, qryOrder, tpFilter, tipoAssjur WSREST JURLEGALPROCESS
Local oResponse  := Nil
Local nPage      := Self:page
Local nPageSize  := Self:pageSize
Local cSearchKey := Self:searchKey
Local cQryOrder  := Self:qryOrder
Local cFilter    := Self:tpFilter
Local cTpAssJur  := Self:tipoAssjur

	Self:SetContentType("application/json")

	oResponse := JWsLpGtNsz(,nPage,nPageSize,cSearchKey,cQryOrder,cFilter,, cTpAssJur)

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))
Return .T.


//-------------------------------------------------------------------
/*/{Protheus.doc} GET DetailProcess
Detalhe do Processo

@author Willian Yoshiaki Kazahaya
@since 17/10/17
@version 1.0

@param page - Numero da p�gina
@param pageSize - Quantidade de itens na p�gina
@param saldoNT2 - Indica o saldo que deseja mostrar de garantias (1= Garantia)

@example [Sem Opcional] GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/process/{codProc}

/*/
//-------------------------------------------------------------------
	WSMETHOD GET DetailProcess PATHPARAM codProc WSRECEIVE saldoNT2 WSREST JURLEGALPROCESS
	Local oResponse := Nil
	Local cCodPro   := Self:codProc
	Local cSaldoNt2 := Self:saldoNT2

	Self:SetContentType("application/json")

	oResponse := JWsLpGtNsz(cCodPro,,,,,,cSaldoNT2)

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} GET ListArea
Listagem de Area Jur�dica

@author Willian Yoshiaki Kazahaya
@since 17/10/17
@version 1.0

@example [Sem Opcional] GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/area

/*/
//-------------------------------------------------------------------
	WSMETHOD GET ListAreas WSREST JURLEGALPROCESS
	Local oResponse := Nil

	Self:SetContentType("application/json")

	oResponse := JWsLpArea()

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} GET ListFup
Listagem de Follow-ups

@author Marcelo Araujo Dente
@since 16/11/17
@version 1.0

@param page - Numero da p�gina
@param pageSize - Quantidade de itens na p�gina

@example [Sem Opcional] GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/process/{codProc}/fups

/*/
//-------------------------------------------------------------------
	WSMETHOD GET ListFup PATHPARAM codProc WSREST JURLEGALPROCESS
	Local oResponse := JSonObject():New()
	Local cCodPro   := Self:codProc

	Self:SetContentType("application/json")
	oResponse['fup'] := {}
	oResponse['fup'] := JWsListFup(cCodPro,,,.F.)

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} GET ListAndamentos
Listagem de Andamentos

@since 23/07/19

@param cCodPro - C�digo do Processo
@param nPage - Numero da p�gina
@param nPageSize - Quantidade de itens na p�gina
@param nQtdCarac - Quantidade de caracteres exibidos no campo NT4_DESC

@example [Sem Opcional] GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/process/{codProc}/andamentos
/*/
//-------------------------------------------------------------------
	WSMETHOD GET ListAndamentos PATHPARAM codProc WSRECEIVE page, pageSize, qtdCarac, searchKey WSREST JURLEGALPROCESS
	Local oResponse  := JSonObject():New()
	Local cCodPro    := Self:codProc
	Local nPage      := Self:page
	Local nPageSize  := Self:pageSize
	Local nQtdCarac  := Self:qtdCarac
	Local cSearchKey := Self:searchKey

	Self:SetContentType("application/json")
	oResponse['history'] := {}
	oResponse['history'] := JWsListAnd(cCodPro,nPage,nPageSize,nQtdCarac,cSearchKey)

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} GET SearchProcess
Pesquisa r�pida de processos

@since 15/07/2019 palavra a ser pesquisada no Nome do Envolvido
					(NT9_NOME) e N�mero do Processo (NUQ_NUMPRO)

@param cSearchKey -

@example [Sem Opcional] GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/searchProcess
/*/
//-------------------------------------------------------------------
	WSMETHOD GET SearchProcess WSRECEIVE searchKey, pageSize WSREST JURLEGALPROCESS
	Local oResponse := Nil
	Local cSearchKey := Self:searchKey
	Local nPageSize  := Self:pageSize

	Self:SetContentType("application/json")
	oResponse := JWSPsqRNSZ(cSearchKey,nPageSize)

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} GET StockEvolution
Traz o total de casos Em andamento e a movimenta��o (Novos e Encerrados) m�s a m�s

@since 26/06/2019
@version 1.0

@example GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/evolution
/*/
//-------------------------------------------------------------------
WSMETHOD GET StockEvolution WSRECEIVE tipoAssjur WSREST JURLEGALPROCESS
Local oResponse := Nil
Local cTpAssJur := Self:tipoAssjur

	Self:SetContentType("application/json")
	oResponse := GetStkEvo(cTpAssJur)
	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} GET DetalheProcesso
Busca as informa��es do processo de forma resumida.
Utilizado na tela resumo do processo TOTVS Legal

@since 24/07/2019

@param page - Numero da p�gina
@param pageSize - Quantidade de itens na p�gina
@param saldoNT2 - Indica o saldo que deseja mostrar de garantias (1= Garantia)

@example [Sem Opcional] GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/process/{codProc}
/*/
//-------------------------------------------------------------------
	WSMETHOD GET DetalheProcesso PATHPARAM codProc WSRECEIVE saldoNT2 WSREST JURLEGALPROCESS
	Local oResponse := Nil
	Local cCodPro   := Self:codProc
	Local cSaldoNt2 := Self:saldoNT2

	Self:SetContentType("application/json")

	oResponse := JWsLpGtPro(cCodPro,,,,,,cSaldoNT2)

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} JWsLpGtNsz
Listagem de Processos

@author Willian Yoshiaki Kazahaya
@since 17/10/17
@version 1.0

@param cCodProc - C�digo do processo a ser pesquisado
@param nPage - Numero da p�gina
@param nPageSize  - Quantidade de itens na p�gina
@param cSearchKey - C�digo do processo (cajuri)
@param cQryOrder  - Order by da query ("1 - Ordena decrescente por Vlr. Provis�o , 2 - Ordena decrescente por Dt. Ult. Andamento)
@param cFilter    - Filtra a query (1 = Processos em Andamento, 2 = Casos Novos, 3 = Encerrados)
@param cSaldoNT2  - Busca o saldo da garantia (1 - garantia)
@param cTpAssJur  - Qual � o tipo de assunto jur�dico que deseja filtrar?

/*/
//-------------------------------------------------------------------
Function JWsLpGtNsz(cCodProc, nPage, nPageSize, cSearchKey, cQryOrder, cFilter, cSaldoNT2, cTpAssJur)
Local cQrySelect   := ""
Local cQryFrom     := ""
Local cQryWhere    := ""
Local cQuery       := ""
Local cCajuri      := ""
Local cInstan      := ""
Local nCount       := 0
Local oResponse    := JsonObject():New()
Local lHasNext     := .F.
Local aSQLRest     := Ja162RstUs(,,,.T.)
Local cTpAJ        := JWsLpGtNyb()
Local cAlias       := GetNextAlias()
Local nIndexJSon   := 0
Local aNuq         := {}
Local aNt9         := {}
Local aNsy         := {}
Local aNt3         := {}
Local aNt2         := {}
Local aNszEnc      := {}
Local aNT4Dec      := {}
Local aNT4Inj      := {}
Local cNszSituac   := ""
Local cNuqInstan   := ""
Local cExists      := ""
Local cWhere       := ""
Local cNSZName     := Alltrim(RetSqlName("NSZ"))
Local aFilUsr      := JURFILUSR( __CUSERID, "NSZ" )
Local cAssJurGrp   := ""

Default cCodProc   := ""
Default nPage      := 1
Default nPageSize  := 10
Default cSearchKey := ""
Default cTpAssJur  := ""
Default cQryOrder  := "0"
Default cFilter    := "0"
Default cSaldoNT2  := "0"

	cQrySelect := " SELECT NSZ.NSZ_FILIAL NSZ_FILIAL "
	cQrySelect +=       " ,NSZ.NSZ_COD    NSZ_COD "
	cQrySelect +=       " ,NSZ.NSZ_CCLIEN NSZ_CCLIEN "
	cQrySelect +=       " ,NSZ.NSZ_LCLIEN NSZ_LCLIEN "
	cQrySelect +=       " ,SA1.A1_NOME    SA1_NOME "
	cQrySelect +=       " ,NSZ.NSZ_NUMCAS NSZ_NUMCAS "
	cQrySelect +=       " ,NVE.NVE_TITULO NVE_TITULO "
	cQrySelect +=       " ,NSZ.NSZ_TIPOAS NSZ_TIPOAS "
	cQrySelect +=       " ,NYB.NYB_DESC   NYB_DESC "
	cQrySelect +=       " ,NSZ.NSZ_DTENTR NSZ_DTENTR "
	cQrySelect +=       " ,NSZ.NSZ_VLPROV VPROV "
	cQrySelect +=       " ,NSZ.NSZ_SITUAC NSZ_SITUAC "
	cQrySelect +=       " ,NYB.NYB_CORIG  NYB_CORIG "

	if (Upper(TcGetDb())) == "ORACLE"
		cQrySelect +=       " ,TO_CHAR(SUBSTR(NSZ.NSZ_DETALH,1,4000))  NSZ_DETALH "
		cQrySelect +=       " ,TO_CHAR(SUBSTR(NSZ.NSZ_OBSERV,1,4000))  NSZ_OBSERV "
	Else
		cQrySelect +=       " ,CAST(NSZ.NSZ_DETALH AS VARCHAR(4000))  NSZ_DETALH "
		cQrySelect +=       " ,CAST(NSZ.NSZ_OBSERV AS VARCHAR(4000))  NSZ_OBSERV "
	Endif

	cQrySelect +=       " ,NSZ.NSZ_CSTATL NSZ_CSTATL "
	cQrySelect +=       " ,NSZ.NSZ_DTINLI NSZ_DTINLI "
	cQrySelect +=       " ,NSZ.NSZ_DTFILI NSZ_DTFILI "

	if (Upper(TcGetDb())) == "ORACLE"
		cQrySelect +=       " ,TO_CHAR(SUBSTR(NSZ.NSZ_OBSLIV,1,4000)) NSZ_OBSLIV "
		cQrySelect +=       " ,TO_CHAR(SUBSTR(NSZ.NSZ_OBSLIR,1,4000)) NSZ_OBSLIR "
	Else
		cQrySelect +=       " ,CAST(NSZ.NSZ_OBSLIV AS VARCHAR(4000)) NSZ_OBSLIV "
		cQrySelect +=       " ,CAST(NSZ.NSZ_OBSLIR AS VARCHAR(4000)) NSZ_OBSLIR "
	Endif

	cQrySelect +=       " ,RD01.RD0_NOME  RD0_NOMERESP  "
	cQrySelect +=       " ,RD01.RD0_SIGLA RD0_SIGLARESP "
	cQrySelect +=       " ,RD02.RD0_NOME  RD0_NOMEADVO  "
	cQrySelect +=       " ,RD02.RD0_SIGLA RD0_SIGLAADVO "
	cQrySelect +=       " ,RD03.RD0_NOME  RD0_NOMEESTA  "
	cQrySelect +=       " ,RD03.RD0_SIGLA RD0_SIGLAESTA "
	cQrySelect +=       " ,NSZ.NSZ_CAREAJ NSZ_CAREAJ "
	cQrySelect +=       " ,NRB.NRB_DESC   NRB_DESC "
	cQrySelect +=       " ,NSZ.NSZ_CSUBAR NSZ_CSUBAR "
	cQrySelect +=       " ,NRL.NRL_DESC   NRL_DESC "
	cQrySelect +=       " ,NUQ.NUQ_NUMPRO NUMPRO"
	cQrySelect +=       " ,NSZ.NSZ_VAPROV NSZ_VAPROV"
	cQrySelect +=       " ,NRO.NRO_DESC NRO_DESC"
	cQrySelect +=       " ,ULTAND.NT4_DTANDA NT4_DTANDA"
	cQrySelect +=       " ,NQ4.NQ4_DESC NQ4_DESC"
	cQrySelect +=       " ,SA2.A2_NOME SA2_CORRESP"
	cQrySelect +=       " ,NSZ.NSZ_SJUIZA NSZ_SJUIZA"
	cQrySelect +=       " ,NSZ.NSZ_VAENVO NSZ_VAENVO"

	//-- Valores
	cQrySelect +=       " ,(CASE WHEN NSZ.NSZ_VAPROV = 0 "
	cQrySelect +=                " THEN NSZ.NSZ_VLPROV "
	cQrySelect +=                " ELSE NSZ.NSZ_VAPROV END "
	cQrySelect +=        "  ) VALOR "

	cQryFrom   := " FROM " + cNSZName + " NSZ INNER JOIN " + RetSqlName('SA1') + " SA1  ON (SA1.A1_COD = NSZ.NSZ_CCLIEN) "
	cQryFrom   +=                                                                    " AND (SA1.A1_LOJA = NSZ.NSZ_LCLIEN) "
	cQryFrom   +=                                                                    " AND " + JQryFilial("NSZ","SA1","NSZ","SA1")
	cQryFrom   +=                                                                    " AND (SA1.D_E_L_E_T_ = ' ') "
	cQryFrom   +=                            " LEFT JOIN " + RetSqlName('NVE') + " NVE  ON (NVE.NVE_NUMCAS = NSZ.NSZ_NUMCAS) "
	cQryFrom   +=                                                                    " AND (NVE.NVE_CCLIEN = NSZ.NSZ_CCLIEN) "
	cQryFrom   +=                                                                    " AND (NVE.NVE_LCLIEN = NSZ.NSZ_LCLIEN) "
	cQryFrom   +=                                                                    " AND (NVE.NVE_FILIAL = '" + xFilial("NVE") + "') "
	cQryFrom   +=                                                                    " AND (NVE.D_E_L_E_T_ = ' ') "
	cQryFrom   +=                            " LEFT JOIN " + RetSqlName('NYB') + " NYB  ON (NYB.NYB_COD = NSZ.NSZ_TIPOAS) "
	cQryFrom   +=                                                                    " AND (NYB.NYB_FILIAL = '" + xFilial("NYB") + "') "
	cQryFrom   +=                                                                    " AND (NYB.D_E_L_E_T_ = ' ') "
	cQryFrom   +=                            " LEFT JOIN " + RetSqlName('NYB') + " NYB2 ON (NYB2.NYB_COD = NYB.NYB_CORIG) "
	cQryFrom   +=                                                                    " AND (NYB2.NYB_FILIAL = '" + xFilial("NYB") + "') "
	cQryFrom   +=                                                                    " AND (NYB2.D_E_L_E_T_ = ' ') "
	cQryFrom   +=                            " LEFT JOIN " + RetSqlName('RD0') + " RD01 ON (RD01.RD0_CODIGO = NSZ.NSZ_CPART1) "
	cQryFrom   +=                                                                    " AND (RD01.RD0_FILIAL = '" + xFilial("RD0") + "') "
	cQryFrom   +=                                                                    " AND (RD01.D_E_L_E_T_ = ' ') "
	cQryFrom   +=                            " LEFT JOIN " + RetSqlName('RD0') + " RD02 ON (RD02.RD0_CODIGO = NSZ.NSZ_CPART2) "
	cQryFrom   +=                                                                    " AND (RD02.RD0_FILIAL = '" + xFilial("RD0") + "') "
	cQryFrom   +=                                                                    " AND (RD02.D_E_L_E_T_ = ' ') "
	cQryFrom   +=                            " LEFT JOIN " + RetSqlName('RD0') + " RD03 ON (RD03.RD0_CODIGO = NSZ.NSZ_CPART3) "
	cQryFrom   +=                                                                    " AND (RD03.RD0_FILIAL = '" + xFilial("RD0") + "') "
	cQryFrom   +=                                                                    " AND (RD03.D_E_L_E_T_ = ' ') "
	cQryFrom   +=                            " LEFT JOIN " + RetSqlName('NRB') + " NRB  ON (NRB.NRB_COD = NSZ.NSZ_CAREAJ) "
	cQryFrom   +=                                                                    " AND (NRB.NRB_FILIAL = '" + xFilial("NRB") + "') "
	cQryFrom   +=                                                                    " AND (NRB.D_E_L_E_T_ = ' ') "
	cQryFrom   +=                            " LEFT JOIN " + RetSqlName('NRL') + " NRL  ON (NRL.NRL_COD = NSZ.NSZ_CSUBAR) "
	cQryFrom   +=                                                                    " AND (NRL.NRL_CAREA = NRB.NRB_COD) "
	cQryFrom   +=                                                                    " AND (NRL.NRL_FILIAL = '" + xFilial("NRL") + "') "
	cQryFrom   +=                                                                    " AND (NRL.D_E_L_E_T_ = ' ') "
	cQryFrom   +=                            " INNER JOIN "+ RetSqlName('NUQ') + " NUQ  ON (NUQ.NUQ_CAJURI = NSZ.NSZ_COD "
	cQryFrom   +=                                                                    " AND NUQ.NUQ_INSATU  = '1' "
	cQryFrom   +=                                                                    " AND NUQ.NUQ_FILIAL  = NSZ.NSZ_FILIAL "
	cQryFrom   +=                                                                    " AND NUQ.D_E_L_E_T_ = ' ' ) "
	cQryFrom   +=                            " LEFT JOIN " + RetSqlName('SA2') + " SA2  ON (SA2.A2_COD = NUQ.NUQ_CCORRE "
	cQryFrom   +=                                                                    " AND SA2.A2_LOJA = NUQ.NUQ_LCORRE "
	cQryFrom   +=                                                                    " AND SA2.A2_FILIAL  = '" + xFilial("SA2") + "' "
	cQryFrom   +=                                                                    " AND SA2.D_E_L_E_T_ = ' ' ) "
	cQryFrom   +=                            " LEFT JOIN " + RetSqlName('NQ4') + " NQ4  ON (NQ4.NQ4_COD = NSZ.NSZ_COBJET "
	cQryFrom   +=                                                                    " AND NQ4.NQ4_FILIAL  = NSZ.NSZ_FILIAL "
	cQryFrom   +=                                                                    " AND NQ4.D_E_L_E_T_ = ' ' ) "
	cQryFrom   +=                            " LEFT JOIN (SELECT SUB4.NT4_FILIAL,SUB4.NT4_COD, SUB4.NT4_CAJURI,SUB4.NT4_CATO, SUB4.NT4_DTANDA "
	cQryFrom   +=                                        " FROM " + RetSqlName('NT4') + " SUB4 INNER JOIN (SELECT MAX(SUB2.NT4_COD) CODIGO "
	cQryFrom   +=                                                                                          " FROM " + RetSqlName('NT4') + " SUB2 INNER JOIN (SELECT SUB1.NT4_FILIAL, SUB1.NT4_CAJURI, MAX(SUB1.NT4_DTANDA) AS DTAND "
	cQryFrom   +=                                                                                                                                            " FROM " + RetSqlName('NT4') + " SUB1 "
	cQryFrom   +=                                                                                                                                           " WHERE SUB1.D_E_L_E_T_ = ' ' "
	cQryFrom   +=                                                                                                                                           " GROUP BY SUB1.NT4_FILIAL,SUB1.NT4_CAJURI) ULT_ANDA "
	cQryFrom   +=                                                                                                                               " ON (SUB2.NT4_FILIAL = ULT_ANDA.NT4_FILIAL "
	cQryFrom   +=                                                                                                                              " AND SUB2.NT4_CAJURI = ULT_ANDA.NT4_CAJURI "
	cQryFrom   +=                                                                                                                              " AND SUB2.NT4_DTANDA = ULT_ANDA.DTAND)) SUB3 "
	cQryFrom   +=                                                                             " ON (SUB4.NT4_COD = SUB3.CODIGO)) ULTAND "
	cQryFrom   +=                                                                   " ON (NSZ_FILIAL = ULTAND.NT4_FILIAL "
	cQryFrom   +=                                                                  " AND NSZ_COD = ULTAND.NT4_CAJURI ) "
	cQryFrom   +=                          " LEFT JOIN " + RetSqlName('NRO') + " NRO ON (ULTAND.NT4_CATO = NRO.NRO_COD "
	cQryFrom   +=                                                                 " AND NRO.NRO_FILIAL = NSZ.NSZ_FILIAL"
	cQryFrom   +=                                                                 " AND NRO.D_E_L_E_T_ = ' ' ) "

	// Filtar Filiais que o usu�rio possui acesso
	If ( VerSenha(114) .or. VerSenha(115) )
		cQryWhere := " WHERE NSZ.NSZ_FILIAL IN " + FORMATIN(aFilUsr[1],aFilUsr[2])
	Else
		cQryWhere := " WHERE NSZ.NSZ_FILIAL = '"+xFilial("NSZ")+"'"
	Endif

	cQryWhere  +=     " AND NSZ.D_E_L_E_T_ = ' ' "
	cQryWhere  +=     " AND NSZ.NSZ_TIPOAS IN (" + cTpAJ + ") "

	If !Empty(cSearchKey)
		cSearchKey := Lower(StrTran(JurLmpCpo( cSearchKey,.F. ),'#',''))

		cWhere    :=  " AND " + JurFormat("NT9_NOME", .T.,.T.) + " Like '%" + cSearchKey + "%'"
		cExists   :=  " AND (" + SUBSTR(JurGtExist(RetSqlName("NT9"), cWhere, "NSZ.NSZ_FILIAL"),5)
		cQryWhere += cExists

		cWhere    :=  " AND " + JurFormat("NUQ_NUMPRO", .F.,.T.) + " Like '%" + cSearchKey + "%'"
		cExists   :=  " OR " + SUBSTR(JurGtExist(RetSqlName("NUQ"), cWhere, "NSZ.NSZ_FILIAL"),5)
		cQryWhere += cExists

		cWhere    :=  " AND " + JurFormat("NSZ_DETALH", .T.,.T.) + " Like '%" + cSearchKey + "%')"
		cExists   :=  " OR " + SUBSTR(JurGtExist(cNSZName, cWhere, "NSZ.NSZ_FILIAL"),5)
		cQryWhere += cExists

	EndIf

	cQryWhere  += VerRestricao()

	If !Empty(aSQLRest)
		cQryWhere += " AND ("+Ja162SQLRt(aSQLRest, , , , , , , , , cTpAJ)+")"
	EndIf

	If !Empty(cCodProc)
		cQryWhere += " AND NSZ.NSZ_COD = '" + cCodProc + "'"
	EndIf

	// Define o Filtro aplicado na query
	If cFilter == "1" // Processos em Andamento
		cQryWhere += " AND NSZ.NSZ_SITUAC = '1' "
	ElseIf cFilter == "2" // Processos Cadastrados no m�s corrente
		cQryWhere += " AND NSZ.NSZ_DTINCL >= '" + DTOS(FirstDate( Date() )) + "' "
	ElseIf cFilter == "3" // Processos Encerrados no m�s corrente
		cQryWhere += " AND NSZ.NSZ_DTENCE >= '" + DTOS(FirstDate( Date() )) + "' "
		cQryWhere += " AND NSZ.NSZ_SITUAC = '2' "
	EndIf
	
	cAssJurGrp := JurTpAsJr(__CUSERID)
	If !Empty(cTpAssJur) .And. cTpAssJur $ cAssJurGrp
		cQryWhere += " AND NSZ.NSZ_TIPOAS IN " + FormatIn(cTpAssJur, ",")
	Else
		cQryWhere += " AND NSZ.NSZ_TIPOAS IN (" + cAssJurGrp + ") "
	EndIf

	//Define a ordena��o da query
	If  cQryOrder == "1"
		cQryOrder := " ORDER BY VALOR DESC"
	ElseIf cQryOrder == "2"
		cQryOrder := " ORDER BY ULTAND.NT4_DTANDA DESC, VALOR DESC "
	Else
		cQryOrder := " ORDER BY NSZ.NSZ_COD "
	EndIf

	cQuery := ChangeQuery(cQrySelect + cQryFrom + cQryWhere + cQryOrder)

	cQuery := StrTran(cQuery,",' '",",''")

	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )

	oResponse['processes'] := {}

	nQtdRegIni := ((nPage-1) * nPageSize)
	// Define o range para inclus�o no JSON
	nQtdRegFim := (nPage * nPageSize)
	nQtdReg    := 0

	If !Empty(cCodProc)
		oResponse['operation'] := "DetailProcess"
	Else
		oResponse['operation'] := "ListProcess"
	EndIf

	oResponse['userName'] := cUserName

	//oResponse['query'] := cQuery

	While (cAlias)->(!Eof())

		nQtdReg++
		// Verifica se o registro est� no range da pagina
		if (nQtdReg > nQtdRegIni .AND. nQtdReg <= nQtdRegFim)
			nIndexJSon++

			// Assunto Juridico
			cCajuri := (cAlias)->NSZ_COD

			Aadd(oResponse['processes'], JsonObject():New())
			oResponse['processes'][nIndexJSon]['processBranch']      := (cAlias)->NSZ_FILIAL
			oResponse['processes'][nIndexJSon]['processId']          := cCajuri
			oResponse['processes'][nIndexJSon]['assJur']             := JConvUTF8((cAlias)->NSZ_TIPOAS)
			oResponse['processes'][nIndexJSon]['assJurDesc']         := JConvUTF8((cAlias)->NYB_DESC)
			oResponse['processes'][nIndexJSon]['entryDate']          := JConvUTF8((cAlias)->NSZ_DTENTR)
			oResponse['processes'][nIndexJSon]['provisionValue']     := ROUND((cAlias)->VPROV,2)
			oResponse['processes'][nIndexJSon]['atualizedprovision'] := ROUND((cAlias)->VALOR,2)
			oResponse['processes'][nIndexJSon]['balanceInCourt']     := ROUND((cAlias)->NSZ_SJUIZA,2)
			oResponse['processes'][nIndexJSon]['atualizedinvolved']  := ROUND((cAlias)->NSZ_VAENVO,2)
			oResponse['processes'][nIndexJSon]['subject']            := JConvUTF8((cAlias)->NQ4_DESC)
			oResponse['processes'][nIndexJSon]['office']             := JConvUTF8((cAlias)->SA2_CORRESP)

			If cSaldoNT2 == "1"//valor do saldo das garantias
				oResponse['processes'][nIndexJSon]['balanceguarantees'] := ROUND(JUR98G('1',cCodProc),2)
			EndIf

			// Area
			oResponse['processes'][nIndexJSon]['area'] := {}
			Aadd(oResponse['processes'][nIndexJSon]['area'], JsonObject():New())
			aTail(oResponse['processes'][nIndexJSon]['area'])['code']        := JConvUTF8((cAlias)->NSZ_CAREAJ)
			aTail(oResponse['processes'][nIndexJSon]['area'])['description'] := JConvUTF8((cAlias)->NRB_DESC)

			// Subarea
			oResponse['processes'][nIndexJSon]['subarea'] := {}
			Aadd(oResponse['processes'][nIndexJSon]['subarea'], JsonObject():New())
			aTail(oResponse['processes'][nIndexJSon]['subarea'])['code']        := JConvUTF8((cAlias)->NSZ_CSUBAR)
			aTail(oResponse['processes'][nIndexJSon]['subarea'])['description'] := JConvUTF8((cAlias)->NRL_DESC)

			// Caso
			oResponse['processes'][nIndexJSon]['matter'] := {}
			Aadd(oResponse['processes'][nIndexJSon]['matter'], JsonObject():New())
			aTail(oResponse['processes'][nIndexJSon]['matter'])['code']        := JConvUTF8((cAlias)->NSZ_NUMCAS)
			aTail(oResponse['processes'][nIndexJSon]['matter'])['description'] := JConvUTF8((cAlias)->NVE_TITULO)

			// Cliente
			oResponse['processes'][nIndexJSon]['company'] := {}
			Aadd(oResponse['processes'][nIndexJSon]['company'], JsonObject():New())
			aTail(oResponse['processes'][nIndexJSon]['company'])['code'] := JConvUTF8((cAlias)->NSZ_CCLIEN) + '-' + JConvUTF8((cAlias)->NSZ_LCLIEN)
			aTail(oResponse['processes'][nIndexJSon]['company'])['name'] := JConvUTF8((cAlias)->SA1_NOME)

			// Ultimo Andamento
			oResponse['processes'][nIndexJSon]['lasthistory'] := {}
			Aadd(oResponse['processes'][nIndexJSon]['lasthistory'], JsonObject():New())
			aTail(oResponse['processes'][nIndexJSon]['lasthistory'])['date']  := JConvUTF8((cAlias)->NT4_DTANDA)
			aTail(oResponse['processes'][nIndexJSon]['lasthistory'])['title'] := JConvUTF8((cAlias)->NRO_DESC)

			// Status do Processo
			cNszSituac := JConvUTF8((cAlias)->NSZ_SITUAC)
			oResponse['processes'][nIndexJSon]['status'] := {}
			Aadd(oResponse['processes'][nIndexJSon]['status'], JsonObject():New())
			aTail(oResponse['processes'][nIndexJSon]['status'])['code'] := cNszSituac

			If (cNszSituac == '1')
				aTail(oResponse['processes'][nIndexJSon]['status'])['description'] := JConvUTF8(STR0005) // Em andamento
			Else
				aTail(oResponse['processes'][nIndexJSon]['status'])['description'] := JConvUTF8(STR0006) // Encerrado
			EndIf

			// Participantes do Processo
			// Respons�vel
			oResponse['processes'][nIndexJSon]['staff'] := {}
			Aadd(oResponse['processes'][nIndexJSon]['staff'], JsonObject():New())
			aTail(oResponse['processes'][nIndexJSon]['staff'])['position'] := JConvUTF8(STR0007)  // Respons�vel
			aTail(oResponse['processes'][nIndexJSon]['staff'])['name']     := JConvUTF8((cAlias)->RD0_NOMERESP)
			aTail(oResponse['processes'][nIndexJSon]['staff'])['initials'] := JConvUTF8((cAlias)->RD0_SIGLARESP)

			// Advogado
			Aadd(oResponse['processes'][nIndexJSon]['staff'], JsonObject():New())
			aTail(oResponse['processes'][nIndexJSon]['staff'])['position'] := JConvUTF8(STR0008)  // Advogado
			aTail(oResponse['processes'][nIndexJSon]['staff'])['name']     := JConvUTF8((cAlias)->RD0_NOMEADVO)
			aTail(oResponse['processes'][nIndexJSon]['staff'])['initials'] := JConvUTF8((cAlias)->RD0_SIGLAADVO)

			// Estagi�rio
			Aadd(oResponse['processes'][nIndexJSon]['staff'], JsonObject():New())
			aTail(oResponse['processes'][nIndexJSon]['staff'])['position'] := JConvUTF8(STR0009) // Estagi�rio
			aTail(oResponse['processes'][nIndexJSon]['staff'])['name']     := JConvUTF8((cAlias)->RD0_NOMEESTA)
			aTail(oResponse['processes'][nIndexJSon]['staff'])['initials'] := JConvUTF8((cAlias)->RD0_SIGLAESTA)

			// Envolvidos - Parte contraria
			oResponse['processes'][nIndexJSon]['oppositeParty'] := getPartCont(cCajuri)

			// Para o ListProcess
			If Empty(cCodProc)
				// Inst�ncia
				aNuq := JWsLpGtNuq(cCajuri,.T.) // Somente a inst�ncia atual

				oResponse['processes'][nIndexJSon]['instance'] := {}

				if !Empty(aNuq)
					nCount := 0

					For nCount := 1 to Len(aNuq)
						Aadd(oResponse['processes'][nIndexJSon]['instance'], JsonObject():New())
						cInstan    := JConvUTF8(aNuq[nCount][1])
						cNuqInstan := JConvUTF8(aNuq[nCount][3])

						aTail(oResponse['processes'][nIndexJSon]['instance'])['id']             := cInstan
						aTail(oResponse['processes'][nIndexJSon]['instance'])['instaAtual']     := JConvUTF8(aNuq[nCount][2])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['numInstance']    := JConvUTF8(aNuq[nCount][3])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['displayName']    := JGetInsDes(cNuqInstan)
						aTail(oResponse['processes'][nIndexJSon]['instance'])['processNumber']  := JConvUTF8(aNuq[nCount][4])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['natureCode']     := JConvUTF8(aNuq[nCount][5])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['nature']         := JConvUTF8(aNuq[nCount][6])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['cityCode']       := JConvUTF8(aNuq[nCount][7])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['city']           := JConvUTF8(aNuq[nCount][8])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['distribution']   := JConvUTF8(aNuq[nCount][9])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['local']          := JConvUTF8(aNuq[nCount][10])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['districtCourt']  := JConvUTF8(aNuq[nCount][11])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['branch']         := JConvUTF8(aNuq[nCount][12])
					Next
				EndIf

				// Para o DetailProcess
			Else

				// Follow-up
				oResponse['processes'][nIndexJSon]['fup'] := {}
				oResponse['processes'][nIndexJSon]['fup'] :=  JWsListFup(cCajuri,oResponse,nIndexJSon)

				// Inst�ncia
				aNuq := JWsLpGtNuq(cCajuri,.F.) // Todas as inst�ncias

				oResponse['processes'][nIndexJSon]['instance'] := {}

				if !Empty(aNuq)
					nCount := 0

					For nCount := 1 to Len(aNuq)
						Aadd(oResponse['processes'][nIndexJSon]['instance'], JsonObject():New())
						cInstan    := JConvUTF8(aNuq[nCount][1])
						cNuqInstan := JConvUTF8(aNuq[nCount][3])

						aTail(oResponse['processes'][nIndexJSon]['instance'])['id']             := cInstan
						aTail(oResponse['processes'][nIndexJSon]['instance'])['instaAtual']     := JConvUTF8(aNuq[nCount][2])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['numInstance']    := JConvUTF8(aNuq[nCount][3])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['displayName']    := JGetInsDes(cNuqInstan)
						aTail(oResponse['processes'][nIndexJSon]['instance'])['processNumber']  := JConvUTF8(aNuq[nCount][4])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['natureCode']     := JConvUTF8(aNuq[nCount][5])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['nature']         := JConvUTF8(aNuq[nCount][6])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['cityCode']       := JConvUTF8(aNuq[nCount][7])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['city']           := JConvUTF8(aNuq[nCount][8])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['distribution']   := JConvUTF8(aNuq[nCount][9])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['local']          := JConvUTF8(aNuq[nCount][10])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['districtCourt']  := JConvUTF8(aNuq[nCount][11])
						aTail(oResponse['processes'][nIndexJSon]['instance'])['branch']         := JConvUTF8(aNuq[nCount][12])

					Next
				EndIf

				// Envolvidos
				aNt9 := JWsLpGtNt9(cCajuri,0)
				oResponse['processes'][nIndexJSon]['party'] := {}

				If !Empty(aNt9)
					nCount := 0
					For nCount := 1 to Len(aNt9)
						Aadd(oResponse['processes'][nIndexJSon]['party'], JsonObject():New())
						aTail(oResponse['processes'][nIndexJSon]['party'])['id']           := JConvUTF8(aNt9[nCount][1])
						aTail(oResponse['processes'][nIndexJSon]['party'])['entity']       := JConvUTF8(aNt9[nCount][2])
						aTail(oResponse['processes'][nIndexJSon]['party'])['sourceEntity'] := JConvUTF8(aNt9[nCount][3])
						aTail(oResponse['processes'][nIndexJSon]['party'])['main']         := JConvUTF8(aNt9[nCount][4])
						aTail(oResponse['processes'][nIndexJSon]['party'])['name']         := JConvUTF8(aNt9[nCount][5])
						aTail(oResponse['processes'][nIndexJSon]['party'])['code']         := JConvUTF8(aNt9[nCount][6])
						aTail(oResponse['processes'][nIndexJSon]['party'])['relationCode'] := JConvUTF8(aNt9[nCount][7])
						aTail(oResponse['processes'][nIndexJSon]['party'])['relationName'] := JConvUTF8(aNt9[nCount][8])
						aTail(oResponse['processes'][nIndexJSon]['party'])['positionCode'] := JConvUTF8(aNt9[nCount][9])
						aTail(oResponse['processes'][nIndexJSon]['party'])['position']     := JConvUTF8(aNt9[nCount][10])
					Next
				EndIf

				// Objetos
				aNsy := JWsLpGtNsy(cCajuri)
				oResponse['processes'][nIndexJSon]['values_and_contingency'] := {}

				If !Empty(aNsy)
					For nCount := 1 to Len(aNsy)
						Aadd(oResponse['processes'][nIndexJSon]['values_and_contingency'], JsonObject():New())
						aTail(oResponse['processes'][nIndexJSon]['values_and_contingency'])['probability_of_winning']       := aNsy[nCount][6]
						aTail(oResponse['processes'][nIndexJSon]['values_and_contingency'])['description']                  := JConvUTF8(aNsy[nCount][3])

						aTail(oResponse['processes'][nIndexJSon]['values_and_contingency'])['values'] := {}
						Aadd(aTail(oResponse['processes'][nIndexJSon]['values_and_contingency'])['values'], JsonObject():New())

						aTail(aTail(oResponse['processes'][nIndexJSon]['values_and_contingency'])['values'])['description'] := JConvUTF8(aNsy[nCount][5])
						aTail(aTail(oResponse['processes'][nIndexJSon]['values_and_contingency'])['values'])['value']       := aNsy[nCount][7]
						aTail(aTail(oResponse['processes'][nIndexJSon]['values_and_contingency'])['values'])['currency']    := JConvUTF8(aNsy[nCount][10])
					Next
				EndIf

				// Andamento - Decis�o
				aNT4Dec := JWsLpGtNt4(cCajuri, 1)
				oResponse['processes'][nIndexJSon]['decisions'] := {}
				If !Empty(aNT4Dec)
					For nCount := 1 to Len(aNT4Dec)
						Aadd(oResponse['processes'][nIndexJSon]['decisions'], JsonObject():New())
						aTail(oResponse['processes'][nIndexJSon]['decisions'])['id']       := JConvUTF8(aNT4Dec[nCount][1])
						aTail(oResponse['processes'][nIndexJSon]['decisions'])['title']    := JConvUTF8(aNT4Dec[nCount][2])
						aTail(oResponse['processes'][nIndexJSon]['decisions'])['date']     := JConvUTF8(aNT4Dec[nCount][3])
						aTail(oResponse['processes'][nIndexJSon]['decisions'])['sentence'] := JConvUTF8(aNT4Dec[nCount][4])
						aTail(oResponse['processes'][nIndexJSon]['decisions'])['instance'] := JConvUTF8(aNT4Dec[nCount][5])
					Next
				EndIf

				// Andamento - Liminar
				aNT4Inj := JWsLpGtNt4(cCajuri, 2)
				oResponse['processes'][nIndexJSon]['injuctions'] := {}
				If !Empty(aNT4Inj)
					For nCount := 1 to Len(aNT4Inj)
						Aadd(oResponse['processes'][nIndexJSon]['injuctions'], JsonObject():New())
						aTail(oResponse['processes'][nIndexJSon]['injuctions'])['id']       := JConvUTF8(aNT4Inj[nCount][1])
						aTail(oResponse['processes'][nIndexJSon]['injuctions'])['title']    := JConvUTF8(aNT4Inj[nCount][2])
						aTail(oResponse['processes'][nIndexJSon]['injuctions'])['date']     := JConvUTF8(aNT4Inj[nCount][3])
						aTail(oResponse['processes'][nIndexJSon]['injuctions'])['sentence'] := JConvUTF8(aNT4Inj[nCount][4])
					Next
				EndIf

				// Andamento
				oResponse['processes'][nIndexJSon]['history'] := {}
				oResponse['processes'][nIndexJSon]['history'] := JWsListAnd(cCajuri,nPage,nPageSize,,cSearchKey)

				// Garantia
				aNt2 := JWsLpGtNt2(cCajuri)
				oResponse['processes'][nIndexJSon]['guarantees'] := {}
				If !Empty(aNt2)
					For nCount := 1 to Len(aNt2)
						Aadd(oResponse['processes'][nIndexJSon]['guarantees'], JsonObject():New())
						aTail(oResponse['processes'][nIndexJSon]['guarantees'])['identifier']  := JConvUTF8(aNt2[nCount][2])
						aTail(oResponse['processes'][nIndexJSon]['guarantees'])['description'] := JConvUTF8(aNt2[nCount][3])
						aTail(oResponse['processes'][nIndexJSon]['guarantees'])['date']        := JConvUTF8(aNt2[nCount][4])
						aTail(oResponse['processes'][nIndexJSon]['guarantees'])['value']       := aNt2[nCount][5]
					Next
				EndIf

				// Despesas
				aNt3 := JWsLpGtNt3(cCajuri)
				oResponse['processes'][nIndexJSon]['expenses'] := {}
				If !Empty(aNt3)
					For nCount := 1 to Len(aNt3)
						Aadd(oResponse['processes'][nIndexJSon]['expenses'], JsonObject():New())
						aTail(oResponse['processes'][nIndexJSon]['expenses'])['identifier']  := JConvUTF8(aNt3[nCount][3])
						aTail(oResponse['processes'][nIndexJSon]['expenses'])['description'] := JConvUTF8(aNt3[nCount][4])
						aTail(oResponse['processes'][nIndexJSon]['expenses'])['date']        := JConvUTF8(aNt3[nCount][5])
						aTail(oResponse['processes'][nIndexJSon]['expenses'])['value']       := aNt3[nCount][6]
					Next
				EndIf

				// Encerramento
				aNszEnc := JWsLpGtEnc(cCajuri)
				oResponse['processes'][nIndexJSon]['closure'] := {}
				If !Empty(aNszEnc)
					For nCount := 1 to Len(aNszEnc)
						Aadd(oResponse['processes'][nIndexJSon]['closure'], JsonObject():New())
						aTail(oResponse['processes'][nIndexJSon]['closure'])['type']        := JConvUTF8(aNszEnc[nCount][3])
						aTail(oResponse['processes'][nIndexJSon]['closure'])['description'] := JConvUTF8(aNszEnc[nCount][4])
						aTail(oResponse['processes'][nIndexJSon]['closure'])['date']        := JConvUTF8(aNszEnc[nCount][5])
						aTail(oResponse['processes'][nIndexJSon]['closure'])['finalValue']  := aNszEnc[nCount][6]
						aTail(oResponse['processes'][nIndexJSon]['closure'])['veredict']    := JConvUTF8(aNszEnc[nCount][7])
					Next
				EndIf
			EndIF
		Elseif (nQtdReg == nQtdRegFim + 1)
			lHasNext := .T.
		Endif

		(cAlias)->(DbSkip())
	End

	// Verifica se h� uma proxima pagina
	If (lHasNext)
		oResponse['hasNext'] := "true"
	Else
		oResponse['hasNext'] := "false"
	EndIf
	(cAlias)->( DbCloseArea() )
	oResponse['length'] := nQtdReg

Return oResponse

//-------------------------------------------------------------------
/*/{Protheus.doc} JWsLpGtNta(cFollowup)
Retorna um array com os Follow-ups do Assunto Jur�dico

@param cCajuri - Cajuri para consulta
@param lMax - Ir� retornar somente o Ultimo Follow-up com base na Data/Hora

@Return	aFollowup Array de Follow-ups

@author Willian Yoshiaki Kazahaya
@since 23/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function JWsLpGtNta(cCajuri, lMax)
	Local aFollowup := {}
	Local cSQL    := ''
	Local cSqlSelect, cSqlFrom, cSqlWhere
	Local aArea   := GetArea()
	Local cAlias  := GetNextAlias()
	Local cSqlMax := ""

	Default lMax := .T.

	cSqlSelect := " SELECT NTA.NTA_COD    NTA_COD "
	cSqlSelect += 		 ",NTA.NTA_DTFLWP NTA_DTFLWP "
	cSqlSelect += 		 ",NTA.NTA_HORA   NTA_HORA "
	cSqlSelect += 		 ",NTA.NTA_CRESUL NTA_CRESUL "
	cSqlSelect += 		 ",NQS.NQS_DESC   NQS_DESC "
	cSqlSelect += 		 ",NQN.NQN_TIPO   NQN_TIPO "

	cSqlFrom := " FROM " + RetSqlName("NTA") + " NTA INNER JOIN " + RetSqlName("NQN") + " NQN ON (NQN.NQN_COD = NTA.NTA_CRESUL) "
	cSqlFrom +=                 					                 	     " AND (NQN.NQN_FILIAL = '" + xFilial("NQN") + "') "
	cSqlFrom +=                 					                 	     " AND (NQN.D_E_L_E_T_ = ' ') "
	cSqlFrom +=                 " INNER JOIN " + RetSqlName("NTE") + " NTE ON (NTE.NTE_CFLWP = NTA.NTA_COD) "
	cSqlFrom +=                 					                 	     " AND (NTE.NTE_CAJURI = NTA.NTA_CAJURI) "
	cSqlFrom +=                 					                 	     " AND (NTE.NTE_FILIAL = '" + xFilial("NTE") + "') "
	cSqlFrom +=                 					                 	     " AND (NTE.D_E_L_E_T_ = ' ') "
	cSqlFrom +=                 " INNER JOIN " + RetSqlName("RD0") + " RD0 ON (RD0.RD0_CODIGO = NTE.NTE_CPART) "
	cSqlFrom +=                 					                 	     " AND (RD0.RD0_FILIAL = '" + xFilial("RD0") + "')"
	cSqlFrom +=                 					                 	     " AND (RD0.D_E_L_E_T_ = ' ') "
	cSqlFrom +=                 " INNER JOIN " + RetSqlName("NQS") + " NQS ON (NQS.NQS_COD = NTA.NTA_CTIPO) "
	cSqlFrom +=                 					                 	     " AND (NQS.NQS_FILIAL = '" + xFilial("NQS") + "')"
	cSqlFrom +=                 					                 	     " AND (NQS.D_E_L_E_T_ = ' ') "

	cSqlWhere := " WHERE NTA.NTA_FILIAL = '" + xFilial("NTA") + "'"
	cSqlWhere +=   " AND NTA.NTA_CAJURI = '" + cCajuri + "'"

	If lMax
		cSqlMax := " SELECT MAX(R_E_C_N_O_) R_E_C_N_O_ "
		cSqlMax += " FROM " + RetSqlName("NTA") + " NTA2 "
		cSqlMax += " WHERE NTA2.NTA_CAJURI = '" + cCajuri +"'"
		cSqlMax +=   " AND NTA2.NTA_FILIAL = '" + xFilial("NTA") + "'"

		cSqlWhere += " AND NTA.R_E_C_N_O_ = (" + cSqlMax + ")"
	EndIf

	cSql := cSqlSelect + cSqlFrom + cSqlWhere

	cSql := ChangeQuery(cSql)
	dbUseArea(.T., 'TOPCONN', TcGenQry( ,, cSql ) , cAlias, .T., .F.)

	While !(cAlias)->( EOF() )
		aAdd( aFollowup, { (cAlias)->NTA_COD, (cAlias)->NTA_DTFLWP, (cAlias)->NTA_HORA, (cAlias)->NTA_CRESUL, (cAlias)->NQS_DESC, (cAlias)->NQN_TIPO} )
		(cAlias)->( dbSkip() )
	End

	(cAlias)->( dbcloseArea() )

	RestArea(aArea)

Return aFollowup


//-------------------------------------------------------------------
/*/{Protheus.doc} getPar(cFollowup)
Retorna um array com os participantes respons�veis pelo Fup

@param cFollowup

@Return	aSigla  array de participantes respons�veis pelo FUP.

@author Beatriz Gomes
@since 20/04/17
@version 1.0
/*/
//-------------------------------------------------------------------
Function JWsLpGtNte(cFollowup)
	Local aSigla := {}
	Local cSQL   := ''
	Local aArea  := GetArea()
	Local cAlias := GetNextAlias()

	cSQL  := "SELECT RD0.RD0_CODIGO RD0_COD "
	cSQL  +=      " ,RD0.RD0_SIGLA  RD0_SIGLA "
	cSQL  +=      " ,RD0.RD0_NOME   RD0_NOME "
	cSQL  +=      " ,RD0.RD0_EMAIL  RD0_EMAIL "
	cSQL  +=      " ,RD0.RD0_FONE   RD0_FONE "
	cSQL  += " FROM "+ RetSqlname('NTE') +" NTE INNER JOIN " + RetSqlname('RD0') + " RD0 ON (NTE.NTE_CPART = RD0.RD0_CODIGO) "
	cSQL  +=                 					                 	         	            " AND (RD0.D_E_L_E_T_ = ' ') "
	cSQL  += " WHERE NTE.D_E_L_E_T_ = ' ' "
	cSQL  +=   " AND NTE.NTE_FILIAL = '" + xFilial('NTA') + "' "
	cSQL  +=   " AND NTE.NTE_CFLWP  = '" + cFollowup + "' "
	cSQL  +=   " AND RD0.RD0_FILIAL = '" + xFilial('RD0') + "' "

	cSQL := ChangeQuery(cSQL)
	dbUseArea(.T., 'TOPCONN', TcGenQry( ,, cSQL ) , cAlias, .T., .F.)

	While !(cAlias)->( EOF() )
		aAdd( aSigla, { (cAlias)->RD0_COD,(cAlias)->RD0_SIGLA,(cAlias)->RD0_NOME, (cAlias)->RD0_EMAIL,(cAlias)->RD0_FONE} )
		(cAlias)->( dbSkip() )
	End

	(cAlias)->( dbcloseArea() )

	RestArea(aArea)
Return aSigla


//-------------------------------------------------------------------
/*/{Protheus.doc} JWsLpGtNyb()
Retorna a lista de Nyb

@Return cListNyb - Lista dos tipos de assunto juridicos concatenados

@author Willian Kazahaya
@since 23/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function JWsLpGtNyb()
	Local cListNyb := ""
	Local cSql     := ""
	Local aArea    := GetArea()
	Local cAliasNyb:= GetNextAlias()

	cSql := "SELECT NYB_COD FROM " + RetSqlname("NYB") + " WHERE D_E_L_E_T_ = ' ' "

	cSQL := ChangeQuery(cSQL)

	dbUseArea(.T., 'TOPCONN', TcGenQry( ,, cSQL ) , cAliasNyb, .T., .F.)

	While !(cAliasNyb)->(Eof())
		cListNyb += "'" + AllTrim((cAliasNyb)->NYB_COD) + "',"
		(cAliasNyb)->( dbSkip() )
	End

	If !Empty(cListNyb)
		cListNyb := Left(cListNyb,Len(cListNyb)-1)
	EndIf

	(cAliasNyb)->( DbCloseArea() )
	RestArea(aArea)
Return cListNyb

//-------------------------------------------------------------------
/*/{Protheus.doc} JWsLpGtNuq(cCajuri, lOrigem)
Retorna a lista de Nuq

@param cCajuri - C�digo do assunto jur�dico

@Return cListNuq - Lista da inst�ncia do assunto juridico

@author Willian Kazahaya
@since 23/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function JWsLpGtNuq(cCajuri, lOrigem)
	Local aNuq      := {}
	Local cSql      := ""
	Local aArea     := GetArea()
	Local cAliasNuq := GetNextAlias()
	Default lOrigem := .F.

	cSQL  := " SELECT NUQ.NUQ_COD    NUQ_COD "
	cSQL  +=        ",NUQ.NUQ_INSATU NUQ_INSATU "
	cSQL  +=        ",NUQ.NUQ_INSTAN NUQ_INSTAN "
	cSQL  +=        ",NUQ.NUQ_NUMPRO NUQ_NUMPRO "
	cSQL  +=        ",NUQ.NUQ_CNATUR NUQ_CNATUR "
	cSQL  +=        ",NQ1.NQ1_DESC   NQ1_DESC "
	cSQL  +=        ",NUQ.NUQ_CMUNIC NUQ_CMUNIC "
	cSQL  +=        ",CC2.CC2_MUN    CC2_MUN "
	cSQL  +=        ",NUQ.NUQ_DTDIST NUQ_DTDIST "
	cSQL  +=        ",NQ6.NQ6_DESC   NQ6_DESC "
	cSQL  +=        ",NQC.NQC_DESC   NQC_DESC "
	cSQL  +=        ",NQE.NQE_DESC   NQE_DESC "
	cSQL  +=        ",NUQ.NUQ_CDECIS NUQ_CDECIS "
	cSQL  +=        ",NQQ.NQQ_DESC   NQQ_DESC "
	cSQL  +=        ",NUQ.NUQ_DTDECI NUQ_DTDECI "

	cSQL  += "FROM "+ RetSqlname('NUQ') +" NUQ INNER JOIN "+ RetSqlname('NQ1') +" NQ1 ON (NQ1.NQ1_COD    = NUQ.NUQ_CNATUR) AND (NQ1.NQ1_FILIAL = '" + xFilial("NQ1") + "') "
	cSQL  +=                                                                       " AND (NQ1.D_E_L_E_T_ = ' ' ) "
	cSQL  +=                                  "LEFT  JOIN "+ RetSqlname('CC2') +" CC2 ON (CC2.CC2_CODMUN = NUQ.NUQ_CMUNIC) AND (CC2.CC2_FILIAL = '" + xFilial("CC2") + "') "
	cSQL  +=                                                                       " AND (CC2.D_E_L_E_T_ = ' ' ) "
	cSQL  +=                                  "LEFT  JOIN "+ RetSqlname('NQ6') +" NQ6 ON (NQ6.NQ6_COD    = NUQ.NUQ_CCOMAR) AND (NQ6.NQ6_FILIAL = '" + xFilial("NQ6") + "') "
	cSQL  +=                                                                       " AND (NQ6.D_E_L_E_T_ = ' ' ) "
	cSQL  +=                                  "LEFT  JOIN "+ RetSqlname('NQC') +" NQC ON (NQC.NQC_COD    = NUQ.NUQ_CLOC2N) AND (NQC.NQC_CCOMAR = NQ6.NQ6_COD) AND (NQC.NQC_FILIAL = '" + xFilial("NQC") + "') "
	cSQL  +=                                                                       " AND (NQC.D_E_L_E_T_ = ' ' ) "
	cSQL  +=                                  "LEFT  JOIN "+ RetSqlname('NQE') +" NQE ON (NQE.NQE_COD    = NUQ.NUQ_CLOC3N) AND (NQE.NQE_CLOC2N = NQC.NQC_COD) AND (NQE.NQE_FILIAL = '" + xFilial("NQE") + "') "
	cSQL  +=                                                                       " AND (NQE.D_E_L_E_T_ = ' ' ) "
	cSQL  +=                                  "LEFT  JOIN "+ RetSqlname('NQQ') +" NQQ ON (NQQ.NQQ_COD    = NUQ.NUQ_CDECIS) AND (NQQ.NQQ_FILIAL = '" + xFilial("NUQ") + "') "
	cSQL  +=                                                                       " AND (NQQ.D_E_L_E_T_ = ' ' ) "
	cSQL  += "WHERE NUQ.NUQ_FILIAL = '" + xFilial("NUQ") + "' "
	cSQL  +=  " AND NUQ.NUQ_CAJURI = '" + cCajuri + "' "
	cSQL  +=  " AND NUQ.D_E_L_E_T_  = ' ' "

	If lOrigem
		cSQL += " AND NUQ.NUQ_INSATU = '1' "
	EndIf

	cSQL := ChangeQuery(cSQL)

	dbUseArea(.T., 'TOPCONN', TcGenQry( ,, cSQL ) , cAliasNuq, .T., .F.)

	While !(cAliasNuq)->(Eof())
		aAdd( aNuq, { (cAliasNuq)->NUQ_COD,(cAliasNuq)->NUQ_INSATU,(cAliasNuq)->NUQ_INSTAN, (cAliasNuq)->NUQ_NUMPRO,(cAliasNuq)->NUQ_CNATUR,;
			(cAliasNuq)->NQ1_DESC, (cAliasNuq)->NUQ_CMUNIC, (cAliasNuq)->CC2_MUN, (cAliasNuq)->NUQ_DTDIST,(cAliasNuq)->NQ6_DESC,;
			(cAliasNuq)->NQC_DESC, (cAliasNuq)->NQE_DESC, (cAliasNuq)->NUQ_CDECIS, (cAliasNuq)->NQQ_DESC, (cAliasNuq)->NUQ_DTDECI} )

		(cAliasNuq)->( dbSkip() )
	End

	(cAliasNuq)->( DbCloseArea() )
	RestArea(aArea)
Return aNuq

//-------------------------------------------------------------------
/*/{Protheus.doc} JwsLpGtNt9(cCajuri, cTipoEnt)
Retorna a lista de NT9

@param cCajuri - C�digo do assunto jur�dico
@param cTipoEnt - Numero do Polo

@Return aNt9 - Lista do Envolvido

@author Willian Kazahaya
@since 23/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function JWsLpGtNt9(cCajuri, cTipoEnt)
	Local aNt9       := {}
	Local cSql       := ""
	Local aArea      := GetArea()
	Local cAliasNt9  := GetNextAlias()
	Local cNT9Codigo := ""
	Local cNT9Loja   := ""
	Local cNT9Entida := ""

	Default cTipoEnt := 0

	cSQL  := " SELECT  NT9.NT9_COD    NT9_COD "
	cSQL  +=        ", NT9.NT9_CODENT NT9_CODENT "
	cSQL  +=        ", NT9.NT9_ENTIDA NT9_ENTIDA "
	cSQL  +=        ", NT9.NT9_PRINCI NT9_PRINCI "
	cSQL  +=        ", NT9.NT9_NOME   NT9_NOME "
	cSQL  +=        ", NT9.NT9_CEMPCL NT9_CEMPCL "
	cSQL  +=        ", NT9.NT9_LOJACL NT9_LOJACL "
	cSQL  +=        ", NT9.NT9_CFORNE NT9_CFORNE "
	cSQL  +=        ", NT9.NT9_LFORNE NT9_LFORNE "
	cSQL  +=        ", NT9.NT9_CTPENV NT9_CTPENV "
	cSQL  +=        ", NQA.NQA_DESC   NQA_DESC "
	cSQL  +=        ", NT9.NT9_ENDECL NT9_ENDECL "
	cSQL  +=        ", NT9.NT9_CCRGDP NT9_CCRGDP "
	cSQL  +=        ", SQ3.Q3_DESCSUM Q3_DESCSUM "
	cSQL  +=        ", NT9.NT9_TIPOEN NT9_TIPOEN "
	cSQL  += " FROM "+ RetSqlname('NT9') +" NT9 INNER JOIN "+ RetSqlname('NQA') +" NQA ON (NQA.NQA_COD = NT9.NT9_CTPENV) "
	cSQL  +=                                                                        " AND (NQA.NQA_FILIAL = '" + xFilial("NQA") + "') "
	cSQL  +=                                                                        " AND (NQA.D_E_L_E_T_ = ' ') "
	cSQL  +=                                  " LEFT  JOIN "+ RetSqlName('SQ3') +" SQ3 ON (SQ3.Q3_CARGO = NT9.NT9_CCRGDP) "
	cSQL  +=                                                                        " AND (SQ3.Q3_FILIAL = '" + xFilial("SQ3") + "') "
	cSQL  +=                                                                        " AND (SQ3.D_E_L_E_T_ = ' ') "
	cSQL  += " WHERE NT9.NT9_CAJURI = '" + cCajuri + "'"
	cSQL  +=   " AND NT9.NT9_FILIAL = '" + xFilial("NT9") + "' "
	cSQL  +=   " AND NT9.D_E_L_E_T_ = ' ' "

	Do Case
	Case cTipoEnt == 1 // Polo Ativo
		cSQL += "AND NQA.NQA_POLOAT =  '1' "
	Case cTipoEnt == 2 // Polo Passivo
		cSQL += "AND NQA.NQA_POLOPA =  '1' "
	Case cTipoEnt == 3 // Terceiro Interessado
		cSQL += "AND NQA.NQA_TERCIN =  '1' "
	Case cTipoEnt == 4 // Sociedade Envolvida
		cSQL += "AND NQA.NQA_SOCIED =  '1' "
	Case cTipoEnt == 5 // Participa��o Societ�ria
		cSQL += "AND NQA.NQA_PARTIC =  '1' "
	Case cTipoEnt == 6 // Administra��o
		cSQL += "AND NQA.NQA_ADMINI =  '1' "
	End Case

	cSQL := ChangeQuery(cSQL)

	dbUseArea(.T., 'TOPCONN', TcGenQry( ,, cSQL ) , cAliasNt9, .T., .F.)

	While !(cAliasNt9)->(Eof())
		cNT9Entida := (cAliasNt9)->NT9_ENTIDA

		Do Case
		Case cNT9Entida == "SA1"
			cNT9Codigo := (cAliasNt9)->NT9_CEMPCL
			cNT9Loja   := (cAliasNt9)->NT9_LOJACL

		Case cNT9Entida == "SA2"
			cNT9Codigo := (cAliasNt9)->NT9_CFORNE
			cNT9Loja   := (cAliasNt9)->NT9_LFORNE
		End Case


		aAdd( aNt9, {(cAliasNt9)->NT9_COD,;
			(cAliasNt9)->NT9_CODENT,;
			(cAliasNt9)->NT9_ENTIDA,;
			(cAliasNt9)->NT9_PRINCI,;
			(cAliasNt9)->NT9_NOME,;
			cNT9Codigo + '-' + cNT9Loja,;
			(cAliasNt9)->NT9_CTPENV,;
			(cAliasNt9)->NQA_DESC,;
			(cAliasNt9)->NT9_CCRGDP,;
			(cAliasNt9)->Q3_DESCSUM,;
			(cAliasNt9)->NT9_TIPOEN } )

		(cAliasNt9)->( dbSkip() )
	End

	(cAliasNt9)->( DbCloseArea() )
	RestArea(aArea)

Return aNt9


//-------------------------------------------------------------------
/*/{Protheus.doc} JWsLpGtNsy(cCajuri)
Retorna a lista de NSY

@param cCajuri - C�digo do assunto jur�dico

@Return aNsy - Lista do Objetos

@author Willian Kazahaya
@since 23/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function JWsLpGtNsy(cCajuri)
	Local aNsy       := {}
	Local cSql       := ""
	Local aArea      := GetArea()
	Local cAliasNsy  := GetNextAlias()

	cSQL  := " SELECT NSY.NSY_COD	    NSY_COD "
	cSQL  +=        ",NSY.NSY_CAJURI    NSY_CAJURI "
	cSQL  +=        ",CAST(NSY.NSY_DESC AS VARCHAR(4000)) NSY_DESC "
	cSQL  +=        ",NSY.NSY_CPROG     NSY_CPROG "
	cSQL  +=        ",NQ7.NQ7_DESC      NQ7_DESC "
	cSQL  +=        ",NQ7.NQ7_PORCEN    NQ7_PORCEN "
	cSQL  +=        ",NSY.NSY_PEVLR     NSY_PEVLR "
	cSQL  +=        ",NSY.NSY_CMOPED    NSY_CMOPED "
	cSQL  +=        ",CTO.CTO_DESC      CTO_DESC "
	cSQL  +=        ",CTO.CTO_SIMB      CTO_SIMB "
	cSQL  += " FROM "+ RetSqlname('NSY') +" NSY LEFT JOIN "+ RetSqlname('NQ7') +" NQ7 ON (NQ7.NQ7_COD = NSY.NSY_CPROG) "
	cSQL  +=                                                                       " AND (NQ7.NQ7_FILIAL = '" + xFilial("NQ7") + "') "
	cSQL  +=                                                                       " AND (NQ7.D_E_L_E_T_ = ' ') "
	cSQL  +=                                  " LEFT JOIN "+ RetSqlname('CTO') +" CTO ON (CTO.CTO_MOEDA  = NSY.NSY_CMOPED) "
	cSQL  +=                                                                       " AND (CTO.CTO_FILIAL = '" + xFilial("CTO") + "') "
	cSQL  +=                                                                       " AND (CTO.D_E_L_E_T_ = ' ') "
	cSQL  += " WHERE NSY.NSY_CAJURI = '" + cCajuri + "' "
	cSQL  +=   " AND NSY.NSY_FILIAL = '" +xFilial("NSY")+ "' "
	cSQL  +=   " AND NSY.D_E_L_E_T_ = ' ' "
	cSQL  := ChangeQuery(cSQL)

	dbUseArea(.T., 'TOPCONN', TcGenQry( ,, cSQL ) , cAliasNsy, .T., .F.)

	While !(cAliasNsy)->(Eof())
		aAdd( aNsy, {(cAliasNsy)->NSY_COD;
			, (cAliasNsy)->NSY_CAJURI;
			, (cAliasNsy)->NSY_DESC;
			, (cAliasNsy)->NSY_CPROG;
			, (cAliasNsy)->NQ7_DESC;
			, (cAliasNsy)->NQ7_PORCEN;
			, (cAliasNsy)->NSY_PEVLR;
			, (cAliasNsy)->NSY_CMOPED;
			, (cAliasNsy)->CTO_DESC;
			, (cAliasNsy)->CTO_SIMB})

		(cAliasNsy)->( dbSkip() )
	End

	(cAliasNsy)->( dbCloseArea() )
	RestArea(aArea)
Return aNsy

//-------------------------------------------------------------------
/*/{Protheus.doc} JWsLpGtNt3(cCajuri)
Retorna a lista de NT3

@param cCajuri - C�digo do assunto jur�dico

@Return aNt3 - Lista de Despesas

@author Leandro Silva
@since 26/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function JWsLpGtNt3(cCajuri)
	Local aNt3         := {}
	Local cSql         := ""
	Local aArea        := GetArea()
	Local cAliasNt3    := GetNextAlias()

	cSQL  := " SELECT NT3.NT3_COD      NT3_COD    "
	cSQL  +=        ",NT3.NT3_CTPDES   NT3_CTPDES "
	cSQL  +=        ",NSR.NSR_DESC     NSR_DESC   "
	cSQL  +=        ",CAST(NT3.NT3_DESC AS VARCHAR(4000)) NT3_DESC   "
	cSQL  +=        ",NT3.NT3_DATA     NT3_DATA   "
	cSQL  +=        ",NT3.NT3_VALOR    NT3_VALOR  "
	cSQL  +=        ",NT3.NT3_CMOEDA   NT3_CMOEDA "
	cSQL  +=        ",CTO.CTO_DESC     CTO_DESC   "
	cSQL  +=        ",CTO.CTO_SIMB     CTO_SIMB   "
	cSQL  += " FROM "+ RetSqlname('NT3') +" NT3 LEFT JOIN "+ RetSqlname('NSR') +" NSR ON (NSR.NSR_COD = NT3.NT3_CTPDES) "
	cSQL  +=                                                                       " AND (NSR.NSR_FILIAL = '" + xFilial("NSR") + "') "
	cSQL  +=                                  " LEFT JOIN "+ RetSqlname('CTO') +" CTO ON (CTO.CTO_MOEDA  = NT3.NT3_CMOEDA) "
	cSQL  +=                                                                       " AND (CTO.CTO_FILIAL = '" + xFilial("CTO") + "') "
	cSQL  += " WHERE NT3.NT3_CAJURI = '" + cCajuri + "' "
	cSQL  +=   " AND NT3.NT3_FILIAL = '" +xFilial("NT3")+ "' "
	cSQL  +=   " AND NT3.D_E_L_E_T_ = ' ' "

	cSQL := ChangeQuery(cSQL)

	dbUseArea(.T., 'TOPCONN', TcGenQry( ,, cSQL ) , cAliasNt3, .T., .F.)

	While !(cAliasNt3)->(Eof())
		aAdd( aNt3, {(cAliasNt3)->NT3_COD;
			,(cAliasNt3)->NT3_CTPDES;
			,(cAliasNt3)->NSR_DESC;
			,(cAliasNt3)->NT3_DESC;
			,(cAliasNt3)->NT3_DATA;
			,(cAliasNt3)->NT3_VALOR;
			,(cAliasNt3)->NT3_CMOEDA;
			,(cAliasNt3)->CTO_DESC;
			,(cAliasNt3)->CTO_SIMB})

		(cAliasNt3)->( dbSkip() )
	End

	(cAliasNt3)->( DbCloseArea() )
	RestArea(aArea)
Return aNt3


//-------------------------------------------------------------------
/*/{Protheus.doc} JWsLpGtNt2(cCajuri)
Retorna a lista de NT2

@param cCajuri - C�digo do assunto jur�dico

@author Marcelo Araujo Dente
@since 23/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function JWsLpGtNt2(cCajuri)
	Local aNt2      := {}
	Local cSql      := ""
	Local aArea     := GetArea()
	Local cAliasNt2 := GetNextAlias()

	cSQL  := "SELECT NT2.NT2_COD   NT2_COD "
	cSQL  += 		 ",NQW.NQW_DESC  NQW_DESC " 	                     //identifier: Identificador do tipo de garantia (G, A, etc).
	cSQL  +=		 ",CAST(NT2.NT2_DESC AS VARCHAR(4000)) NT2_DESC " 	//description: Descri��o da garantia.
	cSQL  +=		 ",NT2.NT2_DATA  NT2_DATA "  						 	//date: Data da garantia.
	cSQL  +=		 ",NT2.NT2_VALOR NT2_VALOR " 							//value: Valor da garantia.
	cSQL  += "FROM " + RetSqlname("NT2") + " NT2 LEFT JOIN "+ RetSqlname('NQW') +" NQW ON (NQW.NQW_COD = NT2.NT2_CTPGAR) "
	cSQL  +=                                                                        " AND (NQW.NQW_FILIAL = '" + xFilial("NQW") + "') "
	cSQL  +=                                                                        " AND (NQW.D_E_L_E_T_ = ' ') "
	cSQL  += "WHERE NT2.NT2_FILIAL = '" + xFilial("NT2") + "' "
	cSQL  +=  		"AND NT2.NT2_CAJURI = '" + cCajuri + "' "
	cSQL  +=  		"AND NT2.D_E_L_E_T_ = ' ' "

	cSQL := ChangeQuery(cSQL)

	dbUseArea(.T., 'TOPCONN', TcGenQry( ,, cSQL ) , cAliasNt2, .T., .F.)

	While !(cAliasNt2)->(Eof())
		aAdd( aNt2, { (cAliasNt2)->NT2_COD;
			,(cAliasNt2)->NQW_DESC;
			,(cAliasNt2)->NT2_DESC;
			,(cAliasNt2)->NT2_DATA;
			,(cAliasNt2)->NT2_VALOR} )
		(cAliasNt2)->( dbSkip() )
	End

	(cAliasNt2)->( DbCloseArea() )

	RestArea(aArea)
Return aNt2

//-------------------------------------------------------------------
/*/{Protheus.doc} JWsLpGtNt4(cCajuri, nNroTipo)
Retorna a lista de NT4 ( Tipo Decis�o)

@param cCajuri - C�digo do assunto jur�dico


@author Marcelo Araujo Dente
@since 23/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function JWsLpGtNt4(cCajuri, nNroTipo)
	Local aNt4      := {}
	Local cSql      := ""
	Local aArea     := GetArea()
	Local cAliasNt4 := GetNextAlias()

	Default nNroTipo := 0

	cSql  := "SELECT NT4.NT4_COD "
	cSql  +=       ",NRO.NRO_DESC NRO_DESC "
	cSql  +=       ",NQG.NQG_DESC NQG_DESC "
	cSql  +=       ",NT4.NT4_DTANDA NT4_DTANDA "
	cSql  +=       ",CAST(NT4.NT4_DESC AS VARCHAR(4000)) NT4_DESC "
	cSQL  +=       ",NUQ.NUQ_INSTAN NUQ_INSTAN "
	cSQL  += " FROM " + RetSqlName('NT4') + " NT4 LEFT JOIN " + RetSqlName('NRO') + " NRO ON (NRO.NRO_COD = NT4.NT4_CATO) "
	cSQL  +=                                                                           " AND (NRO.NRO_FILIAL = '"  + xFilial('NRO') + "') "
	cSQL  +=                                                                           " AND (NRO.D_E_L_E_T_ = ' ') "
	cSQL  +=                                    " LEFT JOIN " + RetSqlName('NQG') + " NQG ON (NQG.NQG_COD = NT4.NT4_CFASE) "
	cSQL  +=                                                                           " AND (NQG.NQG_FILIAL = '"  + xFilial('NQG') + "') "
	cSQL  +=                                                                           " AND (NQG.D_E_L_E_T_ = ' ') "
	cSQL  +=                                    " LEFT JOIN " + RetSqlName('NQQ') + " NQQ ON (NQQ.NQQ_COD = NRO.NRO_COD) "
	cSQL  +=                                                                           " AND (NQQ.NQQ_FILIAL = '"  + xFilial('NQQ') + "') "
	cSQL  +=                                                                           " AND (NQQ.D_E_L_E_T_ = ' ') "
	cSQL  +=                                    " LEFT JOIN " + RetSqlName('NUQ') + " NUQ ON (NUQ.NUQ_COD = NT4.NT4_CINSTA) "
	cSQL  +=                                                                           " AND (NUQ.NUQ_CAJURI = NT4.NT4_CAJURI) "
	cSQL  +=                                                                           " AND (NUQ.NUQ_FILIAL = '"+ xFilial('NUQ') + "') "
	cSQL  +=                                                                           " AND (NUQ.D_E_L_E_T_ = ' ') "
	cSQL  += "WHERE NT4.NT4_FILIAL = '" + xFilial("NT4") + "' "
	cSQL  +=  " AND NT4.NT4_CAJURI = '" + cCajuri + "' "
	cSQL  +=  " AND NT4.D_E_L_E_T_ = ' ' "

	If nNroTipo > 0
		cSQL += " AND NRO.NRO_TIPO = '" + cValToChar(nNroTipo) + "'"
	EndIf

	cSQL := ChangeQuery(cSQL)

	dbUseArea(.T., 'TOPCONN', TcGenQry( ,, cSQL ) , cAliasNt4, .T., .F.)

	While !(cAliasNt4)->(Eof())
		aAdd( aNt4, { (cAliasNt4)->NT4_COD;
			,(cAliasNt4)->NRO_DESC;
			,(cAliasNt4)->NT4_DTANDA;
			,(cAliasNt4)->NT4_DESC;
			,(cAliasNt4)->NUQ_INSTAN;
			,(cAliasNt4)->NQG_DESC} )
		(cAliasNt4)->( dbSkip() )
	End

	(cAliasNt4)->( DbCloseArea() )
	RestArea(aArea)
Return aNt4

//-------------------------------------------------------------------
/*/{Protheus.doc} JWsLpGtEnc(cCajuri)
Retorna a lista de Encerramento

@param cCajuri - C�digo do assunto jur�dico

@Return aEnc - lista de Encerramento

@author Leandro Silva
@since 26/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function JWsLpGtEnc(cCajuri)
	Local aNszEnc   := {}
	Local cSql      := ""
	Local aArea     := GetArea()
	Local cAliasEnc := GetNextAlias()

	cSQL  := " SELECT NSZ.NSZ_COD      NSZ_COD    "
	cSQL  +=         ",NSZ.NSZ_SITUAC  NSZ_SITUAC "
	cSQL  +=         ",NSZ.NSZ_CMOENC  NSZ_CMOENC "
	cSQL  +=         ",CAST(NQI.NQI_DESC AS VARCHAR(4000))     NQI_DESC "
	cSQL  +=         ",NSZ.NSZ_DTENCE  NSZ_DTENCE "
	cSQL  +=         ",NSZ.NSZ_VLFINA  NSZ_VLFINA "
	cSQL  +=         ",CAST(NSZ.NSZ_DETENC AS VARCHAR(4000))   NSZ_DETENC "

	cSQL  += " FROM "+ RetSqlname('NSZ') +" NSZ LEFT JOIN "+ RetSqlname('NQI') +" NQI ON (NQI.NQI_COD = NSZ.NSZ_CMOENC) "
	cSQL  +=                                                                       " AND (NQI.NQI_FILIAL = '" + xFilial("NQI") + "') "
	cSQL  +=                                                                       " AND (NQI.D_E_L_E_T_ = ' ') "
	cSQL  += " WHERE NSZ.NSZ_COD = '" + cCajuri + "' "
	cSQL  +=   " AND NSZ.NSZ_FILIAL = '" +xFilial("NSZ")+ "' "
	cSQL +=    " AND NSZ.D_E_L_E_T_ = ' ' "

	cSQL := ChangeQuery(cSQL)

	dbUseArea(.T., 'TOPCONN', TcGenQry( ,, cSQL ) , cAliasEnc, .T., .F.)

	While !(cAliasEnc)->(Eof())
		aAdd( aNszEnc, {(cAliasEnc)->NSZ_COD;
			, (cAliasEnc)->NSZ_SITUAC;
			, (cAliasEnc)->NSZ_CMOENC;
			, (cAliasEnc)->NQI_DESC;
			, (cAliasEnc)->NSZ_DTENCE;
			, (cAliasEnc)->NSZ_VLFINA;
			, (cAliasEnc)->NSZ_DETENC})
		(cAliasEnc)->( dbSkip() )
	End
	(cAliasEnc)->( DbCloseArea() )
	RestArea(aArea)
Return aNszEnc

//-------------------------------------------------------------------
/*/{Protheus.doc} JGetInsDes(nInsta)
Retorna a descri��o da Inst�ncia

@param nInsta - Numero da Inst�ncia

@Return cDesInsta - Descri��o da inst�ncia

@author Willian Kazahaya
@since 23/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function JWsLpArea()
	Local cSqlSelect, cSqlFrom, cSqlWhere, cSqlQuery := ""
	Local oResponse  := JSonObject():New()
	Local cAliasNrb  := GetNextAlias()
	Local aArea 	   := GetArea()
	Local nIndexJSon := 0
	Local aNrb       := {}
	Local nCount     := 0

	cSqlSelect := " SELECT NRB.NRB_COD   NRB_COD "
	cSqlSelect +=       " ,NRB.NRB_DESC  NRB_DESC "
	cSqlSelect +=       " ,NRB.NRB_ATIVO NRB_ATIVO "

	cSqlFrom   := " FROM " + RetSqlname('NRB') + " NRB "

	cSqlWhere  := " WHERE NRB.NRB_ATIVO = '1' "

	cSqlQuery  := "" + cSqlSelect + cSqlFrom + cSqlWhere
	dbUseArea(.T., 'TOPCONN', TcGenQry( ,, cSqlQuery ) , cAliasNrb, .T., .F.)

	oResponse['areas'] := {}

	While !(cAliasNrb)->(Eof())
		nIndexJSon++
		Aadd(oResponse['areas'], JsonObject():New())

		oResponse['areas'][nIndexJSon]['id'] := JConvUTF8((cAliasNrb)->NRB_COD)
		oResponse['areas'][nIndexJSon]['description'] := JConvUTF8((cAliasNrb)->NRB_DESC)

		aNrb := JWsLpGtNrl((cAliasNrb)->NRB_COD)
		oResponse['areas'][nIndexJSon]['subarea'] := {}

		If !Empty(aNrb)
			For nCount := 1 to Len(aNrb)
				Aadd(oResponse['areas'][nIndexJSon]['subarea'], JsonObject():New())
				aTail(oResponse['areas'][nIndexJSon]['subarea'])['idSub']          := JConvUTF8(aNrb[nCount][1])
				aTail(oResponse['areas'][nIndexJSon]['subarea'])['descriptionSub'] := JConvUTF8(aNrb[nCount][2])
			Next
		EndIf

		(cAliasNrb)->( dbSkip())
	End

	(cAliasNrb)->( DbCloseArea() )
	RestArea(aArea)
Return oResponse

//-------------------------------------------------------------------
/*/{Protheus.doc} JWsLpGtNrl(codArea)
Retorna array com as informa��es da subarea

@param codArea - C�digo da �rea

@Return cDesInsta - Descri��o da inst�ncia

@author Willian Kazahaya
@since 23/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function JWsLpGtNrl(codArea)
	Local aNrl     := {}
	Local cSql     := ""
	Local aArea    := GetArea()
	Local cAliasNrl:= GetNextAlias()

	cSql := " SELECT NRL_COD "
	cSql +=       " ,NRL_DESC "

	cSql += " FROM " + RetSqlname("NRL") + " NRL "
	cSql += " WHERE NRL.D_E_L_E_T_ = ' ' "
	cSql +=   " AND NRL.NRL_CAREA = '" + AllTrim(codArea) + "'"
	cSql +=   " AND NRL.NRL_ATIVO = '1' "

	cSql := ChangeQuery(cSql)

	dbUseArea(.T., 'TOPCONN', TcGenQry( ,, cSql ) , cAliasNrl, .T., .F.)

	While !(cAliasNrl)->(Eof())
		aAdd( aNrl, {(cAliasNrl)->NRL_COD;
			,(cAliasNrl)->NRL_DESC})

		(cAliasNrl)->( dbSkip() )
	End

	(cAliasNrl)->( DbCloseArea() )
	RestArea(aArea)
Return aNrl

//-------------------------------------------------------------------
/*/{Protheus.doc} JWsListFup(codProc)
Retorna Follow-ups de um Assunto Jur�dico ( Processo )

@param codProc - C�digo do Processo

@Return oFups - Follow-ups

@author Marcelo Araujo Dente
@since 16/11/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function JWsListFup(cCodProc,oResponse,nIndexJSon,lFupMax)
	Local aNta := JWsLpGtNta(cCodProc,lFupMax)
	Local nCount
	Local cFollowup
	Local aNte
	Local nCountAux
	Local cResp
	Local oRespFup  := JsonObject():New()

	oRespFup := {}
	If !Empty(aNta)
		For nCount := 1 to Len(aNta)
			Aadd(oRespFup, JsonObject():New())
			cFollowup := aNta[nCount][1]

			oRespFup[nCount]['id']          := cFollowup
			oRespFup[nCount]['date']        := JConvUTF8(aNta[nCount][2])
			oRespFup[nCount]['hour']        := SubStr(AllTrim(aNta[nCount][3]),1,2) + ":" + SubStr(AllTrim(aNta[nCount][3]), 3, 2)
			oRespFup[nCount]['status']      := JConvUTF8(aNta[nCount][4])
			oRespFup[nCount]['title']       := JConvUTF8(aNta[nCount][5])
			oRespFup[nCount]['tipFup']      := JConvUTF8(aNta[nCount][6])
			oRespFup[nCount]['tipFupDesc']  := JGetNqnDes(aNta[nCount][6])

			aNte := JWsLpGtNte(cFollowup)

			oRespFup[nCount]['responsable'] := {}

			If !Empty(aNte)
				nCountAux := 0
				for nCountAux := 1 to Len(aNte)
					cResp := aNte[nCountAux][1]
					Aadd(oRespFup[nCount]['responsable'], JsonObject():New())
					aTail(oRespFup[nCount]['responsable'])['id']      := cResp
					aTail(oRespFup[nCount]['responsable'])['acronym'] := JConvUTF8(aNte[nCountAux][2])
					aTail(oRespFup[nCount]['responsable'])['name']    := JConvUTF8(aNte[nCountAux][3])
					aTail(oRespFup[nCount]['responsable'])['email']   := JConvUTF8(aNte[nCountAux][4])
					aTail(oRespFup[nCount]['responsable'])['fone']    := JConvUTF8(aNte[nCountAux][5])
				Next
			EndIf
		Next
	EndIf
Return oRespFup

//-------------------------------------------------------------------
/*/{Protheus.doc} STATIC FUNCTION GetStkEvo()
Traz o total de casos Em andamento e a movimenta��o (Novos e Encerrados) m�s a m�s

@param cTpAssJur  - Qual � o tipo de assunto jur�dico que deseja filtrar?

@return  oResponse, Json, Objeto Json contendo o consolidado de casos Novos, Encerrados e Em Andamento nos ultimos 12 m�ses.
@since 26/06/2019
@version 1.0

/*/
//-------------------------------------------------------------------
Static Function GetStkEvo(cTpAssJur)
Local dData      := Date()
Local dDataIni   := FirstDate( MonthSub( dData , 11 ) )
Local cQry       := ""
Local cFrom      := ""
Local cWhere     := ""
Local aArea      := GetArea()
Local cAlias     := GetNextAlias()
Local aSQLRestri := Ja162RstUs(,,,.T.)
Local cTpAJ      := JWsLpGtNyb()
Local nQtdAnd    := 0
Local aMov       := {}
Local nI         := 0
Local nJ         := 0
Local oResponse  := JsonObject():New()
Local cNSZName   := Alltrim(RetSqlName("NSZ"))
Local aFilUsr    := JURFILUSR( __CUSERID, "NSZ" )
Local cAssJurGrp := ""

Default cTpAssJur := ''

	//Busca o total de casos em andamento hoje
	cQry   := " SELECT COUNT(1) AS QTD "
	cFrom  := " FROM "+ cNSZName + " NSZ INNER JOIN " + RetSqlName("NUQ") + " NUQ ON (NUQ.NUQ_CAJURI = NSZ_COD "
	cFrom  +=                                                                   " AND NUQ.NUQ_FILIAL = NSZ.NSZ_FILIAL "
	cFrom  +=                                                                   " AND NUQ.D_E_L_E_T_ = ' ') "
	cWhere := " WHERE NSZ.D_E_L_E_T_ = ' ' "
	cWhere +=   " AND NSZ.NSZ_TIPOAS IN (" + cTpAJ + ") "
	cWhere +=   " AND (NSZ.NSZ_DTINCL < '" + DToS(dDataIni) + "'"
	cWhere +=   " AND (NSZ.NSZ_SITUAC = '1' OR NSZ.NSZ_DTENCE > '" + DToS(dDataIni) + "'))"
	cWhere +=   " AND NUQ.NUQ_INSATU = '1' "

	// Filtar Filiais que o usu�rio possui acesso
	If ( VerSenha(114) .or. VerSenha(115) )
		cWhere += " AND NSZ.NSZ_FILIAL IN " + FORMATIN(aFilUsr[1],aFilUsr[2])
	Else
		cWhere += " AND NSZ.NSZ_FILIAL = '"+xFilial("NSZ")+"'"
	Endif
	
	cAssJurGrp := JurTpAsJr(__CUSERID)
	If !Empty(cTpAssJur) .And. cTpAssJur $ cAssJurGrp
		cWhere += " AND NSZ.NSZ_TIPOAS IN " + FormatIn(cTpAssJur, ",")
	Else
		cWhere += " AND NSZ.NSZ_TIPOAS IN (" + cAssJurGrp + ") "
	EndIf

	If !Empty(aSQLRestri)
		cWhere +=   " AND ("+Ja162SQLRt(aSQLRestri, , , , , , , , , cTpAJ)+")"
	EndIf

	cQry := ChangeQuery(cQry+cFrom+cWhere)
	dbUseArea(.T., 'TOPCONN', TcGenQry( ,, cQry ) , cAlias, .T., .F.)

	While !(cAlias)->(Eof())
		nQtdAnd := (cAlias)->QTD
		(cAlias)->( dbSkip() )
	End

	(cAlias)->( dbcloseArea() )
	cAlias     := GetNextAlias()

	// Busca o consolidado m�s a m�s dos casos novos e encerrados
	cQry   := "SELECT COALESCE(NOVOS.ANOMES, ENCERRADOS.ANOMES) MES, "
	cQry   +=        "COALESCE(NOVOS.QTD,0) AS NOVO, "
	cQry   +=        "COALESCE(ENCERRADOS.QTD,0) AS ENCERRADO "

	cFrom  :=   "FROM (SELECT COUNT(1) AS QTD , "
	cFrom  +=               " CONCAT(SUBSTRING(NSZA.NSZ_DTINCL,1,6),'01') AS ANOMES "
	cFrom  +=          " FROM "+ cNSZName + " NSZA INNER JOIN " + RetSqlName("NUQ") + " NUQA ON (NUQA.NUQ_CAJURI = NSZA.NSZ_COD "
	cFrom  +=                                                                              " AND NUQA.NUQ_FILIAL = NSZA.NSZ_FILIAL "
	cFrom  +=                                                                              " AND NUQA.D_E_L_E_T_ = ' ')"
	cFrom  +=         " WHERE NSZA.D_E_L_E_T_ = ' ' "
	cFrom  +=           " AND NUQA.NUQ_INSATU = '1' "

	// Filtar Filiais que o usu�rio possui acesso
	If ( VerSenha(114) .or. VerSenha(115) )
		cFrom += " AND NSZA.NSZ_FILIAL IN " + FORMATIN(aFilUsr[1],aFilUsr[2])
	Else
		cFrom += " AND NSZA.NSZ_FILIAL = '"+xFilial("NSZ")+"'"
	Endif

	If !Empty(cTpAssJur) .And. cTpAssJur $ cAssJurGrp
		cFrom += " AND NSZA.NSZ_TIPOAS IN " + FormatIn(cTpAssJur, ",")
	Else
		cFrom += " AND NSZA.NSZ_TIPOAS IN (" + cAssJurGrp + ") "
	EndIf

	If !Empty(aSQLRestri)
		cFrom +=        " AND ("+Ja162SQLRt(aSQLRestri, , , , , , , , , cTpAJ)+")"
	EndIf

	cFrom  +=        " GROUP BY CONCAT(SUBSTRING(NSZA.NSZ_DTINCL,1,6),'01')) NOVOS "

	cFrom  += " FULL JOIN (SELECT COUNT(1) AS QTD , "
	cFrom  +=                   " CONCAT(SUBSTRING(NSZB.NSZ_DTENCE,1,6),'01') AS ANOMES "
	cFrom  +=               "FROM "+ cNSZName + " NSZB INNER JOIN " + RetSqlName("NUQ") + " NUQB ON (NUQB.NUQ_CAJURI = NSZB.NSZ_COD "
	cFrom  +=                                                                              " AND NUQB.NUQ_FILIAL = NSZB.NSZ_FILIAL "
	cFrom  +=                                                                              " AND NUQB.D_E_L_E_T_ = ' ')"
	cFrom  +=             " WHERE NSZB.NSZ_SITUAC = '2' "
	cFrom  +=               " AND NUQB.NUQ_INSATU = '1' "
	cFrom  +=               " AND NSZB.D_E_L_E_T_ = ' ' "

	// Filtar Filiais que o usu�rio possui acesso
	If ( VerSenha(114) .or. VerSenha(115) )
		cFrom += " AND NSZB.NSZ_FILIAL IN " + FORMATIN(aFilUsr[1],aFilUsr[2])
	Else
		cFrom += " AND NSZB.NSZ_FILIAL = '"+xFilial("NSZ")+"'"
	Endif
	
	If !Empty(cTpAssJur) .And. cTpAssJur $ cAssJurGrp
		cFrom += " AND NSZB.NSZ_TIPOAS IN " + FormatIn(cTpAssJur, ",")
	Else
		cFrom += " AND NSZB.NSZ_TIPOAS IN (" + cAssJurGrp + ") "
	EndIf

	If !Empty(aSQLRestri)
		cFrom +=            " AND ("+Ja162SQLRt(aSQLRestri, , , , , , , , , cTpAJ)+")"
	EndIf

	cFrom  +=              "GROUP BY CONCAT(SUBSTRING(NSZB.NSZ_DTENCE,1,6),'01')) ENCERRADOS "

	cFrom  +=    " ON NOVOS.ANOMES = ENCERRADOS.ANOMES "

	cWhere := " WHERE COALESCE(NOVOS.ANOMES, ENCERRADOS.ANOMES) <= '" + DTOS(dData) + "' "
	cWhere +=   " AND COALESCE(NOVOS.ANOMES, ENCERRADOS.ANOMES) >= '" + DTOS(FirstDate( MonthSub( dData , 11 ) )) + "' "

	cWhere += " ORDER BY MES ASC"

	cQry := ChangeQuery(cQry+cFrom+cWhere)
	dbUseArea(.T., 'TOPCONN', TcGenQry( ,, cQry ) , cAlias, .T., .F.)

	// Inclui as movimenta��es somando a quantidade de andamentos
	While !(cAlias)->(Eof())
		nQtdAnd := (nQtdAnd + (cAlias)->NOVO - (cAlias)->ENCERRADO)
		aAdd( aMov, {AllTrim((cAlias)->MES), (cAlias)->NOVO, (cAlias)->ENCERRADO, nQtdAnd})
		(cAlias)->( dbSkip() )
	End

	(cAlias)->( dbcloseArea() )

	//Monta o Json com os valores para os 12 m�ses
	dData := FirstDate( MonthSub( dData , 11 ) )
	nJ := Len(aMov)

	oResponse['evolution'] := {}
	oResponse['operation'] := "StockEvolution"

	// Loop do M�s a M�s
	For nI := 1 to 12
		Aadd(oResponse['evolution'], JsonObject():New())
		oResponse['evolution'][nI]['mesano']      := AllTrim(DToS(dData))
		oResponse['evolution'][nI]['novos']       := 0
		oResponse['evolution'][nI]['encerrados']  := 0
		oResponse['evolution'][nI]['andamento']   := nQtdAnd

		// Verifica se houve gera��o de processo e qual a posi��o no Array
		nJ := aScan(aMov,{|x| AllTrim(x[1]) == AllTrim(DToS(dData))})

		If (nJ > 0)
			// Atualiza a Quantidade de Andamentos
			nQtdAnd := aMov[nJ][4]

			// Inclui os valores
			oResponse['evolution'][nI]['novos']      := aMov[nJ][2]
			oResponse['evolution'][nI]['encerrados'] := aMov[nJ][3]
			oResponse['evolution'][nI]['andamento']  := nQtdAnd
		EndIf

		// Adiciona 1 m�s
		dData := MonthSum( dData , 1 )
	Next

	RestArea(aArea)
Return oResponse

//-------------------------------------------------------------------
/*/{Protheus.doc} JWSPsqRNSZ
Listagem de Processos para a barra de Pesquisa R�pida TotvsLegal

@author nishizaka.cristiane
@since 15/07/2019

@param cSearchKey - palavra a ser pesquisada no Nome do Envolvido
					(NT9_NOME) e N�mero do Processo (NUQ_NUMPRO)
@param nPageSize  - Quantidade de itens na p�gina
/*/
//-------------------------------------------------------------------
Function JWSPsqRNSZ(cSearchKey,nPageSize)
	Local cQrySelect   := ""
	Local cQryFrom     := ""
	Local cQryWhere    := ""
	Local cQuery       := ""
	Local oResponse    := JsonObject():New()
	Local aSQLRest     := Ja162RstUs(,,,.T.)
	Local cTpAJ        := JWsLpGtNyb()
	Local cAlias       := GetNextAlias()
	Local nIndexJSon   := 0
	Local cExists      := ''
	Local cWhere       := ''
	Local aFilUsr      := JURFILUSR( __CUSERID, "NSZ" )
	Local nPage        := 1
	Local cAssJurGrp   := ""
	Local bFiliais     := .F.

	Default cSearchKey := ""
	Default nPageSize  := 10

	cQrySelect := " SELECT DISTINCT NSZ.NSZ_COD    NSZ_COD "
	cQrySelect +=        " ,NVE.NVE_TITULO NVE_TITULO , NSZ.NSZ_FILIAL"

	cQryFrom   :=   " FROM " + RetSqlName('NSZ') + " NSZ "
	cQryFrom   +=   " LEFT  JOIN " + RetSqlName('NVE') + " NVE "
	cQryFrom   +=     " ON  (NVE.NVE_NUMCAS = NSZ.NSZ_NUMCAS) "
	cQryFrom   +=    " AND (NVE.NVE_CCLIEN = NSZ.NSZ_CCLIEN) "
	cQryFrom   +=    " AND (NVE.NVE_LCLIEN = NSZ.NSZ_LCLIEN) "
	cQryFrom   +=    " AND (NVE.NVE_FILIAL = '" + xFilial("NVE") + "') "
	cQryFrom   +=    " AND (NVE.D_E_L_E_T_ = ' ') "
	cQryFrom   +=   " LEFT JOIN " + RetSqlName('NT9') + " NT9 "
	cQryFrom   +=     " ON  (NT9.NT9_CAJURI = NSZ.NSZ_COD "
	cQryFrom   +=    " AND NT9.NT9_FILIAL = NSZ.NSZ_FILIAL "
	cQryFrom   +=    " AND NT9.NT9_PRINCI = '1' "
	cQryFrom   +=    " AND NT9.NT9_TIPOCL = '2' "
	cQryFrom   +=    " AND (NT9.NT9_TIPOEN = '1' "
	cQryFrom   +=     " OR  NT9.NT9_TIPOEN = '2') "
	cQryFrom   +=    " AND NT9.D_E_L_E_T_ = ' ')  "
	cQryFrom   +=   " JOIN " + RetSqlName('NUQ') + " NUQ "
	cQryFrom   +=     " ON (NUQ.NUQ_CAJURI = NSZ.NSZ_COD "
	cQryFrom   +=    " AND NUQ.NUQ_INSATU = '1' "
	cQryFrom   +=    " AND NUQ.NUQ_FILIAL = NSZ.NSZ_FILIAL "
	cQryFrom   +=    " AND NUQ.D_E_L_E_T_ = ' ' ) "

	// Filtar Filiais que o usu�rio possui acesso
	If ( VerSenha(114) .or. VerSenha(115) )
		cQryWhere := " WHERE NSZ.NSZ_FILIAL IN " + FORMATIN(aFilUsr[1],aFilUsr[2])
		If "," $ FORMATIN(aFilUsr[1],aFilUsr[2])
			bFiliais := .T.
		EndIf
	Else
		cQryWhere := " WHERE NSZ.NSZ_FILIAL = '"+xFilial("NSZ")+"'"
	Endif

	cAssJurGrp := JurTpAsJr(__CUSERID)
	cQryWhere += " AND NSZ.NSZ_TIPOAS IN (" + cAssJurGrp + ") "

	cQryWhere  +=  "   AND NSZ.D_E_L_E_T_ = ' ' "

	cSearchKey := Lower(StrTran(JurLmpCpo( cSearchKey,.F. ),'#',''))

	cWhere    := " AND " + JurFormat("NT9_NOME", .T.,.T.) + " Like '%" + cSearchKey + "%'"
	cExists   :=     " AND (" + SUBSTR(JurGtExist(RetSqlName("NT9"), cWhere, "NSZ.NSZ_FILIAL"),5)
	cQryWhere += cExists

	cWhere    :=     " AND " + JurFormat("NUQ_NUMPRO", .F.,.T.) + " Like '%" + cSearchKey + "%'"
	cExists   :=      " OR " + SUBSTR(JurGtExist(RetSqlName("NUQ"), cWhere, "NSZ.NSZ_FILIAL"),5)

	cQryWhere += cExists + " OR NSZ.NSZ_COD  Like '%" + cSearchKey + "%' ) "

	cQryWhere  += VerRestricao()

	If !Empty(aSQLRest)
		cQryWhere += " AND ("+Ja162SQLRt(aSQLRest, , , , , , , , , cTpAJ)+")"
	EndIf

	cQryOrder := " ORDER BY NSZ.NSZ_COD "

	cQuery := cQrySelect + cQryFrom + cQryWhere + cQryOrder

	cQuery := ChangeQuery(cQuery)

	cQuery := StrTran(cQuery,",' '",",''")

	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )

	oResponse['processes'] := {}
	oResponse['userName']  := cUserName
	oResponse['hasMoreBranch']  := bFiliais

	nQtdRegIni := ((nPage-1) * nPageSize)
	// Define o range para inclus�o no JSON
	nQtdRegFim := (nPage * nPageSize)
	nQtdReg    := 0

	While (cAlias)->(!Eof())

		nQtdReg++

		// Assunto Juridico
		If (nQtdReg > nQtdRegIni .AND. nQtdReg <= nQtdRegFim)
			nIndexJSon++
			Aadd(oResponse['processes'], JsonObject():New())

			oResponse['processes'][nIndexJSon]['processCompany'] := cEmpAnt
			oResponse['processes'][nIndexJSon]['processBranch']  := (cAlias)->NSZ_FILIAL
			oResponse['processes'][nIndexJSon]['processId']      := (cAlias)->NSZ_COD
			oResponse['processes'][nIndexJSon]['description']    := JConvUTF8((cAlias)->NVE_TITULO )

			oResponse['length'] := nQtdReg
		Endif

		(cAlias)->(DbSkip())

	End

	(cAlias)->( DbCloseArea() )

Return oResponse

//-------------------------------------------------------------------
/*/{Protheus.doc} JWsListAnd(codProc)
Retorna Andamentos de um Assunto Jur�dico ( Processo )

@param cCajuri - C�digo do Processo
@param nPage - Numero da p�gina
@param nPageSize - Quantidade de itens na p�gina
@param nQtdCarac - Quantidade de caracteres exibidos no campo NT4_DESC

@Return oRespAnd - Andamentos

@since 23/07/19
/*/
//-------------------------------------------------------------------
Function JWsListAnd(cCajuri,nPage,nPageSize,nQtdCarac,cSearchKey)

	Local nIndexJson
	Local oRespAnd  := JsonObject():New()
	Local aNt4      := {}
	Local cQuery    := ""
	Local cQrySel   := ""
	Local cQryFrm   := ""
	Local cQryWhr   := ""
	Local aArea     := GetArea()
	Local cAliasNt4 := GetNextAlias()

	Default nPage      := 1
	Default nPageSize  := 10
	Default nQtdCarac  := 4000
	Default cSearchKey := ""

	cQrySel  := "SELECT NT4.NT4_COD "
	cQrySel  +=       ",NRO.NRO_DESC NRO_DESC "
	cQrySel  +=       ",NQG.NQG_DESC NQG_DESC "
	cQrySel  +=       ",NT4.NT4_DTANDA NT4_DTANDA "
	cQrySel  +=       ",Cast(NT4.NT4_DESC AS VARCHAR("+ cValToChar(nQtdCarac) +")) NT4_DESC "
	cQrySel  +=       ",NUQ.NUQ_INSTAN NUQ_INSTAN "

	cQryFrm  += " FROM " + RetSqlName('NT4') + " NT4 LEFT JOIN " + RetSqlName('NRO') + " NRO ON (NRO.NRO_COD = NT4.NT4_CATO) "
	cQryFrm  +=                                                                           " AND (NRO.NRO_FILIAL = '"  + xFilial('NRO') + "') "
	cQryFrm  +=                                                                           " AND (NRO.D_E_L_E_T_ = ' ') "
	cQryFrm  +=                                    " LEFT JOIN " + RetSqlName('NQG') + " NQG ON (NQG.NQG_COD = NT4.NT4_CFASE) "
	cQryFrm  +=                                                                           " AND (NQG.NQG_FILIAL = '"  + xFilial('NQG') + "') "
	cQryFrm  +=                                                                           " AND (NQG.D_E_L_E_T_ = ' ') "
	cQryFrm  +=                                    " LEFT JOIN " + RetSqlName('NQQ') + " NQQ ON (NQQ.NQQ_COD = NRO.NRO_COD) "
	cQryFrm  +=                                                                           " AND (NQQ.NQQ_FILIAL = '"  + xFilial('NQQ') + "') "
	cQryFrm  +=                                                                           " AND (NQQ.D_E_L_E_T_ = ' ') "
	cQryFrm  +=                                    " LEFT JOIN " + RetSqlName('NUQ') + " NUQ ON (NUQ.NUQ_COD = NT4.NT4_CINSTA) "
	cQryFrm  +=                                                                           " AND (NUQ.NUQ_CAJURI = NT4.NT4_CAJURI) "
	cQryFrm  +=                                                                           " AND (NUQ.NUQ_FILIAL = '"+ xFilial('NUQ') + "') "
	cQryFrm  +=                                                                           " AND (NUQ.D_E_L_E_T_ = ' ') "
	cQryFrm  +=                                    " LEFT JOIN " + RetSqlName('NSZ') + " NSZ ON (NSZ.NSZ_COD = NT4.NT4_CAJURI) "
	cQryFrm  +=                                                                           " AND (NSZ.NSZ_FILIAL = '"+ xFilial('NSZ') + "') "
	cQryFrm  +=                                                                           " AND (NSZ.D_E_L_E_T_ = ' ') "

	cQryWhr  += "WHERE NT4.NT4_FILIAL = '" + xFilial("NT4") + "' "
	cQryWhr  +=  " AND NT4.NT4_CAJURI = '" + cCajuri + "' "
	cQryWhr  +=  " AND NT4.D_E_L_E_T_ = ' ' "

	If !Empty(cSearchKey)
		cSearchKey := Lower(StrTran(JurLmpCpo( cSearchKey,.F. ),'#',''))

		cQryWhr += " AND (" + JurFormat("NRO_DESC", .T.,.T.) + " Like '%" + cSearchKey + "%'"

		cQryWhr += " OR  " + JurFormat("NQG_DESC", .F.,.T.) + " Like '%" + cSearchKey + "%'"

		cQryWhr += " OR  " + JurFormat("NT4_DESC", .T.,.T.) + " Like '%" + cSearchKey + "%'"

		cQryWhr += " OR " + JurFormat("NT4_USUINC", .T.,.T.) + " Like '%" + cSearchKey + "%')"
	EndIf

	cQryWhr  +=  " ORDER BY NT4_DTANDA DESC "

	cQuery := cQrySel + cQryFrm + cQryWhr

	cQuery := ChangeQuery(cQuery)

	cQuery := StrTran(cQuery,",' '",",''")

	dbUseArea(.T., 'TOPCONN', TcGenQry( ,, cQuery ) , cAliasNt4, .T., .F.)

	While !(cAliasNt4)->(Eof())
		aAdd( aNt4, { (cAliasNt4)->NT4_COD;
			,(cAliasNt4)->NRO_DESC;
			,(cAliasNt4)->NT4_DTANDA;
			,(cAliasNt4)->NT4_DESC;
			,(cAliasNt4)->NUQ_INSTAN;
			,(cAliasNt4)->NQG_DESC} )
		(cAliasNt4)->( dbSkip() )
	End

	(cAliasNt4)->(dbCloseArea())

	RestArea(aArea)

	nQtdRegIni := ((nPage-1) * nPageSize)
	// Define o range para inclus�o no JSON
	nQtdRegFim := (nPage * nPageSize)
	nQtdReg    := 0

	oRespAnd := {}
	If !Empty(aNT4)
		For nIndexJSon := 1 to Len(aNT4)
			nQtdReg++
			// Verifica se o registro est� no range da pagina
			If (nQtdReg > nQtdRegIni .AND. nQtdReg <= nQtdRegFim)
				Aadd(oRespAnd, JsonObject():New())
				oRespAnd[nIndexJSon]['id']          := JConvUTF8(aNT4[nIndexJSon][1])
				oRespAnd[nIndexJSon]['title']       := JConvUTF8(aNT4[nIndexJSon][2])
				oRespAnd[nIndexJSon]['date']        := JConvUTF8(aNT4[nIndexJSon][3])
				oRespAnd[nIndexJSon]['description'] := JConvUTF8(aNT4[nIndexJSon][4])
				oRespAnd[nIndexJSon]['phase']       := JConvUTF8(aNT4[nIndexJSon][6])
			Endif
		Next
	EndIf

Return oRespAnd

//-------------------------------------------------------------------
/*/{Protheus.doc} JWsLpGtPro
Listagem de processos com informa��es resumidas
Utilizado na tela de resumo do processo TOTVS Legal

@since 24/07/19

@param cCodProc - C�digo do processo a ser pesquisado
@param nPage - Numero da p�gina
@param nPageSize  - Quantidade de itens na p�gina
@param cSearchKey - C�digo do processo (cajuri)
@param cQryOrder  - Order by da query ("1 - Ordena decrescente por Vlr. Provis�o , 2 - Ordena decrescente por Dt. Ult. Andamento)
@param cFilter    - Filtra a query (1 = Processos em Andamento, 2 = Casos Novos, 3 = Encerrados)
@param cSaldoNT2  - Busca o saldo da garantia (1 - garantia)
@example - http://localhost:4200/juri/JURLEGALPROCESS/tlprocess/detail/0000000066
/*/
//-------------------------------------------------------------------
Function JWsLpGtPro(cCodProc, nPage, nPageSize, cSearchKey, cQryOrder, cFilter,cSaldoNT2)
	Local cQrySelect   := ""
	Local cQryFrom     := ""
	Local cQryWhere    := ""
	Local cQuery       := ""
	Local cCajuri      := ""
	Local cInstan      := ""
	Local nCount       := 0
	Local nIndexJSon   := 0
	Local oResponse    := JsonObject():New()
	Local lHasNext     := .F.
	Local aSQLRest     := Ja162RstUs(,,,.T.)
	Local cTpAJ        := JWsLpGtNyb()
	Local cAlias       := GetNextAlias()
	Local aNuq         := {}
	Local aNT9         := {}
	Local cNszSituac   := ''
	Local cNuqInstan   := ''
	Local cCriaPasta   := ''
	Local cLinkNZ7     := ''
	Local oAnexo       := {}

	Default cCodProc   := ""
	Default nPage      := 1
	Default nPageSize  := 10
	Default cSearchKey := ""
	Default cQryOrder  := "0"
	Default cFilter    := "0"
	Default cSaldoNT2  := "0"

	cQrySelect := " SELECT NSZ.NSZ_COD    NSZ_COD "
	cQrySelect +=       " ,NSZ.NSZ_CCLIEN NSZ_CCLIEN "
	cQrySelect +=       " ,NSZ.NSZ_LCLIEN NSZ_LCLIEN "
	cQrySelect +=       " ,SA1.A1_NOME    SA1_NOME "
	cQrySelect +=       " ,NSZ.NSZ_NUMCAS NSZ_NUMCAS "
	cQrySelect +=       " ,NVE.NVE_TITULO NVE_TITULO "
	cQrySelect +=       " ,NSZ.NSZ_TIPOAS NSZ_TIPOAS "
	cQrySelect +=       " ,NYB.NYB_DESC   NYB_DESC "
	cQrySelect +=       " ,NSZ.NSZ_DTENTR NSZ_DTENTR "
	cQrySelect +=       " ,NSZ.NSZ_VLPROV VPROV "
	cQrySelect +=       " ,NSZ.NSZ_SITUAC NSZ_SITUAC "
	cQrySelect +=       " ,RD01.RD0_NOME  RD0_NOMERESP  "
	cQrySelect +=       " ,RD01.RD0_SIGLA RD0_SIGLARESP "
	cQrySelect +=       " ,RD02.RD0_NOME  RD0_NOMEADVO  "
	cQrySelect +=       " ,RD02.RD0_SIGLA RD0_SIGLAADVO "
	cQrySelect +=       " ,NSZ.NSZ_CAREAJ NSZ_CAREAJ "
	cQrySelect +=       " ,NRB.NRB_DESC   NRB_DESC "
	cQrySelect +=       " ,NUQ.NUQ_NUMPRO NUMPRO"
	cQrySelect +=       " ,NSZ.NSZ_VAPROV NSZ_VAPROV"
	cQrySelect +=       " ,NQ4.NQ4_DESC NQ4_DESC"
	cQrySelect +=       " ,SA2.A2_NOME SA2_CORRESP"
	cQrySelect +=       " ,NSZ.NSZ_SJUIZA NSZ_SJUIZA"
	cQrySelect +=       " ,NSZ.NSZ_VAENVO NSZ_VAENVO"
	cQrySelect +=       " ,NZ7.NZ7_LINK NZ7_LINK"

	// Valores
	cQrySelect +=       " ,(CASE WHEN NSZ.NSZ_VAPROV = 0 "
	cQrySelect +=                " THEN NSZ.NSZ_VLPROV "
	cQrySelect +=                " ELSE NSZ.NSZ_VAPROV END "
	cQrySelect +=        "  ) VALOR "

	// valor da causa
	cQrySelect +=       " ,(CASE WHEN NSZ.NSZ_VACAUS = 0 "
	cQrySelect +=                " THEN NSZ.NSZ_VLCAUS "
	cQrySelect +=                " ELSE NSZ.NSZ_VACAUS END "
	cQrySelect +=        "  ) VLRCAUSA "

	cQryFrom   := " FROM " + RetSqlName('NSZ') + " NSZ INNER JOIN " + RetSqlName('SA1') + " SA1 ON (SA1.A1_COD = NSZ.NSZ_CCLIEN) "
	cQryFrom   +=                                                                           " AND (SA1.A1_LOJA = NSZ.NSZ_LCLIEN) "
	cQryFrom   +=                                                                           " AND " + JQryFilial("NSZ","SA1","NSZ","SA1")
	cQryFrom   +=                                                                           " AND (SA1.D_E_L_E_T_ = ' ') "
	cQryFrom   +=                                   " LEFT  JOIN " + RetSqlName('NVE') + " NVE  ON (NVE.NVE_NUMCAS = NSZ.NSZ_NUMCAS) "
	cQryFrom   +=                                                                           " AND (NVE.NVE_CCLIEN = NSZ.NSZ_CCLIEN) "
	cQryFrom   +=                                                                           " AND (NVE.NVE_LCLIEN = NSZ.NSZ_LCLIEN) "
	cQryFrom   +=                                                                           " AND (NVE.NVE_FILIAL = '" + xFilial("NVE") + "') "
	cQryFrom   +=                                                                           " AND (NVE.D_E_L_E_T_ = ' ') "
	cQryFrom   +=                                   " LEFT  JOIN " + RetSqlName('NYB') + " NYB  ON (NYB.NYB_COD = NSZ.NSZ_TIPOAS) "
	cQryFrom   +=                                                                           " AND (NYB.NYB_FILIAL = '" + xFilial("NYB") + "') "
	cQryFrom   +=                                                                           " AND (NYB.D_E_L_E_T_ = ' ') "
	cQryFrom   +=                                   " LEFT  JOIN " + RetSqlName('NYB') + " NYB2 ON (NYB2.NYB_COD = NYB.NYB_CORIG) "
	cQryFrom   +=                                                                           " AND (NYB2.NYB_FILIAL = '" + xFilial("NYB") + "') "
	cQryFrom   +=                                                                           " AND (NYB2.D_E_L_E_T_ = ' ') "
	cQryFrom   +=                                   " LEFT  JOIN " + RetSqlName('RD0') + " RD01 ON (RD01.RD0_CODIGO = NSZ.NSZ_CPART1) "
	cQryFrom   +=                                                                           " AND (RD01.RD0_FILIAL = '" + xFilial("RD0") + "') "
	cQryFrom   +=                                                                           " AND (RD01.D_E_L_E_T_ = ' ') "
	cQryFrom   +=                                   " LEFT  JOIN " + RetSqlName('RD0') + " RD02 ON (RD02.RD0_CODIGO = NSZ.NSZ_CPART2) "
	cQryFrom   +=                                                                           " AND (RD02.RD0_FILIAL = '" + xFilial("RD0") + "') "
	cQryFrom   +=                                                                           " AND (RD02.D_E_L_E_T_ = ' ') "
	cQryFrom   +=                                   " LEFT  JOIN " + RetSqlName('NRB') + " NRB  ON (NRB.NRB_COD = NSZ.NSZ_CAREAJ) "
	cQryFrom   +=                                                                           " AND (NRB.NRB_FILIAL = '" + xFilial("NRB") + "') "
	cQryFrom   +=                                                                           " AND (NRB.D_E_L_E_T_ = ' ') "
	cQryFrom   +=                                   " LEFT JOIN " + RetSqlName('NUQ') + " NUQ ON (NUQ.NUQ_CAJURI = NSZ.NSZ_COD "
	cQryFrom   +=                                                                           "AND NUQ.NUQ_INSATU  = '1' "
	cQryFrom   +=                                                                           "AND NUQ.NUQ_FILIAL  = '" + xFilial("NUQ") + "' "
	cQryFrom   +=                                                                           "AND NUQ.D_E_L_E_T_ = ' ' ) "
	cQryFrom   +=                                   " LEFT JOIN " + RetSqlName('SA2') + " SA2 ON (SA2.A2_COD = NUQ.NUQ_CCORRE "
	cQryFrom   +=                                                                          " AND SA2.A2_LOJA = NUQ.NUQ_LCORRE "
	cQryFrom   +=                                                                           "AND SA2.A2_FILIAL  = '" + xFilial("SA2") + "' "
	cQryFrom   +=                                                                           "AND SA2.D_E_L_E_T_ = ' ' ) "
	cQryFrom   +=                                   " LEFT JOIN " + RetSqlName('NQ4') + " NQ4 ON (NQ4.NQ4_COD = NSZ.NSZ_COBJET "
	cQryFrom   +=                                                                           "AND NQ4.NQ4_FILIAL  = '" + xFilial("NQ4") + "' "
	cQryFrom   +=                                                                           "AND NQ4.D_E_L_E_T_ = ' ' ) "
	cQryFrom   +=                                   " LEFT JOIN " + RetSqlName('NZ7') + " NZ7 ON (NZ7.NZ7_NUMCAS = NSZ.NSZ_NUMCAS "
	cQryFrom   +=                                                                           "AND NZ7.NZ7_CCLIEN = NSZ.NSZ_CCLIEN "
	cQryFrom   +=                                                                           "AND NZ7.NZ7_LCLIEN = NSZ.NSZ_LCLIEN "
	cQryFrom   +=                                                                           "AND NZ7.NZ7_FILIAL  = '" + xFilial("NZ7") + "' "
	cQryFrom   +=                                                                           "AND NZ7.D_E_L_E_T_ = ' ' ) "

	cQryWhere  := " WHERE NSZ.NSZ_FILIAL = '" + xFilial('NSZ')+ "'"
	cQryWhere  += "   AND NSZ.D_E_L_E_T_ = ' ' "
	cQryWhere  += "   AND NSZ.NSZ_TIPOAS IN (" + cTpAJ + ") "

	cQryWhere  += VerRestricao()

	If !Empty(aSQLRest)
		cQryWhere += " AND ("+Ja162SQLRt(aSQLRest, , , , , , , , , cTpAJ)+")"
	EndIf

	If !Empty(cCodProc)
		cQryWhere += " AND NSZ.NSZ_COD = '" + cCodProc + "'"
	EndIf

	// Define o Filtro aplicado na query
	If cFilter == "1" // Processos em Andamento
		cQryWhere += " AND NSZ.NSZ_SITUAC = '1' "
	ElseIf cFilter == "2" // Processos Cadastrados no m�s corrente
		cQryWhere += " AND NSZ.NSZ_DTINCL >= '" + DTOS(FirstDate( Date() )) + "' "
	ElseIf cFilter == "3" // Processos Encerrados no m�s corrente
		cQryWhere += " AND NSZ.NSZ_DTENCE >= '" + DTOS(FirstDate( Date() )) + "' "
		cQryWhere += " AND NSZ.NSZ_SITUAC = '2' "
	EndIf

	//Define a ordena��o da query
	If  cQryOrder == "1"
		cQryOrder := " ORDER BY VALOR DESC"
	ElseIf cQryOrder == "2"
		cQryOrder := " ORDER BY ULTAND.NT4_DTANDA DESC, VALOR DESC "
	Else
		cQryOrder := " ORDER BY NSZ.NSZ_COD "
	EndIf

	cQuery := cQrySelect + cQryFrom + cQryWhere + cQryOrder

	cQuery := ChangeQuery(cQuery)

	cQuery := StrTran(cQuery,",' '",",''")

	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )

	oResponse['processes'] := {}

	nQtdRegIni := ((nPage-1) * nPageSize)
	// Define o range para inclus�o no JSON
	nQtdRegFim := (nPage * nPageSize)
	nQtdReg    := 0

	If !Empty(cCodProc)
		oResponse['operation'] := "DetailProcess"
	Else
		oResponse['operation'] := "ListProcess"
	EndIf

	oResponse['userName'] := cUserName

	While (cAlias)->(!Eof())

		nQtdReg++
		// Verifica se o registro est� no range da pagina
		if (nQtdReg > nQtdRegIni .AND. nQtdReg <= nQtdRegFim)
			nIndexJSon++

			// Assunto Juridico
			cCajuri := (cAlias)->NSZ_COD
			Aadd(oResponse['processes'], JsonObject():New())

			oResponse['processes'][nIndexJSon]['processId']          := cCajuri
			oResponse['processes'][nIndexJSon]['assJur']             := JConvUTF8((cAlias)->NSZ_TIPOAS)
			oResponse['processes'][nIndexJSon]['assJurDesc']         := JConvUTF8((cAlias)->NYB_DESC)
			oResponse['processes'][nIndexJSon]['entryDate']          := JConvUTF8((cAlias)->NSZ_DTENTR)
			oResponse['processes'][nIndexJSon]['provisionValue']     := ROUND((cAlias)->VPROV,2)
			oResponse['processes'][nIndexJSon]['atualizedprovision'] := ROUND((cAlias)->VALOR,2)
			oResponse['processes'][nIndexJSon]['balanceInCourt']     := ROUND((cAlias)->NSZ_SJUIZA,2)
			oResponse['processes'][nIndexJSon]['atualizedinvolved']  := ROUND((cAlias)->NSZ_VAENVO,2)
			oResponse['processes'][nIndexJSon]['subject']            := JConvUTF8((cAlias)->NQ4_DESC)
			oResponse['processes'][nIndexJSon]['office']             := JConvUTF8((cAlias)->SA2_CORRESP)
			oResponse['processes'][nIndexJSon]['atualizedCause']     := ROUND((cAlias)->VLRCAUSA,2)


			If cSaldoNT2 == "1"//valor do saldo das garantias
				oResponse['processes'][nIndexJSon]['balanceguarantees'] := ROUND(JUR98G('1',cCodProc),2)
				oResponse['processes'][nIndexJSon]['balanceExpenses']   := ROUND(JSumDesp(cCodProc),2)
			EndIf

			// Pasta Fluig
			oResponse['processes'][nIndexJSon]['folderFluig'] := {}
			Aadd(oResponse['processes'][nIndexJSon]['folderFluig'], JsonObject():New())

			If !Empty(JFlgUrl()) // Se a integra��o com o Fluig estiver ativa
				If !Empty((cAlias)->NZ7_LINK) // Verifica se h� conte�do no campo NZ7_LINK obtido na query
					cLinkNZ7 := AllTrim((cAlias)->NZ7_LINK)
				Else
					cCriaPasta := J070PFluig( (cAlias)->NSZ_CCLIEN + (cAlias)->NSZ_LCLIEN + (cAlias)->NSZ_NUMCAS, "") // Realiza a cria��o da pasta no Fluig

					If cCriaPasta == "2"
						cLinkNZ7 := AllTrim(JurGetDados("NZ7", 1, xFilial("NZ7") + (cAlias)->NSZ_CCLIEN + (cAlias)->NSZ_LCLIEN + (cAlias)->NSZ_NUMCAS, "NZ7_LINK"))
					Endif
				Endif
				oAnexo := getAnexo("NSZ", cCajuri)
				aTail(oResponse['processes'][nIndexJSon]['folderFluig'])['link']    := SubStr(cLinkNZ7,1,at(";",cLinkNZ7)-1  )
				aTail(oResponse['processes'][nIndexJSon]['folderFluig'])['version'] := SubStr(cLinkNZ7  ,at(";",cLinkNZ7)+1,4)
				aTail(oResponse['processes'][nIndexJSon]['folderFluig'])['url']     := oAnexo:Abrir(.F.,cLinkNZ7)
			Endif

			// Area
			oResponse['processes'][nIndexJSon]['area'] := {}
			Aadd(oResponse['processes'][nIndexJSon]['area'], JsonObject():New())
			aTail(oResponse['processes'][nIndexJSon]['area'])['code']        := JConvUTF8((cAlias)->NSZ_CAREAJ)
			aTail(oResponse['processes'][nIndexJSon]['area'])['description'] := JConvUTF8((cAlias)->NRB_DESC)

			// Caso
			oResponse['processes'][nIndexJSon]['matter'] := {}
			Aadd(oResponse['processes'][nIndexJSon]['matter'], JsonObject():New())
			aTail(oResponse['processes'][nIndexJSon]['matter'])['code']        := JConvUTF8((cAlias)->NSZ_NUMCAS)
			aTail(oResponse['processes'][nIndexJSon]['matter'])['description'] := JConvUTF8((cAlias)->NVE_TITULO)

			// Status do Processo
			cNszSituac := JConvUTF8((cAlias)->NSZ_SITUAC)
			oResponse['processes'][nIndexJSon]['status'] := {}
			Aadd(oResponse['processes'][nIndexJSon]['status'], JsonObject():New())
			aTail(oResponse['processes'][nIndexJSon]['status'])['code'] := cNszSituac

			If (cNszSituac == '1')
				aTail(oResponse['processes'][nIndexJSon]['status'])['description'] := JConvUTF8(STR0005) // Em andamento
			Else
				aTail(oResponse['processes'][nIndexJSon]['status'])['description'] := JConvUTF8(STR0006) // Encerrado
			EndIf

			// Participantes do Processo
			// Respons�vel
			oResponse['processes'][nIndexJSon]['staff'] := {}
			Aadd(oResponse['processes'][nIndexJSon]['staff'], JsonObject():New())
			aTail(oResponse['processes'][nIndexJSon]['staff'])['position'] := JConvUTF8(STR0007)  // Respons�vel
			aTail(oResponse['processes'][nIndexJSon]['staff'])['name']     := JConvUTF8((cAlias)->RD0_NOMERESP)
			aTail(oResponse['processes'][nIndexJSon]['staff'])['initials'] := JConvUTF8((cAlias)->RD0_SIGLARESP)

			// Advogado
			Aadd(oResponse['processes'][nIndexJSon]['staff'], JsonObject():New())
			aTail(oResponse['processes'][nIndexJSon]['staff'])['position'] := JConvUTF8(STR0008)  // Advogado
			aTail(oResponse['processes'][nIndexJSon]['staff'])['name']     := JConvUTF8((cAlias)->RD0_NOMEADVO)
			aTail(oResponse['processes'][nIndexJSon]['staff'])['initials'] := JConvUTF8((cAlias)->RD0_SIGLAADVO)

			// Autor/Reu
			aNT9 := JWsLpGtNt9(cCajuri,0)
			For nCount := 1 to Len(aNT9)
				If (JConvUTF8(aNT9[nCount][4])) == '1' // � principal?
					If (JConvUTF8(aNT9[nCount][11])) == '1' // Polo Ativo
						oResponse['processes'][nIndexJSon]['author'] := JConvUTF8(aNT9[nCount][5])
					Elseif (JConvUTF8(aNT9[nCount][11])) == '2' // Polo Passivo
						oResponse['processes'][nIndexJSon]['reu'] := JConvUTF8(aNT9[nCount][5])
					Endif
				Endif
			Next

			// Envolvidos - Parte contraria
			oResponse['processes'][nIndexJSon]['oppositeParty'] := getPartCont(cCajuri)

			// Inst�ncia
			aNuq := JWsLpGtNuq(cCajuri,.F.) // Todas as inst�ncias

			oResponse['processes'][nIndexJSon]['instance'] := {}

			if !Empty(aNuq)
				nCount := 0

				For nCount := 1 to Len(aNuq)
					Aadd(oResponse['processes'][nIndexJSon]['instance'], JsonObject():New())
					cInstan    := JConvUTF8(aNuq[nCount][1])
					cNuqInstan := JConvUTF8(aNuq[nCount][3])

					aTail(oResponse['processes'][nIndexJSon]['instance'])['id']             := cInstan
					aTail(oResponse['processes'][nIndexJSon]['instance'])['instaAtual']     := JConvUTF8(aNuq[nCount][2])
					aTail(oResponse['processes'][nIndexJSon]['instance'])['numInstance']    := JConvUTF8(aNuq[nCount][3])
					aTail(oResponse['processes'][nIndexJSon]['instance'])['displayName']    := JConvUTF8(JGetInsDes(cNuqInstan))
					aTail(oResponse['processes'][nIndexJSon]['instance'])['processNumber']  := JConvUTF8(aNuq[nCount][4])
					aTail(oResponse['processes'][nIndexJSon]['instance'])['natureCode']     := JConvUTF8(aNuq[nCount][5])
					aTail(oResponse['processes'][nIndexJSon]['instance'])['nature']         := JConvUTF8(aNuq[nCount][6])
					aTail(oResponse['processes'][nIndexJSon]['instance'])['cityCode']       := JConvUTF8(aNuq[nCount][7])
					aTail(oResponse['processes'][nIndexJSon]['instance'])['city']           := JConvUTF8(aNuq[nCount][8])
					aTail(oResponse['processes'][nIndexJSon]['instance'])['distribution']   := JConvUTF8(aNuq[nCount][9])
					aTail(oResponse['processes'][nIndexJSon]['instance'])['local']          := JConvUTF8(aNuq[nCount][10])
					aTail(oResponse['processes'][nIndexJSon]['instance'])['districtCourt']  := JConvUTF8(aNuq[nCount][11])
					aTail(oResponse['processes'][nIndexJSon]['instance'])['branch']         := JConvUTF8(aNuq[nCount][12])

				Next
			EndIf

			// Encerramento
			aNszEnc := JWsLpGtEnc(cCajuri)
			oResponse['processes'][nIndexJSon]['closure'] := {}
			If !Empty(aNszEnc)
				For nCount := 1 to Len(aNszEnc)
					Aadd(oResponse['processes'][nIndexJSon]['closure'], JsonObject():New())
					aTail(oResponse['processes'][nIndexJSon]['closure'])['type'] 	     := JConvUTF8(aNszEnc[nCount][3])
					aTail(oResponse['processes'][nIndexJSon]['closure'])['description'] := JConvUTF8(aNszEnc[nCount][4])
					aTail(oResponse['processes'][nIndexJSon]['closure'])['date']        := JConvUTF8(aNszEnc[nCount][5])
					aTail(oResponse['processes'][nIndexJSon]['closure'])['finalValue']  := aNszEnc[nCount][6]
					aTail(oResponse['processes'][nIndexJSon]['closure'])['veredict']    := JConvUTF8(aNszEnc[nCount][7])
				Next
			EndIf
		Elseif (nQtdReg == nQtdRegFim + 1)
			lHasNext := .T.
		Endif

		(cAlias)->(DbSkip())
	End

	(cAlias)->(dbCloseArea())
	// Verifica se h� uma proxima pagina
	If (lHasNext)
		oResponse['hasNext'] := "true"
	Else
		oResponse['hasNext'] := "false"
	EndIf

	oResponse['length'] := nQtdReg

Return oResponse

//-------------------------------------------------------------------
/*/{Protheus.doc} GET ListDoc
listagem dos anexos

@author SIGAJURI
@since 24/07/2019
@param cEntidade   - Entidade a ser pesquisado o anexo (NTA / NSZ/ NT2...)
@param cCodDoc     - Codigo do documento para pesquisa de Doc especifica
@param cCodEnti    - Codigo da entidade para pesquisa de Doc especifica
@param cCodProc    - C�digo do processo,
@param cSearchKey  - palavra a ser pesquisada no Nome do documento
					  (NUM_DOC) e Extensao do arquivo (NUM_EXTEN)

@example [Sem Opcional] GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/process/0000000049/docs?searchkey='acaa'
@example [Sem Opcional] GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/process/0000000049/docs?entidade=NT4
@example [Sem Opcional] GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/process/0000000049/docs?entidade=NT4&searchkey='teste'

/*/
//-------------------------------------------------------------------
	WSMETHOD GET ListDoc PATHPARAM codProc, nomeEnt WSRECEIVE codEntidade, codDoc, searchKey WSREST JURLEGALPROCESS
	Local oResponse   := JsonObject():New()
	Local aDocs       := {}
	local aEntidades  := {'NSZ','NT2','NT3','NT4','NTA','NSY'}
	Local nI          := 0
	Local nX          := 0
	Local codProc     := Self:codProc
	Local cNomeEntid  := Self:nomeEnt
	Local cCodEnt     := Self:codEntidade
	Local cCodDoc     := Self:codDoc
	Local nIndexJSon  := 0
	Local nQtdReg     := 0
	Local cSearchKey  := Self:searchKey
	Local cParam      := AllTrim(SuperGetMv('MV_JDOCUME',,'2'))

	oResponse['operation'] := "ReturnDocs"
	Self:SetContentType("application/json")
	oResponse['attachments'] := {}

	If !Empty(cNomeEntid) .And. cNomeEntid != "NSZ"
		aEntidades := {cNomeEntid}
	EndIf
	For nI := 1 to Len(aEntidades)
		If (cNomeEntid == aEntidades[nI])
			Do Case
			Case aEntidades[nI] == "NT2"
				cCodEnt := codProc + cCodEnt
			Case aEntidades[nI] == "NT3"
				cCodEnt := codProc + cCodEnt
			Case aEntidades[nI] == "NSY"
				cCodEnt := cCodEnt + codProc
			End Case
		EndIf
		aDocs := listDocs(aEntidades[nI], cCodDoc, cCodEnt,codProc,cSearchKey)

		If !Empty(aDocs)

			for nX:=1 to Len(aDocs)
				nQtdReg++
				nIndexJSon++
				Aadd(oResponse['attachments'], JsonObject():New())

				If cParam <> "3"
					oResponse['attachments'][nIndexJSon]['nameDocument'] := JConvUTF8(aDocs[nX][1])
				Else
					oResponse['attachments'][nIndexJSon]['nameDocument'] := JConvUTF8(aDocs[nX][8])
				EndIf
				oResponse['attachments'][nIndexJSon]['number']       := JConvUTF8(aDocs[nX][2])
				oResponse['attachments'][nIndexJSon]['codeEntity']   := JConvUTF8(aDocs[nX][3])
				oResponse['attachments'][nIndexJSon]['extension']    := JConvUTF8(aDocs[nX][4])
				oResponse['attachments'][nIndexJSon]['nameEntity']   := JConvUTF8(aDocs[nX][5])
				oResponse['attachments'][nIndexJSon]['codeDocument'] := JConvUTF8(aDocs[nX][6])
				oResponse['attachments'][nIndexJSon]['entity']       := JConvUTF8(aDocs[nX][7])
				oResponse['attachments'][nIndexJSon]['dateInsert']   := JConvUTF8(aDocs[nX][9])
			next nX
		EndIf
	Next nI
	oResponse['length'] := nQtdReg

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} |listDocs|
Listagem de documentos, podendo trazer todos anexos que tenham vinculo
com o processo ou de entidades expecificas

@author SIGAJURI
@since 24/07/2019
@param cEntidade   - Entidade a ser pesquisado o anexo (NTA / NSZ/ NT2...)
@param cCodDoc     - Codigo do documento para pesquisa de Doc especifica
@param cCodEnti    - Codigo da entidade para pesquisa de Doc especifica
@param cCodProc    - C�digo do processo,
@param cSearchKey  - palavra a ser pesquisada no Nome do documento
					  (NUM_DOC) e Extensao do arquivo (NUM_EXTEN)
/*/
//-------------------------------------------------------------------
Static Function listDocs(cEntidade, cCodDoc, cCodEnti, cCodProc, cSearchKey)
	Local cQuery     := ""
	Local cQrySel    := ""
	Local cQryFrm    := ""
	Local cQryWhr    := ""
	Local cIdxEnt    := ""
	Local cEntDom    := "NSZ"
	Local cIdxDom    := Replace(AllTrim(FwX2Unico(cEntDom)),'+','||')
	Local cAlias     := GetNextAlias()
	Local cBanco     := Upper(TcGetDb())
	Local aDocs      := {}
	Local cExists    := ""
	Local cWhere     := ""
	local lNvCmpNUM  := .F.
	Local cDtInclNUM := ""

	Default cEntidade  := ""
	Default cCodDoc    := ""
	Default cCodEnti   := ""
	Default cSearchKey := ""

	//Coluna de data de inclus�o, se tiver o campo no dicionario
	DBSelectArea("NUM")
		lNvCmpNUM := (NUM->(FieldPos('NUM_DTINCL')) > 0)
	NUM->( DBCloseArea() )

	cQrySel := " SELECT NUM_DOC, "
	cQrySel +=        " NUM_NUMERO,"
	cQrySel +=        " NUM_COD,"
	cQrySel +=        " NUM_CENTID,"
	cQrySel +=        " NUM_EXTEN,"
	cQrySel +=        " NUM_ENTIDA,"
	cQrySel +=        " NUM_DESC"
	If lNvCmpNUM
		cQrySel +=        ", NUM_DTINCL"
	EndIf

	If !Empty(cEntidade)

		cQryFrm := ' FROM ' + RetSqlName(cEntDom) + ' ' + cEntDom

		If cEntidade != cEntDom //Se a entidade for diferente da NSZ � feito um INNER JOIN
			cQryFrm += " INNER JOIN " + RetSqlName(cEntidade) + " " + cEntidade + " ON (" + cEntDom + "_COD = " + cEntidade + "_CAJURI)"
			cQryFrm +=                                                           " AND (" + cEntDom + "_FILIAL = " + cEntidade + "_FILIAL)"
		EndIf

		cIdxEnt := Replace(AllTrim(FwX2Unico(cEntidade)),'+','||')

		If cBanco == "POSTGRES"
			If JurHasClas()
				cQryFrm += " INNER JOIN " + RetSqlName('NUM') + " NUM ON RTRIM(CONCAT(NUM.NUM_FILENT , NUM.NUM_CENTID)) = RTRIM(CONCAT(" + replace(cIdxEnt,"||",",") + ") )"
			Else
				cQryFrm += " INNER JOIN " + RetSqlName('NUM') + " NUM ON RTRIM(NUM.NUM_CENTID) = RTRIM(CONCAT(" + replace(cIdxEnt,"||",",") + ") )"
			EndIf

			If !Empty(cCodEnti)
				cQryWhr := " WHERE RTRIM(CONCAT(" + cIdxEnt + ")) = RTRIM('" + xFilial(cEntidade) + cCodEnti + "')"
			Else
				cQryWhr := " WHERE " + cIdxDom + " = '" + xFilial(cEntDom) + cCodProc + "'"
			EndIf

		Else
			If JurHasClas()
				cQryFrm += " INNER JOIN " + RetSqlName('NUM') + " NUM ON RTRIM(NUM.NUM_FILENT || NUM.NUM_CENTID) = RTRIM(" + cIdxEnt + ")"
			Else
				cQryFrm += " INNER JOIN " + RetSqlName('NUM') + " NUM ON RTRIM(NUM.NUM_CENTID) = RTRIM(" + cIdxEnt + " )"
			EndIf

			If !Empty(cCodEnti)
				cQryWhr := " WHERE " + cIdxEnt + " = '" + xFilial(cEntidade) + cCodEnti + "'"
			Else
				cQryWhr := " WHERE " + cIdxDom + " = '" + xFilial(cEntDom) + cCodProc + "'"
			EndIf
		EndIf

		cQryWhr +=   " AND " + cEntDom + ".D_E_L_E_T_ = ' ' "

		If !Empty(cCodDoc)
			cQryWhr := " AND NUM.NUM_CENTID = '" + cCodDoc + "'"
		endif

		cQryWhr +=   " AND NUM.D_E_L_E_T_ = ' '"
		cQryWhr +=   " AND NUM.NUM_ENTIDA = '" + cEntidade + "'"
	Else
		cQryFrm := " FROM " + RetSqlName('NUM') + " NUM"

		cQryWhr := " WHERE (NUM.D_E_L_E_T_ = ' ') "

		If !Empty(cCodDoc)
			cQryWhr := " AND NUM.NUM_CENTID = '" + cCodDoc + "'"
		endif
	EndIf
	//Consulta pelo nome do documento e extensao
	If !Empty(cSearchKey)

		cSearchKey := Lower(StrTran(JurLmpCpo( cSearchKey,.F. ),'#',''))

		cWhere    := " AND " + JurFormat("NUM_DOC", .T.,.T.) + " Like '%" + cSearchKey + "%'"
		cExists   := " AND( " + SUBSTR(JurGtExist(RetSqlName("NUM"), cWhere, cEntDom + "_FILIAL"),5)
		cQryWhr += cExists

		cWhere    := " AND " + JurFormat("NUM_EXTEN", .T.,.T.) + " Like '%" + cSearchKey + "%'"
		cExists   := " OR " + SUBSTR(JurGtExist(RetSqlName("NUM"), cWhere, cEntDom + "_FILIAL"),5)
		cQryWhr += cExists + " ) "

		cQryWhr  += VerRestricao()

	EndIf
	cQuery := cQrySel + cQryFrm + cQryWhr

	cQuery := ChangeQuery(cQuery)

	cQuery := StrTran(cQuery,",' '",",''")

	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )

	While !(cAlias)->(Eof())
		If lNvCmpNUM
			cDtInclNUM := (cAlias)->NUM_DTINCL
		EndIf

		aAdd( aDocs, { (cAlias)->NUM_DOC;
			,(cAlias)->NUM_NUMERO;
			,(cAlias)->NUM_CENTID;
			,(cAlias)->NUM_EXTEN;
			,JurX2Nome( (cAlias)->NUM_ENTIDA);
			,(cAlias)->NUM_COD;
			,(cAlias)->NUM_ENTIDA;
			,(cAlias)->NUM_DESC;
			,cDtInclNUM })

		(cAlias)->( dbSkip() )
	End
	(cAlias)->(dbCloseArea())
return aDocs
//-------------------------------------------------------------------
/*/{Protheus.doc} getAnexo
Fun��o respons�vel por identificar qual anexo est� sendo utilizado para instanciar a classe

@param cEntidade    - Alias da entidade
@param cCodEnt      - Codigo da entidade

@author SIGAJURI
@since 24/07/2019

/*/
//-------------------------------------------------------------------
Static Function getAnexo(cEntidade, cCodEnt,cCodProc)
Local cParam  := AllTrim(SuperGetMv('MV_JDOCUME',,'2'))
Local oAnexo  := Nil
Local nIndice := 1

Default cEntidade := ""
Default cCodEnt   := ""
Default cCodProc  := ""

	If cEntidade == 'NT3' //indice correspondente a FILIAL + COD no caso de despesa � o 2
		nIndice := 2
	EndIf
	
	Do Case
		Case cParam == '1'
			oAnexo := TJurAnxWork():New("WorkSite", cEntidade, xFilial(cEntidade), cCodEnt, nIndice,cCodProc) //"WorkSite"
		Case cParam == '2'
			oAnexo := TJurAnxBase():NewTHFInterface(cEntidade, cCodEnt, cCodProc) //"Base de Conhecimento"
		Case cParam == '3'
			oAnexo := TJurAnxFluig():New("Documentos em Destaque - Fluig", cEntidade, xFilial(cEntidade), cCodEnt, nIndice, .F. ) //"Documentos em Destaque - Fluig"
	EndCase


return oAnexo


//-------------------------------------------------------------------
/*/{Protheus.doc} DELETE DeleteDoc
Deleta documentos anexados.

@since 17/07/2019

@param codDoc     - Codigo do documento para pesquisa de Doc especifica
@param entidade    - Alias da entidade
@param codEntidade - Codigo da entidade
@example [Sem Opcional] GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/

/*/
//-------------------------------------------------------------------
	WSMETHOD DELETE DeleteDoc PATHPARAM codDoc, nomeEnt, codEntidade, codProc WSREST JURLEGALPROCESS
	Local cCodDoc    := Self:codDoc
	Local oResponse  := JsonObject():New()
	Local lRet       := {}
	Local aCodNUM    := StrToArray( cCodDoc, ',' )
	Local nI         := 0
	Local nIndexJSon := 0
	Local cEntidade  := Self:nomeEnt
	Local cCodEnt    := Self:codEntidade
	Local cNameDoc   := ""
	local cCodProc   := Self:codProc

	oResponse['operation'] := "DeleteDocs"
	Self:SetContentType("application/json")
	oResponse['attachments'] := {}

	for nI := 1 to Len(aCodNUM)
		cNameDoc := AllTrim(JurGetDados("NUM", 1, xFilial("NUM") + AllTrim(aCodNUM[nI]), "NUM_DESC"))
		lRet := deleteDocs(aCodNUM[nI], cEntidade, cCodEnt, cCodProc)

		nIndexJSon++
		Aadd(oResponse['attachments'], JsonObject():New())

		If lRet
			oResponse['attachments'][nIndexJSon]['isDelete']    := .T.
			oResponse['attachments'][nIndexJSon]['codDocument'] := aCodNUM[nI]
			oResponse['attachments'][nIndexJSon]['nameDocument'] := JConvUTF8(cNameDoc)
		EndIf
	Next nI

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} listDocs
Fun��o respons�vel por chamar a fun��o da classe de anexo para deletar os documentos

@author SIGAJURI
@since 24/07/2019
@param cCodDoc     - Codigo do documento para pesquisa de Doc especifica
@param entidade    - Alias da entidade
@param codEntidade - Codigo da entidade
/*/
//-------------------------------------------------------------------
Static Function deleteDocs(cCodDoc, cEntidade, cCodEnt,codProc)
	Local lRet    := .F.
	Local cParam  := AllTrim(SuperGetMv('MV_JDOCUME',,'2'))
	Local oAnexo  := getAnexo(cEntidade, cCodEnt,codProc)

	Do Case
	Case cParam == '2' //Base de Conhecimento
		lRet :=  oAnexo:DeleteNUM(cCodDoc)
	Case cParam == '3' //Fluig
		lRet :=  oAnexo:Excluir(cCodDoc)
	EndCase

	FwFreeObj(oAnexo)
	oAnexo := Nil


return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} GET DwnlFile
Efetua o download do anexo selecionado

@author SIGAJURI
@since 29/07/2019
@param cPathDown - Codigo da entidade

@example [Sem Opcional] GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/anexo/0000000001

/*/
//-------------------------------------------------------------------
WSMETHOD GET DwnlFile WSRECEIVE pathArq WSREST JURLEGALPROCESS
Local oResponse := JsonObject():New()
Local lRet      := .T.
Local cPathArq  := Self:pathArq
Local cNomeArq  := ""

	If File(cPathArq)
		cNomeArq := SubStr(cPathArq,Rat("/",cPathArq)+1)
		cNomeArq := SubStr(cNomeArq,Rat("\",cNomeArq)+1)

		Self:SetContentType("application/json")
		oResponse['operation'] := "DownloadFiles"
		oResponse['attachments'] := {}

		Aadd(oResponse['attachments'], JsonObject():New())
		Atail(oResponse['attachments'])['namefile'] := JConvUTF8(cNomeArq)
		Atail(oResponse['attachments'])['fileurl']  := JConvUTF8("/juri/JURLEGALPROCESS/downloadFile?"+cNomeArq)
		Atail(oResponse['attachments'])['filedata'] := DownloadBase(cPathArq)

		Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

	Else
		SetRestFault(400,EncodeUTF8(STR0024))//"Arquivo n�o existe."
		lRet := .F.
	Endif

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} GET DownloadFile
Efetua o download do anexo selecionado

@author SIGAJURI
@since 29/07/2019
@param codDoc      - Codigo do documento
@param entidade    - Alias da entidade
@param codEntidade - Codigo da entidade

@example [Sem Opcional] GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/anexo/0000000001

/*/
//-------------------------------------------------------------------
	WSMETHOD GET DownloadFile PATHPARAM codDoc WSRECEIVE nomeEnt, codEntidade WSREST JURLEGALPROCESS
	Local oResponse   := JsonObject():New()
	Local nIndexJSon  := 0
	Local cContentArq := ""
	Local nI          := 0
	Local cEntidade   := Self:nomeEnt
	Local cCodEnt     := Self:codEntidade
	Local oAnexo      := getAnexo(cEntidade, cCodEnt)
	Local cNumero     := ""
	Local aCodNUM     := StrToArray( Self:codDoc, ',' )
	Local cParam      := AllTrim(SuperGetMv('MV_JDOCUME',,'2'))
	Local lRet        := .T.
	Local cNomArq     := ""
	Local cPastaDown  := "/thf/"
	Local lExisPasta  := ExistDir(cPastaDown)
	Local nRetPasta   := 0

	// Verifica se a pasta thf existe na protheus_data
	If !lExisPasta
		nRetPasta :=  MakeDir(cPastaDown) // Cria a pasta thf

		If nRetPasta != 0
			Conout(STR0051 + cValToChar(FError())) // N�o foi poss�vel criar a pasta /thf. Erro:
		Endif
	Endif

	Self:SetContentType("application/json")
	oResponse['operation'] := "DownloadFiles"
	oResponse['attachments'] := {}

	For nI := 1 to Len(aCodNUM)
		cNomArq := Alltrim(JurGetDados("NUM",1,xFilial("NUM") + aCodNUM[nI]	,"NUM_DESC"))

		If Empty(cNomArq)
			cNomArq := Alltrim(JurGetDados("NUM",1,xFilial("NUM") + aCodNUM[nI]	,"NUM_DOC"))+AllTrim(JurGetDados("NUM",1,xFilial("NUM") + aCodNUM[nI]	,"NUM_EXTEN"))
		EndIf

		cNumero := JurGetDados("NUM",1,xFilial("NUM") + aCodNUM[nI]	,"NUM_NUMERO")

		Do Case
		Case cParam == '2' //Base de Conhecimento

			oAnexo:lSalvaTemp := .F.
			If oAnexo:Exportar("",cPastaDown,cNumero,cNomArq)
				nIndexJSon++
				Aadd(oResponse['attachments'], JsonObject():New())

				cContentArq := "/juri/JURLEGALPROCESS/anexo/" + AllTrim(aCodNUM[nI])

				oResponse['attachments'][nIndexJSon]['namefile'] := JConvUTF8(cNomArq)
				oResponse['attachments'][nIndexJSon]['fileurl']  := JConvUTF8(cContentArq)
				oResponse['attachments'][nIndexJSon]['filedata'] := encode64(DownloadBase(cPastaDown+cNomArq))

				Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))
			Else
				lRet:= .F.
			EndIf

		Case cParam == '3' //Fluig

			nIndexJSon++
			Aadd(oResponse['attachments'], JsonObject():New())

			cContentArq := oAnexo:Abrir(.F.,cNumero)

			If !Empty(cContentArq)
				oResponse['attachments'][nIndexJSon]['namefile'] := JConvUTF8(cNomArq)
				oResponse['attachments'][nIndexJSon]['fileurl']  := JConvUTF8(cContentArq)
				oResponse['attachments'][nIndexJSon]['filedata']  :=  ""
			EndIf

		EndCase
	Next nI

	If cParam == '3'
		Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} DownloadBase(cArq)
Efetua o download do arquivo

@author SIGAJURI
@since 29/07/2019
@param cArq       - Caminho do arquivo

/*/
//-------------------------------------------------------------------

Function DownloadBase(cArq)

	Local cBuffer := ""
	Local nHandle
	Local nBytes := 0
	Local lRet := .F.
	Local cFilecontent := ""

	If File(cArq)
		nHandle := FOPEN(cArq)  // Grava o ID do arquivo

		If nHandle > -1
			//Le os bytes do arquivo que ser� enviado.
			While (nBytes := FREAD(nHandle, @cBuffer, 1024)) > 0      // L� os bytes
				cFileContent += cBuffer
			EndDo

			FCLOSE(nHandle)
			lRet := .T.
		Else
			conout(STR0023) //"Erro ao ler o arquivo"
			SetRestFault(500, STR0024) //"Arquivo n�o existe."
		EndIf
	Else
		conout(STR0024) //"Arquivo n�o existe."
		SetRestFault(404, STR0024) //"Arquivo n�o existe."
	EndIf

Return cFileContent
//-------------------------------------------------------------------
/*/{Protheus.doc} POST UploadFile
Efetua o upload dos anexos

@author SIGAJURI
@since 16/08/2019
@param

@example [Sem Opcional] POST -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/anexo

/*/
//-------------------------------------------------------------------

	WSMETHOD POST UploadFile WSREST JURLEGALPROCESS

	Local oRequest    := JSonObject():New()
	Local cBody       := Self:GetContent()
	Local nTamArquivo := 0
	Local cFileName   := ""
	Local nHDestino   := 0
	Local cContent    := HTTPHeader("content-type")
	Local cLimite     := SubStr(cContent,At("boundary=",cContent)+9)
	Local cFile       := "" //conte�do do arquivo
	Local cData       := "" //conte�do extra
	Local cCajuri     := ""
	Local cNomeEntid  := ""
	Local cCodEnt     := ""
	Local cArquivo    := ""
	Local cSpool      := "\spool\"

	cFileName := SubStr(cBody,At('filename="',cBody)+10, At('"',SubStr(cBody,At('filename="',cBody)+10,200))-1)
	cFileName := decodeUTF8(AllTrim(cFileName))

	// Tratamento para S.O Linux
	If "Linux" $ GetSrvInfo()[2]
		cSpool := StrTran(cSpool,"\","/")
	Endif

	cArquivo  := cSpool + cFileName
	cFile := SubStr(SubStr(SubStr(cBody,;
		At("Content-Type:",;
		cBody)+12),;
		At(Chr(10),;
		SubStr(cBody,At("Content-Type:",cBody)+12))+3),;
		1,;
		At(cLimite,SubStr(SubStr(cBody,At("Content-Type:",cBody)+12),;
		At(Chr(10),;
		SubStr(cBody,At("Content-Type:",cBody)+12))+3))-5)

	cData := SubStr(cBody,At('Content-Disposition: form-data; name="data"',cBody)+46)
	cData := REPLACE(SubStr(cData,1,At(cLimite,cData)-5),CHR(10),'')

	oRequest:fromJson(cData)
	nTamArquivo := Len(cFile)

	nHDestino := FCREATE(cArquivo)
	nBytesSalvo := FWRITE(nHDestino, cFile,nTamArquivo)
	FCLOSE(nHDestino)

	cNomeEntid := oRequest['entidade']
	cCajuri   := oRequest['cajuri']
	cCodEnt   := oRequest['codEntidade']

	Do case
	Case cNomeEntid == "NT2"
		cCodEnt := cCajuri + cCodEnt
	Case cNomeEntid == "NT3"
		cCodEnt := cCajuri + cCodEnt
	Case cNomeEntid == "NSY"
		cCodEnt := cCodEnt + cCajuri
	End Case

	J026Anexar(cNomeEntid, xFilial(cNomeEntid), cCodEnt, cCajuri, cArquivo)

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} GrpAprovNT2
Grupo de Aprova��o da Garantia

@author SIGAJURI
@since 29/07/2019
/*/
//-------------------------------------------------------------------
	WSMETHOD GET GrpAprovNT2 WSREST JURLEGALPROCESS
	Local oResponse  := JsonObject():New()
	Local cQuery     := ""
	Local cAlias     := ""
	Local nIndexJSon := 0

	cQuery := " SELECT FRP_FILIAL, FRP_COD, RD0_CODIGO,RD0_NOME, FRP_LIMMIN, FRP_LIMMAX "
	cQuery +=   " FROM " + RetSqlName("FRP") + " FRP LEFT JOIN " + RetSqlName("RD0") + " RD0 "
	cQuery +=     " ON FRP_USER = RD0_USER "
	cQuery +=  " WHERE FRP.D_E_L_E_T_ = ' '"
	cQuery +=    " AND RD0.D_E_L_E_T_ = ' '"

	cAlias := GetNextAlias()
	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )

	oResponse['groupApprover'] := {}

	While !(cAlias)->(Eof())
		nIndexJSon++
		Aadd(oResponse['groupApprover'], JsonObject():New())
		oResponse['groupApprover'][nIndexJSon]['filial']   := (cAlias)->FRP_FILIAL
		oResponse['groupApprover'][nIndexJSon]['id']       := (cAlias)->FRP_COD
		oResponse['groupApprover'][nIndexJSon]['codResp']  := (cAlias)->RD0_CODIGO
		oResponse['groupApprover'][nIndexJSon]['approver'] := (cAlias)->RD0_NOME
		oResponse['groupApprover'][nIndexJSon]['minLimit'] := (cAlias)->FRP_LIMMIN
		oResponse['groupApprover'][nIndexJSon]['maxLimit'] := (cAlias)->FRP_LIMMAX
		(cAlias)->( dbSkip() )
	End
	
	(cAlias)->(dbCloseArea())

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))
Return .T.


//-------------------------------------------------------------------
/*/{Protheus.doc} TabGenerica
Busca de Registro na tabela gen�rica (SX5)

@author willian.kazahaya
@since 27/08/2019
/*/
//-------------------------------------------------------------------
	WSMethod GET TabGenerica PATHPARAM codTabela WSRECEIVE searchKey, chaveTab WSREST JURLEGALPROCESS
	Local oResponse  := JsonObject():New()
	Local cQuery     := ""
	Local cAlias     := ""
	Local cCodTab    := Self:codTabela
	Local cSearchKey := Self:searchKey
	Local cChaveTab  := Self:chaveTab
	Local nIndexJSon := 0

	cQuery := " SELECT SX5.X5_FILIAL, SX5.X5_TABELA, SX5.X5_CHAVE, SX5.X5_DESCRI, SX5.X5_DESCSPA, SX5.X5_DESCENG, SX5.R_E_C_N_O_ REC "
	cQuery += " FROM " + RetSqlName("SX5") + " SX5 "
	cQuery += " WHERE SX5.X5_TABELA = '" + Alltrim(cCodTab) + "' "

	If !Empty(cSearchKey)
		cQuery += " AND SX5.X5_DESCRI = '%" + AllTrim(cSearchKey) + "%'"
	ElseIf !Empty(cChaveTab)
		cQuery += " AND SX5.X5_CHAVE = '" + AllTrim(cChaveTab) + "'"
	Endif

	cAlias := GetNextAlias()
	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )

	oResponse['result'] := {}

	While !(cAlias)->(Eof())
		nIndexJSon++
		Aadd(oResponse['result'], JsonObject():New())
		oResponse['result'][nIndexJSon]['filial']    := (cAlias)->X5_FILIAL
		oResponse['result'][nIndexJSon]['tabela']    := (cAlias)->X5_TABELA
		oResponse['result'][nIndexJSon]['chave']     := (cAlias)->X5_CHAVE
		oResponse['result'][nIndexJSon]['descricao'] := (cAlias)->X5_DESCRI
		oResponse['result'][nIndexJSon]['descrispa'] := (cAlias)->X5_DESCSPA
		oResponse['result'][nIndexJSon]['descrieng'] := (cAlias)->X5_DESCENG
		(cAlias)->( dbSkip() )
	End

	(cAlias)->(dbCloseArea())

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))
Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} SysParam
Busca de Par�metros da SX6

@author willian.kazahaya
@since 27/08/2019
/*/
//-------------------------------------------------------------------
	WSMethod GET SysParam PATHPARAM codParam WSREST JURLEGALPROCESS
	Local oResponse  := JsonObject():New()
	Local cCodParam  := AllTrim(Self:codParam)

	If Len(cFilant) < 8
		cFilant := PadR(cFilAnt,8)
	EndIf

	oResponse['sysParam'] := JsonObject():New()

	oResponse['sysParam']['name']  := cCodParam
	oResponse['sysParam']['value'] := GetParam(cCodParam)

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))
Return .T.

Static Function GetParam(cParam)
Return SuperGetMv(cParam)


//-------------------------------------------------------------------
/*/{Protheus.doc} Favorite
Consulta se o processo foi favoritado

@return .T.

@since 04/09/2019

@example [Sem Opcional] GET -> http://localhost:12173/rest/JURLEGALPROCESS/favorite
/*/
//-------------------------------------------------------------------
	WSMETHOD GET Favorite WSRECEIVE url WSREST JURLEGALPROCESS

	Local oResponse := JSonObject():New()
	Local cCajuri   := ""
	Local cProFav   := ""
	Local cFilPro   := ""
	Local cQuery    := ""
	Local lFavorito := .F.
	Local cAlias    := GetNextAlias()

	cProFav   := SubStr(Self:url, 11)

	cCajuri   := substr(cProFav,Rat("/",cProFav)+1)
	cFilPro   := substr(cProFav,0,Rat("/",cProFav)-1)

	cQuery := " SELECT 1 FROM " + RetSqlName('O0V')
	cQuery += " WHERE O0V_CAJURI = '" + cCajuri + "' "
	cQuery += " AND O0V_FILCAJ = '" + cFilPro + "'"
	cQuery += " AND O0V_USER = '" + __CUSERID + "'"
	cQuery += " AND D_E_L_E_T_ = ' '"

	cQuery := ChangeQuery(cQuery)
	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )

	If !(cAlias)->(Eof())
		lFavorito := .T.
	EndIf
	(cAlias)->(dbCloseArea())

	oResponse['isFavorite'] := lFavorito
	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.


//-------------------------------------------------------------------
/*/{Protheus.doc} Favorites
Inclui/ Exclui registros na tabela de favoritos

@return lRet - inclus�o/ exclus�o realizadas ou n�o

@since 04/09/2019

@example [Sem Opcional] POST -> http://localhost:12173/rest/JURLEGALPROCESS/favorite
/*/
//-------------------------------------------------------------------

	WSMETHOD POST Favorite WSREST JURLEGALPROCESS
	Local aArea      := GetArea()
	Local aAreaO0V   := O0V->( GetArea() )
	Local lRet       := .T.
	Local oRequest   := JSonObject():New()
	Local oResponse  := JsonObject():New()
	Local cBody      := Self:GetContent()
	Local cCajuri    := ""
	Local cUserId    := ""
	Local cProFav    := ""
	Local cFilPro    := ""

	Local lFavorito  := nil
	Local oModel     := nil

	oRequest:fromJson(cBody)
	lFavorito := oRequest['isFavorite']

	cProFav   := oRequest['url']
	cProFav   := SubStr(cProFav, 11)

	cCajuri   := substr(cProFav,Rat("/",cProFav)+1)
	cFilPro   := substr(cProFav,0,Rat("/",cProFav)-1)

	cUserId   := oRequest['params']['userId']

	oModel   := FWLOADMODEL("JURA269")
	If lFavorito
		oModel:SetOperation(MODEL_OPERATION_INSERT)
		oModel:Activate()
		oModel:SetValue('O0VMASTER','O0V_FILCAJ',cFilPro)
		oModel:SetValue('O0VMASTER','O0V_CAJURI',cCajuri)
		oModel:SetValue('O0VMASTER','O0V_USER',cUserId)

		If !( oModel:VldData() ) .Or. !( oModel:CommitData() )
			lRetorno := .F.
		EndIf

		oModel:DeActivate()
		oModel:Destroy()
	Else
		dbSelectArea("O0V")
		O0V->( dbSetOrder( 1 ) )	//O0V_FILCAJ, O0V_CAJURI, O0V_USER
		If O0V->( dbSeek( cFilPro + cCajuri + cUserId) )
			oModel:SetOperation(MODEL_OPERATION_DELETE)
			oModel:Activate()
			If !( oModel:VldData() ) .Or. !( oModel:CommitData() )
				lRetorno := .F.
			EndIf

			oModel:DeActivate()
			oModel:Destroy()
		EndIf
		O0V->(dbCloseArea())
	EndIf


	If lRet
		oResponse['isFavorite'] := lFavorito
		Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))
	EndIf

	RestArea(aAreaO0V)
	RestArea(aArea)

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JSumDesp(cCajuri)
Soma os valores das despesas

@return nRet - Valor Sumarizado

@since 06/09/2019
/*/
//-------------------------------------------------------------------

Static Function JSumDesp(cCajuri)
	Local nRet    := 0
	Local cQuery  := ""
	Local cQrySel := ""
	Local cQryFrm := ""
	Local cQryWhr := ""
	Local cAlias  := GetNextAlias()

	cQrySel := " SELECT SUM(NT3_VALOR) VALOR "
	cQryFrm := " FROM " + RetSqlName("NT3")
	cQryWhr := " WHERE NT3_CAJURI = '" + cCajuri + "'"
	cQryWhr += " AND NT3_FILIAL = '" + xFilial("NT3") + "' "
	cQryWhr += " AND D_E_L_E_T_ = ' ' "

	cQuery := ChangeQuery(cQrySel + cQryFrm + cQryWhr)

	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )

	If (cAlias)->(!Eof())
		nRet := (cAlias)->(VALOR)
	EndIf
	(cAlias)->(DbCloseArea())
Return nRet

//-------------------------------------------------------------------
/*/{Protheus.doc} GET ListFields
Listagem de campos para Pesquisa Avan�ada

Retorna um array com as seguintes informa��es:
	table- Nome da tabela
	field- Nome do campo
	title- T�tulo do campo
	type- Tipo do campo (N- Num�rico; D- Data; C- Caractere; M-Memo; Combo; F3 e MULTI- F3 M�ltiplo )
	f3func- Fun��o utilizada no F3 do Campo
	comboOptions- Op��es do combo

@since 04/09/19

@param tpPesq - Informa o tipo de pesquisa para busca dos campos

@example [Sem Opcional] GET -> http://127.0.0.1:12173/rest/JURLEGALPROCESS/fields

/*/
//-------------------------------------------------------------------
	WSMETHOD GET ListFields WSRECEIVE searchKey WSREST JURLEGALPROCESS
	Local cSearchKey := Self:searchKey
	Local aArea      := GetArea()
	Local cAliasNVH  := GetNextAlias()
	Local oResponse  := JsonObject():New()
	Local aAux       := {}
	Local cQuery     := ""
	Local nIndexJSon := 0
	Local nI         := 0

	oResponse['result'] := {}

	// Traz informa��es b�sicas dos campos

	cQuery := "SELECT DISTINCT NVH_TABELA, NVH_CAMPO, NVH_DESC, NVH_CHAVE, NVH_LABEL, NVH_COD "
	cQuery +=  " FROM " + RetSqlName("NVH")
	cQuery += " WHERE D_E_L_E_T_ = ' ' AND NVH_FILIAL = '" + xFilial("NVH") + "' AND NVH_TPPESQ = '1' "

	If cSearchKey != "" .and. cSearchKey != Nil
		cQuery += "AND LOWER(NVH_DESC) LIKE LOWER('%" + cSearchKey + "%')"
	EndIf

	cQuery += " ORDER BY 1,3 "
	cQuery := ChangeQuery(cQuery, .F.)

	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAliasNVH, .F., .F. )

	//Monta o Json de Retorno
	While !(cAliasNVH)->(Eof())
		nIndexJSon++
		Aadd(oResponse['result'], JsonObject():New())
		oResponse['result'][nIndexJSon]['table'] := AllTrim((cAliasNVH)->NVH_TABELA)
		oResponse['result'][nIndexJSon]['field'] := AllTrim((cAliasNVH)->NVH_CAMPO)
		oResponse['result'][nIndexJSon]['title'] := JurEncUTF8(AllTrim((cAliasNVH)->NVH_DESC))
		oResponse['result'][nIndexJSon]['codigo'] := JurEncUTF8(AllTrim((cAliasNVH)->NVH_COD))

		If (!Empty(GetSx3Cache((cAliasNVH)->NVH_CAMPO,"X3_F3")) .And. !Empty((cAliasNVH)->NVH_CHAVE) .And. !Empty((cAliasNVH)->NVH_LABEL))
			oResponse['result'][nIndexJSon]['type'] := "F3"
			oResponse['result'][nIndexJSon]['f3fields'] := AllTrim((cAliasNVH)->NVH_CHAVE) + "-" + AllTrim((cAliasNVH)->NVH_LABEL)
		ElseIf !Empty(GetSx3Cache((cAliasNVH)->NVH_CAMPO,"X3_CBOX"))
			oResponse['result'][nIndexJSon]['type'] := "COMBO"

			aAux := StrTokArr(JurEncUTF8(AllTrim(GetSx3Cache((cAliasNVH)->NVH_CAMPO,"X3_CBOX"))),";")

			// Traz os dados para o combo
			For nI := 1 To Len(aAux)
				aAux[nI] := StrTokArr(aAux[nI],"=")
			Next

			oResponse['result'][nIndexJSon]['comboOptions'] := aAux
		Else
			oResponse['result'][nIndexJSon]['type'] := AllTrim(GetSx3Cache((cAliasNVH)->NVH_CAMPO,"X3_TIPO"))
		EndIf

		aAux := {}

		(cAliasNVH)->( dbSkip() )
	End

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

	(cAliasNVH)->(dbCloseArea())
	RestArea(aArea)

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} GET GetListF3
Listagem de Dados para campos F3

Retorna um array com as seguintes informa��es:
	fildid- C�digo do �tem
	filddesc- Descri��o do �tem

@since 04/09/19

@param cCampo - Descri��o do campo para busca de �tens

@example [Sem Opcional] GET -> http://127.0.0.1:12173/rest/JURLEGALPROCESS/f3list/NSP_COD-NSP_DESC

/*/
//-------------------------------------------------------------------
	WSMETHOD GET GetListF3 PATHPARAM campof3 WSRECEIVE searchKey WSREST JURLEGALPROCESS
	Local aArea      := GetArea()
	Local cAlias     := GetNextAlias()
	Local oResponse  := JsonObject():New()
	Local cCampo     := Self:campof3
	Local aExtras    := {"",""}
	Local cSearchKey := Self:searchKey
	Local cTabela    := ""
	Local cQuery     := ""
	Local cQrySel    := ""
	Local cQryFrm    := ""
	Local cQryWhr    := ""

	Local cChave     := SubStr(cCampo,1,At("-",cCampo)-1)
	Local cLabel     := SubStr(cCampo,At("-",cCampo)+1,Len(cCampo))
	Local nCount     := 0
	Local nI         := 0
	Local aFldChave  := {}

	cTabela := PadL(SubStr(cCampo,1,At("_",cCampo)-1),3,"S")
	oResponse['f3Options'] := {}

	If (cChave == "NVE_NUMCAS")
		aFldChave := {"NVE_CCLIEN", "NVE_LCLIEN", "NVE_NUMCAS"}
	Else
		aFldChave := {cChave}
	EndIf

	If (cTabela != "SSS")
		cQrySel := "SELECT DISTINCT "

		For nI := 1 to Len(aFldChave)
			cQrySel += aFldChave[nI] + ","
		Next

		cQrySel +=  cLabel
		cQryFrm := " FROM " + RetSqlName(cTabela) + " "  + cTabela
		cQryWhr := " WHERE D_E_L_E_T_ = ' ' "

		If (cSearchKey != Nil .And. cSearchKey != "")
			cQryWhr += " AND UPPER(" + cLabel + ") Like UPPER('%" + cSearchKey  + "%')"
		EndIf

		aExtras := ExtraFilt(cChave, cTabela)

		// From adicional
		If !Empty(aExtras[1])
			cQryFrm += aExtras[1]
		EndIf

		// Where adicional
		If !Empty(aExtras[2])
			cQryWhr += aExtras[2]
		EndIf

		cQuery := ChangeQuery(cQrySel + cQryFrm + cQryWhr, .F.)
		DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )

		While !(cAlias)->(Eof()) .And. nCount <= 20
			nCount++

			Aadd(oResponse['f3Options'], JsonObject():New())

			cValChave := ""
			For nI := 1 to Len(aFldChave)
				cValChave += AllTrim(&("(cAlias)->" + aFldChave[nI]))
			Next

			oResponse['f3Options'][nCount]['value'] := cValChave
			oResponse['f3Options'][nCount]['label'] := JurEncUTF8(AllTrim(&("(cAlias)->"+cLabel)))

			(cAlias)->( dbSkip() )
		End
		(cAlias)->(dbCloseArea())
	EndIf

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))


	RestArea(aArea)

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} POST leInicial
Recebe uma inicial em pdf via upload, faz o tratamento e devolve o Json
com as informa��es obtidos.

@author SIGAJURI
@since 16/08/2019
@param

@example [Sem Opcional] POST -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/inicial

/*/
//-------------------------------------------------------------------
	WSMETHOD POST leInicial WSREST JURLEGALPROCESS
	Local cArquivo  := ""
	Local oJson := JSonObject():New()
	Local lret := .t.

	Self:SetContentType("application/json")

	If len(cFilant) < 8
		cFilant := PadR(cFilAnt,8)
	EndIf

	cArquivo := recebeFile(Self:GetContent(), HTTPHeader("content-type"))

	if !Empty(cArquivo)
		oJson := J268JSON(cArquivo)
		oJson['data'][1]['inicialPDF'] := cArquivo
		Self:SetResponse(FWJsonSerialize(oJson, .F., .F., .T.))
	else
		lret := .F.
	Endif

Return lret


//-------------------------------------------------------------------
/*/{Protheus.doc} recebeFile(cBody,cContent, oData)
Faz o recebimento do arquivo enviado via download. usa o conte�do do body
da requisi��o e o conte�do do header enviado via cContent.
A vari�vel oData recebe o objeto Json que ser� preenchido caso tenham
informa��es adicionais no body.

@author SIGAJURI
@since 29/07/2019
@param cBody       - Body da requisi��o
@param cContent    - Conte�do do header Content-Type
@param oData    - Objeto Json que deve receber o conte�do adicional do body.

/*/
//-------------------------------------------------------------------
Static Function recebeFile(cBody,cContent, oData)
	Local nTamArquivo
	Local cFileName := ""
	Local nHDestino
	Local cLimite   := SubStr(cContent,At("boundary=",cContent)+9)
	Local cFile     := "" //conte�do do arquivo
	Local cData     := "" //conte�do extra
	Local cArquivo  := ""

	Default oData  := JSonObject():New()

	cFileName := SubStr(cBody,At('filename="',cBody)+10, At('"',SubStr(cBody,At('filename="',cBody)+10,200))-1)
	cFileName := decodeUTF8(AllTrim(cFileName))
	cArquivo  := "\spool\" + cFileName

	cFile := SubStr(SubStr(SubStr(cBody,;
		At("Content-Type:",;
		cBody)+12),;
		At(Chr(10),;
		SubStr(cBody,At("Content-Type:",cBody)+12))+3),;
		1,;
		At(cLimite,SubStr(SubStr(cBody,At("Content-Type:",cBody)+12),;
		At(Chr(10),;
		SubStr(cBody,At("Content-Type:",cBody)+12))+3))-5)

	cData := SubStr(cBody,At('Content-Disposition: form-data; name="data"',cBody)+46)
	if !Empty(cData)
		cData := REPLACE(SubStr(cData,1,At(cLimite,cData)-5),CHR(10),'')

		oData:fromJson(cData)
	Endif
	nTamArquivo := Len(cFile)

	nHDestino := FCREATE(cArquivo)
	nBytesSalvo := FWRITE(nHDestino, cFile,nTamArquivo)
	FCLOSE(nHDestino)

Return cArquivo

//-------------------------------------------------------------------
/*/{Protheus.doc} ExtraFilt(cField, cTabela)
Gera o filtro extra para o select

@Param cField - Campo
@Param cTabela - Tabela

@return [1] Clausula From extra
		[2] Clausula Where Extra

@author SIGAJURI
@since 27/09/2019
/*/
//-------------------------------------------------------------------
Static Function ExtraFilt(cField, cTabela)
	Local cQryWhrExt := ""
	Local cQryFrmExt := ""
	Default cTabela := ""

	Do case
	Case cField == "RD0_SIGLA"
		cQryWhrExt := " AND RD0_TPJUR = '1' "
	End Case

Return {cQryFrmExt, cQryWhrExt}

//-------------------------------------------------------------------
/*/{Protheus.doc} listVara
retona a vara vinculada ao foro

@param cCodForo   - c�digo do foro
@return aVara     [1] Filial da Vara
		          [2] C�d tribunal (localiza��o 3�. n�vel)
		          [3] Descri��o do tribunal (localiza��o 3�. n�vel)
		          [4] C�digo do Foro (localiza��o 2�. n�vel)

@author SIGAJURI
@since 27/08/19

@example [Sem Opcional] GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/foro/00001

/*/
//-------------------------------------------------------------------
static Function listVara(cCodForo, cSearchKey, cSrcVara)
Local cQuery := ''
Local cAlias := GetNextAlias()
Local aVara  := {}

Default cSearchKey := ''
Default cSrcVara   := ''

	cQuery := "SELECT NQE_FILIAL, NQE_COD, NQE_DESC, NQE_CLOC2N FROM "+ RetSqlName("NQE")+ " NQE "
	cQuery += "WHERE NQE.NQE_CLOC2N = '" + cCodForo + "'"
	cQuery +=        " AND NQE.NQE_FILIAL = '"+xFilial("NQE") + "' AND NQE.D_E_L_E_T_ = ' '"
	
	If !Empty(cSearchKey)
		cQuery +=    " AND Upper(NQE_DESC) LIKE  '%" + Upper(cSearchKey) + "%'"
	EndIf
	
	If !Empty(cSrcVara)
		cQuery +=    " AND Upper(NQE_COD) = '" + Upper(cSrcVara) + "'"
	EndIf

	cQuery := ChangeQuery(cQuery)

	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )

	While !(cAlias)->(Eof())
		aAdd( aVara, { (cAlias)->NQE_FILIAL,(cAlias)->NQE_COD,(cAlias)->NQE_DESC, (cAlias)->NQE_CLOC2N} )
		(cAlias)->( dbSkip() )
	End
	(cAlias)->(dbCloseArea())

return aVara

//-------------------------------------------------------------------
/*/{Protheus.doc} GET LtFuncionarios
Lista de Funcionarios - GPEA010

@since 27/08/2019
@version 1.0

@param page      - Numero da p�gina
@param pageSize  - Quantidade de itens na p�gina
@param nomeFunc  - palavra a ser pesquisada no Nome do documento
@param searchKey - C�digo do funcion�rio

@example  GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/funcionarios
/*/
//-------------------------------------------------------------------
	WSMETHOD GET LtFuncionarios WSRECEIVE page, pageSize, searchKey, nomeFunc WSREST JURLEGALPROCESS
	Local oResponse  := Nil
	Local nPage      := Self:page
	Local nPageSize  := Self:pageSize
	Local cSearchKey := Self:searchKey
	Local cNomeFunc  := Self:nomeFunc

	Self:SetContentType("application/json")

		oResponse := JWsFuncion(nPage, nPageSize, cSearchKey, cNomeFunc )

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} JWsFuncion(nPage, nPageSize, cSearchKey, cNomeFunc)
Busca os Funcionarios - SRA

@since 27/08/2019
@version 1.0

@param page       - Numero da p�gina
@param pageSize   - Quantidade de itens na p�gina
@param cSearchKey - palavra a ser pesquisada no c�digo de matr�cula do funcion�rio.
@param cNomeFunc  - palavra a ser pesquisada no Nome do funcion�rio.
/*/
//-------------------------------------------------------------------
Function JWsFuncion(nPage, nPageSize, cSearchKey, cNomeFunc)
Local oResponse  := JSonObject():New()
Local cAliasSRA  := GetNextAlias()
Local aArea 	 := GetArea()
Local nIndexJSon := 0
Local nQtdReg    := 0
Local cQuery     := ""

Default nPage      := 1
Default nPageSize  := 10
Default cSearchKey := ""
Default cNomeFunc  := ""

	cQuery :=  " SELECT RA_FILIAL, RA_MAT, RA_NOME, RA_CIC "
	cQuery +=  " FROM " + RetSqlName("SRA") + " SRA "
	cQuery +=  " WHERE SRA.D_E_L_E_T_ = ' ' "

	If !Empty(cSearchKey)
		cQuery += " AND RA_FILIAL || RA_MAT = '" + cSearchKey + "' "
	EndIf

	If!Empty(cNomeFunc)
		cQuery += " AND UPPER(RA_NOME) LIKE '%" + Upper(cNomeFunc) + "%'" 
	EndIf

	cQuery := ChangeQuery(cQuery)
	dbUseArea( .T., "TOPCONN", TcGenQry( ,, cQuery ) , cAliasSRA, .T., .F. )

	oResponse['funcionarios'] := {}

	nQtdRegIni := ((nPage-1) * nPageSize)

	// Define o range para inclus�o no JSON
	nQtdRegFim := (nPage * nPageSize)
	nQtdReg    := 0

	While !(cAliasSRA)->(Eof())

		nQtdReg++

		// Verifica se o registro est� no range da pagina
		If (nQtdReg > nQtdRegIni .AND. nQtdReg <= nQtdRegFim)

			nIndexJSon++
			Aadd(oResponse['funcionarios'], JsonObject():New())

			oResponse['funcionarios'][nIndexJSon]['filial']    := EncodeUTF8((cAliasSRA)->RA_FILIAL)
			oResponse['funcionarios'][nIndexJSon]['matricula'] := EncodeUTF8((cAliasSRA)->RA_MAT)
			oResponse['funcionarios'][nIndexJSon]['nome']      := JConvUTF8((cAliasSRA)->RA_NOME)
			oResponse['funcionarios'][nIndexJSon]['cpf']       := JConvUTF8((cAliasSRA)->RA_CIC)
		EndIf

		(cAliasSRA)->( dbSkip())
	End

	(cAliasSRA)->( DbCloseArea() )
	RestArea(aArea)

Return oResponse

//-------------------------------------------------------------------
/*/{Protheus.doc} GET - DtRecebidas
Lista de Distribui��es recebidas - JURA219

@since 06/09/2019
@version 1.0

@param page - Numero da p�gina
@param pageSize - Quantidade de itens na p�gina
@param cSearchKey  - palavra a ser pesquisada no Nome do documento

@example  GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/funcionarios
/*/
//-------------------------------------------------------------------
	WSMETHOD GET DtRecebidas PATHPARAM tipoDistr WSRECEIVE page, pageSize, searchKey WSREST JURLEGALPROCESS
	Local oResponse  := Nil
	Local nPage      := Self:page
	Local nPageSize  := Self:pageSize
	Local cSearchKey := Self:searchKey
	Local tipoDistr  := Self:tipoDistr

	Self:SetContentType("application/json")

	oResponse := JSeekPrcss(tipoDistr, nPage, nPageSize, cSearchKey )

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} GET - J219Distr
Busca informa��es de uma determinada Distribui��o com status Recebida
(JURA219)

@since 06/09/2019
@version 1.0

@param page - Numero da p�gina
@param pageSize - Quantidade de itens na p�gina
@param cSearchKey  - palavra a ser pesquisada no Nome do documento

@example  GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/funcionarios
/*/
//-------------------------------------------------------------------
	WSMETHOD GET J219Distr PATHPARAM codDistr WSRECEIVE page, pageSize, searchKey WSREST JURLEGALPROCESS
	Local oResponse  := Nil
	Local cCodDstr   := Self:codDistr

	Self:SetContentType("application/json")

	oResponse := JDistReceb( cCodDstr )

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} JSeekPrcss(tipoDistr,nPage, nPageSize, cSearchKey)
Busca todas as Distribui��es com status 1 - Recebida

@param tipoDistr - Tipo de Distribui��o
@param nPage - Numero da p�gina
@param nPageSize - Quantidade de itens na p�gina
@param cSearchKey  - palavra a ser pesquisada no Nome do documento

@since 	06/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Function JSeekPrcss(tipoDistr,nPage, nPageSize, cSearchKey)

	Local oResponse  := JSonObject():New()
	Local oDadosPro  := JsonObject():New()
	Local cAliasNZZ  := GetNextAlias()
	Local aArea 	 := GetArea()
	Local cQuery     := ""
	Local xConteudo  := ""
	Local nQtdReg    := 0
	Local nIndexJSon := 0

	Default tipoDistr  := '1'
	Default nPage      := 1
	Default nPageSize  := 10
	Default cSearchKey := ""

	oResponse['data'] := {}
	Aadd(oResponse['data'], JsonObject():New())
	oResponse['data'][1]['distribuicoes'] := {}

	cQuery :=  " SELECT NZZ_FILIAL, "
	cQuery +=         " NZZ_COD, "
	cQuery +=         " NZZ_AUTOR AS ATIVO, "
	cQuery +=         " NZZ_REU AS PASSIVO, "
	cQuery +=         " NZZ_TRIBUN AS TRIBUNAL, "
	cQuery +=         " NZZ_NUMPRO "
	cQuery +=  " FROM " + RetSqlName("NZZ") + " NZZ "
	cQuery +=  " WHERE NZZ_FILIAL = '" + xFilial("NZZ") + "' "
	cQuery +=         " AND NZZ_STATUS = '" + tipoDistr + "' "
	If !Empty(cSearchKey)
		cQuery +=     " AND (" + JurFormat("NZZ_NUMPRO", .F.,.T.) + " LIKE '%" + cSearchKey + "%'"
		cQuery +=           " OR " + JurFormat("NZZ_TRIBUN", .F.,.T.) + " LIKE '%" + lower(cSearchKey) + "%'"
		cQuery +=           " OR " + JurFormat("NZZ_AUTOR", .F.,.T.) + " LIKE '%" + lower(cSearchKey) + "%'"
		cQuery +=           " OR " + JurFormat("NZZ_REU", .F.,.T.) + " LIKE '%" + lower(cSearchKey) + "%')"
	EndIf
	cQuery +=         " AND NZZ.D_E_L_E_T_ = ' ' "

	cQuery := ChangeQuery(cQuery)
	cQuery := StrTran(cQuery,",' '",",''")
	
	dbUseArea( .T., "TOPCONN", TcGenQry( ,, cQuery ) , cAliasNZZ, .T., .F. )

	nQtdRegIni := ((nPage-1) * nPageSize)

	// Define o range para inclus�o no JSON
	nQtdRegFim := (nPage * nPageSize)
	nQtdReg    := 0

	While !(cAliasNZZ)->(Eof())
		nQtdReg++

		// Verifica se o registro est� no range da pagina
		If (nQtdReg > nQtdRegIni .AND. nQtdReg <= nQtdRegFim)

			//-- Numero do Processo
			xConteudo := (cAliasNZZ)->NZZ_NUMPRO
			xConteudo := StrTran(xConteudo, "-", "")
			xConteudo := StrTran(xConteudo, ".", "")
			xConteudo := JurEncUTF8(Alltrim(xConteudo))

			nIndexJSon++
			//-- Json com todas as Distribui��es com status "Recebida"
			oDadosPro['codigo']     := (cAliasNZZ)->NZZ_COD
			oDadosPro['descricao']  := JConvUTF8(Alltrim((cAliasNZZ)->ATIVO) + " / " +  Alltrim((cAliasNZZ)->PASSIVO) + " / " + xConteudo  + " / "  +  Alltrim((cAliasNZZ)->TRIBUNAL))
			oDadosPro['NZZ_NUMPRO'] := xConteudo

			aAdd(oResponse['data'][1]['distribuicoes'], oDadosPro )
			oDadosPro := JsonObject():New()
		EndIf

		(cAliasNZZ)->( dbSkip() )
	End

	(cAliasNZZ)->( DbCloseArea() )
	RestArea(aArea)

Return oResponse

//-------------------------------------------------------------------
/*/{Protheus.doc} GET SrchForo
Busca os foros de acordo com a comarca de origem

@since 10/09/19
@param codComarca   - Codigo da Comarca

@example [Sem Opcional] GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/comarca/00001/foros
/*/
//-------------------------------------------------------------------
WSMETHOD GET SrchForo PATHPARAM codComarca WSRECEIVE searchKey, buscaInfo WSREST JURLEGALPROCESS
Local cAliasFor  := GetNextAlias()
Local aArea      := GetArea()
Local cQuery     := ''
Local nIndexJSon := 0
Local cComarca   := Self:codComarca
Local cSrcForo   := Self:buscaInfo
Local cSearchKey := Self:searchKey
Local oResponse  := JsonObject():New()

	cQuery := " SELECT NQC_COD, "
	cQuery +=        " NQC_DESC "
	cQuery += " FROM " + RetSqlName("NQC") + " NQC "
	cQuery += " WHERE NQC.NQC_FILIAL = '" + xFilial("NQC") + "' "
	cQuery +=        " AND NQC.NQC_CCOMAR = '" + cComarca + "' "
	cQuery +=        " AND NQC.D_E_L_E_T_ = ' ' "

	If !Empty(cSearchKey)
		cQuery +=    " AND Upper(NQC_DESC) LIKE '%" + Upper(cSearchKey) + "%' "
	EndIf
	
	If !Empty(cSrcForo)
		cQuery +=    " AND Upper(NQC_COD) = '" + cSrcForo +  "' "
	EndIf

	cQuery := ChangeQuery(cQuery)

	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAliasFor, .F., .F. )


	Self:SetContentType("application/json")
	oResponse['foro'] := {}

	While !(cAliasFor)->(Eof())
		nIndexJSon++

		Aadd(oResponse['foro'], JsonObject():New())

		oResponse['foro'][nIndexJSon]['value'] := (cAliasFor)->NQC_COD
		oResponse['foro'][nIndexJSon]['label'] := JConvUTF8( ALLTRIM( (cAliasFor)->NQC_DESC ) )

		(cAliasFor)->( dbSkip() )
	End

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

	(cAliasFor)->( DbCloseArea() )
	RestArea(aArea)

return .T.


//-------------------------------------------------------------------
/*/{Protheus.doc} GET SrchVara
Busca as varas de acordo com o Foro de origem

@param codComarca   - Codigo da Comarca
@param codForo   - Codigo do Foro

@since 10/09/19

@example [Sem Opcional] GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/foro/00001/varas
/*/
//-------------------------------------------------------------------
WSMETHOD GET SrchVara PATHPARAM codComarca,codForo WSRECEIVE searchKey, buscaInfo WSREST JURLEGALPROCESS
Local aVara      := {}
Local nIndexJSon := 0
Local nX         := 0
Local cSrcVara   := Self:buscaInfo
Local cCodForo   := Self:codForo
Local cSearchKey := Self:searchKey
Local oResponse  := JsonObject():New()

	aVara := listVara( cCodForo, cSearchKey, cSrcVara )

	Self:SetContentType("application/json")
	oResponse['vara'] := {}

	For nX := 1 To Len(aVara)
		nIndexJSon++

		Aadd(oResponse['vara'], JsonObject():New())

		oResponse['vara'][nIndexJSon]['value'] := JConvUTF8( ALLTRIM(aVara[nX][2]) )
		oResponse['vara'][nIndexJSon]['label'] := JConvUTF8( ALLTRIM(aVara[nX][3]) )
	Next Nx

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} SetFilter
Executa a pesquisa avan�ada de processos

@return lRet - .T.

@since 12/09/2019

@example [Sem Opcional] POST -> http://localhost:12173/rest/JURLEGALPROCESS/SetFilter
@param Body - {"filters": [{"label":"Situacao: Em andamento","value":"1","field":"NSZ_SITUAC","type":"COMBO","condition":"000000131","$id":"35a5527d-28ab-4f86-95ba-65182fc74772"}], "count": 1, "page": 1, "pageSize": 20 }
/*/
//-------------------------------------------------------------------

	WSMETHOD POST SetFilter WSREST JURLEGALPROCESS

	Local cTmp       := ""
	Local aArea      := GetArea()
	Local aSQLRestri := Ja162RstUs(,,,.T.)
	Local cTpAJ      := JWsLpGtNyb()
	Local nPage      := 0
	Local nPageSize  := 0
	Local aSQL       := {}
	Local oRequest   := JSonObject():New()
	Local oResponse  := JSonObject():New()
	Local cBody      := Self:GetContent()
	Local cQrySelect := ""
	Local cQryFrom   := ""
	Local cQryWhere  := ""
	Local cQuery     := ""
	Local cQryOrder  := ""
	Local nTotal, nI := 0
	Local cCampo     := ""
	Local xValor     := ""
	Local cTipo      := ""
	Local cCondicao  := ""
	Local nIndexJSon := 0
	Local lRetAspas  := .T.
	Local lHasNext   := .F.
	Local cDtIni     := ""
	Local nQtdRegIni := 0
	Local nQtdRegFim := 0
	Local nQtdReg    := 0
	Local cIsExport  := ""
	Local nQtdExport := 0
	Local cNSZName   := Alltrim(RetSqlName("NSZ"))
	Local aFilUsr    := JURFILUSR( __CUSERID, "NSZ" )
	Local lNoFilter  := .F.
	Local cAssJurGrp   := ""


	oResponse['processes'] := {}
	oRequest:fromJson(cBody)
	nTotal    := oRequest['count']
	nPage     := oRequest['page']
	nPageSize := oRequest['pageSize']
	cIsExport := oRequest['export']

	//-- Tratamento para exporta��o - Relat�rio de Pesquisa Avan�ada
	If cIsExport == "true"
		nQtdExport := oRequest['expCount']
		lNoFilter  := oRequest['isNoFilter']
	EndIf

	If nTotal > 0 .Or. lNoFilter
		//trata os dados de requisi��o
		For nI := 1 To nTotal
			lRetAspas := .T.

			cCampo    := oRequest['filters'][nI]['field']
			cTipo     := oRequest['filters'][nI]['type']
			xValor    := oRequest['filters'][nI]['value']
			cCondicao := oRequest['filters'][nI]['condition'] //c�digo da NVH
			cCondicao := AllTrim(JurGetDados("NVH", 1, xFilial("NVH") + AllTrim(cCondicao), "NVH_WHERE")) //localiza o NVH_WHERE

			//processa o valor entre ## e coloca o valor a ser pesquisado na condi��o
			cDado := SubStr(cCondicao, At("#", cCondicao),(RAt("#", cCondicao)-At("#", cCondicao))+1)
			If (cCampo == "NSZ_NUMCAS")
				cCondicao := StrTran(cCondicao, "NSZ_NUMCAS", "NSZ_CCLIEN || NSZ001.NSZ_LCLIEN || NSZ001.NSZ_NUMCAS")
			Else
				If cDado == "#DADO_LIKE#"
					lRetAspas := .F.
				ElseIf cTipo == "D"
					//De: Altera a condi��o para >= e adiciona ao array do WHERE a data inicial
					cCondicao := StrTran(cCondicao, SubStr(cCondicao, At(cCampo, cCondicao)+len(cCampo), At(cDado, cCondicao)), " >= "+cDado)
					cDtIni    := StrTran(cCondicao, cDado,; //localiza conte�do entre os ##
					FormataVal(StrTran(xValor[1],"-", ""), cTipo, lRetAspas)) //formata o conte�do a ser pesquisado, de acordo com o tipo

					aAdd(aSQL,{cCampo, cDtIni})

					//At�: Altera a condi��o para <= e segue o fluxo
					cCondicao := StrTran(cCondicao, SubStr(cCondicao, At(cCampo, cCondicao)+len(cCampo), At(cDado, cCondicao)), " <= "+cDado)
					xValor := StrTran(xValor[2],"-", "")
				EndIf
			EndIf

			cCondicao := StrTran(cCondicao, cDado, FormataVal(xValor, cTipo, lRetAspas)) //formata o conte�do a ser pesquisado, de acordo com o tipo
			aAdd(aSQL,{cCampo, cCondicao})
		Next

		//Pesquisa de processos
		cQrySelect := " SELECT DISTINCT NSZ001.NSZ_FILIAL    NSZ_FILIAL "
		cQrySelect += "                ,NSZ001.NSZ_COD       NSZ_COD "
		cQrySelect +=                " ,NVE.NVE_TITULO       NVE_TITULO "
		cQrySelect +=                " ,NSZ001.NSZ_VLPROV    NSZ_VLPROV "
		cQrySelect +=                " ,NSZ001.NSZ_DTPROV    NSZ_DTPROV "
		cQrySelect +=                " ,NQ7.NQ7_DESC        NQ7_DESC "

		cQryFrom   := " FROM " + cNSZName + " NSZ001 LEFT  JOIN " + RetSqlName('NVE') + " NVE"
		cQryFrom   +=                                                    " ON  (NVE.NVE_NUMCAS = NSZ001.NSZ_NUMCAS) "
		cQryFrom   +=                                                    " AND (NVE.NVE_CCLIEN = NSZ001.NSZ_CCLIEN) "
		cQryFrom   +=                                                    " AND (NVE.NVE_LCLIEN = NSZ001.NSZ_LCLIEN) "
		cQryFrom   +=                                                    " AND (NVE.NVE_FILIAL = '" + xFilial("NVE") + "') "
		cQryFrom   +=                                                    " AND (NVE.D_E_L_E_T_ = ' ') "
		cQryFrom   +=                                       " LEFT  JOIN " + RetSqlName('NQ7') + " NQ7 "
		cQryFrom   +=                                                    " ON ( NQ7.NQ7_COD = NSZ001.NSZ_CPROGN ) "
		cQryFrom   +=                                                    " AND (NQ7.NQ7_FILIAL = '" + xFilial("NQ7") + "') "
		cQryFrom   +=                                                    " AND (NQ7.D_E_L_E_T_ = ' ') "
		cQryFrom   +=                                             " JOIN " + RetSqlName('NUQ') + " NUQ "
		cQryFrom   +=                                                    " ON (NUQ.NUQ_CAJURI = NSZ001.NSZ_COD "
		cQryFrom   +=                                                    " AND NUQ.NUQ_FILIAL = NSZ001.NSZ_FILIAL "
		cQryFrom   +=                                                    " AND NUQ.D_E_L_E_T_ = ' ' ) "

		For nI := 1 to Len(aSQL)
			If nI > 1
				aTabela  := STRToArray(aSQL[nI][1], "_")

				If !(aTabela[1] + "001" $ cQryFrom)
					cQryFrom += JQryInner(aSQL[nI], Alltrim(cNSZName))
				EndIf
			Else
				cQryFrom += JQryInner(aSQL[nI], Alltrim(cNSZName))
			EndIf
		Next

		// Filtar Filiais que o usu�rio possui acesso
		If ( VerSenha(114) .or. VerSenha(115) )
			cQryWhere := " WHERE NSZ001.NSZ_FILIAL IN " + FORMATIN(aFilUsr[1],aFilUsr[2])
		Else
			cQryWhere := " WHERE NSZ001.NSZ_FILIAL = '"+xFilial("NSZ")+"'"
		Endif

		cAssJurGrp := JurTpAsJr(__CUSERID)
		cQryWhere += " AND NSZ001.NSZ_TIPOAS IN (" + cAssJurGrp + ") "

		cQryWhere  +=   " AND NSZ001.D_E_L_E_T_ = ' ' "
		cQryWhere  +=   " AND NSZ001.NSZ_TIPOAS IN (" + cTpAJ + ") "
		cQryWhere  += VerRestricao()

		If Len(aSQL) > 0 //adiciona as condi��es da pesquisa avan�ada
			cQryWhere += GetCondicao(aSQL, Alltrim(cNSZName))
		EndIf

		If !Empty(aSQLRestri) //adiciona as restri��es de usu�rio
			cQryWhere += " AND ("+Ja162SQLRt(aSQLRestri, , , , , , , , , cTpAJ)+")"
		EndIf

		cQryOrder := " ORDER BY NSZ001.NSZ_COD "

		cQuery := cQrySelect + cQryFrom + cQryWhere + cQryOrder
		cQuery := ChangeQuery(cQuery)
		cQuery := StrTran(cQuery,",' '",",''")

		cTmp   := GetNextAlias()

		DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cTmp, .F., .F. )

		nQtdRegIni := ((nPage-1) * nPageSize)

		// Define o range para inclus�o no JSON
		nQtdRegFim := (nPage * nPageSize)
		nQtdReg    := 0

		//-- Tratamento para exporta��o - Relat�rio de Pesquisa Avan�ada
		If cIsExport == "true"
			nQtdRegIni := 1
			nQtdRegFim := oRequest['count']
			nQtdReg    := 0
		EndIf

		While (cTmp)->(!Eof())
			nQtdReg++

			// Verifica se o registro est� no range da pagina ou se ir� enviar todos os registros para a exporta��o
			If (nQtdReg > nQtdRegIni .AND. nQtdReg <= nQtdRegFim) .OR. (cIsExport == "true")

				nIndexJSon++

				Aadd(oResponse['processes'], JsonObject():New())

				oResponse['processes'][nIndexJSon]['processFilial']   := (cTmp)->NSZ_FILIAL
				oResponse['processes'][nIndexJSon]['processId']       := JConvUTF8((cTmp)->NSZ_COD)
				oResponse['processes'][nIndexJSon]['caseTitle']       := JConvUTF8((cTmp)->NVE_TITULO)
				oResponse['processes'][nIndexJSon]['provisionDate']   := JConvUTF8((cTmp)->NSZ_DTPROV)
				oResponse['processes'][nIndexJSon]['provisionAmount'] := (cTmp)->NSZ_VLPROV
				oResponse['processes'][nIndexJSon]['prognostic']      := JConvUTF8((cTmp)->NQ7_DESC)
			Elseif (nQtdReg == nQtdRegFim + 1)
				lHasNext := .T.
			Endif

			(cTmp)->(DbSkip())
		EndDo

		// Verifica se h� uma proxima pagina
		If (lHasNext)
			oResponse['hasNext'] := "true"
		Else
			oResponse['hasNext'] := "false"
		EndIf

		(cTmp)->( dbcloseArea() )
	EndIf

	oResponse['length'] := nQtdReg
	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

	RestArea( aArea )
Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} GetCondicao(aSQL, cNSZName)
Constroi a clausula WHERE

@param aSQL       - aSQL[1] Campo a ser filtrado
                  - aSQL[2] Clausula WHERE
@param cNSZName   - xFilial("NSZ")
@return cCondicao - Clausula WHERE com todas as condi��es concatenadas

@since 12/09/2019
/*/
//-------------------------------------------------------------------
Static Function GetCondicao(aSQL, cNSZName)
	Local nI        := 0
	Local nC        := 0
	Local cCondicao := ""
	Local cCond     := ""

	For nI := 1 to len(aSQL)

		If !("EXISTS" $ aSQL[nI][2])
			aFiltro  := STRToArray(aSQL[nI][2], ".")
			aTrocApe := STRToArray(aFiltro[2], "_")

			For nC := 1 to Len(aFiltro)
				If nC == 1
					cCond := " AND " + aTrocApe[1] + "001"
				Else
					cCond += "." + aFiltro[nC]
				EndIf
			Next nC

			cCondicao += cCond  + " "
		Else
			cCondicao += JurGtExist(RetSqlName(SubStr(aSQL[nI][1],1,At("_",aSQL[nI][1])-1)) ,aSQL[nI][2]) + " "
		EndIf
	Next
Return cCondicao

//-------------------------------------------------------------------
/*/{Protheus.doc} FormataVal(xValor, cTipo, lRetAspas)
Formata a condi��o da clausula WHERE de acordo com o tipo do campo

@param xValor     - Conte�do a ser formatado
@param cTipo      - Tipo do conte�do (D - data, N - valor, C - caracter,
	                                  M - Memo, COMBO - combo, F3 - consulta padr�o)
@param lRetAspas   - Retorna Aspas?
					.T. - adiciona aspas,
					.F. n�o adiciona aspas
@return cRet - xValor formatado

@since 12/09/2019
/*/
//-------------------------------------------------------------------
Static Function FormataVal(xValor, cTipo, lRetAspas)
	Local cRet := ""

	Do case
	Case cTipo == 'D'
		If ValType(xValor) == "D"
			cRet := CHR(39)+AllTrim(DToS(xValor))+CHR(39)
		Else
			cRet := CHR(39)+AllTrim(xValor)+CHR(39)
		EndIf
	Case cTipo == 'N'
		cRet := AllTrim(STR(xValor))
	Case cTipo == 'C' .Or. cTipo == 'M' .Or. cTipo == 'COMBO' .Or. cTipo == 'F3'
		If lRetAspas
			cRet := CHR(39)+IIf(!Empty(xValor),AllTrim(xValor),xValor)+CHR(39)
		Else
			cRet := IIf(!Empty(xValor),AllTrim(xValor),xValor)
		EndIf
	EndCase

Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JQryInner(aSQL, cNSZName)
Fun��o utilizada para montar a parte do FROM da consulta SQL de acordo
com os campos escolhidos e a tabela padr�o informada.
Uso Geral.

@Param  	aSQL
@Param  	cNSZName Tabela padr�o, Ex. NSZ, NT4, NTA, NT2 e NT3
@Param  	aManual  Tabelas obrigat�rias que devem ser inclu�das.
@Return 	cRet Consulta SQL completa.


@since 10/11/13
@version 1.0
	/*/
//-------------------------------------------------------------------
Static Function JQryInner(aSQL, cNSZName)
	Local aSx9       := {}
	Local aTmpTab1   := {}
	Local aTmpTab2   := {}
	local aManual    := {}
	Local aTrocApe   := {}
	Local aFiltro    := {}
	Local cLeft      := ""
	Local cTmp       := ""
	Local nCtr       := 0

	If !("EXISTS" $ aSQL[2])

		aFiltro  := STRToArray(aSQL[2], ".")
		If Len(aFiltro) > 1
			/*
			aManual[1][1] == "NSZ"
			aManual[1][2] == "NSZ001"
			aManual[1][3] == "Nome tabela de Relacionamento da NSZ (Exemplo: "NUQ", "NT9","NSY")"
			aManual[1][4] == "Nome tabela de relacionamento da NSZ + apelido (Exemplo: "NUQ001","NT9001",NSY001)"
			aManual[1][5] == "Obtem o filtro a ser adicionado no Left Join e troca o nome da tabela pelo apelido"
			*/

			aTrocApe := STRToArray(aFiltro[2], "_")

			aAdd(aManual,{"NSZ", "NSZ001", aTrocApe[1], aTrocApe[1] + "001"})

			aSx9 := JURSX9(aManual[1][1],aManual[1][3])

			If  (Len(aSx9) > 0)

				//valida tabela
				cTmpTabela  := Alltrim( RetSqlName(aManual[1][3]) )
				cTmpApelido := AllTrim( aManual[1][4] )

				If (At(cTmpTabela + " " + cTmpApelido,cLeft) == 0 .And. cTmpTabela != cNSZName)//valida a tabela
					aTmpTab1 := STRToArray(aSX9[1][1], '+')
					aTmpTab2 := STRToArray(aSX9[1][2], '+')

					//n�o � a primeira ocorr�ncia, assim, ser� preciso adicionar a tabela no sql
					cLeft += " LEFT JOIN "+cTmpTabela+" "+cTmpApelido+" ON " + CRLF
					cLeft += " ("

					For nCtr := 1 to Len(aTmpTab1)
						//Determina o apelido que deve ser usado. A fun��o IIF valida se a tabela � do tipo SA1, onde o nome do campo � A1_ por exemplo
						If IIf(At('_',Left(aTmpTab1[1],3))>0,'S'+Left(aTmpTab1[nCtr],2),Left(aTmpTab1[nCtr],3)) == aManual[1][3]
							cApTab1 := aManual[1][4]
							cApTab2 := aManual[1][2]
						Else
							cApTab1 := aManual[1][2]
							cApTab2 := aManual[1][4]
						Endif

						cLeft += IIF(Left(AllTrim(aTmpTab1[nCtr]),3)==cNSZName,AllTrim(aTmpTab1[nCtr]),cApTab1 + "." + AllTrim(aTmpTab1[nCtr])) + ;
							" = " + IIF(Left(AllTrim(aTmpTab2[nCtr]),3)==cNSZName,AllTrim(aTmpTab2[nCtr]),cApTab2 + "." + AllTrim(aTmpTab2[nCtr])) + " AND "
					Next nCtr

					cLeft := Left(cLeft,Len(cLeft)-5) + CRLF
					cLeft += " AND "+ cTmpApelido +".D_E_L_E_T_ = ' ' " + CRLF

					//-- Relacionamento a partir da exclusividade das tabelas.
					cTmp += " AND " + JQryFilial(aManual[1][1], aManual[1][3], aManual[1][2], aManual[1][4]) //-- cTabPai, cTabFilha, cApPai, cApFilha
					cTmp += " )" + CRLF

					cLeft += cTmp
					cTmp := ''
				EndIf
			EndIf
		EndIf
	EndIf
Return cLeft

//-------------------------------------------------------------------
/*/{Protheus.doc} POST RltPesq
Recebe os dados filtrados na Pesquisa Avan�ada e gera a exporta��o - Relat�rio em Excel

@Return	 .T. - L�gico
@since 27/09/2019
@example [Sem Opcional] POST -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/exportPesquisa
Body: {
		"count":1,
		 "listProcess":[{"ProcessFilial":"D MG 01 ", "ProcessId":"0000000122"}],
		"modeloExp": '0005',
		"background": true
	}
/*/
//-------------------------------------------------------------------
WSMETHOD POST RltPesq WSREST JURLEGALPROCESS
Local lRet        := .T.
Local oResponse   := JsonObject():New()
Local oRequest    := JsonObject():New()
Local cBody       := Self:GetContent()

	oRequest:FromJson(cBody)

	oRequest['cEmpAnt']  := cEmpAnt
	oRequest['cFilAnt']  := cFilAnt
	oRequest['cUserId']  := __CUSERID

	oRequest['cNomeArq'] := JurTimeStamp(1) + "_exportacao_" + oRequest['cUserId'] + ".xls"

	// Tratamento para S.O Linux
	If "Linux" $ GetSrvInfo()[2]
		oRequest['cPathSpool'] := "/spool/"
		oRequest['cPathDown']  := "/thf/download/"
	Else
		oRequest['cPathSpool'] := "\spool\"
		oRequest['cPathDown']  := "\thf\download\"
	Endif

	oRequest['cPathArq'] := oRequest['cPathSpool'] + oRequest['cNomeArq']

	If VALTYPE(oRequest['modeloExp']) <> "C"
		oRequest['modeloExp'] := ""
	Endif
	
	/*
	 * Verifica se existe a tabela de notifica��o, 
	 * caso exista, compara com o conteudo da propriedade background, 
	 * caso n�o, ignora o conteudo da propriedade e define que ser� em primeiro plano
	*/
	If FWAliasInDic('O12')
		If VALTYPE(oRequest['background']) <> 'L'
			oRequest['background'] := .T.
		Endif
	Else
		oRequest['background'] := .F.
	Endif
	
	Self:SetContentType("application/json")

	If oRequest['count'] > 0
		//Busca os campos que ir�o compor o relat�rio e preenche o oResponse['aCampos']
		GetCpsRlt(oRequest)

		If !oRequest['background']
			//-- Monta Json para o Download
			oResponse['operation'] := "DownloadFile"
		
			JLPExpRel(oRequest:toJson())

			If File(oRequest['cPathArq'])
				oResponse['export'] := {}
				Aadd(oResponse['export'], JsonObject():New())

				oResponse['export'][1]['namefile'] := JConvUTF8(oRequest['cNomeArq'])
				oResponse['export'][1]['fileurl']  := ""
				oResponse['export'][1]['filedata'] := encode64(DownloadBase(oRequest['cPathArq']))
			Else
				lRet := .F.
			Endif
		Else 
			oResponse['operation'] := "Notification"
			oResponse['message']   := JConvUTF8(STR0055)//"O arquivo ser� gerado em segundo plano e, quando finalizado, ser� enviado uma notifica��o para realizar o download"

			STARTJOB("JLPExpRel", GetEnvServer(), .F., oRequest:toJson())
			
		Endif
	EndIf

	If lRet
		Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))
	Else
		SetRestFault(400,EncodeUTF8(STR0024))//"Arquivo n�o existe."
	Endif

Return lRet

//-----------------------------------------------------------------
/*/{Protheus.doc} JLPExpRel
Fun��o responsavel pela gera��o do relat�rio em excel, podendo ser chamada via job

@param cRequest - string contendo o Json contendo os dados da requisi��o para ser gerado o excel
@since 27/07/2020
/*/
//-----------------------------------------------------------------
Function JLPExpRel(cRequest)
Local lRet       := .T.
Local oRequest   := JsonObject():New()
Local cFile2Down := ""
Local cFile64    := "" 
Local nHDestino  := 0
Local nBytesSalvo:= 0

	oRequest:FromJson(cRequest)
	
	//Caso chamado via StartJob, inicializa o ambiente
	If oRequest['background']
		RPCSetType(3) // Prepara o ambiente e n�o consome licen�a
		RPCSetEnv(oRequest["cEmpAnt"],oRequest["cFilAnt"], , , 'JURI') // Abre o ambiente
		__CUSERID := oRequest["cUserId"] 
	Endif
	
	//Registra os processos na fila para exporta��o
	SetRegFila(oRequest)

	//-- Chama a Exporta��o passando a estrutura dos campos
	JA108EXPOR(;
		1                      ,;
		oRequest["aCampos"]    ,;
		oRequest["modeloExp"] ,;
		oRequest["lGar"]       ,;
		oRequest["lSoma"]      ,;
		""                     ,;
		{}                     ,;
		.T.                    ,;
		{}/*aFiltro*/          ,;
		0                      ,;
		oRequest["cPathArq"]   ,;
		.T.                    ;
	)

	// Deleta os registros da NQ3
	DelRegFila(oRequest['cUserId'], oRequest['cThread'])

	//Caso chamado via StartJob, Finaliza o ambiente
	If oRequest['background']

		lRet := CreatePathDown(oRequest['cPathDown'])

		/*
		* Caso encontrado o arquivo, mover� para a pasta do \thf\download
		* e enviar� a notifica��o para realizar o donwload do arquivo
		* Caso n�o encontrado, enviar� uma notifica��o informando que n�o foi possivel gerar o arquivo
		*/
		If lRet .and. File(oRequest["cPathArq"])

			cFile2Down  := oRequest['cPathDown']+oRequest['cNomeArq']
			cFile64     := encode64(DownloadBase(oRequest["cPathArq"]))

			nHDestino   := FCREATE(cFile2Down)

			If nHDestino > 0
				nBytesSalvo := FWRITE(nHDestino, cFile64 ,Len(cFile64))
				FCLOSE(nHDestino)
				//Cria um registro na tabela O12 - do tipo de Download 
				JA280Notify(I18n(STR0056,{oRequest['nomeModelo'],DtoC(dDataBase),Time()}), oRequest['cUserId'], "download"   , '3', "RltPesq", cFile2Down)//'O relat�rio#1 ficou pronto, clique para fazer o download #2 �s #3'
			Else
				//Cria um registro na tabela O12 - do tipo de notifica��o 
				JA280Notify(I18n(STR0057,{oRequest['nomeModelo']}) , oRequest['cUserId'], "exclamation", '1', "RltPesq")//"Falha na gera��o do relat�rio#1"
	Endif
	
		Else
			//Cria um registro na tabela O12 - do tipo de notifica��o 
			JA280Notify(I18n(STR0057,{oRequest['nomeModelo']}) , oRequest['cUserId'], "exclamation", '1', "RltPesq")//"Falha na gera��o do relat�rio#1"
		Endif

		RpcClearEnv() // Reseta o ambiente
	Endif

Return

//-----------------------------------------------------------------
/*/{Protheus.doc} CreatePathDown
Fun��o responsavel pela cria��o do caminho da pasta /thf/download/

@param cPathDown - Caminho para criar a pasta de download
@since 27/07/2020
/*/
//-----------------------------------------------------------------
Static Function CreatePathDown(cPathDown)
Local lRet     := .T.
Local aAuxPath := nil 
Local cPathAux := ""
Local cSlash   := If("Linux" $ GetSrvInfo()[2],'/','\')
Local n1       := 0

If !ExistDir(cPathDown)
	aAuxPath := Separa(cPathDown,cSlash)
	For n1 := 1 To Len(aAuxPath)
		If Empty(aAuxPath[n1])
			loop
		Endif

		cPathAux += cSlash+aAuxPath[n1]

		If !ExistDir(cPathAux)
			If MakeDir(cPathAux) <> 0
				lRet := .F.
				exit
			Endif
				Endif
	Next
	//Redundancia para garantir que a pasta foi criada depois de realizar a cria��o
	lRet := lRet .and. ExistDir(cPathDown)
	
	aSize(aAuxPath,0)
	aAuxPath := nil
			EndIf

Return lRet
//-----------------------------------------------------------------
/*/{Protheus.doc} GetCpsRlt
Fun��o responsavel pela busca dos campos, conforme o modelo passado

@param oRequest - objeto Json contendo os dados da requisi��o
@since 27/07/2020
/*/
//-----------------------------------------------------------------
Static Function GetCpsRlt(oRequest)
Local aCampos    := {}

	oRequest['nomeModelo'] := ""

		//-- Inclus�o dos campos que ser�o apresentados no relat�rio
	If !Empty(oRequest['modeloExp'])

		oRequest['nomeModelo'] := ' "'+Alltrim(Posicione('NQ5',1,xFilial('NQ5')+oRequest['modeloExp'],"NQ5_DESC"))+'"'
		aCampos                := JA108AtCps( , oRequest['modeloExp'])
			
			//-- Tratamento para apresentar linha com total de valores
		oRequest['lSoma'] := ( aScan(aCampos, { |x| Len(x) > 8 .And. x[9] == 'N' }) ) > 0
		//--Tratamento para apresentar as colunas totalizadoras dos valores de garantias
		oRequest['lGar']  := ( aScan(aCampos, { |x| Len(x) > 2 .And. (x[3] == 'NT2_VALOR' .OR. x[3] == 'NT2_VLRATU') }) ) > 0
		
	Else

		aAdd(aCampos, {'N�m Processo',              '  -  ( Instancias do Processo ) ',  'NUQ_NUMPRO', 'NUQ', 'NUQ', 'NUQ001', 'NUQ001', '08', 'C', "NUQ001.NUQ_INSATU = '1'",                            'NUQ_NUMPRO',  .F., '', ''})
		aAdd(aCampos, {'Autor',                     '  -  ( P�lo Ativo ) ',              'NT9_NOME',   'NT9', 'NT9', 'NT9001', 'NT9001', '16', 'C', "NT9001.NT9_TIPOEN = '1' AND NT9001.NT9_PRINCI = '1'", '',           .F., '', ''})
		aAdd(aCampos, {'R�u',                       '  -  ( P�lo Passivo ) ',            'NT9_NOME',   'NT9', 'NT9', 'NT9002', 'NT9002', '16', 'C', "NT9002.NT9_TIPOEN = '2' AND NT9002.NT9_PRINCI = '1'", '',           .F., '', ''})
		aAdd(aCampos, {'Desc. do Correspondente',   '  -  ( Fornecedores ) ',            'A2_NOME',    'NUQ', 'SA2', 'NUQ001', 'SA2001', '04', 'C', "",                                                    'NUQ_DCORRE', .F., '', ''})
		aAdd(aCampos, {'Progn�stico',               '  -  ( Progn�stico) ',              'NQ7_DESC',   'NSZ', 'NQ7', 'NSZ001', 'NQ7001', '03', 'C', "NQ7001.NQ7_COD = NSZ001.NSZ_CPROGN",                  'NSZ_DPROGN', .F., '', ''})
		aAdd(aCampos, {'Data de Inclus�o',          '  -  ( Assuntos Juridicos ) ',      'NSZ_DTINCL', 'NSZ', 'NSZ', 'NSZ001', 'NSZ001', '12', 'D', "",                                                    '',           .F., '', ''})
		aAdd(aCampos, {'Valor Da Causa',            '  -  ( Assuntos Juridicos ) ',      'NSZ_VLCAUS', 'NSZ', 'NSZ', 'NSZ001', 'NSZ001', '73', 'N', "",                                                    '',           .F., '', ''})
		aAdd(aCampos, {'Valor Envolv',              '  -  ( Assuntos Juridicos ) ',      'NSZ_VLENVO', 'NSZ', 'NSZ', 'NSZ001', 'NSZ001', '78', 'N', "",                                                    '',           .F., '', ''})
		aAdd(aCampos, {'Valor Provis�o',            '  -  ( Assuntos Juridicos ) ',      'NSZ_VLPROV', 'NSZ', 'NSZ', 'NSZ001', 'NSZ001', 'C6', 'N', "",                                                    '',           .F., '', ''})
		aAdd(aCampos, {'Situacao',                  '  -  ( Assuntos Juridicos ) ',      'NSZ_SITUAC', 'NSZ', 'NSZ', 'NSZ001', 'NSZ001', '55', 'C', "",                                                    '',           .F., '', '1=Em andamento;2=Encerrado'})
		aAdd(aCampos, {'Data de Encerramento',      '  -  ( Assuntos Juridicos ) ',      'NSZ_DTENCE', 'NSZ', 'NSZ', 'NSZ001', 'NSZ001', '59', 'D', "",                                                    '',           .F., '', ''})
		aAdd(aCampos, {'Comarca ',                  '  -  ( Comarca ) ',                 'NQ6_DESC',   'NUQ', 'NQ6', 'NUQ001', 'NQ6001', '17', 'C', "NUQ001.NUQ_INSATU = '1'",                             'NUQ_DCOMAR', .F., '', ''})
		aAdd(aCampos, {'Foro / Tribunal ',          '  -  ( Foro / Tribunal )  ',        'NQC_DESC',   'NUQ', 'NQC', 'NUQ001', 'NQC001', '19', 'C', "NUQ001.NUQ_INSATU = '1'",                             'NUQ_DLOC2N', .F., '', ''})
		aAdd(aCampos, {'Vara / Camara ',            '  -  ( Vara / Camara ) ',           'NQE_DESC',   'NUQ', 'NQE', 'NUQ001', 'NQE001', '21', 'C', "NUQ001.NUQ_INSATU = '1'",                             'NUQ_DLOC3N', .F., '', ''})
		
		oRequest['lSoma'] := .T.
			EndIf

	oRequest['aCampos'] := aCampos

Return

//-----------------------------------------------------------------
/*/{Protheus.doc} SetRegFila
Fun��o responsavel para informar a fila de processos

@param oRequest - objeto Json contendo os dados da requisi��o
@since 27/07/2020
/*/
//-----------------------------------------------------------------
Static Function SetRegFila(oRequest)
Local nTotal     := oRequest['count']
Local cFilCajuri := ""
Local cCajuri    := ""
Local nX         := 0

oRequest['cThread'] := SubStr(AllTrim(Str(ThreadId())),1,4)

dbSelectArea("NQ3")
dbSelectArea("NSZ")
			
NQ3->( dbSetOrder( 1 ) )
NSZ->( dbSetOrder( 1 ) )

For nX := 1 To nTotal
	cFilCajuri := oRequest['listProcess'][nX]["ProcessFilial"]
	cCajuri    := oRequest['listProcess'][nX]["ProcessId"]

	// SetOrder da fila de impress�o e assunto jur�dico

	// Inclus�o do Assunto Jur�dico na Fila de Impress�o
	If NSZ->(dbSeek(cFilCajuri + cCajuri)) ; // Verifica se o processo existe 
		.AND. !NQ3->(dbSeek(xFilial("NQ3") + cFilCajuri + cCajuri + oRequest['cUserId'] + oRequest['cThread'])) //se o registro ja n�o esta presente naquela se��o

		If RecLock('NQ3',.T. )
			NQ3->NQ3_FILIAL := xFilial("NQ3")
			NQ3->NQ3_CAJURI := cCajuri
			NQ3->NQ3_CUSER  := oRequest['cUserId']
			NQ3->NQ3_SECAO  := oRequest['cThread']
			NQ3->NQ3_FILORI := cFilCajuri
			NQ3-> (MsUnlock())
	EndIf
	EndIf
Next nX

NQ3->(dbCloseArea())
NSZ->(dbCloseArea())

Return

//-----------------------------------------------------------------
/*/{Protheus.doc} DelRegFila
Deleta a Thread da fila de impress�o ap�s exportar os dados

@param   cUser   - C�digo do usu�rio Protheus
@param   cThread - Numero da Threa atual da exporta��o
@Return  lRet    - Verifica se executou a query no banco
@since 30/09/2019
/*/
//-----------------------------------------------------------------
Static Function DelRegFila(cUser, cThread)
	Local lRet   := .T.
	Local cQuery := ""

	cQuery += "DELETE FROM "+RetSqlName("NQ3")+" "
	cQuery += "WHERE NQ3_FILIAL='"+xFilial("NQ3")+"' AND "
	cQuery += "NQ3_CUSER='"+cUser+"' AND "
	cQuery += "NQ3_SECAO='"+cThread+"' "

	lRet := TcSqlExec(cQuery) < 0

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} PUT UpdateDistr
Atualiza o status da distribui��o, grava o c�digo do processo criado
e fazer a baixa dos documentos

@since 04/10/2019
@param codDistr   - Codigo da distribui��o
@param codProc    - Codigo do processo

@example [Sem Opcional] PUT -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/distr/cod/0000000002/proc/0000000289
/*/
//-------------------------------------------------------------------
	WSMETHOD PUT UpdateDistr PATHPARAM codDistr, codProc WSREST JURLEGALPROCESS

	Local lRet       := .T.
	Local cCodNZZ    := Self:codDistr
	Local cCajuri    := Self:codProc
	Local cLinks     := ""
	Local cErros     := ""
	Local oResponse  := JsonObject():New()

	cLinks := AtuDistr(cCajuri, cCodNZZ)
	
	If !Empty(cLinks)
		//Reaiza a baixa documentos da distribui��o anexando os mesmos ao processo
		cErros := BaixaArqs(cCodNZZ, cLinks, cCajuri)
		Conout(cErros)
		oResponse['doc'] := .T.
	Else
		oResponse['doc'] := .F.
	EndIf

	oResponse['messages'] := JConvUTF8(cErros)
	Self:SetContentType("application/json")

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return lRet

/*/{Protheus.doc} BaixaArqs(cCodDis, cLinks, cCajuri)
Baixa os arquivos relacionados a Distribui��o

@param cCodDis    - C�digo da distribui��o
@param cLinks     - Conte�do do campo NZZ_LINK
@param cCajuri    - C�digo do processo
@return cErros    - Enventuais erros da baixa

@since 	 04/10/2019
/*/
Static Function BaixaArqs(cCodDis, cLinks, cCajuri)

	Local cUser     := AllTrim( SuperGetMv("MV_JDISUSR", .T., "") )	//Usu�rio teste distribuicao	aeseletropaulo
	Local cPwd      := AllTrim( SuperGetMv("MV_JDISPWD", .T., "") ) //Senha teste:  jkl_&mx%v@2018	aeseletropaulo
	Local oRest     := Nil
	Local aHeader   := {}
	Local aArquivos := {}
	Local nArq      := 0
	Local nQtdArqs  := 0
	Local cPath     := ""
	Local cNomeArq  := ""
	Local cDownload := ""
	Local nHandle   := ""
	Local lErro     := .F.
	Local cErros    := ""
	Local cTemp     := MsDocPath() + "\distribui��es\"

	If !Empty(cUser)
		aHeader   := {"Authorization: Basic " + Encode64(cUser + ":" + cPwd)}
	EndIf

	If !Empty(cLinks)
		//Quando h� mais de um link, faz a separa��o deles quebrando nos pipes de divis�o
		aArquivos := StrTokArr( AllTrim(cLinks), "|")
		nQtdArqs  := Len(aArquivos)

		If nQtdArqs > 0
			//N�o � necessario passar o host porque o SetPath tera o caminha absoluto
			oRest := FWRest():New("")
			ProcRegua(nQtdArqs)
		EndIf

		For nArq:=1 To nQtdArqs

			Conout( I18n(STR0042, {cValToChar(nArq) + "/" + cValToChar(nQtdArqs), cCodDis}) )		//"Baixando arquivos #1 - Distribui��o: #2"

			lErro	 := .F.
			cPath 	 := AllTrim( aArquivos[nArq] )
			cNomeArq := AllTrim( SubStr(cPath, Rat("/", cPath) + 1) )

			cPath    := substr(cpath, 1, Rat("/", cPath))
			cPath    += FWURIEncode(cNomeArq)

			//Verifica se j� existe o arquivos
			If !J26aExiNum("NSZ", xFilial("NSZ"), cCajuri, cNomeArq)

				oRest:SetPath(cPath)

				If oRest:Get(aHeader)

					//Download do arquivo
					cDownload := oRest:GetResult()

					//Verifica se diretorio temporario existe
					If !JurMkDir(cTemp)
						lErro := .T.
					EndIf

					//Grava arquivo no servidor
					If !lErro .And. ( nHandle := FCreate(cTemp + cNomeArq, FC_NORMAL) ) < 0
						lErro := .T.
					EndIf

					If !lErro
						If FWrite(nHandle, cDownload) < Len(cDownload)
							lErro := .T.
						EndIf

						If !lErro .And. !FClose(nHandle)
							lErro := .T.
						EndIf
					EndIf

					If lErro
						cErros += " - " + J026aErrAr( FError() ) + " - " + cPath + CRLF
					Else

						//Anexa documento ao processo
						aRetAnx := J026Anexar("NSZ", xFilial("NSZ"), cCajuri, cCajuri, cTemp + cNomeArq)

						If aRetAnx[1]
							FErase(cTemp + cNomeArq)
						Else
							cErros += " - " + aRetAnx[2] + " - " + cPath + CRLF
						EndIf
					EndIf

				Else

					cErros += " - " + oRest:GetLastError() + " - " + cPath + CRLF
				Endif

			EndIf

		Next nArq

		If !Empty(cErros)
			cErros := STR0043 + cCodDis + CRLF + cErros + CRLF		//"Distribui��o: "
		EndIf
	EndIf

	FwFreeObj(oRest)

Return cErros

//-------------------------------------------------------------------
/*/{Protheus.doc} GET EmpresaLogada
Pesquisa busca a empresa logada

@example [Sem Opcional] GET -> http://127.0.0.1:12173/rest/JURLEGALPROCESS/empresaLogada
/*/
//-------------------------------------------------------------------
	WSMETHOD GET EmpresaLogada WSREST JURLEGALPROCESS
	Local oResponse := JsonObject():New()

	Self:SetContentType("application/json")
	oResponse['codEmp'] := cEmpAnt
	oResponse['filLog'] := cFilAnt
	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//-----------------------------------------------------------------
/*/{Protheus.doc} FilUserList
Busca todas as filiais que o usu�rio tem acesso.

@since 11/10/2019
@example [Sem Opcional] GET -> http://127.0.0.1:12173/rest/JURLEGALPROCESS/listFiliaisUser
/*/
//-----------------------------------------------------------------
	WSMETHOD GET FilUserList WSRECEIVE codFil WSREST JURLEGALPROCESS
	Local cCodFil    := Self:codFil
	Local oResponse  := JsonObject():New()
	Local cCodUser   := __CUSERID
	Local nI         := 0
	Local aFilJuri   := {}
	Local nQtdReg    := 0

	oResponse['User'] := {}

	If Empty(cCodFil)
		//Busca de Filiais
		aFilJuri := JURFILUSR( cCodUser, "NSZ" )

		//-- Guarda cada filial que o usu�rio tem acesso - Trata a string para pegar somente a filial
		If Len(aFilJuri) > 2
			For nI := 1 To Len(aFilJuri[3])
				Aadd(oResponse['User'], JsonObject():New())

				oResponse['User'][nI]['value'] := aFilJuri[3][nI]

				If Empty(aFilJuri[3][nI])
					oResponse['User'][nI]['label'] := ""
				Else
					oResponse['User'][nI]['label'] := JConvUTF8( FWFilialName( , aFilJuri[3][nI] ) )
				EndIf

				nQtdReg := nQtdReg + 1

			Next nX
		EndIf
	Else
		Aadd(oResponse['User'], JsonObject():New())
		oResponse['User'][1]['value'] := cCodFil
		oResponse['User'][1]['label'] := JConvUTF8( FWFilialName( , cCodFil ) )

		nQtdReg := nQtdReg + 1
	EndIf

	oResponse['assJurAccess'] := JConvUTF8( JurTpAsJr(__CUSERID) )
	oResponse['length'] := nQtdReg

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//-----------------------------------------------------------------
/*/{Protheus.doc} TpAssuntoJur
 Busca os assuntos juridicos vinculados ao usuario, e que tenham instancia (NUQ)

@since 26/11/2019
@example [Sem Opcional] GET -> http://127.0.0.1:12173/rest/JURLEGALPROCESS/listTpAssuntoJur
/*/
//-----------------------------------------------------------------
	WSMETHOD GET TpAssuntoJur WSREST JURLEGALPROCESS
	Local oResponse    := JsonObject():New()
	Local nI           := 0
	Local aTpAssunto   := {}
	Local nQtdReg      := 0

	oResponse['TpAssJur'] := {}

	aTpAssunto := GetTpAssJur()

	For nI := 1 To Len(aTpAssunto)
		Aadd(oResponse['TpAssJur'], JsonObject():New())

		oResponse['TpAssJur'][nI]['value'] := aTpAssunto[nI][1]

		If Empty(aTpAssunto[nI][2])
			oResponse['TpAssJur'][nI]['label'] := ""
		Else
			oResponse['TpAssJur'][nI]['label'] := JConvUTF8( aTpAssunto[nI][2] )
		EndIf

		nQtdReg := nQtdReg + 1

	Next nX


	oResponse['length'] := nQtdReg


	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} GetTpAssJur()
Retornar assuntos juridicos  relacionadas a tabela de instancia
Uso Geral
@return	aTipoAss	- {'001', 'contencioso'}
@since 	26/11/2019
@version 1.0
/*/
//------------------------------------------------------------------
Static function GetTpAssJur()
	Local aTipoAss := {}
	Local aArea    := GetArea()
	Local cQuery   := ''
	Local cAlias   := GetNextAlias()

	cQuery := " SELECT NYB.NYB_COD COD, NYB.NYB_DESC DESCR"
	cQuery += " FROM " + RetSqlName("NYB") + " NYB "
	cQuery +=      " JOIN " + RetSqlName("NYC") + " NYC "
	cQuery +=        " ON( NYC_CTPASJ = NYB_COD )"
	cQuery += " WHERE NYB.NYB_COD IN (" + JurTpAsJr(__CUSERID) + ")"
	cQuery +=       " AND NYC_TABELA = 'NUQ'
	cQuery +=       " AND NYC.NYC_FILIAL = '" + xFilial("NYC") + "'"
	cQuery +=       " AND NYB.NYB_FILIAL = '" + xFilial("NYB") + "'"
	cQuery +=       " AND NYB.D_E_L_E_T_ = ' '"
	cQuery +=       " AND NYC.D_E_L_E_T_ = ' '"


	cQuery := ChangeQuery(cQuery)
	DbUseArea(.T., "TOPCONN", TcGenQry( , , cQuery), cAlias, .T., .F.)

	While !(cAlias)->( EOF())
		aadd(atipoAss, {(cAlias)->COD, (cAlias)->DESCR})
		(cAlias)->(DbSkip())
	End

	(cAlias)->(dbCloseArea())

	RestArea(aArea)

return aTipoAss

//-------------------------------------------------------------------
/*/{Protheus.doc} GET Historico
Busca o hist�rico de altera��es na tabela de Historico Altera�oes Processo (O0X)
ou no Embedded Audit Trail

@param codProc   - C�digo da Filial + Processo
@param dataIni   - Data Inicial a ser filtrada
@param dataFinal - Data Final a ser filtrada
@param usuario   - C�digo dos Usu�rios a serem filtrados
@param campo     - Campos Monitorados a serem filtrados

@since 25/11/19

@example [Sem Opcional] GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/process/0000000001/historico
/*/
//-------------------------------------------------------------------
	WSMETHOD GET Historico PATHPARAM codProc WSRECEIVE dataIni, dataFinal, usuario, campo, searchKey WSREST JURLEGALPROCESS

	Local aArea      := GetArea()
	Local cAlias     := GetNextAlias()
	Local cAlisUsr   := GetNextAlias()
	Local nIndexJSon := 0
	Local cCajuri    := Self:codProc
	Local aTabela    := { 'NSZ', 'NSZ' }
	Local cTabLog    := ""
	Local cFilterLog := ""
	Local oResponse  := JsonObject():New()
	Local cAliasLog  := ""
	Local cQuery     := ""
	Local cQryUsr    := ""
	Local cDtIni     := ""
	Local cDtFinal   := ""
	Local cUsuarios  := ""
	Local cCampo     := ""
	Local cColUSER   := ""
	Local cColUSERID := ""
	Local lAudTrail  := .F.
	Local lEncerrado := .F.
	Local cSearchKey := Self:searchKey
	Local nUsuario   := 0
	Local nR         := 0
	Local nO         := 0
	Local cOrdem     := ""
	Local lO0XInDic  := FWAliasInDic("O0X") //Verifica se existe a tabela O0X no Dicion�rio (Prote��o)
	Local cTipoBanco := UPPER(TcGetDb())
	Local aDadosNUV  := {}
	Local aDadosNTC  := {}
	Local lCposNTC   := .F.
	Local lCposNUV   := .F.

	//-- Verifica se os campos existem no dicion�rio
	If Select("NTC") > 0
			lCposNTC := (NTC->(FieldPos('NTC_USERID')) > 0);
					.AND. (NTC->(FieldPos('NTC_DATA')) > 0);
					.AND. (NTC->(FieldPos('NTC_HORA')) > 0);
					.AND. (NTC->(FieldPos('NTC_USER')) > 0) 
	Else
		DBSelectArea("NTC")
			lCposNTC := (NTC->(FieldPos('NTC_USERID')) > 0);
					.AND. (NTC->(FieldPos('NTC_DATA')) > 0);
					.AND. (NTC->(FieldPos('NTC_HORA')) > 0);
					.AND. (NTC->(FieldPos('NTC_USER')) > 0) 
		NTC->( DBCloseArea() )
	EndIf

	If Select("NUV") > 0
		lCposNUV := NUV->(FieldPos('NUV_HRALT')) > 0
	Else
		DBSelectArea("NUV")
			lCposNUV := NUV->(FieldPos('NUV_HRALT')) > 0
		NUV->( DBCloseArea() )
	EndIf

	//-- Hist�rico de Altera��es de Processos - Se tem Audit Trail Configurado ou se possui a tabela de log O0X
	If (Findfunction('TCObject')  .AND. TCObject( RetSqlName("NSZ") + "_TTAT_LOG"))
		lAudTrail := .T.
	EndIf

	//Se os par�metros n�o forem passados, retorna se existe ou n�o as tabelas do Embedded Audit Trail
	If Self:usuario == Nil
		oResponse['log'] := lAudTrail

		// Busca lista de usu�rios para preencher o multeselect da consulta de hist�rico de altera��es.
		if lO0XInDic .AND. !lAudTrail
			Self:SetContentType("application/json")
			oResponse['listusr'] := {}

			cColUSERID := "O0X_USERID"
			cColUSER   := "O0X_USER"
			cQryUsr    := "SELECT        O0X_USERID, O0X_USER "
			cQryUsr    += "FROM " + RetSqlName("O0X")"
			cQryUsr    += "WHERE (O0X_KEY = " + Self:codProc +") AND (D_E_L_E_T_ = ' ') "
			cQryUsr    += "GROUP BY O0X_USERID, O0X_USER"
		
			cQryUsr := ChangeQuery(cQryUsr)
			cQryUsr := StrTran(cQryUsr,",' '",",''")
			DbUseArea( .T., "TOPCONN", TCGenQry(,,cQryUsr), cAlisUsr, .F., .F. )

			While !(cAlisUsr)->(Eof())
				nIndexJSon++
				Aadd(oResponse['listusr'], JsonObject():New())
				oResponse['listusr'][nIndexJSon]['userid'] := (cAlisUsr)->(&(cColUSERID))
				oResponse['listusr'][nIndexJSon]['user']   := (cAlisUsr)->(&(cColUSER))
				(cAlisUsr)->(DbSkip())
			End
			(cAlisUsr)->( DbCloseArea() )
		EndIf

	Else
		cDtIni       := StrTran(Self:dataIni,"-","")
		cDtFinal     := StrTran(Self:dataFinal,"-","")
		cUsuarios    := Self:usuario
		cCampo       := Self:campo

		aUsuario := STRTOKARR( cUsuarios, ',' )
		cUsuarios:=''

		oResponse['log'] := {}

		for nUsuario := 1 to LEN(aUsuario)
			if nUsuario > 1
				cUsuarios += ",'"+ aUsuario[nUsuario] + "'"
			else
				cUsuarios += "'"+ aUsuario[nUsuario] + "'"
			endif
		next

		If lAudTrail
			cTabLog := RetSqlName("NSZ") + "_TTAT_LOG"
		ElseIf  lO0XInDic //Verifica se existe a tabela O0X no Dicion�rio
			cTabLog := "O0X"
		EndIf

		If !Empty( cTabLog )
			Self:SetContentType("application/json")

			//-- Embedded Audit Trail - API de consulta
			If lAudTrail

				cFilterLog := " TMP_UNQ = '" + cCajuri + "' AND OPERATI = 'U'  "
				If cTipoBanco == "MSSQL"
					If !Empty(cDtIni)
						cFilterLog += " AND CONVERT(VARCHAR , TMP_DTIME , 112) >= CONVERT(VARCHAR , '"+ cDtIni +"' , 121) "
					EndIf
					If !Empty(cDtFinal)
						cFilterLog += " AND CONVERT(VARCHAR , TMP_DTIME , 112) <= CONVERT(VARCHAR , '"+ cDtFinal +"' , 121) "
					EndIf
				Else
					If !Empty(cDtIni)
						cFilterLog += " AND TO_CHAR( TMP_DTIME , 'YYYYMMDD') >= '"+ cDtIni +"' "
					EndIf
					If !Empty(cDtFinal)
						cFilterLog += " AND TO_CHAR( TMP_DTIME , 'YYYYMMDD') <= '"+ cDtFinal +"' "
					EndIf
				EndIf	
				If !Empty(cUsuarios)
					cFilterLog += " AND TMP_USER IN (" + cUsuarios + ")"
				EndIf
				If !Empty(cCampo)
					cFilterLog += " AND TMP_FIELD = '"+ cCampo +"' "
				EndIf
				If !Empty(cSearchKey)
					cFilterLog += " AND TMP_FIELD LIKE '%"+ cSearchKey +"%' "
				EndIf
				cOrdem := "TMP_DTIME DESC"
				cAliasLog := FwATTViewLog( aTabela, cFilterLog, cOrdem )

				If !Empty( cAliasLog )
					( cAliasLog )->( DbGotop() )
					While !( cAliasLog )->( Eof() )
						Aadd(oResponse['log'], JsonObject():New())
						aTail(oResponse['log'])['user']      := AllTrim(( cAliasLog )->TMP_USER)
						aTail(oResponse['log'])['date']      := StrTran(substr(AllTrim(( cAliasLog )->TMP_DTIME),1,10),'-','')
						aTail(oResponse['log'])['time']      := Substr(AllTrim(( cAliasLog )->TMP_DTIME), 12, 8)
						aTail(oResponse['log'])['field']     := AllTrim(( cAliasLog )->TMP_FIELD)
						aTail(oResponse['log'])['fieldName'] := JConvUTF8(GetSx3Cache(AllTrim(( cAliasLog )->TMP_FIELD),"X3_TITULO"))
						aTail(oResponse['log'])['oldData']   := AllTrim(( cAliasLog )->TMP_COLD)
						aTail(oResponse['log'])['newData']   := AllTrim(( cAliasLog )->TMP_CNEW)
						aTail(oResponse['log'])['tipo']      := '1'
						( cAliasLog )->(DbSkip())
					EndDo
				EndIf

				//-- Fechando alias da API de consulta ao Embedded Audit Trail
				FwATTDropLog(cAliasLog)

			//-- Tabela O0X
			Else
				cQuery := " SELECT O0X_CODIGO, O0X_USER, O0X_DATA, O0X_HORA FROM " + RetSqlName('O0X')
				cQuery += " WHERE O0X_KEY = '" + cCajuri + "'"
				If !Empty(cDtIni)
					cQuery += " AND O0X_DATA >= '"+ cDtIni +"' "
				EndIf
				If !Empty(cDtFinal)
					cQuery += " AND O0X_DATA <= '"+ cDtFinal +"' "
				EndIf
				If !Empty(cUsuarios)
					cQuery += " AND O0X_USER IN ("+ cUsuarios +") "
				EndIf
				cQuery += " ORDER BY O0X_DATA DESC, O0X_HORA DESC, O0X_USER DESC"

				cQuery := ChangeQuery(cQuery)
				cQuery := StrTran(cQuery,",' '",",''")
				DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )

				While !(cAlias)->(Eof())
					nIndexJSon++
					Aadd(oResponse['log'], JsonObject():New())
					oResponse['log'][nIndexJSon]['user']      := (cAlias)->O0X_USER
					oResponse['log'][nIndexJSon]['date']      := (cAlias)->O0X_DATA
					oResponse['log'][nIndexJSon]['time']      := (cAlias)->O0X_HORA
					(cAlias)->(DbSkip())
				End

				(cAlias)->( DbCloseArea() )
			EndIf


		EndIf

		//-- Busca dados de hist�rico de justificativas altera��es caso o processo esteja encerrado
		aDadosNUV  := JSituacProc( cCajuri )
		lEncerrado := aDadosNUV[1]

		If lEncerrado .AND. Len(aDadosNUV[2]) > 0
			For nR := 1 To Len(aDadosNUV[2])
				Aadd(oResponse['log'], JsonObject():New())
				aTail(oResponse['log'])['user']        := ADADOSNUV[2][nR][1]
				aTail(oResponse['log'])['date']        := DTOS(ADADOSNUV[2][nR][2])
				aTail(oResponse['log'])['description'] := JConvUTF8(ADADOSNUV[2][nR][3])
				aTail(oResponse['log'])['time']        := ADADOSNUV[2][nR][4]
				aTail(oResponse['log'])['tipo']        := '2'
			Next nR
		EndIf

		//-- Busca dados de hist�rico de altera��o de correspondentes
		aDadosNTC := JAltCorre( cCajuri )

		If Len(aDadosNTC) > 0
			For nO := 1 To Len(aDadosNTC)
				Aadd(oResponse['log'], JsonObject():New())
				aTail(oResponse['log'])['user']        := aDadosNTC[nO][1]
				aTail(oResponse['log'])['date']        := DTOS(aDadosNTC[nO][2])
				aTail(oResponse['log'])['description'] := JConvUTF8(aDadosNTC[nO][3])
				aTail(oResponse['log'])['time']        := aDadosNTC[nO][4]
				aTail(oResponse['log'])['tipo']        := '3'
			Next nO
		EndIf
		
		//-- Ordena os registros por data e hora
		If lCposNTC .AND. lCposNUV
			asort(oresponse["log"],,,{ |x,y| x["date"] + x["time"] > y["date"] + y["time"] })
		EndIf

	EndIf

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))
	RestArea(aArea)

	aSize(aDadosNUV, 0)
	aSize(aDadosNTC, 0)

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} POST VldCNJ
Faz a valida��o do n�mero do processo (CNJ)
e retorna o cadastro De/Para de Comarcas

@return aDadosVal
		aDadosVal[1] - UF do De/Para
		aDadosVal[2] - Comarca do De/Para
		aDadosVal[3] - Foro do De/Para
		aDadosVal[4] - Vara do De/Para
		aDadosVal[5] - CNJ Valido?
		aDadosVal[6] - Mensagem CNJ inv�lido
		aDadosVal[7] - CNJ n�mero verificador inv�lido, continuar?

@since  29/11/2019

@example [Sem Opcional] POST -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/validNumPro
/*/
//-------------------------------------------------------------------
	WSMETHOD POST VldCNJ WSREST JURLEGALPROCESS

	Local oResponse   := JsonObject():New()
	Local oRequest    := JsonObject():New()
	Local cBody       := Self:GetContent()
	Local cNumPro     := ""
	Local cNatureza   := ""
	Local cTpAssunto  := ""
	Local aDadosVal   := {}


	FWJsonDeserialize(cBody,@oRequest)

	cNumPro    := oRequest['numPro']
	cNumPro := StrTran(cNumPro, "-", "")
	cNumPro := StrTran(cNumPro, ".", "")

	cNatureza  := oRequest['natureza']
	cTpAssunto := oRequest['tpAssunto']

	//Valida��o de CNJ e dados do De/Para
	aDadosVal := JU183VNPRO( cNumPro, cNatureza, cTpAssunto )

	Self:SetContentType("application/json")

	oResponse['uf']       := aDadosVal[1]
	oResponse['comarca']  := aDadosVal[2]
	oResponse['foro']     := aDadosVal[3]
	oResponse['vara']     := aDadosVal[4]
	oResponse['valid']    := aDadosVal[5]
	oResponse['message']  := aDadosVal[6]
	oResponse['continue'] := aDadosVal[7]

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//-------------------------------------------------------------------
 /*/{Protheus.doc} getPartCont
	Fun��o respons�vel por trazer a parte contr�ria do processo

	@param cCajuri codigo do assunto juridico a ser pesquisado
	@return cPartCont - Parte contr�ria
	@since 07/01/2020
	@version 1.0
/*/
//-------------------------------------------------------------------
Static Function getPartCont(cCajuri)
	Local cPartCont   := ""
	Local nCount      := 0
	Local cAutorPrinc := ""
	Local cReuPrinc   := ""
	Local cEntAutor   := ""
	Local cEntReu     := ""
	Local aNT9        := JWsLpGtNt9(cCajuri)

	If !Empty(aNT9)
		//passa por todos os envolvidos do processo para pegar o autor e o r�u e sua entidade
		For nCount := 1 to Len(aNT9)
			If (JConvUTF8(aNT9[nCount][4])) == '1' //� principal ?
				If JConvUTF8(aNT9[nCount][11]) == '1'//Polo ativo - Autor?
					cAutorPrinc := aNT9[nCount][5]
					cEntAutor   := aNT9[nCount][3]
				ElseIf JConvUTF8(aNT9[nCount][11]) == '2'//Polo passivo - Reu?
					cReuPrinc := aNT9[nCount][5]
					cEntReu   := aNT9[nCount][3]
				EndIf
			EndIf
		Next
	EndIf

	If cEntAutor == "SA1"
		cPartCont := cReuPrinc
	ElseIf cEntReu == "SA1"
		cPartCont := cAutorPrinc
	else
		cPartCont := "-"
	EndIf

Return JConvUTF8(cPartCont)

//-----------------------------------------------------------------
/*/{Protheus.doc} StatusSolic
Consulta o follow-up por tipo de aprova��o e resultado a partir do c�digo do workflow do fluig.

@since 14/01/2020
@example [Sem Opcional] GET -> http://127.0.0.1:12173/rest/JURLEGALPROCESS/solicitation/170
/*/
//-----------------------------------------------------------------
	WSMETHOD GET StatusSolic PATHPARAM codWf WSREST JURLEGALPROCESS
	Local oResponse := JsonObject():New()
	Local cCodWf    := Self:codWf
 
	oResponse['status'] := J94FTarFw(xFilial("O0W"), cCodWf, "6", "4")

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.


//-------------------------------------------------------------------
/*/{Protheus.doc} GET ExportPDF
Exporta o resumo do processo em PDF

@param codProc   - C�digo do Processo
@param codFil    - Filial do processo 
@param tipoAssjur - tipo do assunto juridico do processo, utilizado ao realizar a gera��o do relat�rio

@since 15/01/20

@example [Sem Opcional] GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/exportpdf/0000000001?codFil='01'&tipoAssjur=001
/*/
//-------------------------------------------------------------------
	WSMETHOD GET ExportPDF PATHPARAM codProc WSRECEIVE codFil, tipoAssjur WSREST JURLEGALPROCESS
	Local aArea     := GetArea()
	Local aAreaNSZ  := NSZ->( GetArea() )
	Local aAreaNQ3  := NQ3->( GetArea() )
	Local cThread   := SubStr(AllTrim(Str(ThreadId())),1,4)
	Local cCajuri   := AllTrim(Self:codProc)
	Local cUser     := __CUSERID
	Local cFilPro   := Self:codFil
	Local cCfgRel   := AllTrim(JurGetDados("NQR", 2, xFilial("NQR") + "JURR095", "NQR_COD")) //NQR_FILIAL+ NQR_NOMRPT //configura��o de relat�rio padrao de assunto juridico
	Local cParams   := cUser + ";" + cThread + ";S;T;N;0;N;01/01/1900;31/12/2050;S;S;" + cFilPro + ";N;;"
	Local cCaminho  := "\spool\"
	Local cNomerel  := JurTimeStamp(1) + "_relatorioprocesso_" + cCajuri + "_" + cUser
	Local oResponse := JsonObject():New()
	Local cTipoAss  := Self:tipoAssjur

	// Tratamento para S.O Linux
	If "Linux" $ GetSrvInfo()[2]
		cCaminho := StrTran(cCaminho,"\","/")
	Endif

	// SetOrder da fila de impress�o e assunto jur�dico
	dbSelectArea("NSZ")

	NSZ ->(DBSetOrder(1))//NSZ_FILIAL + NSZ_COD
	// Inclus�o do Assunto Jur�dico na Fila de Impress�o
	If NSZ->(dbSeek(cFilPro + cCajuri))
		dbSelectArea("NQ3")
		NQ3->( dbSetOrder( 1 ) )//NQ3_FILIAL + NQ3_FILORI + NQ3_CAJURI + NQ3_CUSER + NQ3_SECAO
		If !NQ3->(dbSeek(xFilial("NQ3")+ cFilPro + cCajuri + cUser + cThread)) // Verifica se o processo existe E se o registro ja n�o esta presente naquela se��o
			If RecLock('NQ3',.T. )
				NQ3->NQ3_FILIAL := xFilial("NQ3")
				NQ3->NQ3_CAJURI := cCajuri
				NQ3->NQ3_CUSER  := cUser
				NQ3->NQ3_SECAO  := cThread
				NQ3->NQ3_FILORI := cFilPro
				NQ3-> (MsUnlock())
			EndIf
		EndIf
		NQ3->(dbCloseArea())
	EndIf
	NSZ->(dbCloseArea())

	RestArea(aAreaNQ3)
	RestArea(aAreaNSZ)
	RestArea(aArea)

	//Chama a fun��o para gerar o relat�rio
	JURRel095(cTipoAss, cUser, cThread, cParams, cCfgRel, .F. , cNomerel, cCaminho)

	// Deleta os registros da NQ3
	DelRegFila(cUser, cThread)

	//-- Monta Json para o Download
	Self:SetContentType("application/json")
	oResponse['operation'] := "ExportProcess"
	oResponse['export']    := {}
	Aadd(oResponse['export'], JsonObject():New())

	oResponse['export'][1]['namefile'] := JConvUTF8(cNomerel + ".pdf")
	oResponse['export'][1]['filedata'] := encode64(DownloadBase(cCaminho + cNomerel + ".pdf"))

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} GET listFavorite
Lista os favoritos do processo.

@param tipoAssjur - tipo do assunto juridico do processo, utilizado ao realizar a gera��o do relat�rio

@since 15/01/20

@example [Sem Opcional] GET -> http://127.0.0.1:9090/rest/JURLEGALPROCESS/listFavorite?tipoAssjur=001
/*/
//-------------------------------------------------------------------
WSMETHOD GET listFavorite WSRECEIVE tipoAssjur WSREST JURLEGALPROCESS
Local oResponse := JSonObject():New()
Local cTipoAss  := Self:tipoAssjur
Local cQuery    := ""
Local cAlias    := GetNextAlias()
local aRet      := {}
Local nIndexJSon := 0
Local nQtdReg    := 0

	If cTipoAss == '001'
		cTipoAss := "001','002','003','004"
	EndIf

	cQuery := " SELECT O0V.O0V_FILCAJ FILIAL, O0V.O0V_CAJURI CAJURI FROM " + RetSqlName('O0V') + " O0V"
	cQuery += " LEFT JOIN "+ RetSqlName('NSZ') + " NSZ "
	cQuery +=             " ON (NSZ.NSZ_FILIAL = O0V.O0V_FILCAJ "
	cQuery +=             " AND NSZ.NSZ_COD = O0V.O0V_CAJURI ) "
	cQuery += " WHERE NSZ.NSZ_TIPOAS IN ('" + cTipoAss + "' )"
	cQuery +=       " AND O0V.O0V_USER = '" + __CUSERID + "' "
	cQuery +=       " AND NSZ.D_E_L_E_T_ = ' '"
	cQuery +=       " AND O0V.D_E_L_E_T_ = ' '"

	cQuery := ChangeQuery(cQuery)

	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )
	oResponse['listFavorite'] := {}
	
	while (cAlias)->(!Eof())
		aRet := J269QryFav( (cAlias)->FILIAL, (cAlias)->CAJURI )
		nQtdReg++
		nIndexJSon++
		Aadd(oResponse['listFavorite'], JsonObject():New())
		
		oResponse['listFavorite'][nIndexJSon]['filial']       := JConvUTF8((cAlias)->FILIAL)
		oResponse['listFavorite'][nIndexJSon]['cajuri']       := JConvUTF8((cAlias)->CAJURI)
		oResponse['listFavorite'][nIndexJSon]['movimentacao'] := JConvUTF8(aRet[5])
		oResponse['listFavorite'][nIndexJSon]['partes']       := JConvUTF8(aRet[3]) +" x "+ JConvUTF8(aRet[4])
		oResponse['listFavorite'][nIndexJSon]['date']         := JConvUTF8(aRet[6])
		oResponse['listFavorite'][nIndexJSon]['fav']          := "favorito"
		(cAlias)->(DbSkip())
	endDo
	(cAlias)->(dbCloseArea())

	oResponse['length'] := nQtdReg

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} JSituacProc
Verifica se o processo esta encerrado e busca dados na tabela de
historico de altera��es de processo encerrado (NUV)

@param  cChaveProc - Chave do processo ( Filial + Cajuri )
@return lEncerrado - boolean -  Indica se o processo esta encerrado
		aDadosNUV  - Array
		aDadosNUV[1] -  Usu�rio       - Nome do usu�rio que realizou a altera��o 
		aDadosNUV[2] -  Data          - Data em que foi realizada a altera��o
		aDadosNUV[3] -  Justificativa - justificativa da altera��o
		aDadosNUV[4] -  Hora          - Hora em que foi realizada a altera��o

@since 28/05/2020
/*/
//-------------------------------------------------------------------
Static Function JSituacProc( cChaveProc )

Local aAreaNUV   := GetArea()
Local cQueryNUV  := ""
Local cAliasNUV  := ""
Local aDadosNUV  := {}
Local lCpoHora   := .F.
Local lEncerrado := .F.

	//-- Verifica se o campo NUV_HRALT existe no dicion�rio
	If Select("NUV") > 0
			lCpoHora := NUV->(FieldPos('NUV_HRALT')) > 0
	Else
		DBSelectArea("NUV")
			lCpoHora := NUV->(FieldPos('NUV_HRALT')) > 0
		NUV->( DBCloseArea() )
	EndIf

	//--Verifica se o processo esta encerrado
	DbSelectArea("NSZ")
	NSZ->(DbSetOrder(1)) // NSZ_FILIAL+NSZ_COD

	If NSZ->(Dbseek( cChaveProc ))
		lEncerrado := IIF( NSZ->NSZ_SITUAC == '2', .T., .F. )


	EndIf
	NSZ->(DbCloseArea())

	//-- Se o processo estiver encerra busca dados na NUV
	If lEncerrado

		cAliasNUV := GetNextAlias()

		cQueryNUV := " SELECT R_E_C_N_O_ RECNONUV "

		cQueryNUV += " FROM " + RetSqlName("NUV") + " NUV " 
		cQueryNUV += " WHERE NUV_FILIAL || NUV_CAJURI ='" + cChaveProc + "' "
		cQueryNUV += " AND NUV.D_E_L_E_T_ = ' ' "

		cQueryNUV := ChangeQuery(cQueryNUV)
		DbUseArea(.T., "TOPCONN", TcGenQry(,,cQueryNUV), cAliasNUV, .F., .T.)

		DbSelectArea("NUV")
		NUV->( dbSetOrder( 1 ) ) // NUV_FILIAL+NUV_COD
		NUV->( DbGoTop() )

		While (cAliasNUV)->(!Eof())
			NUV->( DbGoTo( (cAliasNUV)->RECNONUV ) )
			aAdd( aDadosNUV, { NUV->NUV_USUALT, NUV->NUV_DTALT, ALLTRIM( NUV->NUV_JUSTIF ), IIF( lCpoHora, ALLTRIM(NUV->NUV_HRALT), "" ) } )
			(cAliasNUV)->(DbSkip())
		End
		
		NUV->(DbCloseArea())
		(cAliasNUV)->(DbCloseArea())
	EndIf

	RestArea(aAreaNUV)

Return { lEncerrado , aDadosNUV }

//-------------------------------------------------------------------
/*/{Protheus.doc} JAltCorre
Busca dados na tabela de historico de altera��es correspondente (NTC)

@param  cChaveProc - Chave do processo ( Filial + Cajuri )
@return aDadosNTC - Array
		aDadosNTC[1] - Usu�rio - Nome do usu�rio que realizou a altera��o
		aDadosNTC[2] - Data    - Data em que foi realizada a altera��o
		aDadosNTC[3] - Data    - Motivo da altera��o
		aDadosNTC[4] - Hora    - Hora em que foi realizada a altera��o
@since 28/05/2020
/*/
//-------------------------------------------------------------------
Static Function JAltCorre( cChaveProc )

Local aAreaNTC   := GetArea()
Local cQueryNTC  := ""
Local cAliasNTC  := ""
Local aDadosNTC  := {}
Local lNewCpos   := .F.

	//-- Verifica se os campos existem no dicion�rio
	If Select("NTC") > 0
			lNewCpos := (NTC->(FieldPos('NTC_USERID')) > 0);
					.AND. (NTC->(FieldPos('NTC_DATA')) > 0);
					.AND. (NTC->(FieldPos('NTC_HORA')) > 0);
					.AND. (NTC->(FieldPos('NTC_USER')) > 0) 
	Else
		DBSelectArea("NTC")
			lNewCpos := (NTC->(FieldPos('NTC_USERID')) > 0);
					.AND. (NTC->(FieldPos('NTC_DATA')) > 0);
					.AND. (NTC->(FieldPos('NTC_HORA')) > 0);
					.AND. (NTC->(FieldPos('NTC_USER')) > 0) 
		NTC->( DBCloseArea() )
	EndIf

	//-- Busca dados do hist�rico de altera��o de correspondente - NTC
	cAliasNTC := GetNextAlias()

	cQueryNTC := " SELECT R_E_C_N_O_ RECNONTC "
	cQueryNTC += " FROM " + RetSqlName("NTC") + " NTC "
	cQueryNTC += " WHERE NTC_FILIAL || NTC_CAJURI ='" + cChaveProc + "' "
	cQueryNTC += " AND NTC.D_E_L_E_T_ = ' ' "

	cQueryNTC := ChangeQuery(cQueryNTC)
	DbUseArea(.T., "TOPCONN", TcGenQry(,,cQueryNTC), cAliasNTC, .F., .T.)

	DbSelectArea("NTC")
	NTC->( dbSetOrder( 1 ) ) // NTC_FILIAL+NTC_COD
	NTC->( DbGoTop() )

	While (cAliasNTC)->(!Eof())
		NTC->( DbGoTo( (cAliasNTC)->RECNONTC ) )

		If lNewCpos
			aAdd( aDadosNTC, { NTC->NTC_USER, NTC->NTC_DATA, ALLTRIM( NTC->NTC_MOTIVO ), ALLTRIM(NTC_HORA) } )
		EndIf
		(cAliasNTC)->(DbSkip())
	End
	
	NTC->(DbCloseArea())
	(cAliasNTC)->(DbCloseArea())
	RestArea(aAreaNTC)

Return aDadosNTC

//-----------------------------------------------------------------
/*/{Protheus.doc} getSequencial
Busca o proximo sequencial disponivel na tabela SA1

@since 28/05/20
@example [Sem Opcional] GET -> http://127.0.0.1:12173/rest/JURLEGALPROCESS/getSequencial
/*/
//-----------------------------------------------------------------
WSMETHOD GET getSequencial WSRECEIVE searchKey WSREST JURLEGALPROCESS
	Local oResponse := JsonObject():New()

	oResponse['sequencial'] := getSequencial(self:searchKey)

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} GET getSequencial
Busca o sequencial da tabela.
@param cTabela - tabela para buscar o pr�ximo c�digo

@since 28/05/20

/*/
//-------------------------------------------------------------------
static function getSequencial(cTabela)
Local aArea       := GetArea()
Local cAlias      := GetNextAlias()
local lEncontrou  := .F.
local nTamanho    := 0 
local cSequencial := '0'
Local cQuery      := ''

Default cTabela := 'SA1'

	nTamanho := FWTamSX3(substr(cTabela,2,2)+"_COD")[1]

	// Pega o Recno do pr�ximo registro
	cQuery := "SELECT (MAX(R_E_C_N_O_)+1) SEQUENCIAL FROM " + RetSqlName(cTabela)
	cQuery := ChangeQuery(cQuery)
	DbUseArea(.T., "TOPCONN", TcGenQry(,,cQuery), cAlias, .F., .T.)
		If (cAlias)->(!Eof())
			cSequencial := 	StrZero(((cAlias)->SEQUENCIAL) +1, nTamanho)
		EndIf
	(cAlias)->(DbCloseArea())

	dbSelectArea(cTabela)
		//Valida se j� existe registro com esta chave
		(cTabela)->( dbSetOrder( 1 ) ) // FILIAL + COD + LOJA
		While !lEncontrou
			If (cTabela)->( dbSeek( xFilial(cTabela) + cSequencial + '01') )
				cSequencial := StrZero(Val(cSequencial) +1, nTamanho)
			else
				lEncontrou := .T.
			EndIf
		End
	(cTabela)->(dbCloseArea())

	RestArea(aArea)

return cSequencial


//-----------------------------------------------------------------
/*/{Protheus.doc} GetSeqNXY
Busca o proximo sequencial disponivel na tabela NXY.

@since 08/06/2020
@example [Sem Opcional] GET -> http://127.0.0.1:12173/rest/JURLEGALPROCESS/getSeqNXY
/*/
//-----------------------------------------------------------------
WSMETHOD GET SeqNXY WSREST JURLEGALPROCESS
	Local oResponse := JsonObject():New()

	oResponse['seq'] := GetSeqNXY()

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} GET listFavorite
Busca o sequencial da tabela NXY.

@return - cSeqnxy - String com o codigo sequencial (NXY_COD)
@since 08/06/20
/*/
//-------------------------------------------------------------------
Static function GetSeqNXY()

Local aArea      := GetArea()
Local cTabela    := 'NXY'
Local lEncontrou := .F.
Local cFilial    := xFilial('NXY')
Local nTamanho   := FWTamSX3("NXY_COD")[1]
Local cSeqnxy    := StrZero(1, nTamanho)

		DbSelectArea(cTabela)
		NXY->( dbSetOrder( 2 ) ) // NXY_FILIAL+NXY_COD+NXY_CAJURI

		While !lEncontrou
			If NXY->( dbSeek( cFilial + cSeqnxy ) )
				cSeqnxy := StrZero(Val(cSeqnxy) +1, nTamanho)
			Else
				lEncontrou := .T.
			EndIf
		End
		NXY->(DbCloseArea())

	RestArea(aArea)

return cSeqnxy

//-------------------------------------------------------------------
/*/{Protheus.doc} distribuicoes
Efetua o tembamento em lote das distribui��es

@return result - 'Processando' Indica que o tombamento ser� executado em segundo plano

@since 10/08/2020

@example POST -> http://localhost:12173/rest/JURLEGALPROCESS/distribuicoes
Body ->:  {
 	"distribuicoes": [
 		["pk do registro", "n�mero do processo"],
 		["yyyyyy", "5002949-03.2018.8.13.0693"],
 		["zzzzzz", "5002949-03.2018.8.13.0693"]
 	],
 	"modelo": [
 		["Campo", "Tipo", "Valor", "Modelo"],
 		["NSZ_HCITAC", "C", "2", "NSZMASTER"],
 		["NT9_ENTIDA", "C", "SA1", "NT9DETAIL"],
 		["NT9_CODENT", "C", "JLT00101", "NT9DETAIL"],
 		["NT9_DENTID", "C", "Clientes", "NT9DETAIL"],
 		["NT9_TIPOCL", "C", "1", "NT9DETAIL"],
 		["NT9_TIPOEN", "C", "2", "NT9DETAIL"],
 		["NT9_CTPENV", "C", "02", "NT9DETAIL"],
 		["NT9_DTPENV", "C", "REU", "NT9DETAIL"],
 		["NT9_CEMPCL", "C", "JLT001", "NT9DETAIL"],
 		["NT9_LOJACL", "C", "01", "NT9DETAIL"],
 		["NT9_NOME", "C", "LEGALTASK 001", "NT9DETAIL"],
 		["NT9_TIPOP", "C", "1", "NT9DETAIL"],
 		["NT9_RESP", "C", "1", "NT9DETAIL"],
 		["NT9_ENDECL", "C", "LEGALTASK", "NT9DETAIL"],
 		["NT9_ESTADO", "C", "SP", "NT9DETAIL"],
 		["NUQ_INSATU", "C", "1", "NUQDETAIL"],
 		["NUQ_CNATUR", "C", "001", "NUQDETAIL"]
 	]
 }
/*/
//-------------------------------------------------------------------
WSMETHOD POST distribuicoes WSREST JURLEGALPROCESS
Local oRequest   := JsonObject():New()
Local oResponse  := JsonObject():New()
Local cBody      := Self:GetContent()
Local aDistri    := {}
Local aModelo    := {}
Local aTread     := {}
Local nThread    := 0
Local nReg       := 0 
Local nQtd       := 0
Local cAssunto   := ""
Local cTpAudi    := ""
Local cRespAudi  := ""

	oRequest:fromJson(cBody)
	cAssunto  := oRequest['tipoAssunto']
	cTpAudi   := oRequest['tipoAudiencia']
	cRespAudi := oRequest['respAudiencia']
	aDistri   := oRequest['distribuicoes']
	aModelo   := oRequest['detModelo']

	For nReg := 1 To len(aDistri)

		 Importando(Decode64(aDistri[nReg][1]))

		nQtd ++ 
		Aadd(aTread, aDistri[nReg])

		// Abre at� 10 Treads
		If (nQtd >= len(aDistri) / 4) .Or. (nReg == len(aDistri) )

			nThread ++
			STARTJOB("ImpDistr", GetEnvServer(), .F., ;
					aModelo, aTread, __CUSERID, cEmpAnt, cFilAnt, nThread, cAssunto, cTpAudi, cRespAudi)

			aTread := {}
			nQtd := 0 
		EndIf
	Next nReg

	oResponse['result'] := 'Processando'
	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))

Return .T.

//----------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ImpDistr( aModelo, aDistr)
Importa a tista de distribui��es

@param aModelo    - Array com a rela��o de campos e valores do template de processo
@param aDistr     - C�digo do processo
@param cUsuario   - C�digo do usu�rio que fez a requisi��o
@param cEmpLog    - C�digo da empresa logada
@param cFilLog    - C�digo da filial logada
@param nThread      - n�mero da thread 
@param cTpAssunto - 

@since 	 10/08/2020
/*/
//----------------------------------------------------------------------------------------------------
Function ImpDistr( aModelo, aDistr, cUsuario, cEmpLog, cFilLog, nThread, cTpAssunto, cTpAudi, cRespAudi )
Local oModelJ95   := Nil
Local oDadosNZZ   := Nil
Local aCFV        := {}
Local aMsg        := {}
Local aLinks      := {}
Local cLinks      := ''
Local cNumPro     := ''
Local cCajuri     := ''
Local cCobjet     := ''
Local cCareaj     := ''
Local cTpAss      := ''
Local nDistrib    := 0
Local nI          := 0
Local lSucesso    := .F.

public cTipoAsJ   := cTpAssunto
public c162TipoAs := cTpAssunto

	// Inicializa o ambiente
	RPCSetType(3) // Prepara o ambiente e n�o consome licen�a
	RPCSetEnv(cEmpLog, cFilLog, , , 'JURI_' + Str(nThread)) // Abre o ambiente
	__CUSERID := cUsuario 

	//Instancia o modelo
	oModelJ95  := FWLoadModel("JURA095")
	oModelJ95:SetOperation(MODEL_OPERATION_INSERT)

	// Varre o array de distribui��es
	For nDistrib  := 1 To Len(aDistr)

		lSucesso  := .F.
		cNumPro   := StrTran(aDistr[nDistrib][2], '-')
		cNumPro   := AllTrim(StrTran(cNumPro, '.'))

		// Busca dados complementares do processo
		oDadosNZZ := JsonObject():New()
		oDadosNZZ := JDistReceb(aDistr[nDistrib][2],.F.)

		oDadosNZZ[1]['NUQ_TLOC3N'] := Decode64((oDadosNZZ[1]['NUQ_TLOC3N']))

		oModelJ95:Activate()

		// Popula NSZ
		SetModVal('NSZMASTER',aModelo,oDadosNZZ, oModelJ95)

		// Popula NUQ
		oModelJ95:LoadValue('NUQDETAIL', 'NUQ_INSTAN', '1')
		oModelJ95:LoadValue('NUQDETAIL', 'NUQ_INSATU', '1')
		oModelJ95:LoadValue('NUQDETAIL', 'NUQ_NUMPRO', cNumPro)
		SetModVal('NUQDETAIL',aModelo,oDadosNZZ, oModelJ95)

		// Popula NT9
		SetModVal('NT9DETAIL',aModelo,oDadosNZZ, oModelJ95)

		// Valida preenchimento de Data, Moeda e Valor
		If ( Empty( oModelJ95:GetValue('NSZMASTER', 'NSZ_DTCAUS') ) .Or. ;
			Empty( oModelJ95:GetValue('NSZMASTER', 'NSZ_CMOCAU') ) .Or. ;
			Empty( oModelJ95:GetValue('NSZMASTER', 'NSZ_VLCAUS') ) )

			oModelJ95:LoadValue('NSZMASTER', 'NSZ_DTCAUS', SToD(''))
			oModelJ95:LoadValue('NSZMASTER', 'NSZ_CMOCAU', '')
			oModelJ95:LoadValue('NSZMASTER', 'NSZ_VLCAUS', 0)
		EndIf
		
		// Valida preenchimento de Comarca, Foro e Vara
		If ( Empty( oModelJ95:GetValue('NUQDETAIL', 'NUQ_CCOMAR') ) .Or. ;
			Empty( oModelJ95:GetValue('NUQDETAIL', 'NUQ_CLOC2N') ) )

			aCFV := GetCFV(aDistr[nDistrib][2])
			oModelJ95:LoadValue('NUQDETAIL', 'NUQ_INSTAN', GetInst(aCFV[2]))
			oModelJ95:LoadValue('NUQDETAIL', 'NUQ_CCOMAR', aCFV[1])
			oModelJ95:LoadValue('NUQDETAIL', 'NUQ_CLOC2N', aCFV[2])
			oModelJ95:LoadValue('NUQDETAIL', 'NUQ_CLOC3N', aCFV[3])
		EndIf

		//Valida Jura095
		If ( oModelJ95:VldData() )
				cCajuri := oModelJ95:GetValue('NSZMASTER', 'NSZ_COD')

				cCobjet:= oModelJ95:GetValue("NSZMASTER","NSZ_COBJET")
				cCareaj:= oModelJ95:GetValue("NSZMASTER","NSZ_CAREAJ")
				cTpAss := oModelJ95:GetValue("NSZMASTER","NSZ_TIPOAS")

				If Empty(cRespAudi)
					cRespAudi   := oModelJ95:GetValue('NSZMASTER', 'NSZ_SIGLA1')
				EndIf

				If( oModelJ95:CommitData() )
					lSucesso := .T.
				EndIf
		Else 
			aMsg := oModelJ95:GetModel():GetErrormessage()

			For nI := 1 To Len (aMsg)
				Conout(aMsg[nI])
			Next nI
		EndIf

		oModelJ95:Deactivate()

		// Atualiza NZZ
		cLinks := AtuDistr(cCajuri, oDadosNZZ[1]['NZZ_COD'], lSucesso, aMsg)
		If !(Empty(cLinks))
			Aadd(aLinks, {oDadosNZZ[1]['NZZ_COD'], cLinks, cCajuri})
		EndIf

		// Grava fups autom�ticos
		If (lSucesso)

			//Grava Audi�ncia
			If ( !Empty(oDadosNZZ[1]['NZZ_DTAUDI']) )
			
				If !(Empty(cTpAudi))
					GravaAud(cCajuri, cTpAudi, cRespAudi, oDadosNZZ[1]['NZZ_DTAUDI'] , oDadosNZZ[1]['NZZ_HRAUDI'] )
				Else
					JAINCFWAUT('3', cCajuri, cCobjet, cCareaj, cTpAss, oDadosNZZ[1]['NZZ_DTAUDI'] , oDadosNZZ[1]['NZZ_HRAUDI'])
				EndIf
			EndIf
		EndIf

		FreeObj(oDadosNZZ)

	Next nDistrib

	oModelJ95:Destroy()

	// Baixa os documentos
	For nI := 1 To Len(aLinks)
		BaixaArqs(aLinks[nI][1], aLinks[nI][2], aLinks[nI][3])
	Next nI

Return .T.

//----------------------------------------------------------------------------------------------------
/*/{Protheus.doc} SetModVal(cModel,aModel,oDadosNZZ, oModelJ95)
Seta os valores do modelo

@param cModel    - Nome do Modelo
@param aModel    - Array com a rela��o de campos e valores do template de processo
@param oDadosNZZ - Objeto com os dados do processo
@param oModelJ95 - Modelo da Jura095 

@since 	 10/08/2020
/*/
//----------------------------------------------------------------------------------------------------
Static Function SetModVal(cModel,aModel,oDadosNZZ, oModelJ95)
Local aStruct    := {}
Local xValue     := Nil
Local cType      := ''
Local nModel     := 0
Local nReg       := 0
Local nField     := 0
Local nI         := 0

	//Busca a estrutura
	If (cModel = 'NSZMASTER')
		nModel   := 1
		aStruct  := FWFormStruct( 2, 'NSZ', { | cCampo | getSx3Cache(cCampo, 'X3_VISUAL') != 'V' } ):aFields
	ElseIf (cModel = 'NUQDETAIL')
		nModel   := 2
		aStruct  := FWFormStruct( 2, 'NUQ', { | cCampo | getSx3Cache(cCampo, 'X3_VISUAL') != 'V' } ):aFields
	ElseIf (cModel = 'NT9DETAIL') 
		nModel   := 3
		aStruct  := FWFormStruct( 2, 'NT9', { | cCampo | getSx3Cache(cCampo, 'X3_VISUAL') != 'V' } ):aFields
	EndIf

	If (cModel == 'NT9DETAIL')
		// Carrega a unidade como Polo Passivo Principal 
		cCodCli  := GetValue( aModel, 'NSZ_CCLIEN', .T. )
		codLoja  := GetValue( aModel, 'NSZ_LCLIEN', .T. )
		aUnidade := GetUnidade(cCodCli, codLoja)

		If Len(aUnidade) > 0
			oModelJ95:LoadValue(cModel, "NT9_ENTIDA", "SA1")
			oModelJ95:SetValue(cModel, "NT9_CODENT", cCodCli + codLoja)
			oModelJ95:SetValue(cModel, "NT9_CGC"   , aUnidade[2])
			oModelJ95:SetValue(cModel, "NT9_PRINCI", "1")
			oModelJ95:SetValue(cModel, "NT9_TIPOEN", "2")
			oModelJ95:SetValue(cModel, "NT9_CTPENV", J219GetNQA({"reu"},"2"))
			nI := 1
		EndIf
	
		//Carrega demais envolvidos
		For nReg := 1 To len(oDadosNZZ[1]['Envolvidos'])
			nI++

			If nI > 1
				If oModelJ95:GetModel(cModel):AddLine() < nI
					Exit
				EndIf
			EndIf

			oModelJ95:LoadValue(cModel, "NT9_ENTIDA", oDadosNZZ[1]['Envolvidos'][nReg]["NT9_ENTIDA"])
			oModelJ95:LoadValue(cModel,  "NT9_CODENT", oDadosNZZ[1]['Envolvidos'][nReg]["NT9_CODENT"])
			oModelJ95:SetValue(cModel,  "NT9_CGC"   , oDadosNZZ[1]['Envolvidos'][nReg]["NT9_CGC"])
			//Valida Preenchimento TipoPessoa
			If(len(alltrim(oModelJ95:GetValue(cModel,'NT9_CGC'))) > 11)
				oModelJ95:LoadValue(cModel, 'NT9_TIPOP', '2')
			Else
				oModelJ95:LoadValue(cModel, 'NT9_TIPOP', '1')
			EndIf

			oModelJ95:SetValue(cModel,  "NT9_PRINCI", oDadosNZZ[1]['Envolvidos'][nReg]["NT9_PRINCI"])

			// Controla o Principal envolvido
			If ( oDadosNZZ[1]['Envolvidos'][nReg]["NT9_TIPOEN"] = "2" .And. Len(aUnidade) > 0)
				oModelJ95:SetValue(cModel, "NT9_PRINCI", '2')
			EndIf

			oModelJ95:SetValue(cModel, "NT9_TIPOEN", oDadosNZZ[1]['Envolvidos'][nReg]["NT9_TIPOEN"])
			oModelJ95:SetValue(cModel, "NT9_CTPENV", oDadosNZZ[1]['Envolvidos'][nReg]["NT9_CTPENV"])
			oModelJ95:LoadValue(cModel, "NT9_NOME", oDadosNZZ[1]['Envolvidos'][nReg]["NT9_NOMEEN"])
			
		Next nReg
	Else
		//Carrega Detlhe do Processo
		For nField := 1 To Len(aStruct)

			//Seta valor do Modelo
			xValue := GetValue( aModel, aStruct[nField][1], .T. )
	
			If (Empty(xValue))
				//Seta valor da NZZ
				xValue := oDadosNZZ[1][aStruct[nField][1]]
			EndIf

			If !(Empty(xValue))
				cType := AllTrim( GetSx3Cache(aStruct[nField][1],"X3_TIPO") )
				//Corrige a tipagem
				If cType = 'N'
					xValue := Val(CValToChar(xValue)) 
				ElseIf  cType = 'D'
					xValue := SToD(strtran(CValToChar(xValue),'-',''))
				EndIf

				If ( oModelJ95:CanSetValue(cModel,aStruct[nField][1]) )
						oModelJ95:LoadValue(cModel, aStruct[nField][1], xValue)
				EndIf

				// Seta data do valor da causa
				If( 'NUQ_DTDIST' == aStruct[nField][1] )
					If ( oModelJ95:CanSetValue('NSZMASTER', 'NSZ_DTCAUS') )
						oModelJ95:LoadValue('NSZMASTER', 'NSZ_DTCAUS', xValue)
					EndIf
				EndIf
			EndIf
		Next nField
	EndIf

Return .T.
//----------------------------------------------------------------------------------------------------
/*/{Protheus.doc} GetValue(aModelo, cCampo, lModelo)
Buscao  valor do campo em um array de modelo

@param aModelo  - Array contendo os dados dos campos
@param cCampo   - Nome do campo procurado
@param lModelo  - Indica se � um array de modelos ou de campos da NZZ

@since 	 10/08/2020
/*/
//----------------------------------------------------------------------------------------------------
Static Function GetValue(aModelo, cCampo, lModelo)
Local nPos := 0
Local xRet := Nil

	nPos := aScan(aModelo,{|x| x[1] == cCampo})

	If nPos > 0
		If(lModelo)
			xRet := aModelo[nPos][3]
		Else
			xRet := aModelo[nPos][2]
		EndIf
	EndIf

Return xRet 

/*/{Protheus.doc} GetCFV(cNumPro)
Buscao  Comarca, Foro e Vara dado o n�mero do processo

@param cNumPro  - N�mero do processo

@return aCFV    -  Retorana array com tr�s posi��es sendo:
					1= C�digo Comarca
					2= C�digo Foro
					3= C�digo Vara

@since 	 10/08/2020
/*/
Static Function GetCFV(cNumPro)
Local aArea     := GetArea()
Local cJustica  := ""
Local cTribunal := ""
Local cOrigem   := ""
Local cMascara  := ""
Local aCFV      := {}

	If ( Len(cNumPro) >= 20 )
		// Limpa o n� processo
		cNumPro:= StrTran(cNumPro, '-')
		cNumPro:= Alltrim( StrTran(cNumPro, '.') )

		cJustica  := SubStr(cNumPro, 14, 1)
		cTribunal := SubStr(cNumPro, 15, 2)
		cOrigem   := SubStr(cNumPro, 17, 4)

		cMascara := cJustica + '.' + cTribunal + '.' + cOrigem

		dbSelectArea("O00") // M�scara CNJ
		O00->(dbSetOrder(1)) // O00_FILIAL+O00_MASCAR+O00_CCOMAR+O00_CLOC2N+O00_CLOC3N

		If O00->(dbSeek( xFilial("O00") + cMascara ) )
			Aadd(aCFV, O00->O00_CCOMAR ) // Comarca
			Aadd(aCFV, O00->O00_CLOC2N ) // Foro
			Aadd(aCFV, O00->O00_CLOC3N ) // Vara
		Endif
	EndIf

	RestArea(aArea)

Return aCFV

//----------------------------------------------------------------------------------------------------
/*/{Protheus.doc} GetInst(cForo)
Busca a inst�ncia dado o c�digo do foro

@param cForo    - C�digo do Foro

@return cInstan -  retorna qual a inst�ncia.

@since 	 10/08/2020
/*/
//----------------------------------------------------------------------------------------------------
Static Function GetInst(cForo)
Local cInstan := ''
Local aArea   := GetArea() 

	dbSelectArea("NQC") // Foro / Tribunal
		NQC->(dbSetOrder(1)) // NQC_FILIAL+NQC_COD

		If NQC->(dbSeek( xFilial("NQC") + cForo) )
			cInstan := NQC->NQC_INSTAN
		Endif

		If Empty(cInstan)
			cInstan := '1'
		EndIf 
	RestArea(aArea)
	
Return cInstan

//----------------------------------------------------------------------------------------------------
/*/{Protheus.doc} GravaAud(cCajuri, cTipo, cPart, cDtAudi , cHora )
Cria o Fup de audi�ncia

@param cCajuri - C�digo do processo
@param cTipo   - C�digo do tipo de FUP
@param cPart   - Sigla do participante respons�vel
@param cDtAudi - Data da audi�ncia
@param cHora   - Hora da audi�ncia

@since 	 10/08/2020
/*/
//----------------------------------------------------------------------------------------------------
Static Function GravaAud(cCajuri, cTipo, cPart, cDtAudi , cHora )
Local oModelJ106 := FWLoadModel("JURA106")
Default cHora    := ""

	oModelJ106:SetOperation(MODEL_OPERATION_INSERT)
	oModelJ106:Activate()

	oModelJ106:LoadValue("NTAMASTER", "NTA_CAJURI", cCajuri)
	oModelJ106:LoadValue("NTAMASTER", "NTA_CTIPO", cTipo)
	oModelJ106:LoadValue("NTAMASTER", "NTA_DTFLWP", SToD(cDtAudi))
	oModelJ106:LoadValue("NTAMASTER", "NTA_HORA", SubStr(cHora, 1,2) + SubStr(cHora, 4,2))
	oModelJ106:GetModel("NTEDETAIL"):SetValue("NTE_SIGLA", cPart)

	If ( oModelJ106:VldData() )
		oModelJ106:CommitData() 
	EndIf

	oModelJ106:Destroy()

Return .T.

//----------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AtuDistr(cCajuri, cCodNZZ, lSucesso, aMsg)
Atualiza o Status de uma distribui��o

@param cCajuri  - C�digo do processo gerado � partir da distribui��o
@param cCodNZZ  - C�digo da dsitribui��o
@param lSucesso - Indica se a inforta��o foi efetuada ou houve erro
@param aMsg     - Array com a mensagem de erro da importa��o

@return cLinks  -  String contendo link dos documentos para download.

@since 	 10/08/2020
/*/
//----------------------------------------------------------------------------------------------------
Static Function AtuDistr(cCajuri, cCodNZZ, lSucesso, aMsg)
Local aArea      := GetArea()
Local aAreaNZZ   := NZZ->( GetArea() )
Local cLinks     := ""
Local cMsg       := ""
Local cNumPro    := ""
Default lSucesso := .T.
Default aMsg     := {}

	DbSelectArea("NZZ")
	NZZ->(DbSetOrder(1)) //NZZ_FILIAL+NZZ_COD

		If dbSeek(xFilial("NZZ") + cCodNZZ)

			cNumPro := NZZ->NZZ_NUMPRO

			RecLock("NZZ", .F.)
				If (lSucesso)
					//Atualiza o status da distribui��o e atribui o c�digo do processo
					NZZ->NZZ_STATUS := "2"
					NZZ->NZZ_CAJURI := cCajuri
					NZZ->NZZ_ERRO := ""
					cLinks:= NZZ->NZZ_LINK
				Else
					cMsg := SubStr("Erro: " + aMsg[6] , 1, 250)
					NZZ->NZZ_ERRO := cMsg
					NZZ->NZZ_STATUS := "1"
				EndIf
			NZZ->( MsUnLock() )
		EndIf

	NZZ->(DbCloseArea())

	If(lSucesso)
		cMsg := (STR0060 + cNumPro + STR0061) //"A Distribui��o de n�mero "  + cNumPro +  " foi importada com sucesso!"
		JA280Notify(cMsg, , , '2', "ImpDistr", 'processo/'+Encode64(xFilial('NSZ'))+"/"+Encode64(cCajuri) )
	Else
		cMsg := (STR0059 + cNumPro) // "Falha na importa��o da distribui��o: "
		JA280Notify(cMsg, , 'minus-circle', '2', "ImpDistr", 'Distribuicao/detalhe/'+Encode64(xFilial('NSZ')+cCodNZZ) )
	EndIf

	RestArea(aAreaNZZ)
	RestArea(aArea)

Return cLinks

//----------------------------------------------------------------------------------------------------
/*/{Protheus.doc} GetUnidade(cCodCli, codLoja)
Busca dados da unidade

@param cCodCli  - C�digo do Cliente
@param codLoja  - C�digo da Loja

@return aRet, Retorna um array com 3 posi��es, sendo:
			 1= Nome do Cliente, 
			 2= CPF/CNPJ
			 3= Tipo de Pessoa (1=F�sica; 2=Jur�dica)

@since 	 10/08/2020
/*/
//----------------------------------------------------------------------------------------------------
Static Function GetUnidade(cCodCli, codLoja)
Local cAlias := GetNextAlias()
Local cQuery := ""
Local aRet   := {}

	cCodCli := cValToChar(cCodCli)
	codLoja := cValToChar(codLoja)

	cQuery := "SELECT A1_NOME, A1_CGC FROM " + RetSqlName('SA1') 
	cQuery += " WHERE A1_FILIAL = " + "'" + xFilial('SA1') + "' AND A1_COD = '" + cCodCli + "' AND A1_LOJA = '"+codLoja+"'"
	cQuery := ChangeQuery(cQuery)

	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )
		If (cAlias)->(!Eof())
			Aadd(aRet, (cAlias)->A1_NOME)
			Aadd(aRet, (cAlias)->A1_CGC)
			Aadd(aRet, Iif( Len((cAlias)->A1_CGC) > 11  , '2', '1' ) )
		EndIf
	(cAlias)->( DbCloseArea() )

Return aRet

//----------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Importando(cCodNZZ)
Atualiza o status da distribui��o para 5= importando
@Param cCodNZZ - c�digo da distribui��o

@version 1.0
/*/
//----------------------------------------------------------------------------------------------------
Static Function Importando(cCodNZZ)
Local aArea := GetArea() 

	DbSelectArea("NZZ")
		NZZ->(DbSetOrder(1)) //NZZ_FILIAL+NZZ_COD

			If dbSeek(cCodNZZ)

				RecLock("NZZ", .F.)
					NZZ->NZZ_STATUS := "5"
				NZZ->( MsUnLock() )

			EndIf

		NZZ->(DbCloseArea())

	RestArea(aArea)

Return .T.


