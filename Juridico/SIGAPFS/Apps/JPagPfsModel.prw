#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} JPagPfsModel
Classe dos modelos para os App do PFS

@since 07/07/2020
/*/
//-------------------------------------------------------------------

Class JPagPfsModel From FwRestModel
	Method Activate()
	Method SetFilter()
	Method SetModel()
EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} Activate()
Tratamento para Ativar o modelo

@since 07/07/2020
/*/
//-------------------------------------------------------------------
Method Activate() Class JPagPfsModel
Local cVerbo     := self:GetHttpHeader("_METHOD_")
Local cPath      := self:GetHttpHeader("_PATH_")
Local cId        := ""
Public lF050Auto := .F.

	Do Case 
		Case IsInPath(cPath, "JURA246")
			cId := SubStr(cPath, RAt("/", cPath) - Len(cPath))
			If (cId != "JURA246")
				cId := Decode64(cId)
				
				DbSelectArea("SE2")
				SE2->( DbSetOrder(1) ) //E2_FILIAL+E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO+E2_FORNECE+E2_LOJA
				SE2->( DbSeek(cId) )
			EndIf
	End Case
Return _Super:Activate()

//-------------------------------------------------------------------
/*/{Protheus.doc} SetFilter()
Filtro recebido da requisi��o.

@since 07/08/2020
/*/
//-------------------------------------------------------------------
Method SetFilter(cFilter)  Class JPagPfsModel
Local cVerbo     := self:GetHttpHeader("_METHOD_")
Local cPath      := self:GetHttpHeader("_PATH_")
Local cQuery     := self:GetHttpHeader("_QUERY_")
Local cFiltFil   := ""

	If (cVerbo == "GET") && ( "FILTFIL=" $ Upper(cQuery) )
		Do Case 
			Case IsInPath(cPath, "JPPJUR148")
				cFiltFil := "A1_FILIAL='" + FWxFilial("SA1") + "'"
			Case IsInPath(cPath, "JPPMAT020")
				cFiltFil := "A2_FILIAL='" + FWxFilial("SA2") + "'"
			Case IsInPath(cPath, "JPPJUR159")
				cFiltFil := "RD0_FILIAL='" + FWxFilial("RD0") + "'"
			cASE IsInPath(cPath, "JPPCTB140")
				cFIltFil := "CTO_FILIAL='" + FWxFilial("CTO") + "'"
			Otherwise
				cFiltFil := ""
		End Case

		If !Empty(cFiltFil)
			If !Empty(cFilter)
				cFilter += " AND "
			EndIf

			cFilter += cFiltFil
		EndIf
	EndIf

Return _Super:SetFilter(cFilter)

//-------------------------------------------------------------------
/*/{Protheus.doc} SetModel
M�todo para setar o modelo de dados que ser� utilizado.
E for�a a configura��o do alias a ser utilizado.
@param  oModel  Objeto do modelo de dados
@return oModel  Objeto do modelo de dados setado.

@since 07/08/2020
@version P11, P12
/*/
//-------------------------------------------------------------------
Method SetModel(oModel) Class JPagPfsModel
Return _Super:SetModel(oModel)

//-------------------------------------------------------------------
/*/{Protheus.doc} IsInPath(cPath, cFonte)
Verifica se o Endpoint (cFonte) est� na requisi��o (cPath)

@Param cPath - Path completo da requisi��o
@Param cFonte - Nome do fonte a ser verificado

@since 30/09/2019
/*/
//-------------------------------------------------------------------
Static Function IsInPath(cPath, cFonte)
Default cPath  := ""
Default cFonte := ""
Return  cFonte $ cPath


Static Function mkFiltTbl(cEntidade)
Local aCompTab   := {}
Local aFiliais   := LoadSM0()
Local cFilEntida := cFilAnt

	aAdd(aCompTab, FWModeAccess(cEntidade,1)) // Empresa 
	aAdd(aCompTab, FWModeAccess(cEntidade,2)) // Unidade 
	aAdd(aCompTab, FWModeAccess(cEntidade,3)) // Filial


Return cFilEntida

// Modelos utilizados nos Apps com fontes de outras equipes
PUBLISH MODEL REST NAME JVALACESSORIO    SOURCE FINA050VA RESOURCE OBJECT JPagPfsModel      //Valores acess�rios - FINA050VA
PUBLISH MODEL REST NAME JTITPAGAR        SOURCE FINA050   RESOURCE OBJECT JPagPfsModel      //Titulo a pagar - FINA050
PUBLISH MODEL REST NAME JFNCONTASPAGAR   SOURCE FINA750   RESOURCE OBJECT JPagPfsModel      //Fun��es contas a pagar  - FINA750
PUBLISH MODEL REST NAME JCOMPTITULO      SOURCE FINA986   RESOURCE OBJECT JPagPfsModel      //Complemento de T�tulo - FINA986
PUBLISH MODEL REST NAME JTRACKERCONTABIL SOURCE CTBC662   RESOURCE OBJECT JPagPfsModel      //Tracker Cont�bil - CTBC662

// Modelos utilizados nos Apps do SIGAPFS
PUBLISH MODEL REST NAME JURA246          SOURCE JURA246  RESOURCE OBJECT JPagPfsModel       //Desdobramento - JURA246
PUBLISH MODEL REST NAME JURA247          SOURCE JURA247  RESOURCE OBJECT JPagPfsModel       //Desdobramento P�s-pagamento - JURA247
PUBLISH MODEL REST NAME JURA277          SOURCE JURA277  RESOURCE OBJECT JPagPfsModel       //Titulo do Pagar - FWSE2
PUBLISH MODEL REST NAME JURA278          SOURCE JURA278  RESOURCE OBJECT JPagPfsModel       //Item do Projeto - FWOHM
PUBLISH MODEL REST NAME JURA240          SOURCE JURA240  RESOURCE OBJECT JPagPfsModel       // Hist�rico Padr�o - JURA240

// Modelos presentes no JurRestModel mas que precisam do tratamento do JPagPfsModel
PUBLISH MODEL REST NAME JPPJUR148        SOURCE JURA148 RESOURCE OBJECT JPagPfsModel       //Clientes - JURA148
PUBLISH MODEL REST NAME JPPJUR159        SOURCE JURA159 RESOURCE OBJECT JPagPfsModel       //Participantes - JURA159
PUBLISH MODEL REST NAME JPPJUR238        SOURCE JURA238 RESOURCE OBJECT JPagPfsModel       //Tabela Rateio - JURA238
PUBLISH MODEL REST NAME JPPMAT020        SOURCE MATA020 RESOURCE OBJECT JPagPfsModel       //Fornecedor - MATA020
PUBLISH MODEL REST NAME JPPCTB140        SOURCE CTBA140 RESOURCE OBJECT JPagPfsModel          //Moedas Cont�beis - CTBA140
