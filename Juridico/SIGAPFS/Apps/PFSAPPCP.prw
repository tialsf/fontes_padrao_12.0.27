#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH"
#INCLUDE "PFSAPPCP.CH"
//-------------------------------------------------------------------
/*/{Protheus.doc} PFSAPPCP
App do contas a pagar

@author Willian Yoshiaki Kazahaya
@since 02/07/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Function PFSAPPCP()
	// 1� Param: Nome da Aplica��o
	// 6� Param: Nome do fonte caso seja diferente do App
	FWCallApp("PFSAPPCP")
Return NIL

//-------------------------------------------------------------------
/*/{Protheus.doc} JsToAdvpl(oWebChannel, cType, cContent)
Fun��o que pode ser chamada do PO-UI quando dentro do Protheus

@param oWebChannel - TWebEngine utilizado para renderizar o PO-UI
@param cType - Par�metro de tipo
@param cContent - Conteudo passado pelo PO-UI

@author Willian Yoshiaki Kazahaya
@since 07/07/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function JsToAdvpl(oWebChannel, cType, cContent)
Local cJsonCompany := ""
Local aFiliais     := {}
	conout(cType)
	conout(cContent)
	Do Case
		Case cType == "preLoad"
			sendFilEmp(oWebChannel)
		Case cType == "a"
			JURA010()
		Case cType == "anexo"
			Processa({ || CriaAnx(cContent, .F. )}, STR0002, STR0004, .F.) //"Gerando planilha" "Aguarde..."
		Case cType == "exportar"
			Processa({ || CriaAnx(cContent, .T. )}, STR0003, STR0004, .F.) //"Gerando impress�o" "Aguarde..."
	EndCase
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} CriaAnx(cContent, lTemp)
Cria o arquivo excel utilizando as fun��es do Protheus

@param cContent - Conteudo do arquivo em formato blob
@param lTemp - Flag de arquivo tempor�rio ou n�o

@author Willian Yoshiaki Kazahaya
@since 07/07/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function CriaAnx(cContent, lTemp)
Local oReqJSon    := Nil
Local aArqData    := {}
Local nI          := 0
Local nRet        := 0
Local cCamArq     := ""
Local cExcel      := ""
Local lCriaPast   := .F.
Local oExcel

Default lTemp     := .F.

	IncProc()

	// Verifica se a cria��o ser� em diret�rio tempor�rio ou n�o
	If lTemp
		cCamArq   := GetTempPath()
		lCriaPast := JurMkDir(cCamArq,.F.,.F.)
		cCamArq   += "tmprel" + GetMark()
	Else
		cCamArq     := cGetFile(STR0005 + "|*.*", STR0006, , "C:\", .F., nOr(GETF_LOCALHARD, GETF_NETWORKDRIVE), ,.F.) //"Todos os arquivos" "Selecionar caminho"
	EndIf

	// Verifica se h� caminho para o arquivo ser criado
	If (!Empty(cCamArq))
		If At(".xls",cCamArq) == 0
			cCamArq += ".xlsx"
		Endif

		FWJsonDeserialize(cContent,@oReqJSon)
		aArqData := oReqJSon:data

		nHDestino := FCREATE(cCamArq)
		
		For nI := 1 To Len(aArqData)
			cExcel += Chr(aArqData[nI])
		Next nI

		nBytesSalvo := FWRITE(nHDestino, cExcel)

		FCLOSE(nHDestino)

		if lTemp
			nRet := ShellExecute( 'open', cCamArq , '', "C:\", 1 )
		EndIf

		ApMsgInfo(STR0007) //"O documento foi gerado com sucesso!"
	EndIf
Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} sendFilEmp
Envia os dados de pr�-carregamento para o Angular

@author Willian.Kazahaya
@since 04/08/2020
/*/ 
//-------------------------------------------------------------------
Static Function sendFilEmp(oWebChannel)
Local cJsonCompany := ""
Local aJson        := {}
Local aFiliais     := FWLoadSM0()
Local nI           := 0
Local nPos         := 0
Local oJSonResp    := Nil

 	oJSonResp := JsonObject():New()

	For nI := 1 To Len(aFiliais)
		If (aFiliais[nI][11])
			Aadd(aJson,JsonObject():new())
			nPos := Len(aJson)
			aJson[nPos]['empresa' ]   := aFiliais[nI][1]
			aJson[nPos]['filial' ]    := aFiliais[nI][2]
			aJson[nPos]['descricao' ] := aFiliais[nI][7]
		EndIf
	Next nI

	oJSonResp:Set(aJson)
	cJsonCompany	:=	'{ "company_code" : "' + FWGrpCompany() + '", "branch_code":"' + FWCodFil() + '"}'
	oWebChannel:AdvPLToJS( "setCompany"   , cJsonCompany  )
	oWebChannel:AdvPLToJS( "setListFilUsu", Encode64(oJSonResp:toJSon()))

Return Nil
