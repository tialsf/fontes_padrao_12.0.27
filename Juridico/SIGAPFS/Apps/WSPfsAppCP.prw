#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "WSPFSAPPCP.CH"

WSRESTFUL WSPfsAppCP DESCRIPTION STR0001
	WSDATA recnoTitulo  AS STRING
	WSDATA ChaveBordero AS STRING
	WSDATA filtLeg      AS STRING
	WSDATA codEntidade  AS STRING
	WSDATA codFields    AS STRING
	WSDATA desFields    AS STRING
	WSDATA extFields    AS STRING
	WSDATA valorDig     AS STRING
	WSDATA filtRelSA2   AS STRING
	WSDATA searchKey    AS STRING
	WSDATA chaveTab     AS STRING
	WSDATA codParam     AS STRING
	WSDATA valorCod     AS STRING
	WSDATA valorLoja    AS STRING

	WSDATA pageSize     AS NUMBER
	WSDATA page         AS NUMBER

	// M�todos GET
	WSMETHOD GET gtPrBaixAut       DESCRIPTION STR0002 PATH "param/baixaAut"                 PRODUCES APPLICATION_JSON // "Busca de par�metros para Baixa Autom�tica"
	WSMETHOD GET cmpFiltro         DESCRIPTION STR0003 PATH "filcmp"                         PRODUCES APPLICATION_JSON // "Busca os campos para filtro"
	WSMETHOD GET FornTitulo        DESCRIPTION STR0004 PATH "forntitpag"                     PRODUCES APPLICATION_JSON // "Busca os fornecedores que est�o nos titulos"
	WSMETHOD GET TabGenerica       DESCRIPTION STR0005 PATH "congen/sx5/{codEntidade}"       PRODUCES APPLICATION_JSON // "Busca nas tabelas gen�ricas (SX5)"
	WSMETHOD GET ConGenerica       DESCRIPTION STR0006 PATH "congen/tab/{codEntidade}"       PRODUCES APPLICATION_JSON // "Consulta gen�rica de tabelas"
	WSMETHOD GET SysParam          DESCRIPTION STR0068 PATH "sysParam/{codParam}"            PRODUCES APPLICATION_JSON // "Consulta de Par�metros do sistema"
	WSMETHOD GET CliJuri           DESCRIPTION STR0069 PATH "congen/cli"                     PRODUCES APPLICATION_JSON //"Consulta de Clientes habilitados"

	// M�todos PUT
	WSMETHOD PUT DadosTit          DESCRIPTION STR0007 PATH "titpag"                         PRODUCES APPLICATION_JSON // "Busca os dados dos Titulos do Pagar"
	WSMETHOD PUT SubTitPag         DESCRIPTION STR0008 PATH "subtitpag"                      PRODUCES APPLICATION_JSON // "Substituir o Titulo do Pagar"
	WSMETHOD PUT AltTitPag         DESCRIPTION STR0009 PATH "titpag/{recnoTitulo}"           PRODUCES APPLICATION_JSON // "Alterar o Titulo do Pagar"
	WSMETHOD PUT BaixaTitulo       DESCRIPTION STR0010 PATH "titbai/{recnoTitulo}"           PRODUCES APPLICATION_JSON // "Baixa Manual de Titulo do Pagar"
	WSMETHOD PUT SimpDesdobram     DESCRIPTION STR0011 PATH "desdobr/simples/{recnoTitulo}"  PRODUCES APPLICATION_JSON // "Inclus�o/Altera��o de Desdobramento Simples"
	WSMETHOD PUT TrDesdobramento   DESCRIPTION STR0012 PATH "desdobr/transit/{recnoTitulo}"  PRODUCES APPLICATION_JSON // "Inclus�o/Altera��o de Desdobramento Grid"
	WSMETHOD PUT PPagDesdobram     DESCRIPTION STR0013 PATH "desdobr/pospag/{recnoTitulo}"   PRODUCES APPLICATION_JSON // "Inclus�o/Altera��o de Desdobr. P�s Pagto"

	// M�todos POST
	WSMETHOD POST CrtTitPag        DESCRIPTION STR0014 PATH "titpag"                         PRODUCES APPLICATION_JSON // "Cria��o de titulo do Pagar"
	WSMETHOD POST LiberaManu       DESCRIPTION STR0015 PATH "libpagmanual"                   PRODUCES APPLICATION_JSON // "Libera��o Manual de Pagamento"
	WSMETHOD POST ChqSTitulo       DESCRIPTION STR0016 PATH "chequesemtitulo/{recnoTitulo}"  PRODUCES APPLICATION_JSON // "Cheque sem titulo"
	WSMETHOD POST BxAutTit         DESCRIPTION STR0017 PATH "titautbai"                      PRODUCES APPLICATION_JSON // "Baixa de Titulo Automatico"
	WSMETHOD POST PrBaixAut        DESCRIPTION STR0018 PATH "param/baixaAut"                 PRODUCES APPLICATION_JSON // "Altera��o de Par�metros para Baixa do Pagar"

	// M�todos DELETE
	WSMETHOD DELETE Titulo         DESCRIPTION STR0020 PATH "titpag/{recnoTitulo}"           PRODUCES APPLICATION_JSON // "Exclus�o de Titulo"
	WSMETHOD DELETE ExcluiBaixa    DESCRIPTION STR0021 PATH "titexc/{recnoTitulo}"           PRODUCES APPLICATION_JSON // "Exclus�o de baixa do Pagar"
	WSMETHOD DELETE CancelaBaixa   DESCRIPTION STR0022 PATH "titcan/{recnoTitulo}"           PRODUCES APPLICATION_JSON // "Cancela de baixa do Pagar"
	WSMETHOD DELETE Bordero        DESCRIPTION STR0023 PATH "canbrd/{ChaveBordero}"          PRODUCES APPLICATION_JSON // "Cancelamento de Border�"
	WSMETHOD DELETE IBordero       DESCRIPTION STR0024 PATH "canbrdimp/{ChaveBordero}"       PRODUCES APPLICATION_JSON // "Cancelamento de Border� - Imposto"
	WSMETHOD DELETE FatPag         DESCRIPTION STR0025 PATH "canfat/{recnoTitulo}"           PRODUCES APPLICATION_JSON // "Cancelamento de Fatura"
	WSMETHOD DELETE CCompensacao   DESCRIPTION STR0026 PATH "compens/{recnoTitulo}"          PRODUCES APPLICATION_JSON // "Cancelar Compensa��o"
	WSMETHOD DELETE ECompensacao   DESCRIPTION STR0027 PATH "compens/estornar/{recnoTitulo}" PRODUCES APPLICATION_JSON // "Estornar Compensa��o"
	WSMETHOD DELETE Cheque         DESCRIPTION STR0028 PATH "cheque/{recnoTitulo}"           PRODUCES APPLICATION_JSON // "Cancelar Cheque"

END WSRESTFUL

//-------------------------------------------------------------------
/*/{Protheus.doc} GET FornTitulo
Retorna os par�metros de Baixa autom�tica

@example GET -> http://127.0.0.1:9090/rest/WSPfsAppCP/forntitpag

@author Willian Kazahaya
@since 06/04/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD GET FornTitulo WSREST WSPfsAppCP
Local oResponse := JSonObject():New()
Local cQrySel   := ""
Local cQryFrm   := ""
Local cQryWhr   := ""
Local cQryGrp   := ""
Local cQryOrd   := ""
Local cQuery    := ""
Local cAlias    := ""
Local lRet      := .T.
Local nIndexJSon := 0

	cQrySel += " SELECT SA2.A2_COD, "
	cQrySel +=        " SA2.A2_LOJA, "
	cQrySel +=        " SA2.A2_NOME "
	cQryFrm += " FROM " + RetSqlName("SA2") + " SA2 INNER JOIN " + RetSqlName("SE2") + " SE2 ON (SA2.A2_COD = SE2.E2_FORNECE  "
	cQryFrm +=                                                                             " AND SA2.A2_LOJA = SE2.E2_LOJA) "
	cQryWhr += " WHERE SA2.D_E_L_E_T_ = ' ' "
	cQryWhr +=   " AND SE2.D_E_L_E_T_ = ' ' "
	cQryGrp += " GROUP BY SA2.A2_COD, "
	cQryGrp +=          " SA2.A2_LOJA, "
	cQryGrp +=          " SA2.A2_NOME "
	cQryOrd += " ORDER BY SA2.A2_NOME "

	cQuery := ChangeQuery( cQrySel + cQryFrm + cQryWhr + cQryGrp + cQryOrd )

	cAlias := GetNextAlias()
	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )
	oResponse['fornecedor'] := {}

	While !(cAlias)->(Eof())
		nIndexJSon++
		Aadd(oResponse['fornecedor'], JsonObject():New())
		oResponse['fornecedor'][nIndexJSon]['codigo']   := (cAlias)->A2_COD
		oResponse['fornecedor'][nIndexJSon]['loja']     := (cAlias)->A2_LOJA
		oResponse['fornecedor'][nIndexJSon]['nome']     := (cAlias)->A2_NOME
		(cAlias)->( dbSkip() )
	End

	( cAlias )->( dbCloseArea() )
	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} SysParam
Busca de Par�metros da SX6

@author willian.kazahaya
@since 31/07/2020
/*/
//-------------------------------------------------------------------
WSMethod GET SysParam PATHPARAM codParam WSREST WSPfsAppCP
	Local oResponse  := JsonObject():New()
	Local cCodParam  := AllTrim(Self:codParam)

	If Len(cFilant) < 8
		cFilant := PadR(cFilAnt,8)
	EndIf

	oResponse['sysParam'] := JsonObject():New()

	oResponse['sysParam']['name']  := cCodParam
	oResponse['sysParam']['value'] := SuperGetMv(cCodParam)

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))
Return .T.


//-------------------------------------------------------------------
/*/{Protheus.doc} GET CliJuri
Consulta de Cliente ativo com NUH vinculada

@example GET -> http://127.0.0.1:9090/rest/WSPfsAppCP/congen/cli

@author Willian Kazahaya
@since 06/04/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMethod GET CliJuri QUERYPARAM valorDig, valorCod, valorLoja WSREST WSPfsAppCP
Local oResponse := JSonObject():New()
Local cAlias    := ""
Local cQuery    := ""
Local cQrySel   := ""
Local cQryFrm   := ""
Local cQryWhr   := ""
Local cQryOrd   := ""
Local nIndexJSon:= 0
Local cSearchKey:= Self:valorDig
Local cCodigo   := Self:valorCod
Local cLoja     := Self:valorLoja

	cQrySel += " SELECT SA1.A1_COD "
	cQrySel +=       " ,SA1.A1_LOJA "
	cQrySel +=       " ,SA1.A1_NOME "	  
	cQryFrm += " FROM " + RetSqlName("SA1") + " SA1 INNER JOIN " + RetSqlName("NUH") + " NUH ON (NUH.NUH_COD = SA1.A1_COD "
	cQryFrm +=                                                                             " AND NUH.NUH_LOJA = SA1.A1_LOJA "
	cQryFrm +=                                                                             " AND NUH.D_E_L_E_T_ = ' ') "
	cQryWhr += " WHERE NUH.NUH_ATIVO = '1' "
	cQryWhr +=   " AND SA1.A1_FILIAL = '" + FWxFilial("SA1") + "' "
	cQryWhr +=   " AND SA1.D_E_L_E_T_ = ' ' "
	cQryOrd += " ORDER BY SA1.A1_NOME "

	// Pesquisa por valor digitado
	if !Empty(cSearchKey)
		cSearchKey := Upper(cSearchKey)
		cQryWhr += " AND ( UPPER(A1_NOME) LIKE '%" + AllTrim(cSearchKey) + "%'"
		cQryWhr +=    " OR UPPER(A1_COD) LIKE '%" + AllTrim(cSearchKey) + "%'"
		cQryWhr +=    " OR UPPER(A1_LOJA) LIKE '%" + AllTrim(cSearchKey) + "%')"
	EndIf

	// Pesquisa por c�digo
	If !Empty(cCodigo)
		cQryWhr += " AND UPPER(A1_COD) = '" + cCodigo + "'"
		cQryWhr += " AND UPPER(A1_LOJA) = '" + cLoja + "' "
	EndIf

	// Monta a Query
	cQuery := ChangeQuery( cQrySel + cQryFrm + cQryWhr + cQryOrd )
	cAlias := GetNextAlias()

	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )
	oResponse := {}

	// Monta o response
	While !(cAlias)->(Eof())
		nIndexJSon++
		Aadd(oResponse, JsonObject():New())
		oResponse[nIndexJSon]['codigo'] := (cAlias)->(A1_COD)
		oResponse[nIndexJSon]['loja']   := (cAlias)->(A1_LOJA)
		oResponse[nIndexJSon]['nome']   := JConvUTF8((cAlias)->(A1_NOME))

		(cAlias)->( dbSkip() )
	End
	( cAlias )->( dbCloseArea() )
	Self:SetResponse( FWJsonSerialize(oResponse, .F., .F., .T.) )	
Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} GET ConGenerica
Retorna os par�metros de Baixa autom�tica

@example GET -> http://127.0.0.1:9090/rest/WSPfsAppCP/param/baixaAut

@author Willian Kazahaya
@since 06/04/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD GET ConGenerica PATHPARAM codEntidade QUERYPARAM codFields, desFields, valorDig, extFields, filtRelSA2 WSREST WSPfsAppCP
Local oResponse  := JSonObject():New()
Local lRet       := .T.
Local nIndexJSon := 0
Local nI         := 0
Local cQuery     := ""
Local cAlias     := ""
Local cTabela    := Self:codEntidade
Local cQrySel    := " SELECT "
Local cQryFrm    := " FROM "
Local cQryWhr    := " WHERE " + cTabela + ".D_E_L_E_T_ = ' ' "
Local cQryGrp    := ""
Local cQryOrd    := ""
Local cCamposSel := ""
Local cRelac     := ""
Local cCampos    := Self:codFields + "," + Self:desFields
Local lFilRelSA2 := (!Empty(Self:filtRelSA2) .AND. Self:filtRelSA2 == "T")
Local aRelac     := {}
Local aCampos    := {}
Local aCmpFilt   := {}

	If !Empty(Self:extFields)
		cCampos += "," + Self:extFields
	EndIf

	aCampos := JStrArrDst( cCampos, "," )

	For nI := 1 to Len(aCampos)
		cCamposSel += aCampos[nI] + ","
	Next nI

	// Monta o Select e o GroupBy com os Campos passados
	cCamposSel := SubStr(cCamposSel, 1, Len(cCamposSel) - 1)
	cQrySel += cCamposSel
	cQryGrp += " GROUP BY " + cCamposSel

	// Monta o From
	cQryFrm += RetSqlName(cTabela) + " " + cTabela
	If (lFilRelSA2)
		Do Case
			Case cTabela == "SA2"
				cRelac := "SE2.E2_FORNECE = SA2.A2_COD AND SE2.E2_LOJA = SA2.A2_LOJA AND "
			Otherwise
				aRelac := JURSX9("SE2", cTabela)
				For nI := 1 To Len(aRelac)
					cRelac := cTabela + "." + AllTrim(aRelac[nI][1]) + " = SE2." + AllTrim(aRelac[nI][2]) + " AND "
				Next nI
		End

		cRelac  += " SE2.D_E_L_E_T_ = ' ' "
		cQryFrm += " INNER JOIN " + RetSqlName("SE2") + " SE2 ON (" + cRelac + ") "
	EndIf

	// Filtra caso tenha algum valor digitado
	If !Empty(self:valorDig)
		aCmpFilt   := JStrArrDst(Self:desFields, ",")
		cQryWhr += " AND ("

		For nI := 1 To Len(aCmpFilt)
			cQryWhr += " UPPER(" + aCmpFilt[nI] + ") LIKE '%" + Upper(self:valorDig) + "%' OR "
		Next nI

		cQryWhr := SubStr(cQryWhr, 1, Len(cQryWhr) - 3) + ")"
	EndIf

	// Monta a Query
	cQuery := ChangeQuery( cQrySel + cQryFrm + cQryWhr + cQryGrp + cQryOrd )
	conout(cQuery)
	cAlias := GetNextAlias()

	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )
	oResponse['item'] := {}

	// Monta o response
	While !(cAlias)->(Eof())
		nIndexJSon++
		If (nIndexJSon <= 10)
			Aadd(oResponse['item'], JsonObject():New())

			For nI := 1 To Len(aCampos)
				oResponse['item'][nIndexJSon][aCampos[nI]] := JConvUTF8((cAlias)->&(aCampos[nI]))
			Next nI
		EndIf
		(cAlias)->( dbSkip() )
	End

	( cAlias )->( dbCloseArea() )
	Self:SetResponse( FWJsonSerialize(oResponse, .F., .F., .T.) )
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} TabGenerica
Busca de Registro na tabela gen�rica (SX5)

@author willian.kazahaya
@since 27/08/2019
/*/
//-------------------------------------------------------------------
WSMethod GET TabGenerica PATHPARAM codEntidade WSRECEIVE searchKey, chaveTab WSREST WSPfsAppCP
	Local oResponse  := JSonObject():New()
	Local cQuery     := ""
	Local cAlias     := ""
	Local cCodTab    := Self:codEntidade
	Local cSearchKey := Self:searchKey
	Local cChaveTab  := Self:chaveTab
	Local nIndexJSon := 0

	cQuery := " SELECT SX5.X5_FILIAL, SX5.X5_TABELA, SX5.X5_CHAVE, SX5.X5_DESCRI, SX5.X5_DESCSPA, SX5.X5_DESCENG, SX5.R_E_C_N_O_ REC "
	cQuery += " FROM " + RetSqlName("SX5") + " SX5 "
	cQuery += " WHERE SX5.X5_TABELA = '" + Alltrim(cCodTab) + "' "

	If !Empty(cSearchKey)
		cQuery += " AND SX5.X5_DESCRI = '%" + AllTrim(cSearchKey) + "%'"
	ElseIf !Empty(cChaveTab)
		cQuery += " AND SX5.X5_CHAVE = '" + AllTrim(cChaveTab) + "'"
	Endif

	cAlias := GetNextAlias()
	DbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAlias, .F., .F. )

	oResponse['result'] := {}

	While !(cAlias)->(Eof())
		nIndexJSon++
		Aadd(oResponse['result'], JsonObject():New())
		oResponse['result'][nIndexJSon]['filial']    := (cAlias)->X5_FILIAL
		oResponse['result'][nIndexJSon]['tabela']    := (cAlias)->X5_TABELA
		oResponse['result'][nIndexJSon]['chave']     := (cAlias)->X5_CHAVE
		oResponse['result'][nIndexJSon]['descricao'] := JConvUTF8((cAlias)->X5_DESCRI)
		oResponse['result'][nIndexJSon]['descrispa'] := JConvUTF8((cAlias)->X5_DESCSPA)
		oResponse['result'][nIndexJSon]['descrieng'] := JConvUTF8((cAlias)->X5_DESCENG)
	
		(cAlias)->( dbSkip() )
	End

	(cAlias)->(dbCloseArea())

	Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))
Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} GET gtPrBaixAut
Retorna os par�metros de Baixa autom�tica

@example GET -> http://127.0.0.1:9090/rest/WSPfsAppCP/param/baixaAut

@author Willian Kazahaya
@since 06/04/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD GET gtPrBaixAut WSREST WSPfsAppCP
Local oResponse := JsonObject():New()
Local nI         := 0
Local lRet     := .F.
Local oObj := FWSX1Util():New()
Local aPergunte

	oObj:AddGroup("FIN090")
	oObj:SearchGroup()
	aPergunte := oObj:GetGroup("FIN090")

	Pergunte("FIN090", .F.)
	oResponse['params'] := {}
	If aPergunte != Nil
		For nI := 1 To Len(aPergunte[2])
			aAdd(oResponse['params'], JsonObject():New())
			oResponse['params'][nI]['id']    := JConvUTF8(aPergunte[2][nI]:CX1_ORDEM)
			oResponse['params'][nI]['desc']  := JConvUTF8(aPergunte[2][nI]:CX1_PERGUNT)
			oResponse['params'][nI]['tipo']  := JConvUTF8(aPergunte[2][nI]:CX1_TIPO)
			oResponse['params'][nI]['valor'] := &("MV_PAR" + aPergunte[2][nI]:CX1_ORDEM)

		Next nI
	EndIf

	If nI > 0
		lRet := .T.
		Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))
	Else
		setRespError(404, STR0029) //"Os par�metros n�o foram encontrados na base!"
	EndIf
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} EmpFilUsu
Retorna o array de Empresa/Filial que o usu�rio logado tem permiss�o

@author Willian Kazahaya
@since 06/04/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function EmpFilUsu(cEntidade)
Local aFiliais     := FWLoadSM0()
Local aFilUsu      := {}
Local aCompTab     := {}
Local nI           := 0
Local nY           := 0
Local cFilEntida   := ""

Default cEntidade  := ""

	If !Empty(cEntidade)
		aAdd(aCompTab, FWModeAccess(cEntidade,1)) // Empresa 
		aAdd(aCompTab, FWModeAccess(cEntidade,2)) // Unidade 
		aAdd(aCompTab, FWModeAccess(cEntidade,3)) // Filial
	EndIf

	For nI := 1 To Len(aFiliais)
		If (aFiliais[nI][11]) // Verifica se o usu�rio tem acesso. No LoadSM0 ele pega o usu�rio logado
			cFilEntida := ""
			If (aScan(aCompTab, "C") > 0) // Realiza tratamento no compartilhamento da tabela
				For nY := 1 To Len(aCompTab)
					If (aCompTab[nY] == "C")
						cFilEntida += Space(Len(aFiliais[nI][2+nY]))
					Else
						cFilEntida += aFiliais[nI][2+nY]
					EndIf
				Next nY
			Else 
				cFilEntida := aFiliais[nI][3] + aFiliais[nI][4] + aFiliais[nI][5]
			EndIf

			If (aScan(aFilUsu, cFilEntida) == 0)
				aAdd(aFilUsu, cFilEntida)
			EndIf
		EndIf
	Next nI
Return aFilUsu

//-------------------------------------------------------------------
/*/{Protheus.doc} JoinFilCom(cTabPai, cTabFil, cApePai, cApeFil, cCmpFilPai, cCmpFilFil)
Monta o Join de Filial levando em considera��o o compartilhamento da tabela, 
seja completamente compartilhando, parcialmente compartilhada ou totalmente exclusiva

@author Willian Kazahaya
@since 06/04/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function JoinFilCom(cTabPai, cTabFil, cApePai, cApeFil, cCmpFilPai, cCmpFilFil)
Local cCompFil    := ""
Local aCompTabPai := {}
Local aCompTabFil := {}
Local nFilLenPai  := Len(AllTrim(FWxFilial(cTabPai)))
Local nFilLenFil  := Len(AllTrim(FWxFilial(cTabFil)))
Local cFilialPai  := ""
Local cFilialFil  := ""
Local nFilLenLow  := 0

Default cApePai := cTabPai
Default cApeFil := cTabFil
Default cCmpFilPai := cTabPai + "_FILIAL"
Default cCmpFilFil := cTabFil + "_FILIAL"

	aAdd(aCompTabPai, FWModeAccess(cTabPai,1)) // Empresa 
	aAdd(aCompTabPai, FWModeAccess(cTabPai,2)) // Unidade 
	aAdd(aCompTabPai, FWModeAccess(cTabPai,3)) // Filial
	
	aAdd(aCompTabFil, FWModeAccess(cTabFil,1)) // Empresa 
	aAdd(aCompTabFil, FWModeAccess(cTabFil,2)) // Unidade 
	aAdd(aCompTabFil, FWModeAccess(cTabFil,3)) // Filial
	
	If ((nFilLenFil == nFilLenPai) .Or. (nFilLenFil != nFilLenPai .And. (nFilLenFil != 0 .And. nFilLenPai != 0)))
		nFilLenPai := filLen(aCompTabPai)
		nFilLenFil := filLen(aCompTabFil)
		
		If ( nFilLenPai != nFilLenFil ) .And. ( nFilLenFil > 0 .Or. nFilLenPai > 0)
			nFilLenLow := nFilLenPai

			If (nFilLenLow == 0 )
				nFilLenLow := nFilLenFil
			ElseIf (nFilLenFil < nFilLenLow .And. nFilLenFil > 0)
				nFilLenLow := nFilLenFil
			EndIf

			If (nFilLenLow > 0)
				cFilialFil := "SUBSTRING(" +cApeFil + "." + cCmpFilFil + ",1," + cValToChar(nFilLenLow) + ")"
			Else 
				cFilialFil := cApeFil + "." + cCmpFilFil
			EndIf

			If (nFilLenLow > 0)
				cFilialPai := "SUBSTRING(" +cApePai + "." + cCmpFilPai + ",1," + cValToChar(nFilLenLow) + ")"
			Else
				cFilialPai := cApePai + "." + cCmpFilPai
			EndIf

			cCompFil := " AND " + cFilialFil + " = " + cFilialPai
		Else
			cCompFil := " AND " + cApeFil  + "." + cCmpFilFil + " = " + cApePai + "." + cCmpFilPai
		EndIf
	EndIf
Return cCompFil

//-------------------------------------------------------------------
/*/{Protheus.doc} filLen(aCompTab)
Retorna a quantidade de caracteres presentes na filial da tabela

@author Willian Kazahaya
@since 06/04/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function filLen(aCompTab)
Local aFilAtu    := FwArrFilAtu()
Local aLenFil    := { Len(aFilAtu[13]), Len(aFilAtu[14]), Len(aFilAtu[15]) }
Local nLenFilial := aFilAtu[8]
Local nI         := 0
Local nIndSubStr := 0

	For nI := 1 To Len(aLenFil)
		If (aCompTab[nI] == "C")
			Exit
		Else
			nIndSubStr += aLenFil[nI]
		EndIf
	Next nI

	If (nIndSubStr == nLenFilial)
		nIndSubStr := 0
	EndIf
Return nIndSubStr
//-------------------------------------------------------------------
/*/{Protheus.doc} PUT DadosTit
Retorna de todos os titulos

@example PUT -> http://127.0.0.1:9090/rest/WSPfsAppCP/titpag

@author Willian Kazahaya
@since 06/04/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD PUT DadosTit QUERYPARAM filtLeg, pageSize, page WSREST WSPfsAppCP
Local oReqBody  := Nil
Local oJsonBody := JsonObject():new()
Local oJsonSub  := JsonObject():new()
Local oResponse := JsonObject():New()
Local aValLegen := Fa040Legenda("SE2")
Local aQtdLegen := {}
Local aTotalizad:= {0, 0, 0, 0} // Saldo / Total Desdobrado / Total Desdob. P�s-Pagto / Total Titulo
Local cAliasDb  := ""
Local cQrySel   := ""
Local cQryFrm   := ""
Local cQryWhr   := ""
Local cQryOrd   := ""
Local cQuery    := ""
Local cStatusTit:= ""
Local lRet      := .T.
Local lFiltraLeg:= !Empty(Self:filtLeg)
Local lFiltraPag:= (Self:pageSize != Nil .And. Self:page != Nil)
Local nIndex    := 0
Local nI        := 0
Local nY        := 0
Local nRegMin   := 0
Local nRegMax   := 0
Local nNumReg   := 0
Local nQtdRegTot:= 0
Local nValorTot := 0
Local aFiltros  := {}
Local aOrder    := {}
Local aFilFilt  := EmpFilUsu("SE2")

Local cBody     := ""
Local cChave    := ""
Local cTipo     := ""
Local cValor    := ""
Local cOrdConcat:= ""
Local cFilConcat:= ""
Local itensJs
Local retJson
Local cBdBase   :=  (Upper(TcGetDb()))

	cBody := StrTran(Self:GetContent(),CHR(10),"")
	retJson := oJsonBody:fromJson(cBody)
	itensJs := oJsonBody:GetNames()

	For nI := 1 To Len(itensJs)
		If (itensJs[nI] == "filtro")
			oJsonSub := oJsonBody:getJsonObject("filtro")
			For nY := 1 To Len(oJsonSub)
				cValor := oJsonSub[nY]["valor"] // oJsonSub[1][oJsonSub[1]:getNames()[1]] // Valor
				cChave := oJsonSub[nY]["chave"] // oJsonSub[1][oJsonSub[1]:getNames()[2]] // Chave
				Do Case
					Case cChave == "E2_EMISSAO" .OR. cChave == "E2_FORNECE" .OR. cChave == "E2_VENCREA"
						aAdd(aFiltros, { cChave, StrToKArr(cValor, "||") } )
					Otherwise
						aAdd(aFiltros, { cChave, cValor } )
				EndCase
			Next nY

			oJsonSub := Nil
		Elseif (itensJs[nI] == "ordem")
			oJsonSub := oJsonBody:getJsonObject("ordem")
			For nY := 1 To Len(oJsonSub)
				cChave := oJsonSub[nY]["campo"] // oJsonSub[1][oJsonSub[1]:getNames()[1]] // Valor
				cTipo  := oJsonSub[nY]["ordenacao"] // oJsonSub[1][oJsonSub[1]:getNames()[2]] // Chave
				aAdd(aOrder, { cChave , cTipo })
			Next nY

			oJsonSub := Nil
		EndIf
	Next nI

	// Verifica se haver� pagina��o e realiza os calculos
	If lFiltraPag
		nRegMax := Val(Self:pageSize) * Val(Self:page)
		nRegMin := (Val(Self:page)-1) * Val(Self:pageSize)
	EndIf

	// Monta o array para o Totalizador por Legenda
	For nI := 1 To Len(aValLegen)
		//              "Nome da legenda", Qtd, Soma Valor Titulo
		aAdd(aQtdLegen, {aValLegen[nI][2], 0, 0 })
	Next nI

	cQrySel := " SELECT SE2.E2_FILIAL "
	cQrySel +=       " ,SE2.E2_PREFIXO "
	cQrySel +=       " ,SE2.E2_NUM "
	cQrySel +=       " ,SE2.E2_PARCELA "
	cQrySel +=       " ,SE2.E2_TIPO "
	cQrySel +=       " ,SE2.E2_FORNECE "
	cQrySel +=       " ,SE2.E2_LOJA "
	cQrySel +=       " ,SA2.A2_NOME "
	cQrySel +=       " ,SE2.E2_NATUREZ "
	cQrySel +=       " ,SED.ED_DESCRIC "
	cQrySel +=       " ,SE2.E2_EMISSAO "
	cQrySel +=       " ,SE2.E2_VENCTO "
	cQrySel +=       " ,SE2.E2_VENCREA "
	cQrySel +=       " ,SE2.E2_VALOR "
	cQrySel +=       " ,SE2.E2_VLCRUZ "
	cQrySel +=       " ,SE2.E2_SALDO "
	cQrySel +=       " ,SE2.E2_BAIXA "
	cQrySel +=       " ,SE2.E2_DATALIB "
	cQrySel +=       " ,SE2.E2_HIST "

	if (cBdBase == "ORACLE")
		cQrySel +=       " ,COALESCE(TO_CHAR(OHF.VALOR),'0') VLRTRANSIT "
		cQrySel +=       " ,COALESCE(TO_CHAR(OHG.VALOR),'0') VLRPOSTRAN "
	Else
		cQrySel +=       " ,COALESCE(OHF.VALOR,'0') VLRTRANSIT "
		cQrySel +=       " ,COALESCE(OHG.VALOR,'0') VLRPOSTRAN "
	EndIf
	cQrySel +=       " ,SE2.R_E_C_N_O_ SE2RECNO " 
	cQrySel +=       " ,FK7.R_E_C_N_O_ FK7RECNO "
	cQryFrm := " FROM " + RetSqlName("SE2") + " SE2 INNER JOIN " + RetSqlName("SA2") + " SA2 ON (SA2.A2_COD = SE2.E2_FORNECE "
	cQryFrm +=                                                                             " AND SA2.A2_LOJA = SE2.E2_LOJA "
	cQryFrm +=                                                                             JoinFilCom("SE2", "SA2",,, "E2_FILIAL", "A2_FILIAL")
	cQryFrm +=                                                                             " AND SA2.D_E_L_E_T_ = ' ') "
	cQryFrm +=                " LEFT  JOIN " + RetSqlName("FK7") + " FK7 ON (FK7.FK7_CHAVE = SE2.E2_FILIAL || '|' ||  "
	cQryFrm +=                                                                             " SE2.E2_PREFIXO || '|' ||  "
	cQryFrm +=                                                                             " SE2.E2_NUM || '|' ||  "
	cQryFrm +=                                                                             " SE2.E2_PARCELA || '|' ||  "
	cQryFrm +=                                                                             " SE2.E2_TIPO || '|' ||  "
	cQryFrm +=                                                                             " SE2.E2_FORNECE || '|' ||  "
	cQryFrm +=                                                                             " SE2.E2_LOJA "
	cQryFrm +=                                                         " AND FK7.FK7_ALIAS = 'SE2'  "
	cQryFrm +=                                                         " AND FK7.D_E_L_E_T_ = ' ')  "
	cQryFrm +=                " LEFT  JOIN " + RetSqlName("SED") + " SED ON (SED.ED_CODIGO = SE2.E2_NATUREZ "
	cQryFrm +=                                                         JoinFilCom("SE2", "SED",,, "E2_FILIAL", "ED_FILIAL")
	cQryFrm +=                                                         " AND SED.D_E_L_E_T_ = ' ') "
	cQryFrm +=                " LEFT  JOIN (SELECT OHF_IDDOC "
	cQryFrm +=                                   ",SUM(OHF_VALOR) VALOR "
	cQryFrm +=                            " FROM " + RetSqlName("OHF") + "  "
	cQryFrm +=                            " WHERE D_E_L_E_T_ = ' ' "
	cQryFrm +=                            " GROUP BY OHF_IDDOC) OHF ON (OHF.OHF_IDDOC = FK7.FK7_IDDOC) "
	cQryFrm +=                " LEFT  JOIN (SELECT OHG_IDDOC "
	cQryFrm +=                                   ",SUM(OHG_VALOR) VALOR "
	cQryFrm +=                            " FROM " + RetSqlName("OHG") + "  "
	cQryFrm +=                            " WHERE D_E_L_E_T_ = ' ' "
	cQryFrm +=                            " GROUP BY OHG_IDDOC) OHG ON (OHG.OHG_IDDOC = FK7.FK7_IDDOC) "
	cQryWhr := " WHERE SE2.D_E_L_E_T_ = ' ' "

	For nI := 1 To Len(aFilFilt)
		cFilConcat += "'" + aFilFilt[nI] + "',"
	Next nI

	// Inclui as filiais que o usu�rio tem permiss�o
	If !Empty(cFilConcat) .And. aScan(aFiltros, { |x| x[1] == "E2_FILIAL"}) == 0
		cQryWhr += " AND SE2.E2_FILIAL IN (" + SubStr(cFilConcat,1, Len(cFilConcat)-1) + ")"
	EndIf

	// Loop para inputar os filtros
	For nI := 1 To Len(aFiltros)
		Do Case
			Case aFiltros[nI][1] == "E2_FORNECE"
				cQryWhr += " AND E2_FORNECE = '" + aFiltros[nI][2][1] + "'"
				cQryWhr += " AND E2_LOJA = '" + aFiltros[nI][2][2]+ "'"
			Case aFiltros[nI][1] == "E2_EMISSAO"
				cQryWhr += " AND E2_EMISSAO >= '" + aFiltros[nI][2][1]+ "'"
				cQryWhr += " AND E2_EMISSAO <= '" + aFiltros[nI][2][2] + "'"
			Case aFiltros[nI][1] == "E2_VENCREA"
				cQryWhr += " AND E2_VENCREA >= '" + aFiltros[nI][2][1]+ "'"
				cQryWhr += " AND E2_VENCREA <= '" + aFiltros[nI][2][2] + "'"
			Case aFiltros[nI][1] == "E2_NATUREZ"
				cQryWhr += " AND E2_NATUREZ = '" + aFiltros[nI][2] + "'"
			Case aFiltros[nI][1] == "CONCTITULO"
				cQryWhr += " AND ( E2_PREFIXO || E2_NUM || E2_PARCELA LIKE '%" + StrTran(aFiltros[nI][2], "-","") + "%')"
			Otherwise
				cQryWhr += " AND " + aFiltros[nI][1] + " = '" + aFiltros[nI][2] + "'"
		EndCase
	Next nI

	// Loop para a Ordena��o da query a partir do que foi selecionado em tela
	For nI := 1 To Len(aOrder)
		cOrdConcat := aOrder[nI][1]
		If (cOrdConcat == "E2_NUM")
			cOrdConcat := " SE2.E2_PREFIXO || SE2.E2_NUM || SE2.E2_PARCELA "
		ElseIf (cOrdConcat == "E2_FORNECE-E2_LOJA")
			cOrdConcat := " SA2.A2_NOME"
		ElseIf (cOrdConcat == "E2_NATUREZ")
			cOrdConcat := " SED.ED_DESCRIC"
		EndIf

		If (aOrder[nI][2] == "D")
			cQryOrd += " " + cOrdConcat + " DESC,"
		else
			cQryOrd += " " + cOrdConcat + " ,"
		EndIf
	Next nI

	If (cQryOrd != "")
		cQryOrd := " ORDER BY " + SubStr(cQryOrd, 1, Len(cQryOrd)-1)
	EndIf

	cQuery := ChangeQuery(cQrySel + cQryFrm + cQryWhr + cQryOrd)
	cAliasDb := GetNextAlias()
	conout(cQuery)
	dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery), cAliasDb, .F., .T.)

	// Inicializa o agrupador "titulos"
	oResponse['titulos'] := {}
	
	// Loop pelos registros
	While (cAliasDb)->( !Eof() )
		cStatusTit := ""

		DbSelectArea("SE2")
		dbGoTo((cAliasDb)->(SE2Recno))
		// Posiciona na linha da SE2 por conta da express�o da legenda a ser executada
		If SE2->( Recno() ) == (cAliasDb)->(SE2RECNO)
			For nI := 1 To Len(aValLegen)
				// Executa a fun��o da legenda para atribuir a Legenda do Titulo
				If &(aValLegen[nI][1])
					If  ((lFiltraLeg .And. aValLegen[nI][2] == Self:filtLeg ) .Or. (!lFiltraLeg))
						cStatusTit := aValLegen[nI][2] // Atribui o status do titulo encontrado
						nNumReg++
					EndIf
					aQtdLegen[nI][3] += (cAliasDb)->(E2_VALOR) // Incluindo o valor total por tipo de legenda
					aQtdLegen[nI][2]++ // Adiciona ao totalizador de titulos
					nQtdRegTot++
					exit
				EndIf
			Next nI
		EndIf

		// Verifica a pagina��o
		If ((!lFiltraPag) .Or. (lFiltraPag .And. nNumReg > nRegMin .And. nNumReg <= nRegMax) ) .And. !Empty(cStatusTit)
			nIndex++
			aAdd(oResponse['titulos'], JSonObject():New())
			oResponse['titulos'][nIndex]['filial']           := (cAliasDb)->(E2_FILIAL)
			oResponse['titulos'][nIndex]['prefixo']          := (cAliasDb)->(E2_PREFIXO)
			oResponse['titulos'][nIndex]['numero']           := JConvUTF8((cAliasDb)->(E2_NUM))
			oResponse['titulos'][nIndex]['parcela']          := (cAliasDb)->(E2_PARCELA)
			oResponse['titulos'][nIndex]['tipoTitulo']       := JConvUTF8((cAliasDb)->(E2_TIPO))
			oResponse['titulos'][nIndex]['codFornecedor']    := (cAliasDb)->(E2_FORNECE)
			oResponse['titulos'][nIndex]['lojaFornecedor']   := (cAliasDb)->(E2_LOJA)
			oResponse['titulos'][nIndex]['nomeFornecedor']   := JConvUTF8((cAliasDb)->(A2_NOME))
			oResponse['titulos'][nIndex]['codNatureza']      := (cAliasDb)->(E2_NATUREZ)
			oResponse['titulos'][nIndex]['descNatureza']     := JConvUTF8((cAliasDb)->(ED_DESCRIC))
			oResponse['titulos'][nIndex]['dataEmissao']      := (cAliasDb)->(E2_EMISSAO)
			oResponse['titulos'][nIndex]['dataVencto']       := (cAliasDb)->(E2_VENCTO)
			oResponse['titulos'][nIndex]['dataVenctoReal']   := (cAliasDb)->(E2_VENCREA)
			oResponse['titulos'][nIndex]['valorTitulo']      := (cAliasDb)->(E2_VALOR)
			oResponse['titulos'][nIndex]['valorConvertido']  := (cAliasDb)->(E2_VLCRUZ)
			oResponse['titulos'][nIndex]['saldoTitulo']      := (cAliasDb)->(E2_SALDO)
			oResponse['titulos'][nIndex]['dataBaixa']        := (cAliasDb)->(E2_BAIXA)
			oResponse['titulos'][nIndex]['dataLiberacao']    := (cAliasDb)->(E2_DATALIB)
			oResponse['titulos'][nIndex]['historico']        := JConvUTF8((cAliasDb)->(E2_HIST))
			oResponse['titulos'][nIndex]['valorTransit']     := (cAliasDb)->(VLRTRANSIT)
			oResponse['titulos'][nIndex]['valorPosTran']     := (cAliasDb)->(VLRPOSTRAN)
			oResponse['titulos'][nIndex]['RecnoSE2']         := (cAliasDb)->(SE2RECNO)
			oResponse['titulos'][nIndex]['RecnoFK7']         := (cAliasDb)->(FK7RECNO)
			oResponse['titulos'][nIndex]['status']           := cStatusTit
		EndIf

		aTotalizad[1] += (cAliasDb)->(E2_SALDO)

		if (cBdBase == "ORACLE")
			aTotalizad[2] += Val((cAliasDb)->(VLRTRANSIT))
			aTotalizad[3] += Val((cAliasDb)->(VLRPOSTRAN))
		else 
			aTotalizad[2] += (cAliasDb)->(VLRTRANSIT)
			aTotalizad[3] += (cAliasDb)->(VLRPOSTRAN)
		EndIf
		aTotalizad[4] += (cAliasDb)->(E2_VALOR)

		(cAliasDb)->( dbSkip() )
	EndDo

	// Monta a estrutura do JSON para os totalizadores por legenda
	oResponse['totalLegenda'] := {}
	For nI := 1 To Len(aQtdLegen)
		aAdd(oResponse['totalLegenda'], JSonObject():New())
		oResponse['totalLegenda'][nI]['legenda'] := aQtdLegen[nI][1]
		oResponse['totalLegenda'][nI]['total']   := aQtdLegen[nI][2]
		oResponse['totalLegenda'][nI]['valorTot']:= aQtdLegen[nI][3]
		nValorTot += aQtdLegen[nI][3]
	Next nI

	oResponse['totalizadores'] := JSonObject():New()
	oResponse['totalizadores']['saldo']       := aTotalizad[1]
	oResponse['totalizadores']['transitoria'] := aTotalizad[2]
	oResponse['totalizadores']['transPosPag'] := aTotalizad[3]
	oResponse['totalizadores']['valorTitulo'] := aTotalizad[4]

	oResponse['length'] := nNumReg
	(cAliasDb)->( DbCloseArea() )

	If nIndex == 0
		//setRespError(404, STR0030) //"N�o foram encontrados titulos!"
		//lRet := .F.
	Else
		Self:SetResponse(FWJsonSerialize(oResponse, .F., .F., .T.))
	EndIf
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} PUT SubTitPag
Realiza a substitui��o do Titulo

@example PUT -> http://127.0.0.1:9090/rest/WSPfsAppCP/subtitpag
@example BODY -> {
	"titulos": [
		{ "prefixo": "WPR", "numero": "WPR008", "parcela": "00",
		  "tipo": "PR", "fornecedor": "WYK","loja": "01" }
	],
	"novoTitulo": {
		"E2_PREFIXO": "WSB", "E2_NUM": "WSB004", "E2_PARCELA": "00", "E2_TIPO": "BOL",
		"E2_FORNECE": "WYK", "E2_LOJA": "01", "E2_NATUREZ": "PARTWYK", "E2_EMISSAO": "20200323",
		"E2_VENCTO": "20210301", "E2_VENCREA": "20210401", "E2_VALOR": 100000, "E2_HIST": "Incluso na substitui��o vai POSTMAN",
		"E2_SALDO": 90000, "E2_VLCRUZ": 100000, "E2_MOEDA": 1, "E2_TXMOEDA": 1, "AUTBANCO": "247", "AUTAGENCIA": "1546", "AUTCONTA": "15465666"
	}
}
@author Willian Kazahaya
@since 06/04/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD PUT SubTitPag WSREST WSPfsAppCP
Local oReqBody     := Nil
Local oLinhaTit    := Nil
Local nI           := 0
Local lRet         := .T.
Local aDadosTitulo := {}
Local aTitulos     := {}
Local aNovoTitulo  := {}
Local cBody        := ""

Private lMsErroAuto          := .F.
Private lAutoErrNoFile       := .T.

	cBody := StrTran(Self:GetContent(),CHR(10),"")
	FWJsonDeserialize(cBody,@oReqBody)

	If '"novoTitulo":' $ cBody
		oLinhaTit := oReqBody:novoTitulo
		AAdd(aNovoTitulo , {"E2_FILIAL"  , xFilial("SE2")                                                                              , Nil})
		AAdd(aNovoTitulo , {"E2_PREFIXO" , gtValJSON(oLinhaTit     , "E2_PREFIXO"     , cBody, ""            , TamSx3("E2_PREFIXO")[1] ), Nil})
		AAdd(aNovoTitulo , {"E2_NUM"     , gtValJSON(oLinhaTit     , "E2_NUM"         , cBody, ""            , TamSx3("E2_NUM")[1]     ), Nil})
		AAdd(aNovoTitulo , {"E2_PARCELA" , gtValJSON(oLinhaTit     , "E2_PARCELA"     , cBody, ""            , TamSx3("E2_PARCELA")[1] ), Nil})
		AAdd(aNovoTitulo , {"E2_TIPO"    , gtValJSON(oLinhaTit     , "E2_TIPO"        , cBody, ""            , TamSx3("E2_TIPO")[1]    ), Nil})
		AAdd(aNovoTitulo , {"E2_FORNECE" , gtValJSON(oLinhaTit     , "E2_FORNECE"     , cBody, ""            , TamSx3("E2_FORNECE")[1] ), Nil})
		AAdd(aNovoTitulo , {"E2_LOJA"    , gtValJSON(oLinhaTit     , "E2_LOJA"        , cBody, ""            , TamSx3("E2_LOJA")[1]    ), Nil})
		AAdd(aNovoTitulo , {"E2_NATUREZ" , gtValJSON(oLinhaTit     , "E2_NATUREZ"     , cBody, ""            , TamSx3("E2_NATUREZ")[1] ), Nil})
		AAdd(aNovoTitulo , {"E2_EMISSAO" , SToD(gtValJSON(oLinhaTit, "E2_EMISSAO"     , cBody, DTOS(Date()))                           ), Nil})
		AAdd(aNovoTitulo , {"E2_VENCTO"  , SToD(gtValJSON(oLinhaTit, "E2_VENCTO"      , cBody, DTOS(Date()))                           ), Nil})
		AAdd(aNovoTitulo , {"E2_VENCREA" , SToD(gtValJSON(oLinhaTit, "E2_VENCREA"     , cBody, DTOS(Date()))                           ), Nil})
		AAdd(aNovoTitulo , {"E2_VALOR"   , gtValJSON(oLinhaTit     , "E2_VALOR"       , cBody, 0                                       ), Nil})
		AAdd(aNovoTitulo , {"E2_HIST"    , gtValJSON(oLinhaTit     , "E2_HIST"        , cBody, ""            , TamSx3("E2_HIST")[1]    ), Nil})
		AAdd(aNovoTitulo , {"E2_SALDO"   , gtValJSON(oLinhaTit     , "E2_SALDO"       , cBody, 0                                       ), Nil})
		AAdd(aNovoTitulo , {"E2_MOEDA"   , gtValJSON(oLinhaTit     , "E2_MOEDA"       , cBody, 1                                       ), Nil})
		AAdd(aNovoTitulo , {"E2_VLCRUZ"  , gtValJSON(oLinhaTit     , "E2_VLCRUZ"      , cBody, 0                                       ), Nil})
		AAdd(aNovoTitulo , {"E2_TXMOEDA" , gtValJSON(oLinhaTit     , "E2_TXMOEDA"     , cBody, 0                                       ), Nil})
		AAdd(aNovoTitulo , {"E2_CODAPRO" , gtValJSON(oLinhaTit     , "E2_CODAPRO"     , cBody, ""            , TamSx3("E2_CODAPRO")[1] ), Nil})

		oLinhaTit := Nil
	EndIf

	//MSExecAuto({|a,b,c| FINA050(a,b,c)}, aTitulo, Nil, 8) //Efetua a operacao
	If '"titulos":' $ cBody
		For nI := 1 to Len(oReqBody:Titulos)
			oLinhaTit := oReqBody:Titulos[nI]
			aAdd(aDadosTitulo, {"E2_PREFIXO", gtValJSON(oLinhaTit , "prefixo"     , cBody, ""  , TamSx3("E2_PREFIXO")[1] ), Nil})
			aAdd(aDadosTitulo, {"E2_NUM"    , gtValJSON(oLinhaTit , "numero"      , cBody, ""  , TamSx3("E2_NUM")[1] )    , Nil})
			aAdd(aDadosTitulo, {"E2_PARCELA", gtValJSON(oLinhaTit , "parcela"     , cBody, ""  , TamSx3("E2_PARCELA")[1] ), Nil})
			aAdd(aDadosTitulo, {"E2_TIPO"   , gtValJSON(oLinhaTit , "tipo"        , cBody, ""  , TamSx3("E2_TIPO")[1] )   , Nil})
			aAdd(aDadosTitulo, {"E2_FORNECE", gtValJSON(oLinhaTit , "fornecedor"  , cBody, ""  , TamSx3("E2_FORNECE")[1] ), Nil})
			aAdd(aDadosTitulo, {"E2_LOJA"   , gtValJSON(oLinhaTit , "loja"        , cBody, ""  , TamSx3("E2_LOJA")[1] )   , Nil})

			oLinhaTit := Nil
			aAdd(aTitulos, aClone(aDadosTitulo))
			aSize(aDadosTitulo, 0)
		Next nI

		DbSelectArea("SE2")
		SE2->( DbSetOrder(1) )
		If Len(aNovoTitulo) > 0 .And. ;
			Len(aTitulos) > 0 .And. ;
			SE2->( DbSeek( xFilial("SE2") + aTitulos[1][1][2] + aTitulos[1][2][2] + aTitulos[1][3][2] + aTitulos[1][4][2] + aTitulos[1][5][2] + aTitulos[1][6][2] ))
			MSExecAuto({|a,b,c,d,e,f,g,h,i,j| FINA050(a,b,c,d,e,f,g,h,i,j)},aNovoTitulo,,6,,,,,,aTitulos)
		EndIf

		If lMsErroAuto
			setRespError(500, STR0031) //"Ocorreu um erro durante a substitui��o do Titulo a pagar"
			lRet := .F.
		EndIf
	EndIf

	If lRet
		Self:SetResponse(getRespOk("SubstituirTituloPagar"))
	EndIf
Return lRet



//-------------------------------------------------------------------
/*/{Protheus.doc} PUT - AltTitPag
Altera�a� de Titulo a pagar
@example PUT  -> http://localhost:12173/rest/WSPfsAppCP/titpag/{recnoTitulo}
@example Body ->
	{ "E2_VALOR": 200000, "E2_HIST": "Teste de altera��o via POSTMAN",
	  "E2_SALDO": 120000, "E2_MOEDA": 2 }
@author Willian Kazahaya
@since 20/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD PUT AltTitPag PATHPARAM recnoTitulo WSREST WSPfsAppCP
Local lRet         := .T.
Local aDadosTitulo := {}
Local aTitulo      := {}
Local cBody        := ""
Local nIndex       := 0
Local aCmpsChave   := {"E2_FILIAL","E2_PREFIXO","E2_NUM","E2_PARCELA", "E2_TIPO", "E2_FORNECE", "E2_LOJA"}
Local aExtCampos   := {"E2_NATUREZ","E2_EMISSAO","E2_VENCTO","E2_VENCREA","E2_VALOR","E2_HIST","E2_SALDO","E2_MOEDA","E2_VLCRUZ","E2_TXMOEDA","E2_CODAPRO"}
Local oJson         := JsonObject():new()
Local nI            := 0
Local itensJson
Local retJson

Private lMsErroAuto          := .F.
Private lAutoErrNoFile       := .T.

	aDadosTitulo := gtDadoByRC(Self:recnoTitulo, 1, aExtCampos)
	cBody := Self:GetContent()

	retJson := oJson:fromJson(cBody)
	itensJson := oJson:GetNames()

	DbSelectArea("SE2")
	SE2->( DbSetOrder(1) )
	If Len(aDadosTitulo) > 0 .And. ;
	   SE2->( dbSeek(aDadosTitulo[1][1][2]))

		// Essa parte n�o passa por altera��o por conta da Chave da Tabela ( X2_UNICO )
		If (nIndex := aScan(aDadosTitulo[1], {|x| x[1] == "E2_FILIAL" })) > 0
			AAdd(aTitulo , {"E2_FILIAL" , aDadosTitulo[1][nIndex][2], Nil})
		EndIf
		If (nIndex := aScan(aDadosTitulo[1], {|x| x[1] == "E2_PREFIXO" })) > 0
			AAdd(aTitulo , {"E2_PREFIXO" , aDadosTitulo[1][nIndex][2], Nil})
		EndIf
		If (nIndex := aScan(aDadosTitulo[1], {|x| x[1] == "E2_NUM" })) > 0
			AAdd(aTitulo , {"E2_NUM" , aDadosTitulo[1][nIndex][2], Nil})
		EndIf
		If (nIndex := aScan(aDadosTitulo[1], {|x| x[1] == "E2_PARCELA" })) > 0
			AAdd(aTitulo , {"E2_PARCELA" , aDadosTitulo[1][nIndex][2], Nil})
		EndIf
		If (nIndex := aScan(aDadosTitulo[1], {|x| x[1] == "E2_TIPO" })) > 0
			AAdd(aTitulo , {"E2_TIPO" , aDadosTitulo[1][nIndex][2], Nil})
		EndIf
		If (nIndex := aScan(aDadosTitulo[1], {|x| x[1] == "E2_FORNECE" })) > 0
			AAdd(aTitulo , {"E2_FORNECE" , aDadosTitulo[1][nIndex][2], Nil})
		EndIf
		If (nIndex := aScan(aDadosTitulo[1], {|x| x[1] == "E2_LOJA" })) > 0
			AAdd(aTitulo , {"E2_LOJA" , aDadosTitulo[1][nIndex][2], Nil})
		EndIf

		// 
		For nI := 1 To Len(itensJson)
			if (aScan(aCmpsChave, itensJson[nI]) == 0)
				AAdd(aTitulo , {itensJson[nI] , CvJsonVal(oJson[itensJson[nI]], TamSx3(itensJson[nI])) , Nil})
			EndIf
		Next nI

		MSExecAuto({|a,b,c| FINA050(a,b,c)}, aTitulo, Nil, 4) //Efetua a operacao

		If (lMsErroAuto)
			setRespError(500, STR0032) //"Ocorreu um erro durante a altera��o do Titulo a pagar"
			lRet := .F.
		EndIf
	Else
		setRespError(404, STR0033) //  "O titulo n�o foi encontrado!"
		lRet := .F.
	EndIf

	If lRet
		Self:SetResponse(getRespOk("AlterarTituloPagar"))
	EndIf
Return lRet


//-------------------------------------------------------------------
/*/{Protheus.doc} PUT - BaixaTitulo
Baixa de titulos do Pagar
@example PUT  -> http://localhost:12173/rest/WSPfsAppCP/titbai/{recnoTitulo}
@example Body ->
	{ "MotivoBaixa": "DACAO", "HistoricoBaixa": "Baixa via POSTMAN" }

@author Willian Kazahaya
@since 20/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD PUT BaixaTitulo PATHPARAM recnoTitulo WSREST WSPfsAppCP
Local cBody        := Self:GetContent()
Local oReqBody     := Nil
Local aDadosTitulo := {}
Local aErrGrAuto   := {}
Local aBaixa       := {}
Local cTipoMotivo  := ""
Local cHistBaixa   := ""
Local cAliasDb     := ""
Local cFKTitulo    := getFKTitulo(Self:recnoTitulo)
Local lRet         := .T.
Local nCodHttp     := 0

Private lF080Auto       := .T.
Private lMsErroAuto     := .F.
Private lAutoErrNoFile  := .T.

	aDadosTitulo := gtDadoByRC(Self:recnoTitulo, 1)

	FWJsonDeserialize(cBody,@oReqBody)

	cTipoMotivo := gtValJSON(oReqBody, "MotivoBaixa"         , cBody)
	cHistBaixa  := gtValJSON(oReqBody, "HistoricoBaixa"      , cBody)
	cValDesc    := gtValJSON(oReqBody, "ValorDesconto"       , cBody, 0)
	cValMulta   := gtValJSON(oReqBody, "ValorMulta"          , cBody, 0)
	cValJuros   := gtValJSON(oReqBody, "ValorJuros"          , cBody, 0)
	cValPagto   := gtValJSON(oReqBody, "ValorPagto"          , cBody, 0)
	cValEstrg   := gtValJSON(oReqBody, "ValorConvertido"     , cBody, 0)

	DbSelectArea("SE2")
	SE2->( DbSetOrder(1) )
	if SE2->( dbSeek(aDadosTitulo[1][1][2]))
		aAdd(aBaixa,{"E2_PREFIXO" , aDadosTitulo[1][3][2]    ,Nil})
		aAdd(aBaixa,{"E2_NUM"     , aDadosTitulo[1][4][2]    ,Nil})
		aAdd(aBaixa,{"E2_PARCELA" , aDadosTitulo[1][5][2]    ,Nil})
		aAdd(aBaixa,{"E2_TIPO"    , aDadosTitulo[1][6][2]    ,Nil})
		aAdd(aBaixa,{"E2_FORNECE" , aDadosTitulo[1][7][2]    ,Nil})
		aAdd(aBaixa,{"E2_LOJA"    , aDadosTitulo[1][8][2]    ,Nil})
		aAdd(aBaixa,{"AUTMOTBX"   , cTipoMotivo     ,Nil}) //"DACAO"
		aAdd(aBaixa,{"AUTHIST"    , cHistBaixa      ,Nil}) //"Baixa Automatica"
		aAdd(aBaixa,{"AUTDESCONT" , cValDesc        ,Nil})
		aAdd(aBaixa,{"AUTMULTA"   , cValMulta       ,Nil})
		aAdd(aBaixa,{"AUTJUROS"   , cValJuros       ,Nil})
		aAdd(aBaixa,{"AUTVLRPG"   , cValPagto       ,Nil})
		aAdd(aBaixa,{"AUTVLRME"   , cValEstrg       ,Nil})

		lRet := MSExecAuto({| a,b,c,d,e,f | FINA080(a,b,c,d,e,f)} ,aBaixa,3,,,,)//3 para baixar ou 5 para cancelar a baixa.

		If lRet .And. !lMsErroAuto
			cAliasDb := GetNextAlias()
			dbUseArea(.T., "TOPCONN", TCGenQry(,,qryFK2(cFKTitulo)), cAliasDb, .F., .T.)

			If (cAliasDb)->( !Eof() )
				If (cAliasDb)->FK2_RECPAG != "P"
					nCodHttp := 500
					lRet := .F.
				EndIf
			Else
				nCodHttp := 500
				lRet := .F.
			EndIf

			(cAliasDb)->( DbCloseArea() )
		else
			nCodHttp := 500
			lRet := .F.
		EndIf
	Else
		nCodHttp := 404
		lRet := .F.
	EndIf

	If lRet
		Self:SetResponse(getRespOk("BaixaTitulo"))
	Else
		aErrGrAuto := GetAutoGrLog()
		Do Case
			Case nCodHttp == 404
				setRespError(nCodHttp, STR0033) //  "O titulo n�o foi encontrado!"
			Case nCodHttp == 500
				If Len(aErrGrAuto) > 0
					setRespError(nCodHttp, aErrGrAuto[1])
				Else
					setRespError(nCodHttp, STR0034) //"Houve problema no Cancelamento da baixa do Titulo selecionado!"
				EndIf
		End Case
	EndIf
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} PUT - SimpDesdobram
Desdobramento de Titulo - Simples (N�o Transit�rio)
@example PUT  -> http://localhost:12173/rest/WSPfsAppCP/desdobr/simples/{recnoTitulo}
@example Body ->
{
	"siglaParticipante": "WYK",
	"escritorio": "SP",
	"centroCusto": "3",
	"siglaProfissional": "WYK",
	"tabelaRateio": "0000000002",
	"codigoCliente": "000001",
	"lojaCliente": "00",
	"casoCliente": "000001",
	"codigoDespesa": "0001",
	"qtdDespesa": 1,
	"dataDespesa": "20200331",
	"cobraDespesa": 1,
	"codigoProjeto": "",
	"codigoItemProjeto": "",
	"codigoHistPadrao": "",
	"historico": "Alterando desdobramento via POSTMAN"
}

@author Willian Kazahaya
@since 20/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD PUT SimpDesdobram PATHPARAM recnoTitulo WSREST WSPfsAppCP
Local lRet          := .F.
Local aArea         := getArea()
Local aAreaSE2      := SE2->( getArea() )
Local aAreaOHF      := OHF->( getArea() )
Local oReqBody      := Nil
Local oModel        := Nil
Local oModelOHF     := Nil
Local cBody         := ""
Local aDadosTitulo  := {}
Local aExtCampos    := {"E2_NATUREZ", "E2_VALOR"}
Local cFKTitulo     := getFKTitulo(Self:recnoTitulo)
Local bHasOHF       := .F.

	cBody := Self:GetContent()
	FWJsonDeserialize(cBody,@oReqBody)
	aDadosTitulo := gtDadoByRC(Self:recnoTitulo,1,aExtCampos)

	DbSelectArea("SE2")
	SE2->( DbSetOrder(1) )
	If Len(aDadosTitulo) > 0 .And. ;
	   SE2->( dbSeek(aDadosTitulo[1][1][2]))

		DbSelectArea("OHF")
		OHF->( DbSetOrder(1) )
		bHasOHF := OHF->( DbSeek(aDadosTitulo[1][aScan(aDadosTitulo[1], {|x| x[1] == "E2_FILIAL"})][2] + cFKTitulo ))

		oModel := FWLoadModel("JURA246")
		oModel:SetOperation(4)
		oModel:Activate()

		oModelOHF := oModel:getModel("OHFDETAIL")

		If !bHasOHF
			oModelOHF:setValue("OHF_FILIAL", oModel:GetValue("SE2MASTER","E2_FILIAL"))
			oModelOHF:setValue("OHF_IDDOC", cFKTitulo)
			oModelOHF:setValue("OHF_CITEM", "0001")
			oModelOHF:setValue("OHF_CNATUR", aDadosTitulo[1][aScan(aDadosTitulo[1], {|x| x[1] == "E2_NATUREZ"})][2])
			oModelOHF:setValue("OHF_VALOR" , aDadosTitulo[1][aScan(aDadosTitulo[1], {|x| x[1] == "E2_VALOR"})][2])
		EndIf

		oModelOHF:setValue("OHF_SIGLA", oReqBody:siglaParticipante)

		If ( oModelOHF:canSetValue("OHF_CESCR") )
			oModelOHF:setValue("OHF_CESCR", oReqBody:escritorio)
		EndIf

		If ( oModelOHF:canSetValue("OHF_CCUSTO") )
			oModelOHF:setValue("OHF_CCUSTO", oReqBody:centroCusto)
		EndIf

		If ( oModelOHF:canSetValue("OHF_SIGLA2") )
			oModelOHF:setValue("OHF_SIGLA2", oReqBody:siglaProfissional)
		EndIf

		If ( oModelOHF:canSetValue("OHF_CRATEI") )
			oModelOHF:setValue("OHF_CRATEI", oReqBody:tabelaRateio)
		EndIf

		If ( oModelOHF:canSetValue("OHF_CCLIEN") )
			oModelOHF:setValue("OHF_CCLIEN", oReqBody:codigoCliente)
			oModelOHF:setValue("OHF_CLOJA", oReqBody:lojaCliente)
			oModelOHF:setValue("OHF_CCASO", oReqBody:casoCliente)
			oModelOHF:setValue("OHF_CTPDSP", oReqBody:codigoDespesa)
			oModelOHF:setValue("OHF_QTDDSP", oReqBody:qtdDespesa)
			oModelOHF:setValue("OHF_DTDESP", oReqBody:dataDespesa)
			oModelOHF:setValue("OHF_COBRA", oReqBody:cobraDespesa)
		EndIf

		oModelOHF:setValue("OHF_CPROJE", oReqBody:codigoProjeto)

		If ( oModelOHF:canSetValue("OHF_CITPRJ") )
			oModelOHF:setValue("OHF_CITPRJ", oReqBody:codigoItemProjeto)
		EndIf

		oModelOHF:setValue("OHF_CHISTP", oReqBody:codigoHistPadrao)
		oModelOHF:setValue("OHF_HISTOR", oReqBody:historico)

		If oModel:VldData()
			If oModel:CommitData()
				lRet := .T.
			EndIf
		EndIf
	EndIf

	If lRet
		Self:SetResponse(getRespOk("DesdobramentoSimples"))
	EndIf

	RestArea( aAreaOHF )
	RestArea( aAreaSE2 )
	RestArea( aArea )
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} PUT - SimpDesdobram
Desdobramento de Titulo - Transit�ria de Pagamento
@example PUT  -> http://localhost:12173/rest/WSPfsAppCP/desdobr/transit/{recnoTitulo}
@example Body ->
{
	"items": [
		{
			"sequencialItem": "0001",
			"codigoNatureza": "PARTPST",
			"valorItem": 43242,
			"siglaParticipante": "JAX",
			"escritorio": "SP",
			"centroCusto": "8",
			"siglaProfissional": "PST  ",
			"tabelaRateio": "0000000003",
			"codigoCliente": "000001",
			"lojaCliente": "00",
			"casoCliente": "000001",
			"codigoDespesa": "0001",
			"qtdDespesa": 1,
			"dataDespesa": "20200331",
			"cobraDespesa": 1,
			"codigoProjeto   ": "",
			"codigoItemProjeto": "",
			"codigoHistPadrao": "",
			"historico": "Alterando desdobramento via POSTMAN"
		}
	]
}

@author Willian Kazahaya
@since 20/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD PUT TrDesdobramento PATHPARAM recnoTitulo WSREST WSPfsAppCP
Local lRet          := .F.
Local aArea         := getArea()
Local aAreaSE2      := SE2->( getArea() )
Local aAreaOHF      := OHF->( getArea() )
Local oReqBody      := Nil
Local oReqBodyLin   := Nil
Local oModel        := Nil
Local oModelOHF     := Nil
Local cBody         := ""
Local aDadosTitulo  := {}
Local aExtCampos    := {"E2_NATUREZ", "E2_VALOR"}
Local aSeek         := {}
Local cFKTitulo     := getFKTitulo(Self:recnoTitulo)
Local nI            := 0
Local nLine         := 0
Local nMaxSeq       := 0
Local lFindReg      := .T.
Local lNovoReg      := .F.
Local bHasOHF       := .F.

	cBody := Self:GetContent()
	FWJsonDeserialize(cBody,@oReqBody)
	aDadosTitulo := gtDadoByRC(Self:recnoTitulo,1,aExtCampos)

	DbSelectArea("SE2")
	SE2->( DbSetOrder(1) )
	If Len(aDadosTitulo) > 0 .And. ;
	   SE2->( dbSeek(aDadosTitulo[1][1][2]))

		DbSelectArea("OHF")
		OHF->( DbSetOrder(1) )
		bHasOHF := OHF->( DbSeek(aDadosTitulo[1][aScan(aDadosTitulo[1], {|x| x[1] == "E2_FILIAL"})][2] + cFKTitulo ))

		If bHasOHF
			nMaxSeq := gtMaxSqTrn(cFKTitulo)
		EndIf

		oModel := FWLoadModel("JURA246")
		oModel:SetOperation(4)
		oModel:Activate()

		oModelOHF := oModel:getModel("OHFDETAIL")

		For nI := 1 To Len(oReqBody:items)
			oReqBodyLin := oReqBody:items[nI]
			If bHasOHF
				aAdd(aSeek, {"OHF_CITEM",oReqBodyLin:sequencialItem})
				lFindReg := oModelOHF:SeekLine(aSeek)
				lNovoReg := !lFindReg

				aSize(aSeek,0)
			Else
				lNovoReg := .T.
			EndIf

			If lNovoReg
				lNovoReg := oModelOHF:AddLine()
				If lNovoReg
					nMaxSeq++
					oModelOHF:setValue("OHF_FILIAL", oModel:GetValue("SE2MASTER","E2_FILIAL"))
					oModelOHF:setValue("OHF_IDDOC", cFKTitulo)
					oModelOHF:setValue("OHF_CITEM", Right("0000" + cValToChar(nMaxSeq), 4))
				else
					Break
				EndIf
			EndIF

			oModelOHF:setValue("OHF_CNATUR", gtValJSON(oReqBodyLin       , "codigoNatureza"      , cBody, ""            , TamSx3("OHF_CNATUR")[1] ) )
			oModelOHF:setValue("OHF_VALOR" , gtValJSON(oReqBodyLin       , "valorItem"           , cBody, 0             ,  ) )
			oModelOHF:setValue("OHF_SIGLA" , gtValJSON(oReqBodyLin      , "siglaParticipante"   , cBody, ""            , TamSx3("OHF_SIGLA")[1] ) )

			If ( oModelOHF:canSetValue("OHF_CESCR") )
				oModelOHF:setValue("OHF_CESCR",  gtValJSON(oReqBodyLin   , "escritorio"          , cBody, ""            , TamSx3("OHF_CESCR")[1] ))
			EndIf

			If ( oModelOHF:canSetValue("OHF_CCUSTO") )
				oModelOHF:setValue("OHF_CCUSTO",  gtValJSON(oReqBodyLin  , "centroCusto"         , cBody, ""            , TamSx3("OHF_CCUSTO")[1] ))
			EndIf

			If ( oModelOHF:canSetValue("OHF_SIGLA2") )
				oModelOHF:setValue("OHF_SIGLA2",  gtValJSON(oReqBodyLin  , "siglaProfissional"   , cBody, ""            , TamSx3("OHF_SIGLA2")[1] ))
			EndIf

			If ( oModelOHF:canSetValue("OHF_CRATEI") )
				oModelOHF:setValue("OHF_CRATEI",  gtValJSON(oReqBodyLin  , "tabelaRateio"        , cBody, ""            , TamSx3("OHF_CRATEI")[1] ))
			EndIf

			If ( oModelOHF:canSetValue("OHF_CCLIEN") )
				oModelOHF:setValue("OHF_CCLIEN",  gtValJSON(oReqBodyLin  , "codigoCliente"       , cBody, ""            , TamSx3("OHF_CCLIEN")[1] ))
				oModelOHF:setValue("OHF_CLOJA",  gtValJSON(oReqBodyLin   , "lojaCliente"         , cBody, ""            , TamSx3("OHF_CLOJA")[1] ))
				oModelOHF:setValue("OHF_CCASO",  gtValJSON(oReqBodyLin   , "casoCliente"         , cBody, ""            , TamSx3("OHF_CCASO")[1] ))
				oModelOHF:setValue("OHF_CTPDSP",  gtValJSON(oReqBodyLin  , "codigoDespesa"       , cBody, ""            , TamSx3("OHF_CTPDSP")[1] ))
				oModelOHF:setValue("OHF_QTDDSP",  gtValJSON(oReqBodyLin  , "qtdDespesa"          , cBody, 0             ,                         ))
				oModelOHF:setValue("OHF_DTDESP",  gtValJSON(oReqBodyLin  , "dataDespesa"         , cBody, ""            , TamSx3("OHF_DTDESP")[1] ))
				oModelOHF:setValue("OHF_COBRA",  gtValJSON(oReqBodyLin   , "cobraDespesa"        , cBody, ""            , TamSx3("OHF_COBRA")[1] ))
			EndIf

			oModelOHF:setValue("OHF_CPROJE",  gtValJSON(oReqBodyLin      , "codigoProjeto"       , cBody, ""            , TamSx3("OHF_CPROJE")[1] ))

			If ( oModelOHF:canSetValue("OHF_CITPRJ") )
				oModelOHF:setValue("OHF_CITPRJ",  gtValJSON(oReqBodyLin  , "codigoItemProjeto"   , cBody, ""            , TamSx3("OHF_CITPRJ")[1] ))
			EndIf

			oModelOHF:setValue("OHF_CHISTP",  gtValJSON(oReqBodyLin      , "codigoHistPadrao"    , cBody, ""            , TamSx3("OHF_CHISTP")[1] ))
			oModelOHF:setValue("OHF_HISTOR",  gtValJSON(oReqBodyLin      , "historico"           , cBody, ""            , TamSx3("OHF_HISTOR")[1] ))

		Next nI

		If oModel:VldData()
			If oModel:CommitData()
				lRet := .T.
			EndIf
		EndIf
	EndIf

	If lRet
		Self:SetResponse(getRespOk("DesdobramentoTransitoria"))
	else
		If !Empty(oModelOHF:aErrorMessage[6])
			setRespError(500,oModel:aErrorMessage[6] + "||" + oModel:aErrorMessage[7])

		EndIf
	EndIf

	RestArea( aAreaOHF )
	RestArea( aAreaSE2 )
	RestArea( aArea )
Return lRet


//-------------------------------------------------------------------
/*/{Protheus.doc} PUT - SimpDesdobram
Desdobramento de Titulo - Transit�ria de P�s-Pagamento
@example PUT  -> http://localhost:12173/rest/WSPfsAppCP/desdobr/pospag/{recnoTitulo}
@example Body ->
{
	"items": [
		{
			"sequencialItem": "0001",
			"codigoNatureza": "PARTPST",
			"valorItem": 43242,
			"siglaParticipante": "JAX",
			"escritorio": "SP",
			"centroCusto": "8",
			"siglaProfissional": "PST  ",
			"tabelaRateio": "0000000003",
			"codigoCliente": "000001",
			"lojaCliente": "00",
			"casoCliente": "000001",
			"codigoDespesa": "0001",
			"qtdDespesa": 1,
			"dataDespesa": "20200331",
			"cobraDespesa": 1,
			"codigoProjeto   ": "",
			"codigoItemProjeto": "",
			"codigoHistPadrao": "",
			"historico": "Alterando desdobramento via POSTMAN"
		}
	]
}

@author Willian Kazahaya
@since 20/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD PUT PPagDesdobram PATHPARAM recnoTitulo WSREST WSPfsAppCP
Local lRet          := .F.
Local aArea         := getArea()
Local aAreaSE2      := SE2->( getArea() )
Local aAreaOHG      := OHG->( getArea() )
Local oReqBody      := Nil
Local oReqBodyLin   := Nil
Local oModel        := Nil
Local oModelOHG     := Nil
Local cBody         := ""
Local aDadosTitulo  := {}
Local aExtCampos    := {"E2_NATUREZ", "E2_VALOR"}
Local aSeek         := {}
Local cFKTitulo     := getFKTitulo(Self:recnoTitulo)
Local nI            := 0
Local nLine         := 0
Local nMaxSeq       := 0
Local lFindReg      := .T.
Local lNovoReg      := .F.
Local bHasOHG       := .F.

	cBody := Self:GetContent()
	FWJsonDeserialize(cBody,@oReqBody)
	aDadosTitulo := gtDadoByRC(Self:recnoTitulo,1,aExtCampos)

	DbSelectArea("SE2")
	SE2->( DbSetOrder(1) )
	If Len(aDadosTitulo) > 0 .And. ;
	   SE2->( dbSeek(aDadosTitulo[1][1][2]))

		DbSelectArea("OHG")
		OHG->( DbSetOrder(1) )
		bHasOHG := OHG->( DbSeek(aDadosTitulo[1][aScan(aDadosTitulo[1], {|x| x[1] == "E2_FILIAL"})][2] + cFKTitulo ))

		If bHasOHG
			nMaxSeq := gtMaxSqTrn(cFKTitulo, "OHG")
		EndIf

		oModel := FWLoadModel("JURA247")
		oModel:SetOperation(4)
		oModel:Activate()

		oModelOHG := oModel:getModel("OHGDETAIL")

		For nI := 1 To Len(oReqBody:items)
			oReqBodyLin := oReqBody:items[nI]
			If bHasOHG
				aAdd(aSeek, {"OHG_CITEM",oReqBodyLin:sequencialItem})
				lFindReg := oModelOHG:SeekLine(aSeek)
				lNovoReg := !lFindReg

				aSize(aSeek,0)
			Else
				lNovoReg := .T.
			EndIf

			If lNovoReg
				lNovoReg := oModelOHG:AddLine()
				If lNovoReg
					nMaxSeq++
					oModelOHG:setValue("OHG_FILIAL", oModel:GetValue("SE2MASTER","E2_FILIAL"))
					oModelOHG:setValue("OHG_IDDOC", cFKTitulo)
					oModelOHG:setValue("OHG_CITEM", Right("0000" + cValToChar(nMaxSeq), 4))
				else
					Break
				EndIf
			EndIF

			oModelOHG:setValue("OHG_CNATUR", gtValJSON(oReqBodyLin       , "codigoNatureza"      , cBody, ""            , TamSx3("OHG_CNATUR")[1] ) )
			oModelOHG:setValue("OHG_VALOR" , gtValJSON(oReqBodyLin       , "valorItem"           , cBody, 0             ,  ) )
			oModelOHG:setValue("OHG_SIGLA" , gtValJSON(oReqBodyLin      , "siglaParticipante"   , cBody, ""            , TamSx3("OHG_SIGLA")[1] ) )

			If ( oModelOHG:canSetValue("OHG_CESCR") )
				oModelOHG:setValue("OHG_CESCR",  gtValJSON(oReqBodyLin   , "escritorio"          , cBody, ""            , TamSx3("OHG_CESCR")[1] ))
			EndIf

			If ( oModelOHG:canSetValue("OHG_CCUSTO") )
				oModelOHG:setValue("OHG_CCUSTO",  gtValJSON(oReqBodyLin  , "centroCusto"         , cBody, ""            , TamSx3("OHG_CCUSTO")[1] ))
			EndIf

			If ( oModelOHG:canSetValue("OHG_SIGLA2") )
				oModelOHG:setValue("OHG_SIGLA2",  gtValJSON(oReqBodyLin  , "siglaProfissional"   , cBody, ""            , TamSx3("OHG_SIGLA2")[1] ))
			EndIf

			If ( oModelOHG:canSetValue("OHG_CRATEI") )
				oModelOHG:setValue("OHG_CRATEI",  gtValJSON(oReqBodyLin  , "tabelaRateio"        , cBody, ""            , TamSx3("OHG_CRATEI")[1] ))
			EndIf

			If ( oModelOHG:canSetValue("OHG_CCLIEN") )
				oModelOHG:setValue("OHG_CCLIEN",  gtValJSON(oReqBodyLin  , "codigoCliente"       , cBody, ""            , TamSx3("OHG_CCLIEN")[1] ))
				oModelOHG:setValue("OHG_CLOJA",  gtValJSON(oReqBodyLin   , "lojaCliente"         , cBody, ""            , TamSx3("OHG_CLOJA")[1] ))
				oModelOHG:setValue("OHG_CCASO",  gtValJSON(oReqBodyLin   , "casoCliente"         , cBody, ""            , TamSx3("OHG_CCASO")[1] ))
				oModelOHG:setValue("OHG_CTPDSP",  gtValJSON(oReqBodyLin  , "codigoDespesa"       , cBody, ""            , TamSx3("OHG_CTPDSP")[1] ))
				oModelOHG:setValue("OHG_QTDDSP",  gtValJSON(oReqBodyLin  , "qtdDespesa"          , cBody, 0             ,                         ))
				oModelOHG:setValue("OHG_DTDESP",  gtValJSON(oReqBodyLin  , "dataDespesa"         , cBody, ""            , TamSx3("OHG_DTDESP")[1] ))
				oModelOHG:setValue("OHG_COBRA",  gtValJSON(oReqBodyLin   , "cobraDespesa"        , cBody, ""            , TamSx3("OHG_COBRA")[1] ))
			EndIf

			oModelOHG:setValue("OHG_CPROJE",  gtValJSON(oReqBodyLin      , "codigoProjeto"       , cBody, ""            , TamSx3("OHG_CPROJE")[1] ))

			If ( oModelOHG:canSetValue("OHG_CITPRJ") )
				oModelOHG:setValue("OHG_CITPRJ",  gtValJSON(oReqBodyLin  , "codigoItemProjeto"   , cBody, ""            , TamSx3("OHG_CITPRJ")[1] ))
			EndIf

			oModelOHG:setValue("OHG_CHISTP",  gtValJSON(oReqBodyLin      , "codigoHistPadrao"    , cBody, ""            , TamSx3("OHG_CHISTP")[1] ))
			oModelOHG:setValue("OHG_HISTOR",  gtValJSON(oReqBodyLin      , "historico"           , cBody, ""            , TamSx3("OHG_HISTOR")[1] ))

		Next nI

		If oModel:VldData()
			If oModel:CommitData()
				lRet := .T.
			EndIf
		EndIf
	EndIf

	If lRet
		Self:SetResponse(getRespOk("DesdobramentoPosPagto"))
	else
		If !Empty(oModelOHG:aErrorMessage[6])
			setRespError(500,oModel:aErrorMessage[6] + "||" + oModel:aErrorMessage[7])

		EndIf
	EndIf

	RestArea( aAreaOHG )
	RestArea( aAreaSE2 )
	RestArea( aArea )
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} POST - CrtTitPag
Desdobramento de Titulo - Transit�ria de P�s-Pagamento
@example POST  -> http://localhost:12173/rest/WSPfsAppCP/titpag
@example Body ->
{
	"E2_FORNECE": "WYK000",
	"E2_LOJA": "01",
	"E2_PREFIXO": "WTTS",
	"E2_NUM": "WTT003",
	"E2_PARCELA": "00",
	"E2_TIPO": "FOL",
	"E2_NATUREZ": "20.010.001",
	"E2_EMISSAO": "20200401",
	"E2_VENCTO": "20200401",
	"E2_VENCREA": "20200401",
	"E2_VALOR": 1000000,
	"E2_HIST": "Inclus�o de Titulo a Pagar com Natureza Transit�ria",
	"E2_SALDO": 1000000,
	"E2_MOEDA": 1,
	"E2_VLCRUZ": 1000000,
	"E2_TXMOEDA": 0,
	"E2_CODAPRO": ""
}

@author Willian Kazahaya
@since 20/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD POST CrtTitPag  WSREST WSPfsAppCP
Local oReqBody      := Nil
Local cBody         := ""
Local lRet          := .T.
Local aTitulo       := {}
Local aValid        := {}
Local oJson         := JsonObject():new()
Local nI            := 0
Local aJsonExtra    := {}
Local aTitulo       := {}
Local cRecno        := ""
Local cCodigoTitulo := ""
Local itensJson
Local retJson

Private lMsErroAuto := .F.
Private lMSHelpAuto := .F. // para nao mostrar os erro na tela

	cBody := Self:GetContent()

	retJson := oJson:fromJson(cBody)
	itensJson := oJson:GetNames()

	AAdd(aTitulo , {"E2_FILIAL"  , xFilial("SE2") , Nil})
	For nI := 1 To Len(itensJson)
		AAdd(aTitulo , {itensJson[nI] , CvJsonVal(oJson[itensJson[nI]], TamSx3(itensJson[nI])) , Nil})
	Next nI

	aValid := vldCrtTit(aTitulo)
	If aValid[1]
		MSExecAuto({|a,b,c| FINA050(a,b,c)}, aTitulo, Nil, 3) //Efetua a operacao

		If (lMsErroAuto)
			setRespError(500, STR0035) //"Ocorreu um erro durante a cria��o do Titulo a pagar"
		EndIf
	else
		setRespError(400, aValid[2])
	EndIf

	lRet := aValid[1] .And. !lMsErroAuto
	If lRet
		extSE2ByPk(aTitulo, @cRecno)
		aAdd(aJsonExtra, {"Recno", cRecno})

		cCodigoTitulo := SE2->E2_FILIAL + SE2->E2_PREFIXO + SE2->E2_NUM + SE2->E2_PARCELA + SE2->E2_TIPO + SE2->E2_FORNECE + SE2->E2_LOJA
		cCodigoTitulo := Encode64(cCodigoTitulo)
		aAdd(aJsonExtra, {"PkTitulo", cCodigoTitulo })
		Self:SetResponse(getRespOk("CriarTituloPagar", aJsonExtra))
	EndIf
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} POST - LiberaManu
Libera��o de Titulos para pagamento - Em lote
@example POST  -> http://localhost:12173/rest/WSPfsAppCP/libpagmanual
@example Body ->
{
	"titulo": {
		"recno":[
			201,200,199
		]
	}
}

@author Willian Kazahaya
@since 20/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD POST LiberaManu WSREST WSPfsAppCP
Local aTitulo       := {}
Local aDadosTitulo  := {}
Local oReqBody      := Nil
Local cBody         := ""
Local nI            := 0
Local nCodHttp      := 0
Local lAuto         := .T.
Local lRet          := .T.

//-- Vari�veis utilizadas para o controle de erro da rotina autom�tica
Private lMsErroAuto        := .F.
Private lAutoErrNoFile    := .T.
	cBody := Self:GetContent()
	FWJsonDeserialize(cBody,@oReqBody)

	aDadosTitulo := gtDadoByRC(oReqBody:titulo:recno)
	DbSelectArea("SE2")
	SE2->( dbSetOrder(1) )
	For nI := 1 To Len(aDadosTitulo)
		aAdd(aTitulo,{aDadosTitulo[nI][aScan(aDadosTitulo[nI], {|x| x[1] == "E2_PREFIXO"})][2],  ;//Prefixo
					  aDadosTitulo[nI][aScan(aDadosTitulo[nI], {|x| x[1] == "E2_NUM"})][2],      ;//Numero Titulo
					  aDadosTitulo[nI][aScan(aDadosTitulo[nI], {|x| x[1] == "E2_PARCELA"})][2],  ;//Parcela
					  aDadosTitulo[nI][aScan(aDadosTitulo[nI], {|x| x[1] == "E2_TIPO"})][2],     ;//Tipo
					  aDadosTitulo[nI][aScan(aDadosTitulo[nI], {|x| x[1] == "E2_FORNECE"})][2],  ;//Fornecedor
					  aDadosTitulo[nI][aScan(aDadosTitulo[nI], {|x| x[1] == "E2_LOJA"})][2]}     )//Loja

		// Posiciona no Registro para poder realizar a libera��o
		If SE2->( dbSeek( aDadosTitulo[nI][1][2] ) ) .And. Empty(SE2->E2_DATALIB)
			MSExecAuto({|x,y,z,k,a| FA580MAN(x,y,z,k,a)},"SE2", aDadosTitulo[nI][aScan(aDadosTitulo[nI], {|x| x[1] == "Recno"})][2], 2, lAuto, aClone(aTitulo))
		EndIf

		aSize(aTitulo, 0)
		If lMsErroAuto
			setRespError(nCodHttp, STR0036) //"Houve um problema durante a libera��o dos Titulos selecionados!"
			lRet     := .F.
		EndIf
	Next nI

	If lRet
		Self:SetResponse(getRespOk("LiberacaoPagto"))
	EndIf
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} POST - ChqSTitulo
Cria��o de Cheque sem Titulo
@example POST  -> http://localhost:12173/rest/WSPfsAppCP/chequesemtitulo/{recnoTitulo}
@example Body ->
{
  "AUTBANCO": "247",
  "AUTAGENCIA": "1546 ",
  "AUTCONTA": "15465666  ",
  "AUTCHEQUE": "232134324231421",
  "AUTVENCINI": "20200316",
  "AUTVENCFIM": "20200326",
  "AUTFORN": "WYK000",
  "AUTBENEF": "WILLIAN",
  "AUTNATUREZA": "PARTWYK   ",
  "AUTHIST": "TESTE DE CHEQUE"
}

@author Willian Kazahaya
@since 20/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD POST ChqSTitulo PATHPARAM recnoTitulo WSREST WSPfsAppCP
Local aRetAuto      := {}
Local aDadosTitulo  := {}
Local oReqBody      := Nil
Local cBody         := ""
Local lRet          := .T.

//-- Vari�veis utilizadas para o controle de erro da rotina autom�tica
Private lMsErroAuto        := .F.
Private lAutoErrNoFile    := .T.
//Private nValor          := 0

	cBody := Self:GetContent()
	FWJsonDeserialize(cBody,@oReqBody)

	aDadosTitulo := gtDadoByRC(Self:recnoTitulo)
	DbSelectArea("SE2")
	SE2->( dbSetOrder(1) )
	If Len(aDadosTitulo) > 0 .And. ;
	   SE2->( dbSeek( aDadosTitulo[1][1][2] ) )

		aRetAuto := Array(0)
		//nValor := gtValJSON(oReqBody , "AUTVALOR"   , cBody, 0            , TamSx3("E5_VALOR")[1]     )
		aAdd(aRetAuto,{"AUTBANCO"   ,      gtValJSON(oReqBody , "AUTBANCO"   , cBody, ""           , TamSx3("E5_BANCO")[1]     )}) //"Banco"
		aAdd(aRetAuto,{"AUTAGENCIA" ,      gtValJSON(oReqBody , "AUTAGENCIA" , cBody, ""           , TamSx3("E5_AGENCIA")[1]   )}) //"Agencia"
		aAdd(aRetAuto,{"AUTCONTA"   ,      gtValJSON(oReqBody , "AUTCONTA"   , cBody, ""           , TamSx3("E5_CONTA")[1]     )}) //"Conta"
		aAdd(aRetAuto,{"AUTCHEQUE"  ,      gtValJSON(oReqBody , "AUTCHEQUE"  , cBody, ""           , TamSx3("E5_NUMCHEQ")[1]   )}) //"Cheque"
		aAdd(aRetAuto,{"AUTVENCINI" , SToD(gtValJSON(oReqBody , "AUTVENCINI" , cBody, DToS(Date()) , TamSx3("E5_DATA")[1])     )}) //"Dt Inicio"
		aAdd(aRetAuto,{"AUTVENCFIM" , SToD(gtValJSON(oReqBody , "AUTVENCFIM" , cBody, DToS(Date()) , TamSx3("E5_DATA")[1])     )}) //"Dt Fim"
		aAdd(aRetAuto,{"AUTFORN"    ,      gtValJSON(oReqBody , "AUTFORN"    , cBody, ""           , TamSx3("E5_CLIFOR")[1]    )}) //"Fornecedor"
		aAdd(aRetAuto,{"AUTBENEF"   ,      gtValJSON(oReqBody , "AUTBENEF"   , cBody, ""           , 0                         )}) //"Benefici"
		aAdd(aRetAuto,{"AUTNATUREZA",      gtValJSON(oReqBody , "AUTNATUREZA", cBody, ""           , TamSx3("E5_NATUREZ")[1]   )}) //"Natureza"
		//aAdd(aRetAuto,{"AUTVALOR",nValor}) //"Valor"

		//|-------------------------|
		//| COMMIT DA FUN��O        |
		//|-------------------------|
		MSExecAuto({|a,b,c|FINA390(a,b,c)},,aRetAuto,2)

		If lMsErroAuto
			setRespError(500, STR0037) //"Erro ao gerar o cheque para o t�tulo selecionado!"
			lRet := !lMsErroAuto
		EndIf
	EndIf

	If lRet
		Self:SetResponse(getRespOk("ChqueSemTitulo"))
	EndIf
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} POST - BxAutTit
Baixa autom�tica de titulos
@example POST  -> http://localhost:12173/rest/WSPfsAppCP/titautbai
@example Body ->
{
  "recnoTitulos": [
	95,95,96,97,93
  ],
  "codBanco": "247",
  "codAgencia": "1546 ",
  "codConta": "15465666  ",
  "numCheque": "",
  "loteFin": "",
  "codNatureza": "02.020.001",
  "dataBaixa": "20200319"
}

@author Willian Kazahaya
@since 20/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD POST BxAutTit WSREST WSPfsAppCP
Local cBody := Self:GetContent()
Local oReqBody := Nil
Local lRet     := .T.
Local nCodHttp := 0
Local aRecnos  := {}
Local cBanco   := ""
Local cAgencia := ""
Local cConta   := ""
Local cCheque  := ""
Local cLoteFin := ""
Local cNaturez := ""
Local dBaixa   := Date()

Private lMsHelpAuto := .T.
Private lMsErroAuto := .F.

	FWJsonDeserialize(cBody,@oReqBody)

	aRecnos   := gtValJSON(oReqBody, "recnoTitulos"        , cBody)
	cBanco    := gtValJSON(oReqBody, "codBanco"            , cBody)
	cAgencia  := gtValJSON(oReqBody, "codAgencia"          , cBody, "", 5 )
	cConta    := gtValJSON(oReqBody, "codConta"            , cBody, "", 10)
	cCheque   := gtValJSON(oReqBody, "numChque"            , cBody, "", 15)
	cLoteFin  := gtValJSON(oReqBody, "codBanco"            , cBody, "", 4 )
	cNatureza := gtValJSON(oReqBody, "codBanco"            , cBody, "", 10)
	dBaixa    := SToD(gtValJSON(oReqBody, "codBanco"       , cBody, DTOS(Date())))

	If Len(aRecnos) > 0
		lRet := FBxLotAut("SE2",aRecnos,cBanco,cAgencia,cConta,cCheque,cLoteFin,cNaturez,dBaixa)

		// Verifica se o titulo foi baixado
		If !(lRet .And. checkTitulo(aRecnos))
			setRespError(nCodHttp, STR0038) //"Houve um erro no processo de Baixa autom�tica dos titulos selecionados!"
			lRet     := .F.
		EndIf
	Else
		setRespError(nCodHttp, STR0039) //"Os titulos a serem baixados n�o foram informados!"
		lRet      := .F.
	EndIf

	If lRet
		Self:SetResponse(getRespOk("BaixaAutomaticaTitulos"))
	EndIf
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} DELETE - Titulo
Exclus�o do titulo

@author Willian Kazahaya
@since 20/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD DELETE Titulo PATHPARAM recnoTitulo WSREST WSPfsAppCP
Local lRet         := .T.
Local aTitulo      := {}
Local nCodHttp     := 0
Local aDadosTitulo := gtDadoByRC(Self:recnoTitulo, 1)

Private lF080Auto       := .T.
Private lMsErroAuto     := .F.
Private lAutoErrNoFile  := .T.

	DbSelectArea("SE2")
	SE2->( DbSetOrder(1) )
	if Len(aDadosTitulo) > 0 .And. ;
	   SE2->( dbSeek(aDadosTitulo[1][1][2]))
		aAdd(aTitulo, {"E2_PREFIXO" , aDadosTitulo[1][3][2] ,Nil})
		aAdd(aTitulo, {"E2_NUM"     , aDadosTitulo[1][4][2] ,Nil})
		aAdd(aTitulo, {"E2_PARCELA" , aDadosTitulo[1][5][2] ,Nil})
		aAdd(aTitulo, {"E2_TIPO"    , aDadosTitulo[1][6][2] ,Nil})
		aAdd(aTitulo, {"E2_FORNECE" , aDadosTitulo[1][7][2] ,Nil})
		aAdd(aTitulo, {"E2_LOJA"    , aDadosTitulo[1][8][2] ,Nil})
		MSExecAuto({|a,b,c| FINA050(a,b,c)}, aTitulo, Nil, 5) //Efetua a operacao

		If (lMsErroAuto)
			setRespError(nCodHttp, STR0040) // "Ocorreu um erro durante a exclus�o do Titulo a pagar"
			lRet     := .F.
		EndIf
	Else
		setRespError(nCodHttp, STR0033) //  "O titulo n�o foi encontrado!"
		lRet     := .F.
	EndIf

	If lRet
		Self:SetResponse(getRespOk("ExcluirTituloPagar"))
	EndIf
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} DELETE - Exclus�o de Baixa

@example http://localhost:12173/rest/WSPfsAppCP/titexc/{recnoTitulo}
@author Willian Kazahaya
@since 20/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD DELETE ExcluiBaixa PATHPARAM recnoTitulo WSREST WSPfsAppCP
Local aDadosTitulo := {}
Local aErrGrAuto   := {}
Local aBaixa       := {}
Local cAliasDb     := ""
Local cFKTitulo    := getFKTitulo(Self:recnoTitulo)
Local lRet         := .T.
Local nCodHttp     := 0

Private lF080Auto       := .T.
Private lMsErroAuto     := .F.
Private lAutoErrNoFile  := .T.

	aDadosTitulo := gtDadoByRC(Self:recnoTitulo, 1)
	DbSelectArea("SE2")
	SE2->( DbSetOrder(1) )
	If Len(aDadosTitulo) > 0 .And. ;
	   SE2->( dbSeek(aDadosTitulo[1][1][2]))
		AADD(aBaixa, {"E2_PREFIXO" , aDadosTitulo[1][3][2]    ,Nil})
		AADD(aBaixa, {"E2_NUM"     , aDadosTitulo[1][4][2]    ,Nil})
		AADD(aBaixa, {"E2_PARCELA" , aDadosTitulo[1][5][2]    ,Nil})
		AADD(aBaixa, {"E2_TIPO"    , aDadosTitulo[1][6][2]    ,Nil})
		AADD(aBaixa, {"E2_FORNECE" , aDadosTitulo[1][7][2]    ,Nil})
		AADD(aBaixa, {"E2_LOJA"    , aDadosTitulo[1][8][2]    ,Nil})

		MSExecAuto({| a,b,c,d,e,f | FINA080(a,b,c,d,e,f)} ,aBaixa,6,,,,)//3 para baixar ou 5 para cancelar a baixa.

		If lRet .And. !lMsErroAuto
			cAliasDb := GetNextAlias()
			dbUseArea(.T., "TOPCONN", TCGenQry(,,qryFK2(cFKTitulo)), cAliasDb, .F., .T.)

			If (cAliasDb)->( !Eof() )
				If (cAliasDb)->FK2_RECPAG != "R"
					nCodHttp := 500
					lRet := .F.
				EndIf
			Else
				nCodHttp := 500
				lRet := .F.
			EndIf

			(cAliasDb)->( DbCloseArea() )
		Else
			nCodHttp := 500
			lRet := .F.
		EndIf
	Else
		nCodHttp := 404
		lRet := .F.
	EndIf
	If lRet
		Self:SetResponse(getRespOk("ExclusaoBaixa"))
	Else
		aErrGrAuto := GetAutoGrLog()
		Do Case
			Case nCodHttp == 404
				setRespError(nCodHttp, STR0033) //  "O titulo n�o foi encontrado!"
			Case nCodHttp == 500
				If Len(aErrGrAuto) > 0
					setRespError(nCodHttp, aErrGrAuto[1])
				Else
					setRespError(nCodHttp, STR0041) //"Houve problema na Exclus�o da baixa do Titulo selecionado!"
				EndIf
		End Case
	EndIf
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} DELETE - Cancelamento de Baixa

@example http://localhost:12173/rest/WSPfsAppCP/titcan/{recnoTitulo}
@author Willian Kazahaya
@since 20/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD DELETE CancelaBaixa PATHPARAM recnoTitulo WSREST WSPfsAppCP
Local aDadosTitulo := {}
Local aErrGrAuto   := {}
Local aBaixa       := {}
Local cAliasDb     := ""
Local cFKTitulo    := getFKTitulo(Self:recnoTitulo)
Local lRet         := .T.
Local nCodHttp     := 0

Private lF080Auto       := .T.
Private lMsErroAuto     := .F.
Private lAutoErrNoFile  := .T.

	aDadosTitulo := gtDadoByRC(Self:recnoTitulo, 1)
	DbSelectArea("SE2")
	SE2->( DbSetOrder(1) )
	if SE2->( dbSeek(aDadosTitulo[1][1][2]))
		AADD(aBaixa,{"E2_PREFIXO" , aDadosTitulo[1][3][2]    ,Nil})
		AADD(aBaixa,{"E2_NUM"     , aDadosTitulo[1][4][2]    ,Nil})
		AADD(aBaixa,{"E2_PARCELA" , aDadosTitulo[1][5][2]    ,Nil})
		AADD(aBaixa,{"E2_TIPO"    , aDadosTitulo[1][6][2]    ,Nil})
		AADD(aBaixa,{"E2_FORNECE" , aDadosTitulo[1][7][2]    ,Nil})
		AADD(aBaixa,{"E2_LOJA"    , aDadosTitulo[1][8][2]    ,Nil})

		MSExecAuto({| a,b,c,d,e,f | FINA080(a,b,c,d,e,f)} ,aBaixa,5,,,,)//3 para baixar ou 5 para cancelar a baixa.

		If lRet .And. !lMsErroAuto
			cAliasDb := GetNextAlias()
			dbUseArea(.T., "TOPCONN", TCGenQry(,,qryFK2(cFKTitulo)), cAliasDb, .F., .T.)

			If (cAliasDb)->( !Eof() )
				If (cAliasDb)->FK2_RECPAG != "R"
					nCodHttp := 500
					lRet := .F.
				EndIf
			Else
				nCodHttp := 500
				lRet := .F.
			EndIf

			(cAliasDb)->( DbCloseArea() )
		Else
			nCodHttp := 500
			lRet := .F.
		EndIf
	Else
		nCodHttp := 404
		lRet := .F.
	EndIf

	If lRet
		Self:SetResponse(getRespOk("CancelamentoBaixa"))
	Else
		aErrGrAuto := GetAutoGrLog()
		Do Case
			Case nCodHttp == 404
				setRespError(nCodHttp, STR0033) //  "O titulo n�o foi encontrado!"
			Case nCodHttp == 500
				If Len(aErrGrAuto) > 0
					setRespError(nCodHttp, aErrGrAuto[1])
				Else
					setRespError(nCodHttp, STR0034) //"Houve problema no Cancelamento da baixa do Titulo selecionado!"
				EndIf
		End Case
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} DELETE - Border�
Exclus�o de Border�

@example http://localhost:12173/rest/WSPfsAppCP/canbrd/{ChaveBordero}
@author Willian Kazahaya
@since 20/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD DELETE Bordero PATHPARAM ChaveBordero WSREST WSPfsAppCP
Local codBord  := Self:ChaveBordero
Local lRet     := .T.

	If vrBordero(codBord, "FINA240")
		SetMVValue("AFI240","MV_PAR01",codBord)
		FA240Canc()

		If vrBordero(codBord, "FINA240")
			setRespError(500, STR0042) //"N�o foi poss�vel excluir o border�!"
			lRet := .F.
		EndIf
	Else
		setRespError(404, STR0043) //"O border� n�o foi encontrado! Verifique o numero digitado ou j� n�o foi cancelado anteriormente!"
		lRet := .F.
	EndIf

	If lRet
		Self:SetResponse(getRespOk("ExclusaoBordero"))
	EndIf
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} DELETE - Cancelamento de Bordero Impostos

@example http://localhost:12173/rest/WSPfsAppCP/canbrdimp/{ChaveBordero}
@author Willian Kazahaya
@since 20/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD DELETE IBordero PATHPARAM ChaveBordero WSREST WSPfsAppCP


	If vrBordero(codBord, "FINA241")
		SetMVValue("AFI240","MV_PAR01",codBord)
		FA241Canc(/*cAlias*/,/*nReg*/,/*nOpcx*/,/*aBorAut*/,.T.)

		If vrBordero(codBord, "FINA241")
			setRespError(500, STR0044) // "N�o foi poss�vel excluir o border� de impostos!"
			lRet := .F.
		EndIf
	Else
		setRespError(404, STR0045) // "O border� de impostos n�o foi encontrado! Verifique o numero digitado ou j� n�o foi cancelado anteriormente!"
		lRet := .F.
	EndIf

	If lRet
		Self:SetResponse(getRespOk("ExclusaoBorderoImp"))
	EndIf
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} DELETE - Cancela da Faturanotific

@example http://localhost:12173/rest/WSPfsAppCP/canfat/{recnoTitulo}
@author Willian Kazahaya
@since 20/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD DELETE FatPag PATHPARAM recnoTitulo WSREST WSPfsAppCP
Local aFatPagAut   := {}
Local aTitulos     := {}
Local aDadosTitulo := {}
Local aCmpExtra    := {"E2_NATUREZ", "E2_EMISSAO"}
Local lRet         := .T.

	aDadosTitulo := gtDadoByRC(Self:recnoTitulo, 1, aCmpExtra)
	DbSelectArea("SE2")
	SE2->( DbSetOrder(1) )
	If Len(aDadosTitulo) > 0 .And. ;
	   SE2->( dbSeek(aDadosTitulo[1][1][2]))
	   //aDadosTitulo[1][aScan(aDadosTitulo[1], {|x| x[1] == "E2_HIST"})][2]
		aFatPagAut := { aDadosTitulo[1][aScan(aDadosTitulo[1], {|x| x[1] == "E2_PREFIXO"})][2]  /*Prefixo*/,;
						aDadosTitulo[1][aScan(aDadosTitulo[1], {|x| x[1] == "E2_TIPO"})][2]     /*Tipo*/,;
						aDadosTitulo[1][aScan(aDadosTitulo[1], {|x| x[1] == "E2_NUM"})][2]      /*Numero da Fatura*/,;
						aDadosTitulo[1][aScan(aDadosTitulo[1], {|x| x[1] == "E2_NATUREZ"})][2]  /*Natureza*/, ;
						Date()                                                                  /*Data de*/,;
						Date()                                                                  /*Data Ate*/,;
						aDadosTitulo[1][aScan(aDadosTitulo[1], {|x| x[1] == "E2_FORNECE"})][2]  /*Fornecedor*/,;
						aDadosTitulo[1][aScan(aDadosTitulo[1], {|x| x[1] == "E2_LOJA"})][2]     /*Loja*/,;
						""                                                                      /*Fornecedor para geracao*/,;
						""                                                                      /*Loja do fornecedor para geracao*/,;
						""                                                                      /*Condicao de pagto*/,;
																								/*Moeda*/,;
						aTitulos                                                                /*ARRAY com os titulos da fatura*/,;
																								/*Valor de decrescimo*/,;
																								/*Valor de acrescimo*/ }

		lRet := FINA290(4,aFatPagAut) //- nPosArotina,aFatPag

		If !(lRet .And. !vrFatura(aDadosTitulo[1][aScan(aDadosTitulo[1], {|x| x[1] == "E2_PREFIXO"})][2],;
								aDadosTitulo[1][aScan(aDadosTitulo[1], {|x| x[1] == "E2_NUM"})][2]    ,;
								aDadosTitulo[1][aScan(aDadosTitulo[1], {|x| x[1] == "E2_TIPO"})][2]   ,;
								aDadosTitulo[1][aScan(aDadosTitulo[1], {|x| x[1] == "E2_FORNECE"})][2],;
								aDadosTitulo[1][aScan(aDadosTitulo[1], {|x| x[1] == "E2_LOJA"})][2]   ))
			setRespError(500, STR0046) //"N�o foi possivel realizar o cancelamento da fatura informada!"
			lRet := .F.
		EndIf
	else
		setRespError(404, STR0047) //"O t�tulo informado n�o foi encontrado!"
	EndIF

	If lRet
		Self:SetResponse(getRespOk("CancelaFatura"))
	EndIf
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} DELETE - Cancela a compensa��o de titulo

@example http://localhost:12173/rest/WSPfsAppCP/compens/{recnoTitulo}
@author Willian Kazahaya
@since 07/07/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD DELETE CCompensacao PATHPARAM recnoTitulo WSREST WSPfsAppCP
Local aDadosTitulo := {}
Local aCmpExtra    := {"E2_NATUREZ", "E2_EMISSAO"}
Local lRet         := .T.

	aDadosTitulo := gtDadoByRC(Self:recnoTitulo, 1, aCmpExtra)
	DbSelectArea("SE2")
	SE2->( DbSetOrder(1) )
	If Len(aDadosTitulo) > 0 .And. ;
		SE2->( dbSeek(aDadosTitulo[1][1][2]))
		FINA340(4, .T.)
	EndIf

	If lRet
		Self:SetResponse(getRespOk("CancelaCompensacao"))
	EndIf
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} DELETE - Exclui a compensa��o de titulo

@example http://localhost:12173/rest/WSPfsAppCP/compens/estornar/{recnoTitulo}
@author Willian Kazahaya
@since 07/07/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD DELETE ECompensacao PATHPARAM recnoTitulo WSREST WSPfsAppCP
Local aDadosTitulo := {}
Local aCmpExtra    := {"E2_NATUREZ", "E2_EMISSAO"}
Local lRet         := .T.

	aDadosTitulo := gtDadoByRC(Self:recnoTitulo, 1, aCmpExtra)
	DbSelectArea("SE2")
	SE2->( DbSetOrder(1) )
	If Len(aDadosTitulo) > 0 .And. ;
		SE2->( dbSeek(aDadosTitulo[1][1][2]))
		FINA340(5, .T.)
	EndIf

	If lRet
		Self:SetResponse(getRespOk("EstornaCompensacao"))
	EndIf
Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} DELETE - Cancela o cheque

@example http://localhost:12173/rest/WSPfsAppCP/cheque/{recnoTitulo}
@author Willian Kazahaya
@since 07/07/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WSMETHOD DELETE Cheque PATHPARAM recnoTitulo WSREST WSPfsAppCP
Local oReqBody     := nil
Local aRetAuto     := {}
Local aDadosTitulo := {}
Local aCmpExtra    := {"E2_NATUREZ", "E2_EMISSAO"}
Local lRet         := .T.
Local cBody        := ""

	cBody := Self:GetContent()
	FWJsonDeserialize(cBody,@oReqBody)

	aDadosTitulo := gtDadoByRC(Self:recnoTitulo, 1, aCmpExtra)
	DbSelectArea("SE2")
	SE2->( DbSetOrder(1) )
	If Len(aDadosTitulo) > 0 .And. ;
	   SE2->( dbSeek(aDadosTitulo[1][1][2]))

		aRetAuto := Array(0)

		aAdd(aRetAuto,{"AUTBANCO",      gtValJSON(oReqBody , "AUTBANCO"   , cBody, ""           , TamSx3("E5_BANCO")[1]   )}) //"Banco"
		aAdd(aRetAuto,{"AUTAGENCIA",    gtValJSON(oReqBody , "AUTAGENCIA" , cBody, ""           , TamSx3("E5_AGENCIA")[1] )}) //"Agencia"
		aAdd(aRetAuto,{"AUTCONTA",        gtValJSON(oReqBody , "AUTCONTA"   , cBody, ""           , TamSx3("E5_CONTA")[1]   )}) //"Conta"
		aAdd(aRetAuto,{"AUTCHEQUE",        gtValJSON(oReqBody , "AUTCHEQUE"  , cBody, ""           , TamSx3("E5_NUMCHEQ")[1] )}) //"Cheque"
		//|-------------------------|
		//| COMMIT DA FUN��O        |
		//|-------------------------|
		MSExecAuto({|a,b,c|FINA390(a,b,c)},,aRetAuto,5)
	EndIf
Return lRet


//-----------------------------------------------------------------
/*/{Protheus.doc} vldCrtTit(aTitulo)
Valida se os dados s�o validos para a Cria��o do Titulo do Pagar e
se j� existe algum titulo criado com as mesmas informa��es

@param aTitulo - Array de dados do titulo
@since 20/03/2020
@author Willian Kazahaya
/*/
//-----------------------------------------------------------------
Static Function vldCrtTit(aTitulo)
Local aRet    := {.F.,""} // [1] Validado [2] Mensagem
Local cErrMsg := ""
Local nIndex  := 0

	// Verifica se o valor � v�lido
	nIndex := aScan(aTitulo,{|x| x[1] == "E2_VALOR"})
	If (aTitulo[nIndex][2] <= 0)
		cErrMsg := STR0048 // "O valor n�o pode ser menor ou igual a zero!"
	EndIf

	// Verifica que se o titulo j� existe
	If extSE2ByPk(aTitulo)
		cErrMsg := STR0049 // "O titulo informado j� existe na base!"
	EndIf

	aRet[1] := Empty(cErrMsg)
	aRet[2] := cErrMsg
Return aRet

//-----------------------------------------------------------------
/*/{Protheus.doc} JConvUTF8(cValue)
Converte o Texto para UTF8, removendo os CRLF por || e removendo os espa�os laterais

@param cValue - Valor a ser formatado
@since 20/03/2020
@author Willian Kazahaya
/*/
//-----------------------------------------------------------------
Static Function JConvUTF8(cValue)
	Local cReturn := ""
	cReturn := StrTran(JurEncUTF8(Alltrim(cValue)), CRLF, "||")
Return cReturn

//-----------------------------------------------------------------
/*/{Protheus.doc} gtValJSON(oObj, cValor, cJSON, xDefault, nQtdEsp)
Busca o valor no JSON. Verificando se ele existe no JSON e caso seja um campo
de texto, pode preencher o resto do Valor com espa�os

@param oObj     - Objeto JSON
@param cValor   - Valor a ser procurado no Objeto
@param cJSON    - String JSON
@param xDefault - Valor default a ser retornado caso n�o encontre Valor no JSON
@param nQtdEsp  - Quantidade de espa�os a preencher quando o campo � Caracter

@since 20/03/2020
@author Willian Kazahaya
/*/
//-----------------------------------------------------------------
Static Function gtValJSON(oObj, cValor, cJSON, xDefault, nQtdEsp)
Local cRetValue := ""

Default cJSON    := ""
Default xDefault := ""
Default nQtdEsp  := 0


	If !Empty(cJSON)
		// Verifica se o campo existe no JSOn
		If ('"' + cValor + '":' $ StrTran(cJSON, " ", ""))
			cRetValue := &("oObj:" + cValor) // Realiza a macroexecu��o para buscar o valor do Objeto
		Else
			cRetValue := xDefault
		EndIf

		// Caso tenha que autocompletar espa�os e se o valor � String
		If nQtdEsp > 0 .And. ValType(cRetValue) == "C"
			If DecodeUTF8(cRetValue) != Nil
				cRetValue := DecodeUTF8(cRetValue)
			EndIf
			cRetValue := PadL(cRetValue + Space(nQtdEsp), nQtdEsp)
		EndIf
	Else
		cRetValue := xDefault
	EndIf

Return cRetValue

//-----------------------------------------------------------------
/*/{Protheus.doc} CvJsonVal(xValor, aTamSx3)
Inclui os espa�os a direita dos campos caracter e converte as datas

@param xValor - Valor que ser� informado no JSON
@param aTamSx3 - Chamada do TamSx3 do campo

@since 16/06/2020
@author Willian Kazahaya
/*/
//-----------------------------------------------------------------
Static Function CvJsonVal(xValor, aTamSx3)
Local cRetValue := xValor
Local nQtdEsp   := aTamSx3[1]
	if (aTamSx3[3] == "C")
		// Caso tenha que autocompletar espa�os e se o valor � String
		If nQtdEsp > 0 .And. ValType(cRetValue) == "C"
			If DecodeUTF8(cRetValue) != Nil
				cRetValue := DecodeUTF8(cRetValue)
			EndIf
			cRetValue := PadL(cRetValue + Space(nQtdEsp), nQtdEsp)
		EndIf
	elseif (aTamSx3[3] == "D")
		cRetValue := StoD(cRetValue)
	ElseIf (aTamSx3[3] == "N")
		cRetValue := cRetValue
	EndIf
Return cRetValue

//-----------------------------------------------------------------
/*/{Protheus.doc} getSIXTable(cTable, nIndice)
Busca o Indice da tabela

@param cTable - Tabela a ser pesquisada
@param nIndice - Numero do Indice a ser procurado

@since 20/03/2020
@author Willian Kazahaya
/*/
//-----------------------------------------------------------------
Static Function getSIXTable(cTable, nIndice)
Local aArea    := getArea()
Local aSixArea := SIX->(getArea())
Local cIndice  := ""

	DbSelectArea("SIX")
	SIX->( dbSetOrder(1) )

	If SIX->(DbSeek( cTable + cValToChar(nIndice)))
		cIndice := AllTrim(SIX->CHAVE)
	EndIf

	RestArea(aSixArea)
	RestArea(aArea)
Return cIndice

//-----------------------------------------------------------------
/*/{Protheus.doc} gtDadoByRC(cRecno, cIndice, aCmpExtra)
Busca dados da SE2 pelo RECNO.

Caso seja necess�rio, h� um Array para incluir outros campos como o
Saldo. A Query sempre ir� retornar o Indice na primeira posi��o concatenado
e logo ap�s os campos desse mesmo indice separados

@param cRecno - Recno a ser procurado. Pode ser tanto uma String com um Recno ou Array
@param nIndice - Numero do Indice da SE2. Os campos do indice ser�o inseridos na query
@param aCmpExtra - Array de campos extras da SE2

@return aRet
	[Linha][Campo][1] - Nome da Coluna
	[Linha][Campo][2] - Valor da Coluna

@since 20/03/2020
@author Willian Kazahaya
/*/
//-----------------------------------------------------------------
Static Function gtDadoByRC(xRecno, cIndice, aCmpExtra, bFiltDelete)
Local aRet      := {}
Local aCmpIndex := {}
Local aRegistro := {}
Local nI        := 0
Local nLenCmpExt:= 0
Local cIndTable := ""
Local cQuery    := ""
Local cQrySel   := ""
Local cQryFrm   := ""
Local cQryWhr   := ""
Local cAliasDb  := ""
Local cTable    := "SE2"

Default cIndice     := 1
Default aCmpExtra   := {"R_E_C_N_O_"}
Default bFiltDelete := .T.

	cIndTable := getSIXTable(cTable, cIndice)
	cQrySel := " SELECT " + StrTran(cIndTable, "+", "||" ) + " TblIndex, " + StrTran(cIndTable,"+",",")
	cQryFrm := " FROM " + RetSqlName(cTable)
	cQryWhr := " WHERE 1=1 "

	If ValType(xRecno) == "A"
		cQryWhr += " AND R_E_C_N_O_ IN (" + concatArr(xRecno) + ")"
	Else
		cQryWhr += " AND R_E_C_N_O_ = '" + xRecno + "'"
	EndIf
	If bFiltDelete
		cQryWhr += " AND D_E_L_E_T_ = ' ' "
	EndIf

	aCmpIndex := JStrArrDst(cIndTable,"+")
	nLenCmpExt := Len(aCmpExtra)
	For nI := 1 To nLenCmpExt
		If !(aCmpExtra[nI] $ cQrySel)
			cQrySel += "," + aCmpExtra[nI]
			aAdd(aCmpIndex, aCmpExtra[nI])
		Else
			ADel(aCmpExtra,nI)
			nI--
			nLenCmpExt--
		EndIf
	Next nI

	cQuery := ChangeQuery(cQrySel + cQryFrm + cQryWhr)

	cAliasDb := GetNextAlias()
	dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery), cAliasDb, .F., .T.)

	// Monta o retorno, campo a campo
	While (cAliasDb)->( !Eof() )
		aAdd( aRegistro, {"TblIndex" ,(cAliasDb)->TblIndex} )

		For nI := 1 To Len(aCmpIndex)
			Do Case
				Case AllTrim(aCmpIndex[nI]) == "R_E_C_N_O_"
					aAdd( aRegistro, {"Recno", (cAliasDb)->(&(aCmpIndex[nI]))} )
				Otherwise
					aAdd( aRegistro, {aCmpIndex[nI], (cAliasDb)->(&(aCmpIndex[nI]))} )
			End Case
		Next nI

		aAdd(aRet, aClone(aRegistro))
		aSize(aRegistro, 0)

		(cAliasDb)->( dbSkip() )
	EndDo
	(cAliasDb)->( dbCloseArea() )
Return aRet

//-----------------------------------------------------------------
/*/{Protheus.doc} extSE2ByPk(aTitulo)
Verifica se j� existe um titulo gravado com os dados passados

@param aTitulo - Array de dados do Titulo. Obedecendo a estrutura abaixo
	[item][1] - Nome do campo
	[item][2] - Valor

@since 20/03/2020
@author Willian Kazahaya
/*/
//-----------------------------------------------------------------
Static Function extSE2ByPk(aTitulo, cRecno )
Local lRet     := .T.
Local cAliasDb := ""
Local cQrySel  := ""
Local cQryFrm  := ""
Local cQryWhr  := ""
Local cQuery   := ""

Default cRecno := ""

	cQrySel := " SELECT SE2.R_E_C_N_O_ Recno "
	cQryFrm := " FROM " + RetSqlName("SE2") + " SE2 "
	cQryWhr := " WHERE SE2.D_E_L_E_T_ = ' ' "
	cQryWhr += sqlWhrBuild(aTitulo, "E2_FILIAL")
	cQryWhr += sqlWhrBuild(aTitulo, "E2_PREFIXO")
	cQryWhr += sqlWhrBuild(aTitulo, "E2_NUM")
	cQryWhr += sqlWhrBuild(aTitulo, "E2_PARCELA")
	cQryWhr += sqlWhrBuild(aTitulo, "E2_TIPO")
	cQryWhr += sqlWhrBuild(aTitulo, "E2_FORNECE")
	cQryWhr += sqlWhrBuild(aTitulo, "E2_LOJA")

	cQuery := ChangeQuery( cQrySel + cQryFrm + cQryWhr )

	cAliasDb := GetNextAlias()
	dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery), cAliasDb, .F., .T.)

	lRet := (cAliasDb)->( !Eof() )
	if (lRet)
		cRecno := (cAliasDb)->Recno
	EndIf
	(cAliasDb)->( DbCloseArea() )
Return lRet

//-----------------------------------------------------------------
/*/{Protheus.doc} sqlWhrBuild(aValores, cField, cApelido)
Monta a condi��o para o Where, validando o Tipo do dado

@param aValores - Array de valores. Respeitando a estutura
	[item][1]- Nome do campo
	[item][2]- Valor
@param cField - Campo a ser buscado
@param cApelido - Apelido da tabela
@since 20/03/2020
@author Willian Kazahaya
/*/
//-----------------------------------------------------------------
Static Function sqlWhrBuild(aValores, cField, cApelido)
Local cCondicao := ""
Local nIndex    := 0

Default cApelido := "SE2"

	nIndex := aScan(aValores, {|x| x[1]== cField })
	If nIndex > 0
		Do Case
			Case ValType(aValores[nIndex][2]) == "N"
				cCondicao := " AND " + cApelido + "." + cField + " = " + aValores[nIndex][2] + " "
			Case ValType(aValores[nIndex][2]) == "D"
				cCondicao := " AND " + cApelido + "." + cField + " = " + DToS(aValores[nIndex][2]) + " "
			Otherwise
				cCondicao := " AND " + cApelido + "." + cField + " = '" + aValores[nIndex][2] + "' "
		End Case
	EndIf
Return cCondicao

//-----------------------------------------------------------------
/*/{Protheus.doc} getFKTitulo(cSE2Recno)
Busca a FK do RECNO da SE2 passado

@param cSE2Recno - Recno da SE2
@since 20/03/2020
@author Willian Kazahaya
/*/
//-----------------------------------------------------------------
Static Function getFKTitulo(cSE2Recno)
Local cFKIdDoc  := ""
Local cAliasQry := ""

	cAliasQry := GetNextAlias()
	dbUseArea(.T., "TOPCONN", TCGenQry(,,qryFK7IdDoc({cSE2Recno})), cAliasQry, .F., .T.)

	If ((cAliasQry)->( !Eof() ))
		cFKIdDoc := (cAliasQry)->FK7_IDDOC
	EndIf

	(cAliasQry)->(DbCloseArea())

Return cFKIdDoc


//-----------------------------------------------------------------
/*/{Protheus.doc} qryFK2(aRecnos)
Retorna a FK do Titulo

@param aRecnos - Recnos da SE2 a serem consultados
@since 20/03/2020
@author Willian Kazahaya
/*/
//-----------------------------------------------------------------
Static Function qryFK2(cFKIdDoc)
Local cQuery   := ""
Local cQrySel  := ""
Local cQryFrm  := ""
Local cQryWhr  := ""

	cQrySel := " SELECT FK2.FK2_IDDOC, FK2.FK2_RECPAG "
	cQryFrm := " FROM " + RetSqlName("FK2") + " FK2 "
	cQryWhr := " WHERE R_E_C_N_O_ = (SELECT MAX(R_E_C_N_O_) "
	cQryWhr +=                     " FROM " + RetSqlName("FK2") + " FK2SUB  "
	cQryWhr +=                     " WHERE FK2SUB.FK2_IDDOC = '" + cFKIdDoc + "' "
	cQryWhr +=                       " AND FK2SUB.D_E_L_E_T_ = ' ') "

	cQuery := ChangeQuery( cQrySel + cQryFrm + cQryWhr )

Return cQuery

//-----------------------------------------------------------------
/*/{Protheus.doc} qryFK7IdDoc(aRecnos)
Retorna a FK do Titulo

@param aRecnos - Recnos da SE2 a serem consultados
@since 20/03/2020
@author Willian Kazahaya
/*/
//-----------------------------------------------------------------
Static Function qryFK7IdDoc(aRecnos)
Local cQrySel   := ""
Local cQryFrm   := ""
Local cQryWhr   := ""
Local cSE2Recno := ""
Default aRecnos := {}

	cSE2Recno := concatArr(aRecnos)

	cQrySel := " SELECT FK7.FK7_IDDOC, SE2.R_E_C_N_O_ Recno "
	cQryFrm := " FROM " + RetSqlName("SE2") + " SE2 INNER JOIN " + RetSqlName("FK7") + " FK7 ON (FK7.FK7_CHAVE = SE2.E2_FILIAL  + '|' + "
	cQryFrm +=                                                                                                 " SE2.E2_PREFIXO + '|' + "
	cQryFrm +=                                                                                                 " SE2.E2_NUM     + '|' + "
	cQryFrm +=                                                                                                 " SE2.E2_PARCELA + '|' + "
	cQryFrm +=                                                                                                 " SE2.E2_TIPO    + '|' + "
	cQryFrm +=                                                                                                 " SE2.E2_FORNECE + '|' + "
	cQryFrm +=                                                                                                 " SE2.E2_LOJA "
	cQryFrm +=                                                                                                 " AND SE2.D_E_L_E_T_ = ' ') "
	cQryWhr := " WHERE SE2.R_E_C_N_O_ IN (" + cSE2Recno + ")"

	cQuery := ChangeQuery(cQrySel + cQryFrm + cQryWhr)

Return cQuery

//-----------------------------------------------------------------
/*/{Protheus.doc} getRespOk(cOperation)
Monta a estrutura padr�o para Resposta de sucesso

@param cOperation - Opera��o executada
@param aExtraInfo - Array de informa��es extras para o retorno
		[1] - Nome para o Response
		[2] - Valor
@since 20/03/2020
@author Willian Kazahaya
/*/
//-----------------------------------------------------------------
Static Function getRespOk(cOperation, aExtraInfo)
Local oResponse := JsonObject():New()
Local cMessage  := ""
Local nI        := 0

Default aExtraInfo := {}

	oResponse['operation'] := cOperation
	Do Case
		Case cOperation == "BaixaTitulo"
			cMessage :=  STR0050 //"A baixa do t�tulo foi realizada com sucesso!"
		Case cOperation == "CancelamentoBaixa"
			cMessage :=  STR0051 //"A baixa do t�tulo foi cancelada com sucesso!"
		Case cOperation == "ExclusaoBaixa"
			cMessage :=  STR0052 //"A baixa do t�tulo foi excluida com sucesso!"
		Case cOperation == "BaixaAutomaticaTitulos"
			cMessage :=  STR0053 //"A Baixa dos t�tulos selecionados foi realizada com sucesso!"
		Case cOperation == "CriarTituloPagar"
			cMessage :=  STR0054 //"O t�tulo foi criado com sucesso!"
		Case cOperation == "AlterarTituloPagar"
			cMessage :=  STR0055 //"O t�tulo foi alterado com sucesso!"
		Case cOperation == "ExcluirTituloPagar"
			cMessage :=  STR0056 //"O t�tulo foi excluido com sucesso!"
		Case cOperation == "SubstituirTituloPagar"
			cMessage :=  STR0057 //"O t�tulo foi substituido com sucesso!"
		Case cOperation == "LiberacaoPagto"
			cMessage :=  STR0058 //"T�tulo(s) liberado(s) com sucesso!"
		Case cOperation == "ExclusaoBorderoImp"
			cMessage :=  STR0059 //"O border� de impostos foi excluido com sucesso!"
		Case cOperation == "ExclusaoBordero"
			cMessage :=  STR0060 //"O border� foi excluido com sucesso!"
		Case cOperation == "CancelaFatura"
			cMessage :=  STR0061 //"Fatura cancelada com sucesso!"
		Case cOperation == "CancelaCompensacao"
			cMessage :=  STR0062 //"A compensa��o foi cancelada com sucesso!"
		Case cOperation == "EstornaCompensacao"
			cMessage :=  STR0063 //"A compensa��o foi estornada com sucesso!"
		Case cOperation == "DesdobramentoSimples"
			cMessage :=  STR0064 //"A opera��o de desdobramento foi realizada com sucesso!"
		Case cOperation == "DesdobramentoTransitoria"
			cMessage :=  STR0065 //"A opera��o de transit�ria foi realizada com sucesso!"
		Case cOperation == "DesdobramentoPosPagto"
			cMessage :=  STR0066 //"A opera��o de desdobramento p�s-pagamento foi realizada com sucesso!"
		Otherwise
			cMessage := STR0067 + " [" + cOperation + "]."
	End Case

	oResponse['status']  = 201
	oResponse['message'] = JurEncUTF8(cMessage)

	For nI := 1 To Len(aExtraInfo)
		oResponse[aExtraInfo[nI][1]] := aExtraInfo[nI][2]
	Next nI

Return FWJsonSerialize(oResponse, .F., .F., .T.)

//-----------------------------------------------------------------
/*/{Protheus.doc} setRespError(nCodHttp, cErrMessage)
Padroniza a resposta sempre convertendo o texto para UTF-8

@param nCodHttp - C�digo HTTP
@param cErrMessage - Mensagem de erro a ser convertido

@since 20/03/2020
@author Willian Kazahaya
/*/
//-----------------------------------------------------------------
Static Function setRespError(nCodHttp, cErrMessage)
	SetRestFault(nCodHttp, JConvUTF8(cErrMessage), .T.)
Return nil


//-----------------------------------------------------------------
/*/{Protheus.doc} concatArr(aValues)
Concatena os valores presentes no Array

@param aValues - Valores a serem concatenados
@since 20/03/2020
@author Willian Kazahaya
/*/
//-----------------------------------------------------------------
Static Function concatArr(aValues)
Local nI := 0
Local cRet := ""

	For nI := 1 To Len(aValues)
		Do case
			Case ValType(aValues[nI]) == "N"
				cRet += cValToChar(aValues[nI]) + ','
			Case ValType(aValues[nI]) == "D"
				cRet += DToS(aValues[nI]) + ','
			Otherwise
				cRet += "'"+ aValues[nI] + "',"
		End Case
	Next nI

	// Remove a ultima virgula
	If !Empty(cRet)
		cRet := SubStr(cRet, 1, Len(cRet)-1)
	EndIf
Return cRet

//-----------------------------------------------------------------
/*/{Protheus.doc} checkTitulo(aRecnos)
Monta a estrutura padr�o para Resposta de sucesso

@param cOperation - Opera��o executada
@since 20/03/2020
@author Willian Kazahaya
/*/
//-----------------------------------------------------------------
Static Function checkTitulo(aRecnos)
Local lRet     := .F.
Local cRecnos  := ""
Local cQrySel  := ""
Local cQryFrm  := ""
Local cQryWhr  := ""
Local cQuery   := ""
Local cAliasDb := ""

Default aRecnos := {}

	If Len(aRecnos) > 0
		cRecnos := concatArr(aRecnos)

		cQrySel := " SELECT SE2.R_E_C_N_O_ RECNO, FK7.FK7_CHAVE , COALESCE(FK2.FK2_RECPAG, '-') FK2_RECPAG "
		cQryFrm := " FROM " + RetSqlName("SE2") + " SE2 INNER JOIN " + RetSqlName("FK7") + " FK7 ON (FK7.FK7_CHAVE = SE2.E2_FILIAL  + '|' + "
		cQryFrm +=                                                                                                 " SE2.E2_PREFIXO + '|' + "
		cQryFrm +=                                                                                                 " SE2.E2_NUM     + '|' + "
		cQryFrm +=                                                                                                 " SE2.E2_PARCELA + '|' + "
		cQryFrm +=                                                                                                 " SE2.E2_TIPO    + '|' + "
		cQryFrm +=                                                                                                 " SE2.E2_FORNECE + '|' + "
		cQryFrm +=                                                                                                 " SE2.E2_LOJA
		cQryFrm +=                                                                             " AND FK7.D_E_L_E_T_ = ' ') "
		cQryFrm +=                                    " LEFT  JOIN (SELECT FK2SUB.* "
		cQryFrm +=                                                " FROM " + RetSqlName("FK2") + " FK2SUB INNER JOIN (SELECT MAX(FK2SUBMAX.R_E_C_N_O_) RECNO "
		cQryFrm +=                                                                                                  " FROM " + RetSqlName("FK2") + " FK2SUBMAX "
		cQryFrm +=                                                                                                  " GROUP BY FK2SUBMAX.FK2_IDDOC) FK7MAX ON (FK7MAX.RECNO = FK2SUB.R_E_C_N_O_)
		cQryFrm +=                                                " WHERE FK2SUB.D_E_L_E_T_ = ' ') FK2 ON (FK2.FK2_IDDOC = FK7.FK7_IDDOC) "
		cQryWhr := " WHERE SE2.R_E_C_N_O_ in (" + cRecnos + ")"

		// O subselect retornar� o ultimo registro de Baixa dos titulos selecionados
		cQuery := ChangeQuery( cQrySel + cQryFrm + cQryWhr )
		cAliasDb := GetNextAlias()

		dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery), cAliasQry, .F., .T.)

		While ((cAliasQry)->( !Eof() )) .And. lRet
			lRet := !((cAliasQry)->(FK2_RECPAG) != "P")

			(cAliasQry)->( dbSkip() )
		EndDo

		(cAliasQry)->(DbCloseArea())

	EndIf
Return lRet

//-----------------------------------------------------------------
/*/{Protheus.doc} vrBordero(codBord, cRotina)
Verifica a quantidade de Borderos abertos para aquele titulo

@param codBord - C�digo do Bordero
@param cRotina - C�digo da Rotina de Origem. Default: Vazio

@return Boolean - H� mais de um Bordero para aquele titulo
@since 20/03/2020
@author Willian Kazahaya
/*/
//-----------------------------------------------------------------
Static Function vrBordero(codBord, cRotina)
Local cAliasQry := ""
Local cQuery    := ""
Local cQrySel   := ""
Local cQryFrm   := ""
Local cQryWhr   := ""

Default cRotina := ""

	cQrySel := " SELECT COUNT(*) QTD"
	cQryFrm := " FROM " + RetSqlName("SEA") + " SEA"
	cQryWhr := " WHERE EA_NUMBOR = '" + codBord + "'"
	cQryWhr += " AND D_E_L_E_T_ = ' ' "

	If !Empty(cRotina)
		cQryWhr += " AND EA_ORIGEM = '" + cRotina + "'"
	EndIf

	cQuery := ChangeQuery(cQrySel + cQryFrm + cQryWhr)
	cAliasQry := GetNextAlias()
	dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery), cAliasQry, .F., .T.)

Return (cAliasQry)->QTD > 0

//-----------------------------------------------------------------
/*/{Protheus.doc} gtMaxSqTrn(cFKTitulo, cTabela)
Verifica a quantidade de Faturas abertas para aquele titulo.

@param cPrefixo - Prefixo do titulo
@param cNumero  - Numero do Titulo
@param cTipo    - Tipo do Titulo
@param cFornCod - C�digo do Fornecedor
@param cFornLoj - Loja do fornecedor

@return Boolean - Se h� mais de uma fatura para aquele titulo

@since 20/03/2020
@author Willian Kazahaya
/*/
//-----------------------------------------------------------------
Static Function vrFatura(cPrefixo, cNumero, cTipo, cFornCod, cFornLoj)
Local cQuery    := ""
Local cQrySel   := ""
Local cQryFrm   := ""
Local cQryWhr   := ""
Local cAliasQry := ""

	cQrySel := " SELECT COUNT(*) QTD"
	cQryFrm := " FROM " + RetSqlName("SE2")
	cQryWhr := " WHERE D_E_L_E_T_ = ' '"
	cQryWhr +=   " AND E2_FATPREF = '" + cPrefixo + "'"
	cQryWhr +=   " AND E2_FATURA  = '" + cNumero  + "'"
	cQryWhr +=   " AND E2_TIPOFAT = '" + cTipo    + "'"
	cQryWhr +=   " AND E2_FATFOR  = '" + cFornCod + "'"
	cQryWhr +=   " AND E2_LOJA    = '" + cFornLoj + "'"

	cQuery := ChangeQuery(cQrySel + cQryFrm + cQryWhr)
	cAliasQry := GetNextAlias()
	dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery), cAliasQry, .F., .T.)

Return (cAliasQry)->QTD > 0

//-----------------------------------------------------------------
/*/{Protheus.doc} gtMaxSqTrn(cFKTitulo, cTabela)
Retorna o maior sequencial de desdobramento disponivel daquele titulo

@param cFKTitulo - C�digo do Titulo da FK7
@param cTabela   - Tabela a ser consultada. Default: OHF
@since 20/03/2020
@author Willian Kazahaya
/*/
//-----------------------------------------------------------------
Static Function gtMaxSqTrn(cFKTitulo, cTabela)
Local cQuery    := ""
Local cQrySel   := ""
Local cQryFrm   := ""
Local cQryWhr   := ""
Local cAliasQry := ""
Local cIdRet    := "0"

Default cTabela := "OHF"

	cQrySel := " SELECT MAX(" + cTabela + "_CITEM) MAXID "
	cQryFrm := " FROM " + RetSqlName(cTabela)
	cQryWhr := " WHERE " + cTabela + "_IDDOC = '" + cFKTitulo + "'"

	cQuery := ChangeQuery(cQrySel + cQryFrm + cQryWhr)
	cAliasQry := GetNextAlias()
	dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery), cAliasQry, .F., .T.)

	If (cAliasQry)->( !Eof() )
		cIdRet := (cAliasQry)->MAXID
	EndIf
Return Val(cIdRet)
