#INCLUDE "JURA080.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} JURA080
Cadastro de arm�rios 

@author Andr�ia S. N. de Lima
@since 10/03/10                                   
@version 1.0
/*/
//-------------------------------------------------------------------
Function JURA080()
Local oBrowse

oBrowse := FWMBrowse():New()
oBrowse:SetDescription( STR0007 )
oBrowse:SetAlias( "NSK" )
oBrowse:SetLocate()
JurSetLeg( oBrowse, "NSK" )
JurSetBSize( oBrowse )
oBrowse:Activate()

Return NIL


//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menu Funcional

@return aRotina - Estrutura
[n,1] Nome a aparecer no cabecalho
[n,2] Nome da Rotina associada
[n,3] Reservado
[n,4] Tipo de Transa��o a ser efetuada:
1 - Pesquisa e Posiciona em um Banco de Dados
2 - Simplesmente Mostra os Campos
3 - Inclui registros no Bancos de Dados
4 - Altera o registro corrente
5 - Remove o registro corrente do Banco de Dados
[n,5] Nivel de acesso
[n,6] Habilita Menu Funcional

@author Andr�ia S. N. de Lima
@since 10/03/10
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
Local aRotina := {}

aAdd( aRotina, { STR0001, "PesqBrw"        , 0, 1, 0, .T. } ) // "Pesquisar"
aAdd( aRotina, { STR0002, "VIEWDEF.JURA080", 0, 2, 0, NIL } ) // "Visualizar"
aAdd( aRotina, { STR0003, "VIEWDEF.JURA080", 0, 3, 0, NIL } ) // "Incluir"
aAdd( aRotina, { STR0004, "VIEWDEF.JURA080", 0, 4, 0, NIL } ) // "Alterar"
aAdd( aRotina, { STR0005, "VIEWDEF.JURA080", 0, 5, 0, NIL } ) // "Excluir"
aAdd( aRotina, { STR0006, "VIEWDEF.JURA080", 0, 6, 0, NIL } ) // "Imprimir"

Return aRotina


//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Arm�rios

@author Andr�ia S. N. de Lima
@since 10/03/10
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
Local oView
Local oModel  := FWLoadModel( "JURA080" )
Local oStruct := FWFormStruct( 2, "NSK" )

JurSetAgrp( 'NSK',, oStruct )

oView := FWFormView():New()
oView:SetModel( oModel )
oView:AddField( "JURA080_VIEW", oStruct, "NSKMASTER"  )
oView:CreateHorizontalBox( "FORMFIELD", 100 )
oView:SetOwnerView( "JURA080_VIEW", "FORMFIELD" )
oView:SetDescription( STR0007 ) // "Arm�rios"
oView:EnableControlBar( .T. )

Return oView


//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
"Arm�rios"

@author Andr�ia S. N. de Lima
@since 10/03/10
@version 1.0

@obs NSKMASTER - "Arm�rios"

/*/
//-------------------------------------------------------------------
Static Function Modeldef()
Local oModel     := NIL
Local oStruct    := FWFormStruct( 1, "NSK" )

//-----------------------------------------
//Monta o modelo do formul�rio
//-----------------------------------------
oModel:= MPFormModel():New( "JURA080", /*Pre-Validacao*/, { |oX| J080POSVAL(oX) }/*Pos-Validacao*/, /*Commit*/,/*Cancel*/)
oModel:AddFields( "NSKMASTER", NIL, oStruct, /*Pre-Validacao*/, /*Pos-Validacao*/ )
oModel:SetDescription( STR0008 ) // "Modelo de dados de Arm�rios"
oModel:GetModel( "NSKMASTER" ):SetDescription( STR0009 ) // "Dados de Arm�rios"
JurSetRules( oModel, "NSKMASTER",, "NSK",, "JURA080" )

Return oModel 


//-------------------------------------------------------------------
/*/ { Protheus.doc } J080QTCEAR
Rotina para verificar a quantidade de c�lulas por arm�rio

@author Andr�ia S. N. de Lima
@since 11/03/10
@version 1.0
/*/
//-------------------------------------------------------------------
Function J080QTCEAR(oModel)
Local	lRet     := .T.
Local cQuery   := ''     
Local aArea    := GetArea()
Local cResQRY  := GetNextAlias()

	cQuery := "SELECT COUNT(NSL.NSL_COD) NSLQTDE "
	cQuery += "  FROM " + RetSqlName("NSL") + " NSL "
	cQuery += " WHERE NSL.D_E_L_E_T_ = ' ' "
	cQuery += "   AND NSL.NSL_FILIAL = '" + xFilial( "NSK" ) +"' "
	cQuery += "   AND NSL.NSL_CARM = '"  + M->NSK_COD + "'"
	
	cQuery := ChangeQuery(cQuery)
			
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cResQRY,.T.,.T.)

	If (cResQRY)->NSLQTDE > M->NSK_CELULA
  	  lRet := .F.  
 	    JurMsgErro(STR0010) // "N�o � poss�vel reduzir a capacidade do arm�rio antes de remanejar as c�lulas."       
	EndIf

	dbSelectArea(cResQRY)
 	(cResQRY)->( dbcloseArea() )

	RestArea( aArea )	

return lRet   

//-------------------------------------------------------------------
/*/ { Protheus.doc } J080POSVAL(oModel)
Rotinas executadas no p�s-valida��o do model

@author Andr�ia S. N. de Lima
@since 11/03/10
@version 1.0
/*/
//-------------------------------------------------------------------
function J080POSVAL(oModel)
Local lok	:= .T.

/* 2 visualizar
   3 incluir
   4 editar
   5 excluir*/

  if oModel:GetOperation()==4 
    lok := J080QTCEAR(oModel)     
  endif

return lok
