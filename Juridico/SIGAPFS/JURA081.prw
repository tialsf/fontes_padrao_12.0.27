#INCLUDE "JURA081.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} JURA081
Cadastro de c�lulas

@author Andr�ia S. N. de Lima
@since 11/03/10                                   
@version 1.0
/*/
//-------------------------------------------------------------------
Function JURA081()
Local oBrowse

oBrowse := FWMBrowse():New()
oBrowse:SetDescription( STR0007 )
oBrowse:SetAlias( "NSL" )
oBrowse:SetLocate()
JurSetLeg( oBrowse, "NSL" )
JurSetBSize( oBrowse )
oBrowse:Activate()

Return NIL


//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menu Funcional

@return aRotina - Estrutura
[n,1] Nome a aparecer no cabecalho
[n,2] Nome da Rotina associada
[n,3] Reservado
[n,4] Tipo de Transa��o a ser efetuada:
1 - Pesquisa e Posiciona em um Banco de Dados
2 - Simplesmente Mostra os Campos
3 - Inclui registros no Bancos de Dados
4 - Altera o registro corrente
5 - Remove o registro corrente do Banco de Dados
[n,5] Nivel de acesso
[n,6] Habilita Menu Funcional

@author Andr�ia S. N. de Lima
@since 11/03/10
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
Local aRotina := {}

aAdd( aRotina, { STR0001, "PesqBrw"        , 0, 1, 0, .T. } ) // "Pesquisar"
aAdd( aRotina, { STR0002, "VIEWDEF.JURA081", 0, 2, 0, NIL } ) // "Visualizar"
aAdd( aRotina, { STR0003, "VIEWDEF.JURA081", 0, 3, 0, NIL } ) // "Incluir"
aAdd( aRotina, { STR0004, "VIEWDEF.JURA081", 0, 4, 0, NIL } ) // "Alterar"
aAdd( aRotina, { STR0005, "VIEWDEF.JURA081", 0, 5, 0, NIL } ) // "Excluir"
aAdd( aRotina, { STR0006, "VIEWDEF.JURA081", 0, 6, 0, NIL } ) // "Imprimir"

Return aRotina


//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
C�lulas

@author Andr�ia S. N. de Lima
@since 11/03/10
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
Local oView
Local oModel  := FWLoadModel( "JURA081" )
Local oStruct := FWFormStruct( 2, "NSL" )

JurSetAgrp( 'NSL',, oStruct )

oView := FWFormView():New()
oView:SetModel( oModel )
oView:AddField( "JURA081_VIEW", oStruct, "NSLMASTER"  )
oView:CreateHorizontalBox( "FORMFIELD", 100 )
oView:SetOwnerView( "JURA081_VIEW", "FORMFIELD" )
oView:SetDescription( STR0007 ) // "C�lulas"
oView:EnableControlBar( .T. )

Return oView


//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
"C�lulas"

@author Andr�ia S. N. de Lima
@since 11/03/10
@version 1.0

@obs NSLMASTER - "C�lulas"

/*/
//-------------------------------------------------------------------
Static Function Modeldef()
Local oModel     := NIL
Local oStruct    := FWFormStruct( 1, "NSL" )

//-----------------------------------------
//Monta o modelo do formul�rio
//-----------------------------------------
oModel:= MPFormModel():New( "JURA081", /*Pre-Validacao*/,{ |oX| J081POSVAL(oX) } /*Pos-Validacao*/, /*Commit*/,/*Cancel*/)
oModel:AddFields( "NSLMASTER", NIL, oStruct, /*Pre-Validacao*/, /*Pos-Validacao*/ )
oModel:SetDescription( STR0008 ) // "Modelo de dados de C�lulas"
oModel:GetModel( "NSLMASTER" ):SetDescription( STR0009 ) // "Dados de C�lulas"
JurSetRules( oModel, "NSLMASTER",, "NSL",, "JURA081" )

Return oModel

//-------------------------------------------------------------------
/*/ { Protheus.doc } J081POSVAL(oModel)
Rotinas executadas no p�s-valida��o do model

@author Andr�ia S. N. de Lima
@since 11/03/10
@version 1.0
/*/
//-------------------------------------------------------------------
function J081POSVAL(oModel)
Local lok := .T.

  if oModel:GetOperation()==3 .OR. oModel:GetOperation()==4
    lok := J081QTCEAR(oModel)     
  endif  
  
  if oModel:GetOperation()==4 
    lok := J080QTCESC(oModel)     
  endif  

return lok
       

//-------------------------------------------------------------------
/*/ { Protheus.doc } J081QTCEAR
Rotina para verificar o limite de c�lulas por arm�rio

@author Andr�ia S. N. de Lima
@since 11/03/10
@version 1.0
/*/
//-------------------------------------------------------------------
Function J081QTCEAR(oModel)
Local	lRet     := .T.
Local cQuery   := ''     
Local nQtdCel  := Posicione("NSK",1,xFilial("NSK")+FWFLDGET("NSL_CARM"),"NSK_CELULA")
Local aArea    := GetArea()
Local cResQRY  := GetNextAlias()

	cQuery := "SELECT COUNT(NSL.NSL_COD) NSLQTDE "
	cQuery += "  FROM " + RetSqlName("NSL") + " NSL "
	cQuery += " WHERE NSL.D_E_L_E_T_ = ' ' "
	cQuery += "   AND NSL.NSL_FILIAL = '" + xFilial( "NSL" ) +"' "
	cQuery += "   AND NSL.NSL_CARM = '"  + M->NSL_CARM + "'"
	
	cQuery := ChangeQuery(cQuery)
			
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cResQRY,.T.,.T.)

	If !(cResQRY)->NSLQTDE+1 <= nQtdCel
  	  lRet := .F.  
 	    JurMsgErro(STR0010+" "+alltrim(STR(nQtdCel))+" "+STR0007) // "O arm�rio selecionado possui capacidade somente para () C�lulas."       
	EndIf

	dbSelectArea(cResQRY)
 	(cResQRY)->( dbcloseArea() )

	RestArea( aArea )	

return lRet   

//-------------------------------------------------------------------
/*/ { Protheus.doc } J081QTCESC
Rotina para verificar a quantidade de escaninhos por c�lula.

@author Andr�ia S. N. de Lima
@since 12/03/10
@version 1.0
/*/
//-------------------------------------------------------------------
Function J080QTCESC(oModel)
Local	lRet     := .T.
Local cQuery   := ''     
Local aArea    := GetArea()
Local cResQRY  := GetNextAlias()

	cQuery := "SELECT COUNT(NSN.NSN_COD) NSLQTDE "
	cQuery += "  FROM " + RetSqlName("NSN") + " NSN "
	cQuery += " WHERE NSN.D_E_L_E_T_ = ' ' "
	cQuery += "   AND NSN.NSN_FILIAL = '" + xFilial( "NSN" ) +"' "
	cQuery += "   AND NSN.NSN_CCEL = '"  + M->NSL_COD + "'"
	
	cQuery := ChangeQuery(cQuery)
			
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cResQRY,.T.,.T.)

	If (cResQRY)->NSLQTDE > M->NSL_ESCANI
  	  lRet := .F.  
 	    JurMsgErro(STR0011) // "N�o � poss�vel reduzir a capacidade das c�lulas antes de remanejar os escaninhos."       
	EndIf

	dbSelectArea(cResQRY)
 	(cResQRY)->( dbcloseArea() )

	RestArea( aArea )	

return lRet   


