#INCLUDE "JURA083.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} JURA083
Cadastro de escaninhos

@author Andr�ia S. N. de Lima
@since 12/03/10                                   
@version 1.0
/*/
//-------------------------------------------------------------------
Function JURA083()
Local oBrowse

oBrowse := FWMBrowse():New()
oBrowse:SetDescription( STR0007 )
oBrowse:SetAlias( "NSN" )
oBrowse:SetLocate()
JurSetLeg( oBrowse, "NSN" )
JurSetBSize( oBrowse )
oBrowse:Activate()

Return NIL


//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menu Funcional

@return aRotina - Estrutura
[n,1] Nome a aparecer no cabecalho
[n,2] Nome da Rotina associada
[n,3] Reservado
[n,4] Tipo de Transa��o a ser efetuada:
1 - Pesquisa e Posiciona em um Banco de Dados
2 - Simplesmente Mostra os Campos
3 - Inclui registros no Bancos de Dados
4 - Altera o registro corrente
5 - Remove o registro corrente do Banco de Dados
[n,5] Nivel de acesso
[n,6] Habilita Menu Funcional

@author Andr�ia S. N. de Lima
@since 12/03/10
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
Local aRotina := {}

aAdd( aRotina, { STR0001, "PesqBrw"        , 0, 1, 0, .T. } ) // "Pesquisar"
aAdd( aRotina, { STR0002, "VIEWDEF.JURA083", 0, 2, 0, NIL } ) // "Visualizar"
aAdd( aRotina, { STR0003, "VIEWDEF.JURA083", 0, 3, 0, NIL } ) // "Incluir"
aAdd( aRotina, { STR0004, "VIEWDEF.JURA083", 0, 4, 0, NIL } ) // "Alterar"
aAdd( aRotina, { STR0005, "VIEWDEF.JURA083", 0, 5, 0, NIL } ) // "Excluir"
aAdd( aRotina, { STR0006, "VIEWDEF.JURA083", 0, 6, 0, NIL } ) // "Imprimir"

Return aRotina


//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Escaninhos

@author Andr�ia S. N. de Lima
@since 12/03/10
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
Local oView
Local oModel  := FWLoadModel( "JURA083" )
Local oStruct := FWFormStruct( 2, "NSN" )

JurSetAgrp( 'NSN',, oStruct )

oView := FWFormView():New()
oView:SetModel( oModel )
oView:AddField( "JURA083_VIEW", oStruct, "NSNMASTER"  )
oView:CreateHorizontalBox( "FORMFIELD", 100 )
oView:SetOwnerView( "JURA083_VIEW", "FORMFIELD" )
oView:SetDescription( STR0007 ) // "Escaninhos"
oView:EnableControlBar( .T. )

Return oView


//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
"Escaninhos"

@author Andr�ia S. N. de Lima
@since 12/03/10
@version 1.0

@obs NSNMASTER - "Escaninhos"

/*/
//-------------------------------------------------------------------
Static Function Modeldef()
Local oModel     := NIL
Local oStruct    := FWFormStruct( 1, "NSN" )

//-----------------------------------------
//Monta o modelo do formul�rio
//-----------------------------------------
oModel:= MPFormModel():New( "JURA083", /*Pre-Validacao*/,{ |oX| J083POSVAL(oX) } /*Pos-Validacao*/, /*Commit*/,/*Cancel*/)
oModel:AddFields( "NSNMASTER", NIL, oStruct, /*Pre-Validacao*/, /*Pos-Validacao*/ )
oModel:SetDescription( STR0008 ) // "Modelo de dados de escaninhos"
oModel:GetModel( "NSNMASTER" ):SetDescription( STR0009 ) // "Dados de escaninhos"
JurSetRules( oModel, "NSNMASTER",, "NSN",, "JURA083" )

Return oModel

//-------------------------------------------------------------------
/*/ { Protheus.doc } J083POSVAL(oModel)
Rotinas executadas no p�s-valida��o do model

@author Andr�ia S. N. de Lima
@since 12/03/10
@version 1.0
/*/
//-------------------------------------------------------------------
function J083POSVAL(oModel)
Local lok := .T.

  if oModel:GetOperation()==3 .OR. oModel:GetOperation()==4
    lok := J083QTCEAR(oModel)     
  endif

return lok
       

//-------------------------------------------------------------------
/*/ { Protheus.doc } J083QTCEAR
Rotina para verificar o limite de escaninhos por c�lula.

@author Andr�ia S. N. de Lima
@since 12/03/10
@version 1.0
/*/
//-------------------------------------------------------------------
Function J083QTCEAR(oModel)
Local	lRet     := .T.
Local cQuery   := ''     
Local nQtdEsc  := Posicione("NSL",1,xFilial("NSL")+FWFLDGET("NSN_CCEL"),"NSL_ESCANI")
Local aArea    := GetArea()
Local cResQRY  := GetNextAlias()

	cQuery := "SELECT COUNT(NSN.NSN_COD) NSNQTDE "
	cQuery += "  FROM " + RetSqlName("NSN") + " NSN "
	cQuery += " WHERE NSN.D_E_L_E_T_ = ' ' "
	cQuery += "   AND NSN.NSN_FILIAL = '" + xFilial( "NSN" ) +"' "
	cQuery += "   AND NSN.NSN_CCEL = '"  + M->NSN_CCEL + "'"
	
	cQuery := ChangeQuery(cQuery)
			
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cResQRY,.T.,.T.)

	If !(cResQRY)->NSNQTDE+1 <= nQtdEsc
  	  lRet := .F.  
 	    JurMsgErro(STR0010+" "+alltrim(STR(nQtdEsc))+" "+STR0007) // "A c�lula selecionada possui capacidade somente para () escaninhos."       
	EndIf

	dbSelectArea(cResQRY)
 	(cResQRY)->( dbcloseArea() )

	RestArea( aArea )	

return lRet
