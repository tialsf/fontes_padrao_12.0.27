#INCLUDE "JURA141.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} JURA141
Inclus�o de WO - Fatura

@author David Gon�alves Fernandes
@since 29/12/2009
@version 1.0
/*/
//-------------------------------------------------------------------
Function JURA141()
Local lVldUser     := IIf(FindFunction("JurVldUxP"), JurVldUxP(), .T.)
Local lJura074     := FWIsInCallStack("JURA074") // Cadastro de Protocolos 
Local cFiltro      := J141Filter(lJura074)

Private oBrowse
Private lMarcar    := .F.

If lVldUser
	oBrowse := FWMarkBrowse():New()
	If lJura074
		oBrowse:SetDescription( STR0016 ) //"Gera��o Autom�tica" - Protocolo de fatura
		oBrowse:SetMenuDef('JURA141')
	Else
		oBrowse:SetDescription( STR0007 ) //"Inclus�o de WO - Fatura"
	EndIf
	oBrowse:SetAlias( "NXA" )
	oBrowse:SetLocate()
	oBrowse:SetFilterDefault( cFiltro )
	oBrowse:SetFieldMark( 'NXA_OK' )
	oBrowse:bAllMark := { ||  JurMarkALL(oBrowse, "NXA", 'NXA_OK', lMarcar := !lMarcar ), oBrowse:Refresh() }
	JurSetLeg( oBrowse, "NXA" )
	JurSetBSize( oBrowse )
	oBrowse:Activate()
EndIf

Return NIL

//-------------------------------------------------------------------
/*/{Protheus.doc} J141Filter
Filtra para ignorar faturas com pagamentos

@author  Jonatas Martins
@since   25/11/2019
/*/
//-------------------------------------------------------------------
Static Function J141Filter(lJura074)
	Local cPrefixo := ""
	Local cTipo    := ""
	Local cFilter  := ""

	If lJura074 // Cadastro de Protocolos 
		cFilter := "NXA_TIPO == 'FT' .And. NXA_SITUAC == '1'"
	Else
		cPrefixo := PadR(SuperGetMV("MV_JPREFAT", , "PFS"), TamSX3("E1_PREFIXO")[1])
		cTipo    := PadR(SuperGetMV("MV_JTIPFAT", , "FT "), TamSX3("E1_TIPO")[1])
	
		cFilter := "@ NXA_TIPO = 'FT' AND NXA_SITUAC = '1'"
		cFilter += " AND NOT EXISTS ( SELECT 1"
		cFilter +=                    " FROM " + RetSqlName("SE5") + " SE5"
		cFilter +=                   " WHERE SE5.E5_FILIAL = '" + xFilial("SE5") + "'"
		cFilter +=                     " AND SE5.E5_PREFIXO = '" + cPrefixo + "'"
		cFilter +=                     " AND SE5.E5_NUMERO = NXA_COD"
		cFilter +=                     " AND SE5.E5_TIPO = '" + cTipo + "'"
		cFilter +=                     " AND SE5.E5_CLIFOR = NXA_CLIPG"
		cFilter +=                     " AND SE5.E5_LOJA = NXA_LOJPG"
		cFilter +=                     " AND SE5.E5_TIPODOC <> 'CP'"
		cFilter +=                     " AND SE5.E5_DTCANBX = '        '"
		cFilter +=                     " AND SE5.D_E_L_E_T_ = ' '"
		cFilter +=                     " AND NOT EXISTS(SELECT 1 
		cFilter +=                                      " FROM " + RetSqlName("SE5") + " B"
		cFilter +=                                     " WHERE B.E5_FILIAL = '" + xFilial("SE5") + "'" 
		cFilter +=                                       " AND B.R_E_C_N_O_ = SE5.R_E_C_N_O_"
		cFilter +=                                       " AND B.E5_TIPODOC = 'ES'"
		cFilter +=                                       " AND B.D_E_L_E_T_ = ' '))"
	EndIf

Return (cFilter)

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menu Funcional

@return aRotina - Estrutura
[n,1] Nome a aparecer no cabecalho
[n,2] Nome da Rotina associada
[n,3] Reservado
[n,4] Tipo de Transa��o a ser efetuada:
1 - Pesquisa e Posiciona em um Banco de Dados
2 - Simplesmente Mostra os Campos
3 - Inclui registros no Bancos de Dados
4 - Altera o registro corrente
5 - Remove o registro corrente do Banco de Dados
6 - Altera��o sem inclus�o de registros
7 - C�pia
8 - Imprimir
[n,5] Nivel de acesso
[n,6] Habilita Menu Funcional

@author Felipe Bonvicini Conti
@since 17/06/09
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
	Local aRotina := {}

	aAdd( aRotina, { STR0001, "PesqBrw", 0, 1, 0, .T. } ) // "Pesquisar"
	
	If IsInCallStack( 'JURA074' )
		aAdd( aRotina, { STR0015, "JA074SET(oBrowse)", 0, 6, 0, NIL } ) // "Gerar" //Protocolo de fatura - JURA074	
		aAdd( aRotina, { STR0002, "VIEWDEF.JURA204", 0, 2, 0, NIL } ) // "Visualizar"
	Else
		aAdd( aRotina, { STR0002, "VIEWDEF.JURA141", 0, 2, 0, NIL } ) // "Visualizar"
		aAdd( aRotina, { "WO", "JA141SET(oBrowse)", 0, 6, 0, NIL } ) // "WO"
	EndIf

Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
View de dados de Fatura dos Profissionais

@author Felipe Bonvicini Conti
@since 17/06/09
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
Local oView
Local oModel     := FWLoadModel( "JURA141" )
Local oStructNXA := FWFormStruct( 2, "NXA" )

	JurSetAgrp( 'NXA',, oStructNXA )
	
	oView := FWFormView():New()
	oView:SetModel( oModel )
	oView:AddField( "JURA141_NXA", oStructNXA, "NXAMASTER"  )
	oView:CreateHorizontalBox( "NXAFIELDS", 100 )
	oView:SetOwnerView( "JURA141_NXA", "NXAFIELDS" )
	
	oView:SetDescription( STR0007 ) // "Fatura dos Profissionais"
	oView:EnableControlBar( .T. )

Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados de Fatura dos Profissionais

@author Felipe Bonvicini Conti
@since 17/06/09
@version 1.0

@obs NXAMASTER - Dados do Fatura dos Profissionais
/*/
//-------------------------------------------------------------------
Static Function Modeldef()
Local oModel     := NIL
Local oStructNXA := FWFormStruct( 1, "NXA" )

	oModel:= MPFormModel():New( "JURA141", /*Pre-Validacao*/, /*Pos-Validacao*/, /*Commit*/,/*Cancel*/)
	oModel:AddFields( "NXAMASTER", NIL, oStructNXA, /*Pre-Validacao*/, /*Pos-Validacao*/ )
	oModel:SetDescription( STR0008 ) // "Modelo de Dados de Fatura dos Profissionais"
	oModel:GetModel( "NXAMASTER" ):SetDescription( STR0009 ) // "Dados de Fatura dos Profissionais"
	JurSetRules( oModel, "NXAMASTER",, "NXA",,  )

Return oModel

//-------------------------------------------------------------------
/*/{Protheus.doc} JA141SET
Envia os Lan�amentos para WO: Cria um registro na Tabela de WO,
vincula os lan�amentos ao n�mero do WO e
atualiza o valor dos lan�amentos na tabela WO Caso

@param 	cTipo  	Tipo da altera��o a ser executada nos time-Sheets

@author David Gon�alves Fernandes
@since 07/12/09
@version 1.0
/*/
//-------------------------------------------------------------------
Function JA141SET(oBrowse)
	Local lRet       := .T.
	Local cMarca     := oBrowse:Mark()
	Local aArea      := GetArea()
	Local aAreaNXA   := NXA->( GetArea() )
	Local cFiltro    := "(NXA_OK == '" + cMarca + "')"
	Local cMsg       := ''
	Local aOBS       := {}
	Local aRetNXA    := {}
	
	If (lRet := JURA203G( 'FT', Date(), 'FATCAN' )[2]) //Testa se h� per�odo de fechamento, caso contr�rio, a fatura n�o poder� ser cancelada e nem ser realizado o WO
		aOBS := JurMotWO('NUF_OBSEMI', STR0007, STR0014 )
		If !Empty(aOBS)
			FWMsgRun(, {|| aRetNXA := JAWOFATURA(cFiltro, aOBS)}, STR0007, STR0018) // Inclus�o de WO - Faturas # Processando, aguarde..."
			If aRetNXA[1] > 0
				cMsg := Alltrim(Str(aRetNXA[1])) + " " + STR0012 + CRLF // "Fatura Enviada para WO "
				cMsg += STR0013 + Str(aRetNXA[2]) // "N� de Lan�amentos: "
			EndIf
		EndIf
	EndIf
		
	If !Empty(cMsg)
		ApMsgInfo( cMsg )
	EndIf
	
	RestArea( aAreaNXA )
	RestArea( aArea )

Return lRet
