#Include "Protheus.Ch"
#Include "JURA203F.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} JURA203
Rotina emiss�o de Faturas

@Params		*cTipo		Tipo de Emiss�o: 1 - Fatura / 2 - Minuta de Fatura / 3 - Minuta de Pr�-fatura
			  -- mudar para TpExec .. 
			cFila		C�digo da fila de emiss�o de Faturas

@Return		aRet		Indica o resultado da opera��o 	[1]: .T. - Conclu�do com �xito / .F. - Falha na execu��o
														[2]: Nome da rotina que originou o erro

@author David G. Fernandes
@since 22/02/10
@version 1.0
/*/	
//-------------------------------------------------------------------
Function JA203FEmi(oParams, cCPREFT, cCFATAD, cCFIXO, cCJCONT, cCCONTR, cCCLIEN, cCLOJA)
	Local aRet      := {.F., "JA203FEmi"} 
	Local aArea     := GetArea()
	Local aAreaNX0  := NX0->(GetArea())
	Local cCodCaso  := ""
	
	Local cEscr     := ""	
	Local cMoedaPF  := ""
	Local cRelat    := ""
	Local cTEMTS    := ""
	Local cTEMLT    := ""
	Local cTEMDP    := ""
	Local cTEMFX    := ""
	Local cTEMFA    := ""
	Local cQuery    := ""
	Local cQryRes   := GetNextAlias()

	Local cDIniH    := ""
	Local cDFinH    := ""
	Local cDIniD    := ""
	Local cDFinD    := ""
	Local cFATSUB   := ""
	Local cESCSUB   := ""

	//Verificar Escrit�rio e Filial de emiss�o (se houver jun��o � da jun��o)
	If !Empty(cCPREFT)
		
		aRet := J203FCaMin(cCPREFT)  //Verifica se existe e cancela as Minutas ativas da pr�-fatura antes de gerar a Fatura 
		
		If aRet[1]
		
			NX0->( dbSetOrder(1) )
			If NX0->(DbSeek( xFilial("NX0") + cCPREFT))
				cEscr    := NX0->NX0_CESCR
				cMoedaPF := NX0->NX0_CMOEDA
				cTEMFA   := NX0->NX0_FATADC
				cFATSUB  := NX0->NX0_FATOLD
				cESCSUB  := NX0->NX0_ESCOLD
				cCFATAD  := NX0->NX0_CFTADC
		
				oParams:SetFatSub( cFATSUB )
				oParams:SetEscSub( cESCSUB )
				oParams:SetDIniH( IIF(Empty(NX0->NX0_DINITS), NX0->NX0_DINIFX, NX0->NX0_DINITS) )
				oParams:SetDFinH( IIF(Empty(NX0->NX0_DFIMTS), NX0->NX0_DFIMFX, NX0->NX0_DFIMTS) )
				oParams:SetDIniD( NX0->NX0_DINIDP )
				oParams:SetDFinD( NX0->NX0_DFIMDP )
				oParams:SetDIniT( NX0->NX0_DINITB )
				oParams:SetDFinT( NX0->NX0_DFIMTB )

				//	Traz os casos dos contrados da Pr�-Fatura
				cQuery := " SELECT NX1_CCLIEN, NX1_CLOJA, NX1_CJCONT, NX1_CCONTR, NX1_CCASO,  " + CRLF
				cQuery +=  		   " NX1_TS, NX1_DESP, NX1_LANTAB, NX8_FIXO " + CRLF
				cQuery +=   " FROM " +RetSqlName( 'NX1' )+ " NX1 " + CRLF
				cQuery +=         " INNER JOIN " +RetSqlName( 'NX8' )+ " NX8 ON ( NX8.NX8_FILIAL = '"+ xFilial("NX8") +"' AND "+CRLF
				cQuery +=                                                       " NX8.NX8_CPREFT = NX1.NX1_CPREFT AND " + CRLF
				cQuery +=                                                       " NX8.NX8_CCONTR = NX1.NX1_CCONTR AND " + CRLF
				cQuery +=                                                       " NX8.D_E_L_E_T_ = ' ' ) " + CRLF
				cQuery += " WHERE	NX1.NX1_FILIAL = '"+ xFilial("NX1") +"' "+CRLF
				cQuery += "   AND NX1.NX1_CPREFT = '"+ cCPREFT +"' "+CRLF
				cQuery += "   AND NX1.D_E_L_E_T_ = ' ' " + CRLF
				
				cQuery := ChangeQuery(cQuery, .F.)
				dbUseArea( .T., 'TOPCONN', TcGenQry( ,, cQuery ), cQryRes, .T., .F. )
				
				While !(cQryRes)->(EOF())
					cCJCONT  := (cQryRes)->NX1_CJCONT
					cCCONTR  := (cQryRes)->NX1_CCONTR
					
					cCCLIEN  := (cQryRes)->NX1_CCLIEN
					cCLOJA   := (cQryRes)->NX1_CLOJA
					cCodCaso := (cQryRes)->NX1_CCASO

					cTEMTS   := (cQryRes)->NX1_TS
					cTEMLT   := (cQryRes)->NX1_LANTAB
					cTEMDP   := (cQryRes)->NX1_DESP
					cTEMFX   := (cQryRes)->NX8_FIXO
					
					// Apaga as confer�ncias ao emitir a Fatura				
					JA201JApag(oParams, cCFATAD, cCJCONT, cCCONTR, cCCLIEN, cCLOJA, cCodCaso, cTEMTS, cTEMLT, cTEMDP, cTEMFX, cTEMFA, '2', cCPREFT, STR0002 ) //# Cancelamento por emiss�o de fatura
						
					//Vincula os lan�amentos � Fatura (de pr�)
					If cTEMFA == "2" 
						aRet    := JA201BVinc(oParams, cCFIXO, cCPREFT, cCFATAD, cCJCONT, cCCONTR, cCCLIEN, cCLOJA, cCodCaso, cTEMTS, cTEMLT, cTEMDP, cTEMFX, cTEMFA  ) 
					EndIf
					
					(cQryRes)->(DbSkip())
				EndDo
				(cQryRes)->(dbCloseArea())
			
			EndIf

			oParams:SetContrato(cCCONTR)

		EndIf

		If !Empty(cCJCONT)
			cRelat	:= JurGetDados("NW2", 1 , xFilial("NW2") + cCJCONT, "NW2_CRELAT")
		Else
			cRelat	:= JurGetDados("NT0", 1 , xFilial("NT0") + cCCONTR, "NT0_CRELAT")
		EndIf

	EndIf
 
	If !Empty(cCFATAD)
		
		aRet := J203FCaMin(Nil, cCFATAD)  //Verifica se existe e cancela as Minutas ativas antes de gerar a Fatura 
		
		If aRet[1]
		
			cEscr     := JurGetDados("NVV", 1 , xFilial("NVV") + cCFATAD, "NVV_CESCR")
			cMoedaPF  := JurGetDados("NVV", 1 , xFilial("NVV") + cCFATAD, "NVV_CMOE3")	
		
			//	Traz os casos dos contrados da Fatura Adicional
			cQuery := " SELECT	NVV.NVV_COD, NVV.NVV_CCONTR, NVW.NVW_CCLIEN, NVW.NVW_CLOJA, NVW.NVW_CCASO, " + CRLF
			cQuery += " 		NVV.NVV_TRALT, NVV.NVV_TRATS, NVV.NVV_DTINIH, NVV.NVV_DTFIMH, NVV.NVV_TRADSP, " + CRLF
			cQuery += " 		NVV.NVV_DTINID, NVV.NVV_DTFIMD, NVV.NVV_DSPCAS, NVV.NVV_DTINIT, NVV.NVV_DTFIMT " + CRLF
			cQuery += " FROM	" +RetSqlName( 'NVV' )+ " NVV, " +RetSqlName( 'NVW' )+ " NVW " + CRLF
			cQuery += " WHERE	NVV.NVV_FILIAL	= '"+ xFilial("NVV") +"'  "+CRLF
			cQuery += " AND		NVW.NVW_FILIAL	= '"+ xFilial("NVW") +"'  "+CRLF
			cQuery += " AND		NVV.NVV_COD		= NVW.NVW_CODFAD " + CRLF
			cQuery += " AND		NVV.NVV_COD		= '"+ cCFATAD +"'  "+CRLF
	        If !Empty(!Empty(cCPREFT))
				cQuery += " AND		NVV.NVV_CPREFT	= '"+ cCPREFT +"'  "+CRLF
			EndIf
			cQuery += " AND		NVV.D_E_L_E_T_ = ' '  "+CRLF
			cQuery += " AND		NVW.D_E_L_E_T_ = ' '  "+CRLF
	
			cQuery := ChangeQuery(cQuery, .F.)
			dbUseArea( .T., 'TOPCONN', TcGenQry( ,, cQuery ), cQryRes, .T., .F. )
			
			While !(cQryRes)->(EOF())
	
				cCodCaso := (cQryRes)->NVW_CCASO
				cCCLIEN  := (cQryRes)->NVW_CCLIEN
				cCLOJA   := (cQryRes)->NVW_CLOJA
				cTEMTS   := (cQryRes)->NVV_TRATS
				cTEMLT   := (cQryRes)->NVV_TRALT
				cCCONTR  := (cQryRes)->NVV_CCONTR
	
				If (cQryRes)->NVV_TRADSP == "1" .OR. (cQryRes)->NVV_DSPCAS == "1"
					cTEMDP := "1"
				Else
					cTEMDP := "2"
				EndIf
				cTEMFX := "2"
				cTEMFA := "1"
	
				cDIniH := (cQryRes)->NVV_DTINIH
				cDFinH := (cQryRes)->NVV_DTFIMH
				cDIniT := (cQryRes)->NVV_DTINIT
				cDFinT := (cQryRes)->NVV_DTFIMT
				cDIniD := (cQryRes)->NVV_DTINID
				cDFinD := (cQryRes)->NVV_DTFIMD 
		
				oParams:SetDIniH( sTod(cDIniH) )
				oParams:SetDFinH( sTod(cDFinH) )
				oParams:SetDIniT( sTod(cDIniT) )
				oParams:SetDFinT( sTod(cDFinT) )
				oParams:SetDIniD( sTod(cDIniD) )
				oParams:SetDFinD( sTod(cDFinD) )
	
				// Vincula os lan�amendos � Fatura (Casos da Fatura Adicinal)
				aRet := JA201BVinc(oParams, cCFIXO, cCPREFT, cCFATAD, cCJCONT, cCCONTR, cCCLIEN, cCLOJA, cCodCaso, cTEMTS, cTEMLT, cTEMDP, cTEMFX, cTEMFA  )
	
				(cQryRes)->(DbSkip())
			EndDo
			(cQryRes)->(dbCloseArea())
			oParams:SetContrato(cCCONTR)
		EndIf
	EndIf
	
	If !Empty(cCFIXO)
		
		aRet := J203FCaMin(Nil, Nil, cCFIXO) //Verifica se existe e cancela as Minutas ativas antes de gerar a Fatura 
		
		If aRet[1]
	
			cEscr      := JurGetDados("NT0", 1, xFilial("NT0") + cCCONTR, "NT0_CESCR")
			cMoedaPF   := JurGetDados("NT0", 1, xFilial("NT0") + cCCONTR, "NT0_CMOE")
			cRelat     := JurGetDados("NT0", 1, xFilial("NT0") + cCCONTR, "NT0_CRELAT")
	
			// Traz os casos dos contrados da Fixos
			cQuery := " SELECT	NT1.NT1_SEQUEN, NT1.NT1_CCONTR, NUT.NUT_CCLIEN, NUT.NUT_CLOJA, NUT.NUT_CCASO, " + CRLF
			cQuery += " 		NT1.NT1_DATAIN, NT1.NT1_DATAFI  " + CRLF
			cQuery += " FROM	" +RetSqlName( 'NT1' )+ " NT1, " +RetSqlName( 'NT0' )+ " NT0, " +RetSqlName( 'NUT' )+ " NUT " + CRLF
			cQuery += " WHERE	NT1.NT1_FILIAL	= '"+ xFilial("NT1") +"'  "+CRLF
			cQuery += " AND		NT0.NT0_FILIAL	= '"+ xFilial("NT0") +"'  "+CRLF
			cQuery += " AND		NUT.NUT_FILIAL	= '"+ xFilial("NUT") +"'  "+CRLF
			cQuery += " AND		NT1.NT1_CCONTR	= NT0.NT0_COD " + CRLF
			cQuery += " AND		NT1.NT1_CCONTR	= NUT.NUT_CCONTR " + CRLF
			cQuery += " AND		NT1.NT1_SEQUEN	= '"+ cCFIXO +"'  "+CRLF
			cQuery += " AND		NT1.D_E_L_E_T_ = ' ' " + CRLF
			cQuery += " AND		NT0.D_E_L_E_T_ = ' ' " + CRLF
			cQuery += " AND		NUT.D_E_L_E_T_ = ' ' " + CRLF
	
			cQuery := ChangeQuery(cQuery, .F.)
			dbUseArea( .T., 'TOPCONN', TcGenQry( ,, cQuery ), cQryRes, .T., .F. )
			
			While !(cQryRes)->(EOF())

				cTEMTS   := "2"   //N�o tem TS � Faturas de Fixo - verifica na rotina de v�nculo.
				cTEMLT   := "2"
				cTEMDP   := "2"
				cTEMFA   := "2"
				cTEMFX   := "1"
				
				cCCLIEN  := (cQryRes)->NUT_CCLIEN
				cCLOJA   := (cQryRes)->NUT_CLOJA
				cCodCaso := (cQryRes)->NUT_CCASO
				cCCONTR  := (cQryRes)->NT1_CCONTR
				
				cDIniH   := (cQryRes)->NT1_DATAIN
				cDFinH   := (cQryRes)->NT1_DATAFI
				
				oParams:SetDIniH( sTod(cDIniH) )
				oParams:SetDFinH( sTod(cDFinH) )
		
				// Vincula os lan�amendos dos casos
				aRet := JA201BVinc(oParams, cCFIXO, cCPREFT, cCFATAD, cCJCONT, cCCONTR, cCCLIEN, cCLOJA, cCodCaso, cTEMTS, cTEMLT, cTEMDP, cTEMFX, cTEMFA  ) 
				
				(cQryRes)->(DbSkip())
			EndDo
			(cQryRes)->(dbCloseArea())
	
			oParams:SetContrato(cCCONTR)
		EndIf
	EndIf

	oParams:SetResomaFt(.F., .T.) // Limpa a vari�vel a cada emiss�o para garantir o correto preenchimento em caso de emiss�es em lote.

	If aRet[1]
		//Calcula o Total dos casos
		aRet := JA201DCaso(oParams, cCPREFT, cMoedaPF, cCFATAD, cCJCONT, cCCONTR, cCFIXO) 
	EndIf    
	//Calcula o Total dos Contratos
	If aRet[1]
		aRet := JA201ECont(oParams, cCPREFT, cMoedaPF, cCFATAD, cCJCONT, cCCONTR)
	EndIf    
	//Grava a Fatura e ajusta a numera��o
	If aRet[1]
		aRet := JA203HFatu(oParams, cCPREFT, cMoedaPF, cCFATAD, cCJCONT, cCCONTR, cCFIXO)
	EndIf
	
	RestArea(aArea)
	RestArea(aAreaNX0)

Return (aRet)

//-------------------------------------------------------------------
/*/{Protheus.doc} J203FCaMin()
Rotina que verifica e cancela as minutas no momento da emiss�o de fatura.

@Param   cCodPre  - pr�-fatura a ser analisada
@Param   cFatAdic - fatura adicional a ser analisada
@Param   cFixo    - fixo a ser analisado

@Return  lRet     - .F. := N�o coseguiu efetuar o cancelamento     

@author Luciano Pereira dos Santos 
@since 08/08/2013
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function J203FCaMin(cCodPre, cFatAdic, cFixo)
Local aRet       := {.T., "J203FCaMin", ""} 
Local aArea      := GetArea()
Local aAreaNXA   := NXA->(GetArea())
Local aCancFat   := {}
Local nI         := 0
Local cMotivo    := ""
Local cMsgErro   := ""
Local cSolucao   := ""
Local cLanc      := ""

Default cCodPre  := ""
Default cFatAdic := ""
Default cFixo    := ""

cQuery := " SELECT NXA.R_E_C_N_O_ NXA_RECNO "
cQuery +=   " FROM " + RetSqlname('NXA') + " NXA "
cQuery +=     " WHERE  NXA.NXA_FILIAL  = '" + xFilial("NXA") + "' "
If !Empty(cCodPre)
	cQuery += " AND NXA.NXA_CPREFT = '" + cCodPre + "' "
	cLanc := STR0003  // "pr�-fatura"
ElseIf !Empty(cFatAdic)
	cQuery += " AND NXA.NXA_CFTADC = '" + cFatAdic + "' "
	cLanc := STR0005  // "fatura adicional"
ElseIf !Empty(cFixo)
	cQuery += " AND NXA.NXA_CFIXO = '" + cFixo + "' "
	cLanc := STR0004  // "parcela fixa"
EndIf
cQuery +=     " AND NXA.NXA_SITUAC = '1' "
cQuery +=     " AND NXA.NXA_TIPO IN ('MP','MF','MS') "
cQuery +=     " AND NXA.D_E_L_E_T_ = ' ' "

aCancFat := JurSQL(cQuery, {'NXA_RECNO'})

For nI := 1 To Len(aCancFat)
	
	NXA->(DbGoto(aCancFat[nI][1]))

	If !JA204CanFa(cMotivo, @cMsgErro, @cSolucao)
		aRet[1] := .F.
		aRet[2] := I18N(STR0001, {cCodPre, cLanc}) + CRLF  + cMsgErro //"J203FCaMin - Problema no cancelamento da(s) minuta(s) da '#2' '#1': "
		aRet[3] := cSolucao
		Exit
	EndIf 
		
Next nI

RestArea( aArea )
RestArea( aAreaNXA )

Return aRet
