#INCLUDE "JURA232.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH"
#INCLUDE "FWMVCDEF.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} JURA232
Contatos (SU5) para integra��o com o LegalDesk.

@author Cristina Cintra
@since 22/02/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function JURA232()
Local oBrowse

oBrowse := FWMBrowse():New()
oBrowse:SetDescription( STR0001 ) //"Contatos - Integra��o LegalDesk"
oBrowse:SetAlias( "SU5" )
oBrowse:SetLocate()
JurSetBSize( oBrowse )
oBrowse:Activate()

Return NIL

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menu Funcional

@return aRotina - Estrutura
[n,1] Nome a aparecer no cabecalho
[n,2] Nome da Rotina associada
[n,3] Reservado
[n,4] Tipo de Transa��o a ser efetuada:
1 - Pesquisa e Posiciona em um Banco de Dados
2 - Simplesmente Mostra os Campos
3 - Inclui registros no Bancos de Dados
4 - Altera o registro corrente
5 - Remove o registro corrente do Banco de Dados
6 - Altera��o sem inclus�o de registros
7 - C�pia
8 - Imprimir
[n,5] Nivel de acesso
[n,6] Habilita Menu Funcional

@author Cristina Cintra
@since 22/02/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
Local aRotina := {}

aAdd( aRotina, { STR0002, "PesqBrw"        , 0, 1, 0, .T. } ) // "Pesquisar"
aAdd( aRotina, { STR0003, "VIEWDEF.JURA232", 0, 8, 0, NIL } ) // "Imprimir"

Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados dos Contatos para integra��o com o LegalDesk.

@author Cristina Cintra
@since 22/02/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function Modeldef()
Local oModel     := NIL
Local oStructSU5 := FWFormStruct(1, "SU5")
Local oStructAGA := FWFormStruct(1, "AGA")
Local oStructAGB := FWFormStruct(1, "AGB")

oModel:= MPFormModel():New( "JURA232", /*Pre-Validacao*/, /*Pos-Validacao*/, /*Commit*/,/*Cancel*/)
oModel:AddFields("SU5MASTER", /*cOwner*/, oStructSU5,/*Pre-Validacao*/,/*Pos-Validacao*/,/*Load*/) 
oModel:AddGrid( "AGADETAIL", "SU5MASTER", oStructAGA,/*bLinePre*/,/*bLinePost*/,,/*bPosVal*/,/*bLoad*/)
oModel:AddGrid( "AGBDETAIL", "SU5MASTER", oStructAGB,/*bLinePre*/,/*bLinePost*/,,/*bPosVal*/,/*bLoad*/)


oModel:GetModel( "AGADETAIL" ):SetDelAllLine(.T.)
oModel:GetModel( "AGADETAIL" ):SetUniqueLine( { "AGA_CODIGO","AGA_ENTIDA","AGA_CODENT" } )
oModel:SetRelation( "AGADETAIL", { { "AGA_FILIAL", "U5_FILIAL" }, ;
                                   { "AGA_ENTIDA", "Upper('SU5')" }, ;
                                   { "AGA_CODENT", "U5_CODCONT" }}, AGA->( IndexKey( 1 ) ) )

oModel:GetModel( "AGBDETAIL" ):SetDelAllLine(.T.)
oModel:GetModel( "AGBDETAIL" ):SetUniqueLine( { "AGB_CODIGO","AGB_ENTIDA","AGB_CODENT" } )
oModel:SetRelation( "AGBDETAIL", { { "AGB_FILIAL", "U5_FILIAL" }, ;
                                   { "AGB_ENTIDA", "Upper('SU5')" }, ;
								   { "AGB_CODENT", "U5_CODCONT" }}, AGB->( IndexKey( 1 ) ) )
																 
oModel:GetModel("SU5MASTER"):SetDescription( STR0001 ) //"Contatos - Integra��o LegalDesk"
oModel:GetModel("AGADETAIL"):SetDescription( STR0004 ) //"Endere�os do Contato - Integra��o LegalDesk"
oModel:GetModel("AGBDETAIL"):SetDescription( STR0005 ) //"Telefones do Contato - Integra��o LegalDesk"

oModel:SetOptional("AGADETAIL", .T.)
oModel:SetOptional("AGBDETAIL", .T.)

Return oModel