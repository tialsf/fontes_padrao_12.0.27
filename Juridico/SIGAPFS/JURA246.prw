#INCLUDE "JURA246.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH"
#INCLUDE "FWMVCDEF.CH"

Static _aRecDesCtb := {} // Variavel para controlar lan�amentos estornados por altera��es

#DEFINE ICO_TEM_ANEXO "F5_VERD_OCEAN.BMP"

//-------------------------------------------------------------------
/*/{Protheus.doc} JURA246
Itens de desdobramento

@param  nOperacao , Opera��o para abrir o modelo.
@param  lForceAtu , For�a abertura da rotina de desdobramento.
@param  lConfirma , Indica que a chamada foi feita na confirma��o do t�tulo
@param  lTransit  , Se executa a rotina de desdobramento completa
@param  lBrowse   , Se foi chamada a execu��o pelo Browse
@param  lAfterSave, Se foi chamada ap�s salvar um t�tulo (inclus�o ou altera��o)

@author Luciano Pereira dos Santos
@since 03/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function JURA246(nOperacao, lForceAtu, lConfirma, lTransit, lBrowse, lAfterSave)
	Local cNaturSE2    := IIf(lBrowse, SE2->E2_NATUREZ, M->E2_NATUREZ)
	Local lRet         := .T.
	Local lDesdOld     := SuperGetMV("MV_NRASDSD", .F.) // Permite que o desdobramento de t�tulos seja realizado no processo antigo, sem rastreamento e excluindo o t�tulo originador.
	Local lTitPriPrc   := Iif(lDesdOld, SE2->E2_DESDOBR == "S" .And. SE2->(Deleted()), SE2->E2_STATUS == "D") // Se o t�tulo est� sendo desdobrado pelo financeiro
	Local lExecAF050   := Type("lF050Auto") == "U" .Or. ( Type("lF050Auto") == "L" .And. !lF050Auto ) // Quando for ExecAuto n�o devem ser validados os desdobramentos

	Default nOperacao  := MODEL_OPERATION_UPDATE
	Default lForceAtu  := .F.
	Default lConfirma  := .F.
	Default lTransit   := JurGetDados("SED", 1, xFilial("SED") + cNaturSE2, "ED_CCJURI") == "7" // Transit�ria de Pagamento
	Default lBrowse    := .F.
	Default lAfterSave := .F.

	If !JurIsRest() .And. !IsBlind() .And. lExecAF050

		lRet := JVldTipoCp(SE2->E2_TIPO, .T.)

		If lRet .And. !lAfterSave .And. lTitPriPrc
			lRet := .F.
			JurMsgErro(STR0059,,; // "A situa��o do t�tulo n�o permite inserir desdobramentos."
			      I18n(STR0060, {AllTrim(FwX3Titulo('E2_DESDOBR'))}), .F.) // "Ap�s parcelamento do t�tulo ('#1' - E2_DESDOBR), s� � poss�vel desdobrar as parcelas geradas."
		EndIf

		If lRet
			If lBrowse .Or. lForceAtu .Or. M->E2_NATUREZ == SE2->E2_NATUREZ
				If lTransit
					FWExecView( STR0003, 'JURA246', nOperacao, , { || .T. }, , , ) // "Itens de desdobramento"
				Else
					lRet := J246DIALOG(lConfirma, nOperacao)
					If lRet .And. ExistBlock("J246Comp") // Pondo de entrada ap�s confirmar a tela de complemento do t�tulo
						ExecBlock("J246Comp", .F., .F.)
					EndIf
				EndIf
			Else
				ApMsgAlert(STR0028) // "Houve altera��o da Natureza. Confirme a altera��o para preencher os detalhes/desdobramentos do t�tulo."
			EndIf
		EndIf
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados de Itens de desdobramento

@author Luciano Pereira dos Santos
@since 03/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function Modeldef()
Local oModel     := NIL
Local oStructSE2 := FWFormStruct( 1, "SE2" )
Local oStructOHF := FWFormStruct( 1, "OHF" )
Local oEvent     := JA246Event():New()
Local cChave     := SE2->E2_FILIAL+'|'+SE2->E2_PREFIXO+'|'+SE2->E2_NUM+'|'+SE2->E2_PARCELA+'|'+SE2->E2_TIPO+'|'+SE2->E2_FORNECE+'|'+SE2->E2_LOJA
Local cIdDoc     := FINGRVFK7('SE2', cChave)
Local lUtProj    := SuperGetMv( "MV_JUTPROJ", .F., .F., ) // Indica se ser� utilizado Projeto/Finalidade nas rotinas do Financeiro (.T. = Sim; .F. = N�o)
Local lContOrc   := SuperGetMv( "MV_JCONORC", .F., .F., ) // Indica se ser� utilizado Controle Or�ament�rio (.T. = Sim; .F. = N�o)

oStructSE2 := J246AddCpM(oStructSE2)

// Adiciona o campo de anexo no Model
oStructOHF := J247CpoAnx(oStructOHF, "OHF", "M", {||JA246Anexo()})

oModel:= MPFormModel():New( "JURA246", /*Pre-Validacao*/, /*Pos-Validacao*/, /*Commit*/,/*Cancel*/)
oModel:AddFields( "SE2MASTER", NIL         /*cOwner*/, oStructSE2, /*Pre-Validacao*/, /*Pos-Validacao*/ )
oModel:AddGrid("OHFDETAIL",�"SE2MASTER"/*cOwner*/,�oStructOHF,�{|oGrid, nLine, cAction, cField, xNewValue, xOldValue| J246PreOHF(oModel,�nLine,�cAction, cField, xNewValue, xOldValue)}�/*Pre-Validacao*/,�{|| J246PosOHF(oModel)} /*Pos-Validacao*/,�/*bPre*/,�/*bPost*/�)

oModel:SetRelation("OHFDETAIL", {{"OHF_FILIAL", "E2_FILIAL" }, {"OHF_IDDOC", '"'+cIdDoc+'"'}}, OHF->(IndexKey(1)))

oModel:GetModel( "SE2MASTER" ):SetDescription( STR0004 ) // "T�tulo"
oModel:GetModel( "OHFDETAIL" ):SetDescription( STR0003 ) // "Itens de desdobramento"

J235MAnexo(@oModel, "OHFDETAIL", "OHF", "FWFldGet('OHF_IDDOC') + FWFldGet('OHF_CITEM')") // Grid de Anexos

oStructSE2:SetProperty("E2_PREFIXO", MODEL_FIELD_WHEN, {||.F.})
oStructSE2:SetProperty("E2_NUM"    , MODEL_FIELD_WHEN, {||.F.})
oStructSE2:SetProperty("E2_PARCELA", MODEL_FIELD_WHEN, {||.F.})
oStructSE2:SetProperty("E2_TIPO"   , MODEL_FIELD_WHEN, {||.F.})
oStructSE2:SetProperty("E2__NATURE", MODEL_FIELD_WHEN, {||.F.})
oStructSE2:SetProperty("E2__VENCTO", MODEL_FIELD_WHEN, {||.F.})
oStructSE2:SetProperty("E2__VENCRE", MODEL_FIELD_WHEN, {||.F.})
oStructSE2:SetProperty("E2__CMOEDA", MODEL_FIELD_WHEN, {||.F.})
oStructSE2:SetProperty("E2__VALOR" , MODEL_FIELD_WHEN, {||.F.})
oStructSE2:SetProperty("E2__VLRLIQ", MODEL_FIELD_WHEN, {||.F.})
If !lUtProj .And. !lContOrc .And. OHF->(ColumnPos("OHF_CPROJE")) > 0
	oStructOHF:SetProperty("OHF_CPROJE", MODEL_FIELD_WHEN, {||.F.})
	oStructOHF:SetProperty("OHF_CITPRJ", MODEL_FIELD_WHEN, {||.F.})
EndIf

oModel:GetModel("OHFDETAIL"):SetUniqueLine( {"OHF_CITEM"} )

oModel:SetOptional( "OHFDETAIL", .T. )
oModel:GetModel( "OHFDETAIL" ):SetDelAllLine( .T. )

oModel:InstallEvent("JA246Event", /*cOwner*/, oEvent)

oModel:SetActivate( {|oModel|�JIniValDes(oModel,�"OHF"), J246ActWhn(@oStructOHF)} ) // Preenche os valores dos campos de total e saldo do desdobramento ao abrir a tela, e ajusta when
oModel:SetVldActivate( { |oModel| J246VldACT( oModel ) } )

Return oModel

//-------------------------------------------------------------------
/*/{Protheus.doc} J246ActWhn
Ajuste o WHEN do modelo JURA246 que n�o s�o alterados ap�s a ativa��o do modelo

@param  oStructOHF, estrutura do modelo para ser alterado o when
@return Nil

@author  Bruno Ritter
@since   07/11/2019
/*/
//-------------------------------------------------------------------
Function J246ActWhn(oStructOHF)
	Local lDesdOld   := SuperGetMV("MV_NRASDSD", .F.) // Permite que o desdobramento de t�tulos seja realizado no processo antigo, sem rastreamento e excluindo o t�tulo originador.
	Local lDesdFin   := Iif(lDesdOld, SE2->E2_DESDOBR == "S" .And. SE2->(Deleted()), SE2->E2_STATUS == "D") // Se o t�tulo est� sendo desdobrado pelo financeiro
	Local lExisteBx  := SE2->E2_SALDO != SE2->E2_VALOR
	Local lFldNoUpd  := lExisteBx .And. !lDesdFin

	oStructOHF:SetProperty("OHF_CNATUR", MODEL_FIELD_NOUPD, lFldNoUpd )
	oStructOHF:SetProperty("OHF_SALDO" , MODEL_FIELD_NOUPD, lFldNoUpd )
	oStructOHF:SetProperty("OHF_VALOR" , MODEL_FIELD_NOUPD, lFldNoUpd )

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
View de dados de Itens de desdobramento

@author Luciano Pereira dos Santos
@since 03/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
Local oView      := Nil
Local cAddCpo    := "E2_PREFIXO|E2_NUM|E2_PARCELA|E2_TIPO|E2__NATURE|E2__DNATUR|E2__VENCTO|E2__VENCRE|E2__CMOEDA|E2__DMOEDA|E2__VALOR|E2__VLRLIQ|E2__TOTDES|E2__SLDDES"
Local aOrdemCpo  := STRTOKARR(cAddCpo, "|")
Local oModel     := FWLoadModel( "JURA246" )
Local oStructSE2 := FWFormStruct( 2, "SE2", {|cCampo| J246SE2Cpo(cCampo, cAddCpo)})
Local oStructOHF := FWFormStruct( 2, "OHF" )
Local cLojaAuto  := SuperGetMv( "MV_JLOJAUT", .F., "2", ) // Indica se a Loja do Caso deve ser preenchida automaticamente. (1-Sim; 2-N�o)
Local lUtProj    := SuperGetMv( "MV_JUTPROJ", .F., .F., ) // Indica se ser� utilizado Projeto/Finalidade nas rotinas do Financeiro (.T. = Sim; .F. = N�o)
Local lContOrc   := SuperGetMv( "MV_JCONORC", .F., .F., ) // Indica se ser� utilizado Controle Or�ament�rio (.T. = Sim; .F. = N�o)

oStructSE2 := J246AddCpV(oStructSE2)
oStructSE2 := J246SE2Ord(oStructSE2, aOrdemCpo)

// Adiciona o campo de anexo no View
oStructOHF := J247CpoAnx(oStructOHF, "OHF", "V")

oStructOHF:RemoveField("OHF_IDDOC")
oStructOHF:RemoveField("OHF_CPART")
oStructOHF:RemoveField("OHF_CPART2")
oStructOHF:RemoveField("OHF_DTINCL")

If (cLojaAuto == "1") // Loja Autom�tica
	oStructOHF:RemoveField("OHF_CLOJA")
EndIf

If !lUtProj .And. !lContOrc .And. OHF->(ColumnPos("OHF_CPROJE")) > 0
	oStructOHF:RemoveField("OHF_CPROJE")
	oStructOHF:RemoveField("OHF_DPROJE")
	oStructOHF:RemoveField("OHF_CITPRJ")
	oStructOHF:RemoveField("OHF_DITPRJ")
EndIf

// Remove campo da data de contabiliza��o off-line
If OHF->(ColumnPos("OHF_DTCONT")) > 0
	oStructOHF:RemoveField("OHF_DTCONT")
EndIf

// Remove campo da data de contabiliza��o off-line para inclus�o de desdobramento
If OHF->(ColumnPos("OHF_DTCONI")) > 0
	oStructOHF:RemoveField("OHF_DTCONI")
EndIf

If OHF->(FieldPos("OHF_CODLD")) > 0
	oStructOHF:RemoveField('OHF_CODLD')
EndIf

If AllTrim(SE2->E2_ORIGEM) == "JURCTORC" // Integra��o Controle Or�ament�rio SIGAPFS x SIGAFIN
	oStructOHF:SetProperty("OHF_VALOR" , MVC_VIEW_CANCHANGE, .F.)
	oStructOHF:SetProperty("OHF_CESCR" , MVC_VIEW_CANCHANGE, .F.)
	oStructOHF:SetProperty("OHF_CCUSTO", MVC_VIEW_CANCHANGE, .F.)
	oStructOHF:SetProperty("OHF_SIGLA2", MVC_VIEW_CANCHANGE, .F.)
	oStructOHF:SetProperty("OHF_CRATEI", MVC_VIEW_CANCHANGE, .F.)
	If OHF->(ColumnPos("OHF_CPROJE")) > 0
		oStructOHF:SetProperty("OHF_CPROJE", MVC_VIEW_CANCHANGE, .F.)
		oStructOHF:SetProperty("OHF_CITPRJ", MVC_VIEW_CANCHANGE, .F.)
	EndIf
EndIf

oView := FWFormView():New()
oView:SetModel(oModel)
oView:AddField("JURA246_SE2", oStructSE2, "SE2MASTER")
oView:AddGrid("JURA246_OHF" , oStructOHF, "OHFDETAIL")

oView:SetViewProperty( 'JURA246_OHF', "ENABLEDGRIDDETAIL", { 50 } )

oView:CreateHorizontalBox("FORMFIELD", 30)
oView:CreateHorizontalBox("FORMGRID",  70)

oView:SetOwnerView("JURA246_SE2", "FORMFIELD")
oView:SetOwnerView("JURA246_OHF", "FORMGRID")

oView:EnableTitleView("JURA246_OHF")

oView:EnableControlBar( .T. )
oView:AddIncrementField( 'OHFDETAIL', 'OHF_CITEM' )

If AllTrim(SE2->E2_ORIGEM) == "JURCTORC" // Integra��o Controle Or�ament�rio SIGAPFS x SIGAFIN
	oView:SetNoInsertLine("OHFDETAIL")
	oView:SetNoDeleteLine("OHFDETAIL")
EndIf

oView:SetViewProperty("JURA246_OHF", "GRIDDOUBLECLICK", {{|oFormulario, cFieldName, nLineGrid, nLineModel| Iif(Alltrim(cFieldName) == "OHF__ANEXO", JA246Anexo(), .T.) }})

If !IsBlind()
	oView:AddUserButton( STR0046, "CLIPS" , { | oView | JA246Anexo() } ) // "Anexos"
	oView:AddUserButton( STR0047, "BUDGET", { | oView | JA247Legen() } ) // "Legenda"
EndIf

Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} J246SE2Cpo(cCampo)
Fun��o para selecionar os campos do Model da tabela SE2

@param cCampo campo da estrutura.

@return .T. para campos que ope

@author Luciano Pereira dos Santos
@since 03/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function J246SE2Cpo(cCampo, cAddCpo)
Local lRet     := .F.
Local cNomeCpo := AllTrim(cCampo)

If cNomeCpo $ cAddCpo
	lRet := .T.
EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J246AddCpM(oStruct)
Inclui campos no model atrav�s da fun��o AddField

@param oStruct Estrutura a ser adicionadas os campos

@author Luciano Pereira dos Santos
@since 03/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function J246AddCpM(oStruct)
	Local aNat       := TamSx3('ED_DESCRIC')
	Local aCMoe      := TamSx3('CTO_MOEDA')
	Local aDMoe      := TamSx3('CTO_SIMB')
	Local aVal       := TamSx3('E2_VALOR')
	Local aCodNat    := TamSx3('E2_NATUREZ')
	Local aVencto    := TamSx3('E2_VENCTO')

	Local cTitNat    := GetSx3Cache( 'E2_NATUREZ', 'X3_TITULO' )
	Local cDesNat    := GetSx3Cache( 'E2_NATUREZ', 'X3_DESCRIC')
	Local cTitVencto := GetSx3Cache( 'E2_VENCTO' , 'X3_TITULO' )
	Local cDesVencto := GetSx3Cache( 'E2_VENCTO' , 'X3_DESCRIC')
	Local cTitVencRe := GetSx3Cache( 'E2_VENCREA', 'X3_TITULO' )
	Local cDesVencRe := GetSx3Cache( 'E2_VENCREA', 'X3_DESCRIC')
	Local cTitValor  := GetSx3Cache( 'E2_VALOR'  , 'X3_TITULO' )
	Local cDesValor  := GetSx3Cache( 'E2_VALOR'  , 'X3_DESCRIC')

	                //Titulo   , Descricao , Campo       , Tipo do campo , Tamanho    , Decimal    ,  bValid,  bWhen   , Lista , lObrigat,  bInicializador                          , � chave, � edit�vel , � virtual
	oStruct:AddField(STR0005   , STR0006   , 'E2__VLRLIQ', aVal[3]       , aVal[1]    , aVal[2]    ,        ,  {||.F.} ,       ,         , {|| J246InitP('E2__VLRLIQ')} ,        ,            , .T.       ) // 'Vlr. L�quido' - 'Valor l�quido'
	oStruct:AddField(STR0007   , STR0008   , 'E2__DNATUR', aNat[3]       , aNat[1]    , aNat[2]    ,        ,  {||.F.} ,       ,         , {|| J246InitP('E2__DNATUR')} ,        ,            , .T.       ) // 'Desc. Natureza' - 'Descri��o Natureza'
	oStruct:AddField(STR0009   , STR0009   , 'E2__TOTDES', aVal[3]       , aVal[1]    , aVal[2]    ,        ,  {||.T.} ,       ,         , {|| J246InitP('E2__TOTDES')} ,        ,            , .T.       ) // 'Total Desdobramento'
	oStruct:AddField(STR0010   , STR0010   , 'E2__SLDDES', aVal[3]       , aVal[1]    , aVal[2]    ,        ,  {||.T.} ,       ,         , {|| J246InitP('E2__SLDDES')} ,        ,            , .T.       ) // 'Saldo Desdobramento'
	oStruct:AddField(STR0011   , STR0012   , 'E2__CMOEDA', aCMoe[3]      , aCMoe[1]   , aCMoe[2]   ,        ,  {||.F.} ,       ,         , {|| J246InitP('E2__CMOEDA')} ,        ,            , .T.       ) // 'C�d. Moeda' - 'C�digo da Moeda'
	oStruct:AddField(STR0013   , STR0014   , 'E2__DMOEDA', aDMoe[3]      , aDMoe[1]   , aDMoe[2]   ,        ,  {||.F.} ,       ,         , {|| J246InitP('E2__DMOEDA')} ,        ,            , .T.       ) // 'S�mb. Moeda' - 'S�mbolo da Moeda'
	oStruct:AddField(cTitNat   , cDesNat   , 'E2__NATURE', aCodNat[3]    , aCodNat[1] , aCodNat[2] ,        ,  {||.F.} ,       ,         , {|| J246InitP('E2__NATURE')} ,        ,            , .T.       ) // 'Natureza'
	oStruct:AddField(cTitVencto, cDesVencto, 'E2__VENCTO', aVencto[3]    , aVencto[1] , aVencto[2] ,        ,  {||.F.} ,       ,         , {|| J246InitP('E2__VENCTO')} ,        ,            , .T.       ) // 'Vencimento'
	oStruct:AddField(cTitVencRe, cDesVencRe, 'E2__VENCRE', aVencto[3]    , aVencto[1] , aVencto[2] ,        ,  {||.F.} ,       ,         , {|| J246InitP('E2__VENCRE')} ,        ,            , .T.       ) // 'Vencimento Real'
	oStruct:AddField(cTitValor , cDesValor , 'E2__VALOR' , aVal[3]       , aVal[1]    , aVal[2]    ,        ,  {||.F.} ,       ,         , {|| J246InitP('E2__VALOR' )} ,        ,            , .T.       ) // 'Valor T�tulo'

Return oStruct

//-------------------------------------------------------------------
/*/{Protheus.doc} J246AddCpV(oStruct, nTipo)
Inclui campos no view atrav�s da fun��o AddField

@Param oStruct Estrutura a ser adicionadas os campos

@author Luciano Pereira dos Santos
@since 03/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function J246AddCpV(oStruct)
Local cPict := Alltrim(X3Picture('E2_VALOR'))
Local aLgpd := {}

Local cTitNat    := GetSx3Cache( 'E2_NATUREZ', 'X3_TITULO' )
Local cDesNat    := GetSx3Cache( 'E2_NATUREZ', 'X3_DESCRIC')
Local cTitVencto := GetSx3Cache( 'E2_VENCTO' , 'X3_TITULO' )
Local cDesVencto := GetSx3Cache( 'E2_VENCTO' , 'X3_DESCRIC')
Local cTitVencRe := GetSx3Cache( 'E2_VENCREA', 'X3_TITULO' )
Local cDesVencRe := GetSx3Cache( 'E2_VENCREA', 'X3_DESCRIC')
Local cTitValor  := GetSx3Cache( 'E2_VALOR'  , 'X3_TITULO' )
Local cDesValor  := GetSx3Cache( 'E2_VALOR'  , 'X3_DESCRIC')

                 //Campo     , Ordem, Titulo    , Descricao , Help , Tipo do campo, Picture, PictVar,   F3,  When, Folder, Group, Lista Combo, Tam Max Combo, Inic. Browse, Virtual
oStruct:AddField('E2__VLRLIQ', 'ZZ' , STR0005   , STR0006   , {}   , 'GET'        ,cPict   ,        ,     ,   .T., '1'   ,      , {}         ,              ,             , .T.    ) // 'Vlr. L�quido' - 'Valor l�quido'
oStruct:AddField('E2__DNATUR', 'ZZ' , STR0007   , STR0008   , {}   , 'GET'        ,'!@'    ,        ,     ,   .T., '1'   ,      , {}         ,              ,             , .T.    ) // 'Desc. Natureza' - 'Descri��o Natureza'
oStruct:AddField('E2__TOTDES', 'ZZ' , STR0009   , STR0009   , {}   , 'GET'        ,cPict   ,        ,     ,   .F., '1'   ,      , {}         ,              ,             , .T.    ) // 'Total Desdobramento'
oStruct:AddField('E2__SLDDES', 'ZZ' , STR0010   , STR0010   , {}   , 'GET'        ,cPict   ,        ,     ,   .F., '1'   ,      , {}         ,              ,             , .T.    ) // 'Saldo Desdobramento'
oStruct:AddField('E2__CMOEDA', 'ZZ' , STR0011   , STR0012   , {}   , 'GET'        ,'!@'    ,        ,     ,   .T., '1'   ,      , {}         ,              ,             , .T.    ) // 'C�d. Moeda' - 'C�digo da Moeda'
oStruct:AddField('E2__DMOEDA', 'ZZ' , STR0013   , STR0014   , {}   , 'GET'        ,'!@'    ,        ,     ,   .T., '1'   ,      , {}         ,              ,             , .T.    ) // 'S�mb. Moeda' - 'S�mbolo da Moeda'
oStruct:AddField('E2__NATURE', 'ZZ' , cTitNat   , cDesNat   , {}   , 'GET'        ,'!@'    ,        ,     ,   .F., '1'   ,      , {}         ,              ,             , .T.    ) // 'Natureza'
oStruct:AddField('E2__VENCTO', 'ZZ' , cTitVencto, cDesVencto, {}   , 'GET'        ,'!@'    ,        ,     ,   .F., '1'   ,      , {}         ,              ,             , .T.    ) // 'Vencimento'
oStruct:AddField('E2__VENCRE', 'ZZ' , cTitVencRe, cDesVencRe, {}   , 'GET'        ,'!@'    ,        ,     ,   .F., '1'   ,      , {}         ,              ,             , .T.    ) // 'Vencimento Real'
oStruct:AddField('E2__VALOR' , 'ZZ' , cTitValor , cDesValor , {}   , 'GET'        ,cPict   ,        ,     ,   .F., '1'   ,      , {}         ,              ,             , .T.    ) // 'Valor T�tulo'

aAdd(aLgpd, {"E2__VLRLIQ", "E2_VALOR"  })
aAdd(aLgpd, {"E2__DNATUR", "OHF_DNATUR"})
aAdd(aLgpd, {"E2__TOTDES", "OHF_VALOR" })
aAdd(aLgpd, {"E2__SLDDES", "OHF_VALOR" })
aAdd(aLgpd, {"E2__CMOEDA", "E2_MOEDA"  })
aAdd(aLgpd, {"E2__DMOEDA", "CTO_SIMB"  })
aAdd(aLgpd, {"E2__NATURE", "E2_NATUREZ"})
aAdd(aLgpd, {"E2__VENCTO", "E2_VENCTO" })
aAdd(aLgpd, {"E2__VENCRE", "E2_VENCREA"})
aAdd(aLgpd, {"E2__VALOR" , "E2_VALOR"  })

If FindFunction("JPDOfusca")
	JPDOfusca(@oStruct, aLgpd)
EndIf

Return oStruct

//-------------------------------------------------------------------
/*/{Protheus.doc} J246SE2Ord(oStruct, aOrdemCpo)
Ajusta a ordem dos campos na view da SE2.

@Param oStruct     Estrutura da SE2
@Param aOrdemCpo  Array com os campos ordenados

@author Luciano Pereira dos Santos
@since 03/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function J246SE2Ord(oStruct, aOrdemCpo)
Local nI := 1

For nI := 1 to Len(aOrdemCpo)

	oStruct:SetProperty(aOrdemCpo[nI], MVC_VIEW_ORDEM, RetAsc(Str(nI), 2, .T.) )

Next nI

Return oStruct

//-------------------------------------------------------------------
/*/{Protheus.doc} J246InitP
Inicializador padr�o dos campos virtuais da SE2

@param cCampo  Nome do campo que ter� o inicializador atribu�do

@author Luciano Pereira dos Santos
@since 04/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function J246InitP(cCampo)
	Local xRet   := Nil

	Do Case
		Case cCampo == 'E2__DNATUR'
			xRet := POSICIONE("SED", 1, XFILIAL("SED") + SE2->E2_NATUREZ, 'ED_DESCRIC ')

		Case cCampo == 'E2__CMOEDA'
			xRet := PADL(SE2->E2_MOEDA, TamSx3('CTO_MOEDA')[1], '0')

		Case cCampo == 'E2__DMOEDA'
			xRet := POSICIONE('CTO', 1, xFilial('CTO') + PADL(SE2->E2_MOEDA, TamSx3('CTO_MOEDA')[1], '0'), 'CTO_SIMB')

		Case cCampo == 'E2__TOTDES'
			xRet := 0

		Case cCampo == 'E2__SLDDES'
			xRet := 0

		Case cCampo == 'E2__NATURE'
			xRet := SE2->E2_NATUREZ

		Case cCampo == 'E2__VALOR'
			xRet := JCPVlBruto(SE2->(Recno()))

		Case cCampo == 'E2__VENCRE'
			xRet := SE2->E2_VENCTO

		Case cCampo == 'E2__VENCTO'
			xRet := SE2->E2_VENCTO

		Case cCampo == 'E2__VLRLIQ'
			xRet := JCPVlLiqui(SE2->(Recno()))
	EndCase

Return xRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J246OpDesp(oModel, nOperDesp)
Valida e prepara a despesa para inclus�o, altera��o ou exclus�o.

@param oModel    => Modelo ativo
@param nOperDesp => Operacao para a Despesa (1=INSERT;2=UPDATE;3=DELETE)

@return oModelNVY Retorna o modelo preparado da NVY para

@author bruno.ritter
@since 04/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function J246OpDesp(oModel)
	Local aModelDesp := {}
	Local oModelSE2  := oModel:GetModel("SE2MASTER")
	Local oModelOHF  := oModel:GetModel("OHFDETAIL")
	Local cCobraOld  := ""
	Local lOk        := .T.
	Local nLine      := 1
	Local nQtdOHF    := oModelOHF:GetQTDLine()
	Local nOperDesp  := 0
	Local nUltimoDp  := 0
	Local cChave     := SE2->E2_FILIAL+'|'+SE2->E2_PREFIXO+'|'+SE2->E2_NUM+'|'+SE2->E2_PARCELA+'|'+SE2->E2_TIPO+'|'+SE2->E2_FORNECE+'|'+SE2->E2_LOJA

	For nLine := 1 To nQtdOHF
		nOperDesp := J246AcDesp(oModel, nLine) // Verifica se � necess�rio gerar um INSERT/UPDATE/DELETE de Despesa

		If nOperDesp != 0 //N�o � necess�rio atualizar despesa.
			If nOperDesp == MODEL_OPERATION_UPDATE
				cCobraOld := JurGetDados('OHF', 1, xFilial("OHF") + oModelOHF:GetValue('OHF_IDDOC', nLine) + oModelOHF:GetValue('OHF_CITEM', nLine), 'OHF_COBRA')
			Else
				cCobraOld := ""
			EndIf

			aAdd (aModelDesp, JA049GerDp(nOperDesp,;
								oModelOHF:GetValue("OHF_CDESP"  , nLine),;
								oModelOHF:GetValue("OHF_CCLIEN" , nLine),;
								oModelOHF:GetValue("OHF_CLOJA"  , nLine),;
								oModelOHF:GetValue("OHF_CCASO"  , nLine),;
								oModelOHF:GetValue("OHF_DTDESP" , nLine),;
								oModelOHF:GetValue("OHF_SIGLA"  , nLine),;
								oModelOHF:GetValue("OHF_CTPDSP" , nLine),;
								oModelOHF:GetValue("OHF_QTDDSP" , nLine),;
								oModelOHF:GetValue("OHF_COBRA"  , nLine),;
								oModelOHF:GetValue("OHF_HISTOR" , nLine),;
								oModelSE2:GetValue("E2__CMOEDA"),;
								oModelOHF:GetValue("OHF_VALOR"  , nLine),;
								cCobraOld,;
								,;
								cChave,;
								oModelOHF:GetValue("OHF_CITEM", nLine) ;
								))

			nUltimoDp := Len(aModelDesp)
			If Empty(aModelDesp[nUltimoDp])
				lOk        := .F.
				aModelDesp := {}
				Exit
			ElseIf nOperDesp == MODEL_OPERATION_INSERT
				oModelOHF:GoLine(nLine)
				oModelOHF:SetValue("OHF_CDESP", aModelDesp[nUltimoDp]:GetValue("NVYMASTER", "NVY_COD"))
			ElseIf nOperDesp == MODEL_OPERATION_DELETE
				oModelOHF:GoLine(nLine)
				oModelOHF:SetValue("OHF_CDESP", "")
			EndIf
		EndIf
	Next nLine

Return {lOk, aModelDesp}

//-------------------------------------------------------------------
/*/{Protheus.doc} J246AcDesp(oModel)
Verifica se � necess�rio gerar um INSERT/UPDATE/DELETE de Despesa e retorna qual opera��o ser� executada

@param oModel     => Modelo ativo

@return nOperDesp => A opera��o que � necess�rio para atualizar a Despesa vinculada, retorna 0 quando n�o existe atualiza��o para ser realizada.

@author bruno.ritter
@since 04/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function J246AcDesp(oModel, nLine)
Local nOperDesp  := 0
Local oModelOHF  := oModel:GetModel("OHFDETAIL")
Local lNatDspNew := JurGetDados('SED', 1, xFilial('SED') + oModelOHF:GetValue("OHF_CNATUR", nLine), 'ED_CCJURI') == "5"
Local cNatOld    := JurGetDados('OHF', 1, xFilial('OHF') + oModelOHF:GetValue("OHF_IDDOC", nLine) + oModelOHF:GetValue("OHF_CITEM", nLine), 'OHF_CNATUR')
Local lNatDspOld := JurGetDados('SED', 1, xFilial('SED') + cNatOld, 'ED_CCJURI') == "5"

	If !oModelOHF:IsUpdated(nLine);
	   .Or. oModelOHF:IsFieldUpdated("OHF_CPART" , nLine);
	   .Or. oModelOHF:IsFieldUpdated("OHF_SIGLA" , nLine);
	   .Or. oModelOHF:IsFieldUpdated("OHF_CCLIEN", nLine);
	   .Or. oModelOHF:IsFieldUpdated("OHF_CLOJA" , nLine);
	   .Or. oModelOHF:IsFieldUpdated("OHF_CCASO" , nLine);
	   .Or. oModelOHF:IsFieldUpdated("OHF_CTPDSP", nLine);
	   .Or. oModelOHF:IsFieldUpdated("OHF_QTDDSP", nLine);
	   .Or. oModelOHF:IsFieldUpdated("OHF_DTDESP", nLine);
	   .Or. oModelOHF:IsFieldUpdated("OHF_VALOR" , nLine);
	   .Or. oModelOHF:IsFieldUpdated("OHF_COBRA" , nLine);
	   .Or. oModelOHF:IsFieldUpdated("OHF_HISTOR", nLine)

		If oModelOHF:IsInserted(nLine)
			Iif(lNatDspNew, nOperDesp := MODEL_OPERATION_INSERT, )

		ElseIf oModelOHF:IsDeleted(nLine)
			If lNatDspNew .And. lNatDspOld
				nOperDesp := MODEL_OPERATION_DELETE
			EndIf

		ElseIf oModelOHF:IsUpdated(nLine)
			If lNatDspNew .And. lNatDspOld //Se o lan�amento era e continua sendo com despesa
				nOperDesp := MODEL_OPERATION_UPDATE

			ElseIf lNatDspNew //Se o lan�amento N�O era de Despesa e agora � de Despesa
				nOperDesp := MODEL_OPERATION_INSERT

			ElseIf lNatDspOld //Se o lan�amento era de Despesa e agora N�O � mais de Despesa
				nOperDesp := MODEL_OPERATION_DELETE

			EndIf
		EndIf
	EndIf

Return nOperDesp

//-------------------------------------------------------------------
/*/{Protheus.doc} J246CMMdls
Efetua o commit nos modelos.

@param aModels, Array com os Modelos para comitar
@param cTable , Tabela principal dos modelos (aModels)
@param cIdModel, Id do modelo principal (cTable)

@author bruno.ritter
@since 04/10/2017
/*/
//-------------------------------------------------------------------
Static Function J246CMMdls(aModels, cTable, cIdModel)
	Local nRecLine  := 0
	Local nQtdMdls  := Len(aModels)
	Local nMdl      := 1
	Local oModel    := Nil

	ProcRegua(nQtdMdls)

	For nMdl := 1 To nQtdMdls
		oModel   := aModels[nMdl]:GetModel(cIdModel)
		nRecLine := oModel:GetDataID()
		(cTable)->(DbGoTo(nRecLine))
		aModels[nMdl]:CommitData()
		aModels[nMdl]:DeActivate()
		aModels[nMdl]:Destroy()
		IncProc()
	Next nMdl

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} JA246WHEN
When dos campos da OHF - Desdobramento financeiro

1 - Escrit�rio
2 - Escrit�rio e Centro de Custos
3 - Profissional
4 - Tabela de Rateio
5 - Despesa Cliente
6 - Transit�ria de Pagamentos

@author bruno.ritter
@since 07/08/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function JA246WHEN()
Local lRet     := .T.
Local cCampo   := Alltrim(StrTran(ReadVar(), 'M->', ''))
Local cModelo  := "OHFDETAIL"
Local cNatur   := "OHF_CNATUR"
Local cEscrit  := "OHF_CESCR"
Local cCusto   := "OHF_CCUSTO"
Local cSigla   := "OHF_SIGLA2"
Local cRateio  := "OHF_CRATEI"
Local cClient  := "OHF_CCLIEN"
Local cLoja    := "OHF_CLOJA"
Local cCaso    := "OHF_CCASO"

// Grupo Natureza
If cCampo $ 'OHF_CESCR'
	lRet := JurWhNatCC("1", cModelo, cNatur, cEscrit, cCusto, cSigla, cRateio)

ElseIf cCampo $ 'OHF_CCUSTO'
	lRet := JurWhNatCC("2", cModelo, cNatur, cEscrit, cCusto, cSigla, cRateio)

ElseIf cCampo $ 'OHF_SIGLA2|OHF_CPART2'
	lRet := JurWhNatCC("3", cModelo, cNatur, cEscrit, cCusto, cSigla, cRateio)

ElseIf cCampo $ 'OHF_CRATEI'
	lRet := JurWhNatCC("4", cModelo, cNatur, cEscrit, cCusto, cSigla, cRateio)

// Grupo Despesa
ElseIf cCampo $ 'OHF_CCLIEN|OHF_CLOJA|OHF_QTDDSP|OHF_COBRA|OHF_DTDESP|OHF_CTPDSP'
	lRet := JurWhNatCC("5", cModelo, cNatur, , , , , cClient, cLoja, cCaso)

ElseIf cCampo $ 'OHF_CCASO'
	lRet := JurWhNatCC("6", cModelo, cNatur, , , , , cClient, cLoja, cCaso)

EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J246IniCBD()
Fun��o do gatilho das naturezas para preencher o valor padr�o "cobrar despesa?".

@return cOpcao => Op��o do campo cobrar despesa

@author bruno.ritter/ricardo.neves
@since 12/09/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function J246IniCBD(cCampo)
	Local cOpcao := ''

	Default cCampo := 'OHF_CNATUR'

	If JurGetDados('SED', 1, xFilial('SED') + FwFldGet(cCampo), 'ED_CCJURI') == '5'
		cOpcao := '1'
	EndIf

Return cOpcao

//-------------------------------------------------------------------
/*/{Protheus.doc} J246VldEscr(cEscrit)
Valida��o do campo de Escrit�rio

@param cEscrit  C�digo do escrit�rio

@author Luciano Pereira dos Santos
@since  05/10/2017
@obs    Fun��o chamada no X3_VALID dos campos OHF_CESCR e OHV_CESCR
/*/
//-------------------------------------------------------------------
Function J246VldEscr(cEscrit, cCampo)
	Local lRet   := .T.

	Default cCampo := "OHF_CESCR"

	If cCampo == "OHF_CESCR"
		lRet := ExistCpo('NS7', cEscrit, 1) .And. JAVLDCAMPO('OHFDETAIL', 'OHF_CESCR', 'NS7', 'NS7_ATIVO', '1')
	ElseIf cCampo == "OHV_CESCR"
		lRet := ExistCpo('NS7', cEscrit, 1) .And. JAVLDCAMPO('OHVDETAIL', 'OHV_CESCR', 'NS7', 'NS7_ATIVO', '1')
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JA246DESC
Retorna a descri��o do caso. Chamado pelo inicializador padr�o dos campos

@param  - cCampo    Nome do campo para busca dos dados de Cliente e Loja

@return - cRet      Descri��o/Assunto do Caso

@author bruno.ritter
@since  05/10/2017
@obs    Fun��o chamada no X3_RELACAO dos campos OHF_DCASO e OHV_DCASO
/*/
//-------------------------------------------------------------------
Function JA246DESC(cCampo)
	Local cRet     := ""
	
	Default cCampo := ""

	If !Empty(cCampo)
		If cCampo == 'OHF_DCASO'
			cRet := Posicione('NVE', 1, xFilial('NVE') + OHF->OHF_CCLIEN + OHF->OHF_CLOJA + OHF->OHF_CCASO, 'NVE_TITULO')
		ElseIf cCampo == 'OHV_DCASO'
			cRet := Posicione('NVE', 1, xFilial('NVE') + OHV->OHV_CCLIEN + OHV->OHV_CLOJA + OHV->OHV_CCASO, 'NVE_TITULO')
		EndIf
	EndIf

Return (cRet)

//-------------------------------------------------------------------
/*/{Protheus.doc} J246ClxCa()
Rotina para verificar se o cliente/loja pertece ao caso.
Utilizado para condi��o de gatilho

@Return - lRet  .T. quando o cliente PERTENCE ao caso informado OU
                .F. quando o cliente N�O pertence ao caso informado

@author bruno.ritter
@since 07/08/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function J246ClxCa()
Local lRet      := .F.
Local oModel    := FWModelActive()
Local cTab      := IIF(oModel:GetID() == "JURA246", "OHF", "OHV")
Local cIdGrid   := cTab + "DETAIL"
Local cClien    := ""
Local cLoja     := ""
Local cCaso     := ""

cClien := oModel:GetValue(cIdGrid, cTab + "_CCLIEN")
cLoja  := oModel:GetValue(cIdGrid, cTab + "_CLOJA")
cCaso  := oModel:GetValue(cIdGrid, cTab + "_CCASO")

lRet := JurClxCa(cClien, cLoja, cCaso)

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J246VldHis
Valida��o do historico padr�o

@param cHist  C�digo do hit�rico padr�o

@author Luciano Pereira dos Santos
@since  05/10/2017
@obs    Fun��o chamada X3_VALID dos campos OHF_CHISTP e OHV_CHISTP
/*/
//-------------------------------------------------------------------
Function J246VldHis(cHist, cCampo)
	Local lRet := .T.

	Default cHist  := ""
	Default cCampo := "OHF_CHISTP"

	If cCampo == "OHF_CHISTP"
		lRet := ExistCpo('OHA', cHist, 1) .And. JAVLDCAMPO('OHFDETAIL', 'OHF_CHISTP', 'OHA', 'OHA_CTAPAG', '1')
	ElseIf cCampo == "OHV_CHISTP"
		lRet := ExistCpo('OHA', cHist, 1) .And. JAVLDCAMPO('OHVDETAIL', 'OHV_CHISTP', 'OHA', 'OHA_CTAPAG', '1')
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J246PosOHF
P�s valida��o do grid OHF

Centro de Custo Jur�dico (cCCNatur || cCCNatDest)
1 - Escrit�rio
2 - Centro de Custos
3 - Profissional
4 - Tabela de Rateio
5 - Despesa Cliente
6 - Transit�ria de Pagamentos

@author bruno.ritter
@since 06/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function J246PosOHF(oModel)
Local lRet      := .T.
Local lIsRest   := (Iif(FindFunction("JurIsRest"), JurIsRest(), .F.))

	lRet := JurVldNCC(oModel, "OHFDETAIL", "OHF_CNATUR", "OHF_CESCR", "OHF_CCUSTO", "OHF_CPART2", "OHF_SIGLA2", "OHF_CRATEI", "OHF_CCLIEN", "OHF_CLOJA", ;
                       "OHF_CCASO", "OHF_CTPDSP", "OHF_QTDDSP", "OHF_COBRA ", "OHF_DTDESP", "OHF_CPART", "OHF_SIGLA", "OHF_CPROJE", "OHF_CITPRJ" )

	If lRet .And. oModel:GetModel("OHFDETAIL"):IsInserted() .And. lIsRest .And. OHF->(FieldPos( "OHF_CODLD" )) > 0 .And. FindFunction("JurMsgCdLD") .And. JVldTipoCp(SE2->E2_TIPO,.F.)
		lRet := JurMsgCdLD(oModel:GetValue("OHFDETAIL", "OHF_CODLD"))
	EndIf
	
	If lRet .And. Empty(oModel:GetValue("OHFDETAIL", "OHF_CHISTP")) .And. SuperGetMv("MV_JHISPAD", .F., .F.)
		lRet := .F.
		JurMsgErro(STR0054,, STR0055) // "� obrigat�rio o preenchimento do Hist�rico Padr�o, conforme o par�metro MV_JHISPAD." # "Informe um c�digo v�lido para o Hist�rico Padr�o."
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J246VldCli()
Validacao cliente/loja igual os parametros: MV_JURTS5 e MV_JURTS6 ou
MV_JURTS9 e MV_JURTS10

@Param oModel  Modelo de dados

@author Jorge Luis Branco Martins Junior
@since 06/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function J246VldCli(oModel)
Local lRet       := .T.
Local oModelOHF  := oModel:GetModel("OHFDETAIL")
Local nLine      := 1
Local nQtdOHF    := oModelOHF:GetQtdLine()

If (oModel:GetOperation() == MODEL_OPERATION_INSERT .Or. oModel:GetOperation() == MODEL_OPERATION_UPDATE)
	For nLine := 1 To nQtdOHF
		If !oModelOHF:IsDeleted(nLine)
			lRet := JurCliLVld(oModel, oModelOHF:GetValue('OHF_CCLIEN', nLine), oModelOHF:GetValue('OHF_CLOJA', nLine))
			If !lRet
				Exit
			EndIf
		EndIf
	Next nLine
EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J246VldACT(oModel)
Fun��o de valida��o da ativa��o do modelo.

@author bruno.ritter
@since 07/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function J246VldACT(oModel)
Local lRet     := .T.
Local nOper    := oModel:GetOperation()
Local aArea    := GetArea()

 	If nOper == MODEL_OPERATION_UPDATE .Or. nOper == MODEL_OPERATION_INSERT .Or. nOper == MODEL_OPERATION_DELETE
		If ! FWIsInCallStack("J246DelOHF") .And. Empty(JURUSUARIO(__CUSERID))
			lRet := .F.
			ApMsgAlert(STR0019 + CRLF + STR0020) // "N�o ser� poss�vel manipular os desdobramentos do Contas Pagar, pois o usu�rio n�o est� vinculado a um participante." "Associe seu usu�rio a um participante para ter acesso a opera��o.
		EndIf
	EndIf

RestArea(aArea)

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J246PreOHF()
Fun��o de pr� valida��o do modelo OHF

@author bruno.ritter
@since 11/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Function J246PreOHF(oModel,�nLine, cAction, cField, xNewValue, xOldValue)
Local lRet        := .T.
Local lIsRest     := (IIF(FindFunction("JurIsRest"), JurIsRest(), .F.))
Local lOrigJ235A  := FwIsInCallStack("J235ACancela")
Local lOrigJ281   := FwIsInCallStack("MATA103")
Local oModelOHF   := oModel:GetModel("OHFDETAIL")
Local cIdDoc      := ""
Local cNatur      := ""
Local cCCNaturez  := ""
Local cCCNatNew   := ""
Local cCCNatOld   := ""
Local cBxTPosPag  := ""
Local cDtContabil := IIF(OHF->(ColumnPos("OHF_DTCONT")) > 0, DtoS(oModelOHF:GetValue("OHF_DTCONT", nLine)), "")
Local cCodAprDes  := IIF(OHF->(ColumnPos("OHF_NZQCOD")) > 0, oModelOHF:GetValue("OHF_NZQCOD", nLine), "")
Local cAnexo      := oModelOHF:GetValue("OHF__ANEXO", nLine)

// N�o permite altera��o de registros contabilizados
If !Empty(cDtContabil) .And. cAction $ "CANSETVALUE|DELETE"
	lRet := .F.
	If cAction == "DELETE"
		JurMsgErro(STR0044, , STR0045, .F.) // "N�o � poss�vel alterar/excluir o registro!" # "Desdobramento j� contabilizado."
	EndIf
EndIf

// Verifica se o desdobramento � originado de uma aprova��o de despesa
If lRet .And. !IsBlind() .And. !Empty(cCodAprDes) .And. cAction $ "DELETE" .And. !lOrigJ235A .And. !lOrigJ281
	lRet := ApMsgYesNo(STR0050) // "Esse desdobramento tem como origem a aprova��o de uma solicita��o de despesa. Deseja realmente excluir o desdobramento e reprovar a solicita��o de despesa?"
	If !lRet
		JurMsgErro(STR0051, , STR0052, .F.) // "Opera��o cancelada." # "Desdobramento n�o removido."
	EndIf
EndIf

//Se o campo OHF_CNATUR foi alterado � porque inicialmente ele n�o era do tipo "6"
If lRet .And. "CANSETVALUE" != cAction .And. !oModelOHF:IsFieldUpdated("OHF_CNATUR",�nLine)
	cNatur     := oModelOHF:GetValue("OHF_CNATUR")
	cCCNaturez := JurGetDados("SED", 1, xFilial("SED") + cNatur, "ED_CCJURI")

	If cCCNaturez == "6" //Transit�ria p�s pagamento
		cIdDoc  := oModelOHF:GetValue("OHF_IDDOC")
		OHG->(DbSetOrder(1))
		If OHG->(DbSeek(xFilial("OHG")+cIdDoc))
			lRet       := .F.
			cBxTPosPag := JurInfBox("ED_CCJURI", "6" )
			JurMsgErro(i18n(STR0023, {cBxTPosPag});//"Quando j� existe um desdobramento p�s pagamento lan�ado no t�tulo, n�o � poss�vel alterar um desdobranto cujo a natureza � do tipo '6 - #1'."
			,, STR0024) //"Verifique as o desdobramento p�s pagamento lan�ado para esse t�tulo."
		EndIf
	EndIf
EndIf

If lRet .And. !lIsRest ;                                   // Execu��o via REST integra��o com LegalDesk
        .And. "CANSETVALUE" != cAction ;                   // Altera��o de Valor
        .And. cField == "OHF_CNATUR" ;                     // Campo de natureza
        .And. xNewValue != xOldValue                       // Valor novo diferente do valor antigo

	cCCNatNew := JurGetDados("SED", 1, xFilial("SED") + xNewValue, "ED_CCJURI")
	cCCNatOld := JurGetDados("SED", 1, xFilial("SED") + xOldValue, "ED_CCJURI")

	If AllTrim(SE2->E2_ORIGEM) == "JURCTORC" // Integra��o Controle Or�ament�rio SIGAPFS x SIGAFIN
		If ! Empty(cCCNatOld) .And. cCCNatNew != cCCNatOld // Centros de custos jur�dico diferentes
			lRet := .F.
			JurMsgErro(STR0033,,; // "N�o � poss�vel alterar a natureza desse desdobramento."
			      i18n(STR0034, {AllTrim(xOldValue)}) ) // "Indique uma natureza que possua o mesmo centro de custo jur�dico da natureza '#1'."
		EndIf
	EndIf

	If lRet .And. AllTrim(cAnexo) == ICO_TEM_ANEXO // Possui anexos
		If cCCNatNew $ '6|7'// Houve mudan�a de centro de custo e o novo centro de custo � transit�rio ou transit�rio p�s pagamento
			lRet := .F.
			JurMsgErro(STR0033,,; // "N�o � poss�vel alterar a natureza desse desdobramento."
			      i18n(STR0048, {AllTrim(xNewValue)}) ) // "O desdobramento possui anexo(s). Para indicar a natureza '#1' � necess�rio excluir o(s) anexo(s)."
		EndIf
	EndIf
EndIf

If lRet .And. cAction == "SETVALUE" .And. !Empty(oModelOHF:GetValue("OHF_CDESP")) ;
   .And. cField $ "OHF_CPART|OHF_SIGLA|OHF_CCLIEN|OHF_CLOJA|OHF_CCASO|OHF_CTPDSP|OHF_QTDDSP|OHF_DTDESP|OHF_COBRA"

	lRet := J246VldPre(oModelOHF,"OHF")
EndIf

If lRet
	lRet := JAtuValDes("OHF",�oModel,�nLine,�cAction)
EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J246VldPre
Valida se o usu�rio tem permiss�o para alterar lan�amentos em pr�-fatura
e se a mesma pode ser editada.

@Param oMdlDes, Modelo de dados de desdobramentos/desd. p�s pag.
@Param  cTab  , Tabela do desdobramento "OHF" ou desdobramento p�s pag. "OHG"

@author Abner Foga�a de Oliveira
@since 04/05/2020
/*/
//-------------------------------------------------------------------
Function J246VldPre(oMdlDes, cTab)
	Local lRet     := .T.
	Local cPartLog := JurUsuario(__CUSERID)

	If JurGetDados("NUR", 1, xFilial("NUR") + cPartLog, "NUR_LCPRE") == "2" // Participante sem permiss�o para alterar lan�amentos em pr�-fatura
		JurMsgErro(STR0062,, STR0063) // "O participante n�o tem permiss�o para alterar despesas com Pr�-faturas." # "Verifique o cadastro do participante."
		lRet := .F.
	Else
		cDespesa := oMdlDes:GetValue(cTab + "_CDESP")
		aRetDesp := JurGetDados("NVY", 1, xfilial("NVY") + cDespesa, {"NVY_SITUAC", "NVY_CPREFT"})

		If !Empty(aRetDesp) .And. (aRetDesp[1] == "2" .Or.;
			(!Empty(aRetDesp[2]) .And. !(JurGetDados("NX0", 1, xFilial("NX0") + aRetDesp[2], "NX0_SITUAC") $ "2|3"))) // Pr�-Fatura difernete de An�lise e Alterada
			lRet := .F.
			JurMsgErro(STR0056,,; // "A situa��o da despesa vinculada ao desdobramento n�o permite altera��es."
				i18n(STR0057, {AllTrim(cDespesa)}) ) // "Verifique o cadastro da despesa '#1'."
		EndIf
	EndIf

Return lRet
//-------------------------------------------------------------------
/*/{Protheus.doc} JA246Event
Classe interna implementando o FWModelEvent, para execu��o de fun��o
durante o commit.

@author bruno.ritter
@since 04/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Class JA246Event FROM FWModelEvent
	Data aModelDesp // Model para inclus�o de Despesa
	Data aModelLanc // Model para altera��o de Lan�amentos
	Data aModelNZQ  // Model para aprova��o de despesa

	Method New()
	Method GridLinePreVld()
	Method ModelPosVld()
	Method Before()
	Method InTTS()
	Method Destroy()
End Class

//-------------------------------------------------------------------
/*/ { Protheus.doc } New()
New FWModelEvent
/*/
//-------------------------------------------------------------------
Method New() Class JA246Event

	Self:aModelDesp := {}
	Self:aModelLanc := {}
	Self:aModelNZQ  := {}

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} GridLinePreVld
M�todo que � chamado pelo MVC quando ocorrer as a��es de pre valida��o da linha do Grid

@param oSubModel , Modelo principal
@param cModelId  , Id do submodelo
@param nLine     , Linha do grid
@param cAction   , A��o executada no grid, podendo ser: ADDLINE, UNDELETE, DELETE, SETVALUE, CANSETVALUE, ISENABLE
@param cId       , nome do campo
@param xValue    , Novo valor do campo
@param xCurrentVl, Valor atual do campo

@author bruno.ritter
@since 20/12/2018
/*/
//-------------------------------------------------------------------
Method GridLinePreVld(oSubModel, cModelId, nLine, cAction, cId, xValue, xCurrentVl) Class JA246Event
	Local lRet       := .T.
	Local lDesdOld   := SuperGetMV("MV_NRASDSD", .F.) // Permite que o desdobramento de t�tulos seja realizado no processo antigo, sem rastreamento e excluindo o t�tulo originador.
	Local lDesdFin   := Iif(lDesdOld, SE2->E2_DESDOBR == "S" .And. SE2->(Deleted()), SE2->E2_STATUS == "D") // Se o t�tulo est� sendo desdobrado pelo financeiro
	Local lExisteBx  := SE2->E2_SALDO != SE2->E2_VALOR

	If cModelId == "OHFDETAIL" .And. (lExisteBx .And. !lDesdFin)
		If cAction $ "DELETE" .And. !oSubModel:IsInserted()
			JurMsgErro(STR0021,, STR0022, .F.) // "N�o � permitido incluir ou excluir desdobramentos quando a situa��o do t�tulo � diferente de aberto." "Altere a situa��o do t�tulo."
			lRet := .F.
		EndIf

		If cAction == 'CANSETVALUE' .And. oSubModel:IsInserted()
			lRet := .F.
		EndIf
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelPosVld
M�todo que � chamado pelo MVC quando ocorrer as a��es de p�s valida��o do Model.

@author bruno.ritter
@since 04/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Method ModelPosVld(oModel, cModelId) Class JA246Event
	Local lRet       := .T.
	Local lIsRest    := FindFunction("JurIsRest") .And. JurIsRest()
	Local lOrigJu049 := FwIsInCallStack("J049RepDsb") // Quando a origem da opera��o for da JURA049(Despesa)
	Local lOrigJ235A := FwIsInCallStack("J235ACancela") .Or. FwIsInCallStack("J235ADsdb") // Quando a origem � a JURA235A (aprova��o de solicita��o de despesas ou cancelamento da aprova��o)
	Local lCodAprDes := OHF->(ColumnPos("OHF_NZQCOD")) > 0
	Local lCancAprov := FWIsInCallStack("J235ACancela") // Quando a origem da opera��o for da Cancelamento aprova��o de despesas (JURA235A)
	Local aRetTemp   := {} // Recebe retorno das fun��es de modelo
	Local nOper      := oModel:GetOperation()
	Local lDesdOld   := SuperGetMV("MV_NRASDSD", .F.) // Permite que o desdobramento de t�tulos seja realizado no processo antigo, sem rastreamento e excluindo o t�tulo originador.
	Local lDesdFin   := Iif(lDesdOld, SE2->E2_DESDOBR == "S" .And. SE2->(Deleted()), SE2->E2_STATUS == "D") // Se o t�tulo est� sendo desdobrado pelo financeiro
	Local lExisteBx  := SE2->E2_SALDO != SE2->E2_VALOR
	
	Self:aModelDesp  := {}
	Self:aModelLanc  := {}
	Self:aModelNZQ   := {}

	// Valida cliente e loja nos desdobramentos
	lRet := J246VldCli(oModel)

	// Altera as aprova��es de despesa conforme atualiza��o do desdobramento
	If lRet .And. lCodAprDes .And. FindFunction("J235AUpdNZQ") .And. !lOrigJ235A
		aRetTemp := J235AUpdNZQ(oModel)
		If lRet := aRetTemp[1]
			Self:aModelNZQ := aRetTemp[2]
		EndIf
	EndIf

	If lRet .And. !lOrigJu049
		//Gera e valida modelo para INSERT/UPDATE/DELETE da Despesa
		aRetTemp := J246OpDesp(oModel)
		If lRet := aRetTemp[1]
			Self:aModelDesp := aRetTemp[2]
		EndIf
	EndIf

	// Valida��o Calend�rio cont�bil x Lan�amentos
	If lRet
		lRet := JA246VldCal(oModel)
	EndIf

	// Valida dados do desdobramento
	If lRet .And. ! oModel:GetModel("OHFDETAIL"):IsDeleted()
		lRet := J246VldDes(oModel)
	EndIf

	If lRet .And. (lExisteBx .And. !lDesdFin)
		Self:aModelLanc := J246VlLanc(oModel)
	EndIf

	If !lRet
		JurFreeArr(@Self:aModelDesp)
		JurFreeArr(@Self:aModelLanc)
		JurFreeArr(@Self:aModelNZQ)
	EndIf

	If lRet .And. FindFunction("J235Anexo") .And. !FWIsInCallStack("J247LANC") .And. (lIsRest .Or. lCancAprov .Or. nOper == MODEL_OPERATION_DELETE)
		lRet := J235Anexo(oModel, "OHF", "OHFDETAIL", "OHF_IDDOC", "OHF_CITEM")
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} Before
M�todo que � chamado pelo MVC quando ocorrer as a��es do commit
antes da grava��o de cada submodelo (field ou cada linha de uma grid)

@author Bruno Ritter
@since 07/11/2019
/*/
//-------------------------------------------------------------------
Method Before(oSubModel, cModelId, cAlias, lNewRecord) Class JA246Event

	// Processamento para t�tulos que foram desdobrados (parcelado) pelo financeiro
	If cModelId == "SE2MASTER"
		J246DesFin(oSubModel:GetModel(), @Self:aModelDesp, @Self:aModelLanc, @Self:aModelNZQ)
	EndIf

	// Executa estorno de contabiliza��o na altera��o/exclus�o de cada linha do desdobramento
	If !lNewRecord .And. cModelId == "OHFDETAIL" .And. !FWIsInCallStack("J246DelOHF") .And. FindFunction("JURA265B");
	   .And. FindFunction("J265LpFlag") .And. OHF->(ColumnPos("OHF_DTCONT")) > 0 .And. OHF->(ColumnPos("OHF_DTCONI")) > 0
		J246EstCtb(oSubModel, "OHF", "948")
	EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} InTTS
M�todo que � chamado pelo MVC quando ocorrer as a��es do commit Ap�s as grava��es por�m
antes do final da transa��o
@author bruno.ritter
@since 04/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Method InTTS(oModel, cModelId) Class JA246Event
	Local cChave   := ""
	Local cItem    := ""
	Local cIdDoc   := ""
	Local nCtb   := 0

	If !Empty(Self:aModelDesp)
		Processa({|| J246CMMdls(Self:aModelDesp, "NVY", "NVYMASTER")}, STR0001, STR0002) // "Gravando." "Atualizando Despesa..."
	EndIf

	If !Empty(Self:aModelLanc)
		Processa({|| J246CMMdls(Self:aModelLanc, "OHB", "OHBMASTER")}, STR0001, STR0049) // "Gravando." "Atualizando Lan�amentos..."
	EndIf

	If !Empty(Self:aModelNZQ)
		Processa({|| J246CMMdls(Self:aModelNZQ, "NZQ", "NZQMASTER")}, STR0001, STR0053) // "Gravando." "Atualizando Aprova��es de Despesas..."
	EndIf

	If FWIsInCallStack("J235APreApr") .And. FindFunction("J235RepAnex") // Replica anexos da solicita��o de despesa quando vier da aprova��o
		cChave := SE2->E2_FILIAL+'|'+SE2->E2_PREFIXO+'|'+SE2->E2_NUM+'|'+SE2->E2_PARCELA+'|'+SE2->E2_TIPO+'|'+SE2->E2_FORNECE+'|'+SE2->E2_LOJA
		cItem  := oModel:GetValue("OHFDETAIL", "OHF_CITEM")
		cIdDoc := FINGRVFK7('SE2', cChave) + cItem
		J235RepAnex("OHF", xFilial("OHF"), cIdDoc, cChave, cItem)
	EndIf

	// Exclui os anexos dos desdobramentos que forem exclu�dos
	J247ExcAnx(oModel, "OHF")
	
	// Executa contabiliza��o desdobramentos estornados por altera��es
	If FindFunction("JURA265B")
		For nCtb := 1 To Len(_aRecDesCtb)
			JURA265B("947", _aRecDesCtb[nCtb]) // Contabiliza��o de inclus�o de desdobramento
		Next nCtb
	EndIf

	JurFreeArr(_aRecDesCtb)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Destroy
Destrutor da classe

@author bruno.ritter
@since 04/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Method Destroy() Class JA246Event

	JurFreeArr(@Self:aModelDesp)
	JurFreeArr(@Self:aModelLanc)
	JurFreeArr(@Self:aModelNZQ)

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} J246DIALOG()
Monta a tela para o Desdobramento ap�s a inclus�o do t�tulo a pagar.

@param  lConfirma  Indica que a chamada foi feita na confirma��o do t�tulo
@param  nOperacao  Opera��o realizada (1-Visualiza��o / 4-Altera��o)

@author Nivia Ferreira | Bruno Ritter
@since 09/02/18
@version 1.0
/*/
//-------------------------------------------------------------------
Function J246DIALOG(lConfirma, nOperacao)
Local aArea       := GetArea()
Local lRet        := .T.
Local aSize       := {}
Local aButtons    := {}
Local oLayer      := FWLayer():new()
Local oModel      := Nil
Local oModelOHF   := Nil
Local oMainColl   := Nil
Local oScroll     := Nil
Local oPanel      := Nil
Local nSaveRec    := SE2->(Recno())
Local nAltura     := 0
Local nSizeTela   := 0
Local nTamDialog  := 0
Local nCoordPos   := 95
Local nLargura    := 270
Local nPosLoja    := 0
Local nLarLoja    := 0
Local nValorSE2   := JCPVlBruto(nSaveRec)
Local cNaturSE2   := SE2->E2_NATUREZ
Local cCCJuri     := JurGetDados("SED", 1, xFilial("SED") + cNaturSE2, "ED_CCJURI")
Local lCCJuriDef  := !Empty(cCCJuri) // Indica se a natureza tem Centro de Custo Jur�dico definido
Local lVisualiza  := .F.
Local lContOrcam  := AllTrim(SE2->E2_ORIGEM) == "JURCTORC" // Integra��o Controle Or�ament�rio SIGAPFS x SIGAFIN
Local lUtProj     := SuperGetMv( "MV_JUTPROJ", .F., .F., ) // Indica se ser� utilizado Projeto/Finalidade nas rotinas do Financeiro (.T. = Sim; .F. = N�o)
Local lContOrc    := SuperGetMv( "MV_JCONORC", .F., .F., ) // Indica se ser� utilizado Controle Or�ament�rio (.T. = Sim; .F. = N�o)
Local cChvPagP    := SE2->E2_FILIAL + "|" +  SE2->E2_PREFIXO + "|" + SE2->E2_NUM + "|" + SE2->E2_PARCELA + "|" + SE2->E2_TIPO + "|" + SE2->E2_FORNECE + "|" + SE2->E2_LOJA
Local cIdDocPag   := FINGRVFK7("SE2", cChvPagP)
Local cLojaAuto   := SuperGetMv( "MV_JLOJAUT", .F., "2", ) //Indica se a Loja do Caso deve ser preenchida automaticamente. (1-Sim; 2-N�o)
Local aCampos     := {}
Local aNoAcess    := {}
Local aOfuscar    := {}

Private oDlg      := Nil
Private oCliOr    := Nil
Private oDesCli   := Nil
Private oLojaOr   := Nil
Private oLojaOrF3 := Nil
Private oCodNat   := Nil
Private oDesNat   := Nil
Private oVlTit    := Nil
Private oSiglSol  := Nil
Private oNomSigl  := Nil
Private oCodEsc   := Nil
Private oDesEsc   := Nil
Private oCodCc    := Nil
Private oDesCc    := Nil
Private oSigPart  := Nil
Private oDesPart  := Nil
Private oCodRate  := Nil
Private oDesRate  := Nil
Private oCasoOr   := Nil
Private oDesCas   := Nil
Private oCodDesp  := Nil
Private oDesDesp  := Nil
Private oQtdDes   := Nil
Private oDtDesp   := Nil
Private oCbDesp   := Nil
Private oCodHp    := Nil
Private oHistor   := Nil
Private oCodProj  := Nil
Private oDesProj  := Nil
Private oCItProj  := Nil
Private oDItProj  := Nil

Private cCliOr    := ""
Private cLojaOr   := ""

Default lConfirma := .F.
Default nOperacao := MODEL_OPERATION_UPDATE

lVisualiza := nOperacao == 1

If AliasInDic("OHF")
	oModel    := FWLoadModel("JURA246")
	cCliOr    := CriaVar('OHF_CCLIEN', .F.) // Filtro do F3 caso
	cLojaOr   := CriaVar('OHF_CLOJA',  .F.) // Filtro do F3 caso

	oModel:SetOperation(nOperacao)
	If oModel:CanActivate()
		oModel:Activate()
		oModelOHF := oModel:GetModel("OHFDETAIL")

		If oModelOHF:CanSetValue("OHF_CITEM") .And. Empty(oModelOHF:GetValue("OHF_CITEM"))
			oModelOHF:SetValue("OHF_CITEM", StrZero(1, TAMSX3("OHF_CITEM")[1]))
		EndIf

		If lUtProj .Or. lContOrc // Aumenta a quantidade de pixels para ajustar a tela e acionar o scroll
			nAltura := 60
		EndIf

		Do Case
			Case Empty(cCCJuri) .Or. cCCJuri $ "5" // N�o definido ou Despesa de Cliente
				nAltura += 340
			Case cCCJuri $ "1|3|4" // Escrit�rio / Profissional / Tabela de Rateio
				nAltura += 250
			Case cCCJuri $ "2"     // Escrit�rio e Centro de Custo
				nAltura += 280
		EndCase

		If FindFunction("FwPDCanUse") .And. FwPDCanUse(.T.)
			aCampos := {"OHF_DNATUR","OHF_DPART" ,"OHF_DESCR" ,"OHF_DCUSTO","OHF_DPART2","OHF_DRATEI","OHF_DCLIEN","OHF_DCASO" ,"OHF_DPROJE","OHF_DITPRJ","OHF_HISTOR"}

			aNoAcess := FwProtectedDataUtil():UsrNoAccessFieldsInList(aCampos)
			AEval(aNoAcess, {|x| AAdd( aOfuscar, x:CFIELD)})

		EndIf

		// Retorna o tamanho da tela
		aSize   := MsAdvSize(.F.)
		nSizeTela := ((aSize[6]/2)*0.85) // Diminui 15% da altura.

		If nAltura > 0 .And. nSizeTela < nAltura
			nTamDialog := nSizeTela
		Else
			nTamDialog := nAltura
		EndIf

		aAdd(aButtons, {,STR0046,{|| JA246Anexo(.F.) },,,.T.,.T.} ) // "Anexos"

		oDlg := FWDialogModal():New()
		oDlg:SetFreeArea(nLargura, nTamDialog)
		oDlg:SetEscClose(.F.)    // N�o permite fechar a tela com o ESC
		oDlg:SetCloseButton(.F.) // N�o permite fechar a tela com o "X"
		oDlg:SetBackground(.T.)  // Escurece o fundo da janela
		oDlg:SetTitle(STR0025)   // "Detalhes do Contas a Pagar"
		oDlg:CreateDialog()
		oDlg:addButtons(aButtons)
		oDlg:addOkButton({|| IIf(J246Commit(@oModel), oDlg:oOwner:End(), lRet := .F.)})
		oDlg:addCloseButton({|| IIf(lConfirma, J246DelOHF(), Nil), IIf(lVisualiza .Or. JA246Desd(cIdDocPag), oDlg:oOwner:End(), Alert(STR0043)) }) //"Cancelar" // "O preenchimento dos detalhes do t�tulo � obrigat�rio. Por favor, verifique!"

		// Cria objeto Scroll
		oScroll := TScrollArea():New(oDlg:GetPanelMain(),01,01,365,545)
		oScroll:Align := CONTROL_ALIGN_ALLCLIENT

		@ 000,000 MSPANEL oPanel OF oScroll SIZE nLargura, nAltura

		oLayer := FwLayer():New()
		oLayer:Init(oPanel, .F.)
		oLayer:addCollumn("MainColl",100,.F.) //Cria as colunas do Layer
		oMainColl := oLayer:GetColPanel( 'MainColl' )

		// Define objeto painel como filho do scroll
		oScroll:SetFrame( oPanel )

		//-----------------
		// "C�d Natureza" //
		oCodNat := TJurPnlCampo():New(005,015,060,022,oMainColl, AllTrim(RetTitle("OHF_CNATUR")), ("OHF_CNATUR"),{|| },{|| },,,,'SEDOHF',,,,,)
		oCodNat:SetWhen({|| J246DlgVal(@oModel, "OHF_CNATUR", cNaturSE2, cCCJuri) .And. .F. })
		oCodNat:SetValue(cNaturSE2)

		// "Desc Naturez" //
		oDesNat := TJurPnlCampo():New(005,085,170,022,oMainColl, AllTrim(RetTitle("OHF_DNATUR")) ,("ED_DESCRIC"),{|| },{|| },,,,,,,,,aScan(aOfuscar,"OHF_DNATUR") > 0)
		oDesNat:SetWhen({||.F.})

		//-----------------
		// "Vl. T�tulo." //
		oVlTit := TJurPnlCampo():New(035,015,90,022,oMainColl, STR0027 ,("OHF_SALDO"),{|| },{|| },,,,,,,,,) // "Vl. T�tulo"
		oVlTit:SetWhen({|| J246DlgVal(@oModel, "OHF_VALOR", nValorSE2, cCCJuri) .And. .F. })
		oVlTit:SetValue(nValorSE2)

		//-----------------
		// "Sigla Solic." //
		oSiglSol := TJurPnlCampo():New(065,015,060,022,oMainColl, AllTrim(RetTitle("OHF_SIGLA")) ,("RD0_SIGLA"),{|| },{|| },,,,'RD0ATV',,,,,)
		oSiglSol:SetValid({|| J246DlgVal(@oModel, "OHF_SIGLA", oSiglSol:GetValue(), cCCJuri)})
		oSiglSol:SetWhen({||oModelOHF:CanSetValue("OHF_SIGLA")})
		oSiglSol:Enable(!lVisualiza)

		// "Nome Solic" //
		oNomSigl := TJurPnlCampo():New(065,085,170,022,oMainColl, AllTrim(RetTitle("OHF_DPART ")) ,("OHF_DPART "),{|| },{|| },,,,,,,,,aScan(aOfuscar,"OHF_DPART") > 0)
		oNomSigl:SetWhen({||.F.})

		//-----------------

		If cCCJuri $ "1|2" .Or. !lCCJuriDef // Escrit�rio / Escrit�rio - Centro de Custo / N�o Definido

			// "Escrit�rio  " //
			oCodEsc := TJurPnlCampo():New(nCoordPos,015,060,022,oMainColl, AllTrim(RetTitle("OHF_CESCR")) ,("OHF_CESCR"),{|| },{|| },,,,'NS7ATV',,,,,)
			oCodEsc:SetValid({|| J246DlgVal(@oModel, "OHF_CESCR", oCodEsc:GetValue(), cCCJuri)})
			oCodEsc:SetWhen({||oModelOHF:CanSetValue("OHF_CESCR")})
			oCodEsc:Enable(!lVisualiza .And. !lContOrcam)

			// "Desc. Escrit" //
			oDesEsc := TJurPnlCampo():New(nCoordPos,085,170,022,oMainColl, AllTrim(RetTitle("OHF_DESCR")) ,("OHF_DESCR"),{|| },{|| },,,,,,,,,aScan(aOfuscar,"OHF_DESCR") > 0)
			oDesEsc:SetWhen({||.F.})

			nCoordPos += 30

			//-----------------

			If cCCJuri == "2" .Or. !lCCJuriDef // Escrit�rio - Centro de Custo / N�o Definido

				// "Centro Custo" //
				oCodCc := TJurPnlCampo():New(nCoordPos,015,060,022,oMainColl, AllTrim(RetTitle("OHF_CCUSTO")) ,("OHF_CCUSTO"),{|| },{|| },,,,'CTTNS7',,,,,)
				oCodCc:SetValid({||J246DlgVal(@oModel, "OHF_CCUSTO", oCodCc:GetValue(), cCCJuri)})
				oCodCc:SetWhen({||oModelOHF:CanSetValue("OHF_CCUSTO")})
				oCodCc:Enable(!lVisualiza .And. !lContOrcam)

				//"Desc C Custo" //
				oDesCc := TJurPnlCampo():New(nCoordPos,085,170,022,oMainColl, AllTrim(RetTitle("OHF_DCUSTO")) ,("OHF_DCUSTO"),{|| },{|| },,,,,,,,,aScan(aOfuscar,"OHF_DCUSTO") > 0)
				oDesCc:SetWhen({||.F.})

				nCoordPos += 30

			EndIf

		EndIf

		//-----------------

		If cCCJuri == "3" .Or. !lCCJuriDef // Profissional / N�o Definido

			// "Sigla Partic" //
			oSigPart := TJurPnlCampo():New(nCoordPos,015,060,022,oMainColl, AllTrim(RetTitle("OHF_SIGLA2")) ,("RD0_SIGLA"),{|| },{|| },,,,'RD0ATV',,,,,)
			oSigPart:SetValid({||J246DlgVal(@oModel, "OHF_SIGLA2", oSigPart:GetValue(), cCCJuri)})
			oSigPart:SetWhen({||oModelOHF:CanSetValue("OHF_SIGLA2")})
			oSigPart:Enable(!lVisualiza .And. !lContOrcam)

			// "Nome Part." //
			oDesPart := TJurPnlCampo():New(nCoordPos,085,170,022,oMainColl, AllTrim(RetTitle("OHF_DPART2")) ,("OHF_DPART2"),{|| },{|| },,,,,,,,,aScan(aOfuscar,"OHF_DPART2") > 0)
			oDesPart:SetWhen({||.F.})

			nCoordPos += 30

		EndIf

		//-----------------

		If cCCJuri == "4" .Or. !lCCJuriDef // Tabela de Rateio / N�o Definido

			// "Tab. Rateio " //
			oCodRate := TJurPnlCampo():New(nCoordPos,015,060,022,oMainColl, AllTrim(RetTitle("OHF_CRATEI")) ,("OH6_CODIGO"),{|| },{|| },,,,'OH6',,,,,)
			oCodRate:SetValid({||J246DlgVal(@oModel, "OHF_CRATEI", oCodRate:GetValue(), cCCJuri)})
			oCodRate:SetWhen({||oModelOHF:CanSetValue("OHF_CRATEI")})
			oCodRate:Enable(!lVisualiza .And. !lContOrcam)

			// "Desc. Rateio" //
			oDesRate := TJurPnlCampo():New(nCoordPos,085,170,022,oMainColl, AllTrim(RetTitle("OHF_DRATEI")) ,("OHF_DRATEI"),{|| },{|| },,,,,,,,,aScan(aOfuscar,"OHF_DRATEI") > 0)
			oDesRate:SetWhen({||.F.})

			nCoordPos += 30

		EndIf

		//-----------------

		If cCCJuri == "5" // Despesa de Cliente

			// "C�d Cliente" //
			oCliOr := TJurPnlCampo():New(nCoordPos,015,060,022,oMainColl, AllTrim(RetTitle("OHF_CCLIEN")) ,("OHF_CCLIEN"),{|| },{|| },,,,'SA1NUH',,,,,)
			oCliOr:SetValid({||J246DlgVal(@oModel, "OHF_CCLIEN", oCliOr:GetValue(), cCCJuri)})
			oCliOr:SetWhen({||oModelOHF:CanSetValue("OHF_CCLIEN")})
			oCliOr:Enable(!lVisualiza)
			If(cLojaAuto == "1")
				oCliOr:SetChange( {|| cCliOr := oCliOr:GetValue(), oLojaOr:SetValue(JurGetLjAt()), cLojaOr := JurGetLjAt() } )
			Else
				oCliOr:SetChange( {|| cCliOr := oCliOr:GetValue(), cLojaOr := oLojaOr:GetValue() } )
			EndIf

			// "Loja do F3, usado pois a fun��o de valid apaga a loja quando o preenchimento veio do F3" //
			oLojaOrF3 := TJurPnlCampo():New(nCoordPos,085,030,022,oMainColl, "" ,("OHF_CLOJA "),{|| },{|| },,,,,,,,,)
			oLojaOrF3:SetValid({||J246DlgVal(@oModel, "OHF_CLOJA", oLojaOrF3:GetValue(), cCCJuri)})
			oLojaOrF3:SetWhen({||oModelOHF:CanSetValue("OHF_CLOJA")})
			oLojaOrF3:SetChange({|| cCliOr := oCliOr:GetValue(),;
									cLojaOr:= oLojaOr:GetValue()})
			oLojaOrF3:Visible(.F.)

			// "Loja" //
			oLojaOr := TJurPnlCampo():New(nCoordPos,085,030,022,oMainColl, "" ,("OHF_CLOJA "),{|| },{|| },,,,,,,,,)
			oLojaOr:SetValid({||J246DlgVal(@oModel, "OHF_CLOJA", oLojaOr:GetValue(), cCCJuri)})
			oLojaOr:SetWhen({||oModelOHF:CanSetValue("OHF_CLOJA")})
			oLojaOr:Enable(!lVisualiza)
			oLojaOr:SetChange({|| cCliOr := oCliOr:GetValue(),;
								cLojaOr:= oLojaOr:GetValue()})
			oLojaOr:Visible(cLojaAuto == "2")

			// "NOME CLIENTE" //
			If cLojaAuto == "2"
				nPosLoja := 115
				nLarLoja := 140
			Else
				nPosLoja := 085
				nLarLoja := 170
			EndIf

			oDesCli := TJurPnlCampo():New(nCoordPos,nPosLoja,nLarLoja,022,oMainColl, AllTrim(RetTitle("OHF_DCLIEN")) ,("OHF_DCLIEN"),{|| },{|| },,,,,,,,,aScan(aOfuscar,"OHF_DCLIEN") > 0)
			oDesCli:SetWhen({||.F.})

			nCoordPos += 30

			//-----------------
			// "C�digo Caso" //
			oCasoOr := TJurPnlCampo():New(nCoordPos,015,060,022,oMainColl, AllTrim(RetTitle("OHF_CCASO")) ,("OHF_CCASO"),{|| },{|| },,,,'NVELOJ',,,,,)
			oCasoOr:SetValid({||J246DlgVal(@oModel, "OHF_CCASO", oCasoOr:GetValue(), cCCJuri)})
			oCasoOr:SetWhen({||oModelOHF:CanSetValue("OHF_CCASO")})
			oCasoOr:Enable(!lVisualiza)

			// "Desc. Caso" //
			oDesCas := TJurPnlCampo():New(nCoordPos,085,170,022,oMainColl, AllTrim(RetTitle("OHF_DCASO")) ,("OHF_DCASO"),{|| },{|| },,,,,,,,,aScan(aOfuscar,"OHF_DCASO") > 0)
			oDesCas:SetWhen({||.F.})

			nCoordPos += 30

			//-----------------
			// "Tipo Despesa" //
			oCodDesp := TJurPnlCampo():New(nCoordPos,015,060,022,oMainColl, AllTrim(RetTitle("OHF_CTPDSP")) ,("NRH_COD"),{|| },{|| },,,,'NRH',,,,,)
			oCodDesp:SetValid({|| J246DlgVal(@oModel, "OHF_CTPDSP", oCodDesp:GetValue(), cCCJuri)})
			oCodDesp:SetWhen({|| oModelOHF:CanSetValue("OHF_CTPDSP") })
			oCodDesp:Enable(!lVisualiza)

			// "Desc Tp Desp" //
			oDesDesp := TJurPnlCampo():New(nCoordPos,085,170,022,oMainColl, AllTrim(RetTitle("OHF_DTPDSP")) ,("NRH_DESC"),{|| },{|| },,,,,,,,,aScan(aOfuscar,"OHF_DTPDSP") > 0)
			oDesDesp:SetWhen({||.F.})

			nCoordPos += 30

			//-----------------
			// "Qtd despesa " //
			oQtdDes := TJurPnlCampo():New(nCoordPos,015,040,022,oMainColl, AllTrim(RetTitle("OHF_QTDDSP")) ,("OHF_QTDDSP"),{|| },{|| },,,,,,,,,)
			oQtdDes:SetValid({|| J246DlgVal(@oModel, "OHF_QTDDSP", oQtdDes:GetValue(), cCCJuri)})
			oQtdDes:SetWhen({|| oModelOHF:CanSetValue("OHF_QTDDSP") })
			oQtdDes:Enable(!lVisualiza)

			// "Data Despesa" //
			oDtDesp := TJurPnlCampo():New(nCoordPos,085,060,022,oMainColl, AllTrim(RetTitle("OHF_DTDESP")) ,("OHF_DTDESP"),{|| },{|| },,,,,,,,,)
			oDtDesp:SetValid({|| J246DlgVal(@oModel, "OHF_DTDESP", oDtDesp:GetValue(), cCCJuri)})
			oDtDesp:SetWhen({|| oModelOHF:CanSetValue("OHF_DTDESP") })
			oDtDesp:Enable(!lVisualiza)

			//-----------------
			// "Cobrar Desp?" //
			oCbDesp := TJurPnlCampo():New(nCoordPos,155,060,025,oMainColl, AllTrim(RetTitle("OHF_COBRA")) ,("OHF_COBRA"),{|| },{|| },,,,,,,,,)
			oCbDesp:SetValid({|| J246DlgVal(@oModel, "OHF_COBRA", oCbDesp:GetValue(), cCCJuri)})
			oCbDesp:SetWhen({|| oModelOHF:CanSetValue("OHF_COBRA") })
			oCbDesp:Enable(!lVisualiza)

			nCoordPos += 30

		EndIf

		If (lUtProj .Or. lContOrc) .And. OHF->(ColumnPos("OHF_CPROJE")) > 0
			// "C�digo Projeto" //
			oCodProj := TJurPnlCampo():New(nCoordPos,015,060,022, oMainColl, AllTrim(RetTitle("OHF_CPROJE")) ,("OHF_CPROJE"),{|| },{|| },,,,'OHL',,,,,)
			oCodProj:SetValid({||J246DlgVal(@oModel, "OHF_CPROJE", oCodProj:GetValue(), cCCJuri)})
			oCodProj:SetWhen({||oModelOHF:CanSetValue("OHF_CPROJE")})
			oCodProj:Enable(!lVisualiza .And. !lContOrcam)

			// "Desc. Projeto" //
			oDesProj := TJurPnlCampo():New(nCoordPos,085,170,022, oMainColl, AllTrim(RetTitle("OHF_DPROJE")) ,("OHF_DPROJE"),{|| },{|| },,,,,,,,,aScan(aOfuscar,"OHF_DPROJE") > 0)
			oDesProj:SetWhen({||.F.})

			nCoordPos += 30

			// "C�digo Item Projeto" //
			oCItProj := TJurPnlCampo():New(nCoordPos,015,060,022, oMainColl, AllTrim(RetTitle("OHF_CITPRJ")) ,("OHF_CITPRJ"),{|| },{|| },,,,'OHM',,,,,)
			oCItProj:SetValid({||J246DlgVal(@oModel, "OHF_CITPRJ", oCItProj:GetValue(), cCCJuri)})
			oCItProj:SetWhen({||!Empty(oCodProj:GetValue()) .And. oModelOHF:CanSetValue("OHF_CITPRJ")})
			oCItProj:Enable(!lVisualiza .And. !lContOrcam)

			// "Desc. Item Projeto" //
			oDItProj := TJurPnlCampo():New(nCoordPos,085,170,022, oMainColl, AllTrim(RetTitle("OHF_DITPRJ")) ,("OHF_DITPRJ"),{|| },{|| },,,,,,,,,aScan(aOfuscar,"OHF_DITPRJ") > 0)
			oDItProj:SetWhen({||.F.})

			nCoordPos += 30

		EndIf

		// "C�d Hist Pad" //
		oCodHp := TJurPnlCampo():New(nCoordPos,015,080,022,oMainColl, AllTrim(RetTitle("OHF_CHISTP")) ,("OHF_CHISTP"),{|| },{|| },,,,,,,,,)
		oCodHp:SetValid({|| J246DlgVal(@oModel, "OHF_CHISTP", oCodHp:GetValue(), cCCJuri)})
		oCodHp:SetWhen({|| oModelOHF:CanSetValue("OHF_CHISTP") })
		oCodHp:Enable(!lVisualiza)

		nCoordPos += 30

		//-----------------
		// "Hist�rico   " //
		oHistor := TJurPnlCampo():New(nCoordPos,015,200,090,oMainColl, AllTrim(RetTitle("OHF_HISTOR")), ("OHF_HISTOR"),{|| },{|| },,,,,,,,,aScan(aOfuscar,"OHF_HISTOR") > 0)
		oHistor:SetValid({|| J246DlgVal(@oModel, "OHF_HISTOR", oHistor:GetValue(), cCCJuri)})
		oHistor:SetWhen({|| oModelOHF:CanSetValue("OHF_HISTOR") })
		oHistor:Enable(!lVisualiza)

		oDlg:Activate()

		oModel:DeActivate()
		SE2->(dbGoto(nSaveRec))
	EndIf

EndIf

JurFreeArr(@aButtons)

RestArea( aArea )

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J246Commit()
Bot�o de ok da dialog, valida e inclui do Desdobramento.

@param oModel  Modelo de dados de Detalhes / Desdobramentos

@author Nivia Ferreira | Bruno Ritter
@since 15/02/18
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function J246Commit(oModel)
Local lRet     := .T.
Local aErro    := {}
Local aArea    := GetArea()

	If oModel:GetOperation() != MODEL_OPERATION_VIEW
		If !(oModel:VldData() .And. oModel:CommitData())
			aErro := oModel:GetErrorMessage(.T.)
			Help("", 1, "HELP",, aErro[6], 1,,,,,,, {aErro[7]})
			lRet := .F.
		EndIf
	EndIf

	RestArea( aArea )
	
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J246DlgVal()
Valid dos campos da Dialog.
Valida os campos e preenche os campos referentes aos gatilhos da OHF

@param oModel    Modelo de dados de Detalhes / Desdobramentos
@param cCampo    Campo que ser� atualizado
@param cValue    Valor que ser� indicado no cCampo
@param cCCJuri   Centro de Custo Jur�dico da natureza indicada no t�tulo

@author Nivia Ferreira | Bruno Ritter
@since 15/02/18
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function J246DlgVal(oModel, cCampo, cValue, cCCJuri)
Local aErro      := {}
Local lRet       := .T.
Local oModelOHF  := oModel:GetModel("OHFDETAIL")
Local lCCJuriDef := !Empty(cCCJuri) // Indica se a natureza tem Centro de Custo Jur�dico definido
Local lUtProj    := SuperGetMv( "MV_JUTPROJ", .F., .F., ) // Indica se ser� utilizado Projeto/Finalidade nas rotinas do Financeiro (.T. = Sim; .F. = N�o)
Local lContOrc   := SuperGetMv( "MV_JCONORC", .F., .F., ) // Indica se ser� utilizado Controle Or�ament�rio (.T. = Sim; .F. = N�o)
Local cLojaAuto  := SuperGetMv( "MV_JLOJAUT", .F., "2", ) //Indica se a Loja do Caso deve ser preenchida automaticamente. (1-Sim; 2-N�o)

	If oModel:GetOperation() != MODEL_OPERATION_VIEW
		If oModelOHF:CanSetValue(cCampo)
			lRet := oModelOHF:SetValue(cCampo, cValue)
		EndIf

		If lRet .And. cCCJuri == "5" .And. !Empty(oLojaOrF3:GetValue()) .And. cLojaAuto == '2'
			lRet := oModelOHF:SetValue("OHF_CLOJA", oLojaOrF3:GetValue())
			oLojaOrF3:SetValue(CriaVar('OHF_CLOJA', .F.))
		EndIf
	EndIf

	If lRet
		oCodNat:SetValue( oModelOHF:GetValue("OHF_CNATUR"))
		oDesNat:SetValue( oModelOHF:GetValue("OHF_DNATUR"))
		oSiglSol:SetValue(oModelOHF:GetValue("OHF_SIGLA "))
		oNomSigl:SetValue(oModelOHF:GetValue("OHF_DPART" ))
		If cCCJuri $ "1|2" .Or. !lCCJuriDef
			oCodEsc:SetValue( oModelOHF:GetValue("OHF_CESCR "))
			oDesEsc:SetValue( oModelOHF:GetValue("OHF_DESCR "))
			If cCCJuri == "2" .Or. !lCCJuriDef
				oCodCc:SetValue( oModelOHF:GetValue("OHF_CCUSTO"))
				oDesCc:SetValue( oModelOHF:GetValue("OHF_DCUSTO"))
			EndIf
		EndIf
		If cCCJuri == "3" .Or. !lCCJuriDef
			oSigPart:SetValue(oModelOHF:GetValue("OHF_SIGLA2"))
			oDesPart:SetValue(oModelOHF:GetValue("OHF_DPART2"))
		EndIf
		If cCCJuri == "4" .Or. !lCCJuriDef
			oCodRate:SetValue(oModelOHF:GetValue("OHF_CRATEI"))
			oDesRate:SetValue(oModelOHF:GetValue("OHF_DRATEI"))
		EndIf
		If cCCJuri == "5"
			oCliOr:SetValue(  oModelOHF:GetValue("OHF_CCLIEN"))
			oLojaOr:SetValue( oModelOHF:GetValue("OHF_CLOJA "))
			oDesCli:SetValue( oModelOHF:GetValue("OHF_DCLIEN"))
			oCasoOr:SetValue( oModelOHF:GetValue("OHF_CCASO "))
			oDesCas:SetValue( oModelOHF:GetValue("OHF_DCASO "))
			oCodDesp:SetValue(oModelOHF:GetValue("OHF_CTPDSP"))
			oDesDesp:SetValue(oModelOHF:GetValue("OHF_DTPDSP"))
			oQtdDes:SetValue( oModelOHF:GetValue("OHF_QTDDSP"))
			oDtDesp:SetValue( oModelOHF:GetValue("OHF_DTDESP"))
			oCbDesp:SetValue( oModelOHF:GetValue("OHF_COBRA "))
		EndIf
		If lUtProj .Or. lContOrc
			oCodProj:SetValue(oModelOHF:GetValue("OHF_CPROJE"))
			oDesProj:SetValue(oModelOHF:GetValue("OHF_DPROJE"))
			oCItProj:SetValue(oModelOHF:GetValue("OHF_CITPRJ"))
			oDItProj:SetValue(oModelOHF:GetValue("OHF_DITPRJ"))
		EndIf
		oCodHp:SetValue(  oModelOHF:GetValue("OHF_CHISTP"))
		oHistor:SetValue( oModelOHF:GetValue("OHF_HISTOR"))
	Else
		aErro := oModel:GetErrorMessage(.T.)
		Help("", 1, "HELP",, aErro[6], 1,,,,,,, {aErro[7]})
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J246AtuOHF()
Realiza a atualiza��o dos detalhes / desdobramentos quando houver
altera��o de natureza ou outros dados no t�tulo.
Uso no cadastro de t�tulo a pagar (FINA050 - SIGAFIN)

@param lInclui   Indica se a opera��o � inclus�o
@param nRecno    Recno do t�tulo SE2

@author Jorge Martins / Cristina Cintra
@since 13/03/18
@version 1.0
/*/
//-------------------------------------------------------------------
Function J246AtuOHF(lInclui, nRecno)
Local aArea       := GetArea()
Local lRet        := .T.
Local lConfirma   := .T. // Indica que a chamada foi feita na confirma��o do t�tulo
Local lAtuTransit := .F. // Indica se a natureza atual � Transit�ria de Pagamento
Local lOldTransit := .F. // Indica se a natureza antiga � Transit�ria de Pagamento
Local nOpc        := MODEL_OPERATION_UPDATE
Local cOldNatSE2  := IIf(Type('cOldNatPFS') <> 'U', cOldNatPFS, "")
Local cNatTrans   := AllTrim(JurBusNat("7")) // Natureza Transit�ria de Pagamento
Local cTpNatOld   := JurGetDados("SED", 1, xFilial("SED") + cOldNatSE2, "ED_CCJURI")
Local cTpNatAtu   := ""
Local lTpNatDif   := .F. // Indica se o Centro de Custo Jur�dico das naturezas s�o diferentes
Local lDesdOld    := SuperGetMV("MV_NRASDSD", .F.) // Permite que o desdobramento de t�tulos seja realizado no processo antigo, sem rastreamento e excluindo o t�tulo originador.
Local lTitPriPrc  := .F. // T�tulo digitado pelo usu�rio para ser parcelado (desdobramento do financeiro)
Local lAlteNat    := .F. // Se ocorreu altera��o de natureza abre a tela de detalhe / desdobramento para preenchimento
Local lPodParTit  := .T.
Local cTmpSE2     := ""
Local nValOrig    := 0

Default nRecno := SE2->(Recno())

SE2->(dbGoto(nRecno))

cTpNatAtu   := JurGetDados("SED", 1, xFilial("SED") + SE2->E2_NATUREZ, "ED_CCJURI")
lTpNatDif   := cTpNatOld <> cTpNatAtu
lTitPriPrc  := Iif(lDesdOld, SE2->E2_DESDOBR == "S" .And. SE2->(Deleted()), SE2->E2_STATUS == "D") // T�tulo digitado pelo usu�rio para ser parcelado (desdobramento do financeiro)
lAlteNat    := cOldNatSE2 <> SE2->E2_NATUREZ // Se ocorreu altera��o de natureza abre a tela de detalhe / desdobramento para preenchimento
lOldTransit := AllTrim(cOldNatSE2)      == cNatTrans
lAtuTransit := AllTrim(SE2->E2_NATUREZ) == cNatTrans
nValOrig    := SE2->E2_VALOR

If !lInclui
	// Se n�o houve altera��o de natureza
	If cOldNatSE2 == SE2->E2_NATUREZ .And. !lAtuTransit
		lRet := J246AltOHF() // Altera os campos (ex. Valor) no detalhe / desdobramento

	// Se o Centro de Custo Jur�dico das naturezas forem diferentes, exclu� os registros de detalhes / desdobramento
	ElseIf lTpNatDif
		lRet := J246DelOHF()
	EndIf
EndIf

// Se for inclus�o de uma parcela de desdobramento financeiro, ent�o N�O abre a nossa tela de desdobramento,
//     pois ser� aberta apenas a uma vez a tela para o t�tulo principal (SE2->E2_STATUS != "D") e 
//     ao salvar o model ser� replicado o desdobramento para as parcelas igualmente.
// Foi utilizado o FwisInCallStack, pois n�o foi poss�vel utilizar o campo E2_DESDOBR para indentificar a parcela
//     o financeiro inclui a parcela com E2_DESDOBR = 'N', mas depois ele altera a SE2 para E2_DESDOBR = 'S'
lPodParTit := !FwIsInCallStack("GeraParcSe2")

If lInclui .And. lTitPriPrc .And. lPodParTit

	// Se � o t�tulo digitado pelo usu�rio para ser parcelado (desdobramento), ent�o s� ir� abrir a nossa tela
	// de desdobramento se for no momento de inclus�o do mesmo e s� vamos abrir a tela quando o
	// par�metro MV_NRASDSD utilizar a forma nova de parcelar, onde preenche os dados na FI8 para
	// rastrear os t�tulos gerados pelo parcelamento
	lPodParTit := lPodParTit .And. !lDesdOld

	If lPodParTit
		cTmpSE2 := J246ParcFi() // Retrona �rea com o Recno e Valor de cada parcela da SE2 posicionada.

		If lAtuTransit .And. (cTmpSE2)->(!Eof())
			// Se foi um parcelamento e a natureza for transit�ria de pagamento, n�o vamos replicar os desdobramento, pois os valores gerados nos desdobramentos (JURA246) v�o ficar quebrados e n�o v�o bater com o t�tulo
			// Se o desdobramento (financeiro) foi para replicar o t�tulo, vamos perguntar se o usu�rio quer replicar os desdobramento informados, sen�o, vamos abrir o desdobramento (JURA246) da primeira parcela
			If (cTmpSE2)->FI8_VALOR != nValOrig .Or. !ApMsgYesNo(STR0061) // "Os desdobramentos informados a seguir ser�o destinados � primeira parcela gerada. Deseja replicar para as demais parcelas?"
				SE2->(DbGoTo((cTmpSE2)->RECNO))
			EndIf
		EndIf
		(cTmpSE2)->(dbCloseArea())
	EndIf
EndIf

If lRet .And. lAlteNat .And. lPodParTit
	JURA246(nOpc, .T., lConfirma, lAtuTransit, .F., .T.)
EndIf

SE2->(dbGoto(nRecno))
RestArea(aArea)

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J246DelOHF()
Realiza a exclus�o dos detalhes / desdobramentos quando houver
altera��o de natureza no t�tulo, ou quando for PA - "Pagamento Adiantado".

@author Jorge Martins / Cristina Cintra
@since 13/03/18
@version 1.0
/*/
//-------------------------------------------------------------------
Function J246DelOHF(nSE2Recno)
Local oModel      := FWLoadModel("JURA246")
Local oModelOHF   := Nil
Local nQtdOHF     := 0
Local nLine       := 0
Local lRet        := .T.
Default nSE2Recno := 0

	If nSE2Recno != 0
		SE2->(dbGoTo(nSE2Recno))
	EndIf

	oModel:SetOperation(�MODEL_OPERATION_UPDATE�)
	oModel:Activate()
	oModelOHF := oModel:GetModel("OHFDETAIL")

	nQtdOHF := oModelOHF:GetQtdLine()

	For nLine := 1 To nQtdOHF
		oModelOHF:GoLine( nLine )
		oModelOHF:DeleteLine()
	Next

	// Executa estorno da contabiliza��o na exclus�o de TODAS as linhas do desdobramento
	If nQtdOHF > 0 .And. FindFunction("JURA265B") .And. OHF->(ColumnPos("OHF_DTCONI")) > 0 .And. VerPadrao("948")
		JURA265B("948")
	EndIf

	lRet := J246Commit(oModel)

	oModel:DeActivate()

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J246AltOHF()
Realiza as altera��es nos detalhes / desdobramentos quando houver
altera��es no t�tulo e n�o for necess�rio abrir a tela de
detalhes / desdobramentos.

@author Jorge Martins
@since 13/03/18
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function J246AltOHF()
Local oModel    := FWLoadModel("JURA246")
Local oModelOHF := Nil
Local nQtdOHF   := 0
Local lRet      := .T.

	oModel:SetOperation(�MODEL_OPERATION_UPDATE�)
	oModel:Activate()
	oModelOHF := oModel:GetModel("OHFDETAIL")

	nQtdOHF := oModelOHF:GetQtdLine()

	// Atualiza o campo de valor somente se existir "1" detalhe / desdobramento
	If AllTrim(cOldNatPFS) == AllTrim(SE2->E2_NATUREZ) .And. nQtdOHF == 1
		oModelOHF:GoLine( 1 )

		If !Empty(oModelOHF:GetValue("OHF_CNATUR"))
			oModelOHF:SetValue("OHF_VALOR", JCPVlBruto(SE2->(Recno())))

			lRet := J246Commit(oModel)
		EndIf
	EndIf

	oModel:DeActivate()

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JA246VldCal
Valida��o Calend�rio Cont�bil x Lan�amentos

@param oModel Modelo de dados de lan�amentos

@author Anderson Carvalho
@since 05/04/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function JA246VldCal(oModel)
Local lRet      := .T.
Local lCalBlock := .F.
Local cFilAtu   := cFilAnt
Local nI        := 1
Local nLine     := 1
Local aStruct   := {}
Local oModelOHF := oModel:GetModel("OHFDETAIL")
Local cCpoLiber := ""
Local cCampo    := ""
Local cTitulo   := ""
Local oStruct   := FWFormStruct( 2, "OHF" )
Local nOHFQtdLn := 0

cCpoLiber := "OHF_CPART2|OHF_DPART2|"+;
				"OHF_CRATEI|OHF_DRATEI|"+;
					"OHF_CCLIEN|OHF_CLOJA|OHF_DCLIEN|OHF_CCASO|OHF_CRATEI|OHF_DRATEI|OHF_DCASO|OHF_CTPDSP|OHF_DTPDSP|OHF_QTDDSP|OHF_CCUSTO|OHF_DCUSTO|"+;
						"OHF_CESCR|OHF_DESCR|OHF_SIGLA2|OHF_CPART2|OHF_DPART2|OHF_COBRA|OHF_DTDESP"

cFilAnt := oModel:GetValue("SE2MASTER", "E2_FILIAL")

lCalBlock := !(CtbValiDt(, oModel:GetValue("OHFDETAIL", "OHF_DTINCL"), .F.,,, {"PFS001"},))

If lCalBlock
	aStruct := oStruct:GetFields()
	nQtdStruct := Len(aStruct)
	nOHFQtdLn := oModelOHF:GetQtdLine()
	For nLine := 1 To nOHFQtdLn
		If oModelOHF:IsInserted(nLine) .OR. oModelOHF:IsDeleted(nLine)
			lRet := .F.
			Exit
		Else
			For nI := 1 To nQtdStruct
				cCampo  := aStruct[nI][1]
				If (cCampo == "OHF_SIGLA") .And. oModelOHF:IsFieldUpdated(cCampo, nLine)
					lRet := .F.
					cTitulo := I18n(STR0031, {aStruct[nI][MODEL_FIELD_IDFIELD]}) //# "O Calend�rio Cont�bil esta bloqueado e o campo '#1' n�o pode ser alterado."
					Exit
				Else
					If !(cCampo $ cCpoLiber) .And. oModelOHF:IsFieldUpdated(cCampo, nLine)
						lRet := .F.
						cTitulo := I18n(STR0031, {aStruct[nI][MODEL_FIELD_IDFIELD]}) //# "O Calend�rio Cont�bil esta bloqueado e o campo '#1' n�o pode ser alterado."
						Exit
					EndIf
				EndIf
			Next nI
		EndIf
	Next nLine
EndIf

If !lRet
	JurMsgErro(Iif(Empty(cTitulo), STR0029, cTitulo),, I18n(STR0030, {cFilAnt})) //"Calend�rio Cont�bil bloqueado." -- "Verifique o bloqueio do processo 'PFS001' no Calend�rio Cont�bil da filial '#1', para o per�odo da data do lan�amento."
EndIf

cFilAnt := cFilAtu

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J246IncOHF
Inclui um desdobramento para o t�tulo de PA - Pagamento adiantado ou
para t�tulos de TX - Impostos

@param nRecnoSE2    Recno do t�tulo SE2
@param cTipo        Tipo de desdobramento "PA" - Pagamento antecipado; "TX" - Impostos

@author Bruno Ritter
@since 26/04/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Function J246IncOHF(nRecnoSE2, cTipo)
	Local lRet       := .T.
	Local oModel     := Nil
	Local oModelOHF  := Nil
	Local cNatDesdob := ""
	Local cHistoric  := ""
	Local cItem      := StrZero(1, TamSx3("OHF_CITEM")[1])
	Local cNatSE2    := ""
	Local cCodOHP    := ""
	Local nValorSE2  := JCPVlBruto(nRecnoSE2)
	Local nRecOld    := SE2->(Recno())

	Default cTipo := "PA"

	SE2->(DbGoTo(nRecnoSE2))

	If cTipo == "PA"
		cNatDesdob := JurBusNat("7",,,, .T.)
		cHistoric  := STR0032 + " - " + AllTrim(SE2->E2_FORNECE) + "/" + AllTrim(SE2->E2_LOJA) + " - " // "Pagamento Antecipado"
		cHistoric  += Capital(AllTrim(JurGetDados("SA2", 1, xFilial("SA2") + SE2->E2_FORNECE + SE2->E2_LOJA , "A2_NOME")))
		cHistoric  += Iif(!Empty(SE2->E2_HIST), " - " + Capital(AllTrim(SE2->E2_HIST)), "")
	ElseIf cTipo == "TX"
		cNatSE2 := Alltrim(SE2->E2_NATUREZ)
		Do Case
			Case cNatSE2 == StrTran(SuperGetMV("MV_IRF",, ''), '"', '') // IRRF Colocado STRTRAN porque o parametro possui aspas
				cCodOHP := "009"
			Case cNatSE2 == StrTran(SuperGetMV("MV_ISS",, ''), '"', '') // ISS
				cCodOHP := "010"
			Case cNatSE2 == StrTran(SuperGetMV("MV_INSS",, ''), '"', '') // INSS
				cCodOHP := "011"
			Case cNatSE2 == StrTran(SuperGetMV("MV_PISNAT",, ''), '"', '') // PIS
				cCodOHP := "012"
			Case cNatSE2 == StrTran(SuperGetMV("MV_COFINS",, ''), '"', '') // COFINS
				cCodOHP := "013"
			Case cNatSE2 == StrTran(SuperGetMV("MV_CSLL",, ''), '"', '') // CSLL
				cCodOHP := "014"
		EndCase
		If cCodOHP == ""
			cNatDesdob := SE2->E2_NATUREZ
		Else
			cNatDesdob := JurClasNat(cCodOHP)
		EndIf

		cHistoric := STR0042 // "Impostos"
	EndIf

	If Empty(cNatDesdob)
		lRet := .F.
	Else
		// O Modelo deve ser instanciado ap�s o posicionamento da SE2
		oModel    := FWLoadModel("JURA246")
		oModelOHF := oModel:GetModel("OHFDETAIL")
		oModel:SetOperation(MODEL_OPERATION_UPDATE)
		oModel:Activate()

		Iif(lRet, lRet := oModelOHF:LoadValue("OHF_CITEM", cItem     ), Nil)
		Iif(lRet, lRet := oModelOHF:SetValue("OHF_CNATUR", cNatDesdob), Nil)
		Iif(lRet, lRet := oModelOHF:SetValue("OHF_VALOR" , nValorSE2 ), Nil)
		Iif(lRet, lRet := oModelOHF:SetValue("OHF_HISTOR", cHistoric ), Nil)

		lRet := J246Commit(oModel)
		oModel:DeActivate()
	EndIf

	SE2->(DbGoTo(nRecOld))

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J246RetPrj()
Retorna o valor do c�digo do projeto da dialog criada pela fun��o J246DIALOG

@author Bruno Ritter
@since 26/04/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Function J246RetPrj()
Local cRet := ""

If Type("oCodProj") == "O"
	cRet := oCodProj:GetValue()
EndIf

Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J246VldDes()
Valida dados do desdobramento quando a natureza do t�tulo for diferente
de Transit�ria de Pagamento

@param oModel, objeto, Modelo da OHF

@obs Essa valida��o � necess�ria devido a inclus�o de CP com desdobramento 
( servi�o: JurRestCP)

@author bruno.ritter/queizy.nascimento
@since 12/07/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function J246VldDes(oModel)
	Local lRet       := .T.
	Local cNatTit    := AllTrim(oModel:GetValue("SE2MASTER", "E2_NATUREZ")) // Natureza do t�tulo
	Local nValTit    := JCPVlBruto(SE2->(Recno()))
	Local cValtit    := ""
	Local cNatTrans  := AllTrim(JurBusNat("7")) // Natureza Transit�ria de Pagamento
	Local oModelOHF  := Nil

	If cNatTit != cNatTrans
		oModelOHF := oModel:GetModel("OHFDETAIL")
		lRet := oModelOHF:Length(.T.) <= 1

		If !lRet
			JurMsgErro(STR0035, , STR0036)// "Natureza permite adicionar apenas um desdobramento.", "Informe apenas um desdobramento."
		EndIf

		If lRet .And. AllTrim(SE2->E2_TIPO) == "PA"
			lRet := .F.
			JurMsgErro(STR0037, , STR0041)// "Natureza do desdobramento inv�lida!", "A natureza do desdobramento deve ser do tipo 6 - Transit�ria de Pagamento"
		EndIf

		If lRet .And. nValTit != oModelOHF:GetValue("OHF_VALOR")
			lRet    := .F.
			cValtit := AllTrim(Transform(nValTit, PesqPict("SE2", "E2_VALOR")))
			JurMsgErro(STR0039, , I18N(STR0040, {cValTit}))// "Valor do desdobramento inv�lido!", "O valor do desdobramento deve ser igual ao valor do t�tulo #1."
		EndIf
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JA246Desd()
Verifica se existe desdobramento para o t�tulo.

@param cIdDoc    Chave para busca do desdobramento

@author Cristina Cintra
@since 01/08/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function JA246Desd(cIdDoc)
Local aAreaOHF   := OHF->(GetArea())
Local cFilOHF    := xFilial("OHF")
Local lRet       := .T.

OHF->(DbSetOrder(1)) //OHF_FILIAL + OHF_IDDOC + OHF_CITEM
If ! OHF->(DbSeek(cFilOHF + cIdDoc))
	lRet := .F.
EndIf

RestArea(aAreaOHF)

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JA246Anexo()
Anexo de documentos

@param  lView  => .T./.F. - Indica se a chamada foi feita em uma view.
                            Ser� falso quando for a tela de detalhes
                            do contas a pagar.

@return lRet   => .T./.F. - Indica se foi poss�vel anexar documentos.

@author Jorge Martins
@since  21/11/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function JA246Anexo(lView)
Local aAreas     := { OHF->(GetArea()), GetArea() }
Local oModel     := FWModelActive()
Local oView      := FWViewActive()
Local oModelOHF  := oModel:GetModel("OHFDETAIL")
Local nLineOHF   := oModelOHF:GetLine()
Local lRet       := .T.

Default lView    := .T.

	oModelOHF := oModel:GetModel("OHFDETAIL")
	nLineOHF  := oModelOHF:GetLine()

	If lRet := J247VAnexo(oModelOHF, nLineOHF, "OHF_CNATUR") // Verifica que pode anexar nesse desdobramento

		OHF->(dbGoto(oModelOHF:GetDataId())) // Posiciona a tabela para a rotina de anexos

		JURANEXDOC("OHF", "OHFDETAIL", "", "OHF_IDDOC", "", "", "", "", "", "3", "OHF_CITEM", .F., .F., .T.) // Abre tela de anexo de documento

		If lView
			oModelOHF:LoadValue("OHF__ANEXO", J247IcoAnx("OHF") ) // Atualiza a legenda
			oView:Refresh("OHFDETAIL")
		EndIf

	EndIf

AEval( aAreas, {|aArea| RestArea( aArea ) } )

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J246VlLanc
Gera os modelos e valida os lan�amentos gerados pelos desdobramentos
alterados

@param oModel   , Model da JURA246

@return aMdlLanc, Modelos dos lan�amento que devem ser comitados

@author Bruno Ritter
@since 20/12/18
/*/
//-------------------------------------------------------------------
Static Function J246VlLanc(oModel)
	Local oModelOHF   := oModel:GetModel("OHFDETAIL")
	Local lDesdOld    := SuperGetMV("MV_NRASDSD", .F.) // Permite que o desdobramento de t�tulos seja realizado no processo antigo, sem rastreamento e excluindo o t�tulo originador.
	Local lDesdFin    := Iif(lDesdOld, SE2->E2_DESDOBR == "S" .And. SE2->(Deleted()), SE2->E2_STATUS == "D") // Se o t�tulo est� sendo desdobrado pelo financeiro
	Local lExisteBx   := SE2->E2_SALDO != SE2->E2_VALOR //Existe baixa
	Local cChave      := SE2->E2_FILIAL+'|'+SE2->E2_PREFIXO+'|'+SE2->E2_NUM+'|'+SE2->E2_PARCELA+'|'+SE2->E2_TIPO+'|'+SE2->E2_FORNECE+'|'+SE2->E2_LOJA
	Local aLinesChg   := 0
	Local nI          := 0
	Local nY          := 0
	Local nLine       := 0
	Local aMdlLanc    := {}
	Local aRecLanc    := {}
	Local cItem       := ""
	Local nUltimoLanc := 0
	Local lValido     := .T.

	If (lExisteBx .And. !lDesdFin) .And. oModelOHF:IsModified()
		aLinesChg := oModelOHF:GetLinesChanged(MODEL_GRID_LINECHANGED_UPDATED)

		For nI := 1 To Len(aLinesChg)
			nLine := aLinesChg[nI]
			cItem := oModelOHF:GetValue("OHF_CITEM", nLine)
			aRecLanc := J246QrLanc(cChave, cItem)

			For nY := 1 To Len(aRecLanc)
				Aadd( aMdlLanc, J246GrLanc(oModelOHF, nLine, aRecLanc[nY][1]) )

				nUltimoLanc := Len(aMdlLanc)
				If Empty(aMdlLanc[nUltimoLanc])
					lValido := .F.
					JurFreeArr(@aMdlLanc)
					Exit
				EndIf
			Next nY

		Next nI
	EndIf

Return aMdlLanc

//-------------------------------------------------------------------
/*/{Protheus.doc} J246QrLanc
Retorna os lan�amentos de um desdobramento

@param cChave   , Chava da SE1 com separado por '|' acada campo da SE1
@param cItem    , OHF_CITEM do desdobramento para achar o seus lan�amentos

@return aRecLanc, Array com os recnos dos lan�amentos

@author Bruno Ritter
@since 20/12/18
/*/
//-------------------------------------------------------------------
Static Function J246QrLanc(cChave, cItem)
	Local cQuery   := ""
	Local aRecLanc := {}

	cQuery := " SELECT R_E_C_N_O_ FROM " + RetSqlName("OHB") + " OHB "
	cQuery += " WHERE OHB.OHB_FILIAL = '" + xFilial("OHB") + "' "
	cQuery +=   " AND OHB.OHB_CPAGTO = '" + cChave + "' "
	cQuery +=   " AND OHB.OHB_ITDES = '"  + cItem + "' "
	cQuery +=   " AND OHB.D_E_L_E_T_ = ' ' "

	aRecLanc := JurSQL(cQuery, "R_E_C_N_O_")

Return aRecLanc

//-------------------------------------------------------------------
/*/{Protheus.doc} J246GrLanc
Replica os dados do desdobramento para um lan�amento

@param oModelOHF  , O model da OHFDETAIL
@param nLine      , Linha para replicar valores
@param nRecLanc   , Recno do lan�amento que vai receber os valores

@return oModelLanc, Model da JURA241 - Lan�amento com os valores replicados e validado

@obs retorna Nil se o n�o foi poss�vel replicar os valores

@author Bruno Ritter
@since 20/12/18
/*/
//-------------------------------------------------------------------
Static Function J246GrLanc(oModelOHF, nLine, nRecLanc)
	Local oModelLanc := Nil
	Local oModelOHB  := Nil

	OHB->(DbGoTo(nRecLanc))
	oModelLanc := FWLoadModel("JURA241")
	oModelLanc:SetOperation(MODEL_OPERATION_UPDATE)
	oModelLanc:Activate()

	oModelOHB := oModelLanc:GetModel("OHBMASTER")

	JurSetVal(oModelOHB, "OHB_NATDES", "") // Limpa a natureza para limpar os campos de CCJuri para n�o dar problema no when
	JurSetVal(oModelOHB, "OHB_NATDES", oModelOHF:GetValue("OHF_CNATUR", nLine))
	JurSetVal(oModelOHB, "OHB_CESCRD", oModelOHF:GetValue("OHF_CESCR" , nLine))
	JurSetVal(oModelOHB, "OHB_CCUSTD", oModelOHF:GetValue("OHF_CCUSTO", nLine))
	JurSetVal(oModelOHB, "OHB_SIGLAD", oModelOHF:GetValue("OHF_SIGLA2", nLine))
	JurSetVal(oModelOHB, "OHB_CTRATD", oModelOHF:GetValue("OHF_CRATEI", nLine))
	JurSetVal(oModelOHB, "OHB_CCLID" , oModelOHF:GetValue("OHF_CCLIEN", nLine))
	JurSetVal(oModelOHB, "OHB_CLOJD" , oModelOHF:GetValue("OHF_CLOJA" , nLine))
	JurSetVal(oModelOHB, "OHB_CCASOD", oModelOHF:GetValue("OHF_CCASO" , nLine))
	JurSetVal(oModelOHB, "OHB_CTPDPD", oModelOHF:GetValue("OHF_CTPDSP", nLine))
	JurSetVal(oModelOHB, "OHB_QTDDSD", oModelOHF:GetValue("OHF_QTDDSP", nLine))
	JurSetVal(oModelOHB, "OHB_COBRAD", oModelOHF:GetValue("OHF_COBRA" , nLine))
	JurSetVal(oModelOHB, "OHB_DTDESP", oModelOHF:GetValue("OHF_DTDESP", nLine))
	JurSetVal(oModelOHB, "OHB_SIGLA" , oModelOHF:GetValue("OHF_SIGLA" , nLine))
	JurSetVal(oModelOHB, "OHB_VALOR" , oModelOHF:GetValue("OHF_VALOR" , nLine))
	JurSetVal(oModelOHB, "OHB_CHISTP", oModelOHF:GetValue("OHF_CHISTP", nLine))
	JurSetVal(oModelOHB, "OHB_HISTOR", oModelOHF:GetValue("OHF_HISTOR", nLine))
	JurSetVal(oModelOHB, "OHB_CPROJE", oModelOHF:GetValue("OHF_CPROJE", nLine))
	JurSetVal(oModelOHB, "OHB_CITPRJ", oModelOHF:GetValue("OHF_CITPRJ", nLine))

	If oModelLanc:HasErrorMessage()
		aErro := oModelLanc:GetErrorMessage()

		JurMsgErro(STR0029,, Alltrim(aErro[7])) // "Erro ao atualizar lan�amento: "
		oModelLanc:Destroy()
		oModelLanc := Nil

	ElseIf !oModelLanc:VldData()
		aErro := oModelLanc:GetErrorMessage()

		JurMsgErro(STR0029,, Alltrim(aErro[7])) // "Erro ao atualizar lan�amento: "
		oModelLanc:Destroy()
		oModelLanc := Nil
	EndIf

Return oModelLanc

//-------------------------------------------------------------------
/*/{Protheus.doc} J246VldPro()
Rotina de dicion�rio para validar o projeto, considerando
se a situa��o esta dirente de '2'.

@param cProjeto codigo do projeto a ser validado

@author Luciano Pereira dos Santos
@since   14/01/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Function J246VldPro(cProjeto)
	Local lRet    := .T.
	Local lValBlq := .T.

	lRet := JurVldProj(cProjeto, "2", lValBlq)

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} J246DesFin
Processamento para t�tulos que foram desdobrados (parcelado) pelo financeiro

@param oModel    , modelo principal da JURA246
@param aModelDesp, Array de modelos vinculados ao desdobramento
@param aModelLanc, Array de modelos vinculados ao desdobramento
@param aModelNZQ , Array de modelos vinculados ao desdobramento

@return Nil

@author  Bruno Ritter
@since   07/11/2019
/*/
//-------------------------------------------------------------------
Static Function J246DesFin(oModel, aModelDesp, aModelLanc, aModelNZQ)
	Local oModelOHF  := oModel:GetModel("OHFDETAIL")
	Local nTotalOHF  := oModelOHF:Length()
	Local cNaoCopiar := "OHF_FILIAL|OHF_IDDOC|OHF_CDESP|OHF_NZQCOD|OHF_CODLD"
	Local lDesdOld   := SuperGetMV("MV_NRASDSD", .F.) // Permite que o desdobramento de t�tulos seja realizado no processo antigo, sem rastreamento e excluindo o t�tulo originador.
	Local lDesdFin   := Iif(lDesdOld, SE2->E2_DESDOBR == "S" .And. SE2->(Deleted()), SE2->E2_STATUS == "D") // Se o t�tulo est� sendo desdobrado pelo financeiro
	Local aAreaSE2   := {}
	Local cTmpSE2    := ""
	Local nRecAtual  := SE2->(Recno())
	Local oModelParc := Nil
	Local oMdlPrcOHF := Nil
	Local aFieldsOHF := {}
	Local nCpo       := 0
	Local cCampo     := ""
	Local xValor     := Nil
	Local nLine      := 0
	Local pPerc      := Nil // Percentual em ponto flutuante entre a parcela e o t�tulo original
	Local pVlDesd    := Nil
	Local pVlParc    := Nil
	Local pVlTitOrig := Nil // Valor em ponto flutuante do t�tulo digitado pelo usu�rio
	Local pVlTitParc := Nil // Valor em ponto flutuante da parcela
	Local nDecValor  := 0   // Casas decimais do campo OHF_VALOR
	Local nDecimal   := 18  // Uso de casas decimais no ponto flutuante
	Local aDesdPerc  := {}  // Percentual que o desdobramento vale no t�tulo
	Local nDesdOri   := 0

	If lDesdFin .And. oModelOHF:Length(.T.) > 0
		JurFreeArr(@aModelDesp)
		JurFreeArr(@aModelLanc)
		JurFreeArr(@aModelNZQ)

		aAreaSE2   := SE2->(GetArea())
		nDecValor  := TamSX3("OHF_VALOR")[2]
		pVlTitOrig := DEC_CREATE(cValToChar(SE2->E2_VALOR), 64, nDecimal)

		For nDesdOri := 1 To nTotalOHF
			pVlDesd := DEC_CREATE(cValToChar(oModelOHF:GetValue("OHF_VALOR", nDesdOri)), 64, nDecimal)
			pPerc   := DEC_DIV(pVlDesd, pVlTitOrig)
			aAdd(aDesdPerc, pPerc)
		Next nDesdOri

		aFieldsOHF := oModelOHF:GetStruct():GetFields()

		cTmpSE2 := J246ParcFi() // Retrona �rea com o Recno e Valor de cada parcela da SE2 posicionada.

		While (cTmpSE2)->(!Eof())
			SE2->(DbGoTo((cTmpSE2)->RECNO))
			oModelParc := FwLoadModel("JURA246") // Recriao modelo, pois ele � montado conforme a SE2 posicionada, ent�o n�o podemos aproveitar o modelo criado anteriormente
			oModelParc:SetOperation(MODEL_OPERATION_UPDATE)

			If oModelParc:CanActivate() .And. oModelParc:Activate()
				oMdlPrcOHF := oModelParc:GetModel("OHFDETAIL")
				pVlTitParc := DEC_CREATE(cValToChar(SE2->E2_VALOR), 64, nDecimal)

				For nLine := 1 To nTotalOHF
					If !oModelOHF:IsDeleted(nLine)
						pPerc := aDesdPerc[nLine]

						For nCpo := 1 To Len(aFieldsOHF)
							If !aFieldsOHF[nCpo][14] // Campo N�O � virtual
								cCampo := AllTrim(aFieldsOHF[nCpo][3])

								If oMdlPrcOHF:HasField(cCampo) .And. !(cCampo $ cNaoCopiar)
									If cCampo == "OHF_VALOR"
										pVlParc := DEC_MUL(pVlTitParc, pPerc)
										xValor  := Val(cValToChar(DEC_RESCALE(pVlParc, nDecValor, 0)))
									Else
										xValor := oModelOHF:GetValue(cCampo, nLine)
									EndIf

									oMdlPrcOHF:LoadValue(cCampo, xValor) // J� foi feita as valida��es, poir isso o loadvalue
								EndIf
							EndIf
						Next nCpo
					EndIf

					oMdlPrcOHF:AddLine()
				Next nLine

				oMdlPrcOHF:DeleteLine()
				// Valida sem pegar o retorno, pois j� foi validado anteriormente,
				// mas � necess�rio passar pelas fun��es de valid para carregar os atributos do objeto FWModelEvent antes de comitar o modelo
				oModelParc:VldData()
				oModelParc:CommitData()
				oModelParc:DeActivate()
				oModelParc:Destroy() // Destroy pois o modelo � montado conforme a SE2 posicionada, ent�o n�o podemos aproveitar o modelo criado
			EndIf

			(cTmpSE2)->(DbSkip())
		EndDo

		(cTmpSE2)->(dbCloseArea())

		SE2->(DbGoTo(nRecAtual))
		oModelOHF:DelAllLine()

		RestArea(aAreaSE2)
	EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} J246ParcFi
Gera uma area tempor�ria com os dados das parcelas geradas pelo
desdobramento do financeiro

@return cTmpSE2, �rea com o Recno e Valor de cada parcela da SE2 posicionada.

@author  Bruno Ritter / Jorge Martins
@since   14/11/2019
/*/
//-------------------------------------------------------------------
Static Function J246ParcFi()
	Local cTmpSE2  := GetNextAlias()

	BeginSql Alias cTmpSE2
		SELECT
			SE2.R_E_C_N_O_ RECNO,
			FI8.FI8_VALOR
		FROM %Table:SE2% SE2
		INNER JOIN %Table:FI8% FI8
				ON FI8.FI8_FILIAL = %xFilial:FI8%
			AND FI8.FI8_PRFORI = %Exp:SE2->E2_PREFIXO%
			AND FI8.FI8_NUMORI = %Exp:SE2->E2_NUM%
			AND FI8.FI8_PARORI = %Exp:SE2->E2_PARCELA%
			AND FI8.FI8_TIPORI = %Exp:SE2->E2_TIPO%
			AND FI8.FI8_FORORI = %Exp:SE2->E2_FORNECE%
			AND FI8.FI8_LOJORI = %Exp:SE2->E2_LOJA%
			AND FI8.%NotDel%
		WHERE SE2.E2_FILIAL    = FI8.FI8_FILDES
			AND SE2.E2_PREFIXO = FI8.FI8_PRFDES
			AND SE2.E2_NUM     = FI8.FI8_NUMDES
			AND SE2.E2_PARCELA = FI8.FI8_PARDES
			AND SE2.E2_TIPO    = FI8.FI8_TIPDES
			AND SE2.E2_FORNECE = FI8.FI8_FORDES
			AND SE2.E2_LOJA    = FI8.FI8_LOJDES
			AND SE2.%NotDel%
		ORDER BY SE2.E2_PARCELA
	EndSql

Return cTmpSE2

//-------------------------------------------------------------------
/*/{Protheus.doc} J246EstCtb
Fun��o que chama o estorno da contabiliza��o do desdobramento/desd. p�s pag.
quando j� contabilizado e houve altera��o ou exclus�o.

@Param oMdlDes , Objeto, Modelo de dados de desdobramentos/desd. p�s pag.
@Param  cTab   , caractere, Tabela do desdobramento "OHF" ou desdobramento p�s pag. "OHG"
@Param  cCodLP , caractere, C�digo do lan�amento padr�o de estorno "948" ou "949"

@author Jonatas Martins
@since  14/10/2019
@Obs    Nesse ponto est� posicionado na linha da OHF que sofreu modifica��o
/*/
//-------------------------------------------------------------------
Function J246EstCtb(oMdlDes, cTab, cCodLP)
	Local nRecLine  := 0
	Local lDeleted  := .F.
	Local lModified := .F.
	Local lReversal := .F.
	Local cCpoFlag  := ""

	Default oMdlDes := Nil
	Default cTab    := ""
	Default cCodLP  := ""

	If ValType(oMdlDes) == "O" .And. !Empty(cTab) .And. !Empty(cCodLP)
		cCpoFlag := J265LpFlag(cCodLP) // Busca campo de flag da contabiliza��o
		
		If !Empty(oMdlDes:GetValue(cCpoFlag)) // Verifica se o registro est� contabilizado "947"
			lDeleted  := oMdlDes:IsDeleted()
			lModified := lDeleted .Or. J246IsUpdLin(oMdlDes, cTab)
			If lModified
				nRecLine  := oMdlDes:GetDataID()
				lReversal := JURA265B(cCodLP, nRecLine) // Estorno da contabiliza��o de desdobramento/desd. p�s pagamento
				If lReversal .And. !lDeleted
					If cCodLP == "948"
						AAdd(_aRecDesCtb, nRecLine) // Array est�tico utilizado no m�todo INTTS para nova contabiliza��o
					Else // Desdobramento P�s pagamento
						J247SetEst(nRecLine) // Alimenta array _aRecPosCtb est�tico do fonte JURA247
					EndIf
				EndIf
			EndIf
		EndIf
	EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} J246IsUpdLin
Avalia a altera��o de dados do desdobramentos

@Param  oMdlDes  , Objeto   , Modelo de dados de desdobramentos/desd. p�s pagamento
@Param  cTab     , caractere, Tabela do desdobramento "OHF" ou desdobramento p�s pag. "OHG"

@Return lModified, logico   , Se .T. o desdobramento foi modificados

@author Luciano Pereira dos Santos
@since  14/10/2019
@Obs    N�o utilizado o m�todo IsFieldUpdated pois h� situa��es
        que o campo n�o foi alterado e o m�todo retorna .T.
/*/
//-------------------------------------------------------------------
Static Function J246IsUpdLin(oMdlDes, cTab)
	Local aFields   := {cTab + "_CNATUR", cTab + "_VALOR", cTab + "_CCLIEN", cTab + "_CLOJA", cTab + "_CTPDSP"}
	Local cChave    := SE2->E2_FILIAL + "|" + SE2->E2_PREFIXO + "|" + SE2->E2_NUM + "|" + SE2->E2_PARCELA + "|" + SE2->E2_TIPO + "|" + SE2->E2_FORNECE + "|" +SE2->E2_LOJA
	Local cIdDoc    := FINGRVFK7("SE2", cChave)
	Local aValues   := {}
	Local cValue    := ""
	Local nFld      := 0
	Local lModified := .F.

	AEval(aFields, {|cField| xValue := oMdlDes:GetValue(cField), AAdd(aValues, {xValue, ValType(xValue)})})

	cQuery := "SELECT " + cTab + "_IDDOC"
	cQuery +=  " FROM " + RetSqlName(cTab)
	cQuery += " WHERE " + cTab + "_FILIAL = '" + xFilial(cTab) + "'"
	cQuery +=   " AND " + cTab + "_IDDOC = '" + cIdDoc + "' AND "
	For nFld := 1 To Len(aFields)
		cValue := J246ConvVal(aValues[nFld][1], aValues[nFld][2])
		cQuery += aFields[nFld] + " = " + cValue + " AND "
	Next nFld
	cQuery += "D_E_L_E_T_ = ' '"

	aRetSql := JurSQL(cQuery, "*")

	// Avalia se o registro permanece inalterado no banco de dados
	lModified := Empty(aRetSql)

Return (lModified)

//-------------------------------------------------------------------
/*/{Protheus.doc} J246ConvVal
Fun��o para converter dados como texto para uso em query

@Param  xValue, indefinido, Valor a ser convertido
@Param  cType , caractere , Tipo de dado a ser convertido

@Return cValue, caractere, Valor convertido como texto

@author Jonatas Martins
@since  14/10/2019
@Obs    N�o utilizado o m�todo IsFieldUpdated pois h� situa��es
        que o campo n�o foi alterado e o m�todo retorna .T.
/*/
//-------------------------------------------------------------------
Function J246ConvVal(xValue, cType)
	Local cValue := ""

	Do Case
		Case cType == "N"
			cValue := AllTrim(Str(xValue))
		
		Case cType == "D"
			cValue := "'" + DtoS(xValue) + "'"
		
		OtherWise // Caractere
			cValue := "'" + xValue + "'"
	End Case

Return (cValue)