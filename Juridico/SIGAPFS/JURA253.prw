#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "JURA253.CH"

//------------------------------------------------------------------------------
/*/{Protheus.doc} JURA253
Modelo de dados da rotina CTBA010 - Calend�rio Cont�bil.

@author	 Cristina Cintra
@since   30/01/2018
@version 1.0
/*/
//------------------------------------------------------------------------------
Function JURA253()
Local oBrowse := Nil

oBrowse := FWMBrowse():New()	
oBrowse:SetDescription(STR0001) // "Modelo de Calend�rio Cont�bil"
oBrowse:SetAlias("CTG")
oBrowse:SetLocate()
oBrowse:Activate()
	
Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menu Funcional

@return aRotina - Estrutura
[n,1] Nome a aparecer no cabecalho
[n,2] Nome da Rotina associada
[n,3] Reservado
[n,4] Tipo de Transa��o a ser efetuada:
1 - Pesquisa e Posiciona em um Banco de Dados
2 - Simplesmente Mostra os Campos
3 - Inclui registros no Bancos de Dados
4 - Altera o registro corrente
5 - Remove o registro corrente do Banco de Dados
6 - Altera��o sem inclus�o de registros
7 - C�pia
8 - Imprimir
[n,5] Nivel de acesso
[n,6] Habilita Menu Funcional

@author	 Cristina Cintra
@since   30/01/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
Local aRotina := {}

aAdd( aRotina, { STR0002, "VIEWDEF.JURA253", 0, 2, 0, NIL } ) // "Visualizar"

Return aRotina

//------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Monta modelo de dados da Atualiza��o de Or�amentos

@return		oModel, objeto, Modelo de Dados

@author	 Cristina Cintra
@since   30/01/2018
@version 1.0
/*/
//------------------------------------------------------------------------------
Static Function ModelDef()
Local oModel      := Nil
Local oStructCQD  := FWFormStruct( 1, "CQD" )
Local oCommit     := JA253COMMIT():New()
Local aData       := TamSx3('CTG_DTINI')

                  // Titulo , Descricao,  Campo      , Tipo do campo  , Tamanho   , Decimal ,  bValid ,  bWhen   , Lista , lObrigat,  bInicializador                                                                                            , � chave, � edit�vel , � virtual
oStructCQD:AddField( STR0005, STR0005  , 'CQD__DTINI', aData[3]       , aData[1]  , aData[2] ,        ,  {||.F.} ,       ,         , {|| POSICIONE('CTG', 1, xFilial('CTG')+ CQD->CQD_CALEND + CQD->CQD_EXERC + CQD->CQD_PERIOD, 'CTG_DTINI') } ,        ,            , .T.       ) // 'Ano M�s Ini'
oStructCQD:AddField( STR0006, STR0006  , 'CQD__DTFIM', aData[3]       , aData[1]  , aData[2] ,        ,  {||.F.} ,       ,         , {|| POSICIONE('CTG', 1, xFilial('CTG')+ CQD->CQD_CALEND + CQD->CQD_EXERC + CQD->CQD_PERIOD, 'CTG_DTFIM') } ,        ,            , .T.       ) // 'Ano M�s Fim'

// Cria o objeto do Modelo de Dados
oModel := MPFormModel():New( 'JURA253',/*bPreValidacao*/, /*bPosValidacao*/, /*bCommit*/, /*bCancel*/ )

oModel:AddFields( 'CQDMASTER', /*cOwner*/, oStructCQD, /*bPre*/, /*bPost*/, /*bLoad*/ )

oModel:SetPrimaryKey({'CQD_CALEND', 'CQD_EXERC', 'CQD_PERIOD', 'CQD_PROC'})

oModel:SetDescription(STR0001) // "Modelo de Calend�rio Cont�bil"

Return ( oModel )

//------------------------------------------------------------------------------
/*/{Protheus.doc} JA253COMMIT
Classe interna implementando o FWModelEvent, para execu��o de fun��o 
durante o commit.

@author	 Cristina Cintra
@since   30/01/2018
@version 1.0
/*/
//------------------------------------------------------------------------------
Class JA253COMMIT FROM FWModelEvent

	Method New()
	Method ModelPosVld()
	
End Class

//------------------------------------------------------------------------------
/*/{Protheus.doc} New
M�todo construtor FWModelEvent

@author	 Cristina Cintra
@since   30/01/2018
@version 1.0
/*/
//------------------------------------------------------------------------------
Method New() Class JA253COMMIT
Return Nil

//------------------------------------------------------------------------------
/*/{Protheus.doc} ModelPosVld
M�todo que � chamado pelo MVC quando ocorrer as a��es de pos valida��o do Modelo.

@author	 Cristina Cintra
@since   30/01/2018
@version 1.0
@Obs     Valida��o criada para n�o permitir as opera��o de PUT e POST do REST
/*/
//------------------------------------------------------------------------------
Method ModelPosVld(oModel, cModelId) Class JA253COMMIT
Local nOperation := oModel:GetOperation()
Local lPosVld    := .T.
	
If nOperation <> MODEL_OPERATION_VIEW
	oModel:SetErrorMessage(,, oModel:GetId(),, "ModelPosVld", STR0003, STR0004,, ) // "Opera��o n�o permitida" # "Essa rotina s� permite a opera��o de visualiza��o!"
	lPosVld := .F.
EndIf
	
Return lPosVld