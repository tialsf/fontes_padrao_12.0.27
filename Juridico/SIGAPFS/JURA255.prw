#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "JURA255.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} SchedDef
Par�metros para execu��o via Schedule.

@author  Bruno Ritter | Jorge Martins
@since   09/02/2018
@version 1
/*/
//-------------------------------------------------------------------
Static Function SchedDef()
Local aOrd   := {}
Local aParam := {}

aParam := { "P"       ,; // Tipo R para relatorio P para processo
            "PARAMDEF",; // Pergunte do relatorio, caso nao use passar ParamDef
            ""        ,; // Alias
            aOrd      ,; // Array de ordens
          }
Return aParam

//------------------------------------------------------------------------------
/*/{Protheus.doc} JURA255
Posi��o Hist�rica do Contas a Receber.

@author  Cristina Cintra
@since   05/02/2018
@version 1.0
/*/
//------------------------------------------------------------------------------
Function JURA255()
Local oBrowse := Nil

oBrowse := FWMBrowse():New()
oBrowse:SetDescription(STR0001) // "Posi��o Hist�rica do Contas a Receber"
oBrowse:SetAlias("OHH")
oBrowse:SetLocate()
oBrowse:Activate()
	
Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menu Funcional

@return aRotina - Estrutura
[n,1] Nome a aparecer no cabecalho
[n,2] Nome da Rotina associada
[n,3] Reservado
[n,4] Tipo de Transa��o a ser efetuada:
1 - Pesquisa e Posiciona em um Banco de Dados
2 - Simplesmente Mostra os Campos
3 - Inclui registros no Bancos de Dados
4 - Altera o registro corrente
5 - Remove o registro corrente do Banco de Dados
6 - Altera��o sem inclus�o de registros
7 - C�pia
8 - Imprimir
[n,5] Nivel de acesso
[n,6] Habilita Menu Funcional

@author  Cristina Cintra
@since   05/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
Local aRotina := {}

aAdd( aRotina, { STR0002, "VIEWDEF.JURA255", 0, 2, 0, NIL } ) // "Visualizar"

Return aRotina

//------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Monta o modelo de dados da Posi��o Hist�rica do Contas a Receber.

@return  oModel, objeto, Modelo de Dados

@author  Cristina Cintra
@since   05/02/2018
@version 1.0
/*/
//------------------------------------------------------------------------------
Static Function ModelDef()
Local oModel      := Nil
Local oStructOHH  := FWFormStruct(1, "OHH")
Local oCommit     := JA255COMMIT():New()
		
oModel := MPFormModel():New("JURA255")
	
oModel:AddFields("OHHMASTER",, oStructOHH)
oModel:InstallEvent("JA255COMMIT",, oCommit)

oModel:SetDescription(STR0001) // "Posi��o Hist�rica do Contas a Receber"
 
Return ( oModel )

//------------------------------------------------------------------------------
/*/{Protheus.doc} JA255COMMIT
Classe interna implementando o FWModelEvent, para execu��o de fun��o 
durante o commit.

@author  Cristina Cintra
@since   05/02/2018
@version 1.0
/*/
//------------------------------------------------------------------------------
Class JA255COMMIT FROM FWModelEvent

	Method New()
	Method ModelPosVld()
	Method InTTS()
	
End Class

//------------------------------------------------------------------------------
/*/{Protheus.doc} New
M�todo construtor FWModelEvent

@author	 Cristina Cintra
@since   05/02/2018
@version 1.0
/*/
//------------------------------------------------------------------------------
Method New() Class JA255COMMIT
Return Nil

//------------------------------------------------------------------------------
/*/{Protheus.doc} ModelPosVld
M�todo que � chamado pelo MVC quando ocorrer as a��es de pos valida��o do Modelo.

@author  Cristina Cintra
@since   05/02/2018
@version 1.0
@Obs     Valida��o criada para n�o permitir as opera��o de PUT e POST do REST
/*/
//------------------------------------------------------------------------------
Method ModelPosVld(oModel, cModelId) Class JA255COMMIT
Local nOperation := oModel:GetOperation()
Local lPosVld    := .T.
	
If nOperation <> MODEL_OPERATION_VIEW
	oModel:SetErrorMessage(,, oModel:GetId(),, "ModelPosVld", STR0003, STR0004,, ) // "Opera��o n�o permitida" # "Essa rotina s� permite a opera��o de visualiza��o!"
	lPosVld := .F.
EndIf
	
Return lPosVld

//------------------------------------------------------------------------------
/*/{Protheus.doc} InTTS
M�todo que � chamado pelo MVC quando ocorrer as a��es do commit Ap�s as grava��es
por�m antes do final da transa��o

@author		Nivia Ferreira
@since		07/02/2018
@version	12.1.20
/*/
//------------------------------------------------------------------------------
Method InTTS(oSubModel, cModelId) Class JA255COMMIT
	Local lIntFinanc  := SuperGetMV("MV_JURXFIN",, .F.)
	
	If lIntFinanc 
		JFILASINC(oSubModel:GetModel(),"OHH", "OHHMASTER", "OHH_PREFIX", "OHH_NUM", "OHH_PARCEL") // Fila de sincroniza��o
	EndIf
	
Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} JURA255S
Chamada via schedule para atualizar a posi��o do contas a receber referente ao ano-m�s atual

@author Bruno Ritter
@since 07/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Function JURA255S(aParam)
Local cEmp  := ""
Local cFil  := ""
Local cUser := ""

	If !Empty(aParam) .And. Len(aParam) >= 3
		cEmp  := aParam[1]
		cFil  := aParam[2]
		cUser := aParam[3]

		RPCSetType(3) // Prepara o ambiente e n�o consome licen�a
		RPCSetEnv(cEmp, cFil, , , , "JURA255S")
		__cUserID := cUser
		J255UpdHis()
	EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} JURA255M
Chamada via menu para atualizar a posi��o do contas a receber referente ao ano-m�s atual

@author Bruno Ritter
@since 07/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Function JURA255M()
	Processa({|| J255UpdHis() }, STR0008) //  "Atualizando a posi��o do contas a receber..."
Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} J255UpdHis
Atualiza a posi��o do contas a receber referente ao ano-m�s atual.

@author Bruno Ritter | Jorge Martins
@since 06/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Function J255UpdHis()
Local cQuery     := ""
Local cQryRes    := ""
Local nTotal     := 0
Local lViaTela   := !IsInCallStack("CheckTask") .And. !IsBlind() // Se n�o for Schedule e n�o for execu��o autom�tica
Local lIntFinanc := SuperGetMV("MV_JURXFIN",, .F.)
Local lExecuta   := .T.
Local lExecLote  := .T.
Local nCount     := 0

If FWAliasInDic("OHH")
	If lViaTela .And. !lIntFinanc
		JurMsgErro(STR0010,, STR0011) // "O par�metro MV_JURXFIN deve estar ativo para atualizar a posi��o hist�rica do contas a receber." "Verifique o par�metro MV_JURXFIN."
		lExecuta := .F.
	EndIf

	If lExecuta .And. lViaTela
		lExecuta := ApMsgYesNo(STR0005, STR0006 ) // "Deseja realmente atualizar a posi��o hist�rica?" // "ATEN��O:"
	EndIf

	If lExecuta
		dbSelectArea( 'OHH' ) // Cria a tabela caso ela n�o exista ainda no banco.

		cQryRes := GetNextAlias()
		cQuery  := J255QryOHH()
		dbUseArea( .T., 'TOPCONN', TcGenQry( ,, cQuery ), cQryRes, .T., .F. )

		BEGIN TRANSACTION
			If lViaTela //Conta a quantidade de registros
				dbSelectArea( cQryRes )
				Count To nTotal
				(cQryRes)->(DbGoTop())

				ProcRegua( nTotal )
			EndIf
		
			While !(cQryRes)->( EOF() )
				nCount++
				J255APosHis((cQryRes)->RECNO, dDataBase, lExecLote)

				(cQryRes)->( dbSkip() )
				If lViaTela
					IncProc( I18n(STR0012, {cValToChar(nCount), cValToChar(nTotal)}) ) //"#1 de #2."
				EndIf
			EndDo
		END TRANSACTION

		(cQryRes)->(DbCloseArea())

		If lViaTela
			If nTotal == 0
				ApMsgInfo( STR0009 ) // "N�o existem registros para atualizar."
			Else
				ApMsgInfo( I18n(STR0007, {cValToChar(nTotal)} ) ) // "Posi��o hist�rica foi atualizada com sucesso para #1 registros."
			EndIf
		EndIf
	EndIf
EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} J255QryOHH()
Gera a query com os t�tulo que devem ser atualizados com posi��o do contas a receber referente ao ano-m�s atual.

@author Bruno Ritter
@since 07/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function J255QryOHH()
Local cQuery   := ""
Local cAnoMes  := AnoMes(dDataBase)

	cQuery := " SELECT SE1.R_E_C_N_O_ RECNO "
	cQuery +=   " FROM " + RetSqlName( "SE1" ) + " SE1 "
	cQuery +=   " LEFT JOIN " + RetSqlName( "OHH" ) + " OHH "
	cQuery +=     " ON  OHH.D_E_L_E_T_ = ' ' "
	cQuery +=    " AND OHH.OHH_FILIAL = SE1.E1_FILIAL "
	cQuery +=    " AND OHH.OHH_PREFIX = SE1.E1_PREFIXO "
	cQuery +=    " AND OHH.OHH_NUM = SE1.E1_NUM "
	cQuery +=    " AND OHH.OHH_PARCEL = SE1.E1_PARCELA "
	cQuery +=    " AND OHH.OHH_TIPO = SE1.E1_TIPO "
	cQuery +=    " AND OHH.OHH_ANOMES = '" + cAnoMes + "' "
	cQuery +=  " WHERE SE1.E1_FILIAL = '" + xFilial( "SE1" ) + "' " 
	cQuery +=    " AND SE1.E1_ORIGEM IN ('JURA203','FINA040') "
	cQuery +=    " AND SE1.E1_TITPAI = '" + Space(TamSx3('E1_TITPAI')[1]) + "' "
	cQuery +=    " AND SE1.E1_SALDO > 0 "
	cQuery +=    " AND OHH.R_E_C_N_O_ IS NULL "
	cQuery +=    " AND SE1.D_E_L_E_T_ = ' ' "
	cQuery +=  " ORDER BY SE1.E1_FILIAL, SE1.E1_PREFIXO, SE1.E1_NUM, SE1.E1_PARCELA, SE1.E1_TIPO " 

	cQuery := ChangeQuery(cQuery, .F.)

Return cQuery

//-------------------------------------------------------------------
/*/{Protheus.doc} J255APosHis()
Atualiza a posi��o hist�rica do Contas a Receber (OHH).

@param nRecno,     Recno do t�tulo (SE1)
@param dDtMov,     Data da movimenta��o
@param lExecLote,  Indica se � uma execu��o em lote
@param aOHIBxAnt,  Valores de honor�rios e despesas utilizados para a fun��o de estorno
@param lGrParcAnt, Executa fun��o para gerar parcelas anteriores
@param nSaldoRet , Saldo no momento do ajuste de base retroativo (usado somente via UPDRASTR)

@author Bruno Ritter | Jorge Martins
@since 08/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Function J255APosHis(nRecno, dNewDtMov, lExecLote, aOHIBxAnt, lGrParcAnt, nSaldoRet)
Local aArea        := {}
Local nSaldo       := 0
Local nAbatimentos := 0
Local cAnoMesOHH   := ""
Local cTpEntr      := ""
Local cMoedaLanc   := ""
Local cOpcOHH      := ""
Local cAnoMesAtu   := AnoMes(Date()) // Date, pois a emiss�o da fatura altera o dDataBase
Local dDtOHH       := dDataBase
Local dDtMov       := Nil
Local dDtEmiSE1    := Nil
Local lInclui      := .F.
Local lEstorno     := FwIsInCallStack("JCancBaixa")
Local aSaldos      := {0, 0, 0, 0}
Local cTitulo      := ""
Local nRecOld      := SE1->(Recno())
Local lDespTrib    := OHH->(ColumnPos("OHH_VLREMB")) > 0
Local lCpoSaldo    := OHH->(ColumnPos("OHH_SALDOH")) > 0
Local lCpoMoeda    := OHH->(ColumnPos("OHH_CMOEDC")) > 0
Local nSomaAbat    := 0

Default dNewDtMov  := Nil
Default lExecLote  := .F.
Default aOHIBxAnt  := {0, 0, 0, 0, 0, 0}
Default lGrParcAnt := .T.
Default nSaldoRet  := 0

If FWAliasInDic("OHH")
	
	SE1->(dbGoTo(nRecno))
	dDtEmiSE1 := SE1->E1_EMISSAO
	// Executa somente se o registro n�o estiver deletado
	If SE1->( ! Eof() ) .And. SE1->( ! Deleted() ) .And. !(AllTrim(SE1->E1_TIPO) $ "RA|PR") // T�tulos n�o tratados

		If !lExecLote
			aArea := GetArea()
			dbSelectArea( 'OHH' ) // Cria a tabela caso ela n�o exista ainda no banco.
		EndIf

		// Guarda o valor de impostos que ser� abatido para uso em relat�rios, visto que h� impostos como o ISS que podem estar sendo apenas destacados
		nSomaAbat := SomaAbat(SE1->E1_PREFIXO, SE1->E1_NUM, SE1->E1_PARCELA, "R", 1,, SE1->E1_CLIENTE, SE1->E1_LOJA, SE1->E1_FILIAL, SE1->E1_EMISSAO, SE1->E1_TIPO)

		// Valor de impostos e abatimentos dos t�tulos - Usado somente se o saldo estiver zerado
		nAbatimentos := Iif(SE1->E1_SALDO == 0, nSomaAbat, 0)

		// Indica se o t�tulo foi digitado manualmente (1) ou se gerado pelo SIGAPFS (2)
		cTpEntr    := Iif(Empty(SE1->E1_JURFAT) .Or. SE1->E1_ORIGEM == "FINA040", "1", "2")
		cMoedaLanc := StrZero(SE1->E1_MOEDA, 2)

		// Se uma data de movimenta��o n�o for indicada, pega do campo E1_MOVIMEN
		If Empty(dNewDtMov)
			dDtMov := Iif(Empty(SE1->E1_MOVIMEN), SE1->E1_EMISSAO, SE1->E1_MOVIMEN)
		Else
			dDtMov := dNewDtMov
		EndIf

		dDtOHH     := Lastday(dDtMov) // Indica o �ltimo dia do m�s (data de fechamento)
		cAnoMesOHH := AnoMes(dDtOHH)

		OHH->(DBSetOrder(1)) // OHH_FILIAL+OHH_PREFIX+OHH_NUM+OHH_PARCEL+OHH_TIPO+OHH_ANOMES

		// Atualiza a posi��o hist�rica para todos os meses (retroativo) at� a data atual
		While cAnoMesOHH <= cAnoMesAtu
			If lCpoSaldo .And. !lExecLote .And. lGrParcAnt
				// Gera parcelas at� o no m�s atual para ratear o valor entre honor�rios e despesas.
				GrvParcAnt(lGrParcAnt, dDtOHH, SE1->E1_PREFIXO, SE1->E1_NUM, SE1->E1_PARCELA, SE1->E1_TIPO)
			EndIf
			cOpcOHH := "" // 3-Inclus�o, 4-Altera��o, 5-Exclus�o.

			If nSaldoRet == 0
				// Se for atualiza��o do m�s atual, deve considerar a data do dia como data do fechamento.
				If cAnoMesOHH == cAnoMesAtu
					dDtOHH := Date() // Date, pois a emiss�o da fatura altera o dDataBase
					nSaldo := SE1->E1_SALDO

				Else // Retroativa - Considera o �ltimo dia do m�s como data do fechamento
					// Retorno o saldo referente a data informada em 'dDtOHH'
					nSaldo := SaldoTit(SE1->E1_PREFIXO, SE1->E1_NUM, SE1->E1_PARCELA, SE1->E1_TIPO,;
					                   SE1->E1_NATUREZ, "R", SE1->E1_CLIENTE, SE1->E1_MOEDA,;
					                   dDtOHH, dDtOHH, SE1->E1_LOJA, SE1->E1_FILIAL, /*nCotacao*/, 1)

					// Quando se trata de impostos (abatimento) o saldotit n�o funciona corretamente por nao tratar tais movimentos de baixa.
					// Com isso se o retorno do saldotit for o mesmo valor de abatimentos, deve-se zerar o saldo.
					If nSaldo == nAbatimentos
						nSaldo := 0
					EndIf

				EndIf
			Else
				nSaldo := nSaldoRet
			EndIf
			cTitulo := SE1->E1_FILIAL + SE1->E1_PREFIXO + SE1->E1_NUM + SE1->E1_PARCELA + SE1->E1_TIPO
			lInclui := !(OHH->(DbSeek(cTitulo + cAnoMesOHH)))

			If nSaldo > 0
				If !Empty(SE1->E1_JURFAT) .And. lCpoSaldo
					If lEstorno
						aSaldos := JEstorno(lInclui, cTitulo, aOHIBxAnt)
					Else
						aSaldos := JGrvSaldo(SE1->E1_JURFAT, cAnoMesOHH, cTitulo, lInclui, dDtEmiSE1)
					EndIf
				EndIf

				RecLock("OHH", lInclui)
				OHH->OHH_FILIAL := SE1->E1_FILIAL
				OHH->OHH_PREFIX := SE1->E1_PREFIXO
				OHH->OHH_NUM    := SE1->E1_NUM
				OHH->OHH_PARCEL := SE1->E1_PARCELA
				OHH->OHH_TIPO   := SE1->E1_TIPO
				OHH->OHH_DTHIST := dDtOHH
				OHH->OHH_JURFAT := SE1->E1_JURFAT
				OHH->OHH_ANOMES := cAnoMesOHH
				OHH->OHH_HIST   := SE1->E1_HIST
				OHH->OHH_CMOEDA := SE1->E1_MOEDA
				OHH->OHH_CCLIEN := SE1->E1_CLIENTE
				OHH->OHH_CLOJA  := SE1->E1_LOJA
				OHH->OHH_CNATUR := SE1->E1_NATUREZ
				OHH->OHH_VALOR  := SE1->E1_VALOR
				OHH->OHH_SALDO  := nSaldo
				OHH->OHH_TPENTR := cTpEntr
				OHH->OHH_VLIRRF := SE1->E1_IRRF
				OHH->OHH_VENCRE := SE1->E1_VENCREA
				OHH->OHH_VLPIS  := SE1->E1_PIS
				OHH->OHH_VLCOFI := SE1->E1_COFINS
				OHH->OHH_VLCSLL := SE1->E1_CSLL
				OHH->OHH_VLISS  := SE1->E1_ISS
				OHH->OHH_VLINSS := SE1->E1_INSS

 				If lCpoSaldo .And. cTpEntr == '2'
					OHH->OHH_NFELET := SE1->E1_NFELETR
					If Len(aSaldos) > 0
						If lInclui
							OHH->OHH_VLFATH := aSaldos[1] // Valor Original de Honor�rios
							OHH->OHH_VLFATD := aSaldos[2] // Valor Original de Despesas
							If lDespTrib
								OHH->OHH_VLREMB := aSaldos[3] // Valor Original de Despesas Reembols�veis
								OHH->OHH_VLTRIB := aSaldos[4] // Valor Original de Despesas Tribut�veis
								OHH->OHH_VLTXAD := aSaldos[5] // Valor Original de Taxa Administrativa
								OHH->OHH_VLGROS := aSaldos[6] // Valor Original de Gross Up
							EndIf
						EndIf

						OHH->OHH_SALDOH := aSaldos[7]  // Saldo de Honor�rios
						OHH->OHH_SALDOD := aSaldos[8]  // Saldo de Despesas
						If lDespTrib
							OHH->OHH_SDREMB := aSaldos[9]  // Saldo de Despesas Reembols�veis
							OHH->OHH_SDTRIB := aSaldos[10] // Saldo de Despesas Tribut�veis
							OHH->OHH_SDTXAD := aSaldos[11] // Saldo de Taxa Administrativa
							OHH->OHH_SDGROS := aSaldos[12] // Saldo de Gross Up
						EndIf
					EndIf
				EndIf

				If lCpoMoeda
					OHH->OHH_CMOEDC := AllTrim(Str(SE1->E1_MOEDA))
				EndIf

				If OHH->(ColumnPos( "OHH_ABATIM" )) > 0
					OHH->OHH_ABATIM := nSomaAbat
				EndIf

				OHH->(MsUnLock())

				cOpcOHH := Iif(lInclui, "3", "4")

			ElseIf !lInclui .And. nSaldo == 0 // Deleta os registros encontrados que est�o sem saldo.
				Reclock( "OHH", .F. )
				OHH->(dbDelete())
				OHH->(MsUnLock())
				cOpcOHH := "5"
			EndIf

			If !Empty(cOpcOHH)
				//Grava na fila de sincroniza��o a altera��o
				J170GRAVA("OHH", SE1->E1_FILIAL+;
				                 SE1->E1_PREFIXO+;
				                 SE1->E1_NUM+;
				                 SE1->E1_PARCELA+;
				                 SE1->E1_TIPO+;
				                 cAnoMesOHH, cOpcOHH)
			EndIf

			dDtOHH     := LastDay(MonthSum(dDtOHH, 1)) // �ltimo dia do proximo m�s
			cAnoMesOHH := AnoMes(dDtOHH)

			// Controle para gerar todas as parcelas para cada ano-m�s, at� o ano-m�s atual, com base na data dNewDtMov
			If !lGrParcAnt 
				Exit
			EndIf
		EndDo

		If !lExecLote
			SE1->(DbGoTo(nRecOld))
			RestArea(aArea)
		EndIf
	EndIf
EndIf

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} J255DelHist()
Deleta a posi��o hist�rica do Contas a Receber (OHH) referente a cChaveSE1.

@param cChaveSE1, Chave do t�tulo a receber que foi deletado

@author Bruno Ritter | Jorge Martins
@since 09/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Function J255DelHist(cChaveSE1)

If FWAliasInDic("OHH")
	dbSelectArea( 'OHH' )
	OHH->(DBSetOrder(1)) // OHH_FILIAL+OHH_PREFIX+OHH_NUM+OHH_PARCEL+OHH_TIPO+OHH_ANOMES
	OHH->(DbGoTop())

	While OHH->(DbSeek(cChaveSE1))
		//Grava na fila de sincroniza��o a exclus�o
		J170GRAVA("OHH", OHH->OHH_FILIAL+;
		                 OHH->OHH_PREFIX+;
		                 OHH->OHH_NUM+;
		                 OHH->OHH_PARCEL+;
		                 OHH->OHH_TIPO+;
		                 OHH->OHH_ANOMES, "5")
		
		Reclock( "OHH", .F. )
		OHH->(dbDelete())
		OHH->(MsUnLock())
	EndDo
EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} JGrvSaldo()
Retorna o saldo e o valor original de Honor�rios e Despesas.

@param cJurFat,    C�digo da Fatura 
@param cAnoMes,    Ano\M�s do hist�rico do Contas a Receber
@param cTitulo,    Chave do titulo do Contas a Receber
@param lInclui,    Verifica se ser� gravado um novo registro na OHH
@param dDtEmiSE1,  Data de emiss�o do t�tulo

@author Anderson Carvalho
@since 14/12/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function JGrvSaldo(cJurFat, cAnoMes, cTitulo, lInclui, dDtEmiSE1)
	Local nTamFil    := TamSX3("NXA_FILIAL")[1]
	Local nTamEsc    := TamSX3("NXA_CESCR")[1]
	Local nTamFat    := TamSX3("NXA_COD")[1]
	Local nTamVlH    := TamSX3("OHH_SALDOH")[2]
	Local nTamVlD    := TamSX3("OHH_SALDOD")[2]
	Local nVlrHon    := 0
	Local nVlrDesp   := 0
	Local nVlDesRemb := 0
	Local nVlDesTrib := 0
	Local nVlrTxAdm  := 0
	Local nVlrGross  := 0
	Local nValParc   := SE1->E1_VALOR // Valor total da parcela
	Local nTotFat    := 0 // Total da Fatura Honor�rios + Despesas
	Local cAux       := Strtran(cJurFat,"-","")
	Local cEscrit    := Substr(cAux, nTamFil + 1, nTamEsc)
	Local cFatura    := Substr(cAux, nTamFil + nTamEsc + 1, nTamFat)
	Local cQryOHI    := ""
	Local cQryRes    := ""
	Local cTpPriori  := SuperGetMv('MV_JTPRIO',, '1') //1-Prioriza despesas 2-Proporcional
	Local aSaldos    := {}
	Local aVlrOrig   := {}
	Local aVlrDesp   := {0, 0, 0, 0, 0}
	Local dDiaIni    := StoD(cAnoMes + "01")
	Local dDiaFim    := LastDay(dDiaIni)
	Local nBaixaDesp := 0
	Local nBxDesRemb := 0
	Local nBxDesTrib := 0
	Local nBxTxAdm   := 0
	Local nBxGross   := 0
	Local nBaixaHon  := 0
	Local nI         := 0
	Local nParcAtu   := 0
	Local nQtdParc   := 0
	Local aDespExist := {}
	Local lDespTrib  := OHH->(ColumnPos("OHH_VLREMB")) > 0 .And. OHI->(ColumnPos("OHI_VLREMB")) > 0 // Prote��o

	aSaldos := JurGetDados( "NXA", 1, xFilial("NXA") + cEscrit + cFatura, {"NXA_VLFATH", "NXA_VLFATD","NXA_VLREMB","NXA_VLTRIB", "NXA_VLTXAD", "NXA_VLGROS"})
	nTotFat := aSaldos[1] + aSaldos[2]

	While Len(aSaldos) < 12 // Criando as posi��es para os saldos de honor�rios e despesas
		AAdd(aSaldos, 0)
	EndDo

	cQryOHI := "SELECT SUM(OHI_VLHCAS) AS SALDO_H, SUM(OHI_VLDCAS) AS SALDO_D "
	If lDespTrib
		cQryOHI += " , SUM(OHI_VLREMB) AS SALDO_DREMB, SUM(OHI_VLTRIB) AS SALDO_DTRIB, SUM(OHI_VLTXAD) AS SALDO_TXADM, SUM(OHI_VLGROS) AS SALDO_GROSS "
	EndIf
	cQryOHI +=  " FROM " + RetSqlName("OHI") + " "
	cQryOHI += " WHERE OHI_CHVTIT = '" + cTitulo + "' "
	cQryOHI +=   " AND OHI_DTAREC <= '" + DtoC(dDiaFim) + "' "
	cQryOHI +=   " AND D_E_L_E_T_ = ' ' "

	cQryRes := GetNextAlias()
	cQryOHI := ChangeQuery(cQryOHI)
	DbUseArea(.T., "TOPCONN", TcGenQry(,, cQryOHI), cQryRes, .T., .T.)

	nBaixaDesp := (cQryRes)->SALDO_D
	nBaixaHon  := (cQryRes)->SALDO_H
	If lDespTrib
		nBxDesRemb := (cQryRes)->SALDO_DREMB
		nBxDesTrib := (cQryRes)->SALDO_DTRIB
		nBxTxAdm   := (cQryRes)->SALDO_TXADM
		nBxGross   := (cQryRes)->SALDO_GROSS
	EndIf
	
	(cQryRes)->( DbCloseArea())

	If cTpPriori == "1" // Prioriza despesas
		aVlrOrig := GetVlOri(cTitulo, dDtEmiSE1) // Verifica se existem valores em ano-m�s anterior
		If Len(aVlrOrig) >= 2 .And. aVlrOrig[1] == 0 .And. aVlrOrig[2] == 0
			aVlrDesp := J256PriDes(, cEscrit, cFatura,,, aSaldos[2], "OHH",, lInclui, aSaldos, cAnoMes)

			nVlrDesp   := aVlrDesp[1]
			nVlDesRemb := aVlrDesp[2]
			nVlDesTrib := aVlrDesp[3]
			nVlrTxAdm  := aVlrDesp[4]
			nVlrGross  := aVlrDesp[5]
		Else
			nVlrDesp   := aVlrOrig[2]
			nVlDesRemb := aVlrOrig[3]
			nVlDesTrib := aVlrOrig[4]
			nVlrTxAdm  := aVlrOrig[5]
			nVlrGross  := aVlrOrig[6]
		EndIf
	
		aSaldos[2] := nVlrDesp
		aSaldos[3] := nVlDesRemb
		aSaldos[4] := nVlDesTrib
		aSaldos[5] := nVlrTxAdm
		aSaldos[6] := nVlrGross

		If Len(aVlrOrig) >= 2 .And. aVlrOrig[1] == 0 .And. aVlrOrig[2] == 0
			nVlrHon := RatPontoFl(aSaldos[1], aSaldos[1], (nValParc - aSaldos[2]), nTamVlH)
		Else
			nVlrHon := aVlrOrig[1]
		EndIf

		aSaldos[1] := nVlrHon
	
		If !Empty(nBaixaHon) .Or. !Empty(nBaixaDesp)
			aSaldos[7]  := (aSaldos[1] - nBaixaHon)  // Saldo Honor�rios
			aSaldos[8]  := (aSaldos[2] - nBaixaDesp) // Saldo Despesas
			aSaldos[9]  := (aSaldos[3] - nBxDesRemb) // Saldo Despesas Reembols�vel
			aSaldos[10] := (aSaldos[4] - nBxDesTrib) // Saldo Despesas Tribut�vel
			aSaldos[11] := (aSaldos[5] - nBxTxAdm  ) // Saldo Taxa Administrativa
			aSaldos[12] := (aSaldos[6] - nBxGross  ) // Saldo Gross Up
		Else
			aSaldos[7]  := aSaldos[1]
			aSaldos[8]  := aSaldos[2]
			aSaldos[9]  := aSaldos[3]
			aSaldos[10] := aSaldos[4]
			aSaldos[11] := aSaldos[5]
			aSaldos[12] := aSaldos[6]
		EndIf

		For nI := 1 To Len(aSaldos)
			If aSaldos[nI] < 0
				aSaldos[nI] := 0
			EndIf
		Next

	Else // 2-Proporcional
	
		If lInclui

			aDespExist := J255GetDes(cEscrit, cFatura, lInclui) // Valores de despesas na OHH
			nParcAtu   := aDespExist[7] + 1                     // Parcela atual
			nQtdParc   := Round(nTotFat / nValParc, 2)          // Total de Parcelas

			If nQtdParc == nParcAtu // �ltima Parcela - Joga o valor restante
				// Campos totalizadores
				aSaldos[1] := aSaldos[1] - aDespExist[6]
				aSaldos[2] := aSaldos[2] - aDespExist[1]
				aSaldos[3] := aSaldos[3] - aDespExist[2]
				aSaldos[4] := aSaldos[4] - aDespExist[3]
				aSaldos[5] := aSaldos[5] - aDespExist[4]
				aSaldos[6] := aSaldos[6] - aDespExist[5]
			Else
				// Campos totalizadores
				aSaldos[1] := RatPontoFl(aSaldos[1], nTotFat, nValParc, nTamVlH)
				aSaldos[2] := RatPontoFl(aSaldos[2], nTotFat, nValParc, nTamVlD)
				aSaldos[3] := RatPontoFl(aSaldos[3], nTotFat, nValParc, nTamVlD)
				aSaldos[4] := RatPontoFl(aSaldos[4], nTotFat, nValParc, nTamVlD)
				aSaldos[5] := RatPontoFl(aSaldos[5], nTotFat, nValParc, nTamVlD)
				aSaldos[6] := RatPontoFl(aSaldos[6], nTotFat, nValParc, nTamVlD)
			EndIf
			// Campos de saldos
			aSaldos[7]  := aSaldos[1] - nBaixaHon  // Saldo Honor�rios
			aSaldos[8]  := aSaldos[2] - nBaixaDesp // Saldo Despesas
			aSaldos[9]  := aSaldos[3] - nBxDesRemb // Saldo Despesas Reembols�vel
			aSaldos[10] := aSaldos[4] - nBxDesTrib // Saldo Despesas Tribut�vel
			aSaldos[11] := aSaldos[5] - nBxTxAdm   // Saldo Taxa administrativa
			aSaldos[12] := aSaldos[6] - nBxGross   // Saldo Gross Up
		Else
			aSaldos[1] := OHH->OHH_VLFATH
			aSaldos[2] := OHH->OHH_VLFATD
			If lDespTrib
				aSaldos[3] := OHH->OHH_VLREMB
				aSaldos[4] := OHH->OHH_VLTRIB
				aSaldos[5] := OHH->OHH_VLTXAD
				aSaldos[6] := OHH->OHH_VLGROS
			EndIf
			If !Empty(nBaixaHon) .Or. !Empty(nBaixaDesp)
				aSaldos[7] := (aSaldos[1] - nBaixaHon)  // Saldo Honor�rios
				aSaldos[8] := (aSaldos[2] - nBaixaDesp) // Saldo Despesas
				If lDespTrib
					aSaldos[9]  := (aSaldos[3] - nBxDesRemb) // Saldo Despesas Reembols�vel
					aSaldos[10] := (aSaldos[4] - nBxDesTrib) // Saldo Despesas Tribut�vel
					aSaldos[11] := (aSaldos[5] - nBxTxAdm  ) // Saldo Taxa administrativa
					aSaldos[12] := (aSaldos[6] - nBxGross  ) // Saldo Gross Up
				EndIf
			Else
				aSaldos[7]  := aSaldos[1]
				aSaldos[8]  := aSaldos[2]
				aSaldos[9]  := aSaldos[3]
				aSaldos[10] := aSaldos[4]
				aSaldos[11] := aSaldos[5]
				aSaldos[12] := aSaldos[6]
			EndIf
		EndIf
	EndIf

Return aSaldos

//-------------------------------------------------------------------
/*/{Protheus.doc} J255GetDes()
Retorna os valores de despesas j� gravadas na OHH para cada parcela de t�tulo

@param cEscrit   , C�digo do escrit�rio da fatura
@param cFatura   , C�digo da fatura
@param lInclui   , Verifica se ser� gravado um novo registro na OHH
@param cAnoMesAtu, Ano M�s de refer�ncia

@author  Anderson Carvalho / Abner Foga�a
@since   26/12/2018
/*/
//-------------------------------------------------------------------
Function J255GetDes(cEscrit, cFatura, lInclui, cAnoMesAtu)
	Local aDesp      := {0, 0, 0, 0, 0, 0, 0}
	Local cQryOHH    := ""
	Local cAliasOHH  := ""
	Local lDespTrib  := OHH->(ColumnPos("OHH_VLREMB")) > 0 // Prote��o

	Default cAnoMesAtu := Anomes(dDataBase)

	nTotDesp := JurGetDados("NXA", 1, xFilial("NXA") + cEscrit + cFatura, "NXA_VLFATD")

	cQryOHH := " SELECT SUM(OHH_VLFATD) OHH_VLFATD "
	If lDespTrib
		cQryOHH += ", SUM(OHH_VLREMB) OHH_VLREMB, SUM(OHH_VLTRIB) OHH_VLTRIB, SUM(OHH_VLTXAD) OHH_VLTXAD, SUM(OHH_VLGROS) OHH_VLGROS, SUM(OHH_VLFATH) OHH_VLFATH, COUNT(*) CONTADOR  "
	EndIf
	cQryOHH +=   " FROM " + RetSqlName("OHH") + " "
	cQryOHH +=  " WHERE OHH_FILIAL = '" + xFilial("OHH") + "' "
	cQryOHH +=    " AND OHH_JURFAT = '" + SE1->E1_JURFAT + "' "
	If lInclui
		cQryOHH +=    " AND OHH_ANOMES = '" + cAnoMesAtu + "' "
	Else
		cQryOHH +=    " AND OHH_ANOMES < '" + cAnoMesAtu + "' "
	EndIf
	cQryOHH +=    " AND D_E_L_E_T_ = ' ' "

	cAliasOHH := GetNextAlias()
	cQryOHH   := ChangeQuery(cQryOHH)
	DbUseArea(.T., "TOPCONN", TcGenQry(,, cQryOHH), cAliasOHH, .T., .T.)

	If !Empty((cAliasOHH)->OHH_VLFATD)
		aDesp[1] := (cAliasOHH)->OHH_VLFATD
		If lDespTrib
			aDesp[2] := (cAliasOHH)->OHH_VLREMB
			aDesp[3] := (cAliasOHH)->OHH_VLTRIB
			aDesp[4] := (cAliasOHH)->OHH_VLTXAD
			aDesp[5] := (cAliasOHH)->OHH_VLGROS
			aDesp[6] := (cAliasOHH)->OHH_VLFATH
			aDesp[7] := (cAliasOHH)->CONTADOR
		EndIf
	EndIf

	(cAliasOHH)->( DbCloseArea())

Return (aDesp)

//-------------------------------------------------------------------
/*/{Protheus.doc} JEstorno()
Retornar o valor original e de saldo (honor�rios e despesas) quando 
realizado estorno da baixa do t�tulo.

@param lInclui, Verifica se ser� gravado um novo registro na OHH
@param cTitulo, Chave do titulo do Contas a Receber
@param aOHIBxAnt, Total de honor�rios e despesas baixados antes do estorno

@author Anderson Carvalho | Abner Foga�a
@since 16/01/2019
/*/
//-------------------------------------------------------------------
Static Function JEstorno(lInclui, cTitulo, aOHIBxAnt)
	Local nVlrOriHon := 0
	Local nVlrOriDes := 0
	Local nVlOriRemb := 0
	Local nVlOriTrib := 0
	Local nVlOriTxAd := 0
	Local nVlOriGros := 0
	Local nSaldoHon  := 0
	Local nSaldoDes  := 0
	Local nSaldoDRem := 0
	Local nSaldoDTri := 0
	Local nSaldoTxAd := 0
	Local nSaldoGros := 0
	Local nTamVlH    := TamSX3("OHH_SALDOH")[2]
	Local nTamVlD    := TamSX3("OHH_SALDOD")[2]
	Local lDespTrib  := OHH->(ColumnPos("OHH_VLREMB")) > 0 // Prote��o
	Local aOHIBxNew  := J255TotBx(cTitulo)

	If lInclui
		nVlrOriHon := aOHIBxAnt[1]
		nVlrOriDes := aOHIBxAnt[2]
		nVlOriRemb := aOHIBxAnt[3]
		nVlOriTrib := aOHIBxAnt[4]
		nVlOriTxAd := aOHIBxAnt[5]
		nVlOriGros := aOHIBxAnt[6]
		nSaldoHon  := Round(nVlrOriHon - aOHIBxNew[1], nTamVlH)
		nSaldoDes  := Round(nVlrOriDes - aOHIBxNew[2], nTamVlD)
		nSaldoDRem := Round(nVlOriRemb - aOHIBxNew[3], nTamVlD)
		nSaldoDTri := Round(nVlOriTrib - aOHIBxNew[4], nTamVlD)
		nSaldoTxAd := Round(nVlOriTxAd - aOHIBxNew[5], nTamVlD)
		nSaldoGros := Round(nVlOriGros - aOHIBxNew[6], nTamVlD)
	Else
		nVlrOriHon := OHH->OHH_VLFATH
		nVlrOriDes := OHH->OHH_VLFATD
		nSaldoHon  := Round(OHH->OHH_VLFATH - aOHIBxNew[1], nTamVlH)
		nSaldoDes  := Round(OHH->OHH_VLFATD - aOHIBxNew[2], nTamVlD)
		If lDespTrib
			nVlOriRemb := OHH->OHH_VLREMB
			nVlOriTrib := OHH->OHH_VLTRIB
			nVlOriTxAd := OHH->OHH_VLTXAD
			nVlOriGros := OHH->OHH_VLGROS
			nSaldoDRem := Round(OHH->OHH_VLREMB - aOHIBxNew[3], nTamVlD)
			nSaldoDTri := Round(OHH->OHH_VLTRIB - aOHIBxNew[4], nTamVlD)
			nSaldoTxAd := Round(OHH->OHH_VLTXAD - aOHIBxNew[5], nTamVlD)
			nSaldoGros := Round(OHH->OHH_VLGROS - aOHIBxNew[6], nTamVlD)
		EndIf
	EndIf

	aOHIBxNew[1] := nVlrOriHon
	aOHIBxNew[2] := nVlrOriDes
	aOHIBxNew[3] := nVlOriRemb
	aOHIBxNew[4] := nVlOriTrib
	aOHIBxNew[5] := nVlOriTxAd
	aOHIBxNew[6] := nVlOriGros
	aAdd(aOHIBxNew, nSaldoHon)
	aAdd(aOHIBxNew, nSaldoDes)
	aAdd(aOHIBxNew, nSaldoDRem)
	aAdd(aOHIBxNew, nSaldoDTri)
	aAdd(aOHIBxNew, nSaldoTxAd)
	aAdd(aOHIBxNew, nSaldoGros)

Return aOHIBxNew

//-------------------------------------------------------------------
/*/{Protheus.doc} J255TotBx()
Retorna a somat�ria das baixas de honor�rios e despesas para cada t�tulo.

@param cTitulo, Chave do titulo do Contas a Receber

@author  Anderson Carvalho | Abner Foga�a
@since   16/01/2019
/*/
//-------------------------------------------------------------------
Function J255TotBx(cTitulo)
	Local cQryOHI   := ""
	Local cAliasOHI := ""
	Local aTotBx    := {0, 0, 0, 0, 0, 0}
	Local nTamVlH   := TamSX3("OHH_SALDO")[2]
	Local nTamVlD   := TamSX3("OHH_SALDOD")[2]
	Local lDespTrib := OHI->(ColumnPos("OHI_VLREMB")) > 0

	cQryOHI := " SELECT SUM(OHI_VLHCAS) AS OHI_VLHCAS, SUM(OHI_VLDCAS) AS OHI_VLDCAS "
	If lDespTrib
		cQryOHI +=   " ,SUM(OHI_VLREMB) AS OHI_VLREMB, SUM(OHI_VLTRIB) AS OHI_VLTRIB, SUM(OHI_VLTXAD) AS OHI_VLTXAD, SUM(OHI_VLGROS) AS OHI_VLGROS "
	EndIf
	cQryOHI +=   " FROM " + RetSqlName("OHI") + " "
	cQryOHI +=  " WHERE OHI_CHVTIT = '" + cTitulo + "' "
	cQryOHI +=    " AND D_E_L_E_T_ = ' ' "
	
	cAliasOHI := GetNextAlias()
	cQryOHI   := ChangeQuery( cQryOHI )
	DbUseArea(.T., "TOPCONN", TcGenQry(,, cQryOHI), cAliasOHI, .T., .T.)

	If !Empty((cAliasOHI)->OHI_VLHCAS) .Or. !Empty((cAliasOHI)->OHI_VLDCAS)
		aTotBx[1] := RatPontoFl((cAliasOHI)->OHI_VLHCAS, (cAliasOHI)->OHI_VLHCAS, (cAliasOHI)->OHI_VLHCAS, nTamVlH)
		aTotBx[2] := RatPontoFl((cAliasOHI)->OHI_VLDCAS, (cAliasOHI)->OHI_VLDCAS, (cAliasOHI)->OHI_VLDCAS, nTamVlD)
		If lDespTrib
			aTotBx[3] := RatPontoFl((cAliasOHI)->OHI_VLREMB, (cAliasOHI)->OHI_VLREMB, (cAliasOHI)->OHI_VLREMB, nTamVlD)
			aTotBx[4] := RatPontoFl((cAliasOHI)->OHI_VLTRIB, (cAliasOHI)->OHI_VLTRIB, (cAliasOHI)->OHI_VLTRIB, nTamVlD)
			aTotBx[5] := RatPontoFl((cAliasOHI)->OHI_VLTXAD, (cAliasOHI)->OHI_VLTXAD, (cAliasOHI)->OHI_VLTXAD, nTamVlD)
			aTotBx[6] := RatPontoFl((cAliasOHI)->OHI_VLGROS, (cAliasOHI)->OHI_VLGROS, (cAliasOHI)->OHI_VLGROS, nTamVlD)
		EndIf
	EndIf

Return aTotBx

//-------------------------------------------------------------------
/*/{Protheus.doc} GrvParcAnt()
Executa a grava��o de parcelas anteriores at� a parcela posicionada.

@param lGrParcAnt, Executa fun��o para gerar parcelas anteriores.
@param dDtOHH,     Data da movimenta��o da OHH
@param cE1Prefixo, Prefixo do t�tulo
@param cE1Num,     N�mero do t�tulo
@param cE1Parcela, Parcela do t�tulo
@param cSe1Tipo,   Tipo do t�tulo

@author  Anderson Carvalho | Abner Foga�a
@since   18/01/2019
/*/
//-------------------------------------------------------------------
Static Function GrvParcAnt(lGrParcAnt, dDtOHH, cE1Prefixo, cE1Num, cE1Parcela, cSe1Tipo)
	Local cQuery     := ""
	Local cAliasQry  := ""
	Local cAnoMesOHH := AnoMes(dDtOHH)

	cQuery := " SELECT SE1.R_E_C_N_O_"
	cQuery +=   " FROM " + RetSqlName("SE1") + " SE1 "
	cQuery +=   " LEFT JOIN " + RetSqlName( "OHH" ) + " OHH "
	cQuery +=     " ON OHH.OHH_ANOMES = '" + cAnoMesOHH + "' "
	cQuery +=    " AND OHH.OHH_FILIAL = SE1.E1_FILIAL "
	cQuery +=    " AND OHH.OHH_PREFIX = SE1.E1_PREFIXO "
	cQuery +=    " AND OHH.OHH_NUM = SE1.E1_NUM "
	cQuery +=    " AND OHH.OHH_PARCEL = SE1.E1_PARCELA "
	cQuery +=    " AND OHH.OHH_TIPO = SE1.E1_TIPO "
	cQuery +=    " AND OHH.D_E_L_E_T_ = ' '  "
	cQuery +=  " WHERE SE1.E1_FILIAL = '" + xFilial("SE1") + "' "
	cQuery +=    " AND SE1.E1_PREFIXO = '" + cE1Prefixo + "' "
	cQuery +=    " AND SE1.E1_NUM = '" + cE1Num + "' "
	cQuery +=    " AND SE1.E1_PARCELA < '" + cE1Parcela + "' "
	cQuery +=    " AND SE1.E1_TIPO = '" + cSe1Tipo + "' "
	cQuery +=    " AND SE1.E1_SALDO > 0 "
	cQuery +=    " AND SE1.D_E_L_E_T_ = ' ' "
	cQuery +=    " AND OHH.R_E_C_N_O_ IS NULL "
	cQuery +=  " ORDER BY SE1.E1_PARCELA "

	cAliasQry := GetNextAlias()
	cQuery    := ChangeQuery( cQuery )
	DbUseArea(.T., "TOPCONN", TcGenQry(,, cQuery), cAliasQry, .T., .T.)

	While !(cAliasQry)->( EOF() )
		J255APosHis((cAliasQry)->R_E_C_N_O_, dDtOHH,,, .F.)
		(cAliasQry)->( dbSkip() )
	EndDo

	(cAliasQry)->(DbCloseArea())

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} GetVlOri()
Retorna os valores originais (honor�rios e despesas) do ano m�s anteriores.

@param cTitulo,    Chave do t�tulo
@param dDtEmiSE1,  Data de emiss�o do t�tulo

@author  Anderson Carvalho | Abner Foga�a
@since   18/01/2019
/*/
//-------------------------------------------------------------------
Static Function GetVlOri(cTitulo, dDtEmiSE1)
	Local aArea     := GetArea()
	Local cAnoMes   := ""
	Local nRecOld   := OHH->(Recno())
	Local aRetVlOri := {0, 0, 0, 0, 0, 0}

	OHH->(DBSetOrder(1))  // OHH_FILIAL+OHH_PREFIX+OHH_NUM+OHH_PARCEL+OHH_TIPO+OHH_ANOMES
	cAnoMes := AnoMes(dDtEmiSE1)
	If OHH->(DbSeek(cTitulo + cAnoMes))
		aRetVlOri[1] := OHH->OHH_VLFATH
		aRetVlOri[2] := OHH->OHH_VLFATD
		If OHH->(ColumnPos("OHH_VLREMB")) > 0 // Prote��o
			aRetVlOri[3] := OHH->OHH_VLREMB
			aRetVlOri[4] := OHH->OHH_VLTRIB
			aRetVlOri[5] := OHH->OHH_VLTXAD
			aRetVlOri[6] := OHH->OHH_VLGROS
		EndIf
	EndIf

	OHH->(DbGoto(nRecOld))
	RestArea(aArea)

Return aRetVlOri

//-------------------------------------------------------------------
/*/{Protheus.doc} J255AjNfe
Ajusta a OHH com o n�mero da Nota Fiscal Eletr�nica do t�tulo a receber.

@param nRecSE1, Recno da SE1 que est� sendo alteado o campo E1_NFELETR

@author Bruno Ritter
@since 17/01/2019
/*/
//-------------------------------------------------------------------
Function J255AjNfe(nRecSE1)
	Local nRecOld   := SE1->(Recno())
	Local aArea     := GetArea()
	Local cChaveSE1 := ""
	Local cAnoMes   := AnoMes(dDataBase)

	SE1->(dbGoTo(nRecSE1))
	cChaveSE1 := SE1->E1_FILIAL + SE1->E1_PREFIXO + SE1->E1_NUM + SE1->E1_PARCELA + SE1->E1_TIPO

	OHH->(DBSetOrder(1)) // OHH_FILIAL+OHH_PREFIX+OHH_NUM+OHH_PARCEL+OHH_TIPO+OHH_ANOMES
	If OHH->(DbSeek(cChaveSE1 + cAnoMes))
		RecLock("OHH", .F.)
		OHH->OHH_NFELET := SE1->E1_NFELETR
		OHH->(MsUnlock())

		//Grava na fila de sincroniza��o
		J170GRAVA("OHH", cChaveSE1 + cAnoMes, "4")
	EndIf

	SE1->(dbGoTo(nRecOld))
	RestArea(aArea)
	
Return Nil