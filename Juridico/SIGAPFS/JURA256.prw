#INCLUDE "JURA256.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH"
#INCLUDE "FWMVCDEF.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} JURA256
Rastreamento de Fatura

@author Luciano Pereira dos Santos
@since 07/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Function JURA256()
Local oBrw256 := Nil

oBrw256 := FWMBrowse():New()
oBrw256:SetDescription( STR0004 ) //"Rastreamento de recebimentos por casos da fatura"
oBrw256:SetAlias( 'SE1' )
oBrw256:SetMenuDef('JURA256')
oBrw256:SetFilterDefault("!Empty(E1_JURFAT)")
JurSetLeg( oBrw256, 'SE1' )
JurSetBSize( oBrw256 )
oBrw256:Activate()

Return NIL

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menu Funcional

@return aRotina - Estrutura
[n,1] Nome a aparecer no cabecalho
[n,2] Nome da Rotina associada
[n,3] Reservado
[n,4] Tipo de Transa��o a ser efetuada:
1 - Pesquisa e Posiciona em um Banco de Dados
2 - Simplesmente Mostra os Campos
3 - Inclui registros no Bancos de Dados
4 - Altera o registro corrente
5 - Remove o registro corrente do Banco de Dados
6 - Altera��o sem inclus�o de registros
7 - C�pia
8 - Imprimir
[n,5] Nivel de acesso
[n,6] Habilita Menu Funcional

@author Luciano Pereira dos Santos
@since 07/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
Local aRotina := {}

aAdd( aRotina, { STR0001, 'PesqBrw'         , 0, 1, 0, .T. } ) //"Pesquisar"
aAdd( aRotina, { STR0002, 'VIEWDEF.JURA256' , 0, 2, 0, NIL } ) //"Visualizar"
aAdd( aRotina, { STR0003, 'VIEWDEF.JURA256' , 0, 8, 0, NIL } ) //"Imprimir"

Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados de Rastreamento de Fatura

@author Luciano Pereira dos Santos
@since 03/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function Modeldef()
Local oModel     := NIL
Local oStructSE1 := FWFormStruct( 1, "SE1",{|cCampo| J256SE1Cpo(cCampo)})
Local oStructOHI := FWFormStruct( 1, "OHI" )
Local oEvent     := JA256Event():New()

oStructSE1 := J256whenF(oStructSE1)

oModel:= MPFormModel():New( "JURA256", /*Pre-Validacao*/, /*Pos-Validacao*/, /*Commit*/,/*Cancel*/)

oModel:AddFields( "SE1MASTER", NIL/*cOwner*/, oStructSE1, /*Pre-Validacao*/, /*Pos-Validacao*/ )
oModel:AddGrid(� "OHIDETAIL",�"SE1MASTER"�/*cOwner*/,�oStructOHI,�/*Pre-Validacao*/,�/*Pos-Validacao*/,�/*bPre*/,�/*bPost*/�)

oModel:GetModel( "SE1MASTER" ):SetDescription( STR0005 ) // "Titulo"
oModel:GetModel( "OHIDETAIL" ):SetDescription( STR0006 ) // "Recebimentos por casos"

oModel:SetRelation("OHIDETAIL", {{"OHI_FILIAL", "xFilial('OHI')" }, {"OHI_CHVTIT", "SE1->(E1_FILIAL+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO)"}}, OHI->(IndexKey(2)))

oModel:SetOptional( "OHIDETAIL", .T. )
oModel:GetModel("OHIDETAIL"):SetNoInsertLine(.T.)
oModel:GetModel("OHIDETAIL"):SetNoUpdateLine(.T.)
oModel:GetModel("OHIDETAIL"):SetNoDeleteLine(.T.)

oModel:InstallEvent("JA256Event", /*cOwner*/, oEvent)

Return oModel

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
View de dados de Itens de desdobramento

@author Luciano Pereira dos Santos
@since 07/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
Local oView      := Nil
Local oModel     := FWLoadModel( "JURA256" )
Local oStructSE1 := FWFormStruct( 2, "SE1", {|cCampo| J256SE1Cpo(cCampo)})
Local oStructOHI := FWFormStruct( 2, "OHI")
Local cLojaAuto  := SuperGetMv( "MV_JLOJAUT" , .F. , "2" ,) // Indica se a Loja do Caso deve ser preenchida automaticamente. (1-Sim; 2-N�o)

oStructOHI:RemoveField("OHI_FILIAL")
oStructOHI:RemoveField("OHI_CESCR")
oStructOHI:RemoveField("OHI_CFATUR")
If (cLojaAuto == "1") // Loja Autom�tica
	oStructOHI:RemoveField("OHI_CLOJA")
EndIf

oView := FWFormView():New()
oView:SetModel(oModel)
oView:AddField("JURA256_SE1", oStructSE1, "SE1MASTER")
oView:AddGrid("JURA256_OHI" , oStructOHI, "OHIDETAIL")

oView:CreateHorizontalBox("FORMFIELD", 30)
oView:CreateHorizontalBox("FORMGRID",  70)

oView:SetOwnerView("JURA256_SE1", "FORMFIELD")
oView:SetOwnerView("JURA256_OHI", "FORMGRID")

oView:AddIncrementField("JURA256_OHI","OHI_ITEM")

oView:EnableTitleView("JURA256_OHI")

Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} J256whenF(oStruct)
Rotina para desabilitar os campos das estrutura

@Param oStruct    Estrutura da tabela

@Return oStruct   Estrutura da tabela alterada

@author Luciano Pereira dos Santos
@since 07/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function J256whenF(oStruct)
Local aStruct := oStruct:GetFields()
Local nI      := 1

For nI := 1 to Len(aStruct)
	oStruct:SetProperty(aStruct[nI][MODEL_FIELD_IDFIELD], MODEL_FIELD_WHEN, {||.F.})
Next nI

Return oStruct

//-------------------------------------------------------------------
/*/{Protheus.doc} J256SE1Cpo(cCampo)
Fun��o para selecionar os campos do Model da tabela NXA

@param cCampo campo da estrutura.

@Return .T. para campos que pode ser carregado no modelo

@author Luciano Pereira dos Santos
@since 03/10/2017
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function J256SE1Cpo(cCampo)
Local cCampos  := 'E1_FILIAL|E1_PREFIXO|E1_NUM|E1_PARCELA|E1_TIPO|E1_EMISSAO|E1_VENCTO|E1_INSS|E1_PIS|E1_COFINS|E1_CSLL|E1_VALOR'
Local lRet     := .F.
Local cNomeCpo := AllTrim(cCampo)

If cNomeCpo $ cCampos
	lRet := .T.
Endif

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JA256Event
Classe interna implementando o FWModelEvent, para execu��o de fun��es
nos eventos do modelo.

@author Luciano Pereira dos Santos
@since 07/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Class JA256Event FROM FWModelEvent
	Method New()
	Method ModelPosVld()

End Class

//-------------------------------------------------------------------
/*/{Protheus.doc} New()
Metodo de inicializa��o da Classe FWModelEvent.

@author Luciano Pereira dos Santos
@since 07/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Method New() Class JA256Event
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelPosVld
M�todo que � chamado pelo MVC quando ocorrer as a��es de pos valida��o do Model.

@author Luciano Pereira dos Santos
@since 07/02/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Method ModelPosVld(oModel, cModelId) Class JA256Event
Local lRet       := .T.
Local nOperation := oModel:GetOperation()

If nOperation != MODEL_OPERATION_VIEW
	JurMsgErro(STR0007,, STR0008) //#"Opera��o n�o permitida."  ##"Essa rotina s� permite a opera��o de visualiza��o."
	lRet := .F.
EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JGrvRast
Grava o rastreamento da baixa por casos da fatura.

@param  nSE1Recno, numerico, Recno do registro SE1
@param  nSE5Recno, numerico, Recno do registro SE5
@param  nRegCmp  , numerico, Recno do registro SE1 compensado (RA)
@return lRast    , logico  , Sucesso ou falha na grava��o do rastreamento

@author  Jonatas Martins
@since   08/02/2018
@version 12.1.20
/*/
//-------------------------------------------------------------------
Function J256GrvRas( nSE1Recno , nSE5Recno, nRegCmp )
	Local aAreas    := { SE1->(GetArea()) , SE5->(GetArea()) , OHI->(GetArea()) , GetArea() }
	Local aDados    := {}
	Local cSincOper := ""
	Local lContinue := .T.
	Local lRast     := .T.

	Default nSE1Recno := 0
	Default nSE5Recno := 0
	Default nRegCmp   := 0
	//--------------------------------------------
	// Prote��o devido ao congelamento do release
	//--------------------------------------------
	If AliasIndic("OHI")
		SE1->( DbGoTo( nSE1Recno ) )

		If SE1->( ! Eof() ) .And. ! Empty( SE1->E1_JURFAT )

			SE5->( DbGoTo( nSE5Recno ) )

			If SE5->( ! Eof() ) .And. SE5->E5_TIPO <> PadR( "RA" , TamSX3("E5_TIPO")[1] ) ;  //Ignora baixas de Adiantamento
				.And. ! ( AllTrim(SE5->E5_MOTBX) $ "CNF|LIQ") //Ignora baixas de cancelamento de fatura e liquida��o
				
				//Se o registro do movimento posicionado for de desconto / juros / multas, ent�o localiza o movimento da baixa
				If AllTrim(SE5->E5_TIPODOC) $ "DC|JR|MT"
					SE5->( DbSetOrder(21) )
					lContinue := SE5->( DbSeek( SE5->E5_FILIAL + SE5->E5_IDORIG + PadR("BA" , TamSX3("E5_TIPODOC")[1]) ) )
				EndIf
				
				//---------------------------
				// Busca casos da fatura
				//---------------------------
				If lContinue
				aDados := LoadData()
				//---------------------------
				// Grava��o dos dados na OHI
				//---------------------------
				If Len( aDados ) > 0
					cSincOper := GetSincOper( aDados[1][1] , aDados[1][2] , .T. )
					lRast := GrvRastOHI( aDados, nRegCmp )
					//---------------------------------
					// Atualiza fila de sincroniza��o
					//---------------------------------
					lRast := lRast .And. J170GRAVA( "JURA256" , xFilial("NXA") +  aDados[1][1] + aDados[1][2] , cSincOper )
					EndIf
				EndIf
			EndIf
		EndIf
	EndIf

	Aeval( aAreas , {|aArea| RestArea( aArea ) } )
Return ( lRast )

//-------------------------------------------------------------------
/*/{Protheus.doc} LoadData
Busca casos da fatura v�nculado ao t�tulo.

@return aDadosQry  , array  , Dados dos casos da fatura

@author  Jonatas Martins
@since   08/02/2018
@version 12.1.20
/*/
//-------------------------------------------------------------------
Static Function LoadData()
	Local nTamFil   := TamSX3("NXA_FILIAL")[1]
	Local nTamEsc   := TamSX3("NXA_CESCR")[1]
	Local nTamFat   := TamSX3("NXA_COD")[1]
	Local cJurFat   := Strtran(SE1->E1_JURFAT,"-","")
	Local cFilFat   := Substr(cJurFat, 1, nTamFil)
	Local cEscrit   := Substr(cJurFat, nTamFil+1, nTamEsc)
	Local cFatura   := Substr(cJurFat, nTamFil+nTamEsc+1, nTamFat)
	Local cQryNXC   := ""
	Local aDadosQry := {}

	cQryNXC := " SELECT NXC_CESCR , NXC_CFATUR, NXC_CCLIEN, NXC_CLOJA , NXC_CCONTR, "
	cQryNXC +=        " NXC_CCASO , NXC_VLHFAT, NXC_VLDFAT, 0 VALDESCH, 0 VALACRESH, 0 VALDESCD, 0 VALACRESD, "
	cQryNXC +=        " NXC_VLREMB, NXC_VLTRIB, NXC_VLGROS, NXC_VLTXAD "
	cQryNXC +=   " FROM " + RetSqlName("NXC") + " "
	cQryNXC +=  " WHERE NXC_FILIAL = '" + cFilFat + "' "
	cQryNXC +=    " AND NXC_CESCR  = '" + cEscrit + "' "
	cQryNXC +=    " AND NXC_CFATUR = '" + cFatura + "' "
	cQryNXC +=    " AND D_E_L_E_T_ = ' ' "

	aDadosQry := JurSQL(cQryNXC, {"NXC_CESCR" , "NXC_CFATUR", "NXC_CCLIEN", "NXC_CLOJA" ,;
	                              "NXC_CCONTR", "NXC_CCASO" , "NXC_VLHFAT", "NXC_VLDFAT",;
	                              "VALDESCH"  , "VALACRESH" , "VALDESCD"  , "VALACRESD" ,;
	                              "NXC_VLREMB", "NXC_VLTRIB", "NXC_VLTXAD", "NXC_VLGROS" })

Return ( aDadosQry )

//-------------------------------------------------------------------
/*/{Protheus.doc} GrvRastOHI
Grava o rastreamento da baixa por casos da fatura.

@param  aDados  , array   , Dados dos casos da fatura
@param  nRegCmp , numerico, Recno do registro SE1 compensado (RA)

@return lInsertOHI , logico  , Sucesso ou falha na inclus�o do rastreamento

@author  Jonatas Martins
@since   08/02/2018
@version 12.1.20
/*/
//-------------------------------------------------------------------
Static Function GrvRastOHI( aDados , nRegCmp )
	Local aDadosCas    := {}
	Local nVal         := 0
	Local cItem        := ""
	Local cChvTit      := ""
	Local nTaxaMoe     := IIf(SE5->E5_TXMOEDA == 0, 1, 1 / SE5->E5_TXMOEDA) // Taxa de convers�o para moeda da baixa
	Local nValAcess    := J256ValAce( SE5->E5_IDORIG )
	Local nVlDesc      := ( SE5->E5_VLDESCO * nTaxaMoe ) + IIf( nValAcess < 0, nValAcess * (-1), 0 ) // - Descontos - Decrescimo - Valores Acess�rios (Subtrair)
	Local nVlAcres     := ( ( SE5->E5_VLJUROS + SE5->E5_VLMULTA ) * nTaxaMoe ) + IIf( nValAcess > 0, nValAcess, 0 ) // + Acressimo + Tx. Permanencia + Multa + Valores Acess�rios (Somar)
	Local lCompens     := SE5->E5_TIPODOC == "CP" // Compensa��o
	Local nAbatimentos := IIf(SE1->E1_SALDO == 0, SomaAbat(SE1->E1_PREFIXO, SE1->E1_NUM, SE1->E1_PARCELA, "R", 1,, SE1->E1_CLIENTE, SE1->E1_LOJA, SE1->E1_FILIAL, SE1->E1_EMISSAO), 0) // Valor de impostos e abatimentos dos t�tulos - Usado somente se o saldo estiver zerado
	Local nValCRec     := IIf(lCompens, SE5->E5_VALOR, SE5->E5_VLMOED2) - nVlAcres + nVlDesc +  nAbatimentos // Recomposi��o do valor da baixa sem os valores adicionais
	Local cEscrit      := aDados[1][1]
	Local cFatura      := aDados[1][2]
	Local lInsertOHI   := .T.
	Local cMoedaFat    := JurGetDados('NXA', 1, xFilial('NXA') + cEscrit + cFatura, 'NXA_CMOEDA')
	Local lDespTrib    := OHI->(ColumnPos("OHI_VLREMB")) > 0 // Prote��o
	Local lCpoNatRec   := OHI->(ColumnPos("OHI_NATREC")) > 0 // Prote��o
	Local lCpoMoeda    := OHI->(ColumnPos("OHI_CMOEDA")) > 0 // Prote��o
	Local lCpoTpBx     := OHI->(ColumnPos("OHI_MOTBX")) > 0  // Prote��o
	Local lIsMovBco    := JIsMovBco(SE5->E5_MOTBX)

	Default nRegCmp    := 0
	
	cItem     := GetItem( cEscrit , cFatura )
	cChvTit   := SE1->E1_FILIAL + SE1->E1_PREFIXO + SE1->E1_NUM + SE1->E1_PARCELA + SE1->E1_TIPO

	//-----------------------------
	// Calcula valores por casos
	//-----------------------------
	aDadosCas := CalRatCas(aDados, cEscrit, cFatura, nValCRec, nVlDesc, nVlAcres, SE5->E5_DATA)

	BEGIN TRANSACTION
		For nVal := 1 To Len( aDadosCas )
			If RecLock("OHI", .T.)
				cItem := Soma1( cItem )
				OHI->OHI_FILIAL := xFilial("OHI")
				OHI->OHI_ITEM   := cItem
				OHI->OHI_CESCR  := aDadosCas[nVal][1]  // Escrit�rio
				OHI->OHI_CFATUR := aDadosCas[nVal][2]  // Fatura
				OHI->OHI_CCLIEN := aDadosCas[nVal][3]  // Cliente
				OHI->OHI_CLOJA  := aDadosCas[nVal][4]  // Loja
				OHI->OHI_CCONTR := aDadosCas[nVal][5]  // Contrato
				OHI->OHI_CCASO  := aDadosCas[nVal][6]  // Caso
				OHI->OHI_VLHCAS := aDadosCas[nVal][7]  // Valor Honor�rios do Caso
				OHI->OHI_VLDCAS := aDadosCas[nVal][8]  // Valor Despesas do Caso
				OHI->OHI_VLDESH := IIF(lIsMovBco, aDadosCas[nVal][9], aDadosCas[nVal][7])  // Valor de desconto no honor�rio do caso
				OHI->OHI_VLACRH := aDadosCas[nVal][10] // Valor de acr�scimo no honor�rio do caso
				OHI->OHI_VLDESD := IIF(lIsMovBco, aDadosCas[nVal][11], aDadosCas[nVal][8]) // Valor de desconto na despesa do caso
				OHI->OHI_VLACRD := aDadosCas[nVal][12] // Valor de acr�scimo na despesa do caso
				If lDespTrib
					OHI->OHI_VLREMB := aDadosCas[nVal][13] // Valor Despesas Reembols�vel do Caso
					OHI->OHI_VLTRIB := aDadosCas[nVal][14] // Valor Despesas Tribut�vel do Caso
					OHI->OHI_VLTXAD := aDadosCas[nVal][15] // Valor de Taxa Administrativa
					OHI->OHI_VLGROS := aDadosCas[nVal][16] // Valor de Gross Up
					OHI->OHI_CMOERE := SE5->E5_MOEDA       // Moeda do Recebimento
					OHI->OHI_VARCAM := SE5->E5_VLCORRE     // Varia��o Cambial / Corre��o Monet�ria
				EndIf
				OHI->OHI_DTAREC := SE5->E5_DATA
				If lIsMovBco
					OHI->OHI_VLCREC := IIF(lCompens, SE5->E5_VALOR, SE5->E5_VLMOED2)
				Else
					OHI->OHI_VLCREC := 0
				EndIf
				OHI->OHI_CHVTIT := cChvTit
				OHI->OHI_SE5SEQ := SE5->E5_SEQ
				OHI->OHI_IDORIG := SE5->E5_IDORIG
				If lCpoNatRec
					OHI->OHI_NATREC := J256NatRec(nRegCmp)
					OHI->OHI_COTAC  := IIF(SE5->E5_TXMOEDA == 0, 1, SE5->E5_TXMOEDA)
				EndIf
				If lCpoMoeda
					OHI->OHI_CMOEDA := cMoedaFat
				EndIf
				If lCpoTpBx
					OHI->OHI_MOTBX := SE5->E5_MOTBX
				EndIf
				OHI->( MsUnLock() )
			Else
				lInsertOHI := .F.
				DisarmTransaction()
				Exit
			EndIf
		Next nVal
	END TRANSACTION

Return ( lInsertOHI )

//-------------------------------------------------------------------
/*/{Protheus.doc} J256NatRec
Obtem a natureza vinculada ao banco da baixa. Quando for compensa��o 
busca a natureza vinculado ao banco do t�tulo de RA.

@param   nRegCmp , numerico, Recno do registro SE1 compensado (RA)

@return  cNatRec, caractere, Natureza de recebimento

@author  Jonatas Martins
@since   02/10/2018
@version 12.1.17
/*/
//-------------------------------------------------------------------
Static Function J256NatRec(nRegCmp)
	Local aAreaSE1  := {}
	Local cNatRec   := ""
		
	If Empty(SE5->E5_DOCUMENT)
		cNatRec := JurBusNat("", SE5->E5_BANCO, SE5->E5_AGENCIA, SE5->E5_CONTA)
	Else 
		//==============
		// Compensa��o
		//==============
		aAreaSE1 := SE1->(GetArea())
		SE1->(DbGoTo(nRegCmp))
		If SE1->(! Eof())
			cNatRec := JurBusNat("", SE1->E1_PORTADO, SE1->E1_AGEDEP, SE1->E1_CONTA)
		EndIf
		RestArea(aAreaSE1)
	EndIf

Return (cNatRec)

//-------------------------------------------------------------------
/*/{Protheus.doc} GetItem
Rotina de c�lculo de rateio do valor da baixa entre os casos da fatura

@param  cEscrit, caracter, C�digo do Escrit�rio da Fatura
@param  cFatura, caracter, C�digo da Fatura
@return cItem  , caracter, �ltimo item da tabela de rastreamento por fatura

@author  Luciano Santos
@since   08/02/2018
@version 12.1.20
/*/
//-------------------------------------------------------------------
Static Function GetItem( cEscrit , cFatura )
	Local cQryRes  := ""
	Local cQryItem := ""
	Local cItem    := ""

	cQryItem := "SELECT MAX(OHI_ITEM) OHI_ITEM "+ CRLF
	cQryItem += "FROM " + RetSqlName("OHI") + " " + CRLF
	cQryItem += "WHERE OHI_FILIAL = '" + xFilial("OHI") + "' " + CRLF
	cQryItem +=   "AND OHI_CESCR = '" + cEscrit + "' " + CRLF
	cQryItem +=   "AND OHI_CFATUR = '" + cFatura + "' " + CRLF
	cQryItem +=   "AND D_E_L_E_T_ = ' ' "

	cQryRes := GetNextAlias()
	cQryItem  := ChangeQuery( cQryItem )

	DbUseArea(.T.,"TOPCONN",TcGenQry(,,cQryItem),cQryRes,.T.,.T.)

	If (cQryRes)->( ! Eof() ) .And. ! Empty( (cQryRes)->OHI_ITEM )
		cItem := (cQryRes)->OHI_ITEM
	Else
		cItem := StrZero( 0 , TamSX3("OHI_ITEM")[1] )
	EndIf

	(cQryRes)->( DbCloseArea() )

Return ( cItem )

//-------------------------------------------------------------------
/*/{Protheus.doc} CalRatCas
Rotina de c�lculo de rateio do valor da baixa entre os casos da fatura

@param  aDados  , Dados dos casos da fatura
@param  cChvTit , Chave para localizar os recebimentos por casos
@param  nValCRec, Valor do recebimento
@param  nDescRec, Valor do descontos
@param  nAcreRec, Valor do acrescimo
@param  dDtMov  , Data da movimenta��o (SE5)

@return aDados  , array   , Dados rateado dos casos da fatura

@author  Thiago Malaquias
@since   08/02/2018
@version 12.1.20
/*/
//-------------------------------------------------------------------
Static Function CalRatCas( aDados, cEscrit, cFatura, nValCRec, nDescRec, nAcreRec, dDtMov)
	Local nTotHon    := 0 // Valor total de horarios na fatura
	Local nTotDesp   := 0 // Valor total de despesas na fatura
	Local nTotFat    := 0 // Valor total da fatura
	Local nCasos     := 0 // Posi��o da linha do caso da fatura no array aDados
	Local cTpPriori  := SuperGetMv('MV_JTPRIO',,'1') //1-Prioriza despesas 2-Proporcional
	Local nTamVlH    := TamSX3("OHI_VLHCAS")[2]
	Local nTamVlD    := TamSX3("OHI_VLDCAS")[2]
	Local nDecVlacrH := TamSX3("OHI_VLACRH")[2]
	Local nDecVlacrD := TamSX3("OHI_VLACRD")[2]
	Local nDecVldesH := TamSX3("OHI_VLDESH")[2]
	Local nDecVldesD := TamSX3("OHI_VLDESD")[2]
	Local aSaldosOHH := {} // Recebe os saldos (honorario e despesa) da tabela OHH
	Local lDespTrib  := OHH->(ColumnPos("OHH_VLREMB")) > 0

	Local nNXADesRem := 0 // Valor de Despesa Reembols�vel da Fatura
	Local nNXADesTri := 0 // Valor de Despesa Tribut�vel da Fatura
	Local nNXATxAdm  := 0 // Valor de Taxa Adm. da Fatura
	Local nNXAGross  := 0 // Valor de Gross Up da Fatura
	Local nNXATotTri := 0 // Valor Total de Despesas Tribut�veis (Despesa Tribut�vel + Taxa Adm. + Gross Up)

	Local nBaseRemb  := 0
	Local nBaseTrib  := 0
	Local nBaseTxAd  := 0
	Local nBaseGros  := 0
	Local nBaseDesp  := 0

	Local cUltAnoMes := J256AnoMes()

	aEval(aDados, {|x| nTotHon += x[7], nTotDesp += x[8], nNXADesRem += x[13], nNXADesTri += x[14], nNXATxAdm += x[15], nNXAGross += x[16] })
	nTotFat := nTotHon + nTotDesp

	nNXATotTri := nNXADesTri + nNXATxAdm + nNXAGross

	aDados := RatVlAcess('1', aDados, nAcreRec, 10, 12, nDecVlacrH, nDecVlacrD) // Faz o rateio de Acr�scimo primeiro priorizando os honor�rios

	cTitulo := SE1->E1_FILIAL + SE1->E1_PREFIXO + SE1->E1_NUM + SE1->E1_PARCELA + SE1->E1_TIPO

	If lDespTrib
		aSaldosOHH := JurGetDados( "OHH", 1, cTitulo + cUltAnoMes, {"OHH_VLFATH", "OHH_VLFATD", "OHH_SDREMB", "OHH_SDTRIB", "OHH_SDTXAD", "OHH_SDGROS"} )
	Else
		aSaldosOHH := JurGetDados( "OHH", 1, cTitulo + cUltAnoMes, {"OHH_VLFATH", "OHH_VLFATD"} )
	EndIf

	For nCasos := 1 To Len(aDados)

		// Rateio de Honorarios e Despesas
		If cTpPriori == '1' // 1-Prioriza despesas
			If nCasos == 1  // Primeiro Caso
				aDados   := J256PriDes(aDados, cEscrit, cFatura, nValCRec, nDecVldesD, aSaldosOHH[2], "OHI", cTitulo) // Faz o rateio na despesa primeiro para calcular o rateio no honor�rio
				nTotDesp := 0
				aEval(aDados, {|x| nTotDesp += x[8] }) // Soma o novo valor do total de despesas rateado (Verifica se tem excedente para o honor�rio)
			EndIf
			If !Empty(aSaldosOHH[1])
				aDados[nCasos][7] := RatPontoFl(aDados[nCasos][7], nTotHon, (nValCRec - nTotDesp), nTamVlH) // Valor de recebimento rateado pelo valor de honorarios do caso
			Else
				aDados[nCasos][7] := 0
			EndIf
		Else // 2-Proporcional
			// Reajuste dos valores dos casos
			aDados[nCasos][7]  := RatPontoFl(aDados[nCasos][7] , nTotFat, nValCRec, nTamVlH) // Valor de recebimento rateado pelo valor de honorarios do caso
			aDados[nCasos][8]  := RatPontoFl(aDados[nCasos][8] , nTotFat, nValCRec, nTamVlD) // Valor de recebimento rateado pelo valor de despesas totais do caso
			aDados[nCasos][13] := RatPontoFl(aDados[nCasos][13], nTotFat, nValCRec, nTamVlD) // Valor de recebimento rateado pelo valor de despesas reembols�veis do caso
			aDados[nCasos][14] := RatPontoFl(aDados[nCasos][14], nTotFat, nValCRec, nTamVlD) // Valor de recebimento rateado pelo valor de despesas tribut�veis do caso
			aDados[nCasos][15] := RatPontoFl(aDados[nCasos][15], nTotFat, nValCRec, nTamVlD) // Valor de recebimento rateado pelo valor de taxa administrativa do caso
			aDados[nCasos][16] := RatPontoFl(aDados[nCasos][16], nTotFat, nValCRec, nTamVlD) // Valor de recebimento rateado pelo valor de taxa gross up do caso
		EndIf

	Next nCasos

	aDados := RatVlAcess('2', aDados, nDescRec, 9, 11, nDecVldesH, nDecVldesD) // Faz o rateio de Desconto depois para aplicar o maximo de valor possivel nos honor�rios
	
	If lDespTrib
		If cTpPriori == '1' // Prioriza Despesa
			// Valores com base na propor��o entre baixa e o valor da parcela utilizadas para ajuste de saldo
			nBaseRemb := Round(IIf(nValCRec > aSaldosOHH[3], aSaldosOHH[3], nValCRec), nTamVlD)
			nBaseTrib := Round(IIf(nValCRec > aSaldosOHH[3], IIf(nValCRec - aSaldosOHH[3] < aSaldosOHH[4] + aSaldosOHH[5] + aSaldosOHH[6], (nValCRec - aSaldosOHH[3]) * (nNXADesTri / nNXATotTri) , aSaldosOHH[4] ), 0 ), nTamVlD)
			nBaseTxAd := Round(IIf(nValCRec > aSaldosOHH[3], IIf(nValCRec - aSaldosOHH[3] < aSaldosOHH[4] + aSaldosOHH[5] + aSaldosOHH[6], (nValCRec - aSaldosOHH[3]) * (nNXATxAdm  / nNXATotTri) , aSaldosOHH[5] ), 0 ), nTamVlD)
			nBaseGros := Round(IIf(nValCRec > aSaldosOHH[3], IIf(nValCRec - aSaldosOHH[3] < aSaldosOHH[4] + aSaldosOHH[5] + aSaldosOHH[6], (nValCRec - aSaldosOHH[3]) * (nNXAGross  / nNXATotTri) , aSaldosOHH[6] ), 0 ), nTamVlD)
		Else // Proporcional
			// Valores com base na propor��o entre baixa e o valor da parcela utilizadas para ajuste de saldo
			If SE1->E1_SALDO == 0
				nBaseRemb := aSaldosOHH[3]
				nBaseTrib := aSaldosOHH[4]
				nBaseTxAd := aSaldosOHH[5]
				nBaseGros := aSaldosOHH[6]
			Else
				nBaseRemb := RatPontoFl(nValCRec, nTotFat, nNXADesRem, nTamVlD)
				nBaseTrib := RatPontoFl(nValCRec, nTotFat, nNXADesTri, nTamVlD)
				nBaseTxAd := RatPontoFl(nValCRec, nTotFat, nNXATxAdm , nTamVlD)
				nBaseGros := RatPontoFl(nValCRec, nTotFat, nNXAGross , nTamVlD)
			EndIf
			
			nBaseDesp := nBaseRemb + nBaseTrib + nBaseTxAd + nBaseGros // Base de despesa total
			aDados    := AjustSaldo(aDados, nBaseDesp, 0, 8) // Faz o ajuste do valor de despesa total. Pois pode estar diferente dos valores de despesas desmembrados
		EndIf

		aDados := AjustSaldo(aDados, nBaseRemb, 0, 13) // Ajusta saldo do arredondamento do rateio para Despesas Reembols�veis
		aDados := AjustSaldo(aDados, nBaseTrib, 0, 14) // Ajusta saldo do arredondamento do rateio para Despesas Tribut�veis
		aDados := AjustSaldo(aDados, nBaseTxAd, 0, 15) // Ajusta saldo do arredondamento do rateio para Taxa Administrativa
		aDados := AjustSaldo(aDados, nBaseGros, 0, 16) // Ajusta saldo do arredondamento do rateio para Taxa Gross Up
	EndIf

	aDados := AjustSaldo(aDados, nValCRec,  7,  8) // Ajusta saldo do arredondamento do rateio para honorarios e despesas
	aDados := AjustSaldo(aDados, nDescRec,  9, 11) // Ajusta saldo do arredondamento do rateio para descontos
	aDados := AjustSaldo(aDados, nAcreRec, 10, 12) // Ajusta saldo do arredondamento do rateio para acr�scimos

Return ( aDados )

//-------------------------------------------------------------------
/*/{Protheus.doc} RatVlAcess(cTipo, aDados, nValAcess, nPosH, nPosD, nDecVlH, nDecVlD)
Rotina de c�lculo de rateio do valores acess�rios (descontos / acrescimos) entre os casos da fatura

@param  cTipo     , Indica se o valor acessorio � '1' - acr�scimo ou '2' - desconto
@param  aDados    , Dados da fatura/caso
@param  nValAcess , Valor Acess�rio do movimento (acr�scimo ou desconto)
@param  nPosH     , Posi��o no array referente aos valores acess�rios de Honor�rios
@param  nPosD     , Posi��o no array referente aos valores acess�rios de Despesas
@param  nDecVlH   , N�mero de casas decimanis referente valores acess�rios de Honor�rios
@param  nDecVlD   , N�mero de casas decimanis referente aos valores acess�rios de Despesas

@Obs Necessario ratear a despesa antes para calcular o rateio de honor�rios
@author  Luciano Pereira dos Santos
@since   08/02/2018
@version 12.1.20
/*/
//-------------------------------------------------------------------
Static Function RatVlAcess(cTipo, aDados, nValAcess, nPosH, nPosD, nDecVlH, nDecVlD)
	Local nCasos     := 0 // Posi��o da linha do caso da fatura no array aDados
	Local nTotAcesH  := 0 // Total de valor acessorio nos honor�rios na fatura
	Local nTotDesp   := 0 // Total de despesas na fatura
	Local nVlAcesDes := 0 // Valor acess�rio rateado pelo valor de despesa no caso

	aEval(aDados, {|x| nTotDesp += x[8] })

	For nCasos := 1 to Len(aDados)

		If nCasos == 1
			aDados := GetVlAcesH(aDados, nValAcess, nPosH, nDecVlH, cTipo) //Prioriza o rateio de desconto/acrescimo primeiro no honor�rio para calcular o rateio de desconto/acrescimo na despesa
			aEval(aDados, {|x| nTotAcesH += x[nPosH] }) // Soma o valor do total de acrescimo dos honor�rios na fatura
		EndIf

		nVlAcesDes := RatPontoFl(aDados[nCasos][8], nTotDesp, (nValAcess - nTotAcesH), nDecVlD)
		aDados[nCasos][nPosD] := nVlAcesDes

	Next nCasos

Return ( aDados )

//-------------------------------------------------------------------
/*/{Protheus.doc} GetVlAcesH(aDados, nValAcess, nPosH, nDecVlH, cTipo)
Rotina de c�lculo de rateio descontos / acrescimo no valor honorarios do casos da fatura

@param  cTipo    ,  caracter ,  Indica se o valor acessorio � '1' - acr�scimo ou '2' -desconto
@param  aDados   ,  array    ,  Dados da fatura/caso
@param  nValAcess,  numerico,  Valor Acess�rio do movimento (acr�scimo ou desconto)
@param  nPosH     ,  numerico,  Posi��o no array referente aos valores acess�rios de Honor�rios
@param  nDecVlH   ,  numerico,   N�mero de casas decimanis referente valores acess�rios de Honor�rios

@return aDados , array   , array aDados com os valores de descontos / acrescimo rateados para honorario.

@Obs Necessario ratear descontos / acrescimo nos honoararios antes para calcular o residual para as despesas
@author  Luciano Pereira dos Santos
@since   08/02/2018
@version 12.1.20
/*/
//-------------------------------------------------------------------
Static Function GetVlAcesH(aDados, nValAcess, nPosH, nDecVlH, cTipo)
	Local nTotHon    := 0 // Valor total de honor�rios na fatura
	Local nValMax    := 0 // Verifica se o valor recebido excede o total de honor�rios
	Local nCasos     := 0 // Posi��o da linha do caso da fatura no array aDados
	Local nVlAcesHon := 0 // Valor acess�rio rateado pelo valor de honor�rios do caso

	aEval(aDados, {|x| nTotHon += x[7] })

	If cTipo == '1' //Se for acr�scimo, basta existir valor de honor�rios para aplicar todo o acr�scimo
		nValMax := nValAcess
	Else
		nValMax := Iif((nTotHon > nValAcess), nValAcess, nTotHon) //Se for descconto, aplica o maximo valor possivel nos honor�rios
	EndIf

	For nCasos := 1 to Len(aDados)

		nVlAcesHon := RatPontoFl(aDados[nCasos][7], nTotHon, nValMax, nDecVlH)
		aDados[nCasos][nPosH] := nVlAcesHon

	Next nCasos

	If cTipo == '1' .And. nTotHon > 0 //Se for acr�scimo de uma fatura com honor�rios, o saldo de acr�scismo tamb�m tem que ficar nos honor�rios
		aDados := AjustSaldo(aDados, nValAcess, 10, 12) //Ajusta saldo do arredondamento do rateio para acr�scimos
	EndIf

Return ( aDados )


//-------------------------------------------------------------------
/*/{Protheus.doc} RatPontoFl(nValUnt, nValtot, nValRat, nDecRet)
Rotina de c�lculo de rateio do valor proporcional do lan�amento (honorario/despesas) entre os casos da fatura


@param  nValUnt , numerico, Valor unit�rio do lan�amento (honorarios/Despesa) por caso
@param  nValtot , numerico, Valor total do lan�amento na fatura
@param  nValRat , numerico, Valor a ser rateado entre os casos
@param  nDecRet , numerico, Precis�o decimal do valor calculado

@return nRet    , numerico, Valor ratedo do lan�amento

@Obs: Necess�rio ratear por ponto flutuante para minimizar distor��es no rateio de valor com grandes diferen�as
de dimens�o entre honor�rios e despesas.

@author  Luciano Pereira dos Santos
@since   08/02/2018
@version 12.1.20
/*/
//-------------------------------------------------------------------
Function RatPontoFl(nValUnt, nValtot, nValRat, nDecRet)
Local nRet       := 0
Local nDec       := 20 // Numero e casas decimais para c�lculo de ponto flutuante
Local fPerLanCas := DEC_CREATE("0",32,nDec) // Percentual do valor do lan�amento (honorarios/Despesa) no caso pelo total da fatura
Local nValLanRat := 0 // Valor do lan�amento rateado

	fPerLanCas := DEC_DIV(DEC_CREATE(cValToChar(nValUnt), 32, nDec), DEC_CREATE(cValToChar(nValtot), 32, nDec))
	nValLanRat := Val(cValToChar(DEC_RESCALE(DEC_MUL(DEC_CREATE(nValRat,32,nDec), fPerLanCas),8,0)))
	nRet       := Round(nValLanRat, nDecRet)

Return nRet


//-------------------------------------------------------------------
/*/{Protheus.doc} J256PriDes()
Rotina de c�lculo de rateio do valor de despesas da baixa entre os casos da fatura
conforme o parametro MV_JTPRIO

@param  aDados ,    array   ,  Dados da fatura/caso
@param  cEscrit,    caracter, C�digo do Escrit�rio da Fatura
@param  cFatura,    caracter, C�digo da Fatura
@param  nValCRec,   numerico, Valor recebido
@param  nDecVlDp,   numerico, N�mero de casas decimanis referente aos valores de Despesas
@param  nTotFatDes, numerico, Total de Despesa na Fatura
@param  cAlias,     caracter, Define de onde est� sendo chamado a rotina
@param  cTitulo,    caracter, Chave do titulo do Contas a Receber
@param  lInclui,    Verifica se ser� gravado um novo registro na OHH
@param  aValNXA     Valores originais da fatura
@param  cAnoMes     Ano-M�s da movimenta��o da OHH (usado somente via UPDRASTR)

@return aDados , array   , array aDados com os valores de despesa rateados.

@Obs Necessario ratear a despesa antes para calcular o rateio de honor�rios
@author  Luciano Pereira dos Santos
@since   08/02/2018
/*/
//-------------------------------------------------------------------
Function J256PriDes(aDados, cEscrit, cFatura, nValCRec, nDecVlDp, nTotDesTit, cAlias, cTitulo, lInclui, aValNXA, cAnoMes)
	Local aDespUtil  := {}
	Local aDespPri   := {0, 0, 0, 0, 0} // Valores de Despesas - [1] Total | [2] Reembols�vel | [3] Tribut�vel | [4] Tx. Adm. | [5] Gross Up
	Local nTotDesCas := 0 // Valor total de despesas dos Casos
	Local nCasos     := 0
	Local nVlRatDes  := 0 // Valor de recebimento rateado pelo valor de despesas do caso
	Local nVlRatRem  := 0 // Valor de recebimento rateado pelo valor de despesas reembols�veis do caso
	Local nVlRatTrib := 0 // Valor de recebimento rateado pelo valor de despesas tribut�veis do caso
	Local nVlRatTxAd := 0 // Valor de recebimento rateado pelo valor de taxa administrativa do caso
	Local nVlRatGros := 0 // Valor de recebimento rateado pelo valor de taxa gross up do caso
	Local nValParc   := 0 // Valor total da parcela
	Local lParc1     := .F. // Indica que � a primeira parcela

	Local nNXATotHon := 0 // Valor Total de Honor�rios da Fatura
	Local nNXATotDes := 0 // Valor Total de Despesas da Fatura
	Local nNXADesRem := 0 // Valor de Despesa Reembols�vel da Fatura
	Local nNXADesTri := 0 // Valor de Despesa Tribut�vel da Fatura
	Local nNXATxAdm  := 0 // Valor de Taxa Adm. da Fatura
	Local nNXAGross  := 0 // Valor de Gross Up da Fatura
	Local nNXATotTri := 0 // Valor Total de Despesas Tribut�veis (Despesa Tribut�vel + Taxa Adm. + Gross Up)

	Local nOHHDesRem := 0 // Valor de Despesa Reembols�vel da Posi��o Hist�rica Ctas Receber
	Local nOHHDesTri := 0 // Valor de Despesa Tribut�vel da Posi��o Hist�rica Ctas Receber
	Local nOHHTxAdm  := 0 // Valor de Taxa Adm. da Posi��o Hist�rica Ctas Receber
	Local nOHHGross  := 0 // Valor de Gross Up da Posi��o Hist�rica Ctas Receber
	Local nOHHTotTri := 0 // Valor Total de Despesas Tribut�veis (Despesa Tribut�vel + Taxa Adm. + Gross Up)

	Local lDespTrib  := OHH->(ColumnPos("OHH_VLREMB")) > 0
	Local cUltAnoMes := J256AnoMes()

	Default aDados     := {}
	Default cEscrit    := ""
	Default cFatura    := ""
	Default cTitulo    := ""
	Default nDecVlDp   := 0
	Default lInclui    := .F.
	Default aValNXA    := {}
	Default cAnoMes    := Nil

	If cAlias == "OHI"
		aDespUtil := GetTotDesp(cEscrit, cFatura, cTitulo) // Total de despesas utilizadas no caso.
		lParc1    := aDespUtil[1] == 0
		
		aEval(aDados, {|x| nTotDesCas += x[8], nNXADesRem += x[13], nNXADesTri += x[14], nNXATxAdm += x[15], nNXAGross += x[16] })
		nNXATotTri := nNXADesTri + nNXATxAdm + nNXAGross
	
		If lDespTrib
			aDadosOHH := JurGetDados("OHH", 1, cTitulo + cUltAnoMes, {"OHH_VLREMB", "OHH_VLTRIB", "OHH_VLTXAD", "OHH_VLGROS"} )
			If Len(aDadosOHH) == 4
				nOHHDesRem := aDadosOHH[1]
				nOHHDesTri := aDadosOHH[2]
				nOHHTxAdm  := aDadosOHH[3]
				nOHHGross  := aDadosOHH[4]
				nOHHTotTri := nOHHDesTri + nOHHTxAdm + nOHHGross
			EndIf
		EndIf

		aDespPri[1] := Iif(nTotDesTit - aDespUtil[1] > nValCRec, nValCRec, nTotDesTit - aDespUtil[1]) // Despesa Total
		aDespPri[2] := J256CalRem(aDespUtil[2], nValCRec, nOHHDesRem) // Despesa Reembols�vel
		aDespPri[3] := J256CalTri(lParc1, aDespUtil[3], nValCRec, aDespPri[1], aDespPri[2], nOHHDesTri, nOHHTotTri, nOHHDesRem) // Despesa Tribut�vel
		aDespPri[4] := J256CalTri(lParc1, aDespUtil[4], nValCRec, aDespPri[1], aDespPri[2], nOHHTxAdm , nOHHTotTri, nOHHDesRem) // Taxa Administrativa
		aDespPri[5] := J256CalTri(lParc1, aDespUtil[5], nValCRec, aDespPri[1], aDespPri[2], nOHHGross , nOHHTotTri, nOHHDesRem) // Taxa Gross Up

		For nCasos := 1 to Len(aDados)

			nVlRatDes  := RatPontoFl(aDados[nCasos][8] , nTotDesCas, aDespPri[1], nDecVlDp)
			nVlRatRem  := RatPontoFl(aDados[nCasos][13], nNXADesRem, aDespPri[2], nDecVlDp)
			nVlRatTrib := RatPontoFl(aDados[nCasos][14], nNXADesTri, aDespPri[3], nDecVlDp)
			nVlRatTxAd := RatPontoFl(aDados[nCasos][15], nNXATxAdm , aDespPri[4], nDecVlDp)
			nVlRatGros := RatPontoFl(aDados[nCasos][16], nNXAGross , aDespPri[5], nDecVlDp)

			aDados[nCasos][8]  := nVlRatDes
			aDados[nCasos][13] := nVlRatRem
			aDados[nCasos][14] := nVlRatTrib
			aDados[nCasos][15] := nVlRatTxAd
			aDados[nCasos][16] := nVlRatGros

		Next nCasos

		aDados := AjustSaldo(aDados, aDespPri[1], 0, 8) // Faz o ajuste do valor de despesa total. Pois pode estar diferente dos valores de despesas desmembrados
		
		aDespPri := aClone(aDados)

	Else

		If Len(aValNXA) >= 6
			nNXATotHon := aValNXA[1]
			nNXATotDes := aValNXA[2]
			nNXADesRem := aValNXA[3]
			nNXADesTri := aValNXA[4]
			nNXATxAdm  := aValNXA[5]
			nNXAGross  := aValNXA[6]

			nNXATotTri := nNXADesTri + nNXATxAdm + nNXAGross
		EndIf

		aDespUtil := IIf(FindFunction("J255GetDes"), J255GetDes(cEscrit, cFatura, lInclui, cAnoMes), 0) // Total de despesas gravadas nas parcelas
		
		lParc1    := aDespUtil[1] == 0
		nValParc  := SE1->E1_VALOR // Valor total da parcela
		
		aDespPri[1] := Iif(nTotDesTit - aDespUtil[1] > nValParc, nValParc, nTotDesTit - aDespUtil[1]) // Valor total de despesas (Desp Reemb + Desp Trib + Tx Adm. + Gross UP)
		aDespPri[2] := J256CalRem(aDespUtil[2], nValParc, nNXADesRem) // Despesa Reembols�vel
		aDespPri[3] := J256CalTri(lParc1, aDespUtil[3], nValParc, aDespPri[1], aDespPri[2], nNXADesTri, nNXATotTri, nNXADesRem) // Despesa Tribut�vel
		aDespPri[4] := J256CalTri(lParc1, aDespUtil[4], nValParc, aDespPri[1], aDespPri[2], nNXATxAdm , nNXATotTri, nNXADesRem) // Taxa Administrativa
		aDespPri[5] := J256CalTri(lParc1, aDespUtil[5], nValParc, aDespPri[1], aDespPri[2], nNXAGross , nNXATotTri, nNXADesRem) // Taxa Gross Up

	EndIf

Return (aDespPri)

//-------------------------------------------------------------------
/*/{Protheus.doc} J256CalRem()
Calcula o valor para distribui��o dos valores de despesas reembols�veis

@param  nValUtil   , Valor de despesa reembols�vel j� utilizado
@param  nValParc   , Valor da parcela atual
@param  nDesRemTot , Valor de despesa reembols�vel da fatura ou OHH

@author  Abner Foga�a / Jorge Martins
@since   14/02/2020
/*/
//-------------------------------------------------------------------
Function J256CalRem(nValUtil, nValParc, nDesRemTot)
	Local nValor := 0

	If nValUtil == 0 // Primeira vez que ser� atribu�do valor
		nValor := IIf(nValParc < nDesRemTot, nValParc, nDesRemTot)
	Else
		If nDesRemTot != nValUtil // Valor j� utilizado (parcelas anteriores) diferente do valor de despesa reembols�vel total (fatura ou OHH)
			If nValParc < (nDesRemTot - nValUtil)
				nValor := nValParc
			Else
				nValor := nDesRemTot - nValUtil
			EndIf
		EndIf
	EndIf

	nValor := IIf(nValor < 0, 0, nValor)

Return nValor

//-------------------------------------------------------------------
/*/{Protheus.doc} J256CalTri()
Calcula o valor para distribui��o dos valores de despesas tribut�veis
(despesa tribut�vel, taxa administrativa e taxa gross up).

@param  lParc1     , Indica se � a primeira parcela
@param  nValUtil   , Valor j� utilizado (Caso seja 1� parcela ser� 0)
@param  nValParc   , Valor da parcela atual
@param  nTotDesPar , Valor Total de despesas da parcela atual
@param  nDesRemPar , Valor de despesa reembols�vel da parcela atual
@param  nNXAValTri , Valor da despesa tribut�vel da fatura (despesa tribut�vel ou taxa administrativa ou taxa gross up)
@param  nNXATotTri , Valor total de despesa tribut�vel da fatura (despesa tribut�vel + taxa administrativa + taxa gross up)
@param  nDesRemTot , Valor de despesa reembols�vel da fatura ou  OHH

@author  Abner Foga�a / Jorge Martins
@since   14/02/2020
/*/
//-------------------------------------------------------------------
Function J256CalTri(lParc1, nValUtil, nValParc, nTotDesPar, nDesRemPar, nNXAValTri, nNXATotTri, nDesRemTot)
	Local nValor    := 0
	Local nPropTrib := nNXAValTri / nNXATotTri // Calcula propor��o de um dos 3 valores sobre o total (Desp Trib + Tx Adm. + Gross Up)

	If lParc1 .And. nValUtil == 0 .And. nValParc > nNXAValTri
		If nNXATotTri > nValParc - nDesRemTot // Verifica se o Total Tribut�vel � maior que o valor da parcela menos o valor de Desp. Reembols�vel
			If nValParc > nDesRemTot
				nValor := (nValParc - nDesRemTot) * nPropTrib
			EndIf
		Else
			nValor := nNXAValTri
		EndIf
	Else
		If nDesRemTot != nValUtil
			If nValParc < (nNXAValTri - nValUtil - nDesRemPar)
				nValor := (nValParc - nDesRemPar) * nPropTrib
			Else
				nValor := (nTotDesPar - nDesRemPar) * nPropTrib
			EndIf
		EndIf
	EndIf

	nValor := IIf(nValor < 0, 0, nValor)

Return nValor

//-------------------------------------------------------------------
/*/{Protheus.doc} GetTotDesp
Rotina de c�lculo de rateio do valor da baixa entre os casos da fatura por t�tulo

@param  cEscrit, caracter, C�digo do Escrit�rio da Fatura
@param  cFatura, caracter, C�digo da Fatura
@param  cTitulo, caracter, Chave do t�tulo na SE1

@return aDesp  , numerico, Valor total da despesa

@author  Thiago Malaquias
@since   08/02/2018
/*/
//-------------------------------------------------------------------
Static Function GetTotDesp(cEscrit, cFatura, cTitulo)
	Local aDesp     := {0, 0, 0, 0, 0}
	Local cQuery    := ""
	Local cQryRes   := ""
	Local lDespTrib := OHI->(ColumnPos("OHI_VLREMB")) > 0 // Prote��o
	
	Default cTitulo   := ""

	cQuery := " SELECT SUM(OHI_VLDCAS) OHI_VLDCAS "
	If lDespTrib
		cQuery +=   ", SUM(OHI_VLREMB) OHI_VLREMB, SUM(OHI_VLTRIB) OHI_VLTRIB, SUM(OHI_VLTXAD) OHI_VLTXAD, SUM(OHI_VLGROS) OHI_VLGROS "
	EndIf
	cQuery +=  " FROM " + RetSqlName("OHI") + " "
	cQuery += " WHERE OHI_FILIAL = '" + xFilial("OHI") + "' "
	cQuery +=   " AND OHI_CESCR  = '" + cEscrit + "' "
	cQuery +=   " AND OHI_CFATUR = '" + cFatura + "' "
	cQuery +=   " AND OHI_CHVTIT = '" + cTitulo + "' "
	cQuery +=   " AND D_E_L_E_T_ = ' ' "

	cQryRes := GetNextAlias()
	cQuery  := ChangeQuery( cQuery )

	DbUseArea(.T.,"TOPCONN", TcGenQry(,,cQuery), cQryRes, .T., .T.)

	If (cQryRes)->(!Eof()) .And. !Empty((cQryRes)->OHI_VLDCAS)
		aDesp[1] := (cQryRes)->OHI_VLDCAS
		If lDespTrib
			aDesp[2] := (cQryRes)->OHI_VLREMB
			aDesp[3] := (cQryRes)->OHI_VLTRIB
			aDesp[4] := (cQryRes)->OHI_VLTXAD
			aDesp[5] := (cQryRes)->OHI_VLGROS
		EndIf
	EndIf

	(cQryRes)->(DbCloseArea())

Return aDesp


//-------------------------------------------------------------------
/*/{Protheus.doc} AjustSaldo(aDados, nValor, nVldPosH, nVldPosD)
Rotina de ajuste de saldo do arredondamento do rateio com o valor total

@param  aDados ,   array   ,   Dados da fatura/caso
@param  nValConf,  numerico,  Valor Total para verificar o ajuste de saldo do rateio
@param  nVldPosH,  numerico,  Posi��o no array referente aos valores de honorarios
@param  nVldPosD,  numerico,  Posi��o no array referente aos valores de despesa

@return aDados , array   , array aDados com o ajuste de saldo realizado.

@author  Luciano Pereira dos Santos
@since   12/02/2018
@version 12.1.20
/*/
//-------------------------------------------------------------------
Static Function AjustSaldo(aDados, nValConf, nVldPosH, nVldPosD)
	Local nPosD    := 0
	Local nMaiorD  := 0
	Local nTotDesp := 0
	Local nPosH    := 0
	Local nMaiorH  := 0
	Local nTotHon  := 0
	Local nValDiff := 0
	Local nI       := 0

	Default aDados    := {}
	Default nValConf  := 0
	Default nVldPosD  := 0
	Default nVldPosH  := 0

	For nI := 1 To Len(aDados)
		If nVldPosH > 0
			If nMaiorH <= aDados[nI][nVldPosH]
				nMaiorH := aDados[nI][nVldPosH]
				nPosH := nI
			EndIf
			nTotHon += aDados[nI][nVldPosH]
		EndIf

		If nVldPosD > 0
			If nMaiorD <= aDados[nI][nVldPosD]
				nMaiorD := aDados[nI][nVldPosD]
				nPosD := nI
			EndIf
			nTotDesp += aDados[nI][nVldPosD]
		EndIf

	Next nI

	nValDiff := nValConf - (nTotHon + nTotDesp)

	If nValDiff != 0
		If (nPosH > 0) .And. (nMaiorH > 0)
			aDados[nPosH][nVldPosH] += nValDiff
		Elseif (nPosD > 0)
			aDados[nPosD][nVldPosD] += nValDiff // Aplica a diferen�a no total de despesas
		EndIf
	EndIf

Return ( aDados )


//-------------------------------------------------------------------
/*/{Protheus.doc} J256DelRas
Remove rastreamento dos casos da fatura no cancelamento da baixa
a receber.

@param  nSE1Recno, numerico, Recno do registro SE1
@param  nSE5Recno, numerico, Recno do registro SE5
@param  aOHIBxAnt, Array,    Valor total de baixas de honor�rios e despesas

@return lCancBx  , logico  , Sucesso ou falha na grava��o do rastreamento

@author  Jonatas Martins
@since   09/02/2018
@version 12.1.20
/*/
//-------------------------------------------------------------------
Function J256DelRas( nSE1Recno , nSE5Recno, aOHIBxAnt)
	Local aAreas    := { SE1->(GetArea()) , SE5->(GetArea()) , OHI->(GetArea()) , GetArea() }
	Local cTmpOHI   := ""
	Local cEscrit   := ""
	Local cFatura   := ""
	Local cTitulo   := SE1->E1_FILIAL + SE1->E1_PREFIXO + SE1->E1_NUM + SE1->E1_PARCELA + SE1->E1_TIPO
	Local lCancBx   := .T.
	Default nSE1Recno := 0
	Default nSE5Recno := 0
	//--------------------------------------------
	// Prote��o devido ao congelamento do release
	//---------------------------------------------
	If AliasIndic("OHI")
		SE1->( DbGoTo( nSE1Recno ) )

		If SE1->( ! Eof() ) .And. ! Empty( SE1->E1_JURFAT )
			SE5->( DbGoTo( nSE5Recno ) )

			If SE5->( ! Eof() ) .And. SE5->E5_TIPO <> PadR( "RA" , TamSX3("E5_TIPO")[1] )
				cTmpOHI := GetDataCan()

				If (cTmpOHI)->( ! Eof() )
					aOHIBxAnt := J255TotBx(cTitulo)
					lCancBx   := DeleteOHI( cTmpOHI )
					//---------------------------------
					// Atualiza fila de sincroniza��o
					//---------------------------------
					cEscrit   := OHI->OHI_CESCR
					cFatura   := OHI->OHI_CFATUR
					lCancBx   := lCancBx .And. J170GRAVA( "JURA256" , xFilial("NXA") +  cEscrit + cFatura , GetSincOper( cEscrit , cFatura ) )
				EndIf
				(cTmpOHI)->( DbCloseArea() )
			EndIf
		EndIf
	EndIf

	Aeval( aAreas , {|aArea| RestArea( aArea ) } )
Return ( lCancBx )


//-------------------------------------------------------------------
/*/{Protheus.doc} GetDataCan
Busca registros do rastreamento vinculados a baixa cancelada

@return cTmpOHI, caracter, Alias tempor�rio

@author  Jonatas Martins
@since   09/02/2018
@version 12.1.20
/*/
//-------------------------------------------------------------------
Static Function GetDataCan()
	Local cChvTit := SE1->E1_FILIAL + SE1->E1_PREFIXO + SE1->E1_NUM + SE1->E1_PARCELA + SE1->E1_TIPO
	Local cSE5Seq := SE5->E5_SEQ
	Local cTmpOHI := GetNextAlias()

	BeginSql Alias cTmpOHI
		SELECT R_E_C_N_O_ nRecno
		FROM %Table:OHI%
		WHERE OHI_FILIAL = %xFilial:OHI%
			AND OHI_CHVTIT = %Exp:cChvTit%
			AND OHI_SE5SEQ = %Exp:cSE5Seq%
			AND %NotDel%
	EndSql

Return ( cTmpOHI )


//-------------------------------------------------------------------
/*/{Protheus.doc} DeleteOHI
Deleta resgistro vinculados a baixa cancelada na tabela de rastreamento

@param  cTmpOHI    , caracter, Alias tempor�rio
@return lDelOHI    , logico  , Sucesso ou falha na exclus�o do registro

@author  Jonatas Martins
@since   09/02/2018
@version 12.1.20
/*/
//-------------------------------------------------------------------
Static Function DeleteOHI( cTmpOHI )
	Local lDelOHI := .T.

	BEGIN TRANSACTION
		While (cTmpOHI)->( ! Eof() )
			OHI->( DbGoTo( (cTmpOHI)->nRecno ) )

			If OHI->( ! Eof() ) .And. RecLock("OHI", .F.)
				OHI->( DbDelete() )
				OHI->( MsUnLock() )
			Else
				lDelOHI := .F.
				DisarmTransaction()
				Exit
			EndIf

			(cTmpOHI)->( DbSkip() )
		EndDo
	END TRANSACTION

Return ( lDelOHI )


//-------------------------------------------------------------------
/*/{Protheus.doc} GetSincOper
Verifica se existe registro de rastreamento e retorna a opera��o para
fila de sincroniza��o

@param  cEscrit   , caracter  , C�digo do escrit�rio
@param  cFatura   , caracter  , C�digo da fatura
@param  lInclui   , logico    , Se verdadeiro opera��o de inclus�o
@return cSincOper , caraceter , Tipo da opera��o: 4=Altera��o; 5=Exclus�o

@author  Jonatas Martins
@since   09/02/2018
@version 12.1.20
/*/
//-------------------------------------------------------------------
Static Function GetSincOper( cEscrit , cFatura , lInclui )
	Local cSincOper  := 0

	Default cEscrit := ""
	Default cFatura := ""
	Default lInclui := .F.

	OHI->( DbSetOrder(1) ) //OHI_FILIAL, OHI_CESCR, OHI_CFATUR, OHI_CCONTR, OHI_ITEM
	If OHI->( DbSeek( xFilial("OHI") + cEscrit  + cFatura ) )
		cSincOper := "4"
	Else
		cSincOper := IIF( lInclui , "3", "5" )
	EndIf

Return ( cSincOper )


//-------------------------------------------------------------------
/*/{Protheus.doc} J256FCarga
Fun��o de filtro para carga inicial

@param   cKey   , caracter  , Chave do registro
@return  lFilter, logico    , Verdadeiro/Falso

@author  Jonatas Martins
@since   09/02/2018
@version 12.1.20
@obs     Fun��o chamada no fonte JURA170
/*/
//-------------------------------------------------------------------
Function J256FCarga( cKey )
	Local aAreas  := {SE1->(GetArea()), GetArea()}
	Local lFilter := .F.

	Default cKey   := ""

	SE1->( DbSetOrder(1) ) 
	If SE1->( DbSeek( cKey )) 
		lFilter := !Empty(SE1->E1_JURFAT)
	EndIf
	
	Aeval( aAreas , {|cArea| RestArea(cArea)} )

Return ( lFilter )


//-------------------------------------------------------------------
/*/{Protheus.doc} J256ValAce
Calcula valores acess�rios da baixa.

@param  cSE5IdOri, caractere, Id de origem dos dados da SE5

@return nValor   , num�rico , Valores acess�rios

@author  Jorge Martins / Bruno Ritter
@since   25/10/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Function J256ValAce( cSE5IdOri )
Local aAreas := { SE5->(GetArea()) , GetArea() }
Local nValor := 0

SE5->(dbSetOrder(21)) // E5_FILIAL + E5_IDORIG + E5_TIPODOC
SE5->(dbSeek( xFilial("SE5") + cSE5IdOri + "VA" ) )

While SE5->( !EOF() ) .And. SE5->E5_IDORIG == cSE5IdOri .AND. SE5->E5_TIPODOC == "VA"

	nValor += SE5->E5_VLMOED2

	SE5->( DbSkip() )

EndDo

Aeval( aAreas , {|aArea| RestArea( aArea ) } )

Return nValor

//-------------------------------------------------------------------
/*/{Protheus.doc} J256AnoMes
Rotina identificar o �ltimo registro na OHH para a fatura.

@author  Jorge Martins / Bruno Ritter
@since   05/03/2020
/*/
//-------------------------------------------------------------------
Static Function J256AnoMes()
	Local cAnoMes := ""
	Local cQuery  := ""
	Local aAnoMes := {}

	cQuery := " SELECT MAX(OHH_ANOMES) ANOMES "
	cQuery +=   " FROM " + RetSqlName("OHH") + " "
	cQuery +=  " WHERE OHH_FILIAL = '" + xFilial("OHH") + "' "
	cQuery +=    " AND OHH_PREFIX = '" + SE1->E1_PREFIXO + "' "
	cQuery +=    " AND OHH_NUM    = '" + SE1->E1_NUM + "' "
	cQuery +=    " AND OHH_PARCEL = '" + SE1->E1_PARCELA + "' "
	cQuery +=    " AND OHH_TIPO   = '" + SE1->E1_TIPO + "' "
	cQuery +=    " AND D_E_L_E_T_ = ' ' "

	aAnoMes := JurSQL(cQuery, {"ANOMES"})

	If Len(aAnoMes) > 0
		cAnoMes := aAnoMes[1][1]
	EndIf

	JurFreeArr(@aAnoMes)

Return cAnoMes