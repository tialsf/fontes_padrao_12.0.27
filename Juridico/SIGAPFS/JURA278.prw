#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "JURA278.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} JURA278
Modelo de Titulos a pagar

@author Jonatas Martins
@since  06/05/2020
/*/
//-------------------------------------------------------------------
Function JURA278()
	Local oBrowse := FWMBrowse():New()
	
	oBrowse:SetDescription(STR0001) //"Contas a Pagar - PFS"
	oBrowse:SetAlias("SE2")
	oBrowse:SetLocate()
	oBrowse:Activate()

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menu Funcional

@return aRotina - Estrutura do menu
        [n,1] Nome a aparecer no cabecalho
        [n,2] Nome da Rotina associada
        [n,3] Reservado
        [n,4] Tipo de Transa��o a ser efetuada:
            1 - Pesquisa e Posiciona em um Banco de Dados
            2 - Simplesmente Mostra os Campos
            3 - Inclui registros no Bancos de Dados
            4 - Altera o registro corrente
            5 - Remove o registro corrente do Banco de Dados
        [n,5] Nivel de acesso
        [n,6] Habilita Menu Funcional

@author Jonatas Martins
@since  06/05/2020
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
	Local aRotina := {}
Return (aRotina)

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Estutura da tela de 

@author Jonatas Martins
@since  06/05/2020
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
	Local oStructSE2 := FWFormStruct(2, "SE2")
	Local oModel     := FWLoadModel("JURA278")
	Local oView      := Nil
	
	oView := FWFormView():New()
	oView:SetModel(oModel)
	oView:AddField("JURA278_VIEW", oStructSE2, "SE2MASTER")
	oView:CreateHorizontalBox("FORMFIELD", 100)
	oView:SetOwnerView("JURA278_VIEW", "FORMFIELD")
	oView:SetDescription(STR0001) //"Contas a Pagar - PFS"
	oView:EnableControlBar(.T.)

Return (oView)

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Estrutura do modelo de dados do Tipo de Fechamento

@author Jonatas Martins
@since  06/05/2020
/*/
//-------------------------------------------------------------------
Static Function Modeldef()
	Local oStructSE2 := FWFormStruct(1, "SE2")
	Local oModel     := NIL
	
	oModel:= MPFormModel():New("JURA278", /*Pre-Validacao*/, /*Pos-Validacao*/, /*Commit*/, /*Cancel*/)
	oModel:AddFields("SE2MASTER", Nil, oStructSE2, /*Pre-Validacao*/, /*Pos-Validacao*/)
	oModel:SetDescription(STR0001) //"Contas a Pagar - PFS"
	oModel:GetModel("SE2MASTER"):SetDescription(STR0001) // "Contas a Pagar - PFS"

Return (oModel)
