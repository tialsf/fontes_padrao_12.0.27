#INCLUDE "PROTHEUS.CH" 
#INCLUDE "TOPCONN.CH"
#INCLUDE "COLORS.CH"
#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPRINTSETUP.CH"
#INCLUDE "JURAPAD034.CH"

#DEFINE nTamCarac 5.5    // Tamanho de um caractere no relat�rio
#DEFINE nSalto    10     // Salto de uma linha a outra

#DEFINE nPColData  0     // Posi��o vertical do campo Data
#DEFINE nPColSol   40    //   ''       ''    ''  ''   Solicitante
#DEFINE nPColHist  70    //   ''       ''    ''  ''   Hist�rico
#DEFINE nPColLanc  305   //   ''       ''    ''  ''   Lan�amento
#DEFINE nPColNat   360   //   ''       ''    ''  ''   Natureza
#DEFINE nPColValCv 450   //   ''       ''    ''  ''   Valor Convertido
#DEFINE nPColVal   480   //   ''       ''    ''  ''   Valor
#DEFINE nPColSaldo 540   //   ''       ''    ''  ''   Saldo

#DEFINE cDateFt    cValToChar( Date() ) // Data - Footer
#DEFINE cTimeFt    Time()               // Hora - Footer

Static cAlsTmp     := ""   // Alias da query de Escrit�rio
Static nPage       := 1    // Contador de p�ginas
Static nSaldoNat   := 0    // Saldo da Natureza
Static nSubTotEsc  := 0    // Subtotal por natureza
Static nTotEnt     := 0    // Total Geral de Entrada
Static nTotSaida   := 0    // Total Geral de Saida

//-------------------------------------------------------------------
/*/{Protheus.doc} JURAPAD034
Relat�rio de Extrato por Natureza/Centro de Custo

@author Jonatas Martins / Jorge Martins
@since 21/03/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Function JURAPAD034()
	Local aArea     := GetArea()
	Local lCanc     := .F.
	Local lPDUserAc := Iif(FindFunction("JPDUserAc"), JPDUserAc(), .T.) // Indica se o usu�rio possui acesso a dados sens�veis ou pessoais (LGPD)
	
	If lPDUserAc
		While !lCanc
			If JPergunte()
				If JP034TdOk(MV_PAR01, MV_PAR02, MV_PAR03)
					JP034Relat(DtoS( MV_PAR01 ) , DtoS( MV_PAR02 ) , MV_PAR03 , MV_PAR04 , cValToChar(MV_PAR05) , MV_PAR06 , MV_PAR07 , MV_PAR08, MV_PAR09, MV_PAR10)
				EndIf
			Else
				lCanc := .T.
			Endif
		EndDo
	Else
		MsgInfo(STR0030, STR0031) // "Usu�rio com restri��o de acesso a dados pessoais/sens�veis.", "Acesso restrito"
	EndIf

	RestArea( aArea )

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} JPergunte
Abre o Pergunte para filtro do relat�rio

@author Jorge Martins
@since  26/11/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function JPergunte()
Local lRet := .T.

	lRet := Pergunte('JURAPAD034')

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JP034TdOk
Rotina validar os dados do pergunte

@param  cDataIni   , caractere, Data inicial dos lan�amentos
@param  cDataFim   , caractere, Data final dos lan�amentos
@param  cEscritFil , caractere, Escrit�rio da filial dos lan�amentos

@author Jonatas Martins / Jorge Martins
@since 30/03/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function JP034TdOk(dDataIni, dDataFim, cEscritFil)
Local lRet := .T.

If Empty(dDataIni) .Or. Empty(dDataFim)
	JurMsgErro(STR0021,,STR0022) // "Data inicial e final s�o obrigat�rias." - "Preencha as datas para filtro."
	lRet := .F.
EndIf

If lRet .And. Empty(cEscritFil)
	JurMsgErro(STR0023,,STR0024) // "� obrigat�rio o preenchimento do Escrit�rio do Lancamento." - "Preencha o campo para filtro."
	lRet := .F.
EndIf

Return lRet

//=======================================================================
/*/{Protheus.doc} JP034Relat
Relat�rio de Extrato por Natureza/Centro de Custo

@param  cDataIni   , caractere, Data inicial dos lan�amentos
@param  cDataFim   , caractere, Data final dos lan�amentos
@param  cEscritFil , caractere, Escrit�rio da filial dos lan�amentos
@param  cNatureza  , caractere, Natureza dos lan�amentos
@param  cStatusNat , caractere, Status da Natureza 1=Ambas; 2=Bloqueado; 3=N�o Bloqueado
@param  cTpconta   , caractere, Tipo da Conta 
@param  cEscritCC  , caractere, Escrit�rio
@param  cCusto     , caractere, Centro de Custo dos la�amentos
@param  cProjeto   , Projeto/finalidade
@param  cItemPrj   , Item do proejeto

@author  Jonatas Martins / Jorge Martins
@since   28/03/2018
/*/
//=======================================================================
Static Function JP034Relat( cDataIni , cDataFim , cEscritFil , cNatureza , cStatusNat , cTpConta , cEscritCC , cCusto, cProjeto, cItemPrj)
	Local cReportName   := "Extrato_Natureza_CC_" + FwTimeStamp(1)
	Local cDirectory    := GetSrvProfString( "StartPath" , "" )
	Local bRun          := Nil
	Local lRet          := .T.
	Local cFilialAtu    := cFilAnt
	Local cFilEsct      := JurGetDados( "NS7", 1, xFilial("NS7") + cEscritFil, "NS7_CFILIA" )

	Default cProjeto := ""
	Default cItemPrj := ""

	cFilAnt := cFilEsct

	nSaldoNat := 0

	//----------------------
	// Busca dados no banco
	//----------------------
	JReportQry( cDataIni , cDataFim , cEscritFil , cNatureza , cStatusNat , cTpConta , cEscritCC , cCusto, cProjeto, cItemPrj )

	//-----------------
	// Gera relat�rios 
	//-----------------
	If (cAlsTmp)->( ! Eof() )
		bRun := {|| PrintReport(cReportName , cDirectory) }
		FwMsgRun( , bRun , STR0001 , "" ) //"Gerando relat�rio, aguarde..."
	Else
		lRet := .F.
		JurMsgError( STR0002 ) //"N�o foram encontrados dados para impress�o!"
	EndIf
		
	nPage      := 1 // Contador de p�ginas
	nSubTotEsc := 0 // Subtotal por escrit�rio
	nTotEnt    := 0 // Total Geral de Entrada
	nTotSaida  := 0 // Total Geral de Saida
		
	(cAlsTmp)->( DbCloseArea() )

	cFilAnt := cFilialAtu
	
Return lRet

//=======================================================================
/*/{Protheus.doc} JReportQry
Monta alias tempor�rio com dados do relat�rio

@param  cDataIni   , Data inicial dos lan�amentos
@param  cDataFim   , Data final dos lan�amentos
@param  cEscritFil , Escrit�rio da filial dos lan�amentos
@param  cNatureza  , Natureza dos lan�amentos
@param  cStatusNat , Status da Natureza 1=Ambas; 2=Bloqueado; 3=N�o Bloqueado
@param  cTpconta   , Tipo da Conta 
@param  cEscritCC  , Escrit�rio
@param  cCusto     , Centro de Custo dos la�amentos
@param  cProjeto   , Projeto/finalidade
@param  cItemPrj   , Item do proejeto

@author Luciano Pereira dos Santos
@since  28/03/2018
/*/
//=======================================================================
Static Function JReportQry( cDataIni , cDataFim , cEscritFil , cNatureza , cStatusNat , cTpConta , cEscritCC , cCusto, cProjeto, cItemPrj)
	Local oTpConta   := JurTpConta():New()
	Local cQuery     := ""
	Local cFilEscr   := ""
	Local lEscrit    := !Empty(cEscritCC)
	Local lCusto     := !Empty(cCusto)
	Local cSCPARTO   := Space(TamSx3('OHB_CPARTO')[1])
	Local cSCTRATO   := Space(TamSx3('OHB_CTRATO')[1])
	Local cSCPARTD   := Space(TamSx3('OHB_CPARTD')[1])
	Local cSCTRATD   := Space(TamSx3('OHB_CTRATD')[1])
	Local cNUSAMFIM  := Space(TamSx3('NUS_AMFIM')[1])
	Local cOH8AMFIM  := Space(TamSx3('OH8_AMFIM')[1])
	Local lExitNat   := Iif(Empty(cNatureza), .F., ExistCpo("SED", cNatureza,,, .F.))
	Local cTpContTmp := ""
	Local cFilProje  := ""

	Default cEscritFil  := ''
	Default cNatureza   := ''
	Default cTpConta    := ''
	Default cEscritCC   := ''
	Default cCusto      := ''
	Default cStatusNat  := ''

	If !Empty(cProjeto) // Projeto
		cFilProje := " AND OHB.OHB_CPROJE = '" + cProjeto + "' "

		If !Empty(cItemPrj) // Item Projeto
			cFilProje += " AND OHB.OHB_CITPRJ = '" + cItemPrj + "' "
		EndIf
	EndIf

	
	If !Empty(cEscritFil)
		cFilEscr := JurGetDados( "NS7", 1, xFilial("NS7") + cEscritFil, "NS7_CFILIA" )
	EndIf

	cAlsTmp := GetNextAlias()

	// TABELA TEMPOR�RIA - Tipo de contas
	oTpConta:GeraTmp()
	cTpContTmp := oTpConta:GetTmpName()

	//--SALDO ANTERIOR
	cQuery := " SELECT TAB.ORDEM, TAB.FILIAL, TAB.DATA, RD0.RD0_SIGLA, TAB.DOCTO, TAB.NATUREZA, SED.ED_DESCRIC, SED.ED_CMOEJUR, TAB.NATUREZA2, CTOCONV.CTO_SIMB MOEDA_CONV, CTO.CTO_SIMB, TAB.ESCRITORIO, NS7.NS7_NOME, TAB.CENTRO_CUSTO, CTT.CTT_DESC01, TAB.VALOR_CONV, TAB.VALOR_LANC, TAB.SALDO, TAB.RECNO "
	cQuery +=   " FROM ( "
	cQuery +=          " SELECT '1' ORDEM, SALDOANT.FILIAL FILIAL, MAX(SALDOANT.DATA) DATA, '' SOLICITANTE, '' DOCTO, SALDOANT.NATUREZA, SALDOANT.MOEDA, ' ' NATUREZA2, ' ' MOEDACONV, SALDOANT.ESCRITORIO, '' CENTRO_CUSTO, 0 VALOR_CONV, SUM(SALDOANT.VALOR_LANC) VALOR_LANC, SUM(SALDOANT.VALOR_LANC) SALDO, 0 RECNO "
	cQuery +=            " FROM ( SELECT OHB.OHB_FILIAL FILIAL, OHB.OHB_DTLANC DATA, OHB.OHB_NATORI NATUREZA, SED.ED_CMOEJUR MOEDA, NS7.NS7_COD ESCRITORIO, "
	cQuery +=                            JP034CsVal(cTpContTmp, "O", .F., .T.) + " VALOR_LANC "
	cQuery +=                     " FROM " + RetSqlName("OHB") + " OHB "
	cQuery +=                    " INNER JOIN " + RetSqlName("SED") + " SED "
	cQuery +=                            " ON (SED.ED_FILIAL = '" + xFilial("SED") + "' "
	cQuery +=                           " AND  SED.ED_CODIGO = OHB.OHB_NATORI "
	cQuery +=                           " AND  SED.D_E_L_E_T_  = ' ' ) "
	cQuery +=                     " LEFT JOIN " + RetSqlName("NS7") + " NS7 "
	cQuery +=                            " ON (NS7_FILIAL = '" + xFilial("NS7") + "' "
	cQuery +=                           " AND  NS7.NS7_CFILIA = OHB.OHB_FILIAL "
	cQuery +=                           " AND  NS7.D_E_L_E_T_ = ' ' ) "
	cQuery +=                    " WHERE OHB.OHB_FILIAL = '" + xFilial("OHB") + "' "
	cQuery +=                      " AND OHB.OHB_DTLANC < '" + cDataIni + "' "
	cQuery +=                      " AND OHB.D_E_L_E_T_ = ' ' "
	If !Empty(cFilEscr)
		cQuery +=                  " AND NS7.NS7_COD = '" + cEscritFil + "' "
	EndIf
	cQuery +=                   " UNION ALL "
	cQuery +=                   " SELECT OHB.OHB_FILIAL FILIAL, OHB.OHB_DTLANC DATA, OHB.OHB_NATDES NATUREZA, SED.ED_CMOEJUR MOEDA, NS7.NS7_COD ESCRITORIO, "
	cQuery +=                            JP034CsVal(cTpContTmp, "D", .F., .T.) + " VALOR_LANC "
	cQuery +=                     " FROM " + RetSqlName("OHB") +" OHB "
	cQuery +=                    " INNER JOIN " +RetSqlName("SED")+" SED "
	cQuery +=                            " ON (SED.ED_FILIAL = '"+ xFilial("SED")+ "' "
	cQuery +=                           " AND SED.ED_CODIGO = OHB.OHB_NATDES "
	cQuery +=                           " AND SED.D_E_L_E_T_  = ' ' ) "
	cQuery +=                     " LEFT JOIN " +RetSqlName("NS7")+" NS7 "
	cQuery +=                            " ON (NS7_FILIAL = '"+ xFilial("NS7")+ "' "
	cQuery +=                           " AND NS7.NS7_CFILIA = OHB.OHB_FILIAL "
	cQuery +=                           " AND NS7.D_E_L_E_T_ = ' ' ) "
	cQuery +=                    " WHERE OHB.OHB_FILIAL = '" + xFilial("OHB") + "' "
	cQuery +=                      " AND OHB.OHB_DTLANC < '" + cDataIni + "' "
	cQuery +=                      " AND OHB.D_E_L_E_T_ = ' ' "
	If !Empty(cFilEscr)
		cQuery +=                  " AND NS7.NS7_COD = '" + cEscritFil + "' "
	Endif
	cQuery +=                 " ) SALDOANT "
	cQuery +=           " GROUP BY SALDOANT.FILIAL, SALDOANT.NATUREZA, SALDOANT.MOEDA, SALDOANT.ESCRITORIO "

	cQuery +=      " UNION "

	// LAN�AMENTOS DE ORIGEM QUE N�O SEJAM CENTRO DE CUSTO: PARTICIPANTE OU TABELA DE RATEIO
	cQuery +=          " SELECT '2' ORDEM, EXT.OHB_FILIAL FILIAL, EXT.OHB_DTLANC DATA, EXT.OHB_CPART SOLICITANTE, EXT.DOCTO, EXT.NATUREZA, EXT.MOEDA, EXT.NATUREZA2, EXT.MOEDACONV, EXT.ESCRITORIO, EXT.CENTRO_CUSTO, EXT.VALOR_CONV VALOR_CONV, EXT.VALOR_LANC, 0 SALDO, EXT.RECNO "
	cQuery +=          "  FROM ( "
	cQuery +=                 " (SELECT OHB.OHB_FILIAL, OHB.OHB_DTLANC, OHB.OHB_CPART, OHB.OHB_CODIGO DOCTO, OHB.OHB_NATORI NATUREZA, SED.ED_CMOEJUR MOEDA, OHB.OHB_NATDES NATUREZA2, OHB.OHB_CMOELC MOEDACONV, OHB.OHB_CESCRO ESCRITORIO, OHB.OHB_CCUSTO CENTRO_CUSTO, "
	cQuery +=                           JP034CsVal(cTpContTmp, "O") + " VALOR_CONV, "
	cQuery +=                         " OHB.OHB_VALOR * (SELECT SINAL FROM " + cTpContTmp + " WHERE CODIGO = SED.ED_TPCOJR AND TIPO = 'O') VALOR_LANC,
	cQuery +=                         " OHB.R_E_C_N_O_ RECNO "
	cQuery +=                   " FROM " +RetSqlName("OHB")+" OHB "
	cQuery +=                   " INNER JOIN " +RetSqlName("SED")+" SED "
	cQuery +=                           " ON (SED.ED_FILIAL = '"+ xFilial("SED")+ "' "
	cQuery +=                               " AND SED.ED_CODIGO = OHB.OHB_NATORI "
	cQuery +=                               " AND SED.D_E_L_E_T_  = ' ' ) "
	cQuery +=                   " WHERE OHB.OHB_FILIAL = '"+ xFilial("OHB")+ "' "
	cQuery +=                     " AND OHB.OHB_CPARTO = '"+ cSCPARTO + "' "
	cQuery +=                     " AND OHB.OHB_CTRATO = '"+ cSCTRATO + "' "
	cQuery +=                       cFilProje // Filtro Projeto/item
	cQuery +=                     " AND OHB.D_E_L_E_T_ = ' ') "
	
	cQuery +=                 " UNION "
	
	// LAN�AMENTOS DE DESTINO QUE N�O SEJAM CENTRO DE CUSTO: PARTICIPANTE OU TABELA DE RATEIO
	cQuery +=                 " (SELECT OHB.OHB_FILIAL, OHB.OHB_DTLANC, OHB.OHB_CPART, OHB.OHB_CODIGO DOCTO, OHB.OHB_NATDES NATUREZA, SED.ED_CMOEJUR MOEDA, OHB.OHB_NATORI NATUREZA2, OHB.OHB_CMOELC MOEDACONV, OHB.OHB_CESCRD ESCRITORIO, OHB.OHB_CCUSTD CENTRO_CUSTO, "
	cQuery +=                           JP034CsVal(cTpContTmp, "D") + " VALOR_CONV, "
	cQuery +=                         " OHB.OHB_VALOR * (SELECT SINAL FROM " + cTpContTmp + " WHERE CODIGO = SED.ED_TPCOJR AND TIPO = 'D') VALOR_LANC, "
	cQuery +=                         " OHB.R_E_C_N_O_ RECNO "
	cQuery +=                  " FROM " +RetSqlName("OHB")+" OHB "
	cQuery +=                  " INNER JOIN " +RetSqlName("SED")+" SED "
	cQuery +=                          " ON (SED.ED_FILIAL = '"+ xFilial("SED")+ "' "
	cQuery +=                              " AND SED.ED_CODIGO = OHB.OHB_NATDES
	cQuery +=                              " AND SED.D_E_L_E_T_ = ' ' ) "
	cQuery +=                  " WHERE OHB.OHB_FILIAL = '"+ xFilial("OHB")+ "' "
	cQuery +=                    " AND OHB.OHB_CPARTD = '"+ cSCPARTD + "' "
	cQuery +=                    " AND OHB.OHB_CTRATD = '"+ cSCTRATD + "' "
	cQuery +=                      cFilProje // Filtro Projeto/item
	cQuery +=                    " AND OHB.D_E_L_E_T_ = ' ') "
	
	cQuery +=                 " UNION "
	
	// LAN�AMENTOS DE ORIGEM QUE SEJAM CENTRO DE CUSTO: PARTICIPANTE
	cQuery +=                 " (SELECT OHB.OHB_FILIAL, OHB.OHB_DTLANC, OHB.OHB_CPART, OHB.OHB_CODIGO DOCTO, OHB.OHB_NATORI NATUREZA, SED.ED_CMOEJUR MOEDA, OHB.OHB_NATDES NATUREZA2, OHB.OHB_CMOELC MOEDACONV, NUS.NUS_CESCR ESCRITORIO, NUS.NUS_CC CENTRO_CUSTO, "
	cQuery +=                           JP034CsVal(cTpContTmp, "O") + " VALOR_CONV, "
	cQuery +=                         " OHB.OHB_VALOR * (SELECT SINAL FROM " + cTpContTmp + " WHERE CODIGO = SED.ED_TPCOJR AND TIPO = 'O') VALOR_LANC, "
	cQuery +=                         " OHB.R_E_C_N_O_ RECNO "
	cQuery +=                  " FROM " +RetSqlName("OHB")+" OHB "
	cQuery +=                  " INNER JOIN " +RetSqlName("SED")+" SED "
	cQuery +=                          " ON (SED.ED_FILIAL = '"+ xFilial("SED")+ "' "
	cQuery +=                              " AND SED.ED_CODIGO = OHB.OHB_NATORI
	cQuery +=                              " AND SED.D_E_L_E_T_ = ' ' ) "
	cQuery +=                  " INNER JOIN " +RetSqlName("NUS")+" NUS "
	cQuery +=                          " ON (NUS.NUS_FILIAL = '"+ xFilial("NUS")+ "'"
	cQuery +=                              " AND OHB.OHB_CPARTO = NUS.NUS_CPART "
	cQuery +=                              " AND ((SUBSTRING(OHB.OHB_DTLANC,1,6) >= NUS.NUS_AMINI AND NUS.NUS_AMFIM = '"+ cNUSAMFIM + "') "
	cQuery +=                                    "OR (SUBSTRING(OHB.OHB_DTLANC,1,6) BETWEEN NUS.NUS_AMINI AND NUS.NUS_AMFIM)) "
	cQuery +=                              " AND NUS.D_E_L_E_T_ = ' ' ) "
	cQuery +=                  " WHERE OHB.OHB_FILIAL = '"+ xFilial("OHB")+ "' "
	cQuery +=                    " AND OHB.OHB_CPARTO > '"+ cSCPARTO + "' "
	cQuery +=                      cFilProje // Filtro Projeto/item
	cQuery +=                    " AND OHB.D_E_L_E_T_ = ' ') "
	
	cQuery +=                 " UNION "
	
	//LAN�AMENTOS DE destino QUE SEJAM CENTRO DE CUSTO: PARTICIPANTE
	cQuery +=                 " (SELECT OHB.OHB_FILIAL, OHB.OHB_DTLANC, OHB.OHB_CPART, OHB.OHB_CODIGO DOCTO, OHB.OHB_NATDES NATUREZA, SED.ED_CMOEJUR MOEDA, OHB.OHB_NATORI NATUREZA2, OHB.OHB_CMOELC MOEDACONV, NUS.NUS_CESCR ESCRITORIO, NUS.NUS_CC CENTRO_CUSTO, "
	cQuery +=                           JP034CsVal(cTpContTmp, "D") + " VALOR_CONV, "
	cQuery +=                         " OHB.OHB_VALOR * (SELECT SINAL FROM " + cTpContTmp + " WHERE CODIGO = SED.ED_TPCOJR AND TIPO = 'D') VALOR_LANC, "
	cQuery +=                         " OHB.R_E_C_N_O_ RECNO "
	cQuery +=                  " FROM " +RetSqlName("OHB")+" OHB "
	cQuery +=                  " INNER JOIN " +RetSqlName("SED")+" SED "
	cQuery +=                          " ON (SED.ED_FILIAL = '"+ xFilial("SED")+ "' "
	cQuery +=                              " AND SED.ED_CODIGO = OHB.OHB_NATDES "
	cQuery +=                              " AND SED.D_E_L_E_T_ = ' ' ) "
	cQuery +=                  " INNER JOIN " +RetSqlName("NUS")+" NUS "
	cQuery +=                          " ON (NUS.NUS_FILIAL = '"+ xFilial("NUS")+ "'"
	cQuery +=                              " AND OHB.OHB_CPARTD = NUS.NUS_CPART "
	cQuery +=                              " AND ((SUBSTRING(OHB.OHB_DTLANC,1,6) >= NUS.NUS_AMINI AND NUS.NUS_AMFIM = '"+ cNUSAMFIM + "') "
	cQuery +=                                   " OR (SUBSTRING(OHB.OHB_DTLANC,1,6) BETWEEN NUS.NUS_AMINI AND NUS.NUS_AMFIM)) "
	cQuery +=                              " AND NUS.D_E_L_E_T_ = ' ') "
	cQuery +=                  " WHERE OHB.OHB_FILIAL = '"+ xFilial("OHB")+ "' "
	cQuery +=                    " AND OHB.OHB_CPARTD > '"+ cSCPARTD + "' "
	cQuery +=                      cFilProje // Filtro Projeto/item
	cQuery +=                    " AND OHB.D_E_L_E_T_ = ' ') "
	
	cQuery +=                 " UNION "
	
	//LAN�AMENTOS DE ORIGEM QUE SEJAM CENTRO DE CUSTO: TABELA de RATEIO (ESCRITORIO ou ESCRITORIO e CENTRO DE CUSTO)
	cQuery +=                 " (SELECT OHB.OHB_FILIAL, OHB.OHB_DTLANC, OHB.OHB_CPART, OHB.OHB_CODIGO DOCTO, OHB.OHB_NATORI NATUREZA, SED.ED_CMOEJUR MOEDA, OHB.OHB_NATDES NATUREZA2, OHB.OHB_CMOELC MOEDACONV, OH8.OH8_CESCRI ESCRITORIO, OH8.OH8_CCCUST CENTRO_CUSTO, "
	cQuery +=                           JP034CsVal(cTpContTmp, "O", .T.) + " VALOR_CONV, "
	cQuery +=                         " ( (OHB.OHB_VALOR * OH8.OH8_PERCEN) / 100) * (SELECT SINAL FROM " + cTpContTmp + " WHERE CODIGO = SED.ED_TPCOJR AND TIPO = 'O') VALOR_LANC, "
	cQuery +=                         " OHB.R_E_C_N_O_ RECNO "
	cQuery +=                  " FROM " +RetSqlName("OHB")+" OHB "
	cQuery +=                  " INNER JOIN " +RetSqlName("SED")+" SED "
	cQuery +=                          " ON (SED.ED_FILIAL = '"+ xFilial("SED")+ "' "
	cQuery +=                              " AND SED.ED_CODIGO = OHB.OHB_NATORI "
	cQuery +=                              " AND SED.D_E_L_E_T_ = ' ' ) "
	cQuery +=                  " INNER JOIN " +RetSqlName("OH6")+" OH6 "
	cQuery +=                          " ON (OH6.OH6_FILIAL = '"+ xFilial("OH6")+ "' "
	cQuery +=                              " AND OHB.OHB_CTRATO = OH6.OH6_CODIGO "
	cQuery +=                              " AND OH6.OH6_TIPO IN ('1','2') "
	cQuery +=                              " AND OH6.D_E_L_E_T_ = ' ') "
	cQuery +=                  " INNER JOIN " + RetSqlName("OH8")+" OH8 "
	cQuery +=                          " ON (OH8.OH8_FILIAL = '"+ xFilial("OH8")+ "' "
	cQuery +=                              " AND OH8.OH8_CODRAT = OH6.OH6_CODIGO "
	cQuery +=                              " AND ((SUBSTRING(OHB.OHB_DTLANC,1,6) >= OH8.OH8_AMINI AND OH8.OH8_AMFIM = '"+ cOH8AMFIM + "') "
	cQuery +=                                   " OR (SUBSTRING(OHB.OHB_DTLANC,1,6) BETWEEN OH8.OH8_AMINI AND OH8.OH8_AMFIM)) "
	cQuery +=                              " AND OH8.D_E_L_E_T_ = ' ') "
	cQuery +=                  " WHERE OHB.OHB_FILIAL = '"+ xFilial("OHB")+ "' "
	cQuery +=                    " AND OHB.OHB_CTRATO > '"+ cSCTRATO + "' "
	cQuery +=                      cFilProje // Filtro Projeto/item
	cQuery +=                    " AND OHB.D_E_L_E_T_ = ' ') "
	
	cQuery +=                 " UNION "
	
	// LAN�AMENTOS DE DESTINO QUE SEJAM CENTRO DE CUSTO: TABELA de RATEIO (ESCRITORIO ou ESCRITORIO e CENTRO DE CUSTO)
	cQuery +=                 " (SELECT OHB.OHB_FILIAL, OHB.OHB_DTLANC, OHB.OHB_CPART, OHB.OHB_CODIGO DOCTO, OHB.OHB_NATDES NATUREZA, SED.ED_CMOEJUR MOEDA, OHB.OHB_NATORI NATUREZA2, OHB.OHB_CMOELC MOEDACONV, OH8.OH8_CESCRI ESCRITORIO, OH8.OH8_CCCUST CENTRO_CUSTO, "
	cQuery +=                           JP034CsVal(cTpContTmp, "D", .T.) + " VALOR_CONV, "
	cQuery +=                         " ( (OHB.OHB_VALOR * OH8.OH8_PERCEN) / 100) * (SELECT SINAL FROM " + cTpContTmp + " WHERE CODIGO = SED.ED_TPCOJR AND TIPO = 'D') VALOR_LANC, "
	cQuery +=                         " OHB.R_E_C_N_O_ RECNO "
	cQuery +=                  " FROM " +RetSqlName("OHB")+" OHB "
	cQuery +=                  " INNER JOIN " +RetSqlName("SED")+" SED "
	cQuery +=                          " ON (SED.ED_FILIAL = '"+ xFilial("SED")+ "' "
	cQuery +=                              " AND SED.ED_CODIGO = OHB.OHB_NATDES "
	cQuery +=                              " AND SED.D_E_L_E_T_ = ' ' ) "
	cQuery +=                  " INNER JOIN " +RetSqlName("OH6")+" OH6 "
	cQuery +=                          " ON (OH6.OH6_FILIAL = '"+ xFilial("OH6")+ "' "
	cQuery +=                              " AND OHB.OHB_CTRATD = OH6.OH6_CODIGO "
	cQuery +=                              " AND OH6.OH6_TIPO IN ('1','2') "
	cQuery +=                              " AND OH6.D_E_L_E_T_ = ' ' ) "
	cQuery +=                  " INNER JOIN " + RetSqlName("OH8")+" OH8 "
	cQuery +=                          " ON (OH8.OH8_FILIAL = '"+ xFilial("OH8")+ "' "
	cQuery +=                              " AND OH8.OH8_CODRAT = OH6.OH6_CODIGO "
	cQuery +=                              " AND ((SUBSTRING(OHB.OHB_DTLANC,1,6) >= OH8.OH8_AMINI AND OH8.OH8_AMFIM = '"+ cOH8AMFIM + "') "
	cQuery +=                                   " OR (SUBSTRING(OHB.OHB_DTLANC,1,6) BETWEEN OH8.OH8_AMINI AND OH8.OH8_AMFIM)) "
	cQuery +=                              " AND OH8.D_E_L_E_T_ = ' ') "
	cQuery +=                  " WHERE OHB.OHB_FILIAL = '"+ xFilial("OHB")+ "' "
	cQuery +=                    " AND OHB.OHB_CTRATD > '"+ cSCTRATD + "' "
	cQuery +=                      cFilProje // Filtro Projeto/item
	cQuery +=                    " AND OHB.D_E_L_E_T_ = ' ') "
	
	cQuery +=                 " UNION "
	
	// LAN�AMENTOS DE ORIGEM QUE SEJAM CENTRO DE CUSTO: TABELA de RATEIO (PROFISSIONAL)
	cQuery +=                 " (SELECT OHB.OHB_FILIAL, OHB.OHB_DTLANC, OHB.OHB_CPART, OHB.OHB_CODIGO DOCTO, OHB.OHB_NATORI NATUREZA, SED.ED_CMOEJUR MOEDA, OHB.OHB_NATDES NATUREZA2, OHB.OHB_CMOELC MOEDACONV, NUS.NUS_CESCR ESCRITORIO, NUS.NUS_CC CENTRO_CUSTO, " 
	cQuery +=                           JP034CsVal(cTpContTmp, "O", .T.) + " VALOR_CONV, "
	cQuery +=                         " ( (OHB.OHB_VALOR * OH8.OH8_PERCEN) / 100 ) * (SELECT SINAL FROM " + cTpContTmp + " WHERE CODIGO = SED.ED_TPCOJR AND TIPO = 'O') VALOR_LANC, "
	cQuery +=                         " OHB.R_E_C_N_O_ RECNO "
	cQuery +=                  " FROM " +RetSqlName("OHB")+" OHB "
	cQuery +=                  " INNER JOIN " +RetSqlName("SED")+" SED "
	cQuery +=                          " ON (SED.ED_FILIAL = '"+ xFilial("SED")+ "' "
	cQuery +=                              " AND SED.ED_CODIGO = OHB.OHB_NATORI "
	cQuery +=                              " AND SED.D_E_L_E_T_ = ' ' ) "
	cQuery +=                  " INNER JOIN " +RetSqlName("OH6")+" OH6 "
	cQuery +=                          " ON (OH6.OH6_FILIAL = '"+ xFilial("OH6")+ "' "
	cQuery +=                              " AND OHB.OHB_CTRATO = OH6.OH6_CODIGO "
	cQuery +=                              " AND OH6.OH6_TIPO = '3' "
	cQuery +=                              " AND OH6.D_E_L_E_T_ = ' ') "
	cQuery +=                  " INNER JOIN " + RetSqlName("OH8")+" OH8 "
	cQuery +=                          " ON (OH8.OH8_FILIAL = '"+ xFilial("OH8")+ "' "
	cQuery +=                              " AND OH8.OH8_CODRAT = OH6.OH6_CODIGO "
	cQuery +=                              " AND ((SUBSTRING(OHB.OHB_DTLANC,1,6) >= OH8.OH8_AMINI AND OH8.OH8_AMFIM = '"+ cOH8AMFIM + "') "
	cQuery +=                                   "OR (SUBSTRING(OHB.OHB_DTLANC,1,6) BETWEEN OH8.OH8_AMINI AND OH8.OH8_AMFIM)) "
	cQuery +=                              " AND OH8.D_E_L_E_T_ = ' ') "
	cQuery +=                  " INNER JOIN " + RetSqlName("NUS")+" NUS "
	cQuery +=                          " ON (NUS.NUS_FILIAL = '"+ xFilial("NUS")+ "' "
	cQuery +=                              " AND OH8.OH8_CPARTI = NUS.NUS_CPART "
	cQuery +=                              " AND ((SUBSTRING(OHB.OHB_DTLANC,1,6) >= NUS.NUS_AMINI AND NUS.NUS_AMFIM = '"+ cNUSAMFIM + "') "
	cQuery +=                                   " OR (SUBSTRING(OHB.OHB_DTLANC,1,6) BETWEEN NUS.NUS_AMINI AND NUS.NUS_AMFIM)) "
	cQuery +=                              " AND NUS.D_E_L_E_T_ = ' ') "
	cQuery +=                  " WHERE OHB.OHB_FILIAL = '"+ xFilial("OHB")+ "' "
	cQuery +=                    " AND OHB.OHB_CTRATO > '"+ cSCTRATO + "' "
	cQuery +=                      cFilProje // Filtro Projeto/item
	cQuery +=                    " AND OHB.D_E_L_E_T_ = ' ') "
	
	cQuery +=                 " UNION "
	
	// LAN�AMENTOS DE DESTINO QUE SEJAM CENTRO DE CUSTO: TABELA de RATEIO (PROFISSIONAL)
	cQuery +=                 " (SELECT OHB.OHB_FILIAL, OHB.OHB_DTLANC, OHB.OHB_CPART, OHB.OHB_CODIGO DOCTO, OHB.OHB_NATDES NATUREZA, SED.ED_CMOEJUR MOEDA, OHB.OHB_NATORI NATUREZA2, OHB.OHB_CMOELC MOEDACONV, NUS.NUS_CESCR ESCRITORIO, NUS.NUS_CC CENTRO_CUSTO, 
	cQuery +=                           JP034CsVal(cTpContTmp, "D", .T.) + " VALOR_CONV, "
	cQuery +=                         " ( (OHB.OHB_VALOR * OH8.OH8_PERCEN) / 100 ) * (SELECT SINAL FROM " + cTpContTmp + " WHERE CODIGO = SED.ED_TPCOJR AND TIPO = 'D') VALOR_LANC, "
	cQuery +=                         " OHB.R_E_C_N_O_ RECNO "
	cQuery +=                  " FROM " +RetSqlName("OHB")+" OHB "
	cQuery +=                  " INNER JOIN " +RetSqlName("SED")+" SED "
	cQuery +=                          " ON (SED.ED_FILIAL = '"+ xFilial("SED")+ "' "
	cQuery +=                              " AND SED.ED_CODIGO = OHB.OHB_NATDES "
	cQuery +=                              " AND SED.D_E_L_E_T_ = ' ' ) "
	cQuery +=                  " INNER JOIN " +RetSqlName("OH6")+" OH6 "
	cQuery +=                          " ON (OH6.OH6_FILIAL = '"+ xFilial("OH6")+ "' "
	cQuery +=                              " AND OHB.OHB_CTRATD = OH6.OH6_CODIGO "
	cQuery +=                              " AND OH6.OH6_TIPO = '3' "
	cQuery +=                              " AND OH6.D_E_L_E_T_ = ' ') "
	cQuery +=                  " INNER JOIN " + RetSqlName("OH8")+" OH8 "
	cQuery +=                          " ON (OH8.OH8_FILIAL = '"+ xFilial("OH8")+ "' "
	cQuery +=                              " AND OH8.OH8_CODRAT = OH6.OH6_CODIGO "
	cQuery +=                              " AND ((SUBSTRING(OHB.OHB_DTLANC,1,6) >= OH8.OH8_AMINI AND OH8.OH8_AMFIM = '"+ cOH8AMFIM + "') "
	cQuery +=                                   " OR (SUBSTRING(OHB.OHB_DTLANC,1,6) BETWEEN OH8.OH8_AMINI AND OH8.OH8_AMFIM)) "
	cQuery +=                              " AND OH8.D_E_L_E_T_ = ' ') "
	cQuery +=                  " INNER JOIN " + RetSqlName("NUS")+" NUS "
	cQuery +=                          " ON (NUS.NUS_FILIAL = '"+ xFilial("NUS")+ "' "
	cQuery +=                              " AND OH8.OH8_CPARTI = NUS.NUS_CPART "
	cQuery +=                              " AND ((SUBSTRING(OHB.OHB_DTLANC,1,6) >= NUS.NUS_AMINI AND NUS.NUS_AMFIM = '"+ cNUSAMFIM + "') "
	cQuery +=                                   " OR (SUBSTRING(OHB.OHB_DTLANC,1,6) BETWEEN NUS.NUS_AMINI AND NUS.NUS_AMFIM)) "
	cQuery +=                              " AND NUS.D_E_L_E_T_ = ' ') "
	cQuery +=                  " WHERE OHB.OHB_FILIAL = '"+ xFilial("OHB")+ "' "
	cQuery +=                    " AND OHB.OHB_CTRATD > '"+ cSCTRATD + "' "
	cQuery +=                      cFilProje // Filtro Projeto/item
	cQuery +=                    " AND OHB.D_E_L_E_T_ = ' ') "
	cQuery +=                  ") EXT "
	cQuery += IIf(lEscrit, " INNER", " LEFT") + " JOIN "+ RetSqlName("NS7")+ " NS7 "
	cQuery +=                                     " ON (NS7_FILIAL = '"+ xFilial("NS7")+ "' "
	cQuery +=                                         " AND NS7.NS7_COD = EXT.ESCRITORIO "
	If lEscrit
		cQuery +=                                      " AND NS7.NS7_COD = '" + cEscritCC + "' " // escritorio (quando o filtro estiver ativo incluir todo o inner, se nao faz left para descri��o)
	EndIf
	cQuery +=                                          " AND NS7.D_E_L_E_T_ = ' ') "
	
	cQuery += IIf(lCusto, " INNER ", " LEFT ") + " JOIN "+ RetSqlName("CTT")+ " CTT "
	cQuery +=                                      " ON (CTT_FILIAL = '"+ xFilial("CTT")+ "' "
	cQuery +=                                          " AND CTT.CTT_CUSTO = EXT.CENTRO_CUSTO "
	If lCusto
		cQuery +=                                      " AND CTT.CTT_CUSTO = '" + cCusto + "' " // centro de custo (quando o filtro estiver ativo incluir todo o inner, se nao faz left para descri��o)
	EndIf
	cQuery +=                                          " AND CTT.D_E_L_E_T_ = ' ') "
	cQuery +=          " WHERE EXT.OHB_DTLANC BETWEEN '" + cDataIni + "'  AND '" + cDatafim + "' " // Periodo 
	cQuery +=          ") TAB "
	cQuery +=          " LEFT JOIN " +RetSqlName("RD0")+" RD0 "
	cQuery +=                 " ON (RD0.RD0_FILIAL = '"+ xFilial("RD0")+ "' "
	cQuery +=                      " AND RD0.RD0_CODIGO = TAB.SOLICITANTE " // Solicitante
	cQuery +=                      " AND RD0.D_E_L_E_T_ = ' ') "
	
	cQuery +=          " INNER JOIN " +RetSqlName("SED")+" SED "
	cQuery +=                  " ON (SED.ED_FILIAL = '"+ xFilial("SED")+ "' "
	cQuery +=                      " AND SED.ED_CODIGO = TAB.NATUREZA "

	If lExitNat
		cQuery +=                  " AND SED.ED_CODIGO = '" + cNatureza + "' " // natureza
	ElseIf !Empty(cNatureza)
		cQuery +=                  " AND SED.ED_CODIGO LIKE '" + AllTrim(cNatureza) + "%' " // natureza
	EndIf

	If !Empty(cTpConta)
		cQuery +=                  " AND SED.ED_TPCOJR = '" + cValToChar( cTpConta ) + "' " // tipo de conta
	EndIf

	If !Empty(cStatusNat) .And. cStatusNat != "1"
		cQuery +=                  " AND SED.ED_MSBLQL " + Iif(cStatusNat == '2', " = '1'", " != '1'") // situa��o natureza (alterar operador para bloqueado)
	EndIf
	cQuery +=                      " AND SED.D_E_L_E_T_ = ' ') "

	cQuery +=          " INNER JOIN " +RetSqlName("CTO")+" CTO "
	cQuery +=                  " ON (CTO.CTO_FILIAL = '"+ xFilial("CTO")+ "' "
	cQuery +=                      " AND CTO.CTO_MOEDA = TAB.MOEDA "
	cQuery +=                      " AND CTO.D_E_L_E_T_ = ' ') "
	
	cQuery +=          " LEFT JOIN " +RetSqlName("CTO")+" CTOCONV "
	cQuery +=                  " ON (CTOCONV.CTO_FILIAL = '"+ xFilial("CTO")+ "' "
	cQuery +=                      " AND CTOCONV.CTO_MOEDA = TAB.MOEDACONV "
	cQuery +=                      " AND CTOCONV.D_E_L_E_T_ = ' ') "
	
	cQuery +=          " LEFT JOIN "+ RetSqlName("NS7")+ " NS7 "
	cQuery +=                  " ON (NS7_FILIAL = '"+ xFilial("NS7")+ "' "
	cQuery +=                      " AND NS7.NS7_COD = TAB.ESCRITORIO "
	cQuery +=                      " AND NS7.D_E_L_E_T_ = ' ') "
	
	cQuery +=          " LEFT JOIN "+ RetSqlName("CTT")+ " CTT "
	cQuery +=                  " ON (CTT_FILIAL = '"+ xFilial("CTT")+ "' "
	cQuery +=                      " AND CTT.CTT_CUSTO = TAB.CENTRO_CUSTO "
	cQuery +=                      " AND CTT.D_E_L_E_T_ = ' ') "
	If !Empty(cFilEscr)
		cQuery +=      " WHERE TAB.FILIAL = '" + cFilEscr + "' " // Filial do lan�amento (Escrit�rio)
	Endif
	
	cQuery +=         " ORDER BY TAB.NATUREZA, TAB.ORDEM, TAB.ESCRITORIO, TAB.CENTRO_CUSTO, TAB.DATA, TAB.DOCTO "
	
	cQuery := ChangeQuery( cQuery )

	DbUseArea( .T. , "TOPCONN" , TcGenQry(,,cQuery) , cAlsTmp , .T. , .T. )

Return Nil

//=======================================================================
/*/{Protheus.doc} PrintReport
Fun��o para gerar PDF do relat�rio de Balancete Plano/Empresa.

@param  cReportName , caracter , Nome do relat�rio
@param  cDirectory  , caracter , Caminho da pasta

@author Jonatas Martins / Jorge Martins
@since  28/03/2018
/*/
//=======================================================================
Static Function PrintReport( cReportName , cDirectory )
	Local oPrinter          := Nil 
	Local cNameFile         := cReportName
	Local nIniH             := 0
	Local nFimH             := 560
	Local lAdjustToLegacy   := .F.
	Local lDisableSetup     := .T.
	Local aRetNewNat        := {}
	Local lNovaNatur        := .F.
	Local cCodEscrit        := ""
	Local cEscrit           := ""
	Local cCodCCusto        := ""
	Local cCCusto           := ""
	Local cNatNewPag        := ""
	
	Default cReportName := FwTimeStamp(1)
	Default cDirectory  := GetSrvProfString( "StartPath" , "" )
	
	//Configura��es do relat�rio		
	oPrinter := FWMsPrinter():New( cNameFile, IMP_PDF, lAdjustToLegacy, cDirectory, lDisableSetup,,, "PDF" ) 
	oPrinter:SetPortrait()
	oPrinter:SetPaperSize(DMPAPER_A4)
	oPrinter:SetMargin(60,60,60,60) 
	
	//Gera nova folha
	aRetNewNat := NewPage( @oPrinter , nIniH , nFimH )

	// IsNewNat est� sendo chamada aqui, para evitar erro de recurs�o "stack depth overflow"
	If !Empty(aRetNewNat) .And. Len(aRetNewNat) >= 6
		lNovaNatur := aRetNewNat[1]
		cCodEscrit := aRetNewNat[2]
		cEscrit    := aRetNewNat[3]
		cCodCCusto := aRetNewNat[4]
		cCCusto    := aRetNewNat[5]
		cNatNewPag := aRetNewNat[6]

		If lNovaNatur
			IsNewNat( oPrinter , nIniH , nFimH , /*nIniV*/ , /*nRegPos*/ , cCodEscrit , cEscrit , cCodCCusto , cCCusto, cNatNewPag)
		EndIf
	EndIf

	//Imprime se��o de escrit�rio	 
	PrintRepData( @oPrinter , nIniH , nFimH )
	
	//Gera arquivo relat�rio
	oPrinter:Print()
	
Return Nil

//=======================================================================
/*/{Protheus.doc} NewPage
Cria nova p�gina do relat�rio.

@param  oPrinter   , objeto     , Estrutra do relat�rio
@param  nIniH      , numerico   , Coordenada horizontal inicial
@param  nFimH      , numerico   , Coordenada horizontal final
@param  lImpTitCol , logico     , Indica se imprime os t�tulos das colunas
@param  cNatureza  , caractere  , Codigo da natureza corrente de impress�o

@return aRetNewNat , Dados para executar a fun��o IsNewNat()

@author Jonatas Martins / Jorge Martins
@since  28/03/2018
/*/
//=======================================================================
Static Function NewPage( oPrinter , nIniH , nFimH, lImpTitCol, cNatureza)
	Local cCodEscrit    := ""
	Local cEscrit       := ""
	Local cCodCCusto    := ""
	Local cCCusto       := ""
	Local lNovaNatur    := .F. // Indica se � uma nova Natureza (Linha de Saldo Anterior)
	Local aRetNewNat    := {}
	
	Default lImpTitCol := .T.
	Default cNatureza  := (cAlsTmp)->NATUREZA
	
	//Inicio P�gina
	oPrinter:StartPage()
	
	//Monta cabe�alho
	PrintHead( @oPrinter , nIniH , nFimH , @lNovaNatur , @cCodEscrit , @cEscrit , @cCodCCusto , @cCCusto, cNatureza )
	aRetNewNat := {lNovaNatur, cCodEscrit , cEscrit , cCodCCusto , cCCusto, cNatureza}

	// Monta t�tulos das colunas
	If lImpTitCol
		PrintTitCol( @oPrinter , nIniH , nFimH , 124, cNatureza )
	EndIf
	
	//Imprime Rodap�
	PrintFooter( @oPrinter , nIniH , nFimH )

Return aRetNewNat

//=======================================================================
/*/{Protheus.doc} PrintHead
Imprime dados do cabe�alho.

@param  oPrinter   , objeto     , Estrutra do relat�rio
@param  nIniH      , numerico   , Coordenada horizontal inicial
@param  nFimH      , numerico   , Coordenada horizontal final
@param  lNovaNatur , caractere  , Indica se � uma nova natureza
@param  cCodEscrit , caractere  , C�digo do Escrit�rio
@param  cEscrit    , caractere  , Nome do Escrit�rio
@param  cCodCCusto , caractere  , C�digo do Centro de Custo
@param  cCCusto    , caractere  , Descri��o do Centro de Custo
@param  cNatureza  , caractere  , Codigo da natureza corrente de impress�o

@author Jonatas Martins / Jorge Martins
@since  28/03/2018
/*/
//=======================================================================
Static Function PrintHead( oPrinter , nIniH , nFimH , lNovaNatur , cCodEscrit , cEscrit , cCodCCusto , cCCusto, cNatureza )
	Local oFontHead     := Nil
	Local oFontHead2    := Nil
	Local aDadosNat     := {}
	Local cSimb         := ""
	Local cEscritFil    := AllTrim( JurGetDados( "NS7", 1, xFilial("NS7") + AllTrim( MV_PAR03 ), "NS7_NOME" ) )
	Local cSaldoAnt     := FormatNum( 0 )
	Local lImpSaldoAnt  := nSaldoNat == 0
	
	Default cNatureza   := (cAlsTmp)->NATUREZA

	//-------------------------------------------------------------------------
	// Imprime saldo anterior a data inicial do filtro "MV_PAR01" caso existir
	//-------------------------------------------------------------------------
	If lImpSaldoAnt .And. (cAlsTmp)->ORDEM == "1"
		cSaldoAnt := FormatNum( (cAlsTmp)->VALOR_LANC )
		nSaldoNat += (cAlsTmp)->VALOR_LANC
		
		cCodEscrit := AllTrim( (cAlsTmp)->ESCRITORIO   )
		cEscrit    := AllTrim( (cAlsTmp)->NS7_NOME     )
		cCodCCusto := AllTrim( (cAlsTmp)->CENTRO_CUSTO )
		cCCusto    := AllTrim( (cAlsTmp)->CTT_DESC01   )

		(cAlsTmp)->( DbSkip() )

		// Verifica se houve mudan�a de natureza. 
		// Se houve, significa que n�o existem movimenta��es para a natureza anterior.
		lNovaNatur := !Empty(Alltrim((cAlsTmp)->NATUREZA)) .And. (cAlsTmp)->NATUREZA != cNatureza
	EndIf
	
	oFontHead   := TFont():New('Arial',,-16,,.T.,,,,,.F.,.F.)
	oFontHead2  := TFont():New('Arial',,-10,,.F.,,,,,.F.,.F.)
	
	//---------------------
	// T�tulo do relat�rio
	//---------------------
	oPrinter:SayAlign( 030, nIniH, STR0003, oFontHead, nFimH, 200, CLR_BLACK, 2, 1 ) //"Extrato de Contas (Centro de Custos) 
	
	//---------------------------------
	// Detalhes do filtro do relat�rio
	//---------------------------------
	oPrinter:Line( 060, nIniH, 060, nFimH, 0, "-8")
	oPrinter:Say( 070, nIniH , I18N( STR0004 , { cValToChar( MV_PAR01 ), cValToChar( MV_PAR02 ) } ), oFontHead2, 1200,/*color*/) //"Per�odo de #1 � #2"
	
	oPrinter:SayAlign( 062, nIniH, I18N( STR0005, { cEscritFil } ), oFontHead2, nFimH, 200, CLR_BLACK, 1, 1 ) //"Escrit�rio: #1"
	oPrinter:Line( 074, nIniH, 074, nFimH, 0, "-8")
	
	//---------------------------------
	// Detalhes da natureza
	//---------------------------------
	aDadosNat := JurGetDados( "SED" , 1 , xFilial("SED") + cNatureza , {"ED_DESCRIC", "ED_CMOEJUR"} )
	cSimb     := JurGetDados( "CTO" , 1 , xFilial("CTO") + aDadosNat[2], "CTO_SIMB" )
	oPrinter:Line( 088, nIniH, 088, nFimH, CLR_HRED, "-8")
	oPrinter:Say( 098, nIniH , I18N( STR0006 , { Alltrim( cNatureza ) , Alltrim( aDadosNat[1] ) , AllTrim( cSimb ) } ), oFontHead2, 1200,/*color*/) //"Natureza: #1 - #2 (Valores em #3)"
	
	If lImpSaldoAnt
		oPrinter:SayAlign( 098 -8, nIniH, STR0019 + ": " + cSaldoAnt, oFontHead2, nFimH, 200, CLR_BLACK, 1, 1 ) //"Saldo Anterior"
	EndIf
	
	oPrinter:Line( 102, nIniH, 102, nFimH, CLR_HRED, "-8")

Return Nil

//=======================================================================
/*/{Protheus.doc} PrintTitCol
Imprime t�tulo das colonas do relat�rio.

@param  oPrinter   , objeto     , Estrutra do relat�rio
@param  nIniH      , numerico   , Coordenada horizontal inicial
@param  nFimH      , numerico   , Coordenada horizontal final
@param  nIniV      , numerico   , Coordenada vertical inicial
@param  cNatureza  , caractere  , Codigo da natureza corrente de impress�o

@author Jonatas Martins / Jorge Martins
@since  28/03/2018
/*/
//=======================================================================
Static Function PrintTitCol( oPrinter, nIniH, nFimH, nIniV, cNatureza )	
	Local oFontTitCol := Nil
	Local cEscrit     := AllTrim( (cAlsTmp)->ESCRITORIO )   + " - " + AllTrim( (cAlsTmp)->NS7_NOME )
	Local cCC         := AllTrim( (cAlsTmp)->CENTRO_CUSTO ) + " - " + AllTrim( (cAlsTmp)->CTT_DESC01 )
	
	oFontTitCol := TFont():New('Arial',,-10,,.F.,,,,,.F.,.F.)
	
	//-----------------------
	// Avalia fim da p�gina
	//----------------------- 
	EndPage( @oPrinter , nIniH , nFimH , @nIniV , /*nRegPos*/ , (4 * nSalto), /*lImpTitCol*/,  /*lEndForced*/, cNatureza )
	
	oPrinter:Say( nIniV          , nIniH, I18N( STR0005, { cEscrit } ), oFontTitCol, 1200,/*color*/) //"Escrit�rio: #1"
	oPrinter:Say( nIniV += nSalto, nIniH, I18N( STR0007, { cCC     } ), oFontTitCol, 1200,/*color*/) //"Centro de Custo: #1"
	nIniV += nSalto
	
	oPrinter:Line( nIniV, nIniH, nIniV, nFimH, CLR_HRED, "-8")
	oPrinter:Say( nIniV += 9, nIniH + nPColData , STR0008 , oFontTitCol, 1200,/*color*/) //"Data"
	oPrinter:Say( nIniV     , nIniH + nPColSol  , STR0009 , oFontTitCol, 1200,/*color*/) //"Solic."
	oPrinter:Say( nIniV     , nIniH + nPColHist , STR0010 , oFontTitCol, 1200,/*color*/) //"Hist�rico"
	oPrinter:Say( nIniV     , nIniH + nPColNat  , STR0011 , oFontTitCol, 1200,/*color*/) //"Natureza"
	oPrinter:Say( nIniV     , nIniH + nPColLanc , STR0020 , oFontTitCol, 1200,/*color*/) //"Lan�amento"
	oPrinter:Say( nIniV     , nIniH + nPColValCv, " "     , oFontTitCol, 1200,/*color*/)
	oPrinter:Say( nIniV     , nIniH + nPColVal  , STR0012 , oFontTitCol, 1200,/*color*/) //"Valor"
	oPrinter:Say( nIniV     , nIniH + nPColSaldo, STR0013 , oFontTitCol, 1200,/*color*/) //"Saldo"
	oPrinter:Line( nIniV += 4, nIniH, nIniV, nFimH, CLR_HRED, "-8")
	nIniV += nSalto

Return Nil

//=======================================================================
/*/{Protheus.doc} PrintFooter
Imprimide rodap� do cabe�alho.

@param  oPrinter, objeto   , Estrutra do relat�rio
@param  nIniH   , numerico , Coordenada horizontal inicial
@param  nFimH   , numerico , Coordenada horizontal final

@author Jonatas Martins / Jorge Martins
@since	28/03/2018
/*/
//=======================================================================
Static Function PrintFooter( oPrinter , nIniH , nIniF )
	Local oFontRod := Nil
	Local nLinRod  := 830
	
	oFontRod := TFont():New('Arial',,-10,,.F.,,,,,.F.,.F.)
	
	oPrinter:Line( nLinRod, nIniH, nLinRod, nIniF, CLR_HRED, "-8")
	nLinRod += nSalto
	oPrinter:SayAlign( nLinRod, nIniH, cDateFt + " - " + cTimeFt, oFontRod, nIniF, 200, CLR_BLACK, 2, 1 )
	oPrinter:SayAlign( nLinRod, nIniH, cValToChar( nPage )      , oFontRod, nIniF, 200, CLR_BLACK, 1, 1 )

Return Nil

//=======================================================================
/*/{Protheus.doc} PrintSubTot
Imprimide subtotal na quebra por escrit�rio.

@param  oPrinter   , objeto     , Estrutra do relat�rio
@param  nIniH      , numerico   , Coordenada horizontal inicial
@param  nFimH	   , numerico   , Coordenada horizontal final
@param  nIniV	   , numerico   , Coordenada vertical inicial
@param  cCodEscrit , caractere  , C�digo do Escrit�rio
@param  cEscrit    , caractere  , Nome do Escrit�rio
@param  cCodCCusto , caractere  , C�digo do Centro de Custo
@param  cCCusto    , caractere  , Descri��o do Centro de Custo
@param  cNatureza  , caractere  , Codigo da natureza corrente de impress�o

@author Jonatas Martins / Jorge Martins
@since  28/03/2018
/*/
//=======================================================================
Static Function PrintSubTot( oPrinter , nIniH , nFimH , nIniV , cCodEscrit , cEscrit , cCodCCusto , cCCusto, cNatureza )
	Local oFontSubTot := Nil
	Local cSubTot     := FormatNum(nSubTotEsc)
	
	Default cCodEscrit := (cAlsTmp)->ESCRITORIO
	Default cEscrit    := (cAlsTmp)->NS7_NOME
	Default cCodCCusto := (cAlsTmp)->CENTRO_CUSTO
	Default cCCusto    := (cAlsTmp)->CTT_DESC01
	Default cNatureza  := (cAlsTmp)->NATUREZA
	
	oFontSubTot := TFont():New('Arial',,-10,,.T.,,,,,.F.,.F.)
	
	//-----------------------
	// Avalia fim da p�gina
	//----------------------- 
	EndPage( @oPrinter , nIniH , nFimH , @nIniV , /*nRegPos*/ , (3 * nSalto), .F., /*lEndForced*/ , cNatureza )

	oPrinter:SayAlign( nIniV,  -100, I18N( STR0014, { AllTrim(cCodEscrit) + " - " + AllTrim(cEscrit) } ) , oFontSubTot, nFimH, 200, CLR_BLACK, 1, 1 ) //"Total do Escrit�rio #1: "
	oPrinter:SayAlign( nIniV, nIniH, cSubTot , oFontSubTot, nFimH, 200, CLR_BLACK, 1, 1 )
	
	nIniV += nSalto
	
	oPrinter:SayAlign( nIniV, -100 , I18N( STR0015, { AllTrim(cCodCCusto) + " - " + AllTrim(cCCusto) } ) , oFontSubTot, nFimH, 200, CLR_BLACK, 1, 1 ) //"Total do Centro de Custo #1: "
	oPrinter:SayAlign( nIniV, nIniH, cSubTot  , oFontSubTot, nFimH, 200, CLR_BLACK, 1, 1 )
	
	oPrinter:Line( nIniV + 12, 460, nIniV + 12, nFimH, 0, "-8")
	
	nIniV += nSalto

Return Nil

//=======================================================================
/*/{Protheus.doc} PrintTotGer
Imprimide Total Geral

@param  oPrinter   , objeto     , Estrutra do relat�rio
@param  nIniH      , numerico   , Coordenada horizontal inicial
@param  nFimH	   , numerico   , Coordenada horizontal final
@param  nIniV      , numerico   , Coordenada vertical inicial
@param  cNatureza  , caractere  , Codigo da natureza corrente de impress�o

@author Jonatas Martins / Jorge Martins
@since  28/03/2018
/*/
//=======================================================================
Static Function PrintTotGer( oPrinter , nIniH , nFimH , nIniV , cNatureza)
	Local oFontTotGer := TFont():New('Arial',,-10,,.T.,,,,,.F.,.F.)
	
	Default cNatureza  := (cAlsTmp)->NATUREZA

	//-----------------------
	// Avalia fim da p�gina
	//----------------------- 
	EndPage( @oPrinter , nIniH , nFimH , @nIniV , /*nRegPos*/ , (5 * nSalto), .F., /*lEndForced*/ , cNatureza )

	nIniV += nSalto + 5
	
	oPrinter:SayAlign( nIniV,  -100, STR0016              , oFontTotGer, nFimH, 200, CLR_BLACK, 1, 1 ) //"TOTAL DE ENTRADA (+)"
	oPrinter:SayAlign( nIniV, nIniH, FormatNum( nTotEnt ) , oFontTotGer, nFimH, 200, CLR_BLACK, 1, 1 )
	
	nIniV += nSalto + 5
	
	oPrinter:SayAlign( nIniV,  -100, STR0017               , oFontTotGer, nFimH, 200, CLR_BLACK, 1, 1 ) //"TOTAL DE SAIDA (-)"
	oPrinter:SayAlign( nIniV, nIniH, FormatNum( nTotSaida ), oFontTotGer, nFimH, 200, CLR_BLACK, 1, 1 )
	
	nIniV += nSalto + 5
	
	oPrinter:SayAlign( nIniV,  -100, STR0018                          , oFontTotGer, nFimH, 200, CLR_BLACK, 1, 1 ) //"SALDO FINAL ="
	oPrinter:SayAlign( nIniV, nIniH, FormatNum( nSaldoNat ), oFontTotGer, nFimH, 200, CLR_BLACK, 1, 1 )
	
	oPrinter:Line( nIniV + 12, 460, nIniV + 12, nFimH, 0, "-8")
	oPrinter:Line( nIniV + 14, 460, nIniV + 14, nFimH, 0, "-8")
	
Return Nil

//=======================================================================
/*/{Protheus.doc} PrintRepData
Imprime registros do relat�rio.

@param  oPrinter, objeto   , Estrutra do relat�rio
@param  nIniH	, numerico , Coordenada horizontal inicial
@param  nFimH	, numerico , Coordenada horizontal final

@author Jonatas Martins / Jorge Martins
@since  28/03/2018
/*/
//=======================================================================
Static Function PrintRepData( oPrinter , nIniH , nFimH )
	Local oFontReg   := Nil
	Local oFontCv    := Nil
	Local cNatureza  := ""
	Local cHist      := ""
	Local cValorConv := ""
	Local cValorLanc := ""
	Local cCodEscrit := ""
	Local cEscrit    := ""
	Local cCodCCusto := ""
	Local cCCusto    := ""
	Local nIniV      := 165
	Local nRegPos    := 1
	Local nValorLanc := 0
	
	oFontReg := TFont():New('Arial',,-7,,.F.,,,,,.F.,.F.)
	oFontCv  := TFont():New('Arial',,-6,,.F.,,,,,.F.,.F.)
	
	While (cAlsTmp)->( ! Eof() )
		//-----------------------
		// Avalia fim da p�gina
		//----------------------- 
		EndPage( @oPrinter , nIniH , nFimH , @nIniV , @nRegPos, /*nNewIniV*/, /*lImpTitCol*/ )
		//-----------------------
		// Insere cor nas linhas
		//-----------------------
		ColorLine( @oPrinter , nIniH , nFimH , nIniV , nRegPos )
		//-----------------------
		// Imprime registros
		//-----------------------
		cHist := GetHistLanc( (cAlsTmp)->ORDEM , (cAlsTmp)->RECNO )
		oPrinter:Say( nIniV ,   nIniH + nPColData , cValToChar( StoD( (cAlsTmp)->DATA ) )   , oFontReg , 1200 ,/*color*/) //Data
		oPrinter:Say( nIniV ,   nIniH + nPColSol  , AllTrim( (cAlsTmp)->RD0_SIGLA )         , oFontReg , 1200 ,/*color*/) //Solic
		oPrinter:Say( nIniV ,   nIniH + nPColHist , cHist                                   , oFontReg , 1200 ,/*color*/) //Hist�rico
		oPrinter:Say( nIniV ,   nIniH + nPColLanc , (cAlsTmp)->DOCTO                        , oFontReg , 1200 ,/*color*/) //Lan�amento
		oPrinter:Say( nIniV ,   nIniH + nPColNat  , (cAlsTmp)->NATUREZA2                    , oFontReg , 1200 ,/*color*/) //Natureza
		
		If (cAlsTmp)->VALOR_CONV <> 0
			nValorLanc := (cAlsTmp)->VALOR_CONV
			cValorConv := "(" + AllTrim((cAlsTmp)->MOEDA_CONV) + " "  + FormatNum( (cAlsTmp)->VALOR_LANC ) + ")" //Valor Convertido
			cValorLanc := FormatNum( nValorLanc ) // Valor na moeda da natureza
			
			oPrinter:SayAlign( nIniV - 7, -105 , cValorConv , oFontCv  , nFimH, 200, CLR_BLACK, 1, 1 )
			oPrinter:SayAlign( nIniV - 8, -60  , cValorLanc , oFontReg , nFimH, 200, CLR_BLACK, 1, 1 )
		Else
			nValorLanc := (cAlsTmp)->VALOR_LANC
			cValorLanc := FormatNum( nValorLanc ) // Valor na moeda da natureza
			oPrinter:SayAlign( nIniV - 8, -60  , cValorLanc , oFontReg , nFimH, 200, CLR_BLACK, 1, 1 )
		EndIf 
		
		oPrinter:SayAlign( nIniV - 8, nIniH, FormatNum( nSaldoNat += nValorLanc ), oFontReg, nFimH, 200, CLR_BLACK, 1, 1 )  //Saldo
		
		cNatureza  := (cAlsTmp)->NATUREZA
		cCodEscrit := (cAlsTmp)->ESCRITORIO
		cEscrit    := (cAlsTmp)->NS7_NOME
		cCodCCusto := (cAlsTmp)->CENTRO_CUSTO
		cCCusto    := (cAlsTmp)->CTT_DESC01
		
		nSubTotEsc += nValorLanc            // Subtotal do escrit�rio

		If nValorLanc < 0 
			nTotSaida += nValorLanc         // Total Geral de Saida
		Else
			nTotEnt   += nValorLanc         // Total Geral de Entrada
		EndIf
		nIniV      += nSalto                // Pula linha
		
		(cAlsTmp)->( DbSkip() )
		
		//-----------------------------
		// Avalia quebra de linha
		//-----------------------------
		IsBrokenRep( @oPrinter , nIniH , nFimH , @nIniV , @nRegPos , cNatureza, cCodEscrit , cEscrit , cCodCCusto , cCCusto )
	End

	//Imprime Subtotal da �ltima sess�o
	PrintSubTot( @oPrinter , nIniH , nFimH , @nIniV , cCodEscrit , cEscrit , cCodCCusto , cCCusto, cNatureza )
	
	//Imprime Total Geral
	PrintTotGer( @oPrinter , nIniH , nFimH , nIniV, cNatureza )

Return Nil

//=======================================================================
/*/{Protheus.doc} EndPage
Avalia quebra de p�gina.

@param  oPrinter   , objeto     , Estrutra do relat�rio
@param  nIniH      , numerico   , Coordenada horizontal inicial
@param  nFimH      , numerico   , Coordenada horizontal final
@param  nIniV      , numerico   , Coordenada vertical inicial
@param  nRegPos    , numerico   , Contador de registros
@param  nNewIniV   , numerico   , Coordenada vertical que ser� verificada
@param  lImpTitCol , logico     , Indica se imprime os t�tulos das colunas
@param  lEndForced , logico     , Indica se deve ser for�ada a quebra da p�gina
                                  Usado quando existe mudan�a de natureza na impress�o
@param  cNatureza  , caractere  , Codigo da natureza corrente de impress�o

@author Jonatas Martins / Jorge Martins
@since  28/03/2018
/*/
//=======================================================================
Static Function EndPage( oPrinter , nIniH , nFimH , nIniV , nRegPos, nNewIniV, lImpTitCol, lEndForced, cNatureza )
	Local nIFimV     := 825  // Coordenada vertical final
	Local aRetNewNat := {}
	Local lNovaNatur := .F.
	Local cCodEscrit := ""
	Local cEscrit    := ""
	Local cCodCCusto := ""
	Local cCCusto    := ""
	Local cNatNewPag := ""

	Default nRegPos    := 1
	Default nNewIniV   := 0
	Default lImpTitCol := .T.
	Default lEndForced := .F.
	Default cNatureza  := (cAlsTmp)->NATUREZA

	If lEndForced .Or. ( nIniV + nNewIniV ) >= nIFimV
		nIniV   := IIf(lImpTitCol , 165, 124 )
		nPage += 1
		oPrinter:EndPage()
		aRetNewNat := NewPage( @oPrinter , nIniH , nFimH, lImpTitCol, cNatureza )

		// IsNewNat est� sendo chamada aqui, para evitar erro de recurs�o "stack depth overflow"
		If !Empty(aRetNewNat) .And. Len(aRetNewNat) >= 6
			lNovaNatur := aRetNewNat[1]
			cCodEscrit := aRetNewNat[2]
			cEscrit    := aRetNewNat[3]
			cCodCCusto := aRetNewNat[4]
			cCCusto    := aRetNewNat[5]
			cNatNewPag := aRetNewNat[6]

			If lNovaNatur
				IsNewNat( oPrinter , nIniH , nFimH , /*nIniV*/ , /*nRegPos*/ , cCodEscrit , cEscrit , cCodCCusto , cCCusto, cNatNewPag)
			EndIf
		EndIf
		nRegPos := 1
	EndIf

Return Nil

//=======================================================================
/*/{Protheus.doc} ColorLine
Muda cor da linha impressa.

@param   oPrinter, objeto   , Estrutra do relat�rio
@param   nIniH   , numerico , Coordenada horizontal inicial
@param   nFimH   , numerico , Coordenada horizontal final
@param   nIniV   , numerico , Coordenada vertical inicial
@param   nRegPos , numerico , Contador de registros
@param   lForce  , logico   , For�a alterar cor da linha
@param   nColor  , numerico , Cor da linha

@author  Jonatas Martins / Jorge Martins
@since   28/03/2018
/*/
//=======================================================================
Static Function ColorLine( oPrinter , nIniH , nFimH , nIniV , nRegPos , lForce , nColor )
	Local aCoords := {}
	Local oBrush  := Nil
	Local cPixel  := ""
	
	Default nRegPos	:= 1
	Default lForce	:= .F.
	Default nColor 	:= RGB( 220 , 220 , 220 )
	
	//-----------------------------
	// Avalia se a linha � impar
	//-----------------------------
	If Mod( nRegPos , 2 ) == 0 .Or. lForce
		oBrush  :=  TBrush():New( Nil , nColor )
		aCoords := { nIniV - 8 , nIniH , nIniV + 2 , nFimH }
		cPixel  := "-2"
		oPrinter:FillRect( aCoords , oBrush , cPixel )
	EndIf
Return Nil

//=======================================================================
/*/{Protheus.doc} GetHistLanc
Pega hist�rico do lan�amento

@param  nTpLanc   , Tipo do lan�amento 1=Saldo Anterior; 2=Hist�rico Atual
@param  nRecno    , Recno do registro

@return cHistorico, Parte do hist�rico que poder� ser exibida no relat�rio

@author Jonatas Martins / Jorge Martins
@since  28/03/2018
/*/
//=======================================================================
Static Function GetHistLanc( nTpLanc , nRecno )
	Local aArea      := GetArea()
	Local aAreaOHB   := OHB->( GetArea() )
	Local cHistorico := ""
	Local nLength    := 75
	
	If nTpLanc == "1"
		cHistorico := STR0019 //"Saldo Anterior"
	Else
		OHB->( DbGoTo( nRecno ) )

		cHistorico := StrTran(OHB->OHB_HISTOR, Chr(13) + Chr(10), " ") // Substitui a quebra de linhas dos textos por em espa�o simples

		If Len(cHistorico) > nLength
			cHistorico := SubStr(cHistorico, 1, nLength) + "..."
		EndIf
	EndIf 
	
	RestArea( aAreaOHB )
	RestArea( aArea )

Return ( cHistorico )

//=======================================================================
/*/{Protheus.doc} IsBrokenRep
Avalia quebra de relat�rio.

@param  oPrinter   , objeto     , Estrutra do relat�rio
@param  nIniH      , numerico   , Coordenada horizontal inicial
@param  nFimH      , numerico   , Coordenada horizontal final
@param  nIniV      , numerico   , Coordenada vertical inicial
@param  nRegPos    , numerico   , Contador de registros
@param  cNatureza  , caractere  , Natureza
@param  cCodEscrit , caractere  , C�digo do Escrit�rio
@param  cEscrit    , caractere  , Nome do Escrit�rio
@param  cCodCCusto , caractere  , C�digo do Centro de Custo
@param  cCCusto    , caractere  , Descri��o do Centro de Custo

@author Jonatas Martins / Jorge Martins
@since  28/03/2018
/*/
//=======================================================================
Static Function IsBrokenRep( oPrinter , nIniH , nFimH , nIniV , nRegPos , cNatureza , cCodEscrit , cEscrit , cCodCCusto , cCCusto )	
	Local cNovaNatur  := (cAlsTmp)->NATUREZA
	Local cNovoEscrit := (cAlsTmp)->ESCRITORIO
	Local cNovoCCusto := (cAlsTmp)->CENTRO_CUSTO

	//-----------------------------------------
	// Avalia quebra de p�gina (Nova natureza)
	//-----------------------------------------
	If !Empty(cNovaNatur) .And. (cNatureza != cNovaNatur)
	
		// Imprime os totalizadores e quebra p�gina para impress�o da nova natureza
		IsNewNat( @oPrinter , nIniH , nFimH , @nIniV , nRegPos , cCodEscrit , cEscrit , cCodCCusto , cCCusto, cNatureza)

	//-------------------------------------------------------------
	// Avalia quebra de sess�o (Novo Escrit�rio / Centro de Custo)
	//-------------------------------------------------------------
	ElseIf !Empty(cNovoEscrit) .And. !Empty(cNovoCCusto) .And. (cCodEscrit != cNovoEscrit .Or. cCodCCusto != cNovoCCusto)
		//------------------
		// Imprime subtotal
		//------------------
		PrintSubTot( @oPrinter , nIniH , nFimH , @nIniV , cCodEscrit , cEscrit , cCodCCusto , cCCusto, cNatureza )
		nRegPos    := 1 // Zera contador de registros
		nSubTotEsc := 0 // Zera subtotal por escrit�rio
		nIniV      += ( 2 * nSalto )
		//---------------------------------------------------------------------
		// Avalia se existem mais registros a ser impressos e imprime colunas
		//---------------------------------------------------------------------
		If (cAlsTmp)->( ! Eof() )
			PrintTitCol( @oPrinter , nIniH , nFimH , @nIniV )
		EndIf
	Else
		nRegPos ++      // Incrementa contador de registros
	EndIf
	
Return Nil

//=======================================================================
/*/{Protheus.doc} FormatNum
Coloca separa��o decimal nos valores num�ricos

@param  nValue  , numerico , Numero a ser formatado

@author Jonatas Martins / Jorge Martins
@since  28/03/2018
/*/
//=======================================================================
Static Function FormatNum( nValue )
	Local cNumber := ""
	
	Default nValue := 0
	
	cNumber := AllTrim( TransForm( nValue , PesqPict( "OHB" , "OHB_VALOR" ) ) )

Return ( cNumber )

//=======================================================================
/*/{Protheus.doc} IsNewNat
Quebra p�gina do relat�rio quando existe mudan�a de natureza.

@param  oPrinter   , objeto     , Estrutra do relat�rio
@param  nIniH      , numerico   , Coordenada horizontal inicial
@param  nFimH      , numerico   , Coordenada horizontal final
@param  nIniV      , numerico   , Coordenada vertical inicial
@param  nRegPos    , numerico   , Contador de registros
@param  cCodEscrit , caractere  , C�digo do Escrit�rio
@param  cEscrit    , caractere  , Nome do Escrit�rio
@param  cCodCCusto , caractere  , C�digo do Centro de Custo
@param  cCCusto    , caractere  , Descri��o do Centro de Custo
@param  cNatureza  , caractere  , Codigo da natureza corrente de impress�o

@author Jonatas Martins / Jorge Martins
@since  31/03/2018
/*/
//=======================================================================
Static Function IsNewNat( oPrinter , nIniH , nFimH , nIniV , nRegPos , cCodEscrit , cEscrit , cCodCCusto , cCCusto, cNatureza)

	Default nIniV   := 165
	Default nRegPos := 1

	//Imprime Subtotal da �ltima sess�o
	PrintSubTot( @oPrinter , nIniH , nFimH , @nIniV , cCodEscrit , cEscrit , cCodCCusto , cCCusto, cNatureza )

	//Imprime Total Geral
	PrintTotGer( @oPrinter , nIniH , nFimH , nIniV, cNatureza )
	
	// � necess�rio zerar os valores de totais, pois s�o totalizadores por natureza e houve mudan�a de natureza
	nSaldoNat  := 0 // Saldo da natureza
	nSubTotEsc := 0 // Subtotal por escrit�rio
	nTotEnt    := 0 // Total Geral de Entrada
	nTotSaida  := 0 // Total Geral de Saida
	nRegPos    := 1 // Contador de registros

	If (cAlsTmp)->( !Eof() )
		cNatureza := Nil
	EndIf
	// Finaliza a p�gina para troca de natureza
	EndPage( @oPrinter , nIniH , nFimH , @nIniV , nRegPos , /*nNewIniV*/, /*lImpTitCol*/, .T., cNatureza )

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} JP034VlDt
Valida data inicial e final

@param dDataIni , data, Data inicial do filtro
@param dDataFim , data, Data final do filtro

@return lRet, logico, T./.F. As informa��es s�o v�lidas ou n�o

@author Jonatas Martins / Jorge Martins
@since 30/03/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Function JP034VlDt(dDataIni, dDataFim)
Local lRet := .T.

If !Empty(dDataIni) .And. !Empty(dDataFim)

	If dDataIni > Date()
		JurMsgErro(STR0029,,STR0026) // "Data Final deve ser maior que a inicial." - "Informe uma data valida."
		lRet := .F.
	EndIf

	If dDataIni > dDataFim
		JurMsgErro(STR0025,,STR0026) // "Data Final deve ser maior que a inicial." - "Informe uma data valida."
		lRet := .F.
	EndIf

EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JP034VlTpC
Valida��o do Tipo de conta - ED_TPCOJR

@param cTpConta, caractere, Valor informado no campo

@return lRet, logico, T./.F. As informa��es s�o v�lidas ou n�o

@author Jonatas Martins / Jorge Martins
@since 30/03/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Function JP034VlTpC(cTpConta)
Local lRet := .T.

If !Empty(cTpConta)
	lRet := JVldTpCont(cTpConta)
EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JP034VlNat
Valida��o do campo de natureza

@param cNatureza, C�digo da Natureza

@return lRet, logico, T./.F. As informa��es s�o v�lidas ou n�o

@author Bruno Ritter / Abner Foga�a
@since 19/07/2019
/*/
//-------------------------------------------------------------------
Function JP034VlNat(cNatureza)
	Local lRet := .T.

	SED->( DbSetOrder( 1 ) )
	If !Empty(cNatureza) .And. !SED->(DbSeek(xFilial("SED") + AllTrim(cNatureza)))
		JurMsgErro(I18N(STR0027, {cNatureza}), , STR0028) //"A natureza '#1' n�o foi localizada." //"Selecione uma natureza v�lida."
		lRet := .F.
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JP034CsVal
Peda�o da query para retornar o valor correspodente do lan�amento
na moeda da natureza.

@param cTpContTmp, GetTmpName da tabela tempor�ria referente ao Tipo de contas
@param cOriNat   , Onde a natureza est� no lan�amento na query: O=Origem, D=Destino
@param lPerOH8   , Retorna o valor conforme o percentual na OH8 (Tabela de Rateio)
@param lVlMoeLanc, Retorna o valor do lan�amento quando a moeda do lan�amento � o
                   mesmo que a da natureza, se informar .F. retorna 0 nessa situa��o

@Return cQuery, Case para retorna o valor do lan�amento na moeda da natureza

@author  Bruno Ritter
@since   17/10/2019
/*/
//-------------------------------------------------------------------
Static Function JP034CsVal(cTpContTmp, cOriNat, lPerOH8, lVlMoeLanc)
Local cQuery := ""

Default lPerOH8    := .F.
Default lVlMoeLanc := .F.

	cQuery := " CASE WHEN OHB.OHB_CMOELC = SED.ED_CMOEJUR "
	If lVlMoeLanc
		If lPerOH8
			cQuery +=  " THEN ( (OHB.OHB_VALOR * OH8.OH8_PERCEN) / 100) "
		Else
			cQuery +=  " THEN OHB.OHB_VALOR "
		EndIf
		cQuery +=                         " * (SELECT SINAL FROM " + cTpContTmp + " "
		cQuery +=                             " WHERE CODIGO = SED.ED_TPCOJR AND TIPO = '" + cOriNat + "') "
	Else
		cQuery +=      " THEN 0 "
	EndIf

	cQuery +=      " WHEN OHB.OHB_CMOEC = SED.ED_CMOEJUR "
	If lPerOH8
		cQuery +=       " THEN ( (OHB.OHB_VALOR * OH8.OH8_PERCEN) / 100) "
	Else
		cQuery +=       " THEN  OHB.OHB_VALOR * OHB.OHB_COTAC"
	EndIf
	cQuery +=                                 " * (SELECT SINAL FROM " + cTpContTmp + " "
	cQuery +=                                    "  WHERE CODIGO = SED.ED_TPCOJR AND TIPO = '" + cOriNat + "') "

	If lPerOH8
		cQuery +=  " ELSE ( (OHB.OHB_VLNAC * OH8.OH8_PERCEN) / 100) "
	Else
		cQuery +=  " ELSE  OHB.OHB_VLNAC "
	EndIf
	cQuery +=                         " * (SELECT SINAL FROM " + cTpContTmp + " "
	cQuery +=                             " WHERE CODIGO = SED.ED_TPCOJR AND TIPO = '" + cOriNat + "') "
	cQuery +=                         " * (SELECT CTP_TAXA FROM " + RetSqlName("CTP") + " CTP "
	cQuery +=                              " WHERE CTP_FILIAL = '" + xFilial("CTP") + "' "
	cQuery +=                                " AND CTP_DATA = OHB.OHB_DTLANC "
	cQuery +=                                " AND CTP_MOEDA = SED.ED_CMOEJUR) "
	cQuery += " END "

Return cQuery

//-------------------------------------------------------------------
/*/{Protheus.doc} JP034VlPro()
Valida��o do campo do pergunte MV_PAR09 - Projeto/finalidade

@Return lRet, se o campo � v�lido

@author  Bruno Ritter
@since   30/10/2019
/*/
//-------------------------------------------------------------------
Function JP034VlPro(cProjeto)
	Local lRet := Empty(cProjeto) .Or. ExistCpo('OHL', cProjeto , 1)
	MV_PAR10 := Space(TamSx3("OHM_ITEM")[1]) // Limpa o campo de item do projeto
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JP034VlItm()
Valida��o do campo do pergunte MV_PAR10 - Item do projeto

@Return lRet, se o campo � v�lido

@author  Bruno Ritter
@since   30/10/2019
/*/
//-------------------------------------------------------------------
Function JP034VlItm(cProjeto, cItemPrj)
	Local lRet := ExistCpo('OHM', cProjeto + cItemPrj, 1)
Return lRet
