#INCLUDE "PROTHEUS.CH"
#INCLUDE "MATXDEF.CH"

//-----------------------------------------------------------------------------------------------------------------------
//Este fonte tem objetivo de concentrar todas as fun��es e regras do configurador de tributos
//com objetivo de centralizar o c�digo do configurador, evitando assim eventuais problemas de concorr�ncia de fontes
//Somente fun��es que s�o envolvidas com o configurador dever�o ser adicionadas nestes fonte.
//Este fonte � dependente da MATXFIS, IMPXFIS e MATXDEF
//-----------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------
/*/{Protheus.doc} xFisTrbGen()
Fun��o que far� o enquadramento das regras de tributos g�ricos, procurando
pelos perfis de opera��o, produto/origem, opera��o e origem/destino.
Esta fun��o tamb�m far� os c�lculos dos tributos gen�ricos, e todas as informa��es
das regras e valores ser�o atualizados diretamente no aNfItem.

@param aNfCab   - Array com as informa��es cabe�alho da nota fiscal
@param aNfItem  - Array com toda as informa��es do item da nota fiscal
@param nItem    - N�mero do item da nota fiscal
@param cCampo   - Campo processado na Recall
@param cExecuta - Campo com propriedade do tributo gen�rico que dever� ser processada, BSE, VLR ou ALQ
@param cTrib    - Tributo gen�rico que dever� ser processado
@param aPos    - Array com cache dos fieldpos
@param aDic    - Array com cache de aliasindic
@param nTGITRef - Tamanho do array ItemRef dos tributos gen�ricos

@author Erick Gon�alves Dias
@since 26/06/2018
@version 12.1.17
/*/
//-------------------------------------------------------------------
Function FisTribGen(aNfCab, aNfItem, nItem, cCampo, cExecuta, cTrib, aPos, aDic, nTGITRef)
  
Local cAliasQry		:= ""
Local cCodProd		:= aNfItem[nItem][IT_PRODUTO]
Local cPart			:= aNfCab[NF_CODCLIFOR]
Local cLoja			:= aNfCab[NF_LOJA]
Local cTipoPart		:= Iif( aNfCab[NF_CLIFOR] == "C", "2" , "1" )
Local cOrigProd		:= SubStr( aNfItem[nItem][IT_CLASFIS] , 1 , 1 )
Local cUfOrigem		:= aNFCab[NF_UFORIGEM]
Local cUfDestino	:= aNfCab[NF_UFDEST]
Local cCfop			:= aNfItem[nItem][IT_CF]
Local cTpOper		:= aNfItem[nItem][IT_TPOPER]
Local nTrbGen		:= 0
Default cExecuta    := ""
Default cTrib		:= ""

cCampo	:= Alltrim(cCampo)

// Verifica se a flag para c�lculo dos tributos gen�ricos foi passada como ".T.". Esta flag � passada via MaFisIni e serve para indicar que
// a rotina consumidora est� preparada para gravar, visualizar e excluir os tributos gen�ricos. Esta prote��o serve para evitar que os tributos
// sejam calculados e n�o sejam gravados/visualizados devido � aus�ncia da chamada dos componentes espec�ficos criados para este fim.
If aNfCab[NF_CALCTG]

	/*
	Considero as refer�ncias IT_RECORI pois � quando alterou o recno da nota original.
	Considero o IT_QUANT pois influencia diretamente na devolu��o, seja parcial ou integral.
	Refa�o a query quando se altera a quantidade, pois preciso do valor original como base para refazer a proporcionalidade,
	caso contr�rio conseguria fazer a proporcionalidade correta somente da primeira vez.
	Verificou tamb�m se cCampo est� vazio, pois no caso da planilha financeira e no faturamento a recall � chamada sem campo espec�fico
	Verifico tamb�m se o RECORI est� preenchido e se o tipo da nota � devolu��o ou beneficiamento
	*/
	If cCampo <> "IT_TRIBGEN" .AND. aNFCab[NF_TIPONF] $ "DB" .And. !Empty(aNFItem[nItem][IT_RECORI])
		IF Alltrim(cCampo) == "IT_RECORI" .OR. Alltrim(cCampo) == "IT_QUANT" .OR. cCampo == "IT_" .OR. Empty(cCampo)
			//Chama fun��o que far� o tratamento das devolu��es dos tributos gen�ricos
			FisDevTrbGen(aNfCab, @aNfItem, nItem, aPos, aDic, cCampo, nTGITRef)
		EndIF
	Else

		/*Verifico se a Recall foi chamada na altera��o de c�digo de produto, c�digo do participante, loja do participanta, uf de origem,
		UF de destino,c�digo de TES e CFOP. Por�m estou verificando tamb�m se cCampo est� vazia, pois o processamento do faturamente chama somente 1
		vez a Recall, e n�o chama com altera��o de campos espec�fico, j� que o envio de informa��es para MATXFIS � feito via load.
		Se estas condi��es forem atendidas a query ser� feira e todas as refer�ncias dos tributos gen�ricos tamb�m ser�o refeitos.*/
		If cCampo == "IT_PRODUTO" .OR. cCampo == "NF_CODCLIFOR" .OR. cCampo == "NF_LOJA"   .OR. cCampo == "NF_UFORIGEM" .OR. ;
	       cCampo == "NF_UFDEST"  .OR. cCampo == "IT_CF"        .OR. cCampo == "IT_TES"    .OR. cCampo == "NF_DTEMISS"  .OR. ;
		   cCampo == "IT_"        .OR. cCampo == "IT_CLASFIS"   .OR. cCampo == "IT_TPOPER" .OR. Empty(cCampo)

			//Verifica primeiro se todos os campos "chaves" est�o preenchidos antes de prosseguir com a query.
			If !Empty(cCodProd)  .AND. !Empty(cPart)      .AND. !Empty(cLoja)  .AND. !Empty(cTipoPart) .AND. ;
			   !Empty(cUfOrigem) .AND. !Empty(cUfDestino) .AND. !Empty(cCfop)

				//Zero toda a estrutura dos tributos gen�ricos, j� que as regras e perfis ser�o enquadrados novamente e tudo ser� refeito.
				aNfItem[nItem][IT_TRIBGEN]	:= Nil
				aNfItem[nItem][IT_TRIBGEN]	:= {}				

				//Somente far� a query se o participante estiver contido em ao menos 1 perfil.
				If aNfCab[NF_PERF_PART]
					//Se todos os campos "chaves" est�o preenchidos, chamaremos a fun��o para realizar a query.
					cAliasQry	:= QryTribGen(cCodProd, cPart, cLoja, cTipoPart, cOrigProd, cUfOrigem, cUfDestino, cCfop, aNfCab[NF_DTEMISS], cTpOper, aPos)

					Do While !(cAliasQry)->(Eof())
						//Chama fun��o para adicionar nova estrutura do tributo gen�rico, populando todas as refer�ncias das regras cadastradas
						//As informa��es ser�o atualizadas no pr�prio aNfItem
						nTrbGen	:= AddTrbGen(@aNfItem, nItem, cAliasQry, nTGITRef,aNfCab, aPos)

						//Chama fun��o que interpretar� as regras de base de c�lculo e al�quota e valor
						//As informa��es ser�o atualizadas no pr�prio aNfItem						
						FisCalcTG(@aNFItem, nItem, nTrbGen,,aNfCab)						

						(cAliasQry)->(DbSKip())
					Enddo

					//Fecha o Alias antes de sair da fun��o
					dbSelectArea(cAliasQry)
					dbCloseArea()
				EndIF

			EndIF

		ElseIf Alltrim(cCampo) == "IT_TRIBGEN"

			//Aqui far� rec�lculo de um tributo espec�fico. Ele precisa existir no IT_TRIBGEN, caso contr�rio n�o far� nenhuma a��o.
			If !Empty(cTrib) .AND. (nTrbGen	:= aScan(aNfItem[nItem][IT_TRIBGEN],{|x| AllTrim(x[TG_IT_SIGLA]) == Alltrim(cTrib)})) > 0
				//Aqui chamo a fun��o para recalcular o tributo gen�rico espec�fico, conforme passado no cTrib, bem como a propriedade passada no cExecuta
				FisCalcTG(@aNFItem, nItem, nTrbGen, cExecuta,aNfCab)
			EndIf

		Else
			/*Aqui significa que n�o houve altera��o dos campos chaves dos perfis, nem das regras e n�o � altera��o do IT_TRIBGEN , logo a query n�o ser� refeita
			por�m todos os c�lculo dos tributos gen�ricos ser�o refeitos.*/
			For nTrbGen:= 1 to Len(aNfItem[nItem][IT_TRIBGEN])
				//Chama fun��o que interpretar� as regras de base de c�lculo, al�quota e valor. Os valores ser�o atualizados no pr�prio aNfItem
				FisCalcTG(@aNFItem, nItem, nTrbGen,,aNfCab)
			Next nTrbGen

		EndIF
	EndIf
EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} QryTribGen()
Fun��o que far� query na tabela de Tributos por Opera��o, buscando
as regras considerando os perfis.
O retorno desta fun��o ser� o alias com o resultado da query.

@param cCodProd   - C�digo do produto informado no item da nota fiscal
@param cPart      - C�digo do Participante informado na nota fiscal
@param cLoja      - Loja do Participante informado na nota fiscal
@param cTpPart    - Tipo do participante. Indica se � cliente (C) ou ent�o fornecedor (F)
@param cOriProd   - Origem do produto informado no item da nota fiscal
@param cUfOrigem  - UF de origem da nota fiscal
@param cUfDestino - UF de Destino da nota fiscal
@param cCfop      - CFOP informado no item da nota fiscal
@param dDataOper  - Data da opera��o a ser considerada no enquadramento das regras
@param cTpOper    - Tipo da opera��o do documento fiscal
@param aPos    	  - Array com cache de fieldpos

@return   cAlias  - Alias da query processada

@author Erick Gon�alves Dias
@since 26/06/2018
@version 12.1.17
/*/
//-------------------------------------------------------------------
Static Function QryTribGen(cCodProd, cPart, cLoja, cTpPart, cOriProd, cUfOrigem, cUfDestino, cCfop, dDataOper, cTpOper, aPos)

Local cSelect	:= ""
Local cFrom	    := ""
Local cWhere	:= ""
Local cAliasQry	:= ""
Local cMes		:= StrZero(Month(dDataOper),2)
Local cAno		:= StrZero(Year(dDataOper),4)
Local cTodosPart := PadR("TODOS", TamSX3("F22_CLIFOR")[1])
Local cTodosLoj := Replicate("Z", TamSx3("F22_LOJA")[1])

//---------------------------------------------------------------------------------
//IMPORTANTE - OS NOMES DOS CAMPOS DEVEM SER IGUAIS DA QUERY DA FUN��O FisLoadTG()
//---------------------------------------------------------------------------------

//Se��o dos campos do cadastro do tributo F2B, tributo e descri��o
cSelect += "F2B.F2B_REGRA TRIBUTO_SIGLA, F2B.F2B_DESC TRIBUTO_DESCRICAO, F2B.F2B_ID TRIBUTO_ID, F2B.F2B_RFIN REGRA_FIN, F2E.F2E_IDTRIB IDTRIB, "

//Verifica se o campo existe antes de adicionar na query
If aPos[FP_F2B_RND]
	cSelect += " F2B.F2B_RND TRIBUTO_RND, "
EndIf

//Se��o dos campos da regra de base de c�lculo
cSelect += "F27.F27_CODIGO BASE_COD   , F27.F27_VALORI BASE_VALORI , F27.F27_DESCON BASE_DESCON, F27.F27_FRETE  BASE_FRETE, "
cSelect += "F27.F27_SEGURO BASE_SEGURO, F27.F27_DESPE  BASE_DESPE  , F27.F27_ICMDES BASE_ICMDES, F27.F27_ICMRET BASE_ICMRET,  "
cSelect += "F27.F27_REDBAS BASE_REDBAS, F27.F27_TPRED  BASE_TPRED  , F27.F27_UM     BASE_UM    , F27.F27_ID     BASE_ID,"

//Se��o dos campos da regra de al�quota
cSelect += "F28.F28_CODIGO ALQ_CODIGO , F28.F28_VALORI ALQ_VALORI, F28.F28_TPALIQ ALQ_TPALIQ, F28.F28_ALIQ ALQ_ALIQ, "
cSelect += "F28.F28_URF    ALQ_URF    , F28.F28_UFRPER ALQ_UFRPER, F28.F28_ID     ALQ_ID,"

//Se��o com campos da Unidade Referencial Fiscal
cSelect += "F2A.F2A_VALOR URF_VALOR,"

//Campos para que na se��o de query eu tenha os campos base de c�lculo, al�quota e valor
cSelect += "0 BASE_CALCULO, 0 BASE_QTDE, 0 ALIQUOTA, 0 VALOR"

//From ser� executado na tabela F2B - Regras dos tributos x Opera��o
cFrom   += RetSQLName("F2B") + " F2B "

//Join com o cadastro de Tributo F2E
cFrom += "JOIN " + RetSQLName("F2E") + " F2E " + " ON (F2E.F2E_FILIAL = " + ValToSQL(xFilial("F2E")) + " AND F2E.F2E_TRIB = F2B.F2B_TRIB AND F2E.D_E_L_E_T_ = ' ') "

//Join com o perfil de origem e destino
cFrom += "JOIN " + RetSQLName("F21") + " F21 " + " ON (F21.F21_FILIAL = " + ValToSQL(xFilial("F21")) + " AND F21.F21_CODIGO = F2B.F2B_PEROD AND F21.F21_UFORI = " + ValToSQL(cUfOrigem) + " AND F21.F21_UFDEST = " + ValToSQL(cUfDestino) + " AND F21.D_E_L_E_T_ = ' ') "

//Join com o perfil de participante
cFrom += "JOIN " + RetSQLName("F22") + " F22 " + " ON (F22.F22_FILIAL = " + ValToSQL(xFilial("F22")) + " AND F22.F22_CODIGO = F2B.F2B_PERFPA AND F22.F22_TPPART = " + VAlToSql(cTpPart) + " AND ((F22.F22_CLIFOR = " + ValToSql(cPart) + " AND F22.F22_LOJA = " + ValToSql(cLoja) + ") OR (F22.F22_CLIFOR = " + ValToSql(cTodosPart) + " AND F22.F22_LOJA = " + ValToSql(cTodosLoj) + ")) AND F22.D_E_L_E_T_ = ' ') "

//Join com o perfil de opera��o
cFrom += "JOIN " + RetSQLName("F23") + " F23 " + " ON (F23.F23_FILIAL = " + ValToSQL(xFilial("F23")) + " AND F23.F23_CODIGO = F2B.F2B_PERFOP AND F23.F23_CFOP = " +  ValToSQL(cCfop) + " AND F23.D_E_L_E_T_ = ' ') "

If !Empty(cTpOper)
	//Join com o perfil de opera��o considerando o tipo de opera��o
	cFrom += "JOIN " + RetSQLName("F26") + " F26 " + " ON (F26.F26_FILIAL = " + ValToSQL(xFilial("F26")) + " AND F26.F26_CODIGO = F2B.F2B_PERFOP AND (F26.F26_TPOPER = " + ValToSQL(cTpOper) + " OR F26.F26_TPOPER= 'TODOS') AND F26.D_E_L_E_T_ = ' ') "
EndIF

//Join com o perfil de produto
cFrom += "JOIN " + RetSQLName("F24") + " F24 " + " ON (F24.F24_FILIAL = " + ValToSQL(xFilial("F24")) + " AND F24.F24_CODIGO = F2B.F2B_PERFPR AND (F24.F24_CDPROD = " +  ValToSQL(cCodProd) + " OR F24.F24_CDPROD = 'TODOS' ) AND F24.D_E_L_E_T_ = ' ') "

If !Empty(cOriProd)
	//Join com o perfil de origem de produto. Origem do produto somente ser� obrigat�ria se estiver informada.
	cFrom += "JOIN " + RetSQLName("F25") + " F25 " + " ON (F25.F25_FILIAL = " + ValToSQL(xFilial("F25")) + " AND F25.F25_CODIGO = F2B.F2B_PERFPR AND F25.F25_ORIGEM = " + ValToSQL(cOriProd) + " AND F25.D_E_L_E_T_ = ' ') "
EndIF

//Join com a regra de base de c�lculo. Traz sempre a regra vig�nte considerando o campo F27_ALTERA = 2
cFrom += "JOIN " + RetSQLName("F27") + " F27 " + " ON (F27.F27_FILIAL = " + ValToSQL(xFilial("F27")) + " AND F27.F27_CODIGO = F2B.F2B_RBASE AND F27.F27_ALTERA = '2' AND F27.D_E_L_E_T_ = ' ') "

//Join com a regra de al�quota. Traz sempre a regra vig�nte considerando o campo F28_ALTERA = 2
cFrom += "JOIN " + RetSQLName("F28") + " F28 " + " ON (F28.F28_FILIAL = " + ValToSQL(xFilial("F28")) + " AND F28.F28_CODIGO = F2B.F2B_RALIQ AND F28.F28_ALTERA = '2' AND F28.D_E_L_E_T_ = ' ') "

//LEFT Join com a tabela com URF. Esta tabela � LEFT pelo motivo de nem todas as al�quotas semre por URF.
cFrom += "LEFT JOIN " + RetSQLName("F2A") + " F2A " + " ON (F2A.F2A_FILIAL = " + ValToSQL(xFilial("F2A")) + " AND F2A.F2A_URF = F28.F28_URF AND F2A.F2A_ANO = " + ValToSQL(cAno) + " AND F2A.F2A_MES = " + ValToSQL(cMes) + "  AND F2A.D_E_L_E_T_ = ' ') "

//--------------------------------
//TODO Join com regra Financeira
//--------------------------------

//Se��o do Where, considerando a vig�ncia do tributo.
cWhere  += "F2B.F2B_FILIAL = " + ValToSQL( xFilial("F2B") ) + " AND "
cWhere  += ValToSql(dDataOper) + " >= F2B.F2B_VIGINI AND ( " + ValToSql(dDataOper) + " <= F2B.F2B_VIGFIM OR F2B.F2B_VIGFIM = ' ' ) AND "
cWhere  += "F2B.D_E_L_E_T_ = ' '"

//Concatenar� o % e executar� a query.
cSelect := "%" + cSelect + "%"
cFrom   := "%" + cFrom   + "%"
cWhere  := "%" + cWhere  + "%"

cAliasQry := GetNextAlias()

BeginSQL Alias cAliasQry

	SELECT
		%Exp:cSelect%
	FROM
		%Exp:cFrom%
	WHERE
		%Exp:cWhere%

EndSQL

Return cAliasQry

//-------------------------------------------------------------------
/*/{Protheus.doc} AddTrbGen()
Fun��o que tem como objetivo a cria��o da estrutur b�sicaa de refer�ncias do
tributo gen�rico, criando os arrays e populando com as informa��es
das regras de base de c�lculo, al�quota e regra financeira dos
tributos gen�ricos.
Os valores de base de c�lculo, al�quota e valor do tributo n�o ser�o
preenchidos nesta fun��o, ser�o interpretados por outra fun��o.

@param aNfItem    - Array com todas as informa��es do item
@param nItem      - N�mero do item da nota processado
@param cAliasQry  - Alias com todas as informa��es cadastrais das regras e tributos gen�ricos
@param nTGITRef   - Tamanho do array ItemRef dos tributos gen�ricos
@param aNFCab     - Array com informa��es do cabe�alho da nota fiscal
@param aPos       - Array com cache de fieldpos

@return nTrbGen  - Posi��o do novo tributo gen�rico na refer�ncia IT_TRIBGEN

@author Erick Gon�alves Dias
@since 27/06/2018
@version 12.1.17
/*/
//-------------------------------------------------------------------
Static Function AddTrbGen(aNfItem, nItem, cAliasQry, nTGITRef, aNFCab, aPos)

local nTrbGen		:= 0

//Adiciona estrutura b�sica do tributo gen�rico na refer�ncia IT_TRIBGEN
aadd(aNfItem[nItem][IT_TRIBGEN],Array(NMAX_IT_TG))

//Obtem a posi��o do tributo gen�rico adicionado
nTrbGen := Len(aNfItem[nItem][IT_TRIBGEN])

//-------------------------------------------
//Preenche as refer�ncias do tributo gen�rico
//-------------------------------------------
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_ID_REGRA]					:= (cAliasQry)->TRIBUTO_ID
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_SIGLA]					:= (cAliasQry)->TRIBUTO_SIGLA
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_DESCRICAO]				:= (cAliasQry)->TRIBUTO_DESCRICAO
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_ALIQUOTA]					:= (cAliasQry)->ALIQUOTA
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_VALOR]					:= (cAliasQry)->VALOR
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS]				:= Array(NMAXTGBAS)
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ]				:= Array(NMAXTGALQ)
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_FIN]				:= (cAliasQry)->REGRA_FIN
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_BASE]						:= Iif((cAliasQry)->BASE_QTDE > 0,(cAliasQry)->BASE_QTDE, (cAliasQry)->BASE_CALCULO)
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_RND]						:= aPos[FP_F2B_RND] .AND. ( (cAliasQry)->TRIBUTO_RND == "1" .OR. Empty((cAliasQry)->TRIBUTO_RND))
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC]					:= {Array(nTGITRef), Array(nTGITRef)}
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_IDTRIB]					:= (cAliasQry)->IDTRIB

//-------------------------------------------------------
//Preenche as refer�cias com as regras da base de c�lculo
//-------------------------------------------------------
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_ID]		:= (cAliasQry)->BASE_ID
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_COD]	:= (cAliasQry)->BASE_COD
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_VLORI]	:= (cAliasQry)->BASE_VALORI
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_DESCON]	:= (cAliasQry)->BASE_DESCON
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_FRETE]	:= (cAliasQry)->BASE_FRETE
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_SEGURO]	:= (cAliasQry)->BASE_SEGURO
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_DESP]	:= (cAliasQry)->BASE_DESPE
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_ICMSDES]:= (cAliasQry)->BASE_ICMDES
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_ICMSST]	:= (cAliasQry)->BASE_ICMRET
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_REDUCAO]:= (cAliasQry)->BASE_REDBAS
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_TPRED]	:= (cAliasQry)->BASE_TPRED
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_UM]		:= (cAliasQry)->BASE_UM

//-------------------------------------------------
//Preenche as refer�ncias com as regras de al�quota
//-------------------------------------------------
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_ID]		:= (cAliasQry)->ALQ_ID
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_COD]	:= (cAliasQry)->ALQ_CODIGO
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_VLORI]	:= (cAliasQry)->ALQ_VALORI
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_TPALIQ]	:= (cAliasQry)->ALQ_TPALIQ
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_ALIQ]	:= (cAliasQry)->ALQ_ALIQ
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_CODURF]	:= (cAliasQry)->ALQ_URF
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_PERURF]	:= (cAliasQry)->ALQ_UFRPER
aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_VALURF]	:= (cAliasQry)->URF_VALOR

//--------------------------------------------------------------------------
//Inicializo com zeros o controle do ItemDec do item dos tributos gen�ricos
//--------------------------------------------------------------------------
aFill(aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][1],0)
aFill(aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][2],0)

Return nTrbGen

//-------------------------------------------------------------------
/*/{Protheus.doc} FisCalcTG()
Fun��o respons�vel por interpretar as regras e efetuar o c�lculo dos
tributos gen�ricos conforme cadastrados.

@param aNfItem - Array com toda as informa��es do item da nota fiscal
@param nItem   - N�mero do item da nota fiscal
@param nTrbGen - Posi��o do tributo gen�rio na refer�ncia IT_TRIBGEN
@param cExecuta - Indica as op��es de base, al�quota e valor que dever�o ser calculadas.
@param aNFCab   - Array com informa��es do cabe�alho da nota fiscal

@author joao.pellegrini
@since 27/06/2018
@version 12.1.17
/*/
//-------------------------------------------------------------------
Function FisCalcTG(aNFItem, nItem, nTrbGen, cExecuta, aNFCab)

Local nBase 	:= 0
Local nAliquota := 0
Local nValor 	:= 0
Local cPrUm 	:= ""
Local cSgUm 	:= ""

// N�o reduzir a base quando o valor de origem for a quantidade.
Local lReduzBase := aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_VLORI] <> '02' .And. aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_REDUCAO] > 0

DEFAULT cExecuta := "BSE|ALQ|VLR"

//--------------------------------------------------------------------
//Adiciono nova posi��o para controle do SaveDec do tributo gen�rico
//--------------------------------------------------------------------
//Preciso verificar se o tributo j� consta no array do SaveDec, se j� existe n�o precisa adicoonar, se n�o existe ai ser� criado.		
TgSaveDec(@aNFCab, @aNfItem, nItem, nTrbGen)

//---------------------------------------
// Defini��o da base de c�lculo
//---------------------------------------
If "BSE" $ cExecuta

	Do Case

		// 01 - Valor da mercadoria
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_VLORI] == '01'

			nBase := aNFItem[nItem][IT_VALMERC]

		// 02 - Quantidade
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_VLORI] == '02'

			// Se for informada uma unidade de medida espec�fica para o c�lculo
			If !Empty(aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_UM])

				// Obtenho a primeira e a segunda unidade de medida informadas no produto
				cPrUm := Iif(!Empty(aNfItem[nItem][IT_B1UM]), aNfItem[nItem][IT_B1UM], "")
				cSgUm := Iif(!Empty(aNfItem[nItem][IT_B1SEGUM]), aNfItem[nItem][IT_B1SEGUM], "")

				// Se a primeira unidade do produto j� for a unidade cadastrada a base ser�
				// a pr�pria quantidade informada. N�o � necess�rio converter. Caso contr�rio
				// preciso efetuar a convers�o conforme o fator de convers�o informado no produto
				If cPrUm == aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_UM]
					nBase := aNFItem[nItem][IT_QUANT]
				ElseIf cSgUm == aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_UM]
					nBase := ConvUm(aNfItem[nItem][IT_PRODUTO],aNfItem[nItem][IT_QUANT],0,2)
				EndIf

			Else
				nBase := aNFItem[nItem][IT_QUANT]
			EndIf

		// 03 - Valor Cont�bil
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_VLORI] == '03'

			nBase := aNfItem[nItem][IT_LIVRO][LF_VALCONT]

		// 04 - Valor do Cr�dito Presumido - OBRIGATORIO usar o gen�rico!
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_VLORI] == '04'

			nBase := aNfItem[nItem][IT_LIVRO][LF_CRDPRES]

		// 05 - Base do ICMS
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_VLORI] == '05'

			nBase := aNfItem[nItem][IT_BASEICM]

		// 06 - Base "original" do ICMS
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_VLORI] == '06'

			nBase := aNfItem[nItem][IT_BICMORI]

		// 07 - Valor do ICMS
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_VLORI] == '07'

			nBase := aNfItem[nItem][IT_VALICM]

		// 08 - Valor do Frete
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_VLORI] == '08'

			nBase := aNfItem[nItem][IT_FRETE]

		// 09 - Valor da Duplicata
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_VLORI] == '09'

			nBase := aNfItem[nItem][IT_BASEDUP]

		// 10 - Valor total do item
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_VLORI] == '10'

			nBase := aNfItem[nItem][IT_TOTAL]			

	EndCase

	// Verifica configura��o para aplicar a redu��o de base antes das dedu��es/adi��es...
	If lReduzBase .And. aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_TPRED] == '1'
		nBase := (nBase * (1 - (aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_REDUCAO] / 100)))
	EndIf

	/*

	Regra geral das adi��es subtra��es:

	1 - Sem a��o
	2 - Subtrai
	3 - Soma

	*/

	// Desconto

	Do Case
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_DESCON] == '2'
			nBase -= (aNfItem[nItem][IT_DESCONTO] + aNfItem[nItem][IT_DESCTOT])
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_DESCON] == '3'
			nBase += (aNfItem[nItem][IT_DESCONTO] + aNfItem[nItem][IT_DESCTOT])
	EndCase

	// Frete

	Do Case
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_FRETE] == '2'
			nBase -= aNfItem[nItem][IT_FRETE]
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_FRETE] == '3'
			nBase += aNfItem[nItem][IT_FRETE]
	EndCase

	// Seguro

	Do Case
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_SEGURO] == '2'
			nBase -= aNfItem[nItem][IT_SEGURO]
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_SEGURO] == '3'
			nBase += aNfItem[nItem][IT_SEGURO]
	EndCase

	// Despesas

	Do Case
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_DESP] == '2'
			nBase -= aNfItem[nItem][IT_DESPESA]
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_DESP] == '3'
			nBase += aNfItem[nItem][IT_DESPESA]
	EndCase

	// ICMS Desonerado

	Do Case
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_ICMSDES] == '2'
			nBase -= aNfItem[nItem][IT_DEDICM]
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_ICMSDES] == '3'
			nBase += aNfItem[nItem][IT_DEDICM]
	EndCase

	// ICMS-ST

	Do Case
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_ICMSST] == '2'
			nBase -= aNfItem[nItem][IT_VALSOL]
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_ICMSST] == '3'
			nBase += aNfItem[nItem][IT_VALSOL]
	EndCase


	// Verifica configura��o para aplicar a redu��o de base ap�s as dedu��es/adi��es
	If lReduzBase .And. aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_TPRED] == '2'
		nBase := (nBase * (1 - (aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_REDUCAO] / 100)))
	EndIf

	// Atribuindo base "final" na refer�ncia de base do tributo.
	aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_BASE] := nBase

EndIf

//---------------------------------------
// Defini��o da al�quota
//---------------------------------------
If "ALQ" $ cExecuta

	Do Case

		// 01 - Al�quota do ICMS
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_VLORI] == '01' .AND. aNfItem[nItem][IT_BASEICM] > 0

			nAliquota := aNfItem[nItem][IT_ALIQICM]

		// 02 - Al�quota do Cr�dito Presumido
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_VLORI] == '02' .AND. aNfItem[nItem,IT_LIVRO,LF_CRDPRES] > 0

			nAliquota := aNFItem[nItem][IT_TS][TS_CRDPRES]

		// 03 - Al�quota do ICMS-ST
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_VLORI] == '03' .AND. aNfItem[nItem][IT_BASESOL] > 0

			nAliquota := aNFItem[nItem][IT_ALIQSOL]

		// 04 - Al�quota Informada Manualmente
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_VLORI] == '04'

			nAliquota := aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_ALIQ]

		// 05 - Unidade de Refer�ncia Fiscal
		Case aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_VLORI] == '05'

			nAliquota := (aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_VALURF] * (aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_PERURF] / 100))

	EndCase


	aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_ALIQUOTA] := nAliquota

EndIf

//---------------------------------------
// Defini��o do valor
//---------------------------------------
If "VLR" $ cExecuta

	If aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_VLORI] == '04'
		// Divido por 100 caso a al�quota informada for do tipo 1 - percentual
		If aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_TPALIQ] == '1'
			nValor := aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_BASE] * (aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_ALIQUOTA] / 100)
		Else
			nValor := aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_BASE] * aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_ALIQUOTA]
		EndIf
	ElseIf aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_VLORI] == '05'
		nValor := aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_BASE] * aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_ALIQUOTA]
	Else
		nValor := aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_BASE] * (aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_ALIQUOTA] / 100)
	EndIf

	aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_VALOR] := nValor

EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} FisLoadTG()
Fun��o respons�vel por buscar os valores dos tributos gen�ricos gravados
na CD2, para carregar estes valores e adicionar nas refer�ncias do IT_TRBGEN

@param aNfCab   - Array Com todas informa��es do aNfCab
@param aNfItem  - Array Com todas informa��es do aNfItem
@param cIdDevol - ID da tabela F2D para as notas de devolu��es
@param aNFCab   - Array com informa��es do cabe�alho da nota fiscal
@param aPos     - Array com cache dos fieldpos

@author Erick Gon�alves Dias
@since 03/07/2018
@version 12.1.17
/*/
//-------------------------------------------------------------------
Function FisLoadTG(aNfItem, nItem, cIdDevol, nTGITRef, aNFCab, aPos)

Local cSelect		:= ""
Local cFrom	    	:= ""
Local cWhere		:= ""
Local cAliasQry		:= ""
Local cIdTrbGen		:= ""
Default cIdDevol	:= ""

//---------------------------------------------------------------------------------
//IMPORTANTE - OS NOMES DOS CAMPOS DEVEM SER IGUAIS DA QUERY DA FUN��O QryTribGen()
//---------------------------------------------------------------------------------

//Para as devolu��es dever� considerar o ID do cIdDevol, para os demais cas
cIdTrbGen	:= Iif(!Empty(cIdDevol),cIdDevol,aNfItem[nItem][IT_ID_LOAD_TRBGEN])

//Zero o controle de SaveDEc dos tributos gen�ricos caso j� tenha sido criado
aNFCab[NF_SAVEDEC_TG]	:= {}

//Somente farei a query se o ID estiver preenchido.
If !Empty(cIdTrbGen)

	//Se��o dos campos da tabela F2D.
	cSelect := "F2D.F2D_TRIB  TRIBUTO_SIGLA, F2D.F2D_BASE  BASE_CALCULO, F2D.F2D_BASQTD BASE_QTDE        , F2D.F2D_ALIQ   ALIQUOTA, "
	cSelect += "F2D.F2D_VALOR VALOR        , F2D.F2D_IDCAD TRIBUTO_ID  ,F2B.F2B_DESC    TRIBUTO_DESCRICAO, F2D_VALURF URF_VALOR, "
	cSelect += "F2D.F2D_RFIN REGRA_FIN, F2E.F2E_IDTRIB IDTRIB, "

	//Verifica se o campo existe antes de adicionar na query
	If aPos[FP_F2B_RND]
		cSelect += " F2B.F2B_RND TRIBUTO_RND, "
	EndIf

	//Se��o dos campos da regra de base de c�lculo
	cSelect += "F27.F27_CODIGO BASE_COD   , F27.F27_VALORI BASE_VALORI , F27.F27_DESCON BASE_DESCON, F27.F27_FRETE  BASE_FRETE, "
	cSelect += "F27.F27_SEGURO BASE_SEGURO, F27.F27_DESPE  BASE_DESPE  , F27.F27_ICMDES BASE_ICMDES, F27.F27_ICMRET BASE_ICMRET,  "
	cSelect += "F27.F27_REDBAS BASE_REDBAS, F27.F27_TPRED  BASE_TPRED  , F27.F27_UM     BASE_UM    , F27.F27_ID     BASE_ID,"

	//Se��o dos campos da regra de al�quota
	cSelect += "F28.F28_CODIGO ALQ_CODIGO , F28.F28_VALORI ALQ_VALORI, F28.F28_TPALIQ ALQ_TPALIQ, F28.F28_ALIQ ALQ_ALIQ, "
	cSelect += "F28.F28_URF    ALQ_URF    , F28.F28_UFRPER ALQ_UFRPER, F28.F28_ID     ALQ_ID"

	//From na tabela F2D
	cFrom   += RetSQLName("F2D") + " F2D "

	//Join com a tabela de tributo F2B. Aqui est� LEFT JOIN somente por precau��o, se fosse INNER JOIN o tributo n�o seria carregado caso a F2B tivesse sido deletada indevidamente.
	//De qualquer forma o usu�rio n�o consegue deletar devido o relacionamento na X9 entre as tabelas F2D e F2B.
	cFrom += "LEFT JOIN " + RetSQLName("F2B") + " F2B " + " ON (F2B.F2B_ID = F2D.F2D_IDCAD AND F2B.D_E_L_E_T_ = ' ') "

	//Join com o cadastro de Tributo F2E
	cFrom += "LEFT JOIN " + RetSQLName("F2E") + " F2E " + " ON (F2E.F2E_FILIAL = " + ValToSQL(xFilial("F2E")) + " AND F2E.F2E_TRIB = F2B.F2B_TRIB AND F2E.D_E_L_E_T_ = ' ') "
	
	//Join com a regra de base de c�lculo utilizada no c�lculo do tributo gen�rico
	cFrom += "LEFT JOIN " + RetSQLName("F27") + " F27 " + " ON (F27.F27_ID = F2D.F2D_IDBASE AND F27.D_E_L_E_T_ = ' ') "

	//Join com a regra de al�quota utilizada no c�lculo do tributo gen�rico
	cFrom += "LEFT JOIN " + RetSQLName("F28") + " F28 " + " ON (F28.F28_ID = F2D.F2D_IDALIQ AND F28.D_E_L_E_T_ = ' ') "

	//Condi��o do where da query para trazer as informa��es do ID em quest�o.
	cWhere  += "F2D.F2D_IDREL = " + ValToSQL(cIdTrbGen) + " AND "
	cWhere  += "F2D.D_E_L_E_T_ = ' '"

	//Concatenar� o % e executar� a query.
	cSelect := "%" + cSelect + "%"
	cFrom   := "%" + cFrom   + "%"
	cWhere  := "%" + cWhere  + "%"

	cAliasQry := GetNextAlias()

	BeginSQL Alias cAliasQry

		SELECT
			%Exp:cSelect%
		FROM
			%Exp:cFrom%
		WHERE
			%Exp:cWhere%

	EndSQL

	//Processa todos os tributos gen�ricos gravados na F2D
	Do While !(cAliasQry)->(Eof())

		//Adiciona na refer�ncia IT_TRIBGEN as informa��es do tributo gen�rico considerando as informa��es da F2D.
		AddTrbGen(@aNfItem, nItem, cAliasQry,nTGITRef,aNFCab,aPos)
		(cAliasQry)->(DbSKip())
	Enddo

	//Fecha o Alias antes de sair da fun��o
	dbSelectArea(cAliasQry)
	dbCloseArea()

EndIF

Return
//-------------------------------------------------------------------
/*/{Protheus.doc} xFisGrbTrbGen()
Fun��o respons�vel por realizar a grava��o dos tributos gen�ricos na tabela F2D.
A grava��o ir� considerar as informa��es contidas na refer�ncia do aNfItem IT_TRBGEN

@param aNfItem - Array com todas as informa��es do item da nota fiscal
@param nItem - N�mero do item a ser processado por esta fun��o.
@param cAlias - Alias da tabela do item que ter� gravado o ID do tributo gen�rico.

@return cRet - Retornar o ID utilizado na grava��o dos tributos na F2D, para que os fontes
consumidores possam gravar este ID em suas respectivas tabelas de itens, como a SD1 e SD2.

@author Erick Gon�alves Dias
@since 09/07/2018
@version 12.1.17
/*/
//-------------------------------------------------------------------
Function FisGrvTrbGen(aNfItem, nItem, cAlias)

Local nTrbGen 	:= 0
Local cRet		:= ""

dbSelectArea("F2D")

//Percorre o array e gravar� F2B para todos os tributos gen�ricos calculados
For nTrbGen:= 1 to Len(aNfItem[nItem][IT_TRIBGEN])

	If aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_BASE] > 0 .And. aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_VALOR] > 0
		RecLock("F2D",.T.)

		F2D->F2D_FILIAL	:=	xFilial("F2D")
		F2D->F2D_ID		:=	FWUUID("F2D")
		F2D->F2D_IDREL	:=	aNfItem[nItem][IT_ID_TRBGEN]
		F2D->F2D_TABELA	:=	cAlias
		F2D->F2D_RFIN	:=	aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_FIN]
		F2D->F2D_TRIB  	:=	aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_SIGLA]
		F2D->F2D_ALIQ  	:=	aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_ALIQUOTA]
		F2D->F2D_VALOR 	:=	aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_VALOR]
		F2D->F2D_IDCAD 	:=	aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_ID_REGRA]
		F2D->F2D_IDBASE :=	aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_ID]
		F2D->F2D_IDALIQ :=	aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_ID]
		F2D->F2D_VALURF	:=	aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_ALQ][TG_ALQ_VALURF]

		//Tratamento para base de c�lculo em quantidade
		If aNFItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_REGRA_BAS][TG_BAS_VLORI] == '02'
			//Base de c�lculo em quantidade
			F2D->F2D_BASQTD	:= aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_BASE]
		Else
			//Base de c�lculo normal com valor
			F2D->F2D_BASE  	:=	aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_BASE]
		EndIF

		cRet	:= aNfItem[nItem][IT_ID_TRBGEN]

		F2D->(MsUnLock())
	EndIF

Next nTrbGen

Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} FisDelTrbGen()
Fun��o respons�vel por adicionar a data de exclus�o do registro na
tabela F2D. Esta tabela nunca ser� efetivamente deletada pois caso
o documento de origem seja cancelado/exclu�do perderia-se a rela��o
entre as tabelas.

@param cIdTribGen - ID para buscar as informa��es que ser�o deletadas

@author Erick Gon�alves Dias
@since 10/07/2018
@version 12.1.17
/*/
//-------------------------------------------------------------------
Function FisDelTrbGen(cIdTrbGen)

dbSelectArea("F2D")
dbSetOrder(2) //F2D_FILIAL+F2D_IDREL

If !Empty(cIdTrbGen)
	//Busca por tributos considerando o Id
	If F2D->(MsSeek(xFilial("F2D")+cIdTrbGen))
		//La�o para excluir todos os tributos gen�ricos do ID em quest�o
		While !F2D->(Eof()) .And. xFilial("F2D")+cIdTrbGen == F2D->F2D_FILIAL+F2D->F2D_IDREL
			RecLock("F2D",.F.)
			F2D->F2D_DTEXCL := dDataBase
			MsUnLock()
			F2D->(FkCommit())
			F2D->(dbSkip())
		EndDo
	EndIF
EndIF

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} FisChkTG()
Fun��o respons�vel por efetuar algumas valida��es para utiliza��o dos
tributos gen�ricos.

@param cAlias - Alias da tabela no qual ser� gravado o ID de relacionamento
com a tabela F2D.
@para cCampo - Campo no qual ser� gravado o ID de relacionamento com a
tabela F2D.

@author Erick Gon�alves Dias
@since 10/07/2018
@version 12.1.17
/*/
//-------------------------------------------------------------------
Function FisChkTG(cAlias, cCampo)

Local lRet := cPaisLoc == "BRA" .And. AliasInDic("F2D") .And. !Empty(cAlias) .And. AliasInDic(cAlias) .AND. (cAlias)->(FieldPos(cCampo)) > 0 .AND. ;
		      FindFunction("MaFisTG") .AND. FindFunction("FisRetTG") .AND. FindFunction("FisF2F") .And. FindFunction("FisTitTG") .AND. ;
			  !Empty(MaFisScan("NF_TRIBGEN",.F.))

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} FisDevTrbGen()
Fun��o respons�vel por tratar as devolu��es de venda e de compra dos tributos
gen�ricos.
Esta fun��o utilizar� o RECORI da SD1/SD2 para buscar o ID do tributo gen�rico,
far� a carga dos valores e proporcionalizar� considerando a quantidade da nota
original com a nota de devolu��o.

@param aNfCab   - Array com as informa��es cabe�alho da nota fiscal
@param aNfItem  - Array com toda as informa��es do item da nota fiscal
@param nItem    - N�mero do item da nota fiscal
@param aPos    - Array com cache dos fieldpos
@param aDic    - Array com cache de aliasindic
@param cCampo  - String com o campo alterado na pilha da recall
@param nTGITRef - Tamanho do array ItemRef dos tributos gen�ricos

@author Erick Gon�alves Dias
@since 11/07/2018
@version 12.1.17
/*/
//-------------------------------------------------------------------
Function FisDevTrbGen(aNfCab, aNfItem, nItem, aPos, aDic, cCampo,nTGITRef)

Local cIdTrbGen := ""
Local nTrbGen	:= 0
Local nQtdeOri	:= 0

//Verifica se � devolu��o de compra ou venda, posiciona no item original e busca o ID do tribGEN e quantidade
If aNFCab[NF_CLIFOR] == "C" .AND. aPos[FP_D2_IDTRIB]
	//Devolu��o de compra
	dbSelectArea("SD2")
	MsGoto(aNFItem[nItem][IT_RECORI])
	cIdTrbGen	:= SD2->D2_IDTRIB
	nQtdeOri	:= SD2->D2_QUANT
ElseIF aPos[FP_D1_IDTRIB]
	//Devolu��o de venda
	dbSelectArea("SD1")
	MsGoto(aNFItem[nItem][IT_RECORI])
	cIdTrbGen	:= SD1->D1_IDTRIB
	nQtdeOri	:= SD1->D1_QUANT
EndIF

//Zero toda a estrutura dos tributos gen�ricos, j� que os valores ser�o todos carregados da nota fiscal de origem
aNfItem[nItem][IT_TRIBGEN]	:= Nil
aNfItem[nItem][IT_TRIBGEN]	:= {}

//Verifico se o ID est� preenchido e se a tabela existe antes de fazer carga dos tributos gen�ricos da nota original
IF !Empty(cIdTrbGen) .AND. aDic[AI_F2D]
	//Somente farei a query se a quantidade devolvida for maior que zero.
	If aNfItem[nItem][IT_QUANT] > 0
		//Fun��o que faz query na F2D parra buscar os valores dos tributos gen�ricos da nota original
		FisLoadTG(@aNfItem, nItem, cIdTrbGen, nTGITRef, aNFCab, aPos)
	EndIF
EndIF

//Percorre o os tributos gen�ricos carregados para aplicar a proporcionalidade caso seja devolu��o parcial.
//Para devolu��o integral n�o h� necessidade de fazer proporcionalidade
If nQtdeOri <> aNfItem[nItem][IT_QUANT]
	For nTrbGen:= 1 to Len(aNfItem[nItem][IT_TRIBGEN])

		//--------------------------------------------------------------------
		//Adiciono nova posi��o para controle do SaveDec do tributo gen�rico
		//--------------------------------------------------------------------
		//Preciso verificar se o tributo j� consta no array do SaveDec, se j� existe n�o precisa adicoonar, se n�o existe ai ser� criado.		
		TgSaveDec(@aNFCab, @aNfItem, nItem, nTrbGen)

		aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_VALOR] := (aNfItem[nItem][IT_QUANT] * aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_VALOR]) / nQtdeOri
		aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_BASE]  := (aNfItem[nItem][IT_QUANT] * aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_BASE])  / nQtdeOri
	Next nTrbGen
EndIF

Return

/*/{Protheus.doc} FisRetTG()
@description Fun��o respons�vel por retornar os tributos gen�ricos pass�veis de reten��o

@param dDataOper   - Data da opera��o, para enquadrar somente as regras vigentes
@return   aRet     - Array com os tributos que possuem regras de reten��es vigentes

@author erick.dias
/*/
Function FisRetTG(dDataOper)

Local aRet	:= {}
Local cSelect	:= ""
Local cFrom	    := ""
Local cWhere	:= ""
Local cAliasQry	:= ""
Local lFinFkkVIg	:= FindFunction("FinFKKVig")

IF lFinFkkVIg
	//Se��o dos campos do cadastro do tributo F2B, tributo e descri��o
	cSelect += "F2B.F2B_REGRA TRIBUTO_SIGLA, F2B.F2B_DESC TRIBUTO_DESCRICAO, F2B.F2B_RFIN  "

	//From ser� executado na tabela F2B - Regras dos tributos x Opera��o
	cFrom   += RetSQLName("F2B") + " F2B "

	//Se��o do Where, considerando a vig�ncia do tributo.
	cWhere  += "F2B.F2B_FILIAL = " + ValToSQL( xFilial("F2B") ) + " AND "
	cWhere  += ValToSql(dDataOper) + " >= F2B.F2B_VIGINI AND ( " + ValToSql(dDataOper) + " <= F2B.F2B_VIGFIM OR F2B.F2B_VIGFIM = ' ' ) AND F2B.F2B_RFIN <> ' ' AND "
	cWhere  += "F2B.D_E_L_E_T_ = ' '"

	//Concatenar� o % e executar� a query.
	cSelect := "%" + cSelect + "%"
	cFrom   := "%" + cFrom   + "%"
	cWhere  := "%" + cWhere  + "%"

	cAliasQry := GetNextAlias()

	BeginSQL Alias cAliasQry

		SELECT
			%Exp:cSelect%
		FROM
			%Exp:cFrom%
		WHERE
			%Exp:cWhere%

	EndSQL

	//Adiciona no array sigla e descri��o dos tributos retornados
	Do While !(cAliasQry)->(Eof())

		//A fun��o posiciona na FKK corrente, considerando o c�digo da FKK e dataOper, retornando o RECNO.
		//Somente avaliar� a FKK
		If FinFKKVig((cAliasQry)->F2B_RFIN, dDataOper) > 0 .AND. !Empty(FKK->FKK_CODFKO)
			aAdd(aRet,{(cAliasQry)->TRIBUTO_SIGLA,(cAliasQry)->TRIBUTO_DESCRICAO} )
		EndIF

		(cAliasQry)->(DbSKip())
	Enddo

	//Fecha o Alias antes de sair da fun��o
	dbSelectArea(cAliasQry)
	dbCloseArea()

EndIF

Return aRet

//-------------------------------------------------------------------
/*/{Protheus.doc} FisGetURF

Fun��o que retornar� o valor atual da URF, considerando o per�odo
e c�digo da URF.

@param dDate     - Data da opera��o, para poder enquadrar a URF vig�nte
@param cCodURF   - C�digo da URF configurada na regra de al�quota
@param nPercURF  - Percentual da URF configurada na regra de al�quota

@return nUrfAtual  - Valor da URF conforme os par�metros de entradas

@author Erick Dias
@since 06/11/2018
@version 12.1.17
/*/
//-------------------------------------------------------------------
Function FisGetURF(dDate, cCodURF, nPercURF)

Local nUrfAtual		:= 0
Default nPercURF	:= 100

IF AliasIndic("F2A")
	F2A->(dbSetOrder(1))
	If !Empty(dDate) .AND. !Empty(cCodURF) .AND. F2A->(MsSeek(xFilial("F2A") + padr(cCodURF,6) + Str(Year(dDate),4) + Strzero(Month(dDate),2)))

		nUrfAtual	:= F2A->F2A_VALOR
		If nPercURF > 0
			nUrfAtual	:= nUrfAtual * (nPercURF / 100)
		EndIF

	EndIF
EndIf

Return nUrfAtual

/*/{Protheus.doc} FisHdrTG()
@description Fun��o respons�vel por montar o aHeader do folder
dos tributos gen�ricos.
@author erick.dias
/*/
Function FisHdrTG()

Local aHdrTrbGen    := {}

aAdd(aHdrTrbGen,;
{"Item",;
"ITEM",;
"@!",;
4,;
0,;
"",;
"",;
"C",;
"",;
"R",;
"",;
"",;
""})

aAdd(aHdrTrbGen,;
{"Sigla",;
"F2D_TRIB",;
PesqPict("F2D","F2D_TRIB"),;
TamSX3("F2D_TRIB")[1] + 5 ,;
TamSX3("F2D_TRIB")[2],;
"",;
"",;
"C",;
"",;
"R",;
"",;
"",;
""})

aAdd(aHdrTrbGen,;
{"Descri��o",;
"F2D_DESC",;
"@!",;
TamSx3("F2B_DESC")[1],;
TamSX3("F2B_DESC")[2],;
"",;
"",;
"C",;
"",;
"R",;
"",;
"",;
""})

aAdd(aHdrTrbGen,;
{"Base de C�lculo",;
"F2D_BASE",;
PesqPict("F2D","F2D_BASE"),;
TamSx3("F2D_BASE")[1],;
TamSX3("F2D_BASE")[2],;
"!Empty(GdFieldGet('F2D_TRIB',,.T.)) .AND. Positivo() .And. MaFisTGRef('IT_TRIBGEN',GdFieldGet('F2D_BASE',,.T.),{GdFieldGet('F2D_TRIB',,.T.),'TG_IT_BASE'}, Val(GdFieldGet('ITEM',,.T.)))",;
"",;
"N",;
"",;
"R",;
"",;
"",;
""})

aAdd(aHdrTrbGen,;
{"Al�quota",;
"F2D_ALIQ",;
PesqPict("F2D","F2D_ALIQ"),;
TamSx3("F2D_ALIQ")[1],;
TamSX3("F2D_ALIQ")[2],;
"!Empty(GdFieldGet('F2D_TRIB',,.T.)) .AND. Positivo() .And. MaFisTGRef('IT_TRIBGEN',GdFieldGet('F2D_ALIQ',,.T.),{GdFieldGet('F2D_TRIB',,.T.),'TG_IT_ALIQUOTA'}, Val(GdFieldGet('ITEM',,.T.)))",;
"",;
"N",;
"",;
"R",;
"",;
"",;
""})

aAdd(aHdrTrbGen,;
{"Valor",;
"F2D_VALOR",;
PesqPict("F2D","F2D_VALOR"),;
TamSx3("F2D_VALOR")[1],;
TamSX3("F2D_VALOR")[2],;
"!Empty(GdFieldGet('F2D_TRIB',,.T.))  .AND. Positivo() .And. MaFisTGRef('IT_TRIBGEN',GdFieldGet('F2D_VALOR',,.T.),{GdFieldGet('F2D_TRIB',,.T.),'TG_IT_VALOR'},Val(GdFieldGet('ITEM',,.T.)))",;
"",;
"N",;
"",;
"R",;
"",;
"",;
""})

Return aHdrTrbGen

//-------------------------------------------------------------------
/*/{Protheus.doc} FisF2F

Funcao respons�vel por componentizar a grava��o da tabela F2F.

@param cOper      - Opera��o, indica se � inclus�o ou exclus�o do t�tulo
@param cIdNF      - ID de relacionamento com o documento fiscal, este ID � fundamental para vincular o t�tulo com a nota
@param cTabela    - Esta par�metro identifica a tabela de origem da movimenta��o que gerou este t�tulo
@param aTGCalcRec - Lista dos tributos gen�ricos calculados que dever�o ter t�tulos gerados.

@author joao.pellegrini
@since 08/10/2018
@version 11.80
/*/
//-------------------------------------------------------------------
Function FisF2F(cOper, cIdNF, cTabela, aTGCalcRec)

Local nX := 0
Local cChvF2F := ""

DEFAULT aTGCalcRec := {}

If AliasInDic("F2F") .And. !Empty(cIdNF)

	dbSelectArea("F2F")
	F2F->(dbSetOrder(1))

	// Inclusao
	If cOper == "I"

		For nX := 1 to Len(aTGCalcRec)
			// Verifico se tem ID FK7 gerado pelo financeiro, o que
			// significa que o t�tulo em quest�o foi gerado.
			If !Empty(aTGCalcRec[nX, 4])
				RecLock("F2F", .T.)
				F2F->F2F_FILIAL := xFilial("F2F")
				F2F->F2F_IDNF := cIdNF
				F2F->F2F_TABELA := cTabela
				F2F->F2F_IDFK7 := aTGCalcRec[nX, 4]
				F2F->F2F_IDF2B := aTGCalcRec[nX, 5]
				F2F->(MsUnlock())
			EndIf
		Next nX

	// Exclusao
	ElseIf cOper == "E"

		cChvF2F := xFilial("F2F") + cIdNF + cTabela

		If F2F->(MsSeek(cChvF2F))
			While !F2F->(EoF()) .And. F2F->(F2F_FILIAL + F2F_IDNF + F2F_TABELA) == cChvF2F
				RecLock("F2F",.F.)
				F2F->(dbDelete())
				MsUnLock()
				F2F->(FkCommit())
				F2F->(dbSkip())
			EndDo
		EndIf

	EndIf

	F2F->(dbCloseArea())

EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} FisTitTG

Fun��o respons�vel por retornar o n�mero do t�tulo de tributo gen�rico
a ser gerado.

@author joao.pellegrini
@since 08/10/2018
@version 11.80
/*/
//-------------------------------------------------------------------
Function FisTitTG()

Local cNumero := ""

If SX5->(dbSeek(xFilial("SX5")+"53"+"TG"))
	cNumero := "TG" + Soma1(Substr(X5Descri(),3,7),7)
	RecLock("SX5",.F.)
	SX5->X5_DESCRI  := cNumero
	SX5->X5_DESCSPA := cNumero
	SX5->X5_DESCENG := cNumero
	SX5->(MsUnlock())
EndIf

Return cNumero

//-------------------------------------------------------------------
/*/{Protheus.doc} FISFK7E1E2

Fun��o respons�vel por converter uma chave FK7 para uma chave de SE1/SE2.

@param cChaveFK7 - Chave Fk7 do t�tulo
@param cTabela   - Indica se dever� considerar SE1 ou SE2 no momento de converter a chave Fk7

@return cChvSE   - Chave do t�tulo j� convertida

@author joao.pellegrini
@since 10/10/2018
@version 11.80
/*/
//-------------------------------------------------------------------
Function FISFK7E1E2(cChaveFK7, cTabela)

Local aChvSE := {}
Local cChvSE := ""
Local aTamSE2 := {TamSX3("E2_FILIAL")[1], TamSX3("E2_PREFIXO")[1], TamSX3("E2_NUM")[1], TamSX3("E2_PARCELA")[1], TamSX3("E2_TIPO")[1], TamSX3("E2_FORNECE")[1], TamSX3("E2_LOJA")[1]}
Local aTamSE1 := {TamSX3("E1_FILIAL")[1], TamSX3("E1_PREFIXO")[1], TamSX3("E1_NUM")[1], TamSX3("E1_PARCELA")[1], TamSX3("E1_TIPO")[1], TamSX3("E1_CLIENTE")[1], TamSX3("E1_LOJA")[1]}

aChvSE := StrToKarr(cChaveFK7, "|")

If Len(aChvSE) >= 7

	cChvSE := (PadR(aChvSE[1], IIf(cTabela == "SE1", aTamSE1[1], aTamSE2[1])) +;
			   PadR(aChvSE[2], IIf(cTabela == "SE1", aTamSE1[2], aTamSE2[2])) +;
			   PadR(aChvSE[3], IIf(cTabela == "SE1", aTamSE1[3], aTamSE2[3])) +;
			   PadR(aChvSE[4], IIf(cTabela == "SE1", aTamSE1[4], aTamSE2[4])) +;
			   PadR(aChvSE[5], IIf(cTabela == "SE1", aTamSE1[5], aTamSE2[5])) +;
			   PadR(aChvSE[6], IIf(cTabela == "SE1", aTamSE1[6], aTamSE2[6])) +;
			   PadR(aChvSE[7], IIf(cTabela == "SE1", aTamSE1[7], aTamSE2[7])))

EndIf

Return cChvSE

//-------------------------------------------------------------------
/*/{Protheus.doc} FisDelTit

Fun��o respons�vel por retornar o n�mero do t�tulo de tributo gen�rico
a ser gerado.

@param cIdNF    - ID da nota fiscal que est� sendo exclu�da
@param cTabela  - Tabela de origem da movimenta��o que gerou o t�tulo
@param cOrigem  - Rotina que gerou o t�tulo
@param nOpcao   - Op��o para identificar tabela SE1 ou SE2
@param cNumTit  - N�mero do t�tulo a ser processado nesta fun��o

@return lRet  - Indica se o t�tulo pode ou n�o ser excluido. 

@author joao.pellegrini
@since 08/10/2018
@version 11.80
/*/
//-------------------------------------------------------------------
Function FisDelTit(cIdNF, cTabela, cOrigem, nOpcao, cNumTit)

Local lRet := .T.
Local cChvF2F := ""
Local cChvSE := ""
Local cMensagem	:= ""
Local aRecnoExcl := {}
Local nX := 0
Local aArea := GetArea()

Default cNumTit	:= ""

dbSelectarea("F2F")
F2F->(dbSetOrder(1))

dbSelectarea("FK7")
FK7->(dbSetOrder(1))

dbSelectarea("SE2")
SE2->(dbSetOrder(1))

dbSelectarea("SE1")
SE1->(dbSetOrder(1))

cChvF2F := xFilial("F2F") + cIdNF + cTabela

If !Empty(cIdNF) .And. F2F->(MsSeek(cChvF2F))

	// La�o na F2F para posicionar a FK7 com o campo F2F_IDFK7
	While !F2F->(Eof()) .And. F2F->(F2F_FILIAL + F2F_IDNF + F2F_TABELA) == cChvF2F

		cChvSE := ""
		cAlsSE := ""

		// Se encontrou na FK7 vou usar os campos FK7_ALIAS e FK7_CHAVE para chegar no t�tulo
		// gerado na SE1 ou SE2 conforme o caso.
		If FK7->(MsSeek(xFilial("FK7") + F2F->F2F_IDFK7))

			cAlsSE := FK7->FK7_ALIAS
			// Converte o conte�do do campo FK7_CHAVE para poder localizar a SE1/SE2.
			cChvSE := FISFK7E1E2(FK7->FK7_CHAVE, cAlsSE)

			// Verifica se h� algum t�tulo vinculado que n�o possa ser excluido pois sofreu algum tipo de baixa ou movimenta��o no financeiro.
			// Se houver paro o la�o e j� retorno .F. pois o documento em quest�o n�o pode ser exclu�do.
			If (cAlsSE)->(MsSeek(cChvSE))
				If nOpcao == 1
					If cAlsSE == "SE1"
						If !(lRet := FaCanDelCR("SE1", cOrigem, .F.))
							cNumTit := SE1->E1_NUM + "/" + SE1->E1_PREFIXO
						EndIF
					ElseIf cAlsSE == "SE2"
						IF !(lRet := FaCanDelCP("SE2", cOrigem, .F.))
							cNumTit := SE2->E2_NUM + "/" + SE2->E2_PREFIXO
						EndIF
					EndIf

					If !lRet
						cMensagem := "N�o Ser� poss�vel excluir o documento. Verifique o t�tulo " + cNumTit //
						Help(" ",1,"NAOEXCNF","NAOEXCNF",cMensagem,1,0,,,,,,{"Verifique a exist�ncia de border�s, baixas totais, parciais ou outras movimenta��es financeiras envolvendo este t�tulo."}) //
						Exit
					EndIf
				Else
					aAdd(aRecnoExcl, {cAlsSE, (cAlsSE)->(RecNo())})
				EndIf
			EndIf

		EndIf

		F2F->(dbSkip())

	EndDo

	// Exclus�o dos t�tulos...
	If lRet .And. nOpcao == 2
		For nX := 1 to Len(aRecnoExcl)
			If aRecnoExcl[nX, 1] == "SE1
				SE1->(dbGoTo(aRecnoExcl[nX, 2]))
				If FindFunction("FinGrvEx")
					FinGrvEx("R") // Gravar o hist�rico.
				EndIf
				RecLock("SE1",.F.)
				SE1->(dbDelete())
				FaAvalSE1(2)
				FaAvalSE1(3)
				MsUnLock()
			ElseIf aRecnoExcl[nX, 1] == "SE2
				SE2->(dbGoTo(aRecnoExcl[nX, 2]))
				If FindFunction("FinGrvEx")
					FinGrvEx("P") // Gravar o hist�rico.
				EndIf
				RecLock("SE2",.F.)
				SE2->(dbDelete())
				FaAvalSE2(2)
				FaAvalSE2(3)
				MsUnLock()
			EndIf
		Next nX
	EndIf

EndIf

RestArea(aArea)

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} FisRetGen

Fun��o que percorre-r� todos tributos gen�ricos verificando
se ele � pass�vel de reten��o

@param aTGCalc - Obt�m tributos gen�ricos calculados pelo motor Fiscal
@param aTGRet - Obt�m tributos gen�ricos pass�veis de reten��o
@param lFinFkk - Vari�vel que indica que a tabela Fkk do financeiro poder� ser utilizada
@param aTGCalcRet - Array para tributos pass�veis de reten��o
@param aTGCalcRec - Array para tributos de recolhimento
@param dEmissao - Data de emissao do Documento Fiscal

@author Renato Rezende
@since 28/11/2019
@version 12.1.27
/*/
//-------------------------------------------------------------------
Function FisRetGen(aTGCalc,aTGRet,lFinFkk,aTGCalcRet,aTGCalcRec,dEmissao)

Local cNumTitTG	:= ""
Local cHistRec	:= ""
Local nContTg	:= 0

//Obt�m todos os tributos gen�ricos calculados pelo motor Fiscal
aTGCalc := MaFisRet(,"NF_TRIBGEN")

//Obt�m todos os tributos gen�ricos pass�veis de reten��o
aTGRet	:= xFisRetTG(dEmissao)

For nContTg := 1 to Len(aTGCalc)

	//procuro pelo tributo gen�rico calculado na lista dos tributos pass�veis de reten��o
	nPosTgRet	:=  AScan(aTGRet, { |x| Alltrim(x[1]) == Alltrim(aTGCalc[nContTg][1])})

	// Se o tributo consta na lista dos pass�veis de reten��o, adiciona no aTGCalcRet.
	// Caso contr�rio, trata-se de um recolhimento e os valores ser�o adicionados no aTGCalcRec.
	If nPosTgRet > 0
		If lFinFkk
			//Se o tributo est� previsto a ter reten��o, ent�o ser� adicionado no array aTGCalcRet para ser rateado entre as parcelas.
			aAdd(aTGCalcRet,{aTGCalc[nContTg][1],; //Sigla do Tributo
							aTGCalc[nContTg][2],;//Base de C�lculo Tributo
							aTGCalc[nContTg][3],;//Valor do Tributo
							aTGCalc[nContTg][4],;//C�digo da Regra FKK
							FinParcFKK(aTGCalc[nContTg][4]),;//Indica se retem integralmente na primeira parcela
							aTGCalc[nContTg][3],;//Saldo restante do tributo, � iniciado com o pr�prio valor do tributo
							aTGCalc[nContTg][2],;//Saldo restante da base de c�lculo, que � iniciado com o pr�prio valor do tributo
							aTGCalc[nContTg][5],;//ID da regra Fiscal da tabela F2B
							aTGCalc[nContTg][6],;//C�digo da URF
							aTGCalc[nContTg][7]})//Percentual aplic�vel ao valor da URF
		EndIf
	ElseIf !Empty(aTGCalc[nContTg][4])
		// Se o tributo n�o � uma reten��o, ou seja, � um recolhimento, adiciono no array aTGCalcRec para que os t�tulos
		// sejam gerados posteriormente.
		cNumTitTG := xFisTitTG()
		//TODO na onda 2 retirar a refer�ncia para F2_DOC e F2_SERIE, de forma que receba por par�metro estas informa��es
		cHistRec := AllTrim(aTGCalc[nContTg][1]) + " - NF: " + AllTrim(SF2->F2_DOC) + " / " + AllTrim(SF2->F2_SERIE)
		aAdd(aTGCalcRec, {aTGCalc[nContTg][4],; // C�digo da Regra FKK
						aTGCalc[nContTg][3],; // Valor do tributo
						cNumTitTG,; // N�mero do t�tulo a ser gerado
						'',; // ID FK7 do t�tulo gerado -> S� usar como retorno.
						aTGCalc[nContTg][5],;//ID da regra Fiscal da tabela F2B
						cHistRec}) // Hist�rico para gravar no t�tulo
	EndIf

Next nContTg

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} ChkTribLeg

Fun��o que verifica se algum tributo gen�rico com ID de tributo legado foi
calculado, e ter� como retorno booleano, indicando se existe ou n�o
tributo gen�rico calculado com ID de tribuo legado

@param aNFItem 		- Array com informa��es do aNfItem
@param nItem 		- N�mero do item a ser verificado
@param cIdtribLeg 	- ID do tributo que deseja verificar

@author Erick Dias
@since 03/12/2019
@version 12.1.27
/*/
//-------------------------------------------------------------------
Function ChkTribLeg(aNFItem, nItem, cIdtribLeg)
Local nPosTrib	:= aScan(aNFItem[nItem][IT_TRIBGEN],{|x| Alltrim(x[TG_IT_IDTRIB])�== Alltrim(cIdtribLeg)})
Return nPosTrib > 0 .AND. aNFItem[nItem][IT_TRIBGEN][nPosTrib][TG_IT_VALOR]  > 0

//-------------------------------------------------------------------
/*/{Protheus.doc} ListTribLeg

Fun��o que retorna lista dos tributos legados que est�o
previstos/contemplados nos tributos gen�ricos

@author Erick Dias
@since 04/12/2019
@version 12.1.27
/*/
//-------------------------------------------------------------------
Function ListTrbLeg()

Local aTrib			:= {{TRIB_ID_AFRMM      , .F.},;
						{TRIB_ID_FABOV      , .F.},;
						{TRIB_ID_FACS       , .F.},;
						{TRIB_ID_FAMAD      , .F.},;
						{TRIB_ID_FASEMT     , .F.},;
						{TRIB_ID_FETHAB     , .F.},;
						{TRIB_ID_FUNDERSUL  , .F.},;
						{TRIB_ID_FUNDESA    , .F.},;
						{TRIB_ID_IMAMT      , .F.},;
						{TRIB_ID_SEST       , .F.},;
						{TRIB_ID_TPDP       , .F.}}
					
Return aTrib

//-------------------------------------------------------------------
/*/{Protheus.doc} ListTLegTG

Fun��o que retorna lista dos tributos legados que tamb�m
foram calculados na lista dos tributos gen�ricos

@param aNFItem 		- Array com informa��es do aNfItem
@param nItem 		- N�mero do item a ser verificado

@author Erick Dias
@since 04/12/2019
@version 12.1.27
/*/
//-------------------------------------------------------------------
Function ListTLegTG(aNFItem, nItem)

Local nX			:= 0
Local aTrib			:= ListTrbLeg() //Obtem lista dos tributos legados que j� est� contemplados no configurador

//Percorre os tributos e atualiza as posi��es dos tributos gen�ricos que tem ID de tributos legado
For nX:= 1 to Len(aTrib)
	aTrib[nX][2]	:= ChkTribLeg(aNFItem, nItem, aTrib[nX][1])
Next nX

Return aTrib

//-------------------------------------------------------------------
/*/{Protheus.doc} ChkCalcTLeg

Fun��o que retorna lista dos tributos legados que precisam ser recalculados
ap�s enquadramento/c�lculo dos tributos gen�ricos.

Esta fun��o receber� array com lista de tributos legados que tinha tributo gen�rico
calculado antes do c�lculo do tributo gen�rico, e tamb�m uma lsita de de tributos 
legados que tinha tributo gen�rico calculado depois do c�lculo do tributo gen�rico.

@param aTrbAntes 	- Lista dos tributos legado que tinha tributo gen�rico calculado antes do c�lculo do TG
@param aTrbDepois	- Lista dos tributos legado que tinha tributo gen�rico calculado depois do c�lculo do TG

@author Erick Dias
@since 04/12/2019
@version 12.1.27
/*/
//-------------------------------------------------------------------
Function ChkCalcTLeg(aTrbAntes, aTrbDepois)

Local nX			:= 0
Local aTrib			:= ListTrbLeg()

//Verifico primeiro se os arrays est�o com tamanhos corretos, todos precisam ter a mesma dimens�o e quantidade
If Len(aTrib) == Len(aTrbAntes) .AND. Len(aTrib) == Len(aTrbDepois) 

	//Uma vez garantido que os arrays possuem o mesmo tamanho percorro o aTrib
	For nX:= 1 to Len(aTrib)
		
		//Verifico se o tributo legado foi calculado em algum tribugo gen�rico antes ou se o tributo legado foi calculado agora em algum tribugo gen�rico
		//Em abos os casos indico que o tributo legado precisa ser recalculado, seja para ser zerado e evitando a duplicidade, ou seja devido o motivo
		//de desemquadrar algum tributo gen�rico e refazer o tributo legado
		IF aTrbAntes[nX][2] .OR. aTrbDepois[nX][2]
			aTrib[nX][2]	:= .T.
		EndIF

	Next nX

EndIF

Return aTrib

//-------------------------------------------------------------------
/*/{Protheus.doc} FisTgArred

Fun��o que far� tratamento do arredondamento dos valores dos tributos gen�ricos.
Esta fun��o foi constru�da com base na fun��o MaItArred, seguindo a mesma linha de
racioc�nio, por�m escalando para N tributos gen�ricos.
Esta fun��o ser� chamada no final da MaItArred, ela foi criada para separar os fontes
do configurador e n�o onerar o tamanho da MATXFIS.

@param aNFCab 	- Array com informa��es do cabe�alho da nota
@param aNfItem	- Array com informa��es do item da nota
@param aSX6	    - Array com informa��es dos par�metros
@param aTGITRef	- Arrays com as refer�ncias dos tributos gen�ricos 
@param aRefs	- Array com os campos espec�ficos que foram solicitados para serem arredondados na chamada da MaItArred
@param nDec	    - N�mero da precis�o de decimal, no caso do Brasil � com dias casas decimais
@param nx	    - N�mero do item posicionado
@param lSobra	- INdica se o sistema est� configurado para controlar a Sobra(MV_SOBRA)

@author Erick Dias
@since 04/12/2019
@version 12.1.27
/*/
//-------------------------------------------------------------------
Function FisTgArred(aNFCab, aNfItem, aSX6, aTGITRef, aRefs, nDec, nx, lSobra)

Local nZ			:= 0
Local nY			:= 0
Local nTrbGen		:= 0
Local nPosTribTg 	:= 0
Local nCampoTG		:= 0
Local nUmCentavo 	:= 0
Local nMeioCentavo 	:= 0
Local nPosTgDel		:= 0
Local nValor		:= 0		
Local nRndPrec		:= 0
Local nDifItem		:= 0
Local nDifItDel		:= 0

//Vari�veis abaixo para facilitar a leitura do c�digo
nUmCentavo		:= (1/10**nDec) //Corresponde a 1 centavo
nMeioCentavo	:= (50/(10**(nDec + 2))) //Corresponde a meio centavo
nRndPrec  		:= IIf( aSX6[MV_RNDPREC] < 3 , 10 , aSX6[MV_RNDPREC] ) // Precisao para o arredondamento

//Percorre lista dos tributos enquadrados e calculados
For nTrbGen:= 1 to Len(aNfItem[nx][IT_TRIBGEN])

	//Neste la�o percorro os campos do tributo gen�rico(Base, Al�quota, Valor) que est�o com flag para tratar arredondamento e sobra
	For nY:= 1 to Len(aTGITRef)

		If aRefs == Nil .Or. aScan( aRefs, aTGITRef[nY][1] ) <> 0

			If lSobra
				//La�o nos itens procurando itens deletados
				For nZ := 1 To Len(aNfItem)
					
					//Verifica se o item est� deletado
					If aNfItem[nZ][IT_DELETED]
	
						//Verifica se no item deletado existe este tributo calculado
						If (nPosTgDel	:= aScan(aNfItem[nZ][IT_TRIBGEN], {|x| Alltrim(x[2])�== Alltrim(aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_SIGLA])})) > 0

							If aNfItem[nZ][IT_TRIBGEN][nPosTgDel][TG_IT_ITEMDEC][1][nY] > 0
								nDifItDel += aNfItem[nZ][IT_TRIBGEN][nPosTgDel][TG_IT_ITEMDEC][1][nY]
								nDifItDel -= nUmCentavo
							Else								
								nDifItDel += aNfItem[nZ][IT_TRIBGEN][nPosTgDel][TG_IT_ITEMDEC][2][nY]
							EndIf

						EndIF						

					EndIf
				Next nZ
			EndIF

			//Verifica se este campo deve fazer tratamento de arredondamento e sobra
			If aTGITRef[nY][4]

				//Zerando variave que controla sobra ap�s a terceira casa decimal
				nDifItem	:= 0 
				
				//Obtem a posi��o do campo que ser� processado
				nCampoTG	:= aTGITRef[nY][2]
				
				//Obtem o valor bruto calculado, com todas as decimais e sobras
				nValor := aNfItem[nX][IT_TRIBGEN][nTrbGen][nCampoTG]

				//Verifica se existe valor do tributo para ser processado 
				If nValor <> 0
					
					While Int(nValor) <> Int(NoRound(NoRound(nValor,nRndPrec),nDec,nDifItem,10)) .And. nRndPrec > 2
						nRndPrec -= 1
					Enddo

					//Trunca o valor e guarda a diferen�a a partir da segunda casa decimal na vari�vel nDifItem
					aNfItem[nX][IT_TRIBGEN][nTrbGen][nCampoTG]  := NoRound(NoRound(nValor,nRndPrec),nDec,@nDifItem,10)
					
					//Verifica se existe valor a partir da terceira casa decimal, ou seja, se existe algum valor de sobra					
					If nDifItem <> 0 .AND. ;
					(nPosTribTg	:= aScan(aNFCab[NF_SAVEDEC_TG], {|x| Alltrim(x[1])�== Alltrim(aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_SIGLA])})) > 0 //Aqui verifico e busco posi��o do tributo no SaveDec que est� no aNfCab
					
						//Acumulo no SaveDec o valor do ItemDec [1]
						aNFCab[NF_SAVEDEC_TG][nPosTribTg][2][nCampoTG] += aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][1][nY]

						//Agora posso zerar o ItemDec [1], pois j� teve seu valor acumulado no SaveDec
						aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][1][nY]	:= 0
												
						//-----------------------------------------------------------
						//Verifica se o tributo n�o est� configurado para arredondar
						//-----------------------------------------------------------
						If !aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_RND]
							//Aqui o tributo n�o est� configurado para arredondars							
							
							aNFCab[NF_SAVEDEC_TG][nPosTribTg][2][nCampoTG] 			-= aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][2][nY]
							aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][2][nY] 	:= nDifItem
							aNFCab[NF_SAVEDEC_TG][nPosTribTg][2][nCampoTG]			+= nDifItem

							//Verifica se controla a sobra e se o valor da sobra acumulado � suficiente para descarregar no item
							If lSobra .And. ( aNFCab[NF_SAVEDEC_TG][nPosTribTg][2][nCampoTG] - nDifItDel ) >= nMeioCentavo
							
								aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][1][nY] 	:= nUmCentavo - nDifItem 
								aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][2][nY] 	:= 0								
								//Atualiza o SaveDec retirando 1 centavo, pois logo abaixo ser� adicionado 1 centavo no valor
								aNFCab[NF_SAVEDEC_TG][nPosTribTg][2][nCampoTG]			-= nUmCentavo								
								//Aqui adiciona 1 centavo no tributo, pois j� acumulou sobra suficiente
								aNfItem[nX][IT_TRIBGEN][nTrbGen][nCampoTG] 				+= nUmCentavo

							EndIF

						//----------------------------------------------------------------------------------------------------------------------
						//Verifica se o tributo est� configurado para arredondar, se existe valor de sobra e se o valor do tributo foi calculado
						//----------------------------------------------------------------------------------------------------------------------
						ElseIF aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_RND] .And. nDifItem > 0 .And. aNfItem[nX][IT_TRIBGEN][nTrbGen][nCampoTG] > 0
							//Aqui o tributo est� configurado para arredondar							
							aNFCab[NF_SAVEDEC_TG][nPosTribTg][2][nCampoTG] 			-= aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][2][nY]
							aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][2][nY] 	:= nDifItem
							aNFCab[NF_SAVEDEC_TG][nPosTribTg][2][nCampoTG]			+= nDifItem
							
							//Verifica se controla a sobra e se o valor da sobra acumulado � suficiente para descarregar no item
							
							If ( aNFCab[NF_SAVEDEC_TG][nPosTribTg][2][nCampoTG] - nDifItDel ) >= nMeioCentavo						 						
								
								aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][1][nY] 	:= nUmCentavo - nDifItem 
								aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][2][nY] 	:= 0
								
								//Atualiza o SaveDec retirando 1 centavo, pois logo abaixo ser� adicionado 1 centavo no valor
								aNFCab[NF_SAVEDEC_TG][nPosTribTg][2][nCampoTG]			-= nUmCentavo
								
								//Aqui adiciona 1 centavo no tributo, pois j� acumulou sobra suficiente
								aNfItem[nX][IT_TRIBGEN][nTrbGen][nCampoTG] 				+= nUmCentavo

							EndIf

						EndIF

						//Caso o controle de sobra esteja desabilitado, o savedec e itemdec ser�o zerados
						If !lSobra
							aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][1][nY]	:= 0
							aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][2][nY]	:= 0
							aNFCab[NF_SAVEDEC_TG][nPosTribTg][2][nCampoTG] 			:= 0
						Endif
					
					EndIF					

				EndIF

			EndIf
		EndIF
	Next nY

Next nTrbGen

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} TGAjuArred

Fun��o que far� tratamento do arredondamento dos valores dos tributos gen�ricos.
Esta fun��o foi constru�da com base na fun��o MaItArred, seguindo a mesma linha de
racioc�nio, por�m escalando para N tributos gen�ricos.
Esta fun��o ser� chamada no final da MaItArred, ela foi criada para separar os fontes
do configurador e n�o onerar o tamanho da MATXFIS.

@param aNFCab 	- Array com informa��es do cabe�alho da nota
@param aNfItem	- Array com informa��es do item da nota
@param aTGITRef	- Arrays com as refer�ncias dos tributos gen�ricos 
@param nx	    - N�mero do item posicionado
@param cCampo	- Refer�ncia a ser atualizada

@author Erick Dias
@since 04/12/2019
@version 12.1.27
/*/
//-------------------------------------------------------------------
Function TGAjuArred(aNFCab, aNfItem, aTGITRef, nx, cCampo)

Local nTrbGen		:= 0
Local nPosTribTg	:= 0
Local nY			:= 0
Local nCampoTG		:= 0

//Percorre lista dos tributos enquadrados e calculados para realizar corre��es de arredondamento
For nTrbGen:= 1 to Len(aNfItem[nx][IT_TRIBGEN])

	//Aqui verifico e busco posi��o do tributo no SaveDec que est� no aNfCab
	nPosTribTg	:= aScan(aNFCab[NF_SAVEDEC_TG], {|x| Alltrim(x[1])�== Alltrim(aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_SIGLA])})
	
	If nPosTribTg > 0

		//Rodo os campos de base, al�quota e valor do tributo gen�rico
		For nY:= 1 to Len(aTGITRef)
			
			//Verifica se o campo faz controle de arredondamento
			If aTGITRef[nY][4]
				
				nCampoTG	:= aTGITRef[nY][2]

				//Aqui verifica se o tributo est� configurador para truncar
				If !aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_RND]
						
					aNfItem[nX][IT_TRIBGEN][nTrbGen][nCampoTG] += aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][2][nY]
					aNfItem[nX][IT_TRIBGEN][nTrbGen][nCampoTG] -= aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][1][nY]
					
					aNFCab[NF_SAVEDEC_TG][nPosTribTg][2][nCampoTG] += aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][1][nY]
					aNFCab[NF_SAVEDEC_TG][nPosTribTg][2][nCampoTG] -= aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][2][nY]					

				Else
					//Aqui verifica se o tributo est� configurado para arredondar
					aNfItem[nX][IT_TRIBGEN][nTrbGen][nCampoTG] -= aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][2][nY]
					aNfItem[nX][IT_TRIBGEN][nTrbGen][nCampoTG] += aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][1][nY]

					If !(!Empty(cCampo) .And. cCampo == aTGITRef[nY][1])
						aNFCab[NF_SAVEDEC_TG][nPosTribTg][2][nCampoTG] += aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][1][nY]
						aNFCab[NF_SAVEDEC_TG][nPosTribTg][2][nCampoTG] -= aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][2][nY]
					EndIf

				EndiF
				aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][1][nY]:= 0
				aNfItem[nX][IT_TRIBGEN][nTrbGen][TG_IT_ITEMDEC][2][nY]:= 0

			EndIF

		NExt nY

	EndIF

Next nTrbGen

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} TgSaveDec

Fun��o que adiciona-r� uma nova posi��o para controle do SaveDec do tributo gen�rico,
caso o tributo n�o conste no Array.

@param aNFCab 		- Array com informa��es do cabe�alho da nota
@param aNfItem		- Array com informa��es do item da nota
@param nItem		- N�mero do item posicionado
@param nTrbGen		- N�mero do tributo gen�rico posicionado

@author Erick Dias
@since 09/12/2019
@version 12.1.27
/*/
//-------------------------------------------------------------------
Static Function TgSaveDec(aNFCab, aNfItem, nItem, nTrbGen)

//--------------------------------------------------------------------
//Adiciono nova posi��o para controle do SaveDec do tributo gen�rico
//--------------------------------------------------------------------
//Preciso verificar se o tributo j� consta no array do SaveDec, se j� existe n�o precisa adicionar, se n�o existe ai ser� criado.
If aScan(aNFCab[NF_SAVEDEC_TG], {|x| Alltrim(x[1])�== Alltrim(aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_SIGLA])}) == 0
	aadd(aNFCab[NF_SAVEDEC_TG],{aNfItem[nItem][IT_TRIBGEN][nTrbGen][TG_IT_SIGLA], Array(NMAX_IT_TG)})
	aFill(aNFCab[NF_SAVEDEC_TG] [Len(aNFCab[NF_SAVEDEC_TG])][2],0)
EndIf

Return