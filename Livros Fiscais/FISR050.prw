#INCLUDE "FISR050.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "REPORT.CH"
 
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} FISR050()

FUNCAO PARA CRIACAO DE RELATORIO DE APURACAO DE IMPOSTOS RETIDOS  POR NATUREZA
  
@author    Robson de Souza Moura
@version   12.1.3
@since     01/04/2015

/*/
//------------------------------------------------------------------------------------------
Function FISR050()        

Local lVerpesssen := Iif(FindFunction("Verpesssen"),Verpesssen(),.T.)

Private oReport := Nil
Private oSecCab := Nil 

If lVerpesssen
	ReportDef()
	oReport:PrintDialog()
EndIf
        
Return Nil   

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} FISR050()

REPORTDEF() - Defini��es do Relat�rio  

@author    Robson de Souza Moura
@version   12.1.3
@since     01/04/2015

/*/
//------------------------------------------------------------------------------------------
Static Function ReportDef()

Pergunte( "FISR050" , .F. ) 
//��������������������������������������������������������������Ŀ
//� Variaveis utilizadas para parametros                         �
//� mv_par01     // Natureza Inicial                             �
//� mv_par02     // Natureza Final                               �
//� mv_par03     // Data Vencimento Inicial                      �
//� mv_par04     // Data Vencimento Final                        �
//� mv_par05     // Imprime Abertos/Baixados/Ambos               �
//� mv_par06     // Ordem de Impressao Por Titulo / Data Emissao �
//����������������������������������������������������������������

dbSelectArea("SE2") 

oReport := TReport():New("FISR050",STR0001,"FISR050",{|oReport| PrintReport(oReport)},STR0002)

oReport:SetUseGC(.T.) 

oSection := TRSection():New(oReport,STR0001,{"TMP","SE2","SM0","SA2","SF1"}) 

oReport:SetLandscape() 

TRCell():New(oSection,"E2_NUM"      ,"TMP",STR0003)//PREFIXO DO T�TULO
TRCell():New(oSection,"E2_PREFIXO"  ,"TMP",STR0004)//PREFIXO DO T�TULO
TRCell():New(oSection,"E2_TIPO"     ,"TMP",STR0005)//TIPO
TRCell():New(oSection,"A2_COD"      ,"TMP",STR0006)//CODIGO DO FORNECEDOR
TRCell():New(oSection,"A2_NOME"     ,"TMP",STR0007)//RAZ�O SOCIAL
TRCell():New(oSection,"A2_CGC"      ,"TMP",STR0008)//CNPJ
TRCell():New(oSection,"E2_EMISSAO"  ,"TMP",STR0009)//EMISSAO
TRCell():New(oSection,"E2_VENCTO"   ,"TMP",STR0010)//VENCIMENTO
TRCell():New(oSection,"E2_VENCREA"  ,"TMP",STR0011)//VENCIMENTO REAL
TRCell():New(oSection,"F1_VALBRUT"  ,"TMP",STR0012)//VALOR BRUTO
TRCell():New(oSection,"F1_VALBRUT"  ,"TMP",STR0013)//VALBRUTO 
TRCell():New(oSection,"TMP_ALIQ"    ,"TMP",STR0014)//ALIQUOTA
TRCell():New(oSection,"E2_VALOR"    ,"TMP",STR0015)//IMPOSTO
TRCell():New(oSection,"E2_NATUREZ"  ,"TMP",STR0016)//NATUREZA

TRFunction():New(oSection:Cell("E2_VALOR"),/*cId*/,"SUM"     ,/*oBreak*/,/*cTitle*/,/*cPicture*/,/*uFormula*/,.F.           ,.T.           ,.F.        ,oSecCab)  

Return oReport 

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} FISR050()
IMPRESS�O  
@author    Robson de Souza Moura
@version   12.1.3
@since     01/04/2015
/*/
//------------------------------------------------------------------------------------------
Static Function PrintReport(oReport)

Local oSection  := oReport:Section(1)
Local cQuery    := ""
Local nPrefixo  := TamSx3("E2_PREFIXO")[1]
Local nNumTit   := TamSx3("E2_NUM")[1]
Local nTipo     := TamSx3("E2_TIPO")[1]
Local nParcela  := TamSx3("E2_PARCELA")[1]
Local nFornece  := TamSx3("E2_FORNECE")[1]
Local nLoja     := TamSx3("E2_LOJA")[1]
Local aStruSE2  := SE2->(dbStruct()) 
Local nX        := 0
Local cFitro    :=  oSection:GetSqlExp()
Local cTipoDB	:= AllTrim(Upper(TcGetDb()))

dbSelectArea("SE2") 

//� Quebra de secoes e totalizadores do Relatorio �

oBreak01 := TRBreak():New(oSection,oSection:Cell("E2_NATUREZ"),STR0017,.F.)
TRFunction():New(oSection:Cell("E2_VALOR"),"TOTNAT","SUM",oBreak01,/*Titulo*/,/*cPicture*/,/*uFormula*/,.F.,.F.)

TRPosition():New(oSection,"SA2",1,{|| xFilial("SA1") + TMP->A2_COD + TMP->A2_LOJA})
TRPosition():New(oSection,"SF1",1,{|| xFilial("SF1") + TMP->F1_DOC + TMP->F1_SERIE + TMP->F1_FORNECE + TMP->F1_LOJA })
	
cQuery := " SELECT  E2_NUM,E2_PREFIXO,E2_TIPO,E2_FORNECE,E2_EMISSAO,E2_VENCTO,E2_VENCREA,E2_VALOR,F1_EMISSAO,F1_VALBRUT,F1_DOC,F1_SERIE,F1_FORNECE,F1_LOJA,A2_NOME,A2_CGC,A2_COD,E2_NATUREZ,A2_LOJA, "  
cQuery += " CASE  WHEN (E2_VALOR >= F1_VALBRUT) THEN  0 ELSE  ((E2_VALOR * 100)/F1_VALBRUT) END AS TMP_ALIQ "
cQuery += " FROM " + RetSqlName("SE2")+ " SE2 "

cQuery += " INNER JOIN " + RetSqlname("SA2")+ " SA2 ON SA2.A2_FILIAL = '" + xFilial("SA2") + "'  AND SA2.A2_COD||SA2.A2_LOJA = SUBSTRING(SE2.E2_TITPAI,"+Alltrim(Str(nPrefixo+nNumTit+nTipo+nParcela+1))+","+Alltrim(Str(nFornece+nLoja))+") AND SA2.D_E_L_E_T_=' ' " 
cQuery += " LEFT JOIN  " + RetSqlname("SF1")+ " SF1 ON SF1.F1_FILIAL = '" + xFilial("SF1") + "'  AND SF1.F1_DOC = SUBSTRING(SE2.E2_TITPAI,"+Alltrim(Str(nPrefixo+1))+","+Alltrim(Str(nNumTit))+") AND SF1.F1_FORNECE||SF1.F1_LOJA = SUBSTRING(SE2.E2_TITPAI,"+Alltrim(Str(nPrefixo+nNumTit+nTipo+nParcela+1))+","+Alltrim(Str(nFornece+nLoja))+") AND SF1.F1_EMISSAO = SE2.E2_EMISSAO AND SF1.D_E_L_E_T_=' ' "

If MV_PAR05     == 1 
    cQuery += " WHERE SE2.E2_FILIAL='" +xFilial("SE2")+ "' AND SE2.E2_NATUREZ BETWEEN '"+MV_PAR01+"' AND '"+MV_PAR02+"' AND SE2.E2_VENCTO BETWEEN '"+DTOS(MV_PAR03)+"' AND '"+DTOS(MV_PAR04)+"' AND SE2.E2_BAIXA=' ' AND   SE2.D_E_L_E_T_=' ' "   
ElseIf MV_PAR05 == 2
	cQuery += " WHERE SE2.E2_FILIAL='" +xFilial("SE2")+ "' AND SE2.E2_NATUREZ BETWEEN '"+MV_PAR01+"' AND '"+MV_PAR02+"' AND SE2.E2_VENCTO BETWEEN '"+DTOS(MV_PAR03)+"' AND '"+DTOS(MV_PAR04)+"' AND SE2.E2_BAIXA <>' ' AND SE2.D_E_L_E_T_=' ' "
ElseIf MV_PAR05 == 3 
    cQuery += " WHERE SE2.E2_FILIAL='" +xFilial("SE2")+ "' AND SE2.E2_NATUREZ BETWEEN '"+MV_PAR01+"' AND '"+MV_PAR02+"' AND SE2.E2_VENCTO BETWEEN '"+DTOS(MV_PAR03)+"' AND '"+DTOS(MV_PAR04)+"' AND SE2.D_E_L_E_T_=' ' "       
Endif
If !Empty(cFitro)
	cQuery += " AND " + cFitro
EndIf
If MV_PAR06  == 1
	cQuery += " ORDER BY SE2.E2_NATUREZ,SE2.E2_NUM "
ElseIf MV_PAR06 == 2
	cQuery += " ORDER BY SE2.E2_NATUREZ,SE2.E2_EMISSAO "
Endif

cQuery := ChangeQuery(cQuery) 

dbUseArea(.T.,"TOPCONN",TCGenQry(,,cQuery),"TMP",.T.,.T.) //"Seleccionado registros"

For nX := 1 To len(aStruSE2)
		If aStruSE2[nX][2] <> "C" .And. FieldPos(aStruSE2[nX][1])<>0
			TcSetField("TMP",aStruSE2[nX][1],aStruSE2[nX][2],aStruSE2[nX][3],aStruSE2[nX][4])
		EndIf
Next nX

oReport:SetMeter(RecCount())

DbSelectArea("TMP")

ProcRegua(0)

While !TMP->(Eof())

	IncProc(STR0018) //"Aguarde, Gerando Relat�rio...."
	
	If oReport:Cancel()
		Exit
	EndIf	
	 
   	oSection:Init() 	
	oSection:PrintLine() 
	
	oSection:Cell("E2_NUM")	    :Show()
	oSection:Cell("E2_PREFIXO")	:Show()
	oSection:Cell("E2_TIPO")	:Show()
	oSection:Cell("A2_COD")     :Show()
	oSection:Cell("A2_NOME")	:Show()
	oSection:Cell("A2_CGC") 	:Show()
	oSection:Cell("E2_EMISSAO")	:Show()
	oSection:Cell("E2_VENCTO") 	:Show()
	oSection:Cell("E2_VENCREA")	:Show()
  	oSection:Cell("F1_VALBRUT")	:Show() 
	oSection:Cell("F1_VALBRUT")	:Show()
	oSection:Cell("TMP_ALIQ")   :Show()	
	oSection:Cell("E2_VALOR")	:Show()
	oSection:Cell("E2_NATUREZ")	:Show()

	TMP->(DbSkip())
Enddo         

oSection:Finish()
oReport:SkipLine()
oReport:IncMeter()
TMP->(dbCloseArea())

Return()

