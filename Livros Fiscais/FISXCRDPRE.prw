#INCLUDE "PROTHEUS.CH"
#INCLUDE "MATXDEF.CH"

/*/{Protheus.doc} FISXCRDPRE
    (Componentiza��o de um peda�o da fun��o xFisLF - 
    Atualiza os livros fiscais para o item.)
    
	@author Rafael Oliveira
    @since 11/05/2020
    @version 12.1.27

    @Autor da fun��o original 
    Edson Maricate # 20/12/1999
    
	@param:
	aNfCab      -> Array com dados do cabe�alho da nota
	aNFItem     -> Array com dados item da nota
	nItem       -> Item que esta sendo processado
	aPos        -> Array com dados de FieldPos de campos
	aInfNat	    -> Array com dados da narutureza
	aPE		    -> Array com dados dos pontos de entrada
	aSX6	    -> Array com dados Parametros
	aDic	    -> Array com dados Aliasindic
	aFunc	    -> Array com dados Findfunction        
    cExecuta    -> String vinda da pilha do MATXFIS 
                    "1" -  Presumido ICM
                    "2" -  Presumido Substituicao Tributaria
                    "3" - Credito Presumido Pela Carga Tribut�ria
/*/

Function FISXCRDPRE(aNfCab, aNFItem, nItem, aPos, aInfNat, aPE, aSX6, aDic, aFunc, nBICMOri,cExecuta, cAliasPROD)
Local cLeiteIn 	 := aSX6[MV_PRODLEI]
Local aRegra     := {}
Local nX         := 0
Local cMvEstado  := aSX6[MV_ESTADO]
Local cProdLeite := IIf((cAliasPROD)->(FieldPos(cLeiteIn)) > 0 , (cAliasPROD)->&(cLeiteIn) , "" )
Local nCrdPresMG := Iif( aPos[FP_B1_CRDPRES] .And. !Empty(aNfItem[nItem][IT_PRD][SB_CRDPRES]) , aNfItem[nItem][IT_PRD][SB_CRDPRES] , aNFItem[nItem][IT_TS][TS_CRDPRES] )
Local nCrePSC	 := 0
Local aMVCRPRESC := &(aSX6[MV_CRPRESC])
IF cExecuta == "1"
    // CREDITO ESTIMULO MANAUS - TS_CRDEST = 1 - Nao Calcula | 2 - Produtos Eletronicos | 3 - Contrucao Civil |4 - Pelo NCM
    If aNFItem[nItem][IT_TS][TS_CRDEST]$"23" .And. aPos[FP_B1_CRDEST] .And. aPos[FP_F3_CRDEST]
        aNfItem[nItem][IT_LIVRO][LF_CRDEST]	:= NoRound(aNfItem[nItem][IT_VALICM] * aNfItem[nItem][IT_PRD][SB_CRDEST] /100,2)
    ElseIf aNFItem[nItem][IT_TS][TS_CRDEST]$"4" //Deve ser feito para entradas e saidas (calculo: Cr�ditos - D�bitos * %de est�mulo)
        If FindFunction("M953CRDM")
            aRegra := M953CRDM() // Lembre-se de refazer esta funcao para melhorar a performance
        EndIf
        If Len(aRegra)>0 .And.;
            (nX := AScanX(aRegra, {|x| x[1]== Alltrim(aNfItem[nItem][IT_LIVRO][LF_POSIPI]) .And. x[2]== aNfCab[NF_A1CRDMA] } )) > 0 .Or.;
            (nX := AScanX(aRegra, {|x| x[1]== Alltrim(aNfItem[nItem][IT_LIVRO][LF_POSIPI]) .And. x[2]=="4" } )) > 0
            aNfItem[nItem][IT_LIVRO][LF_CRDEST]	:= NoRound(aNfItem[nItem][IT_VALICM] * aRegra[nX,3]/100, 2 )
        EndIf
    EndIf

    //Preenche Tipo de Credito presumido para PE mesmo que nota seja n�o icentivada
    If aNFItem[nItem][IT_TS][TS_TPPRODE] <> "" .And. cMvEstado == "PE"
        aNfItem[nItem][IT_LIVRO][LF_TPPRODE] := aNFItem[nItem][IT_TS][TS_TPPRODE]
    EndIf

    //CREDITO PRESUMIDO referente a Zona Franca de Manaus
    aNfItem[nItem][IT_LIVRO][LF_CRDZFM] := aNfItem[nItem][IT_CRDZFM]

    If aNFItem[nItem][IT_TS][TS_CRDPRES] > 0 .And. !Empty(aNFItem[nItem][IT_TS][TS_TPCPRES])

        //Se o percentual do Cr�dito Presumido TS_CRDPRES estiver preenchido e o Tipo de Cr�dito Presumido TS_TPCPRES
        //tamb�m estiver preenchido, ent�o o cr�dito presumido ser� calculado seguindo esta nova regra. Caso somente o percentual TS_CRDPRES
        //estivere preenchido, ent�o ir� seguir as regras que j� existiam antes desta implementa��o, mantendo assim o legado.
        Do Case
            Case aNFItem[nItem][IT_TS][TS_TPCPRES] == "C"
                //Base de c�lculo para cr�dito presumido ser� o valor cont�bil
                aNfItem[nItem][IT_LIVRO][LF_CRDPRES] := NoRound((aNfItem[nItem][IT_LIVRO][LF_VALCONT] * aNFItem[nItem][IT_TS][TS_CRDPRES]) / 100,2)
                aNfItem[nItem][IT_LIVRO][LF_BASECPR] := aNfItem[nItem][IT_LIVRO][LF_VALCONT]

            Case aNFItem[nItem][IT_TS][TS_TPCPRES] == "R"
                //Base de c�lculo para cr�dito presumido ser� a base do ICMS, e ir� reduzir o valor do cr�dito presumido do total do documento.
                aNfItem[nItem][IT_LIVRO][LF_CRDPRES] := NoRound((aNfItem[nItem][IT_BASEICM] * aNFItem[nItem][IT_TS][TS_CRDPRES]) / 100,2)
                aNfItem[nItem][IT_LIVRO][LF_BASECPR] := aNfItem[nItem][IT_BASEICM]
                aNfItem[nItem][IT_LIVRO][LF_VALCONT] -= aNfItem[nItem][IT_LIVRO][LF_CRDPRES]
            Case aNFItem[nItem][IT_TS][TS_TPCPRES] == "F" // Opcao limita total operacao por frete
                If aNfItem[nItem][IT_FRETE]>0
                    aNfItem[nItem][IT_LIVRO][LF_CRDPRES] := NoRound((aNfItem[nItem][IT_LIVRO][LF_VALCONT] * aNFItem[nItem][IT_TS][TS_CRDPRES]) / 100,2)
                    aNfItem[nItem][IT_LIVRO][LF_BASECPR] := aNfItem[nItem][IT_LIVRO][LF_VALCONT]
                    // Limita ao valor do Frete
                    If aNfItem[nItem][IT_LIVRO][LF_CRDPRES] > aNfItem[nItem][IT_FRETE]
                        aNfItem[nItem][IT_LIVRO][LF_CRDPRES] := aNfItem[nItem][IT_FRETE]
                        aNfItem[nItem][IT_LIVRO][LF_BASECPR] := aNfItem[nItem][IT_FRETE]
                    EndIf
                    // Para SC sera baseado no frete so' e somente se, este nao ultrapassa valor de pauta.
                    // Sendo assim, limita ao valor da pauta
                    If (cMvEstado$"SC") .AND. (aNfItem[nItem][IT_PAUTIC]>0) .AND. (aNfItem[nItem][IT_LIVRO][LF_CRDPRES]>aNfItem[nItem][IT_PAUTIC])
                        aNfItem[nItem][IT_LIVRO][LF_CRDPRES] := aNfItem[nItem][IT_PAUTIC]
                        aNfItem[nItem][IT_LIVRO][LF_BASECPR] := aNfItem[nItem][IT_PAUTIC]
                    EndIf
                EndIf
            Case aNFItem[nItem][IT_TS][TS_TPCPRES] == "M"
                aNfItem[nItem][IT_LIVRO][LF_CRDPRES] := NoRound((aNfItem[nItem][IT_VALMERC] * aNFItem[nItem][IT_TS][TS_CRDPRES]) / 100,2)
                aNfItem[nItem][IT_LIVRO][LF_BASECPR] := aNfItem[nItem][IT_VALMERC]
            Case aNFItem[nItem][IT_TS][TS_TPCPRES] == "B"
                //Base de c�lculo para cr�dito presumido ser� a base do ICMS
                aNfItem[nItem][IT_LIVRO][LF_CRDPRES] := NoRound((aNfItem[nItem][IT_BASEICM] * aNFItem[nItem][IT_TS][TS_CRDPRES]) / 100,2)
                aNfItem[nItem][IT_LIVRO][LF_BASECPR] := aNfItem[nItem][IT_BASEICM]
            Case aNFItem[nItem][IT_TS][TS_TPCPRES] == "N" //Base calculo valor do ICMS
                If aSX6[MV_RNDICM]
                    aNfItem[nItem][IT_LIVRO][LF_CRDPRES] := Round((aNfItem[nItem][IT_VALICM] * aNFItem[nItem][IT_TS][TS_CRDPRES]) / 100,2)
                Else
                    aNfItem[nItem][IT_LIVRO][LF_CRDPRES] := NoRound((aNfItem[nItem][IT_VALICM] * aNFItem[nItem][IT_TS][TS_CRDPRES]) / 100,2)
                EndIf
                aNfItem[nItem][IT_LIVRO][LF_BASECPR] := aNfItem[nItem][IT_VALICM]
        EndCase

        //CREDITO PRESUMIDO - MG - RICMS/02 - Inciso X, artigo 75 do estado de MG
        //Agora passa a funcionar de forma gen�rica, n�o s� para MG.
        If aNFItem[nItem][IT_TS][TS_AGREGCP]=="1" // Agrega o credito presumido ao valor total
            // Agrega o credito presumido ao valor total e duplicata, seguindo a mesma regra do produto agregando ao total
            aNfItem[nItem][IT_TOTAL] += aNfItem[nItem][IT_LIVRO][LF_CRDPRES]
            If aNFItem[nItem][IT_TS][TS_TPCPRES] $ "C|M"// C-Cred. Tot. Oper; M-Cred. Val. Merc.
                aNfItem[nItem][IT_LIVRO][LF_VALCONT] += aNfItem[nItem][IT_LIVRO][LF_CRDPRES]
            EndIf

            If aNFItem[nItem][IT_TS][TS_DUPLIC] <> "N"
                aNfItem[nItem][IT_BASEDUP] += aNfItem[nItem][IT_LIVRO][LF_CRDPRES]
            EndIf
        EndIf

    ElseIf aNFItem[nItem][IT_TS][TS_CRDPRES] > 0 .Or. aNfItem[nItem][IT_B1DIAT] == "1" .Or. nCrdPresMG > 0 .Or. aNFItem[nItem][IT_TS][TS_CRPRSIM] > 0 .Or. aNFItem[nItem][IT_TS][TS_CRPRERO] > 0 .Or. ;
        aNFItem[nItem][IT_TS][TS_CRPRESP] > 0 .Or. aNFItem[nItem][IT_TS][TS_CROUTSP] > 0 .Or. aNFItem[nItem][IT_TS][TS_CRPREPR] > 0 .Or. aNFItem[nItem][IT_TS][TS_CPRESPR] > 0 .Or. aNFItem[nItem][IT_TS][TS_CRPREPE] > 0 .Or. aNFItem[nItem][IT_TS][TS_CPPRODE] > 0

        If cMvEstado == "RJ" .Or. cMvEstado == "SC" .Or. cMvEstado == "PR" .Or. cMvEstado == "SP" .Or. cMvEstado == "MT" .Or. cMvEstado == "PE";
            .Or. cMvEstado == "RO" .Or. cMvEstado == "MG" .Or. cMvEstado == "CE" .Or. (cMvEstado == "RS" .And. (aNFItem[nItem][IT_TS][TS_CREDPRE] > 0 .Or. cProdLeite == "1" ))

            //CREDITO PRESUMIDO - MG - RICMS/02 - Inciso X, artigo 75 do estado de MG
            If cMvEstado == "MG" .And. nCrdPresMG > 0
                aNfItem[nItem][IT_CRPREMG]	:= 0

                If aSX6[MV_RNDICM]
                    aNfItem[nItem][IT_CRPREMG]	:= Round((aNfItem[nItem][IT_VALMERC] + Iif( aNFItem[nItem][IT_TS][TS_AGREG] == "I" , aNfItem[nItem][IT_VALICM] , 0 ) ) * (nCrdPresMG / 100) , 2 )
                Else
                    aNfItem[nItem][IT_CRPREMG]	:= NoRound((aNfItem[nItem][IT_VALMERC] + Iif( aNFItem[nItem][IT_TS][TS_AGREG] == "I" , aNfItem[nItem][IT_VALICM] , 0 ) ) * (nCrdPresMG / 100) )
                EndIf

                If aNFCab[NF_OPERNF]=="S" .And. !aNFItem[nItem][IT_TS][TS_AGREG] == "I" .And. aSX6[MV_VALICM]
                    If aSX6[MV_RNDICM]
                        aNfItem[nItem][IT_CRPREMG]	:= Round((aNfItem[nItem][IT_VALICM] * nCrdPresMG) / 100,2)
                    Else
                        aNfItem[nItem][IT_CRPREMG]	:= NoRound((aNfItem[nItem][IT_VALICM] * nCrdPresMG) / 100,2)
                    EndIf
                EndIf

                If aNFItem[nItem][IT_TS][TS_AGREGCP]=="1" // Agrega o credito presumido ao valor total e duplicata
                    aNfItem[nItem][IT_TOTAL] += NoRound(aNfItem[nItem][IT_CRPREMG])
                    If aNFItem[nItem][IT_TS][TS_DUPLIC] <> "N"
                        aNfItem[nItem][IT_BASEDUP] += NoRound(aNfItem[nItem][IT_CRPREMG])
                    EndIf
                EndIf

                aNfItem[nItem][IT_LIVRO][LF_CRDPRES] := aNfItem[nItem][IT_CRPREMG]
            EndIf

            //CREDITO PRESUMIDO - RJ - Rio de Janeiro
            If cMvEstado == "RJ"
                If aSX6[MV_CRPRERJ]
                    aNfItem[nItem][IT_LIVRO][LF_CRDPRES] := NoRound((aNfItem[nItem][IT_BASEICM] * aNFItem[nItem][IT_TS][TS_CRDPRES]) / 100,2)
                Else
                    aNfItem[nItem][IT_LIVRO][LF_CRDPRES] := NoRound((aNfItem[nItem][IT_LIVRO][LF_VALCONT] * aNFItem[nItem][IT_TS][TS_CRDPRES]) / 100,2)
                Endif
            Endif

            //CREDITO PRESUMIDO - SP - Lei 6.374,de 01.03.1989 nos art.38,6� e 112 regulamentada pelo Dec. 52.381 de 19.11.2007 DOE PR de 22.11.2007
            If cMvEstado == "SP"
                aNfItem[nItem][IT_LIVRO][LF_CRDPRES] := NoRound((aNfItem[nItem][IT_LIVRO][LF_VALCONT] * aNFItem[nItem][IT_TS][TS_CRDPRES]) / 100,2)
                //CREDITO PRESUMIDO - SP - Conforme o Decreto 52.586 de 28.12.2007, relativo a aquisicao de Leite Cru
                If aNFItem[nItem][IT_TS][TS_CRPRESP] > 0 .And. aNFCab[NF_OPERNF] == "E"
                    aNfItem[nItem][IT_LIVRO][LF_CRPRESP] := NoRound((aNfItem[nItem][IT_LIVRO][LF_VALCONT] * aNFItem[nItem][IT_TS][TS_CRPRESP]) / 100,2)
                Endif
                //CREDITO OUTORGADO - SP
                //Conforme Decreto 56.018 de 16.07.2010 - Art. 31 do Anexo III do RICMS,
                //relativo a entrada de carnes e demais produtos comestiveis.
                aNfItem[nItem][IT_CROUTSP]:= 0
                If Substr(aNfItem[nItem][IT_POSIPI],1,4) $ aSX6[MV_CROUTSP] .And. aNFCab[NF_UFDEST]=="SP" .And. aNFCab[NF_UFORIGEM]=="SP" .And. aNFItem[nItem][IT_TS][TS_CROUTSP] > 0
                    aNfItem[nItem][IT_CROUTSP]:= NoRound((aNfItem[nItem][IT_VALMERC] * aNFItem[nItem][IT_TS][TS_CROUTSP]) / 100,2)
                Endif
                aNfItem[nItem][IT_LIVRO][LF_CROUTSP] := aNfItem[nItem][IT_CROUTSP]
            Endif

            //CREDITO PRESUMIDO - RS de Acordo com o RICMS - Livro I, titulo V, Atr. 32, Inciso XIX.
            If cMvEstado == "RS"
                If aNFItem[nItem][IT_TS][TS_CREDPRE] > 0 .And. aNFCab[NF_OPERNF] == "E"
                    aNfItem[nItem][IT_LIVRO][LF_CREDPRE] := NoRound((aNfItem[nItem][IT_QUANT] * aNFItem[nItem][IT_TS][TS_CREDPRE]) , 2 )
                EndIf

                If cProdLeite == "1" .And. aNFCab[NF_OPERNF] == "S"
                    aNfItem[nItem][IT_LIVRO][LF_CRDPRES] := NoRound((aNfItem[nItem][IT_VALICM]*(aNFItem[nItem][IT_TS][TS_CRDPRES]/100)),2)
                Endif
            Endif

            //CREDITO PRESUMIDO - SC RICMS - Anexo 02 - Benef�cios Fiscais - Capitulo III (Art. 18) e  Art 142
            If cMvEstado == "SC"
                aNfItem[nItem][IT_CRPRESC]	:= 0

                If 	aNfItem[nItem][IT_B1DIAT] == "1" .And. aNfItem[nItem][IT_PREDIC] == 0 .And. aNFItem[nItem][IT_TS][TS_CRDPRES] == 0  ;
                    .And. ( aNFCab[NF_OPERNF] == "S" .Or. (aNFCab[NF_OPERNF] == "E" .And. aNFCab[NF_TIPONF]$"DB") )
                    //Conforme Art. 15, IX do Anexo 2 - RICMS - DO CREDITO PRESUMIDO SC
                    //De acordo com o Regime Especial DIAT-SC o percentual do credito presumido
                    //e' definido conforme aliquota do ICMS DIAT - SC
                    For nCrePSC := 1 to Len(aMVCRPRESC)
                        If aNfItem[nItem][IT_ALIQICM] == aMVCRPRESC[nCrePSC][1]
                            aNfItem[nItem][IT_CRPRESC]:= ((aNfItem[nItem][IT_VALICM] * aMVCRPRESC[nCrePSC][2] ) / 100)
                            Exit
                        EndIf
                    Next
                    MaItArred(nItem,{"IT_CRPRESC"})

                ElseIf aNFItem[nItem][IT_TS][TS_CRDPRES] > 0 .And. aNFCab[NF_OPERNF] == "E" .And. !Empty(aNfItem[nItem][IT_FRETE])
                    If aNfItem[nItem][IT_FRETE] > NoRound((aNfItem[nItem][IT_VALMERC] * aNFItem[nItem][IT_TS][TS_CRDPRES]) / 100,2)
                        aNfItem[nItem][IT_CRPRESC]	:= NoRound((aNfItem[nItem][IT_VALMERC] * aNFItem[nItem][IT_TS][TS_CRDPRES]) / 100,2)
                    Else
                        aNfItem[nItem][IT_CRPRESC]	:= aNfItem[nItem][IT_FRETE]
                    EndIf
                Elseif aNFCab[NF_OPERNF] == "S"
                    aNfItem[nItem][IT_CRPRESC]	:= NoRound((aNfItem[nItem][IT_BASEICM] * aNFItem[nItem][IT_TS][TS_CRDPRES]) / 100,2)
                EndIf
                aNfItem[nItem][IT_LIVRO][LF_CRDPRES]	:= aNfItem[nItem][IT_CRPRESC]

                //CREDITO PRESUMIDO - SC - Simples Nacional
                //Lei 14.264/07 - Decreto 1036 de 28/01/08  RICMS/SC Art. 29, Parag 5
                aNfItem[nItem][IT_CRPRSIM]	:= 0
                If aNFCab[NF_SIMPNAC]=="1" .And. aNFItem[nItem][IT_TS][TS_CRPRSIM] > 0 .And. !(aNfItem[nItem][IT_VALSOL] > 0) .And. (Substr(aNfItem[nItem][IT_LIVRO][LF_CFO],1,1)$"1" .Or. (aNFCab[NF_OPERNF] == "S" .And. aNFCab[NF_TIPONF] $ "BD"))
                    aNfItem[nItem][IT_CRPRSIM]	:= (aNfItem[nItem][IT_VALMERC] - (aNfItem[nItem][IT_DESCONTO]+aNfItem[nItem][IT_DESCTOT])) * aNFItem[nItem][IT_TS][TS_CRPRSIM] / 100
                EndIf
                aNfItem[nItem][IT_LIVRO][LF_CRPRSIM] := aNfItem[nItem][IT_CRPRSIM]
            EndIf

            //CREDITO PRESUMIDO - CE - Artigo 64 Inciso VII
            if cMvEstado=="CE" .And. (aNFItem[nItem][IT_TS][TS_CRDPRES]>0) .And. aNFCab[NF_OPERNF]=='E'
                If aNfItem[nItem][IT_FRETE] > NoRound((aNfItem[nItem][IT_VALMERC] * aNFItem[nItem][IT_TS][TS_CRDPRES]) / 100,2)
                    aNfItem[nItem][IT_CRPRECE]	:= NoRound((aNfItem[nItem][IT_VALMERC] * aNFItem[nItem][IT_TS][TS_CRDPRES]) / 100,2)
                Else
                    aNfItem[nItem][IT_CRPRECE]	:= aNfItem[nItem][IT_FRETE]
                EndIf
                aNfItem[nItem][IT_LIVRO][LF_CRDPRES] :=  aNfItem[nItem][IT_CRPRECE]
            EndIF

            //CREDITO PRESUMIDO - PR  Lei 14.985 de 06.01.2006 Decreto 6.144 - 22.02.2006 - DOE PR
            If cMvEstado == "PR"
                If aNFItem[nItem][IT_TS][TS_AGREG]$"BC"
                    aNfItem[nItem][IT_LIVRO][LF_CRDPRES] := NoRound((IIF(aNfItem[nItem][IT_BASEICM]==0,aNfItem[nItem][IT_LIVRO][LF_VALCONT]/(1-(aNfItem[nItem][IT_ALIQICM]/100)),aNfItem[nItem][IT_BASEICM])*(aNFItem[nItem][IT_TS][TS_CRDPRES]/100)),2)
                Else
                    aNfItem[nItem][IT_LIVRO][LF_CRDPRES] := NoRound((IIF(aNfItem[nItem][IT_BASEICM]==0,nBICMOri,aNfItem[nItem][IT_BASEICM])*(aNFItem[nItem][IT_TS][TS_CRDPRES]/100)),2)
                EndIf
                //CREDITO PRESUMIDO - PR  RICMS - Art. 4 Anexo III - Credito Presumido - PR Decreto n. 1.980
                aNfItem[nItem][IT_CRPREPR]	:= 0
                If aNFItem[nItem][IT_TS][TS_CRPREPR] > 0 // .And. aNFCab[NF_OPERNF] == "E"
                    aNfItem[nItem][IT_CRPREPR] := NoRound((aNfItem[nItem][IT_LIVRO][LF_VALCONT] * aNFItem[nItem][IT_TS][TS_CRPREPR]) / 100,2)
                Endif
                aNfItem[nItem][IT_LIVRO][LF_CRPREPR] := aNfItem[nItem][IT_CRPREPR]
                aNfItem[nItem][IT_LIVRO][LF_CPRESPR] := aNfItem[nItem][IT_CPRESPR] // CREDITO PRESUMIDO - PR - Art.631-A do RICMS/2008
            Endif

            //CREDITO PRESUMIDO - MT - Mato Grosso Comunicado PRODEIC 067/2005 Resolucao 36/2005 Lei 7.958/2003 Decreto 1.432/2003
            If cMvEstado == "MT"
                aNfItem[nItem][IT_LIVRO][LF_CRDPRES] := NoRound((aNfItem[nItem][IT_VALICM]*(aNFItem[nItem][IT_TS][TS_CRDPRES]/100)),2)
            Endif

            //CREDITO PRESUMIDO - RO - Rondonia Lei 1.473/2005 - Artigo 1 Operacoes Interestaduais com produtos importados
            If cMvEstado == "RO"
                If SubStr(aNfItem[nItem][IT_LIVRO][LF_CFO],1,1) $ "6" .And. aNfItem[nItem][IT_PRD][SB_ORIGEM] == "1"
                    aNfItem[nItem][IT_LIVRO][LF_CRDPRES] := NoRound((aNfItem[nItem][IT_VALICM]*(aNFItem[nItem][IT_TS][TS_CRDPRES]/100)),2)
                EndIf

                If aNFItem[nItem][IT_TS][TS_CRPRERO] > 0 //CREDITO PRESUMIDO - RO - RICMS - (Art. 39) Anexo IV
                    aNfItem[nItem][IT_LIVRO][LF_CRPRERO] := NoRound((aNfItem[nItem][IT_VALICM]*(aNFItem[nItem][IT_TS][TS_CRPRERO]/100)),2)
                EndIf
            Endif

            //CREDITO PRESUMIDO - PE - Art.6 Decreto  n28.247
            If cMvEstado == "PE"
                aNfItem[nItem][IT_CRPREPE]:= 0
                If aNFItem[nItem][IT_TS][TS_CRPREPE] > 0
                    aNfItem[nItem][IT_CRPREPE]:= NoRound( ( aNfItem[nItem][IT_VALICM] * (aNFItem[nItem][IT_TS][TS_CRPREPE]/100) ) , 2 )
                EndIf
                aNfItem[nItem][IT_LIVRO][LF_CRPREPE] := aNfItem[nItem][IT_CRPREPE]

                //No trecho abaixo do c�lculo do Prodepe, a refer�ncia IT_CPPRODE somente existe para controle de arredondamento e controle de sobra
                If aNFItem[nItem][IT_TS][TS_CPPRODE] > 0
                    If 	aNfItem[nItem][IT_LIVRO][LF_TPPRODE] $ "5"
                        aNfItem[nItem][IT_CPPRODE]	:= ( aNfItem[nItem][IT_LIVRO][LF_VALCONT] - aNfItem[nItem][IT_VALSOL] ) * ( aNFItem[nItem][IT_TS][TS_CPPRODE] / 100 )
                        MaItArred(nItem,{"IT_CPPRODE"})
                        aNfItem[nItem][IT_LIVRO][LF_CPPRODE] := aNfItem[nItem][IT_CPPRODE]
                    Else
                        aNfItem[nItem][IT_CPPRODE]	:= aNfItem[nItem][IT_VALICM] * ( aNFItem[nItem][IT_TS][TS_CPPRODE] / 100 )
                        MaItArred(nItem,{"IT_CPPRODE"})
                        aNfItem[nItem][IT_LIVRO][LF_CPPRODE] := aNfItem[nItem][IT_CPPRODE]
                    Endif
                    If 	aNfItem[nItem][IT_LIVRO][LF_TPPRODE] $ "3#4" .And. aNfItem[nItem][IT_LIVRO][LF_CPPRODE] > aNfItem[nItem][IT_FRETE]
                        aNfItem[nItem][IT_CPPRODE]	:= aNfItem[nItem][IT_FRETE]
                        MaItArred(nItem,{"IT_CPPRODE"})
                        aNfItem[nItem][IT_LIVRO][LF_CPPRODE] := aNfItem[nItem][IT_CPPRODE]
                    EndIf
                EndIf
            EndIf

        Else
            //Calculo do Credito Presumido para todos os outros estados que nao possuem uma regra definida
            //Caso a regra do calculo seja essa mesma, somente sera preciso alterar o P9AUTOTEXT para apresentar na apuracao.
            aNfItem[nItem][IT_LIVRO][LF_CRDPRES] := NoRound((aNfItem[nItem][IT_LIVRO][LF_VALCONT] * aNFItem[nItem][IT_TS][TS_CRDPRES]) / 100,2)
        EndIf
    EndIf
Elseif cExecuta == "2" // Presumido Substituicao Tributaria
    //Grava valor Credito Presumido Substituicao Tributaria retido pelo contratante do servico de transporte - Decreto 44.147/2005 (MG)
    If aNFItem[nItem][IT_TS][TS_CRPRST]<>0
        aNfItem[nItem][IT_LIVRO][LF_CRPRST]	 := aNfItem[nItem][IT_VLCSOL] - aNfItem[nItem][IT_VALSOL]
        // Joao: Verifiquei todas as chamadas de MaFisLF e ningu�m passa este par�metro como .T.
        // Para n�o mudar todas as fun��es abaixo para FUNCTION optei por comentar o trecho. Caso
        // ocorra algum reflexo ser� necess�rio alterar todas as fun��es para FUNCTION pois foi
        // preciso retirar a MaFisLF do MATXFIS por conta do tamanho do fonte.
        /*If lRecPreSt
            MaAliqSoli(nItem)
            MaExcecao(nItem)
            MaMargem(nItem)
            MaFisVSol(nItem)
            MaFisVTot(nItem)
        EndIf*/
    EndIf
ElseIF  cExecuta == "3"  //CREDITO PRESUMIDO PELA CARGA TRIBUT�RIA

    //  Exemplo: DECRETO N. 42.649 DE 05 DE OUTUBRO DE 2010  /RJ
    If aNFItem[nItem][IT_TS][TS_CPRCATR] == "1" .And. aNfItem[nItem][IT_PRD][SB_B1CALTR] == "1"
        If aNFCab[NF_OPERNF] == "S" .And. (Substr(aNfItem[nItem][IT_LIVRO][LF_CFO],1,1)$"6" .Or. aNfItem[nItem][IT_PRD][SB_ORIGEM] <> "0" .Or. aSX6[MV_CPCATRI])
            aNfItem[nItem][IT_LIVRO][LF_CRDPCTR] := aNfItem[nItem][IT_LIVRO][LF_VALICM] - aNfItem[nItem][IT_LIVRO][LF_VALFECP] - ( aNfItem[nItem][IT_LIVRO][LF_BASEICM] * ( aNfItem[nItem][IT_PRD][SB_B1CATRI] / 100 )  )
        ElseIf aNFCab[NF_OPERNF] == "E" .And. aNFCab[NF_TIPONF] $ "DB"
            aNfItem[nItem][IT_LIVRO][LF_CRDPCTR] := aNfItem[nItem][IT_LIVRO][LF_VALICM] - aNfItem[nItem][IT_LIVRO][LF_VALFECP] - ( aNfItem[nItem][IT_LIVRO][LF_BASEICM] * ( aNfItem[nItem][IT_PRD][SB_B1CATRI] / 100)  )
        EndIf
    EndIf
Endif

Return

    