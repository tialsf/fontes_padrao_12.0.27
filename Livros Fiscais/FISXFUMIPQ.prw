#INCLUDE "PROTHEUS.CH"
#INCLUDE "MATXDEF.CH"

/*/{Protheus.doc} FISXFUMIPQ
    (Componentiza��o da fun��o MaFisFMPEQ - 
    Fundo Municipal de Fomento � Micro e Pequena Empresa (Fumipeq)
    
	@author Rafael.soliveira
    @since 17/02/2020
    @version 12.1.25
    
	@param:
	aNfCab -> Array com dados do cabe�alho da nota
	aNFItem-> Array com dados item da nota
	nItem  -> Item que esta sendo processado
	aPos   -> Array com dados de FieldPos de campos
	aInfNat	-> Array com dados da narutureza
	aPE		-> Array com dados dos pontos de entrada
	aSX6	-> Array com dados Parametros
	aDic	-> Array com dados Aliasindic
	aFunc	-> Array com dados Findfunction	
    /*/
Function FISXFUMIPQ(aNfCab, aNFItem, nItem, aPos, aInfNat, aPE, aSX6, aDic, aFunc)
    aNfItem[nItem][IT_VALFMP] := 0
    aNfItem[nItem][IT_BASEFMP]:= 0
    aNfItem[nItem][IT_ALQFMP] := aInfNat[25]
    
    IF ((aNfCab[NF_OPERNF] == "S" .And. aNfCab[NF_UFDEST] == "AM") .Or. aNfCab[NF_TIPONF]=="D") .And. aInfNat[24] == "1" .And. aNfItem[nItem][IT_ALQFMP] > 0
        aNfItem[nItem][IT_BASEFMP]:= aNfItem[nItem][IT_TOTAL]
        aNfItem[nItem][IT_VALFMP] := aNfItem[nItem][IT_BASEFMP] * (aNfItem[nItem][IT_ALQFMP]/100)
    EndIf
Return