#INCLUDE "PROTHEUS.CH"
#INCLUDE "MATXDEF.CH"

/*/{Protheus.doc} FISxSenar
    (Componentiza��o da fun��o MaFisSENAR - 
    Calculo do Servi�o Nacional de Aprendizagem Rural (SENAR))    
    
	@author Renato Rezende
    @since 17/02/2020
    @version 12.1.25
    
	@param:
	aNfCab -> Array com dados do cabe�alho da nota
	aNFItem-> Array com dados item da nota
	nItem  -> Item que esta sendo processado
	aPos   -> Array com dados de FieldPos de campos
	aInfNat	-> Array com dados da narutureza
	aPE		-> Array com dados dos pontos de entrada
	aSX6	-> Array com dados Parametros
	aDic	-> Array com dados Aliasindic
	aFunc	-> Array com dados Findfunction	
    /*/
Function FISxSenar(aNfCab, aNFItem, nItem, aPos, aInfNat, aPE, aSX6, aDic, aFunc)

Local lDev := Iif(Alltrim(aNfCab[NF_TIPONF]) $ "DB",.T.,.F.)
Local nBCFun := MaFisBCFun(nItem)
Local cFunrural := aSX6[MV_FUNRURA]

aNfItem[nItem][IT_BSSENAR]:= 0
aNfItem[nItem][IT_ALSENAR]:= 0
aNfItem[nItem][IT_VLSENAR]:= 0

If aNFItem[nItem][IT_TS][TS_ALSENAR] > 0 .And. (aNFItem[nItem][IT_TS][TS_DUPLIC] == "S" .Or. aNFItem[nItem][IT_TS][TS_CSENAR]=="1")
	If ((SubStr(aNfItem[nItem][IT_CF],1,1) $ "5|6|7" .And. !lDev) .Or.(SubStr(aNfItem[nItem][IT_CF],1,1) $ "1|2" .And. lDev))
		iF nBCFun > 0 .and. cFunrural = '1'
			aNfItem[nItem][IT_BSSENAR] := nBCFun 
		Else
			aNfItem[nItem][IT_BSSENAR]:= aNfItem[nItem][IT_TOTAL]
		EndIf
		aNfItem[nItem][IT_ALSENAR]:= aNFItem[nItem][IT_TS][TS_ALSENAR]
		aNfItem[nItem][IT_VLSENAR]:= aNfItem[nItem][IT_BSSENAR]*(aNFItem[nItem][IT_TS][TS_ALSENAR]/100)
		MaItArred(nItem,{"IT_VLSENAR"})
	ElseIf (((SubStr(aNfItem[nItem][IT_CF],1,1) $ "1|2") .Or.(SubStr(aNfItem[nItem][IT_CF],1,1) $ "5|6" .And. lDev)) .And. (aNfCab[NF_TPCLIFOR] == "F" .Or. aNfCab[NF_TIPORUR] == "F"))
		iF nBCFun > 0 .and. cFunrural == '1'
			aNfItem[nItem][IT_BSSENAR] := nBCFun
		Else
			aNfItem[nItem][IT_BSSENAR]:= aNfItem[nItem][IT_TOTAL]
		EndIf
		aNfItem[nItem][IT_ALSENAR]:= aNFItem[nItem][IT_TS][TS_ALSENAR]
		aNfItem[nItem][IT_VLSENAR]:= aNfItem[nItem][IT_BSSENAR]*(aNFItem[nItem][IT_TS][TS_ALSENAR]/100)
		MaItArred(nItem,{"IT_VLSENAR"})
	Endif
Endif

Return