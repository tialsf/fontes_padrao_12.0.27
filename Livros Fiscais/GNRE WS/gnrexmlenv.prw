#include "protheus.ch"
#include "tbiconn.ch"


//-----------------------------------------------------------------------
/*/{Protheus.doc} gnreXMLEnv
Fun��o que monta o XML �nico de envio para GNRE ao TSS.

@author Simone dos Santos de Oliveira
@since 24.06.2015
@version 12.25

@return	cString		Tag montada em forma de string.
/*/
//-----------------------------------------------------------------------
user function GnreXMLEnv( cAliasSF6 )
local cId			:= ''
local cString		:= ''
local cUf			:= ''
local cMVUFGNWS		:= GetNewPar('MV_UFGNWS' ,'') //Apenas as UF's que possuem GNRE Web Service.
Local cLote			:= ""
Local lLote			:= .F.
Local cTotal		:= 0
Local dDTPgto		:= CTOD("  /  /    ")

default cAliasSF6:= ( PARAMIXB[1] )

Private cVersao		:= GetMv('MV_GNREVE' ,,'1.00')

If IsInCallStack("FISA214")
	lLote	:= .T.
	cLote	:= "L"
ENdIf

cUF			:= IIF( lLote , alltrim((cAliasSF6)->CIB_EST) , alltrim((cAliasSF6)->F6_EST))
cNumGNRE	:= IIF( lLote , alltrim((cAliasSF6)->CIB_ID)  , alltrim((cAliasSF6)->F6_NUMERO))

//Tratamento para ID
cId	 := cLote + cUF + cNumGNRE

//Cabe�alho XML
cString := '<gnre id="gnre:' +  cNumGNRE  + '" tssversao="2.00">'
cString += '<versaoGuia>' + cVersao + '</versaoGuia>'
If cVersao >= '2.00'
	cString += '<dadosGNRE>'
	cString += '<tipoGnre>'+TpGNRE(cAliasSF6,lLote)+'</tipoGnre>'
	cString	+= '<uf>' + cUF + '</uf>'
	cString	+= '<numerognre>' +  cNumGNRE  + '</numerognre>'
	cString	+= Emitente(cUF)
	cString += '<itensGNRE>'

EndIf

If cUF $ cMVUFGNWS
	cTotal	:= TotGNRE(cAliasSF6,lLote)
	dDTPgto	:= DTPgto(cAliasSF6,lLote)
	If lLote
		// Itens via FISA214 2.00
		While (cAliasSF6)->(!Eof())
			cString += GeraItem( cAliasSF6 , lLote  )
			(cAliasSF6)->(DbSkip())
		End
	Else
		// Itens via FISA095 1.00 E 2.00
		cString +=  GeraItem( cAliasSF6 , lLote  )
	EndIf
EndIf

If cVersao >= '2.00'
	cString += '</itensGNRE>'
	cString += '<valorGNRE>' + AllTrim(ConvType( cTotal ,15,2))  + '</valorGNRE>'
	cString += '<pagamento>' + AllTrim(DTOS( dDTPgto  ) ) + '</pagamento>'
	cString += '</dadosGNRE>'
EndIf

cString += '</gnre>'
cString := IIf(!empty(cString),encodeUTF8(cString ), "")

return ({cId,cString})

//-----------------------------------------------------------------------
/*/{Protheus.doc} RetDest
Fun��o retorna o Destinat�rio da GNRE.

@author Raphael Augustos
@since 17.10.2019
@version 12.25

@return	aDest		Array com informa��es do Destinat�rio.
/*/
//-----------------------------------------------------------------------
static function RetDest(cCliFor, cLjCliFor, cOper, cTpDoc, cAliasSF6)

local aReturn		:= {}
//local cTpPessoa		:= ""
local cCpfCnpj		:= ""
local cInsEst		:= ""
local cRzSocial		:= ""
local cCodMun		:= ""
Local cDescMun		:= ""


default cCliFor		:= ""
default cLjCliFor	:= ""
default cOper		:= ""


if cOper == '2' .And. !cTpDoc $ 'B|D' 	//Sa�da
	dbselectarea ('SA1')				//Cadastro do Cliente
	SA1->(dbsetorder (1))
	SA1->(dbseek(xFilial('SA1')+cCliFor+cLjCliFor))

	cCpfCnpj	:= alltrim( SA1->A1_CGC )
	cInsEst		:= iif(!empty(SA1->A1_INSCR) .And. alltrim(SA1->A1_INSCR)<>'ISENTO',ConvType(VldIE(SA1->A1_INSCR,.F.,.F.)),'')
	cRzSocial	:= Alltrim( SA1->A1_NOME )
	cCodMun		:= alltrim( SA1->A1_COD_MUN )
	cDescMun	:= alltrim( SA1->A1_MUN )
else
	dbselectarea ('SA2')				//Cadastro do Cliente
	SA2->(dbsetorder (1))
	SA2->(dbseek(xFilial('SA2')+(cAliasSF6)->F6_CLIFOR+(cAliasSF6)->F6_LOJA))

	cCpfCnpj	:= alltrim( SA2->A2_CGC )
	cInsEst		:= iif(!empty(SA2->A2_INSCR) .And. alltrim(SA2->A2_INSCR)<>'ISENTO',ConvType(VldIE(SA2->A2_INSCR,.F.,.F.)),'')
	cRzSocial	:= alltrim( SA2->A2_NOME )
	cCodMun		:= alltrim( SA2->A2_COD_MUN )
	cDescMun	:= alltrim( SA2->A2_MUN )
endif

//Preenchimento do Array
aadd(aReturn,{ cCpfCnpj,;
				 cInsEst,;
				 cRzSocial,;
				 cCodMun,;
				 cDescMun})

return aReturn
//-----------------------------------------------------------------------
/*/{Protheus.doc} ConvType
@author Simone dos Santos de Oliveira
@since 17.10.2019
@version 12.25
/*/
//-----------------------------------------------------------------------
static function ConvType(xValor,nTam,nDec)

local cNovo 	:= ''

default nDec 	:= 0

do case
	case valtype(xValor)=='N'
		if xValor <> 0
			cNovo := AllTrim(Str(xValor,nTam,nDec))
		else
			cNovo := '0'
		endif
	case valtype(xValor)== 'D'
		cNovo := FsDateConv(xValor,'YYYYMMDD')
		cNovo := substr(cNovo,1,4)+'-'+substr(cNovo,5,2)+'-'+substr(cNovo,7)
	case valtype(xValor)=='C'
		if nTam == nil
			xValor := AllTrim(xValor)
		endif

		default nTam := 60

		cNovo := AllTrim(EnCodeUtf8(NoAcento(substr(xValor,1,nTam))))
endcase

return(cNovo)

//-----------------------------------------------------------------------
/*/{Protheus.doc} RetCampAdic
Fun��o retorna as informa��es extras referente � gnre.
@author Simone dos Santos de Oliveira
@since 03/03/2016
@version 12.25
/*/
//-----------------------------------------------------------------------
static function RetCampAdic( cUF, cCodRec )

local aInfExtra		:= {}
local cWhere		:= ''
local cFiltro		:= ''
local cIndex		:= ''
local cAmbiente		:= alltrim(GetMv('MV_AMBGNRE',,'2'))
local cAliasF0N		:= 'F0N'
local nX			:= 0

default cUF			:= ''
default cCodRec		:= ''


if !( empty(cUF) .and. empty(cCodRec) .and. empty(cAmbiente) )

	dbselectarea('F0N')
	F0N->(dbsetorder(1))

	#IFDEF TOP

		if (TcSrvType ()<>"AS/400")
			lQuery    := .T.
			cAliasF0N := GetNextAlias()

			cWhere := "%"
			cWhere += "F0N.F0N_FILIAL = '"+xFilial ("F0N")+"' AND"
			cWhere += " F0N.F0N_UF = '"+ cUF +"' AND F0N.F0N_CODREC = '"+ cCodRec +"' "
			cWhere += " AND F0N.F0N_AMBWS = '" + cAmbiente + "' "
			cWhere += "AND F0N.D_E_L_E_T_ = '' "
			cWhere += "%"


			BeginSql Alias cAliasF0N
				SELECT * FROM %Table:F0N% F0N WHERE %Exp:cWhere% ORDER BY %Order:F0N%
			EndSql

		else
	#EndIf
			cIndex  := CriaTrab(NIL,.F.)
			cFiltro := 'F0N_FILIAL=="'+xFilial ("F0N")+'".And.'
			cFiltro += 'F0N_UF =="'+ cUF +'".And. F0N_CODREC =="'+ cCodRec +'" '
			cFiltro += '.And. F0N_AMBWS == "'+cAmbiente+'" '
			indregua (cAliasF0N, cIndex, F0N->(IndexKey ()),, cFiltro)
			nIndex := retindex(cAliasF0N)
			#IFNDEF TOP
				dbSetIndex(cIndex+OrdBagExt())
			#ENDIF
			dbSelectArea (cAliasF0N)
			dbSetOrder (nIndex+1)
	#IFDEF TOP
		endif
	#EndIf

dbSelectArea (cAliasF0N)
(cAliasF0N)->(dbGoTop ())

	while !(cAliasF0N)->(eof ())

		aadd(aInfExtra,{})
		nX := len(aInfExtra)

		aadd(aInfExtra[nX],(cAliasF0N)->F0N_CODSEF)
		aadd(aInfExtra[nX],(cAliasF0N)->F0N_TIPO)
		aadd(aInfExtra[nX],(cAliasF0N)->F0N_CODINT)
		aadd(aInfExtra[nX],(cAliasF0N)->F0N_OBRIGA)
		aadd(aInfExtra[nX],(cAliasF0N)->F0N_TITULO)
		aadd(aInfExtra[nX],(cAliasF0N)->F0N_CODINT)

		(cAliasF0N)->(dbSkip())
	enddo
endif

#IFDEF TOP
	dbSelectArea(cAliasF0N)
	dbCloseArea()
#ELSE
	dbSelectArea(cAliasF0N)
	retindex(cAliasF0N)
	ferase(nIndex+OrdBagExt())
#ENDIF

return aInfExtra

//-----------------------------------------------------------------------
/*/{Protheus.doc} RetVlrAdic
Fun��o retorna o campo conforme C�digo interno apresentado na tabela F0N

@author Simone dos Santos de Oliveira
@since 03/03/2016
@version 12.25

/*/
//-----------------------------------------------------------------------
static function RetVlrAdic( cCodInterno, cAliasSF6 )

local cValAdic	:= ''
local cCampo		:= ''

default cCodInterno := ''

if ! empty(cCodInterno)

	do case
   		case cCodInterno $ 'OBS' 												//Observa��o
			cValAdic	:= (cAliasSF6)->(FieldGet(FieldPos('F6_OBSERV')))

		case cCodInterno $ 'INF' 												//Informa��o Complementar
			cValAdic	:= (cAliasSF6)->(FieldGet(FieldPos('F6_INF')))

		case cCodInterno $ 'CHV' 												//Chave NF-e / DF-e / Ct-e
			cValAdic	:= SF3->(FieldGet(FieldPos('F3_CHVNFE')))

		case cCodInterno $ 'DEM#DSA' 											//Data de Emiss�o NF#Data Sa�da
			cValAdic	:= SF3->(FieldGet(FieldPos('F3_EMISSAO')))
			cValAdic	:= iif(! empty(cValAdic), dtos(cValAdic), '')

		case cCodInterno $ 'DET'  												//Detalhamento da Receita
			cValAdic	:= (cAliasSF6)->(FieldGet(FieldPos('F6_DETRECE')))

		case cCodInterno $ 'NNF'  												//Num. NF
			cValAdic	:= SF3->(FieldGet(FieldPos('F3_NFISCAL')))

		case cCodInterno $ 'ATM'  												//Atualiza��o Monet�ria
			cValAdic	:= (cAliasSF6)->(FieldGet(FieldPos('F6_ATMON')))
			cValAdic	:= ConvType(cValAdic,15,2)

		case cCodInterno $ 'NRE' 		 										//Nome Remetente
			cValAdic	:= SM0->(FieldGet(FieldPos('M0_NOMECOM')))

		case cCodInterno $ 'CNP'  												//CNPJ Remetente
			cValAdic	:= SM0->(FieldGet(FieldPos('M0_CGC')))

		case cCodInterno $ 'JRS'  												//Juros
			cValAdic	:= (cAliasSF6)->(FieldGet(FieldPos('F6_JUROS')))
			cValAdic	:= ConvType(cValAdic,15,2)

		case cCodInterno $ 'MLT'  												//Multa
			cValAdic	:= (cAliasSF6)->(FieldGet(FieldPos('F6_MULTA')))
			cValAdic	:= ConvType(cValAdic,15,2)

		case cCodInterno $ 'MOR'  												//Municipio de Origem
			cValAdic	:= SM0->(FieldGet(FieldPos('M0_CODMUN')))

		case cCodInterno $ 'CRG#CNA#CIN#DES#GST#MCR#PCA#PTS#VLD'				//Carga#Cnae#Conhec. Interno#Cnae#Dt Desembara�o#Guia ST#Manif. de Carga#Placa Caminh�o#Prot. de caminh�o#Valor Aduaneiro
			cCampo		:= RetNwCmp( cCodInterno )
			cValAdic	:= iif(! empty(cCampo), (cAliasSF6)->(FieldGet(FieldPos(cCampo))),'')

		otherwise
			cValAdic	:= ' '

	endCase
endif

return cValAdic

//-----------------------------------------------------------------------
/*/{Protheus.doc} RetNwCmp
Fun��o retorna campo que n�o � tratado por padr�o no sistema

@author Simone dos Santos de Oliveira
@since 03/03/2016
@version 12.25

/*/
//-----------------------------------------------------------------------
static function RetNwCmp( cCdInt )

local cValAdc		:= ''
local cMVNWCODGN	:= alltrim(GetNewPar('MV_NWCODGN',' '))

default cCdInt	:= ''

if !(empty( cCdInt ) .and. empty( cMVNWCODGN )) .and. ( cCdInt $ cMVNWCODGN)

	cValAdc := substr(cMVNWCODGN,at(cCdInt,cMVNWCODGN),at('/',cMVNWCODGN)-1)
	cValAdc := substr(cValAdc,at('F6',cValAdc),len(cValAdc))

endif

return cValAdc

//-----------------------------------------------------------------------
/*/{Protheus.doc} RetTpDoc
Fun��o retorna tipo de documento de origem

@author Simone dos Santos de Oliveira
@since 06/04/2016
@version 12.25

/*/
//-----------------------------------------------------------------------
static function RetTpDoc( cEspecie , cUF , cCodRec)

local cTipoDoc	:= ''

default cEspecie	:= ''
default cUF			:= ''
default cCodRec		:= ''

if ! empty( cEspecie )
	do case
		case cUF == "SC" .And. cCodRec == "100030"
			cTipoDoc := '10'
		case cUF == "PR" .And. cCodRec == "100099"
			cTipoDoc := '10'
		case cUF == "PE" .And. cCodRec == "100099"
			cTipoDoc := '22'
		case alltrim( cEspecie )== "NFA"
			cTipoDoc := '01'
		case Alltrim( cEspecie )$ "NF/SPED/NTST/NFCEE"
			cTipoDoc := '10'
		case Alltrim( cEspecie )== "CA"
			cTipoDoc := '08'
		case Alltrim( cEspecie )$ "CTR/CTE"
			cTipoDoc := '07'
		OtherWise
			cTipoDoc := ''
	endcase
endif

return cTipoDoc

//-----------------------------------------------------------------------
/*/{Protheus.doc} Emitente
Gera��o do conjunto emitente
@author Raphael Augustos
@since 10/10/2019
@version 12.25
/*/
//-----------------------------------------------------------------------
Static Function Emitente(cUF)
Local cString	:= ""
Local cInsc		:= ""
Local cEmail	:= ""
Local cFoneEmi	:= ""
Local aTelEmi	:= {}

Default cUF		:= ""
//Emitente
cInsc 	 := IESubTrib(cUF)
cEmail	 := GetNewPar("MV_EMAILGN","")

aTelEmi	 := FisGetTel(SM0->M0_TEL)
cFoneEmi := iif(aTelEmi[2] > 0,ConvType(aTelEmi[2],3),"") // C�digo da �rea
cFoneEmi += iif(aTelEmi[3] > 0,ConvType(aTelEmi[3],9),"") // C�digo do Telefone

cString	+= '<emitente>'

cString	+= '<cnpjcpf>' + SM0->M0_CGC + '</cnpjcpf>'
cString	+= '<nome>' + ConvType(SM0->M0_NOMECOM) + '</nome>'
cString	+= '<ie>' + cInsc + '</ie>'
cString	+= '<endereco>' + ConvType(SM0->M0_ENDENT) + '</endereco>'
cString	+= '<municipio>' + alltrim(SM0->M0_CODMUN) + '</municipio>'
cString	+= '<descmun>' + alltrim(SM0->M0_CIDENT) + '</descmun>'
cString	+= '<uf>' + alltrim(SM0->M0_ESTENT) + '</uf>'
cString	+= '<cep>' + allTrim(SM0->M0_CEPENT) + '</cep>'
cString	+= '<telefone>' + cFoneEmi + '</telefone>'
cString	+= '<email>' + alltrim(cEmail) + '</email>'
cString	+= '<inscufavorecida>' + iif(!empty(cInsc), "1","2") + '</inscufavorecida>' // Indica se tem ou n�o IE na UF favorecida para utiliza��o do TSS

cString	+= '</emitente>'
Return cString

//-----------------------------------------------------------------------
/*/{Protheus.doc} Valores
Gera��o do conjunto Valores
@author Raphael Augustos
@since 10/10/2019
@version 12.25
/*/
//-----------------------------------------------------------------------
Static Function Valores(cAliasSF6,nQtd)
Local cString		:= ""
Local nValPrinc		:= 0
Local nValTotal		:= 0
Default nQtd		:= 0

nValPrinc := (cAliasSF6)->F6_VALOR
nValTotal := (cAliasSF6)->F6_VALOR + (cAliasSF6)->F6_ATMON + (cAliasSF6)->F6_JUROS + (cAliasSF6)->F6_MULTA

cString	+= '<valores>'
cString	+= '<atumonetaria tipo="51">'+ ConvType((cAliasSF6)->F6_ATMON,15,2)  + '</atumonetaria>'
cString	+= '<juros tipo="41">' + ConvType((cAliasSF6)->F6_JUROS,15,2) + '</juros>'
cString	+= '<multa tipo="31">' + ConvType((cAliasSF6)->F6_MULTA,15,2) + '</multa>'
cString	+= '<valordeducao>' + ConvType((cAliasSF6)->F6_VIMPDED,15,2) + '</valordeducao>'
cString	+= '<principal tipo="11">' + ConvType(nValPrinc,15,2) + '</principal>'
cString	+= '<total tipo="21">' + ConvType(nValTotal,15,2) + '</total>'
cString	+= '<atualizacao>' + Iif(nValTotal-nValPrinc > 0,ConvType(nValTotal,15,2),ConvType(nValTotal-nValPrinc,15,2)) + '</atualizacao>'
cString	+= '<qtde>' + ConvType(nQtd,15,2) + '</qtde>'
cString	+= '</valores>'
Return cString

//-----------------------------------------------------------------------
/*/{Protheus.doc} Destinatario
Gera��o do conjunto Destinatario
@author Raphael Augustos
@since 10/10/2019
@version 12.25
/*/
//-----------------------------------------------------------------------
Static Function Destinatario(aDestinat)
Local cString		:= ""
Local cCpfCnpj 		:= ""
Local cInscDest 	:= ""
Local cNomeDest 	:= ""
Local cMunDest 		:= ""
Local cDescMunDs 	:= ""
Local cCodArea		:= ""
Default aDestinat	:= {}

If len(aDestinat) > 0
	cCpfCnpj	:= allTrim(aDestinat[1,1])
	cInscDest	:= allTrim(aDestinat[1,2])
	cNomeDest	:= allTrim(aDestinat[1,3])
	cMunDest	:= allTrim(aDestinat[1,4])
	cDescMunDs	:= allTrim(aDestinat[1,5])
	cCodArea	:= SubStr(allTrim(aDestinat[1,4]),1,2)
EndIf

cString	+= '<destinatario>'
cString	+= '<cnpjcpf>' + cCpfCnpj + '</cnpjcpf>'
cString	+= '<ie>' + cInscDest + '</ie>'
cString	+= '<nome>' + ConvType(cNomeDest) + '</nome>'
cString	+= '<municipio>' + cMunDest + '</municipio>'
cString	+= '<descmun>' + cDescMunDs + '</descmun>'
cString	+= '<inscufavorecida>' + iif(!empty(cInscDest), "1","2") + '</inscufavorecida>' // Indica se tem ou n�o IE na UF favorecida para utiliza��o do TSS
cString	+= '</destinatario>'

Return cString

//-----------------------------------------------------------------------
/*/{Protheus.doc} Referencia
Gera��o do conjunto Referencia
@author Raphael Augustos
@since 10/10/2019
@version 12.25
/*/
//-----------------------------------------------------------------------
Static Function Referencia(cAliasSF6)
Local cString	:= ""

cString	+= '<referencia>'
cString	+= '<periodo>' + iif(! empty((cAliasSF6)->F6_REF) .And. (cAliasSF6)->F6_REF$"1","0","") + '</periodo>'
cString	+= '<mes>' + strzero((cAliasSF6)->F6_MESREF,2) + '</mes>'
cString	+= '<ano>' + cvaltochar((cAliasSF6)->F6_ANOREF) + '</ano>'
cString	+= '<parcela>1</parcela>'
cString	+= '</referencia>'
Return cString

//-----------------------------------------------------------------------
/*/{Protheus.doc} Transporte
Gera��o do conjunto Transporte
@author Raphael Augustos
@since 10/10/2019
@version 12.25
/*/
//-----------------------------------------------------------------------
Static Function Transporte(cAliasSF6)
Local cString	:= ""

cString	+= '<transporte>'
cString	+= '<manifcarga></manifcarga>'
cString	+= '<cti></cti>'
cString	+= '<dtdesmbaraco></dtdesmbaraco>'
cString	+= '<manifcarga></manifcarga>'
cString	+= '<valoradua></valoradua>'
cString	+= '</transporte>'
Return cString

//-----------------------------------------------------------------------
/*/{Protheus.doc} Sintegra
Gera��o do conjunto Sintegra
@author Raphael Augustos
@since 10/10/2019
@version 12.25
/*/
//-----------------------------------------------------------------------
Static Function Sintegra(cAliasSF6)
Local cString	:= ""

cString	+= '<sintegra>'
cString	+= '<protocoloTED></protocoloTED>'
cString	+= '<justificativa></justificativa>'
cString	+= '</sintegra>'
Return cString

//-----------------------------------------------------------------------
/*/{Protheus.doc} Campoadic
Gera��o do conjunto Campoadic
@author Simone dos Santos de Oliveira
@since 10/10/2019
@version 12.25
/*/
//-----------------------------------------------------------------------
Static Function Campoadic(cAliasSF6)
Local cString	:= ""
Local cExtra	:= ""
Local lGera		:= .T.
Local lObriga	:= .F.
Local nX		:= 1
Local aInfAdic	:= {}

aInfAdic := RetCampAdic( AllTrim((cAliasSF6)->F6_EST), AllTrim((cAliasSF6)->F6_CODREC) )

for nX:= 1 to len( aInfAdic )

	cCodAdic := alltrim(aInfAdic [nX,1])
	cTpAdic  := alltrim(aInfAdic [nX,2])
	cValor   := RetVlrAdic( aInfAdic [nX,3], cAliasSF6 )
	lObriga  := iif( aInfAdic [nX,4]$'1S', .T., .F. )
	cTitulo  := alltrim(upper(aInfAdic [nX,5]))
	cCodInt  := alltrim(aInfAdic [nX,6])

	if lObriga .or. ! empty( cValor )

		//Tratamento quando for do tipo Data
		if cTpAdic $ 'D'
			cValor := substr(cValor,1,4)+ '-' + substr(cValor,5,2) + '-' + substr(cValor,7,2)
		endif

		//Tratamento para considerar apenas uma Chave quando h� mais de uma Chave cadastrada na tabela F0N
		if cCodInt == 'CHV'

			//Tratamento quando a esp�cie � SPED
			cEspecie	:= iif( cEspecie == 'SPED', 'NFE', cEspecie)

			if ! empty(cEspecie)
				if ! (cEspecie $  cTitulo .or. ( cEspecie == 'NFE' .and. cTitulo $ 'CHAVE DE ACESSO|CHAVE DA NOTA FISCAL ELETRONICA' ))
					lGera := .f.
				endif
			endif

		endif

		if lGera
			cExtra	+= '<campoadic>'
			cExtra	+= '<cod>' +  cCodAdic + '</cod>'
			cExtra	+= '<tipo>' + cTpAdic +'</tipo>'
			cExtra	+= '<valor>' + cValor + '</valor>'
			cExtra	+= '</campoadic>'
		endif
	endif

next

If ! empty( cExtra )
	cString	+= '<camposadic>'
		cString += cExtra
	cString	+= '</camposadic>'
EndIf
Return cString

//-----------------------------------------------------------------------
/*/{Protheus.doc} IdentGNRE
Gera��o do conjunto IdentGNRE
@author Raphael Augustos
@since 10/10/2019
@version 12.25
/*/
//-----------------------------------------------------------------------
Static Function IdentGNRE(cAliasSF6,nQtd,aDestinat)
Local cString	 := ""
Local cCodRec	 := ""
Local cUF		 := ""
Local cNumGNRE	 := alltrim((cAliasSF6)->F6_NUMERO)
Local lVerTagRJ	 := .F.
Local cPerApur	 := AllTrim((cAliasSF6)->F6_REF)
Local cTpDocOrig := ""
Local cCpfCnpj   := ""
Local cEmissNF   := ""
Local cNumeroNF  := ""
Local cSerieNf   := ""
Local cTipoNf    := ""
Local cChvNFe    := ""
Local cCodArea   := ""
Local cProduto	 := ""

//C�digo Receita
cCodRec	:= alltrim((cAliasSF6)->F6_CODREC)
cUF		:= alltrim((cAliasSF6)->F6_EST)
//Para RJ e Receita 100099 -> exceto para natureza = 4 - Substituicao Tributaria por Responsabilidade.
lVerTagRJ := cUF=="RJ".And.cCodRec$"100099"
cProduto := iif(! empty((cAliasSF6)->F6_CODPROD), cValtoChar((cAliasSF6)->F6_CODPROD),"")

cString	+= '<identgnre>'
cString	+= '<uf>' + cUF + '</uf>'
cString	+= '<numerognre>' +  cNumGNRE  + '</numerognre>'
cString	+= '<receita>' + cCodRec + '</receita>'
cString	+= '<detreceita>' + allTrim((cAliasSF6)->F6_DETRECE) + '</detreceita>'
cString	+= '<produto>' + cProduto + '</produto>'
cString	+= '<vencimento>' + dtos((cAliasSF6)->F6_DTVENC)  + '</vencimento>'
cString	+= '<convenio>' + allTrim((cAliasSF6)->F6_NUMCONV) + '</convenio>'
cString	+= '<pagamento>' + dtos((cAliasSF6)->F6_DTPAGTO) + '</pagamento>'
cString	+= Iif(lVerTagRJ,'','<fatogerador>' + Iif(valtype((cAliasSF6)->F6_DTARREC)=="D",dtos((cAliasSF6)->F6_DTARREC),(cAliasSF6)->F6_DTARREC) + '</fatogerador>')
cString	+= '<tipoperiodoapur>' + Iif( !Empty(cPerApur) , cPerApur , "0"  ) + '</tipoperiodoapur>'
cString	+= Iif(lVerTagRJ,'','<mesref>'+ ConvType((cAliasSF6)->F6_MESREF) + '</mesref>')
cString	+= Iif(lVerTagRJ,'','<anoref>' + ConvType((cAliasSF6)->F6_ANOREF) + '</anoref>')
cString	+= '<decref></decref>'
cString	+= '<observacoes>' + ConvType((cAliasSF6)->F6_OBSERV)+ '</observacoes>'
cString	+= '<informacoes>' + ConvType((cAliasSF6)->F6_INF) + '</informacoes>'
cString	+= '<infcompl>' + ConvType((cAliasSF6)->F6_DESCOMP) + '</infcompl>'
cString	+= '<dtsaimerc></dtsaimerc>'
cString	+= '<diavencimento></diavencimento>'
cString	+= '<tipoimport></tipoimport>'

//Demais informa��es GNRE
cString	+= '<banco>'+ AllTrim((cAliasSF6)->F6_BANCO) +'</banco>'
cString	+= '<agencia>' + AllTrim((cAliasSF6)->F6_AGENCIA) + '</agencia>'
cString	+= '<classevcto>' + AllTrim((cAliasSF6)->F6_CLAVENC) + '</classevcto>'
cString	+= '<cnpjcontrib>' + AllTrim((cAliasSF6)->F6_CNPJ) + '</cnpjcontrib>'
cString	+= '<vencaut>' + AllTrim((cAliasSF6)->F6_VENCAUT) + '</vencaut>'
cString	+= '<docorigem>' + AllTrim((cAliasSF6)->F6_DOCOR) + '</docorigem>'
cString	+= '<autentbanc>' + AllTrim((cAliasSF6)->F6_AUTENT) + '</autentbanc>'
cString	+= '<numproc>' + AllTrim((cAliasSF6)->F6_NUMPROC) + '</numproc>'
cString	+= '<indproc>' + AllTrim((cAliasSF6)->F6_INDPROC) + '</indproc>'
cString	+= '<pedidodeducao>' + AllTrim((cAliasSF6)->F6_PEDDED) + '</pedidodeducao>'
cString	+= '<issor>' + AllTrim((cAliasSF6)->F6_ISSOR) + '</issor>'
cString	+= '<codmuniss>' + AllTrim((cAliasSF6)->F6_CODMUN) + '</codmuniss>'


//Nota Fiscal - Quando for por opera��o
If !( empty((cAliasSF6)->F6_DOC) .And. empty((cAliasSF6)->F6_SERIE) .And. empty((cAliasSF6)->F6_CLIFOR) .And. empty((cAliasSF6)->F6_LOJA) )

	dbselectarea("SF3") //Livros Fiscais
	SF3->(dbsetorder(4)) //F3_FILIAL+F3_CLIEFOR+F3_LOJA+F3_NFISCAL+F3_SERIE

	If SF3->(dbseek(xFilial("SF3")+ (cAliasSF6)->F6_CLIFOR + (cAliasSF6)->F6_LOJA + (cAliasSF6)->F6_DOC + (cAliasSF6)->F6_SERIE))

		cEmissNF	:= dtos(SF3->F3_EMISSAO)
		cNumeroNF	:= alltrim(SF3->F3_NFISCAL)
		cSerieNf	:= alltrim(SF3->F3_SERIE)
		cEspecie	:= alltrim(SF3->F3_ESPECIE)
		cTipoNf	:= iif(cEspecie=="SPED","NF-e","M")
		cChvNFe	:= alltrim(SF3->F3_CHVNFE)

		//Tipo Doc Origem
		cTpDocOrig :=	RetTpDoc( alltrim(SF3->F3_ESPECIE),cUF,cCodRec)
		cTpDocOrig := iif(! empty( cTpDocOrig ), cTpDocOrig,(cAliasSF6)->F6_TIPODOC)

		//Informa��es Fornecedor / Cliente
		IF SUBSTR(SF3->F3_CFO,1,1 )$ "123"//Fornecedor
			aDestinat := RetDest(SF3->F3_CLIEFOR, SF3->F3_LOJA, (cAliasSF6)->F6_OPERNF, (cAliasSF6)->F6_TIPODOC, cAliasSF6)
		ELSE//Cliente. Neste caso � usado cliente entrega para compor a parte de destinat�rio
			aDestinat := RetDest(SF3->F3_CLIENT, SF3->F3_LOJENT, (cAliasSF6)->F6_OPERNF, (cAliasSF6)->F6_TIPODOC, cAliasSF6)
		ENDIF

		If len(aDestinat) > 0
			cCpfCnpj	:= allTrim(aDestinat[1,1])
			cInscDest	:= allTrim(aDestinat[1,2])
			cNomeDest	:= allTrim(aDestinat[1,3])
			cMunDest	:= allTrim(aDestinat[1,4])
			cDescMunDs	:= allTrim(aDestinat[1,5])
			cCodArea	:= SubStr(allTrim(aDestinat[1,4]),1,2)
		EndIf

		DbSelectArea("SFT")
		SFT->(DbSetOrder(1))

		If SFT->(DbSeek(xFilial("SFT")+Iif((cAliasSF6)->F6_OPERNF == '1','E','S')+SF3->F3_SERIE+SF3->F3_NFISCAL+SF3->F3_CLIEFOR+SF3->F3_LOJA))
			nQtd	:= SFT->FT_QUANT
		EndIf
	EndIf
EndIf

If (cTpDocOrig == '10' .Or. (cTpDocOrig == '22' .And. cUF == 'PE')) .And. cVersao >= '2.00'
	cString	+= '<docorig>' + cChvNFe + '</docorig>'
Else
	cString	+= '<docorig>' + allTrim((cAliasSF6)->F6_DOC) + '</docorig>'
EndIf

cString	+= '<tipodocorig>' + allTrim(cTpDocOrig) + '</tipodocorig>'
cString	+= '<cnpjcpfnf>'+ cCpfCnpj + '</cnpjcpfnf>'
cString	+= '<dataemissaonf>'+ cEmissNF + '</dataemissaonf>'
cString	+= '<numeronf>'+ cNumeroNF + '</numeronf>'
cString	+= '<serienf>'+ cSerieNf + '</serienf>'
cString	+= '<tiponf>'+ cTipoNf + '</tiponf>'
cString	+= '<chavenf>'+ cChvNFe + '</chavenf>'
cString	+= '<codarea>'+ cCodArea + '</codarea>'
cString	+= '</identgnre>'

Return cString

//-----------------------------------------------------------------------
/*/{Protheus.doc} GeraItem
Fun��o respons�vel por gerar o corpo do XML da GNRE.
@author Raphael Augustos Ferreira
@since 17.10.2019
@version 12.25
/*/
//-----------------------------------------------------------------------
Static Function GeraItem( cAliasSF6, lLote)
Local cString 	 := ""
Local nQtd		 :=  0
Local cVersao	 := GetMv('MV_GNREVE' ,,'1.00')
Local aDestinat	 := {}
Private cEspecie := ""


If cVersao >= "2.00"
	cString += '<item>'
EndIf

cString	+= IdentGNRE(cAliasSF6,@nQtd,@aDestinat)
// Emitente
If cVersao == "1.00"
	cString	+= Emitente( (cAliasSF6)->F6_EST )
EndIf
//Valores
nValPrinc := (cAliasSF6)->F6_VALOR
nValTotal := (cAliasSF6)->F6_VALOR + (cAliasSF6)->F6_ATMON + (cAliasSF6)->F6_JUROS + (cAliasSF6)->F6_MULTA
cString	+= Valores(cAliasSF6,nQtd)
//Destinat�rio
cString	+= Destinatario(aDestinat)
//Referencia
cString	+= Referencia(cAliasSF6)
//Transporte
cString	+= Transporte(cAliasSF6)
//Sintegra
cString	+= Sintegra(cAliasSF6)
//Campos Adicionais
cString	+= Campoadic(cAliasSF6)

If cVersao >= "2.00"
	cString += '</item>'
EndIf

Return cString

//-----------------------------------------------------------------------
/*/{Protheus.doc} TpGNRE
Retorna o tipo da guia
@author Raphael Augustos Ferreira
@since 17.10.2019
@version 12.25
/*/
//-----------------------------------------------------------------------
Static Function TpGNRE(cAliasSF6,lLote)
Local cRet	:= "0"

If lLote
	cRet	:= (cAliasSF6)->CIB_TPGNRE
EndIf
Return cRet

//-----------------------------------------------------------------------
/*/{Protheus.doc} TotGNRE
Retorna o Total da GNRE na vers�o 2.0
@author Raphael Augustos Ferreira
@since 17.10.2019
@version 12.25
/*/
//-----------------------------------------------------------------------
Static Function TotGNRE(cAliasSF6,lLote)
Local nValor 	:= nValTotal := (cAliasSF6)->F6_VALOR + (cAliasSF6)->F6_ATMON + (cAliasSF6)->F6_JUROS + (cAliasSF6)->F6_MULTA

If lLote
	nValor	:= (cAliasSF6)->CIB_VALOR + (cAliasSF6)->CIB_ATUMON + (cAliasSF6)->CIB_MULTA  + (cAliasSF6)->CIB_JUROS
EndIf
Return nValor

//-----------------------------------------------------------------------
/*/{Protheus.doc} DtPgto
Retorna a Data de Pagamento da GNRE 2.0
@author Raphael Augustos Ferreira
@since 17.10.2019
@version 12.25
/*/
//-----------------------------------------------------------------------
Static Function DtPgto(cAliasSF6,lLote)
Local dData 	:= (cAliasSF6)->F6_DTPAGTO

If lLote
	dData	:= (cAliasSF6)->CIB_DTPGTO
EndIf
Return dData



