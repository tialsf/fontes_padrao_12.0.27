#INCLUDE "protheus.ch"
#INCLUDE "fisr057.ch"

Static cPerg := "FISR057"

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} FISR057
Emite o relatorio do tipo TReport das notas fiscais eletronicas do Uruguai
@author  MICROSIGA
@version P10
@since 	 17/01/2014
@return 
/*/
//-------------------------------------------------------------------------------------
User Function FISR057()
	Local oReport			:= Nil	

	Private cIvaMinTP		:= ""
	Private cIvaBasTP		:= ""
	Private cSusTP			:= ""
	Private cExpTP			:= ""
	Private cIseTP			:= ""
	Private nIvaMinCmp		:= 0
	Private nIvaBasCmp		:= 0
	Private nSuspenso		:= 0
	Private nExportacao		:= 0
	Private nIsento			:= 0

	
	Private cIvaMinCmp		:= ""
	Private cIvaBasCmp		:= ""
	Private cBaseMinCmp		:= ""
	Private cBaseBasCmp		:= ""	
	Private cIvaSus			:= "" 
	Private cBaseSus		:= ""
	Private cIvaExp			:= "" 
	Private cBaseEXP		:= ""	
	Private cIvaIse			:= "" 
	Private cBaseIse		:= "" 
	
	
	AjustaSX()							//Cria as perguntas no SX1
	If Pergunte(cPerg,.T.)	  			//Exibe ao usuario a dialog de perguntas SX1	
	    
		//Obtem os nomes dos campos do IVA
		nIvaMinCmp  := GetIvaField(MV_PAR07) 
		cIvaMinTP   := GetIvaTBclass(MV_PAR07)
		nIvaBasCmp  := GetIvaField(MV_PAR08)
		cIvaBasTP	:= GetIvaTBclass(MV_PAR08) 	
		nSuspenso   := GetIvaField(MV_PAR09)
		cSusTP		:= GetIvaTBclass(MV_PAR09)
		nExportacao := GetIvaField(MV_PAR10)
		cExpTP		:= GetIvaTBclass(MV_PAR10)
		nIsento 	:= GetIvaField(MV_PAR11)
		cIseTP		:= GetIvaTBclass(MV_PAR11)
		
		cIvaMinCmp 	:= "_VALIMP" + alltrim(str(nIvaMinCmp))
		cBaseMinCmp	:= "_BASIMP" + alltrim(str(nIvaMinCmp))		
		cIvaBasCmp 	:= "_VALIMP" + alltrim(str(nIvaBasCmp))
		cBaseBasCmp	:= "_BASIMP" + alltrim(str(nIvaBasCmp))		
		cIvaSus 	:= "_VALIMP" + alltrim(str(nSuspenso))
		cBaseSus	:= "_BASIMP" + alltrim(str(nSuspenso))		
		cIvaExp 	:= "_VALIMP" + alltrim(str(nExportacao))
		cBaseEXP	:= "_BASIMP" + alltrim(str(nExportacao))		
		cIvaIse 	:= "_VALIMP" + alltrim(str(nIsento))
		cBaseIse	:= "_BASIMP" + alltrim(str(nIsento))
		
		If TRepInUse()
			oReport := RepInit() 		//Funcao que instancia o objeto do TReport, com as definicoes das tabelas utilizadas
			oReport:SetLandScape()		//Define padrao de impressao como paisagem
			oReport:PrintDialog()		//Exibe ao usuario a dialog de impressao  
		EndIf
	EndIf
	
Return


//-------------------------------------------------------------------------------------
/*/{Protheus.doc} RepInit
Fun��o responsavel por elaborar o layout do relatorio a ser impresso
@author  MICROSIGA
@version P10
@since 	 17/01/2014
@return 
/*/
//-------------------------------------------------------------------------------------
Static Function RepInit()
	Local oReport
	Local oSection1
	Local oSection2
	Local oSection3	
   	
   	//Instancia o objeto de retatorio
	oReport := TReport():New("FISR057",STR0001,cPerg,{|oReport| PrintReport(oReport)},STR0002)

	//Instancia as SECOES que o relatorio tera	
	oSection1 := TRSection():New(oReport	,STR0003,{"SF2"})			//Seccao 01 - "Tipo de Documento/Totalizadores"
	
	//Celulas da Secao 01
	TRCell():New(oSection1,"F2_FILIAL"		,		,"Filial"					,"@!"								,TamSx3("F2_FILIAL")[1])
	TRCell():New(oSection1,"CODDOC"			,		,"Cod Documento"			, PesqPict('SF2','F2_CODDOC')		,TamSx3("F2_CODDOC")[1])
	TRCell():New(oSection1,"DESC_TP_DOC"	,		,"Descricao Documento"		, "@!"						  		,30)
	TRCell():New(oSection1,"EMISSAO"		,		,"Data Emissao"				, "@!"		    		    		,30)		
	TRCell():New(oSection1,"IVA_MIN"		,		,"IVA MIM"					, PesqPict('SFB','FB_ALIQ')	  		,TamSx3("FB_ALIQ")[1])
	TRCell():New(oSection1,"IVA_BAS"		,		,"IVA BAS"					, PesqPict('SFB','FB_ALIQ')	  		,TamSx3("FB_ALIQ")[1])
	TRCell():New(oSection1,"IVA_SUS"		,		,"IVA SUS"					, PesqPict('SFB','FB_ALIQ')	  		,TamSx3("FB_ALIQ")[1])
	TRCell():New(oSection1,"IVA_EXP"		,		,"IVA EXP"					, PesqPict('SFB','FB_ALIQ')	  		,TamSx3("FB_ALIQ")[1])
	TRCell():New(oSection1,"IVA_ISE"		,		,"IVA ISE"					, PesqPict('SFB','FB_ALIQ')	  		,TamSx3("FB_ALIQ")[1])		

	TRCell():New(oSection1,"IVA_MIN_RET"	,		,"Monto IVA MIN"			, PesqPict('SF2','F2'+cIvaMinCmp)	,TamSx3('F2'+cIvaMinCmp)[1])
	TRCell():New(oSection1,"IVA_MIN_BASE"	,		,"Total IVA MIN"			, PesqPict('SF2','F2_VALBRUT')		,TamSx3('F2_VALBRUT')[1])
	
	//TRCell():New(oSection1,"IVA_SUS_RET"	,		,"Monto IVA SUS"			, PesqPict('SF2','F2'+cIvaSus) 		,TamSx3('F2'+cIvaSus)[1])      	
	TRCell():New(oSection1,"IVA_SUS_BASE"	,		,"Total IVA SUS"			, PesqPict('SF2','F2_VALBRUT')		,TamSx3('F2_VALBRUT')[1])
	
	//TRCell():New(oSection1,"IVA_EXP_RET"	,		,"Monto IVA EXP"			, PesqPict('SF2','F2'+cIvaExp)		,TamSx3('F2'+cIvaExp)[1])
	TRCell():New(oSection1,"IVA_EXP_BASE"	,		,"Total IVA EXP"			, PesqPict('SF2','F2_VALBRUT')		,TamSx3('F2_VALBRUT')[1])
    
	TRCell():New(oSection1,"IVA_BAS_RET"	,		,"Monto IVA BAS"			, PesqPict('SF2','F2'+cIvaBasCmp)	,TamSx3('F2'+cIvaBasCmp)[1])		
	TRCell():New(oSection1,"IVA_BAS_BASE"	,		,"Total IVA BAS"			, PesqPict('SF2','F2_VALBRUT')		,TamSx3('F2_VALBRUT')[1])	

	//TRCell():New(oSection1,"IVA_ISE_RET"	,		,"Monto IVA EXE"			, PesqPict('SF2','F2'+cBaseEXP)		,TamSx3('F2'+cBaseEXP)[1])			
	TRCell():New(oSection1,"IVA_ISE_BASE"	,		,"Total IVA EXE"			, PesqPict('SF2','F2_VALBRUT')		,TamSx3('F2_VALBRUT')[1])		    
	
	TRCell():New(oSection1,"TOTAL_BASE"		,		,"Monto Tot."				, PesqPict('SF2','F2_VALBRUT')		,TamSx3('F2_VALBRUT')[1])

    oSection2 := TRSection():New(oReport	,STR0004,{"SF2"})			//Seccao 02 - "Notas"	
    
    //Celulas da Secao 02
	TRCell():New(oSection2,"SERIE"			,		,"Serie"			   		, "@!"				   				,TamSx3('F2_SERIE')[1])
	TRCell():New(oSection2,"DOC"			,		,"Numero"		   			, "@!"				   				,TamSx3('F2_DOC')[1])
	TRCell():New(oSection2,"STATUS"			,		,"Status"			  		, "@!"				   				,30)
	TRCell():New(oSection2,"CLIENTE"		,		,"Cod Cliente"				, "@!"								,TamSx3('F2_CLIENTE')[1])
	TRCell():New(oSection2,"SERCAE"			,		,"Serie CAE"		  		, "@!"		  						,TamSx3("F2_SERCAE")[1])
	TRCell():New(oSection2,"CAEE"			,		,"Numero CAE"				, "@!"			 		 			,TamSx3("F2_CAEE")[1])
	TRCell():New(oSection2,"SITNOTA"		,		,"Obs RondaNet"		 		, "@!"					 			,TamSx3("F2_SITNOTA")[1])

	oSection3 := TRSection():New(oReport	,STR0005,{"SF2"})			//Seccao 03 - "Quantidades"	

	//Celulas da Secao 03
	TRCell():New(oSection3,"QTD_TRANS"		,		,"Cantidad Transmitidos"	, "@ 999,999,999"					,11)
	TRCell():New(oSection3,"QTD_REJEI"		,		,"Cantidad Anulados"		, "@ 999,999,999"					,11)
	TRCell():New(oSection3,"QTD_APROV"		,		,"Cantidad docs emitidos"	, "@ 999,999,999"					,11)

Return oReport

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} PrintReport()
Fun��o responsavel por "pintar" o relatorio de NFe do Uruguai
@author  MICROSIGA
@version P10
@since 	 17/01/2014
@return 
/*/
//-------------------------------------------------------------------------------------
Static Function PrintReport(oReport)
	Local oSection1   := oReport:Section(1)
	Local oSection2   := oReport:Section(2)
	Local oSection3   := oReport:Section(3)
	Local cDataIni    := DTOS(MV_PAR05)
	Local cDataFim    := DTOS(MV_PAR06)
	Local nI		  := 0
	Local nAut		  := 0
	
	Local cQuery	  := ""
	Local cCodDoc	  := ""
	Local cImpVar	  := ""
	Local cImpVarDes  := ""
	Local cIvaMinX    := ""
	Local cIvaBasX    := ""
	Local cSusX       := ""
	Local cExpX       := ""
	Local cTxMoeda	  := ""   	
	Local cIseX       := ""				
    Local cLivro  	  := ""	
	Local cData		  := ""	 		 
	Local aNotas	  := {}
		 
	Local nIvaMin	  := 0
	Local nIvaMax	  := 0
	Local nIvaSus	  := 0
	Local nIvaExp	  := 0
	Local nIvaIse	  := 0
	Local nTotBas	  := 0
	Local nTotNF	  := 0
	Local nNum		  := 0
	Local nSer		  := 0
	Local nIvaMinX    := 0
	Local nIvaBasX    := 0										
	Local nSuspensX   := 0			
	Local nExportacaX := 0 			
	Local nIsentX     := 0
    Local nValTot 	  := 0
    Local nValBas 	  := 0
    Local nValImp 	  := 0
    Local nValBasMin  := 0
    Local nValImpMin  := 0									
    Local nValTotMin  := 0
    Local nValBasBas  := 0
    Local nValImpBas  := 0										
    Local nValTotBas  := 0
    Local nValBasSus  := 0
    Local nValImpSus  := 0										
    Local nValTotSus  := 0
    Local nValBasExp  := 0
    Local nValImpExp  := 0								
    Local nValTotExp  := 0
    Local nValBasIse  := 0
    Local nValImpIse  := 0								
    Local nValTotIse  := 0

	//��������������������������Ŀ
	//�         SECAO 01         �
	//����������������������������
	oSection1:BeginQuery()
	BeginSql alias "SCT1"				
	   	SELECT DISTINCT CODDOC, EMISSAO 	   	 
	   	  FROM ( 
		    SELECT DISTINCT F2_CODDOC CODDOC, F2_EMISSAO EMISSAO 
				FROM %Table:SF2% SF2 
	           WHERE SF2.F2_FILIAL = %xFilial:SF2%  
				AND SF2.F2_SERIE BETWEEN %Exp:MV_PAR01% AND %Exp:MV_PAR02% 
			AND SF2.F2_DOC BETWEEN %Exp:MV_PAR03% AND %Exp:MV_PAR04% 
	  			AND SF2.F2_EMISSAO >= %Exp:cDataIni% AND SF2.F2_EMISSAO <= %Exp:cDataFim% 
			AND SF2.F2_CODDOC = ' ' 
			AND SF2.D_E_L_E_T_ = ' ' 
			UNION ALL
			SELECT DISTINCT F1_CODDOC CODDOC, F1_EMISSAO EMISSAO FROM %Table:SF1% SF1 WHERE 
				SF1.F1_FILIAL = %xFilial:SF1%	
				AND SF1.F1_SERIE BETWEEN %Exp:MV_PAR01% AND %Exp:MV_PAR02% 
  				AND SF1.F1_DOC BETWEEN %Exp:MV_PAR03% AND %Exp:MV_PAR04% 
				AND SF1.F1_EMISSAO >= %Exp:cDataIni% 
	  			AND SF1.F1_EMISSAO <= %Exp:cDataFim% 
				AND SF1.F1_CODDOC = ' ' 
				AND SF1.D_E_L_E_T_ = ' ' 
				) PIVO
			ORDER BY CODDOC, EMISSAO
	EndSql
	oSection1:EndQuery()
   
   	//Seta as colunas de IVA Min e o IVA Bas
	nIvaMin	:= GetIvaAliq(MV_PAR07)
	nIvaMax	:= GetIvaAliq(MV_PAR08)
	nIvaSus	:= GetIvaAliq(MV_PAR09)
	nIvaExp	:= GetIvaAliq(MV_PAR10)
	nIvaIse	:= GetIvaAliq(MV_PAR11)	
	oSection1:Cell("IVA_MIN"):SetBlock({|| nIvaMin })
	oSection1:Cell("IVA_BAS"):SetBlock({|| nIvaMax })
	oSection1:Cell("IVA_SUS"):SetBlock({|| nIvaSus })
	oSection1:Cell("IVA_EXP"):SetBlock({|| nIvaExp })
	oSection1:Cell("IVA_ISE"):SetBlock({|| nIvaIse })
    
	//Percorre todos os docs em todos tipos localizados
	oSection1:SetHeaderSection(.T.) 
	While SCT1->(!Eof())

		cQuery := "SELECT SF2.F2_DOC DOC, SF2.F2_SERIE SERIE, SF2.F2_CLIENTE CLIE_FOR, SF2.F2_LOJA LOJA, SF2.F2_SITNOTA SIT, "
		cQuery += " SF2.F2_SERCAE SERCAE, SF2.F2_CAEE CAEE, SF2.F2_TXMOEDA TXMOEDA, SF2.F2_VALBRUT VALBRUT, SD2.R_E_C_N_O_ RECN, "
		cQuery += "	SFC.FC_IMPOSTO, SFB.FB_TPCLASS, SFB.FB_CPOLVRO, 'SAIDA' AS TIPO_OPER "
		cQuery += "	FROM " + RetSqlname('SF2') + " SF2, " + RetSqlname("SD2") + " SD2, " + RetSqlname("SFC") + " SFC, " + RetSqlName("SFB") + " SFB "
		cQuery += "	WHERE SF2.F2_FILIAL = '" + xFilial("SF2") + "'" 					
		cQuery += "		AND SD2.D2_FILIAL = '" + xFilial("SD2") + "'"
		cQuery += "		AND SFC.FC_FILIAL = '" + xFilial("SFC") + "'"					
		cQuery += "		AND SFB.FB_FILIAL = '" + xFilial("SFB") + "'"					
		cQuery += "		AND SF2.F2_SERIE BETWEEN '" + MV_PAR01 + "' AND '" + MV_PAR02 + "'"
		cQuery += "		AND SF2.F2_DOC BETWEEN '" + MV_PAR03 + "' AND '" + MV_PAR04 + "'"
		cQuery += "		AND SF2.F2_EMISSAO = '" + SCT1->EMISSAO + "'" 
		cQuery += "		AND SF2.F2_CODDOC = '" + SCT1->CODDOC + "'"
		cQuery += "		AND SD2.D2_DOC = SF2.F2_DOC "
		cQuery += "		AND SD2.D2_SERIE = SF2.F2_SERIE "
		cQuery += "		AND SD2.D2_CLIENTE = SF2.F2_CLIENTE "
		cQuery += "		AND SD2.D2_LOJA = SF2.F2_LOJA "
		cQuery += "		AND SFC.FC_TES = SD2.D2_TES "
		//cQuery += "		AND SFC.FC_SEQ = (SELECT MAX(SFC2.FC_SEQ) FROM " + RetSqlName("SFC") + " SFC2 "
		//cQuery += "							WHERE SFC2.FC_FILIAL = '" + xFilial("SFC") + "'"
		//cQuery += "								AND SFC2.FC_TES = SFC.FC_TES "
		//cQuery += "								AND SFC2.D_E_L_E_T_ = ' ') "
		cQuery += "		AND SFB.FB_CODIGO = SFC.FC_IMPOSTO "
		cQuery += "		AND SF2.D_E_L_E_T_ = ' ' "
		cQuery += "		AND SD2.D_E_L_E_T_ = ' ' "
		cQuery += "		AND SFC.D_E_L_E_T_ = ' ' "
		cQuery += "		AND SFB.D_E_L_E_T_ = ' ' "
		cQuery += "	UNION ALL "
		cQuery += "	SELECT	SF1.F1_DOC DOC, SF1.F1_SERIE SERIE, SF1.F1_FORNECE CLIE_FOR, SF1.F1_LOJA LOJA, SF1.F1_SITNOTA SIT, "
		cQuery += " SF1.F1_SERCAE SERCAE, SF1.F1_CAEE CAEE, SF1.F1_TXMOEDA TXMOEDA, SF1.F1_VALBRUT VALBRUT, SD1.R_E_C_N_O_ RECN, "
		cQuery += "	SFC.FC_IMPOSTO, SFB.FB_TPCLASS, SFB.FB_CPOLVRO, 'ENTRADA' AS TIPO_OPER "
		cQuery += "	FROM " + RetSqlName("SF1") + " SF1, " + RetSqlName("SD1") + " SD1, " + RetSqlName("SFC") + " SFC, " + RetSqlName("SFB") + " SFB "
		cQuery += "	WHERE SF1.F1_FILIAL = '" + xFilial("SF1") + "' "
		cQuery += "		AND SD1.D1_FILIAL = '" + xFilial("SD1") + "'" 
		cQuery += "		AND SFC.FC_FILIAL = '" + xFilial("SFC") + "'"
		cQuery += "		AND SFB.FB_FILIAL = '" + xFilial("SFB") + "'"
		cQuery += "		AND SF1.F1_SERIE BETWEEN '" + MV_PAR01 + "' AND '"  + MV_PAR02 + "'"
		cQuery += "		AND SF1.F1_DOC BETWEEN '" + MV_PAR03 + "' AND '" + MV_PAR04 + "'"
		cQuery += "		AND SF1.F1_EMISSAO = '" + SCT1->EMISSAO + "'"
		cQuery += "		AND SF1.F1_CODDOC = '" + SCT1->CODDOC + "'"
		cQuery += "		AND SF1.F1_FORMUL = 'S'" 						//Na SF1, somente formulario proprio
		cQuery += "		AND SD1.D1_DOC = SF1.F1_DOC "
		cQuery += "		AND SD1.D1_SERIE = SF1.F1_SERIE "
		cQuery += "		AND SD1.D1_FORNECE = SF1.F1_FORNECE "
		cQuery += "		AND SD1.D1_LOJA = SF1.F1_LOJA "
		cQuery += "		AND SFC.FC_TES = SD1.D1_TES "
		//cQuery += "		AND SFC.FC_SEQ = (SELECT MAX(SFC2.FC_SEQ) FROM " + RetSqlName("SFC") + " SFC2 "
		//cQuery += "												WHERE SFC2.FC_FILIAL = '" + xFilial('SFC') + "'"
		//cQuery += "												AND SFC2.FC_TES = SFC.FC_TES "
		//cQuery += "												AND SFC2.D_E_L_E_T_ = ' ' )"
		cQuery += "		AND SFB.FB_CODIGO = SFC.FC_IMPOSTO "
		cQuery += "		AND SF1.D_E_L_E_T_ = ' ' "
		cQuery += "		AND SD1.D_E_L_E_T_ = ' ' "
		cQuery += "		AND SFC.D_E_L_E_T_ = ' ' "
		cQuery += "		AND SFB.D_E_L_E_T_ = ' ' "
		iif(Select('QRY')>0,QRY->(dbCloseArea()),Nil)
		dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQuery), "QRY", .F., .T.)	

		While QRY->(!Eof())	
			
			//Obtem o nro do livro, taxa da moeda e valor total da NFe
			cLivro   := QRY->FB_CPOLVRO	
			cTxMoeda := QRY->TXMOEDA
			nValTot  := (QRY->VALBRUT)			                       

			//Coleto o valor do imposto e a base do item da NFe 
			nValBas  := GetValBasImp(QRY->TIPO_OPER,QRY->RECN,cLivro,"BAS")
			nValImp  := GetValBasImp(QRY->TIPO_OPER,QRY->RECN,cLivro,"IMP")
			                                                  
            //Fa�o a conversao de moedas
			nValTot := nValTot * cTxMoeda
			nValBas := nValBas * cTxMoeda
			nValImp := nValImp * cTxMoeda

            //Totalizador do IvaMin   
			IF alltrim(upper(MV_PAR07)) == alltrim(upper(QRY->FC_IMPOSTO))
				nValBasMin += nValBas
				nValImpMin += nValImp										
				nValTotMin += ((nValImp * nValBas) / 100)
			Endif                       

            //Totalizador do IvaBas
			IF alltrim(upper(MV_PAR08)) == alltrim(upper(QRY->FC_IMPOSTO))
				nValBasBas += nValBas
				nValImpBas += nValImp										
				nValTotBas += ((nValImp * nValBas) / 100)
			Endif	                        	                        

            //Totalizador do IvaSus
			IF alltrim(upper(MV_PAR09)) == alltrim(upper(QRY->FC_IMPOSTO))
				nValBasSus += nValBas
				nValImpSus += nValImp										
				nValTotSus += ((nValImp * nValBas) / 100)
			Endif	                        

            //Totalizador do IvaExp			
			IF alltrim(upper(MV_PAR10)) == alltrim(upper(QRY->FC_IMPOSTO))
				nValBasExp += nValBas
				nValImpExp += nValImp										
				nValTotExp += ((nValImp * nValBas) / 100)
			Endif	                        

            //Totalizador do IvaIse
			IF alltrim(upper(MV_PAR11)) == alltrim(upper(QRY->FC_IMPOSTO))
				nValBasIse += nValBas
				nValImpIse += nValImp
				nValTotIse += ((nValImp * nValBas) / 100)
			Endif		   						  
                       
           	//Adiciona no Array de NFs a ser usado na section 2
           	If aScan(aNotas,{|x| x[1] == QRY->SERCAE .and. x[2] == QRY->CAEE .and. x[3] == QRY->SERIE .and. x[4] == QRY->DOC .and. x[5] == QRY->CLIE_FOR }) <= 0
				aAdd(aNotas,{QRY->SERCAE,QRY->CAEE,QRY->SERIE,QRY->DOC,QRY->CLIE_FOR})
			EndIf
           
            QRY->(dbSkip())
		EndDo
		
        //Totalizador de NFe
		//nTotNF += nValTot
		nTotNF += nValImpMin + nValImpBas + nValBasBas + nValBasSus + nValBasExp + nValBasMin + nValBasIse
		
		//Seta as colunas do documento com os relatorios existentes
        oSection1:Cell("CODDOC"):SetBlock({|| SCT1->CODDOC })		
		oSection1:Cell("DESC_TP_DOC"):SetBlock({|| GetTypeDesc(SCT1->CODDOC) })
		oSection1:Cell("EMISSAO"):SetBlock({|| SCT1->EMISSAO })				
		oSection1:Cell("IVA_BAS_RET"):SetBlock({|| nValImpBas })
		oSection1:Cell("IVA_BAS_BASE"):SetBlock({|| nValBasBas })				
		
		oSection1:Cell("IVA_MIN_RET"):SetBlock({|| nValImpMin })
		oSection1:Cell("IVA_MIN_BASE"):SetBlock({|| nValBasMin })	
		
		//oSection1:Cell("IVA_ISE_RET"):SetBlock({|| nValImpIse })
		oSection1:Cell("IVA_ISE_BASE"):SetBlock({|| nValBasIse })			
		//oSection1:Cell("IVA_EXP_RET"):SetBlock({|| nValImpExp })
		oSection1:Cell("IVA_EXP_BASE"):SetBlock({|| nValBasExp })		
		//oSection1:Cell("IVA_SUS_RET"):SetBlock({|| nValImpSus })
		oSection1:Cell("IVA_SUS_BASE"):SetBlock({|| nValBasSus })
		oSection1:Cell("TOTAL_BASE"):SetBlock({|| nTotNF })
        	
		//Imprime a linha do relatorio (Secao01)
		oSection1:Init()
		oSection1:PrintLine()
		oSection1:Finish()

		//��������������������������Ŀ
		//�         SECAO 02         �
		//���������������������������� 
		oSection2:Init()
		For nI := 1 to len(aNotas)
			//Define as colunas do rel com as posicoes do array
			oSection2:Cell("STATUS"):SetBlock({|| iif(!Empty(aNotas[nI,2]),"Autorizado","N�o Autorizado/Aguardando") })
			oSection2:Cell("SERCAE"):SetBlock({|| aNotas[nI,1] })			
			oSection2:Cell("CAEE"):SetBlock({|| aNotas[nI,2] })
			oSection2:Cell("SERIE"):SetBlock({|| aNotas[nI,3] })
			oSection2:Cell("DOC"):SetBlock({|| aNotas[nI,4] })			
			oSection2:Cell("CLIENTE"):SetBlock({|| aNotas[nI,5] })
            
			//Imprime a linha do relatorio
			oSection2:PrintLine()
			
			//Atualiza o contador de NFe Autorizadas.
			nAut += iif(!Empty(aNotas[nI,2]),1,0)
		Next nI
		oReport:ThinLine() 
		oSection2:Finish()
        
		//��������������������������Ŀ
		//�         SECAO 03         �
		//����������������������������        
		//Armazena o codigo do documento atual e pula para o proximo.
		oSection3:Cell("QTD_APROV"):SetBlock({|| nAut })   
		oSection3:Cell("QTD_REJEI"):SetBlock({|| len(aNotas) - nAut })
	
		//Imprime a linha do relatorio (Secao03)
		oSection3:Init()
		oSection3:PrintLine()		
		oSection3:Finish()

		
		//Zera Variaveis de Controle e vai para o proximo Dia/Documento
    	nTotNF     := 0
    	nValBasMin := 0
    	nValImpMin := 0									
    	nValTotMin := 0
    	nValBasBas := 0
    	nValImpBas := 0										
    	nValTotBas := 0
    	nValBasSus := 0
    	nValImpSus := 0										
    	nValTotSus := 0
    	nValBasExp := 0
    	nValImpExp := 0								
    	nValTotExp := 0
    	nValBasIse := 0
    	nValImpIse := 0								
    	nValTotIse := 0
    	aNotas	   := {}
    	nAut	   := 0

    	SCT1->(dbSkip())	
	EndDo
Return

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} GetValBasImp()
Fun��o responsavel por retornar a base e o valor de um imposto de um item de uma 
determinada NF
@author  MICROSIGA
@version P10
@since 	 02/04/2014
@return 
/*/
//-------------------------------------------------------------------------------------
Static Function GetValBasImp(cTipo,nRec,cLivro,cInfo)
	Local aArea := GetArea()
	Local nRet  := 0
 	
 	If cTipo == "ENTRADA"
 		dbSelectArea('SD1')
 		SD1->(dbGoTo(nRec))
 		If cInfo == "BAS"
 			nRet := &("SD1->D1_BASIMP" + cLivro ) 
 		Else
 			nRet := &("SD1->D1_VALIMP" + cLivro )  		
 		EndIf
 	Else
 		dbSelectArea('SD2')
 		SD2->(dbGoTo(nRec))
 		If cInfo == "BAS"
 			nRet := &("SD2->D2_BASIMP" + cLivro ) 
 		Else
 			nRet := &("SD2->D2_VALIMP" + cLivro )  		
 		EndIf
 	EndIf
	
	RestArea(aArea)
Return nRet

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} GetIvaAliq(cCod)
Fun��o responsavel por retornar a aliquota de um determinado IVA passado como parametro
@author  MICROSIGA
@version P10
@since 	 17/01/2014
@return 
/*/
//-------------------------------------------------------------------------------------
Static Function GetIvaAliq(cCod)
	Local nRet := 0
    Local aArea := GetArea()
	  
	If !Empty(cCod)
		dbSelectArea('SFB')
		SFB->(dbSetOrder(1))
		If SFB->(dbSeek(xFilial('SFB')+ Padr(cCod,TamSx3("FB_CODIGO")[1])))
			nRet := SFB->FB_ALIQ
		EndIf
	Else
		nRet := 0
	EndIf
	 	  
	RestArea(aArea)
Return nRet

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} GetIvaField()
Fun��o responsavel por retornar o numero do campo _VALIMP e BASEIMP relativo ao codigo 
de imposto passado como parametro
@author  MICROSIGA
@version P10
@since 	 17/01/2014
@return 
/*/
//-------------------------------------------------------------------------------------
Static Function GetIvaField(cImpCod)
	Local aArea 	:= GetArea()
	Local nRet		:= 0
	Default cImpCod := ""
	
	If !Empty(cImpCod)
		dbSelectArea('SFB')
		SFB->(dbSetOrder(1))
		If SFB->(dbSeek(xFilial('SFB')+ Padr(cImpCod,TamSx3("FB_CODIGO")[1])))
			nRet := VAL(SFB->FB_CPOLVRO)
		EndIf
	Else
		nRet := 1
	EndIf
	
	RestArea(aArea)	
Return nRet

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} GetIvaTBclass()
Fun��o responsavel por retornar o numero do FB_TPCLASS
@author  MICROSIGA
@version P10
@since 	 26/02/2014
@return 
/*/
//-------------------------------------------------------------------------------------
Static Function GetIvaTBclass(cImpCod)
	Local aArea 	:= GetArea()
	Local cRet		:= "0"
	Default cImpCod := ""
	
	If !Empty(cImpCod)
		dbSelectArea('SFB')
		SFB->(dbSetOrder(1))
		If SFB->(dbSeek(xFilial('SFB')+ Padr(cImpCod,TamSx3("FB_CODIGO")[1])))
			cRet := SFB->FB_TPCLASS
		EndIf
	Else
		cRet := "0"
	EndIf
	
	RestArea(aArea)	
Return cRet

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} GetListTypes()
Fun��o responsavel por retornar todos os codigos de NFe disponiveis no uruguai
@author  MICROSIGA
@version P10
@since 	 17/01/2014
@return 
/*/
//-------------------------------------------------------------------------------------
Static Function GetListTypes()
	Local aRet := {}

	aAdd(aRet,{"101",STR0010})		//"e-Ticket"
	aAdd(aRet,{"102",STR0011})		//"Nota de Credito e-Ticket"
	aAdd(aRet,{"103",STR0012})		//"Nota de Debito e-Ticket"    
	aAdd(aRet,{"111",STR0007})		//"e-Factura"
	aAdd(aRet,{"112",STR0008})		//"Nota de Credito e-Factura"
	aAdd(aRet,{"113",STR0009})		//"Nota de Debito e-Factura"	
	aAdd(aRet,{"201",STR0010})		//"e-Ticket"
	aAdd(aRet,{"202",STR0011})		//"Nota de Credito e-Ticket"
	aAdd(aRet,{"203",STR0012})		//"Nota de Debito e-Ticket"    
	aAdd(aRet,{"211",STR0007})		//"e-Factura"
	aAdd(aRet,{"212",STR0008})		//"Nota de Credito e-Factura"
	aAdd(aRet,{"213",STR0009})		//"Nota de Debito e-Factura"
	
Return aRet 

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} GetTypeDesc
Retorna a descri��o de um determinado tipo de documento

@author  MICROSIGA
@version P10
@since 	 22/01/2014
@return  Nil
/*/
//-------------------------------------------------------------------------------------
Static Function GetTypeDesc(cCodDoc)
	Local aTypes	:= GetListTypes()
	Local nPos		:= 0
	Local cDesc		:= ""
	Default cCodDoc := ""
	        
	If !Empty(cCodDoc)
		nPos := aScan(aTypes,{|x|AllTrim(Upper(x[1])) == alltrim(Upper(cCodDoc))})
		IF !Empty (nPos)
			cDesc := aTypes[nPos,2]
		Else	
			cDesc := aTypes[2]
		Endif	
	EndIf
		
Return cDesc

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} AjustaSX
Funcao do tipo "AjustaSx" responsavel por criar registros no dicionario de dados.
1 - Cria as perguntas do SX1

@author  MICROSIGA
@version P10
@since 	 22/01/2014
@return  Nil
/*/
//-------------------------------------------------------------------------------------
Static Function AjustaSX()
       
	//Apagar este bloco
    /*
	dbSelectArea('SX1')
	SX1->(dbSetOrder(1))
	If SX1->(dbSeek(cPerg))
		While SX1->(!Eof()) .and. alltrim(SX1->X1_GRUPO) == cPerg
			RecLock('SX1',.F.)
			SX1->(dbDelete())
			SX1->(msUnlock())
			SX1->(dbSkip())
			conout('Apagando registro da SX1... ')
		EndDo
	EndIf*/

	
	//Cria perguntas no SX1
	//PutSX1(cGrupo,cOrdem,cPergunt,cPergSpa,cPergEng,cVar,cTipo,nTamanho,nDecimal,nPreSel,cGSC,cValid,cF3,cGrpSXG,cPyme,cVar01,cDef01,cDefSpa1,cDefEng1,cCnt01,cDef02,cDefSpa2,cDefEng2,cDef03,cDefSpa3,cDefEng3,cDef04,cDefSpa4,cDefEng4,cDef05,cDefSpa5,cDefEng5,aHelpPor,aHelpEng,aHelpSpa,cHelp)
	                                                                                                                             	
	PutSx1(cPerg,"01",STR0013	,STR0013	,STR0013	,"mv_ch1", "C"	,TamSX3("F2_SERIE")[1]		,0,0,"G",/*Valid*/""	,"",,		,"mv_par01")	//"Serie De"
	PutSx1(cPerg,"02",STR0014 	,STR0014	,STR0014	,"mv_ch2", "C"	,TamSX3("F2_SERIE")[1]		,0,0,"G",/*Valid*/""	,"",,		,"mv_par02")	//"Serie Ate"
	PutSx1(cPerg,"03",STR0015	,STR0015	,STR0015	,"mv_ch3", "C"	,TamSX3("F2_DOC")[1]		,0,0,"G",/*Valid*/""	,"",,		,"mv_par03")	//"Numero De"
	PutSx1(cPerg,"04",STR0016	,STR0016	,STR0016	,"mv_ch4", "C"	,TamSX3("F2_DOC")[1]		,0,0,"G",/*Valid*/""	,"",,		,"mv_par04")	//"Numero Ate"
	PutSx1(cPerg,"05",STR0017	,STR0017	,STR0017	,"mv_ch5", "D"	,TamSX3("F2_EMISSAO")[1]	,0,0,"G",/*Valid*/""	,"",,		,"mv_par05")	//"Dt Emiss�o De"
	PutSx1(cPerg,"06",STR0018	,STR0018	,STR0018	,"mv_ch6", "D"	,TamSX3("F2_EMISSAO")[1]	,0,0,"G",/*Valid*/""	,"",,		,"mv_par06")	//"Dt Emiss�o Ate"
	PutSx1(cPerg,"07",STR0019	,STR0019	,STR0019	,"mv_ch7", "C"	,TamSX3("FB_CODIGO")[1]		,0,0,"G",/*Valid*/""	,"SFB",,	,"mv_par07")	//"Cod IVA min"
	PutSx1(cPerg,"08",STR0020	,STR0020	,STR0020	,"mv_ch8", "C"	,TamSX3("FB_CODIGO")[1]		,0,0,"G",/*Valid*/""	,"SFB",,	,"mv_par08")	//"Cod IVA bas"
	PutSx1(cPerg,"09",STR0021	,STR0021	,STR0021	,"mv_ch9", "C"	,TamSX3("FB_CODIGO")[1]		,0,0,"G",/*Valid*/""	,"SFB",,	,"mv_par09")	//"Cod SUSPEN"
	PutSx1(cPerg,"10",STR0022	,STR0022	,STR0022	,"mv_ch10","C"	,TamSX3("FB_CODIGO")[1]		,0,0,"G",/*Valid*/""	,"SFB",,	,"mv_par10")	//"Cod EXPORT"
	PutSx1(cPerg,"11",STR0023	,STR0023	,STR0023	,"mv_ch11","C"	,TamSX3("FB_CODIGO")[1]		,0,0,"G",/*Valid*/""	,"SFB",,	,"mv_par11")	//"Cod ISENTO"
Return