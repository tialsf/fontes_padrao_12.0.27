#INCLUDE "MATA021.ch"
#INCLUDE "PROTHEUS.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funci�n    � MATA021  � Autor �                      � Data �09/07/04  ���
�������������������������������������������������������������������������Ĵ��
���Descripci�n� Funci�n para crear Condominios a partir de Proveedores    ���
���           �                                                           ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe    � MATA021()                                                 ���
�������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.             ���
�������������������������������������������������������������������������Ĵ��
���Programador � Data   � BOPS �  Motivo da Alteracao                     ���
�������������������������������������������������������������������������Ĵ��
���Alf. Medrano�29/06/17�MMI-6110  �Replica de 12.1.16 Existan proveedores���
���            �        �          �cuando Sit. Persona = Persona Jur�dica���
���            �        �          �(A2_CONDO == '1') en func VldExReg    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Function MATA021

Local lPyme		  := Iif(Type("__lPyme") <> "U",__lPyme,.F.)
Local aRotAdic	  := {}
Local aIndexSA1  := {}
Local cFiltraSA1 := ""
Local cFiltra	  := ""	//Variavel para filtro

   
PRIVATE aRotina := { 	{STR0001,"PesqBrw"    , 0 , 1},;     // "Pesquisar"
    							{STR0002,"MT021Cond" , 0 , 2},;    // "Visualizar"
			   				{STR0014,"Mt021Cond" , 0 , 3}}   //"Mant. Condominos" //"Mant.Condominos"


//��������������������������������������������������������������Ŀ
//� Define o cabecalho da tela de atualizacoes                   �
//����������������������������������������������������������������
PRIVATE cCadastro := STR0006  //"Clientes" 
PRIVATE aMemos    := {}
PRIVATE nOpc
PRIVATE xRotAuto
Private aRecSA2 := {}
Private bFiltraBrw 	:= {|| Nil}	//Variavel para Filtro
Private aIndexSA2	:= {}			//Variavel Para Filtro

nOpc := if (nOpc == Nil, 3, nOpc)
//��������������������������������������������������������������Ŀ
//� Definicao de variaveis para rotina de inclusao automatica    �
//����������������������������������������������������������������
Private l030Auto := ( xRotAuto <> NIL )  

//������������������������������������������������������������������������Ŀ
//� Inicializa o filtro utilizando a funcao FilBrowse                      �
//��������������������������������������������������������������������������
dbSelectArea("SA2")
dbSetOrder(1)

If SA2->(FieldPos("A2_CODCOND")*FieldPos("A2_CONDO")*FieldPos("A2_PERCCON")) == 0 
	MsgAlert(STR0024,STR0025)   // "Existe Inconsistencia no tratamento de Personas Plurais. Favor verificar os procedimentos de implantacao no boletim de Personas Plurais disponivel no FTP" ## "Condominios"
	Return(.F.)
EndIf		


cFiltra 	  := "A2_FILIAL=='"+xFilial('SA2')+"' .And. A2_CONDO == '1'"
bFiltraBrw 	:= {|| FilBrowse("SA2",@aIndexSA2,@cFiltra) }
Eval(bFiltraBrw)

dbSelectArea("SA2")
dbGotop()

mBrowse( 6, 1,22,75,"SA2")


//������������������������������������������������������������������������Ŀ
//� Deleta o filtro utilizando a funcao FilBrowse                     	   �
//��������������������������������������������������������������������������
EndFilBrw("SA2",aIndexSA2)

dbSelectArea("SA2")
dbSetOrder(1)
         


Return .T.

/*
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Fun��o    �FinaCondVis� Autor � Rafael Rizzatto       � Data �09/07/04  ���
��������������������������������������������������������������������������Ĵ��
���Descri��o � Programa de atualizacao de Condominios                      ���
��������������������������������������������������������������������������Ĵ��
���Sintaxe   � FinaCondVis(ExpC1,ExpN1,ExpN2)                              ���
��������������������������������������������������������������������������Ĵ��
���Parametros� ExpC1 = Alias do arquivo                                    ���
���          � ExpN1 = Numero do registro                                  ���
���          � ExpN2 = Opcao selecionada                                   ���
��������������������������������������������������������������������������Ĵ��
��� Uso      � Financeiro                                                  ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
FUNCTION MT021Vis(cAlias,nReg,nOpc)

Local aUsrBut   := {} 
Local aButtons  := {{"POSCLI",{|| a450F4Con()},STR0012 }}

nOpcA:=AxVisual( cAlias, nReg, nOpc,,,,,aButtons)

dbSelectArea(cAlias)

Return

/*
���������������������������������������������������������������������������������
���������������������������������������������������������������������������������
�����������������������������������������������������������������������������Ŀ��
���Fun��o    �FinCondomi    � Autor �  Rafael Rizzatto      � Data � 09/07/04 ���
�����������������������������������������������������������������������������Ĵ��
���Descri��o � Manutencao das informacoes cadastrais do Condomino             ���
�����������������������������������������������������������������������������Ĵ��
���Sintaxe   �                                                                ���
�����������������������������������������������������������������������������Ĵ��
���Uso       � Generico                                                       ���
������������������������������������������������������������������������������ٱ�
���������������������������������������������������������������������������������
���������������������������������������������������������������������������������
*/
Function Mt021Cond(cAlias,nReg,nOpcx)
Local aPages:= {}, aTitles:= {}
Local nControl	:= 0
Local ni,nX, oDlg

Local aG1	 := {"A2_COD","A2_LOJA","A2_NOME","A2_NREDUZ","A2_PERCCON","A2_EST","A2_CGC","A2_END","A2_BAIRRO","A2_MUN","A2_EST","A2_CEP","A2_TIPO"}
                 
Local lGetD := .F.
Local aSavSA2 := SA2->(GetArea())
Local aObjects := {} 
Local aPosObj  := {} 
Local aSizeAut := {}
Local aNaoGD   := {} 
Local lDel		:=	(nOpcx<>2)
Private aRotina := { {""  	,""		, 0 , 1},;
                      {STR0010	,""	, 0 , 2},; // //"Vizualizar"
                      {STR0011  	,""	, 0 , 3}} // //"Alterar"
                      
Private nOpcao			:= If(nOpcx#2,3,nOpcx)
Private oGet01			:= NIL
Private aCols        := {}
Private aHeader      := {}
Private aSvAtela		:= {{},{},{}}
Private aTela			:= {}
Private aGets			:= {}
Private oEnc01			:= NIL
Private lOk				:= .F.
Private nColsOri		:=	0
Private lAutomato := isBlind()

If !VldExReg()
	Return
EndIf
aSizeAut := MsAdvSize()
aObjects := {} 
//AAdd( aObjects, { 68, 312, .T., .t. } )
//AAdd( aObjects, { 105,309, .t., .t. } )

AAdd( aObjects, {  68,130, .T., .T. } )
AAdd( aObjects, { 105,510, .T., .T. } )

aInfo := { aSizeAut[ 1 ], aSizeAut[ 2 ], aSizeAut[ 3 ], aSizeAut[ 4 ], 3, 3 } 

aPosObj := MsObjSize( aInfo, aObjects,.T. ) 

   dbSelectArea("SA2")
   DbSetOrder(1)
  
   If !lAutomato
   	DEFINE MSDIALOG oDlg TITLE cCadastro FROM aSizeAut[7],0 TO aSizeAut[6],aSizeAut[5] OF oMainWnd PIXEL
   Endif	
	//��������������������������������������������Ŀ
	//� Enchoice 							            �
	//����������������������������������������������  
	dbSelectArea("SA2")                 

	aGets := {}                                            
	aTela := {}

 	RegToMemory("SA2",.F.,.F.)
 	If !lAutomato
		oEnc01:= MsMGet():New("SA2" ,nReg ,2 ,,,,,aPosObj[1],,,,,,oDlg,,.T.,.F.,"aSvATela[1]",.T.)
		oEnc01:Refresh()
	Endif
	
	//�������������������������������������������Ŀ
	//� getDados 			                         �
	//���������������������������������������������
	aHeader 	:= aClone(CriaHeader('SA2',aG1))
	If !lAutomato
		aCols		:= CriaCols('SA2',1,nOpcao,IndexOrd(),"A2_FILIAL+A2_CODCOND",xFilial("SA2")+SA2->A2_CODCOND,aG1,"A2_CONDO == '2'",@nColsOri)
	Else
		If FindFunction("GetParAuto")
			aRetAuto 	:= GetParAuto("MATA021TESTCASE")
			aCols 		:= aRetAuto[1]
		Endif
	EndIf
	n        := 1
	For nX:= 1 To Len(aHeader)		
		aHeader[nX][2] := "_"+aHeader[nX][2]
	Next   
	If !lAutomato 
		oGet01 	:= MSGetDados():New(aPosObj[2,1],aPosObj[2,2],aPosObj[2,3],aPosObj[2,4],nOpcao,"Mt021LinOk()","Mt021Tudok()";
		,,lDel,,,,,"Mt021FieldOk()",,,'(n>nColsOri)',oDlg)				
		 		
		oGet01:oBrowse:Default()	
		oGet01:oBrowse:Refresh()
	                     
		ACTIVATE DIALOG oDlg ON ;
		INIT ( EnchoiceBar(oDlg, {|| lOk:=.T.,If(oGet01:TudoOk(),(If(Str(nOpcao,1) $ "345",Mt021Grv(),.t.),oDlg:End()),.F.)  },{|| lOk := .F.,oDlg:End()} ))
	Else
		lOk:=.T.
		Mt021LinOk()	
		Mt021Grv()
	EndIf
	RestArea(aSavSA2)
  
Return
/*
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �CondoGrava �Autor � Rafael Rizzatto       � Data � 09/07/04 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Funcao de gravacao dos dados.                              ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Financeiro                                                 ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function Mt021Grv()
Local nI := 0,nJ               
Local nPosCli	:=	Ascan(aHeader,{|x| Alltrim(x[2])=="_A2_COD"})
Local nPosLoja	:=	Ascan(aHeader,{|x| Alltrim(x[2])=="_A2_LOJA"})
Local cFilter
Local aArea	:=	SA2->(GetArea())
DbSelectArea("SA2")
DbSetOrder(1)
#IFDEF TOP 
	cFilter	:=	DbFilter()
	Set Filter To
#ENDIF	          
Begin Transaction
For nI := 1 To Len(aCols)
 	If	!aCols[nI][Len(aCols[nI])] 
	 	If !DbSeek(xFilial()+aCols[nI][nPosCli]+aCols[nI][nPosLoja])
		 	RecLock("SA2",.T.)
			A2_FILIAL   := xFilial("SA2")
			A2_CODCOND  := M->A2_CODCOND
			A2_NREDUZ   := M->A2_NREDUZ
			A2_NOME     := M->A2_NOME  
			A2_BAIRRO   := M->A2_BAIRRO
			A2_MUN      := M->A2_MUN
			A2_END      := M->A2_END
			A2_EST      := M->A2_EST
			A2_TIPO     := M->A2_TIPO
			A2_AGENRET  := M->A2_AGENRET
			A2_PORIVA   := M->A2_PORIVA
			A2_PORGAN   := M->A2_PORGAN
			A2_PERCIVA  := M->A2_PERCIVA
			A2_INSCGAN  := M->A2_INSCGAN
			A2_PERCIB   := M->A2_PERCIB
			A2_AGREGAN  := M->A2_AGREGAN
			A2_RETIB    := M->A2_RETIB
			A2_CONDO    := "2"
			MsUnLock()
		Endif	
	 	RecLock("SA2",.F.)
		For nJ := 1 To Len(aHeader)  //laco dos campos (colunas)
			If !Empty(aCols[nI,nJ]) .Or. SubStr(aHeader[nJ,2],2) == "A2_PERCCON" 
				Replace &(SubStr(aHeader[nJ,2],2)) With aCols[nI,nJ]
			Endif
		Next         
		//Para los proveedores que ya existen
		If  SA2->A2_CONDO != "2" //Significa que ya se habia dado de alta como parte del condominio
			SA2->A2_CONDO	:= "2"   //Proveedor existente se va a convertir en Participante del Condominio        
			SA2->A2_CODCOND	:= M->A2_CODCOND
			If  VldProvSA2()
				SA2->A2_TIPO  	:= M->A2_TIPO
				SA2->A2_AGENRET := M->A2_AGENRET
				SA2->A2_PORIVA  := M->A2_PORIVA
				SA2->A2_PORGAN  := M->A2_PORGAN
				SA2->A2_PERCIVA := M->A2_PERCIVA
				SA2->A2_INSCGAN := M->A2_INSCGAN
				SA2->A2_PERCIB  := M->A2_PERCIB
				SA2->A2_AGREGAN := M->A2_AGREGAN
				SA2->A2_RETIB   := M->A2_RETIB 
			Endif
		Endif
  		MsUnlock()         
 		
	Endif
Next                             
End Transaction      
#IFDEF TOP            
	DbSelectArea("SA2")
	DbSetOrder(aArea[2])	
	If !lAutomato				
		Eval(bFiltraBrw)
	EndIf
#ELSE
	RestArea(aArea)
#ENDIF

Return .T.
      
/*
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �CriaHeader �Autor � Rafael Rizzatto       � Data � 09/07/04 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Funcao de criacao do aHeader.                              ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Financeiro                                                 ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function CriaHeader(cAlias,aCampos)
Local aTmpheader := {}
dbSelectArea("SX3")
dbSetOrder(1)
dbSeek(cAlias)
nUsado := 0
While !EOF() .And. (x3_arquivo == cAlias)
	IF X3USO(x3_usado) .AND. cNivel >= x3_nivel .And. !Empty( AScan( aCampos, { |x| x == AllTrim(SX3->X3_CAMPO) } ) )
		nUsado++
		cValid	:=	X3_VALID
		If Alltrim(X3_CAMPO) == "A2_COD" .Or.Alltrim(X3_CAMPO) == "A2_LOJA"
			cValid	:=	"Mt021VldCod()"
		Endif
		AADD(aTmpHeader,{ TRIM(x3titulo()), x3_campo, x3_picture,x3_tamanho, x3_decimal, cValid,x3_usado, x3_tipo, x3_arquivo, x3_context } )
	EndIf
	dbSkip()
End    
Return aTmpheader

/*
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �CriaCols   �Autor � Rafael Rizzatto       � Data � 09/07/04 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Funcao de criacao do aCols de acordo com o aHeader         ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Financeiro                                                 ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                       
Static Function CriaCols(cAlias,nF,nOpcao,nOrdem,cKey,cConteudo,aCampos,cFiltro,nLenCols)
Local nUsado:= 0
Local aTmpHeader:= {}, aTmpCols:= {}
Local nCnt:=0
Local nHeader := 0
Local cVar,i,nI  
Private cFilter	:= ''

nOrdem 		:= if(nOrdem==NIL,1,nOrdem)
cKey   		:= if(cKey==NIL,'',cKey)
cConteudo	:= if(cConteudo=NIL,'',cConteudo)
aCampos  	:= if(aCampos=NIL,{},aCampos)

SX3->(DbSetOrder(2))
nCnt := 0
aRecSA2:={}
aTmpCols:={}
dbSelectArea(cAlias)
#IFDEF TOP 
	cFilter	:=	DbFilter()
	Set Filter To
#ENDIF
dbSetOrder(1)
dbSeek(cConteudo)
While !EOF() .AND. xFilial("SA2") == SA2->A2_FILIAL 
	If (Iif(cKey=='',.t.,(&cKey == cConteudo) ))
		If &cFiltro
			nCnt++
			nUsado:=0
			Aadd(aRecSA2,SA2->(Recno()))
			Aadd(aTmpCols,Array(Len(aHeader)+1))
			For nHeader := 1 To Len(aHeader)
				SX3->(DbSeek(aHeader[nHeader][2]))
				If X3USO(SX3->X3_USADO) .AND. cNivel >= SX3->x3_nivel .And. !Empty( AScan( aCampos, { |x| x == AllTrim(SX3->X3_CAMPO) } ) )
					If SX3->X3_CONTEXT # "V"
						aTmpCols[nCnt][nHeader] := &(cAlias+"->"+Trim(SX3->x3_campo))
					Else
						aTmpCols[nCnt][nHeader] := CriaVar(AllTrim(SX3->x3_campo))
					EndIf
				EndIf
			Next
		   aTmpCols[nCnt][Len(aHeader)+1] := .F.
		Endif  
	Endif
	dbSkip()
EndDo

nLenCols	:=	Len(aTmpCols)
//���������������������������Ŀ
//� Monta Array de 1 elemento �
//� vazio. Se inclus�o.       �
//�����������������������������
If Len(aTmpCols) == 0
	aTmpCols := Array(1, Len(aHeader) + 1)
	aTmpCols[1,Len(aHeader)+1] := .F.
	For nHeader := 1 To Len(aHeader)
		SX3->(DbSeek(aHeader[nHeader][2]))
		If X3USO(SX3->x3_usado) .AND. cNivel >= SX3->x3_nivel .And. !Empty( AScan( aCampos, { |x| x == AllTrim(SX3->X3_CAMPO) } ) )
			nUsado++
			If SX3->x3_tipo == "C"
				aTmpCols[1][nHeader] := SPACE(SX3->x3_tamanho)
			ElseIf SX3->x3_tipo == "N"
				aTmpCols[1][nHeader] := 0
			ElseIf SX3->x3_tipo == "D"
				aTmpCols[1][nHeader] := CTOD("  /  /  ")
			ElseIf SX3->x3_tipo == "M"
				aTmpCols[1][nHeader] := ""
			Else
				aTmpCols[1][nHeader] := .F.
			EndIf
			If SX3->x3_context == "V"
				aTmpCols[1][nHeader] := CriaVar(allTrim(SX3->x3_campo))
			EndIf
		EndIf
	Next
EndIf


#IFDEF TOP              
	Eval(bFiltraBrw)
#ELSE
	DbSetOrder(nOrdem)
#ENDIF

Return aClone(aTmpCols)         

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � CondoTudok� Autor � Rafael Rizzatto      � Data � 09/07/04 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Verifica se os campos estao OK                             ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Financeiro                                                 ���
�������������������������������������������������������������������������Ĵ��
���Programador � Data   � BOPS   �           Motivo da Alteracao          ���
�������������������������������������������������������������������������Ĵ��
���Laura Medina�19/04/17�MMI-5484�Se agrego validacion del Codigo y Tienda���
���            �        �        �para que no permita dar de alta un regis���
���            �        �        �tro o con el mismo dato que el del enca-���
���            �        �        �bezado que se est� dando mantenimiento. ���
��������������������������������������������������������������������������ٱ�
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function Mt021Tudok()
Local lRet := .T.
Local nX
Local nTotal	:=	0
Local nPosCli	:=	Ascan(aHeader,{|x| Alltrim(x[2])=="_A2_COD"})
Local nPosLoja	:=	Ascan(aHeader,{|x| Alltrim(x[2])=="_A2_LOJA"})
Local nPosPerc	:=	Ascan(aHeader,{|x| Alltrim(x[2])=="_A2_PERCCON"})
Local aArea	:= GetArea()
Local cCodFor := ""
Local lNoGrab := .T.
DbSelectArea("SA2")
DbSetOrder(1)
For nX:= 1 To Len(aCols)
	If !aCols[nX][Len(aCols[nX])] 
		nTotal	+=	aCols[nX][nPosPerc]	
	Endif
	If  SA2->(DbSeek(xFilial("SA2")+aCols[nX][nPosCli]+aCols[nX][nPosLoja]))
    	cCodFor+=  STR0021 + aCols[nX][nPosCli]+ STR0022 +aCols[nX][nPosLoja] + Chr(13) + Chr(10)//    " Codigo: " ### " Sucursal: "
    	If  aCols[nX][nPosCli]+aCols[nX][nPosLoja] == M->A2_COD + M->A2_LOJA 
			Msgalert( STR0031 +Chr(13) + Chr(10)+ Chr(13) + Chr(10)+ ;
						STR0021 + aCols[nX][nPosCli]+ STR0022 +aCols[nX][nPosLoja] + Chr(13) + Chr(10))
			lNoGrab:=.F.
			lRet	:=.F.
		Endif
    EndIf
Next
RestArea(aArea)
If	lNoGrab
	If nTotal <> 100
		Help('1',0,'NO100%')
		lRet	:=	.F.
	Endif
	If  lRet .AND. Len(cCodFor) > 0
		lRet:=.F.
		DEFINE FONT oFont NAME "Courier New" SIZE 9,14   //6,15
		DEFINE MSDIALOG oDlg TITLE STR0023 From 3,0 to 240,450 PIXEL    // "Proveedor ya existe. Confirma modificaci�n de datos."
		@ 5,5 GET oMemo  VAR cCodFor MEMO SIZE 215,090 OF oDlg PIXEL 
		oMemo:bRClicked := {||AllwaysTrue()}
		oMemo:oFont:=oFont
		DEFINE SBUTTON  FROM 100,150 TYPE 1 ACTION (If(!lRet,lRet:= .T.,oDlg:End()),)ENABLE OF oDlg PIXEL //Apaga
		DEFINE SBUTTON  FROM 100,180 TYPE 2 ACTION (oDlg:End()) ENABLE OF oDlg PIXEL 
		
		ACTIVATE MSDIALOG oDlg CENTER
	EndIf
EndIf

Return lRet
/*
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �Mt021LinOk �Autor � Rafael Rizzatto       � Data � 09/07/04 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Checagem de linha na getdados.                             ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Financeiro                                                 ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function Mt021LinOk()
Local lRet	:=	.T.
Local nPosCli	:=	Ascan(aHeader,{|x| Alltrim(x[2])=="_A2_COD"})
Local nPosLoja	:=	Ascan(aHeader,{|x| Alltrim(x[2])=="_A2_LOJA"})
If !aCols[n,Len(aHeader)+1]
	If Empty(aCols[n,nPosCli]).Or. Empty(aCols[n,nPosLoja])
		Help('1',0,"Obrigat")
		lRet	:=	.F.
	EndIf
Endif	   
Return lRet

/*
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �Mt021VldCod�Autor � Rafael Rizzatto       � Data � 09/07/04 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Checagem de linha na getdados.                             ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Financeiro                                                 ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function Mt021VldCod()
Local lRet	:=	.T.            
Local nPosCli	:=	Ascan(aHeader,{|x| Alltrim(x[2])=="_A2_COD"})
Local nPosLoja	:=	Ascan(aHeader,{|x| Alltrim(x[2])=="_A2_LOJA"})  
Local cCod 	:=	IIf("M->_A2_COD" == Alltrim(ReadVar()),&(ReadVar()),aCols[n][nPosCli])
Local cLoja	:=	IIf("M->_A2_LOJA" == Alltrim(ReadVar()),&(ReadVar()),aCols[n][nPosLoja])
Local nX	:=	0
If n>nColsOri              
	If !aCols[n,Len(aHeader)+1]
		If "M->_A2_COD" == Alltrim(ReadVar())
			If Substr(cCod,1,3)<>M->A2_CODCOND
				Aviso("Atencion",STR0015+M->A2_CODCOND+')',{STR0016}) //'El codigo de proveedor debe comenzar con el codigo de condominio ('###'Ok'
				lRet	:=	.F.
			EndIf
		Endif
		If lRet    
			nX	:=	1
			While nX <= Len(aCols) .And. lRet
				If nX <> n
					If aCols[nX][nPosCli]+aCols[nX][nPosLoja] == cCod+cLoja  .And.  !aCols[3][len(aHeader)+1] //No debe considerar los registros marcados como borrados
						Aviso("Atencion",STR0017+Alltrim(Str(nX)),{STR0016}) //'El codigo de proveedor+sucursal informado ya esta registrado en la linea '###'Ok'
						lRet	:=	.F.					
					Endif 
				Endif
				nX++
			Enddo
		Endif
	Endif	        
Endif	 
Return lRet

/*
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �Mt021FielOk�Autor � Rafael Rizzatto       � Data � 09/07/04 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Checagem de cada campo da GEtdados                         ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Financeiro                                                 ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function Mt021FieldOk()
Local lRet	:=	.T.
If !("A2_PERCCON"$ReadVar()) .And. n<=nColsOri
	Aviso(STR0018,STR0019,{STR0020}) //"Atencion"###"Este campo no puede ser editado, por favor modifiquelo a traves de la rutina de mantenimiento de proveedores"###"Ok"
	lRet	:=	.F.
Endif	 
Return lRet



/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � VldProvSA2� Autor � Laura Medina         � Data � 01/09/16 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Verifica si el Proveedor ya existe en la tabla SA2         ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Financeiro                                                 ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function VldProvSA2()
Local lRet := .T.
Local nX
Local nTotal	:=	0
Local nPosCli	:=	Ascan(aHeader,{|x| Alltrim(x[2])=="_A2_COD"})
Local nPosLoja	:=	Ascan(aHeader,{|x| Alltrim(x[2])=="_A2_LOJA"})
Local nPosPerc	:=	Ascan(aHeader,{|x| Alltrim(x[2])=="_A2_PERCCON"})
Local aArea	:= GetArea()
Local cCodFor := ""
DbSelectArea("SA2")
DbSetOrder(1)

If  SA2->(DbSeek(xFilial("SA2")+aCols[n][nPosCli]+aCols[n][nPosLoja]))
   	cCodFor+=  STR0021 + aCols[n][nPosCli]+ STR0022 +aCols[n][nPosLoja] + Chr(13) + Chr(10)//    " Codigo: " ### " Sucursal: "
   	cCodFor+=  Chr(13) + Chr(10) 					
   	cCodFor+=  STR0027 + Chr(13) + Chr(10)
  	cCodFor+=  STR0028 + Chr(13) + Chr(10)
   	cCodFor+=  STR0029 + Alltrim(M->A2_COD)+" "+ Alltrim(M->A2_LOJA) +", "+ STR0030+ Chr(13) + Chr(10)
   	cCodFor+=  " "+POSICIONE("SX3",2,"A2_TIPO","X3_TITSPA") 	+" (A2_TIPO)" 	+ Chr(13) + Chr(10)
	cCodFor+=  " "+POSICIONE("SX3",2,"A2_AGENRET","X3_TITSPA")+" (A2_AGENRET)"	+ Chr(13) + Chr(10)
	cCodFor+=  " "+POSICIONE("SX3",2,"A2_PORIVA","X3_TITSPA") +" (A2_PORIVA)" 	+ Chr(13) + Chr(10)
	cCodFor+=  " "+POSICIONE("SX3",2,"A2_PORGAN","X3_TITSPA") +" (A2_PORGAN)"	+ Chr(13) + Chr(10)
	cCodFor+=  " "+POSICIONE("SX3",2,"A2_PERCIVA","X3_TITSPA")+" (A2_PERCIVA)"	+ Chr(13) + Chr(10)
	cCodFor+=  " "+POSICIONE("SX3",2,"A2_INSCGAN","X3_TITSPA")+" (A2_INSCGAN)"	+ Chr(13) + Chr(10)
	cCodFor+=  " "+POSICIONE("SX3",2,"A2_PERCIB","X3_TITSPA") +" (A2_PERCIB)"	+ Chr(13) + Chr(10)
	cCodFor+=  " "+POSICIONE("SX3",2,"A2_AGREGAN","X3_TITSPA")+" (A2_AGREGAN)"	+ Chr(13) + Chr(10)
	cCodFor+=  " "+POSICIONE("SX3",2,"A2_RETIB","X3_TITSPA")  +" (A2_RETIB)"	+ Chr(13) + Chr(10)  	
EndIf     

RestArea(aArea)

If  lRet .AND. Len(cCodFor) > 0
	lRet:=.F.
	DEFINE FONT oFont NAME "Courier New" SIZE 9,14   //6,15
	DEFINE MSDIALOG oDlg TITLE STR0026 From 3,0 to 240,450 PIXEL    // "Proveedor ya existe. �Confirma modificaci�n de datos.?"
	@ 5,5 GET oMemo  VAR cCodFor MEMO SIZE 215,090 OF oDlg PIXEL 
	oMemo:bRClicked := {||AllwaysTrue()}
	oMemo:oFont:=oFont
	DEFINE SBUTTON  FROM 100,150 TYPE 1 ACTION (If(!lRet,lRet:= .T.,oDlg:End()),)ENABLE OF oDlg PIXEL //Apaga
	DEFINE SBUTTON  FROM 100,180 TYPE 2 ACTION (oDlg:End()) ENABLE OF oDlg PIXEL 
	
	ACTIVATE MSDIALOG oDlg CENTER
EndIf

Return lRet
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � VldExReg  � Autor  � Alf Medrano         � Data � 14/16/17 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Verifica si existe Proveedor en SA2 como Persona Jur�dica  ���
���          � para configurar condominios                                ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Financeiro                                                 ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function VldExReg()
Local aArea	:= GetArea()
Local cQuery 	:= ""
Local nCnt 	:= 0
Local lRet 	:= .T. 
cQuery := " SELECT COUNT(*) TOTAL "
cQuery += " FROM "+	RetSqlName("SA2")
cQuery += " WHERE A2_FILIAL = '" + xFilial("SA2") + "' AND "
cQuery += " A2_CONDO = '1' AND  D_E_L_E_T_ = ' ' "
dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery), 'TmpSA2', .F., .T.)
dbSelectArea("TmpSA2")
nCnt := TmpSA2->TOTAL

If  nCnt <= 0
	MsgInfo( STR0033 , STR0032 )	// No existen Proveedores configurados con Sit. Persona = Persona Jur�dica. ### "Atenci�n"
	lRet := .F.
EndIf

dbCloseArea()
RestArea( aArea )
Return lRet
