#INCLUDE "PROTHEUS.CH"

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} FISA302A
  
Defini��o da Estrutura de Classes para a Apura��o do Ressarcimento ou Complemento do ICMS Retido por 
Substitui��o Tribut�ria ou Antecipado.
Para o Estado de S�o Paulo, o m�todo de apura��o � determinado pela portaria CAT 42/2018.

@author Rafael.Soliveira
@since 08/11/2019
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Classe FISA302APURACAO

Classe respons�vel pela apura��o de cada movimento da query principal, controlando: 
Atualiza��o de saldos / Enquadramento Legal / Valor de Confronto / Valor a Ressarcir ou Complementar 
(Colunas 11 a 27 da Ficha 3).
  
@author Rafael.Soliveira
@since 08/11/2019
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
CLASS FISA302APURACAO FROM LongClassName

Data cIdApur     As Character //---Identificador da Apura��o                                  ---//
Data cFilApur    As Character //---C�digo da Filial apurada                                   ---//
Data cAnoMes     As Character //---Per�odo da Apura��o                                        ---//
Data aTotApur    As Array     //---Totais da Apura��o                                         ---//
Data oSaldoProd  As Object    //---Objeto que controla o saldo f�sico e finaneceiro do produto---//
Data oMovimento  As Object    //---Objeto que controla o movimento analisado                  ---//
Data oMovimApur  As Object    //---Objeto que controla os valores apurados para o movimento   ---//


Method New(cIdApur,cAnoMes) CONSTRUCTOR
Method SetaSldIni(cCodProd,nQtdade,nVUnitOP,nVlSldTtal,nVUnitBSST,nVlSldTtBS,nVUnitST,nVlSdTotST,nVUnitFCP,nVlSldTtFC)
Method SetaMovim(dDataMov,cTipoMov,cNumDoc,cItemDoc,cSerieDoc,cEspecDoc,cChaveDoc,cNumPDV,cTipoDoc,cCodProd,cCodPart,cTipoPart,nAliqInt,cCFOP,cCST,cFGerNReal,nQtdade,nVlrTotPrd,nVlrFrete,nVlrSeguro,nVlrDesp,nVlrDesc,nVlrBICMS,nVlrICMS,nVlrICMSST,nVlrICMSAn,nVlrBICMND,nVlrICMSND,lEntProt,nVlrICMSEn,aDocOriApu,nVlrPauta,cNrLivro,cUnid,nBASERET,nBFCPANT,nVFCPANT,nAFCPANT,nVFECPST,nALQFECP,nVUNIT)
Method ApuraMovim()

//---Getters e Setters---//
Method GetIdApur()
Method GetAnoMes()

ENDCLASS


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} New()
  
M�todo construtor da Classe FISA302APURACAO

@author Rafael.Soliveira
@since 08/11/2019
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Method New(cIdApur,cAnoMes) Class FISA302APURACAO
	Self:cIdApur    := cIdApur
	Self:cFilApur   := ""
	Self:cAnoMes    := cAnoMes
    
    //---{cCodEnquad,nApurVlrR,nApurVlrC,nApurVlrCO,nApurVlrFR,nApurVlrFC}---//
    Self:aTotApur   := {{'0',0,0,0,0,0},;  //SP000 - Opera��o n�o ensejadora de Ressarcimento ou Complemento de ICMS-ST  
                        {'1',0,0,0,0,0},;  //SP100 - Opera��o ensejadora de Ressarcimento ICMS-ST na hip�tese do Inciso I do Art. 269 do RICMS
                        {'2',0,0,0,0,0},;  //SP101 - Opera��o ensejadora de Ressarcimento de ICMS-ST na hip�tese Inciso II do Art. 269 do RICMS
                        {'3',0,0,0,0,0},;  //SP200 - Opera��o ensejadora de Ressarcimento de ICMS-ST na hip�tese Inciso III do Art. 269 do RICMS, com manuten��o do cr�dito de opera��o pr�pria 
                        {'4',0,0,0,0,0},;  //SP201 - Opera��o ensejadora de Ressarcimento de ICMS-ST na hip�tese Inciso IV do Art. 269 do RICMS                        
                        {'6',0,0,0,0,0}}   //SP300 - Opera��o ensejadora de Complemento de ICMS-ST na hip�tese do Inciso I do Art. 269 do RICMS


    Self:oSaldoProd := FISA302SALDOPRODUTO():New()
    Self:oMovimento := FISA302MOVIMENTO():New()
    Self:oMovimApur := FISA302MOVIMENTOAPURACAO():New()
Return Self


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} SetaSldIni()
  
M�todo que carrega no objeto o saldo inicial do per�odo para o produto analisado. 
Deve ser chamado a cada produto analisado, antes de sua movimenta��o.

@author Rafael.Soliveira
@since 08/11/2019
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Method SetaSldIni(cCodProd,nQtdade,nVUnitOP,nVlSldTtal,nVUnitBSST,nVlSldTtBS,nVUnitST,nVlSdTotST,nVUnitFCP,nVlSldTtFC) Class FISA302APURACAO
    
    Self:oSaldoProd:SetCodProd(cCodProd)    
    Self:oSaldoProd:SetQtdade(nQtdade)
    Self:oSaldoProd:SetOrdMov(0)

    //OP
    Self:oSaldoProd:SetVlrUnit(nVUnitOP)
    Self:oSaldoProd:SetVlrTot(nVlSldTtal)
    Self:oSaldoProd:SetUltDif0(nVUnitOP)

    //BAS ST    
    Self:oSaldoProd:SetVUtBSST(nVUnitBSST)
    Self:oSaldoProd:SetVSdTtBS(nVlSldTtBS)
    Self:oSaldoProd:SetUtUnDBS(nVUnitBSST)  

    //ST    
    Self:oSaldoProd:SetnVUntST(nVUnitST)
    Self:oSaldoProd:SetVlSdTST(nVlSdTotST)
    Self:oSaldoProd:SetUlUnDST(nVUnitBSST)
    
    //FECP    
    Self:oSaldoProd:SetVUntFCP(nVUnitFCP)
    Self:oSaldoProd:SetVlSdTFC(nVlSldTtFC)
    Self:oSaldoProd:SetUtUnDFC(nVUnitFCP)
   
    
Return


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} SetaMovim()
  
M�todo que carrega no objeto o movimento a ser analisado. 
Deve ser chamado a cada movimento encontrado para o produto em quest�o.

@author Rafael.Soliveira
@since 08/11/2019
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Method SetaMovim(dDataMov,cTipoMov,cNumDoc,cItemDoc,cSerieDoc,cEspecDoc,cChaveDoc,cNumPDV,cTipoDoc,cCodProd,cCodPart,cTipoPart,nAliqInt,cCFOP,cCST,cFGerNReal,nQtdade,nVlrTotPrd,nVlrFrete,nVlrSeguro,nVlrDesp,nVlrDesc,nVlrBICMS,nVlrICMS,nVlrICMSST,nVlrICMSAn,nVlrBICMND,nVlrICMSND,lEntProt,nVlrICMSEn,aDocOriApu,nVlrPauta,cNrLivro,cUnid,nBASERET,nBFCPANT,nVFCPANT,nAFCPANT,nVFECPST,nALQFECP,nVUNIT,nEFETIVO) Class FISA302APURACAO
    Self:oMovimento:ClearMov()
    Self:oMovimento:SetDataMov(dDataMov)
    Self:oMovimento:SetTipoMov(cTipoMov)
    Self:oMovimento:SetNumDoc(cNumDoc)
    Self:oMovimento:SetItemDoc(cItemDoc)
    Self:oMovimento:SetSerDoc(cSerieDoc)
    Self:oMovimento:SetEspDoc(cEspecDoc)
    Self:oMovimento:SetChvDoc(cChaveDoc)
    Self:oMovimento:SetNPDV(cNumPDV)
    Self:oMovimento:SetTipoDoc(cTipoDoc)
    Self:oMovimento:SetCodProd(cCodProd)
    Self:oMovimento:SetCodPart(cCodPart)
    Self:oMovimento:SetTipoPar(cTipoPart)
    Self:oMovimento:SetAliqInt(nAliqInt)
    Self:oMovimento:SetCFOP(cCFOP)
    Self:oMovimento:SetCST(cCST)
    Self:oMovimento:SetFGerNR(cFGerNReal)
    Self:oMovimento:SetQtdade(nQtdade)
    Self:oMovimento:SetTotPrd(nVlrTotPrd)
    Self:oMovimento:SetFrete(nVlrFrete)
    Self:oMovimento:SetSeguro(nVlrSeguro)
    Self:oMovimento:SetDespesa(nVlrDesp)
    Self:oMovimento:SetDescont(nVlrDesc)
    Self:oMovimento:SetBICMS(nVlrBICMS)
    Self:oMovimento:SetVICMS(nVlrICMS)
    Self:oMovimento:SetVICMSST(nVlrICMSST)
    Self:oMovimento:SetVICMSAn(nVlrICMSAn)
    Self:oMovimento:SetVBICMND(nVlrBICMND)
    Self:oMovimento:SetVICMSND(nVlrICMSND)
    Self:oMovimento:SetEntProt(lEntProt)
    Self:oMovimento:SetVICMSEn(nVlrICMSEn)
    Self:oMovimento:SetDocOrig(aDocOriApu)
    Self:oMovimento:SetVlrPauta(nVlrPauta)    
    Self:oMovimento:SetNrLivro(cNrLivro)    
    Self:oMovimento:SetUnid(cUnid)    
    Self:oMovimento:SetnBASERET(nBASERET)
    Self:oMovimento:SetnBFCPANT(nBFCPANT)
    Self:oMovimento:SetnVFCPANT(nVFCPANT)
    Self:oMovimento:SetnAFCPANT(nAFCPANT)
    Self:oMovimento:SetnVFECPST(nVFECPST)
    Self:oMovimento:SetnALQFECP(nALQFECP)
    Self:oMovimento:SetnVUNIT(nVUNIT)
    Self:oMovimento:SetEfetivo(nEFETIVO)
    
    

Return


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ApuraMovim()
  
M�todo respons�vel por controlar, para cada movimento: 
Atualiza��o de saldos / Enquadramento Legal / Valor de Confronto / Valor a Ressarcir ou Complementar 
(Colunas 11 a 27 da Ficha 3).

@author Rafael.Soliveira
@since 08/11/2019
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Method ApuraMovim() Class FISA302APURACAO
    Local cTipoMov    := Self:oMovimento:GetTipoMov()
    Local cCodProd    := Self:oMovimento:GetCodProd()
    Local nQtdade     := Self:oMovimento:GetQtdade()
    Local cTipoDoc    := Self:oMovimento:GetTipoDoc()
    Local nAliqInt    := Self:oMovimento:GetAliqInt()
    Local nVlrICMS    := Self:oMovimento:GetVICMS()
    Local nVlrICMSST  := Self:oMovimento:GetVICMSST()
    Local nVlrICMSAn  := Self:oMovimento:GetVICMSAn()
    Local nVlrBICMND  := Self:oMovimento:GetVBICMND()
    Local nVlrICMSND  := Self:oMovimento:GetVICMSND()
    Local aDocOriApu  := Self:oMovimento:GetDocOrig()
    Local nVlSldUnit  := 0
    Local nVlrICMSSu  := 0
    Local nVlrICMSCo  := 0
    Local nVlrICMSOp  := 0    
    Local cCodEnquad  := ''
    Local aVlrRC      := {}
    Local nPos        := 0
    Local nBaseIC     := 0
    Local nEFETIVO    := Self:oMovimento:GetEfetivo()

    Local nBASERET    := Self:oMovimento:GetnBASERET()
    Local nBFCPANT    := Self:oMovimento:GetnBFCPANT()
    Local nVFCPANT    := Self:oMovimento:GetnVFCPANT()
    Local nAFCPANT    := Self:oMovimento:GetnAFCPANT()
    Local nVFECPST    := Self:oMovimento:GetnVFECPST()
    Local nALQFECP    := Self:oMovimento:GetnALQFECP()
    Local cCST        := Self:oMovimento:GetCST()
    Local cCFOP       := Self:oMovimento:GetCFOP()

    Local nVlrTotPrd :=  Self:oMovimento:GetTotPrd()
    Local nVlrFrete  :=  Self:oMovimento:GetFrete()
    Local nVlrSeguro :=  Self:oMovimento:GetSeguro()
    Local nVlrDesp   :=  Self:oMovimento:GetDespesa()
    Local nVlrDesc   :=  Self:oMovimento:GetDescont()

    Local nVUnitST   := 0
    Local nVUnitBSST := 0
    Local nVUnitFCP  := 0

    Local nCalVlrICM    := 0

    Local nMUCRED      :=0
    Local nVREFCP      :=0
    Local nVCMFCP      :=0
    Local nVUCRED      :=0
    Local nVURFCP      :=0
    Local nVURET       :=0
    Local nBURET       :=0
    Local cCODRES      := ""
    Local nVUCFC       :=0
    Local nVUCST       :=0
    Local nVURTFC      :=0
    Local nVUREST      :=0
    Local nMUVSF       :=0
    Local nMUVSTF      :=0
    Local nMUBST       :=0
    Local nVUNIT       :=0
    Local nVlrFECP     := 0
    Local nVlrBSST     := 0
    Local nSTRest       := 0
    Local nSTCompl      := 0
    Local nStResfecp    := 0
    Local nStComfecp    := 0

    
    If (cTipoMov=='E' .And. cTipoDoc != 'D') .Or. (cTipoMov=='S' .And. cTipoDoc == 'D') //---Para Entradas / Devolu��es de Entradas: Define Total de ICMS Suportado / Atualiza o saldo do produto---//

        //---Define Total de ICMS Suportado---//
        If cTipoDoc != 'D' .Or. (cTipoDoc == 'D' .And.  aDocOriApu[9] = 0)
            
            /*ICMS*/
            If nVlrICMS  > 0 
                nVlrICMSSu := nCalVlrICM := nVlrICMS
            Else
                nBaseIC := (nVlrTotPrd+nVlrFrete+nVlrSeguro+nVlrDesp-nVlrDesc)            
                nVlrICMSSu := nCalVlrICM := Iif(nVlrICMS > 0, nVlrICMS, ((nBaseIC * (nAliqInt+nAFCPANT)) / 100))                
            Endif                      

            //BASE DO ICMS ST
            If nBASERET > 0  .and. nVlrBICMND == 0
                nVlrBSST := nBASERET
            Else
                nVlrBSST := nVlrBICMND
            EndIf

            /* ST*/
            If (nVlrICMSST + nVlrICMSAn) == 0
                If nVlrICMSND == 0
                    nVlrICMSND := Iif(nVlrBICMND > 0 .And. nAliqInt > 0, Round(((nVlrBSST*(nAliqInt+nAFCPANT))/100)-nCalVlrICM,2), nVlrICMSND)   //ICMS Ant com FECP
                Endif
            Else
                nVlrICMSND := nVlrICMSST
            EndIf

            /*FECP*/             
            If  nVFECPST > 0 .and. nVFCPANT == 0
                nVlrFECP := nVFECPST
            Else                        
                nVlrFECP := Iif(nVFCPANT > 0 ,nVFCPANT,0)                        

                If nVlrFECP == 0 .and. nBFCPANT > 0 .And. nAFCPANT > 0
                    nVlrFECP := Round(((nBFCPANT*nAFCPANT)/100),2)
                EndIf
            EndIf

        Else
            nVlrICMSSu  := (aDocOriApu[9]/aDocOriApu[1])*nQtdade
            nVlrICMSND  := (aDocOriApu[10]/aDocOriApu[1])*nQtdade
            nVlrBSST    := (aDocOriApu[11]/aDocOriApu[1])*nQtdade
            nVlrFECP    := (aDocOriApu[12]/aDocOriApu[1])*nQtdade
        EndIf

        //---Atualiza o saldo do produto---//
        Self:oSaldoProd:AtuSaldo(cTipoMov,cCodProd,nQtdade,nVlrICMSSu,nVlrICMSND,nVlrBSST,nVlrFECP)

    ElseIf cTipoMov=='S' .And. cTipoDoc != 'D' //---Para Sa�das : Define Total de ICMS Suportado / Atualiza o saldo do produto / Enquadra o Movimento / Define Valor de Confronto / Define Valor a Ressarcir ou Complementar / Define Valor do Cr�dito da Opera��o Pr�pria---//

        //---Define Total de ICMS Suportado---//
        nVlSldUnit := Self:oSaldoProd:GetVlrUnit()        
        nVUnitST   := Self:oSaldoProd:GetnVUntST()
        nVUnitBSST := Self:oSaldoProd:GetVUtBSST()
        nVUnitFCP  := Self:oSaldoProd:GetVUntFCP()

        If Self:oSaldoProd:GetQtdade() == nQtdade // DSERFIS1-15944 - Evitar erro de arredondamento, o ideal seria aumentar as casas decimai do campos CIL_MUST  , CIL_TUST  , CIL_MFICM e CIL_TFICM
            nVlrICMSSu := Self:oSaldoProd:GetVlrTot()            
            nVlrICMSND := Self:oSaldoProd:GetVlSdTST()
            nVlrBSST   := Self:oSaldoProd:GetVSdTtBS()
            nVlrFECP   := Self:oSaldoProd:GetVlSdTFC()
        Else
            nVlrICMSSu := nVlSldUnit*nQtdade
            nVlrICMSND := nVUnitST*nQtdade
            nVlrBSST   := nVUnitBSST*nQtdade
            nVlrFECP   := nVUnitFCP*nQtdade
        EndIf    

        //---Atualiza o saldo do produto---//
        Self:oSaldoProd:AtuSaldo(cTipoMov,cCodProd,nQtdade,nVlrICMSSu,nVlrICMSND,nVlrBSST,nVlrFECP)

        //---Enquadra o Movimento / Define Valor de Confronto / Define Valor a Ressarcir ou Complementar / Define Valor do Cr�dito da Opera��o Pr�pria---//
        cCodEnquad := Self:oMovimento:EnquadMov()
        /*
        If cCodEnquad != '0'
            nVlrICMSCo := Self:oMovimento:DefVlrConf(cCodEnquad)
            aVlrRC     := Self:oMovimento:DefVlrRC(cCodEnquad,nVlrICMSSu,nVlrICMSCo)
            If cCodEnquad == '4'
                nVlrICMSOp := nVlrICMSCo
            EndIf
        EndIf
        */
        If cCodEnquad != '0'
            
            //Calcula ICMS Efetivo // C185 
            //Campo 10 (VL_UNIT_ICMS_NA_OPERACAO_CONV) - ICMS Efetivo da sa�da
            If nEFETIVO > 0 .and.  SUBSTR(cCFOP,1,1) == '5' //FT_VICEFET
                nCalVlrICM :=  nEFETIVO
            Else
                nBaseIC := (nVlrTotPrd+nVlrFrete+nVlrSeguro+nVlrDesp-nVlrDesc)            
                nCalVlrICM := ((nBaseIC * (nAliqInt+nAFCPANT)) / 100)
            Endif            
            nCalVlrICM := nCalVlrICM/nQtdade
            
            IF cCodEnquad != '2'
                
                //Campo 15 (VL_UNIT_ICMS_ST_CONV_REST)
                //sem campo 11
                /*Campo 12 (VL_UNIT_ICMS_OP_ESTOQUE_CONV)
                + Campo 13 (VL_UNIT_ICMS_ST_ESTOQUE_CONV)
                - Campo 10 (VL_UNIT_ICMS_NA_OPERACAO_CONV)
                ------------------------------------------------------
                = Campo 15 (VL_UNIT_ICMS_ST_CONV_REST)*/
                nSTRest      := Max(nVlSldUnit + nVUnitST - nCalVlrICM,0) 

                //Campo 17 (VL_UNIT_ICMS_ST_CONV_COMPL)
                /*Campo 10 (VL_UNIT_ICMS_NA_OPERACAO_CONV)
                - Campo 12 (VL_UNIT_ICMS_OP_ESTOQUE_CONV)
                - Campo 13 (VL_UNIT_ICMS_ST_ESTOQUE_CONV)
                ------------------------------------------------
                = Campo 17 (VL_UNIT_ICMS_ST_CONV_COMPL)*/
 
                nSTCompl     := Max(nCalVlrICM - nVUnitST - nVlSldUnit,0) 
            Else
                //Campo 15 (VL_UNIT_ICMS_ST_CONV_REST)
                //com campo 11
                /*Campo 12 (VL_UNIT_ICMS_OP_ESTOQUE_CONV)
                + Campo 13 (VL_UNIT_ICMS_ST_ESTOQUE_CONV)
                - Campo 11 (VL_UNIT_ICMS_OP_CONV)
                -------------------------------------------------------
                = Campo 15 (VL_UNIT_ICMS_ST_CONV_REST)*/                
                //Efetivo da entrada
                //Para as UFs em que a legisla��o estabelecer que o valor desse campo corresponder� ao mesmo valor expresso no campo 12
                //(VL_UNIT_ICMS_OP_ESTOQUE_CONV), seu preenchimento ser� facultativo.
                nSTRest      := Max(nVlSldUnit + nVUnitST - nVlSldUnit,0) 
                                
                //Campo 17 (VL_UNIT_ICMS_ST_CONV_COMPL)
                nSTCompl     := Max(nCalVlrICM - nVUnitST - nVlSldUnit,0) 
            Endif          


            IF nSTRest > 0               
                nStResfecp  := nVlrFECP
            Else                
                nStComfecp  := nVUnitFCP
                cCodEnquad  := '6' // COMPLEMENTO
            Endif
        Endif


    Else //---Para Devolu��es de Sa�das : Define Total de ICMS Suportado (De acordo com mov. apurado original) / Atualiza o saldo do produto / Enquadra o Movimento (De acordo com mov. apurado original) / Define Valor de Confronto (De acordo com mov. apurado original) / Define Valor a Ressarcir ou Complementar (De acordo com mov. apurado original) / Define Valor do Cr�dito da Opera��o Pr�pria (De acordo com mov. apurado original)---//

        If !Empty(aDocOriApu[3]) //---Encontrou mov. apurado original (tabela CII)---//

            //--- Define Total de ICMS Suportado (De acordo com mov. apurado original)---//
            nVlSldUnit := aDocOriApu[2]
            nVUnitST   := aDocOriApu[10]
            nVUnitBSST := aDocOriApu[11]
            nVUnitFCP  := aDocOriApu[12]
            nCalVlrICM := aDocOriApu[5]

            nVlrICMSSu := nVlSldUnit*nQtdade
            nVlrICMSND := nVUnitST*nQtdade
            nVlrBSST   := nVUnitBSST*nQtdade
            nVlrFECP   := nVUnitFCP*nQtdade

            //---Atualiza o saldo do produto---//
            Self:oSaldoProd:AtuSaldo(cTipoMov,cCodProd,nQtdade,nVlrICMSSu,nVlrICMSND,nVlrBSST,nVlrFECP)              
        EndIf

    EndIf

    
    //---Carrega os valores apurados no atributo oMovimApur---//
    Self:oMovimApur:ClearMovAp()
    Self:oMovimApur:SetQtdade(nQtdade)
    If (cTipoMov=='E' .And. cTipoDoc != 'D') .Or. (cTipoMov=='S' .And. cTipoDoc == 'D')
        
        //---Define o respons�vel pela reten��o do ICMS-ST (1 � Remetente Direto / 2 � Remetente Indireto / 3 � Pr�prio declarante )---//        
        If nVlrBICMND > 0
            Self:oMovimApur:SetcCODRES('2')
        ElseIf nVlrICMSAn > 0
            Self:oMovimApur:SetcCODRES('3')
        Else
            Self:oMovimApur:SetcCODRES('1')
        EndIf         

        Self:oMovimApur:SetSupUni(nVlSldUnit)
        Self:oMovimApur:SetSupTot(nVlrICMSSu)         
        Self:oMovimApur:SetConfEnt(nCalVlrICM/nQtdade)
        Self:oMovimApur:SetnVURET(nVlrICMSND/nQtdade)
        Self:oMovimApur:SetnBURET(nVlrBSST/nQtdade)
        Self:oMovimApur:SetnVURFCP(nVlrFECP/nQtdade)
               

    Else        
        Self:oMovimApur:SetEnquad(cCodEnquad)
        
        If cCodEnquad != '0'
            Self:oMovimApur:SetSupUni(nVlSldUnit)
            Self:oMovimApur:SetSupTot(nVlrICMSSu)            
            Self:oMovimApur:SetApurRes(nSTRest*nQtdade)      
            Self:oMovimApur:SetApurCom(nSTCompl*nQtdade)     
            Self:oMovimApur:SetApurFR(nStResfecp*nQtdade)   
            Self:oMovimApur:SetApurFC(nStComfecp*nQtdade)

            Self:oMovimApur:SetnVUCFC(nStComfecp)
	        Self:oMovimApur:SetnVUCST(nSTCompl)
	        Self:oMovimApur:SetnVURTFC(nStResfecp)
	        Self:oMovimApur:SetnVUREST(nSTRest)
            Self:oMovimApur:SetConfSai(nCalVlrICM)
            
        Endif
        
        If cCodEnquad == '2'
            Self:oMovimApur:SetnVUCRED(nVlSldUnit)
            Self:oMovimApur:SetApurCop(nVlrICMSSu)            
        Endif
        

        //---Atualiza o atributo totalizador da apura��o---//
        nPos := Ascan(Self:aTotApur,{|a|a[1] == cCodEnquad})
        If nPos > 0
            If (cTipoMov=='S' .And. cTipoDoc != 'D')
                Self:aTotApur[nPos][2] += Self:oMovimApur:GetApurRes()
                Self:aTotApur[nPos][3] += Self:oMovimApur:GetApurCom()
                Self:aTotApur[nPos][4] += Self:oMovimApur:GetnVUCRED()
                Self:aTotApur[nPos][5] += Self:oMovimApur:GetApurFR()
                Self:aTotApur[nPos][6] += Self:oMovimApur:GetApurFC()
            Else
                Self:aTotApur[nPos][2] -= Self:oMovimApur:GetApurRes()
                Self:aTotApur[nPos][3] -= Self:oMovimApur:GetApurCom()
                Self:aTotApur[nPos][4] -= Self:oMovimApur:GetnVUCRED()
                Self:aTotApur[nPos][5] -= Self:oMovimApur:GetApurFR()
                Self:aTotApur[nPos][6] -= Self:oMovimApur:GetApurFC()
            EndIf
        EndIf
        //---FIM Atualiza o atributo totalizador da apura��o---//

    EndIf
    
    Self:oMovimApur:SetSldQtd(Self:oSaldoProd:GetQtdade())

    Self:oMovimApur:SetSldUni(Self:oSaldoProd:GetVlrUnit())
    Self:oMovimApur:SetSldTot(Self:oSaldoProd:GetVlrTot())

    Self:oMovimApur:SetSldUST(Self:oSaldoProd:GetnVUntST())    
    Self:oMovimApur:SetSldTST(Self:oSaldoProd:GetVlSdTST())

    Self:oMovimApur:SetSldUBS(Self:oSaldoProd:GetVUtBSST())
    Self:oMovimApur:SetSldTBS(Self:oSaldoProd:GetVSdTtBS())
    
    Self:oMovimApur:SetSldUFC(Self:oSaldoProd:GetVUntFCP())
    Self:oMovimApur:SetSldTFC(Self:oSaldoProd:GetVlSdTFC())

    
    //---FIM Carrega objeto de retorno com valores apurados para o movimento---//

Return 


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Getters e Setters

@author Rafael.Soliveira
@since 12/11/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Method GetIdApur() Class FISA302APURACAO
Return Self:cIdApur

Method GetAnoMes() Class FISA302APURACAO
Return Self:cAnoMes


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Classe FISA302SALDOPRODUTO

Classe respons�vel por controlar saldos f�sicos e financeiros do produto analisado.
  
@author Rafael.Soliveira
@since 08/11/2019
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
CLASS FISA302SALDOPRODUTO FROM LongClassName

Data cCodProd    As Character //---C�digo do Produto                                  ---//
Data nQtdade     As Numeric   //---Saldo F�sico do Produto                            ---//
Data nOrdMovAtu  As Numeric   //---Ordena��o do Movimento Atual do Produto            ---//

//OP
Data nVlSldUnit  As Numeric   //---Valor Unit�rio do Saldo                           ---// CIL_MICM	Vlr Unit Inicial ICMS
Data nVlSldTtal As Numeric   //---Valor Total do Saldo                               ---//
Data nUltUnDif0  As Numeric   //---Valor do �ltimo Unit�rio do Saldo Diferente de Zero---//
 
//ST
Data nVUnitST    As Numeric    //---Valor Unit�rio do Saldo                           ---// CIL_MUST	V. Unit Ini. ICMS ST
Data nVlSdTotST As Numeric   //---Valor Total do Saldo                               ---//
Data nUltUDifST As Numeric   //---Valor do �ltimo Unit�rio do Saldo Diferente de Zero---//

//BC ST
Data nVUnitBSST   As Numeric    //---Valor Unit�rio do Saldo                           ---// CIL_MUBCST	Vl Unit. Inicial BC ICMS ST
Data nVlSldTtBS  As Numeric   //---Valor Total do Saldo                               ---//
Data nUltUDifBS  As Numeric   //---Valor do �ltimo Unit�rio do Saldo Diferente de Zero---//

//FECP
Data nVUnitFCP   As Numeric    //---Valor Unit�rio do Saldo                           ---// CIL_MIFC	V. Unit Inicial FECP
Data nVlSldTtFC As Numeric   //---Valor Total do Saldo                               ---//
Data nUltUDifFC As Numeric   //---Valor do �ltimo Unit�rio do Saldo Diferente de Zero---//


Method New() CONSTRUCTOR
Method AtuSaldo(cTipoMov,cCodProd,nQtdade,nVlSldTtal,nVlSdTotST,nVlSldTtBS,nVlSldTtFC)

//---Getters e Setters---//

//Controle Produto
Method SetCodProd(cCodProd)
Method SetQtdade(nQtdade)
Method SetOrdMov(nOrdMovAtu)

Method GetCodProd()
Method GetQtdade()
Method GetOrdMov()

//ICMS OP
Method SetVlrUnit(nVlSldUnit)
Method SetVlrTot(nVlSldTtal)
Method SetUltDif0(nUltUnDif0)

Method GetVlrUnit()
Method GetVlrTot()
Method GetUltDif0()

//ST
Method SetnVUntST(nVUnitST)
Method SetVlSdTST(nVlSdTotST)
Method SetUlUnDST(nUltUDifST)

Method GetnVUntST()
Method GetVlSdTST()
Method GetUlUnDST()

//BASE ST
Method SetVUtBSST(nVUnitBSST)
Method SetVSdTtBS(nVlSldTtBS)
Method SetUtUnDBS(nUltUDifBS)

Method GetVUtBSST()
Method GetVSdTtBS()
Method GetUtUnDBS()

//FECP
Method SetVUntFCP(nVUnitFCP)
Method SetVlSdTFC(nVlSldTtFC)
Method SetUtUnDFC(nUltUDifFC)

Method GetVUntFCP()
Method GetVlSdTFC()
Method GetUtUnDfFC()


ENDCLASS


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} New()
  
M�todo construtor da Classe FISA302SALDOPRODUTO

@author Rafael.Soliveira
@since 08/11/2019
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Method New() Class FISA302SALDOPRODUTO
    Self:cCodProd    := ""
	Self:nQtdade     := 0
	Self:nVlSldUnit  := 0
    Self:nVlSldTtal  := 0
    Self:nUltUnDif0  := 0
    Self:nOrdMovAtu  := 0
    Self:nVUnitST    := 0
    Self:nVlSdTotST  := 0
    Self:nUltUDifST  := 0
    Self:nVUnitBSST  := 0
    Self:nVlSldTtBS  := 0
    Self:nUltUDifBS  := 0
    Self:nVUnitFCP   := 0
    Self:nVlSldTtFC  := 0
    Self:nUltUDifFC  := 0
    
Return Self


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AtuSaldo()
  
M�todo que atualiza o saldo atual do produto analisado. � disparado a cada movimento analisado.

@author Rafael.Soliveira
@since 08/11/2019
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Method AtuSaldo(cTipoMov,cCodProd,nQtdade,nVlSldTtal,nVlSdTotST,nVlSldTtBS,nVlSldTtFC) Class FISA302SALDOPRODUTO
    Local cProdAtual    := Self:GetCodProd()
    Local nQtdAtual     := Self:GetQtdade()

    Local nTotAtual      := Self:GetVlrTot()
    Local nTtAtualST    := Self:GetVlSdTST()
    Local nTtAtualBS    := Self:GetVSdTtBS()
    Local nTtAtualFC    := Self:GetVlSdTFC()



    Local nUnitAtual    := 0
    Local nUntAtuST  	:= 0
    Local nUntAtuBC  	:= 0
    Local nUntAtuFC  	:= 0

    If cCodProd == cProdAtual
        If cTipoMov=='E'
            nQtdAtual   += nQtdade
            nTotAtual   += nVlSldTtal
            nTtAtualST  += nVlSdTotST
            nTtAtualBS  += nVlSldTtBS
            nTtAtualFC  += nVlSldTtFC
        Else
            nQtdAtual   -= nQtdade
            nTotAtual   -= nVlSldTtal
            nTtAtualST  -= nVlSdTotST
            nTtAtualBS  -= nVlSldTtBS
            nTtAtualFC  -= nVlSldTtFC
        EndIf
        If nQtdAtual > 0
            nUnitAtual := nTotAtual/nQtdAtual
            nUntAtuST  := nTtAtualST/nQtdAtual
            nUntAtuBC  := nTtAtualBS/nQtdAtual
            nUntAtuFC  := nTtAtualFC/nQtdAtual
        Else
            nUnitAtual    := 0
            nUntAtuST  := 0
            nUntAtuBC  := 0
            nUntAtuFC  := 0
        EndIf

        If nUnitAtual>0
            Self:SetUltDif0(nUnitAtual)            
            Self:SetUlUnDST(nUntAtuST)
            Self:SetUtUnDBS(nUntAtuBC)
            Self:SetUtUnDFC(nUntAtuFC)

        EndIf
        
        Self:SetQtdade(nQtdAtual)
        Self:SetOrdMov(Self:GetOrdMov()+1)

        //ICMS OP
        Self:SetVlrUnit(nUnitAtual)
        Self:SetVlrTot(nTotAtual)

        //ST
        Self:SetnVUntST(nUntAtuST)
        Self:SetVlSdTST(nTtAtualST)        

        //BASE ST
        Self:SetVUtBSST(nUntAtuBC)
        Self:SetVSdTtBS(nTtAtualBS)        

        //FECP
        Self:SetVUntFCP(nUntAtuFC)
        Self:SetVlSdTFC(nTtAtualFC)
    EndIf
Return


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Getters e Setters

@author Rafael.Soliveira
@since 08/11/2019
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Method SetCodProd(cCodProd) Class FISA302SALDOPRODUTO
    Self:cCodProd := cCodProd
Return

Method SetQtdade(nQtdade) Class FISA302SALDOPRODUTO
    Self:nQtdade := nQtdade
Return

Method SetOrdMov(nOrdMovAtu) Class FISA302SALDOPRODUTO
    Self:nOrdMovAtu := nOrdMovAtu
Return

Method GetCodProd() Class FISA302SALDOPRODUTO
Return Self:cCodProd

Method GetQtdade() Class FISA302SALDOPRODUTO
Return Self:nQtdade

Method GetOrdMov() Class FISA302SALDOPRODUTO
Return Self:nOrdMovAtu


//ICMS OP
Method SetVlrUnit(nVlSldUnit) Class FISA302SALDOPRODUTO
    Self:nVlSldUnit := nVlSldUnit
Return

Method SetVlrTot(nVlSldTtal) Class FISA302SALDOPRODUTO
    Self:nVlSldTtal := nVlSldTtal
Return

Method SetUltDif0(nUltUnDif0) Class FISA302SALDOPRODUTO
    Self:nUltUnDif0 := nUltUnDif0
Return

Method GetVlrUnit() Class FISA302SALDOPRODUTO
Return Self:nVlSldUnit

Method GetVlrTot() Class FISA302SALDOPRODUTO
Return Self:nVlSldTtal

Method GetUltDif0() Class FISA302SALDOPRODUTO
Return Self:nUltUnDif0


//ST
Method SetnVUntST(nVUnitST) Class FISA302SALDOPRODUTO
    Self:nVUnitST := nVUnitST
Return

Method SetVlSdTST(nVlSdTotST) Class FISA302SALDOPRODUTO
    Self:nVlSdTotST := nVlSdTotST
Return

Method SetUlUnDST(nUltUDifST) Class FISA302SALDOPRODUTO
    Self:nUltUDifST := nUltUDifST
Return

Method GetnVUntST() Class FISA302SALDOPRODUTO
Return Self:nVUnitST

Method GetVlSdTST() Class FISA302SALDOPRODUTO
Return Self:nVlSdTotST

Method GetUlUnDST() Class FISA302SALDOPRODUTO
Return Self:nUltUDifST


//BASE ST
Method SetVUtBSST(nVUnitBSST) Class FISA302SALDOPRODUTO
    Self:nVUnitBSST := nVUnitBSST
Return

Method SetVSdTtBS(nVlSldTtBS) Class FISA302SALDOPRODUTO
    Self:nVlSldTtBS := nVlSldTtBS
Return

Method SetUtUnDBS(nUltUDifBS) Class FISA302SALDOPRODUTO
    Self:nUltUDifBS := nUltUDifBS
Return

Method GetVUtBSST() Class FISA302SALDOPRODUTO
Return Self:nVUnitBSST

Method GetVSdTtBS() Class FISA302SALDOPRODUTO
Return Self:nVlSldTtBS

Method GetUtUnDBS() Class FISA302SALDOPRODUTO
Return Self:nUltUDifBS


//FECP
Method SetVUntFCP(nVUnitFCP) Class FISA302SALDOPRODUTO
    Self:nVUnitFCP := nVUnitFCP
Return

Method SetVlSdTFC(nVlSldTtFC) Class FISA302SALDOPRODUTO
    Self:nVlSldTtFC := nVlSldTtFC
Return

Method SetUtUnDFC(nUltUDifFC) Class FISA302SALDOPRODUTO
    Self:nUltUDifFC := nUltUDifFC
Return

Method GetVUntFCP() Class FISA302SALDOPRODUTO
Return Self:nVUnitFCP

Method GetVlSdTFC() Class FISA302SALDOPRODUTO
Return Self:nVlSldTtFC

Method GetUtUnDfFC() Class FISA302SALDOPRODUTO
Return Self:nUltUDifFC


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Classe FISA302MOVIMENTO

Classe respons�vel por controlar todas as colunas da Ficha 3, al�m de definir: 
Enquadramento Legal / Valor de Confronto / Valor a Ressarcir ou Complementar 
  
@author Rafael.Soliveira
@since 23/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
CLASS FISA302MOVIMENTO FROM LongClassName

Data dDataMov     As Date      //---Data do Movimento
Data cTipoMov     As Character //---Tipo do Movimento (E-Entrada / S-Sa�da)
Data cNumDoc      As Character //---N�mero do Documento Fiscal
Data cItemDoc     As Character //---Item do Documento Fiscal
Data cSerieDoc    As Character //---S�rie do Documento Fiscal
Data cEspecDoc    As Character //---Esp�cie do Documento Fiscal
Data cChaveDoc    As Character //---Chave do Documento Fiscal
Data cNumPDV      As Character //---N�mero de S�rie de Fabrica��o do ECF
Data cTipoDoc     As Character //---Tipo do Documento (Normal / Devolu��o / Complemento)
Data cCodProd     As Character //---C�digo do Produto
Data cCodPart     As Character //---C�digo do Participante
Data cTipoPart    As Character //---Tipo do Participante (Cliente Final / Revendedor)
Data nAliqInt     As Numeric   //---Al�quota Interna do Produto
Data cCFOP        As Character //---CFOP
Data cCST         As Character //---CST ICMS
Data cFGerNReal   As Character //---Indica se a opera��o (CFOP) deve ser enquadrada como 2-Fato Gerador n�o realizado
Data cNrLivro     AS Character //-- Numero de livro
Data cUnid        As Character //-- Primeira Unidade de medida
Data nQtdade      As Numeric   //---Quantidade
Data nVlrTotPrd   As Numeric   //---Valor Total do Produto
Data nVlrFrete    As Numeric   //---Valor do Frete
Data nVlrSeguro   As Numeric   //---Valor do Seguro
Data nVlrDesp     As Numeric   //---Valor das Despesas
Data nVlrDesc     As Numeric   //---Valor do Desconto
Data nVlrBICMS    As Numeric   //---Base de C�lculo do ICMS
Data nVlrICMS     As Numeric   //---Valor do ICMS
Data nVlrICMSST   As Numeric   //---Valor do ICMS Retido por ST
Data nVlrICMSAn   As Numeric   //---Valor do ICMS Antecipado
Data nVlrBICMND   As Numeric   //---Base de C�lculo do ICMS Recolhido Anteriormente por ST em entradas CST 60
Data nVlrICMSND   As Numeric   //---Valor do ICMS do ICMS Recolhido Anteriormente por ST em entradas CST 60
Data lEntProt                  //---.T. -> Em sa�das interestaduais, busca ICMS suportado na entrada atrav�s da tabela SFT (Informar 0 no atributo nVlrICMSEn) / .F. -> N�o busca ICMS suportado na entrada atrav�s da tabela SFT (Informar o valor de ICMS suportado na entrada no atributo nVlrICMSEn)
Data nVlrICMSEn   As Numeric   //---Valor do ICMS suportado na entrada, usado como Valor de Confronto apenas para sa�das interestaduais
Data aDocOriApu   As Array     //---Valores apurados para o Documento Fiscal Original, em casos de movimentos de devolu��o
Data nVlrPauta    As Numeric   //---Valor da pauta informado no cadastro do produto no campo B1_VLR_ICM
Data nBASERET     As Numeric    //
Data nBFCPANT     As Numeric    //
Data nVFCPANT     As Numeric    //
Data nAFCPANT     As Numeric    //
Data nVFECPST     As Numeric    //
Data nALQFECP     As Numeric    //
Data nVUNIT       As Numeric
Data nEFETIVO     As Numeric


Method New() CONSTRUCTOR
Method EnquadMov()

/*
Method DefVlrConf(cCodEnquad)
Method DefVlrRC(cCodEnquad,nVlrICMSSu,nVlrICMSCo)
Method DefBaseICM()
*/
Method ClearMov()

//---Getters e Setters---//
Method SetDataMov(dDataMov)
Method SetTipoMov(cTipoMov)
Method SetNumDoc(cNumDoc)
Method SetItemDoc(cItemDoc)
Method SetSerDoc(cSerieDoc)
Method SetEspDoc(cEspecDoc)
Method SetChvDoc(cChaveDoc)
Method SetNPDV(cNumPDV)
Method SetTipoDoc(cTipoDoc)
Method SetCodProd(cCodProd)
Method SetCodPart(cCodPart)
Method SetTipoPar(cTipoPart)
Method SetAliqInt(nAliqInt)
Method SetCFOP(cCFOP)
Method SetCST(cCST)
Method SetFGerNR(cFGerNReal)
Method SetQtdade(nQtdade)
Method SetTotPrd(nVlrTotPrd)
Method SetFrete(nVlrFrete)
Method SetSeguro(nVlrSeguro)
Method SetDespesa(nVlrDesp)
Method SetDescont(nVlrDesc)
Method SetBICMS(nVlrBICMS)
Method SetVICMS(nVlrICMS)
Method SetVICMSST(nVlrICMSST)
Method SetVICMSAn(nVlrICMSAn)
Method SetVBICMND(nVlrBICMND)
Method SetVICMSND(nVlrICMSND)
Method SetEntProt(lEntProt)
Method SetVICMSEn(nVlrICMSEn)
Method SetDocOrig(aDocOriApu)
Method SetVlrPauta(nVlrPauta)
Method SetNrLivro(cNrLivro)
Method SetUnid(cUnid)
Method SetnBASERET(nBASERET)
Method SetnBFCPANT(nBFCPANT)
Method SetnVFCPANT(nVFCPANT)
Method SetnAFCPANT(nAFCPANT)
Method SetnVFECPST(nVFECPST)
Method SetnALQFECP(nALQFECP)
Method SetnVUNIT(nVUNIT)
Method SetEfetivo()


Method GetDataMov()
Method GetTipoMov()
Method GetNumDoc()
Method GetItemDoc()
Method GetSerDoc()
Method GetEspDoc()
Method GetChvDoc()
Method GetNPDV()
Method GetTipoDoc()
Method GetCodProd()
Method GetCodPart()
Method GetTipoPar()
Method GetAliqInt()
Method GetCFOP()
Method GetCST()
Method GetFGerNR()
Method GetQtdade()
Method GetTotPrd()
Method GetFrete()
Method GetSeguro()
Method GetDespesa()
Method GetDescont()
Method GetBICMS()
Method GetVICMS()
Method GetVICMSST()
Method GetVICMSAn()
Method GetVBICMND()
Method GetVICMSND()
Method GetEntProt()
Method GetVICMSEn()
Method GetDocOrig()
Method GetVlrPauta()
Method GetNrLivro()
Method GetUnid()
Method GetnBASERET()
Method GetnBFCPANT()
Method GetnVFCPANT()
Method GetnAFCPANT()
Method GetnVFECPST()
Method GetnALQFECP()
Method GetnVUNIT()
Method GetEfetivo()

ENDCLASS


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} New()
  
M�todo construtor da Classe FISA302MOVIMENTO

@author Rafael.Soliveira
@since 23/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Method New() Class FISA302MOVIMENTO
    Self:ClearMov()
Return Self


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} EnquadMov()
  
M�todo que define o Enquadramento Legal da oper��o de sa�da, retornando:
0-Sa�da para comercializa��o subsequente e todas demais sa�das escrituradas no controle de estoque n�o elencadas nesta tabela. 
1-Sa�da a consumidor ou usu�rio final, conforme artigo 269 inciso I do RICMS/00. 
2-Fato gerador n�o realizado, conforme artigo 269 inciso II do RICMS/00. 
3-Sa�da ou sa�da subsequente amparada com isen��o ou n�o incid�ncia, conforme artigo 269 inciso III do RICMS/00.
4-Sa�da para outro estado, conforme artigo 269 inciso IV do RICMS/00.

"Nos casos de mercadoria recebida para comercializa��o vier a perecer, deteriorar-se ou for objeto de roubo, furto ou extravio, dever� emitir 
 nota fiscal de sa�da para baixa do estoque, sem destaque dos impostos com o CFOP 5.927. Dever� utilizar o c�digo de enquadramento 2. 
 Preferencialmente dever� emitir uma �nica nota fiscal por per�odo de refer�ncia, com todas as baixas ocorridas no per�odo.""

@author Rafael.Soliveira
@since 23/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Method EnquadMov() Class FISA302MOVIMENTO
    Local cCFOP      := Self:GetCFOP()
    Local cCST       := Self:GetCST()
    Local cFGerNReal := Self:GetFGerNR()
    Local cTipoPart  := Self:GetTipoPar()
    Local cTipoMov   := Self:GetTipoMov()
    Local nVlrPauta  := Self:GetVlrPauta()
    Local nQtdade    := Self:GetQtdade()
    Local nVlrTotPrd := Self:GetTotPrd()
    Local cRetorno   := '0'
    //Local nVldPauta  := IIf(ValType(MV_PAR02) = "N", MV_PAR02, 1)   

    If (cTipoMov == 'S' .And. Left(cCFOP,1) == '6') .Or. (cTipoMov == 'E' .And. Left(cCFOP,1) == '2')
        If cCST $ '30/40/41'
            cRetorno := '3'      //--- 3-Sa�da ou sa�da subsequente com isen��o ou n�o incid�ncia ---//
        Else
            cRetorno := '4'      //--- 4-Sa�da para outro Estado ---//
        EndIf
    Else
        If cFGerNReal == '1'
            cRetorno := '2'      //--- 2-Fato Gerador n�o realizado ---//
            //ElseIf (cTipoPart = 'F' .And. nVlrPauta > 0 .And. nVlrTotPrd <= (nQtdade*nVlrPauta) .And. nVldPauta == 1);
           //.Or. (cTipoPart = 'F' .And. nVldPauta == 2) 
        ElseIf cTipoPart = 'F'
            cRetorno := '1'      //--- 1-Consumidor ou Usu�rio Final ---//
        ElseIf cCST $ '30/40/41' //--- 3-Sa�da ou sa�da subsequente com isen��o ou n�o incid�ncia ---//
            cRetorno := '3'
        EndIf
    EndIf

Return cRetorno


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} DefVlrConf()
  
M�todo que define, para cada movimento analisado, o Valor de Confronto (Colunas 20 e 21 da Ficha 3).

@author Rafael.Soliveira
@since 23/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
/*
Method DefVlrConf(cCodEnquad) Class FISA302MOVIMENTO
    Local   nRetorno   := 0
    Local   cCodProd   := Self:GetCodProd()
    Local   nQtdade    := Self:GetQtdade()
    Local   dDataMov   := Self:GetDataMov()
    Local   nAliqInt   := Self:GetAliqInt()
    Local   nVlrBICMS  := Self:GetBICMS()
    Local   lEntProt   := Self:GetEntProt()
    Local   nVlrICMSEn := Self:GetVICMSEn()
    Local   oMovEnProt := Nil
    Default cCodEnquad := ''

    If cCodEnquad != '0'
        If cCodEnquad == '2' .Or.  cCodEnquad == '4'
            If lEntProt
                oMovEnProt := FISA302MOVIMENTOENTPROTHEUS():New()
                oMovEnProt:cTipoRet := 'V' //--- 'V'-Valor de Confronto ---//
                oMovEnProt:cCodProd := cCodProd
                oMovEnProt:nQtdade  := nQtdade
                oMovEnProt:dDataMov := dDataMov
                oMovEnProt:nAliqInt := nAliqInt
                oMovEnProt:DefICMSEnt()
                nRetorno := oMovEnProt:nVlrICMSCo
            Else
                nRetorno := nVlrICMSEn
            EndIf
        Else
            If nVlrBICMS = 0
                nVlrBICMS := Self:DefBaseICM()
            EndIf
            nRetorno := Round(((nVlrBICMS*nAliqInt)/100),2)
        EndIf
    EndIf

Return nRetorno
*/

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} DefVlrRC()

M�todo que define, para cada movimento analisado, o Valor de Ressarcimento ou Complemento
(Colunas 25 a 27 da Ficha 3).

@author Rafael.Soliveira
@since 03/12/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
/*
Method DefVlrRC(cCodEnquad,nVlrICMSSu,nVlrICMSCo) Class FISA302MOVIMENTO
    Local aRetorno := {}

    If nVlrICMSSu >= nVlrICMSCo
        aRetorno := {'R',nVlrICMSSu-nVlrICMSCo}
    Else
        If cCodEnquad == '1'
            aRetorno := {'C',nVlrICMSCo-nVlrICMSSu}
        EndIf
    EndIf

Return aRetorno
*/

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} DefBaseICM()
  
M�todo que define a correspondente Base de ICMS da opera��o.

@author Rafael.Soliveira
@since 27/11/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
/*
Method DefBaseICM() Class FISA302MOVIMENTO
    Local nRetorno   := 0
    Local nVlrTotPrd := Self:GetTotPrd()
    Local nVlrFrete  := Self:GetFrete()
    Local nVlrSeguro := Self:GetSeguro()
    Local nVlrDesp   := Self:GetDespesa()
    Local nVlrDesc   := Self:GetDescont()
    Local nQtdade    := Self:GetQtdade()
    Local nVlrPauta  := Self:GetVlrPauta()

    If nVlrPauta > 0
        nRetorno := (nQtdade * nVlrPauta) + nVlrFrete + nVlrSeguro + nVlrDesp - nVlrDesc
    Else
        nRetorno := nVlrTotPrd + nVlrFrete + nVlrSeguro + nVlrDesp - nVlrDesc
    EndIf

Return nRetorno
*/

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ClearMov
  
M�todo que limpa os valores do movimento.

@author Rafael.Soliveira
@since 12/11/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Method ClearMov() Class FISA302MOVIMENTO
    Self:dDataMov   := CToD("  /  /    ")
    Self:cTipoMov   := ""
    Self:cNumDoc    := ""
    Self:cItemDoc   := ""
    Self:cSerieDoc  := ""
    Self:cEspecDoc  := ""
    Self:cChaveDoc  := ""
    Self:cNumPDV    := ""
    Self:cTipoDoc   := ""
    Self:cCodProd   := ""
    Self:cCodPart   := ""
    Self:cTipoPart  := ""
    Self:nAliqInt   := 0
    Self:cCFOP      := ""
    Self:cCST       := ""
    Self:cFGerNReal := ""
    Self:nQtdade    := 0
    Self:nVlrTotPrd := 0
    Self:nVlrFrete  := 0
    Self:nVlrSeguro := 0
    Self:nVlrDesp   := 0
    Self:nVlrDesc   := 0
    Self:nVlrBICMS  := 0
    Self:nVlrICMS   := 0
    Self:nVlrICMSST := 0
    Self:nVlrICMSAn := 0
    Self:nVlrBICMND := 0
    Self:nVlrICMSND := 0
    Self:lEntProt   := .F.
    Self:nVlrICMSEn := 0
    Self:aDocOriApu := {}
    Self:nVlrPauta  := 0
    Self:cNrLivro   := ""
    Self:cCST       := ""
    Self:cUnid      := ""
    Self:nBASERET   := 0
    Self:nBFCPANT   := 0
    Self:nVFCPANT   := 0
    Self:nAFCPANT   := 0
    Self:nVFECPST   := 0
    Self:nALQFECP   := 0
    Self:nVUNIT     := 0
    Self:nEFETIVO   := 0
    
Return


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Getters e Setters

@author Rafael.Soliveira
@since 23/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Method SetDataMov(dDataMov) Class FISA302MOVIMENTO
    Self:dDataMov := dDataMov
Return

Method SetTipoMov(cTipoMov) Class FISA302MOVIMENTO
    Self:cTipoMov := cTipoMov
Return

Method SetNumDoc(cNumDoc) Class FISA302MOVIMENTO
    Self:cNumDoc := cNumDoc
Return

Method SetItemDoc(cItemDoc) Class FISA302MOVIMENTO
    Self:cItemDoc := cItemDoc
Return

Method SetSerDoc(cSerieDoc) Class FISA302MOVIMENTO
    Self:cSerieDoc := cSerieDoc
Return

Method SetEspDoc(cEspecDoc) Class FISA302MOVIMENTO
    Self:cEspecDoc := cEspecDoc
Return

Method SetChvDoc(cChaveDoc) Class FISA302MOVIMENTO
    Self:cChaveDoc := cChaveDoc
Return

Method SetNPDV(cNumPDV) Class FISA302MOVIMENTO
    Self:cNumPDV := cNumPDV
Return

Method SetTipoDoc(cTipoDoc) Class FISA302MOVIMENTO
    Self:cTipoDoc := cTipoDoc
Return

Method SetCodProd(cCodProd) Class FISA302MOVIMENTO
    Self:cCodProd := cCodProd
Return

Method SetCodPart(cCodPart) Class FISA302MOVIMENTO
    Self:cCodPart := cCodPart
Return

Method SetTipoPar(cTipoPart) Class FISA302MOVIMENTO
    Self:cTipoPart := cTipoPart
Return

Method SetAliqInt(nAliqInt) Class FISA302MOVIMENTO
    Self:nAliqInt := nAliqInt
Return

Method SetCFOP(cCFOP) Class FISA302MOVIMENTO
    Self:cCFOP := cCFOP
Return

Method SetCST(cCST) Class FISA302MOVIMENTO
    Self:cCST := cCST
Return

Method SetFGerNR(cFGerNReal) Class FISA302MOVIMENTO
    Self:cFGerNReal := cFGerNReal
Return

Method SetQtdade(nQtdade) Class FISA302MOVIMENTO
    Self:nQtdade := nQtdade
Return

Method SetTotPrd(nVlrTotPrd) Class FISA302MOVIMENTO
    Self:nVlrTotPrd := nVlrTotPrd
Return

Method SetFrete(nVlrFrete) Class FISA302MOVIMENTO
    Self:nVlrFrete := nVlrFrete
Return

Method SetSeguro(nVlrSeguro) Class FISA302MOVIMENTO
    Self:nVlrSeguro := nVlrSeguro
Return

Method SetDespesa(nVlrDesp) Class FISA302MOVIMENTO
    Self:nVlrDesp := nVlrDesp
Return

Method SetDescont(nVlrDesc) Class FISA302MOVIMENTO
    Self:nVlrDesc := nVlrDesc
Return

Method SetBICMS(nVlrBICMS) Class FISA302MOVIMENTO
    Self:nVlrBICMS := nVlrBICMS
Return

Method SetVICMS(nVlrICMS) Class FISA302MOVIMENTO
    Self:nVlrICMS := nVlrICMS
Return

Method SetVICMSST(nVlrICMSST) Class FISA302MOVIMENTO
    Self:nVlrICMSST := nVlrICMSST
Return

Method SetVICMSAn(nVlrICMSAn) Class FISA302MOVIMENTO
    Self:nVlrICMSAn := nVlrICMSAn
Return

Method SetVBICMND(nVlrBICMND) Class FISA302MOVIMENTO
    Self:nVlrBICMND := nVlrBICMND
Return

Method SetVICMSND(nVlrICMSND) Class FISA302MOVIMENTO
    Self:nVlrICMSND := nVlrICMSND
Return

Method SetEntProt(lEntProt) Class FISA302MOVIMENTO
    Self:lEntProt := lEntProt
Return

Method SetVICMSEn(nVlrICMSEn) Class FISA302MOVIMENTO
    Self:nVlrICMSEn := nVlrICMSEn
Return

Method SetDocOrig(aDocOriApu) Class FISA302MOVIMENTO
    Self:aDocOriApu := aDocOriApu
Return

Method SetVlrPauta(nVlrPauta) Class FISA302MOVIMENTO
    Self:nVlrPauta := nVlrPauta
Return

Method SetNrLivro(cNrLivro) Class FISA302MOVIMENTO
    Self:cNrLivro := cNrLivro
Return 

Method SetUnid(cUnid) Class FISA302MOVIMENTO
    Self:cUnid := cUnid
Return 

Method SetnBASERET(nBASERET) Class FISA302MOVIMENTO
    Self:nBASERET := nBASERET
Return

Method SetnBFCPANT(nBFCPANT) Class FISA302MOVIMENTO
    Self:nBFCPANT := nBFCPANT
Return

Method SetnVFCPANT(nVFCPANT) Class FISA302MOVIMENTO
    Self:nVFCPANT := nVFCPANT
Return

Method SetnAFCPANT(nAFCPANT) Class FISA302MOVIMENTO
    Self:nAFCPANT := nAFCPANT
Return

Method SetnVFECPST(nVFECPST) Class FISA302MOVIMENTO
    Self:nVFECPST := nVFECPST
Return

Method SetnALQFECP(nALQFECP) Class FISA302MOVIMENTO
    Self:nALQFECP := nALQFECP
Return

Method SetnVUNIT(nVUNIT) Class FISA302MOVIMENTO
    Self:nVUNIT := nVUNIT
Return

Method SetEfetivo(nEFETIVO) Class FISA302MOVIMENTO
    Self:nEFETIVO := nEFETIVO
Return 



Method GetDataMov() Class FISA302MOVIMENTO
Return Self:dDataMov

Method GetTipoMov() Class FISA302MOVIMENTO
Return Self:cTipoMov

Method GetNumDoc()  Class FISA302MOVIMENTO
Return Self:cNumDoc

Method GetItemDoc()  Class FISA302MOVIMENTO
Return Self:cItemDoc

Method GetSerDoc()  Class FISA302MOVIMENTO
Return Self:cSerieDoc

Method GetEspDoc()  Class FISA302MOVIMENTO
Return Self:cEspecDoc

Method GetChvDoc()  Class FISA302MOVIMENTO
Return Self:cChaveDoc

Method GetNPDV() Class FISA302MOVIMENTO
Return Self:cNumPDV

Method GetTipoDoc()  Class FISA302MOVIMENTO
Return Self:cTipoDoc

Method GetCodProd() Class FISA302MOVIMENTO
Return Self:cCodProd

Method GetCodPart() Class FISA302MOVIMENTO
Return Self:cCodPart

Method GetTipoPar() Class FISA302MOVIMENTO
Return Self:cTipoPart

Method GetAliqInt()  Class FISA302MOVIMENTO
Return Self:nAliqInt

Method GetCFOP() Class FISA302MOVIMENTO
Return Self:cCFOP

Method GetCST() Class FISA302MOVIMENTO
Return Self:cCST

Method GetFGerNR() Class FISA302MOVIMENTO
Return Self:cFGerNReal

Method GetQtdade() Class FISA302MOVIMENTO
Return Self:nQtdade

Method GetTotPrd() Class FISA302MOVIMENTO
Return Self:nVlrTotPrd

Method GetFrete() Class FISA302MOVIMENTO
Return Self:nVlrFrete

Method GetSeguro() Class FISA302MOVIMENTO
Return Self:nVlrSeguro

Method GetDespesa() Class FISA302MOVIMENTO
Return Self:nVlrDesp

Method GetDescont() Class FISA302MOVIMENTO
Return Self:nVlrDesc

Method GetBICMS() Class FISA302MOVIMENTO
Return Self:nVlrBICMS

Method GetVICMS() Class FISA302MOVIMENTO
Return Self:nVlrICMS

Method GetVICMSST() Class FISA302MOVIMENTO
Return Self:nVlrICMSST

Method GetVICMSAn() Class FISA302MOVIMENTO
Return Self:nVlrICMSAn

Method GetVBICMND() Class FISA302MOVIMENTO
Return Self:nVlrBICMND

Method GetVICMSND() Class FISA302MOVIMENTO
Return Self:nVlrICMSND

Method GetEntProt() Class FISA302MOVIMENTO
Return Self:lEntProt

Method GetVICMSEn() Class FISA302MOVIMENTO
Return Self:nVlrICMSEn

Method GetDocOrig() Class FISA302MOVIMENTO
Return Self:aDocOriApu

Method GetVlrPauta() Class FISA302MOVIMENTO
Return Self:nVlrPauta

Method GetNrLivro() Class FISA302MOVIMENTO
Return Self:cNrLivro

Method GetUnid() Class FISA302MOVIMENTO
Return Self:cUnid

Method GetnBASERET() Class FISA302MOVIMENTO
Return Self:nBASERET

Method GetnBFCPANT() Class FISA302MOVIMENTO
Return Self:nBFCPANT

Method GetnVFCPANT() Class FISA302MOVIMENTO
Return Self:nVFCPANT

Method GetnAFCPANT() Class FISA302MOVIMENTO
Return Self:nAFCPANT

Method GetnVFECPST() Class FISA302MOVIMENTO
Return Self:nVFECPST

Method GetnALQFECP() Class FISA302MOVIMENTO
Return Self:nALQFECP

Method GetnVUNIT() Class FISA302MOVIMENTO
Return Self:nVUNIT

Method GetEfetivo() Class FISA302MOVIMENTO
Return Self:nEFETIVO



//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Classe FISA302MOVIMENTOAPURACAO

Classe repons�vel por controlar os valores j� apurados para o movimento.
  
@author Rafael.Soliveira
@since 24/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
CLASS FISA302MOVIMENTOAPURACAO FROM LongClassName

Data nQtdade    As Numeric    //--- ---//
Data nVlrSupTot As Numeric    //--- ---//
Data nVlrSupUni As Numeric    //--- ---//
Data cCodEnquad As Character  //--- ---//
Data nConfSaida As Numeric    //--- ---//
Data nConfEnt   As Numeric    //--- ---//
Data nApurVlrR  As Numeric    //--- ---//
Data nApurVlrC  As Numeric    //--- ---//
Data nApurVlrCO As Numeric    //--- ---//
Data nApurVlrFC As Numeric    //--- ---//
Data nApurVlrFR As Numeric    //--- ---//
Data nSldQtdade As Numeric    //--- ---//
Data nSldVlrUni As Numeric    //--- ---//
Data nSldVlrTot As Numeric    //--- ---//

Data nSldVlrUST As Numeric
Data nSldVlrTST As Numeric
Data nSldVlrUBS As Numeric
Data nSldVlrTBS As Numeric
Data nSldVlrUFC As Numeric
Data nSldVlrTFC As Numeric

Data nMUCRED 	As Numeric		//--
Data nVREFCP 	As Numeric		//--
Data nVCMFCP 	As Numeric		//--
Data nVUCRED 	As Numeric		//--
Data nVURFCP 	As Numeric		//--
Data nVURET  	As Numeric		//--
Data nBURET  	As Numeric		//--
Data cCODRES 	As Character	//--
Data nVUCFC  	As Numeric		//--
Data nVUCST  	As Numeric		//--
Data nVURTFC 	As Numeric		//--
Data nVUREST 	As Numeric		//--
Data nMUVSF  	As Numeric		//--
Data nMUVSTF 	As Numeric		//--
Data nMUBST  	As Numeric		//--
Data nVUNIT  	As Numeric		//--





Method New() CONSTRUCTOR
Method ClearMovAp()

//---Getters e Setters---//
Method SetQtdade(nQtdade)
Method SetSupTot(nVlrSupTot)
Method SetSupUni(nVlrSupUni)
Method SetEnquad(cCodEnquad)
Method SetConfSai(nConfSaida)
Method SetConfEnt(nConfEnt)
Method SetApurRes(nApurVlrR)
Method SetApurCom(nApurVlrC)
Method SetApurCop(nApurVlrCO)
Method SetApurFC(nApurVlrFC)
Method SetApurFR(nApurVlrFR)
Method SetSldQtd(nSldQtdade)

Method SetSldUni(nSldVlrUni)
Method SetSldTot(nSldVlrTot)

Method SetSldUST(nSldVlrUST)
Method SetSldTST(nSldVlrTST)
Method SetSldUBS(nSldVlrUBS)
Method SetSldTBS(nSldVlrTBS)
Method SetSldUFC(nSldVlrUFC)
Method SetSldTFC(nSldVlrTFC)

Method SetnMUCRED(nMUCRED) 
Method SetnVREFCP(nVREFCP) 
Method SetnVCMFCP(nVCMFCP) 
Method SetnVUCRED(nVUCRED) 
Method SetnVURFCP(nVURFCP) 
Method SetnVURET(nVURET) 
Method SetnBURET(nBURET) 
Method SetcCODRES(cCODRES) 
Method SetnVUCFC(nVUCFC) 
Method SetnVUCST(nVUCST) 
Method SetnVURTFC(nVURTFC) 
Method SetnVUREST(nVUREST) 
Method SetnMUVSF(nMUVSF) 
Method SetnMUVSTF(nMUVSTF) 
Method SetnMUBST(nMUBST) 
Method SetnVUNIT(nVUNIT)


Method GetQtdade()
Method GetSupTot()
Method GetSupUni()
Method GetEnquad()
Method GetConfSai()
Method GetConfEnt()
Method GetApurRes()
Method GetApurCom()
Method GetApurCop()
Method GetApurFC()
Method GetApurFR()
Method GetSldQtd()
Method GetSldUni()
Method GetSldTot()

Method GetSldUST()
Method GetSldTST()
Method GetSldUBS()
Method GetSldTBS()
Method GetSldUFC()
Method GetSldTFC()

Method GetnMUCRED() 
Method GetnVREFCP() 
Method GetnVCMFCP() 
Method GetnVUCRED() 
Method GetnVURFCP() 
Method GetnVURET() 
Method GetnBURET() 
Method GetcCODRES() 
Method GetnVUCFC() 
Method GetnVUCST() 
Method GetnVURTFC() 
Method GetnVUREST() 
Method GetnMUVSF() 
Method GetnMUVSTF() 
Method GetnMUBST() 
Method GetnVUNIT()



ENDCLASS


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} New()
  
M�todo construtor da Classe FISA302MOVIMENTOAPURACAO

@author Rafael.Soliveira
@since 24/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Method New() Class FISA302MOVIMENTOAPURACAO
    Self:ClearMovAp()
Return Self


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Method ClearMovAp()
  
M�todo que limpa os valores apurados do movimento.

@author Rafael.Soliveira
@since 12/11/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Method ClearMovAp() Class FISA302MOVIMENTOAPURACAO
    Self:nQtdade    := 0
    Self:nVlrSupTot := 0
    Self:nVlrSupUni := 0
    Self:cCodEnquad := ""
    Self:nConfSaida := 0
    Self:nConfEnt   := 0
    Self:nApurVlrR  := 0
    Self:nApurVlrC  := 0
    Self:nApurVlrCO := 0
    Self:nApurVlrFC := 0
    Self:nApurVlrFR := 0
    Self:nSldQtdade := 0
    Self:nSldVlrUni := 0
    Self:nSldVlrTot := 0

    Self:nSldVlrUST := 0
    Self:nSldVlrTST := 0
    Self:nSldVlrUBS := 0
    Self:nSldVlrTBS := 0
    Self:nSldVlrUFC := 0
    Self:nSldVlrTFC := 0
    
    Self:nMUCRED 	:= 0
    Self:nVREFCP 	:= 0
    Self:nVCMFCP 	:= 0
    Self:nVUCRED 	:= 0
    Self:nVURFCP 	:= 0
    Self:nVURET  	:= 0
    Self:nBURET  	:= 0
    Self:cCODRES 	:= ""  
    Self:nVUCFC  	:= 0
    Self:nVUCST  	:= 0
    Self:nVURTFC 	:= 0
    Self:nVUREST 	:= 0
    Self:nMUVSF  	:= 0
    Self:nMUVSTF 	:= 0
    Self:nMUBST  	:= 0
    Self:nVUNIT  	:= 0
    
Return


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Getters e Setters

@author Rafael.Soliveira
@since 24/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Method SetQtdade(nQtdade) Class FISA302MOVIMENTOAPURACAO
    Self:nQtdade := nQtdade
Return

Method SetSupTot(nVlrSupTot) Class FISA302MOVIMENTOAPURACAO
    Self:nVlrSupTot := nVlrSupTot
Return

Method SetSupUni(nVlrSupUni) Class FISA302MOVIMENTOAPURACAO
    Self:nVlrSupUni := nVlrSupUni
Return

Method SetEnquad(cCodEnquad) Class FISA302MOVIMENTOAPURACAO
    Self:cCodEnquad := cCodEnquad
Return

Method SetConfSai(nConfSaida) Class FISA302MOVIMENTOAPURACAO
    Self:nConfSaida := nConfSaida
Return

Method SetConfEnt(nConfEnt) Class FISA302MOVIMENTOAPURACAO
    Self:nConfEnt := nConfEnt
Return

Method SetApurRes(nApurVlrR) Class FISA302MOVIMENTOAPURACAO
    Self:nApurVlrR := nApurVlrR
Return

Method SetApurCom(nApurVlrC) Class FISA302MOVIMENTOAPURACAO
    Self:nApurVlrC := nApurVlrC
Return

Method SetApurCop(nApurVlrCO) Class FISA302MOVIMENTOAPURACAO
    Self:nApurVlrCO := nApurVlrCO
Return

Method SetApurFC(nApurVlrFC) Class FISA302MOVIMENTOAPURACAO
    Self:nApurVlrFC := nApurVlrFC
Return

Method SetApurFR(nApurVlrFR) Class FISA302MOVIMENTOAPURACAO
    Self:nApurVlrFR := nApurVlrFR
Return

Method SetSldQtd(nSldQtdade) Class FISA302MOVIMENTOAPURACAO
    Self:nSldQtdade := nSldQtdade
Return

Method SetSldUni(nSldVlrUni) Class FISA302MOVIMENTOAPURACAO
    Self:nSldVlrUni := nSldVlrUni
Return

Method SetSldTot(nSldVlrTot) Class FISA302MOVIMENTOAPURACAO
    Self:nSldVlrTot := nSldVlrTot
Return

Method SetnMUCRED(nMUCRED)  Class FISA302MOVIMENTOAPURACAO
    Self:nMUCRED   := nMUCRED
Return

Method SetnVREFCP(nVREFCP)  Class FISA302MOVIMENTOAPURACAO
    Self:nVREFCP   := nVREFCP
Return

Method SetnVCMFCP(nVCMFCP)  Class FISA302MOVIMENTOAPURACAO
    Self:nVCMFCP   := nVCMFCP 
Return

Method SetnVUCRED(nVUCRED)  Class FISA302MOVIMENTOAPURACAO
    Self:nVUCRED   := nVUCRED
Return

Method SetnVURFCP(nVURFCP)  Class FISA302MOVIMENTOAPURACAO
    Self:nVURFCP   := nVURFCP
Return

Method SetnVURET(nVURET)    Class FISA302MOVIMENTOAPURACAO
    Self:nVURET   := nVURET 
Return

Method SetnBURET(nBURET)    Class FISA302MOVIMENTOAPURACAO
    Self:nBURET   := nBURET 
Return

Method SetcCODRES(cCODRES)  Class FISA302MOVIMENTOAPURACAO
    Self:cCODRES   := cCODRES 
Return

Method SetnVUCFC(nVUCFC)    Class FISA302MOVIMENTOAPURACAO
    Self:nVUCFC   := nVUCFC
Return

Method SetnVUCST(nVUCST)    Class FISA302MOVIMENTOAPURACAO
    Self:nVUCST   := nVUCST
Return

Method SetnVURTFC(nVURTFC)  Class FISA302MOVIMENTOAPURACAO
    Self:nVURTFC   := nVURTFC
Return

Method SetnVUREST(nVUREST)  Class FISA302MOVIMENTOAPURACAO
    Self:nVUREST   := nVUREST
Return

Method SetnMUVSF(nMUVSF)    Class FISA302MOVIMENTOAPURACAO
    Self:nMUVSF   := nMUVSF
Return

Method SetnMUVSTF(nMUVSTF)  Class FISA302MOVIMENTOAPURACAO
    Self:nMUVSTF   := nMUVSTF
Return

Method SetnMUBST(nMUBST)    Class FISA302MOVIMENTOAPURACAO
    Self:nMUBST   := nMUBST
Return

Method SetnVUNIT(nVUNIT)    Class FISA302MOVIMENTOAPURACAO
    Self:nVUNIT   := nVUNIT
Return

Method SetSldUST(nSldVlrUST) Class FISA302MOVIMENTOAPURACAO
    Self:nSldVlrUST := nSldVlrUST
Return

Method SetSldTST(nSldVlrTST) Class FISA302MOVIMENTOAPURACAO
    Self:nSldVlrTST := nSldVlrTST
Return

Method SetSldUBS(nSldVlrUBS) Class FISA302MOVIMENTOAPURACAO
    Self:nSldVlrUBS := nSldVlrUBS
Return

Method SetSldTBS(nSldVlrTBS) Class FISA302MOVIMENTOAPURACAO
    Self:nSldVlrTBS := nSldVlrTBS
Return

Method SetSldUFC(nSldVlrUFC) Class FISA302MOVIMENTOAPURACAO
    Self:nSldVlrUFC := nSldVlrUFC
Return

Method SetSldTFC(nSldVlrTFC) Class FISA302MOVIMENTOAPURACAO
    Self:nSldVlrTFC := nSldVlrTFC
Return





Method GetQtdade() Class FISA302MOVIMENTOAPURACAO
Return Self:nQtdade

Method GetSupTot() Class FISA302MOVIMENTOAPURACAO
Return Self:nVlrSupTot

Method GetSupUni() Class FISA302MOVIMENTOAPURACAO
Return Self:nVlrSupUni

Method GetEnquad() Class FISA302MOVIMENTOAPURACAO
Return Self:cCodEnquad

Method GetConfSai() Class FISA302MOVIMENTOAPURACAO
Return Self:nConfSaida

Method GetConfEnt() Class FISA302MOVIMENTOAPURACAO
Return Self:nConfEnt

Method GetApurRes() Class FISA302MOVIMENTOAPURACAO
Return Self:nApurVlrR

Method GetApurCom() Class FISA302MOVIMENTOAPURACAO
Return Self:nApurVlrC

Method GetApurCop() Class FISA302MOVIMENTOAPURACAO
Return Self:nApurVlrCO

Method GetApurFC() Class FISA302MOVIMENTOAPURACAO
Return Self:nApurVlrFC

Method GetApurFR() Class FISA302MOVIMENTOAPURACAO
Return Self:nApurVlrFR

Method GetSldQtd() Class FISA302MOVIMENTOAPURACAO
Return Self:nSldQtdade

Method GetSldUni() Class FISA302MOVIMENTOAPURACAO
Return Self:nSldVlrUni

Method GetSldTot() Class FISA302MOVIMENTOAPURACAO
Return Self:nSldVlrTot

Method GetnMUCRED() Class FISA302MOVIMENTOAPURACAO
return Self:nMUCRED

Method GetnVREFCP() Class FISA302MOVIMENTOAPURACAO
return Self:nVREFCP

Method GetnVCMFCP() Class FISA302MOVIMENTOAPURACAO
return Self:nVCMFCP

Method GetnVUCRED() Class FISA302MOVIMENTOAPURACAO
return Self:nVUCRED

Method GetnVURFCP() Class FISA302MOVIMENTOAPURACAO
return Self:nVURFCP

Method GetnVURET()  Class FISA302MOVIMENTOAPURACAO
return Self:nVURET

Method GetnBURET()  Class FISA302MOVIMENTOAPURACAO
return Self:nBURET

Method GetcCODRES() Class FISA302MOVIMENTOAPURACAO
return Self:cCODRES

Method GetnVUCFC()  Class FISA302MOVIMENTOAPURACAO
return Self:nVUCFC

Method GetnVUCST()  Class FISA302MOVIMENTOAPURACAO
return Self:nVUCST

Method GetnVURTFC() Class FISA302MOVIMENTOAPURACAO
return Self:nVURTFC

Method GetnVUREST() Class FISA302MOVIMENTOAPURACAO
return Self:nVUREST

Method GetnMUVSF()  Class FISA302MOVIMENTOAPURACAO
return Self:nMUVSF

Method GetnMUVSTF() Class FISA302MOVIMENTOAPURACAO
return Self:nMUVSTF

Method GetnMUBST()  Class FISA302MOVIMENTOAPURACAO
return Self:nMUBST

Method GetnVUNIT()  Class FISA302MOVIMENTOAPURACAO
return Self:nVUNIT

Method GetSldUST() Class FISA302MOVIMENTOAPURACAO
Return Self:nSldVlrUST

Method GetSldTST() Class FISA302MOVIMENTOAPURACAO
Return Self:nSldVlrTST

Method GetSldUBS() Class FISA302MOVIMENTOAPURACAO
Return Self:nSldVlrUBS

Method GetSldTBS() Class FISA302MOVIMENTOAPURACAO
Return Self:nSldVlrTBS

Method GetSldUFC() Class FISA302MOVIMENTOAPURACAO
Return Self:nSldVlrUFC

Method GetSldTFC() Class FISA302MOVIMENTOAPURACAO
Return Self:nSldVlrTFC



//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Classe FISA302MOVIMENTOENTPROTHEUS
  
@author Rafael.Soliveira
@since 30/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
CLASS FISA302MOVIMENTOENTPROTHEUS FROM LongClassName

Data cTipoRet   As Character //---C: Carga Inicial | V: Valor de Confronto---//
Data cCodProd   As Character //------//
Data nQtdade    As Numeric   //------//
Data dDataMov   As Date      //------//
Data nAliqInt   As Numeric   //------//
Data nVlrICMSCo As Numeric   //------//
Data nVlrICMSSu As Numeric   //------//
Data aSldVlrDet As Array     //Array com dados da nota de entrada
Data nVlrBSST   As Numeric   //Valor da base de calculo do ICMS ST
Data nVlrICMSST As Numeric   //Valor do ICMS ST
Data nVlrFECP   As Numeric   //Valor do FECP

Method New() CONSTRUCTOR
Method DefICMSEnt()

//---Getters e Setters---//

ENDCLASS


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} New()
  
M�todo construtor da Classe FISA302MOVIMENTOENTPROTHEUS

@author Rafael.Soliveira
@since 30/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Method New() Class FISA302MOVIMENTOENTPROTHEUS
    Self:cTipoRet   := ""
    Self:cCodProd   := ""
    Self:nQtdade    := 0
    Self:dDataMov   := CToD("  /  /    ")
    Self:nAliqInt   := 0
    Self:nVlrICMSCo := 0
    Self:nVlrICMSSu := 0
    Self:aSldVlrDet := {}
    Self:nVlrBSST   := 0
    Self:nVlrICMSST := 0
    Self:nVlrFECP   := 0
Return Self


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} DefICMSEnt()
  
M�todo que define o Valor de Confronto � partir das Entradas. � tamb�m acionado para compor a Carga 
Inicial de Saldos.

@author Rafael.Soliveira
@since 30/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Method DefICMSEnt() Class FISA302MOVIMENTOENTPROTHEUS
    Local cTipoRet    := Self:cTipoRet
    Local cCodProd    := Self:cCodProd
    Local nQtdade     := Self:nQtdade
    Local dDataMov    := Self:dDataMov
    Local nAliqInt    := Self:nAliqInt
    Local aArea  	  := GetArea()
    Local cAlias 	  := GetNextAlias()
    Local cAliasSFT   := ''
    Local dDataDe     := FirstDay(dDataMov)
    Local dDataAte    := dDataMov
    Local lAchouFT	  := .F.
    Local aSldVlrDet  := {}
    Local nRetorno    := 0
    Local nQtdadePro  := 0
    Local nValorPro   := 0

    Local nVlrST      := 0 
    Local nVlrFCP     := 0 
    Local nVlrBST     := 0 
    Local nRetBST     := 0
    Local nRetVlST    := 0
    Local nRetFECP    := 0

    Local nVlrICMS    := 0
    Local nVlrICMSST  := 0
    Local nVlrFECP    := 0
    Local nVlrBSST    := 0
    Local cSGBD       := TCGetDB()
    Local cSubStrBD   := ''
    Local cCODRES     := ''
    Local nCalVlrICM  := 0
    Local nQtANT      := 0

    //---Verifica se existe quantidade suficiente na movimenta��o de entrada para compor valores para o produto---//
    If cSGBD = 'ORACLE'
        cSubStrBD := 'SUBSTR(SFT.FT_CLASFIS,2,2)'
    Else
        cSubStrBD := 'RIGHT(SFT.FT_CLASFIS,2)'
    EndIf
    cSubStrBD := "%" + cSubStrBD + "%"

    BeginSql Alias cAlias
        SELECT SUM(SFT.FT_QUANT) QUANT
        FROM %TABLE:SFT% SFT
        INNER JOIN %TABLE:CIJ% CIJ ON (CIJ.CIJ_FILIAL = %XFILIAL:CIJ% AND CIJ.CIJ_CFOP  = SFT.FT_CFOP    AND CIJ.%NOTDEL%)
        INNER JOIN %TABLE:CIK% CIK ON (CIK.CIK_FILIAL = %XFILIAL:CIK% AND CIK.CIK_IDTAB = CIJ.CIJ_IDTAB  AND CIK.CIK_CSTICM = %Exp:cSubStrBD% AND CIK.%NOTDEL%)
        WHERE  SFT.FT_FILIAL=%XFILIAL:SFT%                  AND
               SFT.FT_PRODUTO = %EXP:cCodProd%              AND
               SFT.FT_TIPO NOT IN ('D','B','S','P','I','C') AND
               SFT.FT_TIPOMOV  = 'E'                        AND
               SFT.FT_DTCANC   = ''                         AND
               SFT.FT_ENTRADA  <=  %EXP:dDataAte%           AND
               SFT.FT_NFORI    = ' '                        AND
               SFT.FT_SERORI   = ' '                        AND
               SFT.FT_ITEMORI  = ' '                        AND
               SFT.%NOTDEL%	
    EndSql

    DbSelectArea(cAlias)
    If (cAlias)->QUANT >= nQtdade
    	lAchouFT := .T.	
    EndIf
    (cAlias)->(DbCloseArea()) 

    If lAchouFT

    	While nQtdade > 0
            cAliasSFT := GetNextAlias()

            BeginSql Alias cAliasSFT
                SELECT  SFT.FT_NFISCAL, 
                        SFT.FT_SERIE, 
                        SFT.FT_CLIEFOR, 
                        SFT.FT_LOJA, 
                        SFT.FT_ITEM, 
                        SFT.FT_PRODUTO, 
                        SFT.FT_CFOP,
                        SFT.FT_VALANTI,                         
                        CASE 
                            WHEN %Exp:cSubStrBD% = '60' AND (SFT.FT_ICMSRET+SFT.FT_VALANTI) = 0
                            THEN 0
                            ELSE CASE 
                                     WHEN SFT.FT_VALICM = 0 AND (SFT.FT_BASEICM+SFT.FT_OUTRICM) > 0
                                     THEN SD1.D1_VALICM 
                                     ELSE SFT.FT_VALICM
                                 END
                        END                                                                      AS VALORICMS,
                        CASE
                            WHEN SFT.FT_VALANTI > 0
                            THEN SFT.FT_VALANTI                                   
                            ELSE SFT.FT_ICMSRET 
                        END                                                                      AS VALORST,
                        CASE 
                            WHEN %Exp:cSubStrBD% = '60' AND (SFT.FT_ICMSRET+SFT.FT_VALANTI) = 0
                            THEN SFT.FT_BASNDES
                            ELSE 0
                        END                                                                      AS BASEANT,
                        CASE 
                            WHEN %Exp:cSubStrBD% = '60' AND (SFT.FT_ICMSRET+SFT.FT_VALANTI) = 0
                            THEN SFT.FT_ICMNDES
                            ELSE 0
                        END                                                                      AS VALORANT,
                        SD1.D1_UM                                                                UNID,
                        (SFT.FT_TOTAL+SFT.FT_FRETE+SFT.FT_SEGURO+SFT.FT_DESPESA-SFT.FT_DESCONT)  AS BASE, 
                        SFT.FT_QUANT                                                             AS QUANT,                        
                        SFT.FT_BASERET                                                           AS BASERET,
                        SD1.D1_BFCPANT                                                           AS BASEFCPANT,           
                        SD1.D1_VFCPANT                                                           AS VALFECPANT,
                        SD1.D1_AFCPANT                                                           AS ALIQFCPANT,
                        SD1.D1_VFECPST                                                           AS VALFECPST,
                        SD1.D1_ALQFECP                                                           AS ALIQFECP,
                        SD1.D1_VUNIT                                                             AS VUNIT                        
                FROM  %TABLE:SFT% SFT INNER JOIN %TABLE:SD1% SD1 ON (SD1.D1_FILIAL  = %XFILIAL:SD1% AND SD1.D1_DOC    = SFT.FT_NFISCAL AND SD1.D1_SERIE = SFT.FT_SERIE AND SD1.D1_FORNECE = SFT.FT_CLIEFOR AND SD1.D1_LOJA = SFT.FT_LOJA AND SD1.D1_COD = SFT.FT_PRODUTO AND SD1.D1_ITEM = SFT.FT_ITEM AND SD1.%NOTDEL%)
                                      INNER JOIN %TABLE:CIJ% CIJ ON (CIJ.CIJ_FILIAL = %XFILIAL:CIJ% AND CIJ.CIJ_CFOP  = SFT.FT_CFOP    AND CIJ.%NOTDEL%)
                                      INNER JOIN %TABLE:CIK% CIK ON (CIK.CIK_FILIAL = %XFILIAL:CIK% AND CIK.CIK_IDTAB = CIJ.CIJ_IDTAB  AND CIK.CIK_CSTICM = %Exp:cSubStrBD% AND CIK.%NOTDEL%)
                WHERE  SFT.FT_FILIAL=%XFILIAL:SFT%                       AND
                       SFT.FT_PRODUTO = %EXP:cCodProd%                   AND
                       SFT.FT_TIPO NOT IN ('D','B','S','P','I','C')      AND
                       SFT.FT_TIPOMOV  = 'E'                             AND
                       SFT.FT_DTCANC   = ''		                         AND
                       SFT.FT_ENTRADA >=  %EXP:dDataDe%                  AND
                       SFT.FT_ENTRADA <=  %EXP:dDataAte%                 AND
                       SFT.FT_NFORI    = ' '                             AND
                       SFT.FT_SERORI   = ' '                             AND
                       SFT.FT_ITEMORI  = ' '                             AND
                       SFT.%NOTDEL%
                ORDER BY SFT.FT_ENTRADA DESC, SD1.D1_NUMSEQ DESC
            EndSql 

            DbSelectArea(cAliasSFT)
            While !(cAliasSFT)->(Eof()) .And. nQtdade > 0

                If cTipoRet == 'C'

                    //ICMS EFETIVO DA ENTRADA
                    If (cAliasSFT)->VALORICMS > 0
                        nCalVlrICM := nVlrICMS := (cAliasSFT)->VALORICMS
                    Else
                        nVlrICMS := nCalVlrICM := (((cAliasSFT)->BASE * (nAliqInt+(cAliasSFT)->ALIQFECP)) / 100)
                    Endif

                    //BASE DO ICMS ST
                    If (cAliasSFT)->BASERET > 0 .and. (cAliasSFT)->BASEANT == 0
                        nVlrBSST := (cAliasSFT)->BASERET
                    Else
                        nVlrBSST := (cAliasSFT)->BASEANT
                    EndIf

                    //VALOR DO ICMS ST
                    If (cAliasSFT)->VALORST > 0 .And. (cAliasSFT)->VALORANT == 0
                        nVlrICMSST := (cAliasSFT)->VALORST
                    Else
                        nVlrICMSST := Iif((cAliasSFT)->VALORANT > 0 , (cAliasSFT)->VALORANT, 0)
                        
                        If nVlrICMSST == 0 
                            nVlrICMSST :=  Round((((cAliasSFT)->BASEANT*(nAliqInt+(cAliasSFT)->ALIQFCPANT))/100),2) - nCalVlrICM
                        EndIf

                    EndIf

                    //VALOR DO FECP
                    If  (cAliasSFT)->VALFECPST > 0 .and. (cAliasSFT)->VALFECPANT == 0
                        nVlrFECP := (cAliasSFT)->VALFECPST
                    Else                        
                        nVlrFECP := Iif((cAliasSFT)->VALFECPANT >0 ,(cAliasSFT)->VALFECPANT,0)                        

                        If nVlrFECP == 0 .and. (cAliasSFT)->BASEFCPANT > 0 .And. ALIQFCPANT > 0
                            nVlrFECP := Round((((cAliasSFT)->BASEFCPANT*(cAliasSFT)->ALIQFCPANT)/100),2)
                        EndIf

                    EndIf

                    If (cAliasSFT)->BASEANT > 0
                        cCODRES :='2'
                    ElseIf (cAliasSFT)->FT_VALANTI > 0
                        cCODRES :='3'
                    Else
                        cCODRES :='1'
                    EndIf  

                Else
                    nVlrICMS := Iif((cAliasSFT)->VALORICMS > 0, (cAliasSFT)->VALORICMS, (((cAliasSFT)->BASE * nAliqInt) / 100))
                EndIf

                If (cAliasSFT)->QUANT <= nQtdade
                    nValorPro  := nVlrICMS
                    nQtdadePro := (cAliasSFT)->QUANT
                    nVlrST     := nVlrICMSST
                    nVlrFCP    := nVlrFECP
                    nVlrBST    := nVlrBSST

                Else
                    nValorPro  := (nVlrICMS / (cAliasSFT)->QUANT) * nQtdade
                    nQtdadePro := nQtdade
                    nVlrST     := (nVlrICMSST / (cAliasSFT)->QUANT) * nQtdade
                    nVlrFCP    := (nVlrFECP / (cAliasSFT)->QUANT) * nQtdade
                    nVlrBST    := (nVlrBSST / (cAliasSFT)->QUANT) * nQtdade
                EndIf
                nRetorno += nValorPro
                nRetBST  += nVlrBST
                nRetVlST += nVlrST
                nRetFECP += nVlrFCP
                nQtdade  -= nQtdadePro
                nQtANT += nQtdadePro

                If cTipoRet == 'C'
                    aAdd(aSldVlrDet,{(cAliasSFT)->FT_NFISCAL,;          //1
                                     (cAliasSFT)->FT_SERIE,;
                                     (cAliasSFT)->FT_CLIEFOR,;
                                     (cAliasSFT)->FT_LOJA,;
                                     (cAliasSFT)->FT_ITEM,;             //5
                                     (cAliasSFT)->FT_PRODUTO,; 
                                     (cAliasSFT)->FT_CFOP,; 
                                     nQtANT,; 
                                     Round((nValorPro/nQtdadePro),2),;
                                     nValorPro,;                        //10
                                     Round((nVlrST/nQtdadePro),2),;
                                     nVlrST,;
                                     Round((nVlrFCP/nQtdadePro),2),;
                                     nVlrFCP,;
                                     Round((nVlrBST/nQtdadePro),2),;    //15
                                     nVlrBST,;
                                     cCODRES,;
                                    (cAliasSFT)->VUNIT,;
                                    (cAliasSFT)->QUANT,;
                                    nCalVlrICM/nQtdadePro})                //20
                EndIf

                (cAliasSFT)->(DbSkip())
            EndDo
            (cAliasSFT)->(DbCloseArea()) 

            //---Per�odo Anterior---//
            dDataDe	 := FirstDay(dDataDe-1)
            dDataAte := LastDay(dDataDe)
    	EndDo

        If cTipoRet == 'C'
            Self:nVlrICMSSu := nRetorno
            Self:nVlrBSST   := nRetBST
            Self:nVlrICMSST := nRetVlST
            Self:nVlrFECP   := nRetFECP
            Self:aSldVlrDet := aSldVlrDet
        Else
            Self:nVlrICMSCo := nRetorno
        EndIf
    
    EndIf

    RestArea(aArea)

Return
