#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FISA302C.CH"

/*
Fonte responsavel pela Tela inicial -- FISA192
*/

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} FISA302C
  
Rotina de Apura��o do Ressarcimento ou Complemento do ICMS Retido por Substitui��o Tribut�ria ou Antecipado.
Para o Estado de S�o Paulo, o m�todo de apura��o � determinado pela portaria CAT 42/2018.

@author Rafael.Soliveira
@since 08/11/2019
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Function FISA302C()
    Local aArea     := GetArea()
    Local cIdApur   := ''
    Local cPerApur  := ''
    Local lProcess  := .T.
	Local cMes      := ""
	Local cAno      := ""
    

    If AliasIndic("CIG") .and. AliasIndic("CIH") .and. AliasIndic("CII") .and. AliasIndic("CIJ") .and. AliasIndic("CIK") .and. AliasIndic("CIL") .and. AliasIndic("CIF") .and. AliasIndic("CIM") 
        //Faz Chamada para verifica��o da carga inicial dos cadastros
        

        If Pergunte("FISA302C",.T.)
            cPerApur := MV_PAR01

            If Len(MV_PAR01) < 6
                Help(Nil, Nil, "Help",, "O per�odo informado encontra-se fora do padr�o.Informe um per�odo no padr�o AAAAMM", 1, 0,,,,,, {cMsg})
                Return .F.
            EndIf
            //---Verifica a exist�ncia de apura��o no per�odo selecionado---//
            cIdApur := CheckApur(cPerApur)        

            If !Empty(cIdApur)
                If (ApMsgNoYes(STR0002 + CHR(10) + CHR(13) + STR0001 ) ) //"Apura��o j� realizada no per�odo selecionado." "Deseja fazer o reprocessamento?"
                    DeletApur(cIdApur,cPerApur)
                Else
                    lProcess := .F.
                EndIf
            EndIf

            If lProcess
                FwMsgRun(,{|oSay| FISA302CA(oSay,cPerApur)},STR0003,"") //"Apura��o do Ressarcimento do ICMS Retido por ST"
            EndIf

        EndIf
    EndIf

    RestArea(aArea)
Return


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} 
  
Rotina de Processamento da Apura��o.

@author Rafael.Soliveira
@since 08/11/2019
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Function FISA302CA(oSay,cPerApur)
    Local cAlias      := GetNextAlias()
    Local aCodRes     := Iif(Empty(SuperGetMv("MV_CODRESS",,"")),{"SP10090719","SP10090721"},&(SuperGetMv("MV_CODRESS",,{"SP10090719","SP10090721"}))) //---C�digos de Lan�amento de Ressarcimento---//
    Local cIdApur     := ''
    Local cPerSld     := ''
    Local dDataDe     := CtoD('  /  /    ')
    Local dDataAte    := CtoD('  /  /    ')
    Local cProduto    := ''
    Local nMVICMPAD   := SuperGetMV('MV_ICMPAD')
    Local nAliqInt    := 0
    Local cTipoPart   := ''
    Local aDocOriApu  := {}
    Local oApuracao   := Nil
    Local cSGBD       := TCGetDB()
    Local cSubStrBD   := ''
    Local oModel      := FWLoadModel('FISA302B') //---Model da rotina FISA302B---//

    Private lAutomato := IiF(IsBlind(),.T.,.F.)

    If lAutomato
        FwMsgRun(,{|oSay| FISA302Carga( oSay ) },STR0001,"") //"Processando carga inicial das Regras"
        FwMsgRun(,{|oSay| F302CCarga( oSay ) },STR0001,"") //"Processando carga inicial das Regras"
    EndIf

    AtualizaMsg(oSay,STR0004) //"Iniciando processamento..."

    cIdApur   := FWUUID("CII")
    dDataDe   := StoD(MV_PAR01+'01')
    dDataAte  := LastDay(dDataDe)

    //---Classe respons�vel pela apura��o do movimento---//
    oApuracao := FISA302APURACAO():New(cIdApur,cPerApur)

    AtualizaMsg(oSay,STR0005) //"Verificando movimento no per�odo..."

    //---Query Principal---//
    If cSGBD = 'ORACLE'
        cSubStrBD := 'SUBSTR(SFT.FT_CLASFIS,2,2)'
    Else
        cSubStrBD := 'RIGHT(SFT.FT_CLASFIS,2)'
    EndIf
    cSubStrBD := "%" + cSubStrBD + "%"
    
    BeginSql Alias cAlias
	    COLUMN FT_EMISSAO AS DATE
	    COLUMN FT_ENTRADA AS DATE
        COLUMN FT_DATAMOV AS DATE

        SELECT SFT.FT_PRODUTO             FT_PRODUTO,
	           SB1.B1_CRICMS              B1_CRICMS,
	           SB1.B1_PICM                B1_PICM,
	           ISNULL(CIL.CIL_PERIOD,'')  CIL_PERIOD,
	           ISNULL(CIL.CIL_QTDSLD,0)   CIL_QTDSLD,
	           ISNULL(CIL.CIL_MUST  ,0)   CIL_MUST  , 
	           ISNULL(CIL.CIL_TUST  ,0)   CIL_TUST  ,
               ISNULL(CIL.CIL_MICM  ,0)   CIL_MICM  ,
               ISNULL(CIL.CIL_MUBCST,0)   CIL_MUBCST,
               ISNULL(CIL.CIL_MIFC,0)     CIL_MIFC,
               ISNULL(CIL.CIL_TICM,0)     CIL_TICM,
               ISNULL(CIL.CIL_TIBCST,0)   CIL_TIBCST,
               ISNULL(CIL.CIL_TIFC,0)     CIL_TIFC,
               
	           CASE SFT.FT_TIPOMOV
                   WHEN 'E' THEN          FT_ENTRADA
		           ELSE                   FT_EMISSAO
               END                        FT_DATAMOV,
	           SFT.FT_TIPOMOV             FT_TIPOMOV,
	           SFT.FT_TIPO                FT_TIPO,
	           CASE SFT.FT_TIPOMOV
                   WHEN 'E' THEN 
                        CASE SFT.FT_TIPO 
                            WHEN 'D' THEN 
                                CASE 
                                    WHEN SFT.FT_ENTRADA = SF22.F2_EMISSAO THEN '5'
                                    ELSE				                       '2'
                                END
                            ELSE '1' 
                        END 
                    ELSE 
                        CASE SFT.FT_TIPO 
                            WHEN 'D' THEN '3' 
                            ELSE          '4' 
                        END 
               END                        FT_ORDEM,
               SFT.FT_NFISCAL             FT_NFISCAL,
               SFT.FT_SERIE               FT_SERIE,
               SFT.FT_ITEM                FT_ITEM,
               SFT.FT_ESPECIE             FT_ESPECIE,
	           SFT.FT_CHVNFE              FT_CHVNFE,
               SFT.FT_CLIEFOR             FT_CLIEFOR,
               SFT.FT_LOJA                FT_LOJA,
	           SFT.FT_CFOP                FT_CFOP,
               SFT.FT_CLASFIS             FT_CLASFIS,
	           SFT.FT_QUANT               FT_QUANT,
	           SFT.FT_TOTAL               FT_TOTAL,
	           SFT.FT_FRETE               FT_FRETE,
	           SFT.FT_SEGURO              FT_SEGURO,
	           SFT.FT_DESPESA             FT_DESPESA,
	           SFT.FT_DESCONT             FT_DESCONT,
	           SFT.FT_BASEICM             FT_BASEICM,
               SFT.FT_ESTOQUE             FT_ESTOQUE,
               SFT.FT_NRLIVRO             FT_NRLIVRO,
               SFT.FT_BASERET             FT_BASERET,
               SD1.D1_BFCPANT             D1_BFCPANT,
               SD1.D1_VFCPANT             D1_VFCPANT,
               SD1.D1_AFCPANT             D1_AFCPANT,
               SD1.D1_VFECPST             D1_VFECPST,
               SD1.D1_ALQFECP             D1_ALQFECP,
               SFT.FT_PRCUNIT             FT_PRCUNIT,
               SFT.FT_VICEFET             FT_VICEFET,
	           CASE 
	               WHEN %Exp:cSubStrBD% = '60' AND (SFT.FT_ICMSRET+SFT.FT_VALANTI) = 0
	               THEN 0
	               ELSE CASE 
	                        WHEN SFT.FT_BASEICM = 0 AND SFT.FT_OUTRICM > 0
	                        THEN CASE SFT.FT_TIPOMOV
	                                 WHEN 'E' 
	                                 THEN SD1.D1_VALICM
	                                 ELSE SD2.D2_VALICM
	                             END
	                        ELSE SFT.FT_VALICM 
	                    END
	           END                        FT_VALICM,
	           SFT.FT_ICMSRET             FT_ICMSRET,
	           SFT.FT_VALANTI             FT_VALANTI,
               CASE 
                    WHEN %Exp:cSubStrBD% = '60' AND (SFT.FT_ICMSRET+SFT.FT_VALANTI) = 0
                    THEN SFT.FT_BASNDES
                    ELSE 0
               END                        FT_BASNDES,
               CASE 
                    WHEN %Exp:cSubStrBD% = '60' AND (SFT.FT_ICMSRET+SFT.FT_VALANTI) = 0
                    THEN SFT.FT_ICMNDES
                    ELSE 0
               END                        FT_ICMNDES,
               CASE 
                    WHEN SFT.FT_TIPOMOV = 'S'
                    THEN SD2.D2_UM
                    ELSE SD1.D1_UM
               END                        UNID,               
               ISNULL(SA1.A1_TIPO,'')     A1_TIPO, 
               ISNULL(SF21.F2_TIPOCLI,'') F21_TIPOCLI, 
               ISNULL(SF22.F2_TIPOCLI,'') F22_TIPOCLI,  
	           CIJ.CIJ_FATGER             CIJ_FATGER,
	           //ISNULL(SFI.FI_SERPDV,'')   FI_SERPDV,
               SFT.FT_PDV                 FT_PDV,
	           SFT.FT_NFORI               FT_NFORI, 
	           SFT.FT_SERORI              FT_SERORI, 
	           SFT.FT_ITEMORI             FT_ITEMORI,
               SFT.FT_PAUTST              FT_PAUTST
        FROM  %table:SFT% SFT INNER JOIN      %table:SB1% SB1  ON (SB1.B1_FILIAL  = %xFilial:SB1% AND SB1.B1_COD     = SFT.FT_PRODUTO  AND SB1.%NotDel%)
                              INNER JOIN      %table:CIJ% CIJ  ON (CIJ.CIJ_FILIAL = %xFilial:CIJ% AND CIJ.CIJ_CFOP   = SFT.FT_CFOP     AND CIJ.%NotDel%)
                              INNER JOIN      %table:CIK% CIK  ON (CIK.CIK_FILIAL = %xFilial:CIK% AND CIK.CIK_IDTAB  = CIJ.CIJ_IDTAB   AND CIK.CIK_CSTICM = %Exp:cSubStrBD%         AND CIK.%NotDel%)
                              LEFT OUTER JOIN %table:CIL% CIL  ON (CIL.CIL_FILIAL = %xFilial:CIL% AND CIL.CIL_PERIOD = %Exp:cPerApur%  AND CIL.CIL_PRODUT = SB1.B1_COD              AND CIL.%NotDel%)
                              LEFT OUTER JOIN %table:SA1% SA1  ON (SA1.A1_FILIAL  = %xFilial:SA1% AND SA1.A1_COD     = SFT.FT_CLIEFOR  AND SA1.A1_LOJA    = SFT.FT_LOJA             AND ((SFT.FT_TIPOMOV='S' AND SFT.FT_TIPO NOT IN ('D','B')) OR (SFT.FT_TIPOMOV='E' AND SFT.FT_TIPO IN ('D','B')))              AND  SA1.%NotDel%)
				              LEFT OUTER JOIN %table:SF2% SF21 ON (SF21.F2_FILIAL = %xFilial:SF2% AND SF21.F2_DOC    = SFT.FT_NFISCAL  AND SF21.F2_SERIE  = SFT.FT_SERIE            AND SF21.F2_CLIENTE = SFT.FT_CLIEFOR AND SF21.F2_LOJA = SFT.FT_LOJA AND (SFT.FT_TIPOMOV='S' AND SFT.FT_TIPO NOT IN ('D','B')) AND SF21.%NotDel%)
				              LEFT OUTER JOIN %table:SF2% SF22 ON (SF22.F2_FILIAL = %xFilial:SF2% AND SF22.F2_DOC    = SFT.FT_NFORI    AND SF22.F2_SERIE  = SFT.FT_SERORI           AND SF22.F2_CLIENTE = SFT.FT_CLIEFOR AND SF22.F2_LOJA = SFT.FT_LOJA AND (SFT.FT_TIPOMOV='E' AND SFT.FT_TIPO='D')              AND SF22.%NotDel%)
                              //LEFT OUTER JOIN %table:SFI% SFI  ON (SFI.FI_FILIAL  = %xFilial:SFI% AND SFI.FI_DTMOVTO = SFT.FT_EMISSAO  AND SFI.FI_PDV     = SFT.FT_PDV              AND SFI.%NotDel%)
                              LEFT OUTER JOIN %table:SD1% SD1  ON (SD1.D1_FILIAL  = %xFilial:SD1% AND SD1.D1_DOC     = SFT.FT_NFISCAL  AND SD1.D1_SERIE   = SFT.FT_SERIE            AND SD1.D1_FORNECE = SFT.FT_CLIEFOR AND SD1.D1_LOJA = SFT.FT_LOJA AND SD1.D1_COD = SFT.FT_PRODUTO AND SD1.D1_ITEM = SFT.FT_ITEM AND SFT.FT_TIPOMOV = 'E' AND SD1.%NotDel%)
                              LEFT OUTER JOIN %table:SD2% SD2  ON (SD2.D2_FILIAL  = %xFilial:SD2% AND SD2.D2_DOC     = SFT.FT_NFISCAL  AND SD2.D2_SERIE   = SFT.FT_SERIE            AND SD2.D2_CLIENTE = SFT.FT_CLIEFOR AND SD2.D2_LOJA = SFT.FT_LOJA AND SD2.D2_COD = SFT.FT_PRODUTO AND SD2.D2_ITEM = SFT.FT_ITEM AND SFT.FT_TIPOMOV = 'S' AND SD2.%NotDel%) 
        WHERE SFT.FT_FILIAL   = %xFilial:SFT%  AND 
        	  SFT.FT_ENTRADA >= %Exp:dDataDe%  AND 
        	  SFT.FT_ENTRADA <= %Exp:dDataAte% AND
        	  SFT.FT_TIPO    <> 'S'            AND
        	  SFT.FT_DTCANC   = ''             AND
              SFT.%NOTDEL%                     AND
              SB1.B1_CRICMS   = '1'         
        ORDER BY SFT.FT_PRODUTO, FT_DATAMOV, FT_ORDEM, FT_CLIEFOR, FT_LOJA, FT_NFISCAL, FT_ITEM, FT_SERIE

    EndSql

    // FOI RETIRADO O TRANSACTION POIS ESTAVA DANDO IMPACTO QUANDO SE RODAVA PARA UMA QUANTIDADE GRADE DE REGISTROS.
    // A PROPRIA ROTINA FOI CRIADA JA COM UM CONTROLE DE SEGURAN�A CASO UM APURA��O DE PROBLEMA DURANTE O PROCESSO.
    //Begin Transaction

        //---Grava registro cabe�alho da apura��o (Tabela CIG)---//        
        GravaCIGT(oApuracao,1)

        AtualizaMsg(oSay,STR0006) //"Processando movimento..."

        DbSelectArea(cAlias)
        While !(cAlias)->(Eof())

            //DSERFIS1-16149 - Caso a TES utilizada seja Entrada para teste, CFOP 1949, CST 00 que N�O atualiza estoque
            If !(Alltrim((cAlias)->FT_CFOP) == '1949' .And. Right((cAlias)->FT_CLASFIS,2) == '00' .And. (cAlias)->FT_ESTOQUE  != 'S' .And. (cAlias)->FT_TIPOMOV == 'E')

                If cProduto != (cAlias)->FT_PRODUTO

                    //---Atualiza o saldo final do produto (Tabela CIL)---//
                    If !Empty(cProduto)
                        GravaCIL('1',oApuracao,Iif(Empty(cPerSld),.T.,.F.),,oModel)
                    EndIf

                    //---M�todo SetaSldIni: Carrega o saldo inicial do produto (Tabela CIL)---//
                    oApuracao:SetaSldIni((cAlias)->FT_PRODUTO,(cAlias)->CIL_QTDSLD,;
                                        (cAlias)->CIL_MICM,(cAlias)->CIL_TICM,;
                                        (cAlias)->CIL_MUBCST,(cAlias)->CIL_TIBCST,;
                                        (cAlias)->CIL_MUST,(cAlias)->CIL_TUST,;
                                        (cAlias)->CIL_MIFC,(cAlias)->CIL_TIFC)

                    //---Define a al�quota interna do ICMS para o produto---//
                    nAliqInt := Iif((cAlias)->B1_PICM>0, (cAlias)->B1_PICM, nMVICMPAD)

                EndIf

                //---Define Tipo do Participante (Cliente Final / Revendedor)---//
                If !Empty((cAlias)->F21_TIPOCLI)
                    cTipoPart := (cAlias)->F21_TIPOCLI
                ElseIf !Empty((cAlias)->F22_TIPOCLI)
                    cTipoPart := (cAlias)->F22_TIPOCLI            
                Else
                    cTipoPart := (cAlias)->A1_TIPO
                EndIf

                //---Valores apurados para o Documento Fiscal Original, em casos de movimentos de devolu��o---//
                aDocOriApu := aSize(aDocOriApu,0)
                If (cAlias)->FT_TIPO == 'D'
                    aDocOriApu := PesqApur((cAlias)->FT_TIPOMOV, (cAlias)->FT_NFORI, (cAlias)->FT_SERORI, (cAlias)->FT_ITEMORI, (cAlias)->FT_CLIEFOR, (cAlias)->FT_LOJA, (cAlias)->FT_PRODUTO, aCodRes)
                EndIf

                //---M�todo SetaMovim: Carrega os dados do movimento para que seja feita sua apura��o---//
                oApuracao:SetaMovim((cAlias)->FT_DATAMOV,;           //---dDataMov   - Data do Movimento
                                    (cAlias)->FT_TIPOMOV,;           //---cTipoMov   - Tipo do Movimento (E-Entrada / S-Sa�da)
                                    (cAlias)->FT_NFISCAL,;           //---cNumDoc    - N�mero do Documento Fiscal
                                    (cAlias)->FT_ITEM,;              //---cItemDoc   - Item do Documento Fiscal
                                    (cAlias)->FT_SERIE,;             //---cSerieDoc  - S�rie do Documento Fiscal
                                    (cAlias)->FT_ESPECIE,;           //---cEspecDoc  - Esp�cie do Documento Fiscal
                                    (cAlias)->FT_CHVNFE,;            //---cChaveDoc  - Chave do Documento Fiscal
                                    (cAlias)->FT_PDV,;               //---cNumPDV	 - N�mero do PDV Cupom fiscal
                                    (cAlias)->FT_TIPO,;              //---cTipoDoc   - Tipo do Documento (Normal / Devolu��o / Complemento)
                                    (cAlias)->FT_PRODUTO,;           //---cCodProd   - C�digo do Produto
                                    (cAlias)->(FT_CLIEFOR+FT_LOJA),; //---cCodPart   - C�digo do Participante
                                    cTipoPart,;                      //---cTipoPart  - Tipo do Participante (Cliente Final / Revendedor)
                                    nAliqInt,;                       //---nAliqInt   - Al�quota Interna do Produto
                                    (cAlias)->FT_CFOP,;              //---cCFOP      - CFOP
                                    Right((cAlias)->FT_CLASFIS,2),;  //---cCST       - CST ICMS
                                    (cAlias)->CIJ_FATGER,;           //---cFGerNReal - Indica se a opera��o (CFOP) deve ser enquadrada como 2-Fato Gerador n�o realizado
                                    (cAlias)->FT_QUANT,;             //---nQtdade    - Quantidade
                                    (cAlias)->FT_TOTAL,;             //---nVlrTotPrd - Valor Total do Produto
                                    (cAlias)->FT_FRETE,;             //---nVlrFrete  - Valor do Frete
                                    (cAlias)->FT_SEGURO,;            //---nVlrSeguro - Valor do Seguro
                                    (cAlias)->FT_DESPESA,;           //---nVlrDesp   - Valor das Despesas
                                    (cAlias)->FT_DESCONT,;           //---nVlrDesc   - Valor do Desconto
                                    (cAlias)->FT_BASEICM,;           //---nVlrBICMS  - Base de C�lculo do ICMS
                                    (cAlias)->FT_VALICM,;            //---nVlrICMS   - Valor do ICMS
                                    (cAlias)->FT_ICMSRET,;           //---nVlrICMSST - Valor do ICMS Retido por ST
                                    (cAlias)->FT_VALANTI,;           //---nVlrICMSAn - Valor do ICMS Antecipado
                                    (cAlias)->FT_BASNDES,;           //---nVlrBICMND - Base de C�lculo do ICMS Recolhido Anteriormente por ST em entradas CST 60
                                    (cAlias)->FT_ICMNDES,;           //---nVlrICMSND - Valor do ICMS do ICMS Recolhido Anteriormente por ST em entradas CST 60
                                    .T.,;                            //---lEntProt   - .T. -> Em sa�das interestaduais, busca ICMS suportado na entrada atrav�s da tabela SFT (Informar 0 no atributo nVlrICMSEn) / .F. -> N�o busca ICMS suportado na entrada atrav�s da tabela SFT (Informar o valor de ICMS suportado na entrada no atributo nVlrICMSEn)
                                    0,;                              //---nVlrICMSEn - Valor do ICMS suportado na entrada, usado como Valor de Confronto apenas para sa�das interestaduais
                                    aDocOriApu,;                     //---aDocOriApu - Valores apurados para o Documento Fiscal Original, em casos de movimentos de devolu��o
                                    (cAlias)->FT_PAUTST,;            //---nVlrPauta  - Valor da pauta informado no cadastro do produto no campo B1_VLR_ICM                                    
                                    (cAlias)->FT_NRLIVRO,;           //---cNrLivro   - Numero do livro da nota                                    
                                    (cAlias)->UNID,;                 //---cUnid      - primeira Unidade de medida da nota  
                                    (cAlias)->FT_BASERET,;           //---nBASERET   - Base ICMS Ret
                                    (cAlias)->D1_BFCPANT,;           //---nBFCPANT   - Base FECP recolhido anteriormente
                                    (cAlias)->D1_VFCPANT,;           //---nVFCPANT   - Valor FECP recolhido anteriormente
                                    (cAlias)->D1_AFCPANT,;           //---nAFCPANT   - Aliquota FECP recolhido anteriormente
                                    (cAlias)->D1_VFECPST,;           //---nVFECPST   - Valor Fecp ST
                                    (cAlias)->D1_ALQFECP,;           //---nALQFECP   - Aliquota Fecp ST
                                    (cAlias)->FT_PRCUNIT,;           //---nVUNIT     - Valor Unitario
                                    (cAlias)->FT_VICEFET)            //---nEfetivo   - ICMS Efetivo


                //---M�todo ApuraMovim: Para Entradas / Devolu��es de Entradas.: Define Total de ICMS Suportado / Atualiza o saldo do produto---//
                //---                   Para Sa�das............................: Define Total de ICMS Suportado / Atualiza o saldo do produto / Enquadra o Movimento / Define Valor de Confronto / Define Valor a Ressarcir ou Complementar---//
                //---                   Para Devolu��es de Sa�das..............: Define Total de ICMS Suportado (De acordo com mov. apurado original) / Atualiza o saldo do produto / Enquadra o Movimento (De acordo com mov. apurado original) / Define Valor de Confronto (De acordo com mov. apurado original) / Define Valor a Ressarcir ou Complementar (De acordo com mov. apurado original)---//
                //---                   Carrega os valores apurados no atributo oMovimApur---//
                oApuracao:ApuraMovim()

                //---Grava o movimento apurado (Tabela CII)---//
                GravaCII(oApuracao)

                cProduto := (cAlias)->FT_PRODUTO
                cPerSld  := (cAlias)->CIL_PERIOD
            EndIf

            (cAlias)->(DbSkip())
        EndDo
        (cAlias)->(DbCloseArea()) 
        //---FIM Query Principal---//

        AtualizaMsg(oSay,STR0007) //"Atualizando saldos..."

        //---Atualiza o saldo final do produto (Tabela CIL)---//
        GravaCIL('1',oApuracao,Iif(Empty(cPerSld),.T.,.F.),,oModel)

        //---Atualiza o saldo final dos produtos para os quais n�o houve movimento no per�odo apurado (Tabela CIL)---//
        GravaCIL('2',oApuracao,,cPerApur,oModel)
        
        AtualizaMsg(oSay,STR0008) //"Gravando apura��o..."

        //---Grava totalizadores da apura��o (Tabelas CIG e CIH)---//        
        GravaCIGT(oApuracao,2)

    //End Transaction

    oModel:Destroy()
    AtualizaMsg(oSay,STR0009) //"Processamento conclu�do."
Return


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} GravaCIL
  
Fun��o que atualiza, na tabela CIL, o saldo final dos produtos no per�odo analisado, al�m de transferir esse saldo final para o saldo inicial do pr�ximo per�odo.
� chamada em duas situa��es (par�metro cTipo):

1-De dentro do la�o da query de movimento (informando o par�metro oApuracao): Deve, nesse caso, atualizar saldos do produto setado no objeto oApuracao com o resultado da apura��o;
2-Ap�s o la�o da query de movimento (informando o par�metro cPerApur).......: Deve, nesse caso, atualizar saldos dos produtos que n�o tiveram movimento no per�odo atual, e, portanto, n�o entraram na query de movimento.

@author Rafael.Soliveira
@since 08/11/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Static Function GravaCIL(cTipo,oApuracao,lInsSldIni,cPerApur,oModel)
    Local aArea     := GetArea()
    Local cAlias    := GetNextAlias()
    Local cProxPer  := ''
    Local aDadosCIL := {}
    Local nCount    := 0
    Local nCount2   := 0
    Local nPos      := 0
    Local nPos2     := 0
    Local lCILOk    := .T.

    

    If cTipo == '1'
        If Empty(oApuracao:oSaldoProd:GetCodProd())
            Return
        EndIf
        If lInsSldIni
            //---Caso n�o exista saldo inicial do produto no per�odo analisado, cria registro com saldo inical 0 e atualiza o saldo final (tabela CIL)---//
            Aadd(aDadosCIL,{3,                                                ;
                            {{'CIL_PERIOD',oApuracao:GetAnoMes()            },;
                             {'CIL_PRODUT',oApuracao:oSaldoProd:GetCodProd()},;
                             {'CIL_QTDFIM',oApuracao:oSaldoProd:GetQtdade() },;
                             {'CIL_MFICM',oApuracao:oSaldoProd:GetVlrUnit() },;
                             {'CIL_TFICM',oApuracao:oSaldoProd:GetVlrTot()  },;                             
                             {'CIL_MFST',oApuracao:oSaldoProd:GetnVUntST()  },;
                             {'CIL_TFST',oApuracao:oSaldoProd:GetVlSdTST()  },;                            
                             {'CIL_MFBCST',oApuracao:oSaldoProd:GetVUtBSST()},;
                             {'CIL_TFBCST',oApuracao:oSaldoProd:GetVSdTtBS()},;                            
                             {'CIL_MFFC',oApuracao:oSaldoProd:GetVUntFCP()  },;
                             {'CIL_TFFC',oApuracao:oSaldoProd:GetVlSdTFC()  },;
                             {'CIL_TPREG','3'                               },;
                             {'CIL_IDAPUR',oApuracao:GetIdApur()            },;
                             {'CIL_SPED' ,"1"                               }}})
        Else
            //---Atualiza o saldo final do produto no per�odo analisado (tabela CIL)---//
            Aadd(aDadosCIL,{4,                                                ;
                            {{'CIL_PERIOD',oApuracao:GetAnoMes()            },;
                             {'CIL_PRODUT',oApuracao:oSaldoProd:GetCodProd()},;
                             {'CIL_QTDFIM',oApuracao:oSaldoProd:GetQtdade() },;
                             {'CIL_MFICM',oApuracao:oSaldoProd:GetVlrUnit() },;
                             {'CIL_TFICM',oApuracao:oSaldoProd:GetVlrTot()  },;                             
                             {'CIL_MFST',oApuracao:oSaldoProd:GetnVUntST()  },;
                             {'CIL_TFST',oApuracao:oSaldoProd:GetVlSdTST()  },;                            
                             {'CIL_MFBCST',oApuracao:oSaldoProd:GetVUtBSST()},;
                             {'CIL_TFBCST',oApuracao:oSaldoProd:GetVSdTtBS()},;                            
                             {'CIL_MFFC',oApuracao:oSaldoProd:GetVUntFCP()  },;
                             {'CIL_TFFC',oApuracao:oSaldoProd:GetVlSdTFC()  },;
                             {'CIL_IDAPUR',oApuracao:GetIdApur()            },;
                             {'CIL_SPED' ,"1"                               }}})
        EndIf

        //---Cria/Atualiza o saldo inicial do produto para o pr�ximo per�odo (tabela CIL)---//
        cProxPer := ProxPer(oApuracao:GetAnoMes())
        CIL->(DbSetOrder(1))
        If !CIL->(DbSeek(xFilial("CIL")+cProxPer+oApuracao:oSaldoProd:GetCodProd()))
            Aadd(aDadosCIL,{3,                                                ;
                            {{'CIL_PERIOD',cProxPer                         },;
                             {'CIL_PRODUT',oApuracao:oSaldoProd:GetCodProd()},;
                             {'CIL_QTDSLD',oApuracao:oSaldoProd:GetQtdade() },;
                             {'CIL_MICM  ',oApuracao:oSaldoProd:GetVlrUnit()},;
                             {'CIL_TICM  ',oApuracao:oSaldoProd:GetVlrTot() },;                            
                             {'CIL_MUST',oApuracao:oSaldoProd:GetnVUntST()  },;
                             {'CIL_TUST',oApuracao:oSaldoProd:GetVlSdTST()  },;                            
                             {'CIL_MUBCST',oApuracao:oSaldoProd:GetVUtBSST()},;
                             {'CIL_TIBCST',oApuracao:oSaldoProd:GetVSdTtBS()},;                            
                             {'CIL_MIFC',oApuracao:oSaldoProd:GetVUntFCP()  },;
                             {'CIL_TIFC',oApuracao:oSaldoProd:GetVlSdTFC()  },;
                             {'CIL_TPREG' ,'3'                              },;
                             {'CIL_IDAPUR',oApuracao:GetIdApur()            },;
                             {'CIL_SPED' ,"1"                               }}})
        Else
            Aadd(aDadosCIL,{4,                                                ;
                            {{'CIL_PERIOD',cProxPer                         },;
                             {'CIL_PRODUT',oApuracao:oSaldoProd:GetCodProd()},;
                             {'CIL_QTDSLD',oApuracao:oSaldoProd:GetQtdade() },;
                             {'CIL_MICM  ',oApuracao:oSaldoProd:GetVlrUnit()},;
                             {'CIL_TICM  ',oApuracao:oSaldoProd:GetVlrTot() },;                             
                             {'CIL_MUST',oApuracao:oSaldoProd:GetnVUntST()  },;
                             {'CIL_TUST',oApuracao:oSaldoProd:GetVlSdTST()  },;                            
                             {'CIL_MUBCST',oApuracao:oSaldoProd:GetVUtBSST()},;
                             {'CIL_TIBCST',oApuracao:oSaldoProd:GetVSdTtBS()},;                            
                             {'CIL_MIFC',oApuracao:oSaldoProd:GetVUntFCP()  },;
                             {'CIL_TIFC',oApuracao:oSaldoProd:GetVlSdTFC()  },;
                             {'CIL_IDAPUR',oApuracao:GetIdApur()            },;
                             {'CIL_SPED' ,"1"                               }}})
        EndIf
    Else
        BeginSql Alias cAlias
            SELECT CIL.CIL_PRODUT, CIL.CIL_QTDSLD, CIL.CIL_MUST, CIL.CIL_TUST , CIL.CIL_MICM , CIL.CIL_TICM, CIL.CIL_MUBCST, CIL.CIL_TIBCST ,CIL_MIFC, CIL.CIL_TIFC, CIL.CIL_MFST,;
             CIL.CIL_TFST, CIL.CIL_MFBCST, CIL.CIL_TFBCST, CIL.CIL_MFFC, CIL.CIL_TFFC

            FROM %Table:CIL% CIL LEFT OUTER JOIN (SELECT CII.CII_FILIAL, CII.CII_PERIOD, CII.CII_PRODUT , MAX(CII.CII_ORDEM) CII_ORDEM
                                                  FROM %Table:CII% CII
                                                  WHERE CII.CII_FILIAL = %xFilial:CII%  AND 
                                                        CII.CII_PERIOD = %Exp:cPerApur% AND
                                                        CII_TPREG      = ' '            AND
                                                        CII.%NotDel%
                                                  GROUP BY CII.CII_FILIAL, CII.CII_PERIOD, CII.CII_PRODUT) CII_ ON (CII_.CII_FILIAL = %xFilial:CII% AND CII_.CII_PERIOD = %Exp:cPerApur% AND CII_.CII_PRODUT = CIL.CIL_PRODUT)
            WHERE CIL.CIL_FILIAL = %xFilial:CIL%  AND 
                  CIL.CIL_PERIOD = %Exp:cPerApur% AND
            	  CIL.%NotDel%                    AND
            	  ISNULL(CII_.CII_ORDEM,'') = ''
        EndSql

        DbSelectArea(cAlias)
        While !(cAlias)->(Eof())

            //---Atualiza o saldo final do produto no per�odo analisado (tabela CIL)---//
            Aadd(aDadosCIL,{4,                                   ;
                            {{'CIL_PERIOD',cPerApur             },;
                             {'CIL_PRODUT',(cAlias)->CIL_PRODUT },;
                             {'CIL_QTDFIM',(cAlias)->CIL_QTDSLD },;
                             {'CIL_MFICM ',(cAlias)->CIL_MICM   },;
                             {'CIL_TFICM ',(cAlias)->CIL_TICM   },;                             
                             {'CIL_MFST  ',(cAlias)->CIL_MUST   },;
                             {'CIL_TFST  ',(cAlias)->CIL_TUST   },;                            
                             {'CIL_MFBCST',(cAlias)->CIL_MUBCST },;
                             {'CIL_TFBCST',(cAlias)->CIL_TIBCST },;                            
                             {'CIL_MFFC  ',(cAlias)->CIL_MIFC   },;
                             {'CIL_TFFC  ',(cAlias)->CIL_TIFC   },;
                             {'CIL_IDAPUR',oApuracao:GetIdApur()},;
                             {'CIL_SPED' ,"2"                   }}})

            //---Cria/Atualiza o saldo inicial do produto para o pr�ximo per�odo (tabela CIL)---//
            cProxPer := ProxPer(cPerApur)
            CIL->(DbSetOrder(1))
            If !CIL->(DbSeek(xFilial("CIL")+cProxPer+(cAlias)->CIL_PRODUT))
                Aadd(aDadosCIL,{3,                                   ;
                                {{'CIL_PERIOD',cProxPer            },;
                                 {'CIL_PRODUT',(cAlias)->CIL_PRODUT},;
                                 {'CIL_QTDSLD',(cAlias)->CIL_QTDSLD},;
                                 {'CIL_MICM  ',(cAlias)->CIL_MICM  },;
                                 {'CIL_TICM  ',(cAlias)->CIL_TICM  },;                                 
                                 {'CIL_MUST  ',(cAlias)->CIL_MUST  },;
                                 {'CIL_TUST  ',(cAlias)->CIL_TUST  },;                                
                                 {'CIL_MUBCST',(cAlias)->CIL_MUBCST},;
                                 {'CIL_TIBCST',(cAlias)->CIL_TIBCST},;                                
                                 {'CIL_MIFC'  ,(cAlias)->CIL_MIFC  },;
                                 {'CIL_TIFC'  ,(cAlias)->CIL_TIFC  },;
                                 {'CIL_TPREG' ,'3'                 },;
                                 {'CIL_IDAPUR',oApuracao:GetIdApur()},;
                                 {'CIL_SPED' ,"2"                  }}})
            Else
                Aadd(aDadosCIL,{4,                                   ;
                                {{'CIL_PERIOD',cProxPer              },;
                                 {'CIL_PRODUT',(cAlias)->CIL_PRODUT  },;
                                 {'CIL_QTDSLD',(cAlias)->CIL_QTDSLD  },;
                                 {'CIL_MICM  ',(cAlias)->CIL_MICM    },;
                                 {'CIL_TICM  ',(cAlias)->CIL_TICM    },;                                 
                                 {'CIL_MUST  ',(cAlias)->CIL_MUST    },;
                                 {'CIL_TUST  ',(cAlias)->CIL_TUST    },;                                
                                 {'CIL_MUBCST',(cAlias)->CIL_MUBCST  },;
                                 {'CIL_TIBCST',(cAlias)->CIL_TIBCST  },;                                
                                 {'CIL_MIFC  ',(cAlias)->CIL_MIFC    },;
                                 {'CIL_TIFC  ',(cAlias)->CIL_TIFC    },;
                                 {'CIL_IDAPUR',oApuracao:GetIdApur() },;
                                 {'CIL_SPED' ,"2"                    }}})
            EndIf
        
            (cAlias)->(DbSkip())
        EndDo
        (cAlias)->(DbCloseArea())
    EndIf

    //---Grava a tabela CIL---//
    For nCount := 1 To Len(aDadosCIL)
        lCILOk := .T.
        If aDadosCIL[nCount][1] = 4
            nPos  := Ascan(aDadosCIL[nCount][2],{|a| a[1] == 'CIL_PERIOD'})
            nPos2 := Ascan(aDadosCIL[nCount][2],{|a| a[1] == 'CIL_PRODUT'})

            CIL->(DbSetOrder(1))
            If !CIL->(DbSeek(xFilial("CIL")+aDadosCIL[nCount][2][nPos][2]+aDadosCIL[nCount][2][nPos2][2]))
                lCILOk := .F.
            EndIf
        EndIf

        If lCILOk
            oModel:SetOperation(aDadosCIL[nCount][1])
            oModel:Activate()
            oModel:SetPre({||.T.})  //---Retirada da valida��o do Model para que a rotina de apura��o possa atualizar registros tipo 3-Saldo da Apura��o---//
            oModel:SetPost({||.T.}) //---Retirada da valida��o do Model para que a rotina de apura��o possa atualizar registros tipo 3-Saldo da Apura��o---//

            For nCount2 := 1 To Len(aDadosCIL[nCount][2])
                oModel:SetValue('FISA302B',aDadosCIL[nCount][2][nCount2][1],aDadosCIL[nCount][2][nCount2][2])
            Next nCount2

            If oModel:VldData()
                oModel:CommitData()
            EndIf

            oModel:DeActivate()
        EndIf
    Next nCount
    //---FIM Grava a tabela CIL---//

    RestArea(aArea)
Return


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} GravaCII
  
Fun��o que insere, na tabela CII, os dados do movimento apurado.

@author Rafael.Soliveira
@since 08/11/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Static Function GravaCII(oApuracao)
    Local aCodEnq   := {}

    RecLock('CII',.T.)
    CII->CII_FILIAL := xFilial("CII")
    CII->CII_IDAPUR := oApuracao:GetIdApur()
    CII->CII_PERIOD := oApuracao:GetAnoMes()
	CII->CII_ORDEM  := StrZero(oApuracao:oSaldoProd:GetOrdMov(),9)

    //---Dados do Movimento---//
    CII->CII_PRODUT := oApuracao:oMovimento:GetCodProd()    
    CII->CII_DTMOV  := oApuracao:oMovimento:GetDataMov()    
    CII->CII_TPMOV  := oApuracao:oMovimento:GetTipoMov()
    CII->CII_SERIE  := oApuracao:oMovimento:GetSerDoc()
    CII->CII_NFISCA := oApuracao:oMovimento:GetNumDoc()
    CII->CII_PARTIC := Left(oApuracao:oMovimento:GetCodPart(),TamSx3("A1_COD")[1])
    CII->CII_LOJA   := Right(oApuracao:oMovimento:GetCodPart(),TamSx3("A1_LOJA")[1])
    CII->CII_CFOP   := oApuracao:oMovimento:GetCFOP()
    CII->CII_ITEM   := oApuracao:oMovimento:GetItemDoc()    
    CII->CII_ESPECI := oApuracao:oMovimento:GetEspDoc()
	CII->CII_PDV	:= oApuracao:oMovimento:GetNPDV()
	CII->CII_LIVRO  := oApuracao:oMovimento:GetNrLivro()
	CII->CII_CST    := oApuracao:oMovimento:GetCST()
	CII->CII_UNID   := oApuracao:oMovimento:GetUnid()
    CII->CII_VUNIT 	:= oApuracao:oMovimento:GetnVUNIT()
    CII->CII_QTDMOV := oApuracao:oMovimento:GetQtdade()
    CII->CII_QTDSLD := oApuracao:oMovimApur:GetSldQtd()

    //C180 - Dados nota de entrada
    CII->CII_CODRES := oApuracao:oMovimApur:GetcCODRES() //Codigo responsavel   
    CII->CII_ICMEFE := oApuracao:oMovimApur:GetConfEnt() //ICM Efet Ent
    CII->CII_BURET  := oApuracao:oMovimApur:GetnBURET()	//Base unitaria valor pago ou retido anteriormente
    CII->CII_VURET  := oApuracao:oMovimApur:GetnVURET() //Valor unitario ICMS pago anteriormente com FECP
    CII->CII_VURFCP := oApuracao:oMovimApur:GetnVURFCP() //Valor unitario pago anteriormente FECP
	CII->CII_NUMDA  := ""                                //Modelo de arrecada��o
	CII->CII_CODDA  := ""                                //Numero do doc de arrecada��o    

    //---Valores apurados para o movimento---//        
	CII->CII_VUCRED := oApuracao:oMovimApur:GetnVUCRED()  //Vl Unit Cred ICMS
    
    //c185
	CII->CII_VUCFC  := oApuracao:oMovimApur:GetnVUCFC()   //Valor unitario complemento do FECP
	CII->CII_VUCST  := oApuracao:oMovimApur:GetnVUCST()   //Valor Unitario complemento do ICMS com FECP
	CII->CII_VURTFC := oApuracao:oMovimApur:GetnVURTFC()  //Valor unitario restitui�ao do FECP
	CII->CII_VUREST := oApuracao:oMovimApur:GetnVUREST()  //Valor Unitario complemento do ICMS com FECP
    CII->CII_ICMEFS := oApuracao:oMovimApur:GetConfSai()  //ICMS Efetivo Sa�da
    
    //Medio estoque
	CII->CII_MUCRED := oApuracao:oMovimApur:GetSupUni()  //Vl Med Cred ICMS	//SetSupTot GetSupUni    
	CII->CII_MUVSTF := oApuracao:oMovimApur:GetSldUST() // Valor Medio unitario ICMS st + FECP em estoque
    CII->CII_MUVSF  := oApuracao:oMovimApur:GetSldUFC() //Valor Medio unitario do FECP ST em estoque
	CII->CII_MUBST  := oApuracao:oMovimApur:GetSldUBS() //Valor Medio unitario da base de calculo ICMS ST em estoque

    If !Empty(oApuracao:oMovimApur:GetEnquad())
        aCodEnq   := FISA302Enq(oApuracao:oMovimApur:GetEnquad())
        CII->CII_ENQLEG := aCodEnq[2] //Converte enquadramento em c�digo da tabela 5.7
        CII->CII_REGRA  := aCodEnq[1] //Converte enquadramento em c�digo da tabela 5.7        
    EndIf

    
    //Valor Apurado por nota										
    CII->CII_VCREDI := oApuracao:oMovimApur:GetApurCop() //Val Cr�d ICM
    CII->CII_VRESSA := oApuracao:oMovimApur:GetApurRes() //Val Resarc.
    CII->CII_VCMPL  := oApuracao:oMovimApur:GetApurCom() //Val Complem.    
    CII->CII_VREFCP := oApuracao:oMovimApur:GetApurFR() //Val Resarc FECP.
    CII->CII_VCMFCP := oApuracao:oMovimApur:GetApurFC() //Val Complem FECP.

    
    CII->CII_TPREG  := ' '

	CII->(MsUnlock())

Return


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} GravaCIGT
  
Fun��o que grava, nas tabelas CIG e CIH, os totalizadores da apura��o.

@author Rafael.Soliveira
@since 16/11/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Static Function GravaCIGT(oApuracao,cTipoGrv)
    Local nVlrTotRes := 0
    Local nVlrTotCom := 0
    Local nVlrTotCop := 0
	Local nVlrComFcp := 0
	Local nVlrResFcp := 0
    Local nCount     := 0
    Local aCodEnq    := {}

    If cTipoGrv = 1     //--- Par�metro cTipoGrv = 1 -> Chamada no in�cio do processamento. Insere registro na tabala CIG ---//
        RecLock('CIG',.T.)
        CIG->CIG_FILIAL := xFilial("CIG")
        CIG->CIG_IDAPUR := oApuracao:GetIdApur()
        CIG->CIG_PERIOD := oApuracao:GetAnoMes()
        CIG->(MsUnlock())
    ElseIf cTipoGrv = 2 //--- Par�metro cTipoGrv = 2 -> Chamada ao fim do processamento. Insere registros na tabala CIH e atualiza totais da tabela CIG ---//
        For nCount := 1 To Len(oApuracao:aTotApur)
            //If oApuracao:aTotApur[nCount][1] != '0'
                aCodEnq   := FISA302Enq(oApuracao:aTotApur[nCount][1])
                
                RecLock('CIH',.T.)
                CIH->CIH_FILIAL := xFilial("CIH")
                CIH->CIH_IDAPUR := oApuracao:GetIdApur()
                CIH->CIH_PERIOD := oApuracao:GetAnoMes()
                CIH->CIH_ENQLEG := aCodEnq[2]    //Converte enquadramento em c�digo da tabela 5.7
                CIH->CIH_REGRA	:= aCodEnq[1]
                CIH->CIH_VRESSA := oApuracao:aTotApur[nCount][2]
                CIH->CIH_VCOMPL := oApuracao:aTotApur[nCount][3]
                CIH->CIH_VCREDI := oApuracao:aTotApur[nCount][4]
				CIH->CIH_REFECP	:= oApuracao:aTotApur[nCount][5]
				CIH->CIH_VCMFCP	:= oApuracao:aTotApur[nCount][6]
                
                CIH->(MsUnlock())

                nVlrTotRes += oApuracao:aTotApur[nCount][2]
                nVlrTotCom += oApuracao:aTotApur[nCount][3]
                nVlrTotCop += oApuracao:aTotApur[nCount][4]
				nVlrResFcp += oApuracao:aTotApur[nCount][5]
                nVlrComFcp += oApuracao:aTotApur[nCount][6]
				
            //EndIf
        Next nCount

        CIG->(DbSetOrder(2))
        If CIG->(DbSeek(xFilial("CIG")+oApuracao:GetIdApur()))
            RecLock('CIG',.F.)
                CIG->CIG_VRESSA := nVlrTotRes
                CIG->CIG_VCMPL  := nVlrTotCom
                CIG->CIG_VCREDI := nVlrTotCop
				CIG->CIG_REFECP	:= nVlrResFcp
				CIG->CIG_VCMFCP	:= nVlrComFcp
            MsUnLock()
        EndIf
    EndIf

Return


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} CheckApur
  
Fun��o que verifica a exist�ncia de apura��o no per�odo selecionado.

@author Rafael.Soliveira
@since 22/11/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Static Function CheckApur(cPerApur)
    Local cIdApur := ''

    CIG->(DbSetOrder(1))
    If CIG->(DbSeek(xFilial("CIG")+cPerApur))
        cIdApur := CIG->CIG_IDAPUR
    EndIf

Return cIdApur


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} DeletApur
  
Fun��o de exclus�o da apura��o.

@author Rafael.Soliveira
@since 22/11/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Static Function DeletApur(cIdApur,cPerApur)
    Local cQuery     := ''
    Local cProxPer   := ''
    Local cIdApuProx := ''

    //---Tabela CIG [Apura��o]---//
    CIG->(DbSetOrder(2))
    If CIG->(DbSeek(xFilial("CIG")+cIdApur))
		RecLock('CIG',.F.)
		    CIG->(DbDelete())
		MsUnLock()
    EndIf

    //---Tabela CIH [Apura��o por Enquadramento Legal]---//
    CIH->(DbSetOrder(2))
    CIH->(DbSeek(xFilial("CIH")+cIdApur))
    While !CIH->(Eof()) .And. CIH->CIH_IDAPUR == cIdApur
		RecLock('CIH',.F.)
		    CIH->(DbDelete())
		MsUnLock()
        CIH->(DbSkip())
    EndDo

    //---Tabela CIL [Saldos Iniciais]---//
    cQuery := "UPDATE " +  RetSqlName('CIL') + " SET CIL_QTDFIM = 0, CIL_MFICM = 0, CIL_TFICM = 0,CIL_TFST = 0, CIL_MFST = 0,CIL_TFFC = 0, CIL_MFFC = 0,CIL_TFBCST = 0, CIL_MFBCST = 0 WHERE CIL_FILIAL = " + ValToSql(xFilial('CIL')) + " AND CIL_PERIOD = " + ValToSql(cPerApur)
    If !Empty(AllTrim(cQuery))
        TcSqlExec(cQuery)
    EndIf	


    cProxPer := cPerApur
    cProxPer := LastDay(CtoD('01/'+Right(cProxPer,2)+'/'+Left(cProxPer,4)))+1
    cProxPer := Left(DToS(cProxPer),6)

    //---Verifica a exist�ncia de apura��o no pr�ximo per�odo. Caso exista, n�o exclui registro da CIL---//
    cIdApuProx := CheckApur(cProxPer)
    If Empty(cIdApuProx)
        CIL->(DbSetOrder(1))
        CIL->(DbSeek(xFilial("CIL")+cProxPer))
        While !CIL->(Eof()) .And. CIL->CIL_PERIOD == cProxPer
            RecLock('CIL',.F.)
                CIL->(DbDelete())
            MsUnLock()
            CIL->(DbSkip())
        EndDo
    EndIf

    //---Tabela CII [Apura��o Detalhada por Movimento]---//
    cQuery := "DELETE FROM " +  RetSqlName('CII')  + " WHERE CII_FILIAL = " + ValToSql(xFilial('CII')) + " AND CII_IDAPUR = " + ValToSql(cIdApur)
    If !Empty(AllTrim(cQuery))
        TcSqlExec(cQuery)
    EndIf

Return


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} PesqApur
  
Fun��o que pesquisa, no movimento j� apurado (tabela CII), o movimento de sa�da original, em casos de devolu��es de sa�das.
Retorna um vetor contendo os seguintes valores:

[1]-Quantidade do movimento original
[2]-Valor unit�rio do ICMS suportado no movimento original
[3]-C�digo do Enquadramento Legal do movimento original
[4]-Valor do ICMS Efeitivo na Sa�da do movimento original
[5]-Valor do ICMS Efeitivo na Entrada do movimento original
[6]-Valor do Ressarcimento do movimento original
[7]-Valor do Complemento do movimento original
[8]-Valor do Cr�dito da Opera��o Pr�pria
[9]-Valor total do ICMS suportado no movimento original

@author Rafael.Soliveira
@since 03/12/2018
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------
Static Function PesqApur(cTipoMov, cDocOri, cSerOri, cItemOri, cCliOri, cLojaOri, cProdOri, aCodRes)
    Local aArea    := GetArea()
    Local cAlias   := GetNextAlias()
    Local lApurAnt := .F.
    Local nPos     := 0
    Local nVlrRes  := 0
    Local nVlrCred := 0
    Local nQtdade  := 0
    Local aRetorno := {0,0,'',0,0,0,0,0,0}
    Local nTotalCred := 0

    cTipoMov := Iif(cTipoMov == 'E','S','E')

    //--Localiza sa�da original apurada na tabela CII, para utilizar os valores informados anteriormente---//
    BeginSql Alias cAlias
        SELECT CII_TPMOV, CII_QTDMOV, CII_ENQLEG, CII_MUCRED, CII_ICMEFS, CII_ICMEFE, CII_VRESSA, CII_VCMPL , CII_VCREDI, CII_VUCRED,CII_MUBST,CII_MUVSTF,CII_MUVSF
        FROM  %TABLE:CII% CII
        WHERE CII.CII_FILIAL = %XFILIAL:CII%  AND 
              CII.CII_TPMOV  = %EXP:cTipoMov% AND
        	  CII.CII_SERIE  = %EXP:cSerOri%  AND
        	  CII.CII_NFISCA = %EXP:cDocOri%  AND
        	  CII.CII_PARTIC = %EXP:cCliOri%  AND
        	  CII.CII_LOJA   = %EXP:cLojaOri% AND
        	  CII.CII_ITEM   = %EXP:cItemOri% AND
        	  CII.CII_PRODUT = %EXP:cProdOri% AND
        	  CII.%NOTDEL%	
    EndSql

    DbSelectArea(cAlias)
    If !(cAlias)->(Eof())

        nTotalCred := (cAlias)->CII_VUCRED * (cAlias)->CII_QTDMOV

        aRetorno := {(cAlias)->CII_QTDMOV,;  //1
                    (cAlias)->CII_MUCRED,;   //2
                    (cAlias)->CII_ENQLEG,;   //3
                    (cAlias)->CII_ICMEFS,;   //4
                    (cAlias)->CII_ICMEFE,;   //5
                    (cAlias)->CII_VRESSA,;   //6
                    (cAlias)->CII_VCMPL,;    //7
                    (cAlias)->CII_VCREDI,;   //8
                    nTotalCred,;             //9
                    (cAlias)->CII_MUVSTF,;   //10       
                    (cAlias)->CII_MUBST,;    //11
                    (cAlias)->CII_MUVSF}     //12
        lApurAnt := .T.
    EndIf
    (cAlias)->(DbCloseArea())    

    RestArea(aArea)
Return aRetorno


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ProxPer
Fun��o que calcula o pr�ximo per�odo

@author Rafael.Soliveira
@since 27/12/2018
@version 1.0

/*/
//--------------------------------------------------------------------------------------------------
Static Function ProxPer(cPerApur)
    Local cProxPer := ''

    cProxPer := LastDay(CtoD('01/'+Right(cPerApur,2)+'/'+Left(cPerApur,4)))+1
    cProxPer := Left(DToS(cProxPer),6)
Return cProxPer


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AtualizaMsg

Fun��o que ser� chamada para atualizar descri��o da barra de status

@author Rafael.Soliveira
@since 22/11/2018
@version 12.1.17
/*/
//--------------------------------------------------------------------------------------------------
Static Function AtualizaMsg(oSay,cMsg)
    If !lAutomato
        oSay:cCaption := (cMsg)
        ProcessMessages()
    EndIf
Return


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} FISA302CEXC
Fun��o de exclus�o da apura��o selecionada, a partir da rotina FISA193.

@author Rafael.Soliveira
@since 22/11/2018
@version 1.0

/*/
//--------------------------------------------------------------------------------------------------
Function FISA302CEXC()
    FWExecView(STR0010,"FISA302D",MODEL_OPERATION_DELETE,,{|| DeletApur(CIG->CIG_IDAPUR,CIG->CIG_PERIOD) },,,) //"Exclus�o"
Return