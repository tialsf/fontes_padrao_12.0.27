#INCLUDE "TOTVS.CH"
#INCLUDE "PROTHEUS.CH"

/*/{Protheus.doc} function WzdMaisSaldo
Fun��o que realiza a configura��o do Conector entre Protheus e a Carol
@author  Hugo de Oliveira
@since   02/06/2020
@version 1.0
/*/
Main Function WzdMaisSaldo()
    MsApp():New("SIGAFIN")
    
    oApp:cInternet  := Nil
    __cInterNet     := NIL
    oApp:bMainInit  := { || (oApp:lFlat := .F. , fMakeWzd(), Final( "Encerramento Normal" , "")) }
    
    oApp:CreateEnv()
    OpenSM0()
    PtSetTheme("TEMAP10")
    SetFunName("UPDDISTR")
    oApp:lMessageBar := .T.
    oApp:Activate()
Return


/*/{Protheus.doc} function fMakeWzd
Fun��o que realiza a montagem da estrutura do Wizard
@author  Hugo de Oliveira
@since   02/06/2020
@version 1.0
/*/
Static Function fMakeWzd()
    Local oNewPag
    Local cWelcome1         := "Seja Bem-vindo(a) ao Assistente de Configura��o de Integra��o +Saldo"
    Local cWelcome2         := "Para continuar pressione o bot�o 'Avan�ar' ou 'Cancelar' para abortar o processo."
    Local nFieldSize        := 150
    Local cUser             := Space(nFieldSize)
    Local cPsw              := Space(nFieldSize)
    Local cCarolURL         := Space(nFieldSize)
    Local cPlatformURL      := Space(nFieldSize)
    Local cCarolConnID      := Space(nFieldSize)
    Local cCarolAPIToken    := Space(nFieldSize)
    Local cRACID            := Space(nFieldSize)
    Local cRACSecret        := Space(nFieldSize)
    Local cClient           := ''
    Local cStore            := ''
    Local cName             := ''
    Local cFilSA1           := ''
    Local oStepWiz          := nil
    Local oStatus           := Nil
    Local aCompany          := {}
    Local aSM0              := {}

    oStepWiz:= FWWizardControl():New(, {580, 650} )
    oStepWiz:ActiveUISteps()
    
    // Pagina 1 - Boas Vindas
    oNewPag := oStepWiz:AddStep()
    oNewPag:SetStepDescription("Boas Vindas")
    oNewPag:SetConstruction({|Panel| MakeStep1(Panel, cWelcome1, cWelcome2)})
    oNewPag:SetNextAction({|| .T.})
    
    // Pagina 2 - Autentica��o
    oNewPag := oStepWiz:AddStep()
    oNewPag:SetConstruction( { |Panel| MakeStep2(Panel, @cUser, @cPsw, @aCompany, @aSM0) } )
    oNewPag:SetStepDescription("Autentica��o")
    oNewPag:SetNextAction({|| VldStep2(cUser, cPsw, @aCompany, @cCarolURL, @cPlatformURL, @cCarolConnID, @cCarolAPIToken, @cClient, @cStore, @cName) }) 

    // Pagina 3 - Dados de Plataforma
    oNewPag := oStepWiz:AddStep()
    oNewPag:SetConstruction( { |Panel| MakeStep3(Panel, @cCarolURL, @cPlatformURL, @cCarolConnID, @cCarolAPIToken, @cRACID, @cRACSecret )})
    oNewPag:SetStepDescription("Dados de Plataforma")
    oNewPag:SetNextAction({||VldStep3(cCarolURL, cPlatformURL, cCarolConnID, cCarolAPIToken, cRACID, cRACSecret)})

    // Pagina 4 - Ativa��o da Integra��o
    oNewPag := oStepWiz:AddStep()
    oNewPag:SetConstruction( {|Panel| MakeStep4(Panel, @oStatus), fSaveConf(oStatus, cUser, cPsw, aCompany, aSM0, cFilSA1, cClient, cStore, cCarolURL, cPlatformURL, cCarolConnID, cCarolAPIToken, cRACID, cRACSecret) })
    oNewPag:SetStepDescription("Ativando a Integra��o")
    oNewPag:SetNextAction({|| .T.})
    oNewPag:SetPrevWhen( { || .F. } )
    oNewPag:SetCancelWhen( { || .F. } )
    oStepWiz:Activate()
Return

/*/{Protheus.doc} function MakeStep1
Fun��o que realiza a montagem da p�gina 1 do configurador
@author  Hugo de Oliveira
@since   02/06/2020
@version 1.0
/*/
Static Function MakeStep1(oPanel, cWelcome1, cWelcome2)
    Local oSay1
    Local oSay2

    oSay1 := TSay():New(10,10,{||cWelcome1},oPanel,,,,,,.T.,,,200,20)
    oSay2 := TSay():New(30,10,{||cWelcome2},oPanel,,,,,,.T.,,,200,20)
Return


/*/{Protheus.doc} function MakeStep2
Fun��o que realiza a montagem da p�gina 2 do configurador
@author  Hugo de Oliveira
@since   02/06/2020
@version 1.0
/*/
Static Function MakeStep2(oPanel, cUser, cPsw, aCompany, aSM0)
    Local oTGet1
    Local oTGet2
    Local oList

    aCompany := LoadComp(@aSM0)

    DEFINE FONT oCHFont	NAME 'Arial' WEIGHT 10 BOLD 
	DEFINE FONT oCMFont	NAME 'Arial' WEIGHT 10

    @ 10,10 SAY "Usu�rio:" OF oPanel PIXEL FONT oCHFont //"Usu�rio:"
    @ 20,10 MSGET oTGet1 VAR cUser SIZE 100,10 OF oPanel Font oCMFont PIXEL 

    @ 10,140 SAY "Senha:" OF oPanel PIXEL FONT oCHFont //"Senha:"
    @ 20,140 MSGET oTGet2 VAR cPsw SIZE 100,10 OF oPanel Font oCMFont PIXEL PASSWORD
 
    // Monta a lista de empresas
    @ 050, 010 LISTBOX oList;
        FIELDS HEADER "", "C�digo", "Descri��o da empresa" ; // "C�digo"###"Descri��o da Empresa"
        SIZE 160, 115 OF oPanel PIXEL; 
        ON DBLCLICK (aCompany[oList:nAt, 1] := !aCompany[oList:nAt, 1], oList:Refresh(.f.)) 
    
        oList:SetArray( aCompany )
        oList:bLine := {|| { If(aCompany[oList:nAt, 1], LoadBitmap( GetResources(), "LBTIK" ), LoadBitmap( GetResources(), "LBNO" )), aCompany[oList:nAt, 2], aCompany[oList:nAt, 3] }}
        oList:bHeaderClick := { |a, b| iif(b == 1 , MarkAll(aCompany, b),), oList:Refresh() }
    oTGet1:SetFocus()
Return

/*/{Protheus.doc} function MarkAll
Fun��o que realiza a sele��o de todos os itens de acordo com o contexto utilizado
@author  Hugo de Oliveira
@since   02/06/2020
@version 1.0
/*/
Static Function MarkAll(aLista, nPos)
	Local lMark := .F.
	
	aEval(aLista, {|x| iif(!x[nPos], lMark := .T.,)  })
	aEval(aLista, {|x, i| aLista[i, nPos] := lMark })
Return .T.

/*/{Protheus.doc} function ValidArr
Fun��o que valida quantidade de elementos de 1 array
@author  Hugo de Oliveira
@since   02/06/2020
@version 1.0
/*/
Static Function ValidArr( aArray )
Return ( aScan( aArray, {|x| x[1] == .T. } ) > 0 )

/*/{Protheus.doc} function VldStep2
Fun��o que realiza a valida��o dos campos da p�gina 2
@author  Hugo de Oliveira
@since   02/06/2020
@version 1.0
/*/
Static Function VldStep2(cUser, cPsw, aCompany, cCarolURL, cPlatformURL, cCarolConnID, cCarolAPIToken, cClient, cStore, cName)
    Local nVldLogin
    Local nCompany
    Local lValid := .F. 

    If Empty(cUser)
        cMsg := "Informe o usu�rio"
    ElseIf !ValidArr( aCompany )
        cMsg := "Selecione ao menos uma empresa"
    Else 
        nVldLogin := PswAdmin( Alltrim(cUser), Alltrim(cPsw) )
        
        If nVldLogin == 0
            nCompany := Ascan( aCompany, {|x| x[1] == .T.})
            OpenSM0( aCompany[nCompany][2] )
            fSetComp( SM0->M0_CODIGO, SM0->M0_CODFIL, cUser, cPsw )
            lValid := .T.
        Else
            If nVldLogin == 1
                cMsg := "O usu�rio informado n�o � Administrador do sistema."
            ElseIf nVldLogin == 2
                cMsg := "Dados para login incorretos."
            EndIf
        EndIf
    ENDIF

    If !lValid
        ApMsgAlert(cMsg)
    EndIf

Return lValid


/*/{Protheus.doc} function MakeStep3
Fun��o que realiza a cria��o da p�gina 3
@author  Hugo de Oliveira
@since   02/06/2020
@version 1.0
/*/
Static Function MakeStep3(oPanel, cCarolURL, cPlatformURL, cCarolConnID, cCarolAPIToken, cRACID, cRACSecret)
    Local oCarolURL
    Local oPlatformURL
    Local oCarolConnID
    Local oCarolAPIToken
    Local oRACID
    Local oRACSecret
  
    DEFINE FONT oCHFont	NAME 'Arial' WEIGHT 10 BOLD 
	DEFINE FONT oCMFont	NAME 'Arial' WEIGHT 10

    @ 10,10 SAY "Informe a URL da Carol:" OF oPanel PIXEL FONT oCHFont
    @ 20,10 MSGET oCarolURL VAR cCarolURL SIZE 120,10 OF oPanel Font oCMFont PIXEL 

    @ 40,10 SAY "Informe a URL da Plataforma:" OF oPanel PIXEL FONT oCHFont
    @ 50,10 MSGET oPlatformURL VAR cPlatformURL SIZE 120,10 OF oPanel Font oCMFont PIXEL

    @ 70,10 SAY "Informe o ID do Conector da Carol:" OF oPanel PIXEL FONT oCHFont
    @ 80,10 MSGET oCarolConnID VAR cCarolConnID SIZE 120,10 OF oPanel Font oCMFont PIXEL 

    @ 100,10 SAY "Informe o Token da Carol:" OF oPanel PIXEL FONT oCHFont
    @ 110,10 MSGET oCarolAPIToken VAR cCarolAPIToken SIZE 120,10 OF oPanel Font oCMFont PIXEL

    @ 130,10 SAY "Informe o RAC Client Id:" OF oPanel PIXEL FONT oCHFont
    @ 140,10 MSGET oRACID VAR cRACID SIZE 120,10 OF oPanel Font oCMFont PIXEL

    @ 160,10 SAY "Informe o RAC Client Secret:" OF oPanel PIXEL FONT oCHFont 
    @ 170,10 MSGET oRACSecret VAR cRACSecret SIZE 120,10 OF oPanel Font oCMFont PIXEL
Return

/*/{Protheus.doc} function VldStep3
Fun��o que realiza a valida��o da p�gina 3
@author  Hugo de Oliveira
@since   02/06/2020
@version 1.0
/*/
Static Function VldStep3( cCarolURL, cPlatformURL, cCarolConnID, cCarolAPIToken, cRACID, cRACSecret)
    Local lValid    := .T.
    Local cMsg      := ""
    Local cRacToken := ""

    If Empty(cCarolURL)
        lValid := .F.
        cMsg := "Informe a URL da Carol."

    ElseIf Empty(cPlatformURL)
        lValid := .F.
        cMsg := "Informe a URL da plataforma."

    ElseIf Empty(cCarolConnID)
        lValid := .F.
        cMsg := "Informe o ID do conector da Carol."

    ElseIf Empty(cCarolAPIToken)
        lValid := .F.
        cMsg := "Informe o API Token da Carol."
    
    ElseIf lValid .And. !fValidCarolURL(cCarolURL, cCarolConnID, cCarolAPIToken)
        lValid  := .F.
        cMsg    := "O ID e API Token da Carol informados n�o s�o v�lidos, por favor revise as informa��es."
    
    ElseIf lValid .And. Empty(cRacToken := fValidRac(cRACID, cRACSecret))
        lValid  := .F.
        cMsg    := "O RAC Client Id ou RAC Client Secret informados n�o s�o v�lidos, por favor revise as informa��es."

     ElseIf lValid .And. !fValidPlatform(cPlatformURL, cRacToken)
        lValid  := .F.
        cMsg    := " A URL da Plataforma informada n�o � v�lida, por favor revise a informa��o."
    EndIf

    If !lValid
        ApMsgAlert(cMsg)
    EndIf
Return lValid

/*/{Protheus.doc} function MakeStep4
Fun��o que realiza a cria��o da p�gina 4
@author  Hugo de Oliveira
@since   02/06/2020
@version 1.0
/*/
Static Function MakeStep4(oPanel, oStatus)
    Local oFont
    Local cStatus
    Local aStatus   := {}
    Local oBtnPanel := TPanel():New(0,0,"",oPanel,,,,,,40,40, .T., .T.)
    
    oBtnPanel:Align := CONTROL_ALIGN_ALLCLIENT
    DEFINE FONT oFont NAME "Courier New" SIZE 10,0
    @ 015,035 LISTBOX oStatus VAR cStatus ITEMS aStatus SIZE 250, 140 OF oBtnPanel PIXEL  FONT oFont
Return

/*/{Protheus.doc} function fSaveConf
Fun��o que chama todos os processos para salvar as informa��es Inputadas
@author  Hugo de Oliveira
@since   02/06/2020
@version 1.0
/*/
Static Function fSaveConf(oStatus, cUser, cPsw, aCompany, aSM0, cFilSA1, cClient, cStore, cCarolURL, cPlatformURL, cCarolConnID, cCarolAPIToken, cRACID, cRACSecret) 
    Local oRet
    Local nCompany := 0

    For nCompany := 1 to len(aCompany)
        If aCompany[nCompany][1]
            IF nCompany > 1
                oStatus:Add( Replicate('-', 50))
            EndIF

            // Abre o ambiente
            oStatus:Add(Padr( Dtoc(MsDate()), 14) + Padr(Time(),10) + '- Conectando Ambiente - ' + aCompany[nCompany][2])
            fOpenEnv(cUser, cPsw, aCompany[nCompany][2])

             // Salva os dados da Plataforma
            If nCompany == 1
                oStatus:Add(Padr( Dtoc(MsDate()), 14) + Padr(Time(),10) + '- Salvando Dados da Plataforma')
                fSetCarolConfig(cCarolURL, cPlatformURL, cCarolConnID, cCarolAPIToken, cRACID, cRACSecret)
            EndIf
            
            // Chamada do Job de Integra��o da Carol
            oStatus:Add(Padr( Dtoc(MsDate()), 14) + Padr(Time(),10) + '- Chamando JOB de Integra��o da Carol')
            fCarolJob()

            // Valida��o dos dados configurados
            oRet := fwtfconfig()

            If  !Empty(oRet["platform-endpoint"]) .And. !Empty(oRet["carol-endpoint"]) .And. !Empty(oRet["carol-connectorId"]) .And. ;
                !Empty(oRet["carol-apiToken"]) .And. !Empty(oRet["rac-endpoint"]) .And. !Empty(oRet["platform-clientId"]) .And. !Empty(oRet["platform-secret"])
                oStatus:Add(Padr( Dtoc(MsDate()), 14) + Padr(Time(),10) + '- Dados Configurados com Sucesso!')
            Else
                ApMsgAlert("Erro na Configura��o. Revise os dados inseridos e tente novamente!")
            EndIf

            oStatus:Add(Padr( Dtoc(MsDate()), 14) + Padr(Time(),10) + '- Finalizando Ambiente - ' + aCompany[nCompany][2])
        EndIf
    Next

    oStatus:Add(Padr( Dtoc(MsDate()), 14) + Padr(Time(),10) + '- Configura��es Finalizadas!')
    oStatus:Refresh()
Return

/*/{Protheus.doc} function fOpenEnv
Fun��o que habilita a empresa para o sincronismo
@author  Hugo de Oliveira
@since   02/06/2020
@version 1.0
/*/
Static Function fOpenEnv(cUser, cPsw, cCompany)
    Local oParam
    
    OpenSM0( cCompany )
    fSetComp( cCompany, SM0->M0_CODFIL, cUser, cPsw )

    oParam := FwAppParam():New()
    oParam:Put("FWCarolCompany" + cCompany, "true")
Return

/*/{Protheus.doc} function fSetComp
Fun��o que inicializa o ambiente
@author  Hugo de Oliveira
@since   02/06/2020
@version 1.0
/*/
Static Function fSetComp( cCompany, cBranch, cUser, cPsw)
    Default cUser   := ""
    Default cPsw    := ""

    RPCSetType( 3 )
    
    IF !Empty(cUser)
        RpcSetEnv( cCompany, cBranch, cUser, cPsw )
    Else
        RpcSetEnv( cCompany, cBranch )
    EndIf

    oApp:cInternet := Nil
    __cInternet := NIL
    lMsHelpAuto := .F.
Return

/*/{Protheus.doc} function fSetCarolConfig
Fun��o que realiza a grava��o dos dados de configura��o
@author  Hugo de Oliveira
@since   02/06/2020
@version 1.0
/*/
Static Function fSetCarolConfig(cCarolURL, cPlatformURL, cCarolConnID, cCarolAPIToken, cRACID, cRACSecret)
    Local oCarol := JsonObject():New()

    oCarol["platform-endpoint"]   := AllTrim(cPlatformURL)
    oCarol["carol-endpoint"]      := AllTrim(cCarolURL)
    oCarol["carol-connectorId"]   := AllTrim(cCarolConnID)
    oCarol["carol-apiToken"]      := AllTrim(cCarolAPIToken)
    
    // O Frame adiciona automaticamente o restante do patch(/totvs.rac/connect/token) no final do EndPoint.
    oCarol["rac-endpoint"]        := "https://admin.rac.dev.totvs.io"
    oCarol["platform-clientId"]   := AllTrim(cRACID)
    oCarol["platform-secret"]     := AllTrim(cRACSecret)

    FwTFSetConfig(oCarol)
    
    // Habilita a chamada da Integra��o
    StaticCall(FwTechFinWizard, EnableInt)
Return


/*/{Protheus.doc} function fCarolJob
Fun��o que realiza a chamada do JOB de Integra��o com a Carol
@author  Hugo de Oliveira
@since   02/06/2020
@version 1.0
/*/
Static Function fCarolJob()
    Local cID
    Local lInsert := .T.

    // Valida se o agendamento ja est� cadastrado
    FWOpenXX1()
    XX1->(DbGoTop())

    // N�o existe �ndice por nome da Rotina
    While XX1->(!EoF()) .And. lInsert
        If RTrim(Upper(XX1->XX1_ROTINA)) == "FWTECHFINJOB"
            lInsert := .F.
        EndIf
        XX1->(DbSkip())
    EndDo

    If lInsert
        cID := FWAddSchedule("FWTECHFINJOB", , "A")
    EndIf
Return

/*/{Protheus.doc} function LoadComp
Fun��o que faz o carregamento das empresas
@author  Hugo de Oliveira
@since   02/06/2020
@version 1.0
/*/
Static Function LoadComp(aSM0) 
	Local aCompany := {}
	Local oParam
    Local nI := 0
    Local cContent := ''

	SET DELET ON

    OpenSM0()
    aSM0 := FWLoadSM0()

    aEval( FWAllGrpCompany(), {|oComp| AAdd(aCompany, { .F., oComp, FWEmpName(oComp) }) } )

    oParam := FwAppParam():New()
    For nI := 1 to len(aCompany)
        cContent := oParam:Get( "FWCarolCompany" + aCompany[nI][2])

        If cContent == 'true'
            aCompany[ni][1] := .T.
        EndIf
    Next
Return aCompany

/*/{Protheus.doc} function fValidCarolURL
Fun��o que realiza a valida��o da URL da Carol
@author  Hugo de Oliveira
@since   02/06/2020
@version 1.0
/*/
Static Function fValidCarolURL(cCarolURL, cID, cToken)
    Local lRet      := .F.
    Local aHeader   := {}
    Local oCarol    := FwRest():New(ALLTRIM(cCarolURL) + "/v2/apiKey/details")
    
	aAdd(aHeader, "Content-Type: application/json; charset=UTF-8")
    aAdd(aHeader, "x-auth-connectorid:" + ALLTRIM(cID))
    aAdd(aHeader, "x-auth-key:" + ALLTRIM(cToken))

    oCarol:setPath("?apiKey=" + ALLTRIM(cToken) + "&connectorId=" + ALLTRIM(cID))
   
    If oCarol:Get(aHeader)
        If oCarol:ORESPONSEH:CSTATUSCODE == "200" .AND. !Empty(oCarol:GetResult())
            lRet := .T.
        EndIf
    EndIf

    FreeObj(oCarol)
Return lRet

/*/{Protheus.doc} function fValidRac
Fun��o que realiza a valida��o das credenciais do RAC
@author  Hugo de Oliveira
@since   02/06/2020
@version 1.0
/*/
Static Function fValidRac(cRACID, cRACSecret)
    Local cResponse     := ""
    Local cHeaderRet    := ""
    Local cBody         := ""
    Local cRacToken     := ""
    Local aHeader       := {}
    Local nTimeOut      := 120 // Segundos
    Local oResponse     := JsonObject():New()
    Local cURL          := "https://admin.rac.dev.totvs.io/totvs.rac/connect/token"

    Aadd(aHeader, "Content-Type: application/x-www-form-urlencoded")
    AAdd(aHeader, "User-Agent: Protheus " + GetBuild())
    
    cBody := "client_id="     + ALLTRIM(cRACID) + "&"
    cBody += "client_secret=" + ALLTRIM(cRACSecret) + "&"
    cBody += "grant_type=client_credentials&"
    cBody += "scope=authorization_api"
    
    cResponse := HttpPost(cUrl, "", cBody, nTimeOut, aHeader, cHeaderRet)

    If AT("200 OK", cHeaderRet) > 0 .AND. AT("access_token", cResponse) > 0
        oResponse:fromJson(cResponse)
        cRacToken := oResponse:GetJsonText("access_token")
    EndIf

Return cRacToken

/*/{Protheus.doc} function fValidPlatform
Fun��o que realiza a valida��o da URL da Plataforma
@author  Hugo de Oliveira
@since   02/06/2020
@version 1.0
/*/
Static Function fValidPlatform(cPlatformURL, cRacToken)
    Local lRet      := .F.
    Local aHeader   := {}
    Local cPatch    := "/integration/api/entities/protheus/"
    Local oPlatform := FwRest():New(ALLTRIM(cPlatformURL))

	Aadd(aHeader, "Authorization: " + "Bearer " + cRacToken)
    Aadd(aHeader, "Content-Type: application/json" )

   oPlatform:setPath(cPatch)
   
    If oPlatform:Get(aHeader)
        If oPlatform:ORESPONSEH:CSTATUSCODE == "200" .AND. !Empty(oPlatform:GetResult())
            lRet := .T.
        EndIf
    EndIf

    FreeObj(oPlatform)

Return lRet
