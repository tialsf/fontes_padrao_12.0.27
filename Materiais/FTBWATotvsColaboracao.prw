#Include 'PROTHEUS.CH'
#Include "APWEBSRV.CH"


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} FTBWATotvsColaboracao
Classe para troca de mensagens entre o Protheus e o TOTVS Colaboracao (TSS).

@author	Danilo Dias
@since		02/03/2012
@version	P11.7
/*/
//------------------------------------------------------------------------------------------
Class FTBWATotvsColaboracao

	//Propriedades
	Data cURL			//URL do servidor TOTVS Colaboracao
	Data aMonitor		//Array com dados obtidos atrav�s do m�todo de 
						//monitoramento do WS { nStatus, cDescricao, dDataDoc, cHoraDoc, 
						//dDataEnv, cHoraEnv, dDataRec, cHoraRec }
	Data aDocs			//Dados de documentos no formato { cIdTSS, cXML } 
	Data aConfirma	//Dados da confirma��o de documentos { cIdTSS, cStatus }
	Data aRemessa		//Dados com resposta da remessa de documentos { cDescricao, cIdTSS, cIdERP, nStatus }
	Data nStatus		//Status da remessa de documentos
	Data cDescricao	//Descri��o do status do documento
	Data cXML			//XML com dados de envio/recebimento de documentos
	Data cUserToken	//User Token para uso nos m�todos do TOTVS Colabora��o
	Data cIdEnt		//Id da entidade, obtido atrav�s do metodo GetIdEnt()
	Data cErro			//Mensagem de erro na execu��o do m�todo
	Data lErro			//Indica erro na execu��o de algum m�todo
	Data oWSColab		//Objeto que implementa a classe com o servi�o do TOTVS Colabora��o	
	
	//M�todos
	Method New() Constructor		//Construtor da classe
	Method ReceiveDocuments()	//Metodo de recebimento de documentos
	Method SendDocuments()		//Metodo de envio de documentos
	Method ConfirmDocuments()	//Metodo de confirmacao dos documentos
	Method DocumentsMonitor()	//Monitora os documentos que estao no servidor
	Method GetIdEnt()				//Metodo para pegar o Id da entidade no servidor
	Method GetError()				//M�todo para obter mensagens de erro
	Method IsError()				//M�todo que indica se houve erro 

EndClass


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} New
M�todo construtor da classe.

@sample	FTBWATotvsColaboracao():New( cURL, cUserToken )

@param		cUrl			URL do servidor TOTVS Colaboracao.
@param		cUserToken		User Token (Padrao � "TOTVS")

@return	Self			Objeto da classe FTBWATotvsColaboracao

@author	Danilo Dias
@since		02/03/2012
@version	P11.7
/*/
//------------------------------------------------------------------------------------------
Method New( cURL, cUserToken ) Class FTBWATotvsColaboracao

	Default cUserToken := "TOTVS"
	
	::cURL				:= cURL
	::cUserToken		:= cUserToken
	::cErro			:= ""
	::lErro 			:= .F.
	::cIdEnt			:= Self:GetIdEnt()
	::oWSColab			:= WsTSSTotvsColabDoc():New()		//Web Service do TOTVS Colabora��o
	::oWSColab:_URL	:= cURL + '/TSSTOTVSCOLABDOC.apw'	//Endere�o do servidor
			
Return Self


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ReceiveDocuments
M�todo respons�vel pelo recebimento de documentos armazenados no servidor 
TOTVS Colaboracao.

@sample	ReceiveDocuments( dDtInic, dDtFim, nModelo )

@param		dDtInic	Data inicial para recebimento
@param		dDtFim		Data final para recebimento
@param		nModelo	Indica o modelo de documento
						0 - Pedidos
						1 - Nota Fiscal / Aviso de Embarque
						2 - Programa��o de Entrega
						
@return	lRet		Indica se a execu��o do m�todo foi bem sucedida.
			
@author	Danilo Dias
@since		02/03/2012
@version	P11.7
/*/
//------------------------------------------------------------------------------------------
Method ReceiveDocuments( dDtInic, dDtFim, nModelo ) Class FTBWATotvsColaboracao

	Local lRet		:= .T.
	Local nI		:= 1
	Local aDocs	:= {}
	Local cXML		:= ""
	Local cId		:= ""
	
	Default dDtInic 	:= Date()
	Default dDtFim	:= Date()
	Default nModelo 	:= 2
	
	Self:lErro := .F.
	
	//Prepara estrutura para recebimento da resposta
	Self:oWSColab:oWSTOTVSColabRetornaDocResult:oWSRetornaDocsRet := TSSTOTVSColabDoc_ArrayOfRetornaDoc():New()
	
	If Self:oWSColab:TotvsColabRetornaDoc( Self:cUserToken, Self:cIdEnt, dDtInic, dDtFim, nModelo )	
		
		If ( Len( Self:oWSColab:oWSTOTVSColabRetornaDocResult:oWSRetornaDocsRet:oWSRetornaDoc ) > 0 )	
			
			For nI := 1 To Len( Self:oWSColab:oWSTOTVSColabRetornaDocResult:oWSRetornaDocsRet:oWSRetornaDoc )
							
				cId 	:= Self:oWSColab:oWSTOTVSColabRetornaDocResult:oWSRetornaDocsRet:oWSRetornaDoc[nI]:cIdTSS
				cXML 	:= Self:oWSColab:oWSTOTVSColabRetornaDocResult:oWSRetornaDocsRet:oWSRetornaDoc[nI]:cXMLDoc				
				AAdd( aDocs, { cId, cXML } )
				
			Next Ni
			
		EndIf
		
		Self:aDocs := AClone( aDocs )				
		lRet := .T.	
	Else
		Self:lErro	:= .T.
		::cErro 	:= "ReceiveDocuments() Error: " + IIf( Empty( GetWscError(3) ), GetWscError(1), GetWscError(3) )
		lRet 		:= .F.	
	EndIf
	
Return lRet


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ConfirmDocuments
M�todo respons�vel pela confirma��o de integra��o de documentos com o ERP. 
Os resultados ser�o armazenados no atributo aConfirma da classe, que possui o 
formato { { cId1, nStatus1 }, { cId2, nStatus2 }, { cIdN, nStatusN }... }

@sample	ConfirmDocuments( aIdTSS )

@param		aIdTSS		Array com Id's a confirmar.	{ cId1, cId2, cIdN... }
 
@return	lRet		Indica se a execu��o do m�todo foi bem sucedida.

@author	Danilo Dias
@since		02/03/2012
@version	P11.7
/*/
//------------------------------------------------------------------------------------------
Method ConfirmDocuments( cIdTSS ) Class FTBWATotvsColaboracao

	Local nI			:= 1
	
	Self:lErro 	:= .F.
	Self:nStatus 	:= -1
	
	//Prepara estrutura para envio da solicita��o ao Web Service
	Self:oWSColab:oWSConfirmaDocTOTVSColab:oWSColabTSSIds := TSSTOTVSColabDoc_ArrayOfColabId():New()
	 
	//Inclui o Id a consultar para envio ao Web Service	
	AAdd( Self:oWSColab:oWSConfirmaDocTOTVSColab:oWSColabTSSIds:oWSColabId, TSSTOTVSColabDoc_ColabId():New() )	
	Self:oWSColab:oWSConfirmaDocTOTVSColab:oWSColabTSSIds:oWSColabId[nI]:cIdTSS := cIdTSS

	//Prepara estrutura para recebimento da resposta
	Self:oWSColab:oWSTOTVSColabConfirmaDocResult:oWSColabTSSIdsRet := TSSTOTVSColabDoc_ArrayOfColabIdRet():New()
	
	//Aciona m�todo de confirma��o do TOTVS Colabora��o
	If Self:OWSColab:TOTVSColabConfirmaDoc( ::cUserToken, ::cIdEnt, Self:OWSColab:OWSConfirmaDocTOTVSColab )
		
		For nI := 1 To Len( Self:oWSColab:oWSTOTVSColabConfirmaDocResult:oWSColabTSSIdsRet:oWSColabIdRet )
		
			If ( cIdTss == Self:oWSColab:oWSTOTVSColabConfirmaDocResult:oWSColabTSSIdsRet:oWSColabIdRet[nI]:cIdTSS )
				Self:nStatus := Self:oWSColab:oWSTOTVSColabConfirmaDocResult:oWSColabTSSIdsRet:oWSColabIdRet[nI]:nStatus
				Exit
			EndIf
				
		Next nI
		
	Else
		Self:lErro	:= .T.
		::cErro 	:= "ConfirmDocuments() Error: " + IIf( Empty( GetWscError(3) ), GetWscError(1), GetWscError(3) )
	EndIf
	
Return


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} SendDocuments
M�todo respons�vel pelo envio de documentos para o servidor TOTVS Colaboracao

@sample	SendDocuments( cIdERP, cIdTSS, nModelo, cXMLDoc )

@param		cIdERP		Numero de identifica��o do documento no ERP 
@param		cIdTSS 	Numero de identifica��o do documento no TSS
@param		nModelo	Indica o modelo de documento
						0 - Pedidos
						1 - Nota Fiscal / Aviso de Embarque
						2 - Programa��o de Entrega
@param		cXMLDoc	XML a ser enviado.

@return	lRet		Indica se a execu��o do m�todo foi bem sucedida.

@author	Danilo Dias
@since		02/03/2012
@version	P11.7
/*/
//------------------------------------------------------------------------------------------
Method SendDocuments( cIdERP, cIdTSS, nModelo, cXMLDoc ) Class FTBWATotvsColaboracao

	Local lRet 		:= .T.
	Local cDesc		:= ""
	Local cIdTSS		:= ""
	Local cIdERP		:= ""
	Local nStatus		:= 0
	Local aRemessa	:= {}
	
	Default cIdERP 	:= ""
	Default cIdTSS	:= ""
	Default nModelo	:= 0
	Default cXmlDoc	:= ""
	
	Self:lErro := .F.
	
	//Prepara estrutura para envio da solicita��o ao Web Service
	Self:oWSColab:oWsRemessaDocTotvsColab:oWSRemessaDoc := TSSTOTVSColabDoc_ArrayOfDocumento():New()
	AAdd( Self:oWSColab:oWsRemessaDocTotvsColab:oWSRemessaDoc:oWSDocumento, TSSTOTVSColabDoc_Documento():New() )
	
	Self:oWSColab:oWsRemessaDocTotvsColab:oWSRemessaDoc:oWSDocumento[1]:cIdERP	:= cIdERP
	Self:oWSColab:oWsRemessaDocTotvsColab:oWSRemessaDoc:oWSDocumento[1]:cIdTSS	:= cIdTSS
	Self:oWSColab:oWsRemessaDocTotvsColab:oWSRemessaDoc:oWSDocumento[1]:nModelo	:= nModelo
	Self:oWSColab:oWsRemessaDocTotvsColab:oWSRemessaDoc:oWSDocumento[1]:cXMLDoc	:= cXMLDoc

	//Prepara estrutura para recebimento da resposta
	Self:oWSColab:oWSTOTVSColabRemessaDocResult:oWSRemessaDocRet := TSSTOTVSColabDoc_ArrayOfDocumentoRet():New()
	
	If  Self:oWSColab:TOTVSColabRemessaDoc( ::cUserToken, ::cIdEnt, Self:oWSColab:oWsRemessaDocTotvsColab )
	
		cDesc		:= Self:oWSColab:oWSTOTVSColabRemessaDocResult:oWSRemessaDocRet:oWSDocumentoRet[1]:cDescricao
		cIdTSS	 	:= Self:oWSColab:oWSTOTVSColabRemessaDocResult:oWSRemessaDocRet:oWSDocumentoRet[1]:cIdTSS
		cIdERP		:= Self:oWSColab:oWSTOTVSColabRemessaDocResult:oWSRemessaDocRet:oWSDocumentoRet[1]:cIdERP		
		nStatus 	:= Self:oWSColab:oWSTOTVSColabRemessaDocResult:oWSRemessaDocRet:oWSDocumentoRet[1]:nStatus
		
		AAdd( aRemessa, { cDesc, cIdTSS, cIdERP, nStatus } )
	
		Self:aRemessa := AClone( aRemessa )
		lRet := .T.
	Else
		Self:lErro	:= .T.
		::cErro	:= "SendDocuments() Error: " + IIf( Empty( GetWscError(3) ), GetWscError(1), GetWscError(3) )
		lRet 		:= .F.	
	EndIf
	
Return lRet


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} DocumentsMonitor
M�todo respons�vel por monitorar os documentos no servidor TOTVS Colaboracao

@sample	DocumentsMonitor( aIdTSS, nModelo )

@param		aIdTSS		Array contendo os Id's a serem monitorados ( { cId1, cId2, cIdN ... } ) 
@param		nModelo	Indica o modelo de documento
						0 - Pedidos
						1 - Nota Fiscal / Aviso de Embarque
						2 - Programa��o de Entrega

@return	lRet		Indica se a execu��o do m�todo foi bem sucedida.

@author	Danilo Dias
@since		02/03/2012
@version	P11.7
/*/
//------------------------------------------------------------------------------------------
Method DocumentsMonitor( aIdTSS, nModelo ) Class FTBWATotvsColaboracao

	Local lRet			:= .T.
	Local nI			:= 1
	Local aMonitor	:= {}
	Local nStatus		:= 0
	Local cDesc		:= ""
	Local cId			:= ""
	Local dDataDoc	:= Nil
	Local cHoraDoc	:= Nil
	Local dDataEnv	:= Nil
	Local cHoraEnv	:= Nil
	Local dDataRec	:= Nil
	Local cHoraRec	:= Nil
	
	Default aIdTSS 	:= {}
	Default nModelo	:= Nil
	
	Self:lErro := .F.
	
	//Prepara estrutura para envio da solicita��o ao Web Service
	Self:oWSColab:oWSMonitorDocTOTVSColab:oWSMonitorDocs := TSSTOTVSColabDoc_ArrayOfMonitorDoc():New()
	
	For nI := 1 To Len(aIdTSS)
	 
		//Inclui os Id's a consultar para envio ao Web Service	
		AAdd( Self:oWSColab:oWSMonitorDocTOTVSColab:oWSMonitorDocs:oWSMonitorDoc, TSSTOTVSColabDoc_MonitorDoc():New() )	
		Self:oWSColab:oWSMonitorDocTOTVSColab:oWSMonitorDocs:oWSMonitorDoc[nI]:cIdTSS := aIdTSS[nI]
		
	Next nI
	
	//Prepara estrutura para recebimento da resposta
	Self:oWSColab:oWSTOTVSColabMonitorDocResult:oWSMonitorDocsRet := TSSTOTVSColabDoc_ArrayOfMonitorDocRet():New()
	AAdd( Self:oWSColab:oWSTOTVSColabMonitorDocResult:oWSMonitorDocsRet:oWSMonitorDocRet, TSSTOTVSColabDoc_MonitorDocRet():New() )

	//Aciona m�todo de monitoramento
	If Self:oWSColab:TOTVSColabMonitorDoc( ::cUserToken, ::cIdEnt, Self:oWSColab:oWSMonitorDocTOTVSColab, nModelo )
	
		//Guarda os dados da resposta no array
		For nI := 1 To Len(Self:oWSColab:oWSTOTVSColabMonitorDocResult:oWSMonitorDocsRet:oWSMonitorDocRet)
		
			cId			:= Self:oWSColab:oWSTOTVSColabMonitorDocResult:oWSMonitorDocsRet:oWSMonitorDocRet[nI]:cIdTSS
			nStatus 	:= Self:oWSColab:oWSTOTVSColabMonitorDocResult:oWSMonitorDocsRet:oWSMonitorDocRet[nI]:nStatus
			cDesc		:= Self:oWSColab:oWSTOTVSColabMonitorDocResult:oWSMonitorDocsRet:oWSMonitorDocRet[nI]:cDescricao
			dDataDoc	:= Self:oWSColab:oWSTOTVSColabMonitorDocResult:oWSMonitorDocsRet:oWSMonitorDocRet[nI]:dDtDoc
			cHoraDoc	:= Self:oWSColab:oWSTOTVSColabMonitorDocResult:oWSMonitorDocsRet:oWSMonitorDocRet[nI]:cHrDoc
			dDataEnv	:= Self:oWSColab:oWSTOTVSColabMonitorDocResult:oWSMonitorDocsRet:oWSMonitorDocRet[nI]:dDtTSSEnv
			cHoraEnv	:= Self:oWSColab:oWSTOTVSColabMonitorDocResult:oWSMonitorDocsRet:oWSMonitorDocRet[nI]:cHrTSSEnv
			dDataRec	:= Self:oWSColab:oWSTOTVSColabMonitorDocResult:oWSMonitorDocsRet:oWSMonitorDocRet[nI]:dDtTSSRec
			cHoraRec	:= Self:oWSColab:oWSTOTVSColabMonitorDocResult:oWSMonitorDocsRet:oWSMonitorDocRet[nI]:cHrTSSRec
			
			AAdd( aMonitor, { cId, nStatus, cDesc, dDataDoc, cHoraDoc, dDataEnv, cHoraEnv, dDataRec, cHoraRec } )
		
		Next nI
		
		Self:aMonitor := aClone( aMonitor )
		lRet := .T.
	Else
		Self:lErro	:= .T.
		::cErro 	:= "DocumentsMonitor() Error: " + IIf( Empty( GetWscError(3) ), GetWscError(1), GetWscError(3) )
		lRet 		:= .F.
	EndIf
	
Return lRet


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} GetIdEnt
Obt�m o Id da empresa para utiliza��o nos m�todos do TOTVS Colabora��o.

@sample	GetIdEnt()

@return	cIdEnt		Id da entidade.

@author	Danilo Dias
@since		02/03/2012
@version	P11.7
/*/
//------------------------------------------------------------------------------------------
Method GetIdEnt() Class FTBWATotvsColaboracao

	Local cIdEnt 		:= Nil
	Local oWS_SPED	:= Nil
	
	Self:lErro := .F.
	
	oWS_SPED := WsSPEDAdm():New()
	
	oWS_SPED:cUserToken 				:= ::cUserToken	
	oWS_SPED:oWSEmpresa:cCNPJ		:= IIf( SM0->M0_TPINSC == 2 .Or. Empty( SM0->M0_TPINSC ), SM0->M0_CGC, "" )	
	oWS_SPED:oWSEmpresa:cCPF       	:= IIf( SM0->M0_TPINSC == 3, SM0->M0_CGC, "" )
	oWS_SPED:oWSEmpresa:cIE        	:= SM0->M0_INSC
	oWS_SPED:oWSEmpresa:cIM        	:= SM0->M0_INSCM
	oWS_SPED:oWSEmpresa:cNome      	:= SM0->M0_NOMECOM
	oWS_SPED:oWSEmpresa:cFantasia	:= SM0->M0_NOME
	oWS_SPED:oWSEmpresa:cEndereco	:= FisGetEnd( SM0->M0_ENDENT )[1]
	oWS_SPED:oWSEmpresa:cNum       	:= FisGetEnd( SM0->M0_ENDENT )[3]
	oWS_SPED:oWSEmpresa:cCompl     	:= FisGetEnd( SM0->M0_ENDENT )[4]
	oWS_SPED:oWSEmpresa:cUF        	:= SM0->M0_ESTENT
	oWS_SPED:oWSEmpresa:cCEP       	:= SM0->M0_CEPENT
	oWS_SPED:oWSEmpresa:cCod_Mun   	:= SM0->M0_CODMUN
	oWS_SPED:oWSEmpresa:cCod_Pais  	:= "1058"
	oWS_SPED:oWSEmpresa:cBairro    	:= SM0->M0_BAIRENT
	oWS_SPED:oWSEmpresa:cMun       	:= SM0->M0_CIDENT
	oWS_SPED:oWSEmpresa:cCEP_CP		:= Nil
	oWS_SPED:oWSEmpresa:cCP			:= Nil
	oWS_SPED:oWSEmpresa:cDDD			:= Str( FisGetTel( SM0->M0_TEL )[2], 3 )
	oWS_SPED:oWSEmpresa:cFone		:= AllTrim(Str(FisGetTel(SM0->M0_TEL)[3],15))
	oWS_SPED:oWSEmpresa:cFax			:= AllTrim(Str(FisGetTel(SM0->M0_FAX)[3],15))
	oWS_SPED:oWSEmpresa:cEMail		:= UsrRetMail(RetCodUsr())
	oWS_SPED:oWSEmpresa:cNIRE		:= SM0->M0_NIRE
	oWS_SPED:oWSEmpresa:dDTRE		:= SM0->M0_DTRE
	oWS_SPED:oWSEmpresa:cNIT			:= IIf( SM0->M0_TPINSC == 1, SM0->M0_CGC, "" )
	oWS_SPED:oWSEmpresa:cINDSITESP	:= ""
	oWS_SPED:oWSEmpresa:cID_Matriz	:= ""
	
	oWS_SPED:oWSOutrasInscricoes:oWSInscricao := SPEDAdm_ArrayOfSPED_GenericStruct():New()
	oWS_SPED:_URL := AllTrim( ::cURL ) + "/SPEDADM.apw"
	
	If oWS_SPED:ADMEMPRESAS()
		cIdEnt  := oWS_SPED:cAdmEmpresasResult
	Else
		Self:lErro	:= .T.
		::cErro 	:= "GetIdEnt() Error: " + IIf( Empty( GetWscError(3) ), GetWscError(1), GetWscError(3) )
	EndIf

Return cIdEnt


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} GetError
Retorna a mensagem do �ltimo erro ocorrido.

@sample	GetError()
@return	cErro		Mensagem de erro.

@author	Danilo Dias
@since		23/08/2012
@version	P11.7
/*/
//------------------------------------------------------------------------------------------
Method GetError() Class FTBWATotvsColaboracao

Return Self:cErro


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} IsError
Indica se houve erro na execu��o de algum m�todo.

@sample	GetError()

@return	cErro		Mensagem de erro.

@author	Danilo Dias
@since		23/08/2012
@version	P11.7
/*/
//------------------------------------------------------------------------------------------
Method IsError() Class FTBWATotvsColaboracao

Return Self:lErro
