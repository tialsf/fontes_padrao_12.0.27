#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH" 
#INCLUDE "CRM980EventCOL.CH"   

//-------------------------------------------------------------------
/*/{Protheus.doc} CRM980EventCOL
Classe respons�vel pelo evento das regras de neg�cio da 
localiza��o Col�mbia.

@type 		Classe
@author 	Squad CRM / FAT
@version	12.1.17 / Superior
@since		24/05/2017 
/*/
//-------------------------------------------------------------------
Class CRM980EventCOL From FwModelEvent 
		
	Method New() CONSTRUCTOR
	
	//---------------------
	// PosValid do Model. 
	//---------------------
	Method ModelPosVld()
		
EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} New
Metodo respons�vel pela constru��o da classe.

@type 		M�todo
@author 	Squad CRM / FAT
@version	12.1.17 / Superior
@since		24/05/2017 
/*/
//-------------------------------------------------------------------
Method New() Class CRM980EventCOL
Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelPosVld
M�todo respons�vel por executar as valida��es das regras de neg�cio
gen�ricas do cadastro antes da grava��o do formulario.
Se retornar falso, n�o permite gravar.

@type 		M�todo

@param 		oModel	,objeto	,Modelo de dados de Clientes.
@param 		cID		,caracter	,Identificador do sub-modelo.

@author 	Squad CRM / FAT
@version	12.1.17 / Superior
@since		24/05/2017 
/*/
//-------------------------------------------------------------------
Method ModelPosVld(oModel,cID) Class CRM980EventCOL
	Local lValid 		:= .T.
	Local nOperation	:= oModel:GetOperation()
	Local oMdlSA1		:= oModel:GetModel("SA1MASTER")
	
	If ( nOperation == MODEL_OPERATION_INSERT .Or. nOperation == MODEL_OPERATION_UPDATE )
		
		If !Empty( oMdlSA1:GetValue("A1_TIPDOC") )
		
			If oMdlSA1:GetValue("A1_TIPDOC") == "31" //Tipo de Documento de Indetificacion NIT
				If Empty( oMdlSA1:GetValue("A1_CGC") )	                                          
					Help(,,"MDLPVLD",,STR0001,1,0) //"Deve ser indicado o campo NIT do Cliente."
			   		lValid := .F.
			    EndIf              
			    If !Empty( oMdlSA1:GetValue("A1_PFISICA") )
			    	Help(,,"MDLPVLD",,STR0002,1,0) //"O campo RG/Ced. Ext. deve estar vazio."
			    	lValid := .F.
			    EndIf
			Else
			    If !Empty( oMdlSA1:GetValue("A1_CGC") )	                                          
			    	Help(,,"MDLPVLD",,STR0003,1,0) //"O campo NIT do Cliente deve estar vazio."
			    	lValid := .F.
			    EndIf              
			    If Empty( oMdlSA1:GetValue("A1_PFISICA") )
			    	Help(,,"MDLPVLD",,STR0004,1,0) //"Deve ser indicado o campo RG/Ced Ext."
			    	lValid := .F.
			    EndIf
			EndIf
		
		EndIf
	
	EndIf            	
Return lValid