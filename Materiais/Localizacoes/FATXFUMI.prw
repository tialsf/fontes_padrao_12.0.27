#include 'protheus.ch'
#include 'FATXFUMI.ch'

/*/{Protheus.doc} FATXMIDsIt
Funci�n para tratamineto de descuentos por �tem de Pedidos de Venta (Mercado Internacional)
@type
@author luis.enriquez
@since 04/03/2020
@version 1.0
@param nPrUnit, numeric, Precio Unitario del �tem
@param nPrcVen, numeric, Precio de Venta del �tem
@param nQtdVen, numeric, Cantidad del �tem
@param nTotal, numeric, Valor total del �tem
@param nPerc, numeric, Porcentaje de descuento del �tem
@param nDesc, numeric, Importe de descuento del �tem
@param nDescOri, numeric, Importe de descuento original del �tem
@param nTipo, numeric, Tipo de descuento del �tem (1-Porcentaje, 2-Valor descuento)
@param nQtdAnt, numeric, Cantidad anterior del �tem
@param nMoeda, numeric, Moneda del �tem
@return nPreco, numeric, Precio del �tem con descuento
@example
LXMexAcc(@aRotina) 
@see (links_or_references)
/*/
Function FATXMIDsIt(nPrUnit,nPrcVen,nQtdVen,nTotal,nPerc,nDesc,nDescOri,nTipo,nQtdAnt,nMoeda)
	Local nPreco 		:= 0
	Local nValTot		:= 0
	
	Default nMoeda		:= Nil
	Default nTipo		:= 1
	Default nQtdAnt		:= nQtdVen
	
	//��������������������������������������������������������������Ŀ
	//�Calculo o Preco de Lista quando nao houver tabela de preco    �
	//����������������������������������������������������������������
	If nPrUnit == 0
		nPrUnit += a410Arred((nTotal + nDescOri) / nQtdAnt,"D2_PRCVEN")		
	EndIf
	//��������������������������������������������������������������Ŀ
	//�Calcula o novo preco de Venda                                 �
	//����������������������������������������������������������������
	If nTipo == 1
		nPreco := A410Arred(nPrUnit * (1-(nPerc/100)),"D2_PRCVEN")
		nTotal := A410Arred((nPrUnit * nQtdVen) * (1-(nPerc/100)),"D2_TOTAL")
	Else
		If nDesc > 0 .And. (IsInCallStack('CN120GrvPed') .Or. IsInCallStack('CN121GerDoc'))
			nPreco := (nTotal - nDesc) / nQtdVen
		Else
			nPreco := A410Arred(nPrUnit-(nDesc/nQtdVen),"D2_PRCVEN",nMoeda)
			nTotal := A410Arred((nPrUnit * nQtdVen) - nDesc,"D2_TOTAL")	
		EndIf
	EndIf

	nValTot:= A410Arred(nPrUnit * nQtdVen,"D2_TOTAL",nMoeda)
	//��������������������������������������������������������������Ŀ
	//�Calculo dos descontos                                         �
	//����������������������������������������������������������������

	If nPrUnit == 0
		nDesc := 0
		nPerc := 0
	Else	
		nDesc := A410Arred(nValTot-nTotal,"D2_DESCON")
		If nTipo <> 1
			nPerc := A410Arred((1-(nPreco/nPrUnit))*100,"C6_DESCONT")
		EndIf
	EndIf	
Return(nPreco)

/*/{Protheus.doc} FxMIVldItS
Valida que el item seleccionado pertenezca a un documento transmitido en la
rutina MATA465N para NCC - Colombia/Bolivia.
@author Marco Augusto Gonzalez Rivera
@since 25/04/2019
@version 1.0
@param cNumDoc, caracter, (Folio del documento)
@param cSerie,	caracter, (Serie del documento)
@return lRet, Verdadero si el documento se entra transmitido.
/*/
Function FxMIVldItS(cNumDoc, cSerie)
	Local lRet		:= .T.
	Local lM465PE  := .T.
	Local cFunName	:= FunName()
	Local aArea		:= {}
	Local lFactElec	:= !Empty(GetMV("MV_PROVFE", .F., "")) //Facturacion Electronica Activa
	Local cTpDoc := ""
	Local cVldD  := ""
	Local lValFE := .T.	
	Local lCFDUso  := IIf(Alltrim(GetMv("MV_CFDUSO", .T., "1"))<>"0", .T.,.F.)
	
	Default cNumDoc	:= ""
	Default cSerie	:= ""
	
	If ExistBlock("M465DORIFE")
		lM465PE := ExecBlock("M465DORIFE",.F.,.F.,{xFilial("SF2"),cNumDoc,cSerie,M->F1_FORNECE,M->F1_LOJA})
	EndIf
	
	If lM465PE
		IF cPaisLoc=="COL"
			cTpDoc := AllTrim(ObtColSAT("S017",Alltrim(M->F1_TIPOPE),1,4,85,3))
			cVldD  := AllTrim(ObtColSAT("S017",Alltrim(M->F1_TIPOPE),1,4,88,1))
			lValFE := IIf((cTpDoc == "NCC" .And. cVldD $ "1|2") .Or. !(cTpDoc $ "NF|NDC|NCC") .Or. !(cVldD $ "0|1|2"),.T.,.F.)
			If cFunName $ "MATA465N" .And. lFactElec .And. lValFE
				aArea := GetArea()
				dbSelectArea("SF2")
				SF2->(dbSetOrder(1)) //F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENTE+F2_LOJA+F2_FORMUL+F2_TIPO
				//Regla de negocio
				If SF2->(MsSeek(xFilial("SF2") + cNumDoc + cSerie))
					If (Empty(SF2->F2_FLFTEX) .Or. SF2->F2_FLFTEX == "0") .Or. Empty(SF2->F2_UUID)
						MsgAlert(StrTran(STR0001, '###', AllTrim(SF2->F2_SERIE) + " " +  AllTrim(SF2->F2_DOC))) //"El documento seleccionado (###), no ha sido transmitido. Realice la transmisi�n e intente nuevamente."
						lRet := .F.
					EndIf
					If lRet .And. cVldD == "2" .And. Len(Alltrim(SF2->F2_UUID)) <> 40
						MsgAlert(StrTran(STR0002, '###', AllTrim(SF2->F2_SERIE) + " " +  AllTrim(SF2->F2_DOC))) //"El UUID del documento seleccionado (###), no pertenece a un documento emitido con el modelo de validaci�n posterior."
						lRet := .F.					
					EndIf
				EndIf
				RestArea(aArea)
			EndIf
		ElseIf cPaisLoc == "BOL" .AND. cFunName $ "MATA465N" .And. lCFDUso 
				aArea := GetArea()
				dbSelectArea("SF2")
				SF2->(dbSetOrder(1)) //F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENTE+F2_LOJA+F2_FORMUL+F2_TIPO
				//Regla de negocio
				If SF2->(MsSeek(xFilial("SF2") + cNumDoc + cSerie))
					If Empty(SF2->F2_CODDOC)  .Or. SF2->F2_FLFTEX <> '6' .Or. Empty(SF2->F2_UUID)
						MsgAlert(StrTran(STR0003, '###', AllTrim(SF2->F2_SERIE) + " " +  AllTrim(SF2->F2_DOC))) //"El documento seleccionado (###), no ha sido transmitido y/o validado. Realice la transmisi�n y validacion de recepci�n del documento e intente nuevamente."
						lRet := .F.
					EndIf
				EndIf
				RestArea(aArea)	
			EndIf
		EndIf
Return lRet