#include "fisa817.ch"
#include "protheus.ch"
#include "fileio.ch"
#include "shell.ch"
#include "xmlxfun.ch"
#include "vkey.ch"

// Elementos de aRegs
#Define dfRecNo			1	// Recno()
#Define dfEstatus		2	// Status actual (F1/F2_FLFTEX)
#Define dfCancelable	3	// Es cancelable (F1/F2_ESCANC)
#Define dfXML			4	// Nombre xml
#Define dfResultado		5	// Resultado de WS
#Define dfMensaje		6	// Mensaje de WS
#Define dfActualiza		7	// Actualizar status: .T. / .F. 
#Define dfDocumento		8	// Doc + Serie
#Define dfCliente		9	// Cliente + Loja
#Define dfLongitud		9	// Subarreglo en aRegs

// #########################################################################################
// Projeto: Factura Electr�nica M�xico
// Modulo : SIGATFAT
// Fonte  : FISA817
// -----------+-------------------+------------+--------------------------------------------
// Data       | Autor             | Issue      | Descricao
// -----------+-------------------+------------+--------------------------------------------
// 23/10/2018 | Alberto Rodriguez |DMINA-4570  | Servicios de Cancelaci�n de CFDI.
// -----------+-------------------+------------+--------------------------------------------
// 07/12/2018 | Alf Medrano       |DMINA-4858  | se quita line en blanco para relacionar la rutina al issue
// 07/12/2018 | M.Camargo         |DMINA-4980  | se agrega comentario para relacionar la rutina al issue
// 07/12/2018 | M.Camargo         |DMINA-4923  | se agrega comentario para relacionar la rutina al issue
// 09/01/2019 | Alberto Rodriguez |DMINA-4923  | Masteredi: Respuesta JSON con atributos diferentes al SAT, sin EstatusUUID.
// 09/04/2019 | Luis Enr�quez Mata|DMINA-6337  | Modificaci�n para que al cancelar CFDI se realice anulado en lugar de borrado de los documentos.
// 09/04/2019 | Luis Enr�quez Mata|DMINA-6337  | Defecto: DMINA-6566 Se modifica agregado de descripci�n del c�digo 201 y 202 cuando en respuesta del PAC es vac�a..
// 09/04/2019 | Alfredo Medrano   |DMINA-6646  | Se agrega fun ObtOrigen que obtiene la rutina origen del documento(E1_ORIGEM)
// 28/05/2019 | Alberto Rodriguez |DMINA-6699  | Ejecuci�n directa de las rutinas de la LOCXNF (MATA467N, MATA465N); sin MsExecAuto()
// 25/07/2019 | Alf Medrano       |DMINA-7162  | En fun RespWS para el proceso �cancelaci�n con aceptaci�n� se v�lida para que no se elimine el docto y se quede en estatus de "proceso de cancelaci�n"".
//----------------------------------------------------------------------------------------------------------
// 07/10/2019 | M.Camargo         |DMINA-7452  | ajustes nuevo proceso Cancelaci�n para PAC Soluci�n Factible
// 05/03/2020 | Alberto Rodriguez |DMINA-8306  | Aplica ChangeQuery() a expresi�n para filtrar browse. Depuraci�n F817BRWSFX() por buenas pr�cticas
//            | Marco Augusto Glez|            | Tambien se elimina funcion LeeSX3, porque se reemplaza con el uso de FWSX3Util
// 12/03/2020 | Marco Augusto Glez|DMINA-8183  | Se modifica la funcion ChecaResp() para concatenar el contenido del atributo "EsCancelable" en el mensaje
//            |                   |            | mensaje mostrado al usuario.

/*/{Protheus.doc} FISA817
Servicios de Cancelaci�n de CFDI.

@author    Alberto Rodriguez
@version   11.8
@since     23/10/2018
/*/
//------------------------------------------------------------------------------------------
Function FISA817()
	Local aArea			:= GetArea()	

	Private cCadastro	:= OemToAnsi(STR0001)	//"Cancelaci�n de CFDI"
	Private cPerg		:= "FISA817"
	Private nTipo		:= 0
	Private dFechaI	:= CtoD("")
	Private dFechaF	:= CtoD("")
	Private cSerieI	:= ""
	Private cSerieF	:= ""
	Private cDocI		:= ""
	Private cDocF		:= ""
	Private cClienteI	:= ""
	Private cClienteF	:= ""
	Private cLojaI		:= ""
	Private cLojaF		:= ""

	// Parametros de timbrado
	Private cRutaSrv := ""
	Private cRutaSmr := ""
	Private cCfdUser := ""
	Private cCfdPass := ""
	Private cCFDiPAC := ""
	Private cCFDiAmb := ""
	Private cCFDiCer := ""
	Private cCFDiKey := ""
	Private cCFDiCve := ""
	Private cCFDiNF  := ""
	Private cCFDiNC  := ""
	Private nCFDiCmd := 0
	Private cCFDiSF1 := ""
	Private cCFDiSF2 := ""
	Private lProxySr := .F.
	Private cProxyIP := ""
	Private nProxyPt := 0
	Private lProxyAW := .F.
	Private cProxyUr := ""
	Private cProxyPw := ""
	Private cProxyDm := ""
	Private cLogWS   := ""
	Private cRutina  := ""
	Private cProxy   := "[PROXY]"
	Private cTipo    := ""
	
	// Validar proceso de F.E.
	If !ChecaCFD()
		Return Nil
	EndIf
	
	If Pergunte(cPerg,.T.)
		nTipo		:= MV_PAR01
		dFechaI		:= MV_PAR02
		dFechaF		:= MV_PAR03
		cSerieI		:= MV_PAR04
		cSerieF		:= MV_PAR05
		cDocI		:= MV_PAR06
		cDocF		:= MV_PAR07
		cClienteI	:= MV_PAR08
		cClienteF	:= MV_PAR09
		cLojaI		:= MV_PAR10
		cLojaF		:= MV_PAR11
		If nTipo = 3
			cTipo := "C"
		EndIf
		// Pantalla de Browse
		F817BRWSFX()
	EndIf

	RestArea(aArea)
Return Nil

/*/{Protheus.doc} ChecaCFD
//Valida par�metros de factura electr�nica.
@author ARodriguez
@since 23/10/2018
@version 1.0
${parametersSection}
@return Nil
@example
(examples)
@see (links_or_references)
/*/
Static Function ChecaCFD()
	Local aGerarCFD		:= {}
	Local lRet			:= .T.
	Local cMsgCFD		:= ""
	Local nX			:= 0
	Local nVersion		:= 0

	If !SuperGetmv( "MV_CFDICAN" , .F. , .F. )
		MsgAlert(STR0036, STR0002)	// "No est� en uso el nuevo esquema de cancelaci�n de CFDI." # "CFDI"
		Return .F.
	EndIf

	// Checa parametros
	aGerarCFD := CFDVerific()

	If aGerarCFD[1] == "0"
		MsgAlert(STR0003, STR0002)	// "No est� en uso la Factura Electr�nica" # "CFDI"
		lRet := .F.
		
	ElseIf !Empty(aGerarCFD[2])
		For nX := 1 To Len(aGerarCFD[2])
			cMsgCFD += aGerarCFD[2][nX][2] + CRLF
		Next nX

		MsgAlert(cMsgCFD, STR0002)	// <mensajes generados en CFDVerific()> # "CFDI"
		lRet := .F.

	Else
		cRutaSrv := &(SuperGetmv( "MV_CFDDOCS" , .F. , "\cfd\facturas\" ))	// Ruta donde se encuentran las facturas.xml (servidor)
		
		If GetRemoteType() == 5 //Tratamiento HTML
			cRutaSmr := SuperGetmv( "MV_CFDSMAR" , .F. , "\system\\" )
		Else
			cRutaSmr := &(SuperGetmv( "MV_CFDSMAR" , .F. , "GetClientDir()" ))	// Ruta A donde se copiaran los archivos que vienen del servidor . . .  + "\TimbradoATEB\bin\Debug\"
		Endif
		
		nVersion := Val(GetVersao(.F.))
		
		If nVersion < 12
			cCfdUser := SuperGetmv( "MV_CFDUSER" , .F. , "" )				// Usuario del servicio web
			cCfdPass := SuperGetmv( "MV_CFDPASS" , .F. , "" )				// Password del servicio web
			cCFDiPAC := SuperGetmv( "MV_CFDIPAC" , .F. , "" )				// Rutina a ejecutar (PAC)
			cCFDiAmb := SuperGetmv( "MV_CFDIAMB" , .F. , "T" )				// Ambiente (Teste o Produccion)
			cCFDiCer := SuperGetmv( "MV_CFDICER" , .F. , "" )				// Archivo de llave p�blica (.cer)
			cCFDiKey := SuperGetmv( "MV_CFDIKEY" , .F. , "" )				// Archivo de llave privada (.key)
			cCFDiCve := SuperGetmv( "MV_CFDICVE" , .F. , "" )				// Clave de certificado de llave privada
		Else
			cCfdUser := SuperGetmv( "MV_CFDI_US" , .F. , "" )				// Usuario del servicio web (Ant: MV_CFDUSER)
			cCfdPass := SuperGetmv( "MV_CFDI_CO" , .F. , "" )				// Password del servicio web (Ant: MV_CFDPASS)
			cCFDiPAC := SuperGetmv( "MV_CFDI_PA" , .F. , "" )				// Rutina a ejecutar (PAC) (Ant: MV_CFDIPAC)
			cCFDiAmb := SuperGetmv( "MV_CFDI_AM" , .F. , "T" )				// Ambiente (Teste o Produccion) (Ant: MV_CFDIAMB)
			cCFDiCer := SuperGetmv( "MV_CFDI_CE" , .F. , "" )				// Archivo de llave p�blica (.cer) (Ant: MV_CFDICER)
			cCFDiKey := SuperGetmv( "MV_CFDI_PR" , .F. , "" )				// Archivo de llave privada (.key) (Ant: MV_CFDIKEY)
			cCFDiCve := SuperGetmv( "MV_CFDI_CL" , .F. , "" )				// Clave del certificado de llave privada
		EndIf

		nCFDiCmd := SuperGetmv( "MV_CFDICMD" , .F. , 0 )					// Mostrar ventana de comando del Shell: 0=no, 1=si
		cCFDiSF1 := SuperGetmv( "MV_CFDNAF1" , .F. , "" )					// Nombre para archivo xml de SF1
		cCFDiSF2 := SuperGetmv( "MV_CFDNAF2" , .F. , "" )					// Nombre para archivo xml de SF2
		lProxySr := SuperGetmv( "MV_PROXYSR" , .F. , .F. )					// Emplear Proxy Server?
		cProxyIP := Trim(SuperGetmv( "MV_PROXYIP" , .F. , "" ))				// IP del Proxy Server
		nProxyPt := SuperGetmv( "MV_PROXYPT" , .F. , 0 )					// Puerto del Proxy Server
		lProxyAW := SuperGetmv( "MV_PROXYAW" , .F. , .F. )					// Autenticaci�n en Proxy Server con credenciales de Windows?
		cProxyUr := Trim(SuperGetmv( "MV_PROXYUR" , .F. , "" ))				// Usuario para autenticar Proxy Server
		cProxyPw := Trim(SuperGetmv( "MV_PROXYPW" , .F. , "" ))				// Clave para autenticar Proxy Server
		cProxyDm := Trim(SuperGetmv( "MV_PROXYDM" , .F. , "" ))				// Dominio para autenticar Proxy Server
		cLogWS   := SuperGetmv( "MV_CFDILOG" , .F. , "LOG" )				// Tipo de log en consumo del servicio web: LOG (default), LOGDET (detallado), NOLOG (ninguno)

		cRutina := "Timbrado" + Trim(cCFDiPAC) + ".exe "
		
		If Empty(cRutaSmr) .Or. !( cRutaSmr == Strtran( cRutaSmr , " " ) )
			cMsgCFD += "MV_CFDSMAR"
		EndIf
		If Empty(cCfdUser)
			cMsgCFD += IIf(nVersion < 12, "MV_CFDUSER", "MV_CFDI_US") + CRLF
		EndIf
		If Empty(cCfdPass)
			cMsgCFD += IIf(nVersion < 12, "MV_CFDPASS", "MV_CFDI_CO") + CRLF
		EndIf
		If Empty(cCFDiPAC)
			cMsgCFD += IIf(nVersion < 12, "MV_CFDIPAC", "MV_CFDI_PA") + CRLF
		EndIf
		If Empty(cCFDiAmb)
			cMsgCFD += IIf(nVersion < 12, "MV_CFDIAMB", "MV_CFDI_AM") + CRLF
		EndIf
		If Empty(cCFDiCer) .And. Empty(cCFDiKey)
			cMsgCFD += IIf(nVersion < 12, "MV_CFDICER", "MV_CFDI_CE") + CRLF
		EndIf
		If Empty(cCFDiKey)
			cMsgCFD += IIf(nVersion < 12, "MV_CFDIKEY", "MV_CFDI_PR") + CRLF
		EndIf
		If Empty(cCFDiCve)
			cMsgCFD += IIf(nVersion < 12, "MV_CFDICVE", "MV_CFDI_CL") + CRLF
		EndIf

		If !Empty(cMsgCFD)
			cMsgCFD := STR0023 + CRLF + cMsgCFD		//"Revise la configuraci�n de los siguientes par�metros:"
		EndIf

		If !Empty(cCFDiPAC) .And. !File( cRutaSmr + Trim(cRutina) )
			cMsgCFD += IIf( !Empty(cMsgCFD), CRLF, "") + Strtran( STR0024 , "#EXEPAC#", cRutina)	//"El ejecutable de timbrado #EXEPAC# no existe en la ruta indicada en el par�metro MV_CFDSMAR."
		EndIf

		If !Empty(cCFDiCer) .And. !File( cRutaSmr + Trim(cCFDiCer) )
			cMsgCFD += IIf( !Empty(cMsgCFD), CRLF, "") + Strtran( STR0025 , "#CERT#", cCFDiCer)	//"El certificado #CERT# no existe en la ruta indicada en el par�metro MV_CFDSMAR."
		EndIf

		If !Empty(cCFDiKey) .And. !File( cRutaSmr + Trim(cCFDiKey) )
			cMsgCFD += IIf( !Empty(cMsgCFD), CRLF, "") + Strtran( STR0025 , "#CERT#", cCFDiKey)	//"El certificado #CERT# no existe en la ruta indicada en el par�metro MV_CFDSMAR."
		EndIf

		If !Empty(cMsgCFD)
			//MsgAlert(cMsgCFD, STR0002)	//  # "CFDI"
			MsgAlert(cMsgCFD + CRLF + cRutaSmr + CRLF + "Version " + Str(nVersion) , STR0002)		//"CFDI"
			lRet := .F.

		Else
			If nCFDiCmd < 0 .Or. nCFDiCmd > 10
				nCFDiCmd := 0
			Endif

			// Par�metros para el Proxy Server
			cProxy += "[" + If( lProxySr , "1" , "0" ) + "]"
			cProxy += "[" + cProxyIP + "]"
			cProxy += "[" + lTrim( Str( nProxyPt ) ) + "]"
			cProxy += "[" + If( lProxyAW , "1" , "0" ) + "]"
			cProxy += "[" + If( lProxyAW , "" , cProxyUr ) + "]"
			cProxy += "[" + If( lProxyAW , "" , cProxyPw ) + "]"
			cProxy += "[" + If( lProxyAW , "" , cProxyDm ) + "]"

		EndIf
		
	EndIf

Return lRet

/*/{Protheus.doc} F817BRWSFX
//Browse con documentos del proceso de cancelaci�n de CFDI.
@author ARodriguez
@since 23/10/2018
@version 1.0
${parametersSection}
@return Nil
@example
(examples)
@see (links_or_references)
nTipo == 1 => cEspecie == "NF " // Factura Venta
nTipo == 2 => cEspecie == "NDC" // Nota D�bito al Cliente
nTipo == 3 => cEspecie == "NCC" // Nota de Cr�dito al cliente
/*/
Static Function F817BRWSFX()
	Local aCores		:= F817Cores(nTipo)
	Local cAliasSF		:= ""
	Local aFields		:= {}
	Local aCampos		:= {}
	Local cCampo		:= ""
	Local cStatus		:= ""
	Local cQuery		:= ""
	Local nX			:= 0
	
	Private cMarca		:= ""
	Private aRotina		:= MenuDef()
	Private aRegs		:= {}
	
	//Se obtienen campos no virtuales
	Private aCabsSF1	:= FWSX3Util():GetAllFields("SF1", .F.)
	Private aCabsSF2	:= FWSX3Util():GetAllFields("SF2", .F.)
	Private aItensSD1	:= FWSX3Util():GetAllFields("SD1", .F.)
	Private aItensSD2	:= FWSX3Util():GetAllFields("SD2", .F.)

	// Genera cadena SQL / Filter segun la tabla de documentos
	If nTipo == 1 .Or. nTipo == 2
		cAliasSF := "SF2"
		aFields := {"F2_DOC","F2_SERIE","F2_CLIENTE","F2_LOJA","F2_EMISSAO","F2_UUID","F2_FECCANC","F2_HORACAN"}
		aCampos := CamposBrw( aFields )
		cCampo := "F2_MARK"
		cQuery := "F2_ESPECIE = " + IIf(nTipo == 1, "'NF'", "'NDC'") + ;
					" AND F2_EMISSAO BETWEEN '" + DtoS(MV_PAR02) + "' AND '" + DtoS(MV_PAR03) + "'" + ;
					" AND F2_SERIE >= '" + MV_PAR04 + "' AND F2_SERIE <= '" + MV_PAR05 + "'" + ;
					" AND F2_DOC >= '" + MV_PAR06 + "' AND F2_DOC <= '" + MV_PAR07 + "'" + ;
					" AND F2_CLIENTE >= '" + MV_PAR08 + "' AND F2_CLIENTE <= '" + MV_PAR09 + "'" + ;
					" AND F2_LOJA >= '" + MV_PAR10 + "' AND F2_LOJA <= '" + MV_PAR11 + "'" + ;
					" AND F2_UUID <> ''"

	ElseIf nTipo == 3 // Nota de Cr�dito al cliente
		cAliasSF := "SF1"
		aFields := {"F1_DOC","F1_SERIE","F1_FORNECE","F1_LOJA","F1_EMISSAO","F1_UUID","F1_FECCANC","F1_HORACAN"}
		aCampos := CamposBrw( aFields )
		cCampo := "F1_MARK"
		cQuery := "F1_ESPECIE = 'NCC'" + ;
					" AND F1_EMISSAO BETWEEN '" + DtoS(MV_PAR02) + "' AND '" + DtoS(MV_PAR03) + "'" + ;
					" AND F1_SERIE >= '" + MV_PAR04 + "' AND F1_SERIE <= '" + MV_PAR05 + "'" + ;
					" AND F1_DOC >= '" + MV_PAR06 + "' AND F1_DOC <= '" + MV_PAR07 + "'" + ;
					" AND F1_FORNECE >= '" + MV_PAR08 + "' AND F1_FORNECE <= '" + MV_PAR09 + "'" + ;
					" AND F1_LOJA >= '" + MV_PAR10 + "' AND F1_LOJA <= '" + MV_PAR11 + "'" + ;
					" AND F1_UUID <> ''"

	EndIf

	// Aplica ChangeQuery; en Oracle no funciona F?_UUID <> '', debe ser F?_UUID <> ' '
	cQuery := "SELECT * FROM " + RetSqlName(cAliasSF) + " WHERE " + cQuery
	cQuery := ChangeQuery(cQuery)
	cQuery := Alltrim(Substr(cQuery, At(" WHERE ",cQuery)+7))

	cMarca := GetMark(,cAliasSF,cCampo)
	aCores := F817Cores(IIf(cAliasSF == "SF2", 1, 2))

	// Pantalla de Browse con los documentos filtrados
	MarkBrow(cAliasSF,cCampo,cStatus,aCampos,,cMarca,"F817VMARK(1)",,,,"F817VMARK(2)",,cQuery,,aCores)

	// Desmarcar registros
	For nX := 1 to Len(aRegs)
		If nTipo == 1 .Or. nTipo == 2
			SF2->(dbGoto(aRegs[nX,dfRecNo]))
			RecLock("SF2", .F.)
			SF2->F2_MARK	:= "  "
			SF2->(MsUnlock())
		Else
			SF1->(dbGoto(aRegs[nX,dfRecNo]))
			RecLock("SF1", .F.)
			SF1->F1_MARK	:= "  "
			SF1->(MsUnlock())
		EndIf
	Next nX
	
	DbSelectArea(cAliasSF)
	RetIndex(cAliasSF)

Return Nil

/*/{Protheus.doc} MenuDef
//Genera botones de opciones del proceso.
@author ARodriguez
@since 23/10/2018
@version 1.0
${parametersSection}
@return Nil
@example
(examples)
@see (links_or_references)
/*/
Static Function MenuDef()
	Local aRotina := {}
	
	aAdd(aRotina, {STR0005 ,"F817Consul"		,0,2,0,.F.}) //Act. Status
	aAdd(aRotina, {STR0004 ,"F817Cancel"		,0,2,0,.F.}) //Cancelar
	aAdd(aRotina, {STR0006 ,"F817Visual"		,0,2,0,.F.}) //Visualizar
	aAdd(aRotina, {STR0007 ,"F817Leyend"		,0,2,0,.F.}) //Leyenda
	aAdd(aRotina, {STR0008 ,"PesqBrw"			,0,1,0,.F.}) //Buscar

Return aRotina

/*/{Protheus.doc} F817Cores
//Sem�foros con color seg�n el status.
@author ARodriguez
@since 23/10/2018
@version 1.0
${parametersSection}
@return Nil
@example
(examples)
@see (links_or_references)
/*/
Static Function F817Cores(nType)
	Local aCores := {}
	Default nType := 1
	
	If nType == 1
		aCores := {	{"(F2_FLFTEX==' ' .Or. F2_FLFTEX=='0') .And. F2_ESCANC==' '", 'BR_VERDE' },;	//Vigente
					{"(F2_FLFTEX==' ' .Or. F2_FLFTEX=='0') .And. F2_ESCANC=='1'", 'BR_LARANJA' },;	//Vigente - Cancelable sin aceptaci�n
					{"(F2_FLFTEX==' ' .Or. F2_FLFTEX=='0') .And. F2_ESCANC=='2'", 'BR_PINK' },;		//Vigente - Cancelable con aceptaci�n
					{"F2_FLFTEX=='1'", 'BR_AZUL'},;													//En proceso
					{"F2_FLFTEX=='2'", 'BR_PRETO'},;												//Rechazado
					{"F2_FLFTEX=='3' .Or. F2_FLFTEX=='4' .Or. F2_FLFTEX=='5'", 'BR_AMARELO'},;		//Cancelada -> deber�a ser BR_VERMELHO pero no procede
					{"F2_FLFTEX=='6'", 'BR_AMARELO'}}												//Documentos relacionados
	Else
		aCores := {	{"(F1_FLFTEX==' ' .Or. F1_FLFTEX=='0') .And. F1_ESCANC==' '", 'BR_VERDE' },;	//Vigente
					{"(F1_FLFTEX==' ' .Or. F1_FLFTEX=='0') .And. F1_ESCANC=='1'", 'BR_LARANJA' },;	//Vigente - Cancelable sin aceptaci�n
					{"(F1_FLFTEX==' ' .Or. F1_FLFTEX=='0') .And. F1_ESCANC=='2'", 'BR_PINK' },;		//Vigente - Cancelable con aceptaci�n
					{"F1_FLFTEX=='1'", 'BR_AZUL'},;													//En proceso
					{"F1_FLFTEX=='2'", 'BR_PRETO'},;												//Rechazado
					{"F1_FLFTEX=='3' .Or. F1_FLFTEX=='4' .Or. F1_FLFTEX=='5'", 'BR_AMARELO'},;		//Cancelada -> deber�a ser BR_VERMELHO pero no procede
					{"F1_FLFTEX=='6'", 'BR_AMARELO'}}												//Documentos relacionados
	EndIf

Return aCores

/*/{Protheus.doc} F817Leyend
//Leyenda: Pantalla con descripci�n de status.
@author ARodriguez
@since 23/10/2018
@version 1.0
${parametersSection}
@return Nil
@example
(examples)
@see (links_or_references)
/*/
Function F817Leyend()
	BrwLegenda(cCadastro,OemToAnsi(STR0014), {;				//"Cancelaci�n de CFDI" # "Leyenda"
            {"BR_VERDE", OemToAnsi(STR0009)},;				//"Vigente"
            {"BR_LARANJA", OemToAnsi(STR0041)},;			//"Vigente - Cancelable sin aceptaci�n"
            {"BR_PINK", OemToAnsi(STR0042)},;				//"Vigente - Cancelable con aceptaci�n"
            {"BR_AZUL", OemToAnsi(STR0011)},;				//"En proceso de cancelaci�n"
            {"BR_PRETO", OemToAnsi(STR0012)},;				//"Solicitud de cancelaci�n rechazada"
            {"BR_AMARELO", OemToAnsi(STR0013)};				//"Contiene documentos relacionados"
            })	

            // No procede este status porque Protheus no marca cancelaci�n sino que borrar documentos
            //{"BR_VERMELHO", OemToAnsi(STR0010)}			//"Cancelada"

Return Nil

/*/{Protheus.doc} CamposBrw
//Genera array de campos para el Browse.
@author arodriguez
@since 29/10/2018
@version 1.0
@return aColumns, arreglo de campos para MarkBrowse
@param aFields, array, Vector de campos para mostrar en MarkBrowse
@type function
/*/
Static Function CamposBrw( aFields )
	Local aArea	:= GetArea()
	Local aAreaSX3 := SX3->(GetArea())
	Local aColumns := {}
	Local nX := 0

	DbSelectArea('SX3')
	SX3->( DbSetOrder(2) )

	For	nX:=1 To Len(aFields)
		If SX3->( MsSeek( aFields[nX] ) )
			AAdd( aColumns, {aFields[nX], , X3Titulo(), SX3->X3_PICTURE} ) // Capital()
		EndIf	
	Next nX

	RestArea(aAreaSX3)
	RestArea(aArea)

Return aColumns

/*/{Protheus.doc} F817VMark
//Valida si se puede marcar el registro.
@author ARodriguez
@since 23/10/2018
@version 1.0
${parametersSection}
@return lOK, L�gico, Marc�/desmarc�? 
@example
(examples)
@see (links_or_references)
/*/
Function F817VMark( nOpc )
Local lOK      := .T.
Local lValida  := .T.
Local cRutOrig := ""

If nOpc == 1
	// Marcar todos; no procede
	Return .F.
EndIf

// Valida stataus para seleccion de documento
If nTipo == 1 .Or. nTipo == 2
	If !SF2->(Empty(F2_FLFTEX) .Or. F2_FLFTEX $ "0|1|2|6")
		lOK := .F.
	EndIf
	If lOK
		cRutOrig := ObtOrigen(SF2->F2_CLIENTE, SF2->F2_LOJA, SF2->F2_PREFIXO, SF2->F2_DOC )
		lValida := MaCanDelF2("SF2", SF2->(RecNo()), , , , cRutOrig )	
	EndIf
Else
	If !SF1->(Empty(F1_FLFTEX) .Or. F1_FLFTEX $ "0|1|2|6")
		lOK := .F.
	EndIf
	If lOK
		lValida := LxMaCanDelF1(SF1->(Recno()),,,,,,.F.,"MATA465N")
	EndIf
EndIf

If !lValida
	lOK := .F.
EndIf

If lOK
	// Marca/Desmarca
	If nTipo == 1 .Or. nTipo == 2
		// NF/NDC
		RecLock("SF2",.F.)
		SF2->F2_MARK := IIf(SF2->F2_MARK==cMarca,"  ",cMarca)
		SF2->(MsUnLock())

		If SF2->F2_MARK == cMarca
			//Agrega a array de Recno y Status
			aAdd( aRegs , Array(dfLongitud))
			aRegs[Len(aRegs),dfRecNo] := SF2->(Recno())
			aRegs[Len(aRegs),dfEstatus] := SF2->F2_FLFTEX
			aRegs[Len(aRegs),dfCancelable] := SF2->F2_ESCANC
			aRegs[Len(aRegs),dfDocumento] := SF2->F2_DOC + SF2->F2_SERIE
			aRegs[Len(aRegs),dfCliente] := SF2->F2_CLIENTE +"/"+ SF2->F2_LOJA
		Else
			If ( nReg := aScan( aRegs , {|x| x[1]==SF2->(Recno())} ) ) > 0
				//Elimina de array de Recno y Status
				aDel( aRegs , nReg )
				aSize( aRegs , Len(aRegs)-1 )
			EndIf
		EndIf

	Else
		// NCC
		RecLock("SF1",.F.)
		SF1->F1_MARK := IIf(SF1->F1_MARK==cMarca,"  ",cMarca)
		SF1->(MsUnLock())

		If SF1->F1_MARK == cMarca
			//Agrega a array de Recno y Status
			aAdd( aRegs , Array(dfLongitud))
			aRegs[Len(aRegs),dfRecNo] := SF1->(Recno())
			aRegs[Len(aRegs),dfEstatus] := SF1->F1_FLFTEX
			aRegs[Len(aRegs),dfCancelable] := SF1->F1_ESCANC
			aRegs[Len(aRegs),dfDocumento] := SF1->F1_DOC + SF1->F1_SERIE
			aRegs[Len(aRegs),dfCliente] := SF1->F1_FORNECE +"/"+ SF1->F1_LOJA
		Else
			If ( nReg := aScan( aRegs , {|x| x[1]==SF1->(Recno())} ) ) > 0
				//Elimina de array de Recno y Status
				aDel( aRegs , nReg )
				aSize( aRegs , Len(aRegs)-1 )
			EndIf
		EndIf

	EndIf

Else
	If !lValida
		MsgAlert( OemToAnsi(STR0048), OemToAnsi(STR0015))	// "El documento no puede ser seleccionado para anulaci�n." # "ATENCION"
	Else
		MsgAlert( OemToAnsi(STR0016), OemToAnsi(STR0015))	// "Documento ya cancelado." # "ATENCION"
	EndIf
EndIf

Return lOK

/*/{Protheus.doc} F817Visual
//Visualiza documento, utiliza rutinas de LOCXNF.
@author ARodriguez
@since 23/10/2018
@version 1.0
${parametersSection}
@return Nil
@example
(examples)
@see (links_or_references)
/*/
Function F817Visual()
	Private aCfgNF := {}
	
	If nTipo == 1 .Or. nTipo == 2
	 	aCfgNF := MontaCfgNf( nTipo, {}, .T.)
	 Else
	 	aCfgNF := MontaCfgNf( 4, {}, .T.)
	EndIf

 	LocxDlgNF(aCfgNF,2)

	//bFiltraBrw := {|| FilBrowse(cAliasSF,@aIndexSF,@cFiltro) }
	//bFiltraBrw := {|x| If(x==Nil,FilBrowse(cAliasSF,@aIndexSF,@cFiltro),If(x==1,cFiltro,cQuery)) }

	//If !lTopConn
	//	Eval(bFiltraBrw)
	//EndIf
Return Nil

/*/{Protheus.doc} F817Consul
//Consulta status del proceso de cancelaci�n.
@author ARodriguez
@since 23/10/2018
@version 1.0
@return Nil
@example
(examples)
@see (links_or_references)
/*/
Function F817Consul()
	Local nRegs		:= 0

	If Len(aRegs) > 0
		// Checa status de los registros seleccionados
		aEval(aRegs , {|x,y| IIf(Empty(aRegs[y,dfEstatus]) .Or. aRegs[y,dfEstatus]$"0|1|6|", ++nRegs,) } )

		If Len(aRegs) <> nRegs
			MsgAlert( OemToAnsi(STR0018), OemToAnsi(STR0015))	// "Para consultar estado de proceso seleccione solo documentos En Proceso." # "ATENCION"

		ElseIf MsgYesNo(STR0022,cCadastro)						// "�Continuar con la consulta de estado de proceso?" # "Cancelaci�n de CFDI"
			// Ejecutar rutina de consulta de estado de proceso
			EjecutaWS("E")

		EndIf

	Else
		MsgAlert( OemToAnsi(STR0020), OemToAnsi(STR0015))		// "Debe seleccionar al menos un documento para consulta de estado de proceso." # "ATENCION"

	EndIf

Return Nil

/*/{Protheus.doc} F817Cancel
//Solicitud de cancelaci�n.
@author ARodriguez
@since 23/10/2018
@version 1.0
@return Nil
@example
(examples)
@see (links_or_references)
/*/
Function F817Cancel()
Local nRegs := 0

If Len(aRegs) > 0
	// Checa status de los registros seleccionados
	aEval(aRegs , {|x,y| IIf(aRegs[y,dfEstatus]$"0|2|" .And. !Empty(aRegs[y,dfCancelable]) .And. aRegs[y,dfCancelable]$"1|2", ++nRegs,) } )
	
	If UPPER(cCFDiPAC) <> "SOLUCIONFACTIBLE" .and. Len(aRegs) <> nRegs
		MsgAlert( OemToAnsi(STR0017), OemToAnsi(STR0015))	// "Para solicitud de cancelaci�n los documentos deben ser cancelables, realice antes la consulta de estado." # "ATENCION"
	ElseIf MsgYesNo(STR0021,cCadastro)						// "�Continuar con solicitud de cancelaci�n?" # "Cancelaci�n de CFDI"
		// Ejecutar rutina de solicitud de cancelaci�n
		EjecutaWS("S")
	EndIf

Else
	MsgAlert( OemToAnsi(STR0019), OemToAnsi(STR0015))		// "Debe seleccionar al menos un documento para solicitud de cancelaci�n." # "ATENCION"

EndIf

Return Nil

/*/{Protheus.doc} EjecutaWS
//Genera .INI y .Bat para consumo de WS de solicitud cancelaci�n o consulta status.
@author arodriguez
@since 27/10/2018
@version 1.0
@Param1 E=Consulta estado, S=Solicitud cancelaci�n
@return Nil

@type function
/*/
Static Function EjecutaWS(cOpcion)

	Processa( {|| ProcesoWS(cOpcion) }, IIf(cOpcion=="E", STR0043, STR0044), STR0045, .F.)	// "Consulta de estado de CFDI" o "Solicitud de cancelaci�n de CFDI" # "Procesando solicitud... Espere"

	// Imprime log del proceso
	ImprimeLog(cOpcion)

	// Reset del array
	aSize(aRegs, 0)

Return Nil

Static Function ProcesoWS(cOpcion)
	Local cRutaCFDI		:= cRutaSmr + "Recibos\"
	Local cIniFile		:= "timbradocfdi.ini"
	Local cBatch		:= "CancelaCFDI.bat"
	Local cNameCFDI		:= ""
	Local cParametros	:= ""
	Local cResultado	:= ""
	Local cMensaje		:= ""
	Local nHandle		:= 0
	Local nOpc			:= 0
	Local nX			:= 0

	// Archivo para lista de documentos a enviar al WS
	nHandle	:= FCreate( cRutaSmr + cIniFile )

	If nHandle == -1
		MsgAlert( OemToAnsi(STR0026) + cRutaSmr, OemToAnsi(STR0015))		// "No es posible crear archivo temporal en la ruta " # "ATENCION"
		Return .F.
	EndIf

	FWrite( nHandle, "[RECIBOS]" + CRLF )

	// Asegura existencia de carpeta local de xml
	MakeDir( cRutaCFDI )

	For nX := 1 to Len(aRegs)
		// Nombre del xml
		If nTipo == 1 .Or. nTipo == 2
			// NF  - Nota fiscal clientes
			// NDC - Nota de debito clientes
			SF2->(dbGoto(aRegs[nX,dfRecNo]))
			cNameCFDI := &(cCFDiSF2)
		Else
			// NCC - Nota de credito clientes
			SF1->(dbGoto(aRegs[nX,dfRecNo]))
			cNameCFDI := &(cCFDiSF1)
		EndIf

		aRegs[nX, dfXML] := cNameCFDI
		
		//Validar si el archivo xml existe
		If !File(cRutaSrv + cNameCFDI)
			aRegs[nX,5] := STR0027	//"No existe el archivo del CFDI "
			Loop
		Endif

		If File( cRutaCFDI + cNameCFDI )
			FErase( cRutaCFDI + cNameCFDI )
		Endif

		If File( cRutaCFDI + cNameCFDI + ".out" )
			FErase( cRutaCFDI + cNameCFDI + ".out" )
		Endif

		// Copiar archivos .xml del servidor a la ruta del smartclient o la establecida (StartPath...\CFD\RECIBOS\xxx...xxx.XML a x:\totvs\protheusroot\bin\smartclient)
		CpyS2T( cRutaSrv + cNameCFDI , cRutaCFDI )

		// Agrega a la lista
		FWrite( nHandle, cNameCFDI + CRLF )
	Next nX

	fClose( nHandle )

	// parametros: Usuario, Password, Factura.xml, Ambiente,
	cParametros += cCFDUser + " " + cCFDPass + " " + cIniFile + " " +cCFDiAmb +  " " 
	//             Archivo.cer, Archivo.key, ClaveAutenticacion, nil, Solicitud/Estado proceso
	cParametros += cCFDiCer + " " + cCFDiKey + " " + cCFDiCve + " . " + cOpcion + " "
	//			   Proxy, log
	cParametros += cProxy + " " + cLogWS

	If nCFDiCmd == 3 .Or. nCFDiCmd == 10
		nHandle	:= FCreate( cRutaSmr + cBatch )

		If nHandle <> -1
			FWrite( nHandle, cRutaSmr + cRutina + Trim(cParametros) + CRLF )
			FWrite( nHandle, "Pause" + CRLF )
			fClose( nHandle )
			nOpc := WAITRUN( cRutaSmr + cBatch, nCFDiCmd )

		Else
			// Ejecuta cliente de servicio web
			nOpc := WAITRUN( cRutaSmr + cRutina + Trim(cParametros), nCFDiCmd )

		Endif

	Else
		// Ejecuta cliente de servicio web
		nOpc := WAITRUN( cRutaSmr + cRutina + Trim(cParametros), nCFDiCmd )

	Endif
	If  UPPER(cCFDiPAC) == "SOLUCIONFACTIBLE"
		cOpcion := "E"
	EndIf
	// Procesa respuestas
	For nX := 1 to Len( aRegs )
		cNameCFDI := aRegs[nX, dfXML]
		cNameResp := cNameCFDI + IIf(cOpcion=="S",".sol",".con")
		cResultado := ""
		cMensaje := ""

		If nOpc == 0 .And. File( cRutaCFDI + cNameCFDI + ".out" )
			//Copiar respuesta del WS al servidor
			__CopyFile( cRutaCFDI + cNameCFDI + ".out" , cRutaSrv + cNameResp )

			// Revisa contenido de respuesta 
			aRegs[nX, dfActualiza] := ChecaResp(cRutaSrv + cNameResp, cRutaCFDI + cNameCFDI + ".out", @cResultado, @cMensaje, cOpcion)
			IIf( Empty(cMensaje) , cMensaje := STR0029 , )		//"No se pudo procesar la solicitud/consulta de estado"

		Else
			aRegs[nX, dfActualiza] := .F.
			cMensaje := STR0028	//"No se encuentra archivo de respuesta del WS"

		Endif

		aRegs[nX, dfResultado] := cResultado
		aRegs[nX, dfMensaje] := cMensaje

		// Eliminar temporales
		Ferase( cRutaCFDI + cNameCFDI )
		Ferase( cRutaCFDI + cNameCFDI + ".out" )
	Next nX

	// Actualiza status documentos y efecta cancelaciones
	ActualizaDoc(cOpcion)
	
Return Nil

/*/{Protheus.doc} ChecaResp
//Checa la respuesta del WS seg�un la solucitud.
@author arodriguez
@since 30/10/2018
@version 1.0
@return l�gico, (.T.)Correcto o (.F.)Con error
@param cFile, characters, archivo xml de respuesta
@param cResultado, characters, retorna el resultado del ws
@param cOpcion, characters,  (E)-Consulta estado, (S)-Solicitud cancelaci�n
@type function
/*/
Static Function ChecaResp( cFileSrv, cFileRmt , cResultado , cMensaje , cOpcion )
Local oXml		:= Nil
Local cXML		:= ""
Local cCodigo	:= ""
Local cError	:= ""
Local cDetalle	:= ""
Local nHandle 	:= 0
Local aInfoFile	:= {}
Local nSize		:= 0
Local nRegs		:= 0
Local nFor		:= 0
Local cBuffer	:= ""
Local cLine		:= ""
Local lRet      := .F.

oXml := XmlParserFile(cFileSrv, "", @cError, @cDetalle )

If Valtype(oXml) == "O"				//Es un objeto
	SAVE oXml XMLSTRING cXML

	If cOpcion == "E"
		// Consulta de estado - valida xml de respuesta 
		If XmlChildEx(oXml:_CLSESTADO, "_CODIGOESTATUS") <> Nil
			cCodigo := oXml:_CLSESTADO:_CODIGOESTATUS:Text
			cDetalle := oXml:_CLSESTADO:_ESTADO:Text
		EndIf

		If Substr(cCodigo,1,2) == "S "
			// Solicitud recibida correctamente
			lRet := .T.
			If Len(cCodigo) <= 2 .And. XmlChildEx(oXml:_CLSESTADO, "_ESCANCELABLE") <> Nil
				cCodigo += "- " + oXml:_CLSESTADO:_ESCANCELABLE:Text
			EndIf
			cMensaje := cCodigo
		Else
			// Error en recepci�n
			cMensaje := cCodigo + IIf( Substr(cCodigo,1,2)=="N ", "", " " + cDetalle )
		EndIf

	Else
		// Solicitud de cancelaci�n - valida xml de respuesta 
		If XmlChildEx(oXml, "_CLSCANCELA") <> Nil
			cCodigo := oXml:_CLSCANCELA:_CODESTATUS:Text
			If XmlChildEx(oXml:_CLSCANCELA, "_MENSAJE") <> Nil
				cDetalle := oXml:_CLSCANCELA:_MENSAJE:Text
			ElseIf XmlChildEx(oXml:_CLSCANCELA, "_FOLIOS") <> Nil
				// Nodo de estado del UUID
				If ValType(oXml:_CLSCANCELA:_FOLIOS) == "A"
					cDetalle := oXml:_CLSCANCELA:_FOLIOS[1]:_FOLIO:_MENSAJE:Text
				Else
					cDetalle := oXml:_CLSCANCELA:_FOLIOS:_FOLIO:_MENSAJE:Text
				EndIf
			EndIf
		EndIf

		If cCodigo == "0" .Or. "201" $ cCodigo .Or. "202" $ cCodigo
			// Solicitud recibida correctamente
			lRet := .T.
			If Empty(cDetalle)
				cDetalle := IIf(cCodigo == "201", STR0046, IIf(cCodigo == "202", STR0047, "")) //"El folio se ha cancelado con �xito, documento anulado." //"El CFDI ya hab�a sido cancelado previamente, y ser� anulado el documento."
			EndIf
			cMensaje := cCodigo + Trim(" " + cDetalle)
		Else
			// Error en recepci�n
			cMensaje := cCodigo + IIf( Len(Trim(cCodigo))>5, "", " " + cDetalle )
		EndIf

	EndIf

	If !lRet .And. Empty(cCodigo)
		// El archivo no contiene formato esperado ==> Error
		If AT( "<ERROR" , Upper(cXML) ) > 0
			If 	ValType(oXml:_ERROR) == "O"
				cError := oXml:_ERROR:_CODIGO:TEXT
				cDetalle := oXml:_ERROR:_DESCRIPCIONERROR:TEXT
			EndIf

		ElseIf AT( "<CODERROR" , Upper(cXML) ) > 0
			If 	ValType(oXml:_CODERROR) == "O"
				cError := oXml:_CODERROR:_CODIGO:TEXT
				cDetalle := ""
			EndIf

		ElseIf AT( "CFDI:ERROR" , Upper(cXML) ) > 0
			If 	ValType(oXml:_CFDI_ERROR) == "O"
				cError := oXml:_CFDI_ERROR:_CODIGO:TEXT
				cDetalle := oXml:_CFDI_ERROR:_CFDI_DESCRIPCIONERROR:TEXT
			EndIf

	    EndIf

		cMensaje := Trim(cError) + " " + cDetalle
		IIf(Empty(cMensaje), cMensaje := STR0029, )		//"No se pudo procesar la consulta de estado/solicitud de cancelaci�n."
	EndIf
	
	cResultado := cXML
	
Else
	// El archivo no tiene formato de XML 
	Begin Sequence
	   	nHandle := fOpen(cFileRmt)

		If nHandle <= 0
			cResultado := STR0028  //"No se encuentra archivo de respuesta del WS"
			Break
		EndIf

		aInfoFile := Directory(cFileRmt)
		nSize := aInfoFile[ 1 , 2 ]
		nRegs := Int(nSize/2048)

		For nFor := 1 to nRegs
			fRead( nHandle , @cBuffer , 2048 )
			cLine += cBuffer
		Next

		If nSize > nRegs * 2048
			fRead( nHandle , @cBuffer , (nSize - nRegs * 2048) )
			cLine += cBuffer
		EndIf

		fClose(nHandle)
	End Sequence

	If Substr(cLine,1,1) == "("
		cLine := Substr(cLine,2)
		cLine := Strtran( cLine , ")" , " " , 1 , 1 )
	EndIf

	cBuffer := Upper(cLine)

	If cOpcion == "E"
		// Consulta de estado - Busca nodo/atributo previsto
		If "CLSESTADO" $ cBuffer .And. "CODIGOESTATUS" $ cBuffer
			nFor := At("CODIGOESTATUS", cBuffer) + 14
			cCodigo := Substr(cBuffer, nFor, At("</CODIGOESTATUS", cBuffer) - nFor)
		EndIf

		If Substr(cCodigo,1,2) == "S "
			// Solicitud recibida correctamente
			lRet := .T.
		EndIf

		cMensaje := cCodigo

	Else
		// Solicitud de cancelaci�n - Busca nodo/atributo previsto
		If "CLSCANCELA" $ cBuffer .And. "CODESTATUS" $ cBuffer
			nFor := At("CODESTATUS", cBuffer) + 11
			cCodigo := Substr(cBuffer, nFor, At("</CODESTATUS", cBuffer) - nFor)
		EndIf

		If cCodigo == "0" .Or. "201" $ cCodigo .Or. "202" $ cCodigo
			// Solicitud recibida correctamente, Vigente (Es espera) o ya est� cancelado
			lRet := .T.
		EndIf

		cMensaje := cCodigo

	EndIf

	If !lRet .And. Empty(cCodigo)
		// El archivo no contiene formato esperado ==> Error
		nFor := At("<ERROR" , cBuffer)
		If nFor == 0
			nFor := At("<CODERROR", cBuffer)
		EndIf
		If nFor == 0
			nFor := At("CFDI:ERROR" , cBuffer)
			IIf(nFor > 1, --nFor, )
		EndIf
		If nFor > 0
			cError := Substr(cBuffer, nFor)
			nFor := At(">" , cError)
			If nFor > 0
				cError := Substr(cError, 1, nFor)
			EndIf
			cMensaje := cError
		EndIf
		
		IIf(Empty(cMensaje), cMensaje := STR0029, ) //"No se pudo procesar la consulta de estado/solicitud de cancelaci�n."
	EndIf
	
	cResultado := Alltrim(cLine)

Endif

Return 	lRet

Static Function ActualizaDoc(cOpcion)
	Local dFecha	:= Date()
	Local cHora		:= GetRmtTime()
	Local cFechaXML	:= ""
	Local aCabs		:= {}
	Local aItens	:= {}
	Local aAreas	:= {SF1->(GetArea("SF1")), SF2->(GetArea("SF2")), SD1->(GetArea("SD1")),  SD2->(GetArea("SD2")), GetArea()}
	Local cFilSD1	:= xFilial("SD1")
	Local cFilSD2	:= xFilial("SD2")
	Local nX		:= 0
	Local nY		:= 0

	For nX := 1 to Len(aRegs)
		Begin Sequence
		
			If !aRegs[nX, dfActualiza]
				Break
			EndIf
		
			cFechaXML := ""
			aResp := RespWS(cOpcion, aRegs[nX,dfXML], @cFechaXML, aRegs[nX,dfCancelable])
			
			If Empty(aResp[1])
				aRegs[nX, dfActualiza] := .F.
				Break
			EndIf
			
			If nTipo == 1 .Or. nTipo == 2
				// NF  - Nota fiscal clientes
				// NDC - Nota de debito clientes
				SF2->(dbGoto(aRegs[nX,dfRecNo]))

				If aResp[1] == "201" .Or. aResp[1] == "202"
					// Cancelado / Previamente cancelado
					If SF2->F2_FLFTEX $ "3|4|5"
						aResp[1] := SF2->F2_FLFTEX
					Else
						aResp[1] := IIf(SF2->F2_ESCANC=="1", "3", IIf(SF2->F2_ESCANC=="2", "4", "5"))
					Endif

					aResp[2] := SF2->F2_ESCANC
				EndIf

				If SF2->F2_FLFTEX == aResp[1] .And. SF2->F2_ESCANC == aResp[2]
					aRegs[nX, dfMensaje] += " " + STR0040	//"Sin cambio de estado."
					aRegs[nX, dfActualiza] := .F.
					Break
				EndIf

				If aResp[1] $ "3|4|5"
					// Procesa cancelaci�n del documento NF/NDC
					aSize(aCabs, 0)
					aSize(aItens, 0)
					For nY := 1 to Len(aCabsSF2)
						aAdd(aCabs, {aCabsSF2[nY], &("SF2->"+aCabsSF2[nY]), Nil})
					Next nY

					SD2->(dbSetOrder(3))
					SD2->(dbSeek(cFilSD2+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA))
					Do While !SD2->(Eof()) .And. cFilSD2+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA==SD2->D2_FILIAL+SD2->D2_DOC+SD2->D2_SERIE+SD2->D2_CLIENTE+SD2->D2_LOJA
						aAdd(aItens, {})
						For nY := 1 to Len(aItensSD2)
							aAdd(aItens[Len(aItens)], {aItensSD2[nY],&("SD2->"+aItensSD2[nY]), Nil})
						Next nY
						SD2->(dbSkip())
					 Enddo

					// Baja por rutina autom�tica
					BeginTran()
					lMSErroAuto := .F.
					MaFisEnd()

					If nTipo == 1
						MATA467N(aCabs,aItens,6) // MSExecAuto({|x,y,z| MATA467N(x,y,z)},aCabs,aItens,6)
					Else
						MATA465N(aCabs,aItens,6) // MSExecAuto({|x,y,z| MATA465N(x,y,z)},aCabs,aItens,6)
					Endif

					If lMSErroAuto
						DisarmTransaction()	 //MostraErro()
						aRegs[nX,dfMensaje] += " " + STR0030	//"El CFDI se cancel� pero ocurri� error al procesar baja en Protheus."
					Else
						EndTran()
					EndIf

					MsUnlockAll()
				EndIf

				// Actualiza campos relacionados con el proceso de cancelaci�n
				RecLock("SF2", .F.)
				SF2->F2_FLFTEX	:= aResp[1]
				SF2->F2_ESCANC	:= aResp[2]
				
				If aResp[1] $ "3|4|5"
					// Documento cancelado - Fecha actual o del Acuse
					If Empty(cFechaXML)
						SF2->F2_FECCANC	:= dFecha
						SF2->F2_HORACAN	:= cHora
					Else
						SF2->F2_FECCANC	:= StoD( Strtran( Substr(cFechaXML,1,10), "-") )
						SF2->F2_HORACAN	:= Substr(cFechaXML,12,8)
					EndIf
					SF2->F2_FECANTF := SF2->F2_FECCANC
				Else
					// Solo actualiz� estado
					SF2->F2_FECCANC	:= dFecha
					SF2->F2_HORACAN	:= cHora
				EndIf

				SF2->F2_MARK	:= "  "
				SF2->(MsUnlock())
			
			Else
				// NCC - Nota de credito clientes
				SF1->(dbGoto(aRegs[nX,dfRecNo]))

				If aResp[1] == "201" .Or. aResp[1] ==  "202"
					// Cancelado / Previamente cancelado
					If SF1->F1_FLFTEX $ "3|4|5"
						aResp[1] := SF1->F1_FLFTEX
					Else
						aResp[1] := IIf(SF1->F1_ESCANC=="1", "3", IIf(SF1->F1_ESCANC=="2", "4", "5"))
					Endif

					aResp[2] := SF1->F1_ESCANC
				EndIf

				If SF1->F1_FLFTEX == aResp[1] .And. SF1->F1_ESCANC == aResp[2]
					aRegs[nX, dfMensaje] += " " + STR0040	//"Sin cambio de estado."
					aRegs[nX, dfActualiza] := .F.
					Break
				EndIf

				If aResp[1] $ "3|4|5"
					// Procesa cancelaci�n del documento NCC
					aSize(aCabs, 0)
					aSize(aItens, 0)
					For nY := 1 to Len(aCabsSF1)
						aAdd(aCabs, {aCabsSF1[nY], &("SF1->"+aCabsSF1[nY]), Nil})
					Next nY

					SD1->(dbSetOrder(3))
					SD1->(dbSeek(cFilSD1+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA))
					Do While !SD1->(Eof()) .And. cFilSD1+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA==SD1->D1_FILIAL+SD1->D1_DOC+SD1->D1_SERIE+SD1->D1_FORNECE+SD1->D1_LOJA
						aAdd(aItens, {})
						For nY := 1 to Len(aItensSD1)
							aAdd(aItens[Len(aItens)], {aItensSD1[nY],&("SD1->"+aItensSD1[nY]), Nil})
						Next nY
						SD1->(dbSkip())
					 Enddo

					// Baja por rutina autom�tica
					BeginTran()
					lMSErroAuto := .F.
					MaFisEnd()
					MATA465N(aCabs,aItens,6) // MSExecAuto({|x,y,z| MATA465N(x,y,z)},aCabs,aItens,6)

					If lMSErroAuto
						DisarmTransaction()	//MostraErro()
						aRegs[nX,dfMensaje] += " " + STR0030	//"El CFDI se cancel� pero ocurri� error al procesar baja en Protheus."
					Else
						EndTran()
					EndIf

					MsUnlockAll()
				EndIf
			
				// Actualiza campos relacionados con el proceso de cancelaci�n
				RecLock("SF1", .F.)
				SF1->F1_FLFTEX	:= aResp[1]
				SF1->F1_ESCANC	:= aResp[2]
				
				If aResp[1] $ "3|4|5"
					// Documento cancelado - Fecha actual o del Acuse
					If Empty(cFechaXML)
						SF1->F1_FECCANC	:= dFecha
						SF1->F1_HORACAN	:= cHora
					Else
						SF1->F1_FECCANC	:= StoD( Strtran( Substr(cFechaXML,1,10), "-") )
						SF1->F1_HORACAN	:= Substr(cFechaXML,12,8)
					EndIf
					SF1->F1_FECANTF := SF1->F1_FECCANC
				Else
					// Solo actualiz� estado
					SF1->F1_FECCANC	:= dFecha
					SF1->F1_HORACAN	:= cHora
				EndIf

				SF1->F1_MARK	:= "  "
				SF1->(MsUnlock())
			
			EndIf

		End Sequence

		If !aRegs[nX, dfActualiza]
			// Desmarca
			If nTipo == 1 .Or. nTipo == 2
				SF2->(dbGoto(aRegs[nX,dfRecNo]))
				RecLock("SF2", .F.)
				SF2->F2_MARK	:= "  "
				SF2->(MsUnlock())
			Else
				SF1->(dbGoto(aRegs[nX,dfRecNo]))
				RecLock("SF1", .F.)
				SF1->F1_MARK	:= "  "
				SF1->(MsUnlock())
			EndIf
		EndIf

	Next nX

	// Restaura areas... "el �ltimo ser� el primero"
	aEval( aAreas, {|x,y| RestArea(aAreas[y])})
	
Return Nil

/*/{Protheus.doc} RespWS
//Obtiene atributos de la respuesta de WS de consulta estado / solicutud de cancelaci�n.
@author arodriguez
@since 30/10/2018
@version 1.0
@return aResp, array con Estado, EsCancelable, EstadoCancelacion
@param cOpcion, characters, (E)-Consulta estado, (S)-Solicitud cancelaci�n
@param aResp, array, Respuesta del WS (xml)
@type function
/*/
Static Function RespWS(cOpcion, cNameCFDI, cFechaXML, cEsCancel)
Local oXML		:= nil
Local cError	:= ""
Local cDetalle	:= ""
Local cCodigo	:= ""
Local cEstado	:= ""
local cStatus	:= ""
Local cCancel	:= ""
Local aResp		:= {"", ""}

//oXML := XmlParser(cResp, "_", "", "")
cNameCFDI := cRutaSrv + cNameCFDI + IIf(cOpcion=="S",".sol",".con")
oXml := XmlParserFile(cNameCFDI, "", @cError, @cDetalle )

If cOpcion == "E"
	// Consulta de estado
	If XmlChildEx(oXml:_CLSESTADO, "_CODIGOESTATUS") <> Nil
		// XML generado por el ejecutable de timbrado
		cCodigo := oXml:_CLSESTADO:_CODIGOESTATUS:Text
		cEstado := oXML:_CLSESTADO:_ESTADO:Text
		cStatus := oXML:_CLSESTADO:_ESTATUSCANCELACION:Text
		cCancel := oXML:_CLSESTADO:_ESCANCELABLE:Text
	Endif

	// Estado y EstatusCancelacion
	If Substr(cCodigo,1,2) == "S "
		//"S - Comprobante obtenido satisfactoriamente."
		If "VIGENTE" $ Upper(cEstado)
			If "RECHAZADA" $ Upper(cStatus)
				//"Solicitud rechazada"
				aResp[1] := "2"
			ElseIf "PROCESO" $ Upper(cStatus)
				//"En proceso"
				aResp[1] := "1"
			Else
				//"Vigente"
				aResp[1] := "0"
			EndIf

		ElseIf "CANCELADO" $ Upper(cEstado)
			//"Cancelado"
			If "CANCELADO SIN" $ Upper(cStatus)
				//"Cancelado sin aceptaci�n"
				aResp[1] := "3"
			ElseIf "CANCELADO CON" $ Upper(cStatus)
				//"Cancelado con aceptaci�n"
				aResp[1] := "4"
			ElseIf "PLAZO" $ Upper(cStatus)
				//"Plazo vencido"
				aResp[1] := "5"
			EndIf

		EndIf
	Endif

	// EsCancelable
	If "CANCELABLE SIN " $ Upper(cCancel)
		//"Cancelable sin aceptaci�n"
		aResp[2] := "1"
	ElseIf "CANCELABLE CON " $ Upper(cCancel)
		//"Cancelable con aceptaci�n"
		aResp[2] := "2"
	ElseIf "NO CANCELABLE" $ Upper(cCancel)
		//"No cancelable" ==> Documentos relacionados
		aResp[1] := "6"
		aResp[2] := "3"
	Endif

Else
	If XmlChildEx(oXml, "_CLSCANCELA") <> Nil
		// XML generado por el ejecutable de timbrado
		cCodigo := oXml:_CLSCANCELA:_CODESTATUS:Text

		If cCodigo == "0"
			//"En proceso"
			aResp[1] := "1"

			If XmlChildEx(oXml:_CLSCANCELA, "_FOLIOS") <> Nil
				// Nodo de estado del UUID
				If ValType(oXml:_CLSCANCELA:_FOLIOS) == "A"
					cCodigo := oXml:_CLSCANCELA:_FOLIOS[1]:_FOLIO:_ESTATUSUUID:Text
				Else
					If XmlChildEx(oXml:_CLSCANCELA:_FOLIOS:_FOLIO,"_ESTATUSUUID") <> Nil
						cCodigo := oXml:_CLSCANCELA:_FOLIOS:_FOLIO:_ESTATUSUUID:Text
					Else
						If cEsCancel != "2" // Diferente a Cancelable con Aceptaci�n 
							cCodigo := "201"
						EndIf
					EndIf
				EndIf
			EndIf

			If XmlChildEx(oXml:_CLSCANCELA, "_ACUSE") <> Nil
				// Fecha de cancelaci�n (Acuse)
				If XmlChildEx(oXml:_CLSCANCELA:_ACUSE, "_CANCELACFDRESPONSE") <> Nil
					cFechaXML := oXml:_CLSCANCELA:_ACUSE:_CANCELACFDRESPONSE:_CANCELACFDRESULT:_FECHA:Text
				Else
					cFechaXML := ""
				EndIf
			EndIf
		EndIf
	EndIf

	If cCodigo == "201" .Or. cCodigo == "202"
		// Cancelado ==> La rutina de actualizaci�n coloca estado correcto
		aResp[1] := cCodigo
	EndIf

EndIf

Return aResp

/*/{Protheus.doc} ImprimeLog
//Imprime Log del proceso.
@author arodriguez
@since 30/10/2018
@version 1.0
@return Nil

@type function
/*/
Static Function ImprimeLog(cOpcion)
	Local aReturn	:= {"xxxx", 1, "yyy", 2, 2, 1, "",1 }	//"Zebrado"###"Administra��o"
	Local cTamanho	:= "M"
	Local cTitulo	:= STR0001	//"Cancelaci�n de CFDI"
	Local aLogTitle	:= Array(2)	
	Local aLog		:= {}
	Local nLenDoc	:= Len(SF2->(F2_DOC+F2_SERIE)) + 4
	Local nLenCte	:= Len(SF2->(F2_CLIENTE+F2_LOJA)) + 5
	Local nX		:= 1

	aLogTitle[1] := PadR(STR0031,nLenDoc)+PadR(STR0032,nLenCte)+STR0033	//"Documento" # "Cliente" # "Mensaje"
	aLogTitle[2] := IIf(cOpcion=="E", STR0037, STR0038)	//"Resumen del proceso de consulta de estado de CFDI" # "Resumen del proceso de solicitud de cancelaci�n de CFDI"
	aAdd( aLog, {})

	For nX := 1 to Len(aRegs)
	    aAdd(aLog[1],	aRegs[nX,dfDocumento] + Space(4) + ; // Documento + Serie
	    				aRegs[nX,dfCliente] + Space(4) + ; // Cliente + Loja
	    				IIf(!Empty(aRegs[nX,dfMensaje]), aRegs[nX,dfMensaje], STR0034) ) // Detalle # "Procesado." 	    			
	Next nX
	
	aAdd( aLog, {})
	aAdd( aLog[2], "")
	aAdd( aLog[2], STR0039 + Str(Len(aRegs),5))	//"Total de documentos procesados: "
	
	/*
		1 -	aLogFile 	//Array que contem os Detalhes de Ocorrencia de Log
		2 -	aLogTitle	//Array que contem os Titulos de Acordo com as Ocorrencias
		3 -	cPerg		//Pergunte a Ser Listado
		4 -	lShowLog	//Se Havera "Display" de Tela
		5 -	cLogName	//Nome Alternativo do Log
		6 -	cTitulo		//Titulo Alternativo do Log
		7 -	cTamanho	//Tamanho Vertical do Relatorio de Log ("P","M","G")
		8 -	cLandPort	//Orientacao do Relatorio ("P" Retrato ou "L" Paisagem )
		9 -	aRet		//Array com a Mesma Estrutura do aReturn
		10-	lAddOldLog	//Se deve Manter ( Adicionar ) no Novo Log o Log Anterior
	*/
	MsAguarde( { ||fMakeLog( aLog , aLogTitle , , .T. , FunName() , cTitulo , cTamanho , "P" , aReturn , .F. )}, STR0035) //"Generando Log de proceso..."	
Return Nil

/*/{Protheus.doc} ImprimeLog
//Obtiene la rutina origen del documento(E1_ORIGEM) 
@author alf Medrano
@since 30/04/2019
@version 1.0
@return nombre de rutina origen
@type function
/*/
Static function ObtOrigen(cCliente,cLoja,cPrefixo,cNumDoc )
	Local aArea		:= GetArea()
	Local cRutOrg	:= ""
	
	DbSelectArea("SE1")
	SE1->(DbSetOrder(2))//E1_FILIAL+E1_CLIENTE+E1_LOJA+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO
	If SE1->(dbSeek(xFilial("SE1")+cCliente+cLoja+cPrefixo+cNumDoc ))
		 cRutOrg := SE1->E1_ORIGEM
	EndIf
	SE1->(dbCloseArea())		
	RestArea(aArea)	
	
Return cRutOrg
