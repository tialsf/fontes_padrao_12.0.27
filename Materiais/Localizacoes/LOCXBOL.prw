#include 'protheus.ch'
#include 'parmtype.ch'
#include 'LOCXBOL.ch'
/*Rutina Funciones Gen�rica Factura Electr�nica Bolivia*/
/*/{Protheus.doc} M486XVldBO
//Valida campos obligatorios para Fac. Electr�nica. 
@author Alfredo Medrano
@since 21/04/2020
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
Function M486XVldBO(cValCpo,cCpo)
	Local lRetVld := .T.
	Local cAviso  := ""
	Default cValCpo:= ""
	Default cCpo   := ""
	If cCpo == "F1_TIPNOTA" .Or. cCpo == "F2_TPDOC"  .OR. cCpo == "F1_MODCONS" .OR. cCpo == "F2_MODCONS"  .OR. cCpo == "C5_CODMPAG" .OR. cCpo == "C5_TPDOCSE"
		If Empty(cValCpo)
			cAviso := StrTran(STR0001, '###', RTrim(FWX3Titulo(cCpo))) + " (" + cCpo + ")." //"Es necesario informar en el encabezado el campo ###"  
			lRetVld := .F.
		Else
			If cCpo == "F1_TIPNOTA" .Or. cCpo == "F2_TPDOC" .OR. cCpo == "C5_TPDOCSE" //Tip. Doc. Sector.
				lRetVld := ValidF3I("S008", cValCpo ,1,2)  
			ElseIf  cCpo == "F1_MODCONS" .OR. cCpo == "F2_MODCONS" .OR. cCpo == "C5_CODMPAG" //C�d. Met. Pago.
				lRetVld := ValidF3I("S004", cValCpo,1,2)                                                                                  
			EndIf
			If !lRetVld                                                                               
				cAviso := StrTran(STR0002, '###', RTrim(FWX3Titulo(cCpo))) + cCpo + STR0002 //"El campo ###( //"), no contiene informaci�n v�lida para el tipo de documento."
			EndIf
		EndIf
	EndIF
	If !Empty(cAviso)
		Aviso(STR0004, cAviso, {STR0005}) //"Atenci�n" //"Ok"
	EndIf	
Return lRetVld

/*/{Protheus.doc} VldFacE
//Valida campos que son necesarios para la trasnmision de la Fac. Electr�nica
//utiliza en el valid de los campos F1/F2_LOJA, F1/F2_SERIE y D1/D2_COD
@author Alfredo Medrano
@since 21/04/2020
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
Function  VldFacE()
	Local aArea	:= getArea()
	Local cOrd		:= ""
	Local lRet		:= .T.
	Local nCount  := 0
	Local aError  := {}
	Local cAviso	:= ""
	Local cSerieF  := ""
	Local cProdF  := ""
	Local cCampR := ReadVar() // obtiene nombre campo 
	Local cNumDto  	:=""
	Local cCpo   	:= ""
	Local lCFDUso   := IIf(Alltrim(GetMv("MV_CFDUSO", .T., "1"))<>"0",.T.,.F.)
	
	If FunName() != "MATA410"
		cNumDto := iif(AllTrim(cEspecie) =="NF", '1',iif(AllTrim(cEspecie) =="NCC", '4', iif(AllTrim(cEspecie) =="NDC", '5', '0')) )
	EndIf
	
	If lCFDUso
		cCpo := substr(Alltrim(cCampR),4)
		If cCpo == "F2_LOJA" .OR.  cCpo == "F1_LOJA" .OR.  cCpo == "C5_LOJACLI" .OR.  cCpo == "C5_CLIENTE" 
			cAviso	:= STR0006 +  CHR(10) //"Informaci�n faltante en registro del Cliente."
			cOrd := IIf(cCpo == "F2_LOJA", M->F2_CLIENTE + M->F2_LOJA, IIf(cCpo == "F1_LOJA", M->F1_FORNECE + M->F1_LOJA, IIf(cCpo $  "C5_LOJACLI|C5_CLIENTE", M->C5_CLIENTE + M->C5_LOJACLI, ) ))
		 	dbSelectArea("SA1")
		 	SA1->(DBSETORDER(1)) //A1_FILIAL+A1_COD+A1_LOJA
			If SA1->(dbSeek(xFilial("SA1")+cOrd)) 
		 		If Empty(SA1->A1_TIPDOC) 
		 			aAdd(aError ,{"A1_TIPDOC",RTrim(FWX3Titulo("A1_TIPDOC"))})
		 		EndIf
		 		If Empty(SA1->A1_CGC) 
		 			aAdd(aError ,{"A1_CGC",RTrim(FWX3Titulo("A1_CGC"))}) 
		 		EndIf
			EndIf
		EndIF
		
		If cCpo == "F1_SERIE" .OR. cCpo == "F2_SERIE"
			cAviso	:= STR0007 +  CHR(10) //"Informaci�n faltante en registro del Control de Folios."
			cSerieF := IIf(cCpo == "F1_SERIE", M->F1_SERIE, IIf(cCpo == "F2_SERIE", M->F2_SERIE, ))
		 	dbSelectArea("SFP")
			SFP->(dbSetOrder(5))//FP_FILIAL+FP_FILUSO+FP_SERIE+FP_ESPECIE+FP_PV
			If SFP->(MsSeek(xFilial("SFP") + cFilAnt + cSerieF + cNumDto))
				If Empty(SFP->FP_DOCFIS) 
		 			aAdd(aError ,{"FP_DOCFIS",RTrim(FWX3Titulo("FP_DOCFIS"))}) 
		 		EndIf
		 		If Empty(SFP->FP_PV) 
		 			aAdd(aError ,{"FP_PV",RTrim(FWX3Titulo("FP_FP_PV"))}) 
		 		EndIf
			EndIf
		EndIF
		
		If cCpo == "D1_COD" .OR. cCpo == "D2_COD" .OR. cCpo == "C6_PRODUTO"
			cAviso	:= STR0008 +  CHR(10) //"Informaci�n faltante en registro del Producto."
			cProdF := IIf(cCpo == "D1_COD", M->D1_COD, IIf(cCpo == "D2_COD", M->D2_COD, IIf(cCpo == "C6_PRODUTO", M->C6_PRODUTO, ) ))
		 	dbSelectArea("SB1")
			SB1->(dbSetOrder(1))//B1_FILIAL+B1_COD
			If SB1->(MsSeek(xFilial("SB1") + cProdF))
				If Empty(SB1->B1_PRODSAT) 
		 			aAdd(aError ,{"B1_PRODSAT",RTrim(FWX3Titulo("B1_PRODSAT"))}) // "Informaci�n faltante registro del Producto""
		 		EndIf
			EndIf
		EndIF
		
		If Len(aError) > 0
			For nCount:= 1 to Len(aError)
				cAviso += StrTran(STR0002, '###', RTrim(aError[nCount,2])) + " (" + aError[nCount,1] + ")" + STR0009 +  CHR(10) //El campo ###, no contiene informaci�n.
			Next
			cAviso += STR0010 //"La ausencia de �sta informaci�n impedir� la transici�n del documento."
			Aviso(STR0004, cAviso, {STR0005}) //"Atenci�n" //"Ok"
		EndIf
	EndIf
	RestArea(aArea)	
Return lRet 

/*/{Protheus.doc} LxCposBol
//Funci�n para agregar campos para Bolivia.
@author Alfredo Medrano
@since 21/04/2020
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
Function LxCposBol(aCposNF, cTabla)
Local nPosF := 0
Local cValOrigen := ""
Local lCFDUso   := IIf(Alltrim(GetMv("MV_CFDUSO", .T., "1"))<>"0",.T.,.F.)
If cTabla == "SF2"
	If lCFDUso 
		/*asigna validacion a F2_CLIENTE/F2_LOJA */
		nPosF := ASCAN(aCposNF,{|x| AllTrim(x[2]) == "F2_LOJA"})
		If nPosF > 0
			cValOrigen := ""
			cValOrigen := aCposNF[nPosF, 6]
			If !Empty(cValOrigen)
				cValOrigen += " .AND. " 
			EndIf
			aCposNF[nPosF, 6]:= cValOrigen + "VldFacE()"
		EndIf	
		/*asigna validacion a F2_SERIE*/
		nPosF := ASCAN(aCposNF,{|x| AllTrim(x[2]) == "F2_SERIE"})
		If nPosF > 0
			cValOrigen := ""
			cValOrigen := aCposNF[nPosF, 6]
			If !Empty(cValOrigen)
				cValOrigen += " .AND. " 
			EndIf
			aCposNF[nPosF, 6]:= cValOrigen + "VldFacE()"
		EndIf
	EndIf

	If SF2->(ColumnPos( "F2_MODCONS" )) > 0
		SX3->(MsSeek("F2_MODCONS"))
		cVld := LocX3Valid("F2_MODCONS")
		AAdd(aCposNF,{X3Titulo(),"F2_MODCONS",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,cVld,SX3->X3_USADO,SX3->X3_TIPO,"SF2",SX3->X3_CONTEXT,,,,,,SX3->X3_F3})
	EndIf
	If SF2->(ColumnPos( "F2_IDRGS" )) > 0
		SX3->(MsSeek("F2_IDRGS"))
		cVld := LocX3Valid("F2_IDRGS")
		AAdd(aCposNF,{X3Titulo(),"F2_IDRGS",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,cVld,SX3->X3_USADO,SX3->X3_TIPO,"SF2",SX3->X3_CONTEXT,,,,,,})
	EndIf
	If SF2->(ColumnPos( "F2_TPDOC" )) > 0
		SX3->(MsSeek("F2_TPDOC"))
		cVld := LocX3Valid("F2_TPDOC")
		AAdd(aCposNF,{X3Titulo(),"F2_TPDOC",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,cVld,SX3->X3_USADO,SX3->X3_TIPO,"SF2",SX3->X3_CONTEXT,,,,,,SX3->X3_F3})
	EndIf
ElseIf cTabla == "SF1"
	If lCFDUso
		/*asigna validacion a F1_FORNECE/F1_LOJA*/
		nPosF := ASCAN(aCposNF,{|x| AllTrim(x[2]) == "F1_LOJA"})
		If nPosF > 0
			cValOrigen := ""
			cValOrigen := aCposNF[nPosF, 6]
			If !Empty(cValOrigen)
				cValOrigen += " .AND."
			EndIf
			aCposNF[nPosF, 6]:= cValOrigen + " VldFacE()"
		EndIf
		/*asigna validacion a F1_SERIE*/
		nPosF := ASCAN(aCposNF,{|x| AllTrim(x[2]) == "F1_SERIE"})
		If nPosF > 0
			cValOrigen := ""
			cValOrigen := aCposNF[nPosF, 6]
			If !Empty(cValOrigen)
				cValOrigen += " .AND. " 
			EndIf
			aCposNF[nPosF, 6]:= cValOrigen +" VldFacE()"
		EndIf	
	EndIf	
	If SF1->(ColumnPos( "F1_TIPNOTA" )) > 0
		SX3->(MsSeek("F1_TIPNOTA"))
		cVld := LocX3Valid("F1_TIPNOTA")
		AAdd(aCposNF,{X3Titulo(),"F1_TIPNOTA",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,cVld,SX3->X3_USADO,SX3->X3_TIPO,"SF1",SX3->X3_CONTEXT,,,,,,SX3->X3_F3})
	EndIf
		If SF1->(ColumnPos( "F1_MODCONS" )) > 0
		SX3->(MsSeek("F1_MODCONS"))
		cVld := LocX3Valid("F1_MODCONS")
		AAdd(aCposNF,{X3Titulo(),"F1_MODCONS",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,cVld,SX3->X3_USADO,SX3->X3_TIPO,"SF1",SX3->X3_CONTEXT,,,,,,SX3->X3_F3})
	EndIf
	If SF1->(ColumnPos( "F1_IDRGS" )) > 0
		SX3->(MsSeek("F1_IDRGS"))
		cVld := LocX3Valid("F1_IDRGS")
		AAdd(aCposNF,{X3Titulo(),"F1_IDRGS",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,cVld,SX3->X3_USADO,SX3->X3_TIPO,"SF1",SX3->X3_CONTEXT,,,,,,})
	EndIf
EndIf
Return aCposNF

/*/{Protheus.doc} LxVldBol
//Valida campos para el pa�s Bolivia..
@author Alfredo Medrano
@since 21/04/2020
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
Function LxVldBol(cAliasSF)
Local lRetVld	:= .T.
Local lCFDUso   := IIf(Alltrim(GetMv("MV_CFDUSO", .T., "1"))<>"0",.T.,.F.)
	If lCFDUso
		If cAliasSF == "SF1"
			IIF(!ValRetSat(M->F1_TIPNOTA, "F1_TIPNOTA") .OR. !ValRetSat(M->F1_MODCONS, "F1_MODCONS"), lRetVld:= .F.,)
		ElseIf cAliasSF == "SF2"
			IIF(!ValRetSat(M->F2_TPDOC, "F2_TPDOC")  .OR. !ValRetSat(M->F2_MODCONS, "F2_MODCONS"), lRetVld:= .F.,)
		EndIf
	EndIf
Return lRetVld
/*/{Protheus.doc} LxBoVldDel
//Valida Borrado/ anulaci�n de documentos fiscales
@author Alfredo Medrano
@since 21/04/2020
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
Function LxBoVldDel(cAlias, cAliCampo)
Local lRet := .T.
Local lCFDUso   := IIf(Alltrim(GetMv("MV_CFDUSO", .T., "1"))<>"0",.T.,.F.)
DEFAULT cAliCampo := ""
If lCFDUso
	If !Empty(cAlias) .AND. !Empty(cAliCampo)
	If IIf (cAlias $ "SF2|SF1" , !Empty((cAlias)->(FieldPos(cAliCampo+"_FLFTEX"))), .F.)
			If (cAlias)->&(cAliCampo+"_FLFTEX") $ "1|4|6"
				MsgAlert( STR0011 + (cAlias)->&(cAliCampo+"_SERIE") + (cAlias)->&(cAliCampo+"_DOC") + STR0012 )//"El documento " ## " no puede ser borrado/anulado pues ya fue transmitido. Utilice funcionalidad de Anulaci�n de Factura Electr�nica de la rutina Transmisi�n Electr�nica (MATA486)."
				Return .F.
			ElseIf (cAlias)->&(cAliCampo+"_FLFTEX") $ "7"
				MsgAlert( STR0011 + (cAlias)->&(cAliCampo+"_SERIE") + (cAlias)->&(cAliCampo+"_DOC") + STR0013 )//"El documento " ## " se encuentra en proceso de validaci�n de anulaci�n. Utilice funcionalidad de Monitor de la rutina Transmisi�n Electr�nica."
				Return .F.
			EndIf
		EndIf
	EndIf
EndIf
return lRet

/*/{Protheus.doc} LVlSerBol
//Valida serie para Bolivia.
@author Alfredo Medrano
@since 12/05/2020
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
Function LVlSerBol(cSerieF)
Local aArea	:= getArea()
Local lRet := .T.
Local cAviso := ""
Local cNumDto := '1'
Local lCFDUso   := IIf(Alltrim(GetMv("MV_CFDUSO", .T., "1"))<>"0",.T.,.F.)
	If !Empty(cSerieF) .And. !Empty(cNumDto) .and. lCFDUso
			cAviso	:= STR0007 +  CHR(10) //"Informaci�n faltante en registro del Control de Folios."
		 	dbSelectArea("SFP")
			SFP->(dbSetOrder(5))//FP_FILIAL+FP_FILUSO+FP_SERIE+FP_ESPECIE+FP_PV
			If SFP->(MsSeek(xFilial("SFP") + cFilAnt + cSerieF + cNumDto))
				If Empty(SFP->FP_DOCFIS) 
		 			cAviso +=   StrTran(STR0002, '###', RTrim(FWX3Titulo("FP_DOCFIS"))) + " (FP_DOCFIS)" + STR0009 +  CHR(10) //El campo ###, no contiene informaci�n.
		 			lRet := .F.
		 		EndIf
		 		If Empty(SFP->FP_PV) 
		 			cAviso +=   StrTran(STR0002, '###', RTrim(FWX3Titulo("FP_PV"))) + " (FP_PV)" + STR0009 +  CHR(10) //El campo ###, no contiene informaci�n.
		 			lRet := .F.
		 		EndIf
			EndIf
		EndIF
		If !lRet
			cAviso += STR0010 //"La ausencia de �sta informaci�n impedir� la transici�n del documento."
			Aviso(STR0004, cAviso, {STR0005}) //"Atenci�n" //"Ok"
		EndIf
	RestArea(aArea)	
return lRet