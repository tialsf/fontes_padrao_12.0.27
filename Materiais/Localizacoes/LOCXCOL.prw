#include 'protheus.ch'
#include 'parmtype.ch'
#include 'LOCXCOL.ch'

#Define SAliasHead  4

/*/{Protheus.doc} LxVldEncC
Valida datos del encabezado al ejecutar la acci�n Doc Orig para Notas de Cr�dito para pa�s Colombia.
@type
@author luis.enriquez
@since 12/03/2020
@version 1.0
@param aCfgNF, arreglo, Arreglo de datos del documento
@param cFunName, caracter, Nombre del programa en ejecuci�n
@return lRetVld, falso si se detecto que no existe informado alg�n campo
@example
LxVldEncC(aCfgNF,cFunName) 
@see (links_or_references)
/*/
Function LxVldEncC(aCfgNF,cFunName)
	Local cDato   := ""
	Local cCpoCli := IIf(aCfgNF[SAliasHead] == "SF1","F1_FORNECE","F2_CLIENTE")
	Local cCpoLoja:= IIf(aCfgNF[SAliasHead] == "SF1","F1_LOJA","F2_LOJA")
	Local cCpoTpO := IIf(aCfgNF[SAliasHead] == "SF1","F1_TIPOPE","F2_TIPOPE")
	Local cCliFor := IIf(aCfgNF[SAliasHead] == "SF1",M->F1_FORNECE,M->F2_CLIENTE)
	Local cLoja   := IIf(aCfgNF[SAliasHead] == "SF1",M->F1_LOJA,M->F2_LOJA)
	Local cTipOpe := IIf(aCfgNF[SAliasHead] == "SF1",M->F1_TIPOPE,M->F2_TIPOPE)
	Local cCRLF   := (Chr(13) + Chr(10))
	Local lRetVld := .T.		
	
	If Empty(cCliFor) //Cliente
		cDato += "-" + FWX3Titulo(cCpoCli) + "(" + cCpoCli + ")" + cCRLF
	EndIf
	If Empty(cLoja) //Tienda
		cDato += "-" + FWX3Titulo(cCpoLoja) + "(" + cCpoLoja + ")" + cCRLF
	EndIf
	
	If cFunName == "MATA465N" .And. Empty(cTipOpe) .And. !Empty(GetMV("MV_PROVFE", .F., ""))
		cDato += "-" + FWX3Titulo(cCpoTpO) + "(" + cCpoTpO + ")" + cCRLF
	EndIf
	
	If !Empty(cDato) //Tienda
		MsgAlert(STR0001 + cCRLF + cDato) //"Es necesario informar los siguientes datos en el encabezado:"
		lRetVld := .F.
	EndIf			
Return lRetVld

/*/{Protheus.doc} LxMIVldCO
Funci�n que realiza validaci�n de acuerdo al valor y nombre del campo para pa�s Colombia.
@type
@author luis.enriquez
@since 12/03/2020
@version 1.0
@param cValCpo, caracter, Valor del campo
@param cCpo, caracter, Nombre del campo
@return lRetVld, falso si se detecta que ocurri� algun detalle con la validaci�n del campo.
@example
LxMIVldCO(cTpRel,cCpo)
@see (links_or_references)
/*/
Function LxMIVldCO(cValCpo,cCpo)
	Local lRetVld := .T.
	Local cProvFE := SuperGetMV("MV_PROVFE", .F., "")
	Local cTpDoc  := ""
	Local cAviso  := ""
	
	Default cValCpo:= ""
	Default cCpo   := ""
	
	If !Empty(cProvFE) 
		If cCpo == "F1_TIPOPE" .Or. cCpo == "F2_TIPOPE"
			If Empty(cValCpo)
				cAviso := StrTran(STR0002, '###', RTrim(FWX3Titulo(cCpo))) + " (" + cCpo + ")." //"Es necesario informar en el encabezado el campo ###"  
				lRetVld := .F.
			Else
				cTpDoc := AllTrim(ObtColSAT("S017",Alltrim(cValCpo),1,4,85,3))
				If !Empty(cTpDoc) .And. !(Alltrim(cEspecie) == Alltrim(cTpDoc))
					cAviso := StrTran(STR0003, '###', RTrim(FWX3Titulo(cCpo))) + cCpo + STR0004 //"El campo ###( //"), no contiene un tipo de operaci�n v�lido para el tipo de documento."
					lRetVld := .F.
				EndIf		
			EndIf
		EndIf
	EndIf
	If !Empty(cAviso)
		Aviso(STR0005, cAviso, {STR0006}) //"Atenci�n" //"OK"
	EndIf	
Return lRetVld

/*/{Protheus.doc} LxCposCol
Funcion utilizada para agregar campos al encabezado de
Notas Fiscales para Colombia.
@type Function
@author luis.enriquez
@since 08/08/2019
@version 1.1
@param aCposNF, Array, Array con campos del encabezado de NF
@param cFunName, Character, Codigo de rutina
@param cTablaEnc, Character, Alias del encabezado de Notas Fiscales
@example LxCposCol(aCposNF, cFunName, cTablaEnc)
@return aCposNF, Array, Campos para el Encabezado de Notas Fiscales.
@see (links_or_references)
/*/
Function LxCposCol(aCposNF, cFunName, cTablaEnc)
	
	Local cProvFE := SuperGetMV("MV_PROVFE",,"")
    Local cVld    := ""
    Local aSX3    := {}
    
    If cTablaEnc == "SF2"
		If SF2->(ColumnPos( "F2_CODMUN" )) > 0
			aSX3 := LxSX3Cache("F2_CODMUN")
			cVld := LocX3Valid("F2_CODMUN")
			AAdd(aCposNF,{X3Titulo(),"F2_CODMUN",aSX3[1],aSX3[2],aSX3[3],cVld,aSX3[5],aSX3[6],"SF2",aSX3[7],,,,,,aSX3[8]})
		EndIf
		If SF2->(ColumnPos( "F2_TPACTIV" )) > 0
			aSX3 := LxSX3Cache("F2_TPACTIV")
			cVld := LocX3Valid("F2_TPACTIV")
			AAdd(aCposNF,{X3Titulo(),"F2_TPACTIV",aSX3[1],aSX3[2],aSX3[3],cVld,aSX3[5],aSX3[6],"SF2",aSX3[7],,,,,,})
		EndIf
		IF SF2->(ColumnPos( "F2_TRMPAC" )) > 0 .AND. ( cFunName $ "MATA467N|MATA462N|" )
			SX3->(MsSeek("F2_TRMPAC"))
			AAdd(aCposNF,{X3Titulo(),"F2_TRMPAC",,,,,,,"SF2",,,,,,,,,,,.T.,StrTokArr(Alltrim(X3CBox()),';'),{|x| x:nAt}})
		EndIf
		IF SF2->(ColumnPos( "F2_TIPOPE" )) > 0 .AND. ( cFunName $ "MATA467N|MATA462N|MATA465N" ) .And. !Empty(cProvFE)
			aSX3 := LxSX3Cache("F2_TIPOPE")
			AAdd(aCposNF,{X3Titulo(),"F2_TIPOPE",aSX3[1],aSX3[2],aSX3[3],aSX3[4],aSX3[5],aSX3[6],"SF2",aSX3[7],,,,,,aSX3[8]})
		EndIf
	Else
		IF SF1->(Fieldpos("F1_CODMUN")) > 0
			SX3->(MsSeek("F1_CODMUN"))
			cVld := LocX3Valid("F1_CODMUN")
			AAdd(aCposNF,{X3Titulo(),"F1_CODMUN",	SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL, cVld, SX3->X3_USADO,SX3->X3_TIPO,"SF1",SX3->X3_CONTEXT,,,,,,SX3->X3_F3})
		EndIF
		If SF1->(Fieldpos("F1_TPACTIV")) > 0 .AND. ( cFunName$"MATA465N|MATA101N|MATA466N" )
			SX3->(MsSeek("F1_TPACTIV"))
			cVld := LocX3Valid("F1_TPACTIV")
			AAdd(aCposNF,{X3Titulo(),"F1_TPACTIV",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,cVld,SX3->X3_USADO,SX3->X3_TIPO,"SF1",SX3->X3_CONTEXT,,,,,,SX3->X3_F3})
		EndIf
		IF SF1->(Fieldpos("F1_TRMPAC")) > 0 .AND. ( cFunName $ "MATA101N" )
			SX3->(MsSeek("F1_TRMPAC"))
			AAdd(aCposNF,{X3Titulo(),"F1_TRMPAC",,,,,,,"SF1",,,,,,,,,,,.T.,StrTokArr(ALLTRIM(x3cbox()),';'),{|x| x:nAt}})
		EndIf
	EndIf
	
Return aCposNF

/*/{Protheus.doc} LxSX3Cache
Funci�n para obtener datos del SX3 para campos usando la funci�n GetSX3Cache
@type
@author luis.enriquez
@since 18/03/2020
@version 1.0
@param cCampo, caracter, Nombre del campo.
@return aSX3Cpos, array, Arreglo con contenido de la tabla SX3 para el campo.
@see (links_or_references)
/*/
Function LxSX3Cache(cCampo)
	Local aSX3Cpos := {}
	
	Default cCampo := "" 
	
	If !Empty(cCampo)
		aSX3Cpos := {GetSX3Cache(cCampo,"X3_PICTURE"), ; //1
		GetSX3Cache(cCampo,"X3_TAMANHO"), ; //2
		GetSX3Cache(cCampo,"X3_DECIMAL"), ; //3
		GetSX3Cache(cCampo,"X3_VALID"), ;   //4
		GetSX3Cache(cCampo,"X3_USADO"), ;   //5
		GetSX3Cache(cCampo,"X3_TIPO"), ;    //6
		GetSX3Cache(cCampo,"X3_CONTEXT"), ; //7
		GetSX3Cache(cCampo,"X3_F3")}        //8
	EndIf
Return aSX3Cpos

/*/{Protheus.doc} LxVldFact
Funci�n para validar el borrado TSS de la factura para el pais Colombia
@type
@author eduardo.manriquez
@since 24/06/2020
@version 1.0
@param cAlias, caracter, Alias de la tabla.
@see (links_or_references)
/*/
Function LxVldFact(cAlias)
	Default cAlias := ""
	
	If cAlias == "SF2" .And. SF2->(FieldPos("F2_FLFTEX"))>0
			If Val(SF2->F2_FLFTEX) == 6 .OR. !Empty(SF2->F2_FLFTEX)
				//"La factura Serie y No. "###"no puede ser borrada pues ya fue procesada por la Transmisi�n Electr�nica. Utilice la opci�n Anular"  ###"�TSS: Transmisi�n Electr�nica !"
				MsgAlert( STR0007 + SF2->F2_SERIE + SF2->F2_DOC + STR0008 , STR0010 )
				Return .F.
			EndIf
		ElseIf cAlias <> "SF2" .And. SF1->(FieldPos("F1_FLFTEX"))>0
			If Val(SF1->F1_FLFTEX) == 6 .OR. !Empty(SF1->F1_FLFTEX)
				//"La factura Serie y No. "###"no puede ser borrada pues ya fue procesada por la Transmisi�n Electr�nica. Utilice la opci�n Anular"  ###"�TSS: Transmisi�n Electr�nica !"
				MsgAlert( STR0007 + SF1->F1_SERIE + SF1->F1_DOC + STR0009 , STR0010 )
				Return .F.
			EndIf
		Endif
Return

/*/{Protheus.doc} M030AltCV0
Funcion utilizada en la rutina de Clientes para actualizar
o incluir valores en tabla CV0 (MATN030).
@type Function
@author Marco Augusto Gonzalez Rivera	
@since 30/07/2020
@version 1.0
@param lIncReg, L�gico, Indica si es inclusi�n o modificaci�n.
/*/
Function M030AltCV0(lIncReg)
	
	Local aArea		:= GetArea()
	Local cAliasCV0	:= ""
	Local nRecnoCV0	:= 0
	
	If lIncReg
		DBSelectArea("CV0")
		cItm := GetSxENum( "CV0", "CV0_ITEM" )
		Begin Transaction
			RecLock("CV0", .T.)
			CV0->CV0_FILIAL 	:= xFilial("CV0")
			CV0->CV0_CODIGO 	:= IIf(AllTrim(M->A1_TIPDOC) == "31", M->A1_CGC, M->A1_PFISICA)
			CV0->CV0_PLANO  	:=	"01"
			CV0->CV0_ITEM		:=	cItm
			CV0->CV0_CLASSE 	:= "2"
			CV0->CV0_NORMAL 	:= "1"
			CV0->CV0_ENTSUP 	:= "13"
			CV0->CV0_DTIEXI 	:= dDatabase
			CV0->CV0_TIPO00 	:= "01"
			CV0->CV0_TIPO01 	:= M->A1_TIPDOC
			CV0->CV0_DESC		:= M->A1_NOME
			CV0->CV0_COD		:= M->A1_COD
			CV0->CV0_LOJA		:= M->A1_LOJA
			CV0->(MsUnlock())
			ConfirmSX8()
		End Transaction
	Else
		cAliasCV0 := GetNextAlias()
		
		BeginSQL Alias cAliasCV0
			SELECT CV0.R_E_C_N_O_
			FROM %table:CV0% CV0
			WHERE CV0.CV0_FILIAL = %xfilial:CV0% AND
				CV0.CV0_COD = %Exp:M->A1_COD% AND
				CV0.CV0_LOJA = %Exp:M->A1_LOJA% AND
				CV0.CV0_TIPO00 = '01' AND
				CV0.%notDel%
		EndSQL
		
		nRecnoCV0 := (cAliasCV0)->(R_E_C_N_O_)
		
		If nRecnoCV0 > 0
			DBSelectArea("CV0")
			CV0->(DBGoTo(nRecnoCV0))
			RecLock("CV0", .F.)
			CV0->CV0_DESC	:= M->A1_NOME
			CV0->CV0_COD	:= M->A1_COD
			CV0->CV0_LOJA	:= M->A1_LOJA
			CV0->CV0_CODIGO := IIf(AllTrim(M->A1_TIPDOC) == "31", M->A1_CGC, M->A1_PFISICA)
			CV0->CV0_TIPO01 := M->A1_TIPDOC
			CV0->(MsUnlock())
		EndIf
		
		(cAliasCV0)->(DBCloseArea())
		
	EndIf
	
	RestArea(aArea)
	
Return 