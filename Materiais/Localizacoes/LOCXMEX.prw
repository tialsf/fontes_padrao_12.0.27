#include 'protheus.ch'
#include 'LOCXMEX.CH'

/*/{Protheus.doc} LXMexAcc
Agrega acciones para rutinas de generaci�n de notas fiscales (LOCXNF) para el pa�s M�xico
@type
@author luis.enriquez
@since 27/02/2020
@version 1.0
@param/@return aRotina, arreglo, Arregl� con acciones
@example
LXMexAcc(@aRotina) 
@see (links_or_references)
/*/
Function LXMexAcc(aRotina)
	If (cFunName$ "MATA101N") 
		If (SF1->(FieldPos("F1_RUTDOC")) >0) 
			aAdd(aRotina,{OemToAnsi(STR0001),"MT459VDOC(SF1->F1_RUTDOC)",0,5,0,NIL}) //"Visualizar PDF/XML"
	 	EndIf
	 	aAdd(aRotina,{OemToAnsi(STR0002),"LXList69B()",0,5,0,NIL}) //"Cargar Listado 69-B"
	EndIf
Return Nil

/*/{Protheus.doc} LeerXML
Lee el archivo XML para obtener la informacion para el pa�s M�xico
@type
@author laura.medina
@since 02/07/2015
@version 1.0
@example
LeerXML() 
@see (links_or_references)
/*/
Function LeerXML()
	Local _cErrMsg := ""
	Local _cWrnMsg := ""
	Local _cCert   := ""	// Certificado
	Local _dFchTim := ""	// Fecha timbrado
	Local lRet     := .T.	// Nodo invalido
	Local nCar 	 := 0
	Local aCarEsp  := {}
	Local nPosFTim := 0
	Local nPosRDoc	:= 0
	Local cDato := ""
	Local cCRLF := (chr(13)+chr(10))
	Local cFunName	:= AllTrim(FunName())

	_cRFC 		:= ""	// RFC Proveedor
	_cRFCRec	:= ""   // RFC Receptor (SM0)
	cPathXML 	:= ""   // Path XML (local)
 
	_oXml := XmlParserFile( cPathSrv, "_", @_cErrMsg, @_cWrnMsg )
	
	//Uso de Caracteres especiales amperson en RFC
	Aadd(aCarEsp,{"&","&amp;"})
	Aadd(aCarEsp,{"&","&#38;"})
	
	If  (_oXml == NIL ) .Or. (!Empty(_cErrMsg) .or. !Empty(_cWrnMsg) )
		MsgStop(STR0017) //"El documento XML es invalido, verifique!" 
		Return(.F.)
	Else
		If XmlChildEx(_oXml, "_CFDI_COMPROBANTE") <> Nil
			If XmlChildEx(_oXml:_CFDI_COMPROBANTE, "_CFDI_COMPLEMENTO") <> Nil
				If 	XmlChildEx(_oXml:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO, "_TFD_TIMBREFISCALDIGITAL") <> Nil				
					If XmlChildEx(_oXml:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO:_TFD_TIMBREFISCALDIGITAL, "_UUID") <> Nil					
						_cUUID := _oXml:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO:_TFD_TIMBREFISCALDIGITAL:_UUID:TEXT   //Obtener UUID
					Else
						cDato += STR0018 + cCRLF //"-Elemento UUID (nodo tfd:TimbreFiscalDigital)"
					EndIf
					If XmlChildEx(_oXml:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO:_TFD_TIMBREFISCALDIGITAL, "_NOCERTIFICADOSAT") <> Nil				
						_cCert := _oXML:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO:_TFD_TIMBREFISCALDIGITAL:_NOCERTIFICADOSAT:TEXT  //Obtener Certificado
					Else
						cDato += STR0019 + cCRLF //"-Elemento N�mero de Certificado SAT (nodo tfd:TimbreFiscalDigital)"
					EndIf
					If XmlChildEx(_oXml:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO:_TFD_TIMBREFISCALDIGITAL, "_FECHATIMBRADO") <> Nil					
						_dFchTim:= _oXml:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO:_TFD_TIMBREFISCALDIGITAL:_FECHATIMBRADO:TEXT     //Obtener Fecha Timbrado
						_dFchTim:= IIF(!Empty(_dFchTim),Ctod(substr(_dFchTim,9,2)+'/'+substr(_dFchTim,6,2)+'/'+substr(_dFchTim,3,2)),CTOD("//"))
					Else
						cDato += STR0020 + cCRLF //"-Elemento Fecha de Timbrado (nodo tfd:TimbreFiscalDigital)"
					EndIf
				Else
					cDato += STR0021 + cCRLF //"-Nodo Timbre Fiscal (tfd:TimbreFiscalDigital)"
				EndIf
			Else
				cDato += STR0034 + cCRLF //"-Nodo Complemento (cfdi:Complemento)"
			EndIf
			If XmlChildEx(_oXml:_CFDI_COMPROBANTE, "_CFDI_EMISOR") <> Nil				
				If XmlChildEx(_oXml:_CFDI_COMPROBANTE:_CFDI_EMISOR, "_RFC") <> Nil					
					_cRFC := _oXml:_CFDI_COMPROBANTE:_CFDI_EMISOR:_RFC:TEXT //RFC Emisor
					For nCar := 1 To Len(aCarEsp)
	            		_cRFC:= StrTran(_cRFC, aCarEsp[nCar,2] , aCarEsp[nCar,1] )
	        		Next nCar
	        	Else
					cDato += STR0022 + cCRLF //"-Elemento RFC del Emisor (nodo cfdi:Emisor)"
				EndIf
			Else
				cDato += STR0023 + cCRLF //"-Nodo de datos del Emisor (cfdi:Emisor)"
			EndIf
			If XmlChildEx(_oXml:_CFDI_COMPROBANTE, "_CFDI_RECEPTOR") <> Nil				
				If XmlChildEx(_oXml:_CFDI_COMPROBANTE:_CFDI_RECEPTOR, "_RFC") <> Nil //RFC Receptor
					_cRFCRec := _oXml:_CFDI_COMPROBANTE:_CFDI_RECEPTOR:_RFC:TEXT
					For nCar := 1 To Len(aCarEsp)
	            		_cRFCRec:= StrTran(_cRFCRec, aCarEsp[nCar,2] , aCarEsp[nCar,1] )
	        		Next nCar
	        	Else
					cDato += STR0024 + cCRLF //"-Elemento RFC del Receptor (nodo cfdi:Receptor)"        		
				EndIf
			Else
				cDato += STR0025 + cCRLF //"-Nodo de datos del Receptor (cfdi:Receptor)"
			EndIf
		Else
			cDato += STR0026 + cCRLF //"-Nodo Comprobante (cfdi:Comprobante)"
		EndIf
	EndIf
	
	IIf(Empty(_cRFC), cDato += STR0033 + cCRLF,)    //"-El elemento RFC del nodo Emisor se encuentra vac�o."
	IIf(Empty(_cRFCRec), cDato += STR0050 + cCRLF,) //"-El elemento RFC del nodo Receptor se encuentra vac�o."
	IIf(Empty(_cUUID), cDato += STR0051 + cCRLF,)   //"-El Elemento UUID del nodo TimbreFiscalDigital se encuentra vac�o."
	IIf(Empty(_dFchTim), cDato += STR0052 + cCRLF,) //"-El Elemento Timbre Fiscal del nodo TimbreFiscalDigital se encuentra vac�o."
	IIf(Empty(_cCert), cDato += STR0053 + cCRLF,)   //"-El Elemento No. Certificado SAT del nodo TimbreFiscalDigital se encuentra vac�o."

	If Empty(cDato)
		If cFunName == "MATA447"
			M->RSE_UUID   	:= _cUUID
			nPosFTim := aScan(aHeader, { |x,y| x[2] == "RSE_FECTIM" })
			nPosRDoc := aScan(aHeader, { |x,y| x[2] == "RSE_RUTDOC" })
			If nPosFTim > 0
				aCols[n,nPosFTim] := _dFchTim
			EndIf
			If nPosRDoc > 0
				aCols[n,nPosRDoc] := _cArq
			EndIf
		ElseIf cFunName $ "MATA466N"
			M->F2_UUID   	:= _cUUID
			M->F2_FECTIMB	:= _dFchTim
			M->F2_TIMBRE	:= _cCert
			cPathXML        := SUBSTR(cPathSrv,1,RAT(".XML",UPPER(cPathSrv))-1)
		Else
			M->F1_UUID   	:= _cUUID
			M->F1_FECTIMB	:= _dFchTim
			M->F1_TIMBRE	:= _cCert
			cPathXML        := SUBSTR(cPathSrv,1,RAT(".XML",UPPER(cPathSrv))-1)
		EndIf
	Else
		MsgStop(STR0027 + cCRLF + cDato) //"El XML no tiene la estructura necesaria para obtener los datos:"
		lRet := .F.
	EndIf			
Return(lRet)

/*/{Protheus.doc} VldUUID
Validaciones del campo de UUID, en el modulo de Factura de entrada.
@type
@author laura.medina
@since 01/07/2015
@version 1.0
@example
LeerXML() 
@see (links_or_references)
/*/
Function VldUUID()
	Local _aArea  	:= GetArea()
	Local lRet		:= .T.
	Local cProCli 	:= ""
	Local cCGC		:= ""
	Local cFunName  := AllTrim(funname())
	Local aDatosSM0 := FWSM0Util():GetSM0Data( cEmpAnt, cFilAnt , { "M0_CGC"} )
	Local cFilMB0   := xFilial("MB0")
	Local cRFCAux   := ""
	Local nTamCGC   := ""
	Local cAviso    := ""
	Local cCRLF     := (chr(13)+chr(10))
	Local lM466N	:= (cFunName == "MATA466N" .And. nNFTipo == 7)
	Local cTabSF	:= IIf(lM466N, "SF2", "SF1")
	Local cAliSF	:= IIf(lM466N, "F2", "F1")
	Local nOrderSF	:= IIf(lM466N, 15, 9) //F2_FILIAL + F2_UUID //F1_FILIAL + F1_UUID
	Local lPedim	:= (cFunName == "MATA447")

	If IIf(lPedim, .F., !Empty(Replace(M->&(cAliSF+"_UUID"),"-",""))) //NF generada desde pedimentos no valida XML
		If Empty(Replace(_cUUID,"-",""))
			MsgStop(STR0056)//"�Archivo no seleccionado!"
			Return .F.
		ElseIf Replace(M->&(cAliSF+"_UUID"),"-","") <> Replace(_cUUID,"-","")
			_cUUID := ""
			MsgStop(STR0057)//"El UUID del documento seleccionado no corresponde con el UUID informado, seleccione el archivo nuevamente."
			Return .F.
		EndIf
		If !ExisteAlias("MB0") .Or. !ChkVazio("MB0",.F.)
			MsgAlert(STR0049) //"No existe la tabla Listado 69-B (MB0) o no contiene registros, ejecute la acci�n Cargar Listado 69-B desde Otras acciones de la rutina de Facturas de Entrada (MATA101N)"
			Return .F.
		EndIf
		dbSelectArea(cTabSF)
		(cTabSF)->(dbSetOrder(nOrderSF))
		If (cTabSF)->(DbSeek(xFilial(cTabSF) + M->&(cAliSF+"_UUID"))) //verifica que la Pre-Factura no exista en la tabla SF1
			MsgStop(STR0028 + Alltrim((cTabSF)->&(cAliSF+"_DOC")) + " " + Alltrim((cTabSF)->&(cAliSF+"_SERIE")) + ", " + STR0029 + ": " + Alltrim(SA2->A2_COD) + " " + Alltrim(SA2->A2_LOJA)) //"El Folio Fiscal ya fue utilizado para la factura: " //"Proveedor"
			lRet := .F.
		EndIf
		
		If nNFTipo != 12
			cCGC	:= Alltrim(SA2->A2_CGC)
			cProCli := STR0029 + " " + Alltrim(SA2->A2_COD) + "-" + Alltrim(SA2->A2_LOJA) + " (" + cCGC + ")" //"Proveedor"
		Else
			cCGC	:= Alltrim(SA1->A1_CGC)
			cProCli := STR0030 + " " + Alltrim(SA1->A1_COD) + "-" + Alltrim(SA1->A1_LOJA)+ " (" + cCGC + ")" //"Cliente"
		EndIf
		If !Empty(_cRFCRec)
			If lRet .And. !(Alltrim(aDatosSM0[1][2]) == Alltrim(_cRFCRec))
				MsgStop(StrTran( STR0031, '###', Alltrim(aDatosSM0[1][2]) ) + Alltrim(_cRFCRec) + ")") //"El RFC de la empresa (###) no coincide con el RFC del Receptor del documento XML ("
				lRet	:= .F.
			EndIf
		EndIf
		If !Empty(_cRFC)
			If lRet .And. !(cCGC == Alltrim(_cRFC))
				MsgStop(StrTran( STR0032, '###', cCGC ) + Alltrim(_cRFC) + ")") //"El RFC del Proveedor (###) no coincide con el RFC del Receptor del documento XML ("
				lRet	:= .F.
			EndIf
			If lRet
				nTamCGC   := TamSX3("MB0_CGC")[1]
				cRFCAux := Padr(_cRFC,nTamCGC," ")
				dbSelectArea("MB0")
				MB0->(dbSetOrder(1)) //MB0_FILIAL + MB0_CGC
				If MB0->(DbSeek(cFilMB0 + cRFCAux))
					Do While MB0->(!Eof()) .And. MB0->MB0_FILIAL + MB0->MB0_CGC == cFilMB0 + cRFCAux
						cAviso += STR0036 + Upper(Alltrim(MB0->MB0_STATUS)) + cCRLF + ; //"Situaci�n: "
						   	      STR0037 + IIf(!Empty(MB0->MB0_FECPRE),Dtoc(MB0->MB0_FECPRE),"") + ;     //" Fec. Pres.: "
						   	      STR0038 + IIf(!Empty(MB0->MB0_FECDES),Dtoc(MB0->MB0_FECDES),"") + ;     //" Fec. Desv.: "
						   	      STR0039 + IIf(!Empty(MB0->MB0_FECDEF),Dtoc(MB0->MB0_FECDEF),"") + ;     //" Fec. Def.: "
						   	      STR0040 + IIf(!Empty(MB0->MB0_FECSFA),Dtoc(MB0->MB0_FECSFA),"") + cCRLF //" Fec. Sent. Fav.: "
						MB0->(DbSkip())
					EndDo
					lRet := MsgYesNo(StrTran( STR0054, '###', _cRFC ) + cCRLF + cAviso + cCRLF + STR0035) //"El RFC del Emisor (###) existe en el listado de contribuyentes que desvirtuaron la presunci�n de inexistencia de operaciones ante el SAT: " //"�Desea continuar?"
				EndIf
			EndIf
		EndIf		
	EndIf
	If IIf(lPedim, .F., lRet .And. (Empty(Replace(M->&(cAliSF+"_UUID"),"-",""))))
		M->&(cAliSF + "_FECTIMB") := CTOD("//")
		M->&(cAliSF + "_TIMBRE")  := SPACE(TAMSX3(cAliSF+"_TIMBRE")[1])
		cPathXML := SPACE(TAMSX3(cAliSF+"_RUTDOC")[1])
	EndIf		
	RestArea( _aArea )
Return lRet

/*/{Protheus.doc} LXList69B
Asistente para proceso de carga de la tabla Listado 69-B (MB0)
@type
@author luis.enriquez
@since 03/03/2020
@version 1.0
@example
LXList69B()
@see (links_or_references)
/*/
Function LXList69B()
	Local aSays := {}
	Local aButtons := {}
	Local cPerg    := "M10169B"
	
	Private cRutaCSV := ""
	Private cArchivo := "Listado69B.csv"
	
	If Pergunte(cPerg,.F.)
		cRutaCSV := MV_PAR01
	EndIf
	
	If ExisteAlias("MB0")
		AADD(aSays,OemToAnsi(STR0003)) //"Esta funci�n tiene como objetivo cargar el listado de contribuyentes que desvirtuaron"
		AADD(aSays,OemToAnsi(STR0004)) //"la presunci�n de inexistencia de operaciones ante el SAT, a trav�s de la emisi�n de "
		AADD(aSays,OemToAnsi(STR0005)) //"facturas  o comprobantes fiscales. (Art�culo 69-B del C�digo Fiscal de la Federaci�n)"
		AADD(aSays,OemToAnsi(""))
		AADD(aSays,OemToAnsi(STR0006)) //"Importante: Cada que se ejecute el proceso se actualizara el listado por completo de"
		AADD(aSays,OemToAnsi(STR0007 + cArchivo)) //"acuerdo al contenido del archivo "
		AADD(aSays,OemToAnsi(""))
		AADD(aSays,OemToAnsi(STR0008 + cRutaCSV) ) //"Ruta del archivo: "
		
		AADD(aButtons, { 5,.T.,{|| Pergunte(cPerg,.T. ) } } )
		AADD(aButtons, { 1,.T.,{|o| nOpca := 1,If(LXCarCSV(),FechaBatch(),nOpca:=0) }} )
		AADD(aButtons, { 2,.T.,{|o| FechaBatch() }} )
		
		FormBatch(STR0009, aSays, aButtons) //"Carga del Listado 69-B"
	Else
		MsgAlert(STR0055) //"No existe creada la tabla Listado 69-B (MB0).
		Return .F.	
	EndIf
Return Nil

/*/{Protheus.doc} LXCarCSV
Proceso de descarga de archivo Listado69B.csv para llenado de la tabla Listado 69-B (MB0)
@type
@author luis.enriquez
@since 03/03/2020
@version 1.0
@example
LXCarCSV()
@see (links_or_references)
/*/
Function LXCarCSV()
	Local nOpc := 0
	Local lRet := .T.
	Local cRutaSMR := &("GetClientDir()")
	Local cRutina := "DescargaCSV.exe "
	Local cURL    := GetMV("MV_WSRTSS",.F.,"http://omawww.sat.gob.mx/cifras_sat/Documents/Listado_Completo_69-B.csv")
	Local cPos    := GetMV("MV_IDCBAJA",.F.,"")	
	Local cPath   := ""
	Local cParam  := ""
	Local cQuery  := ""
	Local cCRLF   := (chr(13)+chr(10))
	
	Private aPos := {}
	
    cRutaCSV := MV_PAR01 
    
    If !Empty(cPos)
		aPos := StrTokArr(cPos, "|")
    EndIf
	If Len(aPos) < 6
	 	//"No se tienen configuradas todas las posiciones para los datos a actualizar a partir del archivo Listado69B.cvs (MV_IDCBAJA):"
	 	//"-RFC"
	 	//"-Situaci�n"
	 	//"-Fecha Publicaci�n de Presunto"
	 	//"-Fecha Publicaci�n de Desvirtuado"
	 	//"-Fecha Publicaci�n de Definitivo"
	 	//"-Fecha Publicaci�n de Sentencia Favorable"
		MsgAlert(STR0042 + cCRLF + STR0043 + cCRLF + STR0044 + cCRLF + STR0045 + cCRLF + STR0046 + cCRLF + STR0047 + cCRLF + STR0048) 
		Return .F.		
	EndIf
    If !Empty(cRutaCSV)
    	cParam := Trim(cURL) + " " + cRutaCSV + " " + cArchivo
    
	    cPath := Alltrim(cRutaCSV) + Alltrim(cArchivo)
		
		If ChkVazio("MB0",.F.)
			lRet := MsgYesNo(STR0010) //"Existe informaci�n en el listado 69-B. �Desea continuar para cargar nuevamente?"
		EndIf
		If lRet		
			If File( cRutaSMR + cRutina ) //Ejecutable	
				If !Empty(cURL)
					Processa( {|| nOpc := WAITRUN( cRUTASMR + cRutina + cParam, 0 )},STR0011, , .T. ) //"Descargando el Listado 69-B..."
				Else
					MsgAlert(STR0041) //"No se tiene configurada la URL para descarga del archivo Listado69b.csv (MV_WSRTSS)."
					Return .F.
				EndIf
			EndIf
			If File(Alltrim(cPath)) //Archivo .csv		
				cQuery := "DELETE FROM " + RetSqlName("MB0")
				TcSqlExec(cQuery)		
				Processa( {|| LXGravaMB0(cPath)},STR0012, , .T. ) //"Actualizando el Listado 69-B..."
			Else
				MsgAlert(STR0013 + cPath) //"No se encontr� el archivo: "
			EndIf
		Else
			Return lRet
		EndIf
	Else
		MsgAlert(STR0014) //"No se indic� el par�metro con la ruta que contiene el archivo .CSV" 
    EndIf
Return lRet

/*/{Protheus.doc} LXGravaMB0
Llenado de la tabla Listado 69-B (MB0) leyendo l�neas del archivo Listado69B.csv
@type
@author luis.enriquez
@since 03/03/2020
@version 1.0
@param cFile, caracter, Ruta donde se localiza el archivo Listado69B.csv
@example
LXGravaMB0(cPath)
@see (links_or_references)
/*/
Function LXGravaMB0(cFile)
	Local nHandle  := 0
	Local nFor     := 0
	Local nX       := 0
	Local cLinea   := ""
	Local aLinea   := {}
	Local cSepara  := ","
	Local cFilMB0  := xFilial("MB0")
	Local cSitua   := ""
	Local nCarIni  := 0
	Local nCarFin  := 0
	Local nTamSta  := TamSX3("MB0_STATUS")[1]
	Local lInserta := .T.
	Local nA       := 0
	Local nTotCar  := 0   
	Local nResid   := 0
	
	nHandle := FT_FUse(cFile)
	If nHandle != -1
		FT_FGoTop()
		nFor := FT_FLastRec()
		ProcRegua(nFor)
		
		While !FT_FEOF()			
			IncProc(Str(nX))
			aLinea := {}
			cSitua := ""
			nTotCar := 0
			lInserta := .T.
			lContinua := .T.
			nResid := 0
			
			cLinea := cLinea + FT_FREADLN()
			
			For nA := 1 To Len(cLinea)
				If SubStr(cLinea, nA, 1) == '"'
					nTotCar++
				EndIf
			Next nA	
			
		    nResid := nTotCar/2
		    nResid := nResid - Int(nResid)
		    If nResid > 0
		    	lInserta := .F.		    	
		    EndIf
				
			If lInserta		
				//Tratamiento comilla
				cLinea := StrTran( cLinea, '""', '' )
				nCarIni := At('"',cLinea)
				If nCarIni > 0
					cLinAux := SubStr(cLinea,nCarIni+1,Len(cLinea))
					nCarFin := At('"',cLinAux)				
					cLinAux := SubStr(cLinea,nCarIni,nCarFin)
					cLinAux2 := cLinAux
					cLinAux := StrTran( cLinAux, ',', '!#' )
					cLinea := StrTran( cLinea, cLinAux2, cLinAux )
				EndIf
				
				aLinea := Separa(cLinea,cSepara)
				
				If !(Alltrim(aLinea[Val(aPos[2])]) $ "Definitivo|Desvirtuado|Presunto|Sentencia Favorable|")
					lContinua := .F.
				EndIf	
	
				If  Empty(aLinea) .Or. At("XXXXXXXXXXXX",cLinea) > 0 .Or. !lContinua
					cLinea := ""
					FT_FSKIP() 
					Loop
				EndIf 
			
				RecLock('MB0',.T.) 
					MB0->MB0_FILIAL := cFilMB0				 				
					MB0->MB0_CGC    := aLinea[Val(aPos[1])] //RFC	
					MB0->MB0_STATUS := Substr(aLinea[Val(aPos[2])],1,nTamSta) //Situaci�n	
					MB0->MB0_FECPRE := Ctod(aLinea[Val(aPos[3])]) //Fec. publicaci�n Presunto SAT
					MB0->MB0_FECDES := Ctod(aLinea[Val(aPos[4])]) //Fec. publicaci�n Desvirtuado SAT
					MB0->MB0_FECDEF := Ctod(aLinea[Val(aPos[5])]) //Fec. publicaci�n Definitivo SAT
					MB0->MB0_FECSFA := Ctod(aLinea[Val(aPos[6])]) //Fec. publicaci�n Sentencia Favorable SAT
				MB0->(MsUnLock())
				cLinea := ""
				nX++
			EndIf	
			FT_FSKIP()
		EndDo
		FT_FUSE()
	Else
		MsgAlert(STR0015 + cFile) //"No se puede leer el archivo "
	EndIf
	
	APMSGINFO(STR0016 + Str(nX)) //"Registros insertados:"
Return

/*/{Protheus.doc} fCposNf2
Funcion utilizada para agregar campos al encabezado de
Notas Fiscales para M�xico.
@type Function
@author luis.enriquez
@since 05/03/2020
@version 1.1
@param aCposNF, Array, Array con campos del encabezado de NF
@param cFunName, Character, Codigo de rutina
@param cTablaEnc, Character, Alias del encabezado de Notas Fiscales
@example fCposNf2(aCposNF, cFunName, cTablaEnc)
@return aCposNF, Array, Campos para el Encabezado de Notas Fiscales.
@see (links_or_references)
/*/
Function fCposNf2(aCposNF, cFunName, cTablaEnc)
Local cSFx := ""
Local cSFT := ""
Local cCFDUso := Alltrim(GetMv("MV_CFDUSO", .T., "1"))
	If OAPP:CMODNAME $ "SIGACOM|SIGAEST" .And. cFunName $ ("MATA101N")  .And. cTablaEnc=="SF1" .And. (nNFTipo == 10 .OR. nNFTipo == 20 .OR. nNFTipo == 12 .OR. nNFTipo == 13 .OR. nNFTipo == 14)
		If (SF1->(FieldPos("F1_UUID")) >0 .And. SF1->(FieldPos("F1_FECTIMB")) >0 .And. SF1->(FieldPos("F1_TIMBRE")) >0 )
	     	aAdd(aCposNF,{NIL,"F1_UUID",NIL,NIL,NIL,"VldUUID()",NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,"UUID","VldWhen()"})
	     	aAdd(aCposNF,{NIL,"F1_FECTIMB",NIL,NIL,NIL,"",NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,"VldWhen()"})
	     	aAdd(aCposNF,{NIL,"F1_TIMBRE",NIL,NIL,NIL,"",NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,"VldWhen()"})
		Endif
	EndIf
	If OAPP:CMODNAME $ "SIGACOM" .And. cFunName $ ("MATA466N")  .And. cTablaEnc=="SF2" .And. (nNFTipo == 7)
		If (SF2->(FieldPos("F2_UUID")) >0 .And. SF2->(FieldPos("F2_FECTIMB")) >0 .And. SF2->(FieldPos("F2_TIMBRE")) >0 )
	     	aAdd(aCposNF,{NIL,"F2_UUID",NIL,NIL,NIL,"VldUUID()",NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,"UUID","VldWhen(.T.)"})
	     	aAdd(aCposNF,{NIL,"F2_FECTIMB",NIL,NIL,NIL,"",NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,"VldWhen(.T.)"})
	     	aAdd(aCposNF,{NIL,"F2_TIMBRE",NIL,NIL,NIL,"",NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,"VldWhen(.T.)"})
		Endif
	EndIf
	If (cTablaEnc == "SF2" .And. ;
		( cFunName $ "MATA467N" .And. (nNFTipo == 1 .Or. nNFTipo == 21) ) .Or. ;
		( cFunName $ "MATA465N" .And. (nNFTipo == 1 .Or. nNFTipo == 2) ) .Or. ;
		( cFunName $ "MATA468N" )	) .Or. ;
		( cFunName $ "MATA465N" .And. nNFTipo == 4 )
		cSFx := Substr(cTablaEnc,2,2)
		cSFT := cTablaEnc
		If ((cSFT)->(FieldPos(cSFx+"_UUID")) >0 .And. (cSFT)->(FieldPos(cSFx+"_FECTIMB"))>0 .And. (cSFT)->(FieldPos(cSFx+"_TIMBRE"))>0 ) .And. cCFDUso <> "0"
			aAdd(aCposNF,{NIL,cSFx+"_UUID",NIL,NIL,NIL,"",NIL,NIL,NIL,"V",NIL,NIL,NIL,NIL,NIL,NIL,".F."})
			aAdd(aCposNF,{NIL,cSFx+"_FECTIMB",NIL,NIL,NIL,"",NIL,NIL,NIL,"V",NIL,NIL,NIL,NIL,NIL,NIL,".F."})
			aAdd(aCposNF,{NIL,cSFx+"_FECANTF",NIL,NIL,NIL,"",NIL,NIL,NIL,"V",NIL,NIL,NIL,NIL,NIL,NIL,".F."})
			aAdd(aCposNF,{NIL,cSFx+"_TIMBRE",NIL,NIL,NIL,"",NIL,NIL,NIL,"V",NIL,NIL,NIL,NIL,NIL,NIL,".F."})
		EndIf
	EndIf
	If cTablaEnc == "SF2" .And. cFunName $ "MATA467N|MATA465N|MATA462N" .And. ( nNFTipo == 1 .Or. nNFTipo == 2 .Or. nNFTipo == 21 .or. nNFTipo == 50 )
		If SuperGetMV("MV_CFDIEXP", .F., .F.) .And. nNFTipo <> 21
			aAdd(aCposNF,{NIL,"F2_TIPOPE",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F2_CVEPED",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F2_CERORI",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F2_NUMCER",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F2_EXPCONF",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F2_INCOTER",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F2_SUBDIV",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			If SF2->(ColumnPos("F2_OBSCE")) > 0
				aAdd(aCposNF,{NIL,"F2_OBSCE",NIL,NIL,NIL,NIL,NIL,"M", NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			EndIf
			aAdd(aCposNF,{NIL,"F2_TCUSD",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F2_TOTUSD",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F2_IDTRIB",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F2_RESIDE",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F2_TRASLA",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			If SF2->(ColumnPos("F2_CONUNI")) > 0 .And. cFunName $ "MATA467N"
				aAdd(aCposNF,{NIL,"F2_CONUNI",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			EndIf
		EndIf
		If cCFDUso <> "0"
			aAdd(aCposNF,{NIL,"F2_RELSAT" ,NIL,NIL,NIL,IIF(nNFTipo == 21 .Or. nNFTipo == 1 .Or. nNFTipo == 2 .Or. nNFTipo == 50, 'ValRetSat(M->F2_RELSAT ,"F2_RELSAT") .AND. ValidF3I("S012", M->F2_RELSAT,1,2)',NIL),NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F2_USOCFDI",NIL,NIL,NIL,IIF(nNFTipo == 21 .Or. nNFTipo == 50, 'ValRetSat(M->F2_USOCFDI,"F2_USOCFDI")',NIL),NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F2_UUIDREL",NIL,NIL,NIL,IIF(nNFTipo == 21 .Or. nNFTipo == 50, 'ValRetSat(M->F2_UUIDREL,"F2_UUIDREL")',NIL),NIL,"M",NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
		EndIf
	Elseif cTablaEnc == "SF1" .And. cFunName $ "MATA465N" .And. nNFTipo == 4
		If SuperGetMV("MV_CFDIEXP", .F., .F.)
			aAdd(aCposNF,{NIL,"F1_TIPOPE",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F1_CVEPED",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F1_CERORI",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F1_NUMCER",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F1_EXPCONF",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F1_INCOTER",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F1_SUBDIV",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			If SF1->(ColumnPos("F1_OBSCE")) > 0
				aAdd(aCposNF,{NIL,"F1_OBSCE",NIL,NIL,NIL,NIL,NIL,"M", NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			EndIf
			aAdd(aCposNF,{NIL,"F1_TCUSD",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F1_TOTUSD",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F1_IDTRIB",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F1_RESIDE",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F1_TRASLA",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
		EndIf
		If cCFDUso <> "0"
			aAdd(aCposNF,{NIL,"F1_RELSAT",NIL,NIL,NIL,NIL,NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			aAdd(aCposNF,{NIL,"F1_USOCFDI",NIL,NIL,NIL,'ValRetSat(M->F1_USOCFDI,"F1_USOCFDI") .And. ValidF3I("S013", M->F1_USOCFDI,1,3)',NIL,NIL, NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
			AAdd(aCposNF,{NIL,"F1_UUIDREL",NIL,NIL,NIL,NIL,NIL,"M",NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL,NIL})
		EndIf
	EndIf
Return aCposNF

/*/{Protheus.doc} LoCnta120
Validaciones Genericas para mediciones de contrato (SIGAGCT)
@type
@author Alfredo.Medrano
@since 08/07/2020
@version 1.0
@example
LoCnta120(cAct,aCab)
@see (links_or_references)
/*/
Function LoCnta120(cAcc, aCab)
Local uContent
default cAcc := ""
default aCab := {}
	
	If cAcc == "Grv120"
		dbSelectArea("SC5")
		If CND->(ColumnPos("CND_USOCFD")) > 0 .AND. SC5->(ColumnPos("C5_USOCFDI")) > 0
			aAdd(aCab, {"C5_USOCFDI", CND->CND_USOCFD, Nil})// USO CFDI
		Endif
		If CND->(ColumnPos("CND_RELSAT")) > 0 .AND. SC5->(ColumnPos("C5_RELSAT")) > 0
			aAdd(aCab, {"C5_RELSAT", CND->CND_RELSAT, Nil})//Relacion CFD
		Endif
		If CND->(ColumnPos("CND_UUIDRE")) > 0 .AND. SC5->(ColumnPos("C5_UUIDREL")) > 0
			aAdd(aCab, {"C5_UUIDREL", CND->CND_UUIDRE, Nil})// UUID Relacs
		Endif
		uContent := aCab
	EndIf
	
Return uContent