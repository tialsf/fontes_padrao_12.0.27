#Include 'PROTHEUS.CH'
//#Include 'LOCXPER.CH'

/*/{Protheus.doc} fCposNfPer
Funcion utilizada para agregar campos al encabezado de
Notas Fiscales para Peru.
@type Function
@author Marco Augusto Gonzalez Rivera
@since 17/07/2020
@version 1.0
@param aCposNF, Array, Array con campos del encabezado de NF
@param cFunName, Character, Codigo de rutina
@param cTablaEnc, Character, Alias del encabezado de Notas Fiscales
@example fCposNfPer(aCposNF, cFunName, cTablaEnc)
@return aCposNF, Array, Campos para el Encabezado de Notas Fiscales.
@see (links_or_references)
/*/
Function fCposNfPer(aCposNF, cFunName, cTablaEnc)

	If cTablaEnc == "SF1" .And. cFunName $ "MATA101N"
		If SF1->(FieldPos("F1_TIPOPE")) > 0
			aAdd(aCposNF, {Nil, "F1_TIPOPE", Nil, Nil, Nil, GetSX3Cache("F1_TIPOPE", "X3_VALID"), Nil, Nil, Nil, Nil, Nil, Nil, Nil, Nil, Nil, Nil, Nil})
		EndIf
	EndIf
	
Return aCposNF