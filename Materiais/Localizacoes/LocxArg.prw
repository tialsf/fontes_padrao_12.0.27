#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "LOCXNF2.CH"
#INCLUDE "FWBROWSE.CH"

/*
���Programa  �LDOCORISD  �Autor  �Danilo             � Data � 07/02/2020  ���
���Desc.     �Mostra as faturas que serao amarradas ao documento sendo di-���
���          �gitado paras as notas de debito do cliente NDC              ���
����Parametros�Parametros do array a Rotina:                              ���
*/
Function LDocOriSd()

Local aArea    		:= GetArea()
Local aSF2			:= SF2->(GetArea())
Local aSD2			:= SD2->(GetArea())
Local aCposF4		:= {}
Local aRecs    		:= {}
Local aRet     		:= {}
Local nI 			:= 0
Local nJ 			:= 0
Local nPosTotal		:= 0
Local nPosTES		:= 0
Local nPosQuant		:= 0
Local nPosQtSegun	:= 0
Local nPosValDesc	:= 0
Local nTaxaNf		:= 0
Local nTaxaPed		:= 0
Local nPorDesc		:= 0
Local nUm			:= 0
Local nSegUm		:= 0
Local nCod			:= 0
Local nLocal		:= 0
Local nQuant		:= 0
Local nNfOri		:= 0
Local nSeriOri		:= 0
Local nItemOri		:= 0
Local nItem			:= 0
Local Tes			:= 0
Local nCf			:= 0
Local nLoteCtl		:= 0
Local nNumLote		:= 0
Local nDtValid		:= 0
Local nVunit		:= 0
Local nTotal		:= 0
Local nQTSegum		:= 0
Local nConta		:= 0
Local nItemCta   	:= 0
Local nCCusto		:= 0
Local nDesc			:= 0
Local nValDesc		:= 0
Local nProvEnt 		:= 0
Local nClVl			:= 0
Local nCliD2		:= 0
Local nLojaD2		:= 0
Local cCampo 		:= ""
Local cCondicao 	:= ""
Local cItem			:= ""
Local cTipoDoc 		:= ""
Local cCliFor		:= M->F2_CLIENTE
Local cLoja  		:= M->F2_LOJA
Local cSeek  		:= ""
Local cWhile 		:= ""
Local cAliasCab		:= ""
Local cAliasItem	:= ""
Local cAliasTRB		:= ""
Local cQuery		:= ""
Local cDoc			:= ""
Local cSerDoc		:= ""
Local cFilSD		:= ""
Local lDescDVIt		:= .T.
Local lD2_PROVENT	:= .F.
Local cFilSB1		:= xFilial("SB1")
Local cFilSD2		:= xFilial("SD2")
Local cFilSF4		:= xFilial("SF4")
Local cDessai		:= SuperGetMV("MV_DESCSAI",.T.,'1')
Local aAreaSF4		:= {}
Local cCentavos		:= Iif(nMoedaNF==1,"MV_CENT",("MV_CENT"+AllTrim(Str(nMoedaNF))))
Local cCtrl			:= CHR(13) + CHR(10)
Local cFunName		:= FunName()
Local cCFO			:= ""

Private aFiltro		:= {}

If Empty(cCliFor) .OR. Empty(cLoja)
	Aviso(cCadastro,STR0094,{STR0021}) //"Llene los datos del encabezado."###"OK"
	Return
EndIf

For nI:=1 to Len(aHeader)
	Do Case
		Case  Alltrim(aHeader[nI][2]) == "D2_UM"
			nUm      := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_SEGUM"
			nSegUm   := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_COD"
			nCod     := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_LOCAL"
			nLocal   := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_QUANT"
			nQuant   := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_NFORI"
			nNfOri  := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_SERIORI"
			nSeriOri := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_ITEMORI"
			nItemOri := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_ITEM"
			nItem    := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_TES"
			nTes     := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_CF"
			nCf      := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_LOTECTL"
			nLoteCtl := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_NUMLOTE"
			nNumLote := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_DTVALID"
			nDtValid := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_PRCVEN"
			nVunit   := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_TOTAL"
			nTotal   := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_QTSEGUM"
			nQTSegum := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_CONTA"
			nConta := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_CCUSTO"
			nCCusto := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_DESCON"
			nValDesc := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_DESC"
			nDesc := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_PROVENT"
			nProvEnt := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_ITEMCC"
			nItemCta := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_CLVL"
			nClVl := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_CLIENTE"
			nCliD2 := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_LOJA"
			nLojaD2 := nI
		Case  Alltrim(aHeader[nI][2]) == "D2_UNIADU"
			nUniaduD1 := nI
	Endcase
Next nI
cAliasCab	:= "SF2"
cAliasItem	:= "SD2"
SX3->(DbSetOrder(1))
SX3->(DbSeek(cAliasCab))
While !SX3->(EOF()) .AND. SX3->X3_ARQUIVO == cAliasCab
	If SX3->X3_BROWSE == "S" .AND. cNivel >= SX3->X3_NIVEL
		AAdd(aCposF4,SX3->X3_CAMPO)
	Endif
	SX3->(DbSkip())
EndDo

If aCfgNF[1] == 2

	cTipoDoc	:= "'01'" //Tipo documento origem 
	cSeek  	:= "'" + xFilial(cAliasCab)+cCliFor+cLoja + "'"
	cWhile 	:= "SF2->(!EOF()) .AND. SF2->(F2_FILIAL+F2_CLIENTE+F2_LOJA)== " + cSeek
	cCondicao	:= "Ascan(aFiltro,SF2->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENTE+F2_LOJA+F2_TIPODOC)) > 0"
	cItem		:= aCols[Len(aCols),nItem]

	cAliasTRB := GetNextAlias()
	cQuery := "select distinct D2_FILIAL,D2_DOC,D2_SERIE,D2_CLIENTE,D2_LOJA,D2_TIPO,D2_TIPODOC,D2_ITEM"
	cQuery += " from " + RetSqlName("SD2") + " SD2 where "
	cQuery += " D2_FILIAL ='" + xFilial("SD2") + "'"
	cQuery += " and D2_CLIENTE = '" + cCliFor + "'"
	cQuery += " and D2_LOJA = '" + cLoja + "'"
	cQuery += " and D2_TIPODOC in (" + cTipoDoc + ")"
	cQuery += " and D2_QUANT > D2_QTDEDEV"
	cQuery += " and SD2.D_E_L_E_T_ = ' ' "
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasTRB,.F.,.T.)
    DbSelectArea(cAliasTRB)

    While (cAliasTRB)->(!Eof())
    	nI := Ascan(aCols,{|x| x[nNFORI] == (cAliasTRB)->D2_DOC .AND. x[nItemOri] == (cAliasTRB)->D2_ITEM .AND. !x[Len(x)]})
    	If nI == 0
			Aadd(aFiltro, (cAliasTRB)->D2_FILIAL + (cAliasTRB)->D2_DOC + (cAliasTRB)->D2_SERIE + (cAliasTRB)->D2_CLIENTE + (cAliasTRB)->D2_LOJA + (cAliasTRB)->D2_TIPODOC)
		Endif
		(cAliasTRB)->(DbSkip())
	EndDo
	(cAliasTRB)->(DbCloseArea())
Else
	Return
EndIf

If !Empty(aFiltro)	
	aRet := LocxF4(cAliasCab,2,cWhile,cSeek,aCposF4,,IIf(cTipoDoc =="'50'",GetDescRem(),Iif(cTipoDoc =="'02'", STR0124,STR0028)),cCondicao,.T.,,,,,.F.)  // Retorn
Else
	Help(" ",1,"A103F4")
	Return
EndIf
If ValType(aRet)=="A" .AND. Len(aRet)==3
	aRecs := aRet[3]
EndIf
If ValType(aRecs)!="A" .OR. (ValType(aRecs)=="A" .AND. Len(aRecs)==0)
	Return
EndIf
SD2->(DbSetOrder(3))
cFilSD := cFilSD2
ProcRegua(Len(aRecs))

lD2_PROVENT	:=	cPaisLoc == "ARG"
For nI := 1 To Len(aRecs)
	SF2->(MsGoTo(aRecs[nI]))
	SD2->(DbSeek(cFilSD + SF2->F2_DOC + SF2->F2_SERIE + SF2->F2_CLIENTE + SF2->F2_LOJA))
	IncProc("Actualizando �tem " + "(" + SF2->F2_DOC + ")")
	While SD2->D2_FILIAL == cFilSD .AND. SD2->D2_DOC == SF2->F2_DOC .AND. SD2->D2_SERIE == SF2->F2_SERIE .AND. SD2->D2_CLIENTE == SF2->F2_CLIENTE .AND. SD2->D2_LOJA == SF2->F2_LOJA
		If SD2->D2_QUANT > SD2->D2_QTDEDEV
        	If Ascan(aCols,{|x| x[nNFORI] == SD2->D2_DOC .AND. x[nItemOri] == SD2->D2_ITEM .AND. !x[Len(x)]}) == 0
				nLenAcols := Len(aCols)
				If !Empty(aCols[nLenAcols,nCod])
					AAdd(aCols,Array(Len(aHeader)+1))
					nLenAcols := Len(aCols)
					cItem := Soma1(cItem)
				Endif
			 	aCols[nLenAcols][Len(aHeader)+1] := .F.
				SB1->(MsSeek(cFilSB1+SD2->D2_COD))
				If (nUm      >  0  ,  aCOLS[nLenAcols][nUm     ] := SD2->D2_UM	,)
				If (nSegUm   >  0  ,  aCOLS[nLenAcols][nSegUm  ] := SB1->B1_SEGUM,)
				If (nCod     >  0  ,  aCOLS[nLenAcols][nCod    ] := SD2->D2_COD,)
				If (nLocal   >  0  ,  aCOLS[nLenAcols][nLocal  ] := SD2->D2_LOCAL,)
				If (nNfOri   >  0  ,  aCOLS[nLenAcols][nNfOri  ] := SD2->D2_DOC,)
				If (nSeriOri >  0  ,  aCOLS[nLenAcols][nSeriOri] := SD2->D2_SERIE,)
				If (nItemOri >  0  ,  aCOLS[nLenAcols][nItemOri] := SD2->D2_ITEM,)
				If (nItem    >  0  ,  aCOLS[nLenAcols][nItem   ] := cItem,)
				If (nConta   >  0  ,  aCOLS[nLenAcols][nConta  ] := SD2->D2_CONTA,)
	   			If (nCCusto  >  0  ,  aCOLS[nLenAcols][nCCusto ] := SD2->D2_CCUSTO,)
			   	If (nItemCta >  0  ,  aCOLS[nLenAcols][nItemCta] := SD2->D2_ITEMCC,)
			   	If (nClVl    >  0  ,  aCOLS[nLenAcols][nClVl]  	:= SD2->D2_CLVL,)
				If (nLoteCtl >  0  ,  aCOLS[nLenAcols][nLoteCtl] := SD2->D2_LOTECTL,)
				If (nNumLote >  0  ,  aCOLS[nLenAcols][nNumLote] := SD2->D2_NUMLOTE,)
				If (nDtValid >  0  ,  aCOLS[nLenAcols][nDtValid] := SD2->D2_DTVALID,)
				If (nQtSegUm >  0  ,  aCOLS[nLenAcols][nQtSegUm] := SD2->D2_QTSEGUM,)
				If (nCliD2   >  0  ,  aCOLS[nLenAcols][nCliD2]   := SD2->D2_CLIENTE,)
				If (nLojaD2  >  0  ,  aCOLS[nLenAcols][nLojaD2]	:= SD2->D2_LOJA,)

				nQtdeDev := SD2->D2_QUANT - SD2->D2_QTDEDEV
				If nQuant > 0
					aCols[nLenAcols,nQuant] := nQtdeDev
				Endif
				If nQtSegUm > 0
					aCols[nLenAcols,nQtSegUm] := ConvUm(SD2->D2_COD,nQtdeDev,0,2)
				Endif
				If nTES <> 0
					aCols[nLenAcols][nTES] := SD2->D2_TES
					aCols[nLenAcols][nCf] := SD2->D2_CF
				EndIf
				nTaxaNF := MaFisRet(,'NF_TXMOEDA')
				nTaxaNF := Iif(nTaxaNF == 0,Recmoeda(dDatabase,M->F1_MOEDA),nTaxaNF)
				If cPaisLoc == "ARG" .And. nProvEnt > 0 .And. lD2_PROVENT
					aCols[nLenAcols,nProvEnt] := SD2->D2_PROVENT
				Endif
				AEval(aHeader,{|x,y| If(aCols[nLenAcols][y]==NIL,aCols[nLenAcols][y]:=CriaVar(x[2]),) })
				MaColsToFis(aHeader,aCols,nLenAcols,"MT100",.T.)
				MaFisAlt("IT_RECORI",SD2->(Recno()),nLenAcols)

			EndIf
		Endif
		SD2->(DbSkip())
	EndDo
Next nI
oGetDados:lNewLine:=.F.
oGetDados:obrowse:refresh()
Eval(bDoRefresh)
AtuLoadQt()
If cPaisLoc == "ARG" 
	MaFisReprocess(2)
EndiF
RestArea(aSD2)
RestArea(aSF2)
RestArea(aArea)

Return

/*
���Programa  �ARGNFORIDB  �Autor  �Danilo            � Data � 08/02/2020  ���
���Desc.     �Faz a chamada da Tela de Consulta a NF original             ���
���          �por item                                                    ���
*/


Function ARGNFORIDB()

Local bSavKeyF4 := SetKey(VK_F4,Nil)
Local bSavKeyF5 := SetKey(VK_F5,Nil)
Local bSavKeyF6 := SetKey(VK_F6,Nil)
Local bSavKeyF7 := SetKey(VK_F7,Nil)
Local bSavKeyF8 := SetKey(VK_F8,Nil)
Local bSavKeyF9 := SetKey(VK_F9,Nil)
Local bSavKeyF10:= SetKey(VK_F10,Nil)
Local bSavKeyF11:= SetKey(VK_F11,Nil)
Local nPosCod	:= aScan(aHeader,{|x| AllTrim(x[2])=='D2_COD'})
Local nPosLocal := aScan(aHeader,{|x| AllTrim(x[2])=='D2_LOCAL'})
Local nPosTes	:= aScan(aHeader,{|x| AllTrim(x[2])=='D2_TES'})
Local nPLocal	:= aScan(aHeader,{|x| AllTrim(x[2])=='D2_LOCAL'})
Local nRecSD2   := 0
Local lContinua := .T.

//���������������������������������������������������������������������Ŀ
//� Impede de executar a rotina quando a tecla F3 estiver ativa		    �
//�����������������������������������������������������������������������
If Type("InConPad") == "L"
	lContinua := !InConPad
EndIf

If lContinua

	dbSelectArea("SF4")
	dbSetOrder(1)
	MsSeek(xFilial("SF4")+aCols[n][nPosTes])

	If MaFisFound("NF") .And. !Empty(M->F2_CLIENTE) .And. !empty(M->F2_LOJA) //.And. 
		If 	! Empty(aCols[n][nPosCod])
			
			If F4NFORIARG(,,"M->D2_NFORI",M->F2_CLIENTE,M->F2_LOJA,aCols[n][nPosCod],"A465N",aCols[n][nPLocal],@nRecSD2)
				LxA103SD2ToaCols(nRecSD2,n)
			EndIf
		Else
			Help(NIL, NIL, "Item da NF", NIL, "Informar el Producto", 1, 0, NIL, NIL, NIL, NIL, NIL, {"Informe al producto de la nota para vincular al item"})
		Endif	
	Else
		Help('   ',1,'A103CAB')
	EndIf
Endif
If cPaisLoc == "ARG"
	MaFisReprocess(2)
Endif	
SetKey(VK_F4,bSavKeyF4)
SetKey(VK_F5,bSavKeyF5)
SetKey(VK_F6,bSavKeyF6)
SetKey(VK_F7,bSavKeyF7)
SetKey(VK_F8,bSavKeyF8)
SetKey(VK_F9,bSavKeyF9)
SetKey(VK_F10,bSavKeyF10)
SetKey(VK_F11,bSavKeyF11)

/*��������������������������������������������Ŀ
  �Atualiza o browse de quantidade de produtos.�
  ����������������������������������������������*/
AtuLoadQt(.T.)

Return .T.


/*/
���Programa  �F4NfOriArg  �Autor  �Danilo             � Data � 15/01/2020  ���
��������������������������������������������������������������������������Ĵ��
���          �Interface de visualizacao dos documentos de saida            ���
���          �que poder�o ser vinculados a nota de origem por item         ���
���Parametros�ExpC1: Nome da rotina chamadora                              ���
���          �ExpN2: Numero da linha da rotina chamadora              (OPC)���
���          �ExpC4: Nome do campo GET em foco no momento             (OPC)���
���          �ExpC5: Codigo do Cliente/Fornecedor                          ���
���          �ExpC6: Loja do Cliente/Fornecedor                            ���
���          �ExpC7: Codigo do Produto                                     ���
���          �ExpC8: Local a ser considerado                               ���
���          �ExpN9: Numero do recno do SD1/SD2                            ���
/*/
Function F4NfOriArg(cRotina,nLinha,cReadVar,cCliFor,cLoja,cProduto,cPrograma,cLocal,nRecSD2,nRecSD1,dInvDate)

Local aArea     := GetArea()
Local aAreaSF1  := SF1->(GetArea())
Local aAreaSF2  := SF2->(GetArea())
Local aAreaSD1  := SD1->(GetArea())
Local aAreaSD2  := SD2->(GetArea())	
Local aStruTRB  := {}
Local aStruSD1  := {}
Local aStruSD2  := {}
Local aStruSF1  := {}
Local aStruSF2  := {}
Local aValor    := {}
Local aOrdem    := {AllTrim(RetTitle("F2_DOC"))+"+"+AllTrim(RetTitle("F2_SERIE")),AllTrim(RetTitle("F2_EMISSAO"))}
Local aChave    := {}
Local aPesq     := {}
Local aNomInd   := {}
Local aObjects  := {}
Local aInfo     := {}
Local aPosObj   := {}
Local aSize     := MsAdvSize( .F. )
Local aHeadTRB  := {}
Local aSavHead  := aClone(aHeader)
Local cAliasSD2 := "SD2"
Local cAliasSF2 := "SF2"
Local cAliasSF4 := "SF4"
Local cAliasTRB := "F4NFORI"
Local cNomeTrb  := ""
Local cQuery    := ""
Local cCombo    := ""
Local cTexto1   := ""
Local cTexto2   := ""
Local lRetorno  := .F.
Local lSkip     := .F.
Local cTpCliFor := "C"
Local nX        := 0
Local nY        := 0
Local nSldQtd   := 0
Local nSldQtd2  := 0
Local nSldLiq   := 0
Local nSldBru   := 0
Local nHdl      := GetFocus()
Local nOpcA     := 0
Local nPNfOri   := 0
Local nPSerOri  := 0
Local nPItemOri := 0
Local nPLocal   := 0
Local nPPrUnit  := 0
Local nPPrcVen  := 0
Local nPQuant   := 0
Local nPQuant2UM:= 0
Local nPLoteCtl := 0
Local nPNumLote := 0
Local nPDtValid := 0
Local nPPotenc  := 0
Local nPValor   := 0
Local nPValDesc := 0
Local nPDesc    := 0
Local nPOrigem  := 0
Local nPDespacho:= 0
Local nPTES     := 0
Local nPCf		:= 0
Local nPProvEnt := 0
Local nPConcept := 0
Local nD1Fabric := 0
Local nPPeso	:= 0
Local nFciCod   := 0
Local nPCC      := 0 // Posici�n Centro de Costo
Local xPesq     := ""
Local oDlg
Local oCombo
Local oGet
Local oGetDb
Local oPanel
Local cFiltraQry:=""
Local lFiltraQry:=ExistBlock('F4NFORI')
Local lUsaNewKey:= TamSX3("F2_SERIE")[1] == 14 // Verifica se o novo formato de gravacao do Id nos campos _SERIE esta em uso
Local lDescSai  := IIF(cPaisLoc == "BRA",SuperGetMV("MV_DESCSAI",.F.,"2"),SuperGetMV("MV_DESCSAI",.F.,"1")) == "1"
Local cCtrl     := CHR(13) + CHR(10) // Salto de l�nea para los UUID Relacionados
Local cPreSD        := ""

Local nMoedaOri := 0
Local nMdaConv := 0
Local nTotalOri := 0
Local nTtalConv := 0

DEFAULT cReadVar := ReadVar()
DEFAULT dInvDate := dDataBase

PRIVATE aRotina  := {}

For nX := 1 To 11	// Walk_Thru
	aAdd(aRotina,{"","",0,0})
Next

If ("_NFORI"$cReadVar) .Or. ( lUsaNewKey .And. ("_SERIORI"$cReadVar .Or. "_ITEMORI"$cReadVar )  )
	
	cTpCliFor := "C"
	aChave    := {"D2_DOC+D2_SERIE","D2_EMISSAO"}
	aPesq     := {{Space(Len(SD2->D2_DOC+SD2->D2_SERIE)),"@!"},{Ctod(""),"@!"}}

	//���������������������������������������������������������������������Ŀ
	//� Montagem do arquivo temporario dos itens do SD2                     �
	//�����������������������������������������������������������������������
	dbSelectArea("SX3")
	dbSetOrder(1)
	MsSeek("SD2")
	While !Eof() .And. SX3->X3_ARQUIVO == "SD2"
		If ( X3USO(SX3->X3_USADO) .And. cNivel >= SX3->X3_NIVEL .And.;
			Trim(SX3->X3_CAMPO) <> "D2_COD" .And.;
				SX3->X3_CONTEXT <> "V"  .And.;
				SX3->X3_TIPO<>"M" ) .Or.;
				Trim(SX3->X3_CAMPO) == "D2_DOC" .Or.;
				Trim(SX3->X3_CAMPO) == "D2_SERIE"  .Or.;
				Trim(SX3->X3_CAMPO) == "D2_EMISSAO" .Or.;
				Trim(SX3->X3_CAMPO) == "D2_TIPO" .Or.;
				Trim(SX3->X3_CAMPO) == "D2_PRUNIT" .Or. ;
				Trim(SX3->X3_CAMPO) == "D2_DESCZFR"
				Aadd(aHeadTrb,{ TRIM(X3Titulo()),;
					SX3->X3_CAMPO,;
					SX3->X3_PICTURE,;
					SX3->X3_TAMANHO,;
					SX3->X3_DECIMAL,;
					SX3->X3_VALID,;
					SX3->X3_USADO,;
					SX3->X3_TIPO,;
					SX3->X3_ARQUIVO,;
					SX3->X3_CONTEXT,;
					IIf(AllTrim(SX3->X3_CAMPO)$"D2_DOC#D2_SERIE#D2_ITEM#D2_TIPO","00",SX3->X3_ORDEM) })
				aadd(aStruTRB,{SX3->X3_CAMPO,SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL,IIf(AllTrim(SX3->X3_CAMPO)$"D2_DOC#D2_SERIE#D2_ITEM","00",SX3->X3_ORDEM)})
		EndIf				
		dbSelectArea("SX3")
		dbSkip()
	EndDo
	//����������������������������Ŀ
	//�Walk-Thru                   �
	//������������������������������	
	ADHeadRec("SD2",aHeadTrb)
	aSize(aHeadTrb[Len(aHeadTrb)-1],11)
	aSize(aHeadTrb[Len(aHeadTrb)],11)
	aHeadTrb[Len(aHeadTrb)-1,11] := "ZX"
	aHeadTrb[Len(aHeadTrb),11]	 := "ZY"
	aadd(aStruTRB,{"D2_ALI_WT","C",3,0,"ZX"})
	aadd(aStruTRB,{"D2_REC_WT","N",18,0,"ZY"})
	aadd(aStruTRB,{"D2_TOTAL2","N",18,2,"ZZ"})
	aHeadTrb := aSort(aHeadTrb,,,{|x,y| x[11] < y[11]})
	aStruTrb := aSort(aStruTrb,,,{|x,y| x[05] < y[05]})

	cNomeTrb := FWOpenTemp(cAliasTRB,aStruTRB,,.T.)
	dbSelectArea(cAliasTRB)
	For nX := 1 To Len(aChave)
		aAdd( aNomInd , StrTran( (SubStr( cNomeTrb, 1 , 7 ) + Chr( 64 + nX ) ), "_" , "") )
		IndRegua(cAliasTRB,aNomInd[nX],aChave[nX])
	Next nX
	dbClearIndex()
	For nX := 1 To Len(aNomInd)
		dbSetIndex(aNomInd[nX])
	Next nX
	//���������������������������������������������������������������������Ŀ
	//� Atualizacao do arquivo temporario com base nos itens do SD2         �
	//�����������������������������������������������������������������������
	If lFiltraQry
		cFiltraQry	:=	ExecBlock('F4NFORI',.F.,.F.,{"SD2",cPrograma,cClifor,cLoja})
		If ValType(cFiltraQry) <> 'C'
			cFiltraQry	:=	''
		Endif	
	Endif
	dbSelectArea("SF2")
	dbSetOrder(2)

	cAliasSF2 := "F4NFORI_SQL"
	cAliasSD2 := "F4NFORI_SQL"			    
	cAliasSF4 := "F4NFORI_SQL"			    			    
	aStruSF2 := SF2->(dbStruct())
	aStruSD2 := SD2->(dbStruct())
	cQuery := "SELECT SF4.F4_PODER3,SD2.R_E_C_N_O_ SD2RECNO,"
	cQuery += "SF2.F2_FILIAL,SF2.F2_CLIENTE,SF2.F2_LOJA,"
	cQuery += "SF2.F2_TIPO,SF2.F2_DOC,SF2.F2_SERIE,SD2.D2_FILIAL,SD2.D2_COD,"
	cQuery += "SD2.D2_TIPO,SD2.D2_DOC,SD2.D2_SERIE,SD2.D2_FILIAL,SD2.D2_CLIENTE,"
	cQuery += "SD2.D2_LOJA,SD2.D2_QTDEDEV,SD2.D2_VALDEV,SD2.D2_ORIGLAN,SD2.D2_TES,SD2.D2_TIPOREM "
	cQuery += ",SD2.D2_TIPODOC "

	For nX := 1 To Len(aStruTRB)
		If !"D2_REC_WT"$aStruTRB[nX][1] .And. !"D2_ALI_WT"$aStruTRB[nX][1] .And. !"D2_TOTAL2"$aStruTRB[nX][1]
			cQuery += ","+aStruTRB[nX][1]
		EndIf
	Next nX
	cQuery += " FROM "+RetSqlName("SF2")+" SF2,"
	cQuery +=  RetSqlName("SD2")+" SD2,"
	cQuery +=  RetSqlName("SF4")+" SF4 "
	cQuery += "WHERE " 
	cQuery += "SF2.F2_FILIAL = '"+xFilial("SF2")+"' AND "
	cQuery += "SF2.F2_CLIENTE = '"+cCliFor+"' AND "
	cQuery += "SF2.F2_LOJA = '"+cLoja+"' AND "
	cQuery += "SF2.D_E_L_E_T_=' ' AND "
	cQuery += "SD2.D2_FILIAL='"+xFilial("SD2")+"' AND "
	cQuery += "SD2.D2_CLIENTE=SF2.F2_CLIENTE AND "
	cQuery += "SD2.D2_LOJA=SF2.F2_LOJA AND "
	cQuery += "SD2.D2_DOC=SF2.F2_DOC AND "
	cQuery += "SD2.D2_SERIE=SF2.F2_SERIE AND "        
	cQuery += "SD2.D2_TIPO=SF2.F2_TIPO AND "
	If IsInCallStack("Documentos") .And. Type("cTipo") == "U"
		cTipo := SDS->DS_TIPO
	EndIf

	cQuery += "F2_TIPO not in('B','D','C') AND " // Tipo da nota de saida		
	cQuery += "SD2.D2_COD='"+cProduto+"' AND "
	cQuery += "SD2.D2_ORIGLAN<>'LF' AND "
	cQuery += "SD2.D_E_L_E_T_=' ' AND "
	cQuery += "SF4.F4_FILIAL='"+xFilial("SF4")+"' AND "	
	cQuery += "SF4.F4_CODIGO=SD2.D2_TES AND "
	cQuery += "SF4.D_E_L_E_T_=' ' "
	cQuery += "	AND	SF2.F2_EMISSAO <= '" + DTOS(dInvDate) + "' "		
	If !Empty(cFiltraQry)
		cQuery += " AND "+cFiltraQry
	Endif
	cQuery += "ORDER BY "+SqlOrder(SF2->(IndexKey()))
			
	cQuery := ChangeQuery(cQuery)

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasSF2,.T.,.T.)
						
	For nX := 1 To Len(aStruSD2)
		If aStruSD2[nX][2] <> "C" 
			TcSetField(cAliasSF2,aStruSD2[nX][1],aStruSD2[nX][2],aStruSD2[nX][3],aStruSD2[nX][4])
		EndIf				
	Next nX
	For nX := 1 To Len(aStruSF2)
		If aStruSF2[nX][2] <> "C" 
			TcSetField(cAliasSF2,aStruSF2[nX][1],aStruSF2[nX][2],aStruSF2[nX][3],aStruSF2[nX][4])
		EndIf				
	Next nX				

	While !Eof() .And. (cAliasSF2)->F2_FILIAL = xFilial("SF2") .And.;
		(cAliasSF2)->F2_CLIENTE == cCliFor .And.;
		(cAliasSF2)->F2_LOJA == cLoja
		lSkip := .F.
		While !Eof() .And. xFilial("SD2") == (cAliasSD2)->D2_FILIAL .And.;
			cProduto == (cAliasSD2)->D2_COD .And.;
			(cAliasSF2)->F2_DOC == (cAliasSD2)->D2_DOC .And.;
			(cAliasSF2)->F2_SERIE == (cAliasSD2)->D2_SERIE .And.;
			(cAliasSF2)->F2_CLIENTE == (cAliasSD2)->D2_CLIENTE .And.;
			(cAliasSF2)->F2_LOJA == (cAliasSD2)->D2_LOJA
					
			If (cAliasSD2)->D2_TIPO ==(cAliasSF2)->F2_TIPO .And. ( (cAliasSF4)->F4_PODER3 == "N" .Or. ((cAliasSF4)->F4_PODER3 == "R" .And. (cAliasSD2)->D2_TIPOREM == "A")) .And. !Empty((cAliasSD2)->D2_TES) .And. (cAliasSD2)->D2_ORIGLAN<>"LF" .And. (cAliasSD2)->D2_TIPO<>"D"
												
	      		If Empty(cFiltraQry) .Or. ValType(cFiltraQry) == "C"
	         		If (cAliasSD2)->D2_TIPODOC < "50"
						nSldQtd:= (cAliasSD2)->D2_QUANT-(cAliasSD2)->D2_QTDEDEV
						nSldQtd2:=ConvUm((cAliasSD2)->D2_COD,nSldQtd,0,2)
						nSldBru:= (cAliasSD2)->D2_TOTAL+((cAliasSD2)->D2_DESCON+(cAliasSD2)->D2_DESCZFR)-(cAliasSD2)->D2_VALDEV
						nPNfOri   := aScan(aHeader,{|x| AllTrim(x[2])=="D2_NFORI"})
						nPSerOri  := aScan(aHeader,{|x| AllTrim(x[2])=="D2_SERIORI"})
						nPItemOri := aScan(aHeader,{|x| AllTrim(x[2])=="D2_ITEMORI"})
						nPValor   := aScan(aHeader,{|x| AllTrim(x[2])=="D2_TOTAL"})
						nPQuant   := aScan(aHeader,{|x| AllTrim(x[2])=="D2_QUANT"})
						nPQuant2UM:= aScan(aHeader,{|x| AllTrim(x[2])=="D2_QTSEGUM"})

						For nX := 1 To Len(aCols)
							If nX <> N .And.;
								!aCols[nX][Len(aHeader)+1] .And.;
								aCols[nX][nPNfOri] == (cAliasSD2)->D2_DOC .And.;
								aCols[nX][nPSerOri] == (cAliasSD2)->D2_SERIE .And.;
								aCols[nX][nPItemOri] == (cAliasSD2)->D2_ITEM
								nSldQtd -= aCols[nX][nPQuant]
								nSldBru -= aCols[nX][nPValor]
							EndIf
						Next nX
						nSldQtd2:=ConvUm((cAliasSD2)->D2_COD,nSldQtd,0,2)
						nSldLiq:= nSldBru-A410Arred(nSldBru*((cAliasSD2)->D2_DESCON+(cAliasSD2)->D2_DESCZFR)/((cAliasSD2)->D2_TOTAL+((cAliasSD2)->D2_DESCON+(cAliasSD2)->D2_DESCZFR)),"C6_VALOR")
						//���������������������������������������������������������������������Ŀ
						//� Atualiza o arquivo de trabalho                                      �
						//�����������������������������������������������������������������������
						If nSldQtd <> 0 .Or. nSldLiq <> 0
							RecLock(cAliasTRB,.T.)
							For nX := 1 To Len(aStruTRB)
								If !(AllTrim(aStruTRB[nX][1]) $ "D2_ALI_WT|D2_REC_WT|D2_TOTAL2")
									(cAliasTRB)->(FieldPut(nX,(cAliasSD2)->(FieldGet(FieldPos(aStruTRB[nX][1])))))
								EndIf	
							Next nX
							(cAliasTRB)->D2_QUANT := a410Arred(nSldQtd,"C6_QTDVEN")
							(cAliasTRB)->D2_QTSEGUM:= a410Arred(nSldQtd2,"C6_UNSVEN")
							(cAliasTRB)->D2_TOTAL := a410Arred(nSldLiq,"C6_VALOR")
							(cAliasTRB)->D2_TOTAL2:= a410Arred(nSldBru,"C6_VALOR")
							(cAliasTRB)->D2_PRCVEN:= a410Arred(nSldLiq/IIf(nSldQtd==0,1,nSldQtd),"C6_PRCVEN")
							If Abs((cAliasTRB)->D2_PRCVEN-(cAliasSD2)->D2_PRCVEN)<= 0.01
								(cAliasTRB)->D2_PRCVEN := (cAliasSD2)->D2_PRCVEN
							EndIf
							(cAliasTRB)->D2_PRUNIT := (cAliasTRB)->D2_PRCVEN
							(cAliasTRB)->D2_REC_WT:= (cAliasSD2)->SD2RECNO
							(cAliasTRB)->D2_ALI_WT := "SD2"
							MsUnLock()
						EndIf
					EndIf
		  		EndIf
			Endif
			dbSelectArea(cAliasSD2)
			dbSkip()
			lSkip := .T.
		EndDo
		If !lSkip
			dbSelectArea(cAliasSF2)
			dbSkip()
		EndIf
	EndDo
	
	(cAliasSF2)->(dbCloseArea())
						
	cPreSD := IIf(cTpCliFor == "C", "D2", "D1")
	If (cAliasTRB)->(LastRec())<>0
		PRIVATE aHeader := aHeadTRB
		xPesq := aPesq[(cAliasTRB)->(IndexOrd())][1]
		//���������������������������������������������������������������������Ŀ
		//� Posiciona registros                                                 �
		//�����������������������������������������������������������������������
		dbSelectArea("SA1")
		dbSetOrder(1)
		MsSeek(xFilial("SA1")+cCliFor+cLoja)

		dbSelectArea("SB1")
		dbSetOrder(1)
		MsSeek(xFilial("SB1")+cProduto)
		
		dbSelectArea(cAliasTRB)
		dbGotop()	
		//���������������������������������������������������������������������Ŀ
		//� Calcula as coordenadas da interface                                 �
		//�����������������������������������������������������������������������
		aSize[1] /= 1.5
		aSize[2] /= 1.5
		aSize[3] /= 1.5
		aSize[4] /= 1.3
		aSize[5] /= 1.5
		aSize[6] /= 1.3
		aSize[7] /= 1.5
		
		AAdd( aObjects, { 100, 020,.T.,.F.,.T.} )
		AAdd( aObjects, { 100, 060,.T.,.T.} )
		AAdd( aObjects, { 100, 020,.T.,.F.} )
		aInfo   := { aSize[ 1 ], aSize[ 2 ], aSize[ 3 ], aSize[ 4 ], 3, 3 } 
		aPosObj := MsObjSize( aInfo, aObjects,.T.)
	
		//���������������������������������������������������������������������Ŀ
		//� Interface com o usuario                                             �
		//�����������������������������������������������������������������������
		DEFINE MSDIALOG oDlg TITLE OemToAnsi(STR0028) FROM aSize[7],000 TO aSize[6],aSize[5] OF oMainWnd PIXEL //"Notas Fiscais de Origem"
		@ aPosObj[1,1],aPosObj[1,2] MSPANEL oPanel PROMPT "" SIZE aPosObj[1,3],aPosObj[1,4] OF oDlg CENTERED LOWERED
		cTexto1 := AllTrim(RetTitle("F2_CLIENTE"))+"/"+AllTrim(RetTitle("F2_LOJA"))+": "+SA1->A1_COD+"/"+SA1->A1_LOJA+"  -  "+RetTitle("A1_NOME")+": "+SA1->A1_NOME
		@ 002,005 SAY cTexto1 SIZE aPosObj[1,3],008 OF oPanel PIXEL
		cTexto2 := AllTrim(RetTitle("B1_COD"))+": "+SB1->B1_COD+"/"+SB1->B1_DESC
		@ 012,005 SAY cTexto2 SIZE aPosObj[1,3],008 OF oPanel PIXEL	
		
		@ aPosObj[3,1]+00,aPosObj[3,2]+00 SAY OemToAnsi(STR0027) PIXEL //Pesquisar por:
		@ aPosObj[3,1]+12,aPosObj[3,2]+00 SAY OemToAnsi(STR0026) PIXEL //Localizar
		@ aPosObj[3,1]+00,aPosObj[3,2]+40 MSCOMBOBOX oCombo VAR cCombo ITEMS aOrdem SIZE 100,044 OF oDlg PIXEL ;
		VALID ((cAliasTRB)->(dbSetOrder(oCombo:nAt)),(cAliasTRB)->(dbGotop()),xPesq := aPesq[(cAliasTRB)->(IndexOrd())][1],.T.)
	  	@ aPosObj[3,1]+12,aPosObj[3,2]+40 MSGET oGet VAR xPesq Of oDlg PICTURE aPesq[(cAliasTRB)->(IndexOrd())][2] PIXEL ;
	  	VALID ((cAliasTRB)->(MsSeek(Iif(ValType(xPesq)=="C",AllTrim(xPesq),xPesq),.T.)),.T.).And.IIf(oGetDb:oBrowse:Refresh()==Nil,.T.,.T.)
	  	
	  	oGetDb := MsGetDB():New(aPosObj[2,1],aPosObj[2,2],aPosObj[2,3],aPosObj[2,4],1,"Allwaystrue","allwaystrue","",.F., , ,.F., ,cAliasTRB)
		
		DEFINE SBUTTON FROM aPosObj[3,1]+000,aPosObj[3,4]-030 TYPE 1 ACTION (nOpcA := 1,oDlg:End()) ENABLE OF oDlg
		DEFINE SBUTTON FROM aPosObj[3,1]+012,aPosObj[3,4]-030 TYPE 2 ACTION (nOpcA := 0, oDlg:End()) ENABLE OF oDlg
		ACTIVATE MSDIALOG oDlg CENTERED
		
		If nOpcA == 1
			lRetorno := .T.
			aHeader   := aClone(aSavHead)
				 
			nPNfOri   := aScan(aHeader,{|x| AllTrim(x[2])=="D2_NFORI"})
			nPSerOri  := aScan(aHeader,{|x| AllTrim(x[2])=="D2_SERIORI"})
			nPItemOri := aScan(aHeader,{|x| AllTrim(x[2])=="D2_ITEMORI"})
			nPLocal   := aScan(aHeader,{|x| AllTrim(x[2])=="D2_LOCAL"})
			nPPrcVen  := aScan(aHeader,{|x| AllTrim(x[2])=="D2_PRCVEN"})
			nPQuant   := aScan(aHeader,{|x| AllTrim(x[2])=="D2_QUANT"})
			nPQuant2UM:= aScan(aHeader,{|x| AllTrim(x[2])=="D2_QTSEGUM"})
			nPLoteCtl := aScan(aHeader,{|x| AllTrim(x[2])=="D2_LOTECTL"})
			nPNumLote := aScan(aHeader,{|x| AllTrim(x[2])=="D2_NUMLOTE"})
			nPDtValid := aScan(aHeader,{|x| AllTrim(x[2])=="D2_DTVALID"})
			nPPotenc  := aScan(aHeader,{|x| AllTrim(x[2])=="D2_POTENCI"})
			nPValor   := aScan(aHeader,{|x| AllTrim(x[2])=="D2_TOTAL"})
			nPTES     := aScan(aHeader,{|x| AllTrim(x[2])=="D2_TES"})
			nPCf     := aScan(aHeader,{|x| AllTrim(x[2])=="D2_CF"})
			nPProvEnt := aScan(aHeader,{|x| AllTrim(x[2])=="D2_PROVENT"})
			nPConcept := aScan(aHeader,{|x| AllTrim(x[2])=="D2_CONCEPT"})
					
			If nPNfOri <> 0
 				aCols[N][nPNfOri] := (cAliasTRB)->D2_DOC
 			EndIf
 			If nPSerOri <> 0
 				aCols[N][nPSerOri] := (cAliasTRB)->D2_SERIE
 			EndIf
			If nPItemOri <> 0
 				aCols[N][nPItemOri] := (cAliasTRB)->D2_ITEM
			EndIf
			If nPLocal <> 0
 				aCols[N][nPLocal] := (cAliasTRB)->D2_LOCAL
 			EndIf
 			If nPQuant <> 0
				aCols[N][nPQuant] := (cAliasTRB)->D2_QUANT
			EndIf
			If nPQuant2UM <> 0
				aCols[N][nPQuant2UM] := (cAliasTRB)->D2_QTSEGUM
			EndIf
			If nPConcept <> 0 
	 			aCols[N][nPConcept] := (cAliasTRB)->D2_CONCEPT
	 		Endif
			If nPLoteCtl <> 0
				aCols[N][nPLoteCtl] := (cAliasTRB)->D2_LOTECTL
			EndIf
			If nPNumLote <> 0
				aCols[N][nPNumLote] := (cAliasTRB)->D2_NUMLOTE
			EndIf
			If nPDtValid <> 0 .Or. npPotenc <> 0
				dbSelectArea("SB8")
				dbSetOrder(3)
				If MsSeek(xFilial("SB8")+cProduto+aCols[N][nPLocal]+aCols[n][nPLoteCtl]+IIf(Rastro(cProduto,"S"),aCols[N][nPNumLote],""))
					If nPDtValid <> 0
						aCols[n][nPDtValid] := SB8->B8_DTVALID
					EndIf
					If npPotenc <> 0	
						aCols[n][nPPotenc] := SB8->B8_POTENCI
					EndIf
				EndIf
			EndIf
			If nPProvEnt <> 0 
		 		aCols[N][nPProvEnt] := (cAliasTRB)->D2_PROVENT
		 	Endif

			If ("_NFORI"$cReadVar)
				&(cReadVar) := (cAliasTRB)->D2_DOC
			EndIf

			If ("_SERIORI"$cReadVar)
				&(cReadVar) := (cAliasTRB)->D2_SERIE
			EndIf

			If ("_ITEMORI"$cReadVar)
				&(cReadVar) := (cAliasTRB)->D2_ITEM
			EndIf
					
			nRecSD2	:= (cAliasTRB)->D2_REC_WT
				
		EndIf
	Else
		HELP(" ",1,"F4NAONOTA")
	EndIf
	//���������������������������������������������������������������������Ŀ
	//� Restaura a integridade da rotina                                    �
	//�����������������������������������������������������������������������
	dbSelectArea(cAliasTRB)
	//��������������������������������������������������������������������Ŀ
	//�ARQUIVO TEMPORARIO DE MEMORIA (CTREETMP)                            �
	//�A funcao MSCloseTemp ira substituir a linha de codigo abaixo:       �
	//|--> dbCloseArea()                                                   |
	//����������������������������������������������������������������������	
	FWCloseTemp(cAliasTRB,cNomeTrb)
EndIf
dbSelectArea("SA1")
RestArea(aArea)
SetFocus(nHdl)
Return(lRetorno)
