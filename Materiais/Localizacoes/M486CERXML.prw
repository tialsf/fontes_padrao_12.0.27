#Include "Protheus.ch"
#Include "rwmake.ch"
#Include "topconn.ch"

/*/苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噮o    矼486CERXML  � Autor � Dora Vega             � Data � 03.07.17 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Generacion de XML para certificado de retencion de Peru,     潮�
北�          � de acuerdo a esquema estandar UBL 2.0 para ser enviado a TSS 潮�
北�          � para su envio a la SUNAT. (PER)                              潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � M486CERXML(cFil,cNumCer,cNumDoc,cSerie,cProv,cLoja)          潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� cFil .- Sucursal que emitio el documento.                    潮�
北�          � cNumCer .- Numero de Certificado.                            潮�
北�          � cNumDoc .- Numero de documento.                              潮�
北�          � cSerie .- Numero o Serie del Documento.                      潮�
北�          � cProv .- Codigo del Proveedor .                              潮�
北�          � cLoja .- Codigo de la tienda del cliente.                    潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � MATA486                                                      潮�
北媚哪哪哪哪哪哪穆哪哪哪哪履哪哪哪哪哪履哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅rogramador   � Data   � BOPS/FNC  �  Motivo da Alteracao                潮�
北媚哪哪哪哪哪哪呐哪哪哪哪拍哪哪哪哪哪拍哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北矹onathan Glz  �31/08/17矰MINA-38   砈e modifica funcion fGenXMLCER para  潮�
北�              �        �           硁egenerar de manera correcta el nodo 潮�
北�              �        �           砪on se pondra la firma digital. 		潮�
北矨ndres S.  	 �06/02/20矰MINA-7985 砈e modifica funcion M486CERXML para  潮�
北�              �        �           硄ue genere un solo XML por numero de 潮�
北�              �        �           砪ertificado (PER) 					潮�
北滥哪哪哪哪哪哪牧哪哪哪哪聊哪哪哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�/*/
Function M486CERXML(cFil,cNumCer, cNumDoc, cSerie, cProv, cLoja) 
	Local cXML       := ""
	Local aEncab := {}	
	Local aDocRef :={}
	Local aArea 	:= getArea()
	Local nTamCer 	:= TamSX3("FE_NROCERT")[1]
	Local nTotalPa := 0
	Local cAliq := ""
	Local nValimp := 0
	Local cFilSFE := xFilial("SFE")
    Local lPros := .T.
    Local cMoneda := ""
	
	Private cFecha := ""
    
	dbSelectArea("SFE") 
	SFE ->(dbSetORder(1))//FE_FILIAL+FE_NROCERT+STR(FE_ITEM,3)
	If SFE->(dbSeek(cFilSFE + cNumCer)) 
		Do While SFE->(!EOF()) .AND. SFE->(FE_FILIAL+FE_NROCERT) == cFilSFE + cNumCer
			
			cFolio := RTRIM(SFE->FE_SERIE2)  + "-" + STRZERO(VAL(SFE->FE_NROCERT),8)
			cFecha := Alltrim(Str(YEAR(SFE->FE_EMISSAO))) + "-" + Padl(Alltrim(Str(MONTH(SFE->FE_EMISSAO))),2,'0') + "-" +;
			Padl(Alltrim(Str(DAY(SFE->FE_EMISSAO))),2,'0')
			cAliq := SFE->FE_ALIQ
			cMoneda   := Alltrim(M486VALSX5("XQ1")) 
			nTotalPa := SFE->FE_VALBASE - SFE->FE_VALIMP			
			nValimp := SFE->FE_VALIMP
			
			If lPros
				aEncab := {cFolio,cFecha,cAliq,nValimp,nTotalPa,cMoneda}
				lPros := .F.					
			Else
				aEncab[4] += nValimp
				aEncab[5] += nTotalPa 
			EndIf
								
			//Comprobante Relacionado  
			M486RETFAC(SFE->FE_NFISCAL,SFE->FE_SERIE, @aDocRef)			
				
			SFE->(dbSkip())	
		EndDo
		//Genera XML
		cXML := fGenXMLCER(cProv,cLoja,aEncab,aDocRef)
	EndIf

	RestArea(aArea)	
Return cXML

/*苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噮o    � fGenXMLCER � Autor � Dora Vega             � Data � 03.07.17 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Genera estructura XML para certificado de retencion de       潮�
北�          � acuerdo al estandar UBL 2.0 (PERU)                           潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � fGenXMLCER(cProv,cLoja,aEncab,aDocRef)                       潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� cProv .- Codigo del Proveedor .                              潮�
北�          � cLoja .- Codigo de la tienda del cliente.                    潮�
北�          � aEncab .- Datos de encabezado del documento.                 潮�
北�          � aDocRef .- Datos de identificacion del Documento.            潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砇etorno   � cXML.-String de estructrura de XML certificado de retenciones潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � M486CERXML                                                   潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�*/
Static Function fGenXMLCER(cProv,cLoja,aEncab,aDocRef)
	Local cXML  := ""
	Local nI		:= 0
	Local cCRLF	 := (chr(13)+chr(10))

	cXML := '<?xml version="1.0" encoding="iso-8859-1" standalone="no"?>' + cCRLF
	cXML += '<Retention' + cCRLF 
	cXML += '	xmlns="urn:sunat:names:specification:ubl:peru:schema:xsd:Retention-1"' + cCRLF 
	cXML += '	xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"' + cCRLF 
	cXML += '	xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"' + cCRLF 
	cXML += '	xmlns:ccts="urn:un:unece:uncefact:documentation:2"' + cCRLF 
	cXML += '	xmlns:ds="http://www.w3.org/2000/09/xmldsig#" ' + cCRLF
	cXML += '	xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"' + cCRLF 
	cXML += '	xmlns:qdt="urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2"' + cCRLF
	cXML += '	xmlns:sac="urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1" ' + cCRLF
	cXML += '	xmlns:udt="urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2"' + cCRLF
	cXML += '	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' + cCRLF

	//Adicionales
	cXML += '	<ext:UBLExtensions>' + cCRLF
    cXML += '		<ext:UBLExtension>' + cCRLF
	cXML += '			<ext:ExtensionContent></ext:ExtensionContent>' + cCRLF  
    cXML += '		</ext:UBLExtension>' + cCRLF
    cXML += '	</ext:UBLExtensions>' + cCRLF	
    
    //Identificacion del Documento
    cXML += '	<cbc:UBLVersionID>2.0</cbc:UBLVersionID>' + cCRLF
	cXML += '	<cbc:CustomizationID>1.0</cbc:CustomizationID>' + cCRLF

	//Firma Electronica
	cXML += M486XmlFE() 
	
	cXML += '	<cbc:ID>' + aEncab[1] + '</cbc:ID>' + cCRLF  
	cXML += '	<cbc:IssueDate>' + aEncab[2] + '</cbc:IssueDate>' + cCRLF 
	
	//Emisor
	cXML += M486EMICER() 
	
	//Receptor
	cXML += M486RECCER(cProv,cLoja) 
	
	cXML += '	<sac:SUNATRetentionSystemCode>01</sac:SUNATRetentionSystemCode>' + cCRLF
	cXML += '	<sac:SUNATRetentionPercent>' + alltrim(TRANSFORM(aEncab[3],"999999.99")) + '</sac:SUNATRetentionPercent>' + cCRLF
	cXML += '	<cbc:Note/>' + cCRLF
	cXML += '	<cbc:TotalInvoiceAmount currencyID="'+ aEncab[6] +'">' + alltrim(TRANSFORM(aEncab[4],"999999.99")) + '</cbc:TotalInvoiceAmount>' + cCRLF
	cXML += '	<sac:SUNATTotalPaid currencyID="'+ aEncab[6] +'">' + alltrim(TRANSFORM(aEncab[5],"999999.99")) + '</sac:SUNATTotalPaid>' + cCRLF
		
	For nI := 1 to len(aDocRef)

		cXML += '	<sac:SUNATRetentionDocumentReference>' + cCRLF
		cXML += '		<cbc:ID schemeID="01">' + aDocRef[nI,1] + '</cbc:ID>' + cCRLF
		cXML += '		<cbc:IssueDate>' + aDocRef[nI,3] + '</cbc:IssueDate>' + cCRLF
		cXML += '		<cbc:TotalInvoiceAmount currencyID="'+ aDocRef[nI,2] +'">' + alltrim(TRANSFORM(aDocRef[nI,4],"999999.99")) + '</cbc:TotalInvoiceAmount>' + cCRLF
		cXML += '		<cac:Payment>' + cCRLF
		cXML += '			<cbc:ID>01</cbc:ID>' + cCRLF
		cXML += '			<cbc:PaidAmount currencyID="'+ aDocRef[nI,2] +'">' + alltrim(TRANSFORM(aDocRef[nI,5],"999999.99")) + '</cbc:PaidAmount>' + cCRLF
		cXML += '			<cbc:PaidDate>' + aDocRef[nI,3] + '</cbc:PaidDate>' + cCRLF
		cXML += '		</cac:Payment>' + cCRLF
		cXML += '		<sac:SUNATRetentionInformation>' + cCRLF
		cXML += '			<sac:SUNATRetentionAmount currencyID="'+ aDocRef[nI,2] +'">' + alltrim(TRANSFORM(aDocRef[nI,6],"999999.99")) + '</sac:SUNATRetentionAmount>' + cCRLF
		cXML += '			<sac:SUNATRetentionDate>' + aEncab[2] + '</sac:SUNATRetentionDate>' + cCRLF
		cXML += '			<sac:SUNATNetTotalPaid currencyID="'+ aDocRef[nI,2] +'">' + alltrim(TRANSFORM(aDocRef[nI,7],"999999.99")) + '</sac:SUNATNetTotalPaid>' + cCRLF
		cXML += ' 		<cac:ExchangeRate>' + cCRLF
		cXML += ' 			<cbc:SourceCurrencyCode>' + aDocRef[nI,2] + '</cbc:SourceCurrencyCode>' + cCRLF
		cXML += ' 			<cbc:TargetCurrencyCode>' + aDocRef[nI,2] + '</cbc:TargetCurrencyCode>' + cCRLF
		cXML += ' 			<cbc:CalculationRate>' + aDocRef[nI,8] + '</cbc:CalculationRate>' + cCRLF
		cXML += ' 			<cbc:Date>' + aDocRef[nI,3] + '</cbc:Date>' + cCRLF
		cXML += ' 		</cac:ExchangeRate>' + cCRLF
		cXML += '		</sac:SUNATRetentionInformation>' + cCRLF
		cXML += '	</sac:SUNATRetentionDocumentReference>' + cCRLF
	
	Next nI
	
	cXML += '</Retention>' + cCRLF

Return cXML

/*苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噮o    � M486RETFAC � Autor � Dora Vega             � Data � 06.07.17 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Obtiene los datos del Comprobante Relacionado                潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � M486RETFAC(cNumDoc, cSerie, aDocRef)                         潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� cNumDoc .- Numero de documento.                              潮�
北�          � cSerie .- Numero o Serie del Documento.                      潮�
北�          � aDocRef .- Datos de identificacion del Documento.            潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砇etorno   �                                                              潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � M486CERXML                                                   潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�*/
Static Function M486RETFAC(cNumDoc, cSerie, aDocRef)
	Local cMoneda := ""
	Local cTasMon := ""
	Local cFolio := ""
	Local cFecha := ""
	Local nValBr := 0 
	Local nTamDoc 	:= TamSX3("F1_DOC")[1]
	Local nTamMon 	:= TamSX3("F1_TXMOEDA")[1]
	Local nValCob := 0
	Local nValIRE := 0
	Local nResTot := 0


	
	dbSelectArea("SF1")
	dbSetOrder(1) //F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENTE+F2_LOJA+F2_FORMUL+F2_TIPO
	If dbSeek(xfilial("SF1") + cNumDoc + cSerie) 
		cMoneda   := Alltrim(M486VALSX5("XQ1"))
		cFolio := RTRIM(SF1->F1_SERIE2)  + "-" + STRZERO(VAL(SF1->F1_DOC),8) 
		cFecha := Substr(dTos(SF1->F1_EMISSAO),0,4) + "-" + Substr(dTos(SF1->F1_EMISSAO),5,2) + "-" +;
		Substr(dTos(SF1->F1_EMISSAO),7,2)	
		nValBr := SF1->F1_VALBRUT
		nValCob := SFE->FE_VALBASE
		nValIRE := SFE->FE_VALIMP
		nResTot := nValCob - nValIRE
		cTasMon := LTRIM(STR(SF1->F1_TXMOEDA,nTamMon,2))
		
		aAdd(aDocRef ,{cFolio,cMoneda,cFecha,nValBr,nValCob,nValIRE,nResTot,cTasMon})
	EndIf
Return NIL

/*苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噮o    � M486EMICER � Autor � Dora Vega             � Data � 03.07.17 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Genera estructura de Emisor para XML de acuerdo al estandar  潮�
北�          � UBL 2.0 (PERU)                                               潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � M486EMICER()                                                 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� No aplica.                                                   潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砇etorno   � cXMLEmi .- Nodo de emisor para XML de estandar UBL 2.0       潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � M486CERXML                                                   潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�*/
Static Function M486EMICER()
	Local cXMLEmi := ""
	Local cCRLF	 := (chr(13)+chr(10))
	cXMLEmi += '	<cac:AgentParty>' + cCRLF
	cXMLEmi += '		<cac:PartyIdentification>' + cCRLF 
	cXMLEmi += '			<cbc:ID schemeID="6">' + RTRIM(SM0->M0_CGC) + '</cbc:ID>' + cCRLF 
	cXMLEmi += '		</cac:PartyIdentification>' + cCRLF 
	cXMLEmi += '		<cac:PartyName>' + cCRLF
	cXMLEmi += '			<cbc:Name><![CDATA[' + RTRIM(SM0->M0_NOMECOM) + ']]></cbc:Name>' + cCRLF 
	cXMLEmi += '		</cac:PartyName>' + cCRLF
	cXMLEmi += '		<cac:PostalAddress>' + cCRLF
	cXMLEmi += '			<cbc:ID>' + RTRIM(SM0->M0_CEPENT) + '</cbc:ID>' + cCRLF
	cXMLEmi += '           	<cbc:StreetName><![CDATA[' + RTRIM(SM0->M0_ENDENT) + ']]></cbc:StreetName>' + cCRLF
	cXMLEmi += '           	<cbc:CitySubdivisionName><![CDATA[' + RTRIM(SM0->M0_CIDENT) + ']]></cbc:CitySubdivisionName>' + cCRLF
	cXMLEmi += '           	<cbc:CityName><![CDATA[' + RTRIM(SM0->M0_CIDENT) + ']]></cbc:CityName>' + cCRLF
	cXMLEmi += '           	<cbc:CountrySubentity><![CDATA[' + RTRIM(SM0->M0_CIDENT) + ']]></cbc:CountrySubentity>' + cCRLF
	cXMLEmi += '			<cbc:District><![CDATA[' + RTRIM(SM0->M0_BAIRENT) + ']]></cbc:District>' + cCRLF
	cXMLEmi += '			<cac:Country>' + cCRLF
	cXMLEmi += '				<cbc:IdentificationCode>PE</cbc:IdentificationCode>' + cCRLF 
	cXMLEmi += '			</cac:Country>' + cCRLF
	cXMLEmi += '		</cac:PostalAddress>' + cCRLF
	cXMLEmi += '		<cac:PartyLegalEntity>' + cCRLF
	cXMLEmi += '			<cbc:RegistrationName><![CDATA[' + RTRIM(SM0->M0_NOME) + ']]></cbc:RegistrationName>' + cCRLF
	cXMLEmi += ' 		</cac:PartyLegalEntity>' + cCRLF
	cXMLEmi += '	</cac:AgentParty>' + cCRLF
Return cXMLEmi 

/*苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噮o    � M486RECCER � Autor � Dora Vega             � Data � 03.07.17 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Genera estructura de Receptor para XML de acuerdo al estandar潮�
北�          � UBL 2.0 (PERU)                                               潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � M486RECCER(cProv,cLoja)                                      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� cProv.- Clave del Proveedor                                  潮�
北�          � cLoja.- Clave de tienda del proveedor                        潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砇etorno   � cXMLRec .- Nodo de receptor para XML de estandar UBL 2.0     潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � M486CERXML                                                   潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�*/
Static Function M486RECCER(cProv,cLoja)
	Local cXMLRec := ""
	Local cCRLF	   := (chr(13)+chr(10))
	Local aArea   := getArea()
	Local cPais   := ""
		
	//Receptor (Proveedores)
	dbSelectArea("SA2")
	SA2->(dbSetOrder(1)) //A2_FILIAL+A2_COD+A2_LOJA
	If SA2->(dbSeek(xFilial("SA2") + cProv + cLoja)) 			
		dbSelectArea("SYA")
		SYA->(dbSetOrder(1)) //YA_FILIAL+YA_CODGI
		If SYA->(dbSeek(xFilial("SYA") + SA2->A2_PAIS))
			cPais := SYA->YA_CODERP
		EndIf
		cXMLRec += '	<cac:ReceiverParty>' + cCRLF
		cXMLRec += '		<cac:PartyIdentification>' + cCRLF
		cXMLRec += '			<cbc:ID schemeID="6">' + RTRIM(SA2->A2_CGC) + '</cbc:ID>' + cCRLF
		cXMLRec += '		</cac:PartyIdentification>' + cCRLF
		cXMLRec += '		<cac:PartyName>' + cCRLF
		cXMLRec += '			<cbc:Name><![CDATA[' + RTRIM(SA2->A2_NOME) + ']]></cbc:Name>' + cCRLF 
		cXMLRec += '		</cac:PartyName>' + cCRLF
		cXMLRec += '		<cac:PostalAddress>' + cCRLF
		cXMLRec += '			<cbc:ID>' + RTRIM(SA2->A2_CEP) + '</cbc:ID>' + cCRLF
		cXMLRec += '			<cbc:StreetName><![CDATA[' + RTRIM(SA2->A2_END) + ']]></cbc:StreetName>' + cCRLF
		cXMLRec += '           	<cbc:CitySubdivisionName><![CDATA[' + RTRIM(SM0->M0_CIDENT) + ']]></cbc:CitySubdivisionName>' + cCRLF
		cXMLRec += '			<cbc:CityName><![CDATA[' + RTRIM(SA2->A2_MUN) + ']]></cbc:CityName>' + cCRLF
		cXMLRec += '           	<cbc:CountrySubentity><![CDATA[' + RTRIM(SM0->M0_CIDENT) + ']]></cbc:CountrySubentity>' + cCRLF
		cXMLRec += '			<cbc:District><![CDATA[' + RTRIM(SA2->A2_BAIRRO) + ']]></cbc:District>' + cCRLF
		cXMLRec += '			<cac:Country>' + cCRLF
		cXMLRec += '				<cbc:IdentificationCode>' + TRIM(cPais) + '</cbc:IdentificationCode>' + cCRLF
		cXMLRec += '			</cac:Country>' + cCRLF
		cXMLRec += '		</cac:PostalAddress>' + cCRLF
		cXMLRec += '		<cac:PartyLegalEntity>' + cCRLF
		cXMLRec += '			<cbc:RegistrationName><![CDATA[' + RTRIM(SA2->A2_NOME) + ']]></cbc:RegistrationName>' + cCRLF
		cXMLRec += '		</cac:PartyLegalEntity>' + cCRLF
		cXMLRec += '	</cac:ReceiverParty>' + cCRLF	
	EndIf
	RestArea(aArea)
Return cXMLRec