#INCLUDE "protheus.ch"
#include "FILEIO.CH"
#INCLUDE "rwmake.ch"
#INCLUDE "M486XFUNBO.CH"

/*/{Protheus.doc} M486XFUNBOL
Comunicaci�n con el WS para transmisi�n y consulta
@author alfredo.medrano
@since 11/10/2019
@version 1.0
@param aFact, array, Array con los documentos de los cuales se generar�n los XML
@param aError, array, Array con Errores 
@param aTrans, array, Array con los documentos que han sido transmitidos 
@param cTipTrans, string, String con el tipo de transacci�n T= transmisi�n, E= Consulta
@param aRetObs, array, Array con las observaciones (generadas por la consulta de la transmisi�n)
@return lRet, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Function M486XFUNBOL(aFact, aError, aTrans, cTipTrans, aRetObs, lRetSal)
	Local lRet := .T.
	Local cParametros := ""
	Local cMVCFDIAMB:= SuperGetMV("MV_CFDIAMB",,"") //cod Ambiente 2 
	Local cMVSISTEMA:= SuperGetMV("MV_SINSIS",,"") //cod Sistema
	Local cMVNUMSUC	:= SuperGetMV("MV_NUMSUC",,0) 	//cod Sucursal
	Local cMVCUIS   := SuperGetMV("MV_CUIS",,"")	//cuis
	Local cMVNIT    := alltrim(SM0->M0_CGC)	//nit
	Local cMV814PATH:= SuperGetMV("MV_PATH814 ",,"")//Direccion donde se guardaran los archivos csv
	Local cRutaSMR	:= &(SuperGetMV("MV_CFDSMAR",,""))//ruta donde reside el cliente de WS 
	Local cMVCFDIPA := SuperGetMV("MV_CFDI_PA",,"") //Nombre del ejecutable del servicio web a utiliza.   
	Local cPath 	:= &(SuperGetMV("MV_CFDDOCS",,""))  
	Local cMVMODAL	:= SuperGetMV("MV_SINMOD",,"") //Modalidad 1=Electr�nica, 2=Computarizada, 3=Manual, 4=Prevalorada Electr�nica, 5=Prevalorada Computarizada
	Local cCUFD     := SuperGetMV("MV_CUFD",,"") //CUFD
	Local cCUFDFEC	:= SuperGetMV("MV_CUFDFEC",,"") // Fecha vigencia cufd
	Local cMVCFDIPX := SuperGetMV("MV_CFDI_PX",,"") //Archivo certificado pfx
	Local cMVCFDICVE:= SuperGetMV("MV_CFDICVE",,"") // Clave de llave privada.
	Local cMVEMISION:= SuperGetMV("MV_SINEMI",,"1") //1=Online, 2=Offline
	Local cFilTab   := IIf(Alltrim(cEspecie) $ "NF|NDC", xFilial("SF2"), xFilial("SF1"))
	local cMsg 		:= ""
	Local cCodigo 	:= ""
	Local cDescrip 	:= ""
	local cNomArch 	:= ""
	Local cError 	:= ""
	Local cCatOk 	:= ""
	Local cWarning 	:= ""
	Local lHasError := .F.
	Local cCodTab 	:= ""
	Local cCodEst 	:= ""
	Local cCodRes 	:= ""
	Local nX		:= 0 	
	Local nOpc 		:= 0
	Local nHandle 	:= 0
	Local nRet		:= 0
	Local nI 		:= 0
	Local nT		:= 0
	Local cCUF		:= ""
	Local cFechTrns := ""
	Local cHoraTrns := ""
	Local aTrErr	:= {}
	Local cNumDto  	:=iif(AllTrim(cEspecie) =="NF", '1',iif(AllTrim(cEspecie) =="NCC", '4', iif(AllTrim(cEspecie) =="NDC", '5', '0')) )
	Local cCodDocF 	:= "0"
	Local cPuntoVen := "0"
	Local cSeriePe 	:= MV_PAR01
	Local lRetSal	:= .T.
	/* array aCodEst
	0   = no transmitida.                        0    BR_CINZA
	901 = Recepci�n Pendiente.                   4    BR_AMARELO
	902 = Recepci�n Rechazada.                   5    BR_VERMELHO
	903 = Recepci�n Procesada.                   1    BR_AZUL
	904 = Recepci�n Observada.                   5    BR_VERMELHO
	905 = Anulaci�n Confirmada.                  8    BR_LARANJA
	906 = Anulaci�n Rechazada.                   9    BR_PRETO
	907 = Anulaci�n Pendiente de Confirmaci�n.   7    BR_PINK
	908 - Recepci�n Validada.                    6    BR_VERDE
	*/
	Private aCodEst := {{'901','4'},{'902','5'},{'903','1'},{'904','5'},{'905','8'},{'906','9'},{'907','7'},{'908','6'}}
	Private cTipT 	:= cTipTrans
	Private aCabsSF1  := {}
	Private aCabsSF2  := {}
	Private aItensSD1 := {}
	Private aItensSD2 := {}
	default aRetObs	:= {}
	default aError  := {} 
	default aTrans  := {} 
	

	dbSelectArea("SFP")
	SFP->(dbSetOrder(5))//FP_FILIAL+FP_FILUSO+FP_SERIE+FP_ESPECIE+FP_PV
	If SFP->(MsSeek(xFilial("SFP") + cFilAnt + cSeriePe + cNumDto))
		cCodDocF := SFP->FP_DOCFIS
		cPuntoVen:= SFP->FP_PV
	EndIf
	
	// Obtener arreglos de campos de SF1/SF2/SD1/SD2
	M486SX3(@aCabsSF1, @aCabsSF2, @aItensSD1, @aItensSD2)
	
	cParametros := alltrim(cTipT) + " "
	/*If cTipT == "T"
		cParametros := "T "//Opci�n para transmisi�n
	ElseIf cTipT == "E"
		cParametros := "E "//Opci�n para validacion 
	ElseIf cTipT == "C"
		cParametros := "C "//Opci�n para anulacion
	EndIf*/
	cParametros += cMVCFDIAMB + " "							
	cParametros += cMVSISTEMA + " "							
	cParametros += alltrim(STR(cMVNUMSUC)) + " "							            
	cParametros += cMVCUIS + " "						            
	cParametros += cMVNIT + " "							        						        
	cParametros += cMV814PATH + " "	
	cParametros += cRutaSMR	 + " " 
	cParametros += cMVMODAL	 
	
	If Empty(cCUFD)
		cParametros += " X"
	Else
		cParametros += " " + cCUFD	 
	EndIf
	If Empty(cCUFDFEC)
		cParametros += " X"	
	Else
		cParametros += " " + Alltrim(substr(cCUFDFEC,1,10) + "-" + substr(cCUFDFEC,12,8))
	EndIf
	
	cParametros += " " + cMVCFDIPX + " "
	cParametros += cMVCFDICVE + " "
	cParametros += cCodDocF + " "	
	cParametros += cMVEMISION + " "
	cParametros += "1 "
	cParametros += alltrim(cEspecie) + " "
	cParametros += alltrim(cPuntoVen)
	
	If cTipT == "T"
		cMsg := STR0001 + chr(13)+ Chr(10)//"Confirma la transmici�n de los documentos ?"
	ElseIf cTipT == "E"
		cMsg := STR0002 + chr(13)+ Chr(10)//"Confirma la validaci�n de la recepci�n de documentos ?"
	ElseIf cTipT == "C"
		cMsg := STR0036 + chr(13)+ Chr(10)// "Confirma la anulaci�n de los documentos seleccionados?" 
	EndIf
	 			        
	If MsgYESNO(cMsg)
	CursorWait()
		aRet	:= {}
		nOpc := WAITRUN( cRutaSMR + cMVCFDIPA + ".exe " + cParametros, 0 )	// Se ejecuta exe	
		If nOpc == 0
			// Lee xml de cufd para actualizar los parametros MV_CUFD y MV_CUFDFEC
			If File(cRutaSMR  + "cufd.xml.out")
				// Procesar archivo out
				CpyT2S(cRutaSMR  + "cufd.xml.out" , cPath)
				oXMLResp := XmlParserFile(EncodeUtf8(cPath + "cufd.xml.out"), "_", @cError, @cWarning )
				If oXMLResp <> Nil
					lHasError := "True" $ oXMLResp:_RESPONSES:_RESPONSE:_HASERROR:TEXT
					If lHasError // Si el .out report� error		
						cCodigo := oXMLResp:_RESPONSES:_RESPONSE:_EXCEPTION:_CODE:TEXT			
						cError 	+= STR0003  + cCodigo + chr(13)+ Chr(10) //"Error al generar el cufd, Cod Respuesta: "
						cDescrip := ObtColSAT('S012',Alltrim(cCodigo),1,4,5,120)
						aAdd(aError, {"","","","", cCodigo +" "+ ALLTRIM(cDescrip)})	
						lRet := .F.
					Else 
						cCodigo	:= oXMLResp:_RESPONSES:_RESPONSE:_MESSAGE:_CODCUFD:TEXT
						cDataTim:= ALLTRIM(oXMLResp:_RESPONSES:_RESPONSE:_MESSAGE:_DATE:TEXT)
						If cCUFD != cCodigo .or. cCUFDFEC != cDataTim
							PutMvPar("MV_CUFD",cCodigo)
							PutMvPar("MV_CUFDFEC",cDataTim)
						EndIf
					EndIf
				EndIf	
				// Se eliminan archivos de la carpeta del smartclient
				Ferase( cRutaSMR  + "cufd.xml.out" )
			/*Else
				aAdd(aError, {"","","","", STR0053})//"000 No se gener� el archivo .out del CUFD"
				lRet := .F.*/
			EndIf
			
			If lRet
				For nI:= 1 to len(aFact)
					cDocXml := alltrim(aFact[nI][1]) + alltrim(aFact[nI][2]) + iif( cTipT == "C", "Anula", alltrim(cEspecie)) + ".xml"
					cDocto := cDocXml + ".out"
					cFilTab := Iif(cTipT == "E", aFact[nI][6],cFilTab)
					If File(cRutaSMR  + cDocXml)
						CpyT2S(cRutaSMR  + cDocXml, cPath)
					EndIf
					If File(cRutaSMR  + cDocto)
					 IncProc(STR0038 + cDocto)//"Procesando Documentos:"
						CpyT2S(cRutaSMR  + cDocto, cPath)
						oXMLResp := XmlParserFile(EncodeUtf8(cPath + cDocto), "_", @cError, @cWarning )
						If oXMLResp <> Nil
							cCodEst := oXMLResp:_RESPUESTA:_CODIGOESTADO:TEXT
							cDescrip := ObtColSAT('S012',cCodEst,1,4,5,120)
							
							If cTipT == "E"
								nT := ascan(aCodEst,{|x| x[1]= cCodEst})
								If  nT > 0
									aAdd(aRet, {val(aCodEst[nT][2]),;
									Alltrim(aFact[nI,1]) + "-" + aFact[nI,2],;
									IIf(cMVCFDIAMB == "1", STR0004, STR0005),;//"Producci�n" // "Pruebas"
									dDataBase,;
									cCodEst,; //Cod. Estado
									cDescrip,;
									"", ;
									aFact[nI,2], ; //Documento
									aFact[nI,1], ; //Serie
									aFact[nI,3], ; //Cliente
									aFact[nI,4], ; //Loja
									""})  
								EndIf
							EndIf
							
							lHasError := "true" $ oXMLResp:_RESPUESTA:_TRANSACCION:TEXT
							If lHasError // Si el .out reporta envio ok
								cCUF := ""
								cFechTrns := ""
								cHoraTrns := ""
								If XmlChildEx(oXMLResp:_RESPUESTA, "_CUF") <> Nil
									cCUF := oXMLResp:_RESPUESTA:_CUF:TEXT
								EndIf
								If XmlChildEx(oXMLResp:_RESPUESTA, "_FECHA") <> Nil
									cFechTrns := oXMLResp:_RESPUESTA:_FECHA:TEXT
								EndIf
								If XmlChildEx(oXMLResp:_RESPUESTA, "_HORA") <> Nil
									cHoraTrns := oXMLResp:_RESPUESTA:_HORA:TEXT
								EndIf
								cCodRes := oXMLResp:_RESPUESTA:_CODIGORECEPCION:TEXT
								aAdd(aTrans, {cFilTab,aFact[nI,1],aFact[nI,2],aFact[nI,3],aFact[nI,4],cCodEst,cCodRes,cCUF,cFechTrns,cHoraTrns})
								aAdd(aError, {aFact[nI,1],aFact[nI,2],aFact[nI,3], aFact[nI,4], cCodEst +" "+ ALLTRIM(cDescrip)})
							Else
								If cCodEst !="000"// error msg de visual Studio
									aAdd(aError, {aFact[nI,1],aFact[nI,2],aFact[nI,3], aFact[nI,4], cCodEst +" "+ ALLTRIM(cDescrip)})
									aAdd(aTrErr, {cFilTab,aFact[nI,1],aFact[nI,2],aFact[nI,3],aFact[nI,4],cCodEst,"",""})
								EndIF
								If ValType( oXMLResp:_RESPUESTA:_LISTACODIGOSRESPUESTAS) == "A" //Varios 
									For nX := 1 To Len(oXMLResp:_RESPUESTA:_LISTACODIGOSRESPUESTAS)
										cCodRes := oXMLResp:_RESPUESTA:_LISTACODIGOSRESPUESTAS[nX]:TEXT	
										cDescrip := ObtColSAT('S012',cCodRes,1,4,5,120)
										aAdd(aError, {aFact[nI,1],aFact[nI,2],aFact[nI,3], aFact[nI,4], cCodRes +" "+ ALLTRIM(cDescrip)})	
										If cTipT == "E"
											nT := ascan(aCodEst,{|x| x[1]= cCodEst})
											If  nT > 0
												aAdd(aRetObs, {val(aCodEst[nT][2]),;
												Alltrim(aFact[nI,1]) + "-" + aFact[nI,2],;
												IIf(cMVCFDIAMB == "1", STR0004, STR0005),;//"Producci�n" // "Pruebas"
												dDataBase,;
												cCodEst,; //Cod. Estado
												cCodRes +" - "+ ALLTRIM(cDescrip),;
												"", ;
												aFact[nI,2], ; //Documento
												aFact[nI,1], ; //Serie
												aFact[nI,3], ; //Cliente
												aFact[nI,4], ; //Loja
												""})  
											EndIf
										EndIf
									
									Next
								ElseIf ValType( oXMLResp:_RESPUESTA:_LISTACODIGOSRESPUESTAS) == "O"
										cCodRes := oXMLResp:_RESPUESTA:_LISTACODIGOSRESPUESTAS:TEXT	
										cDescrip := ObtColSAT('S012',cCodRes,1,4,5,120)
										aAdd(aError, {aFact[nI,1],aFact[nI,2],aFact[nI,3], aFact[nI,4], cCodRes +" "+ ALLTRIM(cDescrip)})	
										If cTipT == "E"
											nT := ascan(aCodEst,{|x| x[1]= cCodEst})
											If  nT > 0
												aAdd(aRetObs, {val(aCodEst[nT][2]),;
												Alltrim(aFact[nI,1]) + "-" + aFact[nI,2],;
												IIf(cMVCFDIAMB == "1", STR0004, STR0005),;//"Producci�n" // "Pruebas"
												dDataBase,;
												cCodEst,; //Cod. Estado
												cCodRes +" - "+ ALLTRIM(cDescrip),;
												"", ;
												aFact[nI,2], ; //Documento
												aFact[nI,1], ; //Serie
												aFact[nI,3], ; //Cliente
												aFact[nI,4], ; //Loja
												""})  
											EndIf
										EndIf
								EndIf
								/////////////LISTADESCRIPCIONESRESPUESTAS //////////////////////
								If XmlChildEx(oXMLResp:_RESPUESTA, "_LISTADESCRIPCIONESRESPUESTAS") <> Nil
									If ValType( oXMLResp:_RESPUESTA:_LISTADESCRIPCIONESRESPUESTAS) == "A" //Varios impuestos
										For nX := 1 To Len(oXMLResp:_RESPUESTA:_LISTADESCRIPCIONESRESPUESTAS)
											cDescrip := oXMLResp:_RESPUESTA:_LISTADESCRIPCIONESRESPUESTAS[nX]:TEXT	
											If cTipT == "C" // Solicitud de anulaci�n
												aAdd(aRetObs, {aFact[nI,1],aFact[nI,2],aFact[nI,3], aFact[nI,4], cCodRes +" "+ ALLTRIM(cDescrip)})	
											EndIf
											If cTipT == "E" 
												nT := ascan(aCodEst,{|x| x[1]= cCodEst})
												If  nT > 0
													aAdd(aRetObs, {val(aCodEst[nT][2]),;
													Alltrim(aFact[nI,1]) + "-" + aFact[nI,2],;
													IIf(cMVCFDIAMB == "1", STR0004, STR0005),;//"Producci�n" // "Pruebas"
													dDataBase,;
													cCodEst,; //Cod. Estado
													cDescrip,;
													"", ;
													aFact[nI,2], ; //Documento
													aFact[nI,1], ; //Serie
													aFact[nI,3], ; //Cliente
													aFact[nI,4], ; //Loja
													""})  
												EndIf
											EndIf
										
										Next
									ElseIf ValType( oXMLResp:_RESPUESTA:_LISTADESCRIPCIONESRESPUESTAS) == "O"
											cDescrip := oXMLResp:_RESPUESTA:_LISTADESCRIPCIONESRESPUESTAS:TEXT	
											If cTipT == "T" .AND. cCodEst =="000"// error msg de visual Studio
												aAdd(aError, {aFact[nI,1],aFact[nI,2],aFact[nI,3], aFact[nI,4], cCodEst +" "+ ALLTRIM(cDescrip)})	
											EndIf
											If cTipT == "C" // Solicitud de anulaci�n
												aAdd(aRetObs, {aFact[nI,1],aFact[nI,2],aFact[nI,3], aFact[nI,4], cCodRes +" "+ ALLTRIM(cDescrip)})	
											EndIf
											If cTipT == "E"
												nT := ascan(aCodEst,{|x| x[1]= cCodEst})
												aAdd(aRetObs, {iif( nT > 0, val(aCodEst[nT][2]),'0'),;
												Alltrim(aFact[nI,1]) + "-" + aFact[nI,2],;
												IIf(cMVCFDIAMB == "1", STR0004, STR0005),;//"Producci�n" // "Pruebas"
												dDataBase,;
												cCodEst,; //Cod. Estado
												cDescrip,;
												"", ;
												aFact[nI,2], ; //Documento
												aFact[nI,1], ; //Serie
												aFact[nI,3], ; //Cliente
												aFact[nI,4], ; //Loja
												""})  
											EndIf
									EndIf
								EndIf
							EndIf
							
						EndIf
						
						// Se eliminan archivos de la carpeta del smartclient .out, .xml y .gz
						Ferase( cRutaSMR  + cDocto )
						If File(cRutaSMR  + cDocXml)
							Ferase( cRutaSMR  + cDocXml)
						EndIf
						If File(cRutaSMR  + cDocXml + ".gz")
							Ferase( cRutaSMR  + cDocXml + ".gz")
						EndIf
					Else
						aAdd(aError, {aFact[nI,1],aFact[nI,2],aFact[nI,3], aFact[nI,4], STR0052})	//"000 No se gener� el archivo .out del documento."
					EndIf	
				Next
				
				If len(aTrans) > 0
					IncProc(STR0039)//"Actualizando Documentos..."
					M486UPBO(aTrans)
				EndIf
				If len(aTrErr) > 0
					M486UPBO(aTrErr)
				EndIf
				// Se eliminan archivos de la carpeta del smartclient .ini
				If File(cRutaSMR  + "listaxml.ini") .AND. cTipT == "T"
					Ferase( cRutaSMR  + "listaxml.ini") //ini de solicitud transmisi�n 
				EndIf
				If File(cRutaSMR  + "listaxmlC.ini") .AND. cTipT == "E"
					Ferase( cRutaSMR  + "listaxmlC.ini") //ini de consulta transmisi�n
				EndIf
				If File(cRutaSMR  + "listaxmlA.ini") .AND. cTipT == "C"
					Ferase( cRutaSMR  + "listaxmlA.ini") // ini de solicitud de anulaci�n 
					Ferase( cRutaSMR  + cDocto)
				EndIf
			EndIf 
		Endif
	CursorArrow()
	Else
		If cTipT == "T"
			For nI:= 1 to len(aFact)
				cDocXml := alltrim(aFact[nI][1]) + alltrim(aFact[nI][2]) +  alltrim(cEspecie) + ".xml"
				If File(cRutaSMR  + cDocXml)
					Ferase( cRutaSMR  + cDocXml) //ini de solicitud transmisi�n 
				EndIf
			Next
			If File(cRutaSMR  + "listaxml.ini") 
				Ferase( cRutaSMR  + "listaxml.ini") //ini de solicitud transmisi�n 
			EndIf
		ElseIf File(cRutaSMR  + "listaxmlC.ini") .AND. cTipT == "E"
			Ferase( cRutaSMR  + "listaxmlC.ini") //ini de consulta transmisi�n
		ElseIf File(cRutaSMR  + "listaxmlA.ini") .AND. cTipT == "C"
			Ferase( cRutaSMR  + "listaxmlA.ini") // ini de solicitud de anulaci�n 
		EndIf
		lRetSal := .F.
		lRet := .F.
		
	EndIf
Return lRet

/*/{Protheus.doc} M486BOLXML
Generaci�n de XML 
@type
@author alfredo.medrano
@since 07/10/2019
@version 1.0
@param aFact, array, Array con los documentos de los cuales se generar�n los XML
@param aError, array, Array con Errores 
@return lRet, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Function M486BOLXML(aFact,aError)
	Local nI := 0
	Local lRet := .F.
	Local aArcXML := {}
	Local nArch 
	Local cNomArc :=""
	Local cPath	:= &(SuperGetMV("MV_CFDDOCS",,"")) 
	Local cRutaSMR	:= &(SuperGetMV("MV_CFDSMAR",,""))//ruta donde reside el cliente de WS 
	Private SD2ORIG := "SD2ORIG"
	ProcRegua(len(aFact))	
	For nI := 1 to len(aFact)
		IncProc()
		lRet := CFDGerXML(cEspecie,aFact[nI,3],aFact[nI,4],aFact[nI,2],aFact[nI,1],.F.)
		aFact[nI,7] := lRet
		If !lRet // Si No se gener� XML, se agrega Error a Log 			
			aAdd(aError, {aFact[nI,1],aFact[nI,2],aFact[nI,3], aFact[nI,4], STR0013  }) // "Error al Generar XML"
		Else 
			cNomArc := lower(alltrim(aFact[nI,1])+alltrim(aFact[nI,2])+alltrim(cEspecie)) 
			AADD(aArcXML,cNomArc )	
			//Copia el archivo xml del servidor a la ruta del smartclient 
			CpyS2T(cPath + cNomArc + '.xml', cRUTASMR)		
		EndIf
	Next nI	
	
	If len(aArcXML) > 0 
	 	nArch:= FCREATE(cPath + "listaxml.ini")
	 	If nArch != -1
	 		fwrite(nArch, '[XML]' + chr(13)+ Chr(10))
	 		For nI:=1 to len(aArcXML)
				fwrite(nArch, aArcXML[nI] + '.xml' + chr(13)+ Chr(10))
			Next nI
		EndIf
		fclose(nArch)
		//Copia el archivo listaxml.ini del servidor a la ruta del smartclient 
		CpyS2T(cPath + "listaxml.ini", cRUTASMR)
	EndIf
	
Return lRet

/*/{Protheus.doc} M486BOLXML
Consulta de valores SF1 / SF2 para generar el XML
@type
@author alfredo.medrano
@since 07/10/2019
@version 1.0
@param cDoc, N�mero de documento
@param cSerie, Serie del documento
@param cClient, Cliente 
@param cLoja, Tienda
@param cTipTab, 1- Encabezado (SF1, SF2)  2- detalle (SD1, SD2)
@return aDatos
@example
(examples)
@see (links_or_references)
/*/
Function M486BOLDAT(cDoc, cSerie, cClient, cLoja, cTipTab)
	Local cAliasRet := CriaTrab(Nil, .F.)
	Local aDatos:= {}
	Local cFecha:= ""
	Local cQuery:= ""
	Local nCount:= 0
	Local cFilTab := ""
	Local cPrefx := ""
	Local cTabF := ""
	Local cCli := ""
	Local cPreUn := ""
	Local cBActiv := ""
	Local cBProdS := ""
	Local cBCod	  := ""
	Local cBDesc  := ""
	Local cSAHCO  := ""
	Local cPic := "99999999999999999999.9999"
	Local xmlOut := ""
	
	default cTipTab := "" // '1'- encabezado, '2'- detalle 
		
	If cDoc!="" .and. cSerie!="" .and.  cClient!="" .and.  cLoja!=""
	
		If Alltrim(cEspecie) $ "NF|NDC|NCC"
			cFilTab := xFilial("SF2")
		  	cCli := "_CLIENTE"
		  	cPreUn:="_PRCVEN"
		  	If cTipTab =='1'
		  		cPrefx := "F2"
		  		cTabF := "SF2"
		  	ElseIf cTipTab =='2'
		  		cPrefx := "D2"
		  		cTabF := "SD2"
		  	EndIf
		  Else
		   cPreUn:="_VUNIT"
		   	cFilTab := xFilial("SF1")
		   	cCli := "_FORNECE"
		   	If cTipTab =='1'
		   		cPrefx := "F1"
		   		cTabF := "SF1"
		  	ElseIf cTipTab =='2'
		  		cPrefx := "D1"
		  		cTabF := "SD1"
		  	EndIf
		EndIf
		
	 	If cTipTab =='1'
	 		cQuery += "SELECT " + cPrefx + "_CODDOC, " + cPrefx + "_FECTIMB, " + cPrefx + "_HORATRM, " + cPrefx + "_VALBRUT, " + cPrefx + "_VALMERC "
		ElseIf cTipTab =='2'
			cQuery += "SELECT " + cPrefx + "_UM, " + cPrefx + "_COD, " + cPrefx + "_QUANT, " + cPrefx + cPreUn + ", " + cPrefx +"_TOTAL "
		EndIF
	    cQuery += " FROM " + RetSqlName(cTabF) + " " + cTabF 
		cQuery += " WHERE " + cTabF + "." + cPrefx + "_FILIAL = '" + cFilTab + "' "
		cQuery += "AND " + cTabF + "." + cPrefx +  cCli + " = '" + cClient + "' "
		cQuery += "AND " + cTabF + "." + cPrefx + "_LOJA = '" + cLoja + "' "
		cQuery += "AND " + cTabF + "." + cPrefx + "_DOC = '" + cDoc + "' "
		cQuery += "AND " + cTabF + "." + cPrefx + "_SERIE = '" + cSerie + "' "
		cQuery += "AND " + cTabF + ".D_E_L_E_T_ = ''"
		cQuery := ChangeQuery(cQuery)
		dbUseArea(.T., "TOPCONN", TcGenQry(,,cQuery), cAliasRet, .T., .T.)
		count to nCount
		(cAliasRet)->(dbGoTop())
		While (!(cAliasRet)->(EOF()))
			
			If cTipTab =='1'
				cFecha := Left((cAliasRet)->&(cPrefx + "_FECTIMB"),4) + "-" + Substr((cAliasRet)->&(cPrefx + "_FECTIMB"),5,2)+ "-" + Right((cAliasRet)->&(cPrefx + "_FECTIMB"),2)+ "T" + alltrim((cAliasRet)->&(cPrefx + "_HORATRM")) + iif(Len(alltrim((cAliasRet)->&(cPrefx + "_HORATRM"))) == 5, ":00.000", ".000" )
				AADD(aDatos, {(cAliasRet)->&(cPrefx + "_CODDOC"),cFecha, (cAliasRet)->&(cPrefx + "_VALBRUT"), (cAliasRet)->&(cPrefx + "_VALMERC")} )
			
			ElseIf cTipTab =='2'
				cBActiv := ALLTRIM(SM0->M0_INSC)
				If SAH->(DbSeek(xFilial("SAH") + (cAliasRet)->&(cPrefx + "_UM")))
					cSAHCO := Alltrim(SAH->AH_COD_CO)
				EndIf	
				If SB1->(DbSeek(xFilial("SB1") + (cAliasRet)->&(cPrefx +"_COD")))
					cBProdS := Alltrim(SB1->B1_PRODSAT)
					cBCod 	:= Alltrim(SB1->B1_COD)
					cBDesc 	:= Alltrim(SB1->B1_DESC)
				EndIf	
				
				xmlOut += '  <detalle>' +  chr(13) + chr(10) 
				xmlOut += '    <actividadEconomica>' + cBActiv + '</actividadEconomica>' +  chr(13) + chr(10)
				xmlOut += '    <codigoProductoSin>' + cBProdS + '</codigoProductoSin>' + chr(13) + chr(10)
				xmlOut += '    <codigoProducto>' +cBCod + '</codigoProducto>' + chr(13) + chr(10)
				xmlOut += '    <descripcion>' + cBDesc + '</descripcion>' +  chr(13) + chr(10)
				xmlOut += '    <cantidad>' + Alltrim(TRANSFORM((cAliasRet)->&(cPrefx + "_QUANT"),cPic)) + '</cantidad>' +  chr(13) + chr(10)
				xmlOut += '    <unidadMedida>' + cSAHCO + '</unidadMedida>' +  chr(13) + chr(10)
				xmlOut += '    <precioUnitario>'+ Alltrim(TRANSFORM((cAliasRet)->&(cPrefx + cPreUn),cPic)) +'</precioUnitario>' +  chr(13) + chr(10)  
				xmlOut += '    <subTotal>'+ Alltrim(TRANSFORM((cAliasRet)->&(cPrefx + "_TOTAL"),cPic)) +'</subTotal>' +  chr(13) + chr(10)
				xmlOut += '    <codigoDetalleTransaccion>1</codigoDetalleTransaccion>' +  chr(13) + chr(10)
				xmlOut += '  </detalle>' +  chr(13) + chr(10) 	
			EndIf	
		(cAliasRet)->(dbSkip())
		EndDo
		
		If cTipTab =='2'
			If !empty(xmlOut)
				AADD(aDatos, {xmlOut} )
			Else
				AADD(aDatos, {" "} )
			EndIf
		EndIf
		
		(cAliasRet)->(dbCloseArea())  
	Endif                                                                                          
return aDatos

/*/{Protheus.doc} M486UPBO
Actualiza estaus de los documentos SF1/SF2 
@type
@author alfredo.medrano
@since 07/10/2019
@version 1.0
@param aDoct, array, Array con los datos obtenidos por el webService.
@return .T.
@example
(examples)
@see (links_or_references)
/*/
Static Function M486UPBO(aDoct)
	Local aArea	:= getArea()
	Local nI := 0
	Local nT := 0
	Local nRec := ""
	default aDoct := {}
	
	For nI := 1 to len(aDoct)
		nT := ascan(aCodEst,{|x| x[1]= aDoct[nI,6]}) // Cod. Estado
		If  nT > 0
			If nTipoDoc == 0 //NCC
				cClave := aDoct[nI,1] + aDoct[nI,3] + aDoct[nI,2] + aDoct[nI,4] + aDoct[nI,5]
				dbSelectArea("SF1")
				nRec := ConsultSF(cClave, 'SF1') 
				If nRec > 0
					SF1->(dbgoto(nRec))
					If cTipT $ "T|E" // transmisi�n y consulta 
						RecLock("SF1",.F.)
						SF1->F1_FLFTEX := aCodEst[nT][2]
						If !Empty(aDoct[nI,7]) // El c�digo recepci�n debera esta lleno para actualizar los campos
							SF1->F1_UUID := aDoct[nI,7] 
							If cTipT == "T" // solo para transmisi�n 
								SF1->F1_CODDOC := aDoct[nI,8] // se almacena el CUF que fue autorizado
								SF1->F1_FECTIMB  := STOD(STRTRAN(aDoct[nI,9],'-',"")) // se almacena la Fecha de transmisi�n
								SF1->F1_HORATRM  := aDoct[nI,10] // se almacena la Hora de transmisi�n
							EndIf
							If aCodEst[nT][2] $  '8'// 8 = cancelada
								SF1->F1_FECANTF := STOD(STRTRAN(aDoct[nI,9],'-',"")) // se actualiza la Fecha de Cancelaci�n
								SF1->F1_HORACAN  := aDoct[nI,10] // se actualiza la Hora cancelaci�n
							EndIf
						EndIf
						SF1->(MsUnlock())
						IIF(aCodEst[nT][2] == '8' .and. !Empty(aDoct[nI,7]), M486BOAUTO(cEspecie),) //8 = cancelada // fun M486BOAUTO Ejecuci�n de rutinas estandar para movimientos de anulaci�n 	
					ElseIf cTipT $ "C" // Anulaci�n 
						If !Empty(aDoct[nI,7]) // El c�digo recepci�n debera esta lleno para actualizar los campos
							RecLock("SF1",.F.)
						    SF1->F1_FLFTEX := aCodEst[nT][2]
							SF1->F1_UUIDC := aDoct[nI,7]
							SF1->F1_FECANTF := STOD(STRTRAN(aDoct[nI,9],'-',"")) // se almacena la Fecha de Cancelaci�n
							SF1->F1_HORACAN  := aDoct[nI,10] // se almacena la Hora de transmisi�n de cancelaci�n
							SF1->F1_CODAUT  := MV_PAR04 // Codigo motivo cancelaci�n 
							SF1->(MsUnlock())
						EndIf
					EndIf
				EndIF
			Else //NF / NDC		
				cClave := aDoct[nI,1] + aDoct[nI,4] + aDoct[nI,5] + aDoct[nI,3] + aDoct[nI,2]
				dbSelectArea("SF2")
				nRec := ConsultSF(cClave, 'SF2') 
				If nRec > 0
					SF2->(dbgoto(nRec))
					If cTipT $ "T|E" // transmisi�n y consulta 
					    RecLock("SF2",.F.)
						SF2->F2_FLFTEX := aCodEst[nT][2]
						If !Empty(aDoct[nI,7]) // El c�digo recepci�n debera esta lleno para actualizar los campos	
							SF2->F2_UUID := aDoct[nI,7] 
							If cTipT == "T"// solo para transmisi�n 
								SF2->F2_CODDOC := aDoct[nI,8] // se almacena el CUF que fue autorizado
								SF2->F2_FECTIMB  := STOD(STRTRAN(aDoct[nI,9],'-',"")) // se almacena la Fecha de transmisi�n
								SF2->F2_HORATRM  := aDoct[nI,10] // se almacena la Hora de transmisi�n
							EndIf
							If aCodEst[nT][2] $  '8'// 8 = cancelada
								SF2->F2_FECANTF := STOD(STRTRAN(aDoct[nI,9],'-',"")) // se actualiza la Fecha de Cancelaci�n
								SF2->F2_HORACAN  := aDoct[nI,10] // se actualiza la Hora cancelaci�n
							EndIf
						Endif
						SF2->(MsUnlock())
						IIF(aCodEst[nT][2] == '8' .and. !Empty(aDoct[nI,7]), M486BOAUTO(cEspecie),) //8 = cancelada // fun M486BOAUTO Ejecuci�n de rutinas estandar para movimientos de anulaci�n 	
					ElseIf cTipT $ "C" // Anulaci�n 
						If !Empty(aDoct[nI,7]) // El c�digo recepci�n debera esta lleno para actualizar los campos
							RecLock("SF2",.F.)
						    SF2->F2_FLFTEX := aCodEst[nT][2]
							SF2->F2_UUIDC := aDoct[nI,7]
							SF2->F2_FECANTF := STOD(STRTRAN(aDoct[nI,9],'-',"")) // se almacena la Fecha de Cancelaci�n
							SF2->F2_HORACAN  := aDoct[nI,10] // se almacena la Hora de transmisi�n de cancelaci�n
							SF2->F2_CODAUT  := MV_PAR04 // Codigo motivo cancelaci�n 
							SF2->(MsUnlock())
						EndIf
					EndIf
				EndIF
			EndIf	
		EndIf
	Next nI
	RestArea(aArea)
Return .t.

/*/{Protheus.doc} ConsultSF
Consulta las tablas SF1/SF2
@type
@author alfredo.medrano
@since 07/10/2019
@version 1.0
@param cClave, datos para el filtro de las tablas. (SEEK)
@param cxAlias, Alias de la tabla (SF1/SF2).
@return nRegno, regno del registro
@example
(examples)
@see (links_or_references)
/*/
Static Function ConsultSF(cClave, cxAlias)
	Local cAliasRet := CriaTrab(Nil, .F.)
	Local cQuery := ""
	Local cSWere := ""
	Local nRegno := 0
	
	If cClave != ""
		If cxAlias == 'SF1'
			cSWere := " WHERE F1_FILIAL+F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA ="
		Else
			cSWere := " WHERE F2_FILIAL+F2_CLIENTE+F2_LOJA+F2_DOC+F2_SERIE ="
		EndIf 
	
		cQuery := "SELECT R_E_C_N_O_ FROM " + RetSqlName(cxAlias) + cSWere + "'" + cClave +"'"
		dbUseArea(.T., "TOPCONN", TcGenQry(,,cQuery), cAliasRet, .T., .T.)
		If (cAliasRet)->(!EOF())
			nRegno := (cAliasRet)->R_E_C_N_O_ 
		EndIf
	EndIf
	(cAliasRet)->(dbCloseArea()) 
return nRegno

/*/{Protheus.doc} M486GETSIN
Obtiene datos SIN para los documentos contenidos en aDocs (Consulta estado)
@type
@author alfredo.medrano
@since 07/10/2019
@version 1.0
@param aDocs, array, Array con los datos del documento a consultar
@param aRetObs, array que ser� lledo con las observaciones generadas por la consulta.
@return .T.
@example
(examples)
@see (links_or_references)
/*/
Function M486GETSIN(aDocs, aRetObs)
	Local cRutaSMR	:= &(SuperGetMV("MV_CFDSMAR",,""))//ruta donde reside el cliente de WS 
	Local cPath	:= &(SuperGetMV("MV_CFDDOCS",,"")) 
	Local nArch := 0
	Local nI 	:= 0
	Local cNomArc:= ""
	Local cDocto := ""
	Local cDocXml:= ""
	Local aDocsC := {}
	Local aError := {}
	Local aTrans := {}
	Private aRet := {}
	Default aRetObs := {}

	CURSORWAIT()
	If Len(aDocs) > 0  //aDocs se llena en funcion M486GETINF (MATA486)
	 	nArch:= FCREATE(cPath + "listaxmlC.ini")
	 	If nArch != -1
	 		fwrite(nArch, '[XML]' + chr(13)+ Chr(10))
	 		For nI:=1 to len(aDocs)
	 			//aDocsC {SERIE,DOC,CLIFOR,TIENDA,UUID,FILIAL,.F.,"",}
	 			AADD(aDocsC,{aDocs[nI,2],aDocs[nI,3],aDocs[nI,4],aDocs[nI,5],aDocs[nI,6],aDocs[nI,1],.F.,"",aDocs[nI,7]} )
	 			cNomArc := lower(alltrim(aDocs[nI,2]) + alltrim(aDocs[nI,3])+ alltrim(cEspecie)) 
				fwrite(nArch, alltrim(aDocs[nI,2]) + "|" +  alltrim(aDocs[nI,3]) + "|" + cNomArc + "|" + alltrim(aDocs[nI,6]) + "|" + alltrim(aDocs[nI,7]) + '|' + alltrim(aDocs[nI,8]) + '|' + alltrim(aDocs[nI,9])  + '|' + alltrim(aDocs[nI,10]) + '|' + alltrim(aDocs[nI,11])  + chr(13)+ Chr(10))
			Next nI
		EndIf
		fclose(nArch)
		//Copia el archivo listaxml.ini del servidor a la ruta del smartclient 
		CpyS2T(cPath + "listaxmlC.ini", cRUTASMR)
		
		//llena aRet
		M486XFUNBOL(aDocsC,,,"E", @aRetObs)
	EndIf
	CURSORARROW()
Return aRet

/*/{Protheus.doc} M486BoObs
Muestra las observaciones en una lista de texto.
@type
@author alfredo.medrano
@since 07/10/2019
@version 1.0
@param nLin, linea seleccionada
@param aRetObs, array que ser� lledo con las observaciones generadas por la consulta.
@param aRetoList, Objeto de la lista de texto.
@param aObs, array que contiene las observaciones obtenidas del WS
@return .T.
@example
(examples)
@see (links_or_references)
/*/
Function M486BoObs(nLin, oList, aObs)
	Local aGrlist := {}	
	Local nI := 0
	Local nReg := 0
	Local cMsg := ""
	Local lban := .T.
	Local  oDlg, oMemo, oButton
	
	CURSORWAIT()
	If Len(oList:aArray)> 0
		nReg := Len(aObs)
		If nReg > 0 
			// DOC + SERIE + CLIENTE + TIENDA + COD. ESTADO
			cChave := oList:aArray[nLin,8] + oList:aArray[nLin,9] + oList:aArray[nLin,10] + oList:aArray[nLin,11] + oList:aArray[nLin,5]  
			nT := ascan(aObs,{|x| x[8]+x[9]+x[10]+x[11]+x[5] == cChave })
			If nT > 0
				For nI:=1 to nReg
					If aObs[nI,8]+aObs[nI,9]+aObs[nI,10]+aObs[nI,11]+aObs[nI,5] == cChave
						If lban
							cMsg += STR0006 + aObs[nI,8] + chr(13)+chr(10)//"Docto   "
							cMsg += STR0007 + aObs[nI,9] + chr(13)+chr(10)//"Serie   "
							cMsg += STR0008 + aObs[nI,10]+ chr(13)+chr(10)//"Cliente "
							cMsg += STR0009 + aObs[nI,11]+ chr(13)+chr(10)//"Tienda  "
							lban := .F.
						EndIf							
						cMsg += aObs[nI,5] + " " + aObs[nI,6] + chr(13)+chr(10)
					EndIf
				Next 
			EndIf
		Else
			cMsg := STR0010//"Sin observaciones "
		EndIf
	EndIf
	
	CURSORARROW()
	DEFINE MSDIALOG oDlg FROM 0,0 TO 375,440 PIXEL TITLE STR0011//'Observaciones'
	oMemo:= tMultiget():New(10,10,{|u|if(Pcount()>0,cMsg:=u,cMsg)} ,oDlg,200,150,,.T.,,,,.T.,,,,,,.T.,,,,,.T.)
	oButton := TButton():New(165, 160,STR0012,oDlg,{||oDlg:End()},30,11,,,,.T.) //"Salir"	
	ACTIVATE MSDIALOG oDlg CENTERED	
Return

/*/{Protheus.doc} m486xBoMax
obtiene la �ltima secuencia de la tabla informada
@type
@author alfredo.medrano
@since 22/11/2019
@version 1.0
@param cCod, tabla alfanumeria 
@return nNumRet
@example
(examples)
@see (links_or_references)
/*/
Function m486xBoMax(cCod)
	Local cAliasRet := CriaTrab(Nil, .F.)
	Local cQuery := ""
	Local nNumRet := 0
	
	If cCod != ""
		cQuery := "SELECT  MAX(F3I_SEQUEN) NUMMAX FROM " + RetSqlName("F3I")  
		cQuery += " WHERE F3I_CODIGO ='" + cCod + "'"   
		dbUseArea(.T., "TOPCONN", TcGenQry(,,cQuery), cAliasRet, .T., .T.)
		If (cAliasRet)->(!EOF())
			nNumRet := Val((cAliasRet)->NUMMAX)
		EndIf
	EndIf
	(cAliasRet)->(dbCloseArea()) 
Return nNumRet

/*/{Protheus.doc} M486XFBOLB
  Funci�n que comunica la anulacion de documentos electr�nicos SIN.
  @type
  @author Alfredo Medrano
  @since 26/11/2019
  @version 1.0
  @param N/A
  @return ${return}, ${return_description}
  @example
  (examples)
  @see (links_or_references)
  /*/
Function M486XFBOLB()
	Local cPergBC     := "MATA486G"
	Local aDocs       := {}
	Local aArea       := GetArea()
	Local aTamaho     := MsAdvSize()
	Local aIndx	      := {STR0020}   //"Factura + Serie"
	Local cIndx	      := aIndx[1]
	Local nOpc        := 0
	Local cBusca      := Space(TAMSX3("F2_LOJA")[1]+TAMSX3("F2_CLIENTE")[1]+TAMSX3("F2_DOC")[1]+TAMSX3("F2_SERIE")[1])
	Local oOkS        := LoadBitmap(GetResources(),"br_verde")//disponible para anular
	Local oNoS        := LoadBitmap(GetResources(),"br_vermelho")//Anulaci�n Rechazada
	Local oOkP        := LoadBitmap(GetResources(),"br_pink")//Anulaci�n Pendiente
	Local oOk	      := LoadBitmap(GetResources(),"LBOK")
	Local oNo	      := LoadBitmap(GetResources(),"LBNO")
	Local oDlgFat     := Nil
	Local bSet15	  := {|| VALGENBOL(oLbx1,oDlgFat,@nOpc,aDocs)}
	Local bSet24	  := {|| nOpc:=0, oDlgFat:End()}
	Local aButtons    := {{"S4WB011N", {|| LeyendaCB()}, STR0019, STR0019}} //"Leyenda"
	Local bDialogInit := { || EnchoiceBar(oDlgFat,bSet15,bSet24,nil,aButtons)}
	Local nX          := 0
	Local nPosLbx     := 0
	Local aFact       := {}
	Local oBoton      := Nil
	Local oBusca      := Nil
	Local oMarTodos   := Nil
	Local oDesTodos   := Nil
	Local oInvSelec   := Nil
	Local cMsgLog     := ""	
	Local aError      := {}	
	Local aTrans	  := {}							
    Local nArch 
    Local lRet		  := .T.
    Local cNomArc	  :=""
    Local aDocsA      := {}
	Local cPath		  := &(SuperGetMV("MV_CFDDOCS",,"")) 
	Local cRutaSMR	  := &(SuperGetMV("MV_CFDSMAR",,""))//ruta donde reside el cliente de WS 
	Local n 		  := 0
	private aRetObs	  := {}	
		
	If nTDTras = 1 .Or. nTDTras = 4 //Factura/Boleta de Venta
		cTipo := "N"
	ElseIf nTDTras = 2 //Nota de D�bito
		cTipo := "D"
	ElseIf nTDTras = 3 //Nota de Cr�dito
		cTipo := "C"
	EndIf
	
	If Pergunte(cPergBC,.T.)

		aDocs := M486SFANU(MV_PAR01, MV_PAR02, MV_PAR03, MV_PAR04)
		
		If Len(aDocs) == 0
			Aviso(STR0014,STR0015,{STR0016})// "Anulaci�n", "No se encontraron facturas para anular, revise los parametros de selecci�n", {"Ok"}
			Return Nil
		Else	
			DEFINE MSDIALOG oDlgFat FROM aTamaho[1],aTamaho[2] TO aTamaho[6],aTamaho[5] TITLE STR0017 PIXEL   //"Anulacci�n de Facturas"
		
			@ c(30),c(05) MSCOMBOBOX oIndx VAR cIndx ITEMS aIndx SIZE c(90),c(10) PIXEL OF oDlgFat
			@ c(30),c(98) BUTTON oBoton PROMPT STR0018 SIZE c(35),c(10) ; //"Buscar"
					 ACTION (oLbx1:nAT := M486BusBol(oLbx1,aDocs,cBusca,oIndx:nAT), ;
							oLbx1:bLine := {|| {If(aDocs[oLbx1:nAt,11] == "6",oOkS,If(aDocs[oLbx1:nAt,11]=="9",oNoS,oOkP)),If(aDocs[oLbx1:nAt,2],oOk,oNo),aDocs[oLbx1:nAt,3],aDocs[oLbx1:nAt,4],;
							aDocs[oLbx1:nAt,5],aDocs[oLbx1:nAt,6],aDocs[oLbx1:nAt,7],aDocs[oLbx1:nAt,8],aDocs[oLbx1:nAt,9]}},;
							oLbx1:SetFocus()) PIXEL OF oDlgFat
			@ c(42),c(05)  MSGET oBusca VAR cBusca PICTURE "@!" SIZE c(130),c(10) PIXEL  OF oDlgFat
			@ c(58),c(05)  LISTBOX oLbx1 VAR nPosLbx FIELDS HEADER ;
							OemToAnsi(""),;     //Status
							OemToAnsi(""),;     //Check
							STR0007,;           //serie
							STR0021,;    		//Documento
							STR0022,;			//Fecha de emisi�n
							STR0023,;			//Fecha de autorizaci�n (SUNAT)
							STR0009,;	        //"Tienda"
							STR0008,;	        //"Cliente"
							STR0024;	        //"Nombre"   
			          SIZE aTamaho[3] - 25,IIf(aTamaho[6]>700,(aTamaho[4] * .775)-25, IIf(aTamaho[6]<500,aTamaho[4] * .6,aTamaho[4] * .7)) OF oDlgFat ; //aTamaho[6] * .875
			          PIXEL ON DBLCLICK ( IIf(aDocs[oLbx1:nAt,1] ;			          
			          .And. (IIf(aDocs[oLbx1:nAt,10]$"NF|NDC",MaCanDelF2("SF2",aDocs[oLbx1:nAt,12],,,,aDocs[oLbx1:nAt,13]),LxMaCanDelF1(aDocs[oLbx1:nAt,12],,,,,,.F.,aDocs[oLbx1:nAt,13]))), ;
			          (M486MrcBol(oLbx1,@aDocs,@oDlgFat),oLbx1:nColPos:= 1,oLbx1:Refresh()), ) ) NOSCROLL
			oLbx1:SetArray(aDocs)
			oLbx1:bLine := {|| {If(aDocs[oLbx1:nAt,11] == "6",oOkS,If(aDocs[oLbx1:nAt,11]=="9",oNoS,oOkP)),If(aDocs[oLbx1:nAt,2],oOk,oNo),aDocs[oLbx1:nAt,3],aDocs[oLbx1:nAt,4],;
							aDocs[oLbx1:nAt,5],aDocs[oLbx1:nAt,6],aDocs[oLbx1:nAt,7],aDocs[oLbx1:nAt,8],aDocs[oLbx1:nAt,9]}}
			oLbx1:Refresh()
		
			@ aTamaho[4] * .953,c(005) BUTTON oMarTodos PROMPT STR0025 SIZE c(45),c(10) ACTION M486MarcaI( oLbx1 , @aDocs , @oDlgFat , "M" ) PIXEL OF oDlgFat //aTamaho[4] * .92 //"Marcar Todos"
			@ aTamaho[4] * .953,c(055) BUTTON oDesTodos PROMPT STR0026 SIZE c(45),c(10) ACTION M486MarcaI( oLbx1 , @aDocs , @oDlgFat , "D" ) PIXEL OF oDlgFat //"Desmarcar Todos"
			@ aTamaho[4] * .953,c(110) BUTTON oInvSelec PROMPT STR0027 SIZE c(45),c(10) ACTION M486MarcaI( oLbx1 , @aDocs , @oDlgFat , "I" ) PIXEL OF oDlgFat //"Invierte selecci�n"
		
			ACTIVATE MSDIALOG oDlgFat ON INIT Eval(bDialogInit) CENTERED
		
			CursorWait()
		
			If  nOpc == 1
				nArch:= FCREATE(cPath + "listaxmlA.ini")
				If nArch != -1
					fwrite(nArch, '[XML]' + chr(13)+ Chr(10))
					nTotDoc := Len(aDocs)
					For nX := 1 To nTotDoc
						If aDocs[nX][2] //Seleccionado
							aAdd(aFact,{aDocs[nX][3], aDocs[nX][4], aDocs[nX][7], aDocs[nX][8], aDocs[nX][5], aDocs[nX][14], .F., ""})
				 			AADD(aDocsA,{aDocs[nX,3],aDocs[nX,4],aDocs[nX,8],aDocs[nX,7],aDocs[nX,16],aDocs[nX,14],.F.,"",} ) 
							fwrite(nArch, alltrim(aDocs[nX,3]) + "|" +  alltrim(aDocs[nX,4]) + "|" + alltrim(aDocs[nX,15]) + "|" + alltrim(aDocs[nX,16]) + "|" + alltrim(aDocs[nX,17]) + "|" + alltrim(MV_PAR04) + "|" + alltrim(aDocs[nX,18]) + chr(13)+ Chr(10))
						EndIf
					Next nX		
				EndIf
				fclose(nArch)
				//Copia el archivo listaxml.ini del servidor a la ruta del smartclient 
				CpyS2T(cPath + "listaxmlA.ini", cRUTASMR)
				//ejecuta .exe para consumir servicios web de anulaci�n
				Processa({|lEnd| lRet := M486XFUNBOL(aDocsA,@aError,@aTrans,"C", @aRetObs)},STR0037) //"Transmitiendo Documentos..."	
			EndIf
			
			If Len(aError) > 0 .And. Len(aTrans) <> Len(aError)
				cMsgLog := STR0028  + cCRLF + STR0029 // "Ocurrieron inconvenientes al momento de solicitud de anulaci�n" // "�Desea visualizar log de anulaci�n?"
			ElseIf Len(aError) > 0 .And. Len(aTrans) == Len(aError)
				cMsgLog := STR0030 + cCRLF + STR0029// "Solicitud de anulaci�n exitosa"  // "�Desea visualizar log de anulaci�n?"
			EndIf
			
			nDetalle := Len(aRetObs)
			If nDetalle > 0
				for n:= 1 to nDetalle
					AADD(aError,{aRetObs[n][1], aRetObs[n][2], aRetObs[n][3], aRetObs[n][4], aRetObs[n][5]})
				next
			EndIf

			If !Empty(cMsgLog)
				If MsgYESNO(cMsgLog) 
					M486GENLOG(aError, nTotDoc, Len(aTrans))				
				EndIf
			EndIf
		
			DeleteObject(oOk)
			DeleteObject(oNo)
			DeleteObject(oOkS)
			DeleteObject(oNoS)
			DeleteObject(oOkP)
		
			CursorArrow()
			bFiltraBrw := {|| FilBrowse(cAliasB,@aIndArqE,@cFiltro) }
			Eval(bFiltraBrw)	

			CursorArrow()
			RestArea(aArea)		
		EndIf
	EndIf
Return

/*/{Protheus.doc} VALGENBOL
//Valida que hayan sido seleccionado al menos un documento para anular.
@author Alfredo Medrano
@since 26/11/2019
@version 1.0
@return Nil
@type function
/*/
Static Function VALGENBOL(oLbx1, oDlgFat, nOpc, aItems)
	Local lRet  := .F.
	Local nPos  := 0
	
	nPos := aScan(aItems, {|aVal| aVal[2] == .T.} )
	If  nPos>0
		lRet := .T.
		nOpc := 1
		oDlgFat:End()
	Else
		Aviso(STR0017, STR0031 ,{STR0016}) //"Anulaci�n de Facturas" //"Es necesario selecionar al menos una factura." //"Ok"
	EndIf
Return lRet

/*/{Protheus.doc} M486SFANU
//Carga datos a ser mostrados para anulaci�nd e documentos electr�nicos.
@author Alfredo Medrano
@since 26/11/2019
@version 1.0
@return Nil
@type function
/*/
Static Function M486SFANU(cSerie, cDocIni, cDocFin, cMotivo)
	Local cAliasTmp := GetNextAlias() 
	Local cEsp      := ""
	Local cCampos   := ""
	Local cTablas   := ""
	Local cCond     := ""
	Local cCondFB   := ""
	Local cOrder    := ""
	Local aFacturas := {}
	Local nReg 		:= 0
	Local cRutina   := ""

	If nTDTras == 1 .Or. nTDTras == 2 .Or. nTDTras == 4 //Factura de Venta - Nota de D�bito
		cRutina := "MATA467N|MATA460"
		If nTDTras == 1 //Factura/
			cEsp := "NF"
		ElseIf nTDTras == 2 //nota de d�bito
			cEsp := "NDC"
			cRutina := "MATA465N"
		EndIf
		cCampos	:= "% SF2.F2_FILIAL FILIAL, SF2.F2_CLIENTE CLIENTE, SF2.F2_LOJA LOJA, SF2.F2_DOC DOC, SF2.F2_SERIE SERIE, SF2.F2_EMISSAO EMISSAO, SF2.F2_FECTIMB FECHATRANS, F2_TPDOC TIPDOC, "
		cCampos	+= "  SF2.F2_FECANTF FECHCANCEL, SA1.A1_NOME, SF2.F2_FLFTEX STATUS, SF2.F2_ESPECIE ESPECIE, SF2.R_E_C_N_O_ SFRECNO, F2_UUIDC IDANULA , F2_UUID IDTRANS, F2_CODDOC CUF %"
		cTablas := "% " + RetSqlName("SF2") + " SF2, " + RetSqlName("SA1") + " SA1 %"
		cCond	:= "% SF2.F2_FILIAL = '" + xFilial("SF2") + "'"
		cCond	+= " AND SA1.A1_FILIAL = '" + xFilial("SA1") + "'"
		cCond	+= " AND SF2.F2_CLIENTE = SA1.A1_COD"
		cCond	+= " AND SF2.F2_LOJA = SA1.A1_LOJA"
		cCond	+= " AND SF2.F2_ESPECIE = '" + cEsp + "'"
		cCond	+= " AND SF2.F2_SERIE = '" + cSerie + "'"
		cCond	+= " AND SF2.F2_DOC >= '" + cDocIni + "'"
		cCond	+= " AND SF2.F2_DOC <= '" + cDocFin + "'"
		cCond	+= " AND SF2.F2_FLFTEX  IN ('6','7') "
		cCond	+= " AND SF2.D_E_L_E_T_  = ' ' "
		cCond	+= " AND SA1.D_E_L_E_T_  = ' ' %"
		cOrder 	:= "% SF2.F2_SERIE, SF2.F2_DOC %"
	ElseIf nTDTras == 3 //Nota de Cr�dito
		cEsp    := "NCC"
		cRutina := "MATA465N"
		cCampos	:= "% SF1.F1_FILIAL FILIAL, SF1.F1_FORNECE CLIENTE, SF1.F1_LOJA LOJA, SF1.F1_DOC DOC, SF1.F1_SERIE SERIE, SF1.F1_EMISSAO EMISSAO, SF1.F1_FECTIMB FECHATRANS,F1_TIPNOTA TIPDOC, "
		cCampos	+= "  SF1.F1_FECANTF FECHCANCEL, SA1.A1_NOME, SF1.F1_FLFTEX STATUS, SF1.F1_ESPECIE ESPECIE, SF1.R_E_C_N_O_ SFRECNO, F1_UUIDC IDANULA, F1_UUID IDTRANS, F1_CODDOC CUF  %"
		cTablas := "% " + RetSqlName("SF1") + " SF1, " + RetSqlName("SA1") + " SA1 %"
		cCond	:= "% SF1.F1_FILIAL = '" + xFilial("SF1") + "'"
		cCond	+= " AND SA1.A1_FILIAL = '" + xFilial("SA1") + "'"
		cCond	+= " AND SF1.F1_FORNECE = SA1.A1_COD"
		cCond	+= " AND SF1.F1_LOJA = SA1.A1_LOJA"
		cCond	+= " AND SF1.F1_ESPECIE = '" + cEsp + "'"
		cCond	+= " AND SF1.F1_SERIE = '" + cSerie + "'"
		cCond	+= " AND SF1.F1_DOC >= '" + cDocIni + "'"
		cCond	+= " AND SF1.F1_DOC <= '" + cDocFin + "'"
		cCond	+= " AND SF1.F1_FLFTEX  IN ('6','7') "
		cCond	+= " AND SF1.D_E_L_E_T_  = ' ' "
		cCond	+= " AND SA1.D_E_L_E_T_  = ' ' %"
		cOrder 	:= "% SF1.F1_SERIE, SF1.F1_DOC %"		
	EndIf

	BeginSql alias cAliasTmp
		SELECT %exp:cCampos%
		FROM  %exp:cTablas%
		WHERE %exp:cCond%
		ORDER BY %exp:cOrder%
	EndSql
	
	TCSetField(cAliasTmp,"EMISSAO","D")
	TCSetField(cAliasTmp,"FECHATRANS","D")

	Count to nReg
	
	If nReg > 0
		dbSelectArea(cAliasTmp)
		(cAliasTmp)->(dbGotop())
		
		While  (cAliasTmp)->(!EOF())
			aAdd(aFacturas,{(cAliasTmp)->STATUS $ "5|6|7", ;                    //[1]Status/ 9=anulacion Rechazada /6=disponible p/anulaci�n/7=anulacion pendiente 
							.F., ;                                              //[2]Selecci�n al cargar
			                (cAliasTmp)->SERIE, ;                               //[3]Serie
			                (cAliasTmp)->DOC, ;                                 //[4]Documento
							(cAliasTmp)->FECHATRANS, ;                          //[5]Fecha de emisi�n
							(cAliasTmp)->FECHCANCEL, ;                          //[6]Fecha de autorizaci�n SUNAT
							(cAliasTmp)->LOJA, ;                                //[7]Tienda
							(cAliasTmp)->CLIENTE, ;                             //[8]C�d. Cliente
							Alltrim((cAliasTmp)->A1_NOME), ;                    //[9]Nombre Cliente
							Alltrim((cAliasTmp)->ESPECIE), ;                    //[10]Especie
							(cAliasTmp)->STATUS, ;                              //[11]Status
							(cAliasTmp)->SFRECNO, ;                             //[12]RecNo
							cRutina, ;                                          //[13]Rutina
							(cAliasTmp)->FILIAL, ;                              //[14]Filial
							(cAliasTmp)->IDANULA,;                              //[15]ID de anulaci�n
							(cAliasTmp)->IDTRANS,;                              //[16]ID de transmisi�n
							(cAliasTmp)->CUF,;                                   //[17]CUF
							(cAliasTmp)->TIPDOC})                               //[18]Docto Sector
			(cAliasTmp)->(dbSkip())
		EndDo
		
		(cAliasTmp)->( dbCloseArea())
	EndIf
Return aFacturas

/*/{Protheus.doc} M486BUSCVE
//Valida posici�n de documentos para comunicado de baja.
@author Alfredo Medrano
@since 26/11/2019
@version 1.0
@return Nil
@type function
/*/
static Function M486BusBol(oLbx1,aItems,cBusca,nIndx)
	Local nPos := 0
	
	cBusca := Upper(Alltrim(cBusca))
	If  nIndx == 1    //"Factura + Serie"
		nPos := aScan(aItems, {|aVal| aVal[4] + aVal[3] = Alltrim(cBusca)} ) 
	EndIf
	If  nPos == 0
		nPos := oLbx1:nAt
	EndIf
Return nPos

/*/{Protheus.doc} M486MarcaI
//Marca el item para comunicado de baja.
@author Alfredo Medrano
@since 26/11/2019
@version 1.0
@return Nil
@type function
/*/
Static Function M486MrcBol(oLbx1,aItems,oDlgRec,cMarckTip)
	Default cMarckTip := ""
	If Empty( cMarckTip )
		aItems[oLbx1:nAt,2]:= !aItems[oLbx1:nAt,2]
	ElseIf cMarckTip == "M"
		aEval( aItems , { |x,y| aItems[y,2] := .T. } )
	ElseIf cMarckTip == "D"
		aEval( aItems , { |x,y| aItems[y,2] := .F. } )
	ElseIf cMarckTip == "I"
		aEval( aItems , { |x,y| aItems[y,2] := !aItems[y,2] } )
	EndIf
Return Nil

/*/{Protheus.doc} Leyenda
Genera ventana con leyenda y significado de los estatus
@type function
@author Alfredo Medrano
@since 27/11/2019
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
Static Function LeyendaCB()
	BrwLegenda(STR0032,STR0019,{;//"Anulaci�n de Documentos","Leyenda"
		            {"BR_VERMELHO",STR0033},;//"Anulaci�n Rechazada"
		            {"BR_PINK",STR0034},;//"Anulaci�n Pendiente de Confirmaci�n"
		            {"BR_VERDE",STR0035};//"Disponible para Anulaci�n"
		            })	        
Return

/*/{Protheus.doc} M486AUTOCB
//Realiza baja automatica de documentos.
@author Alfredo Medrano
@since 13/04/2020
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
Function M486BOAUTO(cEsp)
	Local aArea	:= getArea()
	Local aCabs  := {}
	Local aItens := {}
	Local cFilSD := IIf(Alltrim(cEsp) $ "NF|NDC", xFilial("SD2"), xFilial("SD1"))
	Local nY     := 0
	
	aSize(aCabs, 0)
	aSize(aItens, 0)
	
	If Alltrim(cEsp) $ "NF|NDC"
		For nY := 1 to Len(aCabsSF2)
			aAdd(aCabs, {aCabsSF2[nY], &("SF2->"+aCabsSF2[nY]), Nil})
		Next nY
	
		SD2->(dbSetOrder(3)) //D2_FILIAL + D2_DOC + D2_SERIE + D2_CLIENTE + D2_LOJA + D2_COD + D2_ITEM
		SD2->(dbSeek(cFilSD + SF2->F2_DOC + SF2->F2_SERIE + SF2->F2_CLIENTE + SF2->F2_LOJA))
		Do While !SD2->(Eof()) .And. cFilSD+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA==SD2->D2_FILIAL+SD2->D2_DOC+SD2->D2_SERIE+SD2->D2_CLIENTE+SD2->D2_LOJA
			aAdd(aItens, {})
			For nY := 1 to Len(aItensSD2)
				aAdd(aItens[Len(aItens)], {aItensSD2[nY],&("SD2->"+aItensSD2[nY]), Nil})
			Next nY
			SD2->(dbSkip())
		 Enddo
	
		BeginTran()
		lMSErroAuto := .F.
		MaFisEnd()	
		
		If Alltrim(cEsp) == "NF" //Factura-Boleta de Venta
			MSExecAuto({|x,y,z,a| MATA467N(x,y,z,a)},aCabs,aItens,6) //Anulado
		ElseIf Alltrim(cEsp) == "NDC" //Nota de D�bito
			MSExecAuto({|x,y,z,a| MATA465N(x,y,z,a)},aCabs,aItens,6)
		EndIF
	ElseIf Alltrim(cEsp) = "NCC"
		For nY := 1 to Len(aCabsSF1)
			aAdd(aCabs, {aCabsSF1[nY], &("SF1->"+aCabsSF1[nY]), Nil})
		Next nY

		SD1->(dbSetOrder(1)) //D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA+D1_COD+D1_ITEM
		SD1->(dbSeek(cFilSD + SF1->F1_DOC +SF1->F1_SERIE + SF1->F1_FORNECE + SF1->F1_LOJA))
		Do While !SD1->(Eof()) .And. cFilSD+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA==SD1->D1_FILIAL+SD1->D1_DOC+SD1->D1_SERIE+SD1->D1_FORNECE+SD1->D1_LOJA
			aAdd(aItens, {})
			For nY := 1 to Len(aItensSD1)
				aAdd(aItens[Len(aItens)], {aItensSD1[nY],&("SD1->"+aItensSD1[nY]), Nil})
			Next nY
			SD1->(dbSkip())
		 Enddo

		// Baja por rutina autom�tica
		BeginTran()
		lMSErroAuto := .F.
		MaFisEnd()
		MSExecAuto({|x,y,z,a| MATA465N(x,y,z,a)},aCabs,aItens,6) //Anulado	
	EndIf
	
	If lMSErroAuto
		DisarmTransaction()
	Else
		EndTran()
	EndIf
	RestArea(aArea)
Return
/*/{Protheus.doc} M486MonBo
Muestra pantalla gr�fica del monitor 
@type function
@author Alf Medrano
@since 23/04/2020
@version 1.0
@param aItems, array, (Array con los items que ser�n mostrados en el monitor)
@param aDocs, array, (Array original con los datos de los documentos a consultar)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Function M486MonBo(aItems,aDocs,aObs )

	Local oDlg, oList, oButton0, oButton1, oButton2
	default aObs := {}
	DEFINE MSDIALOG oDlg FROM 0,0 TO 347,958 PIXEL TITLE STR0051 //Monitor
					
	@ 01,01 LISTBOX oList FIELDS;
	HEADER "", STR0040 , STR0041 , STR0042 , STR0046 , STR0044 , STR0045 ; //Documento //Ambiente //Fch. Aut //"Cod. Estado" //Mensaje //Recomendacion 
	SIZE 480,130 OF oDlg PIXEL			

	oList:SetArray(aItems)
	If len(aItems) > 0
		oList:bLine := {|| {GetStatus(aItems[oList:nAt,1]),aItems[oList:nAt,2],aItems[oList:nAt,3],aItems[oList:nAt,4],aItems[oList:nAt,5],aItems[oList:nAt,6],aItems[oList:nAt,7]}}			
	EndIf

	oButton0 := TButton():New(140, 338,STR0047,oDlg,{||ObtieneXML(oList:nAt,oList,aObs)},46,17,,,,.T.) //Obtener XML
	oButton1 := TButton():New(140, 390,STR0048,oDlg,{||M486BoObs(oList:nAt,oList,aObs)},46,17,,,,.T.) //Observaciones 	
	oButton2 := TButton():New(140, 442,STR0049,oDlg,{||oDlg:End()},33,17,,,,.T.) //"Salir"
					
	ACTIVATE MSDIALOG oDlg CENTERED
Return
/*/{Protheus.doc} GetStatus
Obtiene el icono del status de los documentos mostrados en la opci�n monitor
@type function
@author Alfredo Medrano
@since 23/04/2020
@version 1.0
@param nStatus, num�rico, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function GetStatus(nStatus)
	Local oStatus
	If nStatus == 1
		oStatus := LoadBitmap(GetResources(), "BR_AZUL")
	ElseIf nStatus == 4 .or. nStatus == 2
		oStatus := LoadBitmap(GetResources(), "BR_AMARELO")
	ElseIf nStatus == 5 .or. nStatus == 3
		oStatus := LoadBitmap(GetResources(), "BR_VERMELHO")
	ElseIf nStatus == 6
		oStatus := LoadBitmap(GetResources(), "BR_VERDE")
	ElseIf cPaisLoc == "BOL" .and. nStatus == 9
		oStatus := LoadBitmap(GetResources(), "BR_PRETO")
	ElseIf cPaisLoc == "BOL" .and. nStatus == 8
		oStatus := LoadBitmap(GetResources(), "BR_LARANJA")
	EndIf
Return oStatus

/*/{Protheus.doc} ObtieneXMl
Obtiene el xml del documento seleccionado
@type function
@author Alfredo Medrano
@since 27/04/2020
@version 1.0
@param nStatus, num�rico, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
static function ObtieneXMl(nLin, oList, aObs)
	Local aGrlist := {}	
	Local nI := 0
	Local nReg := 0
	Local cMsg := ""
	Local lban := .T.
	Local  oDlg, oMemo, oButton
	Local cSchema := ""
	Local cPath		:= (getMV("MV_CFDDOCS"))
	
	CURSORWAIT()
	If Len(oList:aArray)> 0
		cFile := alltrim(oList:aArray[nLin,9]) + alltrim(oList:aArray[nLin,8]) +alltrim(cEspecie)+".xml"
		if !Empty(cFile)	
			cSchema:= fReadfile(cPath,cFile)
			cSchema := strtran(cSchema,chr(13) + chr(10), "")
		EndIf
		If !Empty(cSchema)
			DEFINE MSDIALOG oDlg FROM 0,0 TO 375,440 PIXEL TITLE STR0050 + " " + cFile//'ARchivo'
			oMemo:= tMultiget():New(10,10,{|u|if(Pcount()>0,cSchema:=u,cSchema)} ,oDlg,200,150,,.T.,,,,.T.,,,,,,.T.,,,,,.T.)
			oButton := TButton():New(165, 160,STR0049,oDlg,{||oDlg:End()},30,11,,,,.T.) //"Salir"	
			ACTIVATE MSDIALOG oDlg CENTERED	
		EndIf

	EndIf	
	CURSORARROW()
	
return
/*/{Protheus.doc} fReadfile
Lee archivo XML 
@type function
@author Alfredo MEdrano
@since 27/04/2020
@version 1.0
@param cPath, character, (Ruta del archivo)
@param cFile, character, (Nombre del archivo)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static function fReadfile(cPath,cFile)
	Local cTexto     := ""
	Local cNewFile   := ""
	Local cExt       := ""
	Local nHandle    := 0
	Local nTamanho   := 0
	
	cNewFile := &(cPath) + cFile
	nHandle := FOpen(cNewFile)
	If nHandle > 0
		nTamanho := Fseek(nHandle,0,FS_END)
		FSeek(nHandle,0,FS_SET)
		FRead(nHandle,@cTexto,nTamanho)
		FClose(nHandle)
	EndIf
Return cTexto

/*/{Protheus.doc} ObtImpBol
Obtiene y suma los valimp por medio del Impuesto de cada �tem
@type function
@author Alfredo Medrano
@since 27/04/2020
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Function ObtImpBol(cDoc,cSerie,cCliente,cLoja)
Local aArea  	:= GetArea()
Local cTmp   	:= getNextAlias()
Local cValimp 	:= ""
Local nValimp	:= 0
Local cCpoCab 	:= ""

dbSelectArea("SX3")
dbSetOrder(1)//
dbSeek("SD1")
While !Eof() .and. SX3->X3_ARQUIVO == "SD1"
	If "D1_VALIMP" $ SX3->X3_CAMPO 
		cValimp += SX3->X3_campo + ","
	EndIf
	dbSkip()
EndDo
cValimp := "%"+cValimp+"%"
BeginSql alias cTmp
	SELECT %exp:cValimp% FC_TES, FB_CODIGO, FB_CPOLVRO, FB_ALIQ, FC_INCDUPL
	FROM %table:SFB% SFB INNER JOIN %table:SFC% SFC ON FB_CODIGO = FC_IMPOSTO
	INNER JOIN %table:SF4% SF4 ON SFC.FC_TES = SF4.F4_CODIGO INNER JOIN %table:SD1% SD1 ON SFC.FC_TES = SD1.D1_TES 
	WHERE FB_FILIAL = %exp:xfilial("SFB")%
		AND SD1.D1_FILIAL = %exp:xfilial("SD1")%
		AND FC_FILIAL = %exp:xfilial("SFC")%
		AND F4_FILIAL = %exp:xfilial("SF4")%
		AND SD1.D1_DOC =  %exp:cDoc%
		AND SD1.D1_SERIE =  %exp:cSerie%
		AND SD1.D1_FORNECE = %exp:cCliente%
		AND SD1.D1_LOJA = %exp:cLoja%
		AND SD1.%notDel%
		AND SFC.%notDel%
		AND SFB.%notDel%
		AND SF4.%notDel%	
EndSql

dbSelectArea(cTmp)
(cTmp)->(DbGoTop())
While (!(cTmp)->(EOF()))
	cCpoCab  := 'D1_VALIMP'+ (cTmp)->FB_CPOLVRO
	nValimp += (cTmp)->(&cCpoCab)
	dbSkip()
EndDo 
(cTmp)->(dbCloseArea())                          
RestArea(aArea)
Return nValimp
