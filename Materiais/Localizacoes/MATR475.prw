#INCLUDE 'PROTHEUS.CH'
#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "RPTDEF.CH"
#INCLUDE "FONT.CH"
#INCLUDE "MATR475.CH"

/*
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
���������������������������������������������������������������������������������Ŀ��
���Fun��o    � MATR475  � Autor � Marco Augusto Gonzalez Rivera � Data �28/08/2018���
���������������������������������������������������������������������������������Ĵ��
���Descri��o � Generacion de PDF para Documentos Fiscales de Entrada/Salida.      ���
���������������������������������������������������������������������������������Ĵ��
���Uso       � Facturacion - Mexico.                                              ���
���������������������������������������������������������������������������������Ĵ��
���                ATUALIZACOES SOFRIDAS DESDE A CONSTRUCAO INICIAL.              ���
���������������������������������������������������������������������������������Ĵ��
��� PROGRAMADOR  � FECHA  �     BOPS    �        MOTIVO DA ALTERACAO              ���
���������������������������������������������������������������������������������Ĵ��
���Alf. Medrano�27/12/18�DMINA-1644�En fun MATR475Enc se asigna la Fun F815LinCar ���
���            �        �          � a la Forma de Pago para  recorta y dividir en���
���            �        �          � lineas la descripcion. se asigna la descripc.���
���            �        �          � del R�gimen Fiscal obtenida de la tabla S010 ���
���Oscar Garcia�25/02/19�DMINA-6068�Creaci�n de PE M475IMPPER para impresion usan-���
���            �        �          �do formato personalizado.(MEX)                ���
���Alf. Medrano�02/07/19�DMINA-6599�En fun MATR475Det a precio unitario cValUniItm���
���            �        �          � se quita la fun trasnform() y solo se asigna ���
���            �        �          � un ALLTRIM() para quitar espacios.           ���
���Alf. Medrano�09/07/19�DMINA-6966�Se crea Fun UTf8ToChr() para el control de ca-���
���            �        �          �-racteres especciales(uff8 a Char). Dentro de ���
���            �        �          �la func MATR475Enc se asigna UTf8ToChr al nom-���
���            �        �          �-bre del Emisor, Cliente y descrip de producto���
����������������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
/*/
Function MATR475()

	Local cPerg			:= "MTR475"
	Local cQuery		:= ""
	Local cEspecie		:= ""
	Local cTipoFact		:= ""
	Local nNumRegs		:= 0
	Local nNumDocImp	:= 0
	Local lTabSF2		:= .T.
	Local lVisualPDF	:= .F.
	Local cAliasQry		:= GetNextAlias()
	Local lImpPer		:= ExistBlock("M475IMPPER")

	//�������������������������������������������������������Ŀ
	//� Variables utilizadas como par�metros                  �
	//� mv_par01 - Tipo de Documento: NF, NDC y NCC           �
	//� mv_par02 - Tipo de Factura: Normal, Traslado y Ambas  �
	//� mv_par03 - De fecha                                   �
	//� mv_par04 - A Fecha                                    �
	//� mv_par05 - De Serie                                   �
	//� mv_par06 - A Serie                                    �
	//� mv_par07 - De Documento                               �
	//� mv_par08 - A Documento                                �
	//� mv_par09 - Imprime Timbrados: Si, No y Ambos          �
	//� mv_par10 - Visualizar PDF: Si y No                    �
	//���������������������������������������������������������
	If !Pergunte(cPerg,.T.)
		Return
	Endif

	If MV_PAR01 == 1 //Factura de Venta
		cEspecie := "NF"
	ElseIf MV_PAR01 == 2 //Nota de Debito
		cEspecie := "NDC"
	ElseIf MV_PAR01 == 3 //Nota de Credito
		cEspecie := "NCC"
		lTabSF2 := .F.
	EndIf

	If MV_PAR02 == 1 //Factura Normal
		cTipoFact := "01"
	ElseIf MV_PAR02 == 2 //Factura de Traslado
		cTipoFact := "21"
	ElseIf MV_PAR02 == 3 //Ambas
		cTipoFact := "'01', '21'"
	EndIf
	
	If MV_PAR10 == 1
		lVisualPDF := .T.
	EndIf

	If MV_PAR01 == 1 .Or. MV_PAR01 == 2
		cQuery := "SELECT R_E_C_N_O_ RECNO "
		cQuery += "FROM " + RetSqlName('SF2') + " SF2 "
		cQuery += "WHERE F2_FILIAL = '" + xFilial("SF2") + "' "
		cQuery += "AND F2_ESPECIE = '" + cEspecie + "' "
		If MV_PAR01 == 1 //Facturas
			If MV_PAR02 == 1 //Factura Normal
				cQuery += "AND F2_TIPODOC = '" + cTipoFact + "' "
			ElseIf MV_PAR02 == 2 //Factura de Traslado
				cQuery += "AND F2_TIPODOC = '" + cTipoFact + "' "
			ElseIf MV_PAR02 == 3 //Ambas
				cQuery += "AND F2_TIPODOC IN (" + cTipoFact + ") "
			EndIf
		EndIf
		cQuery += "AND F2_EMISSAO >= '" + DTOS(MV_PAR03) + "' "
		cQuery += "AND F2_EMISSAO <= '" + DTOS(MV_PAR04) + "' "
		cQuery += "AND F2_SERIE >= '" + MV_PAR05 + "' "
		cQuery += "AND F2_SERIE <= '" + MV_PAR06 + "' "
		cQuery += "AND F2_DOC >= '" + MV_PAR07 + "' "
		cQuery += "AND F2_DOC <= '" + MV_PAR08 + "' "
		If MV_PAR09 == 1 //Documentos Timbrados
			cQuery += "AND F2_UUID <> '' "
			cQuery += "AND F2_FECTIMB <> '' "
		ElseIf MV_PAR09 == 2 //Documentos sin Timbre
			cQuery += "AND F2_UUID = '' "
			cQuery += "AND F2_FECTIMB = '' "
		EndIf
		cQuery += "AND SF2.D_E_L_E_T_= ' ' "
		cQuery += "ORDER BY F2_SERIE, F2_DOC"
	Else
		cQuery := "SELECT R_E_C_N_O_ RECNO "
		cQuery += "FROM " + RetSqlName('SF1') + " SF1 "
		cQuery += "WHERE F1_FILIAL = '" + xFilial("SF1") + "' "
		cQuery += "AND F1_ESPECIE = '" + cEspecie + "' "
		cQuery += "AND F1_EMISSAO >= '" + DTOS(MV_PAR03) + "' "
		cQuery += "AND F1_EMISSAO <= '" + DTOS(MV_PAR04) + "' "
		cQuery += "AND F1_SERIE >= '" + MV_PAR05 + "' "
		cQuery += "AND F1_SERIE <= '" + MV_PAR06 + "' "
		cQuery += "AND F1_DOC >= '" + MV_PAR07 + "' "
		cQuery += "AND F1_DOC <= '" + MV_PAR08 + "' "
		If MV_PAR09 == 1 //Documentos Timbrados
			cQuery += "AND F1_UUID <> '' "
			cQuery += "AND F1_FECTIMB <> '' "
		ElseIf MV_PAR09 == 2 //Documentos sin Timbre
			cQuery += "AND F1_FECTIMB = '' "
			cQuery += "AND F1_FECTIMB = '' "
		EndIf
		cQuery += "AND SF1.D_E_L_E_T_= ' ' "
		cQuery += "ORDER BY F1_SERIE, F1_DOC"
	EndIf

	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T., "TOPCONN", TcGenQry( , , cQuery), cAliasQry, .T., .T.)

	Count to nNumRegs
	
	If nNumRegs > 0
		(cAliasQry)->(DBGoTop()) //Se posiciona en el primer registro de la tabla
		
		If lTabSF2 //NF, NF Traslado y NDC
			DBSelectArea("SF2")
		Else //Nota de Credito
			DBSelectArea("SF1")
		EndIf
		
		While (cAliasQry)->(!Eof())
			nNumDocImp++
			
			If lTabSF2 //NF, NF Traslado y NDC
				SF2->(DBGoTo((cAliasQry)->RECNO))
				If lImpPer //PE para impresion de formato personalizado
					Processa({ |lEnd| ExecBlock("M475IMPPER",.F.,.F.,{SF2->F2_ESPECIE, SF2->F2_SERIE, SF2->F2_DOC, SF2->F2_TIPODOC, SF2->F2_CLIENTE, SF2->F2_LOJA, lVisualPDF})}, STR0053 + AllTrim(Str(nNumDocImp)) + "/" + AllTrim(Str(nNumRegs))) //"Imprimiendo documentos... "
                Else
                	Processa({ |lEnd| MATR475Gen(SF2->F2_ESPECIE, SF2->F2_SERIE, SF2->F2_DOC, SF2->F2_TIPODOC, SF2->F2_CLIENTE, SF2->F2_LOJA, lVisualPDF)}, STR0053 + AllTrim(Str(nNumDocImp)) + "/" + AllTrim(Str(nNumRegs))) //"Imprimiendo documentos... "
				EndIf
			Else //Nota de Credito
				SF1->(DBGoTo((cAliasQry)->RECNO))
				If lImpPer //PE para impresion de formato personalizado
					Processa({ |lEnd| ExecBlock("M475IMPPER",.F.,.F.,{SF1->F1_ESPECIE, SF1->F1_SERIE, SF1->F1_DOC, SF1->F1_TIPODOC, SF1->F1_FORNECE, SF1->F1_LOJA, lVisualPDF})}, STR0053 + AllTrim(Str(nNumDocImp)) + "/" + AllTrim(Str(nNumRegs))) //"Imprimiendo documentos... "
				Else
					Processa({ |lEnd| MATR475Gen(SF1->F1_ESPECIE, SF1->F1_SERIE, SF1->F1_DOC, SF1->F1_TIPODOC, SF1->F1_FORNECE, SF1->F1_LOJA, lVisualPDF)}, STR0053 + AllTrim(Str(nNumDocImp)) + "/" + AllTrim(Str(nNumRegs))) //"Imprimiendo documentos... "
				EndIf
			EndIf
			(cAliasQry)->(DBSkip())
		EndDo
	Else
		MsgInfo(STR0001) //"No se encontraron coincidencias con los par�metros informados, modifiquelos e intente nuevamente."
	EndIf
	
	(cAliasQry)->(DBCloseArea())

Return

/*
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
���������������������������������������������������������������������������������Ŀ��
���Fun��o    �MATR475Gen� Autor � Marco Augusto Gonzalez Rivera � Data �28/08/2018���
���������������������������������������������������������������������������������Ĵ��
���Descri��o � Generacion de PDF para Documentos Fiscales de Entrada/Salida.      ���
���          � Mediante rutina automatica o rutina manual.                        ���
���������������������������������������������������������������������������������Ĵ��
���Sintaxe   � MATR475Gen(ExpC1, ExpC2, ExpC3, ExpC4, ExpC5, ExpC6, ExpC7, ExpL1) ���
���������������������������������������������������������������������������������Ĵ��
���Parametros� ExpC1 = Especie del Documento (NF, NDC y NCC)                      ���
���          � ExpC2 = Serie del Documento                                        ���
���          � ExpC3 = Folio del Documento                                        ���
���          � ExpC4 = Tipo de Documento                                          ���
���          � ExpC5 = Cliente del Documento                                      ���
���          � ExpC6 = Tienda del Documento                                       ���
���          � ExpL1 = Informa si visualizara PDF tras Impresion.                 ���
���������������������������������������������������������������������������������Ĵ��
���Uso       � MATR475, MATA467N, MATA468N y MATA465N.                            ���
����������������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
/*/
Function MATR475Gen(cEspecie, cSerie, cNumDoc, cTipoDoc, cCliente, cTienda, lVisualPDF)

	Local aArea			:= GetArea()
	Local oXml			:= Nil
	Local cDirClient	:= GetClientDir()+ "pdf\"

	Private cFileName	:= ""
	Private nLin		:= 500
	Private oPrint		:= 	Nil
	Private cPathDocs	:= &(SuperGetmv("MV_CFDDOCS", .F., "\cfd\facturas\")) // Ruta donde se encuentran las facturas.xml (servidor)
	
	//Declaracion de variables privadas para mantenerlas hasta el final del proceso.
	Private cParamEspD	:= cEspecie		//Parametro para Especie de Documento
	Private cParamSerD	:= cSerie		//Parametro para Serie de Documento
	Private cParamDocD	:= cNumDoc		//Parametro para Folio de Documento
	Private cParamTpoD	:= cTipoDoc		//Parametro para Tipo de Documento
	Private cParamCliD	:= cCliente		//Parametro para Cliente de Documento
	Private cParamLojD	:= cTienda		//Parametro para Tienda de Documento

	cFileName := Lower(AllTrim(cParamEspD)) + '_' + Lower(AllTrim(cParamSerD)) + '_' + Lower(AllTrim(cParamDocD)) //Nombre de Archivo, en base a nomenclatura del archivo XML.
	
	If !ExistDir(cDirClient) //Valida que exista directorio auxiliar para crear PDF
		MakeDir(cDirClient)
	EndIf
	
	If File(cDirClient + cFileName + ".pdf") //Si existe una version impresa del Comprobante Fiscal, se elimina. (Directorio Auxiliar)
		FErase(cDirClient + cFileName + ".pdf")
	Endif
	
	If File(cPathDocs + cFileName + ".pdf") //Si existe una version impresa del Comprobante Fiscal, se elimina. (Directorio MV_CFDDOCS)
		FErase(cPathDocs + cFileName + ".pdf")
	Endif
	
	/*
	FWMsPrinter(): New ( < cFilePrintert >, [ nDevice], [ lAdjustToLegacy], [ cPathInServer], [ lDisabeSetup ], [ lTReport], [ @oPrintSetup], [ cPrinter], [ lServer], [ lPDFAsPNG], [ lRaw], [ lViewPDF] ) --> oPrinter 
	------------------|---------------|------------------------------------------------------------------------------------------------------------------------
	Nombre				Tipo			Descripci�n
	------------------|---------------|------------------------------------------------------------------------------------------------------------------------
	cFilePrintert		Caracter		Nome do arquivo de relat�rio a ser criado. 	X 	
	nDevice				Num�rico		Tipos de Sa�da aceitos: IMP_SPOOL Envia para impressora. IMP_PDF Gera arquivo PDF � partir do relat�rio. Default � IMP_SPOOL 		
	lAdjustToLegacy		L�gico			Se .T. recalcula as coordenadas para manter o legado de propor��es com a classe TMSPrinter. Default � .T. IMPORTANTE: Este c�lculos n�o funcionam corretamente quando houver ret�ngulos do tipo BOX e FILLRECT no relat�rio, podendo haver distor��es de algumas pixels o que acarretar� no encavalamento dos ret�ngulos no momento da impress�o. 		
	cPathInServer		Caracter		Diret�rio onde o arquivo de relat�rio ser� salvo 		
	lDisabeSetup		L�gico			Se .T. n�o exibe a tela de Setup, ficando � cargo do programador definir quando e se ser� feita sua chamada. Default � .F. 		
	lTReport			L�gico			Indica que a classe foi chamada pelo TReport. Default � .F. 		
	oPrintSetup			Objeto			Objeto FWPrintSetup instanciado pelo usu�rio. 		X
	cPrinter			Caracter		Impressora destino "for�ada" pelo usu�rio. Default � "" 		
	lServer				L�gico			Indica impress�o via Server (.REL N�o ser� copiado para o Client). Default � .F. 		
	lPDFAsPNG			L�gico			.T. Indica que ser� gerado o PDF no formato PNG. O Default � .T. 		
	lRaw				L�gico			.T. indica impress�o RAW/PCL, enviando para o dispositivo de impress�o caracteres bin�rios(RAW) ou caracteres program�veis espec�ficos da impressora(PCL) 
	------------------|---------------|------------------------------------------------------------------------------------------------------------------------
	*/
	
	oPrint := FWMsPrinter():New(AllTrim(cFileName) + ".pdf", 6, .T., , .T.)
	oPrint:SetViewPDF(lVisualPDF) //Define si se visualiza PDF
	oPrint:SetResolution(72)
	oPrint:SetPortrait()
	oPrint:SetPaperSize(1)
	oPrint:cPathPDF := cDirClient

	oXml := MATR475Xml() //Genera objeto a partir del XML del documento

	If oXml == Nil
		FreeObj(oPrint)
		Return .F.
	EndIf

	MATR575Imp(oXml) //Realiza Impresion de PDF
	
	nLin := 0
	
	oPrint:Print()
	COPY FILE (cDirClient + cFileName + ".pdf") TO (cPathDocs + cFileName + ".pdf")

	FreeObj(oPrint)
	oPrint := Nil
	FreeObj(oXml)
	oXml := Nil

	RestArea(aArea)

Return .T.

/*
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
���������������������������������������������������������������������������������Ŀ��
���Fun��o    �MATR475Xml� Autor � Marco Augusto Gonzalez Rivera � Data �28/08/2018���
���������������������������������������������������������������������������������Ĵ��
���Descri��o � Generacion de PDF para Documentos Fiscales de Entrada/Salida.      ���
���������������������������������������������������������������������������������Ĵ��
���Uso       � Facturacion - Mexico.                                              ���
����������������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
/*/
Function MATR475Xml()

	Local cPathXML	:= ""
	Local cAviso	:= ""
	Local cErro		:= ""
	Local oXml		:= Nil

	cPathXML := cPathDocs + cFileName + '.xml'

	If !File(cPathXML)
		MsgAlert(STR0002 + AllTrim(cParamSerD) + "-" + AllTrim(cParamDocD) + STR0003) //"El archivo XML del documento: " - " no fu� localizado. No ser� posible realizar la impresi�n del mismo."
		Return Nil
	EndIf

	oXml := XmlParserFile(cPathXML, "_", @cAviso,@cErro )

	If !Empty(cAviso) .Or. !Empty(cErro)
		MsgAlert(STR0004 + Chr(13)+Chr(10) + Upper(cAviso) + Chr(13)+Chr(10) + Upper(cErro)) //"Se detectaron problemas con el archivo XML: "
		Return(Nil)
	EndIf	

Return oXml

/*
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
���������������������������������������������������������������������������������Ŀ��
���Fun��o    �MATR575Imp� Autor � Marco Augusto Gonzalez Rivera � Data �28/08/2018���
���������������������������������������������������������������������������������Ĵ��
���Descri��o � Generacion de PDF para Documentos Fiscales de Entrada/Salida.      ���
���������������������������������������������������������������������������������Ĵ��
���Uso       � Facturacion - Mexico.                                              ���
����������������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
/*/
Function MATR575Imp(oXml)

	Private nPagNum		:= 1		// Indica el n�mero de p�gina actual.

	Private	 nEmiY 		:= 80		// Coordenadas de l�nea en donde inician los datos del emisor
	Private	 nFacX		:= 1700   	// Columna de inicio de datos de factura en encabezado
	Private nFacY		:= 80		// L�nea de inicio de datos de factura en enabezado
	Private nCliX		:= 100		// Columna de inicio de datos del cliente en encabezado
	Private nCliY		:= 330		// L�nea de inicio de datos del cliente en encabezado

	Private nDetX		:= 100 		// Columna de inicio de datos del detalle de la factura
	Private nDetY		:= 600		// L�nea de inicio de datos del detalle de la factura.
	Private nTamSalto	:= 30		// Tama�o del salto

	// Fuentes
	Private oAr07R  	:= TFont():New("Arial",07,07,,.F.,,,,.T.,.F.)
	Private oAr08R  	:= TFont():New("Arial",08,08,,.F.,,,,.T.,.F.)
	Private oAr09B  	:= TFont():New("Arial",09,09,,.T.,,,,.T.,.F.)
	Private oAr10R  	:= TFont():New("Arial",10,10,,.F.,,,,.T.,.F.)
	Private oAr10B  	:= TFont():New("Arial",10,10,,.T.,,,,.T.,.F.)
	Private oAr12B  	:= TFont():New("Arial",12,12,,.T.,,,,.T.,.F.)
	Private oAr14B  	:= TFont():New("Arial",14,14,,.T.,,,,.T.,.F.)
	
	//Datos del Documento
	Private cCerEmi		:= ""
	
	//Datos del Emisor
	Private	 cRFCEmisor	:= ""
	Private cNomEmisor	:= ""
	Private cRegFisEmi	:= ""
	
	//Datos del Receptor
	Private	 cUsoCFDI	:= ""
	Private cDesUsoCFD	:= ""
	Private cClientNom	:= ""
	Private cClientRFC	:= ""
	
	//Totales del documento
	Private cTotalDocu	:= ""
	Private nDescTot	:= 0
	Private cSubTot		:= ""
	Private nSubTot		:= ""
	
	Default oXml		:= Nil

	oPrint:StartPage() //Inicia nueva Pagina

	MATR475Enc(oXml) //Imprimir Encabezado de Documento
	MATR475Det(oXml) //Imprimir Detalle de Documento
	MATR475Pie(oXml) //Imprimir Pie de Documento

	oPrint:EndPage()

Return

/*
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
���������������������������������������������������������������������������������Ŀ��
���Fun��o    �MATR475Enc� Autor � Marco Augusto Gonzalez Rivera � Data �28/08/2018���
���������������������������������������������������������������������������������Ĵ��
���Descri��o � Generacion de PDF para Documentos Fiscales de Entrada/Salida.      ���
���������������������������������������������������������������������������������Ĵ��
���Uso       � Facturacion - Mexico.                                              ���
����������������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
/*/
Function MATR475Enc(oXml)

	Local cFileLogoR	:=  GetSrvProfString("Startpath","") + "lgrl.bmp" // Logo
	Local cFranja		:=  GetSrvProfString("Startpath","") + "Franja_factura10.bmp" // Franja de fondo de la factura
	
	// Datos de la Factura
	Local cFolio		:= ""
	Local cFechaXml		:= ""
	Local cFechaFac		:= ""
	Local cUUIDRel		:= ""
	Local cTpoRelSAT	:= ""
	Local cTpoCompro	:= ""
	Local cDescCompr	:= ""
	Local cLugarExpe	:= ""
	Local cTpoForPgo	:= ""
	Local cDesForPgo	:= ""
	Local cMetodoPgo	:= ""
	Local cDesMtdPgo	:= ""
	Local nLinSal 		:= 0
	// Datos del Cliente - genericos
	Local cClientDir	:= ""
	Local cClientCol	:= ""
	Local cCliNumExt	:= "S/N"
	Local cCliNumInt	:= "S/N"
	Local cClientMun	:= ""
	Local cClientEst	:= ""
	Local cClientPai	:= ""
	Local cClienteCP	:= ""
	
	// Datos del Timbrado Fiscal Digital
	Local cFechaTim		:= STR0005 // Fecha del Timbre Fiscal Digital		//"-- Sin Timbre Fiscal Digital --"
	Local cVerTim		:= STR0006 // Versi�n del Timbre Fiscal Digital		//"Documento Inv�lido."
	Local cUUIDTim		:= STR0006 // Folio Fiscal							//"Documento Inv�lido."
	Local cCerTim		:= STR0006 // No. Certificado Timbre Fiscal Digital	//"Documento Inv�lido."
	
	//Valida que exista atributo Folio Fiscal
	If XMLChildEx(oXml:_CFDI_COMPROBANTE, "_FOLIO") <> Nil
		cFolio := OemToAnsi(oXml:_CFDI_COMPROBANTE:_FOLIO:TEXT)
	EndIf
	//Valida que exista atributo Fecha
	If XMLChildEx(oXml:_CFDI_COMPROBANTE, "_FECHA") <> Nil
		cFechaXml := oXml:_CFDI_COMPROBANTE:_FECHA:TEXT
		cFechaFac := SubStr(cFechaXml,9,2)+"/"+SubStr(cFechaXml,6,2)+"/"+SubStr(cFechaXml,1,4)+" "+SubStr(cFechaXml,12,8)
	EndIf
	//Valida que exista atributo Tipo de Comprobante
	If XMLChildEx(oXml:_CFDI_COMPROBANTE, "_TIPODECOMPROBANTE") <> Nil
		cTpoCompro := oXml:_CFDI_COMPROBANTE:_TIPODECOMPROBANTE:TEXT
	EndIf
	//Valida que exista atributo Lugar de Expedicion
	If XMLChildEx(oXml:_CFDI_COMPROBANTE, "_LUGAREXPEDICION") <> Nil
		cLugarExpe := oXml:_CFDI_COMPROBANTE:_LUGAREXPEDICION:TEXT
	EndIf
	//Valida que exista atributo Total
	If XMLChildEx(oXml:_CFDI_COMPROBANTE, "_TOTAL") <> Nil
		cTotalDocu := oXml:_CFDI_COMPROBANTE:_TOTAL:TEXT
	EndIf
	//Valida que exista atributo Descuento
	If XMLChildEx(oXml:_CFDI_COMPROBANTE, "_DESCUENTO") <> Nil
		nDescTot := Val(OemToAnsi(oXml:_CFDI_COMPROBANTE:_DESCUENTO:TEXT))
	EndIf
	//Valida que exista atributo SubTotal
	If XMLChildEx(oXml:_CFDI_COMPROBANTE, "_SUBTOTAL") <> Nil
		cSubTot := Transform(Val(OemToAnsi(oXML:_CFDI_COMPROBANTE:_SUBTOTAL:TEXT)),"999,999,999.99")
		nSubTot := Val(OemToAnsi(oXML:_CFDI_COMPROBANTE:_SUBTOTAL:TEXT))
	EndIf
	//Valida que exista atributo Numero de Certificado
	If XMLChildEx(oXml:_CFDI_COMPROBANTE, "_NOCERTIFICADO") <> Nil
		cCerEmi := OemToAnsi(oXml:_CFDI_COMPROBANTE:_NOCERTIFICADO:TEXT)
	EndIf
	
	//Valida que exista Nodo con Informacion del Emisor
	If XMLChildEx(oXml:_CFDI_COMPROBANTE, "_CFDI_EMISOR") <> Nil
		cRFCEmisor := oXml:_CFDI_COMPROBANTE:_CFDI_EMISOR:_RFC:TEXT
		cNomEmisor := oXml:_CFDI_COMPROBANTE:_CFDI_EMISOR:_NOMBRE:TEXT
		cRegFisEmi := oXml:_CFDI_COMPROBANTE:_CFDI_EMISOR:_REGIMENFISCAL:TEXT
	EndIf
	
	//Valida que exista Nodo con Informacion del Receptor
	If XMLChildEx(oXml:_CFDI_COMPROBANTE, "_CFDI_RECEPTOR") <> Nil
		cUsoCFDI := oXml:_CFDI_COMPROBANTE:_CFDI_RECEPTOR:_USOCFDI:TEXT
		cDesUsoCFD := ObtColSAT("S013",AllTrim(cUsoCFDI),1,3,4,90)
		cClientNom := MTR475CarE(oXml:_CFDI_COMPROBANTE:_CFDI_RECEPTOR:_NOMBRE:TEXT)
		cClientRFC := MTR475CarE(AllTrim(oXml:_CFDI_COMPROBANTE:_CFDI_RECEPTOR:_RFC:TEXT))
	EndIf	
	
	//Valida que exista Nodo de UUID Relacionados
	If XMLChildEx(oXml:_CFDI_COMPROBANTE, "_CFDI_CFDIRELACIONADOS") <> Nil
		cUUIDRel := oXml:_CFDI_COMPROBANTE:_CFDI_CFDIRELACIONADOS:_CFDI_CFDIRELACIONADO:_UUID:TEXT
		cTpoRelSAT := oXml:_CFDI_COMPROBANTE:_CFDI_CFDIRELACIONADOS:_TIPORELACION:TEXT
	EndIf
	
	If AllTrim(cParamTpoD) != "21"
		cTpoForPgo := oXml:_CFDI_COMPROBANTE:_FORMAPAGO:TEXT
		cDesForPgo := ObtColSAT("S005",AllTrim(cTpoForPgo),1,2,3,40)
		cMetodoPgo := oXml:_CFDI_COMPROBANTE:_METODOPAGO:TEXT
		cDesMtdPgo := ObtColSAT("S007",AllTrim(cMetodoPgo),1,3,4,38)
	EndIf
	
	If nPagNum == 1
		If cTpoCompro == "I"
			cDescCompr := STR0007 //"Ingreso"
		ElseIf cTpoCompro == "E"
			cDescCompr := STR0008 //"Egreso"
		ElseIf cTpoCompro == "T"
			cDescCompr := STR0009 //"Traslado"
		EndIf
	EndIf

	// Verificar si est� timbrado para obtener fecha del TFD.
	If XMLChildEx(oXml:_CFDI_COMPROBANTE, "_CFDI_COMPLEMENTO") <> Nil
		If XMLChildEx(oXml:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO, "_TFD_TIMBREFISCALDIGITAL") <> Nil
			cFechaTim 	:= oXml:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO:_TFD_TIMBREFISCALDIGITAL:_FECHATIMBRADO:TEXT
			cFechaTim	:= (SubStr(cFechaTim, 9, 2) + "/" + SubStr(cFechaTim, 6, 2) + "/" + SubStr(cFechaTim, 1, 4) + " " + SubStr(cFechaTim, 12, 8))
			cVerTim 	:= oXml:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO:_TFD_TIMBREFISCALDIGITAL:_VERSION:TEXT
			cUUIDTim 	:= oXml:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO:_TFD_TIMBREFISCALDIGITAL:_UUID:TEXT
			cCerTim 	:= oXml:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO:_TFD_TIMBREFISCALDIGITAL:_NOCERTIFICADOSAT:TEXT
		EndIf
	EndIf

	DBSelectArea("SA1")
	SA1->(DBSetOrder(1)) //A1_FILIAL+A1_COD+A1_LOJA
	If SA1->(MsSeek(xFilial("SA1") + cParamCliD + cParamLojD))
		cClientDir	:= RTrim(SA1->A1_END)
		cCliNumExt	:= AllTrim(SA1->A1_NR_END)
		cCliNumInt	:= AllTrim(SA1->A1_NROINT)
		cClientCol	:= Rtrim(SA1->A1_BAIRRO)
		cClientMun	:= RTrim(SA1->A1_MUN)
		cClientEst	:= RTrim(SA1->A1_ESTADO)
		cClientPai	:= RTrim(Posicione("SYA", 1, xFilial("SYA")+SA1->A1_PAIS, "YA_DESCR"))
		cClienteCP	:= AllTrim(SA1->A1_CEP)
	Else
		MsgInfo(STR0054 + cParamSerD + " - " +  cParamDocD) //"No se ha encontrado el Cliente utilizado en el Documento Fiscal: "
	EndIf

	// Imprimir Logo de emisor
	// ---------------------------------------------------------------------
	oPrint:SayBitmap(85	, 100	, cFileLogoR	, 200, 200	) //y, x, archivo, ancho, alto
	oPrint:SayBitmap(1	, 1750	, cFranja		, 565, 3050	) //y, x, archivo, ancho, alto

	// Datos de la empresa
	// ---------------------------------------------------------------------
	oPrint:SayAlign(nEmiY					, 1, UTf8ToChr(cNomEmisor)									, oAr12B, 2300, 70, , 2, 0)
	oPrint:SayAlign(nEmiY + (nTamSalto*1)	, 1, RTRIM(SM0->M0_ENDENT) + ", " + RTRIM(SM0->M0_BAIRENT)	, oAr10R, 2300, 70, , 2, 0)
	oPrint:SayAlign(nEmiY + (nTamSalto*2)	, 1, RTRIM(SM0->M0_CIDENT) + ", " + RTRIM(SM0->M0_CEPENT)	, oAr10R, 2300, 70, , 2, 0)
	oPrint:SayAlign(nEmiY + (nTamSalto*3)	, 1, "R.F.C. " + cRFCEmisor									, oAr10R, 2300, 70, , 2, 0)

	// Layout del encabezado
	// -----------------------------------------------------------------------	
	If AllTrim(cParamEspD) == "NF" //Factura
		oPrint:SayAlign(nFacY - 40	, nFacX, STR0010, oAr14B, 500, 50, , 2, 0) //"FACTURA"
		oPrint:SayAlign(nFacY		, nFacX, STR0011, oAr12B, 500, 50, , 2, 0) //"SERIE/FOLIO INTERNO"
	ElseIf AllTrim(cParamEspD) == "NDC" //Nota de Debito
		oPrint:SayAlign(nFacY - 40	, nFacX, STR0012, oAr14B, 500, 50, , 2, 0) //"NOTA DE CARGO"
		oPrint:SayAlign(nFacY		, nFacX, STR0011, oAr12B, 500, 50, , 2, 0) //"SERIE/FOLIO INTERNO"
	ElseIf AllTrim(cParamEspD) == "NCC" //Nota de Credito
		oPrint:SayAlign(nFacY - 40	, nFacX, STR0013, oAr14B, 500, 50, , 2, 0) //"NOTA DE CR�DITO"
		oPrint:SayAlign(nFacY		, nFacX, STR0011, oAr12B, 500, 50, , 2, 0) //"SERIE/FOLIO INTERNO"
	EndIf

	oPrint:SayAlign(nFacY + (nTamSalto*1)	, nFacX, AllTrim(cParamSerD) + " - " + AllTrim(cFolio)	, oAr10R, 500, 30, , 2, 0)
	oPrint:SayAlign(nFacY + (nTamSalto*3)	, nFacX, STR0014 + cFechaFac 							, oAr10B, 500, 30, , 2, 0) //"Fecha de emisi�n: "
	oPrint:SayAlign(nFacY + (nTamSalto*5)	, nFacX, STR0015 + "(" + cTpoCompro + ") " + cDescCompr	, oAr10R, 500, 30, , 2, 0) //"Tipo de Comprobante: "

	oPrint:Line(nFacY + (nTamSalto*8), 100, nFacY + (nTamSalto*8), 2200)

	// Datos del Receptor
	oPrint:Say(nCliY + (nTamSalto*1)	, nCliX + 20, UTf8ToChr(cClientNom)															        , oAr12B, , CLR_BLACK, , 2)
	oPrint:Say(nCliY + (nTamSalto*3)	, nCliX + 20, STR0016 + cClientRFC															, oAr10R, , CLR_BLACK, , 2) //"RFC: "
	oPrint:Say(nCliY + (nTamSalto*4)	, nCliX + 20, STR0017																		, oAr10R, , CLR_BLACK, , 2) //"Domicilio: "
	oPrint:Say(nCliY + (nTamSalto*5)	, nCliX + 20, cClientDir + STR0018 + cCliNumExt + "-" + cCliNumInt + STR0019 + cClientCol	, oAr10R, , CLR_BLACK, , 2) //" No. " - " Col. "
	oPrint:Say(nCliY + (nTamSalto*6)	, nCliX + 20, STR0020 + cClientMun + ", " + STR0021 + cClienteCP							, oAr10R, , CLR_BLACK, , 2) //"Municipio " - "C.P. "
	oPrint:Say(nCliY + (nTamSalto*7)	, nCliX + 20, STR0022 + cUsoCFDI + " - " + RTrim(cDesUsoCFD)								, oAr10R, , CLR_BLACK, , 2) //"Uso del CFDI: "
	If !Empty(cUUIDRel) .And. !Empty(cTpoRelSAT)
		oPrint:Say(nCliY + (nTamSalto*8), nCliX + 20, STR0023 + cUUIDRel	, oAr10R, , CLR_BLACK, , 2) //"UUID CFDI relacionado: "
		oPrint:Say(nCliY + (nTamSalto*9), nCliX + 20, STR0024 + cTpoRelSAT	, oAr10R, , CLR_BLACK, , 2) //"Tipo de Relacion: "
	EndIf
	// Folio Fiscal
	oPrint:Say(nCliY					, nCliX			, ""		, oAr08R, , CLR_BLACK, , 2)
	oPrint:Say(nCliY + (nTamSalto*1)	, nCliX + 1500	, STR0025	, oAr12B, , CLR_BLACK, , 2) //"Folio fiscal"
	oPrint:Say(nCliY + (nTamSalto*2)	, nCliX + 1500	, cUUIDTim	, oAr10R, , CLR_BLACK, , 2)

	// Lugar de expedici�n
	oPrint:Say(nCliY					, nCliX			, ""												, oAr08R, , CLR_BLACK, , 2)
	nLinSal := 6
	If AllTrim(cParamTpoD) != "21"
		oPrint:Say(nCliY + (nTamSalto * nLinSal), nCliX + 1500	, STR0026	, oAr10R, , CLR_BLACK, , 2) //"Forma de Pago: "
		F815LinCar(oPrint,nCliY + (nTamSalto * nLinSal),nCliX + 1680, cTpoForPgo + " - " + RTrim(cDesForPgo), 31, 20, 2, oAr10R,@nLinSal)	
		nLinSal := nLinSal + 6
	EndIf
	nLinSal := nLinSal + 1
	oPrint:Say(nCliY + (nTamSalto * nLinSal)	, nCliX + 1500	, STR0027 + cLugarExpe								, oAr10R, , CLR_BLACK, , 2) //"Lugar de Expedici�n: "
	If AllTrim(cParamTpoD) != "21"
		nLinSal := nLinSal + 1
		oPrint:Say(nCliY + (nTamSalto * nLinSal), nCliX + 1500	, STR0028 + cMetodoPgo + " - " + RTrim(cDesMtdPgo)	, oAr10R, , CLR_BLACK, , 2) //"M�todo de Pago: "
	EndIf
	nLinSal := nLinSal + 1
	oPrint:Say(nCliY + (nTamSalto * nLinSal)	, nCliX + 1500	, STR0029 + cRegFisEmi + " - " + ObtColSAT("S010",cRegFisEmi,1,3,4,80) 	, oAr10R, , CLR_BLACK, , 2) //"R�gimen Fiscal: "
	
	// Encabezado de las columnas de datos de los Productos.
	oPrint:Say(nDetY + (nTamSalto*2),	nDetX,"",oAr09B,,CLR_BLACK,,2)
	
	oPrint:SayAlign(nDetY + (nTamSalto*3), nDetX		, STR0030 , oAr12B, 200		, 60, , 2, 0) //"Cantidad"
	oPrint:SayAlign(nDetY + (nTamSalto*3), nDetX + 200	, STR0031 , oAr12B, 200		, 60, , 2, 0) //"Unidad"
	oPrint:SayAlign(nDetY + (nTamSalto*3), nDetX + 400	, STR0032 , oAr12B, 1000	, 60, , 2, 0) //"Concepto"
	oPrint:SayAlign(nDetY + (nTamSalto*3), nDetX + 1400	, STR0033 , oAr12B, 300		, 60, , 2, 0) //"P.Unitario"
	oPrint:SayAlign(nDetY + (nTamSalto*3), nDetX + 1700	, STR0034 , oAr12B, 300		, 60, , 2, 0) //"Importe"

	oPrint:Line(nDetY+(nTamSalto*4)+15,100,nDetY+(nTamSalto*4)+15,2200)
	hdrHeight := (nTamSalto*4)	
	nFall := 4
	
Return

/*
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
���������������������������������������������������������������������������������Ŀ��
���Fun��o    �MATR475Det� Autor � Marco Augusto Gonzalez Rivera � Data �28/08/2018���
���������������������������������������������������������������������������������Ĵ��
���Descri��o � Generacion de PDF para Documentos Fiscales de Entrada/Salida.      ���
���������������������������������������������������������������������������������Ĵ��
���Uso       � Facturacion - Mexico.                                              ���
����������������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
/*/
Function MATR475Det(oXml)

	Local nCurLine		:= nDetY
	Local nCurItem		:= 1
	Local nFall			:= 4
	Local nSaltoItem	:= 35	// Salto entre items
  	Local nItems		:= 0
  	Local cPagina		:= AllTrim(Str(oPrint:nPageCount))
  	Local aItems		:= {}
  	Local nIteration	:= 0
  	
  	// Datos del detalle
  	Local cCantiItem	:= "" //Cantidad Item	
  	Local cUnidaItem	:= "" //Unitad Item
  	Local cConceItem	:= "" //Concepto Item
  	Local cValUniItm	:= "" //Valor Unitario Item
  	Local cImporItem	:= "" //Importe Item
  	
  	// Quiebre de Textos
  	Local _lQuebra 	:= .F.
  	Local _nQuebra	:= 0
  	Local _nX		:= 0
  	Local _nTam		:= 0
  	
  	//Valida que exista Nodo de UUID Relacionados
	If XMLChildEx(oXml:_CFDI_COMPROBANTE, "_CFDI_CONCEPTOS") <> Nil
		If ValType(oXml:_CFDI_COMPROBANTE:_CFDI_CONCEPTOS:_CFDI_CONCEPTO) == "A"
			aItems := aAdd(aItems, {oXml:_CFDI_COMPROBANTE:_CFDI_CONCEPTOS:_CFDI_CONCEPTO})
			nItems := Len(aItems[1])
		Else
			nItems := 1
		EndIf
	Else
		nItems := 1
	EndIf
	
	For nIteration := 1 To nItems
		
		If (nCurLine > 1900) // Se define el tamano maximo que tendran la seccion de items.
			oPrint:SayAlign(3000, 1750, STR0050 + cPagina	, oAr10R, 550, 30, , 2, 0) //"Pagina: "
			MTR475NewP(oXml)
			hdrHeight := (nTamSalto*2)
			nFall := 4
			nCurLine := nDetY
		EndIf
		
		//Datos del cuerpo de la factura  	  
		If nItems > 1 // Mas de un producto por documento fiscal
			cCantiItem := Transform(Val(oXml:_CFDI_COMPROBANTE:_CFDI_CONCEPTOS:_CFDI_CONCEPTO[nIteration]:_CANTIDAD:TEXT),"999,999.99")
			cUnidaItem := oXml:_CFDI_COMPROBANTE:_CFDI_CONCEPTOS:_CFDI_CONCEPTO[nIteration]:_UNIDAD:TEXT
			cConceItem := UTf8ToChr(oXml:_CFDI_COMPROBANTE:_CFDI_CONCEPTOS:_CFDI_CONCEPTO[nIteration]:_DESCRIPCION:TEXT)
			cValUniItm := ALLTRIM(oXml:_CFDI_COMPROBANTE:_CFDI_CONCEPTOS:_CFDI_CONCEPTO[nIteration]:_VALORUNITARIO:TEXT)
			cImporItem := Transform(Val(oXml:_CFDI_COMPROBANTE:_CFDI_CONCEPTOS:_CFDI_CONCEPTO[nIteration]:_IMPORTE:TEXT),"999,999,999.99")
		Else
			cCantiItem := Transform(Val(oXml:_CFDI_COMPROBANTE:_CFDI_CONCEPTOS:_CFDI_CONCEPTO:_CANTIDAD:TEXT),"999,999.99")
			cUnidaItem := oXml:_CFDI_COMPROBANTE:_CFDI_CONCEPTOS:_CFDI_CONCEPTO:_UNIDAD:TEXT
			cConceItem := UTf8ToChr(oXml:_CFDI_COMPROBANTE:_CFDI_CONCEPTOS:_CFDI_CONCEPTO:_DESCRIPCION:TEXT)
			cValUniItm := ALLTRIM(oXml:_CFDI_COMPROBANTE:_CFDI_CONCEPTOS:_CFDI_CONCEPTO:_VALORUNITARIO:TEXT)
			cImporItem := Transform(Val(oXml:_CFDI_COMPROBANTE:_CFDI_CONCEPTOS:_CFDI_CONCEPTO:_IMPORTE:TEXT),"999,999,999.99")
		EndIf
		
		//Salto de Linea para los Productos
		If Len(AllTrim(cConceItem)) > 80 // Valida que la descripcion del producto sea de 80 caracteres
			_lQuebra := .T.
			
			If Mod(Len(AllTrim(cConceItem)), 80) == 0
				_nQuebra := Len(AllTrim(cConceItem)) / 80
			Else	
				_nQuebra := Int((Len(AllTrim(cConceItem)))/80) + 1			
			EndIf
		EndIf
		// Resaliza saldo de linea en base al tamano de descripcion del producto (80)
		If _lQuebra
			oPrint:SayAlign(nDetY + (nSaltoItem*nFall),	nDetX		, cCantiItem, oAr10R, 200, 30, , 1, 0)
		    oPrint:SayAlign(nDetY + (nSaltoItem*nFall),	nDetX+200	, cUnidaItem, oAr10R, 200, 30, , 2, 0)
		    oPrint:SayAlign(nDetY + (nSaltoItem*nFall),	nDetX+1400	, cValUniItm, oAr10R, 300, 30, , 1, 0)
		    oPrint:SayAlign(nDetY + (nSaltoItem*nFall),	nDetX+1700	, cImporItem, oAr10R, 300, 30, , 1, 0)
		    
		    For _nX := 1 To _nQuebra
		    	If _nX == 1
		    		_nTam := 1
		    	Else
		    		_nTam := ((_nX - 1) * 80) + 1
		    		nFall ++
		    	EndIf	
		    	oPrint:SayAlign(nDetY+(nSaltoItem*nFall),	nDetX+400,	SubStr(cConceItem,_nTam,80),	oAr10R,1500	,30,,0,0)
		    Next _nX			
		Else			   
		    oPrint:SayAlign(nDetY + (nSaltoItem*nFall),	nDetX		, cCantiItem, oAr10R, 200	, 30, , 1, 0)
		    oPrint:SayAlign(nDetY + (nSaltoItem*nFall),	nDetX+200	, cUnidaItem, oAr10R, 200	, 30, , 2, 0)
		    oPrint:SayAlign(nDetY + (nSaltoItem*nFall),	nDetX+400	, cConceItem, oAr10R, 1500	, 30, , 0, 0)
		    oPrint:SayAlign(nDetY + (nSaltoItem*nFall),	nDetX+1400	, cValUniItm, oAr10R, 300	, 30, , 1, 0)
		    oPrint:SayAlign(nDetY + (nSaltoItem*nFall),	nDetX+1700	, cImporItem, oAr10R, 300	, 30, , 1, 0)
	   	EndIf

		nCurLine += nSaltoItem
   		
   		nCurItem++
		nFall++
		_lQuebra := .F.
	Next nIteration
	
	nCurLine += 10

Return

/*
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
���������������������������������������������������������������������������������Ŀ��
���Fun��o    �MATR475Pie� Autor � Marco Augusto Gonzalez Rivera � Data �28/08/2018���
���������������������������������������������������������������������������������Ĵ��
���Descri��o � Generacion de PDF para Documentos Fiscales de Entrada/Salida.      ���
���������������������������������������������������������������������������������Ĵ��
���Uso       � Facturacion - Mexico.                                              ���
����������������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
/*/
Function MATR475Pie(oXml)

	Local nFall			:= 1
	Local nCadLn		:= 0
	Local cCadOrig 		:= ""
	Local cImpEnLetr	:= ""
	Local cTotal		:= ""
	Local cTasaCuota	:= ""
	Local cImporTras	:= ""
	Local aImpTras      := {}
	Local cImporRete	:= ""
	Local nFotX			:= 100  // Columna en donde comienza la impresion del Pie del Documento.
	Local nFotY			:= 1900 // Linea en donde comienza la impresion del Pie del Documento.
	Local cPagina		:= AllTrim(Str(oPrint:nPageCount))
	Local cCerSAT		:= STR0006 //"Documento Inv�lido." 
	Local cCertQR		:= ""
	Local nIteration	:= 0
	Local cFechaTim		:= ""
	Local cSelloSAT		:= ""
	Local cSello		:= ""
	Local cUUID			:= ""
	Local nX            := 0
	Local cTsa          := ""
	
	If AllTrim(cParamTpoD) != "21"
		// Verificar que exista nodo de Traslados
		If XMLChildEx(oXml:_CFDI_COMPROBANTE, "_CFDI_IMPUESTOS") <> Nil
			If XMLChildEx(oXml:_CFDI_COMPROBANTE:_CFDI_IMPUESTOS, "_CFDI_TRASLADOS") <> Nil 
				If Valtype(oXml:_CFDI_COMPROBANTE:_CFDI_IMPUESTOS:_CFDI_TRASLADOS:_CFDI_TRASLADO) == "A"
					For nX := 1 To Len(oXml:_CFDI_COMPROBANTE:_CFDI_IMPUESTOS:_CFDI_TRASLADOS:_CFDI_TRASLADO)
					    cTsa := &("oXml:_CFDI_COMPROBANTE:_CFDI_IMPUESTOS:_CFDI_TRASLADOS:_CFDI_TRASLADO[" + Str(nX) + "]:_TASAOCUOTA:TEXT")	
						cTasaCuota := AllTrim(Str(Val(cTsa) * 100))
						cImporTras := &("oXml:_CFDI_COMPROBANTE:_CFDI_IMPUESTOS:_CFDI_TRASLADOS:_CFDI_TRASLADO[" + Str(nX) + "]:_IMPORTE:TEXT")
						Aadd(aImpTras,{cTasaCuota, cImporTras})
					Next nX	
				Else
					cTasaCuota	:= AllTrim(Str(Val(oXml:_CFDI_COMPROBANTE:_CFDI_IMPUESTOS:_CFDI_TRASLADOS:_CFDI_TRASLADO:_TASAOCUOTA:TEXT) * 100))
					cImporTras	:= oXml:_CFDI_COMPROBANTE:_CFDI_IMPUESTOS:_CFDI_TRASLADOS:_CFDI_TRASLADO:_IMPORTE:TEXT
					Aadd(aImpTras,{cTasaCuota, cImporTras})				
				EndIf

			EndIf
		EndIf
	EndIf
	
	// Verificar si est� timbrado para obtener Fecha / Certificado SAT / Sello SAT / UUID
	If XMLChildEx(oXml:_CFDI_COMPROBANTE, "_CFDI_COMPLEMENTO") <> Nil
		If XMLChildEx(oXml:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO, "_TFD_TIMBREFISCALDIGITAL") <> Nil
			cFechaTim := oXml:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO:_TFD_TIMBREFISCALDIGITAL:_FECHATIMBRADO:TEXT
			cCerSAT	:= oXml:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO:_TFD_TIMBREFISCALDIGITAL:_NOCERTIFICADOSAT:TEXT
			cSelloSAT := oXml:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO:_TFD_TIMBREFISCALDIGITAL:_SELLOSAT:TEXT
			cUUID := oXml:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO:_TFD_TIMBREFISCALDIGITAL:_UUID:TEXT
		EndIf
	EndIf

	If AllTrim(cParamTpoD) != "21"
		// Verificar que exista nodo de Impuestos
		If XMLChildEx(oXml:_CFDI_COMPROBANTE, "_CFDI_IMPUESTOS") <> Nil
			If XMLChildEx(oXml:_CFDI_COMPROBANTE:_CFDI_IMPUESTOS, "_TOTALIMPUESTOSRETENIDOS") <> Nil
				cImporRete := Transform(Val(OemToAnsi(oXml:_CFDI_COMPROBANTE:_CFDI_IMPUESTOS:_TOTALIMPUESTOSRETENIDOS:TEXT)),"999,999,999.99")
			EndIf
		EndIf
	EndIf

	// Importe y totales
	cImpEnLetr	:= MTR475CanL(cTotalDocu, cParamDocD, cParamSerD) // Obtener la cantidad en Letra
	cTotal		:= Transform(Val(cTotalDocu), "999,999,999.99")
	
	// Impresion Certificados
	oPrint:Line(nFotY+(nTamSalto*0),100,nFotY+(nTamSalto*0),2200)
	
	oPrint:Say(nFotY + (nTamSalto*18), nFotX+20	, STR0036		, oAr12B, , CLR_BLACK, , 2) //"Importe con letra"
	oPrint:Say(nFotY + (nTamSalto*19), nFotX+20	, cImpEnLetr	, oAr10R, , CLR_BLACK, , 2)
	
	oPrint:Say(nFotY + (nTamSalto*21), nFotX+20	, STR0035		, oAr12B, , CLR_BLACK, , 2) //"No. Certificado Digital"
	oPrint:Say(nFotY + (nTamSalto*22), nFotX+20	, cCerEmi		, oAr10R, , CLR_BLACK, , 2) 

	oPrint:Say(nFotY + (nTamSalto*21), nFotX+400, STR0037		, oAr12B, , CLR_BLACK, , 2) //"No. de Certificado del SAT"
	oPrint:Say(nFotY + (nTamSalto*22), nFotX+400, cCerSAT		, oAr10R, , CLR_BLACK, , 2)
	
	oPrint:Say(nFotY + (nTamSalto*21), nFotX+800, STR0052		, oAr12B, , CLR_BLACK, , 2) //"Fecha y Hora de Certificaci�n"
	oPrint:Say(nFotY + (nTamSalto*22), nFotX+800, cFechaTim		, oAr10R, , CLR_BLACK, , 2)
	
	//Seccion de Totales
	oPrint:SayAlign(nFotY + (nTamSalto*2),	nFotX+1300,	STR0038	, oAr10R, 700, 30, , 0, 0) //"Subtotal"
	oPrint:SayAlign(nFotY +( nTamSalto*2),	nFotX+1700,	cSubTot	, oAr10R, 300, 30, , 1, 0)  
	           
	k := 0
	If (nDescTot > 0) 
		oPrint:SayAlign(nFotY + (nTamSalto*3), nFotX+1300, STR0039												, oAr10R, 700, 30, , 0, 0) //"Descuentos"	
		oPrint:SayAlign(nFotY + (nTamSalto*3), nFotX+1700, Transform(nDescTot, "@E 999,999,999.99")			, oAr10R, 300, 30, , 1, 0)
		
		oPrint:SayAlign(nFotY + (nTamSalto*5), nFotX+1300, STR0040												, oAr10R, 700, 30, , 0, 0) //"Subtotal c/desc"	   
		oPrint:SayAlign(nFotY + (nTamSalto*5), nFotX+1700, Transform((nSubTot - nDescTot), "@E 999,999,999.99"), oAr10R, 300, 30, , 1, 0)
		k += 3
	EndIf
	
	For nX := 1 To Len(aImpTras)
		oPrint:SayAlign(nFotY + (nTamSalto*(4+k)), nFotX+1300, STR0041 + aImpTras[nX,1] + "%"							, oAr10R, 700, 30, , 0, 0) //"IVA "	
		oPrint:SayAlign(nFotY + (nTamSalto*(4+k)), nFotX+1700, Transform(Val(aImpTras[nX,2]), "@E 999,999,999.99")		, oAr10R, 300, 30, , 1, 0)
		k++	
    Next nX                        
	If (!Empty(cImporRete))
		k++
		oPrint:SayAlign(nFotY + (nTamSalto*(4+k)), nFotX+1300, STR0042											, oAr10R, 700, 30, , 0, 0) //"Retenciones "	
		oPrint:SayAlign(nFotY + (nTamSalto*(4+k)), nFotX+1700, Transform(Val(cImporRete), "@E 999,999,999.99")	, oAr10R, 300, 30, , 1, 0)	
	EndIf
	
	oPrint:Line(nFotY + (nTamSalto*10),1300, nFotY+(nTamSalto*10),2200)
	oPrint:SayAlign(nFotY + (nTamSalto*10)+5, nFotX+1300, STR0043												, oAr10R, 700, 30, , 0, 0) //"Total"	
	oPrint:SayAlign(nFotY + (nTamSalto*10)+5, nFotX+1700, cTotal												, oAr10R, 300, 30, , 1, 0)	
		                   	
	// Datos de Sellado
	nFall += 22	//Indica la posicion donde se imprimiran los datos del sellado	
	MTR475CadO(@cCadOrig, oXml) // Construir la cadena original

    // Cadena Original del Complemento de certificaci�n digital del SAT
	oPrint:Line(nFotY+(nTamSalto*nFall),100,nFotY+(nTamSalto*nFall),2200)
	oPrint:Say(	nFotY+(nTamSalto*(nFall+1)),	nFotX+20,	STR0044,	oAr09B,,CLR_BLACK,,2) //"Cadena Original del Complemento de Certificaci�n Digital del SAT"
	
	nFall += 2                                                      
	
	For nIteration := 1 To Len(cCadOrig) Step 200
		oPrint:Say(	nFotY+(nTamSalto*(nFall+nCadLn)),	nFotX+20,	SubStr(cCadOrig, nIteration, 200), oAr07R, , CLR_BLACK, , 2)
		nCadLn++
		If (nCadLn == 10)
			Exit
		EndIf
	Next nIteration
         
    // Sello Digital del Emisor
    nFall += 3
    nCadLn := 0           
    
	oPrint:Line(nFotY+(nTamSalto*nFall),100,nFotY+(nTamSalto*nFall),2200)
	oPrint:Say(	nFotY+(nTamSalto*(nFall+1)),	nFotX+20,	STR0045,	oAr09B,,CLR_BLACK,,2) //"Sello Digital del CFDI"
	nFall += 2
	
	//Valida que exista atributo Folio Fiscal
	If XMLChildEx(oXml:_CFDI_COMPROBANTE, "_SELLO") <> Nil
		cSello := OemToAnsi(oXml:_CFDI_COMPROBANTE:_SELLO:TEXT)
	EndIf                                                   

	For nIteration := 1 To Len(cSello) Step 200
		oPrint:Say(nFotY + (nTamSalto*(nFall+nCadLn)), nFotX+20, SubStr(cSello,nIteration,200),oAr07R,,CLR_BLACK,,2)
		nCadLn++
		If (nCadLn == 4)
			Exit
		EndIf
	Next nIteration

    // Sello Digital del SAT
    nFall += 2
    nCadLn := 0           
    
	oPrint:Line(nFotY+(nTamSalto*nFall),100,nFotY+(nTamSalto*nFall),2200)
	oPrint:Say(	nFotY+(nTamSalto*(nFall+1)),	nFotX+20,	STR0046,	oAr09B,,CLR_BLACK,,2) //"Sello Digital del SAT"
	nFall += 2                                                      
    
	If XMLChildEx( oXml:_CFDI_COMPROBANTE, "_CFDI_COMPLEMENTO" ) == Nil
		oPrint:Say(	nFotY+(nTamSalto*(nFall+nCadLn)), nFotX+20, STR0047, oAr07R, , CLR_BLACK, , 2) //"-- �ste documento no ha sido Timbrado. --"
		cCertQR := ""
	Else	
		For nIteration := 1 To Len(cSelloSAT) Step 200
			oPrint:Say(	nFotY + (nTamSalto*(nFall+nCadLn)), nFotX+20, SubStr(cSelloSAT, nIteration, 200), oAr07R,,CLR_BLACK,,2)
			nCadLn++
			If (nCadLn == 4)
				Exit
			EndIf
		Next nIteration
		cCertQR := "?re=" + cRFCEmisor +"&"+"rr="+  cClientRFC + "&" + "tt=" + cTotalDocu +"&id=" + AllTrim(cUUID)
	EndIf

	//CODIGO QR en Base a Certificado SAT
	oPrint:QRCode(2400, 100, cCertQR, 100)
	
 	oPrint:SayAlign(nFotY + (nTamSalto*20) + 400, 1750, STR0048				, oAr10R, 550, 30, , 2, 0) //"�ste documento es una representaci�n "
 	oPrint:SayAlign(nFotY + (nTamSalto*20) + 440, 1750, STR0049				, oAr10R, 550, 30, , 2, 0) //"impresa de un CFDI"
 	
 	oPrint:SayAlign(nFotY + (nTamSalto*22) + 440, 1750, STR0050 + cPagina	, oAr10R, 550, 30, , 2, 0) //"Pagina: "
 	
	nFall += 1
	oPrint:EndPage()

Return

/*
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
���������������������������������������������������������������������������������Ŀ��
���Fun��o    �MTR475NewP� Autor � Marco Augusto Gonzalez Rivera � Data �28/08/2018���
���������������������������������������������������������������������������������Ĵ��
���Descri��o � Funcion utilizada para realizar saldo de pagina, cuando hay mas de ���
���Descri��o � 15 items por Documento Fiscal.                                     ���
���������������������������������������������������������������������������������Ĵ��
���Uso       � Facturacion - Mexico.                                              ���
����������������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
/*/
Static Function MTR475NewP(oXml)

	Default oXml := Nil
	
	oPrint:EndPage()
	oPrint:StartPage()
	MATR475Enc(oXml)

Return

/*
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
���������������������������������������������������������������������������������Ŀ��
���Fun��o    �MTR475CanL� Autor � Marco Augusto Gonzalez Rivera � Data �28/08/2018���
���������������������������������������������������������������������������������Ĵ��
���Descri��o � Funcion utilizada para tranformar cantidad en letra.               ���
���������������������������������������������������������������������������������Ĵ��
���Uso       � Facturacion - Mexico.                                              ���
����������������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
/*/
Static Function MTR475CanL(cCantidad, cFolio, cSerie)
	
	Local cRet		:= ""
	Local cFilSF2	:= xFilial("SF2")
	Local nMoneda	:= 1
	
	Default cCantidad	:= ""
	Default cFolio	:= ""
	Default cSerie	:= ""
	
	SF2->(DbSetOrder(1)) //F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENTE+F2_LOJA+F2_FORMUL+F2_TIPO
	If SF2->(DbSeek(cFilSF2 + cFolio + cSerie))
		nMoneda := SF2->F2_MOEDA
	EndIf
	
	cRet := Extenso(Val(cCantidad), .F., nMoneda, '', IIf(nMoneda == 1, "2", "3"), .T., .T. )
	
Return cRet

/*
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
���������������������������������������������������������������������������������Ŀ��
���Fun��o    �MTR475CarE� Autor � Marco Augusto Gonzalez Rivera � Data �28/08/2018���
���������������������������������������������������������������������������������Ĵ��
���Descri��o � Tratamiento de caracteres especiales en Cadena Original.           ���
���������������������������������������������������������������������������������Ĵ��
���Uso       � Facturacion - Mexico.                                              ���
����������������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
/*/
Static Function MTR475CarE(cCad)

	Local cRet			:= ""
	Local aTags			:= {"&amp;"	, "&quot;"	, "&lt;"	, "&gt;"	, "&#36;"	, "&#38;"	}
	Local aText			:= {"&"		, '"'		, "<"		, ">"		, "'"		, "&"		}
	Local nIteration	:= 0
	
	Default cCad	:= ""
	
	cRet := cCad
	
	For nIteration := 1 To Len(aTags)
		cRet := StrTran(cRet, aTags[nIteration], aText[nIteration])
	Next nIteration
			
	cRet := AllTrim(cRet)

Return cRet

/*
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
���������������������������������������������������������������������������������Ŀ��
���Fun��o    �MTR475CadO� Autor � Marco Augusto Gonzalez Rivera � Data �28/08/2018���
���������������������������������������������������������������������������������Ĵ��
���Descri��o �Imprime Cadena Original en caso de existir Timbre, con formato:     ���
���          �||Version|UUID|Fecha/Hora de Certificaci�n|Sello digital del CFDI|  ���
���          �Numero de certificado||                                             ���
���������������������������������������������������������������������������������Ĵ��
���Uso       � Facturacion - Mexico.                                              ���
����������������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
/*/
Static Function MTR475CadO(cCadOrig, oXML)

	Local cFechaTim		:= ""
	Local cUUIDTim		:= ""
	Local cCerTim		:= ""
	Local cSelloCFDI	:= ""
	
	Default cCadOrig	:= ""
	Default oXml		:= Nil

	If XMLChildEx( oXML:_CFDI_COMPROBANTE, "_CFDI_COMPLEMENTO" ) == Nil
		cCadOrig := STR0051 //"-- Documento inv�lido (Sin Timbre Fiscal Digital). --"
	Else
		cFechaTim 	:= oXML:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO:_TFD_TIMBREFISCALDIGITAL:_FECHATIMBRADO:TEXT
		cUUIDTim 	:= oXML:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO:_TFD_TIMBREFISCALDIGITAL:_UUID:TEXT
		cCerTim 	:= oXML:_CFDI_COMPROBANTE:_CFDI_COMPLEMENTO:_TFD_TIMBREFISCALDIGITAL:_NOCERTIFICADOSAT:TEXT
		cSelloCFDI	:= oXML:_CFDI_COMPROBANTE:_SELLO:TEXT

		cCadOrig := "||"
		cCadOrig += "1.0|" 				//	Version
		cCadOrig += cUUIDTim + "|"		// 	UUID
		cCadOrig += cFechaTim + "|"     // 	Fecha y hora de certificaci�n
		cCadOrig += cSelloCFDI + "|"	// 	Sello digital del CFDI
		cCadOrig += cCerTim + "||"		//	N�mero de certificado		
	EndIf
	
Return Nil


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    �UTf8ToChr  � Autor � Alf. Medrano         � Data � 09/07/19 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Controle de caracteres especiais                           ���
�������������������������������������������������������������������������Ĵ��
���Uso       �CFD - Mexico                                                ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function UTf8ToChr(cTexto)
	Local cRet := ""                    
	Local nChar := 0
	Local aCarEsp := {}
	Default cTexto := ""

	If !Empty(cTexto)
		cRet := cTexto
		Aadd(aCarEsp,{"&","&#38;"})
		Aadd(aCarEsp,{'"',"&#34;"})
		Aadd(aCarEsp,{"<","&#60;"})
		Aadd(aCarEsp,{">","&#62;"})
		Aadd(aCarEsp,{"'","&#39;"})
			
		For nChar := 1 To Len(aCarEsp)
			cRet := StrTran(cRet,aCarEsp[nChar,2],aCarEsp[nChar,1])
		Next

	EndIf

Return(cRet)