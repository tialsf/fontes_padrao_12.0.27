#INCLUDE "TOTVS.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "RESTFUL.CH"
#include "FINA884.CH"

WSRESTFUL wsfin884 DESCRIPTION "plaid Rest"

WSDATA session_id      AS string

WSMETHOD POST  DESCRIPTION "Integración Plaid" WSSYNTAX "/wsfin884"

END WSRESTFUL

WSMETHOD POST  WSSERVICE wsfin884
	Local lPost := .T.
	Local cBody 
	Local oObj  
	Local cEmp	:= "01"
	Local cFil	:= "01" 
	
	::SetContentType("application/json")

	cBody := ::GetContent()

	FWJsonDeserialize(cBody,@oObj)
	If empty(cBody)
		cBody := STR0060// "Sin Valor"
	Else
		if(Select("SX6")==0)
			RPCSetType(3)
			PREPARE ENVIRONMENT EMPRESA cEmp FILIAL cFil
		Endif
 		dbSelectArea("RVR")
		RecLock("RVR",.T.)
			REPLACE  RVR->RVR_FILIAL	WITH xFilial("RVR") 
			REPLACE  RVR->RVR_TOKEN 	WITH cvaltochar(oObj:token)
			REPLACE  RVR->RVR_USERID 	WITH "INTEGRATIONPLAID"
		RVR->(MsUnlock())
		RVR->(DbCloseArea())
		::SetResponse('{"Recibido":"' + oObj:token + '", "client":"'+ "oObj:session_id" + '"}')
	EndIf

Return lPost