#INCLUDE "PROTHEUS.CH"
#INCLUDE "APWEBSRV.CH"

/* ===============================================================================
WSDL Location    http://int.dfacture.com.co/ws/obj/v1.1/Service.svc?wsdl
Generado en        08/29/18 09:38:54
Observaciones      Codigo Fuente generado por ADVPL WSDL Client 1.120703
                 Modificaciones en este archivo pueden causar funcionamiento incorrecto
                 y se perderan en caso de que se genere nuevamente el codigo fuente.
=============================================================================== */

User Function _GLUPMXW ; Return  // "dummy" function - Internal Use 

/* -------------------------------------------------------------------------------
WSDL Service WSNFEColProd
------------------------------------------------------------------------------- */

WSCLIENT WSNFEColProd

	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD RESET
	WSMETHOD CLONE
	WSMETHOD Enviar
	WSMETHOD EstadoDocumento
	WSMETHOD EnvioCorreo
	WSMETHOD DescargaPDF
	WSMETHOD DescargaXML
	WSMETHOD FoliosRestantes
	WSMETHOD CargarCertificado
	WSMETHOD ValidarXml

	WSDATA   _URL                      AS String
	WSDATA   _HEADOUT                  AS Array of String
	WSDATA   _COOKIES                  AS Array of String
	WSDATA   ctokenEmpresa             AS string
	WSDATA   ctokenPassword            AS string
	WSDATA   oWSfactura                AS Service_FacturaGeneral
	WSDATA   cadjuntos                 AS string
	WSDATA   oWSEnviarResult           AS Service_DocumentResponse
	WSDATA   cdocumento                AS string
	WSDATA   oWSEstadoDocumentoResult  AS Service_DocumentStatusResponse
	WSDATA   ccorreo                   AS string
	WSDATA   oWSEnvioCorreoResult      AS Service_SendEmailResponse
	WSDATA   oWSDescargaPDFResult      AS Service_DownloadPDFResponse
	WSDATA   oWSDescargaXMLResult      AS Service_DownloadXMLResponse
	WSDATA   oWSFoliosRestantesResult  AS Service_FoliosRemainingResponse
	WSDATA   ccertificado              AS string
	WSDATA   cpassword                 AS string
	WSDATA   oWSCargarCertificadoResult AS Service_LoadCertificateResponse
	WSDATA   oWSValidarXmlResult       AS Service_ValidateXML

ENDWSCLIENT

WSMETHOD NEW WSCLIENT WSNFEColProd
::Init()
If !FindFunction("XMLCHILDEX")
	UserException("O C�digo-Fonte Client atual requer os execut�veis do Protheus Build [7.00.131227A-20180123] ou superior. Atualize o Protheus ou gere o C�digo-Fonte novamente utilizando o Build atual.")
EndIf
Return Self

WSMETHOD INIT WSCLIENT WSNFEColProd
	::oWSfactura         := Service_FACTURAGENERAL():New()
	::oWSEnviarResult    := Service_DOCUMENTRESPONSE():New()
	::oWSEstadoDocumentoResult := Service_DOCUMENTSTATUSRESPONSE():New()
	::oWSEnvioCorreoResult := Service_SENDEMAILRESPONSE():New()
	::oWSDescargaPDFResult := Service_DOWNLOADPDFRESPONSE():New()
	::oWSDescargaXMLResult := Service_DOWNLOADXMLRESPONSE():New()
	::oWSFoliosRestantesResult := Service_FOLIOSREMAININGRESPONSE():New()
	::oWSCargarCertificadoResult := Service_LOADCERTIFICATERESPONSE():New()
	::oWSValidarXmlResult := Service_VALIDATEXML():New()
Return

WSMETHOD RESET WSCLIENT WSNFEColProd
	::ctokenEmpresa      := NIL 
	::ctokenPassword     := NIL 
	::oWSfactura         := NIL 
	::cadjuntos          := NIL 
	::oWSEnviarResult    := NIL 
	::cdocumento         := NIL 
	::oWSEstadoDocumentoResult := NIL 
	::ccorreo            := NIL 
	::oWSEnvioCorreoResult := NIL 
	::oWSDescargaPDFResult := NIL 
	::oWSDescargaXMLResult := NIL 
	::oWSFoliosRestantesResult := NIL 
	::ccertificado       := NIL 
	::cpassword          := NIL 
	::oWSCargarCertificadoResult := NIL 
	::oWSValidarXmlResult := NIL 
	::Init()
Return

WSMETHOD CLONE WSCLIENT WSNFEColProd
Local oClone := WSNFEColProd():New()
	oClone:_URL          := ::_URL 
	oClone:ctokenEmpresa := ::ctokenEmpresa
	oClone:ctokenPassword := ::ctokenPassword
	oClone:oWSfactura    :=  IIF(::oWSfactura = NIL , NIL ,::oWSfactura:Clone() )
	oClone:cadjuntos     := ::cadjuntos
	oClone:oWSEnviarResult :=  IIF(::oWSEnviarResult = NIL , NIL ,::oWSEnviarResult:Clone() )
	oClone:cdocumento    := ::cdocumento
	oClone:oWSEstadoDocumentoResult :=  IIF(::oWSEstadoDocumentoResult = NIL , NIL ,::oWSEstadoDocumentoResult:Clone() )
	oClone:ccorreo       := ::ccorreo
	oClone:oWSEnvioCorreoResult :=  IIF(::oWSEnvioCorreoResult = NIL , NIL ,::oWSEnvioCorreoResult:Clone() )
	oClone:oWSDescargaPDFResult :=  IIF(::oWSDescargaPDFResult = NIL , NIL ,::oWSDescargaPDFResult:Clone() )
	oClone:oWSDescargaXMLResult :=  IIF(::oWSDescargaXMLResult = NIL , NIL ,::oWSDescargaXMLResult:Clone() )
	oClone:oWSFoliosRestantesResult :=  IIF(::oWSFoliosRestantesResult = NIL , NIL ,::oWSFoliosRestantesResult:Clone() )
	oClone:ccertificado  := ::ccertificado
	oClone:cpassword     := ::cpassword
	oClone:oWSCargarCertificadoResult :=  IIF(::oWSCargarCertificadoResult = NIL , NIL ,::oWSCargarCertificadoResult:Clone() )
	oClone:oWSValidarXmlResult :=  IIF(::oWSValidarXmlResult = NIL , NIL ,::oWSValidarXmlResult:Clone() )
Return oClone

// WSDL Method Enviar of Service WSNFEColProd

WSMETHOD Enviar WSSEND ctokenEmpresa,ctokenPassword,oWSfactura,cadjuntos WSRECEIVE oWSEnviarResult WSCLIENT WSNFEColProd
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<Enviar xmlns="http://tempuri.org/">'
cSoap += WSSoapValue("tokenEmpresa", ::ctokenEmpresa, ctokenEmpresa , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("tokenPassword", ::ctokenPassword, ctokenPassword , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("factura", ::oWSfactura, oWSfactura , "FacturaGeneral", .F. , .F., 0 , "http://tempuri.org/", .F.,.F.) 
cSoap += WSSoapValue("adjuntos", ::cadjuntos, cadjuntos , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</Enviar>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://tempuri.org/IService/Enviar",; 
	"DOCUMENT","http://tempuri.org/",,,; 
	"http://int.dfacture.com.co/ws/obj/v1.1/Service.svc")

::Init()
::oWSEnviarResult:SoapRecv( WSAdvValue( oXmlRet,"_ENVIARRESPONSE:_ENVIARRESULT","DocumentResponse",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method EstadoDocumento of Service WSNFEColProd

WSMETHOD EstadoDocumento WSSEND ctokenEmpresa,ctokenPassword,cdocumento WSRECEIVE oWSEstadoDocumentoResult WSCLIENT WSNFEColProd
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<EstadoDocumento xmlns="http://tempuri.org/">'
cSoap += WSSoapValue("tokenEmpresa", ::ctokenEmpresa, ctokenEmpresa , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("tokenPassword", ::ctokenPassword, ctokenPassword , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("documento", ::cdocumento, cdocumento , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</EstadoDocumento>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://tempuri.org/IService/EstadoDocumento",; 
	"DOCUMENT","http://tempuri.org/",,,; 
	"http://int.dfacture.com.co/ws/obj/v1.1/Service.svc")

::Init()
::oWSEstadoDocumentoResult:SoapRecv( WSAdvValue( oXmlRet,"_ESTADODOCUMENTORESPONSE:_ESTADODOCUMENTORESULT","DocumentStatusResponse",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method EnvioCorreo of Service WSNFEColProd

WSMETHOD EnvioCorreo WSSEND ctokenEmpresa,ctokenPassword,cdocumento,ccorreo WSRECEIVE oWSEnvioCorreoResult WSCLIENT WSNFEColProd
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<EnvioCorreo xmlns="http://tempuri.org/">'
cSoap += WSSoapValue("tokenEmpresa", ::ctokenEmpresa, ctokenEmpresa , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("tokenPassword", ::ctokenPassword, ctokenPassword , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("documento", ::cdocumento, cdocumento , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("correo", ::ccorreo, ccorreo , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</EnvioCorreo>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://tempuri.org/IService/EnvioCorreo",; 
	"DOCUMENT","http://tempuri.org/",,,; 
	"http://int.dfacture.com.co/ws/obj/v1.1/Service.svc")

::Init()
::oWSEnvioCorreoResult:SoapRecv( WSAdvValue( oXmlRet,"_ENVIOCORREORESPONSE:_ENVIOCORREORESULT","SendEmailResponse",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method DescargaPDF of Service WSNFEColProd

WSMETHOD DescargaPDF WSSEND ctokenEmpresa,ctokenPassword,cdocumento WSRECEIVE oWSDescargaPDFResult WSCLIENT WSNFEColProd
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<DescargaPDF xmlns="http://tempuri.org/">'
cSoap += WSSoapValue("tokenEmpresa", ::ctokenEmpresa, ctokenEmpresa , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("tokenPassword", ::ctokenPassword, ctokenPassword , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("documento", ::cdocumento, cdocumento , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</DescargaPDF>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://tempuri.org/IService/DescargaPDF",; 
	"DOCUMENT","http://tempuri.org/",,,; 
	"http://int.dfacture.com.co/ws/obj/v1.1/Service.svc")

::Init()
::oWSDescargaPDFResult:SoapRecv( WSAdvValue( oXmlRet,"_DESCARGAPDFRESPONSE:_DESCARGAPDFRESULT","DownloadPDFResponse",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method DescargaXML of Service WSNFEColProd

WSMETHOD DescargaXML WSSEND ctokenEmpresa,ctokenPassword,cdocumento WSRECEIVE oWSDescargaXMLResult WSCLIENT WSNFEColProd
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<DescargaXML xmlns="http://tempuri.org/">'
cSoap += WSSoapValue("tokenEmpresa", ::ctokenEmpresa, ctokenEmpresa , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("tokenPassword", ::ctokenPassword, ctokenPassword , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("documento", ::cdocumento, cdocumento , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</DescargaXML>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://tempuri.org/IService/DescargaXML",; 
	"DOCUMENT","http://tempuri.org/",,,; 
	"http://int.dfacture.com.co/ws/obj/v1.1/Service.svc")

::Init()
::oWSDescargaXMLResult:SoapRecv( WSAdvValue( oXmlRet,"_DESCARGAXMLRESPONSE:_DESCARGAXMLRESULT","DownloadXMLResponse",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method FoliosRestantes of Service WSNFEColProd

WSMETHOD FoliosRestantes WSSEND ctokenEmpresa,ctokenPassword WSRECEIVE oWSFoliosRestantesResult WSCLIENT WSNFEColProd
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<FoliosRestantes xmlns="http://tempuri.org/">'
cSoap += WSSoapValue("tokenEmpresa", ::ctokenEmpresa, ctokenEmpresa , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("tokenPassword", ::ctokenPassword, ctokenPassword , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</FoliosRestantes>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://tempuri.org/IService/FoliosRestantes",; 
	"DOCUMENT","http://tempuri.org/",,,; 
	"http://int.dfacture.com.co/ws/obj/v1.1/Service.svc")

::Init()
::oWSFoliosRestantesResult:SoapRecv( WSAdvValue( oXmlRet,"_FOLIOSRESTANTESRESPONSE:_FOLIOSRESTANTESRESULT","FoliosRemainingResponse",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method CargarCertificado of Service WSNFEColProd

WSMETHOD CargarCertificado WSSEND ctokenEmpresa,ctokenPassword,ccertificado,cpassword WSRECEIVE oWSCargarCertificadoResult WSCLIENT WSNFEColProd
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<CargarCertificado xmlns="http://tempuri.org/">'
cSoap += WSSoapValue("tokenEmpresa", ::ctokenEmpresa, ctokenEmpresa , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("tokenPassword", ::ctokenPassword, ctokenPassword , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("certificado", ::ccertificado, ccertificado , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("password", ::cpassword, cpassword , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</CargarCertificado>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://tempuri.org/IService/CargarCertificado",; 
	"DOCUMENT","http://tempuri.org/",,,; 
	"http://int.dfacture.com.co/ws/obj/v1.1/Service.svc")

::Init()
::oWSCargarCertificadoResult:SoapRecv( WSAdvValue( oXmlRet,"_CARGARCERTIFICADORESPONSE:_CARGARCERTIFICADORESULT","LoadCertificateResponse",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method ValidarXml of Service WSNFEColProd

WSMETHOD ValidarXml WSSEND ctokenEmpresa,ctokenPassword,cdocumento WSRECEIVE oWSValidarXmlResult WSCLIENT WSNFEColProd
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<ValidarXml xmlns="http://tempuri.org/">'
cSoap += WSSoapValue("tokenEmpresa", ::ctokenEmpresa, ctokenEmpresa , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("tokenPassword", ::ctokenPassword, ctokenPassword , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("documento", ::cdocumento, cdocumento , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</ValidarXml>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://tempuri.org/IService/ValidarXml",; 
	"DOCUMENT","http://tempuri.org/",,,; 
	"http://int.dfacture.com.co/ws/obj/v1.1/Service.svc")

::Init()
::oWSValidarXmlResult:SoapRecv( WSAdvValue( oXmlRet,"_VALIDARXMLRESPONSE:_VALIDARXMLRESULT","ValidateXML",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.


// WSDL Data Structure FacturaGeneral

WSSTRUCT Service_FacturaGeneral_Prod
	WSDATA   oWScliente                AS Service_Cliente OPTIONAL
	WSDATA   cconsecutivoDocumento     AS string OPTIONAL
	WSDATA   cconsecutivoDocumentoModificado AS string OPTIONAL
	WSDATA   oWSdetalleDeFactura       AS Service_ArrayOfFacturaDetalle OPTIONAL
	WSDATA   cestadoPago               AS string OPTIONAL
	WSDATA   oWSextras                 AS Service_ArrayOfExtras OPTIONAL
	WSDATA   cfechaEmision             AS string OPTIONAL
	WSDATA   cfechaEmisionDocumentoModificado AS string OPTIONAL
	WSDATA   cfechaVencimiento         AS string OPTIONAL
	WSDATA   cicoterms                 AS string OPTIONAL
	WSDATA   cimporteTotal             AS string OPTIONAL
	WSDATA   oWSimpuestosGenerales     AS Service_ArrayOfFacturaImpuestos OPTIONAL
	WSDATA   cinformacionAdicional     AS string OPTIONAL
	WSDATA   cmedioPago                AS string OPTIONAL
	WSDATA   cmoneda                   AS string OPTIONAL
	WSDATA   cmotivoNota               AS string OPTIONAL
	WSDATA   cpropina                  AS string OPTIONAL
	WSDATA   crangoNumeracion          AS string OPTIONAL
	WSDATA   ctipoDocumento            AS string OPTIONAL
	WSDATA   ctotalDescuentos          AS string OPTIONAL
	WSDATA   ctotalSinImpuestos        AS string OPTIONAL
	WSDATA   cuuidDocumentoModificado  AS string OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Service_FacturaGeneral
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Service_FacturaGeneral
Return

WSMETHOD CLONE WSCLIENT Service_FacturaGeneral
	Local oClone := Service_FacturaGeneral():NEW()
	oClone:oWScliente           := IIF(::oWScliente = NIL , NIL , ::oWScliente:Clone() )
	oClone:cconsecutivoDocumento := ::cconsecutivoDocumento
	oClone:cconsecutivoDocumentoModificado := ::cconsecutivoDocumentoModificado
	oClone:oWSdetalleDeFactura  := IIF(::oWSdetalleDeFactura = NIL , NIL , ::oWSdetalleDeFactura:Clone() )
	oClone:cestadoPago          := ::cestadoPago
	oClone:oWSextras            := IIF(::oWSextras = NIL , NIL , ::oWSextras:Clone() )
	oClone:cfechaEmision        := ::cfechaEmision
	oClone:cfechaEmisionDocumentoModificado := ::cfechaEmisionDocumentoModificado
	oClone:cfechaVencimiento    := ::cfechaVencimiento
	oClone:cicoterms            := ::cicoterms
	oClone:cimporteTotal        := ::cimporteTotal
	oClone:oWSimpuestosGenerales := IIF(::oWSimpuestosGenerales = NIL , NIL , ::oWSimpuestosGenerales:Clone() )
	oClone:cinformacionAdicional := ::cinformacionAdicional
	oClone:cmedioPago           := ::cmedioPago
	oClone:cmoneda              := ::cmoneda
	oClone:cmotivoNota          := ::cmotivoNota
	oClone:cpropina             := ::cpropina
	oClone:crangoNumeracion     := ::crangoNumeracion
	oClone:ctipoDocumento       := ::ctipoDocumento
	oClone:ctotalDescuentos     := ::ctotalDescuentos
	oClone:ctotalSinImpuestos   := ::ctotalSinImpuestos
	oClone:cuuidDocumentoModificado := ::cuuidDocumentoModificado
Return oClone

WSMETHOD SOAPSEND WSCLIENT Service_FacturaGeneral
	Local cSoap := ""
	cSoap += WSSoapValue("cliente", ::oWScliente, ::oWScliente , "Cliente", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("consecutivoDocumento", ::cconsecutivoDocumento, ::cconsecutivoDocumento , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("consecutivoDocumentoModificado", ::cconsecutivoDocumentoModificado, ::cconsecutivoDocumentoModificado , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("detalleDeFactura", ::oWSdetalleDeFactura, ::oWSdetalleDeFactura , "ArrayOfFacturaDetalle", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("estadoPago", ::cestadoPago, ::cestadoPago , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("extras", ::oWSextras, ::oWSextras , "ArrayOfExtras", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("fechaEmision", ::cfechaEmision, ::cfechaEmision , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("fechaEmisionDocumentoModificado", ::cfechaEmisionDocumentoModificado, ::cfechaEmisionDocumentoModificado , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("fechaVencimiento", ::cfechaVencimiento, ::cfechaVencimiento , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("icoterms", ::cicoterms, ::cicoterms , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("importeTotal", ::cimporteTotal, ::cimporteTotal , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("impuestosGenerales", ::oWSimpuestosGenerales, ::oWSimpuestosGenerales , "ArrayOfFacturaImpuestos", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("informacionAdicional", ::cinformacionAdicional, ::cinformacionAdicional , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("medioPago", ::cmedioPago, ::cmedioPago , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("moneda", ::cmoneda, ::cmoneda , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("motivoNota", ::cmotivoNota, ::cmotivoNota , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("propina", ::cpropina, ::cpropina , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("rangoNumeracion", ::crangoNumeracion, ::crangoNumeracion , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("tipoDocumento", ::ctipoDocumento, ::ctipoDocumento , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("totalDescuentos", ::ctotalDescuentos, ::ctotalDescuentos , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("totalSinImpuestos", ::ctotalSinImpuestos, ::ctotalSinImpuestos , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("uuidDocumentoModificado", ::cuuidDocumentoModificado, ::cuuidDocumentoModificado , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
Return cSoap

// WSDL Data Structure DocumentResponse

WSSTRUCT Service_DocumentResponse_Prod
	WSDATA   ncodigo                   AS int OPTIONAL
	WSDATA   cconsecutivoDocumento     AS string OPTIONAL
	WSDATA   ccufe                     AS string OPTIONAL
	WSDATA   cfechaRespuesta           AS string OPTIONAL
	WSDATA   cmensaje                  AS string OPTIONAL
	WSDATA   cresultado                AS string OPTIONAL
	WSDATA   cxml                      AS string OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Service_DocumentResponse
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Service_DocumentResponse
Return

WSMETHOD CLONE WSCLIENT Service_DocumentResponse
	Local oClone := Service_DocumentResponse():NEW()
	oClone:ncodigo              := ::ncodigo
	oClone:cconsecutivoDocumento := ::cconsecutivoDocumento
	oClone:ccufe                := ::ccufe
	oClone:cfechaRespuesta      := ::cfechaRespuesta
	oClone:cmensaje             := ::cmensaje
	oClone:cresultado           := ::cresultado
	oClone:cxml                 := ::cxml
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Service_DocumentResponse
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::ncodigo            :=  WSAdvValue( oResponse,"_CODIGO","int",NIL,NIL,NIL,"N",NIL,NIL) 
	::cconsecutivoDocumento :=  WSAdvValue( oResponse,"_CONSECUTIVODOCUMENTO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::ccufe              :=  WSAdvValue( oResponse,"_CUFE","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cfechaRespuesta    :=  WSAdvValue( oResponse,"_FECHARESPUESTA","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cmensaje           :=  WSAdvValue( oResponse,"_MENSAJE","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cresultado         :=  WSAdvValue( oResponse,"_RESULTADO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cxml               :=  WSAdvValue( oResponse,"_XML","string",NIL,NIL,NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure DocumentStatusResponse

WSSTRUCT Service_DocumentStatusResponse_Prod
	WSDATA   cacuseComentario          AS string OPTIONAL
	WSDATA   cacuseEstatus             AS string OPTIONAL
	WSDATA   cacuseResponsable         AS string OPTIONAL
	WSDATA   cacuseRespuesta           AS string OPTIONAL
	WSDATA   ncodigo                   AS int OPTIONAL
	WSDATA   ccufe                     AS string OPTIONAL
	WSDATA   nestatusDocumento         AS int OPTIONAL
	WSDATA   cfechaDocumento           AS string OPTIONAL
	WSDATA   cmensaje                  AS string OPTIONAL
	WSDATA   cmensajeDocumento         AS string OPTIONAL
	WSDATA   cresultado                AS string OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Service_DocumentStatusResponse
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Service_DocumentStatusResponse
Return

WSMETHOD CLONE WSCLIENT Service_DocumentStatusResponse
	Local oClone := Service_DocumentStatusResponse():NEW()
	oClone:cacuseComentario     := ::cacuseComentario
	oClone:cacuseEstatus        := ::cacuseEstatus
	oClone:cacuseResponsable    := ::cacuseResponsable
	oClone:cacuseRespuesta      := ::cacuseRespuesta
	oClone:ncodigo              := ::ncodigo
	oClone:ccufe                := ::ccufe
	oClone:nestatusDocumento    := ::nestatusDocumento
	oClone:cfechaDocumento      := ::cfechaDocumento
	oClone:cmensaje             := ::cmensaje
	oClone:cmensajeDocumento    := ::cmensajeDocumento
	oClone:cresultado           := ::cresultado
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Service_DocumentStatusResponse
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cacuseComentario   :=  WSAdvValue( oResponse,"_ACUSECOMENTARIO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cacuseEstatus      :=  WSAdvValue( oResponse,"_ACUSEESTATUS","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cacuseResponsable  :=  WSAdvValue( oResponse,"_ACUSERESPONSABLE","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cacuseRespuesta    :=  WSAdvValue( oResponse,"_ACUSERESPUESTA","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::ncodigo            :=  WSAdvValue( oResponse,"_CODIGO","int",NIL,NIL,NIL,"N",NIL,NIL) 
	::ccufe              :=  WSAdvValue( oResponse,"_CUFE","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::nestatusDocumento  :=  WSAdvValue( oResponse,"_ESTATUSDOCUMENTO","int",NIL,NIL,NIL,"N",NIL,NIL) 
	::cfechaDocumento    :=  WSAdvValue( oResponse,"_FECHADOCUMENTO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cmensaje           :=  WSAdvValue( oResponse,"_MENSAJE","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cmensajeDocumento  :=  WSAdvValue( oResponse,"_MENSAJEDOCUMENTO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cresultado         :=  WSAdvValue( oResponse,"_RESULTADO","string",NIL,NIL,NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure SendEmailResponse

WSSTRUCT Service_SendEmailResponse_Prod
	WSDATA   ncodigo                   AS int OPTIONAL
	WSDATA   cmensaje                  AS string OPTIONAL
	WSDATA   cresultado                AS string OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Service_SendEmailResponse
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Service_SendEmailResponse
Return

WSMETHOD CLONE WSCLIENT Service_SendEmailResponse
	Local oClone := Service_SendEmailResponse():NEW()
	oClone:ncodigo              := ::ncodigo
	oClone:cmensaje             := ::cmensaje
	oClone:cresultado           := ::cresultado
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Service_SendEmailResponse
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::ncodigo            :=  WSAdvValue( oResponse,"_CODIGO","int",NIL,NIL,NIL,"N",NIL,NIL) 
	::cmensaje           :=  WSAdvValue( oResponse,"_MENSAJE","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cresultado         :=  WSAdvValue( oResponse,"_RESULTADO","string",NIL,NIL,NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure DownloadPDFResponse

WSSTRUCT Service_DownloadPDFResponse_Prod
	WSDATA   ncodigo                   AS int OPTIONAL
	WSDATA   ccufe                     AS string OPTIONAL
	WSDATA   cdocumento                AS string OPTIONAL
	WSDATA   cmensaje                  AS string OPTIONAL
	WSDATA   cresultado                AS string OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Service_DownloadPDFResponse
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Service_DownloadPDFResponse
Return

WSMETHOD CLONE WSCLIENT Service_DownloadPDFResponse
	Local oClone := Service_DownloadPDFResponse():NEW()
	oClone:ncodigo              := ::ncodigo
	oClone:ccufe                := ::ccufe
	oClone:cdocumento           := ::cdocumento
	oClone:cmensaje             := ::cmensaje
	oClone:cresultado           := ::cresultado
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Service_DownloadPDFResponse
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::ncodigo            :=  WSAdvValue( oResponse,"_CODIGO","int",NIL,NIL,NIL,"N",NIL,NIL) 
	::ccufe              :=  WSAdvValue( oResponse,"_CUFE","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cdocumento         :=  WSAdvValue( oResponse,"_DOCUMENTO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cmensaje           :=  WSAdvValue( oResponse,"_MENSAJE","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cresultado         :=  WSAdvValue( oResponse,"_RESULTADO","string",NIL,NIL,NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure DownloadXMLResponse

WSSTRUCT Service_DownloadXMLResponse_Prod
	WSDATA   ncodigo                   AS int OPTIONAL
	WSDATA   ccufe                     AS string OPTIONAL
	WSDATA   cdocumento                AS string OPTIONAL
	WSDATA   cmensaje                  AS string OPTIONAL
	WSDATA   cresultado                AS string OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Service_DownloadXMLResponse
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Service_DownloadXMLResponse
Return

WSMETHOD CLONE WSCLIENT Service_DownloadXMLResponse
	Local oClone := Service_DownloadXMLResponse():NEW()
	oClone:ncodigo              := ::ncodigo
	oClone:ccufe                := ::ccufe
	oClone:cdocumento           := ::cdocumento
	oClone:cmensaje             := ::cmensaje
	oClone:cresultado           := ::cresultado
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Service_DownloadXMLResponse
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::ncodigo            :=  WSAdvValue( oResponse,"_CODIGO","int",NIL,NIL,NIL,"N",NIL,NIL) 
	::ccufe              :=  WSAdvValue( oResponse,"_CUFE","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cdocumento         :=  WSAdvValue( oResponse,"_DOCUMENTO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cmensaje           :=  WSAdvValue( oResponse,"_MENSAJE","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cresultado         :=  WSAdvValue( oResponse,"_RESULTADO","string",NIL,NIL,NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure FoliosRemainingResponse

WSSTRUCT Service_FoliosRemainingResponse_Prod
	WSDATA   ncodigo                   AS int OPTIONAL
	WSDATA   nfoliosRestantes          AS int OPTIONAL
	WSDATA   cmensaje                  AS string OPTIONAL
	WSDATA   cresultado                AS string OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Service_FoliosRemainingResponse
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Service_FoliosRemainingResponse
Return

WSMETHOD CLONE WSCLIENT Service_FoliosRemainingResponse
	Local oClone := Service_FoliosRemainingResponse():NEW()
	oClone:ncodigo              := ::ncodigo
	oClone:nfoliosRestantes     := ::nfoliosRestantes
	oClone:cmensaje             := ::cmensaje
	oClone:cresultado           := ::cresultado
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Service_FoliosRemainingResponse
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::ncodigo            :=  WSAdvValue( oResponse,"_CODIGO","int",NIL,NIL,NIL,"N",NIL,NIL) 
	::nfoliosRestantes   :=  WSAdvValue( oResponse,"_FOLIOSRESTANTES","int",NIL,NIL,NIL,"N",NIL,NIL) 
	::cmensaje           :=  WSAdvValue( oResponse,"_MENSAJE","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cresultado         :=  WSAdvValue( oResponse,"_RESULTADO","string",NIL,NIL,NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure LoadCertificateResponse

WSSTRUCT Service_LoadCertificateResponse_Prod
	WSDATA   ncodigo                   AS int OPTIONAL
	WSDATA   cmensaje                  AS string OPTIONAL
	WSDATA   cresultado                AS string OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Service_LoadCertificateResponse
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Service_LoadCertificateResponse
Return

WSMETHOD CLONE WSCLIENT Service_LoadCertificateResponse
	Local oClone := Service_LoadCertificateResponse():NEW()
	oClone:ncodigo              := ::ncodigo
	oClone:cmensaje             := ::cmensaje
	oClone:cresultado           := ::cresultado
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Service_LoadCertificateResponse
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::ncodigo            :=  WSAdvValue( oResponse,"_CODIGO","int",NIL,NIL,NIL,"N",NIL,NIL) 
	::cmensaje           :=  WSAdvValue( oResponse,"_MENSAJE","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cresultado         :=  WSAdvValue( oResponse,"_RESULTADO","string",NIL,NIL,NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure ValidateXML

WSSTRUCT Service_ValidateXML_Prod
	WSDATA   ncodigo                   AS int OPTIONAL
	WSDATA   cmensaje                  AS string OPTIONAL
	WSDATA   cresultado                AS string OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Service_ValidateXML
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Service_ValidateXML
Return

WSMETHOD CLONE WSCLIENT Service_ValidateXML
	Local oClone := Service_ValidateXML():NEW()
	oClone:ncodigo              := ::ncodigo
	oClone:cmensaje             := ::cmensaje
	oClone:cresultado           := ::cresultado
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Service_ValidateXML
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::ncodigo            :=  WSAdvValue( oResponse,"_CODIGO","int",NIL,NIL,NIL,"N",NIL,NIL) 
	::cmensaje           :=  WSAdvValue( oResponse,"_MENSAJE","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cresultado         :=  WSAdvValue( oResponse,"_RESULTADO","string",NIL,NIL,NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure Cliente

WSSTRUCT Service_Cliente_Prod
	WSDATA   capellido                 AS string OPTIONAL
	WSDATA   cciudad                   AS string OPTIONAL
	WSDATA   cdepartamento             AS string OPTIONAL
	WSDATA   cdireccion                AS string OPTIONAL
	WSDATA   cemail                    AS string OPTIONAL
	WSDATA   cnombreRazonSocial        AS string OPTIONAL
	WSDATA   cnotificar                AS string OPTIONAL
	WSDATA   cnumeroDocumento          AS string OPTIONAL
	WSDATA   cpais                     AS string OPTIONAL
	WSDATA   creferencia2              AS string OPTIONAL
	WSDATA   cregimen                  AS string OPTIONAL
	WSDATA   csegundoNombre            AS string OPTIONAL
	WSDATA   csubDivision              AS string OPTIONAL
	WSDATA   ctelefono                 AS string OPTIONAL
	WSDATA   ctipoIdentificacion       AS string OPTIONAL
	WSDATA   ctipoPersona              AS string OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Service_Cliente
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Service_Cliente
Return

WSMETHOD CLONE WSCLIENT Service_Cliente
	Local oClone := Service_Cliente():NEW()
	oClone:capellido            := ::capellido
	oClone:cciudad              := ::cciudad
	oClone:cdepartamento        := ::cdepartamento
	oClone:cdireccion           := ::cdireccion
	oClone:cemail               := ::cemail
	oClone:cnombreRazonSocial   := ::cnombreRazonSocial
	oClone:cnotificar           := ::cnotificar
	oClone:cnumeroDocumento     := ::cnumeroDocumento
	oClone:cpais                := ::cpais
	oClone:creferencia2         := ::creferencia2
	oClone:cregimen             := ::cregimen
	oClone:csegundoNombre       := ::csegundoNombre
	oClone:csubDivision         := ::csubDivision
	oClone:ctelefono            := ::ctelefono
	oClone:ctipoIdentificacion  := ::ctipoIdentificacion
	oClone:ctipoPersona         := ::ctipoPersona
Return oClone

WSMETHOD SOAPSEND WSCLIENT Service_Cliente
	Local cSoap := ""
	cSoap += WSSoapValue("apellido", ::capellido, ::capellido , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("ciudad", ::cciudad, ::cciudad , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("departamento", ::cdepartamento, ::cdepartamento , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("direccion", ::cdireccion, ::cdireccion , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("email", ::cemail, ::cemail , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("nombreRazonSocial", ::cnombreRazonSocial, ::cnombreRazonSocial , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("notificar", ::cnotificar, ::cnotificar , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("numeroDocumento", ::cnumeroDocumento, ::cnumeroDocumento , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("pais", ::cpais, ::cpais , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("referencia2", ::creferencia2, ::creferencia2 , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("regimen", ::cregimen, ::cregimen , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("segundoNombre", ::csegundoNombre, ::csegundoNombre , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("subDivision", ::csubDivision, ::csubDivision , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("telefono", ::ctelefono, ::ctelefono , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("tipoIdentificacion", ::ctipoIdentificacion, ::ctipoIdentificacion , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("tipoPersona", ::ctipoPersona, ::ctipoPersona , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
Return cSoap

// WSDL Data Structure ArrayOfFacturaDetalle

WSSTRUCT Service_ArrayOfFacturaDetalle_Prod
	WSDATA   oWSFacturaDetalle         AS Service_FacturaDetalle OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Service_ArrayOfFacturaDetalle
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Service_ArrayOfFacturaDetalle
	::oWSFacturaDetalle    := {} // Array Of  Service_FACTURADETALLE():New()
Return

WSMETHOD CLONE WSCLIENT Service_ArrayOfFacturaDetalle
	Local oClone := Service_ArrayOfFacturaDetalle():NEW()
	oClone:oWSFacturaDetalle := NIL
	If ::oWSFacturaDetalle <> NIL 
		oClone:oWSFacturaDetalle := {}
		aEval( ::oWSFacturaDetalle , { |x| aadd( oClone:oWSFacturaDetalle , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPSEND WSCLIENT Service_ArrayOfFacturaDetalle
	Local cSoap := ""
	aEval( ::oWSFacturaDetalle , {|x| cSoap := cSoap  +  WSSoapValue("FacturaDetalle", x , x , "FacturaDetalle", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.)  } ) 
Return cSoap

// WSDL Data Structure ArrayOfExtras

WSSTRUCT Service_ArrayOfExtras_Prod
	WSDATA   oWSExtras                 AS Service_Extras OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Service_ArrayOfExtras
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Service_ArrayOfExtras
	::oWSExtras            := {} // Array Of  Service_EXTRAS():New()
Return

WSMETHOD CLONE WSCLIENT Service_ArrayOfExtras
	Local oClone := Service_ArrayOfExtras():NEW()
	oClone:oWSExtras := NIL
	If ::oWSExtras <> NIL 
		oClone:oWSExtras := {}
		aEval( ::oWSExtras , { |x| aadd( oClone:oWSExtras , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPSEND WSCLIENT Service_ArrayOfExtras
	Local cSoap := ""
	aEval( ::oWSExtras , {|x| cSoap := cSoap  +  WSSoapValue("Extras", x , x , "Extras", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.)  } ) 
Return cSoap

// WSDL Data Structure ArrayOfFacturaImpuestos

WSSTRUCT Service_ArrayOfFacturaImpuestos_Prod
	WSDATA   oWSFacturaImpuestos       AS Service_FacturaImpuestos OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Service_ArrayOfFacturaImpuestos
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Service_ArrayOfFacturaImpuestos
	::oWSFacturaImpuestos  := {} // Array Of  Service_FACTURAIMPUESTOS():New()
Return

WSMETHOD CLONE WSCLIENT Service_ArrayOfFacturaImpuestos
	Local oClone := Service_ArrayOfFacturaImpuestos():NEW()
	oClone:oWSFacturaImpuestos := NIL
	If ::oWSFacturaImpuestos <> NIL 
		oClone:oWSFacturaImpuestos := {}
		aEval( ::oWSFacturaImpuestos , { |x| aadd( oClone:oWSFacturaImpuestos , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPSEND WSCLIENT Service_ArrayOfFacturaImpuestos
	Local cSoap := ""
	aEval( ::oWSFacturaImpuestos , {|x| cSoap := cSoap  +  WSSoapValue("FacturaImpuestos", x , x , "FacturaImpuestos", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.)  } ) 
Return cSoap

// WSDL Data Structure FacturaDetalle

WSSTRUCT Service_FacturaDetalle_Prod
	WSDATA   ccantidadUnidades         AS string OPTIONAL
	WSDATA   ccodigoProducto           AS string OPTIONAL
	WSDATA   cdescripcion              AS string OPTIONAL
	WSDATA   cdescuento                AS string OPTIONAL
	WSDATA   cdetalleAdicionalNombre   AS string OPTIONAL
	WSDATA   cdetalleAdicionalValor    AS string OPTIONAL
	WSDATA   oWSimpuestosDetalles      AS Service_ArrayOfFacturaImpuestos OPTIONAL
	WSDATA   cprecioTotal              AS string OPTIONAL
	WSDATA   cprecioTotalSinImpuestos  AS string OPTIONAL
	WSDATA   cprecioVentaUnitario      AS string OPTIONAL
	WSDATA   cseriales                 AS string OPTIONAL
	WSDATA   cunidadMedida             AS string OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Service_FacturaDetalle
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Service_FacturaDetalle
Return

WSMETHOD CLONE WSCLIENT Service_FacturaDetalle
	Local oClone := Service_FacturaDetalle():NEW()
	oClone:ccantidadUnidades    := ::ccantidadUnidades
	oClone:ccodigoProducto      := ::ccodigoProducto
	oClone:cdescripcion         := ::cdescripcion
	oClone:cdescuento           := ::cdescuento
	oClone:cdetalleAdicionalNombre := ::cdetalleAdicionalNombre
	oClone:cdetalleAdicionalValor := ::cdetalleAdicionalValor
	oClone:oWSimpuestosDetalles := IIF(::oWSimpuestosDetalles = NIL , NIL , ::oWSimpuestosDetalles:Clone() )
	oClone:cprecioTotal         := ::cprecioTotal
	oClone:cprecioTotalSinImpuestos := ::cprecioTotalSinImpuestos
	oClone:cprecioVentaUnitario := ::cprecioVentaUnitario
	oClone:cseriales            := ::cseriales
	oClone:cunidadMedida        := ::cunidadMedida
Return oClone

WSMETHOD SOAPSEND WSCLIENT Service_FacturaDetalle
	Local cSoap := ""
	cSoap += WSSoapValue("cantidadUnidades", ::ccantidadUnidades, ::ccantidadUnidades , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("codigoProducto", ::ccodigoProducto, ::ccodigoProducto , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("descripcion", ::cdescripcion, ::cdescripcion , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("descuento", ::cdescuento, ::cdescuento , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("detalleAdicionalNombre", ::cdetalleAdicionalNombre, ::cdetalleAdicionalNombre , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("detalleAdicionalValor", ::cdetalleAdicionalValor, ::cdetalleAdicionalValor , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("impuestosDetalles", ::oWSimpuestosDetalles, ::oWSimpuestosDetalles , "ArrayOfFacturaImpuestos", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("precioTotal", ::cprecioTotal, ::cprecioTotal , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("precioTotalSinImpuestos", ::cprecioTotalSinImpuestos, ::cprecioTotalSinImpuestos , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("precioVentaUnitario", ::cprecioVentaUnitario, ::cprecioVentaUnitario , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("seriales", ::cseriales, ::cseriales , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("unidadMedida", ::cunidadMedida, ::cunidadMedida , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
Return cSoap

// WSDL Data Structure Extras

WSSTRUCT Service_Extras_Prod
	WSDATA   ccontrolInterno1          AS string OPTIONAL
	WSDATA   ccontrolInterno2          AS string OPTIONAL
	WSDATA   cnombre                   AS string OPTIONAL
	WSDATA   cpdf                      AS string OPTIONAL
	WSDATA   cvalor                    AS string OPTIONAL
	WSDATA   cxml                      AS string OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Service_Extras
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Service_Extras
Return

WSMETHOD CLONE WSCLIENT Service_Extras
	Local oClone := Service_Extras():NEW()
	oClone:ccontrolInterno1     := ::ccontrolInterno1
	oClone:ccontrolInterno2     := ::ccontrolInterno2
	oClone:cnombre              := ::cnombre
	oClone:cpdf                 := ::cpdf
	oClone:cvalor               := ::cvalor
	oClone:cxml                 := ::cxml
Return oClone

WSMETHOD SOAPSEND WSCLIENT Service_Extras
	Local cSoap := ""
	cSoap += WSSoapValue("controlInterno1", ::ccontrolInterno1, ::ccontrolInterno1 , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("controlInterno2", ::ccontrolInterno2, ::ccontrolInterno2 , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("nombre", ::cnombre, ::cnombre , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("pdf", ::cpdf, ::cpdf , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("valor", ::cvalor, ::cvalor , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("xml", ::cxml, ::cxml , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
Return cSoap

// WSDL Data Structure FacturaImpuestos

WSSTRUCT Service_FacturaImpuestos_Prod
	WSDATA   cbaseImponibleTOTALImp    AS string OPTIONAL
	WSDATA   ccodigoTOTALImp           AS string OPTIONAL
	WSDATA   ccontrolInterno           AS string OPTIONAL
	WSDATA   cporcentajeTOTALImp       AS string OPTIONAL
	WSDATA   cvalorTOTALImp            AS string OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Service_FacturaImpuestos
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Service_FacturaImpuestos
Return

WSMETHOD CLONE WSCLIENT Service_FacturaImpuestos
	Local oClone := Service_FacturaImpuestos():NEW()
	oClone:cbaseImponibleTOTALImp := ::cbaseImponibleTOTALImp
	oClone:ccodigoTOTALImp      := ::ccodigoTOTALImp
	oClone:ccontrolInterno      := ::ccontrolInterno
	oClone:cporcentajeTOTALImp  := ::cporcentajeTOTALImp
	oClone:cvalorTOTALImp       := ::cvalorTOTALImp
Return oClone

WSMETHOD SOAPSEND WSCLIENT Service_FacturaImpuestos
	Local cSoap := ""
	cSoap += WSSoapValue("baseImponibleTOTALImp", ::cbaseImponibleTOTALImp, ::cbaseImponibleTOTALImp , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("codigoTOTALImp", ::ccodigoTOTALImp, ::ccodigoTOTALImp , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("controlInterno", ::ccontrolInterno, ::ccontrolInterno , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("porcentajeTOTALImp", ::cporcentajeTOTALImp, ::cporcentajeTOTALImp , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
	cSoap += WSSoapValue("valorTOTALImp", ::cvalorTOTALImp, ::cvalorTOTALImp , "string", .F. , .F., 0 , "http://schemas.datacontract.org/2004/07/Services.Models.Object", .F.,.F.) 
Return cSoap


