#INCLUDE "PROTHEUS.CH" 
#INCLUDE "FWADAPTEREAI.CH"
#INCLUDE "FWMVCDEF.CH"  
#INCLUDE "MATI030.CH"


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �MATI030O   �Autor  �Totvs Cascavel     � Data �  23/05/2018 ���
�������������������������������������������������������������������������͹��
���Desc.     � Funcao de integracao com o adapter EAI para recebimento e  ���
���          � envio de informa��es do cadastro de Clientes (SA1)         ���
���          � utilizando o conceito de mensagem unica JSON.        	  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � MATI030O                                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function MATI030O( oEAIObEt, nTypeTrans, cTypeMessage )
	Local nCount  		:= 0
	Local nX          	:= 0
	Local cValGov     	:= 0
	Local cTypeReg		:= 1
	Local nOpcx			:= 3
	Local lHotel      	:= SuperGetMV( "MV_INTHTL", , .F. )
	Local lRet        	:= .T.
	Local lGetXnum    	:= .F.
	Local lIniPadCod  	:= .F.
	Local cRotina  		:= IIF(MA030IsMVC(),"CRMA980","MATA030")
	Local cAlias      	:= "SA1"
	Local cField      	:= "A1_COD"
	Local cOwnerMsg		:= "CUSTOMERVENDOR"
	Local cEvent       	:= "upsert"
	Local cFilSA1		:= ""
	Local cMarca      	:= ""
	Local cValInt     	:= ""
	Local cValExt     	:= ""
	Local cCode       	:= ""
	Local cStore      	:= ""
	Local cLograd     	:= ""
	Local cNumero     	:= ""
	Local cCodEst     	:= ""
	Local cCodEstE    	:= ""
	Local cCodMun     	:= ""
	Local cCodMunE    	:= ""
	Local cPais  		:= ""
	Local cCodPais     	:= ""
	Local cEst   		:= ""
	Local cTel        	:= ""
	Local cTipoCli		:= ""
	Local cIniCli		:= ""
	Local cIniLoj		:= ""
	Local aRet        	:= {}
	Local aCliente    	:= {}
	Local aAux         	:= {}
	Local aAreaCCH		:= {}
	Local cEndEnt		:= ""
	Local cPaisCode   	:= ""
	Local cLogErro		:= ""
	Local oModel 		:= Nil
	Local ofwEAIObj		:= FWEAIobj():NEW()
	Local oMsgError		:= ""
	Local cProduct		:= ""
	Local cCNPJCPF		:= ""
	Local cTipoRG		:= ""
	Local cRG			:= ""
	Local lPFisica		:= .F.
		
	Private lMsErroAuto    := .F.
	Private lAutoErrNoFile := .T.
	Private lMsHelpAuto    := .T.

   	//--------------------------------------
	//recebimento mensagem
	//--------------------------------------
	If nTypeTrans == TRANS_RECEIVE .And. ValType( oEAIObEt ) == 'O' 
		
		//--------------------------------------
		//chegada de mensagem de negocios
		//--------------------------------------
		If cTypeMessage == EAI_MESSAGE_BUSINESS
			
			//Ajustado para receber o Type conforme documentacao API Totvs
			//Tipo do Registro: Customer Cliente, Vendor Fornecedor, Both Ambos
			//Identifica se o emitente � apenas Cliente, apenas Fornecedor ou Ambos 
			//1 � Cliente  
			//2 � Fornecedor 
			//3 � Ambos 
			Do Case
				Case oEAIObEt:getPropValue("Type") == 1 //Cliente
					cTypeReg := "CUSTOMER"
				Case oEAIObEt:getPropValue("Type") == 2 //Fornecedor
					cTypeReg := "VENDOR"
				Case oEAIObEt:getPropValue("Type") == 3 //Ambos
					cTypeReg := "BOTH"
				Otherwise
					lRet := .F.
					cLogErro := "Tipo de emitente fora de lista de valores (1=CUSTOMER,2=VENDOR,3=BOTH)" + CRLF
			Endcase	

			If lRet .And. (cTypeReg == "VENDOR"  .or. cTypeReg == "BOTH" ) 
			
				aRet := FWIntegDef("MATA020", cTypeMessage, nTypeTrans, oEAIObEt)
				
				If ValType(aRet) == "A"
					If !Empty(aRet)
						If cTypeReg == "VENDOR"
							lRet := aRet[1]
							ofwEAIObj := aRet[2]
						Else
							lRet := aRet[1]
							cLogErro := aRet[1][2] + CRLF
						Endif
					EndIf
				Endif			
			Endif

			If lRet
				If cTypeReg == "CUSTOMER" .Or. cTypeReg == "BOTH"
					
					If !Upper(RTrim(cEvent)) $ "UPSERT|REQUEST|DELETE" 
						lRet := .F.
						cLogErro := STR0014 + CRLF // "O evento informado � inv�lido!"	
					EndIf
					// Obt�m a marca
					If oEAIObEt:getHeaderValue("ProductName") !=  Nil .And. !Empty( oEAIObEt:getHeaderValue("ProductName") )
						cMarca :=  oEAIObEt:getHeaderValue("ProductName")
					Else
						lRet := .F.						
						ofwEAIObj:Activate()
						ofwEAIObj:setProp("ReturnContent")
						cLogErro := STR0010 // "Product � obrigat�rio!"
						ofwEAIObj:getPropValue("ReturnContent"):setProp("Error", cLogErro)														
					EndIf
				
					// Obt�m o Valor externo
					If oEAIObEt:getPropValue("InternalId") != nil  .And. !Empty( oEAIObEt:getPropValue("InternalId") )
						cValExt := oEAIObEt:getPropValue("InternalId")
					Else
						lRet := .F.							
						ofwEAIObj:Activate()
						ofwEAIObj:setProp("ReturnContent")
						cLogErro := STR0011 // "InternalId � obrigat�rio!"
						ofwEAIObj:getPropValue("ReturnContent"):setProp("Error", cLogErro)														
					EndIf
		
					//Obt�m o code
					If oEAIObEt:getPropValue("Code") != nil .And. !Empty( oEAIObEt:getPropValue("Code") )
						cCode := oEAIObEt:getPropValue("Code")
					Else
						//Se for integra��o com hotelaria, ir� gerar um c�digo sequencial ou considerar o inicializador padr�o do campo c�digo
						If !lHotel
							lRet := .F.							
							ofwEAIObj:Activate()
							ofwEAIObj:setProp("ReturnContent")
							cLogErro := STR0012 // "Code � obrigat�rio!"
							ofwEAIObj:getPropValue("ReturnContent"):setProp("Error", cLogErro)															
						Endif
					EndIf
					//Obt�m a loja
					If oEAIObEt:getPropValue("StoreId") != nil 
						cStore := oEAIObEt:getPropValue("StoreId")
					EndIf
					
					//Obt�m o valor interno
					aAux := IntCliInt(cValExt, cMarca)
					
					// Se o evento � Upsert
					cEvent := AllTrim(oEAIObEt:getEvent())
					
					If ( Upper(cEvent) == "UPSERT" ) .Or. ( Upper(cEvent) == "REQUEST" )
						cFilSA1		:= xFilial("SA1")
						// Se o registro existe
						If Len( aAux ) > 0 
							//Verifica se o cliente existe na Base, pois em casos onde um registro
							//� exclu�do ap�s a integra��o o sistema n�o consegue importar novamente.
							DbSelectArea("SA1")
							SA1->(DbSetOrder(1))
							If SA1->(MSSeek(cFilSA1+PADR(AAUX[2][3],LEN(SA1->A1_COD))+ PADR(AAUX[2][4],LEN(SA1->A1_LOJA))))
								nOpcx := 4 // Update
							Else
								nOpcx := 3 // Incluir
							EndIf
						EndIf
					// Se o evento � Delete
					ElseIf ( Upper(cEvent) == "DELETE" ) 
						// Se o registro existe
						If Len( aAux ) > 0 
							nOpcx := 5 // Delete
						Else
							lRet := .F.
							ofwEAIObj:Activate()
							ofwEAIObj:setProp("ReturnContent")
							cLogErro := STR0013 + " -> " + cValExt // "O registro a ser exclu�do n�o existe na base Protheus!"
							ofwEAIObj:getPropValue("ReturnContent"):setProp("Error", cLogErro)															
						EndIf
					Else
						lRet := .F.						
						ofwEAIObj:Activate()
						ofwEAIObj:setProp("ReturnContent")
						cLogErro := STR0014 // "O evento informado � inv�lido!"
						ofwEAIObj:getPropValue("ReturnContent"):setProp("Error", cLogErro)															
					EndIf
		
					// Se � Insert
					If nOpcx == 3
						If Alltrim(cMarca)=="HIS"
							dbSelectArea("SA1")
							dbSetOrder(1)
							If MsSeek(cFilSA1+PadR(cCode, TamSX3("A1_COD")[1])+PadR(cStore, TamSX3("A1_LOJA")[1]))
								nOpcx := 4
								aAdd(aCliente, {"A1_COD",  cCode, Nil})  // C�digo
								aAdd(aCliente, {"A1_LOJA", cStore, Nil}) // Loja
							Else
								cIniCli := GetSx3Cache("A1_COD","X3_RELACAO")
								cIniLoj := GetSx3Cache("A1_LOJA","X3_RELACAO")
								
								// Se n�o h� inicializador padr�o ou se A030INICPD esta contido, pois
								// este inicializador padr�o � utilizado apenas pela RM					   
								If Empty(cIniCli) .Or. "A030INICPD" $ cIniCli
									aAdd(aCliente, {"A1_COD",  cCode, Nil})  // C�digo
								EndIf
							
								If Empty(cIniLoj)
									aAdd(aCliente, {"A1_LOJA", cStore, Nil}) // Loja
								EndIf
							EndIf
						Else
							// Se n�o h� inicializador padr�o
							cFormula := GetSx3Cache("A1_COD","X3_RELACAO")
							lIniPadCod := !Empty(cFormula) .And. !( "A030INICPD" $ cFormula )
							
							If !lIniPadCod
								//Se for integra��o com hotelaria, gera um c�digo sequencial (pode ser alterada a l�gica atrav�s de incializador padr�o)
								If lHotel
									cCode := ProxNum()
								Else
									cCode := MATI030Num(cCode,@lGetXnum)
								EndIf
		
								aAdd(aCliente, {"A1_COD",  cCode, Nil})  // C�digo
							EndIf
					
							If Empty(GetSx3Cache("A1_LOJA","X3_RELACAO"))
								//Se for integra��o com hotelaria, fixa a loja como "00" (pode ser alterada a l�gica atrav�s de incializador padr�o) 
								If lHotel .Or. Empty(cStore)
									cStore := PadL(cStore,TamSX3("A1_LOJA")[1],"0")
								Endif
		
								aAdd(aCliente, {"A1_LOJA", cStore, Nil}) // Loja
							EndIf
						EndIf
					Else
						cValInt := IntCliExt(, , aAux[2][3], aAux[2][4])[2]
						aAdd(aCliente, {"A1_COD",  PadR(aAux[2][3], TamSX3("A1_COD")[1]), Nil})  // C�digo
						aAdd(aCliente, {"A1_LOJA", PadR(aAux[2][4], TamSX3("A1_LOJA")[1]), Nil}) // Loja
					EndIf
		
					If nOpcx != 5
						// Obt�m o Nome ou Raz�o Social
						If oEAIObEt:getPropValue("Name") != nil .And. !Empty( oEAIObEt:getPropValue("Name") )
							aAdd(aCliente, {"A1_NOME", (UPPER(AllTrim(oEAIObEt:getPropValue("Name")))), Nil})
						Else
							lRet := .F.							
							ofwEAIObj:Activate()
							ofwEAIObj:setProp("ReturnContent")
							cLogErro := STR0015 // "O nome � obrigat�rio!"
							ofwEAIObj:getPropValue("ReturnContent"):setProp("Error", cLogErro)																
						EndIf
		
						// Obt�m o Nome de Fantasia
						If oEAIObEt:getPropValue("ShortName") != nil .And. !Empty( oEAIObEt:getPropValue("ShortName") ) 
							aAdd(aCliente, {"A1_NREDUZ", (UPPER(AllTrim(oEAIObEt:getPropValue("ShortName")))), Nil})
						Else
							lRet := .F.								
							ofwEAIObj:Activate()
							ofwEAIObj:setProp("ReturnContent")
							cLogErro := STR0016 // "O nome reduzido � obrigat�rio!"
							ofwEAIObj:getPropValue("ReturnContent"):setProp("Error", cLogErro)																
						EndIf
		
						// Obt�m Pessoa/Tipo
						If oEAIObEt:getPropValue("EntityType") != nil .And. !Empty( oEAIObEt:getPropValue("EntityType") )
							
							//Ajustado para receber o campo EntityType conforme documentacao API Totvs
							//Identifica se o emitente � Pessoa F�sica, Jur�dica, Estrangeiro ou Trading 
							//1 � Pessoa F�sica 
							//2 � Pessoa Jur�dica 
							//3 � Estrangeiro 
							//4 � Trading
							
							If oEAIObEt:getPropValue("EntityType") == 1 //"PERSON"
								aAdd(aCliente, {"A1_PESSOA", "F", Nil}) // Pessoa F�sica
						
							ElseIf oEAIObEt:getPropValue("EntityType") == 2 //"COMPANY"
								aAdd(aCliente, {"A1_PESSOA", "J", Nil}) // Pessoa Jur�dica
							EndIf
						
							If cPaisLoc <> 'BRA'
								aAdd(aCliente, {"A1_TIPO", "1", Nil})
							Else
								aAdd(aCliente, {"A1_TIPO",   "F", Nil}) // Consumidor Final
							EndIf
						Else
							lRet := .F.							
							ofwEAIObj:Activate()
							ofwEAIObj:setProp("ReturnContent")
							cLogErro := STR0017 // "O tipo do cliente � obrigat�rio"
							ofwEAIObj:getPropValue("ReturnContent"):setProp("Error", cLogErro)																
						EndIf
		
						//Pega o tipo de cliente (Cons. Final, Revendedor, Exporta��o, etc)
						If cPaisLoc == 'BRA'
							If oEAIObEt:getPropValue("StrategicCustomerType") != nil 
								cTipoCli := oEAIObEt:getPropValue("StrategicCustomerType")
								
								//Trata o tipo de cliente considerando o formato esperado no Protheus para grava��o desse dado
								If cTipoCli == "1"
									cTipoCli := "F"
								Elseif cTipoCli == "2"
									cTipoCli := "L"
								Elseif cTipoCli == "3"
									cTipoCli := "R"
								Elseif cTipoCli == "4"
									cTipoCli := "S"
								Elseif cTipoCli == "5"
									cTipoCli := "X"
								Endif
								
								aAdd( aCliente, {"A1_TIPO", cTipoCli, Nil} )
							Else
								aAdd( aCliente, {"A1_TIPO", "F", Nil} ) //Consumidor Final
							Endif
						Endif

						If oEAIObEt:getPropValue("Address") != nil .And. !Empty( oEAIObEt:getpropvalue("Address") )
							// Obt�m o N�mero do Endere�o do Cliente						
							If oEAIObEt:getPropValue("Address"):getPropValue("Number") != nil   
								aAdd(aCliente, {"A1_END", (UPPER(AllTrim(oEAIObEt:getPropValue("Address"):getPropValue("Address")))) + ", " + UPPER(oEAIObEt:getPropValue("Address"):getPropValue("Number")), Nil})
							Else
								aAdd(aCliente, {"A1_END", (UPPER(AllTrim(oEAIObEt:getPropValue("Address"):getPropValue("Address")))), Nil})
							EndIf

							// Obt�m o Complemento do Endere�o
							If oEAIObEt:getPropValue("Address"):getPropValue("Complement") != nil  
								aAdd(aCliente, {"A1_COMPLEM", (UPPER(oEAIObEt:getPropValue("Address"):getPropValue("Complement"))), Nil})
							EndIf

							// Obt�m o Bairro do Cliente
							If oEAIObEt:getPropValue("Address"):getPropValue("District") != nil  
								aAdd(aCliente, {"A1_BAIRRO", (UPPER(oEAIObEt:getPropValue("Address"):getPropValue("District"))), Nil})
							EndIf						

							// Obt�m o Cod Endere�amento Postal
							If oEAIObEt:getPropValue("Address"):getPropValue("ZIPCode") != nil 
								aAdd(aCliente, {"A1_CEP", oEAIObEt:getPropValue("Address"):getPropValue("ZIPCode"), Nil})
							EndIf

							// Obt�m a Caixa Postal
							If oEAIObEt:getPropValue("Address"):getPropValue("POBox") != nil 
								aAdd(aCliente, {"A1_CXPOSTA", oEAIObEt:getPropValue("Address"):getPropValue("POBox"), Nil})
							EndIf

							//Obt�m o c�digo de Pais do Cliente, no padr�o BACEN, atrav�s da descri��o recebida (Exemplo: Brasil = 01058)
							If cPaisLoc == 'BRA' .Or. cPaisLoc == 'ARG'//Paises que utilizam a tabela CCH
								If oEAIObEt:getPropValue("Address"):getPropValue("Country") != NIL .AND. oEAIObEt:getPropValue("Address"):getPropValue("Country"):getPropValue("CountryDescription") != nil 
									cPais := AllTrim((Upper(oEAIObEt:getPropValue("Address"):getPropValue("Country"):getPropValue("CountryDescription"))))
									//Tratativa para considerar o nome do pais "BRAZIL"
									If cPaisLoc == "BRA"
										If cPais == "BRAZIL"
											cPais := "BRASIL"
										EndIf
									EndIf
									
									aAreaCCH := CCH->( GetArea() )
									cCodPais := PadR( Posicione( "CCH", 2, FWxFilial("CCH") + PadR( cPais, TamSx3("CCH_PAIS")[1] ), "CCH_CODIGO" ), TamSx3("A1_CODPAIS")[1] )
								
									CCH->( RestArea( aAreaCCH ) )
									If ! Empty( cCodPais )
										aAdd( aCliente, { "A1_CODPAIS", cCodPais, Nil } )
									EndIf
								EndIf
							EndIf
						
							//Obt�m o Pais do Cliente pelo c�digo (padr�o SISCOMEX)
							If oEAIObEt:getPropValue("Address"):getPropValue("Country") != NIL .and. oEAIObEt:getPropValue("Address"):getPropValue("Country"):getPropValue("CountryCode") != nil 
								cPaisCode := oEAIObEt:getPropValue("Address"):getPropValue("Country"):getPropValue("CountryCode")
								cPaisCode := PadR( cPais, GetSX3Cache("A1_PAIS","X3_TAMANHO") )
							EndIf
						
							//Busca o pa�s por c�digo ou descri��o
							cPaisCode := MATI30Pais(cPaisCode, cPais, cMarca)
						
							If !Empty(cPaisCode)
								aAdd(aCliente, {"A1_PAIS", cPaisCode, Nil})
								If cPaisCode <> A2030PALOC("SA1",1)
									cEst := "EX"
								Endif
							Else
								If !Empty(cPais) .And. cPais <> A2030PALOC("SA1",2)
									cEst := "EX"
								Endif
							EndIf

							// Obt�m a Sigla da Federa��o
							If oEAIObEt:getPropValue("Address"):getPropValue("State") != NIL .AND. oEAIObEt:getPropValue("Address"):getPropValue("State"):getPropValue("StateCode") != nil 
								If Empty(cEst)
									cEst := AllTrim(Upper(oEAIObEt:getPropValue("Address"):getPropValue("State"):getPropValue("StateCode")))
								Endif
								aAdd(aCliente, {"A1_EST", cEst, Nil})
							Else
								lRet := .F.								
								ofwEAIObj:Activate()
								ofwEAIObj:setProp("ReturnContent")
								cLogErro := STR0019 // "O estado � obrigat�rio"
								ofwEAIObj:getPropValue("ReturnContent"):setProp("Error", cLogErro)																	
							EndIf

							If oEAIObEt:getPropValue("Address"):getPropValue("City") != Nil 

								// Obt�m a descri��o do Munic�pio do Cliente							
								If oEAIObEt:getPropValue("Address"):getPropValue("City"):getPropValue("CityDescription") != nil 
									aAdd(aCliente, {"A1_MUN", (UPPER(oEAIObEt:getPropValue("Address"):getPropValue("City"):getPropValue("CityDescription"))), Nil})
								Else
									lRet := .F.							
									ofwEAIObj:Activate()
									ofwEAIObj:setProp("ReturnContent")
									cLogErro := STR0020 // "A descri��o do munic�pio � obrigat�ria"
									ofwEAIObj:getPropValue("ReturnContent"):setProp("Error", cLogErro)																
								EndIf

								// Obt�m o C�digo do Munic�pio
								If oEAIObEt:getPropValue("Address"):getPropValue("City"):getPropValue("CityCode") != nil 
									aAdd(aCliente, {"A1_COD_MUN", Right(oEAIObEt:getPropValue("Address"):getPropValue("City"):getPropValue("CityCode"), 5), Nil})
								EndIf

							Else
								lRet := .F.							
								ofwEAIObj:Activate()
								ofwEAIObj:setProp("ReturnContent")
								cLogErro := STR0020 // "A descri��o do munic�pio � obrigat�ria"
								ofwEAIObj:getPropValue("ReturnContent"):setProp("Error", cLogErro)	
							Endif

						Else
							lRet := .F.							
							ofwEAIObj:Activate()
							ofwEAIObj:setProp("ReturnContent")
							cLogErro := STR0018 // "O Endere�o � obrigat�rio"
							ofwEAIObj:getPropValue("ReturnContent"):setProp("Error", cLogErro)																
						EndIf

						// Obt�m Inscri��o Estadual/Inscri��o Municipal/CNPJ/CPF do Fornecedor
						If oEAIObEt:getPropValue("GovernmentalInformation") != nil
							oTaxes := oEAIObEt:getPropValue("GovernmentalInformation")
							For nX := 1 To Len( oTaxes )
								If oTaxes[nX]:getPropValue("Name") != nil
									cValGov := oTaxes[nX]:getPropValue("Id")
									
									If RTrim(Upper(oTaxes[nX]:getPropValue("Name"))) == "INSCRICAO ESTADUAL"
										aAdd(aCliente, {"A1_INSCR", cValGov, Nil})
									ElseIf RTrim(Upper(oTaxes[nX]:getPropValue("Name"))) == "INSCRICAO MUNICIPAL"
										aAdd(aCliente, {"A1_INSCRM", cValGov, Nil})
									ElseIf RTrim(Upper(oTaxes[nX]:getPropValue("Name"))) $ "CPF/CNPJ"
										aAdd(aCliente, {"A1_CGC", cValGov, Nil})
									ElseIf RTrim(Upper(oTaxes[nX]:getPropValue("Name"))) == "SUFRAMA"
										aAdd(aCliente, {"A1_SUFRAMA", cValGov, Nil})
									ElseIf RTrim(Upper(oTaxes[nX]:getPropValue("Name"))) == "PASSAPORTE" .AND. cPaisLoc == "BRA"
										aAdd(aCliente, {"A1_PFISICA", cValGov, Nil})
									ElseIf RTrim(Upper(oTaxes[nX]:getPropValue("Name"))) == "RG" .AND. cPaisLoc == "BRA"
										aAdd(aCliente, {"A1_PFISICA", cValGov, Nil})
										aAdd(aCliente, {"A1_RG", cValGov, Nil})
									EndIf
								
								Endif
							Next nX
						EndIf
						
						If oEAIObEt:getPropValue("ListOfCommunicationInformation") != nil 
							oLtOfCom := oEAIObEt:getPropValue("ListOfCommunicationInformation")				
							cStringTemp:= RemCharEsp(oLtOfCom[Len(oLtOfCom)]:getpropvalue('PhoneNumber'))							
							aTelefone := RemDddTel(cStringTemp)							
							aAdd(aCliente, {"A1_TEL",aTelefone[1], Nil})

							// Obt�m o N�mero do Telefone
							If !Empty(aTelefone[2])
								If Len(AllTrim(aTelefone[2])) == 2
									aTelefone[2] := "0" + aTelefone[2]
								Endif
								aAdd(aCliente, {"A1_DDD",aTelefone[2], Nil})
							Elseif ( oLtOfCom[Len(oLtOfCom)]:getpropvalue('DiallingCode') != nil  )
								cStringTemp:= RemCharEsp(oLtOfCom[Len(oLtOfCom)]:getpropvalue('DiallingCode'))
								aTelefone[2] := "0" + allTrim(cStringTemp)
								aAdd(aCliente, {"A1_DDD",aTelefone[2], Nil})
							EndIf
		
							If !Empty(aTelefone[3])
								aAdd(aCliente, {"A1_DDI",aTelefone[3], Nil})
							ElseIF ( oLtOfCom[Len(oLtOfCom)]:getpropvalue('InternationalDiallingCode') != nil )
								cStringTemp:= RemCharEsp(oLtOfCom[Len(oLtOfCom)]:getpropvalue('InternationalDiallingCode'))
								aTelefone[3] := allTrim(cStringTemp)
								aAdd(aCliente, {"A1_DDI",aTelefone[3], Nil})
							EndIf

							// Obt�m o E-Mail
							If oLtOfCom[Len(oLtOfCom)]:getpropvalue('Email') != nil 
								aAdd(aCliente, {"A1_EMAIL", oLtOfCom[Len(oLtOfCom)]:getpropvalue('Email'), Nil})
							Endif

							// Obt�m o N�mero do Fax do Cliente.
							If oLtOfCom[Len(oLtOfCom)]:getpropvalue('FaxNumber') != nil 
								cStringTemp:= RemCharEsp(oLtOfCom[Len(oLtOfCom)]:getpropvalue('FaxNumber'))
								aTelefone := RemDddTel(cStringTemp)								
								Aadd( aCliente, { "A1_FAX",aTelefone[1],   Nil })
							Endif

							// Obt�m a Home-Page
							If oLtOfCom[Len(oLtOfCom)]:getpropvalue('HomePage') != nil 
								aAdd(aCliente, {"A1_HPAGE", oLtOfCom[Len(oLtOfCom)]:getpropvalue('HomePage'), Nil}) // Home-Page
							Endif
						EndIf
		
						// Obt�m o Contato na Empresa
						If oEAIObEt:getPropValue("ListOfContacts") != nil 
							oLtOfCont := oEAIObEt:getPropValue("ListOfContacts")
							If oLtOfCont[Len(oLtOfCont)]:getpropvalue('ContactInformationName') != nil
								aAdd(aCliente, {"A1_CONTATO", (oLtOfCont[Len(oLtOfCont)]:getpropvalue('ContactInformationName')), Nil})
							Endif
						EndIf
		
						// Obt�m Bloqueia o Cliente?
						If oEAIObEt:getPropValue("RegisterSituation") != nil 
							If Upper(oEAIObEt:getPropValue("RegisterSituation")) == "ACTIVE"
								aAdd(aCliente, {"A1_MSBLQL", "2", Nil})
							Else
								aAdd(aCliente, {"A1_MSBLQL", "1", Nil})
							EndIf
						Else
							If !lHotel //Case seja integra��o com hotelaria, e essa tag esteja vazia, n�o muda o status para bloqueado
								aAdd(aCliente, {"A1_MSBLQL", "1", Nil})
							Endif
						EndIf
		
						// Obt�m a Data de Nasc. ou Abertura
						If oEAIObEt:getpropvalue("RegisterDate") != nil 
							aAdd(aCliente, {"A1_DTNASC", CTOD(oEAIObEt:getpropvalue("RegisterDate")), Nil})
						EndIf
						
						// Obt�m o End. de Cobr. do Cliente
						If oEAIObEt:getPropValue("BillingInformation") != nil  
							If oEAIObEt:getPropValue("BillingInformation"):getPropValue("Address") != nil  
								If oEAIObEt:getPropValue("BillingInformation"):getPropValue("Address"):getPropValue("Number") != nil 
									aAdd(aCliente, {"A1_ENDCOB", (oEAIObEt:getPropValue("BillingInformation"):getPropValue("Address"):getPropValue("Number")), Nil})
								EndIf
		
								// Obt�m o Bairro de Cobran�a
								If oEAIObEt:getPropValue("BillingInformation"):getPropValue("Address"):getPropValue("District") != nil 
									aAdd(aCliente, {"A1_BAIRROC", (oEAIObEt:getPropValue("BillingInformation"):getPropValue("Address"):getPropValue("District")), Nil})
								EndIf
			
								// Obt�m o Cep de Cobran�a
								If oEAIObEt:getPropValue("BillingInformation"):getPropValue("Address"):getPropValue("ZIPCode") != nil 
									aAdd(aCliente, {"A1_CEPC", oEAIObEt:getPropValue("BillingInformation"):getPropValue("Address"):getPropValue("ZIPCode"), Nil})
								EndIf
			
								// Obt�m o Munic�pio de Cobran�a
								If oEAIObEt:getPropValue("BillingInformation"):getPropValue("Address"):getPropValue("City"):getPropValue("CityDescription") != nil 
									aAdd(aCliente, {"A1_MUNC", (oEAIObEt:getPropValue("BillingInformation"):getPropValue("Address"):getPropValue("City"):getPropValue("CityDescription")), Nil})
								EndIf
			
								// Obt�m a Uf de Cobran�a
								If oEAIObEt:getPropValue("BillingInformation"):getPropValue("Address"):getPropValue("State"):getPropValue("StateCode") != nil 
									aAdd(aCliente, {"A1_ESTC", oEAIObEt:getPropValue("BillingInformation"):getPropValue("Address"):getPropValue("State"):getPropValue("StateCode"), Nil})
								EndIf
							EndIf
						Endif
		
						// Obt�m o End. de Entr. do Cliente
						If oEAIObEt:getPropValue("ShippingAddress") != nil 
							If oEAIObEt:getPropValue("ShippingAddress"):getPropValue("Address") != nil
								cEndEnt :=  AllTrim(oEAIObEt:getPropValue("ShippingAddress"):getPropValue("Address"))
								If oEAIObEt:getPropValue("ShippingAddress"):getPropValue("Number") != nil 
									If !Empty(AllTrim(oEAIObEt:getPropValue("ShippingAddress"):getPropValue("Number")))
										cEndEnt += ", " + AllTrim(oEAIObEt:getPropValue("ShippingAddress"):getPropValue("Number"))
									Endif
								Endif
								cEndEnt := AllTrim(Upper(cEndEnt))
							
								If oEAIObEt:getPropValue("ShippingAddress"):getPropValue("Complement") != nil 
									If !Empty(AllTrim(oEAIObEt:getPropValue("ShippingAddress"):getPropValue("Complement")))
										cEndEnt += ", " + AllTrim(oEAIObEt:getPropValue("ShippingAddress"):getPropValue("Complement"))
									Endif
								Endif
								cEndEnt := AllTrim(Upper(cEndEnt))
								
								Aadd( aCliente, { "A1_ENDENT",cEndEnt, Nil })
							EndIf	
		
							// Obt�m o Cep de Entrega
							If oEAIObEt:getPropValue("ShippingAddress"):getPropValue("ZIPCode") != nil 
								aAdd(aCliente, {"A1_CEPE", oEAIObEt:getPropValue("ShippingAddress"):getPropValue("ZIPCode"), Nil})
							EndIf
		
							// Obt�m o Bairro de Entrega
							If oEAIObEt:getPropValue("ShippingAddress"):getPropValue("District") != nil 
								aAdd(aCliente, {"A1_BAIRROE", oEAIObEt:getPropValue("ShippingAddress"):getPropValue("District"), Nil})
							EndIf
		
							// Obt�m o Estado de Entrega
							If oEAIObEt:getPropValue("ShippingAddress"):getPropValue("State") != nil 
								If oEAIObEt:getPropValue("ShippingAddress"):getPropValue("State"):getPropValue("StateCode") != nil 
									aAdd(aCliente, {"A1_ESTE", oEAIObEt:getPropValue("ShippingAddress"):getPropValue("State"):getPropValue("StateCode"), Nil})
								EndIf
							Endif
						
							If oEAIObEt:getPropValue("ShippingAddress"):getPropValue("City") != nil
								// Obt�m o Munic�pio da Entrega
								If oEAIObEt:getPropValue("ShippingAddress"):getPropValue("City"):getPropValue("CityCode") != nil 
									cMunEnt := Right(oEAIObEt:getPropValue("ShippingAddress"):getPropValue("City"):getPropValue("CityCode"), 5)
									aAdd(aCliente, {"A1_CODMUNE", cMunEnt, Nil } )
								EndIf
				
								// Obt�m a descri��o do Munic�pio de Entrega
								If oEAIObEt:getPropValue("ShippingAddress"):getPropValue("City"):getPropValue("CityDescription") != nil 
									aAdd(aCliente, {"A1_MUNE", oEAIObEt:getPropValue("ShippingAddress"):getPropValue("City"):getPropValue("CityDescription"), Nil})
								EndIf
							Endif
						Endif
						
						// Grava o campo "A1_ORIGEM" somente se for integracao com HIS
						If Alltrim(cMarca)=="HIS"
							aAdd( aCliente, { "A1_ORIGEM", "S1", Nil } )
						EndIf
					EndIf
		
					//Ponto de entrada para incluir campos no array aCliente
					If ExistBlock("MTI030NOM")
						aRetPe := ExecBlock("MTI030NOM",.F.,.F.,{aCliente,oEAIObEt:getPropValue("Name")})
						If ValType(aRetPe) == "A" .And. Len(aRetPe) >0
							If ValType(aRetPe) == "A"
								aCliente := aClone(aRetPe)
							EndIf
						EndIf
					EndIf
						
					//Ordena Array conforme dicionario de dados
					aCliente := FWVetByDic(aCliente,"SA1",.F.)
					
					//Verifica se houve erro antes da rotina automatica
					If lRet .AND. Empty( cLogErro )
					
						// Executa Rotina Autom�tica conforme evento
						If MA030IsMVC()
							MSExecAuto({|x, y| CRMA980(x, y)}, aCliente, nOpcx)
						Else
							MSExecAuto({|x, y| MATA030(x, y)}, aCliente, nOpcx)
						EndIf
			
						// Se a Rotina Autom�tica retornou erro
						If lMsErroAuto
							lRet := .F.
							
							// Obt�m o log de erros
							aErroAuto := GetAutoGRLog()
			
							// Varre o array obtendo os erros e quebrando a linha
							cLogErro := ""
							For nCount := 1 To Len(aErroAuto)
								cLogErro += StrTran( StrTran( aErroAuto[nCount], "<", "" ), "-", "" ) + CRLF
							Next nCount
							
							ofwEAIObj:Activate()
							ofwEAIObj:setProp("ReturnContent")
							ofwEAIObj:getPropValue("ReturnContent"):setProp("Error", cLogErro)
			
							lRet := .F.
							//Cancela a utiliza��o do c�digo sequencial
							If (lHotel .Or. lGetXnum ) .And. !lIniPadCod
								RollBackSX8()
							Endif
							
						Else
							// CRUD do XXF (de/para)
							If nOpcx == 3 // Insert
								cValInt := IntCliExt(, , SA1->A1_COD, SA1->A1_LOJA)[2]
								CFGA070Mnt(cMarca, cAlias, cField, cValExt, cValInt, .F.,,,cOwnerMsg)
							
								//Confirma a utiliza��o do c�digo sequencial
								If (lHotel .Or. lGetXnum ).AND. ! lIniPadCod
									ConfirmSX8()
								Endif
							ElseIf nOpcx = 4 // Update
								// se for integracao com o HIS e n�o houver internalId, 
								// ent�o o His esta sincronizando o cliente dele com a do Protheus.
								// necessitando a geracao do internalId
								If Alltrim(cMarca)=="HIS" .AND. Empty(cValInt)
									cValInt := IntCliExt(, , SA1->A1_COD, SA1->A1_LOJA)[2]
								EndIf
								CFGA070Mnt(cMarca, cAlias, cField, cValExt, cValInt, .F.,,,cOwnerMsg)
							Else  // Delete
								CFGA070Mnt(cMarca, cAlias, cField, cValExt, cValInt, .T.,,,cOwnerMsg)								
							EndIf
							
							lRet := .T.				
						EndIf																					
					Endif
				EndIf												
			EndIf

			If cTypeReg <> "VENDOR"
				ofwEAIObj:Activate()
				ofwEAIObj:setProp("ReturnContent")
				If !lRet
					ofwEAIObj:getPropValue("ReturnContent"):setProp("Error", cLogErro)		
				Else
					ofwEAIObj:getPropValue("ReturnContent"):setProp("ListOfInternalID",{},'InternalId',,.T.)
					ofwEAIObj:getPropValue("ReturnContent"):get("ListOfInternalID")[1]:setprop("Name",cOwnerMsg,,.T.)
					ofwEAIObj:getPropValue("ReturnContent"):get("ListOfInternalID")[1]:setprop("Origin",cValExt,,.T.)
					ofwEAIObj:getPropValue("ReturnContent"):get("ListOfInternalID")[1]:setprop("Destination",cValInt,,.T.)					
				EndIf					
			EndIf
		//--------------------------------------
		//resposta da mensagem Unica TOTVS
		//--------------------------------------
		ElseIf cTypeMessage == EAI_MESSAGE_RESPONSE
		
	       	// Se n�o houve erros na resposta
			If Upper(oEAIObEt:getPropValue("ProcessingInformation"):getPropValue("Status")) == "OK"  
	            // Verifica se a marca foi informada
	            cProduct := oEAIObEt:getHeaderValue("ProductName")
				If oEAIObEt:getHeaderValue("ProductName") !=  nil .And. !Empty( oEAIObEt:getHeaderValue("ProductName") )  .AND. ValType(cProduct) = "C" 
					cProduct := oEAIObEt:getHeaderValue("ProductName")
				Else
					lRet := .F.					
					ofwEAIObj:Activate()
					ofwEAIObj:setProp("ReturnContent")
					cLogErro := STR0021 // "Erro no retorno. O Product � obrigat�rio!"
					ofwEAIObj:getPropValue("ReturnContent"):setProp("Error", cLogErro)
															
				EndIf
	
				If lRet .and. oEAIObEt:getPropValue("ReturnContent"):getPropValue("ListOfInternalID") !=  nil 
		            // Verifica se o c�digo interno foi informado
					If oEAIObEt:getPropValue("ReturnContent"):getPropValue("ListOfInternalID")[1]:getPropValue("Origin") != nil 
						cValInt := oEAIObEt:getPropValue("ReturnContent"):getPropValue("ListOfInternalID")[1]:getPropValue("Origin")
					Else
						lRet := .F.						
						ofwEAIObj:Activate()
						ofwEAIObj:setProp("ReturnContent")
						cLogErro := STR0022 // "Erro no retorno. O OriginalInternalId � obrigat�rio!"
						ofwEAIObj:getPropValue("ReturnContent"):setProp("Error", cLogErro)																
					EndIf
		
		            // Verifica se o c�digo externo foi informado
					If oEAIObEt:getPropValue("ReturnContent"):getPropValue("ListOfInternalID")[1]:getPropValue("Destination") != nil
						cValExt := oEAIObEt:getPropValue("ReturnContent"):getPropValue("ListOfInternalID")[1]:getPropValue("Destination")
					Else
						lRet := .F.
						ofwEAIObj:Activate()
						ofwEAIObj:setProp("ReturnContent")
						cLogErro := STR0023 // "Erro no retorno. O DestinationInternalId � obrigat�rio"
						ofwEAIObj:getPropValue("ReturnContent"):setProp("Error", cLogErro)
					EndIf											
					cEvent := oEAIObEt:getPropValue("ReceivedMessage"):getPropValue("Event")
					
					If cEvent != NIL
						cEvent := Upper(cEvent)
					EndIf
		
					If RTrim(cEvent) $ "UPSERT|DELETE"
		          		//Atualiza o registro na tabela XXF (de/para)
						If !CFGA070Mnt(cProduct, cAlias, cField, cValExt, cValInt, (cEvent = "DELETE"),,,cOwnerMsg)
							cLogErro := "N�o foi poss�vel gravar na tabela De/Para. Evento :" +cEvent + CRLF
							lRet := .F.
						EndIf
					Else
						lRet := .F.						
						ofwEAIObj:Activate()
						ofwEAIObj:setProp("ReturnContent")
						cLogErro := STR0025 // "Evento do retorno inv�lido!"
						ofwEAIObj:getPropValue("ReturnContent"):setProp("Error", cLogErro)																
					EndIf
				Endif
			Else				
				If oEAIObEt:getpropvalue('ProcessingInformation') != nil
					oMsgError := oEAIObEt:getpropvalue('ProcessingInformation'):getpropvalue("ListOfMessages")
					For nX := 1 To Len( oMsgError )
						cMessage := oMsgError[nX]:getpropvalue('Message')
						If cMessage != NiL .AND. ValType(cMessage) == "C"
							cLogErro += cMessage + CRLF
						EndIf
					Next nX
				Endif
	
				lRet := .F.
			EndIf
			If !lRet
				ofwEAIObj:Activate()
				ofwEAIObj:setProp("ReturnContent")
				ofwEAIObj:getPropValue("ReturnContent"):setProp("Error", cLogErro)
			EndIf
			
		//--------------------------------------
	  	//whois
	  	//--------------------------------------
		ElseIf   cTypeMessage == EAI_MESSAGE_WHOIS
			ofwEAIObj := "1.000|2.000|2.001|2.002|2.003|2.005"
		EndIf
		
	//--------------------------------------
	//envio mensagem
	//--------------------------------------
	ElseIf(nTypeTrans == TRANS_SEND)
	   
		If cRotina == "MATA030"
			If(!Inclui .And. !Altera)
				cEvent := "delete"
			EndIf
		Else
			oModel := FwModelActive()
			If oModel:GetOperation() == MODEL_OPERATION_DELETE
				cEvent := 'delete'
			EndIf
		EndIf
	      
		If cEvent == "delete"
			CFGA070Mnt(,"SA1","A1_COD",,IntCliExt(, , SA1->A1_COD, SA1->A1_LOJA)[2],.T.,,,cOwnerMsg)
		EndIf
	     
	  	// Trata endere�o separando Logradouro e N�mero
		cLograd := trataEnd(SA1->A1_END, "L")
		cNumero := trataEnd(SA1->A1_END, "N")
	
	  	// Retorna o codigo do estado a partir da sigla
		cCodEst  := Tms120CdUf(SA1->A1_EST, '1')
	
	  	// Codigo do estado de entrega
		cCodEstE:= Tms120CdUf(SA1->A1_ESTE, '1')
	
	  	// Envio do codigo de acordo com padrao IBGE (cod. estado + cod. municipio)
		If(!Empty(SA1->A1_COD_MUN))
			cCodMun := Rtrim(cCodEst) + Rtrim(SA1->A1_COD_MUN)
		Endif
	
	  	// Codigo do municipio de entrega
		If cPaisLoc $ "ANG|BRA|EQU|HAI|PTG" .And. !Empty(SA1->A1_CODMUNE)
			cCodMunE := Rtrim(cCodEstE)  + Rtrim(SA1->A1_CODMUNE)
		EndIf

		//Montagem da mensagem
		ofwEAIObj:Activate()
		ofwEAIObj:setEvent(cEvent)
		
		ofwEAIObj:setprop("CompanyId", cEmpAnt)	
		ofwEAIObj:setprop("BranchId", cFilAnt)
		ofwEAIObj:setprop("CompanyInternalId", cEmpAnt + '|' + cFilAnt)
		ofwEAIObj:setprop("Code", Rtrim(SA1->A1_COD))
		ofwEAIObj:setprop("StoreId", Rtrim(SA1->A1_LOJA))
		ofwEAIObj:setprop("InternalId", IntCliExt(, , SA1->A1_COD, SA1->A1_LOJA)[2])
		ofwEAIObj:setprop("ShortName", Rtrim(SA1->A1_NREDUZ))
		ofwEAIObj:setprop("Name", Rtrim(SA1->A1_NOME))
		
		//Ajustado para enviar o tipo conforme documentacao API Totvs
		//Identifica se o emitente � apenas Cliente, apenas Fornecedor ou Ambos 
		//1 � Cliente 
		//2 � Fornecedor 
		//3 � Ambos
		ofwEAIObj:setprop("Type", 1 ) //'Customer'

		//Ajustado para enviar o tipo conforme documentacao API Totvs
		//Identifica se o emitente � Pessoa F�sica, Jur�dica, Estrangeiro ou Trading 
		//1 � Pessoa F�sica 
		//2 � Pessoa Jur�dica 
		//3 � Estrangeiro 
		//4 � Trading
		If SA1->A1_PESSOA == 'F'
			ofwEAIObj:setprop("EntityType", 1)
			cCNPJCPF := 'CPF'
		Else
			ofwEAIObj:setprop("EntityType", 2)
			cCNPJCPF := 'CNPJ'
		EndIf
		
		If (!Empty(SA1->A1_DTNASC))
			ofwEAIObj:setprop("RegisterDate", SubStr(DToC(SA1->A1_DTNASC), 7, 4) + '-' + SubStr(DToC(SA1->A1_DTNASC), 4, 2) + '-' + SubStr(DToC(SA1->A1_DTNASC), 1, 2))
		EndIf
	
		If SA1->A1_MSBLQL == '1'
			ofwEAIObj:setprop("RegisterSituation", "Inactive")
		Else
			ofwEAIObj:setprop("RegisterSituation", "Active")
		EndIf

		lPFisica := cPaisLoc == "BRA" .And. !Empty(SA1->A1_PFISICA)
		
		If !Empty(SA1->A1_INSCR) .Or. !Empty(SA1->A1_INSCRM) .Or. !Empty(SA1->A1_CGC) .Or. !Empty(SA1->A1_SUFRAMA);
		 .Or. !Empty(SA1->A1_RG) .Or. lPFisica
			ofwEAIObj:setprop('GovernmentalInformation',{},'Tax',,.T.)
       		ofwEAIObj:get("GovernmentalInformation")[1]:setprop("Name"   	, "INSCRICAO ESTADUAL",,.T.)
	        ofwEAIObj:get("GovernmentalInformation")[1]:setprop("Scope"     , "State",,.T.)
	        ofwEAIObj:get("GovernmentalInformation")[1]:setprop("Id"       	, Rtrim(SA1->A1_INSCR),,.T.)
	        ofwEAIObj:setprop('GovernmentalInformation',{},'Tax',,.T.)
	     	ofwEAIObj:get("GovernmentalInformation")[2]:setprop("Name"   	,"INSCRICAO MUNICIPAL",,.T.)
	        ofwEAIObj:get("GovernmentalInformation")[2]:setprop("Scope"     , "Municipal",,.T.)
	        ofwEAIObj:get("GovernmentalInformation")[2]:setprop("Id"       	, Rtrim(SA1->A1_INSCRM),,.T.)
			ofwEAIObj:setprop('GovernmentalInformation',{},'Tax',,.T.)
	     	ofwEAIObj:get("GovernmentalInformation")[3]:setprop("Name"   	, cCNPJCPF,,.T.)
	        ofwEAIObj:get("GovernmentalInformation")[3]:setprop("Scope"     , "Federal",,.T.)
	        ofwEAIObj:get("GovernmentalInformation")[3]:setprop("Id"       	, Rtrim(SA1->A1_CGC),,.T.)
	        ofwEAIObj:setprop('GovernmentalInformation',{},'Tax',,.T.)	        
		  	ofwEAIObj:get("GovernmentalInformation")[4]:setprop("Name"   	, "SUFRAMA",,.T.)
	        ofwEAIObj:get("GovernmentalInformation")[4]:setprop("Scope"     , "Federal",,.T.)
	        ofwEAIObj:get("GovernmentalInformation")[4]:setprop("Id"       	, Rtrim(SA1->A1_SUFRAMA),,.T.)

			If SA1->A1_PESSOA == 'F' .And. (lPFisica .Or. !Empty(SA1->A1_RG))
				cTipoRG := IIF(SA1->A1_EST == "EX","PASSAPORTE","RG")
				cRG := IIF(lPFisica, SA1->A1_PFISICA, SA1->A1_RG) //Campo A1_RG utilizado apenas no modulo SIGALOJA

				ofwEAIObj:setprop('GovernmentalInformation',{},'Tax',,.T.)
				ofwEAIObj:get("GovernmentalInformation")[5]:setprop("Name"   	, cTipoRG,,.T.)
	        	ofwEAIObj:get("GovernmentalInformation")[5]:setprop("Scope"     , "Federal",,.T.)
	        	ofwEAIObj:get("GovernmentalInformation")[5]:setprop("Id"       	, RTrim(cRG),,.T.)
				
			Endif
		EndIf
		
		oAddress := ofwEAIObj:setprop("Address")
		oAddress:setprop("Address", Rtrim(cLograd) )
		oAddress:setprop("Number", Rtrim(cNumero) )
		oAddress:setprop("Complement", Iif(Empty(SA1->A1_COMPLEM),_NoTags(trataEnd(SA1->A1_END,"C")),_NoTags(Rtrim(SA1->A1_COMPLEM))) )
		If !Empty(cCodMun) .Or. !Empty(SA1->A1_MUN)
			oAddress:setprop("City")
			If !Empty(cCodMun)
				oAddress:getPropValue("City"):setprop("CityCode", cCodMun )
				oAddress:getPropValue("City"):setprop("CityInternalId", cCodMun )
			Else
				oAddress:getPropValue("City"):setprop("CityCode", "" )
				oAddress:getPropValue("City"):setprop("CityInternalId", "" )
			EndIf
			oAddress:getPropValue("City"):setprop("CityDescription", Rtrim(SA1->A1_MUN) )
		EndIf
		oAddress:setprop("District", Rtrim(SA1->A1_BAIRRO) )
		If !Empty(SA1->A1_EST)
			oAddressState := oAddress:setprop("State")
			oAddressState:setprop("StateCode", AllTrim( SA1->A1_EST ) )
			oAddressState:setprop("StateInternalId", AllTrim( SA1->A1_EST ) )
			oAddressState:setprop("StateDescription", Rtrim(Posicione("SX5",1, xFilial("SX5") + "12" + SA1->A1_EST, "X5DESCRI()" )) )			
		EndIf
		If !Empty(SA1->A1_PAIS)
			oAddressCountry := oAddress:setprop("Country")
			oAddressCountry:setprop("CountryCode", SA1->A1_PAIS )
			oAddressCountry:setprop("CountryInternalId", SA1->A1_PAIS )
			oAddressCountry:setprop("CountryDescription", Rtrim(Posicione("SYA",1,xFilial("SYA")+SA1->A1_PAIS,"YA_DESCR")) )
		EndIf
		oAddress:setprop("ZIPCode", Rtrim(SA1->A1_CEP) )
		oAddress:setprop("POBox", Rtrim(SA1->A1_CXPOSTA) )
		
	  	// Endere�o de entrega
		If !Empty(SA1->A1_ENDENT) .Or. !Empty(SA1->A1_MUNE) .Or. !Empty(SA1->A1_BAIRROE) .Or. !Empty(SA1->A1_ESTE) .Or. !Empty(SA1->A1_CEPE)
			oShipAddress := ofwEAIObj:setprop("ShippingAddress")
			oShipAddress:setprop("Address", _NoTags(trataEnd(SA1->A1_ENDENT,"L")) )
			oShipAddress:setprop("Number", trataEnd(SA1->A1_ENDENT,"N") )
			oShipAddress:setprop("Complement", _NoTags(trataEnd(SA1->A1_ENDENT,"C")) )
			If !Empty(cCodMunE) .And. !Empty(SA1->A1_MUNE)
				oShipAddCity := oShipAddress:setprop("City")
				oShipAddCity:setprop("CityCode", cCodMunE )
				oShipAddCity:setprop("CityDescription", Rtrim(SA1->A1_MUNE) )				
			EndIf	
			oShipAddress:setprop("District", Rtrim(SA1->A1_BAIRROE) )
			If !Empty(SA1->A1_ESTE)
				oShipAddState := oShipAddress:setprop("State")
				oShipAddState:setprop("StateCode", Rtrim(SA1->A1_ESTE) )
			EndIf	
			oShipAddress:setprop("ZIPCode", Rtrim(SA1->A1_CEPE) )	
		EndIf

	  	// Formas de contato
		If !Empty(SA1->A1_TEL) .Or. !Empty(SA1->A1_FAX) .Or. !Empty(SA1->A1_HPAGE) .Or. !Empty(SA1->A1_EMAIL)
			If !Empty(SA1->A1_DDI)
				cTel := AllTrim(SA1->A1_DDI)
			EndIf
			
			If !Empty(SA1->A1_DDD)
				If !Empty(cTel)
					cTel += AllTrim(SA1->A1_DDD)
				Else
					cTel := AllTrim(SA1->A1_DDD)
				EndIf
			EndIf
			
			If !Empty(cTel)
				cTel += AllTrim(SA1->A1_TEL)
			Else
				cTel := AllTrim(SA1->A1_TEL)
			EndIf
			
			ofwEAIObj:setprop('ListOfCommunicationInformation',{},'CommunicationInformation',,.T.)
			ofwEAIObj:get("ListOfCommunicationInformation")[1]:setprop("PhoneNumber", cTel,,.T.)
			ofwEAIObj:get("ListOfCommunicationInformation")[1]:setprop("FaxNumber", Rtrim(SA1->A1_FAX),,.T.)
			ofwEAIObj:get("ListOfCommunicationInformation")[1]:setprop("HomePage", _NoTags(Rtrim(SA1->A1_HPAGE)),,.T.)
			ofwEAIObj:get("ListOfCommunicationInformation")[1]:setprop("Email", _NoTags(Rtrim(SA1->A1_EMAIL)),,.T.)
		EndIf
		
		// Contato
		If !Empty(SA1->A1_CONTATO)
			ofwEAIObj:setprop('ListOfContacts',{},'Contact',,.T.)
			ofwEAIObj:get("ListOfContacts")[1]:setprop("ContactInformationName", _NoTags(Rtrim(SA1->A1_CONTATO)),,.T.)
		EndIf

	  	// Endere�o de cobran�a
		If !Empty(SA1->A1_ENDCOB) .Or. !Empty(SA1->A1_MUNC) .Or. !Empty(SA1->A1_BAIRROC) .Or. !Empty(SA1->A1_ESTC) .Or. !Empty(SA1->A1_CEPC)
			oBillInfor := ofwEAIObj:setprop("BillingInformation")
			oBillInfor := oBillInfor:setprop("Address")
			oBillInfor:setprop("Address", _NoTags(trataEnd(SA1->A1_ENDCOB,"L")) )
			oBillInfor:setprop("Number", trataEnd(SA1->A1_ENDCOB,"N") )
			oBillInfor:setprop("Complement", _NoTags(trataEnd(SA1->A1_ENDCOB,"C")) )
			oBillInfor:setprop("District", _NoTags(Rtrim(SA1->A1_BAIRROC)) )
			oBillInfor:setprop("ZIPCode", Rtrim(SA1->A1_CEPC) )
			oBillInfor:setprop("City")
			oBillInfor:getPropValue("City"):setprop("CityDescription", _NoTags(Rtrim(SA1->A1_MUNC)) )
			oBillState := oBillInfor:setprop("State")
			oBillState:setprop("StateCode", SA1->A1_ESTC )
		EndIf
		
		// Vendedor
		If !Empty(SA1->A1_VEND)
			oVedInf := ofwEAIObj:setprop("VendorInformation")
			oVedInf := oVedInf:setprop("VendorType")
			oVedInf:setprop("Code", SA1->A1_VEND )
		EndIf
		
	   	// Limite de Cr�dito
		If !Empty(SA1->A1_LC)
			ofwEAIObj:setprop("CreditInformation")
			ofwEAIObj:getPropValue("CreditInformation"):setprop("CreditLimit", SA1->A1_LC )
		EndIf
		
	EndIf
	
	
	aRet := { lRet, ofwEAIObj, cOwnerMsg } 

	aSize(aCliente,0 )
	aCliente := {}
	
	aSize(aAux,0)
	aAux := {}

	aSize(aAreaCCH,0)
	aAreaCCH := {}

Return aRet 

//-------------------------------------------------------------------
/*/{Protheus.doc} IntCliExt
Monta o InternalID do Cliente de acordo com o c�digo passado
no par�metro.

@param   cEmpresa   C�digo da empresa (Default cEmpAnt)
@param   cFil       C�digo da Filial (Default cFilAnt)
@param   cCliente   C�digo do Cliente
@param   cLoja      C�digo da Loja do Cliente

@author  Totvs Cascavel
@version P12
@since   23/05/2018
@return  aResult Array contendo no primeiro par�metro uma vari�vel
         l�gica indicando se o registro foi encontrado.
         No segundo par�metro uma vari�vel string com o InternalID
         montado.

@sample  IntCliExt(, , '00001', '01') ir� retornar {.T., '01|01|00001|01|C'}
/*/
//-------------------------------------------------------------------
Static Function IntCliExt(cEmpresa, cFil, cCliente, cLoja )

   	Local   aResult  := {}
   	
   	Default cEmpresa 	:= cEmpAnt
   	Default cFil     	:= xFilial('SA1')
	Default cCliente	:= ""
	Default cLoja		:= ""

	aAdd(aResult, .T.)
	aAdd(aResult, cEmpresa + '|' + RTrim(cFil) + '|' + RTrim(cCliente) + '|' + RTrim(cLoja) + '|C')

Return aResult

//-------------------------------------------------------------------
/*/{Protheus.doc} IntCliInt
Recebe um InternalID e retorna o c�digo do Cliente.

@param   cInternalID InternalID recebido na mensagem.
@param   cRefer      Produto que enviou a mensagem
@param   cVersao     Vers�o da mensagem �nica (Default 2.000)

@author  Leandro Luiz da Cruz
@version P11
@since   08/02/2013
@return  aResult Array contendo no primeiro par�metro uma vari�vel
         l�gica indicando se o registro foi encontrado no de/para.
         No segundo par�metro uma vari�vel array com a empresa,
         filial, o c�digo do cliente e a loja do cliente.

@sample  IntLocInt('01|01|00001|01') ir� retornar
{.T., {'01', '01', '00001', '01', 'C'}}
/*/
//-------------------------------------------------------------------
Static Function IntCliInt(cInternalID, cRefer)

   	Local   aResult  := {}
   	Local   aTemp    := {}
   	Local   cTemp    := ''
   	Local   cAlias   := 'SA1'
   	Local   cField   := 'A1_COD'

	Default cInternalID	:= ''
	Default cRefer		:= ''

	cTemp := CFGA070Int(cRefer, cAlias, cField, cInternalID)
	
  	If !Empty( cTemp )  
		aAdd(aResult, .T.)
	 	aTemp := Separa(cTemp, '|')
	 	aAdd(aResult, {})
	 	aResult[Len(aResult)] := aClone( aTemp )
	Endif

	aSize(aTemp, 0)
	aTemp := {}
	
Return aResult

//-------------------------------------------------------------------
/*/{Protheus.doc} A2030PAIS()
Array com codigo dos pais utilizados na TOTVS

@author  Rodrigo Machado Pontes
@version P11
@since   14/10/2015
@return  aArray - Array retornado conforme dicionario  de dados
/*/
//-------------------------------------------------------------------
Static Function A2030PAIS(cAliasPais)
	Local aAreaSYA	:= SYA->(GetArea())
	Local cCdPais	:= ""
	Local cCpo		:= Iif(cAliasPais=="SA1","A1_PAIS","A2_PAIS")
	Local cFilSYA	:= xFilial("SYA") 
	Local nI		:= 0
	Local aRet		:= {	{"BRASIL"					,"BRA","0"},;
						{"ANGOLA"					,"ANG","0"},;
						{"ARGENTINA"				,"ARG","0"},;
						{"BOLIVIA"					,"BOL","0"},;
						{"CHILE"					,"CHI","0"},;
						{"COLOMBIA"				    ,"COL","0"},;
						{"COSTA RICA"				,"COS","0"},;
						{"REPUBLICA DOMINICANA"	    ,"DOM","0"},;
						{"EQUADOR"					,"EQU","0"},;
						{"ESTADOS UNIDOS"			,"EUA","0"},;
						{"MEXICO"					,"MEX","0"},;
						{"PARAGUAI"				    ,"PAR","0"},;
						{"PERU"					    ,"PER","0"},;
						{"PORTUGAL"				    ,"PTG","0"},;
						{"URUGUAI"					,"URU","0"},;
						{"VENEZUELA"				,"VEN","0"}}
	Default cAliasPais := ''

	For nI := 1 To Len(aRet)
	cCdPais	:= ""
		cCdPais	:= PadR(Posicione("SYA",2,cFilSYA + PadR(aRet[nI,1],TamSx3("YA_DESCR")[1]),"YA_CODGI"),TamSx3(cCpo)[1])
	
	If !Empty(cCdPais)
		aRet[nI,3] := cCdPais
	Endif
	Next nI

	RestArea(aAreaSYA)
	aSize(aAreaSYA, 0)
	aAreaSYA := {}

Return aRet

//-------------------------------------------------------------------
/*/{Protheus.doc} A2030PALOC()
Busca o codigo do pais atraves do cPaisLoc

@author  Rodrigo Machado Pontes
@version P11
@since   14/10/2015
@return  cCdPaisLoc - codigo do pais atraves do cPaisLoc
/*/
//-------------------------------------------------------------------
Static Function A2030PALOC(cAliasPais,nOpc)

	Local aPais			:= A2030PAIS(cAliasPais)
	Local nPos			:= 0
	Local cCdPaisLoc	:= ""

	Default cAliasPais	:= ''
	Default nOpc		:= 0

	If nOpc == 1 //Busca o Codigo do Pais
		nPos := aScan(aPais,{|x| AllTrim(x[2]) == AllTrim(Upper(cPaisLoc))})
		If nPos > 0
			cCdPaisLoc := aPais[nPos,3]
		Endif
	Elseif nOpc == 2 //Busca o Nome do Pais
		nPos := aScan(aPais,{|x| AllTrim(x[2]) == AllTrim(Upper(cPaisLoc))})
		If nPos > 0
			cCdPaisLoc := aPais[nPos,1]
		Endif
	Endif	
	aSize(aPais,0)

Return cCdPaisLoc

//-------------------------------------------------------------------
/*/{Protheus.doc} ProxNum
Rotina para retornar o Proximo numero para grava��o

@return cRet, C�digo sequ�ncial v�lido

@author Pedro Alencar
@since 29/12/2015
@version 12.1.9
/*/
//-------------------------------------------------------------------
Static Function ProxNum()
	Local aAreaSA1 := {}
	Local cRet := ""
	Local lLivre := .F.
	Local cFilSA1	:= FWxFilial("SA1")
	
	aAreaSA1 := SA1->( GetArea() )
	SA1->( dbSetOrder( 1 ) )
	cRet := GetSxeNum( "SA1", "A1_COD" )
	
	While !lLivre
		If SA1->( msSeek( cFilSA1  + cRet ) )
			ConfirmSX8()
			cRet := GetSxeNum( "SA1", "A1_COD" )
		Else
			lLivre := .T.
		Endif
	Enddo
	
	RestArea( aAreaSA1 )
	aSize(aAreaSA1, 0)
	aAreaSA1 := {}

Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} MATI030Num
Recebe conte�do da tag <CODE> e ajusta para o padr�o protheus caso
esteja fora do padr�o. 

@param   cCode      ,String, Conte�do da tag <CODE>
@param   lGetXnum   ,L�gic,  Indica se foi utilizado Getsx8Num
@author  Squad CRM/Faturamento
@version P12
@since   18/04/2018
@return  cCode      ,String, Conte�do da tag <CODE>
@sample  MATI030Num('34563597874', lGetxnum) => '345635' ou o proximo n�mero livre
/*/
//-------------------------------------------------------------------
Static Function MATI030Num(cCode, lGetXnum)
    Local nLengA1Cod    := TamSX3('A1_COD')[1]
    Local aAreaCli      := {}
    Default cCode       := ''
    Default lGetXnum	:= .F. 
    
    IF !Empty(cCode) .And. Len(cCode) > nLengA1Cod
        aAreaCli := SA1->( GetArea() )
        SA1->(dbSetOrder(1))
        
        cCode := Substr( cCode, 1, nLengA1Cod )
        
        If  SA1->(MsSeek(xFilial('SA1') + cCode))
            cCode := ProxNum()
            lGetXnum := .T.
        EndIf 

        SA1->( RestArea( aAreaCli ) )
        Asize(aAreaCli,0)
		aAreaCli := {}
    EndIf

Return cCode

//-------------------------------------------------------------------
/*/{Protheus.doc} MATI30Pais
Tradutor de c�digo de pa�s, para casos onde o c�digo enviado seja diferente
dos encontrados na SYA

@param   cPaisCode      ,String, C�digo do pa�s enviado ao adapter
@param   cPais          ,String, Nome do pa�s enviado ao adaoter
@param   cMarca         ,String, Marca do Adapter para pesquisa no De/Para
@retur   cRet           ,String, C�digo do Pa�s na tabela SYA
@author  Squad CRM/Faturamento
@version P12
@since   11/05/2018
@sample  MATI30Pais('001', 'BRASIL') => '105'
/*/
//-------------------------------------------------------------------
Static Function MATI30Pais(cPaisCode, cPais, cMarca)
    Local aAreaSYA    := {}
    Local cFilSYA     := ''
    Local cRet        := '' 
    Local lHasCode    := !Empty(cPaisCode)
    Local lHasDesc    := !Empty(cPais)
    
    Default cPaisCode := ''
    Default cPais     := ''
    Default	cMarca		:= ''
    
    If lHasCode
        cRet := CFGA070Int(cMarca, 'SYA', 'YA_CODGI', cPaisCode)
    EndIf

    If !Empty(cRet)
        cRet := Alltrim( cRet )
    Else
        aAreaSYA := SYA->( GetArea() )
        cFilSYA := xFilial("SYA")
        
        If lHasCode
            cRet := AllTrim(Posicione("SYA",1,cFilSYA+cPaisCode,"YA_CODGI"))
        EndIf

        If Empty(cRet) .And. lHasDesc
            cRet := AllTrim(Posicione("SYA",2,cFilSYA+ Padr(cPais, GetSX3Cache("YA_DESCR","X3_TAMANHO") ),"YA_CODGI"))
        EndIf
        RestArea(aAreaSYA)
        Asize(aAreaSYA,0)   
		aAreaSYA := {}   
    EndIf
Return cRet