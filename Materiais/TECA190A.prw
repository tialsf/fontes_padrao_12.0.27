#INCLUDE 'TECA190A.CH'
#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'FWBROWSE.CH'
 
#DEFINE FIL_CLIENTE 1
#DEFINE FIL_LOCAL   2
#DEFINE FIL_REGIAO  3
#DEFINE FIL_CONTRAT 4
#DEFINE FIL_SUPERV  5

Static MsgRun1 := STR0001 // 'Filtrando dados...'
Static MsgRun2 := STR0002 // 'Aguarde'

Static dDtLocIni  := dDataBase - 15  // utilizado como data de in�cio do per�odo para consulta dos postos do contrato (TFL)
Static dDtLocFim  := dDataBase + 15  // utilizado como data de fim do per�odo para consulta dos postos do contrato (TFL)

Static dDtRhRef   := dDataBase  // refer�ncia para listagem das agendas dos atendentes
Static oDefRhSit  := {}  // objeto com a lista de tipos para marca��o e filtro da agendas

//------------------------------------------------------------------------------
/*/{Protheus.doc} TECA190A
	Rotina para mesa operacional - chama a rotina que constr�i com mensagem para o usu�rio
aguardar 

@sample 	TECA190A

@since		29/04/2014
@version	P12

/*/
//------------------------------------------------------------------------------
Function TECA190A()

If FindFunction("At190DAvis") //Nova mesa - teca190d
	At190DAvis()
Endif

MsgRun(MsgRun1,MsgRun2, {|| TECA190AT()})  // mensagem para orienta��o aos usu�rios

Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} TECA190AT
	Constr�i a interface da mesa operacional para a vis�o de contratos 

@sample 	TECA190AT

@since		29/04/2014
@version	P12

/*/
//------------------------------------------------------------------------------
Function TECA190AT()

Local aCoors     := FwGetDialogSize(oMainWnd)
Local oDlgMesCtr := Nil
Local oFWLayer1  := Nil

Local oDlgButts  := Nil
Local oPanButts  := Nil
Local oPanLocais := Nil
Local oPanItens  := Nil

Local oDlgRh     := Nil
Local oFWLayerRh := Nil
Local oPanRh     := Nil
Local oPanSitRh  := Nil
Local oPanDataRh := Nil
Local oDlgBtRh   := Nil

Local oDlgMi     := Nil
Local oFWLayerMi := Nil
Local oPanMi     := Nil
Local oPanDataMi := Nil

Local oDlgMc     := Nil
Local oFWLayerMc := Nil
Local oPanMc     := Nil
Local oPanDataMc := Nil

Local oFolder    := Nil

Local oBrwLocais := Nil
Local oBrwRh     := Nil
Local oBrwMc     := Nil
Local oBrwMi     := Nil
Local oBrwItRh   := Nil
Local oBrwItMc   := Nil
Local oBrwItMi   := Nil

Local oGroup     := Nil
Local oDtLocIni  := Nil
Local oSayLocIni := Nil
Local oDtLocFim  := Nil
Local oSayLocFim := Nil

Local oDtRhRef   := Nil
Local oSayRhRef  := Nil

Local xAux       := Nil
Local nK         := 1

dDtLocIni  := dDataBase - 15  // por padr�o define um intervalo de 30 dias 
dDtLocFim  := dDataBase + 15  // para exibi��o dos dados

//-------------------------------------------------------
//  Vari�veis que receber�o a marca��o dos tipos de aloca��o
// necess�rio ser private para que os componentes de CheckBox consigam 
// fazer a atualiza��o de valor
Private aDefRhSit  := At190GetTpAloc()
dDtRhRef   := dDataBase
oDefRhSit  := Array(Len(aDefRhSit))

DEFINE MSDIALOG oDlgMesCtr TITLE STR0003 FROM aCoors[1],aCoors[2] TO iIf(aCoors[3] < 700, aCoors[3]+600,aCoors[3]+200) ,aCoors[4] PIXEL  // "Mesa Operacional - Contratos"

//------------------------------------------
// Divide a tela e organiza os layers a serem apresentados
oFWLayer1 := FWLayer():New()
oFWLayer1:Init(oDlgMesCtr,.F.,.T.)

oFWLayer1:AddLine("SUP",30,.F.)
oFWLayer1:AddLine("INF",70,.F.)

oFWLayer1:AddColumn("FILTROS",20,.T.,"SUP")
oFWLayer1:AddColumn("LOCAIS" ,80,.T.,"SUP")

oFWLayer1:AddColumn("ITENS",100,.T.,"INF")

//-----------------------------------------------
//  Adiciona os pain�is que conter�o os objetos
// nas linhas principais da interface 
oFWLayer1:AddWindow("FILTROS","oPanButts","",100,.F.,.T.,,"SUP",{|| })
oPanButts := oFWLayer1:GetWinPanel("FILTROS","oPanButts","SUP")

oFWLayer1:AddWindow("LOCAIS","oPanLocais","",100,.F.,.T.,,"SUP",{|| })
oPanLocais := oFWLayer1:GetWinPanel("LOCAIS","oPanLocais","SUP")

oFWLayer1:AddWindow("ITENS","oPanItens","",100,.F.,.T.,,"INF",{|| })
oPanItens := oFWLayer1:GetWinPanel("ITENS","oPanItens","INF")

//_________ fim da constru��o da parte superior da janela

//-----------------------------------
// Cria folder para conter as informa��es dos itens vendidos no local 
oFolder := TFolder():New(001,001,{STR0004,STR0005,STR0006},{"HEADER1","HEADER2","HEADER3"},oPanItens,,,, .T., .F.,; // "Rec.Humanos" ### "Mat.Implanta��o" ### "Mat.Consumo" 
			(oPanItens:nClientWidth*0.5),(oPanItens:nClientHeight*0.4))

oDlgRh := oFolder:aDialogs[1]
oDlgMi := oFolder:aDialogs[2]
oDlgMc := oFolder:aDialogs[3]
 

//______________ constr�i o layer para a aba de rh
oFWLayerRh := FwLayer():New()
oFWLayerRh:Init(oDlgRh,.F.,.T.)

oFWLayerRh:AddLine("ITEM_SUPRH",50,.F.)
oFWLayerRh:AddLine("ITEM_INFRH",50,.F.)

oFWLayerRh:AddColumn("ITEM_RH",100,.T.,"ITEM_SUPRH")

oFWLayerRh:AddColumn("SITRH",15,.T.,"ITEM_INFRH")
oFWLayerRh:AddColumn("DATRH" ,85,.T.,"ITEM_INFRH")

oFWLayerRh:AddWindow("ITEM_RH","oPanRh","",100,.F.,.T.,,"ITEM_SUPRH",{|| })
oPanRh := oFWLayerRh:GetWinPanel("ITEM_RH","oPanRh","ITEM_SUPRH")

oFWLayerRh:AddWindow("SITRH","oPanSitRh","",100,.F.,.T.,,"ITEM_INFRH",{|| })
oPanSitRh := oFWLayerRh:GetWinPanel("SITRH","oPanSitRh","ITEM_INFRH")

oFWLayerRh:AddWindow("DATRH","oPanDataRh","",100,.F.,.T.,,"ITEM_INFRH",{|| })
oPanDataRh := oFWLayerRh:GetWinPanel("DATRH","oPanDataRh","ITEM_INFRH")

oDlgBtRh := TScrollBox():New(oPanSitRh,00,00,(oPanSitRh:nClientHeight*0.52),(oPanSitRh:nClientWidth*0.5),.T.,.F.,.F.)

//___________ Status e Filtros
@ 001,020 BUTTON STR0007 SIZE 35,10 OF oDlgBtRh PIXEL Action( ; // "Filtrar"
			MsgRun(MsgRun1,MsgRun2,{||At190ARefBrw((oBrwRh:cAlias)->TFF_COD,{{oBrwItRh,'ABB'}}) } ) )

oSayRhRef := TSay():New( 018, 005, { || STR0008 }, oDlgBtRh,,,,,,.T.,,, 050, 10 ) // 'Data' 
oDtRhRef  := TGet():New( 016, 020, { |u| If( PCount() > 0, dDtRhRef := u, dDtRhRef ) }, oDlgBtRh, 050, 010,,,,,,,,.T.,,,,,,,,,,"dDtRhRef" )

//----------------------------------------------------------
//  Cria os objetos para checkbox que ter�o a fun��o de filtrar os tipos de agenda
For nK := 1 To Len(aDefRhSit)
	oDefRhSit[nK] := TCheckBox():New(035+((nK-1)*10),005,aDefRhSit[nK,3] + " - " + aDefRhSit[nK,2],Nil,oDlgBtRh,060,010,,,,,,,,.T.,,,)
	xAux := cValToChar(nK)
	oDefRhSit[nK]:bSetGet := &('{|u|If( PCount()>0, aDefRhSit['+xAux+',1]:=u, aDefRhSit['+xAux+',1])}')
Next nK
nK -= 1

xAux := Nil
//______________ constr�i o layer para a aba de mat. implanta��o
oFWLayerMi := FwLayer():New()
oFWLayerMi:Init(oDlgMi,.F.,.T.)

oFWLayerMi:AddLine("ITEM_SUPMI",50,.F.)
oFWLayerMi:AddLine("ITEM_INFMI",50,.F.)

oFWLayerMi:AddColumn("ITEM_MI",100,.T.,"ITEM_SUPMI")

oFWLayerMi:AddColumn("DATMI" ,100,.T.,"ITEM_INFMI")

oFWLayerMi:AddWindow("ITEM_MI","oPanMi","",100,.F.,.T.,,"ITEM_SUPMI",{|| })
oPanMi := oFWLayerMi:GetWinPanel("ITEM_MI","oPanMi","ITEM_SUPMI")

oFWLayerMi:AddWindow("DATMI","oPanDataMi","",100,.F.,.T.,,"ITEM_INFMI",{|| })
oPanDataMi := oFWLayerMi:GetWinPanel("DATMI","oPanDataMi","ITEM_INFMI")

//______________ constr�i o layer para a aba de mat. consumo
oFWLayerMc := FwLayer():New()
oFWLayerMc:Init(oDlgMc,.F.,.T.)

oFWLayerMc:AddLine("ITEM_SUPMC",50,.F.)
oFWLayerMc:AddLine("ITEM_INFMC",50,.F.)

oFWLayerMc:AddColumn("ITEM_MC",100,.T.,"ITEM_SUPMC")

oFWLayerMc:AddColumn("DATMC" ,100,.T.,"ITEM_INFMC")

oFWLayerMc:AddWindow("ITEM_MC","oPanMc","",100,.F.,.T.,,"ITEM_SUPMC",{|| })
oPanMc := oFWLayerMc:GetWinPanel("ITEM_MC","oPanMc","ITEM_SUPMC")

oFWLayerMc:AddWindow("DATMC","oPanDataMc","",100,.F.,.T.,,"ITEM_INFMC",{|| })
oPanDataMc := oFWLayerMc:GetWinPanel("DATMC","oPanDataMc","ITEM_INFMC")

oFWLayer1:Show()

//_______ Panel dos filtro __________

oDlgButts := TScrollBox():New(oPanButts,00,00,(oPanButts:nClientHeight*0.52),(oPanButts:nClientWidth*0.5),.T.,.F.,.F.)

@ 005,005 BUTTON STR0007 SIZE 35,10 OF oDlgButts PIXEL Action( ; // "Filtrar"
			If( At190Valid("TUDOOK"),;
			    MsgRun(MsgRun1,MsgRun2,{||At190ACrgDt(oBrwLocais,{ oBrwRh, oBrwMi, oBrwMc, oBrwItRh, oBrwItMi, oBrwItMc })} ),;
			   .F. ) )

oGroup := TGroup():New( 020,005,060,100,STR0008,oDlgButts,,,.T.)  // 'Per�odo de consulta'

oSayLocIni := TSay():New( 032, 015, {|| STR0009},                                      oGroup, , , , , , .T., , , 050, 10 )  // 'Inicial'
oDtLocIni  := TGet():New( 030, 035, {|u| If(PCount() > 0, dDtLocIni := u, dDtLocIni)}, oGroup, 050, 010, "", {|| At190Valid("DTINI")}, 0, , , .F., , .T., , .F., , .F., .F., , .F., .F., , 'dDtLocIni', , , , )
oSayLocFim := TSay():New( 045, 015, {|| STR0010},                                      oGroup, , , , , , .T., , , 050, 10 )  // 'Final'
oDtLocFim  := TGet():New( 045, 035, {|u| If(PCount() > 0, dDtLocFim := u, dDtLocFim)}, oGroup, 050, 010, "", {|| At190Valid("DTFIM")}, 0, , , .F., , .T., , .F., , .F., .F., , .F., .F., , 'dDtLocFim', , , , )

@ 065,005 BUTTON STR0011 SIZE 35,10 OF oDlgButts PIXEL Action(At190AFil(FIL_CLIENTE))  // "Cliente"
@ 065,055 BUTTON STR0012 SIZE 35,10 OF oDlgButts PIXEL Action(At190AFil(FIL_CONTRAT))  // "Contrato"

@ 080,005 BUTTON STR0013 SIZE 35,10 OF oDlgButts PIXEL Action(At190AFil(FIL_REGIAO))  // "Regi�o"
@ 080,055 BUTTON STR0014 SIZE 35,10 OF oDlgButts PIXEL Action(At190AFil(FIL_LOCAL))  // "Local"

@ 095,005 BUTTON STR0052 SIZE 35,10 OF oDlgButts PIXEL Action(At190AFil(FIL_SUPERV))  // "�rea de Sup."

//___________________________________

//_______ Cria��o dos Browses __________
//
xAux := {}
aAdd( xAux, { {|| At190ASLA1( oBrwLocais:cAlias ) }, {|| AT190ALegen('LOCGER') }, STR0017 } )  // 'Geral'
aAdd( xAux, { {|| At190ASLA2( oBrwLocais:cAlias ) }, {|| AT190ALegen('LOCRH' ) }, 'RH'    } )
aAdd( xAux, { {|| At190ASLA3( oBrwLocais:cAlias ) }, {|| AT190ALegen('LOCMI' ) }, 'MI'    } )
aAdd( xAux, { {|| At190ASLA4( oBrwLocais:cAlias ) }, {|| AT190ALegen('LOCMC' ) }, 'MC'    } )
oBrwLocais := At190ABrwNew( "190ATFL", At190ASqlBrw('TFL')                                , '', oPanLocais, At190AGFields('TFL'), .F., xAux)

xAux := {}
aAdd( xAux, { {|| At190ASRh( oBrwRh:cAlias ) }, {|| AT190ALegen('ITRH') }, '' } )
oBrwRh     := At190ABrwNew( "190ATFF", At190ASqlBrw('TFF',(oBrwLocais:cAlias)->TFL_CODIGO), '',     oPanRh, At190AGFields('TFF'), .F., xAux)

xAux := {}
aAdd( xAux, { {|| At190ASAg1( oBrwItRh:cAlias ) }, {|| AT190ALegen('AGENDA1') }, STR0015   } )  // 'Agenda'
aAdd( xAux, { {|| At190ASAg2( oBrwItRh:cAlias ) }, {|| AT190ALegen('AGENDA2') }, STR0016 } )  // 'Aloca��o'
oBrwItRh   := At190ABrwNew( "190AABB", At190ASqlBrw('ABB',(oBrwRh:cAlias)->TFF_COD)       , '', oPanDataRh, At190AGFields('ABB'), .F., xAux)

xAux := {}
aAdd( xAux, { {|| At190ASMi( oBrwMi:cAlias ) }, {|| AT190ALegen('ITMI') }, '' } )
oBrwMi     := At190ABrwNew( "190ATFG", At190ASqlBrw('TFG',(oBrwLocais:cAlias)->TFL_CODIGO), '',     oPanMi, At190AGFields('TFG'), .F., xAux)

xAux := {}
oBrwItMi   := At190ABrwNew( "190ATFS", At190ASqlBrw('TFS',(oBrwMi:cAlias)->TFG_COD)       , '', oPanDataMi, At190AGFields('TFS'), .F., xAux)

xAux := {}
aAdd( xAux, { {|| At190ASMc( oBrwMc:cAlias ) }, {|| AT190ALegen('ITMC') }, '' } )
oBrwMc     := At190ABrwNew( "190ATFH", At190ASqlBrw('TFH',(oBrwLocais:cAlias)->TFL_CODIGO), '',     oPanMc, At190AGFields('TFH'), .F., xAux)

xAux := {}
oBrwItMc   := At190ABrwNew( "190ATFT", At190ASqlBrw('TFT',(oBrwMc:cAlias)->TFH_COD)       , '', oPanDataMc, At190AGFields('TFT'), .F., xAux)

oBrwLocais:bChange := {|| MsgRun( MsgRun1 + STR0056+(oBrwLocais:cAlias)->TFL_CONTRT,MsgRun2, {|| At190ARefBrw((oBrwLocais:cAlias)->TFL_CODIGO,{{oBrwRh,'TFF'},{oBrwMi,'TFG'},{oBrwMc,'TFH'}} ) } ) } // "Contrato: "
oBrwRh:bChange := {|| MsgRun( MsgRun1+STR0057+(oBrwRh:cAlias)->TFF_COD +"-"+ RTrim((oBrwRh:cAlias)->TFF_PRODUT) +"/"+ RTrim((oBrwRh:cAlias)->TFF_DESCRI),MsgRun2, {||At190ARefBrw((oBrwRh:cAlias)->TFF_COD,{{oBrwItRh,'ABB'}} ) } ) }  // "Item: "
oBrwMi:bChange := {|| MsgRun( MsgRun1+STR0057+(oBrwMi:cAlias)->TFG_COD +"-"+ RTrim((oBrwMi:cAlias)->TFG_PRODUT) +"/"+ RTrim((oBrwMi:cAlias)->TFG_DESCRI),MsgRun2, {||At190ARefBrw((oBrwMi:cAlias)->TFG_COD,{{oBrwItMi,'TFS'}} ) } ) }  // "Item: "
oBrwMc:bChange := {|| MsgRun( MsgRun1+STR0057+(oBrwMc:cAlias)->TFH_COD +"-"+ RTrim((oBrwMc:cAlias)->TFH_PRODUT) +"/"+ RTrim((oBrwMc:cAlias)->TFH_DESCRI),MsgRun2, {||At190ARefBrw((oBrwMc:cAlias)->TFH_COD,{{oBrwItMc,'TFT'}} ) } ) }  // "Item: "

//------------------------------------------------------------
//  Adiciona as op��es de menu aos browses

oBrwLocais:AddButton( STR0026, {|| At190LocVi((oBrwLocais:cAlias)->TFL_LOCAL) } )  // 'Local de Atendimento'
oBrwLocais:AddButton( STR0019, {|| TECA440("TIT->TIT_CODABS='"+(oBrwLocais:cAlias)->TFL_LOCAL+"'") } )  // 'Disciplinas'
oBrwLocais:AddButton( STR0020, {|| TECA490("TIW->TIW_NLOCAL='"+(oBrwLocais:cAlias)->TFL_LOCAL+"'") } )  // 'Investiga��es'
oBrwLocais:AddButton( STR0021, {|| TECA750("TE4->TE4_LOCAL='"+(oBrwLocais:cAlias)->TFL_LOCAL+"'")} )  // 'Ocorr�ncias'
oBrwLocais:AddButton( STR0022, {|| At870GerOrc((oBrwLocais:cAlias)->TFL_CODPAI) } )  // 'Servi�o Extra'
oBrwLocais:AddButton( STR0023, {|| TECA880("TFQ->TFQ_ENTDES='1' .And. TFQ->TFQ_DESTIN='"+(oBrwLocais:cAlias)->TFL_LOCAL+"'") } ) // 'Hist�rico Armamentos'
oBrwLocais:AddButton( STR0024, {|| At190LocGeo( (oBrwLocais:cAlias)->TFL_LOCAL ) } )  // 'Localiza��o'
oBrwLocais:AddButton( STR0018, {|| At190Monit((oBrwLocais:cAlias)->TFL_LOCAL)})  // 'Monitor Check-In'
oBrwLocais:AddButton( STR0025, {|| At190MatAp((oBrwLocais:cAlias)->TFL_CODIGO) } )  // 'Apontamento Materiais'
oBrwLocais:AddButton( STR0053, {|| atTECA012A((oBrwLocais:cAlias)->TFL_LOCAL) } )  // "Restri��es por cliente"
oBrwLocais:AddButton( STR0054, {|| atTECA012B((oBrwLocais:cAlias)->TFL_LOCAL) } )  // "Restri��es por local de atendimento" 


oBrwItRh:AddButton( STR0027, {||At190BMnExec( (oBrwItRh:cAlias)->ABB_CODIGO ), Eval(oBrwRh:bChange) } )  // 'Manuten��o da Agenda'

//------------------------------------------------------------
//  Ativa todos os browse a serem exibidos, realiza em ordem inversa de depend�ncia
// pois com a ativa��o, o bloco de c�digo bChange � executado e caso um browse que devesse receber atualiza��o
// ainda n�o estivesse ativo, geraria error.log
oBrwItMc:Activate()
oBrwMc:Activate()
oBrwItMi:Activate()
oBrwMi:Activate()
oBrwItRh:Activate()
oBrwRh:Activate()
oBrwLocais:Activate()

//___________________________________

ACTIVATE MSDIALOG oDlgMesCtr

//--------------------------------------
//  Desativa os browses e elimina o conte�do das vari�veis de interface
// Isso � feito para que a fun��o DelClassIntF consiga eliminar
// a reserva das vari�veis de mem�ria
oBrwItMc:DeActivate()
oBrwMc:DeActivate()
oBrwItMi:DeActivate()
oBrwMi:DeActivate()
oBrwItRh:DeActivate()
oBrwRh:DeActivate()
oBrwLocais:DeActivate()

oDlgMesCtr := Nil
oFWLayer1  := Nil
oDlgButts  := Nil
oPanButts  := Nil
oPanLocais := Nil
oPanItens  := Nil
oDlgRh     := Nil
oFWLayerRh := Nil
oPanRh     := Nil
oPanSitRh  := Nil
oPanDataRh := Nil
oDlgBtRh   := Nil
oDlgMi     := Nil
oFWLayerMi := Nil
oPanMi     := Nil
oPanDataMi := Nil
oDlgMc     := Nil
oFWLayerMc := Nil
oPanMc     := Nil
oPanDataMc := Nil
oFolder    := Nil
oBrwLocais := Nil
oBrwRh     := Nil
oBrwMc     := Nil
oBrwMi     := Nil
oBrwItRh   := Nil
oBrwItMc   := Nil
oBrwItMi   := Nil
oGroup     := Nil
oDtLocIni  := Nil
oSayLocIni := Nil
oDtLocFim  := Nil
oSayLocFim := Nil
oDtRhRef   := Nil
oSayRhRef  := Nil

// chama a rotina que faz a limpeza dos componentes como Nil
DelClassIntf() 

Return


//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At190Valid()
@description	Valida��o das informa��es da rotina
@sample		At190Valid( cTpVld )
@param			cTpVld - Caracter
							'DTINI' = Valida��o da data inicial
							'DTFIM' = Valida��o da data final
							'TUDOOK'= Valida��o geral
@since			23/12/2016
@version	 	P12
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function At190Valid( cTpVld )

Local lRet	:= .T.
Local cMsg	:= ""

Do	Case
	Case cTpVld == "DTINI"
		If	! Empty(dDtLocFim) .AND. dDtLocIni > dDtLocFim
			lRet	:= .F.
			cMsg	:= STR0058	//"Data inicial deve ser anterior ou igual � data final."
		EndIf
	Case cTpVld == "DTFIM"
		If	! Empty(dDtLocIni) .AND. dDtLocFim < dDtLocIni
			lRet	:= .F.
			cMsg	:= STR0059	//"Data final deve ser posterior ou igual � data inicial."
		EndIf
	Case cTpVld == "TUDOOK"
		If	Empty(dDtLocIni)
			lRet	:= .F.
			cMsg	:= STR0060	//"Informe uma data inicial v�lida."
		ElseIf Empty(dDtLocFim)
			lRet	:= .F.
			cMsg	:= STR0061	//"Informe uma data final v�lida."
		ElseIf dDtLocIni > dDtLocFim
			lRet	:= .F.
			cMsg	:= STR0062	//"Informe um per�odo de datas v�lido."
		EndIf
EndCase

If ! lRet
	MsgStop(cMsg)
EndIf
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At190AFil
	Cria o array no formato correto para execu��o do filtro da mesa

@sample		At190AFil( 1 )

@since		24/04/2014 
@version 	P12
     
/*/
//--------------------------------------------------------------------------------------------------------------------
Function At190AFil( nParams )

Local aParam := { { 'SA1', '001', 'Cliente/Loja', 1,'A1_FILIAL+A1_COD+A1_LOJA', 'CLL', { {'SA1', 1, 'A1_NOME'} }, {}, {||.F.}, '', /*VldExtra*/ },;
				  { 'ABS', '001', 'Local de Atendimento', 1,'ABS_FILIAL+ABS_LOCAL', 'ABS', { {'ABS', 1, 'ABS_DESCRI+ABS_REGIAO'} }, {}, {||.F.}, '', /*VldExtra*/ }, ;
				  { 'SX5', 'A2_', 'Regi�o', 1,'X5_FILIAL+X5_TABELA+X5_CHAVE', 'A2', { {'SX5', 1, 'X5_DESCRI'} }, {}, {||.F.}, 'A2', /*VldExtra*/ }, ;
				  { 'TFJ', '001', 'Contrato', 5,'TFJ_FILIAL+TFJ_CONTRT+TFJ_CONREV', 'TFJCTR', {{'TFJ',5,'TFJ_ENTIDA+TFJ_CODENT+TFJ_LOJA'}}, {}, {||.F.}, '', /*VldExtra*/ }, ;
				  { 'TGS', '001', '�rea de Supervisor', 2,'TGS_FILIAL+TGS_SUPERV', 'TGSSUP', { {'TGS', 2, 'TGS_SUPERV+TGS_REGIAO'} }, {}, {||.F.}, '', /*VldExtra*/ } ;
				}

aParam[FIL_CONTRAT,11] := "At201DHas('2','"+__cUserId+"',Posicione('TFJ',5,xValueNew,'TFJ_CONTRT'),'2', .T.)"

TECA670( aParam[nParams], .F. /*lUsaCombobox*/ )

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AT190ACRGDT
	Realiza o carregamento dos dados ap�s pressionar o bot�o filtrar
para a��o dos filtros no browse com os locais por contrato

@sample		At190ACrgDt(oBrwLocais,{ oBrwRh, oBrwMi, oBrwMc, oBrwItRh, oBrwItMi, oBrwItMc })
	
@since		24/04/2014 
@version 	P12

@param		oBrwAtuacao, Objeto, Objeto FwFormBrowse constru�do a partir de query para sofrer a atualiza��o
@param		aBrwRefresh, Array, Lista de objetos FwFormBrowse constru�do a partir de query para sofrer a atualiza��o em fun��o 
				de poss�vel novo item posicionado no browse principal

/*/
//--------------------------------------------------------------------------------------------------------------------
Function At190ACrgDt( oBrwAtuacao, aBrwRefresh )

Local nZ := 1

oBrwAtuacao:SetQuery(At190ASqlBrw('TFL'))

//-------------------------------------------------
// Refresh do Browse para reexecu��o do filtro
oBrwAtuacao:GoTo( 1, .T.)

At190ARefBrw((oBrwAtuacao:cAlias)->TFL_CODIGO,{{aBrwRefresh[1],'TFF'},{aBrwRefresh[2],'TFG'},{aBrwRefresh[3],'TFH'}})

For nZ := 1 To Len(aBrwRefresh)
	aBrwRefresh[nZ]:GoTo( 1, .T.)
Next nZ

oBrwAtuacao:Refresh()

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At190ASqlBrw
	Constr�i a query para a consulta dos locais associados ao filtro fornecido pelo usu�rio

@sample		At190ASqlBrw('TFT',(oBrwMc:cAlias)->TFH_COD)
	
@since		24/04/2014 
@version 	P12

@param		cTabela, Caracter, tabela que definir� como a query ser� constru�da
@param		cparam, Caracter, informa��o adicional para filtro dos dados conforme a tabela

@return		cStrSql, Caracter, query para adi��o ao browse e filtro das informa��es

/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function At190ASqlBrw(cTabela, cParam)

Local cCondCli := ''
Local cCondCtr := ''
Local cCondReg := ''
Local cCondLoc := ''
Local cSuperv  := '' 

Local cStrSql  := ''

Default cTabela := ''
Default cParam  := ' '

If cTabela=='TFL'
	//-------------------------------------------
	//  Constr�i as condi��es considerando os filtros definidos pelo usu�rio
	cCondCli := At670FilSql( __cUserId, .F., 'SA1', '001' )
	cCondCtr := At670FilSql( __cUserId, .F., 'TFJ', '001' )
	cCondReg := At670FilSql( __cUserId, .F., 'SX5', 'A2_' )
	cCondLoc := At670FilSql( __cUserId, .F., 'ABS', '001' )
	cSuperv  := At670FilSql( __cUserId, .F., 'TGS', '001' )
	
	cStrSql += "SELECT TFL_FILIAL,TFL_CODIGO,TFL_LOCAL,TFL_DTINI,TFL_DTFIM,TFL_CONTRT,TFL_CONREV, ABS_DESCRI TFL_DESLOC,TFL_TOTRH,TFL_TOTMI,TFL_TOTMC,TFL_CODPAI"
	
	// identifica a situa��o dos itens de materiais de implanta��o no local
		cStrSql += ",( CASE WHEN TFL_TOTMI > 0 "
		cStrSql +=	"AND EXISTS ("
		cStrSql +=		"SELECT 1 "
		cStrSql +=		"FROM "+RetSqlName('TFF')+" TFF "
		cStrSql +=			"INNER JOIN "+RetSqlName('TFG')+" TFG ON TFG.TFG_FILIAL='"+xFilial('TFG')+"' AND TFG.TFG_CODPAI=TFF.TFF_COD AND TFG.D_E_L_E_T_=' ' "
		cStrSql +=		"WHERE "
		cStrSql +=			"TFF.TFF_FILIAL='"+xFilial('TFF')+"' AND TFF.D_E_L_E_T_=' ' AND TFF.TFF_CODPAI=TFL.TFL_CODIGO "
		cStrSql +=			"AND TFG.TFG_QTDVEN > "+At190AMiTot()
		cStrSql +=	") "
		cStrSql +=	"THEN '1' "
		cStrSql +=	"ELSE '0' END) TFL_MIERRO"
		
	// identificar situa��o de mc
		cStrSql += 	",(CASE WHEN TFL_TOTMC > 0 " 
		cStrSql += 		"THEN (SELECT COALESCE(SUM(TFH.TFH_QTDVEN),0) "
		cStrSql += 				"FROM "+RetSqlName('TFF')+" TFF "
		cStrSql += 					"INNER JOIN "+RetSqlName('TFH')+" TFH ON TFH.TFH_FILIAL='"+xFilial('TFH')+"' AND TFH.TFH_CODPAI=TFF.TFF_COD AND TFH.D_E_L_E_T_=' ' "
		cStrSql += 				"WHERE "
		cStrSql += 					"TFF.TFF_FILIAL='"+xFilial('TFF')+"' AND TFF.D_E_L_E_T_=' ' AND TFF.TFF_CODPAI=TFL.TFL_CODIGO) "
		cStrSql += 			"ELSE 0 END) TFL_MCVD "
		cStrSql += 	",(CASE WHEN TFL_TOTMC > 0 " 
		cStrSql += 		"THEN (SELECT COALESCE(SUM(TFT.TFT_QUANT),0) "
		cStrSql += 				"FROM "+RetSqlName('TFF')+" TFF "
		cStrSql += 					"INNER JOIN "+RetSqlName('TFH')+" TFH ON TFH.TFH_FILIAL='"+xFilial('TFH')+"' AND TFH.TFH_CODPAI=TFF.TFF_COD AND TFH.D_E_L_E_T_=' ' "
		cStrSql += 					"INNER JOIN "+RetSqlName('TFT')+" TFT ON TFT.TFT_FILIAL='"+xFilial('TFT')+"' AND TFT.TFT_CODTFH=TFH.TFH_COD AND TFT.D_E_L_E_T_=' ' "
		cStrSql += 				"WHERE "
		cStrSql += 					"TFF.TFF_FILIAL='"+xFilial('TFF')+"' AND TFF.D_E_L_E_T_=' ' AND TFF.TFF_CODPAI=TFL.TFL_CODIGO) "
		cStrSql += 		"ELSE 0 END) TFL_MCRE "

	cStrSql += " FROM "+RetSQLName('TFL')+" TFL "
	cStrSql += "INNER JOIN "+RetSqlName('TFJ')+" TFJ ON TFJ.TFJ_FILIAL='"+xFilial('TFJ')+"' AND TFJ.TFJ_CODIGO=TFL.TFL_CODPAI AND TFJ.TFJ_STATUS='1' AND TFJ.D_E_L_E_T_=' ' "
	cStrSql += "INNER JOIN "+RetSqlName('SA1')+" SA1 ON SA1.A1_FILIAL='"+xFilial('SA1')+"' AND SA1.A1_COD=TFJ.TFJ_CODENT AND SA1.A1_LOJA=TFJ.TFJ_LOJA AND SA1.D_E_L_E_T_=' ' "
	cStrSql += "INNER JOIN "+RetSqlName('ABS')+" ABS ON ABS.ABS_FILIAL='"+xFilial('ABS')+"' AND ABS.ABS_LOCAL=TFL.TFL_LOCAL AND ABS.D_E_L_E_T_=' ' "
	cStrSql += "LEFT JOIN "+RetSqlName('SX5')+" SX5 ON SX5.X5_FILIAL='"+xFilial('SX5')+"' AND SX5.X5_TABELA='A2' AND SX5.X5_CHAVE=ABS.ABS_REGIAO AND SX5.D_E_L_E_T_=' ' "
	cStrSql += "LEFT JOIN "+RetSqlName('TGS')+" TGS ON TGS.TGS_FILIAL='"+xFilial('TGS')+"' AND TGS.TGS_REGIAO=ABS.ABS_REGIAO AND TGS.D_E_L_E_T_=' ' "
	cStrSql += "WHERE TFL.D_E_L_E_T_=' ' AND TFL.TFL_FILIAL='"+xFilial('TFL')+"' "
	cStrSql += "AND TFL.TFL_CONTRT<>' ' AND TFL.TFL_CODSUB=' ' "
	cStrSql += "AND TFL.TFL_DTINI<='"+DTOS(dDtLocFim)+"' AND TFL.TFL_DTFIM>='"+DTOS(dDtLocIni)+"' "
	
	If Empty(cCondCli) .And. Empty(cCondCtr) .And. Empty(cCondReg) .And. Empty(cCondLoc) .And. Empty(cSuperv)
		cStrSql += "AND TFL.TFL_CODIGO IN (' ') "
	Else
		If !Empty(cCondCli)
			cStrSql += cCondCli
		EndIf
		
		If !Empty(cCondCtr)
			cStrSql += cCondCtr
		EndIf
		
		If !Empty(cCondReg)
			cStrSql += cCondReg
		EndIf
	
		If !Empty(cCondLoc)
			cStrSql += cCondLoc
		EndIf

		If !Empty(cSuperv)
			cStrSql += " AND (ABS.ABS_REGIAO = ' ' OR (" + Substring(cSuperv,At("(",cSuperv)-1,Len(cSuperv)) + " )) "
		EndIf		
	EndIf

//----------- TFF
ElseIf cTabela=='TFF'
	cStrSql += "SELECT "
	cStrSql += 		"TFF_FILIAL,TFF_COD,TFF_PRODUT,TFF_PERINI,TFF_PERFIM,TFF_QTDVEN,TFF_FUNCAO,TFF_TURNO,TFF_CARGO,TFF_CONTRT,TFF_CONREV,"
	cStrSql += 		"SB1.B1_DESC TFF_DESCRI,SRJ.RJ_DESC TFF_DFUNC,Coalesce(SQ3.Q3_DESCSUM,'') TFF_DCARGO,Coalesce(SR6.R6_DESC,'') TFF_DTURNO,TFF_SEQTRN "
	cStrSql += "FROM "
	cStrSql += 		RetSqlName('TFF')+" TFF "
	cStrSql += 		"INNER JOIN "+RetSqlName('SB1')+" SB1 ON SB1.B1_FILIAL='"+xFilial('SB1')+"' AND SB1.B1_COD=TFF.TFF_PRODUT AND SB1.D_E_L_E_T_=' ' "
	cStrSql += 		"INNER JOIN "+RetSqlName('SRJ')+" SRJ ON SRJ.RJ_FILIAL='"+xFilial('SRJ')+"' AND SRJ.RJ_FUNCAO=TFF.TFF_FUNCAO AND SRJ.D_E_L_E_T_=' ' "
	cStrSql += 		"LEFT JOIN "+RetSqlName('SR6')+" SR6 ON SR6.R6_FILIAL='"+xFilial('SR6')+"' AND SR6.R6_TURNO=TFF.TFF_TURNO AND SR6.D_E_L_E_T_=' ' "
	cStrSql += 		"LEFT JOIN "+RetSqlName('SQ3')+" SQ3 ON SQ3.Q3_FILIAL='"+xFilial('SQ3')+"' AND SQ3.Q3_CARGO=TFF.TFF_CARGO AND SQ3.D_E_L_E_T_=' ' "
	cStrSql += "WHERE "
	cStrSql += 		"TFF.TFF_FILIAL='"+xFilial('TFF')+"' AND TFF.D_E_L_E_T_=' ' AND TFF.TFF_CODPAI='"+cParam+"' "
	
//----------- TFG
ElseIf cTabela=='TFG'
	cStrSql += "SELECT "
	cStrSql += 		"TFG_FILIAL,TFG_COD,TFG_PERINI,TFG_PERFIM,TFG_PRODUT,TFG_UM,TFG_QTDVEN,TFG_PRCVEN,SB1.B1_DESC TFG_DESCRI,"
	cStrSql += 		At190AMiTot()+" TFG_MISEND "
	cStrSql += "FROM "
	cStrSql += 		RetSqlName('TFF')+" TFF "
	cStrSql += 		"INNER JOIN "+RetSqlName('TFG')+" TFG ON TFG_FILIAL='"+xFilial('TFG')+"' AND TFG.TFG_CODPAI=TFF.TFF_COD AND TFG.D_E_L_E_T_=' ' "
	cStrSql += 		"INNER JOIN "+RetSqlName('SB1')+" SB1 ON SB1.B1_FILIAL='"+xFilial('SB1')+"' AND SB1.B1_COD=TFG.TFG_PRODUT AND SB1.D_E_L_E_T_=' ' "
	cStrSql += "WHERE "
	cStrSql += 		"TFF.TFF_FILIAL='"+xFilial('TFF')+"' AND TFF.D_E_L_E_T_=' ' AND TFF.TFF_CODPAI='"+cParam+"' "
	
//------------ TFH
ElseIf cTabela=='TFH'
	cStrSql += "SELECT "
	cStrSql += 		"TFH_FILIAL,TFH_COD,TFH_PERINI,TFH_PERFIM,TFH_PRODUT,TFH_UM,TFH_QTDVEN,TFH_PRCVEN,SB1.B1_DESC TFH_DESCRI,"
	cStrSql += 		At190AMcTot()+" TFH_MCSEND "
	cStrSql += "FROM "
	cStrSql += 		RetSqlName('TFF')+" TFF "
	cStrSql += 		"INNER JOIN "+RetSqlName('TFH')+" TFH ON TFH_FILIAL='"+xFilial('TFH')+"' AND TFH.TFH_CODPAI=TFF.TFF_COD AND TFH.D_E_L_E_T_=' ' "
	cStrSql += 		"INNER JOIN "+RetSqlName('SB1')+" SB1 ON SB1.B1_FILIAL='"+xFilial('SB1')+"' AND SB1.B1_COD=TFH.TFH_PRODUT AND SB1.D_E_L_E_T_=' ' "
	cStrSql += "WHERE "
	cStrSql += 		"TFF.TFF_FILIAL='"+xFilial('TFF')+"' AND TFF.D_E_L_E_T_=' ' AND TFF.TFF_CODPAI='"+cParam+"' "
	
//------------ ABB
ElseIf cTabela=='ABB'
	cStrSql += "SELECT "
	cStrSql += 		"ABB_FILIAL,ABB_CODIGO,ABB_CODTEC,ABB_DATA,ABB_DTINI,ABB_HRINI,ABB_DTFIM,ABB_HRFIM,ABB_HRTOT,ABB_ATIVO,ABB_MANUT,ABB_CHEGOU,ABB_ATENDE,ABB_TIPOMV,"
	cStrSql += 		"AA1.AA1_NOMTEC ABB_NOMTEC "
	cStrSql += "FROM "
	cStrSql += 		RetSqlName('ABQ')+" ABQ "
	cStrSql += 		"INNER JOIN "+RetSqlName('ABB')+" ABB ON ABB.ABB_FILIAL='"+xFilial('ABB')+"' AND ABB.D_E_L_E_T_=' ' AND ABB.ABB_IDCFAL=ABQ.ABQ_CONTRT||ABQ.ABQ_ITEM||'CN9' "
	cStrSql += 		"INNER JOIN "+RetSqlName('AA1')+" AA1 ON AA1.AA1_FILIAL='"+xFilial('AA1')+"' AND AA1.AA1_CODTEC=ABB.ABB_CODTEC AND AA1.D_E_L_E_T_=' ' "
	cStrSql += "WHERE "
	cStrSql += 		"ABQ.ABQ_FILIAL='"+xFilial('ABQ')+"' AND ABQ.D_E_L_E_T_=' ' AND ABQ.ABQ_CODTFF='"+cParam+"' "
	cStrSql += 		"AND ABB.ABB_DTINI='"+DTOS(At190aGetDt())+"' "
	cStrSql += 				At190AGetSit()
	
//------------ TFS
ElseIf cTabela=='TFS'
	cStrSql += "SELECT "
	cStrSql += 		"TFS_FILIAL,TFS_CODIGO,TFS_DTAPON,TFS_PRODUT,TFS_QUANT,TFS_CC,TFS_TM,TFS_NUMMOV,SB1.B1_DESC TFS_DPROD "
	cStrSql += "FROM "
	cStrSql += 		RetSqlName('TFS')+" TFS "
	cStrSql += 		"INNER JOIN "+RetSqlName('SB1')+" SB1 ON SB1.B1_FILIAL='"+xFilial('SB1')+"' AND SB1.B1_COD=TFS.TFS_PRODUT AND SB1.D_E_L_E_T_=' ' "
	cStrSql += "WHERE "
	cStrSql += 		"TFS.TFS_FILIAL='"+xFilial('TFS')+"' AND TFS.D_E_L_E_T_=' ' AND TFS.TFS_CODTFG='"+cParam+"' "
	
//------------ TFT
ElseIf cTabela=='TFT'
	cStrSql += "SELECT "
	cStrSql += 		"TFT_FILIAL,TFT_CODIGO,TFT_DTAPON,TFT_PRODUT,TFT_QUANT,TFT_CC,TFT_TM,TFT_NUMMOV,SB1.B1_DESC TFT_DPROD "
	cStrSql += "FROM "
	cStrSql += 		RetSqlName('TFT')+" TFT "
	cStrSql += 		"INNER JOIN "+RetSqlName('SB1')+" SB1 ON SB1.B1_FILIAL='"+xFilial('SB1')+"' AND SB1.B1_COD=TFT.TFT_PRODUT AND SB1.D_E_L_E_T_=' ' "
	cStrSql += "WHERE "
	cStrSql += 		"TFT.TFT_FILIAL='"+xFilial('TFT')+"' AND TFT.D_E_L_E_T_=' ' AND TFT.TFT_CODTFH='"+cParam+"' "
	
EndIf

Return ChangeQuery(cStrSql)

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At190AGFields
	Cria a estrutura dos campos a serem adicionados ao browse 

@sample		At190AGFields( cTabela )
	
@since		24/04/2014 
@version 	P12

@param		cTabela, Caracter, tabela que definir� quais campos ser�o carregados

@return		aEstrut, Array, lista com as informa��es a serem utilizadas para cada campo
     
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function At190AGFields( cTabela )

Local aEstrut    := {}
Local aCampos    := {}
Local nX         := 1

Local aSave      := GetArea()
Local aSaveSX3   := SX3->( GetArea() )

Local cbCharge   := Nil

Default cTabela  := ''

If !Empty(cTabela)
	
	If cTabela == 'TFL'
		aCampos := {'TFL_FILIAL','TFL_CODIGO','TFL_LOCAL','TFL_DESLOC','TFL_DTINI','TFL_DTFIM','TFL_CONTRT','TFL_CONREV'}
	ElseIf cTabela == 'TFF'
		aCampos := {'TFF_FILIAL','TFF_COD','TFF_PERINI','TFF_PERFIM','TFF_PRODUT','TFF_DESCRI','TFF_QTDVEN','TFF_FUNCAO','TFF_DFUNC','TFF_TURNO','TFF_SEQTRN','TFF_DTURNO','TFF_CARGO','TFF_DCARGO'}
	ElseIf cTabela == 'TFG'
		aCampos := {'TFG_FILIAL','TFG_COD','TFG_PERINI','TFG_PERFIM','TFG_PRODUT','TFG_DESCRI','TFG_UM','TFG_QTDVEN','TFG_PRCVEN'} // adicionar descri��o do produto
	ElseIf cTabela == 'TFH'
		aCampos := {'TFH_FILIAL','TFH_COD','TFH_PERINI','TFH_PERFIM','TFH_PRODUT','TFH_DESCRI','TFH_UM','TFH_QTDVEN','TFH_PRCVEN'} // adicionar descri��o do produto
	ElseIf cTabela == 'ABB'
		aCampos := {'ABB_FILIAL','ABB_CODIGO','ABB_CODTEC','ABB_NOMTEC','ABB_DTINI','ABB_HRINI','ABB_DTFIM','ABB_HRFIM','ABB_HRTOT','ABB_ATIVO','ABB_MANUT'} // adicionar nome do t�cnico
	ElseIf cTabela == 'TFS'
		aCampos := {'TFS_FILIAL','TFS_CODIGO','TFS_DTAPON','TFS_PRODUT','TFS_DPROD','TFS_QUANT','TFS_CC','TFS_TM','TFS_NUMMOV'} // adicionar descri��o do produto
	ElseIf cTabela == 'TFT'
		aCampos := {'TFT_FILIAL','TFT_CODIGO','TFT_DTAPON','TFT_PRODUT','TFT_DPROD','TFT_QUANT','TFT_CC','TFT_TM','TFT_NUMMOV'}
	EndIf
	
	DbSelectArea('SX3')
	
	For nX := 1 To Len(aCampos)
		
		DbSetOrder(2) // X3_CAMPO
		
		If SX3->( DbSeek( PadR(aCampos[nX],10) ) )
			cbCharge := If( SX3->X3_TIPO=='D','{||STOD('+aCampos[nX]+')}','{||'+aCampos[nX]+'}')
			aAdd( aEstrut, { X3Titulo(), ;
							 &(cbCharge),;
							 SX3->X3_TIPO,;
							 X3Picture(aCampos[nX]),;
							 1 ,;
							 TamSX3(aCampos[nX])[1],;
							 TamSX3(aCampos[nX])[2],;
							 .F. ,;
							 {|| .F.},;
							 .F. } )
		EndIf
	Next nX

EndIf

RestArea( aSaveSX3 )
RestArea( aSave )


Return aEstrut

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At190ABrwNew
	Cria os browses baseado na FwFormBrowse e query

@sample		At190ABrwNew( "190ATFH", At190ASqlBrw('TFH',(oBrwLocais:cAlias)->TFL_CODIGO), '',     oPanMc, At190AGFields('TFH'), .F., xAux)
	
@since		24/04/2014 
@version 	P12

@param		cNameTab, Caracter, nome da tabela a ser utilizada para o retorno dos dados da query e como refer�ncia ao browse
@param		cQry, Caracter, instru��o sql para consulta dos registro que ir�o preencher o browse
@param		cDescri��o, Caracter, t�tulo a ser exibido no browse
@param		oPanel, Objeto, objeto que ir� conter o browse
@param		lActivate, Logico, define se o objeto dever� ser criado e j� ativado
@param		aAddStCols, Array, lista com as colunas de status/legenda que dever�o ser adicionadas, sendo no formato:
			{ {	posicao 1 - bloco de c�digo de captura do status
				posicao 2 - bloco de c�digo para exibi��o da legenda ao realizar duplo clique no campo
				posicao 3 - t�tulo a ser atribu�do a coluna de status
			  }, ....}    

@return		oBrwTmp, Objeto, inst�ncia da classe FwFormBrowse

/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function At190ABrwNew( cNameTab, cQry, cDescricao, oPanel, aFields, lActivate, aAddStCols)

Local oBrwTmp    := Nil 
Local nX         := 1
Local nCpo       := 1

Default cNameTab   := ''
Default cQry       := ''
Default cDescricao := ''
Default lActivate  := .T.

If !Empty(cNameTab) .And. !Empty(cQry)

	oBrwTmp := FWFormBrowse():New()
	oBrwTmp:SetDataQuery(.T.)
	oBrwTmp:SetAlias( cNameTab )
	oBrwTmp:SetQuery( cQry )
	oBrwTmp:SetDescription( cDescricao )
	oBrwTmp:SetOwner(oPanel)
	oBrwTmp:SetUseFilter( .T. )
	
	If ValType(aAddStCols)=='A'
		For nX := 1 To Len(aAddStCols)
			oBrwTmp:AddStatusColumn( aAddStCols[nX,1], aAddStCols[nX,2] )
			nCpo := Len(oBrwTmp:aColumns)
			oBrwTmp:aColumns[nCpo]:SetTitle(aAddStCols[nX,3])
		Next nX
	EndIf
	
	For nX := 1 To Len(aFields)
		oBrwTmp:AddColumn( aFields[nX] )
	Next nX

	oBrwTmp:DisableDetails()

	If lActivate
		oBrwTmp:Activate()
	EndIf

EndIf

Return oBrwTmp

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AT190AREFBRW
	Atualiza as informa��es dos browses relacionados

@sample		At190ARefBrw((oBrwAtuacao:cAlias)->TFL_CODIGO,{{aBrwRefresh[1],'TFF'},{aBrwRefresh[2],'TFG'},{aBrwRefresh[3],'TFH'}})
	
@since		24/04/2014 
@version 	P12

@param		cParam, Caracter, informa��o a ser utilizada para gera��o da query de atualiza��o do browse
@param		aBrws, Array, lista com os browses a serem atualizados

/*/
//--------------------------------------------------------------------------------------------------------------------
Function At190ARefBrw( cParam, aBrws )

Local nBrw := 1

For nBrw := 1 To Len(aBrws)
	
	aBrws[nBrw,1]:SetQuery(At190ASqlBrw(aBrws[nBrw,2],cParam))
	
	//-------------------------------------------------
	// Refresh do Browse para reexecu��o do filtro
	aBrws[nBrw,1]:GoTo( 1, .T.)
	
	If ValType(aBrws[nBrw,1]:bChange)=='B'
		Eval( aBrws[nBrw,1]:bChange, aBrws[nBrw,1] )
	EndIf
	
	aBrws[nBrw,1]:Refresh()
	
Next nBrw

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AT190AGETDT
	Retorna a data refer�ncia para consulta dos recursos humanos

@sample		At190AGetDt()
	
@since		24/04/2014 
@version 	P12

@return		dDtRhRef, Data, data de refer�ncia para filtro das agendas
     
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function At190aGetDt()

Return ( dDtRhRef )

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AT190AGETSIT
	Retorna a data refer�ncia para consulta dos recursos humanos

@sample		At190AGetSit()
	
@since		24/04/2014 
@version 	P12

@return		cStrQuery, Caracter, instru��o sql considerando os tipos de agenda marcados para filtro das agendas

/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function At190AGetSit()

Local nK        := 1
Local nMax      := Len(aDefRhSit)
Local cStrQuery := ''
Local lMarkAlgum := .F.

cStrQuery += ' AND ('

For nK := 1 To nMax
	If aDefRhSit[nK,1]
		cStrQuery += "ABB_TIPOMV='"+aDefRhSit[nK,3]+"' OR "
		lMarkAlgum := .T.
	EndIf
Next nK

If lMarkAlgum
	cStrQuery := SubStr( cStrQuery, 1, Len(cStrQuery)-3)
	cStrQuery += ')'
Else
	cStrQuery := ''
EndIf

Return cStrQuery

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AT190ALegen
	Monta janela de exibi��o das legendas conforme a refer�ncia informada

@sample		AT190ALegen(cConsulta)
	
@since		24/04/2014 
@version 	P12

@param		cConsulta, Caracter, identifica��o da legenda a ser apresentada
     
/*/
//--------------------------------------------------------------------------------------------------------------------
Function AT190ALegen(cConsulta)

Local oLegenda  :=  FWLegend():New()

If cConsulta=='AGENDA1'
	oLegenda:Add( '', 'BR_VERDE'	, STR0028 )  // 'Agenda Ativa'
	oLegenda:Add( '', 'BR_AMARELO'	, STR0029 )  // 'Agenda Alterada'
	oLegenda:Add( '', 'BR_VERMELHO'	, STR0030 )  // 'Agenda Cancelada'
	oLegenda:Add( '', 'BR_PRETO'	, STR0031 )  // 'Agenda Atendida'

ElseIf cConsulta=='AGENDA2'
	oLegenda:Add( '', 'BR_VERDE'	, STR0032 )  // 'Efetivo'
	oLegenda:Add( '', 'BR_AMARELO'	, STR0033 )  // 'Cobertura'
	oLegenda:Add( '', 'BR_LARANJA'	, STR0034 )  // 'Apoio'
	oLegenda:Add( '', 'BR_VERMELHO'	, STR0035 )  // 'Excedente'
	oLegenda:Add( '', 'BR_AZUL'		, STR0036 )  // 'Treinamento'
	oLegenda:Add( '', 'BR_PRETO'	, STR0037 )  // 'Curso'
	oLegenda:Add( '', 'BR_BRANCO'	, STR0038 )  // 'Cortesia'
	oLegenda:Add( '', 'BR_PINK'		, STR0039 )  // 'Outros Tipos'

ElseIf cConsulta=='ITRH'
	oLegenda:Add( '', 'BR_AZUL' 	, STR0040 )  // 'Item n�o iniciado'
	oLegenda:Add( '', 'BR_PRETO'	, STR0041 )  // 'Item encerrado'
	oLegenda:Add( '', 'BR_VERDE'	, STR0042 )  // 'Opera��o: Atendido'
	oLegenda:Add( '', 'BR_AMARELO'	, STR0043 )  // 'Opera��o: Parcialmente Atendido'
	oLegenda:Add( '', 'BR_VERMELHO'	, STR0044 )  // 'Opera��o: N�o Atendido'
 
ElseIf cConsulta=='ITMI'
	oLegenda:Add( '', 'BR_AZUL' 	, STR0040 )  // 'Item n�o iniciado'
	oLegenda:Add( '', 'BR_PRETO'	, STR0041 )  // 'Item encerrado'
	oLegenda:Add( '', 'BR_VERDE'	, STR0042 )  // 'Opera��o: Atendido'
	oLegenda:Add( '', 'BR_VERMELHO'	, STR0044 )  // 'Opera��o: N�o Atendido'

ElseIf cConsulta=='ITMC'
	oLegenda:Add( '', 'BR_AZUL' 	, STR0040 )  // 'Item n�o iniciado'
	oLegenda:Add( '', 'BR_PRETO'	, STR0041 )  // 'Item encerrado'
	oLegenda:Add( '', 'BR_VERDE'	, STR0042 )  // 'Opera��o: Atendido'
	oLegenda:Add( '', 'BR_AMARELO'	, STR0043 )  // 'Opera��o: Parcialmente Atendido'
	oLegenda:Add( '', 'BR_VERMELHO'	, STR0044 )  // 'Opera��o: N�o Atendido'

ElseIf cConsulta=='LOCGER'
	oLegenda:Add( '', 'BR_AZUL' 	, STR0045 )  // 'Vig�ncia n�o iniciada'
	oLegenda:Add( '', 'BR_VERDE'	, STR0046 )  // 'Em opera��o'
	oLegenda:Add( '', 'BR_PRETO'	, STR0047 )  // 'Vig�ncia encerrada'

ElseIf cConsulta=='LOCRH'
	oLegenda:Add( '', 'BR_BRANCO'	, STR0048 )  // 'N�o existe Recurso Humano'
	oLegenda:Add( '', 'BR_AZUL' 	, STR0040 )  // 'Item n�o iniciado'
	oLegenda:Add( '', 'BR_PRETO'	, STR0041 )  // 'Item encerrado'
	oLegenda:Add( '', 'BR_VERDE'	, STR0042 )  // 'Opera��o: Atendido'
	oLegenda:Add( '', 'BR_AMARELO'	, STR0043 )  // 'Opera��o: Parcialmente Atendido'
	oLegenda:Add( '', 'BR_VERMELHO'	, STR0044 )  // 'Opera��o: N�o Atendido'

ElseIf cConsulta=='LOCMI'
	oLegenda:Add( '', 'BR_BRANCO'	, STR0049 )  // 'N�o existe Material de Implanta��o'
	oLegenda:Add( '', 'BR_AZUL' 	, STR0040 )  // 'Item n�o iniciado'
	oLegenda:Add( '', 'BR_PRETO'	, STR0041 )  // 'Item encerrado'
	oLegenda:Add( '', 'BR_VERDE'	, STR0042 )  // 'Opera��o: Atendido'
	oLegenda:Add( '', 'BR_VERMELHO'	, STR0044 )  // 'Opera��o: N�o Atendido'


ElseIf cConsulta=='LOCMC'
	oLegenda:Add( '', 'BR_BRANCO'	, STR0050 )  // 'N�o existe Material de Consumo'
	oLegenda:Add( '', 'BR_AZUL' 	, STR0040 )  // 'Item n�o iniciado'
	oLegenda:Add( '', 'BR_PRETO'	, STR0041 )  // 'Item encerrado'
	oLegenda:Add( '', 'BR_VERDE'	, STR0042 )  // 'Opera��o: Atendido'
	oLegenda:Add( '', 'BR_AMARELO'	, STR0043 )  // 'Opera��o: Parcialmente Atendido'
	oLegenda:Add( '', 'BR_VERMELHO'	, STR0044 )  // 'Opera��o: N�o Atendido'

EndIf

oLegenda:Activate()
oLegenda:View()
oLegenda:DeActivate()

Return Nil

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At190ASAg1
	Identifica os status para a ABB, considerando a situa��o agenda

@sample		At190ASAg1( cTab )
	
@since		24/04/2014 
@version 	P12

@param		cTab, Caracter, tabela de dados a ter o conte�do dos campos avaliados
@return		cStatus, Caracter, c�digo da cor a ser atribu�do no campo de status

/*/
//--------------------------------------------------------------------------------------------------------------------
Function At190ASAg1( cTab )

Local cStatus := 'BR_VERDE'	//Agendamento n�o sofreu manuten��o, est� ativo e n�o foi atendido

If ( (cTab)->ABB_ATIVO == '1' )
	
	If ( (cTab)->ABB_MANUT == '1' )
		cStatus := 'BR_AMARELO'	//Agendamento alterado e ativo
	EndIf
	If ( (cTab)->ABB_ATENDE == '1' .And. (cTab)->ABB_CHEGOU = 'S' )
		cStatus := 'BR_PRETO'		//Atendimento encerrado
	EndIf
	
Else
	cStatus := 'BR_VERMELHO'	//Agendamento inativo
EndIf

Return cStatus

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At190ASAg2
	Identifica o status para a ABB considerando o tipo de aloca��o

@sample		At190ASAg2( cTab )
	
@since		24/04/2014 
@version 	P12

@param		cTab, Caracter, tabela de dados a ter o conte�do dos campos avaliados
@return		cStatus, Caracter, c�digo da cor a ser atribu�do no campo de status

/*/
//--------------------------------------------------------------------------------------------------------------------
Function At190ASAg2( cTab )

Local cStatus   := 'BR_PINK' 
Local cConteudo := (cTab)->ABB_TIPOMV

If cConteudo = '001'
	cStatus := 'BR_VERDE'    // Efetivo 
ElseIf cConteudo = '002'
	cStatus := 'BR_AMARELO'  // Cobertura
ElseIf cConteudo = '003'
	cStatus := 'BR_LARANJA'  // Apoio
ElseIf cConteudo = '004'
	cStatus := 'BR_VERMELHO' // Excedente 
ElseIf cConteudo = '005'
	cStatus := 'BR_AZUL'     // Treinamento
ElseIf cConteudo = '006'
	cStatus := 'BR_PRETO'    // Curso
ElseIf cConteudo = '007'
	cStatus := 'BR_WHITE'    // Cortesia
EndIf

Return cStatus

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At190ASMi
	Identifica o status para a TFG, considerando os materiais enviados

@sample		At190ASMi( cTab )
	
@since		24/04/2014 
@version 	P12

@param		cTab, Caracter, tabela de dados a ter o conte�do dos campos avaliados
@return		cStatus, Caracter, c�digo da cor a ser atribu�do no campo de status

/*/
//--------------------------------------------------------------------------------------------------------------------
Function At190ASMi( cTab )

Local cStatus  := 'BR_VERMELHO'  // Opera��o: N�o Atendido
Local dPerIni  := STOD((cTab)->TFG_PERINI)
Local dPerFim  := STOD((cTab)->TFG_PERFIM)

If dDatabase < dPerIni
	cStatus := 'BR_AZUL' // Item n�o iniciado

ElseIf dDatabase > dPerFim
	cStatus := 'BR_PRETO'  // Item encerrado

ElseIf ( (cTab)->TFG_MISEND >= (cTab)->TFG_QTDVEN )
	cStatus := 'BR_VERDE' // Opera��o: Atendido

EndIf

Return cStatus

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At190ASMc
	Identifica o status para a TFH, considerando os materiais enviados

@sample		At190ASMc( cTab )
	
@since		24/04/2014 
@version 	P12

@param		cTab, Caracter, tabela de dados a ter o conte�do dos campos avaliados
@return		cStatus, Caracter, c�digo da cor a ser atribu�do no campo de status

/*/
//--------------------------------------------------------------------------------------------------------------------
Function At190ASMc( cTab )

Local cStatus  := 'BR_AMARELO'  // Opera��o: Parcialmente Atendido

If dDatabase < STOD((cTab)->TFH_PERINI)
	cStatus := 'BR_AZUL' // Item n�o iniciado

ElseIf dDatabase > STOD((cTab)->TFH_PERFIM)
	cStatus := 'BR_PRETO'  // Item encerrado

ElseIf ( (cTab)->TFH_MCSEND == 0 )
	cStatus := 'BR_VERMELHO'  // Opera��o: N�o Atendido

ElseIf (cTab)->TFH_MCSEND == (cTab)->TFH_QTDVEN
	cStatus := 'BR_VERDE' // Opera��o: Atendido

EndIf

Return cStatus

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At190ASRh
	  Identifica o status para a TFF, considerando as agendas
	planejadas para o item

@sample		At190ASRh( cTab )
	
@since		24/04/2014 
@version 	P12

@param		cTab, Caracter, tabela de dados a ter o conte�do dos campos avaliados
@return		cStatus, Caracter, c�digo da cor a ser atribu�do no campo de status

/*/
//--------------------------------------------------------------------------------------------------------------------
Function At190ASRh( cTab )

Local cStatus := 'BR_AMARELO'	// 'Opera��o: Parcialmente Atendido'
Local cQryInfos := ''

If dDatabase < STOD((cTab)->TFF_PERINI)
	cStatus := 'BR_AZUL'  // 'Item n�o iniciado'
ElseIf dDatabase > STOD((cTab)->TFF_PERFIM)
	cStatus := 'BR_PRETO' // 'Item encerrado'
Else
	cQryInfos := A190ARhTot( STOD((cTab)->TFF_PERINI), STOD((cTab)->TFF_PERFIM), (cTab)->TFF_TURNO, '01', (cTab)->TFF_CONTRT,.T./*lABQxTFF*/, (cTab)->TFF_COD )

	If (cQryInfos)->(EOF()) .Or. (cQryInfos)->TFF_RHALOC == 0
		cStatus := 'BR_VERMELHO' // 'Opera��o: N�o Atendido'
	ElseIf (cQryInfos)->TFF_RHVEND == (cQryInfos)->TFF_RHALOC
		cStatus := 'BR_VERDE'	// 'Opera��o: Atendido'
	EndIf
EndIf

Return cStatus

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At190ASLA1
	Identifica a situa��o geral do local de atendimento

@sample		At190ASLA1( cTab )
	
@since		24/04/2014 
@version 	P12

@param		cTab, Caracter, tabela de dados a ter o conte�do dos campos avaliados
@return		cStatus, Caracter, c�digo da cor a ser atribu�do no campo de status

/*/
//--------------------------------------------------------------------------------------------------------------------
Function At190ASLA1( cTab )

Local cStatus := 'BR_VERDE'  // Em opera��o

If dDatabase < STOD((cTab)->TFL_DTINI)
	cStatus := 'BR_AZUL'  // Vig�ncia n�o iniciada
ElseIf dDatabase > STOD((cTab)->TFL_DTFIM)
	cStatus := 'BR_PRETO' // Vig�ncia encerrada
EndIf

Return cStatus

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At190ASLA2
	Identifica a situa��o geral dos itens de recurso humano

@sample		At190ASLA2( cTab )
	
@since		24/04/2014 
@version 	P12

@param		cTab, Caracter, tabela de dados a ter o conte�do dos campos avaliados
@return		cStatus, Caracter, c�digo da cor a ser atribu�do no campo de status

/*/
//--------------------------------------------------------------------------------------------------------------------
Function At190ASLA2( cTab )

Local cStatus       := 'BR_VERDE'  // Opera��o: Atendido
Local cStatusTmp    := ''
Local cQryTFF       := ''
Local cTabTFF       := ''
Local lAntVerde     := .F.
Local lAntVermelho  := .F.

If (cTab)->TFL_TOTRH == 0
	cStatus := 'BR_BRANCO' // N�o existe RH/MC/MI

ElseIf dDatabase < STOD((cTab)->TFL_DTINI)
	cStatus := 'BR_AZUL' // Item n�o iniciado

ElseIf dDatabase > STOD((cTab)->TFL_DTFIM)
	cStatus := 'BR_PRETO' // Item encerrado

Else
	
	cQryTFF := At190ASqlBrw('TFF',(cTab)->TFL_CODIGO)
	cTabTFF := GetNextAlias()
	
	DbUseArea( .T., "TopConn", TCGenQry(,,cQryTFF), cTabTFF, .T., .T. )
	
	While (cTabTFF)->(!EOF())
	
		cStatusTmp := At190ASRh( cTabTFF )
		//------------------------------------------------
		//  Verifica se houve a troca entre status 
		// atendido e o anterior n�o atendido, ent�o transforma em Parcialmente 
		// n�o atendido e o anterior atendido, ent�o transforma em Parcialmente
		If lAntVermelho .And. (cStatusTmp=='BR_VERDE')
			cStatusTmp := 'BR_AMARELO'
		ElseIf lAntVerde .And. (cStatusTmp == 'BR_VERMELHO')
			cStatusTmp := 'BR_AMARELO'
		EndIf
		
		//-------------------------------------
		//  Caso exista algum parcial j� sai da rotina
		If cStatusTmp == 'BR_AMARELO'
			Exit
		EndIf
		
		lAntVerde    := (cStatusTmp == 'BR_VERDE')
		lAntVermelho := (cStatusTmp == 'BR_VERMELHO')
				
		(cTabTFF)->(DbSkip())
	End
	
	(cTabTFF)->(DbCloseArea())
	
	cStatus := cStatusTmp
EndIf

Return cStatus

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At190ASLA3
	Identifica a situa��o geral dos itens de materiais de implanta��o

@sample		At190ASLA3( cTab )
	
@since		24/04/2014 
@version 	P12

@param		cTab, Caracter, tabela de dados a ter o conte�do dos campos avaliados
@return		cStatus, Caracter, c�digo da cor a ser atribu�do no campo de status

/*/
//--------------------------------------------------------------------------------------------------------------------
Function At190ASLA3( cTab )

Local cStatus := 'BR_VERDE'  // Opera��o: Atendido

If (cTab)->TFL_TOTMI == 0
	cStatus := 'BR_BRANCO' // N�o existe RH/MC/MI

ElseIf dDatabase < STOD((cTab)->TFL_DTINI)
	cStatus := 'BR_AZUL' // Item n�o iniciado

ElseIf dDatabase > STOD((cTab)->TFL_DTFIM)
	cStatus := 'BR_PRETO' // Item encerrado

ElseIf (cTab)->TFL_MIERRO=='1'
	cStatus := 'BR_VERMELHO' // Opera��o: N�o Atendido

EndIf

Return cStatus

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At190ASLA4
	Identifica a situa��o geral dos itens de materiais de consumo

@sample		At190ASLA4( cTab )
	
@since		24/04/2014 
@version 	P12

@param		cTab, Caracter, tabela de dados a ter o conte�do dos campos avaliados
@return		cStatus, Caracter, c�digo da cor a ser atribu�do no campo de status

/*/
//--------------------------------------------------------------------------------------------------------------------
Function At190ASLA4( cTab )

Local cStatus := 'BR_AMARELO' // Opera��o: Parcialmente Atendido

If (cTab)->TFL_TOTMC == 0
	cStatus := 'BR_BRANCO' // N�o existe RH/MC/MI

ElseIf dDatabase < STOD((cTab)->TFL_DTINI)
	cStatus := 'BR_AZUL' // Item n�o iniciado

ElseIf dDatabase > STOD((cTab)->TFL_DTFIM)
	cStatus := 'BR_PRETO' // Item encerrado

ElseIf (cTab)->TFL_MCRE == 0
	cStatus := 'BR_VERMELHO' // Opera��o: N�o Atendido

ElseIf (cTab)->TFL_MCRE == (cTab)->TFL_MCVD
	cStatus := 'BR_VERDE'  // Opera��o: Atendido

EndIf

Return cStatus

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At190AMiTot
	Instru��es para totalizar a quantidade de materiais de implanta��o enviados

@sample		At190AMiTot()
	
@since		24/04/2014 
@version 	P12

@return		cStrSqlMi, Caracter, instru��o sql para apurar os totais de material de implanta��o

/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function At190AMiTot()

Local cStrSqlMi := "(SELECT COALESCE( SUM(TFS_QUANT), 0) FROM "+RetSqlName('TFS')+" TFS "
cStrSqlMi += 	"WHERE TFS.TFS_FILIAL='"+xFilial('TFS')+"' AND TFS.TFS_CODTFG=TFG.TFG_COD AND TFS.D_E_L_E_T_=' '"
cStrSqlMi += 	")"

Return cStrSqlMi

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At190AMcTot
	Instru��es para totalizar a quantidade de materiais de consumo enviados

@sample		At190AMcTot()
	
@since		24/04/2014 
@version 	P12

@return		cStrSqlMc, Caracter, instru��o sql para apurar os totais de material de consumo

/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function At190AMcTot()

Local cStrSqlMc := "(SELECT COALESCE( SUM(TFT_QUANT), 0) FROM "+RetSqlName('TFT')+" TFT "
	cStrSqlMc += 		"WHERE TFT.TFT_FILIAL='"+xFilial('TFT')+"' AND TFT.TFT_CODTFH=TFH.TFH_COD AND TFT.D_E_L_E_T_=' '"
	cStrSqlMc += 	")"

Return cStrSqlMc

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} A190ARhTot
	Gera arquivo tempor�rio para realizar a captura do status dos itens de rh

@sample		A190ARhTot( STOD((cTab)->TFF_PERINI), STOD((cTab)->TFF_PERFIM), (cTab)->TFF_TURNO, '01', (cTab)->TFF_CONTRT,.T., (cTab)->TFF_COD )
	
@since		24/04/2014 
@version 	P12

@param		dDtIniPer, Data, define a data inicial para avalia��o da situa��o das agenda ou planejado
@param		dDtFimPer, Data, define a data final para avalia��o da situa��o das agenda ou planejado
@param		cTurno, Caracter, turno a ser avaliado pela tabela de hor�rio padr�o
@param		cSeq, Caracter, sequ�ncia da tabela de hor�rio padr�o para avalia��o pelo CriaCalend
@param		cCtrFil, Caracter, contrato a sofrer a avalia��o das agendas/planejamento de aloca��o
@param		lABQxTFF, Logico, define se ir� filtrar o relacionamento entre ABQ e itens vendidos de aloca��o
@param		cCodTFF, Caracter, indica qual c�digo do item de venda deve ser utilizado no relacionamento entre config. de aloca��o e item vendido

/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function A190ARhTot( dDtIniPer, dDtFimPer, cTurno, cSeq, cCtrFil, lABQxTFF, cCodTFF )

Local cStrSqlRh   := ''
Local lCriaQry    := .F.
Local nQtdAloc    := 0
Local cTabTmp     := ''

DEFAULT lABQxTFF   := .F.
DEFAULT dDtIniPer  := CTOD('')
DEFAULT dDtFimPer  := CTOD('')
DEFAULT cTurno     := ''
DEFAULT cSeq       := '01'

If dDtIniPer <> CTOD('') .And. dDtFimPer <> CTOD('')
	lCriaQry := .T.
EndIf

If lCriaQry 
	nQtdAloc := TxQtIntTrab(dDtIniPer,dDtFimPer, cTurno, cSeq)
	cStrSqlRh += "SELECT "
	cStrSqlRh += "TFF_QTDVEN*"+cValToChar(nQtdAloc)+" TFF_RHVEND "
	cStrSqlRh += ", SUM(1) TFF_RHALOC " // conta a quantidade de registros
	cStrSqlRh += "FROM ("
	cStrSqlRh += 	"SELECT DISTINCT " 
	cStrSqlRh += 		"TFF_COD,TFF_QTDVEN,ABB_DTINI,ABB_HRINI,ABB_DTFIM,ABB_HRFIM,ABB.ABB_CODTEC " 
	cStrSqlRh += 	"FROM "+RetSqlName('TFF')+" TFF "
	cStrSqlRh += 	"JOIN "+RetSqlName('ABQ')+" ABQ ON (ABQ.ABQ_FILIAL='"+xFilial('ABQ')+"' AND TFF.TFF_COD=ABQ.ABQ_CODTFF AND ABQ.D_E_L_E_T_=' ')"
	cStrSqlRh += 	"JOIN "+RetSqlName('ABB')+" ABB ON (ABB.ABB_FILIAL='"+xFilial('ABB')+"' AND ABB.ABB_IDCFAL=ABQ.ABQ_CONTRT || ABQ_ITEM || ABQ_ORIGEM AND ABB.ABB_ATIVO='1' AND ABB.D_E_L_E_T_=' ')"
	cStrSqlRh += 	"WHERE "
	cStrSqlRh += 		"TFF.TFF_FILIAL ='"+xFilial('TFF')+"' "
	cStrSqlRh += 		"AND ABQ.ABQ_CONTRT='"+cCtrFil+"' "
	If lABQxTFF
		cStrSqlRh += 	"AND ABQ.ABQ_CODTFF='"+cCodTFF+"' "
	EndIf
	cStrSqlRh += 		"AND ABB.ABB_DTINI BETWEEN '"+DTOS(dDtIniPer)+"' AND '"+DTOS(dDtFimPer)+"' " // -- per�odo da consulta
	cStrSqlRh += 		"AND TFF.D_E_L_E_T_=' ' "
	cStrSqlRh += 	"GROUP BY ABB_CODTEC,TFF_COD,ABB_DTINI,ABB_HRINI,ABB_DTFIM,ABB_HRFIM,TFF_QTDVEN) LEGENDA "
	cStrSqlRh += "GROUP BY TFF_COD,TFF_QTDVEN "
	
	cStrSqlRh := ChangeQuery(cStrSqlRh)
	cTabTmp := GetNextAlias()
	DbUseArea( .T., "TopConn", TCGenQry(,,cStrSqlRh), cTabTmp, .T., .T. )
EndIf

Return cTabTmp

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At190LocVi
	Chama a visualiza��o do local de atendimento

@sample		At190LocVi((oBrwLocais:cAlias)->TFL_LOCAL)
	
@since		24/04/2014 
@version 	P12

@param		cC�digoLocal, Caracter, c�digo do local para posicionamento na tabela ABS

/*/
//--------------------------------------------------------------------------------------------------------------------
Function At190LocVi( cCodigoLocal )

DbSelectArea('ABS')
ABS->( DbSetOrder( 1 ) ) //ABS_FILIAL+ABS_CODIGO

If ABS->(DbSeek(xFilial('ABS')+cCodigoLocal))
	FWExecView(STR0026,"VIEWDEF.TECA160",MODEL_OPERATION_VIEW,,,,10)  // 'Local de Atendimento'
EndIf

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At190LocGeo
	Chama a visualiza��o geogr�fica do local de atendimento

@sample		At190LocGeo((oBrwLocais:cAlias)->TFL_LOCAL)
	
@since		24/04/2014 
@version 	P12

@param		cC�digoLocal, Caracter, c�digo do local para posicionamento na tabela ABS

/*/
//--------------------------------------------------------------------------------------------------------------------
Function At190LocGeo( cCodigoLocal )

Local oDados := Nil
Local lOk    := .F.

DbSelectArea('ABS')
ABS->( DbSetOrder( 1 ) ) //ABS_FILIAL+ABS_CODIGO

If ABS->(DbSeek(xFilial('ABS')+cCodigoLocal))
	
	oDados := FwLoadModel('TECA160')
	oDados:SetOperation(MODEL_OPERATION_VIEW)
	lOk := oDados:Activate()
	
	If lOk
		At160VfLoc(oDados)
	EndIf
EndIf

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At190Monit
	Chama o monitor de check-in 

@sample		At190Monit()
	
@since		24/04/2014 
@version 	P12

@param		cCodLocalAtd, Caracter, c�digo do local que dever� receber a avalia��o das entradas dos atendentes

/*/
//--------------------------------------------------------------------------------------------------------------------
Function At190Monit(cCodLocalAtd)

DbSelectArea('ABS')
ABS->( DbSetOrder( 1 ) ) //ABS_FILIAL+ABS_CODIGO

If ABS->(DbSeek(xFilial('ABS')+cCodLocalAtd))
	
	If ABS->ABS_CHECK=='1'
		// chama o monitor de checkin
		TECA920()
	Else
		Help( " ", 1, "AT160NOCHECK", , STR0051, 1, 0 )  // 'Local n�o permite a utiliza��o de check-in.'
	EndIf
EndIf

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AT190MATAP
	Executa o apontamento de materiais baseado no item de TFL posicionado(posto x contrato)

@sample		At190MatAp(cTFLCodigo)
	
@since		24/04/2014 
@version 	P12

@param		cTFLCodigo, Caracter, c�digo do local x proposta para posicionamento

/*/
//--------------------------------------------------------------------------------------------------------------------
Function At190MatAp(cTFLCodigo)

DbSelectArea('TFL')
TFL->( DbSetOrder( 1 ) ) //TFL_FILIAL+TFL_CODIGO

If TFL->(DbSeek(xFilial('TFL')+cTFLCodigo))
	FWExecView(STR0025,"VIEWDEF.TECA890",MODEL_OPERATION_UPDATE,,,,)  // 'Apontamento Materiais'
EndIf

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AT190GETTPALOC
	Captura para a filial os tipos de aloca��o

@sample		At190GetTpAloc()
	
@since		24/04/2014 
@version 	P12

@return		aRet, Array, Lista com o detalhe dos tipos de loca��o a serem exibidos para filtro das agendas  
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function At190GetTpAloc()

Local aVar := {}

DbSelectArea('TCU')
TCU->( DbSetOrder(1)) //TCU_FILIAL+TCU_COD
TCU->(dbGoTop())
While TCU->( !EOF()) 
	If TCU->TCU_FILIAL==xFilial('TCU')
		// Marcado? ;	TCU_DESC ;	TCU_COD
		aAdd( aVar, { .F., TCU->TCU_DESC, TCU->TCU_COD } )
	EndIf
	TCU->(DbSkip())
End

Return aClone(aVar)

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} atTECA012A(cTFLLocal)
	Restri��es por cliente do local de atendimento

@sample		atTECA012A(cTFLCodigo)
	
@since		30/09/2015 
@version 	P12

@param		cTFLLocal, Caracter, c�digo do local atendimento

/*/
//--------------------------------------------------------------------------------------------------------------------
Function atTECA012A(cTFLLocal)

Local cCliente:=""
Local cLoja:=""
Local lRet := .F.

lRet := At680Perm( Nil, __cUserId, "021" ) // Define regras de restri��o operacional

If !lRet
	Help(,, 'TECA012A',, STR0055, 1, 0) // "Usu�rio sem permiss�o para regras de restri��o operacional"
Else
	DbSelectArea("ABS")
	ABS->( DbSetOrder( 1 ) ) //ABS_FILIAL+ABS_CODIGO
	If ABS->(DbSeek(xFilial("ABS")+cTFLLocal))
		cCliente := ABS->ABS_CODIGO
		cLoja := ABS->ABS_LOJA
		DbSelectArea("SA1")
		ABS->( DbSetOrder( 1 ) ) //SA1_FILIAL+SA1_CODIGO
		If SA1->(DbSeek(xFilial("SA1")+cCliente+cLoja))
			FWExecView(STR0053,"VIEWDEF.TECA012A",MODEL_OPERATION_UPDATE,,,,)  // "Restri��es por cliente"
		Endif
	EndIf
Endif

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} atTECA012B(cTFLLocal)
	Restri��es por cliente do local de atendimento

@sample		atTECA012A(cTFLCodigo)
	
@since		30/09/2015 
@version 	P12

@param		cTFLLocal, Caracter, c�digo do local atendimento

/*/
//--------------------------------------------------------------------------------------------------------------------
Function atTECA012B(cTFLLocal)

Local lRet := .F.

lRet := At680Perm( Nil, __cUserId, "021" ) // Define regras de restri��o operacional

If !lRet
	Help( ,, 'TECA012B',, STR0055, 1, 0) // "Usu�rio sem permiss�o para regras de restri��o operacional"	
Else
	DbSelectArea("ABS")
	ABS->( DbSetOrder( 1 ) ) //ABS_FILIAL+ABS_CODIGO
	If ABS->(DbSeek(xFilial("ABS")+cTFLLocal))
				FWExecView(STR0054,"VIEWDEF.TECA012B",MODEL_OPERATION_UPDATE,,,,)  // "Restri��es por local de atendimento"
	EndIf
Endif

Return lRet
