#Include "Protheus.ch"
#Include "RwMake.ch" 
#Include "TopConn.ch"
#Include "TECA353.ch"

#DEFINE INSALUBRIDADE 1
#DEFINE PERICULOSIDADE 2

#DEFINE NENHUM ""
#DEFINE MINIMO "2"
#DEFINE MEDIO "3"
#DEFINE MAXIMO "4"

#DEFINE INTEGRAL "2"
#DEFINE PROPORCIONAL "3"

#DEFINE ID_PERICULOSIDADE 		36
#DEFINE ID_INSALUBRIDADE_MAXIMA	39
#DEFINE ID_INSALUBRIDADE_MEDIA	38
#DEFINE ID_INSALUBRIDADE_MINIMA	37

#DEFINE ADICIONAIS_ADICIONAL	1
#DEFINE ADICIONAIS_TIPO			2
#DEFINE ADICIONAIS_GRAU			3
#DEFINE ADICIONAIS_HORAS		4
#DEFINE ADICIONAIS_AB9			5
#DEFINE ADICIONAIS_PREV			5

Static aCacheCFol := {}

/*------------------------------------------------------------------------------
{Protheus.doc} TECA353

@sample 	 TECA353() 
@since		 25/05/2015       
@version	 P12    
@description Envio de adicionais de periculosidade e insalubridade 
------------------------------------------------------------------------------*/

Function TECA353(lSemTela, aParams, aEmployees)
 
	Local lPerg := .F.
	Local cTpExp := SuperGetMV("MV_GSOUT", .F., "1") //1 - Integra��o RH protheus(Default) - 2 Ponto de Entrada - 3 Arquivo CSV
	Local lJob := .F.
	Local aRet := {.T.,{}}
	
	Default lSemTela := IsBlind()
	Default aParams := {}
	Default aEmployees := {}
	
	lJob := lSemTela
	
	If !lJob
		lPerg := Pergunte("TECA353",.T.)
	Else
		lPerg := Pergunte("TECA353",.F.)
		If !EMPTY(aParams)
			MV_PAR01 := aParams[ASCAN(aParams, {|d| d[1] == "MV_PAR01"})][2]
			MV_PAR02 := aParams[ASCAN(aParams, {|d| d[1] == "MV_PAR02"})][2]
			MV_PAR03 := aParams[ASCAN(aParams, {|d| d[1] == "MV_PAR03"})][2]
			MV_PAR04 := aParams[ASCAN(aParams, {|d| d[1] == "MV_PAR04"})][2]
			MV_PAR05 := aParams[ASCAN(aParams, {|d| d[1] == "MV_PAR05"})][2]
			MV_PAR06 := aParams[ASCAN(aParams, {|d| d[1] == "MV_PAR06"})][2]
			MV_PAR07 := aParams[ASCAN(aParams, {|d| d[1] == "MV_PAR07"})][2]
		EndIf
	EndIf
	
	If lPerg
		
		If  "2" $ cTpExp .and. !ExistBlock("At353EvRH")
			If !lJob
				Help(,, "At353EvRH",STR0025 ,, 1, 0)//"Ponto de Entrada At910CMa nao compilado."
			EndIf
			cTpExp := StrTran(cTpExp, "2", )
			AADD(aRet[2], STR0025)
		EndIf
		
		If "2" $ cTpExp
			ExecBlock("At353EvRH", .F., .F., {MV_PAR01, MV_PAR02, MV_PAR03, MV_PAR04, MV_PAR05, MV_PAR06, MV_PAR07})
		EndIf

		If "1" $ cTpExp
 			If !lJob
				MsgRun( STR0017,, {|| At353Gera(lJob,@aRet,aEmployees) })//"Processando..."
			Else
				At353Gera(lJob,@aRet,aEmployees)
			EndIf
		EndIf
	EndIf
Return aRet
/*------------------------------------------------------------------------------
{Protheus.doc} At353Gera
	
@since       25/05/2015
@version     12
@param             
@return           
@description Efetiva a inclus�o ou exclus�o de lan�amentos de adicionais de 
             periculosidade e insalubridade no SIGAGPE via GPEA580
------------------------------------------------------------------------------*/
Function At353Gera(lJob,aRet,aEmployees)

	Local aAdicionais := {}	
	Local nPos := 0
	Local cFilFunOld := ""
	Local cMatFunOld := ""
	Local lErro := .F.
	Local nTotReg := 0
	Local cAliasA := GetNextAlias()
	Local cQuery := ""
	Local nX
	Local lGerOS	:= SuperGetMV("MV_GSGEROS",.F.,"1") == "1"
	Local lProcNew	:= !lGerOS .And. ABB->( ColumnPos('ABB_ADIENV') ) > 0 //.T. N�o usa O.S - .F. Usa O.S

	Default lJob := .F.
	Default aRet := {.T.,{}}
	Default aEmployees := {}

	cQuery := "SELECT SRA.RA_FILIAL, SRA.RA_MAT, SRA.RA_NOME, SRA.RA_ADCINS, SRA.RA_ADCPERI, SRA.RA_PROCES, SRA.RA_HRSMES, SRA.RA_CC, SRA.RA_CODFUNC, "
	cQuery += "TFF.TFF_PERICU, TFF.TFF_INSALU, TFF.TFF_GRAUIN, "
	cQuery += "AA1.AA1_FUNFIL, AA1.AA1_CDFUNC, "
	
	If !lProcNew
		cQuery += "AB9.R_E_C_N_O_ AS RECAB9, ABA_QUANT HORAS, ABB.R_E_C_N_O_ AS RECABB"
	Else 
		cQuery += "ABB.R_E_C_N_O_ AS RECABB, ABB.ABB_HRTOT HORASABB"
	EndIf 

	cQuery += " FROM " + RetSqlName("ABB")+ " ABB "
	
	cQuery += " LEFT JOIN " + RetSqlName("AA1")+ " AA1 ON "
	cQuery += " AA1.AA1_FILIAL  = '" + xFilial("AA1") + "'"
	cQuery += " AND AA1.AA1_CODTEC  = ABB.ABB_CODTEC"
	cQuery += " AND AA1.D_E_L_E_T_  = ' '"

	cQuery += " LEFT JOIN " + RetSqlName("SRA")+ " SRA ON "
	cQuery += " SRA.RA_FILIAL   = AA1.AA1_FUNFIL"
	cQuery += " AND SRA.RA_MAT      = AA1.AA1_CDFUNC"
	cQuery += " AND SRA.D_E_L_E_T_  = ' '"

	If !lProcNew

		cQuery += " INNER JOIN " + RetSqlName("AB9")+ " AB9 ON "
		cQuery += " AB9.AB9_FILIAL  = '"+ xFilial("AB9") +"'"
		cQuery += " AND AB9.AB9_ATAUT   = ABB.ABB_CODIGO"	 
		cQuery += " AND SUBSTRING( AB9.AB9_NUMOS, 1, 6 ) = ABB.ABB_NUMOS"
		cQuery += " AND AB9.AB9_CODTEC = ABB.ABB_CODTEC"
		If Mv_par06  == 1
			cQuery += " AND AB9.AB9_ADIENV  = 'F'"
		Else
			cQuery += " AND AB9.AB9_ADIENV  = 'T'"
		EndIf

		cQuery += " AND AB9.D_E_L_E_T_ = ' '"

		cQuery += " INNER JOIN " + RetSqlName("ABA")+ " ABA ON "
		cQuery += " ABA.ABA_FILIAL = '"+ xFilial("ABA") +"'"
		cQuery += " AND ABA.ABA_CODTEC = ABB.ABB_CODTEC"
		cQuery += " AND ABA.ABA_NUMOS = AB9.AB9_NUMOS"
		cQuery += " AND ABA.ABA_SEQ = AB9.AB9_SEQ"
		cQuery += " AND ABA.D_E_L_E_T_ = ' '"
	
	EndIf

	cQuery += " INNER JOIN " + RetSqlName("ABQ")+ " ABQ ON "
	cQuery += " ABQ.ABQ_FILIAL  = '"+ xFilial("ABQ") +"'"
	cQuery += " AND ABQ.ABQ_CONTRT || ABQ.ABQ_ITEM || ABQ.ABQ_ORIGEM = ABB.ABB_IDCFAL"
	cQuery += " AND ABQ.D_E_L_E_T_  = ' '"

	cQuery += " INNER JOIN " + RetSqlName("TFF")+ " TFF ON "
	cQuery += " TFF.TFF_FILIAL  = ABQ.ABQ_FILTFF"
	cQuery += " AND TFF.TFF_COD     = ABQ.ABQ_CODTFF"
	cQuery += " AND (TFF.TFF_PERICU <> '1' OR TFF.TFF_INSALU <> '1')"
	cQuery += " AND TFF.D_E_L_E_T_  = ' '"

	cQuery += " WHERE ABB.ABB_FILIAL  = '"+ xFilial("ABB") +"'"
	If Empty(aEmployees)
		cQuery += " AND ABB.ABB_CODTEC BETWEEN '"+ MV_PAR01 +"' AND '"+ MV_PAR02 +"'"
	Else
		cQuery += " AND ABB.ABB_CODTEC IN ( "
		For nX := 1 to LEN(aEmployees)
			cQuery += " '" + aEmployees[nX] + "',"
		Next nX
		cQuery := LEFT(cQuery, LEN(cQuery) - 1)
		cQuery += " ) "
	EndIf
	cQuery += " AND ABB.ABB_DTINI  >= '"+ DtoS(Mv_Par03) +"'"
	cQuery += " AND ABB.ABB_DTFIM  <= '"+ DtoS(Mv_Par04) +"'"
	cQuery += " AND ABB.ABB_CHEGOU = 'S' AND ABB.ABB_ATENDE = '1'
	cQuery += " AND ABB.ABB_ATIVO   = '1'"
	cQuery += " AND ABB.ABB_LOCAL  <> ' '"
	cQuery += " AND ABB.D_E_L_E_T_  = ' '"
	If lProcNew
		If Mv_par06  == 1
			cQuery += " AND ABB.ABB_ADIENV  = 'F'"
		Else
			cQuery += " AND ABB.ABB_ADIENV  = 'T'"
		EndIf
	EndIf	
	
	cQuery += " ORDER BY ABB.ABB_CODTEC"
	
	cQuery := ChangeQuery(cQuery)
	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery), cAliasA , .T., .T.)
	aEval( ABB->(DbStruct()),{|x| If(x[2] != "C", TcSetField(cAliasA, AllTrim(x[1]), x[2], x[3], x[4]),Nil)})

	DbSelectArea( cAliasA )
	(cAliasA)->( DbEval( { || nTotReg++ },,{ || !Eof() } ) )
	(cAliasA)->( DbGoTop() )
	
	ProcRegua( nTotReg )
 
	If nTotReg <= 0
		If !lJob
			Aviso(STR0001,STR0003, {STR0001}) // "Aten��o" # "N�o h� dados, verifique par�metros # "OK"
		EndIf
		aRet[1] := .F.
		AADD(aRet[2], STR0003)
		(cAliasA)->(DbCloseArea())
		Return()
	EndIf
	
	If (cAliasA)->(!EOF())
		cFilFunOld := (cAliasA)->RA_FILIAL
		cMatFunOld := (cAliasA)->RA_MAT
	EndIf
	

	While (cAliasA)->(!Eof())
		
		//Verifica se mudou funcion�rio e envia informa��es para RH.
		If cFilFunOld != (cAliasA)->RA_FILIAL .OR. cMatFunOld != (cAliasA)->RA_MAT
						
			If !At353EnvRH(cFilFunOld, cMatFunOld, aAdicionais, @aRet, lJob, lProcNew)//Envia para RH
				lErro := .T.
			EndIf
						
			//Reinicia Variaveis
			aAdicionais	:= {}
			cFilFunOld := (cAliasA)->RA_FILIAL
			cMatFunOld := (cAliasA)->RA_MAT
			
		EndIf
		
		//Armazena informa��o relativa a horas e ao nivel de adicional (integral/proporcional,  maximo/medio/minimo)
				
		//Periculosidade
		If !Empty((cAliasA)->TFF_PERICU) .AND. (cAliasA)->TFF_PERICU != "1" 
			
			//Busca posi��o por tipo e grau
			nPos := aScan(aAdicionais, {|x|	x[ADICIONAIS_ADICIONAL] == PERICULOSIDADE .AND.;
				 								x[ADICIONAIS_TIPO] == (cAliasA)->TFF_PERICU .AND.;
				  								x[ADICIONAIS_GRAU] == NENHUM})
				  								
			If nPos == 0
				aAdd(aAdicionais, Array(ADICIONAIS_PREV))
				nPos := Len(aAdicionais)
				aAdicionais[nPos][ADICIONAIS_ADICIONAL] 	:= PERICULOSIDADE
				aAdicionais[nPos][ADICIONAIS_TIPO] 		:= (cAliasA)->TFF_PERICU
				aAdicionais[nPos][ADICIONAIS_GRAU] 		:= NENHUM
				If !lProcNew
					aAdicionais[nPos][ADICIONAIS_HORAS] 		:= (cAliasA)->HORAS
					aAdicionais[nPos][ADICIONAIS_AB9] 			:= {(cAliasA)->RECAB9}
				Else 
					aAdicionais[nPos][ADICIONAIS_HORAS] 		:= Round(HoraToInt(SubStr((cAliasA)->HORASABB,6,10)),2) //(cAliasA)->HORASABB
					aAdicionais[nPos][ADICIONAIS_AB9] 			:= {(cAliasA)->RECABB}
				EndIf
			Else
				If !lProcNew
					aAdicionais[nPos][ADICIONAIS_HORAS] += (cAliasA)->HORAS
					aAdd(aAdicionais[nPos][ADICIONAIS_AB9],(cAliasA)->RECAB9 )
				Else 
					aAdicionais[nPos][ADICIONAIS_HORAS] +=  Round(HoraToInt(SubStr((cAliasA)->HORASABB,6,10)),2) //(cAliasA)->HORASABB
					aAdd(aAdicionais[nPos][ADICIONAIS_AB9],(cAliasA)->RECABB )
				EndIf 	
			EndIf		
			
		EndIf
		
		
		//Insalubridade
		If !Empty((cAliasA)->TFF_INSALU) .AND. (cAliasA)->TFF_INSALU != "1" 	
			
			//Busca posi��o por tipo e grau
			nPos := aScan(aAdicionais, {|x| x[ADICIONAIS_ADICIONAL] == INSALUBRIDADE .AND. ;
												x[ADICIONAIS_TIPO] == (cAliasA)->TFF_INSALU .AND.;
												x[ADICIONAIS_GRAU] == (cAliasA)->TFF_GRAUIN})			
			If nPos == 0
				aAdd(aAdicionais, Array(ADICIONAIS_PREV))
				nPos := Len(aAdicionais)
				aAdicionais[nPos][ADICIONAIS_ADICIONAL] 	:= INSALUBRIDADE
				aAdicionais[nPos][ADICIONAIS_TIPO] 		:= (cAliasA)->TFF_INSALU
				aAdicionais[nPos][ADICIONAIS_GRAU] 		:= (cAliasA)->TFF_GRAUIN
				If !lProcNew
					aAdicionais[nPos][ADICIONAIS_HORAS] 		:= (cAliasA)->HORAS
					aAdicionais[nPos][ADICIONAIS_AB9] 			:= {(cAliasA)->RECAB9}
				Else 
					aAdicionais[nPos][ADICIONAIS_HORAS] 		:= Round(HoraToInt(SubStr((cAliasA)->HORASABB,6,10)),2) //(cAliasA)->HORASABB
					aAdicionais[nPos][ADICIONAIS_AB9] 			:= {(cAliasA)->RECABB}
				EndIf 
			Else
				If !lProcNew
					aAdicionais[nPos][4] += 	(cAliasA)->HORAS
					aAdd(aAdicionais[nPos][5],(cAliasA)->RECAB9 )
				Else 
					aAdicionais[nPos][4] += Round(HoraToInt(SubStr((cAliasA)->HORASABB,6,10)),2) //(cAliasA)->HORASABB
					aAdd(aAdicionais[nPos][5],(cAliasA)->RECABB )
				EndIf 	
			EndIf
			
		EndIf 
	
		(cAliasA)->(DbSkip())
	End
	
	
	//Envia ultimas informa��es para RH.
	If Len(aAdicionais) > 0
			
		If !At353EnvRH(cFilFunOld, cMatFunOld, aAdicionais, @aRet, lJob, lProcNew)//Envia para RH
			lErro := .T.
		EndIf
		
		//Reinicia Variaveis
		aAdicionais	:= {}
		cFilFunOld 	:= ""
		cMatFunOld 	:= ""
		
	EndIf

	(cAliasA)->(DbCloseArea())
   
	If lErro
		If !lJob
			Aviso(STR0016, STR0010 + CRLF + STR0011 +TxLogPath(STR0007),  {STR0001}) // "Ocorreram erros " # "Foi gerado o log no arquivo " # ", deseja visualizar LOG?" # " Aten��o"
		EndIf
		aRet[1] := .F.
		AADD(aRet[2], STR0010 + "##" + STR0011 +TxLogPath(STR0007))
	Else
		If !lJob
			Aviso(STR0016, STR0016, {STR0001}) // "Finaliza��o" # "Processo finalizado" # "OK"
		EndIf
		AADD(aRet[2], STR0016)
	EndIf
Return()


/*/{Protheus.doc} At353EnvRH
Aplica regras de hierarquia de beneficios e realiza o envio dos adicionais para o RH. 
@since 26/06/2015
@version 1.0
@param cFilFun, String, Filial do Funcion�rio
@param cMatFun, String, Matricula do funcion�rio
@param aAdicionais, Array, Adicionais a serem enviados
@param lProcNew, Indica se usa o processo sem O.S .T. - Sem O.S, .F. - Com O.S
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/Static Function at353EnvRH(cFilFun, cMatFun, aAdicionais, aRet, lSemTela,lProcNew)
	Local lRet := .T.	
	Local aAB9 := {}	
	Local lLogSuccess := (MV_PAR07 == 1) //Gera Log Total
	Local aCabec := {}
	Local aItens := {}	
	Local lGera := .T.
	Local cRoteiro := ""
	Local cPeriodo := ""
	Local cNumPagto := ""
	Local aPerAtual := {}
	Local aCodFol := {}
	Local cTxtLog := ""
	Local nPerInt := 0
	Local nPerProp := 0
	Local nInsIntMax := 0
	Local nInsIntMed := 0
	Local nInsIntMin := 0
	Local nInsPrpMax := 0
	Local nInsPrpMed := 0
	Local nInsPrpMin := 0
	Local nOpc      := Iif(MV_PAR06 == 1, 3, 5)//Inclus�o ou estorno
	Local cErro := ""
	Local nI := 0
	Local nY := 0
	Local cCRLF
	
	Private lMsHelpAuto    := .F.
	Private lMsErroAuto    := .F.
	Private lAutoErrNoFile := .T.
	
	Default aRet := {.T.,{}}
	Default lSemTela := .F.
	
	cCRLF := IIF(lSemTela,"##",CRLF)
	
	SRA->(DbSetOrder(1))
	
	If SRA->(DbSeek(cFilFun+cMatFun))
	
		cTxtLog 	:= STR0004+" "+SRA->RA_MAT +" / "+SRA->RA_NOME + CRLF  //"Funcion�rio"
		cRoteiro 	:= At353GtRot()
		aCodFol := At353GetPd(SRA->RA_FILIAL)//Carrega aCodFol da Filial
		
		If Len(aCodFol) == 0
			TxLogFile(STR0007,cTxtLog+ CRLF + STR0008 +CRLF)  // "Funcion�rio " # "Erro ao carregar o roteiro de calculo"
			lGera := .F.	
		EndIf
			
		If Empty(cRoteiro)//VErifica roteiro da Folha
			TxLogFile(STR0007,cTxtLog+ CRLF + STR0008 +CRLF)  // "Funcion�rio " # "Erro ao carregar o roteiro de calculo"
			lGera := .F.	
		EndIF
		
		//Verifica periodo
		If fGetPerAtual( @aPerAtual, NIL, SRA->RA_PROCES, cRoteiro )				
			cPeriodo 	:= aPerAtual[1,1]
			cNumPagto	:= aPerAtual[1,2]				
		Else
			TxLogFile(STR0007,cTxtLog+ CRLF + STR0018 +CRLF)  // "Funcion�rio " # "Erro ao carregar o periodo atual"
			lGera := .F.	
		EndIf
	
		// pagamento de periculosidade configurada pelo cadastro de funcion�rios
		If  SRA->RA_ADCPERI <> '1'
			TxLogFile(STR0007,cTxtLog + CRLF + STR0005 +CRLF)  // "Funcion�rio" # "Pagamento de periculosidade configurada pelo cadastro de funcion�rios"
			lGera    := .F.				
		EndIf
			 
		// pagamento de insalubridade configurada pelo cadastro de funcion�rios
		If SRA->RA_ADCINS <> '1'
			TxLogFile(STR0007,cTxtLog + CRLF + STR0006 +CRLF) // "Funcion�rio" # "Pagamento de insalubridade configurada pelo cadastro de funcion�rios"
			lGera    := .F.			
		EndIf
		
		If lGera
			//Identifica configura��es
			nPerInt	:= aScan(aAdicionais, {|x| x[ADICIONAIS_ADICIONAL] == PERICULOSIDADE .AND. x[ADICIONAIS_TIPO] == INTEGRAL })
			nPerProp 	:= aScan(aAdicionais, {|x| x[ADICIONAIS_ADICIONAL] == PERICULOSIDADE .AND. x[ADICIONAIS_TIPO] == PROPORCIONAL })
			
			nInsIntMax	:= aScan(aAdicionais, {|x| x[ADICIONAIS_ADICIONAL] == INSALUBRIDADE .AND. x[ADICIONAIS_TIPO] == INTEGRAL .AND. x[ADICIONAIS_GRAU] == MAXIMO })
			nInsIntMed	:= aScan(aAdicionais, {|x| x[ADICIONAIS_ADICIONAL] == INSALUBRIDADE .AND. x[ADICIONAIS_TIPO] == INTEGRAL .AND. x[ADICIONAIS_GRAU] == MEDIO })
			nInsIntMin	:= aScan(aAdicionais, {|x| x[ADICIONAIS_ADICIONAL] == INSALUBRIDADE .AND. x[ADICIONAIS_TIPO] == INTEGRAL .AND. x[ADICIONAIS_GRAU] == MINIMO })
			nInsPrpMax	:= aScan(aAdicionais, {|x| x[ADICIONAIS_ADICIONAL] == INSALUBRIDADE .AND. x[ADICIONAIS_TIPO] == PROPORCIONAL .AND. x[ADICIONAIS_GRAU] == MAXIMO })
			nInsPrpMed	:= aScan(aAdicionais, {|x| x[ADICIONAIS_ADICIONAL] == INSALUBRIDADE .AND. x[ADICIONAIS_TIPO] == PROPORCIONAL .AND. x[ADICIONAIS_GRAU] == MEDIO })
			nInsPrpMin	:= aScan(aAdicionais, {|x| x[ADICIONAIS_ADICIONAL] == INSALUBRIDADE .AND. x[ADICIONAIS_TIPO] == PROPORCIONAL .AND. x[ADICIONAIS_GRAU] == MINIMO })
		
			//PRIORIDADE DE ENVIO PERICULOSIDADE
			//1-Periculosidade Integral
			//2-Periculosidade Proporcional		
			
			//PRIORIDADE DE ENVIO INSALUBRIDADE
			//Insalubridade Integral Maxima
			//Insalubridade Integral Media
			//Insalubridade Integral Minima
			//Insalubridade Proporcinal conforme Grau
		
			If nPerInt > 0
				If At353ChkId(aCodFol, ID_PERICULOSIDADE)			
					aItens := At353AddIt(aItens, aCodFol[ID_PERICULOSIDADE,1], SRA->RA_HRSMES, cNumPagto)
				Else
					TxLogFile(STR0007,cTxtLog + CRLF + STR0019 +CRLF) // "Funcion�rio" # "ID de Calculo de Periculosidade n�o configurado no m�dulo de Gest�o de Pessoal"
					lRet := .F.
				EndIf
			ElseIf nPerProp > 0
				If At353ChkId(aCodFol, ID_PERICULOSIDADE)
					aItens := At353AddIt(aItens, aCodFol[ID_PERICULOSIDADE,1], aAdicionais[nPerProp][ADICIONAIS_HORAS], cNumPagto)
				Else
					TxLogFile(STR0007,cTxtLog + CRLF + STR0019 +CRLF) // "Funcion�rio" # "ID de Calculo de Periculosidade n�o configurado no m�dulo de Gest�o de Pessoal"
					lRet := .F.
				EndIf		
			EndIf
		
			
			If nInsIntMax > 0 //Adiciona Insalubridade Integral Maxima
				If At353ChkId(aCodFol, ID_INSALUBRIDADE_MAXIMA)
					aItens := At353AddIt(aItens, aCodFol[ID_INSALUBRIDADE_MAXIMA,1], SRA->RA_HRSMES, cNumPagto)
				Else
					TxLogFile(STR0007,cTxtLog + CRLF + STR0022 +CRLF) // "Funcion�rio" # "ID de Calculo de Insalubridade M�xima n�o configurado no m�dulo de Gest�o de Pessoal"
					lRet := .F.
				EndIf
			
			ElseIf nInsIntMed > 0 //Adiciona Insalubridade Integral Media
				If At353ChkId(aCodFol, ID_INSALUBRIDADE_MEDIA)
					aItens := At353AddIt(aItens, aCodFol[ID_INSALUBRIDADE_MEDIA,1], SRA->RA_HRSMES, cNumPagto)
				Else
					TxLogFile(STR0007,cTxtLog + CRLF + STR0021 +CRLF) // "Funcion�rio" # "ID de Calculo de Insalubridade M�dia n�o configurado no m�dulo de Gest�o de Pessoal"
					lRet := .F.
				EndIf
			
			ElseIf nInsIntMin > 0 //Adiciona Insalubridade Integral Minima
				If At353ChkId(aCodFol, ID_INSALUBRIDADE_MINIMA)
					aItens := At353AddIt(aItens, aCodFol[ID_INSALUBRIDADE_MINIMA,1], SRA->RA_HRSMES, cNumPagto)
				Else
					TxLogFile(STR0007,cTxtLog + CRLF + STR0020 +CRLF) // "Funcion�rio" # "ID de Calculo de Insalubridade Minima n�o configurado no m�dulo de Gest�o de Pessoal"
					lRet := .F.
				EndIf	
			Else
				If nInsPrpMax > 0	 //Adiciona Proporcional Maxima			
					If At353ChkId(aCodFol, ID_INSALUBRIDADE_MAXIMA)
						aItens := At353AddIt(aItens, aCodFol[ID_INSALUBRIDADE_MAXIMA,1], aAdicionais[nInsPrpMax][ADICIONAIS_HORAS], cNumPagto)
					Else
						TxLogFile(STR0007,cTxtLog + CRLF + STR0022 +CRLF) // "Funcion�rio" # "ID de Calculo de Insalubridade M�xima n�o configurado no m�dulo de Gest�o de Pessoal"
						lRet := .F.
					EndIf
				EndIf
				
				If nInsPrpMed > 0 //Adiciona Proporcional Media
					If At353ChkId(aCodFol, ID_INSALUBRIDADE_MEDIA)
						aItens := At353AddIt(aItens, aCodFol[ID_INSALUBRIDADE_MEDIA,1], aAdicionais[nInsPrpMed][ADICIONAIS_HORAS], cNumPagto)
					Else
						TxLogFile(STR0007,cTxtLog + CRLF + STR0021 +CRLF) // "Funcion�rio" # "ID de Calculo de Insalubridade M�dia n�o configurado no m�dulo de Gest�o de Pessoal"
						lRet := .F.
					EndIf
				EndIf
				
				If nInsPrpMin > 0 //Adiciona Proporcional Minima
					If At353ChkId(aCodFol, ID_INSALUBRIDADE_MINIMA)
						aItens := At353AddIt(aItens, aCodFol[ID_INSALUBRIDADE_MINIMA,1], aAdicionais[nInsPrpMin][ADICIONAIS_HORAS], cNumPagto)
					Else
						TxLogFile(STR0007,cTxtLog + CRLF + STR0020 +CRLF) // "Funcion�rio" # "ID de Calculo de Insalubridade Minima n�o configurado no m�dulo de Gest�o de Pessoal"
						lRet := .F.
					EndIf
				EndIf		
			EndIf
		
			If Len(aItens) > 0
				
				aadd(aCabec,{'RA_FILIAL' , SRA->RA_FILIAL, Nil })
				aadd(aCabec,{'RA_MAT'    , SRA->RA_MAT, Nil })
				aadd(aCabec,{'CPERIODO'  , cPeriodo            , Nil })
				aadd(aCabec,{'CROTEIRO'  , cRoteiro            , Nil })
				aadd(aCabec,{'CNUMPAGTO' , cNumPagto           , Nil })	
				
				If nOpc == 3
					nOpc := At353BenVa(SRA->RA_FILIAL,SRA->RA_MAT,SRA->RA_PROCES,cPeriodo,cRoteiro)
				EndIf
  
				
				MsExecAuto( {|x,y,z| GPEA580(Nil,x,y,z)}, aCabec ,aItens, nOpc)
				
				If lMsErroAuto
					cErro := ""
					aEval(GetAutoGRLog(),{|x| cErro +=  x + cCRLF })							
					TxLogFile(STR0007, cTxtLog + cErro)   // "Adicionais"
					AADD(aRet[2], cTxtLog + cErro)
					lRet := .F.
					aRet[1] := .F.
				Else
					If !lProcNew
						For nI:=1 To Len(aAdicionais)
							For nY:=1 To Len(aAdicionais[nI][ADICIONAIS_AB9])
								AB9->(DbGoTo( aAdicionais[nI][ADICIONAIS_AB9][nY] ))
								If AB9->(!EOF())
									RecLock("AB9", .F.)
									AB9->AB9_ADIENV := MV_PAR06 == 1
									MsUnLock()
								EndIf															
							Next nY										
						Next nI
					Else 
						For nI:=1 To Len(aAdicionais)
							For nY:=1 To Len(aAdicionais[nI][ADICIONAIS_AB9])
								ABB->(DbGoTo( aAdicionais[nI][ADICIONAIS_AB9][nY] ))
								If ABB->(!EOF())
									RecLock("ABB", .F.)
									ABB->ABB_ADIENV := MV_PAR06 == 1
									MsUnLock()
								EndIf															
							Next nY										
						Next nI
					EndIf 

					If lLogSuccess
						If nOpc == 5
							TxLogFile(STR0007,cTxtLog + cCRLF + STR0024 + cCRLF)   //"Funcion�rio # "Estorno realizado com sucesso"
							AADD(aRet[2], cTxtLog + cCRLF + STR0024 + cCRLF)								
						Else
							TxLogFile(STR0007,cTxtLog + cCRLF + STR0009 + cCRLF)   //"Funcion�rio # "Lan�amento realizado com sucesso"
							AADD(aRet[2], cTxtLog + cCRLF + STR0009 + cCRLF)
						EndIf
					EndIf
				EndIf
			EndIf
		EndIf
	EndIf
Return lGera .AND. lRet

/*/{Protheus.doc} At353AddIt
Realiza a inclus�o de um novo item na estrutura de itens da RGB
@since 26/06/2015

@param aItens}, Array, array que ser� adicionado o item
@param cVerba, String, C�digo da verba
@param nHoras, Integer, Quantidade de horas
@param cNumPagto, String, Numero de pagamento

@return aItens, Array com o item inclu�do

/*/
Static Function At353AddIt(aItens, cVerba, nHoras, cNumPagto)
	Local aAux := {}
	
	aAdd(aAux,{"RGB_FILIAL" , xFilial("RGB") , Nil })
	aAdd(aAux,{"RGB_MAT"    , SRA->RA_MAT    , Nil })
	aAdd(aAux,{"RGB_PROCESS", SRA->RA_PROCES , Nil })
	aAdd(aAux,{"RGB_PD"     , cVerba         , Nil })
	aAdd(aAux,{"RGB_TIPO1"  , "H"            , Nil })
	aAdd(aAux,{"RGB_HORAS"  , nHoras		, Nil })
	aAdd(aAux,{"RGB_CC"     , SRA->RA_CC     , Nil })
	aAdd(aAux,{"RGB_CODFUN" , SRA->RA_CODFUNC, Nil })
	aAdd(aAux,{"RGB_SEMANA" , cNumPagto, Nil })
	aadd(aItens, aAux)

Return aItens

Static Function At353GtRot()

Return fGetRotOrdinar()

/*/{Protheus.doc} At353GetPd

Realiza otimiza��o do carregamento das verbas por filial

@since 25/06/2015
@version 1.0
@param aCods, Array, Cache para controle de verbas por ID de calculo
@return aCodFol, Array com identificadores de calculo da filial

/*/
Static Function At353GetPd( cFil)
	Local aRet := {}
	Local nPos := 0
	Local aCodFol := {}

	nPos := aScan(aCacheCFol, {|x| x[1] == cFil})
	If nPos == 0	
		Fp_CodFol(@aCodFol, cFil)
		
		aAdd(aCacheCFol, {cFil, aClone(aCodFol)}) 		
	Else
		aCodFol := aClone(aCacheCFol[nPos][2])
	EndIf
	
Return aCodFol

/*/{Protheus.doc} At353ChkId
Verifica se id � existende em aCodFol

@since 26/06/2015
@param aCodFol, Array, COdigos de identificadores de calculo
@param nId, Integer, id do identificador de calculo
@return Boolean
/*/
Static Function At353ChkId(aCodFol, nId)
	Local lRet := .T.
	
	lRet := !Empty(aCodFol[nId][1])
	
Return lRet

/*/{Protheus.doc} At353BenVa

Verifica se h� um lan�amento para o funcionario, para chamar execauto como altera��o

@since 13/07/2020
@version 1.0
@param cFilAux, Caracter, Filial
@param cMatricula, Caracter, Matricula do Funcionario a ser pesquisado
@param cProcesso, Caracter, Codigo do Processo do Funcionario
@param cCodPer, Caracter, Codigo do Periodo
@param cCodRot, Caracter, Codigo do Roteiro
@return nOpc - Retorna o modo para a excauto - Inclus�o ou Altera��o
/*/
Static Function At353BenVa(cFilAux,cMatricula,cProcesso,cCodPer,cCodRot)
Local cQry 			:= GetNextAlias()

BeginSQL Alias cQry
	SELECT 1 REC
	FROM %Table:RGB% RGB
	WHERE RGB.RGB_FILIAL = %Exp:cFilAux%
		AND RGB.%NotDel%
		AND RGB.RGB_PROCES = %Exp:cProcesso%
		AND RGB.RGB_PERIOD = %Exp:cCodPer%
		AND RGB.RGB_ROTEIR = %Exp:cCodRot%
		AND RGB.RGB_MAT = %Exp:cMatricula%
EndSQL

lInclusao := (cQry)->(EOF())
(cQry)->(DbCloseArea())

dBSelectArea("RGB")
dbSetorder(3) // RGB_FILIAL, RGB_PROCES, RGB_MAT, RGB_PERIOD
RGB->(dbSeek(cFilAux+cProcesso+cMatricula+cCodPer))

Return Iif(lInclusao,3,4)
