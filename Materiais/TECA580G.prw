#include 'protheus.ch'
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TECA580G.CH"

Static cRetF3 := ""
//-------------------------------------------------------------------
/*/{Protheus.doc} TECA580G
	Rotina de cadastro de Manuten��es Programadas

@author	boiani
@since 26/11/2019
/*/
//-------------------------------------------------------------------
Function TECA580G()
Local oBrw := FwMBrowse():New()
Local aCampos := At580GCampo()

Private aRotina	:= MenuDef()
oBrw:SetAlias('TDX')
oBrw:SetFields(aCampos)
oBrw:SetOnlyFields( { 'TDX_COD', 'TDX_CODTDW', 'TDX_TURNO', 'TDX_SEQTUR', 'TDX_TIPO', 'TDX_STATUS' } )
oBrw:SetDescription( OEmToAnsi( STR0001) ) //"Manuten��es Programadas"
oBrw:Activate()

Return
//-------------------------------------------------------------------
/*/{Protheus.doc} Menudef
	Op��es de menu da rotina

@author	boiani
@since 26/11/2019
/*/
//-------------------------------------------------------------------
Static Function Menudef()
Local aMenu := {}

ADD OPTION aMenu Title STR0002 Action 'VIEWDEF.TECA580G' OPERATION 2 ACCESS 0 //'Visualizar'
ADD OPTION aMenu Title STR0003 Action 'VIEWDEF.TECA580G' OPERATION 4 ACCESS 0 //'Alterar'

Return aMenu
//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
	Defini��o do modelo de Dados

@author	boiani
@since 26/11/2019
/*/
//-------------------------------------------------------------------
Static Function ModelDef()
Local oModel := Nil
Local oStrTDX 	:= FWFormStruct(1,"TDX")
Local oStrTGW 	:= FWFormStruct(1,"TGW")
Local oStrTXH 	:= FWFormStruct(1,"TXH")
Local lDescLoc	:= oStrTXH:HasField('TXH_DESLOC')

xAux := FwStruTrigger( 'TXH_MANUT', 'TXH_DSMANU',;
	'Posicione("ABN",1,xFilial("ABN") + FwFldGet("TXH_MANUT")+"04","ABN_DESC")', .F. )
	oStrTXH:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])

xAux := FwStruTrigger( 'TXH_CODTFF', 'TXH_DSCTFF',;
	'At580GTgDs()', .F. )
	oStrTXH:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])

xAux := FwStruTrigger( 'TXH_CODTCU', 'TXH_DSTCU',;
	'Posicione("TCU",1,xFilial("TCU") + FwFldGet("TXH_CODTCU"),"TCU_DESC")', .F. )
	oStrTXH:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])
	
xAux := FwStruTrigger( 'TXH_MANUT', 'TXH_HORAIN',;
	'At580GTgHr("TXH_HORAIN")', .F. )
	oStrTXH:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])

xAux := FwStruTrigger( 'TXH_MANUT', 'TXH_HORAFI',;
	'At580GTgHr("TXH_HORAFI")', .F. )
	oStrTXH:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])

xAux := FwStruTrigger( 'TXH_CODTFF', 'TXH_HORAIN',;
	'At580GTgHr("TXH_HORAIN")', .F. )
	oStrTXH:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])

xAux := FwStruTrigger( 'TXH_CODTFF', 'TXH_HORAFI',;
	'At580GTgHr("TXH_HORAFI")', .F. )
	oStrTXH:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])

xAux := FwStruTrigger( 'TXH_CODTCU', 'TXH_HORAIN',;
	'At580GTgHr("TXH_HORAIN")', .F. )
	oStrTXH:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])

xAux := FwStruTrigger( 'TXH_CODTCU', 'TXH_HORAFI',;
	'At580GTgHr("TXH_HORAFI")', .F. )
	oStrTXH:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])

If lDescLoc
	xAux := FwStruTrigger( 'TXH_CODTFF', 'TXH_DESLOC',;
		'At580GDesL()', .F. )
		oStrTXH:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])	
EndIf

oModel := MPFormModel():New("TECA580G", /*bPreValidacao*/, {|oModel| At580GTdOk(oModel) }, /*bCommit*/, /*bCancel*/ )
oModel:AddFields("TDXMASTER", /*cOwner*/, oStrTDX, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )
oModel:AddGrid("TGWDETAIL", "TDXMASTER", oStrTGW,/*bLinePre*/, /*bLinePost*/,/*bPreVal*/,,{|oModel|AtLoadTGW(oModel)})
oModel:SetRelation("TGWDETAIL",{{"TGW_FILIAL","xFilial('TGW')"},{"TGW_EFETDX","TDX_COD"}},TGW->(IndexKey(1)))

oModel:AddGrid("TXHDETAIL", "TGWDETAIL", oStrTXH,/*bLinePre*/, /*bLinePost*/,/*bPreVal*/,,/*bCarga*/)
oModel:SetRelation("TXHDETAIL",{{"TXH_FILIAL","xFilial('TXH')"},{"TXH_CODPAI","TGW_COD"}},TXH->(IndexKey(2)))

oModel:SetDescription(STR0001) //"Manuten��es Programadas"

oStrTDX:SetProperty("*", MODEL_FIELD_OBRIGAT, .F.)
oStrTGW:SetProperty("*", MODEL_FIELD_OBRIGAT, .F.)

oStrTDX:SetProperty("*", MODEL_FIELD_WHEN,  {||.F.})
oStrTGW:SetProperty("*", MODEL_FIELD_WHEN,  {||.F.})

oStrTXH:SetProperty('TXH_MANUT' ,MODEL_FIELD_VALID,{|oModel,cField,xNewValue| At580GVMnt(oModel,cField,xNewValue)})
oStrTXH:SetProperty('TXH_CODTFF',MODEL_FIELD_VALID,{|oModel,cField,xNewValue| At580GVTff(oModel,cField,xNewValue)})
oStrTXH:SetProperty('TXH_CODTCU',MODEL_FIELD_VALID,{|oModel,cField,xNewValue| At580GVTcu(oModel,cField,xNewValue)})

If At580GMtFil()
	oStrTXH:SetProperty('TXH_MTFIL',MODEL_FIELD_VALID,{|oModel,cField,xNewValue| At580GVFil(oModel,cField,xNewValue)})
Endif

oModel:getModel('TXHDETAIL'):SetOptional(.T.)

oModel:SetActivate( {|oModel| InitDados( oModel ) } )

Return oModel
//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
	Defini��o da interface

@author	boiani
@since 29/05/2019
/*/
//-------------------------------------------------------------------
Static Function ViewDef()

Local oView := NIL
Local oModel := ModelDef()
Local oStrTDX := FWFormStruct( 2, "TDX" )
Local oStrTGW := FWFormStruct( 2, "TGW" )
Local oStrTXH := FWFormStruct( 2, "TXH" )

oView := FWFormView():New()
oView:SetModel(oModel)

oView:AddField('VIEW_TDX', oStrTDX, 'TDXMASTER' )
oView:AddGrid('VIEW_TGW' , oStrTGW, 'TGWDETAIL')
oView:AddGrid('VIEW_TXH' , oStrTXH, 'TXHDETAIL')

oStrTDX:RemoveField('TDX_QUANT')
oStrTGW:RemoveField('TDX_STATUS')
oStrTGW:RemoveField('TGW_COBTDX')
oStrTGW:RemoveField('TGW_COBTIP')
oStrTGW:RemoveField('TGW_EFETDX')
oStrTXH:RemoveField('TXH_CODPAI')
oStrTXH:RemoveField('TXH_CODIGO')

If At580GMtFil()
	oStrTXH:SetProperty( "TXH_MTFIL", MVC_VIEW_LOOKUP	, "SM0")
	oStrTXH:SetProperty( "TXH_MTFIL", MVC_VIEW_ORDEM	, "07" )
Endif

oView:CreateHorizontalBox( 'TOP'   , 18 )
oView:CreateHorizontalBox( 'MIDDLE', 45 )
oView:CreateHorizontalBox( 'BOTTOM', 37 )

oView:SetOwnerView( "VIEW_TDX", "TOP" )
oView:SetOwnerView( "VIEW_TGW", "MIDDLE" )
oView:SetOwnerView( "VIEW_TXH", "BOTTOM" )

oView:EnableTitleView('VIEW_TGW',STR0004) //"Dia Trabalhado"
oView:EnableTitleView('VIEW_TXH',STR0005) //"Manuten��o de Hora Extra"

oView:AddUserButton(STR0040, "", { || At580GRpl()  }) //"Replicar Manuten��es"
oView:AddUserButton(STR0049,"", {|| At580DlMt()}) //"Excluir manuten��es"

Return oView
//-------------------------------------------------------------------
/*/{Protheus.doc} AtLoadTGW
	Bloco de load da grid TDX

@author	boiani
@since 29/05/2019
/*/
//-------------------------------------------------------------------
Function AtLoadTGW(oMdl)
Local aRet := {}
Local cAliasTGW := GetNextAlias()
Local nLenFlds := 0
Local aAux := {}
Local oModel := oMdl:GetModel()
Local oMdlTDX := oModel:GetModel('TDXMASTER')
Local cCodTDX := oMdlTDX:GetValue('TDX_COD')
Local oStru   := oMdl:GetStruct()
Local nI := 1
Local aAreaX3 := SX3->(GetArea())
Local aFields := {}

BeginSql Alias cAliasTGW
	SELECT * FROM  %table:TGW% TGW
			WHERE TGW.TGW_FILIAL = %xFilial:TGW%
				AND TGW.TGW_EFETDX = %Exp:cCodTDX%
				AND TGW.TGW_STATUS = '1'
				AND TGW.%notDel%
EndSql

If (cAliasTGW)->(!Eof())

	aFields := oStru:GetFields()
	nLenFlds := Len(aFields)
	SX3->(DbSetOrder(2))
	
	While (cAliasTGW)->(!Eof())
		TGW->(DbGoTo((cAliasTGW)->R_E_C_N_O_))
		aAux := Array(nLenFlds)
		
		For nI := 1 To nLenFlds
			cField := aFields[nI, MODEL_FIELD_IDFIELD]
			If !aFields[nI, MODEL_FIELD_VIRTUAL]

				If aFields[nI, MODEL_FIELD_TIPO] $ 'C|N|L'
					aAux[nI] := (cAliasTGW)->&(cField)
				ElseIf aFields[nI, MODEL_FIELD_TIPO] == 'M'
					aAux[nI] := TGW->&(cField)
				ElseIf aFields[nI, MODEL_FIELD_TIPO] == 'D'
					aAux[nI] := STOD((cAliasTGW)->&(cField))
				EndIf
			EndIf

		Next nI
		Aadd(aRet,{(cAliasTGW)->R_E_C_N_O_,aAux})
		(cAliasTGW)->(DbSkip())
	EndDo
EndIf
RestArea(aAreaX3)
(cAliasTGW)->(DbCloseArea())

Return aRet
//-------------------------------------------------------------------
/*/{Protheus.doc} At580GCampo
	Adiciona os campos "Descri��o da Escala" e "Sequ�ncia" no Browse

@author	boiani
@since 29/05/2019
/*/
//-------------------------------------------------------------------
Static Function At580GCampo()
Local aRet := {}

aAdd( aRet, { STR0006 	,{ || (POSICIONE("TDW",1,xFilial("TDW")+TDX->TDX_CODTDW, "TDW_DESC")) } , "C" , Alltrim(GetSx3Cache( "TDW_DESC", "X3_PICTURE" )) , 0 , TamSX3("TDW_DESC")[1] , 0 , , , , , , , } ) //"Descri��o da Escala"
aAdd( aRet, { STR0007 	,{ || (TDX->TDX_SEQTUR) } , "C" , Alltrim(GetSx3Cache( "TDX_SEQTUR", "X3_PICTURE" )) , 0 , TamSX3("TDX_SEQTUR")[1] , 0 , , , , , , , } ) //"Sequ�ncia"


Return aRet
//-------------------------------------------------------------------
/*/{Protheus.doc} InitDados

@description Bloco de c�digo executado no activate
@param oModel, obj, modelo em ativa��o

@author	boiani
@since	26/11/2019
/*/
//-------------------------------------------------------------------
Static Function InitDados(oModel)
Local aArea 		:= GetArea()
Local aSaveLines 	:= FWSaveRows()
Local cFilBkp	    := cFilAnt
Local lMtFilTFF 	:= At580GMtFil()
Local nX
Local nY
Local oMdlTXH 		:= oModel:GetModel("TXHDETAIL")
Local oMdlTGW 		:= oModel:GetModel("TGWDETAIL")
Local lDescLoc		:= oMdlTXH:HasField('TXH_DESLOC')

For nY := 1 To oMdlTGW:Length()
	oMdlTGW:GoLine(nY)
	For nX := 1 To oMdlTXH:Length()
		oMdlTXH:GoLine(nX)
		If !EMPTY(oMdlTXH:GetValue("TXH_MANUT"))
			oMdlTXH:LoadValue('TXH_DSMANU',Posicione("ABN",1,xFilial("ABN") + oMdlTXH:GetValue("TXH_MANUT")+"04","ABN_DESC"))
		EndIf
		If !EMPTY(oMdlTXH:GetValue("TXH_CODTFF"))
			If lMtFilTFF .And. !Empty(oMdlTXH:GetValue("TXH_MTFIL")) .And. cFilAnt <> oMdlTXH:GetValue("TXH_MTFIL")
				cFilAnt := oMdlTXH:GetValue("TXH_MTFIL")
			Endif
			oMdlTXH:LoadValue('TXH_DSCTFF',Posicione("SB1",1,xFilial("SB1") + Posicione("TFF",1,xFilial("TFF") + oMdlTXH:GetValue("TXH_CODTFF"),"TFF_PRODUT"),"B1_DESC"))
			If lDescLoc
				oMdlTXH:LoadValue('TXH_DESLOC',Posicione("ABS",1,xFilial("ABS") + Posicione("TFF",1,xFilial("TFF") + oMdlTXH:GetValue("TXH_CODTFF"),"TFF_LOCAL"),"ABS_DESCRI"))
			EndIf
			If lMtFilTFF .And. !Empty(oMdlTXH:GetValue("TXH_MTFIL")) .And. cFilBkp <> cFilAnt
				cFilAnt := cFilBkp
			Endif
		EndIf
		If !EMPTY(oMdlTXH:GetValue("TXH_CODTCU"))
			oMdlTXH:LoadValue('TXH_DSTCU',Posicione("TCU",1,xFilial("TCU") + oMdlTXH:GetValue("TXH_CODTCU"),"TCU_DESC"))
		EndIf
	Next nX
Next nY

FWRestRows( aSaveLines )
RestArea(aArea)

Return
//-------------------------------------------------------------------
/*/{Protheus.doc} At58gGera

@description Executa a cria��o das manuten��es de H.E. dentro de uma barra de load
@author	boiani
@since	26/11/2019
/*/
//-------------------------------------------------------------------
Function At58gGera(aAgendas,cEscala,cCodTFF)
Local nTotal 	:= 0
Local oDlg		:= nil
Local oSayMtr	:= nil
Local oMeter	:= nil
Local nMeter	:= 0
Local aManut	:= {}

Default aAgendas := {}
nTotal := LEN(aAgendas)

If isBlind()
	aManut := LoadHE(aAgendas,cEscala,cCodTFF)
Else
	DEFINE MSDIALOG oDlg FROM 0,0 TO 5,60 TITLE STR0008 //"Carregando manuten��es programadas"
		oSayMtr := tSay():New(10,10,{||STR0009},oDlg,,,,,,.T.,,,220,20) //"Processando, aguarde..."
		oMeter  := tMeter():New(20,10,{|u|if(Pcount()>0,nMeter:=u,nMeter)},nTotal,oDlg,220,10,,.T.)
		
	ACTIVATE MSDIALOG oDlg CENTERED ON INIT (aManut := LoadHE(aAgendas,cEscala,cCodTFF,@oDlg,@oMeter))
EndIf

nTotal 	:= LEN(aManut)
oDlg	:= nil
oSayMtr	:= nil
nMeter	:= 0

If isBlind()
	InsertHE(aManut)
Else
	DEFINE MSDIALOG oDlg FROM 0,0 TO 5,60 TITLE STR0010 //"Inserindo manuten��es programadas"
		oSayMtr := tSay():New(10,10,{||STR0009},oDlg,,,,,,.T.,,,220,20) //"Processando, aguarde..."
		oMeter  := tMeter():New(20,10,{|u|if(Pcount()>0,nMeter:=u,nMeter)},nTotal,oDlg,220,10,,.T.)
		
	ACTIVATE MSDIALOG oDlg CENTERED ON INIT ( InsertHE(aManut,@oDlg,@oMeter) )
EndIf

Return
//-------------------------------------------------------------------
/*/{Protheus.doc} LoadHE

@description Compara as agendas inseridas com o cadastro de manuten��es
	planejadas e retorna em um array de duas posi��es Agendas x Manut.Planejada
@author	boiani
@since	26/11/2019
/*/
//-------------------------------------------------------------------
Static Function LoadHE(aAgendas,cEscala,cCodTFF,oDlg,oMeter)
Local lLoadBar := .F.
Local cQry := GetNextAlias()
Local aMntTXH := {} 
Local nX
Local nAux
Local aRet := {}
Local cSpaceTCU := SPACE(TamSX3("TCU_COD")[1])
Local cSpaceTFF := SPACE(TamSX3("TFF_COD")[1]) 
Local lGsGeHor	:= SuperGetMV('MV_GSGEHOR',,.F.)
Local cSlcGsHr	:= ""
Local cLeftGsHr := ""
Local cCodAtd	:= ""
Local dDtIniRf 	:= sTod("")
Local dDtFimRf 	:= sTod("")
Local cSql		:= ""
Local lMtFilTFF := At580GMtFil()
Local lMV_GSLOG   := SuperGetMV('MV_GSLOG',,.F.)
Local oGsLog	  := nil

Default oDlg 	:= nil
Default oMeter 	:= nil

lLoadBar := !isBlind() .AND. oMeter != nil .AND. oDlg != nil

If lMV_GSLOG
	oGsLog  := GsLog():new()
	oGsLog:addLog("TECA580GLoadHE", STR0054 + cEscala ) //"Escala: "
	oGsLog:addLog("TECA580GLoadHE", STR0055 + cCodTFF ) //"C�digo da TFF: "
EndIf

If lGsGeHor
	DbSelectArea("TGY")
	If (lGsGeHor := TGY->(ColumnPos("TGY_ENTRA1"))�>�0)

		If !Empty(aAgendas)
			cCodAtd 	:= aAgendas[1,3]
			dDtIniRf 	:= aAgendas[1,10]
			dDtFimRf 	:= aAgendas[Len(aAgendas),10]
			If lMV_GSLOG
				oGsLog:addLog("TECA580GLoadHE", STR0056 + cCodAtd ) //"Atendente: "
				oGsLog:addLog("TECA580GLoadHE", STR0057 + DTOC(dDtIniRf) ) //"Data In�cio: "
				oGsLog:addLog("TECA580GLoadHE", STR0058 + DTOC(dDtFimRf) ) //"Data Fim: "
			EndIf
		Endif

		cSlcGsHr := ",TGY.TGY_ENTRA1,"
		cSlcGsHr += " TGY.TGY_SAIDA1,"
		cSlcGsHr += " TGY.TGY_ENTRA2,"
		cSlcGsHr += " TGY.TGY_SAIDA2,"
		cSlcGsHr += " TGY.TGY_ENTRA3,"
		cSlcGsHr += " TGY.TGY_SAIDA3,"
		cSlcGsHr += " TGY.TGY_ENTRA4,"
		cSlcGsHr += " TGY.TGY_SAIDA4 "
				
		cLeftGsHr := " JOIN "+RetSqlName("TGY")+" TGY ON TGY.TGY_FILIAL = '" + xFilial("TGY") + "'"
		cLeftGsHr +=	" AND TGY.TGY_ESCALA = TDX.TDX_CODTDW"
		cLeftGsHr +=	" AND TGY.TGY_CODTFF = '" + cCodTFF + "'"
		cLeftGsHr +=	" AND TGY.TGY_ATEND  = '" + cCodAtd + "'"
		cLeftGsHr +=	" AND '" + dTos(dDtIniRf) + "' BETWEEN TGY.TGY_DTINI AND TGY.TGY_DTFIM"
		cLeftGsHr +=	" AND '" + dTos(dDtFimRf) + "' BETWEEN TGY.TGY_DTINI AND TGY.TGY_DTFIM"
		cLeftGsHr +=	" AND TGY.TGY_ULTALO <> ''"
		cLeftGsHr +=	" AND TGY.D_E_L_E_T_ = ''"

	Endif
Endif

cSql += "SELECT TXH.TXH_CODTCU, "
cSql +=	"TXH.TXH_CODTFF,"
cSql +=	"TXH.TXH_HORAFI,"
cSql +=	"TXH.TXH_HORAIN,"
cSql +=	"TXH.TXH_MANUT,"
cSql +=	"TDX.TDX_COD,"
cSql +=	"TDX.TDX_SEQTUR,"
cSql +=	"TGW.TGW_DIASEM,"
cSql +=	"TGW.TGW_HORINI,"
cSql +=	"TGW.TGW_HORFIM "
If !Empty(cSlcGsHr)
	cSql += cSlcGsHr + " "
EndIf
If lMtFilTFF 
	cSql +=	",TXH.TXH_MTFIL "
Endif
cSql += "FROM "+retSqlName("TXH")+" TXH "
cSql += "JOIN "+retSqlName("TGW")+" TGW "
cSql += "ON TGW.TGW_FILIAL = '"+xFilial('TGW')+"' AND "
cSql += "TGW.TGW_COD = TXH.TXH_CODPAI "
cSql += "JOIN "+retSqlName("TDX")+" TDX ON "
cSql += "TDX.TDX_FILIAL = '"+xFilial('TDX')+"' AND "
cSql += "TDX.TDX_COD = TGW.TGW_EFETDX "
If !Empty(cLeftGsHr)
	cSql += cLeftGsHr
EndIf
cSql += "WHERE TXH.TXH_FILIAL = '"+xFilial('TXH')+"' "
cSql += "AND TXH.D_E_L_E_T_ = ' ' "
cSql += "AND TGW.D_E_L_E_T_ = ' ' "
cSql += "AND TDX.TDX_CODTDW = '"+cEscala+"' "

If lMtFilTFF
	cSql += "AND ( TXH.TXH_MTFIL = '"+cFIlAnt+"' OR TXH.TXH_MTFIL = '' ) "
	cSql += "ORDER BY TXH.TXH_MTFIL DESC "
Endif

cSql := ChangeQuery(cSql)
cAliasQry := GetNextAlias()
dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cQry, .F., .T.)

If lMV_GSLOG
	oGsLog:addLog("TECA580GLoadHE", REPLICATE("-",20) + CRLF)
EndIf

While !(cQry)->(EOF())
	
	If lGsGeHor .And. !Empty((cQry)->(TGY_ENTRA1))

		nAux := At580GEnSa(	(cQry)->(TDX_COD)	,;
							(cQry)->(TGW_DIASEM),;
							(cQry)->(TGW_HORINI),;
							(cQry)->(TGW_HORFIM) )

		cHrIni	:= (cQry)->(&("TGY_ENTRA"+cValToChar(nAux)))
		cHrFim	:= (cQry)->(&("TGY_SAIDA"+cValToChar(nAux)))

		If (cQry)->(TGW_HORINI) <> TecConvHr((cQry)->(TXH_HORAIN))
			cHrIni := TecConvHr((TecConvHr((cQry)->(TXH_HORAIN))-(cQry)->(TGW_HORINI))+TecConvHr(cHrIni))
		Endif
		
		If (cQry)->(TGW_HORFIM) <> TecConvHr((cQry)->(TXH_HORAFI))
			cHrFim := TecConvHr((TecConvHr((cQry)->(TXH_HORAFI))-(cQry)->(TGW_HORFIM))+TecConvHr(cHrFim))
		Endif
				
		AADD(aMntTXH, {(cQry)->(TXH_CODTCU),;	//[1]
						(cQry)->(TXH_CODTFF),;	//[2]
						cHrFim,;				//[3]
						cHrIni,;				//[4]
						(cQry)->(TXH_MANUT),;	//[5]
						(cQry)->(TDX_SEQTUR),;	//[6]
						(cQry)->(TGW_DIASEM),;	//[7]
						TecConvHr((cQry)->(&("TGY_ENTRA"+cValToChar(nAux)))),;	//[8]
						TecConvHr((cQry)->(&("TGY_SAIDA"+cValToChar(nAux)))),;  //[9]
						Iif(lMtFilTFF ,(cQry)->(TXH_MTFIL),"")})	//10

	Else
		AADD(aMntTXH, {(cQry)->(TXH_CODTCU),;	//[1]
						(cQry)->(TXH_CODTFF),;	//[2]
						(cQry)->(TXH_HORAFI),;	//[3]
						(cQry)->(TXH_HORAIN),;	//[4]
						(cQry)->(TXH_MANUT),;	//[5]
						(cQry)->(TDX_SEQTUR),;	//[6]
						(cQry)->(TGW_DIASEM),;	//[7]
						(cQry)->(TGW_HORINI),;	//[8]
						(cQry)->(TGW_HORFIM),;	//[9]
						Iif(lMtFilTFF ,(cQry)->(TXH_MTFIL),"")})	//10

	Endif
	(cQry)->(DbSkip())

	If lMV_GSLOG
		oGsLog:addLog("TECA580GLoadHE", "TXH_CODTCU: " + aMntTXH[LEN(aMntTXH)][1] )
		oGsLog:addLog("TECA580GLoadHE", "TXH_CODTFF: " + aMntTXH[LEN(aMntTXH)][2] )
		oGsLog:addLog("TECA580GLoadHE", "TXH_HORAFI: " + aMntTXH[LEN(aMntTXH)][3] )
		oGsLog:addLog("TECA580GLoadHE", "TXH_HORAIN: " + aMntTXH[LEN(aMntTXH)][4] )
		oGsLog:addLog("TECA580GLoadHE", "TXH_MANUT: " + aMntTXH[LEN(aMntTXH)][5] )
		oGsLog:addLog("TECA580GLoadHE", "TDX_SEQTUR: " + aMntTXH[LEN(aMntTXH)][6] )
		oGsLog:addLog("TECA580GLoadHE", "TGW_DIASEM: " + aMntTXH[LEN(aMntTXH)][7] )
		oGsLog:addLog("TECA580GLoadHE", "TGW_HORINI: " + cValToChar(aMntTXH[LEN(aMntTXH)][8]) )
		oGsLog:addLog("TECA580GLoadHE", "TGW_HORFIM: " + cValToChar(aMntTXH[LEN(aMntTXH)][9]) )
		oGsLog:addLog("TECA580GLoadHE", "TXH_MTFIL: " + aMntTXH[LEN(aMntTXH)][10] )
		oGsLog:addLog("TECA580GLoadHE", REPLICATE("-",20) + CRLF)
	EndIf

EndDo 

(cQry)->(DbCloseArea())
/*
aAgendas[x]
	[x][01] = RECNO())
	[x][02] = ABB->ABB_CODIGO
	[x][03] = ABB->ABB_CODTEC
	[x][04] = ABB->ABB_HRINI
	[x][05] = ABB->ABB_HRFIM
	[x][06] = ABB->ABB_TIPOMV
	[x][07] = ABB->ABB_DTINI
	[x][08] = ABB->ABB_DTFIM
	[x][09] = Sequ�ncia
	[x][10] = TDV_DTREF
*/
For nX := 1 To LEN(aAgendas)
	nAux := 0
	If lMV_GSLOG
		oGsLog:addLog("TECA580GLoadHE", STR0059 + aAgendas[nX][9] ) //"Sequ�ncia: "
		oGsLog:addLog("TECA580GLoadHE", STR0060 + aAgendas[nX][4] ) //"Hora Ini.: "
		oGsLog:addLog("TECA580GLoadHE", STR0061 + aAgendas[nX][5] ) //"Hora Fim.: "
		oGsLog:addLog("TECA580GLoadHE", STR0062 + DTOC(aAgendas[nX][10]) ) //"Data: "
		oGsLog:addLog("TECA580GLoadHE", STR0063 + aAgendas[nX][6] ) //"C�d. Movimenta��o: "
		oGsLog:addLog("TECA580GLoadHE", CRLF)
	EndIf
	If ASCAN(aMntTXH, {|a| a[6] == aAgendas[nX][9] .AND.;
							TecNumToHr(a[8]) == aAgendas[nX][4] .AND.;
							TecNumToHr(a[9]) == aAgendas[nX][5] .AND.;
							cValToChar(DOW(aAgendas[nX][10])) == a[7] }) > 0

		If (nAux := ASCAN(aMntTXH, {|a| TecNumToHr(a[8]) == aAgendas[nX][4] .AND.;
		 								TecNumToHr(a[9]) == aAgendas[nX][5] .AND.;
		 								cValToChar(DOW(aAgendas[nX][10])) == a[7] .AND.;
		 								cCodTFF == a[2] .AND.;
		 								aAgendas[nX][6] == a[1] .AND.;
		 								a[6] == aAgendas[nX][9]})) == 0

		 	If (nAux := ASCAN(aMntTXH, {|a| TecNumToHr(a[8]) == aAgendas[nX][4] .AND.;
		 								TecNumToHr(a[9]) == aAgendas[nX][5] .AND.;
		 								cValToChar(DOW(aAgendas[nX][10])) == a[7] .AND.;
		 								cCodTFF == a[2] .AND.;
		 								(cSpaceTCU == a[1] .OR. EMPTY(a[1])) .AND.;
		 								a[6] == aAgendas[nX][9]})) == 0
			 	
				 If (nAux := ASCAN(aMntTXH, {|a| TecNumToHr(a[8]) == aAgendas[nX][4] .AND.;
			 								TecNumToHr(a[9]) == aAgendas[nX][5] .AND.;
			 								cValToChar(DOW(aAgendas[nX][10])) == a[7] .AND.;
			 								(cSpaceTFF == a[2] .OR. EMPTY(a[2])) .AND.;
			 								aAgendas[nX][6] == a[1] .AND.;
			 								a[6] == aAgendas[nX][9]})) == 0
			 	
				 	nAux := ASCAN(aMntTXH, {|a| TecNumToHr(a[8]) == aAgendas[nX][4] .AND.;
			 								TecNumToHr(a[9]) == aAgendas[nX][5] .AND.;
			 								cValToChar(DOW(aAgendas[nX][10])) == a[7] .AND.;
			 								(cSpaceTFF == a[2] .OR. EMPTY(a[2])) .AND.;
			 								(cSpaceTCU == a[1] .OR. EMPTY(a[1])) .AND.;
			 								a[6] == aAgendas[nX][9]})
			 	Endif
		 	EndIf
		EndIf
	EndIf
	If nAux > 0
		AADD(aRet, {aAgendas[nX], aMntTXH[nAux]})
		If lMV_GSLOG
			oGsLog:addLog("TECA580GLoadHE", STR0064 + DTOC(aAgendas[nX][10]) + STR0065 +CRLF) //"Agenda " ## " adicionada para processamento."
		EndIf
	EndIf
	If lLoadBar
		oMeter:Set(nX)
		oMeter:Refresh()
	EndIf
Next nX

If lLoadBar
	oDlg:End()
EndIf

If lMV_GSLOG
	oGsLog:printLog("TECA580GLoadHE")
EndIf

Return aRet
//-------------------------------------------------------------------
/*/{Protheus.doc} InsertHE

@description Insere as manuten��es de H.E. 
@author	boiani
@since	26/11/2019
/*/
//-------------------------------------------------------------------
Static Function InsertHE(aManut,oDlg,oMeter)
Local nX
Local nY
Local oMdl550
Local oModel := FwModelActive()
Local nDiasINI := 0
Local nDiasFIM := 0
Local nFail := 0
Local aErrors := {}
Local aErroMVC := {}
Local cMsg := ""
Local lMtFilTFF := At580GMtFil()
Local cFilBkp := cFilAnt
Default oDlg := nil
Default oMeter := nil

lLoadBar := !isBlind() .AND. oMeter != nil .AND. oDlg != nil

If !EMPTY(aManut)
	oMdl550 := FwLoadModel("TECA550")
EndIf

For nX := 1 To Len(aManut)
	
	If lMtFilTFF .And. !Empty(aManut[nX][2][10]) .And. cFilAnt <> aManut[nX][2][10]
		cFilBkp	:= cFilAnt
		cFilAnt := aManut[nX][2][10]
	Endif
	
	nDiasFIM := 0
	nDiasINI := 0
	
	IF VAL(STRTRAN(aManut[nX][2][4],":",".")) > aManut[nX][2][8]
		nDiasINI := -1
	EndIF
	IF VAL(STRTRAN(aManut[nX][2][3],":",".")) < aManut[nX][2][9]
		nDiasFIM := 1
	EndIF
	ABB->(DbGoTo(aManut[nX][1][1]))

	ABQ->(DbSetOrder(1))
	ABQ->(MsSeek(xFilial("ABQ") + ABB->ABB_IDCFAL))

	TFF->(DbSetOrder(1))
	TFF->(MsSeek(xFilial("TFF") + ABQ->ABQ_CODTFF))
	
	At550SetAlias("ABB")
	At550SetGrvU(.T.)

	oMdl550:SetOperation( MODEL_OPERATION_INSERT)
	If (lRet := oMdl550:Activate())
		lRet := lRet .AND. oMdl550:SetValue("ABRMASTER","ABR_MOTIVO", aManut[nX][2][5])
		lRet := lRet .AND. oMdl550:SetValue("ABRMASTER","ABR_DTINI", (aManut[nX][1][7] + nDiasINI))
		lRet := lRet .AND. oMdl550:SetValue("ABRMASTER","ABR_HRINI", aManut[nX][2][4] )
		lRet := lRet .AND. oMdl550:SetValue("ABRMASTER","ABR_DTFIM", (aManut[nX][1][8] + nDiasFIM))
		lRet := lRet .AND. oMdl550:SetValue("ABRMASTER","ABR_HRFIM", aManut[nX][2][3] )
		If lRet
			Begin Transaction
				If !oMdl550:VldData() .OR. !oMdl550:CommitData()
					nFail++
					aErroMVC := oMdl550:GetErrorMessage()
					AADD(aErrors, {	 STR0011 + ' [' + AllToChar( aErroMVC[1] ) + ']',;	//"Id do formul�rio de origem:"
									 STR0012 + ' [' + AllToChar( aErroMVC[2] ) + ']',;	//"Id do campo de origem:"
									 STR0013 + ' [' + AllToChar( aErroMVC[3] ) + ']',;	//"Id do formul�rio de erro:"
									 STR0014 + ' [' + AllToChar( aErroMVC[4] ) + ']',;	//"Id do campo de erro:"
									 STR0015 + ' [' + AllToChar( aErroMVC[5] ) + ']',;	//"Id do erro:"
									 STR0016 + ' [' + AllToChar( aErroMVC[6] ) + ']',;	//"Mensagem do erro:"
									 STR0017 + ' [' + AllToChar( aErroMVC[7] ) + ']',;	//"Mensagem da solu��o:"
									 STR0018 + ' [' + AllToChar( aErroMVC[8] ) + ']',;	//"Valor atribu�do:"
									 STR0019 + ' [' + AllToChar( aErroMVC[9] ) + ']';	//"Valor anterior:"
									 })
					DisarmTransacation()
				EndIf
			End Transaction
		Else
			nFail++
			aErroMVC := oMdl550:GetErrorMessage()
			AADD(aErrors, {	 STR0011 + ' [' + AllToChar( aErroMVC[1] ) + ']',;	//"Id do formul�rio de origem:"
							 STR0012 + ' [' + AllToChar( aErroMVC[2] ) + ']',;	//"Id do campo de origem:"
							 STR0013 + ' [' + AllToChar( aErroMVC[3] ) + ']',;	//"Id do formul�rio de erro:"
							 STR0014 + ' [' + AllToChar( aErroMVC[4] ) + ']',;	//"Id do campo de erro:"
							 STR0015 + ' [' + AllToChar( aErroMVC[5] ) + ']',;	//"Id do erro:"
							 STR0016 + ' [' + AllToChar( aErroMVC[6] ) + ']',;	//"Mensagem do erro:"
							 STR0017 + ' [' + AllToChar( aErroMVC[7] ) + ']',;	//"Mensagem da solu��o:"
							 STR0018 + ' [' + AllToChar( aErroMVC[8] ) + ']',;	//"Valor atribu�do:"
							 STR0019 + ' [' + AllToChar( aErroMVC[9] ) + ']';	//"Valor anterior:"
							 })
		EndIf
	Else
		nFail++
		aErroMVC := oMdl550:GetErrorMessage()
		AADD(aErrors, {	 STR0011 + ' [' + AllToChar( aErroMVC[1] ) + ']',;	//"Id do formul�rio de origem:"
						 STR0012 + ' [' + AllToChar( aErroMVC[2] ) + ']',;	//"Id do campo de origem:"
						 STR0013 + ' [' + AllToChar( aErroMVC[3] ) + ']',;	//"Id do formul�rio de erro:"
						 STR0014 + ' [' + AllToChar( aErroMVC[4] ) + ']',;	//"Id do campo de erro:"
						 STR0015 + ' [' + AllToChar( aErroMVC[5] ) + ']',;	//"Id do erro:"
						 STR0016 + ' [' + AllToChar( aErroMVC[6] ) + ']',;	//"Mensagem do erro:"
						 STR0017 + ' [' + AllToChar( aErroMVC[7] ) + ']',;	//"Mensagem da solu��o:"
						 STR0018 + ' [' + AllToChar( aErroMVC[8] ) + ']',;	//"Valor atribu�do:"
						 STR0019 + ' [' + AllToChar( aErroMVC[9] ) + ']';	//"Valor anterior:"
						 })
	EndIf
	If lLoadBar
		oMeter:Set(nX)
		oMeter:Refresh()
	EndIf
	oMdl550:DeActivate()

	If lMtFilTFF .And. !Empty(aManut[nX][2][10]) .And. cFilAnt <> cFilBkp
		cFilAnt := cFilBkp
	Endif

Next nX

If lLoadBar
	oDlg:End()
EndIf

If VALTYPE(oModel) == 'O'
	FwModelActive(oModel)
Endif

If !EMPTY(aErrors)
	cMsg += STR0020 + " " + cValToChar(Len(aManut)) + CRLF	//"Total de Manuten��es Programadas processadas:"
	cMsg += STR0021 + " " + cValToChar(Len(aManut) - nFail) + CRLF	//"Total de Manuten��es Programadas inclu�das:"
	cMsg += STR0022 + " " + cValToChar(nFail) + CRLF + CRLF	//"Total de Manuten��es Programadas n�o inclu�das:"
	cMsg += STR0023 + CRLF + CRLF	//"As Manuten��es Programadas abaixo n�o foram inseridas: "
	For nX := 1 To LEN(aErrors)
		For nY := 1 To LEN(aErrors[nX])
			cMsg += aErrors[nX][nY] + CRLF
		Next
		cMsg += CRLF + REPLICATE("-",30) + CRLF
	Next
	If !ISBlind()
		AtShowLog(cMsg,STR0001,/*lVScroll*/,/*lHScroll*/,/*lWrdWrap*/,.F.)	//"Manuten��es Programadas"
	EndIf
EndIf

At550Reset()
At550SetAlias("")
At550SetGrvU(.F.)

Return aErrors
//-------------------------------------------------------------------
/*/{Protheus.doc} At580GVMnt

@description Valid do campo TXH_MANUT
@author	boiani
@since	26/11/2019
/*/
//-------------------------------------------------------------------
Function At580GVMnt(oModel,cField,xNewValue)
Local lRet := .T.
Local aArea := GetArea()
Default xNewValue := ""

If !EMPTY(xNewValue)
	ABN->(DbSetOrder(1))
	If !(lRet := (ABN->(DbSeek(xFilial("ABN")+xNewValue+"04"))))
		Help( " ", 1, "At580GVMnt", , STR0024, 1 ) //"Necess�rio informar um c�digo de manuten��o (ABN_CODIGO) correspondente a uma Hora Extra"
	EndIf
EndIf
RestArea(aArea)
Return lRet
//-------------------------------------------------------------------
/*/{Protheus.doc} At580GVTcu

@description Valid do campo TXH_CODTCU
@author	boiani
@since	26/11/2019
/*/
//-------------------------------------------------------------------
Function At580GVTcu(oModel,cField,xNewValue)
Local lRet := .T.
Local aArea := GetArea()
Default xNewValue := ""

If !EMPTY(xNewValue)
	TCU->(DbSetOrder(1))
	If !(lRet := (TCU->(DbSeek(xFilial("TCU")+xNewValue))) .AND. TCU->TCU_EXALOC == '1')
		Help( " ", 1, "At580GVTcu", , STR0025, 1 ) //"Necess�rio informar um c�digo de movimenta��o (TCU_COD) de aloca��o (TCU_EXALOC = 1)"
	EndIf
EndIf
RestArea(aArea)
Return lRet
//-------------------------------------------------------------------
/*/{Protheus.doc} At580GVTff

@description Valid do campo TXH_CODTFF
@author	boiani
@since	26/11/2019
/*/
//-------------------------------------------------------------------
Function At580GVTff(oModel,cField,xNewValue)
Local lRet := .T.
Local cQry := GetNextAlias()
Local aArea := GetArea()
Local cSpcCTR := Space(TamSx3("CN9_NUMERO")[1])
Local cFilBkp := cFilAnt
Local lMtFilTFF  := At580GMtFil() .And. !Empty(oModel:GetValue("TXH_MTFIL")) 

Default xNewValue := ""

If !EMPTY(xNewValue)

	If lMtFilTFF .And. cFilAnt <> oModel:GetValue("TXH_MTFIL")
		cFilBkp := cFilAnt
		cFilAnt := oModel:GetValue("TXH_MTFIL")
	Endif

	BeginSQL Alias cQry
		SELECT 1
		FROM %Table:TFF% TFF
		JOIN %Table:TFL% TFL ON
			TFF.TFF_CODPAI = TFL.TFL_CODIGO AND
			TFL.TFL_FILIAL = %xFilial:TFL% AND
			TFL.%NotDel%
		JOIN %Table:TFJ% TFJ ON
			TFL.TFL_CODPAI = TFJ.TFJ_CODIGO AND
			TFJ.TFJ_FILIAL = %xFilial:TFJ% AND
			TFJ.%NotDel%
		WHERE TFF_FILIAL = %xFilial:TFF%
			AND TFF_COD = %Exp:xNewValue%
			AND TFJ.TFJ_STATUS = '1'
			AND TFJ.TFJ_CONTRT != %Exp:cSpcCTR% 
			AND TFF.%NotDel%
	EndSQL
	
	If !(lRet := !((cQry)->(EOF())))
		Help( " ", 1, "At580GVTff", , STR0026, 1 ) //"C�digo do posto (TFF_COD) inv�lido."
	EndIf
	
	(cQry)->(DbCloseArea())

	If lMtFilTFF .And. cFilAnt <> cFilBkp
		cFilAnt := cFilBkp
	Endif

EndIf

RestArea(aArea)
Return lRet
//-------------------------------------------------------------------
/*/{Protheus.doc} At580GTgHr

@description Inicializa os campos TXH_HORAIN e TXH_HORAFI automaticamente
	com o mesmo valor da Escala. 
@author	boiani
@since	26/11/2019
/*/
//-------------------------------------------------------------------
Function At580GTgHr(cField)
Local cRet := ""
Local oModel := FwModelActive()
Local oMdlTGW := oModel:GetModel("TGWDETAIL") 

If !EMPTY(FwFldGet(cField))
	cRet := FwFldGet(cField)
Else
	If cField == "TXH_HORAIN"
		cRet := TecNumToHr(oMdlTGW:GetValue("TGW_HORINI"))
	ElseIF cField == "TXH_HORAFI"
		cRet := TecNumToHr(oMdlTGW:GetValue("TGW_HORFIM"))
	EndIf
Endif

Return cRet
//-------------------------------------------------------------------
/*/{Protheus.doc} At580gRF3

@description Variavel Static utilizada no F3 do C�digo do posto
@author	boiani
@since	26/11/2019
/*/
//-------------------------------------------------------------------
Function At580gRF3()

Return cRetF3
//-------------------------------------------------------------------
/*/{Protheus.doc} At580gCons

@description Monta a consulta padr�o (F3) espec�fica
@author	boiani
@since	26/11/2019
/*/
//-------------------------------------------------------------------
Function At580gCons(cTipo)
Local cSpcCTR := Space(TamSx3("CN9_NUMERO")[1])
Local cTitle
Local aSeek := {}
Local aIndex := {}
Local cQry
Local cAls := GetNextAlias()
Local nSuperior
Local nEsquerda
Local nInferior
Local nDireita
Local oDlgEscTela
Local oBrowse
Local lRet := .F.
Local oModel := FwModelActive()
Local cEscala := oModel:GetValue("TDXMASTER","TDX_CODTDW")
Local cFilBkp := cFilAnt
Local lMtFilTFF  := At580GMtFil() .And. !Empty(oModel:GetValue("TXHDETAIL","TXH_MTFIL")) 

If cTipo == "TFF"
	cTitle := STR0027 //"Posto de Trabalho"

	If lMtFilTFF .And. cFilAnt <> oModel:GetValue("TXHDETAIL","TXH_MTFIL")
		cFilBkp := cFilAnt
		cFilAnt := oModel:GetValue("TXHDETAIL","TXH_MTFIL")
	Endif
	
	Aadd( aSeek, { STR0028, {{"","C",TamSX3("TFF_COD")[1],0,STR0028,,}} } )		//"C�digo do Posto" # "C�digo do Posto"
	Aadd( aSeek, { STR0029, {{"","C",TamSX3("B1_COD")[1],0,STR0029,,}} } )		//"C�digo do Produto" # "C�digo do Produto"
	Aadd( aSeek, { STR0030, {{"","C",TamSX3("B1_DESC")[1],0,STR0030,,}} } )		//"Descri��o" # "Descri��o"
	Aadd( aSeek, { STR0031, {{"","C",TamSX3("TFF_CONTRT")[1],0,STR0031,,}} } )	//"Contrato" # "Contrato"
	Aadd( aSeek, { STR0032, {{"","C",TamSX3("ABS_DESCRI")[1],0,STR0032,,}} } )	//"Descri��o do Posto" # "Descri��o do Posto"

	Aadd( aIndex, "TFF_COD" )
	Aadd( aIndex, "B1_COD" )
	Aadd( aIndex, "B1_DESC" )
	Aadd( aIndex, "TFF_CONTRT" )
	Aadd( aIndex, "ABS_DESCRI" )
	Aadd( aIndex, "TFF_FILIAL")  // adicionado para n�o ter problema de n�o encontrar o �ltimo �ndice, em caso de adicionar mais deixe a filial por �ltimo

	cQry := " SELECT TFF.TFF_FILIAL, TFF.TFF_COD, SB1.B1_COD, SB1.B1_DESC, TFF.TFF_CONTRT, ABS.ABS_DESCRI, TFF.TFF_PERINI, TFF.TFF_PERFIM, TFF.TFF_QTDVEN "
	cQry += " FROM " + RetSqlName("TFF") + " TFF "
	cQry += " INNER JOIN " + RetSqlName( "SB1" ) + " SB1 ON SB1.B1_COD = TFF.TFF_PRODUT AND "
	cQry += " SB1.B1_FILIAL = '" + xFilial("SB1") + "' AND SB1.D_E_L_E_T_ = ' ' "
	cQry += " INNER JOIN " + RetSqlName("TFL") + " TFL "
	cQry += " ON TFL.TFL_FILIAL = '" + xFilial("TFL") + "' "
	cQry += " AND TFL.D_E_L_E_T_ = ' ' "
	cQry += " AND TFL.TFL_CODIGO = TFF.TFF_CODPAI "
	cQry += " INNER JOIN " + RetSqlName("TFJ") + " TFJ "
	cQry += " ON TFJ.TFJ_FILIAL = '" + xFilial("TFJ") + "' "
	cQry += " AND TFJ.D_E_L_E_T_ = ' ' "
	cQry += " AND TFJ.TFJ_CODIGO = TFL.TFL_CODPAI "
	cQry += " AND TFJ.TFJ_STATUS = '1' "
	cQry += " AND TFJ.TFJ_CONTRT <> '" + cSpcCTR + "' "
	cQry += " INNER JOIN " + RetSqlName( "ABS" ) + " ABS ON TFL.TFL_LOCAL = ABS.ABS_LOCAL AND "
	cQry += " ABS.ABS_FILIAL = '" + xFilial("ABS") + "' AND ABS.D_E_L_E_T_ = ' ' "
	cQry += " WHERE TFF.TFF_FILIAL = '" +  xFilial('TFF') + "' AND "
	cQry += " TFF.D_E_L_E_T_ = ' ' AND "
	cQry += " ( TFF.TFF_ESCALA = '"+cEscala+"' OR TFF.TFF_ESCALA = '"+Space(LEN(cEscala))+"' ) "
EndIf

nSuperior := 0
nEsquerda := 0

nInferior := GetScreenRes()[2] * 0.6
nDireita  := GetScreenRes()[1] * 0.65

DEFINE MSDIALOG oDlgEscTela TITLE cTitle FROM nSuperior,nEsquerda TO nInferior,nDireita PIXEL

oBrowse := FWFormBrowse():New()
oBrowse:SetOwner(oDlgEscTela)
oBrowse:SetDataQuery(.T.)
oBrowse:SetAlias(cAls)
oBrowse:SetQueryIndex(aIndex)
oBrowse:SetQuery(cQry)
oBrowse:SetSeek(,aSeek)
oBrowse:SetDescription(cTitle)
oBrowse:SetMenuDef("")
oBrowse:DisableDetails()

If cTipo $ "TFF"
	oBrowse:SetDoubleClick({ || cRetF3 := (oBrowse:Alias())->TFF_COD, lRet := .T. ,oDlgEscTela:End()})
	oBrowse:AddButton( OemTOAnsi(STR0033), {|| cRetF3  := (oBrowse:Alias())->TFF_COD, lRet := .T., oDlgEscTela:End() } ,, 2 )	//"Confirmar"
EndIf

oBrowse:AddButton( OemTOAnsi(STR0034),  {|| cRetF3  := "", oDlgEscTela:End() } ,, 2 )	//"Cancelar"	
oBrowse:DisableDetails()

If cTipo $ "TFF"
	ADD COLUMN oColumn DATA { ||  TFF_COD  		} TITLE STR0028 SIZE TamSX3("TFF_COD")[1] OF oBrowse
	ADD COLUMN oColumn DATA { ||  STOD(TFF_PERINI ) 	} TITLE STR0035 SIZE TamSX3("TFF_PERINI")[1] OF oBrowse //"Per�odo Inicial"
	ADD COLUMN oColumn DATA { ||  STOD(TFF_PERFIM ) 	} TITLE STR0036 SIZE TamSX3("TFF_PERFIM")[1] OF oBrowse //"Per�odo Final"
	ADD COLUMN oColumn DATA { ||  TFF_QTDVEN  	} TITLE GetSX3Cache( "TFF_QTDVEN", "X3_DESCRIC" ) SIZE TamSX3("TFF_QTDVEN")[1] OF oBrowse
	ADD COLUMN oColumn DATA { ||  B1_COD 		} TITLE STR0029 SIZE TamSX3("B1_COD")[1] OF oBrowse
	ADD COLUMN oColumn DATA { ||  B1_DESC  		} TITLE STR0037 SIZE TamSX3("B1_DESC")[1] OF oBrowse //"Descri��o do Servi�o"
	ADD COLUMN oColumn DATA { ||  TFF_CONTRT  	} TITLE STR0031 SIZE TamSX3("TFF_CONTRT")[1] OF oBrowse
	ADD COLUMN oColumn DATA { ||  ABS_DESCRI  	} TITLE STR0038 SIZE TamSX3("ABS_DESCRI")[1] OF oBrowse //"Descri��o do Local"
EndIf
oBrowse:Activate()

ACTIVATE MSDIALOG oDlgEscTela CENTERED

If cTipo $ "TFF" .And. lMtFilTFF .And. cFilAnt <> cFilBkp
	cFilAnt := cFilBkp
Endif

Return( lRet )
//-------------------------------------------------------------------
/*/{Protheus.doc} At580GTdOk

@description posValid do modelo
@author	boiani
@since	26/11/2019
/*/
//-------------------------------------------------------------------
Function At580GTdOk(oModel)
Local lRet := .T.
Local nX
Local nY
Local oMdlTGW := oModel:GetModel('TGWDETAIL')
Local oMdlTXH := oModel:GetModel('TXHDETAIL')
Local lMtFilTFF := At580GMtFil()

For nX := 1 To oMdlTGW:Length()
	oMdlTGW:GoLine(nX)
	For nY := 1 To oMdlTXH:Length()
		oMdlTXH:GoLine(nY)
		If !EMPTY(oMdlTXH:GetValue('TXH_CODIGO')) .And. !oMdlTXH:IsDeleted()
			If TecNumToHr(oMdlTGW:GetValue("TGW_HORINI")) == oMdlTXH:GetValue("TXH_HORAIN") .AND.;
					TecNumToHr(oMdlTGW:GetValue("TGW_HORFIM")) == oMdlTXH:GetValue("TXH_HORAFI")
				lRet := .F.
				Help( " ", 1, "At580GTdOk", , STR0039, 1 ) //"Necess�rio informar um hor�rio diferente do hor�rio da Escala."
				Exit
			EndIf
			If lMtFilTFF .And. !Empty(oMdlTXH:GetValue("TXH_MTFIL")) .And. Empty(oMdlTXH:GetValue("TXH_CODTFF"))
				lRet := .F.
				Help( , , "At580GTdOk", , STR0050, 1, 0,,,,,,{STR0051}) //"O campo Fil. Sistema esta preenchido e o campo C�d. Posto esta vazio."##"Preencha o campo C�d. Posto."
				Exit
			Endif
		EndIf
	Next nY
	If !lRet
		Exit
	EndIf
Next nX

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} At580GRpl

@description Realiza replica dos hor�rios
@author	fabiana.silva
@since	09/03/2020
/*/
//-------------------------------------------------------------------

Static function At580GRpl(lView)

Local oModel 	:= FWModelActive()
Local aTDHDet 	:= {}
Local aTGWKey 	:= {}
Local aLinha 	:= {}
Local aSaveRows := {}
Local oView 	:= NIL
Local oMdlTGW 	:= oModel:GetModel("TGWDETAIL")
Local oMdlTXH 	:= oModel:GetModel("TXHDETAIL")
Local cNoCpos 	:= "TXH_FILIAL+TXH_CODIGO+TXH_CODPAI"
Local lSucess 	:= .T.
Local lEqual 	:= .T.
Local nLinTGW 	:= 0
Local nZ 		:= 0
Local nC 		:= 0
Local nX 		:= 0
Local lMtFilTFF := At580GMtFil()

Default lView := .T.

If oModel:GetOperation() ==  MODEL_OPERATION_UPDATE .OR. oModel:GetOperation() == MODEL_OPERATION_INSERT
	aSaveRows := FwSaveRows()

	If !oMdlTGW:IsEmpty() .AND.  !oMdlTXH:IsEmpty() 
		nLinTGW := oMdlTGW:GetLine()
		
		aAdd(aTGWKey, {"TGW_HORINI",  oMdlTGW:GetValue("TGW_HORINI")})
		aAdd(aTGWKey, {"TGW_HORFIM", oMdlTGW:GetValue("TGW_HORFIM")})
		aAdd(aTGWKey, {"TGW_STATUS", oMdlTGW:GetValue("TGW_STATUS")})

		oMdlTXH:GoLine(1)
		aTDHDet := {}
		For nC := 1 to oMdlTXH:Length()
			oMdlTXH:GoLine(nC)
			If !oMdlTXH:IsEmpty()  .AND. !oMdlTXH:IsDeleted()
				aLinha := {}
				For nX := 1 to Len(oMdlTXH:aHeader)
					If !oMdlTXH:aHeader[nX][02] $ cNoCpos
						aAdd(aLinha,  { oMdlTXH:aHeader[nX][02] , oMdlTXH:GetValue(oMdlTXH:aHeader[nX][02])})
					EndIf
				Next nX 
				aAdd(aTDHDet,  aClone(aLinha))
			EndIf
		Next 
		oMdlTGW:GoLine(1)

		If lSucess
			For nC := 1 to oMdlTGW:Length()
				If nC <> nLinTGW
					oMdlTGW:GoLine(nC)
					lEqual := .T.
					aEval(aTGWKey, {|l| lEqual := lEqual .AND. l[2] == oMdlTGW:GetValue(l[1])} )
					If lEqual
						For nX := 1 to Len(aTDHDet)
							If lMtFilTFF .And. oMdlTXH:SeekLine({{"TXH_MANUT", aTDHDet[nX][01][2]},{"TXH_CODTFF",aTDHDet[nX][05][2]},{"TXH_CODTCU",aTDHDet[nX][07][2]},{"TXH_MTFIL",aTDHDet[nX][09][2]} } ) .Or. ;
							 	(!lMtFilTFF .And. oMdlTXH:SeekLine({{"TXH_MANUT", aTDHDet[nX][01][2]},{"TXH_CODTFF",aTDHDet[nX][05][2]},{"TXH_CODTCU",aTDHDet[nX][07][2]} } ))
								For nZ := 1 to Len(aTDHDet[nX])
									If !aTDHDet[nX][nZ][02] $ "TXH_MANUT+TXH_DSMANU"
										oMdlTXH:LoadValue(aTDHDet[nX][nZ][01],aTDHDet[nX][nZ][02] )
									EndIf
								Next nZ
							Else
								If oMdlTXH:Length() == 1 .AND. Empty(oMdlTXH:GetValue('TXH_MANUT'))
									oMdlTXH:GoLine(1)
								Else
									oMdlTXH:GoLine(oMdlTXH:AddLine())
								EndIf

								For nZ := 1 to Len(aTDHDet[nX])
									oMdlTXH:LoadValue(aTDHDet[nX][nZ][01], aTDHDet[nX][nZ][02] )
								Next nZ
							EndIf
						Next nX 
					EndIf
				EndIf
			Next
		EndIf
	EndIf

	If lView
		oView := FwViewActive()
		oView:Refresh("VIEW_TGW")
	EndIf

	FwRestRows(aSaveRows)

	If lSucess
		MsgInfo(STR0041) //"Hor�rios replicados com sucesso."
	else
		Help(,1,"At580GRpl",,STR0042, 1)  //"Falha  na replica��o dos hor�rios."
	EndIf
Else
	Help(,1,"At580GRpl",,STR0043, 1) //"Fun��o somente permitida para as opera��es de inclus�o e altera��o" 
EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} At580GEnSa

@description Garante o numero da entrada e saida.
@author	Servi�os
@since	18/03/2020
/*/
//-------------------------------------------------------------------
Static Function At580GEnSa(cCodTDX,cDiaSem,cHrIni,cHrFim)
Local nRet := 0
Local cQry := GetNextAlias()

BeginSQL Alias cQry
	SELECT TGW.TGW_HORINI,
		   TGW.TGW_HORFIM
	  FROM %Table:TGW% TGW
	 WHERE TGW.TGW_FILIAL = %xFilial:TGW%
	  	AND TGW.TGW_EFETDX = %Exp:cCodTDX%
	  	AND TGW.TGW_DIASEM = %Exp:cDiaSem%
		AND TGW.TGW_STATUS = '1'
		AND TGW.%NotDel%
	ORDER BY TGW.TGW_COD
EndSQL

While !(cQry)->(EOF())
	nRet++
	IF (cQry)->(TGW_HORINI) == cHrIni .And. (cQry)->(TGW_HORFIM) == cHrFim
		Exit
	Endif
	(cQry)->(DbSkip())
EndDo

(cQry)->(DbCloseArea())

Return nRet

//-------------------------------------------------------------------
/*/{Protheus.doc} At580DlMt

@description Realiza a exclus�o em lote das manuten��es planejadas
@author	Diego Bezerra
@since	03/04/2020
/*/
//-------------------------------------------------------------------
Static function At580DlMt()

Local oModel 	:= FWModelActive()
Local oMdlTGW 	:= oModel:GetModel("TGWDETAIL")
Local oMdlTXH 	:= oModel:GetModel("TXHDETAIL")
Local nX 		:= 0
Local nC 		:= 0
Local nDeleted	:= 0

If MsgYesNo(STR0044) //#"Deseja excluir todas as manuten��es planejadas para essa escala?"
	For nC := 1 to oMdlTGW:Length()
		oMdlTGW:GoLine(nC)
		nX  := 1
		For nX := 1 to oMdlTXH:Length()
			oMdlTXH:GoLine(nX)
			If !oMdlTXH:IsEmpty()
				nDeleted ++
				oMdlTXH:DeleteLine()
			EndIf
		Next nX 
	Next nC

	If nDeleted > 0
		MsgInfo(STR0045+cValToChar(nDeleted)+STR0046) //"Foram excluidas "# " programa��es de manuten��o."
	Else
		Help(,1,"At580DlMt",,STR0047, 1) //"N�o existem programa��es para serem exclu�das." 
	EndIf
Else
	Help(,1,"At580DlMt",,STR0048, 1) //"Opera��o cancelada.Nenhum registro foi exclu�do." 
EndIf
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} At580GMtFil
 
@description Campo de Multi-Filial do Sistema.
@author	Kaique Schiller
@since	19/05/2020
/*/
//-------------------------------------------------------------------
Static Function At580GMtFil()
Return TXH->( ColumnPos("TXH_MTFIL")) > 0

//-------------------------------------------------------------------
/*/{Protheus.doc} At580GVFil
 
@description Valida��o da Filial do sistema com a filial da TFF - Posto
@author	Kaique Schiller
@since	19/05/2020
/*/
//-------------------------------------------------------------------
Function At580GVFil(oModel,cField,xNewValue)
Local aAreaTFF := TFF->(GetArea())
Local lRet 	   := .T.
Local cFilBkp  := cFilAnt

If !Empty(xNewValue) 
	If FWModeAccess("TXH",1) == "E" .Or. FWModeAccess("TXH",2) == "E" .Or. FWModeAccess("TXH",3) == "E"
		lRet := .F.
		Help(,1,"At580GVFil",,STR0052, 1) //"N�o � poss�vel realizar o preenchimento do campo, � obrigat�rio que a tabela 'TXH' esteja totalmente compartilhada."    
	Else
		If ExistCpo("SM0",cEmpAnt+xNewValue)
			DbSelectArea("TFF")
			TFF->(DbSetOrder(1))
			If !Empty(oModel:GetValue("TXH_CODTFF"))
				cFIlAnt := xNewValue
				If !TFF->(DbSeek(xFilial("TFF")+oModel:GetValue("TXH_CODTFF")))
					lRet := .F.
					Help(,1,"At580GVFil",,STR0053, 1) //"N�o � poss�vel encontrar a filial relacionada a esse c�digo de posto, informe um c�digo de filial v�lido."
				Endif
				cFilAnt := cFilBkp
			Endif
		Endif
	Endif
Endif

RestArea(aAreaTFF)

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} At580GTgDs
 
@description Gatilho de descri��o do produto do posto.
@author	Kaique Schiller
@since	19/05/2020
/*/
//-------------------------------------------------------------------
Function At580GTgDs()
Local oModel 	:= FwModelActive()
Local oMdlTXH 	:= oModel:GetModel("TXHDETAIL")
Local lMtFilTFF := At580GMtFil() .And. !Empty(oMdlTXH:GetValue("TXH_MTFIL"))
Local cFilBkp   := cFilAnt
Local cRetDesc	:= ""

If lMtFilTFF .And. cFilAnt <> oMdlTXH:GetValue("TXH_MTFIL")
	cFilAnt := oMdlTXH:GetValue("TXH_MTFIL")
Endif

cRetDesc := Posicione("SB1",1,xFilial("SB1") + Posicione("TFF",1,xFilial("TFF") + oMdlTXH:GetValue("TXH_CODTFF"),"TFF_PRODUT"),"B1_DESC")

If lMtFilTFF .And. cFilBkp <> cFilAnt
	cFilAnt := cFilBkp
Endif

Return cRetDesc

//-------------------------------------------------------------------
/*/{Protheus.doc} At580GDesL
 
@description Gatilho de descri��o do local de atendimento do posto.
@author	Augusto Albuquerque
@since	27/05/2020
/*/
//-------------------------------------------------------------------
Function At580GDesL()
Local oModel 	:= FwModelActive()
Local oMdlTXH 	:= oModel:GetModel("TXHDETAIL")
Local lMtFilTFF := At580GMtFil() .And. !Empty(oMdlTXH:GetValue("TXH_MTFIL"))
Local cFilBkp   := cFilAnt
Local cCodTFF	:= oMdlTXH:GetValue("TXH_CODTFF")
Local cRetDesc	:= ""

If !Empty(cCodTFF)
	If lMtFilTFF .And. cFilAnt <> oMdlTXH:GetValue("TXH_MTFIL")
		cFilAnt := oMdlTXH:GetValue("TXH_MTFIL")
	Endif

	cRetDesc := Posicione("ABS",1,xFilial("ABS") + Posicione("TFF",1,xFilial("TFF") + cCodTFF,"TFF_LOCAL"),"ABS_DESCRI")

	If lMtFilTFF .And. cFilBkp <> cFilAnt
		cFilAnt := cFilBkp
	Endif
EndIf
Return cRetDesc