#Include "PROTHEUS.CH"
#Include "FWMVCDEF.CH"
#Include "TECA740D.CH"

Static oBrowse

//----------------------------------------------------------
/*/{Protheus.doc} TECA740D(cContrato)
Configura��o de aloca��o para adicionar horas n�o cobradas

@param 		cContrato, Contrato para a pesquisa da configura��o  

@Return 	nil
@author 	Servi�os
@since 		22/04/2014
/*/
//----------------------------------------------------------
Function TECA740D(cContrato)

Local aColumns	:= {}
Local cQuery		:= ""
Local cAliasABQ	:= "MNTABQ"
Local oDlg 		:= Nil   							// Janela Principal.
Local aSize	 	:= FWGetDialogSize( oMainWnd ) 	// Array com tamanho da janela.
Local aArea		:= GetArea()

Local aMenu
Local nI

If ! At680Perm( Nil, __cUserID, "014" )
	Help(,,"AT740DACESSO",,STR0001,1,0) // "Usuario n�o possui acesso para essa rotina!"
	RestArea(aArea)	
	Return(Nil)
EndIf

dbSelectArea("ABQ")
ABQ->(dbSetOrder(2))

If ABQ->(dbSeek(xFilial("ABQ")+cContrato))

	aColumns := At740DCols()
	cQuery   := At740DQry(cContrato)
	
	//Cria a tela para o browse
	DEFINE DIALOG oDlg TITLE STR0002 FROM aSize[1],aSize[2] TO aSize[3],aSize[4] PIXEL // "Configura��o de aloca��o"
		
		oBrowse := FWFormBrowse():New()
		oBrowse:SetOwner(oDlg)	
		oBrowse:SetAlias(cAliasABQ)
		oBrowse:SetMenuDef("TECA740D")
		oBrowse:SetDataQuery(.T.)
		oBrowse:SetQuery(cQuery)
		oBrowse:SetColumns(aColumns[1])
		oBrowse:SetUseFilter(.T.)
		oBrowse:SetFieldFilter(aColumns[2])
			
		//Gera as opcoes do menu funcional no browse
		aMenu := MenuDef()
			
		For nI := 1 To Len( aMenu )
			oBrowse:AddButton( aMenu[nI][1], aMenu[nI][2], Nil, aMenu[nI][4], aMenu[nI][5], (aMenu[nI][4] > 1) )
		Next nI						 
		
		oBrowse:DisableDetails()
		oBrowse:SetDescription(STR0002)  // "Configura��o de aloca��o"
		oBrowse:Activate()
			
	ACTIVATE DIALOG oDlg CENTERED
	
Else 
	Help(,,"AT740DOK",,STR0003,1,0) //"N�o existem configura��es de aloca��es!"	
EndIf

RestArea(aArea)

Return(Nil) 


//----------------------------------------------------------
/*/{Protheus.doc} MenuDef()
MenuDef - Configura��o de aloca��o para adicionar horas n�o cobradas

@Return 	MenuDef
@author 	Servi�os
@since 		22/04/2014
/*/
//----------------------------------------------------------
Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE STR0005 	ACTION "At740DAuto"	OPERATION 2 	ACCESS 0 	// "Visualizar"
ADD OPTION aRotina TITLE STR0006	ACTION "At740DAuto"	OPERATION 4	ACCESS 0 	// "Alterar"

Return(aRotina)


//----------------------------------------------------------
/*/{Protheus.doc} ModelDef()
Model - Cadastro de Grupo de acesso/perfil

@Return 	model
@author 	Servi�os
@since 		07/04/2013
/*/
//----------------------------------------------------------
Static Function ModelDef()

Local oModel

Local oStruABQ := FWFormStruct(1,"ABQ")

// Cria o objeto do Modelo de Dados
oModel := MPFormModel():New("TECA740D", /*bPreValidacao*/,/*bPosValidacao*/,/*bCommit*/,/*bCancel*/ )

//Cabecalho do grupo
oModel:AddFields("ABQMASTER",/*cOwner*/,oStruABQ, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )

//oModel:SetPrimaryKey( {} )
oModel:SetDescription(STR0002) // Configura��o de aloca��o

//oModel:SetActivate( {|oModel| InitDados( oModel ) } )

 									  
Return(oModel)


//----------------------------------------------------------
/*/{Protheus.doc} ViewDef()
View - Cadastro de Lista de emails

@Return 	view
@author 	Servi�os
@since 		09/09/2013
/*/
//----------------------------------------------------------
Static Function ViewDef()

Local oView

Local oModel := FWLoadModel("TECA740D")

Local oStruABQ := FWFormStruct( 2,"ABQ") 

oStruABQ:SetProperty("*",MVC_VIEW_CANCHANGE, .F.)
oStruABQ:SetProperty("ABQ_HRNCOB",MVC_VIEW_CANCHANGE, .T.)

oView := FWFormView():New()

oView:SetModel( oModel )
oView:SetDescription( STR0002 ) // "Configura��o de aloca��o"
oView:SetCloseOnOk({|| .T. } )

oView:AddField("ABQ_VIEW", oStruABQ, "ABQMASTER" )

Return(oView)


//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At740DQry

Query com os dados da configura��o de aloca��o para adicionar horas n�o cobradas

@author 	Servi�os
@since 		22/04/2014
@version 	P12
	
@return	cQuery - Query com as informa��es do grupo de acesso/perfil
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function At740DQry(cContrato)

Local cQuery  := ""
Local lFilTFF := FindFunction("ExistFilTFF") .And. ExistFilTFF()

cQuery += "SELECT ABQ.ABQ_FILIAL, ABQ.ABQ_CONTRT, ABQ.ABQ_ITEM, ABQ.ABQ_LOCAL,"
cQuery += "ABS.ABS_DESCRI, ABQ.ABQ_CODTFF, ABQ.ABQ_PRODUT, SB1.B1_DESC, ABQ.ABQ_FUNCAO,"
cQuery += "SRJ.RJ_DESC, ABQ.ABQ_TURNO, SR6.R6_DESC, ABQ.ABQ_PERINI, ABQ.ABQ_PERFIM,"
cQuery += "ABQ.ABQ_TOTAL, ABQ.ABQ_HRNCOB, ABQ.ABQ_SALDO "

IF lFilTFF
	cQuery += ", ABQ.ABQ_FILTFF
ENDIF

cQuery += "FROM " +  RetSqlName("ABQ") + " ABQ " 
cQuery += "LEFT JOIN " + RetSqlName("ABS") + " ABS ON "
cQuery += "ABS.ABS_FILIAL = '" + xFilial("ABS") + "' AND " 
cQuery += "ABS.ABS_LOCAL = ABQ.ABQ_LOCAL AND " 
cQuery += "ABS.D_E_L_E_T_ = ' ' "
cQuery += "LEFT JOIN " + RetSqlName("SB1") + " SB1 ON "
cQuery += "SB1.B1_FILIAL = '" + xFilial("SB1") + "' AND " 
cQuery += "SB1.B1_COD = ABQ.ABQ_PRODUT AND "
cQuery += "SB1.D_E_L_E_T_ = ' ' "
cQuery += "LEFT JOIN " + RetSqlName("SRJ") + " SRJ ON " 
cQuery += "SRJ.RJ_FILIAL = '" + xFilial("SRJ") + "' AND " 
cQuery += "SRJ.RJ_FUNCAO = ABQ.ABQ_FUNCAO AND " 
cQuery += "SRJ.D_E_L_E_T_ = ' ' "
cQuery += "LEFT JOIN " + RetSqlName("SR6") + " SR6 ON " 
cQuery += "SR6.R6_FILIAL = '" + xFilial("SR6") + "' AND " 
cQuery += "SR6.R6_TURNO = ABQ.ABQ_TURNO AND " 
cQuery += "SR6.D_E_L_E_T_ = ' ' "
cQuery += "WHERE ABQ.ABQ_CONTRT = '" + cContrato + "' "

Return(cQuery)


//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At740DCols

Colunas para o browse com os dados da configura��o de aloca��o para adicionar horas n�o cobradas

@author 	Servi�os
@since 		22/04/2014
@version 	P12

@return	aColumns - Colunas para o browse
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function At740DCols()

Local nI		:= 0 
Local aArea	 	:= GetArea()
Local aColumns 	:= {}
Local aFiltros	:= {}
Local aCampos  	:= {}
Local lFilTFF  	:= FindFunction("ExistFilTFF") .And. ExistFilTFF()

IF !lFilTFF
	aCampos := { 	"ABQ_FILIAL", "ABQ_CONTRT", "ABQ_ITEM", "ABQ_LOCAL", "ABQ_CODTFF",; 
					"ABQ_PRODUT", "B1_DESC", "ABQ_FUNCAO", "RJ_DESC", "ABQ_TURNO",;
					"R6_DESC", "ABQ_PERINI", "ABQ_PERFIM", "ABQ_TOTAL", "ABQ_HRNCOB",;
					"ABQ_SALDO" }
ELSE
	aCampos := { 	"ABQ_FILIAL", "ABQ_CONTRT", "ABQ_ITEM", "ABQ_LOCAL", "ABQ_CODTFF",; 
					"ABQ_PRODUT", "B1_DESC", "ABQ_FUNCAO", "RJ_DESC", "ABQ_TURNO",;
					"R6_DESC", "ABQ_PERINI", "ABQ_PERFIM", "ABQ_TOTAL", "ABQ_HRNCOB",;
					"ABQ_SALDO", "ABQ_FILTFF" }
ENDIF

dbSelectArea("SX3")
SX3->(DbSetOrder(2))

For nI:=1 To Len(aCampos)

	If SX3->(dbSeek(aCampos[nI]))
	
		cCampo := AllTrim(SX3->X3_CAMPO)
		
		AAdd(aColumns,FWBrwColumn():New())
		nLinha := Len(aColumns)
	   	aColumns[nLinha]:SetType(SX3->X3_TIPO)
	   	aColumns[nLinha]:SetTitle(X3Titulo())
		aColumns[nLinha]:SetSize(SX3->X3_TAMANHO)
		aColumns[nLinha]:SetDecimal(SX3->X3_DECIMAL)		
		aColumns[nLinha]:SetData(&("{||" + cCampo + "}"))		
		
		aAdd(aFiltros,{cCampo,X3Titulo(),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_PICTURE})
		
	EndIf
	
Next nI

SX3->(dbCloseArea())

RestArea(aArea)

Return({aColumns, aFiltros})


//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At740DAuto

Rotinas para utilizar o posicionamento da tabela a ser chamada  

@author 	Servi�os
@since 		22/04/2014
@version 	P12

/*/
//--------------------------------------------------------------------------------------------------------------------
Function At740DAuto( cAlias, nReg, nOpcx )

Local cTitulo := ""

Local nOpeMnu
Local lRet

If nOpcx == 2
	cTitulo := STR0005 // "Visualizar"	
	nOpeMnu := MODEL_OPERATION_VIEW 
ElseIf nOpcx == 4
	cTitulo := STR0006 // "Alterar"
	nOpeMnu := MODEL_OPERATION_UPDATE
EndIf

DbSelectArea("ABQ")
ABQ->(DbSetOrder(1))	
lRet := ABQ->(dbSeek(xFilial("ABQ")+MNTABQ->ABQ_CONTRT+MNTABQ->ABQ_ITEM))	

If lRet .And. !Empty(cTitulo)	
	FWExecView(cTitulo,"VIEWDEF.TECA740D",nOpeMnu,/*oDlg*/,{||.T.}/*bCloseOnOk*/,/*bOk*/,/*nPercReducao*/)
	oBrowse:Data():DeActivate()		
	oBrowse:Data():Activate()
	oBrowse:GoTo( 1, .T.)
	oBrowse:Refresh(.T.)
EndIf 

Return(Nil)