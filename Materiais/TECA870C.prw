#include 'totvs.ch'
#include "TECA870C.CH"

#DEFINE DEF_MEDICAO 1
#DEFINE DEF_FINANC 2
#DEFINE DEF_CRONOG 3 

//-------------------------------------------------------------------
/*/{Protheus.doc} TECA870C
Contrato Simplificado - O objetivo dessa rotina � montar uma tela
que exiba as informa��es relevantes de um ou v�rios contratos de forma
simplificada e r�pida

@param cContrato, character, n�mero do contrato (opcional)

@author Mateus Boiani
@since 21/11/2018
@version 1.0 
/*/
//-------------------------------------------------------------------
Function TECA870C(cContrato)

Local oDlgSelect
Local aDados
Local aSorting   := {0, .F.}
Local oCtrLike
Local oRefresh
Local oMedicoes
Local oTitulos
Local oCronog
Local oVerif
Private oListBox

Default cContrato := SPACE(TamSX3("CNF_CONTRA")[1])

aDados := AtGetCtr(cContrato)

DEFINE MSDIALOG oDlgSelect FROM 0,0 TO 490,950 PIXEL TITLE STR0001

@ 5, 9 SAY STR0002 SIZE 50, 19 PIXEL 

oCtrLike := TGet():New( 015, 009, { | u | If( PCount() == 0, cContrato, cContrato := u ) },oDlgSelect, ;
    				 100, 010, "!@",, 0, 16777215,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F. ,,"cContrato",,,,.T.  )

oExit := TButton():New( 232, 444, STR0003,oDlgSelect,{|| oListBox:aARRAY := {}, oDlgSelect:End() }, 30,10,,,.F.,.T.,.F.,,.F.,,,.F. )
oMedicoes := TButton():New( 232, 015, STR0004,oDlgSelect,{|| at870cGtDt(DEF_MEDICAO,oListBox:aARRAY[oListBox:nAt,1],oListBox:aARRAY[oListBox:nAt,2]) }, 35,10,,,.F.,.T.,.F.,,.F.,,,.F. )
oTitulos := TButton():New( 232, 065, STR0005,oDlgSelect,{|| at870cGtDt(DEF_FINANC,oListBox:aARRAY[oListBox:nAt,1],oListBox:aARRAY[oListBox:nAt,2]) }, 35,10,,,.F.,.T.,.F.,,.F.,,,.F. )
oCronog := TButton():New( 232, 115, STR0006,oDlgSelect,{|| at870cGtDt(DEF_CRONOG,oListBox:aARRAY[oListBox:nAt,1],oListBox:aARRAY[oListBox:nAt,2]) }, 35,10,,,.F.,.T.,.F.,,.F.,,,.F. )


oListBox := TWBrowse():New(030, 007, 465, 200,,{},,oDlgSelect,,,,,,,,,,,,.F.,,.T.,,.F.,,.T.,.F.)

oListBox:addColumn(TCColumn():New(	STR0007, &("{|| oListBox:aARRAY[oListBox:nAt,1] }"),,,,,80))
oListBox:addColumn(TCColumn():New(	STR0008, &("{|| oListBox:aARRAY[oListBox:nAt,2] }"),,,,,25))
oListBox:addColumn(TCColumn():New(	STR0009, &("{|| oListBox:aARRAY[oListBox:nAt,3] }"),,,,,90))
oListBox:addColumn(TCColumn():New(	STR0010, &("{|| oListBox:aARRAY[oListBox:nAt,4] }"),,,,,90))
oListBox:addColumn(TCColumn():New(	STR0011, &("{|| oListBox:aARRAY[oListBox:nAt,5] }"),,,,,90))
oListBox:addColumn(TCColumn():New(	STR0012, &("{|| oListBox:aARRAY[oListBox:nAt,6] }"),,,,,45))

oListBox:SetArray(aDados)
oListBox:lAutoEdit    := .T.
oListBox:bHeaderClick := { |a, b| { T870cClick(oListBox:aARRAY, oListBox, a, b, aSorting, oDlgSelect) }}
oListBox:Refresh()

oRefresh := TButton():New( 016, 115, STR0013,oDlgSelect,{|| At870cRfr(@oListBox,cContrato)}, 40,10,,,.F.,.T.,.F.,,.F.,,,.F. ) //Buscar
//If Date() == cToD("29/11/18") - Utilizado para ajustar base de dados via APSDU
//	oVerif := TButton():New( 007, 400, "Verificar Integridade",oDlgSelect,{|| At870Vint(oListBox:aARRAY)}, 65,18,,,.F.,.T.,.F.,,.F.,,,.F. ) //Buscar
//EndIf
ACTIVATE MSDIALOG oDlgSelect CENTER

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} AtGetCtr
Fun��o utilizada apenas para chamar a AtGetCtr2 dentro de um MsgRun

@param cContrato, character, n�mero do contrato

@return aRet, array, array contendo os dados encontrados

@author Mateus Boiani
@since 21/11/2018
@version 1.0 
/*/
//-------------------------------------------------------------------
Static Function AtGetCtr(cContrato)
Local aRet
If !isBlind()
	MsgRun ( STR0014, STR0015, {|| aRet := AtGetCtr2(cContrato) } )
Else
	aRet := AtGetCtr2(cContrato)
EndIf

Return aRet

//-------------------------------------------------------------------
/*/{Protheus.doc} AtGetCtr2
Busca no banco de dados as informa��es do grid principal

@param cContrato, character, n�mero do contrato

@return aRet, array, array contendo os dados encontrados

@author Mateus Boiani
@since 21/11/2018
@version 1.0 
/*/
//-------------------------------------------------------------------
Static Function AtGetCtr2(cContrato)
Local cAlias870c := GetNextAlias()
Local aRet := {}
Local cSql := ""

Default cContrato := ""

cSql += "SELECT "
cSql += " CNF.CNF_CONTRA, CNF.CNF_REVISA, SUM(CNF.CNF_VLPREV) AS TOTAL, "
cSql += " SUM(CNF.CNF_VLREAL) AS APURADO, SUM(CNF.CNF_SALDO) AS SALDO "
cSql += " FROM " + RetSqlName('CNF') + " CNF "
cSql += " JOIN " + RetSqlName('CN9') + " CN9 ON "
cSql += " CN9.CN9_FILIAL = CNF.CNF_FILIAL AND "
cSql += " CN9.CN9_NUMERO = CNF.CNF_CONTRA AND "
cSql += " CN9.CN9_REVISA = CNF.CNF_REVISA "
cSql += " WHERE CN9.D_E_L_E_T_ = ' ' AND CNF.D_E_L_E_T_ = ' ' "
cSql += " AND CN9.CN9_REVATU = '' AND"
cSql += " CN9.CN9_FILIAL = '" + xFilial("CN9") + "'  AND "
cSql += " CNF.CNF_FILIAL = '" + xFilial("CNF") + "' "
If !EMPTY(cContrato)
	cSql += " AND CNF.CNF_CONTRA LIKE '%" + UPPER(ALLTRIM(cContrato)) + "%' "
EndIf
cSql += " GROUP BY CNF.CNF_CONTRA, CNF.CNF_REVISA "
cSql += " UNION ALL "
cSql += " SELECT "
cSql += " CNA.CNA_CONTRA, CNA.CNA_REVISA, SUM(CNA.CNA_VLTOT) AS TOTAL, "
cSql += " (SUM(CNA.CNA_VLTOT) - SUM(CNA.CNA_SALDO)) AS APURADO , SUM(CNA.CNA_SALDO) AS SALDO "
cSql += " FROM " + RetSqlName('CNA') + " CNA "
cSql += " JOIN " + RetSqlName('CN9') + " CN9 ON "
cSql += " CN9.CN9_FILIAL = CNA.CNA_FILIAL AND "
cSql += " CN9.CN9_NUMERO = CNA.CNA_CONTRA AND "
cSql += " CN9.CN9_REVISA = CNA.CNA_REVISA "
cSql += " WHERE CNA.D_E_L_E_T_ = ' ' AND CN9.D_E_L_E_T_ = ' ' AND CN9.CN9_REVATU = '' AND "
cSql += " CNA.CNA_FILIAL = '" + xFilial("CNA") + "'  AND "
cSql += " CNA.CNA_FILIAL || CNA.CNA_CONTRA || CNA.CNA_REVISA NOT IN "
cSql += " (SELECT CNF.CNF_FILIAL || CNF.CNF_CONTRA || CNF.CNF_REVISA FROM " + RetSqlName('CNF') + " CNF
cSql += " WHERE CNF.D_E_L_E_T_ = ' ')"
If !EMPTY(cContrato)
	cSql += " AND CNA.CNA_CONTRA LIKE '%" + UPPER(ALLTRIM(cContrato)) + "%' "
EndIf
cSql += " GROUP BY CNA.CNA_CONTRA , CNA.CNA_REVISA "

cSql := ChangeQuery(cSql)	
dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAlias870c, .F., .T.)


While (cAlias870c)->(!Eof())

	AADD(aRet, {  (cAlias870c)->(CNF_CONTRA),;
					(cAlias870c)->(CNF_REVISA),;
					TransForm((cAlias870c)->(TOTAL),"@E 999,999,999.99"),;
					TransForm((cAlias870c)->(APURADO),"@E 999,999,999.99"),;
					TransForm((cAlias870c)->(SALDO),"@E 999,999,999.99"),;
					DataUltMed((cAlias870c)->(CNF_CONTRA) , (cAlias870c)->(CNF_REVISA)) })
	(cAlias870c)->(DbSkip())
End

(cAlias870c)->( DbCloseArea() )

Return aRet

//-------------------------------------------------------------------
/*/{Protheus.doc} T870cClick
A��o realizada ao clicar no cabe�alho do objeto TWBrowse

@param aRegs, array, registros presentes no grid
@param oListBox, obj, objeto TWBrowse
@param b, int, coluna selecionada
@param aSorting, array, utilizado para definir se a busca sera a > b ou a < b
@param oDlgSelect, obj, tela em que o TWBrowse � filho

@author Mateus Boiani
@since 21/11/2018
@version 1.0 
/*/
//-------------------------------------------------------------------
Static Function T870cClick(aRegs, oListBox, a, b, aSorting, oDlgSelect)

If aSorting[1] == b .and. aSorting[2]
	aSorting[2] := .F.
	aRegs := aSort(aRegs, 1, Len(aRegs), {|l1, l2| TECtoDate(l1[b]) > TECtoDate(l2[b])})
Else
	If aSorting[1] != b
		aSorting[1] := b
	EndIf
	aSorting[2] := .T.
	aRegs := aSort(aRegs, 1, Len(aRegs), {|l1, l2| TECtoDate(l1[b]) < TECtoDate(l2[b])})
EndIf
oListBox:SetArray(aRegs)
oListBox:Refresh()

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} At870cRfr
Bot�o buscar no MSDIALOG principal

@param oListBox, obj, objeto do tipo TWBrowse na dialog principal
@param cContrato, character, n�mero do contrato

@author Mateus Boiani
@since 21/11/2018
@version 1.0 
/*/
//-------------------------------------------------------------------
Function At870cRfr(oListBox,cContrato)
Local aDados

Default cContrato := ""

cContrato := RTRIM(cContrato)

If VAlTYPE(oListBox) == 'O' .AND. VALTYPE(oListBox:aARRAY) == 'A'
	oListBox:aARRAY := {}
EndIf

FwMsgRun(Nil,{|| aDados := AtGetCtr(cContrato)}, Nil, STR0016)
 
oListBox:aARRAY := aDados
oListBox:Refresh()

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} DataUltMed
Verifica na CND a �ltima medi��o de um contrato e retorna no formato string,
como DD/MM/AAAA

@param cContrt, character, n�mero do contrato
@param cRev, character, revis�o do contrato

@return cRet, character, data da �ltima medi��o como string

@author Mateus Boiani
@since 21/11/2018
@version 1.0 
/*/
//-------------------------------------------------------------------
Static Function DataUltMed(cContrato, cRev)
Local cRet := ""
Local aArea := GetArea()
Local cAlias870c := GetNextAlias()
Local cSql := ""
Local cDataMax
cSql += " SELECT MAX(CND.CND_DTFIM) AS DTFIM FROM " + RetSqlName('CND') + " CND "
cSql += " WHERE CND.D_E_L_E_T_ = ' ' AND "
cSql += " CND.CND_FILIAL = '" + xFilial("CND") + "' AND "
cSql += " CND.CND_CONTRA = '" + cContrato + "' AND "
cSql += " CND.CND_REVISA = '" + cRev + "'"

cSql := ChangeQuery(cSql)	
dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAlias870c, .F., .T.)

If !(cAlias870c)->(EOF())
	cDataMax := (cAlias870c)->(DTFIM)
	cRet := RIGHT(cDataMax,2) + "/" + SUBSTR(cDataMax,5,2) + "/" + LEFT(cDataMax,4)
EndIf

(cAlias870c)->(dbCloseArea())

RestArea(aArea)

Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} TECtoDate
Ordena uma string no formato data.

@param xValue, unknown, valor que ser� avaliado
@return xValue, unknown, se for uma string no formato NN/NN/NNNN ou NN/NNNN inverte a string para
o formato ano/m�s/dia ou ano/m�s. Essa invers�o � necess�ria para que um ASort na String ordene
corretamente uma data

@author Mateus Boiani
@since 21/11/2018
@version 1.0 
/*/
//-------------------------------------------------------------------
Static Function TECtoDate(xValue)

If VALTYPE(xValue) == "C" .AND. !EMPTY(StrTran(xValue, "/"))

	If LEN(StrTokArr(xValue, "/")) == 3 .AND. LEN(xValue) == 10
		xValue := StrTokArr(xValue, "/")
		xValue := xValue[3] + xValue[2] + xValue[1]
	EndIf
	
	If LEN(StrTokArr(xValue, "/")) == 2 .AND. LEN(xValue) == 7
		xValue := StrTokArr(xValue, "/")
		xValue := xValue[2] + xValue[1]
	EndIf

EndIf

Return xValue

//-------------------------------------------------------------------
/*/{Protheus.doc} at870cGtDt
Rotina que monta a tela com os dados do Cronograma / Medi��es ou T�tulos

@param nTipo, int, indica qual das vis�es ser� executada:
		DEF_MEDICAO - 1 - Medi��es
		DEF_FINANC - 2 - T�tulos do Financeiro
		DEF_CRONOG - 3 - Cronograma Financeiro

@param cContrt, character, n�mero do contrato		
@param cRev, character, revis�o do contrato

@author Mateus Boiani
@since 21/11/2018
@version 1.0 
/*/
//-------------------------------------------------------------------
Function at870cGtDt(nTipo, cContrt, cRev)
Local cQry := ""
Local aColunas := {}
Local cAlias870c := GetNextAlias()
Local aArea := GetArea()
Local aDados := {}
Local aAux := {}
Local oDlgInt
Local cTitulo
Local nX
Local aSorting   := {0, .F.}
Local oListBoxInt

If nTipo == DEF_MEDICAO
	aColunas := {{"CND_NUMMED","C","",STR0017} ,;
					{"CND_COMPET","C","",STR0018} ,;
					{"CND_VLTOT","N","@E 999,999,999.99",STR0019} ,;
					{"CND_DTFIM","D","",STR0020}}
	cQry := QryMedicao(cContrt, cRev, aColunas)
	cTitulo := STR0004
ElseIf nTipo == DEF_FINANC
	aColunas := {{"E1_PREFIXO","C","",STR0021} ,;
				{"E1_NUM","C","",STR0017} ,;
				{"E1_PARCELA","C","",STR0022} ,;
				{"E1_TIPO","C","",STR0023} ,;
				{"E1_CLIENTE","C","",STR0024} ,;
				{"E1_LOJA","C","",STR0025} ,;
				{"E1_VENCTO","D","",STR0026},;
				{"E1_VALOR","N","@E 999,999,999.99",STR0027} ,;
				{"E1_SALDO","N","@E 999,999,999.99",STR0011}}
	cQry := QryFinanc(cContrt, cRev, aColunas)
	cTitulo := STR0005
ElseIf nTipo == DEF_CRONOG
	aColunas := {{"CNF_NUMPLA","C","",STR0028} ,;
				{"CNF_COMPET","C","",STR0018} ,;
				{"CNF_PARCEL","C","",STR0022} ,;
				{"CNF_VLPREV","N","@E 999,999,999.99",STR0029} ,;
				{"CNF_VLREAL","N","@E 999,999,999.99",STR0030} ,;
				{"CNF_SALDO","N","@E 999,999,999.99",STR0011} ,;
				{"CNF_DTVENC","D","",STR0026}}
	cQry := QryCronog(cContrt, cRev, aColunas)
	cTitulo := "Cronogramas"
EndIf

cQry := ChangeQuery(cQry)
dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQry),cAlias870c, .F., .T.)

While (cAlias870c)->(!Eof())
	For nX := 1 to LEN(aColunas)
		AADD(aAux, (&("('"+cAlias870c+"')->("+aColunas[nx][1]+")")))
		If aColunas[nx][2] == "N" .AND. !EMPTY(aColunas[nx][3])
			aAux[LEN(aAux)] := TransForm(aAux[LEN(aAux)],aColunas[nx][3])
		ElseIf aColunas[nx][2] == "D"
			aAux[LEN(aAux)] := RIGHT(aAux[LEN(aAux)],2) + "/" + SUBSTR(aAux[LEN(aAux)],5,2) + "/" + LEFT(aAux[LEN(aAux)],4)
		EndIf
	Next
	AADD(aDados, aAux)
	aAux := {}
	(cAlias870c)->(DbSkip())
End
(cAlias870c)->(dbCloseArea())

DEFINE MSDIALOG oDlgInt FROM 0,0 TO 490,950 PIXEL TITLE cTitulo

@ 5, 9 SAY STR0031 SIZE 50, 19 PIXEL 
@ 5, 65 SAY cContrt SIZE 50, 19 PIXEL 
@ 5, 150 SAY STR0032 SIZE 50, 19 PIXEL 
@ 5, 185 SAY cRev SIZE 50, 19 PIXEL 

oExit := TButton():New( 232, 444, STR0003,oDlgInt,{|| oListBoxInt:aARRAY := {}, oDlgInt:End() }, 30,10,,,.F.,.T.,.F.,,.F.,,,.F. ) //Sair

oListBoxInt := TWBrowse():New(030, 007, 465, 200,,{},,oDlgInt,,,,,,,,,,,,.F.,,.T.,,.F.,,.T.,.F.)

For nX := 1 to LEN(aColunas)
	oListBoxInt:addColumn(TCColumn():New(	aColunas[nX][4], &("{|| oListBoxInt:aARRAY[oListBoxInt:nAt,"+cValtoChar(nX)+"] }"),,,,,435/LEN(aColunas)))
Next

oListBoxInt:SetArray(aDados)
oListBoxInt:lAutoEdit    := .T.
oListBoxInt:bHeaderClick := { |a, b| { T870cClick(oListBoxInt:aARRAY, oListBoxInt, a, b, aSorting, oDlgInt) }}
oListBoxInt:Refresh()

ACTIVATE MSDIALOG oDlgInt CENTER

RestArea(aArea)

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} QryMedicao
Monta a query de busca de Medi��es (CND)

@param cContrt, character, n�mero do contrato
@param cRev, character, revis�o do contrato
@param aColunas, array, array contendo o t�tulo das colunas

@return cRet, character, Query de busca na CND

@author Mateus Boiani
@since 21/11/2018
@version 1.0 
/*/
//-------------------------------------------------------------------
Static Function QryMedicao(cContrt, cRev, aColunas)
Local cRet := ""
Local nX

cRet += " SELECT "
For nX := 1 to LEN(aColunas)
	cRet += " CND." + aColunas[nX][1] + " "
	If LEN(aColunas) != nX
		cRet += " , "
	EndIf
Next
cRet += " FROM " + RetSqlName('CND') + " CND "
cRet += " WHERE CND.D_E_L_E_T_ = ' ' AND "
cRet += " CND.CND_FILIAL = '" + xFilial("CND") + "' AND "
cRet += " CND.CND_CONTRA = '" + cContrt + "' AND "
cRet += " CND.CND_REVISA = '" + cRev + "'"

Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} QryFinanc
Monta a query de busca de T�tulos do Financeiro (SE1)

@param cContrt, character, n�mero do contrato
@param cRev, character, revis�o do contrato
@param aColunas, array, array contendo o t�tulo das colunas

@return cRet, character, Query de busca na SE1

@author Mateus Boiani
@since 21/11/2018
@version 1.0 
/*/
//-------------------------------------------------------------------
Static Function QryFinanc(cContrt, cRev, aColunas)
Local cRet := ""
Local nX

cRet += " SELECT "
For nX := 1 to LEN(aColunas)
	cRet += " SE1." + aColunas[nX][1] + " "
	If LEN(aColunas) != nX
		cRet += " , "
	EndIf
Next
cRet += " FROM " + RetSqlName('SE1') + " SE1 "
cRet += " WHERE SE1.D_E_L_E_T_ = ' ' AND "
cRet += " SE1.E1_FILIAL = '" + xFilial("SE1") + "' AND "
cRet += " SE1.E1_MDCONTR = '" + cContrt + "' AND "
cRet += " SE1.E1_MDREVIS = '" + cRev + "'"

Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} QryCronog
Monta a query de busca de Cronograma (CNF)

@param cContrt, character, n�mero do contrato
@param cRev, character, revis�o do contrato
@param aColunas, array, array contendo o t�tulo das colunas

@return cRet, character, Query de busca na CNF

@author Mateus Boiani
@since 21/11/2018
@version 1.0 
/*/
//-------------------------------------------------------------------
Static Function QryCronog(cContrt, cRev, aColunas)
Local cRet := ""
Local nX

cRet += " SELECT "
For nX := 1 to LEN(aColunas)
	cRet += " CNF." + aColunas[nX][1] + " "
	If LEN(aColunas) != nX
		cRet += " , "
	EndIf
Next
cRet += " FROM " + RetSqlName('CNF') + " CNF "
cRet += " WHERE CNF.D_E_L_E_T_ = ' ' AND "
cRet += " CNF.CNF_FILIAL = '" + xFilial("SE1") + "' AND "
cRet += " CNF.CNF_CONTRA = '" + cContrt + "' AND "
cRet += " CNF.CNF_REVISA = '" + cRev + "'"

Return cRet
/*
Function At870Vint(aCtrs)

FwMsgRun(Nil,{|| At870Vint2(aCtrs)}, Nil, "Processando") //STR0016

Return

Static Function At870Vint2(aCtrs)
Local nX
Local cContra
Local cRev
Local cAlias870c
Local cSQL
Local aTotApCNE
Local aVALCNF
Local aVALCNB
Local aVALCNA
Local lHasCNF
Local nX
Local nY
Local nZ
Local nA
Local nAux1
Local aMsg := {}
Local nDecimais
Local lErroCNA
Local lErroCNB
Local aComp := {}
Local cMsg := ""
Local lVerTEW := .F.
Local aTFJs := {}
Local aTFIs := {}
Local aTemp := {}
Local aTEWerro := {}
Local aTFICerta := {}
Local nTFJAtivo := 0
Local lHasRev := .F.
Local aAreaTFI
Local aArea
Local aValTFI
Local aCposTFI
Local nMaior
Local aTEWFinal := {}
Local nRegs := 0
If MsgYesNo("Essa opera��o verificar� " + cValToChar(LEN(aCtrs)) + " contrato(s). Deseja continuar?")
	lVerTEW := MsgYesNo("Deseja tamb�m verificar integridade da tabela de Movimenta��es de Equipamentos (TEW)? Esta opera��o pode demorar v�rios minutos.")
	For nZ := 1 to LEN(aCtrs)
		
		Conout("Contrato " + cValToChar(nZ) + " - " + aCtrs[nZ][1])
		
		aTotApCNE := {}
		aVALCNF := {}
		aVALCNB := {}
		aVALCNA := {}
		
		cSQL := ""
		lHasCNF := .F.
		
		cContra := aCtrs[nZ][1]
		cRev := aCtrs[nZ][2]
		cAlias870c := GetNextAlias()
		
		cSql += " SELECT SUM(CNE.CNE_VLTOT) AS SUMCNE, CNE.CNE_NUMERO, CNE.CNE_ITEM FROM " + RetSqlName('CNE') + " CNE "
		cSql += " WHERE CNE.D_E_L_E_T_ = ' ' AND "
		cSql += " CNE.CNE_FILIAL = '" + xFilial("CNE") + "' AND "
		cSql += " CNE.CNE_CONTRA = '" + cContra + "' AND "
		cSql += " CNE.CNE_REVISA = '" + cRev + "'"
		cSql += " GROUP BY CNE.CNE_NUMERO, CNE.CNE_ITEM "
		cSql := ChangeQuery(cSql)
		dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAlias870c, .F., .T.)
		
		While !(cAlias870c)->(EOF())
			AADD(aTotApCNE, { (cAlias870c)->(SUMCNE) , (cAlias870c)->(CNE_NUMERO) , (cAlias870c)->(CNE_ITEM)} )
			(cAlias870c)->(DbSkip())
		End
		
		(cAlias870c)->(DbCloseArea())
		
		DbSelectArea("CNF")
		DbSetOrder(2)
		lHasCNF := DbSeek(xFilial("CNF")+cContra+cRev)
		
		If lHasCNF
			cAlias870c := GetNextAlias()
			cSQL := ""
			
			cSql += " SELECT SUM(CNF.CNF_VLPREV) AS VLPREV , "
			cSql += " SUM(CNF.CNF_VLREAL) AS VLREAL , SUM(CNF.CNF_SALDO) AS SALDO , "
			cSql += " CNF.CNF_NUMPLA "
			cSql += " FROM " + RetSqlName('CNF') + " CNF "
			cSql += " WHERE CNF.D_E_L_E_T_ = ' ' AND "
			cSql += " CNF.CNF_FILIAL = '" + xFilial("CNF") + "' AND "
			cSql += " CNF.CNF_CONTRA = '" + cContra + "' AND "
			cSql += " CNF.CNF_REVISA = '" + cRev + "'"
			cSql += " GROUP BY CNF.CNF_NUMPLA "
			cSql := ChangeQuery(cSql)
			dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAlias870c, .F., .T.)
			
			While !(cAlias870c)->(EOF())
				AADD(aVALCNF, {(cAlias870c)->(VLPREV),;
								(cAlias870c)->(VLREAL),;
								(cAlias870c)->(SALDO),;
								(cAlias870c)->(CNF_NUMPLA)} )
				(cAlias870c)->(DbSkip())
			End
			(cAlias870c)->(DbCloseArea())
		EndIf
		
		cAlias870c := GetNextAlias()
		cSQL := ""
		cSql += " SELECT CNB.CNB_NUMERO , CNB.CNB_ITEM , CNB.CNB_VLTOT , CNB.CNB_QTDMED, CNB.CNB_SLDMED "
		cSql += " FROM " + RetSqlName('CNB') + " CNB "
		cSql += " WHERE CNB.D_E_L_E_T_ = ' ' AND "
		cSql += " CNB.CNB_FILIAL = '" + xFilial("CNB") + "' AND "
		cSql += " CNB.CNB_CONTRA = '" + cContra + "' AND "
		cSql += " CNB.CNB_REVISA = '" + cRev + "' AND "
		cSql += " CNB.CNB_ITMDST = '" + Space(TamSX3("CNB_ITMDST")[1]) + "'"
		cSql := ChangeQuery(cSql)
		dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAlias870c, .F., .T.)
		
		While !(cAlias870c)->(EOF())
			AADD(aVALCNB, {(cAlias870c)->(CNB_NUMERO),;
							(cAlias870c)->(CNB_ITEM),;
							(cAlias870c)->(CNB_VLTOT),;
							(cAlias870c)->(CNB_QTDMED),;
							(cAlias870c)->(CNB_SLDMED)} )
			(cAlias870c)->(DbSkip())
		End
		(cAlias870c)->(DbCloseArea())
		
		cAlias870c := GetNextAlias()
		cSQL := ""
		cSql += " SELECT CNA.CNA_NUMERO , CNA.CNA_VLTOT , CNA.CNA_SALDO "
		cSql += " FROM " + RetSqlName('CNA') + " CNA "
		cSql += " WHERE CNA.D_E_L_E_T_ = ' ' AND "
		cSql += " CNA.CNA_FILIAL = '" + xFilial("CNA") + "' AND "
		cSql += " CNA.CNA_CONTRA = '" + cContra + "' AND "
		cSql += " CNA.CNA_REVISA = '" + cRev + "'"
		cSql := ChangeQuery(cSql)
		dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAlias870c, .F., .T.)
		
		While !(cAlias870c)->(EOF())
			AADD(aVALCNA, {(cAlias870c)->(CNA_NUMERO),;
							(cAlias870c)->(CNA_VLTOT),;
							(cAlias870c)->(CNA_SALDO)} )
			(cAlias870c)->(DbSkip())
		End
		(cAlias870c)->(DbCloseArea())
		
		//Integridade CNA_SALDO 
		lErroCNA := .F.
		lErroCNB := .F.
		
		For nX := 1 To LEN(aVALCNA)
			nAux1 := 0
			If lHasCNF
				For nY := 1 To LEN(aVALCNF)
					If aVALCNF[nY][4] == aVALCNA[nX][1]
						nAux1 += aVALCNF[nY][3]
					EndIf
				Next nY
				If aVALCNA[nX][3] != nAux1
					lErroCNA := .T.
					AADD(aMsg,{"Saldos incompat�veis CNA x CNF (campo CNA_SALDO)",;
								"Planilha: " + aVALCNA[nX][1],;
								"Valor Esperado: "+ cValToChar(nAux1),;
								"Valor Encontrado: " + cValToChar(aVALCNA[nX][3])})
				EndIf
			EndIf
			nAux1 := 0
			
			For nY := 1 TO LEN(aTotApCNE)
				If aTotApCNE[nY][2] == aVALCNA[nX][1]
					nAux1 += aTotApCNE[nY][1]
				EndIf
			Next nY
			
			If aVALCNA[nX][2] - nAux1 != aVALCNA[nX][3]
				lErroCNA := .T.
				AADD(aMsg,{"Saldos incompat�veis CNA x CNE (campo CNA_SALDO)",;
							"Planilha: " + aVALCNA[nX][1],;
							"Valor Esperado: "+ cValToChar(aVALCNA[nX][2] - nAux1),;
							"Valor Encontrado: " + cValToChar(aVALCNA[nX][3])})
			Endif
		Next nX
		
		//Integridade CNB_QTDMED
		For nX := 1 To LEN(aVALCNB)
			nAux1 := 0
			For nY := 1 TO LEN(aTotApCNE)
				If aTotApCNE[nY][2] == aVALCNB[nX][1] .AND. aTotApCNE[nY][3] == aVALCNB[nX][2]
					nAux1 += aTotApCNE[nY][1]
				EndIf
			Next nY
			If AT(".",cValToChar(aVALCNB[nX][4])) > 0
				nDecimais := LEN(SUBSTR(cValToChar(aVALCNB[nX][4]),AT(".",cValToChar(aVALCNB[nX][4])) + 1))
			Else
				nDecimais := 0
			Endif
			If NOROUND((nAux1 / aVALCNB[nX][3]),nDecimais) != aVALCNB[nX][4]
				lErroCNB := .T.
				AADD(aMsg,{"Saldos incompat�veis CNB x CNE (campo CNB_QTDMED)",;
							"Planilha: " + aVALCNB[nX][1] + " / " + "Item: " + aVALCNB[nX][2],;
							"Valor Esperado: "+ cValToChar(NOROUND((nAux1 / aVALCNB[nX][3]),nDecimais)),;
							"Valor Encontrado: " + cValToChar(aVALCNB[nX][4])})
			EndIf
		Next nX
		
		VerificCNB(cContra,cRev, @aMsg)
		
		If lVerTEW
			cAlias870c := GetNextAlias()
			cSQL := ""
			aTFJs := {}
			aTFIs := {}
			aTemp := {}
			aTEWerro := {}
			aTFICerta := {}
			nTFJAtivo := 0
			nMaior := 0
			cSql += " SELECT TFJ.TFJ_CODIGO, TFJ.TFJ_STATUS FROM " + RetSqlName('TFJ') + " TFJ "
			cSql += " WHERE TFJ.D_E_L_E_T_ = ' ' AND TFJ.TFJ_FILIAL = '" + xFilial("TFJ") + "' AND "
			cSql += " TFJ.TFJ_CONTRT = '" + cContra + "' AND TFJ.TFJ_STATUS NOT IN ('2','4')"
			cSql := ChangeQuery(cSql)
			dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAlias870c, .F., .T.)
			While !(cAlias870c)->(EOF())
				AADD(aTFJs, {(cAlias870c)->(TFJ_CODIGO),;
								(cAlias870c)->(TFJ_STATUS)} )
				If (cAlias870c)->(TFJ_STATUS) == '1'
					nTFJAtivo := LEN(aTFJs)
				EndIf
				(cAlias870c)->(DbSkip())
			End
			(cAlias870c)->(dbCloseArea())
			
			For nX := 1 To LEN(aTFJs)
				cSQL := ""
				cAlias870c := GetNextAlias()
				aTemp := {}
				cSql += " SELECT TFI.TFI_COD FROM " + RetSqlName('TFI') + " TFI "
				cSql += " INNER JOIN " + RetSqlName('TFL') + " TFL ON TFL.TFL_FILIAL = '"
				cSql += xFilial("TFL") + "' AND TFI.TFI_CODPAI = TFL.TFL_CODIGO AND "
				cSql += " TFL.D_E_L_E_T_ = ' ' "
				cSql += " WHERE TFI.TFI_FILIAL = '" + xFilial("TFI") + "' AND TFI.D_E_L_E_T_ = ' ' "
				cSql += " AND TFL.TFL_CODPAI = '" + aTFJs[nX][1] + "'"
				cSql := ChangeQuery(cSql)
				dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAlias870c, .F., .T.)
				While !(cAlias870c)->(EOF())
					AADD(aTemp, (cAlias870c)->(TFI_COD))
					(cAlias870c)->(DbSkip())
				End
				(cAlias870c)->(dbCloseArea())
				AADD(aTFIs, {aTFJs[nX][1], aTFJs[nX][2] == '1', aTemp})
			Next nX

			cSQL := ""
			cAlias870c := GetNextAlias()
			lHasRev := .F.
			
			cSql += " SELECT TEW.TEW_CODEQU, TEW.TEW_ORCSER, TEW.R_E_C_N_O_ FROM " + RetSqlName('TEW') + " TEW "
			cSql += " WHERE TEW.TEW_ORCSER IN ( "
			For nY := 1 to LEN(aTFJs)
				If nY != nTFJAtivo
					cSql += " '" + aTFJs[nY][1] + "',"
					lHasRev := .T.
				EndIf
			Next nY
			cSql := LEFT(cSql, LEN(cSql)-1)
			cSql += " ) AND TEW.TEW_FILIAL = '" + xFilial("TEW") + "'"
			cSql += " AND TEW.D_E_L_E_T_ = ' '"
			
			If lHasRev
				cSql := ChangeQuery(cSql)
				dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAlias870c, .F., .T.)
				While !(cAlias870c)->(EOF())
					AADD(aTEWerro, {(cAlias870c)->(TEW_CODEQU),;
									(cAlias870c)->(TEW_ORCSER),;
									(cAlias870c)->(R_E_C_N_O_),;
									"",;
									""})
					(cAlias870c)->(DbSkip())
				End
				(cAlias870c)->(dbCloseArea())
				
				For nY := 1 to LEN(aTFIs)
					If !(aTFIs[nY][2]) .AND. LEN(aTFIs[nY][3]) > 0 //Or�amento n�o ativo
						lEntrou := .F.
						cSQL := ""
						cAlias870c := GetNextAlias()
						cSql += " SELECT TEW.TEW_CODEQU, TEW.TEW_ORCSER, TEW.R_E_C_N_O_ FROM " + RetSqlName('TEW') + " TEW "
						cSql += " WHERE TEW.TEW_CODEQU IN ( "
						
						For nX := 1 To LEN(aTFIs[nY][3])
							cSql += " '" + aTFIs[nY][3][nX] + "',"
							lEntrou := .T.
						Next nX 
						
						cSql := LEFT(cSql, LEN(cSql)-1)
						cSql += " ) AND TEW.TEW_FILIAL = '" + xFilial("TEW") + "'"
						cSql += " AND TEW.D_E_L_E_T_ = ' '"
						If lEntrou
							cSql := ChangeQuery(cSql)
							dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAlias870c, .F., .T.)
							While !(cAlias870c)->(EOF())
								If ( nA := ASCAN(aTEWerro, {|q| q[3] == (cAlias870c)->(R_E_C_N_O_)}) ) == 0
									AADD(aTEWerro, {(cAlias870c)->(TEW_CODEQU),;
													(cAlias870c)->(TEW_ORCSER),;
													(cAlias870c)->(R_E_C_N_O_),;
													"",;
													""})
								Else 
									aArea := GetArea()
									TEW->(dbGoTo(aTEWerro[nA][3]))
									If TFI->(DbSeek(xFilial("TFI")+TEW->TEW_CODEQU))
										If TFL->(DBseek(xFilial("TFL")+TFI->TFI_CODPAI))
											aTEWerro[nA][2] := TFL->TFL_CODPAI
										EndIf
									EndIf
									RestArea(aArea)
								EndIf
								(cAlias870c)->(DbSkip())
							End
							(cAlias870c)->(dbCloseArea())
						EndIf
					EndIf
				Next nY
			EndIf
			
			If !Empty(aTEWerro)
				aAreaTFI := TFI->(GetArea())
				aArea := GetArea()
				For nY := 1 To LEN(aTEWerro)
					DbSelectArea("TFI") //TFI_FILIAL + TFI_COD
					TFI->(DbSetOrder(1))
					If TFI->(DbSeek(xFilial("TFI") + aTEWerro[nY][1]))
						cSQL := ""
						cAlias870c := GetNextAlias()
						cSql += " SELECT TFI.R_E_C_N_O_ FROM " + RetSqlName('TFI') + " TFI "
						cSql += " INNER JOIN " + RetSqlName('TFL') + " TFL "
						cSql += " ON TFI.TFI_CODPAI = TFL.TFL_CODIGO AND TFL.TFL_FILIAL = '"
						cSql += xFilial("TFL") + "' AND TFL.D_E_L_E_T_ = ' ' "
						cSql += " WHERE TFI.TFI_PRODUT = '" + TFI->TFI_PRODUT + "' AND "
						cSql += " TFI.TFI_ITEM = '" + TFI->TFI_ITEM + "' AND TFI.R_E_C_N_O_ <> " + cValToChar(TFI->(Recno())) + " "
						cSql += " AND TFI.D_E_L_E_T_ = ' ' AND TFL.TFL_CODPAI = '" + aTFJs[nTFJAtivo][1] + "' "
						cSql := ChangeQuery(cSql)
						dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAlias870c, .F., .T.)
						While !(cAlias870c)->(EOF())
							AADD(aTFICerta, {(cAlias870c)->(R_E_C_N_O_),0})
							(cAlias870c)->(DbSkip())
						End
						(cAlias870c)->(dbCloseArea())
						If !(Empty(aTFICerta)) .AND. LEN(aTFICerta) == 1
							TFI->(DbGoTo(aTFICerta[1][1]))
							aTEWerro[nY][4] := TFI->TFI_COD
							aTEWerro[nY][5] := aTFJs[nTFJAtivo][1]
						ElseIf LEN(aTFICerta) > 1
							aValTFI := {TFI->TFI_LOCAL,TFI->TFI_PERINI,TFI->TFI_QTDVEN,TFI->TFI_TOTAL}
							aCposTFI := {"TFI->TFI_LOCAL","TFI->TFI_PERINI","TFI->TFI_QTDVEN","TFI->TFI_TOTAL"}
							For nX := 1 To LEN(aTFICerta)
								TFI->(dbGoTo(aTFICerta[nX][1]))
								For nA := 1 TO LEN(aValTFI)
									If aValTFI[nA] == (&(aCposTFI[nA]))
										aTFICerta[nX][2]++
									EndIf
								Next nA
							Next
							For nA := 1 To LEN(aTFICerta)
								If aTFICerta[nA][2] > nMaior
									nMaior := aTFICerta[nA][2]
								EndIf
							Next nA
							
							TFI->(DBGoTo(aTFICerta[ASCAN(aTFICerta, {|s| s[2] == nMaior})][1]))
							aTEWerro[nY][4] := TFI->TFI_COD
							aTEWerro[nY][5] := aTFJs[nTFJAtivo][1]
						EndIf
					EndIf
					
					If Empty(aTEWerro[nY][4]) .AND. Empty(aTEWerro[nY][5])
						cSQL := ""
						cAlias870c := GetNextAlias()
						cSql += " SELECT 1 FROM " + RetSqlName('TFJ') + " TFJ "
						cSql += " WHERE TFJ.D_E_L_E_T_ = '*' AND TFJ_FILIAL = '" + xFilial("TFJ") + "'"
						cSql += " AND TFJ_CODIGO = '" + aTEWerro[nY][2] + "'"
						cSql := ChangeQuery(cSql)
						dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAlias870c, .F., .T.)
						If !(cAlias870c)->(EOF())
							(cAlias870c)->(dbCloseArea())
							cSQL := ""
							cAlias870c := GetNextAlias()
							cSql += " SELECT 1 FROM " + RetSqlName('TFJ') + " TFJ "
							cSql += " WHERE TFJ.D_E_L_E_T_ = ' ' AND TFJ_FILIAL = '" + xFilial("TFJ") + "'"
							cSql += " AND TFJ_CODIGO = '" + aTEWerro[nY][2] + "'"
							cSql := ChangeQuery(cSql)
							dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAlias870c, .F., .T.)
							If (cAlias870c)->(EOF())
								aTEWerro[nY][2] += " (registro deletado)"
							EndIf
							(cAlias870c)->(dbCloseArea())
						Else
							(cAlias870c)->(dbCloseArea())
						EndIf
						//--
						cSQL := ""
						cAlias870c := GetNextAlias()
						cSql += " SELECT 1 FROM " + RetSqlName('TFI') + " TFI "
						cSql += " WHERE TFI.D_E_L_E_T_ = '*' AND TFI_FILIAL = '" + xFilial("TFI") + "'"
						cSql += " AND TFI_COD = '" + aTEWerro[nY][1] + "'"
						cSql := ChangeQuery(cSql)
						dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAlias870c, .F., .T.)
						If !(cAlias870c)->(EOF())
							(cAlias870c)->(dbCloseArea())
							cSQL := ""
							cAlias870c := GetNextAlias()
							cSql += " SELECT 1 FROM " + RetSqlName('TFI') + " TFI "
							cSql += " WHERE TFI.D_E_L_E_T_ = ' ' AND TFI_FILIAL = '" + xFilial("TFI") + "'"
							cSql += " AND TFI_COD = '" + aTEWerro[nY][1] + "'"
							cSql := ChangeQuery(cSql)
							dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAlias870c, .F., .T.)
							If (cAlias870c)->(EOF())
								aTEWerro[nY][1] += " (registro deletado)"
							EndIf
							(cAlias870c)->(dbCloseArea())
						Else
							(cAlias870c)->(dbCloseArea())
						EndIf
					EndIf
				Next nY
				RestArea(aAreaTFI)
				RestArea(aArea)
				
				If !Empty(aTEWerro)
					AADD(aTEWFinal, {cContra, cRev , aTEWerro})
				EndIf
			EndIf
		EndIf
		
		
		
		If lErroCNB .OR. lErroCNA .OR. !EMPTY(aMsg)
			AADD(aComp,{ cContra, cRev , aMsg})
			aMsg := {}
		EndIf
	Next nZ
EndIf

If !EMPTY(aComp)
	For nX := 1 To Len(aComp)
		cMsg += CRLF
		cMsg += REPLICATE("-",30)
		cMsg += CRLF
		cMsg += "Contrato: " + aComp[nX][1]
		cMsg += CRLF
		cMsg += "Revis�o: " + aComp[nX][2]
		cMsg += CRLF
		cMsg += CRLF
		For nY := 1 To LEN(aComp[nX][3])
			cMsg += aComp[nX][3][nY][1] + CRLF
			cMsg += aComp[nX][3][nY][2] + CRLF
			cMsg += aComp[nX][3][nY][3] + CRLF
			cMsg += aComp[nX][3][nY][4] + CRLF
			cMsg += CRLF
		Next nY
		cMsg += CRLF
		cMsg += CRLF
	Next nX
	AtShowLog(cMsg, "Log de Inconsist�ncias", .T., .T., .T.,.F.)
ElseIf !lVerTEW .OR. (lVerTEW .AND. EMPTY(aTEWFinal))
	MsgInfo("Nenhuma inconsist�ncia localizada !")
EndIf

If lVerTEW .AND. !EMPTY(aTEWFinal)
	cMsg := ""
	For nX := 1 To Len(aTEWFinal)
		cMsg += CRLF
		cMsg += REPLICATE("-",30)
		cMsg += CRLF
		cMsg += "Contrato: " + aTEWFinal[nX][1]
		cMsg += CRLF
		cMsg += "Revis�o: " + aTEWFinal[nX][2]
		cMsg += CRLF
		cMsg += CRLF
		For nY := 1 To LEN(aTEWFinal[nX][3])
			cMsg += "[incorreto] TEW_ORCSER: " + aTEWFinal[nX][3][nY][2] + CRLF
			cMsg += "[incorreto] TEW_CODEQU: " + aTEWFinal[nX][3][nY][1] + CRLF
			cMsg += "[correto] TEW_ORCSER: " + aTEWFinal[nX][3][nY][5] + CRLF
			cMsg += "[correto] TEW_CODEQU: " + aTEWFinal[nX][3][nY][4] + CRLF
			cMsg += CRLF
		Next nY
		cMsg += CRLF
	Next nX
	AtShowLog(cMsg, "Log de Inconsist�ncias (TEW)", .T., .T., .T.,.F.)
	If MsgNoYes("Deseja corrigir automaticamente a tabela de Movimenta��es (TEW)?")
		DBSelectArea("TEW")
		For nX := 1 TO LEN(aTEWFinal)
			For nY := 1 To LEN(aTEWFinal[nX][3])
				If !EMPTY(aTEWFinal[nX][3][nY][5]) .AND. !EMPTY(aTEWFinal[nX][3][nY][4])
					TEW->(DBGoTo(aTEWFinal[nX][3][nY][3]))
					If TEW->TEW_ORCSER == aTEWFinal[nX][3][nY][2] .AND.;
						TEW->TEW_CODEQU == aTEWFinal[nX][3][nY][1]
						RecLock("TEW", .F.)
							TEW->TEW_ORCSER := aTEWFinal[nX][3][nY][5]
							TEW->TEW_CODEQU := aTEWFinal[nX][3][nY][4]
						MsUnLock()
						nRegs++
					EndIf
				EndIf
			Next
		Next
		MsgInfo(cValtoChar(nRegs) + " registro(s) alterados.")
	EndIf
EndIf

Return

Function ProcCNBGS(oView)
Local aRows	:= FWSaveRows()
Local oModel := oView:GetModel()
Local nX
Local oModelCNA := oModel:GetModel("CNADETAIL")
Local nY
Local aAux
For nX := 1 TO oModelCNA:Length()
	oModelCNA:GoLine(nX)
	oModelCNB := oModel:GetModel("CNBDETAIL")
	aAux := {}
	For nY := 1 To oModelCNB:Length()
		oModelCNB:GoLine(nY)
		If !EMPTY(oModelCNB:GetValue("CNB_ITMDST"))
			If ASCAN(aAux, {|a| a[1] == oModelCNB:GetValue("CNB_ITMDST")}) > 0
				aAux[ASCAN(aAux, {|a| a[1] == oModelCNB:GetValue("CNB_ITMDST")})][2] += oModelCNB:GetValue("CNB_VLTOT")
			Else
				AADD(aAux, {oModelCNB:GetValue("CNB_ITMDST") ,oModelCNB:GetValue("CNB_VLTOT"), oModelCNB:GetValue("CNB_PRODUT")})
			EndIf
		EndIf
	Next nY
	
	oModelCNB:GoLine(1)
	
	For nY := 1 To oModelCNB:Length()
		oModelCNB:GoLine(nY)
		If EMPTY(oModelCNB:GetValue("CNB_ITMDST")) .AND. oModelCNB:GetValue("CNB_QUANT") != 1
			oModelCNB:LoadValue("CNB_VLUNIT",oModelCNB:GetValue("CNB_VLUNIT") - aAux[;
				ASCAN(aAux, {|a| a[1] == oModelCNB:GetValue("CNB_ITEM") .AND. a[3] == oModelCNB:GetValue("CNB_PRODUT")})][2])
			oModelCNB:SetValue("CNB_QUANT",1)
		EndIf
	Next nY
	
Next

If Len(aRows) > 0
	FWRestRows(aRows)
EndIf

oView:Refresh()

Return

Static Function VerificCNB(cContra,cRev, aRet)
Local aArea := GetArea()
Local cAliasTemp := GetNextAlias()
Local cSQL := ""
Local aAux := {}
Local nX

cSql += "SELECT "
cSql += " TFL.TFL_PLAN, TFL.TFL_ITPLRH, TFL.TFL_ITPLMI, TFL.TFL_ITPLMC, TFL.TFL_ITPLLE, "
cSql += " TFJ.TFJ_GRPRH, TFJ.TFJ_GRPMI, TFJ.TFJ_GRPMC, TFJ.TFJ_GRPLE, TFL.TFL_CODIGO "
cSql += " FROM " + RetSqlName('TFL') + " TFL "
cSql += " JOIN " + RetSqlName('TFJ') + " TFJ ON "
cSql += " TFJ.TFJ_CODIGO = TFL.TFL_CODPAI AND "
cSql += " TFJ.TFJ_FILIAL = '" + xFilial("TFJ") + "' "
cSql += " WHERE TFL.D_E_L_E_T_ = ' ' AND TFJ.D_E_L_E_T_ = ' ' "
cSql += " AND TFL.TFL_CONTRT = '" + cContra + "' AND "
cSql += " TFL.TFL_CONREV = '" + cRev + "' AND "
cSql += " TFL.TFL_FILIAL = '" + xFilial("TFL") + "' AND "
cSql += " TFJ.TFJ_GRPRH != '" + SPACE(TamSX3("TFJ_GRPRH")[1]) + "' "

cSql := ChangeQuery(cSql)
dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAliasTemp, .F., .T.)

While !(cAliasTemp)->(EOF())
	AADD(aAux, {;
					(cAliasTemp)->TFL_PLAN,;		//1
		 			(cAliasTemp)->TFL_ITPLRH,;		//2
		 			(cAliasTemp)->TFL_ITPLMI,;		//3
		 			(cAliasTemp)->TFL_ITPLMC,;		//4
		 			(cAliasTemp)->TFL_ITPLLE,;		//5
					(cAliasTemp)->TFJ_GRPRH,;		//6
					(cAliasTemp)->TFJ_GRPMI,;		//7
					(cAliasTemp)->TFJ_GRPMC,;		//8
					(cAliasTemp)->TFJ_GRPLE,;		//9
					(cAliasTemp)->TFL_CODIGO;		//10
				})
	(cAliasTemp)->(DbSkip())
End

(cAliasTemp)->(DbCloseArea())

For nX := 1 To LEN(aAux)
	cAliasTemp := GetNextAlias()
	cSql := "SELECT "
	cSql += " CNB.CNB_PRODUT , CNB.CNB_ITEM "
	cSql += " FROM " + RetSqlName('CNB') + " CNB "
	cSql += " WHERE CNB.D_E_L_E_T_ = ' ' AND "
	cSql += " CNB.CNB_CONTRA = '" + cContra + "' AND CNB.CNB_REVISA = '" + cRev + "' AND "
	cSql += " CNB.CNB_FILIAL = '" + xFilial("CNB") + "' AND CNB.CNB_NUMERO = '" + aAux[nX][1] + "' AND "
	cSql += " CNB.CNB_ITMDST = '" + SPACE(TamSX3("CNB_ITMDST")[1]) + "' "
	cSql := ChangeQuery(cSql)
	
	dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAliasTemp, .F., .T.)
	
	While !(cAliasTemp)->(EOF())
		If (cAliasTemp)->CNB_PRODUT == aAux[nX][6] .AND. aAux[nX][2] != (cAliasTemp)->CNB_ITEM
			AADD(aRet,{"Valor incompat�vel TFL_ITPLRH x CNB_ITEM ",;
						"C�digo (TFL_CODIGO): " + aAux[nX][10] + " / Planilha (TFL_PLAN): " + aAux[nX][1] + " / Produto: " + aAux[nX][6],;
						"Valor Esperado (TFL_ITPLRH): "+ (cAliasTemp)->CNB_ITEM,;
						"Valor Encontrado (TFL_ITPLRH): " + aAux[nX][2]})
		EndIf
		
		If (cAliasTemp)->CNB_PRODUT == aAux[nX][7] .AND. aAux[nX][3] != (cAliasTemp)->CNB_ITEM
			AADD(aRet,{"Valor incompat�vel TFL_ITPLMI x CNB_ITEM ",;
						"C�digo (TFL_CODIGO): " + aAux[nX][10] + " / Planilha (TFL_PLAN): " + aAux[nX][1] + " / Produto: " + aAux[nX][7],;
						"Valor Esperado (TFL_ITPLMI): "+ (cAliasTemp)->CNB_ITEM,;
						"Valor Encontrado (TFL_ITPLMI): " + aAux[nX][3]})
		EndIf
		
		If (cAliasTemp)->CNB_PRODUT == aAux[nX][8] .AND. aAux[nX][4] != (cAliasTemp)->CNB_ITEM
			AADD(aRet,{"Valor incompat�vel TFL_ITPLMC x CNB_ITEM ",;
						"C�digo (TFL_CODIGO): " + aAux[nX][10] + " / Planilha (TFL_PLAN): " + aAux[nX][1] + " / Produto: " + aAux[nX][8],;
						"Valor Esperado (TFL_ITPLMC): "+ (cAliasTemp)->CNB_ITEM,;
						"Valor Encontrado (TFL_ITPLMC): " + aAux[nX][4]})
		EndIf
		
		If (cAliasTemp)->CNB_PRODUT == aAux[nX][9] .AND. aAux[nX][5] != (cAliasTemp)->CNB_ITEM
			AADD(aRet,{"Valor incompat�vel TFL_ITPLLE x CNB_ITEM ",;
						"C�digo (TFL_CODIGO): " + aAux[nX][10] + " / Planilha (TFL_PLAN): " + aAux[nX][1] + " / Produto: " + aAux[nX][9],;
						"Valor Esperado (TFL_ITPLLE): "+ (cAliasTemp)->CNB_ITEM,;
						"Valor Encontrado (TFL_ITPLLE): " + aAux[nX][5]})
		EndIf
		
		(cAliasTemp)->(DbSkip())
	End
	(cAliasTemp)->(dbCloseArea())
Next

RestArea(aArea)
Return
*/