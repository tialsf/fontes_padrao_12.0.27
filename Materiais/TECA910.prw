#INCLUDE "PROTHEUS.CH"
#INCLUDE "TECA910.CH"
#INCLUDE "FILEIO.CH"

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TECA910()

Gera as marca��es atraves do atendimento da O.S

@return ExpL: Retorna .T. quando teve sucesso na opera��o
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TECA910(lSemTela, aParams, aAtendentes)
Local cQuery		:= ""
Local cAlias		:= ""	// Alias
Local nTotal		
Local oDlg
Local oPanTop
Local oPanBot
Local oFont
Local nMeter
Local oMeter
Local oSay
Local oSayMsg
Local dDataIni
Local dDataFim
Local lContinua		:= .T.
Local cMvGsxInt 	:= SuperGetMv("MV_GSXINT",,"2")
Local lMV_GSXINT	:= cMvGsxInt  <> "2"
Local cTpExp 		:= SuperGetMV("MV_GSOUT", .F., "1") //1 - Integra��o RH protheus(Default) - 2 Ponto de Entrada - 3 Arquivo CSV
Local cDirArq 		:= At910RHD()
Local nHandle 		:= 0
Local aRet 			:= {.T.,{}}
Local cMsg 			:= ""
Local nHandle2 		:= 0 //Handle para exporta��o do RM
Local aEmpFil 		:= {}
Local lMultFil		:= TecHasPerg("MV_PAR08","TEC910")
Local cFilBkp		:= cFilAnt
Local aMtFil		:= {}
Local nX			:= 0

Default lSemTela 	:= .F.
Default aParams 	:= {}
Default aAtendentes := {}

DbSelectArea("ABB")
If SuperGetMV("MV_GSGEROS",.F.,"1") == "2" .And. ABB->( ColumnPos("ABB_MPONTO") ) == 0
	aRet[1] 	:= .F.
	lContinua 	:= .F.
	cMsg 		:= STR0040+" "+STR0041
	Help(,, "TECA910",,STR0040,1,0,,,,,,{STR0041}) //"O par�metro MV_GSGEROS est� preenchido com '2', mas o campo ABB_MPONTO n�o foi encontrado."##"Realize a inclus�o do campo."
	aAdd(aRet[2], cMsg)
Endif

//----------------------------------------------------------------------------
// Parametros Utilizados no Pergunte
//
// MV_PAR01: Atendente De ?
// MV_PAR02: Atendente Ate ?
// MV_PAR03: Data Inicio De ?
// MV_PAR04: Data Inicio Ate ?
// MV_PAR05: Processamento ? 1=Inclusao;2=Exclusao
// MV_PAR06: Mantem Int Turnos? 1=Sim;2=N�o
// MV_PAR07: Quantidade de Minutos
// MV_PAR08: Filial para gera��o dos atendimentos. Vazio = Filial atual
// MV_PAR09: Local De?
// MV_PAR10: Local At�?
//------------------------------------------------------------------------------
If lContinua

	If  "2" $ cTpExp .and. !ExistBlock("At910CMa")
		cMsg := STR0034
		Help(,, "At910CMa",cMsg ,, 1, 0)//"Ponto de Entrada At910CMa nao compilado."
		cTpExp := StrTran(cTpExp, "2", )
		aAdd(aRet[2], cMsg)
	EndIf
	
	If !lSemTela
		lContinua := Pergunte("TEC910",.T.)
	Else
		Pergunte("TEC910",.F.)
	EndIf

	If lMultFil
		IF(EMPTY(MV_PAR08))
			aAdd(aMtfil,cFilant)
		Else
			At900PMtFl(MV_PAR08,@aMtFil)
		EndIF
	Else
		aAdd(aMtfil,cFilant)
	EndIf
	
	If !Empty(aParams)
		MV_PAR01 := aParams[ASCAN(aParams, {|d| d[1] == "MV_PAR01"})][2]
		MV_PAR02 := aParams[ASCAN(aParams, {|d| d[1] == "MV_PAR02"})][2]
		MV_PAR03 := aParams[ASCAN(aParams, {|d| d[1] == "MV_PAR03"})][2]
		MV_PAR04 := aParams[ASCAN(aParams, {|d| d[1] == "MV_PAR04"})][2]
		MV_PAR05 := aParams[ASCAN(aParams, {|d| d[1] == "MV_PAR05"})][2]
		MV_PAR06 := aParams[ASCAN(aParams, {|d| d[1] == "MV_PAR06"})][2]
		If ASCAN(aParams, {|d| d[1] == "MV_PAR07"}) > 0 
			MV_PAR07 := aParams[ASCAN(aParams, {|d| d[1] == "MV_PAR07"})][2]
		EndIf
		If ASCAN(aParams, {|d| d[1] == "MV_PAR08"}) > 0 
			MV_PAR08 := aParams[ASCAN(aParams, {|d| d[1] == "MV_PAR08"})][2]
		EndIf
		If ASCAN(aParams, {|d| d[1] == "MV_PAR09"}) > 0 
			MV_PAR09 := aParams[ASCAN(aParams, {|d| d[1] == "MV_PAR09"})][2]
		EndIf
		If ASCAN(aParams, {|d| d[1] == "MV_PAR10"}) > 0 
			MV_PAR10 := aParams[ASCAN(aParams, {|d| d[1] == "MV_PAR10"})][2]
		EndIf

	EndIf
Endif

If lContinua 
	For nX := 1 to Len(aMtFil)
		nTotal 	:= 0
		cAlias 	:= GetNextAlias()
		cFilant := aMtFil[nX]

		//Verifica Periodo de Apontamente(MV_PAPONTA)
		If "1" $ cTpExp .AND. !lMV_GSXINT
			PerAponta(@dDataIni,@dDataFim )
		Else
			dDataIni :=  MV_PAR03
			dDataFim :=  MV_PAR04
		EndIf

		If MV_PAR03 < dDataIni .OR. MV_PAR04 > dDataFim
			cMsg := STR0002+Dtoc(dDataIni)+STR0003+Dtoc(dDataFim)+STR0055+cFilant //"Imposs�vel enviar ao RH marca��es anteriores ao per�odo inicial ou posteriores ao per�odo final de apontamento "#"  (MV_PAPONTA). para a filial "
			// "Aten��o"#"Imposs�vel enviar ao RH marca��es anteriores ao per�odo inicial ou posteriores ao per�odo final de apontamento "#"OK"
			Aviso(STR0001,cMsg,{"OK"},2)
			lContinua := .F.
			aAdd(aRet[2], cMsg)
		EndIf

		If  "3" $ cTpExp
			nHandle := At910RHF("at910", cDirArq, .T., Iif(MV_PAR05==1,3,5), ".csv")

			If nHandle == -1
				lContinua := .F.
				cMsg := STR0035//"Problemas na cria��o do arquivo CSV"
				Help(,, "TECA910",cMsg ,, 1, 0)//"Problemas na cria��o do arquivo CSV"
				aAdd(aRet[2], cMsg)
			EndIf
		EndIf
		
		If  cMvGsxInt == "3" .AND. MV_PAR05==1
		
			aEmpFil := GSItEmpFil(, , "RM", .T., .T., cMsg)
			nHandle2 := At910RHF("RM_Marc", cDirArq, .T., Iif(MV_PAR05==1,3,5), ".txt")

			If nHandle2 == -1  .OR. Len(aEmpFil) = 0
				lContinua := .F.
				cMsg := STR0036//"Problemas na exporta��o do arquivo da marca��es para RM"
				Help(,, "TECA910",cMsg ,, 1, 0)
				aAdd(aRet[2], cMsg)
			EndIf
		EndIf

		If lContinua

			DbSelectArea("ABB")

			//Monta a Query para gera��o da marca��o
			cQuery := At910Qry(aAtendentes)

			cQuery := ChangeQuery( cQuery )

			dbUseArea( .T., "TOPCONN", TcGenQry( , , cQuery ), cAlias , .T. , .T. )

			TcSetField(  cAlias	, "AB9_DTINI", "D", 8, 0 )
			TcSetField(  cAlias	, "AB9_DTFIM", "D", 8, 0 )

			(cAlias)->(dbEval({||nTotal++}))

			(cAlias)->(DbGoTop())

			If nTotal > 0
				If (!isBlind() .AND. !lSemTela) .OR. !IsInCallStack("TecGeraReg")
					DEFINE MSDIALOG oDlg TITLE STR0005+cFilant FROM 0,0 TO 100,450 PIXEL STYLE DS_MODALFRAME // "Gera��o das Marca��es"
						oPanTop := TPanel():New( 0, 0, , oDlg, , , , , , 0, 0, ,  )
						oPanTop:Align := CONTROL_ALIGN_ALLCLIENT

						oPanBot := TPanel():New( 0, 0, , oDlg, , , , ,/*CLR_YELLOW*/, 0, 25 , )
						oPanBot:Align := CONTROL_ALIGN_BOTTOM

						DEFINE FONT oFont NAME "Arial" SIZE 0,16
						// "Ser�o processados "#" atendimentos para a Gera��o de Marca��es."
						@ 05,08 SAY oSay Var "<center>"+STR0006+cValToChar(nTotal)+STR0007+cFilant+"</center>" PIXEL SIZE 210,65 HTML FONT oFont PIXEL OF oPanTop

						nMeter := 0
						oMeter := TMeter():New(02,7,{|u|if(Pcount()>0,nMeter:=u,nMeter)},nTotal,oPanBot,200,100,,.T.,,,.F.)
						//"Processando..."#
						@ 10,02 SAY oSayMsg Var "<center>"+STR0008+"</center>" PIXEL SIZE 210,65 HTML FONT oFont PIXEL OF oPanBot

					ACTIVATE DIALOG oDlg CENTERED ON INIT At910GerMa(cAlias,oDlg,oMeter,oSayMsg,dDataIni,dDataFim,MV_PAR06==1,lSemTela,cTpExp,nHandle, @aRet, nHandle2, aEmpFil)

				Else
					At910GerMa(cAlias,/*oDlg*/,/*oMeter*/,/*oSayMsg*/,dDataIni,dDataFim,MV_PAR06==1,lSemTela, cTpExp, nHandle, @aRet, nHandle2, aEmpFil)
				EndIf
			Else
				
				aRet[1] := .F.
				cMsg := STR0009
				AADD(aRet[2], cMsg)
				If !lSemTela
					Aviso(STR0001,STR0057+cFilant+CRLF+cMsg,{STR0004},2) //"Aten��o"#"Filial " "N�o h� registros para gerar marca��es conforme parametros informados."#"OK"
				EndIf
			EndIf
		EndIf
		lContinua := .T.
	Next nX
EndIf

If cFilant <> cFilBkp
	cFilant := cFilBkp
ENDIF

If nHandle > 0
	fClose(nHandle)
EndIf

If nHandle2 > 0
	fClose(nHandle2)
EndIf

Return aRet
//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At910GerMa()

Realiza o Processamento do Pergunte na gera��o de marcacoes

@param ExpC:Alias da Tabela de processamento
@param ExpO:Dialog do Processamento
@param ExpO:Tmeter para atualizar o processamento
@param ExpO:Texto do processamento
@param ExpD:Data Inicial Limite
@param ExpD:Data Final Limite
@param ExpL:Gera marca��o com os intervalos dos turno?

@return ExpL: Retorna .T. quando houve sucesso na opera��o
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function At910GerMa(cAlias,oDlg,oMeter,oSayMsg,dDataIni,dDatafim,lGeraMarcInt,lSemTela, cTpExp, nHandle, aRet, nHandle2, aEmpFil)

Local nCritica		:= 0
Local nX			:= 0
Local nY			:= 0
Local lRet 			:= .F.
Local nReg			:= 0
Local aRetorno		:= {}
Local aMarcacao		:= {}
Local cMsg			:= ""
Local cMsgRM		:= ""
Local cCdFunc		:= ""
Local dMarcIni
Local cMarcIni
Local dMarcFim
Local cMarcFim
Local aMarcTec 		:= {}//Armazena marca��es do atendente para enviar ao M�dulo de ponto eletronico
Local cAtendOld 	:= ""//controle do codigo do atendente
Local lLimite 		:= .F.
Local lGSxInt		:= SuperGetMV("MV_GSXINT",,"2") == "3"
Local lTDXIniHor	:= TDX->( ColumnPos('TDX_INIHOR') ) > 0 
Local lTDWCodHor	:= TDW->( ColumnPos('TDW_CODHOR') ) > 0
Local cTDWIniHor	:= ""
Local cTDWCodHor	:= ""
Local lMostra		:= .T.
Local lEnvia		:= MV_PAR05 == 1
Local dRMDatIni 
Local aEscala		:= {}
Local aEscTGY		:= {}
Local cCodTurno		:= ""
Local cAliasTGY		:= GetNextAlias()
Local cOk           := ""

Default lSemTela := .F.
Default aRet := {.T.,{}}

While !(cAlias)->(Eof())
		
	If lGSxInt
		dRMDatIni	:= (cAlias)->AB9_DTINI
	EndIf
	If (!IsBlind() .AND. !lSemTela ).OR. !IsInCallStack("TecGeraReg")
		oMeter:Set(++nReg) // Atualiza Gauge/Tmeter
		oSayMsg:SetText("<center>"+STR0011+cValToChar(nReg)+"</center>")      // "Processando..."
	EndIf
	cMarcIni	:= (cAlias)->AB9_HRINI
	cMarcFim	:= (cAlias)->AB9_HRFIM
	

	//Impede envio de marca��es anteriores ao periodo inicial do apontamento
	If (cAlias)->TDV_DTREF < DTOS(dDataIni) .OR. (cAlias)->TDV_DTREF > DTOS(dDataFim)
		nCritica++
		// "Imposs�vel enviar ao RH marca��es anteriores ao per�odo inicial ou posteriores ao per�odo final de apontamento "#" a "
		At910Log(cAlias,,STR0012+Dtoc(dDataIni)+STR0013+Dtoc(dDataFim)+"  (MV_PAPONTA).",@aRet)
	Else
		If (cAlias)->AB9_DTINI == (cAlias)->AB9_DTFIM
			dMarcIni	:= (cAlias)->AB9_DTINI
			dMarcFim	:= (cAlias)->AB9_DTFIM
			At910AProc(@aRetorno,cAlias,dMarcIni,cMarcIni,dMarcFim,cMarcFim)
		ElseIf (cAlias)->AB9_DTFIM > (cAlias)->AB9_DTINI
			//Quando hora final > inicial � pq a marca��o final � no dia seguinte. Ex. das 20:00 as 05:00
			If (cAlias)->AB9_HRINI > (cAlias)->AB9_HRFIM
				//Faz por periodo. Se mais de mais de 1 dia de diferen�a.
				//Ex. Se de 01/10 a 03/10 das 20:00 as 05:00 gera atendimento:
				//Entrada - 01/10 as 20:00 saida 02/10 as 05:00
				//Entrada - 02/10 as 20:00 saida 03/10 as 05:00
				For nX := 0 To (((cAlias)->AB9_DTFIM) - ((cAlias)->AB9_DTINI) - 1)
					dMarcIni	:= (cAlias)->AB9_DTINI + nX
					dMarcFim	:= (cAlias)->AB9_DTINI + nX + 1
					At910AProc(@aRetorno,cAlias,dMarcIni,cMarcIni,dMarcFim,cMarcFim)
				Next nX
			Else //(cAlias)->AB9_HRINI <= (cAlias)->AB9_HRFIM
				//Faz por periodo quando hora inicial < hora final. Ex. de 01 a 02 das 08:00 as 17:00
				//Entrada - 01/10 as 08:00 saida 01/10 as 17:00
				//Entrada - 02/10 as 08:00 saida 02/10 as 17:00
				For nX := 0 To (((cAlias)->AB9_DTFIM) - ((cAlias)->AB9_DTINI))
					dMarcIni	:= (cAlias)->AB9_DTINI + nX
					dMarcFim	:= (cAlias)->AB9_DTINI + nX
					At910AProc(@aRetorno,cAlias,dMarcIni,cMarcIni,dMarcFim,cMarcFim)
				Next nX
			EndIf
		EndIf
		If lGSxInt
			If (lTDXIniHor .AND. lTDWCodHor)
				If (cAlias)->ABB_TIPOMV == '001' .AND. (cCodTurno <> (cAlias)->TDV_TURNO .OR. cCdFunc <> (cAlias)->AA1_CDFUNC)
					QryEscala( @cAliasTGY, (cAlias)->TDV_TURNO, (cAlias)->TDV_CODABB, (cAlias)->TDV_SEQTRN )
					cTDWIniHor	:= (cAliasTGY)->TDX_INIHOR
					cTDWCodHor	:= (cAliasTGY)->TDW_CODHOR
					cCdFunc		:= (cAlias)->AA1_CDFUNC
					If !Empty(cTDWIniHor) .AND. !Empty(cTDWCodHor) 
						aEscala := At910ConRM( cCdFunc, dRMDatIni, cAliasTGY, @aEscTGY, lEnvia, cTDWIniHor, cTDWCodHor, @cMsgRM )
						If lEnvia
							If Empty(cCodTurno)
								If (aEscala[2] <> cTDWCodHor) .OR. (aEscala[2] == cTDWCodHor .AND. aEscala[3] <> cTDWIniHor)
									At910EnvRM( cCdFunc, @cMsgRM, aEscTGY, lEnvia, , cAliasTGY, (cAlias)->TDV_DTREF )
								EndIf
							Else
								If aEscala[2] <> cTDWCodHor
									At910EnvRM( cCdFunc, @cMsgRM, aEscTGY, lEnvia, , cAliasTGY, (cAlias)->TDV_DTREF )
								EndIf
							EndIf
						Else
							If !Empty(aEscala) .AND. !Empty(aEscala[1])
								At910EnvRM( cCdFunc, @cMsgRM, aEscTGY, lEnvia, aEscala, cAliasTGY )
							EndIf
						EndIf
					Else
						cMsgRm	+= STR0050 + CRLF // "N�o foi possivel envia a troca de turnos pois os campos TDX_INIHOR e/ou TDW_CODHOR n�o foram preenchidos."
						cMsgRM 	+= STR0051 + (cAliasTGY)->TDW_COD + CRLF // "Escala: " 
						cMsgRM	+= STR0052 + (cAlias)->AA1_NOMTEC + CRLF // "Atendente: " 
						cMsgRM	+= STR0053 + cCdFunc + CRLF // "Matricula (CHAPA): "
						cMsgRM	+= STR0054 + (cAlias)->AB9_CODTEC + CRLF + CRLF // "Codigo do Tecnico: "
					EndIf
					(cAliasTGY)->(DBCloseArea())
				EndIf
				cCodTurno	:= (cAlias)->TDV_TURNO
			Else 
				If lMostra
					cMsgRM += STR0048 // "Os Campos TDX_INIHOR e TDW_CODHOR n�o foram identificados, por favor, para utiliza��o da troca de turno � necess�rio a cria��o dos mesmo."
					lMostra := .F.
				EndIf
			EndIf
		EndIf
	EndIf
	
	(cAlias)->(DbSkip())
End

(cAlias)->(DbCloseArea())


If Empty(aRetorno)
	//"Aten��o"#"N�o h� registros para gerar marca��es conforme parametros informados."#"OK"
	If !lSemTela
		Aviso(STR0001,STR0014,{STR0004},2)
	EndIf
	aRet[1] := .F.
	AADD(aRet[2], STR0014)
Else
	//Ordena por Tecnico/Data|Hora Inicial+Data Referencia
	ASort(aRetorno,,,{|x,y| x[1]+DToS(x[2])+x[3]+x[12] < y[1]+DToS(y[2])+y[3]+y[12] })

	aMarcacao := At910PMarc(aRetorno,lGeraMarcInt, aRet)
	
	lLimite :=  !Empty(aTail(aRet[02]))

	nReg := 0
	If !IsBlind() .AND. !lSemTela
		oMeter:SetTotal(Len(aMarcacao))
		oMeter:Set(nReg) // Atualiza Gauge/Tmeter
	EndIf

	//realiza aglutina��o da marca��o para o atendente, considerando que aMarcacao esteja ordenado por atendente.
	For nX := 1 To Len(aMarcacao)
		If cAtendOld != aMArcacao[nX][1]
			aAdd(aMarcTec, {aMarcacao[nX][1], {}})//cria posi��o do array para o atendente
			cAtendOld := aMarcacao[nX][1]
		EndIf
		aAdd(aMarcTec[Len(aMarcTec)][2], aMarcacao[nX])//adiciona marca��es para o atendente
	Next nX

	For nX := 1 To Len(aMarcTec)
		nReg+= Len(aMarcTec[nX][2])
		If (!IsBlind() .AND. !lSemTela ).OR. !IsInCallStack("TecGeraReg")
			oSayMsg:SetText("<center>"+STR0015+cValToChar(Len(aMarcacao))+STR0016+cValToChar(nReg)+"</center>")
			oMeter:Set(nReg) // Atualiza Gauge/Tmeter
		EndIf

		If  (lRet := At910Marca(aMarcTec[nX][2],@cMsg,cTpExp, nHandle, nX == 1, nHandle2, aEmpFil))
			For nY:=1 To Len(aMarcTec[nX][2])
				At910AtAB9(aMarcTec[nX][2][nY][11])
			Next nY
		Else
			nCritica++
			For nY:=1 To Len(aMarcTec[nX][2])
				At910Log(,aMarcTec[nX][2][nY],cMsg)
			Next nY
		EndIf
	Next nX


	If nCritica == 0
		//"Aten��o"#"Foram processadas: "#" marca��es de entrada e sa�da."#"OK"
		If !lSemTela
			Aviso(STR0001,STR0018 + cValToChar(nReg) + STR0019 + STR0056 + cFilant + IIF(lLimite, CRLF + STR0037, ""),{STR0004},2) //"Verificar o log pois existem colocaboradores que tiveram mais batidas que o limite permitido para o Ponto Protheus"
		EndIf
		AADD(aRet[2], STR0018+ cValToChar(nReg) +  STR0019 )
		cOk := STR0018+ cValToChar(nReg) +  STR0019
	Else
		/*"Aten��o"#"Foram processadas: "#" Ocorreram "#" erro(s) no processamento."#
		"Quando h� critica todas marca��es do tecnico para o per�odo n�o ser�o geradas."
		"Foi gerado o log no arquivo "#"OK" */
		If !lSemTela
			Aviso(STR0001,STR0018+cValToChar(Len(aMarcacao))+STR0019;
			+STR0020+cValtoChar(nCritica)+STR0021+CRLF+STR0022;
			+CRLF+STR0023+TxLogPath(ALLTRIM(cFilant)+"MarcaErro"),{STR0004},2)
		EndIf
		AADD(aRet[2], STR0018+cValToChar(Len(aMarcacao))+STR0019;
			+STR0020+cValtoChar(nCritica)+STR0021+CRLF+STR0022;
			+CRLF+STR0023+TxLogPath(ALLTRIM(cFilant)+"MarcaErro"))
	EndIf
EndIf
If (!IsBlind() .AND. !lSemTela) .OR. !IsInCallStack("TecGeraReg")
	If lGSxInt .AND. !Empty(cMsgRM) 
		AtShowLog(cMsgRM,STR0049,/*lVScroll*/,/*lHScroll*/,/*lWrdWrap*/,.F.) // "Integra��o RM"
	EndIf
	oDlg:End() 
EndIf

If ValType(aRet[2][1]) == "C"
	If aRet[2][1] == cOk
		aRet := {.T.,{}}
	EndIf
EndIf

Return( .T. )

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At910AProc()

Gera a array de processamento dos atendimentos da o.s.

@param ExpA:Array que sera alimentada, enviar por referencia
@param ExpC:Alias da tabela de processamento
@param ExpD:Data Entrada da Marcacao
@param ExpC:Hora Entrada da Marcacao
@param ExpD:Data Saida da Marcacao
@param ExpC:Hora Saida da Marcacao
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function At910AProc(aRetorno,cAlias,dMarcIni,cMarcIni,dMarcFim,cMarcFim)
	AAdd(aRetorno,{;
		(cAlias)->AB9_CODTEC,; 	//1
		dMarcIni,;				//2
		cMarcIni,;				//3
		dMarcFim,;				//4
		cMarcFim,;				//5
		(cAlias)->AA1_CDFUNC,;	//6
		(cAlias)->AA1_FUNFIL,;	//7
		(cAlias)->AB9_NUMOS,;	//8
		(cAlias)->AB9_CODCLI,;	//9
		(cAlias)->AB9_LOJA,;	//10
		(cAlias)->AB9RECNO,;	//11
		(cAlias)->TDV_DTREF,;	//12
		(cAlias)->ABB_CODIGO,; 	//13 
		Nil,;					//14
		Nil,;					//15
		(cAlias)->TDV_FILIAL})	//16
		
If SuperGetMV("MV_GSHRPON",.F., "2") == "1" .AND. ( ABB->(ColumnPos('ABB_HRCHIN')) > 0 )
	aRetorno[Len(aRetorno)][14] := (cAlias)->ABB_HRCHIN
	aRetorno[Len(aRetorno)][15] := (cAlias)->ABB_HRCOUT
EndIf
Return


//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At910Qry()

Gera a query para a gera��o de marca��es

@return ExpC: Retorna a query utilizada para trazer os atendimentos
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function At910Qry(aAtendentes)
Local cQuery := ""
Local nX
Local lGsGerOs	 	:= SuperGetMV("MV_GSGEROS",.F.,"1") == "1" //Gera O.S na rotina de gera��o de atendimento do gest�o de servi�os 1 = Sim e 2 = N�o.
Local lGSxInt		:= SuperGetMV("MV_GSXINT",,"2") == "3"
Local lGSHRPon		:= SuperGetMV("MV_GSHRPON",.F., "2") == "1" .AND. ( ABB->(ColumnPos('ABB_HRCHIN')) > 0 )
Local cComAA1 		:= FwModeAccess("AA1",1) +  FwModeAccess("AA1",2) + FwModeAccess("AA1",3)
Local cComSRA 		:= FwModeAccess("SRA",1) +  FwModeAccess("SRA",2) + FwModeAccess("SRA",3)
Local lMultFil		:= SuperGetMV("MV_GSMSFIL",,.F.) .AND. (LEN(STRTRAN( cComAA1  , "E" )) > LEN(STRTRAN(  cComSRA  , "E" )))
Local lMVPar09		:= TecHasPerg("MV_PAR09","TEC910")
Local lMVPar10		:= TecHasPerg("MV_PAR10","TEC910")
Default aAtendentes := {}

//--------------------------------------
// Parametros Utilizados no Pergunte
//
// MV_PAR01: Atendente De ?
// MV_PAR02: Atendente Ate ?
// MV_PAR03: Data Inicio De ?
// MV_PAR04: Data Inicio Ate ?
// MV_PAR05: Processamento ? 1=Inclusao;2=Exclusao
//--------------------------------------

//������������������������������������������������������������������������Ŀ
//�Monta a Query para gera��o da marca��o                                  �
//��������������������������������������������������������������������������
If lGsGerOs

	cQuery += "SELECT AB9.AB9_FILIAL,AB9.AB9_NUMOS,AB9.AB9_SEQ,AB9.AB9_CODTEC,AB9.AB9_DTINI,"
	cQuery += "AB9.AB9_DTFIM,AB9.AB9_CODCLI,AB9.AB9_LOJA,AB9.AB9_CONTRT, ABB.ABB_CODIGO, ABB.ABB_LOCAL, "
	If lGSHRPon
		cQuery += " CASE WHEN ABB.ABB_HRCHIN = '' OR ABB.ABB_HRCHIN IS NULL THEN ABB.ABB_HRINI ELSE ABB.ABB_HRCHIN END AB9_HRINI, "
		cQuery += " CASE WHEN ABB.ABB_HRCOUT = '' OR ABB.ABB_HRCOUT IS NULL THEN ABB.ABB_HRFIM ELSE ABB.ABB_HRCOUT END AB9_HRFIM, "
		cQuery += " ABB_HRCHIN, ABB_HRCOUT,"
	Else
		cQuery += "AB9_HRINI, AB9_HRFIM, " 
	EndIf
	If lGSxInt
		cQuery += " TDV.TDV_TURNO, TDV.TDV_SEQTRN, ABB.ABB_TIPOMV, TDV.TDV_CODABB, AA1.AA1_NOMTEC, "
	EndIf
	cQuery += "AA1.AA1_CDFUNC,AA1.AA1_FUNFIL,AA1.AA1_CC,AA1.AA1_TURNO,AA1.AA1_MPONTO, "
	cQuery += "AB9.R_E_C_N_O_ AB9RECNO, TDV.TDV_DTREF, TDV.TDV_FILIAL FROM "  + RetSqlName( "AB9" ) + " AB9 "
	cQuery += "INNER JOIN "  + RetSqlName( "AA1" ) + " AA1  ON AA1.AA1_FILIAL='" + xFilial( "AA1" ) + "' AND AA1.AA1_CODTEC = AB9.AB9_CODTEC "
	cQuery += "INNER JOIN "  + RetSqlName( "TDV" ) + " TDV ON "
	If !lMultFil
		cQuery += "TDV.TDV_FILIAL ='"+xFilial("TDV") +"' "
	Else
		cQuery += FWJoinFilial("TDV" , "AB9" , "TDV", "AB9", .T.)
	EndIf
	cQuery += " AND TDV.TDV_CODABB = AB9.AB9_ATAUT "
	cQuery += " INNER JOIN " + RetSqlName( "ABB" ) + " ABB ON "
	If !lMultFil
		cQuery += " ABB.ABB_FILIAL = '" + xFilial("ABB") + "' "
	Else
		cQuery += FWJoinFilial("TDV" , "ABB" , "TDV", "ABB", .T.)
	EndIf
	cQuery += " AND ABB.ABB_CODIGO = TDV.TDV_CODABB "
	cQuery += "WHERE "
	If !lMultFil
		cQuery += "AB9_FILIAL='" + xFilial( "AB9" ) + "' AND "
	EndIf
	cQuery += " AA1.AA1_MPONTO = '2' AND "
	
	//Filtra Tecnico
	If Empty(aAtendentes)
		If !Empty(MV_PAR01)
			cQuery += "AB9.AB9_CODTEC >='" +  MV_PAR01 + "' AND "
		EndIf
		If !Empty(MV_PAR02)
			cQuery += "AB9.AB9_CODTEC <='" +  MV_PAR02 + "' AND "
		EndIf
	Else
		cQuery += " AB9.AB9_CODTEC IN ( "
		For nX := 1 to LEN(aAtendentes)
			cQuery += " '" + aAtendentes[nX] + "',"
		Next nX
		cQuery := LEFT(cQuery, LEN(cQuery) - 1)
		cQuery += " ) AND "
	EndIf
	//Filtra Data de Inicio
	If !Empty(MV_PAR03)
		cQuery += "TDV.TDV_DTREF >='" + DToS( MV_PAR03 ) + "' AND "
	EndIf
	If !Empty(MV_PAR04)
		cQuery += "TDV.TDV_DTREF <='" + DToS( MV_PAR04 ) + "' AND "
	EndIf
	
	//Filtra Marcacao conforme processamento
	cQuery += "AB9.AB9_MPONTO = '"+Iif(MV_PAR05==1,'F','T')+"' AND "

	//Filtra marcacao conforme o Local de Atendimento
	If lMVPar09 .AND. !Empty(MV_PAR09)
		cQuery += "ABB.ABB_LOCAL >='" + MV_PAR09 + "' AND "
	EndIf
	If lMVPar10 .AND. !Empty(MV_PAR10)
		cQuery += "ABB.ABB_LOCAL <='" + MV_PAR10 + "' AND "
	EndIf
	
	cQuery += "AB9.D_E_L_E_T_=' ' "
	cQuery += "AND AA1.D_E_L_E_T_=' ' "
	cQuery += "AND TDV.D_E_L_E_T_=' ' "
	cQuery += "AND ABB.D_E_L_E_T_=' ' AND ABB.ABB_ATIVO = '1' "
	cQuery += " ORDER BY AB9.AB9_CODTEC,AB9.AB9_DTINI"
Else

	cQuery += "SELECT ABB.ABB_FILIAL AB9_FILIAL, ABB.ABB_NUMOS AB9_NUMOS, ' ' AB9_SEQ, ABB.ABB_CODTEC AB9_CODTEC, ABB.ABB_DTINI AB9_DTINI,"
	cQuery += "ABB.ABB_DTFIM AB9_DTFIM,' ' AB9_CODCLI, ' ' AB9_LOJA, ' ' AB9_CONTRT, "
	cQuery += "AA1.AA1_CDFUNC,AA1.AA1_FUNFIL,AA1.AA1_CC,AA1.AA1_TURNO,AA1.AA1_MPONTO, ABB.ABB_CODIGO, ABB.ABB_LOCAL, "
	If lGSHRPon
		cQuery += " CASE WHEN ABB.ABB_HRCHIN = '' OR ABB.ABB_HRCHIN IS NULL THEN ABB.ABB_HRINI ELSE ABB.ABB_HRCHIN END AB9_HRINI, "
		cQuery += " CASE WHEN ABB.ABB_HRCOUT = '' OR ABB.ABB_HRCOUT IS NULL THEN ABB.ABB_HRFIM ELSE ABB.ABB_HRCOUT END AB9_HRFIM, "
		cQuery += " ABB_HRCHIN, ABB_HRCOUT,"
	Else
		cQuery += "ABB.ABB_HRINI AB9_HRINI, ABB.ABB_HRFIM AB9_HRFIM, " 
	EndIf
	If lGSxInt
		cQuery += " TDV.TDV_TURNO, TDV.TDV_SEQTRN, ABB.ABB_TIPOMV, TDV.TDV_CODABB, AA1.AA1_NOMTEC, "
	EndIf
	cQuery += "ABB.R_E_C_N_O_ AB9RECNO, TDV.TDV_DTREF, TDV.TDV_FILIAL FROM "  + RetSqlName( "ABB" ) + " ABB "
	cQuery += "INNER JOIN "  + RetSqlName( "AA1" ) + " AA1  ON AA1.AA1_FILIAL='" + xFilial("AA1") +"' AND AA1.AA1_CODTEC = ABB.ABB_CODTEC "
	cQuery += "INNER JOIN "  + RetSqlName( "TDV" ) + " TDV 	ON "
	If !lMultFil
		cQuery += " TDV.TDV_FILIAL='" + xFilial("TDV") +"' "
	Else
		cQuery += FWJoinFilial("TDV" , "ABB" , "TDV", "ABB", .T.)
	EndIf
	cQuery += " AND TDV.TDV_CODABB = ABB.ABB_CODIGO "
	cQuery += "WHERE "
	If !lMultFil
		cQuery += "ABB.ABB_FILIAL='" + xFilial( "ABB" ) + "' AND "
	EndIf
	cQuery += " AA1.AA1_MPONTO = '2' AND "	
	cQuery += "ABB.ABB_NUMOS ='" + SPACE(TamSX3("ABB_NUMOS")[1]) + "' AND "  
	cQuery += "ABB.ABB_CHEGOU ='S' AND "  
	cQuery += "ABB.ABB_ATENDE ='1' AND "  
	
	//Filtra Tecnico
	If Empty(aAtendentes)
		If !Empty(MV_PAR01)
			cQuery += "ABB.ABB_CODTEC >='" +  MV_PAR01 + "' AND "
		EndIf
		If !Empty(MV_PAR02)
			cQuery += "ABB.ABB_CODTEC <='" +  MV_PAR02 + "' AND "
		EndIf
	Else
		cQuery += " ABB.ABB_CODTEC IN ( "
		For nX := 1 to LEN(aAtendentes)
			cQuery += " '" + aAtendentes[nX] + "',"
		Next nX
		cQuery := LEFT(cQuery, LEN(cQuery) - 1)
		cQuery += " ) AND "
	EndIf

	//Filtra Data de Inicio
	If !Empty(MV_PAR03)
		cQuery += "TDV.TDV_DTREF >='" + DToS( MV_PAR03 ) + "' AND "
	EndIf
	If !Empty(MV_PAR04)
		cQuery += "TDV.TDV_DTREF <='" + DToS( MV_PAR04 ) + "' AND "
	EndIf
	
	//Filtra Marcacao conforme processamento
	cQuery += "ABB.ABB_MPONTO = '"+Iif(MV_PAR05==1,'F','T')+"' AND "
	
	//Filtra marcacao conforme o Local de Atendimento
	If lMVPar09 .AND. !Empty(MV_PAR09)
		cQuery += "ABB.ABB_LOCAL >='" + MV_PAR09 + "' AND "
	EndIf
	If lMVPar10 .AND. !Empty(MV_PAR10)
		cQuery += "ABB.ABB_LOCAL <='" + MV_PAR10 + "' AND "
	EndIf

	cQuery += "ABB.D_E_L_E_T_=' ' AND ABB.ABB_ATIVO = '1' "
	cQuery += "AND AA1.D_E_L_E_T_=' ' "
	cQuery += "AND TDV.D_E_L_E_T_=' ' "
	
	cQuery += "ORDER BY ABB.ABB_CODTEC,ABB.ABB_DTINI"

Endif

Return( cQuery )
//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At910PMarc()

Ordena o array com as datas e horarios iniciais e finais

		Estrutura do Array aRetorno
		[nX][1]	- Codigo do Atendente(Caracter)
		[nX][2]	- Dia Inicial da marca��o(Data)
		[nX][3]	- Horario Inicial(Caracter)
		[nX][4]	- Dia Final(Data)
		[nX][5]	- Horario Final(Caracter)
		[nX][6]	- Codigo da Matricula(Caracter)
		[nX][7]	- Filial do Funcionario(Caracter)
		[nX][8]	- Numero da O.S.(Caracter)
		[nX][9]	- Codigo do Cliente(Caracter)
		[nX][10]	- Loja do Cliente(Caracter)
		[nX][11]	- Recno AB9(Numerico)

@param ExpA:Array contendo os atendimentos

@return ExpA: Array ordenado de acordo com as datas e horarios
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function At910PMarc(aRetorno, lGeraMarcInt, aRet)
Local nX			:= 0
Local aMarcacao		:= {}	//Marcacoes Tratadas
Local aRecno		:= {}	//Array com os Recnos que serao atualizados
Local cCodTec		:= ""
Local dMarcIni		:= CtoD("  /  /    ")
Local cMarcIni		:= ""
Local dMarcFim		:= CtoD("  /  /    ")
Local cMarcRef		:= ""
Local cMarcFim		:= ""
Local cCodFunc		:= ""
Local cFilFun		:= ""
Local cCliente		:= ""
Local cLoja			:= ""
Local cNumOs		:= ""
Local lMV_GSXINT	:= SuperGetMv("MV_GSXINT",,"2") <> "2"
Local nColMarc		:= SuperGetMv("MV_COLMARC",,4)  //limite de colunas do relat�rio
Local nColunas		:= 0
Local nTamRet 		:=  Len(aRetorno)
Local lPrimeira 	:= .T.
Local lHoraExtra 	:= .F.
Local lContinua		:= .T.
Local lUltimo	 	:= .T.
Local lEnviou		:= .F.
//Com o parametro MV_GSHRPON ativo, verifica se os campos ABB_HRCHIN e ABB_HRCOUT est�o preenchidos para utilizar os minutos aleatorios.
Local lGSHRPon		:= SuperGetMV("MV_GSHRPON",.F., "2") == "1" .AND. ( ABB->(ColumnPos('ABB_HRCHIN')) > 0 )
Local lHrIniInt		:= .T.
Local lHrFimInt		:= .T.

Default lGeraMarcInt := .T.

If !lGeraMarcInt .OR. (!lMV_GSXINT .and. nColMarc > 0)
	For nX := 1 To nTamRet
		lEnviou	:= .F.
	
		If !lGeraMarcInt
			//Quando inicia, muda o tecnico ou muda o dia inicial ou final da marcacao
			If cCodTec != aRetorno[nX][1] .OR. dMarcIni != aRetorno[nX][2] 
				//Quando mudar adiciona a marcacao antes de reiniciar as variaveis desde que nao seja a ultima vez no loop
				If nX != 1
					AAdd(aMarcacao,{cCodTec,dMarcIni,cMarcIni,dMarcFim,cMarcFim,cCodFunc,cFilFun,cCliente,cLoja,cNumOs,aRecno, lHrIniInt, lHrFimInt})
					nColunas := 0
				EndIf
				cCodTec		:= aRetorno[nX][1]
				dMarcIni	:= aRetorno[nX][2]
				cMarcIni	:= aRetorno[nX][3]
				dMarcFim	:= aRetorno[nX][4]
				cMarcFim	:= aRetorno[nX][5]
				cCodFunc	:= aRetorno[nX][6]
				cFilFun		:= aRetorno[nX][7]
				cCliente	:= aRetorno[nX][8]
				cLoja		:= aRetorno[nX][9]
				cNumOs		:= aRetorno[nX][10]
				aRecno		:= {aRetorno[nX][11]}	//Quando inicia limpa o recno
				nColunas++
			Else    
				//Quando � o mesmo tecnico e mesmo dia verifica a data/hora fim da Marcacao
			
				nColunas++
				AAdd(aRecno,aRetorno[nX][11])
				
				If  DToS(dMarcFim)+cMarcFim < DToS(aRetorno[nX][4])+aRetorno[nX][5] 
					dMarcFim := aRetorno[nX][4]
					cMarcFim := aRetorno[nX][5]
				EndIf
			EndIf
			
			
		
			//Adiciona o �ltimo registro na marca��o
			If nX == nTamRet //Quando mudar adiciona a marcacao antes de reiniciar as variaveis
				AAdd(aMarcacao,{cCodTec,dMarcIni,cMarcIni,dMarcFim,cMarcFim,cCodFunc,cFilFun,cCliente,cLoja,cNumOs,aClone(aRecno), lHrIniInt, lHrFimInt})
			EndIf
		Else
			If cMarcRef == aRetorno[nX][12] .AND. lHoraExtra .AND. cMarcFim == aRetorno[nX][3]
				
				lHoraExtra := .F.
				nColunas++
				AAdd( aRecno, aRetorno[nX][11] )
				dMarcFim 	:= aRetorno[nX][4]
				cMarcFim 	:= aRetorno[nX][5]
				If lGSHRPon
					lHrFimInt	:= Empty(aRetorno[nX][15])
				EndIf
				lUltimo 	:= .T.
				lContinua 		:= .F.
			Else
				If lHoraExtra .AND. !lContinua
					dMarcIni	:= aRetorno[nX-1][2]
					cMarcIni	:= aRetorno[nX-1][3]
					dMarcFim	:= aRetorno[nX-1][4]
					cMarcFim	:= aRetorno[nX-1][5]
					cCodFunc	:= aRetorno[nX-1][6]
					cFilFun		:= aRetorno[nX-1][7]
					cCliente	:= aRetorno[nX-1][8]
					cLoja		:= aRetorno[nX-1][9]
					cNumOs		:= aRetorno[nX-1][10]
					aRecno		:= {aRetorno[nX-1][11]}
					If lGSHRPon
						lHrIniInt	:= Empty(aRetorno[nX][14])
						lHrFimInt	:= Empty(aRetorno[nX][15])
					EndIf
					AAdd(aMarcacao,{cCodTec,dMarcIni,cMarcIni,dMarcFim,cMarcFim,cCodFunc,cFilFun,cCliente,cLoja,cNumOs,aClone(aRecno), lHrIniInt, lHrFimInt})
					lEnviou := .T.
				EndIf
				lContinua	:= .T.
				DbSelectArea("ABR")
				DbSetOrder(1)
				If ABR->(DbSeek(xfilial('ABR', aRetorno[nX][16]) + aRetorno[nX][13]))
					If Posicione("ABN", 1,xFilial("ABN", aRetorno[nX][16]) + ABR->ABR_MOTIVO, "ABN_TIPO") == "04"
						lHoraExtra := .T.
						If cMarcRef == aRetorno[nX][12]
							lContinua := .F.
							AAdd(aMarcacao,{cCodTec,dMarcIni,cMarcIni,dMarcFim,cMarcFim,cCodFunc,cFilFun,cCliente,cLoja,cNumOs,aClone(aRecno), lHrIniInt, lHrFimInt})
						EndIf
					EndIf
				EndIf
				If lContinua
					If nColunas <= nColMarc 
						If !lPrimeira .AND. !lEnviou
							AAdd(aMarcacao,{cCodTec,dMarcIni,cMarcIni,dMarcFim,cMarcFim,cCodFunc,cFilFun,cCliente,cLoja,cNumOs,aClone(aRecno), lHrIniInt, lHrFimInt})
						EndIf
					Else 
						At910Log(,aRetorno[nX],STR0038+AllTrim(Str(nColMarc)) + STR0039 ,aRet) //"O Atendente possui mais de "##" marca�oes para a data de referencia. Esta marca��o ser� ignorada"
					EndIf
					If cCodTec != aRetorno[nX][1] .OR. cMarcRef != aRetorno[nX][12] //Conta as marcacoes a partir da data de referencia
						nColunas 	:= 0	
						cMarcRef	:=  aRetorno[nX][12]		
						cCodTec 	:= aRetorno[nX][1]
					EndIf
					
					nColunas++
					
					dMarcIni	:= aRetorno[nX][2]
					cMarcIni	:= aRetorno[nX][3]
					dMarcFim	:= aRetorno[nX][4]
					cMarcFim	:= aRetorno[nX][5]
					cCodFunc	:= aRetorno[nX][6]
					cFilFun		:= aRetorno[nX][7]
					cCliente	:= aRetorno[nX][8]
					cLoja		:= aRetorno[nX][9]
					cNumOs		:= aRetorno[nX][10]
					aRecno		:= {aRetorno[nX][11]}
					If lGSHRPon
						lHrIniInt	:= Empty(aRetorno[nX][14])
						lHrFimInt	:= Empty(aRetorno[nX][15])
					EndIf
					//Quando inicia limpa o recno
					lPrimeira := .F.
					lUltimo := .F.
				Else
					cMarcFim	:= aRetorno[nX][5]
				EndIf
			EndIf
			If nX == nTamRet 	//Quando mudar adiciona a marcacao antes de reiniciar as variaveis
				If lHoraExtra .AND. !lContinua
					aRecno		:= {aRetorno[nX][11]}
					If lGSHRPon
						lHrIniInt	:= Empty(aRetorno[nX][14])
						lHrFimInt	:= Empty(aRetorno[nX][15])
					EndIf
					AAdd(aMarcacao,{aRetorno[nX][1],aRetorno[nX][2],aRetorno[nX][3],aRetorno[nX][4],aRetorno[nX][5],aRetorno[nX][6],aRetorno[nX][7],aRetorno[nX][8],aRetorno[nX][9],aRetorno[nX][10],aClone(aRecno), lHrIniInt, lHrFimInt})
				Else
					AAdd(aMarcacao,{cCodTec,dMarcIni,cMarcIni,dMarcFim,cMarcFim,cCodFunc,cFilFun,cCliente,cLoja,cNumOs,aClone(aRecno), lHrIniInt, lHrFimInt})
				EndIf
			EndIf
		EndIf
	Next nX
Else
	aMarcacao := aClone(aRetorno)
EndIf

Return(aMarcacao)

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At910Marca()

Inclui via ExecAuto a Marcacao no POnto Eletronico

@param ExpA:Array contendo os dados para a ExecAuto
@param ExpC:Mensagem de Critica (Passar por referencia. Ira alterar com a mensagem quando houver erro)

@return ExpL: Retorna .T. quando h� sucesso na opera��o
/*/
//--------------------------------------------------------------------------------------------------------------------
Function At910Marca(aMarcacao,cMsg,cTpExp, nHandle, lCab, nHandle2,aEmpFil)
Local lRet		:= .T.	//Retorno da fun��o
Local lExiste	:= .F.
Local aCabec	:= {}
Local aLinha	:= {}
Local aItens	:= {}
Local aRetInc 	:= {}
Local nOpc		:= Iif(MV_PAR05==1,3,5)
Local nMsg		:= 0
Local nAleato	:= 0
Local nMinuto 	:= 0
Local nI		:= 1
Local nX		:= 1
Local cFilProc 	:= ""
Local aRet		:= {} //Retorno do Ponto de entrada
Local cGSxInt 	:= SuperGetMv("MV_GSXINT",,"2") 
Local nMinAleAux:= 0

Default cMsg 		:= ""
Default cTpExp 		:= "1"
Default nHandle 	:= 0
Default lCab 		:= .f.
Default nHandle2 	:= 0
Default aEmpFil 	:= {}

If Len(aMarcacao) > 0
	AAdd(aCabec,{"RA_FILIAL"	,aMarcacao[1][7]})
	AAdd(aCabec,{"RA_MAT" 	,aMarcacao[1][6]})
	cFilProc := aMarcacao[1][7]
EndIf

If nOpc == 5 .AND. ("1" $ cTpExp .AND. cGSxInt == "2") //se for exclus�o , retorna a marca��o da SP8 por causa dos minutos aleatorios
	DbSelectArea("SP8")
	DbSetOrder(2) // P8_FILIAL+P8_MAT+DTOS(P8_DATA)+STR(P8_HORA,5,2)

	For nI := 1 To Len(aMarcacao)
		If SP8->(DbSeek(aMarcacao[nI][7] + aMarcacao[nI][6] + DTOS(aMarcacao[nI][2])))
			While SP8->(P8_FILIAL + P8_MAT + DTOS(P8_DATA) ) == aMarcacao[nI][7] + aMarcacao[nI][6] + DTOS(aMarcacao[nI][2]) .and. SP8->(!EOF())
				For nX := 1 to len(aItens)
					nPosFil	:= AScan(aItens[nX],{|x| Iif(ValType(x[2]) == ValType(SP8->P8_FILIAL)	, x[2] == SP8->P8_FILIAL , 0) })
					nPosMat	:= AScan(aItens[nX],{|x| Iif(ValType(x[2]) == ValType(SP8->P8_MAT)		, x[2] == SP8->P8_MAT , 0) })
					nPosDat	:= AScan(aItens[nX],{|x| Iif(ValType(x[2]) == ValType(SP8->P8_DATA)		, x[2] == SP8->P8_DATA , 0) })
					nPosHor	:= AScan(aItens[nX],{|x| Iif(ValType(x[2]) == ValType(SP8->P8_HORA)		, x[2] == SP8->P8_HORA , 0) })
					If nPosFil > 0 .and. nPosMat > 0 .and. nPosDat > 0 .and. nPosHor > 0
						lExiste := .T.
						Exit
					Else
						lExiste := .F.
					EndIf
				Next

				If !lExiste
					aLinha := {}
					// Entrada
					AAdd(aLinha,{"P8_FILIAL"	,aMarcacao[nI][7]})
					AAdd(aLinha,{"P8_MAT"		,aMarcacao[nI][6]})
					AAdd(aLinha,{"P8_DATA"		,aMarcacao[nI][2]})
					AAdd(aLinha,{"P8_HORA"		,SP8->P8_HORA})
					AAdd(aItens,aLinha)
				EndIf
				SP8->(dbskip())
			EndDo
		EndIf

		If SP8->(DbSeek(aMarcacao[nI][7]  + aMarcacao[nI][6] + DTOS(aMarcacao[nI][4])))
			While SP8->(P8_FILIAL + P8_MAT + DTOS(P8_DATA) ) == aMarcacao[nI][7] + aMarcacao[nI][6] + DTOS(aMarcacao[nI][4]) .and. SP8->(!EOF())
				For nX := 1 to len(aItens)
					nPosFil	:= AScan(aItens[nX],{|x| Iif(ValType(x[2]) == ValType(SP8->P8_FILIAL), x[2] == SP8->P8_FILIAL , 0) })
					nPosMat	:= AScan(aItens[nX],{|x| Iif(ValType(x[2]) == ValType(SP8->P8_MAT), x[2] == SP8->P8_MAT , 0) })
					nPosDat	:= AScan(aItens[nX],{|x| Iif(ValType(x[2]) == ValType(SP8->P8_DATA), x[2] == SP8->P8_DATA , 0) })
					nPosHor	:= AScan(aItens[nX],{|x| Iif(ValType(x[2]) == ValType(SP8->P8_HORA), x[2] == SP8->P8_HORA , 0) })
					If nPosFil > 0 .and. nPosMat > 0 .and. nPosDat > 0 .and. nPosHor > 0
						lExiste := .T.
						Exit
					Else
						lExiste := .F.
					EndIf
				Next

				If !lExiste
					aLinha := {}
					// Saida
					AAdd(aLinha,{"P8_FILIAL"	,aMarcacao[nI][7]})
					AAdd(aLinha,{"P8_MAT"		,aMarcacao[nI][6]})
					AAdd(aLinha,{"P8_DATA"		,aMarcacao[nI][4]})
					AAdd(aLinha,{"P8_HORA"		,SP8->P8_HORA})
					AAdd(aItens,aLinha)
				EndIf
				SP8->(dbskip())
			EndDo
		EndIf
	Next nI

Else

	For nI := 1 To Len(aMarcacao)
	
		nAleato := 0
		
		If Valtype(aMarcacao[nI][12]) == "L" .AND. aMarcacao[nI][12]
			If TecHasPerg("MV_PAR07","TEC910") //Minutos para a Aleatoriedade das Marcacoes
				nMinuto	:=	Abs( MV_PAR07 )		
			EndIf
	
			If nMinuto > 0 
				nMinAleAux := (Val(StrTran(aMarcacao[nI][3],":","."))-(nMinuto/100))
			Endif
	
			//Calculo para nao voltar para o dia anterior quando for minutos aletorios.
			If nMinAleAux < 0
				nMinuto := (nMinAleAux+(nMinuto/100))
			EndIf
		Else
			nMinuto := 0
		EndIf
		
		aLinha := {}

		//Entrada
		AAdd(aLinha,{"P8_FILIAL"	,aMarcacao[nI][7]})
		AAdd(aLinha,{"P8_MAT"		,aMarcacao[nI][6]})
		AAdd(aLinha,{"P8_DATA"		,aMarcacao[nI][2]})
		
		If nMinuto > 0
			AAdd(aLinha,{"P8_HORA"	,DataHora2Ale(aMarcacao[nI][2],Val(StrTran(aMarcacao[nI][3],":",".")),nMinuto,@nAleato,"E")})
		Else
			AAdd(aLinha,{"P8_HORA"	,Val(StrTran(aMarcacao[nI][3],":","."))})
		EndIf

		AAdd(aItens,aLinha)
		
		nAleato := 0
		
		If Valtype(aMarcacao[nI][13]) == "L" .AND. aMarcacao[nI][13] 
			If TecHasPerg("MV_PAR07","TEC910") //Minutos para a Aleatoriedade das Marcacoes
				nMinuto	:=	Abs( MV_PAR07 )		
			EndIf
	
			If nMinuto > 0
				nMinAleAux := (Val(StrTran(aMarcacao[nI][5],":","."))+(nMinuto/100))
			Endif
	
			//Calculo para nao transcender o dia quando for minutos aleatorios
			If nMinAleAux > 23.59
				nMinuto := ((23.59-Val(StrTran(aMarcacao[nI][5],":",".")))*100)
			EndIf
		Else
			nMinuto := 0
		EndIf
		aLinha := {}

		//Saida
		AAdd(aLinha,{"P8_FILIAL"	,aMarcacao[nI][7]})
		AAdd(aLinha,{"P8_MAT"		,aMarcacao[nI][6]})
		AAdd(aLinha,{"P8_DATA"		,aMarcacao[nI][4]})

		If nMinuto > 0
			AAdd(aLinha,{"P8_HORA"	,DataHora2Ale(aMarcacao[nI][4],Val(StrTran(aMarcacao[nI][5],":",".")),nMinuto,@nAleato,"S")})
		Else
			AAdd(aLinha,{"P8_HORA"	,Val(StrTran(aMarcacao[nI][5],":","."))})
		EndIf

		AAdd(aItens,aLinha)

	Next nI
EndIf


If ExistBlock("At910Ma")
	aRet := ExecBlock("At910Ma", .F., .F., {aCabec, aItens, nOpc, cTpExp})
	If Len(aRet) > 1 .and. Valtype(aRet[1]) = "A" .AND. ValType(aRet[2]) == "A"
		aCabec := aClone(aRet[1])
		aItens := aClone(aRet[2])
	EndIf
EndIf

If "1" $ cTpExp .and. cGSxInt == "2"
	aRetInc := Ponm010(		.F.				,;	//01 -> Se o "Start" foi via WorkFlow
							.F. 			,;	//02 -> Se deve considerar as configuracoes dos parametros do usuario
							.T.				,;	//03 -> Se deve limitar a Data Final de Apontamento a Data Base
							cFilProc		,;	//04 -> Filial a Ser Processada
							.F.				,;	//05 -> Processo por Filial
							.F.				,;	//06 -> Apontar quando nao Leu as Marcacoes para a Filial
							.F.				,;	//07 -> Se deve Forcar o Reapontamento
							aCabec			,;
							aItens			,;
							nOpc    		,;
							)

	If Len(aRetInc) > 0 .And. !(aRetInc[1])
		cMsg := ""
		For nMsg := 1 to Len(aRetInc[2])
			cMsg += aRetInc[2,nMsg] + CRLF
		Next
		lRet := .F.

	EndIf
EndIf

If "1" $ cTpExp .AND. cGSxInt == "3" .AND. nOpc == 3 .and. nHandle2 > 0  .AND. Len(aEmpFil) > 0 //envia para a RM 
	aRetInc :=  At910MCSV({}, aItens,nHandle2, .f., cGSxInt, aEmpFil )
EndIf

If "2" $ cTpExp
	aRetInc := ExecBlock("At910CMa", .F., .F., {aCabec, aItens, nOpc, lCab})
	If Len(aRetInc) > 0 .And. !(aRetInc[1])
		For nMsg := 1 to Len(aRetInc[2])
			cMsg += aRetInc[2,nMsg] + CRLF
		Next
		lRet := .F.
	EndIf
EndIf
If "3"$ cTpExp .AND. nHandle > 0
	aRetInc :=  At910MCSV(aCabec, aItens,nHandle, lCab )

	If Len(aRetInc) > 0 .And. !(aRetInc[1])
		For nMsg := 1 to Len(aRetInc[2])
			cMsg += aRetInc[2,nMsg] + CRLF
		Next
		lRet := .F.
	EndIf
EndIf

Return(lRet)

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At910AtAB9()

Atualiza o Atendimento para informar que j� foi gerado Marcacao

@param ExpC:Recno do Atendimento que ser� atualizado

@return ExpL: Retorna .T. a atualiza��o aconteceu com sucesso
/*/
//--------------------------------------------------------------------------------------------------------------------
Function At910AtAB9(xRecno)
Local nI			:= 0
Local lMarca		:= MV_PAR05==1
Local aRecnoAB9		:= {}
Local lGsGerOs	 	:= SuperGetMV("MV_GSGEROS",.F.,"1") == "1" //Gera O.S na rotina de gera��o de atendimento do gest�o de servi�os 1 = Sim e 2 = N�o.

If ValType(xRecno) != "A"
	aRecnoAB9 := {xRecno}
Else
	aRecnoAB9 := xRecno
EndIf

For nI := 1 To Len(aRecnoAB9)
	If lGsGerOs
		AB9->( DBGOTO(aRecnoAB9[nI]) )
		RecLock("AB9", .F.)
		AB9_MPONTO	:= lMarca	//Gerou Marca��o ".T." - Sim ; ".F." - N�o
			AB9->( MsUnLock() )
	Else
		ABB->( DBGOTO(aRecnoAB9[nI]) )
		RecLock("ABB", .F.)
		ABB_MPONTO	:= lMarca	//Gerou Marca��o ".T." - Sim ; ".F." - N�o
		ABB->( MsUnLock() )
	Endif
	
Next nI

Return(.T.)

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At910Log()

Adiciona dados do registro em processamento quando houver cr�tica.

@param ExpA:Array com as criticas de todo o processamento.
@param Expc:Alias da tabela do processamento.
@param cMsg:Mensagem de critica do registro corrente.

/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function At910Log(cAlias,aMarcacao,cMsg,aRet)
Local cText		 := ""
Local cRecno	 := ""
Local lGsGerOs	 := SuperGetMV("MV_GSGEROS",.F.,"1") == "1" //Gera O.S na rotina de gera��o de atendimento do gest�o de servi�os 1 = Sim e 2 = N�o.

Default cAlias		:= ""
Default aMarcacao		:= {}
Default aRet := {.T.,{}}

If !Empty(cAlias)
	If lGsGerOs
		//"Cr�tica ao processar : R_E_C_N_O_ "
		cText += STR0026+cValToChar((cAlias)->AB9RECNO)+CRLF;
		+" "+RetTitle("AB9_CODTEC")+":"+(cAlias)->AB9_CODTEC+CRLF;
		+" "+RetTitle("AB9_NUMOS")+":"+(cAlias)->AB9_NUMOS+CRLF;
		+" "+RetTitle("AB9_SEQ")+":"+(cAlias)->AB9_SEQ+CRLF;
		+" "+RetTitle("AB9_CODCLI")+":"+(cAlias)->AB9_CODCLI+CRLF;
		+" "+RetTitle("AB9_LOJA")+":"+(cAlias)->AB9_LOJA+CRLF;
		+" "+RetTitle("AB9_DTINI")+":"+DtoC((cAlias)->AB9_DTINI)+CRLF;
		+" "+RetTitle("AB9_HRINI")+":"+(cAlias)->AB9_HRINI+CRLF;
		+" "+RetTitle("AB9_DTFIM")+":"+DToC((cAlias)->AB9_DTFIM)+CRLF;
		+" "+RetTitle("AB9_HRFIM")+":"+(cAlias)->AB9_HRFIM+CRLF;
		+" "+CRLF+cMsg+CRLF
	Else
		//"Cr�tica ao processar : R_E_C_N_O_ "
		cText += STR0026+cValToChar((cAlias)->AB9RECNO)+CRLF;
		+" "+RetTitle("ABB_CODTEC")+":"+(cAlias)->AB9_CODTEC+CRLF;
		+" "+RetTitle("ABB_DTINI")+":"+DtoC((cAlias)->AB9_DTINI)+CRLF;
		+" "+RetTitle("ABB_HRINI")+":"+(cAlias)->AB9_HRINI+CRLF;
		+" "+RetTitle("ABB_DTFIM")+":"+DToC((cAlias)->AB9_DTFIM)+CRLF;
		+" "+RetTitle("ABB_HRFIM")+":"+(cAlias)->AB9_HRFIM+CRLF;
		+" "+CRLF+cMsg+CRLF
	Endif
EndIf

If Len(aMarcacao) > 0
	If ValType(aMarcacao[11]) == "A"
		AEval(aMarcacao[11],{|x| cRecno += cValToChar(x)+"," })
	Else
		cRecno += cValToChar(aMarcacao[11])
	EndIf

	If lGsGerOs
		//"Cr�tica execauto de marca��o : R_E_C_N_O_ "
		cText += STR0027+cRecno+CRLF;
		+" "+RetTitle("AB9_CODTEC")+":"+aMarcacao[1]+CRLF;
		+" "+RetTitle("AB9_NUMOS")+":"+aMarcacao[10]+CRLF;
		+" "+RetTitle("AB9_CODCLI")+":"+aMarcacao[8]+CRLF;
		+" "+RetTitle("AB9_LOJA")+":"+aMarcacao[9]+CRLF;
		+" "+RetTitle("AA1_CDFUNC")+":"+aMarcacao[6]+CRLF;
		+" "+RetTitle("AA1_FUNFIL")+":"+aMarcacao[7]+CRLF;
		+STR0029+DtoC(aMarcacao[2])+CRLF;  	// " Data Inicio:"
		+STR0030+aMarcacao[3]+CRLF;   			// " Hora Inicio:"
		+STR0031+DToC(aMarcacao[4])+CRLF;   	// "Data Fim:"
		+STR0032+aMarcacao[5]+CRLF;   			// " Hora Fim:"
		+" "+CRLF+cMsg+CRLF
	Else
		//"Cr�tica execauto de marca��o : R_E_C_N_O_ "
		cText += STR0027+cRecno+CRLF;
		+" "+RetTitle("ABB_CODTEC")+":"+aMarcacao[1]+CRLF;
		+" "+RetTitle("AA1_CDFUNC")+":"+aMarcacao[6]+CRLF;
		+" "+RetTitle("AA1_FUNFIL")+":"+aMarcacao[7]+CRLF;
		+STR0029+DtoC(aMarcacao[2])+CRLF;  	// " Data Inicio:"
		+STR0030+aMarcacao[3]+CRLF;   			// " Hora Inicio:"
		+STR0031+DToC(aMarcacao[4])+CRLF;   	// "Data Fim:"
		+STR0032+aMarcacao[5]+CRLF;   			// " Hora Fim:"
		+" "+CRLF+cMsg+CRLF		
	Endif
EndIf

//Cria arquivo de Log
TxLogFile(ALLTRIM(cFilant)+"-MarcaErro",cText)
AADD(aRet[2], cText)

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At910MCSV
@description Gera o Arquivo CSV das Marca��es
@param aCabec: Array Contendo a filial e matricula do atendente
@param aItens:Array de Marca�oes do atendende do Atendimento que ser� atualizado
@param nHandle:Handle do Arquivo
@param lCab:Gera o cabe�alho da marca��o
@return aRetInc: Array de Retorno da Inclusao onde
		aRetInc[1]  - .t. //sUCESSO
		aRetInc[2]  - Array contendo a mensagem de sucesso/ erro
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function At910MCSV(aCabec, aItens, nHandle, lCab, cGSxInt,aEmpFil)
Local aRetInc 	:= {.T., {""}}
Local cCab 		:= ""
Local cDetCab 	:= ""
Local cLinha 	:= ""
Local cDetLinha := ""
Local nC 		:= 0
Local nY 		:= 0
Local lRM		:= .f.
Local cDelimit  := ";"
Local nTamSX3 := TamSx3("AA1_CDFUNC")[1]

lRM := cGSxInt == "3"


For nC := 1 to len(aCabec)
	cCab += AllTrim(aCabec[nC, 01]) +cDelimit
	cDetCab += Alltrim(IIF( ValType(aCabec[nC, 02])<> "D",cValToChar(aCabec[nC, 02])  , DtoS(aCabec[nC, 02])))+cDelimit
Next nC

For nC := 1 to Len(aItens)

	cLinha := cCab
	cDetLinha := cDetCab
	For nY := 1 to Len(aItens[nC])
	
		If !lRM  .OR. !(AllTrim(aItens[nC, nY, 01]) $ "P8_FILIAL#P8_MAT#P8_HORA")
				cDetLinha +=  Alltrim(IIF( ValType(aItens[nC, nY, 02])<> "D",cValToChar(aItens[nC, nY, 02]) , DtoS(aItens[nC, nY, 02])))+cDelimit
		Else
			
			If (AllTrim(aItens[nC, nY, 01]) = "P8_FILIAL")
				cDetLinha +=  Alltrim(aEmpFil[01])+cDelimit //Coligada
			ElseIf (AllTrim(aItens[nC, nY, 01]) = "P8_HORA")
				cDetLinha += StrTran(StrZero(aItens[nC, nY, 02],5,2),".", ":")+cDelimit				
			ElseIf (AllTrim(aItens[nC, nY, 01]) = "P8_MAT")
				cDetLinha += PadL(AllTrim(aItens[nC, nY, 02]), nTamSX3)+cDelimit	
			EndIf
		EndIf
	Next nY
	If lCab
		cLinha := Substr(cLinha, 1, Len(cLinha)-Len(cDelimit)) + CRLF
		fWrite(nHandle, cLinha)
		lCab := .f.
	EndIf

	cDetLinha := Substr(cDetLinha, 1, Len(cDetLinha)-Len(cDelimit)) + CRLF
	fWrite(nHandle, cDetLinha)
Next nC

Return aRetInc

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At910RHD
@description  Retorna o Diret�rio de Exporta��o do Arquivo CSV da Integra��o RH
@author 		fabiana.silva
@since 			03.05.2019
@version 		12.1.25
@return cDirArq - Diret�rio do server a ser gerado o arquivo
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function At910RHD()
Local cDirArq := SuperGetMV("MV_GSRHDIR", .F., "")

If !Empty(cDirArq) .AND. Right(cDirArq, 1) <> "\"
	cDirArq += "\"
EndIf

If !Empty(cDirArq) .AND. Left(cDirArq, 1) <> "\"
	cDirArq := "\" +cDirArq
EndIf

Return cDirArq
//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} At910RHF
@description Gera o Arquivo CSV das Marca��es
@author 		fabiana.silva
@since 			03.05.2019
@version 		12.1.25
@param cRotina: Prefixo da rotina/aquivo
@param cDirArq:Diret�irio de grava��o do arquivo
@param lDelete: Exclui arquivo caso ele exista?
@param nOpc: Op��o da Rotina Autom�tica
@return nHandle - Handle do Arquivo Gerado
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function At910RHF(cRotina, cDirArq, lDelete, nOpc, cExtensao)
Local nHandle 	:= 0
Local aDir 		:= {}
Local nC 		:= 0
Local cDirTmp 	:= ""

If !ExistDir(cDirArq)
	aDir := StrTokArr(cDirArq, "\")
	For nC := 1 to Len(aDir)
		cDirTmp += "\" +aDir[nC] +"\"
		MakeDir(cDirTmp)
	Next nC
EndIf

cNomeArq := cDirArq+cRotina+"_"+LTrim(Str(nOpc))+"_"+Dtos(Date())+"_"+StrTran(Time(), ":")+cExtensao

If File(cNomeArq)
	If lDelete
		fErase(cNomeArq)
	Else
		nHandle := FOpen(cNomeArq, FO_READWRITE)
		FSeek(nHandle, 0, 2)
	EndIf
EndIf
If nHandle = 0
	nHandle := fCreate(cNomeArq)
EndIf

Return nHandle

//------------------------------------------------------------------------------
/*/{Protheus.doc} At910ConRM
	Fun��o de verifica��o da ultima troca de turno cadastrado no RM.

@sample At910ConRM( cCdFunc, lContinua, dDiaIni, cAlias, aEscTGY, lEnvia )
@author	Augusto.Albuquerque
@since		09/04/2019
@version	P12
/*/
//------------------------------------------------------------------------------
Function At910ConRM( cCdFunc, dDiaIni, cAliasTGY, aEscTGY, lEnvia, cTDWIniHor, cTDWCodHor, cMsgRM )
Local aRet		:= {}
Local aTurno	:= {}
Local aEmpFil	:= {}
Local cWarning 	:= ""
Local cXml		:= ""
Local cError 	:= ""
Local cHoraIni	:= ""
Local cHoraFim	:= ""
Local nTotal	
Local oWS 		:= Nil

DEFAULT	cCdFunc		:= ""
DEFAULT	lContinua	:= .T.
DEFAULT dDiaIni		:= ""
DEFAULT	aEscTGY		:= {}
DEFAULT lEnvia		:= .T.

aEmpFil := GSItEmpFil(, ,  'RM', .T., .F., @cError)
oWS := GSItRMWS("RM", .F., @cError)

If oWS <> NIL
	oWS:cDataServerName := "FopHstHorData"
	If lEnvia
		oWS:cFiltro := "PFHstHor.Chapa='" + cCdFunc + "' and PFHstHor.DtMudanca <= '" + FWTimeStamp(3, dDiaIni, "00:00:00") + "'"
	Else
		oWS:cFiltro := "PFHstHor.Chapa='" + cCdFunc + "' and PFHstHor.CodHorario='" + cTDWCodHor + "' and PFHstHor.IndiniciOHor='" + cTDWIniHor + "'" 
	EndIf
	If oWS:ReadView()
		If !Empty(oWS:cReadViewResult)
			cXML := oWS:cReadViewResult
			If cXML <> "<NewDataSet />"
				oXML2 := XMLParser( cXML, "_", @cError, @cWarning)
				aTurno := XmlChildEx ( oXML2:_NEWDATASET, "_PFHSTHOR")
				If ValType(aTurno) <> Nil
					If ValType(aTurno) == "O" 
						If lEnvia
							aAdd(aRet, { aTurno:_DTMUDANCA:TEXT,;
									 PADR(aTurno:_CODHORARIO:TEXT, TamSX3("TDW_CODHOR")[1]),;
									 PADR(aTurno:_INDINICIOHOR:TEXT, TamSX3("TDX_INIHOR")[1])})
						Else
							cHoraIni := FWTimeStamp(3, MV_PAR03, "00:00:00")
							cHoraFim := FWTimeStamp(3, MV_PAR04, "00:00:00")
							If cHoraIni <= aTurno:_DTMUDANCA:TEXT .AND. aTurno:_DTMUDANCA:TEXT <= cHoraFim
								aAdd(aRet, { aTurno:_DTMUDANCA:TEXT,;
										 PADR(aTurno:_CODHORARIO:TEXT, TamSX3("TDW_CODHOR")[1]),;
										 PADR(aTurno:_INDINICIOHOR:TEXT, TamSX3("TDX_INIHOR")[1])})
							EndIf
						EndIf
					ElseIf ValType(aTurno) == "A"
						nTotal	:= Len(aTurno)
						If lEnvia
							AADD(aRet, { aTurno[nTotal]:_DTMUDANCA:TEXT,;
								 	 PADR(aTurno[nTotal]:_CODHORARIO:TEXT, TamSX3("TDW_CODHOR")[1]),;
								 	 PADR(aTurno[nTotal]:_INDINICIOHOR:TEXT, TamSX3("TDX_INIHOR")[1])} )
						Else
							cHoraIni := FWTimeStamp(3, MV_PAR03, "00:00:00")
							cHoraFim := FWTimeStamp(3, MV_PAR04, "00:00:00")
							If cHoraIni <= aTurno:_DTMUDANCA:TEXT .AND. aTurno:_DTMUDANCA:TEXT <= cHoraFim
								AADD(aRet, { aTurno[nTotal]:_DTMUDANCA:TEXT,;
								 	 PADR(aTurno[nTotal]:_CODHORARIO:TEXT, TamSX3("TDW_CODHOR")[1]),;
								 	 PADR(aTurno[nTotal]:_INDINICIOHOR:TEXT, TamSX3("TDX_INIHOR")[1])} )
							EndIf
						EndIf
					EndIf
				EndIf
			EndIf
		EndIf
	EndIf
EndIf
If Empty( aRet )
	AADD( aRet, { "", (cAliasTGY)->TDW_CODHOR, (cAliasTGY)->TDX_INIHOR } )
EndIf
If !Empty(cError)
	cMsgRM += CRLF + cError 
EndIf
Return ( aRet[1] )

//------------------------------------------------------------------------------
/*/{Protheus.doc} At910EnvRM
	Fun��o de envio ou exclus�o da escala nova para o RM

@sample At910EnvRM( cCdFunc, cMsgRM, aEscTGY, lEnvia, aEscala )
@author	Augusto.Albuquerque
@since		09/04/2019
@version	P12
/*/
//------------------------------------------------------------------------------
Function At910EnvRM( cCdFunc, cMsgRM, aEscTGY, lEnvia, aEscala, cAliasTGY, cDatRef )
Local aEmpFil	:= {}
Local cXml		:= ""
Local cError 	:= ""
Local oWS 		:= Nil

DEFAULT	cCdFunc	:= ""
DEFAULT cMsgRM	:= ""
DEFAULT aEscTGY	:= {}
DEFAULT lEnvia 	:= .T. 
DEFAULT aEscala := {}	

aEmpFil := GSItEmpFil(, ,  'RM', .T., .F., @cError)
oWS := GSItRMWS("RM", .F., @cError)
/*
Adicionado a fun��o SLEEP para que o FWTimeStamp seja diferente.
Ao enviar n�o substitua o ultimo registro por conta dos segundos iguais, e sim crie.
Ao deletar para que o processamento n�o pule o registro no RM.
*/
Sleep(1000) 

If oWS <> NIL
	If lEnvia
		cXml := "<FopHstHor>"
			cXml += "<PFHstHor>"
				cXml += "<CODCOLIGADA>" + aEmpFil[1] + "</CODCOLIGADA>"
				cXml += "<CHAPA>" + cCdFunc + "</CHAPA>"
				cXml += "<DTMUDANCA>" + FWTimeStamp( 3, SToD(cDatRef)) + "</DTMUDANCA>"
				cXml += "<CODHORARIO>" + (cAliasTGY)->TDW_CODHOR + "</CODHORARIO>"
				cXml += "<INDINICIOHOR>" + (cAliasTGY)->TDX_INIHOR + "</INDINICIOHOR>"
			cXml += "</PFHstHor>"
		cXml += "</FopHstHor>"
		oWS:cDataServerName := "FopHstHorData"
		oWS:cXML := cXml
		
		If !(oWS:SaveRecord())
			cMsgRM += STR0042 + cCdFunc + CRLF // "Problema no envio do atendente Chapa(RM): "
			cMsgRM += STR0043 + aEscTGY[1] + STR0044 + (cAliasTGY)->TDW_CODHOR  + CRLF // "Turno: " ## " Codigo Horiario(RM): " 
			cMsgRM += STR0045 + aEscTGY[3] + STR0046 +  (cAliasTGY)->TDX_INIHOR + CRLF + CRLF // "Sequencia: " ## " Indice(RM) : " 
		EndIf
	Else
		cXml := "<FopHstHor>"
			cXml += "<PFHstHor>"
				cXml += "<CODCOLIGADA>" + aEmpFil[1] + "</CODCOLIGADA>"
				cXml += "<CHAPA>" + cCdFunc + "</CHAPA>"
				cXml += "<DTMUDANCA>" + aEscala[1] + "</DTMUDANCA>"
				cXml += "<CODHORARIO>" + aEscala[2] + "</CODHORARIO>"
				cXml += "<INDINICIOHOR>" + aEscala[3] + "</INDINICIOHOR>"
			cXml += "</PFHstHor>"
		cXml += "</FopHstHor>"
		oWS:cDataServerName := "FopHstHorData"
		oWS:cXML := cXml
		If !(oWS:DeleteRecord())
			cMsgRM += STR0047 + cCdFunc + CRLF // "Problema na exclus�o do atendente Chapa(RM): "
			cMsgRM += STR0044 + aEscala[2]  + CRLF // " Codigo Horiario(RM): "
			cMsgRM += STR0046 +  aEscala[3] + CRLF + CRLF // " Indice(RM) : "
		EndIf
	EndIf
EndIf

aEscTGY := {}
Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} QryEscala
	Fun��o para retorno da escala do atendente

@sample QryEscala( cAlias )
@author	Augusto.Albuquerque
@since		09/04/2019
@version	P12
/*/
//------------------------------------------------------------------------------
Static Function QryEscala( cAliasTGY, cTurno, cCodABB, cSeq )
Local aArea		:= GetArea()
Local cQuery	:= ""

cQuery := " SELECT TDW.TDW_COD, TDX.TDX_INIHOR, TDW.TDW_CODHOR FROM "  + RetSqlName("TDX") + " TDX "
cQuery += " INNER JOIN " + RetSqlName("TDW") + " TDW "
cQuery += " ON TDW.TDW_FILIAL = '" + xFilial("TDW") + "' "
cQuery += " AND TDW.TDW_COD = TDX.TDX_CODTDW " 
cQuery += " INNER JOIN " + RetSqlName("TDV") + " TDV "
cQuery += " ON TDV.TDV_FILIAL = '" + xFilial("TDV") + "' "
cQuery += " WHERE TDX.TDX_FILIAL = '" + xFilial("TDX") + "' "
cQuery += " AND TDX.TDX_TURNO = '" + cTurno + "' " 
cQuery += " AND TDX.D_E_L_E_T_ = ' ' "
cQuery += " AND TDV.TDV_CODABB = '" + cCodABB + "' "
cQuery += " AND TDX.TDX_SEQTUR = '" + cSeq + "' "
cQuery += " AND TDW.TDW_STATUS = '1'"
cQuery += " AND TDW.D_E_L_E_T_ = ' ' "
cQuery += " AND TDV.D_E_L_E_T_ = ' ' "

cQuery := ChangeQuery(cQuery)	
DbUseArea(.T., "TOPCONN", TcGenQry(,,cQuery), cAliasTGY, .T., .T. )

RestArea(aArea)
Return ( )

