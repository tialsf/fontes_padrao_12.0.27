#INCLUDE "PROTHEUS.CH"
#INCLUDE "XMLXFUN.CH"
#INCLUDE "FILEIO.CH"
#INCLUDE "TECA940.CH"

//------------------------------------------------------------------------------
/*/{Protheus.doc} TECA920

Rotina respons�vel por importar os arquivos .csv do ftp do UMov.me

@sample  	TECA940() 

@param		oXml
@author	arthur.colado
@version	V119
@since   	18/10/2013 
@return	
/*/
//------------------------------------------------------------------------------

Function TECA940(cXml)                                                                      

	Local cLinha 	  	:= ""
	Local oXML 		:= Nil
	Local cError	  	:= ""
	Local cWarning	  	:= ""
	Local cCaminho		:= ""
	Local nHandle  	:= 0
	
	oXML := XmlParser(cXml,"_",@cError,@cWarning)
	
	If oXml <> Nil .And. Empty( cError ) .And. Empty( cWarning )
	
		If XmlChildEx( oXML, '_UMOVIMPORT' ) <> Nil .And. ;
			XmlChildEx( oXML:_UmovImport, '_RELATIVEDIRECTORY' ) <> Nil .And. ;
			XmlChildEx( oXML:_UmovImport, '_FILENAME' ) <> Nil
	
			cCaminho := oXML:_UmovImport:_RelativeDirectory:Text+"\"+;
					oXML:_UmovImport:_FileName:Text	
	
			nHandle := FT_FUSE(cCaminho) 			
			
			If nHandle == -1
				ConOut(STR0001)	//"N�o existem arquivos para importa��o"
				
			Else
	
				While !FT_FEof()	
			                
					cLinha     		:= FT_FReadLn()           
					cLinha     		:= TECA940Limpar(cLinha) 
					aDados     		:= Separa(cLinha,";")   
								
					If Len(aDados[1]) == 19
					
						dData 		:= CtoD(SubStr(aDados[1],9,2)+"/"+SubStr(aDados[1],6,2)+"/"+Left(aDados[1],4))
						cHora 		:= SubStr(aDados[1],12,5)
						cLocal		:= aDados[2]
						cGps		:= aDados[3]
						cAtende	:= aDados[4]
						cEvento	:= aDados[5]
						cFil		:= aDados[6]					
						
						AT940Query(dData, cHora, cLocal, cGps, cAtende, cEvento, cFil)
					
					EndIf
			 		
			 		FT_FSkip()                
				EndDo
				
				FT_FUSE( )			
			EndIf
		EndIf
	EndIf

Return {.T.,STR0002}//"Ok"//"OK" 

//------------------------------------------------------------------------------
/*/{Protheus.doc} AT920Query

Define a Query e pesquisa os campos na tabela conforme o arquivo que foi recebido no check-in do atendente

@owner  	arthur.colado
@author  	arthur.colado
@version 	V119
@since   	09/10/2013 
@return 	
/*/
//------------------------------------------------------------------------------

Static Function AT940Query(dDt, cHrCk, cLoc, cGps, cCod, cEvent, cFil)

	Local cAlias 		:= GetNextAlias()
	Local cFilial	    := ""
	Local cEntra		:= ""
	Local cSaida		:= ""
	Local cLocal		:= ""
	Local cCodFun		:= ""
	Local dDtEntr		:= ""
	Local cHrEntr		:= ""
	Local cHrSaida		:= "" 
	Local cStatus		:= ""
	Local nHorPerm		:= 0 
	Local nHorAnte		:= 0
	Local cCodigoABB	:= ""
	Local nTamABB		:= TamSX3("ABB_CODIGO")[1]
	Local nHrCk 		:= Val(StrTran(cHrCk, ":", ".")) 
	Local dCalDtEn		:= ""
	Local dCalDtSa		:= ""
	Local cCalHrEn		:= ""
	Local cCalHrSa		:= ""
	
	If cEvent == "Entrada" 
		
		BeginSql alias cAlias  	        
	     	SELECT		ABB.ABB_CODIGO	,	ABB.ABB_DTINI 		, 	ABB.ABB_HRINI	,
						ABS.ABS_LIMENT	
	     	FROM %table:ABB% ABB
	     	JOIN
					%table:ABS% ABS ON (ABS.ABS_FILIAL = %xfilial:ABS% AND ABB.ABB_LOCAL=ABS.ABS_LOCAL)			     		        
	     	WHERE                  
						ABB.ABB_LOCAL		=	%exp:cLoc% 
				AND		ABB.ABB_CODTEC	=	%exp:cCod%	  
				AND 	(ABB.ABB_DTINI	= 	%exp:DTOS(dDt)%) 
				AND		ABB.%notDel%
				AND		ABS.%notDel%		  
		EndSql
	
		DbSelectArea(cAlias)
	
		cStatus 			:= "3"
	
		While (cAlias)->( !Eof() )	
					
			cEntra				:= ( cAlias )->ABB_HRINI
			dDtEntr			:= ( cAlias )->ABB_DTINI
			cHrEntr			:= ( cAlias )->ABS_LIMENT
			cCodigoABB			:= Space(nTamABB)
		
			dCalDtEn			:= StoD(dDtEntr)
			cCalHrEn			:= cEntra
	 
	 		SomaDiaHor(@dCalDtEn,@cCalHrEn,HoraToInt(cHrEntr,2))
		
			dCalDtSa			:= StoD(dDtEntr)
			cCalHrSa			:= cEntra
	
			SubtDiaHor(@dCalDtSa,@cCalHrSa,HoraToInt(cHrEntr,2))
	
			If cHrCk <= cCalHrEn .AND. cHrCk >= cCalHrSa
				
				cStatus 		:= "1"
				cCodigoABB 	:= ( cAlias )->ABB_CODIGO
				Exit
			Else
				
				cStatus 		:= "2"
			EndIf
			
			( cAlias )->(DbSkip())		
		End
		
		(cAlias)->( DbCloseArea() )		
	
	ElseIF cEvent == "Saida"
			BeginSql alias cAlias  
		        
	     	SELECT		ABB.ABB_CODIGO	,	ABB.ABB_DTFIM 		, 	ABB.ABB_HRFIM	,
						ABS.ABS_LIMSAI	
	     	FROM %table:ABB% ABB
	     	JOIN
					%table:ABS% ABS ON (ABS.ABS_FILIAL = %xfilial:ABS% AND ABB.ABB_LOCAL=ABS.ABS_LOCAL)			     		        
	     	WHERE                  
						ABB.ABB_LOCAL		=	%exp:cLoc% 
				AND		ABB.ABB_CODTEC	=	%exp:cCod%	  
				AND 	(ABB.ABB_DTFIM	= 	%exp:DTOS(dDt)%) 
				AND		ABB.%notDel%
				AND		ABS.%notDel%		  
		EndSql
	
		DbSelectArea(cAlias)
	
		cStatus 			:= "3"
		
		While (cAlias)->( !Eof() )
	 				
			cSaida				:= ( cAlias )->ABB_HRFIM
			dDtEntr			:= ( cAlias )->ABB_DTFIM
			cHrSaida			:= ( cAlias )->ABS_LIMSAI
			cCodigoABB			:= Space(nTamABB)
	
	
			dCalDtEn			:= StoD(dDtEntr)
			cCalHrEn			:= cSaida
	
			SomaDiaHor(@dCalDtEn,@cCalHrEn,HoraToInt(cHrSaida,2))
	
			dCalDtSa			:= StoD(dDtEntr)
			cCalHrSa			:= cSaida
	
			SubtDiaHor(@dCalDtSa,@cCalHrSa,HoraToInt(cHrSaida,2))
	
			If cHrCk <= cCalHrEn .AND. cHrCk >= cCalHrSa
				cStatus 		:= "1"
				cCodigoABB 	:= ( cAlias )->ABB_CODIGO
				Exit
			Else
			
				cStatus 		:= "2"
				
			EndIf		
			
			( cAlias )->(DbSkip())	
				
		End
	
		(cAlias)->(DbCloseArea())	
	
	EndIf	
	
	
	DbSelectArea("TIM")
	TIM->(DbSetOrder(1))	//TIM_FILIAL+TIM_CODTEC+TIM_DATA+TIM_HORA
	
	RecLock("TIM",.T.)
			
	TIM->TIM_DATA 		:= dDt
	TIM->TIM_HORA 	  	:= cHrCk	
	TIM->TIM_LOCAL	:= cLoc		
	TIM->TIM_GPS		:= cGps	
	TIM->TIM_CODTEC	:= cCod
	TIM->TIM_EVENTO	:= cEvent
	TIM->TIM_FILIAL	:= cFil
	TIM->TIM_STATUS	:= cStatus
	TIM->TIM_CODABB	:= cCodigoABB
	
				
	TIM->(MsUnlock())
	
	If cStatus  == "1"
	 	dbSelectArea("ABB")
	 	ABB-> (DbSetOrder (8))//ABB_FILIAL + ABB_CODIGO
	 	
	 	If(DbSeek(xFilial("ABB") + cCodigoABB))
	 		RecLock("ABB",.F.)
			
			If cEvent == "Entrada"
			
				ABB->ABB_DTINI := dCalDtEn
				ABB->ABB_HRINI := cHrCk
			
			ElseIf cEvent == "Saida"
			
				ABB->ABB_DTFIM := dCalDtEn
				ABB->ABB_HRFIM := cHrCk
			EndIf
			 		
	 		ABB->(MsUnlock())
	 		
	 	EndIf
	 	
	EndIf

Return 

//------------------------------------------------------------------------------
/*/{Protheus.doc} TECA920

Trata os caracteres n�o comuns que foram importados nos arquivos do Umov.me

@sample  	TECA940Limpar() 

@param		Nenhum
@author	arthur.colado
@version	V119
@since   	18/10/2013 
@return	cRet
/*/
//------------------------------------------------------------------------------

Static Function TECA940Limpar(cTxt)

	Local cRet	:= ""
	Local nX	:= 0
	
	For nX := 1 To Len(cTxt)
			If SubStr(cTxt,nX,1) $ "�í"
			
				If SubStr(cTxt,nX,1) == "�"
				
					cRet += "c"
					
				ElseIf SubStr(cTxt,nX,1) == "�"
				
					cRet += ""	
					
				ElseIf SubStr(cTxt,nX,1) == "�"
				
					cRet += "i"		
				EndIf
			Else
				cRet += SubStr(cTxt,nX,1)
			EndIf
					
	Next nX

Return AllTrim(cRet)