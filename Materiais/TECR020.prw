#Include "Rwmake.ch"
#Include "Protheus.ch"      
#Include "TOPCONN.ch"
#Include "TECR020.ch"
Static cAutoPerg := "TECR020" 
//-------------------------------------------------------------------
/*/{Protheus.doc} TECR020
Monta as defini�oes do relatorio de Recursos (Atendentes) N�o-Alocados.

@author filipe.goncalves
@since 21/01/2016
@version P12.1.11
/*/
//-------------------------------------------------------------------
Function TECR020()
Local oReport := Nil
Private cPerg	:= "TECR020" 

//������������������������������������������������������������������������Ŀ
//� PARAMETROS                                                             �
//� MV_PAR01 : Data de ?                                                   �
//� MV_PAR02 : Data ate?                                                   �
//� MV_PAR03 : Atendente de ?                                              �
//� MV_PAR04 : Atendente ate ?                                             �
//� MV_PAR05 : Centro de custo de ?                                        �
//� MV_PAR06 : Centro de custo ate ?                                       �
//�������������������������������������������������������������������������� 

//Exibe dialog de perguntes ao usuario
If !Pergunte(cPerg,.T.)
	Return nil
EndIf

//Pinta o relatorio a partir das perguntas escolhidas
oReport := ReportDef()   
oReport:PrintDialog()  
Return

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} reportDef
Monta as defini�oes do relatorio de Recursos (Atendentes) N�o-Alocados.

@author filipe.goncalves
@since 21/01/2016
@version P12.1.11
@return  Nil
/*/
//-------------------------------------------------------------------------------------
Static Function ReportDef()

Local cTitulo 	:= STR0002
Local oReport
Local oSection1

If TYPE("cPerg") == "U"
	cPerg	:= "TECR020"
EndIf

oReport 	:= TReport():New(cPerg, cTitulo, cPerg , {|oReport| PrintReport(oReport)},STR0001)
oSection1 := TRSection():New(oReport,"Atendentes",{"AA1","SRA","TGY"})

oReport:ShowHeader()
oReport:SetPortrait()
oReport:SetTotalInLine(.F.)
oSection1:SetTotalInLine(.F.)

TRCell():New(oSection1, STR0024 , "SRA", STR0023 ,PesqPict('SRA',"RA_FILIAL")		,TamSX3("RA_FILIAL")[1],/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection1, STR0009 , "SRA", STR0016 ,PesqPict('SRA',"RA_MAT")		,TamSX3("RA_MAT")[1],/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection1, STR0010 , "AA1", STR0017 ,PesqPict('AA1',"AA1_CODTEC")	,TamSX3("AA1_CODTEC")[1],/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection1, STR0011 , "AA1", STR0018 ,PesqPict('AA1',"AA1_NOMTEC")	,TamSX3("AA1_NOMTEC")[1],/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection1, STR0012 , "SRA", STR0019 ,PesqPict('SRA',"RA_SEXO")		,TamSX3("RA_SEXO")[1],/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection1, STR0013 , "TGY", STR0020 ,PesqPict('TGY',"TGY_DTINI")	,TamSX3("TGY_DTINI")[1],/*lPixel*/,/*{|| code-block de impressao }*/)  

oSection1:Cell(STR0009):SetAlign("LEFT")
oSection1:Cell(STR0010):SetAlign("LEFT")
oSection1:Cell(STR0011):SetAlign("LEFT")
oSection1:Cell(STR0012):SetAlign("LEFT")
oSection1:Cell(STR0013):SetAlign("LEFT")

Return (oReport) 

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} PrintReport
Gera o relatorio de Recursos (Atendentes) N�o-Alocados.

@author filipe.goncalves
@since 21/01/2016
@version P12.1.11
@return  Nil
/*/
//-------------------------------------------------------------------------------------
Static Function PrintReport(oReport)

Local aArea	:= GetArea() 
Local oSection1	:= oReport:Section(1)
Local dDtIni	:= MV_PAR01
Local dDtFim	:= MV_PAR02
Local lExistTGY := .F.
Local lExistABB	:= .F.
Local cAliasTGY	:= ""
Local cAliasABB	:= ""
Local nPosTXBDtI:= 0
Local nPosTXBDtF:= 0	
Local lResRHTXB	:= TableInDic("TXB") //Restri��es de RH
Local lMVPAR07 	:= .F. 
Local aFilsPAR07 := {}
Local cXfilAA1 	:= " = '" + xFilial("AA1", cFilAnt) +"'"
Local cXfilTGY 	:= " = '" + xFilial("TGY", cFilAnt) +"'"
Local cXfilABB 	:= " = '" + xFilial("ABB", cFilAnt) +"'" 
Local cXfilTCU	:= " = '" + xFilial("TCU", cFilAnt) +"'"
Local cFiAA1SRA := FWJoinFilial("SRA","AA1","SRA","AA1",.T.)
Local cValMVPAR07 := ""
Local cFilBkp	:= ""
Local aPrjAgd	:= {}
Local nX		:= 0
Local nY 		:= 0
Local cChavTGY	:= ""
Local lDiaTrb	:= .F.
Local lVerRestr	:= .F.

lMVPAR07 := TecHasPerg("MV_PAR07","TECR020")

MakeSqlExpr("TECR020")

If lMVPAR07 .And. !Empty(MV_PAR07)
	cValMVPAR07 := STRTRAN(MV_PAR07, "AA1_FILIAL")
	cValMVPAR07 := REPLACE(cValMVPAR07, " IN")
	cValMVPAR07 := REPLACE(cValMVPAR07, "(")
	cValMVPAR07 := REPLACE(cValMVPAR07, ")")
	cValMVPAR07 := REPLACE(cValMVPAR07, "'")
	aFilsPAR07 := StrTokArr(cValMVPAR07,",")

	For nX := 1 To LEN(aFilsPAR07)
		If nX == 1
			cXfilAA1 := " IN ("
			cXfilTGY := " IN ("
			cXfilABB := " IN ("
			cXfilTCU := " IN ("
		EndIf

		cXfilAA1 += "'" + xFilial("AA1", aFilsPAR07[nX] ) 
		cXfilTGY += "'" + xFilial("TGY", aFilsPAR07[nX] )
		cXfilABB += "'" + xFilial("ABB", aFilsPAR07[nX] )
		cXfilTCU += "'" + xFilial("TCU", aFilsPAR07[nX] )

		If nX >= 1 .AND. nX < LEN(aFilsPAR07)
			cXfilAA1 +=  "',"
			cXfilTGY +=  "',"
			cXfilABB +=  "',"
			cXfilTCU +=  "',"
		EndIf

		If nX == LEN(aFilsPAR07)
			cXfilAA1 += " ') "		
			cXfilTGY += " ') "
			cXfilABB += " ') "
			cXfilTCU += " ') "
		EndIf
	Next nX
EndIf

cXfilAA1 := "%"+cXfilAA1+"%" 
cXfilTGY := "%"+cXfilTGY+"%" 
cXfilABB := "%"+cXfilABB+"%"  
cXfilTCU := "%"+cXfilTCU+"%"
cFiAA1SRA := "%"+cFiAA1SRA+"%"

BEGIN REPORT QUERY oReport:Section(1)

	BeginSql alias "QRY"

		SELECT AA1.AA1_CODTEC, AA1.AA1_NOMTEC, SRA.RA_FILIAL, SRA.RA_SEXO, RA_MAT, RA_NOME
		  FROM %table:AA1% AA1
		  	INNER JOIN %table:SRA% SRA ON %exp:cFiAA1SRA% 
		  	AND SRA.RA_MAT = AA1.AA1_CDFUNC
		  	AND SRA.RA_FILIAL = AA1.AA1_FUNFIL	  
		  	AND SRA.%notDel% 
		 WHERE AA1.AA1_FILIAL  %exp:cXfilAA1%
			AND AA1.AA1_CODTEC BETWEEN %Exp:MV_PAR03% AND %Exp:MV_PAR04%
			AND AA1.AA1_CC	   BETWEEN %Exp:MV_PAR05% AND %Exp:MV_PAR06%
		   	AND AA1.%notDel%
		ORDER BY SRA.RA_NOME 
		
	EndSql

END REPORT QUERY oReport:Section(1)

//Define tamanho da regua de processamento
oReport:SetMeter(QRY->(RecCount()))

//Monta a primeira secao do relatorio
oSection1:Init()
oSection1:SetHeaderSection(.T.)

dbSelectArea("QRY")

//Printa cada registro da query de busca no relatorio
While QRY->(!Eof())

	lVerRestr := .F.
	aPrjAgd   := {}
	cAliasTGY := GetNextAlias()
	cChavTGY  := ""

	BeginSql Alias cAliasTGY

		SELECT TGY.TGY_FILIAL,TGY.TGY_ESCALA,TGY.TGY_CODTDX,TGY.TGY_CODTFF,TGY.TGY_ITEM,TGY.TGY_DTINI,TGY.TGY_ULTALO
		FROM %table:TGY% TGY
		WHERE TGY.TGY_FILIAL %exp:cXfilTGY%
			AND	TGY.TGY_ATEND = %Exp:QRY->AA1_CODTEC%
				AND (TGY.TGY_DTINI BETWEEN %Exp:MV_PAR01% AND %Exp:MV_PAR02% OR TGY.TGY_ULTALO BETWEEN %Exp:MV_PAR01% AND %Exp:MV_PAR02%
					OR (%Exp:MV_PAR01% BETWEEN TGY.TGY_DTINI AND TGY.TGY_ULTALO OR %Exp:MV_PAR02% BETWEEN TGY.TGY_DTINI AND TGY.TGY_ULTALO)) 
			AND TGY.TGY_ULTALO <> ''
			AND TGY.%notDel%
			AND EXISTS ( SELECT TCU.TCU_COD 
							FROM %table:TCU% TCU 
							WHERE TCU.TCU_FILIAL %exp:cXfilTCU% 
							AND TCU.TCU_COD = TGY.TGY_TIPALO
							AND TCU.TCU_RESTEC <> '1'
							AND TCU.%notDel% )
	EndSql

	cAliasABB := GetNextAlias()
		
	BeginSql Alias cAliasABB
		SELECT ABB.ABB_CODIGO,ABB.ABB_DTINI, ABB.ABB_DTFIM
		FROM %table:ABB% ABB
		WHERE ABB.ABB_FILIAL %exp:cXfilABB%
			AND	ABB.ABB_CODTEC = %Exp:QRY->AA1_CODTEC%
				AND (ABB.ABB_DTINI BETWEEN %Exp:MV_PAR01% AND %Exp:MV_PAR02% OR ABB.ABB_DTFIM BETWEEN %Exp:MV_PAR01% AND %Exp:MV_PAR02%
					OR (%Exp:MV_PAR01% BETWEEN ABB.ABB_DTINI AND ABB.ABB_DTFIM OR %Exp:MV_PAR02% BETWEEN ABB.ABB_DTINI AND ABB.ABB_DTFIM))
            AND ABB.%notDel%
			AND ABB.ABB_ATIVO <> '2'
			AND EXISTS ( SELECT TCU.TCU_COD 
							FROM %table:TCU% TCU 
							WHERE TCU.TCU_FILIAL %exp:cXfilTCU% 
							AND TCU.TCU_COD = ABB.ABB_TIPOMV
							AND TCU.TCU_RESTEC <> '1'
							AND TCU.%notDel% )
		ORDER BY ABB.ABB_DTINI
	EndSql
				
	If lMVPAR07 .And. !Empty(aFilsPAR07)
	
		cFilBkp := cFilAnt

		For nX := 1 To Len(aFilsPAR07)

			cFilAnt := aFilsPAR07[nX]
			// Consulta os conflitos do atendente.
			ChkCfltAlc(MV_PAR01, MV_PAR02, QRY->AA1_CODTEC)
			
		Next nX

		If lResRHTXB .And. nPosTXBDtI == 0 .And. nPosTXBDtF == 0
			nPosTXBDtI:= AScan(AT330ArsSt("aCfltHead"),{|e| e == 'TXB_DTINI'})
			nPosTXBDtF:= AScan(AT330ArsSt("aCfltHead"),{|e| e == 'TXB_DTFIM'})
		Endif

		cFilAnt := cFilBkp
		
	Else
		// Consulta os conflitos do atendente.
		ChkCfltAlc(MV_PAR01, MV_PAR02, QRY->AA1_CODTEC)				

		If lResRHTXB .And. nPosTXBDtI == 0 .And. nPosTXBDtF == 0
			nPosTXBDtI:= AScan(AT330ArsSt("aCfltHead"),{|e| e == 'TXB_DTINI'})
			nPosTXBDtF:= AScan(AT330ArsSt("aCfltHead"),{|e| e == 'TXB_DTFIM'})
		Endif

	Endif

	While dDtIni <= dDtFim

		lExistTGY := .F.
		lExistABB := .F.
		lRestrRH  := .F.
		lDiaTrb   := .F.

		(cAliasTGY)->(dBGoTop())

		While (cAliasTGY)->(!Eof())
			If dDtIni >= sTod((cAliasTGY)->TGY_DTINI) .And. dDtIni <= sTod((cAliasTGY)->TGY_ULTALO)				
				lExistTGY := .T.
				//Proje��o de agenda.
				If cChavTGY <> (cAliasTGY)->(TGY_FILIAL+TGY_ESCALA+TGY_CODTDX+TGY_CODTFF+TGY_ITEM) .And.;
				 	TDX->(MsSeek(xFilial("TDX")+(cAliasTGY)->TGY_CODTDX))
					
					Aadd(aPrjAgd,ProjAgend(sTod((cAliasTGY)->TGY_DTINI),sTod((cAliasTGY)->TGY_ULTALO),TDX->TDX_TURNO,TDX->TDX_SEQTUR))
					cChavTGY := (cAliasTGY)->(TGY_FILIAL+TGY_ESCALA+TGY_CODTDX+TGY_CODTFF+TGY_ITEM)
				Endif
			Endif
			(cAliasTGY)->(dbSkip())
		EndDo

		(cAliasABB)->(dBGoTop())

		While (cAliasABB)->(!Eof())
			If dDtIni >= sTod((cAliasABB)->ABB_DTINI) .And. dDtIni <= sTod((cAliasABB)->ABB_DTFIM)
				lExistABB := .T.
				Exit
			Elseif sTod((cAliasABB)->ABB_DTINI) > dDtIni
				Exit
			Endif
			(cAliasABB)->(dbSkip())
		EndDo
		
		//Verifica a exclus�o de agenda
		If lExistTGY .And. !lExistABB
			For nX := 1 To Len(aPrjAgd)
				For nY := 1 To Len(aPrjAgd[nX])
					If aPrjAgd[nX,nY,1] == dDtIni .And. aPrjAgd[nX,nY,3] == "S"
						lDiaTrb := .T.
						Exit
					Elseif aPrjAgd[nX,nY,1] > dDtIni
						Exit
					Endif
				Next nY
			Next nX		
		Endif
		
		If (!lExistABB .And. !lExistTGY) .Or. lDiaTrb
							
			If !lRestrRH .And. Len(AT330ArsSt("aDiasFer")) > 0
				lRestrRH := (Ascan(AT330ArsSt("aDiasFer"),{|x| dDtIni >= x[2] .And. dDtIni <= x[3]} ) > 0)
			EndIf

			If !lRestrRH .And. Len(AT330ArsSt("aDiasFer2")) > 0
				lRestrRH := (Ascan(AT330ArsSt("aDiasFer2"),{|x| dDtIni >= x[2] .And. dDtIni <= x[3]} ) > 0)
			EndIf

			If !lRestrRH .And. Len(AT330ArsSt("aDiasFer3")) > 0
				lRestrRH := (Ascan(AT330ArsSt("aDiasFer3"),{|x| dDtIni >= x[2] .And. dDtIni <= x[3]} ) > 0)
			EndIf

			If !lRestrRH .And. Len(AT330ArsSt("aDiasDem")) > 0
				lRestrRH := (Ascan(AT330ArsSt("aDiasDem"),{|x| dDtIni >= x[2] } ) > 0)
			EndIf

			If !lRestrRH .And. Len(AT330ArsSt("aDiasAfast")) > 0
				lRestrRH := (Ascan(AT330ArsSt("aDiasAfast"),{|x| dDtIni >= x[2] .And. dDtIni <= x[3]} ) > 0)
			EndIf

			If lResRHTXB .And. !lRestrRH .And. Len(AT330ArsSt("ACFLTATND")) > 0  .And. nPosTXBDtI > 0 .And. nPosTXBDtF > 0
				lRestrRH := (Ascan(AT330ArsSt("ACFLTATND"),{|x| !Empty(x[nPosTXBDtI]) .And. dDtIni >= sTod(x[nPosTXBDtI]) .And. ( Empty(x[nPosTXBDtF]) .Or. dDtIni <= sTod(x[nPosTXBDtF])) } ) > 0)
			Endif

			If !lRestrRH 

				//Atendente Ocioso nesse dia
				oSection1:Cell(STR0009):SetValue(QRY->RA_MAT)
				oSection1:Cell(STR0010):SetValue(QRY->AA1_CODTEC)
				oSection1:Cell(STR0011):SetValue(QRY->AA1_NOMTEC)
				oSection1:Cell(STR0012):SetValue(QRY->RA_SEXO)
				oSection1:Cell(STR0013):SetValue(dDtIni)
				
				If !isBlind()
					oSection1:PrintLine()
				EndIf
		
			Endif

		EndIf

		//Incremento da regua
		oReport:IncMeter()
	
		dDtIni++			
	
	EndDo

	dDtIni := MV_PAR01
		
	QRY->(dbSkip())

	(cAliasTGY)->(dbCloseArea())
	(cAliasABB)->(dbCloseArea())
	
	AT330ArsSt(,.T.)

	//botao cancelar
	If oReport:Cancel()
		Exit
	EndIf

EndDo

QRY->(dbCloseArea())

oSection1:Finish()

RestArea(aArea)

Return

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} GetPergTRp
Retorna o nome do Pergunte utilizado no relat�rio
Fun��o utilizada na automa��o
@author Mateus Boiani
@since 31/10/2018
@return cAutoPerg, string, nome do pergunte
/*/
//-------------------------------------------------------------------------------------
Static Function GetPergTRp()

Return cAutoPerg

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} ProjAgend
Retorna a proje��o de agenda de um determinado per�odo
@author Kaique Schiller
@since 13/05/2020
@return aProjAgd, array, todos os dias do turno.
/*/
//-------------------------------------------------------------------------------------
Static Function ProjAgend(dDtIni,dDtFim,cTurno,cSeqIni)
Local aProjAgd  := {}
Local dDtProj   := dDtIni
Local aDiasSPJ  := {}
Local cAliasSPJ := GetNextAlias()
Local lPriDia	:= .F.
Local cUltSeq	:= ""
Local nSeq		:= 0
Local nPos		:= 0

BeginSql Alias cAliasSPJ
	SELECT SPJ.PJ_TURNO,
		   SPJ.PJ_SEMANA,
		   SPJ.PJ_DIA,
		   SPJ.PJ_TPDIA
	FROM %table:SPJ% SPJ
	WHERE SPJ.PJ_FILIAL = %xFilial:SPJ%
		AND SPJ.PJ_TURNO = %Exp:cTurno%
		AND SPJ.%notDel%
	ORDER BY PJ_TURNO,PJ_SEMANA,PJ_DIA
EndSql

While (cAliasSPJ)->(!Eof())
	aAdd(aDiasSPJ,{(cAliasSPJ)->PJ_SEMANA,; //Sequencia
					(cAliasSPJ)->PJ_DIA,;   //Dia semana
					(cAliasSPJ)->PJ_TPDIA}) //Tipo do dia - "S" Trabalahdo
	(cAliasSPJ)->(dbSkip())
	If !Empty((cAliasSPJ)->PJ_SEMANA)
		cUltSeq := (cAliasSPJ)->PJ_SEMANA
	Endif
EndDo

cSeqAux := cSeqIni

While dDtProj <= dDtFim

	If lPriDia .And. Dow(dDtProj) == 2
		nSeq++
		If (nSeq+Val(cSeqAux)) > Val(cUltSeq)
			nSeq 	:= 0
			cSeqAux := cSeqIni
		Else
			cSeqAux := PadL(cValToChar((nSeq+Val(cSeqAux))),TamSx3("PJ_SEMANA")[1],"0")
		Endif
	Endif
	
	If (nPos := AScan( aDiasSPJ ,{|e| e[1] == cSeqAux .And. e[2] == cValToChar(Dow(dDtProj)) })) > 0
		aAdd(aProjAgd, {dDtProj, aDiasSPJ[nPos,2],aDiasSPJ[nPos,3] })
		lPriDia := .T.
	Endif
		
	dDtProj++
EndDo

(cAliasSPJ)->(DbCloseArea())

Return aProjAgd
