#include 'totvs.ch'
#include 'TECXFUNB.CH'
#INCLUDE "FWMVCDEF.CH"
Static lTecDelPln := .F.
//------------------------------------------------------------------------------
/*/{Protheus.doc} TecGetApnt

@description Retorna em forma de Array os materiais apontados

@param cCod, string, C�digo da TFH ou TFG
@param cTable, string, TFT ou TFS, dependendo da tabela a ser pesquisada
@param aColumns, array, colunas que devem ser incluidas no SELECT.

@return aRet, array, resultado da query

@author	Mateus Boiani
@since		03/12/2018
/*/
//------------------------------------------------------------------------------
Function TecGetApnt(cCod, cTable, aColumns)
Local nX
Local aArea := GetArea()
Local cSql := "SELECT "
Local aRet := {}
Local aAux := {}
Local cAliasAux := GetNextAlias()

Default aColumns := {cTable+"_CODIGO"}

For nX := 1 To LEN(aColumns)
	cSql += " " + cTable+"."+aColumns[nX] + " ,"
Next
cSql := LEFT(cSql, LEN(cSql)-1)
cSql += " FROM " + RetSqlName(cTable) + " " + cTable
cSql += " WHERE " + cTable+".D_E_L_E_T_ = ' ' AND " + cTable
cSql += "."+cTable+"_FILIAL = '" +xFilial(cTable) + "'"
cSql += " AND " + cTable+"."+cTable+"_COD"+IIF(LEFT(cTable,3)=="TFT","TFH","TFG") + " = '"
cSql += cCod + "'"

cSql := ChangeQuery(cSql)
dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAliasAux, .F., .T.)

While (cAliasAux)->(!Eof())
	For nX := 1 To LEN(aColumns)
		AADD(aAux , (&("('"+cAliasAux+"')->("+aColumns[nX]+")")))
	Next
	AADD( aRet, aAux)
	aAux := {}
	(cAliasAux)->(DbSkip())
End

(cAliasAux)->(DbCloseArea())

RestArea(aArea)

Return aRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TecSumPrd

@description Fun��o para Or�amento Agrupados. Soma o total de um determinado produto
agrupador dentro de uma TFL no or�amento

@param cCodTFJ, string, C�digo da TFJ
@param cCodTFL, string, C�digo da TFL
@param cCodProd, string, C�digo do produto que ser� somado

@return nRet, numeric, resultado da soma dos totalizadores

@author	Mateus Boiani
@since		03/12/2018
/*/
//------------------------------------------------------------------------------
Function TecSumPrd(cCodTFJ, cCodTFL ,cCodProd)
Local aArea := GetArea()
Local cAliasAux := GetNextAlias()
Local cSQL := ""
Local nRet := 0

DbSelectArea("TFJ")
DbSetOrder(1)

If MsSeek(xFilial("TFJ") + cCodTFJ)
	cSQL := " SELECT ( "
	If cCodProd == TFJ->TFJ_GRPRH
		cSQL += " SUM(TFL.TFL_TOTRH) +"
	EndIf
	If cCodProd == TFJ->TFJ_GRPMI
		cSQL += " SUM(TFL.TFL_TOTMI) +"
	EndIf
	If cCodProd == TFJ->TFJ_GRPMC
		cSQL += " SUM(TFL.TFL_TOTMC) +"
	EndIf
	If cCodProd == TFJ->TFJ_GRPLE
		cSQL += " SUM(TFL.TFL_TOTLE) +"
	EndIf
	cSQL := LEFT(cSQL, LEN(cSQL)-1)
	cSQL += " ) AS TOTAL "
	cSQL += " FROM " + RetSqlName("TFL") + " TFL "
	cSQL += " WHERE TFL.TFL_CODPAI = '" + cCodTFJ + "' AND "
	cSQL += " TFL.TFL_CODIGO = '" + cCodTFL + "' AND "
	cSQL += " TFL.D_E_L_E_T_ = ' ' AND TFL.TFL_FILIAL = '" + xFilial("TFJ") + "'"
	cSQL := ChangeQuery(cSQL)
	dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAliasAux, .F., .T.)
	nRet := (cAliasAux)->(TOTAL)
	(cAliasAux)->(DbCloseArea())
EndIf

RestArea(aArea)

Return nRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TecSumInMdl

@description Fun��o para Or�amento Agrupados. Soma o total de um determinado produto
agrupador dentro de uma TFL no or�amento. Similar a fun��o TecSumPrd, por�m somando
nos modelos ao inv�s de consultar o banco de dados

@param oModelTFL, obj, Modelo da TFL
@param oModelTFJ, obj, Modelo da TFJ
@param cCodProd, string, C�digo do produto que ser� somado

@return nRet, numeric, resultado da soma dos totalizadores

@author	Mateus Boiani
@since		03/12/2018
/*/
//------------------------------------------------------------------------------
Function TecSumInMdl(oModelTFL, oModelTFJ, cCodPrd)
Local nRet := 0

nRet += IIF(cCodPrd == oModelTFJ:GetValue("TFJ_GRPMI"),oModelTFL:GetValue("TFL_TOTMI"),0)
nRet += IIF(cCodPrd == oModelTFJ:GetValue("TFJ_GRPMC"),oModelTFL:GetValue("TFL_TOTMC"),0)
nRet += IIF(cCodPrd == oModelTFJ:GetValue("TFJ_GRPLE"),oModelTFL:GetValue("TFL_TOTLE"),0)
nRet += IIF(cCodPrd == oModelTFJ:GetValue("TFJ_GRPRH"),oModelTFL:GetValue("TFL_TOTRH"),0)

Return nRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TecMedPrd

@description Apartir de um c�digo de produto, verifica na CNB se h� medi��es,
somando o valor do campo CNB_QTDMED

@param cContra, string, c�digo do contrato
@param cRev, string, revis�o do contrato
@param cPlan, string, c�digo da planilha (CNB_NUMERO)
@param cProd, string, c�digo do produto
@param cItem, string, item do produto na CNB (opcional)

@return nRet, numeric, resultado da soma do campo CNB_QTDMED

@author	Mateus Boiani
@since		03/12/2018
/*/
//------------------------------------------------------------------------------
Function TecMedPrd(cContra, cRev, cPlan, cProd, cItem)
Local nRet := 0
Local cSql := ""
Local aArea := GetArea()
Local cAliasAux := GetNextAlias()

Default cItem := ""

cSql := " SELECT SUM(CNB.CNB_QTDMED) AS QTDMED FROM " + RetSqlName("CNB") + " CNB "
cSql += " WHERE  CNB.CNB_FILIAL = '" +  xFilial("CNB") + "' AND CNB.D_E_L_E_T_ = ' ' AND "
cSql += " CNB.CNB_CONTRA = '" + cContra + "' AND CNB.CNB_PRODUT = '" + cProd + "' AND "
cSql += " CNB.CNB_REVISA = '" + cRev + "' AND CNB.CNB_NUMERO = '" + cPlan + "'"
If !EMPTY(cItem)
	cSql += " AND CNB.CNB_ITEM = '" + cItem + "'"
EndIf
cSql := ChangeQuery(cSql)
dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAliasAux, .F., .T.)

If !(cAliasAux)->(EOF())
	nRet := (cAliasAux)->(QTDMED)
EndIf

(cAliasAux)->(DbCloseArea())

RestArea(aArea)
Return nRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} HasMunProd

Verifica se existe o dicionario de dados para controle de muni��o por produto
@author Matheus Lando Raimundo
@since 17/12/18

/*/
//--------------------------------------------------------------------------------------------------------------------
Function HasMunPrd()
Local lRet := .F.

lRet := AliasInDic('T49')

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} HasOrcSimp

Verifica se existe o dicionario de dados do Or�amento Simplificado
@author Mateus Boiani
@since 19/12/2018

/*/
//--------------------------------------------------------------------------------------------------------------------
Function HasOrcSimp()

Return (TFJ->(ColumnPos('TFJ_ORCSIM')) > 0)

//------------------------------------------------------------------------------
/*/{Protheus.doc} GSGetIns
Recupera os insumos utilizados no GS

@sample 	GSGetCtx(cTipo)
@param		cTipo	Tipo do insumo

@author	guilherme.pimentel
@since		15/05/2018

@Obs		Os tipos de material podem ser:
			MI - Material de implanta��o
			MC - Material de consumo
			RH - Recursos humanos
			LE - Loca��o de Equipamentos
			AR - Armamento

			Os tipos de insumo MI e MC utilizam o memso par�metro
/*/
//------------------------------------------------------------------------------

Function GSGetIns(cTipo)
Local lRet := .T.

If cTipo $ 'MI|MC'
	lRet	:= SuperGetMv("MV_GSMIMC",,.T.)
Else
	lRet	:= SuperGetMv("MV_GS"+cTipo,,.T.)
EndIf

Return lRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TecNvl

@description Faz a mesma coisa que o ISNULL do SqlServer ou o NVL do Oracle,
Se o primeiro par�metro for null, retorna o valor do segundo par�metro, caso contr�rio,
retorna o valor do primeiro par�metro

@author	Mateus Boiani
@since	16/01/2019
/*/
//------------------------------------------------------------------------------
Function TecNvl(uValue1, uValue2)
Local uRet

If ValType(uValue1) != "U"
	uRet := uValue1
Else
	uRet := uValue2
EndIf

Return uRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TecActivt

@description Realiza o ACTIVATE de um modelo
(Essa fun��o � amplamente utilizada na manipula��o da Tabela de Precifica��o do TECA740F)

@author	Mateus Boiani
@since	21/01/2019
/*/
//------------------------------------------------------------------------------
Function TecActivt(oModel)

Return oModel:Activate()

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} TecDaysIn
@description Recebe dois per�odos de datas e retorna quantos dias do per�odo 1 existem no per�odo 2
@param dDtIni, date, Inicio do per�odo 1
@param dDtFim, date, Fim do per�odo 1
@param dSelectD, date, Inicio do per�odo 2
@param dSelectAt, date, Fim do per�odo 2
@author  Mateus Boiani
@version P12
@since 	 28/06/2018
@return nRet, int, quantidade de dias
/*/
//--------------------------------------------------------------------------------------
Function TecDaysIn(dDtIni, dDtFim, dSelectD, dSelectAt)

Local nRet := 0
Local nAux := dDtFim - dDtIni
Local nX

For nX := 0 To nAux
	If dDtIni + nX >= dSelectD .AND. dDtIni + nX <= dSelectAt
		nRet++
	EndIf
Next

Return nRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TecHasPerg

@description Verifica se um determinado pergunte existe em um grupo de perguntas
@author	Mateus Boiani
@since	30/04/2019
/*/
//------------------------------------------------------------------------------
Function TecHasPerg(cX1_VAR01,cX1_GRUPO)
Local lRet := .F.
Local aArea := GetArea()
Local aAreaX1 := SX1->(GetArea())

DbSelectArea("SX1")
SX1->(DbSetOrder(1))

If SX1->(DbSeek(cX1_GRUPO))
	While Alltrim(SX1->X1_GRUPO) == Alltrim(cX1_GRUPO) .AND. !EOF()
		If UPPER(Alltrim(SX1->X1_VAR01)) == UPPER(Alltrim(cX1_VAR01))
			lRet := .T.
			Exit
		EndIf
		SX1->(DbSkip())
	End
EndIf

RestArea(aAreaX1)
RestArea(aArea)
Return lRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TecConfAlo

@description Exibe um log para apresentar se na demiss�o ou qualquer tipo de
afastamento se existe conflito com aloca��o.
@author	Augusto Albuquerque
@since	20/05/2019
/*/
//------------------------------------------------------------------------------
Function TecConfAlo( oModel, dDataDem1 )

Local cAliasTemp	:= GetNextAlias()
Local cAliasDT		:= GetNextAlias()
Local aArea			:= GetArea()
Local aPEConfa		:= {}
Local aAgenda		:= {}
Local cABBIdCFal	:= ""
Local cMatAtendente	:= ""
Local cNomeAtendete := ""
Local cCodTec		:= ""
Local cQuery		:= ""
local cSql			:= ""
Local cMsg			:= ""
Local cAusencia		:= ""
Local dDemissa		:= ""
Local dAusDataI		:= ""
Local dAusDataF		:= ""
Local dABBDataI		:= ""
Local dABBDataF		:= ""
Local lGPEA010		:= IsInCallStack("GPEA010")
Local lGPEA180		:= IsInCallStack("GPEA180")
Local lGPEM060		:= IsInCallStack("GPEM060")
Local lGPEM040		:= IsInCallStack("GPEM040")
Local lGPEA240		:= .F.
Local lGPEM030		:= .F.
Local lATPECONFA	:= ExistBlock('ATPECONFA')
Local lVer			:= .F.
Local nX
Local oMdlGPEM		:= Nil


Default oModel		:= Nil
Default dDataDem1 	:= CtoD("")

If ValType(oModel) == "O"
	lGPEA240		:= oModel:GetId() == "GPEA240"
	lGPEM030		:= oModel:GetId() == "GPEM030"
EndIf

If lGPEA240
	cAusencia	:= STR0001 // "aus�ncia"
ElseIF lGPEA010
	cAusencia	:= STR0002 // "demiss�o"
	cMatAtendente	:= M->RA_MAT
	dDemissa	:= M->RA_DEMISSA
ElseIf lGPEA180
	cAusencia	:= STR0014 //transfer�ncia
	cMatAtendente	:= M->RA_MAT
	dDemissa	:= dDataBase
ElseIf lGPEM030 .OR. lGPEM060
	cAusencia	:= STR0015 //f�rias
	If lGPEM030
		cMatAtendente	:= oModel:GetValue("GPEM030_MSRH", "RH_MAT")
		dAusDataI 	:= oModel:GetValue("GPEM030_MSRH", "RH_DATAINI")
		dAusDataF	:= oModel:GetValue("GPEM030_MSRH", "RH_DATAFIM")
	Else
		cMatAtendente	:= SRH->RH_MAT
		dAusDataI	:= SRH->RH_DATAINI
		dAusDataF	:= SRH->RH_DATAFIM
	EndIf
ElseIf lGPEM040
	oMdlGPEM := FWModelActive()
	cAusencia	:= STR0002 // "demiss�o"
	cMatAtendente	:= oMdlGPEM:GetValue("GPEM040_MSRG", "RG_MAT")
	dDemissa	:= dDataDem1
EndIf


cMsg	+= STR0003 + cAusencia + STR0004 + CRLF + CRLF // "Verificamos que h� integra��o do SIGAGPE com o SIGATEC. A lista abaixo representa o conflito no per�odo de aloca��o do funcion�rio com a" ## "cadastrada. Segue(m) o(s) ponto(s) de conflito(s)."

If !lGPEA240
	cQuery 	:= "SELECT SRA.RA_NOME, AA1.AA1_CODTEC, ABB.ABB_DTINI, ABB.ABB_DTFIM, ABB.ABB_IDCFAL FROM " + RetSqlName("SRA") + " SRA "
	cQuery 	+= "INNER JOIN " + RetSqlName("AA1") + " AA1 "
	cQuery	+= "ON AA1.AA1_CDFUNC = SRA.RA_MAT AND "
	cQuery	+= "AA1.AA1_FILIAL = '" + xFilial("AA1") + "' "
	cQuery 	+= "INNER JOIN " + RetSqlName("ABB") + " ABB "
	cQuery	+= "ON AA1.AA1_CODTEC = ABB.ABB_CODTEC " + " AND "
	cQuery	+= "ABB.ABB_FILIAL = '" + xFilial("ABB") + "' "
	cQuery	+= "WHERE SRA.RA_MAT = '" + cMatAtendente + "'" + " AND "
	cQuery 	+= "SRA.RA_FILIAL = '" + xFilial("SRA") + "' "
	cQuery	+= "AND SRA.D_E_L_E_T_ = ' ' "
	cQuery	+= "AND AA1.D_E_L_E_T_ = ' ' "
	cQuery	+= "AND ABB.D_E_L_E_T_ = ' ' "

	cSql	:= "SELECT MIN(ABB.ABB_DTINI) DTMIN , MAX(ABB.ABB_DTFIM) DTMAX FROM " + RetSqlName("ABB") + " ABB "
	cSql	+= "INNER JOIN " + RetSqlName("AA1") + " AA1 "
	cSql	+= "ON AA1.AA1_CODTEC = ABB.ABB_CODTEC AND "
	cSql	+= "AA1.AA1_FILIAL = '" + xFilial("AA1") + "' "
	cSql	+= "INNER JOIN " + RetSqlName("SRA") + " SRA "
	cSql	+= "ON AA1.AA1_CDFUNC = SRA.RA_MAT AND "
	cSql	+= "SRA.RA_FILIAL = '" + xFilial("SRA") + "' "
	cSql	+= "WHERE SRA.RA_MAT = '" + cMatAtendente + "'" + " AND "
	cSql 	+= "ABB.ABB_FILIAL = '" + xFilial("ABB") + "' "
	cSql	+= "AND SRA.D_E_L_E_T_ = ' ' "
	cSql	+= "AND AA1.D_E_L_E_T_ = ' ' "
	cSql	+= "AND ABB.D_E_L_E_T_ = ' ' "
EndIf

If lGPEA240

	cMatAtendente 	:= oModel:GetValue("GPEA240_SRA", "RA_MAT")
	oModel	:=	oModel:GetModel("GPEA240_SR8")

	For nX := 1 To oModel:Length()

		oModel:GoLine(nX)

		If oModel:IsDeleted() .OR. !(oModel:IsInserted() .OR. oModel:IsUpdated())
			Loop
		EndIF

		dAusDataI	:= oModel:GetValue("R8_DATAINI")
		dAusDataF	:= oModel:GetValue("R8_DATAFIM")

		cQuery 	:= "SELECT SRA.RA_NOME, AA1.AA1_CODTEC, ABB.ABB_DTINI, ABB.ABB_DTFIM, ABB.ABB_IDCFAL FROM " + RetSqlName("SRA") + " SRA "
		cQuery 	+= "INNER JOIN " + RetSqlName("AA1") + " AA1 "
		cQuery	+= "ON AA1.AA1_CDFUNC = SRA.RA_MAT AND "
		cQuery	+= "AA1.AA1_FILIAL = '" + xFilial("AA1") + "' "
		cQuery 	+= "INNER JOIN " + RetSqlName("ABB") + " ABB "
		cQuery	+= "ON AA1.AA1_CODTEC = ABB.ABB_CODTEC " + " AND "
		cQuery	+= "ABB.ABB_FILIAL = '" + xFilial("ABB") + "' "
		cQuery	+= "WHERE SRA.RA_MAT = '" + cMatAtendente + "'" + " AND "
		cQuery	+= "SRA.RA_FILIAL = '" + xFilial("SRA") + "' "
		cQuery	+= "AND SRA.D_E_L_E_T_ = ' ' "
		cQuery	+= "AND AA1.D_E_L_E_T_ = ' ' "
		cQuery	+= "AND ABB.D_E_L_E_T_ = ' ' "
		cQuery	+= "AND (ABB.ABB_DTINI >= '" + DTos( dAusDataI ) + "' AND ABB.ABB_DTINI <= '" + Dtos( dAusDataF )+ "')"

		cQuery		:= ChangeQuery(cQuery)
		DbUseArea(.T., "TOPCONN",TcGenQry(,,cQuery), cAliasTemp, .T., .T.)


		If !(cAliasTemp)->(EOF())

			cQuery 	:= "SELECT MIN(ABB.ABB_DTINI) DTMIN , MAX(ABB.ABB_DTFIM) DTMAX FROM " + RetSqlName("ABB") + " ABB "
			cQuery 	+= "INNER JOIN " + RetSqlName("AA1") + " AA1 "
			cQuery	+= "ON AA1.AA1_CODTEC = ABB.ABB_CODTEC AND "
			cQuery	+= "AA1.AA1_FILIAL = '" + xFilial("AA1") + "' "
			cQuery	+= "INNER JOIN " + RetSqlName("SRA") + " SRA "
			cQuery	+= "ON AA1.AA1_CDFUNC = SRA.RA_MAT AND "
			cQuery	+= "SRA.RA_FILIAL = '" + xFilial("SRA") + "' "
			cQuery	+= "WHERE SRA.RA_MAT = '" + cMatAtendente + "'" + " AND "
			cQuery	+= "ABB.ABB_FILIAL = '" + xFilial("ABB") + "' "
			cQuery	+= "AND SRA.D_E_L_E_T_ = ' ' "
			cQuery	+= "AND AA1.D_E_L_E_T_ = ' ' "
			cQuery	+= "AND ABB.D_E_L_E_T_ = ' ' "
			cQuery	+= "AND (ABB.ABB_DTINI >= '" + DTos( dAusDataI ) + "' AND ABB.ABB_DTINI <= '" + Dtos( dAusDataF )+ "')"

			cQuery		:= ChangeQuery(cQuery)
			DbUseArea(.T., "TOPCONN",TcGenQry(,,cQuery), cAliasDT, .T., .T.)

			If !(cAliasDT)->(EOF())
				dABBDataF 	:= Stod((cAliasDT)->(DTMAX))
				dABBDataI	:= Stod((cAliasDT)->(DTMIN))
				cABBIdCFal	:= LEFT((cAliasTemp)->(ABB_IDCFAL), TamSx3("CN9_NUMERO")[1])
				cNomeAtendete	:= (cAliasTemp)->(RA_NOME)
				cCodTec			:= (cAliasTemp)->(AA1_CODTEC)
				lVer := .T.
			EndIf
			(cAliasDT)->(DbCloseArea())
		EndIf
		(cAliasTemp)->(DbCloseArea())
	Next nX
ElseIf lGPEA010 .OR. lGPEA180 .OR. lGPEM040

	cQuery	+= "AND (ABB.ABB_DTINI >= '" + DTos( dDemissa ) + "' AND ABB.ABB_DTINI >= '" + Dtos( dDemissa )+ "')"

	cQuery		:= ChangeQuery(cQuery)
	DbUseArea(.T., "TOPCONN",TcGenQry(,,cQuery), cAliasTemp, .T., .T.)

	If !(cAliasTemp)->(EOF())
		cSql		+= "AND (ABB.ABB_DTINI >= '" + DTos( dDemissa ) + "' AND ABB.ABB_DTFIM >= '" + Dtos( dDemissa )+ "')"
		cSql		:= ChangeQuery(cSql)
		DbUseArea(.T., "TOPCONN",TcGenQry(,,cSql), cAliasDT, .T., .T.)

		If !(cAliasDT)->(EOF())
			dABBDataF 	:= Stod((cAliasDT)->(DTMAX))
			dABBDataI	:= Stod((cAliasDT)->(DTMIN))
			cABBIdCFal	:= LEFT((cAliasTemp)->(ABB_IDCFAL), TamSx3("CN9_NUMERO")[1])
			cNomeAtendete	:= (cAliasTemp)->(RA_NOME)
			cCodTec			:= (cAliasTemp)->(AA1_CODTEC)
			lVer	:= .T.
		EndIf
		(cAliasDT)->(DbCloseArea())
	EndIF
ElseIf lGPEM030 .OR. lGPEM060
	cQuery	+= "AND (ABB.ABB_DTINI >= '" + DTos( dAusDataI ) + "' AND ABB.ABB_DTINI <= '" + Dtos( dAusDataF )+ "')"
	cQuery		:= ChangeQuery(cQuery)
	DbUseArea(.T., "TOPCONN",TcGenQry(,,cQuery), cAliasTemp, .T., .T.)

	If !(cAliasTemp)->(EOF())
		cSql	+= "AND (ABB.ABB_DTINI >= '" + DTos( dAusDataI ) + "' AND ABB.ABB_DTINI <= '" + Dtos( dAusDataF )+ "')"
		cSql		:= ChangeQuery(cSql)
		DbUseArea(.T., "TOPCONN",TcGenQry(,,cSql), cAliasDT, .T., .T.)

		If !(cAliasDT)->(EOF())
			dABBDataF 	:= Stod((cAliasDT)->(DTMAX))
			dABBDataI	:= Stod((cAliasDT)->(DTMIN))
			cABBIdCFal	:= LEFT((cAliasTemp)->(ABB_IDCFAL), TamSx3("CN9_NUMERO")[1])
			cNomeAtendete	:= (cAliasTemp)->(RA_NOME)
			cCodTec			:= (cAliasTemp)->(AA1_CODTEC)			
			lVer	:= .T.
		EndIf
		(cAliasDT)->(DbCloseArea())
	EndIF
EndIf
If !lGPEA240
	(cAliasTemp)->(DbCloseArea())
EndIf

If lVer
	If lATPECONFA
		cAliasDT	:= GetNextAlias()
		cSql	:= " SELECT ABB.ABB_DTINI, ABB.ABB_DTFIM, ABB.ABB_HRINI, ABB.ABB_HRFIM, ABB.ABB_CODIGO, ABB.ABB_CHEGOU, ABB.ABB_LOCAL, "
		cSql	+= " ABB.ABB_IDCFAL, ABB.R_E_C_N_O_"  
		cSql	+= " FROM " + RetSqlName("ABB") + " ABB "
		cSql	+= " WHERE ABB.ABB_FILIAL = '" + xFilial("ABB") + "' "
		cSql	+= " AND ABB.ABB_CODTEC = '" + cCodTec + "' "
		If EMPTY(dDemissa)
			cSql	+= " AND (ABB.ABB_DTINI >= '" + DTos( dAusDataI ) + "' AND ABB.ABB_DTINI <= '" + Dtos( dAusDataF )+ "') "
		Else	
			cSql	+= " AND (ABB.ABB_DTINI >= '" + DTos( dDemissa ) + "' AND ABB.ABB_DTFIM >= '" + Dtos( dDemissa )+ "') "
	EndIf
		cSql	+= " AND ABB.D_E_L_E_T_ = ' ' "
	
		cSql	:= ChangeQuery(cSql)
		DbUseArea(.T., "TOPCONN",TcGenQry(,,cSql), cAliasDT, .T., .T.)
		
		While (cAliasDT)->(!EOF())
			AADD(aAgenda, { (cAliasDT)->(ABB_DTINI),;	// Data Inicio
							(cAliasDT)->(ABB_DTFIM),;	// Data Fim
							(cAliasDT)->(ABB_HRINI),;	// Hora Inicio
							(cAliasDT)->(ABB_HRFIM),;	// Hora Fim
							(cAliasDT)->(ABB_CODIGO),;	// Codigo da ABB
							(cAliasDT)->(ABB_CHEGOU),;	// ABB_CHEGOU
							(cAliasDT)->(ABB_LOCAL),;	// Local
							(cAliasDT)->(ABB_IDCFAL),;	// IDCFAL
							(cAliasDT)->(R_E_C_N_O_)})	// RECNO
			(cAliasDT)->(DbSkip())		
		EndDo
		(cAliasDT)->(DbCloseArea())
		AADD(aPEConfa, {cMatAtendente,; // Matricula
						cNomeAtendete,; // Nome do Atendente
						cCodTec,; // Codigo do Atendente
						cABBIdCFal,; // Numero do Contrato
						dAusDataI,; // Inicio das Ausencias
						dAusDataF,; // Fim das Ausencias
						dABBDataI,; // Inicio da Agenda
						dABBDataF,; // Fim da Agenda
						cAusencia,; // Motivo da Ausencia
						dDemissa}) // Data da demiss�o
		ExecBlock('ATPECONFA', .F. , .F. , {aPEConfa, aAgenda} )
	Else
		cMsg += STR0005 + cNomeAtendete +  CRLF // "Atendente/Funcion�rio:"
		cMsg += STR0006 + cMatAtendente + CRLF // "Matr�cula: "
		cMsg += STR0007 + cCodTec + CRLF // "Codigo t�cnico: "
		cMsg += STR0008 + cABBIdCFal + CRLF + CRLF // "Contrato: "
		If EMPTY(dDemissa)
			cMsg += STR0009 + Dtoc(dAusDataI) + STR0011 + Dtoc(dAusDataF) + CRLF // "Tempo de aus�ncia: " ## " at� "
		Else
			cMsg += STR0013 + Dtoc(dDemissa) + CRLF  // "Data da demiss�o: "
		EndIf
		cMsg += STR0010 + DToc(dABBDataI) + STR0011 + DToc(dABBDataF) + CRLF + CRLF + CRLF// "Tempo de aloca��o: " ## " at� "
		AtShowLog(cMsg,STR0012,/*lVScroll*/,/*lHScroll*/,/*lWrdWrap*/,.F.) // "Atendente"
	EndIf
EndIf
	
cQuery	:= ""
cSql	:= ""
RestArea(aArea)
Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} TecBRevCTR

@description Modifica a estrutura do CNTA300 durante a revis�o do contrato
@author	Mateus Boiani
@since	01/07/2019
/*/
//------------------------------------------------------------------------------
Function TecBRevCTR(oModel,cTpRev,aCampos)

Cn300EspVld(oModel,cTpRev)
Cn300VdArr()
A300RevCtb(oModel)

//-- Par�metro que permite revis�o de cau��o
If SuperGetMV("MV_CNRVCAU",.F.,.F.)
	//-- Somente libera o percentual de cau��o caso a flag esteja como 1=SIM
	aAdd(aCampos,{'CN9MASTER',{'CN9_MINCAU'}})
	MtBCMod(oModel,aCampos,{||FwFldGet('CN9_FLGCAU') == '1'},'2')
	aCampos := {}
EndIf	
aAdd(aCampos,{'CN9MASTER',{'CN9_PROXRJ', 'CN9_DTASSI', 'CN9_ASSINA'}})
aAdd(aCampos,{'CNCDETAIL', {'CNC_CLIENT','CNC_LOJACL'}})
aAdd(aCampos,{'CNADETAIL',{'CNA_NUMERO','CNA_DTFIM', "CNA_CLIENT", "CNA_LOJACL"}})
aAdd(aCampos,{'CNBDETAIL',{'CNB_NUMERO','CNB_ITEM', 'CNB_TS','CNB_IDPED','CNB_CC'}})
aAdd(aCampos,{'CXIDETAIL',{'CXI_TIPO','CXI_CODCLI','CXI_LOJACL','CXI_NOMCLI','CXI_FILRES','CXI_DESFIL','CXI_PERRAT'}})
CNTA300BlMd(oModel:GetModel("CXIDETAIL"),.F.,.F.)
MtBCMod(oModel,aCampos,{||.T.},'2')

oModel:GetModel("CNCDETAIL"):SetNoInsertLine(.F.)
oModel:GetModel("CNCDETAIL"):SetNoUpdateLine(.F.)
oModel:GetModel("CNADETAIL"):SetNoUpdateLine(.F.)

Return
//------------------------------------------------------------------------------
/*/{Protheus.doc} TecBModFld

@description Modifica as propriedades dos campos do CNTA300 durante a revis�o
@author	Mateus Boiani
@since	01/07/2019
/*/
//------------------------------------------------------------------------------
Function TecBModFld(oStruCNB,oStruCNC,oStruCN9, oStruCNA)
	Default oStruCNA := nil
	oStruCNB:SetProperty('CNB_IDPED', MVC_VIEW_CANCHANGE, .T.)
	oStruCNB:SetProperty('CNB_CC', MVC_VIEW_CANCHANGE, .T.)
	oStruCNB:SetProperty('CNB_TS', MVC_VIEW_CANCHANGE, .T.)
	oStruCNC:SetProperty('*', MVC_VIEW_CANCHANGE, .T.)
	oStruCN9:SetProperty("CN9_TPCTO",	MVC_VIEW_CANCHANGE,.F.)
	oStruCN9:SetProperty("CN9_NUMERO",	MVC_VIEW_CANCHANGE,.F.)
	oStruCN9:SetProperty("CN9_DTINIC",	MVC_VIEW_CANCHANGE,.F.)
	oStruCN9:SetProperty("CN9_UNVIGE",	MVC_VIEW_CANCHANGE,.F.)
	oStruCN9:SetProperty("CN9_VIGE",	MVC_VIEW_CANCHANGE,.F.)
	oStruCN9:SetProperty("CN9_CONDPG",	MVC_VIEW_CANCHANGE,.F.)
	oStruCN9:SetProperty("CN9_INDICE",	MVC_VIEW_CANCHANGE,.F.)
	oStruCN9:SetProperty("CN9_FLGREJ",	MVC_VIEW_CANCHANGE,.F.)
	oStruCN9:SetProperty("CN9_FLGCAU",	MVC_VIEW_CANCHANGE,.F.)
	oStruCN9:SetProperty("CN9_PROXRJ",	MVC_VIEW_CANCHANGE,.T.)
	oStruCN9:SetProperty("CN9_DTASSI",	MVC_VIEW_CANCHANGE,.T.)
	oStruCN9:SetProperty("CN9_ASSINA",	MVC_VIEW_CANCHANGE,.T.)
	If oStruCN9:HasField("CN9_TIPREV")
		oStruCN9:SetProperty("CN9_TIPREV",	MVC_VIEW_CANCHANGE,.F.)
	EndIf
	If IsInCallStack("TECA870") .AND. VALTYPE(oStruCNA) == 'O'
		oStruCNA:SetProperty("CNA_CLIENT", MVC_VIEW_CANCHANGE,.T.)
		oStruCNA:SetProperty("CNA_LOJACL", MVC_VIEW_CANCHANGE,.T.)
	EndIf
Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} GSItEmpFil

@description Chamada ao cadastro de-para do EAI encapsulado para o SG
@author	fabiana.silva
@since 05/08/2019
@param	cCodEmp - Codigo da Empresa
@param cCodFil - Codigo da Filial
@param cMarca - Nome da Marca (Ex. RM)
@param lShowMsg - Exibe o Help da Mensagem
@param cMsg - Mensagem de Erro retornada
@return aEmps - Vetor contendo codigo da empresa e filial
/*/
//------------------------------------------------------------------------------
Function GSItEmpFil(cCodEmp, cCodFil, cMarca, lEnvia, lShowMsg, cMsg)
Local aEmps := {}

Default cCodEmp := cEmpAnt
Default cCodFil := cFilAnt
Default cMarca := IIF(SuperGetMV("MV_GSXINT",,"2") == "3", "RM", "")
Default lEnvia := .T.
Default lShowMsg := .T.
Default cMsg := ""

If !Empty(cMarca)
	aEmps := FWEAIEMPFIL( cCodEmp, cCodFil, cMarca, lEnvia)
	
	If Len(aEmps) = 0
		cMsg := STR0016 + cCodEmp+"/" +cCodFil + STR0017+ cMarca //"N�o localizado o cadastro de-para da Empresa/Filial " ## "para a marca " 
	EndIf
Else
	cMsg := STR0018 //"Informar a Marca para qual sera realizada a convers�o da Empresa/Filial"
	
EndIf

If lShowMsg .AND. !Empty(cMsg)
	Help(,, "GSItEmpFil",, cMsg,1, 0)
EndIf
Return aEmps


//------------------------------------------------------------------------------
/*/{Protheus.doc} GSItVeb

@description Chamada ao cadastro de-para do EAI encapsulado para o SG Verbas
@author	fabiana.silva
@since 05/08/2019
@param	cCodEmp - Codigo da Empresa
@param cCodFil - Codigo da Filial
@param cMarca - Nome da Marca (Ex. RM)
@param cCodEve - Codigo da Verba
@param lShowMsg - Exibe o Help da Mensagem
@param cMsg - Mensagem de Erro retornada
@return aEmps - Vetor contendo codigo da empresa e filial
/*/
//------------------------------------------------------------------------------
Function GSItVeb(cCodEmp, cCodFil, cMarca, cCodEve, lShowMsg, cMsg)
Local aArea := {}
Local aAreaSRV := {}
Local cValInt := ""
Local cValExt := ""

Default cCodEmp := cEmpAnt
Default cCodFil := cFilAnt
Default cMarca := IIF(SuperGetMV("MV_GSXINT",,"2") == "3", "RM", "")
Default cCodEve := ""
Default lShowMsg := .T.
Default cMsg := ""

If !Empty(cMarca) 
	aArea := GetArea()
	aAreaSRV := SRV->(GetArea())
	SRV := SRV->(DbSetOrder(1)) //RV_FILIAL + RV_COD
	If !Empty(cCodEve) .AND. SRV->(DbSeek(xFilial("SRV")+PadR(cCodEve, TamSX3("RV_COD")[1])))
		cValInt := GPEI040Snd( { cEmpAnt, xFilial("SRV"), SRV->RV_COD } )
		cValExt := CFGA070Ext( "RM", "SRV", "RV_COD", cValInt) 
		If Empty(cValExt)
			cMsg := STR0019 + cCodEve + STR0020 + cValInt + STR0021+ cMarca //"N�o localizado o cadastro de-para do cadastro da Verba  " ## " da chave Interna " ## "para a marca "
		Else
			cValExt := Substr(cValExt, RAT("|", cValExt)+1)
		EndIf
	Else
		cMsg := STR0022 //"Codigo da Verba em branco ou n�o localizada"
	EndIf
	
	RestArea(aAreaSRV)
	
	RestArea(aArea)
Else
	cMsg := STR0023 //"Informar a Marca para qual sera realizada a convers�o da Verba"
EndIf

If lShowMsg .AND. !Empty(cMsg)
	Help(,, "GSItVeb",, cMsg,1, 0)
EndIf
Return cValExt

//------------------------------------------------------------------------------
/*/{Protheus.doc} GSItCC

@description Chamada ao cadastro de-para do EAI encapsulado para o GS Centro de Custo
@author	fabiana.silva
@since 05/08/2019
@param	cCodEmp - Codigo da Empresa
@param cCodFil - Codigo da Filial
@param cMarca - Nome da Marca (Ex. RM)
@param cCodCC - Codigo do Centro de Custo
@param lShowMsg - Exibe o Help da Mensagem
@param cMsg - Mensagem de Erro retornada
@return aEmps - Vetor contendo codigo da empresa e filial
/*/
//---------------------------------------------------------------------------
Function GSItCC(cCodEmp, cCodFil, cMarca, cCodCC, lShowMsg, cMsg)
Local aRetCC := {}
Local cValExt := ""

Default cCodEmp := cEmpAnt
Default cCodFil := cFilAnt
Default cMarca := IIF(SuperGetMV("MV_GSXINT",,"2") == "3", "RM", "")
Default cCodCC := ""
Default lShowMsg := .T.
Default cMsg := ""

If !Empty(cMarca) 
//Busca codigo do centro de custo RM no de\para do EAI
	If !Empty(cCodCC)
         aRetCC := IntCusExt(, , cCodCC, )
         If aRetCC[1]
         	cValExt := CFGA070Ext(cMarca, "CTT", "CTT_CUSTO", aRetCC[2])
         	If Empty(cValExt)
         		cMsg := STR0024 + cCodCC + STR0020 +aRetCC[2] + STR0021+ cMarca //"N�o localizado o cadastro de-para do cadastro do Centro de Custo  " ## " da chave Interna " ## "para a marca "  
         	Else
         		cValExt := Substr(cValExt, RAT("|", cValExt)+1)
         	EndIf
         Else
         	cMsg := aRetCC[02] //Mensagem de Erro da Rotina
         EndIf
         
     Else
		cMsg := STR0025 //"Codigo do Centro de Custo em Branco"
	EndIf

Else
	cMsg := STR0026 //"Informar a Marca para qual sera realizada a convers�o do Centro de Custo"
	
EndIf

If lShowMsg .AND. !Empty(cMsg)
	Help(,, "GSItCC",, cMsg,1, 0)
EndIf
Return cValExt

//-----------------GSItRMWS------------------------------------------------------------
/*/{Protheus.doc} GSItEmpFil

@description Realiza o instanciamento do WebService RM
@author	fabiana.silva
@since 05/08/2019
@param cMarca - Nome da Marca (Ex. RM)
@param lShowMsg - Exibe o Help da Mensagem
@param cMsg - Mensagem de Erro retornada
@return oWS  -Objeto WebService
/*/
//------------------------------------------------------------------------------
Function GSItRMWS(cMarca, lShowMsg,cMsg, cFilMarca, cEmpMarca) //Intancia o objeto WebService, passando os dados de autentica��o
Local oWS := NIL
Local cURL:= ""
Local cUser := ""
Local cPsWrd := ""
Local aEmpFil := {}

Default cMarca := IIF(SuperGetMV("MV_GSXINT",,"2") == "3", "RM", "")
Default lShowMsg := .T.
Default cMsg := ""
Default cFilMarca := ""
Default cEmpMarca := ""
If cMarca == "RM" 

	aEmpFil := GSItEmpFil(, ,  cMarca, .T., lShowMsg, @cMsg)

	If Len(aEmpFil) >= 2
		cURL := SuperGetMV("MV_GSURLIN", .F., "" ) 
		cUser := SuperGetMV("MV_GSUSRIN", .F., "" ) 
		cPsWrd := SuperGetMV("MV_GSPWDIN", .F., "" ) 
		
		cFilMarca := aEmpFil[02]
		cEmpMarca := aEmpFil[01]
		
		oWS := WSwsDataServer():New()
		If Right(cURL, 1) <> "/"
			cUrl += "/"
		EndIf
		oWS:_URL := cURL +"wsDataServer/IwsDataServer"
	
		oWS:_HEADOUT  :=  {"Authorization: Basic "+Encode64(cUser+":"+cPsWrd) }
		oWS:cContexto := "CODSISTEMA=P;CODCOLIGADA=" +AllTrim(aEmpFil[01])+";CODUSUARIO="+cUser
	EndIf
EndIf
Return oWS

//-----------------------------------------------------------------------------
/*/{Protheus.doc} TECItGSEn

@description Tratamento para posto encerrado
@author	fabiana.silva
@since 05/08/2019
@param cContra - Codigo do Contrato
@param cRevisa - Codigo da Revis�o
@param cPlan - Codigo da Planilha
@param cItCNB - Codigo do item da CNB
@param lAuto - Indica se a chamada � automatica
@param cProdCTT - Codigo do produto

@return lEncerrado  -Indica se o posto est� encerrado
/*/
//------------------------------------------------------------------------------
Function TECItGSEn(cContra, cRevisa, cPlan, cItCNB, lAuto,  cProdCTT, oModelCNE)

Local lEncerrado := .F.
Local cAlias 	:= ""
Local aCTT 		:= {}
Local lTabPRC 	:= .F.
Local aProduto 	:= {}
Local cProduto 	:= ""
Local aArea 	:= {}
Local lAchouIt 	:= .F.
Local lPostoEnce := SuperGetMV("MV_GSPOSTO",.f.,.f.)

Default cContra := ""
Default cRevisa := ""
Default cPlan := ""
Default cItCNB := ""
Default lAuto := IsInCallStack("CN260Exc") 
Default lPostoEnce := .F.
Default cProdCTT := ""


//Primeiramente verifica se o contrato � de GS

If !Empty(cContra) .AND. !Empty(cPlan) .AND. !Empty(cItCNB) .AND. !Empty(cProdCTT) .AND. lAuto .AND. lPostoEnce 
	aArea := GetArea()
	aCtt := GetAdvFVal("TFJ", {"TFJ_CODIGO", "TFJ_GRPRH", "TFJ_CODTAB"}, xFilial("TFJ")+cContra+cRevisa,5, {"", "", ""})
	
	If Len(aCtt) >= 3 .AND. !Empty(aCtt[01]) .AND. Empty(aCtt[02])  ////� Contrato do GS Desagrupado?
		aProduto := GetAdvFVal("SB5", {"B5_TPISERV", "B5_GSMI", "B5_GSMC", "B5_GSLE"}, xFilial("SB5")+cProdCTT,1, {"", "", "", ""})
	 	lTabPRC := !Empty(aCtt[03])
	 	If Len(aProduto) >= 4
	 		If aProduto[01] == "4"	
	 			cProduto := "RH"
	 		EndIf
	 		If aProduto[02] == "1"
	 			cProduto += "/MI"
	 		EndIf
		 	If aProduto[03] == "1"
	 			cProduto += "/MC"
	 		EndIf	
		 	If aProduto[04] == "1"
	 			cProduto += "/LE"
	 		EndIf
	 	EndIf
	 	
	 	cAlias := GetNextAlias()
	 	
	 	If "RH" $ cProduto		
		//Primeiramente localiza Item de RH
			//Achou � est� encerrado
			//achou n�o esta ecerrado acabou
			BeginSql Alias cAlias
				SELECT TFF.TFF_ENCE AS ENCERRADO
				
				From %table:TFF% TFF
				INNER JOIN  %table:TFL% TFL	
				ON (TFL.TFL_CODIGO = TFF.TFF_CODPAI  AND	
					TFL.%notDel% AND  
					TFL.TFL_PLAN  = %exp:cPlan% AND
					TFL.TFL_CONREV =  %exp:cRevisa% AND
					TFL.TFL_CONTRT = %exp:cContra% AND 
					TFL.TFL_FILIAL  = %xfilial:TFL%  )
				INNER JOIN %table:TFJ% TFJ
					ON ( 					
				TFJ.TFJ_CODIGO = TFL.TFL_CODPAI AND
				TFJ.TFJ_STATUS = '1' AND
					TFJ.%notDel% AND
					TFJ.TFJ_FILIAL  = %xfilial:TFJ%   )
				WHERE 
					(TFF.TFF_ITCNB =  %exp:cItCNB% AND					
				TFF.%notDel% AND  
				TFF.TFF_CONTRT = %exp:cContra% AND 
				TFF.TFF_CONREV =  %exp:cRevisa% AND		
					TFF.TFF_FILIAL = %xfilial:TFF% ) 	
			EndSql
			lAchouIt := !(cAlias)->(Eof())
		EndIf
		
		If !lAchouIt .AND. "LE" $ cProduto
			//LOCALIZA LE
			If Select(cAlias) > 0
				(cAlias)->(DbCloseArea())
			EndIf
			BeginSql Alias cAlias
				SELECT TFI.TFI_ENCE AS ENCERRADO
				From %table:TFI% TFI
				INNER JOIN 	%table:TFL% TFL	 ON
				( 	TFL.TFL_CODIGO  = TFI.TFI_CODPAI AND
					TFL.%notDel% AND  
					TFL.TFL_PLAN  = %exp:cPlan% AND
					TFL.TFL_CONREV =  %exp:cRevisa% AND
					TFL.TFL_CONTRT = %exp:cContra% AND 
					TFL.TFL_FILIAL  = %xfilial:TFL%   )
				 INNER JOIN %table:TFJ% TFJ ON
					 (	TFJ.TFJ_CODIGO = TFL.TFL_CODPAI AND
				TFJ.TFJ_STATUS = '1' AND
					    TFJ.%notDel% AND 									    
					    TFJ.TFJ_FILIAL  = %xfilial:TFJ%  )					
				WHERE 
				(TFI.TFI_ITCNB =  %exp:cItCNB% AND
				TFI.%notDel% AND  
				TFI.TFI_CONREV =  %exp:cRevisa% AND		
				TFI.TFI_CONTRT = %exp:cContra% AND 	
				TFI.TFI_FILIAL = %xfilial:TFI% )   
			EndSql
			lAchouIt := !(cAlias)->(Eof())
		EndIf
		If !lAchouIt .AND. !lTabPRC .AND.  "MC" $ cProduto
			//LOCALIZA Mi/MC
			If Select(cAlias) > 0
				(cAlias)->(DbCloseArea())
			EndIf
			// Verifica se � Mi/MC  -  abaixo de rh
			BeginSql Alias cAlias
				SELECT TFF.TFF_ENCE AS ENCERRADO
				From 
				%table:TFH% TFH
				INNER JOIN %table:TFF% TFF ON
					( TFF.TFF_COD = TFH.TFH_CODPAI AND 	
				TFF.%notDel% AND  
				TFF.TFF_CONREV =  %exp:cRevisa% AND		
					TFF.TFF_CONTRT = %exp:cContra% AND 
					TFF.TFF_FILIAL = %xfilial:TFF% )				
				INNER JOIN %table:TFL% TFL ON
				(	TFL.TFL_CODIGO = TFF.TFF_CODPAI  AND
				TFL.%notDel% AND  
					TFL.TFL_PLAN  = %exp:cPlan% AND
				TFL.TFL_CONREV =  %exp:cRevisa% AND
					TFL.TFL_CONTRT = %exp:cContra% AND 
					TFL.TFL_FILIAL  = %xfilial:TFL%  )	
				INNER JOIN %table:TFJ% TFJ ON
				( TFJ.TFJ_CODIGO = TFL.TFL_CODPAI AND
					TFJ.TFJ_STATUS = '1' AND 
					TFJ.%notDel% AND 				
					TFJ.TFJ_FILIAL  = %xfilial:TFJ% )
				WHERE 
				(TFH.TFH_ITCNB =  %exp:cItCNB% AND
				TFH.%notDel% AND  
				TFH.TFH_CONREV =  %exp:cRevisa% AND	
				TFH.TFH_CONTRT = %exp:cContra% AND 
				TFH.TFH_FILIAL = %xfilial:TFH% ) 
			EndSql	
			lAchouIt := !(cAlias)->(Eof())			
		EndIf	
		If !lAchouIt .AND. !lTabPRC .AND.  "MI" $ cProduto
			If Select(cAlias) > 0
				(cAlias)->(DbCloseArea())
			EndIf
			BeginSql Alias cAlias
				SELECT TFF.TFF_ENCE AS ENCERRADO
				From 
				%table:TFG% TFG
				INNER JOIN %table:TFF% TFF ON
					( TFF.TFF_COD = TFG.TFG_CODPAI AND 	
				TFF.%notDel% AND  
				TFF.TFF_CONREV =  %exp:cRevisa% AND		
					TFF.TFF_CONTRT = %exp:cContra% AND 
					TFF.TFF_FILIAL = %xfilial:TFF% )				
				INNER JOIN %table:TFL% TFL ON
				(	TFL.TFL_CODIGO = TFF.TFF_CODPAI  AND
				TFL.%notDel% AND  
					TFL.TFL_PLAN  = %exp:cPlan% AND
				TFL.TFL_CONREV =  %exp:cRevisa% AND
					TFL.TFL_CONTRT = %exp:cContra% AND 
					TFL.TFL_FILIAL  = %xfilial:TFL%  )	
				INNER JOIN %table:TFJ% TFJ ON
				( TFJ.TFJ_CODIGO = TFL.TFL_CODPAI AND
					TFJ.TFJ_STATUS = '1' AND 
					TFJ.%notDel% AND 				
					TFJ.TFJ_FILIAL  = %xfilial:TFJ% )
				WHERE 
				(TFG.TFG_ITCNB =  %exp:cItCNB% AND
				TFG.%notDel% AND  
				TFG.TFG_CONREV =  %exp:cRevisa% AND	
				TFG.TFG_CONTRT = %exp:cContra% AND 
				TFG.TFG_FILIAL = %xfilial:TFG%  )
			EndSql	
			lAchouIt := !(cAlias)->(Eof())	
		EndIf		
		If lAchouIt
			lEncerrado := (cAlias)->ENCERRADO == "1"
		EndIf
		If Select(cAlias) > 0
			(cAlias)->(DbCloseArea())
		EndIf
	EndIf
	RestArea(aArea)
EndIf

If lEncerrado .AND. VALTYPE(oModelCNE) == 'O'
	oModelCNE:SetValue('CNE_QUANT', 0)
EndIf

Return lEncerrado

//-----------------------------------------------------------------------------
/*/{Protheus.doc} TecMdAtQry

@description Query para n�o apurar posto encerrado
@author	fabiana.silva
@since 05/08/2019
@param cQuery - Query

@return cQuery  -Retorna a query a ser utilizada
/*/
//------------------------------------------------------------------------------
Function TecMdAtQry(cQuery)
Local lPostoEnce := SuperGetMV("MV_GSPOSTO",.F.,.F.)
Local cTmp := ""
Local cWhere := ""
Local nPos := 0
Local cEnd := ""

If lPostoEnce 

	//Adi��o de filtro para n�o apurar local encerrado
	cWhere := " LEFT JOIN ( SELECT TFL_CONTRT, TFL_CONREV, TFL_PLAN, TFL_ENCE FROM " + RetSQLName("TFL") + " TFL, " +;
	 																			       RetSQLName("TFJ") + " TFJ "+;
	 					    " WHERE TFJ.TFJ_CODIGO = TFL.TFL_CODPAI AND TFJ.TFJ_STATUS = '1' AND TFJ.TFJ_FILIAL = '"+ xFilial("TFJ") + "' "+ ;
	 					     " AND TFL.D_E_L_E_T_ <> '*'  AND TFL.TFL_FILIAL = '"+ xFilial("TFL") + "' AND TFL.D_E_L_E_T_ <> '*' ) "+;
	           " TFL ON (  TFL_CONTRT = CNF_CONTRA AND TFL_CONREV = CNF_REVISA  AND TFL_PLAN = CNA_NUMERO  )  "
	
	//Localiza a ultima clausula da query
	nPos := RAt(" WHERE ", cQuery)
	
	cEnd := Substr(cQuery, nPos)
	//Adiciona o left join o filtro de local encerrado
	cEnd :=  cWhere + cEnd +  " AND (TFL_ENCE IS NULL OR TFL_ENCE  <> '1' ) "	
	
	cTmp := Left(cQuery, nPos-1)
	
	//Debug
	cTmp := cTmp + cEnd
	cQuery := cTmp

EndIf


Return cQuery

//------------------------------------------------------------------------------
/*/{Protheus.doc} TecMedPlan

@description Verifica na CNA se h� medi��es. Verificando se o Saldo � diferente
do valor total
@author	Augusto Albuquerque
@since	28/10/2019
/*/
//------------------------------------------------------------------------------
Function TecMedPlan( cContra, cRevisa, cLocal ) 
Local cAliasCNA	:= GetNextAlias()
Local lRet		:= .T.

BeginSql Alias cAliasCNA
	SELECT CNA.CNA_VLTOT, CNA.CNA_SALDO
	FROM %table:CNA% CNA
	INNER JOIN %Table:TFL% TFL
		ON TFL.TFL_FILIAL = %xFilial:TFL%
		AND TFL.TFL_CONTRT = CNA.CNA_CONTRA
		AND TFL.TFL_CONREV = CNA.CNA_REVISA
		AND TFL.TFL_LOCAL = %exp:cLocal%
	WHERE CNA.CNA_FILIAL = %xFilial:CNA%
		AND CNA.CNA_CONTRA = %exp:cContra%
		AND CNA.CNA_REVISA = %exp:cRevisa%
		AND CNA.CNA_NUMERO = TFL.TFL_PLAN
		AND CNA.%NotDel%
		AND TFL.%NotDel%
EndSql

If (cAliasCNA)->(!Eof()) .AND. (cAliasCNA)->CNA_VLTOT == (cAliasCNA)->CNA_SALDO
	lRet := .F.
EndIf

(cAliasCNA)->(DbCloseArea())
Return lRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TecPreCNB

@description Faz as pre valida��es da CNB na efetiva��o
@author	Augusto Albuquerque
@since	31/10/2019
/*/
//------------------------------------------------------------------------------
Function TecPreCNB( cAction, oModel, oModelGrid )
Local cGsDsGcn	:= SuperGetMv("MV_GSDSGCN",,"2")
Local lRet		:= .T.
Local lOrcPrc	:= SuperGetMv("MV_ORCPRC",,.F.)

If (!lOrcPrc .AND. cGsDsGcn == "1") .AND. !IsInCallStack("At870ItDsAgr") 
	If cAction == "UNDELETE"
		lRet := .F.
		Help( "", 1, "TecPreCNB", , STR0027, 1, 0,,,,,,{STR0028}) // "N�o � possivel realizar essa opera��o na efetiva��o." ## "Por favor, fa�a uma revis�o."
	ElseIf cAction == "DELETE"
		lRet := .F.
		Help( "", 1, "TecPreCNB", , STR0029, 1, 0,,,,,,{STR0028}) // "N�o � possivel excluir item na efetiva��o." ## "Por favor, fa�a uma revis�o."
	EndIf
EndIf

Return lRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TecPreCNA

@description Faz as pre valida��es da CNA na efetiva��o
@author	Augusto Albuquerque
@since	31/10/2019
/*/
//------------------------------------------------------------------------------
Function TecPreCNA ( cAction, oModel, oModelGrid )
Local cGsDsGcn	:= SuperGetMv("MV_GSDSGCN",,"2")
Local lRet		:= .T.
Local lOrcPrc	:= SuperGetMv("MV_ORCPRC",,.F.)
Local lAllowDel := TecDelPln()
If (!lOrcPrc .AND. cGsDsGcn == "1" ) .AND. !IsInCallStack("At870ItDsAgr") 
	If cAction == "UNDELETE"
		lRet := .F.
		Help( "", 1, "TecPreCNA", , STR0027, 1, 0,,,,,,{STR0028}) // "N�o � possivel realizar essa opera��o na efetiva��o." ## "Por favor, fa�a uma revis�o."
	ElseIf cAction == "DELETE" .AND. !lAllowDel
		lRet := .F.
		Help( "", 1, "TecPreCNA", , STR0029, 1, 0,,,,,,{STR0028}) // "N�o � possivel excluir item na efetiva��o." ## "Por favor, fa�a uma revis�o."
	EndIf
EndIf

Return lRet
//------------------------------------------------------------------------------
/*/{Protheus.doc} TecNumToHr

@description Converte um n�mero em hora (STRING) no formato 99:99 
@author	Mateus Boiani
@since	26/11/2019
/*/
//------------------------------------------------------------------------------
Function TecNumToHr(nHora)
Local cRet := ""
Local cAux

If VALTYPE(nHora) == 'N'
	cAux := cValToChar(nHora)

	If AT(".",cAux) == 3
		cRet += LEFT(cAux,2) + ":"
	ElseIf AT(".",cAux) == 2 .OR. (AT(".",cAux) == 0 .AND. nHora < 10)
		cRet += "0" + LEFT(cAux,1) + ":"
	Else
		cRet += LEFT(cAux,2) + ":"
	EndIf

	If "." $ cAux
		If "." $ RIGHT(cAux,2)
			If SUBSTR(cAux,3,1) == "." .OR. LEN(cAux) == 3
				cRet += RIGHT(cAux,1) + "0"
			Else
				cRet += "0" + RIGHT(cAux,1)
			EndIf
		Else
			cRet += RIGHT(cAux,2)
		EndIf
	Else
		cRet += "00"
	EndIf
EndIF
Return cRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TecCheckIn()
Fun��o para realizar a batida atrav�s do ClockIn

@param cCracha - Caracter - codigo do cracha do funcionario
@param cFilial - Caracter - codigo da filial do funcionario
@param dData - Data - Data da batida
@param nHoras - Numerico - Hora da batida

@author 	Luiz Gabriel
@since		04/12/2019
@version 	P12
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TecCheckIn(cCracha,cRAFilial,dData,nHora,cTurno,cMat, cTipo, cSeq, dDataApo)
Local cTemp
Local cSeekTDV
Local aAreaSRA := {}
Local aArea := GetArea()
Local lTECA910	:= isInCallStack("TECA910")
Local lJaProc := .F.
Local lMtFil := SuperGetMV("MV_GSMSFIL",,.F.)
Local cDataApo
Local nCount := 1
Default cCracha := ""
Default cRAFilial := ""
Default cMat := ""
Default cTurno := ""
Default cTipo := ""
Default cSeq := ""
Default dDataApo := CTOD("")

If !lTECA910 .AND. !Empty(cRAFilial)
	aAreaSRA	:= SRA->(GetArea())
	
	If !Empty(cCracha)
		DbSelectArea("SRA")
		SRA->(DbSetOrder(9))
		If SRA->(MsSeek(cCracha+cRAFilial))
			cMat := SRA->RA_MAT
		EndIf
	ElseIf Empty(cTurno)
		cMat := ""
	EndIf
	
	If !EMPTY(cMat) .AND. !Empty(cTurno) .AND. !Empty(cTipo) .AND. !Empty(cSeq) .AND. !Empty(dDataApo)
		If lMtFil
			cExpr := FWJoinFilial("AA1" , "TDV" , "AA1", "TDV", .T.)
		Else
			cExpr := "TDV.TDV_FILIAL = '" + xFilial("TDV") + "'"
		EndIf
		cExpr += " AND ABB.ABB_ATIVO = '1' AND (ABB.ABB_CHEGOU <> 'S' OR ABB.ABB_SAIU <> 'S') AND ABB.ABB_ATENDE = '2'"
		cExpr := "%"+cExpr+"%"
		cDataApo := DTOS(dDataApo)
		cSeekTDV := GetNextAlias()
		BeginSQL Alias cSeekTDV
			SELECT TDV.TDV_CODABB
			FROM %Table:TDV% TDV
			INNER JOIN %Table:ABB% ABB ON 
				ABB.ABB_FILIAL = TDV.TDV_FILIAL AND
				ABB.ABB_CODIGO = TDV.TDV_CODABB AND 
				ABB.%NotDel%
			INNER JOIN %Table:AA1% AA1 ON
				AA1.AA1_CODTEC = ABB.ABB_CODTEC AND
				AA1.AA1_CDFUNC = %Exp:cMat% AND
				AA1.AA1_FUNFIL = %Exp:cRAFilial% AND
				AA1.AA1_FILIAL = %Exp:xFilial("AA1")% AND
				AA1.%NotDel%
			WHERE
				TDV.TDV_DTREF = %Exp:cDataApo% AND
				TDV.TDV_TURNO = %Exp:cTurno% AND
				TDV.TDV_SEQTRN = %Exp:cSeq% AND
				TDV.%NotDel% AND
				%exp:cExpr%
		EndSql
		While !(cSeekTDV)->(Eof())
			If cValToChar(nCount) == LEFT(cTipo,1)
				TecAtuABB((cSeekTDV)->TDV_CODABB,IIF(RIGHT(cTipo,1) == 'E','1','2'),TxValToHor(nHora),dData)
				lJaProc := .T.
				Exit
			EndIf
			(cSeekTDV)->(DbSkip())
			nCount++
		End
		(cSeekTDV)->(DbCloseArea())
	EndIf

	If !EMPTY(cMat) .AND. !lJaProc
		cTemp := GetNextAlias()
		BeginSQL Alias cTemp
		SELECT ABB.ABB_CODIGO,
			CASE
				WHEN ABB_CHEGOU <> 'S'  THEN ABB_HRINI
				WHEN ABB_CHEGOU = 'S'
						AND ABB_SAIU = '' THEN ABB_HRFIM
			END ABBHR,
			CASE
				WHEN ABB_CHEGOU <> 'S' THEN '1'
				WHEN ABB_CHEGOU = 'S' AND ABB_SAIU = '' THEN '2'
			END INOUT
		FROM %Table:AA1% AA1
			INNER JOIN %Table:ABB% ABB ON ABB.ABB_FILIAL = %Exp:xFilial("ABB")% AND
			ABB.ABB_CODTEC = AA1.AA1_CODTEC AND 
			AA1.D_E_L_E_T_= ' '
		WHERE AA1.AA1_FUNFIL = %Exp:cRAFilial% AND 
			AA1.AA1_CDFUNC = %Exp:cMat% AND 
			ABB.ABB_FILIAL = %Exp:xFilial("ABB")% AND
			ABB.ABB_CODTEC = AA1.AA1_CODTEC AND 
			( ABB.ABB_DTINI = %Exp:dData%  OR 
			  (ABB.ABB_DTFIM = %Exp:dData% AND ABB.ABB_HRINI > ABB.ABB_HRFIM) )  AND 
			ABB.ABB_ATIVO = 1 AND 
			ABB.%NotDel%  AND 
			AA1.%NotDel%  AND 
			(ABB.ABB_CHEGOU <> 'S' OR ABB.ABB_SAIU <> 'S') AND
			ABB_DTINI||ABB_HRINI =
				(SELECT MIN(ABBSUB.ABB_DTINI||ABBSUB.ABB_HRINI)
					FROM %Table:AA1% AA1SUB
						INNER JOIN %Table:ABB% ABBSUB ON ABBSUB.ABB_FILIAL = %Exp:xFilial("ABB")% 
					AND ABBSUB.ABB_CODTEC = AA1SUB.AA1_CODTEC AND
						AA1SUB.%NotDel% 
					WHERE AA1SUB.AA1_FUNFIL = %Exp:cRAFilial%  AND
						AA1SUB.AA1_CDFUNC = %Exp:cMat% AND 
						ABBSUB.ABB_FILIAL = %Exp:xFilial('ABB')%  AND 
						ABBSUB.ABB_CODTEC = AA1SUB.AA1_CODTEC AND 
						( ABBSUB.ABB_DTINI = %Exp:dData% OR 
						  (ABBSUB.ABB_DTFIM = %Exp:dData% AND ABBSUB.ABB_HRINI  > ABBSUB.ABB_HRFIM) ) AND
						ABBSUB.ABB_ATIVO = 1 AND
						ABBSUB.%NotDel% AND
						AA1SUB.%NotDel% AND
						(ABBSUB.ABB_CHEGOU <> 'S'
							OR ABBSUB.ABB_SAIU <> 'S')) 
					ORDER BY  ABB.ABB_DTINI
		EndSql
			
		If ( cTemp )->( !Eof() )
			TecAtuABB((cTemp)->ABB_CODIGO,(cTemp)->INOUT,TxValToHor(nHora),dData)
		EndIf
		(cTemp)->(DbCloseArea())
	EndIf 

	RestArea(aAreaSRA)
EndIf
RestArea(aArea)
Return
	

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TecAtuABB()
Fun��o para atualizar as batidas da agenda do atendente
	
@param cCodigoABB - Caracter - codigo da agenda do atendente
@param cInOut - Caracter - codigo para informar se a batida � de entrada ou saida
@param cHora - Caracter - horario que ser� realizado a batida
	
@author 	Luiz Gabriel
@since		04/12/2019
@version 	P12
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TecAtuABB(cCodigoABB,cInOut,cHora,dData)
Local aArea		:= GetArea()
Local lRet 		:= .T.
Local lClockIN	:= 	TxExistCLO()
Local lDtInOut	:= ABB->(ColumnPos('ABB_DTCHIN')) > 0 .And. ABB->(ColumnPos('ABB_DTCHOU')) > 0
dbSelectArea('ABB')
		
If !Empty(cCodigoABB)
	dbSelectArea("ABB")
	ABB->(dbSetOrder(8))
	If ABB->(DbSeek(XFilial("ABB")+ cCodigoABB))
		RecLock("ABB",.F.)
					
		If cInOut == '1'
			ABB->ABB_CHEGOU := 'S'
			If lClockIN
				ABB->ABB_CLOIN := '1'
			EndIf
			If lDtInOut
				ABB->ABB_DTCHIN := dData
			EndIf
			ABB->ABB_HRCHIN := cHora			
			ABB->(MsUnlock())
					
		ElseIf cInOut == '2'
			ABB->ABB_SAIU := 'S'
			ABB->ABB_ATENDE := '1'
			If lClockIN
				ABB->ABB_CLOOUT := '1'
			EndIf
			If lDtInOut
				ABB->ABB_DTCHOU := dData
			EndIf
			ABB->ABB_HRCOUT := cHora		
			ABB->(MsUnlock())	
		EndIf   
	Else
		lRet := .F.	
	EndIf
Else
	lRet := .F.			
EndIf
	
RestArea(aArea)
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TxExistCLO()
Fun��o para verificar se ps campos utilizados na integra��o est�o criados no dicionario
	
@author 	Luiz Gabriel
@since		04/12/2019
@version 	P12
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TxExistCLO()
Local lClockIN	:= 	.F.

DbSelectArea("ABB")
If  ABB->(ColumnPos('ABB_CLOIN')) > 0 .And. ABB->(ColumnPos('ABB_CLOOUT')) > 0
	lClockIN := .T.
EndIf

Return lClockIN	


//------------------------------------------------------------------------------
/*/{Protheus.doc} TecGrd2CSV

@description Fun��o para Exportar o arquivo .CSV

@param cNomeArq 	- Nome do Arquivo
@param cId			- Id do Model
@param cIdView		- Id da view para verificar quais campos ser�o exportados
@param aNoCpos		- Array com os campos que ser�o ignorados na view na exporta��o
@param aIncCpo		- Array com os campos, ID da View e ID do model que ser�o incluidos na exporta��o
@param aLegenda		- Array com a regra de legenda caso a grid possua
@param aNoCpoU		- Array com os campos que ser�o ignorados na view na exporta��o do submodelo
@param aIncCpoS		- Array com os campos, ID da View e ID do model que ser�o incluidos na exporta��o do submodelo
@param aLegendaS	- Array com a regra de legenda caso a grid possua, para o submodelo

@author	diego.bezerra
@since	12/12/2019
/*/
//------------------------------------------------------------------------------
Function TecGrd2CSV(cNomeArq,cId,cIdView,aNoCpos,aIncCpo,aLegenda, cMdlID, cFldVld, cIdSub, cIdVwS, aNoCpoU, aIncCpoS, aLegendaS)
Local oMdAll	:= FwModelActive()
Local oView		:= FwViewActive()
Local oMdlId	:= Nil
Local lRet 		:= .F.
Local cPasta	:= ""
Local cNomArq	:= "" //Pega por referencia nome que foi gravado o arquivo
Local aCpoView	:= {} //Campos da view do model prim�rio
Local aCpoVwS	:= {} //Campos da view do model secund�rio
Local oMdlSub	:= Nil

Default aNoCpos 	:= {} //Array com os campos a serem ignorados
Default aIncCpo		:= {} //Array com os campos e valores a serem adicionados
Default aLegenda 	:= {} //Array com as legendas
Default aLegendaS	:= {} //Array com as legendas do model secund�rio
Default cFldVld		:= ""

//Verifica se o Modelo a ser exportado est� valido
If ValType(oMdAll) == "O" .AND. oMdAll:IsActive() .And. oMdAll:GetId() == cMdlID
	
	oMdlId	:= oMdAll:GetModel(cId)
	If ValType(oMdlId) == "O" .And. !oMdlId:IsEmpty()

		//Seleciona a pasta para grava��o do arquivo .csv
		cPasta := TecSelPast()

		If !Empty(cPasta)
			If !Empty(cIdSub)
				oMdlSub := oMdAll:GetModel(cIdSub)
				aCpoVwS := oView:GetViewStruct(cIdVwS):aFields
			EndIf

			//Retorna os campos da View para a grid a ser exportada
			aCpoView := oView:GetViewStruct(cIdView):aFields

			//Imprime o .CSV
			FwMsgRun(Nil,{|| lRet := procCSV(cPasta,cNomeArq,oMdAll,oMdlId,oView,aNoCpos,aIncCpo,aCpoView,aLegenda, @cNomArq, cMdlID, cFldVld,oMdlSub, aNoCpoU, aIncCpoS, aCpoVwS, aLegendaS)}, Nil,  "Aguarde....") 
			
			//Sucesso na Impress�o
			If lRet
				MsgAlert("Arquivo criado com sucesso na pasta! " + CRLF + cPasta + cNomArq) 
			EndIf
			
		EndIf
	EndIf
EndIf

Return lRet
//------------------------------------------------------------------------------
/*/{Protheus.doc} procCSV

@description Fun��o onde � feito a adi��o dos campos e valores a serem exportados

@param cPasta - Pasta onde o arquivo ser� salvo
@param cArquivo	- Prefixo final para o nome do arquivo
@param oMdAll	- Modelo da dados Geral
@param oMdlId	- Modelo de dados a ser exportado
@param oView	- View a ser exportada
@param aNoCpos	- Array com os campos a serem ignorados na exporta��o
@param aIncCpo	- Array com os campos a serem incluidos na exporta��o
@param aCpoView	- Array com os campos da view que ser�o exportados
@param aLegenda	- Array com a regra de legendas
@param cNomeArq - Prefixo inicial para o nome do arquivo
@param cMdlID	- Id do modelo a ser exportado
@param oMdlSub	- Sub-modelo a ser exportado
@param aNoCpos	- Array com os campos a serem ignorados na exporta��o para o sub-modelo
@param aIncCpo	- Array com os campos a serem incluidos na exporta��o para o sub-modelo
@param aCpoView	- Array com os campos da view que ser�o exportados para o sub-modelo
@param aLegenda	- Array com a regra de legendas do sub-modelo

@author	diego.bezerra
@since	12/12/2019
/*/
//------------------------------------------------------------------------------
Static Function procCSV(cPasta,cArquivo,oMdAll,oMdlId,oView,aNoCpos,aIncCpo,aCpoView,aLegenda, cNomArq, cMdlID, cFldVld, oMdlSub, aNoCpoU, aIncCpoS, aCpoVwS, aLegendaS)

Local lRet 		:= .F.
Local aCampos	:= {}
Local cCab		:= ""
Local cItens	:= ""
Local cCsv		:= ""
Local nHandle	:= 0
Local cCabSub	:= ""
Local aCamposS	:= {}

Default oMdlSub	:= Nil	//Model secund�rio
Default aNoCpoU	:= {}	//Campos ignorados do model secund�rio
Default aCpoVwS	:= {}	//Campos da view, do model secund�rio
Default aLegendaS := {}	//Legendas do model secundario

Default cFldVld	:= ""

cNomArq	:= DtoS(dDataBase) + '_' + StrTran(Time(), ':', '') + '_' + cArquivo

//Verifica quais campos ser�o impressos
aCampos	:= TecRtCpo(aNoCpos,aIncCpo,aCpoView,oView)
// Valida se a chamada possui model secund�rio para exportar
If Valtype(oMdlSub) == 'O'
	aCamposS := TecRtCpo(aNoCpoU,aIncCpo,aCpoVwS,oView)
	cCabSub	 := TecImpCab(aCamposS) 
EndIf
//Imprime o cabe�alho do CSV
cCab := TecImpCab(aCampos) // cabe�alho principal
//Imprime os itens do CSV
cItens	:= ImpIt(oMdAll,oMdlId,aCampos,aLegenda,cMdlID, cFldVld, cCabSub, oMdlSub,aLegendaS, aCamposS,cCab)
//Realiza a uni�o do cabe�alho com os Itens
cCsv := cItens
//Cria o arquivo .CSV na pasta selecionada
nHandle := fCreate(cPasta+""+cNomArq+".CSV")

If nHandle > 0
	FWrite(nHandle, cCsv)
	FClose(nHandle)
	lRet := .T.
EndIf

Return lRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TecRtCpo

@description Fun��o onde � feito a adi��o dos campos a serem exportados

@param aNoCpos	- Array com os campos a serem ignorados na exporta��o
@param aIncCpo	- Array com os campos a serem incluidos na exporta��o
@param aCpoView	- Array com os campos da view que ser�o exportados
@param aLegenda	- Array com a regra de legenda
@param oView	- View a ser exportada

@author	diego.bezerra
@since	12/12/2019
/*/
//------------------------------------------------------------------------------
Static Function TecRtCpo(aNoCpos,aIncCpo,aCpoView,oView)
Local aCampos	:= {}
Local lCopy		:= .T.
Local nX		:= 0
Local nY		:= 0
Local lNoCpo	:= Len(aNoCpos) > 0  //Verifica se existem campos a serem retirados
Local lIncCpo	:= Len(aIncCpo) > 0  //Verifica se existem campos a serem incluidos
Local aCpoVw	:= {}				 //Array com os campos exibidos na view

//Verifica se tem campos a adicionar no array
If lIncCpo
	For nX	:= 1 To Len(aIncCpo)
		aCpoVw := oView:GetViewStruct(aIncCpo[nX,1]):aFields
		For nY := 1 to Len(aIncCpo[nX,3])
			nPos := aScan( aCpoVw,{ |a| a[ MVC_VIEW_IDFIELD ] == aIncCpo[nX,3,nY] })
			Aadd(aCampos,{aCpoVw[ nPos, MVC_VIEW_IDFIELD ],aCpoVw[ nPos, MVC_VIEW_TITULO ],aCpoVw[ nPos, 6 ],aIncCpo[nX,2]})
		Next nY
		//Limpa a Variavel
		aCpoVw := {}
	Next nX
EndIf

For nX	:= 1 To Len(aCpoView)
	If lNoCpo
		lCopy := aScan( aNoCpos, aCpoView[ nx, MVC_VIEW_IDFIELD ] ) == 0
	EndIf
	If lCopy
		Aadd(aCampos,{aCpoView[ nx, MVC_VIEW_IDFIELD ],aCpoView[ nx, MVC_VIEW_TITULO ],aCpoView[ nx, 6 ]})
	EndIf
Next nX

Return aCampos

//------------------------------------------------------------------------------
/*/{Protheus.doc} ImpIt

@description Fun��o para imprimir os itens do .CSV

@param oMdAll	- Modelo da dados Geral
@param oMdlId	- Modelo de dados a ser exportada
@param aCampos	- Array com os campos a serem exportados
@param aLegenda - Array com a rega de legenda para exporta��o
@param cFldVld  - Campo utilizado como par�metro para percorrer o modelo
@author	diego.bezerra
@since	12/12/2019
/*/
//------------------------------------------------------------------------------
Static Function ImpIt(oMdlAll,oMdlId,aCampos,aLegenda, cMdlID, cFldVld, cCabSub, oMdlSub,aLegendaS, aCamposS,cCab)
Local cRet		:= ""
Local nY		:= 0
Local nLen		:= oMdlId:Length()
Local nPosBkp	:= oMdlId:GetLine()
Local nLegen	:= 0
Local nLenSub	:= 0
Local lMdlSub 	:= !Empty(cCabSub) .AND. ValType(oMdlSub) == 'O'
Local nK		:= 0

Default cMdlID		:= ""
Default aLegenda	:= {}
Default aLegendS	:= {}
Default aCamposS	:= {}
Default cFldVld		:= ""
Default cCabSub		:= ""

nLegen := Len(aLegenda)
nLegenS := Len(aLegendaS)

If !lMdlSub
	cRet := cRet + cCab 
ENDIF

For nY := 1 To nLen
	oMdlId:GoLine(nY)
	
	If !Empty(cFldVld)
		If Empty(TecGVal(oMdlId:GetID(), cFldVld))
			Loop
		EndIf
	EndIf
	
	If oMdlId:IsDeleted()
		Loop
	EndIf

	If lMdlSub
		cRet := cRet + cCab 
	EndIf
		
	getVlCsv(@cRet, aCampos, oMdlId, oMdlAll, aLegenda, nLegen)
	
	If lMdlSub
		cRet := cRet + CRLF
	ENDIF

	If lMdlSub
		cRet := cRet + CRLF
		cRet := cRet + cCabSub
		nLenSub	:= oMdlSub:Length()
		For nK	:= 1 To nLenSub
			oMdlSub:GoLine(nK)
			getVlCsv(@cRet, aCamposS, oMdlSub, oMdlAll, aLegendaS, nLegenS)
			cRet := cRet + CRLF
		Next nK	
		cRet := cRet + CRLF
	EndIf

	If !lMdlSub
		cRet := cRet + CRLF
	EndIf
Next nY

//Volta para a linha atual
oMdlId:GoLine(nPosBkp)

Return cRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} getVlCsv

@description responsavel por gerar uma linha em formato csv

@param cRet		- String passada por refer�ncia, utilizada para retornar o conte�do da linha
@param aCampos	- Array com os campos do modelo de dados
@param oMdlId	- Modelo de dados que ser� exportado
@param oMdlAll 	- Modelo de dados principal da rotina
@param aLegenda - Array com as caracteristicas das legendas

@author	diego.bezerra
@since	28/05/2020
/*/
//------------------------------------------------------------------------------
Static Function getVlCsv(cRet, aCampos, oMdlId, oMdlAll, aLegenda, nLegen)

Local nPosLeg 	:= 0
Local nPos		:= 0
Local nX		:= 0

For nX	:= 1 To Len(aCampos)
	If Len(aCampos[nX]) < 4			
		If aCampos[ nx, 3] == "D"
			cRet += DtoC(oMdlId:GetValue(aCampos[ nx, 1])) +";"
		ElseIf aCampos[ nx, 3] == "BT"
			If nLegen > 0
				nPosLeg := aScan(aLegenda, { |a| a[1] == aCampos[ nx, 1] })
				If nPosLeg > 0
					nPos := aScan(aLegenda[nPosleg][2], {|b| b[1] == oMdlId:GetValue(aCampos[ nx, 1]) })
					If nPos > 0
						cRet += aLegenda[nPosLeg][2][nPos][2] +";"
					Else
						cRet += oMdlId:GetValue(aCampos[ nx, 1]) +";"
					EndIf
				Else
					cRet += oMdlId:GetValue(aCampos[ nx, 1]) +";"
				EndIf
			Else
				cRet += oMdlId:GetValue(aCampos[ nx, 1]) +";"
			EndIf
		ElseIf aCampos[ nx, 3] == "N"
			cRet += cValToChar(oMdlId:GetValue(aCampos[ nx, 1])) +";"
		ElseIf aCampos[ nx, 3] != "CHECK"
			cRet += oMdlId:GetValue(aCampos[ nx, 1]) +";"
		EndIf
	Else
		If aCampos[ nx, 3] == "D"
			cRet += DtoC(oMdlAll:GetModel(aCampos[nx,4]):GetValue(aCampos[ nx, 1])) +";"
		ElseIf aCampos[ nx, 3] == "BT"
			If nLegen > 0
				npos := aScan( aLegenda,{ |a| a[1] == oMdlAll:GetModel(aCampos[nx,4]):GetValue(aCampos[ nx, 1]) })
				If nPos > 0
					cRet += aLegenda[nPos][2] + ";"
				Else
					cRet += oMdlId:GetValue(aCampos[nx, 1]) + ";"
				EndIf
			Else
				cRet += oMdlAll:GetModel(aCampos[nx,4]):GetValue(aCampos[ nx, 1]) +";"
			EndIf
		ElseIf aCampos[ nx, 3] == "N"
			cRet += cValToChar(oMdlId:GetValue(aCampos[ nx, 1])) +";"
		Else
			cRet += oMdlAll:GetModel(aCampos[nX,4]):GetValue(aCampos[ nx, 1]) +";"
		EndIf
	EndIf
Next nX

return .T.
//------------------------------------------------------------------------------
/*/{Protheus.doc} TecImpCab

@description Fun��o para imprimir o cabe�alho do arquivo .CSV
@param aCampos - Campos que ser�o exportados para o CSV

@author	diego.bezerra
@since	12/12/2019
/*/
//------------------------------------------------------------------------------
Function TecImpCab(aCampos)
Local cRet		:= ""
Local nX		:= 0

For nX	:= 1 To Len(aCampos)
	cRet += aCampos[ nx, 2 ] +";"
Next nX

//Pula linha
cRet := cRet + CRLF

Return cRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TecGVal

@description Executa um GetValue caso o FwFldGet n�o consiga retornar o valor do campo

@author	boiani
@since	06/07/2019
/*/
//------------------------------------------------------------------------------
Function TecGVal(cForm, cField)

Local xValue := FwFldGet(cField)
Local oModel := FwModelActive()
Local oSubModel

If EMPTY(xValue) .AND. VALTYPE(oModel) == "O"
	oSubModel := oModel:GetModel(cForm)
	If VALTYPE(oSubModel) == "O"
		xValue := oSubModel:GetValue(cField)
	EndIf
EndIf

Return xValue

//------------------------------------------------------------------------------
/*/{Protheus.doc} dateToStr

@description Recebe data no formato Data e retorna string em format dd/mm/yyyy
@param dDate, Data
@return cDate, string, data em formato dd/mm/yyyy
@author	diego.bezerra
@since	13/12/2019
/*/
//------------------------------------------------------------------------------
Function TecDToStr(dDate)

Local cDate := ""

Default dDate := ""
If ValType(dDate)=='D'
	cDate := iif(Len(cValToChar(DAY(dDate)))== 1,'0'+cValToChar(DAY(dDate)),cValToChar(DAY(dDate))) 
	cDate += '/' + iif(Len(cValToChar(MONTH(dDate)))== 1,'0'+cValToChar(MONTH(dDate)),cValToChar(MONTH(dDate))) 
	cDate += '/' + cValToChar(YEAR(dDate))
Else
	cDate := ' / / '
EndIf

Retur cDate

//------------------------------------------------------------------------------
/*/{Protheus.doc} TecSelPast
Abre Tela para sele��o do local de grava��o do Arquivo CSV

@author		Luiz Gabriel
@since		03/07/2019
@version	P12.1.23
/*/
//------------------------------------------------------------------------------
Function TecSelPast()
Local cPathDest		:= ""
Local cExtension	:= '*.CSV'
Local lContinua		:= .T.

cPathDest  := Alltrim(cGetFile(STR0030 + cExtension + '|' + cExtension +'|' , STR0031, 1, '', .T., GETF_LOCALHARD+GETF_RETDIRECTORY,.F.))	//"Arquivo"	# "Selecione a pasta para grava��o"
If Empty(cPathDest)
	cPathDest := ""
Else
	lContinua := ChkPerGrv(cPathDest)
	If !lContinua
		Aviso(STR0032, STR0033, {STR0034}, 2)	//"Aten��o"	# "Voc� n�o possu� permiss�o de grava��o para pasta selecionada. Tente Selecionar outra pasta. # "Ok"
		cPathDest := ""
	EndIf
EndIf

Return cPathDest

//------------------------------------------------------------------------------
/*/{Protheus.doc} ChkPerGrv
Checa permissao de gravacao na pasta indicada para geracao
do CSV

@author		Luiz Gabriel
@since		03/07/2019
@version	P12.1.23
/*/
//------------------------------------------------------------------------------
Static Function ChkPerGrv(cPath)
Local cFileTmp := CriaTrab(NIL, .F.)
Local nHdlTmp  := 0
Local lRet     := .F.

cPath   := AllTrim(cPath)
nHdlTmp := MSFCreate(cPath + If(Right(cPath, 1) <> '\', '\', '') + cFileTmp + '.TMP', 0)
If nHdlTmp <= 0
	lRet := .F.
Else
	lRet := .T.
	FClose(nHdlTmp)
	FErase(cPath + If(Right(cPath, 1) <> '\', '\', '') + cFileTmp + '.TMP')
EndIf

Return(lRet)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TecChngCNA

@description Trava as linhas na efetiva��o do contrato.
@author	Augusto Albuquerque
@since	29/01/2020
/*/
//------------------------------------------------------------------------------
Function TecChngCNA( oModel )
If AT870GetTr()
	oModel:GetModel("CNADETAIL"):SetNoDeleteLine(.T.)
	oModel:GetModel("CNADETAIL"):SetNoInsertLine(.T.)
	oModel:GetModel("CNADETAIL"):SetNoUpdateLine(.F.)
	oModel:GetModel("CNBDETAIL"):SetNoDeleteLine(.T.)
	oModel:GetModel("CNBDETAIL"):SetNoInsertLine(.T.)
	oModel:GetModel("CNCDETAIL"):SetNoDeleteLine(.T.)
	If IsInCallStack( "AT870APRRV" )
		oModel:GetModel("CNCDETAIL"):SetNoInsertLine(.T.)
	Else
		oModel:GetModel("CNCDETAIL"):SetNoInsertLine(.F.)
	EndIf
EndIf
Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} TecImpIt

@description Fun��o para transformar csv em array
@author	Augusto Albuquerque
@since	07/02/2020
/*/
//------------------------------------------------------------------------------
Function TecImpIt( aDados, aCampos )
Local cRet := ""
Local nX
Local nY

For nX := 1 To Len( aDados ) 
	For nY := 1 To Len( aDados[nX] )
		If ValType(aDados[nX][nY]) == "D"
			cRet += DToC( aDados[nX][nY] ) + ";"
		ElseIF ValType(aDados[nX][nY]) == "N"
			cRet += cValToChar( aDados[nX][nY] ) + ";"
		ElseIf ValType(aDados[nX][nY]) == "C"
			cRet += aDados[nX][nY] + ";"
		Else
			Loop
		EndIf
	Next nY
	cRet := cRet + CRLF
Next nX

Return cRet
//------------------------------------------------------------------------------
/*/{Protheus.doc} TecDelPln

@description Fun��o de get/set para a variavel Static TecDelPln, que indica se
	o sistema permite ou n�o e exclus�o de CNBs
@author	Mateus Boiani
@since	11/03/2020
/*/
//------------------------------------------------------------------------------
Function TecDelPln(lSetValue)
If Valtype(lSetValue) == 'L'
	lTecDelPln := lSetValue
EndIf
Return lTecDelPln

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} TecAltX1
Atualiza a SX1 
Fun��o utilizada na automa��o
@author Diego Bezerra
@param cPerg, string, nome do pergunte
@param cMvPar, string, par�metro do pergunte que ser� alterado
@param xValue, qualquer, novo valor para o par�metro do pergunte
@param cError, string, cont�m mensagem de erro da execu��o (passar valor via refer�ncia)
@since 30/03/2020
@return lRet, l�gico, .T. = Sucesso - .F. = Falha
/*/
//-------------------------------------------------------------------------------------
Function TecAltX1(cPerg, cMvPar, xValue, cError)

Local lRet		 := .F.
Local cOrdem 	 := ""

Default cPerg 	 := ""
Default cMvPar	 := ""
Default xValue	 := ""
Default cError	 := ""

If !Empty(cPerg)
	If TecHasPerg(cMvPar,cPerg)
		lRet := .T.
		SX1->(DBSetOrder(1))
		SX1->(DBGoTop())
		SX1->(DBSeek(cPerg))

		While ALLTRIM(SX1->X1_GRUPO) == cPerg
			If ALLTRIM(SX1->X1_VAR01) == cMvPar
				cPerg := SX1->X1_GRUPO
				cOrdem := SX1->X1_ORDEM
				Exit
			EndIf
			SX1->(DbSkip())
		End

		If !Empty(cOrdem)
			If SX1->(DBSeek(cPerg + cOrdem))
				If ALLTRIM(SX1->X1_TIPO) == 'D'
					If ValType(xValue) == 'D' .AND. !Empty(xValue)
						xValue := TecDToStr(xValue)
					Else
						xValue := ""
						If !Empty(xValue)
							cError  := STR0035 //#"Tipo de dado inv�lido. Em par�metros do tipo data, informe uma string no formato dd/mm/yyyy"
						Else
							cError  := STR0036 //#"O valor do par�metro n�o foi informado."
						EndIf
					EndIf
				EndIf

				If ALLTRIM(SX1->X1_TIPO) == 'N'
					If ValType(xValue) == 'N'
						xValue := cValToChar(xValue)
					Else
						xValue := ""
						lRet := .F.
					EndIf
				EndIf

				RecLock("SX1", .F.)
					SX1->X1_CNT01 := xValue
				SX1->(MsUnLock())
			EndIf
		Else
			lRet := .F.
			cError := STR0037 //#"Valor inv�lido em SX1->X1_ORDEM"
		EndIf
	Else 
		lRet := .F.
		If !Empty(cMvPar) .AND. ValType(cMvPar) == "C"
			cErro := STR0038 + cMvPar +  STR0039 //#"Par�metro "# " n�o encontrado." 
		Else
			cError := STR0040 //#"Par�metro n�o informado."
		EndIf
	EndIf
Else
	lRet := .F.
	cError := STR0041 //#"Pergunte n�o informado."
EndIf
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TecABBPRHR
@description Fun��o que verifica se existe os campos TFF_QTDHRS e TFF_HRSSAL no dicionario.
@author Augusto Albuquerque
@since  03/04/2020
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TecABBPRHR()

Return TFF->( ColumnPos('TFF_QTDHRS') ) > 0 .AND. TFF->( ColumnPos('TFF_HRSSAL') ) > 0
//------------------------------------------------------------------------------
/*/{Protheus.doc} TecXMxTGYI()

Retorna o �ltimo item da TGY_ITEM de acordo com o CODTDX e ESCALA

@author boiani
@since 24/07/2019
/*/
//------------------------------------------------------------------------------
Function TecXMxTGYI(cEscala, cCodTDX, cCodTFF)
Local cRet := REPLICATE("0",TamSX3("TGY_ITEM")[1])
Local aArea := GetArea()
Local cQry := GetNextAlias()

BeginSQL Alias cQry
	SELECT MAX(TGY.TGY_ITEM) TGY_ITEM
	FROM %Table:TGY% TGY
	WHERE TGY.TGY_FILIAL = %xFilial:TGY%
		AND TGY.%NotDel%
		AND TGY.TGY_ESCALA = %Exp:cEscala%
		AND TGY.TGY_CODTDX = %Exp:cCodTDX%
		AND TGY.TGY_CODTFF = %Exp:cCodTFF%
EndSql
If (cQry)->(!EOF())
	cRet := (cQry)->(TGY_ITEM)
EndIf
(cQry)->(DbCloseArea())

RestArea(aArea)
Return Soma1(cRet)
//------------------------------------------------------------------------------
/*/{Protheus.doc} TecXHasEdH()

Indica se o Editor de Hor�rios est� habilitado

@author boiani
@since 17/04/2020
/*/
//------------------------------------------------------------------------------
Function TecXHasEdH()

Return (SuperGetMV("MV_GSGEHOR",,.F.) .AND. (TGY->( ColumnPos('TGY_ENTRA1')) > 0 ))

//------------------------------------------------------------------------------
/*/{Protheus.doc} TecCotrCli
@description Fun��o que verifica a CNA na revis�o e gera��o do contrato.
@author Augusto Albuquerque
@since  27/04/2020
/*/
//------------------------------------------------------------------------------
Function TecCotrCli(cLoja, cClient, oModel)
Local lRet := .T.

If Empty(cLoja)
	lRet := MTFindMVC(oModel:GetModel("CNCDETAIL"),{{"CNC_CLIENT",cClient}}) > 0
EndIf

Return lRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TECAAHReco
@description Fun��o que verifica a cria��o do campo e da tabela no dicionario
para realizar o processo de recorrencia do FieldService
@author Augusto Albuquerque
@since  27/04/2020
/*/
//------------------------------------------------------------------------------
Function TECAAHReco()
Return AAH->( ColumnPos('AAH_NUMREC')) > 0 .AND. TableInDic("TXJ")


//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TecMultRat
@description Se esta com multifilial e os compartilhamento das tabelas correto para o requisito
@author Augusto Albuquerque
@since  11/06/2020
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TecMultRat()
Local cComAA1 := FwModeAccess("AA1",1) +  FwModeAccess("AA1",2) + FwModeAccess("AA1",3)
Local cComSRA := FwModeAccess("SRA",1) +  FwModeAccess("SRA",2) + FwModeAccess("SRA",3)
Local cComABS := FwModeAccess("ABS",1) +  FwModeAccess("ABS",2) + FwModeAccess("ABS",3)

Return SuperGetMV("MV_GSMSFIL",,.F.) .AND. (LEN(STRTRAN( cComAA1  , "E" )) > LEN(STRTRAN(  cComSRA  , "E" ))) .AND. LEN(STRTRAN(  cComABS  , "E" )) > 0
//------------------------------------------------------------------------------
/*/{Protheus.doc} TecVlPrPar
@description Verifica se a funcinalidade de Valor Provis�rio est� ativa
@author Mateus Boiani
@since  11/06/2020
/*/
//------------------------------------------------------------------------------
Function TecVlPrPar()
Local lRet := TFH->(ColumnPos('TFH_VLPRPA')) > 0 .AND. TFG->(ColumnPos('TFG_VLPRPA')) > 0 .AND.;
					TFF->(ColumnPos('TFF_VLPRPA')) > 0 .AND. TFL->(ColumnPos('TFL_VLPRPA')) > 0
Return lRet
//------------------------------------------------------------------------------
/*/{Protheus.doc} TecxbPrRec
@description Fun��o executada ao alterar o status do contrato para "Vigente"
no GCT, no momento em que os PRs s�o gerados para CTR recorrentes

@return O retorno ser� um num�rico que modificar� o valor do t�tulo PR

@author Mateus Boiani
@since  17/06/2020
/*/
//------------------------------------------------------------------------------
Function TecxbPrRec(nValor, nParcela, aParcelas, nRECCNA)
Local nRet := nValor
Local aArea := GetArea()
Local cSql := ""
Local cAliasAux := ""
Local cSpaceTFL := SPACE(TamSx3("TFL_CODSUB")[1])
Local lAgrupado := SuperGetMv("MV_GSDSGCN",,"2") == '2'

If TecVlPrPar() .AND. nParcela == 1 .AND. !lAgrupado
	cAliasAux := GetNextAlias()
	cSql := " SELECT TFL.TFL_VLPRPA FROM " + RetSqlName("TFL") + " TFL "
	cSql += " INNER JOIN " + RetSqlName("CNA") + " CNA ON "
	cSql += " CNA.CNA_NUMERO = TFL.TFL_PLAN AND "
	cSql += " CNA.CNA_CONTRA = TFL.TFL_CONTRT AND "
	cSql += " CNA.D_E_L_E_T_ = ' ' AND CNA.CNA_FILIAL = '" + xFilial("CNA") + "' "
	cSql += " INNER JOIN " + RetSqlName("TFJ") + " TFJ ON "
	cSql += " TFJ.TFJ_CODIGO = TFL.TFL_CODPAI AND "
	cSql += " TFJ.D_E_L_E_T_ = ' ' AND TFJ.TFJ_FILIAL = '" + xFilial("TFJ") + "' "
	cSql += " WHERE "
	cSql += " TFL.D_E_L_E_T_ = ' ' AND TFL.TFL_FILIAL = '" + xFilial("TFL") + "' "
	cSql += " AND CNA.R_E_C_N_O_ = " + cValToChar(nRECCNA) + " AND "
	cSql += " TFJ.TFJ_CNTREC = '1' AND TFL.TFL_CODSUB = '" + cSpaceTFL + "' "
	cSql := ChangeQuery(cSql)
	dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAliasAux, .F., .T.)

	If !(cAliasAux)->(EOF()) .AND. (cAliasAux)->TFL_VLPRPA > 0
		nRet := (cAliasAux)->TFL_VLPRPA
	EndIf
	(cAliasAux)->(dbCloseArea())
EndIf

RestArea(aArea)
Return nRet
//------------------------------------------------------------------------------
/*/{Protheus.doc} TecMedPrRa
@description Fun��o executada ao selecionar a compet�ncia na tela de medi��o (CNTA121)

@author Mateus Boiani
@since  17/06/2020
/*/
//------------------------------------------------------------------------------
Function TecMedPrRa(oMdlMed, cContra, cRevisa, cNumPla, cCodTFJ)
Local oModelCXN
Local nTFLVlPrPa := 0
Local cAliasTFL	:= GetNextAlias()
Local lAgrupado := SuperGetMv("MV_GSDSGCN",,"2") == '2'

If TecVlPrPar() .AND. Posicione("TFJ",1,xFilial("TFJ")+cCodTFJ,"TFJ_CNTREC") == '1' .AND. !lAgrupado
	BeginSql Alias cAliasTFL
		SELECT TFL.TFL_VLPRPA
		FROM %table:TFL% TFL
		WHERE TFL.TFL_FILIAL = %xFilial:TFL%
			AND TFL.TFL_CONTRT = %exp:cContra%
			AND TFL.TFL_CONREV = %exp:cRevisa%
			AND TFL.TFL_PLAN = %exp:cNumPla%
			AND TFL.TFL_CODPAI = %exp:cCodTFJ%
			AND TFL.%NotDel%
	EndSql
	If !(cAliasTFL)->(EOF()) .AND. (cAliasTFL)->TFL_VLPRPA > 0
		nTFLVlPrPa := (cAliasTFL)->TFL_VLPRPA
	EndIf
	(cAliasTFL)->(DbCloseArea())
	If nTFLVlPrPa > 0 .AND. VALTYPE(oMdlMed) == 'O'
		oModelCXN := oMdlMed:GetModel('CXNDETAIL')
		oModelCXN:LoadValue("CXN_VLPREV", nTFLVlPrPa )
		oModelCXN:LoadValue("CXN_VLSALD", nTFLVlPrPa )
	EndIf
EndIf

Return nil
//------------------------------------------------------------------------------
/*/{Protheus.doc} TecBLoadMd
@description Fun��o executada ao marcar a planilha na tela de medi��o (CNTA121)

@author Mateus Boiani
@since  17/06/2020
/*/
//------------------------------------------------------------------------------
Function TecBLoadMd(cContra, cRevisa, cPlan, cItCNB, oMdlMed, cCodTFJ, cFilCNB)
Local oModelCNE
Local cAliasTFL
Local aArea := GetArea()
Local cJoinMI := ""
Local cJoinMC := ""
Local cSql := ""
Local lOrcPrc := SuperGetMv("MV_ORCPRC",,.F.)
Local lAgrupado := SuperGetMv("MV_GSDSGCN",,"2") == '2'
Local nValPrPa := 0
Default cFilCNB := cFilAnt
If VALTYPE(oMdlMed) == "O" .AND. !lAgrupado
	oModelCNE := oMdlMed:GetModel('CNEDETAIL')
	If TecVlPrPar() .AND. POSICIONE("TFJ",1,xFilial("TFJ", cFilCNB)+cCodTFJ,"TFJ_CNTREC") == '1'
		cAliasTFL := GetNextAlias()
		If lOrcPrc //Material x local
			cJoinMI := " LEFT JOIN " + RetSqlName("TFG") + " TFG ON "
			cJoinMI += " TFG.TFG_CODPAI = TFL.TFL_CODIGO AND TFG.D_E_L_E_T_ = ' ' AND "
			cJoinMI += " TFG.TFG_ITCNB = '" + cItCNB + "' AND "
			cJoinMI += " TFG.TFG_FILIAL = '" + xFilial("TFG", cFilCNB) +"' "

			cJoinMC := " LEFT JOIN " + RetSqlName("TFH") + " TFH ON "
			cJoinMC += " TFH.TFH_CODPAI = TFL.TFL_CODIGO AND TFH.D_E_L_E_T_ = ' ' AND "
			cJoinMC += " TFH.TFH_ITCNB = '" + cItCNB + "' AND "
			cJoinMC += " TFH.TFH_FILIAL = '" + xFilial("TFH", cFilCNB) +"' "
		Else //Material x Item de RH
			cJoinMI := " LEFT JOIN " + RetSqlName("TFG") + " TFG ON "
			cJoinMI += " TFG.TFG_CODPAI = TFF.TFF_COD AND TFG.D_E_L_E_T_ = ' ' AND "
			cJoinMI += " TFG.TFG_ITCNB = '" + cItCNB + "' AND "
			cJoinMI += " TFG.TFG_FILIAL = '" + xFilial("TFG", cFilCNB) +"' "

			cJoinMC := " LEFT JOIN " + RetSqlName("TFH") + " TFH ON "
			cJoinMC += " TFH.TFH_CODPAI = TFF.TFF_COD AND TFH.D_E_L_E_T_ = ' ' AND "
			cJoinMC += " TFH.TFH_ITCNB = '" + cItCNB + "' AND "
			cJoinMC += " TFH.TFH_FILIAL = '" + xFilial("TFH", cFilCNB) +"' "
		EndIF
		cSql += " SELECT TFF.TFF_VLPRPA, TFH.TFH_VLPRPA, TFG.TFG_VLPRPA, TFF.TFF_ITCNB, "
		cSql += " TFF.TFF_QTDVEN, TFH.TFH_QTDVEN, TFG.TFG_QTDVEN "
		cSql += " FROM " + RetSqlName("TFL") + " TFL "
		cSql += " LEFT JOIN " + RetSqlName("TFF") + " TFF ON "
		cSql += " TFF.TFF_CODPAI = TFL.TFL_CODIGO AND "
		cSql += " TFF.TFF_FILIAL = '" + xFilial("TFF", cFilCNB) + "' AND "
		If lOrcPrc
			cSql += " TFF.TFF_ITCNB = '" + cItCNB + "' AND "
		EndIf
		cSql += " TFF.D_E_L_E_T_ = ' ' "
		cSql += cJoinMI
		cSql += cJoinMC
		cSql += " WHERE "
		cSql += " TFL.D_E_L_E_T_ = ' ' AND "
		cSql += " TFL.TFL_PLAN = '" + cPlan + "' AND "
		cSql += " TFL.TFL_CONTRT = '" + cContra + "' AND "
		cSql += " TFL.TFL_CONREV = '" + cRevisa + "' AND "
		cSql += " TFL.TFL_CODPAI = '" + cCodTFJ + "' AND "
		cSql += " TFL.TFL_FILIAL = '" + xFilial("TFL", cFilCNB) + "' "
		cSql := ChangeQuery(cSql)
		dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAliasTFL, .F., .T.)

		While !(cAliasTFL)->(EOF())
			If !EMPTY((cAliasTFL)->TFF_VLPRPA) .AND. (cAliasTFL)->TFF_VLPRPA > 0 .AND. (cAliasTFL)->TFF_ITCNB == cItCNB
				nValPrPa := (cAliasTFL)->TFF_VLPRPA / (cAliasTFL)->TFF_QTDVEN
			ElseIf !EMPTY((cAliasTFL)->TFH_VLPRPA) .AND. (cAliasTFL)->TFH_VLPRPA > 0
				nValPrPa := (cAliasTFL)->TFH_VLPRPA / (cAliasTFL)->TFH_QTDVEN
			ElseIf !EMPTY((cAliasTFL)->TFG_VLPRPA) .AND. (cAliasTFL)->TFG_VLPRPA > 0
				nValPrPa := (cAliasTFL)->TFG_VLPRPA / (cAliasTFL)->TFG_QTDVEN
			EndIf
			If nValPrPa > 0
				oMdlMed:LoadValue('CNEDETAIL','CNE_VLUNIT',nValPrPa)
			EndIF
			(cAliasTFL)->(DbSkip())
		End
		(cAliasTFL)->(DbCloseArea())
	EndIf
EndIf
RestArea(aArea)
Return
//------------------------------------------------------------------------------
/*/{Protheus.doc} TecBAtVlpr
@description Fun��o executada ao encerrar a medi��o (CNTA121)

@author Mateus Boiani
@since  17/06/2020
/*/
//------------------------------------------------------------------------------
Function TecBAtVlpr(cFilCTR, cCodTFJ, cContra, cRevisa, nRecCNA)
Local cCodTFL := ""
Local cSql := ""
Local cAliasTFL	:= GetNextAlias()
Local lOrcPrc := SuperGetMv("MV_ORCPRC",,.F.)
Local aArea := GetArea()
Local nRet := 0
Local nTotLocal := 0
Local lAgrupado := SuperGetMv("MV_GSDSGCN",,"2") == '2'

Default cFilCTR := cFilAnt
If TecVlPrPar() .AND. !lAgrupado .AND. POSICIONE("TFJ",1,xFilial("TFJ", cFilCTR)+cCodTFJ,"TFJ_CNTREC") == '1'
	cSql += " SELECT TFL.TFL_CODIGO "
	cSql += " FROM " + RetSqlName("TFL") + " TFL "
	cSql += " INNER JOIN " + RetSqlName("CNA") + " CNA ON "
	cSql += " CNA.CNA_NUMERO = TFL.TFL_PLAN AND "
	cSql += " CNA.CNA_CONTRA = TFL.TFL_CONTRT AND "
	cSql += " CNA.CNA_REVISA = TFL.TFL_CONREV AND "
	cSql += " CNA.CNA_FILIAL = '" + xFilial("CNA", cFilCTR) + "' "
	cSql += " WHERE "
	cSql += " TFL.D_E_L_E_T_ = ' ' AND "
	cSql += " TFL.TFL_CONTRT = '" + cContra + "' AND "
	cSql += " TFL.TFL_CONREV = '" + cRevisa + "' AND "
	cSql += " TFL.TFL_CODPAI = '" + cCodTFJ + "' AND "
	cSql += " TFL.TFL_FILIAL = '" + xFilial("TFL", cFilCTR) + "' AND "
	cSql += " CNA.R_E_C_N_O_ = " + cValToChar(nRecCNA)
	cSql := ChangeQuery(cSql)
	dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAliasTFL, .F., .T.)
	cCodTFL := (cAliasTFL)->TFL_CODIGO
	(cAliasTFL)->(DbCloseArea())

	DBSelectArea("TFF")
	TFF->(DbSetOrder(3)) //TFF_FILIAL+TFF_CODPAI+TFF_ITEM
	TFF->(MsSeek(xFilial("TFF", cFilCTR) + cCodTFL))
	While !(TFF->(EOF())) .AND. TFF->TFF_FILIAL == xFilial("TFF", cFilCTR) .AND. TFF->TFF_CODPAI == cCodTFL
		If TFF->TFF_COBCTR != '2'
			nRet := At740PrxPa(/*cTipo*/, TFF->TFF_QTDVEN, TFF->TFF_PRCVEN, TFF->TFF_DESCON, TFF->TFF_TXLUCR, TFF->TFF_TXADM)
			RecLock("TFF", .F.)
				TFF->TFF_VLPRPA := nRet
			TFF->(MsUnlock())
			nTotLocal += nRet
		EndIf
		If !lOrcPrc
			DbSelectArea("TFH")
			TFH->(DbSetOrder(3)) //TFH_FILIAL+TFH_CODPAI+TFH_ITEM
			TFH->(MsSeek(xFilial("TFH", cFilCTR) + TFF->TFF_COD))
			While !(TFH->(EOF())) .AND. TFH->TFH_FILIAL == xFilial("TFH", cFilCTR) .AND. TFH->TFH_CODPAI == TFF->TFF_COD
				If TFH->TFH_COBCTR != '2'
					nRet := At740PrxPa(/*cTipo*/, TFH->TFH_QTDVEN, TFH->TFH_PRCVEN, TFH->TFH_DESCON, TFH->TFH_TXLUCR, TFH->TFH_TXADM)
					RecLock("TFH", .F.)
						TFH->TFH_VLPRPA := nRet
					TFH->(MsUnlock())
					nTotLocal += nRet
					TFH->(DbSkip())
				EndIf
			End
			
			DbSelectArea("TFG")
			TFG->(DbSetOrder(3)) //TFG_FILIAL+TFG_CODPAI+TFG_ITEM
			TFG->(MsSeek(xFilial("TFG", cFilCTR) + TFF->TFF_COD))
			While !(TFG->(EOF())) .AND. TFG->TFG_FILIAL == xFilial("TFG", cFilCTR) .AND. TFG->TFG_CODPAI == TFF->TFF_COD
				If TFG->TFG_COBCTR != '2'
					nRet := At740PrxPa(/*cTipo*/, TFG->TFG_QTDVEN, TFG->TFG_PRCVEN, TFG->TFG_DESCON, TFG->TFG_TXLUCR, TFG->TFG_TXADM)
					RecLock("TFG", .F.)
						TFG->TFG_VLPRPA := nRet
					TFG->(MsUnlock())
					nTotLocal += nRet
					TFG->(DbSkip())
				EndIf
			End
		EndIf
		TFF->(DbSkip())
	End
	If lOrcPrc
		DbSelectArea("TFH")
		TFH->(DbSetOrder(3)) //TFH_FILIAL+TFH_CODPAI+TFH_ITEM
		TFH->(MsSeek(xFilial("TFH", cFilCTR) + cCodTFL))
		While !(TFH->(EOF())) .AND. TFH->TFH_FILIAL == xFilial("TFH", cFilCTR) .AND. TFH->TFH_CODPAI == cCodTFL
			If TFH->TFH_COBCTR != '2'
				nRet := At740PrxPa(/*cTipo*/, TFH->TFH_QTDVEN, TFH->TFH_PRCVEN, TFH->TFH_DESCON, TFH->TFH_TXLUCR, TFH->TFH_TXADM)
				RecLock("TFH", .F.)
					TFH->TFH_VLPRPA := nRet
				TFH->(MsUnlock())
				nTotLocal += nRet
				TFH->(DbSkip())
			EndIf
		End
		
		DbSelectArea("TFG")
		TFG->(DbSetOrder(3)) //TFG_FILIAL+TFG_CODPAI+TFG_ITEM
		TFG->(MsSeek(xFilial("TFG", cFilCTR) + cCodTFL))
		While !(TFG->(EOF())) .AND. TFG->TFG_FILIAL == xFilial("TFG", cFilCTR) .AND. TFG->TFG_CODPAI == cCodTFL
			If TFG->TFG_COBCTR != '2'
				nRet := At740PrxPa(/*cTipo*/, TFG->TFG_QTDVEN, TFG->TFG_PRCVEN, TFG->TFG_DESCON, TFG->TFG_TXLUCR, TFG->TFG_TXADM)
				RecLock("TFG", .F.)
					TFG->TFG_VLPRPA := nRet
				TFG->(MsUnlock())
				nTotLocal += nRet
				TFG->(DbSkip())
			EndIf
		End
	EndIf

	DbSelectArea("TFL")
	TFL->(DbSetOrder(1))
	If TFL->(MsSeek(xFilial("TFL",cFilCTR) + cCodTFL))
		RecLock("TFL", .F.)
			TFL->TFL_VLPRPA := nTotLocal
		TFL->(MsUnlock())
	EndIf
EndIf
RestArea(aArea)
Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TecABRComp
@description Fun��o que verifica se existe o campo da ABR no dic
@author Augusto Albuquerque
@since  06/07/2020
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TecABRComp()

Return ABR->( ColumnPos('ABR_COMPEN') ) > 0
//------------------------------------------------------------------------------
/*/{Protheus.doc} TecBRelABB
@description Retorna em formato de array as ABBs relacionadas de um determinado c�digo
(pela Data de Refer�ncia)

@author Mateus Boiani
@since  13/07/2020
/*/
//------------------------------------------------------------------------------
Function TecBRelABB(cCodAbb)
Local aRet := {}
Local cAliasTDV2 := GetNextAlias()
Local cAliasTDV1 := GetNextAlias()
Local cDtRef := ""
Local cCodTec := ""
Local cFilABB := ""

BeginSql Alias cAliasTDV1
	SELECT TDV.TDV_DTREF, ABB.ABB_CODTEC, ABB.ABB_FILIAL
	FROM %table:TDV% TDV
	INNER JOIN %Table:ABB% ABB
		ON ABB.ABB_FILIAL = TDV.TDV_FILIAL
		AND ABB.ABB_CODIGO = TDV.TDV_CODABB
		AND ABB.%NotDel%
	WHERE TDV.TDV_FILIAL = %xFilial:TDV%
		AND TDV.%NotDel%
		AND ABB.ABB_CODIGO = %exp:cCodAbb%
EndSql

cDtRef := (cAliasTDV1)->TDV_DTREF
cCodTec := (cAliasTDV1)->ABB_CODTEC
cFilABB := (cAliasTDV1)->ABB_FILIAL

(cAliasTDV1)->(DbCloseArea())

BeginSql Alias cAliasTDV2
	SELECT TDV.TDV_CODABB
	FROM %table:TDV% TDV
	INNER JOIN %Table:ABB% ABB
		ON ABB.ABB_FILIAL = TDV.TDV_FILIAL
		AND ABB.ABB_CODIGO = TDV.TDV_CODABB
		AND ABB.%NotDel%
	WHERE ABB.ABB_FILIAL = %exp:cFilABB%
		AND TDV.%NotDel%
		AND TDV.TDV_DTREF = %exp:cDtRef%
		AND ABB.ABB_CODTEC = %exp:cCodTec% 
EndSql

While !(cAliasTDV2)->(EOF())
	AADD(aRet, (cAliasTDV2)->TDV_CODABB)
	(cAliasTDV2)->(DbSkip())
End
(cAliasTDV2)->(DbCloseArea())

Return aRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TecCkStAt
@description Compara requisitos do atendente com requisitos do post
@param cCodAtd, string, c�digo do atendente
@param cCodTFF, string, c�digo do posto
@param cCodLocal, string, c�digo do local de atendimento
@param cAtdFunc, string, c�digo da fun��o do posto
@param lTecxRh, l�gico, verifica a integra��o do RH com o GS
@return cLogEsp, string, mensagem contendo as inconsist�ncias entre atendente x post
@author Diego Bezerra
@since  16/07/2020
/*/
//------------------------------------------------------------------------------

Function TecCkStAt(cCodAtd, cCodTFF, cCodLocal, cAtdFunc, cLocFunc, lTecxRh, lDetail)

Local aLocEsp 	:= {}
Local aTecEsp 	:= {} 
Local cLogEsp	:= ""
Local nFound	:= 0
Local nX		:= 0
Local nY		:= 0
Local nCount 	:= 0
Local cCargFun	:= ""	// Cargo do funcion�rio
Local cDescCFun	:= ""	// Descri��o do cargo do funcion�rio
Local cDescCLoc	:= ""	// Descri��o do cargo do local
Local cCargLoc	:= ""	// Cargo do local de atendimento
Local cCodFun	:= ""	// C�digo do funcion�rio
Local cDtFunP	:= ""   // Descri��o da fun��o do posto	
Local cDtFunA	:= ""	// Descri��o da fun��o do atendente
Default lDetail	:= .F.	// .T. = Mensagem detalhada, .F. = Mensagem simplificada

aLocEsp	:= ckFilterATD(cCodAtd, cCodTFF, cCodLocal, lTecXRh)

cCodFun	:= POSICIONE("AA1",1,xFilial("AA1") + cCodAtd, "AA1_CDFUNC")
aTecEsp := TECHabTec(cCodAtd, cCodFun, lTecXRh)

If lTecXRh
	cCargFun := POSICIONE("SRA",1,xFilial("SRA") + cCodFun, "RA_CARGO")
	If lDetail .AND. !Empty(cCargFun) 
		cDescCFun := POSICIONE("SQ3",1, xFilial("SQ3") + cCargFun, "Q3_DESCSUM")
	EndIf
	cCargLoc := POSICIONE("TFF",1,xFilial("TFF") + cCodTFF,"TFF_CARGO")
	If lDetail .AND. !Empty(cCargLoc) 
		cDescCLoc := POSICIONE("SQ3",1, xFilial("SQ3") + cCargLoc, "Q3_DESCSUM")
	EndIf
	If ALLTRIM(cCargFun) <> ALLTRIM(cCargLoc)
		nCount++
		If lDetail
			cLogEsp += STR0061 + ALLTRIM(cCodAtd) +; //"- O cargo do atendente "
				Iif(!Empty(cCargFun), + STR0064 + ALLTRIM(cDescCFun), STR0062 ) +; //" � " #" est� vazio"
				 STR0063 +; //" e o cargo do posto"
				Iif(!Empty(cCargLoc), STR0064 + ALLTRIM(cDescCLoc) + "; ", STR0062 + "; " ) + CRLF //" � " # " est� vazio "
		Else
			cLogEsp += STR0060 //"cargo"
		EndIf
	EndIf
EndIf

If ALLTRIM(cAtdFunc) <> ALLTRIM(cLocFunc)
	nCount++
	If lDetail
		cDtFunP	:= POSICIONE("SRJ",1,xFilial("SRJ") + cLocFunc, "RJ_DESC")
		cDtFunA	:= POSICIONE("SRJ",1,xFilial("SRJ") + cAtdFunc, "RJ_DESC")
		cLogEsp += STR0065 + ALLTRIM(cCodAtd) +; //"- A fun��o do atendente "
		 	Iif(!Empty(cAtdFunc), + STR0064 + ALLTRIM(cDtFunA), STR0062)+; // " � " # " est� vazio"
			  STR0066 +;	// " e a fun��o do posto "
			Iif(!Empty(cLocFunc), + STR0064 + ALLTRIM(cDtFunP) + "; ", + STR0062 + "; ") + CRLF // " � " # " est� vazio"
	Else 
		If Empty(cLogEsp)
			cLogEsp += STR0059 //"fun�ao"
		Else
			cLogEsp += STR0058  //", fun�ao"
		EndIf
	EndIf
EndIf
For nX := 1 to Len(aLocEsp[1][2])
	nFound := 0
	For nY := 1 to Len(aTecEsp[1][2])
		If aLocEsp[1][2][nX][2] == aTecEsp[1][2][nY][2]
			nFound ++
		EndIf
	Next nY
	If nFound == 0
		nCount++
		If lDetail
			cLogEsp += STR0067 + CRLF // "- As caracter�sticas do atendente s�o incompat�veis com as necess�rias para o posto;"
		Else
			If Empty(cLogEsp)
				cLogEsp += STR0057 // "caracter�sticas"
			Else
				cLogEsp += STR0056 //", caracter�sticas"
			EndIf
		EndIf
	EndIf
Next nX

For nX := 1 to Len(aLocEsp[2][2])
	nFound := 0
	For nY := 1 to Len(aTecEsp[2][2])
		If aLocEsp[2][2][nX][2] == aTecEsp[2][2][nY][2]
			nFound ++
		EndIf
	Next nY
	If nFound == 0
		nCount++
		If lDetail
			cLogEsp += STR0068 + CRLF // "- o atendente n�o possui as habilidades necess�rias para o posto; "
		Else
			If Empty(cLogEsp)
				cLogEsp += STR0055 // "habilidades"
			Else
				cLogEsp += STR0054 //", habilidades"
			EndIf
		EndIf
	EndIf
Next nX

For nX := 1 to Len(aLocEsp[3][2])
	nFound := 0
	For nY := 1 to Len(aTecEsp[3][2])
		If aLocEsp[3][2][nX][2] == aTecEsp[3][2][nY][2]
			nFound ++
		EndIf
	Next nY
	If nFound == 0
		nCount++
		If lDetail
			cLogEsp += STR0069 + CRLF // "- O atendente n�o possui os cursos necess�rios para o posto; "
		Else
			If Empty(cLogEsp)
				cLogEsp += STR0053 // "cursos"
			Else
				cLogEsp += STR0052 // ", cursos"
			EndIf
		EndIf
	EndIf
Next nX

nFound := 0
For nY := 1 to Len(aTecEsp[4][2])
	If aLocEsp[4][2] == aTecEsp[4][2][nY][2]
		nFound ++
	EndIf
Next nY

If !Empty(aLocEsp[4][2]) .AND. nFound == 0
	nCount++
	If lDetail
		cLogEsp += STR0070 + CRLF  // "- A regi�o de atendimento do atendente n�o contempla a regi�o do posto "
	Else 
		If Empty(cLogEsp)
			cLogEsp += STR0051 //"regi�o"
		Else
			cLogEsp += STR0050 //", regi�o"
		EndIf
	EndIf
EndIf

If !Empty(cLogEsp ) 
	If lDetail
		cLogEsp := STR0071 + ALLTRIM(cCodAtd) + STR0072 + ALLTRIM(cCodTFF) + " :" + CRLF + cLogEsp + CRLF //"Inconsist�ncias encontradas para o atendente " #" no posto "
	Else
		If nCount > 0
			cLogEsp := STR0047 + cLogEsp + STR0048 //"Inconsist�ncias encontradas em: "#" do atendente"
		Else
			cLogEsp := STR0049 + cLogEsp + STR0048 //"Inconsist�ncia encontra em  "#" do atendente"
		EndIf
	EndIf
EndIf

Return cLogEsp

//------------------------------------------------------------------------------
/*/{Protheus.doc} TECHabTec
@description Obtem caracter�sticas, habilidades, cursos e regi�o do atendente
@param cCodAtd, string, c�digo do atendente
@param cCodTFF, string, c�digo do posto
@param cCodLocal, string, c�digo do local de atendimento
@param lTecXRh, l�gico, integra��o do RH com GS
@return aRet, array, array com as caracter�sticas, habilidades, cursos e regi�o do atendente
@author Diego Bezerra
@since  16/07/2020
/*/
//------------------------------------------------------------------------------
Static Function ckFilterATD(cCodAtd, cCodTFF, cCodLocal, lTecXRh)

Local cQry := ""
Local cAliasTDS := getNextAlias()
Local cAliasTDT := getNextAlias()
Local cAliasTGV := getNextAlias()
Local cAliasABS	:= getNextAlias()

Local aRet		:= { {"Caracteristicas", {}}, {"Habilidades",{}}, {"Cursos",{}}, {"Regiao",""} }

// Caracter�sticas

cQry = "SELECT TDS_CODTFF, TDS_CODTCZ, TCZ_DESC FROM " + retSqlName('TDS')+ " TDS "  
cQry += " INNER JOIN " + retSqlName('TCZ') + " TCZ ON TDS_CODTCZ = TCZ_COD "
cQry += " AND TCZ.TCZ_FILIAL = '" + xFilial('TCZ') + "'  AND TCZ.D_E_L_E_T_ = ' ' "
cQry += " WHERE TDS.TDS_FILIAL = '" + xFilial('TDS') + "' AND TDS.D_E_L_E_T_ = ' ' "
cQry += " AND TDS.TDS_CODTFF = '" + cCodTFF + "' "

cQry := ChangeQuery(cQry)
dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQry),cAliasTDS, .F., .T.)

// Habilidades
cQry = "SELECT  TDT_CODTFF, TDT_CODHAB, RBG_DESC FROM " + retSqlName("TDT") + " TDT "
cQry += " INNER JOIN " + retSqlName("RBG") + " RBG ON TDT_CODHAB = RBG_HABIL "
cQry += " AND RBG.RBG_FILIAL = '" + xFilial("RBG") + "' AND RBG.D_E_L_E_T_ = ' ' " 
cQry += " WHERE TDT.TDT_FILIAL = '" + xFilial("TDT") + "' AND TDT.D_E_L_E_T_ = ' ' " 
cQry += " AND TDT.TDT_CODTFF = '" + cCodTFF + "' "

cQry := ChangeQuery(cQry)
dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQry),cAliasTDT, .F., .T.)

// Cursos
cQry = "SELECT TGV_CODTFF, TGV_CURSO, RA1_DESC FROM " + retSqlName("TGV") + " TGV "
cQry += " INNER JOIN " + retSqlName("RA1") + " RA1 ON TGV_CURSO = RA1_CURSO "
cQry += " AND RA1.RA1_FILIAL = '" + xFilial("RA1") + "' AND RA1.D_E_L_E_T_ = ' ' "
cQry += " WHERE TGV.TGV_FILIAL = '" + xFilial("TGV") + "' AND TGV.D_E_L_E_T_ = ' '"
cQry += " AND TGV_CODTFF = '" + cCodTFF + "' "

cQry := ChangeQuery(cQry)
dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQry),cAliasTGV, .F., .T.)

cQry = "SELECT ABS_REGIAO FROM "+retSqlName("ABS") + " ABS WHERE ABS.ABS_FILIAL = '" + xFilial("ABS") + "' AND ABS.D_E_L_E_T_ = ' ' "
cQry += " AND ABS_LOCAL = '" + cCodLocal + "' "
cQry := ChangeQuery(cQry)
dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQry),cAliasABS, .F., .T.)


While (cAliasTDS)->(!Eof())
	aAdd(aRet[1][2],{ (cAliasTDS)->TDS_CODTFF, (cAliasTDS)->TDS_CODTCZ,(cAliasTDS)->TCZ_DESC })
	(cAliasTDS)->(dbSkip())
EndDo
(cAliasTDS)->(dbCloseArea())

While (cAliasTDT)->(!Eof())
	aAdd(aRet[2][2],{ (cAliasTDT)->TDT_CODTFF, (cAliasTDT)->TDT_CODHAB,(cAliasTDT)->RBG_DESC })
	(cAliasTDT)->(dbSkip())
EndDo
(cAliasTDT)->(dbCloseArea())

While (cAliasTGV)->(!Eof())
	aAdd(aRet[3][2],{ (cAliasTGV)->TGV_CODTFF, (cAliasTGV)->TGV_CURSO,(cAliasTGV)->RA1_DESC })
	(cAliasTGV)->(dbSkip())
EndDo
(cAliasTGV)->(dbCloseArea())

If (cAliasABS)->(!Eof())
	aRet[4][2] := (cAliasABS)->ABS_REGIAO
EndIf
(cAliasABS)->(dbCloseArea())

Return aRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} getPosCar
@description Obtem caracter�sticas, habilidades, cursos e regi�o do atendente
@param cCodTec, string, c�digo do atendente
@param cMatFunc, string, matr�cula do funcion�rio
@param lTecXRh, l�gico, integra��o do RH com GS
@return aRet, array, array com as caracter�sticas, habilidades, cursos e regi�es do posto
@author Diego Bezerra
@since  16/07/2020
/*/
//------------------------------------------------------------------------------
Static Function TECHabTec(cCodTec, cMatFunc, lTecXRh)

Local cQry 		:= ""
Local cAliasTDU	:= getNextAlias()	// Caracter�sticas do atendente
Local cAliasHAB	:= getNextAlias()	// Habilidades do funcion�rio
Local cAliasRA4	:= getNextAlias()	// Cursos do funcionario
Local cAliasABU := getNextAlias()	// Regi�es de atendimento do atendente

Local aRet		:= { {"Caracteristicas", {}}, {"Habilidades",{}}, {"Cursos",{}}, {"Regioes",{}} }

// Caracter�sticas
cQry = "SELECT TDU_CODTEC, TDU_COD, TCZ_DESC FROM " + retSqlName("TDU") + " TDU "
cQry += " INNER JOIN " + retSqlName("TCZ") + " TCZ ON TDU_COD = TCZ_COD "
cQry += " AND TCZ.TCZ_FILIAL = '" + xFilial("TCZ") + "' AND TCZ.D_E_L_E_T_ = ' '"
cQry += " WHERE TDU.TDU_FILIAL = '" + xFilial("TDU") + "' AND TDU.D_E_L_E_T_ = ' '"
cQry += " AND TDU.TDU_CODTEC = '" + cCodTec + "' "
cQry := ChangeQuery(cQry)
dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQry),cAliasTDU, .F., .T.)

// Habilidades
If lTecXRh
	cQry = "SELECT RBI_MAT, RBI_HABIL, RBG_DESC FROM " + retSqlName("RBI") + " RBI "
	cQry += " INNER JOIN " + retSqlName("RBG") + " RBG ON RBI_HABIL = RBG_HABIL "
	cQry += " AND RBG.RBG_FILIAL = '" + xFilial("RBG") + "' AND RBG.D_E_L_E_T_ = ' ' "
	cQry += " WHERE RBI.RBI_FILIAL = '" + xFilial("RBI") + "' AND RBI.D_E_L_E_T_ = ' ' "
	cQry += " AND RBI_MAT = '" + cMatFunc + "' "
	cQry := ChangeQuery(cQry)
	dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQry),cAliasHAB, .F., .T.)
Else
	cQry = "SELECT AA2_CODTEC, AA2_HABIL, AA2_DESCRI FROM " + retSqlName("AA2") + " AA2 "
	cQry += " WHERE AA2.AA2_FILIAL = '" + xFilial("AA2") + "' AND AA2.D_E_L_E_T_ = ' '"
	cQry := ChangeQuery(cQry)
	dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQry),cAliasHAB, .F., .T.)
EndIf

// Cursos
cQry = "SELECT RA4_MAT, RA4_CURSO, RA1_DESC FROM " + retSqlName("RA4") + " RA4 "
cQry += " INNER JOIN " + retSqlName("RA1") + " RA1 ON RA4_CURSO = RA1_CURSO "
cQry += " AND RA1.RA1_FILIAL = '" + xFilial("RA1") + "' AND RA1.D_E_L_E_T_ = ' '"
cQry += " WHERE RA4.RA4_FILIAL = '" + xFilial("RA4") + "' AND RA4.D_E_L_E_T_ = ' '"
cQry += " AND RA4_MAT = '" + cMatFunc + "' "
cQry := ChangeQuery(cQry)
dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQry),cAliasRA4, .F., .T.)

// Regi�es
cQry = "SELECT ABU_CODTEC, ABU_REGIAO FROM " + retSqlName("ABU") + " ABU WHERE ABU.ABU_FILIAL = '" + xFilial("ABU") + "' AND ABU.D_E_L_E_T_ = ' ' " 
cQry += " AND ABU.ABU_CODTEC = '" + cCodTec + "' "
cQry := ChangeQuery(cQry)
dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQry),cAliasABU, .F., .T.)

While (cAliasTDU)->(!Eof())
	aAdd(aRet[1][2],{ (cAliasTDU)->TDU_CODTEC, (cAliasTDU)->TDU_COD,(cAliasTDU)->TCZ_DESC })
	(cAliasTDU)->(dbSkip())
EndDo
(cAliasTDU)->(dbCloseArea())

While (cAliasHAB)->(!Eof())
	If lTecXRh
		aAdd(aRet[2][2],{ (cAliasHAB)->RBI_MAT, (cAliasHAB)->RBI_HABIL,(cAliasHAB)->RBG_DESC })
	Else
		aAdd(aRet[2][2],{ (cAliasHAB)->AA2_CODTEC, (cAliasHAB)->AA2_HABIL,(cAliasHAB)->AA2_DESCRI  })
	EndIF
	(cAliasHAB)->(dbSkip())
EndDo
(cAliasHAB)->(dbCloseArea())

While (cAliasRA4)->(!Eof())
	aAdd(aRet[3][2],{ (cAliasRA4)->RA4_MAT, (cAliasRA4)->RA4_CURSO,(cAliasRA4)->RA1_DESC })
	(cAliasRA4)->(dbSkip())
EndDo
(cAliasRA4)->(dbCloseArea())

While (cAliasABU)->(!Eof())
	aAdd(aRet[4][2],{ (cAliasABU)->ABU_CODTEC, (cAliasABU)->ABU_REGIAO })
	(cAliasABU)->(dbSkip())
EndDo
(cAliasABU)->(dbCloseArea())

Return aRet
//------------------------------------------------------------------------------
/*/{Protheus.doc} TecBOrdMrk

@description Verifica a TDV para inserir os dados da SP8 

(s� � executado caso o sistema localize a altera��o de Ordem entre os dias)

@author	Mateus Boiani
@since	01/07/2019
/*/
//------------------------------------------------------------------------------
Function TecBOrdMrk(aMarcacoes, nMar, aOrd, nTab, cMarc, aTabCalend, nPosCalend, cFilSRA, cMat)
Local aArea
Local cOrdem := aOrd[ nTab , 1 ]
Local dDataApo := aOrd[ nTab , 7 ]
Local cTurno := aOrd[ nTab , 2 ]
Local cSeq := aOrd[ nTab , 6 ]
Local cSql := ""
Local cAliasQry
Local nCount := 0
Local lMtFil := SuperGetMV("MV_GSMSFIL",,.F.)
Local lTECA910 := isInCallStack("TECA910")

If !EMPTY(cFilSRA) .AND. !EMPTY(cMat) .AND. lTECA910 .AND. (nPosCalend > 1 .AND.;
		cOrdem != aTabCalend[nPosCalend-1][2] .AND.	aTabCalend[nPosCalend-1][6] != 'N')
	aArea := GetArea()
	cAliasQry := GetNextAlias()
	cSql += " SELECT TDV.TDV_TURNO, TDV.TDV_SEQTRN, TDV.TDV_DTREF "
	cSql += " FROM " + RetSqlName( "TDV" ) + " TDV INNER JOIN "
	cSql += RetSqlName( "ABB" ) + " ABB ON ABB.ABB_FILIAL = TDV.TDV_FILIAL AND "
	cSql += " ABB.ABB_CODIGO = TDV.TDV_CODABB AND ABB.D_E_L_E_T_ = ' ' INNER JOIN "
	cSql += RetSqlName( "AA1" ) + " AA1 ON AA1.AA1_CODTEC = ABB.ABB_CODTEC AND "
	cSql += " AA1.AA1_FILIAL = '" + xFilial("AA1") + "' AND AA1.D_E_L_E_T_ = ' ' AND "
	cSql += " AA1.AA1_FUNFIL = '" + cFilSRA + "' AND AA1.AA1_CDFUNC = '" + cMat + "' "
	cSql += " WHERE "
	cSql += " TDV.D_E_L_E_T_ = ' ' AND ( ABB.ABB_DTINI = '" + DTOS(aMarcacoes[nMar][1]) + "' OR ABB.ABB_DTFIM = '" + DTOS(aMarcacoes[nMar][1]) + "') "
	cSql += " AND ABB.ABB_ATENDE = '1' AND ABB.ABB_CHEGOU = 'S' AND ABB.ABB_ATIVO = '1' "
	cSql += " AND (ABB.ABB_HRINI = '" + TxValToHor(aMarcacoes[nMar][2]) + "' OR ABB.ABB_HRFIM = '" + TxValToHor(aMarcacoes[nMar][2]) + "') "
	If !lMtFil
		cSql += " AND TDV.TDV_FILIAL = '" + xFilial("TDV") + "' "
	EndIf
	cSql := ChangeQuery(cSql)
	dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAliasQry, .F., .T.)
	If !(cAliasQry)->(EOF())
		While !(cAliasQry)->(EOF())
			cTurno := (cAliasQry)->TDV_TURNO
			cSeq := (cAliasQry)->TDV_SEQTRN
			dDataApo := STOD((cAliasQry)->TDV_DTREF)
			(cAliasQry)->(DbSkip())
			nCount++
		End
		If nCount > 1
			dDataApo := aOrd[ nTab , 7 ]
			cTurno := aOrd[ nTab , 2 ]
			cSeq := aOrd[ nTab , 6 ]
		ElseIf aScan(aTabCalend,{ |x| x[48] == dDataApo }) > 0
			cOrdem := aTabCalend[aScan(aTabCalend,{ |x| x[48] == dDataApo })][2]
		EndIf
	EndIf
	(cAliasQry)->(dbCloseArea())
	RestArea(aArea)
EndIf

IF !( aMarcacoes[ nMar , 13 ] ) //AMARC_L_ORIGEM
	aMarcacoes[ nMar , 03 ]	:= cOrdem //AMARC_ORDEM
	aMarcacoes[ nMar , 25 ] := dDataApo //AMARC_DATAAPO
	aMarcacoes[ nMar , 06 ] := cTurno //AMARC_TURNO
	aMarcacoes[ nMar , 16 ] := cSeq	 //AMARC_SEQ
EndIF
aMarcacoes[ nMar , 14 ]	:= cMarc //AMARC_DTHR2STR

Return nil