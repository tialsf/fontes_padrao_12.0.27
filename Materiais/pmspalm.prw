#include "plmexp.ch"
#include "protheus.ch"

Static lFWCodFil := FindFunction("FWCodFil")

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � PMSPalm  �Autor  �Adriano Ueda        � Data �  02/02/04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Rotinas de exportacao e importacao do Palm                 ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PImpConf  �Autor  �Adriano Ueda        � Data �  02/02/04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Rotina de importacao                                       ���
���          �                                                            ���
���          � - importa os registros recebidos do Palm pelo MCS          ���
�������������������������������������������������������������������������͹��
���Parametros� nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � Palm                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Function PImpConf()

	Local aArquivos :=  {} //{"HAF9", "HAF9", "AF9_FILIAL + AF9_PROJET + AF9_REVISA + AF9_TAREFA"}}
	Local cPathPalm := GetSrvProfString("HandHeldDir","\HANDHELD\") + "P" + AllTrim(PALMUSER->P_DIR) + "\atual\"

	aAdd(aArquivos, {"HAFF", "HAFF", "AFF_FILIAL+AFF_PROJET+AFF_REVISA+AFF_TAREFA+AFF_DATA"})
	
	ConOut( replicate("-",79) )
	ConOut("Importando confirmacoes para usuario: " + AllTrim(PALMUSER->P_USER))
	ConOut( replicate("-",79) )
	
	// usar a funcao PChkFile() para abrir os arquivos
	If PChkFile(cPathPalm, aArquivos)
		dbSelectArea("HAFF")
		dbSetOrder(1)
		HAFF->(dbGoTop())
		
		While !HAFF->(Eof())
			If PmsVldFase("AF8", HAFF->AFF_PROJET, "91")
				Do Case

					////////////////////////////
					// alterar confirmacao
					////////////////////////////
					Case HAFF->AFF_SYNCFL == "A"
						dbSelectArea("AFF")
						dbSetOrder(1)
					
						If MsSeek(HAFF->AFF_FILIAL + HAFF->AFF_PROJET + HAFF->AFF_REVISA + HAFF->AFF_TAREFA + DToS(CToD(HAFF->AFF_DATA)))
							
							If SoftLock("AFF") .And. MaCanAltAFF("AFF", .F.)

								// estorno
								PmsAvalAFF("AFF", 2)

								ConOut("---------------------------------------------")
								ConOut("Alterando a confirmacao:")
								ConOut("Projeto: " + AllTrim(HAFF->AFF_PROJET))
								ConOut("Tarefa : " + AllTrim(HAFF->AFF_TAREFA))
								ConOut("Data   : " + Alltrim(HAFF->AFF_DATA))
								ConOut("Quant  : " + AllTrim(Str(HAFF->AFF_QUANT)))
								ConOut("---------------------------------------------")							
							
								Reclock("AFF", .F.)							
				  				AFF->AFF_FILIAL := HAFF->AFF_FILIAL
				  				AFF->AFF_PROJET := HAFF->AFF_PROJET
				  				AFF->AFF_REVISA := HAFF->AFF_REVISA
				  				AFF->AFF_DATA   := CToD(HAFF->AFF_DATA)  // esta conversao e necessaria
				  				AFF->AFF_TAREFA := HAFF->AFF_TAREFA
				  				//AFF->AFF_PERC   := HAFF->AFF_PERC
				  				AFF->AFF_QUANT  := HAFF->AFF_QUANT
				  				AFF->AFF_OCORRE := HAFF->AFF_OCORRE
				  				AFF->AFF_CODMEM := HAFF->AFF_CODMEM
				  				AFF->AFF_USER   := PALMUSER->P_USERID
				  				AFF->AFF_CONFIR := HAFF->AFF_CONFIR
				  				
				  				MsUnlock()
			  				
				  				// inclusao
			  					PmsAvalAFF("AFF", 1)
			  				Else
							  // a confirmacao nao pode ser alterada
								ConOut("---------------------------------------------")
								ConOut("A seguinte confirmacao nao pode ser alterada:")
								ConOut("Projeto: " + AllTrim(HAFF->AFF_PROJET))
								ConOut("Tarefa : " + AllTrim(HAFF->AFF_TAREFA))
								ConOut("Data   : " + Alltrim(HAFF->AFF_DATA))
								ConOut("Quant  : " + AllTrim(Str(HAFF->AFF_QUANT)))
								ConOut("---------------------------------------------")							
								
								// TODO
								// fazer log
			  				EndIf
						Else
						  	// a confirmacao nao pode ser alterada
							ConOut("---------------------------------------------")
							ConOut("A seguinte confirmacao nao pode ser alterada:")
							ConOut("Projeto: " + AllTrim(HAFF->AFF_PROJET))
							ConOut("Tarefa : " + AllTrim(HAFF->AFF_TAREFA))
							ConOut("Data   : " + Alltrim(HAFF->AFF_DATA))
							ConOut("Quant  : " + AllTrim(Str(HAFF->AFF_QUANT)))
							ConOut("---------------------------------------------")							
							
							// TODO
							// fazer log
					  EndIf
					  
					////////////////////////////
					// nova confirmacao
					////////////////////////////
					Case HAFF->AFF_SYNCFL == "N"
						Reclock("AFF", .T.)
			  				AFF->AFF_FILIAL := HAFF->AFF_FILIAL
			  				AFF->AFF_PROJET := HAFF->AFF_PROJET
			  				AFF->AFF_REVISA := HAFF->AFF_REVISA
			  				AFF->AFF_DATA   := CToD(HAFF->AFF_DATA)  // esta conversao e necessaria
			  				AFF->AFF_TAREFA := HAFF->AFF_TAREFA
			  				AFF->AFF_QUANT  := HAFF->AFF_QUANT
								//AFF->AFF_PERC   := HAFF->AFF_PERC
			  				AFF->AFF_OCORRE := HAFF->AFF_OCORRE
			  				AFF->AFF_CODMEM := HAFF->AFF_CODMEM
			  				AFF->AFF_USER   := PALMUSER->P_USERID
			  				AFF->AFF_CONFIR := HAFF->AFF_CONFIR
		  				MsUnlock()

						// inclusao
						PmsAvalAFF("AFF", 1)
						
					////////////////////////////
					// excluir confirmacao
					////////////////////////////
					Case HAFF->AFF_SYNCFL == "E"
						dbSelectArea("AFF")
						dbSetOrder(1)
					
						If MsSeek(HAFF->AFF_FILIAL + HAFF->AFF_PROJET + HAFF->AFF_REVISA + HAFF->AFF_TAREFA + DToS(CToD(HAFF->AFF_DATA)))
						
							// verifica se e possivel excluir
							If SoftLock("AFF") .And. MaCanAltAFF("AFF", .F.)
							
								// estorno
								PmsAvalAFF("AFF", 2)
								
								// exclusao
								PmsAvalAFF("AFF", 3)
							EndIf
						Else
						
						  // a confirmacao nao pode ser excluida
							ConOut("---------------------------------------------")
							ConOut("A seguinte confirmacao nao pode ser excluida:")
							ConOut("Projeto: " + AllTrim(HAFF->AFF_PROJET))
							ConOut("Tarefa : " + AllTrim(HAFF->AFF_TAREFA))
							ConOut("Data   : " + Alltrim(HAFF->AFF_DATA))
							ConOut("Quant  : " + AllTrim(Str(HAFF->AFF_QUANT)))
							ConOut("---------------------------------------------")							

							// TODO
							// fazer log
						EndIf					

					////////////////////////////
					// confirmacao ja processada
					////////////////////////////					
					Case HAFF->AFF_SYNCFL == "P"
						// se o registro ja foi processado "P",
						// nao realizar nenhuma operacao					
						
					Otherwise
						// caso o flag de sincronizacao
						// (AFF_SYNCFL) seja desconhecido, 
						// nao realizar nenhuma operacao					
					
				End
			EndIf

			Reclock("HAFF", .F.)
			
			// grava o ID do usuario no registro
			// lido do Palm
			HAFF->AFF_USER   := PALMUSER->P_USERID
			
			// marca o registro como processado
			HAFF->AFF_SYNCFL := "P"
			HAFF->(MsUnlock())

			HAFF->(dbSkip())		
		End

		// fecha o arquivo do Palm
		// os arquivos abertos do Palm devem 
		// ser sempre fechados	
		HAFF->(dbCloseArea())		
	EndIf	
Return( .T. )

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PExpConf  �Autor  �Adriano Ueda        � Data �  02/02/04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Rotina de exportacao                                       ���
���          �                                                            ���
���          � Exporta os registros para o Palm atraves do MCS.           ���
���          �                                                            ���
���          � Esta rotina le os registros da tabela AFF do AP,           ���
���          � verifica quais registros devem ser exportados e os copia   ���
���          � para o arquivo HAFF. Posteriormente, os arquivos           ���
���          � HAFF serao transmitidos para o Palm atraves do MCS.        ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Parametros� nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � Palm                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Function PExpConf()
	Local aConf := {}
	Local nAFFSyncID := 0
	Local nTamFilial:= IIf( lFWCodFil, FWGETTAMFILIAL, 2 )

 	//////////////////////
	// cria a tabela AFF
	//////////////////////
	aAdd(aConf, {"AFF_FILIAL", "C",  nTamFilial, 0})
	aAdd(aConf, {"AFF_PROJET", "C", 10, 0})
	aAdd(aConf, {"AFF_REVISA", "C",  2, 0})
	
	// o campo AFF_DATA e caracter propositalmente,
	// pois como ele participa da chave nao eh possivel
	// utilizar o tipo data
	aAdd(aConf, {"AFF_DATA  ", "C",  8, 0})
	//aAdd(aConf, {"AFF_DATA  ", "D",  "8", "0"})	
	
	aAdd(aConf, {"AFF_TAREFA", "C", 12, 0})
	aAdd(aConf, {"AFF_QUANT ", "N", 14, 4})
	//aAdd(aConf, {"AFF_PERC  ", "N",  6, 2})
	aAdd(aConf, {"AFF_OCORRE", "C",  2, 0})
	aAdd(aConf, {"AFF_CODMEM", "C",  6, 0})
	aAdd(aConf, {"AFF_USER  ", "C",  6, 0})
	aAdd(aConf, {"AFF_CONFIR", "C",  1, 0})
	aAdd(aConf, {"AFF_SYNCID", "C",  4, 0})
	aAdd(aConf, {"AFF_SYNCFL", "C",  1, 0})

	PAcertaSX3(@aConf)
	PalmCreate(aConf, "HAFF", "HAFF")
	
	ConOut( replicate("-",79) )
	ConOut("Exportando confirmacoes para usuario: " + AllTrim(PALMUSER->P_USER))
	ConOut( replicate("-",79) )
		
	dbSelectArea("AF8")
	dbSetOrder(1) 
	MsSeek(xFilial())

	// executa um loop em todos os projetos
	While AF8->(!EoF()) .And. AF8->AF8_FILIAL == AF8->(xFilial())

		// verifica se o projeto pode ser incluido
		// evento 91 - Inclusao de Confirmacao     
		If PmsVldFase("AF8", AF8->AF8_PROJET, "91")

			dbSelectArea("AF9")
			dbSetOrder(2)
			MsSeek(xFilial() + AF8->AF8_PROJET + PmsAF8Ver(AF8->AF8_PROJET))

			// executa um loop em todas as tarefas do projeto			
			While !AF9->(Eof()) .And. AF9->AF9_FILIAL + AF9->AF9_PROJET + AF9->AF9_REVISA == ;
			                          xFilial("AF9") +  AF8->AF8_PROJET + PmsAF8Ver(AF8->AF8_PROJET)
        
				// verifica se o usuario tem permissao para alterar a tarefa
				If PMSChkUser(AF9->AF9_PROJET, AF9->AF9_TAREFA,, AF9->AF9_EDTPAI, 1,;
				              "CONFIR", PmsAF8Ver(AF8->AF8_PROJET), PALMUSER->P_USERID)
	      	
			      	// recuperar os apontamentos ja efetuados para esta tarefa
			      	dbSelectArea("AFF")
			      	dbSetOrder(1)  // AFF_FILIAL+AFF_PROJET+AFF_REVISA+AFF_TAREFA+DTOS(AFF_DATA)
		
					MsSeek(xFilial("AFF") + AF9->AF9_PROJET + PmsAF8Ver(AF9->AF9_PROJET) + AF9->AF9_TAREFA)
		
			      	While !AFF->(Eof()) .And. AFF->AFF_FILIAL + AFF->AFF_PROJET + AFF->AFF_REVISA + AFF->AFF_TAREFA == ;
			      	                          xFilial("AFF")  + AF9->AF9_PROJET + PmsAF8Ver(AF9->AF9_PROJET) + AF9->AF9_TAREFA
		
			      		// gravar apontamento desta tarefa
			      		nAFFSyncID++
						
						dbSelectArea("HAFF")
						Reclock("HAFF", .T.)
						  
			  				HAFF->AFF_FILIAL := AFF->AFF_FILIAL
			  				HAFF->AFF_PROJET := AFF->AFF_PROJET
			  				HAFF->AFF_REVISA := AFF->AFF_REVISA
			  				HAFF->AFF_DATA   := DToC(AFF->AFF_DATA)  // esta conversao e necessaria
			  				HAFF->AFF_TAREFA := AFF->AFF_TAREFA
			  				HAFF->AFF_QUANT  := AFF->AFF_QUANT
			  				//HAFF->AFF_PERC   := AFF->AFF_PERC
			  				HAFF->AFF_OCORRE := AFF->AFF_OCORRE
			  				HAFF->AFF_CODMEM := AFF->AFF_CODMEM
			  				HAFF->AFF_USER   := AFF->AFF_USER
			  				HAFF->AFF_CONFIR := AFF->AFF_CONFIR
			  				HAFF->AFF_SYNCID := AllTrim(Str(nAFFSyncID))
			  				HAFF->AFF_SYNCFL := ""
		
				      	MsUnlock()
		
						ConOut("---- CONFIRMACAO ----")
						ConOut("Projeto: " + AFF->AFF_PROJET)
						ConOut("Tarefa : " + AFF->AFF_TAREFA)
						ConOut("Data   : " + DToC(AFF->AFF_DATA))
						ConOut("Quant  : " + AllTrim(Str(AFF->AFF_QUANT)))
						ConOut("---------------------")	      		
            
						// TODO
						// fazer log
		      	
	      				AFF->(dbSkip())
	      			End      	
				EndIf
	
				AF9->(dbSkip())		
			End
		EndIf
		
		AF8->(dbSkip())	
	End

	// fecha os arquivos aberto
	// do Palm  
	HAFF->(dbCloseArea())
Return( .T. )
	

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PConfTab  �Autor  �Adriano Ueda        � Data �  02/02/04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Indica os arquivos do AP a serem utilizados                ���
�������������������������������������������������������������������������͹��
���Parametros� nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Function PConfTab()
Return {"AF8", "AF9", "AFF"}


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PConfArq  �Autor  �Adriano Ueda        � Data �  02/02/04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Indica os arquivos do Palm a serem utilizados              ���
�������������������������������������������������������������������������͹��
���Parametros� nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Function PConfArq()
Return {"HAFF"}


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PConfInd  �Autor  �Adriano Ueda        � Data �  02/02/04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Indica os indices que serao utilizados pelo job do Palm    ���
�������������������������������������������������������������������������͹��
���Parametros� nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Function PConfInd()
Return {"AFF_SYNCID"}

/*
**************************************************************************************************************
**************************************************************************************************************
*/

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PExpPrj   �Autor  �Reynaldo Miyashita  � Data �  11.08.04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Rotina de exportacao                                       ���
���          �                                                            ���
���          � Exporta os registros para o Palm atraves do MCS.           ���
���          �                                                            ���
���          � Esta rotina le os registros da tabela AF9 do AP,           ���
���          � verifica quais registros devem ser exportados e os copia   ���
���          � para o arquivo HAF9. Posteriormente, os arquivos           ���
���          � HAF9 serao transmitidos para o Palm atraves do MCS.        ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Parametros� nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � Palm                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Function PExpPrj()
Local aTarefa 		:= {}
Local nAF9SyncID	:= 0
Local lFaseApp	 	:= .F.
Local lFaseConfir	:= .F.
Local lUserApp 	 	:= .F.
Local lUserConfir	:= .F.
Local nTamFilial	:= IIf( lFWCodFil, FWGETTAMFILIAL, 2 )

	//////////////////////  
	// cria a tabela AF9
	//////////////////////
	aAdd(aTarefa, {"AF9_FILIAL", "C",  nTamFilial, 0})
	aAdd(aTarefa, {"AF9_PROJET", "C", 10, 0})
	aAdd(aTarefa, {"AF9_REVISA", "C",  2, 0})
	aAdd(aTarefa, {"AF9_TAREFA", "C", 12, 0})
	aAdd(aTarefa, {"AF9_NIVEL ", "C",  3, 0})
	aAdd(aTarefa, {"AF9_DESCRI", "C", 90, 0})
	aAdd(aTarefa, {"AF9_UM    ", "C",  2, 0})
	aAdd(aTarefa, {"AF9_QUANT ", "N", 14, 2})
	aAdd(aTarefa, {"AF9_COMPOS", "C", 10, 0})
	aAdd(aTarefa, {"AF9_EMAIL ", "C",  1, 0})
	aAdd(aTarefa, {"AF9_GRPCOM", "C",  5, 0})
	aAdd(aTarefa, {"AF9_ORDEM ", "C",  3, 0})
	aAdd(aTarefa, {"AF9_APTREC", "N",  1, 0})
	aAdd(aTarefa, {"AF9_CONFIR", "N",  1, 0})
	aAdd(aTarefa, {"AF9_SYNCID", "C",  1, 0})

	PAcertaSX3(@aTarefa)
	PalmCreate(aTarefa, "HAF9", "HAF9")
	
	ConOut( replicate("-",79) )
	ConOut("Exportando Projetos e tarefas para usuario: " + AllTrim(PALMUSER->P_USER))
	ConOut( replicate("-",79) )
		
	dbSelectArea("AF8")
	dbSetOrder(1) 
	MsSeek(xFilial())

	// executa um loop em todos os projetos
	While AF8->(!EoF()) .And. AF8->AF8_FILIAL == AF8->(xFilial())

		dbSelectArea("AF9")
		dbSetOrder(2)
		MsSeek(xFilial() + AF8->AF8_PROJET + PmsAF8Ver(AF8->AF8_PROJET))

		// verifica se o projeto pode ser incluido
		// evento 86 - Inclusao de Apontamento de recursos
		// evento 91 - Inclusao de Confirmacao
		lFaseApp	:= PmsVldFase("AF8", AF8->AF8_PROJET, "86",.F.) 
		lFaseConfir := PmsVldFase("AF8", AF8->AF8_PROJET, "91",.F.)
		If lFaseApp	.OR. lFaseConfir
		
			ConOut("---- PROJETO - TAREFA ----")
			ConOut("Projeto: " + AF9->AF9_PROJET)
			// executa um loop em todas as tarefas do projeto			
			While !AF9->(Eof()) .And. AF9->AF9_FILIAL + AF9->AF9_PROJET + AF9->AF9_REVISA == ;
			                          xFilial("AF9") +  AF8->AF8_PROJET + PmsAF8Ver(AF8->AF8_PROJET)
	        
				lUserApp := PMSChkUser(AF9->AF9_PROJET, AF9->AF9_TAREFA,, AF9->AF9_EDTPAI, 3,"RECURS", PmsAF8Ver(AF8->AF8_PROJET), PALMUSER->P_USERID) 
				lUserConfir := PMSChkUser(AF9->AF9_PROJET, AF9->AF9_TAREFA,, AF9->AF9_EDTPAI, 1, "CONFIR", PmsAF8Ver(AF8->AF8_PROJET), PALMUSER->P_USERID)
				
				// verifica se o usuario tem permissao para alterar a tarefa no
				//  RECURS - Apontamento de recursos
				//  CONFIR - Confirmacao
				If lUserApp .OR. lUserConfir
				   
			  		// adicionar ao conjunto de tarefas que podem ser apontadas
					nAF9SyncID++
					      	
					dbSelectArea("HAF9")
					Reclock("HAF9", .T.)
					
					HAF9->AF9_FILIAL := AF9->AF9_FILIAL
					HAF9->AF9_PROJET := AF9->AF9_PROJET
					HAF9->AF9_REVISA := AF9->AF9_REVISA
					HAF9->AF9_TAREFA := AF9->AF9_TAREFA
					HAF9->AF9_NIVEL  := AF9->AF9_NIVEL
					HAF9->AF9_DESCRI := AF9->AF9_DESCRI
					HAF9->AF9_UM     := AF9->AF9_UM
					HAF9->AF9_QUANT  := AF9->AF9_QUANT
					HAF9->AF9_COMPOS := AF9->AF9_COMPOS
					HAF9->AF9_EMAIL  := AF9->AF9_EMAIL
					HAF9->AF9_GRPCOM := AF9->AF9_GRPCOM
					HAF9->AF9_ORDEM  := AF9->AF9_ORDEM

					HAF9->AF9_APTREC := Iif(lFaseApp .AND. lUserApp,1,0 )
					HAF9->AF9_CONFIR := Iif(lFaseConfir .AND. lUserConfir,1,0 )

					HAF9->AF9_SYNCID := AllTrim(Str(nAF9SyncID))
					
					MsUnlock()
			      	
					ConOut("Tarefa : " + AF9->AF9_TAREFA)
				EndIf
				AF9->(dbSkip())		
				
			End
			ConOut("---------------------")
	
		EndIF
		AF8->(dbSkip())	
		
	End

	// fecha os arquivos aberto
	// do Palm  
	HAF9->(dbCloseArea())
	
Return( .T. )
	

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PPrjTab   �Autor  �Reynaldo Miyashita  � Data �  11.08.04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Indica os arquivos do AP a serem utilizados                ���
�������������������������������������������������������������������������͹��
���Parametros� nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Function PPrjTab()
Return {"AF8", "AF9" }


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PPrjArq   �Autor  �Reynaldo Miyashita  � Data �  11.08.04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Indica os arquivos do Palm a serem utilizados              ���
�������������������������������������������������������������������������͹��
���Parametros� nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Function PPrjArq()
Return {"HAF9"}


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PPrjInd  �Autor  �Reynaldo Miyashita  � Data �  11.08.04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Indica os indices que serao utilizados pelo job do Palm    ���
�������������������������������������������������������������������������͹��
���Parametros� nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Function PPrjInd()
Return {"AF9_FILIAL+AF9_PROJET+AF9_REVISA+AF9_TAREFA+AF9_SYNCID"}
