/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PExpAE8   �Autor  �Reynaldo Miyashita  � Data �  11.08.04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Rotina de exportacao                                       ���
���          �                                                            ���
���          � Exporta os registros para o Palm atraves do MCS.           ���
���          �                                                            ���
���          � Esta rotina le os registros das tabelas AE8 e SH7 do AP,   ���
���          � verifica quais registros devem ser exportados e os copia   ���
���          � para o arquivo HAE8 e SH7. Posteriormente, os arquivos     ���
���          � HAE8 serao transmitidos para o Palm atraves do MCS.        ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Parametros� nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � Palm                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function PExpAE8()
	Local aRecurso 	  	:= {}  // estrutura do arquivo AE8 no Palm
	Local aCalendario 	:= {}  // estrutura do arquivo SH7 no Palm
	Local aExcecao    	:= {}  // estrutura do arquivo AFY no Palm
	Local aParametro  	:= {}  // estrutura do arquivo SH6 no Palm
	Local aCodCalen   	:= {}
	Local nAE8SyncID  	:= 0   // id do recurso, utilizado na sinc. com o Palm
	Local nSH7SyncID  	:= 0   // id do calend�rio, utilizado na sinc. com o Palm
	Local nC          	:= 0
	Local lHasDataF
	Local lFWCodFil 	:= FindFunction("FWCodFil")
	Local nTamFilial	:= IIf( lFWCodFil, FWGETTAMFILIAL, 2 )
	
	dbSelectArea("AFY")
	lHasDataF := .T.

	////////////////////////////////////////////
	// cria a tabela AE8 - recursos
	////////////////////////////////////////////
	aAdd(aRecurso, {"AE8_FILIAL", "C", nTamFilial, 0})
	aAdd(aRecurso, {"AE8_RECURS", "C", 15, 0})
	aAdd(aRecurso, {"AE8_DESCRI", "C", 30, 0})
	aAdd(aRecurso, {"AE8_CALEND", "C", 03, 0})
	aAdd(aRecurso, {"AE8_CONFIR", "C", 01, 0})
	aAdd(aRecurso, {"AE8_APTMRE", "C", 01, 0})
	aAdd(aRecurso, {"AE8_SYNCID", "C", 01, 0})

	PAcertaSX3(@aRecurso)
	PalmCreate(aRecurso, "HAE8", "HAE8")
	
	////////////////////////////////////////////
	// cria a tabela SH7 - calendario
	////////////////////////////////////////////
	aAdd(aCalendario, {"SH7_FILIAL", "C",  nTamFilial, 0})
	aAdd(aCalendario, {"SH7_CODIGO", "C",  03, 0})
	aAdd(aCalendario, {"SH7_ALOC",   "C", 112, 0}) // tamanho do SH7_ALOC, considerando a codif. base64
	aAdd(aCalendario, {"SH7_DESCRI", "C",  15, 0})
	aAdd(aCalendario, {"SH7_SYNCID", "C",   1, 0})

	PAcertaSX3(@aCalendario)
	PalmCreate(aCalendario, "HSH7", "HSH7")
	
	////////////////////////////////////////////
	// cria a tabela AFY - excecao ao calendario
	////////////////////////////////////////////
	aAdd(aExcecao, {"AFY_FILIAL", "C",  nTamFilial, 0})
	aAdd(aExcecao, {"AFY_RECURS", "C",  15, 0})
	aAdd(aExcecao, {"AFY_PROJET", "C",  10, 0})
	aAdd(aExcecao, {"AFY_DATA"  , "D",  08, 0})
	aAdd(aExcecao, {"AFY_ALOCA" , "C", 128, 0}) // tamanho do AFY_ALOCA, considerando a codif. base64
	aAdd(aExcecao, {"AFY_MALOCA", "C", 128, 0}) // tamanho do AFY_MALOCA, considerando a codif. base64
	If lHasDataF
		aAdd(aExcecao, {"AFY_DATAF" , "D",  08, 0})
	EndIf
	
	PAcertaSX3(@aExcecao)
	PalmCreate(aExcecao, "HAFY", "HAFY")
	
	////////////////////////////////////////////
	// cria a tabela SX6 - parametros do sistema
	////////////////////////////////////////////
	aAdd(aParametro, {"X6_FIL"    , "C",  nTamFilial, 0})
	aAdd(aParametro, {"X6_VAR"    , "C",  10, 0})
	aAdd(aParametro, {"X6_TIPO"   , "C",  01, 0})
	aAdd(aParametro, {"X6_CONTEUD", "C",  50, 0})

	PAcertaSX3(@aParametro)
	PalmCreate(aParametro, "HSX6", "HSX6")
	
	ConOut(Replicate("-",79) )
	ConOut("Exportando Recursos e seus respectivos calendarios")
	ConOut(Replicate("-",79) )
		
	dbSelectArea("AE8")
	dbSetOrder(1) 
	MsSeek(xFilial())

	// executa um loop em todos os projetos
	While AE8->(!EoF()) .And. AE8->AE8_FILIAL == AE8->(xFilial())

		nAE8SyncID++
		      	
		dbSelectArea("HAE8")
		Reclock("HAE8", .T.)
		
		HAE8->AE8_FILIAL := AE8->AE8_FILIAL
		HAE8->AE8_RECURS := AE8->AE8_RECURS
		HAE8->AE8_DESCRI := AE8->AE8_DESCRI
		HAE8->AE8_CALEND := AE8->AE8_CALEND
		HAE8->AE8_CONFIR := AE8->AE8_CONFIR
		HAE8->AE8_APTMRE := AE8->AE8_APTMRE
		HAE8->AE8_SYNCID := AllTrim(Str(nAE8SyncID))
		
		MsUnlock()
		
		dbSelectArea("SH7")
		dbSetOrder(1)
		MsSeek(xFilial() + AE8->AE8_CALEND)

		If aScan(aCodCalen, AE8->AE8_CALEND) == 0
			aAdd(aCodCalen, AE8->AE8_CALEND)
		EndIf
		
		ConOut("------ Recurso ------")
		ConOut("Recurso : " + AllTrim(AE8->AE8_RECURS) + " : " + AllTrim(AE8->AE8_DESCRI) )
		ConOut("---------------------")
			
		AE8->(dbSkip())	
	End

	If Len(aCodCalen) > 0 
		ConOut("---- Calendario ----")

		For nC := 1 to len(aCodCalen)
			dbSelectArea("SH7")
			dbSetOrder(1)
			If MsSeek(xFilial() + aCodCalen[nC])
				ConOut("Calendario : " + AllTrim(SH7->H7_CODIGO) + " : " + Alltrim(SH7->H7_DESCRI) )

				nSH7SyncID++
				
				Reclock("HSH7", .T.)
					HSH7->SH7_FILIAL := SH7->H7_FILIAL
					HSH7->SH7_CODIGO := SH7->H7_CODIGO
					HSH7->SH7_ALOC   := B64Code(SH7->H7_ALOC)
					HSH7->SH7_DESCRI := SH7->H7_DESCRI
					HSH7->SH7_SYNCID := AllTrim(Str(nSH7SyncID))
				MsUnlock()
			EndIf
		Next

		ConOut("--------------------")
	EndIf
	
	dbSelectArea("AFY")
	dbSetOrder(2)
	MsSeek(xFilial())
	
	ConOut("---- Excecao ao Calendario ----")
	// executa um loop em todas as excecao ao calendario
	While AFY->(!EoF()) .And. AFY->AFY_FILIAL == AFY->(xFilial())
		ConOut("Excecao : " + AllTrim(DToC(AFY->AFY_DATA)) )
		If ! Empty( AFY->AFY_PROJET )
			ConOut("     Projeto : " + AllTrim(DToC(AFY->AFY_PROJET)) )
		EndIf
		If ! Empty( AFY->AFY_RECURS )
			ConOut("     Recurso : " + AllTrim(AFY->AFY_RECURS) )
		EndIf

		Reclock("HAFY", .T.)
			HAFY->AFY_FILIAL := AFY->AFY_FILIAL 
			HAFY->AFY_PROJET := AFY->AFY_PROJET
			HAFY->AFY_RECURS := AFY->AFY_RECURS
			HAFY->AFY_DATA   := AFY->AFY_DATA 
			HAFY->AFY_ALOCA  := B64Code(AFY->AFY_ALOC)
			HAFY->AFY_MALOCA := B64Code(AFY->AFY_MALOC)
			If lHasDataF
				HAFY->AFY_DATAF  := AFY->AFY_DATAF
			EndIf
		MsUnlock()
		AFY->(DbSkip())
	End
	ConOut("-------------------------------")
	
//	ConOut("---- Parametros ----")
	dbSelectArea("SX6")
	dbSetOrder(1)

	// executa um loop em todas as exce��es ao calend�rio
	If SX6->(MsSeek(xFilial() + "MV_PRECISA"))
		Reclock("HSX6", .T.)
			HSX6->X6_FIL     := SX6->X6_FIL
			HSX6->X6_VAR     := SX6->X6_VAR
			HSX6->X6_TIPO    := SX6->X6_TIPO
			HSX6->X6_CONTEUD := SX6->X6_CONTEUD 
		MsUnlock()
	EndIf
//	ConOut("------------------------")
	
	// fecha os arquivos abertos do Palm
	HAE8->(dbCloseArea())
	HSH7->(dbCloseArea())
	HAFY->(dbCloseArea())
	HSX6->(dbCloseArea())
Return .T.
	

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PAE8Tab   �Autor  �Reynaldo Miyashita  � Data �  11.08.04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Indica os arquivos do AP a serem utilizados                ���
�������������������������������������������������������������������������͹��
���Parametros� nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function PAE8Tab()
Return {"AE8", "SH7" ,"AFY" ,"SX6"}


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PAE8Arq   �Autor  �Reynaldo Miyashita  � Data �  11.08.04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Indica os arquivos do Palm a serem utilizados              ���
�������������������������������������������������������������������������͹��
���Parametros� nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function PAE8Arq()
Return {"HAE8", "HSH7", "HAFY", "HSX6"}


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PAE8Ind  �Autor  �Reynaldo Miyashita  � Data �  11.08.04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Indica os indices que serao utilizados pelo job do Palm    ���
�������������������������������������������������������������������������͹��
���Parametros� nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function PAE8Ind()
Return {"AE8_FILIAL+AE8_RECURS+AE8_SYNCID", "SH7_FILIAL+SH7_CODIGO", "AFY_FILIAL+dtos(AFY_DATA)", "X6_FIL+X6_VAR"}
