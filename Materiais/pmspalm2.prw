/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PImpApRec �Autor  �Reynaldo Miyashita  � Data �  09.08.04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Rotina de importacao   .                                   ���
���          �                                                            ���
���          � - importa os registros recebidos do Palm pelo MCS          ���
�������������������������������������������������������������������������͹��
���Parametros� nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � Palm                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function PImpApRec()

Local aArquivos :=  {} //{"HAF9", "HAF9", "AF9_FILIAL + AF9_PROJET + AF9_REVISA + AF9_TAREFA"}}
Local cPathPalm := GetSrvProfString("HandHeldDir","\HANDHELD\") + "P" + AllTrim(PALMUSER->P_DIR) + "\atual\"

	aAdd(aArquivos, {"HAFU", "HAFU", "AFU_FILIAL+AFU_PROJET+AFU_REVISA+AFU_TAREFA+AFU_RECURS+AFU_DATA"})
	
	ConOut( replicate("-",79 ) )
	ConOut("Importando apontamento de recursos do usuario: " + AllTrim(PALMUSER->P_USER))
	ConOut( replicate("-",79) )
	
	// usar a funcao PChkFile() para abrir os arquivos
	If PChkFile(cPathPalm, aArquivos)
		dbSelectArea("HAFU")
		dbSetOrder(1)
		
		HAFU->(dbGoTop())
		
		While !HAFU->(Eof())
			// verifica se o projeto pode ser incluido
			// evento 86 - Inclusao de Apontamento de recursos
			If PmsVldFase("AF8", HAFU->AFU_PROJET, "86")
				Do Case

					////////////////////////////
					// alterar apontamento
					////////////////////////////
					Case HAFU->AFU_SYNCFL == "A"
						dbSelectArea("AFU")
						dbSetOrder(1)
					
						If MsSeek(HAFU->AFU_FILIAL + "1" + HAFU->AFU_PROJET + HAFU->AFU_REVISA + HAFU->AFU_TAREFA + HAFU->AFU_RECURS+DToS(CToD(HAFU->AFU_DATA)))
							///
							// VERIFICAR
							///
							If SoftLock("AFU") 

								// estorno
								PMSAvalAFU("AFU", 2)

								ConOut("---------------------------------------------")
								ConOut("Alterando o apontamento:")
								ConOut("Projeto  : " + AllTrim(HAFU->AFU_PROJET))
								ConOut("Tarefa   : " + AllTrim(HAFU->AFU_TAREFA))
								ConOut("Recuso   : " + Alltrim(HAFU->AFU_RECURS))
								ConOut("Data     : " + Alltrim(HAFU->AFU_DATA))
								ConOut("Qtd.Horas: " + AllTrim(Str(HAFU->AFU_HQUANT)))
								ConOut("---------------------------------------------")							
							
								Reclock("AFU", .F.)
									/*
					  				AFU->AFU_FILIAL := HAFU->AFU_FILIAL
					  				AFU->AFU_PROJET := HAFU->AFU_PROJET
					  				AFU->AFU_REVISA := HAFU->AFU_REVISA
					  				AFU->AFU_TAREFA := HAFU->AFU_TAREFA
					  				AFU->AFU_DATA   := CToD(HAFU->AFU_DATA)  // esta conversao e necessaria
					  				*/
					  				AFU->AFU_HORAI  := HAFU->AFU_HORAI
					  				AFU->AFU_HORAF  := HAFU->AFU_HORAF				  		
					  				AFU->AFU_HQUANT := HAFU->AFU_HQUANT
					  				
				  				MsUnlock()
			  				
				  				// inclusao
			  					PmsAvalAFU("AFU", 1)
			  				Else
							  // a confirmacao nao pode ser alterada
								ConOut("--------------------------------------------------------")
								ConOut("O seguinte apontamento de recurso nao pode ser alterado:")
								ConOut("Projeto  : " + AllTrim(HAFU->AFU_PROJET))
								ConOut("Tarefa   : " + AllTrim(HAFU->AFU_TAREFA))
								ConOut("Recurso  : " + Alltrim(HAFU->AFU_RECURS))
								ConOut("Data     : " + Alltrim(HAFU->AFU_DATA))
								ConOut("Qtd.Horas: " + AllTrim(Str(HAFU->AFU_HQUANT)))
								ConOut("--------------------------------------------------------")
								
								// TODO
								// fazer log
			  				EndIf
						Else
						  	// O apontamento confirmacao nao pode ser alterada
							ConOut("-----------------------------------------------------------------------")
							ConOut("O seguinte apontamento de recurso nao foi encontrado para ser alterado:")
							ConOut("Projeto  : " + AllTrim(HAFU->AFU_PROJET))
							ConOut("Tarefa   : " + AllTrim(HAFU->AFU_TAREFA))
							ConOut("Recurso  : " + Alltrim(HAFU->AFU_RECURS))
							ConOut("Data     : " + Alltrim(HAFU->AFU_DATA))
							ConOut("Qtd.Horas: " + AllTrim(Str(HAFU->AFU_HQUANT)))
							ConOut("-----------------------------------------------------------------------")
							
							// TODO
							// fazer log
						EndIf
					  
					////////////////////////////
					// nova confirmacao
					////////////////////////////
					Case HAFU->AFU_SYNCFL == "N"
						Reclock("AFU", .T.)
						AFU->AFU_FILIAL := HAFU->AFU_FILIAL
						AFU->AFU_CTRRVS	:= "1"
						AFU->AFU_RECURS := HAFU->AFU_RECURS
						AFU->AFU_PROJET := HAFU->AFU_PROJET
						AFU->AFU_REVISA := HAFU->AFU_REVISA
						AFU->AFU_DATA   := ctod(HAFU->AFU_DATA)
						AFU->AFU_TAREFA := HAFU->AFU_TAREFA						
						AFU->AFU_HORAI  := HAFU->AFU_HORAI
						AFU->AFU_HORAF  := HAFU->AFU_HORAF
						AFU->AFU_HQUANT := HAFU->AFU_HQUANT
		  				MsUnlock()

						// inclusao	  				
						PmsAvalAFU("AFU", 1)
					
					////////////////////////////
					// excluir apontamentos
					////////////////////////////
					Case HAFU->AFU_SYNCFL == "E"
						dbSelectArea("AFU")
						dbSetOrder(1)
					
 						If MsSeek(HAFU->AFU_FILIAL + "1" + HAFU->AFU_PROJET + HAFU->AFU_REVISA + HAFU->AFU_TAREFA + HAFU->AFU_RECURS + DToS(CToD(HAFU->AFU_DATA)))
 						
							ConOut("---------------------------------------------")
							ConOut("Excluindo o apontamento:")
							ConOut("Projeto  : " + AllTrim(HAFU->AFU_PROJET))
							ConOut("Tarefa   : " + AllTrim(HAFU->AFU_TAREFA))
							ConOut("Recuso   : " + Alltrim(HAFU->AFU_RECURS))
							ConOut("Data     : " + Alltrim(HAFU->AFU_DATA))
							ConOut("Qtd.Horas: " + AllTrim(Str(HAFU->AFU_HQUANT)))
							ConOut("---------------------------------------------")

							PmsAvalAFU("AFU",2)
							PmsAvalAFU("AFU",3)
														
						Else
						
						  	// a confirmacao nao pode ser excluida
							ConOut("-----------------------------------------------------------------------")
							ConOut("O seguinte apontamento de recurso nao foi encontrado para ser excluido:")
							ConOut("Projeto  : " + AllTrim(HAFU->AFU_PROJET))
							ConOut("Tarefa   : " + AllTrim(HAFU->AFU_TAREFA))
							ConOut("Recurso  : " + Alltrim(HAFU->AFU_RECURS))
							ConOut("Data     : " + Alltrim(HAFU->AFU_DATA))
							ConOut("Qtd.Horas: " + AllTrim(Str(HAFU->AFU_HQUANT)))
							ConOut("-----------------------------------------------------------------------")

							// TODO
							// fazer log
						EndIf

					////////////////////////////
					// confirmacao ja processada
					////////////////////////////
					Case HAFU->AFU_SYNCFL == "P"
						// se o registro ja foi processado "P",
						// nao realizar nenhuma operacao
						
					Otherwise
						// caso o flag de sincronizacao
						// (AFU_SYNCFL) seja desconhecido, 
						// nao realizar nenhuma operacao
					
				End
			EndIf

			Reclock("HAFU", .F.)
				// marca o registro como processado
				HAFU->AFU_SYNCFL := "P"
			HAFU->(MsUnlock())

			HAFU->(dbSkip())		
		End

		// fecha o arquivo do Palm
		// os arquivos abertos do Palm devem 
		// ser sempre fechados	
		HAFU->(dbCloseArea())
	Else
		ConOut("-------------------------------------------------------------------")
		ConOut("N�o foi possivel abrir os arquivos para importacao dos apontamentos")
		ConOut("-------------------------------------------------------------------")
	EndIf
	
Return( .T. )

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PExpApRec �Autor  �Reynaldo Miyashita   � Data �  09.08.04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Rotina de exportacao                                       ���
���          �                                                            ���
���          � Exporta os registros para o Palm atraves do MCS.           ���
���          �                                                            ���
���          � Esta rotina le os registros da tabela AFU do AP,           ���
���          � verifica quais registros devem ser exportados e os copia   ���
���          � para os arquivos HAFU. Posteriormente, os arquivos         ���
���          � HAFU serao transmitidos para o Palm atraves do MCS.        ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Parametros� nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � Palm                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function PExpApRec()
Local aConf   := {}
Local nAFUSyncID := 0
Local lFWCodFil := FindFunction("FWCodFil")
Local nTamFilial:= IIf( lFWCodFil, FWGETTAMFILIAL, 2 )
	
	aAdd(aConf, {"AFU_FILIAL", "C",  nTamFilial, 0})
	aAdd(aConf, {"AFU_PROJET", "C", 10, 0})
	aAdd(aConf, {"AFU_REVISA", "C", 04, 0})

	aAdd(aConf, {"AFU_TAREFA", "C", 12, 0})
	aAdd(aConf, {"AFU_RECURS", "C", 15, 0})
	
	// o campo AFU_DATA e caracter propositalmente,
	// pois como ele participa da chave nao eh possivel
	// utilizar o tipo data
	aAdd(aConf, {"AFU_DATA", "C",  8, 0})
	
	aAdd(aConf, {"AFU_HORAI",  "C",  5, 0})
	aAdd(aConf, {"AFU_HORAF",  "C",  5, 0})
	aAdd(aConf, {"AFU_HQUANT", "N", 12, 2})
	aAdd(aConf, {"AFU_SYNCID", "C",  4, 0})
	aAdd(aConf, {"AFU_SYNCFL", "C",  1, 0})

	PAcertaSX3(@aConf)
	PalmCreate(aConf, "HAFU", "HAFU")
		
	ConOut( replicate("-",79) )
	ConOut("Exportando apontamentos do usuario: " + AllTrim(PALMUSER->P_USER))
	ConOut( replicate("-",79) )
		
	dbSelectArea("AF8")
	dbSetOrder(1) 
	MsSeek(xFilial())

	// executa um loop em todos os projetos
	While AF8->(!EoF()) .And. AF8->AF8_FILIAL == AF8->(xFilial())

		// verifica se o projeto pode ser incluido
		// evento 86 - Inclusao de Apontamento de recursos
		If PmsVldFase("AF8", AF8->AF8_PROJET, "86")

			dbSelectArea("AF9")
			dbSetOrder(2)
			MsSeek(xFilial() + AF8->AF8_PROJET + PmsAF8Ver(AF8->AF8_PROJET))

			// executa um loop em todas as tarefas do projeto
			While !AF9->(Eof()) .And. AF9->AF9_FILIAL + AF9->AF9_PROJET + AF9->AF9_REVISA == ;
			                          xFilial("AF9") + AF8->AF8_PROJET + PmsAF8Ver(AF8->AF8_PROJET)
        
				// verifica se o usuario tem permissao para alterar a tarefa
				If PMSChkUser(AF9->AF9_PROJET, AF9->AF9_TAREFA,, AF9->AF9_EDTPAI, 3,;
				              "RECURS", PmsAF8Ver(AF8->AF8_PROJET), PALMUSER->P_USERID)
 
			      	// recuperar os apontamentos ja efetuados para esta tarefa
			      	dbSelectArea("AFU")
			      	dbSetOrder(1)  // 
		
					MsSeek(xFilial("AFU") + "1" + AF9->AF9_PROJET + PmsAF8Ver(AF9->AF9_PROJET) + AF9->AF9_TAREFA)
		
			      	While !AFU->(Eof()) .And. AFU->AFU_FILIAL + AFU->AFU_PROJET + AFU->AFU_REVISA + AFU->AFU_TAREFA == ;
			      	                          xFilial("AFU")  + AF9->AF9_PROJET + PmsAF8Ver(AF9->AF9_PROJET) + AF9->AF9_TAREFA
		
			      		// gravar apontamento desta tarefa
			      		nAFUSyncID++
						
						dbSelectArea("HAFU")
						Reclock("HAFU", .T.)
						  
							HAFU->AFU_FILIAL := AFU->AFU_FILIAL
							HAFU->AFU_PROJET := AFU->AFU_PROJET
							HAFU->AFU_REVISA := AFU->AFU_REVISA
							HAFU->AFU_DATA   := DToC(AFU->AFU_DATA)
							HAFU->AFU_TAREFA := AFU->AFU_TAREFA 
							HAFU->AFU_RECURS := AFU->AFU_RECURS 
							HAFU->AFU_HORAI  := AFU->AFU_HORAI  
							HAFU->AFU_HORAF  := AFU->AFU_HORAF  
							HAFU->AFU_HQUANT := AFU->AFU_HQUANT 
				  			HAFU->AFU_SYNCID := AllTrim(Str(nAFUSyncID))
			  				HAFU->AFU_SYNCFL := ""
		
				      	MsUnlock()
		
						ConOut("---- APONTAMENTO ----")
						ConOut("Projeto  : " + AllTrim(AFU->AFU_PROJET))
						ConOut("Tarefa   : " + AllTrim(AFU->AFU_TAREFA))
						ConOut("Recurso  : " + Alltrim(AFU->AFU_RECURS))
						ConOut("Data     : " + Alltrim( DToC(AFU->AFU_DATA) ))
						ConOut("Qtd.Horas: " + AllTrim(Str(AFU->AFU_HQUANT)))
						ConOut("---------------------")
            
						// TODO
						// fazer log
		      	
	      				AFU->(dbSkip())
	      			End
				EndIf
	
				AF9->(dbSkip())
			End
		EndIf
		
		AF8->(dbSkip())	
	End

	// fecha os arquivos aberto
	// do Palm  
	HAFU->(dbCloseArea())
	
Return( .T. )


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PApRecTab �Autor  �Reynaldo Miyashita   � Data �  09.08.04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Indica os arquivos do AP a serem utilizados                ���
�������������������������������������������������������������������������͹��
���Parametros� nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function PApRecTab()
Return( {"AF8", "AF9", "AFU"} )


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PApRecArq �Autor  �Reynaldo Miyashita  � Data �  09.08.04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Indica os arquivos do Palm a serem utilizados              ���
�������������������������������������������������������������������������͹��
���Parametros� nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function PApRecArq()
Return( {"HAFU"} )


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PApRecInd �Autor  �Reynaldo Miyashita   � Data �  02/02/04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Indica os indices que serao utilizados pelo job do Palm    ���
�������������������������������������������������������������������������͹��
���Parametros� nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function PApRecInd()
Return( {"AFU_SYNCID"} )

