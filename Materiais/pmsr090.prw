#include "Protheus.ch"
#include "pmsr090.ch"
#DEFINE CHRCOMP If(aReturn[4]==1,15,18)

//--------------------------RELEASE 4-------------------------------------------//
Function PMSR090()
Local aArea		:= GetArea()

Private nQtdIns := 0

If PMSBLKINT()
	Return Nil
EndIf

oReport := ReportDef()
Pergunte(oReport:uParam,.F.)
oReport:PrintDialog()

RestArea(aArea)
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �ReportDef � Autor �Paulo Carnelossi       � Data �21/06/2006���
�������������������������������������������������������������������������Ĵ��
���Descri��o �A funcao estatica ReportDef devera ser criada para todos os ���
���          �relatorios que poderao ser agendados pelo usuario.          ���
���          �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �ExpO1: Objeto do relat�rio                                  ���
�������������������������������������������������������������������������Ĵ��
���Parametros�Nenhum                                                      ���
���          �                                                            ���
�������������������������������������������������������������������������Ĵ��
���   DATA   � Programador   �Manutencao efetuada                         ���
�������������������������������������������������������������������������Ĵ��
���          �               �                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ReportDef()

Local oReport
Local oRelOrdem

Local oProjeto
Local oRecurso
Local oEquipe
Local oGrupo
Local oRecurso5
Local oTarefa5
Local oTarefa
Local oTarefa2
Local oTrfCU01
Local oTrfCU02
Local cObfNRecur := IIF(FATPDIsObfuscate("AE8_DESCRI",,.T.),FATPDObfuscate("RESOURCE NAME","AE8_DESCRI",,.T.),"")  

Local aOrdem := {}

//������������������������������������������������������������������������Ŀ
//�Criacao do componente de impressao                                      �
//�                                                                        �
//�TReport():New                                                           �
//�ExpC1 : Nome do relatorio                                               �
//�ExpC2 : Titulo                                                          �
//�ExpC3 : Pergunte                                                        �
//�ExpB4 : Bloco de codigo que sera executado na confirmacao da impressao  �
//�ExpC5 : Descricao                                                       �
//�                                                                        �
//��������������������������������������������������������������������������
oReport := TReport():New("PMSR090",STR0002,"PMR90A", ;
			{|oReport| ReportPrint(oReport)},;
			STR0001 )

//STR0001 "Este relatorio ira imprimir os detalhes da alocacao dos recursos nos projetos no periodo solicitado."
//STR0002 "Alocacao de Recursos"

oReport:SetLandScape()
//������������������������������������������������������������������������Ŀ
//�Criacao da secao utilizada pelo relatorio                               �
//�                                                                        �
//�TRSection():New                                                         �
//�ExpO1 : Objeto TReport que a secao pertence                             �
//�ExpC2 : Descricao da se�ao                                              �
//�ExpA3 : Array com as tabelas utilizadas pela secao. A primeira tabela   �
//�        sera considerada como principal para a se��o.                   �
//�ExpA4 : Array com as Ordens do relat�rio                                �
//�ExpL5 : Carrega campos do SX3 como celulas                              �
//�        Default : False                                                 �
//�ExpL6 : Carrega ordens do Sindex                                        �
//�        Default : False                                                 �
//�                                                                        �
//��������������������������������������������������������������������������
//adiciona ordens do relatorio
aAdd(aOrdem, STR0003 )  //"PROJETO+TAREFA"
aAdd(aOrdem, STR0004 )  //"RECURSO+DATA DE ALOCACAO"
aAdd(aOrdem, STR0012 )  //"EQUIPE+DATA DE ALOCACAO"
aAdd(aOrdem, STR0020 )  //"PROJETO+GRUPO DE TAREFAS+TAREFA+RECURSO"
aAdd(aOrdem, STR0032 )  //"REC+PRJ+TRF+DATA"

//������������������������������������������������������������������������Ŀ
//�Criacao da celulas da secao do relatorio                                �
//�                                                                        �
//�TRCell():New                                                            �
//�ExpO1 : Objeto TSection que a secao pertence                            �
//�ExpC2 : Nome da celula do relat�rio. O SX3 ser� consultado              �
//�ExpC3 : Nome da tabela de referencia da celula                          �
//�ExpC4 : Titulo da celula                                                �
//�        Default : X3Titulo()                                            �
//�ExpC5 : Picture                                                         �
//�        Default : X3_PICTURE                                            �
//�ExpC6 : Tamanho                                                         �
//�        Default : X3_TAMANHO                                            �
//�ExpL7 : Informe se o tamanho esta em pixel                              �
//�        Default : False                                                 �
//�ExpB8 : Bloco de c�digo para impressao.                                 �
//�        Default : ExpC2                                                 �
//�                                                                        �
//��������������������������������������������������������������������������


oProjeto := TRSection():New(oReport,STR0031+ "1: "+ STR0025,{"AF8" },aOrdem,.F.,.F.) //"Projeto"
TRCell():New(oProjeto,	"AF8_PROJET"	,"AF8",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF8->AF8_PROJET })
TRCell():New(oProjeto,	"AF8_DESCRI"	,"AF8",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF8->AF8_DESCRI })
oProjeto:SetLineStyle()

oTarefa := TRSection():New(oProjeto, STR0026,{"AF9", "AFA","AE8","AED","SB1"},/*aOrdem*/,.F.,.F.) //"Tarefa"
TRCell():New(oTarefa,	"AF9_TAREFA"	,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_TAREFA })
TRCell():New(oTarefa,	"AF9_DESCRI"	,"AF9",/*Titulo*/,/*Picture*/,25/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_DESCRI })
TRCell():New(oTarefa,	"AFA_RECURS"	,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_RECURS })
TRCell():New(oTarefa,	"AE8_DESCRI"	,"AE8",/*Titulo*/,/*Picture*/,25/*Tamanho*/,/*lPixel*/,{|| IIF(Empty(cObfNRecur), AE8->AE8_DESCRI,cObfNRecur) })
TRCell():New(oTarefa,	"AE8_EQUIP"		,"AE8",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AE8->AE8_EQUIP })
TRCell():New(oTarefa,	"AED_DESCRI"	,"AED",/*Titulo*/,/*Picture*/,20/*Tamanho*/,/*lPixel*/,{|| AED->AED_DESCRI })
TRCell():New(oTarefa,	"AFA_START"		,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_START })
TRCell():New(oTarefa,	"AFA_HORAI"		,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_HORAI })
TRCell():New(oTarefa,	"AFA_FINISH"	,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_FINISH })
TRCell():New(oTarefa,	"AFA_HORAF"		,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_HORAF })
TRCell():New(oTarefa,	"AFA_ALOC"		,"AFA",/*Titulo*/,"@E 999.99"/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_ALOC })
TRCell():New(oTarefa,	"AFA_QUANT"		,"AFA",/*Titulo*/,"@E 999999999.99"/*Picture*/,/*Tamanho*/,/*lPixel*/,/*{|| }*/)
TRCell():New(oTarefa,	"B1_UM"		,"SB1",STR0023/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{||If(!Empty(AFA->AFA_PRODUT),SB1->B1_UM,"")}) //"UM"
TRCell():New(oTarefa,	"AFA_CUSTD"		,"AFA",STR0017/*Titulo*/,/*Picture*/,5/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_CUSTD })  //"Custo Prv."
TRCell():New(oTarefa,	"AFA_CUSTD1"	,"AFA",STR0018/*Titulo*/,"@E 999,999,999,999.99"/*Picture*/,5/*Tamanho*/,/*lPixel*/,/*{|| }*/)  //"Total Prv."
oTarefa:Cell("AF9_DESCRI"):SetLineBreak()
oTarefa:Cell("AE8_DESCRI"):SetLineBreak()
oTarefa:Cell("AED_DESCRI"):SetLineBreak()
oTarefa:SetLinesBefore(0)
oTarefa:SetLeftMargin(5)


oRecurso := TRSection():New(oReport,STR0031+ "2: "+STR0027, {"AE8","AED"},/*aOrdem*/,.F.,.F.) //"Recurso"
TRCell():New(oRecurso,	"AE8_RECURS"	,"AE8",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AE8->AE8_RECURS })
TRCell():New(oRecurso,	"AE8_DESCRI"	,"AE8",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| IIF(Empty(cObfNRecur), AE8->AE8_DESCRI,cObfNRecur) })
TRCell():New(oRecurso,	"AED_EQUIP"		,"AED",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,	{|| If(AED->(Found()),AED->AED_EQUIP,"") })
TRCell():New(oRecurso,	"AED_DESCRI"	,"AED",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| If(AED->(Found()),AED->AED_DESCRI,"") })
oRecurso:SetLineStyle()

oTarefa2 := TRSection():New(oRecurso,STR0028, {"AF9","AF8","AFA","AE8","AED","SB1"},/*aOrdem*/,.F.,.F.) //"Aloca��o de Recurso"
TRCell():New(oTarefa2,	"AF8_PROJET"	,"AF8",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF8->AF8_PROJET })
TRCell():New(oTarefa2,	"AF8_DESCRI"	,"AF8",/*Titulo*/,/*Picture*/,25/*Tamanho*/,/*lPixel*/,{|| AF8->AF8_DESCRI })
TRCell():New(oTarefa2,	"AF9_TAREFA"	,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_TAREFA })
TRCell():New(oTarefa2,	"AF9_DESCRI"	,"AF9",/*Titulo*/,/*Picture*/,25/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_DESCRI })
TRCell():New(oTarefa2,	"AFA_START"		,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_START })
TRCell():New(oTarefa2,	"AFA_HORAI"		,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_HORAI })
TRCell():New(oTarefa2,	"AFA_FINISH"	,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_FINISH })
TRCell():New(oTarefa2,	"AFA_HORAF"		,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_HORAF })
TRCell():New(oTarefa2,	"AFA_ALOC"		,"AFA",/*Titulo*/,"@E 999.99"/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_ALOC })
TRCell():New(oTarefa2,	"AFA_QUANT"		,"AFA",/*Titulo*/,"@E 999999999.99"/*Picture*/,/*Tamanho*/,/*lPixel*/,/*{|| }*/)
TRCell():New(oTarefa2,	"B1_UM"		,"SB1",STR0023/*Titulo*/,/*Picture*/,2/*Tamanho*/,/*lPixel*/,{||If(!Empty(AFA->AFA_PRODUT),SB1->B1_UM,"")}) //"UM"
TRCell():New(oTarefa2,	"AFA_CUSTD"		,"AFA",STR0017/*Titulo*/,/*Picture*/,5/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_CUSTD })  //"Custo Prv."
TRCell():New(oTarefa2,	"AFA_CUSTD1"	,"AFA",STR0018/*Titulo*/,"@E 999,999,999,999.99"/*Picture*/,5/*Tamanho*/,/*lPixel*/,/*{|| }*/)  //"Total Prv."
oTarefa2:Cell("AF8_DESCRI"):SetLineBreak()
oTarefa2:Cell("AF9_DESCRI"):SetLineBreak()
oTarefa2:SetLinesBefore(0)
oTarefa2:SetLeftMargin(5)


oEquipe := TRSection():New(oReport, STR0031+ "3: "+STR0029, {"AED"},/*aOrdem*/,.F.,.F.) //"Equipe"
TRCell():New(oEquipe,	"AED_EQUIP"		,"AED",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,	{|| AED->AED_EQUIP })
TRCell():New(oEquipe,	"AED_DESCRI"	,"AED",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AED->AED_DESCRI })
TRCell():New(oEquipe,	"UMaxEquip"		,"",STR0016/*Titulo*/,/*Picture*/,10/*Tamanho*/,/*lPixel*/,{|| Str(UMaxEquip(AED->AED_EQUIP)) })//"Un. Max  :"

oTarefa3:= TRSection():New(oEquipe, STR0030, {"AF9", "AF8", "AFA", "AE8", "AED", "SB1"},/*aOrdem*/,.F.,.F.) //"Tarefa + Aloca��o de Equipe"
TRCell():New(oTarefa3,	"AFA_PROJET"	,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_PROJET })
TRCell():New(oTarefa3,	"AF8_DESCRI"	,"AF8",/*Titulo*/,/*Picture*/,20/*Tamanho*/,/*lPixel*/,{|| GetProjDesc(AFA->AFA_PROJET) })
TRCell():New(oTarefa3,	"AF9_TAREFA"	,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_TAREFA })
TRCell():New(oTarefa3,	"AF9_DESCRI"	,"AF9",/*Titulo*/,/*Picture*/,20/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_DESCRI })
TRCell():New(oTarefa3,	"AFA_RECURS"	,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_RECURS })
TRCell():New(oTarefa3,	"AE8_DESCRI"	,"AE8",/*Titulo*/,/*Picture*/,20/*Tamanho*/,/*lPixel*/,{|| IIF(Empty(cObfNRecur), AE8->AE8_DESCRI,cObfNRecur)  })
TRCell():New(oTarefa3,	"AFA_START"		,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_START })
TRCell():New(oTarefa3,	"AFA_HORAI"		,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_HORAI })
TRCell():New(oTarefa3,	"AFA_FINISH"	,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_FINISH })
TRCell():New(oTarefa3,	"AFA_HORAF"		,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_HORAF })
TRCell():New(oTarefa3,	"AFA_ALOC"		,"AFA",/*Titulo*/,"@E 999.99"/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_ALOC })
TRCell():New(oTarefa3,	"AFA_QUANT"		,"AFA",/*Titulo*/,"@E 999999999.99"/*Picture*/,/*Tamanho*/,/*lPixel*/,/*{|| }*/)
TRCell():New(oTarefa3,	"B1_UM"		,"SB1",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{||If(!Empty(AFA->AFA_PRODUT),SB1->B1_UM,"")})
TRCell():New(oTarefa3,	"AFA_CUSTD"		,"AFA",STR0017/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_CUSTD })  //"Custo Prv."
TRCell():New(oTarefa3,	"AFA_CUSTD1"	,"AFA",STR0018/*Titulo*/,"@E 999,999,999,999.99"/*Picture*/,5/*Tamanho*/,/*lPixel*/,/*{|| }*/)  //"Total Prv."
oTarefa3:Cell("AF8_DESCRI"):SetLineBreak()
oTarefa3:Cell("AF9_DESCRI"):SetLineBreak()
oTarefa3:Cell("AE8_DESCRI"):SetLineBreak()
oTarefa3:SetLeftMargin(5)


oProjeto2 := TRSection():New(oReport,STR0031+ "4: "+STR0025,{"AF8" },/*aOrdem*/,.F.,.F.) //"Projeto"
TRCell():New(oProjeto2,	"AF8_PROJET"	,"AF8",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF8->AF8_PROJET })
TRCell():New(oProjeto2,	"AF8_DESCRI"	,"AF8",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF8->AF8_DESCRI })
oProjeto2:SetLineStyle()

oGrupo := TRSection():New(oProjeto2, STR0019, {"AE5"},/*aOrdem*/,.F.,.F.)  //"Grupo de Composicao"
TRCell():New(oGrupo,	"AE5_GRPCOM"	,"AE5",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,	{|| If(AE5->(Found()), AE5->AE5_GRPCOM, "Outros") })
TRCell():New(oGrupo,	"AE5_DESCRI"	,"AE5",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| If(AE5->(Found()), AE5->AE5_DESCRI, "Nao especificado") })
oGrupo:SetLinesBefore(0)
oGrupo:SetLineStyle()
oGrupo:SetLeftMargin(5)

oTarefa4:= TRSection():New(oGrupo, STR0030, {"AF9", "AF8","AE5", "AFA", "AE8", "AED", "SB1"},/*aOrdem*/,.F.,.F.) //"Tarefa + Aloca��o de Equipe"
TRCell():New(oTarefa4,	"AF9_TAREFA"	,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_TAREFA })
TRCell():New(oTarefa4,	"AF9_DESCRI"	,"AF9",/*Titulo*/,/*Picture*/,20/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_DESCRI })
TRCell():New(oTarefa4,	"AFA_RECURS"	,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_RECURS })
TRCell():New(oTarefa4,	"AE8_DESCRI"	,"AE8",/*Titulo*/,/*Picture*/,20/*Tamanho*/,/*lPixel*/,{|| IIF(Empty(cObfNRecur), AE8->AE8_DESCRI,cObfNRecur)  })
TRCell():New(oTarefa4,	"AFA_START"		,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_START })
TRCell():New(oTarefa4,	"AFA_HORAI"		,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_HORAI })
TRCell():New(oTarefa4,	"AFA_FINISH"	,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_FINISH })
TRCell():New(oTarefa4,	"AFA_HORAF"		,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_HORAF })
TRCell():New(oTarefa4,	"AFA_ALOC"		,"AFA",/*Titulo*/,"@E 999.99"/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_ALOC })
TRCell():New(oTarefa4,	"AFA_QUANT"		,"AFA",/*Titulo*/,"@E 999999999.99"/*Picture*/,/*Tamanho*/,/*lPixel*/,/*{|| }*/)
TRCell():New(oTarefa4,	"B1_UM"		,"SB1",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{||If(!Empty(AFA->AFA_PRODUT),SB1->B1_UM,"")})
TRCell():New(oTarefa4,	"AFA_CUSTD"		,"AFA",STR0017/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AFA->AFA_CUSTD })  //"Custo Prv."
TRCell():New(oTarefa4,	"AFA_CUSTD1"	,"AFA",STR0018/*Titulo*/,"@E 999,999,999,999.99"/*Picture*/,5/*Tamanho*/,/*lPixel*/,/*{|| }*/)  //"Total Prv."
oTarefa4:SetLinesBefore(0)
oTarefa4:SetLeftMargin(10)


oRecurso5 := TRSection():New(oReport,STR0031+ "5: "+STR0033, {"AE8","AED"},/*aOrdem*/,.F.,.F.) //"Recurso(Projeto)"
TRCell():New(oRecurso5,	"AE8_RECURS"	,"AE8",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AE8->AE8_RECURS })
TRCell():New(oRecurso5,	"AE8_DESCRI"	,"AE8",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| IIF(Empty(cObfNRecur), AE8->AE8_DESCRI,cObfNRecur) })
TRCell():New(oRecurso5,	"AED_EQUIP"		,"AED",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,	{|| If(AED->(Found()),AED->AED_EQUIP,"") })
TRCell():New(oRecurso5,	"AED_DESCRI"	,"AED",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| If(AED->(Found()),AED->AED_DESCRI,"") })
oRecurso5:SetLineStyle()

oTarefa5 := TRSection():New(oRecurso5,STR0028, {"AF9","AF8","AFA","AE8","AED","SB1"},/*aOrdem*/,.F.,.F.) //"Aloca��o de Recurso"
TRCell():New(oTarefa5,"AF8_PROJET","AF8",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,)
TRCell():New(oTarefa5,"AF8_DESCRI","AF8",/*Titulo*/,/*Picture*/,25/*Tamanho*/,/*lPixel*/,)
TRCell():New(oTarefa5,"AF9_TAREFA","AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,)
TRCell():New(oTarefa5,"AF9_DESCRI","AF9",/*Titulo*/,/*Picture*/,25/*Tamanho*/,/*lPixel*/,)
TRCell():New(oTarefa5,"AFA_START" ,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,)
TRCell():New(oTarefa5,"AFA_HORAI" ,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,)
TRCell():New(oTarefa5,"AFA_FINISH","AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,)
TRCell():New(oTarefa5,"AFA_HORAF" ,"AFA",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,)
TRCell():New(oTarefa5,"AFA_ALOC"  ,"AFA",/*Titulo*/,"@E 999.99"/*Picture*/,/*Tamanho*/,/*lPixel*/,)
TRCell():New(oTarefa5,"AFA_QUANT" ,"AFA",/*Titulo*/,"@E 999999999.99"/*Picture*/,/*Tamanho*/,/*lPixel*/,/*{|| }*/)
TRCell():New(oTarefa5,"B1_UM"     ,"SB1",STR0023/*Titulo*/,/*Picture*/,2/*Tamanho*/,/*lPixel*/,) //"UM"
TRCell():New(oTarefa5,"AFA_CUSTD" ,"AFA",STR0017/*Titulo*/,/*Picture*/,5/*Tamanho*/,/*lPixel*/,)  //"Custo Prv."
TRCell():New(oTarefa5,"AFA_CUSTD1","AFA",STR0018/*Titulo*/,"@E 999,999,999,999.99"/*Picture*/,5/*Tamanho*/,/*lPixel*/,/*{|| }*/)  //"Total Prv."
oTarefa5:Cell("AF8_DESCRI"):SetLineBreak()
oTarefa5:Cell("AF9_DESCRI"):SetLineBreak()
oTarefa5:SetLinesBefore(0)
oTarefa5:SetLeftMargin(5)
oTarefa5:SetTotalInLine(.F.)


oTrfCU01 := TRSection():New(oReport, STR0026,{"AF9"},/*aOrdem*/,.F.,.F.) //"Tarefa"
TRCell():New(oTrfCU01,	"AF9_TAREFA"	,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_TAREFA })
TRCell():New(oTrfCU01,	"AF9_DESCRI"	,"AF9",/*Titulo*/,/*Picture*/,25/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_DESCRI })
TRCell():New(oTrfCU01,	"AJY_DESC"		,"AJY",STR0035,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AJY->AJY_DESC })
TRCell():New(oTrfCU01,	"AJY_RECURS"	,"AJY",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AJY->AJY_RECURS })
TRCell():New(oTrfCU01,	"AE8_DESCRI"	,"AE8",/*Titulo*/,/*Picture*/,25/*Tamanho*/,/*lPixel*/,{|| IIF(Empty(cObfNRecur), AE8->AE8_DESCRI,cObfNRecur) })
TRCell():New(oTrfCU01,	"AE8_EQUIP"		,"AE8",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AE8->AE8_EQUIP })
TRCell():New(oTrfCU01,	"AED_DESCRI"	,"AED",/*Titulo*/,/*Picture*/,20/*Tamanho*/,/*lPixel*/,{|| AED->AED_DESCRI })
TRCell():New(oTrfCU01,	"AF9_START"		,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_START })
TRCell():New(oTrfCU01,	"AF9_HORAI"		,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_HORAI })
TRCell():New(oTrfCU01,	"AF9_FINISH"	,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_FINISH })
TRCell():New(oTrfCU01,	"AF9_HORAF"		,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_HORAF })
TRCell():New(oTrfCU01,	"AFA_ALOC"		,"AFA",/*Titulo*/,"@E 999.99"/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| GetAloc()  })
TRCell():New(oTrfCU01,	"AEL_QUANT"		,"AEL",/*Titulo*/,"@E 999999999.99"/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| nQtdIns  } )
TRCell():New(oTrfCU01,	"B1_UM"			,"SB1",STR0023/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{||If(!Empty(AJY->AJY_PRODUT),SB1->B1_UM,"")}) //"UM"
TRCell():New(oTrfCU01,	"AJY_CUSTD"		,"AJY",STR0017/*Titulo*/,/*Picture*/,5/*Tamanho*/,/*lPixel*/,{|| AJY->AJY_CUSTD })  //"Custo Prv."
oTrfCU01:Cell("AF9_DESCRI"):SetLineBreak()
oTrfCU01:Cell("AE8_DESCRI"):SetLineBreak()
oTrfCU01:Cell("AED_DESCRI"):SetLineBreak()
oTrfCU01:SetLinesBefore(0)
oTrfCU01:SetLeftMargin(5)

oTrfCU02 := TRSection():New(oRecurso, STR0026,{"AF9"},/*aOrdem*/,.F.,.F.) //"Tarefa"
TRCell():New(oTrfCU02,	"AF8_PROJET"	,"AF8",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF8->AF8_PROJET })
TRCell():New(oTrfCU02,	"AF8_DESCRI"	,"AF8",/*Titulo*/,/*Picture*/,25/*Tamanho*/,/*lPixel*/,{|| AF8->AF8_DESCRI })
TRCell():New(oTrfCU02,	"AF9_TAREFA"	,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_TAREFA })
TRCell():New(oTrfCU02,	"AF9_DESCRI"	,"AF9",/*Titulo*/,/*Picture*/,25/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_DESCRI })
TRCell():New(oTrfCU02,	"AJY_DESC"		,"AJY",STR0035,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AJY->AJY_DESC })
TRCell():New(oTrfCU02,	"AF9_START"		,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_START })
TRCell():New(oTrfCU02,	"AF9_HORAI"		,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_HORAI })
TRCell():New(oTrfCU02,	"AF9_FINISH"	,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_FINISH })
TRCell():New(oTrfCU02,	"AF9_HORAF"		,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_HORAF })
TRCell():New(oTrfCU02,	"AFA_ALOC"		,"AFA",/*Titulo*/,"@E 999.99"/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| GetAloc() })
TRCell():New(oTrfCU02,	"AEL_QUANT"		,"AEL",/*Titulo*/,"@E 999999999.99"/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| nQtdIns })
TRCell():New(oTrfCU02,	"B1_UM"			,"SB1",STR0023/*Titulo*/,/*Picture*/,2/*Tamanho*/,/*lPixel*/,{||If(!Empty(AJY->AJY_PRODUT),SB1->B1_UM,"")}) //"UM"
TRCell():New(oTrfCU02,	"AJY_CUSTD"		,"AF9",STR0017/*Titulo*/,/*Picture*/,5/*Tamanho*/,/*lPixel*/,{|| AJY->AJY_CUSTD })  //"Custo Prv."
oTrfCU02:Cell("AF8_DESCRI"):SetLineBreak()
oTrfCU02:Cell("AF9_DESCRI"):SetLineBreak()
oTrfCU02:SetLinesBefore(0)
oTrfCU02:SetLeftMargin(5)

oTrfCU03 := TRSection():New(oEquipe, STR0030, {"AF9", "AF8", "AFA", "AE8", "AED", "SB1"},/*aOrdem*/,.F.,.F.) //"Tarefa + Aloca��o de Equipe"
TRCell():New(oTrfCU03,	"AF9_PROJET"	,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_PROJET })
TRCell():New(oTrfCU03,	"AF8_DESCRI"	,"AF8",/*Titulo*/,/*Picture*/,20/*Tamanho*/,/*lPixel*/,{|| GetProjDesc(AF9->AF9_PROJET) })
TRCell():New(oTrfCU03,	"AF9_TAREFA"	,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_TAREFA })
TRCell():New(oTrfCU03,	"AF9_DESCRI"	,"AF9",/*Titulo*/,/*Picture*/,20/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_DESCRI })
TRCell():New(oTrfCU03,	"AJY_RECURS"	,"AJY",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AJY->AJY_RECURS })
TRCell():New(oTrfCU03,	"AE8_DESCRI"	,"AE8",/*Titulo*/,/*Picture*/,20/*Tamanho*/,/*lPixel*/,{|| IIF(Empty(cObfNRecur), AE8->AE8_DESCRI,cObfNRecur) })
TRCell():New(oTrfCU03,	"AF9_START"		,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_START })
TRCell():New(oTrfCU03,	"AF9_HORAI"		,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_HORAI })
TRCell():New(oTrfCU03,	"AF9_FINISH"	,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_FINISH })
TRCell():New(oTrfCU03,	"AF9_HORAF"		,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_HORAF })
TRCell():New(oTrfCU03,	"AFA_ALOC"		,"AFA",/*Titulo*/,"@E 999.99"/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| GetAloc() })
TRCell():New(oTrfCU03,	"AEL_QUANT"		,"AFA",/*Titulo*/,"@E 999999999.99"/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| nQtdIns })
TRCell():New(oTrfCU03,	"B1_UM"			,"SB1",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{||If(!Empty(AJY->AJY_PRODUT),SB1->B1_UM,"")})
TRCell():New(oTrfCU03,	"AJY_CUSTD"		,"AJY",STR0017/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AJY->AJY_CUSTD })  //"Custo Prv."
oTrfCU03:Cell("AF8_DESCRI"):SetLineBreak()
oTrfCU03:Cell("AF9_DESCRI"):SetLineBreak()
oTrfCU03:Cell("AE8_DESCRI"):SetLineBreak()
oTrfCU03:SetLeftMargin(5)

oTrfCU04:= TRSection():New(oGrupo, STR0030, {"AF9", "AF8","AE5", "AFA", "AE8", "AED", "SB1"},/*aOrdem*/,.F.,.F.) //"Tarefa + Aloca��o de Equipe"
TRCell():New(oTrfCU04,	"AF9_TAREFA"	,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_TAREFA })
TRCell():New(oTrfCU04,	"AF9_DESCRI"	,"AF9",/*Titulo*/,/*Picture*/,20/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_DESCRI })
TRCell():New(oTrfCU04,	"AJY_RECURS"	,"AJY",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AJY->AJY_RECURS })
TRCell():New(oTrfCU04,	"AE8_DESCRI"	,"AE8",/*Titulo*/,/*Picture*/,20/*Tamanho*/,/*lPixel*/,{|| IIF(Empty(cObfNRecur), AE8->AE8_DESCRI,cObfNRecur) })
TRCell():New(oTrfCU04,	"AF9_START"		,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_START })
TRCell():New(oTrfCU04,	"AF9_HORAI"		,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_HORAI })
TRCell():New(oTrfCU04,	"AF9_FINISH"	,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_FINISH })
TRCell():New(oTrfCU04,	"AF9_HORAF"		,"AF9",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AF9->AF9_HORAF })
TRCell():New(oTrfCU04,	"AFA_ALOC"		,"AFA",/*Titulo*/,"@E 999.99"/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| GetAloc() })
TRCell():New(oTrfCU04,	"AEL_QUANT"		,"AEL",/*Titulo*/,"@E 999999999.99"/*Picture*/,/*Tamanho*/,/*lPixel*/, {|| nQtdIns })
TRCell():New(oTrfCU04,	"B1_UM"			,"SB1",/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{||If(!Empty(AJY->AJY_PRODUT),SB1->B1_UM,"")})
TRCell():New(oTrfCU04,	"AJY_CUSTD"		,"AJY",STR0017/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| AJY->AJY_CUSTD })  //"Custo Prv."
oTrfCU04:SetLinesBefore(0)
oTrfCU04:SetLeftMargin(10)

Return(oReport)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �ReportPrint� Autor �Paulo Carnelossi      � Data �29/05/2006���
�������������������������������������������������������������������������Ĵ��
���Descri��o �A funcao estatica ReportDef devera ser criada para todos os ���
���          �relatorios que poderao ser agendados pelo usuario.          ���
���          �que faz a chamada desta funcao ReportPrint()                ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �ExpO1: Objeto do relat�rio                                  ���
�������������������������������������������������������������������������Ĵ��
���Parametros�Nenhum                                                      ���
���          �ExpO1: Objeto TReport                                       ���
���          �ExpC2: Alias da tabela de Planilha Orcamentaria (AK1)       ���
���          �ExpC3: Alias da tabela de Contas da Planilha (Ak3)          ���
���          �ExpC4: Alias da tabela de Revisoes da Planilha (AKE)        ���
���          �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������Ĵ��
���   DATA   � Programador   �Manutencao efetuada                         ���
�������������������������������������������������������������������������Ĵ��
���          �               �                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ReportPrint( oReport )
Local nOrdem    := oReport:Section(1):GetOrder()
Local oProjeto  := oReport:Section(1)
Local oTarefa   := oReport:Section(1):Section(1)
Local oRecurso  := oReport:Section(2)
Local oTarefa2  := oReport:Section(2):Section(1)
Local oEquipe   := oReport:Section(3)
Local oTarefa3  := oReport:Section(3):Section(1)
Local oProjeto2 := oReport:Section(4)
Local oGrupo    := oReport:Section(4):Section(1)
Local oTarefa4  := oReport:Section(4):Section(1):Section(1)
Local oRecurso5 := oReport:Section(5)
Local oTarefa5  := oReport:Section(5):Section(1)
Local oTrfCU01  := oReport:Section(6)
Local oTrfCU02  := oReport:Section(2):Section(2)
Local oTrfCU03  := oReport:Section(3):Section(2)
Local oTrfCU04  := oReport:Section(4):Section(1):Section(2)

Local oBreak, oBreakGer, oBreakPrj, oTotal, oTotalGer, oTotalPrj
Local aArea     := GetArea()
Local nQuantAFA := 0
Local nDecCst   := TamSX3("AF9_CUSTO")[2]
Local cTrunca   := "1"
Local cRecurso  := ""
Local cEquipe   := ""
Local cProjeto  := ""
Local nTotSecao := 0
Local nTotGeral := 0
Local nTotProje := 0
Local aDados    := {}
Local nX        := 0
Local cB1Desc	:= ""
Local cB1UM		:= ""
Local lUsaAJT	:= .F.
Local nCustD	:= 0
Local nProduc	:= 0

Do Case

	//----------------------------------
	// PROJETO + TAREFA
	//----------------------------------	
	Case nOrdem == 1
		oTarefa:SetTotalInLine(.F.)
	
		oTarefa:Cell("AFA_QUANT")	:SetBlock({||nQuantAFA})
		oTarefa:Cell("AFA_CUSTD1")	:SetBlock({||PmsTrunca(cTrunca,AFA->AFA_CUSTD*nQuantAFA,nDecCst,AF9->AF9_QUANT)})

		oBreak := TRBreak():New(oTarefa,{||.T.},STR0009) //"Total"
		oTotal := TRFunction():New(oTarefa:Cell("AFA_QUANT"),"NQUANTAFA" ,"ONPRINT",oBreak,/*cTitle*/,/*cPicture*/,{||nTotSecao}/*uFormula*/,.F./*lEndSection*/,.F./*lEndReport*/,.F./*lEndPage*/) //"Total"

		oBreakGer := TRBreak():New(oProjeto,{||.T.},STR0021) //"Total Geral"
		oTotalGer := TRFunction():New(oTarefa:Cell("AFA_QUANT"),"NQUANTAFA" ,"ONPRINT",,/*cTitle*/,/*cPicture*/,{||nTotGeral}/*uFormula*/,.F./*lEndSection*/,.F./*lEndReport*/,.F./*lEndPage*/)

		oReport:SetMeter(AFA->(LastRec()))
	    
		dbSelectArea("AF8")
		dbSetOrder(1) // AF8_FILIAL + AF8_PROJET
		MSSeek(xFilial("AF8") + mv_par01,.T.)
		While !Eof() .And. xFilial("AF8") == AF8->AF8_FILIAL ;
					 .And. AF8->AF8_PROJET <= mv_par02 .AND. !oReport:Cancel()
					 
			If AF8->AF8_DATA < mv_par03 .Or. AF8->AF8_DATA > mv_par04
				AF8->( DbSkip() )
				Loop
			EndIf
			// valida o filtro do usuario
			If !Empty(oProjeto:GetAdvplExp('AF8')) .And. !AF8->(&(oProjeto:GetAdvplExp('AF8')))
				AF8->( DbSkip() )
				Loop
			EndIf

			If AF8->AF8_PROJET < mv_par01 .Or. AF8->AF8_PROJET > mv_par02
				AF8->( DbSkip() )
				Loop
			EndIf

			//��������������������������������������������Ŀ
			//�verifica se o projeto usa composicoes aux   �
			//����������������������������������������������
			lUsaAJT := AF8ComAJT( AF8->AF8_PROJET )

			If PmrPertence(AF8->AF8_FASE,MV_PAR06) 
				cTrunca:= AF8->AF8_TRUNCA
				cRevisa := If(Empty(mv_par05),AF8->AF8_REVISA,mv_par05) 

				//��������������������������������������������������������Ŀ
				//�realiza o tratamento se o projeto usa composicoes aux   �
				//����������������������������������������������������������
				If lUsaAJT
					nTotSecao := nQuantAFA

					If AF8->AF8_DATA < mv_par03 .Or. AF8->AF8_DATA > mv_par04
						AF8->( DbSkip() )
						Loop
					EndIf

					DbSelecTArea( "AF9" )
					AF9->( DbSetOrder( 1 ) )
					AF9->( DbSeek( xFilial( "AF9" ) + AF8->( AF8_PROJET + AF8_REVISA ) ) )
					While AF9->( !Eof() ) .AND. AF9->( AF9_FILIAL + AF9_PROJET + AF9_REVISA ) == xFilial( "AF9" ) + AF8->( AF8_PROJET + AF8_REVISA )
						// valida o filtro do usuario
						If !Empty(oProjeto:GetAdvplExp('AF9')) .And. !AF9->(&(oProjeto:GetAdvplExp('AF9')))
							AF9->( DbSkip() )
							Loop
						EndIf

						If 	(mv_par11==1).Or.; //Todas as tarefas 
							(mv_par11==3.And.!Empty(AF9->AF9_DTATUF)).Or.; //Tarefas finalizadas
							(mv_par11==2.And.Empty(AF9->AF9_DTATUF)) // Tarefas a executar

							DbSelecTArea( "AEL" )
							AEL->( DbSetOrder( 1 ) )
							AEL->( DbSeek( xFilial( "AEL" ) + AF9->( AF9_PROJET + AF9_REVISA + AF9_TAREFA ) ) )
							While AEL->( !Eof() ) .AND. AEL->( AEL_FILIAL + AEL_PROJET + AEL_REVISA + AEL_TAREFA ) == xFilial( "AEL" ) + AF9->( AF9_PROJET + AF9_REVISA + AF9_TAREFA )
								DbSelectArea( "AJY" )
								AJY->( DbSetOrder( 1 ) )
								If AJY->( DbSeek( xFilial( "AJY" ) + AEL->( AEL_PROJET + AEL_REVISA + AEL_INSUMO ) ) )
									nProduc := 1
									If AF9->AF9_TIPO<>'1' .And. AEL->AEL_GRORGA $ PmsTPPrdE( AF9->AF9_TPPRDE )
										nProduc := AF9->AF9_PRODUC / nProduc
									EndIf
									AE8->( DbSetOrder( 1 ) )
									AE8->( DbSeek( xFilial( "AE8" ) + AJY->AJY_RECURS ) )
									If AJY->AJY_RECURS >= mv_par07 .AND. AJY->AJY_RECURS <= mv_par08

										nCustD		:= AJY->AJY_CUSTD
										nQuantAFA	:= AEL->AEL_QUANT
										nQuantAFA	:= pmsTrunca( "2", nQuantAFA/nProduc, nDecCst )
										nQuantAFA	:= pmsTrunca( "2", nQuantAFA * AF9->AF9_QUANT, nDecCst )
										nQtdIns		:= nQuantAFA

										If cProjeto <> AF8->AF8_PROJET
											oReport:SkipLine()
											oProjeto:Init()
											oProjeto:PrintLine()
											oTarefa:Init()
											cProjeto	:=	AF8->AF8_PROJET
											nTotSecao  := 0
										Endif	

										nTotSecao	+= nQuantAFA
										nTotGeral	+= nQuantAFA

										oTrfCU01:Init()
										oTrfCU01:PrintLine()
									EndIf
								EndIf

								AEL->( DbSkip() )
							End
							
							DbSelectArea( "AEN" )
							AEN->( DbSetOrder( 1 ) )
							AEN->( DbSeek( xFilial( "AEN" ) + AF9->( AF9_PROJET + AF9_REVISA + AF9_TAREFA ) ) )
							Do While !AEN->( Eof() ) .And. AF9->( AF9_PROJET + AF9_REVISA + AF9_TAREFA ) == AEN->( AEN_PROJET + AEN_REVISA + AEN_TAREFA )

								If AF9->AF9_TIPO<>'1' .And. "E" $ PmsTPPrdE( AF9->AF9_TPPRDE )
									nProduc := AF9->AF9_PRODUC
								Else
									nProduc := 1
								EndIf
					
								ExplodCU1(AF9->AF9_PROJET, AF9->AF9_REVISA, AEN->AEN_SUBCOM, AF9->AF9_QUANT * AEN->AEN_QUANT, nProduc, @oReport, @oProjeto, @oTarefa, @oTrfCU01, @cProjeto, @nCustD, @nTotSecao, @nTotGeral)

								AEN->( DbSkip() )
							EndDo

						EndIf

						AF9->( DbSkip() )
					End

					oTarefa:Finish()
					oProjeto:Finish()
				Else
					dbSelectArea("AFA")
					dbSetOrder(1)
					If MSSeek(xFilial()+AF8->AF8_PROJET+cRevisa)
		
						While !Eof() .And. xFilial()+AF8->AF8_PROJET+cRevisa==;
						                   AFA->AFA_FILIAL+AFA->AFA_PROJET+AFA->AFA_REVISA .AND. !oReport:Cancel()
							
							If !Empty(AFA->AFA_RECURS)
								If (!Empty(mv_par05) .And. AFA->AFA_REVISA != mv_par05) .Or.;
								    (Empty(mv_par05) .And. AFA->AFA_REVISA != AF8->AF8_REVISA)
									dbSelectArea("AFA")
									dbSkip()
									loop
								EndIf
								If (AFA->AFA_START < mv_par09 .And. AFA->AFA_FINISH < mv_par09) .Or.;
									(AFA->AFA_START > mv_par10 .And. AFA->AFA_FINISH > mv_par10)
									dbSelectArea("AFA")
									dbSkip()
									loop
								EndIf
								If AFA->AFA_PROJET < mv_par01 .Or. AFA->AFA_PROJET > mv_par02
									dbSelectArea("AFA")
									dbSkip()
									loop
								EndIf
								// valida o filtro do usuario
								If !Empty(oProjeto:GetAdvplExp('AFA')) .And. !AFA->(&(oProjeto:GetAdvplExp('AFA')))
									dbSelecTArea("AFA")
									dbSkip()
									Loop
								EndIf
								AE8->(dbSetOrder(1))
								AE8->(MSSeek(xFilial()+AFA->AFA_RECURS))
					
								If AE8->AE8_RECURS < mv_par07 .Or. AE8->AE8_RECURS > mv_par08
									dbSelectArea("AFA")
									dbSkip()
									Loop
								EndIf
								If (AE8->AE8_EQUIP < mv_par12 .Or. AE8->AE8_EQUIP > mv_par13)
									dbSelectArea("AFA")
									dbSkip()
									Loop	
								End If
	
								// valida o filtro do usuario
								If !Empty(oProjeto:GetAdvplExp('AE8')) .And. !AE8->(&(oProjeto:GetAdvplExp('AE8')))
									dbSelecTArea("AFA")
									dbSkip()
									Loop
								EndIf
	
								dbSelecTArea("AF9")
								dbSetOrder(1)
								MsSeek(xFilial()+AFA->AFA_PROJET+AFA->AFA_REVISA+AFA->AFA_TAREFA)
								dbSelecTArea("AFA")
								// valida o filtro do usuario
								If !Empty(oProjeto:GetAdvplExp('AF9')) .And. !AF9->(&(oProjeto:GetAdvplExp('AF9')))
									dbSelecTArea("AFA")
									dbSkip()
									Loop
								EndIf
			
					
								If	(mv_par11==1).Or.; //Todas as tarefas 
									(mv_par11==3.And.!Empty(AF9->AF9_DTATUF)).Or.; //Tarefas finalizadas
									(mv_par11==2.And.Empty(AF9->AF9_DTATUF)) // Tarefas a executar
			
									AED->(dbSetOrder(1))
									AED->(MSSeek(xFilial()+AE8->AE8_EQUIP))
									// valida o filtro do usuario
									If !Empty(oProjeto:GetAdvplExp('AED')) .And. !AED->(&(oProjeto:GetAdvplExp('AED')))
										dbSelecTArea("AFA")
										dbSkip()
										Loop
									EndIf
	 								If !Empty(AFA->AFA_PRODUT)
										SB1->(dbSetOrder(1))
										SB1->(MSSeek(xFilial()+AFA->AFA_PRODUT))
										// valida o filtro do usuario
										If !Empty(oProjeto:GetAdvplExp('SB1')) .And. !SB1->(&(oProjeto:GetAdvplExp('SB1')))
											dbSelecTArea("AFA")
											dbSkip()
											Loop
										EndIf
									Endif
									If cProjeto <> AF8->AF8_PROJET
										oReport:SkipLine()
										oProjeto:Init()
										oProjeto:PrintLine()
										oTarefa:Init()
										cProjeto	:=	AF8->AF8_PROJET
										nTotSecao  := 0
									Endif	
	
									//�������������������������������������������������������Ŀ
									//�realiza o tratamento para a quantidade quando o projeto�
									//�usa composicoes aux                                    �
									//���������������������������������������������������������
									nQuantAFA:= PmsAFAQuant(AFA->AFA_PROJET,AFA->AFA_REVISA,AFA->AFA_TAREFA,AFA->AFA_PRODUT,AF9->AF9_QUANT,AFA->AFA_QUANT,AF9->AF9_HDURAC)
	
									oTarefa:PrintLine()
									nTotSecao += nQuantAFA
									nTotGeral += nQuantAFA
								EndIf
								
							EndIf                    
				
							dbSelectArea("AFA")
							dbSkip()
			
							oReport:IncMeter()				
			
						EndDo
				
						oTarefa:Finish()
						oProjeto:Finish()
					EndIf		
				EndIf
			EndIf
			dbSelectArea("AF8")
			dbSkip()
		EndDo

		// verifica o cancelamento pelo usuario..
		If oReport:Cancel()
			oReport:SkipLine()
			oReport:PrintText(STR0034) //"*** CANCELADO PELO OPERADOR ***"
		Else
			oTotalGer:SetBreak(oBreakGer)
			oProjeto:Init()
			oBreakGer:Execute()
			oProjeto:Finish()
		EndIf

	//----------------------------------
	// RECURSO + DATA DE ALOCACAO
	//----------------------------------	
	Case nOrdem == 2
		oTarefa2:SetTotalInLine(.F.)
	
		oTarefa2:Cell("AFA_QUANT")	:SetBlock({||nQuantAFA})
		oTarefa2:Cell("AFA_CUSTD1"):SetBlock({||PmsTrunca(cTrunca,AFA->AFA_CUSTD*nQuantAFA,nDecCst,AF9->AF9_QUANT)})

		oBreak := TRBreak():New(oTarefa2,{||.T.},STR0009) //"Total"
		oTotal := TRFunction():New(oTarefa2:Cell("AFA_QUANT"),"NQUANTAFA" ,"ONPRINT",oBreak,STR0009/*cTitle*/,/*cPicture*/,{||nTotSecao}/*uFormula*/,.F./*lEndSection*/,.F./*lEndReport*/,.F./*lEndPage*/) //"Total"

		oBreakGer := TRBreak():New(oRecurso,{||.T.},STR0021) //"Total Geral"
		oTotalGer := TRFunction():New(oTarefa2:Cell("AFA_QUANT"),"NQUANTAFA" ,"ONPRINT",,/*cTitle*/,/*cPicture*/,{||nTotGeral}/*uFormula*/,.F./*lEndSection*/,.F./*lEndReport*/,.F./*lEndPage*/)

    	oReport:SetMeter(AFA->(LastRec()))

		dbSelectArea("AE8")
		dbSetOrder(1)  // AE8_FILIAL + AE8_RECURS
		MSSeek(xFilial("AE8") + mv_par07, .T.)

		While !Eof() .And. xFilial("AE8") == AE8->AE8_FILIAL ;
					 .And. AE8->AE8_RECURS <= mv_par08 .AND. !oReport:Cancel()
			
			If (AE8->AE8_EQUIP < mv_par12 .Or. AE8->AE8_EQUIP > mv_par13)
				dbSelectArea("AE8")
				dbSkip()
				Loop	
			End If
			
			// valida o filtro do usuario
			If !Empty(oRecurso:GetAdvplExp('AE8')) .And. !AE8->(&(oRecurso:GetAdvplExp('AE8')))
				dbSelecTArea("AE8")
				dbSkip()
				Loop
			EndIf

			dbSelectArea("AFA")
			dbSetOrder(3) //AFA_FILIAL+AFA_RECURS+DTOS(AFA_START)+AFA_HORAI
			MSSeek(xFilial()+AE8->AE8_RECURS)
			While !Eof() .And. AFA->AFA_FILIAL==xFilial("AFA") .And.;
								AFA->AFA_RECURS==AE8->AE8_RECURS

				// valida o filtro do usuario
				If !Empty(oRecurso:GetAdvplExp('AFA')) .And. !AFA->(&(oRecurso:GetAdvplExp('AFA')))
					dbSelecTArea("AFA")
					dbSkip()
					Loop
				EndIf

				AF8->(dbSetOrder(1))
				AF8->(MSSeek(xFilial()+AFA->AFA_PROJET))
				// valida o filtro do usuario
				If !Empty(oRecurso:GetAdvplExp('AF8')) .And. !AF8->(&(oRecurso:GetAdvplExp('AF8')))
					dbSelecTArea("AFA")
					dbSkip()
					Loop
				EndIf
				
				If PmrPertence(AF8->AF8_FASE,MV_PAR06) 
					// valida o filtro do usuario
					dbSelecTArea("AF9")
					dbSetOrder(1)
					MsSeek(xFilial()+AFA->AFA_PROJET+AFA->AFA_REVISA+AFA->AFA_TAREFA)
					// valida o filtro do usuario
					If !Empty(oRecurso:GetAdvplExp('AF9')) .And. !AF9->(&(oRecurso:GetAdvplExp('AF9')))
						dbSelecTArea("AFA")
						dbSkip()
						Loop
					EndIf

					dbSelecTArea("AFA")
					If AFA->AFA_PROJET < mv_par01 .Or. AFA->AFA_PROJET > mv_par02
						dbSelectArea("AFA")
						dbSkip()
						Loop
					EndIf
					
					If AF8->AF8_DATA < mv_par03 .Or. AF8->AF8_DATA > mv_par04
						dbSelectArea("AFA")
						dbSkip()
						Loop
					EndIf
					
					If (!Empty(mv_par05) .And. AFA->AFA_REVISA != mv_par05) .Or.;
					    (Empty(mv_par05) .And. AFA->AFA_REVISA != AF8->AF8_REVISA)
						dbSelectArea("AFA")
						dbSkip()
						Loop
					EndIf
	
					If (AFA->AFA_START < mv_par09 .And. AFA->AFA_FINISH < mv_par09) .Or.;
						(AFA->AFA_START > mv_par10 .And. AFA->AFA_FINISH > mv_par10)
						dbSelectArea("AFA")
						dbSkip()
						Loop
					EndIf
		
					If (mv_par11==1).Or.; //Todas as tarefas 
					   (mv_par11==3.And.!Empty(AF9->AF9_DTATUF)).Or.; //Tarefas finalizadas
					   (mv_par11==2.And.Empty(AF9->AF9_DTATUF)) // Tarefas a executar

						dbSelectArea("AED")
						AED->(dbSetOrder(1))
						AED->(MSSeek(xFilial() + AE8->AE8_EQUIP))
	
						// valida o filtro do usuario
						If !Empty(oRecurso:GetAdvplExp('AED')) .And. !AED->(&(oRecurso:GetAdvplExp('AED')))
							dbSelecTArea("AFA")
							dbSkip()
							Loop
						EndIf
						If !Empty(AFA->AFA_PRODUT)
							SB1->(dbSetOrder(1))
							SB1->(MSSeek(xFilial()+AFA->AFA_PRODUT))
							// valida o filtro do usuario
							If !Empty(oRecurso:GetAdvplExp('SB1')) .And. !SB1->(&(oRecurso:GetAdvplExp('SB1')))
								dbSelecTArea("AFA")
								dbSkip()
								Loop
							EndIf
						Endif

						If cRecurso <> AE8->AE8_RECURS
							oReport:SkipLine()
							cRecurso := AE8->AE8_RECURS
							
							oRecurso:Init()
							oRecurso:PrintLine()
							oTarefa2:Init()
							nTotSecao := 0
						Endif

						nQuantAFA := PmsAFAQuant(AFA->AFA_PROJET,AFA->AFA_REVISA,AFA->AFA_TAREFA,AFA->AFA_PRODUT,AF9->AF9_QUANT,AFA->AFA_QUANT,AF9->AF9_HDURAC)

						oTarefa2:PrintLine()
						nTotSecao += nQuantAFA
						nTotGeral += nQuantAFA
						
					EndIf
				EndIf
				
				dbSelectArea("AFA")
				dbSkip()    
				
				oReport:IncMeter()
				
			EndDo

			oTarefa2:Finish()
			nTotSecao := 0

		
			//����������������������������������������������������������������������Ŀ
			//�Verifica alocacao dos recursos nos projetos que usam composicao aux.  �
			//������������������������������������������������������������������������
			DbSelectArea( "AJY" )
			AJY->( DbSetOrder( 2 ) )
			AJY->( DbSeek( xFilial( "AJY" ) + AE8->AE8_RECURS ) )
			Do While AJY->( !Eof() ) .AND. AJY->( AJY_FILIAL + AJY_RECURS ) == xFilial( "AJY" ) + AE8->AE8_RECURS
				If !AF8ComAJT( AJY->AJY_PROJET )
					AJY->( DbSkip() )
					Loop
				EndIf
	
				DbSelectArea( "AF8" )
				AF8->( DbSetOrder( 1 ) )
				AF8->( DbSeek( xFilial( "AF8" ) + AJY->AJY_PROJET ) )
				If PmrPertence( AF8->AF8_FASE, MV_PAR06 )
					If AJY->AJY_PROJET < mv_par01 .Or. AJY->AJY_PROJET > mv_par02
						AJY->( DbSkip() )
						Loop
					EndIf
						
					If AF8->AF8_DATA < mv_par03 .Or. AF8->AF8_DATA > mv_par04
						AJY->( DbSkip() )
						Loop
					EndIf
							
					If (!Empty(mv_par05) .And. AJY->AJY_REVISA != mv_par05) .Or.;
					    (Empty(mv_par05) .And. AJY->AJY_REVISA != AF8->AF8_REVISA)
						AJY->( DbSkip() )
						Loop
					EndIf
	
					oTarefa2:Init()	

					DbSelectArea( "AEL" )
					AEL->( DbSetOrder( 2 ) )
					AEL->( DbSeek( xFilial( "AEL" ) + AJY->( AJY_PROJET + AJY_REVISA + AJY_INSUMO ) ) )
					Do While AEL->( !Eof() ) .AND. AEL->( AEL_FILIAL + AEL_PROJET + AEL_REVISA + AEL_INSUMO ) == xFilial( "AEL" ) + AJY->( AJY_PROJET + AJY_REVISA + AJY_INSUMO )
						DbSelectArea( "AF9" )
						AF9->( DbSetOrder( 1 ) )
						If AF9->( DbSeek( xFilial( "AF9" ) + AEL->( AEL_PROJET + AEL_REVISA + AEL_TAREFA ) ) )
							If (AF9->AF9_START < mv_par09 .And. AF9->AF9_FINISH < mv_par09) .Or.;
							   (AF9->AF9_START > mv_par10 .And. AF9->AF9_FINISH > mv_par10)
								AEL->( DbSkip() )
								Loop
							EndIf

							If (mv_par11==1).Or.; //Todas as tarefas 
							   (mv_par11==3.And.!Empty(AF9->AF9_DTATUF)).Or.; //Tarefas finalizadas
							   (mv_par11==2.And.Empty(AF9->AF9_DTATUF)) // Tarefas a executar

								nProduc := 1
								If AF9->AF9_TIPO<>'1' .And. AEL->AEL_GRORGA $ PmsTPPrdE( AF9->AF9_TPPRDE )
									nProduc := AF9->AF9_PRODUC / nProduc
								EndIf

								nCustD		:= AJY->AJY_CUSTD
								nQuantAFA	:= AEL->AEL_QUANT
								nQuantAFA	:= pmsTrunca( "2", nQuantAFA/nProduc, nDecCst )
								nQuantAFA	:= pmsTrunca( "2", nQuantAFA * AF9->AF9_QUANT, nDecCst )
								nQtdIns		:= nQuantAFA

								If cRecurso <> AE8->AE8_RECURS
									oReport:SkipLine()
									cRecurso := AE8->AE8_RECURS

									oRecurso:Init()
									oRecurso:PrintLine()
								Endif

								oTrfCU02:Init()
								oTrfCU02:PrintLine()

								nTotSecao += nQuantAFA
								nTotGeral += nQuantAFA
							EndIf
						EndIf

						AEL->( DbSkip() )
					EndDo
	
					DbSelectArea( "AEN" )
					AEN->( DbSetOrder( 1 ) )
					AEN->( DbSeek( xFilial( "AEN" ) + AF9->( AF9_PROJET + AF9_REVISA + AF9_TAREFA ) ) )
					Do While !AEN->( Eof() ) .And. AF9->( AF9_PROJET + AF9_REVISA + AF9_TAREFA ) == AEN->( AEN_PROJET + AEN_REVISA + AEN_TAREFA )

						If AF9->AF9_TIPO<>'1' .And. "E" $ PmsTPPrdE( AF9->AF9_TPPRDE )
							nProduc := AF9->AF9_PRODUC
						Else
							nProduc := 1
						EndIf

						ExplodCU2(AF9->AF9_PROJET, AF9->AF9_REVISA, AEN->AEN_SUBCOM, AF9->AF9_QUANT * AEN->AEN_QUANT, nProduc, AJY->AJY_INSUMO, @oReport, @oRecurso, @oTrfCU02, @cRecurso, @nCustD, @nTotSecao, @nTotGeral)

						AEN->( DbSkip() )
					EndDo
	
				EndIf

				AJY->( DbSkip() )
			EndDo
			oTarefa2:Finish()
			oRecurso:Finish()

			dbSelectArea("AE8")
			dbSkip()
		EndDo

		// verifica o cancelamento pelo usuario..
		If oReport:Cancel()
			oReport:SkipLine()
			oReport:PrintText(STR0034) //"*** CANCELADO PELO OPERADOR ***"
		Else
			oTotalGer:SetBreak(oBreakGer)
			oRecurso:Init()
			oBreakGer:Execute()
			oRecurso:Finish()
		EndIf
					
	//----------------------------------
	// EQUIPE + DATA DE ALOCACAO
	//----------------------------------	
	Case nOrdem==3
		oTarefa3:SetTotalInLine(.F.)

		oTarefa3:Cell("AFA_QUANT")	:SetBlock({||nQuantAFA})
		oTarefa3:Cell("AFA_CUSTD1"):SetBlock({||PmsTrunca(cTrunca,AFA->AFA_CUSTD*nQuantAFA,nDecCst,AF9->AF9_QUANT)})

		oBreak := TRBreak():New(oTarefa3,{||.T.},STR0009) //"Total"
		oTotal := TRFunction():New(oTarefa3:Cell("AFA_QUANT"),"NQUANTAFA" ,"ONPRINT",oBreak,/*cTitle*/,/*cPicture*/,{||nTotSecao}/*uFormula*/,.F./*lEndSection*/,.F./*lEndReport*/,.F./*lEndPage*/)

		oBreakGer := TRBreak():New(oEquipe,{||.T.},STR0021) //"Total Geral"
		oTotalGer := TRFunction():New(oTarefa3:Cell("AFA_QUANT"),"NQUANTAFA" ,"ONPRINT",,/*cTitle*/,/*cPicture*/,{||nTotGeral}/*uFormula*/,.F./*lEndSection*/,.F./*lEndReport*/,.F./*lEndPage*/)

    	oReport:SetMeter(AFA->(LastRec()))
	
		dbSelectArea("AED")
		dbSetOrder(1)  // AED_FILIAL + AE8_EQUIP 
		MSSeek(xFilial("AED") + mv_par12,.T.)

		While !Eof() .And. xFilial("AED") == AED->AED_FILIAL ;
					 .And. AED->AED_EQUIP <= mv_par13 .and. !oReport:Cancel()

			If (AED->AED_EQUIP < mv_par12 .Or. AED->AED_EQUIP > mv_par13)
				dbSelectArea("AED")
				AED->(dbSkip())
				Loop	
			EndIf
			// valida o filtro do usuario
			If !Empty(oEquipe:GetAdvplExp('AED')) .And. !AED->(&(oEquipe:GetAdvplExp('AED')))
				dbSelecTArea("AED")
				dbSkip()
				Loop
			EndIf
 
			dbSelectArea("AE8")
			AE8->(dbSetOrder(4))
			AE8->(MSSeek(xFilial() + AED->AED_EQUIP))

			While !AE8->(Eof()) .And. xFilial("AE8")+AED->AED_EQUIP==AE8->AE8_FILIAL+AE8->AE8_EQUIP	

				If AE8->AE8_RECURS < mv_par07 .Or. AE8->AE8_RECURS > mv_par08
					dbSelectArea("AE8")
					dbSkip()
					Loop
				EndIf
				// valida o filtro do usuario
				If !Empty(oEquipe:GetAdvplExp('AE8')) .And. !AE8->(&(oEquipe:GetAdvplExp('AE8')))
					dbSelecTArea("AE8")
					dbSkip()
					Loop
				EndIf
						
				// procura cada recurso na tabela AFA - tarefa x recurso
				dbSelectArea("AFA")
				AFA->(dbSetOrder(3))
				AFA->(MSSeek(xFilial() + AE8->AE8_RECURS))
				
				While AFA->(! Eof() .And. AE8->AE8_FILIAL+AE8->AE8_RECURS == xFilial()+AFA->AFA_RECURS)
					dbSelectArea("AF8")
					dbSetOrder(1)
					AF8->(MSSeek(xFilial() + AFA->AFA_PROJET))
					If AFA->AFA_PROJET < mv_par01 .Or. AFA->AFA_PROJET > mv_par02
						dbSelectArea("AFA")
						dbSkip()
						loop
					EndIf
										
					If AF8->AF8_DATA < mv_par03 .Or. AF8->AF8_DATA > mv_par04
						dbSelectArea("AFA")
						dbSkip()
						loop
					EndIf

					If !PmrPertence(AF8->AF8_FASE,MV_PAR06) 
						dbSelectArea("AFA")
						dbSkip()
						loop
					EndIf
					
					If (!Empty(mv_par05) .And. AFA->AFA_REVISA != mv_par05) .Or.;
					    (Empty(mv_par05) .And. AFA->AFA_REVISA != AF8->AF8_REVISA)
						dbSelectArea("AFA")
						dbSkip()
						loop
					EndIf
		
					If (AFA->AFA_START < mv_par09 .And. AFA->AFA_FINISH < mv_par09) .Or.;
						(AFA->AFA_START > mv_par10 .And. AFA->AFA_FINISH > mv_par10)
						dbSelectArea("AFA")
						dbSkip()
						loop
					EndIf	
					// valida o filtro do usuario
					If !Empty(oEquipe:GetAdvplExp('AFA')) .And. !AFA->(&(oEquipe:GetAdvplExp('AFA')))
						dbSelecTArea("AFA")
						dbSkip()
						Loop
					EndIf

					dbSelectArea("AF9")
					dbSetOrder(1)
					MsSeek(xFilial()+AFA->AFA_PROJET+AFA->AFA_REVISA+AFA->AFA_TAREFA)
					// valida o filtro do usuario
					If !Empty(oEquipe:GetAdvplExp('AF9')) .And. !AF9->(&(oEquipe:GetAdvplExp('AF9')))
						dbSelecTArea("AFA")
						dbSkip()
						Loop
					EndIf

					AF8->(dbSetOrder(1))
					AF8->(MSSeek(xFilial()+AFA->AFA_PROJET))
					// valida o filtro do usuario
					If !Empty(oEquipe:GetAdvplExp('AF8')) .And. !AF8->(&(oEquipe:GetAdvplExp('AF8')))
						dbSelecTArea("AFA")
						dbSkip()
						Loop
					EndIf
					If !Empty(AFA->AFA_PRODUT)
						SB1->(dbSetOrder(1))
						SB1->(MSSeek(xFilial()+AFA->AFA_PRODUT))
						// valida o filtro do usuario
						If !Empty(oEquipe:GetAdvplExp('SB1')) .And. !SB1->(&(oEquipe:GetAdvplExp('SB1')))
							dbSelecTArea("AFA")
							dbSkip()
							Loop
						EndIf
					Endif
					dbSelecTArea("AFA")

					If cEquipe <> AED->AED_EQUIP
						cEquipe := AED->AED_EQUIP
						oEquipe:Init()
						oEquipe:PrintLine()
						oTarefa3:Init()
						nTotSecao  := 0
					Endif				
			
					nQuantAFA := PmsAFAQuant(AFA->AFA_PROJET,AFA->AFA_REVISA,AFA->AFA_TAREFA,AFA->AFA_PRODUT,AF9->AF9_QUANT,AFA->AFA_QUANT,AF9->AF9_HDURAC)

					oTarefa3:PrintLine()
					nTotSecao += nQuantAFA
					nTotGeral += nQuantAFA

					dbSelectArea("AFA")
					AFA->(dbSkip())
					
					oReport:IncMeter()
					
				EndDo

				oTarefa3:Finish()
				nTotSecao  := 0

			
				//����������������������������������������������������������������������Ŀ
				//�Verifica alocacao dos recursos nos projetos que usam composicao aux.  �
				//������������������������������������������������������������������������
				DbSelectArea( "AJY" )
				AJY->( DbSetOrder( 2 ) )
				AJY->( DbSeek( xFilial( "AJY" ) + AE8->AE8_RECURS ) )
				While AJY->( !Eof() ) .AND. AJY->( AJY_FILIAL + AJY_RECURS ) == xFilial( "AJY" ) + AE8->AE8_RECURS
					If !AF8ComAJT( AJY->AJY_PROJET )
						AJY->( DbSkip() )
						Loop
					EndIf

					DbSelectArea( "AF8" )
					AF8->( DbSetOrder( 1 ) )
					AF8->( DbSeek( xFilial( "AF8" ) + AJY->AJY_PROJET ) )
					If !PmrPertence( AF8->AF8_FASE, MV_PAR06 )
						AJY->( DbSkip() )
						Loop
					EndIf

					If AJY->AJY_PROJET < mv_par01 .Or. AJY->AJY_PROJET > mv_par02
						AJY->( DbSkip() )
						Loop
					EndIf

					If AF8->AF8_DATA < mv_par03 .Or. AF8->AF8_DATA > mv_par04
						AJY->( DbSkip() )
						Loop
					EndIf

					If (!Empty(mv_par05) .And. AJY->AJY_REVISA != mv_par05) .Or.;
					    (Empty(mv_par05) .And. AJY->AJY_REVISA != AF8->AF8_REVISA)
						AJY->( DbSkip() )
						Loop
					EndIf

					DbSelectArea( "AEL" )
					AEL->( DbSetOrder( 2 ) )
					AEL->( DbSeek( xFilial( "AEL" ) + AJY->( AJY_PROJET + AJY_REVISA + AJY_INSUMO ) ) )
					While AEL->( !Eof() ) .AND. AEL->( AEL_FILIAL + AEL_PROJET + AEL_REVISA + AEL_INSUMO ) == xFilial( "AEL" ) + AJY->( AJY_PROJET + AJY_REVISA + AJY_INSUMO )
						DbSelectArea( "AF9" )
						AF9->( DbSetOrder( 1 ) )
						If AF9->( DbSeek( xFilial( "AF9" ) + AEL->( AEL_PROJET + AEL_REVISA + AEL_TAREFA ) ) )
							If (AF9->AF9_START < mv_par09 .And. AF9->AF9_FINISH < mv_par09) .Or.;
							   (AF9->AF9_START > mv_par10 .And. AF9->AF9_FINISH > mv_par10)
								AEL->( DbSkip() )
								Loop
							EndIf

							If (mv_par11==1).Or.; //Todas as tarefas 
							   (mv_par11==3.And.!Empty(AF9->AF9_DTATUF)).Or.; //Tarefas finalizadas
							   (mv_par11==2.And.Empty(AF9->AF9_DTATUF)) // Tarefas a executar

								nProduc := 1
								If AF9->AF9_TIPO<>'1' .And. AEL->AEL_GRORGA $ PmsTPPrdE( AF9->AF9_TPPRDE )
									nProduc := AF9->AF9_PRODUC / nProduc
								EndIf

								nCustD		:= AJY->AJY_CUSTD
								nQuantAFA	:= AEL->AEL_QUANT
								nQuantAFA	:= pmsTrunca( "2", nQuantAFA/nProduc, nDecCst )
								nQuantAFA	:= pmsTrunca( "2", nQuantAFA * AF9->AF9_QUANT, nDecCst )
								nQtdIns		:= nQuantAFA

								If cEquipe <> AED->AED_EQUIP
									nQtdIns := AEL->AEL_QUANT
									cEquipe := AED->AED_EQUIP
									oEquipe:Init()
									oEquipe:PrintLine()
								Endif				

								oTrfCU03:Init()
								oTrfCU03:PrintLine()

								nTotSecao += nQuantAFA
								nTotGeral += nQuantAFA
							EndIf
						EndIf

						AEL->( DbSkip() )
					End
								
					DbSelectArea( "AEN" )
					AEN->( DbSetOrder( 1 ) )
					AEN->( DbSeek( xFilial( "AEN" ) + AF9->( AF9_PROJET + AF9_REVISA + AF9_TAREFA ) ) )
					Do While !AEN->( Eof() ) .And. AF9->( AF9_PROJET + AF9_REVISA + AF9_TAREFA ) == AEN->( AEN_PROJET + AEN_REVISA + AEN_TAREFA )

						If AF9->AF9_TIPO<>'1' .And. "E" $ PmsTPPrdE( AF9->AF9_TPPRDE )
							nProduc := AF9->AF9_PRODUC
						Else
							nProduc := 1
						EndIf

						ExplodCU3(AF9->AF9_PROJET, AF9->AF9_REVISA, AEN->AEN_SUBCOM, AF9->AF9_QUANT * AEN->AEN_QUANT, nProduc, AJY->AJY_INSUMO, @oReport, @oEquipe, @oTrfCU03, @cEquipe, @nCustD, @nTotSecao, @nTotGeral)

						AEN->( DbSkip() )
					EndDo

					AJY->( DbSkip() )
				End
				
			
	
				dbSelectArea("AE8")
				AE8->(dbSkip())
			EndDo

			oTarefa3:Finish()
			oEquipe:Finish()
			
			dbSelectArea("AED")
			AED->(dbSkip())

		EndDo

		// verifica o cancelamento pelo usuario..
		If oReport:Cancel()
			oReport:SkipLine()
			oReport:PrintText(STR0034) //"*** CANCELADO PELO OPERADOR ***"
		Else
			oTotalGer:SetBreak(oBreakGer)
			oEquipe:Init()
			oBreakGer:Execute()
			oEquipe:Finish()
		EndIf
	
	//----------------------------------
	// PROJETO+GRUPO DE TAREFAS+TAREFA+RECURSO
	//----------------------------------	
	Case nOrdem == 4

		oTarefa4:SetTotalInLine()
		
		oTarefa4:Cell("AFA_QUANT")	:SetBlock({|| nQuantAFA})
		oTarefa4:Cell("AFA_CUSTD1"):SetBlock({|| PmsTrunca(cTrunca,AFA->AFA_CUSTD*nQuantAFA,nDecCst,AF9->AF9_QUANT) } )

		oBreak := TRBreak():New(oTarefa4,{||.T.},STR0022) //"Grupo"
		oTotal := TRFunction():New(oTarefa4:Cell("AFA_QUANT"),"NQUANTAFA" ,"ONPRINT",oBreak,/*cTitle*/,/*cPicture*/,{||nTotSecao}/*uFormula*/,.F./*lEndSection*/,.F./*lEndReport*/,.F./*lEndPage*/)

		oBreakPrj := TRBreak():New(oGrupo,{||.T.},STR0025) //"Projeto"
		oTotalPrj := TRFunction():New(oTarefa4:Cell("AFA_QUANT"),"NQUANTAFA" ,"ONPRINT",oBreakPrj,/*cTitle*/,/*cPicture*/,{||nTotProje}/*uFormula*/,.F./*lEndSection*/,.F./*lEndReport*/,.F./*lEndPage*/)

		oBreakGer := TRBreak():New(oProjeto2,{||.T.},STR0021) //"Total Geral"
		oTotalGer := TRFunction():New(oTarefa4:Cell("AFA_QUANT"),"NQUANTAFA" ,"ONPRINT",,/*cTitle*/,/*cPicture*/,{||nTotGeral}/*uFormula*/,.F./*lEndSection*/,.F./*lEndReport*/,.F./*lEndPage*/)

		cGrupo 	:= "XX"
		oReport:SetMeter(AFA->(LastRec()))
	
		dbSelectArea("AF8")
		dbSetOrder(1) // AF8_FILIAL + AF8_PROJET
		MSSeek(xFilial("AF8") + mv_par01,.T.)


		While !Eof() .And. xFilial("AF8") == AF8->AF8_FILIAL ;
					 .And. AF8->AF8_PROJET <= mv_par02 .AND. !oReport:Cancel()
			
			//��������������������������������������������Ŀ
			//�verifica se o projeto usa composicoes aux   �
			//����������������������������������������������
			lUsaAJT :=  AF8ComAJT( AF8->AF8_PROJET )

			If AF8->AF8_DATA < mv_par03 .Or. AF8->AF8_DATA > mv_par04
				dbSelectArea("AF8")
				dbSkip()
				loop
			EndIf
			If !Empty(oProjeto2:GetAdvplExp('AF8')) .And. !AF8->(&(oProjeto2:GetAdvplExp('AF8')))
				dbSelecTArea("AF8")
				dbSkip()
				Loop
			EndIf
			If PmrPertence(AF8->AF8_FASE,MV_PAR06) 

				cTrunca := AF8->AF8_TRUNCA
				cRevisa := If(Empty(mv_par05),AF8->AF8_REVISA,mv_par05)
				 
				dbSelectArea("AF9")
				dbSetOrder(4) //AF9_FILIAL+AF9_PROJET+AF9_REVISA+AF9_GRPCOM+AF9_TAREFA+AF9_ORDEM
				If MSSeek(xFilial()+AF8->AF8_PROJET+cRevisa)
					While !Eof() .And. xFilial()+AF8->AF8_PROJET+cRevisa==;
											AF9->AF9_FILIAL+AF9->AF9_PROJET+AF9->AF9_REVISA
		
			
						dbSelectArea("AE5")
						dbSetOrder(1)
						dbSeek(xFilial()+AF9->AF9_GRPCOM)
						If !Empty(oProjeto2:GetAdvplExp('AE5')) .And. !AF9->(&(oProjeto2:GetAdvplExp('AE5')))
							dbSelecTArea("AF9")
							dbSkip()
							Loop
						EndIf
						// valida o filtro do usuario
						If !Empty(oProjeto2:GetAdvplExp('AF9')) .And. !AF9->(&(oProjeto2:GetAdvplExp('AF9')))
							dbSelecTArea("AF9")
							dbSkip()
							Loop
						EndIf

						//��������������������������������������������������������Ŀ
						//�realiza o tratamento se o projeto usa composicoes aux   �
						//����������������������������������������������������������
						If lUsaAJT
							DbSelecTArea( "AF9" )
							AF9->( DbSetOrder( 1 ) )
							AF9->( DbSeek( xFilial( "AF9" ) + AF8->( AF8_PROJET + AF8_REVISA ) ) )
							While AF9->( !Eof() ) .AND. AF9->( AF9_FILIAL + AF9_PROJET + AF9_REVISA ) == xFilial( "AF9" ) + AF8->( AF8_PROJET + AF8_REVISA )
								If 	(mv_par11==1).Or.; //Todas as tarefas 
									(mv_par11==3.And.!Empty(AF9->AF9_DTATUF)).Or.; //Tarefas finalizadas
									(mv_par11==2.And.Empty(AF9->AF9_DTATUF)) // Tarefas a executar
		
									DbSelecTArea( "AEL" )
									AEL->( DbSetOrder( 1 ) )
									AEL->( DbSeek( xFilial( "AEL" ) + AF9->( AF9_PROJET + AF9_REVISA + AF9_TAREFA ) ) )
									While AEL->( !Eof() ) .AND. AEL->( AEL_FILIAL + AEL_PROJET + AEL_REVISA + AEL_TAREFA ) == xFilial( "AEL" ) + AF9->( AF9_PROJET + AF9_REVISA + AF9_TAREFA )
										DbSelectArea( "AJY" )
										AJY->( DbSetOrder( 1 ) )
										If AJY->( DbSeek( xFilial( "AJY" ) + AEL->( AEL_PROJET + AEL_REVISA + AEL_INSUMO ) ) )
											
											nProduc := 1
											If AF9->AF9_TIPO<>'1' .And. AEL->AEL_GRORGA $ PmsTPPrdE( AF9->AF9_TPPRDE )
												nProduc := AF9->AF9_PRODUC / nProduc
											EndIf

											nCustD		:= AJY->AJY_CUSTD
											nQuantAFA	:= AEL->AEL_QUANT
											nQuantAFA	:= pmsTrunca( "2", nQuantAFA/nProduc, nDecCst )
											nQuantAFA	:= pmsTrunca( "2", nQuantAFA * AF9->AF9_QUANT, nDecCst )
											nQtdIns		:= nQuantAFA

											If cProjeto <> AF8->AF8_PROJET	
												oProjeto2:Init()
												oProjeto2:PrintLine()
												cProjeto	:=	AF8->AF8_PROJET
	
												oGrupo:Init()
												oGrupo:PrintLine()
												cGrupo := AF9->AF9_GRPCOM
												nTotSecao := 0
												nTotProje := 0
												oTrfCU04:Init()
											Else				
												If cGrupo <> AF9->AF9_GRPCOM
													If oGrupo:lPrinting
														oTrfCU04:Finish()
														oGrupo:Finish()
													EndIf
													oReport:SkipLine()
													oGrupo:Init()
													oGrupo:PrintLine()
													cGrupo := AF9->AF9_GRPCOM
													nTotSecao := 0
													oTrfCU04:Init()
												Endif				
											Endif	
		
											oTrfCU04:Init()
											oTrfCU04:PrintLine()

											nTotSecao += nQuantAFA
											nTotProje += nQuantAFA
											nTotGeral += nQuantAFA
		                                EndIf
		
										AEL->( DbSkip() )
		                            End

									DbSelectArea( "AEN" )
									AEN->( DbSetOrder( 1 ) )
									AEN->( DbSeek( xFilial( "AEN" ) + AF9->( AF9_PROJET + AF9_REVISA + AF9_TAREFA ) ) )
									Do While !AEN->( Eof() ) .And. AF9->( AF9_PROJET + AF9_REVISA + AF9_TAREFA ) == AEN->( AEN_PROJET + AEN_REVISA + AEN_TAREFA )

										If AF9->AF9_TIPO<>'1' .And. "E" $ PmsTPPrdE( AF9->AF9_TPPRDE )
											nProduc := AF9->AF9_PRODUC
										Else
											nProduc := 1
										EndIf

			                            ExplodCU4(AF9->AF9_PROJET, AF9->AF9_REVISA, AEN->AEN_SUBCOM, AF9->AF9_QUANT * AEN->AEN_QUANT, nProduc, AJY->AJY_INSUMO, @oReport, @oProjeto2, @oGrupo, @oTrfCU04, @cProjeto, @cGrupo, @nTotSecao, @nTotProje, @nTotGeral)

										AEN->( DbSkip() )
									EndDo

								EndIf
		
								AF9->( DbSkip() )
							End
						Else
							dbSelectArea("AFA")
							dbSetOrder(1)
							If MSSeek(xFilial()+AF9->AF9_PROJET+AF9->AF9_REVISA+AF9->AF9_TAREFA)
			
								While !Eof() .And. xFilial()+AF9->AF9_PROJET+AF9->AF9_REVISA+AF9->AF9_TAREFA==;
														AFA->AFA_FILIAL+AFA->AFA_PROJET+AFA->AFA_REVISA+AFA->AFA_TAREFA
									If !Empty(AFA->AFA_RECURS)
										If AFA->AFA_PROJET < mv_par01 .Or. AFA->AFA_PROJET > mv_par02
											dbSelectArea("AFA")
											dbSkip()
											loop
										EndIf
										// valida o filtro do usuario
										If !Empty(oProjeto2:GetAdvplExp('AFA')) .And. !AFA->(&(oProjeto2:GetAdvplExp('AFA')))
											dbSelecTArea("AFA")
											dbSkip()
											Loop
										EndIf
						
										AE8->(dbSetOrder(1))
										AE8->(MSSeek(xFilial()+AFA->AFA_RECURS))
										If (AE8->AE8_EQUIP < mv_par12 .Or. AE8->AE8_EQUIP > mv_par13)
											dbSelectArea("AFA")
											dbSkip()
											Loop	
										End If
							
	
										dbSelectArea("AFA")
										If !Empty(oProjeto2:GetAdvplExp('AE8')) .And. !AE8->(&(oProjeto2:GetAdvplExp('AE8')))
											dbSelecTArea("AFA")
											dbSkip()
											Loop
										EndIf
							
										If AE8->AE8_RECURS < mv_par07 .Or. AE8->AE8_RECURS > mv_par08
											dbSelectArea("AFA")
											dbSkip()
											Loop
										EndIf
										If (!Empty(mv_par05) .And. AFA->AFA_REVISA != mv_par05) .Or.;
										    (Empty(mv_par05) .And. AFA->AFA_REVISA != AF8->AF8_REVISA)
											dbSelectArea("AFA")
											dbSkip()
											loop
										EndIf
										If (AFA->AFA_START < mv_par09 .And. AFA->AFA_FINISH < mv_par09) .Or.;
											(AFA->AFA_START > mv_par10 .And. AFA->AFA_FINISH > mv_par10)
											dbSelectArea("AFA")
											dbSkip()
											loop
										EndIf
	
										AED->(dbSetOrder(1))
										AED->(MSSeek(xFilial() + AE8->AE8_EQUIP))
										// valida o filtro do usuario
										If !Empty(oProjeto2:GetAdvplExp('AED')) .And. !AED->(&(oProjeto2:GetAdvplExp('AED')))
											dbSelecTArea("AFA")
											dbSkip()
											Loop
										EndIf
											
		 								If !Empty(AFA->AFA_PRODUT)
											SB1->(dbSetOrder(1))
											SB1->(MSSeek(xFilial()+AFA->AFA_PRODUT))
											// valida o filtro do usuario
											If !Empty(oProjeto:GetAdvplExp('SB1')) .And. !SB1->(&(oProjeto:GetAdvplExp('SB1')))
												dbSelecTArea("AFA")
												dbSkip()
												Loop
											EndIf
										Endif
							
										If	(mv_par11==1).Or.; //Todas as tarefas 
											(mv_par11==3.And.!Empty(AF9->AF9_DTATUF)).Or.; //Tarefas finalizadas
											(mv_par11==2.And.Empty(AF9->AF9_DTATUF)) // Tarefas a executar
					
											If cProjeto <> AF8->AF8_PROJET
												
												oProjeto2:Init()
												oProjeto2:PrintLine()
												cProjeto	:=	AF8->AF8_PROJET
	
												oGrupo:Init()
												oGrupo:PrintLine()
												cGrupo := AF9->AF9_GRPCOM
												nTotSecao := 0
												nTotProje := 0
												oTarefa4:Init()
											Else				
												If cGrupo <> AF9->AF9_GRPCOM
													If oGrupo:lPrinting
														oTarefa4:Finish()
														oGrupo:Finish()
													EndIf
													oReport:SkipLine()
													oGrupo:Init()
													oGrupo:PrintLine()
													cGrupo := AF9->AF9_GRPCOM
													nTotSecao := 0
													oTarefa4:Init()
												Endif				
											Endif		
	
											nQuantAFA := PmsAFAQuant(AFA->AFA_PROJET,AFA->AFA_REVISA,AFA->AFA_TAREFA,AFA->AFA_PRODUT,AF9->AF9_QUANT,AFA->AFA_QUANT,AF9->AF9_HDURAC)
						
											oTarefa4:PrintLine()
											nTotSecao += nQuantAFA
											nTotProje += nQuantAFA
											nTotGeral += nQuantAFA
	
										EndIf
									EndIf
						
									dbSelectArea("AFA")
									dbSkip()
									oReport:IncMeter()
									
								EndDo
							EndIf
						EndIf

						dbSelectArea("AF9")
						dbSkip()
					EndDo
				
					If oTarefa4:lPrinting .OR. oTrfCU04:lPrinting
						oTarefa4:Finish()
						oGrupo:Finish()
						oProjeto2:Finish()
					EndIf
		   		EndIf
			EndIf
            
			dbSelectArea("AF8")
			dbSkip()

		EndDo

		// verifica o cancelamento pelo usuario..
		If oReport:Cancel()
			oReport:SkipLine()
			oReport:PrintText(STR0034) //"*** CANCELADO PELO OPERADOR ***"
		Else
			oTotalGer:SetBreak(oBreakGer)
			oProjeto2:Init()
			oBreakGer:Execute()
			oProjeto2:Finish()
		EndIf

	//------------------------------------------
	// RECURSO + PRJ + TAREFA + DATA DE ALOCACAO
	//------------------------------------------
	Case nOrdem == 5
		
		oTarefa5:Cell("AF8_PROJET"):SetBlock({||aDados[nX,1]})
		oTarefa5:Cell("AF8_DESCRI"):SetBlock({||aDados[nX,2]})
		oTarefa5:Cell("AF9_TAREFA"):SetBlock({||aDados[nX,3]})
		oTarefa5:Cell("AF9_DESCRI"):SetBlock({||aDados[nX,4]})
		oTarefa5:Cell("AFA_START") :SetBlock({||aDados[nX,5]})
		oTarefa5:Cell("AFA_HORAI") :SetBlock({||aDados[nX,6]})
		oTarefa5:Cell("AFA_FINISH"):SetBlock({||aDados[nX,7]})
		oTarefa5:Cell("AFA_HORAF") :SetBlock({||aDados[nX,8]})
		oTarefa5:Cell("AFA_ALOC")  :SetBlock({||aDados[nX,9]})
		oTarefa5:Cell("AFA_QUANT") :SetBlock({||aDados[nX,10]})
		oTarefa5:Cell("B1_UM")     :SetBlock({||aDados[nX,11]})
		oTarefa5:Cell("AFA_CUSTD") :SetBlock({||aDados[nX,12]})
		oTarefa5:Cell("AFA_CUSTD1"):SetBlock({||aDados[nX,13]})

		TRPosition():New(oTarefa5,"AF9",1,{|| AF9->(DbGoTo(aDados[nX,14])) },.F.)
		TRPosition():New(oTarefa5,"AF8",1,{|| AF8->(DbGoTo(aDados[nX,15])) },.F.)
		TRPosition():New(oTarefa5,"AFA",1,{|| AFA->(DbGoTo(aDados[nX,16])) },.F.)
		TRPosition():New(oTarefa5,"AED",1,{|| AED->(DbGoTo(aDados[nX,17])) },.F.)
		TRPosition():New(oTarefa5,"SB1",1,{|| SB1->(DbGoTo(aDados[nX,18])) },.F.)

		oBreak := TRBreak():New(oTarefa5,{||.T.},STR0009) //"Total"
		oTotal := TRFunction():New(oTarefa5:Cell("AFA_QUANT"),"NQUANTAFA" ,"ONPRINT",oBreak,STR0009/*cTitle*/,/*cPicture*/,{||nTotSecao}/*uFormula*/,.F./*lEndSection*/,.F./*lEndReport*/,.F./*lEndPage*/) //"Total"

		oBreakGer := TRBreak():New(oRecurso5,{||.T.},STR0021) //"Total Geral"
		oTotalGer := TRFunction():New(oTarefa5:Cell("AFA_QUANT"),"NQUANTAFA" ,"ONPRINT",,/*cTitle*/,/*cPicture*/,{||nTotGeral}/*uFormula*/,.F./*lEndSection*/,.F./*lEndReport*/,.F./*lEndPage*/)

    	oReport:SetMeter(AFA->(LastRec()))

		dbSelectArea("AE8")
		dbSetOrder(1)  // AE8_FILIAL + AE8_RECURS
		AE8->(DbSeek(xFilial("AE8") + mv_par07, .T.))

		While AE8->(!Eof()) .And. xFilial("AE8") == AE8->AE8_FILIAL .And. AE8->AE8_RECURS <= mv_par08
			
			If (AE8->AE8_EQUIP < mv_par12 .Or. AE8->AE8_EQUIP > mv_par13)
				AE8->(DbSkip())
				Loop	
			EndIf
			// valida o filtro do usuario
			If !Empty(oRecurso:GetAdvplExp('AE8')) .And. !AE8->(&(oRecurso:GetAdvplExp('AE8')))
				AE8->(DbSkip())
				Loop
			EndIf
			
			nX     := 0
			aDados := {}

			dbSelectArea("AFA")
			dbSetOrder(3) //AFA_FILIAL+AFA_RECURS+DTOS(AFA_START)+AFA_HORAI
			AFA->(DbSeek(xFilial("AFA")+AE8->AE8_RECURS))
			While AFA->(!Eof()) .And. AFA->AFA_FILIAL==xFilial("AFA") .And. AFA->AFA_RECURS==AE8->AE8_RECURS

				// valida o filtro do usuario
				If !Empty(oRecurso:GetAdvplExp('AFA')) .And. !AFA->(&(oRecurso:GetAdvplExp('AFA')))
					dbSelecTArea("AFA")
					dbSkip()
					Loop
				EndIf

				AF8->(dbSetOrder(1))
				AF8->(MSSeek(xFilial()+AFA->AFA_PROJET))
				// valida o filtro do usuario
				If !Empty(oRecurso:GetAdvplExp('AF8')) .And. !AF8->(&(oRecurso:GetAdvplExp('AF8')))
					dbSelecTArea("AFA")
					dbSkip()
					Loop
				EndIf
				
				If PmrPertence(AF8->AF8_FASE,MV_PAR06) 
					// valida o filtro do usuario
					dbSelecTArea("AF9")
					dbSetOrder(1)
					MsSeek(xFilial()+AFA->AFA_PROJET+AFA->AFA_REVISA+AFA->AFA_TAREFA)
					// valida o filtro do usuario
					If !Empty(oRecurso:GetAdvplExp('AF9')) .And. !AF9->(&(oRecurso:GetAdvplExp('AF9')))
						dbSelecTArea("AFA")
						dbSkip()
						Loop
					EndIf

					dbSelecTArea("AFA")
					If AFA->AFA_PROJET < mv_par01 .Or. AFA->AFA_PROJET > mv_par02
						dbSelectArea("AFA")
						dbSkip()
						Loop
					EndIf
					
					If AF8->AF8_DATA < mv_par03 .Or. AF8->AF8_DATA > mv_par04
						dbSelectArea("AFA")
						dbSkip()
						Loop
					EndIf
					
					If (!Empty(mv_par05) .And. AFA->AFA_REVISA != mv_par05) .Or.;
					    (Empty(mv_par05) .And. AFA->AFA_REVISA != AF8->AF8_REVISA)
						dbSelectArea("AFA")
						dbSkip()
						Loop
					EndIf
	
					If (AFA->AFA_START < mv_par09 .And. AFA->AFA_FINISH < mv_par09) .Or.;
						(AFA->AFA_START > mv_par10 .And. AFA->AFA_FINISH > mv_par10)
						dbSelectArea("AFA")
						dbSkip()
						Loop
					EndIf
		
					If (mv_par11==1).Or.; //Todas as tarefas 
					   (mv_par11==3.And.!Empty(AF9->AF9_DTATUF)).Or.; //Tarefas finalizadas
					   (mv_par11==2.And.Empty(AF9->AF9_DTATUF)) // Tarefas a executar

						dbSelectArea("AED")
						AED->(dbSetOrder(1))
						AED->(MSSeek(xFilial() + AE8->AE8_EQUIP))
	
						// valida o filtro do usuario
						If !Empty(oRecurso:GetAdvplExp('AED')) .And. !AED->(&(oRecurso:GetAdvplExp('AED')))
							dbSelecTArea("AFA")
							dbSkip()
							Loop
						EndIf
						If !Empty(AFA->AFA_PRODUT)
							SB1->(dbSetOrder(1))
							SB1->(MSSeek(xFilial()+AFA->AFA_PRODUT))
							// valida o filtro do usuario
							If !Empty(oRecurso:GetAdvplExp('SB1')) .And. !SB1->(&(oRecurso:GetAdvplExp('SB1')))
								dbSelecTArea("AFA")
								dbSkip()
								Loop
							EndIf
						Endif

						If cRecurso <> AE8->AE8_RECURS
							oReport:SkipLine()
							cRecurso := AE8->AE8_RECURS
							
							oRecurso5:Init()
							oRecurso5:PrintLine()
							oTarefa5:Init()
							nTotSecao := 0
						Endif

						nQuantAFA := PmsAFAQuant(AFA->AFA_PROJET,AFA->AFA_REVISA,AFA->AFA_TAREFA,AFA->AFA_PRODUT,AF9->AF9_QUANT,AFA->AFA_QUANT,AF9->AF9_HDURAC)

						aAdd(aDados,Array(18))
						nX++
						aDados[nX,1] := AF8->AF8_PROJET
						aDados[nX,2] := AF8->AF8_DESCRI
						aDados[nX,3] := AF9->AF9_TAREFA
						aDados[nX,4] := AF9->AF9_DESCRI
						aDados[nX,5] := AFA->AFA_START
						aDados[nX,6] := AFA->AFA_HORAI
						aDados[nX,7] := AFA->AFA_FINISH
						aDados[nX,8] := AFA->AFA_HORAF
						aDados[nX,9] := AFA->AFA_ALOC
						aDados[nX,10] := nQuantAFA
						aDados[nX,11] := If(!Empty(AFA->AFA_PRODUT),SB1->B1_UM,"")
						aDados[nX,12] := AFA->AFA_CUSTD
						aDados[nX,13] := PmsTrunca(cTrunca,AFA->AFA_CUSTD*nQuantAFA,nDecCst,AF9->AF9_QUANT)
						aDados[nX,14] := AF9->(Recno())
						aDados[nX,15] := AF8->(Recno())
						aDados[nX,16] := AFA->(Recno())
						aDados[nX,17] := AED->(Recno())
						aDados[nX,18] := SB1->(Recno())
						
						//oTarefa2:PrintLine()
						nTotSecao += nQuantAFA
						nTotGeral += nQuantAFA
						
					EndIf
				EndIf
				
				dbSelectArea("AFA")
				dbSkip()    
				
				oReport:IncMeter()
			EndDo

			//����������������������������������������������������������������������Ŀ
			//�Verifica alocacao dos recursos nos projetos que usam composicao aux.  �
			//������������������������������������������������������������������������
			DbSelectArea( "AJY" )
			AJY->( DbSetOrder( 2 ) )
			AJY->( DbSeek( xFilial( "AJY" ) + AE8->AE8_RECURS ) )
			While AJY->( !Eof() ) .AND. AJY->( AJY_FILIAL + AJY_RECURS ) == xFilial( "AJY" ) + AE8->AE8_RECURS
				If !AF8ComAJT( AJY->AJY_PROJET )
					AJY->( DbSkip() )
					Loop
				EndIf
	
				DbSelectArea( "AF8" )
				AF8->( DbSetOrder( 1 ) )
				AF8->( DbSeek( xFilial( "AF8" ) + AJY->AJY_PROJET ) )
				If PmrPertence( AF8->AF8_FASE, MV_PAR06 )
					If AJY->AJY_PROJET < mv_par01 .Or. AJY->AJY_PROJET > mv_par02
						AJY->( DbSkip() )
						Loop
					EndIf
					
					If AF8->AF8_DATA < mv_par03 .Or. AF8->AF8_DATA > mv_par04
						AJY->( DbSkip() )
						Loop
					EndIf
					
					If (!Empty(mv_par05) .And. AJY->AJY_REVISA != mv_par05) .Or.;
					    (Empty(mv_par05) .And. AJY->AJY_REVISA != AF8->AF8_REVISA)
						AJY->( DbSkip() )
						Loop
					EndIf

					oTarefa2:Init()

					DbSelectArea( "AEL" )
					AEL->( DbSetOrder( 2 ) )
					AEL->( DbSeek( xFilial( "AEL" ) + AJY->( AJY_PROJET + AJY_REVISA + AJY_INSUMO ) ) )
					While AEL->( !Eof() ) .AND. AEL->( AEL_FILIAL + AEL_PROJET + AEL_REVISA + AEL_INSUMO ) == xFilial( "AEL" ) + AJY->( AJY_PROJET + AJY_REVISA + AJY_INSUMO )
						DbSelectArea( "AF9" )
						AF9->( DbSetOrder( 1 ) )
						If AF9->( DbSeek( xFilial( "AF9" ) + AEL->( AEL_PROJET + AEL_REVISA + AEL_TAREFA ) ) )
							If (AF9->AF9_START < mv_par09 .And. AF9->AF9_FINISH < mv_par09) .Or.;
							   (AF9->AF9_START > mv_par10 .And. AF9->AF9_FINISH > mv_par10)
								AEL->( DbSkip() )
								Loop
							EndIf
					
							If (mv_par11==1).Or.; //Todas as tarefas 
							   (mv_par11==3.And.!Empty(AF9->AF9_DTATUF)).Or.; //Tarefas finalizadas
							   (mv_par11==2.And.Empty(AF9->AF9_DTATUF)) // Tarefas a executar

								dbSelectArea("AED")
								AED->(dbSetOrder(1))
								AED->(MSSeek(xFilial() + AE8->AE8_EQUIP))
			
								// valida o filtro do usuario
								If !Empty(oRecurso:GetAdvplExp('AED')) .And. !AED->(&(oRecurso:GetAdvplExp('AED')))
									AEL->( DbSkip() )
									Loop
								EndIf
								If !Empty(AJY->AJY_PRODUT)
									SB1->(dbSetOrder(1))
									SB1->(MSSeek(xFilial()+AJY->AJY_PRODUT))
									// valida o filtro do usuario
									If !Empty(oRecurso:GetAdvplExp('SB1')) .And. !SB1->(&(oRecurso:GetAdvplExp('SB1')))
										AEL->( DbSkip() )
										Loop
									EndIf
								Endif
		
								nProduc := 1
								If AF9->AF9_TIPO<>'1' .And. AEL->AEL_GRORGA $ PmsTPPrdE( AF9->AF9_TPPRDE )
									nProduc := AF9->AF9_PRODUC / nProduc
								EndIf

								nQuantAFA	:= AEL->AEL_QUANT
								nQuantAFA	:= pmsTrunca( "2", nQuantAFA/nProduc, nDecCst )
								nQuantAFA	:= pmsTrunca( "2", nQuantAFA * AF9->AF9_QUANT, nDecCst )
								nQtdIns		:= nQuantAFA

								If cRecurso <> AE8->AE8_RECURS
									oReport:SkipLine()
									cRecurso := AE8->AE8_RECURS
									
									oRecurso5:Init()
									oRecurso5:PrintLine()
									oTarefa5:Init()
									nTotSecao := 0
								Endif
								
								aAdd(aDados,Array(18))
								nX++
								aDados[nX,1] := AF8->AF8_PROJET
								aDados[nX,2] := AF8->AF8_DESCRI
								aDados[nX,3] := AF9->AF9_TAREFA
								aDados[nX,4] := AF9->AF9_DESCRI
								aDados[nX,5] := AF9->AF9_START
								aDados[nX,6] := AF9->AF9_HORAI
								aDados[nX,7] := AF9->AF9_FINISH
								aDados[nX,8] := AF9->AF9_HORAF
								aDados[nX,9] := GetAloc()
								aDados[nX,10] := nQuantAFA
								aDados[nX,11] := If(!Empty(AJY->AJY_PRODUT),SB1->B1_UM,"")
								aDados[nX,12] := AJY->AJY_CUSTD
								aDados[nX,13] := PmsTrunca(cTrunca,AJY->AJY_CUSTD*nQuantAFA,nDecCst,AF9->AF9_QUANT)
								aDados[nX,14] := AF9->(Recno())
								aDados[nX,15] := AF8->(Recno())
								aDados[nX,16] := AJY->(Recno())
								aDados[nX,17] := AED->(Recno())
								aDados[nX,18] := SB1->(Recno())
								
								nTotSecao += nQuantAFA
								nTotGeral += nQuantAFA
							EndIf
						EndIf

						AEL->( DbSkip() )
					End

					DbSelectArea( "AEN" )
					AEN->( DbSetOrder( 1 ) )
					AEN->( DbSeek( xFilial( "AEN" ) + AF9->( AF9_PROJET + AF9_REVISA + AF9_TAREFA ) ) )
					Do While !AEN->( Eof() ) .And. AF9->( AF9_PROJET + AF9_REVISA + AF9_TAREFA ) == AEN->( AEN_PROJET + AEN_REVISA + AEN_TAREFA )

						If AF9->AF9_TIPO<>'1' .And. "E" $ PmsTPPrdE( AF9->AF9_TPPRDE )
							nProduc := AF9->AF9_PRODUC
						Else
							nProduc := 1
						EndIf

						ExplodCU5(AF9->AF9_PROJET, AF9->AF9_REVISA, AEN->AEN_SUBCOM, AF9->AF9_QUANT * AEN->AEN_QUANT, nProduc, AJY->AJY_INSUMO, @oReport, @oRecurso5, @oTarefa5, @cRecurso, @aDados, @nTotSecao, @nTotGeral)

						AEN->( DbSkip() )
					EndDo

				EndIf
				
				AJY->( DbSkip() )
			End
			

			aSort(aDados,,,{|x,y| x[1]+x[3]+DtoS(x[5])+x[6] < y[1]+y[3]+DtoS(y[5])+y[6] })
			For nX:=1 to Len(aDados)
				oTarefa5:PrintLine()
			Next
			
			oTarefa5:Finish()
			oRecurso5:Finish()

			dbSelectArea("AE8")
			dbSkip()

		EndDo

		// verifica o cancelamento pelo usuario..
		If oReport:Cancel()
			oReport:SkipLine()
			oReport:PrintText(STR0034) //"*** CANCELADO PELO OPERADOR ***"
		Else
			oTotalGer:SetBreak(oBreakGer)
			oRecurso5:Init()
			oBreakGer:Execute()
			oRecurso5:Finish()
		EndIf

EndCase

Return

/*/
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
��������������������������������������������������������������������������������Ŀ��
���Programa  �GetProjDe� Autor � Adriano Ueda                  � Data �28.05.2003���
��������������������������������������������������������������������������������Ĵ��
���Descri��o �Retorna a descricao de um projeto a partir de seu codigo           ���
���������������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
/*/
Function GetProjDesc(cCode)
Local aAreaAF8 := GetArea("AF8")
Local cBuffer  := ""

dbSelectArea("AF8")
AF8->(dbSetOrder(1))

If AF8->(MSSeek(xFilial("AF8") + cCode))
	cBuffer := AF8->AF8_DESCRI
EndIf

RestArea(aAreaAF8)
Return cBuffer


/*/
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
��������������������������������������������������������������������������������Ŀ��
���Programa  �GetAloc  � Autor � Totvs                         � Data �03.06.2009���
��������������������������������������������������������������������������������Ĵ��
���Descri��o �Retorna o percentual de alocacao do recurso                        ���
���������������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
/*/
Static Function GetAloc()
Local nQuant	:= PmsAELQuant(AF9->AF9_PROJET, AF9->AF9_REVISA, AF9->AF9_TAREFA, AF9->AF9_QUANT, nQtdIns, .T. )
Local nHrsItvs	:= PmsHrsItvl(AF9->AF9_START,AF9->AF9_HORAI,AF9->AF9_FINISH,AF9->AF9_HORAF,If(!Empty(AE8->AE8_CALEND),AE8->AE8_CALEND,AF9->AF9_CALEND),AF9->AF9_PROJET,AE8->AE8_RECURS)
Return nQuant/nHrsItvs * 100


/*/
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
��������������������������������������������������������������������������������Ŀ��
���Programa  �ExplodCU1� Autor � Marcelo Akama                 � Data �08/04/2010���
��������������������������������������������������������������������������������Ĵ��
���Descri��o �Explode a composicao auxiliar para impressao dos recursos          ���
���������������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
/*/

Static Function ExplodCU1(cProjet, cRevisa, cCompun, nQtd, nProdEqp, oReport, oProjeto, oTarefa, oTrfCU01, cProjeto, nCustD, nTotSecao, nTotGeral)
Local aArea		:= GetArea()
Local aAreaAJT	:= AJT->(GetArea())
Local aAreaAJX	:= AJX->(GetArea())
Local nQuantAFA	:= 0
Local nProduc	:= 0
Local nDecCst	:= TamSX3("AF9_CUSTO")[2]

DbSelectArea( "AJT" )
AJT->( DbSetOrder( 2 ) ) //AJT_FILIAL+AJT_PROJET+AJT_REVISA+AJT_COMPUN
If AJT->( DbSeek( xFilial( "AJT" ) + cProjet + cRevisa + cCompun ) )
	Do While !AJT->( Eof() ) .And. AJT->( AJT_FILIAL+AJT_PROJET+AJT_REVISA+AJT_COMPUN == xFilial( "AJT" ) + cProjet + cRevisa + cCompun )
		// Verifica os insumos e recursos da composicao aux
		DbSelecTArea( "AJU" ) //AJU_FILIAL+AJU_PROJET+AJU_REVISA+AJU_COMPUN+AJU_ITEM
		AJU->( DbSetOrder( 2 ) )
		AJU->( DbSeek( xFilial( "AJU" ) + cProjet + cRevisa + cCompun ) )
		Do While !AJU->( Eof() ) .And. AJU->( AJU_FILIAL + AJU_PROJET + AJU_REVISA + AJU_COMPUN ) == xFilial( "AJU" ) + cProjet + cRevisa + cCompun
			DbSelectArea( "AJY" )
			AJY->( DbSetOrder( 1 ) )
			If AJY->( DbSeek( xFilial( "AJY" ) + AJU->( AJU_PROJET + AJU_REVISA + AJU_INSUMO ) ) )
				AE8->( DbSetOrder( 1 ) )
				AE8->( DbSeek( xFilial( "AE8" ) + AJY->AJY_RECURS ) )
				If AJY->AJY_RECURS >= mv_par07 .AND. AJY->AJY_RECURS <= mv_par08

					nProduc := nProdEqp
					If AJT->AJT_TIPO<>'1' .And. AJU->AJU_GRORGA $ PmsTPPrdE( AJT->AJT_TPPRDE )
						nProduc := AJT->AJT_PRODUC / nProduc
					EndIf

					nCustD		:= AJY->AJY_CUSTD
					nQuantAFA	:= AJU->AJU_QUANT
					nQuantAFA	:= pmsTrunca( "2", nQuantAFA/nProduc, nDecCst )
					nQuantAFA	:= pmsTrunca( "2", nQuantAFA * nQtd, nDecCst )
					nQtdIns		:= nQuantAFA

					If cProjeto <> cProjet
						oReport:SkipLine()
						oProjeto:Init()
						oProjeto:PrintLine()
						oTarefa:Init()
						cProjeto	:=	cProjet
						nTotSecao  := 0
					Endif	

					nTotSecao	+= nQuantAFA
					nTotGeral	+= nQuantAFA

					oTrfCU01:Init()
					oTrfCU01:PrintLine()
				EndIf
			EndIf

			AJU->( DbSkip() )

		EndDo

		// Verifica as subcomposicoes da composicao aux
		DbSelectArea( "AJX" )
		AJX->( DbSetOrder( 2 ) )
		AJX->( DbSeek( xFilial( "AJX" ) + cProjet + cRevisa + cCompun ) )
		Do While !AJX->( Eof() ) .And. cProjet + cRevisa + cCompun == AJX->( AJX_PROJET + AJX_REVISA + AJX_COMPUN )

			nProduc := nProdEqp
			If AJT->AJT_TIPO<>'1' .And. "E" $ PmsTPPrdE( AJT->AJT_TPPRDE )
				nProduc := nProduc / AJT->AJT_PRODUC
			EndIf
					
			ExplodCU1(cProjet, cRevisa, AJX->AJX_SUBCOM, AJX->AJX_QUANT * nQtd, nProduc, @oReport, @oProjeto, @oTarefa, @oTrfCU01, @cProjeto, @nCustD, @nTotSecao, @nTotGeral)

			AJX->( DbSkip() )
		EndDo

		AJT->( DbSkip() )
		
	EndDo
	
EndIf

RestArea(aAreaAJT)
RestArea(aAreaAJX)
RestArea(aArea)

Return

/*/
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
��������������������������������������������������������������������������������Ŀ��
���Programa  �ExplodCU2� Autor � Marcelo Akama                 � Data �10/04/2010���
��������������������������������������������������������������������������������Ĵ��
���Descri��o �Explode a composicao auxiliar para impressao dos recursos          ���
���������������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
/*/

Static Function ExplodCU2(cProjet, cRevisa, cCompun, nQtd, nProdEqp, cInsumo, oReport, oRecurso, oTrfCU02, cRecurso, nCustD, nTotSecao, nTotGeral)
Local aArea		:= GetArea()
Local aAreaAJT	:= AJT->(GetArea())
Local aAreaAJX	:= AJX->(GetArea())
Local nQuantAFA	:= 0
Local nProduc	:= 0
Local nDecCst	:= TamSX3("AF9_CUSTO")[2]

DbSelectArea( "AJT" )
AJT->( DbSetOrder( 2 ) ) //AJT_FILIAL+AJT_PROJET+AJT_REVISA+AJT_COMPUN
If AJT->( DbSeek( xFilial( "AJT" ) + cProjet + cRevisa + cCompun ) )
	Do While !AJT->( Eof() ) .And. AJT->( AJT_FILIAL+AJT_PROJET+AJT_REVISA+AJT_COMPUN == xFilial( "AJT" ) + cProjet + cRevisa + cCompun )
		// Verifica os insumos e recursos da composicao aux
		DbSelecTArea( "AJU" )
		AJU->( DbSetOrder( 3 ) ) //AJU_FILIAL+AJU_PROJET+AJU_REVISA+AJU_COMPUN+AJU_INSUMO
		AJU->( DbSeek( xFilial( "AJU" ) + cProjet + cRevisa + cCompun + cInsumo ) )
		Do While !AJU->( Eof() ) .And. AJU->( AJU_FILIAL + AJU_PROJET + AJU_REVISA + AJU_COMPUN + AJU_INSUMO ) == xFilial( "AJU" ) + cProjet + cRevisa + cCompun + cInsumo
			DbSelectArea( "AJY" )
			AJY->( DbSetOrder( 1 ) )
			If AJY->( DbSeek( xFilial( "AJY" ) + AJU->( AJU_PROJET + AJU_REVISA + AJU_INSUMO ) ) )
				AE8->( DbSetOrder( 1 ) )
				AE8->( DbSeek( xFilial( "AE8" ) + AJY->AJY_RECURS ) )
				If AJY->AJY_RECURS >= mv_par07 .AND. AJY->AJY_RECURS <= mv_par08

					nProduc := nProdEqp
					If AJT->AJT_TIPO<>'1' .And. AJU->AJU_GRORGA $ PmsTPPrdE( AJT->AJT_TPPRDE )
						nProduc := AJT->AJT_PRODUC / nProduc
					EndIf

					nCustD		:= AJY->AJY_CUSTD
					nQuantAFA	:= AJU->AJU_QUANT
					nQuantAFA	:= pmsTrunca( "2", nQuantAFA/nProduc, nDecCst )
					nQuantAFA	:= pmsTrunca( "2", nQuantAFA * nQtd, nDecCst )
					nQtdIns		:= nQuantAFA

					If cRecurso <> AE8->AE8_RECURS
						oReport:SkipLine()
						cRecurso := AE8->AE8_RECURS

						oRecurso:Init()
						oRecurso:PrintLine()
					Endif

					nTotSecao	+= nQuantAFA
					nTotGeral	+= nQuantAFA

					oTrfCU02:Init()
					oTrfCU02:PrintLine()
				EndIf
			EndIf

			AJU->( DbSkip() )

		EndDo

		// Verifica as subcomposicoes da composicao aux
		DbSelectArea( "AJX" )
		AJX->( DbSetOrder( 2 ) )
		AJX->( DbSeek( xFilial( "AJX" ) + cProjet + cRevisa + cCompun ) )
		Do While !AJX->( Eof() ) .And. cProjet + cRevisa + cCompun == AJX->( AJX_PROJET + AJX_REVISA + AJX_COMPUN )

			nProduc := nProdEqp
			If AJT->AJT_TIPO<>'1' .And. "E" $ PmsTPPrdE( AJT->AJT_TPPRDE )
				nProduc := nProduc / AJT->AJT_PRODUC
			EndIf
					
			ExplodCU2(cProjet, cRevisa, AJX->AJX_SUBCOM, nQtd * AJX->AJX_QUANT, nProduc, cInsumo, @oReport, @oRecurso, @oTrfCU02, @cRecurso, @nCustD, @nTotSecao, @nTotGeral)

			AJX->( DbSkip() )
		EndDo

		AJT->( DbSkip() )
		
	EndDo
	
EndIf

RestArea(aAreaAJT)
RestArea(aAreaAJX)
RestArea(aArea)

Return

/*/
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
��������������������������������������������������������������������������������Ŀ��
���Programa  �ExplodCU3� Autor � Marcelo Akama                 � Data �10/04/2010���
��������������������������������������������������������������������������������Ĵ��
���Descri��o �Explode a composicao auxiliar para impressao dos recursos          ���
���������������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
/*/

Static Function ExplodCU3(cProjet, cRevisa, cCompun, nQtd, nProdEqp, cInsumo, oReport, oEquipe, oTrfCU03, cEquipe, nCustD, nTotSecao, nTotGeral)
Local aArea		:= GetArea()
Local aAreaAJT	:= AJT->(GetArea())
Local aAreaAJX	:= AJX->(GetArea())
Local nQuantAFA	:= 0
Local nProduc	:= 0
Local nDecCst	:= TamSX3("AF9_CUSTO")[2]

DbSelectArea( "AJT" )
AJT->( DbSetOrder( 2 ) ) //AJT_FILIAL+AJT_PROJET+AJT_REVISA+AJT_COMPUN
If AJT->( DbSeek( xFilial( "AJT" ) + cProjet + cRevisa + cCompun ) )
	Do While !AJT->( Eof() ) .And. AJT->( AJT_FILIAL+AJT_PROJET+AJT_REVISA+AJT_COMPUN == xFilial( "AJT" ) + cProjet + cRevisa + cCompun )
		// Verifica os insumos e recursos da composicao aux
		DbSelecTArea( "AJU" )
		AJU->( DbSetOrder( 3 ) ) //AJU_FILIAL+AJU_PROJET+AJU_REVISA+AJU_COMPUN+AJU_INSUMO
		AJU->( DbSeek( xFilial( "AJU" ) + cProjet + cRevisa + cCompun + cInsumo ) )
		Do While !AJU->( Eof() ) .And. AJU->( AJU_FILIAL + AJU_PROJET + AJU_REVISA + AJU_COMPUN + AJU_INSUMO ) == xFilial( "AJU" ) + cProjet + cRevisa + cCompun + cInsumo
			DbSelectArea( "AJY" )
			AJY->( DbSetOrder( 1 ) )
			If AJY->( DbSeek( xFilial( "AJY" ) + AJU->( AJU_PROJET + AJU_REVISA + AJU_INSUMO ) ) )

				nProduc := nProdEqp
				If AJT->AJT_TIPO<>'1' .And. AJU->AJU_GRORGA $ PmsTPPrdE( AJT->AJT_TPPRDE )
					nProduc := AJT->AJT_PRODUC / nProduc
				EndIf

				nCustD		:= AJY->AJY_CUSTD
				nQuantAFA	:= AJU->AJU_QUANT
				nQuantAFA	:= pmsTrunca( "2", nQuantAFA/nProduc, nDecCst )
				nQuantAFA	:= pmsTrunca( "2", nQuantAFA * nQtd, nDecCst )
				nQtdIns		:= nQuantAFA

				If cEquipe <> AED->AED_EQUIP
					nQtdIns := AEL->AEL_QUANT
					cEquipe := AED->AED_EQUIP
					oEquipe:Init()
					oEquipe:PrintLine()
				Endif				

				nTotSecao	+= nQuantAFA
				nTotGeral	+= nQuantAFA

				oTrfCU03:Init()
				oTrfCU03:PrintLine()
			EndIf

			AJU->( DbSkip() )

		EndDo

		// Verifica as subcomposicoes da composicao aux
		DbSelectArea( "AJX" )
		AJX->( DbSetOrder( 2 ) )
		AJX->( DbSeek( xFilial( "AJX" ) + cProjet + cRevisa + cCompun ) )
		Do While !AJX->( Eof() ) .And. cProjet + cRevisa + cCompun == AJX->( AJX_PROJET + AJX_REVISA + AJX_COMPUN )

			nProduc := nProdEqp
			If AJT->AJT_TIPO<>'1' .And. "E" $ PmsTPPrdE( AJT->AJT_TPPRDE )
				nProduc := nProduc / AJT->AJT_PRODUC
			EndIf
					
			ExplodCU3(cProjet, cRevisa, AJX->AJX_SUBCOM, nQtd * AJX->AJX_QUANT, nProduc, cInsumo, @oReport, @oEquipe, @oTrfCU03, @cEquipe, @nCustD, @nTotSecao, @nTotGeral)

			AJX->( DbSkip() )
		EndDo

		AJT->( DbSkip() )
		
	EndDo
	
EndIf

RestArea(aAreaAJT)
RestArea(aAreaAJX)
RestArea(aArea)

Return

/*/
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
��������������������������������������������������������������������������������Ŀ��
���Programa  �ExplodCU4� Autor � Marcelo Akama                 � Data �12/04/2010���
��������������������������������������������������������������������������������Ĵ��
���Descri��o �Explode a composicao auxiliar para impressao dos recursos          ���
���������������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
/*/

Static Function ExplodCU4(cProjet, cRevisa, cCompun, nQtd, nProdEqp, cInsumo, oReport, oProjeto2, oGrupo, oTrfCU04, cProjeto, cGrupo, nTotSecao, nTotProje, nTotGeral)
Local aArea		:= GetArea()
Local aAreaAJT	:= AJT->(GetArea())
Local aAreaAJX	:= AJX->(GetArea())
Local nQuantAFA	:= 0
Local nProduc	:= 0
Local nDecCst	:= TamSX3("AF9_CUSTO")[2]

DbSelectArea( "AJT" )
AJT->( DbSetOrder( 2 ) ) //AJT_FILIAL+AJT_PROJET+AJT_REVISA+AJT_COMPUN
If AJT->( DbSeek( xFilial( "AJT" ) + cProjet + cRevisa + cCompun ) )
	Do While !AJT->( Eof() ) .And. AJT->( AJT_FILIAL+AJT_PROJET+AJT_REVISA+AJT_COMPUN == xFilial( "AJT" ) + cProjet + cRevisa + cCompun )
		// Verifica os insumos e recursos da composicao aux
		DbSelecTArea( "AJU" )
		AJU->( DbSetOrder( 3 ) ) //AJU_FILIAL+AJU_PROJET+AJU_REVISA+AJU_COMPUN+AJU_INSUMO
		AJU->( DbSeek( xFilial( "AJU" ) + cProjet + cRevisa + cCompun + cInsumo ) )
		Do While !AJU->( Eof() ) .And. AJU->( AJU_FILIAL + AJU_PROJET + AJU_REVISA + AJU_COMPUN + AJU_INSUMO ) == xFilial( "AJU" ) + cProjet + cRevisa + cCompun + cInsumo
			DbSelectArea( "AJY" )
			AJY->( DbSetOrder( 1 ) )
			If AJY->( DbSeek( xFilial( "AJY" ) + AJU->( AJU_PROJET + AJU_REVISA + AJU_INSUMO ) ) )

				nProduc := nProdEqp
				If AJT->AJT_TIPO<>'1' .And. AJU->AJU_GRORGA $ PmsTPPrdE( AJT->AJT_TPPRDE )
					nProduc := AJT->AJT_PRODUC / nProduc
				EndIf

				nQuantAFA	:= AJU->AJU_QUANT
				nQuantAFA	:= pmsTrunca( "2", nQuantAFA/nProduc, nDecCst )
				nQuantAFA	:= pmsTrunca( "2", nQuantAFA * nQtd, nDecCst )
				nQtdIns		:= nQuantAFA

				If cProjeto <> AF8->AF8_PROJET
					oProjeto2:Init()
					oProjeto2:PrintLine()
					cProjeto	:=	AF8->AF8_PROJET
	
					oGrupo:Init()
					oGrupo:PrintLine()
					cGrupo := AF9->AF9_GRPCOM
					nTotSecao := 0
					nTotProje := 0
					oTrfCU04:Init()
				Else				
					If cGrupo <> AF9->AF9_GRPCOM
						If oGrupo:lPrinting
							oTrfCU04:Finish()
							oGrupo:Finish()
						EndIf
						oReport:SkipLine()
						oGrupo:Init()
						oGrupo:PrintLine()
						cGrupo := AF9->AF9_GRPCOM
						nTotSecao := 0
						oTrfCU04:Init()
					Endif				
				Endif	
		
				oTrfCU04:Init()
				oTrfCU04:PrintLine()
				nTotSecao += nQuantAFA
				nTotProje += nQuantAFA
				nTotGeral += nQuantAFA

			EndIf

			AJU->( DbSkip() )

		EndDo

		// Verifica as subcomposicoes da composicao aux
		DbSelectArea( "AJX" )
		AJX->( DbSetOrder( 2 ) )
		AJX->( DbSeek( xFilial( "AJX" ) + cProjet + cRevisa + cCompun ) )
		Do While !AJX->( Eof() ) .And. cProjet + cRevisa + cCompun == AJX->( AJX_PROJET + AJX_REVISA + AJX_COMPUN )

			nProduc := nProdEqp
			If AJT->AJT_TIPO<>'1' .And. "E" $ PmsTPPrdE( AJT->AJT_TPPRDE )
				nProduc := nProduc / AJT->AJT_PRODUC
			EndIf
					
			ExplodCU4(cProjet, cRevisa, AJX->AJX_SUBCOM, nQtd * AJX->AJX_QUANT, nProduc, cInsumo, @oReport, @oProjeto2, @oGrupo, @oTrfCU04, @cProjeto, @cGrupo, @nTotSecao, @nTotProje, @nTotGeral)

			AJX->( DbSkip() )
		EndDo

		AJT->( DbSkip() )
		
	EndDo
	
EndIf

RestArea(aAreaAJT)
RestArea(aAreaAJX)
RestArea(aArea)

Return

/*/
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
��������������������������������������������������������������������������������Ŀ��
���Programa  �ExplodCU5� Autor � Marcelo Akama                 � Data �12/04/2010���
��������������������������������������������������������������������������������Ĵ��
���Descri��o �Explode a composicao auxiliar para impressao dos recursos          ���
���������������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
/*/

Static Function ExplodCU5(cProjet, cRevisa, cCompun, nQtd, nProdEqp, cInsumo, oReport, oRecurso5, oTarefa5, cRecurso, aDados, nTotSecao, nTotGeral)
Local aArea		:= GetArea()
Local aAreaAJT	:= AJT->(GetArea())
Local aAreaAJX	:= AJX->(GetArea())
Local nQuantAFA	:= 0
Local nProduc	:= 0
Local nDecCst	:= TamSX3("AF9_CUSTO")[2]
Local cTrunca   := "1"
Local nX

DbSelectArea( "AJT" )
AJT->( DbSetOrder( 2 ) ) //AJT_FILIAL+AJT_PROJET+AJT_REVISA+AJT_COMPUN
If AJT->( DbSeek( xFilial( "AJT" ) + cProjet + cRevisa + cCompun ) )
	Do While !AJT->( Eof() ) .And. AJT->( AJT_FILIAL+AJT_PROJET+AJT_REVISA+AJT_COMPUN == xFilial( "AJT" ) + cProjet + cRevisa + cCompun )
		// Verifica os insumos e recursos da composicao aux
		DbSelecTArea( "AJU" )
		AJU->( DbSetOrder( 3 ) ) //AJU_FILIAL+AJU_PROJET+AJU_REVISA+AJU_COMPUN+AJU_INSUMO
		AJU->( DbSeek( xFilial( "AJU" ) + cProjet + cRevisa + cCompun + cInsumo ) )
		Do While !AJU->( Eof() ) .And. AJU->( AJU_FILIAL + AJU_PROJET + AJU_REVISA + AJU_COMPUN + AJU_INSUMO ) == xFilial( "AJU" ) + cProjet + cRevisa + cCompun + cInsumo
			DbSelectArea( "AJY" )
			AJY->( DbSetOrder( 1 ) )
			If AJY->( DbSeek( xFilial( "AJY" ) + AJU->( AJU_PROJET + AJU_REVISA + AJU_INSUMO ) ) )

				If !Empty(AJY->AJY_PRODUT)
					SB1->(dbSetOrder(1))
					SB1->(MSSeek(xFilial()+AJY->AJY_PRODUT))
					// valida o filtro do usuario
					If !Empty(oRecurso:GetAdvplExp('SB1')) .And. !SB1->(&(oRecurso:GetAdvplExp('SB1')))
						AJU->( DbSkip() )
						Loop
					EndIf
				Endif

				nProduc := nProdEqp
				If AJT->AJT_TIPO<>'1' .And. AJU->AJU_GRORGA $ PmsTPPrdE( AJT->AJT_TPPRDE )
					nProduc := AJT->AJT_PRODUC / nProduc
				EndIf

				nQuantAFA	:= AJU->AJU_QUANT
				nQuantAFA	:= pmsTrunca( "2", nQuantAFA/nProduc, nDecCst )
				nQuantAFA	:= pmsTrunca( "2", nQuantAFA * nQtd, nDecCst )
				nQtdIns		:= nQuantAFA

				If cRecurso <> AE8->AE8_RECURS
					oReport:SkipLine()
					cRecurso := AE8->AE8_RECURS

					oRecurso5:Init()
					oRecurso5:PrintLine()
					oTarefa5:Init()
					nTotSecao := 0
				Endif

				aAdd(aDados,Array(18))
				nX := Len(aDados)
				aDados[nX,1] := AF8->AF8_PROJET
				aDados[nX,2] := AF8->AF8_DESCRI
				aDados[nX,3] := AF9->AF9_TAREFA
				aDados[nX,4] := AF9->AF9_DESCRI
				aDados[nX,5] := AF9->AF9_START
				aDados[nX,6] := AF9->AF9_HORAI
				aDados[nX,7] := AF9->AF9_FINISH
				aDados[nX,8] := AF9->AF9_HORAF
				aDados[nX,9] := GetAloc()
				aDados[nX,10] := nQuantAFA
				aDados[nX,11] := If(!Empty(AJY->AJY_PRODUT),SB1->B1_UM,"")
				aDados[nX,12] := AJY->AJY_CUSTD
				aDados[nX,13] := PmsTrunca(cTrunca,AJY->AJY_CUSTD*nQuantAFA,nDecCst,AF9->AF9_QUANT)
				aDados[nX,14] := AF9->(Recno())
				aDados[nX,15] := AF8->(Recno())
				aDados[nX,16] := AJY->(Recno())
				aDados[nX,17] := AED->(Recno())
				aDados[nX,18] := SB1->(Recno())

				nTotSecao += nQuantAFA
				nTotGeral += nQuantAFA

			EndIf

			AJU->( DbSkip() )

		EndDo

		// Verifica as subcomposicoes da composicao aux
		DbSelectArea( "AJX" )
		AJX->( DbSetOrder( 2 ) )
		AJX->( DbSeek( xFilial( "AJX" ) + cProjet + cRevisa + cCompun ) )
		Do While !AJX->( Eof() ) .And. cProjet + cRevisa + cCompun == AJX->( AJX_PROJET + AJX_REVISA + AJX_COMPUN )

			nProduc := nProdEqp
			If AJT->AJT_TIPO<>'1' .And. "E" $ PmsTPPrdE( AJT->AJT_TPPRDE )
				nProduc := nProduc / AJT->AJT_PRODUC
			EndIf
					
			ExplodCU5(cProjet, cRevisa, AJX->AJX_SUBCOM, nQtd * AJX->AJX_QUANT, nProduc, cInsumo, @oReport, @oRecurso5, @oTarefa5, @cRecurso, @aDados, @nTotSecao, @nTotGeral)

			AJX->( DbSkip() )
		EndDo

		AJT->( DbSkip() )
		
	EndDo
	
EndIf

RestArea(aAreaAJT)
RestArea(aAreaAJX)
RestArea(aArea)

Return

/*/
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
��������������������������������������������������������������������������������Ŀ��
���Programa  �ExplodCU6� Autor � Marcelo Akama                 � Data �12/04/2010���
��������������������������������������������������������������������������������Ĵ��
���Descri��o �Explode a composicao auxiliar para impressao dos recursos          ���
���������������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
/*/

Static Function ExplodCU6(cProjet, cRevisa, cTarefa, cCompun, nQtd, nProdEqp, cTrunca, cProjeto, Cabec1, Cabec2, nTotal, nTotalG)
Local aArea		:= GetArea()
Local aAreaAJT	:= AJT->(GetArea())
Local aAreaAJX	:= AJX->(GetArea())
Local nQuantAFA	:= 0
Local nProduc	:= 0
Local nDecCst	:= TamSX3("AF9_CUSTO")[2]
Local nCustD

DbSelectArea( "AJT" )
AJT->( DbSetOrder( 2 ) ) //AJT_FILIAL+AJT_PROJET+AJT_REVISA+AJT_COMPUN
If AJT->( DbSeek( xFilial( "AJT" ) + cProjet + cRevisa + cCompun ) )
	Do While !AJT->( Eof() ) .And. AJT->( AJT_FILIAL+AJT_PROJET+AJT_REVISA+AJT_COMPUN == xFilial( "AJT" ) + cProjet + cRevisa + cCompun )
		// Verifica os insumos e recursos da composicao aux
		DbSelecTArea( "AJU" ) //AJU_FILIAL+AJU_PROJET+AJU_REVISA+AJU_COMPUN+AJU_ITEM
		AJU->( DbSetOrder( 2 ) )
		AJU->( DbSeek( xFilial( "AJU" ) + cProjet + cRevisa + cCompun ) )
		Do While !AJU->( Eof() ) .And. AJU->( AJU_FILIAL + AJU_PROJET + AJU_REVISA + AJU_COMPUN ) == xFilial( "AJU" ) + cProjet + cRevisa + cCompun
			DbSelectArea( "AJY" )
			AJY->( DbSetOrder( 1 ) )
			If AJY->( DbSeek( xFilial( "AJY" ) + AJU->( AJU_PROJET + AJU_REVISA + AJU_INSUMO ) ) )

				AE8->( DbSetOrder( 1 ) )
				AE8->( DbSeek( xFilial( "AE8" ) + AJY->AJY_RECURS ) )
				If AJY->AJY_RECURS >= mv_par07 .AND. AJY->AJY_RECURS <= mv_par08

					If nLi > 58
						nLi := 0
						nLi := Cabec( Titulo,Cabec1,Cabec2,nomeprog,Tamanho,CHRCOMP )
						nLi++
					EndIf

					If cProjeto <> AF8->AF8_PROJET	
						nLi++
						@ nLi,000 PSAY STR0008 + AF8->AF8_PROJET + " - " + AF8->AF8_DESCRI //"Projeto    : "
						nLi++
						@ nLi,000 PSAY __PrtThinLine()
						nLi++
						cProjeto	:=	AF8->AF8_PROJET	
					EndIf

					DbSelectArea( "AED" )
					AED->( DbSetOrder( 1 ) )
					AED->( DbSeek( xFilial( "AED" ) + AE8->AE8_EQUIP ) )

					@ nLi,000 PSAY cTarefa PICTURE PesqPict("AF9","AF9_TAREFA")
					@ nLi,013 PSAY Substr(AJY->AJY_DESC, 1, 30) PICTURE PesqPict("AJY","AJY_DESC")
					@ nLi,044 PSAY AJY->AJY_RECURS PICTURE PesqPict( "AJY","AJY_RECURS" )	
					@ nLi,060 PSAY AE8->AE8_DESCRI PICTURE PesqPict("AE8","AE8_DESCRI")
					@ nLi,091 PSAY AE8->AE8_EQUIP  PICTURE PesqPict("AE8","AE8_EQUIP")
					@ nLi,102 PSAY AED->AED_DESCRI PICTURE PesqPict("AED","AED_DESCRI")

					nProduc := nProdEqp
					If AJT->AJT_TIPO<>'1' .And. AJU->AJU_GRORGA $ PmsTPPrdE( AJT->AJT_TPPRDE )
						nProduc := AJT->AJT_PRODUC / nProduc
					EndIf

					nCustD		:= AJY->AJY_CUSTD
					nQuantAFA	:= AJU->AJU_QUANT
					nQuantAFA	:= pmsTrunca( "2", nQuantAFA/nProduc, nDecCst )
					nQuantAFA	:= pmsTrunca( "2", nQuantAFA * nQtd, nDecCst )
					nTotal		+= nQuantAFA
					nTotalG		+= nQuantAFA

					@ nLi,172 PSAY nQuantAFA PICTURE "@E 999999999.99"
					@ nLi,185 PSAY AJY->AJY_UM PICTURE PesqPict( "AJY", "AJY_UM" )
					@ nLi,188 PSAY nCustD PICTURE "@E 999,999,999.99"
					@ nLi,203 PSAY PmsTrunca(cTrunca,nCustD*nQuantAFA,nDecCst) PICTURE "@E 999,999,999,999.99"
				EndIf
			EndIf

            nLi++

			AJU->( DbSkip() )

		EndDo

		// Verifica as subcomposicoes da composicao aux
		DbSelectArea( "AJX" )
		AJX->( DbSetOrder( 2 ) )
		AJX->( DbSeek( xFilial( "AJX" ) + cProjet + cRevisa + cCompun ) )
		Do While !AJX->( Eof() ) .And. cProjet + cRevisa + cCompun == AJX->( AJX_PROJET + AJX_REVISA + AJX_COMPUN )

			nProduc := nProdEqp
			If AJT->AJT_TIPO<>'1' .And. "E" $ PmsTPPrdE( AJT->AJT_TPPRDE )
				nProduc := nProduc / AJT->AJT_PRODUC
			EndIf
					
			ExplodCU6(cProjet, cRevisa, cTarefa, AJX->AJX_SUBCOM, AJX->AJX_QUANT * nQtd, nProduc, cTrunca, @cProjeto, @Cabec1, @Cabec2, @nTotal, @nTotalG)

			AJX->( DbSkip() )
		EndDo

		AJT->( DbSkip() )
		
	EndDo
	
EndIf

RestArea(aAreaAJT)
RestArea(aAreaAJX)
RestArea(aArea)

Return

/*/
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
��������������������������������������������������������������������������������Ŀ��
���Programa  �ExplodCU7� Autor � Marcelo Akama                 � Data �12/04/2010���
��������������������������������������������������������������������������������Ĵ��
���Descri��o �Explode a composicao auxiliar para impressao dos recursos          ���
���������������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
/*/

Static Function ExplodCU7(cProjet, cRevisa, cCompun, nQtd, nProdEqp, cInsumo, cTrunca, aRows)
Local aArea		:= GetArea()
Local aAreaAJT	:= AJT->(GetArea())
Local aAreaAJX	:= AJX->(GetArea())
Local nQuantAFA	:= 0
Local nProduc	:= 0
Local nDecCst	:= TamSX3("AF9_CUSTO")[2]
Local nCustD

DbSelectArea( "AJT" )
AJT->( DbSetOrder( 2 ) ) //AJT_FILIAL+AJT_PROJET+AJT_REVISA+AJT_COMPUN
If AJT->( DbSeek( xFilial( "AJT" ) + cProjet + cRevisa + cCompun ) )
	Do While !AJT->( Eof() ) .And. AJT->( AJT_FILIAL+AJT_PROJET+AJT_REVISA+AJT_COMPUN == xFilial( "AJT" ) + cProjet + cRevisa + cCompun )
		// Verifica os insumos e recursos da composicao aux
		DbSelecTArea( "AJU" )
		AJU->( DbSetOrder( 3 ) ) //AJU_FILIAL+AJU_PROJET+AJU_REVISA+AJU_COMPUN+AJU_INSUMO
		AJU->( DbSeek( xFilial( "AJU" ) + cProjet + cRevisa + cCompun + cInsumo ) )
		Do While !AJU->( Eof() ) .And. AJU->( AJU_FILIAL + AJU_PROJET + AJU_REVISA + AJU_COMPUN + AJU_INSUMO ) == xFilial( "AJU" ) + cProjet + cRevisa + cCompun + cInsumo
			DbSelectArea( "AJY" )
			AJY->( DbSetOrder( 1 ) )
			If AJY->( DbSeek( xFilial( "AJY" ) + AJU->( AJU_PROJET + AJU_REVISA + AJU_INSUMO ) ) )

				nProduc := nProdEqp
				If AJT->AJT_TIPO<>'1' .And. AJU->AJU_GRORGA $ PmsTPPrdE( AJT->AJT_TPPRDE )
					nProduc := AJT->AJT_PRODUC / nProduc
				EndIf

				nQuantAFA	:= AJU->AJU_QUANT
				nQuantAFA	:= pmsTrunca( "2", nQuantAFA/nProduc, nDecCst )
				nQuantAFA	:= pmsTrunca( "2", nQuantAFA * nQtd, nDecCst )
				nCustAFA	:= PmsTrunca(cTrunca,AJY->AJY_CUSTD*nQuantAFA,nDecCst)
				nQtdIns		:= nQuantAFA

				aAdd( aRows, {	AE8->AE8_RECURS,;
								AE8->AE8_DESCRI,;
								AE8->AE8_EQUIP,;
								AED->AED_DESCRI,;
								AF8->AF8_PROJET,;
								AF8->AF8_DESCRI,;
								AF9->AF9_TAREFA,;
								AF9->AF9_DESCRI,;
								AF9->AF9_START,;
								AF9->AF9_HORAI,;
								AF9->AF9_FINISH,;
								AF9->AF9_HORAF,;
								GetAloc(),;
								AF9->AF9_REVISA,;
								AF9->AF9_QUANT,;
								AEL->AEL_QUANT,;
				 				AF9->AF9_HDURAC,;
				 				AJY->AJY_CUSTD,;
								AJY->AJY_PRODUT,;
								nQuantAFA,;
								nCustAFA	 } )

			EndIf

            nLi++

			AJU->( DbSkip() )

		EndDo

		// Verifica as subcomposicoes da composicao aux
		DbSelectArea( "AJX" )
		AJX->( DbSetOrder( 2 ) )
		AJX->( DbSeek( xFilial( "AJX" ) + cProjet + cRevisa + cCompun ) )
		Do While !AJX->( Eof() ) .And. cProjet + cRevisa + cCompun == AJX->( AJX_PROJET + AJX_REVISA + AJX_COMPUN )

			nProduc := nProdEqp
			If AJT->AJT_TIPO<>'1' .And. "E" $ PmsTPPrdE( AJT->AJT_TPPRDE )
				nProduc := nProduc / AJT->AJT_PRODUC
			EndIf
					
			ExplodCU7(cProjet, cRevisa, AJX->AJX_SUBCOM, AJX->AJX_QUANT * nQtd, nProduc, cInsumo, cTrunca, @aRows)

			AJX->( DbSkip() )
		EndDo

		AJT->( DbSkip() )
		
	EndDo
	
EndIf

RestArea(aAreaAJT)
RestArea(aAreaAJX)
RestArea(aArea)

Return

/*/
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
��������������������������������������������������������������������������������Ŀ��
���Programa  �ExplodCU8� Autor � Marcelo Akama                 � Data �12/04/2010���
��������������������������������������������������������������������������������Ĵ��
���Descri��o �Explode a composicao auxiliar para impressao dos recursos          ���
���������������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
/*/

Static Function ExplodCU8(cProjet, cRevisa, cCompun, nQtd, nProdEqp, cInsumo, cTrunca, aRows)
Local aArea		:= GetArea()
Local aAreaAJT	:= AJT->(GetArea())
Local aAreaAJX	:= AJX->(GetArea())
Local nQuantAFA	:= 0
Local nProduc	:= 0
Local nDecCst	:= TamSX3("AF9_CUSTO")[2]
Local nCustD

DbSelectArea( "AJT" )
AJT->( DbSetOrder( 2 ) ) //AJT_FILIAL+AJT_PROJET+AJT_REVISA+AJT_COMPUN
If AJT->( DbSeek( xFilial( "AJT" ) + cProjet + cRevisa + cCompun ) )
	Do While !AJT->( Eof() ) .And. AJT->( AJT_FILIAL+AJT_PROJET+AJT_REVISA+AJT_COMPUN == xFilial( "AJT" ) + cProjet + cRevisa + cCompun )
		// Verifica os insumos e recursos da composicao aux
		DbSelecTArea( "AJU" )
		AJU->( DbSetOrder( 3 ) ) //AJU_FILIAL+AJU_PROJET+AJU_REVISA+AJU_COMPUN+AJU_INSUMO
		AJU->( DbSeek( xFilial( "AJU" ) + cProjet + cRevisa + cCompun + cInsumo ) )
		Do While !AJU->( Eof() ) .And. AJU->( AJU_FILIAL + AJU_PROJET + AJU_REVISA + AJU_COMPUN + AJU_INSUMO ) == xFilial( "AJU" ) + cProjet + cRevisa + cCompun + cInsumo
			DbSelectArea( "AJY" )
			AJY->( DbSetOrder( 1 ) )
			If AJY->( DbSeek( xFilial( "AJY" ) + AJU->( AJU_PROJET + AJU_REVISA + AJU_INSUMO ) ) )

				nProduc := nProdEqp
				If AJT->AJT_TIPO<>'1' .And. AJU->AJU_GRORGA $ PmsTPPrdE( AJT->AJT_TPPRDE )
					nProduc := AJT->AJT_PRODUC / nProduc
				EndIf

				nQuantAFA	:= AJU->AJU_QUANT
				nQuantAFA	:= pmsTrunca( "2", nQuantAFA/nProduc, nDecCst )
				nQuantAFA	:= pmsTrunca( "2", nQuantAFA * nQtd, nDecCst )
				nCustAFA	:= PmsTrunca(cTrunca,AJY->AJY_CUSTD*nQuantAFA,nDecCst)
				nQtdIns		:= nQuantAFA

				aAdd( aRows, {	AED->AED_EQUIP,;
								AED->AED_DESCRI,;
								AF9->AF9_PROJET,;
								AF9->AF9_REVISA,;
								AF9->AF9_TAREFA,;
								AJY->AJY_PRODUT,;
								AEL->AEL_QUANT,;
				 				GetAloc(),;
				 				AJY->AJY_CUSTD,;
								AF9->AF9_QUANT,;
								AF9->AF9_DESCRI,;
				 				AF9->AF9_HDURAC,;
				 				AF9->AF9_START,;
				 				AF9->AF9_HORAI,;
				 				AF9->AF9_FINISH,;
				 				AF9->AF9_HORAF,;
								AF9->AF9_QUANT,;
				 				AE8->AE8_RECURS,;
				 				AE8->AE8_DESCRI,;
								AF9->AF9_TAREFA,;
								nQuantAFA,;
								nCustAFA } )

			EndIf

            nLi++

			AJU->( DbSkip() )

		EndDo

		// Verifica as subcomposicoes da composicao aux
		DbSelectArea( "AJX" )
		AJX->( DbSetOrder( 2 ) )
		AJX->( DbSeek( xFilial( "AJX" ) + cProjet + cRevisa + cCompun ) )
		Do While !AJX->( Eof() ) .And. cProjet + cRevisa + cCompun == AJX->( AJX_PROJET + AJX_REVISA + AJX_COMPUN )

			nProduc := nProdEqp
			If AJT->AJT_TIPO<>'1' .And. "E" $ PmsTPPrdE( AJT->AJT_TPPRDE )
				nProduc := nProduc / AJT->AJT_PRODUC
			EndIf
					
			ExplodCU8(cProjet, cRevisa, AJX->AJX_SUBCOM, AJX->AJX_QUANT * nQtd, nProduc, cInsumo, cTrunca, @aRows)

			AJX->( DbSkip() )
		EndDo

		AJT->( DbSkip() )
		
	EndDo
	
EndIf

RestArea(aAreaAJT)
RestArea(aAreaAJX)
RestArea(aArea)

Return

/*/
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
��������������������������������������������������������������������������������Ŀ��
���Programa  �ExplodCU6� Autor � Marcelo Akama                 � Data �12/04/2010���
��������������������������������������������������������������������������������Ĵ��
���Descri��o �Explode a composicao auxiliar para impressao dos recursos          ���
���������������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
/*/

Static Function ExplodCU9(cProjet, cRevisa, cCompun, nQtd, nProdEqp, cTrunca, cProjeto, cGrupo, nTotal, nTotalGrp)
Local aArea		:= GetArea()
Local aAreaAJT	:= AJT->(GetArea())
Local aAreaAJX	:= AJX->(GetArea())
Local nQuantAFA	:= 0
Local nProduc	:= 0
Local nDecCst	:= TamSX3("AF9_CUSTO")[2]
Local nCustD

DbSelectArea( "AJT" )
AJT->( DbSetOrder( 2 ) ) //AJT_FILIAL+AJT_PROJET+AJT_REVISA+AJT_COMPUN
If AJT->( DbSeek( xFilial( "AJT" ) + cProjet + cRevisa + cCompun ) )
	Do While !AJT->( Eof() ) .And. AJT->( AJT_FILIAL+AJT_PROJET+AJT_REVISA+AJT_COMPUN == xFilial( "AJT" ) + cProjet + cRevisa + cCompun )
		// Verifica os insumos e recursos da composicao aux
		DbSelecTArea( "AJU" ) //AJU_FILIAL+AJU_PROJET+AJU_REVISA+AJU_COMPUN+AJU_ITEM
		AJU->( DbSetOrder( 2 ) )
		AJU->( DbSeek( xFilial( "AJU" ) + cProjet + cRevisa + cCompun ) )
		Do While !AJU->( Eof() ) .And. AJU->( AJU_FILIAL + AJU_PROJET + AJU_REVISA + AJU_COMPUN ) == xFilial( "AJU" ) + cProjet + cRevisa + cCompun
			DbSelectArea( "AJY" )
			AJY->( DbSetOrder( 1 ) )
			If AJY->( DbSeek( xFilial( "AJY" ) + AJU->( AJU_PROJET + AJU_REVISA + AJU_INSUMO ) ) )

				If cProjeto <> AF8->AF8_PROJET	
					nLi++
					@ nLi,000 PSAY STR0008 + AF8->AF8_PROJET + " - " + AF8->AF8_DESCRI //"Projeto    : "
					nLi++
					@ nLi,000 PSAY __PrtThinLine()
					nLi++
					cProjeto	:=	AF8->AF8_PROJET	
				Endif	

				If cGrupo <> AF9->AF9_GRPCOM 
					If nTotalGrp > 0
						nLi++
						@ nLi,000 PSAY "Total Grupo"
						@ nLi,012 PSAY Replicate(".", 158)
						@ nLi,172 PSAY nTotalGrp PICTURE "@E 999999999.99"

						nTotalGrp := 0
					EndIf

					nTotalGrp := 0

					dbSelectArea("AE5")
					dbSetOrder(1)
					If dbSeek(xFilial()+AF9->AF9_GRPCOM)
						nLI++								
						@ nLi,000 PSAY "Grupo :"
						@ nLi,011 PSAY AF9->AF9_GRPCOM +"-" +AE5->AE5_DESCRI PICTURE  PesqPict("AF9","AF9_GRPCOM")
					Else
						nLI++								
						@ nLi,000 PSAY "Grupo :"
						@ nLi,011 PSAY "Outros - Nao especificado" 
					EndIf
					nLi++
					cGrupo :=	AF9->AF9_GRPCOM
				Endif				

				dbSelectArea("AED")
				AED->(dbSetOrder(1))
				AED->(MSSeek(xFilial()+AE8->AE8_EQUIP))

				nProduc := nProdEqp
				If AJT->AJT_TIPO<>'1' .And. AJU->AJU_GRORGA $ PmsTPPrdE( AJT->AJT_TPPRDE )
					nProduc := AJT->AJT_PRODUC / nProduc
				EndIf

				nQuantAFA	:= AJU->AJU_QUANT
				nQuantAFA	:= pmsTrunca( "2", nQuantAFA/nProduc, nDecCst )
				nQuantAFA	:= pmsTrunca( "2", nQuantAFA * nQtd, nDecCst )
				nQtdIns		:= nQuantAFA

				@ nLi,000 PSAY Substr(AF9->AF9_TAREFA,1,20) PICTURE PesqPict("AF9","AF9_TAREFA")
				@ nLi,021 PSAY Substr(AF9->AF9_DESCRI, 1, 22) PICTURE PesqPict("AF9","AF9_DESCRI")
				@ nLi,044 PSAY AJY->AJY_RECURS PICTURE PesqPict("AJY","AJY_RECURS")
				@ nLi,060 PSAY AE8->AE8_DESCRI PICTURE PesqPict("AE8","AE8_DESCRI")
				@ nLi,091 PSAY AE8->AE8_EQUIP  PICTURE PesqPict("AE8","AE8_EQUIP")
				@ nLi,102 PSAY AED->AED_DESCRI PICTURE PesqPict("AED","AED_DESCRI")
				@ nLi,134 PSAY AF9->AF9_START  PICTURE PesqPict("AF9","AF9_START")
				@ nLi,143 PSAY AF9->AF9_HORAI  PICTURE PesqPict("AF9","AF9_HORAI")
				@ nLi,149 PSAY AF9->AF9_FINISH PICTURE PesqPict("AF9","AF9_FINISH")
				@ nLi,158 PSAY AF9->AF9_HORAF  PICTURE PesqPict("AF9","AF9_HORAF")
				@ nLi,164 PSAY GetAloc()		PICTURE "@E 999.99%" // % de Alocacao do Recurso

				nTotal		+= nQuantAFA
				nTotalGrp	+= nQuantAFA

				@ nLi,172 PSAY nQuantAFA PICTURE "@E 999999999.99"

				If !Empty(AJY->AJY_PRODUT)
					SB1->(dbSetOrder(1))
					SB1->(xFilial()+AJY->AJY_PRODUT)
					@ nLi,185 PSAY SB1->B1_UM PICTURE PesqPict("SB1","B1_UM")
				EndIf

				@ nLi,188 PSAY AJY->AJY_CUSTD PICTURE "@E 999,999,999.99"
				@ nLi,203 PSAY PmsTrunca(cTrunca,AJY->AJY_CUSTD*nQuantAFA,nDecCst) PICTURE "@E 999,999,999,999.99"
					
				nLi++

			EndIf

			AJU->( DbSkip() )

		EndDo

		// Verifica as subcomposicoes da composicao aux
		DbSelectArea( "AJX" )
		AJX->( DbSetOrder( 2 ) )
		AJX->( DbSeek( xFilial( "AJX" ) + cProjet + cRevisa + cCompun ) )
		Do While !AJX->( Eof() ) .And. cProjet + cRevisa + cCompun == AJX->( AJX_PROJET + AJX_REVISA + AJX_COMPUN )

			nProduc := nProdEqp
			If AJT->AJT_TIPO<>'1' .And. "E" $ PmsTPPrdE( AJT->AJT_TPPRDE )
				nProduc := nProduc / AJT->AJT_PRODUC
			EndIf
					
			ExplodCU9(cProjet, cRevisa, AJX->AJX_SUBCOM, AJX->AJX_QUANT * nQtd, nProduc, cTrunca, @cProjeto, @cGrupo, @nTotal, @nTotalGrp)

			AJX->( DbSkip() )
		EndDo

		AJT->( DbSkip() )
		
	EndDo
	
EndIf

RestArea(aAreaAJT)
RestArea(aAreaAJX)
RestArea(aArea)

Return

//-----------------------------------------------------------------------------------
/*/{Protheus.doc} FATPDIsObfuscate
    @description
    Verifica se um campo deve ser ofuscado, esta fun��o deve utilizada somente ap�s 
    a inicializa��o das variaveis atravez da fun��o FATPDLoad.
	Remover essa fun��o quando n�o houver releases menor que 12.1.27

    @type  Function
    @author Squad CRM & Faturamento
    @since  05/12/2019
    @version P12.1.27
    @param cField, Caractere, Campo que sera validado
    @param cSource, Caractere, Nome do recurso que buscar dados protegidos.
    @param lLoad, Logico, Efetua a carga automatica do campo informado
    @return lObfuscate, L�gico, Retorna se o campo ser� ofuscado.
    @example FATPDIsObfuscate("A1_CGC",Nil,.T.)
/*/
//-----------------------------------------------------------------------------------
Static Function FATPDIsObfuscate(cField, cSource, lLoad)
    
	Local lObfuscate := .F.

    If FATPDActive()
		lObfuscate := FTPDIsObfuscate(cField, cSource, lLoad)
    EndIf 

Return lObfuscate

//-----------------------------------------------------------------------------
/*/{Protheus.doc} FATPDObfuscate
    @description
    Realiza ofuscamento de uma variavel ou de um campo protegido.
	Remover essa fun��o quando n�o houver releases menor que 12.1.27

    @type  Function
    @sample FATPDObfuscate("999999999","U5_CEL")
    @author Squad CRM & Faturamento
    @since 04/12/2019
    @version P12
    @param xValue, (caracter,numerico,data), Valor que sera ofuscado.
    @param cField, caracter , Campo que sera verificado.
    @param cSource, Caractere, Nome do recurso que buscar dados protegidos.
    @param lLoad, Logico, Efetua a carga automatica do campo informado

    @return xValue, retorna o valor ofuscado.
/*/
//-----------------------------------------------------------------------------
Static Function FATPDObfuscate(xValue, cField, cSource, lLoad)
    
    If FATPDActive()
		xValue := FTPDObfuscate(xValue, cField, cSource, lLoad)
    EndIf

Return xValue   


//-----------------------------------------------------------------------------
/*/{Protheus.doc} FATPDActive
    @description
    Fun��o que verifica se a melhoria de Dados Protegidos existe.

    @type  Function
    @sample FATPDActive()
    @author Squad CRM & Faturamento
    @since 17/12/2019
    @version P12    
    @return lRet, Logico, Indica se o sistema trabalha com Dados Protegidos
/*/
//-----------------------------------------------------------------------------
Static Function FATPDActive()

    Static _lFTPDActive := Nil
  
    If _lFTPDActive == Nil
        _lFTPDActive := ( GetRpoRelease() >= "12.1.027" .Or. !Empty(GetApoInfo("FATCRMPD.PRW")) )  
    Endif

Return _lFTPDActive  

