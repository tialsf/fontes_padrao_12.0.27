#INCLUDE 'protheus.ch'
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TECA190G.ch"

#DEFINE DEF_TITULO_DO_CAMPO		01	//Titulo do campo
#DEFINE DEF_TOOLTIP_DO_CAMPO	02	//ToolTip do campo
#DEFINE DEF_IDENTIFICADOR		03	//identificador (ID) do Field
#DEFINE DEF_TIPO_DO_CAMPO		04	//Tipo do campo
#DEFINE DEF_TAMANHO_DO_CAMPO	05	//Tamanho do campo
#DEFINE DEF_DECIMAL_DO_CAMPO	06	//Decimal do campo
#DEFINE DEF_CODEBLOCK_VALID		07	//Code-block de valida��o do campo
#DEFINE DEF_CODEBLOCK_WHEN		08	//Code-block de valida��o When do campo
#DEFINE DEF_LISTA_VAL			09	//Lista de valores permitido do campo
#DEFINE DEF_OBRIGAT				10	//Indica se o campo tem preenchimento obrigat�rio
#DEFINE DEF_CODEBLOCK_INIT		11	//Code-block de inicializacao do campo
#DEFINE DEF_CAMPO_CHAVE			12	//Indica se trata de um campo chave
#DEFINE DEF_RECEBE_VAL			13	//Indica se o campo pode receber valor em uma opera��o de update.
#DEFINE DEF_VIRTUAL				14	//Indica se o campo � virtual
#DEFINE DEF_VALID_USER			15	//Valid do usuario

#DEFINE DEF_ORDEM				16	//Ordem do campo
#DEFINE DEF_HELP				17	//Array com o Help dos campos
#DEFINE DEF_PICTURE				18	//Picture do campo
#DEFINE DEF_PICT_VAR			19	//Bloco de picture Var
#DEFINE DEF_LOOKUP				20	//Chave para ser usado no LooKUp
#DEFINE DEF_CAN_CHANGE			21	//Logico dizendo se o campo pode ser alterado
#DEFINE DEF_ID_FOLDER			22	//Id da Folder onde o field esta
#DEFINE DEF_ID_GROUP			23	//Id do Group onde o field esta
#DEFINE DEF_COMBO_VAL			24	//Array com os Valores do combo
#DEFINE DEF_TAM_MAX_COMBO		25	//Tamanho maximo da maior op��o do combo
#DEFINE DEF_INIC_BROWSE			26	//Inicializador do Browse
#DEFINE DEF_PICTURE_VARIAVEL	27	//Picture variavel
#DEFINE DEF_INSERT_LINE			28	//Se verdadeiro, indica pulo de linha ap�s o campo
#DEFINE DEF_WIDTH				29	//Largura fixa da apresenta��o do campo
#DEFINE DEF_TIPO_CAMPO_VIEW		30	//Tipo do campo

#DEFINE QUANTIDADE_DEFS			30	//Quantidade de DEFs

#DEFINE LEGAGENDA	01
#DEFINE LEGSTATUS	02
#DEFINE GRUPO		03
#DEFINE DATREF		04
#DEFINE DATAAG		05
#DEFINE DIASEM		06
#DEFINE HORINI		07
#DEFINE HORFIM		08
#DEFINE CODTEC		09
#DEFINE NOMTEC		10
#DEFINE TIPO		11
#DEFINE ATENDIDA	12
#DEFINE CODABB		13
#DEFINE TURNO		14
#DEFINE SEQ			15
#DEFINE ITEM		16
#DEFINE KEYTGY		17
#DEFINE ITTGY		18
#DEFINE EXSABB		19
#DEFINE HORASTRAB   20
#DEFINE DALOFIM     21
#DEFINE ARRTDV      22
#DEFINE DESCCONF    23

#DEFINE TAMANHO		23

Static oMdl190D 	:= Nil
Static cMultFil		:= ""

//------------------------------------------------------------------------------
/*/{Protheus.doc} TECA190G - Mesa Operacional - Aloca��o Por Horas
 	ModelDef
 		Defini��o do modelo de Dados

@author	Augusto Albuquerque
@since	03/04/2020
/*/
//------------------------------------------------------------------------------
Static Function ModelDef()
Local oModel	
Local oStrAA1	:= FWFormModelStruct():New()
Local oStrALC	:= FWFormModelStruct():New()
Local oStrTOT	:= FWFormModelStruct():New()
Local aFields	:= {}
Local nX		:= 0
Local nY		:= 0
Local aTables 	:= {}
Local xAux
Local bCommit	:= { || AT190GCmt() }
Local bValid := { || AT190GVld() }

oStrAA1:AddTable("   ",{}, STR0001) //"Aloca��o Por Hora"
oStrALC:AddTable("   ",{}, "   ")
oStrTOT:AddTable("   ",{}, "   ")

AADD(aTables, {oStrAA1, "AA1"})
AADD(aTables, {oStrALC, "ALC"})
AADD(aTables, {oStrTOT, "TOT"})

For nY := 1 To LEN(aTables)
	aFields := AT190GDef(aTables[nY][2], .T.)

	For nX := 1 TO LEN(aFields)
		aTables[nY][1]:AddField(aFields[nX][DEF_TITULO_DO_CAMPO],;
						aFields[nX][DEF_TOOLTIP_DO_CAMPO],;
						aFields[nX][DEF_IDENTIFICADOR	],;
						aFields[nX][DEF_TIPO_DO_CAMPO	],;
						aFields[nX][DEF_TAMANHO_DO_CAMPO],;
						aFields[nX][DEF_DECIMAL_DO_CAMPO],;
						aFields[nX][DEF_CODEBLOCK_VALID	],;
						aFields[nX][DEF_CODEBLOCK_WHEN	],;
						aFields[nX][DEF_LISTA_VAL		],;
						aFields[nX][DEF_OBRIGAT			],;
						aFields[nX][DEF_CODEBLOCK_INIT	],;
						aFields[nX][DEF_CAMPO_CHAVE		],;
						aFields[nX][DEF_RECEBE_VAL		],;
						aFields[nX][DEF_VIRTUAL			],;
						aFields[nX][DEF_VALID_USER		])
	Next nX
Next nY

xAux := FwStruTrigger( 'AA1_CODTEC', 'AA1_NOMTEC',;
	'Posicione("AA1",1,xFilial("AA1") + FwFldGet("AA1_CODTEC"),"AA1_NOMTEC")', .F. )
	oStrAA1:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])

xAux := FwStruTrigger( 'ALC_DATREF', 'ALC_SEMANA',;
	'TECCdow(Dow(FwFldGet("ALC_DATREF")))', .F. )
	oStrALC:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])

xAux := FwStruTrigger( 'ALC_SAIDA', 'ALC_TOTHRS',;
	'AT190GHrsT("ALC_ENTRADA")', .F. )
	oStrALC:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])

xAux := FwStruTrigger( 'ALC_ENTRADA', 'ALC_TOTHRS',;
	'AT190GHrsT("ALC_SAIDA")', .F. )
	oStrALC:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])
	
oModel := MPFormModel():New('TECA190G',/*bPreValidacao*/, bValid, bCommit,/*bCancel*/)
oModel:SetDescription( STR0001 ) //"Aloca��o Por Hora" 

oModel:addFields('AA1MASTER',,oStrAA1)
oModel:SetPrimaryKey({"AA1_FILIAL","AA1_CODTEC"})

oModel:addFields('TOTDETAIL','AA1MASTER',oStrTOT)

oModel:addGrid('ALCDETAIL','AA1MASTER', oStrALC,{|oMdlG,nLine,cAcao,cCampo, xValue, xOldValue| AT190GPrAL(oMdlG, nLine, cAcao, cCampo, xValue, xOldValue)},;
{|oMdlG,nLine,cAcao,cCampo| AT190GPoAL(oMdlG, nLine, cAcao, cCampo)})

oModel:GetModel('ALCDETAIL'):SetOnlyQuery(.T.)
oModel:GetModel('TOTDETAIL'):SetOnlyQuery(.T.)

oModel:GetModel('ALCDETAIL'):SetOptional(.T.)
oModel:GetModel('TOTDETAIL'):SetOptional(.T.)

oModel:GetModel('AA1MASTER'):SetDescription(STR0001)	//"Atendente"
oModel:GetModel('ALCDETAIL'):SetDescription(STR0002)	//"Proje��o de Aloca��o"
oModel:GetModel('TOTDETAIL'):SetDescription("Total de Horas")

oModel:SetActivate( {|oModel| InitDados( oModel ) } )

Return oModel
//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
	Defini��o da interface

@author	Augusto Albuquerque
@since 03/04/2020
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
Local oModel := ModelDef()
Local oView
Local aTables 	:= {}
Local oStrAA1	:= FWFormViewStruct():New()
Local oStrALC	:= FWFormViewStruct():New()
Local oStrTOT	:= FWFormViewStruct():New()
Local nX
Local nY

AADD(aTables, {oStrAA1, "AA1"})
AADD(aTables, {oStrALC, "ALC"})
AADD(aTables, {oStrTOT, "TOT"})

For nY := 1 to LEN(aTables)
	
	aFields := AT190GDef(aTables[nY][2])

	For nX := 1 to LEN(aFields)
		aTables[nY][1]:AddField(aFields[nX][DEF_IDENTIFICADOR],;
						aFields[nX][DEF_ORDEM],;
						aFields[nX][DEF_TITULO_DO_CAMPO],;
						aFields[nX][DEF_TOOLTIP_DO_CAMPO],;
						aFields[nX][DEF_HELP],;
						aFields[nX][DEF_TIPO_CAMPO_VIEW],;
						aFields[nX][DEF_PICTURE],;
						aFields[nX][DEF_PICT_VAR],;
						aFields[nX][DEF_LOOKUP],;
						aFields[nX][DEF_CAN_CHANGE],;
						aFields[nX][DEF_ID_FOLDER],;
						aFields[nX][DEF_ID_GROUP],;
						aFields[nX][DEF_COMBO_VAL],;
						aFields[nX][DEF_TAM_MAX_COMBO],;
						aFields[nX][DEF_INIC_BROWSE],;
						aFields[nX][DEF_VIRTUAL],;
						aFields[nX][DEF_PICTURE_VARIAVEL],;
						aFields[nX][DEF_INSERT_LINE],;
						aFields[nX][DEF_WIDTH])
		If aTables[nY][2] == "AA1" .AND. !(aFields[nX][DEF_IDENTIFICADOR] $ "AA1_CODTEC|AA1_NOMTEC")
			aTables[nY][1]:RemoveField(aFields[nX][DEF_IDENTIFICADOR])
		EndIf
	Next nX
Next nY


oView := FWFormView():New()
oView:SetModel(oModel)

oView:AddField('VIEW_MASTER', oStrAA1, 'AA1MASTER')
oView:AddGrid('DETAIL_ALC', oStrALC, 'ALCDETAIL')
oView:AddField('DETAIL_TOT', oStrTOT, 'TOTDETAIL')

oView:CreateHorizontalBox( 'REALOC_AA1' , 35 )
oView:CreateHorizontalBox( 'REALOC_ALOC', 60 )
oView:CreateHorizontalBox( 'TOTAL_HRS', 05 )

oView:SetOwnerView('VIEW_MASTER','REALOC_AA1')
oView:SetOwnerView('DETAIL_ALC','REALOC_ALOC')
oView:SetOwnerView('DETAIL_TOT','TOTAL_HRS')

oView:SetContinuousForm()

oView:EnableTitleView('VIEW_MASTER', 	STR0001) 		//"Aloca��o Por Hora"

oView:SetDescription(STR0001) //"Aloca��o Por Hora"

Return oView

//------------------------------------------------------------------------------
/*/{Protheus.doc} InitDados

@description Bloco de c�digo executado no activate
@param oModel, obj, modelo em ativa��o

@author	Augusto Albuquerque
@since	03/04/2020
/*/
//------------------------------------------------------------------------------
Static Function InitDados(oModel)
Local oMdlAA1 := oModel:GetModel("AA1MASTER")
Local oStrAA1 := oMdlAA1:GetStruct() 

oMdlAA1:SetValue("AA1_CODTEC", oMdl190d:GetValue("AA1MASTER","AA1_CODTEC"))
oStrAA1:SetProperty("AA1_CODTEC", MODEL_FIELD_WHEN, {|| .F.})
oStrAA1:SetProperty("AA1_NOMTEC", MODEL_FIELD_WHEN, {|| .F.})

If oMdl190d:GetId() == 'TECA190D' .AND. oModel:GetId() == 'TECA190F' 
	oMdlAA1:SetValue("AA1_CODTEC", oMdl190d:GetValue("AA1MASTER","AA1_CODTEC"))
	oStrAA1:SetProperty("AA1_CODTEC", MODEL_FIELD_WHEN, {|| .F.})
	oStrAA1:SetProperty("AA1_NOMTEC", MODEL_FIELD_WHEN, {|| .F.})
	oMdlDTA:SetValue("DTA_DTINI",dDataBase)
	oMdlDTA:SetValue("DTA_DTFIM",dDataBase)
EndIf

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AT190GDef
@description Cria��o dos campos
@return aRet
@author Augusto Albuquerque
@since  03/04/2020
/*/
//--------------------------------------------------------------------------------------------------------------------
Function AT190GDef(cTable, lAtualiza)
Local aRet		:= {}
Local nAux 		:= 0
Local cDescri	:= "X3_DESCRIC"

Default lAtualiza := .F.
#IFDEF SPANISH
	cDescri	:= "X3_DESCSPA"
#ELSE
	#IFDEF ENGLISH
		cDescri	:= "X3_DESCENG"
	#ENDIF
#ENDIF

If lAtualiza
	oMdl190d := Nil
	oMdl190d := FwModelActive()
EndIf

If cTable == "AA1"
	aRet := AT190DDef(cTable) 
ElseIf cTable == "ALC"

    AADD(aRet, ARRAY(QUANTIDADE_DEFS))
	nAux := LEN(aRet)
	aRet[nAux][DEF_TITULO_DO_CAMPO] := STR0003		//"Agenda"
	aRet[nAux][DEF_TOOLTIP_DO_CAMPO] := STR0003		//"Agenda"
	aRet[nAux][DEF_IDENTIFICADOR] := "ALC_SITABB"
	aRet[nAux][DEF_TIPO_DO_CAMPO] := "BT"
	aRet[nAux][DEF_TIPO_CAMPO_VIEW] := "BT"
	aRet[nAux][DEF_TAMANHO_DO_CAMPO] := 1
	aRet[nAux][DEF_DECIMAL_DO_CAMPO] := 0
	aRet[nAux][DEF_CODEBLOCK_VALID] := {||At330AGtLA()}
	aRet[nAux][DEF_CODEBLOCK_INIT] := {|| "BR_VERDE"}
	aRet[nAux][DEF_OBRIGAT] := .F.
	aRet[nAux][DEF_VIRTUAL] := .T.
	aRet[nAux][DEF_ORDEM] := "01"
	aRet[nAux][DEF_PICTURE] := ""
	aRet[nAux][DEF_CAN_CHANGE] := .T.

	AADD(aRet, ARRAY(QUANTIDADE_DEFS))
	nAux := LEN(aRet)
	aRet[nAux][DEF_TITULO_DO_CAMPO] := STR0005		//"Data da Aloca��o"
	aRet[nAux][DEF_TOOLTIP_DO_CAMPO] := STR0005		//"Data da Aloca��o"
	aRet[nAux][DEF_IDENTIFICADOR] := "ALC_DATREF"
	aRet[nAux][DEF_TIPO_DO_CAMPO] := "D"
	aRet[nAux][DEF_TIPO_CAMPO_VIEW] := "D"
	aRet[nAux][DEF_TAMANHO_DO_CAMPO] := 8
	aRet[nAux][DEF_DECIMAL_DO_CAMPO] := 0
	aRet[nAux][DEF_CODEBLOCK_INIT] := {|| dDataBase}
	aRet[nAux][DEF_OBRIGAT] := .F.
	aRet[nAux][DEF_VIRTUAL] := .T.
	aRet[nAux][DEF_ORDEM] := "02"
	aRet[nAux][DEF_PICTURE] := ""
	aRet[nAux][DEF_CAN_CHANGE] := .T.
	aRet[nAux][DEF_HELP] := {STR0006}	//"Dia que o atendente estar� alocado."

	AADD(aRet, ARRAY(QUANTIDADE_DEFS))
	nAux := LEN(aRet)
	aRet[nAux][DEF_TITULO_DO_CAMPO] := STR0007	//"Dia da Semana"
	aRet[nAux][DEF_TOOLTIP_DO_CAMPO] := STR0007	//"Dia da Semana"
	aRet[nAux][DEF_IDENTIFICADOR] := "ALC_SEMANA"
	aRet[nAux][DEF_TIPO_DO_CAMPO] := "C"
	aRet[nAux][DEF_TIPO_CAMPO_VIEW] := "C"
	aRet[nAux][DEF_TAMANHO_DO_CAMPO] := 15
	aRet[nAux][DEF_DECIMAL_DO_CAMPO] := 0
	aRet[nAux][DEF_RECEBE_VAL] := .F.
	aRet[nAux][DEF_VIRTUAL] := .T.
	aRet[nAux][DEF_ORDEM] := "03"
	aRet[nAux][DEF_PICTURE] := "@!"
	aRet[nAux][DEF_CODEBLOCK_INIT] := {|| TECCdow(Dow(dDataBase))}
	aRet[nAux][DEF_CAN_CHANGE] := .F.

    AADD(aRet, ARRAY(QUANTIDADE_DEFS))
	nAux := LEN(aRet)
	aRet[nAux][DEF_TITULO_DO_CAMPO] := STR0008	//"Hora de Entrada"
	aRet[nAux][DEF_TOOLTIP_DO_CAMPO] := STR0008	//"Hora de Entrada"
	aRet[nAux][DEF_IDENTIFICADOR] := "ALC_ENTRADA"
	aRet[nAux][DEF_TIPO_DO_CAMPO] := "C"
	aRet[nAux][DEF_TIPO_CAMPO_VIEW] := "C"
	aRet[nAux][DEF_TAMANHO_DO_CAMPO] := 5
	aRet[nAux][DEF_DECIMAL_DO_CAMPO] := 0
	aRet[nAux][DEF_CODEBLOCK_WHEN] := {|| !Empty( At190dGVal("ALCDETAIL", "ALC_DATREF"))}
	aRet[nAux][DEF_CODEBLOCK_VALID] := {|oMdl,cField,xNewValue| At190dHora(oMdl,cField,xNewValue)}
	aRet[nAux][DEF_OBRIGAT] := .T.	
	aRet[nAux][DEF_RECEBE_VAL] := .T.
	aRet[nAux][DEF_VIRTUAL] := .T.
	aRet[nAux][DEF_ORDEM] := "04"
	aRet[nAux][DEF_PICTURE] := "99:99"
	aRet[nAux][DEF_CAN_CHANGE] := .T.
	aRet[nAux][DEF_HELP] := {STR0009}	//"Horario inicial da agenda"

	AADD(aRet, ARRAY(QUANTIDADE_DEFS))
	nAux := LEN(aRet)
	aRet[nAux][DEF_TITULO_DO_CAMPO] := STR0010	//"Hora de Sa�da"
	aRet[nAux][DEF_TOOLTIP_DO_CAMPO] := STR0010	//"Hora de Sa�da"
	aRet[nAux][DEF_IDENTIFICADOR] := "ALC_SAIDA"
	aRet[nAux][DEF_TIPO_DO_CAMPO] := "C"
	aRet[nAux][DEF_TIPO_CAMPO_VIEW] := "C"
	aRet[nAux][DEF_TAMANHO_DO_CAMPO] := 5
	aRet[nAux][DEF_DECIMAL_DO_CAMPO] := 0
	aRet[nAux][DEF_CODEBLOCK_WHEN] := {|| !Empty( At190dGVal("ALCDETAIL", "ALC_DATREF"))}
	aRet[nAux][DEF_CODEBLOCK_VALID] := {|oMdl,cField,xNewValue| At190dHora(oMdl,cField,xNewValue)}
	aRet[nAux][DEF_RECEBE_VAL] := .T.
	aRet[nAux][DEF_OBRIGAT] := .T.
	aRet[nAux][DEF_VIRTUAL] := .T.
	aRet[nAux][DEF_ORDEM] := "05"
	aRet[nAux][DEF_PICTURE] := "99:99"
	aRet[nAux][DEF_CAN_CHANGE] := .T.
	aRet[nAux][DEF_HELP] := {STR0011}	//"Horario final da agenda"

	AADD(aRet, ARRAY(QUANTIDADE_DEFS))
	nAux := LEN(aRet)
	aRet[nAux][DEF_TITULO_DO_CAMPO] := STR0012 //"Turno" 
	aRet[nAux][DEF_TOOLTIP_DO_CAMPO] := STR0012//"Turno" 
	aRet[nAux][DEF_IDENTIFICADOR] := "ALC_TURNO"
	aRet[nAux][DEF_TIPO_DO_CAMPO] := "C"
	aRet[nAux][DEF_TIPO_CAMPO_VIEW] := "C"
	aRet[nAux][DEF_TAMANHO_DO_CAMPO] := 3
	aRet[nAux][DEF_DECIMAL_DO_CAMPO] := 0
	aRet[nAux][DEF_OBRIGAT] := .F.
	aRet[nAux][DEF_RECEBE_VAL] := .F.
	aRet[nAux][DEF_OBRIGAT] := .T.
	aRet[nAux][DEF_VIRTUAL] := .T.
	aRet[nAux][DEF_ORDEM] := "06"
	aRet[nAux][DEF_PICTURE] := "@!"
	aRet[nAux][DEF_CODEBLOCK_INIT] := {|| GetTurnTGY( "TURNO" ) }
	aRet[nAux][DEF_CAN_CHANGE] := .T.
    aRet[nAux][DEF_LOOKUP] := "SR6   "
	aRet[nAux][DEF_HELP] := {STR0013}	//"Selecione o turno do atendente."

    AADD(aRet, ARRAY(QUANTIDADE_DEFS))
	nAux := LEN(aRet)
	aRet[nAux][DEF_TITULO_DO_CAMPO] := STR0014  //"Sequencia"
	aRet[nAux][DEF_TOOLTIP_DO_CAMPO] := STR0014 //"Sequencia"
	aRet[nAux][DEF_IDENTIFICADOR] := "ALC_SEQ"
	aRet[nAux][DEF_TIPO_DO_CAMPO] := "C"
	aRet[nAux][DEF_TIPO_CAMPO_VIEW] := "C"
	aRet[nAux][DEF_TAMANHO_DO_CAMPO] := 2
	aRet[nAux][DEF_DECIMAL_DO_CAMPO] := 0
	aRet[nAux][DEF_RECEBE_VAL] := .F.
	aRet[nAux][DEF_OBRIGAT] := .T.
	aRet[nAux][DEF_VIRTUAL] := .T.
	aRet[nAux][DEF_CODEBLOCK_INIT] := {|| GetTurnTGY( "SEQ" ) }
	aRet[nAux][DEF_CODEBLOCK_WHEN] := {|| WhensALC("ALC_TURNO")}
	aRet[nAux][DEF_ORDEM] := "07"
	aRet[nAux][DEF_PICTURE] := "@!"
	aRet[nAux][DEF_LOOKUP] := "T19SEQ"
	aRet[nAux][DEF_CAN_CHANGE] := .T.
	aRet[nAux][DEF_HELP] := {STR0015}	//"Selecione a Sequencia do Turno"

    AADD(aRet, ARRAY(QUANTIDADE_DEFS))
	nAux := LEN(aRet)
	aRet[nAux][DEF_TITULO_DO_CAMPO] := STR0016	//"Tipo"
	aRet[nAux][DEF_TOOLTIP_DO_CAMPO] := STR0016	//"Tipo"
	aRet[nAux][DEF_IDENTIFICADOR] := "ALC_TIPO"
	aRet[nAux][DEF_TIPO_DO_CAMPO] := "C"
	aRet[nAux][DEF_TIPO_CAMPO_VIEW] := "C"
	aRet[nAux][DEF_TAMANHO_DO_CAMPO] := 1
	aRet[nAux][DEF_DECIMAL_DO_CAMPO] := 0
	aRet[nAux][DEF_LISTA_VAL] := {"S="+STR0017,"N="+STR0044} //"Trabalhado"#"N�o Trabalhado"
	aRet[nAux][DEF_COMBO_VAL] := {"S="+STR0017,"N="+STR0044} //"Trabalhado"#"N�o Trabalhado"
	aRet[nAux][DEF_CODEBLOCK_INIT] := {|| "S" }
	aRet[nAux][DEF_RECEBE_VAL] := .T.
	aRet[nAux][DEF_VIRTUAL] := .T.
	aRet[nAux][DEF_OBRIGAT] := .F.
	aRet[nAux][DEF_ORDEM] := "08"
	aRet[nAux][DEF_PICTURE] := "@!"
	aRet[nAux][DEF_CAN_CHANGE] := .T.
	aRet[nAux][DEF_CODEBLOCK_WHEN] := {|| .T.}
	aRet[nAux][DEF_HELP] := {STR0023}	//"Tipo de dia: Trabalhado, n�o trabalhado, folga ou DSR."

    AADD(aRet, ARRAY(QUANTIDADE_DEFS))
	nAux := LEN(aRet)
	aRet[nAux][DEF_TITULO_DO_CAMPO] := STR0024  //"Total de Horas"
	aRet[nAux][DEF_TOOLTIP_DO_CAMPO] := STR0024 //"Total de Horas"
	aRet[nAux][DEF_IDENTIFICADOR] := "ALC_TOTHRS"
	aRet[nAux][DEF_TIPO_DO_CAMPO] := "C"
	aRet[nAux][DEF_TIPO_CAMPO_VIEW] := "C"
	aRet[nAux][DEF_TAMANHO_DO_CAMPO] := TamSX3("TFF_QTDHRS")[1]
	aRet[nAux][DEF_DECIMAL_DO_CAMPO] := 0
	aRet[nAux][DEF_OBRIGAT] := .F.
	aRet[nAux][DEF_RECEBE_VAL] := .F.
	aRet[nAux][DEF_VIRTUAL] := .T.
	aRet[nAux][DEF_ORDEM] := "09"
	aRet[nAux][DEF_PICTURE] := "99:99"
	aRet[nAux][DEF_CAN_CHANGE] := .F.
    aRet[nAux][DEF_CODEBLOCK_WHEN] := {|| .F.}

ElseIf cTable == 'TOT'
	AADD(aRet, ARRAY(QUANTIDADE_DEFS))
	nAux := LEN(aRet)
	aRet[nAux][DEF_TITULO_DO_CAMPO] := STR0025  //"Saldo de Horas"
	aRet[nAux][DEF_TOOLTIP_DO_CAMPO] := STR0025 //"Saldo de Horas"
	aRet[nAux][DEF_IDENTIFICADOR] := "TOT_SALHRS"
	aRet[nAux][DEF_TIPO_DO_CAMPO] := "C"
	aRet[nAux][DEF_TIPO_CAMPO_VIEW] := "C"
	aRet[nAux][DEF_TAMANHO_DO_CAMPO] := TamSX3("TFF_QTDHRS")[1]
	aRet[nAux][DEF_DECIMAL_DO_CAMPO] := 0
	aRet[nAux][DEF_OBRIGAT] := .F.
	aRet[nAux][DEF_RECEBE_VAL] := .T.
	aRet[nAux][DEF_VIRTUAL] := .T.
	aRet[nAux][DEF_ORDEM] := "01"
	aRet[nAux][DEF_PICTURE] := "99:99"
	aRet[nAux][DEF_CAN_CHANGE] := .F.
	aRet[nAux][DEF_CODEBLOCK_INIT] := {|| "00:00" }
    aRet[nAux][DEF_CODEBLOCK_WHEN] := {|| .F.}

	AADD(aRet, ARRAY(QUANTIDADE_DEFS))
	nAux := LEN(aRet)
	aRet[nAux][DEF_TITULO_DO_CAMPO] := STR0035 //"Horas do Contrato" 
	aRet[nAux][DEF_TOOLTIP_DO_CAMPO] := STR0035 //"Horas do Contrato" 
	aRet[nAux][DEF_IDENTIFICADOR] := "TOT_CTRSAL"
	aRet[nAux][DEF_TIPO_DO_CAMPO] := "C"
	aRet[nAux][DEF_TIPO_CAMPO_VIEW] := "C"
	aRet[nAux][DEF_TAMANHO_DO_CAMPO] := TamSX3("TFF_QTDHRS")[1]
	aRet[nAux][DEF_DECIMAL_DO_CAMPO] := 0
	aRet[nAux][DEF_OBRIGAT] := .F.
	aRet[nAux][DEF_RECEBE_VAL] := .T.
	aRet[nAux][DEF_VIRTUAL] := .T.
	aRet[nAux][DEF_ORDEM] := "02"
	aRet[nAux][DEF_PICTURE] := "99:99"
	aRet[nAux][DEF_CODEBLOCK_INIT] := {|| POSICIONE("TFF",1,oMdl190d:GetValue("TGYMASTER", "TGY_FILIAL")+oMdl190d:GetValue("TGYMASTER", "TGY_TFFCOD"),"TFF_HRSSAL") }
	aRet[nAux][DEF_CAN_CHANGE] := .F.
    aRet[nAux][DEF_CODEBLOCK_WHEN] := {|| .F.}

	AADD(aRet, ARRAY(QUANTIDADE_DEFS))
	nAux := LEN(aRet)
	aRet[nAux][DEF_TITULO_DO_CAMPO] := STR0034  //"Horas Restantes" 
	aRet[nAux][DEF_TOOLTIP_DO_CAMPO] := STR0034 //"Horas Restantes" 
	aRet[nAux][DEF_IDENTIFICADOR] := "TOT_RESTSAL"
	aRet[nAux][DEF_TIPO_DO_CAMPO] := "C"
	aRet[nAux][DEF_TIPO_CAMPO_VIEW] := "C"
	aRet[nAux][DEF_TAMANHO_DO_CAMPO] := TamSX3("TFF_QTDHRS")[1]
	aRet[nAux][DEF_DECIMAL_DO_CAMPO] := 0
	aRet[nAux][DEF_OBRIGAT] := .F.
	aRet[nAux][DEF_RECEBE_VAL] := .T.
	aRet[nAux][DEF_VIRTUAL] := .T.
	aRet[nAux][DEF_ORDEM] := "03"
	aRet[nAux][DEF_PICTURE] := "99:99"
	aRet[nAux][DEF_CODEBLOCK_INIT] := {|| "00:00" }
	aRet[nAux][DEF_CAN_CHANGE] := .F.
    aRet[nAux][DEF_CODEBLOCK_WHEN] := {|| .F.}
EndIf

Return (aRet)

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AT190GCmt
@description Commit do modelo
@return lRet
@author Augusto Albuquerque
@since  03/04/2020
/*/
//--------------------------------------------------------------------------------------------------------------------
Function AT190GCmt()
Local lRet      := .T.
Local nLinha    := 1
Local oModel    := FwModelActive()
Local oMdlAlc   := oModel:GetModel("ALCDETAIL")
Local oMdlAlc19 := oMdl190D:GetModel("ALCDETAIL")
Local nX

oMdlAlc19:SetNoInsertLine(.F.)
oMdlAlc19:SetNoDeleteLine(.F.)  

oMdlAlc19:ClearData()
oMdlAlc19:InitLine()

For nX := 1 To oMdlAlc:Length()
    oMdlAlc:GoLine(nX)
    
    If !oMdlAlc:IsDeleted()

        If !oMdlAlc19:IsEmpty()
            nLinha := oMdlAlc19:AddLine()
        EndIf

        oMdlAlc19:GoLine(nLinha)

        oMdlAlc19:LoadValue("ALC_GRUPO", 	1)
        oMdlAlc19:LoadValue("ALC_DATREF", oMdlAlc:GetValue("ALC_DATREF"))
        oMdlAlc19:LoadValue("ALC_DATA", 	oMdlAlc:GetValue("ALC_DATREF"))
        oMdlAlc19:LoadValue("ALC_SEMANA", TECCdow(Dow(oMdlAlc:GetValue("ALC_DATREF"))))
        oMdlAlc19:LoadValue("ALC_ENTRADA", oMdlAlc:GetValue("ALC_ENTRADA"))
        oMdlAlc19:LoadValue("ALC_SAIDA", 	oMdlAlc:GetValue("ALC_SAIDA"))
        oMdlAlc19:LoadValue("ALC_TIPO",	oMdlAlc:GetValue("ALC_TIPO"))
        oMdlAlc19:LoadValue("ALC_SEQ",	oMdlAlc:GetValue("ALC_SEQ"))
        oMdlAlc19:LoadValue("ALC_TURNO",	oMdlAlc:GetValue("ALC_TURNO"))
    EndIf
Next nX

lRet := At190dExec("GravaAloc2( .F., , .T., @xPar )", @oMdl190D)

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AT190GHrsT
@description Fun��o de Calculo para o total de horas do periodo
@return nTot	
@author Augusto Albuquerque
@since  03/04/2020
/*/
//--------------------------------------------------------------------------------------------------------------------
Function AT190GHrsT( cCampo )
Local oModel	:= FwModelActive()
Local oMdlALC	:= oModel:GetModel("ALCDETAIL")
Local cRet		:= "00:00"

If !Empty(oMdlALC:GetValue("ALC_ENTRADA")) .AND. !Empty(oMdlALC:GetValue("ALC_SAIDA"))
	cRet := Left(ElapTime(oMdlALC:GetValue("ALC_ENTRADA")+":00", oMdlALC:GetValue("ALC_SAIDA")+":00"), 5)
EndIf
Return cRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AT190GSalH
@description Fun��o de Calculo de saldo de horas
@return nTotal
@author Augusto Albuquerque
@since  03/04/2020
/*/
//--------------------------------------------------------------------------------------------------------------------
Function AT190GSalH( lSubtrai, cOldValue, cNewValue )
Local oModel	:= FwModelActive()
Local oMdlTOT	:= oModel:GetModel("TOTDETAIL")
Local cTotal	:= "00:00"

If lSubtrai
	cTotal := TecConvHr(SomaHoras(SubHoras(TecConvHr(oMdlTOT:GetValue("TOT_SALHRS")), TecConvHr(cOldValue)), TecConvHr(cNewValue)))
Else
	cTotal := TecConvHr(SomaHoras(TecConvHr(oMdlTOT:GetValue("TOT_SALHRS")), TecConvHr(cNewValue)))
EndIf
oMdlTOT:LoadValue("TOT_SALHRS", cTotal)

oMdlTot:LoadValue("TOT_RESTSAL", TecConvHr(SubHoras(TecConvHr(oMdlTOT:GetValue("TOT_CTRSAL")), TecConvHr(cTotal))))

Return .T.

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AT190GPrAL
@description Realiza valida��es da linha do modelo ALC
@return lRet
@author Augusto Albuquerque
@since  03/04/2020
/*/
//--------------------------------------------------------------------------------------------------------------------
Function AT190GPrAL(oMdlG, nLine, cAcao, cCampo, xValue, xOldValue)
Local lRet 			:= .T.
Local aSaveLines	:= FWSaveRows()
Local aArea			:= GetArea()
Local oMdlTOT		:= Nil
Local lSubtrai		:= .F.

If cAcao == 'DELETE'
	If TecConvHr(oMdlG:GetValue("ALC_TOTHRS")) > 0
		oMdlTOT := oMdlG:GetModel():GetModel("TOTDETAIL")
		oMdlTOT:LoadValue("TOT_SALHRS", TecConvHr(SubHoras(TecConvHr(oMdlTOT:GetValue("TOT_SALHRS")), TecConvHr(oMdlG:GetValue("ALC_TOTHRS")))))
		oMdlTOT:LoadValue("TOT_RESTSAL", TecConvHr(SubHoras(TecConvHr(oMdlTOT:GetValue("TOT_CTRSAL")), TecConvHr(oMdlTOT:GetValue("TOT_SALHRS")))))
	EndIf
EndIf

If cAcao == 'UNDELETE'
	If TecConvHr(oMdlG:GetValue("ALC_TOTHRS")) > 0
		oMdlTOT := oMdlG:GetModel():GetModel("TOTDETAIL")
		oMdlTOT:LoadValue("TOT_SALHRS", TecConvHr(SomaHoras(TecConvHr(oMdlTOT:GetValue("TOT_SALHRS")), TecConvHr(oMdlG:GetValue("ALC_TOTHRS")))))
		oMdlTOT:LoadValue("TOT_RESTSAL", TecConvHr(SubHoras(TecConvHr(oMdlTOT:GetValue("TOT_CTRSAL")), TecConvHr(oMdlTOT:GetValue("TOT_SALHRS")))))
	EndIf
EndIf

If cAcao == 'SETVALUE'

	If cCampo == "ALC_TURNO"
		If !EMPTY(xValue)
			lRet := CheckSR6(AT190dLimp(xValue))
			If !lRet
				Help(,,"AT190GPrAL",, STR0026 ,1,0) //"O turno digitado n�o foi encontrado."
			EndIf
		EndIf
		WhensALC("ALC_TURNO")
	ElseIf cCampo == "ALC_SEQ"
		If !EMPTY(xValue)
			lRet := CheckSPJ(AT190dLimp(xValue), , oMdlG:GetValue("ALC_TURNO") )
			If !lRet
				Help(,,"AT190GPrAL",, STR0027 ,1,0) //"A sequencia digitada n�o foi encontrada."
			EndIf
		EndIf
	EndIf
	If (cCampo == "ALC_ENTRADA" .OR. cCampo == "ALC_SAIDA")
		If LEN(ALLTRIM(xValue)) == 5 .AND. AT(":",xValue) == 0
			lRet := .F.
			Help( " ", 1, "AT190GPrAL", Nil, STR0038, 1 )	//"Hor�rio inv�lido. Por favor, insira um hor�rio no formato HH:MM"
		EndIf
		If AT(":",xValue) == 0 .AND. AtJustNum(Alltrim(xValue)) == Alltrim(xValue) .AND. lRet
			If LEN(Alltrim(xValue)) == 4
				xValue := LEFT(Alltrim(xValue),2) + ":" + RIGHT(Alltrim(xValue),2)
			ElseIf LEN(Alltrim(xValue)) == 2
				xValue := Alltrim(xValue) + ":00"
			ElseIf LEN(Alltrim(xValue)) == 1
				xValue := "0" + Alltrim(xValue) + ":00"
			EndIf
		EndIf
		If lRet
			lRet := AtVldHora(Alltrim(xValue))
		EndIf
	EndIf
	If cCampo == 'ALC_TOTHRS'
		If TecConvHr(xValue) < 0 
			Help(,,"AT190GPrAL",, STR0028 ,1,0) //"O total de Horas do dia n�o pode ser menor que o total de horas restante."
			lRet := .F.
		EndIf
		If !Empty(xOldValue)
			lSubtrai := .T.
		EndIf
		AT190GSalH( lSubtrai, xOldValue, xValue )
	EndIf
EndIf

FWRestRows( aSaveLines )
RestArea(aArea)
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AT190GVld
@description Realiza valida��es do modelo ALC
@return lRet
@author Augusto Albuquerque
@since  03/04/2020
/*/
//--------------------------------------------------------------------------------------------------------------------
Function AT190GVld( )
Return ChecarConf()
//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AT190GPoAL
@description Realiza valida��es pos linha da ALC
@return lRet
@author Augusto Albuquerque
@since  03/04/2020
/*/
//--------------------------------------------------------------------------------------------------------------------
function AT190GPoAL(oMdlG, nLine, cAcao, cCampo)
Local aArea			:= GetArea()
Local aSaveLines	:= FWSaveRows()
Local lRet      	:= .T.
Local oMdlFull 		:= oMdlG:GetModel()
Local oMdlTOT		:= oMdlFull:GetModel("TOTDETAIL")

If TecConvHr(oMdlG:GetValue("ALC_TOTHRS")) == 0
	Help(,,"AT190GPoAL",, STR0028 ,1,0) //"O total de Horas do dia n�o pode ser menor que o total de horas restante."
	lRet := .F.
ElseIf TecConvHr(oMdlTOT:GetValue("TOT_SALHRS")) < 0
	Help(,,"AT190GPoAL",, STR0030 ,1,0) // "O saldo de horas n�o pode ser negativo"
	lRet := .F.
ElseIf SubHoras(TecConvHr(oMdlTOT:GetValue("TOT_CTRSAL")), TecConvHr(oMdlTOT:GetValue("TOT_SALHRS"))) < 0
	Help(,,"AT190GPoAL	",, STR0036 ,1,0) // "O Total de Horas usada para aloa��o n�o pode ser maior que o do contrato."
	lRet := .F.
EndIf

If lRet

EndIf

FWRestRows( aSaveLines )
RestArea(aArea)

Return(lRet)

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ChecarConf
@description Mensagem de espera para fazer a checagem
@author Augusto Albuquerque
@since  03/04/2020
/*/
//--------------------------------------------------------------------------------------------------------------------
Function ChecarConf()
Local lRet := .T.
FwMsgRun(Nil,{|| ChkConf(@lRet)}, Nil, STR0032) // "Realizando a checagem de conflitos!"
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} WhensALC
@description Checa conflitos
@return lOpc
@author Servi�os
@since  03/04/2020
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ChkConf( lRet )
Local aFieldsQry := {'AA1_FILIAL','AA1_CODTEC','AA1_NOMTEC','ABB_DTINI','ABB_HRINI','ABB_DTFIM',;
					'ABB_HRFIM','RA_SITFOLH','RA_DEMISSA','RF_DATAINI','RF_DFEPRO1','RF_DATINI2',;
					'RF_DFEPRO2','RF_DATINI3','RF_DFEPRO3','R8_DATAINI','R8_DATAFIM',;
					"DTINI", "DTFIM", 'HRINI' ,"HRFIM", "ATIVO"}
Local cFilTFF := xFilial( "TFF", cFilAnt )
Local aArrConfl := {}
Local aArrDem := {}
Local aArrAfast := {}
Local aArrDFer := {}
Local aArrDFer2 := {}
Local aArrDFer3 := {}
Local aAuxAgenda	:= {}
Local aAux		:= {}
Local aAgenda	:= {}
Local lHasConfl	:= .F.
Local lRestrRH	:= .F.
Local oModel	:= FwModelActive()
Local oMdlALC	:= oModel:GetModel("ALCDETAIL")
Local cCodTec	:= FwFldGet("AA1_CODTEC")
Local cCodTff	:= oMdl190d:GetValue("TGYMASTER", "TGY_TFFCOD")
Local nX
Local nJ
Local lGSVERHR := SuperGetMV("MV_GSVERHR",,.F.)
Local lRestrRH := .F.
Local lProcessa := .T.
Local lAterouSeq := .F.
Local lResRHTXB	:= TableInDic("TXB")
Local oView 		:= FwViewActive()
Local lContinua	:= .T.
Local lHelp := .T.
Local nLineBckp := oMdlALC:GetLine()
Local lPergunte	:= .F.
Local nOpc := 1

For nX := 1 To oMdlALC:Length()
	oMdlAlc:GoLine(nX)
	If !oMdlAlc:IsDeleted()
		If lGSVERHR
			If AsCan( aAgenda,{|e| e[1] == oMdlAlc:GetValue("ALC_DATREF") .AND. ((e[3] <= oMdlALc:GetValue("ALC_ENTRADA") .AND. e[3] >= oMdlALc:GetValue("ALC_ENTRADA"));
									.OR. e[3] <= oMdlALc:GetValue("ALC_SAIDA") .AND. e[3] >= oMdlALc:GetValue("ALC_SAIDA"))}) > 0
				lContinua := .F.
			EndIf
		Else
			If AsCan( aAgenda,{|e| e[1] == oMdlAlc:GetValue("ALC_DATREF")}) > 0 
				lContinua := .F.
			EndIf
		EndIf
		AADD( aAgenda, {oMdlAlc:GetValue("ALC_DATREF"),;
						oMdlALc:GetValue("ALC_SAIDA"),;
						oMdlALc:GetValue("ALC_ENTRADA")})
		If lContinua
			cHorIni := oMdlALc:GetValue("ALC_ENTRADA")
			cHorFim := oMdlALc:GetValue("ALC_SAIDA")
			dDatIni := oMdlALc:GetValue("ALC_DATREF")
			dDatFim := If( cHorFim < cHorIni, dDatIni + 1, dDatIni)
			ChkCfltAlc(dDatIni, dDatFim, cCodTec, /*cHoraIni*/, /*cHoraFim*/, .F., @aFieldsQry,;
										@aArrConfl, @aArrDem, @aArrAfast, @aArrDFer, @aArrDFer2, @aArrDFer3)
			aAteABB := At330AVerABB( dDatIni, dDatFim, cCodTff, cFilTFF, cCodTec )
						
			If !EMPTY(aAteABB)
				aAuxAgenda := ACLONE(aAteABB)
				ASORT(aAuxAgenda,,, { |x, y| x[1] < y[1] } )
				If dDatIni <= aAuxAgenda[1][1] .AND. dDatFim > aAuxAgenda[LEN(aAuxAgenda)][1]
					cSeq := aAuxAgenda[1][8]
					lAterouSeq := .T.
				EndIf
			EndIf

			If lResRHTXB
				nTXBDtIni := AScan(aFieldsQry,{|e| e == 'TXB_DTINI'})
				nTXBDtFim := AScan(aFieldsQry,{|e| e == 'TXB_DTFIM'})
			Endif

			nPosdIni := AScan(aFieldsQry,{|e| e == 'DTINI'})
			nPosdFim := AScan(aFieldsQry,{|e| e == 'DTFIM'})
			nPosHrIni := AScan(aFieldsQry,{|e| e == 'HRINI'})
			nPosHrFim := AScan(aFieldsQry,{|e| e == 'HRFIM'})

			aAux := Array(TAMANHO)
			lRestrRH := .F.

			If !lRestrRH .And. Len(aArrDFer) > 0  
				nPos := Ascan(aArrDFer,{|x| Alltrim(x[1]) == Alltrim(cCodTec) .AND. dDatIni >= x[2] .And. dDatIni <= x[3] } )
				If (lRestrRH :=  nPos > 0)
					lHasConfl := .T.
				EndIf
			EndIf

			If !lRestrRH .And. Len(aArrDFer2) > 0  
				nPos := Ascan(aArrDFer2,{|x| Alltrim(x[1]) == Alltrim(cCodTec) .AND. dDatIni >= x[2] .And. dDatIni <= x[3] } )
				If (lRestrRH :=  nPos > 0)
					lHasConfl := .T.
				EndIf
			EndIf  

			If !lRestrRH .And. Len(aArrDFer3) > 0  
				nPos := Ascan(aArrDFer3,{|x| Alltrim(x[1]) == Alltrim(cCodTec) .AND. dDatIni >= x[2] .And. dDatIni <= x[3] } )
				If (lRestrRH := nPos > 0)
					lHasConfl := .T.
				EndIf
			EndIf

			If !lRestrRH .And. Len(aArrDem) > 0  
				nPos := Ascan(aArrDem,{|x| Alltrim(x[1]) == Alltrim(cCodTec) .AND.  dDatIni >= x[2] } )
				If (lRestrRH := nPos > 0)
					lHasConfl := .T.
				EndIf
			EndIf

			If !lRestrRH .And. Len(aArrAfast) > 0  
				nPos := Ascan(aArrAfast,{|x| Alltrim(x[1]) == Alltrim(cCodTec) .AND. dDatIni >= x[2] .And. dDatIni <= x[3] } )
				If (lRestrRH := nPos > 0)
					lHasConfl := .T.
				EndIf
			EndIf

			If !lRestrRH .And. Len(aArrConfl) > 0 .And. lResRHTXB
				nPos := Ascan(aArrConfl,{|x| Alltrim(x[2]) == Alltrim(cCodTec) .AND.;
							!Empty(x[nTXBDtIni]) .AND. dDatIni >= sTod(x[nTXBDtIni]) .And.;
							( Empty(x[nTXBDtFim]) .Or. dDatIni <= sTod(x[nTXBDtFim]) ) } )
				If (lRestrRH := nPos > 0)
					lHasConfl := .T.
				EndIf
			EndIf

			If !lRestrRH .And. Len(aArrConfl) > 0  
				nLastPos := 0
				nHrIniAge := VAL(AtJustNum(cHorIni))
				nHrFimAge := VAL(AtJustNum(cHorFim))	
				nAddZZX := IIF(nHrIniAge >= nHrFimAge, 2400,0)	
				lProcessa := .T.
						
				Do While lProcessa
					nLastPos++
					nPos := Ascan(aArrConfl,{|x| Alltrim(x[2]) == Alltrim(cCodTec) .And.;
							(dDatIni == x[nPosdIni] .Or.  dDatIni == x[nPosdFim] )}, nLastPos )
					nLastPos := nPos
					If nPos > 0				
						lRestrRH := .T.				
						aAux[EXSABB] := "1"		
						If lRestrRH .And. lGSVERHR .And. (Upper(AllTrim(cHorIni)) <> "FOLGA" .And. Upper(AllTrim(cHorFim)) <> "FOLGA")
							nHrIni := VAL(AtJustNum(aArrConfl[nPos,nPosHrIni]))
							nHrFim := VAL(AtJustNum(aArrConfl[nPos,nPosHrFim]))

							If nHrFim <= nHrIni  
								nAddABB := 2400
							Else
								nAddABB := 0
							EndIf 
							
							If 	( nHrIniAge >= nHrIni .AND.;
									nHrIniAge <= ( nHrFim + nAddABB ) ) ;
										.OR.;
								( ( nHrFimAge  + nAddZZX ) >= nHrIni .AND.;
									( nHrFimAge  + nAddZZX ) <= (nHrFim + nAddABB ));
										.OR.;
								( nHrIniAge <= nHrIni .AND.;
									( nHrFimAge + nAddZZX ) >= ( nHrFim + nAddABB ) )
								lRestrRH := .T.
								aAux[EXSABB] := "1"
								lProcessa := .F.					
							Else
								lRestrRH := .F.
								aAux[EXSABB] := "2"
							EndIf
						Else
							If (Upper(AllTrim(cHorIni)) == "FOLGA" .And. Upper(AllTrim(cHorFim)) == "FOLGA")
								lRestrRH := .F.
								aAux[EXSABB] := "2"
							EndIf
							lProcessa := .F.					
						EndIf				
					Else		
						aAux[EXSABB] := "2"
						lProcessa := .F.
					EndIf
				End 
			Else			
				aAux[EXSABB] := "2"
			EndIf

			If aAux[EXSABB] == "1"
				lHasConfl := .T.
				
			EndIf
		Else
			If lHelp 
				lRestrRH := .T.
				lHelp := .F.
			EndIf
		EndIf
	EndIf
	oMdlALC:LoadValue("ALC_SITABB", At330ACLgA( , , , lRestrRH ))
	If lRestrRH .AND. !lPergunte
		lPergunte := .T.
	EndIf
Next nX

oMdlALC:GoLine(nLineBckp)

If !lHelp .OR. lPergunte
	nOpc := Aviso( STR0039, STR0040, {STR0041, STR0042}) // "Aten��o" ## "Uma ou mais agendas possui confilto deseja continuar com as valida��es ou visualizar as agendas?" ## "Continuar" ## "Visualizar"
EndIf
If nOpc == 1
	lRet := At190dExec("VldGrvAloc()")
else
	lRet := .F.
EndIf
If !lRet .AND. !IsBlind()
	Help(,,"AT190GHrsT",, STR0043 ,1,0) // "Opera��o Cancelada."
	oView:Refresh("DETAIL_ALC")
EndIf
Return .T.

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} WhensALC
@description Faz o travamento do campo na estrutura ALC
@return lOpc
@author Augusto Albuquerque
@since  03/04/2020
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function WhensALC( cCampo )
Local oModel := FwModelActive()
Local oMdlALC := oModel:GetModel("ALCDETAIL")
Local lOpc := .T.

If Empty(oMdlALC:GetValue(cCampo))
	lOpc := .F.
EndIf

Return lOpc

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} CheckSR6
@description Fazer a checagem na tabela SR6 para ver se o registro existe
@return lRet
@author Augusto Albuquerque
@since  03/04/2020
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function CheckSR6( xValue, cFilCtr )
Local cQry
Local lRet := .T.
Local lMV_MultFil := TecMultFil() //Indica se a Mesa considera multiplas filiais
Default cFilCtr := cFilAnt
cQry := " SELECT 1 "
cQry += " FROM " + RetSqlName("SR6") + " SR6 "
cQry += " WHERE SR6.R6_FILIAL = '" +  xFilial('SR6', IIF(lMV_MultFil,cFilCtr,cFilAnt)) + "' AND "
cQry += " SR6.D_E_L_E_T_ = ' ' "
cQry += " AND SR6.R6_TURNO = '" + xValue + "' "
If (QryEOF(cQry))
	lRet := .F.
EndIf

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} CheckSPJ
@description Fazer a checagem na tabela SPJ para ver se o registro existe
@return lRet
@author Augusto Albuquerque
@since  03/04/2020
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function CheckSPJ( xValue, cFilCtr, cTurno )
Local cQry
Local lRet := .T.
Local lMV_MultFil := TecMultFil() //Indica se a Mesa considera multiplas filiais
Default cFilCtr := cFilAnt
cQry := " SELECT 1 "
cQry += " FROM " + RetSqlName("SPJ") + " SPJ "
cQry += " WHERE SPJ.PJ_FILIAL = '" +  xFilial('SPJ', IIF(lMV_MultFil,cFilCtr,cFilAnt)) + "' AND "
cQry += " SPJ.D_E_L_E_T_ = ' ' "
cQry += " AND SPJ.PJ_TURNO = '" + cTurno + "' "
cQry += " AND SPJ.PJ_SEMANA = '" + xValue + "' "
If (QryEOF(cQry))
	lRet := .F.
EndIf

Return lRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} QryEOF()

Executa uma qry e retorna se EOF

@author boiani
@since 29/07/2019
/*/
//------------------------------------------------------------------------------
Static Function QryEOF(cSql, lChangeQry)
Local lRet := .F.
Local cAliasQry := GetNextAlias()
Default lChangeQry := .T.
If lChangeQry
	cSql := ChangeQuery(cSql)
EndIf
dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql),cAliasQry, .F., .T.)
lRet := (cAliasQry)->(EOF())
(cAliasQry)->(DbCloseArea())
Return lRet


//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} GetTurnTGY
@description retorno do ultimo turno da TGY
@author Augusto Albuquerque
@since  03/04/2020
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function GetTurnTGY( cCampo )
Local cQry		:= ""
Local cRet 		:= ""
Local cAliasQry := GetNextAlias()
Local cContCamp	:= ""

cQry := " SELECT TGY.TGY_TURNO, TGY.TGY_ULTALO, TGY.TGY_SEQ "
cQry += " FROM " + RetSqlName("TGY") + " TGY "
cQry += " WHERE TGY.TGY_FILIAL = '" +  xFilial('TGY') + "' AND "
cQry += " TGY.D_E_L_E_T_ = ' ' "
cQry += " AND TGY.TGY_ATEND = '" + oMdl190d:GetValue("AA1MASTER","AA1_CODTEC") + "' "
cQry += " ORDER BY TGY_ULTALO DESC "

cQry := ChangeQuery(cQry)
dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQry),cAliasQry, .F., .T.)
If !(cAliasQry)->(EOF())
	If cCampo == 'TURNO'
		cRet := (cAliasQry)->TGY_TURNO
	Else
		cRet := (cAliasQry)->TGY_SEQ
	EndIf
EndIf

(cAliasQry)->(DbCloseArea())

If Empty(cRet)
	If cCampo == 'TURNO'
		cContCamp := "TFF_TURNO"
	Else
		cContCamp := "TFF_SEQTRN"
	EndIf
	cRet := POSICIONE("TFF",1,oMdl190d:GetValue("TGYMASTER", "TGY_FILIAL")+oMdl190d:GetValue("TGYMASTER", "TGY_TFFCOD"), cContCamp)
EndIf
Return cRet