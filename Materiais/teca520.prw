#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWEVENTVIEWCONSTS.CH"
#INCLUDE "TECA520.CH"

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TECA520()
Agendamento dos Atendentes - ABB
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TECA520(xAutoItens,nOpcAuto)

Default xAutoItens	:= {}
Default nOpcAuto		:= MODEL_OPERATION_VIEW

If !Empty(xAutoItens)
	FWMVCRotAuto(ModelDef(),"ABB",nOpcAuto,{{"ABBDETAIL",xAutoItens}})
Else
	DEFINE FWMBROWSE oMBrowse ALIAS "ABB" DESCRIPTION STR0001 //"Agendamentos"
	ACTIVATE FWMBROWSE oMBrowse
EndIf

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef()
Defini��o do Model 
@return ExpO:oModel
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel
Local oStruCab	:= FWFormStruct(1,'ABB',{|cCampo| AllTrim(cCampo)+"|" $ "ABB_CODTEC|ABB_NOMTEC|"})
Local oStruGrid	:= FWFormStruct(1,'ABB')
Local aAux			:= {}						// Auxiliar para criar a trigger

//Ajustes do Dicionario
aAux := FwStruTrigger('ABB_CODTEC',;
                      'ABB_NOMTEC',;
                      'AA1->AA1_NOMTEC',;
                      .T.,;
                      'AA1',;
                      1,;
                      'xFilial("AA1")+FwFldGet("ABB_CODTEC")')
oStruCab:AddTrigger(aAux[1],;	// [01] identificador (ID) do campo de origem
                    aAux[2],;	// [02] identificador (ID) do campo de destino
                    aAux[3],;	// [03] Bloco de c�digo de valida��o da execu��o do gatilho
                    aAux[4])	// [04] Bloco de c�digo de execu��o do gatilho

//Ajustes do Dicionario
aAux := FwStruTrigger('ABB_ENTIDA',;
                      'ABB_CHAVE',;
                      'Space(TamSX3("ABB_CHAVE")[1])',;
                      .F.,;
                      ,;
                      ,;
                      )
oStruGrid:AddTrigger(aAux[1],;	// [01] identificador (ID) do campo de origem
                     aAux[2],;	// [02] identificador (ID) do campo de destino
                     aAux[3],;	// [03] Bloco de c�digo de valida��o da execu��o do gatilho
                     aAux[4])	// [04] Bloco de c�digo de execu��o do gatilho 	 		

oStruCab:SetProperty("ABB_NOMTEC",MODEL_FIELD_INIT,{|| If(!INCLUI,(POSICIONE("AA1", 1, XFilial("AA1")+ABB->ABB_CODTEC, "AA1_NOMTEC")),AA1->AA1_NOMTEC)})
oStruCab:SetProperty("ABB_CODTEC",MODEL_FIELD_INIT,{|| AA1->AA1_CODTEC })

oStruGrid:RemoveField('ABB_CODTEC')
oStruGrid:RemoveField('ABB_NOMTEC')

oStruGrid:SetProperty("ABB_DTINI", MODEL_FIELD_INIT,   {|| If(Type("dData") != "U",dData,dDatabase)})
oStruGrid:SetProperty("ABB_DTFIM", MODEL_FIELD_INIT,   {|| If(Type("dData") != "U",dData,dDatabase)})
oStruGrid:SetProperty("ABB_NUMOS", MODEL_FIELD_OBRIGAT,.F.)
oStruGrid:SetProperty("ABB_ENTIDA",MODEL_FIELD_OBRIGAT,.F.)
oStruGrid:SetProperty("ABB_CHAVE", MODEL_FIELD_OBRIGAT,.F.)
oStruGrid:SetProperty("ABB_ENTIDA",MODEL_FIELD_WHEN,{|| IIf(oModel:GetOperation()==4 .AND. ("CN9" $ M->ABB_IDCFAL), .F., .T.) })
oStruGrid:SetProperty("ABB_CHAVE", MODEL_FIELD_WHEN,{|| IIf(oModel:GetOperation()==4 .AND. ("CN9" $ M->ABB_IDCFAL), .F., .T.) })
 
oModel := MPFormModel():New('TECA520',/*bPre*/,{|oMdl| Ta520PosVld(oMdl)}/*bPosValidacao*/,{|oMdl| Ta520Comm(oMdl)},/*bCancel*/)
oModel:AddFields('ABBCAB',/*cOwner*/,oStruCab,/*bPreValidacao*/,/*pos*/,/* load */)
oModel:AddGrid('ABBGRID','ABBCAB',oStruGrid,/*bLinePre*/,{|oMdl| Ta520LPos(oMdl)},/*bPreVal*/,/*bPosVal*/,{|oMdl,lCopy| TA520Load(oMdl,lCopy)})
oModel:GetModel("ABBGRID"):SetOptional(.T.)
oModel:GetModel("ABBGRID"):SetUseOldGrid()

oModel:SetVldActivate( { |oModel| At510FPGrl(oModel) } )

//ABB_FILIAL+ABB_CODTEC+DTOS(ABB_DTINI)+ABB_HRINI+DTOS(ABB_DTFIM)+ABB_HRFIM 
oModel:SetRelation("ABBGRID",{{"ABB_FILIAL",'xFilial("ABB")'},{"ABB_CODTEC","ABB_CODTEC"}},ABB->(IndexKey(1)))

oModel:SetPrimaryKey({'ABB_FILIAL','ABB_CODTEC'})
                                     
Return(oModel)

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Ta520PosVld()
Pos Valida��o Model
@param ExpO:oModel
@return ExpL:.T. quando v�lido
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function Ta520PosVld(oModel)
Local nOpc			:= oModel:GetOperation()
Local lRet			:= .T.
Local nCntFor		:= 1
Local aSaveLines	:= {}
Local oMdl			:= oModel:GetModel("ABBGRID")
Local nI

If nOpc == MODEL_OPERATION_INSERT
	//Inicializador padrao da inclusao diz q o model principal precisa de modifica��o :-(
	oModel:GetModel("ABBCAB"):ADATAMODEL[1][1][3] := .T.

	lRet := .F.
	aSaveLines := FWSaveRows()
	
	For nI := 1 To oMdl:Length()
		oMdl:GoLine(nCntFor)
		If !oMdl:IsDeleted() .AND. !Empty(FwFldGet("ABB_CHAVE"))
			lRet := .T.
			Exit
		EndIf
	Next nCntFor
	
	FwRestRows(aSaveLines)
EndIf

If !lRet
	Help("",1,"TA520POSVLD",,STR0003,2,0) //"� necess�rio incluir ao menos um agendamento."
EndIf
	
Return lRet


//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef()
Defini��o da View 
@return ExpO:oView
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ViewDef()

Local oView

Local oModel		:= FWLoadModel('TECA520')
Local oStruCab	:= FWFormStruct(2,'ABB',{|cCampo| AllTrim(cCampo)+"|" $ "ABB_FILIAL|ABB_CODTEC|ABB_NOMTEC|"})
Local oStruGrid	:= FWFormStruct(2,'ABB')

oStruGrid:RemoveField('ABB_CODTEC')
oStruGrid:RemoveField('ABB_NOMTEC')
oStruGrid:RemoveField('ABB_NUMOS')

oView := FWFormView():New()
oView:SetModel(oModel)

oView:AddField('VIEW_CAB',oStruCab,'ABBCAB')
oView:AddGrid('VIEW_GRID',oStruGrid,'ABBGRID' )

oView:CreateHorizontalBox('SUPERIOR',12)
oView:CreateHorizontalBox('INFERIOR',88)

oView:SetOwnerView( 'VIEW_CAB','SUPERIOR' )
oView:SetOwnerView( 'VIEW_GRID','INFERIOR' )

Return(oView)


//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef()
Op��es da Rotina
@return ExpA:aRotina
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function MenuDef()
Return FWMVCMenu( "TECA520" )


//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA520Load()
Load dos dados para o grid 
@param ExpO:Model do Grid
@param ExpL:Se � ou nao copia.
@return ExpA:Acols no Formato da fun��o FormLoadGrid
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA520Load(oMdlGrid,lCopy)

Local aColsGrid	:= {}
Local aLinha		:= {}
Local cAlias		:= GetNextAlias()
Local nX			:= 0
Local nY			:= 0
Local cInit		:= ""

If IsInCallStack("TECA500") .AND. Type("dData") == "D"
	BeginSQL alias cAlias
		SELECT ABB.R_E_C_N_O_ REC
		  FROM %table:ABB% ABB
		 WHERE ABB.ABB_FILIAL = %xfilial:ABB%
		   AND ABB.ABB_CODTEC = %exp:AA1->AA1_CODTEC%
		   AND %exp:dData% BETWEEN ABB.ABB_DTINI AND ABB.ABB_DTFIM
		   AND ABB.%notDel%
	EndSQL

	DbSelectArea(cAlias)
	While (cAlias)->(!Eof())
		nX++
		aLinha := {{},{}}
		ABB->(DbGoTo((cAlias)->REC))
		For nY := 1 To Len(oMdlGrid:aHeader)
			If oMdlGrid:aHeader[nY][10] == "V"
				cInit := Posicione("SX3",2,oMdlGrid:aHeader[nY][2],"X3_RELACAO")
				If !Empty(cInit)
					AAdd(aLinha[2],InitPad(cInit))
				Else
					If oMdlGrid:aHeader[nY][2] == "ABB_NOMTEC"
						AAdd(aLinha[2],POSICIONE("AA1", 1, XFilial("AA1")+ABB->ABB_CODTEC, "AA1_NOMTEC"))
					Else
						AAdd(aLinha[2],"")
					EndIf
				EndIf
			Else
				AAdd(aLinha[2],&("ABB->"+oMdlGrid:aHeader[nY][2]))
			EndIf
		Next nY
		AAdd(aLinha[2],.F.)
		aLinha[1] := (cAlias)->REC
		Aadd(aColsGrid,aLinha)
		DbSkip()
	EndDo
	(cAlias)->(DbCloseArea())

Else
	aColsGrid := FormLoadGrid(oMdlGrid,lCopy)
EndIf

ABB->(DbGoBottom())
ABB->(DbSkip())

Return aColsGrid 


//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Ta520LPos()
Linha OK do Grid do TECA520 e TECA521
@param ExpO:Model do Grid
@return ExpL:.T. quando linha valida
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Ta520LPos(oMdl)	
Return Ta520VlDt(.T.,oMdl:GetDataId(),oMdl) //Valida Se j� h� agendamento para o t�cnico


//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Ta520Comm()
Commit do Teca520 e Teca521
@param ExpO:Model
@return ExpL:.T. quando sucesso no commit
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Ta520Comm(oMdl)

Local nX			:= 0
Local lAT500Alt	:= ExistBlock("AT500ALT")
Local lAT500Grv	:= ExistBlock("AT500GRV")
Local lAT500Del	:= ExistBlock("AT500DEL")
Local oMdlGrid	:= oMdl:GetModel("ABBGRID")
Local cEventID
Local cMensagem
Local cTecnico
Local nOpc			:= oMdl:GetOperation()
Local bBeforeLin	:= NIL
Local bAfterLin	:= NIL
Local lRet			:= .F.

If ExistBlock("AT500OK")
	ExecBlock("AT500OK", .F., .F.)
EndIf

For nX := 1 To oMdlGrid:Length()

	oMdlGrid:GoLine(nX)
	//Corrige Filial - Por algum motivo o MVC as vezes grava errado
	If nOpc == MODEL_OPERATION_INSERT .OR. nOpc == MODEL_OPERATION_UPDATE
		oMdlGrid:SetValue("ABB_FILIAL",XFilial("ABB"))
	ElseIf nOpc == MODEL_OPERATION_DELETE		
		If lAT500Del
			bBeforeLin := {|| ExecBlock( "AT500DEL", .F., .F.,{oMdl}) }
		EndIf
		
		DbSelectArea("AAT")
		DbSetOrder(1)
		If AAT->(DbSeek(xFilial("AAT")+ALLTRIM(oMdlGrid:GetValue("ABB_CHAVE"))))
			RecLock("AAT",.F.)
			AAT->AAT_STATUS	:= "1"
			AAT->AAT_DTINI	:= CTOD("  /  /  ")
			AAT->AAT_HRINI	:= ""
			AAT->AAT_DTFIM	:= CTOD("  /  /  ")
			AAT->AAT_HRFIM	:= ""
			MsUnLock()
		EndIf
	EndIf

	If !Empty(oMdlGrid:GetValue("ABB_NUMOS"))
		If nOpc != MODEL_OPERATION_DELETE

			AB6->( DbSetOrder( 1 ) )
			AB6->( DbSeek( xFilial( "AB6" ) + oMdlGrid:GetValue("ABB_NUMOS") ) )

			SA1->( DbSetOrder( 1 ) )
			SA1->( DbSeek( xFilial( "SA1" ) + AB6->AB6_CODCLI + AB6->AB6_LOJA ) )

			ABB->( DbSetOrder( 1 ) )
			ABB->( DbGoTo(oMdlGrid:GetDataId()) )

			// "Cliente: ", "Endereco: ", "Municipio: ", "UF: ", "Telefone: " 
			cCorpo 	:=	AllTrim(RetTitle( "A1_COD" ))+ ": " + SA1->A1_COD + " - " + SA1->A1_NOME + CRLF +;
							AllTrim(RetTitle( "A1_END" ))+ ": " + SA1->A1_END + CRLF +;
							AllTrim(RetTitle( "A1_MUN" ))+ ": " + SA1->A1_MUN + CRLF +;
							AllTrim(RetTitle( "A1_EST" ))+ ": " + SA1->A1_EST + CRLF +;
							AllTrim(RetTitle( "A1_TEL" )) +": " + SA1->A1_TEL

			If ( !oMdlGrid:IsDeleted() .And. !Empty(oMdlGrid:GetValue("ABB_HRINI")) )
				//������������������������������������������������������������������������Ŀ
				//� Envia o e-Mail caso um campo importante tenha sido alterado            �
				//��������������������������������������������������������������������������		
				If	ABB->ABB_DTINI <> oMdlGrid:GetValue("ABB_DTINI") .Or. ABB->ABB_HRINI <> oMdlGrid:GetValue("ABB_HRINI") .Or. ;
						ABB->ABB_DTFIM <> oMdlGrid:GetValue("ABB_DTFIM") .Or. ABB->ABB_HRFIM <> oMdlGrid:GetValue("ABB_HRFIM") .Or. ;
						ABB->ABB_NUMOS <> oMdlGrid:GetValue("ABB_NUMOS")

					//��������������������������������������������Ŀ
					//� Event Viewer - Envia e-mail ou RSS para    �
					//| Alocacao de Tecnicos - Field Service.      �
					//����������������������������������������������

					cEventID  := "009"
					cMensagem := RetTitle("ABB_NUMOS") + " - " + oMdlGrid:GetValue("ABB_NUMOS") + " " + cCorpo
					cTecnico  := RetTitle("ABB_CODTEC") + " - " + AllTrim( AA1->AA1_NOMTEC )
					EventInsert(FW_EV_CHANEL_ENVIRONMENT, FW_EV_CATEGORY_MODULES, cEventID, FW_EV_LEVEL_INFO,""/*cCargo*/,cTecnico,cMensagem)

				EndIf
			Else
				//��������������������������������������������Ŀ
				//� Event Viewer - Envia e-mail ou RSS para    �
				//| Alocacao de Tecnicos - Field Service.      �
				//����������������������������������������������
				cEventID  := "009"
				cMensagem := RetTitle("ABB_NUMOS") + " - " + oMdlGrid:GetValue("ABB_NUMOS") + " " + cCorpo
				cTecnico  := RetTitle("ABB_CODTEC") + " - " + AllTrim( AA1->AA1_NOMTEC )
				EventInsert(FW_EV_CHANEL_ENVIRONMENT, FW_EV_CATEGORY_MODULES, cEventID, FW_EV_LEVEL_INFO,""/*cCargo*/,cTecnico,cMensagem)
			EndIf
		EndIf
	EndIf

	If oMdlGrid:IsDeleted()
		If lAT500Del
			bBeforeLin := {|| ExecBlock( "AT500DEL", .F., .F.,{oMdl}) }
		EndIf
	Else
		If oMdlGrid:IsUpdated() .And. lAT500Alt
			bBeforeLin := {|| ExecBlock( "AT500ALT", .F., .F.,{oMdl}) }
		EndIf

		If oMdlGrid:IsInserted() .And. lAT500Grv
			bAfterLin := {|| ExecBlock( "AT500GRV", .F., .F.,{oMdl}) }
		EndIf
	EndIf

Next nX

lRet := FwFormCommit(oMdl,bBeforeLin,bAfterLin)
Return lRet


//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Ta520VlDt()
Valida��o das Datas Iniciais e Finais (Acionada pelo LinhaOk do Grid).
@param ExpL:Quanto .T. checar� se existem aloca��es para o per�odo.
@param ExpN:nRecno atual (quando altera��o) para ignorar o registro atual 
@return ExpL:.T. quando sucesso no commit
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Ta520VlDt(lCheckAloc,nRecno,oMdl)

Local cReadVar	:= ""
Local lRetorno	:= .T.
Local dDtIni
Local dDtFim
Local cHrIni
Local cHrFim
Local cCodTec		:= TxGetVar("ABB_CODTEC")
Local cCodEnt		:= TxGetVar("ABB_ENTIDA")
Local cHrVazio	:= "  :  |" + Space( 5 )
Local aArea		:= GetArea()
Local aSaveLines	:= FWSaveRows()
Local nI			:= 0
Local nLinha		:= oMdl:GetLine()
Local lAlocAtiv 	:= ( !oMdl:IsDeleted() .And. TxGetVar("ABB_ATIVO")=='1' )
Local cDtHrIni	:= ""
Local cDtHrFim	:= ""
Local cDtHrIniTmp	:= ""
Local cDtHrFimTmp	:= ""

Default lCheckAloc := .F.

cReadVar := AllTrim( ReadVar() )

dDtIni := TxGetVar( "ABB_DTINI" )
dDtFim := TxGetVar( "ABB_DTFIM" )
cHrIni := TxGetVar( "ABB_HRINI" )
cHrFim := TxGetVar( "ABB_HRFIM" )

//Verifica status do atendente
If ! Ta520StAte(cCodTec)
	Return .F.
EndIf

Do Case
	Case "ABB_DTINI" $ cReadVar
		dDtIni := oMdl:GetValue("ABB_DTINI")
	Case "ABB_DTFIM" $ cReadVar
		dDtFim := oMdl:GetValue("ABB_DTFIM")
	Case "ABB_HRINI" $ cReadVar
		cHrIni := oMdl:GetValue("ABB_HRINI")
	Case "ABB_HRFIM" $ cReadVar
		cHrFim := oMdl:GetValue("ABB_HRFIM")
EndCase

lRetorno := AtVldDiaHr( dDtIni, dDtFim, cHrIni, cHrFim )

If !lRetorno
	Do Case
		Case ( "ABB_DTINI"$cReadVar )
			Help(" ",1,"AT500DATA1")
		Case ( "ABB_DTFIM"$cReadVar )
			Help(" ",1,"AT500DATA2")
	EndCase
EndIf

If lCheckAloc .AND. lRetorno .AND. !Empty(dDtIni) .AND. !Empty(dDtFim) .AND. !(cHrIni $ cHrVazio) .AND. !(cHrFim $ cHrVazio)

	cDtHrIni := DtoS(dDtIni)+cHrIni
	cDtHrFim := DtoS(dDtFim)+cHrFim
	
	If lAlocAtiv
		//Verifica se h� superaloca��o no proprio grid
		For nI := 1 To oMdl:Length()
			oMdl:GoLine(nI)
			cDtHrIniTmp := DtoS(oMdl:GetValue("ABB_DTINI"))+oMdl:GetValue("ABB_HRINI")
			cDtHrFimTmp := DtoS(oMdl:GetValue("ABB_DTFIM"))+oMdl:GetValue("ABB_HRFIM")
	
			If !oMdl:IsDeleted() .And. nLinha != oMdl:GetLine() .AND. !Empty(cDtHrIni) .AND. !Empty(cDtHrFim) .AND. ;
				cCodTec == TxGetVar("ABB_CODTEC") .AND. TxGetVar("ABB_ATIVO")=='1' .AND. (;
					(cDtHrIni		>=	cDtHrIniTmp	.AND.	cDtHrIni		<=	cDtHrFimTmp)	.OR.;
					(cDtHrFim		>=	cDtHrIniTmp	.AND.	cDtHrFim		<=	cDtHrFimTmp)	.OR.;
					(cDtHrIniTmp	>=	cDtHrIni		.AND.	cDtHrIniTmp	<=	cDtHrFim)		.OR.;
					(cDtHrFimTmp	>=	cDtHrIni		.AND.	cDtHrFimTmp	<=	cDtHrFim)		.OR.;
					(cDtHrIni		<=	cDtHrIniTmp	.AND.	cDtHrFim		>=	cDtHrFimTmp)  )
					
				lRetorno := .F.
				Exit
			EndIf
		Next nI
	
		If lRetorno
			lRetorno := ! TxExistAloc( cCodTec,dDtIni, cHrIni, dDtFim , cHrFim, nRecno)
		EndIf
	EndIf
	
	If !lRetorno
		Help("",1,"TA520VLDT",,STR0002,2,0) //"O T�cnico j� possui aloca��o no per�odo escolhido."
	Else
		If cCodEnt == "AAT"
			lRetorno := At510GrAAt(cCodTec,dDtIni, cHrIni, dDtFim , cHrFim)
		EndIf
	EndIf
EndIf

FwRestRows(aSaveLines)
	
RestArea(aArea)
Return(lRetorno)


//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Ta520StAte()
Valida��o do t�cnico a ser alocado
@param ExpC: Codigo do T�cnico a ser validado
@return ExpL:.T. quando sucesso na valida��o
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function Ta520StAte(cCodTec)
Local lRet 		:= .T.

//Verifica se o t�cnico est� bloqueado
lRet := RegistroOk("AA1")

//Verifica se o t�cnico est� disponivel para aloca��o
If lRet .And. !(AA1->AA1_TIPO $ "13" .And. AA1->AA1_ALOCA == "1")
	Help( " ", 1, "AT500VISU" ) // 	Nao e permitida a alteracao deste tecnico
	lRet := .F.
EndIf

Return lRet
