#INCLUDE "PROTHEUS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TECM010.CH"


//------------------------------------------------------------------------------
/*/{Protheus.doc} CHECKINGS

Classe respons�vel por retornar os dados do Atendente

@author	 Matheus Lando Raimundo
@since		29/05/2017
/*/
//------------------------------------------------------------------------------

WSRESTFUL CHECKINGS DESCRIPTION STR0001  //"CheckIn GS"

	WSDATA cAttendant AS STRING
	WSDATA cPswd AS STRING
	WSDATA cDate AS STRING
	WSDATA cBeginDate AS STRING
	WSDATA cEndDate AS STRING
	WSDATA cCodTec AS STRING
	WSDATA cIdStation AS STRING
	WSDATA cInOut AS STRING
	WSDATA cIdSchedule AS STRING
	WSDATA oSelfie AS OBJECT
	WSDATA cComments AS STRING
	WSDATA aImages AS ARRAY
	WSDATA nLatitude AS STRING
	WSDATA nLongitude AS STRING
				
	WSDATA start AS INTEGER
	WSDATA limit AS INTEGER

	WSMETHOD POST  DESCRIPTION STR0002  PATH "attendant" PRODUCES APPLICATION_JSON //"Atendente valido"
	WSMETHOD GET   DESCRIPTION STR0003  PATH "getAppointments" PRODUCES APPLICATION_JSON //"Locais de atendimento"
	WSMETHOD GET getAppointmentsByScale DESCRIPTION STR0004  PATH "getAppointmentsByScale" PRODUCES APPLICATION_JSON //"Locais de atendimento por escala"
	WSMETHOD GET getStationsByDay DESCRIPTION STR0005  PATH "getStationsByDay" PRODUCES APPLICATION_JSON //"Locais por dia"
	WSMETHOD GET getSchedules DESCRIPTION STR0006  PATH "getSchedules" PRODUCES APPLICATION_JSON //"Horarios de um determindo local"
	WSMETHOD GET getHistoricCheckIns DESCRIPTION STR0007  PATH "getHistoricCheckIns" PRODUCES APPLICATION_JSON //"Hist�rico de checkins realizados"
	WSMETHOD POST putCheckIn  DESCRIPTION STR0008  PATH "putCheckIn" PRODUCES APPLICATION_JSON //"Efetiva o checkin do atendente"
	WSMETHOD GET getCheckIn  DESCRIPTION STR0009  PATH "getCheckIn" PRODUCES APPLICATION_JSON //"Dados do proximo Check-in do atendente "
	WSMETHOD POST MailForAttendant  DESCRIPTION STR0021 PATH "MailForAttendant" PRODUCES APPLICATION_JSON //"Envia as marca��es para o Atendente por e-mail"
	

END WSRESTFUL

//-------------------------------------------------------------------
/*/{Protheus.doc} getContratos

Verifica se um atendente � valido

@author Matheus Lando Raimundo
@since 29/05/2017
/*/
//-----------------------------------------------------------------


WSMETHOD POST WSREST CHECKINGS	
Local cTemp			:= GetNextAlias()
Local cResponse		:= '{ "attendant":[], "count": 0 }'	
Local nRecord			:= 0
Local ncount 			:= 0
Local cMessage		:= "Internal Server Error"
Local nStatusCode		:= 500
Local lRet	 			:= .F.
Local cBody				:= ""
Local oOppJson			:= Nil
Local cEmail			:= SuperGetMV("MV_TECAPPM", ,"2") //1-ativo,2-desativado 
 
cBody := Self:GetContent()
		
If !Empty( cBody )
	
	FWJsonDeserialize(cBody,@oOppJson) 
	
	If !Empty( oOppJson )  
		Self:cAttendant := oOppJson:cAttendant
		Self:cPswd := oOppJson:cPswd		
	End
End
				
// Define o tipo de retorno do m�todo
Self:SetContentType("application/json")

If !Empty(Self:cAttendant) .And. !Empty(Self:cPswd) 

	BeginSQL Alias cTemp
		
		SELECT 
			AA1_CODTEC, AA1_NOMTEC
		FROM 	
			%Table:AA1% AA1
		WHERE 
			AA1.AA1_FILIAL = %Exp:xFilial("AA1")% AND
			AA1.AA1_NREDUZ = %Exp:Self:cAttendant% AND
			AA1.AA1_SENHA = %Exp:Self:cPswd% AND
			AA1.%NotDel%
	EndSql
	
	If ( cTemp )->( !Eof() )
	
		lRet := .T.
		
		//-------------------------------------------------------------------
		// Identifica a quantidade de registro no alias tempor�rio.    
		//-------------------------------------------------------------------
		count TO nRecord
		
		//-------------------------------------------------------------------
		// Posiciona no primeiro registro.  
		//-------------------------------------------------------------------	
		( cTemp )->( DBGoTop() )
		
		cResponse	:= ""
		cResponse	+= '{ "attendant":[ ' 
			
			
		Self:cCodTec := ( cTemp )->AA1_CODTEC
		//-------------------------------------------------------------------
		// Incrementa o contador.  
		//-------------------------------------------------------------------	
		ncount++
	
		cResponse += '{"codtec":"' + ( cTemp )->AA1_CODTEC + '",' 
		cResponse += '"nomtec":"' + ( cTemp )->AA1_NOMTEC + '",'
		cResponse += '"cemail":"' + cEmail + '"}'						
		
		cResponse += ' ] } '
		
		nStatusCode	:= 200 
	Else
		nStatusCode	:= 400
		cMessage 		:= STR0010
	EndIf
Else
	nStatusCode	:= 400
	cMessage 		:= STR0011 //"Necessario informar os parametros de usuario e senha do atendente"
EndIf

If lRet
	Self:SetResponse( cResponse )
Else 
	SetRestFault( nStatusCode, EncodeUTF8(cMessage))
EndIf

//cTemp->(dbCloseArea())

Return( lRet ) 


//------------------------------------------------------------------------------
/*/{Protheus.doc} CHECKINGS

@author	 Matheus Lando Raimundo
@since		29/05/2017
/*/
//------------------------------------------------------------------------------
WSMETHOD GET WSRECEIVE cAttendant, cBeginDate, cEndDate WSREST CHECKINGS
Local cIdUserRole		:= ""	
Local cDescription	:= ""
Local cSelected		:= ""
Local cTemp			:= GetNextAlias()
Local cResponse		:= '{ "appointment":[], "count": 0 }'	
Local nRecord			:= ""
Local ncount 			:= 0
Local cMessage		:= "Internal Server Error"
Local nStatusCode		:= 500
Local lRet	 			:= .F.
Local cDtIni			:= ""

Default Self:cAttendant	:= ""

Default Self:cEndDate	:= Self:cBeginDate
 
// Define o tipo de retorno do m�todo	
Self:SetContentType("application/json")

If !Empty(Self:cAttendant) .And. !Empty(Self:cBeginDate) 

	BeginSQL Alias cTemp

		SELECT 
			ABS.ABS_DESCRI, ABB_DTINI, ABB_DTFIM, ABB_HRINI, ABB_HRFIM 
		FROM 
			%Table:AA1% AA1			
			INNER JOIN %Table:ABB% ABB ON ABB.ABB_FILIAL = %Exp:xFilial("ABB")% AND ABB.ABB_CODTEC = AA1.AA1_CODTEC AND AA1.%NotDel%
			INNER JOIN %Table:ABS% ABS ON ABS.ABS_FILIAL = %Exp:xFilial("ABS")% AND ABB.ABB_LOCAL = ABS.ABS_LOCAL AND ABB.%NotDel%
		WHERE 
			AA1.AA1_FILIAL  = %Exp:xFilial("AA1")% AND
			AA1.AA1_NREDUZ  = %Exp:Self:cAttendant% AND
			ABB.ABB_FILIAL = %Exp:xFilial("ABB")% AND			 
			ABB.ABB_CODTEC = AA1.AA1_CODTEC AND
			ABB.ABB_DTINI BETWEEN %Exp:Self:cBeginDate% AND %Exp:Self:cEndDate% AND  			
			ABB.%NotDel% AND
			AA1.%NotDel%
	EndSql
	
	If ( cTemp )->( !Eof() )
		
		lRet := .T.
		
		//-------------------------------------------------------------------
		// Identifica a quantidade de registro no alias tempor�rio.    
		//-------------------------------------------------------------------
		count TO nRecord
		
		//-------------------------------------------------------------------
		// Posiciona no primeiro registro.  
		//-------------------------------------------------------------------	
		( cTemp )->( DBGoTop() )
		
		cResponse	:= ""
		cResponse	+= '{ "appointments":[' 
				
		While ( cTemp )->( !Eof() )
			
			//-------------------------------------------------------------------
			// Incrementa o contador.  
			//-------------------------------------------------------------------									
			cDtIni := ( cTemp )->ABB_DTINI
			cResponse += '{"data" : ' + '"' + cDtIni + '",'							
				
			cResponse += '"stations":['
			While cDtIni == ( cTemp )->ABB_DTINI
				cResponse += '{"station":"' + Alltrim(( cTemp )->ABS_DESCRI) + '",'			 				
				cResponse += '"hrini":"' + ( cTemp )->ABB_HRINI + '",'
				cResponse += '"hrfim":"' + ( cTemp )->ABB_HRFIM + '"}'	
							
				ncount++
					
				( cTemp )->( DBSkip() )
				
				If  cDtIni == ( cTemp )->ABB_DTINI
					cResponse += ','
				Else
					cResponse += ']}'
				EndIf
			End
			
			If ncount < nRecord 
				cResponse += ','
			EndIf								
		End
		
		cResponse += ' ],   '
		cResponse += '"count": ' +cBIStr( nRecord ) + ' } ' 
	Else
		nStatusCode	:= 400
		cMessage 		:= STR0012	 //"Nenhum local encontrado..."
	EndIf
Else
	nStatusCode	:= 400
	cMessage 		:= STR0013 //"Necessario informar os parametros de usuario e data da agenda"
EndIf

If lRet
	Self:SetResponse( cResponse )
Else 
	SetRestFault( nStatusCode, EncodeUTF8(cMessage) )
EndIf

//cTemp->(dbCloseArea())

Return( lRet ) 


//------------------------------------------------------------------------------
/*/{Protheus.doc} CHECKINGS

@author	 Matheus Lando Raimundo
@since		29/05/2017
/*/
//------------------------------------------------------------------------------

WSMETHOD GET getAppointmentsByScale WSRECEIVE cAttendant, cBeginDate, cEndDate WSREST CHECKINGS
Local cIdUserRole		:= ""	
Local cDescription	:= ""
Local cSelected		:= ""
Local cTemp			:= GetNextAlias()
Local cResponse		:= '{ "appointment":[], "count": 0 }'	
Local nRecord			:= 0
Local ncount 			:= 0
Local cMessage		:= "Internal Server Error"
Local nStatusCode		:= 500
Local lRet	 			:= .F.
Local cDtIni			:= ""

Default Self:cAttendant	:= ""

Default Self:cEndDate	:= Self:cBeginDate
 
// Define o tipo de retorno do m�todo	
Self:SetContentType("application/json")

If !Empty(Self:cAttendant) .And. !Empty(Self:cBeginDate)
	lRet := .T. 

	BeginSQL Alias cTemp

		SELECT 
			ABS.ABS_DESCRI, ABB_DTINI, ABB_DTFIM, ABB_HRINI, ABB_HRFIM, 
			CASE 
				WHEN ABB_CHEGOU <> 'S' THEN '2' ELSE '1'
			END ABB_CHEGOU, 
			CASE 
				WHEN ABB_SAIU <> 'S' THEN '2' ELSE '1'
			END ABB_SAIU
		FROM 
			%Table:AA1% AA1			
			INNER JOIN %Table:ABB% ABB ON ABB.ABB_FILIAL = %Exp:xFilial("ABB")% AND ABB.ABB_CODTEC = AA1.AA1_CODTEC AND AA1.%NotDel%
			INNER JOIN %Table:ABS% ABS ON ABS.ABS_FILIAL = %Exp:xFilial("ABS")% AND ABB.ABB_LOCAL = ABS.ABS_LOCAL AND ABB.%NotDel%
		WHERE 
			AA1.AA1_FILIAL  = %Exp:xFilial("AA1")% AND
			AA1.AA1_NREDUZ  = %Exp:Self:cAttendant% AND
			ABB.ABB_FILIAL = %Exp:xFilial("ABB")% AND			 
			ABB.ABB_CODTEC = AA1.AA1_CODTEC AND
			ABB.ABB_DTINI BETWEEN %Exp:Self:cBeginDate% AND %Exp:Self:cEndDate% AND  			
			ABB.%NotDel% AND
			AA1.%NotDel%
			ORDER BY  ABB_DTINI DESC
			
	EndSql
	
	If ( cTemp )->( !Eof() )
		
		
		
		//-------------------------------------------------------------------
		// Identifica a quantidade de registro no alias tempor�rio.    
		//-------------------------------------------------------------------
		count TO nRecord
		
		//-------------------------------------------------------------------
		// Posiciona no primeiro registro.  
		//-------------------------------------------------------------------	
		( cTemp )->( DBGoTop() )
		
		cResponse	:= ""
		cResponse	+= '{ "appointments":[' 
				
		While ( cTemp )->( !Eof() )
			
			//-------------------------------------------------------------------
			// Incrementa o contador.  
			//-------------------------------------------------------------------									
			cDtIni := ( cTemp )->ABB_DTINI
			cResponse += '{"data" : ' + '"' + cDtIni + '",'							
				
			cResponse += '"stations":['
			While cDtIni == ( cTemp )->ABB_DTINI
				cResponse += '{"station":"' + Alltrim(( cTemp )->ABS_DESCRI) + '",'			 				
				cResponse += '"schedule":"' + ( cTemp )->ABB_HRINI + '",'
				cResponse += '"inout":"1",'
				cResponse += '"executed":"' + ( cTemp )->ABB_CHEGOU + '"},'
				
				cResponse += '{"station":"' + Alltrim(( cTemp )->ABS_DESCRI) + '",'			 				
				cResponse += '"schedule":"' + ( cTemp )->ABB_HRFIM  + '",'
				cResponse += '"inout":"2",'
				cResponse += '"executed":"' + ( cTemp )->ABB_SAIU  + '"}'
				
					
							
				ncount++
					
				( cTemp )->( DBSkip() )
				
				If  cDtIni == ( cTemp )->ABB_DTINI
					cResponse += ','
				Else
					cResponse += ']}'
				EndIf
			End
			
			If ncount < nRecord 
				cResponse += ','
			EndIf								
		End
		
		cResponse += ' ],   '
		cResponse += '"count": ' +cBIStr( nRecord * 2 ) + ' } ' 	
	EndIf
Else
	nStatusCode	:= 400
	cMessage 		:= STR0013 //"Necessario informar os parametros de usuario e data da agenda"
EndIf

If lRet
	Self:SetResponse( cResponse )
Else 
	SetRestFault( nStatusCode, EncodeUTF8(cMessage) )
EndIf

//cTemp->(dbCloseArea())

Return( lRet ) 


//------------------------------------------------------------------------------
/*/{Protheus.doc} CHECKINGS

@author	 Matheus Lando Raimundo
@since		29/05/2017
/*/
//------------------------------------------------------------------------------
WSMETHOD GET gethistoricCheckIns WSRECEIVE cAttendant, cBeginDate, cEndDate WSREST CHECKINGS
Local cIdUserRole		:= ""	
Local cDescription	:= ""
Local cSelected		:= ""
Local cTemp			:= GetNextAlias()
Local cResponse		:= '{ "appointment":[], "count": 0 }'	
Local nRecord			:= 0
Local ncount 			:= 0
Local cMessage		:= "Internal Server Error"
Local nStatusCode		:= 500
Local lRet	 			:= .F.
Local cDtIni			:= ""
Local nTot				:= 0

Default Self:cAttendant	:= ""

Default Self:cEndDate	:= Self:cBeginDate
 
// Define o tipo de retorno do m�todo	
Self:SetContentType("application/json")

If !Empty(Self:cAttendant) .And. !Empty(Self:cBeginDate)
	lRet := .T. 

	BeginSQL Alias cTemp

		SELECT 
			ABS.ABS_DESCRI, ABB_DTINI, ABB_DTFIM, ABB_HRINI, ABB_HRFIM, ABB_CHEGOU, ABB_SAIU 
		FROM 
			%Table:AA1% AA1			
			INNER JOIN %Table:ABB% ABB ON ABB.ABB_FILIAL = %Exp:xFilial("ABB")% AND ABB.ABB_CODTEC = AA1.AA1_CODTEC AND AA1.%NotDel%
			INNER JOIN %Table:ABS% ABS ON ABS.ABS_FILIAL = %Exp:xFilial("ABS")% AND ABB.ABB_LOCAL = ABS.ABS_LOCAL AND ABB.%NotDel%
		WHERE 
			AA1.AA1_FILIAL  = %Exp:xFilial("AA1")% AND
			AA1.AA1_NREDUZ  = %Exp:Self:cAttendant% AND
			ABB.ABB_FILIAL = %Exp:xFilial("ABB")% AND			 
			ABB.ABB_CODTEC = AA1.AA1_CODTEC AND
			ABB.ABB_DTINI BETWEEN %Exp:Self:cBeginDate% AND %Exp:Self:cEndDate% AND  			
			(ABB.ABB_CHEGOU = 'S' OR ABB.ABB_SAIU = 'S') AND 
			ABB.%NotDel% AND
			AA1.%NotDel%
		
			ORDER BY  ABB_DTINI DESC
	EndSql
	
	If ( cTemp )->( !Eof() )
		
		
		
		//-------------------------------------------------------------------
		// Identifica a quantidade de registro no alias tempor�rio.    
		//-------------------------------------------------------------------
		count TO nRecord
		
		//-------------------------------------------------------------------
		// Posiciona no primeiro registro.  
		//-------------------------------------------------------------------	
		( cTemp )->( DBGoTop() )
		
		cResponse	:= ""
		cResponse	+= '{ "appointments":[' 
				
		While ( cTemp )->( !Eof() )
			
			//-------------------------------------------------------------------
			// Incrementa o contador.  
			//-------------------------------------------------------------------									
			cDtIni := ( cTemp )->ABB_DTINI
			cResponse += '{"data" : ' + '"' + cDtIni + '",'							
				
			cResponse += '"stations":['
			While cDtIni == ( cTemp )->ABB_DTINI
				If ( cTemp )->ABB_CHEGOU == 'S'
					cResponse += '{"station":"' + Alltrim(( cTemp )->ABS_DESCRI) + '",'			 				
					cResponse += '"schedule":"' + ( cTemp )->ABB_HRINI + '",'
					cResponse += '"inout":"' + '1'+ '"}'
					nTot += 1
					If ( cTemp )->ABB_SAIU == 'S'
						cResponse += ','
					EndIf							
				EndIf
				
					
				If ( cTemp )->ABB_SAIU == 'S'
					cResponse += '{"station":"' + Alltrim(( cTemp )->ABS_DESCRI) + '",'			 				
					cResponse += '"schedule":"' + ( cTemp )->ABB_HRFIM  + '",'
					cResponse += '"inout":"' + '2'+ '"}'
					nTot += 1
				EndIf	
				
					
							
				ncount++
					
				( cTemp )->( DBSkip() )
				
				If  cDtIni == ( cTemp )->ABB_DTINI
					cResponse += ','
				Else
					cResponse += ']}'
				EndIf
			End
			
			If ncount < nRecord 
				cResponse += ','
			EndIf								
		End
		
		cResponse += ' ],   '
		cResponse += '"count": ' +cBIStr( nTot) + ' } ' 	
	EndIf
Else
	nStatusCode	:= 400
	cMessage 		:= STR0013 //"Necessario informar os parametros de usuario e data da agenda"
EndIf

If lRet
	Self:SetResponse( cResponse )
Else 
	SetRestFault( nStatusCode, EncodeUTF8(cMessage) )
EndIf

//cTemp->(dbCloseArea())

Return( lRet ) 



//------------------------------------------------------------------------------
/*/{Protheus.doc} CHECKINGS

@author	 Matheus Lando Raimundo
@since		29/05/2017
/*/
//------------------------------------------------------------------------------
WSMETHOD GET getStationsByDay WSRECEIVE cAttendant, cDate WSREST CHECKINGS
Local cIdUserRole		:= ""	
Local cDescription	:= ""
Local cSelected		:= ""
Local cTemp			:= GetNextAlias()
Local cResponse		:= '{ "stations":[], "count": 0 }'	
Local nRecord			:= 0
Local ncount 			:= 0
Local cMessage		:= "Internal Server Error"
Local nStatusCode		:= 500
Local lRet	 			:= .F.
Local cDtIni			:= ""

Default Self:cAttendant	:= ""

 
// Define o tipo de retorno do m�todo	
Self:SetContentType("application/json")

If !Empty(Self:cAttendant) .And. !Empty(Self:cDate) 

	BeginSQL Alias cTemp

		SELECT 
			DISTINCT ABS.ABS_LOCAL, ABS.ABS_CHFOTO ,ABS.ABS_DESCRI 
		FROM 
			%Table:AA1% AA1			
			INNER JOIN %Table:ABB% ABB ON ABB.ABB_FILIAL = %Exp:xFilial("ABB")% AND ABB.ABB_CODTEC = AA1.AA1_CODTEC AND AA1.%NotDel%
			INNER JOIN %Table:ABS% ABS ON ABS.ABS_FILIAL = %Exp:xFilial("ABS")% AND ABB.ABB_LOCAL = ABS.ABS_LOCAL AND ABB.%NotDel%
		WHERE 
			AA1.AA1_FILIAL  = %Exp:xFilial("AA1")% AND
			AA1.AA1_NREDUZ  = %Exp:Self:cAttendant% AND
			ABB.ABB_FILIAL = %Exp:xFilial("ABB")% AND			 
			ABB.ABB_CODTEC = AA1.AA1_CODTEC AND
			ABB.ABB_DTINI = %Exp:Self:cDate% AND  			
			ABB.%NotDel% AND
			AA1.%NotDel%			
	EndSql
	
	If ( cTemp )->( !Eof() )
		
		lRet := .T.
		
		//-------------------------------------------------------------------
		// Identifica a quantidade de registro no alias tempor�rio.    
		//-------------------------------------------------------------------
		count TO nRecord
		
		//-------------------------------------------------------------------
		// Posiciona no primeiro registro.  
		//-------------------------------------------------------------------	
		( cTemp )->( DBGoTop() )
		
		cResponse	:= ""
		cResponse	+= '{ "stations":[' 
				
		While ( cTemp )->( !Eof() )
			
			//-------------------------------------------------------------------
			// Incrementa o contador.  
			//-------------------------------------------------------------------												
						
				cResponse += '{"id":"' + (( cTemp )->ABS_LOCAL) + '",'
				cResponse += '"requiredPhoto":"' + (IIF(( cTemp )->ABS_CHFOTO == " ", "1", ( cTemp )->ABS_CHFOTO)) + '",'
				cResponse += '"station":"' + Alltrim(( cTemp )->ABS_DESCRI) + '"}'			 				
															
				ncount++
					
				( cTemp )->( DBSkip() )
				
				If ncount < nRecord
					cResponse += ','				
				EndIf
											
		EndDo
		
		cResponse += ' ],   '
		cResponse += '"count": ' +cBIStr( nRecord ) + ' } ' 
	Else
		nStatusCode	:= 400
		cMessage 		:= STR0012	 //"Nenhum local encontrado..."
	EndIf
Else
	nStatusCode	:= 400
	cMessage 		:= STR0013 //"Necessario informar os parametros de usuario e data da agenda"
EndIf

If lRet
	Self:SetResponse( cResponse )
Else 
	SetRestFault( nStatusCode, EncodeUTF8(cMessage) )
EndIf

//cTemp->(dbCloseArea())

Return( lRet ) 


//------------------------------------------------------------------------------
/*/{Protheus.doc} CHECKINGS

@author	 Matheus Lando Raimundo
@since		29/05/2017
/*/
//------------------------------------------------------------------------------
WSMETHOD GET getSchedules WSRECEIVE cAttendant, cDate, cIdStation, cInOut WSREST CHECKINGS
Local cIdUserRole		:= ""	
Local cDescription	:= ""
Local cSelected		:= ""
Local cTemp			:= GetNextAlias()
Local cResponse		:= '{ "schedule":[], "count": 0 }'	
Local nRecord			:= 0
Local ncount 			:= 0
Local cMessage		:= "Internal Server Error"
Local nStatusCode		:= 500
Local lRet	 			:= .F.
Local cDtIni			:= ""
Local cFiltro			:= ""

Default Self:cAttendant	:= ""
 
// Define o tipo de retorno do m�todo	
Self:SetContentType("application/json")

If !Empty(Self:cAttendant) .And. !Empty(Self:cDate) .And. !Empty(Self:cIdStation) .And. !Empty(Self:cInOut) 

	lRet := .T.
	If Self:cInOut == '1'
		cFiltro := "% AND ABB.ABB_CHEGOU <> 'S' %"
	ElseIf Self:cInOut == '2' 
		cFiltro := "% AND ABB.ABB_SAIU <> 'S' %"
	EndIf
	
	 

	BeginSQL Alias cTemp

		SELECT 
		
			ABB_HRINI, ABB_HRFIM, ABB.ABB_CODIGO 
		FROM 
			%Table:AA1% AA1			
			INNER JOIN %Table:ABB% ABB ON ABB.ABB_FILIAL = %Exp:xFilial("ABB")% AND ABB.ABB_CODTEC = AA1.AA1_CODTEC AND AA1.%NotDel%			
		WHERE 
			AA1.AA1_FILIAL  = %Exp:xFilial("AA1")% AND
			AA1.AA1_NREDUZ  = %Exp:Self:cAttendant% AND
			ABB.ABB_FILIAL = %Exp:xFilial("ABB")% AND			 
			ABB.ABB_CODTEC = AA1.AA1_CODTEC AND
			ABB.ABB_DTINI = %Exp:Self:cDate% AND			
			ABB.ABB_LOCAL = %Exp:Self:cIdStation% AND  	  			
			ABB.%NotDel% AND
			AA1.%NotDel%
			%exp:cFiltro%			
	EndSql
	
	If ( cTemp )->( !Eof() )
		
		//lRet := .T.
		
		//-------------------------------------------------------------------
		// Identifica a quantidade de registro no alias tempor�rio.    
		//-------------------------------------------------------------------
		count TO nRecord
		
		//-------------------------------------------------------------------
		// Posiciona no primeiro registro.  
		//-------------------------------------------------------------------	
		( cTemp )->( DBGoTop() )
		
		cResponse	:= ""
		cResponse	+= '{ "schedules":[' 
				
		While ( cTemp )->( !Eof() )
			
			//-------------------------------------------------------------------
			// Incrementa o contador.  
			//-------------------------------------------------------------------												

				If Self:cInOut == '1'
					cResponse += '{"schedule":"' + ((cTemp)->ABB_HRINI) + '",'
				Else	
					cResponse +=  '{"schedule":"' + ((cTemp)->ABB_HRFIM) + '",'
				EndIf	
							 				
				cResponse +=  '"code":"' + ((cTemp)->ABB_CODIGO) + '"}'							 				
															
				ncount++
					
				( cTemp )->( DBSkip() )
				
				If ncount < nRecord
					cResponse += ','				
				EndIf
											
		EndDo
		
		cResponse += ' ],   '
		cResponse += '"count": ' +cBIStr( nRecord ) + ' } ' 	
	EndIf
Else
	nStatusCode	:= 400
	cMessage 		:= STR0014 //"Necessario informar os parametros de atendente, data da agenda, local de trabalho e o tipo (entrada ou sa�da)"
EndIf

If lRet
	Self:SetResponse( cResponse )
Else 
	SetRestFault( nStatusCode, EncodeUTF8(cMessage))
EndIf


//cTemp->(dbCloseArea())

Return( lRet ) 


//------------------------------------------------------------------------------
/*/{Protheus.doc} CHECKINGS

@author	 Matheus Lando Raimundo
@since		29/05/2017
/*/
//------------------------------------------------------------------------------
WSMETHOD POST putCheckIn WSREST CHECKINGS	
Local cTemp				:= GetNextAlias()
Local cResponse			:= '{"status":"ok"}'	
Local nRecord			:= 0
Local ncount 			:= 0
Local cMessage			:= "Internal Server Error"
Local nStatusCode		:= 500
Local lRet	 			:= .F.
Local cBody				:= ""
Local oOppJson			:= Nil
Local lExit 			:= .F.
Local aDist				:= {}
  
cBody := Self:GetContent()
		
If !Empty( cBody )
	
	FWJsonDeserialize(cBody,@oOppJson) 
	
	If !Empty( oOppJson ) 				
		lRet := .T.		
	EndIf 			
End

Self:SetContentType("application/json")

If lRet .And. (Empty(oOppJson:cIdSchedule) .Or. Empty(oOppJson:cInOut))
	lRet := .F.
	nStatusCode := 400
	cMessage := STR0015	 //"Informe o C�digo da ABB e o tipo (entrada ou sa�da)"
EndIf

If lRet
	aDist := TECMABSLtL(oOppJson:cIdSchedule)
	If  aDist[1] > 0 .And. distanciaGPS(oOppJson:nLatitude,oOppJson:nLongitude,aDist[2],aDist[3]) > aDist[1]
		lRet 		:= .F.
		nStatusCode := 400
		cMessage 	:= STR0016 //"Toler�ncia de dist�ncia excedida entre a posi��o atual e o posto de trabalho"
		
	EndIf	
EndIf

If lRet  		
	lRet := TECMProCf(oOppJson)
	If !lRet
		nStatusCode := 400
		cMessage := STR0017  //"N�o foi poss�vel atualizar a agenda do atendente (ABB)"
	EndIf		
EndIf
	
If lRet
	Self:SetResponse( cResponse )
Else 
	SetRestFault( nStatusCode, EncodeUTF8(cMessage))
EndIf	

Return( lRet ) 





//------------------------------------------------------------------------------
/*/{Protheus.doc} CHECKINGS

@author	 Matheus Lando Raimundo
@since		29/05/2017
/*/
//------------------------------------------------------------------------------

WSMETHOD GET getCheckIn WSRECEIVE cAttendant, cDate, cIdStation WSREST CHECKINGS
Local cIdUserRole		:= ""	
Local cDescription	:= ""
Local cSelected		:= ""
Local cTemp			:= GetNextAlias()
Local cResponse		:= '{ "checkin":[], "count": 0 }'	
Local nRecord			:= 0
Local ncount 			:= 0
Local cMessage		:= "Internal Server Error"
Local nStatusCode		:= 500
Local lRet	 			:= .F.
Local cDtIni			:= ""
Local cFiltro			:= ""

Default Self:cAttendant	:= ""
 
// Define o tipo de retorno do m�todo	
Self:SetContentType("application/json")

If !Empty(Self:cAttendant) .And. !Empty(Self:cDate) .And. !Empty(Self:cIdStation) 

	lRet := .T.

	BeginSQL Alias cTemp
			
		SELECT ABB.ABB_CODIGO,
	       CASE
	           WHEN ABB_CHEGOU <> 'S'  THEN ABB_HRINI
	           WHEN ABB_CHEGOU = 'S'
	                AND ABB_SAIU = '' THEN ABB_HRFIM
	       END ABBHR,
	       CASE
	           WHEN ABB_CHEGOU <> 'S' THEN '1'
	           WHEN ABB_CHEGOU = 'S' AND ABB_SAIU = '' THEN '2'
	       END INOUT
		FROM %Table:AA1% AA1
		INNER JOIN %Table:ABB% ABB ON ABB.ABB_FILIAL = %Exp:xFilial("ABB")% AND
		ABB.ABB_CODTEC = AA1.AA1_CODTEC AND 
		AA1.D_E_L_E_T_= ' '
		WHERE AA1.AA1_FILIAL = %Exp:xFilial("AA1")% AND 
	  		AA1.AA1_NREDUZ = %Exp:Self:cAttendant% AND 
	  		ABB.ABB_FILIAL = %Exp:xFilial("ABB")% AND
	  		ABB.ABB_CODTEC = AA1.AA1_CODTEC AND 
	  		ABB.ABB_DTINI = %Exp:Self:cDate%  AND 
	  		ABB.ABB_LOCAL = %Exp:Self:cIdStation%  AND
	  		ABB.ABB_ATIVO = 1 AND 
			ABB.%NotDel%  AND 
			AA1.%NotDel%  AND 
			(ABB.ABB_CHEGOU <> 'S' OR ABB.ABB_SAIU <> 'S') AND
			ABB_HRINI =
				(SELECT MIN(ABBSUB.ABB_HRINI)
			     FROM %Table:AA1% AA1SUB
			     INNER JOIN %Table:ABB% ABBSUB ON ABBSUB.ABB_FILIAL = %Exp:xFilial("ABB")% 
			     AND ABBSUB.ABB_CODTEC = AA1SUB.AA1_CODTEC AND
			     AA1SUB.%NotDel% 
			     WHERE AA1SUB.AA1_FILIAL = %Exp:xFilial("AA1")%  AND
			       AA1SUB.AA1_NREDUZ = %Exp:Self:cAttendant% AND 
			       ABBSUB.ABB_FILIAL = %Exp:xFilial('ABB')%  AND 
			       ABBSUB.ABB_CODTEC = AA1SUB.AA1_CODTEC AND 
			       ABBSUB.ABB_DTINI = %Exp:Self:cDate% AND
			       ABBSUB.ABB_LOCAL = %Exp:Self:cIdStation%  AND
			       ABBSUB.ABB_ATIVO = 1 AND
			       ABBSUB.%NotDel% AND
			       AA1SUB.%NotDel% AND
			       (ABBSUB.ABB_CHEGOU <> 'S'
			            OR ABBSUB.ABB_SAIU <> 'S'))
	EndSql																							
	If ( cTemp )->( !Eof() )
		
	
		
		//-------------------------------------------------------------------
		// Posiciona no primeiro registro.  
		//-------------------------------------------------------------------	
		( cTemp )->( DBGoTop() )
				 													
		cResponse	:= ""
		cResponse	+= '{ "checkin":['		
		cResponse 	+= '{"idschedule":"' + ((cTemp)->ABB_CODIGO) + '",'
		cResponse 	+= '"inout":"' + ((cTemp)->INOUT) + '",'
		cResponse 	+=  '"schedule":"' + ((cTemp)->ABBHR) + '"}'
		cResponse 	+= ' ],'
		cResponse 	+= '"count": 1 }' 	
	EndIf
Else
	nStatusCode	:= 400
	cMessage 		:= STR0018 //"Necessario informar os parametros de atendente, data da agenda, local de trabalho"
EndIf

If lRet
	Self:SetResponse( cResponse )
Else 
	SetRestFault( nStatusCode, cMessage )
EndIf

//cTemp->(dbCloseArea())

Return( lRet ) 

//------------------------------------------------------------------------------
/*/{Protheus.doc} MailForAttendant
localhost:8080/rest/CHECKINGS/MailForAttendant
{ 
  "cAttendant": "1",
  "cBeginDate": "2019/11/05",
  "cEndDate": "2019/11/05"
}
@author	 Luiz Gabriel
@since		05/11/2019
/*/
//------------------------------------------------------------------------------
WSMETHOD POST MailForAttendant WSREST CHECKINGS
Local cMessage		:= "Internal Server Error"
Local nStatusCode	:= 500
Local lRet	 		:= .F.
Local cBody			:= ""
Local oOppJson		:= Nil
Local aResp			:= {}

cBody := Self:GetContent()
		
If !Empty( cBody )
	
	FWJsonDeserialize(cBody,@oOppJson) 
	
	If !Empty( oOppJson ) 				
		lRet := .T.		
	EndIf 			
End

Self:SetContentType("application/json")

If lRet .And. !AttIsMemberOf(oOppJson, "cAttendant")
	lRet := .F.
	nStatusCode := 400
	cMessage := STR0022 //"Expecting <cAttendant> attribute"
EndIf

If lRet .And. !AttIsMemberOf(oOppJson, "cBeginDate")
	lRet := .F.
	nStatusCode := 400
	cMessage := STR0023 //"Expecting <cBeginDate> attribute"
EndIf

If lRet .And. !AttIsMemberOf(oOppJson, "cEndDate")
	lRet := .F.
	nStatusCode := 400
	cMessage := STR0024 //"Expecting <cEndDate> attribute"
EndIf

If lRet .And. Empty(oOppJson:cAttendant)
	lRet := .F.
	nStatusCode := 400
	cMessage := STR0025 //"Informe um valor para o atendente"
EndIf

If lRet .And. (Empty(oOppJson:cBeginDate) .Or. Empty(oOppJson:cEndDate))
	lRet := .F.
	nStatusCode := 400
	cMessage := STR0019 //"Informe uma data inicio ou fim para consulta"
EndIf

If lRet
	aResp := At760RetHtml(oOppJson:cAttendant,oOppJson:cBeginDate,oOppJson:cEndDate,.T.)
	If  Len(aResp) > 0 .And. !aResp[1][1]
		lRet := .F.
		nStatusCode := 400
		cMessage := aResp[1][2]	
	Else
		nStatusCode := 200
		cMessage := STR0020 //"E-mail enviado com sucesso"
	EndIf	
EndIf

cMessage := EncodeUTF8(cMessage)
	
If lRet
	HTTPSetStatus(nStatusCode, cMessage)
Else 
	SetRestFault( nStatusCode,cMessage)
EndIf	

Return( lRet ) 

//------------------------------------------------------------------------------
/*/{Protheus.doc} TECMProCf
Rotina para Confirmar chegadas
@sample 	TECMProCf()
@since		26/08/2016
/*/
//------------------------------------------------------------------------------
Function TECMProCf(oObj)
Local aArea	:= GetArea()
Local lRet := .T.
Local lAtuAgenda := SuperGetMV("MV_GSATAGE",.F.,"1") == "1"
Local cItem		:= ""
Local nI        := 1
Local cTime 	:= Time() 
Local cHour 	:= SubStr( cTime, 1, 2 ) 
Local cMin  	:= SubStr( cTime, 4, 2 ) 

cTime := cHour + ':' + cMin

BEGIN TRANSACTION
	dbSelectArea('ABB')
	
	If !Empty(oObj:cIdSchedule)	
		dbSelectArea("ABB")
		ABB->(dbSetOrder(8))
		If ABB->(DbSeek(XFilial("ABB")+ oObj:cIdSchedule))				
			cItem := GetItemT48(oObj:cIdSchedule)
			RecLock("ABB",.F.)
			
			
			
			If oObj:cInOut == '1'
				ABB->ABB_CHEGOU := 'S'
				//ABB->ABB_SELFIN := oObj:oSelfie:image
				ABB->ABB_OBSIN := DecodeUtf8(oObj:cComments)
				ABB->ABB_LATIN := Alltrim(Str(oObj:nLatitude))
				ABB->ABB_LONIN := Alltrim(Str(oObj:nLongitude))
				ABB->ABB_HRCHIN := cTime
				ABB->(MsUnlock())
				
				RecLock("T48",.T.)
				T48->T48_FILIAL := xFilial('T48')
				T48->T48_CODABB := oObj:cIdSchedule
				T48->T48_ITEM := cItem
				T48->T48_TIPO := '1'
				T48->T48_FOTO := oObj:oSelfie:image
				T48->(MsUnlock())
				
				
				For nI := 1 To Len(oObj:aImages)
					cItem := GetItemT48(oObj:cIdSchedule)
					RecLock("T48",.T.)
					T48->T48_FILIAL := xFilial('T48')
					T48->T48_CODABB := oObj:cIdSchedule
					T48->T48_ITEM := cItem
					T48->T48_TIPO := '3'
					T48->T48_FOTO := oObj:aImages[nI]:image
					T48->(MsUnlock())
				Next nI
				
			ElseIf oObj:cInOut == '2'
				ABB->ABB_SAIU := 'S'
				ABB->ABB_ATENDE := '1'
				ABB->ABB_OBSOUT := DecodeUtf8(oObj:cComments)				
				ABB->ABB_LATOUT := Alltrim(Str(oObj:nLatitude))
				ABB->ABB_LONOUT := Alltrim(Str(oObj:nLongitude))
				ABB->ABB_HRCOUT := cTime
				ABB->(MsUnlock())	
				
				RecLock("T48",.T.)
				T48->T48_FILIAL := xFilial('T48')
				T48->T48_CODABB := oObj:cIdSchedule
				T48->T48_ITEM := cItem
				T48->T48_TIPO := '2'
				T48->T48_FOTO := oObj:oSelfie:image
				T48->(MsUnlock())
				
				For nI := 1 To Len(oObj:aImages)
					cItem := GetItemT48(oObj:cIdSchedule)
					RecLock("T48",.T.)
					T48->T48_FILIAL := xFilial('T48')
					T48->T48_CODABB := oObj:cIdSchedule
					T48->T48_ITEM := cItem
					T48->T48_TIPO := '4'
					T48->T48_FOTO := oObj:aImages[nI]:image
					T48->(MsUnlock())
				Next nI
			EndIf   
		Else
			lRet := .F.	
		EndIf
	Else
		lRet := .F.			
	EndIf

END TRANSACTION	

RestArea(aArea)
Return lRet


//------------------------------------------------------------------------------
/*/{Protheus.doc} CHECKINGS

@author	 Matheus Lando Raimundo
@since		29/05/2017
/*/
//------------------------------------------------------------------------------
Function GetItemT48(cCodAbb)
Local cItem := ""
Local cTemp	:= GetNextAlias()

BeginSQL Alias cTemp
		
	SELECT MAX(T48_ITEM) T48_ITEM FROM %Table:T48% T48
	WHERE T48_FILIAL = %Exp:xFilial("T48")% AND
		T48_CODABB = %Exp:cCodAbb% AND		
		T48.%NotDel%
EndSql

If ( cTemp )->( !Eof() )
	cItem := Soma1(( cTemp )->T48_ITEM)  
Else
	cItem := Soma1(Replicate("0", (TamSx3('T48_ITEM')[1]))) 
EndIf

Return cItem 



//------------------------------------------------------------------------------
/*/{Protheus.doc} distanciaGPS
Calcula a dist�ncia em metros entre duas coordenadas  

@sample     distanciaGPS(cLat1, cLong1, cLat2, cLong2)
@param             cLat1 Latitude da primeira coordenada
@param             cLong1       Longitude da primeira coordenada
@param             cLat2 Latitude da segunda coordenada
@param             cLong2       Longitude da segunda coordenada

@author     guilherme.pimentel
@since             08/08/2017
@version    P12
/*/
//------------------------------------------------------------------------------
Function distanciaGPS(cLat1, cLong1, cLat2, cLong2) 
Local cRaio := 6371
Local nPi := 3.1415
Local nRet := 0
   
cLat1 := cLat1 * nPi / 180
cLong1 := cLong1 * nPi / 180
cLat2 := cLat2 * nPi / 180
cLong2 := cLong2 * nPi / 180
   
cDifLat := cLat2 - cLat1
cDifLong := cLong2 - cLong1
   
nA := sin(cDifLat / 2) * sin(cDifLat / 2) + cos(cLat1) * cos(cLat2) * sin(cDifLong / 2) * sin(cDifLong / 2)
nC := 2 * atn2(sqrt(nA), sqrt(1 - nA)) 

nRet := cRaio * nC * 1000 
nRet := Round(nRet,1)   

Return nRet


//------------------------------------------------------------------------------
/*/{Protheus.doc} TECMABSLtL

@author	 Matheus Lando Raimundo
@since		01/09/2017
@version	12.1.16
/*/
//------------------------------------------------------------------------------
Function TECMABSLtL(cCodABB)
Local aRet := {}
Local aArea := GetArea()
Local cCodLocal	:= ""
Local cCodABS	:= "" 

ABB->(dbSetOrder(8))
If ABB->(DbSeek(XFilial("ABB")+ cCodABB))
	cCodLocal := ABB->ABB_LOCAL 
	TFL->(dbSetOrder(1))
	 	
	ABS->(dbSetOrder(1))
	If ABS->(DbSeek(XFilial("ABS")+ cCodLocal))
		Aadd(aRet,ABS->ABS_METROS)
		Aadd(aRet,Val(ABS->ABS_LATITU))
		Aadd(aRet,Val(ABS->ABS_LONGIT))
	EndIf	
	
EndIf

RestArea(aArea)
Return aRet
