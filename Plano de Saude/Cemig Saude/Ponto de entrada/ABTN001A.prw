#Include 'Protheus.ch'

/*/{Protheus.doc} ABTN001A

Ponto de entrada para incluir bot�es na tela de Protocolo de Reembolso.

@author rafaelalmeida
@since 21/09/2017
@version 1.0
@return aButtons, Array

@see http://tdn.totvs.com.br/display/public/PROT/DT_PE_ABTN001A_PLGRVBOW_PLWEBBOW_PLVLD01A_PLVLWEBA_AOPN001A

/*/
User Function ABTN001A()

Local aButtons := PARAMIXB[1]

Aadd(aButtons, {"Teto",{|| U_FSPLSPBH()},"Consultar Tetos"}) 
AaDd( aButtons, {"beneficiario",{|| U_FSTMKC02()},"Consulta Benefici�rio"}) 

Return aButtons

