#Include 'Protheus.ch'
#Include "FWMVCDEF.CH" 


//-------------------------------------------------------------------------------
/*/{Protheus.doc} FSPLSP63
Rotina para exibir a tela de extrato dos tetos
        
@author 	Leandro de Faria
@since 		01/04/2016
@version 	P12.1.007
@Project	PROJETO_EC_FS007435

@return	Nil
        
Alteracoes Realizadas desde a Estruturacao Inicial 
Data       Programador     		Motivo 
18/01/2017 GUSTAVO BARCELOS		Implementada valida��o de utiliza��o do processo atrav�s dos atalhos do CallCenter.
/*/ 
//-------------------------------------------------------------------------------
User Function FSPLSP63()

Local cPerg 	:= "FSPLSP63"
Local cMsg		:= ""
Local cMatBen := ""
Local aAuxRtn	:= {}
Local aAreas	:= {P30->(GetArea()), GetArea()}	 

Private aTeto		:= {}
Private oBrwTeto	:= Nil
Private aBenef	:= {}
Private cCmpVis	:= ""
Private lGrvEsp	:= .T.
Private oBrwExtr	:= Nil

//Desativa o atalho de teclado para n�o permitir abrir a rotina varias vezes enquanto ela esta aberta
SET KEY VK_F5 TO 

//Mata o aRotina para nao aprecer os botones do FWMBrowser
If (Type("aRotina") == "A")
	aAuxRtn	:= aClone(aRotina)
	aRotina := {}
EndIf

//Perguntas
FAjustSX1(cPerg)

//Valida se processo foi ativado via atalho do CallCenter - GUSTAVO BARCELOS - 18/01/2017
If !(IsInCallStack("TMKA271"))

	If (Pergunte(cPerg,.T.) .And. FValPer(@cMsg))
	
		//Exibe a tela do teto
		FExtTet(MV_PAR01,MV_PAR03,MV_PAR04)
		
	EndIf
	
	If (!Empty(cMsg))
		MsgAlert(cMsg)
	EndIf

Else
	
	cMatBen := BA1->(BA1_CODINT + BA1_CODEMP + BA1_MATRIC + BA1_TIPREG + BA1_DIGITO)
	
	//Exibe a tela do teto
	FExtTet(cMatBen,FirstYDate(dDataBase),LastYDate(dDataBase))
	
	//Chama processo que inclui item de atendimento
	U_FSetItmAtd("999905")
	
EndIf

//Restaura o aRotina
If (Type("aRotina") == "A")
	aRotina := aClone(aAuxRtn)
EndIf	

P30->(dbClearFilter())
AEval(aAreas, {|x| RestArea(x)})

//Reativa a tecla de atalho
SetKey(VK_F5,  {|| U_FSPLSP63()}) //Anexos 

Return Nil


//-------------------------------------------------------------------------------
/*/{Protheus.doc} FAjustSX1
Funcao para criar os parametros 
        
@author 	Leandro de Faria
@since 		01/04/2016
@version 	P12.1.007
@Project	PROJETO_EC_PR14093010

@return	Nil
        
Alteracoes Realizadas desde a Estruturacao Inicial 
Data       Programador     Motivo
05/05/16	Rafael Almeida		Inclus�o da pergunta do nome do benefici�rio. 
/*/ 
//-------------------------------------------------------------------------------
Static Function FAjustSX1(cPerg)

Local aPergs   := {}
Local aHelpPor := {}

aHelpPor:= {}
Aadd( aHelpPor, 'Benefici�rio titular' )
PutSx1(cPerg,"01","Benefici�rio","Benefici�rio","Benefici�rio","mv_ch01","C",TamSx3("BA1_CODINT")[1]+	TamSx3("BA1_CODEMP")[1]+TamSx3("BA1_MATRIC")[1]+TamSx3("BA1_TIPREG")[1]+TamSx3("BA1_DIGITO")[1],0,1,"G","U_PLSP43TRG()","FSEXTE"	,"","","MV_PAR01","","","","","","","","","","","","","","","","",aHelpPor,aHelpPor,aHelpPor)

aHelpPor:= {}
Aadd( aHelpPor, 'Nome do titular' )
PutSx1(cPerg,"02","Nome"	,"Nome","Nome","mv_ch02","C",TamSx3("BA1_NOMUSR")[1]	,0,1,"S","","","","","MV_PAR02","","","","","","","","","","","","","","","","",aHelpPor,aHelpPor,aHelpPor)

aHelpPor:= {}
Aadd( aHelpPor, 'Data Inicial' )
PutSx1(cPerg,"03","Data Inicial","Data Inicial","Data Inicial","mv_ch3","D",8,0,0,"G","NaoVazio()",,"","S","MV_PAR03","","","","","","","","","","","","","","","","",aHelpPor,aHelpPor,aHelpPor)

aHelpPor:= {}
Aadd( aHelpPor, 'Data Final' )
PutSx1(cPerg,"04","Data Final","Data Final","Data Final","mv_ch4","D",8,0,0,"G","NaoVazio()",,"","S","MV_PAR04","","","","","","","","","","","","","","","","",aHelpPor,aHelpPor,aHelpPor)

Return(Nil)


//-------------------------------------------------------------------------------
/*/{Protheus.doc} FValPer
Funcao para validar a tela de pergunta
        
@author 	Leandro de Faria
@since 		01/04/2016
@version 	P12.1.007
@Project	PROJETO_EC_PR14093010

@param		cMsg, string, Mensagem para retorno

@return	lVldPer, logica, Retorna se a tela gera processada
        
Alteracoes Realizadas desde a Estruturacao Inicial 
Data       Programador     Motivo 

/*/ 
//-------------------------------------------------------------------------------
Static Function FValPer(cMsg)

Local lVldPer := .T.
Local cDtIni	:= DToS(MV_PAR03)
Local cDtFim	:= DToS(MV_PAR04)

aBenef	:= U_FSGetTit(SubStr(MV_PAR01,1,TamSx3("BA1_CODINT")[1]+TamSx3("BA1_CODEMP")[1]+TamSx3("BA1_MATRIC")[1]),.F.)

If (SubStr(cDtIni,1,4) <> SubStr(cDtFim,1,4))
	cMsg 	 := "Para exibi��o do extrato � necess�rio que as datas estejam no mesmo ano"
	lVldPer := .F.
EndIf

If (aBenef[1] <> MV_PAR01)
	cMsg 	 := "A exibi��o do extrato ser� permitida apenas para o titular"
	lVldPer := .F.
EndIf

Return (lVldPer)


//-------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menu da tela de cadastro
        
@author 	Leandro de Faria
@since 		06/04/2016
@version 	P12.1.003
@Project	PROJETO_EC_FS007435

@return	Nil
        
Alteracoes Realizadas desde a Estruturacao Inicial 
Data       Programador     Motivo 
/*/ 
//-------------------------------------------------------------------------------
Static Function MenuDef()

Local aRotina := {}

Return (aRotina)


//-------------------------------------------------------------------------------
/*/{Protheus.doc} FExtTet
Funcao para exibir a tela dos teto de acordo com o benefici�rio posicionado
        
@author 	Leandro de Faria
@since 		01/04/2016
@version 	P12.1.007
@Project	PROJETO_EC_FS007435

@param		cBenef, string, 	Beneficiario selecionado
@param		dDtIni, date,		Data inicial
@param		dDtFim, date,		Data final

@return	Nil
        
Alteracoes Realizadas desde a Estruturacao Inicial 
Data       Programador     Motivo 
/*/ 
//-------------------------------------------------------------------------------
Static Function FExtTet(cBenef,dDtIni,dDtFim)

Local aCoors  	:= FWGetDialogSize(oMainWnd)

Local oDlg 		:= Nil
Local oPnlCab		:= Nil
Local oPnlTeto	:= Nil
Local oPnlExtTe	:= Nil
Local oFWLayer	:= Nil
Local oSayMat		:= Nil
Local oGetMat		:= Nil
Local oSayBen		:= Nil
Local oGetBen		:= Nil
Local aButtons	:= {}
Local cTitulo		:= "Extrato Teto"
Local nOpc			:= GD_INSERT+GD_DELETE+GD_UPDATE

SetKey(VK_F2,{|| FPergTeto()})

//Dados do titular
aBenef	:= U_FSGetTit(SubStr(MV_PAR01,1,TamSx3("BA1_CODINT")[1]+TamSx3("BA1_CODEMP")[1]+TamSx3("BA1_MATRIC")[1]),.T.)

//Retorna os tetos do grupo familiar 
MsgRun("Carregando o(s) teto(s)","Aguarde",{|| U_FSSetTeto(1,cBenef,dDtFim,1,dDtIni)})

//Valida se array de tetos possui conte�do - GUSTAVO BARCELOS - 18/01/2017
If len(aTeto) > 0

	DEFINE MSDIALOG oDlg TITLE cTitulo From aCoors[1], aCoors[2] To aCoors[3], aCoors[4] Pixel
	
		//Definicao do layout
		oFWLayer := FWLayer():New()
		oFWLayer:Init( oDlg, .F., .T. )
		
		//Cabecalho
		oFWLayer:AddLine('LINETOP',08,.T.)
		oFWLayer:AddCollumn('ALL',100,.T.,'LINETOP')
		oPnlCab := oFWLayer:GetColPanel( 'ALL','LINETOP')
		
		oSayMat := TSay():New( 011,005,{||"Matricula:"},oPnlCab,,,.F.,.F.,.F.,.T.,,,050,010)
		oGetMat := TGet():New( 009,030,{|u| If(PCount()>0,aBenef[1]:=u,aBenef[1])},oPnlCab,100,008,'',,,,,,,.T.,"",,,.F.,.F.,,.T.,.F.,"","aBenef[1]",,)
		oSayBen := TSay():New( 011,140,{||"Benefici�rio:"},oPnlCab,,,.F.,.F.,.F.,.T.,,,050,010)
		oGetBen := TGet():New( 009,173,{|u| If(PCount()>0,aBenef[2]:=u,aBenef[2])},oPnlCab,230,008,'',,,,,,,.T.,"",,,.F.,.F.,,.T.,.F.,"","aBenef[2]",,)
		
		//Teto
		oFWLayer:AddLine('LINECENTER',30,.T.)
		oFWLayer:AddCollumn('ALL',100,.T.,'LINECENTER')
		oPnlTeto := oFWLayer:GetColPanel( 'ALL','LINECENTER')
		
		oBrwTeto := FWBrowse():New()
		
		oBrwTeto:SetOwner(oPnlTeto)
		oBrwTeto:SetDescription("Tetos") 
		oBrwTeto:SetDataArray()
		oBrwTeto:DisableFilter()
		oBrwTeto:DisableConfig()
		oBrwTeto:DisableReport() 
		oBrwTeto:SetLocate() 
		oBrwTeto:SetArray(aTeto)
		oBrwTeto:SetColumns(GFEC001COL("Cod. Teto",1,,1,2,"oBrwTeto"))
		oBrwTeto:SetColumns(GFEC001COL("Descri��o",2,,1,20,"oBrwTeto"))
		oBrwTeto:SetColumns(GFEC001COL("Vig. Ini.",3,,1,10,"oBrwTeto"))
		oBrwTeto:SetColumns(GFEC001COL("Vig. Fim",4,,1,10,"oBrwTeto"))
		oBrwTeto:SetColumns(GFEC001COL("Matricula",5,,1,15,"oBrwTeto"))
		oBrwTeto:SetColumns(GFEC001COL("Nome",6,,1,30,"oBrwTeto"))
		oBrwTeto:SetColumns(GFEC001COL("Vlr.Inicial",7,,1,20,"oBrwTeto"))
		oBrwTeto:SetColumns(GFEC001COL("Saldo",8,,1,20,"oBrwTeto"))
		oBrwTeto:Activate()
		
		oBrwTeto:bChange := {|| FGetExt(1,MV_PAR01,oBrwTeto:nAT)}
		
		//Extrato do Teto
		oFWLayer:AddLine('LINEITEM',50,.T.)
		oFWLayer:AddCollumn('ALL',100,.T.,'LINEITEM')
		oPnlExtTe := oFWLayer:GetColPanel( 'ALL','LINEITEM')
		
		oBrwExtr:= FWMBrowse():New()
		oBrwExtr:SetOwner(oPnlExtTe)
		oBrwExtr:SetAlias("P30")
		oBrwExtr:SetDescription("Extrato dos Tetos")
		oBrwExtr:SetProfileID('1')
		oBrwExtr:DisableDetails()
		oBrwExtr:SetMenuDef('')	
		oBrwExtr:SetSeek(.F.)
		oBrwExtr:ForceQuitButton()
		oBrwExtr:SetWalkThru(.F.)
		oBrwExtr:SetAmbiente(.F.)
		oBrwExtr:SetUseFilter( .F. )
		oBrwExtr:DisableConfig()
		oBrwExtr:DisableReport()
		
		oBrwExtr:BLDBLCLICK := {|| cCmpVis := "", U_FSPLSC20(1,"Visualiza��o") }
		
		oBrwExtr:Activate()
		
		oBrwExtr:OMENU := Nil
		
		AAdd(aButtons, {'INCLUIR', {|| FInMvTeto()}, "Incluir Teto" , "Incluir Teto"} )
		AAdd(aButtons, {'SELECIONAR', {|| FPergTeto()}, "Selecionar Benefici�rio <f2>" , "Selecionar Benefici�rio <f2>"} )
		
		FGetExt(1,MV_PAR01,oBrwTeto:nAT)
	
	ACTIVATE MSDIALOG oDlg CENTERED ON INIT (EnchoiceBar(oDlg,{|| oDlg:End()}, {||oDlg:End()},,aButtons))

Else
	
	MSGSTOP( "Teto n�o cadastrado para o benefici�rio no per�odo informado.", "Teto n�o cadastrado")
	
EndIf	


Return Nil


//-------------------------------------------------------------------------------
/*/{Protheus.doc} FPergTeto
Funcao para carregar novamente a tela de perguntas 
        
@author 	Leandro de Faria
@since 		08/04/2016
@version 	P12.1.007
@Project	PROJETO_EC_FS007435

@return	Nil
        
Alteracoes Realizadas desde a Estruturacao Inicial 
Data       Programador     Motivo 
/*/ 
//-------------------------------------------------------------------------------
Static Function FPergTeto()

Local cPerg 	:= "FSPLSP63"
Local cMsg		:= ""

SetKey(VK_F2)

If (Pergunte(cPerg,.T.) .And. FValPer(@cMsg))

	//Atualiza a tela de teto novamente
	U_FSSetTeto(1,MV_PAR01,MV_PAR04,1,,MV_PAR03)
	
EndIf

If (!Empty(cMsg))
	MsgAlert(cMsg)
EndIf

SetKey(VK_F2,{|| FPergTeto()})

Return Nil


//-------------------------------------------------------------------------------
/*/{Protheus.doc} FSetTeto
Funcao para carregar os tetos da familia posicionada
        
@author 	Leandro de Faria
@since 		08/04/2016
@version 	P12.1.007
@Project	PROJETO_EC_FS007435

@param		nOpc,		numerico,	1=Filtrar pela matricula	
									2=Filtrar pelo grupo familiar	
@param		cBenef, 	string, 	Beneficiario selecionado
@param		dDtFim, 	date,		Data final
@param		nPosTeto, 	numerico, 	Posicao da matricula que esta no grid do teto

@return	Nil
        
Alteracoes Realizadas desde a Estruturacao Inicial 
Data       Programador     Motivo 
/*/ 
//-------------------------------------------------------------------------------
User Function FSSetTeto(nOpc,cBenef,dDtFim,nPosTeto,dDtIni)

Default nOpc 		:= 1
Default nPosTeto 	:= 1

//Limpa variaveis de controle
aTeto := {}

//Carrega os tetos
FGetTeto(cBenef,dDtFim,dDtIni)

//Valida se array de tetos possui conte�do - GUSTAVO BARCELOS - 18/01/2017
If len(aTeto) > 0
	//Carrega os extratos
	FGetExt(nOpc,cBenef,1)
EndIf

Return Nil


//-------------------------------------------------------------------------------
/*/{Protheus.doc} FGetTeto
Funcao para carregar os tetos da familia posicionada
        
@author 	Leandro de Faria
@since 		04/04/2016
@version 	P12.1.007
@Project	PROJETO_EC_FS007435

@param		cBenef, 	string, 	Beneficiario selecionado
@param		dDtFim, 	date,		Data final

@return	Nil
        
Alteracoes Realizadas desde a Estruturacao Inicial 
Data       Programador     Motivo 
/*/ 
//-------------------------------------------------------------------------------
Static Function FGetTeto(cBenef,dDtFim,dDtIni)

Local nXI		:= 0
Local cCodInt	:= PLSINTPAD()
Local oTeto 	:= Nil
Local aLstBen	:= U_FSGetFam(SubStr(cBenef,1,TamSx3("BA1_CODINT")[1]+TamSx3("BA1_CODEMP")[1]+TamSx3("BA1_MATRIC")[1])) //Beneficiario do grupo familiar
Local aAreas	:= {P28->(GetArea()), GetArea()}

For nXI := 1 To Len(aLstBen)
	
	//Varre a tabela dos teto
	P28->(dbGoTop())
	While (P28->(!Eof()))
		
		//Instancia a classe para carregar os dados do teto 
		oTeto := FsTeto():New(cCodInt,aLstBen[nXI][2],P28->P28_CODIGO,dDtFim)
		
		//Verifica se � familiar
		If (oTeto:lFamilia .And. aLstBen[nXI][2] == cBenef)
			aAdd(aTeto, {	P28->P28_CODIGO,;				//Codigo do teto
							Alltrim(P28->P28_DESTET),;	//Descricao Teto
							oTeto:dDatVigIni,;			//Data Vigencia Inicial
							oTeto:dDatVigFin,;			//Data Vigencia Final
							"",;							//Matricula
							"",;							//Nome
							Transform(Round(oTeto:nVlrTet,2),PesqPict("P30","P30_SALDO")),;				//Valor Inicial do Teto
							Transform(Round(oTeto:Saldo(),2),PesqPict("P30","P30_SALDO"))})		//Saldo
		Else
			
			//Verifica se o benefici�rio tem solitica��es n�o aprovadas.
			oTeto:oBenef:Solicita(/*cProced*/,/*dData*/,/*cTabPad*/,dDtIni,dDtFim,.F.) 
			
			//Verifica se o benefici�rio tem solicita��es aprovadas.
			oTeto:oBenef:Solicita(/*cProced*/,/*dData*/,/*cTabPad*/,dDtIni,dDtFim,.T.)
			
			If !oTeto:oBenef:lSolPgc 
				oTeto:oBenef:lSolPgc := VldForPgc(oTeto:oBenef:cMatric,dDtIni)
			EndIf
			
			//Verifica se o beneficiario tem o PGC
			If (oTeto:lPgc) .And. oTeto:oBenef:lSolPgc 
						 						 
				aAdd(aTeto, {	P28->P28_CODIGO,;				//Codigo do teto
								Alltrim(P28->P28_DESTET),;	//Descricao Teto
								oTeto:dDatVigIni,;			//Data Vigencia Inicial
								oTeto:dDatVigFin,;			//Data Vigencia Final
								aLstBen[nXI][2],;				//Matricula
								aLstBen[nXI][3],;				//Nome
								oTeto:nVlrTet,;				//Valor Inicial do Teto
								Transform(Round(oTeto:Saldo(),2),PesqPict("P30","P30_SALDO"))})		//Saldo
								
								
			EndIf	
			
			//Verifica se o beneficiario tem teto adicional
			If (oTeto:lTetoAd) .And. oTeto:oBenef:lTetoAD 
				aAdd(aTeto, {	P28->P28_CODIGO,;				//Codigo do teto
								Alltrim(P28->P28_DESTET),;	//Descricao Teto
								oTeto:dDatVigIni,;			//Data Vigencia Inicial
								oTeto:dDatVigFin,;			//Data Vigencia Final
								aLstBen[nXI][2],;				//Matricula
								aLstBen[nXI][3],;				//Nome
								oTeto:nVlrTet,;				//Valor Inicial do Teto
								Transform(Round(oTeto:Saldo(),2),PesqPict("P30","P30_SALDO"))})		//Saldo
			EndIf
			
		EndIf 
	
		P28->(dbSkip())
		
	EndDo 	
 
Next nXI

aSort(aTeto,,,{ |Z,W| Z[1] < W[1] })

//Atualiza o Browser
If (Type("oBrwTeto") == "O")
	oBrwTeto:nAT := 1
	oBrwTeto:SetArray(aTeto)
	oBrwTeto:Refresh()
	oBrwTeto:GoTop(.T.)
EndIf	
 
AEval(aAreas, {|x| RestArea(x) })

Return Nil


//-------------------------------------------------------------------------------
/*/{Protheus.doc} FGetExt
Funcao para filtrar a tabela de extratos conforme a matricula do beneficiario
        
@author 	Leandro de Faria
@since 		05/04/2016
@version 	P12.1.007
@Project	PROJETO_EC_FS007435

@param		nOpc,		numerico,	1=Filtrar pela matricula	
									2=Filtrar pelo grupo familiar	
@param		cBenef,	string, 	Beneficiario selecionado
@param		nPosTeto, 	numerico, 	Posicao da matricula que esta no grid do teto

@return	Nil
        
Alteracoes Realizadas desde a Estruturacao Inicial 
Data       	Programador     	Motivo 
01/02/2017	Gustavo Barcelos	Inclu�do tratamento de datas para carregar Extrato de Tetos
/*/ 
//-------------------------------------------------------------------------------
Static Function FGetExt(nOpc,cBenef,nPosTeto)

P30->(dbClearFilter())

//Filtra conforme beneficiario
If (nOpc == 1 .And. nPosTeto <> 1)
	
	//Inclu�do tratamento de Data Inicial + Data Final - GUSTAVO BARCELOS - 01/02/2017
	P30->(dbSetFilter({|| P30_MATRIC == aTeto[nPosTeto][5] .And. P30_CODTET == aTeto[nPosTeto][1] .AND. P30_DATSOL >= aTeto[nPosTeto][3] .AND. P30_DATSOL >= aTeto[nPosTeto][4]},;
							 "@P30_MATRIC = '"+aTeto[nPosTeto][5]+"' AND P30_CODTET = '"+aTeto[nPosTeto][1]+"' AND P30_DATSOL >= '" + DtoS(aTeto[nPosTeto][3]) + "' AND P30_DATSOL <= '" + DtoS(aTeto[nPosTeto][4]) + "'"))
Else
	
	//Inclu�do tratamento de Data Inicial + Data Final - GUSTAVO BARCELOS - 01/02/2017	
	P30->(dbSetFilter({|| P30_CODFAM == SubStr(cBenef,1,TamSx3("BA1_CODINT")[1]+TamSx3("BA1_CODEMP")[1]+TamSx3("BA1_MATRIC")[1]) .And. P30_CODTET == aTeto[nPosTeto][1] .AND. P30_DATSOL >= aTeto[nPosTeto][3] .AND. P30_DATSOL >= aTeto[nPosTeto][4]},;
							 "@P30_CODFAM = '"+SubStr(cBenef,1,TamSx3("BA1_CODINT")[1]+TamSx3("BA1_CODEMP")[1]+TamSx3("BA1_MATRIC")[1])+"' AND P30_CODTET = '"+aTeto[nPosTeto][1]+"' AND P30_DATSOL >= '" + DtoS(aTeto[nPosTeto][3]) + "' AND P30_DATSOL <= '" + DtoS(aTeto[nPosTeto][4]) + "'"))
EndIf

P30->(dbGoTop())
P30->(dbSetOrder(2))

//Atualiza o Browser
If (Type("oBrwExtr") == "O")
	oBrwExtr:Refresh()
	oBrwExtr:GoTop(.T.)
EndIf	

Return Nil


//-------------------------------------------------------------------------------
/*/{Protheus.doc} FInMvTeto
Funcao para incluir a moviemnta��o de teto de forma manual
        
@author 	Leandro de Faria
@since 		05/04/2016
@version 	P12.1.007
@Project	PROJETO_EC_FS007435

@return	Nil
        
Alteracoes Realizadas desde a Estruturacao Inicial 
Data       Programador     Motivo 
/*/ 
//-------------------------------------------------------------------------------
Static Function FInMvTeto()

Local cIdSup	:= ""

Private cNomSup 	:= ""

If( FWAuthSuper(__cUserId))
	
	cIdSup	 := PswId () 
	cNomSup := FWGetUserName(cIdSup)
	
	//Campo para visualziacao
	cCmpVis := "P30_CODTET;P30_DESTET;P30_MATRIC;P30_NOMBEN;P30_VLRCON;P30_DATA;P30_OBS;P30_TIPO"
	
	U_FSPLSC20(3,"Inclus�o")
	
EndIf	

Return Nil

/*/{Protheus.doc} PLSP43TRG

Preenche o nome do benefici�rio na tela de perguntas.

@author rafaelalmeida
@since 05/05/2016
@version 1.0
@return lRet, Retorno l�gico.

/*/
User Function PLSP43TRG()

Local aAreOld := {BA1->(GetArea()),GetArea()}

Local lRet := .T.

BA1->(dbSetOrder(2))
If BA1->(dbSeek(xFilial("BA1") + MV_PAR01))
	MV_PAR02 := BA1->BA1_NOMUSR
EndIf

AEval(aAreOld, {|x| RestArea(x)})

Return lRet

/*/{Protheus.doc} VldForPgc

Valida se o benefici�rio tem movimenta��o no PGC.
Usado para os casos que o grupo de cobertura foi for�ado.

@author rafaelalmeida
@since 19/05/2017
@version 1.0
@param cMatric, character, Matricula
@param dDatIni, data		, Data
@return lRet

/*/
Static Function VldForPgc(cMatric,dDatIni)

Local aAreOld := {GetArea()}
Local cAlsQry	:= GetNextAlias()
Local lRet		:= .t.
Local cDatIni	:= ""
Local cDatFin	:= ""

Default cMatric := ""
Default dDatIni := Date()

cDatIni := DtoS(FirstYDate(dDatIni))
cDatFin := DtoS(LastYDate(dDatIni))

BeginSql Alias cAlsQry

	SELECT 
		P30_CODTET
	FROM
		%table:P30% P30
	WHERE
		P30_FILIAL = %xFilial:P30%
	AND	P30_MATRIC = %Exp:cMatric%
	AND	P30_CODTET = %Exp:'003'%
	AND	P30_DATA BETWEEN %Exp:cDatIni% AND %Exp:cDatFin%
	AND	P30.%notdel%

EndSql

(cAlsQry)->(dbGoTop())
lRet := !(cAlsQry)->(Eof())
(cAlsQry)->(dbCloseArea())

/*P30->(dbSetOrder(1))
lRet := P30->(dbSeek(xFilial("P30")+cMatric+"003")) .And.;
		 P30->P30_DATA >= FirstYDate(dDatIni) .And.;
		 P30->P30_DATA <= LastYDate(dDatIni)*/
		 
AEval(aAreOld, {|x| RestArea(x)})

Return lRet


					 