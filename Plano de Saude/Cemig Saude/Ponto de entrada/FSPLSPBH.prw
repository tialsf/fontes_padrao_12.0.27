#Include 'Protheus.ch'

/*/{Protheus.doc} FSPLSPBH

Fun��o chamada pelo ponto de entrada ABTN001A para apresentar a tela de consulta teto dentro do protocolo de reembolso.

@author rafaelalmeida
@since 22/09/2017
@version 1.0

/*/
User Function FSPLSPBH()

Local cMatric := ""
Local aAreas	:= {P30->(GetArea()), GetArea()}	
Local aRotBkp := aRotina 

Private aTeto		:= {}
Private oBrwTeto	:= Nil
Private aBenef	:= {}
Private cCmpVis	:= ""
Private lGrvEsp	:= .T.
Private oBrwExtr	:= Nil

//Desativa o atalho de teclado para n�o permitir abrir a rotina varias vezes enquanto ela esta aberta
//no protocolo de reembolso
SET KEY VK_F11 TO 

aRotina := {}
If Empty(M->BOW_USUARI) .And. MsgYesNo("Voc� ainda n�o informou o benefici�rio. Deseja continuar?")
	U_FSPLSP63()	
Else
	dbSelectArea("P30")
	cMatric := If(Inclui,M->BOW_USUARI,BOW->BOW_USUARI)
	
	MV_PAR01 := cMatric
	MV_PAR03 := FirstYDate(dDataBase)
	MV_PAR04 := LastYDate(dDataBase)
	
	STATICCALL(FSPLSP63,FExtTet,cMatric,MV_PAR03,MV_PAR04)
	
EndIf

P30->(dbClearFilter())
aRotina := aRotBkp   
AEval(aAreas, {|x| RestArea(x)})

//Reativa a tecla de atalho
SetKey(VK_F11,  {|| U_FSPLSPBH()}) //Anexos

Return Nil

