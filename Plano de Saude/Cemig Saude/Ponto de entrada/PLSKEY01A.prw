#INCLUDE "PROTHEUS.CH"

//------------------------------------------------------------------------------
/*/{Protheus.doc} PLSKEY01A

Inclui atalho de rotina da op��o de menu de dentro do protocolo de reembolso 

@Obs Primeira posi��o corresponde a tecla F11
@Obs Segunda posi��o corresponde a tecla F12
/*/
//------------------------------------------------------------------------------
User Function PLSKEY01A

LOCAL aKeyAnPrt := {}

aKeyAnPrt := {"U_FSPLSPBH()"}

Return aKeyAnPrt