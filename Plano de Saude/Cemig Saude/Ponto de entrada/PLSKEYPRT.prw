#INCLUDE "PROTHEUS.CH"

//------------------------------------------------------------------------------
/*/{Protheus.doc} PLSKEYPRT

Inclui atalho de rotina da op��o de menu de fora do protocolo de reembolso 

@Obs Primeira posi��o corresponde a tecla F5
@Obs Segunda posi��o corresponde a tecla F6
/*/
//------------------------------------------------------------------------------
User Function PLSKEYPRT

LOCAL aKeyProt := {}

aKeyProt := {"U_FSPLSP63()"}

Return aKeyProt