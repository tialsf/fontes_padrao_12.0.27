#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"

class loteGuias 
method New() Constructor

data cTpTran
data cSeqTran
data cDataTran
data cHoraTran
data cNumLote
data cCgcOri
data cCodRDA
data cRegAns
data cCgcDes
data cRegDes
data cVerTiss
data cTipoGuia
data nValTotal //silvia
data nQtdGuias //silvia
data nQtdProcs

endclass

method New() class loteGuias
::cTpTran   := ""
::cSeqTran  := ""
::cDataTran := ""
::cHoraTran := ""
::cNumLote  := ""
::cCgcOri   := ""
::cCodRDA   := ""
::cRegAns   := ""
::cCgcDes   := ""
::cRegDes	:= ""
::cVerTiss  := ""
::cTipoGuia := ""
::nValTotal := 0 //silvia
::nQtdGuias	:= 0 //silvia
::nQtdProcs	:= 0

Return Self
//================================================================
class GConsulta 
method New() Constructor

data cNumGuiPre
data cNumGuiOpe
data cRegAnsCab
data cIndAcid
data cObs
data cDataAtend
data cTpConsult
data oBenef 
data oRda
data oProfExec
data oProced
data oRDASolicitante
data oDadosSolicitacao
data oRDAExecutante
data oDadosAtendimento
data oProcedSADT
data oProfExecSadt
data oProfSolicitante
data cDatAutori
data cSenha
data cDatVldSen
data cNumGuiPri
data oXMLTotais
data oProcedOutDesp
data aProcImp
endclass

method New() class GConsulta
::cNumGuiPre    := ""
::cNumGuiOpe    := ""
::cRegAnsCab	  := ""
::cIndAcid      := ""
::cObs          := ""
::cDataAtend    := ""
::cTpConsult    := ""
::oBenef        := Benef():new()
::oRda          := RDA():new()
::oProfExec     := ProfExec():new()
::oProced       := Procedimento():new()
::oProcedOutDesp:= ProcedOutDesp():new()
::cDatAutori 	:= ""
::cSenha		:= ""
::cDatVldSen	:= ""
::cNumGuiPri	:= ""
::aProcImp		:= {}

// SADT
::oRDASolicitante		:= RDASolicitante():new()
::oDadosSolicitacao	:= DadosSolicitacao():new()
::oRDAExecutante		:= RDAExecutante():new()
::oDadosAtendimento	:= DadosAtendimento():new()
::oProfExecSadt		:= ProfExecSadt():new()
::oProfSolicitante	:= ProfSolicitante():new()
::oXMLTotais			:= XMLTotais():new()

Return Self


//================================================================
class Benef 
method New() Constructor

data cCarteirinha
data cAtendRN
data cNome
data cCNS
data cIndBenef

endclass

method New() class Benef
::cCarteirinha  := ""
::cAtendRN      := ""
::cNome         := ""
::cCNS          := ""
::cIndBenef     := ""

Return Self

//================================================================
class RDA 
method New() Constructor

data cCodRda
data cCgc
data cNome
data cCnes

endclass

method New() class RDA
::cCodRda  := ""
::cCgc     := ""
::cNome    := ""
::cCnes    := ""

Return Self

//================================================================
class ProfExec 
method New() Constructor

data cNome
data cConselho
data cNumCons
data cUF
data cCBOS

endclass

method New() class ProfExec
::cNome     := ""
::cConselho := ""
::cNumCons  := ""
::cUF       := ""
::cCBOS     := ""
Return Self

//================================================================
class Procedimento 
method New() Constructor

data cCodTab
data cCodPro
data nVlrPro
data cDatExec
data cHoraIni
data cHoraFim
data cDescPro
data nQtdExe
data nViaAce
data cTecUti
data nRedAcr
data nVlrTot

endclass

method New() class Procedimento
::cCodTab   := ""
::cCodPro   := ""
::nVlrPro   := 0

::cDatExec 	:= ""
::cHoraIni		:= ""
::cHoraFim 	:= ""
::cCodTab		:= ""	
::cCodPro		:= ""
::cDescPro		:= ""
::nQtdExe		:= 0
::nViaAce		:= 0
::cTecUti		:= ""
::nRedAcr		:= 0
::nVlrTot		:= 0
Return Self




//SADT Exclusivos
//================================================================
class RDASolicitante
method New() Constructor

data cCodRda
data cCgc
data cNome

endclass

method New() class RDASolicitante
::cCodRda  := ""
::cCgc     := ""
::cNome    := ""
Return Self



//================================================================
class ProfSolicitante 
method New() Constructor

data cNome
data cConselho
data cNumCons
data cUF
data cCBOS

endclass

method New() class ProfSolicitante
::cNome     := ""
::cConselho := ""
::cNumCons  := ""
::cUF       := ""
::cCBOS     := ""
Return Self

//================================================================



class DadosSolicitacao
method New() Constructor

data cDataSol
data cCartAtend
data cIndClinica
data cUF
data cCBOS

endclass

method New() class DadosSolicitacao
::cDataSol     	:= ""
::cCartAtend 		:= ""
::cIndClinica  	:= ""
Return Self



//================================================================
class RDAExecutante
method New() Constructor

data cCodRda
data cCgc
data cNome
data cCnes

endclass

method New() class RDAExecutante
::cCodRda  := ""
::cCgc     := ""
::cNome    := ""
::cCnes    := ""
Return Self



//================================================================
class DadosAtendimento
method New() Constructor

data cTipoAtend
data cIndicAcid
data cTipoConsl
data cMotEncerr

endclass

method New() class DadosAtendimento
::cTipoAtend	:= ""
::cIndicAcid	:= ""
::cTipoConsl	:= ""
::cMotEncerr	:= ""
Return Self



//================================================================
class ProfExecSadt
method New() Constructor

data cGrauPart
data cCodProf
data cNome
data cConselho
data cNumCons
data cUF
data cCBOS

endclass

method New() class ProfExecSadt
::cGrauPart	:= ""
::cCodProf		:= ""
::cNome		:= ""
::cConselho	:= ""
::cNumCons		:= ""
::cUF			:= ""
::cCBOS		:= ""

Return Self



//================================================================
class XMLTotais
method New() Constructor

data nVlrProcedimento
data nVlrDiarias
data nVlrTaxAlug
data nVlrMateriais
data nVlrMedicamentos
data nVlrOPME
data nVlrGasesMed
data nVlrTotalGeral

endclass

method New() class XMLTotais
::nVlrProcedimento	:= 0
::nVlrDiarias			:= 0
::nVlrTaxAlug			:= 0
::nVlrMateriais		:= 0
::nVlrMedicamentos	:= 0
::nVlrOPME				:= 0
::nVlrGasesMed		:= 0
::nVlrTotalGeral		:= 0

Return Self



//================================================================
class ProcedOutDesp 
method New() Constructor

data cCodDesp
data cDatExec
data cHoraIni
data cHoraFim
data cCodTab
data cCodPro
data nQtdExe
data cUnMedida
data nRedAcr
data nVlrPro
data nVlrTot
data cDescPro
data cRegAnvisa
data cCodFabric
data cAutoriFunc

endclass

method New() class ProcedOutDesp
::cCodDesp		:= ""
::cDatExec		:= ""
::cHoraIni		:= ""
::cHoraFim		:= ""
::cCodTab		:= ""
::cCodPro		:= ""
::nQtdExe		:= 0
::cUnMedida 	:= ""
::nRedAcr		:= 0
::nVlrPro		:= 0
::nVlrTot		:= 0
::cDescPro		:= ""
::cRegAnvisa	:= ""
::cCodFabric	:= ""
::cAutoriFunc	:= ""
Return Self