#include 'protheus.ch' 
#include 'FWMVCDEF.CH'
//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} PLSMIGCOA

Funcao de importacao do COBERTURA ASSISTENCIAL do PLS para a Central de Obriga��es


@author Roger C
@since 14/11/2017
/*/
//--------------------------------------------------------------------------------------------------
Function PLSMIGCOA(cTrimestre, cAno, cAuto)

	Local lRet	 := .T.
	local aDados := {}
	Local nVez	 := 0
	Local nItem	 := 0
	Local lAuto  := .F.
	Local nQtdDatImp := 0
	Local cErrMsg := ""
	Local aDataImp := {}	

	If !Empty(cAuto) .AND. cAuto == '.T.'
		lAuto := .T.
	Else
		lAuto := .F.
	EndIf

	aDados := PLDCOBASSI(.F.,cTrimestre,cAno, lAuto)

	If aDados[1]

		// Posiciona Operadora
		BA0->(dbSetOrder(1))
		BA0->(dbSeek(xFilial('BA0')+PlsIntPad()))

		// Chama fun��o que informa a Central de Obriga��es que enviaremos o COBERTURA ASSISTENCIAL
		// quadroIniEnvDiops( cQuadro, cCodOpe, cAno, cRefere )
		If qdrPlsIniEnvDiops( '9', BA0->BA0_SUSEP, cAno, cTrimestre, .T. )

			For nVez := 1 to Len( aDados[2] )

				// chamada da funcao de inclus�o do COBERTURA ASSISTENCIAL - a fun��o quadroIniEnvDiops() limpou os registros do periodo, se existiam
				If !Empty(Subs(aDados[2,nVez,1],1,4))
					If IncCobAssi(MODEL_OPERATION_INSERT,aDados[2,nVez])
						nQtdDatImp++
					Else
						aAdd( aErroDIOPS, '9' )	// Variavel Private proveniente do fonte PLSDIOPSPL.prw
						lRet	:= .F.
					EndIf
				Else
					cErrMsg := 'Produto n�o preenchido no quadro Cobertura Assistencial.'
					aAdd( aErroDIOPS, '9' )	// Variavel Private proveniente do fonte PLSDIOPSPL.prw
					lRet	:= .F.

				EndIf					

				// Destruir o objeto
				DelClassIntf()		

			Next

			// Fun��o que informa a Central de Obriga��es que o quadro do COBERTURA ASSISTENCIAL foi enviado.
			// quadroFimEnvDiops( cQuadro, cCodOpe, cAno, cRefere )
			qdrPlsFimEnvDiops( '9', BA0->BA0_SUSEP, cAno, cTrimestre )

		Else
			cErrMsg := 'N�o foi poss�vel inicializar o quadro Cobertura Assistencial.'		
			aAdd( aErroDIOPS, '9' )	// Variavel Private proveniente do fonte PLSDIOPSPL.prw
			lRet	:= .F.
		EndIf
	Else
		cErrMsg := 'N�o foram encontrados dados para exporta��o do quadro Cobertura Assistencial.'
		aAdd( aErroDIOPS, '9' )	// Variavel Private proveniente do fonte PLSDIOPSPL.prw
		lRet	:= .F.
	EndIf

	Aadd(aDataImp,{lRet,nQtdDatImp,cErrMsg})		

Return aDataImp


//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} IncCobAssi

Funcao inclui COBERTURA ASSISTENCIAL no nucleo de informacoes e obrigacoes

@param nOpcMVC	3-Incluir, 4-Alterar

@return lRet	Indica se concluiu .T. ou nao .F. a operacao

@author Roger C
@since 16/11/2017
/*/
//--------------------------------------------------------------------------------------------------
Static Function IncCobAssi(nOpcMVC, aDados)
Local lRet := .F.
Default nOpcMVC	:= MODEL_OPERATION_INSERT
Default aDados	:= {}

If !Empty(aDados)

	oModel:= FWLoadModel( 'PLSMVCCOAS' )
	oModel:SetOperation( nOpcMVC )
	oModel:Activate()
	oModel:SetValue( 'B8IMASTER', 'B8I_FILIAL'	, xFilial('B8I') )
	oModel:SetValue( 'B8IMASTER', 'B8I_CODOPE'	, BA0->BA0_SUSEP )
	oModel:SetValue( 'B8IMASTER', 'B8I_CODOBR'	, "000" )
	oModel:SetValue( 'B8IMASTER', 'B8I_ANOCMP'	, MV_PAR02 )
	oModel:SetValue( 'B8IMASTER', 'B8I_CDCOMP'	, "000" )
	oModel:SetValue( 'B8IMASTER', 'B8I_REFERE'	, StrZero(Val(MV_PAR01),2) )
	oModel:SetValue( 'B8IMASTER', 'B8I_PLANO'	, Subs(aDados[1],1,4) )		
	oModel:SetValue( 'B8IMASTER', 'B8I_ORIGEM'	, Subs(aDados[1],5,1) )
	oModel:SetValue( 'B8IMASTER', 'B8I_CONSUL'	, aDados[2] )
	oModel:SetValue( 'B8IMASTER', 'B8I_EXAMES'	, aDados[3] )
	oModel:SetValue( 'B8IMASTER', 'B8I_TERAPI'	, aDados[4] )
	oModel:SetValue( 'B8IMASTER', 'B8I_INTERN'	, aDados[5] )
	oModel:SetValue( 'B8IMASTER', 'B8I_OUTROS'	, aDados[6] )
	oModel:SetValue( 'B8IMASTER', 'B8I_DEMAIS'	, aDados[7] )
	oModel:SetValue( 'B8IMASTER', 'B8I_STATUS'	, "1" )
			
	If oModel:VldData()
		oModel:CommitData()
		lRet := .T.
	Else
		aErro := oModel:GetErrorMessage()
	EndIf
	
	oModel:DeActivate()
	oModel:Destroy()
	FreeObj(oModel)
	oModel := Nil

EndIf
		
Return lRet
