#Include 'Protheus.ch'
#define REEMBOLSO   "04"

//-------------------------------------------------------------------
/*/{Protheus.doc} PLDCOBASS
Fun��o principal para montagem da DIOPS

@author  Roger C
@version P12
@since   03/02/2017
/*/
//-------------------------------------------------------------------
Function PLDCOBASS()

//Abre pergunte "PLDCOBASS"
/*
Trimestre 	mv_par01
Ano			mv_par02
*/
Local aDados 	:= {}
Local cPerg	:= "PLSDCOBASS"

If !Pergunte(cPerg,.t.)
	Return
Endif

//Chama fun��o para obter os dados
Processa({|| aDados := PLDCOBASSI(.T.,MV_PAR01, MV_PAR02) }, "Aguarde", "Gerando dados...", .t.)

//Chama fun��o para gerar o .CSV
If Len(aDados) > 0
	PLDCOBASSCSV(aDados)
Else
	MsgAlert("N�o foram encontrados dados para gerar o quadro Cobertura Assistencial da DIOPS.")
EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} PLDCOBASSI
Dados da DIOPS da movimenta��o de Cobertura Assitencial

@author  Rodrigo Morgon
@version P12
@since   03/02/2017
@param   aDados, array, dados considerados para gerar o CSV

@return  nil, resultado da fun��o � o .CSV gerado na pasta do server.
/*/
//-------------------------------------------------------------------
function PLDCOBASSI(lDadosCSV,cTrimestre,cAno)
local cQuery		:= ""
local aDados 		:= {}
Local nTotReg		:= 0
Local dDataIni		:= Ctod('')
Local dDataFim		:= Ctod('')
Local cArqTmp		:= GetNextAlias()
Local cPlano		:= ''
Local cOrigem		:= ''
Local nColEvto		:= 0 
Local cTpEvto		:= ''
Local nPos			:= 0
Local nPos2			:= 0

Default lDadosCSV	:= .F.
Default cTrimestre	:= MV_PAR01
Default cAno		:= MV_PAR02

dDataIni	:= CtoD('01/'+StrZero(Val(cTrimestre),2)+'/'+cAno)
dDataFim	:= LastDay(STOD(Alltrim(cAno)+Alltrim(StrZero(Val(cTrimestre)*3,2))+'01'))

// Monta Tempor�rio
cQuery	:= "SELECT SUM(BD7_VLRPAG) AS VALOR, BI3_APOSRG, BI3_NATJCO, BD7_TIPGUI, BD7_OPEUSR, BT5_INTERC, BAU_RECPRO, BD7_TPEVCT  " 
cQuery	+= "FROM " + RetSqlName("BD7") + " BD7 "	
		
//Produto Sa�de
cQuery	+= "INNER JOIN "+RetSqlName("BI3")+" BI3 "
cQuery	+= "ON BI3_FILIAL	= '" + xFilial("BI3") + "' " 
cQuery	+= "AND BI3_CODIGO = BD7.BD7_CODPLA "
cQuery	+= "AND BI3.D_E_L_E_T_ = ' ' "

//Rede de Atendimento
cQuery	+= "INNER JOIN "+RetSqlName("BAU")+" BAU "
cQuery	+= "ON BAU_FILIAL	= '" + xFilial("BAU") + "' " 
cQuery	+= "AND BAU_CODIGO = BD7.BD7_CODRDA "
cQuery	+= "AND BAU.D_E_L_E_T_ = ' ' "

//Contrato
cQuery	+= "LEFT JOIN "+RetSqlName("BT5")+" BT5 "
cQuery	+= "ON BT5_FILIAL	= '" + xFilial("BT5") + "' " 
cQuery	+= "AND BT5_CODINT = BD7.BD7_OPEUSR " 
cQuery	+= "AND BT5_CODIGO = BD7.BD7_CODEMP " 
cQuery	+= "AND BT5_NUMCON = BD7.BD7_CONEMP "
cQuery	+= "AND BT5_VERSAO = BD7.BD7_VERCON "
cQuery	+= "AND BT5.D_E_L_E_T_ = ' ' "

//Rastreamento Cont�bil
cQuery	+= "INNER JOIN "+RetSqlName("CV3")+" CV3 "
cQuery	+= "ON CV3_FILIAL	= '" + xFilial("CV3") + "' "
cQuery	+= "AND CV3.CV3_TABORI = 'BD7' "	
If Upper(TcGetDb()) $ "ORACLE,POSTGRES,DB2,INFORMIX"		
	cQuery	+= "AND NVL(CAST(CV3_RECORI as int),0) = BD7.R_E_C_N_O_  "
Else
	cQuery	+= "AND CONVERT(Int,CV3_RECORI) = BD7.R_E_C_N_O_ "
EndIf
cQuery	+= "AND CV3.D_E_L_E_T_ 	= '' "

//Lan�amentos Cont�beis
cQuery += "INNER JOIN "+RetSqlName("CT2")+" CT2 "
cQuery += "ON CT2_FILIAL 	= '" + xFilial("CT2") + "' "
If Upper(TcGetDb()) $ "ORACLE,POSTGRES,DB2,INFORMIX"		
	cQuery	+= "AND CT2.R_E_C_N_O_ = NVL(CAST(CV3_RECDES as int),0)  "
Else
	cQuery	+= "AND CT2.R_E_C_N_O_ = CONVERT(Int,CV3_RECDES)  "
EndIf		
cQuery	+= "AND CT2.D_E_L_E_T_ = ' ' "
			
cQuery += "WHERE BD7_FILIAL	= '" + xFilial("BD7") + "' "
cQuery	+= "AND BD7.BD7_DTDIGI BETWEEN '" + DtoS(dDataIni) + "' AND '" + DtoS(dDataFim) + "' "	
cQuery	+= "AND BD7_SITUAC = '1' "
cQuery	+= "AND BD7_FASE = '4' " 								
cQuery	+= "AND BD7.D_E_L_E_T_ = ' ' "							

cQuery	+= "GROUP BY BI3_APOSRG, BI3_NATJCO, BD7_TIPGUI, BD7_OPEUSR, BT5_INTERC, BAU_RECPRO, BD7_TPEVCT "
cQuery	+= "ORDER BY BI3_APOSRG, BI3_NATJCO, BD7_TIPGUI, BD7_OPEUSR, BT5_INTERC, BAU_RECPRO, BD7_TPEVCT "

//Garanto que n�o est� aberto
If Select(cArqTmp) > 0
	(cArqTmp)->(dbCloseArea())
EndIf

cQuery	:= ChangeQuery(cQuery)
nHandle := fCreate('QRYDCOAS.SQL', 0)
fWrite(nHandle, CHR(13)+CHR(10)+cQuery+CHR(13)+CHR(10) )
fClose(nHandle)

dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cArqTmp,.T.,.F.)

(cArqTmp)->( DbEval( {|| nTotReg++ } ) )

//Monta r�gua dos meses
ProcRegua(nTotReg) 

If nTotReg > 0 

	(cArqTmp)->(dbGoTop())
	While !(cArqTmp)->(Eof())
		
		IncProc()

		cNatJCo	:= (cArqTmp)->BI3_NATJCO
		/*
		-> TAG: <plano>
		BD7_CODPLA -> BI3_APOSRG (0-ANTES LEI/1-APOS LEI/2-ADAPTADO) BI3_NATJCO (2=Fisica;3=Empresarial;4=Adesao;5=Beneficente)
		""IFAL"" = Carteira de Planos Individuais/Familiares antes da Lei
		""IFPL"" = Carteira de Planos Individuais/Familiares p�s Lei
		""PLAL"" = Planos Coletivos por Ades�o antes da Lei
		""PLAP"" = Planos Coletivos por Ades�o P�s Lei
		""PCEA"" = Planos Coletivos Empresariais antes da Lei
		""PCEL"" = Planos Coletivos Empresariais p�s Lei  "							
		*/
		// Planos N�o Regulamentados
		If (cArqTmp)->BI3_APOSRG == '0'
			Do Case
				// Plano Individual e Familiar
				Case cNatJCo == '2'	
					cPlano	:= 'IFAL'

				// Plano Coletivo Empresarial
				Case cNatJCo == '3'
					cPlano	:= 'PCEA'
				
				// Plano Coletivo por Ades�o	
				Case cNatJCo == '4'
					cPlano	:= 'PLAL'
				
				// Ignora se n�o tem Classifica��o corretamente preenchida
				OtherWise
					cPlano	:= '    '
				
			EndCase

		// Planos Regulamentados
		Else
			Do Case
				// Plano Individual e Familiar
				Case cNatJCo == '2'	
					cPlano	:= 'IFPL'

				// Plano Coletivo Empresarial
				Case cNatJCo == '3'
					cPlano	:= 'PCEL'
				
				// Plano Coletivo por Ades�o	
				Case cNatJCo == '4'
					cPlano	:= 'PLAP'
				
				// Ignora se n�o tem Classifica��o corretamente preenchida
				OtherWise
					cPlano	:= '    '
				
			EndCase
		
		EndIf		
		

		cTpEvto	:= (cArqTmp)->BD7_TPEVCT 
		/*
		Tipo de Evento
		BD7_TPEVCT
		0 = Consulta			=> 01 - Consultas 
		1 = Exames				=> 02/03 - Exames
		2 = Terapias			=> 04/05 - Terapias
		3 = Interna��es			=> 06-11 - Interna��es
		4 = Outros Atendimentos	=> 12 - Outros Atendimento
		5 = Demais Despesas		=> 13 - Demais Despesas							
		???						=> 14 - Odonto							
		*/
		// SER� NECESS�RIO VERIFICAR TRATAMENTO ODONTO COM ALEX QUANDO ELE RETORNAR DE F�RIAS - 15/05/18
		Do Case
			// Consulta
			Case cTpEvto == '01'
				nColEvto := 2
			// Exame
			Case cTpEvto $ '02/03'
				nColEvto := 3
			// Terapias
			Case cTpEvto $ '04/05'
				nColEvto := 4
			// Interna��es
			Case cTpEvto $ '06/07/08/09/10/11'
				nColEvto := 5
			// Outros Atendimentos
			Case cTpEvto == '12'
				nColEvto := 6

			// Demais Despesas
			OtherWise
//			Case cTpEvto == '13'  --> enquanto n�o definir caso do Tipo de Evento = 14
				nColEvto := 7
		EndCase	
			 
		
		cOrigem	:= ''
		/*
		"-> TAG: <origem>
		Tipo de Prestador
		5 = Atendimentos em Corresponsabilidade	=> BD7_OPEUSR <> PlsIntPad() => Somente a partir de 2018							
		2 = Reembolso				=> BD7->BD7_TIPGUI == REEMBOLSO
		3 = Interc�mbio Eventual	=> BT5->BT5_INTERC == '1'
		0 = Rede Pr�pria			=> BAU_RECPRO = '1'
		1 = Rede Contratada			=> BAU_RECPRO = '0'
		4 = Outras Formas Pagamento	=> ELSE?
		*/
		If cAno >= '2018' .and. (cArqTmp)->BD7_OPEUSR <> PlsIntPad()
			// Atendimento em Corresponsabilidade
			cOrigem	:= '5'
		EndIf

		If Empty(cOrigem)
			// Reembolso
			If	(cArqTmp)->BD7_TIPGUI == REEMBOLSO
				cOrigem	:= '1'
			
			// Interc�mbio
			ElseIf (cArqTmp)->BT5_INTERC == '1'
				cOrigem	:= '3'
			
			// Rede Pr�pria
			ElseIf (cArqTmp)->BAU_RECPRO == '1'
				cOrigem	:= '0'
			
			// Rede Contratada
			ElseIf (cArqTmp)->BAU_RECPRO == '0'
				cOrigem	:= '1'
			
			// Outras Formas de Pagamento
			Else
				cOrigem	:= '4'
			
			EndIf
		
		EndIf

		nPos	:= 0
		nPos2	:= 0
		nValor	:= (cArqTmp)->VALOR
		
		// Se j� houver registro, procura para posicionar
		If Len(aDados) > 0
			nPos := Ascan(aDados,{|x| x[1] == cPlano+cOrigem })
			If nPos == 0
				lCria := .T.
			Else
				aDados[nPos,nColEvto] += nValor 				
				lCria := .F.
			EndIf
				
		// Se n�o h� registro, cria diretamente
		Else
			lCria := .T.
		EndIf		

		If lCria .and. nValor > 0
			aAdd( aDados, { cPlano+cOrigem, IIf(nColEvto==2,nValor,0), IIf(nColEvto==3,nValor,0), IIf(nColEvto==4,nValor,0), IIf(nColEvto==5,nValor,0), IIf(nColEvto==6,nValor,0), IIf(nColEvto==7,nValor,0) } )
		EndIf			
		(cArqTmp)->(dbSkip())

	EndDo
			
Else
	(cArqTmp)->(dbCloseArea())
		
EndIf

Return( { (Len(aDados)>0), aDados } ) 
