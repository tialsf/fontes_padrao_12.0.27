#Include 'Protheus.ch'
#include 'FWMVCDEF.CH'

//-------------------------------------------------------------------
/*/{Protheus.doc} PLDMOVPESL
Fun��o principal para montagem da DIOPS

@author  Rodrigo Morgon
@version P12
@since   03/02/2017
/*/
//-------------------------------------------------------------------
Function PLDMOVPESL()

//Abre pergunte "PLDMOVPESL"
/*
cTrimestre 	mv_par01
cAno			mv_par02
*/

local aDados 	:= {}
local cPerg	:= "PLDMOVPESL"      
local AMESESTRIM:={}

if !Pergunte(cPerg,.t.)
	return
endif
cTrimestre:= mv_par01
cAno	  := mv_par02

//Chama fun��o para obter os dados
Processa({|| aDados := PLDMOVPDAD(cTrimestre, cAno , .T.) }, "Aguarde", "Gerando dados...", .t.)

//Chama fun��o para gerar o .CSV
if len(aDados) > 0
	PLDMOVPCSV(aDados, aMesesTrim)
else
	MsgAlert("N�o foram encontrados dados para este quadro da DIOPS.")
endif

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} PLDMOVPDAD
Dados da DIOPS da movimenta��o de PESL

@author  Rodrigo Morgon
@version P12
@since   03/02/2017
@param   aDados, array, dados considerados para gerar o CSV

@return  nil, resultado da fun��o � o .CSV gerado na pasta do server.
/*/
//-------------------------------------------------------------------
Function PLDMOVPDAD(cTrimestre, cAno, lDadosCSV)
Local cQuery		:= ""
Local cSql			:= ""
Local aContas		:= {}
Local aDados 		:= {}
Local aDadosRet		:= {}
Local nI 			:= 1
Local nX 			:= 1
Local nY			:= 1
Local nZ			:= 1
Local nVal			:= 0
Local aDadosTemp	:= {}
Local dDtFim		:= nil
Local nValTotal		:= 0
Local aMesesTrim	:= {}
Local cCtas         := ""
Default lDadosCSV	:= .T.
Default cTrimestre	:= MV_PAR01
Default cAno		:= MV_PAR02 

aMesesTrim := CalcTriMes(cTrimestre, cAno)

//Monta aDados de acordo com o array esperado no CSV
//aDados[ [ano,mes,codigo,valor] , [ano,mes,codigo,valor] , ... , [ano,mes,codigo,valor] ]	

//Monta r�gua dos meses
ProcRegua(len(aMesesTrim)) 

for nI := 1 to len(aMesesTrim)
	
	//Adiciona novo item do m�s para o array de dados
	aadd(aDados, {})
			
	//------------- In�cio 1� quadro ----------------
	// C�digo 0 - Saldo in�cio do m�s
	//-----------------------------------------------
	aadd(aContas,"21111903")
	aadd(aContas,"21112903")
	aadd(aContas,"23111903")
	aadd(aContas,"23112903")
	
	nVal := 0
			
	//Percorre todos os itens das contas e utiliza fun��o do cont�bil para retornar os dados
	for nX := 1 to len(aContas)
		nVal += SaldoConta(aContas[nX],STOD(aMesesTrim[nI][1]+aMesesTrim[nI][2]+"01"),"01","1",1,1)
	next nX
	
	//Adiciona no array principal os valores para o c�digo 0, primeiro quadro
	aadd(aDados[nI], {{0,nVal}})
	//------------- FIM 1� quadro -------------------
	
	//------------- IN�CIO 2� quadro ----------------
	// C�digo 29 a 32 - Total pago no m�s
	//-----------------------------------------------
	//BD7 - Faturado (fase 4) / contabilizado / pago
	// |--- SE2 - Verificar data de baixa
	//Considerar E2_BAIXA como data de baixa (pagamento do t�tulo)
							
	//Fazer a query repetidas vezes para n igual 1 at� que n seja igual a 3
	for nY := 0 to 3		
		//C�lculo para obter m�s e ano corretos			
		if nY > 0
			//Se for da segunda linha em diante
			if cMesAnt == "01"
				//Mes anterior igual a janeiro, diminuo um numero do ano e defino o m�s como dezembro (�ltimo m�s do ano anterior)
				cMesOcorr := AllTrim(STR(Val(cAnoAnt)-1)) + "12"
				cMesAnt := "12"
				cAnoAnt := STR(Val(cAnoAnt)-1)
			else
				//Subtrai um m�s para obter os valores da query
				cMesOcorr := cAnoAnt + AllTrim(STRZERO(Val(cMesAnt)-1,2,0))
				cMesAnt := AllTrim(STRZERO(Val(cMesAnt)-1,2,0))										
			endif
		else
			//Primeira linha do quadro, utiliza os dados de ano e m�s atuais
			cMesAnt := aMesesTrim[nI][2]
			cAnoAnt := aMesesTrim[nI][1]
			cMesOcorr := aMesesTrim[nI][1]+aMesesTrim[nI][2]
		endif
		
		//Buscar a query, agrupar por ano, m�s, where feito com base no aMesesTrim[nI][1] <-- ano e aMesesTrim[nI][2] <-- mes					
		//cQuery	:= "SELECT SUM(CT2_VALOR) AS VALOR " //Soma dos valores para a linha do m�s correspondente
		cQuery	:= "SELECT CT2_VALOR AS VALOR, E2_PREFIXO, E2_NUM, E2_PARCELA, E2_TIPO, E2_FORNECE, E2_LOJA"
		cQuery	+= "FROM " + RetSqlName("BD7") + " BD7 "	
				
		//Rede de Atendimento
		cQuery	+= "INNER JOIN "+RetSqlName("BAU")+" BAU "
		cQuery	+= "ON BAU_FILIAL	= '" + xFilial("BAU") + "' " 
		cQuery	+= "AND BAU_CODIGO = BD7.BD7_CODRDA "
		cQuery	+= "AND BAU.D_E_L_E_T_ = ' ' "
									
		//T�tulos a pagar
		cQuery	+= "INNER JOIN "+RetSqlName("SE2")+" SE2 "
		cQuery	+= "ON  E2_FILIAL 	= '" + xFilial("SE2") + "' "
		cQuery	+= "AND E2_FORNECE	= BAU.BAU_CODSA2 "
		cQuery	+= "AND E2_LOJA		= BAU.BAU_LOJSA2 "
		cQuery	+= "AND E2_ANOBASE	= BD7.BD7_ANOPAG "
		cQuery	+= "AND E2_MESBASE	= BD7.BD7_MESPAG "
		cQuery	+= "AND E2_PLOPELT	= BD7.BD7_OPELOT "
		cQuery	+= "AND E2_PLLOTE	= BD7.BD7_NUMLOT "
		cQuery	+= "AND E2_CODRDA	= BD7.BD7_CODRDA "
		cQuery	+= "AND E2_BAIXA BETWEEN '"+aMesesTrim[nI][1]+aMesesTrim[nI][2]+"01' AND '"+aMesesTrim[nI][1]+aMesesTrim[nI][2]+"31' "
		//J� foi pago e n�o h� saldo
		cQuery	+= "AND E2_SALDO = '0'"	
		cQuery	+= "AND SE2.D_E_L_E_T_ = ' ' "
		
		//Rastreamento Cont�bil
		cQuery	+= "INNER JOIN "+RetSqlName("CV3")+" CV3 "
		cQuery	+= "ON CV3_FILIAL	= '" + xFilial("CV3") + "' "
		cQuery	+= "AND CV3.CV3_TABORI = 'BD7' "	
		If Upper(TcGetDb()) $ "ORACLE,POSTGRES,DB2,INFORMIX"		
			cQuery	+= "AND NVL(CAST(CV3_RECORI as int),0) = BD7.R_E_C_N_O_  "
		Else
			cQuery	+= "AND CONVERT(Int,CV3_RECORI) = BD7.R_E_C_N_O_ "
		EndIf
		
		cQuery	+= "AND CV3.D_E_L_E_T_ 	= '' "
		
		//Lan�amentos Cont�beis
		cQuery += "INNER JOIN "+RetSqlName("CT2")+" CT2 "
		cQuery += "ON CT2_FILIAL 	= '" + xFilial("CT2") + "' "
		If Upper(TcGetDb()) $ "ORACLE,POSTGRES,DB2,INFORMIX"		
			cQuery	+= "AND CT2.R_E_C_N_O_ = NVL(CAST(CV3_RECDES as int),0)  "
		Else
			cQuery	+= "AND CT2.R_E_C_N_O_ = CONVERT(Int,CV3_RECDES)  "
		EndIf		
		cQuery	+= "AND CT2.D_E_L_E_T_ = ' ' "
					
		//----------- Where -----------
		cQuery += "WHERE BD7_FILIAL	= '" + xFilial("BD7") + "' "
			
		//Data de ocorr�ncia do evento. Para a ultima linha, ser�o considerados inclusive todos os itens anteriores da base.
		if nY == 3
			cQuery	+= "AND BD7.BD7_DTDIGI <= '" + cMesOcorr + "31' "	
		else
			cQuery	+= "AND BD7.BD7_DTDIGI BETWEEN '" + cMesOcorr + "01' AND '" + cMesOcorr + "31' "	
		endif
		cQuery	+= "AND BD7_SITUAC = '1' "
		cQuery	+= "AND BD7_FASE = '4' " 								
		cQuery	+= "AND BD7.D_E_L_E_T_ = '' "							
		
		//Garanto que n�o est� aberto
		If Select("TMPMOVPESL") > 0
			TMPMOVPESL->(dbSelectArea("TMPMOVPESL"))
			TMPMOVPESL->(dbCloseArea())
		EndIf
		
		cQuery	:= ChangeQuery(cQuery)
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPMOVPESL",.T.,.F.)
				
		dDtFim := LastDay(STOD(alltrim(aMesesTrim[nI][1])+alltrim(aMesesTrim[nI][2])+'01'))
		While TMPMOVPESL->(!EOF())
			nValTotal += TMPMOVPESL->VALOR
			TMPMOVPESL->(DbSkip())
		End
		//----------------------------------------------------------------------------------------------
		// Percorre t�tulos encontrados que tem v�nculo com a SE2 e CV3
		//    para retirar do valor total baixado os t�tulos de renegocia��o que ainda est�o em aberto
		//----------------------------------------------------------------------------------------------
		//While DEBPESL->(!EOF())
		TMPMOVPESL->(DbGoTop())
		
		While TMPMOVPESL->(!EOF())	
						
			//busco o t�tulo, pois preciso ver em qual per�odo aconteceu o evento e preciso assegurar que ele foi baixado mesmo
			/*if(DEBPESL->CV3_TABORI == "SE5")
				SE5->(dbGoTo(val(DEBPESL->CV3_RECORI)))
				SE2->(msSeek(xFilial("SE2")+SE5->(E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIFOR+E5_LOJA)))
			elseif(DEBPESL->CV3_TABORI == "SE2")
				SE2->(dbGoTo(val(DEBPESL->CV3_RECORI)))
			endIf*/		

			//Com a fun��o saldotit eu asseguro se o titulo est� baixado mesmo, se n�o estiver baixado
			//significa que est� em aberto e eu tenho que manter o saldo da PEL
			//Verifico se o titulo foi negociado, se foi, vou desconsiderar,
			//pq dps vou pegar o debito da parcela que estar� contabilizada				
			//verifico se � um t�tulo de negocia��o para fazer o valor proporcional, aqui � a parcela e n�o o tit origem
			FI8->(dbSetOrder(2)) //FI8_PRFDES+FI8_NUMDES+FI8_PARDES+FI8_TIPDES+FI8_FORDES+FI8_LOJDES
			FI8->(dbGoTop())
			
			if(FI8->(msSeek(xFilial("FI8")+TMPMOVPESL->(E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO+E2_FORNECE+E2_LOJA))))	
				lNeg := .T.
				nVlrTitNeg := SE2->E2_VALOR
				//posiciono no titulo origem
				SE2->(msSeek(xFilial("SE2")+FI8->(FI8_PRFORI+FI8_NUMORI+FI8_PARORI+FI8_TIPORI+FI8_FORORI+FI8_LOJORI)))
			else
				lNeg := .F.
				nVlrTitNeg := 0
			endIf
				
			//verifico se no meu t�tulo eu tenho eventos que foram conhecidos no m�s da linha atual
			//se eu tiver eventos em apenas um per�odo, n�o preciso nem olhar o BD7, s� pegar o CT2_VALOR da
			//conta debito e subtrair do saldo certo
			//mas se eu tiver eventos nos dois per�odos eu preciso subtrair cada um do seu saldo
			
			cSql := " SELECT "
			cSql += " BD7_DTDIGI, "
			cSql += " BD7_VLRAPR,
			cSql += " BD7_VLRPAG "
			cSql += " FROM " + RetSqlName("BD7") 
			cSql += " WHERE "
			cSql += " BD7_FILIAL = '" + XFILIAL('BD7') + "' "
			cSql += " AND BD7_OPELOT = '" + SE2->E2_PLOPELT + "' " 
			cSql += " AND BD7_NUMLOT = '" + SE2->E2_PLLOTE  + "' "
			cSql += " AND BD7_ANOPAG = '" + SE2->E2_ANOBASE + "' "
			cSql += " AND BD7_MESPAG = '" + SE2->E2_MESBASE + "' "
			cSql += " AND D_E_L_E_T_ = ' ' "
			cSql += " ORDER BY BD7_DTDIGI ASC "
			
			cSql := ChangeQuery(cSql)
			
			If Select("EVEPESL") > 0
				EVEPESL->(dbSelectArea("EVEPESL"))
				EVEPESL->(dbCloseArea())
			EndIf
			
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cSql),"EVEPESL",.T.,.F.)
				
			//como est� ordenado por data, se a primeira e a ultima data estiverem no mesmo
			//m�s, sei que n�o preciso percorrer o while da query para subtrair/ratear o valor nos meses anteriores ou posteriores
			if EVEPESL->(!EOF())						
				//Verifico se a data de conhecimento dos eventos est� dentro do m�s da linha, caso contrario, percore cada um deles para verificar o intervalo
				if nY == 3 //Ultima linha						
					if (EVEPESL->BD7_DTDIGI <= cMesOcorr + "31")
						nValTotal -= TMPMOVPESL->CT2_VALOR
					endif							
				elseif (EVEPESL->BD7_DTDIGI >= (cMesOcorr + "01")) .and. (EVEPESL->BD7_DTDIGI <= (cMesOcorr + "31"))							
					EVEPESL->(dbGoBottom())	
					//Verifica se a �ltima linha est� no mesmo intervalo. Se tiver, retiro o valor em aberto do valor do m�s referenciado
					if (EVEPESL->BD7_DTDIGI >= cMesOcorr + "01") .and. (EVEPESL->BD7_DTDIGI <= cMesOcorr + "31")
						nValTotal -= TMPMOVPESL->CT2_VALOR
					endif
				else
					//Se chegar nessa condi��o, significa que possui eventos tanto no m�s referenciado quanto em outros, percorre ent�o todos os itens para								
					While EVEPESL->(!EOF())
						nVlrEvento := iif(EVEPESL->BD7_VLRAPR > 0, EVEPESL->BD7_VLRAPR, EVEPESL->BD7_VLRPAG)
						
						//se for negocia��o, pego o valor proporcional do evento
						if lNeg
							nVlrEvento := (nVlrEvento/SE2->E2_VALOR)*nVlrTitNeg							
						endif
						
						if nY == 3
							if (EVEPESL->BD7_DTDIGI <= cMesOcorr + "31")
								nValTotal -= nVlrEvento
							endif
						elseif (EVEPESL->BD7_DTDIGI >= cMesOcorr + "01") .and. (EVEPESL->BD7_DTDIGI <= cMesOcorr + "31")
							nValTotal -= nVlrEvento
						endif
						
						EVEPESL->(dbSkip())
					EndDo						
				endIf						
			endIf				
							
			TMPMOVPESL->(dbSkip())		
		End
		
		
		
		//Se tiver registro, adiciona no aDadosTemp o codigo correspondente somado com 33 que � o in�cio desse quadro.
		//Ao final do for, ser� 13 + 33 = 46 que � o �ltimo item do quadro.
		aadd(aDadosTemp, {nY + 33, Round(nValTotal,2)})
			
		nValTotal := 0
		
		//DEBPESL->(dbCloseArea())	
		TMPMOVPESL->(dbCloseArea())
	next nY
			
	//Adiciona no aDados os registros obtidos no 3� quadro
	aadd(aDados[nI], aDadosTemp)
	
	aDadosTemp := {}
	
	//------------- FIM 2� quadro -------------------	
	
	//------------- IN�CIO 3� quadro -----------------------------
	// C�digo 33 a 46 - Total dos novos avisos reconhecidos no m�s 
	//------------------------------------------------------------
	//Lista contas a serem buscadas
	//TODO: Desenvolver forma de customizar as contas
	cContas :="'411111011','411111021','411111031','411111041','411111051','411111061','411111017','411111027','411111037',"
	cContas +="'411111047','411111057','411111067','411111081','411121011','411121021','411121031','411121041','411121051',"
	cContas +="'411121061','411121017','411121027','411121037','411121047','411121057','411121067','411121081','411211011',"
	cContas +="'411211021','411211031','411211041','411211051','411211061','411211081','411221011','411221021','411221031',"
	cContas +="'411221041','411221051','411221061','411221081','411311011','411311021','411311031','411311041','411311051',"
	cContas +="'411311061','411311081','411321011','411321021','411321031','411321041','411321051','411321061','411321081',"
	cContas +="'411411011','411411021','411411031','411411041','411411051','411411061','411411017','411411027','411411037',"
	cContas +="'411411047','411411057','411411067','411411081','411421011','411421021','411421031','411421041','411421051',"
	cContas +="'411421061','411421017','411421027','411421037','411421047','411421057','411421067','411421081','411511011',"
	cContas +="'411511021','411511031','411511041','411511051','411511061','411511081','411521011','411521021','411521031',"
	cContas +="'411521041','411521051','411521061','411521081','411711011','411711021','411711031','411711041','411711051',"
	cContas +="'411711061','411711017','411711027','411711037','411711047','411711057','411711067','411721011','411721021',"
	cContas +="'411721031','411721041','411721051','411721061','411721017','411721027','411721037','411721047','411721057',"
	cContas +="'411721067','411911011','411911021','411911031','411911041','411911051','411911061','411911017','411911027',"
	cContas +="'411911037','411911047','411911057','411911067','411911081','411921011','411921021','411921031','411921041',"
	cContas +="'411921051','411921061','411921017','411921027','411921037','411921047','411921057','411921067','411921081'" 
				
	//Fazer a query repetidas vezes para n igual 1 at� que n seja igual a 13
	
	for nY := 0 to 13			
		//C�lculo para obter m�s e ano corretos			
		if nY > 0
			//Se for da segunda linha em diante
			if cMesAnt == "01"
				//Mes anterior igual a janeiro, diminuo um numero do ano e defino o m�s como dezembro (�ltimo m�s do ano anterior)
				cMesOcorr := AllTrim(STR(Val(cAnoAnt)-1)) + "12"
				cMesAnt := "12"
				cAnoAnt := AllTrim(STR(Val(cAnoAnt)-1))
			else
				//Subtrai um m�s para obter os valores da query
				cMesOcorr := cAnoAnt + AllTrim(STRZERO(Val(cMesAnt)-1,2,0))
				cMesAnt := AllTrim(STRZERO(Val(cMesAnt)-1,2,0))
			endif
		else
			//Primeira linha do quadro, utiliza os dados de ano e m�s atuais
			cMesAnt := aMesesTrim[nI][2]
			cAnoAnt := aMesesTrim[nI][1]
			cMesOcorr := aMesesTrim[nI][1]+aMesesTrim[nI][2]
		endif
		
		//Buscar a query, agrupar por ano, m�s, where feito com base no aMesesTrim[nI][1] <-- ano e aMesesTrim[nI][2] <-- mes					
		cQuery	:= "SELECT CASE BD7_VLRAPR WHEN 0 THEN SUM(BD7_VLRBPR) ELSE SUM(BD7_VLRAPR) END AS VALOR " 
		cQuery	+= "FROM " + RetSqlName("BD7") + " BD7 "		
		
		// Rastreamento Cont�bil
		cQuery	+= "INNER JOIN "+RetSqlName("CV3")+" CV3 "
		cQuery	+= "ON CV3_FILIAL = '" + xFilial("CV3") + "' "
		If Upper(TcGetDb()) $ "ORACLE,POSTGRES,DB2,INFORMIX"		
			cQuery	+= "AND NVL(CAST(CV3_RECORI as int),0) = BD7.R_E_C_N_O_  "
		Else
			cQuery	+= "AND CONVERT(Int,CV3_RECORI) = BD7.R_E_C_N_O_ "
		EndIf
		cQuery	+= "AND CV3_TABORI		= 'BD7' "
		cQuery	+= "AND CV3.D_E_L_E_T_	= ' ' "
		
		// Lan�amentos Cont�beis
		cQuery	+= "INNER JOIN "+RetSqlName("CT2")+" CT2 "
		cQuery += "ON CT2_FILIAL = '" + xFilial("CT2") + "' "	
		If Upper(TcGetDb()) $ "ORACLE,POSTGRES,DB2,INFORMIX"		
			cQuery	+= "AND NVL(CAST(CV3_RECDES as int),0)= CT2.R_E_C_N_O_ "
		Else
			cQuery	+= "AND CONVERT(Int,CV3_RECDES) = CT2.R_E_C_N_O_ "
		EndIf
		cQuery += "AND CT2.CT2_DEBITO IN (" + cContas + ") " //Contas d�bito
		cQuery	+= "AND CT2.D_E_L_E_T_	= ' ' "
		
		//----------- WHERE -------------
		cQuery	+= "WHERE BD7_FILIAL = '" + xFilial("BD7") + "' "				
		cQuery += "AND BD7.BD7_DTDIGI BETWEEN '"+aMesesTrim[nI][1]+aMesesTrim[nI][2]+"01' AND '"+aMesesTrim[nI][1]+aMesesTrim[nI][2]+"31' "
		
		if nY == 13 //Data de ocorr�ncia do evento. Para a ultima linha, ser�o considerados inclusive todos os itens anteriores da base.
			cQuery	+= "AND BD7.BD7_DATPRO <= '" + cMesOcorr + "31' "	
		else
			cQuery	+= "AND BD7.BD7_DATPRO BETWEEN '" + cMesOcorr + "01' AND '" + cMesOcorr + "31' "	
		endif
				
		cQuery	+= "AND BD7.D_E_L_E_T_	= ' ' "
		cQuery += "GROUP BY BD7.BD7_VLRBPR, BD7.BD7_VLRAPR"
		
		cQuery	:= ChangeQuery(cQuery)
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPMOVPESL",.T.,.F.)
					
		if !TMPMOVPESL->(EoF())
			//Se tiver registro, adiciona no aDadosTemp o codigo correspondente somado com 33 que � o in�cio desse quadro.
			//Ao final do for, ser� 13 + 33 = 46 que � o �ltimo item do quadro.
			aadd(aDadosTemp, {nY + 33, TMPMOVPESL->VALOR})
		else
			//Se n�o tiver registro, adiciona linha em branco
			aadd(aDadosTemp, {nY + 33, 0}) 
		endif
					
		TMPMOVPESL->(dbCloseArea())
	next nY
			
	//Adiciona no aDados os registros obtidos no 3� quadro
	aadd(aDados[nI], aDadosTemp)
	aDadosTemp := {}
	
	//------------------- FIM 3� quadro ----------------						

	//----------------- In�cio 4� quadro ----------------------------	
	// C�digos 47 a 60 - Total de baixa por glosa reconhecidas no m�s 
	//---------------------------------------------------------------	
	//TODO: Desenvolver forma de customizar as contas
	//Lista contas a serem buscadas
	cContas :="'411111012','411111022','411111032','411111042','411111052','411111062','411111082','411121012','411121022','411121032','411121042','411121052','411121062','411121082','411211012',"
	cContas +="'411211022','411211032','411211042','411211052','411211062','411211082','411221012','411221022','411221032','411221042','411221052','411221062','411221082','411311012','411311022',"
	cContas +="'411311032','411311042','411311052','411311062','411311082','411321012','411321022','411321032','411321042','411321052','411321062','411321082','411411012','411411022','411411032',"
	cContas +="'411411042','411411052','411411062','411411082','411421012','411421022','411421032','411421042','411421052','411421062','411421082','411511012','411511022','411511032','411511042',"
	cContas +="'411511052','411511062','411511082','411521012','411521022','411521032','411521042','411521052','411521062','411521082','411711012','411711022','411711032','411711042','411711052',"
	cContas +="'411711062','411721012','411721022','411721032','411721042','411721052','411721062','411911012','411911022','411911032','411911042','411911052','411911062','411911082','411921012',"
	cContas +="'411921022','411921032','411921042','411921052','411921062','411921082'"

	cMesAnt 	:= ""
	cAnoAnt 	:= ""
	cMesOcorr 	:= ""
	
	//Fazer a query repetidas vezes para n igual 1 at� que n seja igual a 13
	
	for nY := 0 to 13
		//C�lculo para obter m�s e ano corretos			
		if nY > 0
			//Se for da segunda linha em diante
			if cMesAnt == "01"
				//Mes anterior igual a janeiro, diminuo um numero do ano e defino o m�s como dezembro (�ltimo m�s do ano anterior)
				cMesOcorr := AllTrim(STR(Val(cAnoAnt)-1)) + "12"
				cMesAnt := "12"
				cAnoAnt := AllTrim(STR(Val(cAnoAnt)-1))
			else
				//Subtrai um m�s para obter os valores da query
				cMesOcorr := cAnoAnt + AllTrim(STRZERO(Val(cMesAnt)-1,2,0))
				cMesAnt := AllTrim(STRZERO(Val(cMesAnt)-1,2,0))
			endif
		else
			//Primeira linha do quadro, utiliza os dados de ano e m�s atuais
			cMesAnt := aMesesTrim[nI][2]
			cAnoAnt := aMesesTrim[nI][1]
			cMesOcorr := aMesesTrim[nI][1]+aMesesTrim[nI][2]
		endif
		
		//Buscar a query, agrupar por ano, m�s, where feito com base no aMesesTrim[nI][1] <-- ano e aMesesTrim[nI][2] <-- mes	
					
		cQuery	:= "SELECT BD7_VLRGLO AS VALOR " 
		cQuery	+= "FROM " + RetSqlName("BD7") + " BD7 "		
		
		// Rastreamento Cont�bil
		cQuery	+= "INNER JOIN "+RetSqlName("CV3")+" CV3 "
		cQuery	+= "ON CV3_FILIAL = '" + xFilial("CV3") + "' "
		If Upper(TcGetDb()) $ "ORACLE,POSTGRES,DB2,INFORMIX"		
			cQuery	+= "AND NVL(CAST(CV3_RECORI as int),0) = BD7.R_E_C_N_O_  "
		Else
			cQuery	+= "AND CONVERT(Int,CV3_RECORI) = BD7.R_E_C_N_O_ "
		EndIf
		cQuery	+= "AND CV3_TABORI 		= 'BD7' "
		cQuery	+= "AND CV3.D_E_L_E_T_ 	= ' ' "
		
		// Lan�amentos Cont�beis
		cQuery	+= "INNER JOIN "+RetSqlName("CT2")+" CT2 "
		cQuery += "ON	CT2_FILIAL = '" + xFilial("CT2") + "' "
		If Upper(TcGetDb()) $ "ORACLE,POSTGRES,DB2,INFORMIX"		
			cQuery	+= "AND NVL(CAST(CV3_RECDES as int),0)= CT2.R_E_C_N_O_ "
		Else
			cQuery	+= "AND CONVERT(Int,CV3_RECDES) = CT2.R_E_C_N_O_ "
		EndIf
		cQuery += "AND	CT2.CT2_DATA BETWEEN '"+aMesesTrim[nI][1]+aMesesTrim[nI][2]+"01' AND '"+aMesesTrim[nI][1]+aMesesTrim[nI][2]+"31' "
		cQuery += "AND CT2.CT2_DEBITO IN (" + cContas + ") "
		cQuery += "AND CT2.D_E_L_E_T_ 	= ' ' "
		
		cQuery += "WHERE BD7_FILIAL = '" + xFilial("BD7") + "' "
								
		if nY == 13 //Data de ocorr�ncia. Para a ultima linha, ser�o considerados inclusive todos os itens anteriores da base.
			cQuery	+= "AND BD7.BD7_DATPRO <= '" + cMesOcorr + "31' "	
		else
			cQuery	+= "AND BD7.BD7_DATPRO BETWEEN '" + cMesOcorr + "01' AND '" + cMesOcorr + "31' "	
		endif		
				
		cQuery	+= "AND BD7.BD7_FASE IN ('3','4') "
		cQuery	+= "AND BD7.D_E_L_E_T_ 	= ' ' "
		
		cQuery	:= ChangeQuery(cQuery)
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPMOVPESL",.T.,.F.)
					
		if !TMPMOVPESL->(EoF())
			//Se tiver registro, adiciona no aDadosTemp o codigo correspondente somado com 33 que � o in�cio desse quadro.
			//Ao final do for, ser� 13 + 47 = 60 que � o �ltimo item do quadro.
			aadd(aDadosTemp, {nY + 47, TMPMOVPESL->VALOR})
		else
			//Se n�o tiver registro, adiciona linha em branco
			aadd(aDadosTemp, {nY + 47, 0}) 
		endif
		
		TMPMOVPESL->(dbCloseArea())
	next nY
	
	//Adiciona no aDados os registros obtidos no 4� quadro
	aadd(aDados[nI], aDadosTemp)
	aDadosTemp := {}
	
	//------------------- FIM 4� quadro ----------------
	
	//----------------- In�cio 5� quadro ---------------
	// Codigos 61, 62, 63 e 64
	//--------------------------------------------------
	
	//-------------------- Codigo 61 -------------------------	
	nVal61 := SaldoConta('411112',STOD(aMesesTrim[nI][1]+aMesesTrim[nI][2]+"31"),"01","1",1,1)
	
	//Adiciona no array principal os valores para o c�digo 61, primeiro quadro
	aadd(aDados[nI], {{61,nVal61}})
	//--------------------------------------------------------
			
	//-------------------- Codigo 62 -------------------------
	aContas := {}
	nVal := 0
	
	aadd(aContas,"21111903")
	aadd(aContas,"21112903")
	aadd(aContas,"23111903")
	aadd(aContas,"23112903")
			
	//Percorre todos os itens das contas e utiliza fun��o do cont�bil para retornar os dados
	for nX := 1 to len(aContas)
		nVal += SaldoConta(aContas[nX],STOD(aMesesTrim[nI][1]+aMesesTrim[nI][2]+"31"),"01","1",1,1)
	next nX
	
	//Adiciona no array principal os valores para o c�digo 62, primeiro quadro
	aadd(aDados[nI], {{62,nVal}})
	
	//--------------------------------------------------------
	
	//-------------------- Codigo 63 -------------------------
	//Percorre todos os valores de todas as contas no m�s com as contas j� existentes antes, tira o valor do 61
	aContas := {}
	nVal := 0	
		
	cCtas :="'411111011'/'411111021'/'411111031'/'411111041'/'411111051'/'411111061'/'411111017'/'411111027'/'411111037'/'411111047'/'411111057'/'411111067'/'411111081'/'411121011'/'411121021'/'411121031'/'411121041'/'411121051'/"
	cCtas +="'411121061'/'411121017'/'411121027'/'411121037'/'411121047'/'411121057'/'411121067'/'411121081'/'411211011'/'411211021'/'411211031'/'411211041'/'411211051'/'411211061'/'411211081'/'411221011'/'411221021'/'411221031'/"
	cCtas +="'411221041'/'411221051'/'411221061'/'411221081'/'411311011'/'411311021'/'411311031'/'411311041'/'411311051'/'411311061'/'411311081'/'411321011'/'411321021'/'411321031'/'411321041'/'411321051'/'411321061'/'411321081'/"
	cCtas +="'411411011'/'411411021'/'411411031'/'411411041'/'411411051'/'411411061'/'411411017'/'411411027'/'411411037'/'411411047'/'411411057'/'411411067'/'411411081'/'411421011'/'411421021'/'411421031'/'411421041'/'411421051'/"
	cCtas +="'411421061'/'411421017'/'411421027'/'411421037'/'411421047'/'411421057'/'411421067'/'411421081'/'411511011'/'411511021'/'411511031'/'411511041'/'411511051'/'411511061'/'411511081'/'411521011'/'411521021'/'411521031'/"
	cCtas +="'411521041'/'411521051'/'411521061'/'411521081'/'411711011'/'411711021'/'411711031'/'411711041'/'411711051'/'411711061'/'411711017'/'411711027'/'411711037'/'411711047'/'411711057'/'411711067'/'411721011'/'411721021'/"
	cCtas +="'411721031'/'411721041'/'411721051'/'411721061'/'411721017'/'411721027'/'411721037'/'411721047'/'411721057'/'411721067'/'411911011'/'411911021'/'411911031'/'411911041'/'411911051'/'411911061'/'411911017'/'411911027'/"
	cCtas +="'411911037'/'411911047'/'411911057'/'411911067'/'411911081'/'411921011'/'411921021'/'411921031'/'411921041'/'411921051'/'411921061'/'411921017'/'411921027'/'411921037'/'411921047'/'411921057'/'411921067'/'411921081'" 
	
	aContas:=StrTokArr(cCtas, "'/'")
			
	//Percorre todos os itens das contas e utiliza fun��o do cont�bil para retornar os dados
	for nX := 1 to len(aContas)
		nVal += SaldoConta(aContas[nX],STOD(aMesesTrim[nI][1]+aMesesTrim[nI][2]+"31"),"01","1",1,1)
	next nX		
	
	//Adiciona no array principal os valores para o c�digo 63, primeiro quadro
	aadd(aDados[nI], {{63,nVal-nVal61}})
	
	//--------------------------------------------------------
	
	//-------------------- Codigo 64 -------------------------
	//Percorre todos os valores de todas as contas no m�s com as contas j� existentes antes, tira o valor do 61
	aContas := {}
	nVal := 0		
	cCtas :="'411111012'/'411111022'/'411111032'/'411111042'/'411111052'/'411111062'/'411111082'/'411121012'/'411121022'/'411121032'/'411121042'/'411121052'/'411121062'/'411121082'/'411211012'/"
	cCtas +="'411211022'/'411211032'/'411211042'/'411211052'/'411211062'/'411211082'/'411221012'/'411221022'/'411221032'/'411221042'/'411221052'/'411221062'/'411221082'/'411311012'/'411311022'/"
	cCtas +="'411311032'/'411311042'/'411311052'/'411311062'/'411311082'/'411321012'/'411321022'/'411321032'/'411321042'/'411321052'/'411321062'/'411321082'/'411411012'/'411411022'/'411411032'/"
	cCtas +="'411411042'/'411411052'/'411411062'/'411411082'/'411421012'/'411421022'/'411421032'/'411421042'/'411421052'/'411421062'/'411421082'/'411511012'/'411511022'/'411511032'/'411511042'/"
	cCtas +="'411511052'/'411511062'/'411511082'/'411521012'/'411521022'/'411521032'/'411521042'/'411521052'/'411521062'/'411521082'/'411711012'/'411711022'/'411711032'/'411711042'/'411711052'/"
	cCtas +="'411711062'/'411721012'/'411721022'/'411721032'/'411721042'/'411721052'/'411721062'/'411911012'/'411911022'/'411911032'/'411911042'/'411911052'/'411911062'/'411911082'/'411921012'/"
	cCtas +="'411921022'/'411921032'/'411921042'/'411921052'/'411921062'/'411921082'"
	
	aContas:=StrTokArr(cCtas, "'/'")
			
	//Percorre todos os itens das contas e utiliza fun��o do cont�bil para retornar os dados
	for nX := 1 to len(aContas)
		nVal += SaldoConta(aContas[nX],STOD(aMesesTrim[nI][1]+aMesesTrim[nI][2]+"31"),"01","1",1,1)
	next nX		
	
	//Adiciona no array principal os valores para o c�digo 64, primeiro quadro
	aadd(aDados[nI], {{64,nVal-nVal61}})
	
	//--------------------------------------------------------
	
	//------------------- FIM 5� quadro ----------------
					
	//----------------- In�cio 6� quadro -------------------------
	// C�digos 65 a 78 - Total de recupera��es reconhecidas no m�s
	//------------------------------------------------------------		
	//Lista contas a serem buscadas
	//TODO: Desenvolver forma de customizar as contas
	cContas :="'411111013','411111023','411111033','411111043','411111053','411111063','411111019','411111029','411111039','411111049','411111059','411111069','411121013','411121023','411121033',"
	cContas +="'411121043','411121053','411121063','411121019','411121029','411121039','411121049','411121059','411121069','411211013','411211023','411211033','411211043','411211053','411211063',"
	cContas +="'411211019','411211029','411211039','411211049','411211059','411211069','411221013','411221023','411221033','411221043','411221053','411221063','411221019','411221029','411221039',"
	cContas +="'411221049','411221059','411221069','411311013','411311023','411311033','411311043','411311053','411311063','411311019','411311029','411311039','411311049','411311059','411311069',"
	cContas +="'411321013','411321023','411321033','411321043','411321053','411321063','411321019','411321029','411321039','411321049','411321059','411321069','411411013','411411023','411411033',"
	cContas +="'411411043','411411053','411411063','411411019','411411029','411411039','411411049','411411059','411411069','411421013','411421023','411421033','411421043','411421053','411421063',"
	cContas +="'411421019','411421029','411421039','411421049','411421059','411421069','411511013','411511023','411511033','411511043','411511053','411511063','411511019','411511029','411511039',"
	cContas +="'411511049','411511059','411511069','411521013','411521023','411521033','411521043','411521053','411521063','411521019','411521029','411521039','411521049','411521059','411521069',"
	cContas +="'411711013','411711023','411711033','411711043','411711053','411711063','411711019','411711029','411711039','411711049','411711059','411711069','411721013','411721023','411721033',"
	cContas +="'411721043','411721053','411721063','411721019','411721029','411721039','411721049','411721059','411721069','411911013','411911023','411911033','411911043','411911053','411911063',"
	cContas +="'411911019','411911029','411911039','411911049','411911059','411911069','411921013','411921023','411921033','411921043','411921053','411921063','411921019','411921029','411921039',"
	cContas +="'411921049','411921059','411921069'"

	cMesAnt 	:= ""
	cAnoAnt 	:= ""
	cMesOcorr 	:= ""	
	//Fazer a query repetidas vezes para n igual 1 at� que n seja igual a 13
	for nY := 0 to 13
		//C�lculo para obter m�s e ano corretos			
		if nY > 0
			//Se for da segunda linha em diante
			if cMesAnt == "01"
				//Mes anterior igual a janeiro, diminuo um numero do ano e defino o m�s como dezembro (�ltimo m�s do ano anterior)
				cMesOcorr := AllTrim(STR(Val(cAnoAnt)-1)) + "12"
				cMesAnt := "12"
				cAnoAnt := AllTrim(STR(Val(cAnoAnt)-1))
			else
				//Subtrai um m�s para obter os valores da query
				cMesOcorr := cAnoAnt + AllTrim(STRZERO(Val(cMesAnt)-1,2,0))
				cMesAnt := AllTrim(STRZERO(Val(cMesAnt)-1,2,0))
			endif
		else
			//Primeira linha do quadro, utiliza os dados de ano e m�s atuais
			cMesAnt := aMesesTrim[nI][2]
			cAnoAnt := aMesesTrim[nI][1]
			cMesOcorr := aMesesTrim[nI][1]+aMesesTrim[nI][2]
		endif
		
		//Buscar a query, agrupar por ano, m�s, where feito com base no aMesesTrim[nI][1] <-- ano e aMesesTrim[nI][2] <-- mes	
					
		cQuery	:= "SELECT BD7_VLRGLO AS VALOR " 
		cQuery	+= "FROM " + RetSqlName("BD7") + " BD7 "		
		
		// Rastreamento Cont�bil
		cQuery	+= "INNER JOIN "+RetSqlName("CV3")+" CV3 "
		cQuery	+= "ON CV3_FILIAL	= '" + xFilial("CV3") + "' "
		If Upper(TcGetDb()) $ "ORACLE,POSTGRES,DB2,INFORMIX"		
			cQuery	+= "AND NVL(CAST(CV3_RECORI as int),0) = BD7.R_E_C_N_O_  "
		Else
			cQuery	+= "AND CONVERT(Int,CV3_RECORI) = BD7.R_E_C_N_O_ "
		EndIf
		cQuery	+= "AND CV3_TABORI		= 'BD7' "
		cQuery	+= "AND CV3.D_E_L_E_T_	= '' "
		
		// Lan�amentos Cont�beis
		cQuery	+= "INNER JOIN "+RetSqlName("CT2")+" CT2 "
		cQuery	+= "ON CT2_FILIAL	= '" + xFilial("CT2") + "' "
		If Upper(TcGetDb()) $ "ORACLE,POSTGRES,DB2,INFORMIX"		
			cQuery	+= "AND NVL(CAST(CV3_RECDES as int),0)= CT2.R_E_C_N_O_ "
		Else
			cQuery	+= "AND CONVERT(Int,CV3_RECDES) = CT2.R_E_C_N_O_ "
		EndIf
		cQuery += "AND CT2.CT2_DATA BETWEEN '"+aMesesTrim[nI][1]+aMesesTrim[nI][2]+"01' AND '"+aMesesTrim[nI][1]+aMesesTrim[nI][2]+"31' "
		cQuery += "AND CT2.CT2_CREDIT IN (" + cContas + ") " //Contas cr�dito
		cQuery	+= "AND CT2.D_E_L_E_T_	= '' "
		
		//------------------ Where ----------------------
		cQuery	+= "WHERE BD7_FILIAL	= '" + xFilial("BD7") + "' "		
					
		if nY == 13 //Data de ocorr�ncia do evento. Para a ultima linha, ser�o considerados inclusive todos os itens anteriores da base.
			cQuery	+= "AND BD7.BD7_DATPRO <= '" + cMesOcorr + "31' "	
		else
			cQuery	+= "AND BD7.BD7_DATPRO BETWEEN '" + cMesOcorr + "01' AND '" + cMesOcorr + "31' "	
		endif		
					
		cQuery += "AND BD7.BD7_FASE IN ('3','4') " 
		cQuery	+= "AND BD7.D_E_L_E_T_	= '' "
			
		cQuery	:= ChangeQuery(cQuery)
			
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPMOVPESL",.T.,.F.)
					
		if !TMPMOVPESL->(EoF())
			//Se tiver registro, adiciona no aDadosTemp o codigo correspondente somado com 33 que � o in�cio desse quadro.
			//Ao final do for, ser� 13 + 65 = 78 que � o �ltimo item do quadro.
			aadd(aDadosTemp, {nY + 65, TMPMOVPESL->VALOR})
		else
			//Se n�o tiver registro, adiciona linha em branco
			aadd(aDadosTemp, {nY + 65, 0}) 
		endif
		
		TMPMOVPESL->(dbCloseArea())
	next nY
	
	//Adiciona no aDados os registros obtidos no 6� quadro
	aadd(aDados[nI], aDadosTemp)
	aDadosTemp := {}
	
	//------------------- FIM 6� quadro ----------------
	
	//----------------- In�cio 7� quadro ---------------
	// C�digo 79 - PEONA
	//--------------------------------------------------
	aContas := {}
	aadd(aContas,"211111041")
	aadd(aContas,"211121041")
	aadd(aContas,"231111041")
	aadd(aContas,"231121041")
	
	nVal := 0
	
	//Percorre todos os itens das contas e utiliza fun��o do cont�bil para retornar os dados
	for nX := 1 to len(aContas)
		nVal += SaldoConta(aContas[nX],STOD(aMesesTrim[nI][1]+aMesesTrim[nI][2]+"31"),"01","1",1,1)
	next nX
	
	//Adiciona no array principal os valores para o c�digo 79, primeiro quadro
	//aMesesTrim[nI][1] = Ano
	//aMesesTrim[nI][2] = M�s
	aadd(aDados[nI], {{79,nVal}})
	
	//------------------- FIM 7� quadro ----------------
	//Incrementa a r�gua
	IncProc()
		
Next nI //Proximo m�s	


If !lDadosCSV

	//Monta aDados de acordo com padr�o esperado na central de obriga��es do Protheus. 
	// Primeira posi��o confirma se processou corretamente
	// Envia na segunda posi��o os dados de cada linha 
	/* 
	- Exemplo:
	aAdd( aDadosRet, { {'33',3000,2000,1000}, {'34',3000,2000,1000}, {'35',3000,2000,1000}, {'36',3000,2000,1000}, {'37',3000,2000,1000}, {'38',3000,2000,1000}, {'39',3000,2000,1000},; // 33 a 46 - Novos Avisos de Eventos
					{'40',3000,2000,1000}, {'41',3000,2000,1000}, {'42',3000,2000,1000}, {'43',3000,2000,1000}, {'44',3000,2000,1000}, {'45',3000,2000,1000}, {'46',3000,2000,1000},;				
					{'47',3000,2000,1000}, {'48',3000,2000,1000}, {'49',3000,2000,1000}, {'50',3000,2000,1000}, {'51',3000,2000,1000}, {'52',3000,2000,1000}, {'53',3000,2000,1000},; // 47 a 60 - Glosas
					{'54',3000,2000,1000}, {'55',3000,2000,1000}, {'56',3000,2000,1000}, {'57',3000,2000,1000}, {'58',3000,2000,1000}, {'59',3000,2000,1000}, {'60',3000,2000,1000},;
					{'65',3000,2000,1000}, {'66',3000,2000,1000}, {'67',3000,2000,1000}, {'68',3000,2000,1000}, {'69',3000,2000,1000}, {'70',3000,2000,1000}, {'71',3000,2000,1000},; // 65 a 78 - Outras Recupera��es 
					{'72',3000,2000,1000}, {'73',3000,2000,1000}, {'74',3000,2000,1000}, {'75',3000,2000,1000}, {'76',3000,2000,1000}, {'77',3000,2000,1000}, {'78',3000,2000,1000},;
					{'79',3000,2000,1000} }	)	// 79 - PEONA				
	*/

	For nI := 1 to Len(aDados)
		
		For nX := 1 to Len(aDados[nI])
			
			For nY := 1 to Len(aDados[nI,nX])

				If Len(aDadosRet) > 0 .and. aDados[nI,nX,nY][1] > 0		
					nZ := aScan(aDadosRet,{|x| x[1] == StrZero(aDados[nI,nX,nY][1],2) })
					If nZ == 0 
						aAdd(aDadosRet, { StrZero(aDados[nI,nX,nY][1],2) , IIf(nI==3, aDados[nI,nX,nY][2], 0),IIf(nI==2, aDados[nI,nX,nY][2], 0), IIf(nI==1, aDados[nI,nX,nY][2], 0) } )
					Else
						aDadosRet[nZ][ IIf(nI==1, 4, IIf(nI==2, 3, 2 ) ) ] += aDados[nI,nX,nY][2]		
					EndIf

				ElseIf aDados[nI,nX,nY][1] > 0
					aAdd(aDadosRet, { StrZero(aDados[nI,nX,nY][1],2) , IIf(nI==3, aDados[nI,nX,nY][2], 0),IIf(nI==2, aDados[nI,nX,nY][2], 0), IIf(nI==1, aDados[nI,nX,nY][2], 0) } )

				EndIf  
			
			Next
			
		Next
		
	Next
	// Retorna dados formatados para a Central
	Return( { (Len(aDadosRet)>0), aDadosRet } )

EndIf

// Retorna dados formatados para o relat�rio
Return aDados

//-------------------------------------------------------------------
/*/{Protheus.doc} PLDMOVPCSV
Fun��o respons�vel por gerar o CSV a partir dos dados obtidos na fun��o 

@author  Rodrigo Morgon
@version P12
@since   03/02/2017
@param   aDados, array, dados considerados para gerar o CSV

@return  nil, resultado da fun��o � o .CSV gerado na pasta do server.
/*/
//-------------------------------------------------------------------
static function PLDMOVPCSV(aDados, aMesesTrim)

	local nLenMeses := 0
	local cMesesHead := ""
	local nI := 1
	
	cDirCsv := cGetFile("TOTVS","Selecione o diretorio",,"",.T.,GETF_OVERWRITEPROMPT + GETF_NETWORKDRIVE + GETF_LOCALHARD + GETF_RETDIRECTORY)
	nFileCsv := FCreate(cDirCsv+"DIOPS_Movimentacao_PESL.csv",0,,.F.)
	
	//Formato
	//aDados[nMes][nQuadro]
	//		nQuadro == 1: Saldo inicial do m�s 													(linha)
	//		nQuadro == 2: Total pago no m�s 														(array)
	//		nQuadro == 3: Total dos novos avisos reconhecidos no m�s							(array)
	//		nQuadro == 4: Baixa de evento por glosa referentes a eventos ocorridos no m�s  	(array)
	//		nQuadro == 5: Saldo final da PSL relacionados a contratos em p�s-pagamento (+)	(linha)
	//		nQuadro == 6: Saldo final da PSL														(linha)
	//		nQuadro == 7: Total dos eventos ocorridos, avisados e pagos dentro do m�s e... 	(linha)
	//		nQuadro == 8: Total das glosas reconhecidas no m�s que n�o tenham transitado...	(linha)
	//		nQuadro == 9:	Outras recupera��es referentes a eventos ocorridos no m�s			(array)
	//		nQuadro == 10: PEONA																		(linha)
	
	
	//Cria arquivo CSV
	If nFileCsv > 0
		//Monta t�tulo
		FWrite(nFileCSV,"Movimenta��o da Provis�o de Eventos/Sinistros a Liquidar"+CRLF)
		
		nLenMeses := len(aMesesTrim)
		
		for nI := 1 to nLenMeses
			cMesesHead += aMesesTrim[nI][2] + "/" + aMesesTrim[nI][1] + ";"
		next nI
		
		//No cabe�alho, ir� incluir os meses referenciados no relat�rio
		FWrite(nFileCSV,"C�digo;Descri��o;" + cMesesHead + CRLF) //Pegar meses para iterar
		
		//Para montar as linhas do relat�rio, altero a ordem do array para escrever cada linha.
		FWrite(nFileCSV,"0;Saldo In�cio do m�s;" + RetValPESL(aDados,nLenMeses,1,1) + CRLF)
		FWrite(nFileCSV,"29;Total de eventos pagos no m�s para os eventos avisados no m�s;" + RetValPESL(aDados,nLenMeses,2,1) + CRLF)
		FWrite(nFileCSV,"30;Total de eventos pagos no m�s para os eventos avisados no m�s n-1;" + RetValPESL(aDados,nLenMeses,2,2) + CRLF)
		FWrite(nFileCSV,"31;Total de eventos pagos no m�s para os eventos avisados no m�s n-2;" + RetValPESL(aDados,nLenMeses,2,3) + CRLF)
		FWrite(nFileCSV,"32;Total de eventos pagos no m�s para os eventos avisados no m�s n-3 e anteriores a essa data;" + RetValPESL(aDados,nLenMeses,2,4) + CRLF)		
		FWrite(nFileCSV,"33;Novos avisos referentes a eventos ocorridos no m�s;" + RetValPESL(aDados,nLenMeses,3,1) + CRLF)
		FWrite(nFileCSV,"34;Novos avisos referentes a eventos ocorridos no m�s n-1;" + RetValPESL(aDados,nLenMeses,3,2) + CRLF)
		FWrite(nFileCSV,"35;Novos avisos referentes a eventos ocorridos no m�s n-2;" + RetValPESL(aDados,nLenMeses,3,3) + CRLF)
		FWrite(nFileCSV,"36;Novos avisos referentes a eventos ocorridos no m�s n-3;" + RetValPESL(aDados,nLenMeses,3,4) + CRLF)
		FWrite(nFileCSV,"37;Novos avisos referentes a eventos ocorridos no m�s n-4;" + RetValPESL(aDados,nLenMeses,3,5) + CRLF)
		FWrite(nFileCSV,"38;Novos avisos referentes a eventos ocorridos no m�s n-5;" + RetValPESL(aDados,nLenMeses,3,6) + CRLF)
		FWrite(nFileCSV,"39;Novos avisos referentes a eventos ocorridos no m�s n-6;" + RetValPESL(aDados,nLenMeses,3,7) + CRLF)
		FWrite(nFileCSV,"40;Novos avisos referentes a eventos ocorridos no m�s n-7;" + RetValPESL(aDados,nLenMeses,3,8) + CRLF)
		FWrite(nFileCSV,"41;Novos avisos referentes a eventos ocorridos no m�s n-8;" + RetValPESL(aDados,nLenMeses,3,9) + CRLF)
		FWrite(nFileCSV,"42;Novos avisos referentes a eventos ocorridos no m�s n-9;" + RetValPESL(aDados,nLenMeses,3,10) + CRLF)
		FWrite(nFileCSV,"43;Novos avisos referentes a eventos ocorridos no m�s n-10;" + RetValPESL(aDados,nLenMeses,3,11) + CRLF)
		FWrite(nFileCSV,"44;Novos avisos referentes a eventos ocorridos no m�s n-11;" + RetValPESL(aDados,nLenMeses,3,12) + CRLF)
		FWrite(nFileCSV,"45;Novos avisos referentes a eventos ocorridos no m�s n-12;" + RetValPESL(aDados,nLenMeses,3,13) + CRLF)
		FWrite(nFileCSV,"46;Novos avisos referentes a eventos ocorridos no m�s n-13 e anteriores a essa data;" + RetValPESL(aDados,nLenMeses,3,14) + CRLF)
		FWrite(nFileCSV,"47;Baixa de evento por glosa referentes a eventos ocorridos no m�s;" + RetValPESL(aDados,nLenMeses,4,1) + CRLF)
		FWrite(nFileCSV,"48;Baixa de evento por glosa referentes a eventos ocorridos no m�s n-1;" + RetValPESL(aDados,nLenMeses,4,2) + CRLF)
		FWrite(nFileCSV,"49;Baixa de evento por glosa referentes a eventos ocorridos no m�s n-2;" + RetValPESL(aDados,nLenMeses,4,3) + CRLF)
		FWrite(nFileCSV,"50;Baixa de evento por glosa referentes a eventos ocorridos no m�s n-3;" + RetValPESL(aDados,nLenMeses,4,4) + CRLF)
		FWrite(nFileCSV,"51;Baixa de evento por glosa referentes a eventos ocorridos no m�s n-4;" + RetValPESL(aDados,nLenMeses,4,5) + CRLF)
		FWrite(nFileCSV,"52;Baixa de evento por glosa referentes a eventos ocorridos no m�s n-5;" + RetValPESL(aDados,nLenMeses,4,6) + CRLF)
		FWrite(nFileCSV,"53;Baixa de evento por glosa referentes a eventos ocorridos no m�s n-6;" + RetValPESL(aDados,nLenMeses,4,7) + CRLF)
		FWrite(nFileCSV,"54;Baixa de evento por glosa referentes a eventos ocorridos no m�s n-7;" + RetValPESL(aDados,nLenMeses,4,8) + CRLF)
		FWrite(nFileCSV,"55;Baixa de evento por glosa referentes a eventos ocorridos no m�s n-8;" + RetValPESL(aDados,nLenMeses,4,9) + CRLF)
		FWrite(nFileCSV,"56;Baixa de evento por glosa referentes a eventos ocorridos no m�s n-9;" + RetValPESL(aDados,nLenMeses,4,10) + CRLF)
		FWrite(nFileCSV,"57;Baixa de evento por glosa referentes a eventos ocorridos no m�s n-10;" + RetValPESL(aDados,nLenMeses,4,11) + CRLF)
		FWrite(nFileCSV,"58;Baixa de evento por glosa referentes a eventos ocorridos no m�s n-11;" + RetValPESL(aDados,nLenMeses,4,12) + CRLF)
		FWrite(nFileCSV,"59;Baixa de evento por glosa referentes a eventos ocorridos no m�s n-12;" + RetValPESL(aDados,nLenMeses,4,13) + CRLF)
		FWrite(nFileCSV,"60;Baixa de evento por glosa referentes a eventos ocorridos no m�s n-13 e anteriores a essa data;" + RetValPESL(aDados,nLenMeses,4,14) + CRLF)
		FWrite(nFileCSV,"61;Saldo final da PSL relacionados a contratos em p�s-pagamento (+);" + RetValPESL(aDados,nLenMeses,5,1) + CRLF)
		FWrite(nFileCSV,"62;Saldo final da PSL;" + RetValPESL(aDados,nLenMeses,6,1) + CRLF)
		FWrite(nFileCSV,"63;Total dos eventos ocorridos, avisados e pagos dentro do m�s e que n�o tenham transitado pela Provis�o de Sinistros a Liquidar;" + RetValPESL(aDados,nLenMeses,7,1) + CRLF)
		FWrite(nFileCSV,"64;Total das glosas reconhecidas no m�s que n�o tenham transitado pela Provis�o de Sinistros a Liquidar;" + RetValPESL(aDados,nLenMeses,8,1) + CRLF)
		FWrite(nFileCSV,"65;Outras recupera��es referentes a eventos ocorridos no m�s;" + RetValPESL(aDados,nLenMeses,9,1) + CRLF)
		FWrite(nFileCSV,"66;Outras recupera��es referentes a eventos ocorridos no m�s n-1;" + RetValPESL(aDados,nLenMeses,9,2) + CRLF)
		FWrite(nFileCSV,"67;Outras recupera��es referentes a eventos ocorridos no m�s n-2;" + RetValPESL(aDados,nLenMeses,9,3) + CRLF)
		FWrite(nFileCSV,"68;Outras recupera��es referentes a eventos ocorridos no m�s n-3;" + RetValPESL(aDados,nLenMeses,9,4) + CRLF)
		FWrite(nFileCSV,"69;Outras recupera��es referentes a eventos ocorridos no m�s n-4;" + RetValPESL(aDados,nLenMeses,9,5) + CRLF)
		FWrite(nFileCSV,"70;Outras recupera��es referentes a eventos ocorridos no m�s n-5;" + RetValPESL(aDados,nLenMeses,9,6) + CRLF)
		FWrite(nFileCSV,"71;Outras recupera��es referentes a eventos ocorridos no m�s n-6;" + RetValPESL(aDados,nLenMeses,9,7) + CRLF)
		FWrite(nFileCSV,"72;Outras recupera��es referentes a eventos ocorridos no m�s n-7;" + RetValPESL(aDados,nLenMeses,9,8) + CRLF)
		FWrite(nFileCSV,"73;Outras recupera��es referentes a eventos ocorridos no m�s n-8;" + RetValPESL(aDados,nLenMeses,9,9) + CRLF)
		FWrite(nFileCSV,"74;Outras recupera��es referentes a eventos ocorridos no m�s n-9;" + RetValPESL(aDados,nLenMeses,9,10) + CRLF)
		FWrite(nFileCSV,"75;Outras recupera��es referentes a eventos ocorridos no m�s n-10;" + RetValPESL(aDados,nLenMeses,9,11) + CRLF)
		FWrite(nFileCSV,"76;Outras recupera��es referentes a eventos ocorridos no m�s n-11;" + RetValPESL(aDados,nLenMeses,9,12) + CRLF)
		FWrite(nFileCSV,"77;Outras recupera��es referentes a eventos ocorridos no m�s n-12;" + RetValPESL(aDados,nLenMeses,9,13) + CRLF)
		FWrite(nFileCSV,"78;Outras recupera��es referentes a eventos ocorridos no m�s n-13 e anteriores a essa data;" + RetValPESL(aDados,nLenMeses,9,14) + CRLF)
		FWrite(nFileCSV,"79;PEONA;" + RetValPESL(aDados,nLenMeses,10,1) + CRLF)
				
		FClose(nFileCSV)
		
		MsgInfo("Arquivo gerado com sucesso: " + cDirCsv + "DIOPS_Movimentacao_PESL.csv","TOTVS")
	Else
		MsgAlert("N�o foi poss�vel criar o arquivo " + cDirCsv + "DIOPS_Movimentacao_PESL","TOTVS")
	EndIf
return


//-------------------------------------------------------------------
/*/{Protheus.doc} CalcTrimes
Calcula os meses e anos de cada trimestre informado na busca

@author  Rodrigo Morgon
@version P12
@since   08/02/2017

@return  aMesesTrim, array de meses e anos baseado no trimestre
/*/
//-------------------------------------------------------------------
Static Function CalcTriMes(cTrimestre, cAno)
Local aMesesTrim := {}

Default cTrimestre	:= MV_PAR01
Default cAno		:= MV_PAR02

If cTrimestre == "1"
		aadd(aMesesTrim, {cAno,"01"}) //Jan
		aadd(aMesesTrim, {cAno,"02"}) //Fev
		aadd(aMesesTrim, {cAno,"03"}) //Mar
ElseIf cTrimestre == "2"
		aadd(aMesesTrim, {cAno,"04"}) //Abr
		aadd(aMesesTrim, {cAno,"05"}) //Mai
		aadd(aMesesTrim, {cAno,"06"}) //Jun
ElseIf cTrimestre == "3"
		aadd(aMesesTrim, {cAno,"07"}) //Jul
		aadd(aMesesTrim, {cAno,"08"}) //Ago
		aadd(aMesesTrim, {cAno,"09"}) //Set
Else		
		aadd(aMesesTrim, {cAno,"10"}) //Out
		aadd(aMesesTrim, {cAno,"11"}) //Nov
		aadd(aMesesTrim, {cAno,"12"}) //Dez
EndIf

Return(aMesesTrim)

//-------------------------------------------------------------------
/*/{Protheus.doc} RetValPESL
Retorna os dados de cada linha dos quadros de acordo com a linha solicitada.

@author  Rodrigo Morgon
@version P12
@since   08/02/2017

@return  cRetValPESL, valor da linha com todas as colunas de todos os meses solicitados
/*/
//-------------------------------------------------------------------
static function RetValPESL(aDados,nLenMeses,nQuadro,nLinha)

local cRetValPESL := ""
local nX := 0

default aDados := {}
default nLenMeses := 0
default nQuadro := 1
default nLinha := 1

//		nQuadro == 1: Saldo inicial do m�s 													(linha / array de 1)
//		nQuadro == 2: Total pago no m�s 														(array)
//		nQuadro == 3: Total dos novos avisos reconhecidos no m�s							(array)
//		nQuadro == 4: Baixa de evento por glosa referentes a eventos ocorridos no m�s  	(array)
//		nQuadro == 5: Saldo final da PSL relacionados a contratos em p�s-pagamento (+)	(linha / array de 1)
//		nQuadro == 6: Saldo final da PSL														(linha / array de 1)
//		nQuadro == 7: Total dos eventos ocorridos, avisados e pagos dentro do m�s e... 	(linha / array de 1)
//		nQuadro == 8: Total das glosas reconhecidas no m�s que n�o tenham transitado...	(linha / array de 1)
//		nQuadro == 9:	Outras recupera��es referentes a eventos ocorridos no m�s			(array)
//		nQuadro == 10: PEONA																		(linha / array de 1)

if !empty(aDados) .and. nLenMeses > 0
	for nX := 1 to nLenMeses
		//Percorre todos os registros de todos os meses e adiciona na string para retorno
		cRetValPESL += AllTrim(Str(aDados[nX][nQuadro][nLinha][2])) + ";"
	next nX
endif

Return cRetValPESL



//-------------------------------------------------------------------
/*/{Protheus.doc} PLDMOVPD2
Dados da DIOPS da movimenta��o de PESL

@author  Roger C
@version P12
@since   20/11/2018
@param   Trimestre e Ano para c�lculo

@return  aDados formatada para Central de Obriga��es
/*/
//-------------------------------------------------------------------
Function PLDMOVPD2(cTrimestre, cAno)
Local cQuery		:= ""
Local cSql			:= ""
Local aContas		:= {}
Local aDados 		:= {}
Local aDadosRet		:= {}
Local nVez 			:= 0
Local nX			:= 0
Local nVal			:= 0
Local aDadosTemp	:= {}
Local dDtFim		:= nil
Local nValTotal		:= 0
Local aMesesTrim	:= {}
Local aMeses1		:= {}
Local aMeses2		:= {}
Local aMeses3		:= {}

Default lDadosCSV	:= .T.
Default cTrimestre	:= MV_PAR01
Default cAno		:= MV_PAR02 

aAdd( aDadosRet, { {'33',0,0,0}, {'34',0,0,0}, {'35',0,0,0}, {'36',0,0,0}, {'37',0,0,0}, {'38',0,0,0}, {'39',0,0,0},; // 33 a 46 - Novos Avisos de Eventos
				{'40',0,0,0}, {'41',0,0,0}, {'42',0,0,0}, {'43',0,0,0}, {'44',0,0,0}, {'45',0,0,0}, {'46',0,0,0},;				
				{'47',0,0,0}, {'48',0,0,0}, {'49',0,0,0}, {'50',0,0,0}, {'51',0,0,0}, {'52',0,0,0}, {'53',0,0,0},; // 47 a 60 - Glosas
				{'54',0,0,0}, {'55',0,0,0}, {'56',0,0,0}, {'57',0,0,0}, {'58',0,0,0}, {'59',0,0,0}, {'60',0,0,0},;
				{'65',0,0,0}, {'66',0,0,0}, {'67',0,0,0}, {'68',0,0,0}, {'69',0,0,0}, {'70',0,0,0}, {'71',0,0,0},; // 65 a 78 - Outras Recupera��es 
				{'72',0,0,0}, {'73',0,0,0}, {'74',0,0,0}, {'75',0,0,0}, {'76',0,0,0}, {'77',0,0,0}, {'78',0,0,0},;
				{'79',0,0,0}})
				
aMesesTrim 	:= CalcTriM2(cTrimestre, cAno)

aMeses1		:= CalcTriM3(aMesesTrim[1,2], aMesesTrim[1,1] )
aMeses2		:= CalcTriM3(aMesesTrim[2,2], aMesesTrim[2,1] )
aMeses3		:= CalcTriM3(aMesesTrim[3,2], aMesesTrim[3,1] )

//Monta r�gua dos meses
ProcRegua(Len(aMesesTrim)) 

// Query de novos eventos conhecidos no trimestre
cQuery	:= "SELECT BD7_DTCTBF, BD7_DATPRO, BD7_DTANAL, BD7_CODPLA, BD7_FASE, BD7_SITUAC, BD7_VLRAPR, BD7_VLRGLO, BD7_VLRTPF, BD7_VLRPAG "
cQuery	+= "FROM "+RetSqlName("BD7") + " BD7 "
cQuery	+= "INNER JOIN "+RetSqlName("BI3") + " BI3 "
cQuery	+= "ON BI3_FILIAL = BD7_FILIAL "
cQuery	+= "AND BI3_CODIGO = BD7_CODPLA "
cQuery	+= "AND BI3_VERSAO = '001' "
cQuery	+= "AND BI3_MODPAG IN ('1','4') "
cQuery	+= "WHERE BD7.D_E_L_E_T_ = ' ' "
cQuery	+= "AND BD7_DTCTBF BETWEEN ('"+DtoS(aMesesTrim[1,3])+"') AND ('"+DtoS(aMesesTrim[3,4])+"') " 

cQuery	:= ChangeQuery(cQuery)

MPSysOpenQuery( cQuery, 'TMPMOV' )
TCSetField("TMPMOV","BD7_DTCTBF","D",8,0)
TCSetField("TMPMOV","BD7_DATPRO","D",8,0)
TCSetField("TMPMOV","BD7_DTANAL","D",8,0)
TCSetField("TMPMOV","BD7_VLRAPR","N",16,2)
TCSetField("TMPMOV","BD7_VLRGLO","N",16,2)
TCSetField("TMPMOV","BD7_VLRTPF","N",16,2)
TCSetField("TMPMOV","BD7_VLRPAG","N",16,2)		
				
While !TMPMOV->(EoF())

	dDatCol	:= FirstDay(TMPMOV->BD7_DTCTBF)
	dDatLin	:= FirstDay(TMPMOV->BD7_DATPRO)
	
	nCol := Ascan(aMesesTrim,{|x| x[3] == dDatCol })
	
	// Movimento do primeiro mes de referencia
	If  nCol == 1
		nLin := Ascan(aMeses1,{|x| x[3] == dDatLin })

	// Movimento do segundo mes de referencia
	ElseIf nCol == 2
		nLin := Ascan(aMeses2,{|x| x[3] == dDatLin })
	
	// Movimento do terceiro mes de referencia
	Else
		nLin := Ascan(aMeses3,{|x| x[3] == dDatLin })
	
	EndIf

	If nLin > 0 .and. nCol > 0
		nValRef	:= IIf(TMPMOV->BD7_VLRAPR > 0, TMPMOV->BD7_VLRAPR, TMPMOV->(BD7_VLRPAG+BD7_VLRGLO) ) 
		aDadosRet[1,nLin, nCol+1] += nValRef 
	EndIf

	TMPMOV->(dbSkip())

EndDo

TMPMOV->(dbCloseArea())
IncProc()

// Query de glosas e outras recupera��es conhecidos no trimestre
cQuery	:= "SELECT BD7_DTCTBF, BD7_DATPRO, BD7_DTANAL, BD7_CODPLA, BD7_FASE, BD7_SITUAC, BD7_VLRAPR, BD7_VLRGLO, BD7_VLRTPF, BD7_VLRPAG "
cQuery	+= "FROM "+RetSqlName("BD7") + " BD7 "
cQuery	+= "INNER JOIN "+RetSqlName("BI3") + " BI3 "
cQuery	+= "ON BI3_FILIAL = BD7_FILIAL "
cQuery	+= "AND BI3_CODIGO = BD7_CODPLA "
cQuery	+= "AND BI3_VERSAO = '001' "
cQuery	+= "AND BI3_MODPAG IN ('1','4') "
cQuery	+= "WHERE BD7.D_E_L_E_T_ = ' ' "
cQuery	+= "AND BD7_DTANAL BETWEEN ('"+DtoS(aMesesTrim[1,3])+"') AND ('"+DtoS(aMesesTrim[3,4])+"') " 

cQuery	:= ChangeQuery(cQuery)
			
MPSysOpenQuery( cQuery, 'TMPMOV' )
TCSetField("TMPMOV","BD7_DTCTBF","D",8,0)
TCSetField("TMPMOV","BD7_DATPRO","D",8,0)
TCSetField("TMPMOV","BD7_DTANAL","D",8,0)
TCSetField("TMPMOV","BD7_VLRAPR","N",16,2)
TCSetField("TMPMOV","BD7_VLRGLO","N",16,2)
TCSetField("TMPMOV","BD7_VLRTPF","N",16,2)
TCSetField("TMPMOV","BD7_VLRPAG","N",16,2)		
									
While !TMPMOV->(EoF())

	dDatCol	:= FirstDay(TMPMOV->BD7_DTANAL)
	dDatLin	:= FirstDay(TMPMOV->BD7_DATPRO)
	
	nCol := Ascan(aMesesTrim,{|x| x[3] == dDatCol })
	
	// Movimento do primeiro mes de referencia
	If  nCol == 1
		nLin := Ascan(aMeses1,{|x| x[3] == dDatLin })

	// Movimento do segundo mes de referencia
	ElseIf nCol == 2
		nLin := Ascan(aMeses2,{|x| x[3] == dDatLin })
	
	// Movimento do terceiro mes de referencia
	Else
		nLin := Ascan(aMeses3,{|x| x[3] == dDatLin })
	
	EndIf

	If nLin > 0 .and. nCol > 0
		If TMPMOV->BD7_VLRGLO > 0
			aDadosRet[1,nLin+14, nCol+1] += TMPMOV->BD7_VLRGLO 
		EndIf
		If TMPMOV->BD7_VLRTPF > 0
			aDadosRet[1,nLin+28, nCol+1] += TMPMOV->BD7_VLRTPF 
		EndIf
			
	EndIf

	TMPMOV->(dbSkip())

EndDo


TMPMOV->(dbCloseArea())
IncProc()
	
// C�digo 79 - PEONA
aContas := {}
aadd(aContas,"211111041")
aadd(aContas,"211121041")
aadd(aContas,"231111041")
aadd(aContas,"231121041")
	
// Processa 3 meses de PEONA
For nVez := 1 to 3

	nVal := 0
	//Percorre todos os itens das contas e utiliza fun��o do cont�bil para retornar os dados
	for nX := 1 to len(aContas)
		nVal += SaldoConta(aContas[nX],STOD(aMesesTrim[nVez][1]+aMesesTrim[nVez][2]+"31"),"01","1",1,1)
	next nX
	
	//Adiciona no array principal os valores para o c�digo 79, primeiro quadro
	//aMesesTrim[nI][1] = Ano
	//aMesesTrim[nI][2] = M�s
	aDadosRet[1,Len(aDadosRet), nVez+1 ] += nVal 
	
Next

IncProc()


//Monta aDados de acordo com padr�o esperado na central de obriga��es do Protheus. 
// Primeira posi��o confirma se processou corretamente
// Envia na segunda posi��o os dados de cada linha 
/* 
- Exemplo:
aAdd( aDadosRet, { {'33',3000,2000,1000}, {'34',3000,2000,1000}, {'35',3000,2000,1000}, {'36',3000,2000,1000}, {'37',3000,2000,1000}, {'38',3000,2000,1000}, {'39',3000,2000,1000},; // 33 a 46 - Novos Avisos de Eventos
				{'40',3000,2000,1000}, {'41',3000,2000,1000}, {'42',3000,2000,1000}, {'43',3000,2000,1000}, {'44',3000,2000,1000}, {'45',3000,2000,1000}, {'46',3000,2000,1000},;				
				{'47',3000,2000,1000}, {'48',3000,2000,1000}, {'49',3000,2000,1000}, {'50',3000,2000,1000}, {'51',3000,2000,1000}, {'52',3000,2000,1000}, {'53',3000,2000,1000},; // 47 a 60 - Glosas
				{'54',3000,2000,1000}, {'55',3000,2000,1000}, {'56',3000,2000,1000}, {'57',3000,2000,1000}, {'58',3000,2000,1000}, {'59',3000,2000,1000}, {'60',3000,2000,1000},;
				{'65',3000,2000,1000}, {'66',3000,2000,1000}, {'67',3000,2000,1000}, {'68',3000,2000,1000}, {'69',3000,2000,1000}, {'70',3000,2000,1000}, {'71',3000,2000,1000},; // 65 a 78 - Outras Recupera��es 
				{'72',3000,2000,1000}, {'73',3000,2000,1000}, {'74',3000,2000,1000}, {'75',3000,2000,1000}, {'76',3000,2000,1000}, {'77',3000,2000,1000}, {'78',3000,2000,1000},;
				{'79',3000,2000,1000} }	)	// 79 - PEONA				
*/


// Retorna dados formatados para a Central
Return( { (Len(aDadosRet)>0), IIF(Len(aDadosRet)>0,aDadosRet[1],aDadosRet) } )



//-------------------------------------------------------------------
/*/{Protheus.doc} CalcTrim2
Calcula os meses e anos de cada trimestre informado na busca

@author  Roger C
@version P12
@since   20/11/2018

@return  aMesesTrim, array de meses e anos baseado no trimestre
/*/
//-------------------------------------------------------------------
Static Function CalcTriM2(cTrimestre, cAno)
Local aMesesTrim := {}

Default cTrimestre	:= MV_PAR01
Default cAno		:= MV_PAR02
If cTrimestre == "1"
		aadd(aMesesTrim, {cAno,"01",FirstDay(Ctod('01/01/'+cAno)),LastDay(Ctod('01/01/'+cAno))}) //Jan
		aadd(aMesesTrim, {cAno,"02",FirstDay(Ctod('01/02/'+cAno)),LastDay(Ctod('01/02/'+cAno))}) //Fev
		aadd(aMesesTrim, {cAno,"03",FirstDay(Ctod('01/03/'+cAno)),LastDay(Ctod('01/03/'+cAno))}) //Mar
ElseIf cTrimestre == "2"
		aadd(aMesesTrim, {cAno,"04",FirstDay(Ctod('01/04/'+cAno)),LastDay(Ctod('01/04/'+cAno))}) //Abr
		aadd(aMesesTrim, {cAno,"05",FirstDay(Ctod('01/05/'+cAno)),LastDay(Ctod('01/05/'+cAno))}) //Mai
		aadd(aMesesTrim, {cAno,"06",FirstDay(Ctod('01/06/'+cAno)),LastDay(Ctod('01/06/'+cAno))}) //Jun
ElseIf cTrimestre == "3"
		aadd(aMesesTrim, {cAno,"07",FirstDay(Ctod('01/07/'+cAno)),LastDay(Ctod('01/07/'+cAno))}) //Jul
		aadd(aMesesTrim, {cAno,"08",FirstDay(Ctod('01/08/'+cAno)),LastDay(Ctod('01/08/'+cAno))}) //Ago
		aadd(aMesesTrim, {cAno,"09",FirstDay(Ctod('01/09/'+cAno)),LastDay(Ctod('01/09/'+cAno))}) //Set
Else		
		aadd(aMesesTrim, {cAno,"10",FirstDay(Ctod('01/10/'+cAno)),LastDay(Ctod('01/10/'+cAno))}) //Out
		aadd(aMesesTrim, {cAno,"11",FirstDay(Ctod('01/11/'+cAno)),LastDay(Ctod('01/11/'+cAno))}) //Nov
		aadd(aMesesTrim, {cAno,"12",FirstDay(Ctod('01/12/'+cAno)),LastDay(Ctod('01/12/'+cAno))}) //Dez
EndIf

Return(aMesesTrim)


//-------------------------------------------------------------------
/*/{Protheus.doc} CalcTrim3
Calcula os meses e anos para cada mes do trimestre 

@author  Roger C
@version P12
@since   20/11/2018

@return  aMeses, array de meses e anos baseado no mes 
/*/
//-------------------------------------------------------------------
Static Function CalcTriM3(cMes, cAno)
Local aMeses := {}
Local nVez	:= 0
Local dDatAtu := Ctod('01/'+cMes+'/'+cAno)

For nVez := 1 to 14
	dDatAtu := FirstDay(dDatAtu)
	aadd(aMeses, {nVez, dDatAtu, LastDay(dDatAtu) } )
	dDatAtu	:= dDatAtu - 25
Next

Return(aMeses)