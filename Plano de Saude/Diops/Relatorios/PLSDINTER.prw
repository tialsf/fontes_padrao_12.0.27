#Include 'Protheus.ch'
#Include 'PLSDINTER.ch'
#Include 'TopConn.ch'
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � PLSDINTER� Autor �F�bio S. dos Santos	� Data �01/04/2016���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gera��o de arquivo CSV. DIOPS - Interc�mbio Eventual       ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � TOTVS - SIGAPLS			                                  ���
�������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.             ���
�������������������������������������������������������������������������Ĵ��
���Programador � Data   � Motivo da Alteracao                             ���
�������������������������������������������������������������������������Ĵ��
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function PLSDINTER()
Local aSays			:= {}
Local aButtons		:= {}
Local cCadastro		:= STR0001 //"DIOPS Interc�mbio Eventual"

Private cPerg		:= "PLSDINTER"

//��������������������������������������������������������������������������Ŀ
//� Monta texto para janela de processamento                                 �
//����������������������������������������������������������������������������
aadd(aSays,STR0002 ) //"Esta rotina efetua a exportac�o do arquivo CSV da DIOPS - Interc�mbio Eventual."
aadd(aSays,STR0003 ) //"Antes de executar a rotina, informar os par�metros."

//��������������������������������������������������������������������������Ŀ
//� Monta botoes para janela de processamento                                �
//����������������������������������������������������������������������������
Aadd(aButtons, { 5,.T.,{|| Pergunte(cPerg,.T. ) } } )
AADD(aButtons, { 1,.T.,{|| Processa( {|| Pergunte(cPerg,.F. ),DINTERPROC(MV_PAR01) }, STR0008,STR0009,.F. ) } } )//"Processando DIOPS - Interc�mbio Eventual"###"Buscando informa��es..."
Aadd(aButtons, { 2,.T.,{|| FechaBatch() }} )

//��������������������������������������������������������������������������Ŀ
//� Exibe janela de processamento                                            �
//����������������������������������������������������������������������������
FormBatch(cCadastro,aSays,aButtons)

Return

//------------------------------------------------------------------
/*/{Protheus.doc} DINTERPROC

@description Processa o DIOPS 
@author F�bio Siqueira dos Santos
@since 13/02/2017
@version P12
@return Grava arquivo .CSV com as informa��es

/*/
//------------------------------------------------------------------
Static Function DINTERPROC(dDatAte)
Local cCabec		:= ""
Local cQuery		:= ""
Local aDados		:= {}
Local cTpCob		:= ""
Local dDatDe		:= CtoD('01/01/1900')
Local aRet			:= {}
Local nVez			:= 0
Default dDatAte		:= dDataBase

// Transforma no ultimo dia do mes a data enviada por parametro
dDatAte := LastDay(dDatAte)

If Empty(dDatAte) 
	MsgInfo(STR0004,STR0001) //"Par�metros n�o informados, por favor informar!"#"DIOPS Interc�mbio Eventual"
Else
	FechaBatch()		
	//������������������������������������������������������Ŀ
	//� Prepara os Dados para contas a receber               �
	//��������������������������������������������������������
	aRet := PlDIntR(.T.,dDatDe,dDatAte)		// Mostra Tela, Data Inicio, Data Fim - ser� considerado o �ltimo dia do m�s 
	If !aRet[1]
		MsgAlert('N�o foram encontrados dados de contas a receber para a gera��o do arquivo.')
	Else
		aDados := aClone(aRet[2])
	EndIf
	
	//������������������������������������������������������Ŀ
	//� Prepara os Dados para contas a pagar                 �
	//��������������������������������������������������������
	aRet := PlDIntP(.T.,dDatDe,dDatAte)		// Mostra Tela, Data Inicio, Data Fim - ser� considerado o �ltimo dia do m�s 
	If !aRet[1]
		MsgAlert('N�o foram encontrados dados de contas a pagar para a gera��o do arquivo.')
    	Return()
	EndIf
	For nVez := 1 to Len(aRet[2])
		aAdd( aDados, aRet[nVez,2] )
	Next nVez
	
	If Len(aDados) > 0					
		//Gera arquivo CSV
		PLSGerCSV("intercambio_"+ StrTran(DtoC(dDataBase), "/", "") + "_" + StrTran(time(),":","") + ".csv", cCabec, aDados)
	Else
		MsgInfo(STR0007,STR0001)//"N�o foram encontrados dados para gera��o do arquivo!"#"DIOPS Interc�mbio Eventual"
	EndIf

EndIf


Return


/*/{Protheus.doc} getTrbQuery
Executa query 

@author RogerC
@since 19/10/17
@version ALL
/*/
Function PlDIntR(lTela,dDatDe,dDatAte)
local lRet      := .f.
Local aFields := {}
Local oTempTable
Local oTempTabl2
Local nI		:= 0
Local cQuery
LOCAL nQryfile := 0
Local aPtoQuery:= {}
Local aPtoTpDoc:= {}
Local nPos		:= 0
Local aRecTit	:= 0
Local cWhere	:= ''
Local aSetField := {}
Local nVar		:= 0
Local nVez		:= 0

Local nTFilial	:= tamSX3("E1_FILIAL")[1]
Local nPFilial	:= 1

Local nTPrefixo	:= tamSX3("E1_PREFIXO")[1]
Local nPPrefixo	:= nPFilial + nTFilial + 1

Local nTNumero	:= tamSX3("E1_NUM")[1]
Local nPNumero	:= nPPrefixo + nTPrefixo + 1

Local nTParcela	:= tamSX3("E1_PARCELA")[1]
Local nPParcela	:= nPNumero + nTNumero +  1

Local nTTipo	:= tamSX3("E1_TIPO")[1]
Local nPTipo	:= nPParcela + nTParcela + 1

Local nTCliente	:= tamSX3("E1_CLIENTE")[1]
Local nPCliente	:= nPTipo + nTTipo + 1

Local nTLoja	:= tamSX3("E1_LOJA")[1]
Local nPLoja	:= nPCliente + nTCliente + 1

Local lMsSql	:= Upper(TcGetDb()) $ "MSSQL"

local lDtCTB	:= getNewPar("MV_PLDTREC", '0') == '0'

Local nRecTr1	:= 0
Local nCount	:= 0

Local aContas	:= {}
// Array default das contas do DIOPS
Local aDefDiops	:= {'124119022',;
					'123129022' }

Local nCta		:= 0
Local cConta	:= ''
Local aDados	:= {}

Local cTITTMP	:= GetNextAlias()
Local cArqTmp	:= GetNextAlias()

Default lTela	:= .T.
Default dDatDe	:= CtoD('01/01/1900')
Default dDatAte	:= dDataBase 

// Ser� considerado sempre o �ltimo dia do m�s da data que for enviada por par�metro
dDatAte	:= LastDay(dDatAte) 
					
// Loop de colunas para preenchimento do array principal de valores do DIOPS
For nVar := 1 to 2
	cStr	:= GetNewPar('MV_PLIER'+AllTrim(StrZero(nVar,2)), aDefDiops[nVar] )
	For nPos := 1 to Len(cStr)
		If Subs(cStr,nPos,1) == ','
			aAdd(aContas, { cConta, nVar } )
			cConta := '' 
		Else
			cConta := cConta + Subs(cStr,nPos,1)
		EndIf   		
	Next nPos
	If !Empty(cConta)
		aAdd(aContas, { cConta, nVar } )
		cConta := '' 
	EndIf
Next nVar

// Filtra registros baixados at� o per�odo
cQuery	:= "SELECT BM1.R_E_C_N_O_ AS BM1REG, BM1_VALOR AS VALOR, BM1_TIPO, BM1_PREFIX AS PREFIXO, BM1_NUMTIT AS NUMERO, BM1_PARCEL AS PARCELA, BM1_TIPTIT, "
if lDtCTB
	cQuery += "FK5_DATA AS VENCTO, "
else
	cQuery += "FK5_DTDISP AS VENCTO, "
endIf	
cQuery	+= "BA0_NOMINT, BA0_SUSEP "

cQuery += " FROM " + PLSSQLNAME("FK5") + " FK5 "

cQuery += " INNER JOIN " + PLSSQLNAME("FK7") + " FK7 " 
cQuery += "    ON FK7_FILIAL = '" + xFilial("FK7") + "' "
cQuery += "   AND FK7_ALIAS  = 'SE1' "
cQuery += "   AND FK7.D_E_L_E_T_ = ' ' "
cQuery += "   AND FK7_IDDOC  = (SELECT FK1_A.FK1_IDDOC "
cQuery += "                       FROM " + PLSSQLNAME("FK1") + " FK1_A " 
cQuery += "                      WHERE FK1_A.FK1_FILIAL = '" + xFilial("FK1") + "' "
cQuery += "                        AND FK1_A.D_E_L_E_T_ = ' ' "
cQuery += "                        AND FK1_IDFK1 = (SELECT MIN(FKA_B.FKA_IDORIG) "
cQuery += "                                           FROM " + PLSSQLNAME("FKA") + " FKA_B " 
cQuery += "                                          WHERE FKA_B.FKA_FILIAL = '" + xFilial("FKA") + "' "
cQuery += "                                            AND FKA_B.FKA_TABORI = 'FK1' "
cQuery += "                                            AND FKA_B.D_E_L_E_T_ = ' ' "
cQuery += "                                            AND FKA_B.FKA_IDPROC = (SELECT MIN(FKA_A.FKA_IDPROC) "
cQuery += "                                                                      FROM " + PLSSQLNAME("FKA") + " FKA_A "
cQuery += "                                                                     WHERE FKA_A.FKA_FILIAL = '" + xFilial("FKA") + "' "
cQuery += "                                                                       AND FKA_A.FKA_TABORI = 'FK5' "
cQuery += "                                                                       AND FKA_A.D_E_L_E_T_  = ' ' "
cQuery += "                                                                       AND FKA_A.FKA_IDORIG = FK5.FK5_IDMOV) ) ) "

cQuery += " LEFT JOIN " + PLSSQLNAME("FK6") + " FK6 "
cQuery += "   ON FK6_FILIAL = '" + xFilial("FK6") + "' "
cQuery += "  AND FK6_IDORIG = FK5_IDMOV "
cQuery += "  AND FK6_TABORI = 'FK1' "
cQuery += "  AND FK6.D_E_L_E_T_ = ' ' "

cQuery += " INNER JOIN " + PLSSQLNAME("SE1") + " SE1 "
cQuery += "    ON E1_FILIAL	= SUBSTRING(FK7_CHAVE," + cValToChar(nPFilial)  + "," + cValToChar(nTFilial) + ") " 
cQuery += "   AND E1_PREFIXO	= SUBSTRING(FK7_CHAVE," + cValToChar(nPPrefixo) + "," + cValToChar(nTPrefixo) + ") "
cQuery += "   AND E1_NUM		= SUBSTRING(FK7_CHAVE," + cValToChar(nPNumero)  + "," + cValToChar(nTNumero) + ") "
cQuery += "   AND E1_PARCELA	= SUBSTRING(FK7_CHAVE," + cValToChar(nPParcela) + "," + cValToChar(nTParcela) + ") "
cQuery += "   AND E1_TIPO		= SUBSTRING(FK7_CHAVE," + cValToChar(nPTipo)  	+ "," + cValToChar(nTTipo) + ") "
cQuery += "   AND E1_CLIENTE	= SUBSTRING(FK7_CHAVE," + cValToChar(nPCliente) + "," + cValToChar(nTCliente) + ") "
cQuery += "   AND E1_LOJA		= SUBSTRING(FK7_CHAVE," + cValToChar(nPLoja)  	+ "," + cValToChar(nTLoja) + ") "
//AB-|FB-|FC-|FU-|IR-|IN-|IS-|PI-|CF-|CS-|FE-|IV- //IR- //IN- //NCC //NDF //RA //PA
cQuery += "   AND E1_TIPO NOT IN " + formatIn(MVABATIM+"|"+MVIRABT+"|"+MVINABT+"|"+MV_CRNEG+"|"+MV_CPNEG+"|"+MVPAGANT+"|"+MVRECANT ,"|") 
cQuery += "   AND E1_TIPO NOT IN ('RA ','PA ',' NCC ',' NDF') "  
cQuery += "   AND SUBSTRING(E1_ORIGEM,1,3) = 'PLS' " 
// a data de emiss�o deve ser menor que a informada no par�metro
cQuery += "   AND E1_EMISSAO <='"+DtoS(dDatAte)+"' "	
cQuery += "   AND SE1.D_E_L_E_T_ = ' ' " 

cQuery += " INNER JOIN " + PLSSQLNAME("BM1") + " BM1 "
cQuery += "    ON BM1_FILIAL =  '" + xFilial("BM1") + "' "
cQuery += "   AND BM1_PLNUCO = E1_PLNUCOB "
cQuery += "   AND BM1_PREFIX = E1_PREFIXO "
cQuery += "   AND BM1_NUMTIT = E1_NUM "
cQuery += "   AND BM1_PARCEL = E1_PARCELA " 
cQuery += "   AND BM1_TIPTIT = E1_TIPO " 
cQuery += "   AND BM1.D_E_L_E_T_ = ' ' "

// Contrato
cQuery += "INNER JOIN "+RetSqlName("BT5")+" BT5 "
cQuery += "ON BT5_FILIAL='"+xFilial('BT5')+"' "
cQuery += "  AND BT5_CODINT=BM1.BM1_CODINT "
cQuery += "  AND BT5_CODIGO=BM1.BM1_CODEMP "
cQuery += "  AND BT5_NUMCON=BM1.BM1_CONEMP "
cQuery += "  AND BT5_VERSAO=BM1.BM1_VERCON "
cQuery += "  AND BT5_INTERC = '1' "
cQuery += "  AND BT5_INFANS = '1' "
cQuery += "  AND BT5.D_E_L_E_T_='' "

// Operadora
cQuery += "INNER JOIN "+RetSqlName("BA0")+" BA0 "
cQuery += "ON BA0_FILIAL='"+xFilial("BA0")+"' "
cQuery += " AND BA0_CODIDE= '0' "
If Upper(TcGetDb()) $ "ORACLE,POSTGRES,DB2,INFORMIX"		
	cQuery += " AND BA0_CODINT = DECODE(BT5.BT5_ALLOPE, '1', '"+Subs(PlsIntPad(),2,3)+"', BT5.BT5_OPEINT ) "
Else
	cQuery += " AND BA0_CODINT = ("
	cQuery += "CASE WHEN BT5.BT5_ALLOPE = '1' THEN '"+Subs(PlsIntPad(),2,3)+"' "
	cQuery += "ELSE BT5.BT5_OPEINT END) "	
EndIf
cQuery += "  AND BA0.D_E_L_E_T_='' "

cQuery += " WHERE FK5_FILIAL = '" + xFilial("FK5") + "' "
if lDtCTB
	cQuery += "   AND FK5_DATA BETWEEN '" + dtos(dDatDe) + "' AND '" +  dtos(dDatAte) + "' "
else
	cQuery += "   AND FK5_DTDISP BETWEEN '" + dtos(dDatDe) + "' AND '" +  dtos(dDatAte) + "' "
endIf	
cQuery	+= "AND SE1.E1_TIPO NOT IN ('RA ','PA ',' NCC ',' NDF') "  
cQuery	+= "AND SUBSTRING(SE1.E1_ORIGEM,1,3) = 'PLS' " 
cQuery	+= "AND SE1.E1_TIPOLIQ NOT IN('LIQ','CAN') "
cQuery  += "AND FK5.D_E_L_E_T_ = ' ' "
cQuery	+= "AND SE1.D_E_L_E_T_=' ' "
cQuery	+= "AND BM1.D_E_L_E_T_=' ' "

//consulta de titulos negociados
cQuery += " UNION ALL "

cQuery	+= "SELECT BM1.R_E_C_N_O_ AS BM1REG, BM1_VALOR AS VALOR, BM1_TIPO, BM1_PREFIX AS PREFIXO, BM1_NUMTIT AS NUMERO, BM1_PARCEL AS PARCELA, BM1_TIPTIT, "
if lDtCTB
	cQuery += "FK5_DATA AS VENCTO, "
else
	cQuery += "FK5_DTDISP AS VENCTO, "
endIf	
cQuery	+= "BA0_NOMINT, BA0_SUSEP "

cQuery += " FROM " + PLSSQLNAME("FK5") + " FK5 "

cQuery += " INNER JOIN " + PLSSQLNAME("FK7") + " FK7 " 
cQuery += "    ON FK7_FILIAL = '" + xFilial("FK7") + "' "
cQuery += "   AND FK7_ALIAS  = 'SE1' "
cQuery += "   AND FK7.D_E_L_E_T_ = ' ' "
cQuery += "   AND FK7_IDDOC  = (SELECT FK1_A.FK1_IDDOC "
cQuery += "                       FROM " + PLSSQLNAME("FK1") + " FK1_A " 
cQuery += "                      WHERE FK1_A.FK1_FILIAL = '" + xFilial("FK1") + "' "
cQuery += "                        AND FK1_A.D_E_L_E_T_ = ' ' "
cQuery += "                        AND FK1_IDFK1 = (SELECT MIN(FKA_B.FKA_IDORIG) "
cQuery += "                                           FROM " + PLSSQLNAME("FKA") + " FKA_B " 
cQuery += "                                          WHERE FKA_B.FKA_FILIAL = '" + xFilial("FKA") + "' "
cQuery += "                                            AND FKA_B.FKA_TABORI = 'FK1' "
cQuery += "                                            AND FKA_B.D_E_L_E_T_ = ' ' "
cQuery += "                                            AND FKA_B.FKA_IDPROC = (SELECT MIN(FKA_A.FKA_IDPROC) "
cQuery += "                                                                      FROM " + PLSSQLNAME("FKA") + " FKA_A "
cQuery += "                                                                     WHERE FKA_A.FKA_FILIAL = '" + xFilial("FKA") + "' "
cQuery += "                                                                       AND FKA_A.FKA_TABORI = 'FK5' "
cQuery += "                                                                       AND FKA_A.D_E_L_E_T_  = ' ' "
cQuery += "                                                                       AND FKA_A.FKA_IDORIG = FK5.FK5_IDMOV) ) ) "

cQuery += " LEFT JOIN " + PLSSQLNAME("FK6") + " FK6 "
cQuery += "   ON FK6_FILIAL = '" + xFilial("FK6") + "' "
cQuery += "   AND FK6_IDORIG = FK5_IDMOV "
cQuery += "   AND FK6_TABORI = 'FK1' "
cQuery += "   AND FK6.D_E_L_E_T_ = ' ' "

cQuery += " INNER JOIN " + PLSSQLNAME("SE1") + " SE1 "
cQuery += "    ON E1_FILIAL	= SUBSTRING(FK7_CHAVE," + cValToChar(nPFilial)  + "," + cValToChar(nTFilial) + ") " 
cQuery += "   AND E1_PREFIXO	= SUBSTRING(FK7_CHAVE," + cValToChar(nPPrefixo) + "," + cValToChar(nTPrefixo) + ") "
cQuery += "   AND E1_NUM		= SUBSTRING(FK7_CHAVE," + cValToChar(nPNumero)  + "," + cValToChar(nTNumero) + ") "
cQuery += "   AND E1_PARCELA	= SUBSTRING(FK7_CHAVE," + cValToChar(nPParcela) + "," + cValToChar(nTParcela) + ") "
cQuery += "   AND E1_TIPO		= SUBSTRING(FK7_CHAVE," + cValToChar(nPTipo)  	+ "," + cValToChar(nTTipo) + ") "
cQuery += "   AND E1_CLIENTE	= SUBSTRING(FK7_CHAVE," + cValToChar(nPCliente) + "," + cValToChar(nTCliente) + ") "
cQuery += "   AND E1_LOJA		= SUBSTRING(FK7_CHAVE," + cValToChar(nPLoja)  	+ "," + cValToChar(nTLoja) + ") "
//AB-|FB-|FC-|FU-|IR-|IN-|IS-|PI-|CF-|CS-|FE-|IV- //IR- //IN- //NCC //NDF //RA //PA
cQuery += "   AND E1_TIPO NOT IN " + formatIn(MVABATIM+"|"+MVIRABT+"|"+MVINABT+"|"+MV_CRNEG+"|"+MV_CPNEG+"|"+MVPAGANT+"|"+MVRECANT ,"|") 
cQuery += "   AND SUBSTRING(E1_ORIGEM,1,3) = 'PLS' " 
// a data de emiss�o deve ser menor que a informada no par�metro
cQuery += "   AND E1_EMISSAO <='"+DtoS(dDatAte)+"' "	
cQuery += "   AND SE1.D_E_L_E_T_ = ' ' " 

//VERIFICA SE E UM TITULO NEGOCIADO
cQuery += "   INNER JOIN " + PLSSQLNAME("FI7") + " FI7 " 
cQuery += "      ON FI7.FI7_FILIAL = E1_FILIAL "
cQuery += "     AND FI7.FI7_PRFDES = E1_PREFIXO "
cQuery += "     AND FI7.FI7_NUMDES = E1_NUM "
cQuery += "     AND FI7.FI7_PARDES = E1_PARCELA "
cQuery += "     AND FI7.FI7_TIPDES = E1_TIPO "
cQuery += "     AND FI7.FI7_CLIDES = E1_CLIENTE "
cQuery += "     AND FI7.FI7_LOJDES = E1_LOJA "
cQuery += "     AND FI7.D_E_L_E_T_ = ' ' "

//POSICIONA NO TITULO PAI QUE ESTA AMARRADO COM A BM1
cQuery += "   INNER JOIN " + PLSSQLNAME("BM1") + " BM1 " 
cQuery += "      ON BM1.BM1_FILIAL = '" + xFilial("BM1") + "' "
cQuery += "     AND BM1.BM1_PREFIX = FI7.FI7_PRFORI "
cQuery += "     AND BM1.BM1_NUMTIT = FI7.FI7_NUMORI "
cQuery += "     AND BM1.BM1_PARCEL = FI7.FI7_PARORI "
cQuery += "     AND BM1.BM1_TIPTIT = FI7.FI7_TIPORI "
cQuery += "     AND BM1.D_E_L_E_T_ = ' '  "

// Contrato
cQuery += "INNER JOIN "+RetSqlName("BT5")+" BT5 "
cQuery += "ON BT5_FILIAL='"+xFilial('BT5')+"' "
cQuery += "  AND BT5_CODINT=BM1.BM1_CODINT "
cQuery += "  AND BT5_CODIGO=BM1.BM1_CODEMP "
cQuery += "  AND BT5_NUMCON=BM1.BM1_CONEMP "
cQuery += "  AND BT5_VERSAO=BM1.BM1_VERCON "
cQuery += "  AND BT5_INTERC = '1' "
cQuery += "  AND BT5_INFANS = '1' "
cQuery += "  AND BT5.D_E_L_E_T_='' "

// Operadora
cQuery += "INNER JOIN "+RetSqlName("BA0")+" BA0 "
cQuery += "ON BA0_FILIAL='"+xFilial("BA0")+"' "
cQuery += " AND BA0_CODIDE= '0' "
If Upper(TcGetDb()) $ "ORACLE,POSTGRES,DB2,INFORMIX"		
	cQuery += " AND BA0_CODINT = DECODE(BT5.BT5_ALLOPE, '1', '"+Subs(PlsIntPad(),2,3)+"', BT5.BT5_OPEINT ) "
Else
	cQuery += " AND BA0_CODINT = ("
	cQuery += "CASE WHEN BT5.BT5_ALLOPE = '1' THEN '"+Subs(PlsIntPad(),2,3)+"' "
	cQuery += "ELSE BT5.BT5_OPEINT END) "	
EndIf
cQuery += "  AND BA0.D_E_L_E_T_='' "

cQuery += " WHERE FK5_FILIAL = '" + xFilial("FK5") + "' "
If lDtCTB
	cQuery += "  AND FK5_DATA BETWEEN '" + dtos(dDatDe) + "' AND '" +  dtos(dDatAte) + "' "
else
	cQuery += "  AND FK5_DTDISP BETWEEN '" + dtos(dDatDe) + "' AND '" +  dtos(dDatAte) + "' "
endIf	
cQuery += "  AND FK5_LA = 'S' " 
cQuery += "  AND FK5.D_E_L_E_T_ = ' ' "
cQuery += "  AND BM1.D_E_L_E_T_=' ' "

//Consulta de titulos em aberto
cQuery += " UNION ALL "

cQuery	+= "SELECT BM1.R_E_C_N_O_ AS BM1REG, BM1_VALOR AS VALOR, BM1_TIPO, BM1_PREFIX AS PREFIXO, BM1_NUMTIT AS NUMERO, BM1_PARCEL AS PARCELA, BM1_TIPTIT, "
if lDtCTB
	cQuery += "E1_VENCREA AS VENCTO, "
else
	cQuery += "E1_MOVIMEN AS VENCTO, "
endIf	
cQuery	+= "BA0_NOMINT, BA0_SUSEP "

cQuery += " FROM " + PLSSQLNAME("SE1") + " SE1 "

//POSICIONA NO TITULO PAI QUE ESTA AMARRADO COM A BM1
cQuery += "   INNER JOIN " + PLSSQLNAME("BM1") + " BM1 " 
cQuery += "      ON BM1.BM1_FILIAL = '" + xFilial("BM1") + "' "
cQuery += "     AND BM1.BM1_PREFIX = SE1.E1_PREFIXO "
cQuery += "     AND BM1.BM1_NUMTIT = SE1.E1_NUM "
cQuery += "     AND BM1.BM1_PARCEL = SE1.E1_PARCELA "
cQuery += "     AND BM1.BM1_TIPTIT = SE1.E1_TIPO  "
cQuery += "     AND BM1.D_E_L_E_T_ = ' '  "

// Contrato
cQuery += "INNER JOIN "+RetSqlName("BT5")+" BT5 "
cQuery += "ON BT5_FILIAL='"+xFilial('BT5')+"' "
cQuery += "  AND BT5_CODINT=BM1.BM1_CODINT "
cQuery += "  AND BT5_CODIGO=BM1.BM1_CODEMP "
cQuery += "  AND BT5_NUMCON=BM1.BM1_CONEMP "
cQuery += "  AND BT5_VERSAO=BM1.BM1_VERCON "
cQuery += "  AND BT5_INTERC = '1' "
cQuery += "  AND BT5_INFANS = '1' "
cQuery += "  AND BT5.D_E_L_E_T_='' "

// Operadora
cQuery += "INNER JOIN "+RetSqlName("BA0")+" BA0 "
cQuery += "ON BA0_FILIAL='"+xFilial("BA0")+"' "
cQuery += " AND BA0_CODIDE= '0' "
If Upper(TcGetDb()) $ "ORACLE,POSTGRES,DB2,INFORMIX"		
	cQuery += " AND BA0_CODINT = DECODE(BT5.BT5_ALLOPE, '1', '"+Subs(PlsIntPad(),2,3)+"', BT5.BT5_OPEINT ) "
Else
	cQuery += " AND BA0_CODINT = ("
	cQuery += "CASE WHEN BT5.BT5_ALLOPE = '1' THEN '"+Subs(PlsIntPad(),2,3)+"' "
	cQuery += "ELSE BT5.BT5_OPEINT END) "	
EndIf
cQuery += "  AND BA0.D_E_L_E_T_='' "

cQuery += " WHERE E1_FILIAL = '" + xFilial("SE1") + "' "
//AB-|FB-|FC-|FU-|IR-|IN-|IS-|PI-|CF-|CS-|FE-|IV- //IR- //IN- //NCC //NDF //RA //PA
cQuery += "   AND E1_TIPO NOT IN " + formatIn(MVABATIM+"|"+MVIRABT+"|"+MVINABT+"|"+MV_CRNEG+"|"+MV_CPNEG+"|"+MVPAGANT+"|"+MVRECANT ,"|") 
cQuery += "   AND SUBSTRING(SE1.E1_ORIGEM,1,3) = 'PLS' " 
// a data de emiss�o deve ser menor que a informada no par�metro
cQuery += "   AND SE1.E1_EMISSAO <='"+DtoS(dDatAte)+"' "	
cQuery += "   AND SE1.E1_BAIXA > '"+DtoS(dDatAte)+"' OR SE1.E1_BAIXA = '' "	
cQuery += "   AND SE1.D_E_L_E_T_ = ' ' " 
cQuery	+= "  AND BM1.D_E_L_E_T_=' ' "

cQuery	+= "ORDER BY BA0_NOMINT, BA0_SUSEP, PREFIXO, NUMERO, PARCELA, BM1_TIPTIT, VENCTO, BM1REG, BM1_TIPO  "

nHandle := fCreate('QRYDINTR.sql', 0)
fWrite(nHandle, CHR(13)+CHR(10)+cQuery+CHR(13)+CHR(10) )
fClose(nHandle)

MPSysOpenQuery( ChangeQuery(cQuery), cArqTmp )
TCSetField(cArqTmp,"BM1_VALOR","N",16,2)
TCSetField(cArqTmp,"VENCTO","D",08,0)

//-------------------
//Cria��o da Temporaria para Evitar recalculo de saldo do Titulo
//-------------------
aFields := {}
aadd(aFields,{"CHAVE","C",40,0})
aadd(aFields,{"VALOR1","N",16,2})
aadd(aFields,{"VALOR2","N",16,2})
oTempTabl2 := FWTemporaryTable():New( cTITTMP, aFields )
oTempTabl2:AddIndex("INDCHV", {"CHAVE"} )
//------------------
//Cria��o da tabela
//------------------
oTempTabl2:Create()

If lTela
	(cArqTmp)->(dbGotop())
	(cArqTmp)->(dbEval({|| nRecTr1++ }))
	ProcRegua((cArqTmp)->(RecCount()))	
EndIf

(cArqTmp)->(dbGotop())
While !(cArqTmp)->(EOF())
//	BM1->(dbGoTo((cArqTmp)->BM1REG))
	If lTela
		nCount ++
		IncProc("Foram encontrados " + alltrim(str(nRecTr1)) + " registros. Processando registro " + alltrim(str(nCount) ))
	EndIf

	// Se n�o achou contabiliza��o, pula o registro
	If !CV3->(msSeek(xFilial('CV3')+'BM1'+AllTrim(Str((cArqTmp)->BM1REG)) ) )
		(cArqTmp)->(dbSkip())
		Loop

	Else
		lCT2Ativo 	:= .F.
		cLastReg 	:= allTrim( str( (cArqTmp)->BM1REG ) )
		While !lCT2Ativo .and. !CV3->(eof()) .and. allTrim(CV3->CV3_RECORI) == cLastReg
			CT2->( msGoTo( val( CV3->CV3_RECDES ) ) )
			if CT2->(deleted())
				CV3->(dbSkip())
				loop
			else
				lCT2Ativo 	:= .t.
				Exit
			endIf
		endDo

		If !lCT2Ativo
			(cArqTmp)->(dbSkip())
			Loop
		EndIf
		
	EndIf		
	
	If !(cTITTMP)->(dbSeek((cArqTmp)->(PREFIXO+NUMERO+PARCELA+BM1_TIPTIT)+DTOS(dDatAte),.F.) )
		// Fun��o que retona o Valor Recebido e Valor Total, mesmo que haja renegocia��o de t�tulos
		aRecTit := PLRETVLRTOT((cArqTmp)->(PREFIXO+NUMERO+PARCELA+BM1_TIPTIT),dDatAte)
		// aRecTit[1]	// Valor Recebido 	= E1_VALOR - E1_SALDO
		// aRecTit[2]	// Valor Total		= E1_VALOR
		RecLock(cTITTMP,.T.)
		(cTITTMP)->CHAVE	:= (cArqTmp)->(PREFIXO+NUMERO+PARCELA+BM1_TIPTIT)+DTOS(dDatAte)
		(cTITTMP)->VALOR1	:= aRecTit[1]
		(cTITTMP)->VALOR2	:= aRecTit[2]
		(cTITTMP)->(msUnlock())
	Else
		aRecTit := { (cTITTMP)->VALOR1, (cTITTMP)->VALOR2 }
	EndIf
	// Se n�o houver valor a receber, n�o deve sair no relat�rio.
	If aRecTit[1] == aRecTit[2] 
		(cArqTmp)->(dbSkip())
		Loop
	EndIf		

	// Se Debito
	If CV3->CV3_DC == '1'
		cConta	:= CV3->CV3_DEBITO
		nVez	:= 1
	// Se Credito
	ElseIf CV3->CV3_DC == '2'
		cConta	:= CV3->CV3_CREDIT
		nVez	:= 1
	// Se Partida Dobrada
	ElseIf CV3->CV3_DC == '3'
		cConta	:= CV3->CV3_DEBITO		
		cConta2	:= CV3->CV3_CREDIT
		nVez	:= 2
	Else
		(cArqTmp)->(dbSkip())
		Loop
	EndIf
	
	For nVar := 1 to nVez
	
		cConta	:= AllTrim(IIf(nVar==1,cConta,cConta2))		
		For nCta := Len(cConta) to 1 Step -1
			nPos := aScan(aContas, {|x| x[1] == Subs(cConta,1,nCta)} )
			// Se achou, sai do loop
			If nPos > 0 
				Exit
			EndIf
		Next nCta
		
		// Se nao achou, pula registro
		If nPos == 0
			Loop
		EndIf
	
		// TIPO DE COBERTURA - HOSPITALAR OU ODONTOLOGICA - CONFORME A CONTA CONTABIL
		If aContas[nPos,2] == 1
			cTpCob	:= "H"
		Else
			cTpCob	:= "O"
		EndIf
	
		nValor := ((cArqTmp)->VALOR/aRecTit[2]) * ( aRecTit[2] - aRecTit[1] ) 
		// Se for cr�dito, deve aparecer subtraindo
		If (cArqTmp)->BM1_TIPO == '2'		
			nValor := nValor * -1
		EndIf 		  
	
		If ( nPos := aScan( aDados, { |x| x[1] == (cArqTmp)->(PREFIXO+NUMERO+PARCELA+BM1_TIPTIT)+DtoS((cArqTmp)->VENCTO) } ) ) == 0  
			aadd(aDados, { 	(cArqTmp)->(PREFIXO+NUMERO+PARCELA+BM1_TIPTIT)+DtoS((cArqTmp)->VENCTO),;
							(cArqTmp)->BA0_SUSEP,;
							(cArqTmp)->BA0_NOMINT,;
							DtoS((cArqTmp)->VENCTO),;
							cTpCob,;
							nValor,;
							'R' } )				
		Else
			aDados[nPos,6] += nValor
		EndIf

	Next nVar

	(cArqTmp)->(dbSkip())

EndDo

// Limpa os arrays
aSize(aFields,1)
aDel(aFields,1)
// Fecha os temporarios
(cArqTmp)->(dbCloseArea())
(cTITTMP)->(dbCloseArea())

Return({ ( Len(aDados)>0 ), aDados })



/*/{Protheus.doc} getTrbQuery
Executa query 

@author RogerC
@since 19/10/2017
@version ALL
/*/
Function PlDIntP(lTela,dDatDe,dDatAte)
local lRet      := .f.
Local aFields := {}
Local oTempTable
Local oTempTabl2
Local nI		:= 0
Local cQuery
LOCAL nQryfile := 0
Local aPtoQuery:= {}
Local aPtoTpDoc:= {}
Local nPos	:= 0
Local aRecTit	:= 0
Local cWhere	:= ''
Local aSetField := {}

local nTFilial	:= tamSX3("E1_FILIAL")[1]
local nPFilial	:= 1

local nTPrefixo	:= tamSX3("E1_PREFIXO")[1]
local nPPrefixo	:= nPFilial + nTFilial + 1

local nTNumero	:= tamSX3("E1_NUM")[1]
local nPNumero	:= nPPrefixo + nTPrefixo + 1

local nTParcela	:= tamSX3("E1_PARCELA")[1]
local nPParcela	:= nPNumero + nTNumero +  1

local nTTipo	:= tamSX3("E1_TIPO")[1]
local nPTipo	:= nPParcela + nTParcela + 1

local nTCliente	:= tamSX3("E1_CLIENTE")[1]
local nPCliente	:= nPTipo + nTTipo + 1

local nTLoja	:= tamSX3("E1_LOJA")[1]
local nPLoja	:= nPCliente + nTCliente + 1

Local lMsSql	:= Upper(TcGetDb()) $ "MSSQL"

local lDtCTB	:= getNewPar("MV_PLDTREC", '0') == '0'

Local nRecTr1	:= 0
Local nCount	:= 0
Local nVar		:= 0
Local nVez		:= 0

Local aContas	:= {}
// Array default das contas do DIOPS
Local aDefDiops	:= {'211119033',;
					'211129033' }

Local nCta		:= 0
Local cConta	:= ''
Local aDados	:= {}

Local lBD7_TPEVCT:= BD7->(fieldPos('BD7_TPEVCT')) > 0

Local cArqTmp	:= GetNextAlias()
Local cTITTMP	:= GetNextAlias()
Local cNotIn	:= RetLocIgn()

Default lTela	:= .T.
Default dDatDe	:= CtoD('01/01/1900')
Default dDatAte	:= dDataBase 

// Ser� considerado sempre o �ltimo dia do m�s da data que for enviada por par�metro
dDatAte	:= LastDay(dDatAte) 
					
// Loop de colunas para preenchimento do array principal de valores do DIOPS
For nVar := 1 to 2
	cStr	:= GetNewPar('MV_PLIEP'+AllTrim(StrZero(nVar,2)), aDefDiops[nVar] )
	For nPos := 1 to Len(cStr)
		If Subs(cStr,nPos,1) == ','
			aAdd(aContas, { cConta, nVar } )
			cConta := '' 
		Else
			cConta := cConta + Subs(cStr,nPos,1)
		EndIf   		
	Next nPos
	If !Empty(cConta)
		aAdd(aContas, { cConta, nVar } )
		cConta := '' 
	EndIf
Next nVar

// Query principal na Composi��o dos Itens da Guia
cQuery	:= " SELECT BD7_TIPGUI,BD7_DTDIGI,BD7_CODRDA,BD7_CODOPE,BD7_CODLDP,BD7_CODPEG,BD7_NUMERO,BD7_OPEUSR,BD7_CODEMP,"
cQuery	+= "BD7_CONEMP,BD7_MATRIC,BD7_TIPREG,"
If lBD7_TPEVCT
	cQuery	+= "BD7_TPEVCT,BD7_DATPRO,"
Else
	cQuery	+= "BD7_CODPAD,BD7_CODPRO,BD7_DATPRO,"
EndIf
cQuery	+= "BD7_FASE,MIN(BD7.R_E_C_N_O_) AS BD7Recno, "
cQuery	+= "SUM(BD7_VLRPAG) AS BD7TVLRPAG, "
cQuery	+= "BA0_NOMINT, BA0_SUSEP, "
cQuery	+= "SE2.R_E_C_N_O_ AS SE2Recno, " 
cQuery	+= "E2_PREFIXO, E2_NUM, E2_PARCELA, E2_TIPO "
cQuery += " FROM " + PLSSQLNAME("SE2") + " SE2 " 

// Composi��o dos Itens da Guia		
cQuery += " INNER JOIN " + PLSSQLNAME("BD7") + " BD7 "
cQuery += "    ON BD7_FILIAL = '" + xFilial("BD7") + "' "
cQuery += "   AND BD7_CHKSE2 = E2_FILIAL || '|' || E2_PREFIXO || '|' || E2_NUM || '|' || E2_PARCELA || '|' || E2_TIPO || '|' || E2_FORNECE || '|' || E2_LOJA "
cQuery += "   AND BD7_SITUAC <> '2' " // 1 - Ativo / 2 - Cancelado / 3 - Bloqueado
cQuery += "   AND BD7_DTDIGI  <= '" + DtoS(dDatAte) + "' "
If !Empty(cNotIn)
	cQuery += "   AND BD7_CODLDP NOT IN" + cNotIn + " "
EndIf
cQuery += "   AND BD7.D_E_L_E_T_ = ' ' "

// Itens da Guia
cQuery	+= "INNER JOIN "+RetSqlName("BD6")+" BD6 "
cQuery	+= "ON BD6_FILIAL='"+xFilial('BD6')+"' "
cQuery	+= "AND BD6_CODOPE=BD7.BD7_CODOPE "
cQuery	+= "AND BD6_CODLDP=BD7.BD7_CODLDP "
cQuery	+= "AND BD6_CODPEG=BD7.BD7_CODPEG "
cQuery	+= "AND BD6_NUMERO=BD7.BD7_NUMERO "
cQuery	+= "AND BD6_SEQUEN=BD7.BD7_SEQUEN "
cQuery	+= "AND BD6_CODPAD = BD7.BD7_CODPAD "
cQuery	+= "AND BD6_CODPRO = BD7.BD7_CODPRO "
cQuery	+= "AND BD6_MESPAG = BD7_MESPAG "
cQuery	+= "AND BD6_ANOPAG = BD7_ANOPAG "
cQuery	+= "AND BD6.D_E_L_E_T_ = '' "

// Contrato
cQuery += "INNER JOIN "+RetSqlName("BT5")+" BT5 "
cQuery += "ON BT5_FILIAL='"+xFilial('BT5')+"' "
cQuery += "  AND BT5_CODINT=BD7.BD7_OPEUSR "
cQuery += "  AND BT5_CODIGO=BD7.BD7_CODEMP "
cQuery += "  AND BT5_NUMCON=BD7.BD7_CONEMP "
cQuery += "  AND BT5_VERSAO=BD7.BD7_VERCON "
cQuery += "  AND BT5_INTERC = '1' "
cQuery += "  AND BT5_INFANS = '1' "
cQuery += "  AND BT5.D_E_L_E_T_='' "

// Operadora
cQuery += "INNER JOIN "+RetSqlName("BA0")+" BA0 "
cQuery += "ON BA0_FILIAL='"+xFilial("BA0")+"' "
cQuery += " AND BA0_CODIDE= '0' "
If Upper(TcGetDb()) $ "ORACLE,POSTGRES,DB2,INFORMIX"		
	cQuery += " AND BA0_CODINT = DECODE(BT5.BT5_ALLOPE, '1', '"+Subs(PlsIntPad(),2,3)+"', BT5.BT5_OPEINT ) "
Else
	cQuery += " AND BA0_CODINT = ("
	cQuery += "CASE WHEN BT5.BT5_ALLOPE = '1' THEN '"+Subs(PlsIntPad(),2,3)+"' "
	cQuery += "ELSE BT5.BT5_OPEINT END) "	
EndIf
cQuery += "  AND BA0.D_E_L_E_T_='' "

cQuery += " WHERE E2_FILIAL  = '" + xFilial("SE2") + "' "	
cQuery += "   AND E2_TIPO NOT IN " + formatIn(MVABATIM+"|"+MVIRABT+"|"+MVINABT,"|") //AB-|FB-|FC-|FU-|IR-|IN-|IS-|PI-|CF-|CS-|FE-|IV-//IR-//IN- 
cQuery += "   AND E2_EMIS1 <= '" + dtoS(dDatAte) + "' "
cQuery += "   AND ( E2_SALDO > 0 OR E2_BAIXA > '" + dtoS(dDatAte) + "' ) "
cQuery += "   AND SE2.D_E_L_E_T_ = ' ' "
cQuery += "   AND BD7_FILIAL = '" + xFilial("BD7") + "' "
cQuery += "   AND BD7_SITUAC <> '2' " // 1 - Ativo / 2 - Cancelado / 3 - Bloqueado
cQuery += "   AND BD7_BLOPAG <> '1' "	// Pagamento liberado 
cQuery += "   AND BD7_DTDIGI  > ' ' "
cQuery += "   AND BD7_DTDIGI  <= '" + DtoS(dDatAte) + "' "
If !Empty(cNotIn)
	cQuery += "   AND BD7_CODLDP NOT IN" + cNotIn + " "
EndIf
cQuery += "   AND BD7.D_E_L_E_T_ = ' ' "

cQuery	+= " GROUP BY BA0_SUSEP, BA0_NOMINT, E2_PREFIXO, E2_NUM, E2_PARCELA, E2_TIPO, SE2.R_E_C_N_O_, BD7_TIPGUI,BD7_DTDIGI,BD7_CODRDA,"
cQuery	+= "BD7_CODOPE,BD7_CODLDP,BD7_CODPEG,BD7_NUMERO,BD7_OPEUSR,BD7_CODEMP,BD7_CONEMP,BD7_MATRIC,BD7_TIPREG,BD7_FASE,"
If lBD7_TPEVCT
	cQuery	+= "BD7_TPEVCT,BD7_DATPRO,"
Else
	cQuery	+= "BD7_CODPAD,BD7_CODPRO,BD7_DATPRO,"
EndIf
cQuery	+= "BA0_NOMINT, BA0_SUSEP "

cQuery	+= " ORDER BY BA0_SUSEP, BA0_NOMINT, SE2Recno,BD7_TIPGUI,BD7_DTDIGI,BD7_CODRDA,BD7_CODOPE,BD7_CODLDP,"
cQuery	+= "BD7_CODPEG,BD7_NUMERO,BD7_OPEUSR,BD7_CODEMP,BD7_CONEMP,BD7_MATRIC,BD7_TIPREG,"
If lBD7_TPEVCT
	cQuery	+= "BD7_TPEVCT,BD7_DATPRO  "
Else
	cQuery	+= "BD7_CODPAD,BD7_CODPRO,BD7_DATPRO  "
EndIf

nHandle := fCreate('QRYDINTP.sql', 0)
fWrite(nHandle, CHR(13)+CHR(10)+cQuery+CHR(13)+CHR(10) )
fClose(nHandle)

MPSysOpenQuery( ChangeQuery(cQuery), cArqTmp )
TCSetField(cArqTmp,"BD7TVLRPAG","N",16,2)
TCSetField(cArqTmp,"VENCTO","D",08,0)

//-------------------
//Cria��o da Temporaria para Evitar recalculo de saldo do Titulo
//-------------------
aFields := {}
aadd(aFields,{"CHAVE","C",40,0})
aadd(aFields,{"VALOR1","N",16,2})
aadd(aFields,{"VALOR2","N",16,2})
oTempTabl2 := FWTemporaryTable():New( cTITTMP, aFields )
oTempTabl2:AddIndex("INDCHV", {"CHAVE"} )
//------------------
//Cria��o da tabela
//------------------
oTempTabl2:Create()

If lTela
	(cArqTmp)->(dbGotop())
	(cArqTmp)->(dbEval({|| nRecTr1++ }))
	ProcRegua((cArqTmp)->(RecCount()))	
EndIf

(cArqTmp)->(dbGotop())
While !(cArqTmp)->(EOF())
//	BM1->(dbGoTo((cArqTmp)->BM1REG))
	If lTela
		nCount ++
		IncProc("Foram encontrados " + alltrim(str(nRecTr1)) + " registros. Processando registro " + alltrim(str(nCount) ))
	EndIf

	// Se n�o achou contabiliza��o, pula o registro
	If !CV3->(msSeek(xFilial('CV3')+'BD7'+AllTrim(Str((cArqTmp)->BD7Recno)) ) )
		(cArqTmp)->(dbSkip())
		Loop
	Else
		lCT2Ativo 	:= .F.
		cLastReg 	:= allTrim( str( (cArqTmp)->BD7Recno ) )
		While !lCT2Ativo .and. !CV3->(eof()) .and. allTrim(CV3->CV3_RECORI) == cLastReg
			CT2->( msGoTo( val( CV3->CV3_RECDES ) ) )
			if CT2->(deleted())
				CV3->(dbSkip())
				loop
			else
				lCT2Ativo 	:= .t.
				Exit
			endIf
		endDo

		If !lCT2Ativo
			(cArqTmp)->(dbSkip())
			Loop
		EndIf
		
	EndIf		

	If !(cTITTMP)->(dbSeek((cArqTmp)->(E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO)+DTOS(dDatAte),.F.) )
		// Fun��o que retona o Valor Recebido e Valor Total, mesmo que haja renegocia��o de t�tulos
		aRecTit := PLRETVLRTOT((cArqTmp)->(E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO),dDatAte)
		// aRecTit[1]	// Valor Recebido 	= E1_VALOR - E1_SALDO
		// aRecTit[2]	// Valor Total		= E1_VALOR
		RecLock(cTITTMP,.T.)
		(cTITTMP)->CHAVE	:= (cArqTmp)->(E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO)+DTOS(dDatAte)
		(cTITTMP)->VALOR1	:= aRecTit[1]
		(cTITTMP)->VALOR2	:= aRecTit[2]
		(cTITTMP)->(msUnlock())
	Else
		aRecTit := { (cTITTMP)->VALOR1, (cTITTMP)->VALOR2 }
	EndIf
	// Se n�o houver valor a receber, n�o deve sair no relat�rio.
	If aRecTit[1] == aRecTit[2] 
		(cArqTmp)->(dbSkip())
		Loop
	EndIf		

	// Se Debito
	If CV3->CV3_DC == '1'
		cConta	:= CV3->CV3_DEBITO
		nVez	:= 1
	// Se Credito
	ElseIf CV3->CV3_DC == '2'
		cConta	:= CV3->CV3_CREDIT
		nVez	:= 1
	// Se Partida Dobrada
	ElseIf CV3->CV3_DC == '3'
		cConta	:= CV3->CV3_DEBITO		
		cConta2	:= CV3->CV3_CREDIT
		nVez	:= 2
	Else
		(cArqTmp)->(dbSkip())
		Loop
	EndIf
	
	For nVar := 1 to nVez
	
		cConta	:= AllTrim(IIf(nVar==1,cConta,cConta2))		
		For nCta := Len(cConta) to 1 Step -1
			nPos := aScan(aContas, {|x| x[1] == Subs(cConta,1,nCta)} )
			// Se achou, sai do loop
			If nPos > 0 
				Exit
			EndIf
		Next nCta
		
		// Se nao achou, pula registro
		If nPos == 0
			Loop
		EndIf
	
		// TIPO DE COBERTURA - HOSPITALAR OU ODONTOLOGICA - CONFORME A CONTA CONTABIL
		If aContas[nPos,2] == 1
			cTpCob	:= "H"
		Else
			cTpCob	:= "O"
		EndIf
	
		nValor := ((cArqTmp)->VALOR/aRecTit[2]) * ( aRecTit[2] - aRecTit[1] ) 
	
		If ( nPos := aScan( aDados, { |x| x[1] == (cArqTmp)->(E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO)+DtoS((cArqTmp)->VENCTO) } ) ) == 0  
			aadd(aDados, { 	(cArqTmp)->(E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO)+DtoS((cArqTmp)->VENCTO),;
							(cArqTmp)->BA0_SUSEP,;
							(cArqTmp)->BA0_NOMINT,;
							DtoS((cArqTmp)->VENCTO),;
							cTpCob,;
							nValor,;
							'R' } )
		Else
			aDados[nPos,6] += nValor
		EndIf

	Next nVar

	(cArqTmp)->(dbSkip())

EndDo

// Limpa os arrays
aSize(aFields,1)
aDel(aFields,1)
// Fecha os temporarios
(cArqTmp)->(dbCloseArea())
(cTITTMP)->(dbCloseArea())

Return({ ( Len(aDados)>0 ), aDados })


