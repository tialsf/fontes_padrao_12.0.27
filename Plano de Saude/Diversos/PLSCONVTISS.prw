#Include "protheus.ch"
#include "FILEIO.CH

#Define CRLF Chr(10) + Chr(13)

//---------------------------------------
/*/{Protheus.doc} PLSCONVTIS 
Tela da importa��o
@author  Lucas Nonato 
@version P11 
@since   13/06/2016
/*/ 
//---------------------------------------
function PLSCONVTIS
local cDirOri 	:= space(100)
local cVerTiss	:= PLSTISSVER()
	
local aTags		:= {{'BTQ_DSCDET','Termo,Grupo,Descri��o do grupo'},;
					{'BTQ_CDTERM','C�digo,Codigo,C�digo do Termo,Terminologia,C�digo da Tabela'},;					
					{'BTQ_VIGDE','Data de in�cio de vig�ncia','Data de inicio de vigencia'},;
					{'BTQ_VIGATE','Data de fim de vig�ncia'},;
					{'BTQ_DATFIM','Data de fim de implanta��o','Data de fim de implantacao'},;
					{'BTQ_FABRIC','Fabricante'},;
					{'BTQ_REFFAB','Refer�ncia no fabricante'},;					
					{'BTQ_APRESE','Apresenta��o'},;
					{'BTQ_LABORA','Laborat�rio'},;
					{'BTQ_SIGLA' ,'Sigla'},;
					{'BTQ_CDTERM','TUSS,C�digo TUSS,Codigo TUSS'},;
					{'BTQ_CODGRU','C�digo do grupo'},;
					{'BTQ_DSCDET','Descri��o Detalhada do Termo'},;
					{'BTQ_FENVIO','Forma de envio'}}

local aPergs	:= {}
local __aRet	:= {}
private cRet	:= ''
private oProcess

aadd(/*01*/ aPergs,{ 6,"Caminho CSV",cDirOri,"@!","","",90,.t.,,,GETF_LOCALHARD + GETF_LOCALFLOPPY + GETF_RETDIRECTORY })
aadd(/*02*/ aPergs,{ 1,"Vers�o TISS",cVerTiss,"@R 9.99.99",'.t.',,/*'.t.'*/,7,.t. } )

if( paramBox( aPergs,"Par�metros - Importa��o Terminologia TISS",__aRet,/*bOK*/,/*aButtons*/,.f.,/*nPosX*/,/*nPosY*/,/*oDlgWizard*/,/*cLoad*/'PLSCTISS',/*lCanSave*/.t.,/*lUserSave*/.t. ) )
	cIni := time()
	oProcess := MsNewProcess():New( { || ProcTiss(__aRet[1],__aRet[2],aTags) } , "Processando" , "Aguarde..." , .f. )
	oProcess:Activate()
	cFim := time()
	Aviso( "Resumo","Processamento finalizado. " + CRLF + 'Inicio: ' + cvaltochar( cIni ) + "  -  " + 'Fim: ' + cvaltochar( cFim ) ,{ "Ok" }, 2 )
endif

return 

//---------------------------------------
/*/{Protheus.doc} ProcTiss 
Processamento do csv e grava��o nas tabelas da terminologia. 
@author  Lucas Nonato 
@version P11 
@since   13/06/2016
/*/
//---------------------------------------

static function ProcTiss(cDirOri,cVerTiss,aTags)
local aFiles 	:= ""
local nArq		:= 0
local aCabec	:= {}
local aItens 	:= {}
local nVazio 	:= 0 //Variavel para controle de linhas vazias, pois com a convers�o de XLS para CSV, foram geradas mais de 1k de linhas vazias.
local cTab 		:= ""
local cDesc		:= ""
local cAntes	:= ""
local cAlt 		:= ""
local cDepois	:= ""
local cTabCab		:= SuperGetMv("MV_TISSCAB",.f.,"87")
local oFileRead As Object
local aLines 	:= {}
local nLoop 	:= 0
local cLine 	:= ""
local cItem		:= ""
local lMesmaLinha := .f.
local lGrava	:= .T.

BTQ->(DbSetOrder(2))
BTP->(DbSetOrder(1))

cDirOri 	:= AllTrim(cDirOri)
aFiles := (Directory(cDirOri + '\*.CSV'))

if Len(aFiles) == 0
	MsgStop('Nenhum arquivo .CSV encontrado em ' + cDirOri)
	return PLSCONVTIS()
endif

oProcess:SetRegua1(Len(aFiles)) 
For nArq := 1 To Len(aFiles)
	oFileRead := FWFileReader():New( cDirOri +'\'+ aFiles[nArq][1] )
	nVazio	:= 0
	cTab 	:= ""
	cDesc	:= ""
	aCabec	:= {}
	aItens	:= {}
	if oFileRead:Open()
 		aLines 	:= oFileRead:GetAllLines()
		nLoop 	:= 1
		cTab	:= ""
		aCabec	:= {}
		nVazio	:= 1
		oProcess:IncRegua1("Processando Arquivos")
		oProcess:SetRegua2(Len( aLines )) //Alimenta a segunda barra de progresso
		while nLoop <= Len( aLines ) .and. nVazio <= 15
			lGrava := .T. //Retorna valor default
			aItens := {} 
			oProcess:IncRegua2("Lendo Tabela: " + cTab)
			cLine := aLines[ nLoop ]
			if Substr(cLine,1,1) == ';'	
				nVazio += 1	
			elseif empty(cTab) .and. 'ndice' $ AllTrim(cLine)  
				nVazio := 15				
			elseif 'Tabela' $ AllTrim(cLine) .and. empty(cTab)
				cTab := AllTrim(cvaltochar(val(Substr(cLine,8,3))))
				cTab := Iif(Len(cTab) == 1, '0' + cTab, cTab)	
			elseif Len(aCabec) == 0 
				if !empty(cTab)						
					aCabec := Separa(cLine,";",.t.) 
				endif				
			elseif !empty(cTab) .and. len(aCabec) <> 0				
				if len(separa(cLine,";",.t.)) <> len(aCabec)
					lMesmaLinha	:= .t.				
					while lMesmaLinha
						cLine := aLines[ nLoop ]
						cItem += cLine
						nLoop++
						if (substr(aLines[ nLoop ],1,1) $ "0123456789" .and. substr(aLines[ nLoop ],8,1) $ "0123456789") .or. (nLoop == len(aLines))
							lMesmaLinha := .f.
							nLoop--
						endif
					enddo
					while '"' $ cItem   
						cAntes 	:= substr(cItem,1,at('"',cItem)-1)
						cItem 	:= substr(cItem,at('"',cItem)+1,len(cItem))
						cAlt 	:= strtran(substr(cItem,1,at('"',cItem)-1),";","")
						cDepois := substr(cItem,at('"',cItem)+1,len(cItem))
						cItem	:= cAntes + cAlt + cDepois
					enddo
					aItens := Separa(cItem,";",.t.)
					cItem := ""
				else
					cAntes 	:= substr(cLine,1,at('"',cLine)-1)
					cLine 	:= substr(cLine,at('"',cLine)+1,len(cLine))
					cAlt 	:= strtran(substr(cLine,1,at('"',cLine)-1),";","")
					cDepois := substr(cLine,at('"',cLine)+1,len(cLine))
					cLine	:= cAntes + cAlt + cDepois
					aItens := Separa(cLine,";",.t.)
				endif
				
			endif

			//Valida��o para n�o incluir Terminologias 00 87 90 e 98
			lGrava := Iif(cTab=="00" .or. cTab=="87" .or. cTab=="90" .or. cTab=="98",.F.,.T.) 

			if len(aItens) > 0 .and. lGrava 
				if cTab == cTabCab
					gravaBTP(cTab,aCabec,aItens)
				endif
				gravaBTQ(cTab,aCabec,aItens)
			endif
			nLoop++				
	 	enddo nLoop
	
		oFileRead:Close()
		FWFreeVar( @oFileRead ) 		
	else
		msginfo( "Nao foi possivel abrir o arquivo: " + oFileRead:error:ToString() )
	endif
Next

return

//---------------------------------------
/*/{Protheus.doc} getValue 
Retorna o valor a ser gravado de acordo com o cabe�alho
@author  Lucas Nonato 
@version P11 
@since   13/06/2016
/*/
//---------------------------------------

static function getValue(cTit, aCabec, aItens)
local cRet 	:= ""
local nPos	:= 0

nPos := aScan(aCabec,{|x| cTit $ x })
if nPos > 0
	cRet :=  aItens[nPos]
else
	nPos := aScan(aCabec,{|x| x $ cTit })
	if nPos > 0
		cRet :=  aItens[nPos]
	endif	
endif

return cRet

static function RetPonto(cExp)

// Corrige campos...
cExp := StrTran(cExp,"#","")                                       
cExp := StrTran(cExp,"&","")  
cExp := StrTran(cExp,"�","")  
cExp := StrTran(cExp,"$","")  
cExp := StrTran(cExp,"'","")  
cExp := StrTran(cExp,"�","") 

return(cExp)

//---------------------------------------
/*/{Protheus.doc} gravaBTP 
Grava tabela BTP
@author  Lucas Nonato 
@version P11 
@since   13/06/2016
/*/
//---------------------------------------
static function gravaBTP(cTab,aCabec,aItens)
local lInclui	:= .t.
local cDataIni		:= getValue("Data de in�cio de vig�ncia, Data de inicio de vigencia", aCabec, aItens)

if BTP->(MsSeek(xFilial("BTP")+cTab+cDataIni))
	lInclui := .f.
else
	lInclui := .t.
endif

BTP->(RecLock("BTP", lInclui))
	BTP->BTP_FILIAL := xFilial("BTP")
	BTP->BTP_CODTAB := cTab
	BTP->BTP_DESCRI := getValue("Descri��o,Desc,Descricao", aCabec, aItens)
	BTP->BTP_VIGDE  := ctod(cDataIni)
	BTP->BTP_VIGATE := ctod(getValue("Data de fim de vig�ncia, Data de fim de vig", aCabec, aItens))
	BTP->BTP_DATFIM := ctod(getValue("Data de fim de implanta��o, Data de fim de implanta", aCabec, aItens))
	BTP->BTP_TIPVIN := '0'
BTP->(dbCommit())
BTP->(MsUnlock())

return

//---------------------------------------
/*/{Protheus.doc} gravaBTQ
Grava tabela BTQ
@author  Lucas Nonato 
@version P11 
@since   13/06/2016
/*/
//---------------------------------------
static function gravaBTQ(cTab,aCabec,aItens)
local lInclui	:= .t.
local cCode		:= getValue("TUSS,C�digo TUSS,Codigo TUSS", aCabec, aItens)

if empty(cCode)
	cCode := getValue("C�digo,Codigo,C�digo do Termo,Terminologia,C�digo da Tabela", aCabec, aItens	)
endif

if BTQ->(MsSeek(xFilial("BTQ")+cTab+PadR(cCode,len(BTQ->BTQ_CDTERM))))
	lInclui := .f.
else
	lInclui := .t.
endif

BTQ->(RecLock("BTQ", lInclui))
	BTQ->BTQ_FILIAL := xFilial("BTQ")
	BTQ->BTQ_CODTAB := cTab
	BTQ->BTQ_CDTERM	:= cCode
	BTQ->BTQ_DESTER := FwNoAccent(getValue("Termo,Grupo,Descri��o do grupo", aCabec, aItens))				
	BTQ->BTQ_VIGDE 	:= ctod(getValue("Data de in�cio de vig�ncia, Data de inicio de vigencia", aCabec, aItens))
	BTQ->BTQ_VIGATE := ctod(getValue("Data de fim de vig�ncia", aCabec, aItens))
	BTQ->BTQ_DATFIM := ctod(getValue("Data de fim de implanta��o Data de fim de implantacao", aCabec, aItens))
	BTQ->BTQ_FABRIC := getValue("Fabricante", aCabec, aItens)
	BTQ->BTQ_REFFAB := getValue("Refer�ncia no fabricante", aCabec, aItens)
	BTQ->BTQ_APRESE := getValue("Apresenta��o", aCabec, aItens)
	BTQ->BTQ_LABORA := getValue("Laborat�rio", aCabec, aItens)
	BTQ->BTQ_SIGLA  := getValue("Sigla", aCabec, aItens)
	BTQ->BTQ_CODGRU := getValue("C�digo do grupo", aCabec, aItens)
	BTQ->BTQ_DSCDET := RetPonto(getValue("Descri��o Detalhada do Termo", aCabec, aItens))
	BTQ->BTQ_FENVIO := getValue("Forma de envio", aCabec, aItens)
	BTQ->BTQ_HASVIN := '0'
BTQ->(dbCommit())
BTQ->(MsUnlock())

return

//---------------------------------------
/*/{Protheus.doc} PLSIMPTERM 
Chama function ProcTiss
@author Eduardo Bento
@version P11 
@since   07/01/2020
/*/
//---------------------------------------
Function PLSIMPTERM(cDirOri, cVerTiss)

	ProcTiss(cDirOri, cVerTiss)

Return