﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace TISSExportTableDomain
{
    public class DalBase
    {
        /// <summary>
        /// Metodo que consulta um arquivo excel e carrega em um DataTable.
        /// </summary>
        /// <param name="fileName">Caminho do arquivo .xlsx</param>
        /// <returns></returns>
        public DataSet getDataSetFromExcel(string fileName)
        {
            OleDbConnection connection = null;
            OleDbDataAdapter da = null;
            DataSet dsResult = new DataSet("TabelasDominioTISS");

            try
            {
                string str = string.Empty;
                string select = string.Empty;
                DataTable dtTabDomain = null;


                if (fileName.Trim().EndsWith(".xlsx"))
                    str = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", fileName);
                else if (fileName.Trim().EndsWith(".xls"))
                    str = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";", fileName);
                else
                    return null;
                
                connection = new OleDbConnection(str);
                connection.Open();

                //DataTable com todas as planilhas do excel
                DataTable dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                List<TableType> lstTableType = Enum.GetValues(typeof(TableType)).Cast<TableType>().ToList();

                lstTableType.ForEach(tableType =>
                    {
                        string tableID = tableType.ToString().Replace("Item", string.Empty);

                        if (tableID == "87")
                            select = "SELECT * FROM [Tab 87 - Lista de terminologias$]";
                        else
                            select = string.Format("SELECT * FROM [Tab {0}$]", tableID);

                        da = new OleDbDataAdapter(select, connection);

                        dtTabDomain = new DataTable(string.Format("Table{0}", tableID));
                        da.Fill(dtTabDomain);

                        dsResult.Tables.Add(dtTabDomain);
                    });
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (da != null)
                    da.Dispose();
            }
            return dsResult;
        }

        public string GenerateComplexTypeXML(object complexType)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(complexType.GetType());
            
            MemoryStream memoryStream = new MemoryStream();
            
            XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.GetEncoding("ISO-8859-1"));
            
            xmlTextWriter.Formatting = Formatting.Indented;
            
            xmlSerializer.Serialize(xmlTextWriter, complexType);
            
            xmlTextWriter.Flush();

            StreamReader streamReader = new StreamReader(memoryStream, System.Text.Encoding.GetEncoding("ISO-8859-1"));
            
            streamReader.BaseStream.Position = 0;

            return streamReader.ReadToEnd();
        }
    }
}
