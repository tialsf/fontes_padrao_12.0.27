﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace TISSExportTableDomain
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        #region --- EVENTOS ---

        private void btnExport_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFile.Text.Trim()))
            {
                MessageBox.Show("Arquivo excel não selecionado.", "Aviso");
                return;
            }

            if (string.IsNullOrEmpty(txtPath.Text.Trim()))
            {
                MessageBox.Show("Destino da exportação não selecionado.", "Aviso");
                return;
            }

            EnabledFields(false);
            Export();
            EnabledFields(true);

            MessageBox.Show("Exportação realizada com sucesso!", "Exportação Finalizada");
        }

        /// <summary>
        /// Procura o arquivo .xmlx
        /// </summary>
        private void btnSearchFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Selecione o arquivo excel da ANS para exportação";
            openFileDialog1.Multiselect = false;
            openFileDialog1.Filter = "Excel |*.xlsx;*.xls";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtFile.Text = openFileDialog1.FileName;
            }
        }

        /// <summary>
        /// Procura a pasta de destino da exportação
        /// </summary>
        private void txtSearchPath_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.Description = "Selecione a pasta de destino da exportação";
            folderBrowserDialog1.ShowNewFolderButton = true;

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtPath.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        #endregion

        #region --- PRIVATE METHODS ----

        /// <summary>
        /// Método que seleciona as tabelas do Excel da TISS e cria os XMLs exportados
        /// </summary>
        private void Export()
        {
            SetProgress("Copiando dados do Excel...");

            DalBase dalBase = new DalBase();

            //seleciona todas as tabelas de dominio do excel
            DataSet ds = dalBase.getDataSetFromExcel(txtFile.Text.Trim());

            if (ds == null)
                MessageBox.Show("Arquivo excel não identificado.");

            //lista das tabelas de dominio para exportar
            List<DomainTable> lstDomainTable = new List<DomainTable>();

            SetProgress("Percorrendo as Tabelas de Domínio...");

            //percorre as tabelas de dominio
            foreach (DataTable dtTableDomain in ds.Tables)
            {
                decimal tableId = 0;
                decimal.TryParse(dtTableDomain.TableName.Substring(dtTableDomain.TableName.Length - 2, 2), out tableId); //decimal.Parse(dtTableDomain.TableName.Substring(dtTableDomain.TableName.Length - 2, 2));

                SetProgress(string.Format("Exportando a tabela {0}", tableId.ToString()));

                List<DomainTableItem> lstData = new List<DomainTableItem>();

                bool findData = false;

                #region --- POPULA A TEBELA DE DOMÍNIO

                if (tableId == 18 || tableId == 22 || tableId == 60)
                {
                    #region 18 - 22 - 60
                    //percorre o dataTable da Tabela de Domínio
                    foreach (DataRow dr in dtTableDomain.Rows)
                    {
                        if (findData)
                        {
                            if (!string.IsNullOrEmpty(dr[0].ToString()))
                            {
                                DomainTableItem data = new DomainTableItem();
                                data.Code = dr[0].ToString();
                                data.Description = RemoveSpecialCharacter(dr[1].ToString());
                                data.DetailedDescription = RemoveSpecialCharacter(dr[2].ToString());

                                if (!string.IsNullOrEmpty(dr[3].ToString()))
                                    data.DateStartValidity = DateTime.Parse(dr[3].ToString());
                                if (!string.IsNullOrEmpty(dr[4].ToString()))
                                    data.DateEndValidity = DateTime.Parse(dr[4].ToString());
                                if (!string.IsNullOrEmpty(dr[5].ToString()))
                                    data.DateEndDeployment = DateTime.Parse(dr[5].ToString());

                                lstData.Add(data);
                            }
                        }
                        else
                            findData = dr[0].ToString() == "Código do Termo";
                    }
                    #endregion
                }
                else if (tableId == 19)
                {
                    #region 19
                    //percorre o dataTable da Tabela de Domínio
                    foreach (DataRow dr in dtTableDomain.Rows)
                    {
                        if (findData)
                        {
                            if (!string.IsNullOrEmpty(dr[0].ToString()))
                            {
                                DomainTableItem data = new DomainTableItem();
                                data.Code = dr[0].ToString();
                                data.Description = RemoveSpecialCharacter(dr[1].ToString());
                                data.ReferenceManufacturer = RemoveSpecialCharacter(dr[2].ToString());
                                data.Manufacturer = RemoveSpecialCharacter(dr[3].ToString());

                                if (!string.IsNullOrEmpty(dr[4].ToString()))
                                    data.DateStartValidity = DateTime.Parse(dr[4].ToString());
                                if (!string.IsNullOrEmpty(dr[5].ToString()))
                                    data.DateEndValidity = DateTime.Parse(dr[5].ToString());
                                if (!string.IsNullOrEmpty(dr[6].ToString()))
                                    data.DateEndDeployment = DateTime.Parse(dr[6].ToString());

                                lstData.Add(data);
                            }
                        }
                        else
                            findData = dr[0].ToString() == "Código do Termo";
                    }
                    #endregion
                }
                else if (tableId == 20)
                {
                    #region 20
                    //percorre o dataTable da Tabela de Domínio
                    foreach (DataRow dr in dtTableDomain.Rows)
                    {
                        if (findData)
                        {
                            if (!string.IsNullOrEmpty(dr[0].ToString()))
                            {
                                DomainTableItem data = new DomainTableItem();
                                data.Code = dr[0].ToString();
                                data.Description = RemoveSpecialCharacter(dr[1].ToString());
                                data.Presentation = RemoveSpecialCharacter(dr[2].ToString());
                                data.Laboratory = RemoveSpecialCharacter(dr[3].ToString());

                                if (!string.IsNullOrEmpty(dr[4].ToString()))
                                    data.DateStartValidity = DateTime.Parse(dr[4].ToString());
                                if (!string.IsNullOrEmpty(dr[5].ToString()))
                                    data.DateEndValidity = DateTime.Parse(dr[5].ToString());
                                if (!string.IsNullOrEmpty(dr[6].ToString()))
                                    data.DateEndDeployment = DateTime.Parse(dr[6].ToString());

                                lstData.Add(data);
                            }
                        }
                        else
                            findData = dr[0].ToString() == "Código do Termo";
                    }
                    #endregion
                }
                else if ((tableId >= 23 && tableId <= 58) ||
                    (tableId >= 61 && tableId <= 63))
                {
                    #region (>= 23 && <= 58) || (>= 61 && <= 63)
                    //percorre o dataTable da Tabela de Domínio
                    foreach (DataRow dr in dtTableDomain.Rows)
                    {
                        if (findData)
                        {
                            if (!string.IsNullOrEmpty(dr[0].ToString()))
                            {
                                DomainTableItem data = new DomainTableItem();
                                data.Code = dr[0].ToString();
                                data.Description = RemoveSpecialCharacter(dr[1].ToString());

                                if (!string.IsNullOrEmpty(dr[2].ToString()))
                                    data.DateStartValidity = DateTime.Parse(dr[2].ToString());
                                if (!string.IsNullOrEmpty(dr[3].ToString()))
                                    data.DateEndValidity = DateTime.Parse(dr[3].ToString());
                                if (!string.IsNullOrEmpty(dr[4].ToString()))
                                    data.DateEndDeployment = DateTime.Parse(dr[4].ToString());

                                lstData.Add(data);
                            }
                        }
                        else
                        {
                            findData = dr[0].ToString() == "Código do Termo" || dr[0].ToString() == "Código";
                        }
                    }
                    #endregion
                }
                else if (tableId == 59)
                {
                    #region 59
                    //percorre o dataTable da Tabela de Domínio
                    foreach (DataRow dr in dtTableDomain.Rows)
                    {
                        if (findData)
                        {
                            if (!string.IsNullOrEmpty(dr[0].ToString()))
                            {
                                DomainTableItem data = new DomainTableItem();
                                data.Code = dr[0].ToString();
                                data.Description = RemoveSpecialCharacter(dr[1].ToString());
                                data.Acronym = RemoveSpecialCharacter(dr[2].ToString());

                                if (!string.IsNullOrEmpty(dr[3].ToString()))
                                    data.DateStartValidity = DateTime.Parse(dr[3].ToString());
                                if (!string.IsNullOrEmpty(dr[4].ToString()))
                                    data.DateEndValidity = DateTime.Parse(dr[4].ToString());
                                if (!string.IsNullOrEmpty(dr[5].ToString()))
                                    data.DateEndDeployment = DateTime.Parse(dr[5].ToString());

                                lstData.Add(data);
                            }
                        }
                        else
                            findData = dr[0].ToString() == "Código do Termo";
                    }
                    #endregion
                }
                else if (tableId == 64)
                {
                    #region 64
                    //percorre o dataTable da Tabela de Domínio
                    foreach (DataRow dr in dtTableDomain.Rows)
                    {
                        if (findData)
                        {
                            if (!string.IsNullOrEmpty(dr[0].ToString()))
                            {
                                DomainTableItem data = new DomainTableItem();
                                data.Code = dr[0].ToString();
                                data.TUSSCode = dr[1].ToString();
                                data.ShippingMethod = RemoveSpecialCharacter(dr[3].ToString());
                                data.GroupCode = RemoveSpecialCharacter(dr[4].ToString());
                                data.Description = RemoveSpecialCharacter(dr[5].ToString());

                                if (!string.IsNullOrEmpty(dr[6].ToString()))
                                    data.DateStartValidity = DateTime.Parse(dr[6].ToString());
                                if (!string.IsNullOrEmpty(dr[7].ToString()))
                                    data.DateEndValidity = DateTime.Parse(dr[7].ToString());
                                if (!string.IsNullOrEmpty(dr[8].ToString()))
                                    data.DateEndDeployment = DateTime.Parse(dr[8].ToString());

                                lstData.Add(data);
                            }
                        }
                        else
                            findData = dr[0].ToString() == "Terminologia";
                    }
                    #endregion
                }
                else
                {
                    #region 87
                    //percorre o dataTable da Tabela de Domínio
                    foreach (DataRow dr in dtTableDomain.Rows)
                    {
                        if (findData)
                        {
                            if (!string.IsNullOrEmpty(dr[0].ToString()))
                            {
                                DomainTableItem data = new DomainTableItem();
                                data.Code = dr[0].ToString();
                                data.Description = RemoveSpecialCharacter(dr[1].ToString());

                                if (!string.IsNullOrEmpty(dr[2].ToString()))
                                    data.DateStartValidity = DateTime.Parse(dr[2].ToString());
                                if (!string.IsNullOrEmpty(dr[3].ToString()))
                                    data.DateEndValidity = DateTime.Parse(dr[3].ToString());
                                if (!string.IsNullOrEmpty(dr[4].ToString()))
                                    data.DateEndDeployment = DateTime.Parse(dr[4].ToString());

                                lstData.Add(data);
                            }
                        }
                        else
                            findData = dr[0].ToString() == "Código da Tabela";
                    }
                    #endregion
                }

                #endregion

                #region --- ADICIONA A TABELA DE DOMÍNIO ---

                //quantidade de itens por arquivo
                int qtdReg = !string.IsNullOrEmpty(txtQtdReg.Text.Trim()) ? int.Parse(txtQtdReg.Text.Trim()) : 0;

                //enquanto tiver itens na lista
                while (lstData.Count > 0)
                {
                    //quantidade de registros que serão pegos
                    int countRemove = qtdReg > 0 && lstData.Count > qtdReg ? qtdReg : lstData.Count;
                    //cria a Tabela de Domínio
                    DomainTable domainTable = new DomainTable();
                    //seta a versão da tiss
                    domainTable.TISSVersion = txtTISSVersion.Text.Replace(",", ".");
                    //seta o tipo da Tabela de Domínio
                    domainTable.TableType = (TableType)Enum.Parse(typeof(TableType), tableId.ToString());
                    //seleciona somente a quantidade de itens necessárias
                    domainTable.Item = lstData.GetRange(0, countRemove).ToArray();
                    //remove os itens que já utilzados
                    lstData.RemoveRange(0, countRemove);
                    //adiciona a Tabela de Dominio na lista de tabelas
                    lstDomainTable.Add(domainTable);
                }

                #endregion
            }

            SetProgress("Gerando arquivos XML.");

            #region --- CRIAR OS ARQUIVOS XML ---

            lstDomainTable.OrderBy(order => order.TableType).ToList().ForEach(domainTable =>
            {
                //gera xml da classe
                string xml = dalBase.GenerateComplexTypeXML(domainTable);
                //cria arquivo
                StreamWriter text = new StreamWriter(string.Format("{0}\\{1}-{2}.xml", txtPath.Text, domainTable.TableType.ToString(), domainTable.GetHashCode()), false, Encoding.UTF8);
                //insere o xml no arquivo
                text.Write(xml);
                //fecha conexão com o arquivo
                text.Close();
            });

            #endregion
        }

        /// <summary>
        /// Habilita ou Desabilita os campos da tela
        /// </summary>
        private void EnabledFields(bool enabled)
        {
            btnSearchFile.Enabled = txtTISSVersion.Enabled = txtQtdReg.Enabled = 
                txtSearchPath.Enabled = btnExport.Enabled = enabled;

            if (enabled)
            {
                btnExport.Text = "Exportar";
                SetProgress(string.Empty);
            }
            else
                btnExport.Text = "Aguarde...";

            Application.DoEvents();
        }

        /// <summary>
        /// Altera o label lblProgress para informar o status da Exportação
        /// </summary>
        /// <param name="text">Texto do progresso</param>
        private void SetProgress(string text)
        {
            lblProgress.Visible = !string.IsNullOrEmpty(text);
            lblProgress.Text = text;
            Application.DoEvents();
        }

        private string RemoveSpecialCharacter(string input)
        {
            if (input == null) return null;

            Regex alphaNum = new Regex("[^a-zA-Z0-9 |/(),.-]");

            MatchEvaluator matchEvaluator = new MatchEvaluator(ReplaceMonitor);

            return alphaNum.Replace(input, matchEvaluator).Trim();
        }
        private string ReplaceMonitor(Match m)
        {
            String replacement = String.Empty;

            string[] diacriticChar = new string[] { "á", "ã", "à", "é", "ê", "í", "ó", "ô", "ú", "ü", "ç" };
            string[] characterChar = new string[] { "a", "a", "a", "e", "e", "i", "o", "o", "u", "u", "c" };

            for (int i = 0; i < diacriticChar.Length; i++)
            {
                if (m.Value.Equals(diacriticChar[i]))
                {
                    replacement = characterChar[i];
                    break;
                }
                else if (m.Value.Equals(diacriticChar[i].ToUpper()))
                {
                    replacement = characterChar[i].ToUpper();
                    break;
                }
            }

            return replacement;
        }


        #endregion
    }
}
