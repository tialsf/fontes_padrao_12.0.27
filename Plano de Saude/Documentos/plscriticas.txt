Este arquivo nao deve ser vinculado a projeto de compilacao e tambem nao pode estar ligado como CH e nenhum fonte.
Aqui e um lugar unico que sera colocado os codigos e criticas padroes do pls
Sempre que for necessario usar uma nova critica do contas medicas deve ser visto o proximo codigo disponivel e adicionado nesse 
arquivo e referenciado o define NO PROPRIO FONTE QUE VAI SER CRITICADO (exemplo no plsa720.prw e plsp500.prw)

#define __aCdCri001 {"035","Usuario nao possui cobertura para este procedimento."}
#define __aCdCri002 {"001","Idade do usuario incompativel com a idade limite para o procedimento."}
#define __aCdCri003 {"002","Procedimento em carencia para este usuario."}
#define __aCdCri004 {"003","Sexo invalido para este procedimento."}
#define __aCdCri005 {"502","Unidade da Rede de atendimento bloqueada."}
#define __aCdCri006 {"503","Rede de Atendimento sem local de atendimento cadastrado."}
#define __aCdCri007 {"504","Local de atendimento invalido para o produto do usuario."}
#define __aCdCri008 {"505","Familia bloqueada."}
#define __aCdCri009 {"506","Usuario bloqueado."}
#define __aCdCri010 {"507","Operadora invalida para este usuario."}
#define __aCdCri011 {"508","Matricula do usuario: Invalida."}
#define __aCdCri012 {"509","Operadora da Rede de atendimento: Invalida."}
#define __aCdCri013 {"510","Matricula da Rede de atendimento: Invalida."}
#define __aCdCri014 {"511","Rede de atendimento nao permitida para a operadora informada."}
#define __aCdCri015 {"512","Rede de atendimento sem local de atendimento cadastrado."}
#define __aCdCri016 {"513","Rede de atendimento sem especialidade cadastrada"}
#define __aCdCri017 {"004","Critica Financeira."}
#define __aCdCri018 {"005","Procedimento em carencia para este usuario (PREEXISTENCIA)."}
#define __aCdCri019 {"514","Existe uma internacao para este usuario cuja data de alta encontra-se sem preenchimento."}
#define __aCdCri020 {"006","RDA nao autorizada a executar o procedimento (Campo Executa/Solicita/Ambos)"}
#define __aCdCri021 {"007","Procedimento bloqueado na especialidade de Rede de atendimento."}
#define __aCdCri022 {"008","Idade do usuario incompativel com a idade limite para a especialidade."}
#define __aCdCri023 {"009","Sexo invalido para a especialidade."}
#define __aCdCri024 {"010","A data do evento e anterior a data de inclusao do usuario."}
#define __aCdCri025 {"044","Nao existe calendario de pagamento para a data do evento informada OU nao existe U.S mensais para o calendario de pagamento."}
#define __aCdCri026 {"050","Abrangencia de atendimento nao permitida para este produto."}
#define __aCdCri027 {"517","Nao foi encontrada nenhuma ocorrencia para o Codigo da Tabela de Honorarios a ser utilizada."}
#define __aCdCri028 {"518","Nao existe composicao para esse procedimento."}
#define __aCdCri029 {"519","A expressao para o Calculo da US em Procedimentos Autorizados na Especialidade da RDA, nao foi informado corretamente."}
#define __aCdCri030 {"520","A expressao para o Calculo da US em Especialidades na RDA, nao foi informado corretamente."}
#define __aCdCri031 {"521","A expressao para o Calculo da US no Local de Atendimento na RDA, nao foi informado corretamente."}
#define __aCdCri032 {"540","Erro controlado SIGAPLS."}
#define __aCdCri033 {"049","Data limite para recebimento de guias de intercambio vencida."}  
#define __aCdCri034 {"524","Nao foi informado nenhum valor para a US."}
#define __aCdCri035 {"525","Nao foi informado nenhum valor para o Filme."}
#define __aCdCri036 {"526","Nao foi informado nenhum valor para o Porte Anestesico."}
#define __aCdCri037 {"527","Nao foi informado nenhum valor para o auxiliar."}
#define __aCdCri038 {"530","Digito verificador da matricula invalido"}
#define __aCdCri039 {"012","Solicitante nao autorizado a solicitar este procedimento (Campo Executa/Solicita/Ambos)"}
#define __aCdCri040 {"013","Obrigatorio informar o CID para este procedimento."}
#define __aCdCri041 {"014","Obrigatorio a informacao do medico solicitante para este tipo de guia"}
#define __aCdCri044 {"528","O Procedimento foi negado para ser executado por este prestador no local da atendimento e especialidade."}
#define __aCdCri045 {"529","A parametrizacao dos niveis de cobranca esta invalida."}
#define __aCdCri046 {"017","Limite de Quantidade ultrapassada."}
#define __aCdCri047 {"018","Limite de Periodicidade ultrapassada."}
#define __aCdCri048 {"019","Limite de Grupo de Quantidade ultrapassada."}
#define __aCdCri049 {"020","O valor contratato e diferente do valor informado/apresentado."}
#define __aCdCri050 {"021","Para este procedimento necessita Guia da Operadora."}
#define __aCdCri051 {"025","Para este procedimento necessita Auditoria."}
#define __aCdCri052 {"022","Para este procedimento necessita Guia da Empresa."}
#define __aCdCri053 {"023","Para este procedimento necessita Guia da Operadora e Empresa."}
#define __aCdCri054 {"024","Para este procedimento necessita Avaliacao Contratual."}
#define __aCdCri055 {"026","Procedimento informado nao existe na tabela padrao."}
#define __aCdCri056 {"027","Para este procedimento devera ser efetuado o pagamento do procedimento no ato da emissao da guia"}
#define __aCdCri057 {"028","Procedimento incompativel com um dos procedimentos da guia"}
#define __aCdCri058 {"029","Cid incompativel com um dos procedimentos da guia"}
#define __aCdCri059 {"531","Nao foi encontrada nenhuma tabela de pagamento para esta guia"}
#define __aCdCri060 {"030","Procedimento bloqueado pela Operadora para atendimento."}
#define __aCdCri061 {"031","limite de CH por guia emitida ultrapassado conforme parametrizacao feito no subcontrato."}
#define __aCdCri062 {"032","A Rede de Atendimento nao pode atender a usuarios deste produto."}
#define __aCdCri063 {"532","Nao foi informado nenhum valor para o Tempo Cirurgico."}
#define __aCdCri064 {"533","Gerenciador de comunicacao OnLine nao esta ativo."}
#define __aCdCri065 {"534","Time out.Operadora fora do Ar."}
#define __aCdCri066 {"535","Verificar tabela PTU"}
#define __aCdCri067 {"033","Data limite de atendimento informado na familia ultrapassado"}
#define __aCdCri068 {"034","Procedimento invalido para lancamentos como eventos (analitico/sintetico)"}
#define __aCdCri069 {"036","Foi ultrapassado o limite de CH parametrizado para autorizacao de intercambio eventual on-line."}
#define __aCdCri070 {"536","Existem campos obrigatorios que nao foram informados para esta GIH."}
#define __aCdCri071 {"537","CID informado invalido."}
#define __aCdCri072 {"538","Data de validade do cartao da empresa vencida."}
#define __aCdCri073 {"037","Data de validade do cartao do usuario vencido."}
#define __aCdCri074 {"038","Produto do usuario nao permite atendimento por esta Rede de Atendimento."}    
#define __aCdCri075 {"539","Falha no retorno dos parametros na funcao PLSESPNIV."}
#define __aCdCri076 {"039","Via de cartao magnetico nao existente."}    
#define __aCdCri077 {"040","Via de cartao magnetico bloqueada."}    
#define __aCdCri078 {"041","Via de cartao magnetico vencida."}    
#define __aCdCri079 {"042","Via de cartao magnetico invalida."}	
#define __aCdCri080 {"043","Critica por Time out na resposta da transacao."}    
#define __aCdCri081 {"045","Dente extraido/inutilizado para este procedimento."}  
#define __aCdCri082 {"046","Codigo corespondente ao dente no procedimento invalido."}     
#define __aCdCri083 {"047","Especialidade bloqueada para esta RDA."}  
#define __aCdCri084 {"048","Local de Atendimento bloqueado para esta RDA."}  
#define __aCdCri085 {"051","O usuario nao possui cobertura para este tipo de internacao"}  
#define __aCdCri086 {"052","Evento cobrado pela operadora destino nao foi pre-autorizado."}  
#define __aCdCri087 {"053","A quantidade autorizada e diferente da quantidade apresentada/cobrada pela operadora destino."}  
#define __aCdCri088 {"054","O usuario autorizado e diferente do usuario que esta sendo cobrado pela operadora destino."}  
#define __aCdCri089 {"055","A Data/Hora autorizada e diferente da Data/Hora apresentada/cobrada pela operadora destino."}  
#define __aCdCri090 {"056","A guia que foi autorizada para a operadora destino nao foi cobrada portando nao sera paga."}  
#define __aCdCri091 {"057","Usuario importado invalido. Deve ser alterado o usuario para o correto ou glosada a nota."}  
#define __aCdCri092 {"058","Esta empresa nao pode emitir guias via POS."}  
#define __aCdCri093 {"058","Guia do arquivo PTU nao encontrado como importado..."}  
#define __aCdCri094 {"059","Guia com valor apresentado diferente do valor apresentado no ptu."}  
#define __aCdCri095 {"541","Operador do sistema nao habilitado a acessar o usuario."}  
#define __aCdCri096 {"060","Operadora do usuario bloqueada para atendimento em intercambio."}  
#define __aCdCri097 {"061","Glosa de taxa administrativa, devido a data limite para recebimento de faturas de intercambio."}  
#define __aCdCri098 {"062","Procedimento nao existente na liberacao de origem."}  
#define __aCdCri099 {"063","Participacao de servico informada invalida."}  
#define __aCdCri100 {"064","Atendimento bloqueado, no subcontrato do usuario, para a rede de atendimento informada."}
#define __aCdCri101 {"065","Atendimento bloqueado, no produto do usuario, para a rede de atendimento informada."}
#define __aCdCri102 {"702","Usuario ainda nao pagou a primeira mensalidade"}  
#define __aCdCri103 {"704","Cid incompativel com o servico"}
#define __aCdCri104 {"705","Rede de atendimento de Alto Custo"  }
#define __aCdCri105 {"706","Procedimento na cadastrado na tabela MAT/MED"  }
#define __aCdCri106 {"707","Quantidade maxima de procedimentos excedida"  }
#define __aCdCri107 {"703","CID nao permitido para este executante - enviar para auditoria"}  
#define __aCdCri108 {"708","Rda com atendimento suspenso"}  
#define __aCdCri109 {"066","Evento de alto custo. O valor a ser cobrado/pago deve ser analisado."}
#define __aCdCri110 {"067","Evento de alto custo. NF de Entrada nao foi localizada. O valor a ser cobrado/pago deve ser atualizado manualmente."}
#define __aCdCri111 {"068","Evento de alto custo. Valor ja foi pago atraves da NF de Entrada."}
#define __aCdCri112 {"546","Valor total da participa��o financeira de um mesmo procedimento divergentes entre as duas guias."}  
#define __aCdCri113 {"547","Valor de pagamento do subitem de um mesmo procedimento para a mesma R.D.A divergentes entre as duas guias."}  
#define __aCdCri114 {"548","Codigo da R.D.A para pagamento de um mesmo subitem divergentes entre as duas guias."}  
#define __aCdCri115 {"549","Nao foi possivel localizar o lan�amento de debito/credito."}  
#define __aCdCri116 {"550","Nao foi possivel localizar o nivel de cobranca."}  
#define __aCdCri117 {"542","Matriculas dos usu�rios divergentes entre as duas guias."}  
#define __aCdCri119 {"544","Procedimento nao encontrado na guia clonada."}  
#define __aCdCri120 {"545","Codigos de procedimentos divergentes entre as duas guias."}  
#define __aCdCri121 {"551","Procedimento nao encontrado na guia estornada."}  
#define __aCdCri122 {"069","Quantidades de diarias pre-autorizadas diferente da quantidade de diarias contidas na guia."}   
#define __aCdCri123 {"070","Codigo de diaria contido na guia nao localizado na autorizacao de origem."}  
#define __aCdCri124 {"071","Dente/Regiao nao autorizado."}  
#define __aCdCri125 {"072","Localidade n�o � permitida para este usu�rio"}
#define __aCdCri126 {"552","Tipo de admiss�o inv�lida"}  
#define __aCdCri127 {"553","Tipo de contrato inv�lido"}  
#define __aCdCri128 {"554","Tipo de atendimento inv�lido"}  
#define __aCdCri129 {"900","Usuario de intercambio ja cadastrado como eventual."}  
#define __aCdCri130 {"901","Usuario de intercambio ja cadastrado como repasse."}  
#define __aCdCri131 {"902","Usuario de intercambio ja ligado a operadora padrao."}  
#define __aCdCri132 {"903","Subcontrato cadastrado para intercambio invalido."}
#define __aCdCri133 {"904","Operadora do usuario de intercambio possui mesmo codigo da operadora padrao."}
#define __aCdCri134 {"905","Tabela de tipos de intercambio invalida ou nao possui registros."}
#define __aCdCri135 {"073","Quantidade solicitada de procedimentos menor que a quantidade autorizada"}
#define __aCdCri136 {"074","N�o � permitido Selecionar faces n�o autorizadas para o dente "}
#define __aCdCri137 {"075","N�o h� sele��o v�lida para este procedimento. "}
#define __aCdCri138 {"076","N�o foram selecionadas todas as faces necess�rias para o dente "}
#define __aCdCri139 {"077","Quantidade solicitada nao permitida devido ao grupo de quantidade cadastrado."}
#define __aCdCri140 {"078","Quantidade maior que o limite permitido na parametriza��o na tabela padr�o."}  
#define __aCdCri141 {"079","Execu��o sem previa solicita��o"}  
#define __aCdCri142 {"080","Lote de guia XML j� importado para esta RDA"}  
#define __aCdCri143 {"081","Libera��o n�o encontrada"}
#define __aCdCri144 {"082","Item da libera��o j� executado"}
#define __aCdCri145 {"083","Item com quantidade maior que a solicitada"}
#define __aCdCri146 {"084","Resumo de interna��o sem previa Solicita��o"}
#define __aCdCri147 {"555","O produto sa�de do benefici�rio n�o atende a esta cidade ou estado.Verifique a �rea de abrang�ncia do plano"}
#define __aCdCri148 {"556","Hash invalido na valida��o do arquivo"}
#define __aCdCri149 {"557","Subitem nao localizado na guia clonada."} 
#define __aCdCri150 {"558","Bloqueio de cobranca divergente entre os procedimentos."} 
#define __aCdCri151 {"559","Bloqueio de pagamento divergente entre os procedimentos."} 
#define __aCdCri152 {"560","Para solicitacao referente a esta internacao ja consta data de alta"} 
#define __aCdCri153 {"561","Incompatibilidade de informa��o entre (Tipo de faturamento e data de alta)" } 
#define __aCdCri154 {"562","Honor�rio m�dico n�o importado" } 
#define __aCdCri155 {"563","Equipe informada no resumo de interna��o para guia com honor�rio m�dico" } 
#define __aCdCri156 {"085","Produto do usuario nao permite reembolso."}
#define __aCdCri157 {"086","Procedimento informado nao permite reembolso."}
#define __aCdCri158 {"087","Bloqueio de pagamento e cobran�a, evento n�o Autorizado."}
#define __aCdCri159 {"088","Matricula n�o existe ou invalida, criado usu�rio gen�rico."}
#define __aCdCri160 {"089","Tipo de procedimento invalido para o procedimento informado"}
#define __aCdCri161 {"090","Procedimento com periodicidade em rela��o � outro procedimento j� realizado."}
#define __aCdCri162 {"564","Quantidade de procedimentos maior que a quantidade restante autorizada"}
#define __aCdCri163 {"091","Solicita��o (Resumo/Honorario) n�o encontrada para este usu�rio."}
#define __aCdCri164 {"092","Item da interna��o n�o encontrado na importa��o do Resumo de interna��o"}
#define __aCdCri165 {"093","Resumo de Interna��o - Inclui todos os itens encontrados no arquivo que n�o est�o na guia (Contas Medicas)?"}
#define __aCdCri166 {"094","Incluido bloqueado pelo Resumo de interna��o"}
#define __aCdCri167 {"095","Resumo de Interna��o - Rede de Atendimento n�o informada"}
#define __aCdCri168 {"096","Resumo de Interna��o - Profissional de Sa�de n�o informado"}
#define __aCdCri169 {"097","Bloqueio de pagamento e cobran�a, participa��o n�o informada"}
#define __aCdCri170 {"565","Nota n�o encontrada na base de dados"}
#define __aCdCri171 {"566","Procedimento incompativel com um dos procedimentos da guia - Criticar"}
#define __aCdCri172 {"098","Obrigatorio a informacao do medico executante para este tipo de guia"}  
#define __aCdCri173 {"099","Guia j� processada com este numero impresso"}
#define __aCdCri174 {"09A","Participa��o j� informada para este item"}
#define __aCdCri175 {"581","Participa��o informada n�o existe para este procedimento"}
#define __aCdCri176 {"09C","Item n�o encontrado na guia informada."}
#define __aCdCri177 {"09D","Cobranca bloqueada devido ao Parcelamento."}
#define __aCdCri178 {"09E","Bloqueio de pagamento ou exclusao da composicao ao negar sub-item."}
#define __aCdCri179 {"09F","Bloqueio de pagamento, composi��o n�o autorizada no Atendimento."}
#define __aCdCri180 {"09G","N�o existe Vig�ncia Ativa!"}
#define __aCdCri181 {"09H","Procedimento bloqueado na sub-especialidade de Rede de atendimento."}
#define __aCdCri182 {"570","O limite de US foi atingido."}
#define __aCdCri183 {"09I","Esta Guia ser� Glosada, pois, est� duplicada."} 
#define __aCdCri184 {"09J","Atendimento domiciliar necessita libera��o aten��o a saude." }     
#define __aCdCri185 {"09K","A Rede Nao Referenciada executante � uma RDA que permite solicitar/executar"}
#define __aCdCri186 {"09L","Procedimento j� existente no pacote."}
#define __aCdCri187 {"573","Demanda por requerimento"}
#define __aCdCri188 {"567","Procedimento incompativel com um dos procedimentos da guia - Auditar"}
#define __aCdCri189 {"568","Pre-requisito nao encontrado dentro do periodo maximo exigido."}
#define __aCdCri190 {"569","Pre-requisito nao cumpriu periodo minimo exigido."}
#define __aCdCri191 {"09M","N�o permite Reembolso para o Usu�rio!"}
#define __aCdCri192 {"571","Procedimento incompativel com um dos procedimentos da guia - Reduzir a UCO"} 
#define __aCdCri193 {"572","Procedimento incompativel com um dos procedimentos da guia - Criticar"}
#define __aCdCri194 {"574","Auditoria Participativa"}
#define __aCdCri195 {"09N","Data de entrega fora do prazo"}
#define __aCdCri196 {"09O","Evento ja pago ou incidencia excede a quantidade autorizada"}
#define __aCdCri197 {"09P","Habilita checagem de regra para procedimentos incompat�veis!"}
#define __aCdCri198 {"09Q","Habilita checagem DE/PARA TUSS!"}
#define __aCdCri199 {"576","Participacao informada nao existe para este procedimento ou o procedimento nao possui nenhum participacao cadastrada."}
#define __aCdCri200 {"580","Tipo de Carater Inv�lido"}
#define __aCdCri201 {"577","Nao encontrou o prestador informado - Ptu Online"}
#define __aCdCri202 {"578","Registro DS_OBSERVA informado, guia automaticamente enviada para Auditoria"}
#define __aCdCri203 {"579","Registro DS_OPME informado, servi�o n�o existe na tabela de Interc�mbio Nacional"}
#define __aCdCri204 {"0AA","Quantidade de Procedimentos - A quantidade de Procedimentos � superior a quantidade permitida."}
#define __aCdCri09W {"09W","Data da realiza��o do evento informado e superior a data atual.!"}   
#define __aCdCri09Z {"09Z","Guia Juridica - Esta guia n�o ser� submetida aos crit�rios de valida��o do sistema!" }
#define __aCdCri205 {"09R","Codigo de procedimento enviado em Guia de Consulta, nao reconhecido como uma consulta pela operadora"}                    
#define __aCdCri206 {"582","Bloqueio processo AJIUS"}     
#define __aCdCri207 {"09V","Paciente ja esta internado"}     
#define __aCdCri208 {"0TT","Valor cobrado da despesa diferente do valor negociado."}
#define __aCdCri209 {"09T","Evento generico n�o pode ser solicitado no Portal para atendimento de interc�mbio."}   
#define __aCdCri210 {"906","Matr�cula interc�mbio inv�lida, rever cadastro benefici�rio"}        
#define __aCdCri211 {"0A1","RDA Bloqueada na data do procedimento"}
#define __aCdCri585 {"585","Quantidade de diarias Solicitadas diferente do Configurado na Tabela Padrao"}
#define __aCdCri214 {"0A2","RDA Bloqueada nas exce��es do Produto x RDA�s (Redes referenciadas para Cooperativas)."}
#define __aCdCri215 {"09B","Participacao informada nao existe para este procedimento"}
#define __aCdCri216 {"0A3","RDA n�o encontrada na Rede Referenciada deste Produto (Cooperaticas M�dicas)."}
#define __aCdCri217 {"515","Nao existe calendario de pagamento para a data do evento informada."}
#define __aCdCri218 {"516","O Valor do evento esta igual a zero."}
#define __aCdCri219 {"701","Limite de consultas excedido. Necessario autorizacao da empresa."}
#define __aCdCri220 {"709","Necessita de justificativa tecnica (Relatorio Prestador)"}
#define __aCdCri221 {"710","Necessita de justificativa tecnica (Relatorio Prestador)"}
#define __aCdCri222 {"593","Bloqueio de pagamento evento generico"} 
#define __aCdCri223 {"590","Unidade com bloqueio autom�tico pela BD3."}
#define __aCdCri226 {"591",STR0013} //"Bloq. em funcao de glosa pagto"
#define __aCdCri227 {"592","Bloqueio da cobranca da PF, porque o pagamento sera feito diretamente a RDA"} 
#define __aCdCri228 {"586","Erro na estrutura do arquivo PTU Online gerado."}
#define __aCdCri229 {"587","Usu�rio n�o tem premiss�o para solicitar Reembolso"}
#define __aCdCri230 {"594","Unidade com vig�ncia fechada BD4."} 
#define __aCdCri231 {"0A4","Redu��o de custo."} 
#define __aCdCri232 {"595","Unidade com bloqueio autom�tico pela B4R (Exce�ao de US)."} 
#DEFINE __aCdCri233 {"596","Bloqueio em fun��o de todas as unidades estarem bloqueadas"}
#define __aCdCri234 {"597","Unidade n�o existe na composi��o do evento"} 
#define __aCdCri235 {"598","Bloqueio n�o definido no motivo de bloqueio"} 
#define __aCdCri236 {"583","Nao foi informado nenhum valor para o unidade REA."}
#define __aCdCri237 {"584","Nao foi informado nenhum valor para o unidade RCC."}
#define __aCdCri238 {"585","Nao implementado valoracao para esta unidade"}
#define __aCdCri239 {"586","Nao foi informado nenhum valor para o unidade DOP."}

#define __aCdCri500 {"011","O produto do beneficiario nao atende a esta cidade ou estado, verifique a area de abrangencia do plano."}
#define __aCdCri09S {"09S","Quantidade de dias permitido para execu��o da guia foi ultrapassado"}
#define __aCdCri09U {"09U","Guia possui cobranca de participacao nao prevista na composicao do evento"}
#define __aCdCriPTS {"0TS","Apenas um procedimento pode ser executado para a mesma data de execu��o, prestador e beneficiario."}
#define __aCdCri240 {"5A1","Este usu�rio somente poder� utilizar o plano conforme bloqueio pr�-determinado."}

CRITICAS ARQUIVOS XML
#define __aXMLCri00 {"X00","Nao foi possivel criar os diretorios padroes TISS"}
#define __aXMLCri01 {"X01","Arquivo(s) de Schemas nao encontrado(s)"}
#define __aXMLCri02 {"X02","Nao foi possivel encontrar o Cabecalho nos Schemas"}
#define __aXMLCri03 {"X03","Mensagens da operadora nao encontrada nos Schemas"}
#define __aXMLCri04 {"X04","Nao foi possivel encontrar as Mensagens do prestador no Schemas"}
#define __aXMLCri05 {"X05","Nao foi possivel encontrar o Epilogo no Schemas"}
#define __aXMLCri06 {"X06","Numero sequencial no nome do arquivo invalido"}
#define __aXMLCri07 {"X07","Delimitador nao encontrado no nome do arquivo"}
#define __aXMLCri08 {"X08","Tamanho do hash no nome do arquivo invalido"}
#define __aXMLCri09 {"X09","NameSpace definido incorreto com base nos Schemas"}
#define __aXMLCri10 {"X10","Especialidade nao encontrada no sistema, para este prestador, com base no CBOS informado no arquivo"}
#define __aXMLCri11 {"X11","Estrutura da TAG CABECALHO nao definida, ou definida de maneira incorreta, no arquivo submetido"}
#define __aXMLCri12 {"X12","Estrutura da TAG PRESTADORPARAOPERADORA nao definida, ou definida de maneira incorreta, no arquivo submetido"}
#define __aXMLCri13 {"X13","Estrutura da TAG EPILOGO nao definida, ou definida de maneira incorreta, no arquivo submetido"}
#define __aXMLCri14 {"X14","Versao do arquivo nao aceito pela operadora "}
#define __aXMLCri15 {"X15","Hash invalido, diferenca entre informado e a validacao do conteudo"}
#define __aXMLCri16 {"X16",'Tipo de guia enviada no arquivo nao permitida ou incorreta'}
#define __aXMLCri17 {"X17","Conteudo que identifica o prestador na operadora esta invalido"}
#define __aXMLCri18 {"X18","Tipo de transacao invalido para o tipo de guia "}
#define __aXMLCri19 {"X19","Codigo do numero de registo na ANS enviado invalido"}
#define __aXMLCri20 {"X20","Nao existe calendario de pagamento, cadastrado na operadora, para esta competencia"}
#define __aXMLCri21 {"X21","Matricula do beneficiario nao reconhecida pela operadora na data informada"}
#define __aXMLCri22 {"X22","CID informado invalido"}
#define __aXMLCri23 {"X23","Numero da senha de autorizacao enviada invalida"}
#define __aXMLCri24 {"X24","Codigo que identifica o contratado nao reconhecido pela operadora"}
#define __aXMLCri25 {"X25","Especialidade nao encontrada no sistema com base no CBOS informado no arquivo"}
#define __aXMLCri26 {"X26","Codigo que identifica o executante nao reconhecido pela operadora"}
#define __aXMLCri27 {"X27","Codigo de evento enviado invalido ou bloqueado pela operadora"}
#define __aXMLCri28 {"X28","Quantidade pre-autorizada menor do que a quantidade enviada"}
#define __aXMLCri29 {"X29","Local de atendimento nao encontrado com base no endereco enviado"}
#define __aXMLCri30 {"X30","Nao foi possivel localizar uma composicao para este evento"}
#define __aXMLCri31 {"X31","Nao foi possivel localizar uma tabela de pagamento valida para este evento"}
#define __aXMLCri32 {"X32","Problemas com o contrato do beneficiario na data informada"}
#define __aXMLCri33 {"X33","Operadora localizada com base no registro da ans enviado, diferente da operadora padrao"}
#define __aXMLCri34 {"X34","Executante nao informado na guia de consulta"}
#define __aXMLCri36 {"X36","Numero da senha nao autorizada pela operadora"}
#define __aXMLCri37 {"X37","Evento informado nao esta contido na guia de pre-autorizacao"}
#define __aXMLCri38 {"X38","Codigo que identifica o profissional executante complementar nao reconhecido pela operadora"}
#define __aXMLCri39 {"X39","Codigo que identifica o membro de equipe nao reconhecido pela operadora"}
#define __aXMLCri40 {"X40","Codigo de procedimento enviado em Guia de Consulta, nao reconhecido como uma consulta pela operadora"}
#define __aXMLCri41 {"X41","Tipo de Carater de Atendimento  Invalido" }
#define __aXMLCri42 {"X42","Data do procedimento superior a data do sistema" }
#define __aXMLCri43 {"X43","Regime de Internacao Invalido" }
#define __aXMLCri44 {"X44","Evento enviado no arquivo nao foi autorizado na senha enviada" }
#define __aXMLCri45 {"X45","Tempo Referido na Evolucao da Doenca esta Invalido" }
#define __aXMLCri46 {"X46","Beneficiario Bloqueado." }
#define __aXMLCri47 {"X47","O executante nao pode atender usuarios deste produto ." }
#define __aXMLCri48 {"X48","A Rede de atendimento nao pode atender usuarios deste produto ." }
#define __aXMLCri49 {"X49","Via de Acesso diferente da solicitada ." }
#define __aXMLCri52 {"X52","Procedimento com valor negativo." }
#define __aXMLCri53 {"X53","Apenas um procedimento pode ser executado para a mesma data de execucao, prestador e beneficiario." }
#define __aXMLCri54 {"X54","O valor apresentado e diferente do valor contratado." }
#define __aXMLCri55 {"X55","Procedimento bloqueado na especialidade do Prestador."}
#define __aXMLCri56 {"X56","RDA informada como origem divergente da RDA que submeteu o Arquivo." }
#define __aXMLCri57 {"X57","O valor total deve ser igual ao valor unitario X %red/acr X qtde, sendo que o %red/acr deve ser 1 caso nao haja red/acr." }
#define __aXMLCri58 {"X58","Data inferior a quantidade de dias definido no par�metro MV_PLSDTAT." }
#define __aXMLCri59 {"X59","Especialidade bloqueada no cadastro do Prestador."}
#define __aXMLCri60 {"X60","Prestador Bloqueado/Nao Credenciado na data informada."}
#define __aXMLCri61 {"X61","Profissional de Sa�de Bloqueado na data informada"}