#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} PLSA260BF3
Browse de Doen�as Pre-Existentes
@author DEV TOTVS
@since  10/12/2019
@version P12
/*/
//-------------------------------------------------------------------

Function PLSA260BF3(lAutomato)

// Declara��o de Vari�veis
Local oBrowse
Default lAutomato := .F.

oBrowse := FWmBrowse():New()
oBrowse:SetAlias( 'BA1' )
oBrowse:SetDescription( Fundesc() )	
oBrowse:SetMenuDef( 'PLSA260BF3' )
If(!lAutomato,oBrowse:Activate(),)

Return (NIL)

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Defini��o do menu de Doen�as Pre-Existentes
@author  DEV TOTVS
@since   10/12/2019
@version P12
/*/          
//-------------------------------------------------------------------
Static Function MenuDef()

// Declara��o de Vari�veis
Local aRotina := {}

Return aRotina    

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Definicao do modelo de dados de Doen�as Pre-Existentes
@author  DEV TOTVS
@since   10/12/2019
@version P12
/*/
//-------------------------------------------------------------------
Static Function ModelDef()

// Declara��o de Vari�veis
Local oModel	
Local oStruBA1 		:= FWFormStruct(1,'BA1')
Local oStruBF3 		:= FWFormStruct(1,'BF3')
Local oStruBYY 		:= FWFormStruct(1,'BYY')	

Local aCamposBA1	:= {'BA1_CODINT','BA1_CODEMP','BA1_MATRIC'} // Campos a serem adicionado na estrutura
Local nNx

// Cria o objeto do Modelo de Dados	 
oModel := MPFormModel():New('PLSA260BF3')

// Cria os campos na estrutura que est�o como n�o usados no dicionario
For nNx := 1 To Len(aCamposBA1)
	oStruBA1 := CriaCampMVC(1,oStruBA1,aCamposBA1[nNx]) 
Next

// Adiciona as estruturas no modelo
oModel:addFields('BA1MASTER' ,NIL,oStruBA1) 
oModel:AddGrid('BF3DETAIL','BA1MASTER',oStruBF3)
oModel:AddGrid('BYYDETAIL','BF3DETAIL',oStruBYY)

// Permiss�o de grid sem dados
oModel:GetModel('BF3DETAIL'):SetOptional(.T.)
oModel:GetModel('BYYDETAIL'):SetOptional(.T.)

// Relacionamento entre as tabelas
oModel:SetRelation( 'BF3DETAIL', {  { 'BF3_FILIAL'  , 'xFilial( "BF3" )'},;
								    { 'BF3_CODINT'	, 'BA1_CODINT' } ,;
                                    { 'BF3_CODEMP'	, 'BA1_CODEMP' } ,;
                                    { 'BF3_MATRIC'	, 'BA1_MATRIC' } ,;
                                    { 'BF3_TIPREG'	, 'BA1_TIPREG' }},;										
								BF3->( IndexKey( ) ) ) 

oModel:SetRelation( 'BYYDETAIL', {  { 'BYY_FILIAL'  , 'xFilial( "BYY" )'},;
								    { 'BYY_CODINT'	, 'BA1_CODINT' } ,;
                                    { 'BYY_CODEMP'	, 'BA1_CODEMP' } ,;
                                    { 'BYY_MATRIC'	, 'BA1_MATRIC' } ,;
                                    { 'BYY_TIPREG'	, 'BA1_TIPREG' } ,;	
                                    { 'BYY_CODDOE'	, 'BF3_CODDOE' }},;										
								BYY->( IndexKey( ) ) )  					
	
oModel:SetDescription( FunDesc() )	// Descri��o do Modelo de dados

// Descri��o de cada modelo usado
oModel:GetModel('BA1MASTER'):SetDescription('Familia' )
oModel:GetModel('BF3DETAIL'):SetDescription('Doen�as Pr�-Existentes' )	
oModel:GetModel('BYYDETAIL'):SetDescription('Car�ncias' )	

// N�o permite altera��o ou inclus�o no modelo
oModel:GetModel('BA1MASTER'):SetOnlyQuery(.T.)
oModel:GetModel('BA1MASTER'):SetOnlyView(.T.)

// Altera��o no dicionario da tabela BF3                                                                                                                                                        
oStruBF3:setProperty( 'BF3_CODDOE', MODEL_FIELD_VALID, { || BA9->(ExistCpo('BA9',oModel:GetValue('BF3DETAIL','BF3_CODDOE'),1))} )
oStruBF3:setProperty( 'BF3_TIPREG', MODEL_FIELD_INIT,  { || BA1->BA1_TIPREG })


// Campos n�o usados 
//------------------------------------------------------------------------------
//oStruBF3:setProperty( 'BF3_CODPAD', MODEL_FIELD_VALID, { || BR4->(ExistCpo('BR4',oModel:GetValue('BF3DETAIL','BF3_CODPAD'),1))} )
//oStruBF3:setProperty( 'BF3_CODPSA', MODEL_FIELD_VALID, { || BR8->(ExistCpo('BR8',oModel:GetValue('BF3DETAIL','BF3_CODPAD')+oModel:GetValue('BF3DETAIL','BF3_CODPSA'),1))} )
//------------------------------------------------------------------------------
oStruBF3:setProperty( 'BF3_VALAGR', MODEL_FIELD_WHEN,  { || oModel:GetValue('BF3DETAIL','BF3_PAGAGR') == "1" })
oStruBF3:setProperty( 'BF3_PERAGR', MODEL_FIELD_WHEN,  { || oModel:GetValue('BF3DETAIL','BF3_PAGAGR') == "1" })
oStruBF3:setProperty( 'BF3_UNAGR' , MODEL_FIELD_WHEN,  { || oModel:GetValue('BF3DETAIL','BF3_PAGAGR') <> "1" })
oStruBF3:setProperty( 'BF3_DESDOE', MODEL_FIELD_INIT,  { || If(Inclui,"",Posicione("BA9",1,xFilial("BA9")+BF3->BF3_CODDOE,"BA9_ABREVI")) } )

// Altera��o no dicionario da tabela BYY    
oStruBYY:setProperty( 'BYY_TIPREG', MODEL_FIELD_INIT,  { || BA1->BA1_TIPREG })   
oStruBYY:setProperty( 'BYY_CODDOE', MODEL_FIELD_INIT,  { || oModel:GetValue('BF3DETAIL','BF3_CODDOE') })                                                                                                                                             
oStruBYY:setProperty( 'BYY_CODPRO', MODEL_FIELD_VALID, { || BR8->(ExistCpo('BR8',oModel:GetValue('BYYDETAIL','BYY_CODPAD')+oModel:GetValue('BYYDETAIL','BYY_CODPRO'),1))} )

oModel:SetPrimaryKey( { 'BF3_FILIAL', 'BF3_CODINT', 'BF3_CODEMP', 'BF3_MATRIC' , 'BF3_CODDOE' } )


//FwStruTrigger: ( cDom, cCDom, cRegra, lSeek, cAlias, nOrdem, cChave, cCondic )
aAux := FwStruTrigger(;
        'BF3_CODDOE'     ,; 
        'BF3_DESDOE'     ,; 
        'Alltrim(BA9->BA9_DOENCA)',;
        .F.              ,; 
        'BA9'            ,; 
        1                ,; 
        ''               ,;
        '')

oStruBF3:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

Return (oModel)

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Defini��o da View de Doen�as Pre-Existentes
@author  DEV TOTVS
@since   10/12/2019
@version P12
/*/
//-------------------------------------------------------------------
Static Function ViewDef() 

// Declara��o de Vari�veis
Local oStruBA1 := FWFormStruct(2,'BA1', { |cCampo| AllTrim(cCampo) $ 'BA1_CODINT|BA1_CODEMP|BA1_MATRIC|BA1_NOMUSR' } )
Local oStruBF3 := FWFormStruct(2,'BF3')	
Local oStruBYY := FWFormStruct(2,'BYY')	
Local oModel   := FWLoadModel( 'PLSA260BF3') // Carrega o modelo
Local oView
Local aCamposBA1  := {"BA1_CODINT","BA1_CODEMP","BA1_MATRIC"} // Campos a serem adicionado na estrutura
Local nNx	

oView := FWFormView():New() // Cria o Objeto View

// Cria os campos na estrutura que est�o como n�o usados no dicionario
For nNx := 1 To Len(aCamposBA1)
	oStruBA1 := CriaCampMVC(2,oStruBA1,aCamposBA1[nNx],StrZero(nNx,2))
Next

oView:SetModel( oModel )

// Removo os campos para n�o ser exibido no AddGrid
oStruBF3:RemoveField('BF3_TIPREG')
oStruBYY:RemoveField('BYY_TIPREG')
oStruBYY:RemoveField('BYY_CODDOE')

oView:AddField( 'VIEW_BA1' , oStruBA1, 'BA1MASTER' )
oView:AddGrid(  'VIEW_BF3' , oStruBF3, 'BF3DETAIL' )
oView:AddGrid(  'VIEW_BYY' , oStruBYY, 'BYYDETAIL' )

oStruBA1:SetNoFolder() // Retirando as pastas de uma estrutura

oView:CreateHorizontalBox( 'SUPERIOR', 12) 
oView:CreateHorizontalBox( 'MEIO'	 , 50) 
oView:CreateHorizontalBox( 'INFERIOR', 38) 

oView:SetOwnerView('VIEW_BA1', 'SUPERIOR')
oView:SetOwnerView('VIEW_BF3', 'MEIO')	
oView:SetOwnerView('VIEW_BYY', 'INFERIOR')	
	
oView:EnableTitleView('VIEW_BA1','Benefici�rio')
oView:EnableTitleView('VIEW_BF3','Doen�as Pr�-Existentes')
oView:EnableTitleView('VIEW_BYY','Car�ncias')

Return oView                                  