#include "PROTHEUS.CH"
/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Fun�ao    �UPDPLSDROG � Autor � Luciano Aparecido     � Data � 20.05.08 ���
��������������������������������������������������������������������������Ĵ��
���Descri�ao � Atualizacao das tabelas									   ���
��������������������������������������������������������������������������Ĵ��
��� Uso      � SigaPLS x Template Drogaria                                 ���
��������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                      ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
User Function UPDPLSDROG()

Local aSays       := {}
Local aButtons 	  := {}
Local nOpca       := 0
Local aRecnoSM0   := {}     
Local lOpen       := .F.  
Local lOkEmp      := .F.
Local I 
Private cMessage
Private aArqUpd	  := {}
Private aREOPEN	  := {}
Private cCadastro := "Compatibilizador de Dicion�rios x Banco de dados"
Private cCompat   := "UPDPLSDROG"
Private cBops     := "00000146190"
Private cRef      := "Integra��o Plano de Sa�de x Template Drogaria"
Private oMainWnd         
Private cAlias    := ''
Private nModulo	  :=0
cArqEmp := "SigaMat.Emp"
__cInterNet := Nil 
__lPyme     :=.F.

Set Dele On

//����������������������������������������������������������������������������Ŀ
//� Monta texto para janela de processamento                                   �
//������������������������������������������������������������������������������
aadd(aSays,"Esta rotina ir� efetuar a compatibiliza��o dos dicion�rios de dados, banco de dados e demais ajustes referentes ao bops abaixo:") 
aadd(aSays,"   Bops: " + cBops) 
aadd(aSays,"   Refer�ncia: " + cRef)
aadd(aSays," ")
aadd(aSays,"ATEN��O!!!") 
aadd(aSays,"Esta rotina deve ser utilizada em modo exclusivo ! ")
aadd(aSays,"Fa�a um backup dos dicion�rios e da Base de Dados antes da atualiza��o para eventuais falhas de atualiza��o !")

//����������������������������������������������������������������������������Ŀ
//� Monta botoes para janela de processamento                                  �
//������������������������������������������������������������������������������
aadd(aButtons, { 1,.T.,{|| nOpca := 1, FechaBatch() }} )
aadd(aButtons, { 2,.T.,{|| nOpca := 0, FechaBatch() }} )

//����������������������������������������������������������������������������Ŀ
//� Exibe janela de processamento                                              �
//������������������������������������������������������������������������������
FormBatch( cCadastro, aSays, aButtons,, 250, 880 )

//����������������������������������������������������������������������������Ŀ
//� Processa calculo                                                           �
//������������������������������������������������������������������������������
If  nOpca == 1 
	If  Aviso("Compatibilizador", "Deseja confirmar o processamento do compatibilizador ?", {"Sim","N�o"}) == 1 
        Processa({||UpdEmp(aRecnoSM0,lOpen)},"Processando","Aguarde, processando prepara��o dos arquivos",.F.) 
        For I := 1 To Len(aRecnoSM0)
			if aRecnoSM0[I,1]
				lOkEmp:=.T.
			    Exit
			Endif 
		Next I
        if lOkEmp
        	Final("Processamento conclu�do com sucesso !")
        Endif
    Endif
Endif
	
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � PLSProc  � Autor � Luciano Aparecido     � Data � 07.04.08 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Funcao de processamento da gravacao dos arquivos           ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Atualizacao PLS                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function PLSProc(lEnd,aRecnoSM0,lOpen)

Local cTexto    := ''
Local cFile     := ""
Local cMask     := "Arquivos Texto (*.TXT) |*.txt|" 
Local nRecno    := 0
Local nI        := 0
Local nX        := 0
Local lSel      :=.T.

ProcRegua(1)
IncProc("Verificando integridade dos dicion�rios....")
ProcessMessage()

If lOpen
	lSel:=.F.
	For nI := 1 To Len(aRecnoSM0)
		if ! aRecnoSM0[nI,1]
			loop
		Endif 
		lSel:=.T.
		SM0->(dbGoto(aRecnoSM0[nI,2]))
		RpcSetType(2) 
		RpcSetEnv(SM0->M0_CODIGO, SM0->M0_CODFIL)
		nModulo 	 := 33 //SIGAPLS
		lMsFinalAuto := .F.
		cTexto += Replicate("-",128)+CHR(13)+CHR(10)
		cTexto += "Empresa: " +SM0->M0_CODIGO+" - "+SM0->M0_NOME+CHR(13)+CHR(10) 
		ProcRegua(12)
        
      	//�������������������������������Ŀ
		//� Atualiza o Arquivo de Campos  �
		//���������������������������������
		IncProc("Analisando Arquivo de Campos"+"...")
		ProcessMessage()
		
		//�������������������������������Ŀ
		//� Atualiza o Arquivo de Indices �
		//���������������������������������
		IncProc("Atualizando SIX - Arquivo de Ind�ce...")
		cTexto += Replicate("-",120) + CHR(13) + CHR(10)
		cTexto += "SIX - Arquivo de Ind�ce" + CHR(13) + CHR(10)
		cTexto += " " + CHR(13) + CHR(10)
		  
		//����������������������������������������������������������������������������Ŀ
	    //� Atualiza SX5                                                               |
	    //������������������������������������������������������������������������������
	    IncProc("Atualizando SX5 - Tabelas Gen�ricas...") 
	    cTexto += Replicate("-",120) + CHR(13) + CHR(10)
	    cTexto += "SX5 - Tabelas Gen�ricas" + CHR(13) + CHR(10)
	    cTexto += " " + CHR(13) + CHR(10)

		FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01',  "Fun��es descontinuadas pelo SGBD: PLSAtuSX3(),PLSAtuSIX() e PLSAtuSX5()"  , 0, 0, {})		
			
		__SetX31Mode(.F.)
		For nX := 1 To Len(aArqUpd)
			IncProc("Atualizando estruturas. Aguarde"+"... ["+aArqUpd[nx]+"]") 
			ProcessMessage()
			If Select(aArqUpd[nx])>0
				dbSelecTArea(aArqUpd[nx])
				dbCloseArea()
			EndIf
			X31UpdTable(aArqUpd[nx])
			If __GetX31Error()
				Alert(__GetX31Trace())
				Aviso("Aten��o!","Ocorreu um erro desconhecido durante a atualiza��o da tabela: "+ aArqUpd[nx] + ". "+"Verifique a integridade do dicion�rio e da tabela.",{"Continuar"},2) 
				cTexto += "Ocorreu um erro desconhecido durante a atualiza��o da estrutura da tabela: "+aArqUpd[nx] +CHR(13)+CHR(10)
			EndIf
			dbSelectArea(aArqUpd[nx])
	  
		Next nX		
			
		RpcClearEnv()
		If !( lOpen := MyOpenSm0Ex() )
			Exit 
		EndIf 
	Next nI 
		  
	If lOpen
		cTexto := "Log da atualiza��o "+CHR(13)+CHR(10)+cTexto 
		if !lSel 
			cTexto+= "N�o foram selecionadas nenhuma empresa para Atualiza��o"
		Endif
		__cFileLog := MemoWrite(Criatrab(,.f.)+".LOG",cTexto)
		DEFINE FONT oFont NAME "Arial" SIZE 7,15 
		DEFINE MSDIALOG oDlg TITLE "Atualiza��o conclu�da." From 0,0 to 420,680 PIXEL 
		@ 5,5 GET oMemo  VAR cTexto MEMO SIZE 330,185 OF oDlg PIXEL  
		oMemo:bRClicked := {||AllwaysTrue() } 
		oMemo:oFont:=oFont
		DEFINE SBUTTON  FROM 195,300 TYPE 1 ACTION oDlg:End() ENABLE OF oDlg PIXEL //Apaga
		DEFINE SBUTTON  FROM 195,270 TYPE 13 ACTION (cFile:=cGetFile(cMask,""),If(cFile="",.t.,MemoWrite(cFile,cTexto))) ENABLE OF oDlg PIXEL //Salva e Apaga //"Salvar Como..."
		ACTIVATE MSDIALOG oDlg CENTER
	EndIf
	 
EndIf
		
Return(.T.)
 
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � UpdEmp   � Autor � Luciano Aparecido     � Data � 15.05.07 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Trata Empresa. Verifica as Empresas para Atualizar         ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Atualizacao PLS                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

static function  UpdEmp(aRecnoSM0,lOpen) 
		
Local cVar     := Nil
Local oDlg     := Nil
Local cTitulo  := "Escolha a(s) Empresa(s) que ser�(�o) Atualizada(s)" 
Local lMark    := .F.
Local oOk      := LoadBitmap( GetResources(), "CHECKED" )   //CHECKED    //LBOK  //LBTIK
Local oNo      := LoadBitmap( GetResources(), "UNCHECKED" ) //UNCHECKED  //LBNO
Local oChk     := Nil
Local bCode := {||oDlg:End(),Processa({|lEnd| PlsProc(@lEnd,aRecnoSM0,lOpen)},"Processando","Aguarde, processando prepara��o dos arquivos",.F.)} 
Private lChk     := .F.
Private oLbx := Nil

If ( lOpen := MyOpenSm0Ex() )
	
	dbSelectArea("SM0")
	/////////////////////////////////////////
	//| Carrega o vetor conforme a condicao |/
	//////////////////////////////////////////
	dbGotop()
	
	While !Eof() 
  		If Ascan(aRecnoSM0,{ |x| x[3] == M0_CODIGO}) == 0 //--So adiciona no aRecnoSM0 se a empresa for diferente
			Aadd(aRecnoSM0,{lMark,Recno(),M0_CODIGO,M0_CODFIL,M0_NOME+"/ "+M0_FILIAL})
		EndIf			
		dbSkip()
	EndDo	

	///////////////////////////////////////////////////
	//| Monta a tela para usuario visualizar consulta |
	///////////////////////////////////////////////////
	If Len( aRecnoSM0 ) == 0
   		Aviso( cTitulo, "Nao existe bancos a consultar", {"Ok"} ) 
   		Return
   	Endif

	DEFINE MSDIALOG oDlg TITLE cTitulo FROM 0,0 TO 240,500 PIXEL
	@ 10,10 LISTBOX oLbx VAR cVar FIELDS HEADER ;
  	" ", "Recno", "Cod Empresa","Cod Filial","Empresa /Filial" ; 
  	SIZE 230,095 OF oDlg PIXEL ON dblClick(aRecnoSM0[oLbx:nAt,1] := !aRecnoSM0[oLbx:nAt,1],oLbx:Refresh())

	oLbx:SetArray( aRecnoSM0)
	oLbx:bLine := {|| {Iif(aRecnoSM0[oLbx:nAt,1],oOk,oNo),;
                       aRecnoSM0[oLbx:nAt,2],;
                       aRecnoSM0[oLbx:nAt,3],;
                       aRecnoSM0[oLbx:nAt,4],;
                       aRecnoSM0[oLbx:nAt,5]}}
     
	////////////////////////////////////////////////////////////////////
	//| Para marcar e desmarcar todos 
	////////////////////////////////////////////////////////////////////

	@ 110,10 CHECKBOX oChk VAR lChk PROMPT "Marca/Desmarca" SIZE 60,007 PIXEL OF oDlg ; 
      ON CLICK(aEval(aRecnoSM0,{|x| x[1]:=lChk}),oLbx:Refresh())
 
	DEFINE SBUTTON FROM 107,213 TYPE 1 ACTION Eval(bCode) ENABLE OF oDlg
	ACTIVATE MSDIALOG oDlg CENTER 
	
Endif

Return

/*
���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �MyOpenSM0Ex� Autor �Sergio Silveira       � Data �07/01/2003���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Efetua a abertura do SM0 exclusivo                         ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Atualizacao FIS                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������
*/

Static Function MyOpenSM0Ex()

Local lOpen := .F. 
Local nLoop := 0 

For nLoop := 1 to 20
	openSM0( cNumEmp,.F. )
	If  ! Empty(Select("SM0")) 
		lOpen := .T. 
		dbSetIndex("SIGAMAT.IND") 
		Exit	
	EndIf
	Sleep( 500 ) 
Next nLoop 

If  ! lOpen
	Aviso( "Aten��o !", "N�o foi poss�vel a abertura da tabela de empresas de forma exclusiva !", { "Ok" }, 2 )
EndIf                                 

Return(lOpen) 
