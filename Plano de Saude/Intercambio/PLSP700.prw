#define __aCdCri086 {"052","Evento cobrado pela operadora destino nao foi pre-autorizado."}  
#define __aCdCri090 {"056","A guia que foi autorizada para a operadora destino nao foi cobrada portando nao sera paga."}  
#define __aCdCri091 {"057","Usuario importado invalido. Deve ser alterado o usuario para o correto ou glosada a nota."}  
#define __aCdCri099 {"063","Participacao de servico informada invalida."}  


#include "PLSMGER.CH"                                                                                 
#include "PROTHEUS.CH"

#define aLayouts {"2.6","2.7","2.8","2.9","3.0","3.1","3.2","3.3","3.4","3.5","3.8","4.0","8.0","9.0"}
#define cLocalExec "2" //a importacao do ptu e encarada como sendo feita no contas medicas


STATIC aUnidades := {{"AUX","AUR"},{"COP","COR","UCO"},{"HM ","HMR","PPM","RCC","CIR"},{"PA ","PAP","PAR"},{"FIL"},;
                      {"DOP","CRR","INC"},{"TCR","VDI","VMD","VMT","VTX","REA"}}  
                      
                      
STATIC aPriorida := {  {"HM ,HMR,PPM,RCC,CIR"},;//[1]honorario
					    {"PA ,PAP,PAR"},;           //[2]honorario
						{"AUX,AUR"},;					   //[3]honorario
                     	{"DOP,CRR,INC"},;				//[4]honorario         
                     	{"COP,COR,UCO"},;     			//[5]custo      
                     	{"TCR,VDI,VMD,VMT,VTX,REA"},;//[6]custo
                     	{"FIL"}}  								//[7]filme
                     
STATIC aTpPart    := {} 
STATIC nHorIni
STATIC cArquivo   := ""
STATIC cPerg      := "PLS700    "
STATIC lUsrGen    := .F.
STATIC a700Var    := {}
STATIC a700Pos    := {}
STATIC aCriticas  := {}
STATIC aCritReal  := {}
STATIC aResumo    := {}
STATIC aCritNota  := {}
STATIC lOK        := .F.
STATIC _nPosITREG := 0
STATIC _nPosFTREG := 0 
STATIC _nPosISEQ  := 0
STATIC _nPosFSEQ  := 0 
STATIC cUniDes    := ""
STATIC cUniOri    := ""
STATIC cAnoMes    := ""
STATIC cAMPAG     := ""
STATIC aDadRda    := {}
STATIC aDadUsr    := {}
STATIC nLine      := 1
STATIC cAliasPLS  := ""
STATIC aRecnosGer := {}
STATIC cPLSFiltro
STATIC cCodSeq	 := ""
STATIC _DT_EMI_FAT
STATIC _DT_VEM_FAT
STATIC _NR_FATURA
STATIC cCodProGen
STATIC lConverProc
STATIC lCriadoBRJ := .F.
STATIC nRegraGrvImp
STATIC lImpPTU    := .T.
STATIC cMatrCob
STATIC cNomCob
STATIC cUltCodigo := ""
STATIC cUltPart   := ""
STATIC cRdaUlt    := ""
STATIC cDataUlt   := ""
STATIC cMatrAntGen := "99999999999999999"
STATIC cCodPadCon  := ""
STATIC cCodUni702  := ""
STATIC aGuiaNaoExi := {}
STATIC aRegAtuMov := {}
STATIC lCrit099   := .F.
STATIC cDes099    := ""
STATIC pMoeda2    := "@E 999,999,999.99"
STATIC nImpBD6	  := GetNewPar("MV_P700BD6","0")
STATIC lMaiorTrez := .F.
STATIC nNVerTra	  
STATIC cCodPaPro  := GETMV("MV_PLSTBPD")
STATIC aCodEdi	  := {}
STATIC cCodLayPLS := ""
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� !!!!!!!!!!! ATENCAO !!!!!!!!										     �
//� !!!!!!!!!!! ATENCAO !!!!!!!!											 �
//� !!!!!!!!!!! ATENCAO !!!!!!!!											 �
//|																			 �
//| CASO SEJA CRIADA MAIS UMA VARIAVEL STATICA LEMBRAR DE LIMPA-LA NA FUNCAO |
//| ProcA50																	 |
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸                     

//+------------------------------------------------------------------
/*/{Protheus.doc} PLSEDIA700
Importacao A700 
@author  PLSTEAM
@version P11
@since   17.01.2005
/*/
//+------------------------------------------------------------------
Function PLSEDIA700  
PRIVATE aCdCores:= {}
PRIVATE cCadastro := "Importacao PTU-700"
PRIVATE aRotina   := {  { STRPL01      ,'AxPesqui'    , 0 , K_Pesquisar  },;
                        { STRPL02     ,'PLSED700VS'  , 0 , K_Visualizar },;
                        { "Importar"       ,'PLSED700MV'  , 0 , K_Incluir    },;
                        { "Canc.Importacao",'PLSED700CN'  , 0 , K_Alterar    }}
                        

aCdCores := { { 'BR_VERMELHO' ,'Importado' } }
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Chama mBrowse padrao...                                                  �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Aadd(aRotina, { "Legenda"    	 , "PLSP700LEG"		, 0 , K_Incluir     })
aadd(aRotina,{ "Inc.Manual Fat.",'AxInclui'  , 0 , K_Incluir    })
                                                                
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Monta novo Browse                                                        �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸   
cPLSFiltro := "BRJ->BRJ_FILIAL == '"+ BRJ->(xFilial("BRJ"))+ "' .And. BRJ->BRJ_REGPRI == '3' " 

oBrwBRJ := FWmBrowse():New()
oBrwBRJ:SetAlias( 'BRJ' )
oBrwBRJ:SetDescription( 'Importa豫o PTU A700' ) 
oBrwBRJ:SetFilterDefault( cPLSFiltro )     

oBrwBRJ:AddLegend( "BRJ->BRJ_STATUS == '2'",	'BR_VERMELHO',	OemToAnsi( "Importado" ) )      

oBrwBRJ:Activate()

Return

//+------------------------------------------------------------------
/*/{Protheus.doc} PLSEDIA700
Importacao A700 
@author  PLSTEAM
@version P11
@since   17.01.2005
/*/
//+------------------------------------------------------------------
Function PLSED700MV()
LOCAL aSays     := {}
LOCAL aButtons  := {}
LOCAL aRetPri   := {}  
LOCAL aArea     := {}  
LOCAL cCadastro := "Efetua a importacao do Layout A-700"
LOCAL aAmbiente
LOCAL nFor
LOCAL nPos                                                                     
LOCAL aNotas    := {}
LOCAL lFatal	:= .F.
LOCAL lImport	:= .F.
LOCAL lAnalisar := .F.
Local nAltura := 160
Local aPosObj    := {}
Local aObjects   := {}
Local aSize      := {}
Local aInfo      := {}

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Carregas as criticas em memoria e cria o SX1 On Demand...                �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
A700BCG( cPerg )
CarregaCrit()

aCritReal := {}
aResumo   := {}
aCritNota := {}                                                               
aNotas	  := {}
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Analisa ambiente para verificar se a rotina pode ser executada...        �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

cAMPAG := ""
aAmbiente := AnalisaAmb()

If ! aAmbiente[1]
   PLSCRIGEN(aAmbiente[2],{ {"Descricao","@C",200} }, "  Falha no ambiente ",NIL,NIL,NIL,NIL, NIL,NIL,"G",220)
   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
   //� Novo filtro                                                              �
   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
   If Valtype(oBrwBRJ) == "O"
       oBrwBRJ:Refresh(.T.)
       oBrwBRJ:ExecuteFilter()
   Else
	   DbSelectArea("BRJ")     
	   cPLSFiltro := "@BRJ_FILIAL = '"+xFilial("BRJ")+"' AND BRJ_REGPRI = '3' AND D_E_L_E_T_ = ' '"
	   SET FILTER TO &(cPLSFiltro)
	EndIf   
   Return
Else
   cUniDes := PLSINTPAD()
Endif   
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Monta dados para exibicao da tela de perguntas/processamento             �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
aadd(aSays,"Efetua a importacao do Layout A-700")
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Monta botoes para janela de processamento                                �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
aadd(aButtons, { 5,.T.,{|| aCriticas := {},  CarregaCrit(), Pergunte(cPerg,.T. ) } } )
aadd(aButtons, { 1,.T.,{|| Proc2BarGauge({|| aRetPri := ProcA50() }, cCadastro) , FechaBatch() }} )
aadd(aButtons, { 2,.T.,{|| FechaBatch() }} )
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Exibe janela de processamento                                            �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
FormBatch( cCadastro, aSays, aButtons,, 160 )
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� NOVO MECANISMO DE CRITICAS DO PTU 700									 �
//| PLSTEAM 2006/08/19														 |
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

If Len(aRetPri) >= 2
	lFatal   := aRetPri[2]
	lImport  := aRetPri[3]
	If Len(aRetPri) >= 4
		lAnalisar := aRetPri[4]
	Endif
Endif  
                                             
If ! lFatal .and. lImport
	aNotas := PLSP700Usr(nil,nil,nil,nil,.F.,cCodProGen)
Endif

If Len(aCritReal) > 0 .or. Len(aCritNota) > 0 .or. Len(aResumo) > 0 .or. Len(aNotas) > 0
    
	DbSelectArea("BRJ")
	oDialog   := nil               
	aArea     := GetArea()
	cCadastro := "Resumo do processamento"
	oFolder := nil                                        
 	bOK     := {|| RestArea(aArea) , nOpca :=1 , oDialog:End()}
	bCancel := {|| RestArea(aArea) , nOpca :=2 , oDialog:End()}
	oCriti01:= nil
	oCriti02:= nil
	oCriti03:= nil
	nFor	:= 1
	bBlock  := {|| }
	aBut	:= {}
	aCab01  := { {"Codigo","@C",040} , {"Critica","@C",200 } , {"Linha","@C",040 } , {"Descricao","@C",300 } }
	aCab02  := { {"Guia","@C",080} , {"Status","@C",100 },{"Alias","@C",60 }, {"Numero do Registro","99999999",60 },{"Loc.Dig/PEG/Nota","@C",100 } }
	aCab03  := { {"Guia","@C",080} , {"Critica","@C",200 } , {"Descricao","@C",150 },{"Dado","@C",100 }}                     
    aCab04  := { {"Tipo de Critica","@C",080},{"Numero da Guia","@C",080} , {"Matricula","@C",060}, {"Nome do Usuario","@C",080} ,{"Impresso","@C",040 } , {"Codigo do Evento","@C",040 } }
	aCritRea:= {}
	aResum  := {}
	aCritNot:= {}
	aNota	:= {}
	
	
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//|Define Dialogo...                                                         |
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
aSize := MsAdvSize()
AAdd( aObjects, { 001, 020, .T., .F.,.F. } )
AAdd( aObjects, { 001, 000, .T., .T.,.F. } )
AAdd( aObjects, { 001, 100, .T., .F.,.T. } )

aInfo := { aSize[1],aSize[2],aSize[3],aSize[4], 5, 5 }
aPosObj := MsObjSize( aInfo, aObjects )

aPosObj[2][2]+=15
aPosObj[3][2]+=15
aPosObj[2][3]+=100 // 358
aPosObj[3][4]+=60 // 160

aPosObj[3][3]-=15

	DEFINE MSDIALOG oDialog TITLE cCadastro FROM ndLinIni,ndColIni TO ndLinFin,ndColFin OF GetWndDefault()
	
	@ aPosObj[1][1],aPosObj[1][2] FOLDER oFolder SIZE aPosObj[2][3],aPosObj[3][4]+100 OF oDialog PIXEL PROMPTS  OemtoAnsi("Criticas do arquivo"),OemtoAnsi("Guias geradas"),OemtoAnsi("Criticas das guias"),OemtoAnsi("Outras criticas PTU")

	       
	       	                  
	If Len(aCritReal) > 0  
	   
		oCriti01 := TcBrowse():New( 012, 005, 340, 120,,,, oFolder:aDialogs[1],,,,,,,,,,,, .F.,, .T.,, .F., )
		aCritRea := aClone(aCritReal) 
		For nFor := 1 To Len(aCab01)
		     bBlock := "{ || aCritRea[oCriti01:nAt, "+Str(nFor,4)+"] }"
		     bBlock := &bBlock
		     oCriti01:AddColumn(TcColumn():New(aCab01[nFor,1],bBlock,aCab01[nFor,2],nil,nil,nil,aCab01[nFor,3],.F.,.F.,nil,nil,nil,.F.,nil))     
		Next
		oCriti01:SetArray(aCritRea)  
		aCritReal := {}
	Endif
		                           
	If Len(aResumo) > 0
		
		oCriti02 := TcBrowse():New( 012, 005, 340, 120,,,, oFolder:aDialogs[2],,,,,,,,,,,, .F.,, .T.,, .F., )
		aResum	 := aClone(aResumo)
		For nFor := 1 To Len(aCab02)
		     bBlock := "{ || aResum[oCriti02:nAt, "+Str(nFor,4)+"] }"
		     bBlock := &bBlock
		     oCriti02:AddColumn(TcColumn():New(aCab02[nFor,1],bBlock,aCab02[nFor,2],nil,nil,nil,aCab02[nFor,3],.F.,.F.,nil,nil,nil,.F.,nil))     
		Next
		oCriti02:SetArray(aResum)         
		aResumo := {}
	Endif   
	
	If Len(aCritNota) > 0
		
		oCriti03 := TcBrowse():New( 012, 005, 340, 120,,,, oFolder:aDialogs[3],,,,,,,,,,,, .F.,, .T.,, .F., ) 
		For nFor := 1 To Len(aCritNota)
		       nPos := Ascan(aCriticas,{|x| x[1] == aCritNota[nFor,2]})
		       If nPos > 0
		          aCritNota[nFor,2] := aCritNota[nFor,2]+" - "+aCriticas[nPos,2]
		       Endif   
		Next
		aCritNot := aClone(aCritNota)
		For nFor := 1 To Len(aCab03)
		     bBlock := "{ || aCritNot[oCriti03:nAt, "+Str(nFor,4)+"] }"
		     bBlock := &bBlock
		     oCriti03:AddColumn(TcColumn():New(aCab03[nFor,1],bBlock,aCab03[nFor,2],nil,nil,nil,aCab03[nFor,3],.F.,.F.,nil,nil,nil,.F.,nil))     
		Next
		oCriti03:SetArray(aCritNot)         
		aCritNota := {}
	Endif
	
	If Len(aNotas) > 0  
	   
		oCriti04 := TcBrowse():New( 012, 005, 340, 120,,,, oFolder:aDialogs[4],,,,,,,,,,,, .F.,, .T.,, .F., )
		aNota := aClone(aNotas) 
		For nFor := 1 To Len(aCab04)
		     bBlock := "{ || aNota[oCriti04:nAt, "+Str(nFor,4)+"] }"
		     bBlock := &bBlock
		     oCriti04:AddColumn(TcColumn():New(aCab04[nFor,1],bBlock,aCab04[nFor,2],nil,nil,nil,aCab04[nFor,3],.F.,.F.,nil,nil,nil,.F.,nil))     
		Next
		oCriti04:SetArray(aNota)  
		aNotas := {}
	Endif
	
	Aadd(aBut, {"RELATORIO",{ || ImpCriT(aCritRea,aCab01,aResum,aCab02,aCritNot,aCab03,aNota,aCab04,"Criticas","M",132) },"Imprimir"} )

	ACTIVATE MSDIALOG oDialog  ON INIT EnchoiceBar(oDialog,bOK,bCancel,.F.,aBut) CENTER

Elseif Len(aCritReal)+Len(aCritReal)+Len(aResumo)+Len(aNotas) == 0  .and. (lAnalisar .or. lImport)
	MsgInfo("Nao foram encontradas criticas no arquivo.")
Endif	

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Fim da rotina principal...                                               �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
If Valtype(oBrwBRJ) == "O"
   oBrwBRJ:Refresh(.T.)
   oBrwBRJ:ExecuteFilter()
Else
	DbSelectArea("BRJ")
	cPLSFiltro := "@BRJ_FILIAL = '"+xFilial("BRJ")+"' AND BRJ_REGPRI = '3' AND D_E_L_E_T_ = ' '"
	SET FILTER TO &(cPLSFiltro)       
EndIf			 
			 
Return

//+------------------------------------------------------------------
/*/{Protheus.doc} ProcA50
Processamento da Importacao A700  
@author  PLSTEAM
@version P11
@since   17.01.2005
/*/  
//+------------------------------------------------------------------
Static Function ProcA50()
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Define variaveis da rotina...                                            �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
LOCAL cLayPLS
LOCAL cArquivo
LOCAL lImportar
LOCAL lGerarLog
LOCAL cCodLDP
LOCAL bRet       := { || {lOK,lFatal,lImportar,lAnalisar} }
LOCAL lFileOpen
LOCAL aStruc     := { {"CAMPO","C",634,0}}
LOCAL oTempTable
LOCAL nRecSlv
LOCAL aRetFun
LOCAL lFoundUsr
LOCAL lGuiaCriada := .F.
LOCAL nRSeqIni    := 0
LOCAL nFor
LOCAL nPos
LOCAL nRecno               
LOCAL cTipGui    
LOCAL cChvNtPesq      
LOCAL cLote		  := ""	
LOCAL nQtdEveAux := 0
LOCAL lPLS700PU := ExistBlock("PLS700PU")
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� 701																		 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
LOCAL dDatGer              
LOCAL dDatVen                
LOCAL dDatEmi                  
LOCAL nValTot  := 0
LOCAL nValToL  := 0
LOCAL nValAju  := 0
LOCAL cChaveGui                
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� 702																		 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
LOCAL cChav702 := ""                
LOCAL aChav702 := {}
LOCAL lAmbulat := .F.
LOCAL nQtd702  := 0         
LOCAL nQtdDiZ  := 0                   
LOCAL nQtdAmb  := 0
LOCAL cNextLote:= "" 
LOCAL cTpConsult := ""
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� 703																		 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
LOCAL cChav703 := ""                
LOCAL aChav703 := {}
LOCAL nQtd703  := 0     
LOCAL lOnApR703:= .F.
LOCAL nJ       := 1
LOCAL nI	   := 1  
LOCAL lDiaria  := .F.     
LOCAL cCodSer  := ""
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� 704																		 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
LOCAL nQtd704  := 0
LOCAL nVal     := 0
LOCAL nValServ := 0
LOCAL dDatSer  
LOCAL nQtdCob  := 0
LOCAL cChav704 := ""   
LOCAL nFilesTat     
LOCAL aChaUlGu := {}
LOCAL nVal1 := 0
LOCAL nVal2 := 0
LOCAL nVal3 := 0
LOCAL nTot	:= 0   
LOCAL nValBD6Pag := 0
LOCAL nValTotPag := 0
		       		
LOCAL aBD6Nota := {}                                                      
LOCAL cAuxDat  := ""
LOCAL cDataPro := ""
LOCAL cHorPro  := ""                                                       
LOCAL nNextRec := 0            
LOCAL nVlrTot  := 0
LOCAL lCompativel := .F.
LOCAL lFoundBR8 := .F.
LOCAL lConsulta	:= .F.
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� 705																		 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
LOCAL cChave705 := ""                
LOCAL aChav705 := {}
LOCAL nQtd705  := 0
LOCAL lFatal   := .F.
LOCAL cTpNota
LOCAL lAnalisar:= .F.                
			          
PRIVATE lAbortPrint := .F. // Controle para abortar (sempre como esta aqui)
PRIVATE nRecBRJ		:= 0
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Limpa variaveis estaticas                                                �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸                     
aTpPart     := {} 
aCriticas   := {}
aCritReal   := {}
aResumo     := {}
aCritNota   := {}
aNotas		:= {}
lOK         := .F.
_nPosITREG  := 0
_nPosFTREG  := 0 
_nPosISEQ   := 0
_nPosFSEQ   := 0 
cUniOri     := ""
aDadRda     := {}
aDadUsr     := {}
nLine       := 1
cAliasPLS   := ""
aRecnosGer  := {}
cCodSeq	   := ""
_DT_EMI_FAT := nil
_DT_VEM_FAT := nil
_NR_FATURA  := nil
cCodProGen  := nil
lConverProc := nil
lCriadoBRJ  := .F.
nRegraGrvImp:= nil
lImpPTU     := .T.
cMatrCob    := nil
cNomCob 	:= nil
cUltCodigo  := ""
cUltPart    := ""
cRdaUlt     := ""
cDataUlt    := ""
cMatrAntGen := "99999999999999999"
cCodPadCon  := ""
cCodUni702  := ""
aGuiaNaoExi := {}
aRegAtuMov  := {}
lCrit099    := .F.
cDes099     := ""
nImpBD6    := GetNewPar("MV_P700BD6",'0')
lMaiorTrez  := .F.    
nNVerTra	:= 0
aCodEdi		:= {}
cCodLayPLS  := ""
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Mensagem ao usuario...                                                   �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
IncProcG1("Preparando importacao.")
ProcessMessage()
CarregaCrit()
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//쿣erifica qual eh o maior e o menor Recno que satisfaca a selecao          �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
If  GetNewPar("MV_PLS700D","1") == "1" .And. FindFunction("PlsDeReDel")
    IncProcG1("Excluindo fisicamente os registros deletados do BD6 e BD7.")
    ProcessMessage()
	PlsDeReDel({"BD6","BD7"})                                                 
Endif
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Busca parametros...                                                      �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Pergunte(cPerg,.F.)

cLayPLS      := mv_par01
cArquivo     := AllTrim(mv_par02)
lImportar    := mv_par03==1
lGerarLog    := mv_par04
cCodLDP      := mv_par05
cCodProGen  := alltrim(mv_par06)
If !empty(mv_par10+mv_par11)
	cAMPAG   := mv_par10+mv_par11
Endif             
lConverProc  := (mv_par12==2) //1 = Nao , 2 = Sim
cCodPadCon   := mv_par13
nRegraGrvImp := mv_par14             
                                                       
lImpPTU := .T.
                                                       
If !ProcPerImp(cLayPLS,cArquivo,lImportar,lGerarLog,cCodLDP,cCodProGen,cAMPAG,lConverProc,cCodPadCon,;
			   nRegraGrvImp)
	 lFatal := .t.
	 lImportar:= .f.
     Return(Eval(bRet))
Endif      

PLSConTPA()                  

PLSLSC500(@mv_par07,@mv_par08,.F.,@mv_par09)

//--< Cria豫o do objeto FWTemporaryTable >---
oTempTable := FWTemporaryTable():New( "Trb" )
oTemptable:SetFields( aStruc )
oTempTable:AddIndex( "INDTRB1",{ "CAMPO" } )
oTempTable:Create()

IncProcG1("Iniciando Transacao.")
ProcessMessage()
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Inicia a transacao														 |
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Begin Transaction 
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Verifica se layout existe...                                             �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
DE9->(DbSetOrder(1))
If ! DE9->(MsSeek(xFilial("DE9")+cLayPLS))
   AnalisaCrit("01","")
   lFileOpen := .F.
Else
   cCodLayPLS := AllTrim(DE9->DE9_VERLAY)
Endif

lMaiorTrez := alltrim(cCodLayPLS) $ "3.0#3.1#3.2#3.3#3.4#3.5#3.8"
lCrit099   := PLSPOSGLO(PLSINTPAD(),__aCdCri099[1],__aCdCri099[2],cLocalExec)
cDes099    := PLSBCTDESC()
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Regra - Testa existencia do arquivo...                                   �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
If ! File(cArquivo)
   AnalisaCrit("01","")
   lFileOpen := .F.
Else            
   IncProcG1("Abrindo arquivo de origem.")
   ProcessMessage()

   DbSelectArea("Trb")             
   nFileStat := AppFromTXT(cArquivo)

	If nFileStat = 0 // OK
	   lFileOpen := .T.
	   Trb->(DbGoBottom())
	   If Empty(Trb->Campo)
	      Trb->(RecLock("Trb",.F.))
	      Trb->(DbDelete())
	      Trb->(MsUnLock())
	   Endif   
	   Trb->(DbGoTop())
    ElseIf nFileStat = -1
		// Falha de abertura
	   lFileOpen := .F.       
	   MsgStop("Falha na Abertura do Arquivo. Codigo do erro -1")
	elseIF nFileStat = -2
		// Falha de abertura
	   lFileOpen := .F.                                         
	   MsgStop("Falha na Abertura do Arquivo. Codigo do erro -2")
	elseIF nFileStat = -3
		// Falha de abertura
	   lFileOpen := .F.                                         
	   MsgStop("Falha na Abertura do Arquivo. Codigo do erro -3")
	Endif
Endif   

cCodSeq := ""

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Se arquivo de importacao foi lido inicia checagem de regras...           �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
If lFileOpen
   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
   //� Mensagem ao usuario...                                                   �
   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
   BarGauge1Set(Trb->(RecCount()))     
   IncProcG1("Preparando ambiente de processamento.")
   ProcessMessage()
   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
   //� Monta layout para matriz de apoio...                                     �
   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
   MonLayout(cLayPLS)
   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
   //� Regra - Buscar em qual posicao fica o tipo de registro no layout...      �
   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
   DE0->(DbSetOrder(1))
   If DE0->(MsSeek(xFilial("DE0")+cLayPLS))
   
      aRetFun    := PLSA700GPF(AllTrim(DE0->DE0_CODREG),"TP_REG")
      _nPosITREG := aRetFun[1]
      _nPosFTREG := aRetFun[2]
      
      If _nPosITREG == 0 .Or. _nPosFTREG == 0
         AnalisaCrit("03","")
      Endif   

      aRetFun   := PLSA700GPF(AllTrim(DE0->DE0_CODREG),"NR_SEQ")
      _nPosISEQ := aRetFun[1]
      _nPosFSEQ := aRetFun[2]
      
      If _nPosISEQ == 0 .Or. _nPosFSEQ == 0
         AnalisaCrit("04","")
      Endif   
   Else
      AnalisaCrit("05","")
   Endif   
   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
   //� Regra - Testa se layout definido ja esta suportado...                    �
   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
   If Ascan(aLayouts,cCodLayPLS) == 0
      AnalisaCrit("06","")
   Endif   
   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
   //� Validar estrutura fisica do arquivo...                                   �
   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

   While ! Trb->(Eof())
            //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
            //� Exibe mensagem de processamento...                                       �
            //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
            IncProcG1("Analisando estrutura do arquivo [ "+Subs(Trb->Campo,_nPosISEQ,_nPosFSEQ)+" ]")
            ProcessMessage()

            If Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "701"

                   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                   //� Regra - Testa se existe a unimed origem e destino...                     �
                   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
                   BA0->(DbSetOrder(1))
                   If ! BA0->(MsSeek(xFilial("BA0")+PLSA700GCF("R701","CD_UNI_ORI")))
                      AnalisaCrit("08",PL5GetCo("R701","CD_UNI_ORI"),PLSA700GCF("R701","CD_UNI_ORI"))
                   Else
                      BAU->(DbSetOrder(7))
                      If ! BAU->(MsSeek(xFilial("BAU")+PLSA700GCF("R701","CD_UNI_ORI")))
                         AnalisaCrit("09",PL5GetCo("R701","CD_UNI_ORI"),PLSA700GCF("R701","CD_UNI_ORI"))
                      Else
                         aRetFun := PLSDADRDA(cUniDes,BAU->BAU_CODIGO)
                         
                         If ! aRetFun[1]
                            AnalisaCrit("10",PL5GetCo("R701","CD_UNI_ORI"),PLSA700GCF("R701","CD_UNI_ORI"))
                         Else   
                            aDadRda := PLSGETRDA()                           
                            cUniOri := PLSA700GCF("R701","CD_UNI_ORI")
                         Endif   
                      Endif
                   Endif   
                   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                   //� Regra - Testa se a data de geracao eh valida					            �
                   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
 				   dDatGer := stod(PLSA700GCF("R701","DT_GERACAO")) 
				   If ((Year(dDatGer) > Year(dDataBase)) .OR. (Month(dDatGer) < 1 .OR. Month(dDatGer) > 12)) 
                      AnalisaCrit("11",PL5GetCo("R701","DT_GERACAO"),PLSA700GCF("R701","DT_GERACAO"))
				   Endif                                                                       
      
                   If ! BA0->(MsSeek(xFilial("BA0")+PLSA700GCF("R701","CD_UNI_DES")))
                      AnalisaCrit("15",PL5GetCo("R701","CD_UNI_DES"),PLSA700GCF("R701","CD_UNI_DES"))
                   Endif   
                   nNVerTra	:= val(PLSA700GCF("R701","NR_VER_TRA"))
                   
            ElseIf Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "702" 

                   If nNVerTra < 12
	                   lAmbulat := iif(PLSA700GCF("R702","ID_AMBULA") == '1',.T.,.F.)
	               EndIf
            	   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                   //� Regra - Para um mesmo lote, nao podera haver numeros de notas repetidos. |
                   //| A "chave" do registro sera sempre LOTE+NOTA.      						|
                   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
            	   cChav702   := PLSA700GCF("R702","CD_UNI")+PLSA700GCF("R702","NR_LOTE")+PLSA700GCF("R702","NR_NOTA")
            	   cCodUni702 := PLSA700GCF("R702","CD_UNI")
            	   If Ascan(aChav702,{|a| a[1] = cChav702}) <> 0
         		   	  AnalisaCrit("16",PL5GetCo("R702","NR_NOTA"))
         		   Endif            
            	   aadd(aChav702,{cChav702})
	               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                   //� Regra - Tipo de consulta													|
                   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
         		   cTpConsult := ""
         		   If ! PLSA700GCF("R702","TP_CONSULT","041") $ "0,1,2,3"
	               		AnalisaCrit("19",PL5GetCo("R702","TP_CONSULT","012"),PLSA700GCF("R702","TP_CONSULT","041"))
	               Else
	               		cTpConsult := PLSA700GCF("R702","TP_CONSULT","041")
	               Endif 
	               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                   //� Tratamento do tipo de consulta											|
                   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
	               If cCodLayPLS >= '3.2' .and. !Empty(cTpConsult) .and. nNVerTra >= 10
	               		
	                   cLote     := PLSA700GCF("R702","NR_LOTE")+PLSA700GCF("R702","NR_NOTA")
	                   cNextLote := cLote
	                   lConsulta := .F.
	                   nRecno    := Trb->(Recno()) 
	                   BR8->(DbSetOrder(3))  
	                   While !Trb->(Eof()) .and. cLote == cNextLote
					          
	                      	If Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "704"          
	                      		cCodSer := alltrim(PLSA700GCF("R704","CD_SERVICO"))
	                      		If PLSISCON("02",AllTrim(PLSA700GCF("R704","CD_SERVICO"))) .or. PLSISCON("01",AllTrim(PLSA700GCF("R704","CD_SERVICO")))
	                      		    lConsulta := .T.
	                      		    exit
	                      		Endif   	
	                        Endif           
	                      	Trb->(DbSkip())
	                      	
	                      	If 	Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "702"
	                      		cNextLote := PLSA700GCF("R702","NR_LOTE")+PLSA700GCF("R702","NR_NOTA")
	                      	Elseif Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "703"
	                      		cNextLote := PLSA700GCF("R703","NR_LOTE")+PLSA700GCF("R703","NR_NOTA")
	                      	Elseif Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "704"
	                      		cNextLote := PLSA700GCF("R704","NR_LOTE")+PLSA700GCF("R704","NR_NOTA")
	                      	Else
	                      		cNextLote := "###"
	                      	Endif		
	                   Enddo                                                      
	                   
	            	   Trb->(DbGoTo(nRecno))	
	               Endif
	               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                   //� Regra - Indicador do atendimento em Ambulat�rio							|
                   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
                   If nNVerTra < 12 
	         		   If ! PLSA700GCF("R702","ID_AMBULA") $ "1,2"
		             		AnalisaCrit("20",PL5GetCo("R702","ID_AMBULA"),PLSA700GCF("R702","ID_AMBULA"))
		               ElseIf PLSA700GCF("R702","ID_AMBULA") $ "1"
		             	    nQtdAmb ++
		               Endif
		           EndIf
	               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                   //� Regra - Indicador do tipo de paciente									|
                   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸	               	
         		   BGY->(DbSetOrder(1))
         		   If ! BGY->(MsSeek(xFilial("BGY")+PLSINTPAD()+PLSA700GCF("R702","TP_PACIENT")))
	               		AnalisaCrit("21",PL5GetCo("R702","TP_PACIENT"),PLSA700GCF("R702","TP_PACIENT"))
         		   Endif   
	               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                   //� Regra - Tipo de saida para atendimento de consulta e SADT				|
                   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸	               	
         		   nRecno := Trb->(Recno())
				   Trb->(DbSkip())
         		   If Subs(Trb->Campo,_nPosITREG,_nPosFTREG) <> "703" .AND. lMaiorTrez .and. nNVerTra >= 8
         		   		Trb->(DbGoTo(nRecno))
         		   		If !(PLSA700GCF("R702","TP_SAIDCON","018") $ "1,2,3,4,5,6,9")
         		   			AnalisaCrit("61",PL5GetCo("R702","TP_SAIDCON"),PLSA700GCF("R702","TP_SAIDCON","018"))
         		   		Endif
         		   Endif                     
         		   Trb->(DbGoTo(nRecno))
                   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                   //� Checa a regra da guia principal											|
                   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
                   If cCodLayPLS >= '3.2' .and. nNVerTra >= 10
	                   If PLSA700GCF("R702","TP_ATENDIM","019") == "07" .and. Empty(PLSA700GCF("R702","NR_GUIA_PR","020"))
	                    	AnalisaCrit("65",PL5GetCo("R702","NR_GUIA_PR"),PLSA700GCF("R702","NR_GUIA_PR","020"))
	                   Endif
                   Endif
                   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                   //� Regra - Quantidade total de registros									|
                   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
                   nQtd702 ++
            ElseIf Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "703" 
                   nQtd703 ++                      	                                         
                   //guardo o lote atual
                   cLote     := PLSA700GCF("R703","NR_LOTE")+PLSA700GCF("R703","NR_NOTA")
                   cNextLote := cLote
                   lDiaria   := .F.
                   nRecno    := Trb->(Recno()) 
                   BR8->(DbSetOrder(3))  
                   While !Trb->(Eof()) .and. cLote == cNextLote
				          
                      	If Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "704"          
                      		cCodSer := alltrim(PLSA700GCF("R704","CD_SERVICO"))
                      		If BR8->(MsSeek(xFilial("BR8")+cCodSer)) .and. BR8->BR8_TPPROC == '4'
                      		    lDiaria := .T.
                      		    exit
                      		Endif   	
                        Endif           
                      	Trb->(DbSkip())
                      	
                      	If 	Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "702"
                      		cNextLote := PLSA700GCF("R702","NR_LOTE")+PLSA700GCF("R702","NR_NOTA")
                      	Elseif Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "703"
                      		cNextLote := PLSA700GCF("R703","NR_LOTE")+PLSA700GCF("R703","NR_NOTA")
                      	Elseif Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "704"
                      		cNextLote := PLSA700GCF("R704","NR_LOTE")+PLSA700GCF("R704","NR_NOTA")
                      	Else
                      		cNextLote := "###"
                      	Endif		
                   Enddo
                      	
            	   Trb->(DbGoTo(nRecno))	
				   If !lDiaria	.And.  nNVerTra >= 8 .And. lMaiorTrez
				   		AnalisaCrit("63","")
				   Endif
                   
                   If (!lMaiorTrez .And. nNVerTra <= 7 .And. cTpNota $ "3,4,5" .And. ! lAmbulat) .or. ;
                   	  ( lMaiorTrez .And. nNVerTra < 12 .And. ! lAmbulat)
               	      //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                      //� Regra - Para um mesmo lote, nao podera haver numeros de notas repetidos. |
                      //| A "chave" do registro sera sempre LOTE+NOTA.      					   |
                      //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
            	      cChav703 := cCodUni702+PLSA700GCF("R703","NR_LOTE")+PLSA700GCF("R703","NR_NOTA")
            	      If cChav702 != cChav703
         		   	     		AnalisaCrit("22",PL5GetCo("R703","NR_NOTA"))
         			  Endif
              	      If Ascan(aChav703,{|a| a[1] = cChav703}) <> 0
         		   	   		AnalisaCrit("23",PL5GetCo("R703","NR_NOTA"))
         		      Endif            
            	      aadd(aChav703,{cChav703})      
	                  //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                      //� Regra - Tipo de nascimento											   |
                      //| Obrigatorio quando o campo TP_INTERNACAO for igual a 3 - Internacao      |
                      //| Obstetrica.															   |
                      //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸	
                      If PLSA700GCF("R703","TP_INTERNA") == "3"	 // obstetrica
                      	
	                  	    If PLSA700GCF("R703","ID_OBT1","022") <> "S" .and. PLSA700GCF("R703","ID_OBT2","023") <> "S" .and.;
		                       PLSA700GCF("R703","ID_OBT3","024") <> "S" .and.  PLSA700GCF("R703","ID_OBT4","025")<> "S" .and.;
		                       PLSA700GCF("R703","ID_OBT5","026") <> "S" .and.  PLSA700GCF("R703","ID_OBT6","027")<> "S" .and.;
		                       PLSA700GCF("R703","ID_OBT7","028") <> "S" .and. PLSA700GCF("R703","ID_OBT8","029") <> "S" .and.;
		                       PLSA700GCF("R703","ID_OBT9","030") <> "S"                                                   
		                            
		                       	AnalisaCrit("68",PL5GetCo("R703","ID_OBT9","030"),PLSA700GCF("R703","ID_OBT9","030"))
		                    Endif
	                  	
	                  Endif                                                                         
	                  //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                      //� Regra - Tipo de nascimento											   |
                      //| Quando TP_OBSTETRICIA = 11 ou TP_OBSTETRICIA = 12, ao menos um dos campos| 
                      //| das sequencias 017 a 021 devem ser preenchidas. Nos casos de partos      |
	                  //| mltiplos, mais de um dos campos das seq獪ncias 017 a 021 poder�o ser 	   |	
	                  //| preenchidos.                                                             |
                      //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸                                                                                     
	                  	If val(PLSA700GCF("R703","QT_NAVIVOS","017")) > 0 .or. val(PLSA700GCF("R703","QT_NAVIVOP","019")) > 0
                   	  	       
                   	  		If Empty(PLSA700GCF("R703","NR_VIVO1","034")) .and. Empty(PLSA700GCF("R703","NR_VIVO2","035")) .and. ;
                   	  		   Empty(PLSA700GCF("R703","NR_VIVO3","036")) .and. Empty(PLSA700GCF("R703","NR_VIVO4","037")) .and. ;
                   	  		   Empty(PLSA700GCF("R703","NR_VIVO5","038")) 
                   	  			
                   	  			AnalisaCrit("69",PL5GetCo("R703","NR_VIVO1","034"),PLSA700GCF("R703","NR_VIVO1","034"))
                   	  		Endif
                   	  	Endif
                   	  
	               Endif
	               
            ElseIf Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "704" 
            	     //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커 
                     //| A "chave" do registro sera sempre LOTE+NOTA.      						|
                     //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
            	   	  cChav704 := cCodUni702+PLSA700GCF("R704","NR_LOTE")+PLSA700GCF("R704","NR_NOTA")
            	      If cChav702 != cChav704 
         			   		AnalisaCrit("26",PL5GetCo("R704","NR_NOTA"))
       				  Endif     
	                 //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                     //� Regra - Procedimento...      											|
                     //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
                     PLBusProTab(AllTrim(PLSA700GCF("R704","CD_SERVICO")),.F.,PLSA700GCF("R704","DT_SERVICO"),nil,lConverProc,cUniOri,cCodProGen,cCodPadCon,cCodPaPro)
	                   			   
                     If ! BR8->(Found())
                        AnalisaCrit("45",PL5GetCo("R704","CD_SERVICO"),AllTrim(PLSA700GCF("R704","CD_SERVICO")))
                     Endif
	                 //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                     //� Regra - Data de Servico													|
                     //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸 
                      dDatSer := stod(PLSA700GCF("R704","DT_SERVICO")) 
					  If Empty(dDatSer)
                       AnalisaCrit("27",PL5GetCo("R704","DT_SERVICO"),PLSA700GCF("R704","DT_SERVICO"))
					  Endif 
	              //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                 //� Regra - Quantidade Cobrada												|
                 //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
 	              		nQtdCob += val(PLSA700GCF("R704","QT_PAGA"))
 	             
 	             //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                 //� Regra - CPF/CNPJ														  |
                 //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸   
					  If ! PLSA700GCF("R704","TP_PESSOA") $ "F,J" 
                     	AnalisaCrit("31",PL5GetCo("R704","TP_PESSOA"),PLSA700GCF("R704","TP_PESSOA"))
       				  Endif                       
       				  
       				  lFoundBR8 := BR8->(Found())  
       				  //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                      //� Regra - Que trata o valor total das notas								|
                      //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸                                     
			          nVal1 := val(Subs(PLSA700GCF("R704","VL_SERV_CO"),1,12)+"."+Subs(PLSA700GCF("R704","VL_SERV_CO"),13,2))
			          nVal2 := val(Subs(PLSA700GCF("R704","VL_CO_COB"),1,12)+"."+Subs(PLSA700GCF("R704","VL_CO_COB"),13,2))
			          nVal3 := val(Subs(PLSA700GCF("R704","VL_FILME_C"),1,12)+"."+Subs(PLSA700GCF("R704","VL_FILME_C"),13,2))
				   	  
				   	  
       				  If Len(aDadRda) >= 18                                   
       				  
						        aRdas := PLS720IBD7("0",0,BR8->BR8_CODPAD,BR8->BR8_CODPSA,"",cUniDes,BAU->BAU_CODIGO,;
						       			 '','','','',aDadRda[15],aDadRda[18],"3",'',"1","02",;
						           		 STOD(PLSA700GCF("R704","DT_SERVICO")))
								lCompativel := .F.     
								
								//verifico se ele nao eh compativem com nenhuma
								For nI:=1 to Len(aRdas)
								    If   Len(aRdas[nI]) >= 9 .and.;
								    	 !ProcHM(AllTrim(aRdas[nI][1]),AllTrim(PLSA700GCF("R704","TP_PARTICI")),aRdas[nI][9],PLSA700GCF("R704","CD_PORTE_A")) .and. ;
										 !ProcCOP(AllTrim(aRdas[nI][1]),AllTrim(PLSA700GCF("R704","TP_PARTICI"))) .and. ;   
										 !ProcFIL(AllTrim(aRdas[nI][1]),AllTrim(PLSA700GCF("R704","TP_PARTICI")))  
										 	lCompativel := .F.
								    Else
								    		lCompativel := .T.
								    		exit
								    Endif
								Next     
								
								If !lCompativel  .and. lFoundBR8
									AnalisaCrit("62",PL5GetCo("R704","CD_SERVICO"),cChav704)
								Endif
								
								If ((nVal1 > 0 .and. (nVal2 > 0 .or. nVal3 > 0)) .or.;
						             (nVal2 > 0 .and. (nVal1 > 0 .or. nVal3 > 0)) .or.;
						             (nVal3 > 0 .and. (nVal2 > 0 .or. nVal1 > 0))) .and.;
						             lFoundBR8
											           		     
			                            nTot := 0
			                            If nVal1 > 0
			                            	nTot++
			                            Endif
			                            If nVal2 > 0
			                            	nTot++
			                            Endif
			                            If nVal3 > 0
			                            	nTot++
			                            Endif                             
			                            
							 			If Len(aRdas) < nTot
							     			AnalisaCrit("58",PL5GetCo("R704","CD_SERVICO"),cChav704)
				       				 	Endif  		
			       				Endif
			       				// se o procedimento nao tem composi豫o na TDE
			       				If Len(aRdas) == 0 .and. lFoundBR8
			       				   AnalisaCrit("48",PL5GetCo("R704","CD_SERVICO"))   
			       				Endif
				      Endif 		
				      
					  If (lConsulta .and. alltrim(cTpConsult) $ "1" .and. AllTrim(PLSA700GCF("R704","CD_SERVICO"))<>"10101012" ) .or. (!lConsulta .and. alltrim(cTpConsult) $ "2,3" .and. AllTrim(PLSA700GCF("R704","CD_SERVICO"))<>"10101012" ) 
	                   		AnalisaCrit("64",PL5GetCo("R704","CD_SERVICO"),PLSA700GCF("R704","CD_SERVICO"))
	                  Endif
			          nValServ += val(Subs(PLSA700GCF("R704","VL_SERV_CO"),1,12)+"."+Subs(PLSA700GCF("R704","VL_SERV_CO"),13,2))
			          nVal     += nVal1+nVal2+nVal3
				   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                   //� Regra - Quantidade total de registros									|
                   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
	                   	nQtd704 ++
            ElseIf Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "705" 
            	   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                   //� Regra - Para um mesmo lote, nao podera haver numeros de notas repetidos. |
                   //| A "chave" do registro sera sempre LOTE+NOTA.      						|
                   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

            	   		cChave705 := PLSA700GCF("R705","NR_LOTE")+PLSA700GCF("R705","NR_NOTA")
            	   		If Ascan(aChav705,{|a| a[1] = cChave705}) <> 0
         			   	   		AnalisaCrit("32",PL5GetCo("R705","NR_NOTA"))
         			   	Endif            
            	   		aadd(aChav705,{cChave705})

				   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                   //� Regra - Quantidade total de registros									|
                   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
                   nQtd705 ++
            ElseIf Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "709"
					//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
					//� Regra - Que trata o valor total das notas								 |
					//| QT_TOT_R702	Quantidade total de registros do tipo 702.                   |
					//| QT_TOT_R703	Quantidade total de registros do tipo 703.                   |
					//| QT_TOT_R704	Quantidade total de registros do tipo 704.                   |
					//| QT_TOT_R705	Quantidade total de registros do tipo 705.                   |
					//| QT_NOT_EXC	Quantidade total de notas com exce豫o diferente de 0.        |
					//| QT_NOT_AMB	Quantidade total de notas com Ambulat�rio igual a 1 (sim).   |
					//| QT_TOT_SER	Quantidade total de servi�os dos registros 704.              |
					//| VL_TOT_SERV	Valor total de servi�os nos registros 704.                   |
					//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸 
						If val(PLSA700GCF("R709","QT_TOTR702")) != nQtd702 
							AnalisaCrit("33","Quantidade no campo QT_TOTR702 ["+PLSA700GCF("R709","QT_TOTR702")+"], quantidade no arquivo ["+STRZERO(nQtd702 ,5)+"]")
						Endif
						If val(PLSA700GCF("R709","QT_TOTR703")) != nQtd703 
							AnalisaCrit("34","Quantidade no campo QT_TOTR703 ["+PLSA700GCF("R709","QT_TOTR703")+"], quantidade no arquivo ["+STRZERO(nQtd703 ,5)+"]")
						Endif
						If val(PLSA700GCF("R709","QT_TOTR704")) != nQtd704
							AnalisaCrit("35","Quantidade no campo QT_TOTR704 ["+PLSA700GCF("R709","QT_TOTR704")+"], quantidade no arquivo ["+STRZERO(nQtd704 ,5)+"]")
						Endif
						If val(PLSA700GCF("R709","QT_TOTR705")) != nQtd705 
       						AnalisaCrit("36","Quantidade no campo QT_TOTR705 ["+PLSA700GCF("R709","QT_TOTR705")+"], quantidade no arquivo ["+STRZERO(nQtd705 ,5)+"]")
       					Endif 
       					If cCodLayPLS < '4.0'
	                  		If val(PLSA700GCF("R709","QT_NOT_EXC")) != nQtdDiZ 
	       						AnalisaCrit("37",PL5GetCo("R709","QT_NOT_EXC"))
	       					Endif           
       					Endif
       					If nNVerTra < 12
	                  		If val(PLSA700GCF("R709","QT_NOT_AMB")) != nQtdAmb 
	       						AnalisaCrit("38",PL5GetCo("R709","QT_NOT_AMB"))
	       					Endif                                              
	       				EndIf
       					If val(PLSA700GCF("R709","QT_TOT_SER"))  != nQtdCob
	       						AnalisaCrit("39","Quantidade informada no campo QT_TOT_SER ["+alltrim(sTR(val(PLSA700GCF("R709","QT_TOT_SER"))))+"] diferente do total contido no campo QT_PAGA ["+alltrim(str(nQtdCob))+"]")
	       				Endif
                  		If val(Subs(PLSA700GCF("R709","VL_TOT_SER"),1,12)+"."+Subs(PLSA700GCF("R709","VL_TOT_SER"),13,2)) != nValServ 
       					   	AnalisaCrit("40","Valor do campo VL_TOT_SER ["+TransForm(val(Subs(PLSA700GCF("R709","VL_TOT_SER"),1,12)+"."+Subs(PLSA700GCF("R709","VL_TOT_SER"),13,2)),pMoeda2)+"] diferente do total contido em VL_SERV_CO ["+transform(nValServ,pMoeda2)+"]")
       					Endif
            Endif                               
            
      		Trb->(DbSkip())
      		nRSeqIni ++
      		nLine ++
      Enddo   

   	  IncProcG1("Analise finalizada com sucesso.")     
      Trb->(DbGoTop())                      
      aRecnosGer := {}
      BarGauge2Set(nRSeqIni)      
      //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      //� INICIO DO PROCESSAMENTO... Somente se tudo foi validado [lImpPTU]        �
      //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
      If lImpPTU .And. lImportar
         lOK := .T.                            
         nTot := 0
         While ! Trb->(Eof())
               IncProcG2("["+AllTrim(Str(nTot))+" ] De [ "+AllTrim(Str(nRSeqIni))+" ] "+AllTrim(Str((nTot*100)/nRSeqIni,3))+"% Concluido ")
               
               ProcessMessage()
		              
               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
               //� R701 - HEADER                                                            �
               //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
               If     Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "701"
                      nTot++

                      _DT_EMI_FAT := stod(PLSA700GCF("R701","DT_INI_PGT"))//NO 700 USO O CAMPO EMISSAO PARA DEFINIR A DATA INICIO PARA PAGAMENTO
                      _DT_VEM_FAT := stod(PLSA700GCF("R701","DT_FIN_PGT"))//NO 700 USO O CAMPO EMISSAO PARA DEFINIR A DATA FIM PARA PAGAMENTO
                      _NR_FATURA  := PLSA700GCF("R701","NR_SEQ_GER")
                      
                      nNVerTra	:= val(PLSA700GCF("R701","NR_VER_TRA"))
               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
               //� R702 - NOTA DE COBRANCA                                                  �
               //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
               ElseIf Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "702"                                         
					  nTot++
                      cUltCodigo := ""
                      cUltPart   := ""
                      cRdaUlt    := ""
                      cDataUlt   := "" 
                      cTipGui	 := ""
                      cAliasPLS	 := ""
                           
                      
                      If lOnApR703                               					   
                      		AnalisaCrit("56",cLote)
                      		PLCrProGen(aChaUlGu,cCodProGen,PLSA700GCF("R702","DT_ATEND"),;
	                      				   cCodSeq,PLSA700GCF("R704","CD_VIA_ACE"),nRegraGrvImp,;
	                      				   PLSA700GCF("R702","NR_LOTE"),PLSA700GCF("R702","NR_NOTA"))
                      		lOnApR703 := .F.  
                      Endif
                      //guardo o lote atual
                      cLote := PLSA700GCF("R702","NR_LOTE")+PLSA700GCF("R702","NR_NOTA")
                      cNextLote := cLote 
                      aBD6Nota  := {}
                      nRecno    := Trb->(Recno()) 
                      
                      While !Trb->(Eof()) .and. cLote == cNextLote				          
	                      	If Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "704"          
	                      	
	                        	cAuxDat  := PLSA700GCF("R704","DT_SERVICO")            
	  					    	cDataPro := subs(cAuxDat,7,2)+"/"+subs(cAuxDat,5,2)+"/"+subs(cAuxDat,1,4)
					        	cHorPro  := PLSA700GCF("R704","HR_REALIZ")
					        
	                      		If (nI:=aScan(aBD6Nota,{|x| x[1]+x[2]+x[4] == alltrim(PLSA700GCF("R704","CD_SERVICO"))+cDataPro+cHorPro }))  == 0
	                      		
		                      		aadd(aBD6Nota,{ alltrim(PLSA700GCF("R704","CD_SERVICO")),;
		                      					    cDataPro,;
		                      					    {{alltrim(PLSA700GCF("R704","TP_PARTICI")),Trb->(Recno())}} ,;
		                      					    cHorPro})
		                      					    				   
	                      		Else               
		                      		aadd(aBD6Nota[nI][3],{alltrim(PLSA700GCF("R704","TP_PARTICI")),Trb->(Recno())} )
	                      		Endif
	                      	Endif
	                                   
	                      	Trb->(DbSkip())
	                      	
	                      	If 	Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "702"
	                      		cNextLote := PLSA700GCF("R702","NR_LOTE")+PLSA700GCF("R702","NR_NOTA")
	                      	Elseif Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "703"
	                      		cNextLote := PLSA700GCF("R703","NR_LOTE")+PLSA700GCF("R703","NR_NOTA")
	                      	Elseif Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "704"
	                      		cNextLote := PLSA700GCF("R704","NR_LOTE")+PLSA700GCF("R704","NR_NOTA")
	                      	Else
	                      		cNextLote := "###"
	                      	Endif		
                      Enddo
                      	
                      Trb->(DbGoTo(nRecno))	
                      lGuiaCriada := .F.                                             
                      If nNVerTra < 12
                          lAmbulat := iif(PLSA700GCF("R702","ID_AMBULA") == '1',.T.,.F.)                                         
	                      If lAmbulat  //se for ambulatorial eh consulta, mas se tiver mais de um procedimento vai ser guia de servico 
		                     	cTipGui   := "01"
		                     	cAliasPLS := "BD5"
		                  Else
		                  		cTipGui := "02"   
		                    	cAliasPLS := "BD5"
		                  Endif
		              EndIf        
                      
                      nRecno  := Trb->(Recno()) 
                      Trb->(DbSkip())
                      If ! Trb->(Eof()) .and. Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "703"
                      		If cAliasPLS <> "BE4"
                      			cTipGui := "03"   
                            	cAliasPLS := "BE4"
                         	Endif
                      Endif
                      Trb->(DbGoTo(nRecno))
                      
                      //buscar senha NR_AUTORIZ no 704
                      nRecno := Trb->(Recno())
                      
                      nQtdEveAux := 0   
                      lConsulta := .F.
                      Trb->(DbSkip()) //p passar o 702
                      While ! Trb->(Eof())                           
      
                            If Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "704"
                               nQtdEveAux ++
                               If !lConsulta              
                            		lConsulta := PLSISCON("02",AllTrim(PLSA700GCF("R704","CD_SERVICO"))) .or. PLSISCON("01",AllTrim(PLSA700GCF("R704","CD_SERVICO")))
                               Endif
                            Endif        
                            If Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "702" //ja estamos na proxima guia sair do while...
                               Exit
                            Endif   

                      Trb->(DbSkip())
                      Enddo 
                      
                      Trb->(DbGoTo(nRecno))
                      //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                      //� Se nNVerTra for >= 12 ainda nao tratou a guia de consulta devido a  	   �
                      //| retirada de uso do campo ID_AMBULA                                       |
                      //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸         
	                  If nNVerTra >= 12 .And. Empty(cTipGui)
						 cTipGui   := "01"
                         cAliasPLS := "BD5"
                      EndIf
                      //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                      //� Se for guia de consulta, tiver + de um evento eu transformo em guia de   |
                      //| servi�o		                                                           �
                      //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸                      
                      If cTipGui == "01" .And. (nQtdEveAux >= 2 .or. !lConsulta)
                         cTipGui   := "02"
                         cAliasPLS := "BD5"
                      Endif
                      //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                      //� Se for guia de servico, tiver um evento e ele for consulta transformo em |
                      //| guia de consulta                                                         �
                      //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
                      If cTipGui == "02" .And. nQtdEveAux == 1 .and. lConsulta
                      	 cTipGui   := "01"
                         cAliasPLS := "BD5"
                      Endif
                                
                      //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                      //� 1o caminho. Caso a nota tivesse sido autorizada vou confrotar dados que a�
                      //� operadora origem esta cobrando com o que foi autorizado.                 �
                      //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
                      If     nRegraGrvImp == 1
                             cChvNtPesq := Subs(PLSA700GCF("R702","NR_LOTE"),4,5)+PLSA700GCF("R702","NR_NOTA")
                      ElseIf nRegraGrvImp == 2
                             cChvNtPesq := PLSA700GCF("R702","NR_NOTA")
                      ElseIf nRegraGrvImp == 3
                             cChvNtPesq := cUniOri+PLSA700GCF("R702","NR_NOTA")
                      Endif   
                      
                      cMatrCob := PLSA700GCF("R702","CD_UNI")+PLSA700GCF("R702","ID_BENEF")
                      cNomCob  := PLSA700GCF("R702","NM_BENEF")
					  aChaUlGu := {}
					  
					  If Empty(cCodSeq)
  					     AnalisaPEG(cUniDes,BAU->BAU_CODIGO,cAMPAG,cTipGui,cCodLDP,cArquivo,.T.)
  					  Endif   
					  
                      //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                      //� Buscar usuario...                                                        �
                      //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
                      aRetFun := PLSDADUSR(PLSA700GCF("R702","CD_UNI")+PLSA700GCF("R702","ID_BENEF"),"1",.F.,dDataBase)
                      lUsrGen := .F.
                        
                      If ! aRetFun[1]
                           aRetFun := PLSDADUSR(PLSA700GCF("R702","CD_UNI")+PLSA700GCF("R702","ID_BENEF"),"2",.F.,dDataBase)
                           If ! aRetFun[1]
                              aRetFun := PLSDADUSR(Subs(PLSA700GCF("R702","CD_UNI"),2,3)+PLSA700GCF("R702","ID_BENEF"),"2",.F.,dDataBase)                              
                              
                              If ! aRetFun[1] .And. lPLS700PU
                                 lFoundUsr := ExecBlock("PLS700PU",.F.,.F.,{PLSA700GCF("R702","CD_UNI"),PLSA700GCF("R702","ID_BENEF")})
                                 
                                 If lFoundUsr
                                    aRetFun := PLSDADUSR(BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG+BA1_DIGITO),"1",.F.,dDataBase)                              
                                 Endif
                              Endif
                              
                              If ! aRetFun[1]
                                 lFoundUsr := PLSUsrGen(PLSA700GCF("R702","CD_UNI"))
                                 
                                 If ! lFoundUsr
                                    aadd(aCritNota,{PLSA700GCF("R702","NR_LOTE")+PLSA700GCF("R702","NR_NOTA"),"02","Nao foi possivel criar o usuario generico",PLSA700GCF("R702","CD_UNI")+PLSA700GCF("R702","ID_BENEF")})
                                    lFoundUsr := .F.
                                 Else
                                    lUsrGen := .T. 
                                    aRetFun := PLSDADUSR(BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG+BA1_DIGITO),"1",.F.,dDataBase)                              
                                    If ! aRetFun[1]
                                       aadd(aCritNota,{PLSA700GCF("R702","NR_LOTE")+PLSA700GCF("R702","NR_NOTA"),"02",IF(Len(aRetFun)>=2,aRetFun[2,1,2],"Nao foi possivel criar o usuario generico"),PLSA700GCF("R702","CD_UNI")+PLSA700GCF("R702","ID_BENEF")})
                                       lFoundUsr := .F.
                                    Else
                                       lFoundUsr := .T.   
                                       aDadUsr   := PLSGETUSR()
                                    Endif   
                                 Endif   
                              Else
                                 lFoundUsr := .T.   
                                 aDadUsr   := PLSGETUSR()
                              Endif   
                           Else
                              lFoundUsr := .T.   
                              aDadUsr   := PLSGETUSR()
                           Endif
                        Else
                           aDadUsr   := PLSGETUSR()
                           lFoundUsr := .T.   
					  Endif
						
					  If lFoundUsr
      						//Reposiciona Usuario cfme data de atendimento
                            cDatAte := PLSA700GCF("R702","DT_ATEND")
                            dDatAte := ctod(subs(cDatAte,9,2)+"/"+subs(cDatAte,6,2)+"/"+subs(cDatAte,1,4))
      						lMsgUsr := AltCodUsr(dDatAte)
      						If ! lMsgUsr
                                aadd(aCritNota,{PLSA700GCF("R702","NR_LOTE")+PLSA700GCF("R702","NR_NOTA"),"47","Erro no hist�rico de transfer�ncias do usu�rio.",PLSA700GCF("R702","CD_UNI")+PLSA700GCF("R702","ID_BENEF")})
            				Endif
							//Alimenta dados do usuario
							aDadUsr := PLSGETUSR()
                      Endif

                      If lFoundUsr
                           
                           AnalisaPEG(cUniDes,BAU->BAU_CODIGO,cAMPAG,cTipGui,cCodLDP,cArquivo)
                           
                           //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                           //� Cria o arquivo de cabecalho (Guia de servico ou GIH)....                 �
                           //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
                           If cAliasPLS == "BD5"
                              
                              aChaUlGu    := CriaGuiaSV(cAMPAG)                         
                              lGuiaCriada := .T. 
                              
                              aadd(aRecnosGer,{"BD5",BD5->(Recno())})
                              DbSelectArea(cAliasPLS)
                              aadd(aResumo,{PLSA700GCF("R702","NR_LOTE")+PLSA700GCF("R702","NR_NOTA"),"",cAliasPLS,Recno(),RetGuia(cAliasPLS)})
                           Else
                              
                              aChaUlGu := CriaGuiaHS(cAMPAG)                         
                              lGuiaCriada := .T. 
                              
                              aadd(aRecnosGer,{"BE4",BE4->(Recno())})
                              DbSelectArea(cAliasPLS)
                              aadd(aResumo,{PLSA700GCF("R702","NR_LOTE")+PLSA700GCF("R702","NR_NOTA"),"",cAliasPLS,Recno(),RetGuia(cAliasPLS)})
                           Endif
                      Endif
                     
               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
               //� R703 - HOSPITALAR                                                        �
               //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
               ElseIf Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "703" .And. cTipGui == "03" .and. (! lAmbulat .or. cAliasPLS == "BE4")

                      If lGuiaCriada
                                       
                         AtualizaDadosGIH()

                      Endif           
                      
                      lOnApR703 := .T.   
				      nTot++
               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
               //� R704 - SERVICO                                                           �
               //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
               ElseIf Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "704"
                      //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                      //� Atualizacao do BD6/BD7...                                                �
                      //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
                      If lGuiaCriada

                         //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
                      	 //� Agora a CriaEvento() eh chamada em uma ordem pre-estabelecida por mim,   |
                      	 //| isso porque a unimed pode apresentar um arquivo ptu com uma participacao |
                      	 //| de cirurgiao lah na primeira linha e o anestesista la na ultima, e eu    |
                      	 //| tenho que criar um unico BD6											  �
                      	 //| Antes de efetuar qq alteracao neste ciclo consultar o DAHER			  |
                         //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
                         If Len(aBD6Nota) > 0
                         	nMaxRecno := 0 
                         	For nI:=1 to Len(aBD6Nota)                                             
                         		//quando tiver o tipo X, o 0 sempre deve vir primeiro
		                        If aScan(aBD6Nota[nI][3],{|x| upper(x[1]) == 'X'}) > 0
		                        	aBD6Nota[nI][3] := aClone(aSort(aBD6Nota[nI][3],,,{|x,y| x[1] < y[1] }))                         
		                        Endif
                         		For nJ:=1 to Len(aBD6Nota[nI][3])                         			
                         			If aBD6Nota[nI][3][nJ][2] > nMaxRecno
                         				nMaxRecno := aBD6Nota[nI][3][nJ][2]
                         			Endif
                         			Trb->(DbGoTo(aBD6Nota[nI][3][nJ][2])) 
                         			nTot++
                         			IncProcG2("["+AllTrim(Str(nTot))+" ] De [ "+AllTrim(Str(nRSeqIni))+" ] "+AllTrim(Str((nTot*100)/nRSeqIni,3))+"% Concluido ")
               						ProcessMessage()

                         			If nJ < Len(aBD6Nota[nI][3])
                         			    nNextRec := aBD6Nota[nI][3][nJ+1][2]
                         			Else             
                         				If nI < Len(aBD6Nota)
                         					nNextRec := aBD6Nota[nI+1][3][1][2]
                         				Else
                         					nNextRec := 0
                         				Endif
                         			Endif
                         			CriaEvento(nNextRec,aBD6Nota[nI][3],nJ)
                         		Next 
                         	Next 
                         	Trb->(DbGoTo(nMaxRecno))
                         Endif

                      Endif   
                      
                      lOnApR703 := .F.   
               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
               //� R705 - COMPLEMENTO                                                       �
               //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
               ElseIf Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "705"
           			nTot++
               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
               //� R709 - TRAILER                                                           �
               //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
               ElseIf Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "709"
					nTot++
               Endif    

         If Interrupcao(lAbortPrint)
            lImpPTU := .F.
            Exit
         Endif   
         //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
         //� Acessa proximo registro do arquivo de importacao...       .              �
         //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
         Trb->(DbSkip())
         Enddo
     Else
         If lImportar
            MsgStop("O arquivo nao foi importado devido a criticas fatais.")
         Endif   
         
         lFatal := .T.
     Endif    
   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
   //� Fecha o arquivo de trabalho...                                           �
   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸                           
   Trb->(DbGoBottom())
Endif  

if( select( "Trb" ) > 0 )
	oTempTable:Delete()
endIf
  
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Exclui Subitens que nao tiveram participacao...                          �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
For nFor := 1 To Len(aRecnosGer)
    IncProcG2("Atualizacoes finais [ "+AllTrim(Str(nFor))+" ] De [ "+AllTrim(Str(Len(aRecnosGer)))+" ] ")//[ "+AllTrim(Str((nFor*100)/Len(aRecnosGer),3))+"% Concluido ] ")
    ProcessMessage()
    cAliasPLS := aRecnosGer[nFor,1]        
    DbSelectArea(cAliasPLS)
    nRecno := aRecnosGer[nFor,2]
    DbGoto(nRecno)
    BD7->(DbSetOrder(1))
    cChaveGui := xFilial(cAliasPLS)+&(cAliasPLS+"->"+cAliasPLS+"_CODOPE")+&(cAliasPLS+"->"+cAliasPLS+"_CODLDP")+&(cAliasPLS+"->"+cAliasPLS+"_CODPEG")+&(cAliasPLS+"->"+cAliasPLS+"_NUMERO")+&(cAliasPLS+"->"+cAliasPLS+"_ORIMOV")

	cSQL := "SELECT SUM(BD6_QTDPRO) QTDPRO, SUM(BD6_VLRPAG) VLRPAG FROM "+RetSqlName("BD6")+" WHERE "
    cSQL += "BD6_FILIAL = '"+xFilial(cAliasPLS)+"' AND "
    cSQL += "BD6_CODOPE = '"+&(cAliasPLS+"->"+cAliasPLS+"_CODOPE")+"' AND "
    cSQL += "BD6_CODLDP = '"+&(cAliasPLS+"->"+cAliasPLS+"_CODLDP")+"' AND "
    cSQL += "BD6_CODPEG = '"+&(cAliasPLS+"->"+cAliasPLS+"_CODPEG")+"' AND "
    cSQL += "BD6_NUMERO = '"+&(cAliasPLS+"->"+cAliasPLS+"_NUMERO")+"' AND "  
    cSQL += "BD6_ORIMOV = '"+&(cAliasPLS+"->"+cAliasPLS+"_ORIMOV")+"' AND "    
    cSQL += "D_E_L_E_T_ <> '*' "
    PLSQuery(cSQL,"Trb")
    
    If ! Trb->(Eof())
          
          &(cAliasPLS+"->(Reclock('"+cAliasPLS+"',.F.))")
          	&(cAliasPLS+"->"+cAliasPLS+"_QTDEVE") := TRB->QTDPRO
          &(cAliasPLS+"->(MsUnlock())")                   
           
          If nImpBD6 == '1'
          	nVlrTot += TRB->VLRPAG
          Endif
          
    Endif
    
    Trb->(DbCloseArea())
    
    
    If BD7->(MsSeek(cChaveGui)) .and. nImpBD6 	 == '0'
       While ! BD7->(Eof()) .And. cChaveGui == BD7->(BD7_FILIAL+BD7_CODOPE+BD7_CODLDP+BD7_CODPEG+BD7_NUMERO+BD7_ORIMOV)
             BD7->(DbSkip())
             If cChaveGui == BD7->(BD7_FILIAL+BD7_CODOPE+BD7_CODLDP+BD7_CODPEG+BD7_NUMERO+BD7_ORIMOV)
             
                BD7->(DbSkip(-1))
                nVlrTot += BD7->BD7_VLRPAG
                If Empty(BD7->BD7_CODTPA)
                
                   If BD7->BD7_VALORI == 0
                      BD7->(RecLock("BD7",.F.))
                      BD7->(DbDelete())
                      BD7->(MsUnLock())
                   Else                   
                      
                      If lCrit099
                      
                         BD7->(RecLock("BD7",.F.))
                      		PLBLOPC("BD7", .t., __aCdCri099[1], cDes099)   
                         BD7->(MsUnLock())
                             
                         BD7->(DbSkip())
                      Else
                         BD7->(RecLock("BD7",.F.))
                         BD7->(DbDelete())
                         BD7->(MsUnLock())
                      Endif
                   Endif   
                Else
                   BD7->(DbSkip())
                Endif   
             Endif   
       Enddo    	  
    Endif
Next

If nRecBRJ > 0    
	BRJ->(DbGoTo(nRecBRJ))
	BRJ->(RecLock("BRJ",.F.))
	BRJ->BRJ_VALOR := nVlrTot
	BRJ->(MsUnLock())
Endif

If ExistBlock("PLS700AF")
   ExecBlock("PLS700AF",.F.,.F.,{aResumo})
Endif
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Finaliza a transacao												     |
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
End Transaction 

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Processa / muda a fase de todas as guias importadas e ou ajustadas...    �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

BarGauge2Set(Len(aRecnosGer))     
For nFor := 1 To Len(aRecnosGer)
       IncProcG2("Atualiza寤es Finais [ "+AllTrim(Str(nFor))+" ] De [ "+AllTrim(Str(Len(aRecnosGer)))+" ] ")
	   ProcessMessage()
       cAliasPLS := aRecnosGer[nFor,1]        
       DbSelectArea(cAliasPLS)
       nRecno := aRecnosGer[nFor,2]
       DbGoto(nRecno)
       BCL->(DbSetOrder(1))
       BCL->(MsSeek(xFilial("BCL")+PLSINTPAD()+&(cAliasPLS+"->"+cAliasPLS+"_TIPGUI")))
       
       BCI->(DbSetOrder(1))
       BCI->(MsSeek(xFilial("BCI")+&(cAliasPLS+"->"+cAliasPLS+"_CODOPE")+&(cAliasPLS+"->"+cAliasPLS+"_CODLDP")+&(cAliasPLS+"->"+cAliasPLS+"_CODPEG")))
       
       nValTotPag:=0
       nTotQtdEve:=0
       BD6->(DbSetOrder(1))
       If BD6->(MsSeek(xFilial("BD6")+BCI->(BCI_CODOPE+BCI_CODLDP+BCI->BCI_CODPEG+&(cAliasPLS+"->"+cAliasPLS+"_NUMERO")+&(cAliasPLS+"->"+cAliasPLS+"_ORIMOV"))))
			While ! BD6->(Eof()) .And. BD6->(BD6_FILIAL+BD6_CODOPE+BD6_CODLDP+BD6_CODPEG+BD6_NUMERO+BD6_ORIMOV) == ;
                                       xFilial("BD6")+BCI->(BCI_CODOPE+BCI_CODLDP+BCI->BCI_CODPEG+&(cAliasPLS+"->"+cAliasPLS+"_NUMERO")+&(cAliasPLS+"->"+cAliasPLS+"_ORIMOV"))

               nValBD6Pag := 0                            
               
		       BD7->(DbSetOrder(1))
		       If BD7->(MsSeek(xFilial("BD6")+BD6->(BD6_CODOPE+BD6_CODLDP+BD6_CODPEG+BD6_NUMERO+BD6_ORIMOV+BD6_SEQUEN)))
		       		While ! BD7->(Eof()) .And. BD7->(BD7_FILIAL+BD7_CODOPE+BD7_CODLDP+BD7_CODPEG+BD7_NUMERO+BD7_ORIMOV+BD7_SEQUEN) == ;
		                						xFilial("BD6")+BD6->(BD6_CODOPE+BD6_CODLDP+BD6_CODPEG+BD6_NUMERO+BD6_ORIMOV+BD6_SEQUEN)      
		            
		            	If  nImpBD6 != '1'
			            	nValBD6Pag += BD7->BD7_VLRPAG                                
			       			nValTotPag += BD7->BD7_VLRPAG                                
		       			Endif
		       			BD7->(DbSkip())                                             
		       			
		       		Enddo
		       Endif            
		       If  nImpBD6 != '1'       			
		       		BD6->(RecLock("BD6",.F.))
       		   		BD6->BD6_VLRPAG := nValBD6Pag
       		   		BD6->(MsUnLock())                                            
	   		   Else
	   		   		nValTotPag += BD6->BD6_VLRPAG                                
	   		   Endif
	   		   nTotQtdEve += BD6->BD6_QTDPRO
	   		   BD6->(DbSkip())
	   		   
	   		Enddo
	   Endif	       

       &(cAliasPLS)->(RecLock(cAliasPLS,.F.))
       &(cAliasPLS+"->"+cAliasPLS+"_VLRPAG") := nValTotPag
       &(cAliasPLS+"->"+cAliasPLS+"_QTDEVE") := nTotQtdEve
       &(cAliasPLS)->(MsUnLock())

       nPos := Ascan(aResumo,{|x| x[3] == cAliasPLS .And. x[4] == nRecno})
       If nPos > 0
          aResumo[nPos,2] := "Faturada"
       Endif
Next

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Fim da rotina principal...                                               �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

If Valtype(oBrwBRJ) == "O"
    oBrwBRJ:Refresh(.T.)
    oBrwBRJ:ExecuteFilter()
Else
	DbSelectArea("BRJ")
	cPLSFiltro := "@BRJ_FILIAL = '"+xFilial("BRJ")+"' AND BRJ_REGPRI = '3' AND D_E_L_E_T_ = ' '"
	SET FILTER TO &(cPLSFiltro)                                                              
EndIf	
BRJ->(MsSeek(xFilial("BRJ")+BRJ_CODIGO))//nao tirar isso daqui

Return(Eval(bRet))

//+------------------------------------------------------------------
/*/{Protheus.doc} PLSA700GCF
Retorna um determinado dado a partir do layout/arq. de trab.
@author  PLSTEAM
@version P11
@since   17.01.2005
/*/  
//+------------------------------------------------------------------
Static Function PLSA700GCF(cTipReg,cDado,cSeq)
LOCAL nPos
LOCAL cRet   := ""
DEFAULT cSeq := ""

nPos := Ascan( a700Pos , {  | x | x[1] == cTipReg .And. (x[2] == cDado .or. alltrim(x[6]) == alltrim(cSeq)) } )
If nPos > 0
  cRet := Subs(Trb->Campo,a700Pos[nPos,3],(a700Pos[nPos,4]-a700Pos[nPos,3])+1)
Else
  AnalisaCrit("43","Campo "+cDado+" Tipo de Registro "+cTipReg)
Endif

Return(cRet)    

//+------------------------------------------------------------------
/*/{Protheus.doc} PL5GetCo
Retorna o intervalo de colunas 
@author  PLSTEAM
@version P11
@since   17.01.2005
/*/  
//+------------------------------------------------------------------
Static Function PL5GetCo(cTipReg,cDado)
LOCAL nPos
LOCAL cRet := ""

nPos := Ascan( a700Pos , {  | x | x[1] == cTipReg .And. x[2] == cDado } )
If nPos > 0                                                               
  cRet := "Posicao no arquivo ["+alltrim(str(a700Pos[nPos,3])) + "] -> [" + alltrim(str(a700Pos[nPos,4]))+"] "
else
  cRet := "ERRO"
Endif

Return(cRet)

//+------------------------------------------------------------------
/*/{Protheus.doc} PLSA700GPF
Retorna a posicao de um determinado campo no layout. 
@author  PLSTEAM
@version P11
@since   17.01.2005
/*/  
//+------------------------------------------------------------------
Static Function PLSA700GPF(cTipReg,cDado)
LOCAL nPos
LOCAL nPosIni := 0
LOCAL nPosFin := 0

nPos := Ascan( a700Pos , {  | x | x[1] == cTipReg .And. x[2] == cDado } )
If nPos > 0
  nPosIni := a700Pos[nPos,3]
  nPosFin := (a700Pos[nPos,4]-a700Pos[nPos,3])+1
Else
  AnalisaCrit("44","Campo "+cDado+" Tipo de Registro "+cTipReg)
Endif

Return({nPosIni,nPosFin})

//+------------------------------------------------------------------
/*/{Protheus.doc} MonLayout
Monta matriz de apoio com todos os layouts do respectivo EDI 
@author  PLSTEAM
@version P11
@since   17.01.2005
/*/  
//+------------------------------------------------------------------
Static Function MonLayout(cLayPLS)

DE1->(DbSetOrder(1))
If DE1->(MsSeek(xFilial("DE1")+cLayPLS))
   While ! DE1->(Eof()) .And. DE1->DE1_FILIAL == xFilial("DE1") .And. DE1->DE1_CODLAY == cLayPLS
         aadd(a700Pos,{AllTrim(DE1->DE1_CODREG),AllTrim(DE1->DE1_CAMPO),Val(DE1->DE1_POSINI),Val(DE1->DE1_POSFIM), Val(DE1->DE1_POSFIM)-Val(DE1->DE1_POSINI)+1,DE1->DE1_SEQUEN})   
   DE1->(DbSkip())
   Enddo
Endif   

Return

//+------------------------------------------------------------------
/*/{Protheus.doc} AnalisaPEG
Cria a PEG caso necessario
@author  PLSTEAM
@version P11
@since   17.01.2005
/*/  
//+------------------------------------------------------------------
static Function AnalisaPEG(cUniDes,cCodRDA,cAMPAG,cTipGui,cCodLDP,cArquivo,lNaoCriaPeg)
LOCAL lCrieiPEG 	:= .F.    
LOCAL nHBRJ			:= 0
LOCAL cChaveBCI		:= ""
DEFAULT lNaoCriaPeg := .F.        

BCI->(DbSetOrder(4)) //BCI_FILIAL + BCI_OPERDA + BCI_CODRDA+ BCI_ANO + BCI_MES + BCI_TIPO + BCI_FASE + BCI_SITUAC + BCI_TIPGUI + BCI_CODLDP + BCI_ARQUIV
cChaveBCI := cUniDes+cCodRDA+cAMPAG+"241"+cTipGui+cCodLDP+cArquivo

if !BCI->(DbSeek(xFilial("BCI")+cChaveBCI)) .and. !lNaoCriaPeg
	lCrieiPEG := .T.   
	PLSIPP(cUniDes,cCodLDP,cUniDes,cCodRDA,Subs(cAMPAG,5,2),Subs(cAMPAG,1,4),dDataBase,cTipGui,"",{},"4",cArquivo)
endIf

If !lCriadoBRJ
	nHBRJ := PLSAbreSem("PLSBRJ.SMF")	
	   BRJ->(RecLock("BRJ",.T.))
		   BRJ->BRJ_FILIAL := xFilial("BRJ")
		   BRJ->BRJ_CODIGO := GetSx8Num("BRJ","BRJ_CODIGO")
		   BRJ->BRJ_DATA   := dDataBase
		   BRJ->BRJ_REGPRI := "3"
		   BRJ->BRJ_CODOPE := BCI->BCI_CODOPE
		   BRJ->BRJ_CODLDP := BCI->BCI_CODLDP
		   BRJ->BRJ_CODPEG := BCI->BCI_CODPEG
		   BRJ->BRJ_ARQUIV := cArquivo
		   BRJ->BRJ_OPEORI := cUniOri
		   BRJ->BRJ_NOMORI := BA0->(Posicione("BA0",1,xFilial("BA0")+cUniOri,"BA0_NOMINT"))
		   BRJ->BRJ_ANO    := Subs(cAMPAG,1,4)
		   BRJ->BRJ_TPPAG  := CriaVar("BRJ_TPPAG")
		   BRJ->BRJ_ANOPAG := Subs(cAMPAG,1,4)
		   BRJ->BRJ_MESPAG := Subs(cAMPAG,5,2)
		   BRJ->BRJ_MES := Subs(cAMPAG,5,2)
		   BRJ->BRJ_NUMFAT := _NR_FATURA
		   BRJ->BRJ_DTEMIS := _DT_EMI_FAT
		   BRJ->BRJ_DTVENC := _DT_VEM_FAT
		   BRJ->BRJ_STATUS := "2"
		   BRJ->BRJ_CAMCOM := PLSRTCCOMP(BRJ->BRJ_OPEORI)
	   BRJ->(MsUnLock())                               
	   BRJ->(ConfirmSX8())
	   nRecBRJ    := BRJ->(Recno())              
	   cCodSeq    := BRJ->BRJ_CODIGO
	   lCriadoBRJ := .T.
	PLSFechaSem(nHBRJ,"PLSBRJ.SMF")
else
   if lCrieiPEG
      nHBRJ := PLSAbreSem("PLSBRJ.SMF")	
	      BRJ->(RecLock("BRJ",.T.))
		      BRJ->BRJ_FILIAL := xFilial("BRJ")
		      BRJ->BRJ_CODIGO := cCodSeq
		      BRJ->BRJ_DATA   := dDataBase
		      BRJ->BRJ_REGPRI := "4"
		      BRJ->BRJ_CODOPE := BCI->BCI_CODOPE
		      BRJ->BRJ_CODLDP := BCI->BCI_CODLDP
		      BRJ->BRJ_CODPEG := BCI->BCI_CODPEG
	      BRJ->(MsUnLock())                               
      PLSFechaSem(nHBRJ,"PLSBRJ.SMF")
   endIf   
endIf    

return

//+------------------------------------------------------------------
/*/{Protheus.doc} CriaGuiaSV
Cria Guia de servico
@author  PLSTEAM
@version P11
@since   17.01.2005
/*/  
//+------------------------------------------------------------------
Static Function CriaGuiaSV(cAMPAG)
LOCAL nH       := PLSAbreSem("PNUMAUTGRV.SMF")            
LOCAL cNumero  := PLSA500NUM("BD5",BCI->BCI_CODOPE,BCI->BCI_CODLDP,BCI->BCI_CODPEG)
LOCAL cCC      := PLSUSRCC(BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG+BA1_DIGITO))
LOCAL cAuxDat  := PLSA700GCF("R702","DT_ATEND")
LOCAL dDataPro := ctod(subs(cAuxDat,9,2)+"/"+subs(cAuxDat,6,2)+"/"+subs(cAuxDat,1,4))
LOCAL cHorPro  := subs(cAuxDat,11,2)+subs(cAuxDat,14,2)
LOCAL aRet     := {"",""}

BD5->(RecLock("BD5",.T.))
BD5->BD5_FILIAL := xFilial('BD5')
BD5->BD5_CODOPE := BCI->BCI_CODOPE
BD5->BD5_TIPPAC := PLSA700GCF("R702","TP_PACIENT")
BD5->BD5_CODLDP := BCI->BCI_CODLDP
BD5->BD5_CODPEG := BCI->BCI_CODPEG
BD5->BD5_NUMERO := cNumero
BD5->BD5_DATPRO := dDataPro
BD5->BD5_HORPRO := cHorPro
IF     nRegraGrvImp == 1
       BD5->BD5_NUMIMP := Subs(PLSA700GCF("R702","NR_LOTE"),4,5)+PLSA700GCF("R702","NR_NOTA")
ElseIf nRegraGrvImp == 2
       BD5->BD5_NUMIMP := PLSA700GCF("R702","NR_NOTA")
ElseIf nRegraGrvImp == 3
       BD5->BD5_NUMIMP := cUniOri+PLSA700GCF("R702","NR_NOTA")
Endif   

BD5->BD5_OPEUSR := BA1->BA1_CODINT
If ! lUsrGen
   BD5->BD5_MATANT := BA1->BA1_MATANT
   BD5->BD5_NOMUSR := BA1->BA1_NOMUSR
Else
   BD5->BD5_MATANT := PLSA700GCF("R702","CD_UNI")+PLSA700GCF("R702","ID_BENEF")
   BD5->BD5_NOMUSR := PLSA700GCF("R702","NM_BENEF")
Endif   
BD5->BD5_CODRDA := BAU->BAU_CODIGO
BD5->BD5_OPERDA := cUniDes
BD5->BD5_TIPRDA := BAU->BAU_TIPPE
BD5->BD5_NOMRDA := BAU->BAU_NOME
BD5->BD5_DESLOC := aDadRda[19]
BD5->BD5_ENDLOC := aDadRda[20]
BD5->BD5_CODESP := aDadRda[15]
BD5->BD5_CID    := PLSA700GCF("R702","CD_CID")
If Empty(BD5->BD5_CID)
	BD5->BD5_CID:= PlRetCidGe()
Endif
BD5->BD5_OPESOL := cUniDes
BD5->BD5_TIPCON := ALLTRIM(STR(VAL(PLSA700GCF("R702","TP_CONSULT","012"))+1))
BD5->BD5_TIPGUI := BCI->BCI_TIPGUI             
If nNVerTra < 12
	BD5->BD5_ATEAMB := PLSA700GCF("R702","ID_AMBULA")
EndIf
BD5->BD5_CODEMP := BA1->BA1_CODEMP
BD5->BD5_MATRIC := BA1->BA1_MATRIC
BD5->BD5_TIPREG := BA1->BA1_TIPREG
BD5->BD5_CPFUSR := BA1->BA1_CPFUSR
BD5->BD5_IDUSR  := BA1->BA1_DRGUSR
BD5->BD5_DATNAS := BA1->BA1_DATNAS
BD5->BD5_CPFRDA := BAU->BAU_CPFCGC
BD5->BD5_FASE   := BCI->BCI_FASE
BD5->BD5_SITUAC := BCI->BCI_SITUAC
BD5->BD5_DIGITO := BA1->BA1_DIGITO
BD5->BD5_CONEMP := BA1->BA1_CONEMP
BD5->BD5_VERCON := BA1->BA1_VERCON
BD5->BD5_SUBCON := BA1->BA1_SUBCON
BD5->BD5_VERSUB := BA1->BA1_VERSUB
BD5->BD5_LOCAL  := aDadRda[13]
BD5->BD5_CODLOC := aDadRda[18]
BD5->BD5_MATVID := BA1->BA1_MATVID
BD5->BD5_DTDIGI := Date()
BD5->BD5_MATUSA := "1"
BD5->BD5_ATERNA := "0"
BD5->BD5_PACOTE := "0"
BD5->BD5_ORIMOV := "1"
BD5->BD5_MESPAG := Subs(cAMPAG,5,2)
BD5->BD5_ANOPAG := Subs(cAMPAG,1,4)
BD5->BD5_GUIACO := "0"
BD5->BD5_LIBERA := "0"
BD5->BD5_CC     := cCC
BD5->BD5_TIPPRE := BAU->BAU_TIPPRE
BD5->BD5_RGIMP  := "1"
BD5->BD5_TPGRV  := "4"
BD5->BD5_SEQIMP := cCodSeq                   
If cCodLayPLS >= '3.2' .and. nNVerTra >= 10
	BD5->BD5_TIPATE := PLSA700GCF("R702","TP_ATENDIM","019")
	BD5->BD5_TIPSAI := PLSA700GCF("R702","TP_SAIDCON","018")
	BD5->BD5_GUIPRI := PLSA700GCF("R702","NR_GUIA_PR","020")
Endif
BD5->(MsUnLock())

aRet[1] := "BD5"
aRet[2] := BD5->(BD5_CODOPE+BD5_CODLDP+BD5_CODPEG+BD5_NUMERO)
PLSFechaSem(nH,"PNUMAUTGRV.SMF")              

Return aRet

//+------------------------------------------------------------------
/*/{Protheus.doc} CriaGuiaHS
Cria Guia GIH
@author  PLSTEAM
@version P11
@since   20.04.2005
/*/  
//+------------------------------------------------------------------
Static Function CriaGuiaHS(cAMPAG)
LOCAL nH       := PLSAbreSem("PNUMAUTGRV.SMF")            
LOCAL cNumero  := PLSA500NUM("BE4",BCI->BCI_CODOPE,BCI->BCI_CODLDP,BCI->BCI_CODPEG)
LOCAL cCC      := PLSUSRCC(BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG+BA1_DIGITO))
LOCAL cAuxDat  := PLSA700GCF("R702","DT_ATEND")
LOCAL dDataPro := ctod(subs(cAuxDat,9,2)+"/"+subs(cAuxDat,6,2)+"/"+subs(cAuxDat,1,4))
LOCAL cHorPro  := subs(cAuxDat,11,2)+subs(cAuxDat,14,2)
LOCAL cMesInt  := subs(dtos(dDataPro),5,2)
LOCAL cAnoInt  := subs(dtos(dDataPro),1,4)
LOCAL cNumInt  
LOCAL nRecno   := 0
LOCAL aRet     := {"",""} 


cNumInt  := PLNUMINT(PLSINTPAD(),cAnoInt,cMesInt)

BE4->(RecLock("BE4",.T.))
BE4->BE4_FILIAL := xFilial('BE4')
BE4->BE4_CODOPE := BCI->BCI_CODOPE
BE4->BE4_CODLDP := BCI->BCI_CODLDP
BE4->BE4_CODPEG := BCI->BCI_CODPEG
BE4->BE4_NUMERO := cNumero
IF     nRegraGrvImp == 1
       BE4->BE4_NUMIMP := Subs(PLSA700GCF("R702","NR_LOTE"),4,5)+PLSA700GCF("R702","NR_NOTA")
ElseIf nRegraGrvImp == 2
       BE4->BE4_NUMIMP := PLSA700GCF("R702","NR_NOTA")
ElseIf nRegraGrvImp == 3
       BE4->BE4_NUMIMP := cUniOri+PLSA700GCF("R702","NR_NOTA")
Endif   
BE4->BE4_GUIIMP := "1"
BE4->BE4_DATPRO := dDataPro
BE4->BE4_PRVINT := dDataPro
BE4->BE4_DATVAL := BE4->BE4_DATPRO+GetNewPar("MV_PLPRZAI",30)
BE4->BE4_TIPUSR := BA3->BA3_TIPOUS
BE4->BE4_HORPRO := cHorPro
BE4->BE4_OPEUSR := BA1->BA1_CODINT

If ! lUsrGen
   BE4->BE4_MATANT := BA1->BA1_MATANT
   BE4->BE4_NOMUSR := BA1->BA1_NOMUSR
Else
   BE4->BE4_MATANT := PLSA700GCF("R702","CD_UNI")+PLSA700GCF("R702","ID_BENEF")
   BE4->BE4_NOMUSR := PLSA700GCF("R702","NM_BENEF")
Endif

BE4->BE4_CODRDA := BAU->BAU_CODIGO
BE4->BE4_OPERDA := cUniDes
BE4->BE4_NOMRDA := BAU->BAU_NOME
BE4->BE4_CODESP := aDadRda[15]
BE4->BE4_CID    := PLSA700GCF("R702","CD_CID")
If Empty(BE4->BE4_CID)
	BE4->BE4_CID:= PlRetCidGe()
Endif
BE4->BE4_DESCID := BA9->(Posicione("BA9",1,xFilial("BA9")+BD5->BD5_CID,"BA9_DOENCA"))
BE4->BE4_OPESOL := cUniDes
BE4->BE4_TIPGUI := BCI->BCI_TIPGUI
BE4->BE4_CODEMP := BA1->BA1_CODEMP
BE4->BE4_MATRIC := BA1->BA1_MATRIC
BE4->BE4_TIPREG := BA1->BA1_TIPREG
BE4->BE4_CPFUSR := BA1->BA1_CPFUSR
BE4->BE4_IDUSR  := BA1->BA1_DRGUSR
BE4->BE4_DATNAS := BA1->BA1_DATNAS
BE4->BE4_FASE   := BCI->BCI_FASE
BE4->BE4_SITUAC := BCI->BCI_SITUAC
BE4->BE4_DIGITO := BA1->BA1_DIGITO
BE4->BE4_CONEMP := BA1->BA1_CONEMP
BE4->BE4_VERCON := BA1->BA1_VERCON
BE4->BE4_SUBCON := BA1->BA1_SUBCON
BE4->BE4_VERSUB := BA1->BA1_VERSUB
BE4->BE4_LOCAL  := aDadRda[13]
BE4->BE4_CODLOC := aDadRda[12]
BE4->BE4_MATVID := BA1->BA1_MATVID
BE4->BE4_DTDIGI := Date()
BE4->BE4_ANOINT := cAnoInt
BE4->BE4_MESINT := cMesInt
BE4->BE4_NUMINT := cNumInt
BE4->BE4_MATUSA := "1"
BE4->BE4_PACOTE := "0"
BE4->BE4_ORIMOV := "2"
BE4->BE4_MESPAG := Subs(cAMPAG,5,2)
BE4->BE4_ANOPAG := Subs(cAMPAG,1,4)
BE4->BE4_CC     := cCC
BE4->BE4_TIPPRE := BAU->BAU_TIPPRE
BE4->BE4_RGIMP  := "1"
BE4->BE4_SEQIMP := cCodSeq

If Len(aDadUsr) > 0 .and. aDadUsr[1] 
	BE4->BE4_PADCON := PLSACOMUSR(aDadUsr[2],'2') 
Endif
                   
If cCodLayPLS >= '3.2' .and. nNVerTra >= 10
	
	nRecno := Trb->(Recno())
	Trb->(DbSkip())
	If Subs(Trb->Campo,_nPosITREG,_nPosFTREG) == "703"   
		BE4->BE4_NASVIV := val(PLSA700GCF("R703","QT_NAVIVOS","017"))
		BE4->BE4_NASMOR := val(PLSA700GCF("R703","QT_NAMORTO","018"))
		BE4->BE4_NASVPR := val(PLSA700GCF("R703","QT_NAVIVOP","019"))
		BE4->BE4_OBTPRE := val(PLSA700GCF("R703","QT_OBITOPR","020"))
		BE4->BE4_OBTTAR := val(PLSA700GCF("R703","QT_OBITOTR","021"))
		BE4->BE4_TIPFAT := PLSA700GCF("R703","TP_FATURA","031")
		BE4->BE4_CIDOBT := PLSA700GCF("R703","CD_CIDOBT","032")
		BE4->BE4_NRDCOB := PLSA700GCF("R703","NR_DECOBT","033")
		BE4->BE4_OBTMUL	:= PLSA700GCF("R703","TP_OBTMULH","039")
	Endif
	Trb->(DbGoTo(nRecno))
Endif
					
BE4->(MsUnLock())

aRet[1] := "BE4"
aRet[2] := BE4->(BE4_CODOPE+BE4_CODLDP+BE4_CODPEG+BE4_NUMERO) 

PLSFechaSem(nH,"PNUMAUTGRV.SMF")               

Return aRet

//+------------------------------------------------------------------
/*/{Protheus.doc} CriaEvento
Cria Evento a partir do BD5 ou BE4
@author  PLSTEAM
@version P11
@since   24.01.2005
/*/  
//+------------------------------------------------------------------
Static Function CriaEvento(nNextRec,aPartPro,nJ,lForNBD6)
LOCAL aCpoNiv 
LOCAL nFor
LOCAL aCposPad
LOCAL nForCpo
LOCAL cMacro
LOCAL cCampoOri
LOCAL aCodTab
LOCAL nAux
LOCAL cNewSeq
LOCAL nI
LOCAL cChavePes  := &(cAliasPLS+"->"+cAliasPLS+"_CODOPE")+&(cAliasPLS+"->"+cAliasPLS+"_CODLDP")+;
                    &(cAliasPLS+"->"+cAliasPLS+"_CODPEG")+&(cAliasPLS+"->"+cAliasPLS+"_NUMERO")+&(cAliasPLS+"->"+cAliasPLS+"_ORIMOV")
LOCAL cAuxDat  := PLSA700GCF("R704","DT_SERVICO")
LOCAL dDataPro := ctod(subs(cAuxDat,7,2)+"/"+subs(cAuxDat,5,2)+"/"+subs(cAuxDat,1,4))
LOCAL cOriMov  := &(cAliasPLS+"->"+cAliasPLS+"_ORIMOV")
LOCAL cCodSer  := AllTrim(PLSA700GCF("R704","CD_SERVICO"))
LOCAL lAjusBR8 := .F.
LOCAL lCriaBD6 := .T.
LOCAL cTpPart  := PLSA700GCF("R704","TP_PARTICI")
LOCAL lUptHM   := .F.
LOCAL aBD7     := {}
LOCAL cMatAnt  := BA1->BA1_MATANT
LOCAL lAltQtd  := .F.
LOCAL lExclui
LOCAL nControl                       	
LOCAL nTotAtM  := 0
LOCAL nTtTAtM  := 0
LOCAL nTotPro  := 0
LOCAL nToTaAp  := 0
LOCAL nQtdAtM  := 0
LOCAL lFlagDel := .T.
LOCAL nAprCheio := 0
LOCAL nTaxCheio := 0
LOCAL aCtrlBD7  := {.F.,.F.,.F.}
LOCAL lAprUsado := .F.
LOCAL cTipApr   := ""
LOCAL nRecno	:= 0
LOCAL nQtdPro	:= 0
LOCAL cProxPart := ""
LOCAL lPreAut   := .F.  
LOCAL lOldAlt   := .F.
local aMatTOTBD7:= {}

DEFAULT nNextRec := 0     
DEFAULT aPartPro := {}
DEFAULT nJ		 := 0
DEFAULT lForNBD6 := .F.

nRecno := Trb->(Recno())                            
If nNextRec > 0
	Trb->(DbGoTo(nNextRec))                            
Else
	Trb->(DbSkip())
Endif
cProxPart := alltrim(PLSA700GCF("R704","TP_PARTICI"))
Trb->(DbGoTo(nRecno))

If Empty(dDataPro)
   dDataPro := &(cAliasPLS+"->"+cAliasPLS+"_DATPRO")
Endif   

lAjusBR8    := PLBusProTab(cCodSer,.T.,PLSA700GCF("R704","DT_SERVICO"),nil,lConverProc,cUniOri,cCodProGen,cCodPadCon,cCodPaPro)


If cTpPart == "0"
   lCriaBD6 := .T.
Else
   If cUltCodigo == BR8->BR8_CODPSA 
      
      If cTpPart == cUltPart .And. cRdaUlt ==  PLSA700GCF("R704","CD_PREST")  .And. cDataUlt == PLSA700GCF("R704","DT_SERVICO")
         //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	  	 //� Nao criar outro evento									   				                 �
         //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
         lCriaBD6 := .F.
         lAltQtd  := .T.
      Else 
      	 //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	  	 //� Mesmo codigo participacao diferente e um bd7 diferente								 	 �
         //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
         lCriaBD6 := .T.
      Endif
      //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	  //| Aqui eu verifico se a linha de baixo do registro 704 esta complementando a linha de cima|
	  //| antes de se efetuar esta altera豫o foi contactada a UNIMED BRASIL a fim de esclarecer   |
	  //| se esta situacao poderia ocorrer														  | 
      //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      If alltrim(cTpPart) == 'X' .and. ( alltrim(cUltPart) == '0' .or. alltrim(cProxPart) == 'X') .and. ;
      	 cRdaUlt <> PLSA700GCF("R704","CD_PREST") .and. cDataUlt == PLSA700GCF("R704","DT_SERVICO")
      	 	lCriaBD6 := .F. 
         	lAltQtd  := .F.
      Endif
      //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	  //| Aqui indica que eh uma cirurgia, ou seja, antes o sistema criava 3 BD6, o que estava    |
	  //| errado, visto que poderia dar problema no calculo da co-participacao, e no tratamento   |
	  //| da quantidade X percentual.															  |
      //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      If aScan(aPartPro,{|x| upper(alltrim(x[1])) $ "C"}) > 0 .and. ; // tem cirurgiao
         (aScan(aPartPro,{|x| upper(alltrim(x[1])) $ "A"}) > 0 .or. ; // em toda cirurgia tem anestesista
      	 aScan(aPartPro,{|x| upper(alltrim(x[1])) $ "1"}) > 0) .and.; // ou tem auxiliar ou tem os dois
      	 !(upper(alltrim(cTpPart)) $ "0,X,H,P,L") .and. ; // cirugia nao tem isso
      	 nJ <> 1 //jah criou o primeiro procedimento da nota
      	      	 
      		lCriaBD6 := .F. 
         	lAltQtd  := .F.
         	
      Endif  
   Else
      //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	  //� Outro codigo portando eh outro procedimento   		 							  	  �
      //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      lCriaBD6 := .T. 
   Endif   
Endif   

If lForNBD6        
	lCriaBD6 := .F.
	If nJ == 1 .or. cTpPart == "0"  .or. cUltCodigo <> BR8->BR8_CODPSA 
		lOldAlt  := .T.
	Else
		lOldAlt  := lAltQtd
	Endif
	lAltQtd  := .T.                  
	lPreAut  := .T.
Endif
  
If lCriaBD6
   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
   //� Atualiza dados do cabecalho que somente existe no layout no item         �
   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
    BB0->(DbSetOrder(5))
    If ! BB0->(MsSeek(xFilial("BB0")+PLSA700GCF("R704","CD_UNI_PRE")))
      BA0->(DbSetOrder(1))
      If BA0->(MsSeek(xFilial("BA0")+PLSA700GCF("R704","CD_UNI_PRE")))
         BB0->(RecLock("BB0",.T.))
         BB0->BB0_FILIAL := xFilial("BB0")
         BB0->BB0_VINC   := "1"
         BB0->BB0_CODIGO := BB0->(GetSx8Num("BB0","BB0_CODIGO"))
         BB0->(ConfirmSx8())
         BB0->BB0_NOME   := "SOLIC. PAD. OPER. "+PLSA700GCF("R704","CD_UNI_PRE")
         BB0->BB0_CODSIG := GETMV("MV_PLSIGLA")
         BB0->BB0_NUMCR  := "OPE"+PLSA700GCF("R704","CD_UNI_PRE")
         BB0->BB0_ESTADO := BA0->BA0_EST
         BB0->BB0_CGC    := ""
         BB0->BB0_UF     := BA0->BA0_EST
         BB0->BB0_CODOPE := PLSA700GCF("R704","CD_UNI_PRE")
         BB0->BB0_CODORI := PLSA700GCF("R704","CD_UNI_PRE")
         BB0->(MsUnLock())
      Endif   
    Endif   
         
	&(cAliasPLS)->(RecLock(cAliasPLS,.F.))
   	&(cAliasPLS+"->"+cAliasPLS+"_ESTSOL") := BB0->BB0_ESTADO
   	&(cAliasPLS+"->"+cAliasPLS+"_SIGLA")  := BB0->BB0_CODSIG
   	&(cAliasPLS+"->"+cAliasPLS+"_REGSOL") := BB0->BB0_NUMCR
   	&(cAliasPLS+"->"+cAliasPLS+"_NOMSOL") := BB0->BB0_NOME
   	&(cAliasPLS+"->"+cAliasPLS+"_CDPFSO")  := BB0->BB0_CODIGO
   	&(cAliasPLS)->(MsUnLock())
   
   	BD6->(DbSetOrder(1))
   	BD6->(DbSeek(xFilial("BD6")+cChavePes+Replicate("Z",Len(BD6->BD6_SEQUEN)),.T.))
   	BD6->(DbSkip(-1))
      
   	If BD6->(BD6_FILIAL+BD6_CODOPE+BD6_CODLDP+BD6_CODPEG+BD6_NUMERO+BD6_ORIMOV) == xFilial("BD6")+cChavePes
   	   cNewSeq := Soma1(BD6->BD6_SEQUEN)
   	Else
   	   cNewSeq := StrZero(1,Len(BD6->BD6_SEQUEN))
   	Endif
   
	
    aCpoNiv := PLSUpCpoNv(BR8->BR8_CODPAD,BR8->BR8_CODPSA,"BD6")
    nAux := val(PLSA700GCF("R704","QT_PAGA"))
 
    BD6->(RecLock("BD6",.T.))                                             
   
    For nFor := 1 To Len(aCpoNiv)
        &(aCpoNiv[nFor,1]) := (aCpoNiv[nFor,2])
    Next 
       
    BD6->BD6_FILIAL := xFilial("BD6")
    BD6->BD6_CODPLA := BA3->BA3_CODPLA
    BD6->BD6_TPGRV  := "4"
    BD6->BD6_SEQUEN := cNewSeq
    BD6->BD6_CODPAD := BR8->BR8_CODPAD
    BD6->BD6_CODPRO := BR8->BR8_CODPSA
   
    BD6->BD6_SOLORI := PLSA700GCF("R704","CD_UNI_PEQ","019")+PLSA700GCF("R704","CD_PRE_REQ")
   
    cUltCodigo := BR8->BR8_CODPSA
    cUltPart   := cTpPart
    cRdaUlt    := PLSA700GCF("R704","CD_PREST")
    cDataUlt   := PLSA700GCF("R704","DT_SERVICO")
   
   
    BD6->BD6_RDAEDI := PLSA700GCF("R704","CD_PREST")
    BD6->BD6_NOMEDI := PLSA700GCF("R704","NM_PREST")
    BD6->BD6_TRDAED := PLSA700GCF("R704","TP_PREST")
    BD6->BD6_RPEDI  := PLSA700GCF("R704","ID_REC_PRO")
    BD6->BD6_TPRDAE := PLSA700GCF("R704","TP_PESSOA")
    BD6->BD6_CNPJED := PLSA700GCF("R704","NR_CNJP_CP")
    BD6->BD6_ESPEDI := PLSA700GCF("R704","CD_ESPEC")
   
    BD6->BD6_DESPRO := BR8->BR8_DESCRI
    BD6->BD6_QTDPRO := nAux
    
    If nImpBD6 == '1'
	 	  
		BD6->BD6_VALORI := (Val(Subs(PLSA700GCF("R704","VL_SERV_CO"),1,12)+"."+Subs(PLSA700GCF("R704","VL_SERV_CO"),13,2))) 
    	BD6->BD6_VALORI += (Val(Subs(PLSA700GCF("R704","VL_CO_COB"),1,12)+"."+Subs(PLSA700GCF("R704","VL_CO_COB"),13,2)))
		BD6->BD6_VALORI += (Val(Subs(PLSA700GCF("R704","VL_FILME_C"),1,12)+"."+Subs(PLSA700GCF("R704","VL_FILME_C"),13,2)))
		BD6->BD6_VLRAPR := BD6->BD6_VALORI / BD6->BD6_QTDPRO
		
    Endif
    
    BD6->BD6_ORIMOV := cOriMov
    BD6->BD6_NIVEL  := BR8->BR8_NIVEL         
    BD6->BD6_STATUS := "1"
    
    BD6->BD6_BLOCPA := "1"
    
    BD6->BD6_MODCOB := aDadUsr[48]
    BD6->BD6_TIPUSR := iIf( len(aDadUsr) >= 90, aDadUsr[90], "")
	BD6->BD6_INTERC := iIf( len(aDadUsr) >= 91, aDadUsr[91], "")
	BD6->BD6_TIPINT := aDadUsr[43]
     
    BD6->BD6_OPEORI := BA1->BA1_OPEORI
    BD6->BD6_CHVNIV := ""
    BD6->BD6_NIVAUT := ""
    BD6->BD6_PROCCI := If(BR8->BR8_TIPEVE$"2,3","1","0")
    BD6->BD6_SEQIMP := cCodSeq                                                                                  
    BD6->BD6_TIPRDA := PLSA700GCF("R704","TP_PESSOA")
    BD6->BD6_CODATO := PLSA700GCF("R704","CD_ATO")
    BD6->BD6_CODLOC := aDadRda[12]
    BD6->BD6_LOCAL  := aDadRda[13]
    BD6->BD6_MATCOB := cMatrCob
    BD6->BD6_NOMCOB := cNomCob

    aCposPad := Eval( { || DbSelectArea(cAliasPLS), DbStruct() })
                   
    For nForCpo := 1 To Len(aCposPad)
       cMacro := ("BD6->BD6_"+Subs(aCposPad[nForCpo,1],5,10))
       If Type(cMacro) <> "U" .and. !(alltrim("BD6_"+Subs(aCposPad[nForCpo,1],5,10)) $ "BD6_CNPJED,BD6_VLRAPR,BD6_VALORI,BD6_VLRPAG,BD6_TPRDAE,BD6_QTDAPR,BD6_TIPUSR,BD6_RDAEDI,BD6_NOMEDI,BD6_TRDAED,BD6_RPEDI,BD6_ESPEDI")
      
          cCampoOri := cAliasPLS+"->"+cAliasPLS+"_"+Subs(aCposPad[nForCpo,1],5,10)
          &(cMacro) := &(cCampoOri)
       Endif   
    Next     

    aCodTab := PLSRETTAB(BD6->BD6_CODPAD,BD6->BD6_CODPRO,BD6->BD6_DATPRO,;
                        BD6->BD6_CODOPE,BD6->BD6_CODRDA,BD6->BD6_CODESP,BD6->BD6_SUBESP,BD6->(BD6_CODLOC+BD6_LOCAL),;
                        BD6->BD6_DATPRO,"1",BD6->BD6_OPEORI,BD6->BD6_CODPLA,"2","1")

    If aCodTab[1]
      BD6->BD6_CODTAB := aCodTab[3]
      BD6->BD6_ALIATB := aCodTab[4]
    Endif
   
    BD6->BD6_DATPRO := dDataPro  
   
    BGR->(DbSetOrder(2))
    If BGR->(MsSeek(xFilial("BGR")+PLSINTPAD()+StrZero(Val(PLSA700GCF("R704","CD_VIA_ACE")),1)))
   		BD6->BD6_VIA     := BGR->BGR_CODVIA 
   		BD6->BD6_PERVIA  := BGR->BGR_PERC
    Else                         
   		BGR->(DbSetOrder(1))
        If BGR->(MsSeek(xFilial("BGR")+PLSINTPAD()+StrZero(Val(PLSA700GCF("R704","CD_VIA_ACE")),1)))
   			BD6->BD6_VIA    := BGR->BGR_CODVIA                                 
   			BD6->BD6_PERVIA := BGR->BGR_PERC
   		Endif
    Endif
   
    If BD6->BD6_VIA >= "1"
         BD6->BD6_PROCCI := "1" //Somente pelo fato de vir como via de acesso
    Endif   
   
    If lAjusBR8
       BD6->BD6_CODERR := cCodSer
    Endif              
   
    IF     nRegraGrvImp == 1
          BD6->BD6_NUMIMP := Subs(PLSA700GCF("R704","NR_LOTE"),4,5)+PLSA700GCF("R704","NR_NOTA")
    ElseIf nRegraGrvImp == 2
          BD6->BD6_NUMIMP := PLSA700GCF("R704","NR_NOTA")
    ElseIf nRegraGrvImp == 3
          BD6->BD6_NUMIMP := cUniOri+PLSA700GCF("R704","NR_NOTA")
    Endif         
   
    BD6->(MsUnLock())     
   
    If cMatAnt == cMatrAntGen //E usuario generico, vou glosar manualmente p ir para conferencia...
      
      If PLSPOSGLO(PLSINTPAD(),__aCdCri091[1],__aCdCri091[2],cLocalExec)  .And. PLSCHKCRI( {'BAU',BD6->BD6_CODRDA,__aCdCri091[1]} )
         
         BD6->(RecLock("BD6",.F.))
      		
      		PLBLOPC("BD6", .t., __aCdCri091[1], PLSBCTDESC(), .t., .f., .t.)
      		   
         BD6->(MsUnLock())

         BD7->(DbSetOrder(1))
         If BD7->(MsSeek(xFilial("BD7")+BD6->(BD6_CODOPE+BD6_CODLDP+BD6_CODPEG+BD6_NUMERO+BD6_ORIMOV+BD6_SEQUEN)))
            While ! BD7->(Eof()) .And. BD7->(BD7_FILIAL+BD7_CODOPE+BD7_CODLDP+BD7_CODPEG+BD7_NUMERO+BD7_ORIMOV+BD7_SEQUEN) == ;
                                        xFilial("BD7")+BD6->(BD6_CODOPE+BD6_CODLDP+BD6_CODPEG+BD6_NUMERO+BD6_ORIMOV+BD6_SEQUEN)
                                      
                  BD7->(RecLock("BD7",.F.))
                  
                  	PLBLOPC("BD7", .t., __aCdCri091[1], PLSBCTDESC())
                  
                  BD7->(MsUnLock())            

            BD7->(DbSkip())
            Enddo
         Endif                                
      Endif   
    Endif
    
Else                            
   
   cUltCodigo 	:= BR8->BR8_CODPSA
   nAux 		:= val(PLSA700GCF("R704","QT_PAGA"))
   
   If lAltQtd
	  
   	  nTotAtM := BD6->BD6_VALORI
   	  
      if ! lPreAut 
      
         nQtdAtM := BD6->BD6_QTDPRO
         
         BD6->(RecLock("BD6",.F.))
	     BD6->BD6_QTDPRO := nQtdAtM + nAux
	     BD6->(MsUnLock())
	     
      else
      
      	If lOldAlt
      	
		      BD6->(RecLock("BD6",.F.))
		      	BD6->BD6_QTDPRO += nAux
		      BD6->(MsUnLock())
		      
        Endif
        
      endif
      
   Else
      	nTotAtM := BD6->BD6_VALORI
   Endif
 
   If nImpBD6 == '1'
		                                                                                                                  
	   nTotPro := Val(Subs(PLSA700GCF("R704","VL_SERV_CO"),1,12)+"."+Subs(PLSA700GCF("R704","VL_SERV_CO"),13,2))
   	   nTotPro += Val(Subs(PLSA700GCF("R704","VL_CO_COB"),1,12)+"."+Subs(PLSA700GCF("R704","VL_CO_COB"),13,2))
       nTotPro += Val(Subs(PLSA700GCF("R704","VL_FILME_C"),1,12)+"."+Subs(PLSA700GCF("R704","VL_FILME_C"),13,2))
 		 		 
	   //o que foi apresentado + o que ja existia
	   BD6->(RecLock("BD6",.F.))
			BD6->BD6_VALORI := (nTotAtM + nTotPro)
			BD6->BD6_VLRAPR := ( BD6->BD6_VALORI / BD6->BD6_QTDPRO )
	   BD6->(MsUnLock())
	   	    
   Endif
   
Endif


If lCriaBD6
   PLS720IBD7(&(cAliasPLS+"->"+cAliasPLS+"_PACOTE"),BD6->BD6_VLPGMA,BD6->BD6_CODPAD,BD6->BD6_CODPRO,BD6->BD6_CODTAB,BD6->BD6_CODOPE,BD6->BD6_CODRDA,;
              BD6->BD6_REGEXE,BD6->BD6_SIGEXE,BD6->BD6_ESTEXE,BD6->BD6_CDPFRE,BD6->BD6_CODESP,BD6->(BD6_CODLOC+BD6_LOCAL),"1",BD6->BD6_SEQUEN,;
              cOriMov,BCL->BCL_TIPGUI,BD6->BD6_DATPRO,nil,nil)
Endif

BD7->(DbSetOrder(1))
If BD7->(MsSeek(xFilial("BD7")+BD6->(BD6_CODOPE+BD6_CODLDP+BD6_CODPEG+BD6_NUMERO+BD6_ORIMOV+BD6_SEQUEN)))
   While ! BD7->(Eof()) .And. xFilial("BD7")+BD6->(BD6_CODOPE+BD6_CODLDP+BD6_CODPEG+BD6_NUMERO+BD6_ORIMOV+BD6_SEQUEN) == ;
                                              BD7->(BD7_FILIAL+BD7_CODOPE+BD7_CODLDP+BD7_CODPEG+BD7_NUMERO+BD7_ORIMOV+BD7_SEQUEN)


         BD3->(DbSetOrder(1))
         BD3->(DbSeek(xFilial("BD3")+BD7->BD7_CODUNM))
         aadd(aBD7,{BD3->BD3_ORDEM , BD7->(Recno()) , If(  (nI := aScan(aPriorida,{|x| alltrim(BD7->BD7_CODUNM) $ x[1] }))   > 0 , nI , 999 )})
         
   BD7->(DbSkip())
   Enddo
Else
   aadd(aCritNota,{PLSA700GCF("R704","NR_LOTE")+PLSA700GCF("R704","NR_NOTA"),"48","Codigo de procedimento existente porem sem unidade definida na TDE.",cCodSer})   
Endif

aBD7 := aSort(aBD7,,,{|x,y|x[1]<y[1]})                         
While .T.
	lSwapped := .F. 
 	For nI := 1 to (Len(aBD7)-1)
  		If aBD7[nI][3] > aBD7[nI+1][3]    		                       
    		aSlvAr       := aClone(aBD7[nI])
    		aBD7[nI]     := aClone(aBD7[nI+1])
    		aBD7[nI + 1] := aClone(aSlvAr)
      		lSwapped := .T. 
       Endif
    Next
    If !lSwapped
    	Exit
    Endif
Enddo


For nFor := 1 To Len(aBD7)

    BD7->(DbGoTo(aBD7[nFor,2]))         
        
    nAprCheio := 0
    nTaxCheio := 0
    lFlagDel  := .T.
    lAprUsado := .F.
    cTipApr   := ""       
    
    //ATENCAO ANTES DE ALTERAR ESTA LINHA CONSULTAR O PLSTEAM
    If  (AllTrim(cTpPart) == 'X' .and. AllTrim(BD7->BD7_CODUNM) $ PLSCHMP()) .or.   ;
    	(AllTrim(cTpPart) == '0' .and. AllTrim(cProxPart) == 'X' .and. !(AllTrim(BD7->BD7_CODUNM) $ PLSCHMP()))
    	loop
    Endif                                     
    
	If Empty(BD7->BD7_CODTPA) .And. ;
       ProcHM(AllTrim(BD7->BD7_CODUNM),AllTrim(cTpPart),BD7->BD7_NLANC,PLSA700GCF("R704","CD_PORTE_A")) .And. ;
       ! aCtrlBD7[1]
			
	      BD7->(RecLock("BD7",.F.))
          BD7->BD7_CODTPA := cTpPart
          BD7->BD7_SEQIMP := cCodSeq
          lExclui         := .F.
          lFlagDel        := .F.
       
       	  If nImpBD6 == '0'
		  
		      If ( Val(Subs(PLSA700GCF("R704","VL_SERV_CO"),1,12)+"."+Subs(PLSA700GCF("R704","VL_SERV_CO"),13,2)) == 0 ) .Or. ;
		         ( lUptHM .And. cTpPart == "0" )
		         BD7->BD7_BLOPAG := "1"
		         lExclui         := .T.
		      Endif   
        	   
              lUptHM := .t.

          endIf
          
          BD7->(MsUnLock())
          
          lAprUsado := BD7->BD7_VALORI > 0
          
          If lAprUsado 
             cTipApr := "HM"
          Endif
          
          aCtrlBD7[1] := .T.
	              
	Endif
	        
    If ! aCtrlBD7[1] .And. ! lAprUsado
               
       nAprCheio += Val(Subs(PLSA700GCF("R704","VL_SERV_CO"),1,12)+"."+Subs(PLSA700GCF("R704","VL_SERV_CO"),13,2)) 
       
       lAprUsado := nAprCheio > 0
       
       If lAprUsado 
          cTipApr := "HM"
       Endif
       
    Endif
            
	If (Empty(BD7->BD7_CODTPA) .or. (!Empty(BD7->BD7_CODTPA) .and. BD7->BD7_VALORI == 0 .and. Val(Subs(PLSA700GCF("R704","VL_CO_COB"),1,12)+"."+Subs(PLSA700GCF("R704","VL_CO_COB"),13,2)) > 0) ) .And. ;
	   ProcCOP(AllTrim(BD7->BD7_CODUNM),AllTrim(cTpPart)) .And. ;
	   ! aCtrlBD7[2] .And. ;
	   ! lAprUsado
		    
		       BD7->(RecLock("BD7",.F.))
			   BD7->BD7_CODTPA := cTpPart
			   BD7->BD7_SEQIMP := cCodSeq
			   lExclui         := .F.
			   lFlagDel        := .F. 
			   
			   If nImpBD6 == '0'
				
				   If Val(Subs(PLSA700GCF("R704","VL_CO_COB"),1,12)+"."+Subs(PLSA700GCF("R704","VL_CO_COB"),13,2)) == 0 
	   			      BD7->BD7_BLOPAG := "1"
				      lExclui         := .T.
				   Endif
				   
				   aCtrlBD7[2] := .T.
				
               endIf
               			              
	           BD7->(MsUnLock())
	           
   			   lAprUsado := BD7->BD7_VALORI > 0
                            
               If lAprUsado 
   	              cTipApr := "CO"
   	              If BD7->BD7_BLOPAG == '1'
     	              BD7->(RecLock("BD7",.F.))
   	              	  BD7->BD7_BLOPAG := ""      
   	              	  BD7->BD7_MOTBLO := ""
					  BD7->BD7_DESBLO := ""          
   	              	  BD7->(MsUnLock())
   	              Endif
	           Endif

	Endif
		    
    If ! aCtrlBD7[2] .And. ! lAprUsado
       
       nAprCheio += Val(Subs(PLSA700GCF("R704","VL_CO_COB"),1,12)+"."+Subs(PLSA700GCF("R704","VL_CO_COB"),13,2))
       lAprUsado := nAprCheio > 0

       If lAprUsado                          
          cTipApr := "CO"
       Endif
       
    Endif
     	    
    If (Empty(BD7->BD7_CODTPA) .or. (!Empty(BD7->BD7_CODTPA) .and. BD7->BD7_VALORI == 0 .and. Val(Subs(PLSA700GCF("R704","VL_FILME_C"),1,12)+"."+Subs(PLSA700GCF("R704","VL_FILME_C"),13,2)) > 0) ) .And. ;
       ProcFIL(AllTrim(BD7->BD7_CODUNM),AllTrim(cTpPart)) .And. ;
       ! aCtrlBD7[3] .And. ;
       ! lAprUsado
            
		   BD7->(RecLock("BD7",.F.))
		   BD7->BD7_CODTPA := cTpPart
		   BD7->BD7_SEQIMP := cCodSeq
		   lExclui         := .F. 
		   lFlagDel        := .F.
		   
		   If nImpBD6 == '0'
			
			   If Val(Subs(PLSA700GCF("R704","VL_FILME_C"),1,12)+"."+Subs(PLSA700GCF("R704","VL_FILME_C"),13,2)) == 0 
 			      BD7->BD7_BLOPAG := "1"
			      lExclui         := .T.
			   Endif           
			   
			   aCtrlBD7[3] := .T.
			
		   Endif
		   BD7->(MsUnLock()) 
		   
           lAprUsado := BD7->BD7_VALORI > 0

           If lAprUsado 
              cTipApr := "FI"
              If BD7->BD7_BLOPAG == '1'
              	  BD7->(RecLock("BD7",.F.))
              	  BD7->BD7_BLOPAG := ""
              	  BD7->BD7_MOTBLO := ""
				  BD7->BD7_DESBLO := ""          
              	  BD7->(MsUnLock()) 
              Endif
           Endif

	Endif	                                                   
		    
	If ! aCtrlBD7[3] .And. ! lAprUsado
		
		nAprCheio += Val(Subs(PLSA700GCF("R704","VL_FILME_C"),1,12)+"."+Subs(PLSA700GCF("R704","VL_FILME_C"),13,2))
		lAprUsado := nAprCheio > 0

        If lAprUsado 
   	    	cTipApr := "FI"
		endIf
		
	endIf   
                                                                                   
	If BD7->BD7_VALORI == 0 .And. (nAprCheio + nTaxCheio) > 0 .And.; 
	   ( (ProcHM(AllTrim(BD7->BD7_CODUNM),AllTrim(cTpPart),BD7->BD7_NLANC,PLSA700GCF("R704","CD_PORTE_A")) .And. cTipApr == "HM") .or.;
	     (ProcFIL(AllTrim(BD7->BD7_CODUNM),AllTrim(cTpPart)) .And. cTipApr == "FI") .or.;
	     (ProcCOP(AllTrim(BD7->BD7_CODUNM),AllTrim(cTpPart))  .And. cTipApr == "CO" ) ) .And. nImpBD6 == '0'
	       
	       If cTipApr == "HM" .And. ! aCtrlBD7[1]
	          aCtrlBD7[1] := .T.
	       Endif   
	       
	       If cTipApr == "CO" .And. ! aCtrlBD7[2]
	          aCtrlBD7[2] := .T.
	       Endif   

	       If cTipApr == "FI" .And. ! aCtrlBD7[3]
	          aCtrlBD7[3] := .T.
	       Endif   
	       
	Endif
		    
	If lAltQtd  .And. alltrim(BD7->BD7_CODTPA) == AllTrim(cTpPart) .and. nImpBD6 == '0'
		        
        nTotAtM := BD7->BD7_VALORI
    			
    	If ProcCOP(AllTrim(BD7->BD7_CODUNM),AllTrim(cTpPart))

		    nQtdPro := val(PLSA700GCF("R704","QT_PAGA"))
			nTotPro := Val(Subs(PLSA700GCF("R704","VL_CO_COB"),1,12)+"."+Subs(PLSA700GCF("R704","VL_CO_COB"),13,2))
    		
    	ElseIf ProcFIL(AllTrim(BD7->BD7_CODUNM),AllTrim(cTpPart))

	        nQtdPro := val(PLSA700GCF("R704","QT_PAGA"))
    		nTotPro := Val(Subs(PLSA700GCF("R704","VL_FILME_C"),1,12)+"."+Subs(PLSA700GCF("R704","VL_FILME_C"),13,2))
			
    	ElseIf ProcHM(AllTrim(BD7->BD7_CODUNM),AllTrim(cTpPart),BD7->BD7_NLANC,PLSA700GCF("R704","CD_PORTE_A")) 

		    nQtdPro := val(PLSA700GCF("R704","QT_PAGA"))
    		nTotPro := Val(Subs(PLSA700GCF("R704","VL_SERV_CO"),1,12)+"."+Subs(PLSA700GCF("R704","VL_SERV_CO"),13,2))

    	endIf                           

	endIf

	//incrementa valores distribuidos no arquivos distribuindo no BD7
	if nAprCheio > 0
	
		PLDISBD7(nil, nil)
		
		//guarda total do bd7 para posterior conferencia
		getTotBD7(aMatTOTBD7)
		
	endIf
	
Next          

//verifica se o total do BD7 esta igual ao BD6 e ajusta
if len(aMatTOTBD7) > 0
	setAjuGUI(aMatTOTBD7)
endIf			 

If ExistBlock("PLS700CG")
   ExecBlock("PLS700CG",.F.,.F.)
Endif   

Return      

/*/{Protheus.doc} AtualizaDadosGIH
Atualiza determinado campos na GIH 
@author  PLSTEAM
@version P11
@since   24.01.2005
/*/  
static Function AtualizaDadosGIH()
LOCAL cAuxDat  := PLSA700GCF("R703","DT_ALTA")
LOCAL dDatAlta := ctod(subs(cAuxDat,9,2)+"/"+subs(cAuxDat,6,2)+"/"+subs(cAuxDat,1,4))
LOCAL cHorAlta := subs(cAuxDat,11,2)+subs(cAuxDat,14,2)
LOCAL cAux1Dat := PLSA700GCF("R703","DT_INTERNA")
LOCAL dDatPro  := ctod(subs(cAux1Dat,9,2)+"/"+subs(cAux1Dat,6,2)+"/"+subs(cAux1Dat,1,4))
LOCAL cHorPro  := subs(cAux1Dat,11,2)+subs(cAux1Dat,14,2)
LOCAL nDiasIN  := 0
LOCAL nY	   := 0
BE4->(RecLock("BE4",.F.))
BE4->BE4_TIPADM := "0"
BE4->BE4_AUDITO := "0"        
                            

If Len(aCodEdi) == 0 .and. BQR->(FieldPos("BQR_CODEDI")) > 0       
	BQR->(dbSetOrder(1))
	If BQR->(MsSeek(xFilial("BQR")))
		While BQR->(!Eof()) .and. xFilial("BQR") == BQR->BQR_FILIAL
			aadd(aCodEdi,{BQR->BQR_CODEDI,BQR->BQR_GRPINT,BQR->BQR_TIPINT})
			BQR->(DbSkip())
		Enddo
	Endif
Endif

If ExistBlock("PLS700IN")
	ExecBlock("PLS700IN",.F.,.F.,{ PLSA700GCF("R703","TP_INTERNA") })
Else       
	If (nY := aScan(aCodEdi,{|x| alltrim(x[1]) == alltrim(PLSA700GCF("R703","TP_INTERNA"))})) > 0
		BE4->BE4_GRPINT := aCodEdi[nY][2]
		BE4->BE4_TIPINT := aCodEdi[nY][3]
	Else
		Do Case
			Case PLSA700GCF("R703","TP_INTERNA") == "1" // Internacao Clinica
				BE4->BE4_GRPINT := "1" 
				BE4->BE4_TIPINT := "01"
			Case PLSA700GCF("R703","TP_INTERNA") == "6" // Pediatrica
				BE4->BE4_GRPINT := "1" 
				BE4->BE4_TIPINT := "02"
			Case PLSA700GCF("R703","TP_INTERNA") == "7" // Psiquiatrica
				BE4->BE4_GRPINT := "1"
				BE4->BE4_TIPINT := "03"
			Case PLSA700GCF("R703","TP_INTERNA") == "3" // Internacao Obstetrica
				BE4->BE4_GRPINT := "1"
				BE4->BE4_TIPINT := "05"
			Case PLSA700GCF("R703","TP_INTERNA") == "2" // Internacao Cirurgica
				BE4->BE4_GRPINT := "2"
				BE4->BE4_TIPINT := "01"
			Case PLSA700GCF("R703","TP_INTERNA") == "3" // Internacao Obstetrica
				BE4->BE4_GRPINT := "2" 
				BE4->BE4_TIPINT := "03"
			Case PLSA700GCF("R703","TP_INTERNA") == "4" .and. !(cCodLayPLS >= '3.2' .and. nNVerTra >= 10)// Hospital dia - este .and. foi necessario pois na versao 3.2 estas opcoes foram retiradas de uso
				BE4->BE4_GRPINT := "1"
				BE4->BE4_TIPINT := "06"
			Case PLSA700GCF("R703","TP_INTERNA") == "5" .and. !(cCodLayPLS >= '3.2' .and. nNVerTra >= 10)// Domiciliar - este .and. foi necessario pois na versao 3.2 estas opcoes foram retiradas de uso
				BE4->BE4_GRPINT := "1"
				BE4->BE4_TIPINT := "07"
		EndCase        
	Endif
EndIf
					
BE4->BE4_PADINT := If(PLSA700GCF("R703","TP_ACOMODA")=="A","2","1")
BE4->BE4_DTALTA := dDatAlta
BE4->BE4_HRALTA := cHorAlta
BE4->BE4_DATPRO := dDatPro
BE4->BE4_HORPRO := cHorPro

If Len(aDadUsr) > 0 .and. aDadUsr[1] 
	BE4->BE4_PADCON := PLSACOMUSR(aDadUsr[2],'2') 
Endif

nDiasIN := (BE4->BE4_DTALTA-BE4->BE4_DATPRO)+1

If nDiasIN >= 99 .Or. nDiasIN <= 0
   aadd(aCritNota,{PLSA700GCF("R703","NR_LOTE")+PLSA700GCF("R703","NR_NOTA"),"47","Campo data de internacao x data de alta invalido na guia de internacao.",""})
Else
   BE4->BE4_DIASIN := nDiasIN   
Endif   

BE4->BE4_TIPALT := PLSA700GCF("R703","TP_SAIDA")
BE4->BE4_STATUS := "1"

Do Case 
	Case PLSA700GCF("R703","ID_OBT1","022") == "S"
		BE4->BE4_TIPNAS := '01'
	Case PLSA700GCF("R703","ID_OBT2","023") == "S"
		BE4->BE4_TIPNAS := '02'
	Case PLSA700GCF("R703","ID_OBT3","024") == "S"
		BE4->BE4_TIPNAS := '06'
	Case PLSA700GCF("R703","ID_OBT4","025") == "S"
		BE4->BE4_TIPNAS := '07'
	Case PLSA700GCF("R703","ID_OBT5","026") == "S"
		BE4->BE4_TIPNAS := '08'
	Case PLSA700GCF("R703","ID_OBT6","027") == "S"
		BE4->BE4_TIPNAS := '09'
	Case PLSA700GCF("R703","ID_OBT7","028") == "S"
		BE4->BE4_TIPNAS := '10'
	Case PLSA700GCF("R703","ID_OBT8","029") == "S"
		BE4->BE4_TIPNAS := '11'                	
	Case PLSA700GCF("R703","ID_OBT9","030") == "S"
	BE4->BE4_TIPNAS := '12'
EndCase
	
BE4->BE4_NASVIV := val(PLSA700GCF("R703","QT_NAVIVOS","017"))
BE4->BE4_NASMOR := val(PLSA700GCF("R703","QT_NAMORTO","018"))
BE4->BE4_NASVPR := val(PLSA700GCF("R703","QT_NAVIVOP","019"))
BE4->BE4_OBTPRE := val(PLSA700GCF("R703","QT_OBITOPR","020"))
BE4->BE4_OBTTAR := val(PLSA700GCF("R703","QT_OBITOTR","021"))
BE4->BE4_TIPFAT := PLSA700GCF("R703","TP_FATURA","031")
BE4->BE4_CIDOBT := PLSA700GCF("R703","CD_CIDOBT","032")
BE4->BE4_NRDCOB := PLSA700GCF("R703","NR_DECOBT","033")
BE4->BE4_OBTMUL	:= PLSA700GCF("R703","TP_OBTMULH","039")
BE4->BE4_TIPPAR := ""
BE4->BE4_CGCRDA := PLSA700GCF("R703","CGC_HOSPIT")
BE4->(MsUnLock())
Return

//+------------------------------------------------------------------
/*/{Protheus.doc} PLSED700VS
Importacao A700 
@author  PLS TEAM
@version P11
@since   17.01.2005
/*/
//+------------------------------------------------------------------
Function PLSED700VS
PLSED700GN("1",.T.)
Return

//+------------------------------------------------------------------
/*/{Protheus.doc} PLSED700GN
Importacao A700 
@author  PLSTEAM
@version P11
@since   24.01.2005
/*/  
//+------------------------------------------------------------------
static Function PLSED700GN(cTipo,lMsg)
LOCAL aDados   := {}
LOCAL lRet     := .F.
LOCAL cChave   := BRJ->BRJ_CODIGO
LOCAL nRecno   := BRJ->(Recno())
LOCAL nOrdem   := BRJ->(IndexOrd())
LOCAL aPegs    := {}

DEFAULT lMsg := .F.

aadd(aDados,{"Data da Importacao",dtoc(BRJ->BRJ_DATA)})
aadd(aDados,{"Arquivo de Importacao",AllTrim(BRJ->BRJ_ARQUIV)})
aadd(aDados,{"Operadora Origem",BRJ->BRJ_OPEORI+" - "+Alltrim(BRJ->BRJ_NOMORI)})
aadd(aDados,{"Competencia do Arquivo",BRJ->(BRJ_ANO+"/"+BRJ_MES)})
aadd(aDados,{"Competencia de Pagto",BRJ->(BRJ_ANOPAG+"/"+BRJ_MESPAG)})
aadd(aDados,{"Valor da Fatura",TransForm(BRJ->BRJ_VALOR,PLPMONEY_P)})
aadd(aDados,{"Numero da Fatura",BRJ->BRJ_NUMFAT})
aadd(aDados,{"Data Pagamento Inicial",dtoc(BRJ->BRJ_DTEMIS)})                                           
aadd(aDados,{"Data Pagamento Final",dtoc(BRJ->BRJ_DTVENC)})
aadd(aDados,{"",""})
aadd(aDados,{"",""})
aadd(aDados,{"",""})
aadd(aDados,{"Status",X3COMBO("BRJ_STATUS",BRJ->BRJ_STATUS)})

aadd(aDados,{"",""})
DbSelectArea("BRJ")
BRJ->(DbClearFilter())
BRJ->(DbSetOrder(1))
BRJ->(MsSeek(xFilial("BRJ")+cChave))
While ! BRJ->(Eof()) .And. BRJ->BRJ_CODIGO == cChave
	  If !Empty(BRJ->(BRJ_CODLDP+BRJ_CODPEG))
      aadd(aDados,{"PEG Criada",BRJ->(BRJ_CODOPE+"."+BRJ_CODLDP+"."+BRJ_CODPEG)})
      aadd(aPegs,{BRJ->BRJ_CODOPE,BRJ->BRJ_CODLDP,BRJ->BRJ_CODPEG})              
	  Endif
BRJ->(DbSkip())
Enddo 

If Len(aDados) > 0 .And. lMsg
   lRet := PLSCRIGEN(aDados,{ {"Campo","@C",80} , {"Conteudo","@C",80 } },IF(cTipo=="1","Dados da Importacao - Arquivo "+AllTrim(cArquivo),"Confirma Cancelamento da Importacao - Arquivo "+AllTrim(cArquivo)),NIL,NIL,NIL,NIL, NIL,NIL,"G",220)[1]
Endif

If Valtype(oBrwBRJ) == "O"
    oBrwBRJ:Refresh(.T.)
    oBrwBRJ:ExecuteFilter()
Else
	DbSelectArea("BRJ")
	cPLSFiltro := "@BRJ_FILIAL = '"+xFilial("BRJ")+"' AND BRJ_REGPRI = '3' AND D_E_L_E_T_ = ' '"
	SET FILTER TO &(cPLSFiltro)
EndIf
	
BRJ->(DbGoTo(nRecno))
BRJ->(DbSetOrder(nOrdem))

Return(If(cTipo=="1",lRet,{lRet,aPegs}))

//+------------------------------------------------------------------
/*/{Protheus.doc} PLSEDIA700
Trata a exclus�o de uma importacao
@author  PLSTEAM
@version P11
@since   24.01.2005
/*/  
//+------------------------------------------------------------------
Function PLSED700CN
LOCAL aRet
LOCAL lOK 
LOCAL aDados
LOCAL nFor              
LOCAL nI        := 0
LOCAL aBD6Clear := {}
LOCAL cChave    := BRJ->BRJ_CODIGO                                           

aRet   := PLSED700GN("2",.T.)
lOK    := aRet[1]
aDados := aRet[2]

If lOK
   Begin Transaction

   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
   //� Ponto de entrada antes da exclusao das guias importadas                  �
   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
   If ExistBlock("PLSP700E")
      ExecBlock("PLSP700E",.F.,.F.)
   Endif

   For nFor := 1 To Len(aDados)
       BCI->(DbSetOrder(1))
       If BCI->(MsSeek(xFilial("BCI")+aDados[nFor,1]+aDados[nFor,2]+aDados[nFor,3]))
          //ALTERO PARA PODER USAR A FUNCAO GENERICA
          BCI->(Reclock("BCI",.F.)) 
          BCI->BCI_FASE := '1'
          	If BCI->( FieldPos("BCI_STTISS") ) > 0
				BCI->BCI_STTISS := "1" //Recebido
			EndIf
          BCI->(MsUnlock())
          PLSEXCPEG()
       Endif
   Next   
   
   DbSelectArea("BRJ")
   SET FILTER TO

   BRJ->(DbSetOrder(1))
   While BRJ->(DbSeek(xFilial("BRJ")+cChave))
         BRJ->(RecLock("BRJ",.F.))
         BRJ->(DbDelete())
         BRJ->(MsUnLock())   
   Enddo    
	
   End Transaction

	If Valtype(oBrwBRJ) == "O"
	    oBrwBRJ:Refresh(.T.)
	    oBrwBRJ:ExecuteFilter()
	Else
	   DbSelectArea("BRJ")
	   cPLSFiltro := "@BRJ_FILIAL = '"+xFilial("BRJ")+"' AND BRJ_REGPRI = '3' AND D_E_L_E_T_ = ' '"
	   SET FILTER TO &(cPLSFiltro)  
   EndIF
Endif

Return   

//+------------------------------------------------------------------
/*/{Protheus.doc} PLSP700LEG
Exibe a legenda
@author  PLSTEAM
@version P11
@since   05.05.05
/*/
//+------------------------------------------------------------------
Function PLSP700Leg()

BrwLegenda(cCadastro,"Status" ,aCdCores)

Return

//+------------------------------------------------------------------
/*/{Protheus.doc} AnalisaCrit
Importacao A700 
@author  PLSTEAM
@version P11
@since   05.05.2005
/*/
//+------------------------------------------------------------------
static Function AnalisaCrit(cCodigo,cDado,cDadoAux,cTipo,cLote,cNota)
LOCAL nPos                                              
LOCAL lFlag := .T.                   
LOCAL cCritica
DEFAULT cDadoAux := ""
DEFAULT cTipo    := "1"

nPos := Ascan(aCriticas,{|x| x[1] == cCodigo})

If nPos > 0  
   If Empty(cDadoAux)
      cCritica := aCriticas[nPos,2]
   Else
      cCritica := AllTrim(aCriticas[nPos,2])+"   ["+AllTrim(cDadoAux)+"]"
   Endif   
   
   If cTipo == "1"
   		aadd(aCritReal,{aCriticas[nPos,1],cCritica,alltrim(str(nLine)),cDado})
   Elseif cTipo == "2"
   	    aadd(aGuiaNaoExi,{aCriticas[nPos,1],cCritica,cLote,cNota,cDado })                           
   Endif
   
   lFlag := aCriticas[nPos,3]=="1"
Else
   If cTipo == "1"
   		aadd(aCritReal,{cCodigo,"Codigo da Critica nao localizado",alltrim(str(nLine)),cDado})
   Elseif cTipo == "2"
   		aadd(aGuiaNaoExi,{cCodigo,"Codigo da Critica nao localizado",cLote,cNota,"","","","" })                           
   Endif
   
   lFlag := .F.
Endif                      

If ! lFlag .And. lImpPTU
   lImpPTU := .F.
Endif   

Return(lFlag)                                 

//+------------------------------------------------------------------
/*/{Protheus.doc} PLSLSC700
F3 da listagem das criticas do ptu 700  
@author  PLSTEAM
@version P11
@since   01.06.2005
/*/
//+------------------------------------------------------------------
Function PLSLSC700(cDado1,cDado2,lTela,cDado3)
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Define variaveis...                                                      �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
LOCAL oDlg
LOCAL nOpca     := 0
LOCAL bOK       := { || nOpca := K_OK, oDlg:End() }
LOCAL bCancel   := { || oDlg:End() }
LOCAL oCritica
LOCAL cSQL
LOCAL aLista    := {}
LOCAL nInd                     
LOCAL nPos
DEFAULT lTela := .T.                   
DEFAULT cDado3:= ""                                                                                 
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� coloca virgula no comeco (caso tenha inicializador padrao)               �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
cDado1  := AllTrim(cDado1)
cDado2  := AllTrim(cDado2)
cDado3  := AllTrim(cDado3)

if subs(cDado1,len(cDado1),1) != "," .AND. cDado1 != ""
	cDado1 += ","
endif

aCriticas := {}
CarregaCrit()
For nInd := 1 To Len(aCriticas)
	If aCriticas[nInd,3] == "1"
		aadd(aLista,{aCriticas[nInd,1],aCriticas[nInd,2],If(aCriticas[nInd,1]$cDado1+cDado2+cDado3,.T.,.F.)})
	Endif
Next

If lTela

   DEFINE MSDIALOG oDlg TITLE "Criticas" FROM ndLinIni,ndColIni TO ndLinFin,ndColFin OF GetWndDefault()

   @ 020,012 SAY oSay PROMPT "Selecione a(s) criticas(s) que podem(rao) impedir a importacao" SIZE 300,010 OF oDlg PIXEL COLOR CLR_HBLUE

   oCritica := TcBrowse():New( 035, 012, 330, 150,,,, oDlg,,,,,,,,,,,, .F.,, .T.,, .F., )
                                            
   oCritica:AddColumn(TcColumn():New(" ",{ || IF(aLista[oCritica:nAt,3],LoadBitmap( GetResources(), "LBOK" ),LoadBitmap( GetResources(), "LBNO" )) },;
            "@c",nil,nil,nil,015,.T.,.T.,nil,nil,nil,.T.,nil))     

   oCritica:AddColumn(TcColumn():New("Codigo",{ || OemToAnsi(aLista[oCritica:nAt,1]) },;
            "@!",nil,nil,nil,020,.F.,.F.,nil,nil,nil,.F.,nil))     

   oCritica:AddColumn(TcColumn():New("Descricao",{ || OemToAnsi(aLista[oCritica:nAt,2]) },;
            "@C",nil,nil,nil,200,.F.,.F.,nil,nil,nil,.F.,nil))     

   oCritica:SetArray(aLista)         
   oCritica:bLDblClick := { || aLista[oCritica:nAt,3] := IF(aLista[oCritica:nAt,3],.F.,.T.) }

   ACTIVATE MSDIALOG oDlg ON INIT EnChoiceBar(oDlg,bOK,bCancel,.F.,{})

Else
   nOpca := K_OK
Endif

If nOpca == K_OK
                  
   cDado1 := ""
   cDado2 := ""
   cDado3 := ""
   For nInd := 1 To Len(aLista)
   
       If aLista[nInd,3]
			If nInd <= 20
             cDado1 += aLista[nInd,1]+","
          Elseif nInd > 20 .and. nInd <= 40                           
             cDado2 += aLista[nInd,1]+","
          Else
          	 cDado3 += aLista[nInd,1]+","
          Endif   
          
          nPos := Ascan(aCriticas,{|x| x[1] == aLista[nInd,1]})
          If nPos > 0
             aCriticas[nPos,3] := "0"
          Endif   
       Endif   
   Next
Endif
                                  
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� tira a virgula do final                                                  �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
if subs(cDado1,len(cDado1),1) == ","
	cDado1 := subs(cDado1,1,len(cDado1)-1)
endif                

if subs(cDado2,len(cDado2),1) == ","
	cDado2 := subs(cDado2,1,len(cDado2)-1)
endif              

if subs(cDado3,len(cDado3),1) == ","
	cDado3 := subs(cDado3,1,len(cDado3)-1)
endif                
     

Return

//+------------------------------------------------------------------
/*/{Protheus.doc} PLSEDIA700
F3 da listagem das criticas do ptu 700  
@author  PLSTEAM
@version P11
@since   01.06.2005
/*/
//+------------------------------------------------------------------
Function PLSLAR700(cArqImp)
LOCAL cSalvo := cArqImp

cArqImp := cGetFile("*.*","Selecione o Arquivo",0,"SERVIDOR\",.T.,GETF_LOCALFLOPPY + GETF_LOCALHARD + GETF_NETWORKDRIVE)  

If Empty(cArqImp)
   cArqImp := cSalvo
Endif   

Return

//+------------------------------------------------------------------
/*/{Protheus.doc} PLSP700Usr
Analisa uma importacao e lista os usuarios nao encontrados.
@author  PLSTEAM
@version P11
@since   01.06.2005
/*/
//+------------------------------------------------------------------
static Function PLSP700Usr(cAlias,nReg,nOpc,cNil,lMsg,cCodProGen)
LOCAL aRet   := PLSED700GN("2",.F.)
LOCAL lOK    := aRet[1]
LOCAL aDados := aRet[2]          
LOCAL nFor
LOCAL cMatrUsrGen
LOCAL aNotas := {}
LOCAL cSQL                             
DEFAULT lMsg := .T.     
DEFAULT cCodProGen := ""

If Empty(cArquivo) //variavel static
   cArquivo := BRJ->BRJ_ARQUIV
Endif   

If Empty(cCodProGen)
   Pergunte("PLS700    ",.F.)
   cCodProGen := alltrim(mv_par06)
Endif   

BA1->(DbSetOrder(5))
If BA1->(MsSeek(xFilial("BA1")+cMatrAntGen))
   cMatrUsrGen := BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG+BA1_DIGITO)
   
   BD5->(DbSetOrder(5))
   If BD5->(MsSeek(xFilial("BD5")+cMatrUsrGen))
      While ! BD5->(Eof()) .And. BD5->(BD5_FILIAL+BD5_OPEUSR+BD5_CODEMP+BD5_MATRIC+BD5_TIPREG+BD5_DIGITO) == ;
                                  xFilial("BD5")+cMatrUsrGen
            If Ascan(aDados,{|x| x[1]+x[2]+x[3] == BD5->(BD5_CODOPE+BD5_CODLDP+BD5_CODPEG) }) > 0
               aadd(aNotas,{"Usuario Invalido",;
                            BD5->BD5_CODOPE+"-"+BD5->BD5_CODLDP+"-"+BD5->BD5_CODPEG+"-"+BD5->BD5_NUMERO,;
                            BD5->BD5_MATANT,;
                            Subs(BD5->BD5_NOMUSR,1,30),;
                            BD5->BD5_NUMIMP,;
                            ""})
            Endif
      BD5->(DbSkip())
      Enddo
   Endif   
Endif

For nFor := 1 To Len(aDados)
    cSQL := "SELECT BD6_CODOPE,BD6_CODLDP,BD6_CODPEG,BD6_NUMERO,BD6_MATANT,BD6_NOMUSR,BD6_NUMIMP,BD6_DATPRO, BD6_CODPRO "
    cSQL += ", BD6_CODERR "
    
    cSQL += "FROM "+RetSQLName("BD6")+" WHERE "
    cSQL += "BD6_FILIAL = '"+xFilial("BD6")+"' AND "
    cSQL += "BD6_CODOPE = '"+aDados[nFor,1]+"' AND "
    cSQL += "BD6_CODLDP = '"+aDados[nFor,2]+"' AND "
    cSQL += "BD6_CODPEG = '"+aDados[nFor,3]+"' AND "
    cSQL += "BD6_CODPRO = '"+cCodProGen+"' AND "
    cSQL += "D_E_L_E_T_ = ' '"
    PLSQuery(cSQL,"Trb")
    
    While ! Trb->(Eof())
          aadd(aNotas,{"Cod. Evento Invalido",;
                        Trb->BD6_CODOPE+"-"+Trb->BD6_CODLDP+"-"+Trb->BD6_CODPEG+"-"+Trb->BD6_NUMERO,;
                        Trb->BD6_MATANT,;
                        Subs(Trb->BD6_NOMUSR,1,30),;
                        Trb->BD6_NUMIMP,;
                        Trb->BD6_CODERR})
    Trb->(DbSkip())
    Enddo
    Trb->(DbCloseArea())
Next

Return (aNotas)

//+------------------------------------------------------------------
/*/{Protheus.doc} CriaUsrGen
Cria um usuario generico caso nao seja encontrado
@author  PLSTEAM
@version P11
@since   01.06.2005
/*/
//+------------------------------------------------------------------
static Function PLSUsrGen(cOpeOri,cNomeUsr)
LOCAL cMatric
LOCAL cCodInt    
LOCAL cCodEmp
LOCAL lStrTPLS	 := FindFunction("StrTPLS")                                              
LOCAL cModulo   := IIF(lStrTPLS,Modulo11(StrTPLS(cOpeOri+"999999999999")),Modulo11(cOpeOri+"999999999999"))
LOCAL aDadosUsr := PLSUSRIEVE(cOpeOri+"999999999999"+cModulo,"1",dDataBase)
LOCAL lCriaUsr  := .F.
LOCAL lRet      := .T.
LOCAL cConEmp
LOCAL cVerCon
LOCAL cSubCon
LOCAL cVerSub              
DEFAULT cNomeUsr := "USUARIO GENERICO PARA USO NO PTU 700 IMPORTACAO"

If     aDadosUsr[1] == "0"
       lRet     := .T.      
       lCriaUsr := .F.
ElseIf aDadosUsr[1] == "1"
       lRet     := .T.      
       lCriaUsr := .T.

       cConEmp  := aDadosUsr[5]
       cVerCon  := aDadosUsr[6]
       cSubCon  := aDadosUsr[7]
       cVerSub  := aDadosUsr[8]
ElseIf aDadosUsr[1] == "2"
       lRet     := .F.      
       lCriaUsr := .F.
Endif
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Verifica se existe o usuario generico...                                 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
If lCriaUsr
   BA1->(DbSetOrder(5))
   If ! BA1->(MsSeek(xFilial("BA1")+cMatrAntGen))
      //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      //� Se nao existir inclui...                                                 �
      //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
      cCodInt := PLSINTPAD()
      cCodEmp := GetNewPar("MV_PLSGEIN","0050")
      cMatric := PLPROXMAT(cCodInt,cCodEmp)
      
      BA3->(RecLock("BA3",.T.))
      BA3->BA3_FILIAL := xFilial("BA3")
      BA3->BA3_CODINT := cCodInt
      BA3->BA3_CODEMP := cCodEmp
      BA3->BA3_CONEMP := cConEmp
      BA3->BA3_VERCON := cVerCon
      BA3->BA3_SUBCON := cSubCon
      BA3->BA3_VERSUB := cVerSub
      BA3->BA3_ROTINA := "PLSPORFAI"
      BA3->BA3_MATRIC := cMatric
      BA3->BA3_MATANT := cMatrAntGen
      BA3->BA3_HORACN := StrTran(SubStr(Time(),1,5),":","")
      BA3->BA3_COBNIV := "0"
      BA3->BA3_VENCTO := 0
      BA3->BA3_DATBAS := dDataBase
      BA3->BA3_DATCIV := dDataBase
      BA3->BA3_TIPOUS := "2"
      BA3->BA3_USUOPE := BA3->(RETCODUSR())
      BA3->BA3_MODPAG := "2"
      BT6->(DbSetOrder(1))
      If BT6->(MsSeek(xFilial("BT6")+BA3->(BA3_CODINT+BA3_CODEMP+BA3_CONEMP+BA3_VERCON+BA3_SUBCON+BA3_VERSUB)))
         BA3->BA3_CODPLA := BT6->BT6_CODPRO
         BA3->BA3_VERSAO := BT6->BT6_VERSAO
      Else
         BA3->BA3_CODPLA := GetNewPar("MV_PLSPLPE","0001")
         BA3->BA3_VERSAO := GetNewPar("MV_PLSVRPE","001")
      Endif   
      BA3->BA3_FORPAG := GetNewPar("MV_PLSFCPE","101")
      BA3->BA3_DATCON := Date()
      BA3->BA3_HORCON := StrTran(SubStr(Time(),1,5),":","")
      BA3->(MsUnLock())

      BA1->(RecLock("BA1",.T.))
      BA1->BA1_FILIAL := xFilial("BA1")
      BA1->BA1_CODINT := BA3->BA3_CODINT
      BA1->BA1_CODEMP := BA3->BA3_CODEMP
      BA1->BA1_MATRIC := BA3->BA3_MATRIC
      BA1->BA1_CONEMP := BA3->BA3_CONEMP
      BA1->BA1_VERCON := BA3->BA3_VERCON
      BA1->BA1_SUBCON := BA3->BA3_SUBCON
      BA1->BA1_VERSUB := BA3->BA3_VERSUB
      BA1->BA1_IMAGE  := "ENABLE"
      BA1->BA1_TIPREG := GETMV("MV_PLCDTGP")
      If !lStrTPLS
         BA1->BA1_DIGITO := Modulo11(BA1->BA1_CODINT+BA1->BA1_CODEMP+BA1->BA1_MATRIC+BA1->BA1_TIPREG)
      Else
         BA1->BA1_DIGITO := Modulo11(StrTPLS(BA1->BA1_CODINT+BA1->BA1_CODEMP+BA1->BA1_MATRIC+BA1->BA1_TIPREG))
      EndIf
      BA1->BA1_NOMUSR := cNomeUsr
      BA1->BA1_TIPUSU := SuperGetMv("MV_PLCDTIT")
      BA1->BA1_GRAUPA := GetMv("MV_PLCDTGP")
      If BA1->(FieldPos("BA1_ENDCLI")) > 0
      	BA1->BA1_ENDCLI := "0"
      Endif
      BA1->BA1_SEXO   := ""
      BA1->BA1_MATEMP := ""
      BA1->BA1_MATANT := cMatrAntGen
      BA1->BA1_ESTCIV := ""
      BA1->BA1_CPFUSR := ""
      BA1->BA1_DRGUSR := ""
      BA1->BA1_DATINC := ctod("")
      BA1->BA1_DATNAS := ctod("")
      BA1->BA1_DATCAR := ctod("")
      BA1->BA1_CBTXAD := "1"
      BA1->BA1_OPEORI := cOpeOri
      BA1->BA1_OPEDES := cCodInt
      BA1->BA1_OPERES := cCodInt
      BA1->BA1_LOCATE := "2"
      BA1->BA1_LOCCOB := "2"
      BA1->BA1_LOCEMI := "2"
      BA1->BA1_LOCANS := "2"
      BA1->(MsUnLock())
      //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      //� Esta funcao analise a criacao de uma nova vida ou nao...                 �
      //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
      PLSA766ANV(nil,.F.)
      //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      //� Grava no usuario a vida criada ou a ja existente...                      �
      //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
      BA1->(RecLock("BA1",.F.))
	  BA1->BA1_MATVID := BTS->BTS_MATVID
      BA1->(MsUnLock())
   Endif   
Endif

Return(lRet)
                     
//+------------------------------------------------------------------
/*/{Protheus.doc} appfromtxt
apend from txt que funciona no linux
@author  PLS TEAM
@version P11
@since   01.06.2005
/*/
//+------------------------------------------------------------------
/* ===========================================================================
Fun豫o de Importa豫o de TXT para alias exclusivo TRB
Identifica linhas com CRLF e apenas LF ( formatos windows e Linux )  
=========================================================================== */
Static Function AppFromTXT(cTXTFile)
Local nH , nTam , nBuff , cBuff , cTXTBuff := ""
nH := fopen(cTXTFile)
if nH == -1
	Return -1 // Falha de abertura 
Endif
nTam := fseek(nH,0,2)
fseek(nH,0)
If nTam <= 0 
	fclose(nH)
	Return -2 // Tamanho do arquivo invalido
Endif
While nTam > 0 .or. !empty(cTXTBuff)
	
	If len(cTXTBuff) < 10000 .and. nTam > 0
		// Mantem um buffer minimo de 10K do arquivo em mem�ria 
		nBuff := min(10000,nTam)
		cBuff := space(nBuff)
		nRead := fread(nH,@cBuff,nBuff)
		If nRead<>nBuff
			fclose(nH)
			Return -3 // Falha de Leitura
		Endif
		cTXTBuff += left(cBuff,nBuff)
		nTam := nTam - nBuff
	Endif

	// Identifica linhas do arquivo com CRLF e LF 
	nPos := at(chr(10),cTXTBuff)
	If nPos > 0
		IF substr(cTXTBuff,nPos-1,1) == chr(13)
			cLine := substr(cTXTBuff,1,nPos-2)
		Else
			cLine := substr(cTXTBuff,1,nPos-1)
		Endif
		cTXTBuff := substr(cTXTBuff,nPos+1)

		// Alimenta campo com a linha 
		TRB->(dbappend(.T.))
		TRB->CAMPO := cLine

	Else

		cLine := cTXTBuff
		cTXTBuff := ''

		If !empty(cLine)
			// Alimenta campo com a ultima linha 
			// Apenas se tiver conteudo
			Trb->(dbappend(.T.))
			Trb->CAMPO := cLine
		Endif
		
	Endif

Enddo

fclose(nH)

// Reposiciona no topo a tabela
TRB->(dbCommit())
Trb->(DbGoTop())

Return 0 // Append Ok

//+------------------------------------------------------------------
/*/{Protheus.doc} AnalisaAmb
Analisa ambiente para verificar se rotina pode ser executada
@author  PLSTEAM
@version P11
@since   17.01.2005
/*/  
//+------------------------------------------------------------------
Static Function AnalisaAmb()
LOCAL lRet := .T.
LOCAL aRetAux
LOCAL aCri := {}

If ! FindFunction("PLCrProGen")
   aadd(aCri,{"Por favor atualiza fonte PLSA720."})
Endif          

If ! FindFunction("PLBusProTab")
   aadd(aCri,{"Por favor atualiza fonte PLSA720."})
Endif          

If BD6->(FieldPos("BD6_SOLORI")) == 0   
   aadd(aCri,{"Criar campo BD6_SOLORI."})
Endif

If BD6->(FieldPos("BD6_RDAEDI")) == 0   
   aadd(aCri,{"Criar campo BD6_RDAEDI."})
Endif

If BD6->(FieldPos("BD6_NOMEDI")) == 0   
   aadd(aCri,{"Criar campo BD6_NOMEDI."})
Endif

If BD6->(FieldPos("BD6_TRDAED")) == 0   
   aadd(aCri,{"Criar campo BD6_TRDAED."})
Endif

If BD6->(FieldPos("BD6_RPEDI")) == 0   
   aadd(aCri,{"Criar campo BD6_RPEDI."})
Endif

If BD6->(FieldPos("BD6_TPRDAE")) == 0   
   aadd(aCri,{"Criar campo BD6_TPRDAE."})
Endif

If BD6->(FieldPos("BD6_CNPJED")) == 0   
   aadd(aCri,{"Criar campo BD6_CNPJED."})
Endif

If BD6->(FieldPos("BD6_NOMCOB")) == 0   
   aadd(aCri,{"Criar campo BD6_NOMCOB."})
Endif

If BRJ->(FieldPos("BRJ_ANO")) == 0   
   aadd(aCri,{"Criar campo BRJ_ANO."})
Endif
If BRJ->(FieldPos("BRJ_TPPAG"))  == 0
   aadd(aCri,{"Criar campo BRJ_TPPAG."})
Endif
If BRJ->(FieldPos("BRJ_ANOPAG")) == 0
   aadd(aCri,{"Criar campo BRJ_ANOPAG."})
Endif
If BRJ->(FieldPos("BRJ_MESPAG")) == 0
   aadd(aCri,{"Criar campo BRJ_MESPAG."})
Endif
If BRJ->(FieldPos("BRJ_MES")) == 0
   aadd(aCri,{"Criar campo BRJ_MES."})
Endif
If BRJ->(FieldPos("BRJ_NUMFAT"))  == 0
   aadd(aCri,{"Criar campo BRJ_NUMFAT."})
Endif
If BRJ->(FieldPos("BRJ_VALOR")) == 0
   aadd(aCri,{"Criar campo BRJ_VALOR."})
Endif
If BRJ->(FieldPos("BRJ_DTEMIS")) == 0
   aadd(aCri,{"Criar campo BRJ_DTEMIS."})
Endif
If BRJ->(FieldPos("BRJ_DTVENC")) == 0
   aadd(aCri,{"Criar campo BRJ_DTVENC."})
Endif
If BRJ->(FieldPos("BRJ_CAMCOM")) == 0
   aadd(aCri,{"Criar campo BRJ_CAMCOM."})
Endif
If BRJ->(FieldPos("BRJ_FATPLS"))  == 0
   aadd(aCri,{"Criar campo BRJ_FATPLS."})
Endif
If BRJ->(FieldPos("BRJ_FCBPLS")) == 0
   aadd(aCri,{"Criar campo BRJ_FCBPLS."})
Endif
If BD6->(FieldPos("BD6_CODERR")) == 0
   aadd(aCri,{"Criar campo BD6_CODERR."})
Endif
If BD6->(FieldPos("BD6_ENVCON")) == 0
   aadd(aCri,{"Criar campo BD6_ENVCON."})
Endif

If ! PLSALIASEX("BRJ")
   aadd(aCri,{"Arquivo BRJ nao encontrado. Atualize versao."})
Endif   

BCI->(DbSetOrder(4))
If BCI->(FieldPos("BCI_ARQUIV")) == 0 .Or. ! "BCI_ARQUIV" $ BCI->(IndexKey())
   aadd(aCri,{"Criar campo BCI_ARQUIV e adicionar no ultimo campo da chave 4 do indice este campo criado."})
Endif            

If BD6->(FieldPos("BD6_INCAUT")) == 0
   aadd(aCri,{"Criar campo BD6_INCAUT caracter 1 Sim/Nao valor default '0'."})
Endif    

If BE4->(FieldPos("BE4_SEQIMP")) == 0
   aadd(aCri,{"Criar campo BE4_SEQIMP."})
Endif    

If BD5->(FieldPos("BD5_SEQIMP")) == 0
   aadd(aCri,{"Criar campo BD5_SEQIMP."})
Endif    

If BD6->(FieldPos("BD6_SEQIMP")) == 0
   aadd(aCri,{"Criar campo BD6_SEQIMP."})
Endif                     

If BD6->(FieldPos("BD6_SEQIMP")) == 0
   aadd(aCri,{"Criar campo BD6_SEQIMP."})
Endif                     

If BRJ->(FieldPos("BRJ_STATUS")) == 0
   aadd(aCri,{"Criar campo BRJ_STATUS."})
Endif                     

If BD7->(FieldPos("BD7_SEQIMP")) == 0
   aadd(aCri,{"Criar campo BD7_SEQIMP."})
Endif                     

If BD6->(FieldPos("BD6_DATCOB")) == 0
   aadd(aCri,{"Criar campo BD6_DATCOB."})
Endif                     

If BD6->(FieldPos("BD6_HORCOB")) == 0
   aadd(aCri,{"Criar campo BD6_HORCOB."})
Endif                     

If BD6->(FieldPos("BD6_MATCOB")) == 0
   aadd(aCri,{"Criar campo BD6_MATCOB."})
Endif                  

If ! SIX->(MsSeek("BD58"))          
   aadd(aCri,{"Atualize a versao para executar esta rotina"})
Endif

If ! PLSALIASEX("DE1")
   aadd(aCri,{"Arquivo DE1 nao encontrado no SX3"+FWGrpCompany()})
Endif   

If ! PLSALIASEX("DE9")
   aadd(aCri,{"Arquivo DE9 nao encontrado no SX3"+FWGrpCompany()})
Endif   

If ! PLSALIASEX("DE0")
   aadd(aCri,{"Arquivo DE0 nao encontrado no SX3"+FWGrpCompany()})
Endif   

If BGR->(FieldPos("BGR_CODEDI")) == 0
   aadd(aCri,{"Campo BGR_CODEDI nao encontrado."})   
Endif   

SIX->(DbSetOrder(1))
If ! SIX->(MsSeek("BD57"))
   aadd(aCri,{"Indice numero 7 do arquivo BD5 nao encontrado no SINDEX"})
Endif    

If ! SIX->(MsSeek("BE47"))
   aadd(aCri,{"Indice numero 7 do arquivo BE4 nao encontrado no SINDEX"})
Endif

If ! SIX->(MsSeek("BGR2"))
   aadd(aCri,{"Indice numero 2 do arquivo BGR nao encontrado no SINDEX"})
Endif

If ! SIX->(MsSeek("BR85"))
   aadd(aCri,{"Indice numero 5 do arquivo BR8 nao encontrado no SINDEX"})
Endif                     

If ! SIX->(MsSeek("BD6E"))
   aadd(aCri,{"Indice 14 do BD6 deve ser criado como BD6_FILIAL+BD6_SEQIMP+BD6_INCAUT"})
Endif                     

If ! "BD6_FILIAL + BD6_SEQIMP + BD6_INCAUT" $ SIX->CHAVE .and. ! "BD6_FILIAL+BD6_SEQIMP+BD6_INCAUT" $ SIX->CHAVE
   aadd(aCri,{"Indice 14 invalido. O correto seria BD6_FILIAL + BD6_SEQIMP + BD6_INCAUT"})
Endif
   
aRetAux := PLSXVLDCAL(dDataBase,PLSINTPAD(),.T.,"","")
         
If Len(aRetAux) >= 4 .and. empty(cAMPAG)
   cAMPAG := aRetAux[4]+aRetAux[5]
Else
   aadd(aCri,{"Nao existe calendario de pagamento para a competencia atual."})   
Endif                                             

If Len(aCri) > 0                                                                
   lRet := .F.
Endif

Return({lRet,aCri})

//+------------------------------------------------------------------
/*/{Protheus.doc} A700BCG
Cria grupo de perguntas. 
@author  Daher
@version P11
@since   17.01.2005
/*/
//+------------------------------------------------------------------
Static Function A700BCG( cPergSX1 )
	local cLastCod

	if( ! SX1->( msSeek( cPergSX1+"05" ) ) )
		BCG->( dbSetOrder( 1 ) )
		BCG->( msSeek( xFilial( "BCG" ) ) )
		
		while( ! BCG->( eof() ) )
			cLastCod := BCG->BCG_CODLDP
			BCG->( dbSkip() )
		Enddo
   
		BCG->( recLock( "BCG",.T. ) )
		BCG->BCG_FILIAL := xFilial( "BCG" )
		BCG->BCG_CODOPE := PLSINTPAD()
		BCG->BCG_CODLDP := strZero( val( cLastCod )+1,4 )
		BCG->BCG_DESCRI := "MOVIMENTACAO IMPORTADA VIA EDI A700"
		BCG->( msUnLock() )
	endif
return

//+------------------------------------------------------------------
/*/{Protheus.doc} CarregaCrit
Carrega criticas padroes
@author  PLSTEAM
@version P11
@since   17.01.2005
/*/  
//+------------------------------------------------------------------           
Static Function CarregaCrit()
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//| ATENCAO !!! nao excluir nenhum destes aadd e sempre que for adicionar, adiciona-los ao final da funca|
//| ATENCAO !!! nao excluir nenhum destes aadd e sempre que for adicionar, adiciona-los ao final da funca|
//| ATENCAO !!! nao excluir nenhum destes aadd e sempre que for adicionar, adiciona-los ao final da funca|
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸	 
aadd(aCriticas,{"01","Arquivo de importacao nao encontrado.","0"})
aadd(aCriticas,{"02","Nao foi possivel criar usuario generico.","0"})
aadd(aCriticas,{"03","Falha ao buscar posicao do tipo de registro.","0"})
aadd(aCriticas,{"04","Falha ao buscar posicao do numero sequencial.","0"})
aadd(aCriticas,{"05","Nao encontrado nenhum tipo de registro para o layout informado.","0"})
aadd(aCriticas,{"06","Layout definido na operadora origem nao disponivel para utilizacao.","0"})
aadd(aCriticas,{"07","Sequencia invalida para o campo NR_SEQ.","1"})
aadd(aCriticas,{"08","Operadora origem nao encontrada.","0"})
aadd(aCriticas,{"09","Operadora origem cadastrada como RDA nao encontrada.","0"})
aadd(aCriticas,{"10","Operadora origem cadastrada como RDA invalida.","0"})
aadd(aCriticas,{"11","Data de geracao invalida.","1"})
aadd(aCriticas,{"12","Numero da competencia invalido.", "1"})
aadd(aCriticas,{"13","Data de vencimento da fatura invalida.","1" })
aadd(aCriticas,{"14","Data de emissao da fatura invalida.","1"})
aadd(aCriticas,{"15","Operadora destino nao encontrada.","1"})
aadd(aCriticas,{"16","Numero de notas (LOTE+NOTA) repetidos em um mesmo lote.","1"})
aadd(aCriticas,{"17","Tipo de nota invalido.","1"})
aadd(aCriticas,{"18","Codigo da excecao ao atendimento invalido.","1"})
aadd(aCriticas,{"19","Codigo de tipo de consulta invalido.","1"})
aadd(aCriticas,{"20","Codigo de indicador do atendimento em ambulatorio invalido.","1"})
aadd(aCriticas,{"21","Codigo do tipo de paciente nao encontrado no cadastro de tipos de pacientes.","1"})
aadd(aCriticas,{"22","Chave (LOTE+NOTA) do registro 702 diferente do registro 703.","1"})
aadd(aCriticas,{"23","Numero de notas (LOTE+NOTA) repetidos em um mesmo lote.","1"})
aadd(aCriticas,{"24","Codigo do tipo de admissao nao encontrado no cadastro de tipos de admissao.","1"})
aadd(aCriticas,{"26","Chave (LOTE+NOTA) do registro 702 diferente do registro 704.","1"})
aadd(aCriticas,{"27","Data do servico invalida.","1"})
aadd(aCriticas,{"28","Nao foi possivel localizar o tipo de nascimento na tabela de tipos de nascimento.","1"})
aadd(aCriticas,{"29","Codigo do PTU nao localizado na tabela de vias de acesso.","1"})
aadd(aCriticas,{"31","Tipo de pessoa informado invalido.","1"})
aadd(aCriticas,{"32","Numero de notas (LOTE+NOTA) repetidos em um mesmo lote.","1"})
aadd(aCriticas,{"33","Quantidade de registros do tipo 702 invalida.","1"})
aadd(aCriticas,{"34","Quantidade de registros do tipo 703 invalida.","1"})
aadd(aCriticas,{"35","Quantidade de registros do tipo 704 invalida.","1"})
aadd(aCriticas,{"36","Quantidade de registros do tipo 705 invalida.","1"})
aadd(aCriticas,{"37","Quantidade de registros, com excecao diferente de zero, invalida.","1"})
aadd(aCriticas,{"38","Quantidade de registros de ambulatorio invalida.","1"})
aadd(aCriticas,{"39","Quantidade total de servicos cobrados invalida.","1"})
aadd(aCriticas,{"40","Valor total de servicos [VL_TOT_SER] nos registros do tipo 704 invalido.","1"})
aadd(aCriticas,{"41","Valor total da fatura (HEADER), nao corresponde ao somatorio dos valores.","1"})            
aadd(aCriticas,{"43","Campo do tipo de registro nao encontrado no layout.","1"})
aadd(aCriticas,{"44","Campo do tipo de registro nao encontrado no layout.","1"})
aadd(aCriticas,{"45","Codigo do servico nao encontrado.","1"})
aadd(aCriticas,{"46","Nota pre-autorizada sem eventos autorizados encontratos.","0"})
aadd(aCriticas,{"47","Campo data de internacao x data de alta invalido na guia de internacao.","1"})
aadd(aCriticas,{"48","Codigo de procedimento existente porem sem unidade definida na tde.","1"})
aadd(aCriticas,{"49","Para a conversao de CBHPM para AMB deve ser informado o parametro 'Tp.Tab. CBHPM' corretamente.","0"})
aadd(aCriticas,{"50","Codigo de procedimento generico nao localizado na tabela padrao.","0"})
aadd(aCriticas,{"51","Arquivo de importacao informado ja importado com a sequencia de importacao informada.","0"})
aadd(aCriticas,{"52","Valor total apresentado pela unimed diferente do total importado.","0"})
aadd(aCriticas,{"53","Guia do arquivo PTU nao encontrada como importada.","0"})
aadd(aCriticas,{"54","Nao localizado nro. do imp. no arquivo, verif. a regra de gravacao do impresso.","0"})  
aadd(aCriticas,{"55","Nao localizado nro. do imp. no sistema, verif. a regra de gravacao do impresso.","0"})  
aadd(aCriticas,{"56","Nao existe registro 704 para o registro 702 no arquivo informado.","1"})  
aadd(aCriticas,{"57","Valor importado no campo BD7_VLRAPR diferente do total da fatura.","1"})  
aadd(aCriticas,{"58","Quantidade de valores apresentados no registro 704 maior que a composicao do procedimento.","1"})  
aadd(aCriticas,{"59","Preenchimento do campo TP_OBSTETRICIA obrigatorio para a nota informada.","1"})  
aadd(aCriticas,{"60","Ao menos um dos campos das sequencias 017 a 021 devem ser preenchidos.","1"})  
aadd(aCriticas,{"61","Preenchimento do campo TP_SAIDA_CONS_SADT obrigatorio para guias de servico.","1"})  
aadd(aCriticas,{"62","Incompatibilidade entre participacoes enviadas e a participacao do procedimento.","1"})  
aadd(aCriticas,{"63","Informado registro do tipo 703 sem que existam diarias informadas no lote.","1"})  
aadd(aCriticas,{"64","Dom�nio do car�ter de atendimento inv�lido, para os procedimentos contidos na nota.","1"})  
aadd(aCriticas,{"65","Para o tipo de atendimento 07 (SADT Internado) o n�mero da guia principal deve ser informado.","1"})  
aadd(aCriticas,{"66","Conte�do campo tipo de admiss�o inv�lido.","1"})  
aadd(aCriticas,{"67","Tipo de interna豫o informado no regitro 703 inv�lido.","1"})  
aadd(aCriticas,{"68","Ao menos um dos campos das seq獪ncias 022 a 030 dever� estar sinalizado como Sim.","1"})  
aadd(aCriticas,{"69","Os campos de sequ�ncia 34 a 38 dever�o ser preenchidos com a quantidade de nascidos vivos.","1"})  
aadd(aCriticas,{"70","O campo NR_DECLARA_OBITO deve ser informado pois o campo CD_CID_OBITO foi preenchido.","1"})  
aadd(aCriticas,{"71","O campo CD_CNES_PREST deve ser informado para prestadores do tipo hospital.","1"})  

Return

//+------------------------------------------------------------------
/*/{Protheus.doc} PLSConTPA
Carga inicial de dados	
@author  PLSTEAM
@version P11
@since   17.01.2005
/*/  
//+------------------------------------------------------------------         
Static Function PLSConTPA()
LOCAL nFor
LOCAL nAux                    
LOCAL aLista    := {}

BKC->(DbSetOrder(1))
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� user function PL700TPA                                                   |
//| LOCAL aUnidades := PARAMIXB[1]                                           |
//|      aadd(aUnidades[Z],"XXX")                                            |
//| return aUnidades														 |
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
If Existblock("PL700TPA")
	aUnidades := Execblock("PL700TPA",.F.,.F.,{aUnidades})
Endif

For nFor := 1 To Len(aUnidades)
    aLista := {}
    For nAux := 1 To Len(aUnidades[nFor])
        If BKC->(DbSeek(xFilial("BKC")+aUnidades[nFor,nAux]+PLSINTPAD()))
           While ! BKC->(Eof()) .And. BKC->(BKC_FILIAL+BKC_CODIGO+BKC_CODOPE) == xFilial("BKC")+aUnidades[nFor,nAux]+PLSINTPAD()
                 aadd(aLista,{AllTrim(BKC->BKC_CODPAR),AllTrim(BKC->BKC_CODIGO)})
           BKC->(DbSkip())
           Enddo
        Endif
    Next
    aadd(aTpPart,aLista)
Next

Return

//+------------------------------------------------------------------
/*/{Protheus.doc} ProcHM
Composicao Honorario 
@author  PLSTEAM
@version P11
@since   17.01.2005
/*/  
//+------------------------------------------------------------------         
Static Function ProcHM(cCodigo,cTpPart,nLanc,cCodPA)
LOCAL nPos 		:= If(Ascan(aUnidades[1],cCodigo)>0,1,0)
DEFAULT cCodPA  := "" 

If nPos > 0
   If Val(nLanc) <> Val(cTpPart)
      nPos := 0
   Endif
Endif      

If nPos == 0
   nPos := If(Ascan(aUnidades[3],cCodigo)>0,3,0)
Endif          
If nPos == 0
   nPos := If(Ascan(aUnidades[4],cCodigo)>0 .and. !Empty(cCodPA),4,0)
Endif   
If nPos == 0
   nPos := If(Ascan(aUnidades[6],cCodigo)>0,6,0)
Endif   
If nPos == 0
   nPos := If(Ascan(aUnidades[7],cCodigo)>0,7,0)
Endif         

If nPos > 0
   If cTpPart $ "0"
      nPos := 1 //.t. no return
   Else   
      nPos := Ascan(aTpPart[nPos],{ |x| x[1] == cTpPart .And. x[2] == cCodigo })
   Endif   
Endif   

Return(nPos>0)

//+------------------------------------------------------------------
/*/{Protheus.doc} ProcCOP
Composicao Honorario
@author  PLSTEAM
@version P11
@since   17.01.2005
/*/  
//+------------------------------------------------------------------        
Static Function ProcCOP(cCodigo,cTpPart)
LOCAL nPos := If(Ascan(aUnidades[2],cCodigo)>0,2,0)

If nPos > 0
   If cTpPart $ "0"
      nPos := 1 //.t. no return
   Else   
      nPos := Ascan(aTpPart[nPos],{ |x| x[1] == cTpPart .And. x[2] == cCodigo })
   Endif   
Endif   

Return(nPos>0)

//+------------------------------------------------------------------
/*/{Protheus.doc} ProcFIL
Composicao Honorario	
@author  PLSTEAM
@version P11
@since   17.01.2005
/*/  
//+------------------------------------------------------------------           
Static Function ProcFIL(cCodigo,cTpPart)
LOCAL nPos := If(Ascan(aUnidades[5],cCodigo)>0,5,0)

If nPos > 0
   If cTpPart $ "0"
      nPos := 1 //.t. no return
   Else   
      nPos := Ascan(aTpPart[nPos],{ |x| x[1] == cTpPart .And. x[2] == cCodigo })
   Endif   
Endif   

Return(nPos>0)

//+------------------------------------------------------------------
/*/{Protheus.doc} PLSEDIA700
Verifica se o arquivo jah foi importado
@author  PLSTEAM
@version P11
@since   17.01.2005
/*/  
//+------------------------------------------------------------------            
Static Function JaImportado(cArquivo)
LOCAL lFound     := .F.             
LOCAL aDirArq    := DIRECTORY(cArquivo)
LOCAL cArqBusca  := ""
If Len(aDirArq) > 0
    cArqBusca := aDirArq[1,1]
Endif    

BRJ->(DbSetOrder(1))
If BRJ->(DbSeek(xFilial("BRJ")))
   While ! BRJ->(Eof()) .And. BRJ->BRJ_FILIAL == xFilial("BRJ")  
         
         If UPPER(cArqBusca) $ UPPER(BRJ->BRJ_ARQUIVO)
            lFound := .T.
            Exit
         Endif
   
   BRJ->(DbSkip())
   Enddo
Endif
Return(lFound)

//+------------------------------------------------------------------
/*/{Protheus.doc} ProcPerImp
Verifica se pode ocorrer a importacao
@author  PLSTEAM
@version P11
@since   17.01.2005
/*/  
//+------------------------------------------------------------------             
Static Function ProcPerImp(cLayPLS,cArquivo,lImportar,lGerarLog,cCodLDP,cCodProGen,cAMPAG,lConverProc,cCodPadCon,;
			   			   nRegraGrvImp)

LOCAL lRet 		:= .T.
LOCAL aRet 		:= {}
LOCAL aUnidPad  := {{"AUX",4},{"HM",1},{"PA",1},{"COP",1}} //[1]unidade [2]referencia
LOCAL aUnidUsr  := {}
LOCAL nI		:= 1
LOCAL cCodPro   := ""
LOCAL cCodTab   := ""
LOCAL cCdPaDp   := ""
			
If JaImportado(cArquivo)
   CarregaCrit()
   aadd(aRet,{aCriticas[48,1],aCriticas[48,2]})                           
   lRet := .F.
Endif   
             
If lConverProc
   BR4->(DbSetOrder(1))
   If ! BR4->(DbSeek(xFilial("BR4")+cCodPadCon))
	  aadd(aRet,{aCriticas[46,1],aCriticas[46,2]})
      lRet := .F.
   Endif
Endif      
                                            
BR8->(DbSetOrder(3))
If !BR8->(MsSeek(xFilial("BR8")+cCodProGen)) .or. Empty(cCodProGen)
   CarregaCrit()
   aadd(aRet,{aCriticas[47,1],aCriticas[47,2]})
   lRet := .F.
Else
	BA8->(DbSetOrder(4))
	If BA8->(MsSeek(xFilial("BA8")+cCodProGen))  
		cCodPro := BA8->BA8_CODPRO // esse aqui eh o codigo do proc generico
		cCodTab := BA8->BA8_CODTAB 
		cCdPaDp := BA8->BA8_CDPADP
		BD4->(DbSetOrder(2))                                         
		If BD4->(MsSeek(xFilial("BD4")+cCodPro+cCodTab))
			While BD4->(!Eof()) .and. BD4->BD4_CODPRO == cCodPro .and. BD4->BD4_CODTAB == cCodTab
		       	aadd(aUnidUsr,{alltrim(BD4->BD4_CODIGO),BD4->BD4_VALREF})
		    	BD4->(DbSkip())
			Enddo                                 
			For nI:=1 to len(aUnidPad) //todo proc. generico tem q ter essas unidades, se nao tiver eu crio
			    If aScan(aUnidUsr,{|x| x[1] == aUnidPad[nI][1] .and. x[2] == aUnidPad[nI][2]}) == 0
			    	BD4->(Reclock("BD4",.T.))
			    		BD4->BD4_CODPRO := cCodPro
						BD4->BD4_CODTAB := cCodTab
						BD4->BD4_CDPADP := cCdPaDp
						BD4->BD4_CODIGO := aUnidPad[nI][1]
						BD4->BD4_VALREF := aUnidPad[nI][2]
			    	BD4->(MsUnlock())
			    Endif
			Next
		Else
			For nI:=1 to len(aUnidPad) //todo proc. generico tem q ter essas unidades, se nao tiver eu crio
					BD4->(Reclock("BD4",.T.))
				 		BD4->BD4_CODPRO := cCodPro
						BD4->BD4_CODTAB := cCodTab
						BD4->BD4_CDPADP := cCdPaDp
						BD4->BD4_CODIGO := aUnidPad[nI][1]
						BD4->BD4_VALREF := aUnidPad[nI][2]
				   	BD4->(MsUnlock())
			Next
		Endif   
	Endif
Endif                 
       
If Len(aRet) <> 0	
	PLSCRIGEN(aRet,{ {"Codigo","@C",040},{"Descricao","@C",080} }, "Falhas que impossibilitam a importacao.",NIL,NIL,NIL,NIL, NIL,NIL,"G",220)
Endif
   
Return lRet             
        
//+------------------------------------------------------------------
/*/{Protheus.doc} PlRetCidGe
Retorna um codigo de cid Generico  
@author  PLSTEAM
@version P11
@since   17.01.2005
/*/
//+------------------------------------------------------------------        
static function PlRetCidGe()                  
LOCAL cCodCid := GetNewPar("MV_PLCDGEN","00000000")         
BA9->(DbSetOrder(1))
If !BA9->(MsSeek(xFilial("BA9")+cCodCid))
	BA9->(RecLock("BA9",.T.))
	BA9->BA9_CODDOE := cCodCid
	BA9->BA9_DOENCA := "CID GENERICO PARA USO NO PTU 700 IMPORTACAO"
	BA9->BA9_ABREVI := "CID GENERICO"
	BA9->(MsUnlock())
endif
return cCodCid

//+------------------------------------------------------------------
/*/{Protheus.doc} AltCodUsr
Posiciona usuario conforme data de atendimento
@author  Thiago Machado Correa
@version P11
@since   22/05/06
/*/
//+------------------------------------------------------------------
Static Function AltCodUsr(dDatAte)

Local lRet := .F.
Local lSai := .F.
Local lErr := .F.
Local aAreaBA1 := BA1->(GetArea())

BA1->(DbSetOrder(2))

//Posiciona no ultimo codigo do usuario
While ! Empty(BA1->BA1_TRADES)
	If BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG+BA1_DIGITO) <> BA1->BA1_TRADES
		BA1->(MsSeek(xFilial("BA1")+BA1->BA1_TRADES))
	Else 
		lErr := .T.
		Exit
	Endif
EndDo

If ! lErr .and. ! BA1->(Eof())

	While ! lSai
	
		//Testa Data
		If dDatAte >= BA1->BA1_DATINC
			lRet := .T.
			Exit
		Endif
	
		//Reposiciona Usuario
		If Empty(BA1->BA1_TRAORI)
			lSai := .T.
		Else
			BA1->(DbSetOrder(2))
			If ! BA1->(MsSeek(xFilial("BA1")+BA1->BA1_TRAORI))
				lSai := .T.
			Endif
		Endif
	
	EndDo

Endif
	
If ! lRet
	RestArea(aAreaBA1)
Endif

Return lRet

//+------------------------------------------------------------------
/*/{Protheus.doc} RetGuia
Retorna o codigo completo de uma guia.
@author  PLSTEAM
@version P11
@since   17/07/06
/*/
//+------------------------------------------------------------------
Static Function RetGuia(cAlias)
LOCAL cRet := ""

If cAlias $ "BD5/BE4"
   cRet := &(cAlias+"->"+cAlias+"_CODOPE")+"."+&(cAlias+"->"+cAlias+"_CODLDP")+"."+;
                      &(cAlias+"->"+cAlias+"_CODPEG")+"."+&(cAlias+"->"+cAlias+"_NUMERO")
Endif

Return(cRet)

//+------------------------------------------------------------------
/*/{Protheus.doc} ImpCriT
Imprime uma critica
@author  PLSTEAM
@version P11
@since   17.01.2005
/*/
//+------------------------------------------------------------------
Static Function ImpCriT(aCritRea,aCab01,aResum,aCab02,aCritNot,aCab03,aNota,aCab04,cTitulo,cTpRel,nTmRel)
LOCAL aPrints := {}	 
LOCAL nI := 0

If Len(aCritRea) > 0
	aadd(aPrints,{"criticas do arquivo",aCritRea,aCab01})
Endif
If Len(aResum) > 0
	aadd(aPrints,{"guias geradas",aResum,aCab02})
Endif
If Len(aCritNot) > 0
	aadd(aPrints,{"criticas das guias",aCritNot,aCab03})
Endif
If Len(aNota) > 0
	aadd(aPrints,{"outras criticas",aNota,aCab04})
Endif

For nI:=1 To Len(aPrints)        
	If MsgYesNo("Imprimir "+aPrints[nI][1]+" ?")
		RImpCriT(aPrints[nI][2],aPrints[nI][3],aPrints[nI][1],cTpRel,nTmRel)
	Endif
Next

Return                                                     

//+------------------------------------------------------------------
/*/{Protheus.doc} RImpCriT
Imprime uma critica	
@author  PLSTEAM
@version P11
@since   17/07/06
/*/
//+------------------------------------------------------------------
Static Function RImpCriT(aDados,aCabec,cTit,cTpRel,nTmRel)
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Define variaveis padroes para todos os relatorios...                     �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
LOCAL nFor
LOCAL nFor2             
LOCAL cDado
LOCAL uDado
LOCAL cPerg       := nil // Pergunta padrao (SX1) dos parametros
PRIVATE nQtdLin     := 58       // Qtd de Linhas Por Pagina
PRIVATE nLimite     := 132       // Limite de Colunas
PRIVATE cTamanho    := "M"       // P=Pequeno;M=Medio;G=Grande -> P=80;M=132;G=220 (colunas)
PRIVATE cTitulo     := cTit // Titulo do Relatorio
PRIVATE cDesc1      := cTitulo // Descritivo para o usuario
PRIVATE cDesc2      := "" // Descritivo para o usuario
PRIVATE cDesc3      := ""
PRIVATE cAlias      := "BA1" // Alias
PRIVATE cRel        := "IMPCRIGEN" // Nome do Relatorio
PRIVATE nli         := 01   // Variavel padrao para controlar numero de linha
PRIVATE nQtdini     := nli  // Variavel para controlar numero de linha inicial
PRIVATE m_pag       := 1    // Variavel padrao para contar numero da pagina
PRIVATE lCompres    := .F. // nao mude e padrao
PRIVATE lDicion     := .F. // nao mude e padrao
PRIVATE lFiltro     := .F. // Habilitar o filtro ou nao
PRIVATE lCrystal    := .F. // nao mudar controle do crystal reports
PRIVATE aOrderns    := {}
PRIVATE aReturn     := { "", 1,"", 1, 1, 1, "",1 } // padrao nao mude
PRIVATE lAbortPrint := .F. // Controle para abortar (sempre como esta aqui)
PRIVATE cCabec1     := "" // Primeira linha do cabecalho ;
PRIVATE cCabec2     := "" // utilizado pela funcao cabec...
PRIVATE nColuna     := 03 // Numero da coluna que sera impresso as colunas

DEFAULT cTpRel     := "M"
DEFAULT nTmRel     := 132

nLimite     := nTmRel
cTamanho    := cTpRel
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Chama SetPrint (padrao)                                                  �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
cRel  := SetPrint(cAlias,cRel,cPerg,@cTitulo,cDesc1,cDesc2,cDesc3,lDicion,aOrderns,lCompres,cTamanho,{},lFiltro,lCrystal)
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Verifica se foi cancelada a operacao (padrao)                                    �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
If nLastKey  == 27 // Verifica o cancelamento... 
   Return
Endif
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Configura impressora (padrao)                                            �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
SetDefault(aReturn,cAlias) 

@ ++nLi, nColuna pSay "**** "+cTit+" ****"

@ ++nLi, nColuna pSay Replicate("*",nLimite-nColuna)
cDado := ""
For nFor := 1 To Len(aCabec)
    cDado += aCabec[nFor,1]+Space(10)
Next
@ ++nLi, nColuna pSay cDado
@ ++nLi, nColuna pSay Replicate("*",nLimite-nColuna)

For nFor := 1 To Len(aDados)          
    cDado := ""
    For nFor2 := 1 To Len(aCabec)
        uDado := aDados[nFor,nFor2]
        If     ValType(uDado) == "C"
               cDado += uDado+Space(02)
        ElseIf ValType(uDado) == "D"                
               cDado += dtoc(uDado)+Space(02)
        ElseIf ValType(uDado) == "N"                      
               cDado += str(uDado,17,4)+Space(02)
        Endif       
    Next    
    @ ++nLi, nColuna pSay cDado
Next

@ ++nLi, nColuna pSay Replicate("*",nLimite-nColuna)
@ ++nLi, nColuna pSay StrZero(Len(aDados),2)+" Registro(s) Listado(s)"
@ ++nLi, nColuna pSay Replicate("*",nLimite-nColuna)

If  aReturn[5] == 1 
    Set Printer To
    Ourspool(cRel)
End
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Fim da rotina                                                            �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Return

return
