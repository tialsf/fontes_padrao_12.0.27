#INCLUDE "protheus.ch"
#include "FILEIO.CH

#DEFINE CRLF Chr(10) + Chr(13)
STATIC cCodInt := plsintpad()
//-----------------------------------------------
/*/{Protheus.doc} PLSP900 
Importa��o PTU A900
@author  Lucas Nonato 
@version P12 
@since   27/01/2020
/*/ 
function PLSP900()

local aPergs	:= {}
local n902		:= 0
local n905		:= 0
local n902OP	:= 0
local lRet		:= .t.
private aPBox	:= {}
private oProcess

aAdd(/*01*/aPergs,{ 6,"Arquivo",space(200),"","","",85,.T.,"PTU A900 |*.*"})
aAdd(/*02*/aPergs,{ 2,"Criar TDE","1",{ "0=N�o","1=Sim" },60,/*'.T.'*/,.t. } )
aAdd(/*03*/aPergs,{ 1,"Tab Padrao Mat "	,space(2), "@!", 'Vazio() .or. ExistCpo("BR4",mv_par03,1)', 'B41PLS', "mv_par02=='1'", 10, .f. } )
aAdd(/*04*/aPergs,{ 1,"Tab Padrao Med"	,space(2), "@!", 'Vazio() .or. ExistCpo("BR4",mv_par04,1)', 'B41PLS', "mv_par02=='1'", 10, .f. } )
aAdd(/*05*/aPergs,{ 1,"Tab Padrao OPME"	,space(2), "@!", 'Vazio() .or. ExistCpo("BR4",mv_par05,1)', 'B41PLS', "mv_par02=='1'", 10, .f. } )
aAdd(/*06*/aPergs,{ 1,"TDE Materiais"	,space(7), "@!", 'Vazio() .or. ExistCpo("BF8",mv_par06,1)', 'B68PLS', "mv_par02<>'1'", 15, .f. } )
aAdd(/*07*/aPergs,{ 1,"TDE Medicamento"	,space(7), "@!", 'Vazio() .or. ExistCpo("BF8",mv_par07,1)', 'B68PLS', "mv_par02<>'1'", 20, .f. } )
aAdd(/*08*/aPergs,{ 1,"TDE OPME"		,space(7), "@!", 'Vazio() .or. ExistCpo("BF8",mv_par08,1)', 'B68PLS', "mv_par02<>'1'", 20, .f. } ) 

if( paramBox( aPergs,"Importa��o Tabela Nacional de Materiais e Medicamentos PTU A900",aPBox,/*bOK*/,/*aButtons*/,.f.,/*nPosX*/,/*nPosY*/,/*oDlgWizard*/,/*cLoad*/'PLSP900',/*lCanSave*/.t.,/*lUserSave*/.t. ) )
	if validPerg()	
		cIni := time()
		oProcess := MsNewProcess():New( { || PLSP900PRO(.f.,@n902,@n905,@lRet,@n902OP) } , "Processando" , "Aguarde..." , .f. )
		oProcess:Activate()
		cFim := time()
		if lRet
			aviso( "Resumo","Processamento finalizado. " + CRLF + ;
			"Materiais Processados: " 		+ cvaltochar(n902) + CRLF + ;
			"Medicamentos Processados: " 	+ cvaltochar(n905) + CRLF + ;
			"OPME Processados: " 	+ cvaltochar(n902OP) + CRLF + ;
			'Inicio: ' + cvaltochar( cIni ) + "  -  " + 'Fim: ' + cvaltochar( cFim ) + CRLF + "TOTAL: " + ElapTime( cIni, cFim ) ,{ "Ok" }, 2 )
		endif
		PLSP900()	
	else
		PLSP900()
	endif

endif

return 

//-----------------------------------------------
/*/{Protheus.doc} PLSP900PRO 
Processamento do arquivo PTU A900
@author  Lucas Nonato 
@version P12 
@since   27/01/2020
/*/ 
function PLSP900PRO(lAuto,n902,n905,lRet,n902OP)
local oFile 		as object
local cLinAtu 		as char
local nTot			:= 0
local nX			:= 0
local cArq			:= aPBox[1]
local aBF8			:= {}
local nSize			:= 0
local nPosicTab		:= 0
local nPosicPad		:= 0
local xFiliBA8		:= xFilial("BA8")
local xFiliBD4		:= xFilial("BD4")
local xFiliBR8		:= xFilial("BR8")
local lExsDETANV	:= BA8->(fieldpos("BA8_DETANV")) > 0
local lExsTUSEDI	:= BR8->(fieldpos("BR8_TUSEDI")) > 0
local lExsCODIFI	:= BA8->(fieldpos("BA8_CODIFI")) > 0
local cTimDatInt	:= dtoc(date()) + " " + time()

//901
local cTpCarga   	as char
local cNrVerTra 	as char

//902
local cCdMaterial	as char
local nVlMax		as numeric
local dDtInicio		as date
local dDtFim		as date
local cRegAnvisa	as char
local cDetAnvisa	as char
local cMotivo		as char
local cCnpj			as char
local cTpProduto 	as char
local cTpCodific	as char
local cDescr		as char
local cDsProd 		as char
local cDsEsp 		as char
local cDsClas		as char

//905
local cCdMedic		as char
local cTpMed		as char	
local cConfaz 		as char
local cDescAtiv		as char
local cDescGFarm	as char
local cDescCFarm	as char

//906
local cAreaB6I 	:= RetSqlName("B6I")
local cFiliB6I 	:= xFilial("B6I")	
local nTamICMS	:= TamSX3("B6I_ICMS")[1]
local nTamPROC	:= TamSX3("B6I_CODPRO")[1]
local cFiliBTQ 	:= xFilial("BTQ")
local cAreaBTQ 	:= RetSqlName("BTQ")
local cDatPesq	:= dtos(dDataBase)
local oHashUnd	:= HMNew()

default lAuto 	:= .f.
default n902 	:= 0
default n905 	:= 0
default n902OP	:= 0
default lRet  	:= .f.

BF8->(dbSetOrder(1))
BA8->(dbSetOrder(1))
BD4->(dbSetOrder(1))                                                                                    
BR7->(dbSetOrder(1))
BR8->(dbSetOrder(1))
B6I->(dbsetorder(1))
cDescr   := ""

oFile := FWFileReader():New( cArq )
oFile:setBufferSize(131072) //128Kb buffer
if oFile:Open()
	if !lAuto
		oProcess:SetRegua1(1)
		oProcess:IncRegua1("Processando arquivo")
		oProcess:SetRegua2(-1)
	endif
	nSize := oFile:getFileSize()
    while /*!(oFile:eof()) .and.*/ (oFile:hasLine())
		cLinAtu := oFile:getLine()
		nTot++
    	if nTot == 1
			if substr(cLinAtu,9,3) <> "901"
				if !lAuto
					msgAlert("O Arquivo A900 informado � invalido. " + CRLF + cArq)
				endif
				lRet := .f.
				exit
			endif
		endif	
		if !lAuto
			oProcess:IncRegua2(cvaltochar(oFile:getBytesRead()) +" de "+ cvaltochar(nSize))
		endif
		if substr(cLinAtu,9,3) == "901"
			cTpCarga  	:= substr(cLinAtu,24,1)
			cNrVerTra	:= substr(cLinAtu,25,2)
			if aPBox[2] == "1"
				aadd(aBF8, gravaBF8("TABELA NACIONAL DE MATERIAIS UNIMED"	,aPBox[3],"19","1"))
				aadd(aBF8, gravaBF8("TABELA NACIONAL DE MEDICAMENTOS UNIMED",aPBox[4],"20","2"))
				aadd(aBF8, gravaBF8("TABELA NACIONAL DE OPME UNIMED"		,aPBox[5],"19","1"))
			else
				aadd(aBF8, aPBox[6])
				aadd(aBF8, aPBox[7])
				aadd(aBF8, aPBox[8])
			endif
		endif
		if substr(cLinAtu,9,3) == "902"		
			cTpProduto 		:= substr(cLinAtu,365,1) 	// 1 � Material de consumo hospitalar 2 � �rteses 3 � Pr�teses 4 � Material Especial		    
			nPosicPad 		:= iif(cTpProduto == "5", 5, 3 )
			nPosicTab 		:= iif(cTpProduto == "5", 3, 1 )
			iif(cTpProduto == "5", n902OP++, n902++ )
			//
			cCdMaterial		:= substr(cLinAtu,12,8)
			nVlMax			:= val(substr(cLinAtu,229,11)+"."+substr(cLinAtu,240,4))
			dDtInicio		:= stod(iif(!empty(substr(cLinAtu,367,8)),substr(cLinAtu,367,8), dtos(dDataBase)))
			dDtFim			:= stod(iif(!empty(substr(cLinAtu,375,8)),substr(cLinAtu,375,8), ""))
			cMotivo			:= substr(cLinAtu,174,40)
			cCnpj			:= substr(cLinAtu,30,14)
			cDetAnvisa		:= substr(cLinAtu,44,50)
		    cRegAnvisa  	:= substr(cLinAtu,290,15)		    
		    cTpCodific		:= substr(cLinAtu,366,1) 	// 1 � TNUMM 2 � TUSS
			cTpProduto		:= iif(cTpProduto=='5','5','1') // BR8_TPPROC 1=Materiais, 5=OPME
			cCodUnd			:= RetUniMedida(Alltrim(substr(cLinAtu,20,10)), cFiliBTQ, cAreaBTQ, cDatPesq, @oHashUnd)

			for nX:=1 to 7
				cLinAtu := oFile:getLine()
				nTot++
				do case 
				case nX == 1
					cDescr	:= alltrim(cLinAtu)
				case nX == 2
					cDsProd := alltrim(cLinAtu)
				case nX == 3
					cDsEsp 	:= alltrim(cLinAtu)
				case nX == 4
					cDsClas	:= alltrim(cLinAtu)
				endcase
				//case nX == 5
				//case nX == 6
				//case nX == 7
			next
			gravaProc(aBF8[nPosicTab], aPBox[nPosicPad], cCdMaterial,nVlMax,cTpCarga,dDtInicio,dDtFim,cRegAnvisa,cDetAnvisa,cMotivo,cCnpj,cTpProduto,cTpCodific,cDescr,;
					  cDsProd,cDsEsp,cDsClas,"VMT",,,,,,cCodUnd,xFiliBA8,xFiliBD4,xFiliBR8,lExsDETANV,lExsTUSEDI,cTimDatInt,lExsCODIFI)

		endif

		if substr(cLinAtu,9,3) == "905"
			n905++
			
			cCdMedic	:= substr(cLinAtu,12,8)					
			dDtInicio	:= stod(iif(!empty(substr(cLinAtu,160,8)),substr(cLinAtu,160,8), dtos(dDataBase)))
			dDtFim		:= stod(iif(!empty(substr(cLinAtu,168,8)),substr(cLinAtu,168,8), ""))
			cRegAnvisa	:= substr(cLinAtu,144,15)			
			cMotivo		:= substr(cLinAtu,73,40)
			cCnpj		:= substr(cLinAtu,58,14)
			cTpMed		:= substr(cLinAtu,187,1)
			cConfaz 	:= iif(substr(cLinAtu,188,1) == "S", "1", "0")
			cTpCodific	:= substr(cLinAtu,159,1)
			cCodUnd		:= RetUniMedida(Alltrim(substr(cLinAtu,20,10)), cFiliBTQ, cAreaBTQ, cDatPesq, @oHashUnd)

			for nX:=1 to 7
				cLinAtu := oFile:getLine()
				nTot++
				do case 
				case nX == 1
					cDescAtiv	:= alltrim(cLinAtu)
				case nX == 2
					cDescr 		:= alltrim(cLinAtu)
				case nX == 3
					cDescGFarm 	:= alltrim(cLinAtu)
				case nX == 4
					cDescCFarm	:= alltrim(cLinAtu)
				case nX == 6
					cDetAnvisa	:= alltrim(cLinAtu)
				case nX == 7
					nVlMax := val(substr(cLinAtu,17,11)+"."+substr(cLinAtu,28,4))
				endcase
			next
			gravaProc(aBF8[2],aPBox[4],cCdMedic,nVlMax,cTpCarga,dDtInicio,dDtFim,cRegAnvisa,cDetAnvisa,cMotivo,cCnpj,"2",cTpCodific,cDescr,,,,"VMD",;
					  cTpMed,cConfaz,cDescAtiv,cDescGFarm,cDescCFarm,cCodUnd,xFiliBA8,xFiliBD4,xFiliBR8,lExsDETANV,lExsTUSEDI,cTimDatInt,lExsCODIFI)
		endif

		//Tabela de ICMS - B6I
		if substr(cLinAtu,9,3) == "906"
			nValICMS	:= val(substr(cLinAtu,12,3) + "." + substr(cLinAtu,15,2))
			cALivCom	:= iif( substr(cLinAtu,34,1) == "S", "1", "0")
			nPrecTx		:= val(substr(cLinAtu,17,11) + "." + substr(cLinAtu,28,4))
			grvB6IProc(aPBox[4], cCdMedic, dDtInicio, dDtFim, nValICMS, cALivCom, nPrecTx, cAreaB6I, cFiliB6I, nTamPROC, nTamICMS, cDatPesq)
		endif

	enddo
    oFile:Close()
else
	if !lAuto
		msgAlert("Falha na abertura do arquivo: fError[" +cvaltochar(ferror())+"]")
	endif
endif
HMClean(oHashUnd)
return lRet

//-----------------------------------------------
/*/{Protheus.doc} validPerg 
Valida perguntas
@author  Lucas Nonato 
@version P12 
@since   27/01/2020
/*/ 
static function validPerg
local lRet as logical

BF8->(dbSetOrder(1))
if aPBox[2] == "1"
	if !empty(aPBox[3]) .and. !empty(aPBox[4]) .and. !empty(aPBox[5])
		lRet := .t.
	else
		lRet := .f.
		MsgAlert("Para criar uma nova TDE � necess�rio informar os c�digos da Tabela Padr�o!")
	endif
else
	if !empty(aPBox[6]) .and. !empty(aPBox[7]) .and. !empty(aPBox[8])
		lRet := .t.
		aPBox[3]	:= getBF8(aPBox[6])
		aPBox[4]	:= getBF8(aPBox[7])
		aPBox[5]	:= getBF8(aPBox[8])
		if empty(aPBox[3]) .or. empty(aPBox[4]) .or. empty(aPBox[5])				
			lRet := .f.
			MsgAlert("TDE informada n�o possui c�digo de tabela padr�o vinculado!")
		endif
	else
		lRet := .f.
		MsgAlert("Para atualizar uma TDE � necess�rio informar os c�digos das TDEs existes!")
	endif
endif

return lRet

//-----------------------------------------------
/*/{Protheus.doc} gravaBF8 
Grava BF8
@author  Lucas Nonato 
@version P12 
@since   27/01/2020
/*/ 
static function gravaBF8(cDesc, cCodPad, cTabTISS, cTpProc)
local cCodigo as array

cCodigo := PLBF8VLC(cCodInt)
BF8->(RecLock("BF8",.T.))
BF8->BF8_FILIAL := xFilial("BF8")
BF8->BF8_CODINT := cCodInt
BF8->BF8_CODIGO := cCodigo
BF8->BF8_DESCM 	:= cDesc
BF8->BF8_CODPAD := cCodPad
BF8->BF8_ESPTPD := "1"
BF8->BF8_TPPROC := cTpProc
BF8->BF8_TABTIS := cTabTISS 
BF8->(MsUnLock())

return cCodInt + BF8->BF8_CODIGO

//-----------------------------------------------
/*/{Protheus.doc} gravaProc 
Grava os procedimentos
@author  Lucas Nonato 
@version P12 
@since   27/01/2020
/*/ 
static function gravaProc(cCodTab,cCodPad,cCdProc,nVlMax,cTpCarga,dDtInicio,dDtFim,cRegAnvisa,cDetAnvisa,cMotivo,cCnpj,cTpProduto,cTpCodific,cDescr,cDsProd,cDsEsp,;
						  cDsClas,cUnidade,cTpMed,cConfaz,cDescAtiv,cDescGFarm,cDescCFarm,cUniMedida,xFiliBA8,xFiliBD4,xFiliBR8,lExsDETANV,lExsTUSEDI,cTimDatInt,lExsCODIFI)
local cAtivo := "1"

default cDsProd		:= ""
default cDsEsp		:= ""
default cDsClas		:= ""
default cTpMed		:= ""
default cConfaz		:= ""
default cDescAtiv	:= ""
default cDescGFarm	:= ""
default cDescCFarm	:= ""
default cUniMedida	:= "000"
default lExsDETANV	:= .f.
default lExsTUSEDI	:= .f.
default lExsCODIFI	:= .f.

if cTpCarga == "3"
	cAtivo := "0"
endif

if !BA8->(MsSeek(xFiliBA8+cCodTab+cCodPad+cCdProc))
	BA8->(RecLock("BA8",.T.))
	BA8->BA8_FILIAL := xFiliBA8
	BA8->BA8_CDPADP := cCodPad
	BA8->BA8_CODPRO := cCdProc
	BA8->BA8_DESCRI := cDescr
	BA8->BA8_NIVEL  := "3"
	BA8->BA8_ANASIN := "1"
	BA8->BA8_CODPAD := cCodPad
	BA8->BA8_CODTAB := cCodTab
	BA8->BA8_RGANVI := cRegAnvisa
	if lExsDETANV
		BA8->BA8_DETANV := cDetAnvisa
	endif
	if lExsCODIFI
		BA8->BA8_CODIFI := cTpCodific
	endif
	BA8->BA8_DSAINA := cMotivo
	BA8->BA8_CNPJ	:= cCnpj
	BA8->BA8_NMFABR := ""
	BA8->BA8_DSPROD	:= cDsProd
    BA8->BA8_DESESP	:= cDsEsp 
	BA8->BA8_DSCLAS	:= cDsClas
	BA8->BA8_SITUAC	:= '1'
	BA8->BA8_ORIGEM	:= '1'
	BA8->BA8_TPPROD := cTpMed 
	BA8->BA8_CONFAZ := cConfaz
	BA8->BA8_DPRINC	:= cDescAtiv    
	BA8->BA8_DGRUFA	:= cDescGFarm
	BA8->BA8_DCFARM	:= cDescCFarm
	BA8->BA8_UNMEDI	:= cUniMedida
	BA8->(MsUnLock()) 	                 
endif
//BD4_FILIAL+BD4_CODTAB+BD4_CDPADP+BD4_CODPRO+BD4_CODIGO+DTOS(BD4_VIGINI)                                                                                         
If BD4->(MsSeek(xFiliBD4+BA8->BA8_CODTAB+BA8->BA8_CDPADP+BA8->BA8_CODPRO))
	BD4->(Reclock("BD4",.F.))
	if BD4->BD4_VIGINI <> dDtInicio
		if empty(BD4->BD4_VIGFIM) .or. BD4->BD4_VIGFIM >= dDtInicio		
			BD4->BD4_VIGFIM := dDtInicio
		endif
	endif
	BD4->(MsUnlock())
endif
cCodPro := BA8->BA8_CODPRO 
cCodTab := BA8->BA8_CODTAB
cCdPaDp := BA8->BA8_CDPADP

If !BD4->(MsSeek(xFiliBD4+BA8->BA8_CODTAB+BA8->BA8_CDPADP+BA8->BA8_CODPRO+cUnidade+dtos(dDtInicio)))		
	BD4->(Reclock("BD4",.T.))
	BD4->BD4_FILIAL := xFiliBD4
	BD4->BD4_CODPRO := BA8->BA8_CODPRO
	BD4->BD4_CODTAB := BA8->BA8_CODTAB
	BD4->BD4_CDPADP := BA8->BA8_CDPADP
	BD4->BD4_CODIGO := cUnidade
	BD4->BD4_VALREF := nVlMax
	BD4->BD4_VIGINI	:= iif(Empty(dDtInicio),dDataBase,dDtInicio)
	BD4->BD4_VIGFIM	:= iif(Empty(dDtFim),Stod(""),dDtFim)
	BD4->(MsUnlock())		
Endif

if !BR8->(msSeek(xFiliBR8+BA8->BA8_CDPADP+BA8->BA8_CODPRO))
	BR8->(RecLock("BR8",.T.))
    BR8->BR8_FILIAL := xFiliBR8
    BR8->BR8_AUTORI := "1"
	BR8->BR8_BENUTL := cAtivo
    BR8->BR8_TPPROC := cTpProduto
    if lExsTUSEDI
		BR8->BR8_TUSEDI := iif(cTpCodific=="2",iif(cTpProduto=="5","1",cTpProduto),"")
	endif
    BR8->BR8_AOINT 	:= "N"
    BR8->BR8_ACAO 	:= "3"
    BR8->BR8_DTINT 	:= cTimDatInt

    BR7->(dbGoTop())
    while !BR7->(eof())
        cDado  := &("BA8->"+AllTrim(BR7->BR7_FLDTDE))
        cCampo := "BR8->"+AllTrim(BR7->BR7_FLDPSA)
    	&cCampo := cDado               
    	BR7->(DbSkip())
    enddo
	
	BR8->(MsUnLock())
elseif BR8->BR8_BENUTL == "1" .and. cAtivo == "0"
	BR8->(RecLock("BR8",.f.))
	BR8->BR8_BENUTL := cAtivo
	BR8->(MsUnLock())
endif

return

//-----------------------------------------------
/*/{Protheus.doc} PLSED900MV 
Compatibilidade com o menu antigo
@author  Lucas Nonato 
@version P12 
@since   27/01/2020
/*/ 
function PLSED900MV()
PLSP900()
return

//-----------------------------------------------
/*/{Protheus.doc} getBF8 
Retorna CODPAD da BF8.
@author  Lucas Nonato 
@version P12 
@since   27/01/2020
/*/ 
static function getBF8(cCodigo)
local cRet as char

if BF8->(msSeek(xfilial("BF8")+cCodigo))
	cRet := BF8->BF8_CODPAD
else
	cRet := ""
endif

return cRet


//-----------------------------------------------
/*/{Protheus.doc} grvB6IProc 
Grava B6I
@version P12 
@since   08/2020
/*/ 
static function grvB6IProc(cCodPad, cCdProc, dDtInicio, dDtFim, nValICMS, cALivCom, nPrecTx, cAreaB6I, cFiliB6I, nTamPROC, nTamICMS, cDatPesq)
local cSql		:= ""
local lGrvB6I	:= .f.

//Verificamos se existe algum registro aberto, para finalizar sua vig�ncia
cSql := " SELECT R_E_C_N_O_ REC FROM " + cAreaB6I
cSql += "   WHERE B6I_FILIAL = '" + cFiliB6I + "' "
cSql += "     AND B6I_CODPAD = '" + cCodPad + "' "
cSql += "     AND B6I_CODPRO = '" + cCdProc + "' "
cSql += " 	  AND B6I_ICMS   =  " + cvaltochar(nValICMS) + " "   
cSql += " 	  AND B6I_LIVCOM = '" + cALivCom + "' "
cSql += " 	  AND B6I_VALOR <>  " + cvaltochar(nPrecTx) + " "
cSql += " 	  AND (B6I_VIGFIM = ' ' OR B6I_VIGFIM > '" + cDatPesq + "' ) "
cSql += " 	  AND D_E_L_E_T_ = ' ' " 

dbUseArea(.T.,"TOPCONN",tcGenQry(,,csql),"TRBB6I",.F.,.T.)

if ( !TRBB6I->(eof()) )
	lGrvB6I := .t.
	dDtInicio := dDatabase

	while ( !TRBB6I->(eof()) )
		B6I->(dbgoto(TRBB6I->REC))
		B6I->(RecLock("B6I", .f.))	
			B6I->B6I_VIGFIM := dDtInicio
		B6I->(msUnlock())
		TRBB6I->(dbSkip())
	enddo
else
	lGrvB6I := .t.
endif
TRBB6I->(dbclosearea())

if lGrvB6I
	If !B6I->(MsSeek(cFiliB6I + cCodPad + padr(cCdProc, nTamPROC, "") + padr(cvaltochar(nValICMS), nTamICMS, "") + cALivCom + dtos(dDtInicio)))	
		B6I->(RecLock("B6I", .t.))
			B6I->B6I_FILIAL := cFiliB6I
			B6I->B6I_CODPAD := cCodPad 
			B6I->B6I_CODPRO := cCdProc
			B6I->B6I_ICMS   := nValICMS
			B6I->B6I_LIVCOM := cALivCom 
			B6I->B6I_VALOR	:= nPrecTx 
			B6I->B6I_VIGINI := iif( empty(dDtInicio), dDataBase, dDtInicio )
			B6I->B6I_VIGFIM := iif( empty(dDtFim), stod(""), dDtFim)
		B6I->(MsUnLock())
	endif
endif

return


//-----------------------------------------------
/*/{Protheus.doc} RetUniMedida 
Retorna a unidade  de Medida, conforme BTQ, tabela 60, e armazena em hash, para otimizar pesquisa
@version P12 
@since   08/2020
/*/ 
static function RetUniMedida(cCodUnd, cFiliBTQ, cAreaBTQ, cDatPesq, oHashUnd)
local aRetUnd	:= {}
local cSql 		:= ""
local cRet 		:= "000"

if HMGet( oHashUnd, cCodUnd, @aRetUnd ) .and. len(aRetUnd) > 0
	cRet := aRetUnd[1,2]
else
	cSql := "SELECT BTQ_CDTERM COD FROM " + cAreaBTQ
	cSql += "  WHERE BTQ_FILIAL = '" + cFiliBTQ + "' "
	cSql += "    AND BTQ_CODTAB = '60' "
	cSql += "    AND BTQ_DESTER = '" + cCodUnd + "' "
	cSql += "    AND BTQ_VIGDE <= '" + cDatPesq + "' AND ( BTQ_VIGATE >= '" + cDatPesq + "' OR BTQ_VIGATE = ' ' ) "
	cSql += "    AND D_E_L_E_T_ = ' ' "

	dbUseArea(.t., "TOPCONN", TCGENQRY(,,cSql), "UNDMED", .f., .t.)

	cRet := iif( !UNDMED->(eof()), alltrim(UNDMED->COD), "000" )

	UNDMED->(dbclosearea())
	HmAdd(oHashUnd, {cCodUnd, cRet})
endif

return cRet
