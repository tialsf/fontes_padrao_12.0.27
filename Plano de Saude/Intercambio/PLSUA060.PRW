
#include "PROTHEUS.CH"
#include "PLSMGER.CH"

#define K_Copiar 6 
/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    �PLSUA060 � Autor � Marco Paulo            � Data � 23.08.01 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Cadastro de Configuracao de layout de ptu                  ����
�������������������������������������������������������������������������Ĵ���
���Sintaxe   � PLSUA060()                                                 ����
�������������������������������������������������������������������������Ĵ���
��� Uso      � Advanced Protheus                                          ����
�������������������������������������������������������������������������Ĵ���
��� Alteracoes desde sua construcao inicial.                              ����
�������������������������������������������������������������������������Ĵ���
��� Data     � BOPS � Programador � Breve Descricao                       ����
�������������������������������������������������������������������������Ĵ���
��������������������������������������������������������������������������ٱ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/
Function PLSUA060
//��������������������������������������������������������������������������Ŀ
//� Define o cabecalho da tela de atualizacoes                               �
//����������������������������������������������������������������������������
PRIVATE aRotina := { { STRPL01 , 'AxPesqui' , 0 , K_Pesquisar  },; //'Pesquisar'
                      { STRPL02 , 'PLU60MOV' , 0 , K_Visualizar },; //'Visualizar'
                      { STRPL03 , 'PLU60MOV' , 0 , K_Incluir    },; //'Incluir'
                      { STRPL04 , 'PLU60MOV' , 0 , K_Alterar    },; //'Alterar'
                      { STRPL05 , 'PLU60MOV' , 0 , K_Excluir    },; //'Excluir'
                      { 'Copiar'  , 'PLU60MOV' , 0 , K_Copiar     } }
PRIVATE cCadastro := "Cadastro de configuracao de Layout PTU"
//��������������������������������������������������������������������������Ŀ
//� Endereca a funcao de BROWSE                                              �
//����������������������������������������������������������������������������
BU1->(DbSetOrder(1))
BU1->(DbGoTop())
BU1->(mBrowse(06,01,22,75,"BU1"))
//��������������������������������������������������������������������������Ŀ
//� Fim da Rotina Principal                                                  �
//����������������������������������������������������������������������������
Return
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � PL070MOV � Autor � Marco Paulo           � Data � 23.08.01 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Movimentacao do Cadastro de Doencas                        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function PLU60MOV(cAlias,nReg,nOpc)
Local I__f := 0
//��������������������������������������������������������������������������Ŀ
//� Define Variaveis...                                                      �
//����������������������������������������������������������������������������
LOCAL	aAC    := { "", "" } 
LOCAL nOpca	 := 0
LOCAL oDlg          
LOCAL nOpcAux
LOCAL nOpcGet

PRIVATE oEnchoice
PRIVATE oGetDados
PRIVATE aTELA[0][0]
PRIVATE aGETS[0]
PRIVATE aHeader
PRIVATE aCols
PRIVATE aVetTrab := {}
PRIVATE aChave   := {}                              
PRIVATE n        := 1
//��������������������������������������������������������������������������Ŀ
//� Monta aCols e aHeader...                                                 �
//����������������������������������������������������������������������������
Store Header "BU6" TO aHeader For SX3->X3_ORDEM > "02"
If nOpc == K_Incluir
	Copy "BU1" TO Memory Blank
	Store COLS Blank "BU6" TO aCols FROM aHeader
Else
	Copy "BU1" TO MEMORY

	BU6->(DbSetOrder(1))
	If BU6->(DbSeek(xFilial("BU6")+BU1->BU1_CODIGO))
		Store COLS "BU6" TO aCols FROM aHeader VETTRAB aVetTrab While BU6->BU6_CODLAY == BU1->BU1_CODIGO
	Else
		Store COLS Blank "BU6" TO aCols FROM aHeader
	EndIf
EndIf

if nOpc == K_Copiar
   M->BU1_CODIGO := GetSx8Num("BU1","BU1_CODIGO")                                 
   
Endif
//��������������������������������������������������������������������������Ŀ
//� Define Dialogo...                                                        �
//����������������������������������������������������������������������������
DEFINE MSDIALOG oDlg TITLE cCadastro FROM 008.2,010.3 TO 034.4,100.3 OF GetWndDefault()
//��������������������������������������������������������������������������Ŀ
//� Monta Enchoice...                                                        �
//����������������������������������������������������������������������������
If nOpc == K_Copiar
   nOpcAux := K_Alterar
Else
   nOpcAux := nOpc
Endif   
Zero();oEnchoice := MsMGet():New(cAlias,nReg,nOpcAux,,,,,{015,001,070,355},,,,,,oDlg,,,.F.)
//��������������������������������������������������������������������������Ŀ
//� Monta GetDados...                                                        �
//����������������������������������������������������������������������������
oGetDados := MsGetDados():New(072,001,196,355,nOpcAux,"AllwaysTrue","AllwaysTrue",,.T.,,,,,,,,,oDlg)
//��������������������������������������������������������������������������Ŀ
//� Ativa o Dialogo...                                                       �
//����������������������������������������������������������������������������
ACTIVATE MSDIALOG oDlg ON INIT Eval({ || EnchoiceBar(oDlg,{|| nOpca := 1,If(Obrigatorio(aGets,aTela).And.oGetDados:TudoOK(),oDlg:End(),nOpca:=2),If(nOpca==1,oDlg:End(),.F.) },{||oDlg:End()},.F.,{})  })
//��������������������������������������������������������������������������Ŀ
//� Rotina de gravacao dos dados...                                          �
//����������������������������������������������������������������������������
If nOpca == K_OK
   If nOpc <> K_Visualizar
      aChave := { {"BU6_CODLAY",M->BU1_CODIGO} }
      If nOpc <> K_Copiar
         PLUPTENC("BU1",nOpc)   	

   	     PLUPTCOLS("BU6",aCols,aHeader,aVetTrab,nOpc,aChave)
   	  Else
         PLUPTENC("BU1",K_Incluir)   	

   	     PLUPTCOLS("BU6",aCols,aHeader,aVetTrab,K_Incluir,aChave)
   	  Endif
   Endif	     
EndIf
//��������������������������������������������������������������������������Ŀ
//� Fim da Rotina...                                                         �
//����������������������������������������������������������������������������
Return

