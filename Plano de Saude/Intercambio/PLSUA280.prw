#include "PLSMGER.CH"
#include "PROTHEUS.CH"
#include "COLORS.CH"
#IFDEF TOP
       #include "TOPCONN.CH"
#ENDIF       

STATIC _nH
/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    � PLSUA280� Autor � Antonio de Padua       � Data � 14.05.02 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Cadastro de Layout Modelo OffLine                          ����
�������������������������������������������������������������������������Ĵ���
���Sintaxe   � PLSUA280()                                                 ����
�������������������������������������������������������������������������Ĵ���
��� Uso      � Advanced Protheus 5.08                                     ����
�������������������������������������������������������������������������Ĵ���
��� Alteracoes desde sua construcao inicial.                              ����
�������������������������������������������������������������������������Ĵ���
��� Data     � BOPS � Programador � Breve Descricao                       ����
�������������������������������������������������������������������������Ĵ���
��������������������������������������������������������������������������ٱ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/
Function PLSUA280
//��������������������������������������������������������������������������Ŀ
//� Define opcoes da rotina...                                               �
//����������������������������������������������������������������������������
PRIVATE aRotina  :=    { { STRPL01             , 'AxPesqui'   , 0 , K_Pesquisar  },; 
                          { STRPL02       , 'PLSUA280MOV' , 0 , K_Visualizar },;
       				      { STRPL03          , 'PLSUA280MOV' , 0 , K_Incluir    },;
   						  { STRPL04	       , 'PLSUA280MOV' , 0 , K_Alterar    },;
                          { STRPL05	       , 'PLSUA280MOV' , 0 , K_Excluir    }}
//��������������������������������������������������������������������������Ŀ
//� Define o cabecalho da tela de atualizacoes                               �
//����������������������������������������������������������������������������
PRIVATE cAlias    := "BUU"
PRIVATE cCadastro := PLSRetTit(cAlias)
//��������������������������������������������������������������������������Ŀ
//� Chama mBrowse padrao...                                                  �
//����������������������������������������������������������������������������
BUU->(DbSetOrder(1))
BUU->(DbSeek(xFilial("BUU")))
BUU->(mBrowse(06,01,22,75,'BUU',,,20))
//��������������������������������������������������������������������������Ŀ
//� Fim da Rotina Principal...                                               �
//����������������������������������������������������������������������������
Return
/*/
����������������������������������������������������������������������������
����������������������������������������������������������������������������
������������������������������������������������������������������������Ŀ��
���Funcao    �PLSUA280Mov � Autor � Antonio de Padua   � Data � 14.05.02 ���
������������������������������������������������������������������������Ĵ��
���Descricao � Modulo de Manutencao dos Layouts PTU                      ���
������������������������������������������������������������������������Ĵ��
���Sintaxe   � PLSUA280Mov(cAlias,nReg,nOpc)                             ���
������������������������������������������������������������������������Ĵ��
��� Uso      � PLSUA280                                                  ���
�������������������������������������������������������������������������ٱ�
����������������������������������������������������������������������������
����������������������������������������������������������������������������
/*/
Function PLSUA280Mov(cAlias,nReg,nOpc)
Local I__f := 0

LOCAL cTitulo
//���������������������������������������������������������������������Ŀ
//� Define variaveis da EnchoiceBar...                                  �
//�����������������������������������������������������������������������
LOCAL bOK     := {|| nOpca := 1,If( PLSUA280OK(nOpc,M->BUU_CODIGO), ;
                      oDlgFolder:End(),nOpca:=2),If(nOpca==1,oDlgFolder:End(),.F.) }
LOCAL bCancel := {||oDlgFolder:End()}
//���������������������������������������������������������������������Ŀ
//� Controle de lock de registro...                                     �
//����������������������������������������������-�������������������������
LOCAL lLock   := .F.
//���������������������������������������������������������������������Ŀ
//� Define variaveis genericas...                                       �
//�����������������������������������������������������������������������
PRIVATE nOpcx        := nOpc
PRIVATE nOpca        := 0
PRIVATE cChave       := Space(06)
PRIVATE nInd         := 0            
PRIVATE cCodLin      := Space(06)
PRIVATE bFilter                                

If nOpc <> K_Incluir
   cChave  := BUU->(BUU_CODIGO)
   cnChave := BUU->(BUU_CODIGO)+"1"
Endif
//���������������������������������������������������������������������Ŀ
//� Define variaveis da enchoice...                                     �
//�����������������������������������������������������������������������
PRIVATE cAliasEnc    := "BUU"
PRIVATE nRegEnc      := nReg
//���������������������������������������������������������������������Ŀ
//� Define folders...                                                   �
//�����������������������������������������������������������������������
PRIVATE oDlgFolder
PRIVATE oFolder
PRIVATE oFldLinhas
PRIVATE oFldCampos
//���������������������������������������������������������������������Ŀ
//� Define aCols e a aHeader...                                         �
//�����������������������������������������������������������������������
PRIVATE aCols        := {}
PRIVATE aHeader      := {}   

//���������������������������������������������������������������������Ŀ
//� Arquivos do Layout ...                                              �
//�����������������������������������������������������������������������
PRIVATE oBrwArq
PRIVATE aCabArq := {}
PRIVATE aDadArq := {}
PRIVATE aTrbArq := {}

//���������������������������������������������������������������������Ŀ
//� Arquivos dos Parametros...                                          �
//�����������������������������������������������������������������������
PRIVATE oBrwPar
PRIVATE aCabPar := {}
PRIVATE aDadPar := {}
PRIVATE aTrbPar := {}

//���������������������������������������������������������������������Ŀ
//� Linhas do Layout ...                                                �
//�����������������������������������������������������������������������
PRIVATE oBrwLin
PRIVATE aCabLin := {}
PRIVATE aDadLin := {}
PRIVATE aTrbLin := {}
//���������������������������������������������������������������������Ŀ
//� Campos das Linhas...                                                �
//�����������������������������������������������������������������������
PRIVATE oBrwCam
PRIVATE aCabCam := {}
PRIVATE aDadCam := {}
PRIVATE aTrbCam := {}
//���������������������������������������������������������������������Ŀ
//� Monta ,M->??? para enchoice...                                      �
//�����������������������������������������������������������������������
If nOpc == K_Incluir
   Copy cAliasEnc To Memory Blank
Else
   Copy cAliasEnc TO MEMORY
EndIf

n := 1

//���������������������������������������������������������������������Ŀ
//� Monta a GetDados do Folder Arquivos                                 �
//�����������������������������������������������������������������������
Store Header "BU7" TO aCabArq For .T.

If nOpcx == K_Incluir
   Store COLS Blank "BU7" TO aDadArq FROM aCabArq
Else
   BU7->(DbSetOrder(1))
   If ! BU7->(DbSeek(xFilial("BU7")+cnChave))
      Store COLS Blank "BU7" TO aDadArq FROM aCabArq
   Else
      Store COLS "BU7" TO aDadArq FROM aCabArq VETTRAB aTrbArq While xFilial("BU7")+cnChave == BU7->(BU7_FILIAL+BU7_CODLAY)+"1"
   Endif
Endif

//���������������������������������������������������������������������Ŀ
//� Monta a GetDados do Folder Parametros                               �
//�����������������������������������������������������������������������
Store Header "BU8" TO aCabPar For .T.

If nOpcx == K_Incluir
   Store COLS Blank "BU8" TO aDadPar FROM aCabPar
Else
   BU8->(DbSetOrder(1))
   If ! BU8->(DbSeek(xFilial("BU8")+cChave))
      Store COLS Blank "BU8" TO aDadPar FROM aCabPar
   Else
      Store COLS "BU8" TO aDadPar FROM aCabPar VETTRAB aTrbPar While xFilial("BU8")+cChave == BU8->(BU8_FILIAL+BU8_CODLAY)
   Endif
Endif


//���������������������������������������������������������������������Ŀ
//� Monta a GetDados do Folder Linhas                                   �
//�����������������������������������������������������������������������
Store Header "BUV" TO aCabLin For .T.

If nOpcx == K_Incluir
   Store COLS Blank "BUV" TO aDadLin FROM aCabLin
Else
   BUV->(DbSetOrder(1))
   If ! BUV->(DbSeek(xFilial("BUV")+cChave))
      Store COLS Blank "BUV" TO aDadLin FROM aCabLin
   Else
      Store COLS "BUV" TO aDadLin FROM aCabLin VETTRAB aTrbLin While xFilial("BUV")+cChave == BUV->(BUV_FILIAL+BUV_CODLAY)
   Endif
Endif

//���������������������������������������������������������������������Ŀ
//� Monta a GetDados do Folder Campos                                   �
//�����������������������������������������������������������������������
Store Header "BUX" TO aCabCam For .T.

If nOpcx == K_Incluir
   Store COLS Blank "BUX" TO aDadCam FROM aCabCam
Else
   BUX->(DbSetOrder(1))
   If BUX->(DbSeek(xFilial("BUX")+cChave))
      Store COLS "BUX" TO aDadCam FROM aCabCam VETTRAB aTrbCam While xFilial("BUX")+cChave == BUX->(BUX_FILIAL+BUX_CODLAY)
   Else
      Store COLS Blank "BUX" TO aDadCam FROM aCabCam
   Endif
Endif

//���������������������������������������������������������������������Ŀ
//� Define dialogo...                                                   �
//�����������������������������������������������������������������������
SetEnch("") 
DEFINE MSDIALOG oDlgFolder TITLE cCadastro FROM ndLinIni,ndColIni TO ndLinFin,ndColFin OF GetWndDefault()
//���������������������������������������������������������������������Ŀ
//� Define Folder...                                                    �
//�����������������������������������������������������������������������
 @ 060,003 FOLDER oFldLinhas SIZE 350,130 OF oDlgFolder              PIXEL	PROMPTS	"Arquivos","Parametros","Linhas","Campos"

oFldLinhas:bChange := { || UA280Chg() }

//���������������������������������������������������������������������Ŀ
//� Monta Enchoice dos Dados Gerais ...                                 �
//�����������������������������������������������������������������������
Zero(); MsMGet():New(cAliasEnc,nRegEnc,nOpc,,,,,{012,003,061,350},,,,,,oDlgFolder,,,.F.)

//���������������������������������������������������������������������Ŀ
//� Monta o Browse dos Parametros                                       �
//�����������������������������������������������������������������������
oBrwPar            := TPLSBrw():New(004,005,350,125,nil  ,oFldLinhas:aDialogs[2],nil    , nil      ,nil    ,nil  , nil, .T.   ,nil  ,.T.   ,nil   ,aCabPar ,aDadPar    ,.F.      ,"BU8" ,nOpcx,"Parametros",,nil   ,nil,aTrbPar,,)

//���������������������������������������������������������������������Ŀ
//� Monta o Browse das Linhas                                           �
//�����������������������������������������������������������������������
oBrwLin            := TPLSBrw():New(004,005,350,125,nil  ,oFldLinhas:aDialogs[3],nil    , nil      ,nil    ,nil  , nil, .T.   ,nil  ,.T.   ,nil   ,aCabLin ,aDadLin    ,.F.      ,"BUV" ,nOpcx,"Linhas",,nil   ,nil,aTrbLin,,)
oBrwLin:bLostFocus := {|| cCodLin := oBrwLin:aCols[oBrwLin:Linha(),oBrwLin:PlRetPos("BUV_CODLIN")]}
oBrwLin:bChange    := {|| Eval(oBrwLin:bLostFocus), ChangeBRW()} 
Eval(oBrwLin:bLostFocus)
//���������������������������������������������������������������������Ŀ
//� Monta o Browse dos campos 											�
//�����������������������������������������������������������������������
bFilter          := {|nLine,aCols,aHeader| aCols[nLine,GdFieldPos("BUX_CODLIN",aHeader)]==cCodLin }
oBrwCam          := TPLSBrw():New(004,005,350,125,nil  ,oFldLinhas:aDialogs[4],nil    , nil       ,nil    ,nil  , nil, .T.  ,nil   ,.T.   ,nil   ,aCabCam   ,aDadCam ,.F.      ,"BUX" ,nOpcx,"Campos",nil,nil,nil,aTrbCam,,,bFilter)
oBrwCam:oPai     := oBrwLin
oBrwCam:aOrigem  := {"BUV_CODLIN"}
oBrwCam:aRelac   := {"BUX_CODLIN"}

//���������������������������������������������������������������������Ŀ
//� Monta o Browse dos Arquivos...                                      �
//�����������������������������������������������������������������������
//aCols   := aClone(aDadArq)
//aHeader := aClone(aCabArq)
oBrwArq            := TPLSBrw():New(004,005,350,125,nil  ,oFldLinhas:aDialogs[1],nil    , nil      ,nil    ,nil  , nil, .T.   ,nil  ,.T.   ,nil   ,aCabArq,aDadArq,.F.      ,"BU7" ,nOpcx,"Arquivos",,nil   ,nil,aTrbArq,,)
//���������������������������������������������������������������������Ŀ
//� Ativa o dialogo...                                                  �
//�����������������������������������������������������������������������
ACTIVATE MSDIALOG oDlgFolder ON INIT EnchoiceBar(oDlgFolder,bOK,bCancel,.F.,{})
//���������������������������������������������������������������������Ŀ
//� Trata atualizacao dos dados...  
//�����������������������������������������������������������������������
If nOpca == K_OK

   If nOpcx <> K_Visualizar
      //���������������������������������������������������������������������Ŀ
      //� Inicio da Transacao...                                              �
      //����������������������������������������������������������������������� 
      Begin Transaction

      //���������������������������������������������������������������������Ŀ
      //� Layout...                                                           �
      //�����������������������������������������������������������������������
      PLUPTENC("BUU",nOpc)

      //���������������������������������������������������������������������Ŀ
      //� Arquivos...                                                         �
      //�����������������������������������������������������������������������
      aChave := {} 
      aadd(aChave,{"BU7_CODLAY",M->BUU_CODIGO})
      aadd(aChave,{"BU7_TIPOLT","1"})      
      oBrwArq:nOpcx := nOpc
      oBrwArq:Grava(aChave,.F.)

      aChave := {} 
      aadd(aChave,{"BU8_CODLAY",M->BUU_CODIGO})
      oBrwPar:Grava(aChave,.F.)
      
      //���������������������������������������������������������������������Ŀ
      //� Linhas...                                                           �
      //�����������������������������������������������������������������������
      aChave := {} 
      aadd(aChave,{"BUV_CODLAY",M->BUU_CODIGO})
      oBrwLin:Grava(aChave,.F.)

      //���������������������������������������������������������������������Ŀ
      //� Campos....                                                          �
      //�����������������������������������������������������������������������      
      aChave := {} 
      aadd(aChave,{"BUX_CODLAY",M->BUU_CODIGO})
      oBrwCam:Grava(aChave,.F.)      

      //���������������������������������������������������������������������Ŀ
      //� Libera Lock...                                                      �
      //�����������������������������������������������������������������������
      If lLock
         BUU->(MsUnLock())
      Endif 
        
      //���������������������������������������������������������������������Ŀ
      //� Fim do controle de semafaro iniciado em PLSUA280OK                   �
      //�����������������������������������������������������������������������
      If nOpcx == K_Incluir
         PLSFechaSem(_nH,"PLSUA280.SMF")
      Endif   
      //���������������������������������������������������������������������Ŀ
      //� Fim da Transacao...                                                 �
      //�����������������������������������������������������������������������
      End Transaction
   Endif   
Endif
//���������������������������������������������������������������������Ŀ
//� Fim da Rotina Principal...                                          �
//�����������������������������������������������������������������������
Return
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � PLSUA280OK � Autor � Antonio de Padua    � Data � 14.05.02 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Verifica processo de atualizacao...                        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function PLSUA280OK(nOpc,cChave)     
LOCAL lRet    := Eval( { || oBrwLin:TudoOK() })

If lRet
   If nOpc == K_Incluir
//      lRet := MsgYesNo("Confirma a inclusao do contrato/familia")
      
      _nH := PLSAbreSem("PLSUA280.SMF")
   Endif   
Endif                   

If lRet
   PlsEndBrw()
Endif  

Return(lRet)

/*/
�����������������������������������������������������������������������������     
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � A260CHG    � Autor � Tulio Cesar         � Data � 18.09.01 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Change do Folder...                                        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function UA280Chg()
If oFldLinhas:nOption == 4                                                   
   If Empty(oBrwLin:aCols[oBrwLin:Linha(),oBrwLin:PlRetPos("BUV_CODLIN")]) .And. !(oBrwLin:aCols[oBrwLin:Linha(),Len(oBrwLin:aHeader)+1])
      MsgInfo("Informe uma linha")
      oFldLinhas:SetOption(3)
      Return(.F.)
   Endif   
Endif

Return
                      

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � ChangeBrw  � Autor � Eduardo Motta       � Data � 16.10.01 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Atualiza as GETDADOS de acordo com a localidade            ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ChangeBrw()
oBrwCam:ForceRefresh(oBrwLin)
Return