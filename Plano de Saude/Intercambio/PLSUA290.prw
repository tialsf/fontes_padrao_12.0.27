
#include "PLSMGER.CH"
#include "PROTHEUS.CH"
#include "COLORS.CH"
#include "TOPCONN.CH"

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    � PLSUA290� Autor � Antonio de Padua Cunha � Data � 02.06.02 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Cria o arquivo de Offline para o POS                       ����
�������������������������������������������������������������������������Ĵ���
���Sintaxe   � PLSUA290                                                   ����
�������������������������������������������������������������������������Ĵ���
���Parametros�                                                            ����
��������������������������������������������������������������������������ٱ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/
Function PLSUA290()
Local cPerg := "PLU290"
Private cLinha
Private nIntBUV                                            
Private nRegBUV

  //������������������������������������������������������������������������Ŀ
  //� Criacao das variveis que podem ser utilizadas pelos campos layout...   �
  //��������������������������������������������������������������������������
  Private nLinha     //Contem o numero da linha a ser impressa
  Private nLinha2    //Contem o numero da linha por tipo de linha
  Private cTransacao //Contem o codigo da transacao              *
  Private cNomeArq   //Contem o nome do arquivo                  * 

  // * Estes codigos vem pelos parametros
   
  cLinha := ""  
  nLinha := 1 
  nLinha2 := 1
  //������������������������������������������������������������������������Ŀ
  //� Crio uma area para gravar os dados antes de exportar para um arquivo.. �
  //��������������������������������������������������������������������������
  PRIVATE aStruc  := {}
  aadd(aStruc,{"LINHA","C",800,0})
 
  PRIVATE cTrb    := FWTemporaryTable():New( "DADOS" )
  cTrb:SetFields(aStruc)
  cTrb:Create()
  
  //���������������������������������������������������������������������Ŀ
  //� Pesquisa o numero da transacao sera passada no parametro...         �
  //�����������������������������������������������������������������������
  if ! Pergunte(cPerg, .T.)                           
     DADOS->(DBCLoseArea())                               
     MsgStop("O Sistema nao pode continuar sem os parametros")
     Return
  Endif
  cTransacao := mv_par01
  cNomeDir   := AllTrim(mv_Par02)
  cNomeDir := cNomeDir+If(SubStr(cNomeDir,len(cNomeDir),1)=="\","","\")
  aCodigoTrans := PLSUPCLAY(cTransacao,2)
  //��������������������������������������������������������������������������Ŀ
  //� Executa funcao para gerar itens...                                       �
  //����������������������������������������������������������������������������
  MsAguarde({|| ProcessaArq(cTransacao,cNomearq,aCodigoTrans)}, "", "Lendo Arquivo Texto", .T.) // Exibe dialogo padrao...
  cTrb:Delete()
Return

Static Function ProcessaArq(cTransacao,cNomearq,aCodigoTrans)
  
  //���������������������������������������������������������������������Ŀ
  //� Pesquiso Parametros do Layout...                                    �
  //�����������������������������������������������������������������������
  BU8->(DbSetOrder(1))
  If ! BU8->(DbSeek(xFilial("BU8")+aCodigoTrans[1]))
     DADOS->(DBCLoseArea())                               
     MsgStop("Parametros nao localizados")
     Return
  Endif

  Pergunte("PLU"+SubStr(aCodigoTrans[1],4,3), .T.)

  //���������������������������������������������������������������������Ŀ
  //� Pesquiso Arquivo a ser aberto...                                    �
  //�����������������������������������������������������������������������
  BU7->(DbSetOrder(1))
  If ! BU7->(DbSeek(xFilial("BU7")+aCodigoTrans[1]+"1001"))
     DADOS->(DBCLoseArea())                               
     MsgStop("Arquivo nao pode ser aberto")
     Return
  Endif

  //���������������������������������������������������������������������Ŀ
  //� Pesquiso primeira Linha do layout...                                �
  //�����������������������������������������������������������������������
  BUU->(DbSetOrder(1))
  If ! BUU->(DbSeek(xFilial("BUU")+aCodigoTrans[1]))
     DADOS->(DBCLoseArea())                               
     MsgStop("Layout Invalido")
     Return
  Endif
  cNomeArq := BUU->BUU_NOMARQ  

  //���������������������������������������������������������������������Ŀ
  //� Abro os arquivos deste layout...                                    �
  //�����������������������������������������������������������������������
  While ! BU7->(EOF()) .AND. BU7->BU7_CODLAY == aCodigoTrans[1] .And. BU7->BU7_TIPOLT =="1" 
      if ! Empty(BU7->BU7_POSICA)
          cExec := BU7->BU7_NOMARQ+"->(DbSetOrder("+Alltrim(Str(BU7->BU7_INDICE))+"))"
          &cExec
          cExec := BU7->BU7_NOMARQ+"->(DbSeek(xFilial('"+BU7->BU7_NOMARQ+"')+"+AllTrim(BU7->BU7_POSICA)+"))"
          &cExec
      Endif
  BU7->(DBSkip())                                            
  EndDo

  //���������������������������������������������������������������������Ŀ
  //� Pesquiso primeira Linha do layout...                                �
  //�����������������������������������������������������������������������
  BUV->(DbSetOrder(1))
  If ! BUV->(DbSeek(xFilial("BUV")+aCodigoTrans[1]+"001"))
     DADOS->(DBCLoseArea())                               
     MsgStop("Linhas nao configuradas para este Layout")
     Return
  Endif

  //���������������������������������������������������������������������Ŀ
  //� Imprimo as Linhas...                                                 �
  //�����������������������������������������������������������������������

  While ! BUV->(EOF()) .AND. BUV->BUV_CODLAY == aCodigoTrans[1]

        if ! Empty(BUV->BUV_SEQARQ)
           //���������������������������������������������������������������������Ŀ
           //� Pesquiso Arquivo a ser aberto...                                    �
           //�����������������������������������������������������������������������
           BU7->(DbSetOrder(1))
           If ! BU7->(DbSeek(xFilial("BU7")+aCodigoTrans[1]+"1"+BUV->BUV_SEQARQ))
              DADOS->(DBCLoseArea())                               
              MsgStop("Arquivo nao pode ser aberto")
              Return                                      
           Endif

           //���������������������������������������������������������������������Ŀ                       
           //� Pesquiso sua chave no sindex...                                     �
           //�����������������������������������������������������������������������
           SIX->(dbSetorder(1))
           if ! SIX->(dbSeek(BU7->BU7_NOMARQ+AllTRim(Str(BU7->BU7_INDICE))))
              DADOS->(DBCLoseArea())                               
              MsgStop("Problemas na pesquisa do indice")
              Return
           Endif

             //���������������������������������������������������������������������Ŀ
             //� Posiciono no primeiro registro...                                   �
             //�����������������������������������������������������������������������
             cExec := BU7->BU7_NOMARQ+"->(DbSetOrder("+Alltrim(Str(BU7->BU7_INDICE))+"))"
             &cExec
             cExec := BU7->BU7_NOMARQ+"->(DbSeek(xFilial('"+BU7->BU7_NOMARQ+"')+"+AllTrim(BU7->BU7_POSICA)+"))"
             &cExec
   
             cWhile := "!"+BU7->BU7_NOMARQ+"->(EOF()) .AND. "+BU7->BU7_NOMARQ+"->("+AllTrim(SIX->CHAVE)+") == xFilial('"+BU7->BU7_NOMARQ+"')+"+AllTRim(BU7->BU7_POSICA)
        Else          
           I      := 1
           cWhile := " I == 1 "
		endif
        
        While &cWhile
                
            //��������������������������������������������������������������������������Ŀ
            //� Exibo mensagem informativa...                                            �
            //����������������������������������������������������������������������������
            MsProcTxt("Processando linha: "+AllTrim(BUV->BUV_DESLIN)+"...") // Nao mude e padrao...

            
            
            //���������������������������������������������������������������������Ŀ
            //� Abro os arquivos que dependem deste arquivo posicionando-os...      �
            //�����������������������������������������������������������������������
            PLUABREARQ()
            //���������������������������������������������������������������������Ŀ
            //� Verifico se existe condicao para esta linha...                      �
            //�����������������������������������������������������������������������
            if !Empty(BUV->BUV_CONDIC) .And. !&(BUV->BUV_CONDIC)
             if cWhile == " I == 1 "
                I := 2
              Else
                cExec := BU7->BU7_NOMARQ+"->(DbSkip())"
                &cExec
              Endif  
                Loop   
            Endif

            //���������������������������������������������������������������������Ŀ
            //� Executo o comando definido para esta linha...                       �
            //�����������������������������������������������������������������������
            if !Empty(BUV->BUV_EXECLI)
              cAux := "{ || "+BUV->BUV_EXECLI+"}"
              Eval(&cAux)
            Endif
        
            BUX->(DbSetOrder(1))
            If ! BUX->(DbSeek(xFilial("BUX")+aCodigoTrans[1]+BUV->BUV_CODLIN+"001"))
                 DADOS->(DBCLoseArea())                               
                 MsgStop("Campos nao Configurados para este Layout")
                 Return
            Endif   
            DADOS->(RecLock("DADOS",.T.))
            cLinha := ""

            //���������������������������������������������������������������������Ŀ
            //� Verifico todas os campo da linha...                                 �
            //�����������������������������������������������������������������������
            While ! BUX->(EOF()) .AND. BUX->(BUX_CODLAY+BUX_CODLIN) == aCodigoTrans[1]+BUV->BUV_CODLIN
				
		    		//���������������������������������������������������������������������Ŀ
                    //� Pesquiso o Campo para saber os valores dele...                      �
                    //�����������������������������������������������������������������������
     			    BU2->(DbSetOrder(1))
         			If ! BU2->(DbSeek(xFilial("BU2")+BUX->BUX_CODCPO))
            			MsgStop("Campo cadastrado no Layout nao existe no cadastro de Campo")
              			DADOS->(DBCLoseArea())
         	    		Return
      	    		Endif        

                    //���������������������������������������������������������������������Ŀ
                    //� Pesquiso o Tipo do Campo na tabela de tipos de dados...             �
                    //�����������������������������������������������������������������������
                    BU3->(DbSetOrder(1))
                    If ! BU3->(DbSeek(xFilial("BU3")+BU2->BU2_TIPO))
                         MsgStop("Tipo de dados nao cadastrado")
                         DADOS->(DBCLoseArea())
                         Return
                    Endif                               

      		    	//���������������������������������������������������������������������Ŀ
         			//� Verifico se existe um conteudo especifico para este campo...        �
      	    		//�����������������������������������������������������������������������
        			if ! Empty(BUX->BUX_CONTEP)
            			cValorCampo := BUX->BUX_CONTEP
       	     		Else
         	    		cValorCampo := BU2->BU2_CONTCP
      	     		endif                                           
      	     		
                    //��������������������������������������������������������������������������Ŀ
                    //� Altero o tipo de dados para String...									 |
                    //����������������������������������������������������������������������������
                    cTipo := ValType(&cValorCampo)
                    
                    if cTipo == "C" 
                      cTemp := &cValorCampo
                    Elseif cTipo == "N"
                      cTemp := Alltrim(Str(&cValorCampo))                                                   
                      ElseIf cTipo == "D"
                         cTemp := AllTrim(dtos(&cValorCampo))
                         Elseif Empty(cValorCampo)  
                           cTemp := ""
                    Endif                                                

                    //��������������������������������������������������������������������������Ŀ
                    //� Pesquiso se devo alterar o Conteudo do Campo de Acordo com as tabelas... |
                    //����������������������������������������������������������������������������
                    BU5->(DbSetOrder(1))                                         
                    If BU5->(DbSeek(xFilial("BU5")+BU6->BU6_CODCAM+cTemp)) .And. !Empty(cValorCampo)
                       cTemp := BU5->BU5_VLRPTU
                    Endif                               

                    //���������������������������������������������������������������������Ŀ
                    //� Ajusta o Tamanho do campo no arquivo...                             �
                    //�����������������������������������������������������������������������
                    if ("9" $ BU3->BU3_FORMAT) // E o tipo for numerico:
                           if Len(iif(Empty(cTemp),"0",cTemp)) <= BU2->BU2_TAMANH
                              cTemp := Replicate("0",BU2->BU2_TAMANH-Len(iif(Empty(cTemp),"0",cTemp)))+iif(Empty(cTemp),"0",cTemp) //Coloco Zeros a Esquerda
                           Else 
                              cTemp := Substr(cTemp,Len(cTemp)-BU2->BU2_TAMANH,BU2->BU2_TAMANH) //Pego somente os valores que se aplicam ao tamanho. 
                           Endif      
                    Elseif (BU3->BU3_FORMAT = "@!") .OR. Empty(cTemp) // se for tipo alfa 
                        if (Len(iif(Empty(cTemp)," ",cTemp)) > BU2->BU2_TAMANH) // Se o Resultado e maior que o campo Cadastrado
                             cTemp := Substr(cTemp,1,BU2->BU2_TAMANH) //Pego somente o numero de caracteres cadastrados
                        Else
        		             cTemp := iif(Empty(cTemp)," ",cTemp)+Space(BU2->BU2_TAMANH-Len(iif(Empty(cTemp)," ",cTemp))) //Coloco espacos a direita        
                        Endif
                    Endif         
                    cLinha += cTemp
                    BUX->(DBSkip())                                          
             EndDo     
             //���������������������������������������������������������������������Ŀ
             //� Salvo a Linha do Arquivo...                                         �
             //�����������������������������������������������������������������������
             DADOS->LINHA := cLinha
             DADOS->(MsUnlock())                                      

             //���������������������������������������������������������������������Ŀ
             //� Incremento Contadores de Linhas...                                  �
             //�����������������������������������������������������������������������
             cLinha := ""
             nLinha ++
             nLinha2 ++
             I := 2

             //���������������������������������������������������������������������Ŀ
             //� Salvo a posicao e a ordem do BUV...                                 �
             //�����������������������������������������������������������������������
             nIndBUV :=  BUV->(IndexOrd())
             nRegBUV :=  BUV->(Recno())

             //���������������������������������������������������������������������������������Ŀ
             //� Chamo a funcao recursiva que imprime varias linhas dependentes em cascata ...   �
             //�����������������������������������������������������������������������������������
             PLUDetail(BUV->BUV_SEQARQ)

             //���������������������������������������������������������������������Ŀ
             //� Retorno com a posicao do BUV...                                     �
             //�����������������������������������������������������������������������
             BUV->(DbSetOrder(nIndBUV))
             BUV->(DbGoTo(nRegBUV))
             
             //���������������������������������������������������������������������������������Ŀ
         	 //� Pulo o registro do arquivo referente a linha...                                 �
      	     //�����������������������������������������������������������������������������������
      	     if ! Empty(BUV->BUV_SEQARQ)
                cExec := BU7->BU7_NOMARQ+"->(DbSkip())"
                &(cExec)                            
             Endif   
          Enddo     
 
          //���������������������������������������������������������������������Ŀ
          //� Executo o comando definido para esta linha...                       �
          //�����������������������������������������������������������������������
          if !Empty(BUV->BUV_EXECLF)
             cAux := "{ || "+BUV->BUV_EXECLF+"}"
             Eval(&cAux)
          Endif

          BUV->(DBSkip())                                          
          nLinha2 := 1
  Enddo
  
DbSelectArea("Dados")
cNomeArq := &cNomeArq
cNomeArq := cNomeDir+cNomeArq
Copy To &cNomeArq SDF
DADOS->(DBCLoseArea())
Alert ("Arquivo Gerado com Sucesso!")
   
Return
      
/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    �PLUDetail� Autor � Antonio de Padua Cunha � Data � 02.06.02 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Gera detalhe de Linhas                                     ����
�������������������������������������������������������������������������Ĵ���
���Sintaxe   � PLUDetail                                                  ����
�������������������������������������������������������������������������Ĵ���
���Parametros�                                                            ����
��������������������������������������������������������������������������ٱ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������


/*/
Static Function PLUDetail(Seq)
Local nIntBUV
Local nRegBUV        
Local cWhile 

//���������������������������������������������������������������������Ŀ
//� Pesquiso primeira Linha do layout...                                �
//�����������������������������������������������������������������������
   if Empty(Seq)
      Return
   endif         
   
   BUV->(DbSetOrder(2))
   If BUV->(DbSeek(xFilial("BUV")+aCodigoTrans[1]+Seq))
   While !BUV->(EOF()) .AND. BUV->BUV_CODLAY == aCodigoTrans[1] .And. BUV->BUV_TIPLIN > "1" .And. AllTrim(BUV->BUV_LINHAM) == Seq

        if !Empty(BUV->BUV_SEQARQ)
           //���������������������������������������������������������������������Ŀ
           //� Pesquiso Arquivo a ser aberto...                                    �
           //�����������������������������������������������������������������������
           BU7->(DbSetOrder(1))
           If ! BU7->(DbSeek(xFilial("BU7")+aCodigoTrans[1]+"1"+Seq))
              DADOS->(DBCLoseArea())                               
              MsgStop("Arquivo nao pode ser aberto")
              Return
           Endif

           //���������������������������������������������������������������������Ŀ
           //� Pesquiso sua chave no sindex...                                     �
           //�����������������������������������������������������������������������
           SIX->(dbSetorder(1))
           if ! SIX->(dbSeek(BU7->BU7_NOMARQ+BU7->BU7_INDICE))
              DADOS->(DBCLoseArea())                               
              MsgStop("Problemas na pesquisa do indice")
              Return
           Endif

           //���������������������������������������������������������������������Ŀ
           //� Posiciono no primeiro registro...                                   �
           //�����������������������������������������������������������������������
           cExec := BU7->BU7_NOMARQ+"->(DbSetOrder("+Alltrim(Str(BU7->BU7_INDICE))+"))"
           &cExec
           cExec := BU7->BU7_NOMARQ+"->(DbSeek(xFilial('"+BU7->BU7_NOMARQ+"')+"+AllTrim(BU7->BU7_POSICA)+"))"
           &cExec
           cWhile := "!"+BU7->BU7_NOMARQ+"->(EOF()) .AND. "+SIX->CHAVE+" == "+BU7->BU7_POSICA
        Else          
           I := 1
           cWhile := " I == 1 "
        Endif

        While &cWhile
            
            //���������������������������������������������������������������������Ŀ
            //� Abro os arquivos que dependem deste arquivo posicionando-os...      �
            //�����������������������������������������������������������������������
            PLUABREARQ()
  
            //���������������������������������������������������������������������Ŀ
            //� Verifico se existe condicao para esta linha...                      �
            //�����������������������������������������������������������������������
            if !Empty(BUV->BUV_CONDIC) .And. !&(BUV->BUV_CONDIC)
              if cWhile == " I == 1 "
                I := 2
              Else
                cExec := BU7->BU7_NOMARQ+"->(DbSkip())"
                &cExec
              Endif  
                Loop   
            Endif

  

            //���������������������������������������������������������������������Ŀ
            //� Executo o comando definido para esta linha...                       �
            //�����������������������������������������������������������������������
            if !Empty(BUV->BUV_EXECLI)
              cAux := "{ || "+BUV->BUV_EXECLI+"}"
              Eval(&cAux)
            Endif
            

            BUX->(DbSetOrder(1))
            If ! BUX->(DbSeek(xFilial("BUX")+aCodigoTrans[1]+BUV->BUV_CODLIN+"001"))
               DADOS->(DBCLoseArea())                               
               MsgStop("Campos nao Configurados para este Layout")
               Return
            Endif   
            DADOS->(RecLock("DADOS",.T.))
            cLinha := ""
                                                                                              
            While ! BUX->(EOF()) .AND. BUX->(BUX_CODLAY+BUX_CODLIN) == aCodigoTrans[1]+BUV->BUV_CODLIN
    		   //���������������������������������������������������������������������Ŀ
               //� Pesquiso o Campo para saber os valores dele...                      �
               //�����������������������������������������������������������������������
 		       BU2->(DbSetOrder(1))
   			   If ! BU2->(DbSeek(xFilial("BU2")+BUX->BUX_CODCPO))
       		      MsgStop("Campo cadastrado no Layout nao existe no cadastro de Campo")
       			  DADOS->(DBCLoseArea())
   	    		  Return
     		   Endif        
      

               //���������������������������������������������������������������������Ŀ
               //� Pesquiso o Tipo do Campo na tabela de tipos de dados...             �
               //�����������������������������������������������������������������������
               If !BU3->(DbSeek(xFilial("BU3")+AllTrim(BU2->BU2_TIPO)))
                    MsgStop("Tipo de dados nao cadastrado")
                    DADOS->(DBCLoseArea())
                    Return
               Endif                               

	    	   //���������������������������������������������������������������������Ŀ
   			   //� Verifico se existe um conteudo especifico para este campo...        �
    		   //�����������������������������������������������������������������������
   			   if ! Empty(BUX->BUX_CONTEP)
       		    	cValorCampo := BUX->BUX_CONTEP
     		   Else
   	    	     	cValorCampo := BU2->BU2_CONTCP
     		   endif
      			
      		   //��������������������������������������������������������������������������Ŀ
               //� Altero o tipo de dados para String...									 |
               //����������������������������������������������������������������������������
               cTipo := ValType(&cValorCampo)
                 
               if cTipo == "C" 
                  cTemp := AllTrim(&cValorCampo)
               Elseif cTipo == "N"
                  cTemp := Alltrim(Str(&cValorCampo))                                                   
                  ElseIf cTipo == "D"
                      cTemp := AllTrim(dtos(&cValorCampo))
                      Elseif Empty(cValorCampo)        
                         cTemp := ""
               Endif                                                

               //��������������������������������������������������������������������������Ŀ
               //� Pesquiso se devo alterar o Conteudo do Campo de Acordo com as tabelas... |
               //����������������������������������������������������������������������������
               BU5->(DbSetOrder(1))                                         
               If BU5->(DbSeek(xFilial("BU3")+BU6->BU6_CODCAM+cTemp)) .And. !Empty(cValorCampo)
                  cTemp := BU5->BU5_VLRPTU
               Endif                               

               //���������������������������������������������������������������������Ŀ
               //� Ajusta o Tamanho do campo no arquivo...                             �
               //�����������������������������������������������������������������������
                    if ("9" $ BU3->BU3_FORMAT) // E o tipo for numerico:
                           if Len(iif(Empty(cTemp),"0",cTemp)) <= BU2->BU2_TAMANH
                              cTemp := Replicate("0",BU2->BU2_TAMANH-Len(iif(Empty(cTemp),"0",cTemp)))+iif(Empty(cTemp),"0",cTemp) //Coloco Zeros a Esquerda
                           Else 
                              cTemp := Substr(cTemp,Len(cTemp)-BU2->BU2_TAMANH,BU2->BU2_TAMANH) //Pego somente os valores que se aplicam ao tamanho. 
                           Endif      
                    Elseif (BU3->BU3_FORMAT = "@!") .OR. Empty(cTemp) // se for tipo alfa 
                        if (Len(iif(Empty(cTemp)," ",cTemp)) > BU2->BU2_TAMANH) // Se o Resultado e maior que o campo Cadastrado
                             cTemp := Substr(cTemp,1,BU2->BU2_TAMANH) //Pego somente o numero de caracteres cadastrados
                        Else
        		             cTemp := iif(Empty(cTemp)," ",cTemp)+Space(BU2->BU2_TAMANH-Len(iif(Empty(cTemp)," ",cTemp))) //Coloco espacos a direita        
                        Endif
                    Endif         
                
               cLinha += cTemp
               BUX->(DBSkip())                                          
            EndDo    
            DADOS->LINHA := cLinha
            DADOS->(MsUnlock())
            cLinha := ""                                      
            nLinha ++
            nLinha2 ++
            I := 2
       
            nRegBUV :=  BUV->(Recno())

            PLUDetail(BUV->BUV_SEQARQ)
             
            BUV->(DbGoTo(nRegBUV))       
       
            //���������������������������������������������������������������������������������Ŀ
            //� Pulo o registro do arquivo referente a linha...                                 �
            //�����������������������������������������������������������������������������������
            if ! Empty(BUV->BUV_SEQARQ)
              cExec := BU7->BU7_NOMARQ+"->(DbSkip())"
              &cExec
            Endif  
        Enddo     

       //���������������������������������������������������������������������Ŀ
       //� Executo o comando definido para esta linha...                       �
       //�����������������������������������������������������������������������
       if !Empty(BUV->BUV_EXECLF)
           cAux := "{ || "+BUV->BUV_EXECLF+"}"
           Eval(&cAux)
       Endif

  BUV->(DBSkip())                                                     
   nLinha2:= 1
  Enddo 
 Endif   

Return


/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    �PLUABREARQ� Autor � Antonio de Padua Cunha � Data � 02.06.02����
�������������������������������������������������������������������������Ĵ���
���Descricao � Gera detalhe de Linhas                                     ����
�������������������������������������������������������������������������Ĵ���
���Sintaxe   � PLUDetail                                                  ����
�������������������������������������������������������������������������Ĵ���
���Parametros�                                                            ����
��������������������������������������������������������������������������ٱ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/     
Static Function PLUABREARQ()
Local cSeq

      nIndBU7 :=  BU7->(IndexOrd())
      nRegBU7 :=  BU7->(Recno())
      cSeq := BU7->BU7_SEQUEN
      
      //���������������������������������������������������������������������Ŀ
      //� Pesquiso sua chave no sindex...                                     �
      //�����������������������������������������������������������������������
      BU7->(DbSetOrder(2))
      If ! BU7->(DbSeek(xFilial("BU7")+aCodigoTrans[1]+cSeq))
         BU7->(dbSetOrder(nIndBU7))
         BU7->(dbGoto(nRegBU7))    
         Return
      Endif          

      //���������������������������������������������������������������������Ŀ
      //� Abro os arquivos deste layout...                                    �
      //�����������������������������������������������������������������������
      While ! BU7->(EOF()) .AND. BU7->BU7_CODLAY == aCodigoTrans[1] .And. BU7->BU7_TIPOLT =="1"  .And. BU7->BU7_ARQMST == cSeq
          if ! Empty(BU7->BU7_POSICA)
             cExec := BU7->BU7_NOMARQ+"->(DbSetOrder("+Alltrim(Str(BU7->BU7_INDICE))+"))"
             &cExec
             cExec := BU7->BU7_NOMARQ+"->(DbSeek(xFilial('"+BU7->BU7_NOMARQ+"')+"+AllTrim(BU7->BU7_POSICA)+"))"
             &cExec
         Endif
         BU7->(DBSkip())                                            
     EndDo

     BU7->(dbSetOrder(nIndBU7))
     BU7->(dbGoto(nRegBU7))    
     

Return
