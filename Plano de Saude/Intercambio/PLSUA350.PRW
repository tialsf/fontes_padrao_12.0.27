
#Include "PROTHEUS.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PLSUA350  �Autor  �Andre Schwartz      � Data �  26/02/04   ���
�������������������������������������������������������������������������͹��
���Desc.     � PTU - A100 - Movimentacao Cadastral de Beneficiario        ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP6 - Pls                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function PLSUA350()
Local   cPerg   := "PLSU35"
Local   cNomArq
Private cUniOri
Private cUniDes
Private dDatGer
Private dDatIni
Private dDatFin
Private cEmpIni
Private cEmpFin
Private cEmpAtu
Private cTipEnv
Private nTipEnv
Private aTipEnv := {"A","M","P"}
Private n_TOT_R202     :=0
Private n_TOT_R204     :=0
Private n_TOT_INCL     :=0
Private n_TOT_EXC      :=0
Private n_TOT_ALT      :=0
Private n_TOT_TPPLANO  :=0
Private n_TOT_R205     :=0
Private n_TOT_R206     :=0
Private n_TOT_R207     :=0
Private n_TOT_ERRO     :=0

If Pergunte(cPerg,.T.)
//   cUniOri := mv_par01
//   cUniDes := mv_par02
   dDatGer := dDataBase 
   dDatIni := mv_par01
   dDatFin := mv_par02
   cEmpIni := mv_par03
   cEmpFin := mv_par04
   cTipEnv := aTipEnv[mv_par05]
   nTipEnv := mv_par05
	//�������������������
	//�Valida o Pergunte�
	//�������������������
   If !UA350Perg()
     Return Nil
   End If  
   //define o nome do arquivo conforme layout
   //sera necessario criar um campo no DE9 para gravar a ultima 
   //sequencia gerada...
   //Seleciona quais as operadoras que devem ser enviadas
   UA351Qry()
   If EOF()
   	  MsgStop("Nenhum registro encontrado para os parametros informados.")   
   Else	 
      While !EOF()
		   cUniOri := Plsintpad() // retorna a operadora padrao
	   		   cUniDes := OPE->OPE_CODOPE
		   cNomArq := UA350TMov()+Substr(cUniOri,2,3)+"0001."+Substr(cUniDes,2,3)
		   //monta o o tmp da query
		   UA350Qry()
		   If EOF()
		   	  MsgStop("Nenhum registro encontrado para os parametros informados.")   
		   Else	 
		   	  PlsPTU("A200  ",cNomArq)
		   EndIf                       
		   
		   //Marca registros como enviados
		   DBSelectArea("TMP")
		   DbGoTop()
		   While !EOF()
			  DbSelectArea("BA1")
			  DbSetOrder(2)
			  DbGoTop()
			  If DBSeek(xFilial("BA1") + TMP->(BA1_CODINT+R202_ID_BENE))
			     RecLock("BA1",.F.)
			     BA1_STAEDI := "3"
			     MsUnlock()
			  End If 
	      	      DBSelectArea("TMP")
			  DbSkip()
		   End
		   //fecha o tmp da query
		   DBCloseArea()
	
		   DBSelectArea("OPE")
	       DbSkip()
	  End	
   End If
   //fecha o tmp da query
   DBSelectArea("OPE")
   DBCloseArea()
End If
Return Nil

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �UA350Perg �Autor  �Andre Schwartz      � Data �  26/02/04   ���
�������������������������������������������������������������������������͹��
���Desc.     �Valida os parametros informados no  pergunte                ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP6 -  SIGAPLS                                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function UA350Perg()
Local lRet := .T.
If dDatFin > dDataBase
  lRet := .F.
  MsgStop("A data de envio deve ser inferior ou igual a data base do sistema.")
EndIf
/*
If cUniOri == cUniDes
  lRet := .F.
  MsgStop("Unimed Destino deve ser dirente da Unimed Origem.")
EndIf
*/
Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �UA350Qry  �Autor  �Andre Schwartz      � Data �  26/02/04   ���
�������������������������������������������������������������������������͹��
���Desc.     �Selecao dos registros							              ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP6 -  SIGAPLS                                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function UA350Qry()
Local cQuery := ""
Local cEnter := Chr(13)
cQuery := "SELECT  * FROM V_PTU_A200 "+cEnter
cQuery += "WHERE R201_CD_UNI_ORI = '"+cUniOri+"' AND R201_CD_UNI_DES = '"+cUniDes+"' "+cEnter
cQuery += "AND R202_CODEMP >= '"+cEmpIni+"' AND R202_CODEMP <= '"+cEmpFin+"' "+cEnter
Do Case
	Case nTipEnv == 1 //massa total somente com ativos
		cQuery += "AND R202_DT_EXCL_UN = '' "+cEnter		
	Case nTipEnv == 3 //periodico
		cQuery += "AND  "+cEnter
		cQuery += "(  "+cEnter
		cQuery += "(R202_DT_INCL_UN >= '"+DToS(dDatIni)+"' AND R202_DT_INCL_UN <= '"+DToS(dDatFin)+"') "+cEnter
		cQuery += "OR  "+cEnter
		cQuery += "(R202_DT_EXCL_UN >= '"+DToS(dDatIni)+"' AND R202_DT_EXCL_UN <= '"+DToS(dDatFin)+"') "+cEnter
		cQuery += "OR  "+cEnter
		cQuery += "(BA3_DATPLA      >= '"+DToS(dDatIni)+"' AND BA3_DATPLA      <= '"+DToS(dDatFin)+"') "+cEnter
		cQuery += ") "
EndCase
IncProc( "Selecionando registros." )
CursorWait() 
cQuery := ChangeQuery( cQuery )
dbUseArea( .T., "TOPCONN", TcGenQry( ,, cQuery ),"TMP", .F., .T. )
CursorArrow() 
Return
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �UA351Qry  �Autor  �Andre Schwartz      � Data �  26/02/04   ���
�������������������������������������������������������������������������͹��
���Desc.     �Identifica��o das operadoras a serem enviados os arquivos   ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP6 -  SIGAPLS                                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function UA351Qry()
Local cQuery := ""
Local cEnter := Chr(13)
cQuery := "SELECT  DISTINCT(R201_CD_UNI_DES) OPE_CODOPE FROM V_PTU_A200 "+cEnter
cQuery += "WHERE "+cEnter
cQuery += "R202_CODEMP >= '"+cEmpIni+"' AND R202_CODEMP <= '"+cEmpFin+"' "+cEnter
Do Case
	Case nTipEnv == 1 //massa total somente com ativos
		cQuery += "AND R202_DT_EXCL_UN = '' "+cEnter		
	Case nTipEnv == 3 //periodico
		cQuery += "AND  "+cEnter
		cQuery += "(  "+cEnter
		cQuery += "(R202_DT_INCL_UN >= '"+DToS(dDatIni)+"' AND R202_DT_INCL_UN <= '"+DToS(dDatFin)+"') "+cEnter
		cQuery += "OR  "+cEnter
		cQuery += "(R202_DT_EXCL_UN >= '"+DToS(dDatIni)+"' AND R202_DT_EXCL_UN <= '"+DToS(dDatFin)+"') "+cEnter
		cQuery += "OR  "+cEnter
		cQuery += "(BA3_DATPLA      >= '"+DToS(dDatIni)+"' AND BA3_DATPLA      <= '"+DToS(dDatFin)+"') "+cEnter
		cQuery += ") "
EndCase
IncProc( "Selecionando registros." )
CursorWait() 
cQuery := ChangeQuery( cQuery )
dbUseArea( .T., "TOPCONN", TcGenQry( ,, cQuery ),"OPE", .F., .T. )
CursorArrow() 
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �UA350Cont �Autor  �Andre Schwartz      � Data �  26/02/04   ���
�������������������������������������������������������������������������͹��
���Desc.     �Contador do tipo de operacao (beneficiarios incl/exc/alt/pl)���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP6 -  SIGAPLS                                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function UA350Cont()

If (TMP->R202_DT_INCL_UN >= DToS(dDatIni)) .AND.  (TMP->R202_DT_INCL_UN <= DToS(dDatFin))
  n_TOT_INCL++
EndIf

If (TMP->R202_DT_EXCL_UN >= DToS(dDatIni)) .AND.  (TMP->R202_DT_EXCL_UN <= DToS(dDatFin))
  n_TOT_EXC++
EndIf  

If (TMP->BA1_ULTENV < TMP->BA3_DATALT)
  n_TOT_ALT++
EndIf

If (TMP->R202_DT_INCL_UN < TMP->BA3_DATPLA) .AND. (TMP->BA3_DATPLA >= DToS(dDatIni)) .AND.  (TMP->BA3_DATPLA <= DToS(dDatFin))
  n_TOT_TPPLANO++
EndIf

If !Empty(TMP->BA1_CODERR)
  n_TOT_ERRO++
EndIf

Return Nil

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �UA350TMov �Autor  �Andre Schwartz      � Data �  01/03/04   ���
�������������������������������������������������������������������������͹��
���Desc.     �Identifica o tipo da movimentacao retornando                ���
���          �V-Usuario intercambio                                       ���
���          �T-Seguro de vida                                            ���
���          �R-Beneficio familia                                         ���
���          �G-Franquia                                                  ���
���          �H-Aero Medico                                               ���
���          �X-Remissao                                                  ���
���          �Z-Garantia Funeral                                          ���
���          �N-Farmacia                                                  ���
���          �E-Plano Pago                                                ���
���          �C-Ambulancia                                                ���
���          �I-Coracao/p1                                                ���
���          �Q-PRotecao familiar                                         ���
���          �J-Reservado para acordos regionais                          ���
���          �0(zero)-Reservado para acordos regionais                    ���
�������������������������������������������������������������������������͹��
���Uso       � AP6 -  SIGAPLS                                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function UA350TMov()
Local cRet := ""

If BBT->BBT_CODOPE <> BBT->BBT_OPEDES
	cRet := "V"
Else
	cRet := Posicione("BF4",1,xFilial("BF4")+TMP->BA1_CODINT+TMP->BA1_CODEMP+TMP->BA1_MATRIC+TMP->BA1_TIPREG,"BF4_CLAOPC")
End If

Return(cRet)


/*
Function PlsA200Dbg()
Static nCont := 0
nCont++
conout("registro "+Str(nCont,10))
Return

*/
