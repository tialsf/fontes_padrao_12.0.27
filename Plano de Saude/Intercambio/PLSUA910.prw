#include "plsmger.ch"
#include "protheus.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PLSUA910  �Autor  �Eduardo Motta       � Data �  21/02/05   ���
�������������������������������������������������������������������������͹��
���Desc.     � Rotina para agrupar arquivos do PTU A500                   ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP6                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function PLSUA910
Local cCaminho   := ""
Local cOperadora := "" 

//��������������������������������������������������������������������������Ŀ
//� Pergunte																 �
//����������������������������������������������������������������������������
If !Pergunte("PLU910")
   Return
EndIf

cOperadora := mv_par01
cCaminho   := AllTrim(mv_par02)
If SubStr(cCaminho,Len(cCaminho),1)=="\" .or. SubStr(cCaminho,Len(cCaminho),1)=="/"
   cCaminho := SubStr(cCaminho,1,Len(cCaminho)-1)
EndIf    
//�����������������������������������������������������
//�Checa titulo										  �
//�����������������������������������������������������
If mv_par03 == 0
   MsgAlert('Numero do Titulo Invalido')
   Return
EndIf
//�����������������������������������������������������
//�Para complemento do nome do arquivo				  �
//�����������������������������������������������������
cNomArq := AllTrim(Str(mv_par03))
//�����������������������������������������������������
//�Processa											  �
//�����������������������������������������������������
Processa({|| ProcArq(cCaminho,cOperadora,cNomArq) }, "Processando...", "", .T.)

Return


Static Function ProcArq(cCaminho,cOperadora,cNomArq)
Local aFiles
Local nI
Local nK
Local nJ
Local cArq
Local cLinha
Local nVrTotFat := 0.00
Local nVrTotLiq := 0.00
Local nVrTotAju := 0.00
Local nTotR502  := 0.00
Local nTotR503  := 0.00
Local nTotR504  := 0.00
Local nTotR505  := 0.00
Local nTotExc   := 0.00
Local nTotAmb   := 0.00
Local nTotQtSer := 0.00
Local nTotVrSer := 0.00
Local nTotVr511 := 0.00
Local cLinCab   := ""
Local cLinTra   := ""
Local cSeq      := "01"
Local nLinha    := 1
Local aDad510   := {}          
Local aDad511	:= {}
cCaminho := AllTrim(cCaminho)
aFiles := Directory(cCaminho+"\*.*")

ProcRegua(Len(aFiles)*2)

aDad510 := {}
BA0->(DbSetOrder(1))
BA0->(MsSeek(xFilial("BA0")+cOperadora))
aadd(aDad510,{padr("2",1),;
			  padr(BA0->BA0_SUSEP,10),;
			  padr(BA0->BA0_NOMINT,60),;
			  padr(BA0->BA0_END,40),;
			  padr('',20),;
			  padr(BA0->BA0_BAIRRO,30),;
			  padr(BA0->BA0_CEP,8),;
			  padr(BA0->BA0_CIDADE,30),;
			  padr(BA0->BA0_EST,2),;
			  padr(StrZero(Val(BA0->BA0_CGC),15),15),;
			  padr(strzero(val(BA0->BA0_INCEST),20),20),;
			  padr( If( BA0->(FieldPos("BA0_DDD")) > 0 , strzero(val(BA0->BA0_DDD),4)  ,strzero(0,4)),4),;
			  padr( If(!Empty(BA0->BA0_TELEF1),alltrim(strtran(BA0->BA0_TELEF1,'-','')),strzero(0,8)),8),;
			  padr( If(!Empty(BA0->BA0_FAX1  ),alltrim(strtran(BA0->BA0_FAX1  ,'-','')),strzero(0,8)),8)})
						
BA0->(MsSeek(xFilial("BA0")+PlsIntPad()))
aadd(aDad510,{padr("1",1),;
			  padr(BA0->BA0_SUSEP,10),;
			  padr(BA0->BA0_NOMINT,60),;
			  padr(BA0->BA0_END,40),;
			  padr('',20),;
			  padr(BA0->BA0_BAIRRO,30),;
			  padr(BA0->BA0_CEP,8),;
			  padr(BA0->BA0_CIDADE,30),;
			  padr(BA0->BA0_EST,2),;
			  padr(StrZero(Val(BA0->BA0_CGC),15),15),;
			  padr(strzero(val(BA0->BA0_INCEST),20),20),;
			  padr( If( BA0->(FieldPos("BA0_DDD")) > 0 , strzero(val(BA0->BA0_DDD),4)  ,strzero(0,4)),4),;
			  padr( If(!Empty(BA0->BA0_TELEF1),alltrim(strtran(BA0->BA0_TELEF1,'-','')),strzero(0,8)),8),;
			  padr( If(!Empty(BA0->BA0_FAX1  ),alltrim(strtran(BA0->BA0_FAX1  ,'-','')),strzero(0,8)),8)})

For nI := 1 to Len(aFiles)
   IncProc("Analisando arquivo : "+aFiles[nI,1])
   cArq := cCaminho+"\"+aFiles[nI,1]
   FT_FUSE(cArq)
   While !FT_FEof()
      cLinha := FT_FREADLN()
      If SubStr(cLinha,09,03)=="501"
         If Empty(cLinCab)
            cLinCab := cLinha
         EndIf
         nVrTotFat+= (Val(SubStr(cLinha,059,14)))
         nVrTotLiq+= (Val(SubStr(cLinha,073,14)))
         nVrTotAju+= 0
      ElseIf SubStr(cLinha,09,03)=="509"   
         If Empty(cLinTra)
            cLinTra := cLinha
         EndIf
         nTotR502  += (Val(SubStr(cLinha,12,05)))
         nTotR503  += (Val(SubStr(cLinha,17,05)))
         nTotR504  += (Val(SubStr(cLinha,22,05)))
         nTotR505  += (Val(SubStr(cLinha,27,05)))
         nTotExc   += (Val(SubStr(cLinha,32,05)))
         nTotAmb   += (Val(SubStr(cLinha,37,05)))
         nTotQtSer += (Val(SubStr(cLinha,42,11)))
         nTotVrSer += (Val(SubStr(cLinha,53,14)))
      ElseIf SubStr(cLinha,09,03)=="511"   
      	 If (nK := aScan(aDad511,{|x| x[1] == SubStr(cLinha,14,74)})) > 0 
      	 	aDad511[nK][2] += Val(SubStr(cLinha,88,14))        
      	 Else 
      	    aadd(aDad511,{SubStr(cLinha,14,74),Val(SubStr(cLinha,88,14))})
      	 Endif
      EndIf   
      FT_FSKIP()
   EndDo
   FT_FUSE()
Next
//�����������������������������������������������������
//�Monta Header										  �
//�����������������������������������������������������
cLinha := cLinCab
cLinha := SubStr(cLinha,1,11)+cOperadora+SubStr(cLinha,16,16)+StrZero(Val(cNomArq),11)+SubStr(cLinha,43,16)+StrZero(nVrTotFat,14)+StrZero(nVrTotLiq,14)+'              '+StrZero(nVrTotAju,14)+SubStr(cLinha,115)+Chr(13)+Chr(10)
//�����������������������������������������������������
//�Monta nome do arquivo							  �
//�����������������������������������������������������
cNomArq := AllTrim('n'+StrZero(Val(cNomArq),7)+'.'+SubStr(PlsIntPad(),2,3))
//�����������������������������������������������������
//�Abre o Arquivo									  �
//�����������������������������������������������������
nH := fCreate(cCaminho+"\"+cNomArq )

FWrite(nH,cLinha)
nLinha++
For nI := 1 to Len(aFiles)
   IncProc("Processando arquivo : "+aFiles[nI,1])
   cArq := cCaminho+"\"+aFiles[nI,1]
   FT_FUSE(cArq)
   While !FT_FEof()
      cLinha := FT_FREADLN()
      If SubStr(cLinha,09,03)#"501" .and. SubStr(cLinha,09,03)#"509" .and. SubStr(cLinha,09,03)#"510" .and. SubStr(cLinha,09,03)#"511"
         FWrite(nH,StrZero(nLinha++,8)+SubStr(cLinha,9)+Chr(13)+Chr(10))
      EndIf                        
      FT_FSKIP()
   EndDo
   FT_FUSE()
Next
cLinha := cLinTra
cLinha := StrZero(nLinha++,8)+"509"+StrZero(nTotR502,5)+StrZero(nTotR503,5)+StrZero(nTotR504,5)+StrZero(nTotR505,5)+StrZero(nTotExc,5)+StrZero(nTotAmb,5)+StrZero(nTotQtSer,11)+StrZero(nTotVrSer,14)+Chr(13)+Chr(10)

FWrite(nH,cLinha)

For nK := 1 to Len(aDad510)
	cLinha := StrZero(nLinha,8)+"510"
	For nJ:=1 to Len(aDad510[nK])
		cLinha += aDad510[nK][nJ]
	Next                   
	cLinha += Chr(13)+Chr(10)
	FWrite(nH,cLinha)       
	nLinha++
Next        

cSeq := "01"
For nK := 1 to Len(aDad511)
	cLinha := StrZero(nLinha,8)+"511"	
	cLinha += cSeq+aDad511[nK][1]
	cLinha += strzero(aDad511[nK][2],14)
	If nK <> Len(aDad511)
		cLinha += Chr(13)+Chr(10)
	Endif
	FWrite(nH,cLinha) 
	nLinha++
	cSeq := soma1(cSeq)
Next

FClose(nH)

MsgStop("Arquivo Gerado."+cCaminho+"\"+cNomArq)
 
Return