#INCLUDE "PROTHEUS.CH"
#INCLUDE "PLSMGER.CH"
/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Funcao    � PLSUCEDI � Autor � Eduardo Motta           � Data � 25.04.01 ���
���������������������������������������������������������������������������Ĵ��
���Descricao � Conferencia de arquivos PTU Batch                            ���
���������������������������������������������������������������������������Ĵ��
���Sintaxe   �PLSUCEDI()                                                    ���
���������������������������������������������������������������������������Ĵ��
���            ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL             ���
���������������������������������������������������������������������������Ĵ��
���Programador   � Data   � BOPS �  Motivo da Altera��o                     ���
���������������������������������������������������������������������������Ĵ��
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
/*/

Function PLSUCEDI()
Local cCodLay := Space(06)
Local cArq    := ""
Local aCodReg := {}
Local oDlg
Local oCodReg
Local bLine := { || {aCodReg[oCodReg:nAt,1],aCodReg[oCodReg:nAt,2]} }
Local lRet := .f.
Local aCampos := {}	
Local oButCfg
Local nI
Local nJ
Local cArqIni

CriaSX1()

If !Pergunte("PLUEDI")
   Return
EndIf
cCodLay := MV_PAR01
cArq    := AllTrim(MV_PAR02)
cArqIni := RetStartPath()+"\PTUCFG_"+AllTrim(cCodLay)+".INI"

If !File(cArq)
   MsgStop(" Arquivo nao encontrado. ["+cArq+"]")
   Return
EndIf

DE3->(DbSetOrder(1))
DE1->(DbSetOrder(1))
DE0->(DbSetOrder(1))
If !DE0->(DbSeek(xFilial()+cCodLay))
   MsgStop(" LayOut nao existe. ["+cCodLay+"]")
   Return
EndIf

While !DE0->(Eof()) .and. xFilial("DE0")+cCodLay == DE0->(DE0_FILIAL+DE0_CODLAY)
   aCampos := {}
   DE1->(DbSeek(xFilial()+DE0->DE0_CODLAY+DE0->DE0_CODREG))
   While !DE1->(Eof()) .and. DE1->(DE1_FILIAL+DE1_CODLAY+DE1_CODREG) == xFilial("DE1")+DE0->DE0_CODLAY+DE0->DE0_CODREG
      aadd(aCampos,{.T.,DE1->DE1_CAMPO,DE1->DE1_LAYTAM,DE1->DE1_OBSERV,Val(DE1->DE1_POSINI),Val(DE1->DE1_POSFIM),DE1->DE1_CAMPO,"","C",0})

      aCampos[Len(aCampos),1]  := (GetPvProfString( DE0->DE0_CODREG+aCampos[Len(aCampos),2], "Ativo" , "YES", cArqIni )=="YES")
      aCampos[Len(aCampos),7]  := GetPvProfString( DE0->DE0_CODREG+aCampos[Len(aCampos),2], "Titulo" ,aCampos[Len(aCampos),7] , cArqIni )
      aCampos[Len(aCampos),8]  := GetPvProfString( DE0->DE0_CODREG+aCampos[Len(aCampos),2], "Picture" ,aCampos[Len(aCampos),8] , cArqIni )
      aCampos[Len(aCampos),9]  := GetPvProfString( DE0->DE0_CODREG+aCampos[Len(aCampos),2], "Tipo" ,aCampos[Len(aCampos),9] , cArqIni )
      aCampos[Len(aCampos),10] := Val(GetPvProfString( DE0->DE0_CODREG+aCampos[Len(aCampos),2], "Decimais" ,StrZero(aCampos[Len(aCampos),10],4) , cArqIni ))

      DE1->(DbSkip())
   EndDo
   DE3->(DbSeek(xFilial()+DE0->DE0_CODREG))
   aadd(aCodReg,{DE0->DE0_CODREG,DE3->DE3_DSCREG,aClone(aCampos)})
   DE0->(DbSkip())
EndDo

DEFINE MSDIALOG oDlg TITLE "Conferencia do LayOut "+cCodLay FROM 008,010 TO 032,75
oDlg:lEscClose := .F.

   @ 015,000 LISTBOX oCodReg  FIELDS HEADERS "Registro","Descricao" FIELDSIZES 40, 130  SIZE 220,165 OF oDlg PIXEL
   oCodReg:SetArray(aCodReg)
   oCodReg:bLine := bLine
   oCodReg:bLDblClick := {||Eval(oButCfg:bAction)}

   @ 015,222 BUTTON oButCfg Prompt "Configurar" SIZE 35,13 PIXEL ACTION (CfgCampo(cCodLay,oCodReg,aCodReg)) OF oDlg

ACTIVATE DIALOG oDlg  CENTERED ON INIT EnchoiceBar(oDlg,{|| lRet := .T.,oDlg:End()},{|| lRet := .f. , oDlg:End() })

If lRet
   For nI := 1 to Len(aCodReg)
      For nJ := 1 to Len(aCodReg[nI,3])
         WritePPros( aCodReg[nI,1]+aCodReg[nI,3,nJ,2], "Ativo"		, If(aCodReg[nI,3,nJ,1],"YES","NO"), cArqIni )
         WritePPros( aCodReg[nI,1]+aCodReg[nI,3,nJ,2], "Titulo"	, aCodReg[nI,3,nJ,7], cArqIni )
         WritePPros( aCodReg[nI,1]+aCodReg[nI,3,nJ,2], "Picture"	, aCodReg[nI,3,nJ,8], cArqIni )
         WritePPros( aCodReg[nI,1]+aCodReg[nI,3,nJ,2], "Tipo"	    , aCodReg[nI,3,nJ,9], cArqIni )
         WritePPros( aCodReg[nI,1]+aCodReg[nI,3,nJ,2], "Decimais"  , StrZero(aCodReg[nI,3,nJ,10],4), cArqIni )
      Next
   Next   
   ImpPtu(cCodLay,aCodReg,cArq)
EndIf

Return

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    � ImpPtu  � Autor � Eduardo Motta          � Data � 25.04.06 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Imprime conferencia do PTU                                 ����
�������������������������������������������������������������������������Ĵ���
���Sintaxe   � ImpPtu()                                                   ����
�������������������������������������������������������������������������Ĵ���
��� Uso      �                                                            ����
�������������������������������������������������������������������������Ĵ���
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function ImpPtu(cCodLay,aCodReg,cArq)
Local cLinha
Local cCodReg
Local aCampos
Local nPos
Local cLinImp := ""
Local cRegAnt := ""
Local cCab
Local cDadImp
LOCAL nI

PRIVATE nQtdLin     := 70
PRIVATE cNomeProg   := "PLSUAEDI"
PRIVATE nCaracter   := 15
PRIVATE nLimite     := 132
PRIVATE cTamanho    := "M"
PRIVATE cTitulo     := "Conferencia de PTU - "+cCodLay
PRIVATE cDesc1      := "Conferencia de PTU - "+cCodLay
PRIVATE cDesc2      := ""
PRIVATE cDesc3      := ""
PRIVATE cAlias      := "DE0"
PRIVATE cPerg       := "PLUEDI"
PRIVATE nRel        := "PLSUAEDI"
PRIVATE m_pag       := 1
PRIVATE lCompres    := .F.
PRIVATE lDicion     := .F.
PRIVATE lFiltro     := .T.
PRIVATE lCrystal    := .F.
PRIVATE aOrderns    := {}
PRIVATE aReturn     := { "Zebrado", 1,"Administracao", 1, 1, 1, "",1 }
PRIVATE lAbortPrint := .F.
PRIVATE cCabec1 := ""
PRIVATE cCabec2 := ""
PRIVATE nColuna     := 00
PRIVATE nLi         := 0

nRel := SetPrint(cAlias,nRel,cPerg,@cTitulo,cDesc1,cDesc2,cDesc3,lDicion,aOrderns,lCompres,cTamanho,{},lFiltro,lCrystal)
//��������������������������������������������������������������������������Ŀ
//� Verifica se foi cancelada a operacao                                     �
//����������������������������������������������������������������������������
If nLastKey  == 27
   Return
Endif
SetDefault(aReturn,cAlias)

REDIPag(.T.,cTitulo)
FT_FUSE(cArq)
While !FT_FEOF()
   cLinha := FT_FREADLN()
   cCodReg := PadR("R"+SubStr(cLinha,9,3),6)
   nPos := aScan(aCodReg,{|x|x[1]==cCodReg})
   If nPos == 0
      FT_FSKIP()
      Loop
   EndIf   
   aCampos := aClone(aCodReg[nPos,3])
   cLinImp := ""
   cCab    := ""
   For nI := 1 to Len(aCampos)
      If !aCampos[nI,1]
         Loop
      EndIf
      cDadImp := SubStr(cLinha,aCampos[nI,5],aCampos[nI,3])+" "
      If aCampos[nI,9] == "D"
         cDadImp := DtoC(StoD(cDadImp))+" "
      EndIf
      If aCampos[nI,9]=="N" .and. aCampos[nI,10] > 0
         cDadImp := Val(cDadImp)/Val("1"+Replicate("0",aCampos[nI,10]))
      EndIf      
      If !Empty(aCampos[nI,8])
         cDadImp := Transform(cDadImp,AllTrim(aCampos[nI,8]))
      EndIf
      If Len(cDadImp) < Len(AllTrim(aCampos[nI,7]))+1
         cDadImp := PadR(cDadImp,Len(AllTrim(aCampos[nI,7]))+1)
      EndIf
      cCab    += PadR(AllTrim(aCampos[nI,7]),Len(cDadImp))
      cLinImp += cDadImp
   Next
   If Len(cLinImp) > 0
      REDIPag(.f.,cTitulo)
      If ! (cRegAnt == cCodReg)
        nLi++
        @ ++nLi,00 pSay "Registro : "+cCodReg
        @ ++nLi,00 pSay cCab
        @ ++nLi,00 PSAY __PrtThinLine()
      EndIf
      @ ++nLi,00 pSay cLinImp
   EndIf
   cRegAnt := cCodReg
   FT_FSKIP()
EndDo

If  aReturn[5] == 1
    Set Printer To
	OurSpool(nrel)
End

ms_flush()

Return


/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Programa   � REDIPag  � Autor �   Eduardo Motta       � Data � 25.04.06 ���
��������������������������������������������������������������������������Ĵ��
���Descricao  � Avanca Pagina caso necessario...                           ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
/*/
Static Function REDIPag(lForce, cTitulo)
DEFAULT lForce := .F.                             

//��������������������������������������������������������������������������Ŀ
//� Posiciona no credenciado informado no parametro...                       �
//����������������������������������������������������������������������������
If nLi > nQtdLin .Or. lForce
   nLi := 01
   nLi := Cabec(cTitulo,cCabec1,cCabec2,cNomeProg,cTamanho,nCaracter)
   nLi ++
   
Endif

Return

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    �CfgCampo � Autor � Eduardo Motta          � Data � 25.04.06 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Configura campo                                            ����
�������������������������������������������������������������������������Ĵ���
���Sintaxe   � CfgCampo()                                                 ����
�������������������������������������������������������������������������Ĵ���
��� Uso      �                                                            ����
�������������������������������������������������������������������������Ĵ���
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function CfgCampo(cCodLay,oCodReg,aCodReg)
Local oCampos
Local aCampos
Local bLine := { || {If(aCampos[oCampos:nAt,1],oOk,oNo),aCampos[oCampos:nAt,2],aCampos[oCampos:nAt,4]} }
Local oDlg
Local oOk := LoadBitmap( GetResources(), "LBOK" )
Local oNo := LoadBitmap( GetResources(), "LBNO" )
Local lRet := .F.
Local cTitulo  := Space(20)
Local cPicture := Space(30)
Local cTipoCpo := "C"
Local nDec     := 0
Local oTitulo
Local oPicture
Local oTipoCpo
Local oDec
Local nPosAnt := 1

If oCodReg:nAt == 0
   Return
EndIf

aCampos := aClone(aCodReg[oCodReg:nAt,3])

If Len(aCampos) == 0
   Return
EndIf

cTitulo := PadR(aCampos[1,7],30)
cPicture := PadR(aCampos[1,8],30)
cTipoCpo := aCampos[1,9]
nDec     := aCampos[1,10]

DEFINE MSDIALOG oDlg TITLE "Conferencia do LayOut "+cCodLay+". Configuracao de campos" FROM 008,010 TO 040,100
oDlg:lEscClose := .F.

   @ 015,000 LISTBOX oCampos  FIELDS HEADERS "","Registro","Descricao" FIELDSIZES 10,40, 130  SIZE 220,225 OF oDlg PIXEL
   oCampos:SetArray(aCampos)
   oCampos:bLine := bLine
   oCampos:bLDblClick := {||aCampos[oCampos:nAt,1]:=!aCampos[oCampos:nAt,1],oCampos:SetArray(aCampos),oCampos:bLine := bLine,oCampos:Refresh()}
   oCampos:bKeyDown := {|nKey|If(nKey==32,Eval(oCampos:bLDblClick),.t.)}
   oCampos:bChange := {||SvVar(oCampos,aCampos,@nPosAnt,@cTitulo,@cPicture,@cTipoCpo,@nDec),oTitulo:Refresh(),oPicture:Refresh(),oTipoCpo:Refresh(),oDec:Refresh()}
   
   @ 017,230 Say "Titulo" Pixel Of oDlg
   @ 030,230 MsGet oTitulo Var cTitulo Size 120,10 Pixel Of oDlg
   
   @ 047,230 Say "Picture" Pixel Of oDlg
   @ 060,230 MsGet oPicture Var cPicture Size 120,10 Pixel Of oDlg
   
   @ 077,230 Say "Tipo Campo" Pixel Of oDlg
   @ 090,230 MsGet oTipoCpo Var cTipoCpo Size 20,10 Pixel Of oDlg
   
   @ 107,230 Say "Decimais" Pixel Of oDlg
   @ 120,230 MsGet oDec Var nDec Pict "9999" Size 30,10 Pixel Of oDlg

ACTIVATE DIALOG oDlg  CENTERED ON INIT EnchoiceBar(oDlg,{|| Eval(oCampos:bChange),lRet := .T.,oDlg:End()},{|| lRet := .f. , oDlg:End() })

If lRet
   aCodReg[oCodReg:nAt,3] := aClone(aCampos)
EndIf

Return


Static Function SvVar(oCampos,aCampos,nPosAnt,cTitulo,cPicture,cTipoCpo,nDec)
Local nPos := oCampos:nAt

aCampos[nPosAnt,7]  := cTitulo
aCampos[nPosAnt,8]  := cPicture
aCampos[nPosAnt,9]  := cTipoCpo
aCampos[nPosAnt,10] := nDec

cTitulo  := PadR(aCampos[nPos,7],30)
cPicture := PadR(aCampos[nPos,8],30)
cTipoCpo := aCampos[nPos,9]
nDec     := aCampos[nPos,10]

nPosAnt  := nPos

Return

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    �CriaSX1  � Autor � Eduardo Motta          � Data � 25.04.06 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Cria pergunte para este programa                           ����
�������������������������������������������������������������������������Ĵ���
���Sintaxe   � CriaSX1()                                                  ����
�������������������������������������������������������������������������Ĵ���
��� Uso      �                                                            ����
�������������������������������������������������������������������������Ĵ���
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function CriaSX1()
Local aRegs	:=	{}
LOCAL cPerg := "PLUEDI"

aadd(aRegs,{cPerg,"01","LayOut             ?"					,"","","mv_ch1","C",6	,0,0,"G",""			,"mv_par01",""				,"","","","",""					,"","","","",""					,"","","","",""				,"","","","","","","","",""	,""})											
aadd(aRegs,{cPerg,"02","Arquivo            ?"					,"","","mv_ch2","C",90,0,0,"G",""			,"mv_par02",""				,"","","","",""					,"","","","",""					,"","","","",""				,"","","","","","","","","BYRPLS"	,""})											

PlsVldPerg( aRegs )
     
Return




/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Funcao    �RetStartPat� Autor � Eduardo Motta         � Data � 26.12.05 ���
��������������������������������������������������������������������������Ĵ��
���Descricao � Retorna path do sigaadv                                     ���
��������������������������������������������������������������������������Ĵ��
���Sintaxe   �RetStartPath()                                               ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/
Static Function RetStartPath()
Local cStartPath := AllTrim(GetPvProfString( GetEnvServer(), "StartPath", "", GetADV97() ))
If SubStr(cStartPath,Len(cStartPath),1) == "\"
	cStartPath := SubStr(cStartPath,1,Len(cStartPath)-1)
EndIf

Return cStartPath
