#include "PROTHEUS.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PLULIB    �Autor  �Antonio de Padua    � Data �  04/26/02   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcoes Genericas Utilizadas no PTU Unimed                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP6                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PLSUTipTra�Autor  �Antonio de Padua    � Data �  04/26/02   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcoes Genericas Utilizadas no PTU Unimed                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Retorna o Tipo da Trnasacao                                ���
���          � do Arquivo de acordo com parametros                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                 

Function PLSUTipTra(Linha)

Local cTipoTran
Local nPosParam                     
Local nTamParam
  
  nPosParam := Val(Substr(GETMV("MV_PTUTRAN"),1,3))
  nTamParam := Val(Substr(GETMV("MV_PTUTRAN"),4,3))

  cTipoTran := Substr(Linha,nPosParam,nTamParam)

  
Return(cTipoTran)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PLSUVerTra�Autor  �Antonio de Padua    � Data �  04/26/02   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcoes Genericas Utilizadas no PTU Unimed                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Retorna a Versao da Transacao                              ���
���          � do Arquivo de acordo com parametros                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                 
Function PLSUVerTra(Linha)
Local cTipoTran
Local nPosParam                     
Local nTamParam
  
  nPosParam := Val(Substr(GETMV("MV_PTUVERS"),1,3))
  nTamParam := Val(Substr(GETMV("MV_PTUVERS"),4,3))

  cTipoTran := Substr(Linha,nPosParam,nTamParam)
  
Return(cTipoTran)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PLSUPCLAY �Autor  �Antonio de Padua    � Data �  04/26/02   ���
�������������������������������������������������������������������������͹��
���Desc.     �Valor do Codigo do Layout                                   ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Retorna o Codigo do Ultimo Layout                          ���
���          � observando o numero da transacao                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                            
//nTipo indica se o Layout OnLine ou OffLine
//  1 = OnLine
//  2 = OffLine   

// Este programa retorna um vetor com os dados principais do Layout
//   aRet[1]:= Volta o Codigo do Ultimo Layout
//   aRet[2]:= Tipo do Layout : 1=Simples e 2= Mestre/Detalhe 
//   aRet[3]:= Campo do Detalhe, caso seja Mestre Detalhe qual seria o campo que guarda a quantidade do detalhe

Function PLSUPCLAY(cNumTrans,nTipo,cVersao)

Default cVersao := "01"
   
   nOrd1 := BUJ->(IndexOrd())
   nNum1 := BUJ->(Recno())

   //������������������������������Ŀ
   //�Verifico o Codigo da Transacao�  
   //��������������������������������
   BUJ->(dbSetOrder(2))
   If ! BUJ->(DbSeek(xFilial("BUJ")+cNumTrans))
       Return
   endif         


if nTipo == 1
  //������������������������������������������Ŀ
  //�Guardo os indices e a posicao dos arquivos�  
  //��������������������������������������������
   nOrd2 := BU1->(IndexOrd())
   nNum2 := BU1->(Recno())
   
 
   //������������������������������Ŀ
   //�Pesquiso a versao escolhida   �  
   //��������������������������������
   BU1->(DbSetOrder(3))
   If BU1->(DbSeek(xFilial("BU1")+BUJ->BUJ_CODIGO+cVersao)) 
       aRet := {BU1->BU1_CODIGO, BU1->BU1_TIPLAY, BU1->BU1_QTDDET}
   Else
      //������������������������������Ŀ
      //�Pesquiso codigo do layout     �  
      //��������������������������������
      BU1->(DbSetOrder(2))
      If ! BU1->(DbSeek(xFilial("BU1")+BUJ->BUJ_CODIGO)) 
         Return
      Else                         
        //�������������������������������������������Ŀ
        //�Verifico o Ultimo Layout e se esta ativado �  
        //���������������������������������������������
        dData := BU1->BU1_DTATIV
        nTempRecno := BU1->(Recno())
        BU1->(DbSkip())               
        While ! BU1->(EOF()) .AND. BU1->BU1_CODIGO  == BUJ->BUJ_CODIGO
          if (dData < BU1->BU1_DTATIV) .And. (BU1->BU1_DTATIV < dDataBase)  
              nTempRecno := BU1->(Recno())                       
              dData := BU1->BU1_DTATIV
          Endif               
          BU1->(DbSkip())
        EndDo
        BU1->(DbGoTo(nTempRecno))
      Endif         
      aRet := {BU1->BU1_CODIGO, BU1->BU1_TIPLAY, BU1->BU1_QTDDET}
   Endif
      BU1->(DbGoTo(nNum2))
      BU1->(DbSetOrder(nOrd2))
Else
  //������������������������������������������Ŀ
  //�Guardo os indices e a posicao dos arquivos�  
  //��������������������������������������������
   
   nOrd2 := BUU->(IndexOrd())
   nNum2 := BUU->(Recno())
   
   BUU->(DbSetOrder(3))
   If BUU->(DbSeek(xFilial("BUU")+BUJ->BUJ_CODIGO+cVersao)) 
      aRet := {BUU->BUU_CODIGO,BUJ->BUJ_DESCRI}
   Else
      //������������������������������Ŀ
      //�Pesquiso codigo do layout     �  
      //��������������������������������
      BUU->(DbSetOrder(2))
      If ! BUU->(DbSeek(xFilial("BUU")+BUJ->BUJ_CODIGO)) 
         Return
      Else                         
        //�������������������������������������������Ŀ
        //�Verifico o Ultimo Layout e se esta ativado �  
        //���������������������������������������������
        dData := BUU->BUU_DTATIV
        nTempRecno := BUU->(Recno())
        BUU->(DbSkip())               
        While ! BUU->(EOF()) .AND. BUU->BUU_CODIGO  == BUJ->BUJ_CODIGO
          if (dData < BUU->BUU_DTATIV) .And. (BUU->BUU_DTATIV < dDataBase)  
              nTempRecno := BUU->(Recno())                       
              dData := BUU->BUU_DTATIV
          Endif               
          BUU->(DbSkip())
        EndDo
        BUU->(DbGoTo(nTempRecno))
      Endif         
      aRet := {BUU->BUU_CODIGO,BUJ->BUJ_DESCRI}
   Endif      

    BUU->(DbGoTo(nNum2))
    BUU->(DbSetOrder(nOrd2))
endif

  //�������������������������������������������Ŀ
  //�Retorno os indices e a posicao dos arquivos�  
  //���������������������������������������������
   BUJ->(DbGoTo(nNum1))
   BUJ->(DbSetOrder(nOrd1))
   
Return(aRet)
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PLSUGerVal�Autor  �Antonio de Padua    � Data �  02/06/02   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcoes Genericas Utilizadas no PTU Unimed                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Retorna o Valor do Campo que deve ser executado no EXEcBlock��
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                 

Function PLSUGerVal(cContCpo,cConteudo)
Private cResult
//����������������������������������������������������������Ŀ
//�Altero Varivel do sistema por conteudo que veio no arquivo�
//������������������������������������������������������������
  if AllTrim(cContCpo) > ""
     cContCpo := Upper(cContCpo)
     cContCpo := StrTran(cContCpo,"XPTU","'" + cConteudo + "'")
  Else
     cContCpo :=  "'" + cConteudo + "'"    
  Endif 
  
  cResult := &cContCpo
  
Return(cResult)
     
                 
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PLSUPegVal�Autor  �Antonio de Padua    � Data �  02/06/02   ���
�������������������������������������������������������������������������͹��
���Desc.     �Executa funcoes de dentro de variaveis                      ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �Executar o valor do campo                                   ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                 
Function PLSUPegVal(cConteudo)            
 Local Result
 
 Result := &cConteudo
 
Return(Result)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PLSUVerMLr�Autor  �Antonio de Padua    � Data �  02/06/02   ���
�������������������������������������������������������������������������͹��
���Desc.     �Verifica o campo de Mensagens Livres                        ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �Seta que o arquivo recebido tem mensagem livre              ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                 
Function PLSUVerMLr(cConteudo)            
  if Len(aMensLivres) = 0 
     aMensLivres := {}
  Endif
  if ! Empty(cConteudo)
    aadd(aMensLivres,cConteudo)
  Endif
Return(cConteudo)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PLSUExec  �Autor  �Antonio de Padua    � Data �  02/06/02   ���
�������������������������������������������������������������������������͹��
���Desc.     �Executa uma Funcao de Dentro de um Campo                    ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Comandos executados a partir de Conteudos do Arquivo Texto ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                 

Function PLSUExec(cContCpo,cConteudo)
//����������������������������������������������������������Ŀ
//�Altero Varivel do sistema por conteudo que veio no arquivo�
//������������������������������������������������������������
  if AllTrim(cContCpo) > ""
     cContCpo := Upper(cContCpo)
     cContCpo := StrTran(cContCpo,"XPTU","'" + cConteudo + "'")
  Else 
     cContCpo :=  "'" + cConteudo + "'" 
  endif                           

  &cContCpo
  
Return()

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PLSURetVar�Autor  �Antonio de Padua    � Data �  02/06/02   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcoes Genericas Utilizadas no PTU Unimed                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Joga no campo ou varivel definida nos parametros            ��
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                 

Function PLSURetVar(cContCpo,cConteudo)
//����������������������������������������������������������Ŀ
//�Altero Varivel do sistema por conteudo que veio no arquivo�
//������������������������������������������������������������
  if Empty(cContCpo) 
     Return
  Endif

  cContCpo := Upper(cContCpo)  
  cConteudo := StrTran(AllTRim(cConteudo),"'"," ")
  
  if ":=" $ cContCpo
     cContCpo := StrTran(AllTRim(cContCpo),"XPTU","'" + AllTrim(cConteudo) + "'")
  Else
     cContCpo :=  AllTrim(cContcpo)+":='"+AllTrim(cConteudo) + "'" 
  Endif 
  
Return(cContCpo)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PLSUTroVal�Autor  �Antonio de Padua    � Data �  02/06/02   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcoes Genericas Utilizadas no PTU Unimed                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Troca valor de variaveis de acordo com o Campo e o BU5      ��
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                 

Function PLSUTroVal(cConteudo)
Local cTemp  
cConteudo := Upper(cConteudo)

  //��������������������������������������������������������������������������Ŀ
  //� Pesquiso se devo alterar o Conteudo do Campo de Acordo com as tabelas... |
  //����������������������������������������������������������������������������
  BU5->(DbSetOrder(3))
  If BU5->(DbSeek(xFilial("BU5")+BUVTRB->BU2_CODIGO+cConteudo)) 
      cTemp := BU5->BU5_VLRSIS
  Endif                               
Return(cTemp)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PLUVerArq �Autor  �Antonio de Padua    � Data �  02/06/02   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcoes Genericas Utilizadas no PTU Unimed                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Retorna False se encontrou o registro pesquisado pelo campo ��
���          � de posicionamento do BU7                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                 
Function PLUVerArq(cArq)
           //���������������������������������������������������������������������Ŀ
           //� Pesquiso Arquivo a ser aberto...                                    �
           //�����������������������������������������������������������������������
           BU7->(DbSetOrder(3))
           If ! BU7->(DbSeek(xFilial("BU7")+aCodigoTrans[1]+cArq))
              TRBPOS->(DBCLoseArea())                               
              MsgStop("Arquivo nao pode ser aberto")
              Return
           Endif
           //���������������������������������������������������������������������Ŀ
           //� Pesquiso o registro, se existe eu altero senao incluo...            �
           //�����������������������������������������������������������������������
           if !Empty(BU7->BU7_POSICA)
              cExec := BU7->BU7_NOMARQ+"->(DbSetOrder("+Alltrim(Str(BU7->BU7_INDICE))+"))"
              &cExec
              cExec := BU7->BU7_NOMARQ+"->(DbSeek(xFilial('"+BU7->BU7_NOMARQ+"')+"+AllTrim(BU7->BU7_POSICA)+"))"
           Else
              cExec := ".F."
           Endif

          bTemp := !&cExec

          nTemp := aScan(vEstado,{|x|x[1]==BU7->BU7_NOMARQ})
          if nTemp > 0  
              vEstado[nTemp,2] := bTemp
          Else
              aadd(vEstado, {BU7->BU7_NOMARQ,bTemp})
          Endif

Return(bTemp)
                   
//Funcao contraria a dtos

Function PLSUstod(sData, ntam)
Local  cTemp
Default ntam := 8 

  sData := AllTrim(sData) 
 Do Case
	Case ntam == 6           
	   if SubStr(sData,7,2) > "0"
	      cTemp := "19"
	   Else 
	      cTemp := "20"
	   Endif          
	   cData := SubStr(sData,5,2)+"/"+SubStr(sData,3,2)+"/"+cTemp+SubStr(sData,1,2)
	Case ntam == 8 
	   cData := SubStr(sData,7,2)+"/"+SubStr(sData,5,2)+"/"+SubStr(sData,1,4)
	   
	EndCase

Return(ctod(cData))


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PLUEstTab �Autor  �Antonio de Padua    � Data �  05/07/02   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcoes Genericas Utilizadas no PTU Unimed                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Retorna 1 se a tabela esta em modo de Adicao e 2 se em modo ��
���          � de edicao e 0 se nao estiver setada                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                 
Function PLUEstTab(cAlias)
  nTemp := aScan(vEstado,{|x|x[1]==cAlias})
  if nTemp > 0  
     if vEstado[nTemp,2]
        nRet := 1 // Tabela em modo de Adicao
     Else
        nRet := 2  //Tabela em modo de Edicao
     Endif   
  Else
    nRet := 0 //Tabela nao foi setada para alteracoes, pelo sistema de importacao
  Endif
Return(nRet)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PLSUACanc �Autor  �Antonio de Padua    � Data �  05/07/02   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcoes Genericas Utilizadas no PTU Unimed                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Cancela ou Autoriza pela terceira perna uma autorizacao on  ��
���          � line                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                 
Function PLSUACanc(cTipo,cNumAut)
if 	cTipo == "00"
   Return
Elseif cTipo <> "04"
    BEA->(DbSetOrder(9))
    if BEA->(DbSeek(xFilial("BEA")+cNumAut))
       BE2->(DbSetOrder(1))
       If BE2->(DbSeek(xFilial("BEA")+BEA->(BEA_OPEMOV+BEA_ANOAUT+BEA_MESAUT+BEA_NUMAUT)))
          While !BE2->(EOF()) .And. BE2->(BE2_OPEMOV+BE2_ANOAUT+BE2_MESAUT+BE2_NUMAUT) == BEA->(BEA_OPEMOV+BEA_ANOAUT+BEA_MESAUT+BEA_NUMAUT)
              BE2->(RecLock("BE2",.F.))
              BE2->(DbDelete())
              BE2->(MsUnLock())
              BE2->(DbSkip())
          EndDo
       Endif
    Endif    
Endif
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PLSUAGrava�Autor  �Antonio de Padua    � Data �  17/07/02   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcoes Genericas Utilizadas no Import/Export               ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Da um MsUnlock no arquivo pedido pelo parametro gravando    ��
���          � o nome do arquivo importado                                ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                 
// Esta Funcao foi criada para gravar o campo generico IMPORT, para usar cancelamento de importacao
Function PLSUAGrava(cAlias)
  cExec:= AllTrim(cAlias+"->"+cAlias+"_IMPORT:='"+cNomeArq+"'")
  &cExec
  cExec:= cAlias+ "->(MSUNLOCK())"
  &cExec
Return

