
//#Include "TMSME05.CH"
#Include "PROTHEUS.CH"
#Include "plsmger.ch"
#define CRLF Chr(13)+Chr(10)


/*����������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Fun��o    �PLSUA500IMP� Autor � Eduardo de Motta      � Data � 25/06/03 ���
��������������������������������������������������������������������������Ĵ��
���Descri��o � EDI - Importacao                                            ���
��������������������������������������������������������������������������Ĵ��
���Sintaxe   � PLSUA502()                                                  ���
��������������������������������������������������������������������������Ĵ��
��� Uso      � SIGAPLS                                                     ���
��������������������������������������������������������������������������Ĵ��
���                  ATUALIZACOES - VIDE SOURCE SAFE                       ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
����������������������������������������������������������������������������*/
Function Plsua500Imp(cCodLay,cNomArq)
Local nI
Local cCodReg
Local nH
Local nJ
Local cCampo
Local cLinha
Private aCabDE0  := {}
Private aDadDE0  := {}
Private aTrabDE0 := {}
Private aCabDE1  := {}
Private aDadDE1  := {}
Private aTrabDE1 := {}
Private nPosReg    := 0
Private nPosRegDE0 := 0
Private nPosRegCp1 := 0
Private nPosRegCp2 := 0
Private nPosRegCp3 := 0
Private aHeader
Private aDados     := {}

Store Header "DE0" to aCabDE0 For ! AllTrim(SX3->X3_CAMPO) $ "DE0_DSCLAY/DE0_DSCREG/DE0_DSCRCP"
DbSelectArea("DE0")
DbSetOrder(1)
DbSeek(xFilial("DE0")+cCodLay)
Store COLS "DE0" TO aDadDE0 FROM aCabDE0 VETTRAB aTrabDE0 While xFilial("DE0") == DE0->DE0_FILIAL .and. cCodLay == DE0->DE0_CODLAY

Store Header "DE1" to aCabDE1 For .T.
aHeader := aClone(aCabDE1)  // MANTER COMPATIBILIDADE COM INICIALIZADORES PADRAO
DbSelectArea("DE1")
DbSetOrder(2)
DbSeek(xFilial("DE1")+cCodLay)
Store COLS "DE1" TO aDadDE1 FROM aCabDE1 VETTRAB aTrabDE1 While xFilial("DE1") == DE1->DE1_FILIAL .and. cCodLay == DE1->DE1_CODLAY

nPosReg    := GDFieldPos("DE1_CODREG",aCabDE1)
nPosRegDE0 := GDFieldPos("DE0_CODREG",aCabDE0)
nPosRegCp1 := GDFieldPos("DE0_CODRCP",aCabDE0)
nPosRegCp2 := GDFieldPos("DE0_CODRC2",aCabDE0)
nPosRegCp3 := GDFieldPos("DE0_CODRC3",aCabDE0)


FT_FUSE(cNomArq)
FT_FGOTOP()
While !FT_FEOF()
   cLinha := FT_FREADLN()
   For nI := 1 to Len(aDadDE0)
      cCodReg  := GDFieldGet("DE0_CODREG",nI,,aCabDE0,aDadDE0)
      PtuProcReg(cCodReg,nI,nH,cLinha)
   Next
   FT_FSKIP()
EndDo

FT_FUSE()

Return


Static Function PtuProcReg(cCodReg,nPosDE0,nH,cLinha)
Local cAlias
Local lPosic
Local cOrdem
Local cKey
Local cWhile
Local cValid
Local nLinDE0
Local nLinDE1
Local nPosDE1 := aScan(aDadDE1,{|x|x[nPosReg]==cCodReg } )
Local nCont   := 1
Local nJ
Local cCampo
Local cVal
Local aVar := {}
Local aVar2 := {}
Local nPos
Private cLinTXT := cLinha

If GDFieldGet("DE0_STATUS",nPosDE0,,aCabDE0,aDadDE0) # "1"  // SE ESTIVER DESATIVADO
   Return
EndIf

For nJ := 1 To Len(aCabDE0)
   cCampo := AllTrim(aCabDE0[nJ,2])
   PtuPutVar(cCampo,GDFieldGet(cCampo,nPosDE0,,aCabDE0,aDadDE0),aVar)
Next
cAlias := PtuGetVar("DE0_ALIAS",aVar)
lPosic := (PtuGetVar("DE0_POSICI",aVar)=="1")
cOrdem := PtuGetVar("DE0_POSORD",aVar)
cKey   := PtuGetVar("DE0_POSCHV",aVar)
cWhile := PtuGetVar("DE0_COND",aVar)
cValid := PtuGetVar("DE0_VALID",aVar)

If Empty(cWhile)
   cWhile := ".T."
EndIf
If Empty(cValid)
   cValid := ".T."
EndIf

If Empty(cAlias)
   Return .F.
EndIf
DbSelectArea(cAlias)
If Val(cOrdem) # 0
   DbSetOrder(Val(cOrdem))
Else
   DbSetOrder(Asc(cOrdem)-55)
EndIf
If !Empty(cKey)
   DbSeek(&cKey)
EndIf   
If ! &cValid
   Return .F.
EndIf
DbSelectArea(cAlias)
If Empty(cKey) .or. Eof()
   RecLock(cAlias,.T.)
   If SubStr(cAlias,1,1) # "S"
      cCampo := cAlias+"_FILIAL"
   Else
      cCampo := SubStr(cAlias,2)+"_FILIAL"
   EndIf   
   If FieldPos(cCampo) # 0
      &(cAlias+"->"+cCampo) := xFilial(cAlias)
   EndIf   
Else
   RecLock(cAlias,.F.)
EndIf

nLinDE1 := nPosDE1
While Len(aDadDE1) >= nLinDE1 .and. aDadDE1[nLinDE1,nPosReg] == cCodReg
   aVar2 := {}
   For nJ := 1 To Len(aCabDE1)
      cCampo := AllTrim(aCabDE1[nJ,2])
      PtuPutVar(cCampo,GDFieldGet(cCampo,nLinDE1,,aCabDE1,aDadDE1),aVar2)
   Next
   cCampo := AllTrim(PtuGetVar("DE1_CAMPO",aVar2))
   DbSelectArea(cAlias)
   nPos := FieldPos(cCampo)
   cVal := &(AllTrim(PtuGetVar("DE1_REGRA",aVar2)))
   PtuAddVar(cCodReg,cCampo,cVal)
   If nPos # 0
      FieldPut(nPos,cVal)
   EndIf   
   nLinDE1++
EndDo

Return


Static Function PtuGetVar(cCampo,aVar)
Local nPos
Local uRet
nPos := aScan(aVar,{|x|Upper(AllTrim(x[1]))==Upper(AllTrim(cCampo))})
If nPos # 0
   uRet := aVar[nPos,2]   
EndIf
Return uRet

Static Function PtuPutVar(cCampo,uVal,aVar)
Local nPos
Local uRet
nPos := aScan(aVar,{|x|Upper(AllTrim(x[1]))==Upper(AllTrim(cCampo))})
If nPos # 0
   aVar[nPos,2] := uVal
Else
   aadd(aVar,{cCampo,uVal})
EndIf
Return

Static Function PtuAddVar(cCodReg,cCampo,uVal)
nPos := aScan(aDados,{|x|x[3]+"/"+Upper(AllTrim(x[1]))==cCodReg+"/"+Upper(AllTrim(cCampo))})
If nPos#0
   aDados[nPos,2] := uVal
Else
   aadd(aDados,{cCampo,uVal,cCodReg})
EndIf
Return
/*
Static Function PtuVar(cCodReg,cCampo)
Local nPos
Local uRet
Local aVar := aDados
nPos := aScan(aVar,{|x|x[3]+"/"+Upper(AllTrim(x[1]))==cCodReg+"/"+Upper(AllTrim(cCampo))})
If nPos # 0
   uRet := aVar[nPos,2]   
EndIf

Return uRet
*/