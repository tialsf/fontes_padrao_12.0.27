#include "TOTVS.CH"
#include "autSysLog.CH"

/*/{Protheus.doc} hatSysLogger
    Funcao preparada para gravar um syslog da aplicacao
    @type  Function
    @author karine.limp
    @since 16/04/2018
    @params nFacility, nLevel, cMsg, cMsgId, cFunction, cArqLog, nVersion
    @return void, apenas gera o log no servidor
    /*/
Function hatSysLogger(nFacility, nLevel, cMsg, cMsgId, cFunction, cArqLog, nVersion)
    Local oCfgServer := getCfgSvr()
    Local oLogger := oCfgServer:getLogger()
    default cFunction := ProcName(1)
    
    if oLogger:active()
        // SmartJob("ProcGerLog", GetEnvServer(), .F., cFunction, nFacility, nLevel, cMsg, cMsgId)
        oLogger:cProcName := cFunction
        oLogger:logMessage(cMsg, cMsgId, nLevel, nFacility)
    endif
Return

Main Function ProcGerLog(cFunction, nFacility, nLevel, cMsg, cMsgId)
    Local oCfgServer := getCfgSvr()
    Local oLogger := oCfgServer:getLogger()
    
    oLogger:cProcName := cFunction
    oLogger:logMessage(cMsg, cMsgId, nLevel, nFacility)
Return
//