#INCLUDE "TOTVS.CH"
#INCLUDE "AutSysLog.CH"
#INCLUDE "hatActions.ch"

/*/{Protheus.doc} PlsHatSync
    @type  Function
    @author victor.silva
    @since 20190115
/*/
Main Function PlsHatSync(nSyncId,lAuto,cJsonAuto,cTypeCanc)
    Local oSyncHandler := nil
    Default lAuto      := .F.
    Default cJsonAuto  := ''
    Default cTypeCanc  := ''
    
    //Seta job para nao consumir licensas
   /* RpcSetType(3)
    RpcSetEnv( "T1", "M SP 01" )
    nSyncId:=SYNC_CLINICAL_ATTACHMENTS
    */
    do case
        case nSyncId == SYNC_AUTHORIZATIONS
            oSyncHandler := SyncAuthorizations():New()

            if lAuto
                oSyncHandler:lAuto     := .T.
                oSyncHandler:cJsonAuto := cJsonAuto
            endIf

            // Seta o tamanho da pagina
            oSyncHandler:setPage("1")
            oSyncHandler:setPageSize("50") 

            // Seta quais objetos devem ser expandidos
            oSyncHandler:addExpand("healthProvider")
            oSyncHandler:addExpand("professional")
            oSyncHandler:addExpand("procedures.rejectionCauses")
            oSyncHandler:addExpand("beneficiary")
            oSyncHandler:addExpand("medicalTeam")
            oSyncHandler:addExpand("requestedHospitalInfo")
            oSyncHandler:addExpand("authorizedHospitalInfo")
            oSyncHandler:addExpand("cbos")
            oSyncHandler:addExpand("sourceAuthorization")
            oSyncHandler:addExpand("rejectionCauses")
            oSyncHandler:addExpand("healthInsurance")

            // Seta a ordem
            oSyncHandler:addOrder("-authorizationId")

            // Pega s� o que n�o foi sincronizado
            oSyncHandler:addQueryParam({"trackingStatus", "0"}) 


            // Inicia a transacao
            if oSyncHandler:setupRequest()
                oSyncHandler:logInf("Sync com a API '" + oSyncHandler:name() + "' iniciada")
                oSyncHandler:syncAuth()
            else
                oSyncHandler:logInf("Sync com a API '" + oSyncHandler:name() + "' falhou")
            endif
        
        case nSyncId == SYNC_CANCELLATIONS
            
            //Autorizacao SADT/Internacao
            if Empty(cTypeCanc) .Or. cTypeCanc == "1"
                cancAuth(lAuto,cJsonAuto)
            endIf 
            
            //Anexos Clinicos
            if Empty(cTypeCanc) .Or. cTypeCanc == "2"
                cancAttach(lAuto,cJsonAuto)
            endIf

            //Prorrogacoes
            if Empty(cTypeCanc) .Or. cTypeCanc == "3"              
                cancTreat(lAuto,cJsonAuto)
            endIf

        case nSyncId == SYNC_CLINICAL_ATTACHMENTS
            oSyncHandler := SyncClinicalAttach():New()

            if lAuto
                oSyncHandler:lAuto     := .T.
                oSyncHandler:cJsonAuto := cJsonAuto
            endIf
            // Seta o tamanho da pagina
            oSyncHandler:setPage("1")
            oSyncHandler:setPageSize("50")

            // Seta quais objetos devem ser expandidos
            oSyncHandler:addExpand("healthProvider")
            oSyncHandler:addExpand("professional")
            oSyncHandler:addExpand("procedures.rejectionCauses")
            oSyncHandler:addExpand("beneficiary")
            oSyncHandler:addExpand("medicalTeam")
            oSyncHandler:addExpand("requestedHospitalInfo")
            oSyncHandler:addExpand("authorizedHospitalInfo")
            oSyncHandler:addExpand("cbos")
            oSyncHandler:addExpand("sourceAuthorization")
            oSyncHandler:addExpand("rejectionCauses")
            oSyncHandler:addExpand("healthInsurance")

            // Seta a ordem
            oSyncHandler:addOrder("-id")

            // Pega s� o que n�o foi sincronizado
            oSyncHandler:addQueryParam({"trackingStatus", "0"})

            // Inicia a transacao
            if oSyncHandler:setupRequest()
                oSyncHandler:logInf("Sync com a API '" + oSyncHandler:name() + "' iniciada")
                oSyncHandler:sync()
            else
                oSyncHandler:logInf("Sync com a API '" + oSyncHandler:name() + "' falhou")
            endif
        
        case nSyncId == SYNC_TREATMENT_EXTENSIONS
            oSyncHandler := SyncTreatmentExtensions():New()
            
            if lAuto
                oSyncHandler:lAuto     := .T.
                oSyncHandler:cJsonAuto := cJsonAuto
            endIf

            // Seta o tamanho da pagina
            oSyncHandler:setPage("1")
            oSyncHandler:setPageSize("50")

            // Seta quais objetos devem ser expandidos
            oSyncHandler:addExpand("healthProvider")
            oSyncHandler:addExpand("professional")
            oSyncHandler:addExpand("procedures.rejectionCauses")
            oSyncHandler:addExpand("beneficiary")
            oSyncHandler:addExpand("cbos")
            oSyncHandler:addExpand("rejectionCauses")
            oSyncHandler:addExpand("healthInsurance")

            // Seta a ordem
            oSyncHandler:addOrder("-id")

            // Pega s� o que n�o foi sincronizado
            oSyncHandler:addQueryParam({"trackingStatus", "0"})

            // Inicia a transacao
            if oSyncHandler:setupRequest()
                oSyncHandler:logInf("Sync com a API '" + oSyncHandler:name() + "' iniciada")
                oSyncHandler:sync()
            else
                oSyncHandler:logInf("Sync com a API '" + oSyncHandler:name() + "' falhou")
            endif

    end

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} cancAuth
    Cancelamento Consulta/SADT/Internacao

    @author  victor.silva /sakai
    @version P12
    @since   20190115
/*/
//------------------------------------------------------------------- 
Static Function cancAuth(lAuto,cJsonAuto)

    Local oSyncHandler := SyncAuthorizations():New()
    Default lAuto      := .F.
    Default cJsonAuto  := ''

    if lAuto
        oSyncHandler:lAuto     := .T.
        oSyncHandler:cJsonAuto := cJsonAuto
    endIf
            
    // Seta o tamanho da pagina
    oSyncHandler:setPage("1")
    oSyncHandler:setPageSize("50")

    // Seta quais objetos devem ser expandidos
    oSyncHandler:addExpand("healthProvider")
    oSyncHandler:addExpand("professional")
    oSyncHandler:addExpand("procedures.rejectionCauses")
    oSyncHandler:addExpand("beneficiary")
    oSyncHandler:addExpand("medicalTeam")
    oSyncHandler:addExpand("requestedHospitalInfo")
    oSyncHandler:addExpand("authorizedHospitalInfo")
    oSyncHandler:addExpand("cbos")
    oSyncHandler:addExpand("sourceAuthorization")
    oSyncHandler:addExpand("rejectionCauses")
    oSyncHandler:addExpand("healthInsurance")

    // Seta a ordem
    oSyncHandler:addOrder("-authorizationId")

    oSyncHandler:addQueryParam({"authStatus", "C"}) // Pega s� o que est� cancelado
    oSyncHandler:addQueryParam({"dateFrom"  , SubStr(dtos(dDatabase),1,4) + "-" + SubStr(dtos(dDatabase),5,2) + "-" + SubStr(dtos(dDatabase),7,2)}) 
    oSyncHandler:addQueryParam({"dateTo"    , SubStr(dtos(dDatabase),1,4) + "-" + SubStr(dtos(dDatabase),5,2) + "-" + SubStr(dtos(dDatabase),7,2)}) 

    // Inicia a transacao
    if oSyncHandler:setupRequest()
        oSyncHandler:logInf("Sync com a API '" + oSyncHandler:name() + "' iniciada")
        oSyncHandler:syncCancel()
    else
        oSyncHandler:logInf("Sync com a API '" + oSyncHandler:name() + "' falhou")
    endif

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} cancAttach
    Cancelamento Anexos Clinicos

    @author  sakai
    @version P12
    @since   20190115
/*/
//------------------------------------------------------------------- 
Static Function cancAttach(lAuto,cJsonAuto)

    Local oSyncHandler := SyncClinicalAttach():New()
    Default lAuto      := .F.
    Default cJsonAuto  := ''

    if lAuto
        oSyncHandler:lAuto     := .T.
        oSyncHandler:cJsonAuto := cJsonAuto
    endIf
    // Seta o tamanho da pagina
    oSyncHandler:setPage("1")
    oSyncHandler:setPageSize("50")

    // Seta quais objetos devem ser expandidos
    oSyncHandler:addExpand("healthProvider")
    oSyncHandler:addExpand("professional")
    oSyncHandler:addExpand("procedures.rejectionCauses")
    oSyncHandler:addExpand("beneficiary")
    oSyncHandler:addExpand("medicalTeam")
    oSyncHandler:addExpand("requestedHospitalInfo")
    oSyncHandler:addExpand("authorizedHospitalInfo")
    oSyncHandler:addExpand("cbos")
    oSyncHandler:addExpand("sourceAuthorization")
    oSyncHandler:addExpand("rejectionCauses")
    oSyncHandler:addExpand("healthInsurance")

    // Seta a ordem
    oSyncHandler:addOrder("-id")

    oSyncHandler:addQueryParam({"cancel", "1"}) // Pega s� o que est� cancelado
    oSyncHandler:addQueryParam({"requestedDate"  , SubStr(dtos(dDatabase),1,4) + "-" + SubStr(dtos(dDatabase),5,2) + "-" + SubStr(dtos(dDatabase),7,2)}) 
    
    // Inicia a transacao
    if oSyncHandler:setupRequest()
        oSyncHandler:logInf("Sync com a API '" + oSyncHandler:name() + "' iniciada")
        oSyncHandler:syncCancel()
    else
        oSyncHandler:logInf("Sync com a API '" + oSyncHandler:name() + "' falhou")
    endif

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} cancTreat
    Cancelamento Prorrogacao de Internacao

    @author  sakai
    @version P12
    @since   20190115
/*/
//------------------------------------------------------------------- 
Static Function cancTreat(lAuto,cJsonAuto)

    Local oSyncHandler := oSyncHandler := SyncTreatmentExtensions():New()
    Default lAuto      := .F.
    Default cJsonAuto  := ''
                
    if lAuto
        oSyncHandler:lAuto     := .T.
        oSyncHandler:cJsonAuto := cJsonAuto
    endIf

    // Seta o tamanho da pagina
    oSyncHandler:setPage("1")
    oSyncHandler:setPageSize("50")

    // Seta quais objetos devem ser expandidos
    oSyncHandler:addExpand("healthProvider")
    oSyncHandler:addExpand("professional")
    oSyncHandler:addExpand("procedures.rejectionCauses")
    oSyncHandler:addExpand("beneficiary")
    oSyncHandler:addExpand("cbos")
    oSyncHandler:addExpand("rejectionCauses")
    oSyncHandler:addExpand("healthInsurance")

    // Seta a ordem
    oSyncHandler:addOrder("-id")

    oSyncHandler:addQueryParam({"cancel", "1"}) // Pega s� o que est� cancelado
    oSyncHandler:addQueryParam({"requestedDate"  , SubStr(dtos(dDatabase),1,4) + "-" + SubStr(dtos(dDatabase),5,2) + "-" + SubStr(dtos(dDatabase),7,2)}) 
    
    // Inicia a transacao
    if oSyncHandler:setupRequest()
        oSyncHandler:logInf("Sync com a API '" + oSyncHandler:name() + "' iniciada")
        oSyncHandler:syncCancel()
    else
        oSyncHandler:logInf("Sync com a API '" + oSyncHandler:name() + "' falhou")
    endif

Return