#INCLUDE "TOTVS.CH"
#INCLUDE "TBICONN.CH"   
#INCLUDE "PROTHEUS.CH"
#include "PLSMGER.CH"
#INCLUDE "hatActions.ch"

#define __aCdCri187 {"573","Demanda por requerimento"}

/*/{Protheus.doc} SyncAuthorizations
    Synchronizer concreto que lida com a integracoes do endpoint Authorizations
    @type  Class
    @author victor.silva
    @since 20190115
/*/
Class SyncAuthorizations from SyncHandler

    Data hmFieldsCab
    Data hmFieldsIte
    Data hmFieldsCri
    Data hmFieldsPar
    Data hmFieldsB2Z
    
    // Interna��o grava BEA E BE2 - Uso o HmFieldsCab e HmFieldsIte pra essas tabelas
    Data hmFieldsBE4
    Data hmFieldsBEJ
    Data hmFieldsBEL
    Data hmFieldsB43
    
    Method New()
    
    Method syncProcedHist()
    Method syncAuth()
    Method statusByProc(oItem)
    Method posProf(cUF, cNumCR, cSigCR,cName,cCGC)
    Method persistAuth(oItem)
    Method cabConsulta(oItem)
    Method cabExame(oItem)
    Method cabOdonto(oItem)
    Method cabExecucao(oItem)
    Method cabInternacao(oItem)
    Method trackItem(cNumGUi)
    Method grvAuditoria(cNumGuia, cAuthType)
    Method initializeHMapFields()
    Method syncCancel()    
    Method cancelauth(oItem)

EndClass

Method New() Class SyncAuthorizations
    _Super:new(SYNC_AUTHORIZATIONS)
    self:cPath := "v1/authorizations"
Return self

Method statusByProc(oItem) Class SyncAuthorizations
		
	Local aProcedimentos := oItem["procedures"]
    Local nLenProced := len(aProcedimentos)
	Local lAudito := oItem["authorizationStatus"] == "6"
	Local cStatus := ""
	Local nI := 1
	Local nAut := 0
	Local nNeg := 0

	if lAudito	
		cStatus := "6"
	else
		For nI := 1 to nLenProced
            if aProcedimentos[nI]["status"] == 0 .or.;
                aProcedimentos[nI]["status"] == 2 
				nNeg++
			else
				nAut++
			endIf
		Next nI

		if nNeg == nLenProced
			cStatus := "3"
        elseif nAut == nLenProced
            cStatus := "1"
		else
			cStatus := "2"
		endif

	endIf

Return cStatus

Method posProf(cUF, cNumCR, cSigCR,cNome,cCGC) Class SyncAuthorizations
    Local cCodOpe := PLSINTPAD()

    cUF := PADR(AllTrim(cUF),tamSX3("BB0_ESTADO")[1])
    cNumCR := PADR(AllTrim(cNumCR),tamSX3("BB0_NUMCR")[1])
    cSigCR :=PADR(AllTrim(cSigCR),tamSX3("BB0_CODSIG")[1])

    BB0->(dbSetOrder(4))    //BB0_FILIAL+BB0_ESTADO+BB0_NUMCR+BB0_CODSIG+BB0_CODOPE
   
	If !Empty(cUf) .and. !BB0->( MsSeek( xFilial("BB0")+cUf+cNumCR+cSigCR+cCodOpe ) )
		BB0->(RecLock("BB0",.T.))
		BB0->BB0_FILIAL	:= xFilial("BB0")
		BB0->BB0_ESTADO	:= Upper(cUf)
		BB0->BB0_UF		:= Upper(cUf)
		BB0->BB0_NUMCR	:= cNumCR
		BB0->BB0_CODSIG	:= cSigCR
		BB0->BB0_CODOPE	:= cCodOpe
		BB0->BB0_NOME	:= cNome
		BB0->BB0_VINC	:= '2'
		BB0->BB0_CODIGO	:= GetSx8Num("BB0","BB0_CODIGO")
        BB0->BB0_CGC    := cCGC
		BB0->(MsUnlock())
        ConfirmSX8()

	EndIf
    
    BB0->(dbGoTop())
Return BB0->(msSeek(xFilial('BB0')+cUF+cNumCR+cSigCR)) 

Method syncAuth() Class SyncAuthorizations
    Local nAuthId := 0
    Local cAuthType :=          ""
    Local lHMapInitialized := .F.

    while self:hasNext() .And. self:getDataFromClient()    
        while self:hasItens()
            oItem := self:getNextItem()
            nAuthId := oItem["authorizationId"]
            cAuthType := oItem["authorizationType"]           

            if !lHMapInitialized 
                self:initializeHMapFields()
                lHMapInitialized := .T.
            endIf
            
            self:persistAuth(oItem) 
        enddo
    enddo
    
Return

Method persistAuth(oItem) Class  SyncAuthorizations
    Local cCodOpe               := PlsIntPad()
    Local cAuthType             := IIF('Exame - Exec' $ oItem["authorizationDescription"],'3',oItem["authorizationType"])
    Local cCodRDA               := oItem["healthProvider"]["healthProviderId"]
    Local cAuthNumber           := oItem["idOnHealthProvider"]
    Local nAuthId               := oItem["authorizationId"]
    Local cStatus               := oItem["authorizationStatus"]
    Local cStatusByProc         := self:statusByProc(oItem)
    Local lAudito               := cStatus == "6"
    Local cSubscriberId         := oItem["beneficiary"]["subscriberId"]
    Local dDataSolic            := Stod(StrTran(oItem["requestDate"], "-", ""))
    Local dDataAtend            := Stod(StrTran(oItem["authorizationDate"], "-", ""))
    Local nLenProced            := Len(oItem["procedures"])
    Local cCbos                 := oItem["professional"]["cbos"]["code"]
    Local cCodEsp               := Posicione('BAQ', 6, xFilial('BAQ')+cCbos,'BAQ_CODESP')
    Local cAnoAut               := substr(cAuthNumber, 5,4) 
    Local cMesAut               := substr(cAuthNumber, 9,2) 
    Local cNumAut               := substr(cAuthNumber, 11)
    Local cOpeUsr               := substr(cSubscriberId, 1,4) 
	Local cCodEmp               := substr(cSubscriberId, 5,4)
	Local cMatric               := substr(cSubscriberId, 9,6)
	Local cTipReg               := substr(cSubscriberId, 15,2)
	Local cDigito               := substr(cSubscriberId, 17)
    Local oProced               := nil
    Local oCritica              := nil
    Local oPartic               := nil
    Local dDataPro              := nil
    Local nProced               := 1
    Local nCritica              := 1
    Local nPartic               := 1
    Local nLenCritica           := 0
    Local nLenPartic            := 0
    Local cTipGuia              := ""
    Local cNumeroGui            := ""
    Local cCbosPrf              := ""
    Local cCodEspPrf            := ""
    Local cAliasCab             := "BEA"
    Local cAliasIte             := "BE2"    
    Local cAliasCri             := "BEG"
    Local hmFieldsCri           := nil

    Local aTabDup               := PlsBusTerDup(SuperGetMv("MV_TISSCAB", .F. ,"87"))
    Local cCodTab               := ""
    Local cCodProc              := ""
    Local cCodTabDePara         := ""
    Local cCodProcDePara        := ""

    Local cCodLdp := ""
    Local cCodPeg := ""
    Local aItensPac := {}
    Local nI        := 0
    Local cDesloc   := ""
    Local cEndLoc   := ""
    Local cTipPre   := ""
    Local cNumProto := oItem["attendanceProtocol"]
    Local lGerGui   := .T.
    Local cHorInt   := ''
    Local cHorAlt   := ''
    Local dDatInt
    Local dDatAlt
    

    if cAuthType == HAT_CONSULTA
        cTipGuia := "01"
        dDataPro := dDataAtend
    elseif cAuthType == HAT_EXAME
        cTipGuia := "02"
        dDataPro := dDataSolic
    elseif cAuthType == HAT_ODONTO
        cTipGuia := "02"
        dDataPro := dDataSolic
    elseif cAuthType == HAT_EXAME_EXECUCAO 
        cTipGuia := "02"
        dDataPro := dDataAtend
    elseif cAuthType == HAT_INTERNACAO
        cTipGuia  := "03"
        dDataPro  := dDataSolic
        cAliasCab := "BE4"
        cAliasIte := "BEJ"
        cAliasCri := "BEL"    
    endif

    //Se Consulta em Auditoria devo gravar como SADT (regra TISS)
    if cTipGuia == "01" .And. cStatus == "6"
        cTipGuia := "02"
    endIf
    
    //Verifica se a internacao ja existe
    if cAuthType == HAT_INTERNACAO
        BE4->(DbSetOrder(2)) //BE4_FILIAL+BE4_CODOPE+BE4_ANOINT+BE4_MESINT+BE4_NUMINT
        if BE4->(DbSeek(xFilial("BE4")+cAuthNumber))
            
            dDatInt := Stod(StrTran(oItem["hospitalizationDate"], "-", ""))
            cHorInt := oItem["hospitalizationHour"]

            dDatAlt := Stod(StrTran(oItem["dischargedDate"], "-", ""))
            cHorAlt := oItem["dischargedHour"]

            if !Empty(dDatInt) .And. Empty(BE4->BE4_DATPRO)
                PLSA92DtIn(.T.,dDatInt,cHorInt,.F.)
                lGerGui := .F.
            endIf
            
            if !Empty(dDatAlt) .And. Empty(BE4->BE4_DTALTA)
                PLSADtAlt(.F.,dDatAlt,cHorAlt, /* cMotALT*/ )
                lGerGui := .F.
            endIf
         
        endIf
    endIf

    // Verifica se a guia ja foi gravada alguma vez e pula o item
    BEA->(DbSetOrder(1)) //BEA_FILIAL+BEA_OPEMOV+BEA_ANOAUT+BEA_MESAUT+BEA_NUMAUT+DTOS(BEA_DATPRO)+BEA_HORPRO
    if BEA->(DbSeek(xFilial("BEA") + cAuthNumber))
        self:trackItem(cAuthNumber)
        lGerGui := .F.
    endif
    
    if lGerGui

        cCodLdp := PlsRetLdp(5)
        cCodPeg := PLSVRPEGOF(cCodOpe, cCodOpe, cCodRDA,  cAnoAut, cMesAut,;
                    cTipGuia, "1", "1", "1", cCodLdp, "1","2", , dDataPro, dDataBase,1 ,1 ,0, .F.)[1]
        cNumeroGui := PLSA500NUM(iif(cAliasCab == "BE4", "BE4", "BD5"), cCodOpe, cCodLdp, cCodPeg)                                            

        cSenha:= oItem["password"]

        Begin Transaction

            BEA->(dbSelectArea("BEA"))
            BEA->(dbAppend())
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_FILIAL"), xFilial("BEA")))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_STATUS"), cStatusByProc))

                iF cAuthType <> HAT_EXAME_EXECUCAO 
                    BEA->(FieldPut(self:hmFieldsCab:get("BEA_STALIB"), iif(cStatus == "6", "6", "1")))
                EndIf

            BEA->(FieldPut(self:hmFieldsCab:get("BEA_GUIACO"), '0')) 
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_GUIIMP"), '0')) 
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_CANCEL"), iif(cStatus == "9", "1", "0")))
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_AUDITO"), iif(cStatus == "6", "1", "0")))
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_TIPGUI"), cTipGuia))
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_TIPO"), iif(cAuthType == HAT_ODONTO, "4", substr(cTipGuia, 2))))
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_TPGRV"), "2"))
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_ORIMOV"), "6"))
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_TIPPAC"), "1"))
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_GUIPRE"), cAuthNumber))
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_DATPRO"), dDataPro))

                BEA->(FieldPut(self:hmFieldsCab:get("BEA_OPEPEG"), cCodOpe))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_CODLDP"), cCodLdp))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_DTDIGI"), dDataBase))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_HHDIGI"), StrTran(Time(),":","")))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_CODPEG"), cCodPeg))
                
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_OPEMOV"), cCodOpe))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_ANOAUT"), cAnoAut))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_MESAUT"), cMesAut))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_NUMAUT"), cNumAut))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_NUMGUI"), cNumeroGui))

                BEA->(FieldPut(self:hmFieldsCab:get("BEA_SENHA"),  cSenha ))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_VALSEN"), Stod(StrTran(oItem["passwordExpireDate"], "-", ""))))

                BEA->(FieldPut(self:hmFieldsCab:get("BEA_OPEUSR"), cOpeUsr))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_CODEMP"), cCodEmp))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_MATRIC"), cMatric))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_TIPREG"), cTipReg))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_DIGITO"), cDigito))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_ATERNA"), iif(oItem["newbornAttendance"], "1", "0")))

                BEA->(FieldPut(self:hmFieldsCab:get("BEA_OPERDA"), cCodOpe))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_CODRDA"), cCodRDA))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_NOMRDA"), oItem["healthProvider"]["name"]))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_PROATE"), cNumProto))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_STTISS"), PLSANLSTIG(oItem["authorizationStatus"])) )

                BAU->(dbSetOrder(1))    
                    
                if BAU->( MsSeek( xFilial("BB0")+cCodRDA ) )       
                    BEA->(FieldPut(self:hmFieldsCab:get("BEA_TIPPRE"), BAU->BAU_TIPPRE))
                    cTipPre:= BAU->BAU_TIPPRE 
                endif

                // TODO: Quando o HAT mandar o CNES na API, posicionar o Local pelo CNES
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_CODLOC"), oItem["locationCode"]))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_LOCAL"), oItem["attendanceLocation"]))
    
                BB8->(dbSetOrder(1))

                iF BB8->( MsSeek( xFilial("BB8")+cCodRDA+cCodOpe+oItem["locationCode"]+oItem["attendanceLocation"] ) )
                    cDesloc:= ALLTRIM(BB8->BB8_DESLOC)
                    cEndLoc:=AllTrim(BB8->BB8_END)+"+"+AllTrim(BB8->BB8_NR_END)+"-"+AllTrim(BB8->BB8_COMEND)+"-"+AllTrim(BB8->BB8_BAIRRO)
                    BEA->(FieldPut(self:hmFieldsCab:get("BEA_DESLOC"),cDesloc))
                    BEA->(FieldPut(self:hmFieldsCab:get("BEA_ENDLOC"),cEndLoc))
                EndIf

                BEA->(FieldPut(self:hmFieldsCab:get("BEA_OPESOL"), PLSINTPAD()))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_HORPRO"), substr(StrTran(Time(),":",""),1,4)))  

                BA1->(dbSetOrder(2))    //BA1_FILIAL+BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG+BA1_DIGITO                                                                                               

                If BA1->( MsSeek( xFilial("BA1")+cOpeUsr+cCodEmp+cMatric+cTipReg+cDigito ) )
                    BEA->(FieldPut(self:hmFieldsCab:get("BEA_NOMUSR"), BA1->BA1_NOMUSR))
                    BEA->(FieldPut(self:hmFieldsCab:get("BEA_CONEMP"), BA1->BA1_CONEMP))
                    BEA->(FieldPut(self:hmFieldsCab:get("BEA_VERCON"), BA1->BA1_VERCON))
                    BEA->(FieldPut(self:hmFieldsCab:get("BEA_SUBCON"), BA1->BA1_SUBCON))
                    BEA->(FieldPut(self:hmFieldsCab:get("BEA_VERSUB"), BA1->BA1_VERSUB))
                    BEA->(FieldPut(self:hmFieldsCab:get("BEA_MATVID"), BA1->BA1_MATVID))
                    BEA->(FieldPut(self:hmFieldsCab:get("BEA_CPFUSR"), BA1->BA1_CPFUSR))
                EndIf

                iF !Empty(cNumProto) .and. cStatus $  "2|6|3"
                    gerRegB00(cNumProto,,cAliasCab,.T.,.f.,cOpeUsr+cCodEmp+cMatric+cTipReg+cDigito,.f.,,,,,cCodOpe,,,,,,,,.T.,.f.,cAuthNumber)
                    P773AutCon(cAliasCab,cNumProto,cCodOpe+cAnoAut+cMesAut+cNumAut)
                endif

            if cAuthType == HAT_CONSULTA
                self:cabConsulta(oItem)
            elseif cAuthType == HAT_EXAME 
                self:cabExame(oItem)
            elseif cAuthType == HAT_ODONTO 
                self:cabOdonto(oItem)
            elseif cAuthType == HAT_EXAME_EXECUCAO
                self:cabExecucao(oItem)
            elseif cAuthType == HAT_INTERNACAO
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_OPEINT"), cCodOpe))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_ANOINT"), cAnoAut))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_MESINT"), cMesAut))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_NUMINT"), cNumAut))
                BEA->(FieldPut(self:hmFieldsCab:get("BEA_LIBERA"), "0"))
                self:cabInternacao(oItem, "BEA")
            endif

            BEA->(dbcommit())
        
            if cAuthType == HAT_INTERNACAO

                BE4->(dbSelectArea("BE4"))
                BE4->(dbAppend())
                
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_FILIAL"), xFilial("BE4")))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_STATUS"), cStatusByProc))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_CANCEL"), iif(cStatus == "9", "1", "0")))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_AUDITO"), iif(cStatus == "6", "1", "0")))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_TIPGUI"), cTipGuia))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_TPGRV"), "2"))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_ORIMOV"), "6"))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_GUIPRE"), cAuthNumber))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_DATPRO"), Stod(StrTran(oItem["hospitalizationDate"], "-", ""))))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_HORPRO"), oItem["hospitalizationHour"], "-", ""))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_DTALTA"), Stod(StrTran(oItem["dischargedDate"], "-", ""))))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_HRALTA"), oItem["dischargedHour"], "-", ""))


                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_CODLDP"), cCodLdp))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_DTDIGI"), dDataSolic))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_HHDIGI"), StrTran(Time(),":","")))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_CODPEG"), cCodPeg))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_GUIIMP"), '0'))

                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_CODOPE"), cCodOpe))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_ANOINT"), cAnoAut))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_MESINT"), cMesAut))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_NUMINT"), cNumAut))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_NUMERO"), cNumeroGui))

                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_SENHA"), cSenha))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_DATVAL"), Stod(StrTran(oItem["passwordExpireDate"], "-", ""))))

                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_OPEUSR"), cOpeUsr))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_CODEMP"), cCodEmp))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_MATRIC"), cMatric))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_TIPREG"), cTipReg))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_DIGITO"), cDigito))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_ATERNA"), iif(oItem["newbornAttendance"], "1", "0")))

                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_OPERDA"), cCodOpe))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_CODRDA"), cCodRDA))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_NOMRDA"), oItem["healthProvider"]["name"]))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_TIPPRE"), cTipPre))

                    // TODO: Quando o HAT mandar o CNES na API, posicionar o Local pelo CNES
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_CODLOC"),  oItem["locationCode"]   ))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_LOCAL"),  oItem["attendanceLocation"] ))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_PROATE"),  cNumProto))
                    BE4->(FieldPut(self:hmFieldsBE4:get("BE4_STTISS"), PLSANLSTIG(oItem["authorizationStatus"])) )

                    self:cabInternacao(oItem, "BE4")

                BE4->(dbcommit())

            endIf

            while nProced <= nLenProced
                
                BE2->(dbSelectArea("BE2"))
                
                oProced := oItem["procedures"][nProced]
                cCodTab := oProced["tableCode"]
                cCodProc := oProced["procedureCode"]
                cCodTabDePara := AllTrim(PLSVARVINC('87','BR4',cCodTab))
                cCodProcDePara := AllTrim(PLSVARVINC(cCodTab,'BR8', cCodProc, cCodTabDePara+cCodProc,,aTabDup,@cCodTabDePara))
                
                BE2->(dbAppend())
                    
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_FILIAL"), xFilial("BE2")))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_STATUS"), iif(oProced["status"] <> 2, cvaltochar(oProced["status"]), "0")))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_SEQUEN"), strzero(nProced, 3)))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_CODPAD"), cCodTabDePara))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_CODPRO"), cCodProcDePara))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_DESPRO"), DECODEUTF8(oProced["procedureDescription"])))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_QTDSOL"), oProced["requestedQuantity"]))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_TIPPRE"), cTipPre))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_NIVCRI"), iif(oProced["status"] <> 1,"HAT","")))

                    If oProced["authorizedQuantity"] == 0 .And. oProced["status"] == 2
                        BE2->(FieldPut(self:hmFieldsIte:get("BE2_QTDPRO"), oProced["requestedQuantity"]))
                    Else
                        BE2->(FieldPut(self:hmFieldsIte:get("BE2_QTDPRO"), oProced["authorizedQuantity"]))
                    EndIf

                    iF oProced["status"] == 1 
                        BE2->(FieldPut(self:hmFieldsIte:get("BE2_SALDO"), IIF(cAuthType <> HAT_EXAME_EXECUCAO, oProced["authorizedQuantity"],0)))
                    endif

                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_DATPRO"), dDataPro))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_AUDITO"), iif(oProced["status"] == 2, "1", "0")))
                    
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_OPEMOV"), cCodOpe))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_ANOAUT"), cAnoAut))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_MESAUT"), cMesAut))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_NUMAUT"), cNumAut))

                    if cAuthType == HAT_INTERNACAO
                        BE2->(FieldPut(self:hmFieldsIte:get("BE2_ANOINT"), cAnoAut))
                        BE2->(FieldPut(self:hmFieldsIte:get("BE2_MESINT"), cMesAut))
                        BE2->(FieldPut(self:hmFieldsIte:get("BE2_NUMINT"), cNumAut))
                    endIf

                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_TIPGUI"), cTipGuia))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_TIPO")  , substr(cTipGuia, 2)))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_TPGRV") , "2"))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_OPERDA"), cCodOpe))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_CODRDA"), cCodRDA))

                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_CODLOC"), oItem["locationCode"]))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_LOCAL") , oItem["attendanceLocation"]))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_ENDLOC"), cEndLoc))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_DESLOC"), cDesloc))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_CODESP"), cCodEsp))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_OPEUSR"), cOpeUsr))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_CODEMP"), cCodEmp))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_MATRIC"), cMatric))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_TIPREG"), cTipReg))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_DIGITO"), cDigito))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_CODLDP"), cCodLdp))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_CODPEG"), cCodPeg))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_NUMERO"), cNumeroGui))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_CONEMP"), BA1->BA1_CONEMP))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_VERCON"), BA1->BA1_VERCON))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_SUBCON"), BA1->BA1_SUBCON))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_VERSUB"), BA1->BA1_VERSUB))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_MATVID"), BA1->BA1_MATVID))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_NOMUSR"), BA1->BA1_NOMUSR))
                    BE2->(FieldPut(self:hmFieldsIte:get("BE2_HORPRO"), substr(StrTran(Time(),":",""),1,4)))      

                    if !empty(oProced["toothRegion"])
                        // TODO: Depara de dentes - 28 / regi�es - 42 / faces - 32
                        BE2->(FieldPut(self:hmFieldsIte:get("BE2_DENREG"), oProced["toothRegion"]))
                        BE2->(FieldPut(self:hmFieldsIte:get("BE2_FADENT"), oProced["surfaces"]))
                    endif

                    if cAuthType == HAT_EXAME_EXECUCAO
                        BE2->(FieldPut(self:hmFieldsIte:get("BE2_HORPRO"), substr(StrTran(Time(),":",""),1,4)))
                        BE2->(FieldPut(self:hmFieldsIte:get("BE2_HORFIM"), substr(StrTran(Time(),":",""),1,4)))
                        BE2->(FieldPut(self:hmFieldsIte:get("BE2_VIA")   , alltrim(PLSVARVINC('61', nil, oProced["accessWay"]))))
                        BE2->(FieldPut(self:hmFieldsIte:get("BE2_TECUTI"), alltrim(PLSVARVINC('48', nil, oItem["usedTechnique"]))))
                        BE2->(FieldPut(self:hmFieldsIte:get("BE2_PRPRRL"), iif(empty(oProced["increaseDecrease"]), 1, oProced["increaseDecrease"])))
                    endIf

                    if cAuthType == HAT_EXAME_EXECUCAO .OR. cAuthType == HAT_CONSULTA
                        BE2->(FieldPut(self:hmFieldsIte:get("BE2_VLRAPR"), iif(empty(oProced["unitaryWorth"]), 0, oProced["unitaryWorth"])))
                        BE2->(FieldPut(self:hmFieldsIte:get("BE2_LIBERA"), "0"))
                    else
                        BE2->(FieldPut(self:hmFieldsIte:get("BE2_LIBERA"), "1"))
                    endIf

                BE2->(dbcommit())

                B2Z->(dbSelectArea("B2Z"))
                B2Z->(dbAppend())
                    B2Z->(FieldPut(self:hmFieldsB2Z:get("B2Z_FILIAL"), xFilial("B2Z")))
                    B2Z->(FieldPut(self:hmFieldsB2Z:get("B2Z_IDORIG"), cValToChar(nAuthId)))
                    B2Z->(FieldPut(self:hmFieldsB2Z:get("B2Z_OPEMOV"), cCodOpe))
                    B2Z->(FieldPut(self:hmFieldsB2Z:get("B2Z_TIPGUI"), cTipGuia))
                    B2Z->(FieldPut(self:hmFieldsB2Z:get("B2Z_CODRDA"), cCodRDA))
                    B2Z->(FieldPut(self:hmFieldsB2Z:get("B2Z_SENHA" ), cSenha))
                    B2Z->(FieldPut(self:hmFieldsB2Z:get("B2Z_NUMAUT"), oItem["idOnHealthProvider"]))
                    B2Z->(FieldPut(self:hmFieldsB2Z:get("B2Z_MATRIC"), cSubscriberId))
                    B2Z->(FieldPut(self:hmFieldsB2Z:get("B2Z_DATPRO"), dtos(dDataPro)))
                    B2Z->(FieldPut(self:hmFieldsB2Z:get("B2Z_SEQUEN"), strzero(nProced, 3)))
                    B2Z->(FieldPut(self:hmFieldsB2Z:get("B2Z_CODPAD"), cCodTabDePara))
                    B2Z->(FieldPut(self:hmFieldsB2Z:get("B2Z_CODPRO"), cCodProcDePara))
                    B2Z->(FieldPut(self:hmFieldsB2Z:get("B2Z_QTDAUT"), oProced["authorizedQuantity"]) )
                    B2Z->(FieldPut(self:hmFieldsB2Z:get("B2Z_SALDO" ), IIF(cAuthType <> HAT_EXAME_EXECUCAO, oProced["authorizedQuantity"],0)))
                    if !empty(oProced["toothRegion"])
                        B2Z->(FieldPut(self:hmFieldsB2Z:get("B2Z_DENREG"), oProced["toothRegion"]) )
                        B2Z->(FieldPut(self:hmFieldsB2Z:get("B2Z_FADENT"), oProced["surfaces"]) )
                    endif
                
                B2Z->(dbcommit())
            
                if cAuthType == HAT_EXAME_EXECUCAO .And. oProced["status"] == 1 //autorizado controla saldo

                    PLSAtuLib(oItem['mainAuthorizationCode'],strzero(nProced, 3),cCodTabDePara,cCodProcDePara,oProced["authorizedQuantity"],{},.F.,.F.,.F., 0, "", "")
    
                endif

                if cAuthType == HAT_INTERNACAO
                
                    BEJ->(dbSelectArea("BEJ"))
                    BEJ->(dbAppend())
                                    
                        BEJ->(FieldPut(self:hmFieldsBEJ:get("BEJ_FILIAL"), xFilial("BEJ")))
                        BEJ->(FieldPut(self:hmFieldsBEJ:get("BEJ_STATUS"), iif(oProced["status"] <> 2, cvaltochar(oProced["status"]), "0")))
                        BEJ->(FieldPut(self:hmFieldsBEJ:get("BEJ_SEQUEN"), strzero(nProced, 3)))
                        BEJ->(FieldPut(self:hmFieldsBEJ:get("BEJ_CODPAD"), cCodTabDePara))
                        BEJ->(FieldPut(self:hmFieldsBEJ:get("BEJ_CODPRO"), cCodProcDePara))
                        BEJ->(FieldPut(self:hmFieldsBEJ:get("BEJ_DESPRO"), DECODEUTF8(oProced["procedureDescription"])))
                        BEJ->(FieldPut(self:hmFieldsBEJ:get("BEJ_QTDSOL"), oProced["requestedQuantity"]))

                        BEJ->(FieldPut(self:hmFieldsBEJ:get("BEJ_NIVCRI"), iif(oProced["status"] <> 1,"HAT","")))
    
                        If oProced["authorizedQuantity"] == 0 .And. oProced["status"] == 2
                            BEJ->(FieldPut(self:hmFieldsBEJ:get("BEJ_QTDPRO"), oProced["requestedQuantity"]))
                        Else
                            BEJ->(FieldPut(self:hmFieldsBEJ:get("BEJ_QTDPRO"), oProced["authorizedQuantity"]))
    
                        EndIf

                        BEJ->(FieldPut(self:hmFieldsBEJ:get("BEJ_DATPRO"), dDataPro))
                        BEJ->(FieldPut(self:hmFieldsBEJ:get("BEJ_AUDITO"), iif(oProced["status"] == 2, "1", "0")))

                        BEJ->(FieldPut(self:hmFieldsBEJ:get("BEJ_CODOPE"), cCodOpe))
                        BEJ->(FieldPut(self:hmFieldsBEJ:get("BEJ_ANOINT"), cAnoAut))
                        BEJ->(FieldPut(self:hmFieldsBEJ:get("BEJ_MESINT"), cMesAut))
                        BEJ->(FieldPut(self:hmFieldsBEJ:get("BEJ_NUMINT"), cNumAut))
                        BEJ->(FieldPut(self:hmFieldsBEJ:get("BEJ_ESPSOL"), cCodEsp))

                    BEJ->(dbcommit())

                EndIf
                
                (cAliasCri)->(dbSelectArea(cAliasCri))
                nLenCritica := iif(!empty(oItem["procedures"][nProced]["rejectionCauses"]),;
                    Len(oItem["procedures"][nProced]["rejectionCauses"]), 0)
                hmFieldsCri := iif(cAliasCri == "BEG", self:hmFieldsCri, self:hmFieldsBEL)

                BCT->(dbSetOrder(1))
                
                while nCritica <= nLenCritica
                    oCritica := oItem["procedures"][nProced]["rejectionCauses"][nCritica]
                    
                    BCT->(dbGoTop())
                    if BCT->(msSeek(xFilial('BCT')+cCodOpe+oCritica["code"]))
                    
                        (cAliasCri)->(dbAppend())
                            (cAliasCri)->(FieldPut(hmFieldsCri:get(cAliasCri + "_FILIAL"), xFilial(cAliasCri)))

                            if cAuthType <> HAT_INTERNACAO
                                (cAliasCri)->(FieldPut(hmFieldsCri:get(cAliasCri + "_OPEMOV"), cCodOpe))
                                (cAliasCri)->(FieldPut(hmFieldsCri:get(cAliasCri + "_ANOAUT"), cAnoAut))
                                (cAliasCri)->(FieldPut(hmFieldsCri:get(cAliasCri + "_MESAUT"), cMesAut))
                                (cAliasCri)->(FieldPut(hmFieldsCri:get(cAliasCri + "_NUMAUT"), cNumAut))
                            else
                                (cAliasCri)->(FieldPut(hmFieldsCri:get(cAliasCri + "_CODOPE"), cCodOpe))
                                (cAliasCri)->(FieldPut(hmFieldsCri:get(cAliasCri + "_ANOINT"), cAnoAut))
                                (cAliasCri)->(FieldPut(hmFieldsCri:get(cAliasCri + "_MESINT"), cMesAut))
                                (cAliasCri)->(FieldPut(hmFieldsCri:get(cAliasCri + "_NUMINT"), cNumAut))
                            endIf

                            (cAliasCri)->(FieldPut(hmFieldsCri:get(cAliasCri + "_SEQUEN"), strzero(nProced, 3)))
                            (cAliasCri)->(FieldPut(hmFieldsCri:get(cAliasCri + "_CODGLO"), oCritica["code"]))
                            (cAliasCri)->(FieldPut(hmFieldsCri:get(cAliasCri + "_DESGLO"), BCT->BCT_DESCRI))
                            (cAliasCri)->(FieldPut(hmFieldsCri:get(cAliasCri + "_SEQCRI"), strzero(nCritica, 3)))
                            (cAliasCri)->(FieldPut(hmFieldsCri:get(cAliasCri + "_TIPO"), BCT->BCT_TIPO))
                        (cAliasCri)->(dbcommit())

                    endIf

                    nCritica++
                
                endDo

                if cAuthType == HAT_EXAME_EXECUCAO
                
                    nLenPartic := Len(oItem["procedures"][nProced]["medicalTeam"])
                    B4B->(dbSelectArea("B4B"))
                                        
                    while nPartic <= nLenPartic
                        
                        oPartic := oItem["procedures"][nProced]["medicalTeam"][nPartic]
                        cCbosPrf := oPartic["professional"]["cbos"]["code"] 
                        cCodEspPrf := Posicione('BAQ', 6, xFilial('BAQ')+cCbosPrf,'BAQ_CODESP')  
                        
                        if self:posProf(oPartic["professional"]["stateAbbreviation"],;
                            oPartic["professional"]["professionalCouncilNumber"],;
                            oPartic["professional"]["professionalCouncil"],oPartic["professional"]["name"],oPartic["professional"]["professionalIdentifier"])  
                        
                            B4B->(dbAppend())
                                B4B->(FieldPut(self:hmFieldsPar:get("B4B_FILIAL"), xFilial("B4B")))
                                B4B->(FieldPut(self:hmFieldsPar:get("B4B_OPEMOV"), cCodOpe))
                                B4B->(FieldPut(self:hmFieldsPar:get("B4B_ANOAUT"), cAnoAut))
                                B4B->(FieldPut(self:hmFieldsPar:get("B4B_MESAUT"), cMesAut))
                                B4B->(FieldPut(self:hmFieldsPar:get("B4B_NUMAUT"), cNumAut))
                                B4B->(FieldPut(self:hmFieldsPar:get("B4B_SEQUEN"), strzero(nProced, 3)))
                                B4B->(FieldPut(self:hmFieldsPar:get("B4B_GRAUPA"), oPartic["participationDegree"]))
                                B4B->(FieldPut(self:hmFieldsPar:get("B4B_CDPFPR"), BB0->BB0_CODIGO))
                                B4B->(FieldPut(self:hmFieldsPar:get("B4B_CGC")   , BB0->BB0_CGC))
                                B4B->(FieldPut(self:hmFieldsPar:get("B4B_SICONS"), oPartic["professional"]["professionalCouncil"]))
                                B4B->(FieldPut(self:hmFieldsPar:get("B4B_NUCONS"), oPartic["professional"]["professionalCouncilNumber"]))
                                B4B->(FieldPut(self:hmFieldsPar:get("B4B_UFCONS"), oPartic["professional"]["stateAbbreviation"]))
                                B4B->(FieldPut(self:hmFieldsPar:get("B4B_CODESP"), cCodEspPrf))
                            B4B->(dbcommit())

                        endIf
                                
                        nPartic++
                    
                    endDo

                endIf

                If Alltrim(cCodTab) $ "90/98"     // tratamento pacote

                    aItensPac := PlRetPac(cCodOpe, cCodRDA,cCodTabDePara,cCodProcDePara ,,dDataPro,.F.)

                    If Len(aItensPac)>0
                        For nI:=1 To Len(aItensPac)

                        B43->(dbAppend())

                            B43->(FieldPut(self:hmFieldsB43:get("B43_FILIAL"), xFilial("B43")))
                            B43->(FieldPut(self:hmFieldsB43:get("B43_OPEMOV"), cCodOpe))
                            B43->(FieldPut(self:hmFieldsB43:get("B43_ANOAUT"), cAnoAut))
                            B43->(FieldPut(self:hmFieldsB43:get("B43_MESAUT"), cMesAut))
                            B43->(FieldPut(self:hmFieldsB43:get("B43_NUMAUT"), cNumAut))
                            B43->(FieldPut(self:hmFieldsB43:get("B43_SEQUEN"), strzero(nProced, 3)))
                            B43->(FieldPut(self:hmFieldsB43:get("B43_CODOPE"), iif(cStatus == "3", "", cCodOpe)))
                            B43->(FieldPut(self:hmFieldsB43:get("B43_CODLDP"), iif(cStatus == "3", "", cCodLdp)))
                            B43->(FieldPut(self:hmFieldsB43:get("B43_CODPEG"), iif(cStatus == "3", "", cCodPeg)))
                            B43->(FieldPut(self:hmFieldsB43:get("B43_NUMERO"), iif(cStatus == "3", "", cNumeroGui)))
                            B43->(FieldPut(self:hmFieldsB43:get("B43_CODPAD"), aItensPac[nI,1]))
                            B43->(FieldPut(self:hmFieldsB43:get("B43_CODPRO"), aItensPac[nI,2]))
                            B43->(FieldPut(self:hmFieldsB43:get("B43_DESPRO"), POSICIONE('BR8',1,xFilial('BR8') + ALLTRIM(aItensPac[nI,1]) + ALLTRIM(aItensPac[nI,2]),'BR8_DESCRI')))
                            B43->(FieldPut(self:hmFieldsB43:get("B43_VALCH") , aItensPac[nI,4]))
                            B43->(FieldPut(self:hmFieldsB43:get("B43_VALFIX"), aItensPac[nI,5]))
                            B43->(FieldPut(self:hmFieldsB43:get("B43_PRINCI"), aItensPac[nI,6]))
                            B43->(FieldPut(self:hmFieldsB43:get("B43_TIPO")  , aItensPac[nI,3]))
                            B43->(FieldPut(self:hmFieldsB43:get("B43_ORIMOV"), '6'))
                            B43->(FieldPut(self:hmFieldsB43:get("B43_NIVPAC"), aItensPac[nI,10]))
                        B43->(dbcommit())

                        Next nI
                    EndIf
                EndIf
                nPartic := 1
                nCritica := 1
                nProced++
            endDo

            if lAudito

                self:grvAuditoria(cAuthNumber, cAuthType)

            endIf

        End Transaction
    endIf

    BEA->(dbCloseArea())
    BE2->(dbCloseArea())
    
    if Select("BE4") >  0
        BE4->(dbCloseArea())
    endIf

    if Select("BEJ") >  0
        BEJ->(dbCloseArea())
    endIf

    if Select("B4B") >  0
        B4B->(dbCloseArea())
    endIf
 
    if Select("B43") >  0
        B43->(dbCloseArea())
    endIf
    
    (cAliasCri)->(dbCloseArea())

    self:trackItem(oItem["idOnHealthProvider"])

Return

Method cabConsulta(oItem) Class SyncAuthorizations
    Local cCbos := oItem["professional"]["cbos"]["code"]
    Local cCodEsp := Posicione('BAQ', 6, xFilial('BAQ')+cCbos,'BAQ_CODESP')
    Local aObs	:= PLBreakTxt(oItem["attendanceNote"], {"BEA_MSG01", "BEA_MSG02"})
    
    
    if self:posProf(oItem["professional"]["stateAbbreviation"],;
                    oItem["professional"]["professionalCouncilNumber"],;
                    oItem["professional"]["professionalCouncil"],oItem["professional"]["name"],oItem["professional"]["professionalIdentifier"])  
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_NOMEXE"), BB0->BB0_NOME))
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_CDPFRE"), BB0->BB0_CODIGO))
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_SIGEXE"), oItem["professional"]["professionalCouncil"]))
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_REGEXE"), oItem["professional"]["professionalCouncilNumber"]))
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_ESTEXE"), oItem["professional"]["stateAbbreviation"]))
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_ESPEXE"), cCodEsp))
    endIf
    
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_ORIGEM"), "1"))

    BEA->(FieldPut(self:hmFieldsCab:get("BEA_CODESP"), cCodEsp))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_TIPADM"), alltrim(PLSVARVINC('23', nil, oItem["attendanceModel"]))))

    BEA->(FieldPut(self:hmFieldsCab:get("BEA_INDACI"), alltrim(PLSVARVINC('36', nil, oItem["accidentIndication"]))))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_TIPCON"), alltrim(PLSVARVINC('52', nil, oItem["consultationType"]))))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_MSG01"), aObs[1]))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_MSG02"), aObs[2]))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_LIBERA"), "0"))
 
Return

Method cabExame(oItem) Class SyncAuthorizations
    Local cCbos := oItem["professional"]["cbos"]["code"]
    Local cCodEsp := Posicione('BAQ', 6, xFilial('BAQ')+cCbos,'BAQ_CODESP')
    Local aIndCli	:= PLBreakTxt(oItem["clinicalCondition"], {"BEA_INDCLI", "BEA_INDCL2"})
    Local aObs	:= PLBreakTxt(oItem["attendanceNote"], {"BEA_MSG01", "BEA_MSG02"})

    if self:posProf(oItem["professional"]["stateAbbreviation"],;
                    oItem["professional"]["professionalCouncilNumber"],;
                    oItem["professional"]["professionalCouncil"],oItem["professional"]["name"],oItem["professional"]["professionalIdentifier"])  
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_NOMSOL"), BB0->BB0_NOME))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_CDPFSO"), BB0->BB0_CODIGO))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_SIGLA"), oItem["professional"]["professionalCouncil"]))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_REGSOL"), oItem["professional"]["professionalCouncilNumber"]))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_ESTSOL"), oItem["professional"]["stateAbbreviation"]))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_ESPSOL"), cCodEsp))
    endIf

    
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_ORIGEM"), "2"))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_CODESP"), cCodEsp))

    BEA->(FieldPut(self:hmFieldsCab:get("BEA_TIPADM"), alltrim(PLSVARVINC('23', nil, oItem["attendanceModel"]))))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_DATSOL"), Stod(StrTran(oItem["requestDate"], "-", ""))))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_INDCLI"), aIndCli[1]))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_INDCL2"), aIndCli[2]))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_MSG01"), aObs[1]))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_MSG02"), aObs[2]))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_LIBERA"), "1"))
  
Return

Method cabOdonto(oItem) Class SyncAuthorizations
    Local cCbos := oItem["professional"]["cbos"]["code"]
    Local cCodEsp := Posicione('BAQ', 6, xFilial('BAQ')+cCbos,'BAQ_CODESP')
    Local aIndCli	:= PLBreakTxt(oItem["clinicalCondition"], {"BEA_INDCLI", "BEA_INDCL2"})
    Local aObs	:= PLBreakTxt(oItem["attendanceNote"], {"BEA_MSG01", "BEA_MSG02"})

    // Preenche os dados do profissional solicitante
    if self:posProf(oItem["professionalRequestor"]["stateAbbreviation"],;
                    oItem["professionalRequestor"]["professionalCouncilNumber"],;
                    oItem["professionalRequestor"]["professionalCouncil"],oItem["professionalRequestor"]["name"],oItem["professionalRequestor"]["professionalIdentifier"])  
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_NOMSOL"), BB0->BB0_NOME))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_CDPFSO"), BB0->BB0_CODIGO))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_SIGLA"), oItem["professionalRequestor"]["professionalCouncil"]))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_REGSOL"), oItem["professionalRequestor"]["professionalCouncilNumber"]))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_ESTSOL"), oItem["professionalRequestor"]["stateAbbreviation"]))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_ESPSOL"), cCodEsp))
        
    endIf
    
    // Preenche os dados do profissional executante
    if self:posProf(oItem["professional"]["stateAbbreviation"],;
                    oItem["professional"]["professionalCouncilNumber"],;
                    oItem["professional"]["professionalCouncil"],oItem["professional"]["name"],oItem["professional"]["professionalIdentifier"])  
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_CDPFRE"), BB0->BB0_CODIGO))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_NOMEXE"), BB0->BB0_NOME))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_SIGEXE"), oItem["professional"]["professionalCouncil"]))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_REGEXE"), oItem["professional"]["professionalCouncilNumber"]))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_ESTEXE"), oItem["professional"]["stateAbbreviation"]))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_ESPEXE"), cCodEsp))
    endif
    
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_ORIGEM"), "1"))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_CODESP"), cCodEsp))

    BEA->(FieldPut(self:hmFieldsCab:get("BEA_TIPADM"), alltrim(PLSVARVINC('23', nil, oItem["attendanceModel"]))))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_DATSOL"), Stod(StrTran(oItem["requestDate"], "-", ""))))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_INDCLI"), aIndCli[1]))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_INDCL2"), aIndCli[2]))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_MSG01"), aObs[1]))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_MSG02"), aObs[2]))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_LIBERA"), "0"))
  
Return

Method cabExecucao(oItem) Class SyncAuthorizations
    
    Local cCbos := oItem["professional"]["cbos"]["code"]
    Local cCodEsp := Posicione('BAQ', 6, xFilial('BAQ')+cCbos,'BAQ_CODESP')
    Local cCbosSol := oItem["sourceAuthorization"]["professional"]["cbos"]["code"]
    Local cCodEspSol := Posicione('BAQ', 6, xFilial('BAQ')+cCbosSol,'BAQ_CODESP')
    Local aIndCli	:= PLBreakTxt(oItem["sourceAuthorization"]["clinicalCondition"], {"BEA_INDCLI", "BEA_INDCL2"})
    Local aObs	:= PLBreakTxt(oItem["attendanceNote"], {"BEA_MSG01", "BEA_MSG02"})

    // Campos da guia de solicita��o - No HAT, se for execu��o direta, esses campos vem com os dados da mesma guia de execu��o
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_ORIGEM"), "1"))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_GUIPRI"), oItem['mainAuthorizationCode']))
    
    if self:posProf(oItem["sourceAuthorization"]["professional"]["stateAbbreviation"],;
                    oItem["sourceAuthorization"]["professional"]["professionalCouncilNumber"],;
                    oItem["sourceAuthorization"]["professional"]["professionalCouncil"],oItem["sourceAuthorization"]["professional"]["name"],oItem["sourceAuthorization"]["professional"]["professionalIdentifier"])  
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_NOMSOL"), BB0->BB0_NOME))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_CDPFSO"), BB0->BB0_CODIGO))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_SIGLA"), oItem["sourceAuthorization"]["professional"]["professionalCouncil"]))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_REGSOL"), oItem["sourceAuthorization"]["professional"]["professionalCouncilNumber"]))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_ESTSOL"), oItem["sourceAuthorization"]["professional"]["stateAbbreviation"]))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_ESPSOL"), cCodEspSol))
    endif

    BEA->(FieldPut(self:hmFieldsCab:get("BEA_TIPADM"), alltrim(PLSVARVINC('23', nil, oItem["sourceAuthorization"]["attendanceModel"]))))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_DATSOL"), Stod(StrTran(oItem["sourceAuthorization"]["requestDate"], "-", ""))))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_INDCLI"), aIndCli[1]))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_INDCL2"), aIndCli[2]))

    if self:posProf(oItem["professional"]["stateAbbreviation"],;
                    oItem["professional"]["professionalCouncilNumber"],;
                    oItem["professional"]["professionalCouncil"],oItem["professional"]["name"],oItem["professional"]["professionalIdentifier"])  
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_CDPFRE"), BB0->BB0_CODIGO))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_NOMEXE"), BB0->BB0_NOME))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_SIGEXE"), oItem["professional"]["professionalCouncil"]))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_REGEXE"), oItem["professional"]["professionalCouncilNumber"]))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_ESTEXE"), oItem["professional"]["stateAbbreviation"]))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_ESPEXE"), cCodEsp))
    endif

    BEA->(FieldPut(self:hmFieldsCab:get("BEA_CODESP"), cCodEsp))

    BEA->(FieldPut(self:hmFieldsCab:get("BEA_MSG01"), aObs[1]))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_MSG02"), aObs[2]))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_TIPATE"), alltrim(PLSVARVINC('50', nil, oItem["attendanceType"]))))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_INDACI"),  alltrim(PLSVARVINC('36', nil, oItem["accidentIndication"]))))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_TIPCON"), alltrim(PLSVARVINC('52', nil, oItem["consultationType"]))))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_TIPSAI"), alltrim(PLSVARVINC('39', nil, oItem["closingReason"]))))
    BEA->(FieldPut(self:hmFieldsCab:get("BEA_LIBERA"), "0"))

Return

Method cabInternacao(oItem, cAlias) Class SyncAuthorizations
    
    Local cCbos := oItem["professional"]["cbos"]["code"]
    Local cCodEsp := Posicione('BAQ', 6, xFilial('BAQ')+cCbos,'BAQ_CODESP')
    Local aIndCli	:= PLBreakTxt(oItem["clinicalCondition"], {"BEA_INDCLI", "BEA_INDCL2"})
    Local aObs	:= PLBreakTxt(oItem["attendanceNote"], {"BEA_MSG01", "BEA_MSG02"})
    Local aRetD := {}
    Local cSubscriberId         := oItem["beneficiary"]["subscriberId"]
    Local cOpeUsr               := substr(cSubscriberId, 1,4) 
	Local cCodEmp               := substr(cSubscriberId, 5,4)
	Local cMatric               := substr(cSubscriberId, 9,6)
	Local cTipReg               := substr(cSubscriberId, 15,2)
	Local cDigito               := substr(cSubscriberId, 17)

    if cAlias == "BE4"
    
        if self:posProf(oItem["professional"]["stateAbbreviation"],;
                    oItem["professional"]["professionalCouncilNumber"],;
                    oItem["professional"]["professionalCouncil"],oItem["professional"]["name"],oItem["professional"]["professionalIdentifier"])  
            BE4->(FieldPut(self:hmFieldsBE4:get("BE4_NOMSOL"), BB0->BB0_NOME))
            BE4->(FieldPut(self:hmFieldsBE4:get("BE4_CDPFSO"), BB0->BB0_CODIGO))
            BE4->(FieldPut(self:hmFieldsBE4:get("BE4_SIGLA"), oItem["professional"]["professionalCouncil"]))
            BE4->(FieldPut(self:hmFieldsBE4:get("BE4_REGSOL"), oItem["professional"]["professionalCouncilNumber"]))
            BE4->(FieldPut(self:hmFieldsBE4:get("BE4_ESTSOL"), oItem["professional"]["stateAbbreviation"]))
            BE4->(FieldPut(self:hmFieldsBE4:get("BE4_ESPSOL"), cCodEsp))
        endIf

        BE4->(FieldPut(self:hmFieldsBE4:get("BE4_CODESP"), cCodEsp))

        BE4->(FieldPut(self:hmFieldsBE4:get("BE4_TIPADM"), alltrim(PLSVARVINC('23', nil, oItem["attendanceModel"]))))
        BE4->(FieldPut(self:hmFieldsBE4:get("BE4_INDCLI"), aIndCli[1]))
        BE4->(FieldPut(self:hmFieldsBE4:get("BE4_INDCL2"), aIndCli[2]))
        BE4->(FieldPut(self:hmFieldsBE4:get("BE4_MSG01"), aObs[1]))
        BE4->(FieldPut(self:hmFieldsBE4:get("BE4_MSG02"), aObs[2]))
        BE4->(FieldPut(self:hmFieldsBE4:get("BE4_PRVINT"),  Stod(StrTran(oItem["expectedHospitalizationDate"], "-", ""))))
        BE4->(FieldPut(self:hmFieldsBE4:get("BE4_TIPINT"), cValToChar(STRZERO(val(alltrim(PLSVARVINC('57', nil, oItem["hospType"]))),2))))
        BE4->(FieldPut(self:hmFieldsBE4:get("BE4_GRPINT"), alltrim(PLSVARVINC('57', nil, oItem["hospType"]))))
        BE4->(FieldPut(self:hmFieldsBE4:get("BE4_CID"), oItem["primaryICD"]))
        BE4->(FieldPut(self:hmFieldsBE4:get("BE4_DESCID"), POSICIONE("BA9",1,xFilial("BA9")+AllTrim((oItem["primaryICD"])),"BA9_DOENCA")))
        BE4->(FieldPut(self:hmFieldsBE4:get("BE4_REGINT"), alltrim(PLSVARVINC('41', nil, oItem["hospRegime"]))))
        BE4->(FieldPut(self:hmFieldsBE4:get("BE4_DIASSO"), oItem["dailyRequestedQuantity"]))
        BE4->(FieldPut(self:hmFieldsBE4:get("BE4_OPESOL"), PLSINTPAD()))
 
  		aRetD := PLSDADUSR(cOpeUsr+cCodEmp+cMatric+cTipReg+cDigito,"1",.F.,dDataBase)

        If Len(aRetD)>0 .And. aRetD[1]
             BE4->(FieldPut(self:hmFieldsBE4:get("BE4_PADINT"), POSICIONE("BI3",1,xFilial("BI3")+cOpeUsr+aRetD[11]+aRetD[12],"BI3_CODACO")))
             BE4->(FieldPut(self:hmFieldsBE4:get("BE4_NOMUSR"),aRetD[6] ))
             BE4->(FieldPut(self:hmFieldsBE4:get("BE4_CONEMP"), BA1->BA1_CONEMP))
             BE4->(FieldPut(self:hmFieldsBE4:get("BE4_VERCON"), BA1->BA1_VERCON))
             BE4->(FieldPut(self:hmFieldsBE4:get("BE4_SUBCON"), BA1->BA1_SUBCON))
             BE4->(FieldPut(self:hmFieldsBE4:get("BE4_VERSUB"), BA1->BA1_VERSUB))
             BE4->(FieldPut(self:hmFieldsBE4:get("BE4_MATVID"), BA1->BA1_MATVID))
             BE4->(FieldPut(self:hmFieldsBE4:get("BE4_CPFUSR"), BA1->BA1_CPFUSR))

        EndIf

     else
        
        if self:posProf(oItem["professional"]["stateAbbreviation"],;
                    oItem["professional"]["professionalCouncilNumber"],;
                    oItem["professional"]["professionalCouncil"],oItem["professional"]["name"],oItem["professional"]["professionalIdentifier"])  
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_NOMSOL"), BB0->BB0_NOME))
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_CDPFSO"), BB0->BB0_CODIGO))
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_SIGLA"), oItem["professional"]["professionalCouncil"]))
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_REGSOL"), oItem["professional"]["professionalCouncilNumber"]))
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_ESTSOL"), oItem["professional"]["stateAbbreviation"]))
            BEA->(FieldPut(self:hmFieldsCab:get("BEA_ESPSOL"), cCodEsp))
        endif

        BEA->(FieldPut(self:hmFieldsCab:get("BEA_ORIGEM"), "2"))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_CODESP"), cCodEsp))

        BEA->(FieldPut(self:hmFieldsCab:get("BEA_TIPADM"), alltrim(PLSVARVINC('23', nil, oItem["attendanceModel"]))))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_INDCLI"), aIndCli[1]))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_INDCL2"), aIndCli[2]))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_MSG01"), aObs[1]))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_MSG02"), aObs[2]))
        BEA->(FieldPut(self:hmFieldsCab:get("BEA_OPESOL"), PLSINTPAD()))

    endIf
Return

Method trackItem(cNumGui) Class  SyncAuthorizations
	Local cEndPoint   := GetNewPar("MV_PHATURL","")
    Local cJson       := nil
    Local nX          := 0
    Local oClient     := nil
    Local aDadHeader  := {}
    Local aHeader     := {}
    
    //Verifica dados do header no arquivo plshat.ini
    aDadHeader := PLGDadHead()

    if !empty(cEndPoint)
        if Substr(cEndPoint,len(cEndPoint),1) <> "/"
	        cEndPoint += "/"
        endIf
        oClient := FWRest():New(cEndPoint)
        oClient:setPath("v1/authorizations/" + cNumGui)

        for nX := 1 to len(aDadHeader)
			aAdd(aHeader,aDadHeader[nX,1] + ": " + aDadHeader[nX,2])
		next

        cJson := '{"trackingStatus": 1}'
        lSuccess := oClient:Put(aHeader, cJson)

    endif

Return lSuccess

Method grvAuditoria(cNumGuia, cAuthType) Class SyncAuthorizations

    Local o790C			:= nil
	Local aCabCri       := {}
	Local aDadCri		:= {}
	Local aVetCri       := {}
	Local aHeaderITE    := {}
	Local aColsITE      := {}
	Local aVetITE       := {}

    if cAuthType == HAT_INTERNACAO

        BEJ->(dbSetOrder(1))
        BEL->(dbSetOrder(1))

        Store Header "BEJ" TO aHeaderITE For .T.
        Store COLS "BEJ" TO aColsITE FROM aHeaderITE VETTRAB aVetITE While; 
        BEJ->(BEJ_FILIAL+BEJ_CODOPE+BEJ_ANOINT+BEJ_MESINT+BEJ_NUMINT) == xFilial("BEJ")+cNumGuia

        Store Header "BEL" TO aCabCri For .T.
        Store COLS "BEL" TO aDadCri FROM aCabCri VETTRAB aVetCri While;
        BEL->(BEL_FILIAL+BEL_CODOPE+BEL_ANOINT+BEL_MESINT+BEL_NUMINT) == xFilial("BEL")+cNumGuia

        o790C := PLSA790C():New(.T.)
        o790C:SetAuditoria(.T.,.T.,.F.,.F.,.F.,aDadCri,aCabCri,__aCdCri187[1],"0","BEL",aColsITE,aHeaderITE,"BEJ",.F., .F.)
            
        o790C:Destroy()
    
    else
        BE2->(dbSetOrder(1))
        BEG->(dbSetOrder(1))

        Store Header "BE2" TO aHeaderITE For .T.
        Store COLS "BE2" TO aColsITE FROM aHeaderITE VETTRAB aVetITE While; 
        BE2->(BE2_FILIAL+BE2_OPEMOV+BE2_ANOAUT+BE2_MESAUT+BE2_NUMAUT) == xFilial("BE2")+cNumGuia

        Store Header "BEG" TO aCabCri For .T.
        Store COLS "BEG" TO aDadCri FROM aCabCri VETTRAB aVetCri While;
        BEG->(BEG_FILIAL+BEG_OPEMOV+BEG_ANOAUT+BEG_MESAUT+BEG_NUMAUT) == xFilial("BEG")+cNumGuia

        o790C := PLSA790C():New(.T.)
        o790C:SetAuditoria(.T.,.F.,.F.,.F.,.F.,aDadCri,aCabCri,__aCdCri187[1],"0","BEG",aColsITE,aHeaderITE,"BE2",.F., .F.)
        o790C:Destroy()
    endIf

Return

Method initializeHMapFields() Class SyncAuthorizations
    Local aStrucBE4 := {}
    Local aStrucBEJ := {}
    Local aStrucBEL := {}
    Local aStrucCab := {}
    Local aStrucIte := {}
    Local aStrucCri := {}
    Local aStrucB4B := {}
    Local aStrucB2Z := {}
    Local nLenStruct := 0
    Local nControl  := 1
    Local aStrucB43 := {}

    self:hmFieldsBE4 := HashMap():New()
    self:hmFieldsBEJ := HashMap():New()
    self:hmFieldsBEL := HashMap():New()
    self:hmFieldsCab := HashMap():New()
    self:hmFieldsIte := HashMap():New()
    self:hmFieldsCri := HashMap():New()
    self:hmFieldsPar := HashMap():New()
    self:hmFieldsB2Z := HashMap():New()
    self:hmFieldsB43 := HashMap():New()
             
    BE4->(dbSelectArea("BE4"))
        aStrucBE4 := BE4->(dbStruct())
        nLenStruct := Len(aStrucBE4)
        while nControl <= nLenStruct
            self:hmFieldsBE4:set(aStrucBE4[nControl][1],nControl)
            nControl++
        enddo
        nControl := 1
    BE4->(dbCloseArea())

    BEJ->(dbSelectArea("BEJ"))
        aStrucBEJ := BEJ->(dbStruct())
        nLenStruct := Len(aStrucBEJ)
        while nControl <= nLenStruct
            self:hmFieldsBEJ:set(aStrucBEJ[nControl][1],nControl)
            nControl++
        enddo
        nControl := 1
    BEJ->(dbCloseArea())

    BEL->(dbSelectArea("BEL"))
        aStrucBEL := BEL->(dbStruct())
        nLenStruct := Len(aStrucBEL)
        while nControl <= nLenStruct
            self:hmFieldsBEL:set(aStrucBEL[nControl][1],nControl)
            nControl++
        enddo
        nControl := 1
    BEL->(dbCloseArea())  
    
    BEA->(dbSelectArea("BEA"))
        aStrucCab := BEA->(dbStruct())
        nLenStruct := Len(aStrucCab)
        while nControl <= nLenStruct
            self:hmFieldsCab:set(aStrucCab[nControl][1],nControl)
            nControl++
        enddo
        nControl := 1
    BEA->(dbCloseArea())

    BE2->(dbSelectArea("BE2"))
        aStrucIte := BE2->(dbStruct())
        nLenStruct := Len(aStrucIte)
        while nControl <= nLenStruct
            self:hmFieldsIte:set(aStrucIte[nControl][1],nControl)
            nControl++
        enddo
        nControl := 1
    BE2->(dbCloseArea())

    BEG->(dbSelectArea("BEG"))
        aStrucCri := BEG->(dbStruct())
        nLenStruct := Len(aStrucCri)
        while nControl <= nLenStruct
            self:hmFieldsCri:set(aStrucCri[nControl][1],nControl)
            nControl++
        enddo
        nControl := 1
    BEG->(dbCloseArea())

    B4B->(dbSelectArea("B4B"))
        aStrucB4B := B4B->(dbStruct())
        nLenStruct := Len(aStrucB4B)
        while nControl <= nLenStruct
            self:hmFieldsPar:set(aStrucB4B[nControl][1],nControl)
            nControl++
        enddo
        nControl := 1
    B4B->(dbCloseArea())

    B2Z->(dbSelectArea("B2Z"))
        aStrucB2Z := B2Z->(dbStruct())
        nLenStruct := Len(aStrucB2Z)
        while nControl <= nLenStruct
            self:hmFieldsB2Z:set(aStrucB2Z[nControl][1],nControl)
            nControl++
        enddo
        nControl := 1
    B2Z->(dbCloseArea())

   B43->(dbSelectArea("B43"))
        aStrucB43 := B43->(dbStruct())
        nLenStruct := Len(aStrucB43)
        while nControl <= nLenStruct
            self:hmFieldsB43:set(aStrucB43[nControl][1],nControl)
            nControl++
        enddo
        nControl := 1
    B43->(dbCloseArea())

Return

Method syncCancel() Class SyncAuthorizations
    Local nAuthId := 0
    Local cAuthType :=          ""

    while self:hasNext() .And. self:getDataFromClient()
        while self:hasItens()
            oItem     := self:getNextItem()
            nAuthId   := oItem["authorizationId"]
            cAuthType := oItem["authorizationStatus"]
            If cAuthType == "9"
                Self:cancelauth(oItem) 
            EndIf
        enddo
    enddo
    
Return

Method cancelauth(oItem) Class  SyncAuthorizations
    Local cCodOpe               := PlsIntPad()
    Local cAuthType             := IIF('Exame - Exec' $ oItem["authorizationDescription"],'3',oItem["authorizationType"])
    Local cAuthNumber           := oItem["idOnHealthProvider"]
    Local cAnoAut               := substr(cAuthNumber, 5,4) 
    Local cMesAut               := substr(cAuthNumber, 9,2) 
    Local cNumAut               := substr(cAuthNumber, 11)
    Local cCodTiss	            := GetNewPar("MV_MOTTISS","")
	Local cDesTiss              := ""

   	BTQ->(DBSetOrder(1)) //BTQ_FILIAL+BTQ_CODTAB+BTQ_CDTERM
	if BTQ->(msSeek(xFilial("BTQ")+"38"+cCodTiss))
		cCodTiss:= BTQ->BTQ_CDTERM
		cDesTiss:= BTQ->BTQ_DESTER
	endIf

    iF cAuthType = HAT_INTERNACAO
        BE4->(DBSetOrder(2))
        IF BE4->( MsSeek( xFilial("BE4")+cCodOpe+cAnoAut+cMesAut+cNumAut) )
           BE4->(RecLock("BE4",.F.))
           BE4->BE4_CANCEL :="1"          
           BE4->BE4_STATUS :="3"          
           BE4->BE4_CANTIS := cCodTiss
           BE4->BE4_CANEDI := cDesTiss
           BE4->(MsUnlock())
        EndIf     

        IF BEA->( MsSeek( xFilial("BEA")+cCodOpe+cAnoAut+cMesAut+cNumAut) )
            BEA->(RecLock("BEA",.F.))
            BEA->BEA_CANCEL :="1"      
            BEA->BEA_STATUS :="3"          
            BEA->BEA_CANTIS := cCodTiss
            BEA->BEA_CANEDI := cDesTiss
            BEA->(MsUnlock())
        EndIf
      
        BEJ->(dbGoTop())  
        iF BEJ->( MsSeek( xFilial("BEJ")+cCodOpe+cAnoAut+cMesAut+cNumAut) )

            While !BEJ->(Eof()) .And. BE4->(BE4_FILIAL+BE4_CODOPE+BE4_ANOINT+BE4_MESINT+BE4_NUMINT) == BEJ->(BEJ_FILIAL+BEJ_CODOPE+BEJ_ANOINT+BEJ_MESINT+BEJ_NUMINT)
                BEJ->(RecLock("BEJ",.F.))
                BEJ->BEJ_STATUS :="0"          
                BEJ->(MsUnlock())
                BEJ->(DbSkip())
            EndDo
        EndIf

        BE2->(dbGoTop())   
        IF BE2->( MsSeek( xFilial("BE2")+cCodOpe+cAnoAut+cMesAut+cNumAut) )
 
            While !BE2->(Eof()) .And. BE4->(BE4_FILIAL+BE4_CODOPE+BE4_ANOINT+BE4_MESINT+BE4_NUMINT) == BE2->(BE2_FILIAL+BE2_OPEMOV+BE2_ANOAUT+BE2_MESAUT+BE2_NUMAUT)
                BE2->(RecLock("BE2",.F.))
                BE2->BE2_STATUS :="0"          
                BE2->(MsUnlock())
                BE2->(DbSkip())
            EndDo
        EndIf
 
    else

        IF BEA->( MsSeek( xFilial("BEA")+cCodOpe+cAnoAut+cMesAut+cNumAut) )
            BEA->(RecLock("BEA",.F.))
            BEA->BEA_CANCEL :="1"      
            BEA->BEA_STATUS :="3"          
            BEA->BEA_CANTIS := cCodTiss
            BEA->BEA_CANEDI := cDesTiss
            BEA->(MsUnlock())
        EndIf
            
        BE2->(dbGoTop())   
        IF BE2->( MsSeek( xFilial("BE2")+cCodOpe+cAnoAut+cMesAut+cNumAut) )
 
            While !BE2->(Eof()) .And. BEA->(BEA_FILIAL+BEA_OPEMOV+BEA_ANOAUT+BEA_MESAUT+BEA_NUMAUT) == BE2->(BE2_FILIAL+BE2_OPEMOV+BE2_ANOAUT+BE2_MESAUT+BE2_NUMAUT)
                BE2->(RecLock("BE2",.F.))
                BE2->BE2_STATUS :="0"          
                BE2->(MsUnlock())
                BE2->(DbSkip())
            EndDo
        EndIf
    EndIf
 
Return
