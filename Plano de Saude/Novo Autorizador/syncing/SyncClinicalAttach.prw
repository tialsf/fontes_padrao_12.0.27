#INCLUDE "TOTVS.CH"
#INCLUDE "TBICONN.CH"   
#INCLUDE "PROTHEUS.CH"
#include "PLSMGER.CH"
#INCLUDE "hatActions.ch"

#define __aCdCri187 {"573","Demanda por requerimento"}

/*/{Protheus.doc} SyncClinicalAttach
    Synchronizer concreto que lida com a integracoes do endpoint ClinicalAttachments
    @type  Class
    @author robson.nayland
    @since 20200312
/*/
Class SyncClinicalAttach from SyncHandler

    Data hmFieldsCab
    Data hmFieldsIte
    Data hmFieldsCri
    
    Method New()
    
    Method sync()
    Method trackItem()
    Method grvAuditoria(cNumGuia)
    Method grvB2Z(hmFieldsB2Z,oItem,cSequen,cCodTabDePara,cCodProcDePara,nQtdAut,nSaldo) 
    Method syncCancel()
    Method cancAttach(oItem,cCodTiss)

EndClass

Method New() Class SyncClinicalAttach
    _Super:new(SYNC_CLINICAL_ATTACHMENTS)
    self:cPath := "v1/clinicalAttachments"
Return self

/*/{Protheus.doc} sync
Preenche a tabela de historico com as libera��es realizadas no HAT
@type  Class
@author robson.nayland
@since 20190115
/*/
Method sync() Class SyncClinicalAttach
    Local nControl  := 1
    Local hmFieldsCab := HashMap():New()
    Local hmFieldsIte := HashMap():New()
    Local hmFieldsCri := HashMap():New()
    Local hmFieldsB2Z := HashMap():New()
    Local nProced   := 1  
    Local nCritica  := 1
    Local aTabDup   := PlsBusTerDup(SuperGetMv("MV_TISSCAB", .F. ,"87")) 
    Local aRetAux   := {}
    Local cSequen   := ''
    Local cNumAut   := ''
    Local cOpmeve   := ''
    Local cAnoAut   := ''
    Local cMesAut   := ''
    Local lAudGuia  := .F.
    Local nQtdPro   := 0
    Local nQtdAut   := 0

    cCodOpe := PlsIntPad()
    dbSelectArea("BA1")
    BA1->(dbSetOrder(2))

    dbSelectArea("B4A")
    aStrucB4A := B4A->(dbStruct())
    nLenStruct := Len(aStrucB4A)

    while nControl <= nLenStruct
        hmFieldsCab:set(aStrucB4A[nControl][1],nControl)
        nControl++
    enddo
    nControl := 1

    dbSelectArea("B4C")
    aStrucIte := B4C->(dbStruct())
    nLenStruct := Len(aStrucIte)
    while nControl <= nLenStruct
        hmFieldsIte:set(aStrucIte[nControl][1],nControl)
        nControl++
    enddo

    nControl := 1
    
    dbSelectArea("BEG")
    aStrucBEG := dbStruct()
    nLenStruct := Len(aStrucBEG)
    while nControl <= nLenStruct
        hmFieldsCri:set(aStrucBEG[nControl][1],nControl)
        nControl++
    enddo
   
    
    nControl := 1
    
    dbSelectArea("B2Z")
    aStrucB2Z := dbStruct()
    nLenStruct := Len(aStrucB2Z)
    while nControl <= nLenStruct
        hmFieldsB2Z:set(aStrucB2Z[nControl][1],nControl)
        nControl++
    enddo
   
    while self:hasNext() .And. self:getDataFromClient()
        while self:hasItens()
            oItem := self:getNextItem()
         
            B4A->(dbSetOrder(1)) //B4A_FILIAL+B4A_OPEMOV+B4A_ANOAUT+B4A_MESAUT+B4A_NUMAUT
            
            cNumAut     := oItem["attachNumber"]
            cOpmeve     := Substr(oItem["attachNumber"],1,4)
            cAnoAut     := Substr(oItem["attachNumber"],5,4)
            cMesAut     := Substr(oItem["attachNumber"],9,2)
            lAudGuia    := oItem["authorizationStatus"] == '6'
            cNumProto   := oItem["attendanceProtocol"]
            cStatus     := oItem["authorizationStatus"]
            cNrAutPri   := oItem["mainAuthorizationCode"]
          
            aRetAux := PLSXVLDCAL(Stod(StrTran(oItem["requestedDate"],"-","")),PLSINTPAD(),.T.,"","")
            
            If BA1->(DbSeek(xFilial("BA1")+oItem["subscriberId"])) .And. !B4A->(DbSeek(xFilial('B4A')+cNumAut))

                Begin Transaction
                
                DbSelectArea("B4A")
                B4A->(dbAppend())
                FieldPut(hmFieldsCab:get("B4A_FILIAL"), xFilial("B4A"))
                FieldPut(hmFieldsCab:get("B4A_OPEMOV"),cOpmeve)
                FieldPut(hmFieldsCab:get("B4A_TIPANE"),iIf(oItem["attachType"]=="14","1",iIf(oItem["attachType"]=="13","2","3")))
                FieldPut(hmFieldsCab:get("B4A_TIPGUI"),iIf(oItem["journey"]=="13","07",iIf(oItem["journey"]=="14","08","09")))
                FieldPut(hmFieldsCab:get("B4A_PROATE"),oItem["attendanceProtocol"])
                FieldPut(hmFieldsCab:get("B4A_NUMAUT"),Substr(oItem["attachNumber"],11,8))
                FieldPut(hmFieldsCab:get("B4A_GUIREF"),oItem["mainAuthorizationCode"])
                FieldPut(hmFieldsCab:get("B4A_SENHA"),oItem["password"])
                FieldPut(hmFieldsCab:get("B4A_DATSOL"),sTod(StrTran(oItem["requestedDate"],"-","")))
                FieldPut(hmFieldsCab:get("B4A_DATPRO"),sTod(StrTran(oItem["requestedDate"],"-",""))) //Mudan�a de authorizedDate para requestedDate - recomendacao da equipe HAT
                FieldPut(hmFieldsCab:get("B4A_GUIOPE"),oItem["idOnHealthProvider"])
                FieldPut(hmFieldsCab:get("B4A_STATUS"),oItem["authorizationStatus"])
                FieldPut(hmFieldsCab:get("B4A_MATANT"),BA1->BA1_MATANT)
                FieldPut(hmFieldsCab:get("B4A_OPEUSR"),BA1->BA1_CODINT)
            
                FieldPut(hmFieldsCab:get("B4A_CODEMP"),BA1->BA1_CODEMP)
                FieldPut(hmFieldsCab:get("B4A_CONEMP"),BA1->BA1_CONEMP)
                FieldPut(hmFieldsCab:get("B4A_SUBCON"),BA1->BA1_SUBCON)
                FieldPut(hmFieldsCab:get("B4A_VERCON"),BA1->BA1_VERCON)
                FieldPut(hmFieldsCab:get("B4A_VERSUB"),BA1->BA1_VERSUB)
            
                FieldPut(hmFieldsCab:get("B4A_MATRIC"),BA1->BA1_MATRIC)
                FieldPut(hmFieldsCab:get("B4A_TIPREG"),BA1->BA1_TIPREG)
                FieldPut(hmFieldsCab:get("B4A_DIGITO"),BA1->BA1_DIGITO)
                FieldPut(hmFieldsCab:get("B4A_NOMUSR"),BA1->BA1_NOMUSR) 
                FieldPut(hmFieldsCab:get("B4A_CANCEL"),If(oItem["isCancelled"],"1","0"))
                FieldPut(hmFieldsCab:get("B4A_ANOAUT"),cAnoAut)
                FieldPut(hmFieldsCab:get("B4A_MESAUT"),cMesAut)
                FieldPut(hmFieldsCab:get("B4A_AUDITO"),iif(lAudGuia,"1","0"))
                FieldPut(hmFieldsCab:get("B4A_IDADE"),DateDiffYear( Date() , BA1->BA1_DATNAS ))
                FieldPut(hmFieldsCab:get("B4A_SEXO"),BA1->BA1_SEXO)
                
                FieldPut(hmFieldsCab:get("B4A_TPGRV"),"2")
                FieldPut(hmFieldsCab:get("B4A_COMUNI"),"1")
                FieldPut(hmFieldsCab:get("B4A_DESOPE"),cOpmeve)
                FieldPut(hmFieldsCab:get("B4A_GUIPRE"),oItem["idOnHealthProvider"])
                FieldPut(hmFieldsCab:get("B4A_STTISS"),PLSANLSTIG(oItem["authorizationStatus"]))

                if len(aRetAux) > 4
                    FieldPut(hmFieldsCab:get("B4A_ANOPAG"),aRetAux[4]) 
                    FieldPut(hmFieldsCab:get("B4A_MESPAG"),aRetAux[5]) 
                endIf

                if ValType(oItem["professional"]) == "J"
                    FieldPut(hmFieldsCab:get("B4A_NOMSOL"),oItem["professional"]["name"])
                endif
                FieldPut(hmFieldsCab:get("B4A_EMASOL"),oItem["email"])
                FieldPut(hmFieldsCab:get("B4A_TELSOL"),oItem["phoneNumber"])
                
                If oItem["attachType"] == "12" // OPME
                    FieldPut(hmFieldsCab:get("B4A_JUSTTE"),oItem["technicalJustification"])
                    FieldPut(hmFieldsCab:get("B4A_ESPMAT"),oItem["materialSpec"])
       
                ElseIf oItem["attachType"] == "13" // Quimio
       
                    FieldPut(hmFieldsCab:get("B4A_PESO"),oItem["beneficiaryWeight"])
                    FieldPut(hmFieldsCab:get("B4A_ALTURA"),oItem["beneficiaryHeight"])
                    FieldPut(hmFieldsCab:get("B4A_DATDIA"),sTod(StrTran(oItem["diagnosisDate"],"-","")))
                    FieldPut(hmFieldsCab:get("B4A_CIDPRI"),oItem["primaryICD"])
                    FieldPut(hmFieldsCab:get("B4A_CIDSEC"),oItem["secondaryICD"])
                    FieldPut(hmFieldsCab:get("B4A_CIDTER"),oItem["terciaryICD"])
                    FieldPut(hmFieldsCab:get("B4A_CIDQUA"),oItem["quaternaryICD"])
                    FieldPut(hmFieldsCab:get("B4A_ESTADI"),oItem["staging"])
                    FieldPut(hmFieldsCab:get("B4A_TIPQUI"),oItem["chemotherapyType"])                    
                    FieldPut(hmFieldsCab:get("B4A_FINALI"),oItem["purpose"])
                    FieldPut(hmFieldsCab:get("B4A_ECOG"),oItem["ecog"])
                    FieldPut(hmFieldsCab:get("B4A_TUMOR"),oItem["tumor"])
                    FieldPut(hmFieldsCab:get("B4A_NODULO"),oItem["nodule"])
                    FieldPut(hmFieldsCab:get("B4A_METAST"),oItem["metastasis"])
                    FieldPut(hmFieldsCab:get("B4A_PLATER"),oItem["therapeuticPlan"])
                    FieldPut(hmFieldsCab:get("B4A_DIAGCH"),oItem["histopathologicalDiagnosis"])                    
                    FieldPut(hmFieldsCab:get("B4A_INFREL"),oItem["relevantInformations"])
                    FieldPut(hmFieldsCab:get("B4A_CIRURG"),oItem["surgery"])                    
                    FieldPut(hmFieldsCab:get("B4A_DATCIR"),sTod(StrTran(oItem["surgeryDate"],"-","")))  
                    FieldPut(hmFieldsCab:get("B4A_AREA"),oItem["irradiatedArea"])
                    FieldPut(hmFieldsCab:get("B4A_DATIRR"),sTod(StrTran(oItem["radioApplicationDate"],"-","")))   
                    FieldPut(hmFieldsCab:get("B4A_INTCIC"),oItem["intervalBetweenCycles"])
                    FieldPut(hmFieldsCab:get("B4A_NROCIC"),oItem["expectedCyclesNumber"])
                    FieldPut(hmFieldsCab:get("B4A_CICATU"),oItem["currentCycle"])
                    FieldPut(hmFieldsCab:get("B4A_SUPCOR"),Sqrt((oItem["beneficiaryWeight"]*oItem["beneficiaryHeight"])/3600))
                    //FieldPut(hmFieldsCab:get("B4A_CICATU"),oItem["daysFromCurrentCycle"])
                    //FieldPut(hmFieldsCab:get("B4A_DCICAT"),oItem["currentCycle"])

            
                ElseIf oItem["attachType"] == "14" // Radio
                    FieldPut(hmFieldsCab:get("B4A_DATDIA"),sTod(StrTran(oItem["diagnosisDate"],"-","")))
                    FieldPut(hmFieldsCab:get("B4A_CIDSEC"),oItem["secondaryICD"])
                    FieldPut(hmFieldsCab:get("B4A_CIDTER"),oItem["terciaryICD"])
                    FieldPut(hmFieldsCab:get("B4A_CIDQUA"),oItem["quaternaryICD"])
                    FieldPut(hmFieldsCab:get("B4A_DIAIMG"),oItem["imageDiagnosis"])
                    FieldPut(hmFieldsCab:get("B4A_ESTADI"),oItem["staging"])
                    FieldPut(hmFieldsCab:get("B4A_ECOG"),oItem["ecog"])
                    FieldPut(hmFieldsCab:get("B4A_FINALI"),oItem["purpose"])
                    FieldPut(hmFieldsCab:get("B4A_DIAGCH"),oItem["histopathologicalDiagnosis"])
                    FieldPut(hmFieldsCab:get("B4A_INFREL"),oItem["relevantInformations"])
                    FieldPut(hmFieldsCab:get("B4A_CIRURG"),oItem["surgery"]) 
                    FieldPut(hmFieldsCab:get("B4A_DATCIR"),sTod(StrTran(oItem["surgeryDate"],"-",""))) 
                    FieldPut(hmFieldsCab:get("B4A_QUIMIO"),oItem["chemotherapy"])
                    FieldPut(hmFieldsCab:get("B4A_DATQUI"),sTod(StrTran(oItem["chemoApplicationDate"],"-","")))
                    FieldPut(hmFieldsCab:get("B4A_NROCAM"),oItem["radiationFieldsNumber"])
                    FieldPut(hmFieldsCab:get("B4A_DOSDIA"),oItem["dailyDose"])
                    FieldPut(hmFieldsCab:get("B4A_DOSTOT"),oItem["totalDosage"])
                    FieldPut(hmFieldsCab:get("B4A_NRODIA"),oItem["numberOfDays"])
                    FieldPut(hmFieldsCab:get("B4A_DATPRE"),sTod(StrTran(oItem["drugAdministrationStartDate"],"-","")))

                Endif

                B4A->(dbcommit())

                // Gravando Itens
                For nProced:= 1 to Len(oItem["procedures"])
                    dbSelectArea("B4C")

                    oProced := oItem["procedures"][nProced]
                    cCodTab := oProced["tableCode"]
                    cCodProc := oProced["procedureCode"]
                    cCodTabDePara  := AllTrim(PLSVARVINC('87','BR4',cCodTab))
                    cCodProcDePara := AllTrim(PLSVARVINC(cCodTab,'BR8', cCodProc, cCodTabDePara+cCodProc,,aTabDup,@cCodTabDePara))
                    cSequen := strzero(nProced, Len(B4C->B4C_SEQUEN))
                    
                    if oItem["attachType"] == "13" //Quimio
                        nQtdPro := iif(empty(oProced["totalCycleDosage"]),0,oProced["totalCycleDosage"])
                        nQtdAut := iif(empty(oProced["totalCycleDosage"]),0,oProced["totalCycleDosage"])
                    else
                        nQtdPro := iif(empty(oProced["requestedQuantity"]) ,0,oProced["requestedQuantity"])
                        nQtdAut := iif(empty(oProced["authorizedQuantity"]),0,oProced["authorizedQuantity"])
                    endIf

                    dbSelectArea("B2Z")
                    self:grvB2Z(hmFieldsB2Z,oItem,cSequen,cCodTabDePara,cCodProcDePara,nQtdPro,nQtdAut) 
                    
                    B4C->(dbSetOrder(3)) //B4C_FILIAL+B4C_OPEMOV+B4C_ANOAUT+B4C_MESAUT+B4C_NUMAUT+B4C_CODPAD+B4C_CODPRO
                    if !B4C->(DbSeek(xFilial("B4C")+cNumAut+cCodTabDePara+cCodProcDePara))
                        DbSelectArea("B4C")
                        B4C->(dbAppend())
                        FieldPut(hmFieldsIte:get("B4C_FILIAL"), xFilial("B4C"))
                        FieldPut(hmFieldsIte:get("B4C_OPEMOV"), cOpmeve)
                        FieldPut(hmFieldsIte:get("B4C_ANOAUT"), cAnoAut)
                        FieldPut(hmFieldsIte:get("B4C_MESAUT"), cMesAut)
                        FieldPut(hmFieldsIte:get("B4C_NUMAUT"), Substr(oItem["attachNumber"],11,8))
                        FieldPut(hmFieldsIte:get("B4C_IMGSTA"), Iif(oItem["authorizationStatus"]=="1","ENABLE","DISABLE"))
                        FieldPut(hmFieldsIte:get("B4C_TPGRV"),"2" )
                        FieldPut(hmFieldsIte:get("B4C_SEQUEN"),cSequen )
                        FieldPut(hmFieldsIte:get("B4C_DATPRO"),sTod(StrTran(oItem["authorizedDate"],"-","")) )
                        FieldPut(hmFieldsIte:get("B4C_CODPAD"),cCodTabDePara)
                        FieldPut(hmFieldsIte:get("B4C_CODPRO"),cCodProcDePara)
                        FieldPut(hmFieldsIte:get("B4C_DESPRO"),POSICIONE('BR8',1,xFilial('BR8') + cCodTabDePara + cCodProcDePara,'BR8_DESCRI')) 
                        FieldPut(hmFieldsIte:get("B4C_QTDPRO"), nQtdPro )
                        FieldPut(hmFieldsIte:get("B4C_QTDSOL"), nQtdPro )
                        FieldPut(hmFieldsIte:get("B4C_AUDITO") ,iif(oProced["status"] == 2, "1", "0") )
                        FieldPut(hmFieldsIte:get("B4C_VLRUNT"), iif(empty(oProced["unitaryWorth"]), 0, oProced["unitaryWorth"]))
                        FieldPut(hmFieldsIte:get("B4C_VLRUNA"), iif(empty(oProced["unitaryWorth"]), 0, oProced["unitaryWorth"]))
                        FieldPut(hmFieldsIte:get("B4C_SALDO") , nQtdAut )
                        FieldPut(hmFieldsIte:get("B4C_NIVCRI"),iif(oProced["status"] <> 1,"HAT","")  )
                            
        
                        FieldPut(hmFieldsIte:get("B4C_NIVAUT"),"" )                        
                        FieldPut(hmFieldsIte:get("B4C_CHVNIV"),"" )
                        FieldPut(hmFieldsIte:get("B4C_STATUS"), iif(oProced["status"] == 1, "1", "0") ) //Alltrim(Str(oProced["status"])) )
                        FieldPut(hmFieldsIte:get("B4C_NIVEL"),"" )
                        
                        FieldPut(hmFieldsIte:get("B4C_OPCAO"),"" )
                        FieldPut(hmFieldsIte:get("B4C_REGANV"),"" )
                        FieldPut(hmFieldsIte:get("B4C_REFMAF"),"" )
                        FieldPut(hmFieldsIte:get("B4C_AUTFUN"),"" )
                        FieldPut(hmFieldsIte:get("B4C_COMUNI"),"" )
                        FieldPut(hmFieldsIte:get("B4C_NRTROL"),"" )
                        FieldPut(hmFieldsIte:get("B4C_NRAOPE"),"" )
                        if oItem["attachType"] == "13" // Quimio
                            FieldPut(hmFieldsIte:get("B4C_UNMED") ,iif(empty(oProced["unitOfMeasurement"]), "", oProced["unitOfMeasurement"]))
                            FieldPut(hmFieldsIte:get("B4C_VIAADM"),iif(empty(oProced["accessWay"]), "", oProced["accessWay"]))
                            FieldPut(hmFieldsIte:get("B4C_FREQUE"),iif(empty(oProced["frequency"]), 0, oProced["frequency"]))
                        else
                            FieldPut(hmFieldsIte:get("B4C_UNMED") ,"" )
                            FieldPut(hmFieldsIte:get("B4C_VIAADM"),"" )
                            FieldPut(hmFieldsIte:get("B4C_FREQUE"),0 )
                        endIf

                        B4C->(dbcommit())
                    endIf

                    // inclusao de critricas
                    dbSelectArea("BEG")
                    nLenCritica := iif(!empty(oItem["procedures"][nProced]["rejectionCauses"]),;
                    Len(oItem["procedures"][nProced]["rejectionCauses"]), 0)
                    

                    BCT->(dbSetOrder(1))
            
                    while nCritica <= nLenCritica
                        oCritica := oItem["procedures"][nProced]["rejectionCauses"][nCritica]
                
                        BCT->(dbGoTop())
                        if BCT->(msSeek(xFilial('BCT')+cCodOpe+oCritica["code"]))

                            DbSelectArea("BEG")
                            BEG->(dbAppend())
                                FieldPut(hmFieldsCri:get("BEG_FILIAL"), xFilial("BEG"))
                                FieldPut(hmFieldsCri:get("BEG_OPEMOV"), cCodOpe)
                                FieldPut(hmFieldsCri:get("BEG_ANOAUT"), cAnoAut)
                                FieldPut(hmFieldsCri:get("BEG_MESAUT"), cMesAut)
                                FieldPut(hmFieldsCri:get("BEG_NUMAUT"), Substr(oItem["attachNumber"],11,8))
                                FieldPut(hmFieldsCri:get("BEG_SEQUEN"), strzero(nProced, 3))
                                FieldPut(hmFieldsCri:get("BEG_CODGLO"), oCritica["code"])
                                FieldPut(hmFieldsCri:get("BEG_DESGLO"), BCT->BCT_DESCRI)
                                FieldPut(hmFieldsCri:get("BEG_SEQCRI"), strzero(nCritica, 3))
                                FieldPut(hmFieldsCri:get("BEG_TIPO"), BCT->BCT_TIPO)
                            BEG->(dbcommit())
                        endIf
                        nCritica++
                    endDo

                Next nProced

                self:trackItem(oItem["attachNumber"])

                 iF !Empty(cNumProto) .and. cStatus $  "2|6|3"
                    gerRegB00(oItem["attendanceProtocol"],,"B4A",.T.,.f.,BA1->BA1_CODINT+BA1->BA1_CODEMP+BA1->BA1_MATRIC+BA1->BA1_TIPREG+BA1->BA1_DIGITO,.f.,,,,,cCodOpe,,,,,,,,.T.,.f.,cNrAutPri)
                    P773AutCon("B4A",cNumProto,cCodOpe+cAnoAut+cMesAut+cNumAut)
                endif

                if lAudGuia .And. oItem["attachType"] <> "14" //Nao gera B53 para guias de radio
                    self:grvAuditoria(cNumAut)
                endIf

                End Transaction

            Endif                

        enddo
    enddo

    B4A->(dbCloseArea())
    
    hmFieldsCab:clean()

Return

Method trackItem(cAttachNumber) Class SyncClinicalAttach
	Local cEndPoint   := GetNewPar("MV_PHATURL","")
    Local cJson       := nil
    Local nX          := 0
    Local oClient     := nil
    Local aDadHeader  := {}
    Local aHeader     := {}
    
    //Verifica dados do header no arquivo plshat.ini
    aDadHeader := PLGDadHead()

    if !empty(cEndPoint)
        if Substr(cEndPoint,len(cEndPoint),1) <> "/"
	        cEndPoint += "/"
        endIf
        oClient := FWRest():New(cEndPoint)
        oClient:setPath("v1/clinicalAttachments/" + cAttachNumber)

        for nX := 1 to len(aDadHeader)
			aAdd(aHeader,aDadHeader[nX,1] + ": " + aDadHeader[nX,2])
		next

        cJson := '{"trackingStatus": 1}'
        lSuccess := oClient:Put(aHeader, cJson)

    endif

Return lSuccess

//-------------------------------------------------------------------
/*/{Protheus.doc} grvAuditoria
Gera registro na auditoria B53
@type  Class
@author sakai
@since 20190115
/*/
//-------------------------------------------------------------------
Method grvAuditoria(cNumGuia) Class SyncClinicalAttach

    Local o790C			:= nil
	Local aCabCri       := {}
	Local aDadCri		:= {}
	Local aVetCri       := {}
	Local aHeaderITE    := {}
	Local aColsITE      := {}
	Local aVetITE       := {}
    Inclui := .T.

    B4C->(dbSetOrder(1))
    BEG->(dbSetOrder(1))

    Store Header "B4C" TO aHeaderITE For .T.
    Store COLS "B4C" TO aColsITE FROM aHeaderITE VETTRAB aVetITE While; 
    B4C->(B4C_FILIAL+B4C_OPEMOV+B4C_ANOAUT+B4C_MESAUT+B4C_NUMAUT) == xFilial("B4C")+cNumGuia
          
    Store Header "BEG" TO aCabCri For .T.
    Store COLS "BEG" TO aDadCri FROM aCabCri VETTRAB aVetCri While;
    BEG->(BEG_FILIAL+BEG_OPEMOV+BEG_ANOAUT+BEG_MESAUT+BEG_NUMAUT) == xFilial("BEG")+cNumGuia

    o790C := PLSA790C():New(.T.)
    o790C:SetAuditoria(.T.,.F.,.F.,.F.,.F.,aDadCri,aCabCri,__aCdCri187[1],"0","BEG",aColsITE,aHeaderITE,"B4C",.T., .F.,"6")
    o790C:Destroy()

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} grvB2Z
Verifica se um registro existe na B52 e grava ele
@type  Class
@author sakai
@since 20190115
/*/
//-------------------------------------------------------------------
Method grvB2Z(hmFieldsB2Z,oItem,cSequen,cCodTab,cCodPro,nQtdAut,nSaldo) Class SyncClinicalAttach

    Local cSql    := ''
    Local cId     := cValToChar(oItem["id"])
    Local cCodRda := oItem["healthProvider"]["healthProviderId"]
    Local cSenha  := oItem["password"]
    Local cMatric := oItem["subscriberId"]
    Local cNumAut := oItem["attachNumber"]
    Local cCodOpe := Substr(cNumAut,1,4)
    Local cTipGui := iIf(oItem["journey"]=="13","07",iIf(oItem["journey"]=="14","08","09"))
    Default cCodTab  := ''
    Default cCodPro := ''
    Default nQtdAut := 0
    Default nSaldo  := 0

    cSql += " SELECT R_E_C_N_O_ FROM " + RetSqlName("B2Z")
    cSql += " WHERE B2Z_FILIAL = '"+xFilial("B2Z")+"' "
    cSql += " AND B2Z_OPEMOV = '"+cCodOpe+"' "
    cSql += " AND B2Z_CODRDA = '"+cCodRda+"' "
    cSql += " AND B2Z_SENHA  = '"+cSenha+"' "
    cSql += " AND B2Z_NUMAUT = '"+cNumAut+"' "
    cSql += " AND B2Z_MATRIC = '"+cMatric+"' "
    cSql += " AND B2Z_CODPAD = '"+cCodTab+"' "
    cSql += " AND B2Z_CODPRO = '"+cCodPro+"' "
    cSql += " AND D_E_L_E_T_ = ' ' "

    dbUseArea(.T.,"TOPCONN",TcGenQry(,,cSql),"TrbB2Z",.T.,.T.)

	If TrbB2Z->(Eof()) //So gero o registro se ele nao existir na base
        DbSelectArea("B2Z")
        B2Z->(dbAppend())
        FieldPut(hmFieldsB2Z:get("B2Z_FILIAL"), xFilial("B2Z"))
        FieldPut(hmFieldsB2Z:get("B2Z_IDORIG"), cId )
        FieldPut(hmFieldsB2Z:get("B2Z_OPEMOV"), PlsIntPad() )
        FieldPut(hmFieldsB2Z:get("B2Z_TIPGUI"), cTipGui )
        FieldPut(hmFieldsB2Z:get("B2Z_CODRDA"), cCodRda )
        FieldPut(hmFieldsB2Z:get("B2Z_SENHA" ), cSenha )
        FieldPut(hmFieldsB2Z:get("B2Z_NUMAUT"), cNumAut )
        FieldPut(hmFieldsB2Z:get("B2Z_MATRIC"), cMatric )
        FieldPut(hmFieldsB2Z:get("B2Z_DATPRO"), Stod(StrTran(oItem["requestedDate"],"-","")))
        FieldPut(hmFieldsB2Z:get("B2Z_SEQUEN"), cSequen )
        FieldPut(hmFieldsB2Z:get("B2Z_CODPAD"), cCodTab )
        FieldPut(hmFieldsB2Z:get("B2Z_CODPRO"), cCodPro )
        FieldPut(hmFieldsB2Z:get("B2Z_QTDAUT"), nQtdAut )
        FieldPut(hmFieldsB2Z:get("B2Z_SALDO") , nSaldo )
        B2Z->(dbcommit())
	EndIf
    TrbB2Z->(DbCloseArea())

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} syncCancel
Cancela guias

@type  Class
@author sakai
@since 20190115
/*/
//-------------------------------------------------------------------
Method syncCancel() Class SyncClinicalAttach

    Local nAuthId  := 0
    Local lCancel  := ""
    Local cCodTiss := GetNewPar("MV_MOTTISS","")

   	BTQ->(DBSetOrder(1)) //BTQ_FILIAL+BTQ_CODTAB+BTQ_CDTERM
	if BTQ->(msSeek(xFilial("BTQ")+"38"+cCodTiss))
		cCodTiss := BTQ->BTQ_CDTERM
    endIf

    while self:hasNext() .And. self:getDataFromClient()

        while self:hasItens()
            oItem    := self:getNextItem()
            nAuthId  := oItem["id"]
            lCancel  := oItem["isCancelled"]
            If lCancel
                Self:cancAttach(oItem,cCodTiss) 
            EndIf
        enddo

    enddo
    
Return


//-------------------------------------------------------------------
/*/{Protheus.doc} cancAttach
Cancela guias

@type  Class
@author sakai
@since 20190115
/*/
//-------------------------------------------------------------------
Method cancAttach(oItem,cCodTiss) Class SyncClinicalAttach

    Local cCodOpe      := PlsIntPad()
    Local cAuthNumber  := oItem["attachNumber"]
    Local cAnoAut      := substr(cAuthNumber, 5,4) 
    Local cMesAut      := substr(cAuthNumber, 9,2) 
    Local cNumAut      := substr(cAuthNumber, 11)
    Default cCodTiss   := ''

    if B4A->( MsSeek( xFilial("B4A")+cCodOpe+cAnoAut+cMesAut+cNumAut) )
        B4A->(RecLock("B4A",.F.))
        B4A->B4A_CANCEL := "1"
        B4A->B4A_STATUS := "3"
        B4A->B4A_CANTIS := cCodTiss
        B4A->B4A_STTISS := PLSANLSTIG(nil,.F.,.T.)
        B4A->(MsUnlock())
    endIf
            
    B4C->(dbGoTop())   
    if B4C->( MsSeek( xFilial("B4C")+cCodOpe+cAnoAut+cMesAut+cNumAut) )
        
        while !B4C->(Eof()) .And. B4A->(B4A_FILIAL+B4A_OPEMOV+B4A_ANOAUT+B4A_MESAUT+B4A_NUMAUT) == B4C->(B4C_FILIAL+B4C_OPEMOV+B4C_ANOAUT+B4C_MESAUT+B4C_NUMAUT)
            B4C->(RecLock("B4C",.F.))
            B4C->B4C_STATUS := "0"          
            B4C->(MsUnlock())

            B4C->(DbSkip())
        endDo

    endIf
    
    B53->(DbSetOrder(1)) //B53_FILIAL+B53_NUMGUI+B53_ORIMOV
    if B53->(DbSeek(xFilial('B53')+cCodOpe+cAnoAut+cMesAut+cNumAut))
        B53->(RecLock("B53",.F.))
        B53->B53_STATUS := "6"
        B53->B53_SITUAC := "0"
        B53->(MsUnlock())
    endIf

Return
