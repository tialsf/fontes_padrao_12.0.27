#INCLUDE "TBICONN.CH"   
#INCLUDE "TOTVS.CH"
#INCLUDE "AutSysLog.CH"
#INCLUDE "hatActions.ch"

/*/{Protheus.doc} SyncHandler
    Synchronizer que faz a ponte de dados HAT x PLS
    @type  Class
    @author victor.silva
    @since 20190115
/*/
Class SyncHandler
	
    Data lSuccess
    Data lHasNext
    Data nApiReference
    Data cMsgId
    Data oClient
    Data oLogger
    Data oRespBody
    Data aHeader
    Data hSync
    Data nCurrentItem
    Data nItemSize
    
    Data cPath
    Data aFields
    Data aExpand
    Data aQueryParams
    Data aOrder
    Data cPage
    Data cPageSize
    Data lLogHAT
    Data lAuto
    Data cJsonAuto

    Method New(nApiReference)
    
    Method name()
    Method initSyncString()
    Method setupRequest()
    Method setupLogger()
    
    Method addField(cField)
    Method addExpand(cExpand)
    Method addQueryParam(aQueryParam)
    Method addOrder(cOrder)
    Method setPage(cPage)
    Method setPageSize(cPageSize)
    Method getDataFromClient()
    Method getPage(cPage)
    Method getPageSize(cPageSize)
    Method generatePath()
    
    Method hasNext()
    Method hasItens()
    Method getNextItem()
    Method logInf(cMessage)
    Method destroy()

    
EndClass

Method New(nApiReference) Class SyncHandler
    self:nApiReference  := nApiReference
    self:cMsgId         := FWUUIDV4(.T.)
    self:aHeader        := {}
    self:aFields        := {}
    self:aExpand        := {}
    self:aQueryParams   := {}
    self:aOrder         := {}
    self:lSuccess       := .T.
    self:lHasNext       := .T.
    self:oClient        := nil
    self:oRespBody      := nil
    self:nCurrentItem   := 0
    self:nItemSize      := 0
    self:lLogHAT        := GetNewPar("MV_PHATLOG","0") == "1" 
    self:setupLogger()
    self:initSyncString()
    self:logInf("Classe de sincronizacao instanciada: " + GetClassName(self))
    self:lAuto          := .F.
    self:cJsonAuto      := ""
Return self

Method name() Class SyncHandler
Return self:hSync:get(self:nApiReference)

Method initSyncString() Class SyncHandler
    if empty(self:hSync)
        self:hSync := HashMap():New()
        self:hSync:set(SYNC_AUTHORIZATIONS, "authorizations")
        self:hSync:set(SYNC_CANCELLATIONS, "authorizations/cancel")
        self:hSync:set(SYNC_TREATMENT_EXTENSIONS,"clinicalAttachments")
        self:hSync:set(SYNC_CLINICAL_ATTACHMENTS, "treatmentExtensions")
    endif
Return

Method setupRequest() Class SyncHandler
	Local cEndPoint   := GetNewPar("MV_PHATURL","")
    Local lSerialized := .F.
    Local cStatusCode := ""
    Local oJson       := nil
    Local nX          := 0
    Local aDadHeader  := {}
    
    //Verifica dados do header no arquivo plshat.ini
    aDadHeader := PLGDadHead()

    if !empty(cEndPoint)
        if Substr(cEndPoint,len(cEndPoint),1) <> "/"
	        cEndPoint += "/"
        endIf
        self:oClient := FWRest():New(cEndPoint)
        self:oClient:setPath("v1/healthcheck")

        for nX := 1 to len(aDadHeader)
			aAdd(self:aHeader,aDadHeader[nX,1]+": "+aDadHeader[nX,2])
		next

        self:lSuccess := iif(self:lAuto,.T.,self:oClient:Get(self:aHeader))

        if self:lSuccess
            oJson := JsonObject():New()
            lSerialized := iif(self:lAuto,.T.,empty(oJson:fromJson(self:oClient:GetResult())) )

            if lSerialized
                self:logInf("Comunicacao com o HAT estabelecida com sucesso")
            else
                self:lSuccess := .F.
            endif

            FreeObj(oJson)
            oJson := nil
        else
            if empty(self:oClient:oResponseH:cStatusCode)
                cStatusCode := "500"
            else
                cStatusCode := self:oClient:oResponseH:cStatusCode
            endif
            self:logInf("HealthCheck do servidor do HAT retornou StatusCode:" + cStatusCode)
        endif
    endif

Return self:lSuccess

Method setupLogger() Class SyncHandler
    self:oLogger  := Logger():New()
    self:oLogger:setLevel(3)
    self:oLogger:setType(TYPE_SYSLOG)
    self:oLogger:setPath("")
    self:oLogger:setup()
Return

Method addField(cField) Class SyncHandler
    aAdd(self:aFields, cField)
Return

Method addExpand(cExpand) Class SyncHandler
    aAdd(self:aExpand, cExpand)
Return

Method addQueryParam(aQueryParam) Class SyncHandler
    aAdd(self:aQueryParams, aQueryParam)
Return

Method addOrder(cOrder) Class SyncHandler
    aAdd(self:aOrder, cOrder)
Return

Method setPage(cPage) Class SyncHandler
    self:cPage := cPage
Return

Method setPageSize(cPageSize) Class SyncHandler
    self:cPageSize := cPageSize
Return

Method getDataFromClient() Class SyncHandler
    Local cStatusCode := ""
    Local lSerialized := .F.

    if self:lSuccess
        self:oClient:setPath(self:cPath + "?" + self:getPage() + "&" + self:getPageSize() + self:generatePath())
        self:lSuccess := iif(self:lAuto,.T.,self:oClient:Get(self:aHeader))
        if self:lSuccess
            self:oRespBody := JsonObject():New()
            lSerialized := iif(self:lAuto,Empty(self:oRespBody:fromJson(self:cJsonAuto)),Empty(self:oRespBody:fromJson(self:oClient:GetResult())) )

            if lSerialized
                self:nCurrentItem := 1
                self:nItemSize := len(self:oRespBody["items"])
                if self:lHasNext := self:oRespBody["hasNext"]
                    self:setPage(Soma1(self:cPage))
                endif
            else
                self:lSuccess := .F.
                self:lHasNext := .F.
                self:logInf("Erro ao serializar resposta da API '" + self:name() + "'")
            endif

        else
            self:lSuccess := .F.
            self:lHasNext := .F.
            if empty(self:oClient:oResponseH:cStatusCode)
                cStatusCode := "500"
            else
                cStatusCode := self:oClient:oResponseH:cStatusCode
            endif
            self:logInf("Erro na comunicacao com a API '" + self:name() + "'. Status:" + cStatusCode)
        endif
    endif

Return self:lSuccess

Method getPage(cPage) Class SyncHandler
Return "page=" + self:cPage

Method getPageSize(cPageSize) Class SyncHandler
Return "pageSize=" + self:cPageSize

Method generatePath() Class SyncHandler
    Local cPath := ""
    Local nControl := 1
    Local nLenFields := len(self:aFields)
    Local nLenExpand := len(self:aExpand)
    Local nLenOrder := len(self:aOrder)
    Local nLenQueryParam := len(self:aQueryParams)

    // Aplica o parametro fields
    if nLenFields > 0
        cPath += "&fields="
        while nControl <= nLenFields
            cPath += self:aFields[nControl]
            iif(nControl < nLenFields,cPath += ",",nil)
            nControl++
        enddo
        nControl := 1
    endif

    // Aplica o parametro expand
    if nLenExpand > 0
        cPath += "&expand="
        while nControl <= nLenExpand
            cPath += self:aExpand[nControl]
            iif(nControl < nLenExpand,cPath += ",",nil)
            nControl++
        enddo
        nControl := 1
    endif

    // Aplica o parametro expand
    if nLenOrder > 0
        cPath += "&order="
        while nControl <= nLenOrder
            cPath += self:aOrder[nControl]
            iif(nControl < nLenOrder,cPath += ",",nil)
            nControl++
        enddo
        nControl := 1
    endif

    // Aplica os parametros queryString
    if nLenQueryParam > 0
        cPath += "&"
        while nControl <= nLenQueryParam
            cPath += self:aQueryParams[nControl][1] + "=" + self:aQueryParams[nControl][2]
            iif(nControl < nLenQueryParam,cPath += "&",nil)
            nControl++
        enddo
        nControl := 1
    endif

Return cPath

Method hasNext() Class SyncHandler
Return self:lHasNext

Method hasItens() Class SyncHandler
Return self:nCurrentItem <= self:nItemSize 

Method getNextItem() Class SyncHandler
    Local oItem := self:oRespBody["items"][self:nCurrentItem]
    self:nCurrentItem++
Return oItem

Method logInf(cMessage) Class SyncHandler

    if self:lLogHAT
        PLSLogHAT(cMessage)
    endif
Return

/*/{Protheus.doc} destroy
Limpa os objetos auxiliares
@author  victor.silva
@since   20190115
/*/
Method destroy() Class SyncHandler

    if !empty(self:oClient)
        FreeObj(self:oClient)
        self:oClient := nil
    endif

    DelClassIntf()

Return

//
