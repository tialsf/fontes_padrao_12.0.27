#INCLUDE "TOTVS.CH"
#INCLUDE "TBICONN.CH"   
#INCLUDE "PROTHEUS.CH"
#include "PLSMGER.CH"
#INCLUDE "hatActions.ch"

#define __aCdCri187 {"573","Demanda por requerimento"}

//-------------------------------------------------------------------
/*/{Protheus.doc} SyncTreatmentExtensions
    Realiza o sync do endpoint treatmentExtensions - Prorrogacao de Internacao

    @author  sakai
    @version P12
    @since   23/04/20
/*/
//------------------------------------------------------------------- 
Class SyncTreatmentExtensions from SyncHandler

    Data hmFieldsCab
    Data hmFieldsIte
    Data hmFieldsCri
    Data hmFieldsB2Z
    Data hmFieldsB43
    Data cEndPoint As String
    Data aDadHeader As Array
    Data aTabDup As Array

    Method New()
    Method sync()
    Method trackItem(cNumGui)
    Method getHashMap()
    Method grvCabB4Q(oItem)
    Method grvIteBQV(oItem,nProced)
    Method grvCriBQZ(oItem,nProced,nLenCri)
    Method grvPacB43(oItem,cSequen,cCodTab,cCodPro,dDatPro)
    Method grvB2Z(oItem,cSequen,cCodTab,cCodPro,nQtdAut,nSaldo,dDatPro)
    Method grvAuditoria(cNumGuia)
    Method posProf(cUF,cNumCR,cSigCR,cNome,cCGC)
    Method syncCancel()
    Method cancelTreat(oItem,cCodTiss)

EndClass


//-------------------------------------------------------------------
/*/{Protheus.doc} New
    Construtor da classe

    @author  sakai
    @version P12
    @since   23/04/20
/*/
//------------------------------------------------------------------- 
Method New() Class SyncTreatmentExtensions

    _Super:new(SYNC_TREATMENT_EXTENSIONS)
    self:cPath     := "v1/treatmentExtensions"
    self:aTabDup   := PlsBusTerDup(SuperGetMv("MV_TISSCAB", .F. ,"87")) 
    self:cEndPoint := GetNewPar("MV_PHATURL","")
    self:getHashMap()

Return self


//-------------------------------------------------------------------
/*/{Protheus.doc} sync
    Realiza a sincronizacao e grava as guias

    @author  sakai
    @version P12
    @since   23/04/20
/*/
//------------------------------------------------------------------- 
Method sync() Class SyncTreatmentExtensions

    Local nProced  := 0 
    Local lAudGuia := .F.

    //Posiciona indices
    BA1->(DbSetOrder(2)) //BA1_FILIAL+BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG+BA1_DIGITO
    B4Q->(DbSetOrder(1)) //B4Q_FILIAL+B4Q_OPEMOV+B4Q_ANOAUT+B4Q_MESAUT+B4Q_NUMAUT
    BAU->(DbSetOrder(1)) //BAU_FILIAL+BAU_CODIGO
    BE4->(DbSetOrder(2)) //BE4_FILIAL+BE4_CODOPE+BE4_ANOINT+BE4_MESINT+BE4_NUMINT

    //Verifica dados do header no arquivo plshat.ini
    self:aDadHeader := PLGDadHead()

    //Roda 50 eventos no maximo e verifica se ha pendentes (hasNext)
    while self:hasNext() .And. self:getDataFromClient()
        
        //Roda o json de resposta
        while self:hasItens()

            oItem    := self:getNextItem()
            cNumAut  := oItem["treatmentExtensionNumber"]
            lAudGuia := oItem["authorizationStatus"] == '6'

            If BA1->(DbSeek(xFilial("BA1")+oItem["subscriberId"])) .And. !B4Q->(DbSeek(xFilial('B4Q')+cNumAut))
                Begin Transaction
                
                //Grava cabecalho
                self:grvCabB4Q(oItem)
                
                //Grava eventos/criticas/B2Z
                For nProced := 1 to Len(oItem["procedures"])
                    self:grvIteBQV(oItem,nProced)
                Next nProced

                //Gera guia de auditoria
                if lAudGuia
                    self:grvAuditoria(cNumAut)
                endIf

                //Marca as guias que foram processadas
                self:trackItem(oItem["treatmentExtensionNumber"])

                End Transaction
            endIf

        endDo   

    endDo

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} grvCabB4Q
    Grava os dados do cabecalho

    @author  sakai
    @version P12
    @since   23/04/20
/*/
//------------------------------------------------------------------- 
Method grvCabB4Q(oItem) Class SyncTreatmentExtensions

    Local lAudGuia := oItem["authorizationStatus"] == '6'
    Local cGuia    := oItem["treatmentExtensionNumber"]
    Local oProf    := oItem["professional"]
    Local cOpeMov  := Substr(cGuia,1,4)
    Local cAnoAut  := Substr(cGuia,5,4)
    Local cMesAut  := Substr(cGuia,9,2)
    Local cNumAut  := Substr(cGuia,11,8)
    Local nAltura  := iif(empty(oItem["beneficiary"]["height"]), 0, oItem["beneficiary"]["height"])
    Local nPeso    := iif(empty(oItem["beneficiary"]["weight"]), 0, oItem["beneficiary"]["weight"])
    Local dDatPro  := iif(!Empty(oItem["authorizedDate"]),Stod(StrTran(oItem["authorizedDate"],"-","")),Stod(StrTran(oItem["requestedDate"],"-","")))
    Local cNumProt := oItem["attendanceProtocol"]
    Local cStatus  := oItem["authorizationStatus"]
    Local cCodRda  := ''
    Local cRdaName := ''

    if BAU->(DbSeek(xFilial('BAU')+oItem["healthProviderId"]))
        cCodRda  := BAU->BAU_CODIGO
        cRdaName := Alltrim(BAU->BAU_NOME)
    endIf

    DbSelectArea("B4Q")
    B4Q->(dbAppend())

    FieldPut(self:hmFieldsCab:get("B4Q_FILIAL"), xFilial("B4Q") )
    FieldPut(self:hmFieldsCab:get("B4Q_OPEMOV"), cOpeMov )
    FieldPut(self:hmFieldsCab:get("B4Q_NOMUSR"), Alltrim(BA1->BA1_NOMUSR) )
    FieldPut(self:hmFieldsCab:get("B4Q_STATUS"), cStatus )
    FieldPut(self:hmFieldsCab:get("B4Q_AUDITO"), iif(lAudGuia,"1","0") )
    FieldPut(self:hmFieldsCab:get("B4Q_CANCEL"), If(oItem["isCancelled"],"1","0") )
    FieldPut(self:hmFieldsCab:get("B4Q_ANOAUT"), cAnoAut )
    FieldPut(self:hmFieldsCab:get("B4Q_MESAUT"), cMesAut )
    FieldPut(self:hmFieldsCab:get("B4Q_NUMAUT"), cNumAut )
    FieldPut(self:hmFieldsCab:get("B4Q_GUIREF"), oItem["mainAuthorizationCode"] )
    FieldPut(self:hmFieldsCab:get("B4Q_DATPRO"), dDatPro )
    FieldPut(self:hmFieldsCab:get("B4Q_DATSOL"), Stod(StrTran(oItem["requestedDate"],"-","")) )
    FieldPut(self:hmFieldsCab:get("B4Q_SENHA") , oItem["password"] )
    FieldPut(self:hmFieldsCab:get("B4Q_GUIOPE"), oItem["idOnHealthProvider"] )
    FieldPut(self:hmFieldsCab:get("B4Q_MATANT"), BA1->BA1_MATANT )
    FieldPut(self:hmFieldsCab:get("B4Q_OPEUSR"), BA1->BA1_CODINT )
    FieldPut(self:hmFieldsCab:get("B4Q_CODEMP"), BA1->BA1_CODEMP )
    FieldPut(self:hmFieldsCab:get("B4Q_CONEMP"), BA1->BA1_CONEMP )
    FieldPut(self:hmFieldsCab:get("B4Q_SUBCON"), BA1->BA1_SUBCON )
    FieldPut(self:hmFieldsCab:get("B4Q_VERCON"), BA1->BA1_VERCON )
    FieldPut(self:hmFieldsCab:get("B4Q_VERSUB"), BA1->BA1_VERSUB )
    FieldPut(self:hmFieldsCab:get("B4Q_MATRIC"), BA1->BA1_MATRIC )
    FieldPut(self:hmFieldsCab:get("B4Q_TIPREG"), BA1->BA1_TIPREG )
    FieldPut(self:hmFieldsCab:get("B4Q_DIGITO"), BA1->BA1_DIGITO )
    FieldPut(self:hmFieldsCab:get("B4Q_PROATE"), oItem["attendanceProtocol"] )
    FieldPut(self:hmFieldsCab:get("B4Q_CODRDA"), cCodRda )
    FieldPut(self:hmFieldsCab:get("B4Q_NOMRDA"), cRdaName )
    FieldPut(self:hmFieldsCab:get("B4Q_ALTURA"), nAltura )
    FieldPut(self:hmFieldsCab:get("B4Q_PESO")  , nPeso )
    FieldPut(self:hmFieldsCab:get("B4Q_SUPCOR"), iif(nPeso > 0 .And. nAltura > 0,Sqrt((nPeso*nAltura)/3600),0)  )
    FieldPut(self:hmFieldsCab:get("B4Q_IDADE") , DateDiffYear( Date() , BA1->BA1_DATNAS ) )
    FieldPut(self:hmFieldsCab:get("B4Q_SEXO")  , BA1->BA1_SEXO )
    FieldPut(self:hmFieldsCab:get("B4Q_TELSOL"), oItem["professional"]["phoneNumber"] )
    FieldPut(self:hmFieldsCab:get("B4Q_EMASOL"), oItem["professional"]["email"] )
    FieldPut(self:hmFieldsCab:get("B4Q_STTISS"), PLSANLSTIG(oItem["authorizationStatus"]) )
    FieldPut(self:hmFieldsCab:get("B4Q_QTDADD"), iif(empty(oItem["dailyRequestedQuantity"]), 0, oItem["dailyRequestedQuantity"]) )
    FieldPut(self:hmFieldsCab:get("B4Q_QTDADA"), iif(empty(oItem["dailyAuthorizedQuantity"]), 0, oItem["dailyAuthorizedQuantity"]) )
    FieldPut(self:hmFieldsCab:get("B4Q_OPESOL"), cOpeMov )
    FieldPut(self:hmFieldsCab:get("B4Q_COMUNI"),  "0"  )
    FieldPut(self:hmFieldsCab:get("B4Q_NRAOPE"),  "" )  
    FieldPut(self:hmFieldsCab:get("B4Q_TIPACO"), oItem["requestedRoomType"] )
    FieldPut(self:hmFieldsCab:get("B4Q_TIPACA"), oItem["authorizedRoomType"] )
    FieldPut(self:hmFieldsCab:get("B4Q_INDCLI"), oItem["clinicalCondition"] )
    FieldPut(self:hmFieldsCab:get("B4Q_JUSOPE"), oItem["healthInsurerNote"] )
    FieldPut(self:hmFieldsCab:get("B4Q_JUSOBS"), oItem["attendanceNote"] )
    FieldPut(self:hmFieldsCab:get("B4Q_GUIPRE"), oItem["idOnHealthProvider"] )

    //Dados do Solicitante
    if self:posProf(oProf["stateAbbreviation"],;
                    oProf["professionalCouncilNumber"],;
                    oProf["professionalCouncil"],;
                    oProf["name"],;
                    oProf["professionalIdentifier"])  

        FieldPut(self:hmFieldsCab:get("B4Q_CDPFSO"), BB0->BB0_CODIGO )
        FieldPut(self:hmFieldsCab:get("B4Q_NOMSOL"), BB0->BB0_NOME )
        FieldPut(self:hmFieldsCab:get("B4Q_SIGLA") , oProf["professionalCouncil"] )
        FieldPut(self:hmFieldsCab:get("B4Q_REGSOL"), oProf["professionalCouncilNumber"] )
        FieldPut(self:hmFieldsCab:get("B4Q_ESTSOL"), oProf["stateAbbreviation"] )
   
    endIf

    B4Q->(dbcommit())

    if !Empty(cNumProt) .and. cStatus $  "2|6|3"
        gerRegB00(cNumProt,,"B4Q",.T.,.F.,BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG+BA1_DIGITO),.F.,,,,,cOpeMov,,,,,,,,.T.,.F.,oItem["mainAuthorizationCode"])
        P773AutCon("B4Q",cNumProt,cOpeMov+cAnoAut+cMesAut+cNumAut)
    endif

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} grvIteBQV
    Grava os eventos BQV

    @author  sakai
    @version P12
    @since   23/04/20
/*/
//------------------------------------------------------------------- 
Method grvIteBQV(oItem,nProced) Class SyncTreatmentExtensions

    Local oProced := oItem["procedures"][nProced]
    Local cGuia   := oItem["treatmentExtensionNumber"]
    Local cOpeMov := Substr(cGuia,1,4)
    Local cAnoAut := Substr(cGuia,5,4)
    Local cMesAut := Substr(cGuia,9,2)
    Local cNumAut := Substr(cGuia,11,8)
    Local cCodTab := AllTrim(PLSVARVINC('87','BR4',oProced["tableCode"]))
    Local cCodPro := AllTrim(PLSVARVINC(oProced["tableCode"],'BR8', oProced["procedureCode"], cCodTab+oProced["procedureCode"],,self:aTabDup,@cCodTab))
    Local cSequen := oProced["sequence"]  //strzero(nProced, Len(BQV->BQV_SEQUEN))
    Local dDatPro := iif(!Empty(oItem["authorizedDate"]),Stod(StrTran(oItem["authorizedDate"],"-","")),Stod(StrTran(oItem["requestedDate"],"-","")))
    Local nLenCri := 0

    BQV->(DbSetOrder(1)) //BQV_FILIAL, BQV_CODOPE, BQV_ANOINT, BQV_MESINT, BQV_NUMINT, BQV_SEQUEN
    if !BQV->(DbSeek(xFilial("BQV")+cGuia+cSequen))
        
        DbSelectArea("BQV")
        BQV->(dbAppend())

        FieldPut(self:hmFieldsIte:get("BQV_FILIAL"), xFilial("BQV") )
        FieldPut(self:hmFieldsIte:get("BQV_CODOPE"), cOpeMov )
        FieldPut(self:hmFieldsIte:get("BQV_ANOINT"), cAnoAut )
        FieldPut(self:hmFieldsIte:get("BQV_MESINT"), cMesAut )
        FieldPut(self:hmFieldsIte:get("BQV_NUMINT"), cNumAut )
        FieldPut(self:hmFieldsIte:get("BQV_CODPAD"), cCodTab )
        FieldPut(self:hmFieldsIte:get("BQV_CODPRO"), cCodPro )
        FieldPut(self:hmFieldsIte:get("BQV_QTDSOL"), iif(empty(oProced["requestedQuantity"]), 0, oProced["requestedQuantity"]) )
        FieldPut(self:hmFieldsIte:get("BQV_QTDPRO"), iif(empty(oProced["requestedQuantity"]), 0, oProced["requestedQuantity"])) //iif(empty(oProced["authorizedQuantity"]), 0, oProced["authorizedQuantity"]) )
        FieldPut(self:hmFieldsIte:get("BQV_OPEUSR"), BA1->BA1_CODINT )
        FieldPut(self:hmFieldsIte:get("BQV_CODEMP"), BA1->BA1_CODEMP )
        FieldPut(self:hmFieldsIte:get("BQV_MATRIC"), BA1->BA1_MATRIC )
        FieldPut(self:hmFieldsIte:get("BQV_TIPREG"), BA1->BA1_TIPREG )
        FieldPut(self:hmFieldsIte:get("BQV_DIGITO"), BA1->BA1_DIGITO )
        FieldPut(self:hmFieldsIte:get("BQV_SEQUEN"), cSequen )
        FieldPut(self:hmFieldsIte:get("BQV_DESPRO"), Posicione('BR8',1,xFilial('BR8') + cCodTab + cCodPro,'BR8_DESCRI'))  
        FieldPut(self:hmFieldsIte:get("BQV_DATPRO"), dDatPro )
        FieldPut(self:hmFieldsIte:get("BQV_HORPRO"), StrTran(Time(),":","") )
        FieldPut(self:hmFieldsIte:get("BQV_NIVCRI"), iif(oProced["status"] <> 1,"HAT","") )
        FieldPut(self:hmFieldsIte:get("BQV_TRACON"), "0" )
        FieldPut(self:hmFieldsIte:get("BQV_COMUNI"), "0" )
        FieldPut(self:hmFieldsIte:get("BQV_NRTROL"), "" )
        FieldPut(self:hmFieldsIte:get("BQV_SEQPTU"), "" )
        FieldPut(self:hmFieldsIte:get("BQV_NRAOPE"), "" )
        FieldPut(self:hmFieldsIte:get("BQV_REGSOL"), BB0->BB0_NUMCR )
        FieldPut(self:hmFieldsIte:get("BQV_NOMSOL"), BB0->BB0_NOME )
        FieldPut(self:hmFieldsIte:get("BQV_ATERNA"), iif(oItem["beneficiary"]["newbornAttendance"]=="S","1","0") )
        FieldPut(self:hmFieldsIte:get("BQV_TIPDIA"), "1" )
        FieldPut(self:hmFieldsIte:get("BQV_CANCEL"), iif(oItem["authorizationStatus"] == "9", "1", "0") )
        FieldPut(self:hmFieldsIte:get("BQV_CHVNIV"), oProced["authLevelKey"] )
        FieldPut(self:hmFieldsIte:get("BQV_NIVAUT"), oProced["authLevel"] ) 
        FieldPut(self:hmFieldsIte:get("BQV_AUDITO"), iif(oProced["status"] == 2, "1", "0") )
        FieldPut(self:hmFieldsIte:get("BQV_STATUS"), iif(oProced["status"] == 1, "1", "0") )
     
        BQV->(dbcommit())

        //Processa as criticas
        nLenCri := iif(!empty(oItem["procedures"][nProced]["rejectionCauses"]),;
                        Len(oItem["procedures"][nProced]["rejectionCauses"]), 0)
        if nLenCri > 0
            self:grvCriBQZ(oItem,nProced,nLenCri)
        endIf

        //Gera historico importacao HAT - B2Z
        dbSelectArea("B2Z")
        self:grvB2Z(oItem,cSequen,cCodTab,cCodPro,oProced["requestedQuantity"],oProced["requestedQuantity"],dDatPro)

    endIf

    //Grava composicao dos pacotes
    if oProced["tableCode"] $ "90/98"
        self:grvPacB43(oItem,cSequen,cCodTab,cCodPro,dDatPro)
    endIf

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} grvPacB43
    Grava os pacotes B43

    @author  sakai
    @version P12
    @since   23/04/20
/*/
//------------------------------------------------------------------- 
Method grvPacB43(oItem,cSequen,cCodTab,cCodPro,dDatPro) Class SyncTreatmentExtensions

    Local cCodRda      := ''
    Local nI           := 0 
    Local aItensPac    := {}
    Local cAuthNumber  := oItem["treatmentExtensionNumber"]
    Local cCodOpe      := Substr(cAuthNumber,1,4)
    Local cAnoAut      := substr(cAuthNumber,5,4) 
    Local cMesAut      := substr(cAuthNumber,9,2) 
    Local cNumAut      := substr(cAuthNumber,11,8)
        
    if BAU->(DbSeek(xFilial('BAU')+oItem["healthProviderId"]))
        cCodRda := BAU->BAU_CODIGO
    endIf

    aItensPac := PlRetPac(cCodOpe,cCodRDA,cCodTab,cCodPro,,dDatPro,.F.)

    if Len(aItensPac) > 0

        for nI := 1 To Len(aItensPac)

            DbSelectArea("B43")
            B43->(dbAppend())
            FieldPut(self:hmFieldsB43:get("B43_FILIAL"), xFilial("B43"))
            FieldPut(self:hmFieldsB43:get("B43_OPEMOV"), cCodOpe)
            FieldPut(self:hmFieldsB43:get("B43_ANOAUT"), cAnoAut)
            FieldPut(self:hmFieldsB43:get("B43_MESAUT"), cMesAut)
            FieldPut(self:hmFieldsB43:get("B43_NUMAUT"), cNumAut)
            FieldPut(self:hmFieldsB43:get("B43_SEQUEN"), cSequen)
            FieldPut(self:hmFieldsB43:get("B43_CODPAD"), aItensPac[nI,1] )
            FieldPut(self:hmFieldsB43:get("B43_CODPRO"), aItensPac[nI,2] )
            FieldPut(self:hmFieldsB43:get("B43_DESPRO"), Posicione('BR8',1,xFilial('BR8') + ALLTRIM(aItensPac[nI,1]) + ALLTRIM(aItensPac[nI,2]),'BR8_DESCRI') )
            FieldPut(self:hmFieldsB43:get("B43_VALCH") , aItensPac[nI,4] )
            FieldPut(self:hmFieldsB43:get("B43_VALFIX"), aItensPac[nI,5] )
            FieldPut(self:hmFieldsB43:get("B43_PRINCI"), aItensPac[nI,6] )
            FieldPut(self:hmFieldsB43:get("B43_TIPO")  , aItensPac[nI,3] )
            FieldPut(self:hmFieldsB43:get("B43_ORIMOV"), '6')
            FieldPut(self:hmFieldsB43:get("B43_NIVPAC"), aItensPac[nI,10] )
            B43->(dbcommit())

        next nI
    
    endIf 

Return 



//-------------------------------------------------------------------
/*/{Protheus.doc} grvCriBQZ
    Grava as criticas BQZ

    @author  sakai
    @version P12
    @since   23/04/20
/*/
//------------------------------------------------------------------- 
Method grvCriBQZ(oItem,nProced,nLenCri) Class SyncTreatmentExtensions

    Local nCritica := 1
    Local cGuia    := oItem["treatmentExtensionNumber"]
    Local cOpeMov  := Substr(cGuia,1,4)
    Local cAnoAut  := Substr(cGuia,5,4)
    Local cMesAut  := Substr(cGuia,9,2)
    Local cNumAut  := Substr(cGuia,11,8)
    Local cSequen  := oItem["procedures"][nProced]["sequence"] //strzero(nProced, Len(BQZ->BQZ_SEQUEN))

    while nCritica <= nLenCri

        oCritica := oItem["procedures"][nProced]["rejectionCauses"][nCritica]

        BCT->(dbGoTop())
        if BCT->(msSeek(xFilial('BCT')+cOpeMov+oCritica["code"]))

            DbSelectArea("BQZ")
            BQZ->(dbAppend())
            
            FieldPut(self:hmFieldsCri:get("BQZ_FILIAL"), xFilial("BQZ") )
            FieldPut(self:hmFieldsCri:get("BQZ_CODOPE"), cOpeMov )
            FieldPut(self:hmFieldsCri:get("BQZ_ANOINT"), cAnoAut )
            FieldPut(self:hmFieldsCri:get("BQZ_MESINT"), cMesAut )
            FieldPut(self:hmFieldsCri:get("BQZ_NUMINT"), cNumAut )
            FieldPut(self:hmFieldsCri:get("BQZ_SEQUEN"), cSequen )
            FieldPut(self:hmFieldsCri:get("BQZ_CODGLO"), oCritica["code"] )
            FieldPut(self:hmFieldsCri:get("BQZ_DESGLO"), BCT->BCT_DESCRI )
            FieldPut(self:hmFieldsCri:get("BQZ_SEQCRI"), strzero(nCritica, 3) )
            FieldPut(self:hmFieldsCri:get("BQZ_TIPO")  , BCT->BCT_TIPO )
            FieldPut(self:hmFieldsCri:get("BQZ_CODEDI"), BCT->BCT_CODED2 )
              
            BQZ->(dbcommit())
        endIf
        nCritica++
    endDo

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} grvB2Z
Verifica se um registro existe na B52 e grava ele
@type  Class
@author sakai
@since 20190115
/*/
//-------------------------------------------------------------------
Method grvB2Z(oItem,cSequen,cCodTab,cCodPro,nQtdAut,nSaldo,dDatPro) Class SyncTreatmentExtensions

    Local cSql    := ''
    Local cId     := cValToChar(oItem["id"])
    Local cCodRda := oItem["healthProvider"]["healthProviderId"]
    Local cSenha  := oItem["password"]
    Local cMatric := oItem["subscriberId"]
    Local cNumAut := oItem["treatmentExtensionNumber"]
    Local cCodOpe := Substr(cNumAut,1,4)
    Default cCodTab  := ''
    Default cCodPro  := ''
    Default nQtdAut  := 0
    Default nSaldo   := 0

    cSql += " SELECT R_E_C_N_O_ FROM " + RetSqlName("B2Z")
    cSql += " WHERE B2Z_FILIAL = '"+xFilial("B2Z")+"' "
    cSql += " AND B2Z_OPEMOV = '"+cCodOpe+"' "
    cSql += " AND B2Z_CODRDA = '"+cCodRda+"' "
    cSql += " AND B2Z_SENHA  = '"+cSenha+"' "
    cSql += " AND B2Z_NUMAUT = '"+cNumAut+"' "
    cSql += " AND B2Z_MATRIC = '"+cMatric+"' "
    cSql += " AND B2Z_CODPAD = '"+cCodTab+"' "
    cSql += " AND B2Z_CODPRO = '"+cCodPro+"' "
    cSql += " AND D_E_L_E_T_ = ' ' "

    dbUseArea(.T.,"TOPCONN",TcGenQry(,,cSql),"TrbB2Z",.T.,.T.)

	If TrbB2Z->(Eof()) //So gero o registro se ele nao existir na base
        DbSelectArea("B2Z")
        B2Z->(dbAppend())
        FieldPut(self:hmFieldsB2Z:get("B2Z_FILIAL"), xFilial("B2Z"))
        FieldPut(self:hmFieldsB2Z:get("B2Z_IDORIG"), cId )
        FieldPut(self:hmFieldsB2Z:get("B2Z_OPEMOV"), PlsIntPad() )
        FieldPut(self:hmFieldsB2Z:get("B2Z_TIPGUI"), "11" )
        FieldPut(self:hmFieldsB2Z:get("B2Z_CODRDA"), cCodRda )
        FieldPut(self:hmFieldsB2Z:get("B2Z_SENHA" ), cSenha )
        FieldPut(self:hmFieldsB2Z:get("B2Z_NUMAUT"), cNumAut )
        FieldPut(self:hmFieldsB2Z:get("B2Z_MATRIC"), cMatric )
        FieldPut(self:hmFieldsB2Z:get("B2Z_DATPRO"), dDatPro )
        FieldPut(self:hmFieldsB2Z:get("B2Z_SEQUEN"), cSequen )
        FieldPut(self:hmFieldsB2Z:get("B2Z_CODPAD"), cCodTab )
        FieldPut(self:hmFieldsB2Z:get("B2Z_CODPRO"), cCodPro )
        FieldPut(self:hmFieldsB2Z:get("B2Z_QTDAUT"), nQtdAut )
        FieldPut(self:hmFieldsB2Z:get("B2Z_SALDO") , nSaldo )
        B2Z->(dbcommit())
	EndIf

    TrbB2Z->(DbCloseArea())

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} trackItem
    Marca as guias que foram processadas

    @author  sakai
    @version P12
    @since   23/04/20
/*/
//------------------------------------------------------------------- 
Method trackItem(cNumGui) Class SyncTreatmentExtensions

    Local cJson   := '{"trackingStatus": 1}'
    Local nX      := 0
    Local oClient := nil
    Local aHeader := {}
    
    if !empty(self:cEndPoint)
        if Substr(self:cEndPoint,len(self:cEndPoint),1) <> "/"
	        self:cEndPoint += "/"
        endIf
        oClient := FWRest():New(self:cEndPoint)
        oClient:setPath("v1/treatmentExtensions/" + cNumGui)

        for nX := 1 to len(self:aDadHeader)
			aAdd(aHeader,self:aDadHeader[nX,1] + ": " + self:aDadHeader[nX,2])
		next

        lSuccess := oClient:Put(aHeader, cJson)
    endif

Return lSuccess


//-------------------------------------------------------------------
/*/{Protheus.doc} grvAuditoria
Gera registro na auditoria B53
@type  Class
@author sakai
@since 20190115
/*/
//-------------------------------------------------------------------
Method grvAuditoria(cNumGuia) Class SyncTreatmentExtensions

    Local o790C			:= nil
	Local aCabCri       := {}
	Local aDadCri		:= {}
	Local aVetCri       := {}
	Local aHeaderITE    := {}
	Local aColsITE      := {}
	Local aVetITE       := {}
    Inclui := .T.

    BQV->(dbSetOrder(1))
    BQZ->(dbSetOrder(1))

    Store Header "BQV" TO aHeaderITE For .T.
    Store COLS "BQV" TO aColsITE FROM aHeaderITE VETTRAB aVetITE While; 
    BQV->(BQV_FILIAL+BQV_CODOPE+BQV_ANOINT+BQV_MESINT+BQV_NUMINT) == xFilial("BQV")+cNumGuia
          
    Store Header "BQZ" TO aCabCri For .T.
    Store COLS "BQZ" TO aDadCri FROM aCabCri VETTRAB aVetCri While;
    BQZ->(BQZ_FILIAL+BQZ_CODOPE+BQZ_ANOINT+BQZ_MESINT+BQZ_NUMINT) == xFilial("BQZ")+cNumGuia
          
    o790C := PLSA790C():New(.T.)
    o790C:SetAuditoria(.T.,.F.,.F.,.F.,.F.,aDadCri,aCabCri,__aCdCri187[1],"0","BQZ",aColsITE,aHeaderITE,"BQV",.F., .T.,"6")
    o790C:Destroy()

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} getHashMap
    Carrega os hashmaps de Headers

    @author  sakai
    @version P12
    @since   23/04/20
/*/
//------------------------------------------------------------------- 
Method getHashMap() Class SyncTreatmentExtensions

    Local nLenStruct := 0
    Local nControl   := 0
    Local aStrucB4Q  := {}
    Local aStrucBQV  := {}
    Local aStrucBQZ  := {}
    Local aStrucB2Z  := {}
    Local aStrucB43  := {}

    self:hmFieldsCab := HashMap():New()
    self:hmFieldsIte := HashMap():New()
    self:hmFieldsCri := HashMap():New()
    self:hmFieldsB2Z := HashMap():New()
    self:hmFieldsB43 := HashMap():New()
    //Monta HashMap do Cabecalho B4Q
    DbSelectArea("B4Q")
    nControl   := 1
    aStrucB4Q  := B4Q->(dbStruct())
    nLenStruct := Len(aStrucB4Q)

    while nControl <= nLenStruct
        self:hmFieldsCab:set(aStrucB4Q[nControl,1],nControl)
        nControl++
    enddo
    
    //Monta HashMap dos Eventos BQV
    DbSelectArea("BQV")
    nControl   := 1
    aStrucBQV  := BQV->(dbStruct())
    nLenStruct := Len(aStrucBQV)

    while nControl <= nLenStruct
        self:hmFieldsIte:set(aStrucBQV[nControl,1],nControl)
        nControl++
    enddo

    //Monta HashMap das Criticas BQZ
    DbSelectArea("BQZ")
    nControl   := 1
    aStrucBQZ  := BQZ->(dbStruct())
    nLenStruct := Len(aStrucBQZ)

    while nControl <= nLenStruct
        self:hmFieldsCri:set(aStrucBQZ[nControl,1],nControl)
        nControl++
    enddo

    //Monta HashMap do Cabecalho B2Z
    DbSelectArea("B2Z")
    nControl   := 1
    aStrucB2Z  := B2Z->(dbStruct())
    nLenStruct := Len(aStrucB2Z)

    while nControl <= nLenStruct
        self:hmFieldsB2Z:set(aStrucB2Z[nControl,1],nControl)
        nControl++
    enddo

    //Monta HashMap do Pacote B43
    DbSelectArea("B43")
    nControl   := 1
    aStrucB43  := B43->(dbStruct())
    nLenStruct := Len(aStrucB43)

    while nControl <= nLenStruct
        self:hmFieldsB43:set(aStrucB43[nControl,1],nControl)
        nControl++
    enddo

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} posProf
Posiciona/Grava Profissional de Saude

@type  Class
@author sakai
@since 20190115
/*/
//-------------------------------------------------------------------
Method posProf(cUF,cNumCR,cSigCR,cNome,cCGC) Class SyncTreatmentExtensions

    Local cCodOpe  := PlsIntPad()
    Default cUF    := ""
    Default cNumCR := ""
    Default cSigCR := ""
    Default cNome  := ""
    Default cCGC   := ""

    cUF    := Upper(Padr(AllTrim(cUF),tamSX3("BB0_ESTADO")[1]))
    cNumCR := Padr(AllTrim(cNumCR),tamSX3("BB0_NUMCR")[1])
    cSigCR := Padr(AllTrim(cSigCR),tamSX3("BB0_CODSIG")[1])

    BB0->(dbSetOrder(4)) //BB0_FILIAL+BB0_ESTADO+BB0_NUMCR+BB0_CODSIG+BB0_CODOPE
   
	If !Empty(cUf) .and. !BB0->( MsSeek( xFilial("BB0")+cUf+cNumCR+cSigCR+cCodOpe ) )
		BB0->(RecLock("BB0",.T.))
		BB0->BB0_FILIAL	:= xFilial("BB0")
        BB0->BB0_CODIGO	:= GetSx8Num("BB0","BB0_CODIGO")
		BB0->BB0_ESTADO	:= cUf
		BB0->BB0_UF		:= cUf
		BB0->BB0_NUMCR	:= cNumCR
		BB0->BB0_CODSIG	:= cSigCR
		BB0->BB0_CODOPE	:= cCodOpe
		BB0->BB0_NOME	:= cNome
		BB0->BB0_CGC    := cCGC
        BB0->BB0_VINC	:= "2"
		BB0->(MsUnlock())
        ConfirmSX8()
	EndIf
    
    BB0->(dbGoTop())

Return BB0->(DbSeek(xFilial('BB0')+cUF+cNumCR+cSigCR+cCodOpe))


//-------------------------------------------------------------------
/*/{Protheus.doc} syncCancel
Cancela guias

@type  Class
@author sakai
@since 20190115
/*/
//-------------------------------------------------------------------
Method syncCancel() Class SyncTreatmentExtensions

    Local nAuthId  := 0
    Local lCancel  := ""
    Local cCodTiss := GetNewPar("MV_MOTTISS","")

   	BTQ->(DBSetOrder(1)) //BTQ_FILIAL+BTQ_CODTAB+BTQ_CDTERM
	if BTQ->(msSeek(xFilial("BTQ")+"38"+cCodTiss))
		cCodTiss := BTQ->BTQ_CDTERM
    endIf

    while self:hasNext() .And. self:getDataFromClient()

        while self:hasItens()
            oItem    := self:getNextItem()
            nAuthId  := oItem["id"]
            lCancel  := oItem["isCancelled"]
            If lCancel
                Self:cancelTreat(oItem,cCodTiss) 
            EndIf
        enddo

    enddo
    
Return


//-------------------------------------------------------------------
/*/{Protheus.doc} cancelTreat
Cancela guias

@type  Class
@author sakai
@since 20190115
/*/
//-------------------------------------------------------------------
Method cancelTreat(oItem,cCodTiss) Class SyncTreatmentExtensions

    Local cCodOpe      := PlsIntPad()
    Local cAuthNumber  := oItem["treatmentExtensionNumber"]
    Local cAnoAut      := substr(cAuthNumber, 5,4) 
    Local cMesAut      := substr(cAuthNumber, 9,2) 
    Local cNumAut      := substr(cAuthNumber, 11)
    Default cCodTiss   := ''

    if B4Q->( MsSeek( xFilial("B4Q")+cCodOpe+cAnoAut+cMesAut+cNumAut) )
        B4Q->(RecLock("B4Q",.F.))
        B4Q->B4Q_CANCEL := "1"      
        B4Q->B4Q_STATUS := "3"          
        B4Q->B4Q_CANTIS := cCodTiss
        B4Q->B4Q_STTISS := PLSANLSTIG(nil,.F.,.T.)
        B4Q->(MsUnlock())
    endIf
            
    BQV->(dbGoTop())   
    if BQV->( MsSeek( xFilial("BQV")+cCodOpe+cAnoAut+cMesAut+cNumAut) )
 
        while !BQV->(Eof()) .And. B4Q->(B4Q_FILIAL+B4Q_OPEMOV+B4Q_ANOAUT+B4Q_MESAUT+B4Q_NUMAUT) == BQV->(BQV_FILIAL+BQV_CODOPE+BQV_ANOINT+BQV_MESINT+BQV_NUMINT)
            BQV->(RecLock("BQV",.F.))
            BQV->BQV_STATUS := "0"          
            BQV->(MsUnlock())

            BQV->(DbSkip())
        endDo

    endIf

    B53->(DbSetOrder(1)) //B53_FILIAL+B53_NUMGUI+B53_ORIMOV
    if B53->(DbSeek(xFilial('B53')+cCodOpe+cAnoAut+cMesAut+cNumAut))
        B53->(RecLock("B53",.F.))
        B53->B53_STATUS := "6"      
        B53->B53_SITUAC := "0"
        B53->(MsUnlock())
    endIf
 
Return