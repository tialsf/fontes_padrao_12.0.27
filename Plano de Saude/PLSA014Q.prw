#Include 'Protheus.ch'
#Include 'FWMVCDEF.CH'
#Include "topconn.ch"
#Include "APWIZARD.CH"    
#Include "FWBROWSE.CH"

//---------------------------------------------------------------------------------
/*/{Protheus.doc} PLSQRREJ
Query da tela inicial, para exibir o hist�rico encontrado nas tabelas de n�veis.

@author  Renan Martins	
@version P12
@since   07/2016
/*/
//---------------------------------------------------------------------------------
Function PLSQRREJ(cValor, oModel)
Local oModel := oModel
Local cValor := ""
Local cSql := ""

cValor := FwFldGet('B4S_CODIGO') //oModel:GetModel('B4SMASTER'):GetValue('B4S_CODIGO')

cSql := " SELECT 'BC6' TABELA, BC6_FILIAL FILIAL, BC6_CODRDA    RDA, BC6_CODTAB CODPAD, BC6_CODPRO CODPRO, BC6_USPCO USCOPGT,"
cSql += " BC6_VRPCO   USREPGT, BC6_USPPP USPPPGT, BC6_VRPPP USREPGO, 0          BANPGT, 0      UCOPGT, "
cSql += " 0            VLREAL, 0           CHPGT, 0          VLFIXO, 0      VLUSSAUDE"
cSql += " FROM " + RetSQLName( "BC6" ) 
cSql += " WHERE BC6_CODREA = '" + cValor + "'"      
       
cSql += " UNION "


cSql += " SELECT 'BC5' TABELA, BC5_FILIAL FILIAL, BC5_CODRDA    RDA, BC5_CODTAB CODPAD, '' CODPRO        , BC5_VPCO USCOPGT, "
cSql += " 0           USREPGT, BC5_VPPP USPPPGT , 0         USREPGO, BC5_BANDAP BANPGT, BC5_UCO    UCOPGT, "
cSql += " 0            VLREAL, 0           CHPGT, 0          VLFIXO, 0      VLUSSAUDE "
cSql += " FROM " + RetSQLName( "BC5" ) 
cSql += " WHERE BC5_CODREA = '" + cValor + "'"       

cSql += " UNION "


cSql += " SELECT 'BE9' TABELA, BE9_FILIAL FILIAL, BE9_CODIGO    RDA, BE9_CODTAB  CODPAD, BE9_CODPRO CODPRO, 0       USCOPGT, "
cSql += " 0           USREPGT, 0         USPPPGT, 0         USREPGO, BE9_BANDA   BANPGT, 0          UCOPGT, "
cSql += " BE9_VALREA   VLREAL, BE9_VALCH   CHPGT, 0          VLFIXO, 0       VLUSSAUDE "
cSql += " FROM " + RetSQLName( "BE9" ) 
cSql += " WHERE BE9_CODREA = '" + cValor + "'"       

cSql += " UNION "


cSql += " SELECT 'BBI' TABELA, BBI_FILIAL FILIAL, BBI_CODIGO    RDA, BBI_CODTAB  CODPAD, BBI_CODPRO CODPRO, 0        USCOPGT, "
cSql += " 0           USREPGT, 0         USPPPGT, 0         USREPGO, BBI_BANDA   BANPGT, BBI_UCO    UCOPGT, "
cSql += " 0            VLREAL, BBI_VALCH   CHPGT, 0          VLFIXO, 0         VLUSSAUDE "   // from BBIT10  ///*BBI_CODPRO � c�digo do produto/plano*//*CH � igual U.S?*/
cSql += " FROM " + RetSQLName( "BBI" ) 
cSql += " WHERE BBI_CODREA = '" + cValor + "'"      

cSql += " UNION "       


cSql += " SELECT 'BC0' TABELA, BC0_FILIAL FILIAL, BC0_CODIGO    RDA, BC0_CODTAB     CODPAD, BC0_CODOPC    CODPRO, 0       USCOPGT, "
cSql += " 0           USREPGT, 0         USPPPGT, 0         USREPGO, BC0_BANDA      BANPGT, BC0_UCO       UCOPGT, "
cSql += " BC0_VALREA   VLREAL, BC0_VALCH   CHPGT, 0          VLFIXO, 0        VLUSSAUDE "
cSql += " FROM " + RetSQLName( "BC0" ) 
cSql += " WHERE BC0_CODREA = '" + cValor + "'"       

cSql += " UNION "


cSql += " SELECT 'BCK' TABELA, BCK_FILIAL FILIAL, BCK_CODIGO    RDA, ''          CODPAD, ''         CODPRO, 0       USCOPGT, "
cSql += " 0           USREPGT, 0         USPPPGT, 0         USREPGO, 0           BANPGT, 0          UCOPGT, "
cSql += " 0            VLREAL, 0           CHPGT, 0          VLFIXO, 0        VLUSSAUDE "    //from BCKT10  /*bcK_codUNID E BCK_us*/
cSql += " FROM " + RetSQLName( "BCK" ) 
cSql += " WHERE BCK_CODREA = '" + cValor + "'"      

cSql += " UNION "


cSql += " SELECT 'BAX' TABELA, BAX_FILIAL FILIAL, BAX_CODIGO    RDA, ''          CODPAD, ''         CODPRO, 0       USCOPGT, "
cSql += " 0           USREPGT, 0         USPPPGT, 0         USREPGO, BAX_BANDA   BANPGT, BAX_UCO    UCOPGT, "
cSql += " 0            VLREAL, BAX_VALCH   CHPGT, 0          VLFIXO, 0        VLUSSAUDE     "//from BAXT10  
cSql += " FROM " + RetSQLName( "BAX" ) 
cSql += " WHERE BAX_CODREA = '" + cValor + "'"      

cSql += " UNION "


cSql += " SELECT 'BB8' TABELA, BB8_FILIAL FILIAL, BB8_CODIGO    RDA, ''          CODPAD, ''         CODPRO, 0       USCOPGT, "
cSql += " 0           USREPGT, 0         USPPPGT, 0         USREPGO, BB8_BANDA   BANPGT, BB8_UCO    UCOPGT, "
cSql += " 0            VLREAL, BB8_VALCH   CHPGT, 0          VLFIXO, 0        VLUSSAUDE  " //   from BB8T10  "
cSql += " FROM " + RetSQLName( "BB8" ) 
cSql += " WHERE BB8_CODREA = '" + cValor + "'"      

cSql += " UNION "


cSql += " SELECT 'B30' TABELA, B30_FILIAL FILIAL, B30_CODIGO    RDA, B30_CODTAB  CODPAD, B30_CODPRO CODPRO, 0       USCOPGT, "
cSql += " 0           USREPGT, 0         USPPPGT, 0         USREPGO, B30_BANDA   BANPGT, B30_UCO    UCOPGT, "
cSql += " B30_VALCH    VLREAL, 0           CHPGT, 0          VLFIXO, 0        VLUSSAUDE  "//   from B30T10 /*B30_CODPRO � PRODUTO*/
cSql += " FROM " + RetSQLName( "B30" ) 
cSql += " WHERE B30_CODREA = '" + cValor + "'"     

cSql += " UNION "


cSql += " SELECT 'BMI' TABELA, BMI_FILIAL FILIAL, BMI_CODRDA    RDA, BMI_CODPSA   CODPAD, ''         CODPRO, 0        USCOPGT, "
cSql += " 0           USREPGT, 0         USPPPGT, 0         USREPGO, BMI_BANDA    BANPGT,  BMI_UCO   UCOPGT, "
cSql += " 0            VLREAL, 0           CHPGT, 0          VLFIXO, BMI_VALUS  VLUSSAUDE  "   //from BMIT10  /*BMI_VALUS?? BMI_VLRPAG??*/
cSql += " FROM " + RetSQLName( "BMI" ) 
cSql += " WHERE BMI_CODREA = '" + cValor + "'"       

cSql += " UNION "


cSql += " SELECT 'BMH' TABELA, BMH_FILIAL FILIAL, ''            RDA, ''           CODPAD, ''         CODPRO, 0        USCOPGT, "
cSql += " 0           USREPGT, 0         USPPPGT, 0         USREPGO, BMH_BANDA    BANPGT, BMH_UCO    UCOPGT, "
cSql += " 0            VLREAL, 0           CHPGT, 0          VLFIXO, BMH_VALUS    VLUSSAUDE  " //   from BMHT10  /*BMH_VALUS?? BMI_VLRPAG??*/
cSql += " FROM " + RetSQLName( "BMH" ) 
cSql += " WHERE BMH_CODREA = '" + cValor + "'"       

cSql += " UNION "


cSql += " SELECT 'BLY' TABELA, BLY_FILIAL FILIAL, BLY_CODRDA      RDA, ''          CODPAD, BLY_CODOPC CODPRO, 0        USCOPGT, "
cSql += " 0           USREPGT, 0         USPPPGT, 0           USREPGO, 0           BANPGT, 0          UCOPGT, "
cSql += " BLY_VALCH    VLREAL, 0           CHPGT, BLY_VALFIX   VLFIXO, 0        VLUSSAUDE  "
cSql += " FROM " + RetSQLName( "BLY" ) 
cSql += " WHERE BLY_CODREA = '" + cValor + "'"       

Return cSql


//---------------------------------------------------------------------------------
/*/{Protheus.doc} PLSQRY2RP
Query para retornar os dados dos niveis ap�s escolha da RDA - Painel de Sele��o de Nivel

@author  Renan Martins	
@version P12
@since   07/2016
/*/
//---------------------------------------------------------------------------------
Function PLSQRY2RP(cCodRDA)
Local cSql := ""

cSql := " SELECT 'BC6' TABELA, 'RDA x Tabela de Pre�os (Itens) - RDA x Tabela de Pre�o / Complemento / Procedimentos' DESCRI, BC6_FILIAL, "
cSql += " COUNT(BC6_CODRDA) QTD FROM " + RetSQLName ("BC6") 
cSql += " WHERE BC6_CODRDA = '" + cCodRDA + "' AND D_E_L_E_T_ = '' GROUP BY BC6_FILIAL, BC6_CODRDA "

cSql += " UNION "

cSql += " SELECT 'BC5' TABELA,'RDA x Tabela de Pre�os (Cabe�alho) - RDA x Tabela de Pre�o' DESCRI,  BC5_FILIAL, COUNT(BC5_CODRDA) QTD " 
cSql += " FROM " + RetSQLName ("BC5") 
cSql += " WHERE BC5_CODRDA = '" + cCodRDA + "' AND D_E_L_E_T_ = '' GROUP BY BC5_FILIAL, BC5_CODRDA "

cSql += " UNION "

cSql += " SELECT 'BE9' TABELA, 'Procedimentos por Produtos - Cadastro RDA / Complemento / Especialidades / Planos / Procedimentos' " 
cSql += " DESCRI, BE9_FILIAL, COUNT(BE9_CODIGO) QTD FROM " + RetSQLName ("BE9") 
cSql += " WHERE BE9_CODIGO = '" + cCodRDA + "' AND D_E_L_E_T_ = '' GROUP BY BE9_FILIAL, BE9_CODIGO "

cSql += " UNION "

cSql += " SELECT 'BBI' TABELA, 'Planos Autorizados - Cadastro RDA / Complemento / Especialidades / Planos / Planos' DESCRI, BBI_FILIAL, " 
cSql += " COUNT(BBI_CODIGO) QTD FROM " + RetSQLName ("BBI") 
cSql += " WHERE BBI_CODIGO = '" + cCodRDA + "' AND D_E_L_E_T_ = '' GROUP BY BBI_FILIAL, BBI_CODIGO "

cSql += " UNION "

cSql += " SELECT 'BC0' TABELA, 'Procedimentos Rede Atendimento - Cadastro RDA / Complemento / Especialidades / Procedimento Autorizados' " 
cSql += " DESCRI, BC0_FILIAL, COUNT(BC0_CODIGO) QTD FROM " + RetSQLName ("BC0") 
cSql += " WHERE BC0_CODIGO = '" + cCodRDA + "' AND D_E_L_E_T_ = '' GROUP BY BC0_FILIAL, BC0_CODIGO "

cSql += " UNION "

cSql += " SELECT 'BCK' TABELA, 'Refer�ncia por Unidade - Cadastro RDA / Complemento / Local de Atendimento / Diferencia��o da Ref./U.S. "  
cSql += " Por Unidade' DESCRI, BCK_FILIAL, COUNT(BCK_CODIGO) QTD FROM " + RetSQLName ("BCK") 
cSql += " WHERE BCK_CODIGO = '" + cCodRDA + "' AND D_E_L_E_T_ = ''  
cSql += " GROUP BY BCK_FILIAL, BCK_CODIGO "

cSql += " UNION "

cSql += " SELECT 'BAX' TABELA, 'Especialidades do Local - Cadastro RDA / Complemento / Especialidades' DESCRI, BAX_FILIAL, "  
cSql += " COUNT(BAX_CODIGO) QTD FROM " + RetSQLName ("BAX") 
cSql += " WHERE BAX_CODIGO = '" + cCodRDA + "' AND D_E_L_E_T_ = '' GROUP BY BAX_FILIAL, BAX_CODIGO "

cSql += " UNION "

cSql += " SELECT 'BB8' TABELA, 'Locais de Rede Atendimento - Cadastro RDA / Complemento / Local de Atendimento' DESCRI, BB8_FILIAL, " 
cSql += " COUNT(BB8_CODIGO) QTD FROM " + RetSQLName ("BB8") 
cSql += " WHERE BB8_CODIGO = '" + cCodRDA + "' AND D_E_L_E_T_ = '' GROUP BY BB8_FILIAL, BB8_CODIGO "

cSql += " UNION "

/*cSql += " SELECT 'B24' TABELA, 'RDA x Local x Tabela de Pre�os - Cadastro RDA / Complemento / Local de Atendimento/ Tabela de Pre�o' " 
cSql += " DESCRI, B24_FILIAL, COUNT(B24_CODIGO) QTD FROM " + RetSQLName ("B24") 
cSql += " WHERE B24_CODIGO = '" + cCodRDA + "' AND D_E_L_E_T_ = '' GROUP BY B24_FILIAL, B24_CODIGO "

cSql += " UNION "*/

cSql += " SELECT 'B30' TABELA, 'RDA X Planos  - Cadastro RDA / Complemento / RDA x Planos' DESCRI, B30_FILIAL, COUNT(B30_CODIGO) QTD " 
cSql += " FROM " + RetSQLName ("B30") 
cSql += " WHERE B30_CODIGO = '" + cCodRDA + "' AND D_E_L_E_T_ = ''  GROUP BY B30_FILIAL, B30_CODIGO "

/*cSql += " UNION "

cSql += " SELECT 'B29' TABELA, 'Rda X Tabela de Pre�o - Cadastro RDA / Tabela Pre�o' DESCRI, B29_FILIAL, COUNT(B29_CODIGO) QTD  "
cSql += " FROM " + RetSQLName ("B29") 
cSql += " WHERE B29_CODIGO = '" + cCodRDA + "' AND D_E_L_E_T_ = '' GROUP BY B29_FILIAL, B29_CODIGO "


cSql += " SELECT 'B22' TABELA, B22_DESCRI DESCRI, B22_FILIAL, COUNT(B22_CODTAB) QTD "
cSQL += " FROM " + RetSQLName ("B22") 
cSql += " INNER JOIN " + RetSQLName ("B29") + " ON B22_CODTAB = B29_TABPRE "
cSQL += " WHERE B29_CODIGO = '" + cCodRDA + "' AND " + RetSQLName("B22") + ".D_E_L_E_T_ = '' GROUP BY B22_FILIAL, B22_CODTAB, B22_DESCRI "

cSql += " UNION "

cSql += " SELECT 'B23' TABELA, 'Itens Tabela Prestador' DESCRI, B23_FILIAL, COUNT (B23_CODTAB) QTD "
cSql += " FROM " + RetSQLName ("B23")  
cSql += " INNER JOIN " + RetSQLName ("B22") + " ON B23_CODTAB = B22_CODTAB "
cSql += " INNER JOIN " + RetSQLName ("B29") + " ON B22_CODTAB = B29_TABPRE "
cSql += " WHERE B29_CODIGO = '" + cCodRDA + "' AND " + RetSQLName("B23") + ".D_E_L_E_T_ = '' GROUP BY B23_FILIAL, B23_CODTAB "*/

cSql += " UNION "

cSql += " SELECT 'BMI' TABELA, 'Empresa x Prest x Proc x Pagto - Grupo/Empresa / Contratos / Sub-Contrato / Par�metro Pagamento "
cSql += " Prestador' DESCRI, BMI_FILIAL, COUNT(BMI_CODRDA) QTD FROM " + RetSQLName ("BMI") 
cSql += " WHERE BMI_CODRDA = '" + cCodRDA + "' AND D_E_L_E_T_ = '' "
cSql += " GROUP BY BMI_FILIAL, BMI_CODRDA "

cSql += " UNION "

cSql += " SELECT 'BMH' TABELA, 'Empresa x Prest x Pagto - Grupo/Empresa / Contratos / Sub-Contrato / Par�metro Pagamento Prestador' " 
cSql += " DESCRI, BMH_FILIAL, COUNT(BMH_CODRDA) QTD FROM " + RetSQLName ("BMH") 
cSql += " WHERE BMH_CODRDA = '" + cCodRDA + "' AND D_E_L_E_T_ = '' GROUP BY BMH_FILIAL, BMH_CODRDA "

cSql += " UNION "

cSql += " SELECT 'BLY' TABELA, 'Pacotes Cabe�alho - RDA x Pacote / Abas Diversas' DESCRI, BLY_FILIAL, COUNT(BLY_CODRDA) QTD "
cSql += " FROM " + RetSQLName ("BLY") 
cSql += " WHERE BLY_CODRDA = '" + cCodRDA + "' AND D_E_L_E_T_ = '' GROUP BY BLY_FILIAL, BLY_CODRDA "


//Chamo Fun��o de Query
cQuery	:= ChangeQuery(cSql)
TcQuery cQuery New Alias "TabTmp"

While ! TabTmp->(Eof())
	aadd(aValores,{TabTmp->TABELA, TabTmp->DESCRI, TabTmp->QTD, .F.})
	TabTmp->(DbSkip())
Enddo

TabTmp->(DBCLOSEAREA())
oNiveis:SetArray(aValores)
oNiveis:Refresh()

Return 


//---------------------------------------------------------------------------------
/*/{Protheus.doc} PLSQRY2RP
Query para retornar os dados dos niveis ap�s escolha da RDA - Painel de Sele��o de Nivel

@author  Renan Martins	
@version P12
@since   07/2016
/*/
//---------------------------------------------------------------------------------
Function PLSQRY3RP(cVetor)
Local cSql		:= ""
Local aTabs	:= {'BC6','BC5','BE9','BBI','BC0','BCK','BAX','BB8','B24','B30','B29','B22','B23','BMI','BMI','BMH','BLY'} 
Local nI		:= 0 

IF 'BC6' $ cVetor	
	cSql += " SELECT 'BC6' TABELA, BC6_CODTAB TABE, '-' CODPLANO, 'RDA x tab. Pre�o - Itens' CABITE, BC6_CODPAD CODPAD, BC6_CODPRO CODPRO, BR8_DESCRI DESCRI, "+RetSQLName("BC6")+".R_E_C_N_O_  RECNO"
	cSql += RetCmpVal('BC6')
	cSql += " FROM " + RetSQLName ("BC6")  
	cSql += " INNER JOIN " + RetSQLName ("BR8") + " ON BR8_CODPAD = BC6_CODPAD AND BR8_CODPSA = BC6_CODPRO "
	cSql += " WHERE BC6_CODRDA = '" + cCodRDA + "' AND "+ RetSQLName("BC6") + ".D_E_L_E_T_ = '' "
	
	cSQL += " AND ( '"+dtos(dDataBase)+"' >= BC6_VIGINI ) AND "
	cSQL += "     (( '"+dtos(dDataBase)+"' <= BC6_VIGFIM OR BC6_VIGFIM = '' ) OR ( BC6_VIGINI = '' AND BC6_VIGFIM = '' )) "
	
	cSql += " UNION ALL "
ENDIF


IF 'BC5' $ cVetor
	cSql += " SELECT 'BC5' TABELA, BC5_CODTAB TABE, '-' CODPLANO, 'RDA x Tab. Pre�o - Tabelas RDA' CABITE, '-' CODPAD, '-' CODPRO, '-' DESCRI, R_E_C_N_O_  RECNO "
	cSql += RetCmpVal('BC5')
	cSql += " FROM " + RetSQLName ("BC5") 
	cSql += " WHERE BC5_CODRDA = '" + cCodRDA + "' AND D_E_L_E_T_ = '' "
	
	cSQL += " AND ( '"+dtos(dDataBase)+"' >= BC5_DATINI ) AND "
	cSQL += "     (( '"+dtos(dDataBase)+"' <= BC5_DATFIM OR BC5_DATFIM = '' ) OR ( BC5_DATINI = '' AND BC5_DATFIM = '' )) "	
	
	cSql += " UNION ALL "
ENDIF


IF 'BE9' $ cVetor
	cSql += " SELECT 'BE9' TABELA, BE9_CODTAB TABE, '-' CODPLANO, 'Procedimentos por Produtos na Especialidades - Itens' CABITE, BE9_CODPAD CODPAD, BE9_CODPRO CODPRO, BR8_DESCRI DESCRI, "+RetSQLName ("BE9") +".R_E_C_N_O_  RECNO "
	cSql += RetCmpVal('BE9')
	cSql += " FROM " + RetSQLName ("BE9") 
	cSql += " INNER JOIN " + RetSQLName ("BR8") + " ON BR8_CODPAD = BE9_CODPAD AND BR8_CODPSA = BE9_CODPRO "
	cSql += " WHERE BE9_CODIGO = '" + cCodRDA + "' AND " + RetSQLName("BE9") + ".D_E_L_E_T_ = '' "
	
	cSQL += "  AND ( '"+dtos(dDataBase)+"' >= BE9_VIGDE ) AND "
	cSQL += "      (( '"+dtos(dDataBase)+"' <= BE9_VIGATE OR BE9_VIGATE = '' ) OR ( BE9_VIGDE = '' AND BE9_VIGATE = '' )) "
	
	cSql += " UNION ALL "
ENDIF


IF 'BBI' $ cVetor
	cSql += " SELECT 'BBI' TABELA, BBI_CODTAB TABE, BBI_CODPRO CODPLANO, 'Planos Autorizados' CABITE,  '-' CODPAD, '-' CODPRO, '-' DESCRI, R_E_C_N_O_  RECNO "
	cSql += RetCmpVal('BBI')
	cSql += " FROM " + RetSQLName ("BBI")
	cSql += " WHERE BBI_CODIGO = '" + cCodRDA + "' AND D_E_L_E_T_ = '' "
	
	cSQL += "  AND ( '"+dtos(dDataBase)+"' >= BBI_VIGDE ) AND "
	cSQL += "      (( '"+dtos(dDataBase)+"' <= BBI_VIGATE OR BBI_VIGATE = '' ) OR ( BBI_VIGDE = '' AND BBI_VIGATE = '' )) "
	
	cSql += " UNION ALL "
ENDIF


IF 'BC0' $ cVetor
	cSql += " SELECT 'BC0' TABELA, BC0_CODTAB TABE, '-'  CODPLANO, 'Procedimentos Rede Atendimento - Itens' CABITE, BC0_CODPAD CODPAD, BC0_CODOPC CODPRO, BR8_DESCRI DESCRI,"+RetSQLName("BC0") +".R_E_C_N_O_  RECNO"
	cSql += RetCmpVal('BC0')
	cSql += " FROM " + RetSQLName ("BC0") 
	cSql += " INNER JOIN " + RetSQLName ("BR8") + " ON BR8_CODPAD = BC0_CODPAD AND BR8_CODPSA = BC0_CODOPC  "
	cSql += " WHERE BC0_CODIGO = '" + cCodRDA + "' AND " + RetSQLName("BC0") + ".D_E_L_E_T_ = '' "
	
	cSQL += "  AND ( '"+dtos(dDataBase)+"' >= BC0_VIGDE ) AND "
	cSQL += "      (( '"+dtos(dDataBase)+"' <= BC0_VIGATE OR BC0_VIGATE = '' ) OR ( BC0_VIGDE = '' AND BC0_VIGATE = '' )) "
	
	cSql += " UNION ALL "
ENDIF


IF 'BCK' $ cVetor
	cSql += " SELECT 'BCK' TABELA, '-' TABE, '-' CODPLANO, 'Cabe�alho' CABITE, '-' CODPAD, '-' CODPRO, '-' DESCRI, "+RetSQLName("BCK") +".R_E_C_N_O_  RECNO "
	cSql += RetCmpVal('BCK')
	cSql += "FROM " + RetSQLName ("BCK") 
	cSql += " WHERE BCK_CODIGO = '" + cCodRDA + "' AND D_E_L_E_T_ = '' "
	
	cSQL += "  AND ( '"+dtos(dDataBase)+"' >= BCK_VIGINI ) AND "
	cSQL += "      (( '"+dtos(dDataBase)+"' <= BCK_VIGFIN OR BCK_VIGFIN = '' ) OR ( BCK_VIGINI = '' AND BCK_VIGFIN = '' )) "
	
	cSql += " UNION ALL "
ENDIF


IF 'BAX' $ cVetor
	cSql += " SELECT 'BAX' TABELA, '-' TABE, '-' CODPLANO, 'Cabe�alho' CABITE, '-' CODPAD, '-' CODPRO, '-' DESCRI, R_E_C_N_O_  RECNO "
	cSql += RetCmpVal('BAX')
	cSql += "FROM " + RetSQLName ("BAX") 
	cSql += " WHERE BAX_CODIGO = '" + cCodRDA + "' AND D_E_L_E_T_ = '' "
	
	cSQL += "  AND (( '"+dtos(dDataBase)+"' >= BAX_VIGDE ) or (BAX_VIGDE = '')) "
	
	cSql += " UNION ALL "
ENDIF


IF 'BB8' $ cVetor
	cSql += " SELECT 'BB8' TABELA, '-' TABE, '-' CODPLANO, 'Cabe�alho' CABITE, '-' CODPAD, '-' CODPRO, '-' DESCRI, R_E_C_N_O_  RECNO "
	cSql += RetCmpVal('BB8')
	cSql += "FROM " + RetSQLName ("BB8") 
	cSql += " WHERE BB8_CODIGO = '" + cCodRDA + "' AND D_E_L_E_T_ = '' "
	
	cSQL += "  AND (( '"+dtos(dDataBase)+"' >= BB8_VIGUS ) or (BB8_VIGUS  = '')) "
	
	cSql += " UNION ALL "
ENDIF


IF 'B30' $ cVetor
	cSql += " SELECT 'B30' TABELA, '-' TABE, '-' CODPLANO, 'Cabe�alho' CABITE, '-' CODPAD, '-' CODPRO, '-' DESCRI, R_E_C_N_O_  RECNO "
	cSql += RetCmpVal('B30')
	cSql += " FROM " + RetSQLName ("B30") 
	cSql += " WHERE B30_CODIGO = '" + cCodRDA + "' AND D_E_L_E_T_ = ''  "
	
	cSQL += "  AND ( '"+dtos(dDataBase)+"' >= B30_VIGDE ) AND "
	cSQL += "      (( '"+dtos(dDataBase)+"' <= B30_VIGATE OR B30_VIGATE = '' ) OR ( B30_VIGDE = '' AND B30_VIGATE = '' )) "
	
	cSql += " UNION ALL "
ENDIF


IF 'BMI' $ cVetor
	cSql += " SELECT 'BMI' TABELA, '-' TABE, '-' CODPLANO, 'Item' CABITE, BMI_CODPAD CODPAD, BMI_CODPSA CODPRO, BR8_DESCRI DESCRI,"+ RetSQLName("BMI") + ".R_E_C_N_O_  RECNO"
	cSql += RetCmpVal('BMI')
	cSql += " FROM " + RetSQLName ("BMI")
	cSql += " INNER JOIN " + RetSQLName ("BR8") + "  ON BR8_CODPAD = BMI_CODPAD AND BR8_CODPSA = BMI_CODPSA "
	cSql += " WHERE BMI_CODRDA = '" + cCodRDA + "' AND " + RetSQLName("BMI") + ".D_E_L_E_T_ = '' "
	
	cSQL += "  AND (( '"+dtos(dDataBase)+"' >= BMI_DATDE ) or (BMI_DATDE = '')) "
	
	cSql += " UNION ALL "
ENDIF


IF 'BMH' $ cVetor
	cSql += " SELECT 'BMH' TABELA, '-' TABE, '-' CODPLANO, 'Cabe�alho' CABITE, '-' CODPAD, '-' CODPRO, '-' DESCRI, R_E_C_N_O_  RECNO "
	cSql += RetCmpVal('BMH')
	cSql += " FROM " + RetSQLName ("BMH")  
	cSql += " WHERE BMH_CODRDA = '" + cCodRDA + "' AND " + RetSQLName("BMH") + ".D_E_L_E_T_ = '' "
	
	cSQL += "  AND (( '"+dtos(dDataBase)+"' >= BMH_DATDE ) or (BMH_DATDE = '')) "
	
	cSql += " UNION ALL "
ENDIF


IF 'BLY' $ cVetor
	cSql += " SELECT 'BLY' TABELA, '-' TABE, '-' CODPLANO, 'Item' CABITE, BLY_CODPAD CODPAD, BLY_CODOPC CODPRO, BR8_DESCRI DESCRI, "+RetSQLName("BLY") +".R_E_C_N_O_  RECNO"
	cSql += RetCmpVal('BLY')
	cSql += " FROM " + RetSQLName ("BLY") 
	cSql += " INNER JOIN " + RetSQLName ("BR8") + " ON BR8_CODPAD = BLY_CODPAD AND BR8_CODPSA = BLY_CODOPC "
	cSql += " WHERE BLY_CODRDA = '" + cCodRDA + "' AND " + RetSQLName("BLY") + ".D_E_L_E_T_ = '' "
	
	cSQL += "  AND ( '"+dtos(dDataBase)+"' >= BLY_VIGDE ) AND "
	cSQL += "      (( '"+dtos(dDataBase)+"' <= BLY_VIGATE OR BLY_VIGATE = '' ) OR ( BLY_VIGDE = '' AND BLY_VIGATE = '' )) "
	
ENDIF
		        

//Tirar UNION caso exista na �ltima posi��o
IIF (RIGHT(cSQl, 6) $ "N ALL ", cSql := LEFT(cSql, LEN(cSQL)-10), "")
 

//Chamo Fun��o de Query
cQuery	:= ChangeQuery(cSql)
TcQuery cQuery New Alias "TabTmp"

While ! TabTmp->(Eof())
	aadd(aProced,{TabTmp->TABELA, TabTmp->TABE, TabTmp->CODPLANO, TabTmp->CABITE, TabTmp->CODPAD, TabTmp->CODPRO, TabTmp->DESCRI, .F.,TabTmp->RECNO, iif(cValtoChar(TabTmp->USPP) <> "-1",cValtoChar(TabTmp->USPP), "-") , RetCalc(TabTmp->USPP,cComboUS, cUS), iif(cValtoChar(TabTmp->BANDA) <> "-1", cValtoChar(TabTmp->BANDA),"-") , RetCalc(TabTmp->BANDA,cComboBanda, cBanda), iif(cValtoChar(TabTmp->UCO)<> "-1",cValtoChar(TabTmp->BANDA), "-") , RetCalc(TabTmp->UCO,cComboUCO, cUCO), iif(cValtoChar(TabTmp->VALRS)<> "-1",cValtoChar(TabTmp->VALRS), "-") , RetCalc(TabTmp->VALRS,"Aplicar %", cPerRea)})
	TabTmp->(DbSkip())
Enddo

TabTmp->(DBCLOSEAREA())
oProc:SetArray(aProced)
oProc:Refresh()
cVetTmp := ""

Return 

//---------------------------------------------------------------------------------
/*/{Protheus.doc} PLSQRY4RP
Query para retornar as vig�ncias que se iniciam posteriormente a database

@author  Roberto Vanderlei
@version P12
@since   07/2016
/*/
//---------------------------------------------------------------------------------

Function PLSQRY4RP(cVetor)
Local cSql		:= ""
Local aTabs		:= {'BC6','BC5','BE9','BBI','BC0','BCK','BAX','BB8','B30','BMI','BMI','BMH','BLY'}  
Local nI		:= 0 
Local aDtVenc 	:= {}

IF 'BC6' $ cVetor	
	cSql += " SELECT 'BC6' TABELA, BC6_CODTAB TABE, '-' CODPLANO, 'RDA x tab. Pre�o - Itens' CABITE, BC6_CODPAD CODPAD, BC6_CODPRO CODPRO, BR8_DESCRI DESCRI, BC6_VIGINI VIGINI, "+RetSQLName("BC6")+".R_E_C_N_O_  RECNO"
	cSql += " FROM " + RetSQLName ("BC6")  
	cSql += " INNER JOIN " + RetSQLName ("BR8") + " ON BR8_CODPAD = BC6_CODPAD AND BR8_CODPSA = BC6_CODPRO "
	cSql += " WHERE BC6_CODRDA = '" + cCodRDA + "' AND "+ RetSQLName("BC6") + ".D_E_L_E_T_ = '' "
	
	cSQL += " AND ( '"+dtos(dDataBase)+"' < BC6_VIGINI and BC6_VIGINI != '')"
		
	cSql += " UNION ALL "
ENDIF


IF 'BC5' $ cVetor
	cSql += " SELECT 'BC5' TABELA, BC5_CODTAB TABE, '-' CODPLANO, 'RDA x Tab. Pre�o - Tabelas RDA' CABITE, '-' CODPAD, '-' CODPRO, '-' DESCRI, BC5_DATINI VIGINI,  R_E_C_N_O_  RECNO "	
	cSql += " FROM " + RetSQLName ("BC5") 
	cSql += " WHERE BC5_CODRDA = '" + cCodRDA + "' AND D_E_L_E_T_ = '' "
	
	cSQL += " AND ( '"+dtos(dDataBase)+"' < BC5_DATINI and BC5_DATINI != '' ) "
	
	cSql += " UNION ALL "
ENDIF


IF 'BE9' $ cVetor
	cSql += " SELECT 'BE9' TABELA, BE9_CODTAB TABE, '-' CODPLANO, 'Procedimentos por Produtos na Especialidades - Itens' CABITE, BE9_CODPAD CODPAD, BE9_CODPRO CODPRO, BR8_DESCRI DESCRI, BE9_VIGDE VIGINI, "+RetSQLName ("BE9") +".R_E_C_N_O_  RECNO "
	cSql += " FROM " + RetSQLName ("BE9") 
	cSql += " INNER JOIN " + RetSQLName ("BR8") + " ON BR8_CODPAD = BE9_CODPAD AND BR8_CODPSA = BE9_CODPRO "
	cSql += " WHERE BE9_CODIGO = '" + cCodRDA + "' AND " + RetSQLName("BE9") + ".D_E_L_E_T_ = '' "
	
	cSQL += "  AND ( '"+dtos(dDataBase)+"' < BE9_VIGDE  and BE9_VIGDE != '' )  "
	
	cSql += " UNION ALL "
ENDIF


IF 'BBI' $ cVetor
	cSql += " SELECT 'BBI' TABELA, BBI_CODTAB TABE, BBI_CODPRO CODPLANO, 'Planos Autorizados' CABITE,  '-' CODPAD, '-' CODPRO, '-' DESCRI, BBI_VIGDE VIGINI, R_E_C_N_O_  RECNO "
	cSql += " FROM " + RetSQLName ("BBI")
	cSql += " WHERE BBI_CODIGO = '" + cCodRDA + "' AND D_E_L_E_T_ = '' "
	
	cSQL += "  AND ( '"+dtos(dDataBase)+"' < BBI_VIGDE and BBI_VIGDE != '') "
	
	cSql += " UNION ALL "
ENDIF


IF 'BC0' $ cVetor
	cSql += " SELECT 'BC0' TABELA, BC0_CODTAB TABE, '-'  CODPLANO, 'Procedimentos Rede Atendimento - Itens' CABITE, BC0_CODPAD CODPAD, BC0_CODOPC CODPRO, BR8_DESCRI DESCRI,BC0_VIGDE VIGINI, "+RetSQLName("BC0") +".R_E_C_N_O_  RECNO"
	cSql += " FROM " + RetSQLName ("BC0") 
	cSql += " INNER JOIN " + RetSQLName ("BR8") + " ON BR8_CODPAD = BC0_CODPAD AND BR8_CODPSA = BC0_CODOPC  "
	cSql += " WHERE BC0_CODIGO = '" + cCodRDA + "' AND " + RetSQLName("BC0") + ".D_E_L_E_T_ = '' "
	
	cSQL += "  AND ( '"+dtos(dDataBase)+"' < BC0_VIGDE  and BC0_VIGDE != '') "
	
	cSql += " UNION ALL "
ENDIF


IF 'BCK' $ cVetor
	cSql += " SELECT 'BCK' TABELA, '-' TABE, '-' CODPLANO, 'Cabe�alho' CABITE, '-' CODPAD, '-' CODPRO, '-' DESCRI, BCK_VIGINI VIGINI, "+RetSQLName("BCK") +".R_E_C_N_O_  RECNO "
	cSql += "FROM " + RetSQLName ("BCK") 
	cSql += " WHERE BCK_CODIGO = '" + cCodRDA + "' AND D_E_L_E_T_ = '' "
	
	cSQL += "  AND ( '"+dtos(dDataBase)+"' < BCK_VIGINI AND BCK_VIGINI != '') "
	
	cSql += " UNION ALL "
ENDIF


IF 'BAX' $ cVetor
	cSql += " SELECT 'BAX' TABELA, '-' TABE, '-' CODPLANO, 'Cabe�alho' CABITE, '-' CODPAD, '-' CODPRO, '-' DESCRI, BAX_VIGDE VIGINI,  R_E_C_N_O_  RECNO "
	cSql += "FROM " + RetSQLName ("BAX") 
	cSql += " WHERE BAX_CODIGO = '" + cCodRDA + "' AND D_E_L_E_T_ = '' "
	
	cSQL += "  AND (( '"+dtos(dDataBase)+"' < BAX_VIGDE ) and (BAX_VIGDE != '')) "
	
	cSql += " UNION ALL "
ENDIF


IF 'BB8' $ cVetor
	cSql += " SELECT 'BB8' TABELA, '-' TABE, '-' CODPLANO, 'Cabe�alho' CABITE, '-' CODPAD, '-' CODPRO, '-' DESCRI, BB8_VIGUS VIGINI, R_E_C_N_O_  RECNO "
	cSql += "FROM " + RetSQLName ("BB8") 
	cSql += " WHERE BB8_CODIGO = '" + cCodRDA + "' AND D_E_L_E_T_ = '' "
	
	cSQL += "  AND (( '"+dtos(dDataBase)+"' < BB8_VIGUS ) and (BB8_VIGUS  != '')) "
	
	cSql += " UNION ALL "
ENDIF

IF 'B30' $ cVetor
	cSql += " SELECT 'B30' TABELA, '-' TABE, '-' CODPLANO, 'Cabe�alho' CABITE, '-' CODPAD, '-' CODPRO, '-' DESCRI, B30_VIGDE VIGINI, R_E_C_N_O_  RECNO "
	cSql += " FROM " + RetSQLName ("B30") 
	cSql += " WHERE B30_CODIGO = '" + cCodRDA + "' AND D_E_L_E_T_ = ''  "
	
	cSQL += "  AND ( '"+dtos(dDataBase)+"' < B30_VIGDE and B30_VIGDE != '') "
	
	cSql += " UNION ALL "
ENDIF

IF 'BMI' $ cVetor
	cSql += " SELECT 'BMI' TABELA, '-' TABE, '-' CODPLANO, 'Item' CABITE, BMI_CODPAD CODPAD, BMI_CODPSA CODPRO, BR8_DESCRI DESCRI, BMI_DATDE VIGINI, "+ RetSQLName("BMI") + ".R_E_C_N_O_  RECNO"
	cSql += " FROM " + RetSQLName ("BMI")
	cSql += " INNER JOIN " + RetSQLName ("BR8") + "  ON BR8_CODPAD = BMI_CODPAD AND BR8_CODPSA = BMI_CODPSA "
	cSql += " WHERE BMI_CODRDA = '" + cCodRDA + "' AND " + RetSQLName("BMI") + ".D_E_L_E_T_ = '' "
	
	cSQL += "  AND (( '"+dtos(dDataBase)+"' < BMI_DATDE ) and (BMI_DATDE != '')) "
	
	cSql += " UNION ALL "
ENDIF


IF 'BMH' $ cVetor
	cSql += " SELECT 'BMH' TABELA, '-' TABE, '-' CODPLANO, 'Cabe�alho' CABITE, '-' CODPAD, '-' CODPRO, '-' DESCRI, BMH_DATDE VIGINI,  R_E_C_N_O_  RECNO "
	cSql += " FROM " + RetSQLName ("BMH")  
	cSql += " WHERE BMH_CODRDA = '" + cCodRDA + "' AND " + RetSQLName("BMH") + ".D_E_L_E_T_ = '' "
	
	cSQL += "  AND (( '"+dtos(dDataBase)+"' < BMH_DATDE ) and (BMH_DATDE != '')) "
	
	cSql += " UNION ALL "
ENDIF


IF 'BLY' $ cVetor
	cSql += " SELECT 'BLY' TABELA, '-' TABE, '-' CODPLANO, 'Item' CABITE, BLY_CODPAD CODPAD, BLY_CODOPC CODPRO, BR8_DESCRI DESCRI, BLY_VIGDE VIGINI,  "+RetSQLName("BLY") +".R_E_C_N_O_  RECNO"
	cSql += " FROM " + RetSQLName ("BLY") 
	cSql += " INNER JOIN " + RetSQLName ("BR8") + " ON BR8_CODPAD = BLY_CODPAD AND BR8_CODPSA = BLY_CODOPC "
	cSql += " WHERE BLY_CODRDA = '" + cCodRDA + "' AND " + RetSQLName("BLY") + ".D_E_L_E_T_ = '' "
	
	cSQL += "  AND ( '"+dtos(dDataBase)+"' < BLY_VIGDE and BLY_VIGDE != '') "
	
ENDIF
		        

//Tirar UNION caso exista na �ltima posi��o
IIF (RIGHT(cSQl, 6) $ "N ALL ", cSql := LEFT(cSql, LEN(cSQL)-10), "")
 

//Chamo Fun��o de Query
cQuery	:= ChangeQuery(cSql)
TcQuery cQuery New Alias "TabTmp"

While ! TabTmp->(Eof())
	aadd(aProcedFuturo,{TabTmp->TABELA, TabTmp->TABE, TabTmp->CODPLANO, TabTmp->CABITE, TabTmp->CODPAD, TabTmp->CODPRO, TabTmp->DESCRI, TabTmp->VIGINI})
	aadd(aDtVenc, {TabTmp->TABELA,TabTmp->VIGINI})
	TabTmp->(DbSkip())
Enddo

TabTmp->(DBCLOSEAREA())
oProcFuturo:SetArray(aProcedFuturo)
oProcFuturo:Refresh()
cVetTmp := ""

Return aDtVenc

//---------------------------------------------------------------------------------
/*/{Protheus.doc} PLSQRY5CR
Cancelamento - Query para retornar os RECNOS dos registros que fazem parte de um reajuste

@author  Roberto Vanderlei
@version P12
@since   07/2016
/*/
//---------------------------------------------------------------------------------
Function PLSQRY5CR(cCodigo, dDtReajuste)
Local cSql		:= ""
Local aTabs		:= {'BC6','BC5','BE9','BBI','BC0','BCK','BAX','BB8','B30','BMI','BMH','BLY'} 
Local nI 
Local aRecnos 	:= {}
Local cAliasNivel := ""

for nI = 1 to len(aTabs)  	
	cSql += " SELECT '"+aTabs[nI]+"' TABELA, R_E_C_N_O_  RECNO, "  + aTabs[nI] + "_RECREA RECREA" 
 	cSql += " FROM " + RetSQLName (aTabs[nI])  
	cSql += " WHERE" +aTabs[nI]+ "_CODREA = '" + cCodigo + "' AND D_E_L_E_T_ = '' "
		
	cSql += " UNION ALL "
next

//Tirar UNION caso exista na �ltima posi��o
IIF (RIGHT(cSQl, 6) $ "N ALL ", cSql := LEFT(cSql, LEN(cSQL)-10), "")
 

//Chamo Fun��o de Query
cQuery	:= ChangeQuery(cSql)
TcQuery cQuery New Alias "TabTmp"

While ! TabTmp->(Eof())
	aadd(aRecnos, {TabTmp->TABELA, TabTmp->RECNO, TabTmp->RECREA})
	TabTmp->(DbSkip())
Enddo

TabTmp->(DBCLOSEAREA())

Return aRecnos

	
//---------------------------------------------------------------------------------
/*/{Protheus.doc} RetCalc
cTipo = Tipo de calculo  'N�o', 'Aplicar %', 'Valor Novo'
	
@author  Roberto Vanderlei
@version P12
@since   07/2016
/*/
//---------------------------------------------------------------------------------
Function RetCalc(cValor, cTipo, cPercVal)

	Local cRetorno := 0
	
	if alltrim(cValtoChar(cValor)) = "-1"
		cRetorno := "-"
	else
		if cTipo = "N�o"
			cRetorno := cValor
		else
			if cTipo = "Aplicar %"
				cRetorno := round(((val(cPercVal)/100 * cValor) + cValor), 4)
			else
				cRetorno := val(cPercVal)
			endif
		endif
	endif
return  cValtoChar(cRetorno)


//---------------------------------------------------------------------------------
/*/{Protheus.doc} RetCmpVal

@version P12
@since   07/2016
/*/
//---------------------------------------------------------------------------------
Function RetCmpVal(cAliasTab)

	Local cCamposRetorno := ", "
	
	Do Case
		Case cAliasTab $ 'BC5,B22'
			cCamposRetorno +=  cAliasTab + "_VPPP USPP, "
			cCamposRetorno += "-1 VALRS, " //Valor em Reais
			//cCamposRetorno := cAliasTab + "_VPCO USCO, "
		Case cAliasTab = 'BC6'
			cCamposRetorno += cAliasTab + "_USPPP USPP, "
			cCamposRetorno += cAliasTab + "_VRPPP VALRS, " //Valor em Reais
			//cCamposRetorno := cAliasTab + "_USPCO USCO, "			
		Case cAliasTab $ 'BE9,BBI,BC0,BAX,BB8,B30, BLY'
			cCamposRetorno +=  cAliasTab + "_VALCH USPP, " //USPP ou USCO ?
			if cAliasTab $ 'BE9,BC0'
				cCamposRetorno += cAliasTab + "_VALREA VALRS, " //Valor em Reais
			elseif cAliasTab = 'BLY'
				cCamposRetorno += cAliasTab + "_VALFIX VALRS, " //Valor em Reais
			else
				cCamposRetorno += "-1 VALRS, " //Valor em Reais
			endif
		Case cAliasTab = 'BCK'
			cCamposRetorno +=  cAliasTab + "_US USPP, "
			cCamposRetorno += "-1 VALRS, " //Valor em Reais
		Case cAliasTab = 'B23'
			cCamposRetorno +=  cAliasTab + "_USPPP USPP, "
			//cCamposRetorno :=  cAliasTab + "_UPSCO USCO, "
		Case cAliasTab $ 'BMI,BMH'
			if cAliasTab = 'BMI'
				cCamposRetorno += cAliasTab + "_VLRPAG VALRS, " //Valor em Reais
			else
				cCamposRetorno += "-1 VALRS, " //Valor em Reais
			endif
			
			cCamposRetorno +=  cAliasTab + "_VALUS USPP, "
	end case
	
	//Para todas as tabelas que possuem o campo, o nome dele � _UCO.
	if cAliasTab $ 'BC5,BC6,BBI,BC0,BAX,B22,B23,B30,BMI,BMH'
		 cCamposRetorno += cAliasTab+"_UCO UCO, "
	else
		 cCamposRetorno += "-1 UCO, "
	endif
	
	Do Case
		Case cAliasTab $ 'BC5,BC6,B22,B23'
			cCamposRetorno += cAliasTab + "_BANDAP BANDA"
		Case cAliasTab $ 'BE9,BBI,BC0,BAX,B30,BMI,BMH'
			cCamposRetorno += cAliasTab + "_BANDA BANDA"
		OTHERWISE 
			cCamposRetorno += "-1 BANDA"
	end case

Return cCamposRetorno


//---------------------------------------------------------------------------------
/*/{Protheus.doc} ReajNiv
Realiza o Reajuste

@author  Roberto Vanderlei
@version P12
@since   07/2016
/*/
//---------------------------------------------------------------------------------
Function ReajNiv(nCont, cVetTmp)

	Local cAliasNivel := ""
	Local nRecno 	  := 0
	Local cCampoSX3	  := ""
	Local nY
	Local nFor
	Local aCampos 	  := {}
	Local cCodigo
	Local cReajuste   := ""
	Local aDtVenc := {}
	Local cDtFimVig
	Local nI
	Local lAchou := .F.
	
	aDtVenc := PLSQRY4RP(cVetTmp)
	
	Begin Transaction
		cCodigo := GetSx8Num("B4S","B4S_CODIGO")
		
		B4S->(Reclock("B4S",.T.)) //Gravando o Cabe�alho do Reajuste.
		B4S->B4S_FILIAL := xFilial("B4S")
		B4S->B4S_CODOPE := cCodOpe
		B4S->B4S_CODIGO := cCodigo
		B4S->B4S_CODRDA := cCodRda
		//B4S->B4S_NOMRDA := cNomeRDA
		B4S->B4S_ANOREA := cAnoRea
		B4S->B4S_MESREA := cComboMes
		B4S->B4S_VIGINI := ctod(cVigIni)
		B4S->B4S_VIGFIM := ctod(cVigFim)
		B4S->B4S_PERCEN := val(cPerRea)
		
		if cComboUS = "Valor Novo"
			cReajuste := "V"
		else
			if cComboUS = "Aplicar %"
				cReajuste := "%"
			else
				cReajuste := "N"
			endif
		endif
		
		B4S->B4S_REAUS  := cReajuste
		B4S->B4S_US 	:= val(cUS)
		
		if cComboBanda = "Valor Novo"
			cReajuste := "V"
		else
			if cComboBanda = "Aplicar %"
				cReajuste := "%"
			else
				cReajuste := "N"
			endif
		endif		
		
		B4S->B4S_REABAN := cReajuste
		B4S->B4S_BANDA  := val(cBanda)
		
		if cComboUCO = "Valor Novo"
			cReajuste := "V"
		else
			if cComboUCO = "Aplicar %"
				cReajuste := "%"
			else
				cReajuste := "N"
			endif
		endif
		
		B4S->B4S_REAUCO := cReajuste
		B4S->B4S_UCO    := val(cUco)
		
		B4S->B4S_CODUSR := RetCodUsr()
		//B4S->B4S_NOMUSR := USRFULLNAME(RETCODUSR())
		
		B4S->B4S_DTREAJ := dDataBase
		
		B4S->(MsUnlock())
		B4S->(ConfirmSx8())
	
		for nY := 1 to len(aProced)
			if aProced[nY,8]
				aCampos := {}
				IncProc("Processando ... "  + str(nY) + " de " + str(nCont) + ".") 
				cAliasNivel := aProced[nY][1]
				nRecno 	    := aProced[nY][9]
				
				lAchou := .F.
				for nI := 1 to len(aDtVenc)
					if aDtVenc[nI][1] = cAliasNivel .and. lAchou = .F.
						lAchou := .T.	
						cDtFimVig := stod(Transform(aDtVenc[nI][2],PesqPict("BC6","BC6_VIGFIM")))  
					endif
				next
				
				if lAchou = .F.
					cDtFimVig := ctod(cVigFim)
				endif
				
				//Posiciona no Registro que ser� reajustado  
				&(cAliasNivel)->(DbGoTo(nRecno))
				//Posiciona no SX3 para clonar o registro que ser� reajustado 
				SX3->(DbsetOrder(1))
				
				If SX3->( MsSeek(cAliasNivel) ) //Seek na tabela que est� sendo reajustada
				
					SX3->( DbSkip() )
					
					//Carregando o Objeto na mem�ria
					While ! SX3->( Eof() ) .And. SX3->X3_ARQUIVO == cAliasNivel
					
						cCampoSX3 := SX3->X3_CAMPO
						
						If (SX3->X3_CONTEXT <> "V") /*.and. X3Uso(SX3->X3_USADO)*/ .and. ValType(cAliasNivel+"->"+SX3->X3_CAMPO) <> "U"//substr(cCampoSX3,4) <> '_CODREA'
							&("M->"+cCampoSX3) := &(cAliasNivel+"->"+cCampoSX3)
							aadd(aCampos, cCampoSX3)
						endif
						
						SX3->(DbSkip())
							
					Enddo
					
					//Atualizando o Registro anterior para que a vig�ncia do mesmo seja fechada um dia antes da vig�ncia inicial.
					if cAliasNivel <> 'BAX' .and. cAliasNivel <> 'BB8' .and. cAliasNivel <> 'BMI' .and. cAliasNivel <> 'BMH'
						&(cAliasNivel)->(Reclock(cAliasNivel,.F.))
						
						if cAliasNivel = 'BC6'
							&(cAliasNivel+"->"+cAliasNivel+"_VIGFIM") := DaySub(ctod(cVigIni),1) //DaySub(ctod(cVigIni),1)
						elseif cAliasNivel = 'BC5'
							&(cAliasNivel+"->"+cAliasNivel+"_DATFIM") := DaySub(ctod(cVigIni),1)
						elseif cAliasNivel $ 'BE9,BBI,BC0,B30,BLY'
							&(cAliasNivel+"->"+cAliasNivel+"_VIGATE") := DaySub(ctod(cVigIni),1)
						elseif cAliasNivel $ 'BCK,B24,B29' 
							&(cAliasNivel+"->"+cAliasNivel+"_VIGFIN") := DaySub(ctod(cVigIni),1)
						endif
						
						&(cAliasNivel)->(MsUnLock())
					endif 
					
					//Preenchendo o Objeto para grava��o
					&(cAliasNivel)->(Reclock(cAliasNivel,.T.))
					
					for nFor := 1 to len(aCampos)
					
						cCampoSX3 := aCampos[nFor]
						&(cAliasNivel+"->"+cCampoSX3) := &("M->"+cCampoSX3) 	
						
					next
					
					//Inicializando campo com c�digo do Reajuste
					&(cAliasNivel+"->"+cAliasNivel+"_CODREA") := cCodigo
					&(cAliasNivel+"->"+cAliasNivel+"_RECREA") := nRecno 
					
					if cAliasNivel $ 'BE9,BBI,BC0,B30,BLY'
						&(cAliasNivel+"->"+cAliasNivel+"_VIGDE") := ctod(cVigIni)
						&(cAliasNivel+"->"+cAliasNivel+"_VIGATE") := ctod(cVigFim)
					elseif cAliasNivel = 'BAX'
						&(cAliasNivel+"->"+cAliasNivel+"_VIGDE") := ctod(cVigIni)
					elseif cAliasNivel = 'BB8'
						&(cAliasNivel+"->"+cAliasNivel+"_VIGUS") := ctod(cVigIni)
					endif
										
					Do Case
						Case cAliasNivel $ 'BC5'
							&(cAliasNivel+"->"+cAliasNivel+"_VPPP")   := val(aProced[nY][11])
							
							&(cAliasNivel+"->"+cAliasNivel+"_DATINI") := ctod(cVigIni)
							&(cAliasNivel+"->"+cAliasNivel+"_DATFIM") := ctod(cVigFim)
						
						Case cAliasNivel = 'BC6'
							&(cAliasNivel+"->"+cAliasNivel+"_USPPP")  := val(aProced[nY][11])
							&(cAliasNivel+"->"+cAliasNivel+"_VRPPP")  := val(aProced[nY][17])
							&(cAliasNivel+"->"+cAliasNivel+"_VIGINI") := ctod(cVigIni)
							&(cAliasNivel+"->"+cAliasNivel+"_VIGFIM") := ctod(cVigFim)
							
						Case cAliasNivel $ 'BE9,BBI,BC0,BAX,BB8,B30,BLY'
							&(cAliasNivel+"->"+cAliasNivel+"_VALCH")  := val(aProced[nY][11])
							if cAliasNivel $ 'BE9,BC0'
								&(cAliasNivel+"->"+cAliasNivel+"_VALREA")  := val(aProced[nY][17])
							elseif cAliasNivel = 'BLY'
								&(cAliasNivel+"->"+cAliasNivel+"_VALFIX")  := val(aProced[nY][17])
							endif
						Case cAliasNivel = 'BCK'
							&(cAliasNivel+"->"+cAliasNivel+"_US")     := val(aProced[nY][11])
							
							&(cAliasNivel+"->"+cAliasNivel+"_VIGINI") := ctod(cVigIni)
							&(cAliasNivel+"->"+cAliasNivel+"_VIGFIN") := ctod(cVigFim)
						/*Case cAliasNivel = 'B23'
							&(cAliasNivel+"->"+cAliasNivel+"_USPPP")  := val(aProced[nY][11])*/
						Case cAliasNivel $ 'BMI,BMH'
						
							if cAliasNivel = 'BMI'
								&(cAliasNivel+"->"+cAliasNivel+"_VLRPAG")  := val(aProced[nY][17])
							endif
				
							&(cAliasNivel+"->"+cAliasNivel+"_VALUS")  := val(aProced[nY][11])
							
							&(cAliasNivel+"->"+cAliasNivel+"_DATDE") := ctod(cVigIni)
					end case
					
					
					//Para todas as tabelas que possuem o campo, o nome dele � _UCO.
					if cAliasNivel $ 'BC5,BC6,BBI,BC0,BAX,B22,B23,B30,BMI,BMH'
						&(cAliasNivel+"->"+cAliasNivel+"_UCO")        := val(aProced[nY][15])
					endif
					
					Do Case
						Case cAliasNivel $ 'BC5,BC6,B22,B23'
							&(cAliasNivel+"->"+cAliasNivel+"_BANDAP") := val(aProced[nY][13])
						Case cAliasNivel $ 'BE9,BBI,BC0,BAX,B30,BMI,BMH'
							&(cAliasNivel+"->"+cAliasNivel+"_BANDA")  := val(aProced[nY][13])
					end case
					
					&(cAliasNivel)->(MsUnLock()) //Gravando o Reajuste.
				Endif
			endif
		next
	End Transaction
	
return


//---------------------------------------------------------------------------------
/*/{Protheus.doc} CancReaj
Cancela o reajuste

@author  Roberto Vanderlei
@version P12
@since   07/2016
/*/
//---------------------------------------------------------------------------------
Function CancReaj(cCodigo, dDtReajuste)
	Local aRecnos
	Local nI
	Local cAliasNivel := ""
	Local nRecno 	:= 0
	Local nRecnoAlt := 0
	
	aRecnos := PLSQRY5CR(cCodigo, dDtReajuste)
	
	Begin Transaction
	
		B4S->(Reclock("B4S",.F.))
			B4S->B4S_DTCANC := ddatabase
			B4S->B4S_USRCAN := RetCodUsr()
		B4S->(MsUnLock())
		
		for nI := 1 to len(aRecnos) 
			IncProc("Processando ... "  + str(nI) + " de " + str(len(aRecnos)) + ".")
			cAliasNivel := aRecnos[nI][1]
			nRecno		:= aRecnos[nI][2]
			nRecnoAlt   := aRecnos[nI][3]
			
			//Posiciona no Registro que ser� exclu�do  
			&(cAliasNivel)->(DbGoTo(nRecno))		
			
			&(cAliasNivel)->(Reclock(cAliasNivel,.F.))
				&(cAliasNivel)->(DbDelete())					
			&(cAliasNivel)->(MsUnLock())
			
			//Alterar Registro para ficar vigente
			&(cAliasNivel)->(DbGoTo(nRecnoAlt))		
			
			&(cAliasNivel)->(Reclock(cAliasNivel,.F.))
			
				if cAliasNivel = 'BC6'
					&(cAliasNivel+"->"+cAliasNivel+"_VIGFIM") := ctod("")
				elseif cAliasNivel = 'BC5'
					&(cAliasNivel+"->"+cAliasNivel+"_DATFIM") := ctod("")
				elseif cAliasNivel $ 'BE9,BBI,BC0,B30,BLY'
					&(cAliasNivel+"->"+cAliasNivel+"_VIGATE") := ctod("")
				elseif cAliasNivel $ 'BCK' 
					&(cAliasNivel+"->"+cAliasNivel+"_VIGFIN") := ctod("")
				endif
									
			&(cAliasNivel)->(MsUnLock())
			
		next
		
	End Transaction
	
return 