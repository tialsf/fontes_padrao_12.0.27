#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'PLSA109.CH'

//-------------------------------------------------------------------
/*/{Protheus.doc} PLSA109
Rotina de cadastro RDA x Produto

@author Diogo Ximenes
@since 01/01/11
@version P11
/*/

Function PLSA109()
Local oBrowse

oBrowse:= FWmBrowse():New()
oBrowse:SetAlias( 'BAU' )
oBrowse:SetDescription( STR0001 )//'RDA x Produto'
oBrowse:DisableDetails()
oBrowse:Activate()

Return NIL

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Monta o menu

@author Diogo Ximenes
@since 01/01/11
@version P11
/*/
Static Function MenuDef()
Local aRotina := {}

ADD OPTION aRotina Title STR0002  Action 'PesqBrw'         OPERATION 1 ACCESS 0    //'Pesquisar'
ADD OPTION aRotina Title STR0003  Action 'VIEWDEF.PLSA109' OPERATION 2 ACCESS 0   //'Visualizar'
//ADD OPTION aRotina Title STR0004  Action 'VIEWDEF.PLSA109' OPERATION 3 ACCESS 0   //'Incluir'
ADD OPTION aRotina Title STR0005  Action 'VIEWDEF.PLSA109' OPERATION 4 ACCESS 0   //'Alterar'
//ADD OPTION aRotina Title STR0006  Action 'VIEWDEF.PLSA109' OPERATION 5 ACCESS 0   //'Excluir'
Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Define a model

@author Diogo Ximenes
@since 01/01/11
@version P11
/*/
Static Function ModelDef()
// Cria a estrutura a ser usada no Modelo de Dados
Local oStruBAU := FWFormStruct( 1, 'BAU', /*bAvalCampo*/, /*lViewUsado*/ )
Local oStruB61 := FWFormStruct( 1, 'B61', /*bAvalCampo*/, /*lViewUsado*/ )
Local oStruB60 := FWFormStruct( 1, 'B60', /*bAvalCampo*/, /*lViewUsado*/ )
Local oModel

// Cria o objeto do Modelo de Dados
oModel := MPFormModel():New( 'PLSA109MD',/*bPreValidacao*/,Nil,/*bCommit*/, /*bCancel*/ )

// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
oModel:AddFields( 'BAUMASTER', /*cOwner*/, oStruBAU )

// Adiciona ao modelo uma estrutura de formul�rio de edi��o por grid
oModel:AddGrid( 'B61DETAIL', 'BAUMASTER', oStruB61 )

// Adiciona ao modelo uma estrutura de formul�rio de edi��o por grid
oModel:AddGrid( 'B60DETAIL', 'B61DETAIL', oStruB60, ,{ |oStruB60| PlsVldDfe("B60",.T.,'B60DETAIL',Nil) })

// Faz relaciomaneto entre os compomentes do model
oModel:SetRelation( 'B61DETAIL', { 	{ 'B61_FILIAL', 'xFilial( "B61" )' },; 
									{ 'B61_CODRDA', 'BAU_CODIGO' } }, "B61_FILIAL+B61_CODRDA+B61_CODPLA+B61_VERSAO" )

// Faz relaciomaneto entre os compomentes do model
oModel:SetRelation( 'B60DETAIL', { 	{ 'B60_FILIAL', 'xFilial( "B60" )' },; 
									{ 'B60_CODRDA', 'BAU_CODIGO' },; 
									{ 'B60_CODPLA', 'B61_CODPLA' },;
									{ 'B60_VERSAO', 'B61_VERSAO' } }, "B60_FILIAL+B60_CODRDA+B60_CODPLA+B60_VERSAO" )

// Liga o controle de nao repeticao de linha
oModel:GetModel( 'B60DETAIL' ):SetUniqueLine( {'B60_SEQUEN'} )

// Indica que � opcional ter dados informados na Grid
oModel:GetModel( 'B60DETAIL' ):SetOptional(.T.)

// Liga o controle de nao repeticao de linha
oModel:GetModel( 'B61DETAIL' ):SetUniqueLine( {'B61_CODPLA','B61_VERSAO'} )

// Adiciona a descricao do Modelo de Dados
oModel:SetDescription( 'RDA x Produto' )

// Adiciona a descricao do Componente do Modelo de Dados
oModel:GetModel( 'BAUMASTER' ):SetDescription( 'RDA' ) 
oModel:GetModel( 'B61DETAIL' ):SetDescription( STR0007 ) //'Produto'
oModel:GetModel( 'B60DETAIL' ):SetDescription( STR0008  )//'Faixa Desconto X RDA x Produto'  

Return oModel

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Define a view

@author Diogo Ximenes
@since 01/01/11
@version P11
/*/
Static Function ViewDef()
// Cria um objeto de Modelo de Dados baseado no ModelDef do fonte informado
Local oStruBAU := FWFormStruct( 2, 'BAU' )
Local oStruB61 := FWFormStruct( 2, 'B61' )
Local oStruB60 := FWFormStruct( 2, 'B60' )

// Cria a estrutura a ser usada na View
Local oModel   := FWLoadModel( 'PLSA109' )
Local oView

// Cria o objeto de View
oView := FWFormView():New()

// Define qual o Modelo de dados ser� utilizado
oView:SetModel( oModel )

//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
//oView:AddField( 'VIEW_BAU', oStruBAU, 'BAUMASTER' )

//Adiciona no nosso View um controle do tipo FormGrid(antiga newgetdados)
oView:AddGrid(  'VIEW_B61', oStruB61, 'B61DETAIL' )

//Adiciona no nosso View um controle do tipo FormGrid(antiga newgetdados)
oView:AddGrid(  'VIEW_B60', oStruB60, 'B60DETAIL' )

// Cria Folder na view
oView:CreateFolder( 'PASTAS' )
// Cria pastas nas folders
//oView:AddSheet( 'PASTAS', 'ABA01', 'RDA' )
oView:AddSheet( 'PASTAS', 'ABA02', 'Produto' )

// Criar um "box" horizontal para receber algum elemento da view
//oView:CreateHorizontalBox( 'ABA01GERAL', 100,,, 'PASTAS', 'ABA01' )

// Criar um "box" horizontal para receber algum elemento da view
oView:CreateHorizontalBox( 'ABA02SUPERIOR', 50,,, 'PASTAS', 'ABA02' )
oView:CreateHorizontalBox( 'ABA02INFERIOR', 50,,, 'PASTAS', 'ABA02' )

// Relaciona o ID da View com o "box" para exibicao
//oView:SetOwnerView( 'VIEW_BAU', 'ABA01GERAL')
oView:SetOwnerView( 'VIEW_B61', 'ABA02SUPERIOR')
oView:SetOwnerView( 'VIEW_B60', 'ABA02INFERIOR')

// Define campos que terao Auto Incremento
oView:AddIncrementField( 'VIEW_B60', 'B60_SEQUEN' )

// Liga a identificacao do componente
oView:EnableTitleView('VIEW_B60',STR0008)  //'Faixa Desconto X RDA x Produto'

// Liga a identificacao do componente
oView:EnableTitleView('VIEW_B61',STR0007) //'Produto' 

// Forcar o fechamento da tela ao salvar o model
oView:SetCloseOnOk({|| .T.})

Return oView   
