#Include 'Protheus.ch'
#Include 'plsa264.ch'

//---------------------------------------------------------------------------------------
/*/{Protheus.doc} PLSA271
Retorna os dados do beneficiario de acordo com a RN 389

@author	Lucas Nonato
@since		06/12/2016
@version	P12

/*/
//---------------------------------------------------------------------------------------
Function PLSA271( cMatric )
	Local cSQL       	:= ""
	Local cMatrDe     	:= ""
	Local cMatrAte    	:= ""
	Local cDataNasc   	:= ""
	Local cDataInsc   	:= ""
	Local cDatXInsc 	:= ""
	Local cDataValid  	:= ""
	Local cMotivo     	:= ""
	Local cMudaVal    	:= ""
	Local cProduto    	:= ""
	Local nQtd        	:= 0
	Local nPerRen     	:= 0
	Local nUltVia     	:= 0
	Local aStruc      	:= {}
	Local aRetNome    	:= {}
	Local aRetVlrCar  	:= {}
	Local aArea      	:= {}
	Local lOK         	:= .T.
	Local lErro       	:= .F.
	Local nPeRePF   	:= GetNewPar("MV_PLSPRPF",12)
	Local lImpressao	:= .T.
	Local nLayOut  		:= 4
	Local cTipUsu       := GetNewPar("MV_PLCDTIT","T")
	Local cTipReg       := PlGrauTit()
	Local dDatCpt
	Local dDatFimNUMCO
	Local nDias
	Local cNomTit
	Local nRegBTS
	Local nRegBA1
	LOCAL aCriticas   	:= {}
	local aDados		:= {}
	LOCAL cMaior		:= GetMv("MV_PLSMAIO",.F.,"2")
	LOCAL cUniver     	:= ""
	LOCAL _nIdMAXm    	:= 0
	Local _nAno       	:= 0
	Local _sMesDia    	:= ""
	Local bProcTit 		:= {|x| x == PLSEXP1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+GetNewPar('MV_PLTRTIT','00'))}
	Local aTitular		:= {}
	Local lNomCar		:= .F.
	Local lTpCtrBI3		:= (GetNewPar("MV_PLTPBI3","0")=="1") .AND. BII->(FieldPos("BII_TIPPLA")) > 0 // Verifica se busca o tipo de plano SIP pelo BI3 ou continua pelo BT5
	Local cTipPlan 		:= ''
	Local cNumPlan 		:= ''
	Local cDTContrat 	:= ''
	Local dMaxCarenc 	:= CtoD( "  /  /  " )
	Local nCont
	LOCAL cMvPLSFILA := GetMv("MV_PLSFILA")
	LOCAL cMvPLSFIAU := GetMv("MV_PLSFIAU")
	LOCAL cMvPLSFILO := GetMv("MV_PLSFILO")
	LOCAL cMvPLSFIOU := GetMv("MV_PLSFIOU")
	LOCAL cTpAcomod  := ""

	LOCAL _nTipo    	:= 3
	LOCAL lReemissao	:= .F.
	LOCAL lNewLot   	:= .F.
	
	dbSelectArea( "BA3" )
	BA3->( dbSetOrder( 1 ) )
	BA3->( dbSeek( xFilial( "BA3" ) + subStr( cMatric,1,4 ) + subStr( cMatric,5,4 ) + subStr( cMatric,9,6 ) ) )

	BQC->(DbSetOrder(1))

	//RN-360	
	//Tarja magnetica...
	Aadd(aStruc,{"TARJAMAG1"	,"C",34,0})
	Aadd(aStruc,{"TARJAMAG2"	,"C",30,0})

	//Frente do Cartao...
	Aadd(aStruc,{"NOMEUSUARI"  	,"C",30,0}) //nome do beneficiario
	Aadd(aStruc,{"MATRICULA"   	,"C",21,0}) //numero da matricula
	Aadd(aStruc,{"DTNACTO"     	,"C",10,0}) //data de ascimento do beneficiario
	Aadd(aStruc,{"DTVALID"     	,"C",10,0}) //data de ascimento do beneficiario
	Aadd(aStruc,{"DTINC"		,"C",10,0})
	Aadd(aStruc,{"PLANO"		,"C",04,0})
	Aadd(aStruc,{"TPCONTRATO"   ,"C",04,0})
	Aadd(aStruc,{"CNESUSU"	    ,"C",15,0}) //numero do cartao naciona de saude CNS
	Aadd(aStruc,{"SUSEP"	    ,"C",12,0}) //numero do registro do plano ou do cad. do plano na ans
	Aadd(aStruc,{"SEGASSPL"     ,"C",56,0}) //segmentacao assistencial do plano
	Aadd(aStruc,{"NUMREGOPE"    ,"C",6,0}) 	//codigo do registro da operadora na ans - BA0_SUSEP //BA0_FILIAL+BA0_CODIDE+BA0_CODINT
	Aadd(aStruc,{"CONTATOOPE"   ,"C",800,0}) //informacao de contato com a operadora - BIM_NOME-BIM_TELCON-BIM_EMAIL (Quando setor == 012 BIM_SETOR) //BIM_FILIAL+BIM_CODINT+BIM_CODIGO
	Aadd(aStruc,{"CONTATOANS"   ,"C",800,0}) //informacao de contato com a ans - BK5_NOME-BK5_TEL-BK5_EMAIL //BK5_FILIAL+BK5_NOME
	Aadd(aStruc,{"CPT"    		,"C",10,0})  //data de termino da cobertura parcial temporaria

	//Verso do Cartao...
	Aadd(aStruc,{"TPACOMODA"	,"C",100,0})  //padrao de acomodacao
	Aadd(aStruc,{"CONTRATACA"  	,"C",30,0})  //tipo de contratacao
	Aadd(aStruc,{"ABRANG"  		,"C",19,0})  //area de abrangencia geografica
	Aadd(aStruc,{"NOMPRO"		,"C",22,0})	 //nome do produto
	Aadd(aStruc,{"NFANTAZOPE"   ,"C",60,0})	 //nome fantasia da operadora - BA0_NOMINT
	Aadd(aStruc,{"NFAADMBENE"   ,"C",40,0})	 //nome fantasia da administradora de beneficios - BG9_NREDUZ
	Aadd(aStruc,{"RZSOCIAL"     ,"C",40,0})  //nome da p. juridica contratante do plano coletivo ou emp.
	Aadd(aStruc,{"DTVIGPL"  	,"C",10,0})  //data de inicio da vigencia do plano
	Aadd(aStruc,{"NUMCON"  		,"C",20,0})  //Numero do contrato ap�lice
	Aadd(aStruc,{"DATCON"  		,"C",10,0})  //Data de contrata��o do plano de sa�de
	Aadd(aStruc,{"DTMAXCON"  	,"C",10,0})  //Prazo m�ximo previsto no contrato para car�ncia
	Aadd(aStruc,{"INFOPLAN"  	,"C",50,0})  //Informa��o sobre a regulamenta��o do plano
	Aadd(aStruc,{"INFORMACOE"   ,"C",11,0})  //informacoes
	Aadd(aStruc,{"CARENCAMB"   ,"C",20,0})  //Car�ncia Procedimentos Ambulatorias  
	Aadd(aStruc,{"CARENCHOS"   ,"C",20,0})  //Car�ncia Procedimentos Hospitalares
	Aadd(aStruc,{"CARENCPAT"   ,"C",20,0})  //Car�ncia Procedimentos Parto a Termo
	Aadd(aStruc,{"CARENCODO"   ,"C",20,0})  //Car�ncia Procedimentos Odontol�gicos
				
				
	oTempTable := FWTemporaryTable():New( "Dados" )
	oTemptable:SetFields( aStruc )
	oTempTable:AddIndex( "indice1",{ "MATRICULA" } )
	oTempTable:Create()

	//� Tipo: Imprime Usuario/Familia Posicionario...               �
	cSQL := " SELECT BA1.R_E_C_N_O_ BA1REG, BA3.R_E_C_N_O_ BA3REG, BA1_DATNAS ,BA1_DATINC, BA1_DTVLCR, "
	cSQL += " 	BA1_CODINT, BA1_CODEMP, BA1_MATRIC, BA1_TIPREG, BA1_DIGITO, BA1_NOMUSR, BA1_TIPUSU, "
	cSQL += " 	BA1_DATCAR, BA1_VIACAR, BA1_OPERES, BA1_SEXO  , BA3_MATRIC, BA3_CODPLA, BA3_VERSAO, "
	cSQL += " 	BA3_TIPCON, BA3_TIPOUS, BA3_CONEMP, BA3_VERCON, BA3_SUBCON, BA3_VERSUB, "
	cSQL += " 	BA3_CODEMP, BA3_CODINT, BTS_NOMCAR, BTS_NOMUSR, BTS_NRCRNA, BA1_DATINC, BA1_NUMCON,BA1_CONEMP,BA1_VERCON "
	cSQL += " FROM "
	cSQL += retSQLName( "BA1" ) + " BA1 INNER JOIN " 
	cSQL += retSQLName( "BA3" ) + " BA3 ON "
	cSQL += " 	BA3.BA3_FILIAL = '"+ xFilial( "BA3" ) +"' AND "
	cSQL += " 	BA3.BA3_CODINT = BA1.BA1_CODINT AND "
	cSQL += " 	BA3.BA3_CODEMP = BA1.BA1_CODEMP AND "
	cSQL += " 	BA3.BA3_MATRIC = BA1.BA1_MATRIC AND "
	cSQL += " 	BA3.D_E_L_E_T_ = '' INNER JOIN "
	cSQL += retSQLName( "BTS" ) + " BTS ON "
	cSQL += " 	BTS.BTS_FILIAL = '"+ xFilial( "BTS" ) +"' AND "
	cSQL += " 	BTS.BTS_MATVID = BA1.BA1_MATVID AND "
	cSQL += " 	BTS.D_E_L_E_T_ = '' "
	cSQL += " WHERE "
	cSQL += " 	BA1.BA1_FILIAL = '"+ xFilial( "BA1" ) +"' AND "
	cSQL += " 	BA1.BA1_CODINT = '"+ subStr( cMatric,1,4 ) +"' AND "
	cSQL += " 	BA1.BA1_CODEMP = '"+ subStr( cMatric,5,4 ) +"' AND "
	cSQL += " 	BA1.BA1_MATRIC = '"+ subStr( cMatric,9,6 ) +"' AND "
	cSQL += " 	BA1.BA1_TIPREG = '"+ subStr( cMatric,15,2 ) +"' AND "
	cSQL += " 	BA1.BA1_DIGITO = '"+ subStr( cMatric,17,1 ) +"' AND "
	cSQL += " 	BA1.D_E_L_E_T_ = '' "
	cSQL += " ORDER BY BA1_CODINT,BA1_CODEMP,BA1_TIPREG,BA1_NOMUSR "
	
	cSQL := changeQuery( cSQL )
	dbUseArea(.T.,"TOPCONN",TCGENQRY(,,cSQL),"PLSEXP1",.F.,.T.)
	count to nTotal

	ProcRegua(nTotal)
	PLSEXP1->( dbGoTop() )

	While !PLSEXP1->(Eof())
		If PLSEXP1->BA1_TIPREG == cTipReg .Or. PLSEXP1->BA1_TIPUSU == cTipUsu
		
			//� Para qualquer exportacao ou emissao atualizar como modelo abaixo... �
			BA1->(DbGoTo(PLSEXP1->BA1REG))
			BA3->(DbGoTo(PLSEXP1->BA3REG))

			//� Verifica Regulamenta��o do Plano                                    �
			BI3->(dbSetOrder(1))
			BI3->(msSeek(xFilial("BF3")+BA3->(BA3_CODINT+BA3_CODPLA+BA3_VERSAO)))

			Do Case
			Case BI3->BI3_APOSRG == '0'
				cTipPlan := 'Plano Nao Regulamentado'
			Case BI3->BI3_APOSRG == '1'
				cTipPlan := 'Plano Regulamentado'
			Case BI3->BI3_APOSRG == '2'
				cTipPlan := 'Plano Adaptado'
			EndCase
			
			//� Verifica Prazo Maximo de carencia                                   �
			aVetCarenc := {}
			aRetCarenc := PLSCLACAR(BA1->BA1_CODINT,BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG+BA1_DIGITO),/*dDataBase*/)
		
			If Len(aRetCarenc) > 0 .And. aRetCarenc[1]
				dMaxCarenc := aRetCarenc[2][1][3]
				For nCont := 1 to len(aRetCarenc[2])
					If Len(aRetCarenc[2][nCont]) > 7
						If aRetCarenc[2][nCont][3] > dMaxCarenc
							dMaxCarenc := aRetCarenc[2][nCont][3]
						Endif
					Endif
				Next
			Endif

			//� Verifica CPT                                                        �
			BF3->(dbSetOrder(1))
			BF3->(msSeek(xFilial("BF3")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG)))
		
			dDatCpt := CtoD( "  /  /  " )
		
			While ! BF3->(eof()) .and. BF3->(BF3_FILIAL+BF3_CODINT+BF3_CODEMP+BF3_MATRIC+BF3_TIPREG) == xFilial("BF3")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG)
				If  ! empty(BF3->BF3_CODDOE)
					If BF3->BF3_UNAGR == "3" //"Meses"
						If  Empty(BF3->BF3_DATCPT)
							If  Empty(BA1->BA1_DATCPT)
								nDias	:=	Abs(( date() -MonthSum( date() , 0 )))
								dDatCPt:= date()+nDias
							Else
								nDias	:=	Abs((BA1->BA1_DATCPT -MonthSum(BA1->BA1_DATCPT , BF3->BF3_MESAGR )))
								dDatCPt:= BA1->BA1_DATCPT+nDias
							Endif
						Else
							nDias	:=	Abs((BF3->BF3_DATCPT -MonthSum(BF3->BF3_DATCPT , BF3->BF3_MESAGR )))
							dDatCPt:= BF3->BF3_DATCPT+nDias
						Endif
					Else
						nDias   := PLSCarDias(BF3->BF3_MESAGR,BF3->BF3_UNAGR)
						dDatFim := BF3->BF3_DATCPT + nDias
						If  dDatFim > dDatCpt
							dDatCpt := dDatFim
						Endif
					Endif
				Endif
				BF3->(dbSkip())
			Enddo
		
			If BA3->BA3_TIPOUS <> "1"
				If  BT5->(BT5_FILIAL+BT5_CODINT+BT5_CODIGO+BT5_NUMCON+BT5_VERSAO) <>  xFilial("BT5")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_CONEMP+BA1_VERCON)
					BT5->(DbSetOrder(1))
					BT5->(msSeek(xFilial("BT5")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_CONEMP+BA1_VERCON)))
				Endif
			
				If  BQC->(BQC_FILIAL+BQC_CODIGO+BQC_NUMCON+BQC_VERCON+BQC_SUBCON+BQC_VERSUB) <> xFilial("BQC")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_CONEMP+BA1_VERCON+BA1_SUBCON+BA1_VERSUB)
					BQC->(DbSetOrder(1))
					BQC->(msSeek(xFilial("BQC")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_CONEMP+BA1_VERCON+BA1_SUBCON+BA1_VERSUB)))
				Endif
			
				If  BQC->BQC_EMICAR == "0" // indica que nao deve gerar cartao para este subcontrato
					PLSEXP1->(DbSkip())
					Loop
				Endif
			Endif

			If  BG9->(BG9_FILIAL+BG9_CODINT+BG9_CODIGO) <> xFilial("BG9")+PLSEXP1->(BA3_CODINT+BA3_CODEMP)
				BG9->(DbSetOrder(1))
				BG9->(msSeek(xFilial("BG9")+PLSEXP1->(BA3_CODINT+BA3_CODEMP)))
			Endif

			//� Posiciona Produto do usuario... 									�
			If  ! empty(BA1->(BA1_CODPLA+BA1_VERSAO))
				If  BI3->(BI3_FILIAL+BI3_CODINT+BI3_CODIGO+BI3_VERSAO) <> xFilial("BI3")+BA1->(BA1_CODINT+BA1_CODPLA+BA1_VERSAO)
					BI3->(DbSetOrder(1))
					If  ! BI3->(msSeek(xFilial("BI3")+BA1->(BA1_CODINT+BA1_CODPLA+BA1_VERSAO)))
						Aadd(aCriticas, {BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG),STR0015})	        	 //"Plano do usu�rio n�o cadastrado."
						PLSEXP1->(DbSkip())
						lOK := .F.
						Loop
					Endif
				Endif
			Else
				If  BI3->(BI3_FILIAL+BI3_CODINT+BI3_CODIGO+BI3_VERSAO) <> xFilial("BI3")+BA3->(BA3_CODINT+BA3_CODPLA+BA3_VERSAO)
					BI3->(DbSetOrder(1))
					If ! BI3->(msSeek(xFilial("BI3")+BA3->(BA3_CODINT+BA3_CODPLA+BA3_VERSAO)))
						Aadd(aCriticas, {BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG),STR0016})   //"Plano da fam�lia n�o cadastrado."
						PLSEXP1->(DbSkip())
						lOK := .F.
						Loop
					Endif
				Endif
			Endif

			//� Verifica se foi selecionado produto na tela de lote de carteirinha  �
			If  ! empty(cProduto)
				If  BI3->BI3_CODIGO <> cProduto
					PLSEXP1->(DbSkip())
					Loop
				Endif
			Endif
		
			nRegBTS := BTS->(Recno())
			BA1->(DbSetOrder(2))
			BA1->(DbSeek(xFilial()+BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+GetNewPar('MV_PLTRTIT','00'))))
			BTS->(DbSetOrder(1))
			BTS->(DbSeek(xFilial()+BA1->BA1_MATVID))
		
			If BDE->( FieldPos("BDE_NOMCAR") ) > 0 .And. TYPE("M->BDE_NOMCAR") <> "U"
				If M->BDE_NOMCAR == "1"
					cNomTit := BTS->BTS_NOMCAR
				Else
					cNomTit := BTS->BTS_NOMUSR
				EndIf
			Else
				cNomTit := BTS->BTS_NOMCAR
			EndIf
		
			cUniver := BTS->BTS_UNIVER
		
			BTS->(DbGoto(nRegBTS))
			BA1->(DbGoTo(PLSEXP1->BA1REG))

			//� Verifica se usuario esta bloqueado...					 			�
			If UsuBlq264(BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC),BA1->BA1_TIPREG,dDatabase, BA1->BA1_DATBLO, BA1->BA1_MOTBLO, BA1->BA1_CONSID)
				PLSEXP1->(DbSkip())
				Loop
			Endif
		
			aRetVlrCar := PLSEXPIVL(BA1->BA1_CODINT	,	BA1->BA1_CODEMP,	BA1->BA1_CONEMP,	BA1->BA1_VERCON,;
				BI3->BI3_CODIGO,	BI3->BI3_VERSAO,	BG9->BG9_TIPO,		BA1->BA1_VIACAR,;
				BA1->BA1_TIPUSU,	BA1->BA1_GRAUPA,	BA1->BA1_MATRIC,	BA1->BA1_TIPREG,;
				BA1->BA1_SUBCON,	BA1->BA1_VERSUB,	If(nUltVia = 0,GetNewPar("MV_PLSMP1V","4"),cMotivo))
		
			If lOK .and. ! aRetVlrCar[8]
				MostraErro()
				lOK := .F.
			Endif
	
			If cMudaVal == "1"
				If Empty(dDatVal)
					If BA3->BA3_TIPOUS == "1"
						nPerRen := nPeRePF
					Else
						nPerRen := BQC->BQC_NPERRN
					Endif
					dDtVali := BA1->BA1_DTVLCR
					While nPerRen > 12
						dDtVali := stod(strzero((val(substr(dtos(dDtVali),1,4))+1),4)+substr(dtos(dDtVali),5,4))
						nPerRen := nPerRen-12
					EndDo
					dDtVali := stod(substr(dtos(stod(substr(dtos(dDtVali),1,6)+"15")+(nPerRen*30)),1,6)+substr(dtos(dDtVali),7,2))
				Else
					dDtVali := dDatVal
				Endif
			Else
				dDtVali := BA1->BA1_DTVLCR
				nViaCar := BA1->BA1_VIACAR
			Endif

			//� LayOut PLS... 						                                �
			cDataNasc := Eval({ || cDia := StrZero(Day(BA1->BA1_DATNAS),2), cMes := StrZero(Month(BA1->BA1_DATNAS),2), cAno := StrZero(Year(BA1->BA1_DATNAS),4), cDia+"/"+cMes+"/"+cAno })
			cDataInsc := Eval({ || cDia := StrZero(Day(BA1->BA1_DATINC),2), cMes := StrZero(Month(BA1->BA1_DATINC),2), cAno := StrZero(Year(BA1->BA1_DATINC),4), cDia+"/"+cMes+"/"+cAno })
			cDataValid:= Eval({ || cDia := StrZero(Day(dDtVali),2), cMes := StrZero(Month(dDtVali),2), cAno := StrZero(Year(dDtVali),4), cDia+"/"+cMes+"/"+cAno })
			cDatXInsc := Eval({ || cDia := StrZero(Day(BA1->BA1_DATINC),2), cMes := StrZero(Month(BA1->BA1_DATINC),2), cAno := StrZero(Year(BA1->BA1_DATINC),4), cDia+"/"+cMes+"/"+cAno })

			aRetNome:= PLSAVERNIV(	PLSEXP1->BA1_CODINT, PLSEXP1->BA1_CODEMP, PLSEXP1->BA1_MATRIC,	IF(PLSEXP1->BA3_TIPOUS=="1","F","J"),;
				PLSEXP1->BA3_CONEMP, PLSEXP1->BA3_VERCON, PLSEXP1->BA3_SUBCON, PLSEXP1->BA3_VERSUB, 1,;
				PLSEXP1->BA1_TIPREG)

			//� Alimenta arquivo com os dados para arquivo texto...			�
			Dados->(RecLock("Dados",.T.))

			//� LayOut PLS... 						                                �
			Dados->TARJAMAG1 :=	PLSEXP1->BA1_CODINT+;
				PLSEXP1->BA1_CODEMP+;
				PLSEXP1->BA1_MATRIC+;
				PLSEXP1->BA1_TIPREG+;
				PLSEXP1->BA1_DIGITO+;
				"="+; //Campo separador
				Strzero(nViaCar,2)+;
				SubStr(cDataValid,9,2)+SubStr(cDataValid,4,2)+;
				"="+; //Campo separador
				PLSEXP1->BA1_OPERES+;
				BI3->BI3_IDECAR+; //Codigo do produto no Intercambio
				SubStr(BI3->BI3_ABRANG,Len(BI3->BI3_ABRANG),1)+;
				PLSEXP1->BA3_TIPCON
								
			if BDE->( FieldPos("BDE_NOMCAR") ) > 0 .And. TYPE("M->BDE_NOMCAR") <> "U"
				if M->BDE_NOMCAR == "1"
					Dados->TARJAMAG2 := PLSEXP1->BTS_NOMCAR
				else
					Dados->TARJAMAG2 := PLSEXP1->BTS_NOMUSR
				endIf
			else
				Dados->TARJAMAG2 := PLSEXP1->BTS_NOMCAR
			endIf

			Dados->MATRICULA := PLSEXP1->BA1_CODINT + ;
				PLSEXP1->BA1_CODEMP + ;
				PLSEXP1->BA1_MATRIC + ;
				PLSEXP1->BA1_TIPREG + ;
				PLSEXP1->BA1_DIGITO

			cDTContrat := Eval({ || cDia := StrZero(Day(stod(PLSEXP1->BA1_DATINC)),2), cMes := StrZero(Month(stod(PLSEXP1->BA1_DATINC)),2), cAno := StrZero(Year(stod(PLSEXP1->BA1_DATINC)),4), cDia+"/"+cMes+"/"+cAno })
			Dados->DATCON		:= cDTContrat
			Dados->DTMAXCON	:= Eval({ || cDia := StrZero(Day(dMaxCarenc),2), cMes := StrZero(Month(dMaxCarenc),2), cAno := StrZero(Year(dMaxCarenc),4), cDia+"/"+cMes+"/"+cAno })
			Dados->INFOPLAN 	:= cTipPlan

			If  BA3->BA3_TIPOUS <> "1" .and. !Empty(BQC->BQC_NOMCAR)
				Dados->RZSOCIAL   := BQC->BQC_NOMCAR
			Else
				Dados->RZSOCIAL   := aRetNome[1][3]
			Endif
   			
   			// Classes de Carencia
			BDL->(DbSetOrder(1))
			If BDL->( msSeek(xFilial("BDL")+PLSEXP1->BA1_CODINT+Alltrim(GetNewPar("MV_PLCLAMB","XXX"))) )
				If BDL->BDL_UNCAR == "1" //1=Horas;2=Dias;3=Meses;4=Anos
					cDesUnCar := "Horas"
				ElseIf BDL->BDL_UNCAR == "2"
					cDesUnCar := "Dias"
				ElseIf BDL->BDL_UNCAR == "3"
					cDesUnCar := "Meses"
				ElseIf BDL->BDL_UNCAR == "4"
					cDesUnCar := "Anos"
				Else
					cDesUnCar :=""
				Endif                                                                                                 
				Dados->CARENCAMB := Alltrim(Str(BDL->BDL_CARENC)) + " "+cDesUnCar
			ELse
				Dados->CARENCAMB := ""
			Endif
			If BDL->( msSeek(xFilial("BDL")+PLSEXP1->BA1_CODINT+Alltrim(GetNewPar("MV_PLCLHOS","XXX"))) ) 
				If BDL->BDL_UNCAR == "1" //1=Horas;2=Dias;3=Meses;4=Anos
					cDesUnCar := "Horas"
				ElseIf BDL->BDL_UNCAR == "2"
					cDesUnCar := "Dias"
				ElseIf BDL->BDL_UNCAR == "3"
					cDesUnCar := "Meses"
				ElseIf BDL->BDL_UNCAR == "4"
					cDesUnCar := "Anos"
				Else
					cDesUnCar :=""
				Endif           
				Dados->CARENCHOS := Alltrim(Str(BDL->BDL_CARENC))+ " "+cDesUnCar 
			ELse
				Dados->CARENCHOS := ""
			Endif
			If BDL->( msSeek(xFilial("BDL")+PLSEXP1->BA1_CODINT+Alltrim(GetNewPar("MV_PLCLPAT","XXX"))) ) 
				If BDL->BDL_UNCAR == "1" //1=Horas;2=Dias;3=Meses;4=Anos
					cDesUnCar := "Horas"
				ElseIf BDL->BDL_UNCAR == "2"
					cDesUnCar := "Dias"
				ElseIf BDL->BDL_UNCAR == "3"
					cDesUnCar := "Meses"
				ElseIf BDL->BDL_UNCAR == "4"
					cDesUnCar := "Anos"
				Else
					cDesUnCar :=""
				Endif           
				Dados->CARENCPAT := Alltrim(Str(BDL->BDL_CARENC))+ " "+cDesUnCar  
			ELse
				Dados->CARENCPAT := ""
			Endif
			If BDL->( msSeek(xFilial("BDL")+PLSEXP1->BA1_CODINT+Alltrim(GetNewPar("MV_PLCLODO","XXX"))) )
				If BDL->BDL_UNCAR == "1" //1=Horas;2=Dias;3=Meses;4=Anos
					cDesUnCar := "Horas"
				ElseIf BDL->BDL_UNCAR == "2"
					cDesUnCar := "Dias"
				ElseIf BDL->BDL_UNCAR == "3"
					cDesUnCar := "Meses"
				ElseIf BDL->BDL_UNCAR == "4"
					cDesUnCar := "Anos"
				Else
					cDesUnCar :=""
				Endif           
				Dados->CARENCODO := Alltrim(Str(BDL->BDL_CARENC))+ " "+cDesUnCar   
			ELse
				Dados->CARENCODO := ""
			Endif
			
			Dados->INFORMACOE := STR0055 //"Informa��es"
			Dados->CNESUSU	 :=  AllTrim(PLSEXP1->BTS_NRCRNA)
			Dados->SEGASSPL  :=  padr(substr(POSICIONE("BI6",1,XFILIAL("BI6")+ BI3->BI3_CODSEG,"BI6_DESCRI"),1,56),56)
			
			BA0->(DbSetOrder(1))//BA0_FILIAL+BA0_CODIDE+BA0_CODINT
			BA0->( msSeek(xFilial("BA0")+PLSEXP1->BA1_CODINT) )
			
			If BA0->( Fieldpos("BA0_AUTGES") ) > 0 .And.  BA0->BA0_AUTGES == '0'
				cNumPlan := BI3->(BI3_CODINT+BI3_CODIGO+BI3_VERSAO)
			Else				
				cNumPlan := BI3->BI3_SUSEP
			EndIf
			
			Dados->NUMCON		:= cNumPlan
			
			Dados->NUMREGOPE  := BA0->BA0_SUSEP
			Dados->NFANTAZOPE := BA0->BA0_NOMINT

			/*BIM->(DbSetOrder(1))//BIM_FILIAL+BIM_CODINT+BIM_CODIGO
			BIM->( msSeek(xFilial("BIM")+PLSEXP1->BA1_CODINT) )
			
			while !BIM->(eof())
				if BIM->BIM_SETOR == GetNewPar("MV_PLBIMSE","012")
					Dados->CONTATOOPE  := allTrim(padR(BIM->BIM_NOME,30)) +" / "+ AllTrim(padR(BIM->BIM_TELCON,15)) +" / "+ AllTrim(padR(BIM->BIM_EMAIL,30))
					exit
				endIf
			
				BIM->(dbSkip())
			endDo
			*/
			
			BMV->(DbSetOrder(1))//BMV_FILIAL+BMV_CODIGO
			If BMV->( MsSeek(xFilial("BMV")+"STR0019") )
				Dados->CONTATOANS := AllTrim(BMV->BMV_MSGPOR) 
			Else
				Dados->CONTATOANS := ""
			Endif
		
			If BMV->( MsSeek(xFilial("BMV")+"STR0020") )
				Dados->CONTATOOPE := AllTrim(BMV->BMV_MSGPOR)
			Else
				Dados->CONTATOOPE := ""
			Endif
			
			Dados->CPT        := IIF(empty(dDatCpt),PadR('',12),substr(dtos(dDatCpt),7,2)+"/"+substr(dtos(dDatCpt),5,2)+"/"+substr(dtos(dDatCpt),1,4))

			BG9->(DbSetOrder(1))//BG9_FILIAL+BG9_CODINT+BG9_CODIGO+BG9_TIPO
			BG9->( msSeek( xFilial("BG9")+PLSEXP1->(BA1_CODINT+BA1_CODEMP) ) )

			Dados->NFAADMBENE := BG9->BG9_NREDUZ
			Dados->DTNACTO    := cDataNasc

			If  BA3->BA3_TIPOUS == "1"
				Do Case
				Case BI3->BI3_NATJCO = "2"
					Dados->CONTRATACA := 'INDIVIDUAL OU FAMILIAR'
				Case BI3->BI3_NATJCO = "3"
					Dados->CONTRATACA := 'COLETIVO EMPRESARIAL'
				Case BI3->BI3_NATJCO = "4"
					Dados->CONTRATACA := 'COLETIVO POR ADESAO'
				Case BI3->BI3_NATJCO = "5"
					Dados->CONTRATACA := 'BENEFICIENTE'
				Otherwise
					Dados->CONTRATACA := "SEM CONTRATACAO"
				EndCase
			Else
				If  AllTrim(BQC->BQC_ENTFIL) == "1"
					Dados->CONTRATACA := 'BENEFICIENTE'
				Else
					BII->(DbSetOrder(1))
					If lTpCtrBI3 .AND. BII->( msSeek(xFilial("BII")+BI3->BI3_TIPCON) ) 
						If  AllTrim(BII->BII_TIPPLA) $ "1,3"
							Dados->CONTRATACA := 'COLETIVO POR ADESAO'
						Else
							Dados->CONTRATACA := 'COLETIVO EMPRESARIAL'
						Endif
					Else
						If BII->( msSeek(xFilial("BII")+BT5->BT5_TIPCON) ) 
							If  AllTrim(BII->BII_TIPPLA) $ "1,3"
								Dados->CONTRATACA := 'COLETIVO POR ADESAO'
							Else
								Dados->CONTRATACA := 'COLETIVO EMPRESARIAL'
							Endif
						Endif
					EndIf
				Endif
			Endif
			
			cTpAcomod := PLSGETVINC("BTQ_DESTER", "BI4",.F.,"49",PLSGETVINC("BTU_CDTERM", "BI4", .F., "49",AllTrim(BI3->BI3_CODACO),.F. ),.F.) 
			
			Dados->TPACOMODA := cTpAcomod
				
			/*BI4->(dbSetOrder(1))
			If Empty(BI3->BI3_CODACO)
				Dados->TPACOMODA := PadR("" /*STR0061, 13)	// "NAO SE APLICA"
			/*Else
				If BI4->(MsSeek(xFilial("BI4")+AllTrim(BI3->(BI3_CODACO)))) .And.;
						BI4->(FieldPos("BI4_CODEDI")) > 0
											
					If Empty(BI4->BI4_CODEDI)
						Dados->TPACOMODA := PadR("" /*STR0061, 13)	// "NAO SE APLICA"
					/*Else
						If AllTrim(BI4->BI4_CODEDI) == "1"
							Dados->TPACOMODA := PadR(STR0062,13)	// INDIVIDUAL
						ElseIf AllTrim(BI4->BI4_CODEDI) == "2"
							Dados->TPACOMODA := PadR(STR0063,13)	// COLETIVA
						Else
							Dados->TPACOMODA := PadR("" /*STR0061, 13) 	// "NAO SE APLICA"
						/*EndIf
					EndIf
				Else
					Dados->TPACOMODA := PadR("" /*STR0061, 13)	// "NAO SE APLICA"
				/*EndIf
			EndIf*/

			If BDE->( FieldPos( "BDE_NOMCAR" ) ) > 0 .And. TYPE( "M->BDE_NOMCAR" ) <> "U"
				If M->BDE_NOMCAR == "1"
					Dados->NOMEUSUARI := PadR(PLSEXP1->BTS_NOMCAR,25)
				Else
					Dados->NOMEUSUARI := PadR(PLSEXP1->BTS_NOMUSR,25)
				EndIf
			Else
				Dados->NOMEUSUARI := PLSEXP1->BTS_NOMCAR 
			EndIF

			Dados->ABRANG := Posicione("BF7",1,xFilial("BF7")+BI3->BI3_ABRANG,"BF7_DESORI")
			
			Dados->DTVIGPL 	:= cDatXInsc
			Dados->NOMPRO   := BI3->BI3_NREDUZ
			Dados->SUSEP 	:= Iif(BI3->BI3_APOSRG == "1",BI3->BI3_SUSEP,BI3->BI3_SCPA)
			Dados->(MsUnlock())

			cSQL := "SELECT BA1.R_E_C_N_O_ BA1REG, BA3.R_E_C_N_O_ BA3REG, BA1_DATNAS ,BA1_DATINC, BA1_DTVLCR, "
			cSQL += 	  " BA1_CODINT, BA1_CODEMP, BA1_MATRIC, BA1_TIPREG, BA1_DIGITO, BA1_NOMUSR, BA1_MATVID, BA1_TIPUSU,"
			cSQL += 	  " BA1_DATCAR, BA1_VIACAR, BA1_OPERES, BA1_SEXO,   BA3_CODPLA, BA3_VERSAO, BA3_TIPCON, "
			cSQL += 	  " BA3_TIPOUS, BA3_CONEMP, BA3_VERCON, BA3_SUBCON, BA3_VERSUB, BA3_CODEMP, "
			cSQL += 	  " BA3_CODINT, BTS_NOMCAR, BTS_NOMUSR, BTS_NRCRNA "
			cSQL += " FROM "
			cSQL += retSQLName( "BA1" ) + " BA1 INNER JOIN " 
			cSQL += retSQLName( "BA3" ) + " BA3 ON "
			cSQL += " 	BA3.BA3_FILIAL = '"+ xFilial( "BA3" ) +"' AND "
			cSQL += " 	BA3.BA3_CODINT = BA1.BA1_CODINT AND "
			cSQL += " 	BA3.BA3_CODEMP = BA1.BA1_CODEMP AND "
			cSQL += " 	BA3.BA3_MATRIC = BA1.BA1_MATRIC AND "
			cSQL += " 	BA3.D_E_L_E_T_ = '' INNER JOIN "
			cSQL += retSQLName( "BTS" ) + " BTS ON "
			cSQL += " 	BTS.BTS_FILIAL = '"+ xFilial( "BTS" ) +"' AND "
			cSQL += " 	BTS.BTS_MATVID = BA1.BA1_MATVID AND "
			cSQL += " 	BTS.D_E_L_E_T_ = '' "
			cSQL += " WHERE "
			cSQL += " 	BA1.BA1_FILIAL = '"+ xFilial( "BA1" ) +"' AND "
			cSQL += " 	BA1.BA1_CODINT = '"+ subStr( cMatric,1,4 ) +"' AND "
			cSQL += " 	BA1.BA1_CODEMP = '"+ subStr( cMatric,5,4 ) +"' AND "
			cSQL += " 	BA1.BA1_MATRIC = '"+ subStr( cMatric,9,6 ) +"' AND "
			cSQL += " 	BA1.BA1_TIPREG = '"+ subStr( cMatric,15,2 ) +"' AND "
			cSQL += " 	BA1.BA1_DIGITO = '"+ subStr( cMatric,17,1 ) +"' AND "
			cSql += " 	BA1.BA1_TIPREG <> '" + cTipReg + "' AND "
			cSQL += " 	BA1.D_E_L_E_T_ = '' "
			cSQL += " ORDER BY BA1_CODINT,BA1_CODEMP,BA1_NOMUSR "
			cSQL := ChangeQuery(cSQL)
			dbUseArea(.T.,"TOPCONN",TCGENQRY(,,cSQL),"PLSEXP2",.F.,.T.)		
		
			While !PLSEXP2->(Eof())
			
				If _nTipo <> 2
					IncProc(STR0035 + Str(nQtd,10)) // "Selecionando inf. para cart�o magnetico. "
				Else
					If ! (AllTrim(PLSEXP2->(BA1_MATRIC+BA1_TIPREG)) >= AllTrim(cMatrDe) .And.;
							AllTrim(PLSEXP2->(BA1_MATRIC+BA1_TIPREG)) <= AllTrim(cMatrAte))
						PLSEXP2->(dbSkip())
						Loop
					EndIf
					IncProc(STR0035 + AllTrim(Str(nQtd,10)) + "/" + AllTrim(Str(nTotal,10))) // "Selecionando inf. para cart�o magnetico. "
				EndIf
			
				//� Para qualquer exportacao ou emissao atualizar como modelo abaixo... �
				BA1->(DbGoTo(PLSEXP2->BA1REG))
				BA3->(DbGoTo(PLSEXP2->BA3REG))
				
				//� Verifica CPT                                                        �
				BF3->(dbSetOrder(1))
				BF3->(msSeek(xFilial("BF3")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG)))
				dDatCpt := ctod("  /  /  ")
				While ! BF3->(eof()) .and. BF3->(BF3_FILIAL+BF3_CODINT+BF3_CODEMP+BF3_MATRIC+BF3_TIPREG) == xFilial("BF3")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG)
					If  ! empty(BF3->BF3_CODDOE)
						If BF3->BF3_UNAGR == "3" //"Meses"
							If  Empty(BF3->BF3_DATCPT)
								If  Empty(BA1->BA1_DATCPT)
									nDias	:=	Abs(( date() -MonthSum( date() , 0 )))
									dDatCPt:= date()+nDias
								Else
									nDias	:=	Abs((BA1->BA1_DATCPT -MonthSum(BA1->BA1_DATCPT , BF3->BF3_MESAGR )))
									dDatCPt:= BA1->BA1_DATCPT+nDias
								Endif
							Else
								nDias	:=	Abs((BF3->BF3_DATCPT -MonthSum(BF3->BF3_DATCPT , BF3->BF3_MESAGR )))
								dDatCPt:= BF3->BF3_DATCPT+nDias
							Endif
						Else
							nDias   := PLSCarDias(BF3->BF3_MESAGR,BF3->BF3_UNAGR)
							dDatFim := BF3->BF3_DATCPT + nDias
							If  dDatFim > dDatCpt
								dDatCpt := dDatFim
							Endif
						Endif
					Endif
					BF3->(dbSkip())
				Enddo
			
				If BA3->BA3_TIPOUS <> "1"
				
					If  BT5->(BT5_FILIAL+BT5_CODINT+BT5_CODIGO+BT5_NUMCON+BT5_VERSAO) <> xFilial("BT5")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_CONEMP+BA1_VERCON)
						BT5->(DbSetOrder(1))
						BT5->(msSeek(xFilial("BT5")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_CONEMP+BA1_VERCON)))
						If ! BT5->(Found())
							Aadd(aCriticas, {BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG), STR0013}) //"Contrato n�o Cadastrado."
							PLSEXP2->(DbSkip())
							lOK := .F.
							Loop
						Endif
					Endif
				
					If  BQC->(BQC_FILIAL+BQC_CODIGO+BQC_NUMCON+BQC_VERCON+BQC_SUBCON+BQC_VERSUB) <> xFilial("BQC")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_CONEMP+BA1_VERCON+BA1_SUBCON+BA1_VERSUB)
						BQC->(DbSetOrder(1))
						BQC->(msSeek(xFilial("BQC")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_CONEMP+BA1_VERCON+BA1_SUBCON+BA1_VERSUB)))
						If ! BQC->(Found())
							Aadd(aCriticas, {BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG),STR0014}) //"Sub-Contrato n�o Cadastrado."
							PLSEXP2->(DbSkip())
							lOK := .F.
							Loop
						Endif
					Endif
				
					If  BQC->BQC_EMICAR == "0" // indica que nao deve gerar cartao para este subcontrato
						PLSEXP2->(DbSkip())
						Loop
					Endif
				Endif
			
				If  BG9->(BG9_FILIAL+BG9_CODINT+BG9_CODIGO) <> xFilial("BG9")+PLSEXP2->(BA3_CODINT+BA3_CODEMP)
					BG9->(DbSetOrder(1))
					BG9->(msSeek(xFilial("BG9")+PLSEXP2->(BA3_CODINT+BA3_CODEMP)))
				Endif
				
				//� Posiciona Produto do usuario... 									�
				If  ! empty(BA1->(BA1_CODPLA+BA1_VERSAO))
					If  BI3->(BI3_FILIAL+BI3_CODINT+BI3_CODIGO+BI3_VERSAO) <> xFilial("BI3")+BA1->(BA1_CODINT+BA1_CODPLA+BA1_VERSAO)
						BI3->(DbSetOrder(1))
						If  ! BI3->(msSeek(xFilial("BI3")+BA1->(BA1_CODINT+BA1_CODPLA+BA1_VERSAO)))
							Aadd(aCriticas, {BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG),STR0015}) //"Plano do usu�rio n�o cadastrado."
							PLSEXP2->(DbSkip())
							lOK := .F.
							Loop
						Endif
					Endif
				Else
					If  BI3->(BI3_FILIAL+BI3_CODINT+BI3_CODIGO+BI3_VERSAO) <> xFilial("BI3")+BA3->(BA3_CODINT+BA3_CODPLA+BA3_VERSAO)
						BI3->(DbSetOrder(1))
						If ! BI3->(msSeek(xFilial("BI3")+BA3->(BA3_CODINT+BA3_CODPLA+BA3_VERSAO)))
							Aadd(aCriticas, {BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG),STR0016}) //"Plano da familia n�o cadastrado."
							PLSEXP2->(DbSkip())
							lOK := .F.
							Loop
						Endif
					Endif
				Endif
				
				//� Verifica se foi selecionado produto na tela de lote de carteirinha  �
				If  ! empty(cProduto)
					If  BI3->BI3_CODIGO <> cProduto
						PLSEXP2->(DbSkip())
						Loop
					Endif
				Endif
				nRegBTS := BTS->(Recno())
				BA1->(DbSetOrder(2))
				BA1->(DbSeek(xFilial()+BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+GetNewPar('MV_PLTRTIT','00'))))
				BTS->(DbSetOrder(1))
				BTS->(DbSeek(xFilial()+BTS->BTS_MATVID))
			
				cUniver := BTS->BTS_UNIVER
				BTS->(DbGoto(nRegBTS))
				BA1->(DbGoTo(PLSEXP2->BA1REG))

				//� Verifica se usuario esta bloqueado...					 			�
				If UsuBlq264(BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC),BA1->BA1_TIPREG,dDatabase, BA1->BA1_DATBLO, BA1->BA1_MOTBLO, BA1->BA1_CONSID)
					PLSEXP2->(DbSkip())
					Loop
				Endif

				aRetVlrCar := PLSEXPIVL(BA1->BA1_CODINT	,	BA1->BA1_CODEMP,	BA1->BA1_CONEMP,	BA1->BA1_VERCON,;
					BI3->BI3_CODIGO,	BI3->BI3_VERSAO,	BG9->BG9_TIPO,		BA1->BA1_VIACAR,;
					BA1->BA1_TIPUSU,	BA1->BA1_GRAUPA,	BA1->BA1_MATRIC,	BA1->BA1_TIPREG,;
					BA1->BA1_SUBCON,	BA1->BA1_VERSUB,	If(nUltVia = 0,GetNewPar("MV_PLSMP1V","4"),cMotivo))
			
				If lOK .and. ! aRetVlrCar[8]
					MostraErro()
					lOK := .F.
				Endif

				nViaCar := BA1->BA1_VIACAR
				dDtVali := BA1->BA1_DTVLCR
				
				//� LayOut PLS... 						                                �
				cDataNasc := Eval({ || cDia := StrZero(Day(BA1->BA1_DATNAS),2), cMes := StrZero(Month(BA1->BA1_DATNAS),2), cAno := StrZero(Year(BA1->BA1_DATNAS),4), cDia+"/"+cMes+"/"+cAno })
				cDataInsc := Eval({ || cDia := StrZero(Day(BA1->BA1_DATINC),2), cMes := StrZero(Month(BA1->BA1_DATINC),2), cAno := StrZero(Year(BA1->BA1_DATINC),4), cDia+"/"+cMes+"/"+cAno })
				cDataValid:= Eval({ || cDia := StrZero(Day(dDtVali),2), cMes := StrZero(Month(dDtVali),2), cAno := StrZero(Year(dDtVali),4), cDia+"/"+cMes+"/"+cAno })
				cDatXInsc := Eval({ || cDia := StrZero(Day(BA1->BA1_DATINC),2), cMes := StrZero(Month(BA1->BA1_DATINC),2), cAno := StrZero(Year(BA1->BA1_DATINC),4), cDia+"/"+cMes+"/"+cAno })

				aRetNome:= PLSAVERNIV(	PLSEXP2->BA1_CODINT, PLSEXP2->BA1_CODEMP, PLSEXP2->BA1_MATRIC,	IF(PLSEXP2->BA3_TIPOUS=="1","F","J"),;
					PLSEXP2->BA3_CONEMP, PLSEXP2->BA3_VERCON, PLSEXP2->BA3_SUBCON, PLSEXP2->BA3_VERSUB, 1,;
					PLSEXP2->BA1_TIPREG)

				//� Alimenta arquivo com os dados para arquivo texto...			�
				Dados->(RecLock("Dados",.T.))

				//� LayOut PLS... 						                                �
				Dados->TARJAMAG1 :=	PLSEXP2->BA1_CODINT+;
					PLSEXP2->BA1_CODEMP+;
					PLSEXP2->BA1_MATRIC+;
					PLSEXP2->BA1_TIPREG+;
					PLSEXP2->BA1_DIGITO+;
					"="+; //Campo separador
					Strzero(nViaCar,2)+;
					SubStr(cDataValid,9,2)+SubStr(cDataValid,4,2)+;
					"="+; //Campo separador
					PLSEXP2->BA1_OPERES+;
					BI3->BI3_IDECAR+; //Codigo do produto no Intercambio
					SubStr(BI3->BI3_ABRANG,Len(BI3->BI3_ABRANG),1)+;
					PLSEXP2->BA3_TIPCON
									
				if BDE->( FieldPos("BDE_NOMCAR") ) > 0 .And. TYPE("M->BDE_NOMCAR") <> "U"
					if M->BDE_NOMCAR == "1"
						Dados->TARJAMAG2 := PLSEXP2->BTS_NOMCAR
					else
						Dados->TARJAMAG2 := PLSEXP2->BTS_NOMUSR
					endIf
				else
					Dados->TARJAMAG2 := PLSEXP2->BTS_NOMCAR
				endIf
	
				Dados->MATRICULA := PLSEXP2->BA1_CODINT + ;
					PLSEXP2->BA1_CODEMP + ;
					PLSEXP2->BA1_MATRIC + ;
					PLSEXP2->BA1_TIPREG + ;
					PLSEXP2->BA1_DIGITO
					

				If  BA3->BA3_TIPOUS <> "1" .and. !Empty(BQC->BQC_NOMCAR)
					Dados->RZSOCIAL   := BQC->BQC_NOMCAR
				Else
					Dados->RZSOCIAL   := aRetNome[1][3]
				Endif
				
				// Classes de Carencia
				BDL->(DbSetOrder(1))
				If BDL->( msSeek(xFilial("BDL")+PLSEXP2->BA1_CODINT+Alltrim(GetNewPar("MV_PLCLAMB","XXX"))) )  
					
					If BDL->BDL_UNCAR == "1" //1=Horas;2=Dias;3=Meses;4=Anos
						cDesUnCar := "Horas"
					ElseIf BDL->BDL_UNCAR == "2"
						cDesUnCar := "Dias"
					ElseIf BDL->BDL_UNCAR == "3"
						cDesUnCar := "Meses"
					ElseIf BDL->BDL_UNCAR == "4"
						cDesUnCar := "Anos"
					Else
						cDesUnCar :=""
					Endif                            
				
					Dados->CARENCAMB := Alltrim(Str(BDL->BDL_CARENC))+" "+cDesUnCar 
				ELse
					Dados->CARENCAMB := ""
				Endif
				If BDL->( msSeek(xFilial("BDL")+PLSEXP2->BA1_CODINT+Alltrim(GetNewPar("MV_PLCLHOS","XXX"))) ) 
					If BDL->BDL_UNCAR == "1" //1=Horas;2=Dias;3=Meses;4=Anos
						cDesUnCar := "Horas"
					ElseIf BDL->BDL_UNCAR == "2"
						cDesUnCar := "Dias"
					ElseIf BDL->BDL_UNCAR == "3"
						cDesUnCar := "Meses"
					ElseIf BDL->BDL_UNCAR == "4"
						cDesUnCar := "Anos"
					Else
						cDesUnCar :=""
					Endif                
					Dados->CARENCHOS := Alltrim(Str(BDL->BDL_CARENC))+" "+cDesUnCar  
				ELse
					Dados->CARENCHOS := ""
				Endif
				If BDL->( msSeek(xFilial("BDL")+PLSEXP2->BA1_CODINT+Alltrim(GetNewPar("MV_PLCLPAT","XXX"))) )  
					If BDL->BDL_UNCAR == "1" //1=Horas;2=Dias;3=Meses;4=Anos
						cDesUnCar := "Horas"
					ElseIf BDL->BDL_UNCAR == "2"
						cDesUnCar := "Dias"
					ElseIf BDL->BDL_UNCAR == "3"
						cDesUnCar := "Meses"
					ElseIf BDL->BDL_UNCAR == "4"
						cDesUnCar := "Anos"
					Else
						cDesUnCar :=""
					Endif                
					Dados->CARENCPAT := Alltrim(Str(BDL->BDL_CARENC))+" "+cDesUnCar  
				ELse
					Dados->CARENCPAT := ""
				Endif
				If BDL->( msSeek(xFilial("BDL")+PLSEXP2->BA1_CODINT+Alltrim(GetNewPar("MV_PLCLODO","XXX"))) ) 
					If BDL->BDL_UNCAR == "1" //1=Horas;2=Dias;3=Meses;4=Anos
						cDesUnCar := "Horas"
					ElseIf BDL->BDL_UNCAR == "2"
						cDesUnCar := "Dias"
					ElseIf BDL->BDL_UNCAR == "3"
						cDesUnCar := "Meses"
					ElseIf BDL->BDL_UNCAR == "4"
						cDesUnCar := "Anos"
					Else
						cDesUnCar :=""
					Endif                
					Dados->CARENCODO := Alltrim(Str(BDL->BDL_CARENC))+" "+cDesUnCar  
				ELse
					Dados->CARENCODO := ""
				Endif   
				
				Dados->INFORMACOE := STR0055 //"Informa��es"
				Dados->CNESUSU	 :=  AllTrim(PLSEXP2->BTS_NRCRNA)
				Dados->SEGASSPL  :=  padr(substr(POSICIONE("BI6",1,XFILIAL("BI6")+ BI3->BI3_CODSEG,"BI6_DESCRI"),1,56),56)
				
				BA0->(DbSetOrder(1))//BA0_FILIAL+BA0_CODIDE+BA0_CODINT
				BA0->( msSeek(xFilial("BA0")+PLSEXP2->BA1_CODINT) )
				
				If BA0->( Fieldpos("BA0_AUTGES") ) > 0 .And.  BA0->BA0_AUTGES == '0'
					cNumPlan := BI3->(BI3_CODINT+BI3_CODIGO+BI3_VERSAO)
				Else				
					cNumPlan := BI3->BI3_SUSEP
				EndIf
				
				Dados->NUMREGOPE  := BA0->BA0_SUSEP
				Dados->NFANTAZOPE := BA0->BA0_NOMINT
	
				/*BIM->(DbSetOrder(1))//BIM_FILIAL+BIM_CODINT+BIM_CODIGO
				BIM->( msSeek(xFilial("BIM")+PLSEXP2->BA1_CODINT) )
				
				while !BIM->(eof())
					if BIM->BIM_SETOR == GetNewPar("MV_PLBIMSE","012")
						Dados->CONTATOOPE  := allTrim(padR(BIM->BIM_NOME,30)) +" / "+ AllTrim(padR(BIM->BIM_TELCON,15)) +" / "+ AllTrim(padR(BIM->BIM_EMAIL,30))
						exit
					endIf
				
					BIM->(dbSkip())
				endDo
				*/
				
				BMV->(DbSetOrder(1))//BMV_FILIAL+BMV_CODIGO
				If BMV->( MsSeek(xFilial("BMV")+"STR0019") )
					Dados->CONTATOANS := AllTrim(BMV->BMV_MSGPOR) 
				Else
					Dados->CONTATOANS := ""
				Endif
			
				If BMV->( MsSeek(xFilial("BMV")+"STR0020") )
					Dados->CONTATOOPE := AllTrim(BMV->BMV_MSGPOR)
				Else
					Dados->CONTATOOPE := ""
				Endif
				
				Dados->CPT        := IIF(empty(dDatCpt),PadR('',12),substr(dtos(dDatCpt),7,2)+"/"+substr(dtos(dDatCpt),5,2)+"/"+substr(dtos(dDatCpt),1,4))
		
				BG9->(DbSetOrder(1))//BG9_FILIAL+BG9_CODINT+BG9_CODIGO+BG9_TIPO
				BG9->( msSeek( xFilial("BG9")+PLSEXP2->(BA1_CODINT+BA1_CODEMP) ) )
	
				Dados->NFAADMBENE := BG9->BG9_NREDUZ
				Dados->DTNACTO    := cDataNasc
	
				If  BA3->BA3_TIPOUS == "1"
					Do Case
					Case BI3->BI3_NATJCO = "2"
						Dados->CONTRATACA := 'INDIVIDUAL OU FAMILIAR'
					Case BI3->BI3_NATJCO = "3"
						Dados->CONTRATACA := 'COLETIVO EMPRESARIAL'
					Case BI3->BI3_NATJCO = "4"
						Dados->CONTRATACA := 'COLETIVO POR ADESAO'
					Case BI3->BI3_NATJCO = "5"
						Dados->CONTRATACA := 'BENEFICIENTE'
					Otherwise
						Dados->CONTRATACA := "SEM CONTRATACAO"
					EndCase
				Else
					If  AllTrim(BQC->BQC_ENTFIL) == "1"
						Dados->CONTRATACA := 'BENEFICIENTE'
					Else
						BII->(DbSetOrder(1))
						If lTpCtrBI3 .AND. BII->( msSeek(xFilial("BII")+BI3->BI3_TIPCON) ) .AND. !EMPTY(BII->BII_TIPPLA)
							If  AllTrim(BII->BII_TIPPLA) $ "1,3"
								Dados->CONTRATACA := 'COLETIVO POR ADESAO'
							Else
								Dados->CONTRATACA := 'COLETIVO EMPRESARIAL'
							Endif
						Else
							If BII->( msSeek(xFilial("BII")+BT5->BT5_TIPCON) ) .AND. !EMPTY(BII->BII_TIPPLA)
								If  AllTrim(BII->BII_TIPPLA) $ "1,3"
									Dados->CONTRATACA := 'COLETIVO POR ADESAO'
								Else
									Dados->CONTRATACA := 'COLETIVO EMPRESARIAL'
								Endif
							Endif
						EndIf
					Endif
				Endif
					
				/*BI4->(dbSetOrder(1))
				If Empty(BI3->BI3_CODACO)
					Dados->TPACOMODA := PadR("" /*STR0061, 13)	// "NAO SE APLICA"
				/*Else
					If BI4->(MsSeek(xFilial("BI4")+AllTrim(BI3->(BI3_CODACO)))) .And.;
							BI4->(FieldPos("BI4_CODEDI")) > 0
												
						If Empty(BI4->BI4_CODEDI)
							Dados->TPACOMODA := PadR("" /*STR0061, 13)	// "NAO SE APLICA"
						/*Else
							If AllTrim(BI4->BI4_CODEDI) == "1"
								Dados->TPACOMODA := PadR(STR0062,13)	// INDIVIDUAL
							ElseIf AllTrim(BI4->BI4_CODEDI) == "2"
								Dados->TPACOMODA := PadR(STR0063,13)	// COLETIVA
							Else
								Dados->TPACOMODA := PadR("" /*STR0061, 13) 	// "NAO SE APLICA"
							/*EndIf
						EndIf
					Else
						Dados->TPACOMODA := PadR("" /*STR0061, 13)	// "NAO SE APLICA"
					/*EndIf
				EndIf*/
	
				BTQ->(dbSetOrder(1))
				BTQ->(MsSeek(xFilial("BTQ") + "49" + AllTrim(BI3->BI3_CODACO)))
			
				Dados->TPACOMODA := BTQ->BTQ_DESTER
			
				If BDE->( FieldPos("BDE_NOMCAR") ) > 0 .And. TYPE("M->BDE_NOMCAR") <> "U"
					If M->BDE_NOMCAR == "1"
						Dados->NOMEUSUARI := PadR(PLSEXP2->BTS_NOMCAR,25)
					Else
						Dados->NOMEUSUARI := PadR(PLSEXP2->BTS_NOMUSR,25)
					EndIf
				Else
					Dados->NOMEUSUARI := PLSEXP2->BTS_NOMCAR  
				EndIF
				
				Dados->ABRANG := Posicione("BF7",1,xFilial("BF7")+BI3->BI3_ABRANG,"BF7_DESORI")
				
				Dados->DTVIGPL 	:= cDatXInsc
				Dados->NOMPRO   := BI3->BI3_NREDUZ
				Dados->SUSEP 	:= Iif(BI3->BI3_APOSRG == "1",BI3->BI3_SUSEP,BI3->BI3_SCPA)
				Dados->(MsUnlock())
				PLSEXP2->(dbSkip())
			EndDo
			PLSEXP2->(dbCloseArea())
			//�Imprime dependente que esta sem titular no arquivo	�
		Else
			nTemTit   := aScan(aTitular, bProcTit)
		
			//� Verifica se o titular esta bloqueado (para gerar cartao de  �
		    //� dependente que nao esta bloqueado.                          �
			lTitBloq := .F. //Reinicializa variavel
			aArea 	 := BA1->(GetArea())
			BA1->(DbSetOrder(2))
			BA1->(msSeek(xFilial("BA1")+PLSEXP1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC)))
			While BA1->BA1_MATRIC == PLSEXP1->BA1_MATRIC .And. !BA1->( Eof())
				If BA1->BA1_TIPREG == cTipReg .Or. BA1->BA1_TIPUSU == cTipUsu
					If UsuBlq264(BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC),BA1->BA1_TIPREG,dDatabase, BA1->BA1_DATBLO, BA1->BA1_MOTBLO, BA1->BA1_CONSID)
						lTitBloq := .T.
						Exit
					EndIf
				EndIf
				BA1->(DbSkip())
			EndDo
			RestArea(aArea)
		
			If nTemTit == 0 .Or. lTitBloq
				//� Alimenta Mensagem...										�
				If _nTipo <> 2
					IncProc(STR0035 + Str(nQtd,10)) 		 //"Selecionando inf. para cart�o magnetico. "
				Else
					IncProc(STR0035 + AllTrim(Str(nQtd,10)) + "/" + AllTrim(Str(nTotal,10))) //"Selecionando inf. para cart�o magnetico. "
				EndIf

				//� Para qualquer exportacao ou emissao atualizar como modelo abaixo... �
				BA1->(DbGoTo(PLSEXP1->BA1REG))
				BA3->(DbGoTo(PLSEXP1->BA3REG))

				//� Verifica CPT                                                        �
				BF3->(dbSetOrder(1))
				BF3->(msSeek(xFilial("BF3")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG)))
				dDatCpt := ctod("  /  /  ")
				While ! BF3->(eof()) .and. BF3->(BF3_FILIAL+BF3_CODINT+BF3_CODEMP+BF3_MATRIC+BF3_TIPREG) == ;
						xFilial("BF3")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG)
					If  ! empty(BF3->BF3_CODDOE)
						If BF3->BF3_UNAGR == "3" //"Meses"
							If  Empty(BF3->BF3_DATCPT)
								If  Empty(BA1->BA1_DATCPT)
									nDias	:=	Abs(( date() -MonthSum( date() , 0 )))
									dDatCPt:= date()+nDias
								Else
									nDias	:=	Abs((BA1->BA1_DATCPT -MonthSum(BA1->BA1_DATCPT , BF3->BF3_MESAGR )))
									dDatCPt:= BA1->BA1_DATCPT+nDias
								Endif
							Else
								nDias	:=	Abs((BF3->BF3_DATCPT -MonthSum(BF3->BF3_DATCPT , BF3->BF3_MESAGR )))
								dDatCPt:= BF3->BF3_DATCPT+nDias
							Endif
						Else
							nDias   := PLSCarDias(BF3->BF3_MESAGR,BF3->BF3_UNAGR)
							dDatFim := BF3->BF3_DATCPT + nDias
							If  dDatFim > dDatCpt
								dDatCpt := dDatFim
							Endif
						Endif
					Endif
					BF3->(dbSkip())
				Enddo
			
				If BA3->BA3_TIPOUS <> "1"
					If  BT5->(BT5_FILIAL+BT5_CODINT+BT5_CODIGO+BT5_NUMCON+BT5_VERSAO) <> ;
							xFilial("BT5")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_CONEMP+BA1_VERCON)
						BT5->(DbSetOrder(1))
						BT5->(msSeek(xFilial("BT5")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_CONEMP+BA1_VERCON)))
						If ! BT5->(Found())
							Aadd(aCriticas, {BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG),STR0013}) //"Contrato n�o Cadastrado."
							PLSEXP1->(DbSkip())
							lOK := .F.
							Loop
						Endif
					Endif
				
					If  BQC->(BQC_FILIAL+BQC_CODIGO+BQC_NUMCON+BQC_VERCON+BQC_SUBCON+BQC_VERSUB) <> ;
							xFilial("BQC")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_CONEMP+BA1_VERCON+BA1_SUBCON+BA1_VERSUB)
						BQC->(DbSetOrder(1))
						BQC->(msSeek(xFilial("BQC")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_CONEMP+BA1_VERCON+BA1_SUBCON+BA1_VERSUB)))
						If ! BQC->(Found())
							Aadd(aCriticas, {BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG),STR0014}) //"Sub-Contrato n�o Cadastrado."
							PLSEXP1->(DbSkip())
							lOK := .F.
							Loop
						Endif
					Endif
				
					If  BQC->BQC_EMICAR == "0" // indica que nao deve gerar cartao para este subcontrato
						PLSEXP1->(DbSkip())
						Loop
					Endif
				Endif
			
				If  BG9->(BG9_FILIAL+BG9_CODINT+BG9_CODIGO) <> ;
						xFilial("BG9")+PLSEXP1->(BA3_CODINT+BA3_CODEMP)
					BG9->(DbSetOrder(1))
					BG9->(msSeek(xFilial("BG9")+PLSEXP1->(BA3_CODINT+BA3_CODEMP)))
				Endif

				//� Posiciona Produto do usuario... 									�
				If  ! empty(BA1->(BA1_CODPLA+BA1_VERSAO))
					If  BI3->(BI3_FILIAL+BI3_CODINT+BI3_CODIGO+BI3_VERSAO) <> ;
							xFilial("BI3")+BA1->(BA1_CODINT+BA1_CODPLA+BA1_VERSAO)
						BI3->(DbSetOrder(1))
						If  ! BI3->(msSeek(xFilial("BI3")+BA1->(BA1_CODINT+BA1_CODPLA+BA1_VERSAO)))
							Aadd(aCriticas, {BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG),STR0015}) //"Plano do usu�rio n�o cadastrado."
							PLSEXP1->(DbSkip())
							lOK := .F.
							Loop
						Endif
					Endif
				Else
					If  BI3->(BI3_FILIAL+BI3_CODINT+BI3_CODIGO+BI3_VERSAO) <> ;
							xFilial("BI3")+BA3->(BA3_CODINT+BA3_CODPLA+BA3_VERSAO)
						BI3->(DbSetOrder(1))
						If ! BI3->(msSeek(xFilial("BI3")+BA3->(BA3_CODINT+BA3_CODPLA+BA3_VERSAO)))
							Aadd(aCriticas, {BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG),STR0016})   //"Plano da fam�lia n�o cadastrado."
							PLSEXP1->(DbSkip())
							lOK := .F.
							Loop
						Endif
					Endif
				Endif


				//� Verifica se foi selecionado produto na tela de lote de carteirinha  �
				If  ! empty(cProduto)
					If  BI3->BI3_CODIGO <> cProduto
						PLSEXP1->(DbSkip())
						Loop
					Endif
				Endif

				nRegBTS := BTS->(Recno())
				BA1->(DbSetOrder(2))
				BA1->(DbSeek(xFilial()+BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+GetNewPar('MV_PLTRTIT','00'))))
				BTS->(DbSetOrder(1))
				BTS->(DbSeek(xFilial()+BA1->BA1_MATVID))
				cNomTit := BTS->BTS_NOMCAR
				cUniver := BTS->BTS_UNIVER
				BTS->(DbGoto(nRegBTS))
				BA1->(DbGoTo(PLSEXP1->BA1REG))

				//� Verifica se usuario esta bloqueado...					 			�
				If UsuBlq264(BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC),BA1->BA1_TIPREG,dDatabase, BA1->BA1_DATBLO, BA1->BA1_MOTBLO, BA1->BA1_CONSID)
					PLSEXP1->(DbSkip())
					Loop
				Endif

				aRetVlrCar := PLSEXPIVL(BA1->BA1_CODINT	,	BA1->BA1_CODEMP,	BA1->BA1_CONEMP,	BA1->BA1_VERCON,;
					BI3->BI3_CODIGO,	BI3->BI3_VERSAO,	BG9->BG9_TIPO,		BA1->BA1_VIACAR,;
					BA1->BA1_TIPUSU,	BA1->BA1_GRAUPA,	BA1->BA1_MATRIC,	BA1->BA1_TIPREG,;
					BA1->BA1_SUBCON,	BA1->BA1_VERSUB,	If(nUltVia = 0,GetNewPar("MV_PLSMP1V","4"),cMotivo))
			
				If lOK .and. ! aRetVlrCar[8]
					MostraErro()
					lOK := .F.
				Endif
			
				If !lReemissao
					If aRetVlrCar[8]
					
						//� Calcula Nova Data de Validade... 									�
						If cMudaVal == "1"
							If Empty(dDatVal)
								If BA3->BA3_TIPOUS == "1"
									nPerRen := nPeRePF
								Else
									nPerRen := BQC->BQC_NPERRN
								Endif
								dDtVali := BA1->BA1_DTVLCR
								While nPerRen > 12
									dDtVali := stod(strzero((val(substr(dtos(dDtVali),1,4))+1),4)+substr(dtos(dDtVali),5,4))
									nPerRen := nPerRen-12
								EndDo
								dDtVali := stod(substr(dtos(stod(substr(dtos(dDtVali),1,6)+"15")+(nPerRen*30)),1,6)+substr(dtos(dDtVali),7,2))
							Else
								dDtVali := dDatVal
							Endif
						Else
							dDtVali := BA1->BA1_DTVLCR
						Endif
						If BA1->BA1_TIPUSU <> cTipUsu
							If cMaior == "1" .And.;
									BDE->(FieldPos("BDE_MAIORI")) > 0 .And.;
									BDE->(FieldPos("BDE_GFILHO")) > 0 .And.;
									BDE->(FieldPos("BDE_GFILUN")) > 0 .And.;
									BDE->(FieldPos("BDE_GFILHA")) > 0 .And.;
									BDE->(FieldPos("BDE_GFILAU")) > 0 .And.;
									BDE->BDE_MAIORI == "1" .And.;
									BA1->BA1_GRAUPA $ (M->BDE_GFILHO,M->BDE_GFILUN,M->BDE_GFILHA,M->BDE_GFILAU)
							
								BT0->(DbSetOrder(1))
								BT1->(DbSetOrder(1))
								If BT0->(MsSeek(xFilial("BT0") + BA1->(BA1_CODINT + BA1_CODEMP + BA1_NUMCON + BA1_VERCON + BA1_SUBCON + BA1_VERSUB) + BI3->(BI3_CODIGO + BI3_VERSAO)))
									While BT0->(BT0_FILIAL + BT0_CODIGO + BT0_NUMCON + BT0_VERCON + BT0_SUBCON + BT0_VERSUB + BT0_CODPRO + BT0_VERSAO) ==;
											(xFilial("BT0") + BA1->(BA1_CODINT + BA1_CODEMP + BA1_NUMCON + BA1_VERCON + BA1_SUBCON + BA1_VERSUB)+BI3->(BI3_CODIGO + BI3_VERSAO))
										If BT0->BT0_GRAUPA == BA1->BA1_GRAUPA .And.;
												BT0->BT0_TIPUSR == BA1->BA1_TIPUSU .And.;
												BT0->BT0_ESTCIV == BA1->BA1_ESTCIV .And.;
												BT0->BT0_SEXO == BA1->BA1_SEXO
										
											_nIdMAXm := BT0->BT0_IDAMAX
										EndIf
										BT0->(DbSkip())
									EndDo
								ElseIf BT1->(MsSeek(xFilial("BT1")+ BI3->(BI3_CODINT + BI3_CODIGO + BI3_VERSAO)))
									While BT1->(BT1_FILIAL + BT1_CODIGO + BT1_VERSAO + BT1_TIPUSR) ==;
											(xFilial("BT1") + BI3->(BI3_CODINT + BI3_CODIGO + BI3_VERSAO) + BA1->(BA1_TIPUSU))
										If BT1->BT1_GRAUPA == BA1->BA1_GRAUPA .And.;
												BT1->BT1_TIPUSR == BA1->BA1_TIPUSU .And.;
												BT1->BT1_ESTCIV == BA1->BA1_ESTCIV .And.;
												BT1->BT1_SEXO == BA1->BA1_SEXO
										
											_nIdMAXm := BT1->BT1_IDAMAX
										EndIf
										BT1->(DbSkip())
									EndDo
								EndIf
								If Empty(_nIdMAXm)
									Do Case
									Case BA1->BA1_SEXO == "1" .And. cUniver == "1"
										//BA1->BA1_SEXO == 1 - Masculino // BTS->BTS_UNIVER == 1 - Sim
										_nIdMAXm := IIF(BDE->BDE_IMFOUN > 0, BDE->BDE_IMFOUN, cMvPLSFIOU)
									Case BA1->BA1_SEXO == "1" .And. cUniver == "0"
										//BA1->BA1_SEXO == 1 - Masculino // BTS->BTS_UNIVER == 0 - Nao
										_nIdMAXm := IIF(BDE->BDE_IMFILO > 0, BDE->BDE_IMFILO, cMvPLSFILO)
									Case BA1->BA1_SEXO == "2" .And. cUniver == "1"
										//BA1->BA1_SEXO == 2 - Feminino // BTS->BTS_UNIVER == 1 - Sim
										_nIdMAXm := IIF(BDE->BDE_IMFAUN > 0, BDE->BDE_IMFAUN, cMvPLSFIAU)
									Case BA1->BA1_SEXO == "2" .And. cUniver == "0"
										//BA1->BA1_SEXO == 2 - Feminino // BTS->BTS_UNIVER == 0 - Nao
										_nIdMAXm := IIF(BDE->BDE_IMFIHA > 0, BDE->BDE_IMFIHA, cMvPLSFILA)
									EndCase
								EndIf
								_nAno := Year(BA1->BA1_DATNAS) + _nIdMaxm
								_sMesDia := SUBSTR(DTOS(BA1->BA1_DATNAS),5,4)
								If _sMesDia  == '0229' .or. _sMesDia  == '0228'
									_sMesDia := '0301'
								Endif
								If _sMesDia  == '1231'
									_nAno  := _nAno +1
								Endif
								If (STOD(Str(_nAno,4) + _sMesDia ) + 1) < dDtVali
									dDtVali := STOD(Str(_nAno,4) + _sMesDia ) + 1
								EndIf
							EndIf
						EndIf

						//� Acumula quantidade de registros criados...					�
						nQtd ++
					Endif
				EndIf

				//� Valida via do Cartao...										�
				If BA1->BA1_VIACAR == 0 .And. aRetVlrCar[2] <> "2"  .and. !lImpressao
					Aadd(aCriticas, {BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG),STR0025}) //"Via do cart�o do usu�rio inv�lida."
					PLSEXP1->(DbSkip())
					lOK := .F.
					Loop
				Endif

				//� Alimenta Data de Validade e Via do Cartao...						�
				If lReemissao
					nViaCar := BED->BED_VIACAR
					dDtVali := BED->BED_DATVAL
				Else
					nViaCar := BA1->BA1_VIACAR
					dDtVali := BA1->BA1_DTVLCR
				Endif
				
				//� LayOut PLS... 						                                �
				cDataNasc := Eval({ || cDia := StrZero(Day(BA1->BA1_DATNAS),2), cMes := StrZero(Month(BA1->BA1_DATNAS),2), cAno := StrZero(Year(BA1->BA1_DATNAS),4), cDia+"/"+cMes+"/"+cAno })
				cDataInsc := Eval({ || cDia := StrZero(Day(BA1->BA1_DATINC),2), cMes := StrZero(Month(BA1->BA1_DATINC),2), cAno := StrZero(Year(BA1->BA1_DATINC),4), cDia+"/"+cMes+"/"+cAno })
				cDataValid:= Eval({ || cDia := StrZero(Day(dDtVali),2), cMes := StrZero(Month(dDtVali),2), cAno := StrZero(Year(dDtVali),4), cDia+"/"+cMes+"/"+cAno })
				cDatXInsc := Eval({ || cDia := StrZero(Day(BA1->BA1_DATINC),2), cMes := StrZero(Month(BA1->BA1_DATINC),2), cAno := StrZero(Year(BA1->BA1_DATINC),4), cDia+"/"+cMes+"/"+cAno })

				aRetNome:= PLSAVERNIV(	PLSEXP1->BA1_CODINT, PLSEXP1->BA1_CODEMP, PLSEXP1->BA1_MATRIC,	IF(PLSEXP1->BA3_TIPOUS=="1","F","J"),;
					PLSEXP1->BA3_CONEMP, PLSEXP1->BA3_VERCON, PLSEXP1->BA3_SUBCON, PLSEXP1->BA3_VERSUB, 1,;
					PLSEXP1->BA1_TIPREG)

				//� Verifica Regulamenta��o do Plano                                    �
				BI3->(dbSetOrder(1))
				BI3->(msSeek(xFilial("BF3")+BA3->(BA3_CODINT+BA3_CODPLA+BA3_VERSAO)))

				Do Case
				Case BI3->BI3_APOSRG == '0'
					cTipPlan := 'Plano Nao Regulamentado'
				Case BI3->BI3_APOSRG == '1'
					cTipPlan := 'Plano Regulamentado'
				Case BI3->BI3_APOSRG == '2'
					cTipPlan := 'Plano Adaptado'
				EndCase
			
						
				//� Verifica Prazo Maximo de carencia                                   �

				aVetCarenc := {}
				aRetCarenc := PLSCLACAR(BA1->BA1_CODINT,BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG+BA1_DIGITO),/*dDataBase*/)
		
				If Len(aRetCarenc) > 0 .And. aRetCarenc[1]
					dMaxCarenc := aRetCarenc[2][1][3]
					For nCont := 1 to len(aRetCarenc[2])
						If Len(aRetCarenc[2][nCont]) > 7
							If aRetCarenc[2][nCont][3] > dMaxCarenc
								dMaxCarenc := aRetCarenc[2][nCont][3]
							Endif
						Endif
					Next
				Endif
		
				//� Alimenta arquivo com os dados para arquivo texto...			�
				Dados->(RecLock("Dados",.T.))

				//� LayOut PLS... 						                                �
				Dados->TARJAMAG1 :=	PLSEXP1->BA1_CODINT+;
					PLSEXP1->BA1_CODEMP+;
					PLSEXP1->BA1_MATRIC+;
					PLSEXP1->BA1_TIPREG+;
					PLSEXP1->BA1_DIGITO+;
					"="+; //Campo separador
					Strzero(nViaCar,2)+;
					SubStr(cDataValid,9,2)+SubStr(cDataValid,4,2)+;
					"="+; //Campo separador
					PLSEXP1->BA1_OPERES+;
					BI3->BI3_IDECAR+; //Codigo do produto no Intercambio
					SubStr(BI3->BI3_ABRANG,Len(BI3->BI3_ABRANG),1)+;
					PLSEXP1->BA3_TIPCON
									
				if BDE->( FieldPos("BDE_NOMCAR") ) > 0 .And. TYPE("M->BDE_NOMCAR") <> "U"
					if M->BDE_NOMCAR == "1"
						Dados->TARJAMAG2 := PLSEXP1->BTS_NOMCAR
					else
						Dados->TARJAMAG2 := PLSEXP1->BTS_NOMUSR
					endIf
				else
					Dados->TARJAMAG2 := PLSEXP1->BTS_NOMCAR
				endIf
	
				Dados->MATRICULA := PLSEXP1->BA1_CODINT + ;
					PLSEXP1->BA1_CODEMP + ;
					PLSEXP1->BA1_MATRIC + ;
					PLSEXP1->BA1_TIPREG + ;
					PLSEXP1->BA1_DIGITO
				If  BA3->BA3_TIPOUS <> "1" .and. !Empty(BQC->BQC_NOMCAR)
					Dados->RZSOCIAL   := BQC->BQC_NOMCAR
				Else
					Dados->RZSOCIAL   := aRetNome[1][3]
				Endif           
				// Classes de Carencia
				BDL->(DbSetOrder(1))
				If BDL->( msSeek(xFilial("BDL")+PLSEXP1->BA1_CODINT+Alltrim(GetNewPar("MV_PLCLAMB","XXX"))) )
					If BDL->BDL_UNCAR == "1" //1=Horas;2=Dias;3=Meses;4=Anos
						cDesUnCar := "Horas"
					ElseIf BDL->BDL_UNCAR == "2"
						cDesUnCar := "Dias"
					ElseIf BDL->BDL_UNCAR == "3"
						cDesUnCar := "Meses"
					ElseIf BDL->BDL_UNCAR == "4"
						cDesUnCar := "Anos"
					Else
						cDesUnCar :=""
					Endif               
					Dados->CARENCAMB := Alltrim(Str(BDL->BDL_CARENC))+" "+cDesUnCar 
				ELse
					Dados->CARENCAMB := ""
				Endif
				If BDL->( msSeek(xFilial("BDL")+PLSEXP1->BA1_CODINT+Alltrim(GetNewPar("MV_PLCLHOS","XXX"))) ) 
					If BDL->BDL_UNCAR == "1" //1=Horas;2=Dias;3=Meses;4=Anos
						cDesUnCar := "Horas"
					ElseIf BDL->BDL_UNCAR == "2"
						cDesUnCar := "Dias"
					ElseIf BDL->BDL_UNCAR == "3"
						cDesUnCar := "Meses"
					ElseIf BDL->BDL_UNCAR == "4"
						cDesUnCar := "Anos"
					Else
						cDesUnCar :=""
					Endif               
					Dados->CARENCHOS := Alltrim(Str(BDL->BDL_CARENC))+" "+cDesUnCar 
				ELse
					Dados->CARENCHOS := ""
				Endif
				If BDL->( msSeek(xFilial("BDL")+PLSEXP1->BA1_CODINT+Alltrim(GetNewPar("MV_PLCLPAT","XXX"))) )  
					If BDL->BDL_UNCAR == "1" //1=Horas;2=Dias;3=Meses;4=Anos
						cDesUnCar := "Horas"
					ElseIf BDL->BDL_UNCAR == "2"
						cDesUnCar := "Dias"
					ElseIf BDL->BDL_UNCAR == "3"
						cDesUnCar := "Meses"
					ElseIf BDL->BDL_UNCAR == "4"
						cDesUnCar := "Anos"
					Else
						cDesUnCar :=""
					Endif               
					Dados->CARENCPAT := Alltrim(Str(BDL->BDL_CARENC))+" "+cDesUnCar  
				ELse
					Dados->CARENCPAT := ""
				Endif
				If BDL->( msSeek(xFilial("BDL")+PLSEXP1->BA1_CODINT+Alltrim(GetNewPar("MV_PLCLODO","XXX"))) )
					If BDL->BDL_UNCAR == "1" //1=Horas;2=Dias;3=Meses;4=Anos
						cDesUnCar := "Horas"
					ElseIf BDL->BDL_UNCAR == "2"
						cDesUnCar := "Dias"
					ElseIf BDL->BDL_UNCAR == "3"
						cDesUnCar := "Meses"
					ElseIf BDL->BDL_UNCAR == "4"
						cDesUnCar := "Anos"
					Else
						cDesUnCar :=""
					Endif               
					Dados->CARENCODO := Alltrim(Str(BDL->BDL_CARENC)) +" "+cDesUnCar  
				ELse
					Dados->CARENCODO := ""
				Endif
			
				Dados->INFORMACOE := STR0055 //"Informa��es"
				Dados->CNESUSU	 :=  AllTrim(PLSEXP1->BTS_NRCRNA)
				Dados->SEGASSPL  :=  padr(substr(POSICIONE("BI6",1,XFILIAL("BI6")+ BI3->BI3_CODSEG,"BI6_DESCRI"),1,56),56)
				
				BA0->(DbSetOrder(1))//BA0_FILIAL+BA0_CODIDE+BA0_CODINT
				BA0->( msSeek(xFilial("BA0")+PLSEXP1->BA1_CODINT) )
				
				Dados->NUMREGOPE  := BA0->BA0_SUSEP
				Dados->NFANTAZOPE := BA0->BA0_NOMINT
	
				/*BIM->(DbSetOrder(1))//BIM_FILIAL+BIM_CODINT+BIM_CODIGO
				BIM->( msSeek(xFilial("BIM")+PLSEXP1->BA1_CODINT) )
				
				while !BIM->(eof())
				
					if BIM->BIM_SETOR == GetNewPar("MV_PLBIMSE","012")
						Dados->CONTATOOPE  := allTrim(padR(BIM->BIM_NOME,30)) +" / "+ AllTrim(padR(BIM->BIM_TELCON,15)) +" / "+ AllTrim(padR(BIM->BIM_EMAIL,30))
						exit
					endIf
				
					BIM->(dbSkip())
				endDo
				*/
				
				BMV->(DbSetOrder(1))//BMV_FILIAL+BMV_CODIGO
				If BMV->( MsSeek(xFilial("BMV")+"STR0019") )
					Dados->CONTATOANS := AllTrim(BMV->BMV_MSGPOR) 
				Else
					Dados->CONTATOANS := ""
				Endif
			
				If BMV->( MsSeek(xFilial("BMV")+"STR0020") )
					Dados->CONTATOOPE := AllTrim(BMV->BMV_MSGPOR)
				Else
					Dados->CONTATOOPE := ""
				Endif
				
				Dados->CPT        := IIF(empty(dDatCpt),PadR('',12),substr(dtos(dDatCpt),7,2)+"/"+substr(dtos(dDatCpt),5,2)+"/"+substr(dtos(dDatCpt),1,4))
				
				BA0->(DbSetOrder(1))//BA0_FILIAL+BA0_CODIDE+BA0_CODINT
				BA0->( msSeek(xFilial("BA0")+PLSEXP1->BA1_CODINT) )
			
				If BA0->( Fieldpos("BA0_AUTGES") ) > 0 .And.  BA0->BA0_AUTGES == '0'
					cNumPlan := BI3->(BI3_CODINT+BI3_CODIGO+BI3_VERSAO)
				Else				
					cNumPlan := BI3->BI3_SUSEP
				EndIf
				Dados->NUMCON		:= cNumPlan
				
				cDTContrat := Eval({ || cDia := StrZero(Day(stod(PLSEXP1->BA1_DATINC)),2), cMes := StrZero(Month(stod(PLSEXP1->BA1_DATINC)),2), cAno := StrZero(Year(stod(PLSEXP1->BA1_DATINC)),4), cDia+"/"+cMes+"/"+cAno })
				Dados->DATCON		:= cDTContrat
				Dados->DTMAXCON	:= Eval({ || cDia := StrZero(Day(dMaxCarenc),2), cMes := StrZero(Month(dMaxCarenc),2), cAno := StrZero(Year(dMaxCarenc),4), cDia+"/"+cMes+"/"+cAno })
				Dados->INFOPLAN 	:= cTipPlan
				
				BG9->(DbSetOrder(1))//BG9_FILIAL+BG9_CODINT+BG9_CODIGO+BG9_TIPO
				BG9->( msSeek( xFilial("BG9")+PLSEXP1->(BA1_CODINT+BA1_CODEMP) ) )
	
				Dados->NFAADMBENE := BG9->BG9_NREDUZ
				Dados->DTNACTO    := cDataNasc
	
				If  BA3->BA3_TIPOUS == "1"
					Do Case
					Case BI3->BI3_NATJCO = "2"
						Dados->CONTRATACA := 'INDIVIDUAL OU FAMILIAR'
					Case BI3->BI3_NATJCO = "3"
						Dados->CONTRATACA := 'COLETIVO EMPRESARIAL'
					Case BI3->BI3_NATJCO = "4"
						Dados->CONTRATACA := 'COLETIVO POR ADESAO'
					Case BI3->BI3_NATJCO = "5"
						Dados->CONTRATACA := 'BENEFICIENTE'
					Otherwise
						Dados->CONTRATACA := "SEM CONTRATACAO"
					EndCase
				Else
					If  AllTrim(BQC->BQC_ENTFIL) == "1"
						Dados->CONTRATACA := 'BENEFICIENTE'
					Else
						BII->(DbSetOrder(1))
						If lTpCtrBI3 .AND. BII->( msSeek(xFilial("BII")+BI3->BI3_TIPCON) ) .AND. !EMPTY(BII->BII_TIPPLA)
							If  AllTrim(BII->BII_TIPPLA) $ "1,3"
								Dados->CONTRATACA := 'COLETIVO POR ADESAO'
							Else
								Dados->CONTRATACA := 'COLETIVO EMPRESARIAL'
							Endif
						Else
							If BII->( msSeek(xFilial("BII")+BT5->BT5_TIPCON) ) .AND. !EMPTY(BII->BII_TIPPLA)
								If  AllTrim(BII->BII_TIPPLA) $ "1,3"
									Dados->CONTRATACA := 'COLETIVO POR ADESAO'
								Else
									Dados->CONTRATACA := 'COLETIVO EMPRESARIAL'
								Endif
							Endif
						EndIf
					Endif
				Endif
					
				/*BI4->(dbSetOrder(1))
				If Empty(BI3->BI3_CODACO)
					Dados->TPACOMODA := PadR("" /*STR0061, 13)	// "NAO SE APLICA"
				/*Else
					If BI4->(MsSeek(xFilial("BI4")+AllTrim(BI3->(BI3_CODACO)))) .And.;
							BI4->(FieldPos("BI4_CODEDI")) > 0
												
						If Empty(BI4->BI4_CODEDI)
							Dados->TPACOMODA := PadR("" /*STR0061, 13)	// "NAO SE APLICA"
						/*Else
							If AllTrim(BI4->BI4_CODEDI) == "1"
								Dados->TPACOMODA := PadR(STR0062,13)	// INDIVIDUAL
							ElseIf AllTrim(BI4->BI4_CODEDI) == "2"
								Dados->TPACOMODA := PadR(STR0063,13)	// COLETIVA
							Else
								Dados->TPACOMODA := PadR("" /*STR0061, 13) 	// "NAO SE APLICA"
							/*EndIf
						EndIf
					Else
						Dados->TPACOMODA := PadR("" /*STR0061 13)	// "NAO SE APLICA"
					/*EndIf
				EndIf*/
	            
				BTQ->(dbSetOrder(1))
				BTQ->(MsSeek(xFilial("BTQ") + "49" + AllTrim(BI3->BI3_CODACO)))
			
				Dados->TPACOMODA := BTQ->BTQ_DESTER
			
				If BDE->( FieldPos("BDE_NOMCAR") ) > 0 .And. TYPE("M->BDE_NOMCAR") <> "U"
					If M->BDE_NOMCAR == "1"
						Dados->NOMEUSUARI := PadR(PLSEXP1->BTS_NOMCAR,25)
					Else
						Dados->NOMEUSUARI := PadR(PLSEXP1->BTS_NOMUSR,25)
					EndIf
				Else
					Dados->NOMEUSUARI := PLSEXP1->BTS_NOMCAR 
				EndIF
				
				Dados->ABRANG := Posicione("BF7",1,xFilial("BF7")+BI3->BI3_ABRANG,"BF7_DESORI")
				
				Dados->DTVIGPL 	:= cDatXInsc
				Dados->NOMPRO   := BI3->BI3_NREDUZ
				Dados->SUSEP 	:= Iif(BI3->BI3_APOSRG == "1",BI3->BI3_SUSEP,BI3->BI3_SCPA)
				
				Dados->(MsUnlock())
			EndIf
		EndIf
		//� Acessa proximo usuario     						            �
		PLSEXP1->(DbSkip())
	Enddo

	//� Tipo: Re-Imprime lote...									�
	If _nTipo <> 5
		PLSEXP1->(DbCloseArea())
	Else
		IncProc(STR0035) //"Selecionando inf. para cart�o magnetico."
	
		BED->(DbSetOrder(4))
		BED->(msSeek(xFilial("BED")+BDE->BDE_CODIGO))
	
		While xFilial("BED") == BED->BED_FILIAL .and. BDE->BDE_CODIGO == BED->BED_CDIDEN .and. ! BED->(Eof())
		
			lErro := .F.
		
			If BED->(FieldPos("BED_NOMCAR")) <> 0
				lNomCar := (BED->BED_NOMCAR == "1")
			EndIf
		
			If  BA3->(BA3_FILIAL+BA3_CODINT+BA3_CODEMP+BA3_MATRIC) <> ;
					xFilial("BA3")+BED->(BED_CODINT+BED_CODEMP+BED_MATRIC)
				BA3->(DbSetOrder(1))
				BA3->(msSeek(xFilial("BA3")+BED->BED_CODINT+BED->BED_CODEMP+BED->BED_MATRIC))
			Endif
		
			BA1->(DbSetOrder(2))
			BA1->(msSeek(xFilial("BA1")+BED->BED_CODINT+BED->BED_CODEMP+BED->BED_MATRIC+BED->BED_TIPREG+BED->BED_DIGITO))

			//� Verifica CPT                                                        �
			BF3->(dbSetOrder(1))
			BF3->(msSeek(xFilial("BF3")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG)))
			dDatCpt := ctod("  /  /  ")
			While ! BF3->(eof()) .and. BF3->(BF3_FILIAL+BF3_CODINT+BF3_CODEMP+BF3_MATRIC+BF3_TIPREG) == ;
					xFilial("BF3")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG)
				If  ! empty(BF3->BF3_CODDOE)
					If BF3->BF3_UNAGR == "3" //"Meses"
						If  Empty(BF3->BF3_DATCPT)
							If  Empty(BA1->BA1_DATCPT)
								nDias	:=	Abs(( date() -MonthSum( date() , 0 )))
								dDatCPt:= date()+nDias
							Else
								nDias	:=	Abs((BA1->BA1_DATCPT -MonthSum(BA1->BA1_DATCPT , BF3->BF3_MESAGR )))
								dDatCPt:= BA1->BA1_DATCPT+nDias
							Endif
						
						Else
							nDias	:=	Abs((BF3->BF3_DATCPT -MonthSum(BF3->BF3_DATCPT , BF3->BF3_MESAGR )))
							dDatCPt:= BF3->BF3_DATCPT+nDias
						Endif
					Else
						nDias   := PLSCarDias(BF3->BF3_MESAGR,BF3->BF3_UNAGR)
						dDatFim := BF3->BF3_DATCPT + nDias
						If  dDatFim > dDatCpt
							dDatCpt := dDatFim
						Endif
					Endif
				Endif
			
				BF3->(dbSkip())
			Enddo

			//� Verifica se usuario esta bloqueado...					 			�
			If UsuBlq264(BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC),BA1->BA1_TIPREG,dDatabase, BA1->BA1_DATBLO, BA1->BA1_MOTBLO, BA1->BA1_CONSID)
				lErro := .T.
			Endif

			//� Nao imprime Via com Titulo em aberto...						�
			If BED->BED_FATUR == "2" .and. ! lErro
				Aadd(aCriticas, {BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG),STR0042}) //"T�tulo da via de cart�o em aberto."
				lErro := .T.
			Endif
		
			If BA3->BA3_TIPOUS <> "1"
			
				If  BT5->(BT5_FILIAL+BT5_CODINT+BT5_CODIGO+BT5_NUMCON+BT5_VERSAO) <> ;
						xFilial("BT5")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_CONEMP+BA1_VERCON)
					BT5->(DbSetOrder(1))
					BT5->(msSeek(xFilial("BT5")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_CONEMP+BA1_VERCON)))
					If  ! BT5->(Found()) .and. ! lErro
						Aadd(aCriticas, {BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG),STR0013}) //"Contrato n�o Cadastrado."
						lErro := .T.
					Endif
				Endif
			
				If  BQC->(BQC_FILIAL+BQC_CODIGO+BQC_NUMCON+BQC_VERCON+BQC_SUBCON+BQC_VERSUB) <> ;
						xFilial("BQC")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_CONEMP+BA1_VERCON+BA1_SUBCON+BA1_VERSUB)
					BQC->(DbSetOrder(1))
					BQC->(msSeek(xFilial("BQC")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_CONEMP+BA1_VERCON+BA1_SUBCON+BA1_VERSUB)))
					If  ! BQC->(Found()) .and. ! lErro
						Aadd(aCriticas, {BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG),STR0014}) //"Sub-Contrato n�o Cadastrado."
						lErro := .T.
					Endif
				Endif
			
				If  BQC->BQC_EMICAR == "0"
					Aadd(aCriticas, {BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG),STR0025}) //"Via do cartao inv�lida. "
					lErro := .T.
				Endif
			Endif
		
			If  BG9->(BG9_FILIAL+BG9_CODINT+BG9_CODIGO) <> ;
					xFilial("BG9")+BA3->(BA3_CODINT+BA3_CODEMP)
				BG9->(DbSetOrder(1))
				BG9->(msSeek(xFilial("BG9")+BA3->(BA3_CODINT+BA3_CODEMP)))
			Endif
		
			nRegBTS := BTS->(Recno())
			nRegBA1 := BA1->(Recno())
			BA1->(DbSetOrder(2))
			BA1->(DbSeek(xFilial()+BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+GetNewPar('MV_PLTRTIT','00'))))
			BTS->(DbSetOrder(1))
			BTS->(DbSeek(xFilial()+BA1->BA1_MATVID))
			If BDE->( FieldPos("BDE_NOMCAR") ) > 0
				If BDE->BDE_NOMCAR == "1" .Or. lNomCar
					cNomTit := BTS->BTS_NOMCAR
				Else
					cNomTit := BTS->BTS_NOMUSR
				EndIf
			Else
				cNomTit := BTS->BTS_NOMCAR
			EndIf
			BTS->(DbGoto(nRegBTS))
			BA1->(DbGoTo(nRegBA1))
		
			BTS->(DbSetOrder(1))
			BTS->(msSeek(xFilial("BTS")+BA1->BA1_MATVID))

			//� Posiciona Produto do usuario... 									�
			If  ! empty(BA1->(BA1_CODPLA+BA1_VERSAO))
				If  BI3->(BI3_FILIAL+BI3_CODINT+BI3_CODIGO+BI3_VERSAO) <> ;
						xFilial("BI3")+BA1->(BA1_CODINT+BA1_CODPLA+BA1_VERSAO)
					BI3->(DbSetOrder(1))
					If ! BI3->(msSeek(xFilial("BI3")+BA1->(BA1_CODINT+BA1_CODPLA+BA1_VERSAO)))
						Aadd(aCriticas, {BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG),STR0039}) //"Plano do usu�rio n�o cadastrado."
						lErro := .T.
					Endif
				Endif
			Else
				If  BI3->(BI3_FILIAL+BI3_CODINT+BI3_CODIGO+BI3_VERSAO) <> ;
						xFilial("BI3")+BA3->(BA3_CODINT+BA3_CODPLA+BA3_VERSAO)
					If ! BI3->(msSeek(xFilial("BI3")+BA3->(BA3_CODINT+BA3_CODPLA+BA3_VERSAO)))
						Aadd(aCriticas, {BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG),STR0040}) //"Plano da fam�lia n�o cadastrado."
						lErro := .T.
					Endif
				Endif
			Endif

			//� Verifica se a via do cartao esta bloqueada...						�
			If (BED->( FieldPos("BED_CODBLO") ) = 0) .or. ! lErro .and. (BED->( FieldPos("BED_BLOIDE") ) = 0)
				//If ExistBlock("PLS264E2")
				Aadd(aCriticas, {'',STR0057}) //"Dicion�rio de dados desatualizado."
				lErro := .T.
			Else
				If BED->BED_BLOIDE == "1" .and. ! lErro
					Aadd(aCriticas, {BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG),STR0024}) //"Via de cart�o bloqueada. "
					lErro := .T.
				Endif
			Endif

			aRetNome := PLSAVERNIV(BA1->BA1_CODINT,	BA1->BA1_CODEMP,BA1->BA1_MATRIC,IIF(BA3->BA3_TIPOUS=="1","F","J"),	BA3->BA3_CONEMP,BA3->BA3_VERCON,BA3->BA3_SUBCON,BA3->BA3_VERSUB,1,BA1->BA1_TIPREG)

			nQtd++
		
			Dados->(RecLock("Dados",.T.))
				Dados->TARJAMAG1 :=	BA1->BA1_CODINT+;
				BA1->BA1_CODEMP+;
				BA1->BA1_MATRIC+;
				BA1->BA1_TIPREG+;
				BA1->BA1_DIGITO+;
				"="+; //Campo separador
				Strzero(BED->BED_VIACAR,2)+;
				SubStr(cDataValid,9,2)+SubStr(cDataValid,4,2)+;
				"="+; //Campo separador
				BA1->BA1_OPERES+;
				BI3->BI3_IDECAR+; //Codigo do produto no Intercambio
				SubStr(BI3->BI3_ABRANG,Len(BI3->BI3_ABRANG),1)+;
				BA3->BA3_TIPCON

			if BDE->( FieldPos("BDE_NOMCAR") ) > 0 .And. TYPE("M->BDE_NOMCAR") <> "U"
				if M->BDE_NOMCAR == "1"
					Dados->TARJAMAG2 := BTS->BTS_NOMCAR
				else
					Dados->TARJAMAG2 := BTS->BTS_NOMUSR
				endIf
			else
				Dados->TARJAMAG2 := BTS->BTS_NOMCAR
			endIf
	
			Dados->MATRICULA := BA1->BA1_CODINT + ;
				BA1->BA1_CODEMP + ;
				BA1->BA1_MATRIC + ;
				BA1->BA1_TIPREG + ;
				BA1->BA1_DIGITO
	
			If  BA3->BA3_TIPOUS <> "1" .and. !Empty(BQC->BQC_NOMCAR)
				Dados->RZSOCIAL   := BQC->BQC_NOMCAR
			Else
				Dados->RZSOCIAL   := aRetNome[1][3]
			Endif
			// Classes de Carencia
			BDL->(DbSetOrder(1))
			If BDL->( msSeek(xFilial("BDL")+BA1->BA1_CODINT+Alltrim(GetNewPar("MV_PLCLAMB","XXX"))) )   
				If BDL->BDL_UNCAR == "1" //1=Horas;2=Dias;3=Meses;4=Anos
					cDesUnCar := "Horas"
				ElseIf BDL->BDL_UNCAR == "2"
					cDesUnCar := "Dias"
				ElseIf BDL->BDL_UNCAR == "3"
					cDesUnCar := "Meses"
				ElseIf BDL->BDL_UNCAR == "4"
					cDesUnCar := "Anos"
				Else
					cDesUnCar :=""
				Endif               
				Dados->CARENCAMB := Alltrim(Str(BDL->BDL_CARENC))+" "+cDesUnCar 
			ELse
				Dados->CARENCAMB := ""
			Endif
			If BDL->( msSeek(xFilial("BDL")+BA1->BA1_CODINT+Alltrim(GetNewPar("MV_PLCLHOS","XXX"))) )
				If BDL->BDL_UNCAR == "1" //1=Horas;2=Dias;3=Meses;4=Anos
					cDesUnCar := "Horas"
				ElseIf BDL->BDL_UNCAR == "2"
					cDesUnCar := "Dias"
				ElseIf BDL->BDL_UNCAR == "3"
					cDesUnCar := "Meses"
				ElseIf BDL->BDL_UNCAR == "4"
					cDesUnCar := "Anos"
				Else
					cDesUnCar :=""
				Endif               
				Dados->CARENCHOS := Alltrim(Str(BDL->BDL_CARENC))+" "+cDesUnCar  
			ELse
				Dados->CARENCHOS := ""
			Endif
			If BDL->( msSeek(xFilial("BDL")+BA1->BA1_CODINT+Alltrim(GetNewPar("MV_PLCLPAT","XXX"))) )
				If BDL->BDL_UNCAR == "1" //1=Horas;2=Dias;3=Meses;4=Anos
					cDesUnCar := "Horas"
				ElseIf BDL->BDL_UNCAR == "2"
					cDesUnCar := "Dias"
				ElseIf BDL->BDL_UNCAR == "3"
					cDesUnCar := "Meses"
				ElseIf BDL->BDL_UNCAR == "4"
					cDesUnCar := "Anos"
				Else
					cDesUnCar :=""
				Endif               
				Dados->CARENCPAT := Alltrim(Str(BDL->BDL_CARENC))+" "+cDesUnCar   
			ELse
				Dados->CARENCPAT := ""
			Endif
			If BDL->( msSeek(xFilial("BDL")+BA1->BA1_CODINT+Alltrim(GetNewPar("MV_PLCLODO","XXX"))) ) 
				If BDL->BDL_UNCAR == "1" //1=Horas;2=Dias;3=Meses;4=Anos
					cDesUnCar := "Horas"
				ElseIf BDL->BDL_UNCAR == "2"
					cDesUnCar := "Dias"
				ElseIf BDL->BDL_UNCAR == "3"
					cDesUnCar := "Meses"
				ElseIf BDL->BDL_UNCAR == "4"
					cDesUnCar := "Anos"
				Else
					cDesUnCar :=""
				Endif               
				Dados->CARENCODO := Alltrim(Str(BDL->BDL_CARENC))+" "+cDesUnCar 
			ELse
				Dados->CARENCODO := ""
			Endif
			
			Dados->INFORMACOE := STR0055 //"Informa��es"
			Dados->CNESUSU	 :=  AllTrim(BTS->BTS_NRCRNA)
			Dados->SEGASSPL  :=  padr(substr(POSICIONE("BI6",1,XFILIAL("BI6")+ BI3->BI3_CODSEG,"BI6_DESCRI"),1,56),56)
				
			BA0->(DbSetOrder(1))//BA0_FILIAL+BA0_CODIDE+BA0_CODINT
			BA0->( msSeek(xFilial("BA0")+BA1->BA1_CODINT) )
				
			Dados->NUMREGOPE  := BA0->BA0_SUSEP
			Dados->NFANTAZOPE := BA0->BA0_NOMINT
					
			/*BIM->(DbSetOrder(1))//BIM_FILIAL+BIM_CODINT+BIM_CODIGO
			BIM->( msSeek(xFilial("BIM")+BA1->BA1_CODINT) )
				
			while !BIM->(eof())
				if BIM->BIM_SETOR == GetNewPar("MV_PLBIMSE","012")
					Dados->CONTATOOPE  := allTrim(padR(BIM->BIM_NOME,30)) +" / "+ AllTrim(padR(BIM->BIM_TELCON,15)) +" / "+ AllTrim(padR(BIM->BIM_EMAIL,30))
					exit
				endIf
				
				BIM->(dbSkip())
			endDo
			*/
				
			BMV->(DbSetOrder(1))//BMV_FILIAL+BMV_CODIGO
			If BMV->( MsSeek(xFilial("BMV")+"STR0019") )
				Dados->CONTATOANS := AllTrim(BMV->BMV_MSGPOR) 
			Else
				Dados->CONTATOANS := ""
			Endif
			
			If BMV->( MsSeek(xFilial("BMV")+"STR0020") )
				Dados->CONTATOOPE := AllTrim(BMV->BMV_MSGPOR)
			Else
				Dados->CONTATOOPE := ""
			Endif	
			
			Dados->CPT        := IIF(empty(dDatCpt),PadR('',12),substr(dtos(dDatCpt),7,2)+"/"+substr(dtos(dDatCpt),5,2)+"/"+substr(dtos(dDatCpt),1,4))
	
			BG9->(DbSetOrder(1))//BG9_FILIAL+BG9_CODINT+BG9_CODIGO+BG9_TIPO
			BG9->( msSeek( xFilial("BG9")+BA1->(BA1_CODINT+BA1_CODEMP) ) )
	
			Dados->NFAADMBENE := BG9->BG9_NREDUZ
			Dados->DTNACTO    := cDataNasc
	
			If  BA3->BA3_TIPOUS == "1"
				Do Case
				Case BI3->BI3_NATJCO = "2"
					Dados->CONTRATACA := 'INDIVIDUAL OU FAMILIAR'
				Case BI3->BI3_NATJCO = "3"
					Dados->CONTRATACA := 'COLETIVO EMPRESARIAL'
				Case BI3->BI3_NATJCO = "4"
					Dados->CONTRATACA := 'COLETIVO POR ADESAO'
				Case BI3->BI3_NATJCO = "5"
					Dados->CONTRATACA := 'BENEFICIENTE'
				Otherwise
					Dados->CONTRATACA := "SEM CONTRATACAO"
				EndCase
			Else
				If  AllTrim(BQC->BQC_ENTFIL) == "1"
					Dados->CONTRATACA := 'BENEFICIENTE'
				Else
					BII->(DbSetOrder(1))
					If lTpCtrBI3 .AND. BII->( msSeek(xFilial("BII")+BI3->BI3_TIPCON) ) .AND. !EMPTY(BII->BII_TIPPLA)
						If  AllTrim(BII->BII_TIPPLA) $ "1,3"
							Dados->CONTRATACA := 'COLETIVO POR ADESAO'
						Else
							Dados->CONTRATACA := 'COLETIVO EMPRESARIAL'
						Endif
					Else
						If BII->( msSeek(xFilial("BII")+BT5->BT5_TIPCON) ) .AND. !EMPTY(BII->BII_TIPPLA)
							If  AllTrim(BII->BII_TIPPLA) $ "1,3"
								Dados->CONTRATACA := 'COLETIVO POR ADESAO'
							Else
								Dados->CONTRATACA := 'COLETIVO EMPRESARIAL'
							Endif
						Endif
					EndIf
				Endif
			Endif
					
			/*BI4->(dbSetOrder(1))
			If Empty(BI3->BI3_CODACO)
				Dados->TPACOMODA := PadR("" /*STR0061, 13)	// "NAO SE APLICA"
			/*Else
				If BI4->(MsSeek(xFilial("BI4")+AllTrim(BI3->(BI3_CODACO)))) .And.;
						BI4->(FieldPos("BI4_CODEDI")) > 0
												
					If Empty(BI4->BI4_CODEDI)
						Dados->TPACOMODA := PadR("" /*STR0061, 13)	// "NAO SE APLICA"
					/*Else
						If AllTrim(BI4->BI4_CODEDI) == "1"
							Dados->TPACOMODA := PadR(STR0062,13)	// INDIVIDUAL
						ElseIf AllTrim(BI4->BI4_CODEDI) == "2"
							Dados->TPACOMODA := PadR(STR0063,13)	// COLETIVA
						Else
							Dados->TPACOMODA := PadR("" /*STR0061, 13) 	// "NAO SE APLICA"
						/*EndIf
					EndIf
				Else
					Dados->TPACOMODA := PadR("" /*STR0061, 13)	// "NAO SE APLICA"
				/*EndIf
			EndIf*/
	        
			BTQ->(dbSetOrder(1))
			BTQ->(MsSeek(xFilial("BTQ") + "49" + AllTrim(BI3->BI3_CODACO)))
			
			Dados->TPACOMODA := BTQ->BTQ_DESTER
			
			If BDE->( FieldPos("BDE_NOMCAR") ) > 0 .And. TYPE("M->BDE_NOMCAR") <> "U"
				If M->BDE_NOMCAR == "1"
					Dados->NOMEUSUARI := PadR(BTS->BTS_NOMCAR,25)
				Else
					Dados->NOMEUSUARI := PadR(BTS->BTS_NOMUSR,25)
				EndIf
			Else
				Dados->NOMEUSUARI := BTS->BTS_NOMCAR 
			EndIF
				
			Dados->ABRANG := Posicione("BF7",1,xFilial("BF7")+BI3->BI3_ABRANG,"BF7_DESORI")
			
			Dados->DTVIGPL 	:= cDatXInsc
			Dados->NOMPRO   := BI3->BI3_NREDUZ
			Dados->SUSEP 	:= Iif(BI3->BI3_APOSRG == "1",BI3->BI3_SUSEP,BI3->BI3_SCPA)
			Dados->( MsUnlock() )
		
			If lNewLot .and. !lImpressao
				BA1->(RecLock("BA1",.F.))
				BA1->BA1_CDIDEN := BDE->BDE_CODIGO
				BA1->(MsUnlock())
			Endif
		
			BED->( DbSkip() )
		EndDo
	EndIf

	dbSelectArea( "Dados" )
	DADOS->( dbGoTop() )

	aadd(aDados, {	DADOS->NOMEUSUARI,;
		iif(FindFunction("PLSMATBEN") ,PLSMATBEN(DADOS->MATRICULA) ,DADOS->MATRICULA) ,; 
		iif(empty(DADOS->DTNACTO),'',DADOS->DTNACTO),;
		iif(empty(DADOS->CNESUSU),'',DADOS->CNESUSU),;
		iif(empty(DADOS->SUSEP),'',DADOS->SUSEP),;
		iif(empty(DADOS->SEGASSPL),'',DADOS->SEGASSPL),;
		iif(empty(DADOS->NUMREGOPE),'',DADOS->NUMREGOPE),;
		iif(empty(DADOS->CONTATOOPE),'',DADOS->CONTATOOPE),;
		iif(empty(DADOS->CONTATOANS),'',DADOS->CONTATOANS),;
		iif(empty(DADOS->CPT),'',DADOS->CPT),;
		iif(empty(DADOS->TPACOMODA),'',DADOS->TPACOMODA),;
		iif(empty(DADOS->CONTRATACA),'',DADOS->CONTRATACA),;
		iif(empty(DADOS->ABRANG),'',DADOS->ABRANG),;
		iif(empty(DADOS->NOMPRO),'',DADOS->NOMPRO),;
		iif(empty(DADOS->NFANTAZOPE),'',DADOS->NFANTAZOPE),;
		iif(empty(DADOS->NFAADMBENE),'',DADOS->NFAADMBENE),;
		iif(empty(DADOS->RZSOCIAL),'',DADOS->RZSOCIAL),;
		iif(empty(DADOS->DTVIGPL),'',DADOS->DTVIGPL),;
		iif(empty(DADOS->NUMCON),'',DADOS->NUMCON),;
		iif(empty(DADOS->DATCON),'',DADOS->DATCON),;
		iif(empty(DADOS->DTMAXCON),'',DADOS->DTMAXCON),;
		iif(empty(DADOS->INFOPLAN),'',DADOS->INFOPLAN),;
		iif(empty(DADOS->INFORMACOE),'',DADOS->INFORMACOE ),; 
		iif(empty(DADOS->CARENCAMB),'',DADOS->CARENCAMB ),; 
		iif(empty(DADOS->CARENCHOS),'',DADOS->CARENCHOS ),; 
		iif(empty(DADOS->CARENCPAT),'',DADOS->CARENCPAT ),; 
		iif(empty(DADOS->CARENCODO),'',DADOS->CARENCODO ) } )
		
	oTempTable:Delete()
Return( aDados )