
#include "PLSMGER.CH"
#include "PROTHEUS.CH"
#include "COLORS.CH"

#define K_Atend     6
#define K_Cancelar  5
#define K_Legenda   7
#define K_Parametro 8
#define _nBytes 20

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    � PLSA309 � Autor � Tulio Cesar            � Data � 09.09.02 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Cadastro de Procedimentos                                  ����
�������������������������������������������������������������������������Ĵ���
���Sintaxe   � PLSA309()                                                  ����
�������������������������������������������������������������������������Ĵ���
��� Uso      � Agenda Medica                                              ����
�������������������������������������������������������������������������Ĵ���
��� Alteracoes desde sua construcao inicial.                              ����
�������������������������������������������������������������������������Ĵ���
��� Data     � BOPS � Programador � Breve Descricao                       ����
�������������������������������������������������������������������������Ĵ���
��������������������������������������������������������������������������ٱ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/
Function PLSA309
//��������������������������������������������������������������������������Ŀ
//� Definicao de variaveis...                                                �
//����������������������������������������������������������������������������
LOCAL cAlias     := "BBD"                                                    
LOCAL oBrw
LOCAL bAjustBrw  := {|| oBrw := GetObjBrow(), PLSA309Tel(dData,oBrw,cCodInt,cLocal,cCdAmbu,cDsAmbu) }
LOCAL cPerg      := "PLA305"
LOCAL cCodLoja
LOCAL cLocal
LOCAL cCodInt
LOCAL cDsambu
LOCAL cSQL
LOCAL oSay
LOCAL _cFil305                                                                
LOCAL nRcBAU
//��������������������������������������������������������������������������Ŀ
//� Monta opcoes da rotina...                                                �
//����������������������������������������������������������������������������
PRIVATE cSala
PRIVATE cCdAmbu
PRIVATE cCodSala
PRIVATE dData     := MsDate()
PRIVATE aCdCores  := { { 'BR_AMARELO' ,'Encaixe'  },;
                        { 'BR_CINZA'   ,'Agendado'  },;
                        { 'BR_AZUL'    ,'Atendido'  },;
                        { 'BR_VERDE'   ,'Espera'    } }

PRIVATE aCores    := { { 'BBD_STATUS = "1" .And. BBD_ENCAIX = "1"',aCdCores[1,1] },;
                        { 'BBD_STATUS = "1"',aCdCores[2,1] },;
                        { 'BBD_STATUS = "4"',aCdCores[3,1] },;
                        { 'BBD_STATUS = "5"',aCdCores[4,1] } }

PRIVATE aRotina   := MenuDef()

//��������������������������������������������������������������������������Ŀ
//� Define o cabecalho da tela de atualizacoes                               �
//����������������������������������������������������������������������������
PRIVATE cCadastro := "Lancamento de Procedimento"
//��������������������������������������������������������������������������Ŀ
//� Busca parametros da rotina...                                            �
//����������������������������������������������������������������������������
DbSelectArea("SX1")

If ! Pergunte(cPerg,.T.)
   Return
Endif   

cCodInt  := mv_par01
cLocal   := mv_par02     
cCodMed  := Retcodusr()
//��������������������������������������������������������������������������Ŀ
//� Busca o medico logado...                                                 �
//����������������������������������������������������������������������������
cSQL := "SELECT BAU_CODIGO, R_E_C_N_O_ REG FROM "+RetSQLName("BAU")+" WHERE "
cSQL += "D_E_L_E_T_ = '' AND "
cSQL += "BAU_CODCFG = '"+cCodMed+"'"
PLSQuery(cSQL,"Trb309")

If ! Trb309->(Eof())
   cCodLoja := Trb309->BAU_CODIGO
   nRcBAU   := Trb309->REG
Else
   MsgInfo("O Vinculo Rede de Atendimento X Usuario Configurador deve ser feito antes de entrar nesta opcao")
   Trb309->(DbCloseArea())
   Return
Endif                     
Trb309->(DbCloseArea())
//��������������������������������������������������������������������������Ŀ
//� Posiciona no medico...                                                   �
//����������������������������������������������������������������������������
BAU->(DbSetOrder(1))
BAU->(DbGoTo(nRcBAU))

PLSQuery(cSQL,"Trb309")

//��������������������������������������������������������������������������Ŀ
//� Monta filtro padrao...                                                   �
//����������������������������������������������������������������������������
_cFil305 := "BBD_FILIAL = '"+xFilial("BBD")+"' .And. "
_cFil305 += "BBD_CODIGO = '"+Subs(cCodLoja,1,6)+"' .And. "
_cFil305 += "dtos(BBD_DATA) = '"+dtos(dData)+"' .And. "
_cFil305 += "BBD_LOCAL = '"+Subs(cLocal,1,3)+"' "

BBD->(DbSetFilter({||&_cFil305},_cFil305))
//��������������������������������������������������������������������������Ŀ
//� Chama mBrowse padrao...                                                  �
//����������������������������������������������������������������������������
BBD->(DbSetOrder(1))
BBD->(DbSeek(xFilial(cAlias)))

BBD->(mBrowse(006,001,022,075,cAlias, , , , , K_Atend, aCores, , , ,bAjustBrw))
//��������������������������������������������������������������������������Ŀ
//� Libera filtro antes de sair da rotina...                                 �
//����������������������������������������������������������������������������
Trb309->(DbCloseArea())
BBD->(DbClearFilter())

//��������������������������������������������������������������������������Ŀ
//� Fim da Rotina Principal...                                               �
//����������������������������������������������������������������������������
Return
/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    � PLSA309MOV � Autor � Tulio Cesar         � Data � 08.01.02 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Movimentacao do atendimento de um medico a uma consulta    ����
�������������������������������������������������������������������������Ĵ���
���Sintaxe   � PLSA309MOV(cAlias,nReg,nOpc)                               ����
�������������������������������������������������������������������������Ĵ���
��� Uso      � PLSA309                                                    ����
�������������������������������������������������������������������������Ĵ���
��� Alteracoes desde sua construcao inicial.                              ����
�������������������������������������������������������������������������Ĵ���
��� Data     � BOPS � Programador � Breve Descricao                       ����
�������������������������������������������������������������������������Ĵ���
��������������������������������������������������������������������������ٱ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/
Function PLSA309MOV(cAlias,nReg,nOpc, lMode, dLocData)
Local I__f := 0
//��������������������������������������������������������������������������Ŀ
//� Define variaveis locais...                                               �
//����������������������������������������������������������������������������
LOCAL oDlg
LOCAL oFolder
LOCAL oEnc01
LOCAL oEnc02
LOCAL cAliasEnc := "BTT"
LOCAL nRegEnc   := 0          
LOCAL oSay                         
LOCAL nOpca   := 0                                           
LOCAL cHorIni := ""
LOCAL bOK     := { || nOpca := 1,If((Obrigatorio(aGets,aTela)),oDlg:End(),(nOpca:=3,.F.))}
LOCAL bCanc   := {|| If(MsgYesNo(__cMsgAban),(nOpca := 0, oDlg:End()),.F.) }
LOCAL cPrefFil:= "PLSA315"          
LOCAL cFileI

LOCAL nHMsg


LOCAL aCabBTW := {}
LOCAL aDadBTW := {}
LOCAL aTrbBTW := {}
LOCAL aCpo01  := {}
LOCAL aCpo02  := {}
LOCAL nOrdBTT := BTT->(IndexOrd())
LOCAL nRecBTT := BTT->(Recno())


LOCAL aCabBPJ := {}
LOCAL aDadBPJ := {}
LOCAL aTrbBPJ := {}

PRIVATE cFileO

PRIVATE oBrwEvo
PRIVATE aCabEvo := {}
PRIVATE aDadEvo := {}
PRIVATE lEvol   := .F.

PRIVATE nOpcUpt := nOpc

PRIVATE oBrwBTW
PRIVATE oBrwBPJ 

PRIVATE aTela   := {}
PRIVATE aGets   := {}
PRIVATE aCols   := {}
PRIVATE aHeader := {}                                                        
PRIVATE aButtons:= {}

PRIVATE oMemo01
PRIVATE cMemo01 := ""

PRIVATE oMemo02
PRIVATE cMemo02 := ""

PRIVATE oMemo03
PRIVATE cMemo03 := ""

PRIVATE oMemo04
PRIVATE cMemo04 := ""

PRIVATE oMemo05
PRIVATE cMemo05 := ""

PRIVATE oMemo06
PRIVATE cMemo06 := ""

PRIVATE oMemo07
PRIVATE cMemo07 := ""

PRIVATE oMemo08
PRIVATE cMemo08 := ""

PRIVATE aRotina   := { { STRPL01   , "AxPesqui"   , 0 , K_Pesquisar  },;
                        { STRPL02  , "PLSA309MOV" , 0 , K_Visualizar },;
                        { STRPL04     , "PLSA309MOV" , 0 , K_Alterar    },;
                        { "Cancelar"    , "PLSA309MOV" , 0 , K_Cancelar   },;
                        { "Lancar Proc" , "PLSA309MOV" , 0 , K_Atend      },;
                        { "xxxxxxxxx"   , "PLSA309MOV" , 0 , 6 } }

If Valtype(lMode) # "L"
	lMode := .T.
Endif

PRIVATE cCodPac := Iif( lMode, BBD->BBD_CODPAC, M->BBD_CODPAC )

//��������������������������������������������������������������������������Ŀ
//�                                                                          �
//����������������������������������������������������������������������������
If nOpc == K_Alterar .And. BBD->BBD_STATUS <> "4"
   MsgInfo("Nao e possivel lancar p/ um paciente em espera...")
   Return
Endif   

If nOpc <> K_Visualizar .And. nOpc <> K_Excluir
   nOpc := K_Atend
Endif   

//��������������������������������������������������������������������������Ŀ
//� testa se existem registros no Browse...                                  �
//����������������������������������������������������������������������������
If lMode
	If BBD->(Eof())
		Return
	Endif
Endif

//��������������������������������������������������������������������������Ŀ
//� testa se pode alterar...                                                 �
//����������������������������������������������������������������������������
If nOpc <> K_Atend .And. BBD->BBD_STATUS <> "4" .And. nOpc <> K_Visualizar
   MsgInfo("Nao e possivel lancar  ou visualizar sem realizar o atendimento")
   Return
Endif              
//��������������������������������������������������������������������������Ŀ
//� Monta aButtons se for atendimento...                                     �
//����������������������������������������������������������������������������
If nOpc == K_Atend .Or. nOpc == K_Alterar
   aadd(aButtons,{"POSCLI",{ || PLSA309Grv(nOpc,M->BTT_CODPAC)},"Gravar Dados"})
Endif   
//��������������������������������������������������������������������������Ŀ
//� Posiciona no usuario...                                                  �
//����������������������������������������������������������������������������
BA1->(DbSetOrder(2))
BA1->(DbSeek(xFilial("BA1")+Iif(lMode,BBD->BBD_CODPAC, M->BBD_CODPAC)))  

//��������������������������������������������������������������������������Ŀ
//� Se a rotina for chamada pelo botao, eu tenho que posicionar o BTT.       �
//����������������������������������������������������������������������������	
BTT->( dbSetorder(01) )
BTT->( dbSeek( xFilial("BTT")+Iif(lMode, BBD->BBD_CODPAC,M->BBD_CODPAC) ) )

//��������������������������������������������������������������������������Ŀ
//� Monta dados da enchoice...                                               �
//����������������������������������������������������������������������������	
If nOpc == K_Atend .And. nOpcUpt <> K_Alterar
   Copy cAliasEnc To Memory Blank
   //��������������������������������������������������������������������������Ŀ
   //� Monta dados...                                                           �
   //����������������������������������������������������������������������������
   M->BTT_DATA   := Iif( lMode, dData, dLocData)
   M->BTT_CODPAC := Iif( lMode, BBD->BBD_CODPAC, M->BBD_CODPAC )
   M->BTT_NOMPAC := Iif( lMode, BBD->BBD_NOME  , M->BBD_NOME   )
Else                                                           
   //��������������������������������������������������������������������������Ŀ
   //� Posiciona no atendimento da consulta posicionada...                      �
   //����������������������������������������������������������������������������
   BTT->(DbSetOrder(1))
   BTT->(DbSeek(xFilial("BTT")+Iif( lMode, BBD->(BBD_CODPAC), M->BBD_CODPAC) ))

   Copy cAliasEnc To Memory
   nRegEnc := BTT->(Recno())
Endif
//���������������������������������������������������������������������Ŀ
//� Monta dados das Solicitacoes...                                     �
//�����������������������������������������������������������������������
Store Header "BTW" TO aCabBTW For .T.
If nOpc == K_Incluir
   Store COLS Blank "BTW" TO aDadBTW FROM aCabBTW
Else
   BTW->(DbSetOrder(1))
   If ! BTW->(DbSeek(xFilial("BTW")+BTT->BTT_CODPAC ))
      Store COLS Blank "BTW" TO aDadBTW FROM aCabBTW
   Else
      Store COLS "BTW" TO aDadBTW FROM aCabBTW VETTRAB aTrbBTW While BTW->(BTW_FILIAL+BTW_CODPAC) == xFilial("BTW")+BTT->BTT_CODPAC
   Endif
Endif                     
If lMode
	//��������������������������������������������������������������������������Ŀ
	//� Posiciona no proximo horario...                                          �
	//����������������������������������������������������������������������������
	If nOpc == K_Atend .And. BBD->BBD_STATUS == "4" .And. nOpcUpt <> K_Alterar
		BBD->(DbSkip())
		Return
	Endif
Endif

//��������������������������������������������������������������������������Ŀ
//� Posiciona no usuario...                                                  �
//����������������������������������������������������������������������������
BA1->(DbSetOrder(2))
//��������������������������������������������������������������������������Ŀ
//� Monta file de entrada e saida (msg)...                                   �
//����������������������������������������������������������������������������
cFileI := cPrefFil+Iif(lMode, BBD->(BBD_CODIGO), M->BBD_CODPAC)+"I.SMF"
cFileO := cPrefFil+Iif(lMode, BBD->(BBD_CODIGO), M->BBD_CODPAC)+"O.SMF"
//��������������������������������������������������������������������������Ŀ
//� Define dialogo...                                                        �
//����������������������������������������������������������������������������
DEFINE MSDIALOG oDlg TITLE cCadastro FROM 000,000 TO ndLinFin,ndColFin
//��������������������������������������������������������������������������Ŀ
//� Define folder...                                                         �
//����������������������������������������������������������������������������
If lEvol
   @ 018,004 FOLDER oFolder SIZE 390,240 OF oDlg       PIXEL PROMPTS  "Solicitacoes de Procedimentos"
Else
   @ 018,004 FOLDER oFolder SIZE 390,240 OF oDlg       PIXEL PROMPTS  "Solicitacoes de Procedimentos"
Endif
//��������������������������������������������������������������������������Ŀ
//� Define enchoice...                                                       �
//����������������������������������������������������������������������������
STORE FIELD "BTT" TO aCpo01 FOR AllTrim(X3_CAMPO) $ "BTT_CODPAC,BTT_NOMPAC"
                                
aSvaTela := {}
aTela    := {}
aGets    := {}
cVar     := "aSvaTela["+AllTrim(Str(Len(aSvaTela)+1))+"]"
aadd(aSvaTela,{})
oEnc01 := MsMGet():New(cAliasEnc ,nRegEnc ,nOpc,,,,aCpo01,{005,005,045,381},aCpo01,,,,,oFolder:aDialogs[1],,,.T.,cVar)

oBrwBTW := TPLSBRW():New(045,005,381,220,nil  ,oFolder:aDialogs[1],nil    , nil ,nil    ,nil   ,nil, .T.   , nil  ,.T.   ,nil   ,aCabBTW,aDadBTW,.F.,"BTW",K_Alterar,"Solicitacoes")

If nOpc == K_Atend
   nHMsg := FCreate(cFileI)
   FClose(nHMsg)
   FErase(cFileO)
Endif   
//��������������������������������������������������������������������������Ŀ
//� Ativa o dialogo...                                                       �
//����������������������������������������������������������������������������
ACTIVATE MSDIALOG oDlg ON INIT EnChoiceBar(oDlg,bOK,bCanc,.F.,aButtons)
//��������������������������������������������������������������������������Ŀ
//� Trata confirmacao...                                                     �
//����������������������������������������������������������������������������
If nOpca == K_OK
	If nOpc == K_Atend .Or. nOpc == K_Alterar
		PLSA309Grv(nOpc,M->BTT_CODPAC)
		If lMode
			BBD->(DbSkip())
		Endif
	ElseIf nOpc == K_Cancelar
		//��������������������������������������������������������������������������Ŀ
		//� Inicio da transacao...                                                   �
		//����������������������������������������������������������������������������
		Begin Transaction
		If lMode
			//��������������������������������������������������������������������������Ŀ
			//� Atualiza agenda como efetuada...                                         �
			//����������������������������������������������������������������������������
			Copy "BBD" To Memory
			M->BBD_STATUS := "1"
			BBD->(PLUPTENC("BBD",K_Alterar))
		Endif
		
		//��������������������������������������������������������������������������Ŀ
		//� Atualiza arquivo de consultas a partir de agendas medicas...             �
		//����������������������������������������������������������������������������
		BTT->(PLUPTENC("BTT",K_Excluir))
		//��������������������������������������������������������������������������Ŀ
		//� Excluia...                                                               �
		//����������������������������������������������������������������������������
		aChave := {}
		aadd(aChave,{"BTW_CODPAC",M->BTT_CODPAC})
		oBrwBTW:Grava(aChave)
		//��������������������������������������������������������������������������Ŀ
		//� Finaliza transacao...                                                    �
		//����������������������������������������������������������������������������
		End Transaction
	Endif
Else
	If nOpc == K_Atend
		nHMsg := FCreate(cFileO)
		fWrite(nHMsg,M->BTT_CODPAC,_nBytes)
		FClose(nHMsg)
	Endif
Endif   

//��������������������������������������������������������������������������Ŀ
//� Fim da Rotina...                                                         �
//����������������������������������������������������������������������������
Return
/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    � PLSA309TEL � Autor � Tulio Cesar         � Data � 08.01.02 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Abaixa posicao do mBrowse e exibe mensagens...             ����
��������������������������������������������������������������������������ٱ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/
Function PLSA309Tel(dData,oBrw,cCodInt,cLocal,cCdAmbu,cDsAmbu)
LOCAL cNomMed := AllTrim(X3COMBO("BAU_LEGEND",BAU->BAU_LEGEND))+". "+AllTrim(BAU->BAU_NOME)
LOCAL cDesLoc := Posicione("BD1",1,xFilial("BD1")+cCodInt+cLocal,"BD1_DESLOC")
//��������������������������������������������������������������������������Ŀ
//� Redefine o tamanho do mbrowse...                                         �
//����������������������������������������������������������������������������
oBrw:nTop    := 95
oBrw:nBottom := oBrw:nBottom-oBrw:nTop
oBrw:nLeft   := oBrw:nLeft+018
oBrw:NRIGHT  := oBrw:NRIGHT-18
//��������������������������������������������������������������������������Ŀ
//� Monta says informativos...                                               �
//����������������������������������������������������������������������������
@ 005,055 Say oSay Prompt "PRESTADOR" Size 150,008 PIXEL OF oBrw:OWND COLOR CLR_HBLUE         
@ 005,120 Say oSay Prompt cNomMed Size 150,008 PIXEL OF oBrw:OWND

@ 015,055 Say oSay Prompt "LOCAL DE ATENDIMENTO " Size 150,008 PIXEL OF oBrw:OWND COLOR CLR_HBLUE         
@ 015,120 Say oSay Prompt cLocal+" - "+cDesLoc Size 150,008 PIXEL OF oBrw:OWND

@ 035,055 Say oSay Prompt "DATA   " Size 150,008 PIXEL OF oBrw:OWND COLOR CLR_HBLUE         
@ 035,120 Say oSay Prompt dtoc(dData)+" ("+PLSDIASEM(dData)+")"     Size 150,008 PIXEL OF oBrw:OWND

@ 005,273 BITMAP oBmp RESNAME "BR_AMARELO"   OF oBrw:OWND SIZE 20,20 NOBORDER WHEN .F. PIXEL
@ 005,284 Say oSay PROMPT "Encaixe" SIZE 040,010 OF oBrw:OWND PIXEL

@ 005,313 BITMAP oBmp RESNAME "BR_CINZA"   OF oBrw:OWND SIZE 20,20 NOBORDER WHEN .F. PIXEL
@ 005,324 Say oSay PROMPT "Agendado" SIZE 040,010 OF oBrw:OWND PIXEL

@ 020,313 BITMAP oBmp RESNAME "BR_VERDE" OF oBrw:OWND SIZE 20,20 NOBORDER WHEN .F. PIXEL
@ 020,324 Say oSay PROMPT "Espera" SIZE 040,010 OF oBrw:OWND PIXEL

@ 020,273 BITMAP oBmp RESNAME "BR_AZUL" OF oBrw:OWND SIZE 20,20 NOBORDER WHEN .F. PIXEL
@ 020,284 Say oSay PROMPT "Atendido" SIZE 040,010 OF oBrw:OWND PIXEL
//��������������������������������������������������������������������������Ŀ
//� Fim da Rotina...                                                         �
//����������������������������������������������������������������������������
Return
/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    � PLSA309GRV � Autor � Tulio Cesar         � Data � 08.07.02 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Grava os dados sem a necessidade de se fechar a tela...    ����
��������������������������������������������������������������������������ٱ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/                                      
Static Function PLSA309Grv(nOpc,cCodPAC)
LOCAL nH           
LOCAL aChave                                                                  
LOCAL lNovo
//��������������������������������������������������������������������������Ŀ
//� Testa se existe na base de dados...                                      �
//����������������������������������������������������������������������������
BTT->(DbSetOrder(1))
lNovo := !BTT->(DbSeek(xFilial("BTT")+cCodPAC))
//��������������������������������������������������������������������������Ŀ
//� Controle de numeracao igual...                                           �
//����������������������������������������������������������������������������
If nOpc == K_Atend
   nH := PLSAbreSem("PLSA309.SMF")
Endif
//��������������������������������������������������������������������������Ŀ
//� Inicio da transacao...                                                   �
//����������������������������������������������������������������������������
Begin Transaction   

If lNovo
   BTT->(PLUPTENC("BTT",K_Incluir))
Else
   BTT->(PLUPTENC("BTT",K_Alterar))
Endif   
//��������������������������������������������������������������������������Ŀ
//� Grava solicitacoes...                                                    �
//����������������������������������������������������������������������������
aChave := {} 
aadd(aChave,{"BTW_CODPAC",M->BTT_CODPAC})
oBrwBTW:Grava(aChave)
//��������������������������������������������������������������������������Ŀ
//� Finaliza transacao...                                                    �
//����������������������������������������������������������������������������
End Transaction

If nOpc == K_Atend
   nHMsg := FCreate(cFileO)
   fWrite(nHMsg,M->BTT_CODPAC,_nBytes)
   FClose(nHMsg)
Endif   
//��������������������������������������������������������������������������Ŀ
//� Fim controle numeracao...                                                �
//����������������������������������������������������������������������������
If nOpc == K_Atend
   PLSFechaSem(nH,"PLSA309.SMF")
Endif          


Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �MenuDef   � Autor � Darcio R. Sporl       � Data �04/01/2007���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Utilizacao de menu Funcional                               ���
���          �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �Array com opcoes da rotina.                                 ���
�������������������������������������������������������������������������Ĵ��
���Parametros�Parametros do array a Rotina:                               ���
���          �1. Nome a aparecer no cabecalho                             ���
���          �2. Nome da Rotina associada                                 ���
���          �3. Reservado                                                ���
���          �4. Tipo de Transa��o a ser efetuada:                        ���
���          �	  1 - Pesquisa e Posiciona em um Banco de Dados           ���
���          �    2 - Simplesmente Mostra os Campos                       ���
���          �    3 - Inclui registros no Bancos de Dados                 ���
���          �    4 - Altera o registro corrente                          ���
���          �    5 - Remove o registro corrente do Banco de Dados        ���
���          �5. Nivel de acesso                                          ���
���          �6. Habilita Menu Funcional                                  ���
�������������������������������������������������������������������������Ĵ��
���   DATA   � Programador   �Manutencao efetuada                         ���
�������������������������������������������������������������������������Ĵ��
���          �               �                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function MenuDef()
Private aRotina := {	{ STRPL01   , "AxPesqui"   , 0 , K_Pesquisar	, 0, .F.},;
						{ STRPL02  , "PLSA309MOV" , 0 , K_Visualizar	, 0, Nil},;
						{ "Lancar Proc" , "PLSA309MOV" , 0 , K_Atend		, 0, Nil},;
						{ STRPL04     , "PLSA309MOV" , 0 , K_Alterar		, 0, Nil},;
						{ "Cancelar"    , "PLSA309MOV" , 0 , K_Cancelar		, 0, Nil} }
Return(aRotina)
