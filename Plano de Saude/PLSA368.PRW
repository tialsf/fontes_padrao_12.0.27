#Include 'Protheus.ch'
#Include 'FWMVCDEF.CH'
#Include 'FWBROWSE.CH'
#Include 'topconn.ch'
#include 'PLSA368.CH'
 
//-------------------------------------------------------------------
/*/ {Protheus.doc} PLSA368
Cadastro do Contrato Preestabelecido, firmado entre Operadora e RDA
@since 10/2019
@version P12 
/*/
//-------------------------------------------------------------------
Function PLSA368(lAutomat)
Local oBrowse	:= nil
local cFiltro	:= ""
default lAutomat := .f.
_SetOwnerPrvt("lAutoma", .f. ) //Private para automa��o e demais fun��es

cFiltro := "@(B8O_FILIAL = '" + xFilial("B8O") + "') "

oBrowse := FWMBrowse():New()
oBrowse:SetAlias('B8O')
oBrowse:SetFilterDefault(cFiltro)
oBrowse:SetDescription(STR0001) //Rede de Atendimento X Contrato Preestabelecido
oBrowse:SetMenuDef('PLSA368')	
if !lAutomat
    oBrowse:Activate()
endif

Return Nil


//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menus
@since 10/2019
@version P12 
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
Local aRotina := {}

Add Option aRotina Title  STR0002	Action 'VIEWDEF.PLSA368' 	Operation 2 Access 0  //Visualizar
Add Option aRotina Title  STR0003 	Action "VIEWDEF.PLSA368" 	Operation 3 Access 0  //Incluir
Add Option aRotina Title  STR0004	Action "VIEWDEF.PLSA368" 	Operation 4 Access 0  //Alterar
Add Option aRotina Title  STR0005	Action "VIEWDEF.PLSA368"	Operation 5 Access 0  //Excluir

Return aRotina


//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Defini��o do modelo de Dados.
@since 10/2019
@version P12
/*/
//-------------------------------------------------------------------
Static Function ModelDef()
Local oModel    := nil 
Local oStrB8O	:= FWFormStruct(1,'B8O')
Local oStrB8P	:= FWFormStruct(1,'B8P')
local aGatl     := {}

oModel := MPFormModel():New( 'PLSA368', ,  { || PLSCADOK(oModel) }  ) 

aGatl := FwStruTrigger('B8O_CODRDA', 'B8O_NOMRDA', 'BAU->BAU_NOME', .T., 'BAU', 1,'xFilial("BAU") + M->B8O_CODRDA','!empty(M->B8O_CODRDA)')
oStrB8O:AddTrigger( aGatl[1], aGatl[2], aGatl[3], aGatl[4] )

oModel:AddFields('B8OMASTER', /*cOwner*/, oStrB8O )
oModel:AddGrid('B8PDetail', 'B8OMASTER', oStrB8P)

oModel:SetRelation( 'B8PDetail', { ;
    { 'B8P_FILIAL'	, 'xFilial( "B8P" )' },;
    { 'B8P_CODINT'	, 'B8O_CODINT'       },;
    { 'B8P_CODRDA'	, 'B8O_CODRDA'		 },;
    { 'B8P_IDCOPR'	, 'B8O_IDCOPR'       }},;
    B8P->( IndexKey(1) ) )

oStrB8O:setProperty( "B8O_CODRDA" , MODEL_FIELD_OBRIGAT, .t. )    

oStrB8O:setProperty( "B8O_CODINT" , MODEL_FIELD_VALID, { || VerSitGrid(empty(oModel:getModel("B8OMASTER"):getValue("B8O_CODINT")), oModel) } )
oStrB8O:setProperty( "B8O_CODRDA" , MODEL_FIELD_VALID, { || ExistCpo("BAU", oModel:getModel("B8OMASTER"):getValue("B8O_CODRDA"), 1) .and. VerSitGrid(empty(oModel:getModel("B8OMASTER"):getValue("B8O_CODRDA")), oModel)} )
oStrB8O:setProperty( "B8O_IDCOPR" , MODEL_FIELD_VALID, { || VerSitGrid(empty(oModel:getModel("B8OMASTER"):getValue("B8O_IDCOPR")), oModel) } )

oStrB8O:setProperty( "B8O_CODINT" , MODEL_FIELD_WHEN , { || oModel:GetOperation() == MODEL_OPERATION_INSERT } )
oStrB8O:setProperty( "B8O_CODRDA" , MODEL_FIELD_WHEN , { || oModel:GetOperation() == MODEL_OPERATION_INSERT } ) 
oStrB8O:setProperty( "B8O_IDCOPR" , MODEL_FIELD_WHEN , { || oModel:GetOperation() == MODEL_OPERATION_INSERT .and. !empty(oModel:getModel("B8OMASTER"):getValue("B8O_CODRDA")) } )       
oStrB8P:setProperty( "B8P_CODPRO" , MODEL_FIELD_WHEN , { || !empty(oModel:getModel("B8PDetail"):getValue("B8P_CODPAD"))  } ) 

oStrB8P:setProperty( "B8P_CODINT" , MODEL_FIELD_INIT,  { || oModel:getModel("B8OMASTER"):getValue("B8O_CODINT")} )
oStrB8P:setProperty( "B8P_CODRDA" , MODEL_FIELD_INIT,  { || oModel:getModel("B8OMASTER"):getValue("B8O_CODRDA")} )
oStrB8P:setProperty( "B8P_IDCOPR" , MODEL_FIELD_INIT,  { || oModel:getModel("B8OMASTER"):getValue("B8O_IDCOPR")} )

oModel:GetModel('B8PDetail'):SetUniqueLine( { 'B8P_CODINT', 'B8P_CODRDA', 'B8P_IDCOPR', 'B8P_CODPAD', 'B8P_CODPRO' } )
oModel:GetModel('B8OMASTER'):SetDescription( STR0001 ) //Rede de Atendimento X Contrato Preestabelecido

oModel:GetModel( 'B8PDetail' ):SetOptional( .t. )

Return oModel


//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Defini��o da interface.
@since 10/2019
@version P12
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
Local oView 
Local oModel	:= FWLoadModel( 'PLSA368' )
Local oStrB8O	:= FWFormStruct(2,'B8O')
Local oStrB8P	:= FWFormStruct(2,'B8P')

oView := FWFormView():New()
oView:SetModel( oModel )

oView:AddField( 'VIEW_B8O', oStrB8O, 'B8OMASTER' )
oView:CreateHorizontalBox( 'SUPERIOR', 40)
oView:CreateHorizontalBox( 'INFERIOR', 60)
oView:AddGrid( 'ViewB8P', oStrB8P, 'B8PDetail' )
oView:EnableTitleView('ViewB8P',STR0006) //Procedimentos contratados
oView:SetOwnerView('VIEW_B8O', 'SUPERIOR' )
oView:SetOwnerView('ViewB8P', 'INFERIOR' )

Return oView


//-------------------------------------------------------------------
/*/{Protheus.doc} PLSCADOK
Valida a inclus�o do Registro.
@since 10/2019
@version P12
/*/
//-------------------------------------------------------------------
Static Function PLSCADOK(oModel)
Local lRet		:= .T.
local cSql      := ""
local lOperIns  := iif(oModel:nOperation == MODEL_OPERATION_INSERT, .t., .f.) 

if lOperIns
    cSql := " SELECT B8O_FILIAL FROM " + RetSqlName("B8O") 
    cSql += " WHERE B8O_FILIAL = '"    + xFilial("B8O") + "' "
    cSql += " AND B8O_CODINT = '"      + oModel:getModel("B8OMASTER"):getValue("B8O_CODINT") + "' "
    cSql += " AND B8O_CODRDA = '"      + oModel:getModel("B8OMASTER"):getValue("B8O_CODRDA") + "' "
    cSql += " AND B8O_IDCOPR = '"      + oModel:getModel("B8OMASTER"):getValue("B8O_IDCOPR") + "' "
    cSql += " AND D_E_L_E_T_ = ' ' "

    dbUseArea(.t.,"TOPCONN",tcGenQry(,,ChangeQuery(cSql)),"VerRepet",.f.,.t.)

    if ( !VerRepet->(eof()) )
        lRet := .f.
        Help(nil, nil , STR0007, nil, STR0008, 1, 0, nil, nil, nil, nil, nil, {""} ) //Aten��o / J� existe esse n�mero de contrato para a mesma RDA e Operadora. Verifique as informa��es.
    endif 
    VerRepet->(dbclosearea())
endif    

Return lRet


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} PlsVldB8P
Carrega a descricao do procedimento informado 
@since 10/2019
@version P12 
/*/
//------------------------------------------------------------------------------------------
Function PLB8OPRO()
Local lRet      := .T.
local oModel    := nil
local oModelLin := nil  
local nLine     := 0

oModel     := FWModelActive()
oModelLin  := oModel:GetModel("B8PDetail")
nLine  	   := oModelLin:GetLine()

If ReadVar() == "M->B8P_CODPRO"
	If !ExistCPO("BR8", FwFldGet('B8P_CODPAD', nLine) + M->B8P_CODPRO, 1)
        lRet := .F.
    else
        oModel:getModel("B8PDetail"):loadvalue("B8P_DESPRO", Posicione("BR8",1,xFilial("BR8") + FwFldGet('B8P_CODPAD',nLine) + FwFldGet('B8P_CODPRO',nLine),"BR8_DESCRI"))
	Endif
ElseIf ReadVar() == "M->B8P_CODPAD"
	If !ExistCPO("BR4", M->B8P_CODPAD, 1)
		lRet := .F.
    endif 
    FwFldPut("B8P_CODPRO", "", nLine,,.t.,.t.)    
Endif

Return lRet
  

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} PLB8OPesq
Consulta especifica de tipos de tabela  
@since 10/2019
@version P12 
/*/
//------------------------------------------------------------------------------------------
Function PLB8OPesq()
Local lRet    := .F.
local oModel    := nil
local oModelLin := nil  
local nLine     := 0

oModel     := FWModelActive()
oModelLin  := oModel:GetModel("B8PDetail")
nLine  	   := oModelLin:GetLine()
iif( !lAutoma,  lRet := PLSPESPROC(FwFldGet('B8P_CODPAD',nLine),.f.),  lRet := .t. )
  
Return lRet


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} VerSitGrid
Atualizar as informa��es do grid, de acordo com a mudan�as ocorridas no form pai.
@since 10/2019
@version P12 
/*/
//---------------------------------------------------------------------------------------
static function VerSitGrid(lVazCon, oModel)
local oB8P      := oModel:getmodel("B8PDetail")
local oView     := FWViewActive()
local nI        := 0
if !lVazCon
	For nI := 1 to oB8P:Length()
        oB8P:GoLine(nI)
        oB8P:loadValue("B8P_CODINT", oModel:getModel("B8OMASTER"):getValue("B8O_CODINT"))
        oB8P:loadValue("B8P_CODRDA", oModel:getModel("B8OMASTER"):getValue("B8O_CODRDA"))
        oB8P:loadValue("B8P_IDCOPR", oModel:getModel("B8OMASTER"):getValue("B8O_IDCOPR"))       
    Next
    if !lAutoma
        oView:Refresh()
    endif    
endif    

return .t.