#INCLUDE 'Protheus.ch'
#INCLUDE 'FWMVCDef.ch'
#INCLUDE "TopConn.ch"
#INCLUDE "PLSMGER.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "DBTREE.CH"
#INCLUDE "tcBrowse.CH"
#INCLUDE "JPEG.CH"

/*/{Protheus.doc} PLSA502
Funcao para abrir a tela com os itens da guia, para mudan�a de fase por item editado
@author PLSTEAM
@since 11/07/2017
@version P12
/*/
function PLSA502(cAlias)
local nI	  := 0
local oBrowse := nil
local cCampos := "BD6_CODOPE,BD6_CODLDP,BD6_CODPEG,BD6_NUMERO,BD6_SEQUEN,BD6_CODPAD,BD6_CODPRO,BD6_DESPRO,BD6_QTDPRO,BD6_DATPRO,BD6_HORPRO,BD6_DTDIGI,"+; 
				 "BD6_VLRBPR,BD6_VLRAPR,BD6_VLTXAP,BD6_VLRMAN,BD6_VLRGLO,BD6_PRTXPG,BD6_VLTXPG,BD6_VLRGTX,BD6_PEINPT,BD6_VLINPT,BD6_GLINPT,BD6_VLRDES,BD6_VLRPAG,"+;
				 "BD6_PERCOP,BD6_VLRBPF,BD6_VLRPF,BD6_VLRTPF,BD6_PERTAD,BD6_VLRTAD,BD6_PERVIA,BD6_SALDO"
local aAux 	  := strToArray(cCampos,',')
local aOrdCmp := {}

private aRotina := menuDef()

if (cAlias == "BE4" .and. BE4->BE4_FASE <> "3") .or. (cAlias == "BD5" .and. BD5->BD5_FASE <> "3")

	msgAlert("Op��o dispon�vel somente para guias com fase PRONTA", "Aten��o!")
	return
	
endIf

SX3->(dbSetOrder(2))
for nI := 1 to len(aAux)
	
	if SX3->(msSeek(aAux[nI])) 
		aadd(aOrdCmp,{x3Titulo(),aAux[nI],SX3->X3_TIPO,,,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
	endIf
		
next

oBrowse := FWMBrowse():new()

oBrowse:setAlias('BD6')
oBrowse:setMenuDef('PLSA502')
oBrowse:setDescription("Eventos da guia")
oBrowse:setFilterDefault("BD6_FILIAL == '" + xFilial('BD6') + "' .AND. BD6_CODOPE == '" + (cAlias)->&(cAlias + "_CODOPE") + "' .AND. BD6_CODLDP == '" + (cAlias)->&(cAlias + "_CODLDP") +"' .AND. BD6_CODPEG == '" + (cAlias)->&(cAlias + "_CODPEG") + "' .AND. BD6_ORIMOV == '" + (cAlias)->&(cAlias + "_ORIMOV") + "' .AND. BD6_NUMERO == '" + (cAlias)->&(cAlias + "_NUMERO") + "'")

oBrowse:setFields(aOrdCmp)

oBrowse:activate()

return nil

/*/{Protheus.doc} MenuDef
Funcao para criar o menu da tela
@author PLSTEAM
@since 11/07/2017
@version P12
/*/
static function MenuDef()
local aRotina  := {}

aRotina := 	{	{ "Imprimir" 	,'PLSA502'	 , 0 , 7			},; 
				{ "Visualizar" 	,'PLSA500MOV', 0 , K_Visualizar	},; 
				{ "Pesquisar" 	,'PLSA502'	 , 0 , K_Pesquisar	},; 
				{ "Alterar" 	,'PLSA500MOV', 0 , K_Alterar	},;
				{ "Legenda" 	,'PLSA502'	 , 0 , 8			} }

return aRotina