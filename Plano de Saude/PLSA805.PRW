
#INCLUDE "Plsa805.ch"
#include "PROTHEUS.CH"
#include "PLSMGER.CH"
/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    � PLSA805 � Autor � Tulio Cesar            � Data � 04.01.01 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Autorizacao de uma forma mais simples somente para consulta����
�������������������������������������������������������������������������Ĵ���
���Sintaxe   � PLSA805()                                                  ����
�������������������������������������������������������������������������Ĵ���
��� Uso      � Advanced Protheus                                          ����
�������������������������������������������������������������������������Ĵ���
��� Alteracoes desde sua construcao inicial.                              ����
�������������������������������������������������������������������������Ĵ���
��� Data     � BOPS � Programador � Breve Descricao                       ����
�������������������������������������������������������������������������Ĵ���
��������������������������������������������������������������������������ٱ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/
Function PLSA805
//��������������������������������������������������������������������������Ŀ
//� Define variaveis PRIVATE...                                              �
//����������������������������������������������������������������������������
PRIVATE aRotina   := MenuDef()
PRIVATE cCadastro := STR0004 //"Autorizacao de Consultas"
//��������������������������������������������������������������������������Ŀ
//� Endereca a funcao de BROWSE...                                           �
//����������������������������������������������������������������������������
BE2->(DbSetOrder(1))
BE2->(DbGoTop())
BE2->(mBrowse(06,01,22,75,"BE2"))
//��������������������������������������������������������������������������Ŀ
//� Fim da Rotina Principal...                                               �
//����������������������������������������������������������������������������
Return
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � PLA805MOV � Autor � Tulio Cesar          � Data � 04.01.00 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Programa de Movimentacao da Autorizacao de consultas...    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function PLA805Mov(cAlias,nReg,nOpc)
LOCAL aOldArea := GetArea()
LOCAL aRet     := {}
LOCAL nInd     := 1
LOCAL cCampBE2 := ""
LOCAL cCampBE1 := ""
//��������������������������������������������������������������������������Ŀ
//� Caso seja alteracao exibe mensagem...                                    �
//����������������������������������������������������������������������������
If nOpc == K_Alterar
   MsgInfo(STR0005) //"Nao se pode alterar uma autorizacao. Consulte o Administrador do Sistema"
   Return
ElseIf nOpc == K_Excluir 
   cCodUsr := RetCodUsr()
   cGroup  := GrpRetName(cCodUsr)
   If AllTrim(Upper(cGroup)) <> STRPL10
      MsgInfo(STR0006) //"Somente usuarios Administradores pode excluir uma autorizacao"
      Return
   Endif   
Endif
//��������������������������������������������������������������������������Ŀ
//� Salva area antiga...                                                     �
//����������������������������������������������������������������������������
aOldArea := GetArea()
//��������������������������������������������������������������������������Ŀ
//� Dados da Enchoice...                                                     �
//����������������������������������������������������������������������������
PRIVATE nOpcA := 0                                                           
PRIVATE nOpcx := nOpc
//��������������������������������������������������������������������������Ŀ
//� Dados da EnchoiceBar                                                     �
//����������������������������������������������������������������������������
PRIVATE aButtons  := {}
//��������������������������������������������������������������������������Ŀ
//� Dados da Enchoice                                                        �
//����������������������������������������������������������������������������
PRIVATE aTELA[0][0]
PRIVATE aGETS[0]
PRIVATE oEncAut
//��������������������������������������������������������������������������Ŀ
//� Dados da TPLSBRW...                                                      �
//����������������������������������������������������������������������������
PRIVATE oBrwAut
PRIVATE aHeader  := {}
PRIVATE aCols    := {}
PRIVATE aColsTrab
//��������������������������������������������������������������������������Ŀ
//� Logicos genericos...                                                     �
//����������������������������������������������������������������������������
PRIVATE lInclui := (nOpcx == K_Incluir)
PRIVATE lVisual := (nOpcx == K_Visualizar)
//��������������������������������������������������������������������������Ŀ
//� Dados do Dialogo Principal...                                            �
//����������������������������������������������������������������������������
PRIVATE oDlg
PRIVATE cCodPad := "01"
//��������������������������������������������������������������������������Ŀ
//� Genericos...                                                             �
//����������������������������������������������������������������������������
PRIVATE nColAut
PRIVATE nItensSIM := 0
PRIVATE nItensNAO := 0
//��������������������������������������������������������������������������Ŀ
//� Grava chave da autorizacao...                                            �
//����������������������������������������������������������������������������
PRIVATE cChave   := BE2->(BE2_ANOAUT+BE2_MESAUT+BE2_NUMAUT)
PRIVATE nRecno   := BE2->(Recno())
//��������������������������������������������������������������������������Ŀ
//� Monta campos especificos...                                              �
//����������������������������������������������������������������������������
cCampos := "BE1_USUARI,BE1_MATANT,BE1_NOMUSR,BE1_DATPRO,BE1_CODCRE,BE1_NOMCRE,BE1_CODLOC"
cCampos += ",BE1_DESLOC,BE1_ENDLOC,BE1_SIGRES,BE1_CRRESP,BE1_NOMRES,BE1_SIGLA,BE1_REGSOL,BE1_NOMSOL,BE1_USUOPE"
cCampos += ",BE1_NUMAUT,BE1_ANOAUT,BE1_MESAUT,BE1_ESTSOL,BE1_CODESP,BE1_DESESP"
//��������������������������������������������������������������������������Ŀ
//� Ponto de entrada para inclusao de outros campos para serem exibidos      �
//����������������������������������������������������������������������������
If ExistBlock("PL805CPO")
   //��������������������������������������������������������������������������Ŀ
   //� Exemplo de retorno : {BE1_ESTSOL}                                        �
   //�                      {BE1_CODFFF}                                        �
   //����������������������������������������������������������������������������
   aRet    := ExecBlock("PL805CPO",.T.,.T.)
   For nInd := 1 To Len(aRet)
       cCampos += ","+aRet[nInd,1]
   Next
Endif

aCampos := {}

SX3->(DbSetOrder(1))
If SX3->(DbSeek("BE1"))
   While ! SX3->(Eof()) .And. SX3->X3_ARQUIVO == "BE1"
         If AllTrim(SX3->X3_CAMPO) $ cCampos
            aadd(aCampos,SX3->X3_CAMPO)
         Endif   
   SX3->(DbSkip())
   Enddo
Endif   
//��������������������������������������������������������������������������Ŀ
//� Define botoes do EnchoiceBar                                             �
//����������������������������������������������������������������������������
If Inclui
   aadd(aButtons,{"S4WB004N"    ,{|| A090Limpa("BE1",oEncAut,oBrwAut,"BE2") },STR0007}                  ) //"Limpar Tela - <F5>"
   SetKey(VK_F5,{|a,b| A090Limpa("BE1",oEncAut,oBrwAut,"BE2") })
Endif   

aadd(aButtons,{"SALARIOS" ,{|| PLPOSFIN(M->BE1_USUARI,.T.,.T.) },STR0008}           )  //"Posicao Financeira - <F6>"
SetKey(VK_F6,{|a,b| PLPOSFIN(M->BE1_USUARI,.T.,.T.) })

//aadd(aButtons,{"RELATORIO",{|| PLHISMOV(M->BE1_USUARI) },STR0009}     ) //"Historico de Movimentacao - <F7>"
//SetKey(VK_F7,{|a,b| PLHISMOV(M->BE1_USUARI) })

aadd(aButtons,{"EDIT"      ,{|| Pergunte("PLA805",.T.),cCodPad := mv_par01 }, STR0021} ) //"Altera Codigo da Tabela Padrao Saude - <F8>"
SetKey(VK_F8,{|a,b| Pergunte("PLA805",.T.),cCodPad := mv_par01 })

aadd(aButtons,{"POSCLI"      ,{|| (M->BE1_USUARI) },STR0010}                  ) //"Alterar Dados do Titular/Preposto - <F8>"
SetKey(VK_F9,{|a,b| PLAltCli(M->BE1_USUARI) })

aadd(aButtons,{"GROUP"      ,{|| PLSA530() },STR0011}                  ) //"Solicitantes - <F9>"
SetKey(VK_F10,{|a,b| PLSA530() })

aadd(aButtons,{"PLNPROP"     ,{|| PlValCob() },STR0012}                  ) //"Composicao de Cobranca"
SetKey(VK_F11,{|a,b| PlValCob() })

aadd(aButtons,{"POSCLI",{|| aVet := PLSSol(), M->BE1_SIGLCR := aVet[1], M->BE1_REGSOL := aVet[2], M->BE1_NOMSOL := aVet[3]  },"Pesquisar Solicitante por Nome"})
SetKey(VK_F12,{|| aVet := PLSSol(), M->BE1_SIGLCR := aVet[1], M->BE1_REGSOL := aVet[2], M->BE1_NOMSOL := aVet[3]  })
//��������������������������������������������������������������������������Ŀ
//� Monta M->XXX_Campo de acordo com a opcao escolhida...                    �
//����������������������������������������������������������������������������
If lInclui
   BE1->(RegToMemory("BE1",.T.))
Else
   BE1->(RegToMemory("BE1",.F.))
Endif   
//��������������������������������������������������������������������������Ŀ
//� Posiciona no registro selecionado...                                     �
//����������������������������������������������������������������������������
BE2->(DbGoTo(nRecno))
//��������������������������������������������������������������������������Ŀ
//� Cria Dialog...                                                           �
//����������������������������������������������������������������������������
DEFINE MSDIALOG oDlg TITLE cCadastro FROM 008.2,010.3 TO 034.4,100.3 OF GetWndDefault()
//��������������������������������������������������������������������������Ŀ
//� Cria Enchoice e GetDados...                                              �
//����������������������������������������������������������������������������
oEncAut := MSMGET():New("BE1",BE1->(Recno()),nOpcx,,,,aCampos,{015,001,190,355},aCampos,,,,,oDlg,,,.F.)
//��������������������������������������������������������������������������Ŀ
//� Ativa Dialog...                                                          �
//����������������������������������������������������������������������������
ACTIVATE MSDIALOG oDlg ON INIT Eval({ || PlInitCols(aCols,aHeader,oBrwAut), EnchoiceBar(oDlg,{|| nOpca := 1,If(Obrigatorio(aGets,aTela) .And. PLA90Resp(M->BE1_CODCRE,M->BE1_SIGRES,M->BE1_CRRESP) .And. PL805Con() ,oDlg:End(),nOpca:=2),If(nOpca==1,oDlg:End(),.F.) },{||oDlg:End()},.F.,aButtons) })      
//��������������������������������������������������������������������������Ŀ
//� Atualizacao dos dados se necessario...                                   �
//����������������������������������������������������������������������������
If nOpca == K_OK
   If nOpcx == K_Incluir
 	   Begin Transaction         
   
      //��������������������������������������������������������������������������Ŀ
      //� Neste ponto gravo dados fixos.                                           �
      //����������������������������������������������������������������������������
      M->BE1_NUMAUT := PLNUMAUT(M->BE1_ANOAUT,M->BE1_MESAUT)
      M->BE1_HORFIN := Time()
      //��������������������������������������������������������������������������Ŀ
      //� Grava dados utilizando rotina generica...                                �
      //����������������������������������������������������������������������������
      BA1->(DbSetOrder(2))
      
      If ! BA1->(DbSeek(xFilial("BA1")+M->BE1_USUARI))
         Help("",1,"PL805BA1")
      Endif
      BE2->(RecLock("BE2",.T.))
      BE2->BE2_FILIAL := xFilial("BE2")
      BE2->BE2_DATPRO := M->BE1_DATPRO
      BE2->BE2_CODCRE := M->BE1_CODCRE
      BE2->BE2_CODLOC := M->BE1_CODLOC
      BE2->BE2_CODESP := M->BE1_CODESP
      BE2->BE2_USUOPE := PLRETOPE()
      BE2->BE2_DTDIGI := Date()
      BE2->BE2_HORINI := M->BE1_HORINI
      BE2->BE2_HORFIN := Time()
      BE2->BE2_SEGTOT := "00"
      BE2->BE2_NUMCON := BA1->BA1_NUMCON
      BE2->BE2_CODINT := BA1->BA1_CODINT
      BE2->BE2_CODEMP := BA1->BA1_CODEMP
      BE2->BE2_MATRIC := BA1->BA1_MATRIC
      BE2->BE2_TIPREG := BA1->BA1_TIPREG
      BE2->BE2_MATUSU := BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG)
      BE2->BE2_ANOAUT := M->BE1_ANOAUT
      BE2->BE2_MESAUT := M->BE1_MESAUT
      BE2->BE2_NUMAUT := M->BE1_NUMAUT      
      BE2->BE2_POSPAG := "N"          
      BE2->BE2_ESTSOL := M->BE1_ESTSOL
      BE2->BE2_SIGLA  := M->BE1_SIGLA
      BE2->BE2_REGSOL := M->BE1_REGSOL
      BE2->BE2_SIGRES := M->BE1_SIGRES
      BE2->BE2_CRRESP := M->BE1_CRRESP
      BE2->BE2_CODPAD := cCodPad
      BE2->BE2_CODPRO := "00010014"
      BE2->BE2_QTDREF := 1
      BE2->BE2_STACRI := "00"
      BE2->BE2_CODLIB := "00"

      //��������������������������������������������������������������������������Ŀ
      //� Grava os campos que foram criados pelo ponto de entrada ...              �
      //����������������������������������������������������������������������������
      nInd := 1
      SX3->(DbSetOrder(2))
      For nInd := 1 To Len(aRet)
          cCampBE2  := "BE2->BE2_"+Subs(aRet[nInd,1],5,6)
          cCampBE1  := "M->BE1_"+Subs(aRet[nInd,1],5,6)     

          If SX3->(DbSeek(aRet[nInd,1]))
             If SX3->X3_CONTEXT <> "V"
                &cCampBE2 := &cCampBE1
             Endif
          Endif
                        
      Next
      
      BE2->(MsUnLock())
      //��������������������������������������������������������������������������Ŀ
      //� Exibe tela com os dados do autorizacao...                                �
      //����������������������������������������������������������������������������
      If nOpcx == K_Incluir
         A805Final()
      Endif
      
 	   End Transaction
   Endif
Endif
//��������������������������������������������������������������������������Ŀ
//� Desativa Sets Keys...                                                    �
//����������������������������������������������������������������������������
SET KEY VK_F5 TO
SET KEY VK_F6 TO
SET KEY VK_F7 TO
SET KEY VK_F8 TO
SET KEY VK_F9 TO
SET KEY VK_F10 TO
//��������������������������������������������������������������������������Ŀ
//� Restaura area antiga...                                                  �
//����������������������������������������������������������������������������
RestArea(aOldArea)
//��������������������������������������������������������������������������Ŀ
//� Fim da Rotina Principal...                                               �
//����������������������������������������������������������������������������
Return
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � A805Final � Autor � Tulio Cesar          � Data � 20.04.00 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Exibir dados da autorizacao/liberacao efetuada             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
/*/
Static Function A805Final()
LOCAL cNomUser
LOCAL cSIM
LOCAL nNAO
LOCAL cNomCre
LOCAL oDlg

LOCAL oFontNum
LOCAL oFontAutor                                    
LOCAL oFontTit
LOCAL oSay
//��������������������������������������������������������������������������Ŀ
//� Define fontes utilizadas somente nesta funcao...                         �
//����������������������������������������������������������������������������
DEFINE FONT oFontNum NAME "Arial" SIZE 000,-016 BOLD
DEFINE FONT oFontAutor NAME "Arial" SIZE 000,-019 BOLD
DEFINE FONT oFontTit NAME "Arial" SIZE 000,-011 BOLD
//��������������������������������������������������������������������������Ŀ
//� Mostra Resumo da Autorizacao...                                          �
//����������������������������������������������������������������������������
DEFINE MSDIALOG oDlg TITLE STR0013+M->BE1_SEGTOT+STR0014 FROM 009,000 TO 024,070 OF GetWndDefault() //"Dados da Autorizacao - Tempo Percorrido "###" Segundo(s)"

cNomUser := TransForm(M->BE1_USUARI,PesqPict("BE1","BE1_USUARI"))+STR0015+M->BE1_NOMUSR //" - "
cNomCre  := AllTrim(M->BE1_CODCRE)+STR0015+M->BE1_NOMCRE //" - "

SButton():New(097, 005, 1, {|| oDlg:End() },,.T.)
         
@ 007,005 SAY oSay PROMPT STR0016  SIZE 220,010 OF oDlg PIXEL FONT oFontNum //"Autorizacao Numero   "
@ 006,095 SAY oSay PROMPT M->BE1_ANOAUT+STR0017+M->BE1_MESAUT+"-"+AllTrim(Str(Val(M->BE1_NUMAUT))) SIZE 220,010 OF oDlg PIXEL FONT oFontAutor COLOR CLR_HRED //"."

@ 025,005 SAY oSay PROMPT STR0018  SIZE 220,010 OF oDlg PIXEL FONT AdvFont //"Usuario              "
@ 025,065 MSGET cNomUser                           SIZE 205,010 OF oDlg PIXEL FONT oFontTit COLOR CLR_HBLUE

@ 043,005 SAY oSay PROMPT STR0019  SIZE 220,010 OF oDlg PIXEL FONT AdvFont //"Credenciado          "
@ 043,065 MSGET cNomCre                            SIZE 205,010 OF oDlg PIXEL FONT oFontTit COLOR CLR_HBLUE

ACTIVATE MSDIALOG oDlg CENTERED

Return
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � PLVLDCON  � Autor � Tulio Cesar          � Data � 04.01.01 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Validar se o usuario pode realizar a consulta.             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
/*/
Static Function PL805Con()
   
//lRet := PL090Aut("00010014",1,M->BE1_USUARI,M->BE1_CODLOC,M->BE1_CODORD,M->BE1_CODCRE,M->BE1_LOJA,nil,.F.,M->BE1_DATPRO,M->BE1_CRRESP,M->BE1_SIGRES)

//If ! lRet
//   MsgStop(STR0020) //"Nao autorizado."   
//Else
//   lRet := PLSVLDPRO(M->BE1_CODESP,cCodPad,"00010014",M->BE1_CODLOC,Subs(M->BE1_USUARI,1,4),M->BE1_CODCRE)
//Endif


Return(lRet)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �MenuDef   � Autor � Darcio R. Sporl       � Data �08/01/2007���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Utilizacao de menu Funcional                               ���
���          �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �Array com opcoes da rotina.                                 ���
�������������������������������������������������������������������������Ĵ��
���Parametros�Parametros do array a Rotina:                               ���
���          �1. Nome a aparecer no cabecalho                             ���
���          �2. Nome da Rotina associada                                 ���
���          �3. Reservado                                                ���
���          �4. Tipo de Transa��o a ser efetuada:                        ���
���          �		1 - Pesquisa e Posiciona em um Banco de Dados           ���
���          �    2 - Simplesmente Mostra os Campos                       ���
���          �    3 - Inclui registros no Bancos de Dados                 ���
���          �    4 - Altera o registro corrente                          ���
���          �    5 - Remove o registro corrente do Banco de Dados        ���
���          �5. Nivel de acesso                                          ���
���          �6. Habilita Menu Funcional                                  ���
�������������������������������������������������������������������������Ĵ��
���   DATA   � Programador   �Manutencao efetuada                         ���
�������������������������������������������������������������������������Ĵ��
���          �               �                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function MenuDef()
Private aRotina := {	{ STR0001	,'AxPesqui' ,  0, K_Pesquisar  , 0, .F.},; //"Pesquisar"
											{ STR0002	,'PLA805Mov',  0, K_Visualizar , 0, Nil},; //"Visualizar"
											{ STR0003	,'PLA805Mov',  0, K_Incluir    , 0, Nil} } //"Incluir"
Return(aRotina)                                                  