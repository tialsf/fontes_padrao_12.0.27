#INCLUDE "plsa960.ch"
#include "PLSMGER.CH"
#include "PROTHEUS.CH"
/*/
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪哪勘�
北砅rograma  � PLSA960 � Autor � Eduardo Motta        � Data � 14.09.2001 潮�
北媚哪哪哪哪呐哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪哪幢�
北矰escri噭o � Cadastro de Profissionais de saude.                        潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砋so       � Advanced Protheus                                          潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� Nenhum                                                     潮�
北媚哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北�            ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL           潮�
北媚哪哪哪哪哪穆哪哪哪哪履哪哪穆哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅rogramador � Data   � BOPS �  Motivo da Altera噭o                     潮�
北媚哪哪哪哪哪呐哪哪哪哪拍哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北滥哪哪哪哪哪牧哪哪哪哪聊哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
/*/
Function PLSA960
Local cFiltro:=""
Local aRotina   := MenuDef()
PRIVATE cPrePRF   := "00"
PRIVATE cAlias    := "BB0"
PRIVATE cCadastro := PLSRetTit(cAlias)
PRIVATE	aCdCores  := {	{ 'BR_VERDE'    ,OemtoAnsi(STR0014) },; //"Profissional de Sa鷇e Ativo"
						{ 'BR_VERMELHO' ,OemtoAnsi(STR0015) } } //"Profissional de Sa鷇e Bloqueado"

If ExistBlock('PLSA960FIL')
	cFiltro := Execblock('PLSA960FIL',.F.,.F.)
Endif							
					
						
//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
//� Endereca a funcao de BROWSE...                                           �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
BB0->(DbSetOrder(1))
If BB0->(FieldPos("BB0_CODBLO")) > 0 .And. PLSALIASEX("B17")
	BB0->(mBrowse(006,001,022,075,"BB0" , ,"BB0->BB0_CODBLO<>Space(03)" , , , Nil    ,, , , ,NIL,,,,cFiltro))
Else
	BB0->(mBrowse(006,001,022,075,"BB0" , , , , , Nil    ,, , , ,NIL,,,,cFiltro))
EndIf

Return

/*/
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北砅rograma  砅LSA960MOV � Autor � Eduardo Motta        � Data �14.09.2001潮�
北媚哪哪哪哪呐哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Cadastro de Profissionais de saude.                        潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砋so       � Advanced Protheus                                          潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� Nenhum                                                     潮�
北媚哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北�            ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL           潮�
北媚哪哪哪哪哪穆哪哪哪哪履哪哪穆哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅rogramador � Data   � BOPS �  Motivo da Altera噭o                     潮�
北媚哪哪哪哪哪呐哪哪哪哪拍哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北滥哪哪哪哪哪牧哪哪哪哪聊哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
/*/
Function PLSA960MOV(cAlias,nReg,nOpc)
Local lHomonim	:= SuperGetMV("MV_PLSHOMO",,.F.)  // parametro para Verifica玢o de Homonimo 
Local lbkpINCL	:= IIF( Type("INCLUI") == "L", INCLUI, nOpc == K_Incluir )
// Inicializa variavel cCadastro para compatibilizar a rotina para ser utilizada pela auditoria...
If Type('cCadastro') == 'U'
	cCadastro := PLSRetTit(cAlias)
Endif

If nOpc == K_Incluir
	INCLUI := .T.
   AxInclui(cAlias,nReg,K_Incluir,nil,nil,nil,'PLS960VLD("I")')
   INCLUI := lbkpINCL
Endif

If nOpc == K_Alterar   
   If BB0->BB0_VINC == "2"
      AxAltera(cAlias,nReg,K_Alterar,nil,nil,nil,nil,IIF(BB0->(FieldPos("BB0_MATVID")) >0 .and. lHomonim ,'PlsHomoni(M->BB0_NOME,M->BB0_CGC,M->BB0_NUMCR,"","","","",cAlias,M->BB0_MATVID,.F.)',NIL)) 
   Else
      HELP(" ",1,"PLSA960MOV")
   Endif
Endif


If nOpc == K_Visualizar
   AxVisual(cAlias,nReg,K_Visualizar)
Endif

Return
/*/
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲uncao    � PLSA960BLO � Autor � Alexander 		    � Data � 1.11.07  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escricao � Valida inclusao/alteracao e exclusao						  潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
/*/
Function PLS960VLD(cTp)
LOCAL lRet	:= .T.
LOCAL aArea	:= GetArea()
LOCAL lHomonim	:= SuperGetMV("MV_PLSHOMO",,.F.)  // parametro para Verifica玢o de Homonimo    
LOCAL aChaves:={}

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
//� Validacao															�
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
If cTp == 'I'
	BB0->(DbSetOrder(4)) //BB0_FILIAL + BB0_ESTADO + BB0_NUMCR + BB0_CODSIG + BB0_CODOPE
	If BB0->( MsSeek(xFilial("BB0")+M->(BB0_ESTADO+BB0_NUMCR+BB0_CODSIG+BB0_CODOPE) ) )
		MsgAlert('Profissional de Sa鷇e j� cadastrado')
		lRet := .F.
	EndIf
	If lRet .And. BB0->(FieldPos("BB0_MATVID")) >0 .And. lHomonim 
		PlsHomoni(M->BB0_NOME,M->BB0_CGC,M->BB0_NUMCR,"","","","","BB0",M->BB0_MATVID,.T.)
	Endif
	
	If lRet
		BB0->(DbSetOrder(1)) //BB0_FILIAL + BB0_CODIGO
		If BB0->( MsSeek(xFilial("BB0")+M->(BB0_CODIGO) ) )
			MsgAlert('Codigo de Profissional de Sa鷇e j� cadastrado')
			lRet := .F.
		EndIf
	
	Endif
	
		
Else       
	DbSelectArea("BAU")
	BAU->(DbSetOrder(5)) //BAU_FILIAL + BAU_CODBB0
	If BAU->( MsSeek(xFilial("BAU")+BB0->BB0_CODIGO ) )
		MsgAlert('Existe relacionamento deste Profissional de Sa鷇e com uma Rede de Atendimento')
		lRet := .F.	
	EndIf	
	
	If lRet
		//Procura por solicitante
		
		aadd(aChaves,{"B44","B44_CDPFSO",BB0->BB0_CODIGO})
		aadd(aChaves,{"BD5","BD5_CDPFSO",BB0->BB0_CODIGO})
		aadd(aChaves,{"BD6","BD6_CDPFSO",BB0->BB0_CODIGO})
		aadd(aChaves,{"BEA","BEA_CDPFSO",BB0->BB0_CODIGO})
		aadd(aChaves,{"BE2","BE2_CDPFSO",BB0->BB0_CODIGO})
		
		lRet := PLSCHKDEL(aChaves)
		
		If lRet
			//Procura por executate
			aChaves:={}
			aadd(aChaves,{"B44","B44_CDPFRE",BB0->BB0_CODIGO})
			aadd(aChaves,{"BD5","BD5_CDPFRE",BB0->BB0_CODIGO})
			aadd(aChaves,{"BD6","BD6_CDPFRE",BB0->BB0_CODIGO})
			aadd(aChaves,{"BC1","BC1_CODPRF",BB0->BB0_CODIGO})
			aadd(aChaves,{"BEA","BEA_CDPFRE",BB0->BB0_CODIGO})
			aadd(aChaves,{"BE2","BE2_CDPFRE",BB0->BB0_CODIGO})
			
			lRet := PLSCHKDEL(aChaves)
		Endif
		
	Endif
EndIf          

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
//� Retorna para area correte											�
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
RestArea(aArea)

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
//� Fim da rotina														�
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
Return lRet
/*/
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲uncao    � PLSA960BLO � Autor � Sandro Hoffman      � Data � 13.09.06 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escricao � Bloquear/Desbloquear Profissional de Saude                 潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
/*/
Function PLSA960Blo(cAlias,nReg,nOpc)
Local I__f := 0 // Criada para que o comando COPY "B17" TO MEMORY BLANK n鉶 de erro na compilacao
//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
//� Define variaveis usadas na rotina...                                �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
LOCAL cTitulo := PLSRetTit(cAlias)
LOCAL nOpca   := 0
LOCAL aButtons := {}
LOCAL nOrdBB0 := BB0->(IndexOrd())
LOCAL nRecBB0 := BB0->(Recno())

LOCAL aPosObj   := {}
LOCAL aObjects  := {}
LOCAL aSize     := {}
LOCAL aInfo     := {}

Private M->BC4_TIPO := If(!Empty(BB0->BB0_CODBLO),"1","0") // Variavel criada para que a consulta padrao BB1PLS funcione
PRIVATE lBloq
PRIVATE oEncBlo
PRIVATE oDlgBlo
PRIVATE aGets[0]
PRIVATE aTela[0][0]
//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
//� Define controle para saber bloqueio/desbloqueio                     �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
lBloq   := !Empty(BB0->BB0_CODBLO)
//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
//� Define dialogo...                                                   �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�

aSize := MsAdvSize()
aObjects := {}       
AAdd( aObjects, { 100, 100, .T., .T. } )

aInfo := { aSize[ 1 ], aSize[ 2 ], aSize[ 3 ], aSize[ 4 ], 3, 3 }
aPosObj := MsObjSize( aInfo, aObjects )

DEFINE MSDIALOG oDlgBlo TITLE cTitulo FROM aSize[7],0 To aSize[6],aSize[5] OF oMainWnd PIXEL 
//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
//� Define entrada de dados padrao...                                   �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
Copy "B17" TO Memory Blank

aAdd(aButtons, {"HISTORIC",{|| A960HISBLO() },OemtoAnsi(STR0007),OemtoAnsi(STR0008)}) //"Historico de (Des)bloqueio"###"Historico"
	
oEncBlo := MSMGet():New("B17",B17->(Recno()),K_Alterar,,,,nil,aPosObj[1],nil,,,,,oDlgBlo,,,.F.)
//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
//� Define valores para os campos chave da familia...                   �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
BB0->(DbSetOrder(nOrdBB0))
BB0->(DbGoTo(nRecBB0))

M->B17_CODPFS := BB0->BB0_CODIGO
M->B17_NOMPFS := BB0->BB0_NOME
M->B17_TIPO   := If(lBloq,"1","0")
lRefresh       := .T.
//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
//� Ativa dialogo...                                                    �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
ACTIVATE MSDIALOG oDlgBlo ON INIT Eval({ || EnchoiceBar(oDlgBlo,{|| nOpca := 1,If(Obrigatorio(aGets,aTela),oDlgBlo:End(),nOpca:=2),If(nOpca==1,oDlgBlo:End(),.F.) },{||oDlgBlo:End()},.F.,aButtons) })
//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
//� Tratamento para gravacao caso houve confirmacao de bloqueio         �.
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
If nOpca == K_OK
	
	PLUPTENC("B17",K_Incluir)
	
	If ! lBloq
		//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
		//� Bloqueio...                                                         �
		//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
		BB0->(RecLock("BB0",.F.))
			BB0->BB0_CODBLO := M->B17_MOTBLO
			BB0->BB0_DATBLO := M->B17_DATA
		BB0->(MsUnLock())            

		//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
		//� Ponto de entrada para o bloqueio...                                 �
		//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�		
		If ExistBlock("PLS360BLQ")
			ExecBlock("PLS360BLQ",.F.,.F.,{nOpc})
		Endif
	Else
		BB0->(RecLock("BB0",.F.))
			BB0->BB0_CODBLO := ""
			BB0->BB0_DATBLO := ctod("")
		BB0->(MsUnLock())
		//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
		//� Ponto de entrada para o desbloqueio...                              �
		//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�		
		If ExistBlock("PLS360DSBLQ")
			ExecBlock("PLS360DSBLQ",.F.,.F.,{nOpc})
		Endif		
	Endif
	
	BB0->(DbCommitAll())
	
	BB0->(DbSetOrder(nOrdBB0))
	BB0->(DbGoTo(nRecBB0))
	
Endif

Return

/*/
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北
北谀哪哪哪哪穆哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘北
北矲uncao    � A960HISBLO � Autor � Sandro Hoffman      � Data � 13.09.06 潮北
北媚哪哪哪哪呐哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢北
北矰escricao � Consulta o historico de bloqueio do Profissional de Saude  潮北
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦北
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌
/*/

Function A960HISBLO()

Local aCabB17  := {}
Local aDadB17 := {}                                          
Local aTrbB17 := {}
Local nOpca   := 1
Local oBrwB17   
Local oDlg

B17->(DbSetOrder(1)) //B17_FILIAL + B17_CODPFS + DTOS(B17_DATA) + B17_HORA
If  B17->(MsSeek(xFilial("B17")+M->B17_CODPFS))
	DEFINE MSDIALOG oDlg TITLE 'Historico (Des)bloqueio' FROM 009,010 TO 029,110 OF GetWndDefault()
       Store Header "B17" TO aCabB17 For .T. .and. SX3->X3_CAMPO <> "B17_CODPFS" .and. SX3->X3_CAMPO <> "B17_NOMPFS"  
       Store COLS 	"B17" TO aDadB17 From aCabB17 VETTRAB aTrbB17 While xFilial("B17")+M->B17_CODPFS == B17->(B17_FILIAL+B17_CODPFS)
       oBrwB17  := TPLSBrw():New(013 ,001 ,395 ,150 ,nil ,oDlg ,nil ,{|| nil } ,nil ,nil  ,nil , .T., nil, .T. , nil ,aCabB17 ,aDadB17 ,.F. ,"B17" ,K_Visualizar,OemtoAnsi(STR0009),nil,nil,nil,aTrbB17,,,) //"Hist髍ico (Des)bloqueio dos Profissionais de Sa鷇e"
	ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar( oDlg, {|| nOpca := 1,oDlg:End()}, {|| nOpca := 2,oDlg:End()} )
Else
    Aviso( OemtoAnsi(STR0010),OemtoAnsi(STR0011),{ OemtoAnsi(STR0012) }, 2 )//"Profissional de Sa鷇e"###"N鉶 existe Hist髍ico de (Des)bloqueio para este Profissional de Sa鷇e."###"Ok"
Endif
	
Return  


/*/
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北
北谀哪哪哪哪穆哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘北
北矲uncao    � PLS960DEL  � Autor �                     � Data � 22.03.12 潮北
北媚哪哪哪哪呐哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢北
北矰escricao � Delete o profissional de Saude                             幢北
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦北
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌
/*/
Function PLS960DEL(cAlias,nReg,nOpc)


Local lRet:= .T.

If BB0->BB0_VINC == "2"              
		lRet:=PLS960VLD('D')
		if lRet
	      AxDeleta(cAlias,nReg,K_Excluir)
		Endif	      
Else
      HELP(" ",1,"PLSA960MOV")
 Endif

Return       


/*/
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北
北谀哪哪哪哪穆哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘北
北矲uncao    � A960CHKDAT � Autor � Sandro Hoffman      � Data � 13.09.06 潮北
北媚哪哪哪哪呐哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢北
北矰escricao � Checa data de (des)bloqueio								  潮北
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦北
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌
/*/

Function A960CHKDAT()

Local lRet := .T.                                                                                                                                                               
   
If M->B17_DATA < BB0->BB0_DATBLO 
       MsgAlert(OemtoAnsi(STR0013)) //"Data de (Des)bloqueio INVALIDA!"
       lRet := .F.
EndIf 	  
   

Return(lRet)

/*/
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北
北谀哪哪哪哪穆哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘北
北矲uncao    � PLSA960LEG � Autor � Sandro Hoffman      � Data � 13.09.06 潮北
北媚哪哪哪哪呐哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢北
北矰escricao � Exibe a legenda...                                         潮北
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦北
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌
/*/
Function PLSA960LEG()

BrwLegenda(cCadastro,"Status" ,aCdCores)

Return

/*/
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北砅rograma  矼enuDef   � Autor � Darcio R. Sporl       � Data �08/01/2007潮�
北媚哪哪哪哪呐哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Utilizacao de menu Funcional                               潮�
北�          �                                                            潮�
北�          �                                                            潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砇etorno   矨rray com opcoes da rotina.                                 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros砅arametros do array a Rotina:                               潮�
北�          �1. Nome a aparecer no cabecalho                             潮�
北�          �2. Nome da Rotina associada                                 潮�
北�          �3. Reservado                                                潮�
北�          �4. Tipo de Transa噭o a ser efetuada:                        潮�
北�          �		1 - Pesquisa e Posiciona em um Banco de Dados           潮�
北�          �    2 - Simplesmente Mostra os Campos                       潮�
北�          �    3 - Inclui registros no Bancos de Dados                 潮�
北�          �    4 - Altera o registro corrente                          潮�
北�          �    5 - Remove o registro corrente do Banco de Dados        潮�
北�          �5. Nivel de acesso                                          潮�
北�          �6. Habilita Menu Funcional                                  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北�   DATA   � Programador   矼anutencao efetuada                         潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北�          �               �                                            潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
/*/
Static Function MenuDef()
Local aRotina := {	{ STR0001 , 'AxPesqui' 		, 0, K_Pesquisar , 0, .F.},; //"Pesquisar"
						{ STR0002 , 'PLSA960MOV' 	, 0, K_Visualizar, 0, Nil},; //"Visualizar"
       		         	{ STR0003 , 'PLSA960MOV' 	, 0, K_Incluir   , 0, Nil},; //"Incluir"
			   			{ STR0004 , 'PLSA960MOV' 	, 0, K_Alterar   , 0, Nil},; //"Alterar"
			   			{ STR0005 , 'PLS960DEL' 	, 0, K_Excluir   , 0, Nil},;
			   			{ STR0006	, 'PLSA960BLO' , 0, K_Excluir    , 0, Nil},; 
			   			{ STR0016	, 'PLSA960LEG' , 0, 0        , 0, .F.}} 
                  

	AADD(aRotina,{ 'Especialidade'	, 'PLSA960ESP' , 0, K_Alterar        , 0, .F.})


Return(aRotina)


/*/
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北
北谀哪哪哪哪哪履哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪穆哪哪哪履哪哪哪哪目北
北� Fun噮o    砅LSESPMOV � Autor � Daher	             � Data � 04/07/13 潮�
北媚哪哪哪哪哪拍哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪牧哪哪哪聊哪哪哪哪拇北
北� Descri噮o |Ajusta SXB;                                                 潮�
北媚哪哪哪哪哪拍哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪拇北
北� Uso       �                                                            潮�
北媚哪哪哪哪哪拍哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪拇北
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌
/*/
Function PLSESPMOV(cAlias,nReg,nOpc)

BAU->(DbSetORder(5))
If BAU->(MsSeek(xFilial("BAU")+BB0->BB0_CODIGO))
    MsgStop('A manuten玢o das especialidades deste profissional deve ser realizado pelo cadastro de RDA')
else
	AxInclui(cAlias,nReg,K_Incluir,nil,nil,nil,'Iif(Empty(M->BQ1_CODESP),Eval({|| MsgStop("Informe a especialidade"),.F.}),.T.)')
Endif

return

/*/
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北
北谀哪哪哪哪哪履哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪穆哪哪哪履哪哪哪哪目北
北� Fun噮o    硃960VldEsp � Autor � Daher            � Data � 04/07/13 潮�
北媚哪哪哪哪哪拍哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪牧哪哪哪聊哪哪哪哪拇北
北� Descri噮o |Ajusta SXB;                                                 潮�
北媚哪哪哪哪哪拍哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪拇北
北� Uso       �                                                            潮�
北媚哪哪哪哪哪拍哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪拇北
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌
/*/
Function p960VldEsp()
LOCAL nOrdBQ1 := BQ1->(IndexOrd())
LOCAL nRecBQ1 := BQ1->(Recno())
LOCAL lRet := (Vazio() .or. ExistCpo("BAQ",M->BQ1_CODINT+M->BQ1_CODESP))  
If lRet     
	BQ1->(DbSetORder(1))
	lRet := !BQ1->(MsSeek(xFilial("BQ1")+M->BQ1_CODIGO+M->BQ1_CODESP))
	If !lRet
	     help('',1,"EXISTCHAV")
	Endif  
Endif
BQ1->(DbGoTo(nRecBQ1)) 
BQ1->(DbSetOrder(nOrdBQ1))
return lRet
