
#include "PLSMGER.CH"
#include "PROTHEUS.CH"
#define STR0001  "Reembolso"
#define STR0002  "Procedimentos"
#define STR0003  "Procedimentos"
#define STR0004  "Reembolsos em aberto"
#define STR0005  "Reembolsos pagos"
#define STR0006  "Reembolsos"
#define STR0007  "Legenda"
#define STR0008  "Titulo do Reembolso j� baixado!!"
#define STR0009  "Registro n�o existe"
#define k_imprimir 7

// Rever o dado do campo E1_TIPO -> Nota de Credito
/*/
������������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  � PLSA987 � Autor � Marcos Alves         � Data � 27.04.2004 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Cadastro de Reembolso                                      ���
�������������������������������������������������������������������������Ĵ��
���Uso       �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function PLSA987
//��������������������������������������������������������������������������Ŀ
//� Define as cores do status do Reembolso                                   �
//����������������������������������������������������������������������������

Local aCores 		:= {{'PlA987Sts(BKD_CHVSE1,1)',"BR_VERMELHO"},;
                   		{'!PlA987Sts(BKD_CHVSE1,1)' ,"BR_VERDE"}}
//��������������������������������������������������������������������������Ŀ
//� Define o cabecalho da tela de atualizacoes                               �
//����������������������������������������������������������������������������
PRIVATE aRotina   := MenuDef()
Private cFilter	  := ""
//��������������������������������������������������������������������������Ŀ
//� Endereca a funcao de BROWSE                                              �
//����������������������������������������������������������������������������
BKD->(DbSetOrder(1))
BKD->(DbGoTop())
BKD->(mBrowse(006,001,022,075,"BKD",,,,,, aCores))
//��������������������������������������������������������������������������Ŀ
//� Fim da Rotina Principal                                                  �
//����������������������������������������������������������������������������
Return
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � PL987MOV � Autor � Marcos Alves          � Data � 29.04.04 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Movimentacao do Cadastro de Reembolso                      ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function PL987MOV(cAlias,nReg,nOpc)
Local I__f := 0
//��������������������������������������������������������������������������Ŀ
//� Define Variaveis...                                                      �
//����������������������������������������������������������������������������
Local nOpca	   := 0
Local cPrefixo := Space(TamSx3("E1_PREFIXO")[1])
Local cNumTit  := Space(TamSx3("E1_NUM")[1])
Local cTipTit  := Space(TamSx3("E1_TIPO")[1])
Local dvencto  := ctod('')
Local cCodCli  := Space(TamSx3("E1_CLIENTE")[1])
Local cLoja    := Space(TamSx3("E1_LOJA")[1])
LOCAL cCodTabR := ""
LOCAL cDesTabR := ""
LOCAL nUSReem  := 0       
LOCAL aRetAux
LOCAL oDlg
Local aPosObj   := {}
Local aObjects  := {}
Local aSize     := {}
Local aInfo     := {}
LOCAL aRetPto
Local nFor		:= 0
Local nValorR	:= 0
LOCAL nTotal  	:= 0
LOCAL nVlrPag 	:= 0
LOCAL nVlrRem 	:= 0

PRIVATE aChave := {}
PRIVATE oSay 
PRIVATE aCols   := {}
PRIVATE aHeader  := {}
//��������������������������������������������������������������������������Ŀ
//� Define Variaveis Tipo 01                                                 �
//����������������������������������������������������������������������������
PRIVATE oGet01
PRIVATE aCab01
PRIVATE aCols01
PRIVATE aVet01 := {}
PRIVATE cExpAdv01 := Space(10)
//��������������������������������������������������������������������������Ŀ
//� Define Variaveis para Enchoice...                                        �
//����������������������������������������������������������������������������
PRIVATE oEnchoice
PRIVATE aTELA[0][0]
PRIVATE aGETS[0]
//��������������������������������������������������������������������������Ŀ
//� Monta aCols e aHeader...                                                 �
//����������������������������������������������������������������������������
If (nOpc == K_Alterar.OR.nOpc == K_Excluir).AND.PlA987Sts(BKD->BKD_CHVSE1,1)
	MsgInfo(STR0008) //"Titulo do Reembolso j� baixado!!"
	Return Nil
EndIf
Store Header "BKE" TO aCab01 For .T.
If nOpc == K_Incluir
	Copy "BKD" TO Memory Blank
	Store COLS Blank "BKE" TO aCols01 FROM aCab01
Else
	Copy "BKD" TO MEMORY
	BKE->(DbSetOrder(1))
	If BKE->(DbSeek(xFilial("BKE")+BKD->BKD_CODRBS))
		Store COLS "BKE" TO aCols01 FROM aCab01 VETTRAB aVet01 While (BKE->BKE_FILIAL+BKE->BKE_CODRBS) == (BKD->BKD_FILIAL+BKD->BKD_CODRBS)
	Else
		Store COLS Blank "BKE" TO aCols01 FROM aCab01
	EndIf
EndIf
//��������������������������������������������������������������������������Ŀ
//� Posiciona BA1                                                            �
//����������������������������������������������������������������������������
If  nOpc == K_Alterar .OR. nOpc == K_Excluir
    PLSA987USR()
Endif    
//��������������������������������������������������������������������������Ŀ
//� Nova implementacao - Tulio Cesar em 31-05-2004                           �
//�                                                                          �
//� Verificar e montar dados para reembolso.                                 �
//����������������������������������������������������������������������������
If nOpc == K_Incluir
   aRetAux := PLSXVLDCAL(dDataBase,plsintpad(),.T.,"","")

   If ! aRetAux[1]
      Help("",1,"PLSA500CAL")
      Return
   Endif                                      
   //��������������������������������������������������������������������������Ŀ
   //� Verifica se foi parametrizado a tabela de reembolso padrao...            �
   //����������������������������������������������������������������������������
   BA0->(DbSetOrder(1))
   If BA0->(DbSeek(xFilial("BA0")+PLSINTPAD())) .And. Empty(BA0->BA0_TBRFRE)
      Aviso( "Tabela de Reembolso", ;
              "Nao foi parametrizado a tabela padrao para reembolso no cadastro da Operadora Padrao.",;
              { "Ok" }, 2 ) 	                                                                    
      Return                                      
   Else   
      //��������������������������������������������������������������������������Ŀ
      //� Verifica se a tabela parametrizada e valida...                           �
      //����������������������������������������������������������������������������
      BF8->(DbSetOrder(1))
      If ! BF8->(DbSeek(xFilial("BF8")+PLSINTPAD()+BA0->BA0_TBRFRE))
         Aviso( "Tabela de Reembolso", ;
                "Tabela de Reembolso informada na Operadora Padrao invalida.",;
                { "Ok" }, 2 ) 	                                               
         Return       
      Else
         //��������������������������������������������������������������������������Ŀ
         //� Monta dados da tabela...                                                 �
         //����������������������������������������������������������������������������
         cCodTabR := BF8->BF8_CODIGO
         cDesTabR := BF8->BF8_DESCM 
         //��������������������������������������������������������������������������Ŀ
         //� Busca a U.S do mes...                                                    �
         //����������������������������������������������������������������������������
          nUSReem := PlsReUsRem(aRetAux,"","","",dDataBase)
          If nUSReem == 0                      
            Aviso( "U.S de Reembolso", ;
                   "Indice de U.S para reembolso n�o encontrado.",;
                   { "Ok" }, 2 )  
            return                        
          Endif
      Endif
   Endif   
Endif   
//��������������������������������������������������������������������������Ŀ
//� Define Dialogo...                                                        �
//����������������������������������������������������������������������������
aSize := MsAdvSize()
aObjects := {}       
AAdd( aObjects, { 1, 1, .T., .T., .F. } )
AAdd( aObjects, { 1, 1, .T., .T., .T. } )

aInfo := { aSize[ 1 ], aSize[ 2 ], aSize[ 3 ], aSize[ 4 ], 3, 3 }
aPosObj := MsObjSize( aInfo, aObjects )

DEFINE MSDIALOG oDlg TITLE STR0001 FROM aSize[7],0 To aSize[6],aSize[5] OF GetWndDefault() Pixel
//��������������������������������������������������������������������������Ŀ
//� Monta Enchoice...                                                        �
//����������������������������������������������������������������������������
Zero();oEnchoice := MsMGet():New(cAlias,nReg,nOpc,,,,,aPosObj[1],,,,,,oDlg,,,.F.)

//��������������������������������������������������������������������������Ŀ
//� Define Folders...                                                        �
//����������������������������������������������������������������������������
@ aPosObj[2][1],aPosObj[2][2] FOLDER oFolder SIZE aPosObj[2][3],aPosObj[2][4] OF oDlg PIXEL	PROMPTS	STR0002
//��������������������������������������������������������������������������Ŀ
//� Monta GetDados...                                                        �
//����������������������������������������������������������������������������
oGet01 := TPLSBrw():New(3,3,aPosObj[2][3]-4,aPosObj[2][4]-15,nil  ,oFolder:aDialogs[1],nil, nil, nil ,nil   ,nil, .T.   ,nil  ,.T.   ,nil   ,aCab01,aCols01,.F.,"BKE",nOpc,STR0003,nil,nil,nil,nil,nil,nil,nil,nil )
oGet01:aVetTrab := aClone(aVet01)
//��������������������������������������������������������������������������Ŀ
//� Nova implementacao - Tulio Cesar em 31-05-2004                           �
//�                                                                          �
//� Sugerir automaticamente a Tabela e a U.S que sera considerada para       �
//� o calculo do reembolso.                                                  �
//����������������������������������������������������������������������������
If BKD->(FieldPos("BKD_TABREM")) > 0 .And. BKD->(FieldPos("BKD_USREM")) > 0 .And. nOpc == K_Incluir
   M->BKD_TABREM := cCodTabR
   M->BKD_DESTAB := cDesTabR
   M->BKD_USREM  := nUSReem
   M->BKD_ANOBAS := aRetAux[4]
   M->BKD_MESBAS := aRetAux[5]
Endif
//��������������������������������������������������������������������������Ŀ
//� Ativa o Dialogo...                                                       �
//����������������������������������������������������������������������������

ACTIVATE MSDIALOG oDlg ON INIT Eval({ || EnchoiceBar(oDlg,{|| nOpca := 1,If(Obrigatorio(oEnchoice:aGets,oEnchoice:aTela),oDlg:End(),nOpca:=2),If(nOpca==1,oDlg:End(),.F.) },{||oDlg:End()},.F.,{})  })
//��������������������������������������������������������������������������Ŀ
//� Rotina de gravacao dos dados...                                          �
//����������������������������������������������������������������������������
If nOpca == K_OK
   If nOpc <> K_Visualizar
      If Existblock("PL987BOK")
      	Execblock("PL987BOK",.f.,.f.,{oGet01}) 
      Endif
      
      //�������������������������������������������������������������������������������Ŀ
	  //� Atualiza os totais do cabecalho...                                            �
	  //���������������������������������������������������������������������������������
	  For nFor := 1 To Len(oGet01:aCols)
	      If oGet01:aCols[nFor,Len(oGet01:aHeader) + 1] == .F.
	         nTotal  += oGet01:FieldGet("BKE_VLRCST",nFor)   
    	     nVlrPag += oGet01:FieldGet("BKE_VLRPAG",nFor)
	         nVlrRem += oGet01:FieldGet("BKE_VLRRBS",nFor)
	      Endif   
	  Next
	  //�������������������������������������������������������������������������������Ŀ
	  //� Atualiza dados apos a validacao...                                            �
	  //���������������������������������������������������������������������������������
		
	  M->BKD_VLRPAG := nVlrPag
	  M->BKD_VLRREM := nVlrRem
	  M->BKD_VLRCST := nTotal
      
      M->BKD_CODRBS := PLSA625Cd("BKD_CODRBS","BKD",1,"D_E_L_E_T_"," ")                     
      cChvSE1 := BKD->BKD_CHVSE1
      PLUPTENC("BKD",nOpc)   	
      aChave := {}              
      aadd(aChave,{"BKE_CODRBS",M->BKD_CODRBS})
      oGet01:Grava(aChave) 
	  //��������������������������������������������������������������������������Ŀ
      //� Gravar titulo do financeiro											   �
	  //����������������������������������������������������������������������������
	  If BKD->(FieldPos("BKD_FORPAG")) > 0 .and. ;
		 M->BKD_FORPAG == '1'
		 If  nOpc == K_Incluir
		     BA3->(dbSetorder(01)) 
			 BA3->(dbSeek(xFilial("BA3")+BKD->(BKD_CODINT+BKD_CODEMP+BKD_MATRIC)))
			 aRetCli := PLSAVERNIV(BA3->BA3_CODINT,BA3->BA3_CODEMP,BA3->BA3_MATRIC,IF(BA3->BA3_TIPOUS=="1","F","J"),BA3->BA3_CONEMP,BA3->BA3_VERCON,BA3->BA3_SUBCON,BA3->BA3_VERSUB)
             If  aRetCli[1,1] <> "ZZZZZZ" //Definicao do plsaverniv                                         
                 cCodCli  := aRetCli[1,1]                                        
                 cLoja    := aRetCli[1,2]                                        
                 
                 cNat     := GetNewPar("MV_PLSNTRE",'"PLS"')
                 If !('"' $ cNat .Or. "'" $ cNat)
                 	cNat := '"'+Alltrim(cNat)+'"'
                 Endif                           
	             cNat     := Eval({|| &cNat })
	             
	             cPrefixo := GetNewPar("MV_PLSPFRE",'"RLE"')  
                 If !('"' $ cPrefixo .Or. "'" $ cPrefixo)
                 	cPrefixo := '"'+Alltrim(cPrefixo)+'"'
                 Endif                           
	             cPrefixo := Eval({|| &cPrefixo })
	             
			     cNumTit  := PLSE1NUM(cPrefixo)                                                            
			     cTipTit  := GetNewPar("MV_PLSNCRE","NCC")
			     dvencto  := M->BKD_DATVEN //LastDay(CToD("01"+"/"+BKD->BKD_MESBASE+"/"+Subs(BKD->BKD_ANOBASE,3,2)))

                 If ExistBlock("PLS987CLI")
                    aRetPto := ExecBlock("PLS987CLI",.F.,.F.,{cCodCli,cLoja,cNat,cPrefixo,cNumTit,cTipTit,dvencto})
                    
                    cCodCli := aRetPto[1]
                    cLoja   := aRetPto[2]
                    cNat    := aRetPto[3]
                    cPrefixo:= aRetPto[4]
                    cNumTit := aRetPto[5]
                    cTipTit := aRetPto[6]
                    dvencto := aRetPto[7]
                 Endif   

		         PLA987SE1(cPrefixo,cNumTit,cCodCli,cLoja,cNat,cTipTit,dVencto,BKD->BKD_CODINT,BKD->BKD_CODEMP,BKD->BKD_MATRIC,nOpc,BKD->BKD_VLRREM)
		         BKD->(RecLock("BKD",.F.))
		         BKD->BKD_CHVSE1 := cPrefixo+cNumTit+'1'+cTipTit
		         BKD->(MsUnLock())         
		     Else
		         msgalert("Nao foi possivel gerar o titulo a receber porque o nivel de cobranca nao foi encontrado para este ususario.")
			 EndIf
         Else
             SE1->(dbSetOrder(1))
             If  SE1->(dbSeek(xFilial("SE1")+cChvSE1)) 
                 If  SE1->(RecLock("SE1",.F.))
                     SE1->(dbDelete())
                     SE1->(msUnLock())
                 Endif
             Endif
         Endif
      Endif 
   Endif   
EndIf
//��������������������������������������������������������������������������Ŀ
//� Fim da Rotina...                                                         �
//����������������������������������������������������������������������������
Return Nil
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � PLA987SE1  � Autor � Marcos Alves        � Data � 05.06.04 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Cria um titulo de Nota de credito para o cliente           ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
 �����������������������������������������������������������������������������
*/
Static Function PLA987SE1(cPrefixo,cNumTit,cCodCli,cLoja,cNat,cTipTit,dVencto,cCodInt,cCodEmp,cMatric,nOpc,nValor)
DEFAULT nValor := 0
If nOpc == K_Incluir

   SA1->(DbSetOrder(1))
   SA1->(DbSeek(xFilial("SA1")+cCodCli+cLoja))

   SE1->(RecLock("SE1",.T.))
   SE1->E1_FILIAL   := xFilial("SE1")
   SE1->E1_TIPO     := cTipTit
   SE1->E1_CLIENTE  := cCodCli 	
   SE1->E1_LOJA     := cLoja 	
   SE1->E1_NATUREZ  := cNat
   SE1->E1_EMISSAO  := dDataBase
   SE1->E1_EMIS1    := dDataBase
   SE1->E1_PARCELA  := "1"
   SE1->E1_SITUACA  := "0"
   SE1->E1_MOEDA    := 1
   SE1->E1_OCORREN  := "01"
   SE1->E1_FLUXO    := "S"
   SE1->E1_STATUS   := "A"
   SE1->E1_PROJPMS  := "2"
   SE1->E1_VENCORI  := dVencto
   SE1->E1_VENCTO   := dVencto
   SE1->E1_ANOBASE  := BKD->BKD_ANOBAS
   SE1->E1_MESBASE  := BKD->BKD_MESBAS
   SE1->E1_PREFIXO  := cPrefixo
   SE1->E1_NUM      := cNumTit
   SE1->E1_VENCREA  := DataValida(dVencto)
   SE1->E1_VLCRUZ   := nValor
   SE1->E1_IRRF	    := 0
   SE1->E1_VALOR    := nValor
   SE1->E1_NOMCLI   := SA1->A1_NREDUZ
   SE1->E1_SALDO    := nValor
   SE1->E1_DECRESC  := 0
   SE1->E1_SDDECRE  := 0
   SE1->E1_ACRESC   := 0
   SE1->E1_SDACRES  := 0
   SE1->E1_VALLIQ   := 0
   SE1->E1_NUMBCO   := ""
   SE1->E1_PLNUCOB  := ""
   SE1->E1_VALJUR   := 0
   SE1->E1_PORCJUR  := 0
   SE1->E1_CODINT   := cCodInt
   SE1->E1_CODEMP   := cCodEmp
   SE1->E1_MATRIC 	:= cMatric
   SE1->E1_MULTNAT  := "2"
   If SE1->( FieldPos("E1_PLORIG") ) > 0
	   SE1->E1_PLORIG := '7'
   Endif
   SE1->(MsUnLock())   
Endif

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � PlA987Sts  � Autor � Marcos Alves        � Data � 05.06.04 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Retorna o status do titulo (NCC), apresentar no browse     ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Modulo do PLS                                              ���
�������������������������������������������������������������������������Ĵ��
���Parametros� cChaveSE1 	- Chave de pesquisa do SE1					  ���
���          � nOrder		- Ordem de pesquisa na tabela SE1             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
 �����������������������������������������������������������������������������
*/
Function PlA987Sts(cChaveSE1,nOrder)
Local lRet	:= .F.
SE1->(dbSelectArea("SE1"))
SE1->(dbSetOrder(nOrder))
SE1->(dbSeek(xFilial("SE1")+cChaveSE1))
If  SE1->(Found()) .AND. SE1->E1_SALDO <> SE1->E1_VALOR
	lRet:=.T.
EndIf	
Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � PlA987Sts  � Autor � Marcos Alves        � Data � 05.06.04 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Retorna o status do titulo (NCC), apresentar no browse     ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Modulo do PLS                                              ���
�������������������������������������������������������������������������Ĵ��
���Parametros� cChaveSE1 	- Chave de pesquisa do SE1					  ���
���          � nOrder		- Ordem de pesquisa na tabela SE1             ���
�������������������������������������������������������������������������Ĵ��
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
 �����������������������������������������������������������������������������
*/
Function PLA987Leg
Local aLegenda := { {"BR_VERDE",	STR0004},; //"Reembolsos sem aberto"
					{"BR_VERMELHO",	STR0005}}  //"Reembolsos pagos"
BrwLegenda(STR0006,STR0007,aLegenda) //"Reembolsos"###"Legenda"
Return .T.

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � A987Usr    � Autor � Daher			    � Data � 22.07.05 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Preenche campo nome do usuario com o nome do usuario e do  ���
���          � titular.                                                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function A987Usr()      
LOCAL aDadUsr := PLSGETUSR()   

If Len(aDadUsr) > 0
	M->BKD_NOMUSR :=aDadUsr[6]
	M->BKD_CODINT :=subs(aDadUsr[2],1,4)
	M->BKD_CODEMP :=subs(aDadUsr[2],5,4)
	M->BKD_MATRIC :=subs(aDadUsr[2],9,6)
	M->BKD_TIPREG :=subs(aDadUsr[2],15,2)
	M->BKD_DIGITO :=subs(aDadUsr[2],17,1)
	M->BKD_NOMEMP :=aDadUsr[7]
Endif                 

Return(BA1->BA1_NOMUSR)
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � PLSA987PRO � Autor � Michele Tatagiba    � Data � 22.07.02 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Preenche campo nome do usuario com o nome do usuario e do  ���
���          � titular.                                                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function PLSA987PRO()
//��������������������������������������������������������������������������Ŀ
//� Define variaveis da rotina...                                            �
//����������������������������������������������������������������������������
LOCAL aRet
LOCAL nValorR
LOCAL nFor     
LOCAL nTotal  := 0
LOCAL nVlrPag := 0
LOCAL nVlrRem := 0
LOCAL aDadUsr := PLSGETUSR()   
LOCAL lForcado:= .F.
LOCAL cTabRem := ""
LOCAL nUsReem := 0                                 
LOCAL aRetAux := PLSXVLDCAL(dDataBase,plsintpad(),.T.,"","")
If ! aRetAux[1]
   Help("",1,"PLSA500CAL")
   Return
Endif
//��������������������������������������������������������������������������Ŀ
//� Verifica se foi informado o usuario...                                   �
//����������������������������������������������������������������������������
If Empty(aDadUsr)
   Aviso( "Usuario", ;
           "Informe o usuario antes de informar o procedimento.",;
           { "Ok" }, 2 ) 	                                                                    
   Return(.F.)                                               
Endif
//��������������������������������������������������������������������������Ŀ
//� Verifica se o evento e valido...                                         �
//����������������������������������������������������������������������������
aRet := PLSVLDPTP(M->BKE_CODTAB,M->BKE_CODPRO)
If ! aRet[1]
   Aviso( "Procedimento", ;
           "Procedimento invalido.",;
           { "Ok" }, 2 ) 	                                                                    
   Return(.F.)
Endif   
//��������������������������������������������������������������������������Ŀ
//� Verifica se ele pode ter reembolso...                                    �
//����������������������������������������������������������������������������
If BR8->(FieldPos("BR8_PODREM")) > 0
   If BR8->BR8_PODREM == "0"
      Aviso( "Procedimento", ;
              "Procedimento nao permite reembolso (Tabela Padrao)",;
              { "Ok" }, 2 ) 	                                                                    
      Return(.F.)
   Endif
Endif
//��������������������������������������������������������������������������Ŀ
//� Verifica se ele tem cobertura...                                         �
//����������������������������������������������������������������������������

aRet := PLSAUTP(M->BKD_DATA,;
                "",;
                M->BKE_CODTAB,;
                M->BKE_CODPRO,;
                M->BKE_QTDPRO,;
                aDadUsr,;
                0,;
                nil,;
                "1",;
                .F.,;
                "",;
                .T.,;
                NIL,;
                .F.,;
                "",;
                "",;
                M->BKD_ANOBAS,;
                M->BKD_MESBAS,;
                nil,;
                nil,;
                nil,;
                .F.,;
                nil,;
                nil,;
                nil)

If ! aRet[1]
   If ! PLSMOVCRI("1",{M->BKE_CODTAB,M->BKE_CODPRO,BR8->BR8_DESCRI,""},aClone(aRet[2]),.T.)
      Return(.F.)                                             
   Else
     lForcado := .T.
   Endif   
Endif
//��������������������������������������������������������������������������Ŀ
//� Verifica se esta mais de uma vez no browse...                            �
//����������������������������������������������������������������������������
If Type('oGet01') == "O" .And. ! PLSVLDGD({"BKE_CODTAB", "BKE_CODPRO"},.T.,oGet01:aCols)  
   Aviso( "Procedimento", ;
           "Mesmo procedimento informado mais de uma vez.",;
           { "Ok" }, 2 ) 	                                                                    
   Return(.F.)
Endif
//�������������������������������������������������������������������������������Ŀ
//� Testa de novo existencia de U.S e tabela, possiveis customizacoes invalidas   �
//���������������������������������������������������������������������������������
If Empty(M->BKD_TABREM) .Or. M->BKD_USREM == 0
   Aviso( "Tabela de Reembolso e U.S de Reembolso", ;
           "Tabela de Reembolso e U.S de Reembolso invalidas para a valorizacao do procedimento.",;
           { "Ok" }, 2 ) 	                                                                    
   Return(.F.)                                                                                 
Endif    

cTabRem := M->BKD_TABREM
nUsReem := PlsReUsRem(aRetAux,;
					  alltrim(M->BKD_CODCRE),;
					  alltrim(M->BKE_CODTAB),;
					  alltrim(M->BKE_CODPRO),;
					  M->BKD_DATA)  
//�������������������������������������������������������������������������������Ŀ
//� Esta tudo integro, vamos valorizar o procedimento...                          �
//���������������������������������������������������������������������������������
nValorR := PLSVLRTREM(PLSINTPAD(),cTabRem,M->BKD_USUARI,M->BKD_DATA,"",M->BKE_CODTAB,M->BKE_CODPRO,M->BKE_QTDPRO,;
                      nUsReem)
                      
M->BKE_VLRCST := nValorR
M->BKE_DESCRI := BR8->BR8_DESCRI                                

if BKE->(FieldPos("BKE_TABREM")) > 0 .and. BKE->(FieldPos("BKE_USREM"))> 0
	M->BKE_TABREM := cTabRem
	M->BKE_USREM  := nUsReem
Endif

If lForcado
   M->BKE_FORCAD := "1"
Endif

If M->BKE_VLRPAG > 0
   M->BKE_VLRRBS := IF(M->BKE_VLRPAG<M->BKE_VLRCST,M->BKE_VLRPAG,M->BKE_VLRCST)                      
Endif                 
//�������������������������������������������������������������������������������Ŀ
//� Atualiza os totais do cabecalho...                                            �
//���������������������������������������������������������������������������������
For nFor := 1 To Len(oGet01:aCols)
    If nFor == n
       nTotal  += nValorR
       nVlrPag += M->BKE_VLRPAG
       nVlrRem += M->BKE_VLRRBS
    Else                	
       nTotal  += oGet01:FieldGet("BKE_VLRCST",nFor)   
       nVlrPag += oGet01:FieldGet("BKE_VLRPAG",nFor)
       nVlrRem += oGet01:FieldGet("BKE_VLRRBS",nFor)
    Endif   
Next
//�������������������������������������������������������������������������������Ŀ
//� Atualiza dados apos a validacao...                                            �
//���������������������������������������������������������������������������������

M->BKD_VLRPAG := nVlrPag
M->BKD_VLRREM := nVlrRem
M->BKD_VLRCST := nTotal

lRefresh := .T.
oEnchoice:Refresh()
//�������������������������������������������������������������������������������Ŀ
//� Fim da rotina...                                                              �
//���������������������������������������������������������������������������������
Return(.T.)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � PLSA987USR � Autor � Daher			    � Data � 22.07.05 ���
�������������������������������������������������������������������������Ĵ��
���Descricao �   														  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

Function PLSA987USR
LOCAL aRetFun  
LOCAL __dData

LOCAL cMatric    := alltrim(M->BKD_USUARI)
LOCAL lOk        := .F.

If Empty(M->BKD_DATA)
   __dData := dDataBase
Else
   __dData := M->BKD_DATA
Endif   

aRetFun := PLSDADUSR(cMatric,"1",.T.,__dData,nil,nil,"")
lOK     := aRetFun[1]
//��������������������������������������������������������������������������Ŀ
//� se nao achar procura pela matricula antiga...                            �
//����������������������������������������������������������������������������
If ! lOK 

   aRetFun := PLSDADUSR(cMatric,"2",.T.,__dData,nil,nil,"")
   lOK     := aRetFun[1]   
   If lOK 
      cMatric := aRetFun[2]
      aDadUsr := PLSGETUSR()   
   Endif   

Else
   aDadUsr := PLSGETUSR()   
Endif

If ! aRetFun[1]
   Aviso( "Usuario","Usuario invalido",{ "Ok" }, 2 ) 	                                                                    
   lOk := .F.
Else
	//��������������������������������������������������������������������������Ŀ
	//� Preenche as variaveis de memoria...                            			 �
	//����������������������������������������������������������������������������
	A987Usr()
Endif

Return(lOk)                                                                 


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � PlsReUsRem � Autor � Daher			    � Data � 22.05.05 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Retorna Us de reembolso									  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
function PlsReUsRem(aRetAux,cCodPre,cCodPad,cCodPro,dDatAnalise)
LOCAL nRet		 := 0 
Local nFor
LOCAL aOrdensNiv := { "BMC","BMB","BFM" }
LOCAL nForNiv    := 1              
LOCAL lInfPre    := if(empty(cCodPre),.F.,.T.)
LOCAL lInfPro    := if(empty(cCodPro),.F.,.T.)
LOCAL aNiveis    := if(empty(cCodPro),{},PLSESPNIV(cCodPad))
LOCAL nNiveis    := if(empty(cCodPro),0,(aNiveis[1]+1))     
LOCAL cChave     := ""
LOCAL cCodAux    := ""

For nForNiv := 1 To Len(aOrdensNiv)
    If      aOrdensNiv[nForNiv] $ "BMC" .and. lInfPre .and. lInfPro
		//�������������������������������������������������������������������������������Ŀ
		//� Posiciono no prestador...                                                     �
		//���������������������������������������������������������������������������������		    	
    	BK6->(DbSetOrder(3))
 		If BK6->(MsSeek(xFilial("BK6")+cCodPre))                                            
			//�������������������������������������������������������������������������������Ŀ
			//� Operadora x Tipo Prestador xProcedimento...                                   �
			//���������������������������������������������������������������������������������    	
	    	 For nFor := 1 To nNiveis
                
                If nFor == 1
                   cChave  := xFilial("BMC")+PLSINTPAD()+BK6->(BK6_TIPPRE)+cCodPad+cCodPro
                Else
                   cCodAux := Subs(cCodPro,aNiveis[2,(nFor-1),1],aNiveis[2,(nFor-1),2])
                   cCodAux := cCodAux+Space(Len(BMC->BMC_CDNV01)-Len(cCodAux))
                   cChave  := xFilial("BMC")+PLSINTPAD()+BK6->(BK6_TIPPRE)+cCodPad+cCodAux
                Endif
                     
		    	BMC->(DbSetOrder(nFor))                       
		    	If BMC->(DbSeek(cChave))
		    		while BMC->(!Eof()) .and. BMC->(BMC_CODINT+BMC_TIPPRE) == PLSINTPAD()+BK6->(BK6_TIPPRE)  					      			 
						//�������������������������������������������������������������������������������Ŀ
						//� Pego o Valor que esta na vigencia											  �
						//���������������������������������������������������������������������������������    	
		      			 If (empty(BMC->BMC_VIGDE)  .or. BMC->BMC_VIGDE  <= dDatAnalise) .and. ;
		                    (empty(BMC->BMC_VIGATE) .or. BMC->BMC_VIGATE >= dDatAnalise) 
		        				nRet := BMC->BMC_VLUSRE             
		        		 		Exit
		        		 Endif         
		        		 BMC->(DbSkip())
		        	enddo
		    	Endif
	    	Next
	    Endif	
    
    Elseif  aOrdensNiv[nForNiv] $ "BMB" .and. lInfPre                                                                  
		//�������������������������������������������������������������������������������Ŀ
		//� Posiciono no prestador...                                                     �
		//���������������������������������������������������������������������������������		    	
    	BK6->(DbSetOrder(3))
 		If BK6->(MsSeek(xFilial("BK6")+cCodPre))                                            
			//�������������������������������������������������������������������������������Ŀ
			//� Operadora x Tipo Prestador 													  �
			//���������������������������������������������������������������������������������    	
	    	BMB->(DbSetOrder(1))                       
	    	If BMB->(DbSeek(xFilial("BMB")+PLSINTPAD()+BK6->(BK6_TIPPRE)))
	        	nRet := BMB->BMB_VLUSRE
	        	Exit
	    	Endif
	    Endif	
	    
	Elseif  aOrdensNiv[nForNiv] $ "BFM"
		//�������������������������������������������������������������������������������Ŀ
		//� U.S Mensal				 													  �
		//���������������������������������������������������������������������������������		
		BFM->(DbSetOrder(1))                                                         
		If BFM->(DbSeek(xFilial("BFM")+PLSINTPAD()+aRetAux[4]+aRetAux[5]))
			nRet := BFM->BFM_VALRDA 
			Exit
		endif
	Endif
Next

	    //����������������������������������������������������������������������������������������Ŀ
		//� Ponto de Entrada para modifica��o do valor da US conforme regras especificas do cliente�
		//������������������������������������������������������������������������������������������

if ExistBlock("PL987US")
	nRet:=ExecBlock("PL987US",.F.,.F.,{aOrdensNiv[nForNiv],nRet})
Endif
                                 
	                                                                                                       
return  nRet                                                    

function PlsVldPeRe()          
Local nFor
LOCAL nTotal  := 0
LOCAL nVlrPag := 0 
LOCAL nVlrRem := 0
LOCAL nValorR := M->BKE_VLRCST

If BKE->(FieldPos("BKE_PERREM")) > 0 .And. M->BKE_PERREM > 0
	M->BKE_VLRRBS := M->BKE_VLRCST*M->BKE_PERREM/100
ElseIf BKE->(FieldPos("BKE_PREPAG")) > 0 .And. M->BKE_PREPAG > 0
	M->BKE_VLRRBS := M->BKE_VLRPAG*M->BKE_PREPAG/100
Else
	M->BKE_VLRRBS := IIf(M->BKE_VLRPAG<M->BKE_VLRCST,M->BKE_VLRPAG,M->BKE_VLRCST)
Endif           

//�������������������������������������������������������������������������������Ŀ
//� Atualiza os totais do cabecalho...                                            �
//���������������������������������������������������������������������������������
For nFor := 1 To Len(oGet01:aCols)
    If nFor == n
       nTotal  += nValorR
       nVlrPag += M->BKE_VLRPAG
       nVlrRem += M->BKE_VLRRBS
    Else                	
       nTotal  += oGet01:FieldGet("BKE_VLRCST",nFor)   
       nVlrPag += oGet01:FieldGet("BKE_VLRPAG",nFor)
       nVlrRem += oGet01:FieldGet("BKE_VLRRBS",nFor)
    Endif   
Next
//�������������������������������������������������������������������������������Ŀ
//� Atualiza dados apos a validacao...                                            �
//���������������������������������������������������������������������������������

M->BKD_VLRPAG := nVlrPag
M->BKD_VLRREM := nVlrRem
M->BKD_VLRCST := nTotal

lRefresh := .T.
oEnchoice:Refresh()


return .T.

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �MenuDef   � Autor � Darcio R. Sporl       � Data �08/01/2007���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Utilizacao de menu Funcional                               ���
���          �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �Array com opcoes da rotina.                                 ���
�������������������������������������������������������������������������Ĵ��
���Parametros�Parametros do array a Rotina:                               ���
���          �1. Nome a aparecer no cabecalho                             ���
���          �2. Nome da Rotina associada                                 ���
���          �3. Reservado                                                ���
���          �4. Tipo de Transa��o a ser efetuada:                        ���
���          �		1 - Pesquisa e Posiciona em um Banco de Dados           ���
���          �    2 - Simplesmente Mostra os Campos                       ���
���          �    3 - Inclui registros no Bancos de Dados                 ���
���          �    4 - Altera o registro corrente                          ���
���          �    5 - Remove o registro corrente do Banco de Dados        ���
���          �5. Nivel de acesso                                          ���
���          �6. Habilita Menu Funcional                                  ���
�������������������������������������������������������������������������Ĵ��
���   DATA   � Programador   �Manutencao efetuada                         ���
�������������������������������������������������������������������������Ĵ��
���          �               �                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function MenuDef()
Local K_Imprimir := 7
Private aRotina  := {	{ STRPL01          , 'AxPesqui' , 0, K_Pesquisar  	, 0, .F.},; //'Pesquisar'
						{ STRPL02          , 'PL987MOV' , 0, K_Visualizar 	, 0, Nil},; //'Visualizar'
						{ STRPL03          , 'PL987MOV' , 0, K_Incluir    	, 0, Nil},; //'Incluir'
						{ "Nao Disponivel" , 'NAODISP'  , 0, K_Alterar    	, 0, Nil},;
						{ STRPL05          , 'PL987MOV' , 0, K_Excluir  	, 0, Nil},; // Excluir
						{ STR0007          , 'PLA987Leg', 0, K_Visualizar 	, 0, .F.} } // Legenda

If ExistBlock("PL987IMP")
	aPtoEntrada := ExecBlock("PL987IMP",.F.,.F.)
	AaDd( aRotina,{ aPtoEntrada[1] ,aPtoEntrada[2] , 0 , K_Imprimir } )
Endif

Return(aRotina)
