#include "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "plsacanjb.ch"


/*/
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    � PLSVSCAN � Autor � Renan Martins    � Data � 14/09/2015    ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Execu��o gen�ruica de cancelamento de protocolos           ����
��������������������������������������������������������������������������ٱ��
��� *cAlias - Alias da tabela / aStatus - Status que deseja buscar na ta-  ���
��� bela(cAlias)                                                           ���
��� *cAliCpo - Nome do campo da tabela que possui o status                  ���
��� *cDataCpo - Nome do campo da tabela que possui o campo data para veri- ���
��� fica��o                                                                ���
��� *cStatDs - Status desejado ap�s a atualiza��o                          ���
��� *cMotCpo - Campo de descri��o do motivo de cancelamento                ���
��� *cCodF3 - Se a tabela possui campo F3 que necessita de preenchimento   ���
��� (motivo padr�o),indique o valor que deve ser preenchido.               ���
��� *cCodMotCpo - Se a tabela possui campo F3 que necessita de preenchimento���
��� (motivo padr�o), indique o nome do campo                               ���  
��� *cMsgObs - Informe a mensagem que deve ser salva no campo cMotCpo      ���
��� (motivo padr�o),indique o valor que deve ser preenchido.               ���
��� *cNomParam - Se a quantidade de dias vier de um par�metro qualquer,    ���
��� informe o nome deste par�metro                                         ���
��� *cDatCanc - Se possuir, informe o campo em que deve ser salvo a data   ���
��� cancelamento (data do JOB)                                             ���
��� SEMPRE QUE FOR NOME DO CAMPO, PASSAR COM O UNDERLINE (ex: _DATACC)     ���  
������������������������������������������������������������������������������
/*/
Function PLSVSCAN(cAlias, aStatus, cAliCpo, cDataCpo, cStatDs, cMotCpo, cCodF3, cCodMotCpo, cMsgObs, cNomParam, cDatCanc)
LOCAL nQuantD	    := 0  
LOCAL nI		  	  := 0
LOCAL cStrStat	  := ""
Default cAlias    := "BOW" 
Default aStatus 	:= {"A","B"}  //A- Solicita��o n�o conclu�da / B- Aguardando informa��o benefici�rio 
Default cAliCpo 	:= "_STATUS" 
Default cDataCpo	:= "_DTDIGI"
Default cMotCpo   := "_MOTIND"
Default cCodF3    := "XXX"
Default cCodMotCpo:= "_MOTPAD"
Default cMsgObs   := ""
Default cNomParam := ""
Default cStatDs	  := "D"
Default cDatCanc	:= dDataBase


nQuantD	  	:= IIF ( Empty(cNomParam), GetNewPar("MV_PRACAN",15), GetNewPar(cNomParam,15) )

For nI := 1 TO Len(aStatus) 
  cStrStat += aStatus[nI] + ","
Next

cStrStat := SUBSTR(cStrStat,0,Len(cStrStat)-1) 

BBP->(DbSelectArea("BBP"))  
BBP->(DbSetOrder(1))
(cAlias)->(DbSelectArea(cAlias))
(cAlias)->(DbGoTop())

  While !(cAlias)->(EOF())    
    If( (cAlias)->&(cAlias+cAliCpo) $ cStrStat)    //Verifico se o campo escolhido cont�m alguns dos status passados
      If ( !((calias)->&(cAlias+cDataCpo) + nQuantD) >= dDataBase)  
        (cAlias)->(RecLock(cAlias),.F.)
        (cAlias)->&(cAlias+cAliCpo) := cStatDs
        IIF( !(Empty(cMotCpo)), (cAlias)->&(cAlias+cMotCpo) := IIF (Empty(cMsgObs),STR0001, cMsgObs), "")

        If !(Empty(cCodF3)) .AND. !(Empty(cCodMotCpo))
        // Busca o Cod na tabela BBP, para preencher o memo da observa��o
            (cAlias)->&(cAlias+cCodMotCpo) := cCodF3
            If BBP->(MsSeek(xFilial("BBP")+cCodF3))
                If cAlias == "BOW"
                    If !Empty((cAlias)->&(cAlias+"_OBS"))
                        (cAlias)->&(cAlias+"_OBS") := (cAlias)->&(cAlias+"_OBS") + chr(13)+chr(10) + BBP->BBP_OBSERV
                    Else
                        (cAlias)->&(cAlias+"_OBS") := BBP->BBP_OBSERV
                    EndIf
                EndIf  
            EndIf
        EndIf   
        
        IIF( cAlias == "BOW", (cAlias)->&(cAlias+"_DTCANC") := dDataBase, IIF ( !(Empty(cDatCanc)), (cAlias)->&(cAlias+cDatCanc) := dDataBase, "") )
        (cAlias)->(MsUnLock()) 
      EndIf
    EndIf
  (cAlias)->(DbSkip())  
  EndDo  
     
(cAlias)->(DbCloseArea())
BBP->(DbCloseArea())   
        
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} PJB
Executa o job de cancelamento do protocolo de reembolso
@version P12
/*/
//-------------------------------------------------------------------
Function PJB(aJob)
Local cCodEm  := aJob[9]
Local cCodFil  := aJob[10] 

RpcSetEnv( cCodEm, cCodFil , , ,'PLS', , )

FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "Execu��o da Tarefa de cancelamento de guias conforme status" , 0, 0, {})
 
PLSVSCAN (aJob[1],aJob[2],aJob[3],aJob[4],aJob[5],aJob[6],aJob[7],aJob[8])

FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "Execu��o Finalizada!" , 0, 0, {})
 
Return()

