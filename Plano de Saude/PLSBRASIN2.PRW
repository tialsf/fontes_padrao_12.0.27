#Include 'Protheus.ch'
#Include 'FWMVCDEF.CH'
#Include 'FWBROWSE.CH'
#Include 'Totvs.CH'
#Include 'topconn.ch'
#include 'PLSBRASIN2.ch'


//-------------------------------------------------------------------
/*/ {Protheus.doc} PLSBRASIN2
Tela inicial de Importa��es da Bras�ndice
@since 04/2020
@version P12 
/*/
//-------------------------------------------------------------------
Function PLSBRASIN2(lAutoma)
Local oBrowse   := nil
local cFiltro   := "@(B6G_FILIAL = '" + xFilial("B6G") + "' AND B6G_CODOPE = '" + PlsIntPad() + "' AND B6G_TPARQ = '1' )"
default lAutoma := iif( valtype(lAutoma) <> "L", .f., lAutoma )

oBrowse := FWMBrowse():New()
oBrowse:SetAlias('B6G')
oBrowse:SetFilterDefault(cFiltro)
oBrowse:SetMenuDef('PLSBRASIN2')
oBrowse:SetDescription(STR0001) //Configura��o de Importa��o da Bras�ndice�
iif( !lAutoma, oBrowse:Activate(), "")

Return nil


//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menus
@since 04/2020
@version P12 
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
Local aRotina := {}

Add Option aRotina Title  STR0002 	Action 'VIEWDEF.PLSBRASIN2'     Operation 3 Access 0  //Incluir
Add Option aRotina Title  STR0003   Action 'VIEWDEF.PLSBRASIN2'     Operation 4 Access 0  //Alterar
Add Option aRotina Title  STR0004	Action 'VIEWDEF.PLSBRASIN2' 	Operation 2 Access 0  //Visualizar
Add Option aRotina Title  STR0005	Action 'VIEWDEF.PLSBRASIN2'     Operation 5 Access 0  //Excluir

Return aRotina


//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Defini��o do modelo de Dados.
@since 04/2020
@version P12
/*/
//-------------------------------------------------------------------
Static Function ModelDef()
Local oModel    := nil
Local oStrB6G	:= FWFormStruct(1,'B6G')

oModel := MPFormModel():New( 'PLSBRASIN2', , {||PlsCadOk(oModel)}) 
oModel:AddFields( 'B6GMASTER', /*cOwner*/, oStrB6G )
                                         
oStrB6G:setProperty("B6G_CODOPE" , MODEL_FIELD_INIT , { || PlsIntPad()} )
oStrB6G:setProperty("B6G_CODIGO" , MODEL_FIELD_INIT , { || GetSx8Num('B6G', 'B6G_CODIGO')} )
oStrB6G:setProperty("B6G_TPARQ"  , MODEL_FIELD_INIT , { || '1' })//1=Brasindice;2=Simpro;3=A900
oStrB6G:SetProperty("B6G_TIPO"   , MODEL_FIELD_VALID, { || ChkVlrDup("2",oModel)[1] } )
oStrB6G:SetProperty("B6G_TIPPRO" , MODEL_FIELD_WHEN , { || oModel:GetOperation() == MODEL_OPERATION_INSERT} )
oStrB6G:SetProperty("B6G_TIPO"   , MODEL_FIELD_WHEN , { || iif( oModel:GetModel("B6GMASTER"):getvalue("B6G_TIPPRO") $ "1/3" .and.;
                                                        oModel:GetModel("B6GMASTER"):getvalue("B6G_TIPO") == "2", .f., .t.)} )

oModel:GetModel( 'B6GMASTER' ):SetDescription( STR0001 )

Return oModel


//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Defini��o da interface.
@since 04/2020
@version P12
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
Local oView 
Local oModel	:= FWLoadModel( 'PLSBRASIN2' )
Local oStrB6G	:= FWFormStruct(2,'B6G', {|cCampo|PlCmpTab(cCampo)})

oView := FWFormView():New()
oView:SetModel( oModel )

oStrB6G:SetProperty( 'B6G_CODPAD' , MVC_VIEW_ORDEM, "06" )

oView:AddField( 'ViewB6G', oStrB6G, 'B6GMASTER' ) 
oView:CreateHorizontalBox( 'SUPERIOR', 100 )
oView:SetOwnerView( 'ViewB6G', 'SUPERIOR' )
oView:SetFieldAction('B6G_TIPPRO', { |oView| AtuCmpVal(oView, "1")}) 
oView:SetFieldAction('B6G_CRITDE', { |oView| AtuCmpVal(oView, "2")})
Return oView


//-------------------------------------------------------------------
/*/{Protheus.doc} PlCmpTab
Campos que devem ser exibidos no form
@since 04/2020
@version P12
/*/
//-------------------------------------------------------------------
static function PlCmpTab(cCampo)
Local lRet := .f.

if !(alltrim(cCampo) $ 'B6G_CODOPE,B6G_DATINC,B6G_CODIGO,B6G_TPARQ')
	lRet := .t.
endif

return lRet


//-------------------------------------------------------------------
/*/ {Protheus.doc} ChkVlrDup
Verifica valores repetidos na tabela B6G ou preenche array com os dados da tabela
@since 04/2020
@version P12 
/*/
//-------------------------------------------------------------------
static function ChkVlrDup(cOrigem, oModel, cOperad)
local lOriSin2  := iif( cOrigem == "2", .t., .f.)
local aDadRet   := {}
local cTipPro	:= iif( lOriSin2, oModel:getModel("B6GMASTER"):getValue("B6G_TIPPRO"), "" )
local cTipo		:= iif( lOriSin2, oModel:getModel("B6GMASTER"):getValue("B6G_TIPO"), "" )
local cCodOpe	:= iif( lOriSin2, oModel:getModel("B6GMASTER"):getValue("B6G_CODOPE"), cOperad )
local cSql		:= ""
local cSqlComp  := ""
local lRet 		:= .t.

if lOriSin2
    cSqlComp    := "   AND B6G_TIPPRO = '" + cTipPro + "' "
    cSqlComp    += "   AND B6G_TIPO   = '" + cTipo   + "' "
endif

cSql := " SELECT B6G_CODOPE, B6G_TIPPRO, B6G_TIPO, B6G_REGIMP, R_E_C_N_O_ REC FROM " + RetSqlName("B6G") 
cSql += " WHERE B6G_FILIAL = '"    + xFilial("B6G") + "' "
cSql += "   AND B6G_CODOPE = '"    + cCodOpe        + "' "
cSql += "   AND B6G_TPARQ = '1' "
cSql += iif( lOriSin2, cSqlComp, "" ) 
cSql += "   AND D_E_L_E_T_ = ' ' "

dbUseArea(.t.,"TOPCONN",tcGenQry(,,ChangeQuery(cSQL)),"VerRep",.f.,.t.)

if ( !VerRep->(eof()) )
    if lOriSin2
        lRet := .f.
        Help(nil, nil , STR0006, nil, STR0007, 1, 0, nil, nil, nil, nil, nil, {STR0008} ) //Aten��o / "J� existe cadastro ativo para este Tipo de Procedimento (B6G_TIPPRO) e Tipo de Valor (B6G_TIPO)."
        oModel:getModel("B6GMASTER"):loadValue("B6G_TIPO", "")
    else    
        while !VerRep->(eof())
            aadd(aDadRet, {VerRep->B6G_TIPPRO, VerRep->B6G_TIPO, VerRep->B6G_REGIMP, VerRep->REC, {}, {}})
            VerRep->(dbskip())
        enddo
    endif
elseif !lOriSin2
    lRet := .f.
endif

VerRep->(dbclosearea()) 

return ( {lRet, aDadRet} )


//-------------------------------------------------------------------
/*/ {Protheus.doc} PlsCadOk
Valida��o P�s-Model
@since 04/2020
@version P12 
/*/
//-------------------------------------------------------------------
static function PlsCadOk(oModel)
local lRet      := .t.
local cPropTde  := oModel:getModel("B6GMASTER"):getValue("B6G_CRITDE")
local cCodTde   := oModel:getModel("B6GMASTER"):getValue("B6G_CODTDE")
local cCodPad   := oModel:getModel("B6GMASTER"):getValue("B6G_CODPAD")
local aMsg      := {}

if cPropTde == "0" .and. empty(cCodTde)
    lRet := .f.
    aMsg := {STR0009, STR0010} //"Quando a op��o 'Cria TDE' for igual a 'N�o', informe para qual TDE os procedimentos devem ser importados."
elseif cPropTde == "1" .and. empty(cCodPad)
    lRet := .f.
    aMsg := {STR0011, STR0012} //"Quando a op��o 'Cria TDE' for igual a 'Sim', informe o c�digo Tipo Padr�o Sa�de." 
endif

if !lRet
    Help(nil, nil , STR0006, nil, aMsg[1], 1, 0, nil, nil, nil, nil, nil, {aMsg[2]} ) 
else
    B6G->(confirmSX8())
endif

return lRet


//-------------------------------------------------------------------
/*/ {Protheus.doc} AtuCmpVal
Atualiza informa��es dos campos B6G_TIPO e B6G_TIPPRO
@since 07/2020
@version P12 
/*/
//-------------------------------------------------------------------
static function AtuCmpVal(oView, cTipo, oModel)
local oObjB6G   := nil//oView:GetModel("B6GMASTER")
local lRet		:= .t.
default oModel  := nil

oObjB6G := iif( empty(oModel), oView:GetModel("B6GMASTER"), oModel )

if cTipo == "1"
    if oObjB6G:getvalue("B6G_TIPPRO") $ "1/3"
        oObjB6G:loadvalue("B6G_TIPO", "")
        lRet := oObjB6G:setvalue("B6G_TIPO", "2")
        if !lRet
            oObjB6G:setvalue("B6G_TIPPRO", " ")
            oObjB6G:loadvalue("B6G_TIPO", " ")
        endif
    else
        lRet := oObjB6G:loadvalue("B6G_TIPO", " ")
    endif   
else
    if oObjB6G:getvalue("B6G_CRITDE") == "1" .and. !empty(oObjB6G:getvalue("B6G_CODTDE"))
        oObjB6G:loadvalue("B6G_CODTDE","")
    elseif oObjB6G:getvalue("B6G_CRITDE") == "0" .and. !empty(oObjB6G:getvalue("B6G_CODPAD"))
        oObjB6G:loadvalue("B6G_CODPAD", "")    
    endif
endif

if empty(oModel)
    oView:refresh()
endif

return lRet 