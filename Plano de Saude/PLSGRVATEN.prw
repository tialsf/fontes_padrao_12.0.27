#Include 'Protheus.ch'
#include "fileio.ch"

//---------------------------------------------------------------------------------------
/*/{Protheus.doc} plsAtuBenf
Retorna array contendo as informações de todos os pacotes da squad de beneficiários que foram aplicados

@author     Rodrigo Morgon
@since      29/08/2017
@version    P12
/*/
//---------------------------------------------------------------------------------------
function plsAtuAten()

    local aRet      := {}
    
    aAdd(aRet,{"SPRINT 08",findFunction("PLSBSP08")})
    aAdd(aRet,{"SPRINT 09",findFunction("PLSBSP09")})
    aAdd(aRet,{"SPRINT 10",findFunction("PLSBSP10")})
	aAdd(aRet,{"SPRINT 11",findFunction("PLSBSP11")})
	aAdd(aRet,{"SPRINT 12",findFunction("PLSBSP12")})
	aAdd(aRet,{"SPRINT 13",findFunction("PLSBSP13")})
	aAdd(aRet,{"SPRINT 14",findFunction("PLSBSP14")})
	aAdd(aRet,{"SPRINT 15",findFunction("PLSBSP15")})
	aAdd(aRet,{"SPRINT 16",findFunction("PLSBSP16")})
	aAdd(aRet,{"SPRINT 17",findFunction("PLSBSP17")})
	aAdd(aRet,{"SPRINT 18",findFunction("PLSBSP18")})
	aAdd(aRet,{"SPRINT 19",findFunction("PLSASP19")}) // Até a sprint 18 atendimento foi liberado junto com beneficiário
	aAdd(aRet,{"SPRINT 20",findFunction("PLSASP20")})
	aAdd(aRet,{"SPRINT 21",findFunction("PLSASP21")})
	aAdd(aRet,{"SPRINT 22",findFunction("PLSASP22")})
	aAdd(aRet,{"SPRINT 23",findFunction("PLSASP23")})
	aAdd(aRet,{"SPRINT 24",findFunction("PLSASP24")})
	aAdd(aRet,{"SPRINT 25",findFunction("PLSASP25")})
	aAdd(aRet,{"SPRINT 26",findFunction("PLSASP28")})
	aAdd(aRet,{"SPRINT 27",findFunction("PLSASP28")})
	aAdd(aRet,{"SPRINT 28",findFunction("PLSASP28")})
	aAdd(aRet,{"SPRINT 29",findFunction("PLSASP32")})
	aAdd(aRet,{"SPRINT 30",findFunction("PLSASP32")})
	aAdd(aRet,{"SPRINT 31",findFunction("PLSASP32")})
	aAdd(aRet,{"SPRINT 32",findFunction("PLSASP32")})

return aRet