#Include 'Protheus.ch'
#include "fileio.ch"

//---------------------------------------------------------------------------------------
/*/{Protheus.doc} plsAtuBenf
Retorna array contendo as informações de todos os pacotes da squad de beneficiários que foram aplicados

@author     Rodrigo Morgon
@since      29/08/2017
@version    P12
/*/
//---------------------------------------------------------------------------------------
function plsAtuBenf()

    local aRet      := {}
    
	aAdd(aRet,{"SPRINT 08",findFunction("PLSBSP08")})
	aAdd(aRet,{"SPRINT 09",findFunction("PLSBSP09")})
	aAdd(aRet,{"SPRINT 10",findFunction("PLSBSP10")})
	aAdd(aRet,{"SPRINT 11",findFunction("PLSBSP11")})
	aAdd(aRet,{"SPRINT 12",findFunction("PLSBSP12")})
	aAdd(aRet,{"SPRINT 13",findFunction("PLSBSP13")})
	aAdd(aRet,{"SPRINT 14",findFunction("PLSBSP14")})
	aAdd(aRet,{"SPRINT 15",findFunction("PLSBSP15")})
	aAdd(aRet,{"SPRINT 16",findFunction("PLSBSP16")})
	aAdd(aRet,{"SPRINT 17",findFunction("PLSBSP17")})
	aAdd(aRet,{"SPRINT 18",findFunction("PLSBSP18")})
	aAdd(aRet,{"SPRINT 19",findFunction("PLSBSP19")})
	aAdd(aRet,{"SPRINT 20",findFunction("PLSBSP20")})
	aAdd(aRet,{"SPRINT 21",findFunction("PLSBSP21")})
	aAdd(aRet,{"SPRINT 22",findFunction("PLSBSP22")})
	aAdd(aRet,{"SPRINT 23",findFunction("PLSBSP23")})
	aAdd(aRet,{"SPRINT 24",findFunction("PLSBSP24")})
	aAdd(aRet,{"SPRINT 25",findFunction("PLSBSP25")})
	aAdd(aRet,{"SPRINT 26",findFunction("PLSBSP28")})
	aAdd(aRet,{"SPRINT 27",findFunction("PLSBSP28")})
	aAdd(aRet,{"SPRINT 28",findFunction("PLSBSP28")})
	
	aAdd(aRet,{"SPRINT 29",findFunction("PLSBSP32")})
	aAdd(aRet,{"SPRINT 30",findFunction("PLSBSP32")})
	aAdd(aRet,{"SPRINT 31",findFunction("PLSBSP32")})
	aAdd(aRet,{"SPRINT 32",findFunction("PLSBSP32")})
									


return aRet