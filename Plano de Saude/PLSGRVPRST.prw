#Include 'Protheus.ch'
#include "fileio.ch"

//---------------------------------------------------------------------------------------
/*/{Protheus.doc} plsAtuPrst
Retorna array contendo as informações de todos os pacotes da squad de prestadores que foram aplicados

@author     Rodrigo Morgon
@since      29/08/2017
@version    P12
/*/
//---------------------------------------------------------------------------------------
function plsAtuPrst()

    local aRet      := {}
    
    aAdd(aRet,{"SPRINT 08",findFunction("PLSPSP08")})
    aAdd(aRet,{"SPRINT 09",findFunction("PLSPSP09")})
    aAdd(aRet,{"SPRINT 10",findFunction("PLSPSP10")})
	aAdd(aRet,{"SPRINT 11",findFunction("PLSPSP11")})
	aAdd(aRet,{"SPRINT 12",findFunction("PLSPSP12")})
	aAdd(aRet,{"SPRINT 13",findFunction("PLSPSP13")})
	aAdd(aRet,{"SPRINT 14",findFunction("PLSPSP14")})
	aAdd(aRet,{"SPRINT 15",findFunction("PLSPSP15")})
	aAdd(aRet,{"SPRINT 16",findFunction("PLSPSP16")})
	aAdd(aRet,{"SPRINT 17",findFunction("PLSPSP17")})
	aAdd(aRet,{"SPRINT 18",findFunction("PLSPSP18")})
	aAdd(aRet,{"SPRINT 19",findFunction("PLSPSP19")})
	aAdd(aRet,{"SPRINT 20",findFunction("PLSPSP20")})
	aAdd(aRet,{"SPRINT 21",findFunction("PLSPSP21")})
	aAdd(aRet,{"SPRINT 22",findFunction("PLSPSP22")})
	aAdd(aRet,{"SPRINT 23",findFunction("PLSPSP23")})
	aAdd(aRet,{"SPRINT 24",findFunction("PLSPSP24")})
	aAdd(aRet,{"SPRINT 25",findFunction("PLSPSP25")})
	aAdd(aRet,{"SPRINT 26",findFunction("PLSPSP28")})
	aAdd(aRet,{"SPRINT 27",findFunction("PLSPSP28")})
	aAdd(aRet,{"SPRINT 28",findFunction("PLSPSP28")})

	aAdd(aRet,{"SPRINT 29",findFunction("PLSPSP32")})
	aAdd(aRet,{"SPRINT 30",findFunction("PLSPSP32")})
	aAdd(aRet,{"SPRINT 31",findFunction("PLSPSP32")})
	aAdd(aRet,{"SPRINT 32",findFunction("PLSPSP32")})
	
return aRet
