#INCLUDE "PLSM170.ch"
#include "Protheus.CH"
#include "PLSMGER.CH"
/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    � PLSM170 � Autor � Tulio Cesar            � Data � 29.05.04 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Atualizacao off-line de arquivo com receita e custos       ����
�������������������������������������������������������������������������Ĵ���
���Sintaxe   � PLSM170()                                                  ����
�������������������������������������������������������������������������Ĵ���
��� Uso      � Advanced Protheus                                          ����
�������������������������������������������������������������������������Ĵ���
��� Alteracoes desde sua construcao inicial.                              ����
�������������������������������������������������������������������������Ĵ���
��� Data     � BOPS � Programador � Breve Descricao                       ����
�������������������������������������������������������������������������Ĵ���
���          �      �             �                                       ����
��������������������������������������������������������������������������ٱ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/
Function PLSM170()
//��������������������������������������������������������������������������Ŀ
//� Define variaveis da rotina...                                            �
//����������������������������������������������������������������������������
LOCAL cPerg := "PLM170"
//��������������������������������������������������������������������������Ŀ
//� Verifica se campos ja existem na base									 �
//����������������������������������������������������������������������������
If BD7->( FieldPos("BD7_CONEMP") ) == 0 .Or. BD7->( FieldPos("BD7_VERCON") ) == 0 .Or.; 
   BD7->( FieldPos("BD7_SUBCON") ) == 0 .Or. BD7->( FieldPos("BD7_VERSUB") ) == 0
   
	MsgAlert(STR0002) //"Necess�rio executar o Compatinilizador 'UPDPLS68' da FNC - 000000131182010"
	Return()        
EndIf
//��������������������������������������������������������������������������Ŀ
//� Acessa os parametros da rotina...                                        �
//����������������������������������������������������������������������������
If ! Pergunte(cPerg,.T.)
   Return
Endif
//��������������������������������������������������������������������������Ŀ
//� Executa funcao padrao para processamento...                              �
//����������������������������������������������������������������������������
Processa( {|| M170Pro(cPerg) }, STR0001,"",.T. )   //"Apurando Receitas e Custos"
MsgInfo("Processamento conclu�do!")
//��������������������������������������������������������������������������Ŀ
//� Fim da rotina...                                                         �
//����������������������������������������������������������������������������
Return
/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    � M170PRO � Autor � Tulio Cesar            � Data � 29.05.04 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Atualizacao off-line de arquivo com receita e custos       ����
�������������������������������������������������������������������������Ĵ���
���Sintaxe   � M170PRO()                                                  ����
�������������������������������������������������������������������������Ĵ���
��� Uso      � PLSM170()                                                  ����
��������������������������������������������������������������������������ٱ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/
Static Function M170PRO(cPerg)
//��������������������������������������������������������������������������Ŀ
//� Define variaveis da rotina...                                            �
//����������������������������������������������������������������������������
LOCAL cCodOpe
LOCAL cAno
LOCAL cGrupoDe
LOCAL cGrupoAte
LOCAL cContDe
LOCAl cVerConDe
LOCAL cContAte
LOCAl cVerConAte
LOCAL cSubDe
LOCAl cVerSubDe
LOCAL cSubAte
LOCAl cVerSubAte
LOCAL cFamDe
LOCAL cFamAte
LOCAL cUsrDe
LOCAL cUsrAte
LOCAL cMesDe
LOCAL cMesAte                 

LOCAL cQuebCon
LOCAL cQuebSub                                                                
//��������������������������������������������������������������������������Ŀ
//� Cria perguntas adicionais...                                             �
//����������������������������������������������������������������������������
CriaSX1()
//��������������������������������������������������������������������������Ŀ
//� Acessa grupo de perguntes...                                             �
//����������������������������������������������������������������������������
Pergunte(cPerg,.F.)

cCodOpe   := mv_par01
cAno      := mv_par02
cGrupoDe  := mv_par03
cGrupoAte := mv_par04
cContDe   := mv_par05
cVerConDe := mv_par06
cContAte  := mv_par07                                                                                        
cVerConAte:= mv_par08
cSubDe    := mv_par09
cVerSubDe := mv_par10
cSubAte   := mv_par11
cVerSubAte:= mv_par12
cFamDe    := mv_par13
cFamAte   := mv_par14
cUsrDe    := mv_par15
cUsrAte   := mv_par16 
cMesDe    := mv_par17
cMesAte   := mv_par18
nTpRec	  := mv_par19

//��������������������������������������������������������������������������Ŀ
//� INICIO DO PROCESSAMENTO...                                               �
//����������������������������������������������������������������������������
BA0->(DbSetOrder(1))
If BA0->(DbSeek(xFilial("BA0")+cCodOpe))
   //��������������������������������������������������������������������������Ŀ
   //� Analisa o nivel **** OPERADORA ***                                       �
   //����������������������������������������������������������������������������
   CalculaRC("0",cMesDe,cMesAte,cAno,cCodOpe,"","","","","","","",nTpRec,"","")
   //��������������������������������������������������������������������������Ŀ
   //� analisa o nivel ***** GRUPOS EMPRESAS DA OPERADORA *****                 �
   //����������������������������������������������������������������������������
   BG9->(DbSetOrder(1))
   BG9->(DbSeek(xFilial("BG9")))
   While ! BG9->(Eof()) .And. BG9->BG9_FILIAL == xFilial("BG9")
         
         INCPROC(STR0003+BG9->BG9_CODIGO+"...") //"Analisando Grupo Empresa "
         ProcessMessage()

         //��������������������������������������������������������������������������Ŀ
         //� Bypassa registros fora do filtro...                                      �
         //����������������������������������������������������������������������������
         If ! ( ( BG9->BG9_CODINT == cCodOpe ) .And. ;
                ( BG9->BG9_CODIGO >= cGrupoDe .And. BG9->BG9_CODIGO <= cGrupoAte ) )
            BG9->(DbSkip())
            Loop
         Endif       
         //��������������������������������������������������������������������������Ŀ
         //� Acumula dados do GRUPO EMPRESA...                                        �
         //����������������������������������������������������������������������������
         CalculaRC("1",cMesDe,cMesAte,cAno,cCodOpe,BG9->BG9_CODIGO,"","","","","","",nTpRec,"","")
         //��������������������������������������������������������������������������Ŀ
         //� Posiciona na primeira familia...                                         �
         //����������������������������������������������������������������������������
         BA3->(DbSetOrder(7))
         If BA3->(DbSeek(xFilial("BA3")+BG9->(BG9_CODINT+BG9_CODIGO)))
            While ! BA3->(Eof()) .And. BA3->(BA3_FILIAL+BA3_CODINT+BA3_CODEMP) == xFilial("BA3")+BG9->(BG9_CODINT+BG9_CODIGO)
                  //��������������������������������������������������������������������������Ŀ
                  //� Se for PJ busca por contrato e subcontrato...                            �
                  //����������������������������������������������������������������������������
                  If BG9->BG9_TIPO == "2"
                     //��������������������������������������������������������������������������Ŀ
                     //� Acumula dados do CONTRATO...                                             �
                     //����������������������������������������������������������������������������
                     CalculaRC("2",cMesDe,cMesAte,cAno,cCodOpe,BG9->BG9_CODIGO,BA3->BA3_CONEMP,BA3->BA3_VERCON,"","","","",nTpRec,"","")         
                     cQuebCon := BA3->(BA3_CONEMP+BA3_VERCON)
                     //��������������������������������������������������������������������������Ŀ
                     //� Acumula dados do SUBCONTRATO...                                          �
                     //����������������������������������������������������������������������������
                     cQuebSub := BA3->(BA3_SUBCON+BA3_VERSUB)                    
                     CalculaRC("3",cMesDe,cMesAte,cAno,cCodOpe,BG9->BG9_CODIGO,BA3->BA3_CONEMP,BA3->BA3_VERCON,BA3->BA3_SUBCON,;
                                  BA3->BA3_VERSUB,"","",nTpRec,"","")
                     //��������������������������������������������������������������������������Ŀ
                     //� Faz a quebra por subcontrato...                                          �
                     //����������������������������������������������������������������������������
                     While ! BA3->(Eof()) .And. BA3->(BA3_FILIAL+BA3_CODINT+BA3_CODEMP+BA3_CONEMP+BA3_VERCON+BA3_SUBCON+BA3_VERSUB) == ;
                                                 xFilial("BA3")+BG9->(BG9_CODINT+BG9_CODIGO)+cQuebCon+cQuebSub
                     
                           ProcessaFam(	cMesDe,cMesAte,cAno,cCodOpe,cContDe,cVerConDe,cContAte,cVerConAte,cSubDe,;
                           				cVerSubDe,cSubAte,cVerSubAte,cFamDe,cFamAte,cUsrDe,cUsrAte,nTpRec) //PJ
                     BA3->(DbSkip())
                     Enddo
                  Else     
                      ProcessaFam(cMesDe,cMesAte,cAno,cCodOpe,cContDe,cVerConDe,cContAte,cVerConAte,cSubDe,;
                                  cVerSubDe,cSubAte,cVerSubAte,cFamDe,cFamAte,cUsrDe,cUsrAte,nTpRec) //PF
                      BA3->(DbSkip())
                  Endif
            Enddo        
         Endif   

   BG9->(DbSkip())
   Enddo          
Endif
//��������������������������������������������������������������������������Ŀ
//� Fim da rotina...                                                         �
//����������������������������������������������������������������������������
Return
/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    � CalculaRC  � Autor � Tulio Cesar         � Data � 29.05.04 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Calcula a receita e custo do nivel e atualiza arquivo...   ����
��������������������������������������������������������������������������ٱ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/
Static Function CalculaRC(cTipo,cMesDe,cMesAte,cAno,cCodOpe,;
						  cCodEmp,cNumCon,cVerCon,cSubCon,;
						  cVerSub,cMatric,cTipReg,nTpRec,cCodPla,cTipUsr)
LOCAL aReceitas  := {}
LOCAL aCustos    := {}
LOCAL aUsuarios	 := {}
LOCAL aRecCalc   := {}
LOCAL aCusCalc   := {}
LOCAL aTotUser	 := {}
LOCAL nFor
LOCAL cMes
LOCAL cMacro
LOCAL nAcuCusOpe := 0  
LOCAL nAcuRecOpe := 0 
LOCAL nUsuarios	 := 0 
DEFAULT nTpRec	 := 0                                    
DEFAULT cCodPla  := ""
DEFAULT cTipUsr  := ""

For nFor := Val(cMesDe) To Val(cMesAte)
    cMes       := StrZero(nFor,2)
	 // Como estou fazendo mes a mes n�o posso mandar o mes de inicio e mes fim, somenta mes corrente
    aRecCalc   := PLSRECCALC(cTipo,cCodOpe,cCodEmp,cNumCon,cVerCon,cSubCon,cVerSub,;
    						 cMatric,cTipReg,cAno,cMes,cAno,cMes,nTpRec)            
    						 
	 // Como estou fazendo mes a mes n�o posso mandar o mes de inicio e mes fim, somenta mes corrente   
    aCusCalc   := PLSCUSTMOV(cTipo,cCodOpe,cCodEmp,cNumCon,cVerCon,cSubCon,cVerSub,;
    						 cMatric,cTipReg,cAno,cMes,cAno,cMes,.F.)
                             
    nAcuCusOpe += aCusCalc[1]
    nAcuRecOpe += aRecCalc[1]
      
    cMacro     := "BX9->BX9_VRC"+PLSBusMeses(cMes)
    aadd(aCustos,{cMacro,aCusCalc[1]})

    cMacro     := "BX9->BX9_VRR"+PLSBusMeses(cMes)
    aadd(aReceitas,{cMacro,aRecCalc[1]})
                        
    // Nao calcula quantidade de usuarios por familia e nem por usuario. Apenas por empresa, contrato e sub.
	If !(cTipo $ '4,5')
		aTotUser   := PLQTUSEMP(cCodOpe,cCodEmp,cNumCon,cVerCon,;
							 cSubCon,cVerSub,nil,nil,cMes,cAno)           
							 
    	nUsuarios  += Len(aTotUser)

		cMacro := "BX9_USR"+PLSBusMeses(cMes)
		//If BX9->( FieldPos(cMacro) ) > 0
	    	cMacro     := "BX9->BX9_USR"+PLSBusMeses(cMes)
	    	aadd(aUsuarios,{cMacro,Len(aTotUser)})    
 		//Endif
	Endif 	
Next    

If     cTipo == "0"
       BX9->(DbSetOrder(1))
       BX9->(DbSeek(xFilial("BX9")+"0"+cAno+cCodOpe))
ElseIf cTipo == "1"   
       BX9->(DbSetOrder(2))
       BX9->(DbSeek(xFilial("BX9")+"1"+cAno+cCodOpe+cCodEmp))
ElseIf cTipo == "2"   
       BX9->(DbSetOrder(3))
       BX9->(DbSeek(xFilial("BX9")+"2"+cAno+cCodOpe+cCodEmp+cNumCon+cVerCon))
ElseIf cTipo == "3"   
       BX9->(DbSetOrder(4))
       BX9->(DbSeek(xFilial("BX9")+"3"+cAno+cCodOpe+cCodEmp+cNumCon+cVerCon+cSubCon+cVerSub))
ElseIf cTipo == "4"   
       BX9->(DbSetOrder(5))
       BX9->(DbSeek(xFilial("BX9")+"4"+cAno+cCodOpe+cCodEmp+cMatric))
ElseIf cTipo == "5"   
       BX9->(DbSetOrder(6))
       BX9->(DbSeek(xFilial("BX9")+"5"+cAno+cCodOpe+cCodEmp+cMatric+cTipReg))
Endif       

If ! BX9->(Found())
   BX9->(RecLock("BX9",.T.))
   BX9->BX9_FILIAL := xFilial("BX9")
   BX9->BX9_TIPO   := cTipo
   BX9->BX9_ANO    := cAno
   BX9->BX9_CODOPE := cCodOpe
   BX9->BX9_CODEMP := cCodEmp
   BX9->BX9_CONEMP := cNumCon
   BX9->BX9_VERCON := cVerCon
   BX9->BX9_SUBCON := cSubCon
   BX9->BX9_VERSUB := cVerSub
   BX9->BX9_MATRIC := cMatric
   BX9->BX9_TIPREG := cTipReg
Else                         
   BX9->(RecLock("BX9",.F.))
Endif                        

If ! Empty(cTipUsr) .And. cTipo == "5" //Usuario
   BX9->BX9_TIPUSR := cTipUsr
Endif

If ! Empty(cCodPla) .And. cTipo == "5" //Usuario
   BX9->BX9_CODPLA := cCodPla
Endif

For nFor := 1 To Len(aReceitas)
    &(aReceitas[nFor,1]) := aReceitas[nFor,2]
Next        

For nFor := 1 To Len(aCustos)
    &(aCustos[nFor,1]) := aCustos[nFor,2]
Next

For nFor := 1 To Len(aUsuarios)
	&(aUsuarios[nFor,1]) := aUsuarios[nFor,2]
Next

BX9->BX9_VRCACU := nAcuCusOpe
BX9->BX9_VRRACU := nAcuRecOpe

If ExistBlock("PL170BX9")
   ExecBlock("PL170BX9",.F.,.F.)
Endif

BX9->(MsUnLock())

Return

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    � ProcessaFam� Autor � Tulio Cesar         � Data � 29.05.04 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Processa a familia e seus usuarios...                      ����
��������������������������������������������������������������������������ٱ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/
Static Function ProcessaFam(cMesDe,cMesAte,cAno,cCodOpe,cContDe,cVerConDe,cContAte,cVerConAte,cSubDe,cVerSubDe,cSubAte,;
                            cVerSubAte,cFamDe,cFamAte,cUsrDe,cUsrAte,nTpRec)
LOCAL cCodPla := ""
//��������������������������������������������������������������������������Ŀ
//� Acumula dados da FAMILIA...                                              �
//����������������������������������������������������������������������������

INCPROC(STR0004+BA3->(BA3_CODINT+BA3_CODEMP+BA3_MATRIC)+"...") //"Processando Familia "
ProcessMessage()

If ( BA3->BA3_MATRIC >= cFamDe .And. BA3->BA3_MATRIC <= cFamAte ) .And. ;
		( ( BA3->BA3_TIPOUS == "1" ) .Or. ;
		( BA3->BA3_TIPOUS == "2" .And. BA3->BA3_CONEMP+BA3->BA3_VERCON >= cContDe+cVerConDe .And. ;
		BA3->BA3_CONEMP+BA3->BA3_VERCON <= cContAte+cVerConAte .And. ;
		BA3->BA3_SUBCON+BA3->BA3_VERSUB >= cSubDe+cVerSubDe .And. ;
		BA3->BA3_SUBCON+BA3->BA3_VERSUB <= cSubAte+cVerSubAte ) )
	            
	// Faz calculo da familia
	CalculaRC("4",cMesDe,cMesAte,cAno,cCodOpe,BG9->BG9_CODIGO,BA3->BA3_CONEMP,BA3->BA3_VERCON,BA3->BA3_SUBCON,;
			BA3->BA3_VERSUB,BA3->BA3_MATRIC,'',nTpRec,cCodPla,BA3->BA3_TIPOUS)
	
	BA1->(DbSetOrder(2))
	If BA1->(DbSeek(xFilial("BA1")+BA3->(BA3_CODINT+BA3_CODEMP+BA3_MATRIC)))
		While ! BA1->(Eof()) .And. BA1->(BA1_FILIAL+BA1_CODINT+BA1_CODEMP+BA1_MATRIC) == ;
			xFilial("BA1")+BA3->(BA3_CODINT+BA3_CODEMP+BA3_MATRIC)
			//��������������������������������������������������������������������������Ŀ
			//� Acumula dados dos USUARIOS...                                            �
			//����������������������������������������������������������������������������
			If BA1->BA1_TIPREG >= cUsrDe .And. BA1->BA1_TIPREG <= cUsrAte
				
				cCodPla := If(Empty(BA1->BA1_CODPLA),BA3->BA3_CODPLA,BA1->BA1_CODPLA)
				
				CalculaRC("5",cMesDe,cMesAte,cAno,cCodOpe,BG9->BG9_CODIGO,BA3->BA3_CONEMP,BA3->BA3_VERCON,BA3->BA3_SUBCON,;
				BA3->BA3_VERSUB,BA3->BA3_MATRIC,BA1->BA1_TIPREG,nTpRec,cCodPla,BA3->BA3_TIPOUS)
			Endif
			BA1->(DbSkip())
		Enddo
	Endif
Endif


Return

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Programa   � CriaSX1   � Autor �Geraldo Felix Junior. � Data � 03.02.05 ���
��������������������������������������������������������������������������Ĵ��
���Descricao  � Atualiza SX1                                               ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
/*/

Static Function CriaSX1()

Local aRegs	:=	{}
LOCAL cPerg := "PLM170"

If  ! SX1->(msSeek(cPerg+"19"))
	aadd(aRegs,{cPerg,"19",STR0005,"","","mv_chh","N", 1,0,0,"C","","mv_par19",STR0006    ,"","","","",STR0007    ,"","","","",""         ,"","","","",""       ,"","","","","","","","",""   ,""}) //"Consid para Receita"###"Emissao"###"Baixa"
	
	PlsVldPerg( aRegs )
Endif
    
Return
