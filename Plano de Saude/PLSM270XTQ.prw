#DEFINE CRLF chr( 13 ) + chr( 10 )
#include "totvs.ch"
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} PLSM270XTQ
Importa��o do arquivo de qualidade .XTQ

@author    Jonatas Almeida
@version   1.xx
@since     08/09/2016
/*/
//------------------------------------------------------------------------------------------
function PLSM270XTQ()
	local cTitulo	:= "Importar arquivo de qualidade - TISS"
	local cTexto	:= CRLF + CRLF +;
		"Esta op��o ir� efetuar a leitura do arquivo de qualidade .XTQ"+ CRLF + "a ser disponibilizado pela ANS e importado pela operadora"
	local aOpcoes	:= { "Processar","Cancelar" }
	local nTaman	:= 3
	local nOpc		:= aviso( cTitulo,cTexto,aOpcoes,nTaman )
	
	if( nOpc == 1 )
		obterArquivo() 
	endIf 
return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} obterArquivo
Abre janela de dialogo para importacao do arquivo

@author    Jonatas Almeida
@version   1.xx
@since     08/09/2016
/*/
//------------------------------------------------------------------------------------------
static function obterArquivo()
	Local cMascara		:= "Arquivos .XTQ | *.xtq"
	Local cTitulo		:= "Selecione o arquivo "
	Local nMascpad		:= 0
	Local cRootPath	:= ""
	Local lSalvar		:= .T.	//.F. = Salva || .T. = Abre
	Local nOpcoes		:= nOR( GETF_LOCALHARD,GETF_ONLYSERVER )
	Local l3Server		:= .T.	//.T. = apresenta o �rvore do servidor || .F. = n�o apresenta
	Local cFileXTQ		:= ""
	Local cPath			:= ""
	Local cArqTmp		:= ""

	cFileXTQ := cGetFile( cMascara,cTitulo,nMascpad,cRootPath,lSalvar,nOpcoes,l3Server )
	
	if( !empty( cFileXTQ ) )
		if( at( ":\",cFileXTQ ) <> 0 )	//--< COPIA ARQUIVO TEMPORARIO PARA SERVIDOR >---
			cPath := getNewPar( "MV_TISSDIR","\TISS\" ) + "TEMP\"
			
			if( !existDir( cPath ) )
				makeDir( cPath )
			endIf
			
			if( cpyT2S( cFileXTQ,cPath ) )
				cArqTmp := cPath + substr( cFileXTQ,rat( "\",cFileXTQ ) + 1 )
				lerArquivo( cArqTmp )
				if( fErase( cArqTmp ) == -1 )	//--< EXCLUI ARQUIVO TEMPORARIO >---
					
				endIf
			endIf
		else	//--< ARQUIVO SERVIDOR >---
			lerArquivo( cFileXTQ )
		endIf
	endIf
return
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} LerArquivo
Leitura do arquivo .XTQ utilizado a classe TXmlManager

@author    Jonatas Almeida
@version   1.xx
@since     08/09/2016
@param     cFileXTQ = Arquivo extensao XTQ
/*/
//------------------------------------------------------------------------------------------
static function LerArquivo( cFileXTQ )
	local oXML
	local cError	:= ""
	local lOK		:= .F.
	local aCabec	:= { }
	local aGuias	:= { }
	local ans		:= { }

	//begin sequence
		oXML := TXmlManager():New()
		lOK := oXML:ReadFile( cFileXTQ,,oXML:Parse_nsclean )
		
		if( !lOK )
			cError := "Erro: " + oXML:Error()
			msgAlert( cError )
		else
			//--< REGISTRO NAMESPACE 'ANS' >--
			aNS := oXML:XPathGetRootNsList()
			oXML:XPathRegisterNs( aNS[ 1 ][ 1 ],aNS[ 1 ][ 2 ] )
		
			aCabec := getCabecalho( oXML )
			if( gravaCabec( 4,aCabec ) )
				processa( { || ( aGuias := processaGuias( oXML,aCabec ) ) },"Por favor, aguarde!","Gravando os dados do arquivo de qualidade...",.F. )
				
				if( msgYesNo( "Processo finalizado com sucesso!" + CRLF + "Deseja abrir os detalhes do arquivo rec�m importado?" ) )
					msgRun( "Abrindo janela de detalhes...","Processando, por favor aguarde",{ || staticCall( PLSM270,PLSM270Det ) } )
				endIf
			else
				msgAlert( "Registro n�o encontrado no Monitoramento TISS (B4M)" )
			endIf
		endIf
	
return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} gravaCabec
Gravacao dos dados do cabecalho do arquivo - B4M

@author    Jonatas Almeida
@version   1.xx
@since     08/09/2016
@param     nOpc(tipo de operacao) ,aCabec(dados do cabecalho)
/*/
//------------------------------------------------------------------------------------------
static function gravaCabec( nOpc,aCabec )
	local aCampos	:= {}
	local lRet			:= .F.
	local nPosTipTra := 0
	local nPosNumLot := 0
	local nPosComLot := 0
	local nPosDatReg := 0
	local nPosHorReg := 0
	local nPosRegANS := 0
	local nPosVerPad := 0
	
	nPosTipTra := ascan( aCabec,{ |x| lower( x[ 1 ] ) == "tipotransacao" } )
	nPosNumLot := ascan( aCabec,{ |x| lower( x[ 1 ] ) == "numerolote" } )
	nPosComLot := ascan( aCabec,{ |x| lower( x[ 1 ] ) == "competencialote" } )
	nPosDatReg := ascan( aCabec,{ |x| lower( x[ 1 ] ) == "dataregistrotransacao" } )
	nPosHorReg := ascan( aCabec,{ |x| lower( x[ 1 ] ) == "horaregistrotransacao" } )
	nPosRegANS := ascan( aCabec,{ |x| lower( x[ 1 ] ) == "registroans" } )
	nPosVerPad := ascan( aCabec,{ |x| lower( x[ 1 ] ) == "versaopadrao" } )
			
	aadd( aCampos,{ "B4M_FILIAL"	,xFilial( "B4M" ) } )				// filial
	aadd( aCampos,{ "B4M_SUSEP"		,aCabec[ nPosRegANS ][ 03 ] } )		// operadora
	aadd( aCampos,{ "B4M_STATUS"	,'8' } )							// Encerrado
	aadd( aCampos,{ "B4M_CODUSR"	,retCodUsr() } )					// codigo usuario corrente
	aadd( aCampos,{ "B4M_TISVER"	,aCabec[ nPosVerPad ][ 03 ] } ) 	// versao TISS
	aadd( aCampos,{ "B4M_NUMLOT"	,aCabec[ nPosNumLot ][ 03 ] } )		// numero de lote
	aadd( aCampos,{ "B4M_CMPLOT"	,aCabec[ nPosComLot ][ 03 ] } )		// competencia lote
	aadd( aCampos,{ "B4M_DTPRMO"	,stod( strTran( aCabec[ nPosDatReg ][ 03 ],"-","" ) ) } )	// data de processamento do qualidade
	aadd( aCampos,{ "B4M_HRPRMO"	,aCabec[ nPosHorReg ][ 03 ] } )		// hora de processamento do qualidade
	
	B4M->( dbSetOrder( 1 ) ) //B4M_FILIAL + B4M_SUSEP + B4M_CMPLOT + B4M_NUMLOT + B4M_NMAREN
	if( B4M->( dbSeek( xFilial( "B4M" ) + aCabec[ nPosRegANS ][ 3 ] + aCabec[ nPosComLot ][ 3 ] + aCabec[ nPosNumLot ][ 3 ] ) ) )
		lRet := gravaMonit( nOpc,aCampos,'MODEL_B4M','PLSM270' )
	endIf
return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} getCabecalho
Leitura dos dados do cabecalho

@author    TOTVS | Developer Studio - Gerado pelo Assistente de C�digo
@version   1.xx
@since     08/09/2016

@param     oXML(XML convertido em objeto)
@return    Array contendo os dados do cabecalho
/*/
//------------------------------------------------------------------------------------------
static function getCabecalho( oXML )
	local nx			:= 0
	local aCabec	:= {}
	local aPathTag	:= {}
	local cPathTag	:= ""

	cPathTag := "/ans:mensagemEnvioANS/ans:cabecalho"

	if( oXml:XPathHasNode( cPathTag ) )
		aadd( aPathTag,"/ans:mensagemEnvioANS/ans:cabecalho/ans:identificacaoTransacao/ans:tipoTransacao" )
		aadd( aPathTag,"/ans:mensagemEnvioANS/ans:cabecalho/ans:identificacaoTransacao/ans:numeroLote" )
		aadd( aPathTag,"/ans:mensagemEnvioANS/ans:cabecalho/ans:identificacaoTransacao/ans:competenciaLote" )
		aadd( aPathTag,"/ans:mensagemEnvioANS/ans:cabecalho/ans:identificacaoTransacao/ans:dataRegistroTransacao" )
		aadd( aPathTag,"/ans:mensagemEnvioANS/ans:cabecalho/ans:identificacaoTransacao/ans:horaRegistroTransacao" )
		aadd( aPathTag,"/ans:mensagemEnvioANS/ans:cabecalho/ans:registroANS" )
		aadd( aPathTag,"/ans:mensagemEnvioANS/ans:cabecalho/ans:versaoPadrao" )
	
		for nx:=1 to len( aPathTag )
			if( oXml:XPathHasNode( aPathTag[ nx ] ) )
				aadd( aCabec,{ subStr( aPathTag[ nx ],rat( ":",aPathTag[ nx ])+1 ),aPathTag[ nx ],oXML:XPathGetNodeValue( aPathTag[ nx ] ) } )
			endIf
		next nx
	endIf
return aCabec

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} processaGuias
Leitura dos dados da Guia

@author    Jonatas Almeida
@version   1.xx
@since     08/09/2016

@param     oXML(arquivo .XTQ),aCabec(dados do cabecalho)
@return    aray de guias processadas
/*/
//------------------------------------------------------------------------------------------
static function processaGuias( oXML,aCabec )
	local aGuias		:= { }
	local aGuia			:= { }
	local aPathTag		:= { }
	local aDataProc		:= { }
	local aLanc			:= { }
	local aItens		:= { }
	local aPacote		:= { }
	local aOcoPac		:= { }
	local aOcoPro		:= { }
	local nQtdGuias		:= 0
	local nQtdData		:= 0
	local nQtdLanc		:= 0
	local nQtdOcorre	:= 0
	local nQtdItens		:= 0
	local nQtdAssis		:= 0
	local nQtdPacote	:= 0
	local nQtdOcoPac	:= 0
	local nx			:= 0
	local nz			:= 0
	local ni			:= 0
	local nj			:= 0
	local nk			:= 0
	local nl			:= 0
	local cPathTag		:= ""

	cPathTag := "/ans:mensagemEnvioANS/ans:Mensagem/ans:ansParaOperadora/ans:controleQualidade"

	if( oXml:XPathHasNode( cPathTag ) )
		nQtdGuias := oXML:XPathChildCount( cPathTag )

		//--< Leitura das tags 'ans:guiaAtendimento' >--
		procRegua( nQtdGuias )
		
		for nx:=1 to nQtdGuias
			incProc( nQtdGuias )
			
			cPathTag := "/ans:mensagemEnvioANS/ans:Mensagem/ans:ansParaOperadora/ans:controleQualidade/ans:guiaAtendimento["+ allTrim( str( nx ) ) +"]"
			
			aGuia 	:= { }
			aPathTag:= { }
			
			aadd( aPathTag,cPathTag+"/ans:contratadoExecutante/ans:CNES" )
			aadd( aPathTag,cPathTag+"/ans:contratadoExecutante/ans:identificadorExecutante" )
			aadd( aPathTag,cPathTag+"/ans:contratadoExecutante/ans:codigoCNPJ_CPF" )
			aadd( aPathTag,cPathTag+"/ans:numeroGuiaPrestador" )
			aadd( aPathTag,cPathTag+"/ans:numeroGuiaOperadora" )
			aadd( aPathTag,cPathTag+"/ans:identificadorReembolso" )

			for nz:=1 to len( aPathTag )
				if( oXml:XPathHasNode( aPathTag[ nz ] ) )
					aadd( aGuia,{ subStr( aPathTag[ nz ],rat( ":",aPathTag[ nz ] )+1 ),aPathTag[ nz ],oXML:XPathGetNodeValue( aPathTag[ nz ] ) } )
				endIf
			next nz

			//--< Leitura das tags 'ans:lancamentosRegistradosANS/ans:dataProcessamento' >--
			cPathTag := "/ans:mensagemEnvioANS/ans:Mensagem/ans:ansParaOperadora/ans:controleQualidade/ans:guiaAtendimento["+ allTrim( str( nx ) ) +"]/ans:lancamentosRegistradosANS"
			nQtdData := oXML:XPathChildCount( cPathTag )
			
			aDataProc := { }
			
			for nz:=1 to nQtdData
				cPathTag := "/ans:mensagemEnvioANS/ans:Mensagem/ans:ansParaOperadora/ans:controleQualidade/ans:guiaAtendimento["+ allTrim( str( nx ) ) +"]/ans:lancamentosRegistradosANS/ans:dataProcessamento[ "+ allTrim( str( nz ) ) +" ]"
				
				if( oXml:XPathHasNode( cPathTag ) )
					aadd( aDataProc,oXML:XPathGetNodeValue( cPathTag ) )
				endIf
			next nz
			
			aadd( aGuia,{"ans:dataProcessamento",aDataProc} )

			//--< Leitura das tags 'ans:lancamentosCompetencia' >--
			cPathTag := "/ans:mensagemEnvioANS/ans:Mensagem/ans:ansParaOperadora/ans:controleQualidade/ans:guiaAtendimento["+ allTrim( str( nx ) ) +"]/ans:lancamentosCompetencia"
			nQtdLanc := oXML:XPathChildCount( cPathTag )
			aLanc := { }

			for nz:=1 to nQtdLanc
				cPathTag := "/ans:mensagemEnvioANS/ans:Mensagem/ans:ansParaOperadora/ans:controleQualidade/ans:guiaAtendimento["+ allTrim( str( nx ) ) +"]/ans:lancamentosCompetencia/ans:lancamento["+ allTrim( str( nz ) ) +"]/ans:dataProcessamento"
				
				if( oXml:XPathHasNode( cPathTag ) )
					aadd( aLanc, { subStr( cPathTag,rat( ":",cPathTag )+1 ),cPathTag,oXML:XPathGetNodeValue( cPathTag ) } )
				endIf

				//--< Leitura das tags 'ans:ocorrenciasLancamento' >--
				aOcorren := { }
				cPathTag := "/ans:mensagemEnvioANS/ans:Mensagem/ans:ansParaOperadora/ans:controleQualidade/ans:guiaAtendimento["+ allTrim( str( nx ) ) +"]/ans:lancamentosCompetencia/ans:lancamento["+ allTrim( str( nz ) ) +"]/ans:ocorrenciasLancamento"
				nQtdOcorre := oXML:XPathChildCount( cPathTag )

				for ni:=1 to nQtdOcorre
					cPathTag := "/ans:mensagemEnvioANS/ans:Mensagem/ans:ansParaOperadora/ans:controleQualidade/ans:guiaAtendimento["+ allTrim( str( nx ) ) +"]/ans:lancamentosCompetencia/ans:lancamento["+ allTrim( str( nz ) ) +"]/ans:ocorrenciasLancamento/ans:ocorrencia["+ allTrim( str( ni ) ) +"]"
					
					aPathTag := { }
					
					aadd( aPathTag, cPathTag+"/ans:identificadorCampo" )
					aadd( aPathTag, cPathTag+"/ans:conteudoCampo" )
					aadd( aPathTag, cPathTag+"/ans:codigoErro" )
					
					for nj:=1 to len( aPathTag )
						if( oXml:XPathHasNode( aPathTag[ nj ] ) )
							aadd( aOcorren,{ subStr( aPathTag[ nj ],rat( ":",aPathTag[ nj ] )+1 ),aPathTag[ nj ],oXML:XPathGetNodeValue( aPathTag[ nj ] ) } )
						endIf
					next nj
				next ni
				aadd( aLanc,{ "ans:ocorrenciasLancamento",{ aOcorren } } )

				//--< Leitura das tags 'ans:itensLancamento' >--
				cPathTag := "/ans:mensagemEnvioANS/ans:Mensagem/ans:ansParaOperadora/ans:controleQualidade/ans:guiaAtendimento["+ allTrim( str( nx ) ) +"]/ans:lancamentosCompetencia/ans:lancamento["+ allTrim( str( nz ) ) +"]/ans:itensLancamento"
				nQtdItens := oXML:XPathChildCount( cPathTag )
				aItens := { }

				for ni:=1 to nQtdItens
					cPathTag := "/ans:mensagemEnvioANS/ans:Mensagem/ans:ansParaOperadora/ans:controleQualidade/ans:guiaAtendimento["+ allTrim( str( nx ) ) +"]/ans:lancamentosCompetencia/ans:lancamento["+ allTrim( str( nz ) ) +"]/ans:itensLancamento/ans:procedimentoItemAssistencial["+ allTrim( str( ni ) ) +"]"
					
					aPathTag := { }
					
					aadd( aPathTag,cPathTag+"/ans:codigoTabela" )
					aadd( aPathTag,cPathTag+"/ans:procedimento/ans:grupoProcedimento" )
					aadd( aPathTag,cPathTag+"/ans:procedimento/ans:codigoProcedimento" )
					aadd( aPathTag,cPathTag+"/ans:denteRegiao/ans:codDente" )
					aadd( aPathTag,cPathTag+"/ans:denteRegiao/ans:codRegiao" )
					aadd( aPathTag,cPathTag+"/ans:denteFace" )
					
					for nj:=1 to len( aPathTag )
						if( oXml:XPathHasNode( aPathTag[ nj ] ) )
							aadd( aItens,{ subStr( aPathTag[ nj ],rat( ":",aPathTag[ nj ] )+1 ),aPathTag[ nj ],oXML:XPathGetNodeValue( aPathTag[ nj ] ) } )
						endIf
					next nj
					
					cPathTag	:= "/ans:mensagemEnvioANS/ans:Mensagem/ans:ansParaOperadora/ans:controleQualidade/ans:guiaAtendimento["+ allTrim( str( nx ) ) +"]/ans:lancamentosCompetencia/ans:lancamento["+ allTrim( str( nz ) ) +"]/ans:itensLancamento/ans:procedimentoItemAssistencial["+ allTrim( str( ni ) ) +"]/ans:ocorrenciasProcedimentoItemAssistencial"
					nQtdAssis	:= oXML:XPathChildCount( cPathTag )
					aOcoPro		:= {}
					
					//--< Leitura das tags 'ans:ocorrenciasProcedimentoItemAssistencial' >--
					for nj:=1 to nQtdAssis
						cPathTag := "/ans:mensagemEnvioANS/ans:Mensagem/ans:ansParaOperadora/ans:controleQualidade/ans:guiaAtendimento["+ allTrim( str( nx ) ) +"]/ans:lancamentosCompetencia/ans:lancamento["+ allTrim( str( nz ) ) +"]/ans:itensLancamento/ans:procedimentoItemAssistencial["+ allTrim( str( ni ) ) +"]/ans:ocorrenciasProcedimentoItemAssistencial/ans:ocorrencia["+ allTrim( str( nj ) ) +"]"
						
						aPathTag := { }
						
						aadd( aPathTag,cPathTag+"/ans:identificadorCampo" )
						aadd( aPathTag,cPathTag+"/ans:conteudoCampo" )
						aadd( aPathTag,cPathTag+"/ans:codigoErro" )
						
						for nl:=1 to len( aPathTag )
							if( oXml:XPathHasNode( aPathTag[ nl ] ) )
								aadd( aOcoPro,{ subStr( aPathTag[ nl ],rat( ":",aPathTag[ nl ] )+1 ),aPathTag[ nl ],oXML:XPathGetNodeValue( aPathTag[ nl ] ) } )
							endIf
						next nl
					next nj
					aadd( aItens,{ "ans:ocorrenciasProcedimentoItemAssistencial",{ aOcoPro } } )
					
					//--< Leitura das tags 'ans:detalhamentoPacote' >--
					cPathTag	:= "/ans:mensagemEnvioANS/ans:Mensagem/ans:ansParaOperadora/ans:controleQualidade/ans:guiaAtendimento["+ allTrim( str( nx ) ) +"]/ans:lancamentosCompetencia/ans:lancamento["+ allTrim( str( nz ) ) +"]/ans:itensLancamento/ans:procedimentoItemAssistencial["+ allTrim( str( ni ) ) +"]/ans:detalhamentoPacote"
					nQtdPacote	:= oXML:XPathChildCount( cPathTag )
					aPacote		:= { }
					
					for nj:=1 to nQtdPacote
						cPathTag := "/ans:mensagemEnvioANS/ans:Mensagem/ans:ansParaOperadora/ans:controleQualidade/ans:guiaAtendimento["+ allTrim( str( nx ) ) +"]/ans:lancamentosCompetencia/ans:lancamento["+ allTrim( str( nz ) ) +"]/ans:itensLancamento/ans:procedimentoItemAssistencial["+ allTrim( str( ni ) ) +"]/ans:detalhamentoPacote/ans:pacote["+ allTrim( str( nj ) ) +"]"
						
						aPathTag := { }
						
						aadd( aPathTag,cPathTag+"/ans:codigoTabela" )
						aadd( aPathTag,cPathTag+"/ans:codigoProcedimento" )
						
						for nl:=1 to len( aPathTag )
							if( oXml:XPathHasNode( aPathTag[ nl ] ) )
								aadd( aPacote,{ subStr( aPathTag[ nl ],rat( ":",aPathTag[ nl ] )+1 ),aPathTag[ nl ],oXML:XPathGetNodeValue( aPathTag[ nl ] ) } )
							endIf
						next nl
						
						cPathTag	:= "/ans:mensagemEnvioANS/ans:Mensagem/ans:ansParaOperadora/ans:controleQualidade/ans:guiaAtendimento["+ allTrim( str( nx ) ) +"]/ans:lancamentosCompetencia/ans:lancamento["+ allTrim( str( nz ) ) +"]/ans:itensLancamento/ans:procedimentoItemAssistencial["+ allTrim( str( ni ) ) +"]/ans:detalhamentoPacote/ans:pacote["+ allTrim( str( nj ) ) +"]/ans:ocorrenciasPacote"
						nQtdOcoPac	:= oXML:XPathChildCount( cPathTag )
						
						for nk:=1 to nQtdOcoPac
							aOcoPac		:= { }
							aPathTag	:= { }
							
							cPathTag := "/ans:mensagemEnvioANS/ans:Mensagem/ans:ansParaOperadora/ans:controleQualidade/ans:guiaAtendimento["+ allTrim( str( nx ) ) +"]/ans:lancamentosCompetencia/ans:lancamento["+ allTrim( str( nz ) ) +"]/ans:itensLancamento/ans:procedimentoItemAssistencial["+ allTrim( str( ni ) ) +"]/ans:detalhamentoPacote/ans:pacote["+ allTrim( str( nj ) ) +"]/ans:ocorrenciasPacote/ans:ocorrencia["+ allTrim( str( nk ) ) +"]"

							aadd( aPathTag,cPathTag+"/ans:identificadorCampo" )
							aadd( aPathTag,cPathTag+"/ans:conteudoCampo" )
							aadd( aPathTag,cPathTag+"/ans:codigoErro" )
							
							for nl:=1 to len( aPathTag )
								if( oXml:XPathHasNode( aPathTag[ nl ] ) )
									aadd( aOcoPac,{ subStr( aPathTag[ nl ],rat( ":",aPathTag[ nl ] )+1 ),aPathTag[ nl ],oXML:XPathGetNodeValue( aPathTag[ nl ] ) } )
								endIf
							next nl
							
							aadd( aPacote,{ "ans:ocorrenciasPacote",{ aOcoPac } } )
						next nk
					next nj
					
					aadd( aItens,{ "ans:detalhamentoPacote",{ aPacote } } )
				next ni
				
				aadd( aLanc,{ "ans:itensLancamento",{ aItens } } )
			next nz
			
			aadd( aGuia,{ "ans:lancamentosCompetencia",{ aLanc } } )
			aadd( aGuias,aGuia )

			gravaGuia( aCabec,aGuia )
		next nx
	endIf
return aGuias

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} gravaGuia
Efetua a grava��o dos dados da Guia

@author    Jonatas Almeida
@version   1.xx
@since     20/09/2016

@param     aCabec(dados do cabecalho),aGuia(dados da guia)
/*/
//------------------------------------------------------------------------------------------
static function gravaGuia( aCabec,aGuia )
	//--< POSICOES DOS ELEMENTOS DO CABECALHO >---
	local nPosCbNLot := 0
	local nPosCbCLot := 0
	local nPosCbRANS := 0
	local nPosCbVPad := 0

	//--< POSICOES DOS ELEMENTOS DA GUIA >---
	local nPosCNES	 := 0
	local nPosIDExec := 0
	local nPosCPFCNP := 0
	local nPosNGPres := 0
	local nPosNGOper := 0
	local nPosIDReem := 0
	local nPosDatPrc := 0
	local nPosLanCom := 0

	//--< POSICOES DOS ELEMENTOS DA "ans:lancamentosCompetencia" >---
	local aLanCom	 := { }
	local nLanCom	 := 0
	local nPosDatLan := 0
	local nPosOcoLan := 0
	local nPosIteLan := 0
	
	//--< POSICOES DOS ELEMENTOS DA "ans:ocorrenciasLancamento" >---
	local aOcoLan	 := { }
	local nOcoLan	 := 0
	local nPosOLICpo := 0
	local nPosOLCCpo := 0
	local nPosOLCerr := 0
	
	//--< POSICOES DOS ELEMENTOS DA "ans:itensLancamento" >---
	local aIteLan	 := { }
	local nIteLan	 := 0
	local nPosItCTab := 0
	local nPosItGPrc := 0
	local nPosItCPrc := 0
	local nPosItCDen := 0
	local nPosItCReg := 0
	local nPosItDFac := 0
	local nPosIteOco := 0
	local nPosDetPac := 0
	
	//--< POSICOES DOS ELEMENTOS DA "ans:ocorrenciasprocedimentoitemassistencial" >---
	local aOcoIte	 := { }
	local nOcoIte	 := 0
	local nPosOcICpo := 0
	local nPosOcCCpo := 0
	local nPosOcCErr := 0
	
	//--< POSICOES DOS ELEMENTOS DA "ans:detalhamentopacotel" >---
	local aDetPac	 := { }
	local nDetPac	 := 0
	local nPosPcCTab := 0
	local nPosPcCPrc := 0
	local nPosPcOcor := 0
	
	//--< POSICOES DOS ELEMENTOS DA "ans:ocorrenciasPacote" >---
	local aOcoPac	 := { }
	local nOcoPac	 := 0
	local nPosOPICpo := 0
	local nPosOPCCpo := 0
	local nPosOPCErr := 0
	
	//--< INFORMACOES DE CHAVE DE ACESSO >---
	local cRegANS	:= "" // Registro ANS
	local cCmpLote	:= "" // Competencia do lote
	local cNumLote	:= "" // Numero do lote
	local cNGuiaOP	:= "" // Numero guia operadora
	local cNGuiaPR	:= "" // Numero guia prestador
	
	//--< INFORMACOES DE COMPLEMENTARES >---
	local cCodTab	:= ""
	local cCodPro	:= ""
	local cGrpPro	:= ""
	local cCodDente	:= ""
	local cCodRegiao:= ""
	local cDenteFace:= ""
	local cStatus	:= ""
	local aCpos		:= { }
	local lRet		:= .F.
	
	//--< CONTROLE DE REPETICAO >---
	local nx := 0
	local ny := 0
	
	//--< MAPEAMENTO DAS POSICOES DOS ELEMENTOS DO CABECALHO >---
	nPosCbNLot := ascan( aCabec,{ |x| lower( x[ 1 ] ) == "numerolote" } )
	nPosCbCLot := ascan( aCabec,{ |x| lower( x[ 1 ] ) == "competencialote" } )
	nPosCbRANS := ascan( aCabec,{ |x| lower( x[ 1 ] ) == "registroans" } )
	nPosCbVPad := ascan( aCabec,{ |x| lower( x[ 1 ] ) == "versaopadrao" } )
	
	//--< MAPEAMENTO DAS POSICOES DOS ELEMENTOS DA GUIA >---
	nPosCNES	:= ascan( aGuia,{|x| lower( x[ 1 ] ) == "cnes"} )
	nPosIDExec	:= ascan( aGuia,{|x| lower( x[ 1 ] ) == "identificadorexecutante"} )
	nPosCPFCNP	:= ascan( aGuia,{|x| lower( x[ 1 ] ) == "codigocnpj_cpf"} )
	nPosNGPres	:= ascan( aGuia,{|x| lower( x[ 1 ] ) == "numeroguiaprestador"} )
	nPosNGOper	:= ascan( aGuia,{|x| lower( x[ 1 ] ) == "numeroguiaoperadora"} )
	nPosIDReem	:= ascan( aGuia,{|x| lower( x[ 1 ] ) == "identificadorreembolso"} )
	nPosDatPrc	:= ascan( aGuia,{|x| lower( x[ 1 ] ) == "ans:dataprocessamento"} ) //TODO: Pendente de mapeamento
	nPosLanCom	:= ascan( aGuia,{|x| lower( x[ 1 ] ) == "ans:lancamentoscompetencia"} )
	
	//--< MAPEAMENTO DAS POSICOES DOS ELEMENTOS DA "ans:lancamentosCompetencia" >---
	if( nPosLanCom > 0 )
		aLanCom	:= aGuia[ nPosLanCom ][ 2 ]
		nLanCom	:= len( aLanCom )
		
		if( nLanCom > 0 )
			nPosDatLan	:= ascan( aLanCom[ nLanCom ],{|x| lower( x[ 1 ] ) == "dataprocessamento"} )
			nPosOcoLan	:= ascan( aLanCom[ nLanCom ],{|x| lower( x[ 1 ] ) == "ans:ocorrenciaslancamento" } )
			nPosIteLan	:= ascan( aLanCom[ nLanCom ],{|x| lower( x[ 1 ] ) == "ans:itenslancamento" } )
		endIf
	endIf
	
	//--< MAPEAMENTO DAS POSICOES DOS ELEMENTOS DA "ans:ocorrenciasLancamento" >---
	if( nPosOcoLan > 0 )
		aOcoLan := aLanCom[ nLanCom ][ nPosOcoLan ][ 2 ]
		nOcoLan := len( aOcoLan )
		
		if( nOcoLan > 0 )
			nPosOLICpo := ascan( aOcoLan[ nOcoLan ],{|x| lower( x[ 1 ] ) == "identificadorcampo" } )
			nPosOLCCpo := ascan( aOcoLan[ nOcoLan ],{|x| lower( x[ 1 ] ) == "conteudocampo" } )
			nPosOLCerr := ascan( aOcoLan[ nOcoLan ],{|x| lower( x[ 1 ] ) == "codigoerro" } )
		endIf
	endIf
	
	//--< MAPEAMENTO DAS POSICOES DOS ELEMENTOS DA "ans:itensLancamento" >---
	if( nPosIteLan > 0 )
		aIteLan := aLanCom[ nOcoLan ][ nPosIteLan ][ 2 ]
		nIteLan := len( aIteLan )
		
		if( nIteLan > 0 )
			nPosItCTab	:= ascan( aIteLan[ nIteLan ],{|x| lower( x[ 1 ] ) == "codigotabela" } )
			nPosItGPrc	:= ascan( aIteLan[ nIteLan ],{|x| lower( x[ 1 ] ) == "grupoprocedimento" } )
			nPosItCPrc	:= ascan( aIteLan[ nIteLan ],{|x| lower( x[ 1 ] ) == "codigoprocedimento" } )
			nPosItCDen	:= ascan( aIteLan[ nIteLan ],{|x| lower( x[ 1 ] ) == "coddente" } )
			nPosItCReg	:= ascan( aIteLan[ nIteLan ],{|x| lower( x[ 1 ] ) == "codregiao" } )
			nPosItDFac	:= ascan( aIteLan[ nIteLan ],{|x| lower( x[ 1 ] ) == "denteface" } )
			nPosIteOco	:= ascan( aIteLan[ nIteLan ],{|x| lower( x[ 1 ] ) == "ans:ocorrenciasprocedimentoitemassistencial" } )
			nPosDetPac	:= ascan( aIteLan[ nIteLan ],{|x| lower( x[ 1 ] ) == "ans:detalhamentopacote" } )
		endIf
	endIf
	
	//--< MAPEAMENTO DAS POSICOES DOS ELEMENTOS DA "ans:ocorrenciasprocedimentoitemassistencial" >---
	if( nPosIteOco > 0 )
		aOcoIte := aIteLan[ nIteLan ][ nPosIteOco ][ 2 ]
		nOcoIte := len( aIteLan )
		
		if( nOcoIte > 0)
			nPosOcICpo := ascan( aOcoIte[ nOcoIte ],{|x| lower( x[ 1 ] ) == "identificadorcampo" } )
			nPosOcCCpo := ascan( aOcoIte[ nOcoIte ],{|x| lower( x[ 1 ] ) == "conteudocampo" } )
			nPosOcCErr := ascan( aOcoIte[ nOcoIte ],{|x| lower( x[ 1 ] ) == "codigoerro" } )
		endIf
	endIf
	
	//--< MAPEAMENTO DAS POSICOES DOS ELEMENTOS DA "ans:detalhamentopacotel" >---
	if( nPosDetPac > 0 )
		aDetPac := aIteLan[ nIteLan ][ nPosDetPac ][ 2 ]
		nDetPac := len( aDetPac )
		
		if( nDetPac > 0 )
			nPosPcCTab := ascan( aDetPac[ nDetPac ],{|x| lower( x[ 1 ] ) == "codigotabela" } )
			nPosPcCPrc := ascan( aDetPac[ nDetPac ],{|x| lower( x[ 1 ] ) == "codigoprocedimento" } )
			nPosPcOcor := ascan( aDetPac[ nDetPac ],{|x| lower( x[ 1 ] ) == "ans:ocorrenciaspacote" } )
		endIf
	endIf
	
	//--< MAPEAMENTO DAS POSICOES DOS ELEMENTOS DA "ans:ocorrenciasPacote" >---
	if( nPosPcOcor > 0 )
		aOcoPac := aDetPac[ nDetPac ][ nPosPcOcor ][ 2 ]
		nOcoPac := len( aOcoPac )
		
		if( nOcoPac > 0 )
			nPosOPICpo := ascan( aOcoPac[ nOcoPac ],{|x| lower( x[ 1 ] ) == "identificadorcampo" } )
			nPosOPCCpo := ascan( aOcoPac[ nOcoPac ],{|x| lower( x[ 1 ] ) == "conteudocampo" } )
			nPosOPCErr := ascan( aOcoPac[ nOcoPac ],{|x| lower( x[ 1 ] ) == "codigoerro" } )
		endIf
	endIf
	
	//--< INFORMACOES DE CHAVE DE ACESSO >---
	cRegANS  := padR( aCabec[ nPosCbRANS ][ 3 ],tamSX3( "B4N_SUSEP" )[ 1 ] )	// Registro ANS
	cCmpLote := padR( aCabec[ nPosCbCLot ][ 3 ],tamSX3( "B4N_CMPLOT" )[ 1 ] )	// Competencia do lote
	cNumLote := padR( aCabec[ nPosCbNLot ][ 3 ],tamSX3( "B4N_NUMLOT" )[ 1 ] )	// Numero do lote
	cNGuiaOP := padR( aGuia[ nPosNGOper ][ 3 ],tamSX3( "B4N_NMGOPE" )[ 1 ] )	// Numero guia operadora
	cNGuiaPR := padR( aGuia[ nPosNGPres ][ 3 ],tamSX3( "B4N_NMGPRE" )[ 1 ] )	// Numero guia prestador

	//--< *** GRAVA AS OCORRENCIAS DAS GUIAS *** >---
	for nx:=1 to len( aOcoLan )
		cIDCpo		:= padR( aOcoLan[ nx ][ nPosOLICpo ][ 3 ],tamSX3( "B4P_CDCMGU" )[ 1 ] )	// identificadorcampo
		cContCpo	:= aOcoLan[ nx ][ nPosOLCCpo ][ 3 ] 									// conteudocampo
		cCodErro	:= padR( aOcoLan[ nx ][ nPosOLCerr ][ 3 ],tamSX3( "B4P_CDCMER" )[ 1 ] ) // codigoerro
		cDescErro	:= allTrim( posicione( "BTQ",1,xFilial( "BTQ" ) + '38' + cCodErro,"BTQ_DESTER" ) )
		
		B4P->( dbSetOrder( 2 ) ) //B4P_FILIAL + B4P_CMPLOT + B4P_NUMLOT + B4P_NMGOPE + B4P_NMGPRE
		if( !B4P->( dbSeek( xFilial( "B4P" ) + cCmpLote + cNumLote + cNGuiaOP + cNGuiaPR ) ) )
			aCpos := { }
			aAdd( aCpos,{ "B4P_FILIAL"	,xFilial( "B4P" ) } )	// Filial
			aAdd( aCpos,{ "B4P_CDCMGU"	,cIDCpo } )				// ID campo GUIA
			aAdd( aCpos,{ "B4P_CDCMER"	,cCodErro } )			// Codigo Erro
			aadd( aCpos,{ "B4P_DESERR"	,cDescErro } ) 			// Descricao do Erro
			aAdd( aCpos,{ "B4P_SUSEP"	,cRegANS } )			// Registro ANS
			aAdd( aCpos,{ "B4P_CMPLOT"	,cCmpLote } )			// Competencia do lote
			aAdd( aCpos,{ "B4P_NUMLOT"	,cNumLote } )			// Numero do lote
			aAdd( aCpos,{ "B4P_NMGOPE"	,cNGuiaOP } )			// Numero guia operadora
			aAdd( aCpos,{ "B4P_NMGPRE"	,cNGuiaPR } )			// Numero guia prestador
			aAdd( aCpos,{ "B4P_NIVERR"	,"G" } )				// Nivel do erro
			aAdd( aCpos,{ "B4P_ORIERR"	,"3" } )				// 1-sistema | 2-retorno | 3-qualidade
							
			lRet := gravaMonit( 3,aCpos,'MODEL_B4P','PLSM270B4P' )
			cStatus := '7'
		endIf
		
		B4N->( dbSetOrder( 1 ) ) // B4N_FILIAL + B4N_SUSEP + B4N_CMPLOT + B4N_NUMLOT + B4N_NMGOPE
		if( B4N->( dbSeek( xFilial( "B4N" ) + cRegANS + cCmpLote + cNumLote + cNGuiaOP ) ) )
			aCpos := { }
			aAdd( aCpos,{ "B4N_ORIERR",'3' } )				// 1-sistema | 2-retorno | 3-qualidade
			lRet := gravaMonit( 4,aCpos,'MODEL_B4N','PLSM270B4N' )
		endIf
	next nx

	//--< *** GRAVA AS OCORRENCIAS DOS EVENTOS *** >---
	for nx:=1 to len( aIteLan )
		cCodTab := padR( aIteLan[ nx ][ nPosItCTab ][ 3 ],tamSX3( "B4O_CODTAB" )[ 1 ] )	// codigoTabela
		if( nPosPcCPrc > 0 )
			cCodPro := padR( aIteLan[ nx ][ nPosItCPrc ][ 3 ],tamSX3( "B4O_CODPRO" )[ 1 ] )	// codigoprocedimento
		endIf
		
		if( nPosItGPrc > 0 )
			cGrpPro := padR( aIteLan[ nx ][ nPosItGPrc ][ 3 ],tamSX3( "B4O_CODGRU" )[ 1 ] )	// grupoprocedimento
		endIf
		
		cCodDente  := aIteLan[ nx ][ nPosItCDen ][ 3 ] // coddente
		cCodRegiao := aIteLan[ nx ][ nPosItCReg ][ 3 ] // codregiao
		
		if( nPosItDFac > 0 )
			cDenteFace := aIteLan[ nx ][ nPosItDFac ][ 3 ] // denteface
		else
			cDenteFace := ""
		endIf
		
		B4O->( dbSetOrder( 1 ) ) //B4O_FILIAL+B4O_SUSEP+B4O_CMPLOT+B4O_NUMLOT+B4O_NMGOPE+B4O_NMGPRE+B4O_CODGRU+B4O_CODTAB+B4O_CODPRO+B4O_DATREA							
		if( B4O->( dbSeek( xFilial( "B4O" ) + cRegANS + cCmpLote + cNumLote + cNGuiaOP + cNGuiaPR + cGrpPro + cCodTab + cCodPro ) ) )
			aCpos := { }
			aadd( aCpos,{ "B4O_ORIERR","3" } )					// 1-sistema | 2-retorno | 3-qualidade
			lRet := gravaMonit( 4,aCpos,'MODEL_B4O','PLSM270B4O' )
		endIf
		
		for ny:=1 to len( aOcoIte )
			cIDCpo		:= padR( aOcoIte[ ny ][ nPosOcICpo ][ 3 ],tamSX3( "B4P_CDCMGU" )[ 1 ] )	// identificadorcampo
			cContCpo	:= aOcoIte[ ny ][ nPosOcCCpo ][ 3 ] 									// conteudocampo
			cCodErro	:= padR( aOcoIte[ ny ][ nPosOcCErr ][ 3 ],tamSX3( "B4P_CDCMER" )[ 1 ] ) // codigoerro
			cDescErro	:= allTrim( posicione( "BTQ",1,xFilial( "BTQ" ) + '38' + cCodErro,"BTQ_DESTER" ) )
			
			B4P->( dbSetOrder( 1 ) ) //B4P_FILIAL+B4P_SUSEP+B4P_CMPLOT+B4P_NUMLOT+B4P_NMGOPE+B4P_CODGRU+B4P_CODPAD+B4P_CODPRO+B4P_CDCMER
			if( !B4P->( dbSeek( xFilial( "B4P" ) + cRegANS + cCmpLote + cNumLote + cNGuiaOP + cGrpPro + cCodTab + cCodPro + cCodErro ) ) )
				aCpos := { }
				aAdd( aCpos,{ "B4P_FILIAL"	,xFilial( "B4P" ) } )	// Filial
				aAdd( aCpos,{ "B4P_CDCMGU"	,cIDCpo } )				// ID campo GUIA
				aAdd( aCpos,{ "B4P_CDCMER"	,cCodErro } )			// Codigo Erro
				aadd( aCpos,{ "B4P_DESERR"	,cDescErro } ) 			// Descricao do Erro
				aAdd( aCpos,{ "B4P_SUSEP"	,cRegANS } )			// Registro ANS
				aAdd( aCpos,{ "B4P_CMPLOT"	,cCmpLote } )			// Competencia do lote
				aAdd( aCpos,{ "B4P_NUMLOT"	,cNumLote } )			// Numero do lote
				aAdd( aCpos,{ "B4P_NMGOPE"	,cNGuiaOP } )			// Numero guia operadora
				aAdd( aCpos,{ "B4P_NMGPRE"	,cNGuiaPR } )			// Numero guia prestador
				aAdd( aCpos,{ "B4P_NIVERR"	,"E" } )				// Nivel do erro
				aAdd( aCpos,{ "B4P_CODPAD"	,cCodTab } )			// Codigo da tabela
				aAdd( aCpos,{ "B4P_CODPRO"	,cCodPro } )			// Codigo Procedimento
				aAdd( aCpos,{ "B4P_ORIERR"	,"3" } )				// 1-sistema | 2-retorno | 3-qualidade
				If B4P->(FieldPos("B4P_CODGRU")) > 0
					aadd( aCampos,{ "B4P_CODGRU", 	cGrpPro				} ) //Grupo
				EndIf
				
				lRet := gravaMonit( 3,aCpos,'MODEL_B4P','PLSM270B4P' )
				cStatus := '7'
			endIf
		next ny
	next nx

	//--< *** GRAVA AS OCORRENCIAS DOS PACOTES *** >---
	for nx:=1 to len( aDetPac )
		cCodTab := padR( aDetPac[ nx ][ nPosPcCTab ][ 3 ],tamSX3( "B4P_CODPAD" )[ 1 ] )			// codigoTabela
		cCodPro := padR( aDetPac[ nx ][ nPosPcCPrc ][ 3 ],tamSX3( "B4P_CODPRO" )[ 1 ] )			// codigoprocedimento
		
		for ny:=1 to len( aOcoPac )
			cIDCpo		:= aOcoPac[ ny ][ nPosOPICpo ][ 3 ]										// identificadorcampo
			cContCpo	:= aOcoPac[ ny ][ nPosOPCCpo ][ 3 ] 									// conteudocampo
			cCodErro	:= padR( aOcoPac[ ny ][ nPosOPCErr ][ 3 ],tamSX3( "B4P_CDCMER" )[ 1 ] ) // codigoerro
			cDescErro	:= allTrim( posicione( "BTQ",1,xFilial( "BTQ" ) + "38" + cCodErro,"BTQ_DESTER" ) )
			
			B4P->( dbSetOrder( 1 ) ) //B4P_FILIAL+B4P_SUSEP+B4P_CMPLOT+B4P_NUMLOT+B4P_NMGOPE+B4P_CODPAD+B4P_CODPRO+B4P_CDCMER
			if( !B4P->( dbSeek( xFilial( "B4P" ) + cRegANS + cCmpLote + cNumLote + cNGuiaOP + cCodTab + cCodPro + cCodErro ) ) )
				aCpos := { }
				aAdd( aCpos,{ "B4P_FILIAL"	,xFilial( "B4P" ) } )	// Filial
				aAdd( aCpos,{ "B4P_CDCMGU"	,cIDCpo } )				// ID campo GUIA
				aAdd( aCpos,{ "B4P_CDCMER"	,cCodErro } )			// Codigo Erro
				aadd( aCpos,{ "B4P_DESERR"	,cDescErro } ) 			// Descricao do Erro
				aAdd( aCpos,{ "B4P_SUSEP"	,cRegANS } )			// Registro ANS
				aAdd( aCpos,{ "B4P_CMPLOT"	,cCmpLote } )			// Competencia do lote
				aAdd( aCpos,{ "B4P_NUMLOT"	,cNumLote } )			// Numero do lote
				aAdd( aCpos,{ "B4P_NMGOPE"	,cNGuiaOP } )			// Numero guia operadora
				aAdd( aCpos,{ "B4P_NMGPRE"	,cNGuiaPR } )			// Numero guia prestador
				aAdd( aCpos,{ "B4P_NIVERR"	,"P" } )				// Nivel do erro
				aAdd( aCpos,{ "B4P_CODPAD"	,cCodTab } )			// Codigo da tabela
				aAdd( aCpos,{ "B4P_CODPRO"	,cCodPro } )			// Codigo Procedimento
				aAdd( aCpos,{ "B4P_ORIERR"	,"3" } )				// 1-sistema | 2-retorno | 3-qualidade
				
				lRet := gravaMonit( 3,aCpos,'MODEL_B4P','PLSM270B4P' )
				cStatus := '7'
			endIf
		next ny
	next nx
	
	if( !empty( cStatus ) )
		aCampos := {}
		aadd( aCampos,{ "B4M_STATUS",cStatus } )	// status - Arq Qualidade (criticado)
		gravaMonit( 4,aCampos,'MODEL_B4M','PLSM270' )
	endIf

return lRet
