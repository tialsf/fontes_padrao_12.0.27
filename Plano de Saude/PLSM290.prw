#include "Protheus.ch"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PLSM290   �Autor  �Microsiga           � Data �  10/26/12   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function PLSM290()
Local aSay    := {}
Local aButton := {}
Local nOpc    := 0
Local cTitulo := "Grava hist�rio de plano de Saude"
Local cDesc1  := "Este programa tem o objetivo de gerar dados retroativos para a DIRF, "
Local cDesc2  := "alimentando os arquivos RHP - Historico do reembolso e co-participa��o, "
Local cDesc3  := " e RHS- Historico do plano de sa�de. " 

Private cPerg := "PLSM290"

Pergunte(cPerg,.F.)

aAdd( aSay, cDesc1 )
aAdd( aSay, cDesc2 )
aAdd( aSay, cDesc3 )

aAdd( aButton, { 5, .T., {|| Pergunte(cPerg,.T. )    }} )
aAdd( aButton, { 1, .T., {|| nOpc := 1, FechaBatch() }} )
aAdd( aButton, { 2, .T., {|| FechaBatch()            }} )

FormBatch( cTitulo, aSay, aButton )

If nOpc <> 1
   Return
Endif

Processa( {|lEnd| PLSM290Pro(@lEnd)}, "Aguarde...","Executando rotina.", .T. )

Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PLSM290   �Autor  �Microsiga           � Data �  10/26/12   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function PLSM290Pro(lEnd)
Local cQuery := ""
Local cTpPlan := ""
Local cCodFor := ""
Local cTpForn  :=""
Local cSeqDep  :=""
Local cPlanGPE :=""
Local cVerba :=""
Local nInd   := 0 
Local cOrigem :=""
Local aVetAux  := {}
Local aAcuFun:={}
Local aCriUsr:={}
Local aCabUsr    := { {"Matricula PLS","@!",60},{"Nome do Usuario","@!",150},{"Critica","@!",250} }
Local aCriPrd:={}
Local aCabPrd    := { {"Produto/Versao","@!",10},{"Descri��o","@!",50},{"Critica","@!",250} }
Local aCriLct:={}
Local aCabLct := { {"Lan�amento de Faturamento","@!",06},{"Descri��o","@!",50},{"Critica","@!",250} }


cQuery:=" SELECT BM1_CODINT,BM1_CODEMP,BM1_CONEMP,BM1_VERCON, BM1_SUBCON, BM1_VERSUB,BM1_MATRIC,BM1_NOMUSR,BM1_TIPREG,BM1_DIGITO,BM1_VALOR,BM1_CODTIP,	BM1_AGFTFU,BM1_AGMTFU, "
cQuery+=" BM1_VERSIG,BM1_ANO,BM1_MES,BM1_PLNUCO,BM1_TIPREG,BM1_TIPUSU, BFQ_TPLAN,BFQ_VERBA,BA3_CODINT, BA3_CODPLA, BA3_VERSAO "
cQuery+=" FROM "+RetSqlName("BM1")+ " BM1 "
cQuery+= " INNER JOIN "+RetSqlName("BFQ")+" BFQ ON BFQ_FILIAL='"+xFilial("BFQ")+"' AND BFQ_CODINT=BM1_CODINT AND  BFQ_PROPRI|| BFQ_CODLAN = BM1_CODTIP AND BFQ.D_E_L_E_T_='' "
cQuery+= " INNER JOIN "+RetSqlName("BA3")+" BA3 ON BA3_FILIAL='"+xFilial("BA3")+"' AND BA3_CODINT=BM1_CODINT AND  BA3_CODEMP=BM1_CODEMP AND BA3_CONEMP=BM1_CONEMP AND BA3_VERCON=BM1_VERCON AND "
cQuery+= " BA3_SUBCON=BM1_SUBCON AND BA3_VERSUB=BM1_VERSUB AND BA3_MATRIC = BM1_MATRIC AND BA3.D_E_L_E_T_='' "
cQuery+= "WHERE BM1_FILIAL ='"+xFilial("BM1")+"' AND "
cQuery+= "BM1_CODINT ='"+MV_PAR01+"'  AND "
cQuery+= "BM1_CODEMP >='"+MV_PAR02+"' AND BM1_CODEMP <='"+MV_PAR03+"' AND "
cQuery+= "BM1_CONEMP >='"+MV_PAR04+"' AND BM1_CONEMP <='"+MV_PAR05+"' AND "
cQuery+= "BM1_SUBCON >='"+MV_PAR06+"' AND BM1_SUBCON <='"+MV_PAR07+"' AND "
cQuery+= "BM1_MATRIC >='"+MV_PAR08+"' AND BM1_MATRIC <='"+MV_PAR09+"' AND "
cQuery+= "BM1_ANO >='"+MV_PAR10+"' AND BM1_ANO <='"+MV_PAR11+"' AND "
cQuery+= "BM1_MES >='"+MV_PAR12+"' AND BM1_MES <='"+MV_PAR13+"' AND "
cQuery+= "BM1_AGFTFU||BM1_AGMTFU <> ' ' AND "
cQuery+= "BM1.D_E_L_E_T_=' ' "
cQuery+= "ORDER BY BM1_CODINT,BM1_CODEMP,BM1_CONEMP,BM1_VERCON, BM1_SUBCON, BM1_VERSUB,BM1_MATRIC,BM1_ANO,BM1_MES "

cQuery := ChangeQuery(cQuery)
dbUseArea(.T.,"TOPCONN",TCGENQRY(,,cQuery),"TRBBM1",.F.,.T.)

While TRBBM1->(!EOF())
	
	
	cChave:=TRBBM1->(BM1_ANO+BM1_MES+BM1_CODINT+BM1_CODEMP+BM1_CONEMP+BM1_VERCON+ BM1_SUBCON+ BM1_VERSUB+BM1_MATRIC)
	
	
	While TRBBM1->(!EOF()) .and. cChave==TRBBM1->(BM1_ANO+BM1_MES+BM1_CODINT+BM1_CODEMP+BM1_CONEMP+BM1_VERCON+ BM1_SUBCON+ BM1_VERSUB+BM1_MATRIC)
		
		cMatUsu:=TRBBM1->(BM1_CODINT+BM1_CODEMP+BM1_MATRIC+BM1_TIPREG+BM1_DIGITO)
		
		BI3->(DbSeek(xFilial('BI3')+TRBBM1->(BA3_CODINT+ BA3_CODPLA+ BA3_VERSAO))) //BI3_FILIAL + BI3_CODINT + BI3_CODIGO + BI3_VERSAO
		
		//se BI3_TPFORN nao preenchido, critica
		If empty(BI3->BI3_TPFORN)
			Aadd(aCriPrd, {BI3->BI3_CODIGO+" "+BI3->BI3_VERSAO,BI3->BI3_DESCRI,"Nao informado tipo de fornecedor no Produto Saude. Campo BI3_TPFORN! "} )
			TRBBM1->(Dbskip())
			Loop
		Endif
		
		aDadGPE:=MontaVer(cMatUsu,TRBBM1->BA3_CODPLA,TRBBM1->BA3_VERSAO,TRBBM1->BA3_CODINT,ctod("01"+"/"+TRBBM1->BM1_MES+"/"+TRBBM1->BM1_ANO),BI3->BI3_TPFORN)
		
		//Se os lan�amentos de faturamento n�o estiverem preenchidos, critica
		
		If Empty(TRBBM1->BFQ_TPLAN)
			Aadd(aCriLct, {TRBBM1->BM1_CODTIP,TRBBM1->BM1_DESTIP,"Lan�amento de faturamento n�i classificado.Campo BFQ_TPLAN! "} )
			TRBBM1->(Dbskip())
			Loop
			
			
		Endif
		
		If TRBBM1->BFQ_TPLAN <>'1'
			cVerba     := TRBBM1->BFQ_VERBA
		else
			cVerba:= aDadGPE[1]
		Endif
		
		If empty(cVerba)
			Aadd(aCriUsr, {transform(TRBBM1->(BM1_CODINT+BM1_CODEMP+BM1_MATRIC+BM1_TIPREG+BM1_DIGITO),"@R !!!!.!!!!.!!!!!!.!!-!")," "+TRBBM1->BM1_NOMUSR+" ","Verba nao encontrada!"} )
			TRBBM1->(Dbskip())
			Loop
		Endif
		                                           
		cTpPlan := aDadGPE[2]
		cCodFor := aDadGPE[3]
		cTpForn  := BI3->BI3_TPFORN
		cSeqDep  := aDadGPE[4]
		cPlanGPE := aDadGPE[5]
		//se n�o achar dados a GPE, critica
		If empty(cTpPlan)
			Aadd(aCriUsr, {transform(TRBBM1->(BM1_CODINT+BM1_CODEMP+BM1_MATRIC+BM1_TIPREG+BM1_DIGITO),"@R !!!!.!!!!.!!!!!!.!!-!")," "+TRBBM1->BM1_NOMUSR+" ","Nao encontrou Plano de Saude Ativo na Folha!"} )
			TRBBM1->(Dbskip())
			Loop
		Endif
		 
		BA1->(DbSetorder(2))
		If BA1->(DbSeek(Xfilial('BA1')+TRBBM1->(BM1_CODINT+BM1_CODEMP+BM1_MATRIC+BM1_TIPREG+BM1_DIGITO)))
			cOrigem:=iIf(BA1->BA1_TIPUSU == GetNewPar("MV_PLCDTIT","T"),"1","2")
		Endif 
		
		AaDd(aVetAux, {TRBBM1->BM1_CODINT,;  //01
		TRBBM1->BM1_CODEMP,;  //02
		TRBBM1->(BM1_MATRIC+BM1_TIPREG+BM1_DIGITO),;  //03
		TRBBM1->BM1_VALOR,;   //04
		TRBBM1->BM1_CODTIP,;  //05
		TRBBM1->BM1_AGFTFU,;  //06
		TRBBM1->BM1_AGMTFU,;  //07
		cVerba,;  //08
		TRBBM1->BM1_ANO,;     //09
		TRBBM1->BM1_MES,;     //10
		TRBBM1->BM1_PLNUCO,;  //11
		TRBBM1->BM1_TIPREG,;  //12
		cOrigem,; //13
		TRBBM1->BFQ_TPLAN,;  //14
		cTpPlan,; //15 	Tipo do Plano no modulo Gest�o de Pessoal (faixa salarial, valor fixo .....)
		cTpForn,;  // Tipo de fornecedor do plano (1-Ass. Medica; 2 =Ass. Odontologica
		cCodfor,;  //Codigo do fornecedor do plano
		cSeqDep,;  // codigo do dependente
		cPlanGPE})  //codigo do plano do GPE
		
		
		TRBBM1->(Dbskip())
	Enddo
	///gravar os arquivos
	
	
	For nInd := 1 to Len(aVetAux)
		
		j := aScan(aAcuFun, {|x| x[3]+x[4] == aVetAux[nInd][6]+aVetAux[nInd][7] .And. x[5] == aVetAux[nInd][8] .And. x[13] == aVetAux[nInd][13] .And. x[14] == aVetAux[nInd][14] .And. x[18] == aVetAux[nInd][18]})
		
		If j  == 0
			//Procuro a Data de pagamento no arquivo SRD     
			If SRD->(Dbseek(aVetAux[nInd][6]+aVetAux[nInd][7]+(aVetAux[nInd][9]+aVetAux[nInd][10])))
			 	dtpagto := SRD->RD_DATPGT	
			Else
				dtpagto :=LastDay(Ctod("01/" + aVetAux[nInd][10] + "/" + aVetAux[nInd][9]))			
			
			Endif				
			AaDd(aAcuFun,{	aVetAux[nInd][4],;  //[1] VALOR
			aVetAux[nInd][5],;  //[2] TP LANC
			aVetAux[nInd][6],;  //[3] FILIAL FUN
			aVetAux[nInd][7],;  //[4] MATRIC FUN
			aVetAux[nInd][8],;  //[5] VERBA FOLHA
			aVetAux[nInd][1],;  //[6] CODINT
			aVetAux[nInd][2],;  //[7] CODEMP
			aVetAux[nInd][3],;  //[8] MATRIC
			aVetAux[nInd][9],;  //[9] ANO
			aVetAux[nInd][10],; //[10]MES
			aVetAux[nInd][12],; //[11]TIPREG
			aVetAux[nInd][11],; //[12]NUM COB
			aVetAux[nInd][13],; //[13]Tipo de usu�rio
			aVetAux[nInd][14],;//[14]Tipo de lan�amentoo GPE
			aVetAux[nInd][15],; //[15] tipo do plano                   })
			aVetAux[nInd][16],; //[16] tipo do fornecedor              })
			aVetAux[nInd][17],; //[17]cod fornecedor                  })
			aVetAux[nInd][18],; //[18] cod dependente                   })
			aVetAux[nInd][19],; //[19] cod do plano                   })
			dtPagto})          //[20] 
		Else
			aAcuFun[j][1] += aVetAux[nInd][4]
		Endif
	Next
	
	For nInd := 1 to Len(aAcuFun)
		
		//RHR_FILIAL+   RHR_MAT+            RHR_COMPPG+RHR_ORIGEM+       RHR_CODIGO+         RHR_TPLAN
		//+RHR_TPFORN+RHR_CODFOR+RHR_TPPLAN+RHR_PLANO+RHR_PD
		If !(RHS->(DbSeek(aAcuFun[nInd][3]+aAcuFun[nInd][4]+(aAcuFun[nInd][9]+aAcuFun[nInd][10])+aAcuFun[nInd][13]+aAcuFun[nInd][18]+aAcuFun[nInd][14]+;
			aAcuFun[nInd][16]+aAcuFun[nInd][17]+aAcuFun[nInd][15] +aAcuFun[nInd][19]+aAcuFun[nInd][5])))
			RHS->(RecLock("RHS",.T.))
			
			RHS->RHS_FILIAL:= aAcuFun[nInd][3]
			RHS->RHS_MAT   := aAcuFun[nInd][4]
			RHS->RHS_COMPPG:= aAcuFun[nInd][9]+aAcuFun[nInd][10]
			RHS->RHS_ORIGEM:= aAcuFun[nInd][13]
			RHS->RHS_CODIGO:= aAcuFun[nInd][18]
			RHS->RHS_TPLAN:=  aAcuFun[nInd][14]
			RHS->RHS_TPFORN:= aAcuFun[nInd][16]
			RHS->RHS_CODFOR:= aAcuFun[nInd][17]
			RHS->RHS_TPPLAN:= aAcuFun[nInd][15]
			RHS->RHS_PLANO := aAcuFun[nInd][19]
			RHS->RHS_PD    := aAcuFun[nInd][5]
			RHS->RHS_VLRFUN:= aAcuFun[nInd][1]
			RHS->RHS_DATA  := LastDay(Ctod("01/" + aAcuFun[nInd][10] + "/" + aAcuFun[nInd][9]))
			RHS->RHS_DATPGT:= aAcuFun[nInd][20]
			RHS->(MsUnlock())
			
			
			//devo gerar RHO qdo se tratar de co-participa�ao e reembolso
			If aAcuFun[nInd][14] <> "1"
				RHP->(RecLock("RHP",.T.))
				
				RHP->RHP_FILIAL:= aAcuFun[nInd][3]
				RHP->RHP_MAT   := aAcuFun[nInd][4]
				RHP->RHP_COMPPG:= aAcuFun[nInd][9]+aAcuFun[nInd][10]
				RHP->RHP_ORIGEM:= aAcuFun[nInd][13]
				RHP->RHP_CODIGO:= aAcuFun[nInd][18]
				RHP->RHP_TPLAN:=  Iif(aAcuFun[nInd][14]=='2','1','2')
				RHP->RHP_TPFORN:= aAcuFun[nInd][16]
				RHP->RHP_CODFOR:= aAcuFun[nInd][17]
				RHP->RHP_PD    := aAcuFun[nInd][5]
				RHP->RHP_VLRFUN:= aAcuFun[nInd][1]
				RHP->RHP_DTOCOR:= LastDay(Ctod("01/" + aAcuFun[nInd][10] + "/" + aAcuFun[nInd][9]))
				RHP->RHP_DATPGT:= aAcuFun[nInd][20]
				RHP->(MsUnlock())
				
			Endif
		Endif
	Next
	
	
	aVetAux:={}
	aAcuFun:={}
	
Enddo
TRBBM1->(Dbclosearea())

//���������������������������������������������������������������������Ŀ
//� Exibe tela com inconsistencias encontradas durante o processo...    �
//�����������������������������������������������������������������������
If Len(aCriUsr) > 0
	PlsCriGen(aCriUsr, aCabUsr, "Criticas...")
Endif

If Len(aCriPrd) > 0
	PlsCriGen(aCriPrd, aCabPrd, "Criticas...")
Endif

If Len(aCriLct) > 0
	PlsCriGen(aCriLct, aCabLct, "Criticas...")
Endif

aCriUsr:={}
aCriPrd:={}
aCriLct:={}

Return