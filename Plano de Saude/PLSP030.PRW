#INCLUDE "PROTHEUS.CH"
#INCLUDE "MSgraphi.ch"
#INCLUDE "PLSP030.ch"

/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Fun��o    �PLSP010   � Autor � Henry Fila            � Data � 27/04/2007 ���
���������������������������������������������������������������������������Ĵ��
���Descri��o �Monta array para Painel de Gestao Tipo 2 Padrao 1: Quantidade ���
���          �de usuarios incluidos e excluidos no periodo                  ���
���������������������������������������������������������������������������Ĵ��
���Sintaxe   �PLSP010()                                                     ���
���������������������������������������������������������������������������Ĵ��
���Retorno   � Array = {{cCombo1,{cText1,cValor,nColorValor,cClick},..},..} ���
���          � cCombo1     = Detalhes                                       ���
���          � cText1      = Texto da Coluna                         		���
���          � cValor      = Valor a ser exibido (string)                   ���
���          � nColorValor = Cor do Valor no formato RGB (opcional)         ���
���          � cClick      = Funcao executada no click do valor (opcional)  ���
���������������������������������������������������������������������������Ĵ��
���Uso       � SIGAMDI                                                      ���
���������������������������������������������������������������������������Ĵ��
��� Atualizacoes sofridas desde a Construcao Inicial.                       ���
���������������������������������������������������������������������������Ĵ��
��� Programador  � Data   � BOPS �  Motivo da Alteracao                     ���
���������������������������������������������������������������������������Ĵ��
���              �        �      �                                          ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
/*/


Function PLSP030()

Local aDados	  := {}
Local aArea       := GetArea()
Local aRet        := {} 
Local cMes        := StrZero(Month(dDataBase),2)
Local cAno        := Substr(DTOC(dDataBase),7,2)
Local dDataIni    := CTOD("01/"+cMes+"/"+cAno)
Local dDataFim    := CTOD(StrZero(F_ULTDIA(dDataBase),2)+"/"+cMes+"/"+cAno)

//������������������������������������������������������������������������Ŀ
//�                                                                        �
//�                              D I A R I O                               �
//�                                                                        �
//��������������������������������������������������������������������������
//������������������������������������������������������������������������Ŀ
//�Numero de pedidos no dia                                                �
//��������������������������������������������������������������������������
R087Imp(dDataIni,dDataFim,.T.,aDados)
//������������������������������������������������������������������������Ŀ
//�Preenche array do Painel de Gestao                                      �
//��������������������������������������������������������������������������

aRet:= { Nil, {STR0001, STR0002,"%",STR0003,"%", STR0004,"%", STR0005, "%"  } ,;
	 { { aDados[1,1],Transform(aDados[1,2],"@E 999,999"),;
	 					Transform(aDados[1,3],"@E 999,999"),;
	 					Transform(aDados[1,4],"@E 999,999"),;
	 					Transform(aDados[1,5],"@E 999,999"),;
						Transform(aDados[1,6],"@E 999,999"),;
						Transform(aDados[1,7],"@E 999,999"),;
						Transform(aDados[1,8],"@E 999,999"),;
						Transform(aDados[1,9],"@E 999,999")	},;
  		 { aDados[2,1],Transform(aDados[2,2],"@E 999,999"),;
  		 				Transform(aDados[2,3],"@E 999,999"),;
  		 				Transform(aDados[2,4],"@E 999,999"),;
  		 				Transform(aDados[2,5],"@E 999,999"),;
						Transform(aDados[2,6],"@E 999,999"),;
						Transform(aDados[2,7],"@E 999,999")	,;
						Transform(aDados[2,8],"@E 999,999"),;
						Transform(aDados[2,9],"@E 999,999")	},;
  		 { aDados[3,1],Transform(aDados[3,2],"@E 999,999"),;
  		 				Transform(aDados[3,3],"@E 999,999"),;
  		 				Transform(aDados[3,4],"@E 999,999"),;
  		 				Transform(aDados[3,5],"@E 999,999"),;
						Transform(aDados[3,6],"@E 999,999"),;
						Transform(aDados[3,7],"@E 999,999")	,;
						Transform(aDados[3,8],"@E 999,999"),;
						Transform(aDados[3,9],"@E 999,999")	},;
  		 { aDados[4,1],Transform(aDados[4,2],"@E 999,999"),;
  		 				Transform(aDados[4,3],"@E 999,999"),;
  		 				Transform(aDados[4,4],"@E 999,999"),;
  		 				Transform(aDados[4,5],"@E 999,999"),;
						Transform(aDados[4,6],"@E 999,999"),;
						Transform(aDados[4,7],"@E 999,999"),;
						Transform(aDados[4,8],"@E 999,999"),;
						Transform(aDados[4,9],"@E 999,999")	},;
  		 { aDados[5,1],Transform(aDados[5,2],"@E 999,999"),;
  		 				Transform(aDados[5,3],"@E 999,999"),;
  		 				Transform(aDados[5,4],"@E 999,999"),;
  		 				Transform(aDados[5,5],"@E 999,999"),;
						Transform(aDados[5,6],"@E 999,999"),;
						Transform(aDados[5,7],"@E 999,999")	,;
						Transform(aDados[5,8],"@E 999,999"),;
						Transform(aDados[5,9],"@E 999,999")	},;
  		 { aDados[6,1],Transform(aDados[6,2],"@E 999,999"),;
  		 				Transform(aDados[6,3],"@E 999,999"),;
  		 				Transform(aDados[6,4],"@E 999,999"),;
  		 				Transform(aDados[6,5],"@E 999,999"),;
						Transform(aDados[6,6],"@E 999,999"),;
						Transform(aDados[6,7],"@E 999,999"),;
						Transform(aDados[6,8],"@E 999,999")	,;
						Transform(aDados[6,9],"@E 999,999")	} },;
	  	 {"LEFT","LEFT","LEFT","LEFT","LEFT","LEFT","LEFT","LEFT","LEFT"} } 

//������������������������������������������������������������������������Ŀ
//�Restaura areas                                                          �
//��������������������������������������������������������������������������
RestArea(aArea)

                                    
Return aRet


                   