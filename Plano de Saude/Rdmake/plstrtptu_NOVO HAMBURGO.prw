
#INCLUDE "PROTHEUS.CH"
#INCLUDE "PLSMGER.CH"
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    �PLSTRTPTU � Autor � Eduardo Motta         � Data � 12.02.04 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � RDMAKE para tratamento do PTU online (RECEBIMENTO)         ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �PLSTRTPTU()                                                 ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

User Function PlsTrtPtu()
Local aCab := {}
Local aIte := {}
Local aRet := {}
Local cCodInt  := ""
Local cCodUsu  := ""
Local cCodMed  := ""
Local cCodMed2 := ""
Local cData    := ""
Local cHora    := ""
Local dData    := CtoD("")
Local cCidPri  := ""
Local cCodPro  := ""
Local cUniSol  := ""
Local cNomeBene := ""
Local nI := 0
Local nJ := 0
Local nItem := 0
Local nQtd := 0
Local nH := 0
Local cNumImp := ""
Local cMsgErro
Private cMsgErro01 := ""
Private cMsgErro02 := ""
Private cMsgErro03 := ""
Private cMsgErro04 := ""
Private cMsgErro05 := ""
Private cVarMacro := ""


cDelimit := ""

If Upper(PlsPtuGet("TP_TRANS",aDados)) == "80110010"  // CONSULTA
   PlsPtuLog("NR_TRANS  => "+PlsPtuGet("NR_TRANS_R",aDados))
   PlsPtuLog("USUARIO   => "+PlsPtuGet("ID_BENEF",aDados))
   PlsPtuLog("CD_PREREQ => "+PlsPtuGet("CD_PRE_REQ",aDados))
   PlsPtuLog("CD_UNI => "+PlsPtuGet("CD_UNI",aDados))
   PlsPtuLog("CD_UNI_PRE_REQ => "+PlsPtuGet("UNI_PRE_REQ",aDados))
   PlsPtuLog("UNI_PRE => "+PlsPtuGet("CD_UNI_PRE",aDados))
   PlsPtuLog("CID     => "+PlsPtuGet("CD_CID",aDados))
   PlsPtuLog("CD_SERVICO => "+PlsPtuGet("CD_SERVICO",aDados))
   cCodInt  := PlsIntPad()
   cCodUsu  := cCodInt+PlsPtuGet("ID_BENEF",aDados)
   cCodMed  := PlsPtuGet("CD_PREST",aDados)   // prestador
   cCodMed2 := PlsPtuGet("CD_PRE_REQ",aDados)   // requisitante
   cData    := SubStr(PlsPtuGet("DT_TRANS",aDados),1,10)
   cHora    := SubStr(PlsPtuGet("DT_TRANS",aDados),11,5)
   dData    := CtoD(SubStr(cData,9,2)+"/"+SubStr(cData,6,2)+"/"+SubStr(cData,1,4))
   cCidPri  := PlsPtuGet("CD_CID",aDados)
   cCodPro  := "01"+PlsPtuGet("CD_SERVICO",aDados)
   cUniSol  := PlsPtuGet("CD_UNI",aDados)
   nQtd     := Val(PlsPtuGet("QT_SERVICO",aDados))
   PlsPtuLog("cod.int ["+cCodInt+"]")
   PlsPtuLog("cod.usr ["+cCodUsu+"]")
   aadd(aCab,{"OPEMOV",cCodInt})
   aadd(aCab,{"USUARIO",cCodUsu})
   aadd(aCab,{"DATPRO",dData })
   aadd(aCab,{"HORAPRO",SubStr(StrTran(cHora,":",""),1,4)})
   aadd(aCab,{"CIDPRI",cCidPri })
//   aadd(aCab,{"CODESP",BAQ->BAQ_CODESP })
   BAW->(DbSetOrder(3))
   If Val(cCodMed2) # 0
      BAW->(DbSeek(xFilial("BAW")+cCodInt+cCodMed2))
      aadd(aCab,{"OPESOL",cUniSol})
      BAU->(DbSetOrder(1))
      BAU->(DbSeek(xFilial("BAU")+BAW->BAW_CODIGO))
      aadd(aCab,{"CDPFSO",BAU->BAU_CODBB0})
   EndIf   
   BAW->(DbSetOrder(3))
   BAW->(DbSeek(xFilial("BAW")+cCodInt+cCodMed))
   BAU->(DbSetOrder(1))
   BAU->(DbSeek(xFilial("BAU")+BAW->BAW_CODIGO))
//   aadd(aCab,{"CODRDA",BAU->BAU_CODIGO})
   aIte := {}
   aadd(aIte,{})
   aadd(aIte[1],{"SEQMOV","001" })
   aadd(aIte[1],{"CODPAD",SubStr(cCodPro,1,2)})
   aadd(aIte[1],{"CODPRO",SubStr(cCodPro,3)})
   aadd(aIte[1],{"QTD",nQtd })

   nH       := PLSAbreSem("PLSTRTPOS.SMF")
      
   cSQL     := "SELECT MAX(BD6_NUMIMP) MAIOR FROM "+RetSQLName("BD6")+" WHERE BD6_FILIAL = '"+xFilial("BD6")+"' AND D_E_L_E_T_ = '' AND SUBSTRING(BD6_NUMIMP,1,3)='000' "
   PLSQUERY(cSQL,"PLSTEMP")                
   cNumImp := StrZero(Val(PLSTEMP->MAIOR)+1,16)
   PLSTEMP->(DbCloseArea())
   PlsPtuLog("num.imp. ["+cNumImp+"]")
      
   aadd(aCab,{"NUMIMP",cNumImp})   

   aRet := PLSXAUTP(aCab,aIte)
   PLSFechaSem(nH)
   BA1->(DbSetOrder(2))
   If !BA1->(DbSeek(xFilial("BA1")+cCodUsu))
      BA1->(DbSetOrder(5))
      BA1->(DbSeek(xFilial("BA1")+cCodUsu))
   EndIf
   cNomeBene := PadR(BA1->BA1_NOMUSR,25)
   If Empty(cNomeBene)
      cNomeBene := PadR("NAO ENCONTRADO",25)
   EndIf
   PlsPtuPut("NM_BENEF",cNomeBene  ,aDados)
   PlsPtuPut("TP_PESSOA",If(BA3->BA3_TIPOUS=="1","2","1"),aDados)
   If aRet[1]   // autorizou
      PlsPtuPut("NR_AUTORIZ",SubStr(cNumImp,8),aDados)
      BA3->(DbSeek(xFilial()+BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC)))
      PlsPtuPut("ID_AUTORIZ","1",aDados)
      PlsPtuPut("QT_AUTORIZ",StrZero(nQtd,4),aDados)
      PlsPtuPut("DT_VAL_AUT",DtoC(dData),aDados)
      PlsPtuPut("ERRO01","0000",aDados)
      PlsPtuPut("ERRO02","0000",aDados)
      PlsPtuPut("ERRO03","0000",aDados)
      PlsPtuPut("ERRO04","0000",aDados)
      PlsPtuPut("ERRO05","0000",aDados)
      PlsPtuLog("*** AUTORIZADO ***")      
   Else  // nao autorizou
      cMsgErro01 := ""
      cMsgErro02 := ""
      cMsgErro03 := ""
      cMsgErro04 := ""
      cMsgErro05 := ""
      PlsPtuPut("NR_AUTORIZ",StrZero(0,9),aDados)
      PlsPtuPut("ID_AUTORIZ","2",aDados)
      PlsPtuPut("QT_AUTORIZ",StrZero(0,4),aDados)
      PlsPtuLog("*** NAO AUTORIZADO ***")      
      For nI := 1 to Len(aRet[4])
         If nI > 5
            Exit
         EndIf
         PlsPtuLog("ERRO ["+aRet[4,nI,2]+"]")
         PlsPtuLog("ERRO PTU ["+MsgErro(aRet[4,nI,2])+"]")
         cVarMacro := "cMsgErro"+StrZero(nI,2)
         &cVarMacro := aRet[4,nI,2]
         PlsPtuLog(AllTrim(aRet[4,nI,3]))
         PlsPtuLog(AllTrim(aRet[4,nI,4]))
      Next
      PlsPtuLog("retorno ["+cMsgErro01+"]")
      PlsPtuLog("retorno ["+MsgErro(cMsgErro01)+"]")
      PlsPtuPut("ERRO01",MsgErro(cMsgErro01),aDados)
      PlsPtuPut("ERRO02",MsgErro(cMsgErro02),aDados)
      PlsPtuPut("ERRO03",MsgErro(cMsgErro03),aDados)
      PlsPtuPut("ERRO04",MsgErro(cMsgErro04),aDados)
      PlsPtuPut("ERRO05",MsgErro(cMsgErro05),aDados)
      PlsPtuLog("*********************")      
   EndIf

ElseIf Upper(PlsPtuGet("TP_TRANS",aDados)) == "80110020"  // EXAMES
   PlsPtuLog("NR_TRANS  => "+PlsPtuGet("NR_TRANS_R",aDados))
   PlsPtuLog("USUARIO   => "+PlsPtuGet("ID_BENEF",aDados))
   PlsPtuLog("CD_PREREQ => "+PlsPtuGet("CD_PRE_REQ",aDados))
   PlsPtuLog("CD_UNI => "+PlsPtuGet("CD_UNI",aDados))
   PlsPtuLog("CD_UNI_PRE_REQ => "+PlsPtuGet("UNI_PRE_REQ",aDados))
   PlsPtuLog("UNI_PRE => "+PlsPtuGet("CD_UNI_PRE",aDados))
   PlsPtuLog("CID     => "+PlsPtuGet("CD_CID",aDados))
   cCodInt  := PlsIntPad()
   cCodUsu  := cCodInt+PlsPtuGet("ID_BENEF",aDados)
   cCodMed  := PlsPtuGet("CD_PREST",aDados)   // prestador
   cCodMed2 := PlsPtuGet("CD_PRE_REQ",aDados)   // requisitante
   cData    := SubStr(PlsPtuGet("DT_TRANS",aDados),1,10)
   cHora    := SubStr(PlsPtuGet("DT_TRANS",aDados),11,5)
   dData    := CtoD(SubStr(cData,9,2)+"/"+SubStr(cData,6,2)+"/"+SubStr(cData,1,4))
   cCidPri  := PlsPtuGet("CD_CID",aDados)
   cUniSol  := PlsPtuGet("CD_UNI",aDados)
   PlsPtuLog("cod.int ["+cCodInt+"]")
   PlsPtuLog("cod.usr ["+cCodUsu+"]")

   aadd(aCab,{"OPEMOV",cCodInt})
   aadd(aCab,{"USUARIO",cCodUsu})
   aadd(aCab,{"DATPRO",dData })
   aadd(aCab,{"HORAPRO",SubStr(StrTran(cHora,":",""),1,4)})
   aadd(aCab,{"CIDPRI",cCidPri })

   aadd(aCab,{"CODRDA","000181"})
   aadd(aCab,{"OPESOL","0024"})  // CODIGO DA UNIMED BOTUCATU
   aadd(aCab,{"CDPFSO","000181"})
   aadd(aCab,{"CODESP","0024061"})

   BAW->(DbSetOrder(3))
   If Val(cCodMed2) # 0
      BAW->(DbSeek(xFilial("BAW")+cCodInt+cCodMed2))
      aadd(aCab,{"OPESOL",cUniSol})
      BAU->(DbSetOrder(1))
      BAU->(DbSeek(xFilial("BAU")+BAW->BAW_CODIGO))
      aadd(aCab,{"CDPFSO",BAU->BAU_CODBB0})
   EndIf   
   BAW->(DbSetOrder(3))
   BAW->(DbSeek(xFilial("BAW")+cCodInt+cCodMed))
   BAU->(DbSetOrder(1))
   BAU->(DbSeek(xFilial("BAU")+BAW->BAW_CODIGO))
   aIte := {}
   For nI := 1 to Len(aItens)
      cCodPro  := "01"+PlsPtuGet("CD_SERVICO",aItens[nI])
      nQtd     := Val(PlsPtuGet("QT_SERVICO",aItens[nI]))
      aadd(aIte,{})
      aadd(aIte[nI],{"SEQMOV",StrZero(nI,3) })
      aadd(aIte[nI],{"CODPAD",SubStr(cCodPro,1,2)})
      aadd(aIte[nI],{"CODPRO",SubStr(cCodPro,3)})
      aadd(aIte[nI],{"QTD",nQtd })
   Next   

   nH       := PLSAbreSem("PLSTRTPOS.SMF")
      
   cSQL     := "SELECT MAX(BD6_NUMIMP) MAIOR FROM "+RetSQLName("BD6")+" WHERE BD6_FILIAL = '"+xFilial("BD6")+"' AND D_E_L_E_T_ = '' AND SUBSTRING(BD6_NUMIMP,1,3)='000' "
   PLSQUERY(cSQL,"PLSTEMP")                
   cNumImp := StrZero(Val(PLSTEMP->MAIOR)+1,16)
   PLSTEMP->(DbCloseArea())
   PlsPtuLog("num.imp. ["+cNumImp+"]")
      
   aadd(aCab,{"NUMIMP",cNumImp})   
   aRet := PLSXAUTP(aCab,aIte)
   PLSFechaSem(nH)
   BA1->(DbSetOrder(2))
   If !BA1->(DbSeek(xFilial("BA1")+cCodUsu))
      BA1->(DbSetOrder(5))
      BA1->(DbSeek(xFilial("BA1")+cCodUsu))
   EndIf   
   PlsPtuPut("NM_BENEF",PadR(BA1->BA1_NOMUSR,25)  ,aDados)
   PlsPtuPut("TP_PESSOA",If(BA3->BA3_TIPOUS=="1","2","1"),aDados)
   If aRet[1]   // autorizou
      BA3->(DbSeek(xFilial()+BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC)))
      PlsPtuPut("NR_AUTORIZ",SubStr(cNumImp,8),aDados)
      PlsPtuPut("ID_AUTORIZ","1",aDados)
      PlsPtuPut("QT_AUTORIZ",StrZero(nQtd,4),aDados)
      PlsPtuPut("DT_VAL_AUT",DtoC(dData),aDados)
      PlsPtuPut("ERRO01","0000",aDados)
      PlsPtuPut("ERRO02","0000",aDados)
      PlsPtuPut("ERRO03","0000",aDados)
      PlsPtuPut("ERRO04","0000",aDados)
      PlsPtuPut("ERRO05","0000",aDados)
      For nI := 1 to Len(aItens)
         PlsPtuPut("ERRO01","0000",aItens[nI])
         PlsPtuPut("ERRO02","0000",aItens[nI])
         PlsPtuPut("ERRO03","0000",aItens[nI])
         PlsPtuPut("ERRO04","0000",aItens[nI])
         PlsPtuPut("ERRO05","0000",aItens[nI])
      Next
      PlsPtuLog("*** AUTORIZADO ***")      
   Else  // nao autorizou
      cMsgErro01 := ""
      cMsgErro02 := ""
      cMsgErro03 := ""
      cMsgErro04 := ""
      cMsgErro05 := ""
      PlsPtuPut("NR_AUTORIZ",StrZero(0,9),aDados)
      PlsPtuPut("ID_AUTORIZ","2",aDados)
      PlsPtuPut("QT_AUTORIZ",StrZero(0,4),aDados)
      PlsPtuLog("*** NAO AUTORIZADO ***")
      For nI := 1 to Len(aRet[4])
         nItem := Val(aRet[4,nI,1])
         cMsgErro := MsgErro(aRet[4,nI,2])
         If Empty(PlsPtuGet("ERRO01",aItens[nItem]))
            PlsPtuPut("ERRO01",cMsgErro,aItens[nItem])
         ElseIf Empty(PlsPtuGet("ERRO02",aItens[nItem]))
            PlsPtuPut("ERRO02",cMsgErro,aItens[nItem])
         ElseIf Empty(PlsPtuGet("ERRO03",aItens[nItem]))
            PlsPtuPut("ERRO03",cMsgErro,aItens[nItem])
         ElseIf Empty(PlsPtuGet("ERRO04",aItens[nItem]))
            PlsPtuPut("ERRO04",cMsgErro,aItens[nItem])
         ElseIf Empty(PlsPtuGet("ERRO05",aItens[nItem]))
            PlsPtuPut("ERRO05",cMsgErro,aItens[nItem])
         EndIf   
         If nI > 5
            Loop
         EndIf   
         cVarMacro := "cMsgErro"+StrZero(nI,2)
         &cVarMacro := aRet[4,nI,2]
         PlsPtuLog(AllTrim(aRet[4,nI,3]))
         PlsPtuLog(AllTrim(aRet[4,nI,4]))
      Next
      For nI := 1 to Len(aItens)
         PlsPtuPut("ID_AUTORIZ","2",aItens[nI])
         PlsPtuPut("QT_AUTORIZ",StrZero(0,4),aItens[nI])
         If Empty(PlsPtuGet("ERRO01",aItens[nI]))
            PlsPtuPut("ERRO01","0000",aItens[nI])
         EndIf   
         If Empty(PlsPtuGet("ERRO02",aItens[nI]))
            PlsPtuPut("ERRO02","0000",aItens[nI])
         EndIf   
         If Empty(PlsPtuGet("ERRO03",aItens[nI]))
            PlsPtuPut("ERRO03","0000",aItens[nI])
         EndIf   
         If Empty(PlsPtuGet("ERRO04",aItens[nI]))
            PlsPtuPut("ERRO04","0000",aItens[nI])
         EndIf   
         If Empty(PlsPtuGet("ERRO05",aItens[nI]))
            PlsPtuPut("ERRO05","0000",aItens[nI])
         EndIf   
      Next
      PlsPtuLog("retorno ["+MsgErro(cMsgErro01)+"]")
      PlsPtuPut("ERRO01",MsgErro(cMsgErro01),aDados)
      PlsPtuPut("ERRO02",MsgErro(cMsgErro02),aDados)
      PlsPtuPut("ERRO03",MsgErro(cMsgErro03),aDados)
      PlsPtuPut("ERRO04",MsgErro(cMsgErro04),aDados)
      PlsPtuPut("ERRO05",MsgErro(cMsgErro05),aDados)
      PlsPtuLog("*********************")      
   EndIf
EndIf

Return

User Function PlsEndPtu()
Local cDriveProc := ParamIxb[1]
Local cPathOut := ParamIxb[2]
Local cPathIn  := ParamIxb[3]
Local aFilesP
Local nTotFiles
Local nI

aFilesP := Directory(cDriveProc+cPathOut+'*.*')
 
makedir(cPathOut+"\OUT")
For nI := 1 to len(aFilesP)
   fErase(cPathIn+aFilesP[nI][1])
   fErase(cPathOut+"out\"+aFilesP[nI][1])
   PlsPtuLog("copy file de => "+cPathOut+aFilesP[nI][1])
   PlsPtuLog("copy file para => "+cPathOut+"out\"+aFilesP[nI][1])
   __CopyFile(cPathOut+aFilesP[nI][1],cPathOut+"out\"+aFilesP[nI][1])
   If fREname(cDriveProc+cPathOut+aFilesP[nI][1] , cPathIn+aFilesP[nI][1] )#-1
//   If fREname(cDriveProc+cPathOut+aFilesP[nI][1] , cDriveProc+cPathOut+"OUT\"+aFilesP[nI][1] )#-1
      PlsPtuLog("renomeado para "+cPathIn+aFilesP[nI][1])
   EndIf   
Next

Return

User Function PlsArqPtu()
Local cPathIn := ParamIxb[1]
Local aFiles
aFiles := Directory(cPathIn+'*.*')
Return aFiles

Static Function MsgErro(cCod)
Local aErro := {}
Local nPos
Local cRet

If Empty(cCod)
   Return "0000"
EndIf

aadd(aErro,{"501","Usuario nao possui cobertura para este procedimento.","1033"})
aadd(aErro,{"001","Idade do usuario incompativel com a idade limite para o procedimento.","1037"})
aadd(aErro,{"002","Procedimento em carencia para este usuario.","1041"})
If BA1->BA1_SEXO=="1"  // SE FOR SEXO MASCULINO
   aadd(aErro,{"003","Sexo invalido para este procedimento.","1035"})
Else
   aadd(aErro,{"003","Sexo invalido para este procedimento.","1034"})
EndIf   
aadd(aErro,{"502","Unidade da Rede de atendimento bloqueada.","1087"})
aadd(aErro,{"503","Local de atendimento X Rede de atendimento do produto: Invalido.","0000"})
aadd(aErro,{"504","Local de atendimento invalido para o produto do usuario.","0000"})
aadd(aErro,{"505","Familia bloqueada.","1021"})
aadd(aErro,{"506","Usuario bloqueado.","1027"})
aadd(aErro,{"507","Operadora invalida para este usuario.","1002"})
aadd(aErro,{"508","Matricula do usuario: Invalida.","1007"})
aadd(aErro,{"509","Operadora da Rede de atendimento: Invalida.","0000"})
aadd(aErro,{"510","Matricula da Rede de atendimento: Invalida.","0000"})
aadd(aErro,{"511","Rede de atendimento nao permitida para a operadora informada.","0000"})
aadd(aErro,{"512","Rede de atendimento sem local de atendimento cadastrado.","0000"})
aadd(aErro,{"513","Rede de atendimento sem especialidade cadastrada","1092"})
aadd(aErro,{"004","Critica Financeira.","1135"})
aadd(aErro,{"005","Procedimento em carencia para este usuario (PREEXISTENCIA).","1041"})
aadd(aErro,{"514","Existe uma internacao para este usuario cuja data de alta encontra-se sem preenchimento.","0000"})
aadd(aErro,{"006","Unidade da Rede de atendimento nao autorizada a executar o procedimento.","0000"})
aadd(aErro,{"007","Procedimento bloqueado na especialidade de Rede de atendimento.","0000"})
aadd(aErro,{"008","Idade do usuario incompativel com a idade limite para a especialidade.","1023"})
If BA1->BA1_SEXO=="1"  // SE FOR SEXO MASCULINO
   aadd(aErro,{"009","Sexo invalido para a especialidade.","1035"})
Else
   aadd(aErro,{"009","Sexo invalido para a especialidade.","1034"})
EndIf   
aadd(aErro,{"010","A data do evento e anterior a data de inclusao do usuario.","1137"})
aadd(aErro,{"515","Nao existe calendario de pagamento para a data do evento informada.","0000"})
aadd(aErro,{"516","O Valor do evento esta igual a zero.","1005"})
aadd(aErro,{"517","Nao foi encontrada nenhuma ocorrencia para o Codigo da Tabela de Honorarios a ser utilizada.","0000"})
aadd(aErro,{"518","Nao existe composicao para esse procedimento.","0000"})
aadd(aErro,{"519","A expressao para o Calculo da US em Procedimentos Autorizados na Especialidade da RDA, nao foi informado corretamente.","0000"})
aadd(aErro,{"520","A expressao para o Calculo da US em Especialidades na RDA, nao foi informado corretamente.","0000"})
aadd(aErro,{"521","A expressao para o Calculo da US no Local de Atendimento na RDA, nao foi informado corretamente.","0000"})
aadd(aErro,{"522","A expressao para o Calculo da US em Operadoras na RDA, nao foi informado corretamente.","0000"})
aadd(aErro,{"523","A expressao para o Calculo da US em Pacote, nao foi informado corretamente.","0000"})
aadd(aErro,{"524","Nao foi informado nenhum valor para a US.","0000"})
aadd(aErro,{"525","Nao foi informado nenhum valor para o Filme.","0000"})
aadd(aErro,{"526","Nao foi informado nenhum valor para o Porte Anestesico.","0000"})
aadd(aErro,{"527","Nao foi informado nenhum valor para o auxiliar.","0000"})
aadd(aErro,{"530","Digite verificador da matricula invalido","1007"})
aadd(aErro,{"012","livre p/ uso","0000"})
aadd(aErro,{"013","livre p/ uso","0000"})
aadd(aErro,{"014","livre p/ uso","0000"})
aadd(aErro,{"015","livre p/ uso","0000"})
aadd(aErro,{"016","livre p/ uso","0000"})
aadd(aErro,{"528","O Procedimento foi negado para ser executado por este prestador no local da atendimento e especialidade.","0000"})
aadd(aErro,{"529","A parametrizacao dos niveis de cobranca esta invalida.","0000"})
aadd(aErro,{"017","Limite de Quantidade ultrapassada.","1096"})
aadd(aErro,{"018","Limite de Periodicidade ultrapassada.","1094"})
aadd(aErro,{"019","Limite de Grupo de Quantidade ultrapassada.","1094"})
aadd(aErro,{"020","O valor contratato e diferente do valor informado.","0000"})
aadd(aErro,{"021","Para este procedimento necessita Guia da Operadora.","1095"})
aadd(aErro,{"021","Para este procedimento necessita Auditoria.","1095"})
aadd(aErro,{"022","Para este procedimento necessita Guia da Empresa.","1095"})
aadd(aErro,{"023","Para este procedimento necessita Guia da Operadora e Empresa.","1095"})
aadd(aErro,{"024","Para este procedimento necessita Avaliacao Contratual.","1095"})
nPos:=aScan(aErro,{|x|x[1]==cCod})
If nPos == 0
   cRet := "0000"
Else
   cRet:= aErro[nPos,3]
EndIf

Return cRet
