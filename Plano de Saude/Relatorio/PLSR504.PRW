#INCLUDE "plsr504.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PLSR504    �Autor  �Paulo Carnelossi   � Data �  02/09/03   ���
�������������������������������������������������������������������������͹��
���Desc.     �Imprime relatorio de Relatorio de Tabela AMB 92 e outras    ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function PLSR504()    
//��������������������������������������������������������������Ŀ
//� Define Variaveis                                             �
//����������������������������������������������������������������
Local wnrel
Local cDesc1 := STR0001 //"Este programa tem como objetivo imprimir a Tabela"
Local cDesc2 := STR0002 //"Dinamica de Eventos - TDE"
Local cDesc3 := ""
Local cString := STR0003 //"BA8"
Local Tamanho := "G"

PRIVATE cTitulo:= STR0004 //"Relatorio Tabela Dinamica de Eventos - TDE"
PRIVATE cabec1
PRIVATE cabec2
Private aReturn := { STR0009, 1,STR0010, 2, 2, 1, "",1 } //"Zebrado"###"Administracao"
Private cPerg   := "PLR504"
Private nomeprog:= "PLSR504" 
Private nLastKey:=0

//��������������������������������������������������������������Ŀ
//� Definicao dos cabecalhos                                     �
//����������������������������������������������������������������
cabec1:= STR0011+"           "+STR0012+"                                                                                                 " //"CODIGO"###"DESCRICAO"
cabec2:= ""
//        123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
//                 1         2         3         4         5         6         7         8         9        10        11        12        13        14        15
//��������������������������������������������������������������Ŀ
//� Envia controle para a funcao SETPRINT                        �
//����������������������������������������������������������������
wnrel := "PLR504"

Pergunte(cPerg,.F.)

wnrel := SetPrint(cString,wnrel,cPerg,@cTitulo,cDesc1,cDesc2,cDesc3,.F.,"",,Tamanho,,.F.)

If nLastKey == 27
   Return
End

SetDefault(aReturn,cString)

If nLastKey == 27
   Return ( NIL )
End

RptStatus({|lEnd| PLSR504Imp(@lEnd,wnRel,cString)},cTitulo)

Return
/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
��� Fun��o    �PLSR504Imp� Autor � Paulo Carnelossi      � Data � 02/09/03 ���
��������������������������������������������������������������������������Ĵ��
��� Descri��o �Impressao Tabela de Honorarios Medicos                      ���
��������������������������������������������������������������������������Ĵ��
��� Sintaxe   �PLSR504Imp(lEnd,wnRel,cString)                              ���
��������������������������������������������������������������������������Ĵ��
��� Uso       �                                                            ���
��������������������������������������������������������������������������Ĵ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/
Static Function PLSR504Imp(lEnd,wnRel,cString)
Local cbcont,cbtxt
Local tamanho:= "G"
Local nTipo

LOCAL cSql
Local cArqTrab := CriaTrab(nil,.F.)
Local cCodOpe  := mv_par01
Local cCodTab  := mv_par02
//Local cProcDe, cProcAte
Local lCabec := .F., lItens := .F., lSoma := .F.
Local lGrupo,  lSubGrp 
Local cGruPro		//, lSubPro
Local nX, nCtd

Local aDescri := {}
Local aUnidMed := {}

//��������������������������������������������������������������Ŀ
//� Variaveis utilizadas para Impressao do Cabecalho e Rodape    �
//����������������������������������������������������������������
cbtxt    := SPACE(10)
cbcont   := 0
li       := 80
m_pag    := 1

cTitulo :=	STR0005 //"Relatorio Tabela Dinamica de Eventos - "
cTitulo +=  mv_par02 + " - " + Padr(Posicione("BF8",1,xFilial("BF8")+cCodTab,"BF8_DESCM"),30)
cTitulo +=  STR0006+cCodOpe+" - "+ Padr(Posicione("BA0",1,xFilial("BA0")+cCodOpe,"BA0_NOMINT"),45) //"   ***  Operadora : "

nTipo:=GetMv("MV_COMP")

cSql := "SELECT BA8.BA8_CODPRO, BA8.BA8_DESCRI, BA8.BA8_NIVEL FROM "
cSql += RetSQLName("BA8")+" BA8 "
cSql += "WHERE "
//--considerar somente registros validos
cSql += "BA8.D_E_L_E_T_ <> '*' AND "
//--condicao principal 
cSql += "	BA8.BA8_FILIAL = '"+xFilial("BA8")+"' AND "
cSql += "	BA8.BA8_CODTAB = '"+MV_PAR02+"' AND "
cSql += "	BA8.BA8_CODPRO >= '"+Left(MV_PAR03,2)+"' AND "
cSql += "	BA8.BA8_CODPRO <= '"+MV_PAR04+"' "
cSql += " ORDER BY BA8_CODPRO, BA8_NIVEL"

PLSQuery(cSql,cArqTrab)

dbSelectArea(cArqTrab)
SetRegua(RecCount())
(cArqTrab)->(dbGotop())

While	(cArqTrab)->(! EOF())

	IncRegua()
	cGruPro := Left((cArqTrab)->BA8_CODPRO, 2)
   lGrupo := .T.
   
	While (cArqTrab)->( ! Eof() .And. Left((cArqTrab)->BA8_CODPRO, 2) == cGruPro )


		cSubPro := Subs((cArqTrab)->BA8_CODPRO, 3, 2)
      lSubGrp:= .T.
		aUnidMed := Unid_Med(cGruPro, cSubPro, cCodTab)
		Cabec1 := Padr(Cabec1, 137)
		For nX := 1 To Len(aUnidMed)
			Cabec1 += PadL(aUnidMed[nx][1],10)
		Next

		nCtd := 0
		lSoma := .F.
		For nX := 1 TO Len(aUnidMed)
			nCtd += If(aUnidMed[nx][2], 1, 0)
		Next
		If nCtd > 1
		   Cabec1 += PadL(STR0007,10) //"Total CRR"
		   lSoma := .T.
		EndIf
		lItens := .F.		
				
		While (cArqTrab)->( ! Eof() .And. Left((cArqTrab)->BA8_CODPRO, 4) == ;
																					cGruPro+cSubPro)
			IF li > 60 .Or. lCabec
				cabec(cTitulo,cabec1,cabec2,nomeprog,tamanho,nTipo)
				lCabec := .F.
			End
			
			If lGrupo .And. (cArqTrab)->BA8_NIVEL = '1'
				@ li, 000 Psay Padr(STR0008 + Transform((cArqTrab)->BA8_CODPRO,"@R 99.99.999-9") + " - " + (cArqTrab)->BA8_DESCRI,220) //"*** Grupo : "
				li+=2
				lGrupo := .F.
				
			ElseIf lSubGrp .And. (cArqTrab)->BA8_NIVEL = '2'
			   If Left((cArqTrab)->BA8_CODPRO, 4) >= Left(mv_par03, 4)
					@ li, 000 Psay Cabec1
					li++
					@ li, 000 Psay Repl("-",220)
					li++
					@ li, 000 Psay Padr("                 - "+Trim((cArqTrab)->BA8_DESCRI)+" - "+cSubPro+" ("+Trim(Transform((cArqTrab)->BA8_CODPRO,"@R 99.99.999-9"))+")",220)
					li+=2
					lSubGrp := .F.
				EndIf	
		   Else
            If (cArqTrab)->BA8_CODPRO >= mv_par03
			   	aDescri := QbTexto((cArqTrab)->BA8_DESCRI, 120, " ")
			      @ li, 000 PSAY Transform((cArqTrab)->BA8_CODPRO,"@R 99.99.999-9")
	            lItens := .T.
			      For nX := 1 To Len(aDescri)
				      
				      @ li, 017 PSAY aDescri[nX]
				      
				      If nX == 1
				         Lista_BD4(cCodTab, (cArqTrab)->BA8_CODPRO, aUnidMed, lSoma)
				    	EndIf
				    	
				    	li++
				    	
				   Next 	
			   EndIf
		   EndIf
		   
		   (cArqTrab)->(dbSkip())
				
		End
	   li++
	End
   
   lCabec := lItens
   
End

IF li != 80
	roda(cbcont,cbtxt,tamanho)
End
//��������������������������������������������������������������Ŀ
//� Recupera a Integridade dos dados                             �
//����������������������������������������������������������������
dbSelectArea(cArqTrab)
dbCloseArea()

dbSelectArea("BA8")

Set Device To Screen

If aReturn[5] = 1
   Set Printer To
	dbCommitAll()
   OurSpool(wnrel)
Endif

MS_FLUSH()

Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PLSR504   �Autor  �Paulo Carnelossi    � Data �  02/09/03   ���
�������������������������������������������������������������������������͹��
���Desc.     �Quebra o Texto de acordo com tamnho e separador informado   ���
���          �devolvendo um array com a string quebrada                   ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function QbTexto(cTexto, nTamanho, cSeparador)
Local aString := {}, nTamAux := nTamanho
Local nPos, nCtd, nTamOri := Len(cTexto), cAuxTexto

If Len(Trim(cTexto)) > 0

   If Len(Trim(cTexto)) <= nTamanho

      If Len(Trim(cTexto)) > 0
	      aAdd(aString, Trim(cTexto) )
      EndIf

   Else
	
		If (nPos := At(cSeparador, cTexto)) != 0
		   
			For nCtd := 1 TO nTamOri STEP nTamAux
		
				cAuxTexto := Subs(cTexto, nCtd, nTamanho)
			
			   While Len(Subs(cAuxTexto, Len(cAuxTexto), 1)) > 0 .And. ;
			   				Subs(cAuxTexto, Len(cAuxTexto), 1) <> cSeparador
			   
		   		cAuxTexto := Subs(cAuxTexto, 1, Len(cAuxTexto)-1)
		   		
		      End
		      
		      If Len(cAuxTexto) > 0
			      cAuxTexto 	:= Subs(cTexto, nCtd, Len(cAuxTexto))
			      nTamAux 		:= Len(cAuxTexto)
		      Else
		      	cAuxTexto := Subs(cTexto, nCtd, nTamanho)
			      nTamAux 		:= nTamanho
		      EndIf
		
		      If Len(Trim(cAuxTexto)) > 0
			      aAdd(aString, cAuxTexto)
		      EndIf
		   Next
		
		Else
		
			For nCtd := 1 TO nTamOri STEP nTamanho
			   If Len(Subs(cTexto, nCtd, nTamanho)) > 0 
			      If Len(Trim(Subs(cTexto, nCtd, nTamanho))) > 0
						aAdd(aString, Subs(cTexto, nCtd, nTamanho))
					EndIf	
				EndIf	
			Next
		
		EndIf
		
	EndIf
	
EndIf
Return aString

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �Unid_Med  �Autor  �Paulo Carnelossi    � Data �  02/09/03   ���
�������������������������������������������������������������������������͹��
���Desc.     �devolve um array com as unidades medicas para determinado   ���
���          �sub-grupo e se os valores serao somados na coluna total     ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function Unid_Med(cGruPro, cSubPro, cCodTab)
Local cSql, aUnidMed := {}
Local cArqTrb1 := CriaTrab(nil,.F.)

cSql := " SELECT DISTINCT BD4.BD4_CODIGO, BD3_ORDREL, BD3_SOMREL FROM "
cSql += RetSQLName("BD4")+" BD4, "+RetSQLName("BD3")+" BD3 "
cSql += " WHERE SUBSTRING(BD4.BD4_CODPRO, 1, 4) = '"+cGruPro+cSubPro+"' AND"
cSql += " BD4.BD4_CODTAB = '"+cCodTab+"' AND BD4.BD4_CODIGO = BD3.BD3_CODIGO AND "
cSql += " BD4.D_E_L_E_T_ <> '*' AND BD3.D_E_L_E_T_ <> '*' "
cSql += " ORDER BY BD3_ORDREL "

PLSQuery(cSql,cArqTrb1)				

(cArqTrb1)->(dbGoTop())

While (cArqTrb1)->(! Eof())
   
	aAdd(aUnidMed, { Trim((cArqTrb1)->BD4_CODIGO), (cArqTrb1)->BD3_SOMREL=="1" } )
	(cArqTrb1)->(dbSkip())
	
End

dbSelectArea(cArqTrb1)
dbCloseArea()

Return(aUnidMed)
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �Lista_BD4 �Autor  �Paulo Carnelossi    � Data �  02/09/03   ���
�������������������������������������������������������������������������͹��
���Desc.     �Lista os valores referencias na posicao determinada pelo    ���
���          �array somando alguns valores caso lsoma = .t.               ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function Lista_BD4(cCodTab, cCodPro, aUnidMed, lSoma)
Local cSql, nTotal := 0, nPos
Local cArqTrb1 := CriaTrab(nil,.F.)

cSql := " SELECT BD4.BD4_CODIGO, BD4_VALREF,BD3_ORDREL,BD4.R_E_C_N_O_ as RECNO FROM "
cSql += RetSQLName("BD4")+" BD4, "+RetSQLName("BD3")+" BD3 "
cSql += " WHERE BD4.BD4_FILIAL = '"+xFilial("BD4")+"' AND BD3.BD3_FILIAL = '"+xFilial("BD3")+"' AND "
cSql += " BD4.BD4_CODPRO = '"+cCodPro+"' AND"
cSql += " BD4.BD4_CODTAB = '"+cCodTab+"' AND BD4.BD4_CODIGO = BD3.BD3_CODIGO AND "
cSql += " BD4.D_E_L_E_T_ <> '*' AND BD3.D_E_L_E_T_ <> '*' "
cSql += " ORDER BY BD3_ORDREL "

PLSQuery(cSql,cArqTrb1)				

(cArqTrb1)->(dbGoTop())

While (cArqTrb1)->(! Eof())
   
   nPos := ASCAN( aUnidMed, {|aVal| aVal[1] == TRIM((cArqTrb1)->BD4_CODIGO)} )
   
   If lSoma .And. aUnidMed[nPos][2]
      nTotal += (cArqTrb1)->BD4_VALREF
   EndIf
   
	BD4->(DbGoto((cArqTrb1)->RECNO))
   
   If PLSINTVAL("BD4","BD4_VIGINI","BD4_VIGFIM",dDataBase)
		If TRIM((cArqTrb1)->BD4_CODIGO) <> "FIL"
			@ li, 137+(nPos*10)-10 PSay Transform((cArqTrb1)->BD4_VALREF,"@E 99,999,999.99")
		Else  // para filmes imprime com 4 decimais
			@ li, 137+(nPos*10)-10 PSay Transform((cArqTrb1)->BD4_VALREF,"@E 99,999,999.99")
		EndIf
	Endif
	(cArqTrb1)->(dbSkip())
	
End

If lSoma .And. nTotal > 0
	@ li, 137+(Len(aUnidMed)*10) PSay Transform(nTotal,"@E 99,999,999.99" )
EndIf

dbSelectArea(cArqTrb1)
dbCloseArea()

Return NIL