
#include "PROTHEUS.CH"
#include "PLSMGER.CH"
#include "PLSR736.CH"
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � PLSR736  � Autor � Tulio Cesar           � Data � 22.11.03 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Orcamento de compra de uma guia                            ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � PLSR736                                                    ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Generico                                                   ���
�������������������������������������������������������������������������Ĵ��
��� ATUALIZACOES SOFRIDAS DESDE A CONSTRUCAO INICIAL.                     ���
�������������������������������������������������������������������������Ĵ��
��� PROGRAMADOR  � DATA   � BOPS �  MOTIVO DA ALTERACAO                   ���
�������������������������������������������������������������������������Ĵ��
���              �        �      �                                        ��� 
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function PLSR736(aDadosCompra,cNomCli,cPagtoResp,nVlrTotal,cCodRda,cOpeRda,cCodLoc,cLocal,cMatric,cNomUsr,cCid,cEstSol,cRegSol,cSigla,cNomSol,cTipo)

//��������������������������������������������������������������Ŀ
//� Define Variaveis                                             �
//����������������������������������������������������������������
LOCAL aArea		:= GetArea()

PRIVATE wnRel
PRIVATE cNomeProg   := "PLSR730"
PRIVATE Titulo		:= oEmToAnsi(STR0001)
PRIVATE cDesc1      := oEmToAnsi(STR0001)
PRIVATE cDesc2      := ""
PRIVATE cDesc3      := ""
PRIVATE cString     := "BEA"
PRIVATE cPerg       := "PLR730"
PRIVATE Li         	:= 0
PRIVATE m_pag       := 1
PRIVATE lCompres    := .F.
PRIVATE lDicion     := .F.
PRIVATE lFiltro     := .T.
PRIVATE lCrystal    := .F.
PRIVATE aOrd		:= {}
PRIVATE lAbortPrint := .F.
PRIVATE cCabec1     := ""
PRIVATE cCabec2     := ""
PRIVATE aReturn 	:= { OemtoAnsi(STR0004), 1,OemtoAnsi(STR0005), 2, 2, 1, "", 1 }  	//"Zebrado"###"Administracao"
PRIVATE aLinha		:= { }
PRIVATE nomeprog	:="PLSR736",nLastKey := 0
PRIVATE Tamanho 	:= "P" 
PRIVATE nLimite     := 080

DEFAULT cTipo := "1"

DEFAULT cCodRda := ""
DEFAULT cOpeRda := ""
DEFAULT cCodLoc := ""
DEFAULT cLocal  := ""

BA1->(DbSetOrder(2))
BA1->(DbSeek(xFilial("BA1")+cMatric))

BA3->(DbSetOrder(1))
BA3->(DbSeek(xFilial("BA3")+BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC)))

wnrel:="PLSR736"    																	//--Nome Default do relatorio em Disco
wnrel:=SetPrint(cString,wnrel,,@Titulo,cDesc1,cDesc2,cDesc3,.T.,aOrd,.F.,tamanho)

/*��������������������������������������������������������������������������Ŀ
  | Verifica se foi cancelada a operacao                                     �
  ����������������������������������������������������������������������������*/
If nLastKey  == 27
	Return
Endif
/*��������������������������������������������������������������������������Ŀ
  � Configura impressora                                                     �
  ����������������������������������������������������������������������������*/
SetDefault(aReturn,cString)
If nLastKey = 27
	Return
Endif 

RptStatus({|lEnd| R736Imp(@lEnd,wnRel,cString,aDadosCompra,cNomCli,cPagtoResp,nVlrTotal,cCodRda,cOpeRda,;
                          cCodLoc,cLocal,cMatric,cNomUsr,cCid,cEstSol,cRegSol,cSigla,cNomSol)},Titulo)

/*
��������������������������������������������������������������Ŀ
�Restaura Area e Ordem de Entrada                              �
����������������������������������������������������������������*/
RestArea( aArea)
Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � R736IMP  � Autor � Tulio Cesar           � Data � 22.11.03 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Chamada do Relatorio                                       ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � PLSR736                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function R736Imp(lEnd,wnRel,cString,aDadosCompra,cNomCli,cPagtoResp,nVlrTotal,cCodRda,cOpeRda,;
                          cCodLoc,cLocal,cMatric,cNomUsr,cCid,cEstSol,cRegSol,cSigla,cNomSol)

//Local cDet 			:= ""
//Local cChave		:= ""
//Local cPict			:= "@E 9,999.9999"
Local nCount		:= 0 
Local nLinDetMax	:= 05 									//-- Numero maximo de linhas detalhe
LOCAL nValorTotal   := 0
Local nFor
Private nLin		:= 02

/*��������������������������������������������������������������Ŀ
  �Impressao do Cabecalho                                        �
  ����������������������������������������������������������������*/
	fImpCabec(cCodRda,cOpeRda,cCodLoc,cLocal)
/*��������������������������������������������������������������Ŀ
  �Dados do Usuario                                              |
  ����������������������������������������������������������������*/
	fDadosUsua(cMatric,cNomUsr)
/*��������������������������������������������������������������Ŀ
  �Cabecalho do Detalhe                                          |
  ����������������������������������������������������������������*/
	fDetCabec(cCid)

For nFor := 1 To Len(aDadosCompra)

		If ncount =  nLinDetMax
			/*��������������������������������������������������������������Ŀ
			  �Impressao do Cabecalho                                        �
			  ����������������������������������������������������������������*/
				fImpCabec(cCodRda,cOpeRda,cCodLoc,cLocal)
			/*��������������������������������������������������������������Ŀ
			  �Dados do Usuario                                              |
			  ����������������������������������������������������������������*/
				fDadosUsua(cMatric,cNomUsr)
			/*��������������������������������������������������������������Ŀ
			  �Cabecalho do Detalhe                                          |
			  ����������������������������������������������������������������*/
				fDetCabec(cCid)
		Endif

		//-- Linha de Detalhe
		@ nLIn, 001 psay TransForm(aDadosCompra[nFor,2],"@R !!.!!.!!!-!") 	//-- AMB
		@ nLIn, 013 psay Left(aDadosCompra[nFor,4],37)                      //-- Descricao
		@ nLIn, 054 psay Transform(aDadosCompra[nFor,3],"@R 99")            //-- Qtde
		@ nLIn, 060 psay Transform(aDadosCompra[nFor,6],"@E 999,999.99")            //-- Valor
		nLin ++
		nCount	++
		
		nValorTotal += aDadosCompra[nFor,6]
Next   

@ nLin ,001 psay  "---------------------------------------------------------------------"

nLin ++
nLin ++
@ nLin ,001 psay  "                                             VALOR TOTAL   "+TransForm(nValorTotal,"@E 999,999.99")
nLin ++
nLin ++
@ nLin ,001 psay  AllTrim(BA0->BA0_CIDADE)+", "+subs(dtos(dDataBase),7,2)+" de "+PLRETMES(val(subs(dtos(dDataBase),5,2)),"2")+" de "+subs(dtos(dDataBase),1,4)+"."
nLin ++
nLin ++
nLin ++
@ nLin ,001 psay  "                *** VALORES REFERENTES A ESTA DATA ***"
nLin ++
nLin ++
@ nLin ,001 psay  "---------------------------------------------------------------------"


/*��������������������������������������������������������������������������Ŀ
  � Libera impressao                                                         �
  ����������������������������������������������������������������������������*/
m_pag := 0

If  aReturn[5] == 1
	Set Printer To
	Ourspool(wnRel)
EndIf
/*��������������������������������������������������������������������������Ŀ
  � Fim do Relat�rio                                                         �
  ����������������������������������������������������������������������������*/

Return

/*
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������ͻ��
���Programa  �fImpCabec   �Autor  � Tulio Cesar        � Data �  22.11.03   ���
���������������������������������������������������������������������������͹��
���Desc.     �Cabecalho da Guia                                             ���
���          �                                                              ���
���������������������������������������������������������������������������͹��
���Uso       � Generico                                                     ���
���������������������������������������������������������������������������ͼ��
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
*/
Static Function fImpCabec(cCodRda,cOpeRda,cCodLoc,cLocal)
Local cFileLogo		:= ""
Local cPrazoLib		:= StrZero(GetMv("MV_PLPRZLB"),02)
Local cDet			:= ""          

/*��������������������������������������������������������������Ŀ
  �Reinicializa nLIn                     					     �
  ����������������������������������������������������������������*/
nLin		:= 02

/*��������������������������������������������������������������Ŀ
  �Setar Impressora Tamanho Normal       					     �
  ����������������������������������������������������������������*/
aDriver		:= ReadDriver()                  
If aReturn[4] == 1  // Comprimido
    @ 0,0 PSAY &(if(Tamanho=="P",aDriver[2],if(Tamanho=="G",aDriver[5],aDriver[3])))
Else                // Normal
    @ 0,0 PSAY &(if(Tamanho=="P",aDriver[2],if(Tamanho=="G",aDriver[6],aDriver[4])))
Endif

@ nLin , 0 PSAY &(aDriver[4])

/*��������������������������������������������������������������Ŀ
  �Impressao dos Dados Da Operadora 						     �
  ����������������������������������������������������������������*/
fImpNomOpe()

/*��������������������������������������������������������������Ŀ
  �AUTORIZACAO DE GUIA                                           �
  ����������������������������������������������������������������*/
nLin ++
cDet := OemToAnsi(STR0001)
@ nLin , 020 psay cDet 
nLin ++     
nLin ++             

/*��������������������������������������������������������������Ŀ
  �Prestador de Servico                                          �
  ����������������������������������������������������������������*/
BB8->(DbSetOrder(1))
BB8->(DbSeek(xFilial("BB8")  +cCodRda+cOperda+cCodLoc+cLocal))
@ nLin , 001 psay OemToAnsi(STR0013)+ BAU->BAU_NOME
nLin ++
@ nLin , 001 psay OemToAnsi(STR0018)+ BB8->(Alltrim(BB8->BB8_END)) +","  + AllTrim(BB8->BB8_NR_END)+"-"+Alltrim(BB8->BB8_COMEND)  
nLin ++ 
@ nLin , 013 psay alltrim(BB8->BB8_BAIRRO)+ space(1) + Alltrim(fDesc("BID",BB8->BB8_CODMUN,"BID_DESCRI")) + "-" + BB8->BB8_EST  + space(1) + "Fone: "+ BB8->BB8_TEL
nLin  ++

Return(nil)

/*
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������ͻ��
���Programa  �fDadosUsua  �Autor  �Tulio Cesar         � Data �  22.11.03   ���
���������������������������������������������������������������������������͹��
���Desc.     �Informacoes do Usuario                                        ���
���          �                                                              ���
���������������������������������������������������������������������������͹��
���Uso       � Generico                                                     ���
���������������������������������������������������������������������������ͼ��
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
*/ 
Static Function fDadosUsua(cMatric,cNomUsr)
Local cDet := "" 

/*��������������������������������������������������������������Ŀ
  �Usuario  / codigo                                             �
  ����������������������������������������������������������������*/
nLin ++
cDet 	:= OemToAnsi(STR0007) + space(1) + TransForm(cMatric,__cPictUsr)+"-"+BA1->BA1_DIGITO
cDet	+= space(2) + cNomUsr + space(1)
@ nLIn , 001 psay cDet 
nLin ++            
/*��������������������������������������������������������������Ŀ
  �Empresa                                                       �
  ����������������������������������������������������������������*/
BG9->(DbSetOrder(1))
BG9->(DbSeek( xFilial("BG9") + BA1->BA1_CODINT+BA1->BA1_CODEMP )  )
cDet := OemToAnsi(STR0008)   + space(1) + BA1->BA1_CODEMP + "-"
cDet += BG9->BG9_DESCRI 
@ nLin ,001 psay cDet
nLin ++

/*��������������������������������������������������������������Ŀ
  �Plano                                                         �
  ����������������������������������������������������������������*/
BI3->(DbSetOrder(1))
BI3->(DbSeek(xFilial("BI3")+BA3->(BA3_CODINT+BA3_CODPLA+BA3_VERSAO)))
cDet := OemToAnsi(STR0009) + space(1) + BI3->(BI3_CODIGO + "-"+BI3_DESCRI ) 
@ nLin ,001 psay cDet
nLin += 1


Return

/*
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������ͻ��
���Programa  �fDDetCabec  �Autor  �Tulio Cesar         � Data �  22.11.03   ���
���������������������������������������������������������������������������͹��
���Desc.     �Imprime cabecalho da Guia                                     ���
���          � (Linha de Detalhe)                                           ���
���������������������������������������������������������������������������͹��
���Uso       � Generico                                                     ���
���������������������������������������������������������������������������ͼ��
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
*/
Static Function  fDetCabec(cCid)
//Local cDet := ""

nLin ++
@ nLin, 001 psay  oemToAnsi(STR0011) 									//-- Procedimentos autorizados
nLin  ++
@ nLin ,001 psay  "---------------------------------------------------------------------"
nLin  ++
@ nLin, 001 psay  oemToAnsi(STR0015)
nLin  ++
@ nLin ,001 psay  "---------------------------------------------------------------------"
nLin  ++                                                                                 

Return                                                                                   




/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �fImpNomOpe�Autor  � Tulio Cesar        � Data �  22.11.03   ���
�������������������������������������������������������������������������͹��
���Desc.     � Impressao dos Dados da Operadora                           ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP6                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function fImpNomOpe()

/*��������������������������������������������������������������Ŀ
  �Nome da Operadora 										     �
  ����������������������������������������������������������������*/

BA0->(DbSetOrder(1))
BA0->(DbSeek(xFilial("BA0")+ BA1->BA1_CODINT ) ) 
@ nLin, 001 psay  BA0->BA0_NOMINT
nLin  ++ 
/*��������������������������������������������������������������Ŀ
  �Endereco                										 �
  ����������������������������������������������������������������*/
BID->(DbSetOrder(1)) 
BID->(DbSeek( xFilial("BID")+BA0->(BA0_CODMUN ) )  )
@ nLin, 001 psay alltrim(BA0->BA0_END)   + space(02) + alltrim(BA0->BA0_BAIRRO)
nLin ++
@ nLIn, 001 psay Alltrim(BID->BID_DESCRI) + "-"+ BA0->BA0_EST + space(5) +"Cep:"  + BA0->BA0_CEP + space(05) +  "CGC: " +Transform(BA0->BA0_CGC,  "@R ##.###.###/####-##" )
nLin  ++


Return( Nil )
