#Include "TopConn.CH"
#Include "Protheus.ch"
#Include "PLSR953.ch"
static objCENFUNLGP := CENFUNLGP():New() 
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �PLSR953   � Autor �F�bio S. dos Santos	� Data �28/07/2015���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Relat�rio de Capta��o.					                  ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � TOTVS - PLS				                                  ���
�������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.             ���
�������������������������������������������������������������������������Ĵ��
���Programador � Data   � Motivo da Alteracao                             ���
�������������������������������������������������������������������������Ĵ��
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Function PLSR953()
Local oReport
Private cAlias := GetNextAlias()
Private nPagina		:= 0

//-- LGPD ----------
if !objCENFUNLGP:getPermPessoais()
	objCENFUNLGP:msgNoPermissions()
	Return
Endif
//------------------

oReport:= ReportDef()
oReport:PrintDialog()

Return

/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Funcao    �ReportDef � Autor � F�bio S. dos Santos  	  � Data �28/07/2015���
���������������������������������������������������������������������������Ĵ��
���Descricao � Cria Celulas que serao Impressas no Relatorio                ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
/*/
Static Function ReportDef()
Local oReport

Local oSecTion1
Local oSecTion2
Local oSecTion3

Local cPerg := "PLSR953"

Pergunte(cPerg,.F.)

oReport:= TReport():New("PLSR953",STR0001,cPerg,{|oReport|PrintReport(oReport)},STR0001)//Relat�rio de Capta��es
oReport:SetLandscape(.T.) // Imprimir relat�rio em formato paisagem
oReport:SetTotalInLine(.f.)
oReport:lParamPage := .f.

oSection1 := TRSection():New(oReport,"PROSPECT X ENDERECO",{"B9V"}) //tabelas que ser�o usadas no programa
oSection1:SetTotalInLine(.F.)
oSection1:SetHeaderPage(.F.)  
oSection1:SetHeaderSection(.T.)
TRCell():New(oSection1,"B9V_TIPEST","PLSR953",STR0002,,13,,,,,,,,,,,,.F.)//"Cod. Estab."
TRCell():New(oSection1,"B9V_CEP","PLSR953",STR0016,,10,,,,,,,,,,,,.F.)//"CEP"
TRCell():New(oSection1,"B9V_ENDER","PLSR953",STR0003,,,,,,,,,,,,,,.F.)//"Endereco"
TRCell():New(oSection1,"B9V_NUMERO","PLSR953",STR0004,,10,,,,,,,,,,,,.F.)//"N�mero"	
TRCell():New(oSection1,"B9V_COMEND","PLSR953",STR0005,,,,,,,,,,,,,,.F.)//"Complemento"
TRCell():New(oSection1,"B9V_BAIRRO","PLSR953",STR0006,,,,,,,,,,,,,,.F.)//"Bairro"
TRCell():New(oSection1,"B9V_CIDADE","PLSR953",STR0007,,,,,,,,,,,,,,.F.)//"Cidade"
TRCell():New(oSection1,"B9V_EST","PLSR953",STR0008,,10,,,,,,,,,,,,.F.)//"Estado"
TRCell():New(oSection1,"B9V_TEL","PLSR953",STR0009,,,,,,,,,,,,,,.F.)//"Telefone"	
TRCell():New(oSection1,"B9V_EMAIL","PLSR953",STR0010,,,,,,,,,,,,,,.F.)//"E-mail"

oSection2 := TRSection():New(oReport,"PROSPECT X EXPECIALIDADE",{"B9Q"}) //tabelas que ser�o usadas no programa
oSection2:SetTotalInLine(.F.)
//oSection2:SetHeaderPage(.T.)  
TRCell():New(oSection2,"B9Q_ITEM","TRBB9Q",STR0011,,,,,,,,,,,,,,.F.)//"Item"
TRCell():New(oSection2,"B9Q_CODESP","TRBB9Q",STR0012,,,,,,,,,,,,,,.F.)//"C�d. Espec."
TRCell():New(oSection2,"B9Q_DESCR","TRBB9Q",STR0013,,100,,,,,,,,,,,,.F.)//"Descri��o"
TRCell():New(oSection2,"B9Q_TEMFOR","TRBB9Q",STR0014,,20,,,,,,,,,,,,.F.)//"Tempo de Forma��o"	

oSection3 := TRSection():New(oReport,"PROSPECT X SERVICOS",{"B9R"}) //tabelas que ser�o usadas no programa
oSection3:SetTotalInLine(.F.)
//oSection3:SetHeaderPage(.T.)  
TRCell():New(oSection3,"B9R_ITEM","TRBB9R",STR0011,,,,,,,,,,,,,,.F.)//"Item"
TRCell():New(oSection3,"B9R_CODSER","TRBB9R",STR0015,,,,,,,,,,,,,,.F.)//"C�d. Servi�o"
TRCell():New(oSection3,"B9R_DESCR","TRBB9R",STR0013,,,,,,,,,,,,,,.F.)//"Descri��o"

Return oReport

/*/
��������������������������������������������������������������������������������
��������������������������������������������������������������������������������
����������������������������������������������������������������������������Ŀ��
���Funcao    �PrintReport�Autor  � F�bio S. dos Santos	   � Data � 03/02/14 ���
����������������������������������������������������������������������������Ĵ��
���Descricao � Seleciona os dados para o Relat�rio.		                     ���
�����������������������������������������������������������������������������ٱ�
��������������������������������������������������������������������������������
��������������������������������������������������������������������������������
/*/

Static Function PrintReport(oReport)
Local cQuery 		:= ""
Local cQueryB9P		:= ""
Local nRegis		:= 0
Local cDescEsp		:= ""
Local cDescSer		:= ""
Local cSeqAnt		:= ""
Local cItem			:= ""
Local nPageWidth  	:= oReport:PageWidth()
Local nLinhas		:= 70 // numero total de linhas
Local oSection1 	:= oReport:Section(1) 
Local oSection2 	:= oReport:Section(2) 
Local oSection3 	:= oReport:Section(3) 
Private cAlias		:= "PLSR953"
oSection1:BeginQuery()
BeginSql Alias cAlias
	
	SELECT B9P_SEQVIS, B9P_AGENTE, B9P_PRESTA, B9P_PRICON, B9P_CPFCGC, B9P_CRMNUM, B9P_DATAAG, B9P_HORAAG, B9P_DATAVI, B9P_HORAVI, B9P_NOMCON, B9P_DESCON, 
	B9P_EMAIL, B9P_MOTVIS, B9P_CODSOL, B9P_REVNEG, B9P_INTCON, B9P_PROVCT, B9P_CEMIGS, B9P_FCAPTA, B9P.R_E_C_N_O_ RECNO,
	B9V_CODINT, B9V_CODPRO, B9V_CODSEQ, B9V_SEQB9V, B9V.B9V_TIPEST, B9V.B9V_CEP, B9V.B9V_ENDER, B9V.B9V_NUMERO, B9V.B9V_COMEND, B9V.B9V_BAIRRO,  
	B9V.B9V_CODCID, B9V.B9V_EST, B9V.B9V_TEL, B9V.B9V_EMAIL 
	FROM %table:B9P% B9P
	INNER JOIN %table:B9V% B9V ON
	B9V.B9V_FILIAL = %exp:xFilial("B9V")%  AND
	B9P.B9P_CODINT = B9V.B9V_CODINT AND
	B9P.B9P_CODPRO = B9V.B9V_CODPRO AND
	B9P.B9P_SEQVIS = B9V.B9V_SEQB9V AND
	B9V.B9V_CODCID BETWEEN %exp:Mv_PAR07% AND %exp:Mv_PAR08% AND 
	B9V.D_E_L_E_T_ = ''
	WHERE 
	B9P.B9P_FILIAL = %exp:xFilial("B9P")%  AND 
	B9P.B9P_AGENTE BETWEEN %exp:Mv_PAR01% AND %exp:Mv_PAR02% AND 
	B9P.B9P_DATAAG BETWEEN %exp:Mv_PAR03% AND %exp:Mv_PAR04% AND
	B9P.B9P_DATAVI BETWEEN %exp:Mv_PAR05% AND %exp:Mv_PAR06% AND 
	B9P.B9P_MOTVIS BETWEEN %exp:Mv_PAR13% AND %exp:Mv_PAR14% AND 
	B9P.B9P_PRICON = %exp:Iif(Mv_PAR15 == 1,"1","0")%  AND
	B9P.B9P_REVNEG = %exp:Iif(Mv_PAR16 == 1,"1","0")%  AND
	B9P.B9P_INTCON = %exp:Iif(Mv_PAR17 == 1,"1","0")%  AND 
	B9P.B9P_PROVCT = %exp:Iif(Mv_PAR18 == 1,"1","0")%  AND
	B9P.B9P_FCAPTA = %exp:ALLTRIM(STR(Mv_PAR19))%  AND
	B9P.D_E_L_E_T_ = ''

EndSql
oSection1:EndQuery()
cQueryB9P:= oSection1:GetQuery()

//Definindo a quantidade de registro da query (nRegis)

(cAlias)->(DbGotop())
While (cAlias)->(!Eof())
	nRegis++ //Contador para determinar a quantidade de registros da query
	(cAlias)->( DbSkip() )
End

(cAlias)->(DbGotop())
cSeqAnt	:= (cAlias)->B9P_SEQVIS
oReport:SetMeter(nRegis) //Seta com o metodo SetMeter o total de registros para controle da query
DbSelectArea("B9P")
DbGoTo((cAlias)->RECNO)
//�����������������������������������������������������������������������������������������Ŀ
//� Executa o Cabecalho para oReport:EndPage()                                              �
//�������������������������������������������������������������������������������������������
nPagina := 0
If !(cAlias)->(Eof())
	ImpCabec(oReport)
EndIf
While !oReport:Cancel() .And. (cAlias)->( ! EoF() ) //Enquanto n�o for fim dos registro e o usu�rio n�o clicar em cancelar

	oReport:IncMeter()  //Incrementa +1 na regua com o metodo IncMeter
	If oReport:Cancel() //Se clicou em Cancelar, sai do relatat�rio
		Exit
	EndIf
	
	If cSeqAnt!=(cAlias)->B9P_SEQVIS   
		cSeqAnt:= (cAlias)->B9P_SEQVIS
		B9P->(DbGoTo((cAlias)->RECNO))
		oReport:EndPage()
	   	oReport:StartPage()
	   	ImpCabec(oReport)	
	EndIf 	
	
	oSection1:Init()
	oSection1:Cell("B9V_CIDADE"):SetValue(POSICIONE("BID",1,XFILIAL("BID")+(cAlias)->B9V_CODCID,"BID_DESCRI"))
	
	oSection1:PrintLine()
	oSection1:Finish()
	
	//�����������������������������������������������������������������������������������������Ŀ
	//� Executa o Cabecalho para oReport:EndPage()                                              �
	//�������������������������������������������������������������������������������������������
	If oReport:Row() > oReport:LineHeight() * nLinhas
		oReport:EndPage()
	EndIf
	//Verificar a especialidade do Prestador
	cQuery := "SELECT B9Q_CODESP, B9Q_TEMFOR "
	cQuery += "FROM " + RetSqlName("B9Q") + " B9Q "
	cQuery += "WHERE B9Q_FILIAL = '" + xFilial("B9Q") + "' AND "
	cQuery += "B9Q_CODINT = '" + (cAlias)->B9V_CODINT + "' AND "
	cQuery += "B9Q_CODPRO = '" + (cAlias)->B9V_CODPRO + "' AND " 
	cQuery += "B9Q_CODLOC = '" + (cAlias)->B9V_CODSEQ + "' AND "
	cQuery += "B9Q_SEQVIS = '" + (cAlias)->B9V_SEQB9V + "' AND "
	cQuery += "B9Q_CODESP BETWEEN '" + MV_PAR09 + "' AND '" + MV_PAR10 + "' AND " 
	cQuery += "D_E_L_E_T_ ='' "
	
	If Select("TRBB9Q") > 0
		TRBB9Q->(DbCloseArea())
	EndIf
	
	TCQUERY cQuery NEW ALIAS "TRBB9Q"
	
	TRBB9Q->(DbGoTop())
	
	oSection2:Init()
	cItem := "001"
	While !TRBB9Q->(Eof())
		
		oSection2:Cell("B9Q_ITEM"):SetValue(cItem)
		cDescEsp := Posicione("BAQ",7,xFilial("BAQ")+TRBB9Q->B9Q_CODESP,"BAQ_DESCRI")
		oSection2:Cell("B9Q_DESCR"):SetValue(cDescEsp) 
		oSection2:Cell("B9Q_TEMFOR"):SetValue(DtoC(StoD(TRBB9Q->B9Q_TEMFOR)))                                                   
		oSection2:PrintLine()
		
		If oReport:Row() > oReport:LineHeight() * nLinhas
			oReport:EndPage()
		EndIf
		cItem := Soma1(cItem)
		TRBB9Q->(DbSkip())
	End
	oSection2:Finish()	
		                                                    
	//Verificar o servi�o do Prestador
	cQuery := "SELECT B9R_CODSER "
	cQuery += "FROM " + RetSqlName("B9R") + " B9R "
	cQuery += "WHERE B9R_FILIAL = '" + xFilial("B9R") + "' AND "
	cQuery += "B9R_CODINT = '" + (cAlias)->B9V_CODINT + "' AND "
	cQuery += "B9R_CODPRO = '" + (cAlias)->B9V_CODPRO + "' AND " 
	cQuery += "B9R_CODLOC = '" + (cAlias)->B9V_CODSEQ + "' AND "
	cQuery += "B9R_SEQVIS = '" + (cAlias)->B9V_SEQB9V + "' AND "
	cQuery += "B9R_CODSER BETWEEN '" + MV_PAR11 + "' AND '" + MV_PAR12 + "' AND "
	cQuery += "D_E_L_E_T_ ='' "
	
	If Select("TRBB9R") > 0
		TRBB9R->(DbCloseArea())
	EndIf
	
	TCQUERY cQuery NEW ALIAS "TRBB9R"
	
	TRBB9R->(DbGoTop())
	
	oSection3:Init()
	cItem := "001"
	While !TRBB9R->(Eof())
		oSection3:Cell("B9R_ITEM"):SetValue(cItem)    
		cDescSer := Posicione("B9O",2,xFilial("B9O")+PLSINTPAD()+TRBB9R->B9R_CODSER,"B9O_DESSER")
		oSection3:Cell("B9R_DESCR"):SetValue(cDescSer)                                                     
		oSection3:PrintLine()
		If oReport:Row() > oReport:LineHeight() * nLinhas
			oReport:EndPage()
		EndIf
		cItem := Soma1(cItem)
		TRBB9R->(DbSkip())
	End
	oSection3:Finish()
	
	(cAlias)->( DbSkip() ) //Proximo registro do Alias

End

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ImpCabec  �Autor  � Fabio S. dos Santos� Data � 30/07/2015  ���
�������������������������������������������������������������������������͹��
���Desc.     �Imprime o cabecalho das capta��es de visitas.               ���
���          �        				                                      ���
�������������������������������������������������������������������������͹��
���Uso       � Totvs - PLS                                                ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ImpCabec(oReport)
Local nPageWidth 	:= oReport:PageWidth()
Local nRow			:= oReport:Row()
Local cFormCap	 	:= ""
Local nCol01		:= 0060
Local nCol02   		:= 0660
Local nCol03   		:= 1260
Local nCol04   		:= 1920
Local nCol05   		:= 2520
                      
oReport:Row() += 250
//��������������������������������������������������������������������Ŀ
//�Linha 1                                                             �
//����������������������������������������������������������������������
oReport:PrintText( STR0017 + B9P->B9P_AGENTE, oReport:Row(), nCol01 )//"Agente: "
oReport:PrintText( STR0018 + DtoC(B9P->B9P_DATAAG), oReport:Row(), nCol02 )//"Data Agendada: "
oReport:PrintText( STR0019 + Posicione("B9L",2,xFilial("B9L")+PLSINTPAD()+B9P->B9P_MOTVIS,"B9L_DESVIS"), oReport:Row(), nCol03 )//"Motivo Visita: "
oReport:SkipLine(01)  

//��������������������������������������������������������������������Ŀ
//�Linha 2                                                             �
//����������������������������������������������������������������������
oReport:PrintText( STR0020 + Posicione("B9H",2,xFilial("B9P")+PLSINTPAD()+PLS806Zero(B9P->B9P_AGENTE),"B9H_NOME"), oReport:Row(), nCol01 )//"Nome Agente: "
oReport:PrintText( STR0021 + SubStr(B9P->B9P_HORAAG,1,2)+":"+SubStr(B9P->B9P_HORAAG,3,2), oReport:Row(), nCol02 )//"Hora Agendada: "
oReport:PrintText( STR0022 + B9P->B9P_CODSOL, oReport:Row(), nCol03 )//"Solicitante: "
oReport:SkipLine(01)

//��������������������������������������������������������������������Ŀ
//�Linha 3                                                             �
//����������������������������������������������������������������������
oReport:PrintText( STR0023 + B9P->B9P_PRESTA, oReport:Row(), nCol01 )//"Prestador: "
oReport:PrintText( STR0024 + DtoC(B9P->B9P_DATAVI), oReport:Row(), nCol02 )//"Data Visita: "    
oReport:PrintText( STR0025 + Iif(B9P->B9P_REVNEG == "0", "N�o", "Sim"), oReport:Row(), nCol03 )//"Revisita Neg.: "
oReport:SkipLine(01)

//��������������������������������������������������������������������Ŀ
//�Linha 4                                                             �
//����������������������������������������������������������������������
oReport:PrintText( STR0026 + Iif(B9P->B9P_PRICON == "0", "N�o", "Sim"), oReport:Row(), nCol01 )//"1� Contato: "
oReport:PrintText( STR0027 + SubStr(B9P->B9P_HORAVI,1,2)+":"+SubStr(B9P->B9P_HORAVI,3,2) , oReport:Row(), nCol02 )//"Hora Visita: "
oReport:PrintText( STR0028 + Iif(B9P->B9P_INTCON == "0", "N�o", "Sim"), oReport:Row(), nCol03 )//"Interesse Conv�nio: "
oReport:SkipLine(01)

//��������������������������������������������������������������������Ŀ
//�Linha 5                                                             �
//����������������������������������������������������������������������
oReport:PrintText( STR0029 + B9P->B9P_CPFCGC, oReport:Row(), nCol01 )//"CPF/CNPJ: "
oReport:PrintText( STR0030 + B9P->B9P_NOMCON, oReport:Row(), nCol02 )//"Contato: "
oReport:PrintText( STR0031 + Iif(B9P->B9P_PROVCT == "0", "N�o", "Sim"), oReport:Row(), nCol03 )//"Providenciar Contrato: "
oReport:SkipLine(01)

//��������������������������������������������������������������������Ŀ
//�Linha 6                                                             �
//����������������������������������������������������������������������
oReport:PrintText( STR0032 + B9P->B9P_CRMNUM, oReport:Row(), nCol01 )//"CRM: "
oReport:PrintText( STR0033 + B9P->B9P_DESCON, oReport:Row(), nCol02 )//"Tel. Contato: "
oReport:PrintText( STR0034 + AllTrim(Posicione("BA0",1,xFilial("BA0")+PLSINTPAD(),"BA0_NOMINT")) + " : " + Iif(B9P->B9P_CEMIGS == "0", "N�o", "Sim"), oReport:Row(), nCol03 )//"Tel. Contato: "
oReport:SkipLine(01)

//��������������������������������������������������������������������Ŀ
//�Linha 7                                                             �
//����������������������������������������������������������������������
If B9P->B9P_FCAPTA == "1"
	cFormCap := "Prestador Portal" 
ElseIf B9P->B9P_FCAPTA == "2"
	cFormCap := "Indicacao CallCenter"
ElseIf B9P->B9P_FCAPTA == "3"
	cFormCap := "Indicacao Portal"
Else
	cFormCap := "Operadora"
EndIf

oReport:PrintText( STR0035 + B9P->B9P_EMAIL, oReport:Row(), nCol02 )//"E-mail: "
oReport:PrintText( STR0036 + cFormCap, oReport:Row(), nCol03 )//"Forma Capta��o: "
oReport:SkipLine(02)

Return