#Include "TOTVS.CH"
#Define lLinux IsSrvUnix()
#IFDEF lLinux
	#define CRLF Chr(13) + Chr(10)
#ELSE
	#define CRLF Chr(10)
#ENDIF
#Define STR0001 "Relat�rio de processamento dos arquivos XMLs no padr�o TISS"
#Define STR0002 "N�o foi poss�vel abrir o arquivo: "
#Define STR0003 "Relat�rio de cr�ticas n�o encontrado: "
#Define STR0004 "Relat�rio de cr�ticas apenas para arquivos n�o acatados."
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  | PLSR973L � Autor �Microsiga           � Data �  04/19/12   ���
�������������������������������������������������������������������������͹��
���Desc.     �Relatorio de processamento de XML no padrao TISS            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function PLSR973L()
Local nFile := -1 //Handle do arquivo
Local nCol  := 30 //Coordenada horizontal
Local nLin  := 230 //Coordenada vertical
Local aCab  := {} //Itens do Cabecalho
Local lPags := .F.
Local cFile := PLSMUDSIS(AllTrim(BXX->BXX_ARQOUT))
Local cMvPORTAL2 := SuperGetMv("MV_PORTAL2")

If BXX->BXX_STATUS != "2"
	MsgInfo(STR0004)
	Return .F.
EndIf

If !File(cFile,0)
	MsgStop(STR0003 + CRLF + cFile)
	Return .F.
EndIf

nFile := FT_FUse(cFile)

If nFile == -1
	MsgInfo(STR0002 + cFile)//"N�o foi poss�vel abrir o arquivo: "
	Return .F.
EndIf

oPrint := TMSPrinter():New(STR0001)//"Relat�rio de processamento dos arquivos XMLs no padr�o TISS"        
oPrint:StartPage()
oPrint:SetPortrait()
lPrinter := oPrint:IsPrinterActive()

If !lPrinter
	oPrint:Setup()
EndIf

FT_FGoTop()
nRcLi := FT_FRecno()

While !FT_FEOF()

	cLin := FT_FReadLn()

	If nRcLi % 85 == 0
		oPrint:EndPage()
		nRcLi := 1
		nLin  := 230
		lPags := .T.
		oPrint:StartPage()
	EndIf
	
	Do Case

	Case nRcLi == 1 //Titulo
		oPrint:SayBitmap(30,30,cMvPORTAL2,250,150 )
		oFont := TFont():New('Courier new',,-12,,.T.,,,,,.F.,.F.)
		If !lPags
			cLin  := Space(20) + cLin
			aAdd(aCab,cLin)
		EndIf

	Case nRcLi == 2 //Linha
		nLin := nLin + 35
		oPrint:Line(nLin,nCol,nLin,2470)
		If !lPags
			aAdd(aCab,cLin)			
		EndIf

	Case nRcLi > 2 .And. nRcLi < 7 //Cabecalho
		oFont := TFont():New('Courier new',,-10,,.T.,,,,,.F.,.F.)
		If !lPags
			aAdd(aCab,cLin)		
		EndIf

	OtherWise //Itens
		oFont := TFont():New('Courier new',,-09,,.T.,,,,,.F.,.F.)
		If lPags      
			lPags := .F.
		EndIf

	EndCase

	oPrint:Say(nLin,nCol,Iif(!lPags,cLin,aCab[nRcLi]),oFont)
	oFont:End() 

	If !lPags
		FT_FSKIP()
	EndIf
	nLin  := nLin + 35
	nRcLi := nRcLi + 1

End

FT_FUSE()
oPrint:EndPage()
oPrint:Preview()
oPrint:End() 

Return .F.