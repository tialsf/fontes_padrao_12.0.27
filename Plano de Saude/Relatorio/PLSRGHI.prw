#INCLUDE "PLSRGHI.ch"
#include "PROTHEUS.CH"   

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � PLSRGHI	� Autor � Luciano Aparecido     � Data � 20.06.08 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Guia de Internacao Hospitalar /Resumo Interna��o           ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � PLSRGHI(nGuia) /1-Guia honorario Indiv.e 2-Guia de Despesas���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Generico                                                   ���
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function PLSRGHI(nGuia)
//��������������������������������������������������������������Ŀ
//� Define Variaveis                                             �
//����������������������������������������������������������������
Local CbCont, Cabec1, Cabec2, Cabec3, nPos, wnRel
Local cTamanho := "M"
Local cTitulo  := ""
Local cDesc1  := ""
Local cDesc2  := STR0001 //"de acordo com a configuracao do usuario."
Local cDesc3  := " "
//��������������������������������������������������������������������������Ŀ
//� Parametros do relatorio (SX1)...                                         �
//����������������������������������������������������������������������������
Local nLayout

Private aReturn  := { STR0002, 1,STR0003, 2, 2, 1, "", 1 } //"Zebrado"###"Administra��o"
Private aLinha   := { }
Private nLastKey := 0
Private cPerg    := ""	
	
If nGuia ==1
	cTitulo  := STR0004 //"GUIA DE HONOR�RIO INDIVIDUAL"
	cDesc1  := STR0005 //"Ira imprimir a Guia de Honor�rio Individual"
Else
	cTitulo  := STR0006 //"GUIA DE DESPESAS"
	cDesc1  := STR0007 //"Ira imprimir a Guia de Despesas"
Endif
	
cPerg := "PLRGHI"
	
//��������������������������������������������������������������������������Ŀ
//� Ajusta perguntas                                                         �
//����������������������������������������������������������������������������
CriaSX1() //nova pergunta...
			
//��������������������������������������������������������������Ŀ
//� Variaveis utilizadas para Impressao do Cabecalho e Rodape    �
//����������������������������������������������������������������
CbCont  := 0
	
if nGuia ==1
	Cabec1  := STR0004 //"GUIA DE HONOR�RIO INDIVIDUAL"
else
	Cabec1  := STR0006 //"GUIA DE DESPESAS"
Endif
	
Cabec2  := " "
Cabec3  := " "
cString := "B0D"
aOrd    := {}
wnRel   := "PLSRGHI" // Nome Default do relatorio em Disco
	
	
Pergunte(cPerg,.F.)

wRel := SetPrint(cString, wnrel, cPerg, @cTitulo, cDesc1, cDesc2, cDesc3, .T., aOrd, .F., cTamanho)
	
If nLastKey = 27
	Set Filter To
	Return
EndIf
	
nLayout := mv_par01
	
RptStatus({|lEnd| RGHIImp(@lEnd, wRel, cString, nGuia, nLayout)}, cTitulo)
	
Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � RGHIImp  � Autor � Luciano Aparecido		� Data � 20/06/08 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Chamada do Relatorio                                       ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � PLSRGHI	                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function RGHIImp(lEnd, wRel, cString, nGuia, nLayout)

Local aDados  := {}
	
aAdd(aDados, MtaDados(nGuia))
    
If nGuia == 1
	If PLSTISSVER() >= "3"
    	PlSTISSG(aDados,,nLayout)
    Else
    	PlSTISS5(aDados,,nLayout)
    EndIf
ElseIf nGuia == 2
	PlsTISS6(aDados,,nLayout) 
EndIf

Return

/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Funcao    � MtaDados � Autor � Luciano Aparecido       � Data � 22/03/07 ���
���������������������������������������������������������������������������Ĵ��
���Descri��o � Chama a funcao "PLSGHONI" que ira retornar o array com		���
���          � os dados a serem impressos.    								���
���������������������������������������������������������������������������Ĵ��
��� Uso      � PLSRGHI                                                      ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
/*/
Static Function MtaDados(nGuia)

Local aDados := {}

aDados := PLSGHONI(nGuia) // Funcao que monta o array com os dados da guia
	
Return aDados 

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Programa   � CriaSX1   � Autor � Luciano Aparecido    � Data � 22.03.07 ���
��������������������������������������������������������������������������Ĵ��
���Descricao  � Atualiza SX1                                               ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
/*/

Static Function CriaSX1()

LOCAL aRegs	 :=	{}

aadd(aRegs,{cPerg,"01",STR0008 ,"","","mv_ch1","N", 1,0,0,"C","","mv_par01",STR0009         	,"","","","",STR0010            	,"","","","",STR0011              ,"","","","",""       ,"","","","","","","","",""   ,""}) //"Selecionar Layout Papel:"###"Of�cio 2"###"Papel A4"###"Papel Carta"
	                                                                                                                                                                   
PlsVldPerg( aRegs )

Return
