
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Associa arquivo de definicoes                                            �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
#include "PROTHEUS.CH"
#include "PLSMGER.CH"

#DEFINE DEBITO  2
#DEFINE CREDITO 3
#DEFINE NTAMCOL 07 
#DEFINE CDEB 	"D"
/*/
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇
굇旼컴컴컴컴컫컴컴컴컴컫컴컴컴컫컴컴컴컴컴컴컴컴컴컴컴컴쩡컴컴컫컴컴컴컴컴엽굇
굇쿑uncao    � PLSR018 � Autor � Paulo Carnelossi       � Data � 20/01/04 낢굇
굇쳐컴컴컴컴컵컴컴컴컴컨컴컴컴컨컴컴컴컴컴컴컴컴컴컴컴컴좔컴컴컨컴컴컴컴컴눙굇
굇쿏escricao � Rel Geral Consolidado do Extrato Movimentacao Credenciado  낢굇
굇쳐컴컴컴컴컵컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴눙굇
굇쿞intaxe   � PLSR018()                                                  낢굇
굇쳐컴컴컴컴컵컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴눙굇
굇� Uso      � Advanced Protheus                                          낢굇
굇쳐컴컴컴컴컨컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴눙굇
굇� Alteracoes desde sua construcao inicial                               낢굇
굇쳐컴컴컴컴컫컴컴컴쩡컴컴컴컴컴컴쩡컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴눙굇
굇� Data     � BOPS � Programador � Breve Descricao                       낢굇
굇쳐컴컴컴컴컵컴컴컴탠컴컴컴컴컴컴탠컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴눙굇
굇�30/03/05  |      � Alexander   � -Acerto emissao do total em outra 	  낢굇
굇�			 |		|			  |	 pagina								  낢굇
굇�          |      |             | -Faltando uma condicao para calculo   낢굇
굇�          |      |             |    de deb/cred variavel				  낢굇
굇�			 |		|			  |	-Implementacao do calculo de PIS, 	  낢굇
굇�			 |		|			  |	 Cofins, CSLL, IR e INSS PF, INSSPJ   낢굇
굇�			 |		|			  |	 no valor bruto						  낢굇
굇�31/03/05	 |		|			  |	-A variavel nVlrBIR foi retirada pq	  낢굇
굇�			 |		|			  |	 estava pegando o valor bruto para 	  낢굇
굇�			 |		|			  |	 calculo de IR na funcao PLIRCRE como 낢굇
굇�			 |		|			  |	 o valor ja vem calculado do BMR nao e낢굇
굇�			 |		|			  |	 mais necessario.					  낢굇
굇�			 |		|			  |	-No tratamento de valores variados 	  낢굇
굇�			 |		|			  |	 estava sendo referenciado a tabela   낢굇
굇�			 |		|			  |	 BBC e nao BGQ.			 			  낢굇
굇�			 |		|			  |	-Foi implementado um ELSE para exibir 낢굇
굇�			 |		|			  |	 D/C (Fixo ou Variado) quando nao     낢굇
굇�			 |		|			  |	 existir movimentacao				  낢굇
굇�			 |		|			  |	-Inclui AllTrim nos Transforms 		  낢굇
굇읕컴컴컴컴컨컴컴컴좔컴컴컴컴컴컴좔컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴袂굇
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽
/*/
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Define nome da funcao                                                    �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Function PLSR018()
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Define variaveis padroes para todos os relatorios...                     �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
PRIVATE nQtdLin
PRIVATE cNomeProg   := "PLSR018"
PRIVATE nCaracter   := 15
PRIVATE nLimite     := 220
PRIVATE cTamanho    := "G"
PRIVATE cTitulo     := "Resumo de Pagamento do Credenciado (RDA) "
PRIVATE cDesc1      := "Este relatorio fornecera um resumo de pagamento do"
PRIVATE cDesc2      := "credenciado"
PRIVATE cDesc3      := ""
PRIVATE cAlias      := "BD7"
PRIVATE cPerg       := "PLR018"
PRIVATE nRel        := "PLSR018"
PRIVATE m_pag       := 1
PRIVATE lCompres    := .F.
PRIVATE lDicion     := .F.
PRIVATE lFiltro     := .T.
PRIVATE lCrystal    := .F.
PRIVATE aOrderns    := {}
PRIVATE aReturn     := { "Zebrado", 1,"Administracao", 1, 1, 1, "",1 }
PRIVATE lAbortPrint := .F.
PRIVATE cCabec1     := PadR("Prestador", 26)
PRIVATE cCabec2     := PadR("", 26)
PRIVATE nColuna     := 00
PRIVATE nLi         := 999
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Parametros do relatorio (SX1)...                                         �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
PRIVATE nTipoIm
PRIVATE cCodOpe
PRIVATE cCodCreDe
PRIVATE cCodCreAte
PRIVATE cAno   
PRIVATE cMes    
PRIVATE cQuebLDP
PRIVATE cQuebPEG
PRIVATE cQuebTP
PRIVATE cQuebTipPre
PRIVATE lImprTit := .T.
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Variaveis de mascara (picture)                                           �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
PRIVATE pMoeda      := "@EZ 999999"
PRIVATE aRelat      := {}
PRIVATE aTipPre     := {}
PRIVATE aTotal      := {}
PRIVATE aCabec
PRIVATE aColRel     
PRIVATE aPosCol
PRIVATE aTitulo := {"Cus Ope PF",;
					"Cus Ope PJ",;
					"Pre-Pag PF",;
					"Pre-Pag PJ",;
					"Interc",;
					"Outros",;
					"Glosas"}

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Atualiza SX1                                                             �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
CriaSX1()
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Chama SetPrint                                                           �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
nRel := SetPrint(cAlias,nRel,cPerg,@cTitulo,cDesc1,cDesc2,cDesc3,lDicion,aOrderns,lCompres,cTamanho,{},lFiltro,lCrystal)
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Verifica se foi cancelada a operacao                                     �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
If nLastKey  == 27
   Return
Endif
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Acessa parametros do relatorio...                                        �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Pergunte(cPerg,.F.)

nTipoImp  := mv_par01
cCodOpe   := mv_par02
cCodCreDe := mv_par03
cCodCreAte:= mv_par04
cAno      := mv_par05
cMes      := mv_par06  
cPegDe    := mv_par07
cPegAte   := mv_par08
nFase     := mv_par09
cConfig   := mv_par10
cClaPre   := mv_par11
cTipRda   := mv_par12
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Devera ser criado um arq. texto com os codigos de servicos Tab BBB       �
//� Devera ser criado um arq. texto com a seguinte forma                     �
//� 1a.posicao ---> devera ser digitado F-Fixa ou V-Variaveis                �
//� 2a.posicao ate o sinal de igualdade (=) --->Titulo da Coluna             �
//� apos sinal de igualdade (=) ---> os codigos de servico separados por "/" �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
If !File(cConfig)
   MsgStop("Arquivo de Configuracao do Relatorio nao Encontrado")
   MsgStop(;
			"Devera ser criado um arquivo texto com o seguinte formato:"+CHR(10)+CHR(13)+;
			"1a.posicao ---> devera ser digitado F-Fixa ou V-Variaveis"+CHR(10)+CHR(13)+;
			"2a.posicao ate o sinal de igualdade (=) --->Titulo da Coluna"+CHR(10)+CHR(13)+;
			"apos sinal de igualdade (=) ---> os codigos de servico separados por /")
	Return
Else
	aColRel := R018ColRel()
EndIf
aCabec  := R018aCabec()
aPosCol := R018NumCol()
aTipPre := ACLONE(aPosCol)
aTotal  := ACLONE(aPosCol)
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Configura Limite de linhas                                               �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
If nTipoImp == 1
   nQtdLin := 48
Else
   nQtdLin := 70
Endif
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Configura impressora                                                     �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
SetDefault(aReturn,cAlias)
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Emite relat줿io                                                          �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
MsAguarde({|| R018Imp() }, cTitulo, "", .T.)
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Libera filtro feito no arquivo BD7...                                    �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
BD7->(DbClearFilter())
BD7->(RetIndex("BD7"))
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Fim da rotina                                                            �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Return
/*/
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇
굇旼컴컴컴컴컴쩡컴컴컴컴컫컴컴컴컫컴컴컴컴컴컴컴컴컴컴컴컫컴컴컴쩡컴컴컴컴커굇
굇쿛rograma   � R018Imp  � Autor � Paulo Carnelossi      � Data � 20/01/04 낢�
굇쳐컴컴컴컴컴탠컴컴컴컴컨컴컴컴컨컴컴컴컴컴컴컴컴컴컴컴컨컴컴컴좔컴컴컴컴캑굇
굇쿏escricao  � Imprime o extrato mensal dos servicos prestados...         낢�
굇읕컴컴컴컴컴좔컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸굇
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇
/*/
Static Function R018Imp()
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� IndRegua...                                                              �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
LOCAL   nOrd        := aReturn[8]
LOCAL   cFor
LOCAL   cOrdem
LOCAL   cInd        := CriaTrab(Nil,.F.)
LOCAL   cSQL   
LOCAL   cLastGui    := ""
LOCAL   aSomaGlo    := {}
LOCAL   nVlrItem       
LOCAL   nVlrGloI
LOCAL   nVlrTotGui  := 0
LOCAL   nITens      := 0
LOCAL   aGuiasImp   := {}
Local   aProcImp    := {}
Local   aResumo     := {0,0,0,0,0,0,0}
Local   lImpTotal   := .F., lImpTipPre := .F.
Local   lDetalhe    := .F.
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Controle de Glosas...                                                    �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
PRIVATE aGlosas     := {}
PRIVATE lGlosou     := .F.
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Totalizadores...                                                         �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
PRIVATE nQtd        := 0
PRIVATE nVlrGlo     := 0
PRIVATE nVlrPag     := 0
PRIVATE nVlrTot     := 0
PRIVATE nVlrBIR     := 0
PRIVATE nVlrIRF     := 0
PRIVATE cQuebRDA
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� variaveis de trabalho...                                                 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
PRIVATE cNomUsr     //Nome do usuario que fez o procedimento...                          
PRIVATE cNomCre     //Nome do Credenciado completo...
PRIVATE cMovime     //Codigo do movimento
PRIVATE cMatUsr
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Exibe mensagem...                                                        �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
MsProcTxt(PLSTR0001)
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Monta expressao de filtro para o IndRegua...                             �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
cFor := "BD7_FILIAL = '" + xFilial("BD7") + "' .And. "
If  nFase == 2
    cFor += "BD7_OPELOT = '"+cCodOpe+"' .And. SUBSTRING(BD7_NUMLOT,1,6) = '"+cAno+cMes+"' .And. "
Else
    cFor += "BD7_ANOPAG = '" + cAno + "' .And. " 
    cFor += "BD7_MESPAG = '" + cMes + "' .And. "
Endif
cFor += "( BD7_CODRDA >= '" + cCodCreDe    + "' .And. BD7_CODRDA <= '" + cCodCreAte    + "' ) .And. "
cFor += "( BD7_CODPEG >= '" + cPegDe    + "' .And. BD7_CODPEG <= '" + cPegAte    + "' ) .And. "
cFor += "BD7_BLOPAG <> '1' "
If     nFase == 1
       cFor += " .And. BD7_FASE = '3' .And. BD7_SITUAC = '1'"
ElseIf nFase == 2
	   cFor += " .And. BD7_FASE = '4' .And. BD7_SITUAC = '1'"
EndIf  

If ! Empty(aReturn[7])
   cFor := cFor + " .And. "+AllTrim(aReturn[7])
Endif   
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Monta ordem...                                                           �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
cOrdem := "BD7_FILIAL+BD7_CODRDA+BD7_CODOPE+BD7_CODLDP+BD7_CODPEG+BD7_NUMERO+BD7_ORIMOV+BD7_SEQUEN "

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Monta filtro de acordo com os grupos informados no parame	tro...          �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
BD7->(IndRegua("BD7",cInd,cOrdem,nil,cFor,nil,.T.))
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Monta novo nome do titulo do relatorio mostrando mes/ano                 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
cTitulo := AllTrim(cTitulo)+" - "+PLRETMES(Val(cMes))+"/"+cAno
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Define Ordens Default...                                                 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
BA1->(DbSetOrder(2))
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Leitura de todos os prestadores...                                       �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
cSQL := "SELECT BAU_CODIGO, BAU_TIPPRE FROM "+RetSQLName("BAU")+" WHERE "
cSQL += "BAU_FILIAL = '"+xFilial("BAU")+"' AND "
cSQL += "BAU_CODIGO >= '"+cCodCreDe+"' AND BAU_CODIGO <= '"+cCodCreAte+"' AND "
cSQL += "D_E_L_E_T_ = ''"
cSQL += "ORDER BY BAU_TIPPRE"

PLSQUERY(cSQL,"TrbBAU")

TrbBAU->(dbGoTop())
cQuebTipPre := TrbBAU->BAU_TIPPRE

While ! TrbBAU->(Eof())
      
   If  ! empty(cClaPre) .and. ! TrbBAU->BAU_TIPPRE $ cClaPre
       TrbBAU->(DbSkip())
       Loop
   Endif      

   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
   //� Verifica se a RDA existe para a operadora desejada...                    �
   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
   BAW->(DbSetOrder(1))
   If ! BAW->(DbSeek(xFilial("BAW")+TrbBAU->BAU_CODIGO+cCodOpe))
      TrbBAU->(DbSkip())
      Loop
   Endif      
      
   aTipPre := ACLONE(aPosCol)
   cQuebTipPre := TrbBAU->BAU_TIPPRE
   lImprTit := .T.
   lDetalhe := .F.

   While TrbBAU->(!Eof() .And. BAU_TIPPRE == cQuebTipPre)
      
      cQuebRDA := TrbBAU->BAU_CODIGO
      aResumo     := {0,0,0,0,0,0,0}
      
      lPrtCab := .T.
      //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      //� Posiciona na primeira guia deste prestador, se existir...                �
      //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
      If BD7->(DbSeek(xFilial("BD7")+TrbBAU->BAU_CODIGO))
         lPrtCab  := .F.        
         cQuebLDP := BD7->BD7_CODLDP
         cQuebPEG := BD7->BD7_CODPEG
         cQuebTP  := BD7->BD7_ORIMOV
         //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커                         
         //� Zera conteudo das variaveis...                                     �
         //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
         nVlrTot := 0
         
         //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커                         
         //� Variavel nVlrBIR alimentada pela TABELA BMR					    �
         //� Alexander 31/03/2005			   								    �
         //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
		 //nVlrBIR := 0
         BAU->(DbSetOrder(1))
         BAU->(DbSeek(xFilial("BAU")+cQuebRDA))
       
         //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
         //� Inicia a quebra por prestador...                                   �
         //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
         While ! BD7->(Eof()) .And. BD7->BD7_CODRDA == cQuebRDA
         
               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
               //� Verifica se foi abortada a impressao...                            �
               //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
               If Interrupcao(lAbortPrint)
                  @ ++nLi, nColuna pSay PLSTR0002
                  Exit
               Endif
               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
               //� Verifica quebra...                                                 �
               //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
               If     cQuebLDP <> BD7->BD7_CODLDP
                      cQuebLDP := BD7->BD7_CODLDP
                      cQuebPEG := BD7->BD7_CODPEG
                      cQuebTP  := BD7->BD7_ORIMOV
               ElseIf cQuebTP  <> BD7->BD7_ORIMOV
                      cQuebLDP := BD7->BD7_CODLDP
                      cQuebPEG := BD7->BD7_CODPEG
                      cQuebTP  := BD7->BD7_ORIMOV
               ElseIf cQuebPEG <> BD7->BD7_CODPEG
                      cQuebLDP := BD7->BD7_CODLDP
                      cQuebPEG := BD7->BD7_CODPEG
                      cQuebTP  := BD7->BD7_ORIMOV
               Endif   
               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
               //� Exibe mensagem...                                                  �
               //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
               MsProcTXT("Movimentacao "+BD7->(BD7_ANOPAG+"."+BD7_MESPAG+"-"+BD7_NUMERO)+"  - Pagina "+StrZero(m_pag,4))
               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
               //� Posiciona no item...                                               �
               //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
               BD6->(DbSetOrder(1))
               BD6->(DbSeek(xFilial("BD6")+BD7->(BD7_CODOPE+BD7_CODLDP+BD7_CODPEG+BD7_NUMERO+BD7_ORIMOV+BD7_SEQUEN)))
               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
               //� Controle para valores glosados...                                  �
               //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
               lGlosou := BD6->BD6_VLRGLO > 0
               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
               //� Busca nome do usuario do item posicionado...                       �
               //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
               BA1->(DbSetOrder(2))
               If BA1->(DbSeek(xFilial("BA1")+BD6->(BD6_OPEUSR+BD6_CODEMP+BD6_MATRIC+BD6_TIPREG)))
                  cNomUsr := Subs(BA1->BA1_NOMUSR,1,25)
               Else
                  cNomUsr := Space(25)
               Endif
       
               cMatUsr := IF(BD6->BD6_MATUSA=="1",BA1->(BA1_CODINT+"."+BA1_CODEMP+"."+BA1_MATRIC+"-"+BA1_TIPREG+"-"+BA1_DIGITO),Subs(BA1->BA1_MATANT,1,17)+Space(04))
               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
               //� Monta numero da movimentacao...                                    �
               //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
               cMovime := BD7->BD7_NUMERO+"-"+BD7->BD7_SEQUEN
               cNumGui := BD7->BD7_NUMERO
               cCodPro := BD7->BD7_CODPAD+BD7->BD7_CODPRO
               
               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
               //� Imprime movimentacoes do credenciado...                            �
               //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
               //� Foi documentado pq estava pegando o valor BRUTO E NO 673 esta com o VLRPAG   �
               //� Alexander 31/03/2005															�
               //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
               //nVlrItem := IF(BAU->BAU_PAGPRO$"1, ",BD7->BD7_VLRBPR,0)
              
               nVlrItem  := IF(BAU->BAU_PAGPRO $ "1, ",BD7->BD7_VLRPAG,0)
               nVlrGloIt := IF(BAU->BAU_PAGPRO $ "1, ",BD7->BD7_VLRGLO,0)

               lDetalhe := .T. 
			   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
			   //쿟roquei o if pela estrutura case do 673�
			   //쿌lexander 31/03/2005                   �
			   //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
			   Do Case
			      Case BD7->(BD7_MODCOB <> '1' .AND. BD7_TIPUSR = '1' .AND. BD7_INTERC <> '1')
			           aResumo[1] += nVlrItem   //Custo Operacional PF
			      Case BD7->(BD7_MODCOB <> '1' .AND. BD7_TIPUSR = '2' .AND. BD7_INTERC <> '1')
			           aResumo[2] += nVlrItem   //Custo Operacional PJ
			      Case BD7->(BD7_MODCOB = '1' .AND. BD7_TIPUSR = '1' .AND. BD7_INTERC <> '1')
			           aResumo[3] += nVlrItem   //Pre Pagamento PF
			      Case BD7->(BD7_MODCOB = '1' .AND. BD7_TIPUSR = '2' .AND. BD7_INTERC <> '1')    
			           aResumo[4] += nVlrItem   //Pre Pagamento PJ
			      Case BD7->(BD7_INTERC = '1')
			           aResumo[5] += nVlrItem   //Intercambio
			      OtherWise
			           aResumo[6] += nVlrItem   //Outros
			   EndCase
			               
               If nVlrGloIt > 0
               		aResumo[7] += nVlrGloIt   //Glosas
               EndIf
               
	               If cLastGui <> cMovime+cQuebLDP+cQuebPEG+cQuebTP
	         
	                  If Ascan(aGuiasImp,{|x| x == BD7->(BD7_CODOPE+BD7_CODLDP+BD7_CODPEG+BD7_NUMERO+BD7_ORIMOV) }) == 0
	                     cInicio := cMatUsr+Space(01)+cNomUsr+Space(01)
	                     aadd(aGuiasImp,BD7->(BD7_CODOPE+BD7_CODLDP+BD7_CODPEG+BD7_NUMERO+BD7_ORIMOV))
	                  Else
	                     cInicio := Space(Len(cMatUsr+Space(01)+cNomUsr+Space(01)))
	                  Endif   
	                  cLastGui   := cMovime+cQuebLDP+cQuebPEG+cQuebTP
	                  nVlrTotGui := nVlrItem        
	                  nItens     := 1
	               Else
		              nVlrTotGui += nVlrItem
	                  nItens     ++
	               Endif   

	      	   //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
               //� Soma total do medico...                                            �
               //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
               nVlrPag := nVlrPag + nVlrItem
               nQtd ++
               //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
               //� Controle sobre glosas...                                           �
               //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
               If lGlosou .And. Ascan(aSomaGlo,{|x| x = BD7->(BD7_CODOPE+BD7_CODLDP+BD7_CODPEG+BD7_NUMERO) } ) == 0 .And. BAU->BAU_PAGPRO$"1, "
                  aadd(aSomaGlo,BD7->(BD7_CODOPE+BD7_CODLDP+BD7_CODPEG+BD7_NUMERO))
                  BDX->(DbSetOrder(1))
                  If BDX->(DbSeek(xFilial("BDX")+BD7->(BD7_CODOPE+BD7_CODLDP+BD7_CODPEG+BD7_NUMERO)))
                     While ! BDX->(Eof()) .And. BDX->(BDX_FILIAL+BDX_CODOPE+BDX_CODLDP+BDX_CODPEG+BDX_NUMERO) == ;
                                                 xFilial("BDU")+BD7->(BD7_CODOPE+BD7_CODLDP+BD7_CODPEG+BD7_NUMERO)
                                               
                           If BDX->BDX_TIPREG == "1"
                              nVlrGlo := nVlrGlo + BDX->BDX_VLRGLO
                              aadd(aGlosas,cNomUsr+Space(01)+BDX->(BDX_CODLDP+"."+BDX_CODPEG+"."+BDX_NUMERO)+Space(02)+BDX->BDX_CODGLO+" - "+Subs(BDX->BDX_DESGLO,1,50)+Space(07)+;
                              TransForm(BDX->BDX_VLRGLO,pMoeda))
                           Endif   
                     BDX->(DbSkip())
                     Enddo
                   Endif
               Endif   
	         //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
	         //� Acessa proximo registro...                                               �
	         //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
	         BD7->(DbSkip())
		 Enddo
         //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커                         
         //� Variavel nVlrBIR alimentada pela TABELA BMR					    �
         //� Alexander 31/03/2005			   								    �
         //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
		 //nVlrBIR := nVlrPag - nVlrGlo
		 
		 //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
         //� Imprime totalizadores...                                                 �
         //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
         BAU->(DbSetOrder(1))
         BAU->(DbSeek(xFilial("BAU")+cQuebRDA))
         R018CarregaResumo(aResumo)
      
	     If Len(aRelat) > nQtdLin
	     	lImpTotal := .F.
	     	lImpTipPre := .F.
		    R018ImpFol(lImpTotal, lImpTipPre)
		    aRelat := {}
	     EndIf
	     
      else
		 //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
		 //쿔mplementacao para imprimir DEB/CRED (Fixo e Variado) quando nao existir procedimento�
		 //쿌lexander 30/03/2005                                                                 �
		 //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
         lDetalhe := .T.
         BAU->(DbSetOrder(1))
         BAU->(DbSeek(xFilial("BAU")+cQuebRDA))
         R018CarregaResumo(aResumo)
      
	     If Len(aRelat) > nQtdLin
	     	lImpTotal := .F.
	     	lImpTipPre := .F.
		    R018ImpFol(lImpTotal, lImpTipPre)
		    aRelat := {}
	     EndIf                             
	     
      endif
      TrbBAU->(DbSkip())
      
   Enddo //TrbBAU->(!Eof() .And. BAU_TIPPRE == cQuebTipPre)
   
   If lDetalhe
	  lImpTotal := .F.
	  lImpTipPre := .T.
	  R018ImpFol(lImpTotal, lImpTipPre)
	  aRelat := {}
   EndIf

Enddo

IF Len(aRelat) <= nQtdLin
   lImprTit := .F.
   R018ImpFol(.T., .F., .F.)
EndIf 

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Imprime rodade padrao do produto Microsiga                         �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Roda(0,space(10),cTamanho)

TrbBAU->(DbCloseArea())
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Exibe mensagem...                                                  �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
MsProcTXT("Calculando totais do relatorio...")
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Libera impressao                                                         �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
If  aReturn[5] == 1
    Set Printer To
    Ourspool(nRel)
End
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Fim do Relat줿io                                                         �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Return

Static Function R018ImpFol(lImpTotal, lImpTipPre)
Local nPag, nX, nZ, nTot, cValue, nValAux, cDebAux
Local nTotPre

//aSort(aRelat,,, { |x, y| strzero(x[1],4)+ < y[1] .And. x[2] < y[2] })
For nPag := 1 TO Len(aCabec)
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
	//쿝etirei o parametro que for�a a impressao do cabe�alho e coloquea nlin = 999  �
	//쿌lexander 30/03/2005                                  						 �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
    R018Pag(cTitulo, nPag)
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
	//쿔mprime a Classe															     �
	//쿌lexander 31/03/2005                                  						 �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
    If lImprTit
		@ ++nLi, 000 pSAY "* Classe Rede : "+cQuebTipPre+"-"+Alltrim(Posicione("BAG",1,xFilial("BAG")+cQuebTipPre,"BAG_DESCRI"))+" *"
    EndIf
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
	//쿔mprime o Nome e Valores															     �
	//쿌lexander 31/03/2005                                  						 �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
	For nX := 1 TO Len(aRelat)
    	++nLi
  	    R018Pag(cTitulo, nPag)
        aSort(aRelat[nX][nPag],,, { |x, y| strzero(x[1],4)+strzero(x[2],4) < strzero(y[1],4)+strzero(y[2],4) })
    	For nZ := 1 TO Len(aRelat[nX][nPag])
   			@ nLi, aRelat[nX][nPag][nZ][2] pSAY aRelat[nX][nPag][nZ][3]
    	Next //nZ
	Next //nX
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
	//쿔mprime total da Classe													     �
	//쿌lexander 31/03/2005                                  						 �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
	If lImpTipPre
		@ ++nLi, 000 pSAY REPLICATE("-", 220)
		@ ++nLi, 000 pSAY " * Total Classe Rede * "
		For nTotPre := 1 TO Len(aTipPre[nPag])
			If Len(aTipPre[nPag][nTotPre]) == 3
		    	nValAux := aTipPre[nPag][nTotPre][3]
		 	Else
		 		nValAux := 0
		 	EndIf	 
		    cDebAux := If(nValAux<0,CDEB,"")
			cValue := PadL(Alltrim(Transform(nValAux*If(nValAux<0,-1,1),pMoeda))+cDebAux,NTAMCOL)
   		 	@ nLi, aTipPre[nPag][nTotPre][2] pSAY cValue
	    Next //nTotPre
		@ ++nLi, 000 pSAY REPLICATE("-", 220)
    	R018Pag(cTitulo, nPag)
    EndIf	
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
	//쿔mprime  Total Geral													     	 �
	//쿌lexander 31/03/2005                                  						 �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
	If lImpTotal
		@ ++nLi, 000 pSAY " *** Total Geral *** "
		For nTot := 1 TO Len(aTotal[nPag])
			If Len(aTotal[nPag][nTot]) == 3
		    	nValAux := aTotal[nPag][nTot][3]
		 	Else
		 		nValAux := 0
		 	EndIf	 
		    cDebAux := If(nValAux<0,CDEB,"")
			cValue := PadL(Alltrim(Transform(nValAux*If(nValAux<0,-1,1),pMoeda))+cDebAux,NTAMCOL)
   		 	@ nLi, aTotal[nPag][nTot][2] pSAY cValue
	    Next //nTot
    	@ ++nLi, 000 pSAY REPLICATE("-", 220)
		R018Pag(cTitulo, nPag)
    EndIf	
    
Next //nPag	

If lImprTit
	lImprTit := .F.
EndIf

Return

/*/
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇
굇旼컴컴컴컴컴쩡컴컴컴컴컫컴컴컴컫컴컴컴컴컴컴컴컴컴컴컴컫컴컴컴쩡컴컴컴컴커굇
굇쿛rograma   � R018Pag  � Autor � Paulo Carnelossi      � Data � 21/01/04 낢�
굇쳐컴컴컴컴컴탠컴컴컴컴컨컴컴컴컨컴컴컴컴컴컴컴컴컴컴컴컨컴컴컴좔컴컴컴컴캑굇
굇쿏escricao  � Avanca Pagina caso necessario...                           낢�
굇읕컴컴컴컴컴좔컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸굇
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇
/*/
Static Function R018Pag(cTitulo, nFolha)
//DEFAULT lForce := .F.                             

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//쿑oi retirado este parametro para que so imprima o cabe�alho quando nlin > que nqtdlin�
//쿌lexander 30/03/2005                                                                 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
If nLi > nQtdLin //.Or. lForce
   nLi := 01
   nLi := Cabec(cTitulo,aCabec[nFolha][1],aCabec[nFolha][2],cNomeProg,cTamanho,nCaracter)
   ++nLi
Endif

Return

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴袴袴藁袴袴袴佶袴袴袴袴敲굇
굇튡rograma  쿝018CarregaResumo튍utor 쿛aulo Carnelossi � Data � 20/01/04 볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴袴袴姦袴袴賈袴袴袴袴袴묽�
굇튒esc.     쿔mprime o resumo do extrado do RDA                          볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function R018CarregaResumo(aResumo)
Local cCodCol, nPos := 0, cDebAux
Local nValAux, aColAux, nPosRel
Local nVlrPag := 0,nVlrIRF := 0 
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커                         
//� Variavel nVlrBIR alimentada pela TABELA BMR					       �
//� Alexander 31/03/2005			   								   �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
//nVlrBir := 0, 

Local nX := 0, nTotal := 0
Local aVariavel := {}, nProduc := 0
Local lCposVld	:= (BBC->(FieldPos("BBC_VLDINI")) > 0 .And. BBC->(FieldPos("BBC_VLDFIM")) > 0)
					
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Posiciona no credenciado informado no parametro...                       �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
BAU->(DbSetOrder(1))

If BAU->(DbSeek(xFilial("BAU")+cQuebRDA))
   cNomCre := BAU->(BAU_CODIGO+"-"+BAU_NOME)
Else
   cNomCre := BAU->(Space(Len(BAU_CODIGO))+"-"+Space(Len(BAU_NOME)))
Endif              
cNomCre := PadR(cNomCre, 25)

aAdd(aRelat, ARRAY(Len(aCabec)))
nPosRel := Len(aRelat)

//@ nLi, nColuna pSay cNomCre
For nX := 1 TO Len(aRelat[nPosRel])
	aRelat[nPosRel][nX] := {}
	aAdd(aRelat[nPosRel][nX], {nX, 000, cNomCre})
Next

nTotal := 0
For nX := 1 TO Len(aResumo)

    aColAux := R018PosCol("TT"+StrZero(nX, 2))
    If nX < 7
    	//@ nLi, aColAux[2] PSay PadL(Transform(aResumo[nX],pMoeda),NTAMCOL)
    	aAdd(aRelat[nPosRel][aColAux[1]], {aColAux[1], aColAux[2], PadL(Alltrim(Transform(aResumo[nX],pMoeda)),NTAMCOL)})
	    nTotal += aResumo[nX]
	ElseIf nX == 7 //Glosas
	    //@ nLi, aColAux[2] PSay PadL(Alltrim(Transform(aResumo[nX],pMoeda))+If(aResumo[nX]>0,"(D)",""),NTAMCOL)
	    aAdd(aRelat[nPosRel][aColAux[1]], {aColAux[1], aColAux[2], PadL(Alltrim(Transform(aResumo[nX],pMoeda))+If(aResumo[nX]>0,CDEB,""),NTAMCOL)})
		nTotal -= aResumo[nX]
	EndIf
	R018AcumTotal(aColAux, aResumo[nX])
	R018AcumTipPre(aColAux, aResumo[nX])	
Next

aColAux := R018PosCol("TTZZ")
//@ nLi, aColAux[2] PSay PadL(Transform(nTotal,pMoeda),NTAMCOL)
aAdd(aRelat[nPosRel][aColAux[1]], {aColAux[1], aColAux[2], PadL(Alltrim(Transform(nTotal,pMoeda)),NTAMCOL)})
R018AcumTotal(aColAux, nTotal)
R018AcumTipPre(aColAux, nTotal)	

nVlrPag := nTotal
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커                         
//� Variavel nVlrBIR alimentada pela TABELA BMR					       �
//� Alexander 31/03/2005			   								   �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
//nVlrBIR := nVlrPag

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Lista debitos/creditos fixos... Faturada                                 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
if  nFase == 2 
    BGQ->(DbSetOrder(2))
    if  BGQ->(DbSeek(xFilial("BGQ")+BAU->BAU_CODIGO+cAno+cMes+cCodOpe)) .and. ( Alltrim(BGQ->BGQ_NUMLOT) <> '' )            
        while   ( !BGQ->(eof()) ) .And.;
   	    	    ( BGQ->(BGQ_FILIAL+BGQ_CODIGO+BGQ_ANO+BGQ_MES+BGQ_CODOPE) == xFilial("BGQ")+cQuebRDA+cAno+cMes+cCodOpe )
   	    	    
   
           if  alltrim(BGQ->BGQ_NUMLAU) <> "BBC"
               BGQ->(DbSkip())
               loop
           endif 
           	              
	   	   BBB->(DbSetOrder(1))
	       BBB->(DbSeek(xFilial("BBB")+BGQ->BGQ_CODLAN))

		   cCodCol := R018CodCol("V",BGQ->BGQ_CODLAN)
           if (nPos := Ascan(aVariavel, {|aVal|aVal[1] == cCodCol})) == 0
	           aAdd(aVariavel, {cCodCol, 0, 0})
	           nPos := len(aVariavel)
	       endif
	        
	       if BGQ->BGQ_TIPO == "1"
              nVlrPag -= BGQ->BGQ_VALOR
              aVariavel[nPos][DEBITO] += BGQ->BGQ_VALOR
	       else
	          if BGQ->BGQ_TIPO == "2"
                 nVlrPag += BGQ->BGQ_VALOR
                 aVariavel[nPos][CREDITO] += BGQ->BGQ_VALOR
              endif
           endif                                 
           BGQ->(DbSkip())
        enddo
    endif    
else
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
	//쿌ntes da alteracao esta rotina estava fora do if nao tratando nfase.�
	//쿻fase = 2 (faturada) nfase = 1 pronta. estava diferente do 673      �
	//쿌lexander 31/03/2005                                                �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
	BBC->(DbSetOrder(2))
	If BBC->(DbSeek(xFilial("BBC")+cQuebRDA+"11"))
		While ! BBC->(Eof()) .And. ;
	       		BBC->(BBC_FILIAL+BBC_CODIGO+BBC_STATUS+BBC_PROMED) == ;
	       		xFilial("BBC")+cQuebRDA+"11"
	            
				// BOPS 98697
	     		If lCposVld
		    		If ! ( cAno + cMes + "01" >= DtoS(BBC->BBC_VLDINI) .And. ;
		          		(DtoS(LastDay(CtoD("01/"+cMes+"/"+cAno))) <= DtoS(BBC->BBC_VLDFIM) .Or. Empty(BBC->BBC_VLDFIM)) )
		        		BBC->(DbSkip())
		        		Loop
		    		EndIf
		 		EndIf
		 		
	       		BBB->(DbSetOrder(1))
	            BBB->(DbSeek(xFilial("BBB")+BBC->BBC_CODSER))
	         		
	            cCodCol := R018CodCol("F",BBC->BBC_CODSER)
	            
	            If (nPos := Ascan(aVariavel, {|aVal|aVal[1] == cCodCol})) == 0
		            aAdd(aVariavel, {cCodCol, 0, 0})
		            nPos := Len(aVariavel)
		        EndIf

	            If BBB->BBB_TIPSER == "1"
	               nVlrPag -= BBC->BBC_VALOR
	               aVariavel[nPos][DEBITO] += BBC->BBC_VALOR
	            Else
	               If BBB->BBB_TIPSER == "2"
	                  nVlrPag += BBC->BBC_VALOR                  
	                  aVariavel[nPos][CREDITO] += BBC->BBC_VALOR
	               Endif
	            Endif                                 
				//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커                         
				//� Variavel nVlrBIR alimentada pela TABELA BMR					       �
				//� Alexander 31/03/2005			   								   �
				//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
	            //If BBB->BBB_INCIR == "1"
	            //   If BBB->BBB_TIPSER == "1"
	            //      nVlrBIR -= BBC->BBC_VALOR
	            //   Else
	            //	  nVlrBIR += BBC->BBC_VALOR                  
	            //   Endif                                 
	            //Endif   
	                                             
	         	BBC->(DbSkip())
	 	Enddo
	Endif   
Endif
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Lista debitos/creditos variaveis                                         �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
BGQ->(DbSetOrder(2))                                                                        
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Inicio Somente para entrar no if quanto nfase = 1 (pronta)				 �
//� Alexander  31/03/2005													 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
cNumLot := Alltrim(BGQ->BGQ_NUMLOT)
if nFase <> 2
   cNumLot := 'XX'
endif             
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Fim																     	 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
If BGQ->(DbSeek(xFilial("BGQ")+cQuebRDA+cAno+cMes+cCodOpe)) .and. ( cNumLot <> '' )            
   While ! BGQ->(Eof()) .And. ;
     		BGQ->(BGQ_FILIAL+BGQ_CODIGO+BGQ_ANO+BGQ_MES+BGQ_CODOPE) == ;
       		xFilial("BGQ")+cQuebRDA+cAno+cMes+cCodOpe
			//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
			//쿐stava faltando esta verificacao foi colocado igual ao PLSR673�
			//쿌lexander 30/03/2005                                          �
			//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
	        if  alltrim(BGQ->BGQ_NUMLAU) == "BBC"
	            BGQ->(DbSkip())
	            loop
	        endif	              
            BBB->(DbSetOrder(1))
            BBB->(DbSeek(xFilial("BBB")+BGQ->BGQ_CODLAN))

            cCodCol := R018CodCol("V",BGQ->BGQ_CODLAN)
            
            If (nPos := Ascan(aVariavel, {|aVal|aVal[1] == cCodCol})) == 0
	            aAdd(aVariavel, {cCodCol, 0, 0})
	            nPos := Len(aVariavel)
	        EndIf
			//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
			//쿑oi feita a documentacao pq estava pegando o valor do BBC (Fixo) e nao do BGQ (Variavel)  �
			//퀃anto no if como no elseif																 �
			//쿌lexander 31/03/2005                                                            			 �
			//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
            If BGQ->BGQ_TIPO == "1"
               nVlrPag -= BGQ->BGQ_VALOR
               aVariavel[nPos][DEBITO] += BGQ->BGQ_VALOR
			   //aVariavel[nPos][DEBITO] += BBC->BBC_VALOR
            Else
               If BGQ->BGQ_TIPO == "2"
                  nVlrPag += BGQ->BGQ_VALOR
                  aVariavel[nPos][CREDITO] += BGQ->BGQ_VALOR
                  //aVariavel[nPos][CREDITO] += BBC->BBC_VALOR
               Endif
            Endif                                 
            //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커                         
			//� Variavel nVlrBIR alimentada pela TABELA BMR					       �
			//� Alexander 31/03/2005			   								   �
			//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
            //If BGQ->BGQ_INCIR == "1"
            //   If BGQ->BGQ_TIPO == "1"
            //      nVlrBIR -= BGQ->BGQ_VALOR
            //   Else
            //      nVlrBIR += BGQ->BGQ_VALOR                  
            //   Endif                                 
            //Endif   

            BGQ->(DbSkip())
      Enddo
Endif     
For nX := 1 TO Len(aVariavel)
    nValAux := aVariavel[nX][CREDITO]-aVariavel[nX][DEBITO]
	cDebAux := If(nValAux<0,CDEB,"")
	aColAux := R018PosCol(aVariavel[nX][1])
	If aColAux[1] <> NIL .And. aColAux[2] <> NIL
		//@ nLi, aColAux[2] PSay PadL(Alltrim(Transform(nValAux*If(nValAux<0,-1,1),pMoeda))+cDebAux,NTAMCOL)
		aAdd(aRelat[nPosRel][aColAux[1]], {aColAux[1], aColAux[2], PadL(Alltrim(Transform(nValAux*If(nValAux<0,-1,1),pMoeda))+cDebAux,NTAMCOL)})
		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
		//쿞omente quando for diferente de NIL.. tava dando erro no calculo AcumTotal.�
		//쿌lexander 30/03/2005                                                       �
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
		R018AcumTotal(aColAux, nValAux)
	    R018AcumTipPre(aColAux, nValAux)	
    EndIf
Next                        

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Lista Apontamento Producao                                               �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
BCE->(DbSetOrder(2))
If BCE->(DbSeek(xFilial("BGQ")+cQuebRDA+cCodOpe+cAno+cMes))

   While ! BCE->(Eof()) .And. BCE->(BCE_FILIAL+BCE_CODIGO+BCE_CODINT+BCE_ANOPAG+BCE_MESPAG) == ;
                               xFilial("BCE")+cQuebRDA+cCodOpe+cAno+cMes

          nVlrPag += BCE->BCE_VLRAPT
          nProduc += BCE->BCE_VLRAPT

		  //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커                         
		  //� Variavel nVlrBIR alimentada pela TABELA BMR					     �
		  //� Alexander 31/03/2005			   								     �
		  //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
          //If BCE->BCE_INCIR == "1"
          //   nVlrBIR += BCE->BCE_VLRAPT	
          //Endif   

          BCE->(DbSkip())
	Enddo
Endif    
aColAux := R018PosCol("APRD")
//@ nLi, aColAux[2] PSay PadL(Transform(nProduc,pMoeda),NTAMCOL)
aAdd(aRelat[nPosRel][aColAux[1]], {aColAux[1], aColAux[2], PadL(AllTrim(Transform(nProduc,pMoeda)),NTAMCOL)})
R018AcumTotal(aColAux, nProduc)
R018AcumTipPre(aColAux, nProduc)	
nVlrTot := nVlrPag  //pois aqui ja esta subtraido o valor de glosas

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//쿑oi documentada pois o valor o IR vai vir da TABELA BMR			 �
//쿌lexander 30/03/2005                                   			 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
//If nVlrTot > 0
//   nVlrIRF := PLIRCRE(BAU->BAU_CODSA2,BAU->BAU_LOJSA2,nVlrBIR, .F.)
//Else
//   nVlrIRF := 0
//Endif                                 
//nVlrTot := nVlrPag - nVlrIRF  // glosa ja subtraida


//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//쿔mplementa豫o para fazer calculo de PIS, Cofins, CSLL, IR e INSS PF, INSSPJ�
//쿌lexander 30/03/2005                                                       �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
nIss     := 0
nPis  	 := 0
nCofins  := 0
nVlrBIR  := 0
nVlrIRF	 := 0
nCSLL 	 := 0
nINSSPF  := 0
nINSSPJ  := 0
nINSSJF  := 0
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//쿞omente Faturadas														  �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
if  nFase == 2 
    //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	//쿍uscar imposto PIS, Cofins, CSLL, IR e INSS PF, INSSPJ					  �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
    BMR->(DbSetOrder(3))
    if  BMR->(DbSeek(xFilial("BMR")+cCodOpe+cQuebRDA+cCodOpe+cAno+cMes))
	    //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
		//쿐nquanto nao for fim do arquivo ou mudar de RDA						      �
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
        while !BMR->(Eof()) .and.; 
               BMR->(BMR_FILIAL+BMR_OPERDA+BMR_CODRDA+BMR_OPELOTE+BMR_ANOLOT+BMR_MESLOT) == xFilial("BMR")+cCodOpe+BAU->BAU_CODIGO+cCodOpe+cAno+cMes 
	      //旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
		  //쿎heca o codigo do LANCAMENTO EM BMR - Composicao de Pagamento				�
		  //읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
          do case
             case BMR->BMR_CODLAN == "185"
                  nIss     += BMR->BMR_VLRPAG
             case BMR->BMR_CODLAN == "187"
                  nPis     += BMR->BMR_VLRPAG
             case BMR->BMR_CODLAN == "189"
                  nCofins  += BMR->BMR_VLRPAG
             case BMR->BMR_CODLAN == "191"
                  nCSLL    += BMR->BMR_VLRPAG
             case BMR->BMR_CODLAN == "193"
                  nINSSPF  += BMR->BMR_VLRPAG
             case BMR->BMR_CODLAN == "195"
                  nINSSPJ  += BMR->BMR_VLRPAG
             case BMR->BMR_CODLAN == "197"
                  nINSSPF  += BMR->BMR_VLRPAG
             case BMR->BMR_CODLAN == "199"
                  nVlrIRF  += BMR->BMR_VLRPAG
          endcase
          BMR->(DbSkip())
        enddo      
    endif
endif                                                       
nVlrTot := nVlrPag - nIss - nPis - nCofins - nCSLL - nINSSPF - nINSSPJ - nINSSJF - nVlrIRF
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//쿑im da Implementacao														  �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
If nVlrIRF > 0
    aColAux := R018PosCol("VIRF")
	//@ nLi, aColAux[2] PSay PadL(Transform(nVlrIRF,pMoeda),NTAMCOL)
	aAdd(aRelat[nPosRel][aColAux[1]], {aColAux[1], aColAux[2], PadL(AllTrim(Transform(nVlrIRF,pMoeda)),NTAMCOL)})
	R018AcumTotal(aColAux, nVlrIRF)
	R018AcumTipPre(aColAux, nVlrIRF)	
Endif     

aColAux := R018PosCol("VLIQ")
If nVlrTot > 0
	//@ nLi, aColAux[2] PSay PadL(Transform(nVlrTot,pMoeda),NTAMCOL)
	aAdd(aRelat[nPosRel][aColAux[1]], {aColAux[1], aColAux[2], PadL(AllTrim(Transform(nVlrTot,pMoeda)),NTAMCOL)})
Else
	//@ nLi, aColAux[2] PSay PadL(Transform(nVlrTot*-1,pMoeda),NTAMCOL-3)+"(D)"      
	aAdd(aRelat[nPosRel][aColAux[1]], {aColAux[1], aColAux[2], PadL(Alltrim(Transform(nVlrTot*-1,pMoeda))+CDEB,NTAMCOL)})
Endif   
R018AcumTotal(aColAux, nVlrTot)
R018AcumTipPre(aColAux, nVlrTot)	

//nLi ++

Return
/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴袴袴藁袴袴袴佶袴袴袴袴敲굇
굇튡rograma  쿝018AcumTotal    튍utor 쿛aulo Carnelossi � Data � 20/01/04 볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴袴袴姦袴袴賈袴袴袴袴袴묽�
굇튒esc.     쿌cumula total geral array atotal                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function R018AcumTotal(aColAux, nValor)

If Len(aTotal[aColAux[1]][aColAux[3]]) == 3
	aTotal[aColAux[1]][aColAux[3]][3] += nValor
Else
	If Len(aTotal[aColAux[1]][aColAux[3]]) == 2
	    ASIZE(aTotal[aColAux[1]][aColAux[3]], 3)
		aTotal[aColAux[1]][aColAux[3]][3] := nValor
	EndIf
EndIf
	
return

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴袴袴藁袴袴袴佶袴袴袴袴敲굇
굇튡rograma  쿝018AcumTipPre   튍utor 쿛aulo Carnelossi � Data � 17/02/04 볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴袴袴姦袴袴賈袴袴袴袴袴묽�
굇튒esc.     쿌cumula total por classe rede array aTipPre                 볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function R018AcumTipPre(aColAux, nValor)

If Len(aTipPre[aColAux[1]][aColAux[3]]) == 3
	aTipPre[aColAux[1]][aColAux[3]][3] += nValor
Else
	If Len(aTipPre[aColAux[1]][aColAux[3]]) == 2
	    ASIZE(aTipPre[aColAux[1]][aColAux[3]], 3)
		aTipPre[aColAux[1]][aColAux[3]][3] := nValor
	EndIf
EndIf
	
return

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴袴袴藁袴袴袴佶袴袴袴袴敲굇
굇튡rograma  쿝018ColRel       튍utor 쿛aulo Carnelossi � Data � 20/01/04 볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴袴袴姦袴袴賈袴袴袴袴袴묽�
굇튒esc.     쿗er arquivo configuracao e retorna array c/ colunas Rel.    볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function R018ColRel()

Local nX, cFileCfg
Local nTamLin := 80, nCtdLin 
Local aColunas := {}, cStrAux, nPosIgual
Local cCodSer, cTitSer, cCjCods

cFileCfg := MemoRead(cConfig)
nCtdLin  := MLCount(cFileCFG, nTamLin)

For nX := 1 TO nCtdLin
    cStrAux := MemoLine(cFileCFG, nTamLin, nX)
    If Left(cStrAux, 1) $ "FV" .And. (nPosIgual := AT("=",cStrAux)) > 0
	    cCodSer := Left(cStrAux, 1) + StrZero(nX,3)
	    cTitSer := Subs(cStrAux, 2, nPosIgual-2)
	    cCjCods := Alltrim(Subs(cStrAux, nPosIgual+1))
        aAdd(aColunas, { cCodSer, cTitSer, cCjCods} )
     EndIf    
Next   

Return(aColunas)

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴袴袴藁袴袴袴佶袴袴袴袴敲굇
굇튡rograma  쿝018CodCol       튍utor 쿛aulo Carnelossi � Data � 20/01/04 볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴袴袴姦袴袴賈袴袴袴袴袴묽�
굇튒esc.     쿝etorna codigos consolidado que aglutina cod.servico debitos볍�
굇�          쿽u creditos tanto fixo como variavel                        볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function R018CodCol(cVarFix,cCodSer)
Local nX, cCodigo

For nX := 1 TO Len(aColRel)
    If Left(aColRel[nX][1],1) == cVarFix .And. ;
       cCodSer $ aColRel[nX][3]
       cCodigo := aColRel[nX][1]
    EndIf
Next

If cCodigo == NIL
   cCodigo := cVarFix + "XYZ"
EndIf

Return(cCodigo)

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴袴袴藁袴袴袴佶袴袴袴袴敲굇
굇튡rograma  쿝018TitCol       튍utor 쿛aulo Carnelossi � Data � 20/01/04 볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴袴袴姦袴袴賈袴袴袴袴袴묽�
굇튒esc.     쿝etorna titulos da coluna deb/cred fixo e variaveis         볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�

Static Function R018TitCol(cCodigo)
Local nX, cTitle

If Right(cCodigo, 3) == "XYZ"
   cTitle := "Outros"
Else
	For nX := 1 TO Len(aColRel)
	    If aColRel[nX][1] == cCodigo
	       cTitle  := aColRel[nX][2]
	    EndIf
	Next
EndIf

Return(cTitle)

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴袴袴藁袴袴袴佶袴袴袴袴敲굇
굇튡rograma  쿝018aCabec       튍utor 쿛aulo Carnelossi � Data � 20/01/04 볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴袴袴姦袴袴賈袴袴袴袴袴묽�
굇튒esc.     쿝etorna array com cabecalho de cada folha do relatorio      볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function R018aCabec()
Local aAuxCab, nColRel, nColFol,nCtd
Local cLinCabec1, cLinCabec2, cTitle, aAuxText
Local nFolhas, nX, nZ, nQuebra

aAuxCab := R018Cabec()
nColRel := Len(aAuxCab)
nColFol := INT((nLimite-26)/(NTAMCOL+1))

nFolhas := nColRel/nColFol
          
If nFolhas > INT(nFolhas)
	nFolhas := INT(nFolhas)
	nFolhas++
EndIf

nZ := 1
nQuebra := 0
aCabec := Array(nFolhas)

For nX := 1 TO nFolhas
	cLinCabec1 := cCabec1
	cLinCabec2 := cCabec2
	
	For nCtd := nZ TO nColRel
	    nQuebra++
		cTitle := aAuxCab[nCtd]
		
       	If Len(cTitle) <= NTAMCOL
			cLinCabec1 += PadL(cTitle, NTAMCOL)
			cLinCabec2 += Space(NTAMCOL)
		Else
			aAuxText := R018QbTexto(cTitle, NTAMCOL, Space(1))
			cLinCabec1 += PadL(aAuxText[1], NTAMCOL)
			If Len(aAuxText) > 1
				cLinCabec2 += PadL(aAuxText[2], NTAMCOL)
			EndIf	
		EndIf
		cLinCabec1 += Space(1)
		cLinCabec2 += Space(1)
	    nZ++
			
		If nQuebra == nColFol
			aCabec[nX] := {cLinCabec1, cLinCabec2}
			nQuebra := 0
			Exit
		EndIf
	Next
	If nQuebra > 0 .And. nQuebra < nColFol 
		aCabec[nX] := {cLinCabec1, cLinCabec2}
    EndIf
Next	

Return(aCabec)

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴袴袴藁袴袴袴佶袴袴袴袴敲굇
굇튡rograma  쿝018NumCol       튍utor 쿛aulo Carnelossi � Data � 20/01/04 볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴袴袴姦袴袴賈袴袴袴袴袴묽�
굇튒esc.     쿝etorna array com codigo da coluna e posicao da coluna que  볍�
굇�          쿭evera ser impresso                                         볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function R018NumCol()
Local aAuxCol, nColRel, nColFol, nColAux, aNumCol
Local nFolhas, nX, nZ, nQuebra,nCtd

aAuxCol := R018AuxCol()
nColRel := Len(aAuxCol)
nColFol := INT((nLimite-26)/(NTAMCOL+1))

nFolhas := nColRel/nColFol
          
If nFolhas > INT(nFolhas)
	nFolhas := INT(nFolhas)
	nFolhas++
EndIf

nZ := 1
nQuebra := 0
aNumCol := Array(nFolhas)

For nX := 1 TO nFolhas
    nColAux := 26
    aNumCol[nX] := {}

	For nCtd := nZ TO nColRel
	
	    nQuebra++
	    aAdd(aNumCol[nX], {aAuxCol[nCtd], nColAux})
	    nColAux += (NTAMCOL+1)
	    nZ++
			
		If nQuebra == nColFol
			nQuebra := 0
			Exit
		EndIf
	Next

Next	

Return(aNumCol)

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴袴袴藁袴袴袴佶袴袴袴袴敲굇
굇튡rograma  쿝018AuxCol       튍utor 쿛aulo Carnelossi � Data � 20/01/04 볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴袴袴姦袴袴賈袴袴袴袴袴묽�
굇튒esc.     쿝etorna array com codigos da colunas a ser impresso no rel. 볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function R018AuxCol()
Local nX, aColAux := {}

For nX := 1 TO Len(aTitulo)
    aAdd(aColAux, "TT"+StrZero(nX,2))
Next
aAdd(aColAux, "TTZZ")

For nX := 1 TO Len(aColRel)
	If nX > 1 .And. Left(aColRel[nX][1],1) == "V" .And. Left(aColRel[nX-1][1],1) == "F"
		aAdd(aColAux, "FXYZ") // p/ deb.cred. fixa
	EndIf	
	aAdd(aColAux, aColRel[nX][1])
Next
aAdd(aColAux, "VXYZ")  // p/ deb.cred. variaveis
aAdd(aColAux, "APRD")  
aAdd(aColAux, "VIRF") 
aAdd(aColAux, "VLIQ") 

Return(aColAux)

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴袴袴藁袴袴袴佶袴袴袴袴敲굇
굇튡rograma  쿝018PosCol       튍utor 쿛aulo Carnelossi � Data � 20/01/04 볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴袴袴姦袴袴賈袴袴袴袴袴묽�
굇튒esc.     쿝etorna array com:                                          볍�
굇�          �{numero da folha, posicao coluna, Elem.array atotal (acum)} 볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function R018PosCol(cCodSer)
Local nX, nFolha, nPosCol, nPosTot,nZ

For nX := 1 TO Len(aPosCol)
    For nZ := 1 TO Len(aPosCol[nX])
        If aPosCol[nX][nZ][1] == cCodSer
        	nFolha := nX
        	nPosCol := aPosCol[nX][nZ][2]
        	nPosTot := nZ 
        EndIf
    Next
Next

Return({nFolha, nPosCol, nPosTot})

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴袴袴藁袴袴袴佶袴袴袴袴敲굇
굇튡rograma  쿝018Cabec        튍utor 쿛aulo Carnelossi � Data � 20/01/04 볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴袴袴姦袴袴賈袴袴袴袴袴묽�
굇튒esc.     쿝etorna array com titulos das colunas a ser impresso no rel.볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function R018Cabec()
Local nX, aCabCol := {}

For nX := 1 TO Len(aTitulo)
	aAdd(aCabCol, aTitulo[nX])
Next
aAdd(aCabCol, "Total      Bruto")

For nX := 1 TO Len(aColRel)
	If nX > 1 .And. Left(aColRel[nX][1],1) == "V" .And. Left(aColRel[nX-1][1],1) == "F"
		aAdd(aCabCol, "Outros (F)") // p/ deb.cred. fixa
	EndIf	
	aAdd(aCabCol, aColRel[nX][2])
Next
aAdd(aCabCol, "Outros (V)")  // p/ deb.cred. variaveis

aAdd(aCabCol, "Apont Prod")
aAdd(aCabCol, "IRF")
aAdd(aCabCol, "Total     Liquido")

Return(aCabCol)

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴袴藁袴袴袴佶袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿝018QbTexto 튍utor 쿛aulo Carnelossi   � Data �  02/09/03   볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴姦袴袴賈袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     쿜uebra o Texto de acordo com tamnho e separador informado   볍�
굇�          쿭evolvendo um array com a string quebrada                   볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                        볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function R018QbTexto(cTexto, nTamanho, cSeparador)
Local aString := {}, nTamAux := nTamanho
Local nPos, nCtd, nTamOri := Len(cTexto), cAuxTexto

If Len(Trim(cTexto)) > 0

   If Len(Trim(cTexto)) <= nTamanho

      If Len(Trim(cTexto)) > 0
	      aAdd(aString, AllTrim(cTexto) )
      EndIf

   Else
	
		If (nPos := At(cSeparador, cTexto)) != 0
		   
			For nCtd := 1 TO nTamOri STEP nTamAux
		
				cAuxTexto := Subs(cTexto, nCtd, nTamanho)
			
			   If nCtd+nTamanho < nTamOri
				   While Len(Subs(cAuxTexto, Len(cAuxTexto), 1)) > 0 .And. ;
				   				Subs(cAuxTexto, Len(cAuxTexto), 1) <> cSeparador
				   
			   		cAuxTexto := Subs(cAuxTexto, 1, Len(cAuxTexto)-1)
			   		
			      End
			   EndIf
			      
		      If Len(cAuxTexto) > 0
			      cAuxTexto 	:= Subs(cTexto, nCtd, Len(cAuxTexto))
			      nTamAux 		:= Len(cAuxTexto)
		      Else
		      	cAuxTexto := Subs(cTexto, nCtd, nTamanho)
			      nTamAux 		:= nTamanho
		      EndIf
		
		      If Len(Trim(cAuxTexto)) > 0
			      aAdd(aString, Alltrim(cAuxTexto))
		      EndIf
		   Next
		
		Else
		
			For nCtd := 1 TO nTamOri STEP nTamanho
			   If Len(Subs(cTexto, nCtd, nTamanho)) > 0 
			      If Len(Trim(Subs(cTexto, nCtd, nTamanho))) > 0
						aAdd(aString, AllTrim(Subs(cTexto, nCtd, nTamanho)))
					EndIf	
				EndIf	
			Next
		
		EndIf
		
	EndIf
Else
	aAdd(aString, Space(nTamanho))	
EndIf

Return aString

/*/
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇旼컴컴컴컴컫컴컴컴컴컴쩡컴컴컴쩡컴컴컴컴컴컴컴컴컴컴컴쩡컴컴컫컴컴컴컴컴엽�
굇쿑uncao    � CriaSX1  � Autor � Angelo Sperandio      � Data � 05/02/05 낢�
굇쳐컴컴컴컴컵컴컴컴컴컴좔컴컴컴좔컴컴컴컴컴컴컴컴컴컴컴좔컴컴컨컴컴컴컴컴눙�
굇쿏escricao � Atualiza perguntas                                         낢�
굇쳐컴컴컴컴컵컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴눙�
굇쿞intaxe   � CriaSX1()                                                  낢�
굇읕컴컴컴컴컨컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴袂�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
/*/

Static Function CriaSX1()

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Iniciliza variaveis                                                      �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Local aRegs	:=	{}
aadd(aRegs,{cPerg,"09","Fase       ","","","mv_ch9","N", 1,0,0,"C","","mv_par09","Pronta"   ,"","","","","Faturada"   ,"","","","",""           ,"","","","",""     ,"","","","","","","","","",""})
aadd(aRegs,{cPerg,"10","Arquivo Cfg","","","mv_cha","C",30,0,0,"G","","mv_par10",""         ,"","","","",""           ,"","","","",""           ,"","","","",""     ,"","","","","","","","","",""})
aadd(aRegs,{cPerg,"11","Classes RDA","","","mv_chb","C",30,0,0,"G","","mv_par11",""         ,"","","","",""           ,"","","","",""           ,"","","","",""     ,"","","","","","","","","",""})
//aadd(aRegs,{cPerg,"12","Tipo RDA"   ,"","","mv_chc","N", 1,0,0,"C","","mv_par12","Cooperado","","","","","Credenciado","","","","","Funcionario","","","","","Todos","","","","","","","","","",""})
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Atualiza SX1                                                             �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
PlsVldPerg(aRegs)
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Fim da funcao                                                            �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Return

