
#include "PLSR103.CH"
#IFDEF TOP
	#INCLUDE "TOPCONN.CH"
#ENDIF

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    � PLSR103 � Autor � Natie Sugahara         � Data � 25/06/03 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Pacotes   X R.D.A.                                         ����
�������������������������������������������������������������������������Ĵ���
���Sintaxe   � PLSR103()                                                  ����
�������������������������������������������������������������������������Ĵ���
��� Uso      � Advanced Protheus                                          ����
�������������������������������������������������������������������������Ĵ���
��� Alteracoes desde sua construcao inicial                               ����
�������������������������������������������������������������������������Ĵ���
��� Data     � BOPS � Programador � Breve Descricao                       ����
�������������������������������������������������������������������������Ĵ���
���          |      |             |                                       ����
��������������������������������������������������������������������������ٱ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/                                
Function PLSR103()
/*��������������������������������������������������������������������������Ŀ
  � Define variaveis padroes para todos os relatorios...                     �
  ����������������������������������������������������������������������������*/
PRIVATE wnRel         
PRIVATE cNomeProg   := "PLSR103"
PRIVATE nLimite     := 132
PRIVATE nTamanho    := "M"
PRIVATE Titulo		:= oEmToAnsi(STR0001)
PRIVATE cDesc1      := oEmToAnsi(STR0001)
PRIVATE cDesc2      := ""
PRIVATE cDesc3      := ""
PRIVATE cAlias      := "BLZ"
PRIVATE cPerg       := "PLS103"
PRIVATE Li         	:= 0
PRIVATE m_pag       := 1
PRIVATE lCompres    := .F.
PRIVATE lDicion     := .F.
PRIVATE lFiltro     := .T.
PRIVATE lCrystal    := .F.
PRIVATE aReturn     := { oEmToAnsi(STR0002), 1,oEmToAnsi(STR0003) , 1, 1, 1, "",1 }
PRIVATE aOrd		:= { STR0004}														//-- Pacote X R.D.A.
PRIVATE lAbortPrint := .F.
PRIVATE cCabec1     := ""
PRIVATE cCabec2     := ""

//��������������������������������������������������������������Ŀ
//� Variaveis Utilizadas na funcao IMPR                          �
//����������������������������������������������������������������
PRIVATE cCabec
PRIVATE Colunas		:= 132
PRIVATE AT_PRG  	:= "PLSR103"
PRIVATE wCabec0 	:= 1
PRIVATE wCabec1		:= space(3) + oemtoAnsi(STR0009)					//-- Cabecalho do Detalhe
PRIVATE wCabec2		:=""
PRIVATE wCabec3		:=""
PRIVATE wCabec4		:=""
PRIVATE wCabec5		:=""
PRIVATE wCabec6		:=""
PRIVATE wCabec7		:=""
PRIVATE wCabec8		:=""
PRIVATE wCabec9		:=""
PRIVATE CONTFL		:=1
PRIVATE cPathPict	:= ""


Pergunte(cPerg,.F.)

/*��������������������������������������������������������������Ŀ
  � Envia controle para a funcao SETPRINT                        �
  ����������������������������������������������������������������*/
wnrel:="Plsr103"					           //Nome Default do relatorio em Disco
wnrel:=SetPrint(cAlias,wnrel,cPerg,@Titulo,cDesc1,cDesc2,cDesc3,.F.,aOrd,,nTamanho)

/*��������������������������������������������������������������������������Ŀ
  | Verifica se foi cancelada a operacao                                     �
  ����������������������������������������������������������������������������*/
If nLastKey  == 27
   Return
Endif
/*��������������������������������������������������������������������������Ŀ
  � Configura impressora                                                     �
  ����������������������������������������������������������������������������*/
SetDefault(aReturn,cAlias)
If nLastKey = 27
	Return
Endif 

RptStatus({|lEnd| R103Imp(@lEnd,wnRel,cAlias)},Titulo)

Return


/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Programa   � R103Imp  � Autor � Natie Sugahara        � Data � 25/06/03 ���
��������������������������������������������������������������������������Ĵ��
���Descricao  � Emite relatorio                                            ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/
Static Function R103Imp()
Local   cSQL			:= ""
Local   cPict			:= "@E    9,999.9999"
Local   cPictVL			:= "@E  9,999,999.99"
Local   cCodOperadora	:= ""
Local   cCodRDA			:= ""
Local   cProcedi		:= ""
Local   nX				:= 0
Local   nOrdem  		:= aReturn[8]
Local 	nTotUs      	:= nTotUsProc  	:= 0
Local   nTotVlr     	:= nTotVlrProc	:= 0
Local   aProcedi		:= {}
Local   aTpProcedi		:= { ;
							  {"0",	"Procedimento"  },;
                              {"1", "Material"      },;
                              {"2", "Medicamento"   },;
                              {"3", "Taxas"         },;
                              {"4", "Diarias"       },;
                              {"5", "Ortese/Protese"} ;
                           }

/*��������������������������������������������������������������������������Ŀ
  � Acessa parametros do relatorio...                                        �
  � Variaveis utilizadas para parametros                                     �
  ����������������������������������������������������������������������������*/
cOpeDe		:= mv_par01					//-- Codigo da operadora Inicial
cOpeAte		:= mv_par02					//-- Codigo da operadora Final
cRdaDe		:= mv_par03					//-- Codigo RDA  Inicial
cRdaAte		:= mv_par04					//-- Codigo RDA  Final
cProcDe		:= mv_par05					//-- codigo Procedimento Inicial
cProcAte	:= mv_par06 				//-- codigo Procedimento Final
cPacDe		:= mv_par07 				//-- Codigo Pacote Inicial
cPacAte		:= mv_par08					//-- Codigo Pacote Final
cPadDe		:= mv_par09					//-- Codigo Tab. Padrao Inicial
cPadAte		:= mv_par10					//-- codigo Tab. Padrao Final 


//��������������������������������������������������������������������������Ŀ
//� Faz filtro no arquivo...                                                 �
//����������������������������������������������������������������������������
#IFDEF TOP
        cSQL := "SELECT * FROM "+BLZ->(RetSQLName("BLZ"))+" WHERE "
        cSQL += "BLZ_FILIAL = '"+xFilial("BLZ")+"' "
        cSQL += "AND D_E_L_E_T_ = ' '  AND "
        cSQL += "BLZ_CODINT >= '"+cOpeDe   +"' AND BLZ_CODINT <= '"+cOpeAte   +"' AND "
		cSQL += "BLZ_CODRDA >= '"+cRdaDe   +"' AND BLZ_CODRDA <= '"+cRdaAte   +"' AND "
        cSQL += "BLZ_CODPRO >= '"+cProcDe  +"' AND BLZ_CODPRO <= '"+cProcAte  +"' AND "
        cSQL += "BLZ_CODPAC >= '"+cPacDe   +"' AND BLZ_CODPAC <= '"+cPacAte   +"' AND "
        cSQL += "BLZ_CODPAD >= '"+cPadDe   +"' AND BLZ_CODPAD <= '"+cPadAte   +"'"
        //��������������������������������������������������������������������������Ŀ
        //� Se houver filtro executa parse para converter expressoes adv para SQL    �
        //����������������������������������������������������������������������������
        If ! Empty(aReturn[7])
			cSQL += " and " + PLSParSQL(aReturn[7])
        Endif   
        //��������������������������������������������������������������������������Ŀ
        //� Define order by de acordo com a ordem...                                 �
        //����������������������������������������������������������������������������                                                     7
		cSQL += " ORDER BY BLZ_FILIAL+BLZ_CODINT+ BLZ_CODRDA+ BLZ_CODPAD+BLZ_CODPRO+ BLZ_CODPAC"
		cSQL := PLSAvaSQL(cSQL)
        TCQUERY cSQL NEW ALIAS "BLZTrb"
#ENDIF        

BLZTrb->(dbgoTop())
SetRegua(BLZTrb->(RecCount()))
Li 		:= 0

While  !( BLZTrb->(Eof()) )
	IncRegua()
	If BLZTrb->BLZ_CODINT # cCodOperadora .or. BLZTrb->BLZ_CODRDA # cCodRDA
		ImpQuebra()
		cCodOperadora	:= BLZTrb->BLZ_CODINT
		cCodRDA			:= BLZTrb->BLZ_CODRDA
	Endif
	cDet	:= oEmToAnsi(STR0008) + BLZTrb->BLZ_CODPAD	 + space(1)
	cDet	+= fDesc("BR4",BLZTrb->(BLZ_CODPAD) ,"BR4_DESCRI" )  								//-- Tabela Padrao
	Impr(cDet,"C",,,03,.F.)

	cDet	:= oEmToAnsi(STR0004)+"  : "+ BLZTrb->(BLZ_CODPAC + space(1)+ BLZ_DESCRI)			//-- Pacote
	Impr(cDet,"C",,,80,.T.)

	cDet	:= oEmToAnsi(STR0007)+ Alltrim(BLZTrb->BLZ_CODPRO)+ space(1)
	cDet	+= fDesc("BR8",BLZTrb->(BLZ_CODPAD+ BLZ_CODPRO) ,"BR8_DESCRI" )  					//-- Procedimento Principal	
	Impr(cDet,"C",,,03,.T.)
	/*��������������������������������������������������������������������Ŀ
	  � Impressao do Detalhe                                               �
	  ����������������������������������������������������������������������*/
	cSQL := "SELECT * FROM "+BLY->(RetSQLName("BLY"))+" WHERE D_E_L_E_T_ = '' AND "
    cSQL += "BLY_FILIAL = '"+xFilial("BLY")+"' AND BLY_CODRDA+BLY_CODPAD+BLY_CODPRO+BLY_CODPAC = '" + BLZTrb->(BLZ_CODRDA + BLZ_CODPAD + BLZ_CODPRO + BLZ_CODPAC)
    cSQL += "' ORDER BY BLY_FILIAL+BLY_CODRDA+BLY_CODPAD+ BLY_CODPRO+ BLY_CODPAC + BLY_TIPO+ BLY_CODTAB + BLY_CODOPC"
    cSQL := PLSAvaSQL(cSQL)
    #IFDEF TOP
       TCQUERY cSQL NEW ALIAS "BLYTrb"
    #ENDIF
    BLYTrb->(DbGoTop())

	/*��������������������������������������������������������������������Ŀ
	  � Processao Detalhes                                                 �
	  ����������������������������������������������������������������������*/
	aProcedi	:= {}
	While !BLYTrb->( eof() )
		Aadd(aProcedi, { BLYTrb->(BLY_CODRDA+ BLY_CODPAD+BLY_CODPRO+BLY_CODPAC) ,;
						 BLYTrb->BLY_TIPO , ;
						 BLYTrb->BLY_CODTAB + SPACE(1) + left(fDesc("BF8", BLYTrb->(BLY_CODINT+BLY_CODTAB) ,"BF8_DESCM" ),30) , ;
		                 BLYTrb->BLY_CODOPC + SPACE(1) + left(fDesc("BA8", BLYTrb->BLY_CODOPC +alltrim(BLZTrb->BLZ_CODINT)+ BLYTrb->BLY_CODTAB , "BA8_DESCRI",,,4),20) , ;
		                 BLYTrb->BLY_VALCH ,;
		                 BLYTrb->BLY_VALFIX,;
		                 BLYTrb->BLY_ATIVO  ;
		                 };
            )

		BLYTrb->(dbSkip())
	EndDo
	/*��������������������������������������������������������������������Ŀ
	  � Ordena  pelo Vinculo                                               �
	  ����������������������������������������������������������������������*/
	aSort( aProcedi,,,{ |x,y| x[1]+x[2] +x[3] < y[1]+ y[2] + y[3]} )

	/*��������������������������������������������������������������������Ŀ
	  � Impressao da Linha de  Detalhe                                     �
	  ����������������������������������������������������������������������*/
	cProcedi	:= ""
	For nX	:= 1 to Len(aProcedi)
		If cProcedi # aProcedi[nX,2]
		
			//-- classificacao de Procedimento
			nPos := Ascan( aTpProcedi,{|x| x[1] ==aProcedi[nX,2]})
	  		//-- Total por Procedimento
			If nX <> 1
				ImprTotal(aProcedi,cProcedi) 
			Endif	
		  	If nPos > 0
				Impr("","C",000,.f.)
				Impr(aTpProcedi[nPos,2],"C",,,003, .F. )					//-- Classificacao
		  	Else
		  		Impr("","C",000,.f.)
	  		Endif
			cProcedi := aProcedi[nX,2]
		Endif
		Impr(aProcedi[nX,3]                     , "C",,,018 ,.F.) 				//-- Tabela
		Impr(aProcedi[nX,4]                     , "C",,,057 ,.F.) 				//-- Procedimeto
		Impr(Transform(aProcedi[nX,5], cPict  ) ,"C" ,,,0100,.F.) 				//-- US
		Impr(Transform(aProcedi[nX,6], cPictVl ), "C",,,0112,.F.) 				//-- Valor Fixo
		Impr(aProcedi[nX,7]                     , "C",,,0131,.T.) 				//-- Ativo
	Next nX
	/*��������������������������������������������������������������������Ŀ
	  � Total do procedimento/Classificao                                  �
	  ����������������������������������������������������������������������*/
	ImprTotal(aProcedi,cProcedi) 
	/*��������������������������������������������������������������������Ŀ
	  � Total do procedimento principal                                    �
	  ����������������������������������������������������������������������*/
	aEval(aProcedi, { |x| nTotUs     += x[5] })
	aEval(aProcedi, { |x| nTotVlr    += x[6] })
	Impr("","C",,,00,.T.)
	Impr(oEmToAnsi(STR0011)+ SPACE(1) + left(fDesc("BR8",BLZTrb->(BLZ_CODPAD+ BLZ_CODPRO) ,"BR8_DESCRI" ) ,20) ,"C",,,057,.F.)				//-- Procd.Principal
	Impr(Transform(nTotUs , cPict  ),"C",,,100,.F.)
	Impr(Transform(ntotVlr, cPictVL),"C",,,112,.T.)
	nTotUs      	:= 0
	nTotVlr    		:= 0
	cDet    := __PrtThinLine()
	Impr(cDet,"C",,,00,.T.)
	BLYTrb->(DbCloseArea())	
	BLZTrb->(dbSkip())
EndDo
Impr("","F")

//��������������������������������������������������������������������Ŀ
//� Fecha arquivo...                                                   �
//����������������������������������������������������������������������
BLZTrb->(DbCloseArea())

/*��������������������������������������������������������������������������Ŀ
  � Libera impressao                                                         �
  ����������������������������������������������������������������������������*/
If  aReturn[5] == 1
	Set Printer To
	Ourspool(wnRel)
EndIf
/*��������������������������������������������������������������������������Ŀ
  � Fim do Relat�rio                                                         �
  ����������������������������������������������������������������������������*/
Return



/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ImprTotal �Autor  �Microsiga           � Data �  06/25/03   ���
�������������������������������������������������������������������������͹��
���Desc.     �Imprime Totais                                              ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ImprTotal(aProcedi,cProcedAnt) 
Local nX			:= 0
Local nTotUsProc  	:= 0
Local nTotVlrProc	:= 0
Local cPict			:= "@E    9,999.9999"
Local cPictVL		:= "@E  9,999,999.99"

aEval(aProcedi, { |x| If( cProcedAnt= x[2],nTotUsProc += x[5],"") })
aEval(aProcedi, { |x| If( cProcedAnt= x[2],nTotVlrProc+= x[6],"") })

Impr(oEmToAnsi(STR0010)             ,"C",,,057,.F.)								//-- Total ...
Impr(Transform(nTotUsProc , cPict  ),"C",,,100,.F.)
Impr(Transform(ntotVlrProc, cPictVL),"C",,,112,.T.)
Return(NIL)



/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ImpQuebra �Autor  �Microsiga           � Data �  06/25/03   ���
�������������������������������������������������������������������������͹��
���Desc.     �Imprime quebra de Operadora e RDA                           ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ImpQuebra()
Local cDet	:= ""
cDet	:= oEmToAnsi(STR0005) + Transform(BLZTrb->BLZ_CODINT,"@R #.###")  + SPACE(1)
cDet	+= fDesc("BA0",BLZTrb->BLZ_CODINT,"BA0->BA0_NOMINT" )							//-- Cod OPeradora e Descricao
Impr(cDet,"C",,,00,.T.)
cDet	:= oEmToAnsi(STR0006) + BLZTrb->BLZ_CODRDA + SPACE(1)
cDet	+= fDesc("BAU",BLZTrb->BLZ_CODRDA,"BAU->BAU_NOME" )							//-- Cod RDA
Impr(cDet,"C",,,00,.T.) 
Impr('',"C",,,00,.T.)

Return(NIL)
