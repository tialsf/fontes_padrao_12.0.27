#include "PLSMGER.CH"
#include "AP5MAIL.CH"

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    � PLSR197 � Autor � Sandro Hoffman Lopes   � Data � 23.12.05 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Lista relatorio de inconsistencias nas Guias e envia o     ����
���          � relatorio via e-mail para os enderecos configurados        ����
���          � no AP7SRV.INI - Secao "REL_GUIAS"                          ����
�������������������������������������������������������������������������Ĵ���
���Sintaxe   � PLSR197()                                                  ����
�������������������������������������������������������������������������Ĵ���
��� Uso      � Advanced Protheus                                          ����
�������������������������������������������������������������������������Ĵ���
��� Alteracoes desde sua construcao inicial                               ����
�������������������������������������������������������������������������Ĵ���
��� Data     � BOPS � Programador � Breve Descricao                       ����
�������������������������������������������������������������������������Ĵ���
��������������������������������������������������������������������������ٱ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Main Function PLSR197

   //���������������������������������������������������������������������Ŀ
   //� Declaracao de Variaveis                                             �
   //�����������������������������������������������������������������������
   Local cDesc1        := "Este programa tem como objetivo imprimir relatorio "
   Local cDesc2        := "de inconsistencias nas guias."
   Local cDesc3        := ""
   Local titulo        := "RELATORIO DE INCONSISTENCIAS NAS GUIAS"
   Local nLin          := 80
   Local Cabec1        := "LOC  PEG      NUMERO   TIPO GRAV         DTA PROC HORA  COD DESC GLOSA   

   Local Cabec2        := ""
   Local aOrd          := {}
   
   Local lRegsBD5      := .F.
   Local lRegsBE4      := .F.

   Private CbTxt       := ""
   Private nlimite     := 132
   Private cTamanho    := "M"
   Private nomeprog    := "PLSR197"
   Private nTipo       := 15
   Private aReturn     := { "Zebrado", 1, "Administracao", 1, 1, 1, "", 1 }
   Private nLastKey    := 0
   Private cPerg       := ""
   Private cbcont      := 00
   Private CONTFL      := 01
   Private m_pag       := 01
   Private cRel        := "PLSR197"
   Private lCompres    := .T.
   Private lDicion     := .F.
   Private lFiltro     := .F.
   Private lCrystal    := .F.
   Private cString     := "BD5"
   Private lGerTXT     := .T.

   RpcSetEnv ( AllTrim(GetPvProfString( "REL_GUIAS", "EMPRESA", "99", GetADV97() )), ;
               AllTrim(GetPvProfString( "REL_GUIAS", "FILIAL", "01", GetADV97() )), ;
               "", "","","", {"BD5", "BE4", "BDX" } )       

   //���������������������������������������������������������������������Ŀ
   //� Monta a interface padrao com o usuario...                           �
   //�����������������������������������������������������������������������
   cRel:= SetPrint(cString,NomeProg,cPerg,@Titulo, cDesc1,cDesc2,cDesc3,lDicion,aOrd,    lCompres,cTamanho, {},lFiltro,lCrystal,,lGerTXT)

   If nLastKey == 27
   	  Return
   EndIf

   If lGerTXT
      If File(__RelDir + cRel + ".##R")
         fErase(__RelDir + cRel + ".##R")
      EndIf
  	  SetPrintFile(cRel)
   EndIf

   SetDefault(aReturn,cString)

   If nLastKey == 27
      Return
   EndIf

   nTipo := If(aReturn[4]==1,15,18)

   //��������������������������������������������������������������������������Ŀ
   //� Emite relat�rio - BD5                                                    �
   //����������������������������������������������������������������������������
   Titulo := "RELATORIO DE INCONSISTENCIAS NAS GUIAS DE CONSULTA/SERVICO"
   R197Imp(Cabec1,Cabec2,Titulo,nLin,@lRegsBD5,"BD5")
   //��������������������������������������������������������������������������Ŀ
   //� Emite relat�rio - BE4                                                    �
   //����������������������������������������������������������������������������
   Titulo := "RELATORIO DE INCONSISTENCIAS NAS GUIAS DE INTERNACAO"
   R197Imp(Cabec1,Cabec2,Titulo,nLin,@lRegsBE4,"BE4")
   
   If lRegsBD5 .Or. lRegsBE4 // Se havia registros com inconsistencia, imprime rodape e envia e-mail

      Roda(0,"","M")

      //���������������������������������������������������������������������Ŀ
      //� Finaliza a execucao do relatorio...                                 �
      //�����������������������������������������������������������������������

      MS_FLUSH()

      R197Mail(cRel)
   
   EndIf

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �R197Imp    � Autor � Sandro Hoffman Lopes  � Data � 23/12/05���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Imprime relatorio de inconsistencias nas guias e envia email���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function R197Imp(Cabec1,Cabec2,Titulo,nLin,lRegs,cAlias)
       
   Local cCabName := RetSQLName(cAlias)
   Local cBDXName := RetSQLName("BDX")

   //��������������������������������������������������������������������������Ŀ
   //� Faz filtro no arquivo...                                                 �
   //����������������������������������������������������������������������������
   cSQL := " SELECT " + cAlias + "_CODLDP CODLDP, "
   cSQL +=              cAlias + "_CODPEG CODPEG, "
   cSQL +=              cAlias + "_NUMERO NUMERO, "
   If cAlias == "BD5"
      cSQL +=           cAlias + "_TPGRV  TPGRV,  "
    Else
      cSQL +=           "'1' TPGRV, "
   EndIf
   cSQL +=              cAlias + "_DATPRO DATPRO, "
   cSQL +=              cAlias + "_HORPRO HORPRO, BDX_CODGLO, BDX_DESGLO "
   cSQL += " FROM " + cCabName + ", " + cBDXName
   cSQL += " WHERE " + cAlias + "_ERRO    = '1' AND "
   cSQL += "       " + cAlias + "_SITUAC <> '2' AND "
   cSQL += "       " + cCabName + ".D_E_L_E_T_ = ' ' AND "
   cSQL += "       BDX_CODLDP  = " + cAlias + "_CODLDP AND "
   cSQL += "       BDX_CODPEG  = " + cAlias + "_CODPEG AND "
   cSQL += "       BDX_NUMERO  = " + cAlias + "_NUMERO AND "
   cSQL += "       "+cBDXName+".D_E_L_E_T_ = ' ' "
   cSQL += " ORDER BY " + cAlias + "_FILIAL, " + cAlias + "_CODOPE, " + cAlias + "_CODLDP, " + ;
                          cAlias + "_CODPEG, " + cAlias + "_NUMERO"

   PLSQuery(cSQL,"Trb197") // Igual ao TCQuery
   
   If Trb197->(Eof())
      lRegs := .F.
    Else
      Trb197->(DbGotop()) 
      Do While ! Trb197->(Eof())
         fSomaLin(@nLin, Titulo, Cabec1, Cabec2, 1)
         @ nLin, 0 pSay Trb197->CODLDP + " "  + Trb197->CODPEG        + " " + Trb197->NUMERO + " " + ;
                        PadR(X3Combo("BD5_TPGRV", Trb197->TPGRV), 17) + " " + DtoC(StoD(Trb197->DATPRO)) + " " + ;
                        Transform(Trb197->HORPRO, "@R 99:99") + " " + ;
                        Trb197->BDX_CODGLO + " "  + Trb197->BDX_DESGLO 
         Trb197->(DbSkip())
      EndDo
      lRegs := .T.
   EndIf
   Trb197->(DbCloseArea())
   
Return

/*/  
����������������������������������������������������������������������������������
����������������������������������������������������������������������������������
������������������������������������������������������������������������������Ŀ��
��� Programa  � fSomaLin      � Autor � Sandro Hoffman     � Data � 23.12.2005 ���
������������������������������������������������������������������������������Ĵ��
��� Descri��o � Soma "n" Linhas a variavel "nLin" e verifica limite da pagina  ���
���           � para impressao do cabecalho                                    ���
�������������������������������������������������������������������������������ٱ�
����������������������������������������������������������������������������������
����������������������������������������������������������������������������������
/*/
Static Function fSomaLin(nLin, Titulo, Cabec1, Cabec2, nLinSom)

   nLin += nLinSom
   If nLin > 58
      nLin := Cabec(Titulo,Cabec1,Cabec2,NomeProg,cTamanho,nTipo) + 1
   EndIf

Return

/*/  
����������������������������������������������������������������������������������
����������������������������������������������������������������������������������
������������������������������������������������������������������������������Ŀ��
��� Programa  � R197Mail      � Autor � Sandro Hoffman     � Data � 23.12.2005 ���
������������������������������������������������������������������������������Ĵ��
��� Descri��o � Conecta ao Servidor de email envia mensagem                    ���
�������������������������������������������������������������������������������ٱ�
����������������������������������������������������������������������������������
����������������������������������������������������������������������������������
/*/
Static Function R197Mail(cRel)

   Local cServer     := AllTrim(GetNewPar("MV_RELSERV"," "))    // Servidor de e-mail
   Local cAccount    := AllTrim(GetNewPar("MV_RELACNT"," "))    // Conta
   Local cPassword   := AllTrim(GetNewPar("MV_RELPSW" ," "))    // Senha
   Local nTimeOut    := GetMv("MV_RELTIME",,120)                // Tempo de Espera antes de abortar a Conexao
   Local lAutentica  := GetMv("MV_RELAUTH",,.F.)                // Determina se o Servidor de Email necessita de Autenticacao
   Local cUserAut    := Alltrim(GetMv("MV_RELAUSR",,cAccount))  // Usuario para Autenticacao no Servidor de Email
   Local cPassAut    := Alltrim(GetMv("MV_RELAPSW",,cPassword)) // Senha para Autenticacao no Servidor de Email
   Local cFrom       := AllTrim(GetPvProfString( "REL_GUIAS", "FROM", "", GetADV97() ))
   Local cTo         := AllTrim(GetPvProfString( "REL_GUIAS", "TO", "", GetADV97() ))
   Local cCC         := AllTrim(GetPvProfString( "REL_GUIAS", "CC", "", GetADV97() ))
   Local cSubject    := AllTrim(GetPvProfString( "REL_GUIAS", "SUBJECT", "Relat�rio de Inconsist�ncias nas Guias", GetADV97() ))
   Local cBody       := AllTrim(GetPvProfString( "REL_GUIAS", "BODY", "N�o responda este e-mail. Esta mensagem � autom�tica e n�o deve ser respondida.", GetADV97() ))
   Local cAnexos     := __RelDir + cRel + ".##R"

   If Type(cSubject) == "UI"
      cSubject := &(cSubject)
   EndIf

   If Type(cBody) == "UI"
      cBody := &(cBody)
   EndIf

   CONNECT SMTP SERVER cServer ACCOUNT cAccount PASSWORD cPassword TIMEOUT nTimeOut RESULT lOk
   If lOk
      If lAutentica
         If ! MailAuth(cUserAut, cPassAut)
            FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', NomeProg + ": Falha na Autenticacao do Usuario" , 0, 0, {})
            DISCONNECT SMTP SERVER RESULT lOk
            If ! lOk
               GET MAIL ERROR cErrorMsg
               FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', NomeProg + ": Erro na Desconecao: " + cErrorMsg , 0, 0, {})
            EndIf   
            Return .F.
         EndIf
      EndIf 
      If ! Empty(cCC)
         SEND MAIL FROM cFrom TO cTo CC cCC SUBJECT cSubject BODY cBody ATTACHMENT cAnexos RESULT lOk
      Else
         SEND MAIL FROM cFrom TO cTo SUBJECT cSubject BODY cBody ATTACHMENT cAnexos RESULT lOk
      EndIf
      If ! lOk                       
         GET MAIL ERROR cErrorMsg
         FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', NomeProg + ": Erro na tentativa de enviar email: " + cErrorMsg , 0, 0, {})
      EndIf
   Else
      GET MAIL ERROR cErrorMsg
      //ConOut(NomeProg + ": Erro na tentativa de conexao com o SERVER: " + cErrorMsg, 2, 0)
   EndIf
   DISCONNECT SMTP SERVER RESULT lOk
   If ! lOk
      GET MAIL ERROR cErrorMsg
      FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', NomeProg + ": Erro na Desconexao: " + cErrorMsg , 0, 0, {})
   EndIf   

Return
