#include "PLSMGER.CH"
#include "PROTHEUS.CH"
#include "TCBROWSE.CH"
#include "JPEG.CH"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    | PLSA004   � Autor � Daher			    | Data � 11.05.2010��
�������������������������������������������������������������������������Ĵ��
���Descricao �Cadastramento Sinistralidade								  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function PLSA004
LOCAL cAlias  := "B0M"  

//��������������������������������������������������������������������������Ŀ
//� Define variaveis LOCAL...                                                �
//����������������������������������������������������������������������������                        
PRIVATE cCadastro   := "Cadastramento Sinistralidade"
PRIVATE aRotina  := {   			{ "Pesquisar"	   			, 'AxPesqui'     	, 0 , K_Pesquisar  },; 
									{ "Visualizar"	   			, 'AxVisual' 		, 0 , K_Visualizar 	},;
									{ "Incluir"  	    		, 'AxInclui'		, 0 , K_Incluir    	},;
									{ "Alterar"  	    		, 'AxAltera'		, 0 , K_Alterar    	},;
									{ "Excluir"	   				, 'AxExclui' 		, 0 , K_Excluir 	}} 

	
B0M->(DbSetOrder(1))
B0M->(DbGoTop())
B0M->(mBrowse(006,001,022,075,cAlias,,,,,,))
	
Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    | PLSA004   � Autor � Daher			    | Data � 11.05.2010��
�������������������������������������������������������������������������Ĵ��
���Descricao �Cadastramento Sinistralidade								  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function PL004MOV
LOCAL cFiltro := ""
LOCAL cAlias  := "B0M"
PRIVATE cCadastro   := "Cadastramento Sinistralidade"
PRIVATE aRotina  := {   			{ "Pesquisar"	   			, 'AxPesqui'     	, 0 , K_Pesquisar  },; 
									{ "Visualizar"	   			, 'AxVisual' 		, 0 , K_Visualizar 	},;
									{ "Incluir"  	    		, 'AxInclui'		, 0 , K_Incluir    	},;
									{ "Alterar"  	    		, 'AxAltera'		, 0 , K_Alterar    	},;
									{ "Excluir"	   				, 'AxExclui' 		, 0 , K_Excluir 	}} 

If FunName() == "PLSA010"
	cFiltro := "@"+cAlias+"_FILIAL = '"+xFilial("B0M")+"' AND B0M_CODINT = '"+BA0->(BA0_CODIDE+BA0_CODINT)+"'"    
	cFiltro += " AND B0M_CODEMP = ' ' AND B0M_CONEMP = ' ' AND B0M_SUBCON = ' ' AND B0M_MATRIC = ' ' AND B0M_TIPREG = ' '"
	B0M->(DbSetOrder(1))
	SET FILTER TO &cFiltro		
Endif

B0M->(mBrowse(006,001,022,075,cAlias,,,,,,))
B0M->(DbClearFilter())

Return