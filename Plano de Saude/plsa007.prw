#include "PROTHEUS.CH"
#include "PLSA007.CH"
/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Fun�ao    � PLSA007   � Autor � Microsiga             � Data � 05/01/11 ���
��������������������������������������������������������������������������Ĵ��
���Descri�ao � Rotina Processamento Ajuste Data Pagto PEG	 	           ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Function PLSA007()
LOCAL aSays     := {}
LOCAL aButtons  := {}
LOCAL cPerg     := "PLA007"
LOCAL cCadastro := STR0001 //"Processamento das Datas de Pagto das PEGS"
//��������������������������������������������������������������������������Ŀ
//� Monta texto para janela de processamento                                 �
//����������������������������������������������������������������������������
AADD(aSays,STR0002) //"Processamento para Ajustes das Datas de Pagto das PEGS que est�o com status"
AADD(aSays,STR0003) //" em confer�ncia e digitacao e que a data de pagamento j� expirou"

//��������������������������������������������������������������������������Ŀ
//� Monta botoes para janela de processamento                                �
//����������������������������������������������������������������������������
AADD(aButtons, { 5,.T.,{|| Pergunte(cPerg,.T. ) } } )
aadd(aButtons, { 1,.T.,{|| Processa({|| PLSA007Pro(cPerg)},STR0004,STR0005,.F.) } }) //"Processando" #"Aguarde..."
AADD(aButtons, { 2,.T.,{|| FechaBatch() }} )
//��������������������������������������������������������������������������Ŀ
//� Exibe janela de processamento                                            �
//����������������������������������������������������������������������������
FormBatch( cCadastro, aSays, aButtons,, 160 )
//��������������������������������������������������������������������������Ŀ
//� Fim da Rotina Principal...                                               �
//����������������������������������������������������������������������������
Return

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Fun�ao    � PLSA007Pro� Autor � Microsiga             � Data � 05/01/11 ���
��������������������������������������������������������������������������Ĵ��
���Descri�ao � Grava Processo								 	           ���
��������������������������������������������������������������������������Ĵ��
��� Uso      � SigaPLS                                                     ���
��������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                      ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function PLSA007Pro(cPerg)
//���������������������������������������������������������������������Ŀ
//� Declaracao de variaveis...                                          �
//�����������������������������������������������������������������������
LOCAL cSQL
LOCAL lReembolso  := .F.
LOCAL aDadBCI     := {} 
LOCAL cRegime 	  := ""
LOCAL dDatPagIni  := cTod("")
LOCAL dDatPagFim  := cTod("")
LOCAL dDataNovo   := cTod("")
LOCAL cFase       := ""
LOCAL nPeg 		  := 0

cRegime 	:= mv_par01  // Regime
dDatPagIni  := mv_par02  // Data Pagto Inicial
dDatPagFim  := mv_par03  // Data Pagto Final
dDataNovo   := mv_par04  // Data Pagto Nova 
cFase       := mv_par05  // Fase

If cRegime == 2 // Reembolso
	lReembolso:= .T.
Endif

// Regime de Reembolso
cRegime="04" 

//���������������������������������������������������������������������Ŀ
//� Verificar as PEGs que estao Data Pagto anterior Database		    �
//�����������������������������������������������������������������������
cSQL := "SELECT BCI_CODOPE, BCI_CODLDP, BCI_CODPEG,BCI_CODRDA,BCI_CODDAT,BCI_DATPAG,R_E_C_N_O_ RecnoBCI FROM "+RetSQLName("BCI")+" WHERE "
cSQL += " BCI_FILIAL = '"+xFilial("BCI")+"' AND "
cSQL += " BCI_CODOPE = '"+PLSINTPAD()+"' AND "
cSQL += " BCI_FASE ='"+Alltrim(Str(cFase))+"' AND " // Digitacao e Conferencia
cSQL += " BCI_SITUAC ='1' AND "  // Ativo
If lReembolso
	cSQL += " BCI_TIPGUI = '"+cRegime+"' AND "
Else
	cSQL += " BCI_TIPGUI <> '"+cRegime+"' AND "
Endif
cSQL += " BCI_DATPAG <= '"+DtoS(dDataBase)+"' AND "
cSQL += " BCI_DATPAG >= '"+DtoS(dDatPagIni)+"' AND "
cSQL += " BCI_DATPAG <= '"+DtoS(dDatPagFim)+"' AND "
cSQL += " D_E_L_E_T_ = '' "

PLSQuery(cSQL,"TrBCI")

If TrBCI->(Eof())
	MsgInfo("Nao foi encontrado nenhum PEG com as parametriza��es informada.")
	TrBCI->(DbCloseArea())
Else
	
	nPeg := 0
	
	While TrBCI->(!Eof())
	
		IncProc(" Processando PEG :"+TrBCI->(BCI_CODPEG))
	    
	    nPeg ++
	    
		aAdd(aDadBCI,{TrBCI->(BCI_CODOPE),TrBCI->(BCI_CODLDP),TrBCI->(BCI_CODPEG),TrBCI->(BCI_CODRDA),TrBCI->(BCI_CODDAT),TrBCI->(BCI_DATPAG),"000",dDataNovo,nPeg,RecnoBCI})
			
		TrBCI->(DbSkip())
	Enddo

TrBCI->(DbCloseArea())

Endif

If Len(aDadBCI) > 0
	If PLSCRIGEN(aDadBCI,{ {"Operadora","@C",30},{"Local","@C",30},{"Cod Peg","@C",30},{"CodRda","@C",30 }, {"Cod Data De","@C",40 }, {"Dt Pagto De ","@D",40}, {"Cod Data Para","@C",40 }, {"Dt Pagto Para ","@D",40},{"Totalizador","@N",10} },"Itens Selecionados")[1]
		If Aviso("Aten��o","Deseja confirmar o processamento das PEGs",{"Sim","N�o"},1) == 1
			PLGRV007(aDadBCI,)	
		EndIf
	EndIf                    
EndIf
//���������������������������������������������������������������������Ŀ
//� Fim da rotina principal...                                          �
//�����������������������������������������������������������������������
Return

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Fun�ao    � PLGRV007  � Autor � Microsiga             � Data � 05/01/11 ���
��������������������������������������������������������������������������Ĵ��
���Descri�ao � Grava Processo								 	           ���
��������������������������������������������������������������������������Ĵ��
��� Uso      � SigaPLS                                                     ���
��������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                      ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function PLGRV007(aDados)
Local nI := 0

If Len(aDados) > 0
	For nI:= 1 To Len(aDados)
		BCI->(dbGoto(aDados[nI,10]))
		BCI->(RecLock("BCI",.F.))
		BCI->BCI_CODDAT := aDados[nI,7]
		BCI->BCI_DATPAG := aDados[nI,8]
		BCI->( MsUnlock() )
	Next nI
	MsgInfo(STR0006) //"Processado com sucesso!"
Endif

Return

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Fun�ao    � PLSVALSX1 � Autor � Microsiga             � Data � 05/01/11 ���
��������������������������������������������������������������������������Ĵ��
���Descri�ao � Validacao SX1								 	           ���
��������������������������������������������������������������������������Ĵ��
��� Uso      � SigaPLS                                                     ���
��������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                      ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Function PLSVSX1007(cParPrinc, cParAnt,cCodData)
Local   lRet := .T.
Default cCodData := "0" 
Default cParAnt := ""

If Empty(cParPrinc)
	MsgInfo(STR0007)//"Deve informar uma data!"
	lRet := .F.
Endif
//�����������������������������������������������������������������������
//� Valida se e domingo, sabado ou feriado
//�����������������������������������������������������������������������
lRet := PLVLDSD(cParPrinc)

If lRet .And. cCodData == "1" 
	If cParPrinc < dDataBase
		MsgInfo(STR0008) //"A Data informada deve ser maior ou igual a Data Atual do sistema!"
		lRet := .F.
	Endif
Else 
	If lRet .And. cParPrinc > dDataBase
		MsgInfo(STR0009) //"A Data informada deve ser menor que a Data Atual do sistema!"
		lRet := .F.
	Endif 
	If lRet .And. !Empty(cParAnt) .And. cParPrinc < cParAnt 
		MsgInfo(STR0010) //"A Data informada deve ser maior ou igual a Data Anterior!"
		lRet := .F.
	Endif
Endif
	 
Return lRet
