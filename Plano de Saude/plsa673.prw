#include "PROTHEUS.CH"
#include "PLSMGER.CH"
/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    � PLSA673 � Autor � Eduardo Motta          � Data � 13.11.05 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Gerecao de arquivo de extrato de contas                    ����
�������������������������������������������������������������������������Ĵ���
���Sintaxe   � PLSA673()                                                  ����
�������������������������������������������������������������������������Ĵ���
��� Uso      � Advanced Protheus                                          ����
�������������������������������������������������������������������������Ĵ���
��� Alteracoes desde sua construcao inicial                               ����
�������������������������������������������������������������������������Ĵ���
��� Data     � BOPS � Programador � Breve Descricao                       ����
�������������������������������������������������������������������������Ĵ���
��������������������������������������������������������������������������ٱ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/

User Function PLSA673
//��������������������������������������������������������������������������Ŀ
//� Parametros do relatorio (SX1)...                                         �
//����������������������������������������������������������������������������
PRIVATE cTitulo := "Geracao de Extrato de Contas"
PRIVATE cPerg       := "PLA673"
PRIVATE cCodOpe
PRIVATE cRdaDe 
PRIVATE cRdaAte
PRIVATE cAno   
PRIVATE cMes   
PRIVATE nTipQbc
PRIVATE nTipRel
PRIVATE nNumImp
PRIVATE cClaPre
PRIVATE lLisRes
PRIVATE lLisTit
PRIVATE nImpEnd

Private aLog  := {}
//��������������������������������������������������������������������������Ŀ
//� Ajusta perguntas                                                         �
//����������������������������������������������������������������������������
CriaSX1() //nova pergunta...

If !Pergunte(cPerg)
   Return
EndIf

cCodOpe    := mv_par01
cRdaDe     := mv_par02
cRdaAte    := mv_par03
cAno       := mv_par04
cMes       := mv_par05  
cClaPre    := mv_par06

MsAguarde({|| R673() }, cTitulo, "", .T.)


//��������������������������������������������������������������������������Ŀ
//� Libera filtro do BD7                                                     �
//����������������������������������������������������������������������������
BD7->(DbClearFilter())
BD7->(RetIndex("BD7"))

Return


/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Programa   � R673     � Autor � Eduardo Motta         � Data � 17.11.05 ���
��������������������������������������������������������������������������Ĵ��
���Descricao  � Gera dados no TXT Analitico e Sintetico                    ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
/*/

Static Function R673()
Local cLinha
Local cTipPre
Local cCodPre
Local dDatPag := dDataBase
Local aVal := {}
Local cNomUsr
Local cMatric
Local nLote
Local cNota
Local cUniPre
Local cTipPreEx
Local cPreExe
Local cTipTab
Local cCodPro
Local cDesPro
Local dDatPro
Local nQtdPro
Local nVrGlosa
Local nVrFilme
Local nVrCusto
Local nVrHonor
Local nTotPro
Local cTipGlo
Local cMotGlo
Local nTot202 := 0
Local nTot203 := 0
Local nTot204 := 0
Local nVrTotPro := 0.00
Local nVrTotGlo := 0.00
Local nRdTotPro := 0.00
Local nRdTotGlo := 0.00
Local nTot102 := 0.00
Local nTot103 := 0.00
Local nTotPag := 0.00
Local cInd
Local cAnoMes

Local cGuia
Local nNumPag
Local cBanco
Local cAgencia
Local cConta
Local cCtUnicred
Local nValPag
Local nVrInssPF
Local nVrInssPJ
Local cMsg
Local cArqSin
Local cArqAna
Local nCont


Private nHAna
Private nHSin
Private nLinhaAna := 1
Private nLinhaSin := 1













//��������������������������������������������������������������������������Ŀ
//� Monta indregua no BD7                                                    �
//����������������������������������������������������������������������������
cFor := "BD7_FILIAL = '" + xFilial("BD7") + "' .And. "
cFor += "BD7_OPELOT = '"+cCodOpe+"' .And. SUBSTRING(BD7_NUMLOT,1,6) = '"+cAno+cMes+"' .And. "
cFor += "( BD7_CODRDA >= '" + cRdaDe    + "' .And. BD7_CODRDA <= '" + cRdaAte    + "' ) .And. "
cFor += "BD7_FASE = '4' .And. BD7_SITUAC <> '2' "
//��������������������������������������������������������������������������Ŀ
//� Monta ordem                                                              �
//����������������������������������������������������������������������������
cOrdem := "BD7_FILIAL+BD7_CODRDA+BD7_CODOPE+BD7_CODLDP+BD7_CODPEG+BD7_NUMERO+BD7_ORIMOV+BD7_SEQUEN"
//��������������������������������������������������������������������������Ŀ
//� Indregua no BD7                                                          �
//����������������������������������������������������������������������������
cInd        := CriaTrab(Nil,.F.)
BD7->(IndRegua("BD7",cInd,cOrdem,nil,cFor,nil,.T.))


//��������������������������������������������������������������������������Ŀ
//� Monta indregua no SE2                                                    �
//����������������������������������������������������������������������������
cFor := "E2_FILIAL = '" + xFilial("SE2") + "' .And. !Empty(E2_CODRDA) .and. E2_ANOBASE='"+cAno+"'  .and. E2_MESBASE='"+cMes+"'"
//��������������������������������������������������������������������������Ŀ
//� Monta ordem                                                              �
//����������������������������������������������������������������������������
cOrdem := "E2_FILIAL+E2_CODRDA"
//��������������������������������������������������������������������������Ŀ
//� Indregua no BD7                                                          �
//����������������������������������������������������������������������������
cInd := CriaTrab(Nil,.F.)
SE2->(IndRegua("SE2",cInd,cOrdem,nil,cFor,nil,.T.))
       
cArqAna :="Analitico.txt"
nHAna := FCreate(cArqAna)

cArqSin :="Sintetico.txt"
nHSin := FCreate(cArqSin)

// registro 201 - Header
cLinha := StrZero(nLinhaAna++,8)+"201"+cCodOpe+Dtos(dDataBase)+"01"
FWrite(nHAna,cLinha+chr(13)+chr(10))


// registro 101 - Header
cLinha := StrZero(nLinhaSin++,8)+"101"+cCodOpe+Dtos(dDataBase)+"01"
FWrite(nHSin,cLinha+chr(13)+chr(10))

BA1->(dbSetOrder(2))

// registro 202 - producao
//��������������������������������������������������������������������������Ŀ
//� Filtra BAU-Rede de Atendimento                                           �
//����������������������������������������������������������������������������
cSQL := " SELECT R_E_C_N_O_ BAU_RECNO FROM " + RetSQLName("BAU") 
cSQL += " WHERE BAU_FILIAL  = '" + xFilial("BAU") + "' AND "
cSQL += "       BAU_CODIGO >= '" + cRdaDe    + "' AND BAU_CODIGO <= '" + cRdaAte    + "' AND "
cSQL += "D_E_L_E_T_ = ''"

PLSQUERY(cSQL,"TrbBAU")
//�����������������������������������������������������������������������������Ŀ
//� Imprime cada RDA                                                            �
//�������������������������������������������������������������������������������
While ! TrbBAU->(Eof())
   //��������������������������������������������������������������������������Ŀ
   //� Mensagem de processamento                                                �
   //����������������������������������������������������������������������������
   MsProcTxt("Processando ... " + alltrim(BAU->BAU_NOME))
   //��������������������������������������������������������������������������Ŀ
   //� Posiciona BAU-Rede de Atendimento                                        �
   //����������������������������������������������������������������������������
   BAU->(dbGoTo(TrbBAU->BAU_RECNO))
   //��������������������������������������������������������������������������Ŀ
   //� Verifica se a RDA pertence ao grupo informado                            �
   //����������������������������������������������������������������������������
   If  ! empty(cClaPre) .and. ! BAU->BAU_TIPPRE $ cClaPre
       TrbBAU->(DbSkip())
       Loop
   Endif      
   //��������������������������������������������������������������������������Ŀ
   //� Verifica se a RDA existe para a operadora desejada                       �
   //����������������������������������������������������������������������������
   BAW->(DbSetOrder(1))
   If  ! BAW->(msSeek(xFilial("BAW")+BAU->BAU_CODIGO+cCodOpe))
       TrbBAU->(DbSkip())
       Loop
   Endif
   If !SE2->(DbSeek(xFilial()+BAU->BAU_CODIGO))
       TrbBAU->(DbSkip())
       Loop
   Endif
   SA2->(dbSetOrder(1))
   SA2->(msSeek(xFilial("SA2")+BAU->(BAU_CODSA2+BAU_LOJSA2)))
   cTipPre := RetTipPre(BAU->BAU_CODIGO) // retorna o tipo do prestador
   cCodPre := BAU->BAU_CODIGO
   dDatPag := SE2->E2_VENCREA
   cAnoMes    := cAno+cMes
   nNumPag    := 0
   cBanco     := SA2->A2_BANCO
   cAgencia   := SA2->A2_AGENCIA
   cConta     := SA2->A2_NUMCON
   cCtUnicred := space(8)
   nValPag    := SE2->E2_VALOR
   nVrInssPF  := 0.00
   nVrInssPJ  := 0.00
   cMsg       := ""
   
   cLinha := StrZero(nLinhaAna++,8) + ;
             "202" + ;
             cTipPre + ;
             StrZero(Val(cCodPre),8) + ;
             DtoS(dDatPag)
   FWrite(nHAna,cLinha+chr(13)+chr(10))
   nTot202++

   // registro 102 - pagamento
   cLinha := StrZero(nLinhaSin++,8) + ;
             "102" + ;
             cTipPre + ;
             StrZero(Val(cCodPre),8) + ;
             DtoS(dDatPag) + ;
             cAnoMes + ;
             StrZero(nNumPag,8) + ;
             cBanco + ;
             cAgencia + ;
             PadR(cConta,15) + ;
             PadR(cCtUnicred,8) + ;
             StrZero(nValPag*100,14) + ;
             StrZero(nVrInssPF*100,14) + ;
             StrZero(nVrInssPJ*100,14) + ;
             PadR(cMsg,600)
   FWrite(nHSin,cLinha+chr(13)+chr(10))
   nTot102++
   nTotPag+=nValPag

   If SE2->E2_IRRF > 0.00
      Reg103("D","2","I.R.R.F.",SE2->E2_IRRF,"",@nTot103,nHSin)
   EndIf   
   If SE2->E2_INSS > 0.00
      Reg103("D","3","I.N.S.S.",SE2->E2_INSS,"",@nTot103,nHSin)
   EndIf
   If SE2->E2_ISS > 0.00
      Reg103("D","3","I.S.S.",SE2->E2_ISS,"",@nTot103,nHSin)
   EndIf
   If SE2->E2_VRETPIS > 0.00
      Reg103("D","3","P.I.S.",SE2->E2_VRETPIS,"",@nTot103,nHSin)
   EndIf
   If SE2->E2_VRETCOF > 0.00
      Reg103("D","3","COFINS",SE2->E2_VRETCOF,"",@nTot103,nHSin)
   EndIf
   If SE2->E2_VRETCSL > 0.00
      Reg103("D","3","C.S.L.",SE2->E2_VRETCSL,"",@nTot103,nHSin)
   EndIf
   nRdTotPro := 0.00
   nRdTotGlo := 0.00
   cNomUsr := Space(25)
   BD7->(DbSeek(xFilial()+BAU->BAU_CODIGO))
   While !BD7->(Eof()) .and. BAU->BAU_FILIAL==xFilial("BD7") .and. BAU->BAU_CODIGO == BD7->BD7_CODRDA
       nVrFilme  := 0.00
       nVrCusto  := 0.00
       nVrHonor  := 0.00
       nVrGlosa  := 0.00
       cMotGlo   := ""
       If  BD7->(BD7_FILIAL+BD7_CODOPE+BD7_CODLDP+BD7_CODPEG+BD7_NUMERO+BD7_ORIMOV+BD7_SEQUEN) <> ;
           BD6->(BD6_FILIAL+BD6_CODOPE+BD6_CODLDP+BD6_CODPEG+BD6_NUMERO+BD6_ORIMOV+BD6_SEQUEN) 
           BD6->(msSeek(xFilial("BD6")+BD7->(BD7_CODOPE+BD7_CODLDP+BD7_CODPEG+BD7_NUMERO+BD7_ORIMOV+BD7_SEQUEN)))
       Endif  
       cCodPro   := BD6->BD6_CODPRO
       cDesPro   := BD6->BD6_DESPRO
       nQtdPro   := BD6->BD6_QTDPRO
       cGuia     := BD7->(BD7_CODOPE+BD7_CODLDP+BD7_CODPEG+BD7_NUMERO+BD7_ORIMOV+BD7_SEQUEN)
       While !BD7->(Eof()) .and. BAU->BAU_FILIAL==xFilial("BD7") .and. BAU->BAU_CODIGO == BD7->BD7_CODRDA .and. cGuia == BD7->(BD7_CODOPE+BD7_CODLDP+BD7_CODPEG+BD7_NUMERO+BD7_ORIMOV+BD7_SEQUEN)
          //��������������������������������������������������������������������������Ŀ
          //� Posiciona BA1-Usuarios                                                   �
          //����������������������������������������������������������������������������
          If  BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG) <> BD7->(BD7_CODOPE+BD7_CODEMP+BD7_MATRIC+BD7_TIPREG)
              BA1->(msSeek(xFilial("BA1")+BD7->(BD7_CODOPE+BD7_CODEMP+BD7_MATRIC+BD7_TIPREG)))
          Endif
          cNomUsr := PadR(BA1->BA1_NOMUSR,25)
          cMatric := BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG+BA1_DIGITO)

          nLote     := Val(BD7->BD7_LOTEAG)
          cNota     := BD7->BD7_NUMIMP  // confirmar
          cUniPre   := BD7->BD7_CODOPE
          cTipPreEx := RetTipPre(BD7->BD7_CODRDA) // retorna o tipo do prestador
          cUniPre   := BD7->BD7_CODOPE
          cPreExe   := "00" + BD7->BD7_CODRDA
          cTipTab   := "0"
          dDatPro   := BD7->BD7_DATPRO
          aVal      := RetValPro(BD7->BD7_CODUNM,Round(BD7->BD7_VLRPAG,2),BD7->BD7_BLOPAG,BD7->BD7_SITUAC,BD7->BD7_VLRGLO,BD7->BD7_BLQAUG)
          nVrFilme  += aVal[1]
          nVrCusto  += aVal[2]
          nVrHonor  += aVal[3]
          nVrGlosa  += aVal[4]
          If !Empty(aVal[5])
             cMotGlo   := aVal[5]
          EndIf   
          BD7->(DbSkip())
      EndDo    
      If (nVrFilme+nVrCusto+nVrHonor) > 0
         // registro 203 - procedimento
         nTotPro   := nVrFilme+nVrCusto+nVrHonor
         cLinha := StrZero(nLinhaAna++,8) + ;
                   "203" + ;
                   StrZero(nLote,8) + ;
                   StrZero(Val(cNota),11) + ;
                   cUniPre + ;
                   cTipPreEx + ;
                   cPreExe + ;
                   cMatric + ;
                   PadR(cNomUsr,25) + ;
                   cTipTab + ;
                   StrZero(Val(cCodPro),8) + ;
                   PadR(cDesPro,25) + ;
                   DtoS(dDatPro) + ;
                   StrZero(nQtdPro*10000,8) + ;
                   StrZero(nVrFilme*100,14) + ;
                   StrZero(nVrCusto*100,14) + ;
                   StrZero(nVrHonor*100,14) + ;
                   StrZero(nTotPro*100,14)
         FWrite(nHAna,cLinha+chr(13)+chr(10))
         nTot203++
         nVrTotPro += nTotPro
         nRdTotPro += nTotPro
      EndIf  
   EndDo
   If nRdTotPro > 0.00
      Reg103("C","1","Producao",nRdTotPro,"",@nTot103,nHSin)
   EndIf
   If nRdTotGlo > 0.00
      Reg103("D","0","Total de Glosas",nRdTotGlo,"",@nTot103,nHSin)
   EndIf
   BGQ->(DbSetOrder(2))
   BGQ->(DbSeek(xFilial("BGQ")+BAU->BAU_CODIGO+cAno+cMes+cCodOpe))
   While ! BGQ->(Eof()) .And. BGQ->(BGQ_FILIAL+BGQ_CODIGO+BGQ_ANO+BGQ_MES+BGQ_CODOPE) == ;
                              xFilial("BGQ")+BAU->BAU_CODIGO+cAno+cMes+cCodOpe
      BBB->(DbSetOrder(1))
      BBB->(DbSeek(xFilial("BBB")+BGQ->BGQ_CODLAN))
      If  BGQ->BGQ_TIPO == "1" // DEBITO
         Reg103("D","0",BBB->BBB_DESCRI,BGQ->BGQ_VALOR,"",@nTot103,nHSin)
      ElseIf  BGQ->BGQ_TIPO == "2"
         Reg103("C","0",BBB->BBB_DESCRI,BGQ->BGQ_VALOR,"",@nTot103,nHSin)
      EndIf
      BGQ->(DbSkip())
   Enddo
   TrbBAU->(DbSkip())
EndDo
TrbBAU->(DbCloseArea())
// registro 209 - Trailler
cLinha := StrZero(nLinhaAna++,8)+"209"+StrZero(nTot202,6)+StrZero(nTot203,6)+StrZero(nTot204,6)+StrZero(nVrTotPro*100,14)+StrZero(nVrTotGlo*100,14)
FWrite(nHAna,cLinha+chr(13)+chr(10))

// registro 109 - Trailler
cLinha := StrZero(nLinhaSin++,8)+"109"+StrZero(nTot102,6)+StrZero(nTot103,6)+StrZero(nTotPag*100,14)
FWrite(nHSin,cLinha+chr(13)+chr(10))

FClose(nHAna)
FClose(nHSin)

nCont := 1
While .t.
   cArqAna :="A"+StrZero(Year(dDatPag),4)+StrZero(Month(dDatPag),2)+StrZero(Day(dDatPag),2)+StrZero(nCont,2)+"."+SubStr(cCodOpe,2,3)
   If !File(cArqAna)
      fRename("Analitico.txt",cArqAna)
      Exit
   EndIf
   nCont++
EndDo   

nCont := 1
While .t.
   cArqSin :="S"+StrZero(Year(dDatPag),4)+StrZero(Month(dDatPag),2)+StrZero(Day(dDatPag),2)+StrZero(nCont,2)+"."+SubStr(cCodOpe,2,3)
   If !File(cArqSin)
      fRename("Sintetico.txt",cArqSin)
      Exit
   EndIf
   nCont++
EndDo   

MsgStop("Arquivos gerados. "+cArqAna+"/"+cArqSin)

Return

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Programa   � CriaSX1   � Autor � Angelo Sperandio     � Data � 03.02.05 ���
��������������������������������������������������������������������������Ĵ��
���Descricao  � Atualiza SX1                                               ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
/*/

Static Function CriaSX1()

Local aRegs	:=	{}

aadd(aRegs,{"PLA673","01","Operadora"              ,"","","mv_ch2","C", 4,0,0,"G","","mv_par01",""            ,"","","","",""               ,"","","","",""              ,"","","","",""       ,"","","","","","","","","B89",""})
aadd(aRegs,{"PLA673","02","RDA De"                 ,"","","mv_ch3","C", 6,0,0,"G","","mv_par02",""            ,"","","","",""               ,"","","","",""              ,"","","","",""       ,"","","","","","","","","BA0",""})
aadd(aRegs,{"PLA673","03","RDA Ate"                ,"","","mv_ch4","C", 6,0,0,"G","","mv_par03",""            ,"","","","",""               ,"","","","",""              ,"","","","",""       ,"","","","","","","","","BA0",""})
aadd(aRegs,{"PLA673","04","Ano Base"               ,"","","mv_ch5","C", 4,0,0,"G","","mv_par04",""            ,"","","","",""               ,"","","","",""              ,"","","","",""       ,"","","","","","","","",""   ,""})
aadd(aRegs,{"PLA673","05","Mes Base"               ,"","","mv_ch6","C", 2,0,0,"G","","mv_par05",""            ,"","","","",""               ,"","","","",""              ,"","","","",""       ,"","","","","","","","",""   ,""})
aadd(aRegs,{"PLA673","06","Classes RDA"            ,"","","mv_ch7","C",30,0,0,"G","","mv_par06",""            ,"","","","",""               ,"","","","",""              ,"","","","",""       ,"","","","","","","","",""   ,""})

PlsVldPerg( aRegs )
    
Return

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Programa   � RetTipPre � Autor � Eduardo Motta        � Data � 15.11.05 ���
��������������������������������������������������������������������������Ĵ��
���Descricao  � Retorna o tipo do prestador                                ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
/*/
Static Function RetTipPre(cCodigo)
Local cCodCre
Local cTipPre
Local aArea := sGetArea()

sGetArea(aArea,"BAU")
BAU->(DbSetOrder(1))
BAU->(DbSeek(xFilial("BAU")+cCodigo))
cCodCre := BAU->BAU_COPCRE
cTipPre := BAU->BAU_TIPPRE
If cCodCre == "2" .OR. cCodCre == "3" // CREDENCIADO OU FUNCIONARIO
   If cTipPre == "HOS"  // HOSPITAL
      cTipPre := "02"
   ElseIf SubStr(cTipPre,1,2) == "LA"  // LABORATORIO
      cTipPre := "03"
   ElseIf cTipPre == "CLD"  // CLINICA DE DIAGNOSTICO
      cTipPre := "06"
   ElseIf SubStr(cTipPre,1,2) == "CL"  // se for Clinica
      cTipPre := "04"
   Else
      cTipPre  := "07"
   EndIf
ElseIf cCodCre == "4"  // NAO FUNCIONARIO
   cTipPre := "05"
ElseIf cCodCre == "1"  // COOPERADO      
   cTipPre := "01"
EndIf
sRestArea(aArea)
Return cTipPre

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Programa   � RetTipPre � Autor � Eduardo Motta        � Data � 15.11.05 ���
��������������������������������������������������������������������������Ĵ��
���Descricao  � Retorna o valores do procedimento                          ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
/*/

Static Function RetValPro(cCodUnm,nVlrPag,cBloPag,cSituac,nVlrGlc,cBlqAug)
Local nVrFilme  := 0.00
Local nVrCusto  := 0.00
Local nVrHonor  := 0.00
Local nVrGlosa  := 0.00
Local cMotGlo   := ""
Local aRet := {}


nVlrPag    := IF(BAU->BAU_PAGPRO $ "1, ",round(nVlrPag,2),0) 
nVlrGlosa  := IF(BAU->BAU_PAGPRO $ "1, ",round(nVlrGlc,2),0)
If  BAU->BAU_PAGPRO $ "1, " .and. (cBloPag == "1" .OR. cSituac == "3")
   nVlrGlosa := nVlrPag
Endif

/*
If cBloPag == '1' // bloqueado pelo BD7
   nVrGlosa := nVlrPag
   If  cBlqAug $ "1,2" 
      cDesBlo := "BLOQUEADO PELA ROTINA DE AUTO-GERADO"
   Else
      cDesBlo := BD7->BD7_DESBLO
   Endif
ElseIf cSituac = '3' // bloqueado pelo menu
   nVrGlosa := nVlrPag
   cMotGlo  := BD7->BD7_DESBLO
Else
   nVrGlosa := nVlrGlc
   cMotGlo  := BD7->BD7_DESBLO
EndIf
*/

If cCodUnm == "FIL"
   nVrFilme := nVlrPag-nVrGlosa
ElseIf cCodUnm $ "AUX_AUR_CIR_HM _HMR_PA _PAP_PAR_PPM"   
   nVrHonor := nVlrPag-nVrGlosa
Else
   nVrCusto := nVlrPag-nVrGlosa
EndIf

aRet := {nVrFilme,nVrCusto,nVrHonor,nVrGlosa,cMotGlo}
Return aRet

Static Function Reg103(cTipLan,cTipCat,cDesLan,nVrLan,cObs,nTot103,nH)

// registro 103 - lancamento
cLinha := StrZero(nLinhaSin++,8)+"103"+cTipLan+cTipCat+PadR(cDesLan,30)+StrZero(nVrLan*100,14)+PadR(cObs,200)
FWrite(nH,cLinha+chr(13)+chr(10))
nTot103++
      
Return      
