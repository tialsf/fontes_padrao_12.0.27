#include "PROTHEUS.CH"
#include "PLSMGER.CH"
/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    � PLSA781 � Autor � Eduardo Motta          � Data � 30.06.05 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Devolucao do arquivo SIB                                   ����
�������������������������������������������������������������������������Ĵ���
���Sintaxe   � PLSA781()                                                  ����
�������������������������������������������������������������������������Ĵ���
��� Uso      � Advanced Protheus                                          ����
�������������������������������������������������������������������������Ĵ���
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/

Function PLSA781()

//��������������������������������������������������������������������������Ŀ
//� Inicializa variaveis                                                     �
//����������������������������������������������������������������������������
Local nOpca       := 0
Local aSays       := {}, aButtons := {}
Private cCadastro := "Arquivo de devolucao de registro de dados de Beneficiarios"
Private cPerg     := "PLS781"
Private dData
Private cArq
Private nProc

//��������������������������������������������������������������������������Ŀ
//� Verifica se campos novos ja foram criados                                �
//����������������������������������������������������������������������������
If  BA1->(FieldPos("BA1_INCANS")) == 0 .or. ;
    BA1->(FieldPos("BA1_EXCANS")) == 0 .or. ;
    BA1->(FieldPos("BA1_ENVANS")) == 0 .or. ;
    BA1->(FieldPos("BA1_CODCCO")) == 0 .or. ;
    BRP->(FieldPos("BRP_CODSIB")) == 0 .or. ;
    BQC->(FieldPos("BQC_CNPJ"))   == 0
	msgalert("Campos necessarios a esta rotina nao encontrados: BA1_INCANS, BA1_ENVANS, BRP_CODSIB, BQC_CNPJ","Campos inexistentes")
	Return()
Endif	
//��������������������������������������������������������������������������Ŀ
//� Monta texto para janela de processamento                                 �
//����������������������������������������������������������������������������
aadd(aSays,"Esta rotina ira efetuar o processamento do arquivo de retorno de criticas")
aadd(aSays,"do SIB, atualizando o status de cada usuario referente ao SIB.")
//��������������������������������������������������������������������������Ŀ
//� Monta botoes para janela de processamento                                �
//����������������������������������������������������������������������������
AADD(aButtons, { 5,.T.,{|| Pergunte(cPerg,.T. ) } } )
AADD(aButtons, { 1,.T.,{|| nOpca:= 1, If( ConaOk().and.Ver_Perg(), FechaBatch(), nOpca:=0 ) }} )
AADD(aButtons, { 2,.T.,{|| FechaBatch() }} )
//��������������������������������������������������������������������������Ŀ
//� Exibe janela de processamento                                            �
//����������������������������������������������������������������������������
FormBatch(cCadastro,aSays,aButtons)
//��������������������������������������������������������������������������Ŀ
//� Processa importacao do arquivo texto                                     �
//����������������������������������������������������������������������������
If  nOpca == 1
    Processa( {||ProcSIB() },"Retorno de criticas SIB","Processando...",.T. )                  
Endif
//��������������������������������������������������������������������������Ŀ
//� Fim da funcao                                                            �
//����������������������������������������������������������������������������
Return()

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � Ver_Perg    � Autor � Angelo Sperandio   � Data � 26/02/04 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Verifica se parametros estao ok                            ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � Ver_Perg()                                                 ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � nenhum                                                     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

Static Function Ver_perg()

//��������������������������������������������������������������������������Ŀ
//� Inicializa variaveis                                                     �
//����������������������������������������������������������������������������
lRet := .T.
//��������������������������������������������������������������������������Ŀ
//� Atualiza parametros                                                      �
//����������������������������������������������������������������������������
Pergunte(cPerg,.F.)
dData := mv_par01
cArq  := alltrim(mv_par02)
nProc := mv_par03
//���������������������������������������������������������������������Ŀ
//� Verifica se informou a operadora                                    �
//�����������������������������������������������������������������������
If  ! File(cArq)
    MsgAlert("Arquivo nao encontrado.")
    lRet := .F.
EndIf
//��������������������������������������������������������������������������Ŀ
//� Fim da funcao                                                            �
//����������������������������������������������������������������������������
Return(lRet)

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ���
���Funcao    � PROC    � Autor � Eduardo Motta          � Data � 30.06.05 ����
�������������������������������������������������������������������������Ĵ���
���Descricao � Processa devolucao do arquivo SIB                          ����
�������������������������������������������������������������������������Ĵ���
���Sintaxe   � PROC(dData,cArq)                                           ����
�������������������������������������������������������������������������Ĵ���
��� Uso      � Advanced Protheus                                          ����
�������������������������������������������������������������������������Ĵ���
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/

Static Function ProcSIB()

//���������������������������������������������������������������������Ŀ
//� Inicializa variaveis                                                �
//�����������������������������������������������������������������������
Local cLinha
Local cUsuario
Local cSeq 
Local cTip
Local aErro := {}
Local aCCO := {}
Local cSql
Local nI,nJ
Local lAchou
Local aMatPesq := {}
Local cMatAnt
Local aCrit := {}
Local cCritica
Local cMatCri
Local aCabec    := { {"Matricula PLS","@!",60},{"Matricula Criticada","@!",60},{"Nome do Usuario","@!",150},{"Linha","@!",30},{"Critica","@!",150} }
Local nTotBytes,nH
Local nTotReg
Local cWhere
//���������������������������������������������������������������������Ŀ
//� Calcula a qtd de registros a serem processados                      �
//�����������������������������������������������������������������������
nH := FOpen(cArq)
nTotBytes := FSeek(nH,0,2)
FClose(nH)
nTotReg := ((nTotBytes - 112)/41)+1
//���������������������������������������������������������������������Ŀ
//� Calcula a qtd de registros a serem processados                      �
//�����������������������������������������������������������������������
cWhere := " ( BA1_FILIAL = '"+xFilial("BA1")+"' ) AND "
cWhere += " ( BA1_ENVANS = '"+Dtos(dData)+"' ) AND "
If nProc <> 3
	cWhere += " ( BA1_LOCSIB = '3' OR BA1_LOCSIB = '4' OR BA1_LOCSIB = '5' OR BA1_LOCSIB = '6' OR BA1_LOCSIB = '7' OR BA1_LOCSIB = '8' ) AND D_E_L_E_T_ = ' '"
Else
	cWhere += " BA1_LOCSIB >= '1' AND D_E_L_E_T_ = ' ' "  
EndIf

cSQL := "SELECT COUNT(BA1_FILIAL) AS TOTREG FROM "+RetSQLName("BA1")+" WHERE "+cWhere
If  Select("TRBBA1") > 0
    TRBBA1->(DbCloseArea())
EndIf
PLSQuery(cSQL,"TRBBA1")
nTotReg+=TRBBA1->TOTREG
TRBBA1->(DbCloseArea())
//���������������������������������������������������������������������Ŀ
//� Monta array com registros criticados                                �
//�����������������������������������������������������������������������
FT_FUSE(cArq)
FT_FGOTOP()
cSeqAnt:=Space(7)
While !FT_FEOF()
   IncProc()
   cLinha := FT_FREADLN()
   If nProc <> 3 .and. SubStr(cLinha,8,1) $ "1,2,7,8"  // Identifica que a linha e' dos dados de usuario
       cSeq     := SubStr(cLinha,01,07)
		If cSeq <> cSeqAnt
	   		cTip     := SubStr(cLinha,08,01)
			If  SubStr(cLinha,8,1) == "1"     	
	      		cUsuario := SubStr(cLinha,11,30)
		    ElseIf SubStr(cLinha,8,1) $ "2,7"
		   		cUsuario := SubStr(cLinha,11,12)
			ElseIf SubStr(cLinha,8,1) == "8"
				cUsuario := SubStr(cLinha,09,12)
			Endif 
			cNumCamp := SubStr(cLinha,21,2) 
		
		Else
       		cDesMsg  := AllTrim(SubStr(cLinha,96,40))
       		cDesCau  := AllTrim(SubStr(cLinha,136,70))
       		aadd(aErro,{cUsuario,cNumCamp,cDesMsg,cDesCau,cSeq,cTip,.F.})
		Endif 	
   Else
       cUsuario := SubStr(cLinha,11,30)
       cCCO    	:= SubStr(cLinha,149,12)
       aadd(aCCO,{AllTrim(cUsuario),cCCO,.F.})  
   EndIf 
   cSeqAnt:=cSeq
   FT_FSKIP()   
EndDo                  

FT_FUSE()
//���������������������������������������������������������������������Ŀ
//� Query para selecao de usuarios do BA1                               �
//�����������������������������������������������������������������������
cSQL := "SELECT R_E_C_N_O_ AS REG FROM "+RetSQLName("BA1")+" WHERE "+cWhere
PLSQuery(cSQL,"TRBBA1")
//���������������������������������������������������������������������Ŀ
//� Processa BA1                                                        �
//�����������������������������������������������������������������������
While ! TRBBA1->(Eof())
   //���������������������������������������������������������������������Ŀ
   //� Movimenta regua                                                     �
   //�����������������������������������������������������������������������
   IncProc()
   //���������������������������������������������������������������������Ŀ
   //� Posiciona BA1                                                       �
   //�����������������������������������������������������������������������
   BA1->(DbGoto(TRBBA1->REG))
   //���������������������������������������������������������������������Ŀ
   //� Monta array com matricula microsiga e matricula anterior            �
   //� com e sem digito verificador                                        �
   //�����������������������������������������������������������������������
   aMatPesq := {}
   If nProc <> 3 
       If !Empty(BA1->BA1_CODCCO)
	       aadd(aMatPesq,BA1->BA1_CODCCO)
       Endif
       aadd(aMatPesq,BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG))
	   aadd(aMatPesq,BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG+BA1_DIGITO))
   Else
	   If  ! Empty(BA1->BA1_MATANT)
	       cMatAnt := AllTrim(BA1->BA1_MATANT)
	       aadd(aMatPesq,SubStr(cMatAnt,1,Len(cMatAnt)-1))
	       aadd(aMatPesq,cMatAnt)
	   EndIf
	   aadd(aMatPesq,BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG))
	   aadd(aMatPesq,BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG+BA1_DIGITO))
   EndIf
   //���������������������������������������������������������������������Ŀ
   //� Verifica se o usuario esta no array de registros criticados         �
   //�����������������������������������������������������������������������
   lAchou   := .F.
   cCritica := ""
   cMatCri  := ""
   cLinha   := ""
   For nJ := 1 to Len(aMatPesq)
   	   If nProc <> 3    
	       For nI := 1 to Len(aErro)
	           If  alltrim(aErro[nI,1]) == alltrim(aMatPesq[nJ])
	               lAchou   := .T.
	               cCritica := aErro[nI,3]
	               cMatCri  := aMatPesq[nJ]
	               cLinha   := aErro[nI,5]
	               aErro[nI,7] := .T. // flag indicando que erro foi processado
					IF BA1->BA1_LOCSIB $"3,6"
					
						Reclock("BA1",.F.)
						BA1->BA1_LOCSIB := "6"
						MsUnLock()	              
	              	              
	              	Elseif BA1->BA1_LOCSIB $"4,7"
	              
			            Reclock("BA1",.F.)
						BA1->BA1_LOCSIB := "7"
						MsUnLock()	
	              
	              	Elseif  BA1->BA1_LOCSIB $"5,8"
	 	            
	 	                Reclock("BA1",.F.)
						BA1->BA1_LOCSIB := "8"
						MsUnLock()	
	              	Endif 
	              	               
	               Exit
	           EndIf
	       Next
	   Else
	       For nI := 1 to Len(aCCO)
	           If  alltrim(aCCO[nI,1]) == alltrim(aMatPesq[nJ])
	               lAchou   := .T.
	               cCCO	    := aCCO[nI,2]
	               cMatCri  := aMatPesq[nJ]
	               aCCO[nI,3] := .T. // flag indicando que erro foi processado
	               Exit
	           EndIf
	       Next
	   
	   EndIf
       If  lAchou
           Exit
       EndIf   
   Next
   //���������������������������������������������������������������������Ŀ
   //� Verifica se o usuario esta no array de registros criticados         �
   //�����������������������������������������������������������������������
   If  lAchou .and. nProc <> 3
       aadd(aCrit,{transform(BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG+BA1_DIGITO),"@R !!!!.!!!!.!!!!!!.!!-!"),;
       				cMatCri,;
       				BA1->BA1_NOMUSR,;
       				cLinha,;
     				cCritica})
   
   ElseIf !lAchou .and. nProc <> 3 
       If     BA1->BA1_LOCSIB $ "3,6" // usuario enviado por inclusao
              aadd(aCrit,{transform(BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG+BA1_DIGITO),"@R !!!!.!!!!.!!!!!!.!!-!"),"",BA1->BA1_NOMUSR,"","Usuario incluido"})
              If  nProc == 2 // atualizar
                  BA1->(RecLock("BA1",.F.))
                  BA1->BA1_LOCSIB := "1" // Ativo na ANS
                  BA1->(MsUnlock())
              Endif
       ElseIf BA1->BA1_LOCSIB $ "4,7" // usuario enviado por alteracao
              aadd(aCrit,{transform(BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG+BA1_DIGITO),"@R !!!!.!!!!.!!!!!!.!!-!"),"",BA1->BA1_NOMUSR,"","Usuario alterado"})
              If  nProc == 2 // atualizar
                  BA1->(RecLock("BA1",.F.))
                  BA1->BA1_LOCSIB := "1" // Ativo na ANS
                  BA1->(MsUnlock())
              Endif
       ElseIf BA1->BA1_LOCSIB $ "5,8" // usuario enviado por exclusao
              aadd(aCrit,{transform(BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG+BA1_DIGITO),"@R !!!!.!!!!.!!!!!!.!!-!"),"",BA1->BA1_NOMUSR,"","Usuario excluido"})
              If  nProc == 2 // atualizar
                  BA1->(RecLock("BA1",.F.))
                  BA1->BA1_LOCSIB := "2" // Usuario Excluido
                  BA1->(MsUnlock())
              Endif
       EndIf
   ElseIf lAchou .and. nProc == 3 
   	   aadd(aCrit,{transform(BA1->(BA1_CODINT+BA1_CODEMP+BA1_MATRIC+BA1_TIPREG+BA1_DIGITO),"@R !!!!.!!!!.!!!!!!.!!-!"),"",BA1->BA1_NOMUSR,"","Atualizado CCO"})
       BA1->(RecLock("BA1",.F.))
       BA1->BA1_CODCCO := cCCO 
       BA1->(MsUnlock())       
   EndIf
   //���������������������������������������������������������������������Ŀ
   //� Acessa proximo registro                                             �
   //�����������������������������������������������������������������������
   TRBBA1->(DbSkip())
EndDo
//���������������������������������������������������������������������Ŀ
//� Fecha arquivo de trabalho                                           �
//�����������������������������������������������������������������������
TRBBA1->(DbCloseArea())
//���������������������������������������������������������������������Ŀ
//� Verifica se houve algum registro nao processado                     �
//�����������������������������������������������������������������������
For nI := 1 to Len(aErro)
    If  ! aErro[nI,7]  // se erro nao foi processado critica
        aadd(aCrit,{"",aErro[nI,1],"",aErro[nI,5],"Linha nao processada. Verifique."})
    EndIf
Next
For nI := 1 to Len(aCCO)
    If  ! aCCO[nI,3]  // se erro nao foi processado critica
        aadd(aCrit,{"",aCCO[nI,1],"",aCCO[nI,2],"Beneficiario nao processada. Verifique."})
    EndIf
Next
//���������������������������������������������������������������������Ŀ
//� Exibe mensagens de critica                                          �
//�����������������������������������������������������������������������
If  Len(aCrit) = 0
	aadd(aCrit,{"","","Arquivo Inv�lido","",""})
Endif
PlsCriGen(aCrit,aCabec,"Devolucao de registro de dados de Beneficiarios")
//���������������������������������������������������������������������Ŀ
//� Fim da funcao                                                       �
//�����������������������������������������������������������������������
Return