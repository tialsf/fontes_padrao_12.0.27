
#INCLUDE "PlsR580.ch"
#include "PROTHEUS.CH"
#include "PLSMGER.CH"
#define STR0001  "Gerado o arquivo corretamente "
#define STR0002  "Gera��o do arquivo dos boletos de cobranca"
/*/
Pendencias:
1) Tipo de Registro 02-Titulos
a) Definir o tipo de dados a ser gravado no item Tipo da cobranca F - Fechada, A - Aberta
b) Confirmar o momento de gravacao do campo "Nosso numero"

������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Funcao    � PLSA820 � Autor � Marcos Alves           � Data � 13/05/04  ���
��������������������������������������������������������������������������Ĵ��
���Descricao � Geracao do arquivo de boleto 							   ���
��������������������������������������������������������������������������Ĵ��
���Sintaxe   � PLSA820                                                     ���
��������������������������������������������������������������������������Ĵ��
��� Uso      � Advanced Protheus                                           ���
��������������������������������������������������������������������������Ĵ��
������������������������������������������������������������������������������
/*/
Function PLSA820()
					
PRIVATE cAlias      := "SE1"
PRIVATE cPerg       := "PLA820"
PRIVATE aRegs 		:=	{}
//��������������������������������������������������������������������������Ŀ
//� Testa ambiente do relatorio somente top...                               �
//����������������������������������������������������������������������������
If ! PLSRelTop()
	Return .F.
Endif
//�����������������������������������������������Ŀ
//� Parametros usados na rotina                   �
//� mv_par01         Bodero                       �
//�������������������������������������������������
If  ! Pergunte( cPerg, .T. )
    Return()
Endif    
//�����������������������������������������������������������������������������Ŀ
//� Variaveis utilizadas para parametros                         				�
//� mv_par01 // numero do bordero                                 				�
//�������������������������������������������������������������������������������
If !empty(mv_par01)
	MsAguarde({|| PL820Arq() }, STR0002, "", .T.) //"Gera��o do arquivo dos boletos de cobranca"
EndIf	
//��������������������������������������������������������������������������Ŀ
//� Fim da rotina                                                            �
//����������������������������������������������������������������������������
Return

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Programa   � A820Arq  � Autor � Marcos Alves          � Data � 13.05.04 ���
��������������������������������������������������������������������������Ĵ��
���Descricao  � Imprime detalhe do relatorio...                            ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
/*/
Static Function PL820Arq()
//��������������������������������������������������������������������������Ŀ
//� Define variaveis...                                                      �
//����������������������������������������������������������������������������
LOCAL cSQL			:= ""							// Select deste relatorio
Local cPrefixo 		:= ""							// Prefixo do titulo
Local cTitulo 		:= ""							// Titulo
Local cParcela 		:= ""							// Parcela do titulo
Local cTipo 		:= ""							// E1_TIPO
Local aDadosEmp		:= {}							// Array com os dados da empresa.
Local aDadosTit		:= {}							// Array com os dados do titulo			
Local aDadosBanco   := {}							// Array com os dados do banco
Local aDatSacado	:= {}							// Array com os dados do Sacado.
Local cOperadora    := BX4->(PLSINTPAD())			// Retorna a operadora do usuario.
Local aBfq			:= {}
Local aCobranca  	:= {}
Local aDependentes	:= {}							// Array com os dependentes do sacado.
Local aOpenMonth	:= {}							// Array com os meses em aberto.
Local aObservacoes  := {}							// Array com as observacoes do extrato.
Local aMsgBoleto    := {}							// Array com as mensagens do boleto.
Local cUsuAnt       := ""
Local lNome			:= .T.
Local aTrailler		:= {0,0}						// Array para gerar o Triller do arquivo de boleto
Local cStartPath := GetSrvProfString("Startpath","")

PRIVATE cArq		:= CriaTrab(,.F.)+".TXT"
PRIVATE nRegBol		:= 0

SE1->(DbSetOrder(1))
//��������������������������������������������������������������������������Ŀ
//� Monta query...                                                           �
//����������������������������������������������������������������������������
cSQL := "SELECT * "
cSQL += "FROM "+RetSQLName("SE1")+" SE1 "
cSQL += "WHERE "
cSQL += "E1_FILIAL = '" + xFilial("SE1") + "' AND "
cSQL += "E1_NUMBOR = '" + mv_par01  + "' AND "
cSQL += "SE1.D_E_L_E_T_ = '' "
cSQL += "ORDER BY " + SE1->(IndexKey())

PLSQuery(cSQL,"P820")

If P820->(Eof())
	P820->(DbCloseArea())
	Help("",1,"RECNO")
	Return
Endif

//��������������������������������������������������������������������������Ŀ
//� Exibe mensagem...                                                        �
//����������������������������������������������������������������������������
MsProcTxt(PLSTR0001)//#include "PLSMGER.CH" - "Buscando dados no servidor..."

BA0->(DbSeek(xFilial("BA0") + cOperadora))
SE1->(DbSetOrder(1))
BM1->(DbSetOrder(4))
BA1->(DbSetOrder(2))

aDadosEmp	:= {	BA0->BA0_CODIDE+BA0->BA0_CODINT                                           	,; //Codigo da Operadora
					BA0->BA0_NOMINT                                                           	,; //Nome da Empresa
					BA0->BA0_END                                                              	,; //Endere�o
					BA0->BA0_BAIRRO+" "+BA0->BA0_CIDADE+" "+BA0->BA0_EST                  	    ,; //Complemento
					Subs(BA0->BA0_CEP,1,5)+"-"+Subs(BA0->BA0_CEP,6,3)             		        ,; //CEP //"CEP: "
					BA0->BA0_TELEF1                                                             ,;//Telefones //"PABX/FAX: "
					Subs(BA0->BA0_CGC,1,2)+"."+Subs(BA0->BA0_CGC,3,3)+"."+                     ;//"CNPJ.: "
					Subs(BA0->BA0_CGC,6,3)+"/"+Subs(BA0->BA0_CGC,9,4)+"-"+                     ;
					Subs(BA0->BA0_CGC,13,2)                                                    	,; //CGC
					SE1->E1_PORTADO                                                    			,; // Banco do Bordero
					SE1->E1_AGEDEP                                                     			,; // Agencia do bordero
					SE1->E1_CONTA                                                      			,; // Conta corrente    
					BA0->BA0_SUSEP }  															    //I.E //"ANS : "
//�����������������������������������������������������������������Ŀ
//�Gravacao do trailler no arquivo de boletos                       �
//�������������������������������������������������������������������
PL820Header(aDadosEmp)
//��������������������������������������������
//�Inicia laco para a geracao do arquivo    .�
//��������������������������������������������
While ! P820->(Eof())
	aDadosTit	  := {}							// Array com os dados do titulo			
	aDadosBanco   := {}							// Array com os dados do banco
	aDatSacado	  := {}							// Array com os dados do Sacado.
	aCobranca     := {}
	aDependentes  := {}							// Array com os dependentes do sacado.
	aObservacoes  := {}							// Array com as observacoes do extrato.
	aMsgBoleto    := {}							// Array com as mensagens do boleto.

	cPrefixo  	  := P820->E1_PREFIXO
	cTitulo 	  := P820->E1_NUM
	cParcela      := P820->E1_PARCELA
	cTipo 	      := P820->E1_TIPO
	
	BM1->(DbSeek(xFilial("BM1") + cPrefixo + cTitulo + cParcela + cTipo))
	BA3->(DbSeek(xFilial("BA3") + BM1->BM1_CODINT + BM1->BM1_CODEMP + BM1->BM1_MATRIC))

	//�����������������������������������������������������Ŀ
	//�Em caso de pessoa juridica, nao emite os dependentes.�
	//�������������������������������������������������������
	If BA3->BA3_TIPOUS == '1' //Pessoa Fisica
		//����������������������������������������������Ŀ
		//�Busca de dependentes para armazenar  os dados.�
		//������������������������������������������������
		DbSelectArea("BA1")
		If DbSeek(xFilial("BA1") + BM1->BM1_CODINT + BM1->BM1_CODEMP + BM1->BM1_MATRIC)
			
			While 	(!EOF() .And.;
					xFilial("BA1")  == BA1->BA1_FILIAL .And.;
					BA1->BA1_CODINT == BM1->BM1_CODINT .And.;
					BA1->BA1_CODEMP == BM1->BM1_CODEMP .And.;
					BA1->BA1_MATRIC == BM1->BM1_MATRIC)
					
					
	
					//��������������������������������������������������������������Ŀ
					//�Identifica os usuarios que fazem parte do titulo posicionado. �
					//����������������������������������������������������������������
					If 	(SubStr(DtoS(BA1->BA1_DATINC),1,6) >= P820->E1_ANOBASE+P820->E1_MESBASE) .and. ;
						(Empty(BA1->BA1_MOTBLO))
					
						Aadd(aDependentes, {BA1->BA1_TIPREG,BA1->BA1_NOMUSR})
					EndIf	
						
				DbSkip()
			End
		EndIf
	EndIf
	DbSelectArea("P820")
	
	//�������������������������������������Ŀ
	//�Retorna os meses em aberto do sacado.�
	//���������������������������������������
	aOpenMonth:=PL820MES(P820->E1_CLIENTE,P820->E1_LOJA,P820->E1_MESBASE,P820->E1_ANOBASE)
	
	While 	!BM1->(Eof()) 					.And.;
			BM1->BM1_PREFIX = cPrefixo 		.And.;
			BM1->BM1_NUMTIT = cTitulo 		.And.;
			BM1->BM1_PARCEL = cParcela 		.And.;
			BM1->BM1_TIPTIT = cTipo
		
			If BM1->BM1_CODINT+BM1->BM1_CODEMP+BM1->BM1_MATRIC+BM1->BM1_TIPREG == cUsuAnt
				lNome := .F.
			Else
			    cUsuAnt := BM1->BM1_CODINT+BM1->BM1_CODEMP+BM1->BM1_MATRIC+BM1->BM1_TIPREG
			    lNome:= .T.
			EndIf
			
			Aadd(aCobranca,{	BM1->BM1_TIPREG,;
								BM1->BM1_NOMUSR,;
		  						"",;
		  						"",;
		  						"",;
		  						BM1->BM1_CODTIP,;
		  						BM1->BM1_DESTIP,;
		  						Iif(BM1->BM1_CODTIP $ "104;116;120;121;123;124",0,BM1->BM1_VALOR),;
		  						{}})
		    
		    If BM1->BM1_CODTIP $ "104;116;120;121;123;124"		
			
				DbSelectArea("BD6")
				DbSetOrder(5)
				If MsSeek(xFilial("BD6")+BM1->BM1_CODINT+BM1->BM1_CODEMP+BM1->BM1_MATRIC+BM1->BM1_TIPREG)
					While 	!EOF() 					 				.AND.;
							 BM1->BM1_CODINT == BD6->BD6_OPEUSR 	.AND.;
							 BM1->BM1_CODEMP == BD6->BD6_CODEMP		.AND.;
							 BM1->BM1_MATRIC == BD6->BD6_MATRIC		.AND.;
							 BM1->BM1_TIPREG == BD6->BD6_TIPREG
					
						//�������������������������������������������������������Ŀ
						//�Efetua laco no BD6 para selecionar os dados do extrato.�
						//�Armazena IDUSR para posterior ordenacao por este campo.�
						//�                                                       �
						//���������������������������������������������������������

		   				If 	BD6->BD6_VLRTPF > 0 					.AND.;
		   			 		BD6->BD6_ANOPAG = P820->E1_ANOBASE 	.AND.;
		   			 		BD6->BD6_MESPAG = P820->E1_MESBASE 
		    		    
		    		   		Aadd(aCobranca[Len(aCobranca)][9],{BM1->BM1_TIPREG,;
					    									BM1->BM1_NOMUSR,;
					    									BD6->BD6_CODRDA,;
					    									BD6->BD6_NOMRDA,;
					    									BD6->BD6_DATPRO,;
					    									BD6->BD6_NUMERO,;
					    									BD6->BD6_CODPRO,;
					    									BD6->BD6_DESPRO,;
					    									BD6->BD6_QTDPRO,;
					    									BD6->BD6_VLRTPF,;
					    									BD6->BD6_IDUSR,;
					    									BM1->BM1_TIPO})
		    			EndIf
		    	
		    		BD6->(DbSkip())
		   			End
		    	EndIf
	    	EndIf
		BM1->(DbSkip())
	End
	
	aBfq := RetornaBfq(P820->E1_CODINT, "199")
	If P820->E1_IRRF > 0
			Aadd(aCobranca,{	"",;
								"",;
		  						"",;
		  						"",;
		  						"",;
		  						aBfq[1],;
		  						aBfq[2],;
		  						P820->E1_IRRF,;
		  						{}})
	Endif

	//��������������������������������
	//�Carrega mensagens para boleto.�
	//��������������������������������
	aObservacoes := PL820TEXT(2					,P820->E1_CODINT	,P820->E1_CODEMP,P820->E1_CONEMP	,;
	P820->E1_SUBCON	,P820->E1_MATRIC ,P820->E1_ANOBASE+P820->E1_MESBASE)
	
	
	aMsgBoleto   := PL820TEXT(1					,P820->E1_CODINT	,P820->E1_CODEMP,P820->E1_CONEMP	,;
	P820->E1_SUBCON	,P820->E1_MATRIC ,P820->E1_ANOBASE+P820->E1_MESBASE)
	
	//�����������������Ŀ
	//�Dados do Trailler�
	//�������������������
	aTrailler[1]+=1 								//[1]Quantidade de titulos
	aTrailler[2]+= P820->E1_SALDO - P820->E1_IRRF	//[2]Total dos titulos
	
	//���������������Ŀ
	//�Dados do Titulo�
	//�����������������
	aDadosTit   :=  {	AllTrim(P820->E1_NUM)+AllTrim(P820->E1_PARCELA),; 	//[1]Numero do t�tulo
						P820->E1_EMISSAO,;             		   				//[2]Data da emiss�o do t�tulo
						dDataBase,;             							//[3]Data da emiss�o do boleto
						P820->E1_VENCTO,;             						//[4]Data do vencimento
						P820->E1_SALDO - P820->E1_IRRF,;					//[4]Valor do t�tulo
						"XXXXXX"       ,;	    							//[6]Nosso numero (Ver f�rmula para calculo)
						P820->E1_MESBASE       ,;							//[7]Mes de competencia
						P820->E1_ANOBASE       ,;							//[8]Ano de competencia
						P820->E1_CODINT+P820->E1_CODEMP+P820->E1_MATRIC}	//[9]Matricula familia
	//���������������Ŀ
	//�Dados Sacado   �
	//�����������������
	aDatSacado   := {	SA1->A1_NOME                            ,;//[1]Raz�o Social
						SA1->A1_COD                             ,;//[2]C�digo
						SA1->A1_END+" - "+SA1->A1_BAIRRO       ,;//[3]Endere�o
						SA1->A1_MUN                            ,;//[3]Cidade
						SA1->A1_EST                            ,;//[5]Estado
						SA1->A1_CEP                            } //[6]CEP
						
	PL820Proc(	aCobranca		,aDadosEmp		,aDadosTit		,aDatSacado	,;
				aMsgBoleto		,aOpenMonth	    ,aDependentes	,aObservacoes)

	P820->(DbSkip())
End
//�����������������������������������������������������������������Ŀ
//�Gravacao do trailler no arquivo de boletos                       �
//�������������������������������������������������������������������
PL820Trailler(aTrailler)
//�--------------------------------------------------------------------�
//| Fecha arquivo...                                                   |
//�--------------------------------------------------------------------�
P820->(DbCloseArea())
//�--------------------------------------------------------------------------�
//| Fim do Relat�rio                                                         |
//�--------------------------------------------------------------------------�
MsgInfo(STR0001+ cStartPath+cArq ) //"Gerado o arquivo corretamente "
Return

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Programa   �PL820Grava� Autor � Marcos Alves          � Data � 15.05.04 ���
��������������������������������������������������������������������������Ĵ��
���Descricao  � Gravacao dos dados no arquivo para envio                   ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
/*/

Static Function PL820Proc(	aCobranca		,aDadosEmp		,aDadosTit		,aDatSacado	,;
							aMsgBoleto		,aOpenMonth	    ,aDependentes	,aObservacoes)

PL820Titulos(aDadosTit,aDatSacado)
PL820Usuarios(aDependentes)
PL820Cobranca(aCobranca)
PL820AbrMes(aOpenMonth)
PL820Mensagens(aMsgBoleto,aObservacoes)
Return .T.

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Programa   �PL820Header� Autor � Marcos Alves         � Data � 15.05.04 ���
��������������������������������������������������������������������������Ĵ��
���Descricao  � Gravacao dos dados no arquivo Tipo de Registro 01-Header   ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
/*/
Static Function PL820Header(aDadosEmp)
Local cLinha	:= ""
cLinha:="01"  				//Tipo de Registro 01-Header
cLinha+=aDadosEmp[1]       // Codigo da Operadora BA0_CODODE+BA0_CODINT
cLinha+=aDadosEmp[2]       // Nome da Operadora BA0_NOMINT
cLinha+=aDadosEmp[7]       // CNPJ
cLinha+=DToC(dDataBase)    // Data de garacao
cLinha+=Time() 				// Hora da garacao
cLinha+=aDadosEmp[9]		// Banco do Bordero
cLinha+=aDadosEmp[10]   	// Agencia do Bordero
cLinha+=aDadosEmp[11]      // Conta corrente cedente

PL820Grava(cLinha)
Return cLinha

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Programa   �PL820Titulos� Autor � Marcos Alves         � Data �15.05.04 ���
��������������������������������������������������������������������������Ĵ��
���Descricao  � Gravacao dos dados no arquivo Tipo de Registro 02-Titulos  ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
/*/
Static Function PL820Titulos(aDadosTit,aDatSacado)
Local cLinha	:= ""
cLinha:="02"  							//Tipo de Registro 02-Titulos
cLinha+=aDatSacado[1]    				// Sacado A1_NOME
cLinha+=DToC(aDadosTit[2]) 				// Emissao do titulo E1_EMISSA
cLinha+=DToC(aDadosTit[4]) 				// Vencimento E1_VENCTO
cLinha+=Strzero(aDadosTit[5]*100,12)  	// Valor ( E1_SALDO - E1_IRRF)
cLinha+=aDatSacado[3]    				// Sacado Endereco A1_END
cLinha+=aDatSacado[4]      				// Sacado Municipio A1_MUN
cLinha+=aDatSacado[5]      				// Sacado Municipio A1_EST
cLinha+=aDatSacado[6]      				// Sacado Municipio A1_CEP
cLinha+=aDadosTit[6]       				// Nosso numero 
cLinha+=aDadosTit[9]       				// Matricula familia E1_CODINT+E1_CODEMP+E1_MATRIC
cLinha+="X"  							//Tipo da cobranca F - Fechada, A - Aberta
cLinha+=aDadosTit[7]       				// Mes de competencia E1_MESBASE
cLinha+=aDadosTit[8]      				// Ano de competencia E1_ANOBASE

PL820Grava(cLinha)
Return cLinha

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Programa   �PL820Usuarios� Autor � Marcos Alves         � Data �15.05.04���
��������������������������������������������������������������������������Ĵ��
���Descricao  � Gravacao dos dados no arquivo Tipo de Registro 03-Usuarios ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
/*/
Static Function PL820Usuarios(aDependentes)
Local cLinha	:= ""
Local nI        :=0

For nI:= 1 to Len(aDependentes)
	cLinha:="03"  			    	//Tipo de Registro 03-Usuarios
	cLinha+=aDependentes[nI][1]    // Identificacao do usuario BA1_TIPREG
	cLinha+=aDependentes[nI][2]    // Nome do usuario BA1_NOMUSR
	PL820Grava(cLinha)
Next nI

Return cLinha

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Programa   �PL820Cobranca� Autor � Marcos Alves         � Data �15.05.04���
��������������������������������������������������������������������������Ĵ��
���Descricao  � Gravacao dos dados no arquivo Tipo de Registro             ���
���           �04-Composicao cobranca                                      ���
���           �05-Utilizacao                                               ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
/*/
Static Function PL820Cobranca(aCobranca)
Local cLinha	:= ""
Local nI        :=0
Local nx        :=0

For nI:= 1 to Len(aCobranca)
	cLinha:="04"  									// Tipo de Registro 04-Composicao cobranca
	cLinha+=aCobranca[nI][1]       					// Identificacao do usuario BM1_TIPREG
	cLinha+=aCobranca[nI][2]       					// Nome do usuario BM1_NOMUSR
	cLinha+=aCobranca[nI][6]       					// Codigo do lancamento               
	cLinha+=aCobranca[nI][7]       					// Descricao lancamento            
	cLinha+=StrZero(aCobranca[nI][8]*100,12)  	 	// Valor do lancamento            
	cLinha+="X"                     				// Tipo do Lancamento
	PL820Grava(cLinha)
	If Len(aCobranca[nI][9])>0
		For nX:= 1 to Len(aCobranca[nI][9])
			cLinha:="05"  										// Tipo de Registro 05-Utilizacao
			cLinha+=aCobranca[nI][9][nX][1]       				// Identificacao do usuario 	BD6_TIPREG
			cLinha+=aCobranca[nI][9][nX][2]       				// Nome do usuario 				BD6_NOMUSR
			cLinha+=aCobranca[nI][9][nX][3]       				// Codigo RDA 					BD6_CODRDA
			cLinha+=aCobranca[nI][9][nX][4]       				// Descricao RDA 				BD6_NOMRDA
			cLinha+=DToC(aCobranca[nI][9][nX][5]) 				// Data da utilizacao 			BD6_DATPRO
			cLinha+=aCobranca[nI][9][nX][6]       				// Numero impresso    			BD6_NUMERO
			cLinha+=aCobranca[nI][9][nX][7]       				// Codido do procedimento 		BD6_CODPRO
			cLinha+=aCobranca[nI][9][nX][8]       				// Descricao do procedimento 	BD6_DESPRO
			cLinha+=StrZero(aCobranca[nI][9][nX][09]*100,12)  	// Quantidade de procedimento 	BD6_QTDPRO
			cLinha+=StrZero(aCobranca[nI][9][nX][10]*100,12)  	// Valor do procedimento 		BD6_VLRTPF
	
			PL820Grava(cLinha)
		Next nX
	EndIf
Next nI

Return cLinha

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Programa   �PL820Utilizacao� Autor � Marcos Alves       � Data �15.05.04���
��������������������������������������������������������������������������Ĵ��
���Descricao  � Gravacao dos dados no arquivo Tipo  Registro 05-Utilizacao ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
/
Static Function PL820Utilizacao(aCobranca)
Local cLinha	:= ""
Local nI        :=0

For nI:= 1 to Len(aCobranca)
    If Empty(aCobranca[nI][1])
		cLinha:="05"  					// Tipo de Registro 05-Utilizacao
		cLinha+=aCobranca[nI][2]       // Identificacao do usuario 		BD6_TIPREG
		cLinha+=aCobranca[nI][3]       // Nome do usuario 				BD6_NOMUSR
		cLinha+=aCobranca[nI][4]       // Codigo RDA 					BD6_CODRDA
		cLinha+=aCobranca[nI][5]       // Descricao RDA 				BD6_NOMRDA
		cLinha+=aCobranca[nI][6]       // Data da utilizacao 			BD6_DATPRO
		cLinha+=aCobranca[nI][7]       // Numero impresso    			BD6_NUMERO
		cLinha+=aCobranca[nI][8]       // Codido do procedimento 		BD6_CODPRO
		cLinha+=aCobranca[nI][9]       // Descricao do procedimento 	BD6_DESPRO
		cLinha+=aCobranca[nI][10]      // Quantidade de procedimento 	BD6_QTDPRO
		cLinha+=aCobranca[nI][11]      // Valor do procedimento 		BD6_VLRTPF

		PL820Grava(cLinha)
	EndIf	
Next nI
Return cLinha
*/
/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Programa   �PL820Utilizacao� Autor � Marcos Alves       � Data �15.05.04���
��������������������������������������������������������������������������Ĵ��
���Descricao  � Gravacao dos dados arquivo Tipo Registro 06-Meses em aberto���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
/*/
Static Function PL820AbrMes(aOpenMonth)
Local cLinha	:= ""
Local nI        :=0

For nI:= 1 to Len(aOpenMonth)
	cLinha:="06"  					// Tipo de Registro 06-Meses em aberto
	cLinha+=aOpenMonth[nI][1]      // Mes Base E1_MESBASE
	cLinha+=aOpenMonth[nI][2]      // Ano Base E1_ANOBASE

	PL820Grava(cLinha)
Next nI

Return cLinha
/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Programa   �PL820Utilizacao� Autor � Marcos Alves       � Data �15.05.04���
��������������������������������������������������������������������������Ĵ��
���Descricao  � Gravacao dos dados arquivo Tipo Registro 07-Mensagens      ���
���           �  01- Boleto                                                ���
���           �  02- Extrato                                               ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
/*/
Static Function PL820Mensagens(aMsgBoleto,aObservacoes)
Local cLinha	:= ""
Local nI        :=0

For nI:= 1 to Len(aMsgBoleto)
	cLinha:="07"  					// Tipo de Registro 07-Mensagens
	cLinha+="01"				    // Tipo da Mensagenm "01- Boleto"
	cLinha+=aMsgBoleto[nI]         // Ano Base BH1_MSG01

	PL820Grava(cLinha)
Next nI
For nI:= 1 to Len(aObservacoes)
	cLinha:="07"  					// Tipo de Registro 07-Mensagens
	cLinha+="02"				    // Tipo da Mensagenm "02- Extrato"
	cLinha+=aObservacoes[nI]        // Ano Base BH2_MSG01

	PL820Grava(cLinha)
Next nI
Return cLinha

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Programa   �PL820Trailler  � Autor � Marcos Alves       � Data �15.05.04���
��������������������������������������������������������������������������Ĵ��
���Descricao  � Gravacao dos dados arquivo Tipo Registro 99-Trailler       ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
/*/
Static Function PL820Trailler(aTrailler)
Local cLinha	:= ""

cLinha:="99"  			                            // Tipo de Registro 99-Trailler
cLinha+=Str(aTrailler[1])                          	// Quantidade de titulos
cLinha+=StrZero(aTrailler[2]*100,12)  	 		    // Valor total de titulos

PL820Grava(cLinha)

Return cLinha

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Programa   �PL820Grava     � Autor � Marcos Alves       � Data �15.05.04���
��������������������������������������������������������������������������Ĵ��
���Descricao  � Gravacao de texto no arquivo                               ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
/*/
Function PL820Grava(cTexto)
Local lRet		:=.T.
Local nHandle	:= -1

cTexto:=StrZero(++nRegBol,6)+cTexto
If !File( cArq )
	nHandle := FCreate( cArq )
	FClose( nHandle )
Endif
If File( cArq ).AND.!Empty(cTexto)
	nHandle := FOpen( cArq, 2 )
	FSeek( nHandle, 0, 2 )	// Posiciona no final do arquivo
	FWrite( nHandle, cTexto + Chr(13) + Chr(10), Len(cTexto)+2 )
	FClose( nHandle) 
Endif

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PLR240MES �Autor  �Rafael M. Quadrotti � Data �  02/04/04   ���
�������������������������������������������������������������������������͹��
���Desc.     �Retorna os meses em aberto do sacado.                       ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP6                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function PL820MES(cCliente,cLoja,cMesBase,cAnoBase)
Local aMeses 	:= {} 							//Retorno da funcao.
Local cSQL      := ""							//Query
Local cSE1Name 	:= SE1->(RetSQLName("SE1"))	//retorna o alias no TOP.

SE1->(DbSetOrder(1))

//��������������������������������������������������������������������������Ŀ
//� Monta query...                                                           �
//����������������������������������������������������������������������������
cSQL := "SELECT * FROM "+cSE1Name+" WHERE E1_FILIAL = '" + xFilial("SE1") + "' AND "
cSQL += "			E1_CLIENTE = '"+cCliente+"' AND "
cSQL += "			E1_LOJA    = '"+cLoja+"' AND "
cSQL += "			E1_SALDO > 0 AND "
cSQL += "			E1_PARCELA <> '" + StrZero(0, Len(SE1->E1_PARCELA)) + "' AND "
cSQL += cSE1Name+".D_E_L_E_T_ = '' "
cSQL += "ORDER BY " + SE1->(IndexKey())

PLSQuery(cSQL,STR0040) //"Meses"

If Meses->(Eof())
	Meses->(DbCloseArea())
	Aadd(aMeses,"")
Else
	While 	!Eof() .And.;
		Meses->E1_CLIENTE == cCliente .And.;
		Meses->E1_LOJA ==	cLoja
		If (cMesBase<>E1_MESBASE .And. cAnoBase<>E1_ANOBASE)
			Aadd(aMeses,{E1_MESBASE,E1_ANOBASE})
		EndIf	
		DbSkip()
	End
	Meses->(DbCloseArea())
EndIf
Return (aMeses)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �PLR240TEXT�Autor  �Rafael M. Quadrotti � Data �  02/04/04   ���
�������������������������������������������������������������������������͹��
���Desc.     �Retorna as mensagens para impressao.                        ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���          � Esta funcao executa uma selecao em tres tabelas para       ���
���          � encontrar a msg relacionada ao sacado.                     ���
�������������������������������������������������������������������������͹��
���Uso       � AP6                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function PL820TEXT(	nTipo	,cCodInt	,cCodEmp	,cConEmp,;
							cSubCon	,cMatric	,cBase	)//cBase = Ano+mes
Local cQuery  	 := ""
Local cNTable1   := "" // Nome da tabela no SQL
Local cNTable2   := "" // Nome da tabela no SQL
Local cNTable3   := "" // Nome da tabela no SQL
Local aMsg		 := {} // Array de mensagens

DbSelectArea("BH1")
DbSetOrder(2)
cNTable1 := RetSqlName("BH1")
cNTable2 := RetSqlName("BA3")
cNTable3 := RetSqlName("BH2")

cQuery := "SELECT BH1.* ,BH2.* ,BA3.BA3_CODPLA,BA3.BA3_FILIAL,BA3.BA3_CODINT,BA3.BA3_CODEMP,BA3.BA3_MATRIC,BA3.BA3_CONEMP,BA3.BA3_VERCON,BA3.BA3_SUBCON,BA3.BA3_VERSUB "
cQuery += "FROM " +cNTable1+ " BH1 , " +cNTable2 +" BA3 , " +cNTable3 +" BH2 "

//BH1
cQuery += "WHERE BH1.BH1_FILIAL='"	+	xFilial("BH1")		+	"'    AND "
cQuery += "		 BH1.BH1_CODINT='"	+	cCodInt				+	"'    AND "
cQuery += "		 BH1.BH1_TIPO='"	+	Transform(nTipo,"9")+	"'    AND "
If !Empty(cCodEmp)
	cQuery += "(('"+cCodEmp +"' BETWEEN BH1.BH1_EMPDE   AND BH1.BH1_EMPATE) OR (BH1.BH1_EMPATE='' AND BH1.BH1_EMPDE='') ) AND "
EndIf
If !Empty(cConEmp)
	cQuery += "(('"+cConEmp +"' BETWEEN BH1.BH1_CONDE   AND BH1.BH1_CONATE)  OR (BH1.BH1_CONATE='' AND BH1.BH1_CONDE='') ) AND "
EndIf
If !Empty(cSubCon)
	cQuery += "(('"+cSuBCon +"' BETWEEN BH1.BH1_SUBDE   AND BH1.BH1_SUBATE)  OR (BH1.BH1_SUBATE='' AND BH1.BH1_SUBDE='') ) AND "
EndIf
If !Empty(cMatric)
	cQuery += "(('"+cMatric +"' BETWEEN BH1.BH1_MATDE   AND BH1.BH1_MATATE)  OR (BH1.BH1_MATATE='' AND BH1.BH1_MATDE='') ) AND "
EndIf
If !Empty(cBase)
	cQuery += "(('"+cBase   +"' BETWEEN BH1.BH1_BASEIN AND BH1.BH1_BASEFI) OR (BH1.BH1_BASEIN='' AND BH1.BH1_BASEFI='') ) AND "
EndIf
//BA3 PARA ENCONTRAR O PLANO
cQuery += "		 BA3.BA3_FILIAL='"	+	xFilial("BA3")	+	"'    AND "
cQuery += "		 BA3.BA3_CODINT='"	+	cCodInt  		+	"'    AND "
cQuery += "		 BA3.BA3_CODEMP='"	+	cCodEmp  		+	"'    AND "
cQuery += "		 BA3.BA3_MATRIC='"	+	cMatric  		+	"'    AND "
cQuery += "		((BA3.BA3_CODPLA BETWEEN BH1.BH1_PLAINI AND BH1.BH1_PLAFIM) OR (BH1.BH1_PLAINI='' AND BH1.BH1_PLAFIM='') ) AND "

//BH2 MENSAGENS PARA IMPRESSAO
cQuery += "		 BH2.BH2_FILIAL = '"	+	xFilial("BH2")	+	"'    AND "
cQuery += "		 BH2.BH2_CODIGO = BH1.BH1_CODIGO  AND "


cQuery += "		BH1.D_E_L_E_T_<>'*' AND BA3.D_E_L_E_T_<>'*' AND BH2.D_E_L_E_T_<>'*' "
cQuery += "ORDER BY " + BH1->(IndexKey())

PLSQuery(cQuery,"MSG")

If MSG->(Eof())
	MSG->(DbCloseArea())
	Aadd(aMSG,"")
Else
	While 	!Eof()
		If Iif(BH1->(FieldPos("BH1_CONDIC")) > 0 , (Empty(MSG->BH1_CONDIC) .or. (&(MSG->BH1_CONDIC))), .T.)
			Aadd(aMSG,MSG->BH2_MSG01)
		Endif
		DbSkip()
	End  
	If Len(aMSG) == 0
		Aadd(aMSG,"")
	Endif		
	MSG->(DbCloseArea())
EndIf

Return aMsg


