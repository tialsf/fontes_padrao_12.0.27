
Function PLSM200
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Declaracao de variaveis...                                          �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
LOCAL aSays     := {}
LOCAL aButtons  := {}
LOCAL cCadastro := "Analise da Integridade da Base de Dados"
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Monta texto para janela de processamento                                 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
AADD(aSays,"Analise da Integridade da Base de Dados")
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Monta botoes para janela de processamento                                �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
AADD(aButtons, { 1,.T.,{|| MsAguarde({|| PLSM200Pro()}, "", "Analisando...", .T.),FechaBatch() }} )
AADD(aButtons, { 2,.T.,{|| FechaBatch() }} )
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Exibe janela de processamento                                            �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
//FormBatch( cCadastro, aSays, aButtons,, 160 )                                    
MsAguarde({|| PLSM200Pro()}, "", "Analisando...", .T.)
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Fim da Rotina Principal...                                               �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Return


Function PLSM200Pro()
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Declaracao de variaveis...                                          �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
LOCAL cSQL
LOCAL aBD5Dupli  := {}
LOCAL aBD6Dupli  := {}
LOCAL aBD7Dupli  := {}
LOCAL aBE4Dupli  := {}
LOCAL aBD5semBD6 := {}
LOCAL aBE4semBD6 := {}
LOCAL aBD6semBD7 := {}                                                  
LOCAL aRetFin
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Analise numero 1 - BD5 duplicados...                                �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
MsProcTXT("Analisando BD5 duplicados...")

cSQL := "SELECT BD51.BD5_SITUAC,BD51.BD5_CODOPE, BD51.BD5_CODLDP, BD51.BD5_CODPEG, BD51.BD5_NUMERO, BD51.BD5_ANOPAG, BD51.BD5_MESPAG "
cSQL += "FROM "+RetSqlName("BD5")+" BD51 WHERE "
cSQL += "BD51.D_E_L_E_T_ = '' AND "
cSQL += "BD51.BD5_FILIAL = '"+xFilial("BD5")+"' AND "
cSQL += "(SELECT COUNT(*) FROM "+RetSqlName("BD5")+" BD5 WHERE D_E_L_E_T_ = '' AND "
cSQL += " BD5.BD5_FILIAL = '"+xFilial("BD5")+"' AND "
cSQL += " BD5.BD5_FILIAL = BD51.BD5_FILIAL AND "
cSQL += " BD5.BD5_CODOPE = BD51.BD5_CODOPE AND "
cSQL += " BD5.BD5_CODLDP = BD51.BD5_CODLDP AND "
cSQL += " BD5.BD5_CODPEG = BD51.BD5_CODPEG AND "
cSQL += " BD5.BD5_NUMERO = BD51.BD5_NUMERO AND "
cSQL += " BD5.BD5_ORIMOV = BD51.BD5_ORIMOV AND "
cSQL += "BD5.D_E_L_E_T_ = '') > 1 ORDER BY BD51.BD5_CODOPE, BD51.BD5_CODLDP, BD51.BD5_CODPEG, BD51.BD5_NUMERO, BD51.BD5_ANOPAG, BD51.BD5_MESPAG"

PLSQuery(cSQL,"Trb")
Trb->(DBEval( { | | aadd(aBD5Dupli, {BD5_CODOPE,BD5_CODLDP,BD5_CODPEG,BD5_NUMERO,BD5_ANOPAG+"-"+BD5_MESPAG,BD5_SITUAC}) }))
Trb->(DbCloseArea())

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Analise numero 2 - BD6 duplicados...                                �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
MsProcTXT("Analisando BD6 duplicados...")

cSQL := "SELECT BD61.BD6_SITUAC,BD61.BD6_CODOPE, BD61.BD6_CODLDP, BD61.BD6_CODPEG, BD61.BD6_NUMERO, BD61.BD6_SEQUEN, BD61.BD6_ANOPAG, BD61.BD6_MESPAG "
cSQL += "FROM "+RetSqlName("BD6")+" BD61 WHERE "
cSQL += "BD61.D_E_L_E_T_ = '' AND "
cSQL += "BD61.BD6_FILIAL = '"+xFilial("BD6")+"' AND "
cSQL += "(SELECT COUNT(*) FROM "+RetSqlName("BD6")+" BD6 WHERE D_E_L_E_T_ = '' AND "
cSQL += " BD6.BD6_FILIAL = '"+xFilial("BD6")+"' AND "
cSQL += " BD6.BD6_FILIAL = BD61.BD6_FILIAL AND "
cSQL += " BD6.BD6_CODOPE = BD61.BD6_CODOPE AND "
cSQL += " BD6.BD6_CODLDP = BD61.BD6_CODLDP AND "
cSQL += " BD6.BD6_CODPEG = BD61.BD6_CODPEG AND "
cSQL += " BD6.BD6_NUMERO = BD61.BD6_NUMERO AND "
cSQL += " BD6.BD6_SEQUEN = BD61.BD6_SEQUEN AND "
cSQL += " BD6.BD6_ORIMOV = BD61.BD6_ORIMOV AND "
cSQL += "BD6.D_E_L_E_T_ = '') > 1 ORDER BY BD61.BD6_CODOPE, BD61.BD6_CODLDP, BD61.BD6_CODPEG, BD61.BD6_NUMERO, BD61.BD6_SEQUEN, BD61.BD6_ANOPAG, BD61.BD6_MESPAG"

PLSQuery(cSQL,"Trb")
Trb->(DBEval( { | | aadd(aBD6Dupli, {BD6_CODOPE,BD6_CODLDP,BD6_CODPEG,BD6_NUMERO,BD6_SEQUEN,BD6_ANOPAG+"-"+BD6_MESPAG,BD6_SITUAC}) }))
Trb->(DbCloseArea())
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Analise numero 3 - BE4 duplicados...                                �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
MsProcTXT("Analisando BE4 duplicados...")

cSQL := "SELECT BE41.BE4_SITUAC,BE41.BE4_CODOPE, BE41.BE4_CODLDP, BE41.BE4_CODPEG, BE41.BE4_NUMERO, BE41.BE4_ANOPAG, BE41.BE4_MESPAG "
cSQL += "FROM "+RetSqlName("BE4")+" BE41 WHERE "
cSQL += "BE41.D_E_L_E_T_ = '' AND "
cSQL += "BE41.BE4_FILIAL = '"+xFilial("BE4")+"' AND "
cSQL += "(SELECT COUNT(*) FROM "+RetSqlName("BE4")+" BE4 WHERE D_E_L_E_T_ = '' AND "
cSQL += " BE4.BE4_FILIAL = '"+xFilial("BE4")+"' AND "
cSQL += " BE4.BE4_FILIAL = BE41.BE4_FILIAL AND "
cSQL += " BE4.BE4_CODOPE = BE41.BE4_CODOPE AND "
cSQL += " BE4.BE4_CODLDP = BE41.BE4_CODLDP AND "
cSQL += " BE4.BE4_CODPEG = BE41.BE4_CODPEG AND "
cSQL += " BE4.BE4_NUMERO = BE41.BE4_NUMERO AND "
cSQL += " BE4.BE4_ORIMOV = BE41.BE4_ORIMOV AND "
cSQL += "BE4.D_E_L_E_T_ = '') > 1 ORDER BY BE41.BE4_CODOPE, BE41.BE4_CODLDP, BE41.BE4_CODPEG, BE41.BE4_NUMERO, BE41.BE4_ANOPAG, BE41.BE4_MESPAG"

PLSQuery(cSQL,"Trb")
Trb->(DBEval( { | | aadd(aBE4Dupli, {BE4_CODOPE,BE4_CODLDP,BE4_CODPEG,BE4_NUMERO,BE4_ANOPAG+"-"+BE4_MESPAG,BE4_SITUAC}) }))
Trb->(DbCloseArea())

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Analise numero 4 - BD7 duplicados...                                �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
MsProcTXT("Analisando BD7 duplicados...")

cSQL := "SELECT BD71.BD7_SITUAC,BD71.BD7_CODOPE, BD71.BD7_CODLDP, BD71.BD7_CODPEG, BD71.BD7_NUMERO, BD71.BD7_SEQUEN, BD71.BD7_CODUNM, BD71.BD7_NLANC,BD71.BD7_ANOPAG, BD71.BD7_MESPAG "
cSQL += "FROM "+RetSqlName("BD7")+" BD71 WHERE "
cSQL += "BD71.D_E_L_E_T_ = '' AND "
cSQL += "BD71.BD7_FILIAL = '"+xFilial("BD7")+"' AND "
cSQL += "(SELECT COUNT(*) FROM "+RetSqlName("BD7")+" BD7 WHERE D_E_L_E_T_ = '' AND "
cSQL += " BD7.BD7_FILIAL = '"+xFilial("BD7")+"' AND "
cSQL += " BD7.BD7_FILIAL = BD71.BD7_FILIAL AND "
cSQL += " BD7.BD7_CODOPE = BD71.BD7_CODOPE AND "
cSQL += " BD7.BD7_CODLDP = BD71.BD7_CODLDP AND "
cSQL += " BD7.BD7_CODPEG = BD71.BD7_CODPEG AND "
cSQL += " BD7.BD7_NUMERO = BD71.BD7_NUMERO AND "
cSQL += " BD7.BD7_SEQUEN = BD71.BD7_SEQUEN AND "
cSQL += " BD7.BD7_CODUNM = BD71.BD7_CODUNM AND "
cSQL += " BD7.BD7_NLANC  = BD71.BD7_NLANC  AND "
cSQL += " BD7.BD7_ORIMOV = BD71.BD7_ORIMOV AND "
cSQL += "BD7.D_E_L_E_T_ = '') > 1 ORDER BY BD71.BD7_CODOPE, BD71.BD7_CODLDP, BD71.BD7_CODPEG, BD71.BD7_NUMERO, BD71.BD7_SEQUEN, BD71.BD7_CODUNM, BD71.BD7_NLANC,BD71.BD7_ANOPAG, BD71.BD7_MESPAG"

PLSQuery(cSQL,"Trb")
Trb->(DBEval( { | | aadd(aBD7Dupli, {BD7_CODOPE,BD7_CODLDP,BD7_CODPEG,BD7_NUMERO,BD7_SEQUEN,BD7_CODUNM,BD7_NLANC,BD7_ANOPAG+"-"+BD7_MESPAG,BD7_SITUAC}) }))
Trb->(DbCloseArea())
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Analise numero 5 - BD5 sem BD6...                                   �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
MsProcTXT("Analisando BD5 sem BD6...")

cSQL := "SELECT BD5_SITUAC,BD5_CODOPE, BD5_CODLDP, BD5_CODPEG, BD5_NUMERO, BD5_ANOPAG, BD5_MESPAG "
cSQL += "FROM "+RetSQLName("BD5")+" WHERE "
cSQL += "BD5_FILIAL = '"+xFilial("BD5")+"' AND "
cSQL += "D_E_L_E_T_ = ' ' "
cSQL += "AND "
cSQL += "NOT EXISTS( "
cSQL += "SELECT * FROM "+RetSQLName("BD6")+" WHERE "
cSQL += "BD6_FILIAL = BD5_FILIAL AND "
cSQL += "BD6_CODOPE = BD5_CODOPE AND "
cSQL += "BD6_CODPEG = BD5_CODPEG AND "
cSQL += "BD6_NUMERO = BD5_NUMERO AND "
cSQL += "BD6_ORIMOV = BD5_ORIMOV AND "
cSQL += "D_E_L_E_T_ = '') "
cSQL += "ORDER BY BD5_CODOPE, BD5_CODLDP, BD5_CODPEG, BD5_NUMERO, BD5_ANOPAG, BD5_MESPAG "

PLSQuery(cSQL,"Trb")
Trb->(DBEval( { | | aadd(aBD5semBD6, {BD5_CODOPE,BD5_CODLDP,BD5_CODPEG,BD5_NUMERO,BD5_ANOPAG+"-"+BD5_MESPAG,BD5_SITUAC}) }))
Trb->(DbCloseArea())
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Analise numero 6 - BE4 sem BD6...                                   �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
MsProcTXT("Analisando BE4 sem BD6...")

cSQL := "SELECT BE4_SITUAC,BE4_CODOPE, BE4_CODLDP, BE4_CODPEG, BE4_NUMERO, BE4_ANOPAG, BE4_MESPAG "
cSQL += "FROM "+RetSQLName("BE4")+" WHERE "
cSQL += "BE4_FILIAL = '"+xFilial("BE4")+"' AND "
cSQL += "D_E_L_E_T_ = ' ' "
cSQL += "AND "
cSQL += "NOT EXISTS( "
cSQL += "SELECT * FROM "+RetSQLName("BD6")+" WHERE "
cSQL += "BD6_FILIAL = BE4_FILIAL AND "
cSQL += "BD6_CODOPE = BE4_CODOPE AND "
cSQL += "BD6_CODPEG = BE4_CODPEG AND "
cSQL += "BD6_NUMERO = BE4_NUMERO AND "
cSQL += "BD6_ORIMOV = BE4_ORIMOV AND "
cSQL += "D_E_L_E_T_ = '') "
cSQL += "ORDER BY BE4_CODOPE, BE4_CODLDP, BE4_CODPEG, BE4_NUMERO, BE4_ANOPAG, BE4_MESPAG "

PLSQuery(cSQL,"Trb")
Trb->(DBEval( { | | aadd(aBE4semBD6, {BE4_CODOPE,BE4_CODLDP,BE4_CODPEG,BE4_NUMERO,BE4_ANOPAG+"-"+BE4_MESPAG,BE4_SITUAC}) }))
Trb->(DbCloseArea())
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Analise numero 7 - BD6 sem BD7...                                   �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
MsProcTXT("Analisando BD6 sem BD7...")

cSQL := "SELECT BD6_SITUAC,BD6_CODOPE, BD6_CODLDP, BD6_CODPEG, BD6_NUMERO, BD6_SEQUEN, BD6_ANOPAG, BD6_MESPAG "
cSQL += "FROM "+RetSQLName("BD6")+" WHERE "
cSQL += "BD6_FILIAL = '"+xFilial("BD6")+"' AND "
cSQL += "D_E_L_E_T_ = ' ' "
cSQL += "AND "
cSQL += "NOT EXISTS( "
cSQL += "SELECT * FROM "+RetSQLName("BD7")+" WHERE "
cSQL += "BD6_FILIAL = BD7_FILIAL AND "
cSQL += "BD6_CODOPE = BD7_CODOPE AND "
cSQL += "BD6_CODPEG = BD7_CODPEG AND "
cSQL += "BD6_NUMERO = BD7_NUMERO AND "
cSQL += "BD6_ORIMOV = BD7_ORIMOV AND "
cSQL += "BD6_SEQUEN = BD7_SEQUEN AND "
cSQL += "D_E_L_E_T_ = '') "
cSQL += "ORDER BY BD6_CODOPE, BD6_CODLDP, BD6_CODPEG, BD6_NUMERO, BD6_SEQUEN, BD6_ANOPAG, BD6_MESPAG "

PLSQuery(cSQL,"Trb")
Trb->(DBEval( { | | aadd(aBD6semBD7, {BD6_CODOPE,BD6_CODLDP,BD6_CODPEG,BD6_NUMERO,BD6_SEQUEN,BD6_ANOPAG+"-"+BD6_MESPAG,BD6_SITUAC}) }))
Trb->(DbCloseArea())
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Analise numero 8 - Faturas que nao estao batendo...                 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
aRetFin := AnalisaSE1XBDH()
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Monta resultados...                                                 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
If Len(aBD5Dupli) > 0
   PLSCRIGEN(aBD5Dupli,{ {"Operadora","@C",30} , {"Loc.Dig.","@C",30 } , {"PEG","@C",30 } , {"Numero","@C",30 } , {"Competencia","@C",040},{"Situacao","@C",040}},"BD5 Duplicados")
Endif

If Len(aBD6Dupli) > 0
   PLSCRIGEN(aBD6Dupli,{ {"Operadora","@C",30} , {"Loc.Dig.","@C",30 } , {"PEG","@C",30 } , {"Numero","@C",30 } , {"Sequencia","@C",30 }, {"Competencia","@C",040},{"Situacao","@C",040}},"BD6 Duplicados")
Endif

If Len(aBE4Dupli) > 0
   PLSCRIGEN(aBE4Dupli,{ {"Operadora","@C",30} , {"Loc.Dig.","@C",30 } , {"PEG","@C",30 } , {"Numero","@C",30 } , {"Competencia","@C",040},{"Situacao","@C",040}},"BE4 Duplicados")
Endif

If Len(aBD7Dupli) > 0
   PLSCRIGEN(aBD7Dupli,{ {"Operadora","@C",30} , {"Loc.Dig.","@C",30 } , {"PEG","@C",30 } , {"Numero","@C",30 } , {"Sequencia","@C",30 }, {"Unidade","@C",30 },{"No.Lanc.","@C",30 },{"Competencia","@C",040},{"Situacao","@C",040}},"BD7 Duplicados")
Endif

If Len(aBD5semBD6) > 0
   PLSCRIGEN(aBD5semBD6,{ {"Operadora","@C",30} , {"Loc.Dig.","@C",30 } , {"PEG","@C",30 } , {"Numero","@C",30 } , {"Competencia","@C",040},{"Situacao","@C",040}},"BD5 sem BD6")
Endif

If Len(aBE4semBD6) > 0
   PLSCRIGEN(aBE4semBD6,{ {"Operadora","@C",30} , {"Loc.Dig.","@C",30 } , {"PEG","@C",30 } , {"Numero","@C",30 } , {"Competencia","@C",040},{"Situacao","@C",040}},"BE4 sem BD6")
Endif

If Len(aBD6semBD7) > 0
   PLSCRIGEN(aBD6semBD7,{ {"Operadora","@C",30} , {"Loc.Dig.","@C",30 } , {"PEG","@C",30 } , {"Numero","@C",30 } , {"Sequencia","@C",30 } ,{"Competencia","@C",040},{"Situacao","@C",040}},"BD6 sem BD7")
Endif               

If Len(aRetFin) > 0
   PLSCRIGEN(aRetFin,{ {"Titulo","@C",100} , {"Empresa","@C",100},{"Competencia","@C",100},{"Valor BDH","@E 999,999.9999",30 } , {"Valor BD6","@E 999,999.9999",30 } },"Faturas com problemas entre BD6 e BDH")   
Endif
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Fim da rotina principal...                                          �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
Return



Static Function AnalisaSE1XBDH
LOCAL cSQL
LOCAL aRet := {}
LOCAL nVl1 := 0
LOCAL nVl2 := 0         
LOCAL cPref1 := Eval({|| &(GetNewPar("MV_PLPFE11",'"PLS"')) } )
LOCAL cPref2 := Eval({|| &(GetNewPar("MV_PLPFE12",'"PLS"')) } )

DbSelectArea("SE1")

cSQL := "SELECT * FROM "+RetSQLName("SE1")+" WHERE "
cSQL += "D_E_L_E_T_ = '' AND E1_TIPO = 'FT' AND E1_FILIAL = '"+xFilial("SE1")+"' "
cSQL += "AND (E1_PREFIXO = '"+cPref1+"' OR E1_PREFIXO = '"+cPref2+"')

PLSQuery(cSQL,"Trb")

While ! Trb->(Eof())

      MsProcTXT("Analisando Titulo "+Trb->(E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO))
      
      cSQL := "SELECT "
      cSQL += "SUM(BDH_VALOR+BDH_VLRCOP+BDH_VLRCP2+BDH_VLRTAX+BDH_VALCOC+BDH_VALCOM+BDH_VALCOT+BDH_VALCPF+BDH_VALCOF+BDH_VALTCF) VL1 "
      cSQL += " FROM "+RetSQLName("BDH")+" "
      cSQL += "WHERE "
      cSQL += "BDH_FILIAL = '"+xFilial("BDH")+"' AND "
      cSQL += "BDH_STATUS = '0' AND "
      cSQL += "BDH_PREFIX = '"+Trb->E1_PREFIXO+"' AND "
      cSQL += "BDH_NUMTIT = '"+Trb->E1_NUM+"' AND "
      cSQL += "BDH_PARCEL = '"+Trb->E1_PARCELA+"' AND "
      cSQL += "BDH_TIPTIT = '"+Trb->E1_TIPO+"' AND "
      cSQL += "D_E_L_E_T_ = ''"
      
      PLSQuery(cSQL,"Trb2")
      
      nVl1 := Trb2->VL1
      
      Trb2->(DbCloseArea())
      
      cSQL := "SELECT "
      cSQL += "SUM(BD6_VLRTPF) VL2 "
      cSQL += "FROM "+RetSQLName("BD6")+","+RetSQLName("BDH")+" "
      cSQL += "WHERE "
      cSQL += "BDH_STATUS = '0' AND "
      cSQL += "BDH_PREFIX = '"+Trb->E1_PREFIXO+"' AND "
      cSQL += "BDH_NUMTIT = '"+Trb->E1_NUM+"' AND "
      cSQL += "BDH_PARCEL = '"+Trb->E1_PARCELA+"' AND "
      cSQL += "BDH_TIPTIT = '"+Trb->E1_TIPO+"' AND "
      cSQL += RetSQLName("BDH")+".D_E_L_E_T_ = '' AND "
      cSQL += RetSQLName("BD6")+".D_E_L_E_T_ = '' AND "
      cSQL += "BD6_OPEUSR = BDH_CODINT AND "
      cSQL += "BD6_CODEMP = BDH_CODEMP AND "
      cSQL += "BD6_MATRIC = BDH_MATRIC AND "
      cSQL += "BD6_TIPREG = BDH_TIPREG AND "
      cSQL += "BD6_SEQPF = BDH_SEQPF AND "
      cSQL += "BD6_ANOPAG = BDH_ANOFT AND "
      cSQL += "BD6_MESPAG = BDH_MESFT AND "
      cSQL += RetSQLName("BDH")+".BDH_FILIAL = '"+xFilial("BDH")+"' AND "
      cSQL += RetSQLName("BD6")+".BD6_FILIAL = '"+xFilial("BDH")+"'"
      
      PLSQuery(cSQL,"Trb3")
      nVl2 := Trb3->VL2
      Trb3->(DbCloseArea())
      
      If Abs(nVl1-nVl2) > 0.5
         aadd(aRet,{Trb->(E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO),Trb->E1_CODEMP,Trb->E1_ANOBASE+'-'+Trb->E1_MESBASE,nVl1,nVl2})
      Endif

Trb->(DbSkip())
Enddo       
Trb->(DbCloseArea())


Return(aRet)
