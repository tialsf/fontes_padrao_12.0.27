#include "plstose1.ch"
#include "PROTHEUS.CH"
#include "PLSMGER.CH"    

static __cCondPag := ''

/*/{Protheus.doc} PlsToSe1
Gera titulos a partir de uma cobranca     
o array aCampos deve ser enviado para este funcao contendo o nome do campo e seu respectivo conteudo.
� fundamental que seja enviado a numeracao do titulo, vencto, filial, valor e saldo, natureza, cliente, loja
tamb�m deve ser enviado os campos referentes aos impostos (iss, inss, cofins, pis, csll) com valores iguais a zero (0),
para que a rotina crie as variavies que a integracao com o financeiro possa efetuar os calculos
� fundamental tamb�m que o controle de transacao esteja entre a chamada da rotina (begin, chamada, end) para que todas as gravacoes
estejam controladas pela transacao.
esta funcao espera o SED (natureza) e o SA1 (Clientes) j� posicionado pela rotina chamadora

o array aBases dever� conter os valores para a base dos impostos - passar na ordem cfe abaixo
Abaixo exemplo dos campos obrigatorios a serem passados

@author  PLS TEAM
@version P12
@since   25/05/04
/*/
function PLSTOSE1(aCampos, aBases, cMesRef, cAnoRef, cOrigem, lContabiliza, lCusOpe, aVlrCob, cPrefixo, cNumero,;
				  nPLGERREC, nPeriod, lNCC, aEventosCb, lPosSE1Ger, aCritica)
local nX				:= 0
local nFor				:= 0
local nPos				:= 0
local nPTotal			:= 0
local nPBasImp1			:= 0
local nPDesconto		:= 0
local nBasImp1			:= 0	
local nRecSe1 			:= 0
local nCalAcrs			:= 0
local nArredPrcLis		:= 0
local nValor			:= 0
local nTamSerie			:= tamSX3("F2_SERIE")[1]
local cCodDeb 			:= ""
local cNumNFS			:= ""
local cSerieNFS			:= ""
local cClieFor		    := ""
local cLoja			    := ""
local cItem			    := ""
local cNivel			:= "" 
local cNatureza			:= ""
local cDoc				:= ""
local cCampos			:= ""
local cSerOri		    := ""
local cNumORI		    := ""
local cTextoSF2			:= ""
local cMsgErr			:= "N�o Informado"				
local lErro				:= .f.
local lCredito			:= .f.
local lBXComp			:= .f.
local lNoFiscal         := .f.
local lMostraCtb		:= .f.
local lAglutCtb         := .f.
local lCtbOnLine        := .f.
local lCtbCusto         := .f.
local lReajusta			:= .f.
local lAtuSA7			:= .f.
local lECF				:= .f.
local lGerNFBRA			:= ( (getNewPar("MV_PLSNFBR","0") == "1".and. ! lNCC) .or. cPaisLOC <> 'BRA' )
local lTitDesc			:= getNewPar("MV_PLSDCOP",.f.)
local lPgtAto           := .f.
local dDatEmis          := cToD("")
local bFilSD2     		:= NIL
local bSD2     			:= NIL
local bSF2     			:= NIL
local bTTS     			:= NIL
local bFiscalSF2		:= NIL
local bFiscalSD2        := NIL
local bFatSE1           := NIL
local aArea 			:= getArea()
local aItemSD2			:= {}
local aSD2_BM1 			:= {}
local aRetDes			:= {}
local aRetFind			:= {}
local aCliente			:= {} 
local aErro				:= {}
local aStruSF2			:= {}
local aStruSD2			:= {}
local aSF2				:= {}
local aDocOri			:= {}
local lComp             := .F.
local lGerNcc			:= GetNewPar("MV_PLGENCC","0") == "1"  // Modelo antigo de gerar ncc o credito e descontado no valor total da nota
local lNewRec			:= .T.
local nValorBGQ			:= 0
local cSql              := ''

default nPLGERREC  		:= 0
default nPeriod         := 0
default cPrefixo        := ""
default cNumero         := ""
default cOrigem    		:= "PLSTOSE1"
default aVlrCob 		:= {}
default aEventosCb		:= {}
default aCritica		:= nil
default lContabiliza 	:= .f.
default lNCC			:= .f.
default lCusOpe       	:= .t.
default lPosSE1Ger      := .f.

private nVlRetPis  		:= 0
private nVlRetCof  		:= 0
private nVlRetCsl  		:= 0
private NVLORIPIS  		:= 0
private NVLORICOF  		:= 0
private NVLORICSL  		:= 0 
private nIndexSE1 		:= ""
private cIndexSE1 		:= ""
private cModRetPIS 		:= getNewPar( "MV_RT10925", "1" )
private lF040Auto  		:= .f.
private aDadosRef 		:= Array(7)
private aDadosRet 		:= Array(7)
private aDadosImp 		:= Array(3) 

private lMsErroAuto 	:= .f.
private lMsHelpAuto		:= .t.
private lAutoErrNofile	:= .t.

//campos que seram atualizados apos geracao da nota fiscal
cCampos := 'E1_ANOBASE|E1_MESBASE|E1_CODINT|E1_CODEMP|E1_CONEMP|E1_VERCON|E1_SUBCON|E1_VERSUB|E1_NUMCON|'
cCampos += 'E1_MATRIC|E1_TIPREG|E1_CODCOR|E1_PLORIG|E1_NUMBCO|E1_PLNUCOB|E1_ORIGEM|E1_LA|E1_FORMREC|E1_MULTNAT|'
cCampos += 'E1_APLVLMN|E1_PORTADO|E1_AGEDEP|E1_CONTA|E1_BCOCLI|E1_AGECLI|E1_CTACLI|E1_VALJUR|E1_PORCJUR|'
cCampos += 'E1_VENCTO|E1_VENCREA|E1_NATUREZ'

//Inicia os arrays de  impostos do zero                   
aFill(aDadosRef,0)
aFill(aDadosRet,0)
aFill(aDadosImp,0)

//Gravacao do titulo ou notafiscal							
if ! lGerNFBRA 

	nPos := aScan(aCampos,{|x| x[1] == "E1_PREFIXO"}) 

    if aCampos[nPos][2] == "CPP"

		if existBlock('PLSALTCPP')
			aCampos := execBlock('PLSALTCPP',.f.,.f., { aCampos } )	
		endIf

	endIf

	msExecAuto({|x,y| Fina040(x,y)}, aCampos, 3) //Inclusao
	
	if lMsErroAuto

		SE1->( rollBackSX8() )
		disarmTransaction()

		lErro := .t.

		if aCritica <> NIL

			aErro := getAutoGrLog()

			varInfo('Erro FINA040', aErro)

			for nX := 1 to len(aErro)

				if 'INVALIDO' $ upper(aErro[nX]) .or. 'AJUDA' $ upper(aErro[nX])

					if cMsgErr == "N�o Informado"
						cMsgErr := ""
					endIf

					cMsgErr += aErro[nX]

				endIf	

			next

			A627RetCri(@aCritica, '29', 0, nil, {}, cMsgErr)

		else
			mostraErro()
		endIf	
	
	else
		SE1->(confirmSx8())
		nRecSe1 := SE1->(recno())
	endIf

else

	SB1->( dbSetOrder(1) ) //B1_FILIAL+B1_COD                                                                                                                                                
	SF4->( dbSetOrder(1) ) //F4_FILIAL+F4_CODIGO                                                                                                                                             
	
   	aStruSF2 := SF2->(dbStruct())
	   
	//Campos do SF2															   
	for nFor := 1 to len(aStruSF2)

		if aStruSF2[nFor][2] $ "C/M"
			aadd(aSF2,"")
		elseIf aStruSF2[nFor][2] == "N"
			aadd(aSF2,0)
		elseIf aStruSF2[nFor][2] == "D"
			aadd(aSF2,cToD("  /  /  "))
		elseIf aStruSF2[nFor][2] == "L"
			aadd(aSF2,.f.)
		endIf

	next nFor

	aStruSD2 	 := SD2->(dbStruct())
	
	nPTotal      := aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_TOTAL"})
	nPDesconto   := aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_DESCON"})
	nPBasImp1    := aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_BASIMP1"})

	cDoc	     := eval({ || nPos := aScan(aCampos,{|x| allTrim(x[1]) == "E1_NUM"}),     iIf(nPos > 0, aCampos[nPos,2], "") } )
	cCliefor     := eval({ || nPos := aScan(aCampos,{|x| allTrim(x[1]) == "E1_CLIENTE"}), iIf(nPos > 0, aCampos[nPos,2], "") } )
	cLoja        := eval({ || nPos := aScan(aCampos,{|x| allTrim(x[1]) == "E1_LOJA"}),    iIf(nPos > 0, aCampos[nPos,2], "") } )
	cNatureza  	 := eval({ || nPos := aScan(aCampos,{|x| allTrim(x[1]) == "E1_NATUREZ"}), iIf(nPos > 0, aCampos[nPos,2], "") } ) 
	dDatEmis     := eval({ || nPos := aScan(aCampos,{|x| allTrim(x[1]) == "E1_EMISSAO"}), iIf(nPos > 0, aCampos[nPos,2], "") } )
	nValor       := eval({ || nPos := aScan(aCampos,{|x| allTrim(x[1]) == "E1_VALOR"}),   iIf(nPos > 0, aCampos[nPos,2], "") } )

	cSerieNFS    := cPrefixo + Space(nTamSerie - len(cPrefixo))
	cItem        := strZero(0, len(SD2->D2_ITEM))

	//Posiciona no cliente...
	if cCliefor <> SA1->A1_COD   

		SA1->( dbSetOrder(1) )
		if ! SA1->(MsSeek(xFilial("SA1") + cCliefor + cLoja))
			FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "SA1 - nao encontrado plstose1" , 0, 0, {})
		endIf

	endIf

	if empty(__cCondPag)
		plCodPag()
	endIf	 

	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_FILIAL"})]  	:= xFilial("SD2")
	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_TIPO"})]    	:= "N"
	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_DOC"})]     	:= cDoc
	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_EMISSAO"})] 	:= dDatEmis
	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_CLIENT"})]  	:= cClieFor
	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_CLIENTE"})]  	:= cClieFor
	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_LOJA"})]  	:= cLoja
	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_LOJENT"})]  	:= cLoja
	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_ESPECIE"})] 	:= A460Especie(cPrefixo)
	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_COND"})]      := __cCondPag
	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_DTDIGIT"})] 	:= dDataBase
	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_EST"})]     	:= SA1->A1_EST
	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_FORMUL"})]  	:= "S"
	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_SERIE"})]  	:= cSerieNFS
	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_VALMERC"})]  	:= nValor
	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_VALBRUT"})]  	:= nValor

	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_TIPOCLI"})] 	:= SA1->A1_TIPO
	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_HORA"})]    	:= subStr(time(),1,5)
	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_TIPODOC"})] 	:= "01"
	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_MOEDA"})]  	:= 1
	aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_TXMOEDA"})] 	:= 1
	
	if SF2->(fieldPos("F2_XPERIOD")) > 0
		aSF2[aScan(aStruSF2,{|x| allTrim(x[1]) == "F2_XPERIOD"})] := strZero(nPeriod,2)
	endIf	

		//Esta ordenacao se faz necessaria para lancamento de credito ao debito  
		//Somente se existir lancamento de Debito								   
	If lGerNcc 
		aVlrCob := aSort(aVlrCob,,, { |x, y| x[1] > y[1] } )
	Endif	

	//Inicia o processo de aglutinacao e montagem da matriz para criacao do SD2 
	for nFor := 1 to len(aVlrCob)

		If lGerNcc 

			lCredito := ( aVlrCob[nFor,1] == '2' )		
			
			//Esta aglutinacao se refere somente a procura de um Credito para um Debito  
			If lCredito
				aRetFind := PLSCRTODE(aVlrCob,aVlrCob[nFor],Iif(nPeriod > 0,.T.,.F.))
				If aRetFind[1] <> 0
					AaDd( aRetDes, aRetFind  )
					Loop
				EndIf	
			EndIf

		EndIf
		PLAGLUSD2(aStruSD2, aItemSD2, aVlrCob[nFor], aSD2_BM1, aRetDes, aDocOri, @cItem, aCampos, cPrefixo, cClieFor, cLoja, dDatEmis, SA1->A1_EST, cSerieNFS)
	next                                        
	//TODO
	//checar se o produto tem imposto na linha
	//verificar se o valor da base e menor que o valor do desconto e retirar
	//verificar se ainda tem saldo de desconto e retirar da proxima linha
	//cada linha que tiver base alterada deve ser implementada na funcao MaFisAlt("IT_BASEIRR", 10000, 1)

	//Base																	   
	if cPaisLOC == "URU"
		aEval( aItemSD2, {|x| ( x[nPBasImp1] := ( x[nPTotal] - x[nPDesconto] ) ), nBasImp1 += x[nPBasImp1] } )
	endIf

	//P.E criado para alteracao dos campos na SE2 antes da integracao com SIGAFAT																	   
	if existBlock("PLSITEMO")
	   aItemSD2 := execBlock("PLSITEMO", .f., .f., { aItemSD2, aBases, aStruSD2 } )
	endIf
	
	//SF2	
	cTextoSF2  += ' MaFisAlt("NF_NATUREZA" , cNatureza, , , , , , .f. /*lRecal*/) '
	bFiscalSF2 := &( '{||' + cTextoSF2 + '}' )
	
	cNumNFS	:= MaNfs2Nfs(cSerOri,;
						cNumORI,;
						cClieFor,;
						cLoja,;
						cSerieNFS,;
						lMostraCtb,;
						lAglutCtb,;
						lCtbOnLine,;
						lCtbCusto,;
						lReajusta,;
						nCalAcrs,;
						nArredPrcLis,;
						lAtuSA7,;
						lECF,;
						bFilSD2,;
						bSD2,;
						bSF2,;
						bTTS,;
						aDocOri,;
						aItemSD2,;
						aSF2,;
						lNoFiscal,;
						bFiscalSF2,;
						bFiscalSD2,;
						bFatSE1,;
						cNumero)

	if ! SE1->( eof() )

		//Salva posicao SE1                                                          
		nRecSe1 := SE1->( recno() )

		SE1->(dbSetorder(2))//E1_FILIAL+E1_CLIENTE+E1_LOJA+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO 
		SF2->(dbSetOrder(1))//F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENTE+F2_LOJA+F2_FORMUL
		SD2->(dbSetOrder(3))//D2_FILIAL+D2_DOC+D2_SERIE+D2_CLIENTE+D2_LOJA+D2_COD+D2_ITEM
		if SE1->(mSseek(Xfilial("SE1") + cClieFor + cLoja + cSerieNFS + cNumero ) )
		
			while ! SE1->( eof() ) .and. alltrim( SE1->( E1_CLIENTE + E1_LOJA + E1_PREFIXO + E1_NUM ) ) == alltrim( cClieFor + cLoja + cSerieNFS + cNumero )
		
				SE1->( reclock("SE1", .f.) )

					for nFor := 1 to len(aCampos)

						if aCampos[nFor, 1] $ cCampos
							&('SE1->' + aCampos[nFor, 1]) := aCampos[nFor, 2]					
						endIf	

					next

				SE1->(msUnLock())	
				
				//Atualiza campos do BM1 com dados do SD2 e SF2                              
				if SF2->( msSeek( xFilial("SF2") + SE1->(E1_NUM + E1_PREFIXO + E1_CLIENTE + E1_LOJA) ) )

					for nFor := 1 to len(aSD2_BM1)
				
						if SD2->( msSeek( xFilial("SD2") + SF2->( F2_DOC + F2_SERIE + F2_CLIENTE + F2_LOJA + aSD2_BM1[nFor,3] + aSD2_BM1[nFor,1] ) )  )
					
							for nX := 1 to len(aSD2_BM1[nFor,2])					
				
								BM1->( dbGoto(aSD2_BM1[nFor,2,nX]) )
				
								if ! BM1->( eof() ) 
									
									BM1->(recLock("BM1",.f.))
										BM1->BM1_DOCSF2 := SF2->F2_DOC
										BM1->BM1_SERSF2 := SF2->F2_SERIE
										BM1->BM1_ITESD2 := SD2->D2_ITEM
										BM1->BM1_SEQSD2 := SD2->D2_NUMSEQ
									BM1->(msUnLock())                    

								else 
									FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "BM1 - recno nao encontrado - aSD2_BM1[nFor,2,nX] (PLSTOSE1)" , 0, 0, {})
								endIf	

							next

						endIf

					next

				else

					FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "SF2 NAO ENCONTRADO" , 0, 0, {})
					FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "SE1       -> ["+SE1->(E1_NUM+E1_PREFIXO+E1_CLIENTE+E1_LOJA)+"]" , 0, 0, {})
					FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "VARIAVEIS -> ["+cNumero+cSerieNFS+cClieFor+cLoja+"]" , 0, 0, {})
					FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "cNumNFS   -> ["+cNumNFS+"]" , 0, 0, {})

				endIf

			SE1->( dbSkip() )
			enddo	

		else
		
			FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "SE1 EM EOF" , 0, 0, {})
			FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "VARIAVEIS -> ["+cNumero+"-"+cSerieNFS+"-"+cClieFor+"-"+cLoja+"]" , 0, 0, {})

		endIf
			
	endIf		

endIf

//Calcula impostos                                                           
if ! lErro .and. nRecSe1 > 0

	SE1->( dbGoto(nRecSe1) ) 
	
	PLSLOGFAT("DADOS DA NOTA/TITULO GERADO", 1, .f.)
		
	//verifica se tem titulo a ser compensado ncc com saldo restante anterior ou a criada neste momento.
	if BG9->(fieldPos("BG9_COMAUT")) > 0
		
		aCliente := PLS770NIV(BA3->BA3_CODINT, BA3->BA3_CODEMP, BA3->BA3_MATRIC, iIf(BA3->BA3_TIPOUS == "1", "F", "J"),;
							BA3->BA3_CONEMP, BA3->BA3_VERCON, BA3->BA3_SUBCON, BA3->BA3_VERSUB, 1)
		
		cNivel := aCliente[1][18] 
			
		// Empresa
		if cNivel == "1" 
			
			BG9->(dbSetOrder(1))
			BG9->(msSeek(xFilial("BA3") + BA3->BA3_CODINT+BA3->BA3_CODEMP))
				
			lBXComp := ( BG9->BG9_COMAUT == "1" )
			
			//Nivel contrato
		ElseIf cNivel == "2" 
				
			BT5->(dbSetOrder(1))
			BT5->(msSeek(xFilial("BA3") + BA3->BA3_CODINT+BA3->BA3_CODEMP+BA3->BA3_CONEMP+BA3->BA3_VERCON))
			lBXComp := ( BT5->BT5_COMAUT == "1" )
			
			//Nivel subcontrato
		ElseIf cNivel == "3"  
				
			BQC->(dbSetOrder(1))
			BQC->(msSeek(xFilial("BA3") + BA3->BA3_CODINT+BA3->BA3_CODEMP+BA3->BA3_CONEMP+BA3->BA3_VERCON+BA3->BA3_SUBCON+BA3->BA3_VERSUB))
			lBXComp := ( BQC->BQC_COMAUT == "1" )

			//Nivel familia
		ElseIf cNivel == "4" 

			lBXComp := ( BA3->BA3_COMAUT == "1" )

		EndIf

		if lBXComp
			lComp := PLTITBXCR() 
		EndIf	

	Endif	
	
endIf

SE1->( dbGoto(nRecSe1) ) 

//Chama ponto de entrada para usuario poder atualizar campos no SE1          
if  existBlock("PLSE1GRV") 

    if ! lGerNFBRA .or. ( lGerNFBRA .And. ! SF2->( eof() ) )
		    
		execBlock("PLSE1GRV",.f.,.f.,{ lComp, nRecSe1, aBases } )
		
		SE1->( dbGoto(nRecSe1) )
		
	    PLSLOGFAT("PLSE1GRV",1,.f.)

	endIf

endIf

if lTitDesc .and. type("M->BE1_QUACOB") == "C" .And. M->BE1_QUACOB == "1"
   lPgtAto := .t.
endIf

//Verifica se o desconto do plano deve ser feito na producao medica,         
//para o caso dos medicos/secretarias que tem plano de saude                 
if  BA3->(fieldPos("BA3_CODRDA")) > 0 .and. ! lPgtAto .And. ! empty(BA3->BA3_CODRDA) .and.;
	( ( lCusOpe .and. '2' $ getNewPar("MV_PLSMDCB","1,2") ) .or.; 	// Autorizado desconto do custo.
	  ( ! lCusOpe .and. '1' $ getNewPar("MV_PLSMDCB","1,2") ) )	 	// Autorizado desconto de tudo.
	
	if BA3->( fieldPos("BA3_CODLAN") ) > 0
		cCodDeb := BA3->BA3_CODLAN
	endIf

	if Empty(cCodDeb)
		cCodDeb := allTrim(getNewPar("MV_PLCDESC",""))
	endIf
		
	BAU->(dbSetOrder(1))
	if ! Empty(cCodDeb) .And. BAU->(msSeek(xFilial("BAU")+BA3->BA3_CODRDA))
			
		//Inicializa variaveis                                                       
		cNumTit   	:= SE1->E1_PREFIXO + SE1->E1_NUM + SE1->E1_PARCELA
		dBaixa    	:= SE1->E1_EMISSAO
		cTipo     	:= SE1->E1_TIPO
		cNsNum    	:= " "
		dDataCred 	:= SE1->E1_EMISSAO
		nDespes   	:= 0
		nDescont  	:= 0
		nValRec   	:= SE1->E1_SALDO
		nJuros    	:= 0
		nMulta    	:= 0
		nCM       	:= 0
		nAcresc   	:= 0
		nDescresc 	:= 0
		nTotAbat  	:= 0
		cLoteFin  	:= ""
		cMarca    	:= ""
		cMotBx    	:= "DAC"
		cHist070  	:= ""
		cBanco    	:= ""
		cAgencia  	:= ""
		cConta    	:= ""
		nValEstrang := 0
			
        //Baixa titulo por cancelamento no Contas a Receber                                           
		fA070Grv(lContabiliza, .f., lContabiliza, cNsNum, .f., SE1->E1_EMISSAO, .f., "", "")

		//Se o saldo for zerado, atualizo o status da Guia
		if SE1->E1_SALDO == 0
			PLSTitStat()
		endIf

		//Posiciona BBB-Tipo Lancamento Debito/Credito                        
		if  BBB->BBB_CODSER <> cCodDeb
			BBB->(dbSetOrder(1))
			BBB->(msSeek(xFilial("BBB")+cCodDeb))
		endIf

		//Esse item se faz necessario devido que estamos no meio da gera��o dos titulos de debito e credito vindo no While do PLSA627.
		//Precisamos abater do BGQ os debitos dos creditos para que n�o ocorra lan�amentos indevidos.
		//A ideia � que se ja temos o lan�ameto ja criado na BGQ efetuamos somente o debito ou credito no valor liquido
		cSql := " SELECT BGQ_VALOR, BGQ_VALOR FROM " + retSqlName("BGQ")
		cSql += " WHERE BGQ_FILIAL = '"+xFilial("BGQ")+"' "
		cSql += " AND BGQ_CODOPE = '"+BA3->BA3_CODINT+"' "
		cSql += " AND BGQ_CODIGO = '"+BA3->BA3_CODRDA+"' "
		cSql += " AND BGQ_ANO = '"+cAnoref+"' "
		cSql += " AND BGQ_MES = '"+cMesRef+"' "
		cSql += " AND BGQ_CODLAN = '"+cCodDeb+"' "
		cSql += " AND BGQ_PREFIX = '"+SE1->E1_PREFIXO+"' "
		cSql += " AND BGQ_NUMTIT = '"+SE1->E1_NUM+"' "
		cSql += " AND BGQ_PARCEL = '"+SE1->E1_PARCELA+"' "
		cSql += " AND D_E_L_E_T_ = ' ' "
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cSQL),"TRB",.T.,.F.)
		If !TRB->(Eof())
			lNewRec := .F.
			nValorBGQ := Abs(If(lNCC, TRB->BGQ_VALOR-SE1->E1_VALLIQ, TRB->BGQ_VALOR+SE1->E1_VALLIQ))
		Else
			nValorBGQ := SE1->E1_VALLIQ		
		Endif
		TRB->( dbCloseArea())

		// Grava BGQ-Deb/Cred Mensal com o valor do titulo                     
		BGQ->(recLock("BGQ",lNewRec))
		BGQ->BGQ_FILIAL := xFilial("BGQ")
		BGQ->BGQ_CODSEQ := getSx8Num("BGQ","BGQ_CODSEQ")
		BGQ->BGQ_CODIGO := BA3->BA3_CODRDA
		BGQ->BGQ_NOME   := BAU->BAU_NOME
		BGQ->BGQ_ANO    := cAnoref
		BGQ->BGQ_MES    := cMesRef
		BGQ->BGQ_CODLAN := BBB->BBB_CODSER
		BGQ->BGQ_VALOR  := nValorBGQ
		BGQ->BGQ_TIPO   := BBB->BBB_TIPSER
		BGQ->BGQ_TIPOCT := BBB->BBB_TIPOCT
		BGQ->BGQ_INCIR  := BBB->BBB_INCIR
		BGQ->BGQ_INCINS := BBB->BBB_INCINS
		BGQ->BGQ_INCPIS := BBB->BBB_INCPIS
		BGQ->BGQ_INCCOF := BBB->BBB_INCCOF
		BGQ->BGQ_INCCSL := BBB->BBB_INCCSL
		BGQ->BGQ_VERBA  := BBB->BBB_VERBA
		BGQ->BGQ_CODOPE := plsIntPad()
		BGQ->BGQ_CONMFT := BBB->BBB_CONMFT
		BGQ->BGQ_OBS    := STR0001 //"GERADO PELA ROTINA DE LOTE DE COBRANCA"
		BGQ->BGQ_LANAUT := "1"
		BGQ->BGQ_OPELAU := SE1->E1_CODINT
		BGQ->BGQ_PREFIX := SE1->E1_PREFIXO
		BGQ->BGQ_NUMTIT := SE1->E1_NUM
		BGQ->BGQ_PARCEL := SE1->E1_PARCELA
		BGQ->BGQ_TIPTIT := iif(lNewRec,SE1->E1_TIPO,BGQ->BGQ_TIPTIT)
		BGQ->BGQ_INTERC := "0"
		BGQ->(msUnLock())

		confirmSx8()
			
        //Chama ponto de entrada para usuario poder atualizar campos no BGQ          
		if  existBlock("PLBGQGRV")
			execBlock("PLBGQGRV",.f.,.f.)
		endIf

	endIf

	PLSLOGFAT("COMPLEMENTOS APOS GRAVACAO DO TITULO/NF",1,.f.)
	
endIf

restArea(aArea)

//Posiciona no SE1 gerado                                                    
if lPosSE1Ger	
	SE1->( dbGoto(nRecSe1) )                
endIf

return(lErro)

/*/{Protheus.doc} plCodPag
retorna condicao de pagamento avista	

@author  PLS TEAM
@version P12
@since   29/04/10
/*/
static function plCodPag
local cCondPag := ''
local cCondAux := ''

// tratamento para condi��o de pagamento
SE4->(dbSetOrder(1))
SE4->(dbGoTop())
SE4->(DBSeek(xFilial("SE4")))

while ! SE4->(eof()) .And. xFilial("SE4") == SE4->E4_FILIAL

	cCondAux := SE4->E4_CODIGO

	if allTrim(SE4->E4_COND) == '0' .And. !Empty(SE4->E4_CODIGO)
		cCondPag := SE4->E4_CODIGO
		exit
	endIf

SE4->(dbSkip())
endDo   

if empty(cCondPag)
	
	cCondPag := soma1(cCondAux)

	SE4->(recLock("SE4",.t.))
		SE4->E4_FILIAL := xFilial("SE4")
		SE4->E4_CODIGO := cCondPag
		SE4->E4_TIPO   := "1"
		SE4->E4_COND   := "0"
		SE4->E4_DESCRI := "AVISTA"
	SE4->(msUnLock())

endIf

__cCondPag := cCondPag

return()

/*/{Protheus.doc} PLAGLUSD2
Aglutina itens para o sd2 nf	

@author  PLS TEAM
@version P12
@since   29/04/10
/*/
static function PLAGLUSD2(aStruSD2, aItemSD2, aVlrCob, aSD2_BM1, aRetDes, aDocOri, cItem, aCampos, cPrefixo, cClieFor, cLoja, dDatEmis, cEstado, cSerieNFS)
local nI		:= 0
local nX		:= 0
local _nPos 	:= 0
local nPosD		:= 0                                                                                    
local nPos		:= 0
local nValorItem:= 0
local nAnaAglu	:= 0                                                  
local lD2PLSFIL := SD2->( fieldPos("D2_CODINT") ) > 0 .and. SD2->( fieldPos("D2_CODEMP") ) > 0 .and. SD2->( fieldPos("D2_CONEMP") ) > 0 .and. SD2->( fieldPos("D2_VERCON") ) > 0 .and. SD2->( fieldPos("D2_SUBCON") ) > 0 .and. SD2->( fieldPos("D2_VERSUB") ) > 0 .and. SD2->( fieldPos("D2_MATRIC") ) > 0
local cCodInt   := ""
local cCodEmp   := ""
local cConEmp   := ""
local cVerCon   := ""
local cSubCon   := ""
local cVerSub   := ""
local cMatric   := ""
local cCodProSB1:= ""
local cCodTES	:= ""
local lAglutina := iIf(cPaisLOC != "BRA", .t., ((BQC->(fieldPos("BQC_AGLUT")) > 0 .and. BQC->BQC_AGLUT == "1") .or. (BA3->BA3_COBNIV=='1' .and. BA3->(fieldPos("BA3_AGLUT")) > 0 .and. BA3->BA3_AGLUT == "1")))
local lAglu		:= .f.

local nPFilial  := aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_FILIAL"})
local nPItem    := aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_ITEM"})
local nPCod 	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_COD"})
local nPPrcVen	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_PRCVEN"})
local nPTES		:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_TES"})
local nPQuant   := aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_QUANT"})
local nPTotal	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_TOTAL"})
local nPUM		:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_UM"})
local nPCliente	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_CLIENTE"})
local nPLoja	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_LOJA"})
local nPEmis	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_EMISSAO"})
local nPTipo	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_TIPO"})
local nPEspecie	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_ESPECIE"})
local nPTipoDOC	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_TIPODOC"})
local nPDescrip	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_DESCRIP"})
local nPPrUnit	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_PRUNIT"})
local nPDtDigi	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_DTDIGIT"})
local nPFormul	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_FORMUL"})
local nCodInt	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_CODINT"})
local nCodEmp	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_CODEMP"})
local nConEmp	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_CONEMP"})
local nVerCon	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_VERCON"})
local nSubCon	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_SUBCON"})
local nVerSub	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_VERSUB"})
local nConta	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_CONTA"})
local nLocal	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_LOCAL"})
local nGrupo	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_GRUPO"})
local nPEstado	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_EST"})
local nPSerie	:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_SERIE"})
local nPCF 		:= aScan(aStruSD2,{|x| allTrim(x[1]) == "D2_CF"})
local nPDesconto:= aScan(aStruSD2,{|x| AllTrim(x[1]) == "D2_DESCON"})
local nCodISS	:= aScan(aStruSD2,{|x| AllTrim(x[1]) == "D2_CODISS"})
local lGerNcc	:= GetNewPar("MV_PLGENCC","0") == "1"  // Modelo antigo de gerar ncc o credito e descontado no valor total da nota


//Valores														
nValorItem := aVlrCob[2]
cCodProSB1 := aVlrCob[37]
cCodTES    := aVlrCob[38]

//Aglutina ou gera um novo registro							
if lAglutina .and. len(aItemSD2) > 0

	//Neste ponto esta funcao esta aglutinando os lancamentos da nota		  
	//Aqui os itens da nota ja estao definidos (nao tem lancamento de debito)
	nAnaAglu := PLSAGL(aVlrCob, aItemSD2, aStruSD2, cCodProSB1, cCodTES, nValorItem)                                         
	lAglu 	 := .f.
	
	if nAnaAglu > 0

		//Aglutina valores											
		aItemSD2[nAnaAglu][nPQuant] 	+= 1
		aItemSD2[nAnaAglu][nPTotal] 	+= nValorItem

		//Recalcula o Preco Unitario de acordo com Quantidade e Total.
		//Utiliza fun��o de arredondamento que trata o par�metro MV_ARREFAT para a  
		//gravacao da tabela SD2											       	  
		aItemSD2[nAnaAglu][nPPrcVen]   := A410Arred(aItemSD2[nAnaAglu][nPTotal] / aItemSD2[nAnaAglu][nPQuant], "D2_PRCVEN")   
	
		lAglu := .t.

	endIf

endIf

if ! lAglu

	cItem := soma1(cItem)
	aadd( aDocOri, 0 )
	aadd( aItemSD2, {} )
	nIteNota := len(aItemSD2)
	
	//Atualiza campos												
	for nX := 1 to len(aStruSD2)

		if aStruSD2[nX][2] $ "C/M"
			aadd(aItemSD2[nIteNota], "")
		elseIf aStruSD2[nX][2] == "D"
			aadd(aItemSD2[nIteNota], cToD(""))
		elseIf aStruSD2[nX][2] == "N"
			aadd(aItemSD2[nIteNota], 0)
		elseIf aStruSD2[nX][2] == "L"
			aadd(aItemSD2[nIteNota], .t.)
		endIf

	next nX

	if ! SB1->( msSeek( xFilial("SB1") + cCodProSB1) )
		FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "SB1 - nao encontrado plstose1" , 0, 0, {})
	endIf

	if ! SF4->( msSeek( xFilial("SF4") + cCodTES) )
		FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "SF4 - nao encontrado plstose1" , 0, 0, {})
	endIf

	aItemSD2[nIteNota][nPFilial]	:= xFilial("SD2")
	aItemSD2[nIteNota][nPItem]		:= cItem
	aItemSD2[nIteNota][nPQuant]		:= 1
	aItemSD2[nIteNota][nPPrcVen]	:= nValorItem
	aItemSD2[nIteNota][nPPrUnit]	:= nValorItem
	aItemSD2[nIteNota][nPTotal]		:= nValorItem

	if lGerNcc
		aItemSD2[nIteNota][nPDesconto] := iIf( Len(aVlrCob) > 39, aVlrCob[40], 0)
	endIf

	aItemSD2[nIteNota][nPCod]		:= cCodProSB1
	aItemSD2[nIteNota][nPTES]		:= cCodTES
	aItemSD2[nIteNota][nPCF]		:= SF4->F4_CF
	aItemSD2[nIteNota][nPUM]		:= SB1->B1_UM
	aItemSD2[nIteNota][nConta]		:= SB1->B1_CONTA
	aItemSD2[nIteNota][nLocal]		:= SB1->B1_LOCPAD
	aItemSD2[nIteNota][nGrupo]		:= SB1->B1_GRUPO
	aItemSD2[nIteNota][nCodISS]		:= SB1->B1_CODISS

	aItemSD2[nIteNota][nPCliente]	:= cClieFor
	aItemSD2[nIteNota][nPLoja]   	:= cLoja
	aItemSD2[nIteNota][nPEmis]   	:= dDatEmis
	aItemSD2[nIteNota][nPEstado]   	:= cEstado
	aItemSD2[nIteNota][nPTipo]   	:= "N"
	aItemSD2[nIteNota][nPTipoDoc] 	:= "01"
	aItemSD2[nIteNota][nPDtDigi]	:= dDataBase
	aItemSD2[nIteNota][nPFormul]	:= "S"
	aItemSD2[nIteNota][nPSerie]		:= cSerieNFS
	
	if nPEspecie > 0
		aItemSD2[nIteNota][nPEspecie] := A460Especie(cPrefixo)
	endIf	

	if nPDescrip > 0
		aItemSD2[nIteNota][nPDescrip] := SB1->B1_DESC
	endIf	

    if lD2PLSFIL

		cCodInt   := eval({ || nPos := aScan(aCampos,{|x| x[1] == 'E1_CODINT'}), if(nPos>0,aCampos[nPos,2],"") })
		cCodEmp   := eval({ || nPos := aScan(aCampos,{|x| x[1] == 'E1_CODEMP'}), if(nPos>0,aCampos[nPos,2],"") })
		cConEmp   := eval({ || nPos := aScan(aCampos,{|x| x[1] == 'E1_CONEMP'}), if(nPos>0,aCampos[nPos,2],"") })
		cVerCon   := eval({ || nPos := aScan(aCampos,{|x| x[1] == 'E1_VERCON'}), if(nPos>0,aCampos[nPos,2],"") })
		cSubCon   := eval({ || nPos := aScan(aCampos,{|x| x[1] == 'E1_SUBCON'}), if(nPos>0,aCampos[nPos,2],"") })
		cVerSub   := eval({ || nPos := aScan(aCampos,{|x| x[1] == 'E1_VERSUB'}), if(nPos>0,aCampos[nPos,2],"") })
		cMatric   := eval({ || nPos := aScan(aCampos,{|x| x[1] == 'E1_MATRIC'}), if(nPos>0,aCampos[nPos,2],"") })

	   	aItemSD2[nIteNota][nCodInt] := cCodInt
	   	aItemSD2[nIteNota][nCodEmp] := cCodEmp
	   	aItemSD2[nIteNota][nConEmp] := cConEmp
	   	aItemSD2[nIteNota][nVerCon] := cVerCon
	   	aItemSD2[nIteNota][nSubCon] := cSubCon
	   	aItemSD2[nIteNota][nVerSub] := cVerSub

	endIf
	
	//Gravacao de novos campos no SD2 com base no ponto de entrada PL627AGL	  
	if  existBlock("PL627AGL") .and. len(aVlrCob) > 63 .and. len(aVlrCob[64]) > 0

		aAuxAGL	:= aVlrCob[64]

		for nI := 1 to len(aAuxAGL)

			if (nPosAGL := aScan( aStruSD2, {|x| allTrim(x[1]) == allTrim(aAuxAGL[nI,2]) } ) ) > 0

				aItemSD2[nIteNota, nPosAGL] := aAuxAGL[nI,1]

			endIf

		next

		PLSLOGFAT("PL627AGL",1,.f.)

	endIf
	
endIf

//Iniciar pesquisa de relacionamento sd2 com bm1
if nPItem > 0 .and. nAnaAglu > 0				
	_nPos := aScan( aSD2_BM1 , {|x| x[1] == aItemSD2[nAnaAglu, nPItem]} )
endIf	

if _nPos > 0
	
	//Relacionamento do debito com SD2							
	aadd( aSD2_BM1[_nPos,2], aVlrCob[39] )
	
	//Relacionamento do credito com SD2							
	while ( nPosD := aScan( aRetDes, { |x| x[1] == aVlrCob[39] }, (nPosD+1) ) ) > 0
		
		aadd( aSD2_BM1[_nPos, 2], aRetDes[nPosD, 2] )
		aRetDes[nPosD,1] := 0

	endDo

else
	
	aadd(aSD2_BM1, { cItem, { aVlrCob[39] }, aVlrCob[37] } )

endIf

return

/*/{Protheus.doc} PLSAGL
Retorna condicao de aglutinacao	
@author  PLS TEAM
@since   20173007
@version p11
/*/
static function PLSAGL(aVlrCob, aItemSD2, aStruSD2, cCodProSB1, cCodTES, nValorItem, cMatric, lCredito)
local nJ			:= 0
local nPos			:= 0
local nPos1			:= 0
local bAglut 		:= ""
local lD2MATRIC		:= SD2->( fieldPos("D2_MATRIC") ) > 0
local aAuxAGL 		:= {}

default lCredito	:= .f.
default cMatric		:= ""

// Conteudo Default														   
bAglut := " {|| ascan( aItemSD2, { |x| "

nPos := aScan(aStruSD2, {|x| allTrim(x[1]) == 'D2_COD'})
if nPos > 0
	bAglut += " x[" + cValToChar(nPos) + "] == '" + cCodProSB1 + "' .and. "
endIf	

nPos := aScan(aStruSD2, {|x| allTrim(x[1]) == 'D2_TES'})
if nPos > 0
	bAglut += " x[" + cValToChar(nPos) + "] == '" + cCodTES + "' "
endIf	

// Nao considera valor na pesquisa de credito								   
if ! lCredito .and. getNewPar("MV_PLSPRAG","0") == "1"

	nPos := aScan(aStruSD2, {|x| allTrim(x[1]) == 'D2_PRCVEN'})
	if nPos > 0
		bAglut += " .and.  abs( x[" + cValToChar(nPos) + "] ) == abs(" + cValorItem + ") "
	endIf	

endIf

if ! empty(cMatric) .and. lD2MATRIC

	nPos := aScan(aStruSD2, {|x| allTrim(x[1]) == 'D2_MATRIC'})
	if nPos > 0
		bAglut += " .and. x[" + cValToChar(nPos) + "] == '" + cMatric + "' "
	endIf	
	
	// Se considera ou nao o valor liquido na regra de aglutinacao				   
	if getNewPar("MV_PLSVLDI","0") == "1"

		bAglut += " .and. x[1] == '1' "
		
		nPos  := aScan(aStruSD2, {|x| allTrim(x[1]) == 'D2_PRCVEN'})
		nPos1 := aScan(aStruSD2, {|x| allTrim(x[1]) == 'D2_DESCON'})

		if nPos > 0 .and. nPos1
			bAglut += " .and. ( abs(x[" + cValToChar(nPos) + "]) - abs(x[" + cValToChar(nPos1) + "]) ) >= abs(" + cValorItem + ") "
		endIf	

	endIf

endIf

// Verifica se existem mais campos a serem Aglutinados 					   
if len(aVlrCob) < 64 .or. len(aVlrCob[64]) == 0

	bAglut += "}) }"

else

	aAuxAGL	:= aVlrCob[64]

	for nJ := 1 to Len(aAuxAGL)
		
		nPos := aScan(aStruSD2, {|x| allTrim(x[1]) == aAuxAGL[nJ,2]})

		if aAuxAGL[nJ,3] .and. nPos > 0

			if valType(aAuxAGL[nJ,1]) == "C"
				
				bAglut += " .and. x[" + cValToChar(nPos) + "] == '" + aAuxAGL[nJ,1] + "'  "

			elseIf valType(aAuxAGL[nJ,1]) == "N"

				bAglut += " .and. abs(x[" + cValToChar(nPos) + "]) == abs(" + cValToChar(aAuxAGL[nJ,1]) + ") "

			elseIf valType(aAuxAGL[nJ,1]) == "D"

				bAglut += " .and. DTOS(x[" + cValToChar(nPos) + "]) == '" + DTOS(aAuxAGL[nJ,1]) +  "' "
			
			endIf

		endIf

	next nJ

	bAglut += "}) }"

endIf

return(eval(&bAglut))


/*/{Protheus.doc} PLTITBXCR

Baixa/Estorno titulo NCC 
@author  PLSTEAM
@version P12
@since   04/08/19
/*/
function PLTITBXCR(lEstorno) 
local lRet    	:= .f.
local __lOracle := .f.	
local cSql	  	:= ""
local nSaldo  	:= 0
local aRecTIT 	:= {}
local aRecNCC 	:= {}
local aParam  	:= { .f., .f., .f., .f., .f., .f. }

default lEstorno := .f.

private lMsErroAuto 	:= .f.
private lMsHelpAuto		:= .t.
private lAutoErrNofile	:= .t.

getTpDB(@__lOracle)

if ! lEstorno 

	cSql := " SELECT SE1.R_E_C_N_O_ SE1REC, E1_TIPO, E1_SALDO "
	cSql += "   FROM " + retSqlName("SE1") + " SE1 "
	cSql += "  WHERE SE1.E1_FILIAL  = '" + xFilial('SE1')  + "' " 
	cSql += "    AND SE1.E1_CLIENTE = '" + SE1->E1_CLIENTE + "' "
	cSql += "    AND SE1.E1_LOJA    = '" + SE1->E1_LOJA    + "' "
	cSql += "    AND SE1.E1_PREFIXO = '" + SE1->E1_PREFIXO + "' "
	cSql += "    AND SE1.E1_SITUACA NOT IN " + formatIn( strTran( FN022LSTCB(2), '', ' ' ) , '|') //Lista das situacoes de cobranca (EM PROTESTO)

	if __lOracle
		cSql += " AND TRIM(E1_TITPAI) IS NULL "
		cSql += " AND SE1.E1_ANOBASE || SE1.E1_MESBASE <= '" + SE1->(E1_ANOBASE+E1_MESBASE) + "' "
	else
		cSql += " AND E1_TITPAI = ' ' "
		cSql += " AND SE1.E1_ANOBASE + SE1.E1_MESBASE <= '" + SE1->(E1_ANOBASE+E1_MESBASE) + "' "
	endIf	

	cSql += "    AND SE1.E1_SALDO > 0 "
	cSql += "    AND SE1.D_E_L_E_T_ = ' ' "

	cSql += "   AND EXISTS (SELECT 1 "
	cSql += "   		      FROM " + retSqlName("SE1") + " NCC "
	cSql += "   	  	     WHERE NCC.E1_FILIAL  = '" + xFilial('SE1')  + "' " 
	cSql += "   		       AND NCC.E1_CLIENTE = '" + SE1->E1_CLIENTE + "' "
	cSql += "   		       AND NCC.E1_LOJA    = '" + SE1->E1_LOJA    + "' "
	cSql += "   		       AND NCC.E1_PREFIXO = '" + SE1->E1_PREFIXO + "' "
	cSql += "   		       AND NCC.E1_TIPO	  = '" + MV_CRNEG + "' "
	if __lOracle
		cSql += "              AND NCC.E1_ANOBASE || NCC.E1_MESBASE >= '" + SE1->(E1_ANOBASE+E1_MESBASE) + "' "
	else
		cSql += "              AND NCC.E1_ANOBASE + NCC.E1_MESBASE  >= '" + SE1->(E1_ANOBASE+E1_MESBASE) + "' "
	endIf	
	cSql += "   		       AND NCC.E1_SALDO > 0 "
	cSql += "   		       AND NCC.D_E_L_E_T_ = ' ') "  

	if existBlock('PLRDBXCR')
		cSql := execBlock('PLRDBXCR', .f., .f., { cSql } )	
	endIf

	MPSysOpenQuery(cSql, "TRBSE1")

	while ! TRBSE1->(eof())
		
		if TRBSE1->E1_TIPO == MV_CRNEG
			aadd(aRecNCC, TRBSE1->SE1REC)
		else
			
			aadd(aRecTIT, TRBSE1->SE1REC)

			nSaldo += TRBSE1->E1_SALDO

		endIf	

	TRBSE1->(dbSkip())
	endDo

	TRBSE1->(dbClosearea())

	if len(aRecNCC) > 0 .and. len(aRecTIT) > 0

		lRet := maIntBxCR( 3, aRecTIT, /*aBaixa*/, aRecNCC, /*aLiquidacao*/, aParam,;
						/*bBlock*/,/*aEstorno*/,/*aSE1Dados*/,/*aNewSE1*/, nSaldo, /*aCpoUser*/,;
						/*aNCC_RAvlr*/, /*nSomaCheq*/, /*nTaxaCM*/, /*aTxMoeda*/, /*lConsdAbat*/, /*lRetLoja*/,;
						/*cProcComp*/ )

	endIf

else

	nRecSE1 := SE1->(recno())

	lRet := .t.
	
	//Fina330 - 5 - Estorno, .T. - Automatico
	MSExecAuto({|x, y| Fina330(x, y)}, 5, .t.)

	if lMsErroAuto
		mostraErro()
		lRet := .f.
	endIf

	SE1->(msGoTo(nRecSE1))

endIf	

return lRet


/*/{Protheus.doc} PLSCRTODE

Procura lancamento de Credito para um Debito
@author  PLSTEAM
@version P12
@since   01/01/2010
/*/
Function PLSCRTODE(aVlrCob,aDes,lPeriod)
LOCAL nY 	  := 0
LOCAL nP	  := 0
LOCAL nPos	  := 0
LOCAL nReDBM1 := 0
LOCAL bChkPos := ""
LOCAL nValor  := aDes[2]
LOCAL cCodEve := aDes[4]
LOCAL cMatric := aDes[7]
LOCAL cCodPla := aDes[34]
LOCAL cProdut := aDes[37]
LOCAL cTes 	  := aDes[38]
LOCAL nReCBM1 := aDes[39] //Recno BM1
LOCAL aAuxAGL := aDes[64] //Novos campos para checagem Deb/Cred PL627AGL
LOCAL cAno    := aDes[59]
LOCAL cMes    := aDes[60]
LOCAL lBusca  := .T.
Local lfirst 	:= .F. 
DEFAULT lPeriod := .F.

// CodeBlock de checagem Default	
bChkPos := " {|| Ascan( aVlrCob,{ |x| x[1] == '1'     .And. "
If !Empty(cMatric)
	bChkPos += 						" x[7] == cMatric .And. "
EndIf	
If lPeriod
	bChkPos += 						" x[59] == cAno .And. "
	bChkPos += 						" x[60] == cMes .And. "
EndIf	
bChkPos += 							" x[37] == cProdut .And. "
bChkPos += 							" x[38] == cTes "

// Se considera ou nao o valor liquido na regra de aglutinacao
If GetNewPar("MV_PLSVLDI","0") == "1"
	bChkPos += " .And. ( abs(x[2])-abs(x[40]) ) >= abs(nValor) "
EndIf	

// Inclui os campos referente ao PL627AGL
For nY := 1 to Len(aAuxAGL)
	// Somente os validos para checagem
	If aAuxAGL[nY,3] 
		If ValType(aAuxAGL[nY,1]) == "C"          
			bChkPos     += " .And. x[64," + cValToChar(nY) + ",1] == '" + aAuxAGL[nY,1] +  "' "
		ElseIf ValType(aAuxAGL[nY,1]) == "N"
			bChkPos     += " .And. abs(x[64," + cValToChar(nY) + ",1]) == abs(" + cValToChar(aAuxAGL[nY,1]) +  ") "
		ElseIf ValType(aAuxAGL[nY,1]) == "D"
			bChkPos     += " .And. DTOS(x[64," + cValToChar(nY) + ",1]) == '" + DTOS(aAuxAGL[nY,1]) +  "' "
		EndIf
	EndIf
Next                                            		

// Fechamento do CodeBlock			
bChkPos += "}) }"

// While para procura do debito referente a um credito
// O nivel mais alto e o debito propriamente dito

While .T.                                                      
	
	
	// Executa codeblock				
	nPos := Eval(&bChkPos)

	// Se achou atualiza o desconto e pega o recno do bm1 correspondente
	If nPos > 0  
		If nValor > aVlrCob[nPos,2]
			aVlrCob[nPos,40] += aVlrCob[nPos,2]
			nValor-= aVlrCob[nPos,2]     
		    If !lfirst 
				nP := rAt('}) }',Upper(bChkPos))-1
		    	If nP > -1
					bChkPos := SubStr(bChkPos,1,nP) + " .And. ( abs(x[40])= 0) }) } "
				EndIf 
				lfirst := .T.			
			Endif
			If nValor = 0
				exit
			Else
				loop  
			Endif
					
							
		Else
			aVlrCob[nPos,40] += nValor 
			nReDBM1 := aVlrCob[nPos,39]
			Exit
		Endif	
	
	// Se nao achou na primeira chave vai retirar a ultima posicao de checagem e continuar 
	Else          
		
		// Verifica pais
		If cPaisLoc == "URU"

			// Por definicao do mit - 44 - N13 nao deve procurar ate o nivel mais baixo que e
			// o lancamento de debito propriamente dito
			Exit
		EndIf
		
		// Se entrou aqui e porque nao tinha pelo menos um lancamento de debito
		If nP == -1
			FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "N�o foi possivel fazer o lancamento do desconto para nenhum debito." , 0, 0, {})
			FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "cMatric    - ["+cMatric+"]" , 0, 0, {})
			FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "cCodEveOri - ["+cCodEve+"]" , 0, 0, {})
			FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "cCodPla    - ["+cCodPla+"]" , 0, 0, {})
			FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "cProdut    - ["+cProdut+"]" , 0, 0, {})
			FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "cTes       - ["+cTes+"]" , 0, 0, {})
			FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "nValor     - ["+cValToChar(nValor)+"]" , 0, 0, {})
		  	Exit
		EndIf

		// Retira o ultimo .AND. do codeblock para proxima pesquisa
	    nP := rAt('.AND.',Upper(bChkPos))-1
	    If nP > -1
			bChkPos := SubStr(bChkPos,1,nP) + "}) }"
		EndIf 
    EndIf    
	
	// Somente para garantir que nao vai ficar infinitamento no loop
	If Empty(bChkPos) .Or. rAt('ASCAN(',Upper(bChkPos)) == 0
    	Exit
    EndIf	
EndDo   

// Retorno da funcao
Return( {nReDBM1 , nReCBM1} )