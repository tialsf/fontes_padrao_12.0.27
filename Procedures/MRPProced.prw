#INCLUDE "PROTHEUS.CH"
#INCLUDE "MRPPROCED.CH"

/*/{Protheus.doc} MRPProced
Fonte para executar as procedures referentes ao MRP
@author marcelo.neumann
@since 12/11/2019
@version P12
@param cErro, caracter, mensagem de erro (passado por refer�ncia)
@return lOk, l�gico, indica se tudo foi executado com sucesso
/*/
Function MRPProced(cErro)

	Local lOk := .T.

	lOk := CalcNivel(@cErro)

Return lOk

/*/{Protheus.doc} CalcNivel
Calcula o n�vel de cada Produto do MRP (HWA) de acordo com a Estrutura (T4N)
@author marcelo.neumann
@since 12/11/2019
@version P12
@param cErro, caracter, mensagem de erro (passado por refer�ncia)
@return lOk, l�gico, indica se foi executada com sucesso
/*/
Static Function CalcNivel(cErro)

	Local aResult  := {}
	Local cProcNam := GetSPName("MRP001","24")
	Local lOk      := .T.

	//Verifica se a procedure existe
	If ExistProc(cProcNam, VerIDProc())
		//Executa a procedure no banco recuperando o retorno
		aResult := TCSPEXEC(xProcedures(cProcNam),xFilial("HWA"),xFilial("T4N"))

		//Se ocorreu algum erro, retorna o status de execu��o como falso
		If Empty(aResult) .Or. Valtype(aResult) <> "A"
			cErro := STR0001 + AllTrim(cProcNam) + ": " + TcSqlError() //"Erro na execu��o da Stored Procedure "
			lOk   := .F.
		EndIf
	Else
		cErro := STR0002 + AllTrim(cProcNam) + STR0003 //"Stored Procedure " XXX " n�o instalada no banco de dados."
		lOk   := .F.
	EndIf

Return lOk

/*/{Protheus.doc} VerIDProc
Identifica a sequ�ncia de controle do fonte ADVPL com a stored procedure.
Qualquer altera��o que envolva diretamente a procedure a variavel ser� incrementada.
@author marcelo.neumann
@since 12/11/2019
@version P12
@return vers�o da procedure (compatibilidade)
/*/
Static Function VerIDProc()

Return '002'