#INCLUDE "Protheus.ch"
#INCLUDE "FWADAPTEREAI.CH"

//-------------------------------------------------------------------------------------------------
/*/{Protheus.doc} MATI330

Funcao de integracao com o adapter EAI para recebimento dos dados mensagem ItemCosting

@param   cXml        Vari�vel com conte�do XML para envio/recebimento.
@param   nTypeTrans   Tipo de transa��o. (Envio/Recebimento)
@param   cTypeMessage  Tipo de mensagem. (Business Type, WhoIs, etc)

@author  Matheus Lando Raimundo
@version P11
@since   21/04//2014
@return  aRet   - (array)   Cont�m o resultado da execu��o e a mensagem XML de retorno.
       aRet[1] - (boolean)  Indica o resultado da execu��o da fun��o
       aRet[2] - (caracter) Mensagem XML para envio
/*/
//-------------------------------------------------------------------------------------------------
Function MATI330(cXML,nTypeTrans,cTypeMessage,cAliasTmp)
Local lRet			:= .T.
Local cXMLRet		:= ""
Local cEvent		:= "upsert"  
Local cXmlErro	:= ""
Local cXmlWarn	:= ""   
Local aArea		:= GetArea()
Local lPIMSINT 	:= SuperGetMV("MV_PIMSINT",.F.,.F.)// Indica se Existe Integra��o Protheus x PIMS Graos
Local nCount		:= 0

Private oXmlM330		:= Nil
//Tratamento do recebimento de mensagens
If ( nTypeTrans == TRANS_RECEIVE )

	If cTypeMessage == EAI_MESSAGE_BUSINESS
	//Negocio Recebimento
	ElseIf   cTypeMessage == EAI_MESSAGE_RESPONSE
		oXmlM330 := XmlParser(cXml, "_", @cXmlErro, @cXmlWarn)
		If oXmlM330 <> Nil .And. Empty(cXmlErro) .And. Empty(cXmlWarn) 
			If oXmlM330:_TOTVSMessage:_ResponseMessage:_ProcessingInformation:_Status:Text == "ERROR"
				lRet    := .F.
				cXMLRet := 	"Erro no processamento " + ' | ' +cXmlErro + ' | ' + cXmlWarn //""
			EndIf
		Else
			lRet    := .F.
			cXMLRet := 	"Xml mal formatado " + ' | ' +cXmlErro + ' | ' + cXmlWarn //"Xml mal formatado "
		EndIf
	ElseIf   cTypeMessage == EAI_MESSAGE_WHOIS
		cXMLRet := '1.000'
	EndIf
ElseIf nTypeTrans == TRANS_SEND  
	//Monta XML de envio de mensagem unica
	cXMLRet := "<BusinessEvent>"
	cXMLRet +=		"<Entity>ItemCosting</Entity>"
	cXMLRet +=		"<Event>" +cEvent +"</Event>"           
	cXmlRet += 	"<Identification>"
    cXmlRet +=            '<key name="InternalId">' + cEmpAnt + '|' + RTrim(SB1->B1_FILIAL) + '|' +  RTrim((cAliasTmp)->B2_COD)  + '</key>'
    cXmlRet += 	"</Identification>"
    cXmlRet += "</BusinessEvent>"
    
    cXmlRet += "<BusinessContent>"            
    cXmlRet += "  <CompanyId>" + cEmpAnt + "</CompanyId>"
    cXMLRet += "  <CompanyInternal>" + cEmpAnt + "|" + cFilAnt + "</CompanyInternal>"
    cXmlRet += "  <ItemCode>" +  RTrim((cAliasTmp)->B2_COD)  + "</ItemCode>"                
    cXmlRet += "  <ItemInternalId>" + cEmpAnt + '|' + RTrim(SB1->B1_FILIAL) + '|' +  RTrim((cAliasTmp)->B2_COD) + "</ItemInternalId>"
    cXmlRet += "  <ItemDescription>" + _NoTags(Rtrim(SB1->B1_DESC)) + "</ItemDescription>"
    cXmlRet += "  <ListOfSites>"
    
    BeginSQL Alias "ITEMB2"
		SELECT *
		FROM %Table:SB2% SB2
		WHERE SB2.%NotDel% AND
			SB2.B2_FILIAL = %Exp:(cAliasTmp)->B2_FILIAL% AND
			SB2.B2_COD = %Exp:(cAliasTmp)->B2_COD%
		ORDER BY SB2.B2_LOCAL
	EndSQL
	
          
	While ITEMB2->(!EOF()) 
		nCount := 1
		cXmlRet += "            <SiteItemCosting>"
		cXMLRet += "                  <BranchId>" +RTrim(ITEMB2->B2_FILIAL) + "</BranchId>"
		cXMLRet += "                  <WarehouseCode>" +RTrim(ITEMB2->B2_LOCAL) + "</WarehouseCode> "
		cXMLRet += "                  <WarehouseInternalId>" + cEmpAnt + "|" + RTrim(ITEMB2->B2_FILIAL)+ "|" +RTrim(ITEMB2->B2_LOCAL) +"</WarehouseInternalId> "    
		cXMLRet += "                  <WarehouseDescription>" + _NoTags(Rtrim(ITEMB2->B2_LOCALIZ)) +"</WarehouseDescription> "  //-- PEGAR DA NNR
		
		If !Empty(SB1->B1_UCOM)
			cXMLRet += "                  <LastReceivingPrice>"                                                                                                                                                        
	    	cXMLRet += "					  		 <Date>" + Transform(DToS(SB1->B1_UCOM),"@R 9999-99-99") + "</Date>"
	    	cXMLRet += "							 <Price>" + AllTrim(cValToChar(SB1->B1_UPRC)) + "</Price>"
			cXMLRet += "                  </LastReceivingPrice>"
		EndIf
		
		cXMLRet += "                  <BatchAverageCosting>"
		cXMLRet += "                        <BatchCostingMethod>" + If(mv_par14==3,"1","2") + "</BatchCostingMethod>"
		cXMLRet += "                        <LastUpdate>" + If(lPIMSINT,Transform(DToS(dInicio),"@R 9999-99-99"),Transform(DToS(dDataBase),"@R 9999-99-99")) + "</LastUpdate>"
		cXMLRet += "                        <ListOfBatchAverageCosts>"   
		
		For nCount := 1 to 5           
			cXMLRet += "                        	<UnitValues>"                                
			cXMLRet += "                             <Sequence>" + cValToChar(nCount) + "</Sequence>"
			cXMLRet += "                             <MaterialValue>" + AllTrim(cValToChar(ITEMB2->B2_CMFIM1)) + "</MaterialValue>"
			cXMLRet += "                             <LaborValue>" + "0" + "</LaborValue>"
			cXMLRet += "                             <OverHeadValue>" + "0" + "</OverHeadValue>"  
			cXMLRet += "                        	</UnitValues>"
		Next nCount            
		
		cXMLRet += "                        </ListOfBatchAverageCosts>" 
		cXMLRet += "                  </BatchAverageCosting>"             

		If !Empty(SB1->B1_UCALSTD)
			cXMLRet += "                  <StandardCosting>"      
			cXMLRet += "                        <LastUpdate>" + Transform(DToS(SB1->B1_UCALSTD),"@R 9999-99-99")+ "</LastUpdate>"
			cXMLRet += "                        <ListOfStandardCosts>"
			For nCount := 1 to 5   
				cXMLRet += "                        	<UnitValues>"                                                                                   
				cXMLRet += "                             <Sequence>" + cValToChar(nCount) + "</Sequence>"
				cXMLRet += "                             <MaterialValue>" + AllTrim(cValToChar(xMoeda(SB1->B1_CUSTD,1,nCount,SB1->B1_UCALSTD))) + "</MaterialValue>"
				cXMLRet += "                             <LaborValue>" + "0" + "</LaborValue>"
				cXMLRet += "                             <OverHeadValue>" + "0" + "</OverHeadValue>"
				cXMLRet += "                        	</UnitValues>"
			Next nCount         
			cXMLRet += "                        </ListOfStandardCosts>"                           
			cXMLRet += "                  </StandardCosting>"
		EndIf
		
		cXmlRet += "            </SiteItemCosting>"
		
		ITEMB2->(dbSkip())

	EndDo
	ITEMB2->(dbCloseArea())                 
	cXmlRet += "  </ListOfSites>"
	cXmlRet += "</BusinessContent>" 
EndIf 
RestArea(aArea)

Return {lRet,cXMLRet}
