Create procedure CTB023_##
( 
   @IN_FILIALCOR    Char('CT2_FILIAL'),
   @IN_FILIALATE    Char('CT2_FILIAL'),
   @IN_DATADE       Char(08),
   @IN_DATAATE      Char(08),
   @IN_LMOEDAESP    Char(01),
   @IN_MOEDA        Char('CT7_MOEDA'),
   @IN_TPSALDO      Char('CT2_TPSALD'),
   @IN_MVSOMA       Char(01),
   @OUT_RESULTADO   Char(01) OutPut
 )
as
/* ------------------------------------------------------------------------------------
    Vers�o          - <v>  Protheus P12 </v>
    Assinatura      - <a>  001 </a>
    Fonte Microsiga - <s>  CTBA190.PRW </s>
    Procedure       -      Reprocessamento SigaCTB
    Descricao       - <d>  Refaz saldos de documento  </d>
    Funcao do Siga  -      Ct190DOC() - Refaz saldos de documento n�o trata total informado
    Entrada         - <ri> @IN_FILIALCOR    - Filial Corrente
                           @IN_FILIALATE    - Filial final do processamento
                           @IN_DATADE       - Data Inicial
                           @IN_DATAATE      - Data Final
                           @IN_LMOEDAESP    - Moeda Especifica - '1', todas, exceto orca/o - '0'
                           @IN_MOEDA        - Moeda escolhida  - se '0', todas exceto orcamento
                           @IN_TPSALDO      - Tipos de Saldo a Repropcessar - ('1','2',..)
                           @IN_MVSOMA       - Soma 2 vezes
    Saida           - <o>  @OUT_RESULTADO   - Indica o termino OK da procedure </ro>
    Responsavel :     <r>  Alice Yamamoto	</r>
    Data        :     31/03/2014
-------------------------------------------------------------------------------------- */
declare @cFilial_CT2  char('CT2_FILIAL')
declare @cCT2FilDe char('CT2_FILIAL')
declare @cFilial_CTC  char('CTC_FILIAL')
declare @cFILCT2      char('CT2_FILIAL')
declare @cAux         char(03)
Declare @cCT2_DC      char('CT2_DC')
Declare @cCT2_DATA    Char(08)
Declare @cCT2_LOTE    Char('CT2_LOTE')
Declare @cCT2_SBLOTE  Char('CT2_SBLOTE')
Declare @cCT2_DOC     Char('CT2_DOC')
Declare @cCT2_MOEDLC  Char('CT2_MOEDLC')
Declare @cDataI       Char(08)
Declare @cDataF       Char(08)
Declare @nCT2_VALOR   Float
Declare @nCTC_DEBITO  Float
Declare @nCTC_CREDIT  Float
Declare @nCTC_DIG     Float
Declare @nCTC_DEBITOX Float
Declare @nCTC_CREDITX Float
Declare @nCTC_DIGX    Float
Declare @nCTC_INF     Float
Declare @iRecno       Integer
Declare @iRecnoNew    Integer
Declare @iTranCount   Integer --Var.de ajuste para SQLServer e Sybase.
                             -- Ser� trocada por Commit no CFGX051 ap�s passar pelo Parse

begin
   
   select @OUT_RESULTADO = '0'
   select @cDataI = Substring(@IN_DATADE, 1, 6 )||'01'
   Exec LASTDAY_## @IN_DATAATE, @cDataF OutPut
   
   If @IN_FILIALCOR = ' ' select @cCT2FilDe = ' '
   else select @cCT2FilDe = @IN_FILIALCOR
 
   select @cAux = 'CT2'
   exec XFILIAL_## @cAux, @cCT2FilDe, @cFilial_CT2 OutPut
   
   Declare CUR_CT190DOC insensitive cursor for
      Select CT2_FILIAL, CT2_DC, CT2_DATA, CT2_LOTE, CT2_SBLOTE, CT2_DOC, CT2_MOEDLC, Sum(CT2_VALOR)
        From CT2###
       Where CT2_FILIAL between @cFilial_CT2 and @IN_FILIALATE
         and CT2_DATA   between @cDataI   and @cDataF
         and CT2_TPSALD   = @IN_TPSALDO
         and CT2_DC       != '4'
         and ((CT2_MOEDLC = @IN_MOEDA AND @IN_LMOEDAESP = '1') OR @IN_LMOEDAESP = '0')
         and D_E_L_E_T_ = ' '
   GROUP BY CT2_FILIAL, CT2_DATA, CT2_LOTE, CT2_SBLOTE, CT2_DOC, CT2_DC, CT2_MOEDLC
	ORDER BY CT2_FILIAL, CT2_DATA, CT2_LOTE, CT2_SBLOTE, CT2_DOC,CT2_DC, CT2_MOEDLC
   for read only
   open CUR_CT190DOC
   Fetch CUR_CT190DOC Into @cFILCT2, @cCT2_DC, @cCT2_DATA, @cCT2_LOTE, @cCT2_SBLOTE, @cCT2_DOC, @cCT2_MOEDLC, @nCT2_VALOR
   
   While (@@fetch_status = 0) begin
      select @cAux = 'CTC'
      exec XFILIAL_## @cAux, @cFILCT2, @cFilial_CTC OutPut
      
      Select @nCTC_DEBITOX = 0
      Select @nCTC_CREDITX = 0
      Select @nCTC_DIGX    = 0
      select @nCTC_DIG     = 0
      select @nCTC_DEBITO  = 0
      select @nCTC_CREDIT  = 0
      select @nCTC_INF     = 0
      select @nCT2_VALOR   = @nCT2_VALOR
      
      Select @iRecno = IsNull( MIN(R_E_C_N_O_),0 )
        From CTC###
       Where CTC_FILIAL = @cFilial_CTC
         and CTC_DATA   = @cCT2_DATA
         and CTC_LOTE   = @cCT2_LOTE
         and CTC_SBLOTE = @cCT2_SBLOTE
         and CTC_DOC    = @cCT2_DOC
         and CTC_MOEDA  = @cCT2_MOEDLC
         and CTC_TPSALD = @IN_TPSALDO
         and D_E_L_E_T_ = ' '
      
      If @iRecno = 0 begin
         /* --------------------------------------------------------------------------
            Recupera o R_E_C_N_O_ para ser gravado
            -------------------------------------------------------------------------- */
         select @iRecnoNew = Max(R_E_C_N_O_) FROM CTC###
         select @iRecnoNew = @iRecnoNew + 1
         if (@iRecnoNew is null or @iRecnoNew = 0) select @iRecnoNew = 1
         
         if @cCT2_DC IN ('1','3') begin
            select @nCTC_DEBITOX = Round(@nCT2_VALOR, 2)
         end
         if @cCT2_DC IN ('2','3') begin
            select @nCTC_CREDITX = Round(@nCT2_VALOR, 2)
         end
         If @cCT2_DC = '3' begin
            If @IN_MVSOMA = '1' Select @nCTC_DIGX = Round(@nCT2_VALOR, 2)
            else Select @nCTC_DIGX = Round(( 2 * @nCT2_VALOR ), 2)
         end else Select @nCTC_DIGX = Round(@nCT2_VALOR, 2)
         
      end else begin
         
         Select @nCTC_DIG = CTC_DIG, @nCTC_DEBITO = CTC_DEBITO, @nCTC_CREDIT = CTC_CREDIT, @nCTC_INF = CTC_INF
           From CTC###
          Where R_E_C_N_O_ = @iRecno
         
         if @cCT2_DC = '1' begin
            select @nCTC_DEBITOX = Round(@nCTC_DEBITO + @nCT2_VALOR, 2)
            select @nCTC_CREDITX = Round(@nCTC_CREDIT, 2)
         end
         if @cCT2_DC ='2' begin
            select @nCTC_CREDITX = Round(@nCTC_CREDIT + @nCT2_VALOR, 2)
            select @nCTC_DEBITOX = Round(@nCTC_DEBITO, 2)
         end
         If @cCT2_DC = '3' begin
            select @nCTC_DEBITOX = Round(@nCTC_DEBITO + @nCT2_VALOR, 2)
            select @nCTC_CREDITX = Round(@nCTC_CREDIT + @nCT2_VALOR, 2)
            
            If @IN_MVSOMA = '1' Select @nCTC_DIGX = Round(@nCTC_DIG + @nCT2_VALOR, 2)
            else Select @nCTC_DIGX  = Round(@nCTC_DIG + ( 2 * @nCT2_VALOR ), 2)
            
         end else Select @nCTC_DIGX = Round(@nCTC_DIG + @nCT2_VALOR, 2)
         
      end
      /*---------------------------------------------------------------
        Insercao / Atualizacao CTC
      --------------------------------------------------------------- */
      If @iRecno = 0 begin
         ##TRATARECNO @iRecnoNew\
         Begin Tran
         Insert into CTC### ( CTC_FILIAL, CTC_MOEDA,  CTC_TPSALD,  CTC_DATA,   CTC_LOTE,  CTC_SBLOTE, CTC_DOC,   CTC_STATUS, CTC_DEBITO,  CTC_CREDIT, CTC_DIG,   R_E_C_N_O_ )
                     values( @cFilial_CTC,@cCT2_MOEDLC, @IN_TPSALDO,   @cCT2_DATA,    @cCT2_LOTE, @cCT2_SBLOTE,  @cCT2_DOC,   '1',          @nCTC_DEBITOX, @nCTC_CREDITX, @nCTC_DIGX, @iRecnoNew  )
         Commit Tran
         ##FIMTRATARECNO
      end else begin
         Begin Tran
         Update CTC###
            Set CTC_DEBITO = @nCTC_DEBITOX, CTC_CREDIT = @nCTC_CREDITX, CTC_DIG = @nCTC_DIGX
          Where R_E_C_N_O_ = @iRecno
          Commit Tran
      End
      Fetch CUR_CT190DOC Into @cFILCT2, @cCT2_DC, @cCT2_DATA, @cCT2_LOTE, @cCT2_SBLOTE, @cCT2_DOC, @cCT2_MOEDLC, @nCT2_VALOR
   End
   Close CUR_CT190DOC
   Deallocate CUR_CT190DOC
   /*---------------------------------------------------------------
     Se a execucao foi OK retorna '1'
   --------------------------------------------------------------- */
   select @OUT_RESULTADO = '1'
end
