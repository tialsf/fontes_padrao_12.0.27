Create Procedure MAT020_##
(
 @IN_DATA       Char(08),
 @IN_MOEDA      Float,
 @OUT_TAXA      Float     Output
)

as

/* ---------------------------------------------------------------------------------------------------------------------
    Vers�o      -  <v> Protheus 12 </v>
    Programa    -  <s> RecMoeda </s>
    Procedure   -  <d> Recupera taxa para moeda na data em questao </d>
    Assinatura  -  <a> 001 </a>
    Entrada     -  <ri>
                   @IN_DATA   - Data  da taxa
                   @IN_MOEDA  - Moeda da taxa                     
                   </ri>
    Saida       -  <ro> @OUT_TAXA  - Taxa a ser retornada </ro>
    Autor       :  <r> Vicente Sementilli </r>
    Criacao     :  <dt> 28/07/1998 </dt>
<o>    
    Alteracao   :  Conformidade com Informix
    Autor       :  Marcelo Rodrigues de Oliveira
    Data        :  29/05/2000
</o>  

    Estrutura de chamadas
    ========= == ========

    0.MAT020 - Recupera taxa para moeda na data em questao

 -------------------------------------------------------------------------------------------------------------------- */

declare @DataAnt DateTime
declare @TaxaM2  float
declare @TaxaM3  float
declare @TaxaM4  float
declare @TaxaM5  float

begin                

  if @IN_MOEDA is null or @IN_MOEDA = 0 select @OUT_TAXA = 1
  else begin
    /* ------------------------------------------------------------------------------
      Recupera as taxas de cada Moeda data em questao ou a mair possivel
    ------------------------------------------------------------------------------ */
    select @TaxaM2 = M2_MOEDA2, @TaxaM3 = M2_MOEDA3, @TaxaM4 = M2_MOEDA4, @TaxaM5 = M2_MOEDA5
      from SM2### 
     where M2_DATA = (select MAX(substring(M2_DATA,1,8))
                        from SM2### 
                       where M2_DATA <= @IN_DATA  and  D_E_L_E_T_ = ' ')
      and D_E_L_E_T_ = ' '

    select @OUT_TAXA = 1

    if      @IN_MOEDA = 2 select @OUT_TAXA = @TaxaM2
    else if @IN_MOEDA = 3 select @OUT_TAXA = @TaxaM3
    else if @IN_MOEDA = 4 select @OUT_TAXA = @TaxaM4
    else if @IN_MOEDA = 5 select @OUT_TAXA = @TaxaM5
    If @OUT_TAXA is null select @OUT_TAXA = 1
  end
end
