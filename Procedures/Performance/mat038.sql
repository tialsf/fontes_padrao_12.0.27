Create Procedure MAT038_##
(
 @IN_FILIALCOR    char('B1_FILIAL'),
 @IN_DATA         char(08),
 @IN_MV_RASTRO    char(01),
 @IN_MV_ULMES     char(08),
 @IN_MV_PAR02     integer,
 @IN_MV_CUSZERO   char(01),
 @IN_300SALNEG    char(01),
 @IN_MV_CUSFIFO   char(01),
 @IN_MV_MOEDACM   char(05),
 @IN_CFECHFIFO    char(01),
 @IN_MV_CUSMED    char(01),
 @IN_MV_CUSFIL    char(01),
 @IN_MV_CUSEMP    char(01),
 @IN_MV_PAR04     integer,
 @IN_FILSEQ       integer,
 @IN_MV_WMSNEW    char(01),
 @IN_MV_PRODMOD   char(01),
 @IN_MV_ARQPROD   char(03),
 @OUT_RESULTADO   char(01) OutPut
)
as

/* ---------------------------------------------------------------------------------------------------------------------
    Vers�o      -  <v> Protheus P12 </v>
    Programa    -  <s> mata280 </s>
    Assinatura  -  <a> 002 </a>
    Descricao   -  <d> Fechamento mensal do estoque </d>
    Entrada     -  <ri> @IN_FILIALCOR  - Filial corrente
                        @IN_DATA       - Data de fechamento </ri>

    Saida       -  <ro> @OUT_RESULTADO - Soma das Quantidades do SBF </ro>

    Responsavel :  <r> Ricardo Gon�alves </r>
    Data        :  <dt> 04.10.2001 </dt>

    Estrutura de chamadas
    ========= == ========

    0.MAT038 - Fechamento mensal do estoque
      1.MA280CON - Ponto de Entrada
      1.MAT036 - Efetua a grava��o no arquivo SBJ - Saldos Iniciais por Lote.
        2.MAT029 - Retorna o Saldo inicial por do Produto/Local do arquivo SD5
          3.MAT011 - Pesquisa no SB1 se produto corrente usa rastreabilidade
        2.MAT011 - Pesquisa no SB1 se produto corrente usa rastreabilidade
        2.MAT018 - Converte a Unidade de Medida
      1.MAT018 - Converte a Unidade de Medida
      1.MAT037 - Efetua a grava��o no arquivo SBK - Saldos Iniciais por Localiza��o.
        2.MAT012 - Pesquisa no SB1 se produto corrente usa localizacao fisica
        2.MAT018 - Converte a Unidade de Medida
        2.MAT029 - Retorna o Saldo inicial por do Produto/Local do arquivo SD5
          3.MAT011 - Pesquisa no SB1 se produto corrente usa rastreabilidade

--------------------------------------------------------------------------------------------------------------------- */

Declare @cFil_SB1       Char('B1_FILIAL')
Declare @cFil_SB2       Char('B2_FILIAL')
Declare @cFil_SB9       Char('B9_FILIAL')
Declare @cFil_SBD       Char('BD_FILIAL')
Declare @cFil_SC2       Char('C2_FILIAL')
Declare @cFil_SD1       Char('D1_FILIAL')
Declare @cFil_SD2       Char('D2_FILIAL')
Declare @cFil_SD3       char('D3_FILIAL')
Declare @cFil_SCC       Char('CC_FILIAL')
Declare @cFil_SD8       char('D8_FILIAL')
Declare @lIntDl         char(01)

/* ---------------------------------------------------------------------------------------------------------------------
   Vari�veis para cursor
--------------------------------------------------------------------------------------------------------------------- */
declare @cB2_COD        char('B2_COD')
declare @cB2_LOCAL      char('B2_LOCAL')
declare @SB2_RECNO      int
declare @vRecno         int
declare @nB2_QFIM       decimal( 'B2_QFIM' )
declare @nB2_QFIM2      decimal( 'B2_QFIM2' )
declare @nB2_VFIM1      decimal( 'B2_VFIM1' )
declare @nB2_VFIM2      decimal( 'B2_VFIM2' )
declare @nB2_VFIM3      decimal( 'B2_VFIM3' )
declare @nB2_VFIM4      decimal( 'B2_VFIM4' )
declare @nB2_VFIM5      decimal( 'B2_VFIM5' )
declare @nB2_VFIMFF1    decimal( 'B2_VFIMFF1' )
declare @nB2_VFIMFF2    decimal( 'B2_VFIMFF2' )
declare @nB2_VFIMFF3    decimal( 'B2_VFIMFF3' )
declare @nB2_VFIMFF4    decimal( 'B2_VFIMFF4' )
declare @nB2_VFIMFF5    decimal( 'B2_VFIMFF5' )
declare @nB2_QACLASS    decimal( 'B2_QACLASS' )
declare @nB1_CONV       decimal( 'B1_CONV' )
declare @nB1_CUSTD      decimal( 'B1_CUSTD' )
declare @cB1_MCUSTD     char('B1_MCUSTD')

declare @nB2_CMFIM1     decimal( 'B2_CM1' )
declare @nB2_CMFIM2     decimal( 'B2_CM2' )
declare @nB2_CMFIM3     decimal( 'B2_CM3' )
declare @nB2_CMFIM4     decimal( 'B2_CM4' )
declare @nB2_CMFIM5     decimal( 'B2_CM5' )

declare @nB2_CMRP1      decimal( 'B2_CM1' )
declare @nB2_CMRP2      decimal( 'B2_CM2' )
declare @nB2_CMRP3      decimal( 'B2_CM3' )
declare @nB2_CMRP4      decimal( 'B2_CM4' )
declare @nB2_CMRP5      decimal( 'B2_CM5' )

declare @nB2_VFRP1      decimal( 'B2_VFIM1' )
declare @nB2_VFRP2      decimal( 'B2_VFIM2' )
declare @nB2_VFRP3      decimal( 'B2_VFIM3' )
declare @nB2_VFRP4      decimal( 'B2_VFIM4' )
declare @nB2_VFRP5      decimal( 'B2_VFIM5' )

declare @nBD_QFIM       decimal( 'BD_QFIM' )
declare @nBD_QFIM2UM    decimal( 'BD_QFIM2UM' )
declare @nBD_CUSFIM1    decimal( 'BD_CUSFIM1' )
declare @nBD_CUSFIM2    decimal( 'BD_CUSFIM2' )
declare @nBD_CUSFIM3    decimal( 'BD_CUSFIM3' )
declare @nBD_CUSFIM4    decimal( 'BD_CUSFIM4' )
declare @nBD_CUSFIM5    decimal( 'BD_CUSFIM5' )
declare @cBD_PRODUTO    char('BD_PRODUTO')
declare @cBD_LOCAL      char('BD_LOCAL')
declare @cBD_SEQ        char('BD_SEQ')
declare @vBD_QINI       decimal( 'BD_QINI' )
declare @vBD_QINI2UM    decimal( 'BD_QINI2UM' )
declare @vBD_CUSINI1    decimal( 'BD_CUSINI1' )
declare @vBD_CUSINI2    decimal( 'BD_CUSINI2' )
declare @vBD_CUSINI3    decimal( 'BD_CUSINI3' )
declare @vBD_CUSINI4    decimal( 'BD_CUSINI4' )
declare @vBD_CUSINI5    decimal( 'BD_CUSINI5' )
declare @vBD_QFIM       decimal( 'BD_QFIM' )
declare @vBD_QFIM2UM    decimal( 'BD_QFIM2UM' )
declare @vBD_CUSFIM1    decimal( 'BD_CUSFIM1' )
declare @vBD_CUSFIM2    decimal( 'BD_CUSFIM2' )
declare @vBD_CUSFIM3    decimal( 'BD_CUSFIM3' )
declare @vBD_CUSFIM4    decimal( 'BD_CUSFIM4' )
declare @vBD_CUSFIM5    decimal( 'BD_CUSFIM5' )
declare @cBD_DATA       char('BD_DATA')
declare @cBD_OP         char('BD_OP')

declare @vXXBD_CUSINI2  decimal( 'BD_CUSINI2' )
declare @vXXBD_CUSINI3  decimal( 'BD_CUSINI3' )
declare @vXXBD_CUSINI4  decimal( 'BD_CUSINI4' )
declare @vXXBD_CUSINI5  decimal( 'BD_CUSINI5' )
declare @vXXBD_CUSFIM2  decimal( 'BD_CUSFIM2' )
declare @vXXBD_CUSFIM3  decimal( 'BD_CUSFIM3' )
declare @vXXBD_CUSFIM4  decimal( 'BD_CUSFIM4' )
declare @vXXBD_CUSFIM5  decimal( 'BD_CUSFIM5' )

declare @nC2_VFIM1      decimal( 'C2_VFIM1' )
declare @nC2_VFIM2      decimal( 'C2_VFIM2' )
declare @nC2_VFIM3      decimal( 'C2_VFIM3' )
declare @nC2_VFIM4      decimal( 'C2_VFIM4' )
declare @nC2_VFIM5      decimal( 'C2_VFIM5' )
declare @nC2_APRFIM1    decimal( 'C2_APRFIM1' )
declare @nC2_APRFIM2    decimal( 'C2_APRFIM2' )
declare @nC2_APRFIM3    decimal( 'C2_APRFIM3' )
declare @nC2_APRFIM4    decimal( 'C2_APRFIM4' )
declare @nC2_APRFIM5    decimal( 'C2_APRFIM5' )

declare @nC2_VFIMRP1    decimal( 'C2_VFIM1' )
declare @nC2_VFIMRP2    decimal( 'C2_VFIM2' )
declare @nC2_VFIMRP3    decimal( 'C2_VFIM3' )
declare @nC2_VFIMRP4    decimal( 'C2_VFIM4' )
declare @nC2_VFIMRP5    decimal( 'C2_VFIM5' )
declare @nC2_APRFRP1    decimal( 'C2_APRFIM1' )
declare @nC2_APRFRP2    decimal( 'C2_APRFIM2' )
declare @nC2_APRFRP3    decimal( 'C2_APRFIM3' )
declare @nC2_APRFRP4    decimal( 'C2_APRFIM4' )
declare @nC2_APRFRP5    decimal( 'C2_APRFIM5' )

declare @nRetAtuComB2   float -- armazena o resultado das fun��es BJATUCOMB2 e BKATUCOMB2
declare @nSaldoLtUM     decimal( 'B9_QISEGUM' ) --float -- saldo do lote da segunda unidade de medida
declare @dtFech         char(08)

declare @lFatConv       char(01)
declare @cAux           Varchar(3)
declare @nAux           integer
declare @nAux1          integer
declare @cFatConvAux    char(01)
declare @nContador      integer
declare @iTranCount     integer --Var.de ajuste para SQLServer e Sybase. Ser� trocada por Commit no CFGX051 ap�s passar pelo Parse
declare @cRastro        char(01)
declare @cLocaliza      char(01)
declare @iPos           integer
declare @nResult		char(01)
Declare @vDtVai         VarChar(08)
declare @nMod			integer
declare @cB1_CCCUSTO	char('B1_CCCUSTO')

begin

   select @dtFech        = @IN_DATA
   select @lFatConv      = '0'
   select @OUT_RESULTADO = '0'

   /* ------------------------------------------------------------------------------------------------------------------
      Recupera filiais das tabelas
   ------------------------------------------------------------------------------------------------------------------ */
   select @cAux = 'SB1'
   exec XFILIAL_## @cAux, @IN_FILIALCOR, @cFil_SB1 OutPut
   select @cAux = 'SB2'
   exec XFILIAL_## @cAux, @IN_FILIALCOR, @cFil_SB2 OutPut
   select @cAux = 'SB9'
   exec XFILIAL_## @cAux, @IN_FILIALCOR, @cFil_SB9 OutPut
   select @cAux = 'SBD'
   exec XFILIAL_## @cAux, @IN_FILIALCOR, @cFil_SBD OutPut
   select @cAux = 'SC2'
   exec XFILIAL_## @cAux, @IN_FILIALCOR, @cFil_SC2 OutPut
   select @cAux = 'SD1'
   EXEC XFILIAL_## @cAux, @IN_FILIALCOR, @cFil_SD1 OutPut
   select @cAux = 'SD2'
   EXEC XFILIAL_## @cAux, @IN_FILIALCOR, @cFil_SD2 OutPut
   select @cAux = 'SD3'
   EXEC XFILIAL_## @cAux, @IN_FILIALCOR, @cFil_SD3 OutPut

   ##FIELDP01( 'SCC.CC_SEQ' )
   select @cAux = 'SD8'
   EXEC XFILIAL_## @cAux, @IN_FILIALCOR, @cFil_SD8 OutPut
   select @cAux = 'SCC'
   EXEC XFILIAL_## @cAux, @IN_FILIALCOR, @cFil_SCC OutPut
   ##ENDFIELDP01

   select @nContador = 0
   select @vDtVai = Convert(Char(08), dateadd( day, 1, @IN_MV_ULMES  ), 112)

   declare CUR_M280 insensitive cursor for
      select B2_COD,     B2_LOCAL,   SB2.R_E_C_N_O_, B2_QFIM,    B2_VFIM1,
             B2_VFIM2,   B2_VFIM3,   B2_VFIM4,       B2_VFIM5,   B2_VFIMFF1,
             B2_VFIMFF2, B2_VFIMFF3, B2_VFIMFF4,     B2_VFIMFF5, B1_CONV,
             B1_CUSTD,   B1_MCUSTD,  B2_QFIM2,       B2_QACLASS
             ##FIELDP02( 'SB2.B2_CMFIM1;SB2.B2_CMFIM2;SB2.B2_CMFIM3;SB2.B2_CMFIM4;SB2.B2_CMFIM5' )
               ,B2_CMFIM1 , B2_CMFIM2, B2_CMFIM3, B2_CMFIM4, B2_CMFIM5
             ##ENDFIELDP02
             ##FIELDP03( 'SB2.B2_CMRP1;SB2.B2_CMRP2;SB2.B2_CMRP3;SB2.B2_CMRP4;SB2.B2_CMRP5' )
               ,B2_CMRP1 , B2_CMRP2, B2_CMRP3, B2_CMRP4, B2_CMRP5
             ##ENDFIELDP03
             ##FIELDP04( 'SB2.B2_VFRP1;SB2.B2_VFRP2;SB2.B2_VFRP3;SB2.B2_VFRP4;SB2.B2_VFRP5' )
               ,B2_VFRP1 , B2_VFRP2, B2_VFRP3, B2_VFRP4, B2_VFRP5
             ##ENDFIELDP04
				 ,B1_CCCUSTO
		 from SB2### SB2 (nolock), SB1### SB1(nolock)
       where B2_FILIAL      = @cFil_SB2
         and B1_FILIAL      = @cFil_SB1
         and B1_COD         = B2_COD
         and SB2.D_E_L_E_T_ = ' '
         and SB1.D_E_L_E_T_ = ' '
		 ##IF_001({|| !SuperGetMV('MV_A280GRV',.F.,.T.) })
             and (    B2_QFIM  <> 0
                   or B2_VFIM1 <> 0
                   or B2_VFIM2 <> 0
                   or B2_VFIM3 <> 0
                   or B2_VFIM4 <> 0
                   or B2_VFIM5 <> 0
                   or exists (select 1
                                from SD1### (nolock)
                               where D1_FILIAL = @cFil_SD1
                                 and D1_COD    = B2_COD
                                 and D1_LOCAL  = B2_LOCAL
                                 and D1_DTDIGIT between @vDtVai and @IN_DATA
                                 and D_E_L_E_T_ = ' ' )
                   or exists (select 1
                                from SD2### (nolock)
                               where D2_FILIAL = @cFil_SD2
                                 and D2_COD   = B2_COD
                                 and D2_LOCAL = B2_LOCAL
                                 and D2_EMISSAO between @vDtVai and @IN_DATA
                                 and D_E_L_E_T_ = ' ' )
                   or exists (select 1
                                from SD3### (nolock)
                               where D3_FILIAL = @cFil_SD3
                                 and D3_COD   = B2_COD
                                 and D3_LOCAL = B2_LOCAL
                                 and D3_EMISSAO between @vDtVai and @IN_DATA
                                 and D3_ESTORNO = ' '
                                 and D_E_L_E_T_ = ' ' )
                 )
         ##ENDIF_001
   open CUR_M280

   fetch CUR_M280 into @cB2_COD,     @cB2_LOCAL,   @SB2_RECNO,   @nB2_QFIM,    @nB2_VFIM1,
                       @nB2_VFIM2,   @nB2_VFIM3,   @nB2_VFIM4,   @nB2_VFIM5,   @nB2_VFIMFF1,
                       @nB2_VFIMFF2, @nB2_VFIMFF3, @nB2_VFIMFF4, @nB2_VFIMFF5, @nB1_CONV,
                       @nB1_CUSTD,   @cB1_MCUSTD,  @nB2_QFIM2,   @nB2_QACLASS
                       ##FIELDP05( 'SB2.B2_CMFIM1;SB2.B2_CMFIM2;SB2.B2_CMFIM3;SB2.B2_CMFIM4;SB2.B2_CMFIM5' )
                         , @nB2_CMFIM1, @nB2_CMFIM2, @nB2_CMFIM3, @nB2_CMFIM4, @nB2_CMFIM5
                       ##ENDFIELDP05
                       ##FIELDP06( 'SB2.B2_CMRP1;SB2.B2_CMRP2;SB2.B2_CMRP3;SB2.B2_CMRP4;SB2.B2_CMRP5' )
                        ,@nB2_CMRP1 , @nB2_CMRP2, @nB2_CMRP3, @nB2_CMRP4, @nB2_CMRP5
                       ##ENDFIELDP06
                       ##FIELDP07( 'SB2.B2_VFRP1;SB2.B2_VFRP2;SB2.B2_VFRP3;SB2.B2_VFRP4;SB2.B2_VFRP5' )
                        ,@nB2_VFRP1 , @nB2_VFRP2, @nB2_VFRP3, @nB2_VFRP4, @nB2_VFRP5
                       ##ENDFIELDP07
					  ,@cB1_CCCUSTO

/* ------------------------------------------------------------------------------------------------------------------
        Atualiza Saldos
   ------------------------------------------------------------------------------------------------------------------ */
   while (@@Fetch_Status = 0) begin
      select @nContador = @nContador + 1
      select @nSaldoLtUM = 0

      If @nContador = 1 begin
         Begin Tran
         select @nContador = @nContador
      End

		/* ------------------------------------------------------------------------------------------------------------------
		  Identifica se o produto � uma MOD (M�o de obra)
		------------------------------------------------------------------------------------------------------------------ */
		select @nMod = 0
		if substring( @cB2_COD,1,3) = 'MOD' begin
			select @nMod = 1
		end else begin
			If @IN_MV_PRODMOD = '1' begin
				if @cB1_CCCUSTO <> ' ' select @nMod = 1
			end
		end

		if @nMod=1 And @IN_MV_PAR04 = 1 begin
         update SB2###
            set B2_QATU  = (B2_QATU - B2_QFIM), B2_QFIM  = 0,
                B2_VATU1 = (B2_QATU * B2_CM1),  B2_VFIM1 = 0,
                B2_VATU2 = (B2_QATU * B2_CM2),  B2_VFIM2 = 0,
                B2_VATU3 = (B2_QATU * B2_CM3),  B2_VFIM3 = 0,
                B2_VATU4 = (B2_QATU * B2_CM4),  B2_VFIM4 = 0,
                B2_VATU5 = (B2_QATU * B2_CM5),  B2_VFIM5 = 0
                ##FIELDP08( 'SB9.B9_CM1;SB9.B9_CM2;SB9.B9_CM3;SB9.B9_CM4;SB9.B9_CM5' )
                  ,B2_CMFIM1 = 0, B2_CMFIM2 = 0, B2_CMFIM3 = 0,B2_CMFIM4 = 0, B2_CMFIM5 = 0
                ##ENDFIELDP08
                ##FIELDP09( 'SB2.B2_CMRP1;SB2.B2_CMRP2;SB2.B2_CMRP3;SB2.B2_CMRP4;SB2.B2_CMRP5' )
                  ,B2_CMRP1 = 0 , B2_CMRP2 = 0 , B2_CMRP3 = 0, B2_CMRP4 = 0, B2_CMRP5 = 0
                ##ENDFIELDP09
                ##FIELDP10( 'SB2.B2_VFRP1;SB2.B2_VFRP2;SB2.B2_VFRP3;SB2.B2_VFRP4;SB2.B2_VFRP5' )
                  ,B2_VFRP1 = 0 , B2_VFRP2 = 0, B2_VFRP3 = 0, B2_VFRP4 = 0, B2_VFRP5 = 0
                ##ENDFIELDP10
		  where R_E_C_N_O_ = @SB2_RECNO

         /* ---------------------------------------------------------------------------------------------------------
            Atualiza o custo unificado ON-LINE ( B2AtuUnif )
            Parametros utilizados:
            MV_CUSMED - Parametro utilizado para verificar se devera utilizar o custo ON-LINE.
            MV_CUSFIL - Parametro utilizado para verificar se o sistema utiliza custo unificado por:
                       F = Custo Unificado por Filial
                       E = Custo Unificado por Empresa
                       A = Custo Unificado por Armazem
         --------------------------------------------------------------------------------------------------------- */
         if @IN_MV_CUSMED = '1' and ( @IN_MV_CUSFIL = '1' or @IN_MV_CUSEMP  = '1' ) begin
            exec MAT050_## @IN_FILIALCOR, @cB2_COD, @IN_MV_CUSFIL, @IN_MV_MOEDACM, @nResult output
         end

         /* ---------------------------------------------------------------------------------------------------------
            Zerar as variaveis que atualizarao a SB9 - Atualiza saldo atual da MOD
         --------------------------------------------------------------------------------------------------------- */
         select @nB2_QFIM  = 0
         select @nB2_QFIM2 = 0
         select @nB2_VFIM1 = 0
         select @nB2_VFIM2 = 0
         select @nB2_VFIM3 = 0
         select @nB2_VFIM4 = 0
         select @nB2_VFIM5 = 0
      End

		if @nMod=0 or @IN_MV_PAR02 = 1 begin

         select @vRecno = null

         select @vRecno = R_E_C_N_O_
				from SB9### (nolock)
				where B9_FILIAL  = @cFil_SB9
					and B9_COD     = @cB2_COD
					and B9_LOCAL   = @cB2_LOCAL
					and B9_DATA    = @dtFech
					and D_E_L_E_T_ = ' '

         select @lFatConv = '0'

         if @nB1_CONV <> 0 select @lFatConv = '1'

         /* ---------------------------------------------------------------------------------------------------------
            Executa P.E. para verificar se deve usar fator de conversao ou nao
         --------------------------------------------------------------------------------------------------------- */
         select @cFatConvAux = @lFatConv
         exec MA280CON_## @cB2_COD, @cFatConvAux, @lFatConv output
         select @nSaldoLtUM = @nB2_QFIM2

         if @lFatConv = '1' begin
            /* ------------------------------------------------------------------------------------------------------
               Convertendo para segunda unidade de medida
            ------------------------------------------------------------------------------------------------------ */
            select @nAux  = 0
            select @nAux1 = 2

            exec MAT018_## @cB2_COD, @IN_FILIALCOR, @nB2_QFIM, @nAux, @nAux1, @nSaldoLtUM output

            if ( @nSaldoLtUM = 0 and @nB2_QFIM2 <> 0 ) select @nSaldoLtUM = @nB2_QFIM2

         end

         /* ------------------------------------------------------------------------------------------------------
            Tratamento para o DB2
         ------------------------------------------------------------------------------------------------------ */
         SELECT @fim_CUR = 0

         /* -------------------------------------------------------------------------
            Verifica se a rastreabilidade esta em uso
         ------------------------------------------------------------------------- */
         select @cRastro = ' '
         select @cAux = ' '
         exec MAT011_## @IN_MV_RASTRO, @cB2_COD, @cAux, @IN_FILIALCOR, @cRastro OutPut

         /* -------------------------------------------------------------------------
            Verifica se a localizacao esta em uso
         ------------------------------------------------------------------------- */
         exec MAT012_## @cB2_COD, @IN_FILIALCOR, @IN_MV_WMSNEW, @IN_MV_ARQPROD, @cLocaliza OutPut

         /* ------------------------------------------------------------------------------------
            Protecao para evitar divergencia entre saldo por lote/endereco e saldo em estoque
         ------------------------------------------------------------------------------------ */
         If ( @cRastro = '1' OR @cLocaliza = '1' ) AND @nB2_QFIM < 0 AND @IN_300SALNEG = '0' begin
            select @nB2_QFIM  = 0
            select @nB2_QFIM2 = 0
            select @nB2_VFIM1 = 0
            select @nB2_VFIM2 = 0
            select @nB2_VFIM3 = 0
            select @nB2_VFIM4 = 0
            select @nB2_VFIM5 = 0
         End

         if @vRecno is null begin
            /* ---------------------------------------------------------------------------------------------------------
               Obtendo RECNO
            --------------------------------------------------------------------------------------------------------- */
            select @vRecno = Max(R_E_C_N_O_) from SB9### (nolock)

            if @vRecno is null select @vRecno = 1
            else               select @vRecno = @vRecno + 1

            /* ---------------------------------------------------------------------------------------------------------
               Gravando SB9
            --------------------------------------------------------------------------------------------------------- */
            ##TRATARECNO @vRecno\
            insert into SB9### ( B9_FILIAL,    B9_COD,      B9_LOCAL,      B9_DATA,
                                 B9_QINI,      B9_QISEGUM,  B9_VINI1,      B9_VINI2,
                                 B9_VINI3,     B9_VINI4,    B9_VINI5,      B9_VINIFF1,
                                 B9_VINIFF2,   B9_VINIFF3,  B9_VINIFF4,    B9_VINIFF5,
                                 B9_CUSTD,     B9_MCUSTD
                                 ##FIELDP11( 'SB9.B9_CM1;SB9.B9_CM2;SB9.B9_CM3;SB9.B9_CM4;SB9.B9_CM5' )
                                   ,B9_CM1 , B9_CM2, B9_CM3, B9_CM4, B9_CM5
								 ##ENDFIELDP11
                                 ##FIELDP12( 'SB9.B9_CMRP1;SB9.B9_CMRP2;SB9.B9_CMRP3;SB9.B9_CMRP4;SB9.B9_CMRP5' )
                                   ,B9_CMRP1 , B9_CMRP2, B9_CMRP3, B9_CMRP4, B9_CMRP5
								 ##ENDFIELDP12
                                 ##FIELDP13( 'SB9.B9_VINIRP1;SB9.B9_VINIRP2;SB9.B9_VINIRP3;SB9.B9_VINIRP4;SB9.B9_VINIRP5' )
                                   ,B9_VINIRP1 , B9_VINIRP2, B9_VINIRP3, B9_VINIRP4, B9_VINIRP5
								 ##ENDFIELDP13
                                 , R_E_C_N_O_ )
                        values ( @cFil_SB9,    @cB2_COD,     @cB2_LOCAL,   @dtFech,
                                 @nB2_QFIM,    @nSaldoLtUM,  @nB2_VFIM1,   @nB2_VFIM2,
                                 @nB2_VFIM3,   @nB2_VFIM4,   @nB2_VFIM5,   @nB2_VFIMFF1,
                                 @nB2_VFIMFF2, @nB2_VFIMFF3, @nB2_VFIMFF4, @nB2_VFIMFF5,
                                 @nB1_CUSTD,   @cB1_MCUSTD
                                 ##FIELDP14( 'SB9.B9_CM1;SB9.B9_CM2;SB9.B9_CM3;SB9.B9_CM4;SB9.B9_CM5' )
                                  , @nB2_CMFIM1, @nB2_CMFIM2, @nB2_CMFIM3, @nB2_CMFIM4, @nB2_CMFIM5
								 ##ENDFIELDP14
                                 ##FIELDP15( 'SB9.B9_CMRP1;SB9.B9_CMRP2;SB9.B9_CMRP3;SB9.B9_CMRP4;SB9.B9_CMRP5' )
                                   ,@nB2_CMRP1 , @nB2_CMRP2, @nB2_CMRP3, @nB2_CMRP4, @nB2_CMRP5
								 ##ENDFIELDP15
                                 ##FIELDP16( 'SB9.B9_VINIRP1;SB9.B9_VINIRP2;SB9.B9_VINIRP3;SB9.B9_VINIRP4;SB9.B9_VINIRP5' )
                                   ,@nB2_VFRP1 , @nB2_VFRP2, @nB2_VFRP3, @nB2_VFRP4, @nB2_VFRP5
								 ##ENDFIELDP16
                                 , @vRecno )
		      ##FIMTRATARECNO
         end else begin
            update SB9###
               set B9_QINI    = @nB2_QFIM ,   B9_QISEGUM = @nSaldoLtUM,  B9_VINI1   = @nB2_VFIM1,
                   B9_VINI2   = @nB2_VFIM2,   B9_VINI3   = @nB2_VFIM3,   B9_VINI4   = @nB2_VFIM4,
                   B9_VINI5   = @nB2_VFIM5,   B9_DATA    = @dtFech,      B9_VINIFF1 = @nB2_VFIMFF1,
                   B9_VINIFF2 = @nB2_VFIMFF2, B9_VINIFF3 = @nB2_VFIMFF3, B9_VINIFF4 = @nB2_VFIMFF4,
                   B9_VINIFF5 = @nB2_VFIMFF5, B9_CUSTD   = @nB1_CUSTD,   B9_MCUSTD  = @cB1_MCUSTD
                   ##FIELDP17( 'SB9.B9_CM1;SB9.B9_CM2;SB9.B9_CM3;SB9.B9_CM4;SB9.B9_CM5' )
                     ,B9_CM1 = @nB2_CMFIM1, B9_CM2 = @nB2_CMFIM2, B9_CM3 = @nB2_CMFIM3, B9_CM4 = @nB2_CMFIM4, B9_CM5 = @nB2_CMFIM5
                   ##ENDFIELDP17
                   ##FIELDP18( 'SB9.B9_CMRP1;SB9.B9_CMRP2;SB9.B9_CMRP3;SB9.B9_CMRP4;SB9.B9_CMRP5' )
                     ,B9_CMRP1 = @nB2_CMRP1, B9_CMRP2 = @nB2_CMRP2, B9_CMRP3 = @nB2_CMRP3, B9_CMRP4 = @nB2_CMRP4, B9_CMRP5 = @nB2_CMRP5
                   ##ENDFIELDP18
                   ##FIELDP19( 'SB9.B9_VINIRP1;SB9.B9_VINIRP2;SB9.B9_VINIRP3;SB9.B9_VINIRP4;SB9.B9_VINIRP5' )
                     ,B9_VINIRP1 = @nB2_VFRP1, B9_VINIRP2 = @nB2_VFRP2, B9_VINIRP3 = @nB2_VFRP3, B9_VINIRP4 = @nB2_VFRP4, B9_VINIRP5 = @nB2_VFRP5
                   ##ENDFIELDP19

             where R_E_C_N_O_ = @vRecno
         end
         /* ---------------------------------------------------------------------------------------------------------------
            Gravar os Valores finais no SB2 com o CUSTO EM PARTES.
         --------------------------------------------------------------------------------------------------------------- */
         EXEC MA280INB9CP_## @IN_FILIALCOR , @cB2_COD , @IN_MV_CUSZERO , @vRecno , @SB2_RECNO , @nB2_QFIM , @nB2_VFIM1 ,
                @nB2_VFIM2 , @nB2_VFIM3 , @nB2_VFIM4 , @nB2_VFIM5

         /* ---------------------------------------------------------------------------------------------------
            Ponto de Entrada para atualizacao do SB9
         --------------------------------------------------------------------------------------------------- */
         exec M280SB9_## @IN_FILIALCOR, @cB2_COD, @cB2_LOCAL

         /* ---------------------------------------------------------------------------------------------------------
            Efetua a grava��o no arquivo SBJ - Saldos Iniciais por Lote. Fun��o BJAtuComB2()
         --------------------------------------------------------------------------------------------------------- */
         exec MAT036_## @IN_FILIALCOR, @IN_DATA, @cB2_COD, @cB2_LOCAL, @IN_MV_RASTRO, @IN_MV_ULMES, @IN_300SALNEG, @nB2_QFIM, @nB2_QACLASS, '0', @IN_FILSEQ, @IN_MV_WMSNEW, @nRetAtuComB2 output

		   if @cLocaliza = '1'  begin
            If @IN_MV_WMSNEW = '1'
               exec MAT057_## @cB2_COD, @IN_FILIALCOR, @lIntDl output
            else select @lIntDl = '0'
         end
		  /* -------------------------------------------------------------------------------------------------------
			  Deleta movimenta��o negativa ou zerada
		   ------------------------------------------------------------------------------------------------------- */
         if Not (@IN_MV_WMSNEW = '1' and @lIntDl = '1') begin
            /* ---------------------------------------------------------------------------------------------------------
               Efetua a grava��o no arquivo SBK - Saldos Iniciais por Localiza��o. Fun��o BKAtuComB2()
            --------------------------------------------------------------------------------------------------------- */
            exec MAT037_## @IN_FILIALCOR, @IN_DATA, @cB2_COD, @cB2_LOCAL, @IN_MV_ULMES, @IN_300SALNEG, @nB2_QFIM, '0', @IN_FILSEQ,@IN_MV_WMSNEW, @IN_MV_ARQPROD, @cRastro, @nRetAtuComB2 output
         end else begin
            exec MAT058_## @IN_FILIALCOR, @IN_DATA, @cB2_COD, @cB2_LOCAL, @IN_MV_ULMES, @IN_300SALNEG, @nB2_QFIM, '0', @IN_FILSEQ,@IN_MV_WMSNEW, @IN_MV_ARQPROD, @nRetAtuComB2 output
         end

         If @nContador > 1023 begin
            Commit Transaction
            select @nContador = 0
         End

      end

	   fetch CUR_M280 into @cB2_COD,     @cB2_LOCAL,   @SB2_RECNO,   @nB2_QFIM,    @nB2_VFIM1,
                           @nB2_VFIM2,   @nB2_VFIM3,   @nB2_VFIM4,   @nB2_VFIM5,   @nB2_VFIMFF1,
                           @nB2_VFIMFF2, @nB2_VFIMFF3, @nB2_VFIMFF4, @nB2_VFIMFF5, @nB1_CONV,
                           @nB1_CUSTD,   @cB1_MCUSTD,  @nB2_QFIM2,   @nB2_QACLASS
                           ##FIELDP20( 'SB2.B2_CMFIM1;SB2.B2_CMFIM2;SB2.B2_CMFIM3;SB2.B2_CMFIM4;SB2.B2_CMFIM5' )
                              , @nB2_CMFIM1, @nB2_CMFIM2, @nB2_CMFIM3, @nB2_CMFIM4, @nB2_CMFIM5
                           ##ENDFIELDP20
                           ##FIELDP21( 'SB2.B2_CMRP1;SB2.B2_CMRP2;SB2.B2_CMRP3;SB2.B2_CMRP4;SB2.B2_CMRP5' )
                              ,@nB2_CMRP1, @nB2_CMRP2 , @nB2_CMRP3 , @nB2_CMRP4 , @nB2_CMRP5
                           ##ENDFIELDP21
                           ##FIELDP22( 'SB2.B2_VFRP1;SB2.B2_VFRP2;SB2.B2_VFRP3;SB2.B2_VFRP4;SB2.B2_VFRP5' )
                              ,@nB2_VFRP1 , @nB2_VFRP2 , @nB2_VFRP3 , @nB2_VFRP4, @nB2_VFRP5
                           ##ENDFIELDP22
                           ,@cB1_CCCUSTO
   end

   close CUR_M280
   deallocate CUR_M280

   If @nContador > 0 begin
      Commit Tran
      select @iTranCount = 0
   End

   ##FIELDP23( 'SCC.CC_SEQ' )
   If @IN_MV_CUSFIFO = '1' begin
      /* ------------------------------------------------------------------------------------------------------------------
         FechFifo - Nova rotina de Fechamento Fifo
      ------------------------------------------------------------------------------------------------------------------ */
      If @IN_CFECHFIFO = '1' begin
         /* ---------------------------------------------------------------------------------------------------------------
            Atualiza Saldos Iniciais Fifo / Lifo
         --------------------------------------------------------------------------------------------------------------- */
         update SCC###
            set CC_STATUS = 'E'
          where CC_FILIAL = @cFil_SCC
            and CC_STATUS = 'A'
            and D_E_L_E_T_ = ' '

            /* ---------------------------------------------------------------------------------------------------------------
               Ajusta a quantidade do campo D8_QFIMDEV
            --------------------------------------------------------------------------------------------------------------- */
            update SD8###
               set D8_QFIMDEV = 0
             where D8_FILIAL  = @cFil_SD8
               and D8_TM      > '500'
               and D8_ITEM   <> ' '
               and D8_QFIMDEV > 0
               and D_E_L_E_T_ = ' '

         /* ---------------------------------------------------------------------------------------------------------------
            Gera Saldos Iniciais Fifo/Lifo Baseado nos Lotes em aberto
         --------------------------------------------------------------------------------------------------------------- */
         declare CUR_M280SBD insensitive cursor for
         select BD_PRODUTO , BD_LOCAL   , BD_SEQ     , BD_QINI    , BD_QINI2UM , BD_CUSINI1 , BD_CUSINI2 , BD_CUSINI3 , BD_CUSINI4 , BD_CUSINI5 ,
               BD_QFIM    , BD_QFIM2UM , BD_CUSFIM1 , BD_CUSFIM2 , BD_CUSFIM3 , BD_CUSFIM4 , BD_CUSFIM5 , BD_DATA    , BD_OP
         from SBD### (nolock)
           where BD_FILIAL  = @cFil_SBD
             and BD_STATUS <> 'Z'
             and D_E_L_E_T_ = ' '
             for read only
         open CUR_M280SBD
         fetch CUR_M280SBD into @cBD_PRODUTO , @cBD_LOCAL   , @cBD_SEQ  , @vBD_QINI    , @vBD_QINI2UM , @vBD_CUSINI1 , @vBD_CUSINI2 , @vBD_CUSINI3 ,
                                @vBD_CUSINI4 , @vBD_CUSINI5 , @vBD_QFIM , @vBD_QFIM2UM , @vBD_CUSFIM1 , @vBD_CUSFIM2 , @vBD_CUSFIM3 , @vBD_CUSFIM4 ,
                                @vBD_CUSFIM5 , @cBD_DATA    , @cBD_OP

         while (@@fetch_status = 0) begin
            /* ---------------------------------------------------------------------------------------------------------
               Grava SCC
            --------------------------------------------------------------------------------------------------------- */
            select @vXXBD_CUSINI2 = 0
            select @vXXBD_CUSINI3 = 0
            select @vXXBD_CUSINI4 = 0
            select @vXXBD_CUSINI5 = 0
            select @vXXBD_CUSFIM2 = 0
            select @vXXBD_CUSFIM3 = 0
            select @vXXBD_CUSFIM4 = 0
            select @vXXBD_CUSFIM5 = 0
            select @iPos = Charindex( '2', @IN_MV_MOEDACM )
            If @iPos > 0 begin    --Moeda 2
               select @vXXBD_CUSINI2 = @vBD_CUSINI2
               select @vXXBD_CUSFIM2 = @vBD_CUSFIM2
            End
            select @iPos = Charindex( '3', @IN_MV_MOEDACM )
            If @iPos > 0 begin    --Moeda 3
               select @vXXBD_CUSINI3 = @vBD_CUSINI3
               select @vXXBD_CUSFIM3 = @vBD_CUSFIM3
            End
            select @iPos = Charindex( '4', @IN_MV_MOEDACM )
            If @iPos > 0 begin    --Moeda 4
               select @vXXBD_CUSINI4 = @vBD_CUSINI4
               select @vXXBD_CUSFIM4 = @vBD_CUSFIM4
            End
            select @iPos = Charindex( '5', @IN_MV_MOEDACM )
            If @iPos > 0 begin    --Moeda 5
               select @vXXBD_CUSINI5 = @vBD_CUSINI5
               select @vXXBD_CUSFIM5 = @vBD_CUSFIM5
            End

            /* ---------------------------------------------------------------------------------------------------------------
               Obtem RECNO na tabela SCC
            --------------------------------------------------------------------------------------------------------------- */
            select @vRecno = Max(R_E_C_N_O_) from SCC### (nolock)
            if @vRecno is null select @vRecno = 1
            else               select @vRecno = @vRecno + 1

            ##TRATARECNO @vRecno\
            insert into SCC### ( CC_FILIAL      , CC_PRODUTO     , CC_LOCAL       , CC_DATA        , CC_QINI    , CC_QINI2UM   , CC_VINIFF1   ,
                                 CC_VINIFF2     , CC_VINIFF3     , CC_VINIFF4     , CC_VINIFF5     , CC_QFIM    , CC_QFIM2UM   , CC_VFIMFF1   ,
                                 CC_VFIMFF2     , CC_VFIMFF3     , CC_VFIMFF4     , CC_VFIMFF5     , CC_DTORIG  , CC_OP        , CC_SEQ       ,
                                 CC_STATUS      , R_E_C_N_O_ )
                        values ( @cFil_SCC      , @cBD_PRODUTO   , @cBD_LOCAL     , @IN_DATA       , @vBD_QINI  , @vBD_CUSINI2 , @vBD_CUSINI1 ,
                                 @vXXBD_CUSINI2 , @vXXBD_CUSINI3 , @vXXBD_CUSINI4 , @vXXBD_CUSINI5 , @vBD_QFIM  , @vBD_QFIM2UM , @vBD_CUSFIM1 ,
                                 @vXXBD_CUSFIM2 , @vXXBD_CUSFIM3 , @vXXBD_CUSFIM4 , @vXXBD_CUSFIM5 , @cBD_DATA  , @cBD_OP      , @cBD_SEQ     ,
                                 'A'            , @vRecno    )

            fetch CUR_M280SBD into @cBD_PRODUTO , @cBD_LOCAL   , @cBD_SEQ  , @vBD_QINI    , @vBD_QINI2UM , @vBD_CUSINI1 , @vBD_CUSINI2 , @vBD_CUSINI3 ,
                                   @vBD_CUSINI4 , @vBD_CUSINI5 , @vBD_QFIM , @vBD_QFIM2UM , @vBD_CUSFIM1 , @vBD_CUSFIM2 , @vBD_CUSFIM3 , @vBD_CUSFIM4 ,
                                   @vBD_CUSFIM5 , @cBD_DATA    , @cBD_OP
		    ##FIMTRATARECNO
         end  --End While
         close CUR_M280SBD
         deallocate CUR_M280SBD

      end else begin
      ##ENDFIELDP23
           /* ------------------------------------------------------------------------------------------------------------------
              Transfere os saldos dos Lotes FIFO
           ------------------------------------------------------------------------------------------------------------------ */
           select @nContador = 0
           declare CUR_M280SBD2 insensitive cursor for
           select BD_PRODUTO, BD_QFIM,    BD_QFIM2UM, BD_CUSFIM1, BD_CUSFIM2,
                  BD_CUSFIM3, BD_CUSFIM4, BD_CUSFIM5, R_E_C_N_O_
             from SBD### (nolock)
               where BD_FILIAL  = @cFil_SBD
              and D_E_L_E_T_ = ' '
              for read only
          open CUR_M280SBD2
          fetch CUR_M280SBD2 into @cBD_PRODUTO, @nBD_QFIM,    @nBD_QFIM2UM, @nBD_CUSFIM1, @nBD_CUSFIM2,
                                 @nBD_CUSFIM3, @nBD_CUSFIM4, @nBD_CUSFIM5, @vRecno
          while (@@fetch_status = 0) begin
            select @nContador = @nContador + 1
            /* ---------------------------------------------------------------------------------------------------------------
                Obtendo saldo da segunda unidade de medida
            --------------------------------------------------------------------------------------------------------------- */
            select @nAux = 2
            exec MAT018_## @cBD_PRODUTO, @IN_FILIALCOR, @nBD_QFIM, @nBD_QFIM2UM, @nAux, @nSaldoLtUM output
            If @nContador = 1 begin
               begin Tran
               select @nContador = @nContador
            End
            update SBD###
               set BD_QINI    = @nBD_QFIM,    BD_QINI2UM = @nSaldoLtUM,  BD_CUSINI1 = @nBD_CUSFIM1,
                   BD_CUSINI2 = @nBD_CUSFIM2, BD_CUSINI3 = @nBD_CUSFIM3, BD_CUSINI4 = @nBD_CUSFIM4,
                   BD_CUSINI5 = @nBD_CUSFIM5
             where R_E_C_N_O_ = @vRecno
            fetch CUR_M280SBD2 into @cBD_PRODUTO, @nBD_QFIM,    @nBD_QFIM2UM, @nBD_CUSFIM1, @nBD_CUSFIM2,
                                   @nBD_CUSFIM3, @nBD_CUSFIM4, @nBD_CUSFIM5, @vRecno
            If @nContador > 1023 begin
               commit Tran
               select @nContador = 0
            End
          end  --End While
          close CUR_M280SBD2
          deallocate CUR_M280SBD2
          If @nContador > 0 begin
            Commit Tran
            select @iTranCount = 0
          End
   ##FIELDP24( 'SCC.CC_SEQ' )
      end  -- End If cFechFifo
   end -- End If @IN_MV_CUSFIFO
   ##ENDFIELDP24
   /* ------------------------------------------------------------------------------------------------------------------
        Transfere os saldos das OP's
   ------------------------------------------------------------------------------------------------------------------ */
   select @nContador = 0
   declare CUR_M280SC2 insensitive cursor for
      select C2_VFIM1,   C2_VFIM2,   C2_VFIM3,   C2_VFIM4,   C2_VFIM5, C2_APRFIM1,
             C2_APRFIM2, C2_APRFIM3, C2_APRFIM4, C2_APRFIM5, R_E_C_N_O_
		     ##FIELDP25( 'SC2.C2_VFIMRP1;SC2.C2_VFIMRP2;SC2.C2_VFIMRP3;SC2.C2_VFIMRP4;SC2.C2_VFIMRP5' )
			  ,C2_VFIMRP1 , C2_VFIMRP2 , C2_VFIMRP3, C2_VFIMRP4, C2_VFIMRP5
		     ##ENDFIELDP25
		     ##FIELDP26( 'SC2.C2_APRFRP1;SC2.C2_APRFRP2;SC2.C2_APRFRP3;SC2.C2_APRFRP4;SC2.C2_APRFRP5' )
			  ,C2_APRFRP1 , C2_APRFRP2 , C2_APRFRP3, C2_APRFRP4, C2_APRFRP5
		     ##ENDFIELDP26
        from SC2### (nolock)
       where C2_FILIAL  = @cFil_SC2
         and (C2_DATRF  > @IN_MV_ULMES or C2_DATRF = '        ')
         and D_E_L_E_T_ = ' '
   for read only

   open CUR_M280SC2

   fetch CUR_M280SC2 into @nC2_VFIM1,   @nC2_VFIM2,   @nC2_VFIM3,   @nC2_VFIM4,   @nC2_VFIM5, @nC2_APRFIM1,
                          @nC2_APRFIM2, @nC2_APRFIM3, @nC2_APRFIM4, @nC2_APRFIM5, @vRecno
						  ##FIELDP27( 'SC2.C2_VFIMRP1;SC2.C2_VFIMRP2;SC2.C2_VFIMRP3;SC2.C2_VFIMRP4;SC2.C2_VFIMRP5' )
						   ,@nC2_VFIMRP1 , @nC2_VFIMRP2 , @nC2_VFIMRP3, @nC2_VFIMRP4, @nC2_VFIMRP5
						  ##ENDFIELDP27
						  ##FIELDP28( 'SC2.C2_APRFRP1;SC2.C2_APRFRP2;SC2.C2_APRFRP3;SC2.C2_APRFRP4;SC2.C2_APRFRP5' )
						   ,@nC2_APRFRP1 , @nC2_APRFRP2 , @nC2_APRFRP3, @nC2_APRFRP4, @nC2_APRFRP5
						  ##ENDFIELDP28

   while (@@fetch_status = 0) begin
      select @nContador = @nContador + 1
      If @nContador = 1 begin
         Begin Tran
         select @nContador = @nContador
      End

      update SC2###
         set C2_VINI1   = @nC2_VFIM1,   C2_VINI2   = @nC2_VFIM2,   C2_VINI3   = @nC2_VFIM3,   C2_VINI4   = @nC2_VFIM4,
             C2_VINI5   = @nC2_VFIM5,   C2_APRINI1 = @nC2_APRFIM1, C2_APRINI2 = @nC2_APRFIM2, C2_APRINI3 = @nC2_APRFIM3,
             C2_APRINI4 = @nC2_APRFIM4, C2_APRINI5 = @nC2_APRFIM5
		    ##FIELDP29( 'SC2.C2_VINIRP1;SC2.C2_VINIRP2;SC2.C2_VINIRP3;SC2.C2_VINIRP4;SC2.C2_VINIRP5' )
            ,C2_VINIRP1 = @nC2_VFIMRP1, C2_VINIRP2 = @nC2_VFIMRP2, C2_VINIRP3 = @nC2_VFIMRP3, C2_VINIRP4 = @nC2_VFIMRP4, C2_VINIRP5 = @nC2_VFIMRP5
		    ##ENDFIELDP29
		    ##FIELDP30( 'SC2.C2_APRIRP1;SC2.C2_APRIRP2;SC2.C2_APRIRP3;SC2.C2_APRIRP4;SC2.C2_APRIRP5' )
			,C2_APRIRP1 = @nC2_APRFRP1, C2_APRIRP2 = @nC2_APRFRP2 , C2_APRIRP3 = @nC2_APRFRP3, C2_APRIRP4 = @nC2_APRFRP4, C2_APRIRP5 = @nC2_APRFRP5
		    ##ENDFIELDP30
       where R_E_C_N_O_ = @vRecno

       /* ---------------------------------------------------------------------------------------------------------------
          Gravar os Valores finais no SC2 com o CUSTO EM PARTES.
       --------------------------------------------------------------------------------------------------------------- */
       EXEC MA280INC2CP_## @vRecno

      fetch CUR_M280SC2 into @nC2_VFIM1,   @nC2_VFIM2,   @nC2_VFIM3,   @nC2_VFIM4,   @nC2_VFIM5, @nC2_APRFIM1,
                             @nC2_APRFIM2, @nC2_APRFIM3, @nC2_APRFIM4, @nC2_APRFIM5, @vRecno
						     ##FIELDP31( 'SC2.C2_VFIMRP1;SC2.C2_VFIMRP2;SC2.C2_VFIMRP3;SC2.C2_VFIMRP4;SC2.C2_VFIMRP5' )
						     ,@nC2_VFIMRP1 , @nC2_VFIMRP2 , @nC2_VFIMRP3, @nC2_VFIMRP4, @nC2_VFIMRP5
						     ##ENDFIELDP31
						     ##FIELDP32( 'SC2.C2_APRFRP1;SC2.C2_APRFRP2;SC2.C2_APRFRP3;SC2.C2_APRFRP4;SC2.C2_APRFRP5' )
						     ,@nC2_APRFRP1 , @nC2_APRFRP2 , @nC2_APRFRP3, @nC2_APRFRP4, @nC2_APRFRP5
						     ##ENDFIELDP32
      If @nContador > 1023 begin
         Commit Tran
         select @nContador = 0
      End
   end

   close CUR_M280SC2
   deallocate CUR_M280SC2

   If @nContador > 0 begin
      Commit Tran
      select @iTranCount = 0
   End
   select @OUT_RESULTADO = '1'

end
