Create procedure MAT046_##

(
  @IN_FILIAL_SDB  char('DB_FILIAL'),
  @IN_PROD        char('B1_COD'),
  @IN_LOCAL       char('B1_LOCPAD'),
  @IN_LOTE        char('BJ_LOTECTL'),
  @IN_LOCALIZA    char('BK_LOCALIZ'),
  @IN_NUMSER      char('BK_NUMSERI'),
  @IN_SUBLOTE     char('BK_NUMLOTE'),
  @IN_DATA        char(08),
  @IN_cDtSaldo    char(08),
  @IN_cRastroL    char(01),
  @IN_cRastroS    char(01),
  @IN_nSLD1SDB    float,
  @IN_nSLD7SDB    float,
  @IN_MV_WMSNEW   char(01),
  @IN_PRDORI      char('B1_COD'),
  @OUT_SALDO1     float output,
  @OUT_SALDO7     float output
)

as


/* ---------------------------------------------------------------------------------------------------------------------
    Vers�o      -  <v> Protheus P12 </v>
    Programa    -  <s> Virada de Saldo (MATA280.PRX) </s>
    Descricao   -  <d> Retorna o saldo da movimentacao do arquivo SDB </d>
    Assinatura  -  <a> 001 </a>
    Entrada     -  <ri> @IN_FILIAL_SDB - Filial corrente tabela SDB
                        @IN_PROD       - Codigo do Produto corrente
                        @IN_LOCAL      - Local ( almoxarifado )
                        @IN_LOTE       - Lote de controle
                        @IN_LOCALIZA   - Localizacao
                        @IN_NUMSER     - Numero de Serie
                        @IN_SUBLOTE    - SubLote
                        @IN_DATA       - Data Final para obter o saldo
                        @IN_cDtSaldo   - Data Inicial para obter o saldo
                        @IN_cRastroL   - Flag para rastro Lote
                        @IN_cRastroS   - Flag para rastro SubLote</ri>

    Saida       -  <ro> @OUT_SALDO1   - Saldos de saida valor 1
                        @OUT_SALDO7   - Saldos de saida valor 7</ro>

    Responsavel :  <r> Marcelo Pimentel </r>
    Data        :  <dt> 06.07.2004 </dt>
    Observacao  :  <o> Criada a procedure para compatibilizar com o banco DB2, para que que nao seja necessario
                       criado cursor dinamico devido a variavel cDtSaldo.
--------------------------------------------------------------------------------------------------------------------- */
Declare @nDB_QUANT     decimal( 'DB_QUANT' )
Declare @nDB_QTSEGUM   decimal( 'DB_QTSEGUM' )
Declare @cDB_TM        char('DB_TM')

Begin
   select @OUT_SALDO1 = @IN_nSLD1SDB
   select @OUT_SALDO7 = @IN_nSLD7SDB

   /* -------------------------------------------------------------------------------------------------
      Verifica se existe rastro e aplica filtros por lote e sub-lote
   ------------------------------------------------------------------------------------------------- */
   if @IN_cRastroL = '1' begin

       if @IN_cRastroS = '1' and @IN_SUBLOTE <> ' ' begin
        
          declare CUR_MAT029_D1 insensitive cursor for
           select DB_QUANT , DB_TM, DB_QTSEGUM
             from SDB### (nolock)
            where DB_FILIAL   = @IN_FILIAL_SDB
              and DB_PRODUTO  = @IN_PROD
              and DB_LOCAL    = @IN_LOCAL
              and DB_LOCALIZ  = @IN_LOCALIZA
              and DB_NUMSERI  = @IN_NUMSER
              and DB_LOTECTL  = @IN_LOTE
              and DB_NUMLOTE  = @IN_SUBLOTE
              and DB_ESTORNO  = ' '
              and DB_DATA     > @IN_cDtSaldo
              and DB_DATA     < @IN_DATA
              and DB_ATUEST  <> 'N'
             and D_E_L_E_T_  = ' '
          open CUR_MAT029_D1
          fetch CUR_MAT029_D1 into @nDB_QUANT, @cDB_TM, @nDB_QTSEGUM

          while (@@Fetch_Status = 0 ) begin
              if (@cDB_TM <= '500') or (substring( @cDB_TM, 1, 2 ) in ( 'DE', 'PR')) or (@cDB_TM = 'MAN') begin
                select @OUT_SALDO1 = @OUT_SALDO1 + @nDB_QUANT
                select @OUT_SALDO7 = @OUT_SALDO7 + @nDB_QTSEGUM
              end else begin
                select @OUT_SALDO1 = @OUT_SALDO1 - @nDB_QUANT
                select @OUT_SALDO7 = @OUT_SALDO7 - @nDB_QTSEGUM
              end 
              fetch CUR_MAT029_D1 into @nDB_QUANT, @cDB_TM, @nDB_QTSEGUM
          end 
          close CUR_MAT029_D1
          deallocate CUR_MAT029_D1

       end else begin
       
          declare CUR_MAT029_D2 insensitive cursor for
           select DB_QUANT , DB_TM, DB_QTSEGUM
             from SDB### (nolock)
            where DB_FILIAL   = @IN_FILIAL_SDB
              and DB_PRODUTO  = @IN_PROD
              and DB_LOCAL    = @IN_LOCAL
              and DB_LOCALIZ  = @IN_LOCALIZA
              and DB_NUMSERI  = @IN_NUMSER
              and DB_LOTECTL  = @IN_LOTE
              and DB_ESTORNO  = ' '
              and DB_DATA     > @IN_cDtSaldo
              and DB_DATA     < @IN_DATA
              and DB_ATUEST  <> 'N'
             and D_E_L_E_T_  = ' '
          open CUR_MAT029_D2
          fetch CUR_MAT029_D2 into @nDB_QUANT, @cDB_TM, @nDB_QTSEGUM

          while (@@Fetch_Status = 0 ) begin
              if (@cDB_TM <= '500') or (substring( @cDB_TM, 1, 2 ) in ( 'DE', 'PR')) or (@cDB_TM = 'MAN') begin
                select @OUT_SALDO1 = @OUT_SALDO1 + @nDB_QUANT
                select @OUT_SALDO7 = @OUT_SALDO7 + @nDB_QTSEGUM
              end else begin
                select @OUT_SALDO1 = @OUT_SALDO1 - @nDB_QUANT
                select @OUT_SALDO7 = @OUT_SALDO7 - @nDB_QTSEGUM
              end 
              fetch CUR_MAT029_D2 into @nDB_QUANT, @cDB_TM, @nDB_QTSEGUM
          end 
          close CUR_MAT029_D2
          deallocate CUR_MAT029_D2
       end
       
   /* -------------------------------------------------------------------------------------------------
      Filtro por Endereco
   ------------------------------------------------------------------------------------------------- */
   end else begin

       declare CUR_MAT029_D3 insensitive cursor for
        select DB_QUANT , DB_TM, DB_QTSEGUM
          from SDB### (nolock)
         where DB_FILIAL   = @IN_FILIAL_SDB
           and DB_PRODUTO  = @IN_PROD
           and DB_LOCAL    = @IN_LOCAL
           and DB_LOCALIZ  = @IN_LOCALIZA
           and DB_NUMSERI  = @IN_NUMSER
           and DB_ESTORNO  = ' '
           and DB_DATA     > @IN_cDtSaldo
           and DB_DATA     < @IN_DATA
           and DB_ATUEST  <> 'N'
           and D_E_L_E_T_  = ' '
       for read only
       open CUR_MAT029_D3
       fetch CUR_MAT029_D3 into @nDB_QUANT, @cDB_TM, @nDB_QTSEGUM

       while (@@Fetch_Status = 0 ) begin
          if (@cDB_TM <= '500') or (substring( @cDB_TM, 1, 2 ) in ( 'DE', 'PR')) or (@cDB_TM = 'MAN') begin
             select @OUT_SALDO1 = @OUT_SALDO1 + @nDB_QUANT
             select @OUT_SALDO7 = @OUT_SALDO7 + @nDB_QTSEGUM
          end else begin
             select @OUT_SALDO1 = @OUT_SALDO1 - @nDB_QUANT
             select @OUT_SALDO7 = @OUT_SALDO7 - @nDB_QTSEGUM
          end 
         fetch CUR_MAT029_D3 into @nDB_QUANT, @cDB_TM, @nDB_QTSEGUM
       end 
       close CUR_MAT029_D3
       deallocate CUR_MAT029_D3
   end

End