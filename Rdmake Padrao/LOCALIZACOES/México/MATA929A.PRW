#include "protheus.ch"
#INCLUDE "MATA929A.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �MATA929A  �Autor  �Marcello            �Fecha � 02/10/2008  ���
�������������������������������������������������������������������������͹��
���Desc.     �Reverte o ultimo fechamenteo realizado.                     ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � IETU - Mexico                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function MATA929A()
Local aArea	:= GetArea()
Local cMes	:= ""
Local cAno	:= ""

DbSelectArea("CDJ")
DbSetOrder(1)  
CDJ->(DbGoBottom())
If !(CDJ->(Eof()))
	cMes := Substr(CDJ->CDJ_FECHA,5,2)
	cAno := Substr(CDJ->CDJ_FECHA,1,4)
	If MsgYesNo(cMes + "/" + cAno + " - " + STR0001 + "?","IETU - " + STR0002) //"Deseja reverter o fechamento"###"Revers�o"
		MsgRun(cMes + "/" + cAno + " - " + STR0003 + ". " + STR0004 + "...","IETU - " + STR0002,{||M929ARever(cMes,cAno)}) //"Revertendo o fechamento"###"Aguarde"###"Revers�o"
	Endif
Else
	MsgAlert(STR0005 + ".","IETU - " + STR0002) //"N�o h� fechamentos para reverter"###"Revers�o"
Endif
RestArea(aArea)
Return()

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �M929ARever�Autor  �Marcello            �Fecha � 02/10/2008  ���
�������������������������������������������������������������������������͹��
���Desc.     �Executa a reversao                                          ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � IETU - Mexico                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function M929ARever(cMes,cAno,oPnl)
Local cFil		:= ""
Local cAnoMes	:= ""
Local oMsg

If oPnl <> Nil
	@01,01 Say oMsg Var " " + cMes + "/" + cAno + " - " + STR0003 + ". " + STR0004 + "..." Size 180,10 Pixel Of oPnl		//"Revertendo o fechamento"###Aguarde"
	oMsg:Align := CONTROL_ALIGN_TOP
	oMsg:nHeight := 40
	oPnl:Refresh()
	ProcessMessages()
Endif
//
cAnoMes := cAno + cMes
Begin Transaction
	// Reverte o movimento do arquivo CDJ
	CDJ->(DbSetORder(1))
	cFil := xFilial("CDJ")
	CDJ->(DbSeek(cFil + cAnoMes))
	While !(CDJ->(Eof())) .And. CDJ->CDJ_FILIAL == cFil .And. CDJ->CDJ_FECHA == cAnoMes
		RecLock( "CDJ",.F.)
		CDJ->(DbDelete())
		CDJ->(MsUnlock())
		CDJ->(DbSkip())
	EndDo
	// Reverte o movimento do arquivo CDK
	CDK->(DbSetORder(1))
	cFil := xFilial("CDK")
	CDK->(DbSeek(cFil + cAnoMes))
	While !(CDK->(Eof())) .And. CDK->CDK_FILIAL == cFil .And. CDK->CDK_FECHA == cAnoMes
		RecLock( "CDK",.F.)
		CDK->(DbDelete())
		CDK->(MsUnlock())
		CDK->(DbSkip())
	EndDo
End Transaction
If oPnl <> Nil
	oMsg:cCaption := " " + STR0006 //"Processo de revers�o encerrado"
	oPnl:Refresh()
	ProcessMessages()
Endif   

Return()