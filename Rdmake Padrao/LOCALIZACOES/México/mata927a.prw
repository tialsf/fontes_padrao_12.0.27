#INCLUDE "MATA927A.ch"
#include "protheus.ch"
#DEFINE dDT_IETU Ctod("01/01/2008","DDMMYYYY")

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �MATA927A  �Autor  �Marcello            �Fecha � 19/09/2008  ���
������������ͺ�����������������������������������������������������������͹��
���Desc.     �Calculo da base para pagamento do IETU, baseando-se nos     ���
���          �pagamentos e recebimentos realizados em um periodo.         ���
������������ͺ�����������������������������������������������������������͹��
���Uso       � IETU - Mexico                                              ���
�������������������������������������������������������������������������ͺ��
���Programador � Data   � BOPS �  Motivo da Alteracao                     ���
��������������ͺ�������ͺ�����ͺ�����������������������������������������͹��
���ARodriguez  �24/02/11�	   � Version especial para incluir PA y RA    ���
��������������ͺ�������ͺ�����ͺ�����������������������������������������͹��
���Laura Medina�19/04/11�      � Version especial para incluir PE para    ���
���            �        �      � considerar movimientos bancarios.        ���
��������������ͺ�������ͺ�����ͺ�����������������������������������������͹��
���Laura Medina�27/05/11�SDNOJT� Version especial para que la separacion  ���
���            �        �      � de grupos IETU sea correcta cuando existn���
���            �        �      � varios en un documento.                  ���
��������������ͺ�������ͺ�����ͺ�����������������������������������������͹��
���Laura Medina�02/06/11�SDNOJT� Cambio para que tome correctamente la TC ���
���            �        �      � pactada el cliente al momento de hacer la���
���            �        �      � Orden de Pago.                           ���
��������������ͺ�������ͺ�����ͺ�����������������������������������������͹��
���Laura Medina�10/06/11�SDO716� Cambio para que tome en cuenta la TC de  ���
���            �        �      � la orden de pago cuando la TC es diferen-���
���            �        �      � te a la de la debitacion del CH.         ���
��������������ͺ�������ͺ�����ͺ�����������������������������������������͹��
���Laura Medina�28/06/11�SDO716� Considerar la moneda del banco con la    ���
���            �        �      � cual se hace la orden de pago (que puede ���
���            �        �      � ser diferente a la moneda con la que se  ���
���            �        �      � hace la orden de pago).                  ���
��������������ͺ�������ͺ�����ͺ�����������������������������������������͹��
���Laura Medina�30/06/11�SDQCPX� Cambios para considerar los descuentos   ���
���            �        �      � al momento de hacer la factura y que se  ���
���            �        �      � tome en cuenta para el Cierre del IETU.  ���
��������������ͺ�������ͺ�����ͺ�����������������������������������������͹��
���Laura Medina�28/07/11�SDO716� Cambio para que t.ome en cuenta la TC de ���
���            �        �      � la orden de pago cuando se pacta una TC  ���
���            �        �      � diferente a la del d�a.                  ���
��������������ͺ�������ͺ�����ͺ�����������������������������������������͹��
���ARodriguez  �13/09/11�TDLFLJ�-CMP (compensaciones): si por PE graban   ���
���            �        �      � moneda, dejar calculo de % con moneda y  ���
���            �        �      � tasa de la factura para que sea correcto ���
��������������ͺ�������ͺ�����ͺ�����������������������������������������͹��
���ARodriguez  �21/10/11�TDVBZT�-Debitacion posterior en moneda 3         ���
���            �        �TDVCCW�-Pago en parcialidad, debitaci�n autom.   ���
���            �        �      � TC pactada, moneda 3                     ���
��������������ͺ�������ͺ�����ͺ�����������������������������������������͹��
���ARodriguez  �26/10/11�TDVPJF�-Mostrar ingresos, cualquier moneda       ���
��������������ͺ�������ͺ�����ͺ�����������������������������������������͹��
���Laura Medina�08/11/11�TDVSX8� Cambio para que t.ome en cuenta la TC del���
���            �        �      � cobro realizado (recibo)                 ���
��������������ͺ�������ͺ�����ͺ�����������������������������������������͹��
���ARodriguez  �15/11/11�TDYKN8�-Compensaciones otra moneda, tasa pactada ���
���            �        �TDYKNG�-Compensaciones todas las monedas vs PA   ���
��������������ͺ�������ͺ�����ͺ�����������������������������������������͹��
���ARodriguez  �29/11/11�      �-Restaurar correcci�n del llamado TDVPJF  ���
���            �        �      � perdida en revisi�n 08/11/11 TDVSX8      ���
���ARodriguez  �30/11/11�      �-Restaurar correcci�n del llamado TDVPJF  ���
���            �        �      � eliminar filtro de E5_MOTBX en ingresos  ���
��������������ͺ�������ͺ�����ͺ�����������������������������������������͹��
���ARodriguez  �07/02/12�TELAPQ�-Fecha 1: PA en MN con CH debito inmediato���
���            �        �      � Fecha 2: NF en otra moneda               ���
���            �        �      � Fecha 3: Compensacion ==> paridad del dia���
��������������ͺ�������ͺ�����ͺ�����������������������������������������͹��
���ARodriguez  �10/02/12�TEMRY1�-PA en MN con CH debito inmediato         ���
���            �        �      � NF en otra moneda                        ���
���            �        �      � Compensacion cambiando paridad; OK       ���
���            �14/02/12�      �-Considerar que hay PE en ordenes de pago ���
���            �        �      � que aseguran campo de moneda en movtos   ���
���            �        �      � bancarios (SE5)                          ��� 
���Laura Medina�06/03/12�TEPOAY� Modificaci�n para que se valide la serie ���
���            �        �      � del recibo y no se dupliquen movimientos.��� 
���Laura Medina�08/03/12�TEPRAN� Validar si los PA/RA estan compensados,  ���
���            �        �      � para que no se muentren de nuevo, aunque ���
���            �        �      � el parametro MV_IETUANT sea 1 (1=Si;2=No)���
���Laura Medina�16/04/12�TESUCQ� Se realizo un cambio para que tome la TC ���
���            �        �P11   � del d�a del debito (en la SE5 para P11 se���
���            �        �      � graba de diferente forma la informaci�n).���
���Laura Medina�08/05/12�TEWWSK� Modificaci�n para considerar los movimi- ���
���            �        �P11   � entos que vienen del modulo de SIGALOJA. ���
���Laura Medina�25/05/12�TFBBX2� En caso de que un pago se haga con TF, no���
���            �        �P11   � existe TC (deb posterior), por lo que no ���
���            �        �      � se debe validar.                         ���  
���Laura Medina�10/07/12�TFDMC6� No considerar RA/PA compensados si el RA/���
���            �        �P11   � PA es de un mes distinto (anterior) al   ���
���            �        �      � mes de la compensaci�n.                  ��� 
���Laura Medina�24/01/13�TGKSXT� Considere PA con tipo CH (sin compensar).���
���            �        �P11   �                                          ���       
���Laura Medina�25/01/13�TGKSXX� Se muestren PA compensados parcialmente, ���
���            �        �P11   � solo muestra el docto compensado pero el ���
���            �        �      � resto del PA no lo muestra.              ���  
���Laura Medina�13/02/13�TGQNP7� Se estan replicando los llamados de P10: ���
���            �        �      � TGCEI5,TFYOUO y TFYYBA.                  ��� 
���            �        �TGCEI5�-Considerar reversi�n compensaci�n de PA  ���
���            �        �      � en el mismo periodo. NF:prov1, PA:prov2  ��� 
���            �        �TFYOU0�-Seleccionar registros solo Pagos/Cobros  ���
���            �        �      � de acuerdo al proceso                    ���     
���            �        �TFYYBA�-Generacion de los PE IETUSD1P y IETUSF1P ���  
���Laura Medina�14/02/13�TGQNPJ� Crear PE al momento de identificar los RA���
���            �        �      � y solo cuando se encuentre activado el   ��� 
���            �        �      � parametro MV_IETUANT = 1.                ��� 
���Laura Medina�20/02/13�TGRZE7� Incluir 2 opciones al parametr MV_IETUANT���
���            �        �      � 1= Considerar PA/RA (NO compensadas)     ��� 
���            �        �      � 2= No Considerar PA/RA (NO compensadas)  ��� 
���            �        �      � 3= Solo considerar PA (NO compensados)   ��� 
���            �        �      � 4= Solo considerar RA (NO compensados)   ���  
���Laura Medina�21/02/13�TGSAZ0� Actualizar TC cuando se tiene reversion  ���
���            �        �      � de compensacion de PA.                   ��� 
���            �        �      � Actualizar fecha de compensacion cuando  ��� 
���            �        �      � existen varias reversiones sobre la misma��� 
���            �        �      � compensaci�n.                            ��� 
���ARodriguez  �27/02/13�TGSMZS�-PA/RA: Manejo de E5_DOCUMENT de acuerdo a���
���            �        �      � long. campos E?_PREFIXO y E?_NUM de SX3  ���
���            �        �      �-Corr en el manejo de RA segun parametros ��� 
���Laura Medina�01/04/13�TGWJAJ�-Mostrar de manera correcta los registros ���
���            �        �      � compensados entre RA vs. NF.             ��� 
���            �11/04/13�      �-Modificacion en la validacion de la com- ���
���            �        �      � pensacion, para que use el cliente del RA���
���            �        �      � y no el del NF.                          ���   
���Laura Medina�09/05/13�THDYV3�-Cambio para que se utilice la fecha de   ���
���            �        �      � baja del recibo(para la TC) y no la fecha��� 
���            �        �      � de emisi�n del documento (NF).           ���    
���Laura Medina�05/06/13�THJIDB�-Cambio para que no muestre los registros ���
���            �        �      � de descuento en un recibo de cobro.      ��� 
���            �18/06/13�      �-Se agrego la validaci�n para los descuen-���
���            �        �      � tos desde el query.                      ��� 
���Laura Medina�18/06/13�THJLXX�-Cambio para que no muestre los registros ���
���            �        �P11   � de descuento en un recibo de cobro.      ��� 
���Laura Medina�12/08/13�THTJAO�-Cambio para que tome correctamente los   ���
���            �        �P11   � descuentos en las OP.                    ��� 
���Laura Medina�14/08/13�      �-Que en las CMP coloque la palabra "CMP"  ���
���            �        �      � en el lugar de la OP (ya que se queda en ���
���            �        �      � blanco este campo.                       ���
���Laura Medina�30/12/13�TIAJ00�-Cuando una factura tenga aplicada una    ��� 
���            �        �      � multa no se debe prorratear con el resto ��� 
���            �        �      � de las facturas (mofificacion realizada  ���
���            �        �      � por LSamaniego)                          ���
���Oscar Garcia�18/05/18�      � Se elimina #IFDEF TOP de fuente por      ��� 
���            �        �      � Razon: SONARQUBE                         ���  
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/

Function MATA927A()
Local aArea		:= GetArea() 
Local cMesAno	:= "      "
Local nOpc		:= 0
Local lOk		:= .F.
Local oDlg
Local oMesAno
Local oFtArial8b

Private lReversao := .F.
Private cIETUANT  := Alltrim(GetNewPar("MV_IETUANT","2")) //LEMP(27/05/11):Parametro para considerar pagos anticipados (1=Si, 2/vacio=No)
Private lUsaNewKey := iif(TamSX3("F1_SERIE")[1] == 14,.T.,.F.) // Verifica se o novo formato de gravacao do Id nos campos _SERIE esta em uso

While !lOk            
	lReversao := .F.
	If Pergunte("IETUCL",.T.)                                                               
		cMesAno := StrZero(MV_PAR01,2) + StrZero(MV_PAR02,4)
		lOk := M927AVal(cMesAno)
	Else
		cMesAno := ""
		lOk := .T.
	Endif
Enddo
If !Empty(cMesAno)
	oFtArial8b := TFont():New("Arial",,,,.T.,,,8,.F.,,,,,,,)
	oDlg:=TDialog():New(0,0,250,500,STR0001,,,,,,,,,.T.,,,,,) //"C�lculo da base para IETU"
		oPnlCentro := TPanel():New(01,01,,oDlg,oFtArial8B,,,,RGB(221,221,221),5,15,.F.,.F.)
			oPnlCentro:Align := CONTROL_ALIGN_ALLCLIENT
		oPnlTopo := TPanel():New(01,01,,oDlg,,,,,RGB(221,221,221),5,30,.F.,.F.)
			oPnlTopo:Align := CONTROL_ALIGN_TOP
			oPnlTopo:nHeight := 50
			@05,000 Say " " + STR0012 + " " + STR0013 + " (mm/aaaa):" Size 180,10 Pixel Font oFtArial8b Color CLR_BLUE Of oPnlTopo //"Mes / Ano"###"fechamento"
			@04,115 MsGet oMesAno Var cMesAno Size 30,08 Picture "@R 99 / 9999" Pixel Font oFtArial8b Of oPnlTopo When .F.
		oDlg:bInit := {||M927ACalc(Substr(cMesAno,1,2),Substr(cMesAno,3),oPnlCentro,.T.),oDlg:End()}
		oDlg:lCentered := .T.
	oDlg:Activate()
Endif
RestArea(aArea)
Return()

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �M927AVal  �Autor  �Marcello            �Fecha � 19/09/2008  ���
�������������������������������������������������������������������������͹��
���Desc.     �Validar os dados informados pelo usuario                    ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � IETU - Mexico                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function M927AVal(cVal)
Local lRet		:= .T.
Local cMes		:= ""
Local cAno		:= ""
Local cAnoMes	:= ""

cMes := StrZero(Val(Substr(cVal,1,2)),2)
cAno := StrZero(Val(Substr(cVal,3)),4)
lRet := (cMes >= "01" .And. cMes <= "12")
If !lRet
	MsgStop(STR0012 + " " + STR0014 + ". " + STR0015 + ".",STR0004) //"Mes / Ano"###"inv�lido"###"Informe-os corretamente"###"Aten��o"
Else
	If Val(cAno) < Year(dDT_IETU)
		lRet := .F.
		MsgStop(STR0027 + ". " + STR0028 + " " + Dtoc(dDT_IETU))		//"O per�odo informado n�o � v�lido para o c�lculo da base de IETU"###"Informe um per�odo a partir de"
	Else
		cAnoMes := cAno + cMes
		DbSelectArea("CDJ")
		DbSetOrder(1)
		CDJ->(DbGoBottom())
		If !(CDJ->(Eof()))
			cUltMes := CDJ->CDJ_FECHA
			If cAnoMes == cUltMes
				cMsg := cMes + "/" + cAno + " " + STR0016 + ". " + STR0005 + "." //"� necess�rio executar a revers�o para continuar esta opera��o" //"j� foi encerrado"
				cMsg += " " + STR0006 + " " + cMes + "/" + cAno + " ?" //"Deseja executar a revers�o para"
				lRet := MsgYesNo(cMsg)
			Else
				If cAnoMes < cUltMes
					cMsg := cMes + "/" + cAno + " " + STR0017 + ". " + STR0007 + "." //"Para executar seu fechamento � necess�rio reverter o fechamento dos meses anteriores" //"� inferior ao �ltimo m�s encerrado"
					MsgAlert(cMsg)
					lRet := .F.
				Endif
			Endif
		Endif
	Endif
Endif
Return(lRet)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �M927ACalc �Autor  �Marcello            �Fecha � 19/09/2008  ���
�������������������������������������������������������������������������͹��
���Desc.     �Calcula a base de IETU e atualiza os arquivos CDJ e CDK.    ���
���          �Baseado no rdmake enviado                                   ���
�������������������������������������������������������������������������͹��
���Uso       � IETU - Mexico                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function M927ACalc(cMes,cAno,oPnl,lReverter)
Local cAnoMes	:= ""
Local cUltMes	:= ""
Local lCalcula	:= .T.
Local dFecha1	:= Ctod("01/" + cMes + "/" + cAno,"DDMMYYYY")
Local dFecha2	:= Ctod(StrZero(Last_day(dFecha1),2) + "/" + cMes + "/" + cAno,"DDMMYYYY")

Private aDetCob	:= {}  		// Array de Detalle de totales, contiene la fecha del pago o cobro, el grupo Ietu, el total de cobro o pago,Documento, Serie, Orden de Pago o Recibo.
Private aDetPag	:= {}  
Private aGrpIETU	:= {}

Default lReverter := .T.

cAnoMes := cAno + cMes
DbSelectArea("CDJ")
DbSetOrder(1)
CDJ->(DbGoBottom())
If !(CDJ->(Eof()))
	cUltMes := CDJ->CDJ_FECHA
	If cAnoMes >= cUltMes
		If cAnoMes == cUltMes
			If  lReverter
				M929ARever(cMes,cAno,oPnl)
				lCalcula := .T.
			Endif
		Else
			lCalcula := .T.
		Endif
	Endif
Endif
If lCalcula
	M927APag(cMes,cAno,oPnl)
	M927ARec(cMes,cAno,oPnl)
 	If Len(aDetPag) > 0 .Or. Len(aDetCob) > 0
		M927AFec(cMes,cAno,oPnl)
		If MsgYesNo(STR0008 + ". " + STR0009 + " ?","IETU") //"Processo encerrado"###"Deseja emitir o relat�rio"
			M977ARelat("",dFecha1,dFecha2,1)
		Endif
	Else
		MsgAlert(cMes + "/" +  cAno +  " - " + STR0010 + ".","IETU") //"N�o se encontrou dados para o fechamento"
	Endif
Endif
Return()

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �M927APag  �Autor  �Marcello            �Fecha � 24/09/2008  ���
�������������������������������������������������������������������������͹��
���Desc.     �Processa as saidas (egresos) para base do IETU              ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � IETU - Mexico                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function M927APag(cMes,cAno,oPnl)
Local cQuery	:= ""
Local cQryAux	:= ""
Local cFilUsr	:= ""
Local cAlias	:= ""
Local cIetuGrp	:= ""
Local cFilSEK	:= ""
Local cFilSE5	:= ""
Local cFilSE	:= ""
Local cChave	:= ""
Local cCpoIVC	:= ""
Local cCpoIEP	:= ""
Local cCpoImp	:= ""
Local cArqTrab	:= ""
Local cIEPS		:= SubStr(GetNewPar("MV_IEPS ",""),1,3)
Local nValTit	:= 0
Local nI		:= 0
Local nTit		:= 0
Local nPos		:= 0
Local nPorc		:= 0
Local nPorcTit	:= 0
Local nTtlOrd	:= 0
Local nTtlTits	:= 0
Local nVlrBase	:= 0
Local nReg		:= 0
Local dFecha1	:= Ctod("01/" + cMes + "/" + cAno,"DDMMYYYY")
Local dFecha2	:= Ctod(StrZero(Last_day(dFecha1),2) + "/" + cMes + "/" + cAno,"DDMMYYYY")
//Local aGrpIETU	:= {}
Local aTitulos	:= {}
Local aCompens	:= {}
Local aEstrSE5	:= {}
Local lRet		:= .T.
Local oMsg
Local oSepar
Local oMeter
Local cIETUPA	:= GetNewPar("MV_IETUPA","0000")
Local aCpiaPag  := {}
Local dFechOrdPg:= ctod("//")   //LEMP(10/06/11)
Local cTipMoeSEK:= ""           //LEMP(16/06/11)
Local cMonedaBco:= ""           //LEMP(28/06/11)
Local cDocumen  := ""			//ARL(13/09/11)
Local nLenDoc   := TamSX3("E2_NUM")[1]	//ARL(13/09/11)
Local dFechaDeb := CtoD("")		//ARL(20/10/11)
Local lDebitoPost := .F.		//ARL(20/10/11)
Local nTtlPags	:= 0			//ARL(16/11/11)
Local lCmpRev := .F.			//ARL(17/11/11)
Local lIETUSD1P := ExistBlock("IETUSD1P")	  //LEMP(13/02/13)-TGQNP7
Local lIETUSF1P := ExistBlock("IETUSF1P")     //LEMP(13/02/13)-TGQNP7
Local lContinua	:= .F.		    //LEMP(13/02/13)-TGQNP7
Local aDetPagPE	:= {}			//LEMP(13/02/13)-TGQNP7
Local cFornece	:= ""			//ARL(16/11/12)
Local cLojaFor	:= ""			//ARL(16/11/12) 
Local nPrefijo  := TamSX3("E2_PREFIXO")[1]    //LEMP(TFDMC6-10/07/12):Validar si se imprimen RA/PA no compensadas  
Local nNumero   := TamSX3("E2_NUM")[1]        //LEMP(TFDMC6-10/07/12):Validar si se imprimen RA/PA no compensadas
Local nTipo     := TamSX3("E2_TIPO")[1]   
Local nValSE2   := 0                          //LEMP(TGKSXX-24/01/13):Validar que se impriman PA compensadas parcialmente
Local cQryID	:= ""
Local cAliasSF1 := ""

/*
��������������������������������������������Ŀ
�Estrutura do array aTitulos:                �
�1) Tipo - 1 compensacao; 0 normal           �
�2) Ordem de pago                            �
�3) Numero do documento (fatura)             �
�4) Serie do documento                       �
�5) Fornecedor                               �
�6) Filial do fornecedor                     �
�7) Valor                                    �
�8) Especie (CH,EF,TF,NCP,NDI etc)           �
�9) Documento (no caso de compensacao, possui�
�   o documento original da compensacao)     �
����������������������������������������������
���������������������������������������������Ŀ
�Estrutura do array aDetPag                   �
�01) Data                                     �
�02) Grupo de IETU                            �
�03) Valor                                    �
�04) Documento (fatura)                       �
�05) Serie do documento                       �
�06) Ordem de pago                            �
�07) Fornecedor                               �
�08) Filial do fornecedor                     �
�09) Tipo pagamento ("P")                     �
�10) Especie (CH,EF,TF,NCP,NDI etc)           �
�11) Documento (no caso de compensacao, possui�
�    o documento original da compensacao)     �
�����������������������������������������������
*/
@01,01 Say oMsg Var " " + STR0011 Size 180,10 Pixel Of oPnl //"Preparando as informa��es sobre pagamentos"
oMsg:Align := CONTROL_ALIGN_TOP
oMsg:nHeight := 15
oSepar := TPanel():New(01,01,,oPnl,,,,,RGB(221,221,221),5,15,.F.,.F.)
	oSepar:Align := CONTROL_ALIGN_TOP
	oSepar:nHeight := 25
oPnl:Refresh()
ProcessMessages()
//
SB1->(DbSetOrder(1))
SD1->(DbSetOrder(1))
SF1->(DbSetORder(1))
SF2->(DbSetORder(1))
cFilSEK := xFilial("SEK")
SEK->(DbSetOrder(1))
cFilSE5 := xFilial("SE5")
SE5->(DbSetORder(7))
//
DbSelectArea("SFB")
DbSetOrder(1)
SFB->(DbSeek(xFilial("SFB")+"IVC"))
cCpoIVC := "SD1->D1_VALIMP" + AllTrim(SFB->FB_CPOLVRO)
SFB->(DbSeek(xFilial("SFB")+cIEPS))
cCpoIEP := "SD1->D1_VALIMP" + AllTrim(SFB->FB_CPOLVRO)
//
aDetPag := {}

cQuery := "select count(*) as TTLREG from " + RetSqlName("SE5")
cQuery += " where"
cQuery += " D_E_L_E_T_=''"
cQuery += " and E5_FILIAL = '" + cFilSE5 + "'"
cQuery += " and ("
cQryAux := "(E5_DATA >= '" + Dtos(dFecha1) + "'"
cQryAux += " and E5_DATA <= '" + Dtos(dFecha2) + "'"
cQryAux += " and ("
cQryAux += "(E5_RECPAG = 'P' and E5_SITUACA <> 'C' and E5_TIPODOC <> 'ES' and E5_TIPODOC <> 'BA' and E5_TIPODOC <> 'DC' )"
cQryAux += " or "
cQryAux += "(E5_MOTBX='CMP' and E5_RECPAG = 'R' and E5_SITUACA <> 'C' and E5_TIPODOC = 'ES')"
cQryAux += " or "
cQryAux += "(E5_MOTBX='NOR' and E5_RECPAG = 'P' and E5_SITUACA <> 'C' and E5_TIPODOC = 'VL')"  //Baixa Automatica ... IN ('P','R')  //LEMP(13/02/13)-TGQNP7
cQryAux += "))"
/*
Ponto de entrada para alteracao do filtro*/
If ExistBlock("IETUFILP")
	cFilUsr	:=	ExecBlock("IETUFILP",.F.,.F.,{cQryAux})
	If !Empty(cFilUsr)
		cQryAux := cFilUsr
	Endif
Endif
cQuery += cQryAux + ")"
//
cAlias := GetNextAlias()
cQuery := ChangeQuery(cQuery)
dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.T.,.T.)
nReg := (cAlias)->TTLREG
(cAlias)->(DbCloseArea())
//
cQuery := "select E5_DATA,E5_TIPO,E5_TIPODOC,E5_VALOR,E5_MOEDA,E5_VLMOED2,E5_ORDREC,E5_MOTBX,E5_DTDISPO,E5_NUMERO,E5_PREFIXO,E5_PARCELA,E5_FORNECE,E5_CLIENTE,E5_LOJA,E5_DOCUMEN,E5_CLIFOR,E5_TXMOEDA,E5_MOEDA,E5_RECPAG,R_E_C_N_O_ from " + RetSqlName("SE5")
cQuery += " where"
cQuery += " D_E_L_E_T_=''"
cQuery += " and E5_FILIAL = '" + cFilSE5 + "'"
cQuery += " and ("
cQuery += cQryAux + ")"
cAlias := GetNextAlias()
cQuery := ChangeQuery(cQuery)
dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.T.,.T.)
TCSetField(cAlias,"E5_DATA","D",8,0)

dbSelectArea(cAlias)
(cAlias)->(DbGotop())
oMsg:cCaption := " " + STR0019 + ". " + STR0020 + "..."		//"Executando o c�lculo dos pagamentos efetuados"###"Aguarde"
oPnl:Refresh()
ProcessMessages()
oMeter := TMeter():New(08,08,,nReg+1,oSepar,oPnl:nClientWidth-16,8,,.T.,,,.F.,,,,,)
oMeter:Align := CONTROL_ALIGN_TOP
oMeter:nHeight := 15
nReg := 0

While !((cAlias)->(Eof()))
	aTitulos := {}
	aCompens := {}
	nTtlOrd  := 0
	nTtlTits := 0
	nTaxaSEK := 0				//LEMP(28/07/11)
	dFechOrdPg := ctod("//")	//LEMP(10/06/11)
	cMonedaBco := ""			//LEMP(28/06/11)
	cTipMoeSEK := ""			//LEMP(28/06/11)
	dFechaDeb := CtoD("")		//ARL(20/10/11)
	lDebitoPost := .F.			//ARL(20/10/11)  LEMP(16/04/12):Se va a utilizar
	nTtlPags := 0				//ARL(16/11/11)
	lCmpRev := .F.				//ARL(17/11/11)  
	nValSE2   := 0             //LEMP(24/01/13)  
	
	//LEMP(TFDMC6-10/07/12):Validar si se imprimen RA/PA no compensadas (Solo cuando se consideren RA/PA sin compensar)
	If  cIETUANT $ '1|3'   //LEMP(TGRZE7-20/01/13):Agregar la opcion de 'solo considerar PA'
		//Validar si el PA fue generado en mes diferente a la compensacion
		If  SUBSTR((cAlias)->E5_DOCUMENT,nPrefijo+nNumero+2,nTipo)==PADR("PA",nTipo) //ARL (26/02/2013)
			If  VldPAComp((cAlias)->E5_CLIFOR,(cAlias)->E5_LOJA,SUBSTR((cAlias)->E5_DOCUMENT,nPrefijo+1,nNumero),SUBSTR((cAlias)->E5_DOCUMENT,1,nPrefijo),;
				(cAlias)->E5_PARCELA,PADR("PA",nTipo),Month((cAlias)->E5_DATA))      
				(cAlias)->(DbSkip())
				Loop
			Endif
		Endif
	Endif
	
	//�����������������������������������������������������������Ŀ
	//�Uma compensacao pode ser  feita de duas maneiras:          �
	//�1 - Manualmente, associando um titulo de compensacao       �
	//�     (ncp por exemplo) a um titulo a pagar.                �
	//�2 - "Automaticamente", incluindo um titulo de compensacao  �
	//�     na ordem de pago. Neste caso o valor da compensacao e �
	//�     subtraido do valor total da ordem de pago.            �
	//�������������������������������������������������������������
	If (cAlias)->E5_MOTBX == "CMP"
		// <ARL> (12/09/11) Usar TC pactada
		If Empty((cAlias)->E5_MOEDA)
			nValTit := xMoeda((cAlias)->E5_VALOR,Val((cAlias)->E5_MOEDA),1,(cAlias)->E5_DATA,MsDecimais(1))
		Else
			nValTit := xMoeda((cAlias)->E5_VLMOED2,Val((cAlias)->E5_MOEDA),1,(cAlias)->E5_DATA,MsDecimais(1),(cAlias)->E5_TXMOEDA)
		Endif
		cDocumen := Substr((cAlias)->E5_DOCUMEN ,1 , 3 + nLenDoc + 1 + 3)
		// <\ARL>
		aAreaQRy:=GetArea()
		//���������������������������������������������������������������������������������Ŀ
		//�Quando uma compensacao e cancelada, e criado um registro de estorno. Este estorno�
		//�e guardado para ser subtraido do valor pago.                                     �
		//�����������������������������������������������������������������������������������
		If (cAlias)->E5_TIPODOC == "ES" // .And. !((cAlias)->E5_TIPO $ MVPAGANT)	// ARL (16/11/2012) Reversi�n compensaci�n PA en el mismo periodo
			DbSelectArea("SE2")
			SE2->(dbSetOrder(1))
			If (cAlias)->E5_TIPO == "NF "
				DbSeek(xFilial("SE2") +	  (cAlias)->E5_PREFIXO+	(cAlias)->E5_NUMERO+(cAlias)->E5_PARCELA+(cAlias)->E5_TIPO+(cAlias)->E5_CLIFOR+(cAlias)->E5_LOJA  ,.f.)
				// 14/02/2012; PE's en ordenes de pago forzan valor en SE5->E5_MOEDA
				If Empty((cAlias)->E5_MOEDA) .Or. (!(cAlias)->E5_VALOR==(cAlias)->E5_VLMOED2 .And. !(cAlias)->E5_VLMOED2==0)
					//nValTit := xMoeda((cAlias)->E5_VLMOED2,SE2->E2_MOEDA,1,(cAlias)->E5_DATA,MsDecimais(1),SE2->E2_TXMOEDA) //LEMP(TGSAZ0-21/02/13):Considerar la TC
					nTaxaSEK := Round( (cAlias)->E5_VALOR / (cAlias)->E5_VLMOED2 , MsDecimais(1) )   
					nValTit := xMoeda((cAlias)->E5_VLMOED2,SE2->E2_MOEDA,1,(cAlias)->E5_DATA,MsDecimais(1),nTaxaSEK)
				Else
					nValTit := xMoeda((cAlias)->E5_VLMOED2,SE2->E2_MOEDA,1,(cAlias)->E5_DATA,MsDecimais(1),SE2->E2_TXMOEDA)
				Endif
				
			Else
				//If(DbSeek(xFilial("SE2") +	(cAlias)->E5_DOCUMEN ,.f.))   	// ARL (13/09/11)
				If(DbSeek(xFilial("SE2") +	cDocumen ,.f.))
					// 14/02/2012; PE's en ordenes de pago forzan valor en SE5->E5_MOEDA
				 	//If(Empty((cAlias)->E5_MOEDA))
					If Empty((cAlias)->E5_MOEDA) .Or. (!(cAlias)->E5_VALOR==(cAlias)->E5_VLMOED2 .And. !(cAlias)->E5_VLMOED2==0)
				 		nValTit := xMoeda((cAlias)->E5_VLMOED2,SE2->E2_MOEDA,1,(cAlias)->E5_DATA,MsDecimais(1),SE2->E2_TXMOEDA)
					EndIf
				EndIf
			EndIf
			Aadd(aTitulos,{0,"",(cAlias)->E5_NUMERO,(cAlias)->E5_PREFIXO,(cAlias)->E5_FORNECE,(cAlias)->E5_LOJA,-nValTit,1,(cAlias)->E5_TIPO,"",(cAlias)->E5_TXMOEDA,SE2->E2_FORNECE,SE2->E2_LOJA})
			lCmpRev := .T.	// ARL (17/11/11) Identificar reversi�n de compensaci�n para acumulaci�n en detalle de pagos
		Else
			If (cAlias)->E5_TIPO == "NF "
				DbSelectArea("SE2")
				SE2->(dbSetOrder(1))
				DbSeek(xFilial("SE2") + (cAlias)->E5_PREFIXO + (cAlias)->E5_NUMERO + (cAlias)->E5_PARCELA + (cAlias)->E5_TIPO + (cAlias)->E5_CLIFOR + (cAlias)->E5_LOJA , .f.)
				//If(Empty((cAlias)->E5_MOEDA) )	// ARL (13/09/11) Se utiliza para calcular porcentaje vs valor total de la factura
				// 14/02/2012; PE's en ordenes de pago forzan valor en SE5->E5_MOEDA
				//If Empty((cAlias)->E5_MOEDA) .And. !(cAlias)->E5_VALOR==(cAlias)->E5_VLMOED2
				If !(cAlias)->E5_VALOR==(cAlias)->E5_VLMOED2 .And. !(cAlias)->E5_VLMOED2==0
					// 07/02/2012; PA=MN, NF=M? ==> tomar la paridad de la fecha de compensacion
					//nTaxaSEK := RecMoeda((cAlias)->E5_DATA, SE2->E2_MOEDA)
					// 10/02/2012; PA=MN, NF=M? ==> tomar la paridad de compensacion; segun la fecha o pactada
					nTaxaSEK := Round( (cAlias)->E5_VALOR / (cAlias)->E5_VLMOED2 , MsDecimais(1) )
					nValTit := xMoeda((cAlias)->E5_VLMOED2,SE2->E2_MOEDA,1,(cAlias)->E5_DATA,MsDecimais(1),nTaxaSEK)
				Else
					nValTit := xMoeda((cAlias)->E5_VLMOED2,SE2->E2_MOEDA,1,(cAlias)->E5_DATA,MsDecimais(1),SE2->E2_TXMOEDA)
				Endif
			Else
				DbSelectArea("SE2")
				SE2->(dbSetOrder(1))
				//If(DbSeek(xFilial("SE2") +	(cAlias)->E5_DOCUMEN ,.f.))   // ARL (13/09/11)
				If(DbSeek(xFilial("SE2") +	cDocumen ,.f.))
				 	//If(Empty((cAlias)->E5_MOEDA))	// ARL (13/09/11) Se utiliza para calcular porcentaje vs valor total de la factura
				 		nValTit := xMoeda((cAlias)->E5_VLMOED2,SE2->E2_MOEDA,1,(cAlias)->E5_DATA,MsDecimais(1),SE2->E2_TXMOEDA)
					//EndIf
				EndIf
			EndIf
			Aadd(aTitulos,{0,Iif(Alltrim((cAlias)->E5_TIPODOC)="CP","CMP",""),(cAlias)->E5_NUMERO,(cAlias)->E5_PREFIXO,(cAlias)->E5_FORNECE,(cAlias)->E5_LOJA,nValTit,1,(cAlias)->E5_TIPO,"",(cAlias)->E5_TXMOEDA})
		Endif
		RestArea(aAreaQRy)
		// <ARL 16/11/11>
		nTtlPags += (cAlias)->E5_VLMOED2
		If !Empty((cAlias)->E5_TXMOEDA) .And. (cAlias)->E5_TXMOEDA != 1
			// Baja/Estorno: Tasa pactada
			nTaxaSEK := (cAlias)->E5_TXMOEDA
		Endif
		// <\ARL>		
	Else
		If (cAlias)->E5_TIPO == "CH "
		    aArea:=GetArea()
			cOrdPago := Alltrim((cAlias)->E5_NUMERO)
			DbSelectArea("SE2")
			SE2->(dbSetOrder(1))
			DbSeek(xFilial("SE2") +	(cAlias)->E5_PREFIXO+(cAlias)->E5_NUMERO+(cAlias)->E5_PARCELA+(cAlias)->E5_TIPO+(cAlias)->E5_CLIFOR+(cAlias)->E5_LOJA  ,.f.)
			cOrdPago := SE2->E2_ORDPAGO
			dFechaDeb := SE2->E2_BAIXA
			RestArea(aArea)
		Else
			cOrdPago := AllTrim((cAlias)->E5_ORDREC)
		Endif

		SEK->(DbSeek(cFilSEK + cOrdPago))
		While !(SEK->(Eof())) .And. SEK->EK_FILIAL == cFilSEK .And. SEK->EK_ORDPAGO == cOrdPago
			If SEK->EK_TIPO $ "CH |EF |TF |NCP|NDI|"
				If SEK->EK_TIPO $ "NCP|NDI|"
					//�������������������������������������������������������������Ŀ
					//�A compensacao automatica gera um unico registro para toda  a �
					//�ordem de pago. Quando o valor pago e maior que a soma dos    �
					//�titulos e gerado um PA. Neste caso, o valor e armazenado mas �
					//�nao integrara a soma dos pagamentos. Noutro caso, o valor e  �
					//�guardado para depois ser rateado entre os titulos da ordem de�
					//�pago.                                                        �
					//���������������������������������������������������������������
					If SEK->EK_TIPODOC == "PA"
						nTtlOrd += SEK->EK_VLMOED1
					Else
						nTtlOrd -= SEK->EK_VLMOED1
						Aadd(aCompens,{1,SEK->EK_ORDPAGO,SEK->EK_NUM,SEK->EK_PREFIXO,SEK->EK_FORNECE,SEK->EK_LOJA,SEK->EK_VLMOED1,1,SEK->EK_TIPO,""})
					Endif
				Else
					cMonedaBco := SEK->EK_MOEDA  //Moneda del banco-pago (al cual se cargo la OP)
					// <ARL> (19/10/11)
					cMoeda := IIf(Len(Alltrim(SEK->EK_MOEDA))==1 , "0" + Trim(SEK->EK_MOEDA) , Trim(SEK->EK_MOEDA) )
					If SEK->EK_TIPO == "CH "
						If  ObtMovVL((cAlias)->E5_ORDREC, (cAlias)->E5_CLIFOR, (cAlias)->E5_LOJA, SEK->EK_NUM, SEK->EK_TIPO) .Or. dFechaDeb > (cAlias)->E5_DATA
							// Debito posterior
							lDebitoPost := .T.
						Else
							// Indica que hay moneda en el movimiento del cheque ==> Debito inmediato
							If ( SEK->(FieldPos("EK_TXMOE"+cMoeda)) > 0 )
								nTaxaSEK := SEK->&("EK_TXMOE"+cMoeda)
							Endif
						Endif
					Endif
					// <\ARL>
				Endif
			Else        
			//������������������������������������������������������Ŀ
			//�Para pagamentos "normais" os dados sao guardados para �
			//�depois entrarem no calculo da base de IETU            �
			//��������������������������������������������������������
				If  SEK->EK_TIPO == "NF " .Or. (Iif(cIETUANT $ '1|3',(SEK->EK_TIPODOC == "PA" .And. (cAlias)->E5_RECPAG== "P"),.T.))  //LEMP(TGRZE7-20/01/13):Agregar la opcion de 'solo considerar PA'
					nTtlTits += SEK->EK_VLMOED1
					nTtlOrd += SEK->EK_VLMOED1
					nTaxaSEK:=0
					dFechOrdPg := SEK->EK_EMISSAO
					cTipMoeSEK := SEK->EK_MOEDA   //Moneda de la OP
					cMoeda := IIf(Len(Alltrim(SEK->EK_MOEDA))==1,"0"+SEK->EK_MOEDA,SEK->EK_MOEDA)
					If ( SEK->(FieldPos("EK_TXMOE"+cMoeda)) > 0 )
						nTaxaSEK:=SEK->&("EK_TXMOE"+cMoeda)
					EndIf
					Aadd(aTitulos,{0,SEK->EK_ORDPAGO,SEK->EK_NUM,SEK->EK_PREFIXO,SEK->EK_FORNECE,SEK->EK_LOJA,SEK->EK_VLMOED1,1,If(SEK->EK_TIPO == "NF ",(cAlias)->E5_TIPO,IIF(cIETUANT $ '1|3',SEK->EK_TIPODOC,(cAlias)->E5_TIPO) ),"",nTaxaSEK})//LEMP(TGRZE7-20/01/13):Agregar la opcion de 'solo considerar PA'
				Endif  
						
			Endif
			SEK->(DbSkip())
		Enddo

		If  Empty(dFechOrdPg) //LEMP(10/06/11)
			dFechOrdPg:=(cAlias)->E5_DATA
		Endif                   
		//LEMP(11/03/13):Validar si viene de P10 o P11 por la diferencia en los registros de SE5 
		If  "11" $ oApp:cVersion                 
			//LEMP(16/04/12): Validar si la fecha de debitacion es dif a la fecha de la OP
			If  !Empty(dFechaDeb) //LEMP(25/05/12): Solo si contiene fecha de debito (TF: no contiene)
				If	dFechOrdPg <> dFechaDeb
					dFechOrdPg := dFechaDeb    
					lDebitoPost := .T.
				Endif         
			Endif
			//conversao do valor pago para a moeda 1 
			//LEMP(16/04/12): TC del d�a del debito
			If  lDebitoPost  
				nValTit := xMoeda((cAlias)->E5_VALOR,Iif(Empty((cAlias)->E5_MOEDA) .And. (cAlias)->E5_TIPODOC =='VL' .And. cMonedaBco!='1',val(cMoeda),Val((cAlias)->E5_MOEDA)),1,dFechaDeb,MsDecimais(1))   //LEMP(28/07/11): Que tome la moneda de la OP y no la de la fecha de la OP
			Else
		   		nValTit := xMoeda((cAlias)->E5_VALOR,Iif(Empty((cAlias)->E5_MOEDA) .And. (cAlias)->E5_TIPODOC =='VL' .And. cMonedaBco!='1',val(cMoeda),Val((cAlias)->E5_MOEDA)),1,dFechOrdPg,MsDecimais(1),nTaxaSEK)   //LEMP(28/07/11): Que tome la moneda de la OP y no la de la fecha de la OP
		 	Endif   
		 Else
		 	nValTit := xMoeda((cAlias)->E5_VALOR,Iif(Empty((cAlias)->E5_MOEDA) .And. (cAlias)->E5_TIPODOC =='VL' .And. cMonedaBco!='1',val(cMoeda),Val((cAlias)->E5_MOEDA)),1,dFechOrdPg,MsDecimais(1),nTaxaSEK)   //LEMP(28/07/11): Que tome la moneda de la OP y no la de la fecha de la OP
		 Endif    
	             
	 
	 
	 
		//Porcentagem do valor pago em relacao ao valor total do pagamento
		nPorcTit := nValTit / nTtlOrd
		//��������������������������������Ŀ
		//|Tratamento para Baixa Automatica|
		//����������������������������������
		If (cAlias)->E5_TIPO == "NF " .And. (cAlias)->E5_TIPODOC == "VL"
			nPorcTit := 1
			Aadd(aTitulos,{0,"",(cAlias)->E5_NUMERO,(cAlias)->E5_PREFIXO,(cAlias)->E5_FORNECE,(cAlias)->E5_LOJA,nValTit,(cAlias)->E5_TIPODOC,"",(cAlias)->E5_TXMOEDA}) // ARL(21/10/11) Solo 10 elementos!! falta el 8vo
		EndIf
		//����������������������������������������������������������������Ŀ
		//�Os valores dos titulos sao recalculados para serem proporcionais�
		//�ao pagamento efetuado                                           �
		//������������������������������������������������������������������
		For nI := 1 To Len(aCompens)
			//�������������������������������������������������������������Ŀ
			//�Quando ha compensacoes, ratea-se o valor entre os titulos da �
			//�ordem de pago, criando-se um items para cada titulo com o    �
			//�valor da compensacao.                                        �
			//���������������������������������������������������������������
			For nTit := 1 To Len(aTitulos)
				nPorc := aTitulos[nTit,7] / nTtlTits
				Aadd(aTitulos,{1,aTitulos[nTit,2],aTitulos[nTit,3],aTitulos[nTit,4],aTitulos[nTit,5],aTitulos[nTit,6],aCompens[nI,7]*nPorc,1,aCompens[nI,9],aCompens[nI,4] + aCompens[nI,3] + Space(TamSX3("E2_PARCELA")[1]) + aCompens[nI,9],IiF(aTitulos[nTit]>10,aTitulos[nTit,11],0)})
			Next
		Next
		For nI := 1 To Len(aTitulos)
			aTitulos[nI,7] := aTitulos[nI,7] * nPorcTit
			aTitulos[nI,8] := nPorcTit
		Next
	Endif
	//�����������������������������������������������������������������Ŀ
	//�Busca-se os itens dos documentos originais (faturas) para        �
	//�obter-se os seus valores e distribui-los entre os grupos de IETU.�
	//�������������������������������������������������������������������
	For nTit := 1 To Len(aTitulos)
		If (Iif(cIETUANT $ '1|3',!(aTitulos[nTit,9] == "PA"),.T.) )  //LEMP(TGRZE7-20/01/13):Agregar la opcion de 'solo considerar PA' 
			aGrpIETU := {}
			nTtlFat := 0
			
			If Len(aTitulos[nTit]) < 12
				cFornece := aTitulos[nTit,5]
				cLojaFor := aTitulos[nTit,6]
			Else
				cFornece := aTitulos[nTit,12]
				cLojaFor := aTitulos[nTit,13]
			Endif
	
			//If SF1->(DbSeek(xFilial("SF1") + aTitulos[nTit,3] + aTitulos[nTit,4] + cFornece + cLojaFor))
			/*Projeto Chave Unica - Tiago Silva
				A query a seguir foi desenvolvida para substituir o seek que era utilizado nas tabelas SF1/SD1, pois utilizava prefixo como serie.			
			*/
			cQryID1:=" SELECT "
			cQryID1+=" 	F1_EMISSAO,F1_MOEDA,F1_DTDIGIT,F1_TXMOEDA,F1_VALBRUT, D1_COD, D1_TOTAL, D1_VALDESC, D1_TES, D1_VALIMP1,D1_VALIMP2,D1_VALIMP3,D1_VALIMP4,D1_VALIMP5,D1_VALIMP6 "
			cQryID1+=" FROM "+retSqlName("SF1")+" SF1, "
			cQryID1+=" 		"+retSqlName("SD1")+" SD1  "
			cQryID1+=" WHERE SF1.F1_FILIAL  = SD1.D1_FILIAL "
			cQryID1+="   AND SF1.F1_DOC	    = SD1.D1_DOC "
			cQryID1+="   AND SF1.F1_SERIE 	= SD1.D1_SERIE "
			cQryID1+="   AND SF1.F1_FORNECE = SD1.D1_FORNECE "
			cQryID1+="   AND SF1.F1_LOJA    = SD1.D1_LOJA "								
			cQryID1+="   AND SF1.D_E_L_E_T_ ='' "
			cQryID1+="   AND SF1.F1_FILIAL  ='"+xFilial("SF1")+"' "
			cQryID1+="   AND SF1.F1_DOC	   ='"+aTitulos[nTit,3]+"' "
			cQryID1+="   AND SF1.F1_PREFIXO ='"+aTitulos[nTit,4]+"' "
			cQryID1+="   AND SF1.F1_FORNECE ='"+cFornece+"' "
			cQryID1+="   AND SF1.F1_LOJA    ='"+cLojaFor+"' "								
			cQryID1+="   AND SF1.D_E_L_E_T_ ='' "
			cQryID1	  := ChangeQuery(cQryID1)		
			cAliasID1 := CriaTrab(Nil,.F.)
			dbUseArea( .T., "TOPCONN", TcGenQry( , , cQryID1 ), cAliasID1, .T., .T. )
			dbSelectArea(cAliasID1)	
				
			If (cAliasID1)->(!EOF())
				If stod((cAliasID1)->F1_EMISSAO) >= dDT_IETU
					nMoeda		:= (cAliasID1)->F1_MOEDA
					nDtDigit	:= (cAliasID1)->F1_DTDIGIT
					nTxMoeda	:= (cAliasID1)->F1_TXMOEDA
					nValBrut	:= (cAliasID1)->F1_VALBRUT
					cFilSE		:= xFilial("SD1")
					
					//������������������������������������������������������������������
					//�Distribui os valores entre os grupos de IETU (considera somente �
					//�os itens que estejam com o grupo cadastrado). O valor utilizado �
					//�e sem impostos.                                                 �
					//������������������������������������������������������������������
					// <ARL 20/10/11>
					//If lDebitoPost
					//	nTtlFat := xMoeda(SF1->F1_VALBRUT,nMoeda,1,dFechaDeb,MsDecimais(1)) // Tomar paridad de la fecha de debitacion de CH
					If !(nTaxaSEK == 0)
						nTtlFat := xMoeda(nValBrut,nMoeda,1,nDtDigit,MsDecimais(1),nTaxaSEK) // Tasa pactada
					Else
					// <\ARL>
						nTtlFat := xMoeda(nValBrut,nMoeda,1,nDtDigit,MsDecimais(1),nTxMoeda)
					Endif
					nPorce:= aTitulos[nTit,7] / nTtlFat   //Bandera p que tome otra fecha
					nPorce:= Min(1,nPorce)
						
					//SD1->(DbSeek(cFilSE + aTitulos[nTit,3] + aTitulos[nTit,4] + cFornece + cLojaFor)) 
					//While !(SD1->(Eof())) .And. SD1->D1_FILIAL == cFilSE .And. SD1->D1_DOC == aTitulos[nTit,3] .And. AllTrim(SD1->D1_SERIE) == AllTrim(aTitulos[nTit,4]) .And. SD1->D1_FORNECE == cFornece .And. Alltrim(SD1->D1_LOJA) == Alltrim(cLojaFor)
					While !((cAliasID1)->(EOF()))
						If lIETUSD1P   //LEMP(13/02/13)-TGQNP7
							lContinua := ExecBlock("IETUSD1P",.F.,.F.,{aTitulos[nTit]})
							If !lContinua
								(cAliasSD1)->(DbSkip())
								Loop
							Endif
						Endif
	
						If SB1->(MsSeek(xFilial("SB1") + (cAliasID1)->D1_COD)) 
							If !Empty(SB1->B1_GPIETU)
								nVlrBase := (cAliasID1)->D1_TOTAL
								If !Empty((cAliasID1)->D1_VALDESC)   //LEMP(30/06/11):Existe un descuente a nivel item y no se consideraba
									nVlrBase-=(cAliasID1)->D1_VALDESC
									//nVlrTit -= SD1->D1_VALDESC
								Endif
		
								nPos := Ascan(aGrpIETU,{|grp| grp[1] == SB1->B1_GPIETU})
								If nPos == 0
									Aadd(aGrpIETU,{SB1->B1_GPIETU,0})
									nPos := Len(aGrpIETU)
								Endif
								SFC->(DbSeek(xFilial("SFC") + (cAliasID1)->D1_TES))
								While !(SFC->(Eof())) .And. SFC->FC_TES == (cAliasID1)->D1_TES
									Do Case
										Case SFC->FC_IMPOSTO == "IVC"
											nVlrBase := nVlrBase -&cCpoIVC
										Case SFC->FC_IMPOSTO == cIEPS
											nVlrBase := nVlrBase- &cCpoIEP
										OtherWise
											If !(SFC->FC_INCDUPL == SFC->FC_INCNOTA)
												IF SFC->FC_INCDUPL=="2"  .And. SFC->FC_INCNOTA =="3"
													SFB->(DbSeek(xFilial("SFB") + SFC->FC_IMPOSTO))
													cCpoImp := "(cAliasID1)->D1_VALIMP" + AllTrim(SFB->FB_CPOLVRO)
		//											aTitulos[nTit,7] +=( (&cCpoImp * aTitulos[nTit,8]) * nPorce)
													aTitulos[nTit,7] +=( (&cCpoImp * aTitulos[nTit,8]) * 1)
											   ElseIf SFC->FC_INCDUPL=="2"  .And. SFC->FC_INCNOTA =="1"
										            SFB->(DbSeek(xFilial("SFB") + SFC->FC_IMPOSTO))
													cCpoImp := "(cAliasID1)->D1_VALIMP" + AllTrim(SFB->FB_CPOLVRO)
													aTitulos[nTit,7] +=(( (&cCpoImp * aTitulos[nTit,8]) * nPorce)*2)
												ElseIf SFC->FC_INCDUPL=="1"  .And. SFC->FC_INCNOTA =="3"
													SFB->(DbSeek(xFilial("SFB") + SFC->FC_IMPOSTO))
													cCpoImp := "(cAliasID1)->D1_VALIMP" + AllTrim(SFB->FB_CPOLVRO)
													aTitulos[nTit,7] -=( (&cCpoImp * aTitulos[nTit,8]) * nPorce)
												ElseIf SFC->FC_INCDUPL=="1"  .And. SFC->FC_INCNOTA =="2"
													SFB->(DbSeek(xFilial("SFB") + SFC->FC_IMPOSTO))
													cCpoImp := "(cAliasID1)->D1_VALIMP" + AllTrim(SFB->FB_CPOLVRO)
													aTitulos[nTit,7] -=(( (&cCpoImp * aTitulos[nTit,8]) * nPorce)*2)
												Endif
											EndIf
									EndCase
									SFC->(DbSkip())
								Enddo
								If aTitulos[nTit,1] == 0 .And. !lCmpRev
									aGrpIETU[nPos,2] += nVlrBase
								Else
									aGrpIETU[nPos,2] -= nVlrBase
								Endif
							Endif
						Endif
						//SD1->(DbSkip())
						(cAliasID1)->(DbSkip())
					Enddo
					//��������������������������������������������������������������Ŀ
					//�O valor total da nota e convertido para a moeda 1 e calcula-se�
					//�a proporcao entre o valor pago do titulo e o total da nota e  �
					//�aplica-se a mesma proporcao aos valores dos grupos de IETU,   �
					//�obtendo-se ao final os detalhes dos pagamentos (aDetPag).     �
					//����������������������������������������������������������������
					// <ARL 20/10/11>
					//If lDebitoPost
					//	nTtlFat := xMoeda(SF1->F1_VALBRUT,nMoeda,1,dFechaDeb,MsDecimais(1)) // Tomar paridad de la fecha de debitacion de CH
					If !(nTaxaSEK == 0)
						nTtlFat := xMoeda(nValBrut,nMoeda,1,nDtDigit,MsDecimais(1),nTaxaSEK) // Tasa pactada
					Else
					// <\ARL>
						nTtlFat := xMoeda(nValBrut,nMoeda,1,nDtDigit,MsDecimais(1),nTxMoeda)
					Endif
		
					nPorc   := aTitulos[nTit,7] / nTtlFat
					//If !(cTipMoeSEK=='2')    //LEMP(16/06/11): Para que cuando sea moneda2 tome en cuenta % mayores a 1
					If cTipMoeSEK=='1'    		// ARL(17/10/11) solo forza para moneda 1
						nPorc := Min(1,nPorc)
					Endif
		
					// <ARL 16/11/11> Forzar porcentaje a 1 si compensaci�n igual a valor factura
					If nTtlPags == nValBrut
						nPorc := 1
					Endif
					// <\ARL>    
					
					//LEMP(13/02/13)-TGQNP7
					If aTitulos[nTit,7] < 0 .And. nPorc < 0 					
						nPorc := Abs(nPorc)//Invertir signo de % para que no duplique... neg / neg ==> pos
					Endif
					
					If Len(aTitulos[nTit]) > 10 .And. aTitulos[nTit,11]<>0
						//If !(cTipMoeSEK=='2')  //LEMP(16/06/11)
						If cTipMoeSEK=='1'    	// ARL(17/10/11) solo forza para moneda 1
							nTxMoeda:= aTitulos[nTit,11]
						Endif
					EndIf
		
					For nI := 1 To Len(aGrpIETU)
						// <ARL 20/10/11>
						//If lDebitoPost
						//	aGrpIETU[nI,2] := xMoeda(aGrpIETU[nI,2],nMoeda,1,dFechaDeb,MsDecimais(1))
						If !(nTaxaSEK == 0)
							aGrpIETU[nI,2] := xMoeda(aGrpIETU[nI,2],nMoeda,1,nDtDigit,MsDecimais(1),nTaxaSEK)
						Else
						// <\ARL>
							aGrpIETU[nI,2] := xMoeda(aGrpIETU[nI,2],nMoeda,1,nDtDigit,MsDecimais(1),nTxMoeda)
						Endif
		 				nPos := Ascan(aDetPag,{|det| det[10] == aTitulos[nTit,9] .And. det[6] == aTitulos[nTit,2] .And. det[4] == aTitulos[nTit,3] .And. det[5] == aTitulos[nTit,4] .And. det[7] == aTitulos[nTit,5] .And. det[8] == aTitulos[nTit,6] .And. det[2] == aGrpIETU[nI,1]})  //LEMP(27/05/11): Se agrego la condici�n para validar los grupos.
						If nPos == 0
							If aTitulos[nTit,1] == 1	//compensacao
								Aadd(aDetPag,{(cAlias)->(E5_DATA),aGrpIETU[nI,1],aGrpIETU[nI,2] * nPorc,aTitulos[nTit,3],aTitulos[nTit,4],aTitulos[nTit,2],aTitulos[nTit,5],aTitulos[nTit,6],"P",aTitulos[nTit,9],aTitulos[nTit,10]})
							Else
								Aadd(aDetPag,{(cAlias)->(E5_DATA),aGrpIETU[nI,1],aGrpIETU[nI,2] * nPorc,aTitulos[nTit,3],aTitulos[nTit,4],aTitulos[nTit,2],aTitulos[nTit,5],aTitulos[nTit,6],"P",(cAlias)->E5_TIPO,(cAlias)->E5_DOCUMEN})
							Endif
						Else
							aDetPag[nPos,3] += aGrpIETU[nI,2] * nPorc 
							If  aDetPag[nPos,3]==0    //LEMP(TGSAZ0-21/02/13):Considerar la TC
								aDel(aDetPag,nPos)    
								aSize(aDetPag,len(aDetPag)-1)
							Endif
						Endif
					Next	  		
		  		Endif  
		  		
			ElseIf lIETUSF1P
				// No existe factura de entrada... ejecutar PE para determinar montos de pago e impuestos //LEMP(13/02/13)-TGQNP7
				aDetPagPE := ExecBlock("IETUSF1P",.F.,.F.,{aTitulos[nTit], aDetPag})
				If ValType(aDetPagPE) == 'A' .And. Len(aDetPagPE) > 0
					aDetPag := aClone( aDetPagPE )
				Endif
			Endif
			dbCloseArea(cAliasSF1)
		Else    
			//Validar si el documento esta compensando TOTALMENTE (si no esta compensado, se agrega el PA)
			If  VldSE2Comp(cOrdPago,(cAlias)->E5_CLIFOR,(cAlias)->E5_LOJA,Month((cAlias)->E5_DATA),1,@nValSE2)  //LEMP(07/03/12):Validar PA compensados 
				Aadd(aDetPag,{(cAlias)->(E5_DATA),cIETUPA,aTitulos[nTit,7],aTitulos[nTit,3],aTitulos[nTit,4],aTitulos[nTit,2],aTitulos[nTit,5],aTitulos[nTit,6],"P",aTitulos[nTit,9],(cAlias)->E5_DOCUMEN})
				//Validar si esta compensado PARCIALMENTE (se agrega PA parcial)
			Elseif VldSE2Comp(cOrdPago,(cAlias)->E5_CLIFOR,(cAlias)->E5_LOJA,Month((cAlias)->E5_DATA),2,@nValSE2)  //LEMP(25/01/13):Validar PA compensados parcialmente 
				Aadd(aDetPag,{(cAlias)->(E5_DATA),cIETUPA,nValSE2,aTitulos[nTit,3],aTitulos[nTit,4],aTitulos[nTit,2],aTitulos[nTit,5],aTitulos[nTit,6],"P",aTitulos[nTit,9],(cAlias)->E5_DOCUMEN}) 
			Endif
	  	Endif
	Next


	/*
	Punto de entrada para agregar movimientos Bancarios*/
	If  Empty(aTitulos) .And. Empty(aCompens)
		If  ExistBlock("IETUMOVP")
			SE5->(DBGOTO((cAlias)->R_E_C_N_O_))
			aCpiaPag  := ExecBlock("IETUMOVP",.F.,.F.,{aDetPag})
			If Valtype(aCpiaPag)=='A'
				aDetPag := aClone(aCpiaPag)
			Endif
		Endif
	Endif


	(cAlias)->(DbSkip())
	nReg++
	oMeter:Set(nReg)
Enddo
(cAlias)->(DbCloseArea())
nReg++
oMeter:Set(nReg)

oMsg:cCaption := " " + STR0021		//"C�lculo dos pagamentos efetuados encerrado"
oPnl:Refresh()
oMeter:Free()
ProcessMessages()
Return(lRet)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �M927ARec  �Autor  �Marcello            �Fecha � 24/09/2008  ���
�������������������������������������������������������������������������͹��
���Desc.     �Processa as entradas (ingresos) para base do IETU           ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � IETU - Mexico                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function M927ARec(cMes,cAno,oPnl)
Local cQuery	:= ""
Local cQryAux	:= ""
Local cFilUsr	:= ""
Local cAlias	:= ""
Local cIetuGrp	:= ""  		// Grupo IETU en uso
Local cFilSEL	:= ""
Local cFilSE5	:= ""
Local cChave	:= ""

Local cCpoIVC	:= ""
Local cCpoIEP	:= ""
Local cArqTrab	:= ""
Local cIEPS		:= SubStr(GetNewPar("MV_IEPS ",""),1,3)
Local nValTit	:= 0
Local nI		:= 0
Local nTit		:= 0
Local nPos		:= 0
Local nPorc		:= 0
Local nPorcTit	:= 0
Local nTtlRec	:= 0
Local nTtlTits	:= 0
Local nVlrBase	:= 0
Local nReg		:= 0
Local dFecha1	:= Ctod("01/" + cMes + "/" + cAno,"DDMMYYYY")   
Local dFecha2	:= Ctod(StrZero(Last_day(dFecha1),2) + "/" + cMes + "/" + cAno,"DDMMYYYY")
Local aGrpIETU	:= {}
Local aTitulos	:= {}
Local aCompens	:= {}
Local aEstrSE5	:= {}
Local lRet		:= .T.
Local dData		:= Ctod("//")
Local oMsg
Local oSepar
Local oMeter
Local cIETURA	:= GetNewPar("MV_IETURA","0000")
Local aCpiaCob  := {}   
Local cSerREC   := ""  //LEMP(06/03/12):Validar el numero de serie del recibo 
Local nPrefijo  := TamSX3("E1_PREFIXO")[1]    //LEMP(TFDMC6-10/07/12):Validar si se imprimen RA/PA no compensadas  
Local nNumero   := TamSX3("E1_NUM")[1]        //LEMP(TFDMC6-10/07/12):Validar si se imprimen RA/PA no compensadas
Local nValSE1   := 0  //LEMP(TGKSXX-25/01/13):Validar las RA compensados de forma parcial
Local lIETUANTR := ExistBlock("IETUANTR")	//LEMP(TGQNPJ-14/02/13):Agregar PE
Local aDetCobPE	:= {}			//LEMP(TGQNPJ-14/02/13):Agregar PE
Local nTtlDed := 0
Local nTtlFac := 0
Local nValbrut:= 0
/*
��������������������������������������������Ŀ
�Estrutura do array aTitulos:                �
�1) Tipo - 1 compensacao; 0 normal           �
�2) Recibo                                   �
�3) Numero do documento (fatura)             �
�4) Serie do documento                       �
�5) Cliente                                  �
�6) Filial do clientec                       �
�7) Valor                                    �
�8) Especie (CH,EF,TF,NCC,etc)               �
�9) Documento (no caso de compensacao, possui�
�   o documento original da compensacao)     �
����������������������������������������������
���������������������������������������������Ŀ
�Estrutura do array aDetPag                   �
�01) Data                                     �
�02) Grupo de IETU                            �
�03) Valor                                    �
�04) Documento (fatura)                       �
�05) Serie do documento                       �
�06) Recibo                                   �
�07) Cliente                                  �
�08) Filial do clieente                       �
�09) Tipo recebimento ("R")                   �
�10) Especie (CH,EF,TF,NCC etc)               �
�11) Documento (no caso de compensacao, possui�
�    o documento original da compensacao)     �
�����������������������������������������������
*/
@01,01 Say oMsg Var " " + STR0018 Size 180,10 Pixel Of oPnl		//"Preparando as informa��es sobre recebimentos"
oMsg:Align := CONTROL_ALIGN_TOP
oMsg:nHeight := 15
oSepar := TPanel():New(01,01,,oPnl,,,,,RGB(221,221,221),5,15,.F.,.F.)
	oSepar:Align := CONTROL_ALIGN_TOP
	oSepar:nHeight := 25
oPnl:Refresh()
ProcessMessages()
//
SB1->(DbSetOrder(1))
SD2->(DbSetOrder(3))
SF2->(DbSetORder(1))
cFilSEL := xFilial("SEL")
SEL->(DbSetOrder(Retordem("SEL","EL_FILIAL+EL_SERIE+EL_RECIBO+EL_TIPODOC+EL_PREFIXO+EL_NUMERO+EL_PARCELA+EL_TIPO")))
cFilSE5 := xFilial("SE5")
SE5->(DbSetORder(7))
//
DbSelectArea("SFB")
DbSetOrder(1)
SFB->(DbSeek(xFilial("SFB")+"IVC"))
cCpoIVC := "SD2->D2_VALIMP" + AllTrim(SFB->FB_CPOLVRO)
SFB->(DbSeek(xFilial("SFB")+cIEPS))
cCpoIEP := "SD2->D2_VALIMP" + AllTrim(SFB->FB_CPOLVRO)
//
aDetCob := {}

cQuery := "select count(*) as TTLREG from " + RetSqlName("SE5")
cQuery += " where"
cQuery += " D_E_L_E_T_=''"
cQuery += " and E5_FILIAL = '" + cFilSE5 + "'"
cQuery += " and ("
cQryAux := "(E5_DATA >= '" + Dtos(dFecha1) + "'"
cQryAux += " and E5_DATA <= '" + Dtos(dFecha2) + "'"
cQryAux += " and ("
cQryAux += "(E5_RECPAG = 'R' and E5_SITUACA <> 'C' and E5_TIPODOC <> 'ES' and E5_TIPODOC <> 'BA' and E5_TIPODOC <> 'DC' )"
cQryAux += " or "
cQryAux += "(E5_MOTBX='CMP' and E5_RECPAG = 'P' and E5_SITUACA <> 'C' and E5_TIPODOC = 'ES')"
cQryAux += " or "   
cQryAux += "(E5_MOTBX='NOR' and E5_RECPAG = 'R' and E5_SITUACA <> 'C' and E5_TIPODOC = 'VL')"  //Baixa Automatica ... IN ('P','R') //LEMP(13/02/13)-TGQNP7
cQryAux += " or "
cQryAux += "(E5_MOTBX='LIQ' and E5_RECPAG = 'R' and E5_SITUACA <> 'C' and E5_TIPODOC = 'BA')"	
cQryAux += "))"
/*        
Ponto de entrada para alteracao do filtro*/
If ExistBlock("IETUFILR")
	cFilUsr	:=	ExecBlock("IETUFILR",.F.,.F. ,{cQryAux})
	If !Empty(cFilUsr)
		cQryAux := cFilUsr
	Endif
Endif
cQuery += cQryAux + ")"
//
cAlias := GetNextAlias()
cQuery := ChangeQuery(cQuery)
dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.T.,.T.)
nReg := (cAlias)->TTLREG
(cAlias)->(DbCloseArea())
//
//LEMP(08/11/11): Obtener la TC del movimiento de pago 
//LEMP(08/05/12): Se debe obtener el E5_CLIFOR para el caso que sa registro liquidacion (pago con CH en SIGALOJA)
cQuery := "select E5_DATA,E5_TIPO,E5_TIPODOC,E5_VALOR,E5_MOEDA,E5_VLMOED2,E5_ORDREC,E5_SERREC,E5_MOTBX,E5_DTDISPO,E5_NUMERO,E5_PREFIXO,E5_PARCELA,E5_CLIENTE,E5_LOJA,E5_DOCUMEN,E5_RECPAG, E5_TXMOEDA, E5_CLIFOR, E5_FORNADT, E5_LOJAADT, R_E_C_N_O_ FROM " + RetSqlName("SE5")
cQuery += " where"
cQuery += " D_E_L_E_T_=''"
cQuery += " and E5_FILIAL = '" + cFilSE5 + "'"
cQuery += " and ("
cQuery += cQryAux + ")"
cAlias := GetNextAlias()
cQuery := ChangeQuery(cQuery)
dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.T.,.T.)
TCSetField(cAlias,"E5_DATA","D",8,0)

dbSelectArea(cAlias)
(cAlias)->(DbGotop())
oMsg:cCaption := " " + STR0022 + ". " + STR0020 + "..."		//"Executando o c�lculo dos recebimentos"###"Aguarde"
oPnl:Refresh()
ProcessMessages()
oMeter := TMeter():New(08,08,,nReg+1,oSepar,oPnl:nClientWidth-16,8,,.T.,,,.F.,,,,,)
oMeter:Align := CONTROL_ALIGN_TOP
oMeter:nHeight := 15
nReg := 0

While !((cAlias)->(Eof()))
	aTitulos:= {}
	aCompens:= {}
	nValTit := 0
	nTtlRec := 0
	nTtlTits:= 0 
	nValSE1 := 0  //LEMP(TGKSXX-25/01/13):Validar las RA compensados de forma parcial
	//�����������������������������������������������������������Ŀ
	//�Uma compensacao pode ser  feita de duas maneiras:          �
	//�1 - Manualmente, associando um titulo de compensacao       �
	//�     (ncc por exemplo) a um titulo a receber.              �
	//�2 - "Automaticamente", incluindo um titulo de compensacao  �
	//�     no recibo. Neste caso o valor da compensacao e        �
	//�     subtraido do valor total do recibo.                   �
	//�������������������������������������������������������������    
	
	//LEMP(TFDMC6-10/07/12):Validar si se imprimen RA/PA no compensadas (Solo cuando se consideren RA/PA sin compensar)
	If  cIETUANT $ '1|4'   //LEMP(TGRZE7-20/01/13):Agregar la opcion de 'solo considerar RA' 
		//Validar si el RA fue generado en mes diferente a la compensacion
		//LEMP(11/04/13)-TGWJAJ:Se modifico el cliente que se utiliza por el caso de RA con un cliente y NF con otro cliente
		If  SUBSTR((cAlias)->E5_DOCUMENT,nPrefijo+nNumero+2,2)=='RA' //ARL (26/02/2013)
			If  VldRAComp((cAlias)->E5_FORNADT,(cAlias)->E5_LOJAADT,SUBSTR((cAlias)->E5_DOCUMENT,nPrefijo+1,nNumero),SUBSTR((cAlias)->E5_DOCUMENT,1,nPrefijo),;
				(cAlias)->E5_PARCELA,SUBSTR((cAlias)->E5_DOCUMENT,nPrefijo+nNumero+2,2), strzero(year((cAlias)->E5_DATA),4)+strzero(Month((cAlias)->E5_DATA),2) )      
				(cAlias)->(DbSkip())
				Loop
			Endif
		Endif
	Endif
	
	If (cAlias)->E5_MOTBX == "CMP"
		nValTit :=(cAlias)->E5_VALOR
		//���������������������������������������������������������������������������������Ŀ
		//�Quando uma compensacao e cancelada, e criado um registro de estorno. Este estorno�
		//�e guardado para ser subtraido do valor recebido.                                 �
		//�����������������������������������������������������������������������������������
		If (cAlias)->E5_TIPODOC == "ES" .And. (cAlias)->E5_TIPO == "NF "
			Aadd(aTitulos,{0,"",(cAlias)->E5_NUMERO,(cAlias)->E5_PREFIXO,(cAlias)->E5_CLIENTE,(cAlias)->E5_LOJA,-nValTit,1,(cAlias)->E5_TIPO,"", 0})
		Else
			Aadd(aTitulos,{0,Iif(Alltrim((cAlias)->E5_TIPODOC)="CP","CMP",""),(cAlias)->E5_NUMERO,(cAlias)->E5_PREFIXO,(cAlias)->E5_CLIENTE,(cAlias)->E5_LOJA,nValTit,1,(cAlias)->E5_TIPO,"", 0})  //****
		Endif 
	Elseif ((cAlias)->E5_MOTBX == "NOR" .And. (cAlias)->E5_TIPODOC == "LJ") .OR. ((cAlias)->E5_MOTBX == "LIQ" .And. (cAlias)->E5_TIPODOC == "BA")
		nValTit :=(cAlias)->E5_VALOR
		//���������������������������������������������������������������������������������Ŀ 
		//�LEMP(08/05/12):Cambios para considerar movimientos de LOJA                       �
		//�Cuando se realiza una venta desde sigaloja y el movimiento es dado de baja auto- �
		//�maticamente o cuando se paga con CH y se hace una liquidaci�n del mismo.         �
		//�����������������������������������������������������������������������������������

		Aadd(aTitulos,{0,"",(cAlias)->E5_NUMERO,(cAlias)->E5_PREFIXO,Iif(((cAlias)->E5_MOTBX == "LIQ" .And. (cAlias)->E5_TIPODOC == "BA"),(cAlias)->E5_CLIFOR,(cAlias)->E5_CLIENTE),(cAlias)->E5_LOJA,nValTit,1,(cAlias)->E5_TIPO,"", 0})
	Else
		//If !Empty((cAlias)->E5_MOTBX) // <ARL 26/10/11 \> <ARL 30/11/11 \>
			cRecibo := (cAlias)->E5_ORDREC  
			cSerREC := (cAlias)->E5_SERREC //LEMP(06/03/12):Validar la serie del recibo
			SEL->(DbSeek(cFilSEL + cSerREC + cRecibo))  //LEMP(06/03/12):Validar la serie del recibo 
			While !(SEL->(Eof())) .And. SEL->EL_FILIAL == cFilSEL .And. SEL->EL_RECIBO == cRecibo .And. SEL->EL_SERIE == cSerREC //LEMP(06/03/12):Validar la serie del recibo 
				If SEL->EL_TIPO $ "CH |EF |TF |NCC|NCI"
					If SEL->EL_TIPO $ "NCC|NCI|"
						//�������������������������������������������������������������Ŀ
						//�A compensacao automatica gera um unico registro para todo o  �
						//�recibo. Quando o valor pago e maior que a soma dos titulos   �
						//�e gerado um RA. Neste caso, o valor e armazenado mas nao     �
						//�integrara a soma dos pagamentos. Noutro caso, o valor e      �
						//�guardado para depois ser rateado entre os titulos do recibo. �
						//���������������������������������������������������������������
						If SEL->EL_TIPODOC == "RA"
							nTtlRec += xMoeda(SEL->EL_VALOR,Val(SEL->EL_MOEDA),1,SEL->EL_EMISSAO,MsDecimais(1))
						Else
							nTtlRec -= xMoeda(SEL->EL_VALOR,Val(SEL->EL_MOEDA),1,SEL->EL_EMISSAO,MsDecimais(1))
							Aadd(aCompens,{1,SEL->EL_RECIBO,SEL->EL_NUMERO,SEL->EL_PREFIXO,SEL->EL_CLIENTE,SEL->EL_LOJA,SEL->EL_VALOR,1,SEL->EL_TIPO,""})
						Endif
					Endif
				Else
				//��������������������������������������������������������Ŀ
				//�Para recebimentos "normais" os dados sao guardados para �
				//�depois entrarem no calculo da base de IETU              �
				//����������������������������������������������������������
					If  (((SEL->EL_TIPO == "NF " .And. (cAlias)->E5_TIPODOC !='DC')  .Or. (Iif(cIETUANT $ '1|4',(SEL->EL_TIPODOC == "RA" .And. (cAlias)->E5_RECPAG== "R"),.T.))) .And. (cAlias)->E5_TIPODOC !='MT') //LEMP(TGRZE7-20/01/13):Agregar la opcion de 'solo considerar RA'
						//E5_TIPODOC <> 'DC': Los descuentos en RA no aplican
						//nValTit := xMoeda(SEL->EL_VALOR,Val(SEL->EL_MOEDA),1,SEL->EL_EMISSAO,MsDecimais(1))  //THDYV3}
						nTtlDed := xMoeda(SEL->EL_MULTA,Val(SEL->EL_MOEDA),1,SEL->EL_DTDIGIT,MsDecimais(1))
						nValTit := xMoeda(SEL->EL_VALOR,Val(SEL->EL_MOEDA),1,SEL->EL_DTDIGIT,MsDecimais(1))
						nTtlTits += nValTit
						nTtlRec += nValTit
						Aadd(aTitulos,{0,SEL->EL_RECIBO,SEL->EL_NUMERO,SEL->EL_PREFIXO,SEL->EL_CLIENTE,SEL->EL_LOJA,nValTit,1,If(SEL->EL_TIPO == "NF ",(cAlias)->E5_TIPO,IIF(cIETUANT $ '1|4',SEL->EL_TIPODOC,(cAlias)->E5_TIPO)),"",nTtlDed})
					Endif
				Endif
				SEL->(DbSkip())
			Enddo
		//EndIf
		//conversao do valor pago para a moeda 1
		nValTit := xMoeda((cAlias)->E5_VALOR,Val((cAlias)->E5_MOEDA),1,(cAlias)->E5_DATA,MsDecimais(1),(cAlias)->E5_TXMOEDA) //LEMP(08/11/11):Convertir a moneda 1 con la TC del movimiento
		//Porcentagem do valor recebido em relacao ao valor total do recebimento
		nPorcTit := nValTit / nTtlRec      //Porcentaje del valor recibido en relaci�n con el valor total de la recepci�n
		//��������������������������������Ŀ
		//|Tratamento para Baixa Automatica| **Caso 3**
		//����������������������������������
		If (cAlias)->E5_TIPO == "NF " .And. (cAlias)->E5_TIPODOC == "VL"
			nPorcTit := 1
			Aadd(aTitulos,{0,"",(cAlias)->E5_NUMERO,(cAlias)->E5_PREFIXO,(cAlias)->E5_CLIENTE,(cAlias)->E5_LOJA, (cAlias)->E5_VALOR, (cAlias)->E5_TIPODOC,"", "", 0})
		EndIf
		//����������������������������������������������������������������Ŀ
		//�Os valores dos titulos sao recalculados para serem proporcionais�
		//�ao recebimento efetuado                                         �
		//������������������������������������������������������������������
		For nI := 1 To Len(aCompens)
			//�������������������������������������������������������������Ŀ
			//�Quando ha compensacoes, ratea-se o valor entre os titulos do �
			//�recibo, criando-se um items para cada titulo com o valor da  �
			//�compensacao.                                                 �
			//���������������������������������������������������������������
			For nTit := 1 To Len(aTitulos)
				nPorc := aTitulos[nTit,7] / nTtlTits
				Aadd(aTitulos,{1,aTitulos[nTit,2],aTitulos[nTit,3],aTitulos[nTit,4],aTitulos[nTit,5],aTitulos[nTit,6],aCompens[nI,7]*nPorc,1,aCompens[nI,9],aCompens[nI,4] + aCompens[nI,3] + Space(TamSX3("E2_PARCELA")[1]) + aCompens[nI,9]}, 0)
			Next
		Next
		For nI := 1 To Len(aTitulos)
			aTitulos[nI,7] := aTitulos[nI,7] * nPorcTit
			aTitulos[nI,8] := nPorcTit
		Next
	Endif
	//�����������������������������������������������������������������Ŀ
	//�Busca-se os itens dos documentos originais (faturas) para        �
	//�obter-se os seus valores e distribui-los entre os grupos de IETU.�
	//�������������������������������������������������������������������
	For nTit := 1 To Len(aTitulos)
		If (Iif(cIETUANT $'1|4',!(aTitulos[nTit,9] == "RA"),.T.))   //LEMP(TGRZE7-20/01/13):Agregar la opcion de 'solo considerar RA'
			aGrpIETU := {}
			nTtlFat := 0			
			//SF2->(DbSeek(xFilial("SF2") + aTitulos[nTit,3] + aTitulos[nTit,4] + aTitulos[nTit,5] + aTitulos[nTit,6]))
			//������������������������������������������������������������������
			//�Distribui os valores entre os grupos de IETU (considera somente �
			//�os itens que estejam com o grupo cadastrado). O valor utilizado �
			//�e sem impostos.                                                 �
			//������������������������������������������������������������������
			/*Projeto Chave Unica - Tiago Silva
				A query a seguir foi desenvolvida para substituir o seek que era utilizado nas tabelas SF2/SD2, pois utilizava prefixo como serie.			
			*/
			cQryID2:=" SELECT "
			cQryID2+=" 	F2_VALBRUT, F2_MOEDA, F2_DTDIGIT, F2_TXMOEDA, D2_COD, D2_TES, D2_TOTAL"
			cQryID2+=" FROM "+retSqlName("SF2")+" SF2, "
			cQryID2+=" 		"+retSqlName("SD2")+" SD2  "
			cQryID2+=" WHERE SF2.F2_FILIAL  = SD2.D2_FILIAL "
			cQryID2+="   AND SF2.F2_DOC	    = SD2.D2_DOC "
			cQryID2+="   AND SF2.F2_SERIE 	= SD2.D2_SERIE "
			cQryID2+="   AND SF2.F2_FORNECE = SD2.D2_FORNECE "
			cQryID2+="   AND SF2.F2_LOJA    = SD2.D2_LOJA "								
			cQryID2+="   AND SF2.D_E_L_E_T_ ='' "
			cQryID2+="   AND SF2.F2_FILIAL  ='"+xFilial("SF2")+"' "
			cQryID2+="   AND SF2.F2_DOC	   ='"+aTitulos[nTit,3]+"' "
			cQryID2+="   AND SF2.F2_PREFIXO ='"+aTitulos[nTit,4]+"' "
			cQryID2+="   AND SF2.F2_FORNECE ='"+cFornece+"' "
			cQryID2+="   AND SF2.F2_LOJA    ='"+cLojaFor+"' "								
			cQryID2+="   AND SF2.D_E_L_E_T_ ='' "
			cQryID2	  := ChangeQuery(cQryID2)
			cAliasSD2 := CriaTrab(Nil,.F.)
			
			dbUseArea( .T., "TOPCONN", TcGenQry( , , cQryID2 ), cAliasID2, .T., .T. )
			dbSelectArea(cAliasID2)	
			If !((cAliasID2)->(EOF()))	
				nValbrut	:= (cAliasID2)->F2_VALBRUT
				nMoeda		:= (cAliasID2)->F2_MOEDA
				nDtDigit	:= (cAliasID2)->F2_DTDIGIT
				nTxMoeda	:= (cAliasID2)->F2_TXMOEDA
			Endif	
			
			While !((cAliasSD2)->(EOF()))
			//SD2->(DbSeek(cFilSE + aTitulos[nTit,3] + aTitulos[nTit,4] + aTitulos[nTit,5] + aTitulos[nTit,6])) 
			//While !(SD2->(Eof())) .And. SD2->D2_FILIAL == cFilSE .And. SD2->D2_DOC == aTitulos[nTit,3] .And. AllTrim(SD2->D2_SERIE) == AllTrim(SF2->F2_SERIE) .And. SD2->D2_CLIENTE == aTitulos[nTit,5] .And. SD2->D2_LOJA == aTitulos[nTit,6] //AllTrim(aTitulos[nTit,4]) substituido por F2_SERIE
				If SB1->(MsSeek(xFilial("SB1") + (cAliasID2)->D2_COD))
					If !Empty(SB1->B1_GPIETU)
						nPos := Ascan(aGrpIETU,{|grp| grp[1] == SB1->B1_GPIETU})
						If nPos == 0
							Aadd(aGrpIETU,{SB1->B1_GPIETU,0})
							nPos := Len(aGrpIETU)
						Endif
	
						SFC->(DbSeek(xFilial("SFC") + (cAliasID2)->D2_TES))
						While !(SFC->(Eof())) .And. SFC->FC_TES == (cAliasID2)->D2_TES
							Do Case
								Case SFC->FC_IMPOSTO == "IVC"
									nPorc :=  &cCpoIVC
								Case SFC->FC_IMPOSTO == cIEPS
									nPorc :=  &cCpoIEP
								OtherWise
									nPorc := 0
							EndCase
							If  aTitulos[nTit,1] == 0
								aGrpIETU[nPos,2] += ((cAliasID2)->D2_TOTAL - nPorc)
							Else
								aGrpIETU[nPos,2] -= ((cAliasID2)->D2_TOTAL - nPorc)
							Endif
							SFC->(DbSkip())
						EndDo
					Endif
				EndIf
				//SD2->(DbSkip())
				(cAliasID2)->(DbSkip())
			Enddo
			dbCloseArea(cAliasID2)	
				//��������������������������������������������������������������Ŀ
				//�O valor total da nota e convertido para a moeda 1 e calcula-se�
				//�a proporcao entre o valor recebido do titulo e o total da nota�
				//�e aplica-se a mesma proporcao aos valores dos grupos de IETU, �
				//�obtendo-se ao final os detalhes dos recebimentos (aDetCob).   �
				//����������������������������������������������������������������
			nTtlFat := xMoeda(nValBrut,nMoeda,1,nDtDigit,MsDecimais(1),nTxMoeda)
			//Se resta el valor de la multa a la factura
			If aTitulos[nTit,11] != 0                         
				nTtlFac = aTitulos[nTit,7] - aTitulos[nTit,11]
				nPorc := nTtlFac / nTtlFat
			else
				nPorc := aTitulos[nTit,7] / nTtlFat
			endif
			For nI := 1 To Len(aGrpIETU)
				// Si la factura tiene una multa, se suma al total.
				If aTitulos[nTit,11] != 0                         
					aGrpIETU[nI,2] = aGrpIETU[nI,2] + aTitulos[nTit,11]
				EndIf
				aGrpIETU[nI,2] := xMoeda(aGrpIETU[nI,2],nMoeda,1,nDtDigit,MsDecimais(1),nTxMoeda)
				nPos := Ascan(aDetCob,{|det| det[10] == aTitulos[nTit,9] .And. det[6] == aTitulos[nTit,2] .And. det[4] == aTitulos[nTit,3] .And. det[5] == aTitulos[nTit,4] .And. det[7] == aTitulos[nTit,5] .And. det[8] == aTitulos[nTit,6]})
				If nPos == 0
					If aTitulos[nTit,1] == 1	//compensacao
						Aadd(aDetCob,{(cAlias)->(E5_DATA),aGrpIETU[nI,1],aGrpIETU[nI,2] * nPorc,aTitulos[nTit,3],aTitulos[nTit,4],aTitulos[nTit,2],aTitulos[nTit,5],aTitulos[nTit,6],"R",(cAlias)->E5_TIPO,aTitulos[nTit,10]})
					Else
						Aadd(aDetCob,{(cAlias)->(E5_DATA),aGrpIETU[nI,1],aGrpIETU[nI,2] * nPorc,aTitulos[nTit,3],aTitulos[nTit,4],aTitulos[nTit,2],aTitulos[nTit,5],aTitulos[nTit,6],"R",(cAlias)->E5_TIPO,(cAlias)->E5_DOCUMENT})
					Endif
				Else
					aDetCob[nPos,3] += aGrpIETU[nI,2] * nPorc
				Endif
			Next
		Else                     
		   	If  VldSE1Comp((cAlias)->E5_ORDREC,(cAlias)->E5_SERREC,Month((cAlias)->E5_DATA),1,@nValSE1) //LEMP(08/03/12):Validar RA compensados (entra si el RA NO ESTA COMPENSADO)
		   		If  lIETUANTR //LEMP(14/02/13)-TGQNPJ:Agregar PE 
					aDetCobPE := ExecBlock("IETUANTR",.F.,.F.,{(cAlias)->R_E_C_N_O_,aTitulos[nTit], aDetCob})
					If ValType(aDetCobPE) == 'A' .And. Len(aDetCobPE) > 0
						aDetCob := aClone( aDetCobPE )
					Endif
				Else
					Aadd(aDetCob,{(cAlias)->(E5_DATA),cIETURA,aTitulos[nTit,7],aTitulos[nTit,3],aTitulos[nTit,4],aTitulos[nTit,2],aTitulos[nTit,5],aTitulos[nTit,6],"R",aTitulos[nTit,9],(cAlias)->E5_DOCUMEN})
				Endif
			Elseif VldSE1Comp((cAlias)->E5_ORDREC,(cAlias)->E5_SERREC,Month((cAlias)->E5_DATA),2,@nValSE1) //LEMP(25/01/13):Validar si la RA fueron compensados de forma PARCIAL 
				If  lIETUANTR //LEMP(14/02/13)-TGQNPJ:Agregar PE 
					aDetCobPE := ExecBlock("IETUANTR",.F.,.F.,{(cAlias)->R_E_C_N_O_,aTitulos[nTit], aDetCob})
					If ValType(aDetCobPE) == 'A' .And. Len(aDetCobPE) > 0
						aDetCob := aClone( aDetCobPE )
					Endif
				Else
					Aadd(aDetCob,{(cAlias)->(E5_DATA),cIETURA,nValSE1,aTitulos[nTit,3],aTitulos[nTit,4],aTitulos[nTit,2],aTitulos[nTit,5],aTitulos[nTit,6],"R",aTitulos[nTit,9],(cAlias)->E5_DOCUMEN})
				Endif
		   	Endif
	  	Endif
	Next

	/*Punto de entrada para agregar movimientos Bancarios*/
	If  Empty(aTitulos) .And. Empty(aCompens)
		If  ExistBlock("IETUMOVC")
			SE5->(DBGOTO((cAlias)->R_E_C_N_O_))
			aCpiaCob  := ExecBlock("IETUMOVC",.F.,.F.,{aDetCob})
			If Valtype(aCpiaCob)=='A'
				aDetCob := aClone(aCpiaCob)
			Endif
		Endif
	Endif

	(cAlias)->(DbSkip())
	nReg++
	oMeter:Set(nReg)
Enddo
(cAlias)->(DbCloseArea())
nReg++
oMeter:Set(nReg)

oMsg:cCaption := " " + STR0023		//"C�lculo dos recebimentos encerrado"
oPnl:Refresh()
oMeter:Free()
ProcessMessages()
Return(lRet)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �M927AFec  �Autor  �Marcello            �Fecha � 24/09/2008  ���
�������������������������������������������������������������������������͹��
���Desc.     �Efetua a atualizacao dos arquivos com os dados do processa- ���
���          �mento das entradas e saidas.                                ���
�������������������������������������������������������������������������͹��
���Uso       � IETU - Mexico                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function M927AFec(cMes,cAno,oPnl)
Local cFilCD	:= ""
Local cFecha	:= ""
Local cDoc		:= ""
Local cTipo		:= ""
Local cSer		:= ""
Local nX		:= 0
Local nPos		:= 0
Local nReg		:= 0
Local aGrpIETU	:= {}
Local aArea		:= {}
Local lRet		:= .T.
Local oMsg
Local oMeter
Local oSepar  
Local cTotEGR
Local cQryId
Local cAliasSF1

@01,01 Say oMsg Var " " + STR0024 + ". " + STR0020 + "..." Size 180,10 Pixel Of oPnl		//"Executando o fechamento"###"Aguarde"
oMsg:Align := CONTROL_ALIGN_TOP
oMsg:nHeight := 15                                                                                       

oSepar := TPanel():New(01,01,,oPnl,,,,,RGB(221,221,221),5,15,.F.,.F.)
	oSepar:Align := CONTROL_ALIGN_TOP
	oSepar:nHeight := 25
oPnl:Refresh()
ProcessMessages()
CDJ->(DbSetOrder(1))
CDK->(DbSetOrder(1))
nReg := Len(aDetCob) + Len(aDetPag)
oMeter := TMeter():New(08,08,,nReg,oSepar,oPnl:nClientWidth-16,8,,.T.,,,.F.,,,,,)
	oMeter:Align := CONTROL_ALIGN_TOP
	oMeter:nHeight := 15
nReg := 0
For nX := 1 To Len(aDetCob)
	If !Empty(aDetCob[nX,2])
		nPos := Ascan(aGrpIETU,{|grp| grp[1] == aDetCob[nX,2]})
		If nPos == 0
			Aadd(aGrpIETU,{aDetCob[nX,2],0,0})
			nPos := Len(aGrpIETU)
		Endif
		If aDetCob[nX,9] == "R"
			If aDetCob[nX,10] $ "NCC|NCI"
				aGrpIETU[nPos,2] -= aDetCob[nX,3]
			Else
				aGrpIETU[nPos,2] += aDetCob[nX,3]
			Endif
		Else
			aGrpIETU[nPos,3] += aDetCob[nX,3]
		Endif
	Endif
	nReg++
	oMeter:Set(nReg)
Next
For nX := 1 To Len(aDetPag)
	If !Empty(aDetPag[nX,2])
		nPos := Ascan(aGrpIETU,{|grp| grp[1] == aDetPag[nX,2]})
		If nPos == 0
			Aadd(aGrpIETU,{aDetPag[nX,2],0,0})
			nPos := Len(aGrpIETU)
		Endif
		If aDetPag[nX,9] == "P"
			If aDetPag[nX,10] $ "NCP|NDI"
				aGrpIETU[nPos,3] -= aDetPag[nX,3]
			Else
				aGrpIETU[nPos,3] += aDetPag[nX,3]
			Endif
		Else
			aGrpIETU[nPos,2] += aDetPag[nX,3]
		Endif
	Endif
	nReg++
	oMeter:Set(nReg)
Next
//Atualzando os arquivos
oMsg:cCaption := " " + STR0025		//"Atualizando a base de dados"
oMeter:Set(0)
oMeter:nTotal := Len(aGrpIETU) + Len(aDetCob) + Len(aDetPag) + 1
nReg := 0
oPnl:Refresh()
aArea := GetArea()
cFilCD:= xFilial("CDJ")
cFecha:= cAno + cMes
DbSelectArea("CDJ")
Begin Transaction
	For nX := 1 To Len(aGrpIETU)
		RecLock("CDJ",.T.)
		Replace CDJ->CDJ_FILIAL	With cFilCD
		Replace CDJ->CDJ_FECHA	With cFecha
		Replace CDJ->CDJ_GPIETU	With aGrpIETU[nX,1]
		Replace CDJ->CDJ_TOTEGR	With aGrpIETU[nX,3]
		Replace CDJ->CDJ_TOTING	With aGrpIETU[nX,2]
		CDJ->(MsUnLock())
		nReg++
		oMeter:Set(nReg)
	Next
	DbSelectArea("CDK")
	cFilCD := xFilial("CDK")
	nPos := TamSX3("F1_DOC")[1]
	For nX := 1 To Len(aDetCob)
		If aDetCob[nX,3] <> 0
			/* Projeto Chave Unica - Tiago Silva
				A query a seguir � utilizada para localizar a serie correta utilizando os dados da SE5. 
			*/
			cQryID:=" SELECT "
			cQryID+=" 	F2_SERIE "
			cQryID+=" FROM "+retSqlName("SF1")+" SF1 "
			cQryID+=" WHERE SF2.F2_FILIAL  ='"+xFilial("SF2")+"' "
			cQryID+="   AND SF2.F2_DOC	   ='"+aDetCob[nX,4]+"' "
			cQryID+="   AND SF2.F2_PREFIXO ='"+aDetCob[nX,5]+"' "
			cQryID+="   AND SF2.F2_FORNECE ='"+aDetCob[nX,7]+"' "
			cQryID+="   AND SF2.F2_LOJA    ='"+aDetCob[nX,8]+"' "								
			cQryID+="   AND SF2.D_E_L_E_T_ ='' "
			cQryID	  := ChangeQuery(cQryID)
					
			cAliasSF2 := CriaTrab(Nil,.F.)
			dbUseArea( .T., "TOPCONN", TcGenQry( , , cQryID ), cAliasSF2, .T., .T. )
			dbSelectArea(cAliasSF2)
			If !Empty(aDetCob[nX,11])
				cSer := Substr(aDetCob[nX,11],1,3) 
				cDoc := Substr(aDetCob[nX,11],4,nPos)
				cTipo:= Substr(aDetCob[nX,11],3+nPos+2,3)
			Else
				cSer := ""
				cDoc := ""
				cTipo:= aDetCob[nX,10]
			Endif
			RecLock("CDK",.T.)
			Replace CDK->CDK_FILIAL	With cFilCD
			Replace CDK->CDK_FECHA	With cFecha
			Replace CDK->CDK_GPIETU	With aDetCob[nX,2]
			Replace CDK->CDK_DTCOPG	With aDetCob[nX,1]
			Replace CDK->CDK_RECPAG	With aDetCob[nX,6]
			Replace CDK->CDK_SERECP	With cSer //Tiago Silva, apesar do campo ter o nome "serie" ele � o prefixo do t�tulo NCP usado na baixa. 
			Replace CDK->CDK_DOC	With aDetCob[nX,4]
			Replace CDK->CDK_CLIFOR	With aDetCob[nX,7]
			Replace CDK->CDK_LOJA	With aDetCob[nX,8]
			Replace CDK->CDK_BASE	With aDetCob[nX,3]
			Replace CDK->CDK_RP		With aDetCob[nX,9]
			Replace CDK->CDK_TIPO	With cTipo
			Replace CDK->CDK_DOCBX	With cDoc
			SerieNfId ("CDK",1,"CDK_SERIE",,,,(cAliasSF2)->F2_SERIE)
			CDK->(MsUnLock())
		Endif
		nReg++
		oMeter:Set(nReg)
		dbCloseArea(cAliasSF2)
	Next
	For nX := 1 To Len(aDetPag)
		If aDetPag[nX,3] <> 0
			/* Projeto Chave Unica - Tiago Silva
				A query a seguir � utilizada para localizar a serie correta utilizando os dados da SE5. 
			*/	
			cQryID:=" SELECT "
			cQryID+=" 	F1_SERIE "
			cQryID+=" FROM "+retSqlName("SF1")+" SF1 "
			cQryID+=" WHERE SF1.F1_FILIAL  ='"+xFilial("SF1")+"' "
			cQryID+="   AND SF1.F1_DOC	   ='"+aDetPag[nX,4]+"' "
			cQryID+="   AND SF1.F1_PREFIXO ='"+aDetPag[nX,5]+"' "
			cQryID+="   AND SF1.F1_FORNECE ='"+aDetPag[nX,7]+"' "
			cQryID+="   AND SF1.F1_LOJA    ='"+aDetPag[nX,8]+"' "								
			cQryID+="   AND SF1.D_E_L_E_T_ ='' "
			cQryID	  := ChangeQuery(cQryID)
					
			cAliasSF1 := CriaTrab(Nil,.F.)
			dbUseArea( .T., "TOPCONN", TcGenQry( , , cQryID ), cAliasSF1, .T., .T. )
			dbSelectArea(cAliasSF1)
			If !Empty(aDetPAg[nX,11])
				cSer := Substr(aDetPag[nX,11],1,3) 
				cDoc := Substr(aDetPag[nX,11],4,nPos)
				cTipo:= Substr(aDetPag[nX,11],3+nPos+2,3)
			Else
				cSer := ""
				cDoc := ""
				cTipo:= aDetPag[nX,10]
			Endif
			RecLock("CDK",.T.)
			Replace CDK->CDK_FILIAL	With cFilCD
			Replace CDK->CDK_FECHA	With cFecha
			Replace CDK->CDK_GPIETU	With aDetPag[nX,2]
			Replace CDK->CDK_DTCOPG	With aDetPag[nX,1]
			Replace CDK->CDK_RECPAG	With aDetPag[nX,6]
			Replace CDK->CDK_SERECP	With cSer //Tiago Silva, apesar do campo ter o nome "serie" ele � o prefixo do t�tulo NCP usado na baixa. 
			Replace CDK->CDK_DOC	With aDetPag[nX,4]
			Replace CDK->CDK_CLIFOR	With aDetPag[nX,7]
			Replace CDK->CDK_LOJA	With aDetPag[nX,8]
			Replace CDK->CDK_BASE	With aDetPag[nX,3]
			Replace CDK->CDK_RP		With aDetPag[nX,9]
			Replace CDK->CDK_TIPO	With cTipo
			Replace CDK->CDK_DOCBX	With cDoc
			SerieNfId ("CDK",1,"CDK_SERIE",,,,(cAliasSF1)->F1_SERIE)
			CDK->(MsUnLock())
		Endif
		nReg++
		oMeter:Set(nReg)
		dbCloseArea(cAliasSF1)
	Next
End Transaction
RestArea(aArea)
nReg++
oMeter:Set(nReg)
oMsg:cCaption := " " + STR0026		//"Fechamento encerrado"
oPnl:Refresh()
oMeter:Free()
ProcessMessages()
Return(lRet)

//------------------------------------------------------------------------------------------------------------------------------

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ObtMovVL  �Autor  �ARodriguez          �Fecha � 20/10/2011  ���
�������������������������������������������������������������������������͹��
���Desc.     �Identifica si el pago con cheque fue debitado en la OP o    ���
���          �posteriormente. (rutina igual que en DIOT)                  ���
�������������������������������������������������������������������������͹��
���Uso       � IETU - Mexico                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ObtMovVL(cOrdPago,cCliente,cLoja,cCheque,cTipo)
Local aAreaSE5  := SE5->( GetArea() )
Local nIndSE5   := Retordem("SE5","E5_FILIAL+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIFOR+E5_LOJA+E5_SEQ")
Local nTamPref  := TamSX3("E5_PREFIXO")[1]
Local nTamNum   := TamSX3("E5_NUMERO")[1]
Local nTamParc  := TamSX3("E5_PARCELA")[1]
Local cNumero   := ""
Local lEsDebDif := .F.

Static lA6NumCh := (SA6->(FieldPos("A6_NUMCH")) > 0)

cNumero := PadR( Trim(If(lA6NumCh,cCheque,cOrdPago)) , nTamNum )
SE5->(DBSETORDER(nIndSE5))
IF  SE5->(DBSEEK( XFILIAL("SE5") + SPACE(nTamPref) + cNumero + SPACE(nTamParc) + cTipo + cCliente + cLoja ))
	If  Empty(SE5->E5_MOEDA)
		lEsDebDif:= .T.
	Endif
Endif

SE5->( RestArea(aAreaSE5) )

Return lEsDebDif


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � VldSE2Comp�Autor  � Laura Medina      �Fecha � 07/03/2012  ���
�������������������������������������������������������������������������͹��
���Desc.     � Validar si el PA ya esta compensado para no incluirlo.     ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � IETU - Mexico                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function VldSE2Comp(cOrden,cCliFor,cLoja,nMes,nOpcComp,nValSE2) 
Local aAreaSE2  := SE2->( GetArea())
Local aAreaSEK  := SEK->( GetArea())
Local lCompensa := .F.    
Local nTipDoc   := TamSX3("EK_TIPODOC")[1] 
Local nTipo     := TamSX3("EK_TIPO")[1]     
Local nNumero   := TamSX3("E2_NUM")[1]   


DbSelectArea("SE2")
SE2->(dbSetOrder(1))
DbSelectArea("SEK")
SEK->(dbSetOrder(1))

If  SEK->(DBSEEK(XFILIAL("SEK")+cOrden))
	While !(SEK->(Eof())) .And. SEK->EK_FILIAL == XFILIAL("SEK") .And. SEK->EK_ORDPAGO == cOrden
		If SEK->EK_TIPO==padr("PA",nTipo) .AND. SEK->EK_TIPODOC == padr("PA",nTipDoc)
			If  SE2->(DbSeek(xFilial("SE2")+SEK->EK_PREFIXO+PADR(cOrden,nNumero)+SEK->EK_PARCELA+SEK->EK_TIPO+cCliFor+cLoja))
				If  nOpcComp==1 
					If  EMPTY(SE2->E2_BAIXA) .AND. SE2->E2_SALDO!=0   .Or. (Month(SE2->E2_BAIXA)!=nMes)  //En caso que generen IETU de meses atras
						lCompensa:= .T.       //Compensado totalmente
					Endif  
				Elseif nOpcComp==2            //LEMP(25/01/13):Validar PA compensados parcialmente 
					If  !EMPTY(SE2->E2_BAIXA) .AND. SE2->E2_SALDO!=0
						lCompensa:= .T.       //Compensado parcialmente
						nValSE2  := SE2->E2_SALDO
					Endif 
				Endif
			Endif 
		Endif
		SEK->(DbSkip())
	Enddo
Endif
		
SE2->(RestArea(aAreaSE2))
SEK->(RestArea(aAreaSEK))

Return lCompensa  


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � VldSE1Comp�Autor  � Laura Medina      �Fecha � 07/03/2012  ���
�������������������������������������������������������������������������͹��
���Desc.     � Validar si el RA ya esta compensado para no incluirlo.     ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � IETU - Mexico                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function VldSE1Comp(cRecibo,cSerie,nMes,nOpcComp,nValSE1) 
Local aAreaSE1  := SE1->( GetArea())
Local aAreaSEL  := SEL->( GetArea())
Local lCompensa := .F.    
Local nTipDoc   := TamSX3("EL_TIPODOC")[1] 
Local nTipo     := TamSX3("EL_TIPO")[1]     
Local nNumero   := TamSX3("E1_NUM")[1]   


DbSelectArea("SE1")
SE1->(dbSetOrder(1))
DbSelectArea("SEL")
SEL->(DbSetOrder(Retordem("SEL","EL_FILIAL+EL_SERIE+EL_RECIBO+EL_TIPODOC+EL_PREFIXO+EL_NUMERO+EL_PARCELA+EL_TIPO")))


If  SEL->(DbSeek(XFILIAL("SEL")+ cSerie + cRecibo)) 
	While !(SEL->(Eof())) .And. SEL->EL_FILIAL == XFILIAL("SEL") .And. SEL->EL_RECIBO == cRecibo .And. SEL->EL_SERIE == cSerie 	
		If SEL->EL_TIPO==padr("RA",nTipo) .AND. SEL->EL_TIPODOC == padr("RA",nTipDoc)
			If  SE1->(DbSeek(xFilial("SE1")+SEL->EL_PREFIXO+PADR(cRecibo,nNumero)+SEL->EL_PARCELA+SEL->EL_TIPO))
				If  nOpcComp==1 
					If  EMPTY(SE1->E1_BAIXA) .AND. SE1->E1_SALDO!=0  .Or. (Month(SE1->E1_BAIXA)!=nMes)  //En caso que generen IETU de meses atras
						lCompensa:= .T.       //Compensado totalmente
					Endif    
				Elseif nOpcComp==2            //LEMP(25/01/13):Validar RA compensados parcialmente 
					If  !EMPTY(SE1->E1_BAIXA) .AND. SE1->E1_SALDO!=0
						lCompensa:= .T.       //Compensado Parcialmente
						nValSE1  := SE1->E1_SALDO
					Endif 
				Endif
			Endif 
		Endif
		SEL->(DbSkip())
	Enddo
Endif
		
SE1->(RestArea(aAreaSE1))
SEL->(RestArea(aAreaSEL))

Return lCompensa      



/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � VldRAComp �Autor  � Laura Medina      �Fecha � 16/07/2012  ���
�������������������������������������������������������������������������͹��
���Desc.     � Validar si existe un RA en meses anteriores, si es as�, ya ���
���          � no se debe mostrar el registro compensado.                 ���
�������������������������������������������������������������������������͹��
���Uso       � IETU - Mexico                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/ 
Static Function VldRAComp(cCliente,cLoja,cNumero,cPrefijo,cParcela,cTipo,nMes)  
Local aAreaSE1  := SE1->( GetArea())
Local lCompensa := .F.      

DbSelectArea("SE1")   
SE1->(DbSetOrder(Retordem("SE1","E1_FILIAL+E1_CLIENTE+E1_LOJA+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO")))

If  SE1->(DbSeek(XFILIAL("SE1")+cCliente+cLoja+cPrefijo+cNumero+cParcela+cTipo)) 
	If  strzero(year(SE1->E1_EMISSAO),4)+strzero(Month(SE1->E1_EMISSAO),2) <nMes
		lCompensa:= .T.  //El RA fue generado en IETU de meses anteriores (no debe mostrarse en la compensacion)
	Endif
Endif
		
SE1->(RestArea(aAreaSE1))

Return lCompensa         



/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � VldPAComp �Autor  � Laura Medina      �Fecha � 16/07/2012  ���
�������������������������������������������������������������������������͹��
���Desc.     � Validar si existe un PA en meses anteriores, si es asi, ya ���
���          � no se debe mostrar el registro compensado.                 ���
�������������������������������������������������������������������������͹��
���Uso       � IETU - Mexico                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/ 
Static Function VldPAComp(cProveed,cLoja,cNumero,cPrefijo,cParcela,cTipo,nMes)  
Local aAreaSE2  := SE2->( GetArea())
Local aAreaSEK  := SEK->( GetArea())
Local lCompensa := .F.      

DbSelectArea("SE2")   
SE2->(DbSetOrder(Retordem("SE2","E2_FILIAL+E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO+E2_FORNECE+E2_LOJA")))

If  SE2->(DbSeek(XFILIAL("SE2")+cPrefijo+cNumero ))
	While !(SE2->(Eof())) .And. XFILIAL("SE2")+cPrefijo+cNumero== SE2->E2_FILIAL+SE2->E2_PREFIXO+SE2->E2_NUM
		If  cTipo+cProveed+cLoja == SE2->E2_TIPO+SE2->E2_FORNECE+SE2->E2_LOJA
			If  Month(SE2->E2_EMISSAO)<nMes
				lCompensa:= .T.  //El PA fue  generado en IETU de mes anterior (no debe mostrarse en la compensacion)
			Endif 
		Endif
	SE2->(DbSkip())
	Enddo
Endif  

		
SE2->(RestArea(aAreaSE2))
SEK->(RestArea(aAreaSEK))

Return lCompensa         

