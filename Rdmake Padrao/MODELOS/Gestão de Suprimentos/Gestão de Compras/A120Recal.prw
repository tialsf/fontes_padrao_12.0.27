#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"  
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    �A120Recal � Autor � Eduardo Riera         � Data �18.01.2006���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Funcao de recalculo dos itens do contrato - RIPASA          ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �ExpL1: Sempre .T.                                           ���
�������������������������������������������������������������������������Ĵ��
���Parametros�Nenhum                                                      ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
User Function A120Recal()

Local nSavN := N
Local nX    := 0

lGatilha := If(ValType(lGatilha) == "L",lGatilha,.T.)// .T.=Permite preencher aCols /  .F.=Executando via VldHead()

If lGatilha
	For nX := 1 To Len(aCols)
		N := nX
		A120PrcVis()
	Next nX
EndIf

N := nSavN

Return(.T.)
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    �A120PrcVis� Autor � Eduardo Riera         � Data �18.01.2006���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Funcao de validacao do preco a vista - RIPASA               ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �ExpL1: Sempre .T.                                           ���
�������������������������������������������������������������������������Ĵ��
���Parametros�ExpN1: Preco a Vista (OPC)                                  ���
���          �ExpN2: Taxa do fornecedor (OPC)                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function A120PrcVis(nPrcVis,nTaxaFor)

Local aVencto  := {}
Local nPTotal  := aScan(aHeader,{|x| AllTrim(x[2])=="C7_TOTAL" })
Local nPQuant  := aScan(aHeader,{|x| AllTrim(x[2])=="C7_QUANT" })
Local nPPrcVis := aScan(aHeader,{|x| AllTrim(x[2])=="C7_AVISTA" })
Local nPTxFor  := aScan(aHeader,{|x| AllTrim(x[2])=="C7_TAXAFOR" })
Local nPPreco  := aScan(aHeader,{|x| AllTrim(x[2])=="C7_PRECO" })
Local nPVlDesc := aScan(aHeader,{|x| AllTrim(x[2])=="C7_VLDESC" })
Local nPDesc   := aScan(aHeader,{|x| AllTrim(x[2])=="C7_DESC" })
Local nValFV   := 0
Local nX       := 0
Local nValRetImp	:= 0
Local i			:= 0

If nPPrcVis > 0 .And. nPTxFor > 0

	DEFAULT nPrcVis  := aCols[n][nPPrcVis]
	DEFAULT nTaxaFor := aCols[n][nPTxFor ]

	If nPrcVis > 0
	
		If SFB->FB_JNS == 'J' .and. cPaisLoc == 'COL'
			dbSelectArea("SFC")
			dbSetOrder(2)
			If dbSeek(xFilial("SFC") + SF4->F4_CODIGO + "RV0" )
				nValRetImp 	:= MaFisRet(,"NF_VALIV2")
				If FC_INCDUPL == '1'
					nPrcVis := nPrcVis - nValRetImp
				ElseIf FC_INCDUPL == '2'
					nPrcVis := nPrcVis + nValRetImp
				EndIf
			Elseif dbSeek(xFilial("SFC") + SF4->F4_CODIGO + "RF0" )
				nValRetImp 	:= MaFisRet(,"NF_VALIV4")
				If FC_INCDUPL == '1'
					nPrcVis := nPrcVis - nValRetImp
				ElseIf FC_INCDUPL == '2'
					nPrcVis := nPrcVis + nValRetImp
				EndIf
			Elseif dbSeek(xFilial("SFC") + SF4->F4_CODIGO + "RC0" )
				nValRetImp 	:= MaFisRet(,"NF_VALIV7")
				If FC_INCDUPL == '1'
					nPrcVis := nPrcVis - nValRetImp
				ElseIf FC_INCDUPL == '2'
					nPrcVis := nPrcVis + nValRetImp
				EndIf
			EndIf
		EndIf

		aVencto := Condicao(nPrcVis,cCondicao,0,dDataBase,0)

		For nX := 1 To Len(aVencto)
			nValFV += MaValPres(aVencto[nX][2],aVencto[nX][1],nTaxaFor,2)
		Next nX	

		aCols[n][nPPreco] := NoRound(nValFV,TamSx3("C7_PRECO")[2])

		If MaFisFound("IT",N)
			MaFisRef("IT_PRCUNI","MT120",nValFV)
		EndIf

		If Abs(aCols[n][nPTotal]-NoRound(aCols[n][nPPreco]*aCols[n][nPQuant],TamSx3("C7_TOTAL")[2]))<>0.09
			aCols[n][nPTotal] := NoRound(aCols[n][nPPreco]*aCols[n][nPQuant],TamSx3("C7_TOTAL")[2])
			If MaFisFound("IT",N)
				MaFisRef("IT_VALMERC","MT120",aCols[n][nPTotal])
			EndIf
		EndIf	

		If aCols[n][nPDesc]>0
			aCols[n][nPvlDesc] := NoRound(aCols[n][nPTotal]*aCols[n][nPDesc]/100,TamSx3("C7_VLDESC")[2])
			If MaFisFound("IT",N)
				MaFisRef("IT_DESCONTO","MT120",aCols[n][nPVlDesc])
			EndIf
		Else
			Eval(bRefresh)
		EndIf

	EndIf

EndIf

Return(.T.)


