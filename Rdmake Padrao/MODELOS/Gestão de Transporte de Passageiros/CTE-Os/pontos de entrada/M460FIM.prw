#include 'protheus.ch'
#include 'parmtype.ch'
#Include "Totvs.ch"

 //-----------------------------------------------------------------------------------------------------
/*/{Protheus.doc} M460FIM()

PE para gravar a tabela GZH (CTEOS)no fim da grava��o da nota fiscal


@return Nil

@author 	Fernando Amorim(Cafu) 
@since		22/09/2017
@version 12.1.17

/*/
//------------------------------------------------------------------------------------------------------
User Function M460FIM()
	Local aAreaAT		:= GetArea()
	Local aAreaD2		:= SD2->(GetArea())
	Local aAreaF2		:= SF2->(GetArea())
	
	Local lContinua		:= AllTRIM(UPPER(SF2->F2_ESPECIE)) == "CTEOS"         
			
	If lContinua
		
		G001INIGZH(SF2->F2_DOC,SF2->F2_SERIE,SF2->F2_CLIENTE,SF2->F2_LOJA)
	      
	EndIf 	
	
	RestArea(aAreaAT)
	RestArea(aAreaD2)
	RestArea(aAreaF2)

Return Nil

