#include 'protheus.ch'
#include 'parmtype.ch'
#include 'totvs.ch'
#INCLUDE 'FWMVCDEF.CH'


//-----------------------------------------------------------------------------------------------------
/*/{Protheus.doc} SF2520E()

PE para gravar a tabela GZH (CTEOS)status de cancelado 


@return Nil

@author 	
@since		23/09/2017
@version 12.1.17

/*/

User Function SF2520E()

Local cNota    := SF2->F2_DOC
Local cSerie   := SF2->F2_SERIE
Local cCliente := SF2->F2_CLIENTE
Local cLoja    := SF2->F2_LOJA 
   
_aArea:= GetArea()  
  
GZH->(DbSetOrder(1))
If GZH->(DbSeek(xFilial('GZH')+PadR(cNota,TamSx3('F2_DOC')[1])+PadR(cSerie,TamSx3('F2_SERIE')[1])+cCliente+cLoja)) 
	// muda status  pra cancelado
	G001MStatus("GZH_STATUS",'6')
Endif  
			
RestArea(_aArea)

Return
