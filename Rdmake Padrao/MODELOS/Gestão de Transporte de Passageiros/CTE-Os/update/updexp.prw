#INCLUDE "PROTHEUS.CH"

#DEFINE SIMPLES Char( 39 )
#DEFINE DUPLAS  Char( 34 )

#DEFINE CSSBOTAO	"QPushButton { color: #024670; "+;
"    border-image: url(rpo:fwstd_btn_nml.png) 3 3 3 3 stretch; "+;
"    border-top-width: 3px; "+;
"    border-left-width: 3px; "+;
"    border-right-width: 3px; "+;
"    border-bottom-width: 3px }"+;
"QPushButton:pressed {	color: #FFFFFF; "+;
"    border-image: url(rpo:fwstd_btn_prd.png) 3 3 3 3 stretch; "+;
"    border-top-width: 3px; "+;
"    border-left-width: 3px; "+;
"    border-right-width: 3px; "+;
"    border-bottom-width: 3px }"

//--------------------------------------------------------------------
/*/{Protheus.doc} UPDEXP
Fun��o de update de dicion�rios para compatibiliza��o

@author TOTVS Protheus
@since  10/01/2020
@obs    Gerado por EXPORDIC - V.6.3.0.1 EFS / Upd. V.5.0.0 EFS
@version 1.0
/*/
//--------------------------------------------------------------------
User Function UPDEXP( cEmpAmb, cFilAmb )

Local   aSay      := {}
Local   aButton   := {}
Local   aMarcadas := {}
Local   cTitulo   := "ATUALIZA��O DE DICION�RIOS E TABELAS"
Local   cDesc1    := "Esta rotina tem como fun��o fazer  a atualiza��o  dos dicion�rios do Sistema ( SX?/SIX )"
Local   cDesc2    := "Este processo deve ser executado em modo EXCLUSIVO, ou seja n�o podem haver outros"
Local   cDesc3    := "usu�rios  ou  jobs utilizando  o sistema.  � EXTREMAMENTE recomendav�l  que  se  fa�a um"
Local   cDesc4    := "BACKUP  dos DICION�RIOS  e da  BASE DE DADOS antes desta atualiza��o, para que caso "
Local   cDesc5    := "ocorram eventuais falhas, esse backup possa ser restaurado."
Local   cDesc6    := ""
Local   cDesc7    := ""
Local   cMsg      := ""
Local   lOk       := .F.
Local   lAuto     := ( cEmpAmb <> NIL .or. cFilAmb <> NIL )

Private oMainWnd  := NIL
Private oProcess  := NIL

#IFDEF TOP
    TCInternal( 5, "*OFF" ) // Desliga Refresh no Lock do Top
#ENDIF

__cInterNet := NIL
__lPYME     := .F.

Set Dele On

// Mensagens de Tela Inicial
aAdd( aSay, cDesc1 )
aAdd( aSay, cDesc2 )
aAdd( aSay, cDesc3 )
aAdd( aSay, cDesc4 )
aAdd( aSay, cDesc5 )
//aAdd( aSay, cDesc6 )
//aAdd( aSay, cDesc7 )

// Botoes Tela Inicial
aAdd(  aButton, {  1, .T., { || lOk := .T., FechaBatch() } } )
aAdd(  aButton, {  2, .T., { || lOk := .F., FechaBatch() } } )

If lAuto
	lOk := .T.
Else
	FormBatch(  cTitulo,  aSay,  aButton )
EndIf

If lOk

	If FindFunction( "MPDicInDB" ) .AND. MPDicInDB()
		cMsg := "Este update N�O PODE ser executado neste Ambiente." + CRLF + CRLF + ;
				"Os arquivos de dicion�rios se encontram no Banco de Dados e este update est� preparado " + ;
				"para atualizar apenas ambientes com dicion�rios no formato ISAM (.dbf ou .dtc)."

		If lAuto
			AutoGrLog( Replicate( "-", 128 ) )
			AutoGrLog( Replicate( " ", 128 ) )
			AutoGrLog( "LOG DA ATUALIZA��O DOS DICION�RIOS" )
			AutoGrLog( Replicate( " ", 128 ) )
			AutoGrLog( Replicate( "-", 128 ) )
			AutoGrLog( Replicate( " ", 128 ) )
			AutoGrLog( cMsg )
			ConOut( DToC(Date()) + "|" + Time() + cMsg )
		Else
			MsgInfo( cMsg )
		EndIf

		Return NIL
	EndIf

	If lAuto
		aMarcadas :={{ cEmpAmb, cFilAmb, "" }}
	Else

		aMarcadas := EscEmpresa()
	EndIf

	If !Empty( aMarcadas )
		If lAuto .OR. MsgNoYes( "Confirma a atualiza��o dos dicion�rios ?", cTitulo )
			oProcess := MsNewProcess():New( { | lEnd | lOk := FSTProc( @lEnd, aMarcadas, lAuto ) }, "Atualizando", "Aguarde, atualizando ...", .F. )
			oProcess:Activate()

			If lAuto
				If lOk
					MsgStop( "Atualiza��o Realizada.", "UPDEXP" )
				Else
					MsgStop( "Atualiza��o n�o Realizada.", "UPDEXP" )
				EndIf
				dbCloseAll()
			Else
				If lOk
					Final( "Atualiza��o Realizada." )
				Else
					Final( "Atualiza��o n�o Realizada." )
				EndIf
			EndIf

		Else
			Final( "Atualiza��o n�o Realizada." )

		EndIf

	Else
		Final( "Atualiza��o n�o Realizada." )

	EndIf

EndIf

Return NIL


//--------------------------------------------------------------------
/*/{Protheus.doc} FSTProc
Fun��o de processamento da grava��o dos arquivos

@author TOTVS Protheus
@since  10/01/2020
@obs    Gerado por EXPORDIC - V.6.3.0.1 EFS / Upd. V.5.0.0 EFS
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function FSTProc( lEnd, aMarcadas, lAuto )
Local   aInfo     := {}
Local   aRecnoSM0 := {}
Local   cAux      := ""
Local   cFile     := ""
Local   cFileLog  := ""
Local   cMask     := "Arquivos Texto" + "(*.TXT)|*.txt|"
Local   cTCBuild  := "TCGetBuild"
Local   cTexto    := ""
Local   cTopBuild := ""
Local   lOpen     := .F.
Local   lRet      := .T.
Local   nI        := 0
Local   nPos      := 0
Local   nRecno    := 0
Local   nX        := 0
Local   oDlg      := NIL
Local   oFont     := NIL
Local   oMemo     := NIL

Private aArqUpd   := {}

If ( lOpen := MyOpenSm0(.T.) )

	dbSelectArea( "SM0" )
	dbGoTop()

	While !SM0->( EOF() )
		// S� adiciona no aRecnoSM0 se a empresa for diferente
		If aScan( aRecnoSM0, { |x| x[2] == SM0->M0_CODIGO } ) == 0 ;
		   .AND. aScan( aMarcadas, { |x| x[1] == SM0->M0_CODIGO } ) > 0
			aAdd( aRecnoSM0, { Recno(), SM0->M0_CODIGO } )
		EndIf
		SM0->( dbSkip() )
	End

	SM0->( dbCloseArea() )

	If lOpen

		For nI := 1 To Len( aRecnoSM0 )

			If !( lOpen := MyOpenSm0(.F.) )
				MsgStop( "Atualiza��o da empresa " + aRecnoSM0[nI][2] + " n�o efetuada." )
				Exit
			EndIf

			SM0->( dbGoTo( aRecnoSM0[nI][1] ) )

			RpcSetType( 3 )
			RpcSetEnv( SM0->M0_CODIGO, SM0->M0_CODFIL )

			lMsFinalAuto := .F.
			lMsHelpAuto  := .F.

			AutoGrLog( Replicate( "-", 128 ) )
			AutoGrLog( Replicate( " ", 128 ) )
			AutoGrLog( "LOG DA ATUALIZA��O DOS DICION�RIOS" )
			AutoGrLog( Replicate( " ", 128 ) )
			AutoGrLog( Replicate( "-", 128 ) )
			AutoGrLog( " " )
			AutoGrLog( " Dados Ambiente" )
			AutoGrLog( " --------------------" )
			AutoGrLog( " Empresa / Filial...: " + cEmpAnt + "/" + cFilAnt )
			AutoGrLog( " Nome Empresa.......: " + Capital( AllTrim( GetAdvFVal( "SM0", "M0_NOMECOM", cEmpAnt + cFilAnt, 1, "" ) ) ) )
			AutoGrLog( " Nome Filial........: " + Capital( AllTrim( GetAdvFVal( "SM0", "M0_FILIAL" , cEmpAnt + cFilAnt, 1, "" ) ) ) )
			AutoGrLog( " DataBase...........: " + DtoC( dDataBase ) )
			AutoGrLog( " Data / Hora �nicio.: " + DtoC( Date() )  + " / " + Time() )
			AutoGrLog( " Environment........: " + GetEnvServer()  )
			AutoGrLog( " StartPath..........: " + GetSrvProfString( "StartPath", "" ) )
			AutoGrLog( " RootPath...........: " + GetSrvProfString( "RootPath" , "" ) )
			AutoGrLog( " Vers�o.............: " + GetVersao(.T.) )
			AutoGrLog( " Usu�rio TOTVS .....: " + __cUserId + " " +  cUserName )
			AutoGrLog( " Computer Name......: " + GetComputerName() )

			aInfo   := GetUserInfo()
			If ( nPos    := aScan( aInfo,{ |x,y| x[3] == ThreadId() } ) ) > 0
				AutoGrLog( " " )
				AutoGrLog( " Dados Thread" )
				AutoGrLog( " --------------------" )
				AutoGrLog( " Usu�rio da Rede....: " + aInfo[nPos][1] )
				AutoGrLog( " Esta��o............: " + aInfo[nPos][2] )
				AutoGrLog( " Programa Inicial...: " + aInfo[nPos][5] )
				AutoGrLog( " Environment........: " + aInfo[nPos][6] )
				AutoGrLog( " Conex�o............: " + AllTrim( StrTran( StrTran( aInfo[nPos][7], Chr( 13 ), "" ), Chr( 10 ), "" ) ) )
			EndIf
			AutoGrLog( Replicate( "-", 128 ) )
			AutoGrLog( " " )

			If !lAuto
				AutoGrLog( Replicate( "-", 128 ) )
				AutoGrLog( "Empresa : " + SM0->M0_CODIGO + "/" + SM0->M0_NOME + CRLF )
			EndIf

			oProcess:SetRegua1( 8 )

			//------------------------------------
			// Atualiza o dicion�rio SX2
			//------------------------------------
			oProcess:IncRegua1( "Dicion�rio de arquivos" + " - " + SM0->M0_CODIGO + " " + SM0->M0_NOME + " ..." )
			FSAtuSX2()

			//------------------------------------
			// Atualiza o dicion�rio SX3
			//------------------------------------
			FSAtuSX3()

			//------------------------------------
			// Atualiza o dicion�rio SIX
			//------------------------------------
			oProcess:IncRegua1( "Dicion�rio de �ndices" + " - " + SM0->M0_CODIGO + " " + SM0->M0_NOME + " ..." )
			FSAtuSIX()

			oProcess:IncRegua1( "Dicion�rio de dados" + " - " + SM0->M0_CODIGO + " " + SM0->M0_NOME + " ..." )
			oProcess:IncRegua2( "Atualizando campos/�ndices" )

			// Altera��o f�sica dos arquivos
			__SetX31Mode( .F. )

			If FindFunction(cTCBuild)
				cTopBuild := &cTCBuild.()
			EndIf

			For nX := 1 To Len( aArqUpd )

				If cTopBuild >= "20090811" .AND. TcInternal( 89 ) == "CLOB_SUPPORTED"
					If ( ( aArqUpd[nX] >= "NQ " .AND. aArqUpd[nX] <= "NZZ" ) .OR. ( aArqUpd[nX] >= "O0 " .AND. aArqUpd[nX] <= "NZZ" ) ) .AND.;
						!aArqUpd[nX] $ "NQD,NQF,NQP,NQT"
						TcInternal( 25, "CLOB" )
					EndIf
				EndIf

				If Select( aArqUpd[nX] ) > 0
					dbSelectArea( aArqUpd[nX] )
					dbCloseArea()
				EndIf

				X31UpdTable( aArqUpd[nX] )

				If __GetX31Error()
					Alert( __GetX31Trace() )
					MsgStop( "Ocorreu um erro desconhecido durante a atualiza��o da tabela : " + aArqUpd[nX] + ". Verifique a integridade do dicion�rio e da tabela.", "ATEN��O" )
					AutoGrLog( "Ocorreu um erro desconhecido durante a atualiza��o da estrutura da tabela : " + aArqUpd[nX] )
				EndIf

				If cTopBuild >= "20090811" .AND. TcInternal( 89 ) == "CLOB_SUPPORTED"
					TcInternal( 25, "OFF" )
				EndIf

			Next nX

			//------------------------------------
			// Atualiza o dicion�rio SX6
			//------------------------------------
			oProcess:IncRegua1( "Dicion�rio de par�metros" + " - " + SM0->M0_CODIGO + " " + SM0->M0_NOME + " ..." )
			FSAtuSX6()

			//------------------------------------
			// Atualiza o dicion�rio SX7
			//------------------------------------
			oProcess:IncRegua1( "Dicion�rio de gatilhos" + " - " + SM0->M0_CODIGO + " " + SM0->M0_NOME + " ..." )
			FSAtuSX7()

			//------------------------------------
			// Atualiza o dicion�rio SXA
			//------------------------------------
			oProcess:IncRegua1( "Dicion�rio de pastas" + " - " + SM0->M0_CODIGO + " " + SM0->M0_NOME + " ..." )
			FSAtuSXA()

			//------------------------------------
			// Atualiza o dicion�rio SXB
			//------------------------------------
			oProcess:IncRegua1( "Dicion�rio de consultas padr�o" + " - " + SM0->M0_CODIGO + " " + SM0->M0_NOME + " ..." )
			FSAtuSXB()

			//------------------------------------
			// Atualiza o dicion�rio SX9
			//------------------------------------
			oProcess:IncRegua1( "Dicion�rio de relacionamentos" + " - " + SM0->M0_CODIGO + " " + SM0->M0_NOME + " ..." )
			FSAtuSX9()

			//------------------------------------
			// Atualiza os helps
			//------------------------------------
			oProcess:IncRegua1( "Helps de Campo" + " - " + SM0->M0_CODIGO + " " + SM0->M0_NOME + " ..." )
			FSAtuHlp()

			AutoGrLog( Replicate( "-", 128 ) )
			AutoGrLog( " Data / Hora Final.: " + DtoC( Date() ) + " / " + Time() )
			AutoGrLog( Replicate( "-", 128 ) )

			RpcClearEnv()

		Next nI

		If !lAuto

			cTexto := LeLog()

			Define Font oFont Name "Mono AS" Size 5, 12

			Define MsDialog oDlg Title "Atualiza��o concluida." From 3, 0 to 340, 417 Pixel

			@ 5, 5 Get oMemo Var cTexto Memo Size 200, 145 Of oDlg Pixel
			oMemo:bRClicked := { || AllwaysTrue() }
			oMemo:oFont     := oFont

			Define SButton From 153, 175 Type  1 Action oDlg:End() Enable Of oDlg Pixel // Apaga
			Define SButton From 153, 145 Type 13 Action ( cFile := cGetFile( cMask, "" ), If( cFile == "", .T., ;
			MemoWrite( cFile, cTexto ) ) ) Enable Of oDlg Pixel

			Activate MsDialog oDlg Center

		EndIf

	EndIf

Else

	lRet := .F.

EndIf

Return lRet


//--------------------------------------------------------------------
/*/{Protheus.doc} FSAtuSX2
Fun��o de processamento da grava��o do SX2 - Arquivos

@author TOTVS Protheus
@since  10/01/2020
@obs    Gerado por EXPORDIC - V.6.3.0.1 EFS / Upd. V.5.0.0 EFS
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function FSAtuSX2()
Local aEstrut   := {}
Local aSX2      := {}
Local cAlias    := ""
Local cCpoUpd   := "X2_ROTINA /X2_UNICO  /X2_DISPLAY/X2_SYSOBJ /X2_USROBJ /X2_POSLGT /"
Local cEmpr     := ""
Local cPath     := ""
Local nI        := 0
Local nJ        := 0

AutoGrLog( "�nicio da Atualiza��o" + " SX2" + CRLF )

aEstrut := { "X2_CHAVE"  , "X2_PATH"   , "X2_ARQUIVO", "X2_NOME"   , "X2_NOMESPA", "X2_NOMEENG", "X2_MODO"   , ;
             "X2_TTS"    , "X2_ROTINA" , "X2_PYME"   , "X2_UNICO"  , "X2_DISPLAY", "X2_SYSOBJ" , "X2_USROBJ" , ;
             "X2_POSLGT" , "X2_CLOB"   , "X2_AUTREC" , "X2_MODOEMP", "X2_MODOUN" , "X2_MODULO" }


dbSelectArea( "SX2" )
SX2->( dbSetOrder( 1 ) )
SX2->( dbGoTop() )
cPath := SX2->X2_PATH
cPath := IIf( Right( AllTrim( cPath ), 1 ) <> "\", PadR( AllTrim( cPath ) + "\", Len( cPath ) ), cPath )
cEmpr := Substr( SX2->X2_ARQUIVO, 4 )

//
// Tabela G53
//
aAdd( aSX2, { ;
	'G53'																	, ; //X2_CHAVE
	cPath																	, ; //X2_PATH
	'G53'+cEmpr																, ; //X2_ARQUIVO
	'CT-e OS Campos - Carta de corr'										, ; //X2_NOME
	'CT-e OS Campos - Carta de Corr'										, ; //X2_NOMESPA
	'CT-e SO Fields - Adj Letter'											, ; //X2_NOMEENG
	'C'																		, ; //X2_MODO
	''																		, ; //X2_TTS
	''																		, ; //X2_ROTINA
	'S'																		, ; //X2_PYME
	'G53_FILIAL+G53_GRUPO+G53_CAMPO'										, ; //X2_UNICO
	''																		, ; //X2_DISPLAY
	''																		, ; //X2_SYSOBJ
	''																		, ; //X2_USROBJ
	'1'																		, ; //X2_POSLGT
	'2'																		, ; //X2_CLOB
	'2'																		, ; //X2_AUTREC
	'C'																		, ; //X2_MODOEMP
	'C'																		, ; //X2_MODOUN
	88																		} ) //X2_MODULO

//
// Tabela GQ1
//
aAdd( aSX2, { ;
	'GQ1'																	, ; //X2_CHAVE
	cPath																	, ; //X2_PATH
	'GQ1'+cEmpr																, ; //X2_ARQUIVO
	'Tipos de Venda'														, ; //X2_NOME
	'Tipos de Venta'														, ; //X2_NOMESPA
	'Types of Sales'														, ; //X2_NOMEENG
	'C'																		, ; //X2_MODO
	''																		, ; //X2_TTS
	''																		, ; //X2_ROTINA
	'N'																		, ; //X2_PYME
	'GQ1_FILIAL+GQ1_CODIGO+GQ1_SERIE+GQ1_SUBSER+GQ1_NUMDOC'					, ; //X2_UNICO
	''																		, ; //X2_DISPLAY
	''																		, ; //X2_SYSOBJ
	''																		, ; //X2_USROBJ
	'1'																		, ; //X2_POSLGT
	'2'																		, ; //X2_CLOB
	'2'																		, ; //X2_AUTREC
	'E'																		, ; //X2_MODOEMP
	'E'																		, ; //X2_MODOUN
	88																		} ) //X2_MODULO

//
// Tabela GQ2
//
aAdd( aSX2, { ;
	'GQ2'																	, ; //X2_CHAVE
	cPath																	, ; //X2_PATH
	'GQ2'+cEmpr																, ; //X2_ARQUIVO
	'Lote de Taxas'															, ; //X2_NOME
	'Lote de tasas'															, ; //X2_NOMESPA
	'Rate batch'															, ; //X2_NOMEENG
	'C'																		, ; //X2_MODO
	''																		, ; //X2_TTS
	''																		, ; //X2_ROTINA
	'N'																		, ; //X2_PYME
	'GQ2_FILIAL+GQ2_CODIGO'													, ; //X2_UNICO
	''																		, ; //X2_DISPLAY
	''																		, ; //X2_SYSOBJ
	''																		, ; //X2_USROBJ
	'1'																		, ; //X2_POSLGT
	'2'																		, ; //X2_CLOB
	'2'																		, ; //X2_AUTREC
	'E'																		, ; //X2_MODOEMP
	'E'																		, ; //X2_MODOUN
	88																		} ) //X2_MODULO

//
// Tabela GZH
//
aAdd( aSX2, { ;
	'GZH'																	, ; //X2_CHAVE
	cPath																	, ; //X2_PATH
	'GZH'+cEmpr																, ; //X2_ARQUIVO
	'Controle CTE-OS'														, ; //X2_NOME
	'Control CTE-OS'														, ; //X2_NOMESPA
	'CTE-OS Control'														, ; //X2_NOMEENG
	'E'																		, ; //X2_MODO
	''																		, ; //X2_TTS
	''																		, ; //X2_ROTINA
	'S'																		, ; //X2_PYME
	'GZH_FILIAL+GZH_NOTA+GZH_SERIE+GZH_CLIENT+GZH_LOJA'						, ; //X2_UNICO
	''																		, ; //X2_DISPLAY
	''																		, ; //X2_SYSOBJ
	''																		, ; //X2_USROBJ
	'1'																		, ; //X2_POSLGT
	'2'																		, ; //X2_CLOB
	'2'																		, ; //X2_AUTREC
	'E'																		, ; //X2_MODOEMP
	'E'																		, ; //X2_MODOUN
	88																		} ) //X2_MODULO

//
// Atualizando dicion�rio
//
oProcess:SetRegua2( Len( aSX2 ) )

dbSelectArea( "SX2" )
dbSetOrder( 1 )

For nI := 1 To Len( aSX2 )

	oProcess:IncRegua2( "Atualizando Arquivos (SX2)..." )

	If !SX2->( dbSeek( aSX2[nI][1] ) )

		If !( aSX2[nI][1] $ cAlias )
			cAlias += aSX2[nI][1] + "/"
			AutoGrLog( "Foi inclu�da a tabela " + aSX2[nI][1] )
		EndIf

		RecLock( "SX2", .T. )
		For nJ := 1 To Len( aSX2[nI] )
			If FieldPos( aEstrut[nJ] ) > 0
				If AllTrim( aEstrut[nJ] ) == "X2_ARQUIVO"
					FieldPut( FieldPos( aEstrut[nJ] ), SubStr( aSX2[nI][nJ], 1, 3 ) + cEmpAnt +  "0" )
				Else
					FieldPut( FieldPos( aEstrut[nJ] ), aSX2[nI][nJ] )
				EndIf
			EndIf
		Next nJ
		MsUnLock()

	Else

		If  !( StrTran( Upper( AllTrim( SX2->X2_UNICO ) ), " ", "" ) == StrTran( Upper( AllTrim( aSX2[nI][12]  ) ), " ", "" ) )
			RecLock( "SX2", .F. )
			SX2->X2_UNICO := aSX2[nI][12]
			MsUnlock()

			If MSFILE( RetSqlName( aSX2[nI][1] ),RetSqlName( aSX2[nI][1] ) + "_UNQ"  )
				TcInternal( 60, RetSqlName( aSX2[nI][1] ) + "|" + RetSqlName( aSX2[nI][1] ) + "_UNQ" )
			EndIf

			AutoGrLog( "Foi alterada a chave �nica da tabela " + aSX2[nI][1] )
		EndIf

		RecLock( "SX2", .F. )
		For nJ := 1 To Len( aSX2[nI] )
			If FieldPos( aEstrut[nJ] ) > 0
				If PadR( aEstrut[nJ], 10 ) $ cCpoUpd
					FieldPut( FieldPos( aEstrut[nJ] ), aSX2[nI][nJ] )
				EndIf

			EndIf
		Next nJ
		MsUnLock()

	EndIf

Next nI

AutoGrLog( CRLF + "Final da Atualiza��o" + " SX2" + CRLF + Replicate( "-", 128 ) + CRLF )

Return NIL


//--------------------------------------------------------------------
/*/{Protheus.doc} FSAtuSX3
Fun��o de processamento da grava��o do SX3 - Campos

@author TOTVS Protheus
@since  10/01/2020
@obs    Gerado por EXPORDIC - V.6.3.0.1 EFS / Upd. V.5.0.0 EFS
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function FSAtuSX3()
Local aEstrut   := {}
Local aSX3      := {}
Local cAlias    := ""
Local cAliasAtu := ""
Local cMsg      := ""
Local cSeqAtu   := ""
Local cX3Campo  := ""
Local cX3Dado   := ""
Local lTodosNao := .F.
Local lTodosSim := .F.
Local nI        := 0
Local nJ        := 0
Local nOpcA     := 0
Local nPosArq   := 0
Local nPosCpo   := 0
Local nPosOrd   := 0
Local nPosSXG   := 0
Local nPosTam   := 0
Local nPosVld   := 0
Local nSeqAtu   := 0
Local nTamSeek  := Len( SX3->X3_CAMPO )

AutoGrLog( "�nicio da Atualiza��o" + " SX3" + CRLF )

aEstrut := { { "X3_ARQUIVO", 0 }, { "X3_ORDEM"  , 0 }, { "X3_CAMPO"  , 0 }, { "X3_TIPO"   , 0 }, { "X3_TAMANHO", 0 }, { "X3_DECIMAL", 0 }, { "X3_TITULO" , 0 }, ;
             { "X3_TITSPA" , 0 }, { "X3_TITENG" , 0 }, { "X3_DESCRIC", 0 }, { "X3_DESCSPA", 0 }, { "X3_DESCENG", 0 }, { "X3_PICTURE", 0 }, { "X3_VALID"  , 0 }, ;
             { "X3_USADO"  , 0 }, { "X3_RELACAO", 0 }, { "X3_F3"     , 0 }, { "X3_NIVEL"  , 0 }, { "X3_RESERV" , 0 }, { "X3_CHECK"  , 0 }, { "X3_TRIGGER", 0 }, ;
             { "X3_PROPRI" , 0 }, { "X3_BROWSE" , 0 }, { "X3_VISUAL" , 0 }, { "X3_CONTEXT", 0 }, { "X3_OBRIGAT", 0 }, { "X3_VLDUSER", 0 }, { "X3_CBOX"   , 0 }, ;
             { "X3_CBOXSPA", 0 }, { "X3_CBOXENG", 0 }, { "X3_PICTVAR", 0 }, { "X3_WHEN"   , 0 }, { "X3_INIBRW" , 0 }, { "X3_GRPSXG" , 0 }, { "X3_FOLDER" , 0 }, ;
             { "X3_CONDSQL", 0 }, { "X3_CHKSQL" , 0 }, { "X3_IDXSRV" , 0 }, { "X3_ORTOGRA", 0 }, { "X3_TELA"   , 0 }, { "X3_POSLGT" , 0 }, { "X3_IDXFLD" , 0 }, ;
             { "X3_AGRUP"  , 0 }, { "X3_MODAL"  , 0 }, { "X3_PYME"   , 0 } }

aEval( aEstrut, { |x| x[2] := SX3->( FieldPos( x[1] ) ) } )

//
// --- ATEN��O ---
// Coloque .F. na 2a. posi��o de cada elemento do array, para os dados do SX3
// que n�o ser�o atualizados quando o campo j� existir.
//

//
// Campos Tabela G53
//
aAdd( aSX3, { ;
	{ 'G53'																	, .F. }, ; //X3_ARQUIVO
	{ '01'																	, .F. }, ; //X3_ORDEM
	{ 'G53_FILIAL'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 8																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Filial'																, .F. }, ; //X3_TITULO
	{ 'Sucursal'															, .F. }, ; //X3_TITSPA
	{ 'Branch'																, .F. }, ; //X3_TITENG
	{ 'Filial do sistema'													, .F. }, ; //X3_DESCRIC
	{ 'Sucursal del sistema'												, .F. }, ; //X3_DESCSPA
	{ 'System branch'														, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(128) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ ''																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ '033'																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'S'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '1'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'G53'																	, .F. }, ; //X3_ARQUIVO
	{ '02'																	, .F. }, ; //X3_ORDEM
	{ 'G53_GRUPO'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 20																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Grupo'																, .F. }, ; //X3_TITULO
	{ 'Grupo'																, .F. }, ; //X3_TITSPA
	{ 'Group'																, .F. }, ; //X3_TITENG
	{ 'Grupo a ser corrigido'												, .F. }, ; //X3_DESCRIC
	{ 'Grupo que se corregir�'												, .F. }, ; //X3_DESCSPA
	{ 'Group to be adjusted'												, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(133) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'S'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '1'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'G53'																	, .F. }, ; //X3_ARQUIVO
	{ '03'																	, .F. }, ; //X3_ORDEM
	{ 'G53_CAMPO'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 20																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Campo'																, .F. }, ; //X3_TITULO
	{ 'Campo'																, .F. }, ; //X3_TITSPA
	{ 'Field'																, .F. }, ; //X3_TITENG
	{ 'Campo a ser corrigido'												, .F. }, ; //X3_DESCRIC
	{ 'Campo que se corregir�'												, .F. }, ; //X3_DESCSPA
	{ 'Field to adjust'														, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(133) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'S'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '1'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'G53'																	, .F. }, ; //X3_ARQUIVO
	{ '04'																	, .F. }, ; //X3_ORDEM
	{ 'G53_DESCCP'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 50																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Descri��o'															, .F. }, ; //X3_TITULO
	{ 'Descripci�n'															, .F. }, ; //X3_TITSPA
	{ 'Description'															, .F. }, ; //X3_TITENG
	{ 'Descri�ao do campo'													, .F. }, ; //X3_DESCRIC
	{ 'Descripci�n del campo'												, .F. }, ; //X3_DESCSPA
	{ 'Field Description'													, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '1'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

//
// Campos Tabela GQ1
//
aAdd( aSX3, { ;
	{ 'GQ1'																	, .F. }, ; //X3_ARQUIVO
	{ '01'																	, .F. }, ; //X3_ORDEM
	{ 'GQ1_FILIAL'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 8																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Filial'																, .F. }, ; //X3_TITULO
	{ 'Sucursal'															, .F. }, ; //X3_TITSPA
	{ 'Branch'																, .F. }, ; //X3_TITENG
	{ 'Filial do sistema'													, .F. }, ; //X3_DESCRIC
	{ 'Sucursal del sistema'												, .F. }, ; //X3_DESCSPA
	{ 'System Branch'														, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ '033'																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ1'																	, .F. }, ; //X3_ARQUIVO
	{ '02'																	, .F. }, ; //X3_ORDEM
	{ 'GQ1_CODIGO'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 6																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'C�digo'																, .F. }, ; //X3_TITULO
	{ 'C�digo'																, .F. }, ; //X3_TITSPA
	{ 'Code'																, .F. }, ; //X3_TITENG
	{ 'C�digo do Tp. Venda'													, .F. }, ; //X3_DESCRIC
	{ 'C�digo de Tp. Venta'													, .F. }, ; //X3_DESCSPA
	{ 'Sales Type Code'														, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ "GETSXENUM('GQ1','GQ1_CODIGO')"										, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(133) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '1'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ1'																	, .F. }, ; //X3_ARQUIVO
	{ '03'																	, .F. }, ; //X3_ORDEM
	{ 'GQ1_DESCRI'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 30																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Descri��o'															, .F. }, ; //X3_TITULO
	{ 'Descripci�n'															, .F. }, ; //X3_TITSPA
	{ 'Description'															, .F. }, ; //X3_TITENG
	{ 'Desc. Tp. Venda'														, .F. }, ; //X3_DESCRIC
	{ 'Desc. Tp. Venta'														, .F. }, ; //X3_DESCSPA
	{ 'Sales Rep Description'												, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(133) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '1'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ1'																	, .F. }, ; //X3_ARQUIVO
	{ '04'																	, .F. }, ; //X3_ORDEM
	{ 'GQ1_TIPDOC'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 6																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Tp Documento'														, .F. }, ; //X3_TITULO
	{ 'Tp Documento'														, .F. }, ; //X3_TITSPA
	{ 'Document Tp'															, .F. }, ; //X3_TITENG
	{ 'Cod.Tipo Documento'													, .F. }, ; //X3_DESCRIC
	{ 'C�d.Tipo Documento'													, .F. }, ; //X3_DESCSPA
	{ 'Document Type Code'													, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ "ExistCpo('GYA')"														, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ 'GYA'																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ1'																	, .F. }, ; //X3_ARQUIVO
	{ '05'																	, .F. }, ; //X3_ORDEM
	{ 'GQ1_DTPDOC'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 30																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Desc. Tp. Do'														, .F. }, ; //X3_TITULO
	{ 'Desc. Tp. Do'														, .F. }, ; //X3_TITSPA
	{ 'Doc Tp Descr'														, .F. }, ; //X3_TITENG
	{ 'Desc. Tp. Docto'														, .F. }, ; //X3_DESCRIC
	{ 'Desc. Tp. Docto'														, .F. }, ; //X3_DESCSPA
	{ 'Document Type Description'											, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ "IF(!INCLUI,POSICIONE('GYA',1,XFILIAL('GYA')+GQ1->GQ1_TIPDOC,'GYA_DESCRI'),'')", .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ ''																	, .F. }, ; //X3_BROWSE
	{ 'V'																	, .F. }, ; //X3_VISUAL
	{ 'V'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ "POSICIONE('GYA',1,XFILIAL('GYA')+GQ1->GQ1_TIPDOC,'GYA_DESCRI')"		, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '1'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ1'																	, .F. }, ; //X3_ARQUIVO
	{ '06'																	, .F. }, ; //X3_ORDEM
	{ 'GQ1_MSBLQL'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 1																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Bloqueado?'															, .F. }, ; //X3_TITULO
	{ '�Bloqueado?'															, .F. }, ; //X3_TITSPA
	{ 'Blocked?'															, .F. }, ; //X3_TITENG
	{ 'Bloqueado?'															, .F. }, ; //X3_DESCRIC
	{ '�Bloqueado?'															, .F. }, ; //X3_DESCSPA
	{ 'Blocked?'															, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(133) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ '1=N�o; 2=Sim'														, .F. }, ; //X3_CBOX
	{ '1=No; 2=S�'															, .F. }, ; //X3_CBOXSPA
	{ '1=No; 2=Yes'															, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '1'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ1'																	, .F. }, ; //X3_ARQUIVO
	{ '07'																	, .F. }, ; //X3_ORDEM
	{ 'GQ1_SERIE'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 3																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'GQ1_SERIE'															, .F. }, ; //X3_TITULO
	{ 'GQ1_SERIE'															, .F. }, ; //X3_TITSPA
	{ 'GQ1_SERIE'															, .F. }, ; //X3_TITENG
	{ 'Serie da tabela'														, .F. }, ; //X3_DESCRIC
	{ 'Serie de la tabla'													, .F. }, ; //X3_DESCSPA
	{ 'Table series'														, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ1'																	, .F. }, ; //X3_ARQUIVO
	{ '08'																	, .F. }, ; //X3_ORDEM
	{ 'GQ1_SUBSER'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 3																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Sub S�rie'															, .F. }, ; //X3_TITULO
	{ 'Subserie'															, .F. }, ; //X3_TITSPA
	{ 'Subseries'															, .F. }, ; //X3_TITENG
	{ 'Sub S�rie   da tabela'												, .F. }, ; //X3_DESCRIC
	{ 'Subserie de la tabla'												, .F. }, ; //X3_DESCSPA
	{ 'Table subseries'														, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ1'																	, .F. }, ; //X3_ARQUIVO
	{ '09'																	, .F. }, ; //X3_ORDEM
	{ 'GQ1_NUMCOM'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 3																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Complemento'															, .F. }, ; //X3_TITULO
	{ 'Complemento'															, .F. }, ; //X3_TITSPA
	{ 'Complement'															, .F. }, ; //X3_TITENG
	{ 'Complemento'															, .F. }, ; //X3_DESCRIC
	{ 'Complemento'															, .F. }, ; //X3_DESCSPA
	{ 'Complement'															, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ1'																	, .F. }, ; //X3_ARQUIVO
	{ '10'																	, .F. }, ; //X3_ORDEM
	{ 'GQ1_NUMDOC'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 6																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Num. Docto.'															, .F. }, ; //X3_TITULO
	{ 'N� Docto.'															, .F. }, ; //X3_TITSPA
	{ 'Doc. No.'															, .F. }, ; //X3_TITENG
	{ 'Num. Docto.'															, .F. }, ; //X3_DESCRIC
	{ 'N� Docto.'															, .F. }, ; //X3_DESCSPA
	{ 'Doc. No.'															, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ1'																	, .F. }, ; //X3_ARQUIVO
	{ '11'																	, .F. }, ; //X3_ORDEM
	{ 'GQ1_VALOR'															, .F. }, ; //X3_CAMPO
	{ 'N'																	, .F. }, ; //X3_TIPO
	{ 9																		, .F. }, ; //X3_TAMANHO
	{ 2																		, .F. }, ; //X3_DECIMAL
	{ 'Valor'																, .F. }, ; //X3_TITULO
	{ 'Valor'																, .F. }, ; //X3_TITSPA
	{ 'Value'																, .F. }, ; //X3_TITENG
	{ 'Valor'																, .F. }, ; //X3_DESCRIC
	{ 'Valor'																, .F. }, ; //X3_DESCSPA
	{ 'Value'																, .F. }, ; //X3_DESCENG
	{ '@E 999,999.99'														, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ1'																	, .F. }, ; //X3_ARQUIVO
	{ '12'																	, .F. }, ; //X3_ORDEM
	{ 'GQ1_CODGQ2'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 6																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'C�digo Fat.'															, .F. }, ; //X3_TITULO
	{ 'C�digo Fact.'														, .F. }, ; //X3_TITSPA
	{ 'Inv Code'															, .F. }, ; //X3_TITENG
	{ 'C�digo Fat.'															, .F. }, ; //X3_DESCRIC
	{ 'C�digo Fact.'														, .F. }, ; //X3_DESCSPA
	{ 'Inv Code'															, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(133) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '1'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ1'																	, .F. }, ; //X3_ARQUIVO
	{ '13'																	, .F. }, ; //X3_ORDEM
	{ 'GQ1_DTEMI'															, .F. }, ; //X3_CAMPO
	{ 'D'																	, .F. }, ; //X3_TIPO
	{ 8																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Data Emissao'														, .F. }, ; //X3_TITULO
	{ 'Fch Emisi�n'															, .F. }, ; //X3_TITSPA
	{ 'Issue Date'															, .F. }, ; //X3_TITENG
	{ 'Data Emissao'														, .F. }, ; //X3_DESCRIC
	{ 'Fch Emisi�n'															, .F. }, ; //X3_DESCSPA
	{ 'Issue Date'															, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

//
// Campos Tabela GQ2
//
aAdd( aSX3, { ;
	{ 'GQ2'																	, .F. }, ; //X3_ARQUIVO
	{ '01'																	, .F. }, ; //X3_ORDEM
	{ 'GQ2_FILIAL'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 8																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Filial'																, .F. }, ; //X3_TITULO
	{ 'Sucursal'															, .F. }, ; //X3_TITSPA
	{ 'Branch'																, .F. }, ; //X3_TITENG
	{ 'Filial'																, .F. }, ; //X3_DESCRIC
	{ 'Sucursal'															, .F. }, ; //X3_DESCSPA
	{ 'Branch'																, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ ''																	, .F. }, ; //X3_VISUAL
	{ ''																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ '033'																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ2'																	, .F. }, ; //X3_ARQUIVO
	{ '02'																	, .F. }, ; //X3_ORDEM
	{ 'GQ2_CODIGO'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 6																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Codigo'																, .F. }, ; //X3_TITULO
	{ 'C�digo'																, .F. }, ; //X3_TITSPA
	{ 'Code'																, .F. }, ; //X3_TITENG
	{ 'Codigo'																, .F. }, ; //X3_DESCRIC
	{ 'C�digo'																, .F. }, ; //X3_DESCSPA
	{ 'Code'																, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ "GetSxeNum('GQ2','GQ2_CODIGO')"										, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(133) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '1'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ2'																	, .F. }, ; //X3_ARQUIVO
	{ '03'																	, .F. }, ; //X3_ORDEM
	{ 'GQ2_AGENCI'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 6																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Agencia'																, .F. }, ; //X3_TITULO
	{ 'Agencia'																, .F. }, ; //X3_TITSPA
	{ 'Branch'																, .F. }, ; //X3_TITENG
	{ 'Agencia'																, .F. }, ; //X3_DESCRIC
	{ 'Agencia'																, .F. }, ; //X3_DESCSPA
	{ 'Branch'																, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ2'																	, .F. }, ; //X3_ARQUIVO
	{ '04'																	, .F. }, ; //X3_ORDEM
	{ 'GQ2_TIPDOC'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 6																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Tipo Docto.'															, .F. }, ; //X3_TITULO
	{ 'Tipo Docto.'															, .F. }, ; //X3_TITSPA
	{ 'Doc. Type'															, .F. }, ; //X3_TITENG
	{ 'Tipo Docto.'															, .F. }, ; //X3_DESCRIC
	{ 'Tipo Docto.'															, .F. }, ; //X3_DESCSPA
	{ 'Doc. Type'															, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ 'GYA'																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ2'																	, .F. }, ; //X3_ARQUIVO
	{ '05'																	, .F. }, ; //X3_ORDEM
	{ 'GQ2_DESAGE'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 30																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Descr. Ag.'															, .F. }, ; //X3_TITULO
	{ 'Descr. Ag.'															, .F. }, ; //X3_TITSPA
	{ 'Ag Descr'															, .F. }, ; //X3_TITENG
	{ 'Descr. Ag.'															, .F. }, ; //X3_DESCRIC
	{ 'Descr. Ag.'															, .F. }, ; //X3_DESCSPA
	{ 'Ag Descr'															, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ 'IF(!INCLUI, POSICIONE("GI6", 1, XFILIAL("GI6") + GQ2_AGENCI , "GI6_DESCRI"), "")', .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'V'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ 'POSICIONE("GI6", 1, XFILIAL("GI6") + GQ2_AGENCI , "GI6_DESCRI")'		, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ2'																	, .F. }, ; //X3_ARQUIVO
	{ '06'																	, .F. }, ; //X3_ORDEM
	{ 'GQ2_DSCDOC'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 30																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Descr. Doc.'															, .F. }, ; //X3_TITULO
	{ 'Descr. Doc.'															, .F. }, ; //X3_TITSPA
	{ 'Doc. Descr.'															, .F. }, ; //X3_TITENG
	{ 'Descr. Doc.'															, .F. }, ; //X3_DESCRIC
	{ 'Descr. Doc.'															, .F. }, ; //X3_DESCSPA
	{ 'Doc. Descr.'															, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ 'IF(!INCLUI, POSICIONE("GYA", 1, XFILIAL("GYA") + GQ2_TIPDOC , "GYA_DESCRI"), "")', .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'V'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ 'POSICIONE("GYA", 1, XFILIAL("GYA") + GQ2_TIPDOC , "GYA_DESCRI")'		, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ2'																	, .F. }, ; //X3_ARQUIVO
	{ '07'																	, .F. }, ; //X3_ORDEM
	{ 'GQ2_DTINI'															, .F. }, ; //X3_CAMPO
	{ 'D'																	, .F. }, ; //X3_TIPO
	{ 8																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Data Inicial'														, .F. }, ; //X3_TITULO
	{ 'Fecha inicia'														, .F. }, ; //X3_TITSPA
	{ 'Start Date'															, .F. }, ; //X3_TITENG
	{ 'Data Inicial'														, .F. }, ; //X3_DESCRIC
	{ 'Fecha inicial'														, .F. }, ; //X3_DESCSPA
	{ 'Start Date'															, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ 'NaoVazio()'															, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ2'																	, .F. }, ; //X3_ARQUIVO
	{ '08'																	, .F. }, ; //X3_ORDEM
	{ 'GQ2_DTFIM'															, .F. }, ; //X3_CAMPO
	{ 'D'																	, .F. }, ; //X3_TIPO
	{ 8																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Data Final'															, .F. }, ; //X3_TITULO
	{ 'Fecha final'															, .F. }, ; //X3_TITSPA
	{ 'End Date'															, .F. }, ; //X3_TITENG
	{ 'Data Final'															, .F. }, ; //X3_DESCRIC
	{ 'Fecha final'															, .F. }, ; //X3_DESCSPA
	{ 'End Date'															, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ "NaoVazio() .And. (FwFldGet('GQ2_DTFIM') >=  FwFldGet('GQ2_DTINI'))"		, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ2'																	, .F. }, ; //X3_ARQUIVO
	{ '09'																	, .F. }, ; //X3_ORDEM
	{ 'GQ2_DTEMI'															, .F. }, ; //X3_CAMPO
	{ 'D'																	, .F. }, ; //X3_TIPO
	{ 8																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Data Emissao'														, .F. }, ; //X3_TITULO
	{ 'Fch Emisi�n'															, .F. }, ; //X3_TITSPA
	{ 'Issue Date'															, .F. }, ; //X3_TITENG
	{ 'Data Emissao'														, .F. }, ; //X3_DESCRIC
	{ 'Fch Emisi�n'															, .F. }, ; //X3_DESCSPA
	{ 'Issue Date'															, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ "NaoVazio() .And. (FwFldGet('GQ2_DTEMI') >=  FwFldGet('GQ2_DTFIM'))"		, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ2'																	, .F. }, ; //X3_ARQUIVO
	{ '10'																	, .F. }, ; //X3_ORDEM
	{ 'GQ2_VALTOT'															, .F. }, ; //X3_CAMPO
	{ 'N'																	, .F. }, ; //X3_TIPO
	{ 9																		, .F. }, ; //X3_TAMANHO
	{ 2																		, .F. }, ; //X3_DECIMAL
	{ 'Valor Total'															, .F. }, ; //X3_TITULO
	{ 'Valor total'															, .F. }, ; //X3_TITSPA
	{ 'Total Value'															, .F. }, ; //X3_TITENG
	{ 'Valor Total'															, .F. }, ; //X3_DESCRIC
	{ 'Valor total'															, .F. }, ; //X3_DESCSPA
	{ 'Total Value'															, .F. }, ; //X3_DESCENG
	{ '@E 999,999.99'														, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ2'																	, .F. }, ; //X3_ARQUIVO
	{ '11'																	, .F. }, ; //X3_ORDEM
	{ 'GQ2_STATUS'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 1																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Status'																, .F. }, ; //X3_TITULO
	{ 'Estatus'																, .F. }, ; //X3_TITSPA
	{ 'Status'																, .F. }, ; //X3_TITENG
	{ 'Status'																, .F. }, ; //X3_DESCRIC
	{ 'Estatus'																, .F. }, ; //X3_DESCSPA
	{ 'Status'																, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ2'																	, .F. }, ; //X3_ARQUIVO
	{ '12'																	, .F. }, ; //X3_ORDEM
	{ 'GQ2_FILNF'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 7																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Filial NF'															, .F. }, ; //X3_TITULO
	{ 'Sucursal Fac'														, .F. }, ; //X3_TITSPA
	{ 'Inv Branch'															, .F. }, ; //X3_TITENG
	{ 'Filial NF'															, .F. }, ; //X3_DESCRIC
	{ 'Sucursal Fact'														, .F. }, ; //X3_DESCSPA
	{ 'Inv Branch'															, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ2'																	, .F. }, ; //X3_ARQUIVO
	{ '13'																	, .F. }, ; //X3_ORDEM
	{ 'GQ2_NOTA'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 9																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Nota Fiscal'															, .F. }, ; //X3_TITULO
	{ 'Factura'																, .F. }, ; //X3_TITSPA
	{ 'Invoice'																, .F. }, ; //X3_TITENG
	{ 'Nota Fiscal'															, .F. }, ; //X3_DESCRIC
	{ 'Factura'																, .F. }, ; //X3_DESCSPA
	{ 'Invoice'																, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ2'																	, .F. }, ; //X3_ARQUIVO
	{ '14'																	, .F. }, ; //X3_ORDEM
	{ 'GQ2_SERIE'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 3																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'S�rie'																, .F. }, ; //X3_TITULO
	{ 'Serie'																, .F. }, ; //X3_TITSPA
	{ 'Series'																, .F. }, ; //X3_TITENG
	{ 'S�rie'																, .F. }, ; //X3_DESCRIC
	{ 'Serie'																, .F. }, ; //X3_DESCSPA
	{ 'Series'																, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ2'																	, .F. }, ; //X3_ARQUIVO
	{ '15'																	, .F. }, ; //X3_ORDEM
	{ 'GQ2_CLIENT'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 6																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Cliente Nota'														, .F. }, ; //X3_TITULO
	{ 'Cliente Fact'														, .F. }, ; //X3_TITSPA
	{ 'Invoice Cust'														, .F. }, ; //X3_TITENG
	{ 'Cliente Nota'														, .F. }, ; //X3_DESCRIC
	{ 'Cliente Fact'														, .F. }, ; //X3_DESCSPA
	{ 'Invoice Customer'													, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ 'SA1'																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ '001'																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GQ2'																	, .F. }, ; //X3_ARQUIVO
	{ '16'																	, .F. }, ; //X3_ORDEM
	{ 'GQ2_LOJA'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 2																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Loja'																, .F. }, ; //X3_TITULO
	{ 'Tienda'																, .F. }, ; //X3_TITSPA
	{ 'Store'																, .F. }, ; //X3_TITENG
	{ 'Loja'																, .F. }, ; //X3_DESCRIC
	{ 'Tienda'																, .F. }, ; //X3_DESCSPA
	{ 'Store'																, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ '002'																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

//
// Campos Tabela GZH
//
aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '01'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_FILIAL'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 8																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Filial'																, .F. }, ; //X3_TITULO
	{ 'Sucursal'															, .F. }, ; //X3_TITSPA
	{ 'Branch'																, .F. }, ; //X3_TITENG
	{ 'Filial'																, .F. }, ; //X3_DESCRIC
	{ 'Sucursal'															, .F. }, ; //X3_DESCSPA
	{ 'Branch'																, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(148) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ ''																	, .F. }, ; //X3_BROWSE
	{ 'V'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ '033'																	, .F. }, ; //X3_GRPSXG
	{ '1'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '1'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '02'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_NOTA'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 9																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Nota Fiscal'															, .F. }, ; //X3_TITULO
	{ 'Factura'																, .F. }, ; //X3_TITSPA
	{ 'Invoice'																, .F. }, ; //X3_TITENG
	{ 'Nota Fiscal'															, .F. }, ; //X3_DESCRIC
	{ 'Factura'																, .F. }, ; //X3_DESCSPA
	{ 'Invoice'																, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'V'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '1'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '1'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '03'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_SERIE'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 3																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Serie'																, .F. }, ; //X3_TITULO
	{ 'Serie'																, .F. }, ; //X3_TITSPA
	{ 'Series'																, .F. }, ; //X3_TITENG
	{ 'Serie'																, .F. }, ; //X3_DESCRIC
	{ 'Serie'																, .F. }, ; //X3_DESCSPA
	{ 'Series'																, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(136) + Chr(128) + Chr(128)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'V'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '1'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '04'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_CLIENT'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 6																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Cliente'																, .F. }, ; //X3_TITULO
	{ 'Cliente'																, .F. }, ; //X3_TITSPA
	{ 'Customer'															, .F. }, ; //X3_TITENG
	{ 'Cliente'																, .F. }, ; //X3_DESCRIC
	{ 'Cliente'																, .F. }, ; //X3_DESCSPA
	{ 'Customer'															, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'V'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ '001'																	, .F. }, ; //X3_GRPSXG
	{ '1'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '05'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_LOJA'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 2																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Loja'																, .F. }, ; //X3_TITULO
	{ 'Tienda'																, .F. }, ; //X3_TITSPA
	{ 'Store'																, .F. }, ; //X3_TITENG
	{ 'Loja'																, .F. }, ; //X3_DESCRIC
	{ 'Tienda'																, .F. }, ; //X3_DESCSPA
	{ 'Store'																, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'V'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ '002'																	, .F. }, ; //X3_GRPSXG
	{ '1'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '2'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '06'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_NOME'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 40																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Nome'																, .F. }, ; //X3_TITULO
	{ 'Nombre'																, .F. }, ; //X3_TITSPA
	{ 'Name'																, .F. }, ; //X3_TITENG
	{ 'Nome'																, .F. }, ; //X3_DESCRIC
	{ 'Nombre'																, .F. }, ; //X3_DESCSPA
	{ 'Name'																, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ 'Posicione( ' + DUPLAS  + 'SA1' + DUPLAS  + ', 1, xFilial(' + SIMPLES + 'SA1' + SIMPLES + ') + GZH->GZH_CLIENT + GZH->GZH_LOJA , ' + SIMPLES + 'A1_NOME' + SIMPLES + ')', .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'S'																	, .F. }, ; //X3_BROWSE
	{ 'V'																	, .F. }, ; //X3_VISUAL
	{ 'V'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ 'Posicione( ' + DUPLAS  + 'SA1' + DUPLAS  + ', 1, xFilial(' + SIMPLES + 'SA1' + SIMPLES + ') + GZH->(GZH_CLIENT +GZH_LOJA) , ' + SIMPLES + 'A1_NOME' + SIMPLES + ')', .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '1'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '07'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_UMUINI'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 2																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'UF inicial'															, .F. }, ; //X3_TITULO
	{ 'EsPrRg Inici'														, .F. }, ; //X3_TITSPA
	{ 'Initial Stat'														, .F. }, ; //X3_TITENG
	{ 'UF inicial'															, .F. }, ; //X3_DESCRIC
	{ 'E/P/R inicial'														, .F. }, ; //X3_DESCSPA
	{ 'Initial State'														, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ 'ExistCpo("SX5","12"+M->GZH_UMUINI)'									, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ '12'																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '1'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '08'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_CMUINI'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 7																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Mun Inicial'															, .F. }, ; //X3_TITULO
	{ 'Mun Inicial'															, .F. }, ; //X3_TITSPA
	{ 'Initial mun'															, .F. }, ; //X3_TITENG
	{ 'Mun Inicial'															, .F. }, ; //X3_DESCRIC
	{ 'Mun Inicial'															, .F. }, ; //X3_DESCSPA
	{ 'Initial mun'															, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ 'ExistCpo("CC2",M->GZH_UMUINI+M->GZH_CMUINI)'							, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ 'MUGZH1'																, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '1'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '09'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_DMUINI'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 60																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Desc Mun Ini'														, .F. }, ; //X3_TITULO
	{ 'Desc Mun Ini'														, .F. }, ; //X3_TITSPA
	{ 'Ini mun desc'														, .F. }, ; //X3_TITENG
	{ 'Desc Mun Ini'														, .F. }, ; //X3_DESCRIC
	{ 'Desc Mun Ini'														, .F. }, ; //X3_DESCSPA
	{ 'Ini mun desc'														, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'V'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '1'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '10'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_UMUFIM'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 2																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'UF final'															, .F. }, ; //X3_TITULO
	{ 'EsPrRg Final'														, .F. }, ; //X3_TITSPA
	{ 'Final State'															, .F. }, ; //X3_TITENG
	{ 'UF final'															, .F. }, ; //X3_DESCRIC
	{ 'E/P/R Final'															, .F. }, ; //X3_DESCSPA
	{ 'Final State'															, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ 'ExistCpo("SX5","12"+M->GZH_UMUFIM)'									, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ '12'																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '1'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '11'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_CMUFIM'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 7																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Mun Final'															, .F. }, ; //X3_TITULO
	{ 'Mun Final'															, .F. }, ; //X3_TITSPA
	{ 'Final mun'															, .F. }, ; //X3_TITENG
	{ 'Mun Final'															, .F. }, ; //X3_DESCRIC
	{ 'Mun Final'															, .F. }, ; //X3_DESCSPA
	{ 'Final mun'															, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ 'ExistCpo("CC2",M->GZH_UMUFIM+M->GZH_CMUFIM)'							, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ 'MUGZH2'																, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '1'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '12'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_DMUFIM'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 60																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Nome Mun Fim'														, .F. }, ; //X3_TITULO
	{ 'Nomb Mun Fin'														, .F. }, ; //X3_TITSPA
	{ 'Fin mun name'														, .F. }, ; //X3_TITENG
	{ 'Nome Mun Fim'														, .F. }, ; //X3_DESCRIC
	{ 'Nomb Mun Fin'														, .F. }, ; //X3_DESCSPA
	{ 'Final mun name'														, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'V'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '1'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '13'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_VEIC'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 16																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Veiculo'																, .F. }, ; //X3_TITULO
	{ 'Veh�culo'															, .F. }, ; //X3_TITSPA
	{ 'Vehicle'																, .F. }, ; //X3_TITENG
	{ 'Veiculo'																, .F. }, ; //X3_DESCRIC
	{ 'Veh�culo'															, .F. }, ; //X3_DESCSPA
	{ 'Vehicle'																, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ 'ST9CTE'																, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ 'S'																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '2'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '14'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_PLACA'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 8																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Placa'																, .F. }, ; //X3_TITULO
	{ 'Placa'																, .F. }, ; //X3_TITSPA
	{ 'Plate'																, .F. }, ; //X3_TITENG
	{ 'Placa'																, .F. }, ; //X3_DESCRIC
	{ 'Placa'																, .F. }, ; //X3_DESCSPA
	{ 'Plate'																, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '2'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '15'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_RENAVA'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 15																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Renavam'																, .F. }, ; //X3_TITULO
	{ 'Renavam'																, .F. }, ; //X3_TITSPA
	{ 'Renavam'																, .F. }, ; //X3_TITENG
	{ 'Renavam'																, .F. }, ; //X3_DESCRIC
	{ 'Renavam'																, .F. }, ; //X3_DESCSPA
	{ 'Renavam'																, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '2'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '16'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_UFVEI'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 2																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'UF Veic'																, .F. }, ; //X3_TITULO
	{ 'E/P/R Veh�c'															, .F. }, ; //X3_TITSPA
	{ 'Vehicle stat'														, .F. }, ; //X3_TITENG
	{ 'UF Veiculo'															, .F. }, ; //X3_DESCRIC
	{ 'E/P/R Veh�culo'														, .F. }, ; //X3_DESCSPA
	{ 'Vehicle State'														, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ 'ExistCpo("SX5","12"+M->GZH_UFVEI)'									, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '2'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '17'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_INFQ'															, .F. }, ; //X3_CAMPO
	{ 'N'																	, .F. }, ; //X3_TIPO
	{ 3																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Quant Passag'														, .F. }, ; //X3_TITULO
	{ 'Cant. Pasaj.'														, .F. }, ; //X3_TITSPA
	{ 'No. pass'															, .F. }, ; //X3_TITENG
	{ 'Quant Passageiros'													, .F. }, ; //X3_DESCRIC
	{ 'Cantidad pasajeros'													, .F. }, ; //X3_DESCSPA
	{ 'Number of passengers'												, .F. }, ; //X3_DESCENG
	{ '@E 999'																, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '2'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '18'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_UFPER'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 30																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'UF Percurso'															, .F. }, ; //X3_TITULO
	{ 'UF Trayecto'															, .F. }, ; //X3_TITSPA
	{ 'Route state'															, .F. }, ; //X3_TITENG
	{ 'UF Percurso'															, .F. }, ; //X3_DESCRIC
	{ 'E/P/R Trayecto'														, .F. }, ; //X3_DESCSPA
	{ 'Route State'															, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '2'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '19'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_AUTTAF'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 12																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Aut. TAF'															, .F. }, ; //X3_TITULO
	{ 'Aut. TAF'															, .F. }, ; //X3_TITSPA
	{ 'TAF Aut.'															, .F. }, ; //X3_TITENG
	{ 'Aut. TAF'															, .F. }, ; //X3_DESCRIC
	{ 'Aut. TAF'															, .F. }, ; //X3_DESCSPA
	{ 'Aut. TAF'															, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '2'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '20'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_REGEST'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 25																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Reg Estadual'														, .F. }, ; //X3_TITULO
	{ 'Reg Estatal'															, .F. }, ; //X3_TITSPA
	{ 'State reg'															, .F. }, ; //X3_TITENG
	{ 'Reg Estadual'														, .F. }, ; //X3_DESCRIC
	{ 'Reg Estatal'															, .F. }, ; //X3_DESCSPA
	{ 'State reg'															, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '2'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '21'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_STATUS'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 1																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Status'																, .F. }, ; //X3_TITULO
	{ 'Estatus'																, .F. }, ; //X3_TITSPA
	{ 'Status'																, .F. }, ; //X3_TITENG
	{ 'Status'																, .F. }, ; //X3_DESCRIC
	{ 'Estatus'																, .F. }, ; //X3_DESCSPA
	{ 'Status'																, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'V'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ '1=Pendente de Complemento;2=Pendente de Transmissao;3=Transmitida;4=Erro de Transmissao;5=Documento Impresso;6=Cancelado', .F. }, ; //X3_CBOX
	{ '1=Pendiente de complemento;2=Pendiente de transmisi�n;3=Transmitida;4=Error de transmisi�n;5=Documento impreso;6=Anulado', .F. }, ; //X3_CBOXSPA
	{ '1=Complement Pending;2=Transm Pending;3=Transmitted;4=Transmission Error;5=Printing Document;6=Canceled', .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '4'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '22'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_OBSNF'															, .F. }, ; //X3_CAMPO
	{ 'M'																	, .F. }, ; //X3_TIPO
	{ 10																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Obs NF'																, .F. }, ; //X3_TITULO
	{ 'Obs Fact'															, .F. }, ; //X3_TITSPA
	{ 'Inv note'															, .F. }, ; //X3_TITENG
	{ 'Obs NF'																, .F. }, ; //X3_DESCRIC
	{ 'Obs Fact'															, .F. }, ; //X3_DESCSPA
	{ 'Invoice note'														, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '3'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '23'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_MSGFIS'															, .F. }, ; //X3_CAMPO
	{ 'M'																	, .F. }, ; //X3_TIPO
	{ 10																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Obs Fiscal'															, .F. }, ; //X3_TITULO
	{ 'Obs Fiscal'															, .F. }, ; //X3_TITSPA
	{ 'Fiscal Obs'															, .F. }, ; //X3_TITENG
	{ 'Obs Fiscal'															, .F. }, ; //X3_DESCRIC
	{ 'Obs Fiscal'															, .F. }, ; //X3_DESCSPA
	{ 'Fiscal Observation'													, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '3'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '24'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_XMLCTE'															, .F. }, ; //X3_CAMPO
	{ 'M'																	, .F. }, ; //X3_TIPO
	{ 10																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Xml CteOS'															, .F. }, ; //X3_TITULO
	{ 'Xml CteOS'															, .F. }, ; //X3_TITSPA
	{ 'Xml CteOS'															, .F. }, ; //X3_TITENG
	{ 'Xml CteOS'															, .F. }, ; //X3_DESCRIC
	{ 'Xml CteOS'															, .F. }, ; //X3_DESCSPA
	{ 'Xml CteOS'															, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'V'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '4'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '25'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_XMLAUT'															, .F. }, ; //X3_CAMPO
	{ 'M'																	, .F. }, ; //X3_TIPO
	{ 10																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Aut. Sefaz'															, .F. }, ; //X3_TITULO
	{ 'Aut. Sefaz'															, .F. }, ; //X3_TITSPA
	{ 'Sefaz Aut.'															, .F. }, ; //X3_TITENG
	{ 'Aut. Sefaz'															, .F. }, ; //X3_DESCRIC
	{ 'Aut. Sefaz'															, .F. }, ; //X3_DESCSPA
	{ 'Aut. Sefaz'															, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'V'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '4'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '26'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_XMLERR'															, .F. }, ; //X3_CAMPO
	{ 'M'																	, .F. }, ; //X3_TIPO
	{ 10																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Erro CTEOS'															, .F. }, ; //X3_TITULO
	{ 'Error CTEOS'															, .F. }, ; //X3_TITSPA
	{ 'Error CTEOS'															, .F. }, ; //X3_TITENG
	{ 'Erro CTEOS'															, .F. }, ; //X3_DESCRIC
	{ 'Error CTEOS'															, .F. }, ; //X3_DESCSPA
	{ 'Error CTEOS'															, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'V'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '4'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '27'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_CHVCTE'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 44																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Chave CTEOS'															, .F. }, ; //X3_TITULO
	{ 'Clave CTEOS'															, .F. }, ; //X3_TITSPA
	{ 'CTEOS key'															, .F. }, ; //X3_TITENG
	{ 'Chave CTEOS'															, .F. }, ; //X3_DESCRIC
	{ 'Clave CTEOS'															, .F. }, ; //X3_DESCSPA
	{ 'CTEOS key'															, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'V'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '4'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '28'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_PROTCA'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 15																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Prot Canc'															, .F. }, ; //X3_TITULO
	{ 'Prot Anul'															, .F. }, ; //X3_TITSPA
	{ 'Canc Prot'															, .F. }, ; //X3_TITENG
	{ 'Prot Canc'															, .F. }, ; //X3_DESCRIC
	{ 'Prot. Anul'															, .F. }, ; //X3_DESCSPA
	{ 'Prot Canc'															, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'V'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '4'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '29'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_PEDIDO'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 6																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Pedido'																, .F. }, ; //X3_TITULO
	{ 'Pedido'																, .F. }, ; //X3_TITSPA
	{ 'Order'																, .F. }, ; //X3_TITENG
	{ 'Pedido'																, .F. }, ; //X3_DESCRIC
	{ 'Pedido'																, .F. }, ; //X3_DESCSPA
	{ 'Order'																, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'V'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '30'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_COMPVL'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 1																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Compl. VL?'															, .F. }, ; //X3_TITULO
	{ 'Compl. Val.'															, .F. }, ; //X3_TITSPA
	{ 'Compl VL?'															, .F. }, ; //X3_TITENG
	{ 'Compl. VL?'															, .F. }, ; //X3_DESCRIC
	{ 'Compl. Val.'															, .F. }, ; //X3_DESCSPA
	{ 'Compl VL?'															, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128)					, .F. }, ; //X3_USADO
	{ "'2'"																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ '1=SIM;2=NAO'															, .F. }, ; //X3_CBOX
	{ '1=S�;2=NO'															, .F. }, ; //X3_CBOXSPA
	{ '1=YES;2=NO'															, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ ''																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '31'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_MODAL'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 2																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Modalidade'															, .F. }, ; //X3_TITULO
	{ 'Modalidad'															, .F. }, ; //X3_TITSPA
	{ 'Mode'																, .F. }, ; //X3_TITENG
	{ 'Modalidade'															, .F. }, ; //X3_DESCRIC
	{ 'Modalidad'															, .F. }, ; //X3_DESCSPA
	{ 'Mode'																, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ '01=Rodoviario;02=Aereo;03=Aquaviario;04=Ferroviario;05=Dutoviario;06=Multimodal', .F. }, ; //X3_CBOX
	{ '01=Terrestre;02=A�reo;03=Hidroviario;04=Ferroviario;05=Por tuber�as;06=Multimodal', .F. }, ; //X3_CBOXSPA
	{ '01=Road;02=Air;03=Water;04=Train;05=Pipeline;06=Multimodal'			, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '2'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '32'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_TPFRET'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 2																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Fretamento'															, .F. }, ; //X3_TITULO
	{ 'Fletamento'															, .F. }, ; //X3_TITSPA
	{ 'Freight'																, .F. }, ; //X3_TITENG
	{ 'Fretamento'															, .F. }, ; //X3_DESCRIC
	{ 'Fletamento'															, .F. }, ; //X3_DESCSPA
	{ 'Freight'																, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ '01=Continuo;02=Eventual'												, .F. }, ; //X3_CBOX
	{ '01=Continuo;02=Eventual'												, .F. }, ; //X3_CBOXSPA
	{ '01=Continuous;02=Eventual'											, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '1'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '33'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_DSAIDA'															, .F. }, ; //X3_CAMPO
	{ 'D'																	, .F. }, ; //X3_TIPO
	{ 8																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Data Saida'															, .F. }, ; //X3_TITULO
	{ 'Fecha salida'														, .F. }, ; //X3_TITSPA
	{ 'Outflow Date'														, .F. }, ; //X3_TITENG
	{ 'Data Saida'															, .F. }, ; //X3_DESCRIC
	{ 'Fecha salida'														, .F. }, ; //X3_DESCSPA
	{ 'Outflow Date'														, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '1'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '34'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_HSAIDA'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 8																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Hora Saida'															, .F. }, ; //X3_TITULO
	{ 'Hora salida'															, .F. }, ; //X3_TITSPA
	{ 'Outflow Time'														, .F. }, ; //X3_TITENG
	{ 'Hora Saida'															, .F. }, ; //X3_DESCRIC
	{ 'Hora salida'															, .F. }, ; //X3_DESCSPA
	{ 'Outflow Time'														, .F. }, ; //X3_DESCENG
	{ '@! 99:99:99'															, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '1'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '35'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_EVENTO'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 2																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Evento'																, .F. }, ; //X3_TITULO
	{ 'Evento'																, .F. }, ; //X3_TITSPA
	{ 'Event'																, .F. }, ; //X3_TITENG
	{ 'Evento'																, .F. }, ; //X3_DESCRIC
	{ 'Evento'																, .F. }, ; //X3_DESCSPA
	{ 'Event'																, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ '01=Anular;02=Substituir;03=Complementar'								, .F. }, ; //X3_CBOX
	{ '01=Anular;02=Sustituir;03=Complementar'								, .F. }, ; //X3_CBOXSPA
	{ '01=Cancel;02=Replace;03=Complement'									, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '4'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '36'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_CHVANU'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 44																	, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Chave de Anu'														, .F. }, ; //X3_TITULO
	{ 'Clave Anual'															, .F. }, ; //X3_TITSPA
	{ 'Canc Key'															, .F. }, ; //X3_TITENG
	{ 'Chave de Anu'														, .F. }, ; //X3_DESCRIC
	{ 'Clave Anual'															, .F. }, ; //X3_DESCSPA
	{ 'Canc Key'															, .F. }, ; //X3_DESCENG
	{ ''																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ ''																	, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '4'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME

aAdd( aSX3, { ;
	{ 'GZH'																	, .F. }, ; //X3_ARQUIVO
	{ '37'																	, .F. }, ; //X3_ORDEM
	{ 'GZH_CODGQ2'															, .F. }, ; //X3_CAMPO
	{ 'C'																	, .F. }, ; //X3_TIPO
	{ 6																		, .F. }, ; //X3_TAMANHO
	{ 0																		, .F. }, ; //X3_DECIMAL
	{ 'Lote Taxas'															, .F. }, ; //X3_TITULO
	{ 'Lote Tasas'															, .F. }, ; //X3_TITSPA
	{ 'Rate Batch'															, .F. }, ; //X3_TITENG
	{ 'COD Lote Taxas'														, .F. }, ; //X3_DESCRIC
	{ 'C�D Lote Tasas'														, .F. }, ; //X3_DESCSPA
	{ 'Rate Batch COD'														, .F. }, ; //X3_DESCENG
	{ '@!'																	, .F. }, ; //X3_PICTURE
	{ ''																	, .F. }, ; //X3_VALID
	{ Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(128) + ;
	Chr(128) + Chr(128) + Chr(128) + Chr(128) + Chr(160)					, .F. }, ; //X3_USADO
	{ ''																	, .F. }, ; //X3_RELACAO
	{ 'CODGQ2'																, .F. }, ; //X3_F3
	{ 1																		, .F. }, ; //X3_NIVEL
	{ Chr(132) + Chr(128)													, .F. }, ; //X3_RESERV
	{ ''																	, .F. }, ; //X3_CHECK
	{ ''																	, .F. }, ; //X3_TRIGGER
	{ ''																	, .F. }, ; //X3_PROPRI
	{ 'N'																	, .F. }, ; //X3_BROWSE
	{ 'A'																	, .F. }, ; //X3_VISUAL
	{ 'R'																	, .F. }, ; //X3_CONTEXT
	{ ''																	, .F. }, ; //X3_OBRIGAT
	{ ''																	, .F. }, ; //X3_VLDUSER
	{ ''																	, .F. }, ; //X3_CBOX
	{ ''																	, .F. }, ; //X3_CBOXSPA
	{ ''																	, .F. }, ; //X3_CBOXENG
	{ ''																	, .F. }, ; //X3_PICTVAR
	{ ''																	, .F. }, ; //X3_WHEN
	{ ''																	, .F. }, ; //X3_INIBRW
	{ ''																	, .F. }, ; //X3_GRPSXG
	{ '1'																	, .F. }, ; //X3_FOLDER
	{ ''																	, .F. }, ; //X3_CONDSQL
	{ ''																	, .F. }, ; //X3_CHKSQL
	{ 'N'																	, .F. }, ; //X3_IDXSRV
	{ ''																	, .F. }, ; //X3_ORTOGRA
	{ ''																	, .F. }, ; //X3_TELA
	{ '1'																	, .F. }, ; //X3_POSLGT
	{ 'N'																	, .F. }, ; //X3_IDXFLD
	{ ''																	, .F. }, ; //X3_AGRUP
	{ '2'																	, .F. }, ; //X3_MODAL
	{ 'S'																	, .F. }} ) //X3_PYME


//
// Atualizando dicion�rio
//
nPosArq := aScan( aEstrut, { |x| AllTrim( x[1] ) == "X3_ARQUIVO" } )
nPosOrd := aScan( aEstrut, { |x| AllTrim( x[1] ) == "X3_ORDEM"   } )
nPosCpo := aScan( aEstrut, { |x| AllTrim( x[1] ) == "X3_CAMPO"   } )
nPosTam := aScan( aEstrut, { |x| AllTrim( x[1] ) == "X3_TAMANHO" } )
nPosSXG := aScan( aEstrut, { |x| AllTrim( x[1] ) == "X3_GRPSXG"  } )
nPosVld := aScan( aEstrut, { |x| AllTrim( x[1] ) == "X3_VALID"   } )

aSort( aSX3,,, { |x,y| x[nPosArq][1]+x[nPosOrd][1]+x[nPosCpo][1] < y[nPosArq][1]+y[nPosOrd][1]+y[nPosCpo][1] } )

oProcess:SetRegua2( Len( aSX3 ) )

dbSelectArea( "SX3" )
dbSetOrder( 2 )
cAliasAtu := ""

For nI := 1 To Len( aSX3 )

	//
	// Verifica se o campo faz parte de um grupo e ajusta tamanho
	//
	If !Empty( aSX3[nI][nPosSXG][1] )
		SXG->( dbSetOrder( 1 ) )
		If SXG->( MSSeek( aSX3[nI][nPosSXG][1] ) )
			If aSX3[nI][nPosTam][1] <> SXG->XG_SIZE
				aSX3[nI][nPosTam][1] := SXG->XG_SIZE
				AutoGrLog( "O tamanho do campo " + aSX3[nI][nPosCpo][1] + " N�O atualizado e foi mantido em [" + ;
				AllTrim( Str( SXG->XG_SIZE ) ) + "]" + CRLF + ;
				" por pertencer ao grupo de campos [" + SXG->XG_GRUPO + "]" + CRLF )
			EndIf
		EndIf
	EndIf

	SX3->( dbSetOrder( 2 ) )

	If !( aSX3[nI][nPosArq][1] $ cAlias )
		cAlias += aSX3[nI][nPosArq][1] + "/"
		aAdd( aArqUpd, aSX3[nI][nPosArq][1] )
	EndIf

	If !SX3->( dbSeek( PadR( aSX3[nI][nPosCpo][1], nTamSeek ) ) )

		//
		// Busca ultima ocorrencia do alias
		//
		If ( aSX3[nI][nPosArq][1] <> cAliasAtu )
			cSeqAtu   := "00"
			cAliasAtu := aSX3[nI][nPosArq][1]

			dbSetOrder( 1 )
			SX3->( dbSeek( cAliasAtu + "ZZ", .T. ) )
			dbSkip( -1 )

			If ( SX3->X3_ARQUIVO == cAliasAtu )
				cSeqAtu := SX3->X3_ORDEM
			EndIf

			nSeqAtu := Val( RetAsc( cSeqAtu, 3, .F. ) )
		EndIf

		nSeqAtu++
		cSeqAtu := RetAsc( Str( nSeqAtu ), 2, .T. )

		RecLock( "SX3", .T. )
		For nJ := 1 To Len( aSX3[nI] )
			If     nJ == nPosOrd  // Ordem
				SX3->( FieldPut( FieldPos( aEstrut[nJ][1] ), cSeqAtu ) )

			ElseIf aEstrut[nJ][2] > 0
				SX3->( FieldPut( FieldPos( aEstrut[nJ][1] ), aSX3[nI][nJ][1] ) )

			EndIf
		Next nJ

		dbCommit()
		MsUnLock()

		AutoGrLog( "Criado campo " + aSX3[nI][nPosCpo][1] )

	Else

		//
		// Verifica se o campo faz parte de um grupo e ajsuta tamanho
		//
		If !Empty( SX3->X3_GRPSXG ) .AND. SX3->X3_GRPSXG <> aSX3[nI][nPosSXG][1]
			SXG->( dbSetOrder( 1 ) )
			If SXG->( MSSeek( SX3->X3_GRPSXG ) )
				If aSX3[nI][nPosTam][1] <> SXG->XG_SIZE
					aSX3[nI][nPosTam][1] := SXG->XG_SIZE
					AutoGrLog( "O tamanho do campo " + aSX3[nI][nPosCpo][1] + " N�O atualizado e foi mantido em [" + ;
					AllTrim( Str( SXG->XG_SIZE ) ) + "]"+ CRLF + ;
					"   por pertencer ao grupo de campos [" + SX3->X3_GRPSXG + "]" + CRLF )
				EndIf
			EndIf
		EndIf

		//
		// Verifica todos os campos
		//
		For nJ := 1 To Len( aSX3[nI] )

			//
			// Se o campo estiver diferente da estrutura
			//
			If aSX3[nI][nJ][2]
				cX3Campo := AllTrim( aEstrut[nJ][1] )
				cX3Dado  := SX3->( FieldGet( aEstrut[nJ][2] ) )

				If  aEstrut[nJ][2] > 0 .AND. ;
					PadR( StrTran( AllToChar( cX3Dado ), " ", "" ), 250 ) <> ;
					PadR( StrTran( AllToChar( aSX3[nI][nJ][1] ), " ", "" ), 250 ) .AND. ;
					!cX3Campo == "X3_ORDEM"

					cMsg := "O campo " + aSX3[nI][nPosCpo][1] + " est� com o " + cX3Campo + ;
					" com o conte�do" + CRLF + ;
					"[" + RTrim( AllToChar( cX3Dado ) ) + "]" + CRLF + ;
					"que ser� substitu�do pelo NOVO conte�do" + CRLF + ;
					"[" + RTrim( AllToChar( aSX3[nI][nJ][1] ) ) + "]" + CRLF + ;
					"Deseja substituir ? "

					If      lTodosSim
						nOpcA := 1
					ElseIf  lTodosNao
						nOpcA := 2
					Else
						nOpcA := Aviso( "ATUALIZA��O DE DICION�RIOS E TABELAS", cMsg, { "Sim", "N�o", "Sim p/Todos", "N�o p/Todos" }, 3, "Diferen�a de conte�do - SX3" )
						lTodosSim := ( nOpcA == 3 )
						lTodosNao := ( nOpcA == 4 )

						If lTodosSim
							nOpcA := 1
							lTodosSim := MsgNoYes( "Foi selecionada a op��o de REALIZAR TODAS altera��es no SX3 e N�O MOSTRAR mais a tela de aviso." + CRLF + "Confirma a a��o [Sim p/Todos] ?" )
						EndIf

						If lTodosNao
							nOpcA := 2
							lTodosNao := MsgNoYes( "Foi selecionada a op��o de N�O REALIZAR nenhuma altera��o no SX3 que esteja diferente da base e N�O MOSTRAR mais a tela de aviso." + CRLF + "Confirma esta a��o [N�o p/Todos]?" )
						EndIf

					EndIf

					If nOpcA == 1
						AutoGrLog( "Alterado campo " + aSX3[nI][nPosCpo][1] + CRLF + ;
						"   " + PadR( cX3Campo, 10 ) + " de [" + AllToChar( cX3Dado ) + "]" + CRLF + ;
						"            para [" + AllToChar( aSX3[nI][nJ][1] )           + "]" + CRLF )

						RecLock( "SX3", .F. )
						FieldPut( FieldPos( aEstrut[nJ][1] ), aSX3[nI][nJ][1] )
						MsUnLock()
					EndIf

				EndIf

			EndIf

		Next

	EndIf

	oProcess:IncRegua2( "Atualizando Campos de Tabelas (SX3)..." )

Next nI

AutoGrLog( CRLF + "Final da Atualiza��o" + " SX3" + CRLF + Replicate( "-", 128 ) + CRLF )

Return NIL


//--------------------------------------------------------------------
/*/{Protheus.doc} FSAtuSIX
Fun��o de processamento da grava��o do SIX - Indices

@author TOTVS Protheus
@since  10/01/2020
@obs    Gerado por EXPORDIC - V.6.3.0.1 EFS / Upd. V.5.0.0 EFS
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function FSAtuSIX()
Local aEstrut   := {}
Local aSIX      := {}
Local lAlt      := .F.
Local lDelInd   := .F.
Local nI        := 0
Local nJ        := 0

AutoGrLog( "�nicio da Atualiza��o" + " SIX" + CRLF )

aEstrut := { "INDICE" , "ORDEM" , "CHAVE", "DESCRICAO", "DESCSPA"  , ;
             "DESCENG", "PROPRI", "F3"   , "NICKNAME" , "SHOWPESQ" }

//
// Tabela G53
//
aAdd( aSIX, { ;
	'G53'																	, ; //INDICE
	'1'																		, ; //ORDEM
	'G53_FILIAL+G53_GRUPO+G53_CAMPO'										, ; //CHAVE
	'Grupo + Campo'															, ; //DESCRICAO
	'Grupo + Campo'															, ; //DESCSPA
	'Group + Field'															, ; //DESCENG
	'S'																		, ; //PROPRI
	''																		, ; //F3
	''																		, ; //NICKNAME
	'S'																		} ) //SHOWPESQ

//
// Tabela GQ1
//
aAdd( aSIX, { ;
	'GQ1'																	, ; //INDICE
	'1'																		, ; //ORDEM
	'GQ1_FILIAL+GQ1_CODIGO'													, ; //CHAVE
	'C�digo'																, ; //DESCRICAO
	'C�digo'																, ; //DESCSPA
	'Code'																	, ; //DESCENG
	'S'																		, ; //PROPRI
	''																		, ; //F3
	''																		, ; //NICKNAME
	'S'																		} ) //SHOWPESQ

//
// Tabela GQ2
//
aAdd( aSIX, { ;
	'GQ2'																	, ; //INDICE
	'1'																		, ; //ORDEM
	'GQ2_FILIAL+GQ2_CODIGO'													, ; //CHAVE
	'Codigo'																, ; //DESCRICAO
	'C�digo'																, ; //DESCSPA
	'Code'																	, ; //DESCENG
	'S'																		, ; //PROPRI
	''																		, ; //F3
	''																		, ; //NICKNAME
	'S'																		} ) //SHOWPESQ

//
// Tabela GZH
//
aAdd( aSIX, { ;
	'GZH'																	, ; //INDICE
	'1'																		, ; //ORDEM
	'GZH_FILIAL+GZH_NOTA+GZH_SERIE+GZH_CLIENT+GZH_LOJA'						, ; //CHAVE
	'Nota Fiscal + Serie + Cliente + Loja'									, ; //DESCRICAO
	'Factura + Serie + Cliente + Tienda'									, ; //DESCSPA
	'Invoice + Series + Customer + Store'									, ; //DESCENG
	'S'																		, ; //PROPRI
	''																		, ; //F3
	''																		, ; //NICKNAME
	'S'																		} ) //SHOWPESQ

//
// Atualizando dicion�rio
//
oProcess:SetRegua2( Len( aSIX ) )

dbSelectArea( "SIX" )
SIX->( dbSetOrder( 1 ) )

For nI := 1 To Len( aSIX )

	lAlt    := .F.
	lDelInd := .F.

	If !SIX->( dbSeek( aSIX[nI][1] + aSIX[nI][2] ) )
		AutoGrLog( "�ndice criado " + aSIX[nI][1] + "/" + aSIX[nI][2] + " - " + aSIX[nI][3] )
	Else
		lAlt := .T.
		aAdd( aArqUpd, aSIX[nI][1] )
		If !StrTran( Upper( AllTrim( CHAVE )       ), " ", "" ) == ;
		    StrTran( Upper( AllTrim( aSIX[nI][3] ) ), " ", "" )
			AutoGrLog( "Chave do �ndice alterado " + aSIX[nI][1] + "/" + aSIX[nI][2] + " - " + aSIX[nI][3] )
			lDelInd := .T. // Se for altera��o precisa apagar o indice do banco
		EndIf
	EndIf

	RecLock( "SIX", !lAlt )
	For nJ := 1 To Len( aSIX[nI] )
		If FieldPos( aEstrut[nJ] ) > 0
			FieldPut( FieldPos( aEstrut[nJ] ), aSIX[nI][nJ] )
		EndIf
	Next nJ
	MsUnLock()

	dbCommit()

	If lDelInd
		TcInternal( 60, RetSqlName( aSIX[nI][1] ) + "|" + RetSqlName( aSIX[nI][1] ) + aSIX[nI][2] )
	EndIf

	oProcess:IncRegua2( "Atualizando �ndices..." )

Next nI

AutoGrLog( CRLF + "Final da Atualiza��o" + " SIX" + CRLF + Replicate( "-", 128 ) + CRLF )

Return NIL


//--------------------------------------------------------------------
/*/{Protheus.doc} FSAtuSX6
Fun��o de processamento da grava��o do SX6 - Par�metros

@author TOTVS Protheus
@since  10/01/2020
@obs    Gerado por EXPORDIC - V.6.3.0.1 EFS / Upd. V.5.0.0 EFS
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function FSAtuSX6()
Local aEstrut   := {}
Local aSX6      := {}
Local cAlias    := ""
Local cMsg      := ""
Local lContinua := .T.
Local lReclock  := .T.
Local lTodosNao := .F.
Local lTodosSim := .F.
Local nI        := 0
Local nJ        := 0
Local nOpcA     := 0
Local nTamFil   := Len( SX6->X6_FIL )
Local nTamVar   := Len( SX6->X6_VAR )

AutoGrLog( "�nicio da Atualiza��o" + " SX6" + CRLF )

aEstrut := { "X6_FIL"    , "X6_VAR"    , "X6_TIPO"   , "X6_DESCRIC", "X6_DSCSPA" , "X6_DSCENG" , "X6_DESC1"  , ;
             "X6_DSCSPA1", "X6_DSCENG1", "X6_DESC2"  , "X6_DSCSPA2", "X6_DSCENG2", "X6_CONTEUD", "X6_CONTSPA", ;
             "X6_CONTENG", "X6_PROPRI" , "X6_VALID"  , "X6_INIT"   , "X6_DEFPOR" , "X6_DEFSPA" , "X6_DEFENG" , ;
             "X6_PYME"   }

aAdd( aSX6, { ;
	'  '																	, ; //X6_FIL
	'FS_GCTCOT'																, ; //X6_VAR
	'C'																		, ; //X6_TIPO
	'Tipo Contrato para cotacao'											, ; //X6_DESCRIC
	'Tipo Contrato para cotizacion'											, ; //X6_DSCSPA
	'Contract type for quotation'											, ; //X6_DSCENG
	''																		, ; //X6_DESC1
	''																		, ; //X6_DSCSPA1
	''																		, ; //X6_DSCENG1
	''																		, ; //X6_DESC2
	''																		, ; //X6_DSCSPA2
	''																		, ; //X6_DSCENG2
	'001'																	, ; //X6_CONTEUD
	'001'																	, ; //X6_CONTSPA
	'001'																	, ; //X6_CONTENG
	'S'																		, ; //X6_PROPRI
	''																		, ; //X6_VALID
	''																		, ; //X6_INIT
	'001'																	, ; //X6_DEFPOR
	'001'																	, ; //X6_DEFSPA
	'001'																	, ; //X6_DEFENG
	'S'																		} ) //X6_PYME

//
// Atualizando dicion�rio
//
oProcess:SetRegua2( Len( aSX6 ) )

dbSelectArea( "SX6" )
dbSetOrder( 1 )

For nI := 1 To Len( aSX6 )
	lContinua := .F.
	lReclock  := .F.

	If !SX6->( dbSeek( PadR( aSX6[nI][1], nTamFil ) + PadR( aSX6[nI][2], nTamVar ) ) )
		lContinua := .T.
		lReclock  := .T.
		AutoGrLog( "Foi inclu�do o par�metro " + aSX6[nI][1] + aSX6[nI][2] + " Conte�do [" + AllTrim( aSX6[nI][13] ) + "]" )
	Else
		lContinua := .T.
		lReclock  := .F.
		If !StrTran( SX6->X6_CONTEUD, " ", "" ) == StrTran( aSX6[nI][13], " ", "" )

			cMsg := "O par�metro " + aSX6[nI][2] + " est� com o conte�do" + CRLF + ;
			"[" + RTrim( StrTran( SX6->X6_CONTEUD, " ", "" ) ) + "]" + CRLF + ;
			", que � ser� substituido pelo NOVO conte�do " + CRLF + ;
			"[" + RTrim( StrTran( aSX6[nI][13]   , " ", "" ) ) + "]" + CRLF + ;
			"Deseja substituir ? "

			If      lTodosSim
				nOpcA := 1
			ElseIf  lTodosNao
				nOpcA := 2
			Else
				nOpcA := Aviso( "ATUALIZA��O DE DICION�RIOS E TABELAS", cMsg, { "Sim", "N�o", "Sim p/Todos", "N�o p/Todos" }, 3, "Diferen�a de conte�do - SX6" )
				lTodosSim := ( nOpcA == 3 )
				lTodosNao := ( nOpcA == 4 )

				If lTodosSim
					nOpcA := 1
					lTodosSim := MsgNoYes( "Foi selecionada a op��o de REALIZAR TODAS altera��es no SX6 e N�O MOSTRAR mais a tela de aviso." + CRLF + "Confirma a a��o [Sim p/Todos] ?" )
				EndIf

				If lTodosNao
					nOpcA := 2
					lTodosNao := MsgNoYes( "Foi selecionada a op��o de N�O REALIZAR nenhuma altera��o no SX6 que esteja diferente da base e N�O MOSTRAR mais a tela de aviso." + CRLF + "Confirma esta a��o [N�o p/Todos]?" )
				EndIf

			EndIf

			lContinua := ( nOpcA == 1 )

			If lContinua
				AutoGrLog( "Foi alterado o par�metro " + aSX6[nI][1] + aSX6[nI][2] + " de [" + ;
				AllTrim( SX6->X6_CONTEUD ) + "]" + " para [" + AllTrim( aSX6[nI][13] ) + "]" )
			EndIf

		Else
			lContinua := .F.
		EndIf
	EndIf

	If lContinua
		If !( aSX6[nI][1] $ cAlias )
			cAlias += aSX6[nI][1] + "/"
		EndIf

		RecLock( "SX6", lReclock )
		For nJ := 1 To Len( aSX6[nI] )
			If FieldPos( aEstrut[nJ] ) > 0
				FieldPut( FieldPos( aEstrut[nJ] ), aSX6[nI][nJ] )
			EndIf
		Next nJ
		dbCommit()
		MsUnLock()
	EndIf

	oProcess:IncRegua2( "Atualizando Arquivos (SX6)..." )

Next nI

AutoGrLog( CRLF + "Final da Atualiza��o" + " SX6" + CRLF + Replicate( "-", 128 ) + CRLF )

Return NIL


//--------------------------------------------------------------------
/*/{Protheus.doc} FSAtuSX7
Fun��o de processamento da grava��o do SX7 - Gatilhos

@author TOTVS Protheus
@since  10/01/2020
@obs    Gerado por EXPORDIC - V.6.3.0.1 EFS / Upd. V.5.0.0 EFS
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function FSAtuSX7()
Local aEstrut   := {}
Local aAreaSX3  := SX3->( GetArea() )
Local aSX7      := {}
Local cAlias    := ""
Local nI        := 0
Local nJ        := 0
Local nTamSeek  := Len( SX7->X7_CAMPO )

AutoGrLog( "�nicio da Atualiza��o" + " SX7" + CRLF )

aEstrut := { "X7_CAMPO", "X7_SEQUENC", "X7_REGRA", "X7_CDOMIN", "X7_TIPO", "X7_SEEK", ;
             "X7_ALIAS", "X7_ORDEM"  , "X7_CHAVE", "X7_PROPRI", "X7_CONDIC" }

//
// Campo GZH_VEIC
//
aAdd( aSX7, { ;
	'GZH_VEIC'																, ; //X7_CAMPO
	'001'																	, ; //X7_SEQUENC
	'ST9->T9_PLACA'															, ; //X7_REGRA
	'GZH_PLACA'																, ; //X7_CDOMIN
	'P'																		, ; //X7_TIPO
	'S'																		, ; //X7_SEEK
	'ST9'																	, ; //X7_ALIAS
	1																		, ; //X7_ORDEM
	'xFilial("ST9")+M->GZH_VEIC'											, ; //X7_CHAVE
	'S'																		, ; //X7_PROPRI
	''																		} ) //X7_CONDIC

aAdd( aSX7, { ;
	'GZH_VEIC'																, ; //X7_CAMPO
	'002'																	, ; //X7_SEQUENC
	'ST9->T9_RENAVAM'														, ; //X7_REGRA
	'GZH_RENAVA'															, ; //X7_CDOMIN
	'P'																		, ; //X7_TIPO
	'S'																		, ; //X7_SEEK
	'ST9'																	, ; //X7_ALIAS
	1																		, ; //X7_ORDEM
	'xFilial("ST9")+M->GZH_VEIC'											, ; //X7_CHAVE
	'S'																		, ; //X7_PROPRI
	''																		} ) //X7_CONDIC

//
// Atualizando dicion�rio
//
oProcess:SetRegua2( Len( aSX7 ) )

dbSelectArea( "SX3" )
dbSetOrder( 2 )

dbSelectArea( "SX7" )
dbSetOrder( 1 )

For nI := 1 To Len( aSX7 )

	If !SX7->( dbSeek( PadR( aSX7[nI][1], nTamSeek ) + aSX7[nI][2] ) )

		If !( aSX7[nI][1] $ cAlias )
			cAlias += aSX7[nI][1] + "/"
			AutoGrLog( "Foi inclu�do o gatilho " + aSX7[nI][1] + "/" + aSX7[nI][2] )
		EndIf

		RecLock( "SX7", .T. )
	Else

		If !( aSX7[nI][1] $ cAlias )
			cAlias += aSX7[nI][1] + "/"
			AutoGrLog( "Foi alterado o gatilho " + aSX7[nI][1] + "/" + aSX7[nI][2] )
		EndIf

		RecLock( "SX7", .F. )
	EndIf

	For nJ := 1 To Len( aSX7[nI] )
		If FieldPos( aEstrut[nJ] ) > 0
			FieldPut( FieldPos( aEstrut[nJ] ), aSX7[nI][nJ] )
		EndIf
	Next nJ

	dbCommit()
	MsUnLock()

	If SX3->( dbSeek( SX7->X7_CAMPO ) )
		RecLock( "SX3", .F. )
		SX3->X3_TRIGGER := "S"
		MsUnLock()
	EndIf

	oProcess:IncRegua2( "Atualizando Arquivos (SX7)..." )

Next nI

RestArea( aAreaSX3 )

AutoGrLog( CRLF + "Final da Atualiza��o" + " SX7" + CRLF + Replicate( "-", 128 ) + CRLF )

Return NIL


//--------------------------------------------------------------------
/*/{Protheus.doc} FSAtuSXA
Fun��o de processamento da grava��o do SXA - Pastas

@author TOTVS Protheus
@since  10/01/2020
@obs    Gerado por EXPORDIC - V.6.3.0.1 EFS / Upd. V.5.0.0 EFS
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function FSAtuSXA()
Local aEstrut   := {}
Local aSXA      := {}
Local cAlias    := ""
Local nI        := 0
Local nJ        := 0
Local nPosAgr   := 0
Local lAlterou  := .F.

AutoGrLog( "�nicio da Atualiza��o" + " SXA" + CRLF )

aEstrut := { "XA_ALIAS"  , "XA_ORDEM"  , "XA_DESCRIC", "XA_DESCSPA", "XA_DESCENG", "XA_AGRUP"  , "XA_TIPO"   , ;
             "XA_PROPRI" }


//
// Tabela GZH
//
aAdd( aSXA, { ;
	'GZH'																	, ; //XA_ALIAS
	'1'																		, ; //XA_ORDEM
	'Trechos da CTEOS'														, ; //XA_DESCRIC
	'Tramos de CTEOS'														, ; //XA_DESCSPA
	'Excertps of CTEOS'														, ; //XA_DESCENG
	''																		, ; //XA_AGRUP
	''																		, ; //XA_TIPO
	'S'																		} ) //XA_PROPRI

aAdd( aSXA, { ;
	'GZH'																	, ; //XA_ALIAS
	'2'																		, ; //XA_ORDEM
	'Veiculo'																, ; //XA_DESCRIC
	'Veh�culo'																, ; //XA_DESCSPA
	'Vehicle'																, ; //XA_DESCENG
	''																		, ; //XA_AGRUP
	''																		, ; //XA_TIPO
	'S'																		} ) //XA_PROPRI

aAdd( aSXA, { ;
	'GZH'																	, ; //XA_ALIAS
	'3'																		, ; //XA_ORDEM
	'Mensagens da CTEOS'													, ; //XA_DESCRIC
	'Mensajes de la CTEOS'													, ; //XA_DESCSPA
	'Messages of CTEOS'														, ; //XA_DESCENG
	''																		, ; //XA_AGRUP
	''																		, ; //XA_TIPO
	'S'																		} ) //XA_PROPRI

aAdd( aSXA, { ;
	'GZH'																	, ; //XA_ALIAS
	'4'																		, ; //XA_ORDEM
	'Status CTeOS'															, ; //XA_DESCRIC
	'Estatus CTeOS'															, ; //XA_DESCSPA
	'CTeOS Status'															, ; //XA_DESCENG
	''																		, ; //XA_AGRUP
	''																		, ; //XA_TIPO
	'S'																		} ) //XA_PROPRI

nPosAgr := aScan( aEstrut, { |x| AllTrim( x ) == "XA_AGRUP" } )

//
// Atualizando dicion�rio
//
oProcess:SetRegua2( Len( aSXA ) )

dbSelectArea( "SXA" )
dbSetOrder( 1 )

For nI := 1 To Len( aSXA )

	If SXA->( dbSeek( aSXA[nI][1] + aSXA[nI][2] ) )

		lAlterou := .F.

		While !SXA->( EOF() ).AND.  SXA->( XA_ALIAS + XA_ORDEM ) == aSXA[nI][1] + aSXA[nI][2]

			If SXA->XA_AGRUP == aSXA[nI][nPosAgr]
				RecLock( "SXA", .F. )
				For nJ := 1 To Len( aSXA[nI] )
					If FieldPos( aEstrut[nJ] ) > 0 .AND. Alltrim(AllToChar(SXA->( FieldGet( nJ ) ))) <> Alltrim(AllToChar(aSXA[nI][nJ]))
						FieldPut( FieldPos( aEstrut[nJ] ), aSXA[nI][nJ] )
						lAlterou := .T.
					EndIf
				Next nJ
				dbCommit()
				MsUnLock()
			EndIf

			SXA->( dbSkip() )

		End

		If lAlterou
			AutoGrLog( "Foi alterada a pasta " + aSXA[nI][1] + "/" + aSXA[nI][2] + "  " + aSXA[nI][3] )
		EndIf

	Else

		RecLock( "SXA", .T. )
		For nJ := 1 To Len( aSXA[nI] )
			If FieldPos( aEstrut[nJ] ) > 0
				FieldPut( FieldPos( aEstrut[nJ] ), aSXA[nI][nJ] )
			EndIf
		Next nJ
		dbCommit()
		MsUnLock()

		AutoGrLog( "Foi inclu�da a pasta " + aSXA[nI][1] + "/" + aSXA[nI][2] + "  " + aSXA[nI][3] )

	EndIf

oProcess:IncRegua2( "Atualizando Arquivos (SXA)..." )

Next nI

AutoGrLog( CRLF + "Final da Atualiza��o" + " SXA" + CRLF + Replicate( "-", 128 ) + CRLF )

Return NIL


//--------------------------------------------------------------------
/*/{Protheus.doc} FSAtuSXB
Fun��o de processamento da grava��o do SXB - Consultas Padrao

@author TOTVS Protheus
@since  10/01/2020
@obs    Gerado por EXPORDIC - V.6.3.0.1 EFS / Upd. V.5.0.0 EFS
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function FSAtuSXB()
Local aEstrut   := {}
Local aSXB      := {}
Local cAlias    := ""
Local cMsg      := ""
Local lTodosNao := .F.
Local lTodosSim := .F.
Local nI        := 0
Local nJ        := 0
Local nOpcA     := 0

AutoGrLog( "�nicio da Atualiza��o" + " SXB" + CRLF )

aEstrut := { "XB_ALIAS"  , "XB_TIPO"   , "XB_SEQ"    , "XB_COLUNA" , "XB_DESCRI" , "XB_DESCSPA", "XB_DESCENG", ;
             "XB_WCONTEM", "XB_CONTEM" }


//
// Consulta CODGQ2
//
aAdd( aSXB, { ;
	'CODGQ2'																, ; //XB_ALIAS
	'1'																		, ; //XB_TIPO
	'01'																	, ; //XB_SEQ
	'DB'																	, ; //XB_COLUNA
	'Cod Lote Taxas'														, ; //XB_DESCRI
	'C�d Lote Tasas'														, ; //XB_DESCSPA
	'Rate Batch Code'														, ; //XB_DESCENG
	''																		, ; //XB_WCONTEM
	'GQ2'																	} ) //XB_CONTEM

aAdd( aSXB, { ;
	'CODGQ2'																, ; //XB_ALIAS
	'2'																		, ; //XB_TIPO
	'01'																	, ; //XB_SEQ
	'01'																	, ; //XB_COLUNA
	'GQ2_FILIAL+GQ2_CODIG'													, ; //XB_DESCRI
	'GQ2_FILIAL+GQ2_CODIG'													, ; //XB_DESCSPA
	'GQ2_FILIAL+GQ2_CODIG'													, ; //XB_DESCENG
	''																		, ; //XB_WCONTEM
	''																		} ) //XB_CONTEM

aAdd( aSXB, { ;
	'CODGQ2'																, ; //XB_ALIAS
	'4'																		, ; //XB_TIPO
	'01'																	, ; //XB_SEQ
	'01'																	, ; //XB_COLUNA
	'Filial'																, ; //XB_DESCRI
	'Sucursal'																, ; //XB_DESCSPA
	'Branch'																, ; //XB_DESCENG
	''																		, ; //XB_WCONTEM
	'GQ2_FILIAL'															} ) //XB_CONTEM

aAdd( aSXB, { ;
	'CODGQ2'																, ; //XB_ALIAS
	'4'																		, ; //XB_TIPO
	'01'																	, ; //XB_SEQ
	'02'																	, ; //XB_COLUNA
	'Codigo'																, ; //XB_DESCRI
	'C�digo'																, ; //XB_DESCSPA
	'Code'																	, ; //XB_DESCENG
	''																		, ; //XB_WCONTEM
	'GQ2_CODIGO'															} ) //XB_CONTEM

aAdd( aSXB, { ;
	'CODGQ2'																, ; //XB_ALIAS
	'4'																		, ; //XB_TIPO
	'01'																	, ; //XB_SEQ
	'03'																	, ; //XB_COLUNA
	'Data Inicial'															, ; //XB_DESCRI
	'Fecha inicial'															, ; //XB_DESCSPA
	'Start Date'															, ; //XB_DESCENG
	''																		, ; //XB_WCONTEM
	'GQ2_DTINI'																} ) //XB_CONTEM

aAdd( aSXB, { ;
	'CODGQ2'																, ; //XB_ALIAS
	'4'																		, ; //XB_TIPO
	'01'																	, ; //XB_SEQ
	'04'																	, ; //XB_COLUNA
	'Data Final'															, ; //XB_DESCRI
	'Fecha final'															, ; //XB_DESCSPA
	'End Date'																, ; //XB_DESCENG
	''																		, ; //XB_WCONTEM
	'GQ2_DTFIM'																} ) //XB_CONTEM

aAdd( aSXB, { ;
	'CODGQ2'																, ; //XB_ALIAS
	'4'																		, ; //XB_TIPO
	'01'																	, ; //XB_SEQ
	'05'																	, ; //XB_COLUNA
	'Data Emissao'															, ; //XB_DESCRI
	'Fch. Emisi�n'															, ; //XB_DESCSPA
	'Issue date'															, ; //XB_DESCENG
	''																		, ; //XB_WCONTEM
	'GQ2_DTEMI'																} ) //XB_CONTEM

aAdd( aSXB, { ;
	'CODGQ2'																, ; //XB_ALIAS
	'4'																		, ; //XB_TIPO
	'01'																	, ; //XB_SEQ
	'06'																	, ; //XB_COLUNA
	'Cliente Nota'															, ; //XB_DESCRI
	'Cliente Fact.'															, ; //XB_DESCSPA
	'Invoice Customer'														, ; //XB_DESCENG
	''																		, ; //XB_WCONTEM
	'GQ2_CLIENT'															} ) //XB_CONTEM

aAdd( aSXB, { ;
	'CODGQ2'																, ; //XB_ALIAS
	'4'																		, ; //XB_TIPO
	'01'																	, ; //XB_SEQ
	'07'																	, ; //XB_COLUNA
	'Loja'																	, ; //XB_DESCRI
	'Tienda'																, ; //XB_DESCSPA
	'Store'																	, ; //XB_DESCENG
	''																		, ; //XB_WCONTEM
	'GQ2_LOJA'																} ) //XB_CONTEM

aAdd( aSXB, { ;
	'CODGQ2'																, ; //XB_ALIAS
	'5'																		, ; //XB_TIPO
	'01'																	, ; //XB_SEQ
	''																		, ; //XB_COLUNA
	''																		, ; //XB_DESCRI
	''																		, ; //XB_DESCSPA
	''																		, ; //XB_DESCENG
	''																		, ; //XB_WCONTEM
	'GQ2_CODIGO'															} ) //XB_CONTEM

aAdd( aSXB, { ;
	'CODGQ2'																, ; //XB_ALIAS
	'6'																		, ; //XB_TIPO
	'01'																	, ; //XB_SEQ
	''																		, ; //XB_COLUNA
	''																		, ; //XB_DESCRI
	''																		, ; //XB_DESCSPA
	''																		, ; //XB_DESCENG
	''																		, ; //XB_WCONTEM
	'IIF(SuperGetMV("MV_INTGTP",.F.,"0") != "1", GQ2->GQ2_CLIENT==GZH->GZH_CLIENT .AND. GQ2->GQ2_LOJA==GZH->GZH_LOJA, .T.)'} ) //XB_CONTEM

//
// Consulta GQ1
//
aAdd( aSXB, { ;
	'GQ1'																	, ; //XB_ALIAS
	'1'																		, ; //XB_TIPO
	'01'																	, ; //XB_SEQ
	'DB'																	, ; //XB_COLUNA
	'Tipos de Venda'														, ; //XB_DESCRI
	'Tipos de venta'														, ; //XB_DESCSPA
	'Sales Types'															, ; //XB_DESCENG
	''																		, ; //XB_WCONTEM
	'GQ1'																	} ) //XB_CONTEM

aAdd( aSXB, { ;
	'GQ1'																	, ; //XB_ALIAS
	'2'																		, ; //XB_TIPO
	'01'																	, ; //XB_SEQ
	'01'																	, ; //XB_COLUNA
	'C�digo'																, ; //XB_DESCRI
	'C�digo'																, ; //XB_DESCSPA
	'Code'																	, ; //XB_DESCENG
	''																		, ; //XB_WCONTEM
	''																		} ) //XB_CONTEM

aAdd( aSXB, { ;
	'GQ1'																	, ; //XB_ALIAS
	'3'																		, ; //XB_TIPO
	'01'																	, ; //XB_SEQ
	'01'																	, ; //XB_COLUNA
	'Cadastra Novo'															, ; //XB_DESCRI
	'Incluye Nuevo'															, ; //XB_DESCSPA
	'Add New'																, ; //XB_DESCENG
	''																		, ; //XB_WCONTEM
	'01'																	} ) //XB_CONTEM

aAdd( aSXB, { ;
	'GQ1'																	, ; //XB_ALIAS
	'4'																		, ; //XB_TIPO
	'01'																	, ; //XB_SEQ
	'01'																	, ; //XB_COLUNA
	'C�digo'																, ; //XB_DESCRI
	'C�digo'																, ; //XB_DESCSPA
	'Code'																	, ; //XB_DESCENG
	''																		, ; //XB_WCONTEM
	'GQ1_CODIGO'															} ) //XB_CONTEM

aAdd( aSXB, { ;
	'GQ1'																	, ; //XB_ALIAS
	'4'																		, ; //XB_TIPO
	'01'																	, ; //XB_SEQ
	'02'																	, ; //XB_COLUNA
	'Descri��o'																, ; //XB_DESCRI
	'Descripci�n'															, ; //XB_DESCSPA
	'Description'															, ; //XB_DESCENG
	''																		, ; //XB_WCONTEM
	'GQ1_DESCRI'															} ) //XB_CONTEM

aAdd( aSXB, { ;
	'GQ1'																	, ; //XB_ALIAS
	'5'																		, ; //XB_TIPO
	'01'																	, ; //XB_SEQ
	''																		, ; //XB_COLUNA
	''																		, ; //XB_DESCRI
	''																		, ; //XB_DESCSPA
	''																		, ; //XB_DESCENG
	''																		, ; //XB_WCONTEM
	'GQ1_CODIGO'															} ) //XB_CONTEM

//
// Atualizando dicion�rio
//
oProcess:SetRegua2( Len( aSXB ) )

dbSelectArea( "SXB" )
dbSetOrder( 1 )

For nI := 1 To Len( aSXB )

	If !Empty( aSXB[nI][1] )

		If !SXB->( dbSeek( PadR( aSXB[nI][1], Len( SXB->XB_ALIAS ) ) + aSXB[nI][2] + aSXB[nI][3] + aSXB[nI][4] ) )

			If !( aSXB[nI][1] $ cAlias )
				cAlias += aSXB[nI][1] + "/"
				AutoGrLog( "Foi inclu�da a consulta padr�o " + aSXB[nI][1] )
			EndIf

			RecLock( "SXB", .T. )

			For nJ := 1 To Len( aSXB[nI] )
				If FieldPos( aEstrut[nJ] ) > 0
					FieldPut( FieldPos( aEstrut[nJ] ), aSXB[nI][nJ] )
				EndIf
			Next nJ

			dbCommit()
			MsUnLock()

		Else

			//
			// Verifica todos os campos
			//
			For nJ := 1 To Len( aSXB[nI] )

				//
				// Se o campo estiver diferente da estrutura
				//
				If aEstrut[nJ] == SXB->( FieldName( nJ ) ) .AND. ;
					!StrTran( AllToChar( SXB->( FieldGet( nJ ) ) ), " ", "" ) == ;
					 StrTran( AllToChar( aSXB[nI][nJ]            ), " ", "" )

					cMsg := "A consulta padr�o " + aSXB[nI][1] + " est� com o " + SXB->( FieldName( nJ ) ) + ;
					" com o conte�do" + CRLF + ;
					"[" + RTrim( AllToChar( SXB->( FieldGet( nJ ) ) ) ) + "]" + CRLF + ;
					", e este � diferente do conte�do" + CRLF + ;
					"[" + RTrim( AllToChar( aSXB[nI][nJ] ) ) + "]" + CRLF +;
					"Deseja substituir ? "

					If      lTodosSim
						nOpcA := 1
					ElseIf  lTodosNao
						nOpcA := 2
					Else
						nOpcA := Aviso( "ATUALIZA��O DE DICION�RIOS E TABELAS", cMsg, { "Sim", "N�o", "Sim p/Todos", "N�o p/Todos" }, 3, "Diferen�a de conte�do - SXB" )
						lTodosSim := ( nOpcA == 3 )
						lTodosNao := ( nOpcA == 4 )

						If lTodosSim
							nOpcA := 1
							lTodosSim := MsgNoYes( "Foi selecionada a op��o de REALIZAR TODAS altera��es no SXB e N�O MOSTRAR mais a tela de aviso." + CRLF + "Confirma a a��o [Sim p/Todos] ?" )
						EndIf

						If lTodosNao
							nOpcA := 2
							lTodosNao := MsgNoYes( "Foi selecionada a op��o de N�O REALIZAR nenhuma altera��o no SXB que esteja diferente da base e N�O MOSTRAR mais a tela de aviso." + CRLF + "Confirma esta a��o [N�o p/Todos]?" )
						EndIf

					EndIf

					If nOpcA == 1
						RecLock( "SXB", .F. )
						FieldPut( FieldPos( aEstrut[nJ] ), aSXB[nI][nJ] )
						dbCommit()
						MsUnLock()

							If !( aSXB[nI][1] $ cAlias )
								cAlias += aSXB[nI][1] + "/"
								AutoGrLog( "Foi alterada a consulta padr�o " + aSXB[nI][1] )
							EndIf

					EndIf

				EndIf

			Next

		EndIf

	EndIf

	oProcess:IncRegua2( "Atualizando Consultas Padr�es (SXB)..." )

Next nI

AutoGrLog( CRLF + "Final da Atualiza��o" + " SXB" + CRLF + Replicate( "-", 128 ) + CRLF )

Return NIL


//--------------------------------------------------------------------
/*/{Protheus.doc} FSAtuSX9
Fun��o de processamento da grava��o do SX9 - Relacionamento

@author TOTVS Protheus
@since  10/01/2020
@obs    Gerado por EXPORDIC - V.6.3.0.1 EFS / Upd. V.5.0.0 EFS
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function FSAtuSX9()
Local aEstrut   := {}
Local aSX9      := {}
Local cAlias    := ""
Local nI        := 0
Local nJ        := 0
Local nTamSeek  := Len( SX9->X9_DOM )

AutoGrLog( "�nicio da Atualiza��o" + " SX9" + CRLF )

aEstrut := { "X9_DOM"    , "X9_IDENT"  , "X9_CDOM"   , "X9_EXPDOM" , "X9_EXPCDOM", "X9_PROPRI" , "X9_LIGDOM" , ;
             "X9_LIGCDOM", "X9_CONDSQL", "X9_USEFIL" , "X9_VINFIL" , "X9_CHVFOR" , "X9_ENABLE" }


//
// Dom�nio CC2
//
aAdd( aSX9, { ;
	'CC2'																	, ; //X9_DOM
	'018'																	, ; //X9_IDENT
	'GZH'																	, ; //X9_CDOM
	'CC2_CODMUN+CC2_MUN'													, ; //X9_EXPDOM
	'GZH_CMUFIM+GZH_DMUFIM'													, ; //X9_EXPCDOM
	'S'																		, ; //X9_PROPRI
	'1'																		, ; //X9_LIGDOM
	'1'																		, ; //X9_LIGCDOM
	''																		, ; //X9_CONDSQL
	'S'																		, ; //X9_USEFIL
	'S'																		, ; //X9_VINFIL
	'S'																		, ; //X9_CHVFOR
	'S'																		} ) //X9_ENABLE

//
// Dom�nio GQ1
//
aAdd( aSX9, { ;
	'GQ1'																	, ; //X9_DOM
	'001'																	, ; //X9_IDENT
	'GQ4'																	, ; //X9_CDOM
	'GQ1_CODIGO'															, ; //X9_EXPDOM
	'GQ4_TPVEND'															, ; //X9_EXPCDOM
	'S'																		, ; //X9_PROPRI
	'1'																		, ; //X9_LIGDOM
	'1'																		, ; //X9_LIGCDOM
	''																		, ; //X9_CONDSQL
	'S'																		, ; //X9_USEFIL
	'S'																		, ; //X9_VINFIL
	'S'																		, ; //X9_CHVFOR
	'S'																		} ) //X9_ENABLE

//
// Dom�nio GQ2
//
aAdd( aSX9, { ;
	'GQ2'																	, ; //X9_DOM
	'001'																	, ; //X9_IDENT
	'GZH'																	, ; //X9_CDOM
	'GQ2_CODIGO'															, ; //X9_EXPDOM
	'GZH_CODGQ2'															, ; //X9_EXPCDOM
	'S'																		, ; //X9_PROPRI
	'1'																		, ; //X9_LIGDOM
	'1'																		, ; //X9_LIGCDOM
	''																		, ; //X9_CONDSQL
	'S'																		, ; //X9_USEFIL
	'S'																		, ; //X9_VINFIL
	'S'																		, ; //X9_CHVFOR
	'S'																		} ) //X9_ENABLE

//
// Dom�nio GYA
//
aAdd( aSX9, { ;
	'GYA'																	, ; //X9_DOM
	'002'																	, ; //X9_IDENT
	'GQ1'																	, ; //X9_CDOM
	'GYA_CODIGO'															, ; //X9_EXPDOM
	'GQ1_TIPDOC'															, ; //X9_EXPCDOM
	'S'																		, ; //X9_PROPRI
	'1'																		, ; //X9_LIGDOM
	'N'																		, ; //X9_LIGCDOM
	''																		, ; //X9_CONDSQL
	'S'																		, ; //X9_USEFIL
	'S'																		, ; //X9_VINFIL
	'S'																		, ; //X9_CHVFOR
	'S'																		} ) //X9_ENABLE

aAdd( aSX9, { ;
	'GYA'																	, ; //X9_DOM
	'005'																	, ; //X9_IDENT
	'GQ2'																	, ; //X9_CDOM
	'GYA_CODIGO'															, ; //X9_EXPDOM
	'GQ2_TIPDOC'															, ; //X9_EXPCDOM
	'S'																		, ; //X9_PROPRI
	'1'																		, ; //X9_LIGDOM
	'N'																		, ; //X9_LIGCDOM
	''																		, ; //X9_CONDSQL
	'S'																		, ; //X9_USEFIL
	'S'																		, ; //X9_VINFIL
	'S'																		, ; //X9_CHVFOR
	'S'																		} ) //X9_ENABLE

//
// Dom�nio SA1
//
aAdd( aSX9, { ;
	'SA1'																	, ; //X9_DOM
	'023'																	, ; //X9_IDENT
	'GQ2'																	, ; //X9_CDOM
	'A1_COD+A1_LOJA'														, ; //X9_EXPDOM
	'GQ2_CLIENT+GQ2_LOJA'													, ; //X9_EXPCDOM
	'S'																		, ; //X9_PROPRI
	'1'																		, ; //X9_LIGDOM
	'N'																		, ; //X9_LIGCDOM
	''																		, ; //X9_CONDSQL
	'S'																		, ; //X9_USEFIL
	'S'																		, ; //X9_VINFIL
	'S'																		, ; //X9_CHVFOR
	'S'																		} ) //X9_ENABLE

//
// Dom�nio ST9
//
aAdd( aSX9, { ;
	'ST9'																	, ; //X9_DOM
	'003'																	, ; //X9_IDENT
	'GZH'																	, ; //X9_CDOM
	'T9_CODBEM+T9_PLACA+T9_RENAVAM'											, ; //X9_EXPDOM
	'GZH_VEIC+GZH_PLACA+GZH_RENAVA'											, ; //X9_EXPCDOM
	'S'																		, ; //X9_PROPRI
	'1'																		, ; //X9_LIGDOM
	'1'																		, ; //X9_LIGCDOM
	''																		, ; //X9_CONDSQL
	'S'																		, ; //X9_USEFIL
	'S'																		, ; //X9_VINFIL
	'S'																		, ; //X9_CHVFOR
	'S'																		} ) //X9_ENABLE

//
// Dom�nio SX5
//
aAdd( aSX9, { ;
	'SX5'																	, ; //X9_DOM
	'009'																	, ; //X9_IDENT
	'GZH'																	, ; //X9_CDOM
	'X5_TABELA+X5_CHAVE'													, ; //X9_EXPDOM
	"'12'+GZH_UMUFIM"														, ; //X9_EXPCDOM
	'S'																		, ; //X9_PROPRI
	'1'																		, ; //X9_LIGDOM
	'1'																		, ; //X9_LIGCDOM
	''																		, ; //X9_CONDSQL
	'S'																		, ; //X9_USEFIL
	'S'																		, ; //X9_VINFIL
	'S'																		, ; //X9_CHVFOR
	'S'																		} ) //X9_ENABLE

//
// Atualizando dicion�rio
//
oProcess:SetRegua2( Len( aSX9 ) )

dbSelectArea( "SX9" )
dbSetOrder( 2 )

For nI := 1 To Len( aSX9 )

	If !SX9->( dbSeek( PadR( aSX9[nI][3], nTamSeek ) + PadR( aSX9[nI][1], nTamSeek ) ) )

		If !( aSX9[nI][1]+aSX9[nI][3] $ cAlias )
			cAlias += aSX9[nI][1]+aSX9[nI][3] + "/"
		EndIf

		RecLock( "SX9", .T. )
		For nJ := 1 To Len( aSX9[nI] )
			If FieldPos( aEstrut[nJ] ) > 0
				FieldPut( FieldPos( aEstrut[nJ] ), aSX9[nI][nJ] )
			EndIf
		Next nJ
		dbCommit()
		MsUnLock()

		AutoGrLog( "Foi inclu�do o relacionamento " + aSX9[nI][1] + "/" + aSX9[nI][3] )

		oProcess:IncRegua2( "Atualizando Arquivos (SX9)..." )

	EndIf

Next nI

AutoGrLog( CRLF + "Final da Atualiza��o" + " SX9" + CRLF + Replicate( "-", 128 ) + CRLF )

Return NIL


//--------------------------------------------------------------------
/*/{Protheus.doc} FSAtuHlp
Fun��o de processamento da grava��o dos Helps de Campos

@author TOTVS Protheus
@since  10/01/2020
@obs    Gerado por EXPORDIC - V.6.3.0.1 EFS / Upd. V.5.0.0 EFS
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function FSAtuHlp()
Local aHlpPor   := {}
Local aHlpEng   := {}
Local aHlpSpa   := {}

AutoGrLog( "�nicio da Atualiza��o" + " " + "Helps de Campos" + CRLF )


oProcess:IncRegua2( "Atualizando Helps de Campos ..." )

//
// Helps Tabela G53
//
//
// Helps Tabela GQ1
//
//
// Helps Tabela GQ2
//
//
// Helps Tabela GZH
//
AutoGrLog( CRLF + "Final da Atualiza��o" + " " + "Helps de Campos" + CRLF + Replicate( "-", 128 ) + CRLF )

Return {}


//--------------------------------------------------------------------
/*/{Protheus.doc} EscEmpresa
Fun��o gen�rica para escolha de Empresa, montada pelo SM0

@return aRet Vetor contendo as sele��es feitas.
             Se n�o for marcada nenhuma o vetor volta vazio

@author Ernani Forastieri
@since  27/09/2004
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function EscEmpresa()

//---------------------------------------------
// Par�metro  nTipo
// 1 - Monta com Todas Empresas/Filiais
// 2 - Monta s� com Empresas
// 3 - Monta s� com Filiais de uma Empresa
//
// Par�metro  aMarcadas
// Vetor com Empresas/Filiais pr� marcadas
//
// Par�metro  cEmpSel
// Empresa que ser� usada para montar sele��o
//---------------------------------------------
Local   aRet      := {}
Local   aSalvAmb  := GetArea()
Local   aSalvSM0  := {}
Local   aVetor    := {}
Local   cMascEmp  := "??"
Local   cVar      := ""
Local   lChk      := .F.
Local   lOk       := .F.
Local   lTeveMarc := .F.
Local   oNo       := LoadBitmap( GetResources(), "LBNO" )
Local   oOk       := LoadBitmap( GetResources(), "LBOK" )
Local   oDlg, oChkMar, oLbx, oMascEmp, oSay
Local   oButDMar, oButInv, oButMarc, oButOk, oButCanc

Local   aMarcadas := {}


If !MyOpenSm0(.F.)
	Return aRet
EndIf


dbSelectArea( "SM0" )
aSalvSM0 := SM0->( GetArea() )
dbSetOrder( 1 )
dbGoTop()

While !SM0->( EOF() )

	If aScan( aVetor, {|x| x[2] == SM0->M0_CODIGO} ) == 0
		aAdd(  aVetor, { aScan( aMarcadas, {|x| x[1] == SM0->M0_CODIGO .and. x[2] == SM0->M0_CODFIL} ) > 0, SM0->M0_CODIGO, SM0->M0_CODFIL, SM0->M0_NOME, SM0->M0_FILIAL } )
	EndIf

	dbSkip()
End

RestArea( aSalvSM0 )

Define MSDialog  oDlg Title "" From 0, 0 To 280, 395 Pixel

oDlg:cToolTip := "Tela para M�ltiplas Sele��es de Empresas/Filiais"

oDlg:cTitle   := "Selecione a(s) Empresa(s) para Atualiza��o"

@ 10, 10 Listbox  oLbx Var  cVar Fields Header " ", " ", "Empresa" Size 178, 095 Of oDlg Pixel
oLbx:SetArray(  aVetor )
oLbx:bLine := {|| {IIf( aVetor[oLbx:nAt, 1], oOk, oNo ), ;
aVetor[oLbx:nAt, 2], ;
aVetor[oLbx:nAt, 4]}}
oLbx:BlDblClick := { || aVetor[oLbx:nAt, 1] := !aVetor[oLbx:nAt, 1], VerTodos( aVetor, @lChk, oChkMar ), oChkMar:Refresh(), oLbx:Refresh()}
oLbx:cToolTip   :=  oDlg:cTitle
oLbx:lHScroll   := .F. // NoScroll

@ 112, 10 CheckBox oChkMar Var  lChk Prompt "Todos" Message "Marca / Desmarca"+ CRLF + "Todos" Size 40, 007 Pixel Of oDlg;
on Click MarcaTodos( lChk, @aVetor, oLbx )

// Marca/Desmarca por mascara
@ 113, 51 Say   oSay Prompt "Empresa" Size  40, 08 Of oDlg Pixel
@ 112, 80 MSGet oMascEmp Var  cMascEmp Size  05, 05 Pixel Picture "@!"  Valid (  cMascEmp := StrTran( cMascEmp, " ", "?" ), oMascEmp:Refresh(), .T. ) ;
Message "M�scara Empresa ( ?? )"  Of oDlg
oSay:cToolTip := oMascEmp:cToolTip

@ 128, 10 Button oButInv    Prompt "&Inverter"  Size 32, 12 Pixel Action ( InvSelecao( @aVetor, oLbx, @lChk, oChkMar ), VerTodos( aVetor, @lChk, oChkMar ) ) ;
Message "Inverter Sele��o" Of oDlg
oButInv:SetCss( CSSBOTAO )
@ 128, 50 Button oButMarc   Prompt "&Marcar"    Size 32, 12 Pixel Action ( MarcaMas( oLbx, aVetor, cMascEmp, .T. ), VerTodos( aVetor, @lChk, oChkMar ) ) ;
Message "Marcar usando" + CRLF + "m�scara ( ?? )"    Of oDlg
oButMarc:SetCss( CSSBOTAO )
@ 128, 80 Button oButDMar   Prompt "&Desmarcar" Size 32, 12 Pixel Action ( MarcaMas( oLbx, aVetor, cMascEmp, .F. ), VerTodos( aVetor, @lChk, oChkMar ) ) ;
Message "Desmarcar usando" + CRLF + "m�scara ( ?? )" Of oDlg
oButDMar:SetCss( CSSBOTAO )
@ 112, 157  Button oButOk   Prompt "Processar"  Size 32, 12 Pixel Action (  RetSelecao( @aRet, aVetor ), IIf( Len( aRet ) > 0, oDlg:End(), MsgStop( "Ao menos um grupo deve ser selecionado", "UPDEXP" ) ) ) ;
Message "Confirma a sele��o e efetua" + CRLF + "o processamento" Of oDlg
oButOk:SetCss( CSSBOTAO )
@ 128, 157  Button oButCanc Prompt "Cancelar"   Size 32, 12 Pixel Action ( IIf( lTeveMarc, aRet :=  aMarcadas, .T. ), oDlg:End() ) ;
Message "Cancela o processamento" + CRLF + "e abandona a aplica��o" Of oDlg
oButCanc:SetCss( CSSBOTAO )

Activate MSDialog  oDlg Center

RestArea( aSalvAmb )
dbSelectArea( "SM0" )
dbCloseArea()

Return  aRet


//--------------------------------------------------------------------
/*/{Protheus.doc} MarcaTodos
Fun��o auxiliar para marcar/desmarcar todos os �tens do ListBox ativo

@param lMarca  Cont�udo para marca .T./.F.
@param aVetor  Vetor do ListBox
@param oLbx    Objeto do ListBox

@author Ernani Forastieri
@since  27/09/2004
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function MarcaTodos( lMarca, aVetor, oLbx )
Local  nI := 0

For nI := 1 To Len( aVetor )
	aVetor[nI][1] := lMarca
Next nI

oLbx:Refresh()

Return NIL


//--------------------------------------------------------------------
/*/{Protheus.doc} InvSelecao
Fun��o auxiliar para inverter a sele��o do ListBox ativo

@param aVetor  Vetor do ListBox
@param oLbx    Objeto do ListBox

@author Ernani Forastieri
@since  27/09/2004
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function InvSelecao( aVetor, oLbx )
Local  nI := 0

For nI := 1 To Len( aVetor )
	aVetor[nI][1] := !aVetor[nI][1]
Next nI

oLbx:Refresh()

Return NIL


//--------------------------------------------------------------------
/*/{Protheus.doc} RetSelecao
Fun��o auxiliar que monta o retorno com as sele��es

@param aRet    Array que ter� o retorno das sele��es (� alterado internamente)
@param aVetor  Vetor do ListBox

@author Ernani Forastieri
@since  27/09/2004
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function RetSelecao( aRet, aVetor )
Local  nI    := 0

aRet := {}
For nI := 1 To Len( aVetor )
	If aVetor[nI][1]
		aAdd( aRet, { aVetor[nI][2] , aVetor[nI][3], aVetor[nI][2] +  aVetor[nI][3] } )
	EndIf
Next nI

Return NIL


//--------------------------------------------------------------------
/*/{Protheus.doc} MarcaMas
Fun��o para marcar/desmarcar usando m�scaras

@param oLbx     Objeto do ListBox
@param aVetor   Vetor do ListBox
@param cMascEmp Campo com a m�scara (???)
@param lMarDes  Marca a ser atribu�da .T./.F.

@author Ernani Forastieri
@since  27/09/2004
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function MarcaMas( oLbx, aVetor, cMascEmp, lMarDes )
Local cPos1 := SubStr( cMascEmp, 1, 1 )
Local cPos2 := SubStr( cMascEmp, 2, 1 )
Local nPos  := oLbx:nAt
Local nZ    := 0

For nZ := 1 To Len( aVetor )
	If cPos1 == "?" .or. SubStr( aVetor[nZ][2], 1, 1 ) == cPos1
		If cPos2 == "?" .or. SubStr( aVetor[nZ][2], 2, 1 ) == cPos2
			aVetor[nZ][1] := lMarDes
		EndIf
	EndIf
Next

oLbx:nAt := nPos
oLbx:Refresh()

Return NIL


//--------------------------------------------------------------------
/*/{Protheus.doc} VerTodos
Fun��o auxiliar para verificar se est�o todos marcados ou n�o

@param aVetor   Vetor do ListBox
@param lChk     Marca do CheckBox do marca todos (referncia)
@param oChkMar  Objeto de CheckBox do marca todos

@author Ernani Forastieri
@since  27/09/2004
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function VerTodos( aVetor, lChk, oChkMar )
Local lTTrue := .T.
Local nI     := 0

For nI := 1 To Len( aVetor )
	lTTrue := IIf( !aVetor[nI][1], .F., lTTrue )
Next nI

lChk := IIf( lTTrue, .T., .F. )
oChkMar:Refresh()

Return NIL


//--------------------------------------------------------------------
/*/{Protheus.doc} MyOpenSM0
Fun��o de processamento abertura do SM0 modo exclusivo

@author TOTVS Protheus
@since  10/01/2020
@obs    Gerado por EXPORDIC - V.6.3.0.1 EFS / Upd. V.5.0.0 EFS
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function MyOpenSM0(lShared)
Local lOpen := .F.
Local nLoop := 0

If FindFunction( "OpenSM0Excl" )
	For nLoop := 1 To 20
		If OpenSM0Excl(,.F.)
			lOpen := .T.
			Exit
		EndIf
		Sleep( 500 )
	Next nLoop
Else
	For nLoop := 1 To 20
		dbUseArea( .T., , "SIGAMAT.EMP", "SM0", lShared, .F. )

		If !Empty( Select( "SM0" ) )
			lOpen := .T.
			dbSetIndex( "SIGAMAT.IND" )
			Exit
		EndIf
		Sleep( 500 )
	Next nLoop
EndIf

If !lOpen
	MsgStop( "N�o foi poss�vel a abertura da tabela " + ;
	IIf( lShared, "de empresas (SM0).", "de empresas (SM0) de forma exclusiva." ), "ATEN��O" )
EndIf

Return lOpen


//--------------------------------------------------------------------
/*/{Protheus.doc} LeLog
Fun��o de leitura do LOG gerado com limitacao de string

@author TOTVS Protheus
@since  10/01/2020
@obs    Gerado por EXPORDIC - V.6.3.0.1 EFS / Upd. V.5.0.0 EFS
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function LeLog()
Local cRet  := ""
Local cFile := NomeAutoLog()
Local cAux  := ""

FT_FUSE( cFile )
FT_FGOTOP()

While !FT_FEOF()

	cAux := FT_FREADLN()

	If Len( cRet ) + Len( cAux ) < 1048000
		cRet += cAux + CRLF
	Else
		cRet += CRLF
		cRet += Replicate( "=" , 128 ) + CRLF
		cRet += "Tamanho de exibi��o maxima do LOG alcan�ado." + CRLF
		cRet += "LOG Completo no arquivo " + cFile + CRLF
		cRet += Replicate( "=" , 128 ) + CRLF
		Exit
	EndIf

	FT_FSKIP()
End

FT_FUSE()

Return cRet


/////////////////////////////////////////////////////////////////////////////
