#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"

Static _lExecJob   := .F.
Static _lLogOn     := .T.
Static _lQryAllFil := .F.
Static _cStartDate := Space(8)
Static _cEndDate   := Space(8)
Static _aLogSuc    := {}
Static _aLogFail   := {}

//-------------------------------------------------------------------
Static Function SchedDef()
    Local aParam := {}
    Local aOrd   := {}

    aParam := { "P"       ,; // Tipo R para relatorio P para processo
                "PARAMDEF",; // Pergunte do relatorio, caso nao use passar ParamDef
                ""        ,; // Alias
                aOrd      ,; // Array de ordens
              }

Return (aParam)

//-------------------------------------------------------------------------
User Function JLancRDM()
    Local oProccess := Nil
    Local lJob      := IsBlind()

    Local dDate1     := CtoD("  /  /    ")
    Local dDate2     := dDate1
    Local lLog       := .T.
    Local lAllFil    := .F.
    Local cEmpJob    := ""
    Local cFilJob    := ""
    Local cUserJob   := ""
    Local cPassJob   := ""

    If lJob // N�o tem interface
        Conout("Inicio via JOB")
        _lExecJob := .T.

        dDate1     := CtoD("19/08/2019")
        dDate2     := Date()
        lLog       := .T.
        lAllFil    := .F.
        cEmpJob    := "01"
        cFilJob    := "0101"
        cUserJob   := "ldesk"
        cPassJob   := "ldesk"

        If ValidPar(dDate1, dDate2, lLog, lAllFil, cEmpJob, cFilJob, cUserJob, cPassJob)
            LancProc()
        EndIf
    Else
        If GetParams() // Chama tela para preenchimento da data de refer�ncia
            oProccess := MsNewProcess():New({|| LancProc(@oProccess)}, "Aguarde", "Gerando lan�amentos...", .T.)
            oProccess:Activate()
        EndIf
    EndIf

    _lExecJob    := .F.
    _lLogOn      := .T.
    _lQryAllFil  := .F.
    _cStartDate  := ""
    _cEndDate    := ""

    If _lLogOn
        JurFreeArr(_aLogSuc)
        JurFreeArr(_aLogFail)
    EndIf

Return Nil

//-------------------------------------------------------------------------
Static Function GetParams()
    Local aParBox   := {}
    Local aRetPar   := {}
    Local lContinue := .F.
    Local dInit     := CtoD(Space(8))

    AAdd(aParBox, {1, "Data Inicial:" , dInit, "", "", "", "", 50, .T.})
    AAdd(aParBox, {1, "Data Final:  " , dInit, "", "", "", "", 50, .T.})
    AAdd(aParBox, {5, "Habilita Log?" , .T., 50, "", .F.})
    aAdd(aParBox, {5, "Todas Filiais?", .F., 50, "", .F.})
    
    lContinue := ParamBox(aParBox, "Data dos T�tulos", @aRetPar, {|| ValidPar(aRetPar[1], aRetPar[2], aRetPar[3], aRetPar[4],,,,,@aRetPar)},,,,,,, .T.)

    JurFreeArr(aRetPar)

Return (lContinue)

//-------------------------------------------------------------------------
Static Function ValidPar(dDate1, dDate2, lLog, lAllFil, cEmpJob, cFilJob, cUserJob, cPassJob, aRetPar)
    Local lValid := .F.

    Do Case
        Case Empty(dDate1)
            JurMsgErro("Data inicial vazia!", , "Informe a data inicial.")
        
        Case Empty(dDate2)
            JurMsgErro("Data final vazia!", , "Informe a data inicial.")
        
        Case ValType(dDate1) <> "D"
            JurMsgErro("Tipo: '" + Type("dDate1") + "' da data inicial inv�lido!", , "O valor deve ser passado como data 'D'.")

        Case ValType(dDate2) <> "D"
            JurMsgErro("Tipo: '" + Type("dDate2") + "' da data inicial inv�lido!", , "O valor deve ser passado como data 'D'.")

        Case dDate2 < dDate1
            JurMsgErro("A data inicial inv�lida!", , "A data inicial deve ser maior que a data final.")

        Case _lExecJob .And. !LoadEnv(cEmpJob, cFilJob, cUserJob, cPassJob)
            JurMsgErro("Falha ao montar ambiente!", , "Varifique os par�metros de empresa e filial.")

        Case !SuperGetMv("MV_JURXFIN", .F., .F.)
            JurMsgErro("Integra��o com financeiro desabilitado.", , "Verifique a configura��o do par�metro MV_JURXFIN.")

        Case lAllFil .And. !_lExecJob .And. !MsgYesNo("Tem certeza que deseja criar lan�amentos para todas filiais?")
            aRetPar[4] := .F.

        OtherWise
            _cStartDate := DtoS(dDate1)
            _cEndDate   := DtoS(dDate2)
            _lQryAllFil := lAllFil
            _lLogOn     := lLog
            lValid     := .T.
    End Case

Return (lValid)

//-------------------------------------------------------------------------
Static Function LoadEnv(cEmpJob, cFilJob, cUserJob, cPassJob)
    Local lLoadedEnv := .F.

    If Empty(CFILANT)
        RPCSetType(3)
        lLoadedEnv := RpcSetEnv(cEmpJob, cFilJob, cUserJob, cPassJob, "PFS")
    Else
        lLoadedEnv := .T.
    EndIf

Return (lLoadedEnv)

//-------------------------------------------------------------------------
Static Function LancProc(oProccess)
    Local cQuery := ""
    
    If !_lExecJob
        oProccess:SetRegua1(1)
        oProccess:IncRegua1("Gerando lan�amentos...")
    EndIf
    
    cQuery := QueryLanc()

    If TCGetDb() <> "ORACLE"
        cQuery := ChangeQuery(cQuery)
    EndIf

    aData := JurSql(cQuery, "*")

    If Empty(aData)
        JurMsgErro("N�o foram encontrado dados!", , "Verifique o per�odo digitado.")
    Else
        CreateLanc(oProccess, aData)
    EndIf

    JurFreeArr(aData)

Return Nil

//-------------------------------------------------------------------------
Static Function QueryLanc()
    Local cQryLanc := ""

   cQryLanc := "SELECT SE5.E5_FILIAL, SE2.R_E_C_N_O_ SE2REC, SE5.R_E_C_N_O_ SE5REC"
   cQryLanc +=  " FROM " + RetSqlName("SE5") + " SE5"
   cQryLanc += " INNER JOIN " + RetSQlName("SE2") + " SE2"
   cQryLanc +=    " ON SE2.E2_FILIAL = SE5.E5_FILIAL"
   cQryLanc +=   " AND SE2.E2_PREFIXO = SE5.E5_PREFIXO"
   cQryLanc +=   " AND SE2.E2_NUM = SE5.E5_NUMERO"
   cQryLanc +=   " AND SE2.E2_PARCELA = SE5.E5_PARCELA"
   cQryLanc +=   " AND SE2.E2_TIPO = SE5.E5_TIPO"
   cQryLanc +=   " AND SE2.E2_FORNECE = SE5.E5_CLIFOR"
   cQryLanc +=   " AND SE2.E2_LOJA = SE5.E5_LOJA"
   cQryLanc +=   " AND SE2.D_E_L_E_T_ = ' '"
   cQryLanc += " WHERE SE5.E5_RECPAG = 'P'" // Pagamento
   If !_lQryAllFil // N�o considera todas filiais
        cQryLanc +=   " AND SE5.E5_FILIAL = '" + xFilial("SE5") + "'"
    EndIf
   cQryLanc +=   " AND SE5.E5_TIPODOC <> 'ES'" // Diferente de Estorno
   cQryLanc +=   " AND SE5.E5_DTDIGIT BETWEEN '" + _cStartDate + "' AND '" + _cEndDate + "'"
   cQryLanc +=   " AND (SE5.E5_SITUACA = ' ' AND E5_DTCANBX = '       ')" // Baixas n�o canceladas
   cQryLanc +=   " AND SE5.E5_ARQCNAB <> ' '"  // Baixado via CNAB
   cQryLanc +=   " AND NOT EXISTS (SELECT OHB.R_E_C_N_O_ 
   cQryLanc +=                     " FROM " + RetSqlName("OHB") + " OHB"
   cQryLanc +=                    " WHERE OHB.OHB_FILIAL = SE5.E5_FILIAL" 
   cQryLanc +=                      " AND OHB.OHB_CPAGTO = SE5.E5_FILIAL || '|' || SE5.E5_PREFIXO || '|' || SE5.E5_NUMERO || '|' || SE5.E5_PARCELA || '|' || SE5.E5_TIPO || '|' || SE5.E5_CLIFOR || '|' || SE5.E5_LOJA"
   cQryLanc +=                      " AND OHB.D_E_L_E_T_ = ' ')"
   cQryLanc += " AND SE5.D_E_L_E_T_ = ' '
   cQryLanc += " ORDER BY SE5.E5_FILIAL, SE5.E5_SEQ DESC

Return (cQryLanc)

//-----------------------------------------------------
Static Function CreateLanc(oProccess, aData)
    Local aAreas   := {SE2->(GetArea(), SE5->(GetArea()), GetArea())}
    Local cBkpFil  := CFILANT
    Local cSetFil  := ""
    Local nLenData := Len(aData)
    Local nRecSE2  := 0
    Local nRecSE5  := 0
    Local nCount   := 0

    If !_lExecJob
        oProccess:SetRegua2(nLenData)
    EndIf

    SE2->(DbSetOrder(1)) // E2_FILIAL +
    SE5->(DbSetOrder(1)) // E5_FIILIAL +

    For nCount := 1 To nLenData
        If !_lExecJob
            oProccess:IncRegua2(i18n("Criando lan�amentos #1 de #2", {nCount, nLenData}))
        EndIf

        cSetFil := aData[nCount][1]
        nRecSE2 := aData[nCount][2]
        nRecSE5 := aData[nCount][3]

        If _lQryAllFil .And. cSetFil <> CFILANT
            CFILANT := cSetFil
        EndIf
        
        SE2->(DbGoTo(nRecSE2))
        SE5->(DbGoTo(nRecSE5))

        If SE2->(! EOF()) .And. SE5->(! EOF())
            lOk := JGrvBxPag(nRecSE2, 3, nRecSE5)
        EndIf

        If _lLogOn
            SetLog(lOk)
        EndIf
    Next nCount

    CFILANT := cBkpFil

    AEval(aAreas, {|aArea| RestArea(aArea)})

    If _lLogOn .And. (!Empty(_aLogSuc) .Or. !Empty(_aLogFail))
        SaveLog()
    EndIf

Return Nil

//---------------------------------------------------------------
Static Function SetLog(lOk)
    Local cTitulo := SE2->E2_FILIAL + '|' + SE2->E2_PREFIXO + '|' + SE2->E2_NUM + '|' + SE2->E2_PARCELA + '|'
          cTitulo += SE2->E2_TIPO + '|' + SE2->E2_FORNECE + '|' + SE2->E2_LOJA

    If lOk
        AAdd(_aLogSuc, "T�tulo: " + cTitulo)
    Else
        AAdd(_aLogFail, "T�tulo: " + cTitulo)
    EndIf
Return Nil

//-----------------------------------------------------------------
Static Function SaveLog()
    Local cMessage := ""
    
    If !Empty(_aLogSuc)
        cMessage += "T�tulos com sucesso:" + CRLF
        cMessage += Replicate("-", 80) + CRLF
        AEVal(_aLogSuc, {|cText| cMessage += cText + CRLF})
    EndIf

    If !Empty(_aLogFail)
        cMessage += CRLF + Replicate("=", 80)
        cMessage += CRLF + "T�tulos com falha:"
        AEVal(_aLogFail, {|cText| cMessage += cText + CRLF})
    EndIf

    If _lExecJob
        AutoGrLog(cMessage)
    Else
        JurLogMsg(cMessage)
    EndIf
Return Nil