#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'

Static _nTamEntSi := TamSX3("OHS_ENTSIS")[1]
Static _nTamCodSi := TamSX3("OHS_CODSIS")[1]
Static _nTamFilSi := TamSX3("OHS_FILSIS")[1]
Static _cFilOHS   := ""
Static _aBuffReg  := {} //Registro de buffer da funcao posicione

//-------------------------------------------------------------------
/*/{Protheus.doc} MigraPFS
Processa a migra��o SISJURI x SIGAPFS.

@param oProcess    Objeto do processa
@param aFilxEscr   Guarda o De-Para de sigla do escrit�rio x Filial do protheus
                   para realizar a migra��o dos escrit�rios do SisJuri
@param nConnSisJr  Conex�o do SisJuri
@param nConnProth  Conex�o do Protheus

@return cTempTotal, Tempo total da execu��o

@author Cristina Cintra / Jorge Martins
@since 20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
User Function MigraPFS(oProcess, aFilxEscr, nConnSisJr, nConnProth)
Local nI             := 0
Local nTotReg        := 0
Local dTimeInI       := Time()
Local dHoraInici     := dTimeInI
Local cTempTotal     := ""
Local cQuery         := ""
Local cTabProthe     := ""
Local cAlias         := GetNextAlias() //TODO: Validar migra��o com duas entidades.
Local cQryTot        := ""
Local aTabsDel       := {} // Tabelas que j� foram resetadas
Local aEntidades     := {}
Local aDePara        := {}

Local aTabLimpar     := {"OHS", "NVX", "NUX", "NUY", "SE4", "OHP", "NYY", "SM2", "SU5", "AGA", "AGB", "AC8",;
                         "NT0", "NUT", "NXP", "NTK", "NTJ", "NT1", "NTR", "NVN", "NW2", "NW3", "OHB", "SE1",;
                         "SE5", "SEV", "FIV", "FIW", "FK1", "FK5", "FK7", "FKG", "FKF", "CCF", "CTE", "CQD",;
                         "CTG", "NWF", "NX0", "NX1", "NS7", "OHJ", "NX2", "NX4", "OHN", "NXD", "NXE", "NRH",;
                         "NXF", "NXA", "NUF", "NUG", "NWZ", "FIL", "CTT"}

Local aAjuHist       := {{"NRE", "NU1", "NRE_COD"                             , "NU1_FILIAL + NU1_CTAB"} ,;
                         {"NRH", "NRM", "NRH_COD"                             , "NRM_FILIAL + NRM_CTIPO"},;
                         {"RD0", "NUS", "RD0_CODIGO"                          , "NUS_FILIAL + NUS_CPART"},;
                         {"RD0", "NVM", "RD0_CODIGO"                          , "NVM_FILIAL + NVM_CPART"},;
                         {"OH7", "OH8", "OH7_CODRAT + OH7_CODDET"             , "OH8_FILIAL + OH8_CODRAT + OH8_CODDET"},;
                         {"NRF", "NTV", "NRF_COD"                             , "NTV_FILIAL + NTV_CTAB"},;
                         {"SA1", "NUD", "A1_COD + A1_LOJA"                    , "NUD_FILIAL + NUD_CCLIEN + NUD_CLOJA"},;
                         {"NVE", "NUU", "NVE_CCLIEN + NVE_LCLIEN + NVE_NUMCAS", "NUU_FILIAL + NUU_CCLIEN + NUU_CLOJA  + NUU_CCASO"},;
                         {"NUK", "NVF", "NUK_CCLIEN + NUK_CLOJA  + NUK_NUMCAS", "NVF_FILIAL + NVF_CCLIEN + NVF_CLOJA  + NVF_NUMCAS"},;
                         {"NV1", "NUW", "NV1_CCLIEN + NV1_CLOJA + NV1_CCASO + NV1_CCAT", "NUW_FILIAL + NUW_CCLIEN + NUW_CLOJA  + NUW_CCASO + NUW_CCAT"},;
                         {"NV2", "NV0", "NV2_CCLIEN + NV2_CLOJA + NV2_CCASO + NV2_CPART", "NV0_FILIAL + NV0_CCLIEN + NV0_CLOJA  + NV0_CCASO + NV0_CPART"}}


Private lBloqueia501 := .F.      // Usada na carga inicial de Tp de Honor�rios
Private aGuardaInfo  := {}       // Usada no De-Para de algumas tabelas, para armazenar o c�digo sequencial
Private aCliente     := Array(2) // Usada para guardar o c�digo do Cliente e a Loja para saber a pr�xima loja
Private aFornece     := Array(2) // Usada para guardar o c�digo do Fornecedor e a Loja para saber a pr�xima loja

Conout("INICIO MIGRACAO: " + JurTimeStamp(1))

aEntidades := Entidades()                   // Entidades que ser�o migradas
oProcess:SetRegua1(Len(aEntidades) + 5 + 3) //5 do MigraPFS e 3 do MigraCtr

TCSetConn(nConnProth) // PROTHEUS
For nI := 1 To Len(aTabLimpar)
	// Limpar tabelas que n�o est�o no array aEntidades
	Chkfile(aTabLimpar[nI])
	LimpaTab(aTabLimpar[nI], @aTabsDel)
Next nI

_cFilOHS := xFilial("OHS")

For nI := 1 To Len(aEntidades)
	If ValType(aEntidades[nI]) == "B"
		EVal(aEntidades[nI])
	Else
		cTabProthe := aEntidades[nI][2]
		Conout("Tabela em processamento: " + cTabProthe + " - Inicio: " + JurTimeStamp(1))
		dTimeInI := Time()
		oProcess:IncRegua1(i18n("De #1 para #2.", {aEntidades[nI][1], cTabProthe} ))
		aDePara := MontaArray(cTabProthe)
		TCSetConn(nConnSisJr) // SISJURI

		cQuery := QryOrigem(aEntidades[nI], aDePara) // Query na Origem para pegar os registros a serem gravados
		MontaQuery(cQuery, @cAlias, @nTotReg)
		TCSetConn(nConnProth) // PROTHEUS
		If nTotReg > 0
			oProcess:SetRegua2(nTotReg)
			LimpaTab(cTabProthe, @aTabsDel) // Limpa os dados da tabela de destino
			GrvDestino(aDePara, oProcess, aFilxEscr, cAlias, nTotReg) // Grava��o no Destino
			Conout("Tabela em processamento: " + cTabProthe + " - Fim   : " + JurTimeStamp(1))
			Conout("Duracao: " + cTabProthe + " --> " + ElapTime( dTimeInI , Time()))
		EndIf
		If Select(cAlias) > 0
			(cAlias)->(DbCloseArea())
		EndIf
	EndIf
Next nI

If Select(cAlias) > 0
	(cAlias)->(DbCloseArea())
EndIf

oProcess:IncRegua1("Ajuste do Ano-M�s Final das tabelas de hist�rico")
AjuHist(oProcess, aAjuHist) // Fun��o para ajuste do Ano-M�s Final das tabelas de hist�rico

oProcess:IncRegua1("Ajuste no hist�rico das tabelas de servi�os")
InsereHist(oProcess) // Fun��o para ajuste no hist�rico das tabelas de servi�os

oProcess:IncRegua1("Carga inicial de Tipo de Honor�rios")
J037CgInit(.T., oProcess)  // Carga inicial de Tipo de Honor�rios

oProcess:IncRegua1("Carga do cadastro de Agrupamento")
JA118INIC(.T.)

JurFreeArr(aEntidades)
JurFreeArr(aAjuHist)
JurFreeArr(aDePara)
JurFreeArr(aGuardaInfo)
JurFreeArr(aFilxEscr)

cTempTotal := ElapTime(dHoraInici, TIME())

Return cTempTotal

//-------------------------------------------------------------------
/*/{Protheus.doc} Entidades
Monta o array com as entidades da Migra��o - respeitar as ordens de 
depend�ncia entre os cadastros.

@return aEntidades  Array com as tabelas de origem e destino que ser�o
                    migradas.

@author Cristina Cintra / Jorge Martins
@since 20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function Entidades()
Local aEntidades := {}
Local cCondicNRB := "TIPO = 'A'"
Local cCondicCTO := "TIPO = 'M'"
Local cCondicNVQ := "MODULO = 'FATURAMENTO_EMI' OR MODULO = 'FATURAMENTO_CAN'"
Local cCondicCTT := "TIPO = 'G'"
Local cCondicOH6 := "EXISTS (SELECT A.RATNCODIG FROM FINANCE.DETALHERATEIO A WHERE A.RATNCODIG = FINANCE.RATEIO.RATNCODIG)"
Local cOrderSA1  := "CLIENTE.COD_CLIENTE, ENDERECO.COD_ENDERECO"
Local cOrderSA2  := "FORNECEDOR.FORNCODIGO, FORNENDERECO.FORNENDCODIGO"
Local cOrderNU9  := "PARTCLI.COD_CLIENTE, ENDERECO.COD_ENDERECO"
Local cCondicNSD := "RCR.HONORARIO.VALOR_HORA <> SSJR.CAD_TABH_PROF.DTHNVALOR"
Local cCondicNTT := "SSJR.CAD_HONORARIO_HIST.VALOR_HORA <> SSJR.CAD_DETHONOR_HIST.DTHNVALOR"
Local cCondicNWF := "RCR.ADIANTAMENTO.ADICSTATUS	 = 'A' AND ADICOPERACAO = 'A' AND RCR.ADIANTAMENTO.ADINLANCAMENTO IS NOT NULL"
Local aJoinNS7   := {}
Local aJoinRD0   := {}
Local aJoinSA1   := {}
Local aJoinSA2   := {}
Local aJoinFil   := {{"INNER", "FINANCE.FORNENDERECO", {{"FINANCE.FORNCONTA.FORNCODIGO", "FINANCE.FORNENDERECO.FORNCODIGO"}}}}
Local aJoinSED   := {{"LEFT", "RCR.CONTAS", {{"RCR.CONTAS.PCTCNUMEROCONTA", "FINANCE.PLANOCONTAS.PCTCNUMEROCONTA"}}}}
Local aJoinNTP   := {{"INNER", "RCR.CLIENTE", {{"RCR.CLIENTE.COD_CLIENTE", "SSJR.CAD_LOCALIDADE.COD_CLIENTE"}}}}
Local aCondNSD   := {}
Local aJoinNSD   := {}
Local aCondNTT1  := {}
Local aCondNTT2  := {}
Local aJoinNTT   := {}
Local aJoinConv  := {{"INNER", "RCR.TABVAL_D", {{"TABVAL_D.COD_CONV","TABCONV.COD_CONV"}}}}
Local cCondicCTP := "TABCONV.TIPO = 'D' AND CODIGO = (SELECT VALOR FROM SSJR.CAD_PREFERENCIA WHERE NOME = 'MOEDAOFICIAL')"
Local cCondicNXQ := "TABCONV.TIPO = 'M' AND CODIGO = (SELECT VALOR FROM SSJR.CAD_PREFERENCIA WHERE NOME = 'MOEDAOFICIAL')"
Local aJoinNU9   := {}
Local aJoinNUA   := {}
Local cCondNUA   := "HABILITAR = 'N'"
Local aJoinNUB   := {}
Local aJoinNUC   := {}
Local aJoinNUD   := {}
Local aJoinNWF   := {}
Local aJoinNV4   := {}
Local aJoinNW4   := {}
Local aJoinNVV   := {}
Local aJoinNVW   := {}
Local aJoinNWD   := {}
Local aJoinNVY   := {}
Local aJoinNUE   := {}
Local aJoinNX0   := {}
Local aJoinNX1   := {}
Local aJoinNZQ   := {}
Local aJoinNWM   := {}
Local aJoinNX2   := {}
Local aJoinOHN   := {}
Local aJoinNXD   := {}
Local aJoinNXE   := {}
Local aJoinNXA   := {}
Local aJoinNUG   := {}
Local aJoinNWZ   := {}
Local aJoinOHH   := {}
Local aJoinOHB   := {}
Local cGroupNXE  := "rcr.TOTDESP.fatura,rcr.TOTDESP.orgncodigfat,rcr.TOTDESP.orgncodigdesp,rcr.TOTDESP.cliente,RCR.PASTA.COD_ENDERECO,rcr.TOTDESP.pasta,rcr.TOTDESP.tipodesp,rcr.TOTDESP.moeda"
Local cCondNW4   := "SSJR.LANCTABELADO_FATURA.FATURA IS NOT NULL OR SSJR.LANCTABELADO_FATURA.WONCODIGO IS NOT NULL"
Local cConNW4Pre := ""
Local cCondNWD   := "(SSJR.FATURAADICIONAL_FATURA.FATURA IS NOT NULL OR SSJR.FATURAADICIONAL_FATURA.WONCODIGO IS NOT NULL)"
Local cCondNVZ   := "(SSJR.DESPESA_FATURA.FATURA IS NOT NULL OR SSJR.DESPESA_FATURA.WONCODIGO IS NOT NULL)"
Local cConNVZPre := ""
Local aJoinNVZ   := {}
Local cCondNUE   := "(SSJR.TIME_SHEET_FATURA.FATURA IS NOT NULL OR SSJR.TIME_SHEET_FATURA.WONCODIGO IS NOT NULL)"
Local cConNUEPre := ""
Local aJoinNW0   := {}
Local cOrderOHH  := "finance.CONTASRECEBERHISTORICO.crccnumerodocumento,"+;
					"finance.CONTASRECEBERHISTORICO.crccparcela        ,"+;
					"finance.CONTASRECEBERHISTORICO.crcncliente"

cConNW4Pre += " RCR.LANCTABELADO.NUMEROPREFAT IS NOT NULL "
cConNW4Pre += " AND ((SSJR.LANCTABELADO_FATURA.FATURA IS NULL "
cConNW4Pre += " AND SSJR.LANCTABELADO_FATURA.WONCODIGO IS NULL "
cConNW4Pre += " AND SSJR.LANCTABELADO_FATURA.SITUACAO = 'N') "
cConNW4Pre += "  OR SSJR.LANCTABELADO_FATURA.COD_LANCTAB IS NULL) "

cConNVZPre += " RCR.DESPESA.NUMEROPREFAT IS NOT NULL "
cConNVZPre += " AND (( SSJR.DESPESA_FATURA.FATURA IS NULL "
cConNVZPre += " AND SSJR.DESPESA_FATURA.WONCODIGO IS NULL "
cConNVZPre += " AND SSJR.DESPESA_FATURA.SITUACAO = 'N') "
cConNVZPre += "  OR SSJR.DESPESA_FATURA.COD_DESPESA IS NULL) "

cConNUEPre += " RCR.TIME_SHEET.NUMEROPREFAT IS NOT NULL "
cConNUEPre += " AND (( SSJR.TIME_SHEET_FATURA.FATURA IS NULL "
cConNUEPre += " AND SSJR.TIME_SHEET_FATURA.WONCODIGO IS NULL "
cConNUEPre += " AND SSJR.TIME_SHEET_FATURA.SITUACAO = 'N') "
cConNUEPre += "  OR SSJR.TIME_SHEET_FATURA.COD_TIMESHEET IS NULL) "

aAdd(aJoinNS7, {"LEFT", "RCR.ENDEMPRESA", {{"RCR.ESCRITORIO.ORGNCODIG", "RCR.ENDEMPRESA.ORGNCODIG"}}})
aAdd(aJoinNS7, {"LEFT", "RCR.ESTADO", {{"RCR.ENDEMPRESA.ESTNCODIGO", "RCR.ESTADO.ESTNCODIGO"}}})

aAdd(aJoinSA1, {"INNER", "RCR.ENDERECO", {{"RCR.CLIENTE.COD_CLIENTE", "RCR.ENDERECO.COD_CLIENTE"}}})
aAdd(aJoinSA1, {"LEFT", "RCR.ESTADO", {{"RCR.ENDERECO.ESTNCODIGO", "RCR.ESTADO.ESTNCODIGO"}}})
aAdd(aJoinSA1, {"LEFT", "RCR.CIDADE", {{"RCR.ENDERECO.CIDNCODIGO", "RCR.CIDADE.CIDNCODIGO"}}})

aAdd(aJoinNU9, {"INNER", "RCR.ENDERECO", {{"RCR.PARTCLI.COD_CLIENTE", "RCR.ENDERECO.COD_CLIENTE"}}})

aAdd(aJoinNUD, {"INNER", "RCR.PARTCLI", {{"RCR.PARTCLI.COD_CLIENTE", "SSJR.CAD_PARTCLI_HIST.COD_CLIENTE"}}})
aAdd(aJoinNUD, {"INNER", "RCR.ENDERECO", {{"SSJR.CAD_PARTCLI_HIST.COD_CLIENTE", "RCR.ENDERECO.COD_CLIENTE"}}})

aAdd(aJoinNUA, {"INNER", "RCR.ENDERECO", {{"RCR.ENDERECO.COD_CLIENTE", "RCR.TIPORELATCLI.COD_CLIENTE"}}})
aAdd(aJoinNUB, {"INNER", "RCR.ENDERECO", {{"RCR.ENDERECO.COD_CLIENTE", "SSJR.CAD_CLI_TIPOATIV.COD_CLIENTE"}}})
aAdd(aJoinNUC, {"INNER", "RCR.ENDERECO", {{"RCR.ENDERECO.COD_CLIENTE", "SSJR.CAD_CLI_TIPODESP.COD_CLIENTE"}}})

aAdd(aJoinSA2, {"INNER", "FINANCE.FORNENDERECO", {{"FINANCE.FORNECEDOR.FORNCODIGO", "FINANCE.FORNENDERECO.FORNCODIGO"}}})
aAdd(aJoinSA2, {"LEFT", "RCR.ESTADO", {{"FINANCE.FORNENDERECO.ESTNCODIGO", "RCR.ESTADO.ESTNCODIGO"}}})
aAdd(aJoinSA2, {"LEFT", "RCR.CIDADE", {{"FINANCE.FORNENDERECO.CIDNCODIGO", "RCR.CIDADE.CIDNCODIGO"}}})

aAdd(aJoinRD0, {"LEFT", "RCR.ESTADO", {{"RCR.ADVOGADO.ESTADO", "RCR.ESTADO.ESTNCODIGO"}}})
aAdd(aJoinRD0, {"LEFT", "RCR.CIDADE", {{"RCR.ADVOGADO.CIDADE", "RCR.CIDADE.CIDNCODIGO"}}})

aAdd(aCondNSD, {"SSJR.CAD_TABH_PROF.DTHCTABELA"   , "RCR.HONORARIO.TABELA"})
aAdd(aCondNSD, {"SSJR.CAD_TABH_PROF.DTHCMOEDA"    , "RCR.HONORARIO.MOEDA"})
aAdd(aCondNSD, {"SSJR.CAD_TABH_PROF.DTHCCATEGORIA", "RCR.HONORARIO.CATEG_ADVG"})
aAdd(aJoinNSD, {"INNER", "RCR.HONORARIO", aCondNSD})

aAdd(aCondNTT1, {"SSJR.CAD_DETHONOR_HIST.DTHCTABELA"   , "RCR.HONORARIO.TABELA"})
aAdd(aCondNTT1, {"SSJR.CAD_DETHONOR_HIST.DTHCMOEDA"    , "RCR.HONORARIO.MOEDA"})
aAdd(aCondNTT1, {"SSJR.CAD_DETHONOR_HIST.DTHCCATEGORIA", "RCR.HONORARIO.CATEG_ADVG"})
aAdd(aJoinNTT , {"INNER", "RCR.HONORARIO", aCondNTT1})
aAdd(aCondNTT2, {"SSJR.CAD_DETHONOR_HIST.DTHCTABELA"   , "SSJR.CAD_HONORARIO_HIST.TABELA"})
aAdd(aCondNTT2, {"SSJR.CAD_DETHONOR_HIST.DTHCMOEDA"    , "SSJR.CAD_HONORARIO_HIST.MOEDA"})
aAdd(aCondNTT2, {"SSJR.CAD_DETHONOR_HIST.DTHCCATEGORIA", "SSJR.CAD_HONORARIO_HIST.CATEG_ADVG"})
aAdd(aCondNTT2, {"SSJR.CAD_DETHONOR_HIST.ANO_MES"      , "SSJR.CAD_HONORARIO_HIST.ANO_MES"})
aAdd(aJoinNTT,  {"INNER", "SSJR.CAD_HONORARIO_HIST", aCondNTT2})

aAdd(aJoinNWF, {"LEFT", "RCR.MOEDA"         , {{"RCR.ADIANTAMENTO.ADICMOEDA"            , "RCR.MOEDA.CODIGO"                                                                                     }}})
aAdd(aJoinNWF, {"LEFT", "FINANCE.LANCAMENTO", {{"RCR.ADIANTAMENTO.ADINLANCAMENTO"       , "FINANCE.LANCAMENTO.LANNCODIG"                                                                         }}})
aAdd(aJoinNWF, {"LEFT", "RCR.CONTAS"        , {{"FINANCE.LANCAMENTO.PCTCNUMEROCONTADEST", "RCR.CONTAS.PCTCNUMEROCONTA"                                                                           }}})
aAdd(aJoinNWF, {"LEFT", "RCR.PASTA"         , {{"RCR.ADIANTAMENTO.ADINPASTA"            , "RCR.PASTA.PASTA"              },{"RCR.ADIANTAMENTO.ADINCLIENTE"          , "RCR.PASTA.COD_CLIENTE"    }}})
aAdd(aJoinNWF, {"LEFT", "RCR.ESCRITORIO"    , {{"RCR.ADIANTAMENTO.ADINORGANIZACAO"      , "RCR.ESCRITORIO.ORGNCODIG"                                                                             }}})
aAdd(aJoinNWF, {"LEFT", "RCR.ENDERECO"      , {{"RCR.ADIANTAMENTO.ADINCLIENTE"          , "RCR.ENDERECO.COD_CLIENTE"     },{"RCR.PASTA.COD_ENDERECO"                , "RCR.ENDERECO.COD_ENDERECO"}}})
aAdd(aJoinNWF, {"LEFT", "RCR.CLIENTE"       , {{"RCR.ADIANTAMENTO.ADINCLIENTE"          , "RCR.CLIENTE.COD_CLIENTE"                                                                              }}})

aAdd(aJoinNV4, {"INNER", "RCR.PASTA"         , {{"RCR.LANCTABELADO.PASTA_REAL"           , "RCR.PASTA.PASTA"              },{"RCR.LANCTABELADO.COD_CLIENTE"          , "RCR.PASTA.COD_CLIENTE"    }}})
aAdd(aJoinNV4, {"INNER", "RCR.CLIENTE"       , {{"RCR.LANCTABELADO.COD_CLIENTE"          , "RCR.CLIENTE.COD_CLIENTE"                                                                              }}})
aAdd(aJoinNV4, {"INNER", "RCR.ENDERECO"      , {{"RCR.LANCTABELADO.COD_CLIENTE"          , "RCR.ENDERECO.COD_CLIENTE"     },{"RCR.PASTA.COD_ENDERECO"                , "RCR.ENDERECO.COD_ENDERECO"}}})

aAdd(aJoinNW4, {"INNER", "RCR.PASTA"                , {{"RCR.LANCTABELADO.PASTA_REAL"  , "RCR.PASTA.PASTA"                      },{"RCR.LANCTABELADO.COD_CLIENTE"          , "RCR.PASTA.COD_CLIENTE"    }}})
aAdd(aJoinNW4, {"INNER", "RCR.CLIENTE"              , {{"RCR.LANCTABELADO.COD_CLIENTE" , "RCR.CLIENTE.COD_CLIENTE"                                                                                      }}})
aAdd(aJoinNW4, {"INNER", "RCR.ENDERECO"             , {{"RCR.LANCTABELADO.COD_CLIENTE" , "RCR.ENDERECO.COD_CLIENTE"             },{"RCR.PASTA.COD_ENDERECO"                , "RCR.ENDERECO.COD_ENDERECO"}}})
aAdd(aJoinNW4, {"LEFT" , "SSJR.LANCTABELADO_FATURA" , {{"RCR.LANCTABELADO.ID_LANCTAB"  , "SSJR.LANCTABELADO_FATURA.COD_LANCTAB"                                                                         }}})

aAdd(aJoinNVZ, {"INNER", "RCR.PASTA"            , {{"RCR.DESPESA.PASTA_REAL"  , "RCR.PASTA.PASTA"                      },{"RCR.DESPESA.COD_CLIENTE", "RCR.PASTA.COD_CLIENTE"    }}})
aAdd(aJoinNVZ, {"INNER", "RCR.CLIENTE"          , {{"RCR.DESPESA.COD_CLIENTE" , "RCR.CLIENTE.COD_CLIENTE"                                                                       }}})
aAdd(aJoinNVZ, {"INNER", "RCR.ENDERECO"         , {{"RCR.DESPESA.COD_CLIENTE" , "RCR.ENDERECO.COD_CLIENTE"             },{"RCR.PASTA.COD_ENDERECO" , "RCR.ENDERECO.COD_ENDERECO"}}})
aAdd(aJoinNVZ, {"LEFT" , "SSJR.DESPESA_FATURA"  , {{"RCR.DESPESA.CODIGO"      , "SSJR.DESPESA_FATURA.COD_DESPESA"                                                               }}})

aAdd(aJoinNVV, {"LEFT", "RCR.CLIENTE"  , {{"RCR.FATURAADICIONAL.FADNCLIENTE" , "RCR.CLIENTE.COD_CLIENTE"  }                                                                   }})
aAdd(aJoinNVV, {"LEFT", "RCR.PASTA"    , {{"RCR.FATURAADICIONAL.FADNCLIENTE" , "RCR.PASTA.COD_CLIENTE"    },{"RCR.FATURAADICIONAL.FADNCASOFAT"  ,"RCR.PASTA.PASTA"           }}})

aAdd(aJoinNVW, {"LEFT", "RCR.CLIENTE"  , {{"RCR.FATURAADICIONALPASTA.FAPNCLIENTE" , "RCR.CLIENTE.COD_CLIENTE"  }                                                                   }})
aAdd(aJoinNVW, {"LEFT", "RCR.PASTA"    , {{"RCR.FATURAADICIONALPASTA.FAPNCLIENTE" , "RCR.PASTA.COD_CLIENTE"        },{"RCR.FATURAADICIONALPASTA.FAPNPASTA"    ,"RCR.PASTA.PASTA"           }}})

aAdd(aJoinNWD, {"LEFT", "SSJR.FATURAADICIONAL_FATURA", {{"RCR.FATURAADICIONAL.FADNCLIENTE" , "SSJR.FATURAADICIONAL_FATURA.COD_CLIENTE"},;
                                                        {"RCR.FATURAADICIONAL.FADNPARCELA" , "SSJR.FATURAADICIONAL_FATURA.PARCELA"    }}})

aAdd(aJoinNVY, {"LEFT", "RCR.PASTA"             , {{"RCR.DESPESA.COD_CLIENTE"               ,"RCR.PASTA.COD_CLIENTE"         },{"RCR.DESPESA.PASTA_REAL"                ,"RCR.PASTA.PASTA"           }}})
aAdd(aJoinNVY, {"LEFT", "RCR.CLIENTE"           , {{"RCR.DESPESA.COD_CLIENTE"               ,"RCR.CLIENTE.COD_CLIENTE"                                                                               }}})

aAdd(aJoinNUE, {"LEFT", "RCR.CLIENTE"           , {{"RCR.TIME_SHEET.COD_CLIENTE"        ,"RCR.CLIENTE.COD_CLIENTE"                                                                                        }}})
aAdd(aJoinNUE, {"LEFT", "SSJR.EBILL_EMPRESA"    , {{"RCR.CLIENTE.ID_EMPRESA"            ,"SSJR.EBILL_EMPRESA.ID_EMPRESA"                                                                                  }}})
aAdd(aJoinNUE, {"LEFT", "SSJR.EBILL_TPATV_RELAC", {{"RCR.TIME_SHEET.CODIGO"             ,"SSJR.EBILL_TPATV_RELAC.TIPOATIV"},{"SSJR.EBILL_EMPRESA.ID_DOCPADRAO"      ,"SSJR.EBILL_TPATV_RELAC.ID_DOCPADRAO"}}})

aAdd(aJoinNW0, {"LEFT", "SSJR.TIME_SHEET_FATURA", {{"RCR.TIME_SHEET.NUMERO", "SSJR.TIME_SHEET_FATURA.COD_TIMESHEET"}}})

aAdd(aJoinNX0, {"LEFT", "RCR.CLIENTE"           , {{"RCR.PRE_FATURA.COD_CLIENTE"            ,"RCR.CLIENTE.COD_CLIENTE"                                                                               }}})

aAdd(aJoinNX1, {"LEFT", "RCR.PASTA"             , {{"RCR.PRE_FATPASTA.CLIENTE"              ,"RCR.PASTA.COD_CLIENTE"         },{"RCR.PRE_FATPASTA.PASTA"                ,"RCR.PASTA.PASTA"           }}})

aAdd(aJoinNZQ, {"LEFT", "RCR.PASTA"             , {{"SSJR.TS_DESPESA.COD_CLIENTE"           ,"RCR.PASTA.COD_CLIENTE"         },{"SSJR.TS_DESPESA.PASTA"                 ,"RCR.PASTA.PASTA"           }}})

aAdd(aJoinNWM, {"LEFT", "RCR.CLIENTE"           , {{"SSJR.CADLANCTABLOTE.ltlcod_cliente"    ,"RCR.CLIENTE.COD_CLIENTE"                                                                               }}})

aAdd(aJoinNX2, {"LEFT", "RCR.PASTA"             , {{"RCR.PRE_TOTADV.COD_CLIENTE"            ,"RCR.PASTA.COD_CLIENTE"         },{"RCR.PRE_TOTADV.PASTA"                  , "RCR.PASTA.PASTA"          }}})
aAdd(aJoinNX2, {"LEFT", "RCR.CLIENTE"           , {{"RCR.PRE_TOTADV.COD_CLIENTE"            ,"RCR.CLIENTE.COD_CLIENTE"                                                                               }}})

aAdd(aJoinOHN, {"LEFT", "RCR.PASTA"             , {{"RCR.PRE_FATPASTA.CLIENTE"              ,"RCR.PASTA.COD_CLIENTE"         },{"RCR.PRE_FATPASTA.PASTA"                , "RCR.PASTA.PASTA"          }}})
aAdd(aJoinNXD, {"LEFT", "RCR.PASTA"             , {{"rcr.TOTADV.cod_cliente"                ,"RCR.PASTA.COD_CLIENTE"         },{"rcr.TOTADV.pasta"                      , "RCR.PASTA.PASTA"          }}})

aAdd(aJoinNXE, {"LEFT", "RCR.PASTA"             , {{"rcr.TOTDESP.cliente"               ,"RCR.PASTA.COD_CLIENTE"         },{"rcr.TOTDESP.PASTA"                     , "RCR.PASTA.PASTA"              }}})
aAdd(aJoinNXA, {"LEFT", "rcr.FATURA_SUBSTITUIDA", {{"rcr.fatura.numero"                 ,"rcr.FATURA_SUBSTITUIDA.FATURA"                                                                             }}})
aAdd(aJoinNUG, {"LEFT", "RCR.PASTA"             , {{"rcr.WOPASTA.fwoncliente"           ,"RCR.PASTA.COD_CLIENTE"         },{"rcr.WOPASTA.fwonpasta"                 , "RCR.PASTA.PASTA"              }}})
aAdd(aJoinNWZ, {"LEFT", "RCR.PASTA"             , {{"rcr.TOTDESPWO.cliente"             ,"RCR.PASTA.COD_CLIENTE"         },{"rcr.TOTDESPWO.pasta"                   , "RCR.PASTA.PASTA"              }}})
aAdd(aJoinOHH, {"LEFT", "RCR.FATURA"            , {{"finance.CONTASRECEBERHISTORICO.crcnfatura","RCR.FATURA.NUMERO"},{"finance.CONTASRECEBERHISTORICO.orgncodig"    ,"RCR.FATURA.ORGNCODIG"          }}})

aAdd(aJoinOHB, {"INNER", "RCR.ESCRITORIO"       , {{"FINANCE.LANCAMENTO.EMPNCOD"        ,"RCR.ESCRITORIO.EMPNCOD"                                                                                    }}})
aAdd(aJoinOHB, {"LEFT" , "RCR.DESPESA"          , {{"FINANCE.LANCAMENTO.LANNCODIG"      ,"RCR.DESPESA.LANNCODIGSA"                                                                                   }}})

aAdd( aEntidades, {|| LoadMunici(oProcess, nConnSisJr, nConnProth)                                } ) // Munic�pio
aAdd( aEntidades, {|| LoadPais(oProcess, nConnSisJr, nConnProth)                                  } ) // Pa�s
aAdd( aEntidades, {|| GrvCondPag(oProcess, nConnSisJr, nConnProth)                                } ) // Condi��o de pagamento
aAdd( aEntidades, {|| GrvContato(oProcess, nConnSisJr, nConnProth)                                } ) // Contato
aAdd( aEntidades, { 'RCR.ESCRITORIO'                , 'NS7'    , ''        , aJoinNS7 , ''        } ) // Escrit�rio
aAdd( aEntidades, { 'RCR.TIPODESP'                  , 'NRH'    , ''        , {}       , ''        } ) // Tipo de Despesa
aAdd( aEntidades, { 'SSJR.CAD_TPDESC_ESCRITORIO'    , 'OHJ'    , ''        , {}       , ''        } ) // Exce��o Tipo de Despesa
aAdd( aEntidades, { 'SSJR.CAD_IDIOMA'               , 'NR1'    , ''        , {}       , ''        } ) // Idioma
aAdd( aEntidades, { 'RCR.TIPOTABELA'                , 'NRK'    , ''        , {}       , ''        } ) // Tipo da Tabela de Servi�os
aAdd( aEntidades, { 'RCR.ITEMTABELADO'              , 'NRD'    , ''        , {}       , ''        } ) // Servi�o Tabelado
aAdd( aEntidades, { 'SSJR.CAD_IDIOMA_ITEMTAB'       , 'NR3'    , ''        , {}       , ''        } ) // Desc Item tabelado por Idioma 
aAdd( aEntidades, { 'RCR.MOTIVOCANCELAMENTO'        , 'NSA'    , ''        , {}       , ''        } ) // Motivo de Cancelamento de Fatura
aAdd( aEntidades, { 'SSJR.CAD_RETIFICACAO'          , 'NSB'    , ''        , {}       , ''        } ) // Retifica��o de TS
aAdd( aEntidades, { 'SSJR.CAD_SITUACAOCOBPREFAT'    , 'NSC'    , ''        , {}       , ''        } ) // Situa��o de Cobran�a / Tipo de Retorno
aAdd( aEntidades, { 'RCR.ATIVIDADE'                 , 'NRC'    , ''        , {}       , ''        } ) // Tipo de Atividade
aAdd( aEntidades, { 'SSJR.CAD_TIPOPRESTCONTAS'      , 'NUO'    , ''        , {}       , ''        } ) // Tipo de Presta��o de Contas
aAdd( aEntidades, { 'RCR.TIPOORIGINACAO'            , 'NRI'    , ''        , {}       , ''        } ) // Tipo de Origina��o
aAdd( aEntidades, { 'SSJR.CAD_TIPOEVENTO'           , 'NR9'    , ''        , {}       , ''        } ) // Tipo de Fatura
aAdd( aEntidades, { 'RCR.TIPOCARTACOB'              , 'NRG'    , ''        , {}       , ''        } ) // Tipo de Carta de Cobran�a
aAdd( aEntidades, { 'RCR.AREA'                      , 'NRB'    , cCondicNRB, {}       , ''        } ) // �rea Jur�dica
aAdd( aEntidades, { 'SSJR.CAD_SUBAREA'              , 'NRL'    , ''        , {}       , ''        } ) // Sub�rea Jur�dica
aAdd( aEntidades, { 'RCR.MOEDA'                     , 'CTO'    , cCondicCTO, {}       , ''        } ) // Moeda Cont�bil
aAdd( aEntidades, { 'SSJR.CAD_FECHAMENTO'           , 'NVQ'    , cCondicNVQ, {}       , ''        } ) // Fechamento de Per�odo
aAdd( aEntidades, { 'RCR.TABELADO'                  , 'NRE'    , ''        , {}       , ''        } ) // Tabela de Servi�os
aAdd( aEntidades, { 'RCR.DETALHETABELADO'           , 'NS5'    , ''        , {}       , ''        } ) // Detalhe da Tabela de Servi�os
aAdd( aEntidades, { 'SSJR.CAD_TABELADO_HIST'        , 'NU1'    , ''        , {}       , ''        } ) // Hist�rico da Tabela de Servi�os
aAdd( aEntidades, { 'SSJR.CAD_DETALHETABELADO_HIST' , 'NTS'    , ''        , {}       , ''        } ) // Hist�rico Valores dos Servi�os
aAdd( aEntidades, { 'RCR.CATADVG'                   , 'NRN'    , ''        , {}       , ''        } ) // Categoria dos Participantes
aAdd( aEntidades, { 'SSJR.CAD_IDIOMA_CATADVG'       , 'NR2'    , ''        , {}       , ''        } ) // Descri��o Categoria por Idioma
aAdd( aEntidades, { 'SSJR.EBILL_DOCPADRAO'          , 'NRW'    , ''        , {}       , ''        } ) // Documento Padr�o E-billing
aAdd( aEntidades, { 'SSJR.EBILL_CATEG'              , 'NRV'    , ''        , {}       , ''        } ) // Categoria E-Billing
aAdd( aEntidades, { 'SSJR.EBILL_TPCATEG_RELAC'      , 'NS2'    , ''        , {}       , ''        } ) // Categoria E-Billing (De-Para)
aAdd( aEntidades, { 'SSJR.EBILL_FASE'               , 'NRY'    , ''        , {}       , ''        } ) // Fase E-Billing
aAdd( aEntidades, { 'SSJR.EBILL_TAREFA'             , 'NRZ'    , ''        , {}       , ''        } ) // Tarefa E-Billing (De-Para)
aAdd( aEntidades, { 'SSJR.EBILL_TPATV'              , 'NS0'    , ''        , {}       , ''        } ) // Tipo de Atividade E-Billing
aAdd( aEntidades, { 'SSJR.EBILL_TPATV_RELAC'        , 'NS1'    , ''        , {}       , ''        } ) // Tipo de Atividade E-Billing (De-Para)
aAdd( aEntidades, { 'SSJR.EBILL_TPDESP'             , 'NS3'    , ''        , {}       , ''        } ) // Tipo de Despesa E-Billing
aAdd( aEntidades, { 'SSJR.EBILL_TPDESP_RELAC'       , 'NS4'    , ''        , {}       , ''        } ) // Tipo de Despesa E-Billing (De-Para)
aAdd( aEntidades, { 'SSJR.CAD_SERVIDOR_EMAIL'       , 'NR7'    , ''        , {}       , ''        } ) // Conf. Servidores de E-mail
aAdd( aEntidades, { 'SSJR.CAD_SERVIDOR_USUARIOS'    , 'NR8'    , ''        , {}       , ''        } ) // Conf. Usu�rio Envio de Email
aAdd( aEntidades, { 'SSJR.CAD_CONFIG_FATURA_EMAIL'  , 'NRU'    , ''        , {}       , ''        } ) // Config. de Envio de E-mail
aAdd( aEntidades, { 'SSJR.CAD_IDIOMA_TPATV'         , 'NR5'    , ''        , {}       , ''        } ) // Desc Tp Atividade por Idioma
aAdd( aEntidades, { 'SSJR.CAD_IDIOMA_TIPODESP'      , 'NR4'    , ''        , {}       , ''        } ) // Desc Tp Desp por Idioma
aAdd( aEntidades, { 'SSJR.CAD_TIPODESP_HIST'        , 'NRM'    , ''        , {}       , ''        } ) // Hist�rico de Tipo de Despesas
aAdd( aEntidades, { 'RCR.TIPORELAT'                 , 'NRJ'    , ''        , {}       , ''        } ) // Tp de Relat�rio de Faturamento
aAdd( aEntidades, { 'SSJR.CAD_TIPOPROTOCOLO'        , 'NSO'    , ''        , {}       , ''        } ) // Tp Protocolo de Faturas
aAdd( aEntidades, { 'RCR.TITULO'                    , 'OH2'    , ''        , {}       , ''        } ) // Sugest�o de T�tulo
aAdd( aEntidades, { 'RCR.CONTAS'                    , 'SA6'    , ''        , {}       , ''        } ) // Bancos
aAdd( aEntidades, { 'FINANCE.FORNECEDOR'            , 'SA2'    , ''        , aJoinSA2 , cOrderSA2 } ) // Fornecedor
aAdd( aEntidades, { 'FINANCE.FORNCONTA'             , 'FIL|SA2', ''        , aJoinFil , '',.t.    } ) // Bancos do Fornecedor
aAdd( aEntidades, { 'RCR.ADVOGADO'                  , 'RD0|NUR', ''        , aJoinRD0 , ''        } ) // Participante
aAdd( aEntidades, { || CriaRD0MIG()                                                               } ) // Cria o participante MIGRADOR vinculado ao ADMIN devido as valida��es
aAdd( aEntidades, { || CriaFornec(oProcess, nConnSisJr, nConnProth)                               } ) // Cria Fornecedor do participante
aAdd( aEntidades, { 'RCR.AUSENCIA'                  , 'NTX'    , ''        , {}       , ''        } ) // Aus�ncias Participante
aAdd( aEntidades, { 'SSJR.CAD_ADVOGADO_HIST'        , 'NUS'    , ''        , {}       , ''        } ) // Hist�rico Participante
aAdd( aEntidades, { 'RCR.ADVGCONTACORRENTE'         , 'FIL|RD0', ''        , {}       , ''        } ) // Bancos do Participante
aAdd( aEntidades, { 'RCR.SETOR'                     , 'CTT', cCondicCTT    , {}       , ''        } ) // Centro de Custos
aAdd( aEntidades, { || CriaCTTSint()                                                              } ) // Cria centro de custo sint�tico e vincula aos filhos
aAdd( aEntidades, { 'RCR.GERENC_RATEIOADVGSETOR'    , 'NSS'    , ''        , {}      , ''         } ) // Rateio Centros Custo Participante
aAdd( aEntidades, { 'SSJR.CAD_RATEIOADVG_HIST'      , 'NVM'    , ''        , {}      , ''         } ) // Hist. Rateio Centros Custo Partic.
aAdd( aEntidades, { 'SSJR.CAD_GRUPO_LIDER'          , 'FLP', ''            , {}       , ''        } ) // Aprovador por Centro de Custo 
aAdd( aEntidades, { 'SSJR.CAD_GRUPO_RESPONSAVEL'    , 'OHE', ''            , {}       , ''        } ) // Respons�veis X C.Custo
aAdd( aEntidades, { 'RCR.CALENDARIO'                , 'NW9'    , ''        , {}       , ''        } ) // Feriado
aAdd( aEntidades, { 'SSJR.CAD_RAMAL_PROF'           , 'NR6'    , ''        , {}       , ''        } ) // Ramal por Profissional
aAdd( aEntidades, { 'SSJR.CAD_PROJETO'              , 'OHL'    , ''        , {}       , ''        } ) // Projetos/Finalidades
aAdd( aEntidades, { 'SSJR.CAD_PROJETO_ITEM'         , 'OHM'    , ''        , {}       , ''        } ) // Itens de Projeto
aAdd( aEntidades, { 'SSJR.EBILL_EMPRESA'            , 'NRX'    , ''        , {}       , ''        } ) // Empresa E-billing
aAdd( aEntidades, { 'SSJR.EBILL_CODESCRITORIO'      , 'NTQ'    , ''        , {}       , ''        } ) // Escrit�rio E-billing
aAdd( aEntidades, { 'RCR.CONTASESCRITORIO'          , 'OHK'    , ''        , {}       , ''        } ) // Banco x Escrit�rios
aAdd( aEntidades, { 'FINANCE.RATEIO'                , 'OH6'    , cCondicOH6, {}       , ''        } ) // Cabe�alho da tabela de Rateio
aAdd( aEntidades, { 'FINANCE.DETALHERATEIO'         , 'OH7'    , ''        , {}       , ''        } ) // Detalhe do Rateio
aAdd( aEntidades, { 'SSJR.CAD_DETALHERATEIO_HIST'   , 'OH8'    , ''        , {}       , ''        } ) // Hist�rico do Detalhe do Rateio
aAdd( aEntidades, { 'FINANCE.PLANOCONTAS'           , 'SED'    , ''        , aJoinSED , ''        } ) // Naturezas
aAdd( aEntidades, { 'FINANCE.HISTORICOPADRAO'       , 'OHA'    , ''        , {}       , ''        } ) // Hist�rico Padr�o
aAdd( aEntidades, { 'FINANCE.EVENTO'                , 'OHC'    , ''        , {}       , ''        } ) // Eventos Financeiros
aAdd( aEntidades, { 'SSJR.CAD_TABELA_PADRAO_HIST'   , 'NVP'    , ''        , {}       , ''        } ) // Hist. Tab Hon Padr�o
aAdd( aEntidades, { 'RCR.GRUPOEMP'                  , 'ACY'    , ''        , {}       , ''        } ) // Grupo de Clientes
aAdd( aEntidades, { 'RCR.TABHONOR'                  , 'NRF'    , ''        , {}       , ''        } ) // Tabela de Honor�rios
aAdd( aEntidades, { 'RCR.HONORARIO'                 , 'NS9'    , ''        , {}       , ''        } ) // Honor�rios - Categoria
aAdd( aEntidades, { 'SSJR.CAD_TABH_PROF'            , 'NSD'    , cCondicNSD, aJoinNSD , ''        } ) // Honor�rios - Profissional
aAdd( aEntidades, { 'SSJR.CAD_TABHONOR_HIST'        , 'NTV'    , ''        , {}       , ''        } ) // Hist. Tabela de Honor�rios
aAdd( aEntidades, { 'SSJR.CAD_HONORARIO_HIST'       , 'NTU'    , ''        , {}       , ''        } ) // Hist. Honor�rios - Categoria
aAdd( aEntidades, { 'SSJR.CAD_DETHONOR_HIST'        , 'NTT'    , cCondicNTT, aJoinNTT , ''        } ) // Hist. Honor�rios - Profissional
aAdd( aEntidades, { 'RCR.MOEDAESCR'                 , 'NTN'    , ''        , {}       , ''        } ) // Moedas Bloqueadas para fatura
aAdd( aEntidades, { 'RCR.TABCONV'                   , 'CTP'    , cCondicCTP, aJoinConv, ''        } ) // Cota��es
aAdd( aEntidades, { 'RCR.TABCONV'                   , 'NXQ'    , cCondicNXQ, aJoinConv, ''        } ) // Cota��o Mensal
aAdd( aEntidades, { 'SSJR.FAT_EXCESSAO_NUMFAT'      , 'NSE'    , ''        , {}       , ''        } ) // Exc. Numera��os Fat
aAdd( aEntidades, { 'SYNC.EQUITRACCONFIG'           , 'NYT'    , ''        , {}       , ''        } ) // Equitrac - Configura��o Geral
aAdd( aEntidades, { 'SYNC.EQUITRACDESPESA'          , 'NYV'    , ''        , {}       , ''        } ) // Tarifador - Config Despesas
aAdd( aEntidades, { 'SYNC.EQUITRACCONFIG_ARQUIVO'   , 'NYU'    , ''        , {}       , ''        } ) // Equitrac - Config. Arquivos
aAdd( aEntidades, { 'SYNC.EXT2BCS_KEY'              , 'NYW'    , ''        , {}       , ''        } ) // Equitrac - Arquivos
aAdd( aEntidades, { 'SYNC.EXT2BCS'                  , 'NYX'    , ''        , {}       , ''        } ) // Equitrac - Arquivos Detalhes
aAdd( aEntidades, { 'RCR.CLIENTE'                   , 'SA1|NUH', ''        , aJoinSA1 , cOrderSA1 } ) // Cliente / Complemente de Cliente
aAdd( aEntidades, { 'RCR.PARTCLI'                   , 'NU9'    , ''        , aJoinNU9 , cOrderNU9 } ) // Participa��o de Cliente
aAdd( aEntidades, { 'SSJR.CAD_PARTCLI_HIST'         , 'NUD'    , ''        , aJoinNUD , ""  , .T. } ) // Hist�rico de Participa��o de Cliente
aAdd( aEntidades, { 'RCR.TIPORELATCLI'              , 'NUA'    , cCondNUA  , aJoinNUA ,           } ) // Tipo de relat�rios do cliente
aAdd( aEntidades, { 'SSJR.CAD_CLI_TIPOATIV'         , 'NUB'    , ''        , aJoinNUB ,           } ) // Tipos de Atividades N�o Cobr�veis de Clientes
aAdd( aEntidades, { 'SSJR.CAD_CLI_TIPODESP'         , 'NUC'    , ''        , aJoinNUC ,           } ) // Tipos de Despesas N�o Cobr�veis de Clientes
aAdd( aEntidades, { 'RCR.PASTA'                     , 'NVE'    , ''        , {}       , ''        } ) // Caso
aAdd( aEntidades, { 'SSJR.CAD_PASTA_HIST'           , 'NUU'    , ''        , {}       , ''        } ) // Hist�rico do Caso
aAdd( aEntidades, { 'RCR.PARTPAS'                   , 'NUK'    , ''        , {}       , ''        } ) // Participa��o do Caso
aAdd( aEntidades, { 'SSJR.CAD_PARTPAS_HIST'         , 'NVF'    , ''        , {}       , ''        } ) // Hist�rico de Participa��o do Caso
aAdd( aEntidades, { 'SSJR.FAT_EXCHONCATEG'          , 'NV1'    , ''        , {}       , ''        } ) // Exce��o da Tab Honor por Categoria no Caso
aAdd( aEntidades, { 'SSJR.CAD_EXCHONCATEG_HIST'     , 'NUW'    , ''        , {}       , ''        } ) // Hist�rico da Exce��o da Tab Honor por Categoria no Caso
aAdd( aEntidades, { 'SSJR.FAT_EXCHONPROF'           , 'NV2'    , ''        , {}       , ''        } ) // Exce��o da Tab Honor por Participante no Caso
aAdd( aEntidades, { 'SSJR.CAD_EXCHONPROF_HIST'      , 'NV0'    , ''        , {}       , ''        } ) // Hist�rico da Exce��o da Tab Honor por Participante no Caso
aAdd( aEntidades, { 'RCR.DETALHECONDEXITO'          , 'NWL'    , ''        , {}       , ''        } ) // Condi��o de �xito no Caso
aAdd( aEntidades, {|| U_MigraCtr(oProcess, nConnSisJr, nConnProth)                                } ) // Contrato
aAdd( aEntidades, {|| LoadClNat(oProcess, nConnSisJr, nConnProth)                                 } ) // Classifica��o de Naturezas
aAdd( aEntidades, {|| LoadSX6(oProcess, nConnSisJr, nConnProth)                                   } ) // Prefer�ncias
aAdd( aEntidades, { 'SSJR.CAD_LOCALIDADE'           , 'NTP'    , ''         , aJoinNTP , ''       } ) // Localidades
aAdd( aEntidades, { 'SSJR.CADLANCTABLOTE'           , 'NWM'    , ''         , aJoinNWM , ''       } ) // LAN�AMENTO TABELADO EM LOTE
aAdd( aEntidades, { 'SSJR.LANCTABLOTE_CASO'         , 'NWN'    , ''         ,          , ''       } ) // CASOS VINCULADOS AO LANC TAB
aAdd( aEntidades, { 'SSJR.FAT_PROTOCOLO'            , 'NXH'    , ''         , {}       , ''       } ) // Protocolo
aAdd( aEntidades, { 'SSJR.FAT_PROTOCOLOFATURA'      , 'NXI'    , ''         , {}       , ''       } ) // Faturas do Protocolo
aAdd( aEntidades, { 'SSJR.FAT_PROTOCOLOVIA'         , 'NXJ'    , ''         , {}       , ''       } ) // Vias do Protocolo
aAdd( aEntidades, { 'RCR.ADIANTAMENTO'              , 'NWF'    , cCondicNWF , aJoinNWF , ''       } ) // Controle de Adiantamentos
aAdd( aEntidades, {|| AdtCriaTit(oProcess)                                                        } ) // Cria t�tulos dos adiantametnos
aAdd( aEntidades, {|| AdtBaixa(oProcess)                                                          } ) // Efetua a devolu��o dos adiantametnos
aAdd( aEntidades, { 'RCR.LANCTABELADO'               , 'NV4'    , ''         , aJoinNV4 , ''      } ) // Lan�amento Tabelado
aAdd( aEntidades, { 'RCR.LANCTABELADO'               , 'NW4'    , cCondNW4   , aJoinNW4 , ''      } ) // Lan�amento Tabelado Fatu (Fatura e WO)
aAdd( aEntidades, { 'RCR.LANCTABELADO'               , 'NW4_PRE', cConNW4Pre , aJoinNW4 , ''      } ) // Lan�amento Tabelado Fatu (Pr�-fatura)
aAdd( aEntidades, { 'RCR.FATURAADICIONAL'            , 'NVV'    , ''         , aJoinNVV , ''      } ) // Fatura Adicional
aAdd( aEntidades, { 'RCR.FATURAADICIONALPASTA'       , 'NVW'    , ''         , aJoinNVW , ''      } ) // Casos da Fatura Adicional
aAdd( aEntidades, { 'RCR.FATURAADICIONAL'            , 'NWD'    , cCondNWD   , aJoinNWD , ''      } ) // Fatura Adicional Faturamento
aAdd( aEntidades, {|| EncaFatAdc(oProcess, nConnSisJr, nConnProth)                                } ) // Encaminhamentos da Adicional Faturamento
aAdd( aEntidades, { 'RCR.DESPESA'                    , 'NVY'    , ''         , aJoinNVY , ''      } ) // Despesas
aAdd( aEntidades, { 'RCR.DESPESA'                    , 'NVZ'    , cCondNVZ   , aJoinNVZ , '', .T. } ) // Despesa Faturamento (Fatura e WO)
aAdd( aEntidades, { 'RCR.DESPESA'                    , 'NVZ_PRE', cConNVZPre , aJoinNVZ , '', .T. } ) // Despesa Faturamento (Pr�-fatura)
aAdd( aEntidades, { 'SSJR.TS_DESPESA'                , 'NZQ'    , ''         , aJoinNZQ , ''      } ) // Solicita��o/Aprova��o Despesa
aAdd( aEntidades, { 'RCR.TIME_SHEET'                 , 'NUE'    , ''         , aJoinNUE , ''      } ) // Lan�amento Time Sheet
aAdd( aEntidades, { 'RCR.TIME_SHEET'                 , 'NW0'    , cCondNUE   , aJoinNW0 , '', .T. } ) // Time Sheet Faturamento (Fatura e WO)
aAdd( aEntidades, { 'RCR.TIME_SHEET'                 , 'NW0_PRE', cConNUEPre , aJoinNW0 , '', .T. } ) // Time Sheet Faturamento (Pr�-fatura)
aAdd( aEntidades, { 'RCR.PRE_FATURA'                 , 'NX0'    , ''         , aJoinNX0 , ''      } ) // Pr�-fatura
aAdd( aEntidades, { 'RCR.PRE_FATPASTA'               , 'NX1'    , ''         , aJoinNX1 , ''      } ) // Pre-fatura Caso
aAdd( aEntidades, { 'RCR.PRE_TOTADV'                 , 'NX2'    , ''         , aJoinNX2, ''       } ) //Pr�-fatura Participante
aAdd( aEntidades, { 'SSJR.FAT_PREFATURA_COBRANCA_TMP', 'NX4'    , ''         , {}      , ''       } ) // Historico de Cobranca
aAdd( aEntidades, { 'RCR.PRE_FATPASTA'               , 'OHN'    , ''         , aJoinOHN, ''       } ) // S�cios / Revisores
aAdd( aEntidades, { 'RCR.TOTADV'                     , 'NXD'    , ''         , aJoinNXD, ''       } ) // Participantes Da Fatura
aAdd( aEntidades, { 'RCR.TOTDESP'                    , 'NXE'    , ''         , aJoinNXE, '' , .F., cGroupNXE} ) // Resumo de despesas Da Fatura
aAdd( aEntidades, { 'SSJR.FAT_CAMBIOFATURA'          , 'NXF'    , ''         ,         , ''                 } ) // CAMBIOS UTILIZADOS NA FATURA
aAdd( aEntidades, { 'RCR.FATURA'                     , 'NXA'    , ''         ,         , ''                 } ) // Fatura
aAdd( aEntidades, { 'RCR.WO'                         , 'NUF'    , ''         ,         , ''                 } ) // WO
aAdd( aEntidades, { 'RCR.WOPASTA'                    , 'NUG'    , ''         ,aJoinNUG , ''                 } ) // WO Casos
aAdd( aEntidades, { 'RCR.TOTDESPWO'                  , 'NWZ'    , ''         ,aJoinNWZ , ''                 } ) // RESUMO WO DE DESPESAS
// JAX (Aguardar melhoria de performance) aAdd( aEntidades, {|| U_MigraCReceb(oProcess, nConnSisJr, nConnProth)                                       } ) // Contas a Receber
aAdd( aEntidades, { 'FINANCE.CONTASRECEBERHISTORICO'  , 'OHH'    , ''         ,aJoinOHH , cOrderOHH         } ) // Posi��o Hist�rica Ctas Receber
aAdd( aEntidades, {|| MigraCtb(oProcess, nConnSisJr, nConnProth)                                            } ) // Contabilidade
// JAX (Aguardar fonte do Leonardo) aAdd( aEntidades, {|| U_MigraCPagar(oProcess, nConnSisJr, nConnProth)                                       } ) // Contas a Pagar
aAdd( aEntidades, {|| U_MigraNota(oProcess, nConnSisJr, nConnProth)                                         } ) // Nota Fiscal de Saida
aAdd( aEntidades, { 'FINANCE.LANCAMENTO'  , 'OHB'    , " FINANCE.LANCAMENTO.LANCORIGEM = 'D'"         ,aJoinOHB}) 

Return aEntidades

//-------------------------------------------------------------------
/*/{Protheus.doc} MontaArray
Monta o array com as estruturas de De-Para.

@param cTab  Tabela para montagem

@author Cristina Cintra / Jorge Martins
@since 20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MontaArray(cTab)
Local aDePara     := {}
Local nTamFilOHB  := TamSX3("OHB_FILIAL")[1] // Tamanho do Campo de Filial da OHB
Local cTamOHBCod  := cValToChar(TamSX3("OHB_CODIGO")[1]) // Tamanho do C�digo do Lan�amento entre Naturezas
Local cTamEvento  := cValToChar(TamSX3("OHC_COD")[1]) // Tamanho do C�digo do Evento Financeiro
Local cTamProjet  := cValToChar(TamSX3("OHL_CPROJE")[1]) // Tamanho do C�digo do Projeto
Local cTamItemPr  := cValToChar(TamSX3("OHM_ITEM")[1]) // Tamanho do C�digo do Item do Projeto
Local cTamClient  := cValToChar(TamSX3("A1_COD")[1]) // Tamanho do C�digo do Cliente
Local cTamLoja    := cValToChar(TamSX3("A1_LOJA")[1]) // Tamanho do C�digo da Loja
Local cTamCaso    := cValToChar(TamSX3("NVE_NUMCAS")[1]) // Tamanho do C�digo do Caso
Local cTamTpDesp  := cValToChar(TamSX3("NRH_COD")[1]) // Tamanho do C�digo do Tipo de Despesa
Local cTamCodDsp  := cValToChar(TamSX3("NVY_COD")[1]) // Tamanho do C�digo da Despesa
Local cTamHistPa  := cValToChar(TamSX3("OHA_COD")[1]) // Tamanho do C�digo do Hist�rico Padr�o
Local cTamDesdob  := cValToChar(TamSX3("OHF_CITEM")[1]) // Tamanho do C�digo do Item de Desdobramento
Local cTamDesdPg  := cValToChar(TamSX3("OHG_CITEM")[1]) // Tamanho do C�digo do Item de Desdobraento P�s Pagamento
Local cTamCodTs   := cValToChar(TamSX3("NUE_COD")[1]) // Tamanho do C�digo do Timesheet
Local cTamHoraL   := cValToChar(TamSX3('NUE_HORAL')[1]) // Tamanho do Campo de Hora Lan�ado do Timesheet
Local bMigraEscr  := {|| MigraDePar('RCR.ESCRITORIO'         , xValSisJur, '', 'ORGNCODIG'                                            , cTab, aRegs[nI][1]                                                            , 'NS7_COD'                                            )}
Local bMigraPart  := {|| MigraDePar('RCR.ADVOGADO'           , xValSisJur, '', 'COD_ADVG'                                             , cTab, aRegs[nI][1]                                                            , 'RD0_CODIGO'                                         )}
Local bMigrClien  := {|| MigraDePar('RCR.ENDERECO'           , xValSisJur, '', 'COD_CLIENTE|COD_ENDERECO'                             , cTab, aCliente[1] + aCliente[2]                                               , 'A1_COD+A1_LOJA'                                     )}
Local bMigraForn  := {|| MigraDePar('FINANCE.FORNECEDOR'     , xValSisJur, '', 'FORNCODIGO|FORNENDCODIGO'                             , cTab, aFornece[1] + aFornece[2]                                               , 'A2_COD+A2_LOJA'                                     )}
Local bMigraBank  := {|| MigraDePar('RCR.CONTAS'             , xValSisJur, '', 'CTANCODIG'                                            , cTab, aRegs[nI][1] + aRegs[nI][2] + aRegs[nI][3]                              , 'A6_COD+A6_AGENCIA+A6_NUMCON'                        )}
Local bMigraRate  := {|| MigraDePar('FINANCE.RATEIO'         , xValSisJur, '', 'RATNCODIG'                                            , cTab, aRegs[nI][1]                                                            , 'OH6_CODIGO'                                         )}
Local bMigraHist  := {|| MigraDePar('FINANCE.HISTORICOPADRAO', xValSisJur, '', 'HPANCODIGO'                                           , cTab, aRegs[nI][1]                                                            , 'OHA_COD'                                            )}
Local bMigraNU9   := {|| MigraDePar('RCR.PARTCLI'            , xValSisJur, '', 'COD_CLIENTE|COD_ENDERECO|COD_ADVG|CODTIPOORIG|ANO_MES', cTab, aRegs[nI][1] + aRegs[nI][2] + aRegs[nI][3] + aRegs[nI][4] + aRegs[nI][5], 'NUD_CCLIEN+NUD_CLOJA+NUD_CPART+NUD_CTPORI+NUD_DTINI')}
Local bMigraCaso  := {|| MigraDePar('RCR.PASTA'              , xValSisJur, '', 'COD_CLIENTE|PASTA'                                    , cTab, aRegs[nI][1] + aRegs[nI][2] + aRegs[nI][3]                              , 'NVE_CCLIEN+NVE_LCLIEN+NVE_NUMCAS'                   )}
Local bMigraNRH   := {|| MigraDePar('RCR.TIPODESP'           , xValSisJur, '', 'TIPO'                                                 , cTab, aRegs[nI][1]                                                            , 'NRH_COD'                                            )}
Local bMigraNRC   := {|| MigraDePar('RCR.ATIVIDADE'          , xValSisJur, '', 'CODIGO'                                               , cTab, aRegs[nI][1]                                                            , 'NRC_COD'                                            )}
Local bGetTpAtiv  := &('{|| Iif(!Empty(xValSisJur), Substr(GetDePara("RCR.ATIVIDADE", xValSisJur, ""), 1,' + cValToChar( TamSX3("NRC_COD")[1] ) + '), "")}')
Local bMigraNRJ   := {|| MigraDePar('RCR.TIPORELAT'          , xValSisJur, '', 'CODIGO'                                               , cTab, aRegs[nI][1]                                                            , 'NRJ_COD'                                            )}
Local bMigraNRI   := {|| MigraDePar('RCR.TIPOORIGINACAO'     , xValSisJur, '', 'CODIGO'                                               , cTab, aRegs[nI][1]                                                            , 'NRI_COD'                                            )}
Local bConvCIC    := &("{|| Substr(StrTran(StrTran(StrTran(xValSisJur, '.', ''), '-', ''), '/', ''), 1, "+ cValToChar(TamSX3("RD0_CIC")[1]) + ")}")
Local bConvCGC    := &("{|| Substr(StrTran(StrTran(StrTran(xValSisJur, '.', ''), '-', ''), '/', ''), 1, "+ cValToChar(TamSX3("A2_CGC")[1])+ ")}")
Local cCpoCliEnd  := "ENDERECO.COD_CLIENTE||'|'||ENDERECO.COD_ENDERECO"
Local cCpoCasClE  := "PASTA.COD_CLIENTE||'|'||PASTA.COD_ENDERECO"
Local cCpoCliECas := "PASTA.COD_CLIENTE||'|'||PASTA.PASTA"
Local cCpoCliCas  := "COD_CLIENTE||'|'||PASTA"
Local cCpoNWLCas  := "DETALHECONDEXITO.DTCNCLIENTE||'|'||DETALHECONDEXITO.DTCNPASTA"
Local cCpoForEnd  := "FORNENDERECO.FORNCODIGO||'|'||FORNENDERECO.FORNENDCODIGO"
Local bGetCodFor  := &( '{|| Substr(GetDePara("FINANCE.FORNECEDOR", xValSisJur, ""), 1,' + cValToChar(TamSX3("A2_COD")[1])+ ')}')
Local bGetCodLoj  := &('{|| Substr(GetDePara("FINANCE.FORNECEDOR", xValSisJur, ""),' + cValToChar(TamSX3("A2_COD")[1] + 1) + ',' + cValToChar( TamSX3("A2_LOJA")[1]) + ')}')
Local bGetSA1Cli  := &('{|| Iif(!Empty(xValSisJur), StrZero(xValSisJur,'+ cValTochar( TamSX3("A1_COD")[1]) + '), "")}')
Local bGetSA1Loj  := &('{|| Iif(!Empty(xValSisJur), Substr(GetDePara("RCR.ENDERECO", xValSisJur, ""), ' + cValTochar( TamSX3("A1_COD")[1] + 1) + ', '+ cValTochar( TamSX3("A1_LOJA")[1]) + '), "")}')
Local bGetCaso    := &('{|| Iif(!Empty(xValSisJur), StrZero(xValSisJur,' + cValToChar(TamSX3("NVE_NUMCAS")[1]) + '), "")}')
Local bGetLojCas  := & ( '{|| Substr(GetDePara("RCR.PASTA", xValSisJur, ""),' + cValToChar( TamSX3("A1_COD")[1] + 1 ) + ', ' +  cValToChar(TamSX3("A1_LOJA")[1])+ ')}')
Local bGetContr   := &('{|| Substr(GetDePara("PASTA.CONTRATO", xValSisJur, ""), 1, ' + cValToChar(TamSX3("NT0_COD")[1] ) +  ')}')
Local cCriaConta  := "CASE WHEN(EXISTS(SELECT A.ADVCCODADVG FROM RCR.ADVGCONTACORRENTE A WHERE A.ADVCCODADVG = RCR.ADVOGADO.COD_ADVG )) THEN 'S' ELSE CRIARCONTA END"
Local bMigraMoed  := {|| MigraDePar('RCR.MOEDA', xValSisJur, '', 'CODIGO', cTab, aRegs[nI][1], 'CTO_COD')}
Local bMigraCusto := {|| MigraDePar('RCR.SETOR', xValSisJur, '', 'SIGLA', cTab, aRegs[nI][5], 'CTT_CUSTO')}
Local cCodMoeda   := "CASE WHEN CODIGO = (SELECT VALOR FROM SSJR.CAD_PREFERENCIA WHERE NOME = 'MOEDAOFICIAL') THEN 'NAC01' ELSE CODIGO END"
Local cNivelNat   := "CASE WHEN (SELECT MAX(PCTNNIVEL) FROM FINANCE.PLANOCONTAS) = PCTNNIVEL THEN '2' ELSE '1' END"
Local bGetNatPai  := {|| Iif(Substr(StrTran(xValSisJur, ".", ""), 1, TamSX3("ED_PAI")[1]) == aRegs[nI][1], "", Substr(StrTran(xValSisJur, ".", ""), 1, TamSX3("ED_PAI")[1]))}
Local bGetCdBank  := {|| Substr(GetDePara("RCR.CONTAS", xValSisJur, ""), 1, TamSX3("A6_COD")[1])}
Local bGetCdAgen  := {|| Substr(GetDePara("RCR.CONTAS", xValSisJur, ""), TamSX3("A6_COD")[1] + 1, TamSX3("A6_AGENCIA")[1])}
Local bGetCdCont  := {|| Substr(GetDePara("RCR.CONTAS", xValSisJur, ""), TamSX3("A6_COD")[1] + TamSX3("A6_AGENCIA")[1] + 1, TamSX3("A2_NUMCON")[1])}
Local bGetMoeda   := &('{|| Substr(GetDePara("RCR.MOEDA", xValSisJur, ""), 1, ' + cValToChar(TamSX3("CTO_MOEDA")[1]) + ')}')
Local bGetForPag  := {|| Substr(GetDePara("SSJR.CAD_CLI_DIASVENC", xValSisJur, ""), 1, TamSX3("E4_CODIGO")[1])}
Local bGetMoeFin  := {|| Val(GetDePara("RCR.MOEDA", xValSisJur, ""))}
Local bGetTabRat  := {|| Substr(GetDePara("FINANCE.RATEIO", xValSisJur, ""), 1, TamSX3("OH6_CODIGO")[1])}
Local bGrvConCli  := {|| GrvContCli("RCR.ENDERECO-(CONTATOS)", xValSisJur, "")}
Local bGetContat  := {|| Substr(GetDePara("RCR.ENDERECO-(CONTATOS)", xValSisJur, ""), 1, TamSX3("U5_CODCONT")[1])}
Local bGetCondCs  := {|| Substr(GetDePara("SSJR.CAD_CAS_DIASVENC", xValSisJur, ""), 1, TamSX3("E4_CODIGO")[1])}
Local bGetCodNU9  := {|| Substr(GetDePara("RCR.PARTCLI", xValSisJur, ""), 1, TamSX3("NUD_COD")[1])}
Local bTpOrigina  := {|| StrZero(xValSisJur, TamSX3("NRI_COD")[1])}
Local bTpAtivid   := {|| StrZero(xValSisJur, TamSX3("NRC_COD")[1])}
Local bTpRptFat   := {|| StrZero(xValSisJur, TamSX3("NRJ_COD")[1])}
Local bTpFatura   := {|| StrZero(xValSisJur, TamSX3("NR9_COD")[1])}
Local bCartaCob   := {|| StrZero(xValSisJur, TamSX3("NRG_COD")[1])}
Local bGetSubArea := {|| IIF(ValType(xValSisJur) == "N" .And. xValSisJur > 0, AllTrim(Str(xValSisJur)), "")}
Local aOpcTpCont  := {{"B", "1"}, {"C", "2"}, {"E", "3"}, {"I", "4"},{"L", "5"}, {"O", "6"}, {"P", "7"}, {"S", "8"}}
Local bGetEscrt   := {|| Substr( GetDePara("RCR.ESCRITORIO", xValSisJur, ""), 1, TamSX3("NS7_COD")[1])}
Local bGetPart    := {|| GetDePara("RCR.ADVOGADO", xValSisJur, "")}
Local bTipoDesp   := {|| Iif(!Empty(xValSisJur), StrZero(Val(cValToChar(xValSisJur)), TamSX3("NRH_COD")[1]), '')}
Local bNatureza   := {|| Substr(StrTran(xValSisJur, ".", ""), 1, TamSX3("ED_CODIGO")[1])}
Local cCpoNYW     := "KEYDDATA||'|'||KEYCTIPO||'|'||KEYCARQ"
Local bMigraNYW   := {|| MigraDePar('SYNC.EXT2BCS_KEY', xValSisJur, '', 'KEYDDATA|KEYCTIPO|KEYCARQ', cTab, aRegs[nI][1], 'NYW_COD', .T.)}
Local cGetNYX     := "EXTDDATA||'|'||EXTCTIPO||'|'||EXTCARQ"
Local bGetNYW     := {|| Substr(GetDePara('SYNC.EXT2BCS_KEY', xValSisJur, ""), 1, TamSX3("NYW_COD")[1])}
Local bNyuCod     := {|| StrZero(Val(cValToChar(xValSisJur)), TamSX3("NYU_COD")[1])}
Local cCpoKeyDt   := "TO_CHAR(KEYDDATA, 'DD-MM-YYYY HH:MM:SS')"
Local cCpoCliEPad := "CAD_LOCALIDADE.COD_CLIENTE||'|'||CLIENTE.END_PADRAO"
Local cCpoNYXCas  := "EXTNCLIEN||'|'||EXTNCASO"
Local cCpoHora    := "TO_CHAR(CLIDDATAABERTURA,'hh24:mm:ss')"
Local bGetCodRel  := {|| GetDePara("RCR.TIPORELAT", xValSisJur, "")}
Local bValDefaut  := {|| Iif(Empty(xValSisJur),'Migra��o', xValSisJur)}
Local bAnoMes     := {|| StrTran(xValSisJur, "-", "")}
Local bAdtMigraAdt:= {|| MigraDePar('RCR.ADIANTAMENTO', xValSisJur, '', 'ADINDOCUMENTO', cTab, aRegs[nI][1], 'NWF_COD')}
Local cAdtDtFin   := "TO_CHAR(RCR.ADIANTAMENTO.ADIDDATA, 'YYYYMMDD')"
Local cAdtCli     := "RCR.ADIANTAMENTO.ADINCLIENTE||'|'||RCR.PASTA.COD_ENDERECO"
Local cAdtDtMov   := "TO_CHAR(RCR.ADIANTAMENTO.ADIDDATA, 'YYYYMMDD')"
Local cAdtDtVen   := "TO_CHAR(RCR.ADIANTAMENTO.ADIDDATA, 'YYYYMMDD')"
Local cAdtBanco   := "NVL(RCR.CONTAS.BANCCODIGO ,' ')"
Local cAdtConta   := "NVL(RCR.CONTAS.CTACCONTA  ,' ')"
Local cAdtAgenc   := "NVL(RCR.CONTAS.CTACAGENCIA,' ')"
Local cAdtTipo    := "CASE WHEN RCR.ADIANTAMENTO.ADICTIPO = 'D' THEN '1' ELSE '2' END" 
Local cAdtExclus  := "'2'"
Local cAdtSituac  := "CASE WHEN RCR.ADIANTAMENTO.ADICSTATUS = 'A' THEN '1' ELSE 'E' END"
Local bAdtCaso    := {|| StrZero(xValSisJur, TamSX3("NVE_NUMCAS")[1])}
Local cAdtCliEnd  := "RCR.ENDERECO.COD_CLIENTE||'|'||RCR.ENDERECO.COD_ENDERECO"
Local bAdtGrpCli  := {|| Iif(!Empty(xValSisJur), StrZero(xValSisJur, TamSX3("A1_GRPVEN" )[1]), '')}
Local cAdtTitGer  := "2"
Local bAdtGetMoe  := {|| Substr(GetDePara("RCR.MOEDA", xValSisJur, ""), 1, TamSX3("CTO_MOEDA")[1])}

Local bLtbMigra   := {|| MigraDePar('RCR.LANCTABELADO', xValSisJur, '', 'ID_LANCTAB', cTab, aRegs[nI][1], 'NV4_COD')}
Local cLtbId      := "LPAD(RCR.LANCTABELADO.ID_LANCTAB,6,'0')"
Local cLtbData    := "NVL(TO_CHAR(RCR.LANCTABELADO.DATA,'YYYYMMDD'),' ')"
Local cLtbGrpCli  := "NVL(LPAD(RCR.CLIENTE.GRENCODIGO,6,'0'),' ')"
Local cLtbDtConcl := "NVL(TO_CHAR(RCR.LANCTABELADO.DATA_CONCLUSAO,'YYYYMMDD'),' ')"
Local cLtbPreFat  := "NVL(LPAD(RCR.LANCTABELADO.NUMEROPREFAT,8,'0'), ' ')"
Local cLtbLote    := "NVL(LPAD(RCR.LANCTABELADO.LOTE,6,'0'),' ')"

Local cbMigra     := {|| MigraDePar('RCR.LANCTABELADOFATU', xValSisJur, '', 'ID_LANCTAB', cTab, aRegs[nI][1], 'NW4_CLTAB')}
Local bGetCliCont := {|| ContrMae(xValSisJur,1)}
Local bGetLojCont := {|| ContrMae(xValSisJur,2)}
Local bGetCasCont := {|| ContrMae(xValSisJur,3)}
Local bGetParCont := {|| ContrMae(xValSisJur,6)}
Local cLtbSituac  := "CASE WHEN RCR.LANCTABELADO.STATUS = 'N' THEN '1'     "+;
                     "     WHEN RCR.LANCTABELADO.STATUS = 'F' THEN '2'     "+;
                     "     WHEN RCR.LANCTABELADO.STATUS = 'W' THEN '3' END "

Local bMigraFatAdt:= {|| MigraDePar('RCR.FATURAADICIONAL', xValSisJur, '', 'FADNCLIENTE|FADNPARCELA', cTab, aRegs[nI][1], 'NVV_COD')}
Local cChaveNVV   := "TO_CHAR(RCR.FATURAADICIONAL.FADNCLIENTE)      ||'|'|| "+;
                     "TO_CHAR(RCR.FATURAADICIONAL.FADNPARCELA) "
Local cChaveNVW   := "TO_CHAR(RCR.FATURAADICIONALPASTA.FAPNCLIENTE) ||'|'|| "+;
                     "TO_CHAR(RCR.FATURAADICIONALPASTA.FAPNPARCELA) "

Local bMigraDespes:= {|| MigraDePar('RCR.DESPESA'           , xValSisJur, '', 'CODIGO'               , cTab, aRegs[nI][1]        , 'NVY_COD'           )}
Local bGetDespesa := {|| GetDePara("RCR.DESPESA", xValSisJur, "")}
Local bMigraTpAtiv:= {|| MigraDePar('SSJR.EBILL_TPATV_RELAC', xValSisJur, '', 'ID_DOCPADRAO|TIPOATIV', cTab, NS1->NS1_COD        ,'NS1_CDOC+NS1_CATIVJ')}
Local bGetAtivid  := {|| GetDePara("SSJR.EBILL_TPATV_RELAC", xValSisJur, "")}
Local bGetNMoeda  := {|| Val(Substr(GetDePara("RCR.MOEDA", xValSisJur, ""), 1, TamSX3("CTO_MOEDA")[1]))}
Local bOHBValNac  := &('{|| JA201FConv(Substr(GetDePara("RCR.MOEDA", xValSisJur, ""), 1,' + cValToChar(TamSX3("CTO_MOEDA")[1]) + '), "01", OHB->OHB_VALOR, "A", , , , , , , , , OHB->OHB_COTAC)[1]}')

Local cSituaPre   := " CASE WHEN RCR.PRE_FATURA.SITUACAO = 0  THEN '7' "+;
						  " WHEN RCR.PRE_FATURA.SITUACAO = 1  THEN '2' "+; 
						  " WHEN RCR.PRE_FATURA.SITUACAO = 2  THEN '5' "+; 
						  " WHEN RCR.PRE_FATURA.SITUACAO = 3  THEN '3' "+; 
						  " WHEN RCR.PRE_FATURA.SITUACAO = 4  THEN '4' "+; 
						  " WHEN RCR.PRE_FATURA.SITUACAO = 5  THEN '6' "+; 
						  " WHEN RCR.PRE_FATURA.SITUACAO = 6  THEN '8' "+; 
						  " WHEN RCR.PRE_FATURA.SITUACAO = 7  THEN 'C' "+; 
						  " WHEN RCR.PRE_FATURA.SITUACAO = 8  THEN 'D' "+; 
						  " WHEN RCR.PRE_FATURA.SITUACAO = 9  THEN 'E' "+; 
						  " WHEN RCR.PRE_FATURA.SITUACAO = 10 THEN 'H' END " 

Local nVlrTBPre   := " NVL((SELECT SUM(VALORH*INDICE) 												VALORTB     "+;	
					 "	    FROM (SELECT VALORH														VALORH ,    "+;
					 "					 INDICE														INDICE      "+;
					 "		      FROM RCR.LANCTABELADO	WHERE NUMEROPREFAT = RCR.PRE_FATURA.NUMERO)),0)	        "
							 
Local nVlrTSPre   := " 	NVL((SELECT SUM(VALOR*INDICE_PREFATURA)										VALORTS	        "+;
					 "	 	 FROM (	SELECT	VALOR													VALOR	,       "+;
					 "					INDICE_PREFATURA											INDICE_PREFATURA"+;
					 " 			FROM RCR.TIME_SHEET	WHERE NUMEROPREFAT = RCR.PRE_FATURA.NUMERO)),0)	                "
					 
Local nLanTabPre  := " CASE WHEN (	SELECT	COUNT(*)                                    "+;
					 "				FROM RCR.LANCTABELADO                               "+;	
					 "				WHERE NUMEROPREFAT = RCR.PRE_FATURA.NUMERO) <> 0    "+; 
					 "				THEN '1' ELSE '2' END								"

Local cDtMinTabPre:= " NVL(TO_CHAR((SELECT	MIN(RCR.LANCTABELADO.DATA)                            "+;
					 "				FROM RCR.LANCTABELADO                                         "+;	
					 "				WHERE NUMEROPREFAT = RCR.PRE_FATURA.NUMERO), 'YYYYMMDD'),' ') "

Local cDtMaxTabPre:= " NVL(TO_CHAR((SELECT	MAX(RCR.LANCTABELADO.DATA)                            "+;
					 "				FROM RCR.LANCTABELADO                                         "+;	
					 "				WHERE NUMEROPREFAT = RCR.PRE_FATURA.NUMERO), 'YYYYMMDD'),' ') "
					 
Local cTpEmisPre  := " CASE WHEN RCR.PRE_FATURA.TIPO_EMISSAO = 'F' THEN '1'              "+;
					 "	 	WHEN RCR.PRE_FATURA.TIPO_EMISSAO = 'M' THEN '2' ELSE '3' END "

Local nLanTabPFC  := " CASE WHEN (	SELECT	COUNT(*)                                                         "+;
					 "				FROM RCR.LANCTABELADO                                                    "+;
					 "				WHERE RCR.LANCTABELADO.NUMEROPREFAT = RCR.PRE_FATPASTA.NUM_PRE_FAT) <> 0 "+;
					 "				THEN '1' ELSE '2' END                                                    "										

Local cTSheetPFC  := " CASE WHEN NVL((SELECT COUNT(*)                                                 "+;
                     "                FROM RCR.TIME_SHEET WHERE RCR.TIME_SHEET.NUMEROPREFAT =         "+;
                     "                     RCR.PRE_FATPASTA.NUM_PRE_FAT),0) = 0 THEN '2' ELSE '1' END "
                     
LocaL cTpCobPreFat := "CASE WHEN SSJR.FAT_PREFATURA_COBRANCA_TMP.SITUACAO = 'PE' THEN '1'     "+;
                          " WHEN SSJR.FAT_PREFATURA_COBRANCA_TMP.SITUACAO = 'PC' THEN '5'     "+;
                          " WHEN SSJR.FAT_PREFATURA_COBRANCA_TMP.SITUACAO = 'C'  THEN '5'     "+;
                          " WHEN SSJR.FAT_PREFATURA_COBRANCA_TMP.SITUACAO = 'R'  THEN '2'     "+;
                          " WHEN SSJR.FAT_PREFATURA_COBRANCA_TMP.SITUACAO = 'D'  THEN '7'     "+;
                          " WHEN SSJR.FAT_PREFATURA_COBRANCA_TMP.SITUACAO = 'O'  THEN '7'     "+;
                          " WHEN SSJR.FAT_PREFATURA_COBRANCA_TMP.SITUACAO = 'FE' THEN '6' END "

Local cSitTSheet   := "CASE WHEN SSJR.TIME_SHEET_FATURA.situacao = 'N' THEN '1'     "+;
                      "     WHEN SSJR.TIME_SHEET_FATURA.situacao = 'F' THEN '2'     "+;
                      "     WHEN SSJR.TIME_SHEET_FATURA.situacao = 'W' THEN '3' END "
Local nCfgUT     := SuperGetMV( 'MV_JURTS1',, 10 )
Local cCfgUT     := cValtoChar(nCfgUT)
Local bConvHoraF := {|| Val(JURA144C1(1, 2, cValtoChar(xValSisJur),, nCfgUT)) }
Local cPicHoraL  := '"' + RTrim(PesqPict("NUE", "NUE_HORAL")) + '"'
Local bConvHora  := &('{|| Transform(PADL(JURA144C1(1, 3, xValSisJur,,' + cCfgUT + '), ' + cTamHoraL +  ',"0"), ' + cPicHoraL + ')}') //Transforma a string em codeblock para n�o consultar dicion�rio de dados
Local bEscritNUE := {||x:=JurPartHst(NUE->NUE_CPART1, NUE->NUE_DATATS, "NUS_CESCR"), Iif(!Empty(x), x, '')}
Local bCCustoNUE := {||x:=JurPartHst(NUE->NUE_CPART1, NUE->NUE_DATATS, "NUS_CC   "), Iif(!Empty(x), x, '')}
Local bValorHNUE := {|| GetNUEVlrH()}
Local cParcela   := "LPAD(TO_CHAR(ROW_NUMBER()OVER (PARTITION BY finance.CONTASRECEBERHISTORICO.crccnumerodocumento,"+;
					"											 finance.CONTASRECEBERHISTORICO.crccparcela		   ,"+;
					"											 finance.CONTASRECEBERHISTORICO.crcncliente         "+;
					"								  ORDER BY	finance.CONTASRECEBERHISTORICO.crccnumerodocumento, "+;
					"											finance.CONTASRECEBERHISTORICO.crccparcela		  , "+;
					"											finance.CONTASRECEBERHISTORICO.crcncliente)),2,'0') "
If cTab == 'NS7' // Escrit�rio

	//               Sisjuri                               , Protheus     , Force                                                , Aju Tipo, Executa fun��o
	aAdd( aDePara, { 'RCR.ESCRITORIO.SIGLA'                , 'NS7_COD'    , ''                                                   , ''      , Nil} )
	aAdd( aDePara, { 'RCR.ESCRITORIO.ORGNCODIG'            , ''           , ''                                                   , ''      , bMigraEscr } )
	aAdd( aDePara, { 'RCR.ESCRITORIO.NOME'                 , 'NS7_NOME'   , ''                                                   , ''      , Nil } )
	aAdd( aDePara, { 'RCR.ESCRITORIO.SIGLA'                , 'NS7_CEMP'   , {|| EscritXFil(xValSisJur, aFilxEscr, 'NS7_CEMP')}   , ''      , Nil } )
	aAdd( aDePara, { 'RCR.ESCRITORIO.SIGLA'                , 'NS7_CFILIA' , {|| EscritXFil(xValSisJur, aFilxEscr, 'NS7_CFILIA')} , ''      , Nil } )
	aAdd( aDePara, { 'RCR.ESCRITORIO.RAZAOSOCIAL'          , 'NS7_RAZAO'  , ''                                                   , ''      , Nil } )
	aAdd( aDePara, { 'RCR.ESCRITORIO.EMPCCGC'              , 'NS7_CNPJ'   , ''                                                   , ''      , Nil } )
	aAdd( aDePara, { 'RCR.ESCRITORIO.EMPCCCM'              , 'NS7_CCM'    , ''                                                   , ''      , Nil } )
	aAdd( aDePara, { 'RCR.ESCRITORIO.CONTATOINTERNACIONAL' , 'NS7_CTINT'  , ''                                                   , ''      , Nil } )
	aAdd( aDePara, { 'RCR.ESCRITORIO.CONTATONACIONAL'      , 'NS7_CTNAC'  , ''                                                   , ''      , Nil } )
	aAdd( aDePara, { 'RCR.ESCRITORIO.EMITE_FATURA'         , 'NS7_EMITEF' , {{"S", "1"}, {"N", "2"}, {"", "1"}}                  , ''      , Nil } )
	aAdd( aDePara, { 'RCR.ESCRITORIO.EMITENFE'             , 'NS7_EMINFE' , {{"S", "1"}, {"N", "2"}, {"", "1"}}                  , ''      , Nil } )
	aAdd( aDePara, { 'RCR.ESCRITORIO.EMITENF'              , 'NS7_EMINFS' , {{"S", "1"}, {"N", "2"}, {"", "1"}}                  , ''      , Nil } )
	aAdd( aDePara, { 'RCR.ESCRITORIO.EMITENFINT'           , 'NS7_EMINFI' , {{"S", "1"}, {"N", "2"}, {"", "1"}}                  , ''      , Nil } )
	aAdd( aDePara, { 'RCR.ESCRITORIO.TELEFONE'             , 'NS7_TEL'    , ''                                                   , ''      , Nil } )
	aAdd( aDePara, { 'RCR.ESCRITORIO.EMAIL'                , 'NS7_EMAIL'  , ''                                                   , ''      , Nil } )
	aAdd( aDePara, { 'RCR.ENDEMPRESA.ENDCLOGRADOURO'       , 'NS7_END'    , {|| Substr(xValSisJur, 1, TamSX3("NS7_END")[1])}     , ''      , Nil } )
	aAdd( aDePara, { 'RCR.ENDEMPRESA.CIDNCODIGO'           , 'NS7_CMUNIC' , {|| GetDePara("RCR.CIDADE", xValSisJur, "")}         , ''      , Nil } )
	aAdd( aDePara, { 'RCR.ENDEMPRESA.ENDCBAIRRO'           , 'NS7_BAIRRO' , ''                                                   , ''      , Nil } )
	aAdd( aDePara, { 'RCR.ENDEMPRESA.ENDCCEP'              , 'NS7_CEP'    , {|| StrTran(xValSisJur, "-", "")}                    , ''      , Nil } )
	aAdd( aDePara, { 'RCR.ENDEMPRESA.PAINCODIGO'           , 'NS7_CPAIS'  , {|| GetDePara("RCR.PAIS", xValSisJur, "")}           , ''      , Nil } )
	aAdd( aDePara, { 'RCR.ESTADO.ESTCSIGLA'                , 'NS7_ESTADO' , ''                                                   , ''      , Nil } )
	aAdd( aDePara, { ''                                    , 'NS7_SEPARA' , '1'                                                  , ''      , Nil } )
	aAdd( aDePara, { ''                                    , 'NS7_ATIVO'  , '1'                                                  , ''      , Nil } )

ElseIf cTab == 'NRH' // Tipo de Despesas

	//               Sisjuri             , Protheus    , Force                              , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'TIPO'              , 'NRH_COD'   , bTipoDesp                          , ''         ,          } )
	aAdd( aDePara, { 'TIPO'              , ''          , ''                                 , ''         , bMigraNRH} )
	aAdd( aDePara, { 'PERC_CORRECAO'     , 'NRH_PCORRE', ''                                 , ''         ,          } )
	aAdd( aDePara, { 'STATUS'            , 'NRH_ATIVO' , {{"A", "1"}, {"I", "2"}}           , ''         ,          } )
	aAdd( aDePara, { 'DESCRICAO'         , 'NRH_DESC'  , ''                                 , ''         ,          } )
	aAdd( aDePara, { 'COBRAR'            , 'NRH_COBRAR', {{"S", "1"}, {"N", "2"}}           , ''         ,          } )
	aAdd( aDePara, { 'UTILIZA_LOCALIDADE', 'NRH_LOCALI', {{"S", "1"}, {"N", "2"}}           , ''         ,          } )
	aAdd( aDePara, { 'VALOR_UNITARIO'    , 'NRH_VALORU', ''                                 , ''         ,          } )
	aAdd( aDePara, { 'PCTCNUMEROCONTA'   , 'NRH_CCONTA', ''                                 , ''         ,          } )
	aAdd( aDePara, { 'TIPO_COBRANCA'     , 'NRH_CTPCB' , {{"H", "2"}, {"R", "1"}, {"", "1"}}, ''         ,          } )

ElseIf cTab == 'OHJ' // Exce��o Tipo de Despesa

	//               Sisjuri        , Protheus   , Force                                , Ajusta Tipo
	aAdd( aDePara, { 'ORGNCODIG'    , 'OHJ_COD'  , bGetEscrt                            , '' } )
	aAdd( aDePara, { 'TIPODESP'     , 'OHJ_CTPDP', bTipoDesp                            , '' } )
	aAdd( aDePara, { 'TIPO_COBRANCA', 'OHJ_TPCOB', {{"H", "2"}, {"R", "1"}, {"", "1"}}  , '' } )

ElseIf cTab == 'NR1' // Idioma

	//               Sisjuri    , Protheus   , Force, Ajusta Tipo
	aAdd( aDePara, { 'IDIOMA'   , 'NR1_COD'  , ''   , '' } )
	aAdd( aDePara, { 'IDIOMA'   , 'NR1_SIGLA', ''   , '' } )
	aAdd( aDePara, { 'DESCRICAO', 'NR1_DESC' , ''   , '' } )

ElseIf cTab == 'NRK' // Tipo da Tabela de Servi�os

	//               Sisjuri        , Protheus  ,  Force,  Ajusta Tipo
	aAdd( aDePara, { 'TTBCCODIGO'   , 'NRK_COD' , ''    , 'Str' } )
	aAdd( aDePara, { 'TTBCDESCRICAO', 'NRK_DESC', ''    , '' } )

ElseIf cTab == 'NRD' // Servi�os Tabelados

	//               Sisjuri            , Protheus    , Force                   , Ajusta Tipo
	aAdd( aDePara, { 'ITBCCODIGO'       , 'NRD_COD'   , ''                      , '' } )
	aAdd( aDePara, { 'TTBCCODIGO'       , 'NRD_CTIPO' , ''                      , 'Str' } )
	aAdd( aDePara, { 'ITBCDESCRICAOH'   , 'NRD_DESCH' , ''                      , '' } )
	aAdd( aDePara, { 'ITBCLANCTS'       , 'NRD_LANTS' , {{"S", "1"}, {"N", "2"}}, '' } )
	aAdd( aDePara, { 'COBVLMAIOR'       , 'NRD_COBMAI', {{"S", "1"}, {"N", "2"}}, '' } )
	aAdd( aDePara, { 'TIPO_DESP'        , 'NRD_CDESP' , ''                      , 'Str' } )
	aAdd( aDePara, { 'ITBCDESCRICAODESP', 'NRD_DESCD' , ''                      , '' } )
	aAdd( aDePara, { 'ITBCCODIGOINPI'   , 'NRD_CINPI' , ''                      , '' } )
	aAdd( aDePara, { 'ITBCNARRATIVA'    , 'NRD_NARRAT', ''                      , '' } )
	aAdd( aDePara, { ''                 , 'NRD_ATIVO' , '1'                     , '' } )

ElseIf cTab == 'NR3' // Servi�os Tabelados por Idioma

	//               Sisjuri            , Protheus    , Force                            , Ajusta Tipo
	aAdd( aDePara, { ''                 , 'NR3_COD'    , {|| GETSXENUM("NR3", "NR3_COD")}, '' } )
	aAdd( aDePara, { 'ITBCCODIGO'       , 'NR3_CITABE' , ''                              , '' } )
	aAdd( aDePara, { 'IDIOMA'           , 'NR3_CIDIOM' , ''                              , '' } )
	aAdd( aDePara, { 'DESCRICAOH'       , 'NR3_DESCHO' , ''                              , '' } )
	aAdd( aDePara, { 'DESCRICAOD'       , 'NR3_DESCDE' , ''                              , '' } )
	aAdd( aDePara, { 'NARRATIVA'        , 'NR3_NARRAP' , ''                              , '' } )

ElseIf cTab == 'NTP' // Localidades

	//               Sisjuri                     , Protheus     , Force                           , Ajusta Tipo
	aAdd( aDePara, { 'ID_LOCALIDADE'             , 'NTP_COD'    , ''                              , 'Str' } )
	aAdd( aDePara, { 'DESCRICAO'                 , 'NTP_DESC'   , ''                              , '' } )
	aAdd( aDePara, { 'KM'                        , 'NTP_KM'     , ''                              , '' } )
	aAdd( aDePara, { 'CAD_LOCALIDADE.COD_CLIENTE', 'NTP_CCLIEN' , bGetSA1Cli                      , '' } )
	aAdd( aDePara, { cCpoCliEPad                 , 'NTP_CLOJA'  , bGetSA1Loj                      , '' } ) //NOTE: Ser� usado o END_PADRAO no cadastro do Cliente (RCR.CLIENTE) 

ElseIf cTab == 'NSA' // Motivo de Cancelamento de Fatura

	//               Sisjuri            , Protheus     , Force                           , Ajusta Tipo
	aAdd( aDePara, { 'MTCNCODIGO'       , 'NSA_COD'    , ''                              , 'Str' } )
	aAdd( aDePara, { 'MTCCDESCRICAO'    , 'NSA_DESC'   , ''                              , '' } )

ElseIf cTab == 'NSB' // Retifica��o de TS

	//               Sisjuri            , Protheus     , Force                           , Ajusta Tipo
	aAdd( aDePara, { 'ID_RETIFICACAO'   , 'NSB_COD'    , ''                              , 'Str' } )
	aAdd( aDePara, { 'DESCRICAO'        , 'NSB_DESC'   , ''                              , '' } )
	aAdd( aDePara, { 'SITUACAO'         , 'NSB_ATIVO'  , {{"A", "1"}, {"I", "2"}}        , '' } )

ElseIf cTab == 'NSC' // Situa��o de Cobran�a / Tipo de Retorno

	//               Sisjuri         , Protheus     , Force                           , Ajusta Tipo
	aAdd( aDePara, { 'ID_SITUACAO'   , 'NSC_COD'    , ''                              , '' } )
	aAdd( aDePara, { 'DESCRICAO'     , 'NSC_DESC'   , ''                              , '' } )
	aAdd( aDePara, { 'ATIVO'         , 'NSC_ATIVO'  , {{"S", "1"}, {"N", "2"}}        , '' } )
	aAdd( aDePara, { 'RESTRICAO'     , 'NSC_RESTRI' , {{"S", "1"}, {"N", "2"}}        , '' } )
	aAdd( aDePara, { ''              , 'NSC_OBDESC' , '2'                             , '' } )

ElseIf cTab == 'NRC' // Tipo de Atividade

	//               Sisjuri            , Protheus     , Force                                         , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'CODIGO'           , 'NRC_COD'    , bTpAtivid                                     , ''         ,          } )
	aAdd( aDePara, { 'CODIGO'           , ''           ,                                               , ''         , bMigraNRC} )
	aAdd( aDePara, { 'LANCTSDATAFUTURA' , 'NRC_FUTURO' , {{"S", "1"}, {"N", "2"}}                      , ''         ,          } )
	aAdd( aDePara, { 'DESCRICAO'        , 'NRC_DESC'   , ''                                            , ''         ,          } )
	aAdd( aDePara, { 'PART'             , 'NRC_PART'   , {{"S", "1"}, {"N", "2"}}                      , ''         ,          } )
	aAdd( aDePara, { 'COBRARPADRAO'     , 'NRC_COBRAR' , {{"S", "1"}, {"N", "2"}}                      , ''         ,          } )
	aAdd( aDePara, { 'COBRAVEL'         , 'NRC_TEMPOZ' , {{"S", "1"}, {"N", "2"}}                      , ''         ,          } )
	aAdd( aDePara, { 'STATUS'           , 'NRC_ATIVO'  , {{"A", "1"}, {"I", "2"}}                      , ''         ,          } )

ElseIf cTab == 'NUO' // Tipo de Presta��o de Contas

	//               Sisjuri             , Protheus     , Force                           , Ajusta Tipo
	aAdd( aDePara, { 'ID_TIPOPRESTCONTAS', 'NUO_COD'    , ''                              , 'Str' } )
	aAdd( aDePara, { 'DESCRICAO'         , 'NUO_DESC'   , ''                              , '' } )
	aAdd( aDePara, { 'SITUACAO'          , 'NUO_ATIVO'  , {{"A", "1"}, {"I", "2"}}        , '' } )

ElseIf cTab == 'NRI' // Tipo de Origina��o

	//               Sisjuri            , Protheus     , Force                                          , Ajusta Tipo, Executa Fun��o
	aAdd( aDePara, { 'CODIGO'           , 'NRI_COD'    , bTpOrigina                                     , ''         ,          } )
	aAdd( aDePara, { 'CODIGO'           , ''           , ''                                             , ''         , bMigraNRI} )
	aAdd( aDePara, { 'INCSOCIO'         , 'NRI_INCSOC' , {{"S", "1"}, {"N", "2"}}                       , ''         ,          } )
	aAdd( aDePara, { 'OBRIGATORIO'      , 'NRI_OBRIGA' , {{"S", "1"}, {"N", "2"}}                       , ''         ,          } )
	aAdd( aDePara, { 'DESCRICAO'        , 'NRI_DESC'   , ''                                             , ''         ,          } )
	aAdd( aDePara, { 'PRAZOVALIDADE'    , 'NRI_PRAZOV' , ''                                             , ''         ,          } )
	aAdd( aDePara, { 'SOMAORIGINACAO'   , 'NRI_SOMAOR' , ''                                             , ''         ,          } )
	aAdd( aDePara, { 'TIPO'             , 'NRI_TIPO'   , {{"C", "1"}, {"P", "2"}, {"A", "3"}}           , ''         ,          } )
	aAdd( aDePara, { 'STATUS'           , 'NRI_ATIVO'  , {{"A", "1"}, {"I", "2"}}                       , ''         ,          } )

ElseIf cTab == 'NR9' // Tipo de Fatura

	//               Sisjuri            , Protheus     , Force                               , Ajusta Tipo
	aAdd( aDePara, { 'ID_TIPOEVENTO'    , 'NR9_COD'    , bTpFatura                           , '' } )
	aAdd( aDePara, { 'DESCRICAO'        , 'NR9_DESC'   , ''                                  , '' } )
	aAdd( aDePara, { 'SUGTXTFAT'        , 'NR9_TXTFAT' , {{"S", "1"}, {"N", "2"}}            , '' } )
	aAdd( aDePara, { 'SUGTXTREL'        , 'NR9_TXTREL' , {{"S", "1"}, {"N", "2"}}            , '' } )
	aAdd( aDePara, { 'EXITO'            , 'NR9_EXITO'  , {{"S", "1"}, {"N", "2"}}            , '' } )

ElseIf cTab == 'NRG' // Tipo de Carta de Cobran�a

	//               Sisjuri            , Protheus     , Force                               , Ajusta Tipo
	aAdd( aDePara, { 'CODIGO'           , 'NRG_COD'    , bCartaCob                           , 'Str' } )
	aAdd( aDePara, { 'DESCRICAO'        , 'NRG_DESC'   , ''                                  , '' } )
	aAdd( aDePara, { ''                 , 'NRG_ARQ'    , {|| Space(TamSX3("NRG_ARQ")[1])}    , '' } )

ElseIf cTab == 'NRB' // �rea Jur�dica

	//               Sisjuri     , Protheus     , Force                   , Ajusta Tipo
	aAdd( aDePara, { 'SIGLA'     , 'NRB_COD'    , ''                      , '' } )
	aAdd( aDePara, { 'NOME'      , 'NRB_DESC'   , ''                      , '' } )
	aAdd( aDePara, { 'STATUS'    , 'NRB_ATIVO'  , {{"A", "1"}, {"I", "2"}}, '' } )
	aAdd( aDePara, { ''          , 'NRB_FLUIG'  , '2'                     , '' } )
	aAdd( aDePara, { 'FATURAVEL' , 'NRB_FATURA' , {{"S", "1"}, {"N", "2"}}, '' } )
	aAdd( aDePara, { ''          , 'NRB_TECNIC' , '2'                     , '' } )

ElseIf cTab == 'NRL' // Sub�rea Jur�dica

	//               Sisjuri     , Protheus     , Force                   , Ajusta Tipo
	aAdd( aDePara, { 'ID_SUBAREA', 'NRL_COD'    , ''                      , 'Str' } )
	aAdd( aDePara, { 'SIGLA'     , 'NRL_CAREA'  , ''                      , ''    } )
	aAdd( aDePara, { 'DESCRICAO' , 'NRL_DESC'   , ''                      , ''    } )
	aAdd( aDePara, { 'STATUS'    , 'NRL_ATIVO'  , {{"A", "1"}, {"I", "2"}}, ''    } )

ElseIf cTab == 'CTO' // Moeda Cont�bil

	//               Sisjuri      , Protheus     , Force                      , Ajusta Tipo , Grava De Para OHS
	aAdd( aDePara, { cCodMoeda    , 'CTO_MOEDA'  , {|| GetMoeCod(xValSisJur)} , ''          ,            } )
	aAdd( aDePara, { 'CODIGO'     , ''           ,                            , ''          , bMigraMoed } )
	aAdd( aDePara, { 'SIGLA'      , 'CTO_DESC'   , ''                         , ''          , Nil        } )
	aAdd( aDePara, { 'SIGLA'      , 'CTO_SIMB'   , ''                         , ''          , Nil        } )
	aAdd( aDePara, { ''           , 'CTO_BLOQ'   , '2'                        , ''          , Nil        } )
	aAdd( aDePara, { 'DATA_INICIO', 'CTO_DTINIC' , ''                         , ''          , Nil        } )
	aAdd( aDePara, { 'DATA_FIM'   , 'CTO_DTFINA' , ''                         , ''          , Nil        } )

ElseIf cTab == 'NVQ' // Fechamento de Per�odo

	//               Sisjuri        , Protheus     , Force                                                         , Ajusta Tipo
	aAdd( aDePara, { 'ID_FECHAMENTO', 'NVQ_COD'    , ''                                                            , 'Str' } )
	aAdd( aDePara, { 'ANO_MES'      , 'NVQ_ANOMES' , {|| StrTran(xValSisJur, "-", "")}                             , '' } )
	aAdd( aDePara, { 'MODULO'       , 'NVQ_MODULO' , {{"FATURAMENTO_EMI", "FATEMI"}, {"FATURAMENTO_CAN", "FATCAN"}}, '' } )
	aAdd( aDePara, { ''             , 'NVQ_USER'   , '000000'                                                      , '' } )
	aAdd( aDePara, { ''             , 'NVQ_DATA'   , {|| Date()}                                                   , '' } )
	aAdd( aDePara, { 'SITUACAO'     , 'NVQ_SITUAC' , {{"A", "1"}, {"F", "2"}}                                      , '' } )
	aAdd( aDePara, { 'OBSERVACAO'   , 'NVQ_OBS'    , ''                                                            , '' } )

ElseIf cTab == 'NRE' // Tabela de Servi�os

	//               Sisjuri         , Protheus    , Force                                               , Ajusta Tipo
	aAdd( aDePara, { 'TABCCODIGO'    , 'NRE_COD'   , ''                                                  , ''    } )
	aAdd( aDePara, { 'TABCMOEDA'     , 'NRE_MOEDA' , bGetMoeda                                           , ''    } )
	aAdd( aDePara, { 'TTBCCODIGO'    , 'NRE_CTIPO' , ''                                                  , 'Str' } )
	aAdd( aDePara, { 'TABCDESCRICAO' , 'NRE_DESC'  , {|| Substr(xValSisJur, 1, TamSX3("NRE_DESC")[1])}   , ''    } )

ElseIf cTab == 'NS5' // Detalhe da Tabela de Servi�os

	//               Sisjuri             , Protheus     , Force                                               , Ajusta Tipo
	aAdd( aDePara, { 'TABCCODIGO'        , 'NS5_CTAB'   , ''                                                  , '' } )
	aAdd( aDePara, { 'ITBCCODIGO'        , 'NS5_CSERV'  , ''                                                  , '' } )
	aAdd( aDePara, { 'DTTNVALORH'        , 'NS5_VALORH' , ''                                                  , '' } )
	aAdd( aDePara, { 'DTTNVALORTAXA'     , 'NS5_VALORT' , ''                                                  , '' } )
	aAdd( aDePara, { 'DTTCMOEDATAXA'     , 'NS5_MOEDAT' , bGetMoeda                                           , '' } )
	aAdd( aDePara, { 'DTTNQTDMINTAXA'    , 'NS5_MAXTAX' , ''                                                  , '' } )
	aAdd( aDePara, { 'DTTNVALORADICTAXA' , 'NS5_ADITAX' , ''                                                  , '' } )
	aAdd( aDePara, { 'DTTNQTDMINHON'     , 'NS5_MAXSER' , ''                                                  , '' } )
	aAdd( aDePara, { 'DTTNVALORADICHON'  , 'NS5_ADISER' , ''                                                  , '' } )

ElseIf cTab == 'NU1' // Hist�rico da Tabela de Servi�os

	//               Sisjuri        , Protheus     , Force                                               , Ajusta Tipo
	aAdd( aDePara, { ''             , 'NU1_COD'    , {|| GETSXENUM("NU1", "NU1_COD")}                    , ''    } )
	aAdd( aDePara, { 'TABCCODIGO'   , 'NU1_CTAB'   , ''                                                  , ''    } )
	aAdd( aDePara, { 'TABCANO_MES'  , 'NU1_AMINI'  , {|| StrTran(xValSisJur, "-", "")}                   , ''    } )
	aAdd( aDePara, { 'TABCANO_MES'  , 'NU1_AMFIM'  , {|| StrTran(xValSisJur, "-", "")}                   , ''    } )
	aAdd( aDePara, { 'TABCMOEDA'    , 'NU1_CMOEDA' , bGetMoeda                                           , ''    } )
	aAdd( aDePara, { 'TTBCCODIGO'   , 'NU1_CTIPO'  , ''                                                  , 'Str' } )
	aAdd( aDePara, { 'TABCDESCRICAO', 'NU1_DTAB'   , {|| Substr(xValSisJur, 1, TamSX3("NU1_DTAB")[1])}   , ''    } )

ElseIf cTab == 'NTS' // Hist�rico Valores dos Servi�os

	//               Sisjuri             , Protheus     , Force                                                                         , Ajusta Tipo
	aAdd( aDePara, { 'TABCCODIGO'        , 'NTS_CTAB'   , ''                                                                            , '' } )
	aAdd( aDePara, { ''                  , 'NTS_COD'    , {|| GETSXENUM("NTS", "NTS_COD")}                                              , '' } )
	aAdd( aDePara, { 'ITBCCODIGO'        , 'NTS_CSERV'  , ''                                                                            , '' } )
	aAdd( aDePara, { 'DTTNVALORH'        , 'NTS_VALORH' , {|| Iif(xValSisJur > 0, xValSisJur, ValTab(aRegs[nI][1], aRegs[nI][3]))}      , '' } )
	aAdd( aDePara, { 'DTTNQTDMINHON'     , 'NTS_MAXSER' , ''                                                                            , '' } )
	aAdd( aDePara, { 'DTTNVALORADICHON'  , 'NTS_ADISER' , ''                                                                            , '' } )
	aAdd( aDePara, { 'DTTCMOEDATAXA'     , 'NTS_MOEDAT' , bGetMoeda                                                                     , '' } )
	aAdd( aDePara, { 'DTTNVALORTAXA'     , 'NTS_VALORT' , ''                                                                            , '' } )
	aAdd( aDePara, { 'DTTNQTDMINTAXA'    , 'NTS_MAXTAX' , ''                                                                            , '' } )
	aAdd( aDePara, { 'DTTNVALORADICTAXA' , 'NTS_ADITAX' , ''                                                                            , '' } )
	aAdd( aDePara, { 'DTTCANO_MES'       , 'NTS_CHIST'  , {|| BuscaCodAM("NU1", "NU1_COD", aRegs[nI][1], StrTran(xValSisJur, "-", ""))} , '' } )

ElseIf cTab == 'NRN' // Categoria dos Participantes

	//               Sisjuri        , Protheus     , Force                    , Ajusta Tipo
	aAdd( aDePara, { 'CATEG_ADVG'   , 'NRN_COD'    , ''                       , '' } )
	aAdd( aDePara, { 'DESCRICAO'    , 'NRN_DESC'   , ''                       , '' } )
	aAdd( aDePara, { 'META_PADRAO'  , 'NRN_META'   , ''                       , '' } )
	aAdd( aDePara, { 'ASSINAFATURA' , 'NRN_ASSINA' , {{"S", "1"}, {"N", "2"}} , '' } )
	aAdd( aDePara, { 'STATUS'       , 'NRN_ATIVO ' , {{"A", "1"}, {"I", "2"}} , '' } )

ElseIf cTab == 'NR2' // Descri��o Categoria por Idioma

	//               Sisjuri     , Protheus     , Force                           , Ajusta Tipo
	aAdd( aDePara, { ''          , 'NR2_COD'    , {|| GETSXENUM("NR2", "NR2_COD")}, '' } )
	aAdd( aDePara, { 'CATADVG'   , 'NR2_CATPAR' , ''                              , '' } )
	aAdd( aDePara, { 'IDIOMA'    , 'NR2_CIDIOM' , ''                              , '' } )
	aAdd( aDePara, { 'DESCRICAO' , 'NR2_DESC'   , ''                              , '' } )

ElseIf cTab == 'NRW' // Documento Padr�o E-billing

	//               Sisjuri        , Protheus     , Force                    , Ajusta Tipo
	aAdd( aDePara, { 'ID_DOCPADRAO' , 'NRW_COD'    , ''                       , '' } )
	aAdd( aDePara, { 'NOME'         , 'NRW_DESC'   , ''                       , '' } )

ElseIf cTab == 'NRV' // Categoria E-Billing

	//               Sisjuri        , Protheus     , Force                           , Ajusta Tipo  , Executa fun��o
	aAdd( aDePara, { 'ID_DOCPADRAO' , 'NRV_CDOC'  , ''                               , ''           , '' } )
	aAdd( aDePara, { 'ID_CATEG'     , 'NRV_CCATE' , ''                               , ''           , '' } )
	aAdd( aDePara, { 'DESCRICAO'    , 'NRV_DESC'  , ''                               , ''           , '' } )
	aAdd( aDePara, { ''             , 'NRV_COD'   , {|| GETSXENUM("NRV", "NRV_COD")} , ''           , {|| Iif(nI == 1, LimpaArray(),), GuardaInfo(aRegs[nI])}} )

ElseIf cTab == 'NS2' // Categoria E-Billing (De-Para)

	//               Sisjuri        , Protheus     , Force                                        , Ajusta Tipo
	aAdd( aDePara, { 'ID_DOCPADRAO' , 'NS2_CDOC'   , ''                                           , '' } )
	aAdd( aDePara, { 'ID_TPCATEG'   , 'NS2_CCATE'  , {|| BuscaInfo(aRegs[nI][1], aRegs[nI][2], 4)}, '' } )
	aAdd( aDePara, { 'CATEG_ADVG'   , 'NS2_CCATEJ' , ''                                           , '' } )
	aAdd( aDePara, { ''             , 'NS2_COD'    , {|| GETSXENUM("NS2", "NS2_COD")}             , '' } )

ElseIf cTab == 'NRY' // Fase E-Billing

	//               Sisjuri        , Protheus     , Force                           , Ajusta Tipo   , Executa fun��o
	aAdd( aDePara, { 'ID_DOCPADRAO' , 'NRY_CDOC'  , ''                               , ''            , '' } )
	aAdd( aDePara, { 'ID_FASE'      , 'NRY_CFASE' , ''                               , ''            , '' } )
	aAdd( aDePara, { 'DESCRICAO'    , 'NRY_DESC'  , ''                               , ''            , '' } )
	aAdd( aDePara, { ''             , 'NRY_COD'   , {|| GETSXENUM("NRY", "NRY_COD")} , ''            , {|| Iif(nI == 1, LimpaArray(),), GuardaInfo(aRegs[nI])}} )

ElseIf cTab == 'NRZ' // Tarefa E-Billing (De-Para)

	//               Sisjuri        , Protheus     , Force                                       , Ajusta Tipo
	aAdd( aDePara, { 'ID_DOCPADRAO' , 'NRZ_CDOC'  , ''                                           , '' } )
	aAdd( aDePara, { 'ID_FASE'      , 'NRZ_CFASE' , {|| BuscaInfo(aRegs[nI][1], aRegs[nI][2], 4)}, '' } )
	aAdd( aDePara, { 'DESCRICAO'    , 'NRZ_DESC'  , ''                                           , '' } )
	aAdd( aDePara, { 'ID_TAREFA'    , 'NRZ_CTAREF', ''                                           , '' } )
	aAdd( aDePara, { ''             , 'NRZ_COD'   , {|| GETSXENUM("NRZ", "NRZ_COD")}             , '' } )
	
ElseIf cTab == 'NS0' // Tipo de Atividade E-Billing

	//               Sisjuri        , Protheus     , Force                             , Ajusta Tipo  , Executa fun��o
	aAdd( aDePara, { 'ID_DOCPADRAO' , 'NS0_CDOC'  , ''                                 , ''           , '' } )
	aAdd( aDePara, { 'ID_TPATV'     , 'NS0_CATIV' , ''                                 , ''           , '' } )
	aAdd( aDePara, { 'DESCRICAO'    , 'NS0_DESC'  , ''                                 , ''           , '' } )
	aAdd( aDePara, { ''             , 'NS0_CFASE' , {|| Space(TamSX3("NS0_CFASE")[1])} , ''           , '' } )
	aAdd( aDePara, { ''             , 'NS0_CTAREF', {|| Space(TamSX3("NS0_CTAREF")[1])}, ''           , '' } )
	aAdd( aDePara, { ''             , 'NS0_COD'   , {|| GETSXENUM("NS0", "NS0_COD")}   , ''           , {|| Iif(nI == 1, LimpaArray(),), GuardaInfo(aRegs[nI])} } )

ElseIf cTab == 'NS1' // Tipo de Atividade E-Billing (De-Para)

	//               Sisjuri                                , Protheus     , Force                                       , Ajusta Tipo , Executa fun��o
	aAdd( aDePara, { 'ID_DOCPADRAO'                         , 'NS1_CDOC'  , ''                                           , ''          , Nil          })
	aAdd( aDePara, { 'ID_TPATV'                             , 'NS1_CATIV' , {|| BuscaInfo(aRegs[nI][1], aRegs[nI][2], 6)}, ''          , Nil          })
	aAdd( aDePara, { 'TIPOATIV'                             , 'NS1_CATIVJ', bGetTpAtiv                                   , ''          , Nil          })
	aAdd( aDePara, { ''                                     , 'NS1_COD'   , {|| GETSXENUM("NS1", "NS1_COD")}             , ''          , Nil          })
    aAdd( aDePara, { "ID_DOCPADRAO||'|'||TO_CHAR(TIPOATIV)" , ''          , ''                                           , ''          , bMigraTpAtiv })

ElseIf cTab == 'NS3' // Tipo de Despesa E-Billing

	//               Sisjuri        , Protheus     , Force                             , Ajusta Tipo , Executa fun��o
	aAdd( aDePara, { 'ID_DOCPADRAO' , 'NS3_CDOC'  , ''                                 , ''          , '' } )
	aAdd( aDePara, { 'ID_TPDESP'    , 'NS3_CDESP' , ''                                 , ''          , '' } )
	aAdd( aDePara, { 'DESCRICAO'    , 'NS3_DESC'  , ''                                 , ''          , '' } )
	aAdd( aDePara, { ''             , 'NS3_COD'   , {|| GETSXENUM("NS3", "NS3_COD")}   , ''          , {|| Iif(nI == 1, LimpaArray(),), GuardaInfo(aRegs[nI])} } )

ElseIf cTab == 'NS4' // Tipo de Despesa E-Billing (De-Para)

	//               Sisjuri        , Protheus     , Force                                       , Ajusta Tipo
	aAdd( aDePara, { 'ID_DOCPADRAO' , 'NS4_CDOC'  , ''                                           , '' } )
	aAdd( aDePara, { 'ID_TPDESP'    , 'NS4_CDESP' , {|| BuscaInfo(aRegs[nI][1], aRegs[nI][2], 4)}, '' } )
	aAdd( aDePara, { 'TIPODESP'     , 'NS4_CDESPJ', bTipoDesp                                    , '' } )
	aAdd( aDePara, { ''             , 'NS4_COD'   , {|| GETSXENUM("NS4", "NS4_COD")}             , '' } )

ElseIf cTab == 'NR7' // Conf. Servidores de E-mail

	//               Sisjuri        , Protheus    , Force                   , Ajusta Tipo
	aAdd( aDePara, { 'ID_SERVIDOR'  , 'NR7_COD'   , ''                      , 'Str' } )
	aAdd( aDePara, { 'DESCRICAO'    , 'NR7_DESC'  , ''                      , '' } )
	aAdd( aDePara, { 'ENDERECO'     , 'NR7_ENDERE', ''                      , '' } )
	aAdd( aDePara, { 'PORTA'        , 'NR7_PORTA' , ''                      , '' } )
	aAdd( aDePara, { 'AUTENTICACAO' , 'NR7_AUTENT', {{"S", "1"}, {"N", "2"}}, '' } )

ElseIf cTab == 'NR8' // Conf. Servidores de E-mail

	//               Sisjuri       , Protheus    , Force                                               , Ajusta Tipo
	aAdd( aDePara, { 'ID_USUARIO'  , 'NR8_COD'   , ''                                                  , 'Str' } )
	aAdd( aDePara, { 'NOMEUSUARIO' , 'NR8_DESC'  , {|| Substr(xValSisJur, 1, TamSX3("NR8_DESC")[1])}   , '' } )
	aAdd( aDePara, { 'EMAIL'       , 'NR8_EMAIL' , ''                                                  , '' } )
	aAdd( aDePara, { 'SENHA'       , 'NR8_SENHA' , ''                                                  , '' } )
	aAdd( aDePara, { 'TIPO'        , 'NR8_TIPO'  , {{"F", "1"}, {"M", "2"}}                            , '' } )
	aAdd( aDePara, { 'ID_SERVIDOR' , 'NR8_CSERVI', ''                                                  , 'Str' } )

ElseIf cTab == 'NRU' // Config. de Envio de E-mail

	//               Sisjuri      , Protheus    , Force                                               , Ajusta Tipo
	aAdd( aDePara, { 'ID'         , 'NRU_COD'   , ''                                                  , 'Str' } )
	aAdd( aDePara, { 'DESCRICAO'  , 'NRU_DESC'  , ''                                                  , '' } )
	aAdd( aDePara, { 'CORPO'      , 'NRU_CORPO' , ''                                                  , '' } )
	aAdd( aDePara, { 'ATIVO'      , 'NRU_ATIVA' , {{"S", "1"}, {"N", "2"}}                            , '' } )
	aAdd( aDePara, { 'FORMA_PGTO' , 'NRU_FRMPGO', {|| Iif(Empty(xValSisJur), "1", xValSisJur)}        , '' } )
	aAdd( aDePara, { 'EMAILCC'    , 'NRU_CC'    , {|| Substr(xValSisJur, 1, TamSX3("NRU_CC")[1])}     , '' } )
	aAdd( aDePara, { 'EMAILCCO'   , 'NRU_CCO'   , {|| Substr(xValSisJur, 1, TamSX3("NRU_CCO")[1])}    , '' } )
	aAdd( aDePara, { 'CONFLEITURA', 'NRU_CONLEI', {{"S", "1"}, {"N", "2"}}                            , '' } )
	aAdd( aDePara, { 'PRIORIDADE' , 'NRU_PRIOR ', {{0, "1"}, {1, "2"}, {2, "3"}}                      , '' } )
	aAdd( aDePara, { 'REGISTRO'   , 'NRU_EMAILR', {{"C", "3"}, {"N", "1"}, {"R", "2"}, {"R+", "4"}}   , '' } )

ElseIf cTab == 'NR5' // Desc Tp Atividade por Idioma

	//               Sisjuri      , Protheus    , Force                              , Ajusta Tipo
	aAdd( aDePara, { 'TIPOATV'    , 'NR5_CTATV ', ''                                 , 'Str' } )
	aAdd( aDePara, { 'IDIOMA'     , 'NR5_CIDIOM', ''                                 , '' } )
	aAdd( aDePara, { 'DESCRICAO'  , 'NR5_DESC'  , ''                                 , '' } )
	aAdd( aDePara, { 'TEXTOPADRAO', 'NR5_TXTPAD', ''                                 , '' } )
	aAdd( aDePara, { ''           , 'NR5_COD'   , {|| GETSXENUM("NR5", "NR5_COD")}   , '' } )

ElseIf cTab == 'NR4' // Desc Tp Desp por Idioma

	//               Sisjuri      , Protheus    , Force                              , Ajusta Tipo
	aAdd( aDePara, { 'TIPODESP'   , 'NR4_CTDESP', bTipoDesp                          , '' } )
	aAdd( aDePara, { 'IDIOMA'     , 'NR4_CIDIOM', ''                                 , '' } )
	aAdd( aDePara, { 'DESCRICAO'  , 'NR4_DESC'  , ''                                 , '' } )
	aAdd( aDePara, { ''           , 'NR4_COD'   , {|| GETSXENUM("NR4", "NR4_COD")}   , '' } )

ElseIf cTab == 'NRM' // Hist�rico de Tipo de Despesas

	//               Sisjuri         , Protheus    , Force                               , Ajusta Tipo
	aAdd( aDePara, { 'TIPO'          , 'NRM_CTIPO' , ''                                  , 'Str' } )
	aAdd( aDePara, { 'ANO_MES'       , 'NRM_AMINI' , {|| StrTran(xValSisJur, "-", "")}   , '' } )
	aAdd( aDePara, { 'ANO_MES'       , 'NRM_AMFIM' , {|| StrTran(xValSisJur, "-", "")}   , '' } )
	aAdd( aDePara, { 'PERC_CORRECAO' , 'NRM_PCORRE', ''                                  , '' } )
	aAdd( aDePara, { 'VALOR_UNITARIO', 'NRM_VALORU', ''                                  , '' } )

ElseIf cTab == 'NRJ' // Tp de Relat�rio de Faturamento

	//               Sisjuri         , Protheus    , Force                                          , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'CODIGO'        , 'NRJ_COD'   , bTpRptFat                                      , ''         ,           } )
	aAdd( aDePara, { 'CODIGO'        , ''          ,                                                , ''         , bMigraNRJ } )
	aAdd( aDePara, { 'DESCRICAO'     , 'NRJ_DESC'  , ''                                             , ''         ,           } )
	aAdd( aDePara, { ''              , 'NRJ_ATIVO' , '1'                                            , ''         ,           } )
	aAdd( aDePara, { ''              , 'NRJ_ARQ'   , {|| Space(TamSX3("NRJ_ARQ")[1])}               , ''         ,           } )

ElseIf cTab == 'NSO' // Tp Protocolo de Faturas

	//               Sisjuri           , Protheus    , Force                               , Ajusta Tipo
	aAdd( aDePara, { "LPAD(ID_TIPOPROTOCOLO,04,'0')", 'NSO_COD'   , ''                                  , '' } )
	aAdd( aDePara, { 'TIPOPROTOCOLO'                , 'NSO_DESC'  , ''                                  , '' } )
	aAdd( aDePara, { ''                             , 'NSO_ATIVO' , '1'                                 , '' } )
	aAdd( aDePara, { ''                             , 'NSO_ARQ'   , {|| Space(TamSX3("NSO_ARQ")[1])}    , '' } )

ElseIf cTab == 'OH2' // Sugest�o de T�tulo

	//               Sisjuri      , Protheus    , Force                               , Ajusta Tipo
	aAdd( aDePara, { 'COD_TITULO' , 'OH2_COD'   , ''                                  , 'Str' } )
	aAdd( aDePara, { 'TITULO'     , 'OH2_TITULO', ''                                  , '' } )
	aAdd( aDePara, { ''           , 'OH2_ATIVO' , '1'                                 , '' } )

ElseIf cTab == 'SA6' // Bancos

	//               Sisjuri        , Protheus       , Force                                                                , Ajusta Tipo  , Executa fun��o
	aAdd( aDePara, { 'BANCCODIGO'   , 'A6_COD'       , ''                                                                   , ''           , Nil        } )
	aAdd( aDePara, { 'CTACAGENCIA'  , 'A6_AGENCIA'   , {|| Substr(StrTran(xValSisJur, "-", ""), 1, TamSX3("A6_AGENCIA")[1])}, ''           , Nil        } )
	aAdd( aDePara, { 'CTACCONTA'    , 'A6_NUMCON'    , {|| Substr(StrTran(xValSisJur, "-", ""), 1, TamSX3("A6_NUMCON")[1])} , ''           , Nil        } )
	aAdd( aDePara, { 'CTACDESCRICAO', 'A6_NOME'      , ''                                                                   , ''           , Nil        } )
	aAdd( aDePara, { 'CTACADM'      , 'A6_NREDUZ'    , ''                                                                   , ''           , Nil        } )
	aAdd( aDePara, { 'CTACENDERECO' , 'A6_END'       , ''                                                                   , ''           , Nil        } )
	aAdd( aDePara, { 'CTACCEP'      , 'A6_CEP'       , ''                                                                   , ''           , Nil        } )
	aAdd( aDePara, { 'ESTNCODIGO'   , 'A6_EST'       , ''                                                                   , 'Str'        , Nil        } )
	aAdd( aDePara, { 'CTACCONTATO'  , 'A6_CONTATO'   , ''                                                                   , ''           , Nil        } )
	aAdd( aDePara, { 'CTANLIMITE'   , 'A6_LIMCRED'   , ''                                                                   , ''           , Nil        } )
	aAdd( aDePara, { 'MOECCODIGO'   , 'A6_MOEDA'     , bGetMoeFin                                                           , ''           , Nil        } )
	aAdd( aDePara, { 'CTACDESATIVA' , 'A6_BLOCKED'   , {{"S", "1"}, {"N", "2"}}                                             , ''           , Nil        } )
	aAdd( aDePara, { 'CTANCODIG'    , ''             , ''                                                                   , ''           , bMigraBank } )

ElseIf cTab == 'CTT' // Centro de Custos

	//               Sisjuri             , Protheus     , Force                                                 , Ajusta Tipo
	aAdd( aDePara, { 'NOME'              , 'CTT_DESC01' , {|| Substr(xValSisJur, 1, TamSX3("CTT_DESC01")[1])}   , '', Nil 		  } )
	aAdd( aDePara, { 'ESCRITORIO'        , 'CTT_CESCRI' , ''                                                    , '', Nil 		  } )
	aAdd( aDePara, { 'RESPONSAVEL'       , 'CTT_CPART'  , bGetPart                                              , '', Nil 		  } )
	aAdd( aDePara, { 'STATUS'            , 'CTT_BLOQ '  , {{"I", "1"}, {"A", "2"}, {"", "2"}}                   , '', Nil 		  } )
	aAdd( aDePara, { 'SIGLA||ESCRITORIO' , 'CTT_CUSTO'  , {|| StrTran(xValSisJur, ' ', '')}                     , '', Nil         } )
	aAdd( aDePara, { 'SIGLA'             , ''           ,                                                       , '', bMigraCusto } )
	aAdd( aDePara, { 'DATA_INATIVO'      , 'CTT_DTBLIN' , ''                                                    , '', Nil         } )
	aAdd( aDePara, { 'EMAILGRUPO'        , 'CTT_EMAIL ' , ''                                                    , '', Nil         } )
	aAdd( aDePara, { ''                  , 'CTT_CLASSE' , '2'                                                   , '', Nil         } )

ElseIf cTab == 'OHE' // Respons�veis X C.Custo

		//               Sisjuri          , Protheus     , Force                                                                 , Ajusta Tipo
		aAdd( aDePara, { 'COD_ADVG'       , 'OHE_CPART ' , ''                                                                    , '' } )
		aAdd( aDePara, { 'GRUPOJURIDICO'  , 'OHE_CCUST'  , ''                                                                    , '' } )
		
ElseIf cTab == 'FLP' // Aprovador por Centro de Custo

		//               Sisjuri          , Protheus     , Force                                                                 , Ajusta Tipo
		aAdd( aDePara, { 'COD_ADVG'       , 'FLP_CODAPR' , ''                                                                    , '' } )
		aAdd( aDePara, { 'GRUPOJURIDICO'  , 'FLP_CCUSTO' , ''                                                                    , '' } )

ElseIf cTab == 'NW9' // Feriado

	//               Sisjuri      , Protheus     , Force                                           , Ajusta Tipo
	aAdd( aDePara, { ''           , 'NW9_COD   ' , {|| GETSXENUM("NW9", "NW9_COD")}                , '' } )
	aAdd( aDePara, { 'ESCRITORIO' , 'NW9_CESCR'  , ''                                              , '' } )
	aAdd( aDePara, { 'DATA'       , 'NW9_DATA'   , ''                                              , '' } )
	aAdd( aDePara, { 'DESCRICAO'  , 'NW9_DESC'   , ''                                              , '' } )

ElseIf cTab == 'NR6' // Ramal por Profissional

	//               Sisjuri      , Protheus     , Force                                           , Ajusta Tipo
	aAdd( aDePara, { 'ID_CADRAMALPROF', 'NR6_COD'  , ''                                            , 'Str' } )
	aAdd( aDePara, { 'RAMAL'          , 'NR6_RAMAL', ''                                            , ''    } )
	aAdd( aDePara, { 'COD_ADVG'       , 'NR6_CPART', bGetPart                                      , ''    } )
	aAdd( aDePara, { ''               , 'NR6_TIPO' , '1'                                           , ''    } )

ElseIf cTab == 'OHL' // Projetos/Finalidades

	//               Sisjuri      , Protheus     , Force                                                 , Ajusta Tipo
	aAdd( aDePara, { 'ID_PROJETO' , 'OHL_CPROJE' , {|| StrZero(xValSisJur, TamSX3("OHL_CPROJE")[1])}     , '' } )
	aAdd( aDePara, { 'DESCRICAO'  , 'OHL_DPROJE' , {|| Substr(xValSisJur, 1, TamSX3("OHL_DPROJE")[1])}   , '' } )
	aAdd( aDePara, { 'ANO'        , 'OHL_ANOPRJ' , ''                                                    , 'Val' } )
	aAdd( aDePara, { 'SITUACAO'   , 'OHL_SITUAC' , {{"A", "2"}, {"B", "3"}, {"C", "4"}}                  , '' } )

ElseIf cTab == 'OHM' // Itens Projeto

	//               Sisjuri           , Protheus     , Force                                                 , Ajusta Tipo
	aAdd( aDePara, { 'ID_PROJETO_ITEM' , 'OHM_ITEM  ' , ''                                                    , 'Str' } )
	aAdd( aDePara, { 'ID_PROJETO'      , 'OHM_CPROJE' , {|| StrZero(xValSisJur, TamSX3("OHM_CPROJE")[1])}     , 'Str' } )
	aAdd( aDePara, { 'DESCRICAO'       , 'OHM_DITEM'  , {|| Substr(xValSisJur, 1, TamSX3("OHM_DITEM")[1])}    , '' } )
	aAdd( aDePara, { 'TIPO'            , 'OHM_TIPO'   , {{"D", "1"}, {"I", "2"}, {"E", "3"}}                  , '' } )

ElseIf cTab == 'NRX' // Empresa E-billing

	//               Sisjuri        , Protheus     , Force                                                 , Ajusta Tipo
	aAdd( aDePara, { 'ID_EMPRESA'   , 'NRX_COD'    , ''                                                    , 'Str' } )
	aAdd( aDePara, { 'NOME'         , 'NRX_DESC'   , ''                                                    , ''    } )
	aAdd( aDePara, { 'ID_DOCPADRAO' , 'NRX_CDOC'   , ''                                                    , ''    } )

ElseIf cTab == 'NTQ' // Escrit�rio E-billing

	//               Sisjuri           , Protheus     , Force                                                 , Ajusta Tipo
	aAdd( aDePara, { 'ID_EMPRESA'      , 'NTQ_CEMP'   , ''                                                    , 'Str' } )
	aAdd( aDePara, { 'CODIGOESCRITEMP' , 'NTQ_CODIGO' , ''                                                    , ''    } )
	aAdd( aDePara, { 'ORGNCODIG'       , 'NTQ_CESCR ' , bGetEscrt                                             , ''    } )

ElseIf cTab == 'OHK' // Banco x Escrit�rios

	//               Sisjuri      , Protheus     , Force                                                 , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'SIGLA'      , 'OHK_CESCRI' , ''                                                    , ''         , Nil })
	aAdd( aDePara, { 'CTANCODIG'  , 'OHK_CBANCO' , bGetCdBank                                            , ''         , Nil })
	aAdd( aDePara, { 'CTANCODIG'  , 'OHK_CAGENC' , bGetCdAgen                                            , ''         , Nil })
	aAdd( aDePara, { 'CTANCODIG'  , 'OHK_CCONTA' , bGetCdCont                                            , ''         , Nil })

ElseIf cTab == 'OH6' // Cabe�alho da tabela de Rateio

	//               Sisjuri                 , Protheus     , Force                                                               , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'RATNCODIG'            , 'OH6_CODIGO'  , {|| StrZero(xValSisJur, TamSX3("OH6_CODIGO")[1])}                   , 'Str'      ,           })
	aAdd( aDePara, { 'RATNCODIG'            , ''            , ''                                                                  , 'Str'      , bMigraRate})
	aAdd( aDePara, { 'RATCDESCR'            , 'OH6_DESCRI'  , {|| Substr(xValSisJur, 1, TamSX3("OH6_DESCRI")[1])}                 , ''         , Nil       })
	aAdd( aDePara, { CaseRateio()           , 'OH6_TIPO'    , ''                                                                  , ''         , Nil       })
	aAdd( aDePara, { 'RATCINATIVO'          , 'OH6_ATIVO'   , "1"                                                                 , ''         , Nil       })

ElseIf cTab == 'OH7' // Detalhe do Rateio

	//               Sisjuri                 , Protheus     , Force                                             , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'RATNCODIG'            , 'OH7_CODRAT'  , {|| GetDePara("FINANCE.RATEIO", xValSisJur, "")}  , 'Str'      , Nil })
	aAdd( aDePara, { 'DETRNCODIG'           , 'OH7_CODDET'  , ''                                                , 'Str'      , Nil })
	aAdd( aDePara, { 'ESCRITORIO'           , 'OH7_CESCRI'  , ''                                                , ''         , Nil })
	aAdd( aDePara, { 'SETOR||ESCRITORIO'    , 'OH7_CCCUST'  , {|| StrTran(xValSisJur, ' ', '')}                 , ''         , Nil }) 
	aAdd( aDePara, { 'COD_ADVG'             , 'OH7_CPARTI'  , bGetPart                                          , ''         , Nil })
	aAdd( aDePara, { 'PERCENTUAL'           , 'OH7_PERCEN'  , ''                                                , ''         , Nil })

ElseIf cTab == 'OH8' // Hist�rico do Detalhe do Rateio

	//               Sisjuri                 , Protheus     , Force                                              , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'RATNCODIG'            , 'OH8_CODRAT'  , {|| GetDePara("FINANCE.RATEIO", xValSisJur, "")}   , 'Str'      , Nil })
	aAdd( aDePara, { 'DETRNCODIG'           , 'OH8_CODDET'  , ''                                                 , 'Str'      , Nil })
	aAdd( aDePara, { 'ESCRITORIO'           , 'OH8_CESCRI'  , ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'SETOR||ESCRITORIO'    , 'OH8_CCCUST'  , {|| StrTran(xValSisJur, ' ', '')}                  , ''         , Nil }) 
	aAdd( aDePara, { 'COD_ADVG'             , 'OH8_CPARTI'  , bGetPart                                           , ''         , Nil })
	aAdd( aDePara, { 'ANOMES'               , 'OH8_AMINI '  , {|| StrTran(xValSisJur, "-", "")}                  , ''         , Nil })
	aAdd( aDePara, { 'ANOMES'               , 'OH8_AMFIM '  , {|| StrTran(xValSisJur, "-", "")}                  , ''         , Nil })
	aAdd( aDePara, { 'PERCENTUAL'           , 'OH8_PERCEN'  , ''                                                 , ''         , Nil })

ElseIf cTab == 'OHA' // Hist�rico Padr�o

	//               Sisjuri                 , Protheus     , Force                                                               , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'HPANCODIGO'            , 'OHA_COD'    , {|| StrZero(xValSisJur, TamSX3("OHA_COD")[1])}                      , 'Str'      , Nil       })
	aAdd( aDePara, { 'HPANCODIGO'            , ''           , ''                                                                  , 'Str'      , bMigraHist})
	aAdd( aDePara, { 'HPACDESCRICAO'         , 'OHA_RESUMO' , ''                                                                  , ''         , Nil       })
	aAdd( aDePara, { 'HPACFLAGCP'            , 'OHA_CTAPAG' , {{"S", "1"}, {"N", "2"}, {"", "1"}}                                 , ''         , Nil       })
	aAdd( aDePara, { 'HPACFLAGCR'            , 'OHA_CTAREC' , {{"S", "1"}, {"N", "2"}, {"", "1"}}                                 , ''         , Nil       })
	aAdd( aDePara, { 'HPACFLAGLANCAMENTO'    , 'OHA_LANCAM' , {{"S", "1"}, {"N", "2"}, {"", "1"}}                                 , ''         , Nil       })
	aAdd( aDePara, { 'HPACFLAGCOBRANCA'      , 'OHA_COBRAN' , {{"S", "1"}, {"N", "2"}, {"", "1"}}                                 , ''         , Nil       })
	aAdd( aDePara, { 'HPACHISTCONTA'         , 'OHA_TEXTO'  , ''                                                                  , ''         , Nil       })

ElseIf cTab == 'SED' // Naturezas

	//               Sisjuri                 , Protheus     , Force                                                                    , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'FINANCE.PLANOCONTAS.PCTCNUMEROCONTA'  , 'ED_CODIGO'  , bNatureza                                                 , ''         , Nil })
	aAdd( aDePara, { 'PCTCTITULO'            , 'ED_DESCRIC' , {|| Substr(xValSisJur, 1, TamSX3("ED_DESCRIC")[1])}                      , ''         , Nil })
	aAdd( aDePara, { 'TPCCCODIGO'            , 'ED_TPCOJR'  , aOpcTpCont                                                               , ''         , Nil })
	aAdd( aDePara, { 'PLCCCONTA'             , 'ED_CONTA'   , ''                                                                       , ''         , Nil })
	aAdd( aDePara, { 'PCTCFLAGCP'            , 'ED_CPJUR'   , {{"S", "1"}, {"N", "2"}, {"", "2"}}                                      , ''         , Nil })
	aAdd( aDePara, { 'PCTCFLAGCR'            , 'ED_CRJUR'   , {{"S", "1"}, {"N", "2"}, {"", "2"}}                                      , ''         , Nil })
	aAdd( aDePara, { 'PCTCFLAGBANCO'         , 'ED_BANCJUR' , {{"S", "1"}, {"N", "2"}, {"", "2"}}                                      , ''         , Nil })
	aAdd( aDePara, { 'CTANCODIG'             , 'ED_CBANCO'  , bGetCdBank                                                               , ''         , Nil })
	aAdd( aDePara, { 'CTANCODIG'             , 'ED_CAGENC'  , bGetCdAgen                                                               , ''         , Nil })
	aAdd( aDePara, { 'CTANCODIG'             , 'ED_CCONTA'  , bGetCdCont                                                               , ''         , Nil })
	aAdd( aDePara, { CaseCCusto()            , 'ED_CCJURI'  , ''                                                                       , ''         , Nil })
	aAdd( aDePara, { 'PCTCFLAGCASHFLOW'      , 'ED_CFJUR'   , {{"S", "1"}, {"N", "2"}, {"", "2"}}                                      , ''         , Nil })
	aAdd( aDePara, { 'PCTCNUMEROCONTAPAI'    , 'ED_PAI'     , bGetNatPai                                                               , ''         , Nil })
	aAdd( aDePara, { cNivelNat               , 'ED_TIPO'    , ''                                                                       , ''         , Nil })
	aAdd( aDePara, { 'PCTCFLAGDESATIVACONTA' , 'ED_MSBLQL'  , {{"S", "1"}, {"N", "2"}, {"", "2"}}                                      , ''         , Nil })
	aAdd( aDePara, { ''                      , 'ED_COND'    , {|| Iif(Substr(aRegs[nI][3], 1, TamSX3("ED_COND")[1]) == "E", "R", "D")} , ''         , Nil })
	aAdd( aDePara, { ''                      , 'ED_CALCIRF' , "N"                                                                      , ''         , Nil }) // TODO: Ajustar reten��o de impostos antes da migra��o da fatura
	aAdd( aDePara, { ''                      , 'ED_CALCISS' , "N"                                                                      , ''         , Nil }) // TODO: Ajustar reten��o de impostos antes da migra��o da fatura
	aAdd( aDePara, { ''                      , 'ED_CALCINS' , "N"                                                                      , ''         , Nil }) // TODO: Ajustar reten��o de impostos antes da migra��o da fatura
	aAdd( aDePara, { ''                      , 'ED_CALCCSL' , "N"                                                                      , ''         , Nil }) // TODO: Ajustar reten��o de impostos antes da migra��o da fatura
	aAdd( aDePara, { ''                      , 'ED_CALCCOF' , "N"                                                                      , ''         , Nil }) // TODO: Ajustar reten��o de impostos antes da migra��o da fatura
	aAdd( aDePara, { ''                      , 'ED_CALCPIS' , "N"                                                                      , ''         , Nil }) // TODO: Ajustar reten��o de impostos antes da migra��o da fatura
	aAdd( aDePara, { ''                      , 'ED_DEDPIS'  , "2"                                                                      , ''         , Nil }) // TODO: Ajustar reten��o de impostos antes da migra��o da fatura
	aAdd( aDePara, { ''                      , 'ED_DEDCOF'  , "2"                                                                      , ''         , Nil }) // TODO: Ajustar reten��o de impostos antes da migra��o da fatura
	aAdd( aDePara, { 'CODIGO'                , 'ED_CMOEJUR' , bGetMoeda                                                                , ''         , Nil })
	aAdd( aDePara, { 'RATNCODIG'             , 'ED_RATJUR'  , bGetTabRat                                                               , ''         , Nil })
	aAdd( aDePara, { ''                      , 'ED_MOVBCO'  , "1"                                                                      , ''         , Nil })

ElseIf cTab == 'SA2' // Fornecedor

	//               Sisjuri                   , Protheus     , Force                                                      , Ajusta Tipo, Executa fun��o
	//aAdd( aDePara, { ''                        , 'A2_FILIAL'  , ''                                                       , ''         , Nil       } ) // Fornecedor deve ser compartilhado no Protheus
	aAdd( aDePara, { 'FORNECEDOR.FORNCODIGO'   , 'A2_COD'     , {|| StrZero(xValSisJur, TamSX3("A2_COD")[1])}              , ''         , {|| Iif(xValSisJur != aFornece[1], aFornece[2] := "00",), aFornece[1] := xValSisJur} } )
	aAdd( aDePara, { 'FORNECEDOR.FORNCODIGO'   , 'A2_LOJA'    , {|| BuscaLoja("SA2")}                                      , ''         , Nil       } )
	aAdd( aDePara, { cCpoForEnd                , ''           ,  ''                                                        , ''         , bMigraForn} )
	aAdd( aDePara, { 'FORCDESCRICAO'           , 'A2_NOME'    , {|| Upper(Substr(xValSisJur, 1, TamSX3("A2_NOME")[1]))}    , ''         , Nil       } )
	aAdd( aDePara, { 'NOMEFANTASIA'            , 'A2_NREDUZ'  , {|| Upper(Substr(xValSisJur, 1, TamSX3("A2_NREDUZ")[1]))}  , ''         , Nil       } )
	aAdd( aDePara, { 'FORCCONTATO'             , 'A2_CONTATO' , {|| Upper(Substr(xValSisJur, 1, TamSX3("A2_CONTATO")[1]))} , ''         , Nil       } )
	aAdd( aDePara, { 'FORCTELEFONE'            , 'A2_TEL'     , ''                                                         , ''         , Nil       } )
	aAdd( aDePara, { 'FORCFAX'                 , 'A2_FAX'     , {|| Substr(xValSisJur, 1, TamSX3("A2_FAX")[1])}            , ''         , Nil       } )
	aAdd( aDePara, { 'FORCCNPJCPF'             , 'A2_CGC'     , bConvCGC                                                   , ''         , Nil       } )
	aAdd( aDePara, { 'FORCREGIME'              , 'A2_TIPO'    , {{"1", "F"}, {"2", "J"}, {"3", "X"}, {"", "2"}}            , ''         , Nil       } )
	aAdd( aDePara, { 'FORCINSS'                , 'A2_CODINSS' , ''                                                         , 'Str'      , Nil       } )
	aAdd( aDePara, { 'FORCEMAIL'               , 'A2_EMAIL'   , {|| Substr(xValSisJur, 1, TamSX3("A2_EMAIL")[1])}          , ''         , Nil       } )
	aAdd( aDePara, { 'FORCINSCESTADUAL'        , 'A2_INSCR'   , {|| Substr(xValSisJur, 1, TamSX3("A2_INSCR")[1])}          , ''         , Nil       } )
	aAdd( aDePara, { 'FORCCCM'                 , 'A2_INSCRM'  , {|| Substr(xValSisJur, 1, TamSX3("A2_INSCRM")[1])}         , ''         , Nil       } )
	aAdd( aDePara, { 'FORCWEBSITE'             , 'A2_HPAGE'   , {|| Substr(xValSisJur, 1, TamSX3("A2_HPAGE")[1])}          , ''         , Nil       } )
	aAdd( aDePara, { 'FORCINATIVO'             , 'A2_MSBLQL'  , {{"S", "1"}, {"N", "2"}, {"", "2"}}                        , ''         , Nil       } )
	aAdd( aDePara, { 'NIF_FORNECEDOR'          , 'A2_NIFEX'   , {|| Substr(xValSisJur, 1, TamSX3("A2_NIFEX")[1])}          , ''         , Nil       } )
	aAdd( aDePara, { 'ID_MOTIVO_NIF'           , 'A2_MOTNIF'  , {|| Iif(Empty(xValSisJur), "1", AllTrim(Str(xValSisJur)))} , ''         , Nil       } )
	aAdd( aDePara, { 'FORCENDERECO'            , 'A2_END'     , {|| Upper(Substr(xValSisJur, 1, TamSX3("A2_END")[1]))}     , ''         , Nil       } )
	aAdd( aDePara, { 'FORCBAIRRO'              , 'A2_BAIRRO'  , {|| Upper(Substr(xValSisJur, 1, TamSX3("A2_BAIRRO")[1]))}  , ''         , Nil       } )
	aAdd( aDePara, { 'FORCCEP'                 , 'A2_CEP'     , {|| Substr(xValSisJur, 1, TamSX3("A2_CEP")[1])}            , ''         , Nil       } )
	aAdd( aDePara, { 'FORNENDERECO.PAINCODIGO' , 'A2_CODPAIS' , {|| GetDePara("RCR.PAIS", xValSisJur, "")}                 , ''         , Nil       } )
	aAdd( aDePara, { 'CIDADE.CIDNCODIGO'       , 'A2_COD_MUN' , {|| GetDePara("RCR.CIDADE", xValSisJur, "")}               , ''         , Nil       } )
	aAdd( aDePara, { 'CIDADE.CIDCDESCRICAO'    , 'A2_MUN'     , {|| Alltrim(NoAcento(Upper(xValSisJur)))}                  , ''         , Nil       } )
	aAdd( aDePara, { 'RCR.ESTADO.ESTCSIGLA'    , 'A2_EST'     , ''                                                         , ''         , Nil       } )
	aAdd( aDePara, { 'FORNNUMERO'              , 'A2_NR_END'  , ''                                                         , ''         , Nil       } )
	aAdd( aDePara, { 'FORCCOMPLEMENTO'         , 'A2_COMPLEM' , ''                                                         , ''         , Nil       } )

ElseIf cTab == 'FIL|SA2' // Bancos do Fornecedor
// {|| MigraDePar('FINANCE.FORNECEDOR', aFornece[1] + xValSisJur, '', 'FORNCODIGO+FORNENDCODIGO', cTab, aFornece[1] + aFornece[2], 'A2_COD+A2_LOJA')}
	aAdd( aDePara, { 'BANCCODIGO'         , "FIL_BANCO " , ''                                                   , '' , Nil })
	aAdd( aDePara, { cCpoForEnd           , "FIL_FORNEC" , bGetCodFor                                           , '' , Nil })
	aAdd( aDePara, { cCpoForEnd           , "FIL_LOJA  " , bGetCodLoj                                           , '' , Nil })
	aAdd( aDePara, { 'FORCAGENCIA'        , "FIL_AGENCI" , {|| Substr(xValSisJur, 1, TamSX3("FIL_AGENCI")[1])}  , '' , Nil })
	aAdd( aDePara, { 'FORCCONTA'          , "FIL_CONTA " , {|| Substr(xValSisJur, 1, TamSX3("FIL_CONTA")[1])}   , '' , Nil })
	aAdd( aDePara, { ''                   , "FIL_TIPO  " , '2'                                                  , '' , Nil })
	aAdd( aDePara, { ''                   , "FIL_MOEDA " , 1                                                    , '' , Nil })
	aAdd( aDePara, { 'FORNCONTAPOUPANCA'  , "FIL_TIPCTA" , {{"S", "2"}, {"N", "1"}, {"", "1"}}                  , '' , Nil })
	aAdd( aDePara, { ''                   , 'FIL_DETRAC' , '0'                                                  , '' , Nil })
	aAdd( aDePara, { ''                   , 'FIL_MOVCTO' , ''                                                   , '' , Nil })

ElseIf cTab == 'RD0|NUR' // Participante

	//               Sisjuri                , Protheus     , Force, Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { ''                     , 'RD0_CODIGO' , {|| CriaVar("RD0_CODIGO", .T.)}                     , ''    , Nil       })
	aAdd( aDePara, { 'COD_ADVG'             , 'RD0_SIGLA'  , ''                                                  , ''    , bMigraPart})
	aAdd( aDePara, { 'NOME'                 , 'RD0_NOME'   , {|| Substr(xValSisJur, 1, TamSX3("RD0_NOME")[1])}   , ''    , Nil       })
	aAdd( aDePara, { 'ADVCCPF'              , 'RD0_CIC'    , bConvCIC                                            , ''    , Nil       })
	aAdd( aDePara, { 'ENDERECO'             , 'RD0_END'    , {|| Substr(xValSisJur, 1, TamSX3("RD0_END")[1])}    , ''    , Nil       })
	aAdd( aDePara, { 'ENDCOMPL'             , 'RD0_CMPEND' , {|| Substr(xValSisJur, 1, TamSX3("RD0_CMPEND")[1])} , ''    , Nil       })
	aAdd( aDePara, { 'ENDNUMERO'            , 'RD0_NUMEND' , {|| Substr(xValSisJur, 1, TamSX3("RD0_NUMEND")[1])} , ''    , Nil       })
	aAdd( aDePara, { 'TELEFONE'             , 'RD0_FONE'   , {|| Substr(xValSisJur, 1, TamSX3("RD0_FONE")[1])}   , ''    , Nil       })
	aAdd( aDePara, { 'BAIRRO'               , 'RD0_BAIRRO' , {|| Substr(xValSisJur, 1, TamSX3("RD0_BAIRRO")[1])} , ''    , Nil       })
	aAdd( aDePara, { 'CEP'                  , 'RD0_CEP'    , {|| Substr(xValSisJur, 1, TamSX3("RD0_CEP")[1])}    , ''    , Nil       })
	aAdd( aDePara, { 'FAX'                  , 'RD0_FAX'    , {|| Substr(xValSisJur, 1, TamSX3("RD0_FAX")[1])}    , ''    , Nil       })
	aAdd( aDePara, { 'TELEFONE2'            , 'RD0_CELCOM' , {|| Substr(xValSisJur, 1, TamSX3("RD0_CELCOM")[1])} , ''    , Nil       })
	aAdd( aDePara, { 'CELULAR'              , 'RD0_NUMCEL' , {|| Substr(xValSisJur, 1, TamSX3("RD0_NUMCEL")[1])} , ''    , Nil       })
	aAdd( aDePara, { 'CIDCDESCRICAO'        , 'RD0_MUN'    , ''                                                  , ''    , Nil       })
	aAdd( aDePara, { 'RCR.ESTADO.ESTCSIGLA' , 'RD0_UF'     , ''                                                  , ''    , Nil       })
	aAdd( aDePara, { 'SIGLA'                , 'RD0_TPJUR'  , {|| Iif(Empty(xValSisJur), "2", "1")}               , ''    , Nil       })
	aAdd( aDePara, { 'DATA_SAIDA'           , 'RD0_MSBLQL' , {|| Iif(Empty(xValSisJur), "2", "1")}               , ''    , Nil       })
	aAdd( aDePara, { ''                     , 'RD0_TIPO'   , '1'                                                 , ''    , Nil       })
	//aAdd( aDePara, { 'FORNCODIGO'           , 'RD0_FORNEC' , ''                                                , ''    , Nil       }) // NOTE: Fornecedor vai ser criado ap�s a importa��o dos participantes
	//aAdd( aDePara, { ''                     , 'RD0_LOJA'   , ''                                                , ''    , Nil       }) // NOTE: Fornecedor vai ser criado ap�s a importa��o dos participantes
	aAdd( aDePara, { 'SETOR'                , 'RD0_CC'     , ''                                                  , ''    , Nil       }) // TODO: Definir como vai pegar o c�digo do Centro de Custo
	aAdd( aDePara, { 'DATA_ENTRADA'         , 'RD0_DTADMI' , ''                                                  , ''    , Nil       })
	aAdd( aDePara, { 'DATA_SAIDA'           , 'RD0_DTADEM' , ''                                                  , ''    , Nil       })
	aAdd( aDePara, { 'EMAIL'                , 'RD0_EMAIL'  , ''                                                  , ''    , Nil       })
	//NUR
	aAdd( aDePara, { 'COD_ADVG'             , 'NUR_CPART'  , bGetPart                                            , ''    , Nil       })
	aAdd( aDePara, { 'ESCRITORIO'           , 'NUR_CESCR'  , ''                                                  , ''    , Nil       })
	aAdd( aDePara, { 'APELIDO'              , 'NUR_APELI'  ,                                                     , ''    , Nil       })
	aAdd( aDePara, { 'ORD_HIERARQUICA'      , 'NUR_ORDHIE' , {|| StrZero(xValSisJur, TamSX3("NUR_ORDHIE")[1])}   , ''    , Nil       })
	aAdd( aDePara, { 'HORAS_DIA'            , 'NUR_HRDIAD' , {|| StrZero(xValSisJur, 2) + "00"}                  , ''    , Nil       })
	aAdd( aDePara, { 'CATEGORIA'            , 'NUR_CCAT'   ,                                                     , ''    , Nil       })
	aAdd( aDePara, { 'CHAPA'                , 'NUR_CHAPA'  ,                                                     , ''    , Nil       })
	aAdd( aDePara, { 'PARTDISTRIB'          , 'NUR_PARTDI' , {{"S", "1"}, {"N", "2"}, {"", "2"}}                 , ''    , Nil       })
	aAdd( aDePara, { 'REQAPROV'             , 'NUR_APRTS'  , {{"S", "1"}, {"N", "2"}, {"", "2"}}                 , ''    , Nil       })
	aAdd( aDePara, { 'REVISOR'              , 'NUR_REVFAT' , {{"S", "1"}, {"N", "2"}, {"", "2"}}                 , ''    , Nil       })
	aAdd( aDePara, { ''                     , 'NUR_CASOEN' , "2"                                                 , ''    , Nil       })
	aAdd( aDePara, { ''                     , 'NUR_PERIOD' , "2"                                                 , ''    , Nil       })
	aAdd( aDePara, { ''                     , 'NUR_PERINC' , "2"                                                 , ''    , Nil       })
	aAdd( aDePara, { ''                     , 'NUR_PERALT' , "2"                                                 , ''    , Nil       })
	aAdd( aDePara, { ''                     , 'NUR_PEREXC' , "2"                                                 , ''    , Nil       })
	aAdd( aDePara, { 'SOCIO'                , 'NUR_SOCIO'  , {{"S", "1"}, {"N", "2"}, {"", "2"}}                 , ''    , Nil       })
	aAdd( aDePara, { ''                     , 'NUR_LCPRE'  , "1"                                                 , ''    , Nil       })
	aAdd( aDePara, { cCriaConta             , 'NUR_FORNAT' , {{"S", "1"}, {"N", "2"}, {"", "2"}}                 , ''    , Nil       })
	aAdd( aDePara, { 'OAB'                  , 'NUR_OAB'    ,                                                     , ''    , Nil       })
	aAdd( aDePara, { 'RECEBE_RATEIO_DESP'   , 'NUR_RATDES' , {{"S", "1"}, {"N", "2"}, {"", "1"}}                 , ''    , Nil       })
	aAdd( aDePara, { ''/*'TECNICO'*/        , 'NUR_TECNIC' , /*{{"S", "1"}, {"N", "2"}, {"", "1"}}*/"1"                 , ''    , Nil       })// TODO: Campo n�o existe no banco de dados dos SisJuri

ElseIf cTab == 'NSS' // Rateio Centros Custo Participante

	//               Sisjuri        , Protheus     , Force                                               , Ajusta Tipo , Executa fun��o
	aAdd( aDePara, { ''             , 'NSS_COD'    , {|| GETSXENUM("NSS", "NSS_COD") }                   , '' , {|| Iif(nI == 1, LimpaArray(),), GuardaInfo(aRegs[nI])} })
	aAdd( aDePara, { 'COD_ADVG'     , 'NSS_CPART'  , bGetPart                                            , '' , Nil })
	aAdd( aDePara, { 'ESCRITORIO'   , 'NSS_CESCR'  , ''                                                  , '' , Nil })
	aAdd( aDePara, { 'SETOR||ESCRITORIO', 'NSS_CC' , {|| StrTran(xValSisJur, ' ', '')}                   , '' , Nil })
	aAdd( aDePara, { 'PERCENTUAL'   , 'NSS_PERC'   , ''                                                  , '' , Nil })

ElseIf cTab == 'NVM' // Hist. Rateio Centros Custo Partic.

	//               Sisjuri        , Protheus     , Force                                               , Ajusta Tipo
	aAdd( aDePara, { 'ANO_MES'      , 'NVM_AMINI'  , {|| StrTran(xValSisJur, "-", "")}                   , '' })
	aAdd( aDePara, { 'ANO_MES'      , 'NVM_AMFIM'  , {|| StrTran(xValSisJur, "-", "")}                   , '' })
	aAdd( aDePara, { 'COD_ADVG'     , 'NVM_CPART'  , bGetPart                                            , '' })
	aAdd( aDePara, { 'ESCRITORIO'   , 'NVM_CESCR'  , ''                                                  , '' })
	aAdd( aDePara, { 'SETOR||ESCRITORIO', 'NVM_CC' , {|| StrTran(xValSisJur, ' ', '')}                   , '' })
	aAdd( aDePara, { 'PERCENTUAL'   , 'NVM_PERC'   , ''                                                  , '' })
	aAdd( aDePara, { ''             , 'NVM_COD'    , {|| BuscaInfo(aRegs[nI][1], aRegs[nI][3], 1)}       , '' })

ElseIf cTab == 'NTX' // Aus�ncias Participante

	//               Sisjuri        , Protheus     , Force                                               , Ajusta Tipo
	aAdd( aDePara, { 'DATA_INI'     , 'NTX_DATAI'  , ''                                                  , '' })
	aAdd( aDePara, { 'DATA_FIM'     , 'NTX_DATAF'  , ''                                                  , '' })
	aAdd( aDePara, { 'TIPO'         , 'NTX_TIPO'   , {{"FA", "1"}, {"FE", "2"}, {"L", "3"}}              , '' })
	aAdd( aDePara, { 'COD_ADVG'     , 'NTX_CPART'  , bGetPart                                            , '' })

ElseIf cTab == 'NUS' // Hist�rico Participante

	//               Sisjuri                , Protheus     , Force                                               , Ajusta Tipo
	aAdd( aDePara, { 'COD_ADVG'             , 'NUS_CPART'  , bGetPart                                            , ''})
	aAdd( aDePara, { 'ANO_MES'              , 'NUS_AMINI'  , {|| StrTran(xValSisJur, "-", "")}                   , ''})
	aAdd( aDePara, { 'ANO_MES'              , 'NUS_AMFIM'  , {|| StrTran(xValSisJur, "-", "")}                   , ''})
	aAdd( aDePara, { 'ESCRITORIO'           , 'NUS_CESCR'  , ''                                                  , ''})
	aAdd( aDePara, { 'CATEGORIA'            , 'NUS_CCAT'   , ''                                                  , ''})
	aAdd( aDePara, { 'SOCIO'                , 'NUS_SOCIO'  , {{"S", "1"}, {"N", "2"}, {"", "2"}}                 , ''})
	aAdd( aDePara, { 'HORAS_DIA'            , 'NUS_HRDIAD' , {|| StrZero(xValSisJur, 2) + "00"}                  , ''})
	aAdd( aDePara, { 'SETOR||ESCRITORIO'    , 'NUS_CC'     , {|| StrTran(xValSisJur, ' ', '')}                   , ''})
	aAdd( aDePara, { 'ORD_HIERARQUICA'      , 'NUS_ORDHIE' , {|| StrZero(xValSisJur, TamSX3("NUR_ORDHIE")[1])}   , ''})
	aAdd( aDePara, { 'RECEBE_RATEIO_DESP'   , 'NUS_RATDES' , {{"S", "1"}, {"N", "2"}, {"", "1"}}                 , ''})
	aAdd( aDePara, { ''/*'TECNICO'*/        , 'NUS_TECNIC' , /*{{"S", "1"}, {"N", "2"}, {"", "1"}}*/"1"          , ''}) // TODO: Campo n�o existe no banco de dados dos SisJuri
	aAdd( aDePara, { 'DATA_ENTRADA'         , 'NUS_DTADMI' , ''                                                  , ''})
	aAdd( aDePara, { 'DATA_SAIDA'           , 'NUS_DTADEM' , ''                                                  , ''})

// NOTE: FIL deve ser compartilhada
ElseIf cTab == 'FIL|RD0' // Bancos do Participante

	//               Sisjuri              , Protheus     , Force                                               , Aju Tipo , Executa fun��o
	aAdd( aDePara, { 'ADVCCODADVG'        , 'FIL_'       , ''                                                  , ''       , {|| BuscaForne(xValSisJur)} }) //Informa o inicio do campo para saber a tabela
	aAdd( aDePara, { 'BANCCODIGO'         , 'FIL_BANCO'  , ''                                                  , ''       , Nil       })
	aAdd( aDePara, { 'ACTNAGENCIA'        , 'FIL_AGENCI' , {|| Substr(xValSisJur, 1, TamSX3("FIL_AGENCI")[1])} , ''       , Nil       })
	aAdd( aDePara, { 'ACTCCONTACORRENTE'  , 'FIL_CONTA'  , {|| Substr(xValSisJur, 1, TamSX3("FIL_CONTA")[1])}  , ''       , Nil       })
	aAdd( aDePara, { 'ADVCCODADVG'        , 'FIL_FORNEC' , {|| aFornece[1]}                                    , ''       , Nil       })
	aAdd( aDePara, { 'ADVCCODADVG'        , 'FIL_LOJA'   , {|| aFornece[2]}                                    , ''       , Nil       })
	aAdd( aDePara, { 'ACTACPREFER'        , 'FIL_TIPO'   , {{"S", "1"}, {"N", "2"}, {"", "2"}}                 , ''       , Nil       })
	aAdd( aDePara, { ''                   , 'FIL_MOEDA'  , 1                                                   , ''       , Nil       })
	aAdd( aDePara, { ''                   , 'FIL_TIPCTA' , '1'                                                 , ''       , Nil       })
	aAdd( aDePara, { ''                   , 'FIL_DETRAC' , '0'                                                 , ''       , Nil       })
	aAdd( aDePara, { ''                   , 'FIL_MOVCTO' , ''                                                  , ''       , Nil       })

ElseIf cTab == 'OHC' // Eventos Financeiros

	//               Sisjuri                , Protheus      , Force                                                                , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'EVENCODIGO'           , 'OHC_COD'     , &('{|| StrZero(xValSisJur,' + cValToChar(TamSX3("OHC_COD")[1])+')}')                       , 'Str'      , Nil })
	aAdd( aDePara, { 'EVECDESCRICAO'        , 'OHC_DESCRI'  , ''                                                                   , ''         , Nil })
	aAdd( aDePara, { 'PCTCNUMEROCONTAORG'   , 'OHC_NATORI'  , &('{|| Substr(StrTran(xValSisJur, ".", ""), 1,' + cValToChar(TamSX3("OHC_NATORI")[1])+')}'), ''         , Nil })
	aAdd( aDePara, { 'PCTCNUMEROCONTADEST'  , 'OHC_NATDES'  , &('{|| Substr(StrTran(xValSisJur, ".", ""), 1,'+ cValToChar( TamSX3("OHC_NATDES")[1])+')}'), ''         , Nil })
	aAdd( aDePara, { 'HPANCODIGO'           , 'OHC_CHISTP'  , &('{|| StrZero(xValSisJur,' + cValToChar(TamSX3("OHC_CHISTP")[1])+')}')                    , 'Str'      , Nil })
	aAdd( aDePara, { 'FLAGCP'               , 'OHC_CONPAG'  , {{"S", "1"}, {"N", "2"}, {"", "2"}}                                  , ''         , Nil })
	aAdd( aDePara, { 'FLAGCR'               , 'OHC_CONREC'  , {{"S", "1"}, {"N", "2"}, {"", "2"}}                                  , ''         , Nil })
	aAdd( aDePara, { 'FLAGLANCAMENTO'       , 'OHC_LANCAM'  , {{"S", "1"}, {"N", "2"}, {"", "2"}}                                  , ''         , Nil })

ElseIf cTab == 'NVP' // Hist. Tab Hon Padr�o

	//               Sisjuri                , Protheus      , Force                                                                , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'TABELA_PADRAO'        , 'NVP_CTABH'   , ''                                                                   , ''         , Nil })
	aAdd( aDePara, { 'ANO_MES'              , 'NVP_AMINI'   , {|| StrTran(xValSisJur, "-", "")}                                    , ''         , Nil })
	aAdd( aDePara, { 'ANO_MES'              , 'NVP_AMFIM'   , {|| StrTran(xValSisJur, "-", "")}                                    , ''         , Nil })

ElseIf cTab == 'ACY' // Grupo de Clientes

	//               Sisjuri                , Protheus     , Force                                                              , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'GRENCODIGO'           , 'ACY_GRPVEN' , {|| StrZero(xValSisJur, TamSX3("ACY_GRPVEN")[1])}                  , ''         , Nil })
	aAdd( aDePara, { 'GRECDESCRICAO'        , 'ACY_DESCRI' , ''                                                                 , ''         , Nil })

ElseIf cTab == 'NRF' // Tabela de Honor�rios

	//               Sisjuri      , Protheus    , Force                                                                                                    ,Ajusta Tipo ,Executa fun��o
	aAdd( aDePara, { 'TABELA'     , 'NRF_COD'   , ''                                                                                                       , ''         , Nil })
	aAdd( aDePara, { 'MOEDA'      , 'NRF_MOEDA' , bGetMoeda                                                                                                , ''         , Nil })
	aAdd( aDePara, { 'OBSERVACAO' , 'NRF_DESC'  , {|| Iif(!Empty(xValSisJur), Substr(xValSisJur, 1, TamSX3("NRF_DESC")[1]), "Tab. Honor " + aRegs[nI][1])} , ''         , Nil })
	aAdd( aDePara, { 'STATUS'     , 'NRF_ATIVO' , {{"A", "1"}, {"I", "2"}, {"", "1"}}                                                                      , ''         , Nil })

ElseIf cTab == 'NS9' // Honor�rios - Categoria

	//               Sisjuri      , Protheus     , Force , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'TABELA'     , 'NS9_CTAB'   , ''    , ''         , Nil })
	aAdd( aDePara, { 'CATEG_ADVG' , 'NS9_CCAT'   , ''    , ''         , Nil })
	aAdd( aDePara, { 'VALOR_HORA' , 'NS9_VALORH' , ''    , ''         , Nil })

ElseIf cTab == 'NSD' // Honor�rios - Profissional

	//               Sisjuri                            , Protheus     , Force                                          , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'SSJR.CAD_TABH_PROF.DTHCTABELA'    , 'NSD_CTAB'   , ''                                             , ''         , Nil })
	aAdd( aDePara, { 'SSJR.CAD_TABH_PROF.DTHCCODADVG'   , 'NSD_CPART'  , bGetPart                                       , ''         , Nil })
	aAdd( aDePara, { 'SSJR.CAD_TABH_PROF.DTHCCATEGORIA' , 'NSD_CCAT'   , ''                                             , ''         , Nil })
	aAdd( aDePara, { 'SSJR.CAD_TABH_PROF.DTHNVALOR'     , 'NSD_VALORH' , ''                                             , ''         , Nil })

ElseIf cTab == 'NTV' // Hist. Tabela de Honor�rios

	//               Sisjuri      , Protheus      , Force                                          , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'TABELA'     , 'NTV_CTAB'    , ''                                             , ''         , Nil })
	aAdd( aDePara, { 'TABELA'     , ''            , ''                                             , ''         , Nil })
	aAdd( aDePara, { ''           , 'NTV_COD'     , {|| GetSxeNum("NTV", "NTV_COD")}               , ''         , Nil })
	aAdd( aDePara, { 'MOEDA'      , 'NTV_CMOEDA'  , bGetMoeda                                      , ''         , Nil })
	aAdd( aDePara, { 'OBSERVACAO' , 'NTV_DESC'    , ''                                             , ''         , Nil })
	aAdd( aDePara, { 'ANO_MES'    , 'NTV_AMINI'   , {|| StrTran(xValSisJur, "-", "")}              , ''         , Nil })
	aAdd( aDePara, { 'ANO_MES'    , 'NTV_AMFIM'   , {|| StrTran(xValSisJur, "-", "")}              , ''         , Nil })

ElseIf cTab == 'NTU' // Hist. Honor�rios - Categoria

	//               Sisjuri                          , Protheus     , Force                                                                         , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'TABELA'                         , 'NTU_CTAB'   , ''                                                                            , ''         , Nil })
	aAdd( aDePara, { ''                               , 'NTU_COD'    , {|| GetSxeNum("NTU", "NTU_COD")}                                              , ''         , Nil })
	aAdd( aDePara, { 'CATEG_ADVG'                     , 'NTU_CCAT'   , ''                                                                            , ''         , Nil })
	aAdd( aDePara, { 'VALOR_HORA'                     , 'NTU_VALORH' , ''                                                                            , ''         , Nil })
	aAdd( aDePara, { 'ANO_MES'                        , 'NTU_CHIST'  , {|| BuscaCodAM("NTV", "NTV_COD", aRegs[nI][1], StrTran(xValSisJur, "-", ""))} , ''         , Nil })

ElseIf cTab == 'NTT' // Hist. Honor�rios - Profissional

	//               Sisjuri                               , Protheus     , Force                                                                         , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'SSJR.CAD_DETHONOR_HIST.DTHCTABELA'   , 'NTT_CTAB'   , ''                                                                            , ''         , Nil })
	aAdd( aDePara, { ''                                    , 'NTT_COD'    , {|| GetSxeNum("NTT", "NTT_COD")}                                              , ''         , Nil })
	aAdd( aDePara, { 'SSJR.CAD_DETHONOR_HIST.DTHCCODADVG'  , 'NTT_CPART'  , bGetPart                                                                      , ''         , Nil })
	aAdd( aDePara, { 'SSJR.CAD_DETHONOR_HIST.DTHCCATEGORIA', 'NTT_CCAT'   , ''                                                                            , ''         , Nil })
	aAdd( aDePara, { 'SSJR.CAD_DETHONOR_HIST.DTHNVALOR'    , 'NTT_VALORH' , ''                                                                            , ''         , Nil })
	aAdd( aDePara, { 'SSJR.CAD_DETHONOR_HIST.ANO_MES'      , 'NTT_CHIST'  , {|| BuscaCodAM("NTV", "NTV_COD", aRegs[nI][1], StrTran(xValSisJur, "-", ""))} , ''         , Nil })

ElseIf cTab == 'NTN' // Moeda Bloqueadas para fatura

	//               Sisjuri         , Protheus    , Force                                                                         , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'MESNCODIGO'    , 'NTN_COD'   , {|| StrZero(xValSisJur, TamSX3("NTN_COD")[1])}                                , ''         , Nil })
	aAdd( aDePara, { 'MESCESCRITORIO', 'NTN_CESCR' ,                                                                               , ''         , Nil })
	aAdd( aDePara, { 'MESCMOEDA'     , 'NTN_CMOEDA', bGetMoeda                                                                     , ''         , Nil })

ElseIf cTab == 'CTP' // Cota��es

	//               Sisjuri  , Protheus    , Force                                                                         , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'CODIGO2', 'CTP_MOEDA' , bGetMoeda                                                                     , ''         , Nil })
	aAdd( aDePara, { 'DATA'   , 'CTP_DATA'  , ''                                                                            , ''         , Nil })
	aAdd( aDePara, { 'VALOR'  , 'CTP_TAXA'  , ''                                                                            , ''         , Nil })
	aAdd( aDePara, { ''       , 'CTP_BLOQ'  , '2'                                                                           , ''         , Nil })
	aAdd( aDePara, { 'VALOR'   , ''         , ''                                                                            , ''         , {|| GeraSM2(aRegs[nI][2], aRegs[nI][1], xValSisJur)} })

ElseIf cTab == 'NXQ' // Cota��o Mensal

	//               Sisjuri  , Protheus     , Force                                                                         , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'CODIGO2', 'NXQ_CMOEDA' , bGetMoeda                                                                     , ''         , Nil })
	aAdd( aDePara, { 'DATA'   , 'NXQ_ANOMES' , {|| AnoMes(xValSisJur)}                                                       , ''         , Nil })
	aAdd( aDePara, { 'VALOR'  , 'NXQ_COTAC'  , ''                                                                            , ''         , Nil })

ElseIf cTab == 'NSE' // Exc. Numera��o Fat

	//               Sisjuri          , Protheus    , Force                                                                         , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { ''               , 'NSE_COD'   , {|| GetSxeNum("NSE", "NSE_COD")}                                              , ''         , Nil })
	aAdd( aDePara, { 'NUMERO_INICIAL' , 'NSE_NUMINI', {|| StrZero(xValSisJur, TamSX3("NSE_NUMINI")[1])}                             , ''         , Nil })
	aAdd( aDePara, { 'NUMERO_FINAL'   , 'NSE_NUMFIN', {|| StrZero(xValSisJur, TamSX3("NSE_NUMFIN")[1])}                             , ''         , Nil })
	aAdd( aDePara, { 'ORGNCODIG'      , 'NSE_CESC'  , bGetEscrt                                                                     , ''         , Nil })

ElseIf cTab == 'NYT' // Equitrac - Configura��o Geral

	//               Sisjuri     , Protheus    , Force                                                                         , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { ''          , 'NYT_COD'   , {|| StrZero(1, TamSX3("NYT_COD")[1])}                                         , ''         , Nil })
	aAdd( aDePara, { 'CONCPADRA' , 'NYT_SISTEM', {{"System4", "1"}, {"DMate", "2"}, {"", "1"}}                                 , ''         , Nil })
	aAdd( aDePara, { ''          , 'NYT_ATIVO' , "1"                                                                           , ''         , Nil })

ElseIf cTab == 'NYV' // Tarifador - Config Despesas
	//               Sisjuri                  , Protheus     , Force                                      , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { ''                       , 'NYV_CODCFG' , {|| StrZero(1, TamSX3("NYT_COD")[1])}      , ''         , Nil })
	aAdd( aDePara, { CaseTarfi(.T.)           , 'NYV_TIPO'   , bTipoDesp                                  , ''         , Nil })
	aAdd( aDePara, { CaseTarfi(.F.)           , 'NYV_CNATUR' , bNatureza                                  , ''         , Nil })
	aAdd( aDePara, { ''                       , 'NYV_IDPART' , '1'                                        , ''         , Nil })
	aAdd( aDePara, { 'DESCDIRETAIMPO'         , 'NYV_DIAIMP' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESCARQUIDADOS'         , 'NYV_ARQIMP' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESCARQUIERRO'          , 'NYV_ARQERR' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESCDIRETIMPOR'         , 'NYV_DIRIMP' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESCFORMADATA'          , 'NYV_PICTDT' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNPORCTAXAADM'        , 'NYV_TAXA'   , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTECLIENINICI'    , 'NYV_INICLI' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTECLIENFINAL'    , 'NYV_FIMCLI' , {|| xValSisJur - TamSX3("A1_LOJA")[1]}     , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTECLIENFINAL'    , 'NYV_INILOJ' , {|| xValSisJur - TamSX3("A1_LOJA")[1] + 1} , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTECLIENFINAL'    , 'NYV_FIMLOJ' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTEPASTAINICI'    , 'NYV_INICAS' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTEPASTAFINAL'    , 'NYV_FIMCAS' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTEMSOLICINI'     , 'NYV_INISIG' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTEMSOLICFIM'     , 'NYV_FIMSIG' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTERAMALINICIAL'  , 'NYV_INIRAM' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTERAMALFINAL'    , 'NYV_FIMRAM' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTENUMTELINI'     , 'NYV_INITEL' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTENUMTELFIM'     , 'NYV_FIMTEL' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTEDATAINICI'     , 'NYV_INIDTA' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTEDATAFINAL'     , 'NYV_FIMDTA' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTEVALORINICI'    , 'NYV_INIVAL' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTEVALORFINAL'    , 'NYV_FIMVAL' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTEDESCRINICI'    , 'NYV_INIHOR' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTEDESCRFINAL'    , 'NYV_FIMHOR' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTEDESCRICAOINI'  , 'NYV_INIDES' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTEDESCRICAOFINAL', 'NYV_FIMDES' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTEORGNCODIGINI'  , 'NYV_INIESC' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTEORGNCODIGFIM'  , 'NYV_FIMESC' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTEQUANTINI'      , 'NYV_INIQTD' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTEQUANTFIM'      , 'NYV_FIMQTD' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTETIPODESPINI'   , 'NYV_INITPD' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTETIPODESPFIM'   , 'NYV_FIMTPD' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTEMDUTELINI'     , 'NYV_INIDUR' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESNSYSTEMDUTELFIM'     , 'NYV_FIMDUR' , ''                                         , ''         , Nil })
	aAdd( aDePara, { 'DESCSYSTEDESCR'         , ''           , ''                                         , ''         , {|| RepDescTar(aRegs[nI][1], aRegs[nI][2], xValSisJur)} })

ElseIf cTab == 'NYU' // Equitrac - Config. Arquivos

	//               Sisjuri           , Protheus    , Force                                                                   , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'ID_CONF_ARQUIVO' , 'NYU_COD'   , bNyuCod                                                                 , ''         , Nil })
	aAdd( aDePara, { ''                , 'NYU_CODCFG', {|| StrZero(1, TamSX3("NYT_COD")[1])}                                   , ''         , Nil })
	// TODO: N�o � poss�vel migrar o NYU_CAMPO, pois o "Campo" no protheus � um combo com 3 op��es (1=Caso;2=Cliente;3=Profissional),
	//       no Sisjuri � poss�vel informar qualquer campo
	// aAdd( aDePara, { 'CAMPO'           , 'NYU_CAMPO' , ''                                                                     , ''         , Nil })
	aAdd( aDePara, { 'DESCRICAO'       , 'NYU_DESCR' , ''                                                                      , ''         , Nil })
	aAdd( aDePara, { 'ARQUIVO'         , 'NYU_ARQUI' , ''                                                                      , ''         , Nil })
	aAdd( aDePara, { 'SITUACAO'        , 'NYU_ATIVO' , {{"A", "1"}, {"E", "2"}, {"", "2"}}                                     , ''         , Nil })
	aAdd( aDePara, { 'SQL'             , 'NYU_SQL'   , ''                                                                      , ''         , Nil })

ElseIf cTab == 'NYW' // Equitrac - Arquivos

	//               Sisjuri                    , Protheus    , Force                                          , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { ''                         , 'NYW_COD'   , {|| GetSxeNum("NYW", "NYW_COD")}               , ''         , Nil       })
	aAdd( aDePara, { cCpoNYW                    , ''          ,                                                , ''         , bMigraNYW })
	aAdd( aDePara, { cCpoKeyDt                  , 'NYW_DATA'  ,                                                , ''         , Nil       })
	aAdd( aDePara, { CaseTarfi(.T., "KEYCTIPO") , 'NYW_TIPO'  , bTipoDesp                                      , ''         , Nil       })
	aAdd( aDePara, { 'KEYCARQ'                  , 'NYW_ARQUI' , ''                                             , ''         , Nil       })

ElseIf cTab == 'NYX' // Equitrac - Arquivos Detalhes

	//               Sisjuri           , Protheus    , Force                            , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { cGetNYX           , 'NYX_CODARQ', bGetNYW                          , ''         , Nil })
	aAdd( aDePara, { ''                , 'NYX_COD'   , {|| GetSxeNum("NYX", "NYX_COD")} , ''         , Nil })
	aAdd( aDePara, { 'EXTDDATA'        , 'NYX_DATA'  , ''                               , ''         , Nil })
	aAdd( aDePara, { 'EXTNCLIEN'       , 'NYX_CCLIEN', bGetSA1Cli                       , ''         , Nil })
	aAdd( aDePara, { cCpoNYXCas        , 'NYX_CLOJA' , bGetLojCas                       , ''         , Nil }) 
	aAdd( aDePara, { 'EXTNCASO'        , 'NYX_CCASO' , bGetCaso                         , ''         , Nil })
	aAdd( aDePara, { 'EXTCMOEDA'       , 'NYX_MOEDA' , bGetMoeda                        , ''         , Nil })
	aAdd( aDePara, { 'EXTNVALOR'       , 'NYX_VALOR' , ''                               , ''         , Nil })
	aAdd( aDePara, { 'EXTNTIPODESP'    , 'NYX_TPDESP', bTipoDesp                        , ''         , Nil })
	aAdd( aDePara, { 'QUANTIDADE'      , 'NYX_QTDE'  , ''                               , ''         , Nil })
	aAdd( aDePara, { 'EXTCSOLICITANTE' , 'NYX_SIGLA' , ''                               , ''         , Nil })
	aAdd( aDePara, { 'EXTCDESCR'       , 'NYX_DESCR' , ''                               , ''         , Nil })
	//aAdd( aDePara, { 'CODIGODESP'      , 'NYX_DESP'  , ''                               , ''         , Nil }) // TODO: Falta importar a Despesa
	aAdd( aDePara, { 'EXTNRAMAL'       , 'NYX_RAMAL' , ''                               , ''         , Nil })
	aAdd( aDePara, { 'NUMEROTELEFONE'  , 'NYX_TELEFO', ''                               , ''         , Nil })
	aAdd( aDePara, { 'EXTNDUTELEFONEMA', 'NYX_DURTEL', ''                               , ''         , Nil })
	aAdd( aDePara, { 'ORGNCODIG'       , 'NYX_ESCR'  , bGetEscrt                        , ''         , Nil })
	aAdd( aDePara, { 'EXTNPORCTAXAADM' , 'NYX_TAXA'  , ''                               , ''         , Nil })
	aAdd( aDePara, { 'EXTCDESCRICAO'   , 'NYX_DESCR1', ''                               , ''         , Nil })
	aAdd( aDePara, { 'MOTIVO_NIMP'     , 'NYX_INCONS', ''                               , ''         , Nil })
	aAdd( aDePara, { ''                , 'NYX_STATUS', '5'                              , ''         , Nil })

ElseIf cTab == 'SA1|NUH' // Cliente / Complemente de Cliente / Contato

	//               Sisjuri                            , Protheus     , Force                                                                , Ajusta Tipo, Executa fun��o
  //aAdd( aDePara, { ''                                 , 'A1_FILIAL'  , ''                                                                   , ''         , Nil       } ) // Cliente deve ser compartilhado no Protheus	
	aAdd( aDePara, { 'RCR.ENDERECO.COD_CLIENTE'         , 'A1_COD'     , bGetSA1Cli                                                           , ''         , {|| Iif(xValSisJur != aCliente[1], aCliente[2] := "00",), aCliente[1] := xValSisJur}})
    aAdd( aDePara, { 'RCR.ENDERECO.COD_ENDERECO'        , 'A1_LOJA'    , {|| BuscaLoja("SA1")}                                                , ''         , Nil       })
	aAdd( aDePara, { cCpoCliEnd                         , ''           ,                                                                      , ''         , bMigrClien})
	aAdd( aDePara, { 'RCR.ENDERECO.RAZAO_SOCIAL'        , 'A1_NOME'    , {|| Upper(Substr(xValSisJur, 1, TamSX3("A1_NOME")[1]))}              , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.NOME_FANTASIA'       , 'A1_NREDUZ'  , {|| Upper(Substr(xValSisJur, 1, TamSX3("A1_NREDUZ")[1]))}            , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.TIPO'                , 'A1_PESSOA'  , {|| Iif(xValSisJur == "E" , "", xValSisJur)}                         , ''         , Nil       })
	aAdd( aDePara, { ''                                 , 'A1_TIPO'    , {|| Iif(aRegs[nI][5] == "E" , "X", "F")}                             , ''         , Nil       }) //REVISAR SE PODE DEIXAR COMO F QUANDO N�O FOR ESTRANGEIRO
	aAdd( aDePara, { 'RCR.ENDERECO.CGC_CPF'             , 'A1_CGC'     , bConvCGC                                                             , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.INSC_ESTADUAL'       , 'A1_INSCR'   , {|| Substr(xValSisJur, 1, TamSX3("A1_INSCR")[1])}                    , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.EMAIL'               , 'A1_EMAIL'   , {|| Substr(xValSisJur, 1, TamSX3("A1_EMAIL")[1])}                    , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.LOGRADOURO'          , ''                                                                                  , '', ''     , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.NUMERO'              , 'A1_END'     , {|| Upper(Substr(Alltrim(aRegs[nI][11]), 1, TamSX3("A1_END")[1] - 11 ) + ", "  + Substr(Alltrim(xValSisJur), 1, 8))}, ''         , Nil       } )
	aAdd( aDePara, { 'RCR.ENDERECO.CEP'                 , 'A1_CEP'     , {|| Substr(xValSisJur, 1, TamSX3("A1_CEP")[1])}                      , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.BAIRRO'              , 'A1_BAIRRO'  , {|| Upper(Substr(xValSisJur, 1, TamSX3("A1_BAIRRO")[1]))}            , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.ENDCDESCONTAISS'     , 'A1_RECISS'  , {{"S", "1"}, {"N", "2"}}                                             , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.DESCONTARPIS'        , 'A1_RECPIS'  , ''                                                                   , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.DESCONTARCOFINS'     , 'A1_RECCOFI' , ''                                                                   , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.DESCONTARCSLL'       , 'A1_RECCSLL' , ''                                                                   , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ESTADO.ESTCSIGLA'             , 'A1_EST'     , ''                                                                   , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.PAINCODIGO'          , 'A1_PAIS'    , {|| GetDePara("RCR.PAIS", xValSisJur, "")}                           , ''         , Nil       })
	aAdd( aDePara, { 'CIDCDESCRICAO'                    , 'A1_MUN'     ,                                                                      , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.CIDNCODIGO'          , 'A1_COD_MUN' , {|| GetDePara("RCR.CIDADE", xValSisJur, "")}                         , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.ENDCRETERIMPFATURAS' , 'A1_ABATIMP' , {{"S", "1"}, {"N", "2"}, {"", "1"}}                                  , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.DESCONTAIRRF'        , 'A1_RECIRRF' , {{"S", "1"}, {"N", "2"}, {"", "1"}}                                  , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.INSC_MUNICIPAL'      , 'A1_INSCRM'  , {|| Substr(xValSisJur, 1, TamSX3("A1_INSCRM")[1])}                   , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.DESCONTA_INSS'       , 'A1_RECINSS' , ''                                                                   , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.COMPLEMENTO'         , 'A1_COMPLEM' , {|| Substr(xValSisJur, 1, TamSX3("A1_COMPLEM")[1])}                  , ''         , Nil       })
	aAdd( aDePara, { CaseCliAtv()                       , 'A1_MSBLQL'  , {{"1", "2"}, {"2", "1"}}                                             , ''         , Nil       })
//	aAdd( aDePara, { 'RCR.ENDERECO.PCTCNUMEROCONTA'     , 'A1_CONTA'   , ''                                                                   , ''         , Nil       }) //TODO: Ainda n�o ser� migrado pois n�o foi feito o De-Para de contas cont�beis
	aAdd( aDePara, { 'RCR.CLIENTE.CLIDDATAABERTURA'     , 'A1_DTCAD'   , ''                                                                   , ''         , Nil       })
	aAdd( aDePara, { cCpoHora                           , 'A1_HRCAD'   , ''                                                                   , ''         , Nil       })
  //aAdd( aDePara, { 'RCR.CLIENTE.GRENCODIGO'           , 'A1_GRPTRIB' , {|| StrZero(xValSisJur, TamSX3("A1_GRPTRIB")[1])}                    , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.GRENCODIGO'           , 'A1_GRPVEN'  , bAdtGrpCli                                                           , ''         , Nil       })	
	aAdd( aDePara, { 'RCR.ENDERECO.COD_CLIENTE'         , 'A1_COND'    , bGetForPag                                                           , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.COD_CLIENTE'          , 'NUH_COD'    , bGetSA1Cli                                                           , ''         , Nil       })
	aAdd( aDePara, { cCpoCliEnd                         , 'NUH_LOJA'   , bGetSA1Loj                                                           , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.TABELA_PADRAO'        , 'NUH_CTABH'  , ''                                                                   , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.SOCIO_RESPONSAVEL'    , 'NUH_CPART'  , {|| GetDePara("RCR.ADVOGADO", xValSisJur, "")}                       , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.COD_REL'              , 'NUH_CRELAT' , {|| StrZero(xValSisJur, TamSX3("NUH_CRELAT")[1])}                    , ''         , Nil       })
	aAdd( aDePara, { 'CLICSITUACAOCLIENTE'              , 'NUH_SITCLI' ,  {{'P', '1'},{'E', '2'}}                                             , ''         , Nil       })
	aAdd( aDePara, { CaseCliSit()                       , 'NUH_SITCAD' ,                                                                      , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.CLICIDIOMRELAT'       , 'NUH_CIDIO'  , ''                                                                   , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.ESCR_ORIGINADOR'      , 'NUH_CESCR'  , {|| GetDePara("RCR.ESCRITORIO", xValSisJur, "")}                     , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.ORGNCODIG'            , 'NUH_CESCR2' , {|| GetDePara("RCR.ESCRITORIO", xValSisJur, "")}                     , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.CTANCODIG'            , 'NUH_CBANCO' , bGetCdBank                                                           , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.CTANCODIG'            , 'NUH_CAGENC' , bGetCdAgen                                                           , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.CTANCODIG'            , 'NUH_CCONTA' , bGetCdCont                                                           , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.FORMA_PGTO'           , 'NUH_FPAGTO' , ''                                                                   , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.CLIDEFETI'            , 'NUH_DTEFT'  , {|| Substr(Dtos(xValSisJur), 1, TamSX3("NUH_DTEFT")[1])}             , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.CLICNFDESPESA'        , 'NUH_EMITNF' , {{"S", "1"}, {"N", "2"}}                                             , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.COD_RELCARTACOB'      , 'NUH_CCARTA' , {|| StrZero(xValSisJur, TamSX3("NUH_CCARTA")[1])}                    , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.CLIDDATAENCERRAMENTO' , 'NUH_DTENC'  , {|| Substr(Dtos(xValSisJur), 1, TamSX3("NUH_DTENC")[1])}             , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.OBSERVACAO'           , 'NUH_OBSFAT' , ''                                                                   , ''         , Nil       })
	aAdd( aDePara, { CpoClob('OBSCAD')                  , 'NUH_OBSCAD' , ''                                                                   , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.GROSSUP_DESP_TRIB'    , 'NUH_GROSUP' , ''                                                                   , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.TXADM_DESP_TRIB'      , 'NUH_TXADM'  , ''                                                                   , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.ENDERECO'            , 'NUH_ENDI'   , ''                                                                   , ''         , Nil       })
	aAdd( aDePara, { CaseCliAtv()                       , 'NUH_ATIVO'  , ''                                                                   , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.ENVIAEMAIL'          , 'NUH_EMFAT'  , {{"S", "1"}, {"N", "2"}}                                             , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.EMAIL_CC'            , 'NUH_CEMAIL' , {|| Substr(xValSisJur, 1, TamSX3("NUH_CEMAIL")[1])}                  , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.NIF_CLIENTE'         , 'NUH_NIF'    , {|| Substr(xValSisJur, 1, TamSX3("NUH_NIF")[1])}                     , ''         , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.ID_MOTIVO_NIF'       , 'NUH_MOTNIF' , ''                                                                   , 'Str'      , Nil       })
	aAdd( aDePara, { 'RCR.ENDERECO.ID_TIPOVINC'         , 'NUH_TPNIF'  , ''                                                                   , 'Str'      , Nil       })
	aAdd( aDePara, { 'PADRAODESP_AGRUPA_DISCR'          , 'NUH_DSPDIS' , {{"D", "1"}, {"A", "2"}}                                             , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.CODIGO'               , 'NUH_CMOE'   , bGetMoeda                                                            , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.UTILEBILLING'         , 'NUH_UTEBIL' , {{"S", "1"}, {"N", "2"}, {"", "2"}}                                  , ''         , Nil       })
	aAdd( aDePara, { 'RCR.CLIENTE.ID_EMPRESA'           , 'NUH_CEMP'   , ''                                                                   , 'Str'      , Nil       })
	aAdd( aDePara, { 'CARTACOBRANCAUNIFICADA'           , 'NUH_UNIREL' , {{"S", "2"}, {"N", "1"}, {"", "1"}}                                  , 'Str'      , Nil       })
	aAdd( aDePara, { ''                                 , 'NUH_PERFIL' , '1'                                                                  , ''         , Nil       })
	aAdd( aDePara, { ''                                 , 'NUH_AJNV'   , '1'                                                                  , ''         , Nil       })
	aAdd( aDePara, { cCpoCliEnd                         , ''           , ''                                                                   , ''         , bGrvConCli})

ElseIf cTab == 'NU9' // Participa��o de cliente
	
	//               Sisjuri                 , Protheus    , Force                                                                    , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { ''                      , 'NU9_COD'   , {|| GetSXENum("NU9","NU9_COD")}                                          , ''         , Nil })
	aAdd( aDePara, { 'CODTIPOORIG'           , 'NU9_CTIPO' , bTpOrigina                                                               , ''         , Nil })
	aAdd( aDePara, { 'ENDERECO.COD_CLIENTE'  , 'NU9_CCLIEN', bGetSA1Cli                                                               , ''         , Nil })
	aAdd( aDePara, { cCpoCliEnd              , 'NU9_CLOJA' , bGetSA1Loj                                                               , ''         , Nil })
	aAdd( aDePara, { 'COD_ADVG'              , 'NU9_CPART' , {|| GetDePara("RCR.ADVOGADO", xValSisJur, "")}                           , ''         , Nil })
	aAdd( aDePara, { 'DATA_INICIAL'          , 'NU9_DTINI' , {|| Substr(Dtos(xValSisJur), 1, TamSX3("NU9_DTINI")[1])}                 , ''         , Nil })
	aAdd( aDePara, { 'DATA_FINAL'            , 'NU9_DTFIM' , {|| Substr(Dtos(xValSisJur), 1, TamSX3("NU9_DTFIM")[1])}                 , ''         , Nil })
	aAdd( aDePara, { cCpoCliEnd              , ''          ,                                                                          , ''         , bMigraNU9})
	aAdd( aDePara, { 'PARTICIPACAO'          , 'NU9_PERC'  , ''                                                                       , ''         , Nil })

ElseIf cTab == 'NUD' // Hist�rico de Participa��o de Cliente

	//               Sisjuri                              , Protheus    , Force                                                                    , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { cCpoCliEnd                           , 'NUD_COD'   , bGetCodNU9                                                               , ''         , Nil })
	aAdd( aDePara, { 'SSJR.CAD_PARTCLI_HIST.CODTIPOORIG'  , 'NUD_CTPORI', bTpOrigina                                                               , ''         , Nil })
	aAdd( aDePara, { 'ENDERECO.COD_CLIENTE'               , 'NUD_CCLIEN', bGetSA1Cli                                                               , ''         , Nil })
	aAdd( aDePara, { cCpoCliEnd                           , 'NUD_CLOJA' , bGetSA1Loj                                                               , ''         , Nil })
	aAdd( aDePara, { 'SSJR.CAD_PARTCLI_HIST.PARTICIPACAO' , 'NUD_PERC'  , ''                                                                       , ''         , Nil })
	aAdd( aDePara, { 'SSJR.CAD_PARTCLI_HIST.COD_ADVG'     , 'NUD_CPART' , {|| GetDePara("RCR.ADVOGADO", xValSisJur, "")}                           , ''         , Nil })
	aAdd( aDePara, { ''                                   , 'NUD_CPARTI', {|| GetSXENum("NUD","NUD_CPARTI")}                                       , ''         , Nil })
	aAdd( aDePara, { 'SSJR.CAD_PARTCLI_HIST.DATA_INICIAL' , 'NUD_DTINI' , {|| Substr(Dtos(xValSisJur), 1, TamSX3("NU9_DTINI")[1])}                 , ''         , Nil })
	aAdd( aDePara, { 'SSJR.CAD_PARTCLI_HIST.DATA_FINAL'   , 'NUD_DTFIM' , {|| Substr(Dtos(xValSisJur), 1, TamSX3("NU9_DTFIM")[1])}                 , ''         , Nil })
	aAdd( aDePara, { 'SSJR.CAD_PARTCLI_HIST.ANO_MES'      , 'NUD_AMINI' , {|| StrTran(xValSisJur, "-", "")}                                        , ''         , Nil })
	aAdd( aDePara, { 'SSJR.CAD_PARTCLI_HIST.ANO_MES'      , 'NUD_AMIFIM', {|| StrTran(xValSisJur, "-", "")}                                        , ''         , Nil })

ElseIf cTab == "NUA"

	//               Sisjuri                         , Protheus    , Force                                                                            , Ajusta Tipo, Executa fun��o
	If NUA->(ColumnPos('NUA_COD'))
		aAdd( aDePara, { ''                          , 'NUA_COD'   , {|| CriaVar("NUA_COD", .T.)}                                                     , ''         , Nil })
	EndIf
	aAdd( aDePara, { 'CODIGOREL'                     , 'NUA_CTPREL', bTpRptFat                                                                        , ''         , Nil })
	aAdd( aDePara, { 'RCR.TIPORELATCLI.COD_CLIENTE'  , 'NUA_CCLIEN', bGetSA1Cli                                                                       , ''         , Nil })
	aAdd( aDePara, { cCpoCliEnd                      , 'NUA_CLOJA' , bGetSA1Loj                                                                       , ''         , Nil })

ElseIf cTab == "NUB"

	//               Sisjuri                      , Protheus    , Force                                                                            , Ajusta Tipo, Executa fun��o
	If NUB->(ColumnPos('NUB_COD'))
		aAdd( aDePara, { ''                           , 'NUB_COD'   , {|| CriaVar("NUB_COD", .T.)}                                                     , ''         , Nil })
	EndIf
	aAdd( aDePara, { 'SSJR.CAD_CLI_TIPOATIV.TIPO' , 'NUB_CTPATI', bTpAtivid                                                                        , ''         , Nil })
	aAdd( aDePara, { 'ENDERECO.COD_CLIENTE'       , 'NUB_CCLIEN', bGetSA1Cli                                                                       , ''         , Nil })
	aAdd( aDePara, { cCpoCliEnd                   , 'NUB_CLOJA' , bGetSA1Loj                                                                       , ''         , Nil })

ElseIf cTab == "NUC"
	
	//               Sisjuri                                   , Protheus    , Force                                                                            , Ajusta Tipo, Executa fun��o
	If NUC->(ColumnPos('NUC_COD'))
		aAdd( aDePara, { ''                                        , 'NUC_COD'   , {|| CriaVar("NUC_COD", .T.)}                                                     , ''         , Nil })
	EndIf
	aAdd( aDePara, { 'SSJR.CAD_CLI_TIPODESP.TIPO'              , 'NUC_CTPDES', bTipoDesp                                                                        , ''         , Nil })
	aAdd( aDePara, { 'ENDERECO.COD_CLIENTE'                    , 'NUC_CCLIEN', bGetSA1Cli                                                                       , ''         , Nil })
	aAdd( aDePara, { cCpoCliEnd                                , 'NUC_CLOJA' , bGetSA1Loj                                                                       , ''         , Nil })

ElseIf cTab == 'NVE' // Casos
	
	//               Sisjuri                     , Protheus    , Force                                              , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'COD_CLIENTE'               , 'NVE_CCLIEN', bGetSA1Cli                                         , ''         , Nil })
	aAdd( aDePara, { cCpoCasClE                  , 'NVE_LCLIEN', bGetSA1Loj                                         , ''         , Nil })
	aAdd( aDePara, { 'PASTA'                     , 'NVE_NUMCAS', bGetCaso                                           , ''         , Nil })
	aAdd( aDePara, { cCpoCliECas                 , ''          ,                                                    , ''         , bMigraCaso })
	aAdd( aDePara, { 'TITULO'                    , 'NVE_TITULO', ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'ORGNCODIG'                 , 'NVE_CESCRI', bGetEscrt                                          , ''         , Nil })
	aAdd( aDePara, { 'ID_AREA'                   , 'NVE_CAREAJ', ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'ID_SUBAREA'                , 'NVE_CSUBAR', bGetSubArea                                        , ''         , Nil })
	aAdd( aDePara, { 'REVISOR'                   , 'NVE_CPART1', bGetPart                                           , ''         , Nil })
	aAdd( aDePara, { 'NOME'                      , 'NVE_CPART2', bGetPart                                           , ''         , Nil })
	aAdd( aDePara, { 'SOCIO_RESPONSAVEL'         , 'NVE_CPART5', bGetPart                                           , ''         , Nil })
	aAdd( aDePara, { 'SITUACAO'                  , 'NVE_SITUAC', {{"A", "1"}, {"E", "2"}, {"AT", "1"}, {"AD", "1"}} , ''         , Nil })
	aAdd( aDePara, { 'DATA_ABERTURA'             , 'NVE_DTENTR', ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'DATA_ENCERRAMENTO'         , 'NVE_DTENCE', ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'MOTIVO_ENCERRAMENTO'       , 'NVE_DETENC', {|| Substr(xValSisJur, 1, TamSX3("NVE_DETENC")[1])}, ''         , Nil })
	aAdd( aDePara, { 'ADVG_ENC_CASO'             , 'NVE_CPART3', bGetPart                                           , ''         , Nil })
	aAdd( aDePara, { 'DATA_REABERTURA'           , 'NVE_DTREAB', ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'MOTIVO_REABERTURA'         , 'NVE_DETREA', {|| Substr(xValSisJur, 1, TamSX3("NVE_DETREA")[1])}, ''         , Nil })
	aAdd( aDePara, { 'ADVG_REABERTURA'           , 'NVE_CPART4', bGetPart                                           , ''         , Nil })
	aAdd( aDePara, { 'TPVALORHORA'               , 'NVE_TPHORA', {{"F", "1"}, {"L", "2"}}                           , ''         , Nil })
	aAdd( aDePara, { 'VALOR_HORA'                , 'NVE_VLHORA', ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'TABELA'                    , 'NVE_CTABH' , ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'TABCCODIGO'                , 'NVE_CTABS' , ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'IMP_DESP_AGRUPA_DISCRIMINA', 'NVE_DSPDIS', {{"D", "1"}, {"A", "2"}}                           , ''         , Nil })
	aAdd( aDePara, { 'DESCPADRAO'                , 'NVE_DESPAD', ''                                                 , ''         , Nil })
	aAdd( aDePara, { ''                          , 'NVE_TPERCD', {|| Iif(!Empty(aRegs[nI][23]), '1', '')}           , ''         , Nil })
	aAdd( aDePara, { 'IDIOMA'                    , 'NVE_CIDIO' , ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'ENC_HONOR'                 , 'NVE_ENCHON', {{"S", "1"}, {"N", "2"}, {"", "1"}}                , ''         , Nil })
	aAdd( aDePara, { 'ENC_DESP'                  , 'NVE_ENCDES', {{"S", "1"}, {"N", "2"}, {"", "1"}}                , ''         , Nil })
	aAdd( aDePara, { ''                          , 'NVE_ENCTAB', '2'                                                , ''         , Nil })
	aAdd( aDePara, { 'EXITO'                     , 'NVE_EXITO' , {{"S", "1"}, {"N", "2"}, {"", "1"}}                , ''         , Nil })
	aAdd( aDePara, { 'VIRTUAL'                   , 'NVE_VIRTUA', {{"S", "1"}, {"N", "2"}, {"", "1"}}                , ''         , Nil })
	aAdd( aDePara, { ''                          , 'NVE_LANTS' , '1'                                                , ''         , Nil })
	aAdd( aDePara, { ''                          , 'NVE_LANDSP', '1'                                                , ''         , Nil })
	aAdd( aDePara, { ''                          , 'NVE_LANTAB', '1'                                                , ''         , Nil })
	aAdd( aDePara, { 'LITISCONSORCIO'            , 'NVE_LITIS' , {{"S", "1"}, {"N", "2"}, {"", "1"}}                , ''         , Nil })
	aAdd( aDePara, { 'OBSCAD'                    , 'NVE_OBSCAD', ''                                                 , ''         , Nil })
	aAdd( aDePara, { CpoClob('OBSERVACAO')       , 'NVE_OBSFAT', ''                                                 , ''         , Nil })
	aAdd( aDePara, { CpoClob('REDACAO')          , 'NVE_REDFAT', ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'TPCOBNCOBRAR'              , 'NVE_COBRAV', {{"N", "1"}, {"S", "2"}, {"", "1"}}                , ''         , Nil })
	aAdd( aDePara, { 'PESO_CONTRATO'             , 'NVE_PESO'  , ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'REVISADO'                  , 'NVE_REVISA', {{"S", "1"}, {"N", "2"}, {"", "1"}}                , ''         , Nil })
	aAdd( aDePara, { 'DATA_DEFINITIVO'           , 'NVE_DTREVI', ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'VALORLIMITECASO'           , 'NVE_VLRLI' , ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'SALDOINICIALCASO'          , 'NVE_SALDOI', ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'MOEDALIMCASO'              , 'NVE_CMOELI', bGetMoeda                                          , ''         , Nil })
	aAdd( aDePara, { ''                          , 'NVE_DBTPES', '2'                                                , ''         , Nil })
	aAdd( aDePara, { ''                          , 'NVE_CTBCVL', '2'                                                , ''         , Nil })
	aAdd( aDePara, { ''                          , 'NVE_CFACVL', '2'                                                , ''         , Nil })

ElseIf cTab == 'NUU' // Hist�rico do Caso

	//               Sisjuri                     , Protheus    , Force                                              , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'COD_CLIENTE'               , 'NUU_CCLIEN', bGetSA1Cli                                         , ''         , Nil })
	aAdd( aDePara, { cCpoCliCas                  , 'NUU_CLOJA' , bGetLojCas                                         , ''         , Nil })
	aAdd( aDePara, { 'PASTA'                     , 'NUU_CCASO' , bGetCaso                                           , ''         , Nil })
	aAdd( aDePara, { 'ANO_MES'                   , 'NUU_AMINI' , {|| StrTran(xValSisJur, "-", "")}                  , ''         , Nil })
	aAdd( aDePara, { 'ANO_MES'                   , 'NUU_AMFIM' , {|| StrTran(xValSisJur, "-", "")}                  , ''         , Nil })
	aAdd( aDePara, { 'TABELA'                    , 'NUU_CTABH' , ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'TABCCODIGO'                , 'NUU_CTABS' , ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'TPVALORHORA'               , 'NUU_TPHORA', {{"F", "1"}, {"L", "2"}}                           , ''         , Nil })
	aAdd( aDePara, { 'VALOR_HORA'                , 'NUU_VLHORA', ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'ESCRITORIO'                , 'NUU_CESCR' ,                                                    , ''         , Nil })
	aAdd( aDePara, { 'ID_AREA'                   , 'NUU_CAREAJ', ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'ID_SUBAREA'                , 'NUU_CSUBAR', bGetSubArea                                        , 'Str'      , Nil })

ElseIf cTab == 'NUK' // Participa��o do Caso

	//               Sisjuri                     , Protheus     , Force                                              , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { ''                          , 'NUK_COD'    , {|| GetSXENum("NUK", "NUK_COD")}                   , ''         , Nil })
	aAdd( aDePara, { 'COD_CLIENTE'               , 'NUK_CCLIEN' , bGetSA1Cli                                         , ''         , Nil })
	aAdd( aDePara, { cCpoCliCas                  , 'NUK_CLOJA'  , bGetLojCas                                         , ''         , Nil })
	aAdd( aDePara, { 'PASTA'                     , 'NUK_NUMCAS' , bGetCaso                                           , ''         , Nil })
	aAdd( aDePara, { 'COD_ADVG'                  , 'NUK_CPART'  , bGetPart                                           , ''         , Nil })
	aAdd( aDePara, { 'CODTIPOORIG'               , 'NUK_CTIPO'  , ''                                                 , 'Str'      , Nil })
	aAdd( aDePara, { 'PARTICIPACAO'              , 'NUK_PERC'   , ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'DATA_INICIAL'              , 'NUK_DTINI'  , ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'DATA_FINAL'                , 'NUK_DTFIN'  , ''                                                 , ''         , Nil })

ElseIf cTab == 'NVF' // Hist�rico de Participa��o do Caso

	//               Sisjuri                     , Protheus     , Force                                              , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { ''                          , 'NVF_COD'    , {|| GetSXENum("NUK", "NUK_COD")}                   , ''         , Nil })
	aAdd( aDePara, { 'ANO_MES'                   , 'NVF_AMINI'  , {|| StrTran(xValSisJur, "-", "")}                  , ''         , Nil })
	aAdd( aDePara, { 'ANO_MES'                   , 'NVF_AMFIM'  , {|| StrTran(xValSisJur, "-", "")}                  , ''         , Nil })
	aAdd( aDePara, { 'COD_CLIENTE'               , 'NVF_CCLIEN' , bGetSA1Cli                                         , ''         , Nil })
	aAdd( aDePara, { cCpoCliCas                  , 'NVF_CLOJA'  , bGetLojCas                                         , ''         , Nil })
	aAdd( aDePara, { 'PASTA'                     , 'NVF_NUMCAS' , bGetCaso                                           , ''         , Nil })
	aAdd( aDePara, { 'COD_ADVG'                  , 'NVF_CPART'  , bGetPart                                           , ''         , Nil })
	aAdd( aDePara, { 'CODTIPOORIG'               , 'NVF_CTIPO'  , ''                                                 , 'Str'      , Nil })
	aAdd( aDePara, { 'PARTICIPACAO'              , 'NVF_PERC'   , ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'DATA_INICIAL'              , 'NVF_DTINI'  , ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'DATA_FINAL'                , 'NVF_DTFIN'  , ''                                                 , ''         , Nil })

ElseIf cTab == 'NV1' // Exce��o da Tab Honor por Categoria no Caso

	//               Sisjuri                     , Protheus     , Force                                              , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { ''                          , 'NV1_COD'    , {|| GetSXENum("NV1", "NV1_COD")}                   , ''         , Nil })
	aAdd( aDePara, { 'COD_CLIENTE'               , 'NV1_CCLIEN' , bGetSA1Cli                                         , ''         , Nil })
	aAdd( aDePara, { cCpoCliCas                  , 'NV1_CLOJA'  , bGetLojCas                                         , ''         , Nil })
	aAdd( aDePara, { 'PASTA'                     , 'NV1_CCASO'  , bGetCaso                                           , ''         , Nil })
	aAdd( aDePara, { 'CATEGORIA'                 , 'NV1_CCAT'   , ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'REGRA'                     , 'NV1_REGRA'  , {{"P", "1"}, {"S", "2"}, {"F", "3"}}               , ''         , Nil })
	aAdd( aDePara, { 'VALOR'                     , 'NV1_VALOR2' , ''                                                 , ''         , Nil })

ElseIf cTab == 'NUW' // Hist�rico da Exce��o da Tab Honor por Categoria no Caso

	//               Sisjuri                     , Protheus     , Force                                              , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { ''                          , 'NUW_COD'    , {|| GetSXENum("NUW", "NUW_COD")}                   , ''         , Nil })
	aAdd( aDePara, { 'COD_CLIENTE'               , 'NUW_CCLIEN' , bGetSA1Cli                                         , ''         , Nil })
	aAdd( aDePara, { cCpoCliCas                  , 'NUW_CLOJA'  , bGetLojCas                                         , ''         , Nil })
	aAdd( aDePara, { 'PASTA'                     , 'NUW_CCASO'  , bGetCaso                                           , ''         , Nil })
	aAdd( aDePara, { 'ANO_MES'                   , 'NUW_AMINI'  , {|| StrTran(xValSisJur, "-", "")}                  , ''         , Nil })
	aAdd( aDePara, { 'ANO_MES'                   , 'NUW_AMFIM'  , {|| StrTran(xValSisJur, "-", "")}                  , ''         , Nil })
	aAdd( aDePara, { 'CATEGORIA'                 , 'NUW_CCAT'   , ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'REGRA'                     , 'NUW_REGRA'  , {{"P", "1"}, {"S", "2"}, {"F", "3"}}               , ''         , Nil })
	aAdd( aDePara, { 'VALOR'                     , 'NUW_VALOR2' , ''                                                 , ''         , Nil })

ElseIf cTab == 'NV2' // Exce��o da Tab Honor por Participante no Caso

	//               Sisjuri                     , Protheus     , Force                                              , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { ''                          , 'NV2_COD'    , {|| GetSXENum("NV1", "NV1_COD")}                   , ''         , Nil })
	aAdd( aDePara, { 'COD_CLIENTE'               , 'NV2_CCLIEN' , bGetSA1Cli                                         , ''         , Nil })
	aAdd( aDePara, { cCpoCliCas                  , 'NV2_CLOJA'  , bGetLojCas                                         , ''         , Nil })
	aAdd( aDePara, { 'PASTA'                     , 'NV2_CCASO'  , bGetCaso                                           , ''         , Nil })
	aAdd( aDePara, { 'COD_ADVG'                  , 'NV2_CPART'  , bGetPart                                           , ''         , Nil })
	aAdd( aDePara, { 'CATEGORIA'                 , 'NV2_CCAT'   , ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'REGRA'                     , 'NV2_REGRA'  , {{"P", "1"}, {"V", "2"}, {"F", "3"}, {"C", "4"}}   , ''         , Nil })
	aAdd( aDePara, { 'CUMULATIVO'                , 'NV2_EXCCAT' , {{"S", "1"}, {"N", "2"}}                           , ''         , Nil })
	aAdd( aDePara, { 'VALOR'                     , 'NV2_VALOR2' , ''                                                 , ''         , Nil })

ElseIf cTab == 'NV0' // Hist�rico da Exce��o da Tab Honor por Participante no Caso

	//               Sisjuri                     , Protheus     , Force                                              , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { ''                          , 'NV0_COD'    , {|| GetSXENum("NV1", "NV1_COD")}                   , ''         , Nil })
	aAdd( aDePara, { 'COD_CLIENTE'               , 'NV0_CCLIEN' , bGetSA1Cli                                         , ''         , Nil })
	aAdd( aDePara, { cCpoCliCas                  , 'NV0_CLOJA'  , bGetLojCas                                         , ''         , Nil })
	aAdd( aDePara, { 'PASTA'                     , 'NV0_CCASO'  , bGetCaso                                           , ''         , Nil })
	aAdd( aDePara, { 'ANO_MES'                   , 'NV0_AMINI'  , {|| StrTran(xValSisJur, "-", "")}                  , ''         , Nil })
	aAdd( aDePara, { 'ANO_MES'                   , 'NV0_AMFIM'  , {|| StrTran(xValSisJur, "-", "")}                  , ''         , Nil })
	aAdd( aDePara, { 'COD_ADVG'                  , 'NV0_CPART'  , bGetPart                                           , ''         , Nil })
	aAdd( aDePara, { 'CATEGORIA'                 , 'NV0_CCAT'   , ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'REGRA'                     , 'NV0_REGRA'  , {{"P", "1"}, {"V", "2"}, {"F", "3"}, {"C", "4"}}   , ''         , Nil })
	aAdd( aDePara, { 'CUMULATIVO'                , 'NV0_EXCCAT' , {{"S", "1"}, {"N", "2"}}                           , ''         , Nil })
	aAdd( aDePara, { 'VALOR'                     , 'NV0_VALOR2' , ''                                                 , ''         , Nil })

ElseIf cTab == 'NWL' // Condi��o de �xito no Caso
	//               Sisjuri                     , Protheus     , Force                                              , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { ''                          , 'NWL_COD'    , {|| GetSXENum("NV1", "NV1_COD")}                   , ''         , Nil })
	aAdd( aDePara, { 'DTCNCLIENTE'               , 'NWL_CCLIEN' , bGetSA1Cli                                         , ''         , Nil })
	aAdd( aDePara, { cCpoNWLCas                  , 'NWL_CLOJA'  , bGetLojCas                                         , ''         , Nil })
	aAdd( aDePara, { 'DTCNPASTA'                                 , 'NWL_NUMCAS' , bGetCaso                                           , ''         , Nil })
	aAdd( aDePara, { 'DTCNCONDICAO'              , 'NWL_CONDEX' , ''                                                 , 'Str'      , Nil })
	aAdd( aDePara, { CpoRound('DTCNPORCENTUAL'  , 'NWL_PERCEN')  , 'NWL_PERCEN' , ''                                                 , ''         , Nil })
	aAdd( aDePara, { CpoRound('DTCNVALORINICIAL', 'NWL_VALINI')  , 'NWL_VALINI' , ''                                                 , ''         , Nil })
	aAdd( aDePara, { CpoRound('DTCNVALORFINAL'  , 'NWL_VALFIN')  , 'NWL_VALFIN' , ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'DTCCANOINICIAL'            , 'NWL_ANOINI' , ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'DTCCANOFINAL'              , 'NWL_ANOFIN' , ''                                                 , ''         , Nil })
	aAdd( aDePara, { 'DTCDDATABASE'              , 'NWL_DTBASE' , ''                                                 , ''         , Nil })
	// aAdd( aDePara, { DTCCTAXA'                                , 'NWL_CINDIC' , ''                                                 , ''         , Nil }) //TODO - Aguardar migra��o de �ndices
	// aAdd( aDePara, { ''                       , 'NWL_TPCORR' , {|| Iif(!Empty(aRegs[nI][12]), '2', '1') }         , ''         , Nil }) //TODO - Aguardar migra��o de �ndices
	aAdd( aDePara, { CpoRound('DTCNVALORCORRIGIDO', 'NWL_VALCOR'), 'NWL_VALCOR' , ''                                                 , ''         , Nil })	
ElseIf cTab == 'NWF' //Controle de Adiantamentos
    aAdd( aDePara, { 'RCR.ADIANTAMENTO.ADINDOCUMENTO'  , 'NWF_COD'   , {|| CriaVar("NWF_COD", .T.)}                                         , ''         , Nil         })
	aAdd( aDePara, { cAdtDtFin                         , 'NWF_DATAIN', ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { ''                                , 'NWF_USRINC', 'Administrador'                                                      , ''         , Nil         })                                                                                                                       
	aAdd( aDePara, { 'RCR.ADIANTAMENTO.ADINPASTA'      , 'NWF_CCASO' , bAdtCaso                                                             , ''         , Nil         })
    aAdd( aDePara, { 'RCR.ADIANTAMENTO.ADINDOCUMENTO'  , ''          , ''                                                                   , ''         , bAdtMigraAdt})
	aAdd( aDePara, { cAdtTipo                          , 'NWF_TPADI' , ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { cAdtExclus                        , 'NWF_EXCLUS', ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { 'RCR.ADIANTAMENTO.ADINVALOR'      , 'NWF_VALOR' , ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { 'RCR.ADIANTAMENTO.ADINVALOR'      , 'NWF_SALDO' , ''                                                                   , ''         , Nil         })	
	aAdd( aDePara, { cAdtSituac                        , 'NWF_SITUAC', ''                                                                   , ''         , Nil         })	
	aAdd( aDePara, { cAdtDtVen                         , 'NWF_DTMOVI', ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { 'RCR.ADIANTAMENTO.ADICOBSERVACAO' , 'NWF_HIST'  , ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { cAdtDtVen                         , 'NWF_VENCTO', ''                                                                   , ''         , Nil         }) 
	aAdd( aDePara, { 'RCR.ESCRITORIO.SIGLA'            , 'NWF_CESCR' , ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { cAdtBanco                         , 'NWF_BANCO' , ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { cAdtConta                         , 'NWF_CONTA' , {|| Substr(StrTran(xValSisJur, "-", ""), 1, TamSX3("A6_NUMCON" )[1])}, ''         , Nil         })
	aAdd( aDePara, { cAdtAgenc                         , 'NWF_AGENCI', {|| Substr(StrTran(xValSisJur, "-", ""), 1, TamSX3("A6_AGENCIA")[1])}, ''         , Nil         })	
	aAdd( aDePara, { 'RCR.ENDERECO.COD_CLIENTE'        , 'NWF_CCLIEN', bGetSA1Cli                                                           , ''         , Nil         })
	aAdd( aDePara, { cAdtCliEnd                        , 'NWF_CLOJA' , bGetSA1Loj                                                           , ''         , Nil         })
	aAdd( aDePara, { 'RCR.ENDERECO.COD_CLIENTE'        , 'NWF_CCLIAD', bGetSA1Cli                                                           , ''         , Nil         })
	aAdd( aDePara, { cAdtCliEnd                        , 'NWF_CLOJAD', bGetSA1Loj                                                           , ''         , Nil         })
	aAdd( aDePara, { 'RCR.CLIENTE.GRENCODIGO'          , 'NWF_CGRPCL', bAdtGrpCli                                                           , ''         , Nil         })
	aAdd( aDePara, { ''                                , 'NWF_TITGER', cAdtTitGer                                                           , ''         , Nil         })
	aAdd( aDePara, { 'RCR.MOEDA.CODIGO'                , 'NWF_CMOE'  , bAdtGetMoe                                                           , ''         , Nil         })
	aAdd( aDePara, { 'RCR.ADIANTAMENTO.ADINDOCUTIL'    , ''          ,                                                                      , ''         , Nil         })
ElseIf cTab == 'NV4' // Lan�amento Tabelado
	aAdd( aDePara, { cLtbId                            , 'NV4_COD'   , ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { cLtbData                          , 'NV4_DTLANC', ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { cLtbData                          , 'NV4_DTINC' , ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { "'00:00:00'"                      , 'NV4_HRINC' , ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { cLtbGrpCli                        , 'NV4_CGRUPO', ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { 'RCR.LANCTABELADO.COD_CLIENTE'    , 'NV4_CCLIEN', bGetSA1Cli                                                           , ''         , Nil         })
	aAdd( aDePara, { cAdtCliEnd                        , 'NV4_CLOJA' , bGetLojCas                                                           , ''         , Nil         })
	aAdd( aDePara, { 'RCR.LANCTABELADO.PASTA_REAL'     , 'NV4_CCASO' , bGetCaso                                                             , ''         , Nil         })
	aAdd( aDePara, { 'RCR.LANCTABELADO.COD_ADVG'       , 'NV4_CPART' , bGetPart                                                             , ''         , Nil         })
	aAdd( aDePara, { 'RCR.LANCTABELADO.ID_LANCTAB'     , ''          , ''                                                                   , ''         , bLtbMigra   })
	aAdd( aDePara, { 'RCR.LANCTABELADO.ITBCCODIGO'     , 'NV4_CTPSRV', ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { 'RCR.LANCTABELADO.QUANT'          , 'NV4_QUANT' , ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { 'RCR.LANCTABELADO.MOEDAH'         , 'NV4_CMOEH' , bGetMoeda                                                            , ''         , Nil         })
	aAdd( aDePara, { 'RCR.LANCTABELADO.VALORH_REAL'    , 'NV4_VLHFAT', ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { 'RCR.LANCTABELADO.VALORH'         , 'NV4_VLHTAB', ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { 'RCR.LANCTABELADO.MOEDAD'         , 'NV4_CMOED' , bGetMoeda                                                            , ''         , Nil         })
	aAdd( aDePara, { 'RCR.LANCTABELADO.VALORD_REAL'    , 'NV4_VLDFAT', ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { 'RCR.LANCTABELADO.VALORD'         , 'NV4_VLDTAB', ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { 'RCR.LANCTABELADO.DESCRICAO'      , 'NV4_DESCRI', ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { 'RCR.LANCTABELADO.CONCLUIDO'      , 'NV4_CONC'  , ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { cLtbDtConcl                       , 'NV4_DTCONC', ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { 'RCR.LANCTABELADO.COBRAR'         , 'NV4_COBRAR', ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { 'RCR.LANCTABELADO.STATUS'         , 'NV4_SITUAC', {{"N", "1"}, {"F", "2"}, {"W", "2"}}                                 , ''         , Nil         })
	aAdd( aDePara, { 'RCR.LANCTABELADO.ANO_MES'        , 'NV4_ANOMES', {|| StrTran(xValSisJur, "-", "")}                                    , ''         , Nil         })
	aAdd( aDePara, { cLtbPreFat                        , 'NV4_CPREFT', ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { cLtbLote                          , 'NV4_CLOTE' , ''                                                                   , ''         , Nil         })
	aAdd( aDePara, { "'2'"                             , 'NV4_FINANC', ''                                                                   , ''         , Nil         })
ElseIf cTab == 'NW4' .Or. cTab == "NW4_PRE" // Lancamento Tabelado Fatu
	aAdd( aDePara, { "LPAD(RCR.LANCTABELADO.ID_LANCTAB,6,'0')"                                                     , 'NW4_CLTAB' , ''                                       , ''         , Nil         })
	aAdd( aDePara, { "SSJR.LANCTABELADO_FATURA.SITUACAO"                                                           , 'NW4_SITUAC', {{"N", "1"}, {"F", "2"}, {"W", "2"}}     , ''         , Nil         })
	aAdd( aDePara, { "NVL(LPAD(SSJR.LANCTABELADO_FATURA.FATURA,9,'0'), ' ')"                                       , 'NW4_CFATUR', ''                                       , ''         , Nil         })
	aAdd( aDePara, { 'SSJR.LANCTABELADO_FATURA.ORGNCODIG'                                                          , 'NW4_CESCR' , bGetEscrt                                , ''         , Nil         })
	aAdd( aDePara, { "NVL(LPAD(SSJR.LANCTABELADO_FATURA.WONCODIGO,8,'0'), ' ')"                                    , 'NW4_CWO'   , ''                                       , ''         , Nil         })
	aAdd( aDePara, { "SSJR.LANCTABELADO_FATURA.VALIDA"                                                             , 'NW4_CANC'  , {{"S", "2"}, {"N", "1"}}                 , ''         , Nil         })
	aAdd( aDePara, { 'RCR.LANCTABELADO.COD_CLIENTE'                                                                , 'NW4_CCLIEN', bGetSA1Cli                               , ''         , Nil         })
	aAdd( aDePara, { cAdtCliEnd                                                                                    , 'NW4_CLOJA' , bGetLojCas                               , ''         , Nil         })
	aAdd( aDePara, { 'RCR.LANCTABELADO.PASTA_REAL'                                                                 , 'NW4_CCASO' , bGetCaso                                 , ''         , Nil         })
	If cTab == 'NW4'
		aAdd( aDePara, { 'SSJR.LANCTABELADO_FATURA.MOEDA_H'                                                        , 'NW4_CMOEDH', bGetMoeda                                , ''         , Nil         })
		aAdd( aDePara, { 'SSJR.LANCTABELADO_FATURA.VALOR_H'                                                        , 'NW4_VALORH', ''                                       , ''         , Nil         })
	ElseIf cTab == "NW4_PRE"
		aAdd( aDePara, { "NVL(LPAD(RCR.LANCTABELADO.NUMEROPREFAT,8,'0'), ' ')"                                     , 'NW4_PRECNF', ''                                       , ''         , Nil         })
		aAdd( aDePara, { 'RCR.LANCTABELADO.MOEDAH'                                                                 , 'NW4_CMOEDH', bGetMoeda                                , ''         , Nil         })
		aAdd( aDePara, { 'RCR.LANCTABELADO.VALORH'                                                                 , 'NW4_VALORH', ''                                       , ''         , Nil         })
	EndIf
	aAdd( aDePara, { "NVL(TO_CHAR(RCR.LANCTABELADO.DATA_CONCLUSAO,'YYYYMMDD'),' ')"                                , 'NW4_DTCONC', ''                                       , ''         , Nil         })
	aAdd( aDePara, { 'RCR.LANCTABELADO.COD_ADVG'                                                                   , 'NW4_CPART1', bGetPart                                 , ''         , Nil         })
	//TODO: Verificar com o Jacques se � necess�rio preencher os dados de caso m�e.
	//aAdd( aDePara, { "''''"+"||TRIM(RCR.LANCTABELADO.COD_CLIENTE)||'|'||TRIM(RCR.LANCTABELADO.PASTA_REAL)||"+"''''", 'NW4_CCLICM', bGetCliCont                              , ''         , Nil         })
	//aAdd( aDePara, { "''''"+"||TRIM(RCR.LANCTABELADO.COD_CLIENTE)||'|'||TRIM(RCR.LANCTABELADO.PASTA_REAL)||"+"''''", 'NW4_CLOJCM', bGetLojCont                              , ''         , Nil         })
	//aAdd( aDePara, { "''''"+"||TRIM(RCR.LANCTABELADO.COD_CLIENTE)||'|'||TRIM(RCR.LANCTABELADO.PASTA_REAL)||"+"''''", 'NW4_CCASCM', bGetCasCont                              , ''         , Nil         })
	aAdd( aDePara, { 'NVL(RCR.LANCTABELADO.INDICE,0)'                                                              , 'NW4_COTAC' , ''                                       , ''         , Nil         })
ElseIf cTab == 'NVV' // Fatura Adicional
	aAdd( aDePara, { ''                                                                       , 'NVV_COD'   , {|| CriaVar("NVV_COD", .T.)}      , ''         , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(RCR.FATURAADICIONAL.FADDREFERENCIA,'YYYYMMDD'),' ')"        , 'NVV_DTBASE', ''                                , ''         , Nil         })
	aAdd( aDePara, { "NVL(LPAD(RCR.CLIENTE.GRENCODIGO,6,'0'),' ')"                            , 'NVV_CGRUPO', ''                                , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADNCLIENTE"                                        , 'NVV_CCLIEN', bGetSA1Cli                        , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADNCLIENTE||'|'||RCR.FATURAADICIONAL.FADNENDERECO" , 'NVV_CLOJA' , bGetSA1Loj                        , ''         , Nil         })
	aAdd( aDePara, { "NVL(LPAD(RCR.FATURAADICIONAL.FADNPARCELA,4,'0'), ' ')"                  , 'NVV_PARC'  , ''                                , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADNCLIENTE||'|'||RCR.FATURAADICIONAL.FADNCASOFAT"  , 'NVV_CCONTR', bGetContr                         , ''         , Nil         })
	aAdd( aDePara, { cChaveNVV                                                                , ''          , ''                                , ''         , bMigraFatAdt})
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADCTIMESHEET"                                      , 'NVV_TRATS' , ''                                , ''         , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(RCR.FATURAADICIONAL.REFINI_H,'YYYYMMDD'),' ')"              , 'NVV_DTINIH', ''                                , ''         , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(RCR.FATURAADICIONAL.REFFIM_H,'YYYYMMDD'),' ')"              , 'NVV_DTFIMH', ''                                , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADCMOEDA"                                          , 'NVV_CMOE1' , bGetMoeda                         , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADNVALORH"                                         , 'NVV_VALORH',                                   , ''         , Nil         })
	aAdd( aDePara, { ""                                                                       , 'NVV_TRALT' , "2"                               , ''         , Nil         })

	// TODO: Verificar com o Jacques como funciona fatura adicional com tabelado, pois os campos a baixo s�o obrigat�rios se NVV_TRALT for Sim
	// aAdd( aDePara, { "RCR.FATURAADICIONAL.TRANSLANCTAB"                                       , 'NVV_TRALT' , {{"S", "1"}, {"N", "2"}}          , ''         , Nil         })
	// aAdd( aDePara, { ""                                                                       , 'NVV_DTINIT',                                   , ''         , Nil         })
	// aAdd( aDePara, { ""                                                                       , 'NVV_DTFIMT',                                   , ''         , Nil         })
	// aAdd( aDePara, { ""                                                                       , 'NVV_CMOE4' ,                                   , ''         , Nil         })
	// aAdd( aDePara, { ""                                                                       , 'NVV_VALORT',                                   , ''         , Nil         })
		
	aAdd( aDePara, { "RCR.FATURAADICIONAL.COBDESPCASO"                                        , 'NVV_DSPCAS', {{"S", "1"}, {"N", "2"}}          , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADCDESPESA"                                        , 'NVV_TRADSP', {{"S", "1"}, {"N", "2"}}          , ''         , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(RCR.FATURAADICIONAL.REFINI_D,'YYYYMMDD'),' ')"              , 'NVV_DTINID',                                   , ''         , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(RCR.FATURAADICIONAL.REFFIM_D,'YYYYMMDD'),' ')"              , 'NVV_DTFIMD',                                   , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADCMOEDADESP"                                      , 'NVV_CMOE2' , bGetMoeda                         , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADNVALORD"                                         , 'NVV_VALORD',                                   , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADCSOCIO"                                          , 'NVV_CPART1', bGetPart                          , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADNORGANIZACAO"                                    , 'NVV_CESCR' , bGetEscrt                         , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADCIDIOMAREL"                                      , 'NVV_CIDIO1',                                   , ''         , Nil         })
	aAdd( aDePara, { "NVL(LPAD(RCR.FATURAADICIONAL.TIPO_FATURA,3,'0'), ' ')"                  , 'NVV_CTPFAT',                                   , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.SITUACAO"                                           , 'NVV_SITUAC',                                   , ''         , Nil         })
	//TODO: Definir valor padr�o - Jacques
	aAdd( aDePara, { "NVL(RCR.FATURAADICIONAL.FADCDESCRICAO,' ')"                             , 'NVV_DESREL', bValDefaut                        , ''         , Nil         })
	//TODO: Definir valor padr�o - Jacques
	aAdd( aDePara, { "NVL(RCR.FATURAADICIONAL.FADCDESCRICAO2,' ')"                            , 'NVV_DESCRT', bValDefaut                        , ''         , Nil         })
	aAdd( aDePara, { "'1'"                                                                    , 'NVV_TPRAT' ,                                   , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADCMOEDAFATURA"                                    , 'NVV_CMOE3' , bGetMoeda                         , ''         , Nil         })
	// Pagador
	aAdd( aDePara, { ''                                                                       , 'NXG_COD'   , {|| CriaVar("NXG_COD", .T.)}      , ''         , Nil         })
	aAdd( aDePara, { ""                                                                       , 'NXG_CFATAD', {|| aRegs[nI][1]}                 , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADNCLIENTE"                                        , 'NXG_CLIPG' , bGetSA1Cli                        , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADNCLIENTE||'|'||RCR.FATURAADICIONAL.FADNENDERECO" , 'NXG_LOJAPG', bGetSA1Loj                        , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADNCLIENTE||'|'||RCR.FATURAADICIONAL.FADNENDERECO" , 'NXG_CCONT ', bGetContat                        , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADNCLIENTE||'|'||RCR.FATURAADICIONAL.FADNCASOFAT"  , 'NXG_CCDPGT', bGetCondCs                        , ''         , Nil         })
	aAdd( aDePara, { "100"                                                                    , 'NXG_PERCEN',                                   , ''         , Nil         })
	aAdd( aDePara, { "0"                                                                      , 'NXG_DESPAD',                                   , ''         , Nil         })	
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADCFORMAPGTO"                                      , 'NXG_FPAGTO',                                   , ''         , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(RCR.FATURAADICIONAL.FADDVENCIMENTO,'YYYYMMDD'),' ')"        , 'NXG_DTVENC',                                   , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADNCTANCODIG"                                      , 'NXG_CBANCO', bGetCdBank                        , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADNCTANCODIG"                                      , 'NXG_CAGENC', bGetCdAgen                        , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADNCTANCODIG"                                      , 'NXG_CCONTA', bGetCdCont                        , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONAL.FADCMOEDAFATURA"                                    , 'NXG_CMOE'  , bGetMoeda                         , ''         , Nil         })
	aAdd( aDePara, { "RCR.PASTA.COD_REL"                                                      , 'NXG_CRELAT', bGetCodRel                        , ''         , Nil         })
	aAdd( aDePara, { "RCR.PASTA.IDIOMA"                                                       , 'NXG_CIDIO' ,                                   , ''         , Nil         })
	aAdd( aDePara, { "NVL(LPAD(RCR.CLIENTE.COD_RELCARTACOB,04,'0'), ' ')"                     , 'NXG_CCARTA',                                   , ''         , Nil         })
	aAdd( aDePara, { "RCR.PASTA.IDIOMA_CARTA_COB"                                             , 'NXG_CIDIO2',                                   , ''         , Nil         })
	aAdd( aDePara, { "RCR.PASTA.TXADM_DESP_TRIB"                                              , 'NXG_TXADM' ,                                   , ''         , Nil         })	
	aAdd( aDePara, { "RCR.PASTA.GROSSUP_DESP_TRIB"                                            , 'NXG_GROSUP',                                   , ''         , Nil         })	
	aAdd( aDePara, { "0"                                                                      , 'NXG_PIRRF' ,                                   , ''         , Nil         })	
	aAdd( aDePara, { "0"                                                                      , 'NXG_PPIS'  ,                                   , ''         , Nil         })	
	aAdd( aDePara, { "0"                                                                      , 'NXG_PCOFIN',                                   , ''         , Nil         })	
	aAdd( aDePara, { "0"                                                                      , 'NXG_PCSLL' ,                                   , ''         , Nil         })	
	aAdd( aDePara, { "0"                                                                      , 'NXG_PINSS' ,                                   , ''         , Nil         })	
	aAdd( aDePara, { "0"                                                                      , 'NXG_PISS'  ,                                   , ''         , Nil         })		
	aAdd( aDePara, { "0"                                                                      , ''          ,                                   , ''         , Nil         })		

ElseIf cTab == 'NVW' // Casos da Fatura Adicional
	aAdd( aDePara, { cChaveNVW                                                                        , 'NVW_CODFAD',{|| GetDePara('RCR.FATURAADICIONAL', xValSisJur, "")}, '' , Nil })
	aAdd( aDePara, { "RCR.FATURAADICIONALPASTA.FAPNCLIENTE"                                           , 'NVW_CCLIEN', bGetSA1Cli                        , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONALPASTA.FAPNCLIENTE||'|'||RCR.FATURAADICIONALPASTA.FAPNPASTA"  , 'NVW_CLOJA' , bGetLojCas                        , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONALPASTA.FAPNPASTA"                                             , 'NVW_CCASO' , bGetCaso                          , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONALPASTA.FAPNVALORH"                                            , 'NVW_VALORH',                                   , ''         , Nil         })
	aAdd( aDePara, { "RCR.FATURAADICIONALPASTA.FAPNVALORD"                                            , 'NVW_VALORD',                                   , ''         , Nil         })
ElseIf cTab == 'NWD' // Casos da Fatura Adicional
	aAdd( aDePara, { cChaveNVV                                                            , 'NWD_CFTADC',{|| GetDePara('RCR.FATURAADICIONAL', xValSisJur, "")}, '' , Nil })
	aAdd( aDePara, { "CASE WHEN SSJR.FATURAADICIONAL_FATURA.SITUACAO = 'N' THEN '1'    "+;
                          "WHEN SSJR.FATURAADICIONAL_FATURA.SITUACAO = 'F' THEN '2'    "+;
                          "WHEN SSJR.FATURAADICIONAL_FATURA.SITUACAO = 'W' THEN '3' END"  , 'NWD_SITUAC',                                                     , '' , Nil })
	aAdd( aDePara, { "NVL(LPAD(SSJR.FATURAADICIONAL_FATURA.FATURA,9,'0'),' ')"            , 'NWD_CFATUR',                                                     , '' , Nil })
	aAdd( aDePara, { "SSJR.FATURAADICIONAL_FATURA.ORGNCODIG"                              , 'NWD_CESCR ', bGetEscrt                                           , '' , Nil })
	aAdd( aDePara, { "NVL(LPAD(SSJR.FATURAADICIONAL_FATURA.WONCODIGO,8,'0'),' ')"         , 'NWD_CWO   ',                                                     , '' , Nil })
	aAdd( aDePara, { "SSJR.FATURAADICIONAL_FATURA.MOEDA_H"                                , 'NWD_CMOE1 ', bGetMoeda                                           , '' , Nil })
	aAdd( aDePara, { "SSJR.FATURAADICIONAL_FATURA.VALOR_H"                                , 'NWD_VALORH',                                                     , '' , Nil })
	aAdd( aDePara, { "SSJR.FATURAADICIONAL_FATURA.MOEDA_D"                                , 'NWD_CMOE2 ', bGetMoeda                                           , '' , Nil })
	aAdd( aDePara, { "SSJR.FATURAADICIONAL_FATURA.VALOR_D"                                , 'NWD_VALORD',                                                     , '' , Nil })
	aAdd( aDePara, { "SSJR.FATURAADICIONAL_FATURA.VALIDA "                                , 'NWD_CANC  ', {{"S", "2"}, {"N", "1"}}                            , '' , Nil })
	aAdd( aDePara, { "NVL(LPAD(SSJR.FATURAADICIONAL_FATURA.PARCELA,4,'0'), ' ')"          , 'NWD_PARC  ',                                                     , '' , Nil })
ElseIf cTab == 'NVY' //Despesas 
	aAdd( aDePara, { "LPAD(RCR.DESPESA.CODIGO,"+cTamCodDsp+",'0')"                      , 'NVY_COD'   , ''                           , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(RCR.CLIENTE.GRENCODIGO,6,'0'), ' ')"                     , 'NVY_CGRUPO',                              , '' , Nil         })
	aAdd( aDePara, { "RCR.DESPESA.COD_CLIENTE"                                          , 'NVY_CCLIEN', bGetSA1Cli                   , '' , Nil         })
	aAdd( aDePara, { "RCR.DESPESA.COD_CLIENTE||'|'||RCR.DESPESA.PASTA_REAL"             , 'NVY_CLOJA ', bGetLojCas                   , '' , Nil         })
	aAdd( aDePara, { "RCR.DESPESA.PASTA_REAL"                                           , 'NVY_CCASO' , bGetCaso                     , '' , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(RCR.DESPESA.DATA,'YYYYMMDD'),' ')"                    , 'NVY_DATA'  ,                              , '' , Nil         })
	aAdd( aDePara, { "RCR.DESPESA.COD_ADVG"                                             , 'NVY_CPART' , bGetPart                     , '' , Nil         })
	aAdd( aDePara, { "RCR.DESPESA.TIPO"                                                 , 'NVY_CTPDSP', bTipoDesp                    , '' , Nil         })
	aAdd( aDePara, { "RCR.DESPESA.MOEDA"                                                , 'NVY_CMOEDA', bGetMoeda                    , '' , Nil         })
	aAdd( aDePara, { "RCR.DESPESA.VALOR"                                                , 'NVY_VALOR' ,                              , '' , Nil         })
	aAdd( aDePara, { "RCR.DESPESA.QUANTIDADE"                                           , 'NVY_QTD'   ,                              , '' , Nil         })
	aAdd( aDePara, { "RCR.DESPESA.DESCRICAO"                                            , 'NVY_DESCRI',                              , '' , Nil         })
	aAdd( aDePara, { "RCR.DESPESA.COBRAR"                                               , 'NVY_COBRAR',                              , '' , Nil         })
	aAdd( aDePara, { "RCR.DESPESA.OBSNAOCOBRAR"                                         , 'NVY_OBSCOB',                              , '' , Nil         })
	aAdd( aDePara, { "RCR.DESPESA.USERNAOCOBRAR"                                        , 'NVY_USRNCB', bGetPart                     , '' , Nil         })
	aAdd( aDePara, { "RCR.DESPESA.OBSERVACAO"                                           , 'NVY_OBS'   ,                              , '' , Nil         })
	aAdd( aDePara, { "CASE WHEN RCR.DESPESA.SITUACAO IN('W','F') THEN '2' ELSE '1' END" , 'NVY_SITUAC',                              , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(RCR.DESPESA.NUMEROPREFAT,8,'0'), ' ')"                   , 'NVY_CPREFT',                              , '' , Nil         })
	aAdd( aDePara, { "RCR.DESPESA.DESMEMBRADA"                                          , 'NVY_DESDIV',                              , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(RCR.DESPESA.NUM_DP_MAE,8,'0'), ' ')"                     , 'NVY_CODPAI',                              , '' , Nil         })
	aAdd( aDePara, { "RCR.DESPESA.ANO_MES"                                              , 'NVY_ANOMES', bAnoMes                      , '' , Nil         })
	aAdd( aDePara, { "RCR.DESPESA.DUTELEFONEMA"                                         , 'NVY_DURTEL',                              , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(RCR.DESPESA.LANNCODIGSA,10,'0'), ' ')"                   , 'NVY_CLANC' ,                              , '' , Nil         })
	aAdd( aDePara, { "RCR.DESPESA.CPGCNUMEROPAGAR"                                      , 'NVY_CPAGTO',                              , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(RCR.DESPESA.DESNITEM,4,'0'), ' ')"                       , 'NVY_ITDES' ,                              , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(RCR.DESPESA.DESNITEMPAGO,4,'0'), ' ')"                   , 'NVY_ITDPGT',                              , '' , Nil         })
	aAdd( aDePara, { "RCR.DESPESA.INDICE"                                               , 'NVY_COTAC ',                              , '' , Nil         })

ElseIf cTab == 'NUE' //Lan�amento Time Sheet

	aAdd( aDePara, { "LPAD(RCR.TIME_SHEET.NUMERO,"+cTamCodTs+",'0')"                             , 'NUE_COD'   ,                                   , '' , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(RCR.TIME_SHEET.DATA,'YYYYMMDD'),' ')"                          , 'NUE_DATATS',                                   , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.COD_ADVG_REAL"                                              , 'NUE_CPART1', bGetPart                          , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.COD_ADVG"                                                   , 'NUE_CPART2', bGetPart                          , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(RCR.CLIENTE.GRENCODIGO,6,'0'), ' ')"                              , 'NUE_CGRPCL',                                   , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.COD_CLIENTE"                                                , 'NUE_CCLIEN', bGetSA1Cli                        , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.COD_CLIENTE||'|'||RCR.TIME_SHEET.PASTA_REAL"                , 'NUE_CLOJA' , bGetLojCas                        , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.PASTA_REAL"                                                 , 'NUE_CCASO' , bGetCaso                          , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.CODIGO"                                                     , 'NUE_CATIVI', bGetTpAtiv                        , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.TEMPO_REAL"                                                 , 'NUE_UTL'   ,                                   , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.TEMPO_FATU"                                                 , 'NUE_UTR'   ,                                   , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.TEMPO_PROD"                                                 , 'NUE_UTP'   ,                                   , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.TEMPO_REAL"                                                 , 'NUE_HORAL' , bConvHora                         , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.TEMPO_FATU"                                                 , 'NUE_HORAR' , bConvHora                         , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.TEMPO_PROD"                                                 , 'NUE_HORAP' , bConvHora                         , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.TEMPO_REAL"                                                 , 'NUE_TEMPOL', bConvHoraF                        , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.TEMPO_FATU"                                                 , 'NUE_TEMPOR', bConvHoraF                        , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.TEMPO_PROD"                                                 , 'NUE_TEMPOP', bConvHoraF                        , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(RCR.TIME_SHEET.ID_RETIFICACAO,04,'0'), ' ')"                      , 'NUE_CRETIF',                                   , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(RCR.TIME_SHEET.LTBNNUMLANC,06,'0'), ' ')"                         , 'NUE_CLTAB ',                                   , '' , Nil         })
	aAdd( aDePara, { "SSJR.EBILL_EMPRESA.ID_DOCPADRAO"                                           , 'NUE_CDOC'  ,                                   , '' , Nil         })	
	aAdd( aDePara, { "SSJR.EBILL_TPATV_RELAC.ID_TPATV"                                           , 'NUE_CTAREB',                                   , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.ID_FASE"                                                    , 'NUE_CFASE' ,                                   , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.ID_TAREFA"                                                  , 'NUE_CTAREF',                                   , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.ATVCREVISADO"                                               , 'NUE_REVISA',{{'S', '1'}, {'N', '2'}}           , '' , Nil         })
  	aAdd( aDePara, { "NVL(TO_CHAR(RCR.TIME_SHEET.DATA_INC,'YYYYMMDD'),' ')"                      , 'NUE_DATAIN',                                   , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.COBRAR"                                                     , 'NUE_COBRAR',{{'S', '1'}, {'N', '2'}}           , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.COD_ADVG_REAL"                                              , 'NUE_CUSERA', bGetPart                          , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.VALOR"                                                      , 'NUE_VALOR' ,                                   , '' , Nil         })
	aAdd( aDePara, { "CASE WHEN RCR.TIME_SHEET.SITUACAO IN('W','F') THEN '2' ELSE '1' END"       , 'NUE_SITUAC',                                   , '' , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(RCR.TIME_SHEET.DATA_INC,'YYYYMMDD'),' ')"                      , 'NUE_ALTDT' ,                                   , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(RCR.TIME_SHEET.NUMEROPREFAT,09,'0'), ' ')"                        , 'NUE_CPREFT',                                   , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.OBSERVACAO"                                                 , 'NUE_DESC'  , bValDefaut                        , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.INDICE_PREFATURA"                                           , 'NUE_COTAC2',                                   , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(RCR.TIME_SHEET.NUM_TS_PAI,12,'0'), ' ')"                          , 'NUE_CODPAI',                                   , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.ANO_MES"                                                    , 'NUE_ANOMES', bAnoMes                           , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.MOEDA_TABH"                                                 , 'NUE_CMOEDA', bGetMoeda                         , '' , Nil         })
	aAdd( aDePara, { ""                                                                          , 'NUE_HORAIN', "00:00:00"                        , '' , Nil         })
	aAdd( aDePara, { ""                                                                          , 'NUE_ALTHR' , "00:00:00"                        , '' , Nil         })
	aAdd( aDePara, { ""                                                                          , 'NUE_TSDIV ', "2"                               , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.ESCRITORIO"                                                 , 'NUE_CESCR ',                                   , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.SETOR||RCR.TIME_SHEET.ESCRITORIO"                           , 'NUE_CC    ', {|| StrTran(xValSisJur, ' ', '')} , '' , Nil         })
	aAdd( aDePara, { ""                                                                          , 'NUE_VALORH', bValorHNUE                        , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.FLUXO_REVISAO"                                              , 'NUE_FLUREV', {{'P','1'}, {'A','2'}, {'R','3'}} , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.USUARIO_REPROVACAO"                                         , 'NUE_CREPRO', bGetPart                          , '' , Nil         })

ElseIf cTab == 'NX0' //Pr�-fatura
	aAdd( aDePara, { "NVL(LPAD(RCR.PRE_FATURA.NUMERO,8,'0'), ' ')"                          , 'NX0_COD'   ,             , '' , Nil         })
	aAdd( aDePara, { cSituaPre                                                              , 'NX0_SITUAC',             , '' , Nil         })
	aAdd( aDePara, { "TO_CHAR(RCR.PRE_FATURA.DATA_EMI, 'YYYYMMDD')"                         , 'NX0_DTEMI' ,             , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATURA.MOEDA"                                                 , 'NX0_CMOEDA',bGetMoeda    , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATURA.VALOR_FAT_H"                                           , 'NX0_VLFATH',             , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATURA.VALOR_FAT_D"                                           , 'NX0_VLREMB',             , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATURA.VALOR_FAT_D_TRIB"                                      , 'NX0_VLTRIB',             , '' , Nil         })
	aAdd( aDePara, { "(RCR.PRE_FATURA.VALOR_FAT_D + RCR.PRE_FATURA.VALOR_FAT_D_TRIB)"       , 'NX0_VLFATD',             , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATURA.VALOR_FAT_H"                                           , 'NX0_VLFATF',             , '' , Nil         })
	aAdd( aDePara, { nVlrTBPre                                                              , 'NX0_VLFATT',             , '' , Nil         })
	aAdd( aDePara, { nVlrTSPre                                                              , 'NX0_VTS'   ,             , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATURA.DESCONTO"                                              , 'NX0_VDESCT',             , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATURA.ORGNCODIG"                                             , 'NX0_CESCR' ,bGetEscrt    , '' , Nil         })
	//aAdd( aDePara, { ""                                                                     , 'NX0_CCONTR',             , '' , Nil         })
	//aAdd( aDePara, { ""                                                                     , 'NX0_CJCONT',             , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATURA.REVISOR"                                               , 'NX0_CPART' ,bGetPart     , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATURA.HDA"                                                   , 'NX0_TS'    ,             , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATURA.HDA"                                                   , 'NX0_DESP'  ,             , '' , Nil         })
	aAdd( aDePara, { nLanTabPre                                                             , 'NX0_LANTAB',             , '' , Nil         })
	//aAdd( aDePara, { ""                                                                     , 'NX0_FIXO',             , '' , Nil         })
	//aAdd( aDePara, { ""                                                                     , 'NX0_FATADC',             , '' , Nil         })
	//aAdd( aDePara, { ""                                                                     , 'NX0_CODEXI',             , '' , Nil         })
	aAdd( aDePara, { "TO_CHAR(RCR.PRE_FATURA.DATAINI_H, 'YYYYMMDD')"                        , 'NX0_DINITS',             , '' , Nil         })
	aAdd( aDePara, { "TO_CHAR(RCR.PRE_FATURA.DATAFIM_H, 'YYYYMMDD')"                        , 'NX0_DFIMTS',             , '' , Nil         })
	aAdd( aDePara, { "TO_CHAR(RCR.PRE_FATURA.DATAINI_D, 'YYYYMMDD')"                        , 'NX0_DINIDP',             , '' , Nil         })
	aAdd( aDePara, { "TO_CHAR(RCR.PRE_FATURA.DATAFIM_D, 'YYYYMMDD')"                        , 'NX0_DFIMDP',             , '' , Nil         })
	aAdd( aDePara, { cDtMinTabPre                                                           , 'NX0_DINITB',             , '' , Nil         })
	aAdd( aDePara, { cDtMaxTabPre                                                           , 'NX0_DFIMTB',             , '' , Nil         })
	//aAdd( aDePara, { ""                                                                     , 'NX0_DFIMFX',             , '' , Nil         })
	//aAdd( aDePara, { ""                                                                     , 'NX0_DINIFX',             , '' , Nil         })
	//aAdd( aDePara, { ""                                                                     , 'NX0_CFTADC',             , '' , Nil         })
	//aAdd( aDePara, { ""                                                                     , 'NX0_CPAREX',             , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(RCR.CLIENTE.GRENCODIGO,6,'0'), ' ')"                         , 'NX0_CGRUPO',             , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATURA.COD_CLIENTE"                                           , 'NX0_CCLIEN',bGetSA1Cli   , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATURA.COD_CLIENTE||'|'||RCR.PRE_FATURA.COD_ENDERECO"         , 'NX0_CLOJA' ,bGetSA1Loj   , '' , Nil         })
	//aAdd( aDePara, { ""                                                                     , 'NX0_CCONTA',             , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATURA.ID_SITUACAO"                                           , 'NX0_SITCB' ,             , '' , Nil         })
	aAdd( aDePara, { "TO_CHAR(RCR.PRE_FATURA.DATA_INC, 'YYYYMMDD')"                         , 'NX0_DTINC' ,             , '' , Nil         })
	aAdd( aDePara, { "CASE WHEN RCR.PRE_FATURA.PREFAT_REVISADA = 'N' THEN '2' ELSE '1' END" , 'NX0_REVIS' ,             , '' , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(RCR.PRE_FATURA.DATALIBPRE, 'YYYYMMDD'),' ')"              , 'NX0_DTLIB' ,             , '' , Nil         })
	aAdd( aDePara, { cTpEmisPre                                                             , 'NX0_TPEMI' ,             , '' , Nil         })
	//aAdd( aDePara, { ""                                                                     , 'NX0_HRLIB',             , '' , Nil         })
	aAdd( aDePara, { "NVL(DBMS_LOB.SUBSTR(RCR.PRE_FATURA.OBSERVACAO, 4000, 1 ),' ')"        , 'NX0_OBSFAT',             , '' , Nil         })
	aAdd( aDePara, { "NVL(DBMS_LOB.SUBSTR(RCR.PRE_FATURA.OBS_FATURAMENTO, 4000, 1 ),' ')"   , 'NX0_OBSREV',             , '' , Nil         })
	//aAdd( aDePara, { ""                                                                     , 'NX0_OBSRED',             , '' , Nil         })
	aAdd( aDePara, { "TO_CHAR(RCR.PRE_FATURA.DATA_ULT_ALT, 'YYYYMMDD')"                     , 'NX0_DTALT' ,             , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATURA.USUARIO"                                               , 'NX0_USRALT',             , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATURA.IDIOMA"                                                , 'NX0_CIDIO' ,             , '' , Nil         })
	aAdd( aDePara, { ""                                                                     , 'NX0_PERFAT', {||100}     , '' , Nil         })
	aAdd( aDePara, { "CASE WHEN NVL(RCR.PRE_FATURA.FATURA,0) <> 0 THEN '1' ELSE '2' END"    , 'NX0_FATURA',             , '' , Nil         })

ElseIf cTab == 'NX1' //Pre-fatura Caso 

	aAdd( aDePara, { "NVL(LPAD(RCR.PRE_FATPASTA.NUM_PRE_FAT,8,'0'), ' ')"                                                 , 'NX1_CPREFT',                                                     , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATPASTA.CLIENTE"                                                                           , 'NX1_CCLIEN',bGetSA1Cli                                           , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATPASTA.CLIENTE||'|'||RCR.PASTA.COD_ENDERECO"                                              , 'NX1_CLOJA' ,bGetSA1Loj                                           , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATPASTA.PASTA"                                                                             , 'NX1_CCASO' ,bGetCaso                                             , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATPASTA.VALORH_FATURADO"                                                                   , 'NX1_VHON'  ,                                                     , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATPASTA.VALOR_D"                                                                           , 'NX1_VDESP' ,                                                     , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATPASTA.VALOR_D"                                                                           , 'NX1_VLREMB',                                                     , '' , Nil         })
	aAdd( aDePara, { "NVL(RCR.PRE_FATPASTA.VALOR_D_TRIB,0)"                                                               , 'NX1_VLTRIB',                                                     , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATPASTA.VALOR_TABELADO"                                                                    , 'NX1_VTAB'  ,                                                     , '' , Nil         })
	aAdd( aDePara, { "CASE WHEN RCR.PRE_FATPASTA.REVISADO	= 'N' THEN '2' ELSE '1' END"                                  , 'NX1_SITREV',                                                     , '' , Nil         })	
	aAdd( aDePara, { "NVL(DBMS_LOB.SUBSTR(RCR.PRE_FATPASTA.OBSERVACAO_REVISOR, 4000, 1 ),' ')"                            , 'NX1_INSREV',                                                     , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATPASTA.VALOR_H"                                                                           , 'NX1_VTS'   ,                                                     , '' , Nil         })
	aAdd( aDePara, { "NVL(RCR.PRE_FATPASTA.REVISOR,' ')"                                                                  , 'NX1_CPART' ,bGetPart                                             , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATPASTA.VALORUTILFAT"                                                                      , 'NX1_VUTFAT',                                                     , '' , Nil         })
	aAdd( aDePara, { "NVL(DBMS_LOB.SUBSTR(RCR.PRE_FATPASTA.OBSERVACAO_FATURISTA, 4000, 1 ),' ')"                          , 'NX1_INSFAT',                                                     , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATPASTA.VALORTABHORA"                                                                      , 'NX1_VFXFAT',                                                     , '' , Nil         })
	aAdd( aDePara, { "NVL(DBMS_LOB.SUBSTR(RCR.PRE_FATPASTA.REDACAO, 4000, 1 ),' ')"                                       , 'NX1_REDAC' ,                                                     , '' , Nil         })
	aAdd( aDePara, { nLanTabPFC                                                                                           , 'NX1_LANTAB',                                                     , '' , Nil         })	
	aAdd( aDePara, { "RCR.PRE_FATPASTA.DESCONTO"                                                                          , 'NX1_VLDESC',                                                     , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATPASTA.DESCONTO2"                                                                         , 'NX1_VDESCO',                                                     , '' , Nil         })
	aAdd( aDePara, { "NVL(RCR.PRE_FATPASTA.MOEDA_TABHONOR,' ')"                                                           , 'NX1_CMOETH',bGetMoeda                                            , '' , Nil         })
	aAdd( aDePara, { "CASE WHEN RCR.PRE_FATPASTA.TSREVISADO	= 'N' THEN '2' ELSE '1' END"                                  , 'NX1_TSREV' ,                                                     , '' , Nil         })
	aAdd( aDePara, { "CASE WHEN RCR.PRE_FATPASTA.DESPREVISADA	= 'N' THEN '2' ELSE '1' END"                              , 'NX1_DSPREV',                                                     , '' , Nil         })
	aAdd( aDePara, { cTSheetPFC                                                                                           , 'NX1_TS'    ,                                                     , '' , Nil         })
	aAdd( aDePara, { "CASE WHEN RCR.PRE_FATPASTA.HDA = 'D' THEN '1' ELSE '2' END"                                         , 'NX1_DESP'  ,                                                     , '' , Nil         })
ElseIf cTab == 'NXH' //Protocolo 
	aAdd( aDePara, { "NVL(LPAD(SSJR.FAT_PROTOCOLO.id_protocolo,09,'0'), ' ')"                                             , 'NXH_COD'   ,                                                     , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(SSJR.FAT_PROTOCOLO.id_tipoprotocolo,04,'0'), ' ')"                                         , 'NXH_CTIPO' ,                                                     , '' , Nil         })
	aAdd( aDePara, { "UPPER(SSJR.FAT_PROTOCOLO.razao_social)"                                                             , 'NXH_RZSOC' ,                                                     , '' , Nil         })
	aAdd( aDePara, { "UPPER(SSJR.FAT_PROTOCOLO.logradouro)"                                                               , 'NXH_LOGRAD',                                                     , '' , Nil         })	
	aAdd( aDePara, { "UPPER(SSJR.FAT_PROTOCOLO.bairro)"                                                                   , 'NXH_BAIRRO',                                                     , '' , Nil         })
	aAdd( aDePara, { "UPPER(SSJR.FAT_PROTOCOLO.estado)"                                                                   , 'NXH_UF'    ,                                                     , '' , Nil         })
	aAdd( aDePara, { "UPPER(SSJR.FAT_PROTOCOLO.cidade)"                                                                   , 'NXH_CID'   ,                                                     , '' , Nil         })
	aAdd( aDePara, { "UPPER(SSJR.FAT_PROTOCOLO.pais)"                                                                     , 'NXH_PAIS'  ,                                                     , '' , Nil         })
	aAdd( aDePara, { "SSJR.FAT_PROTOCOLO.cep"                                                                             , 'NXH_CEP'   ,                                                     , '' , Nil         })
	aAdd( aDePara, { "UPPER(SSJR.FAT_PROTOCOLO.contato)"                                                                  , 'NXH_CONTAT',                                                     , '' , Nil         })
	aAdd( aDePara, { "NVL(SSJR.FAT_PROTOCOLO.observacao,' ')"                                                             , 'NXH_OBS'   ,                                                     , '' , Nil         })		
ElseIf cTab == 'NXI' //Faturas do Protocolo
	aAdd( aDePara, { "NVL(LPAD(SSJR.FAT_PROTOCOLOFATURA.id_protocolo,09,'0'), ' ')" , 'NXI_CPROT' ,          , '' , Nil         })
	aAdd( aDePara, { "SSJR.FAT_PROTOCOLOFATURA.orgncodig"                           , 'NXI_CESCR' , bGetEscrt, '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(SSJR.FAT_PROTOCOLOFATURA.numero,09,'0'), ' ')"       , 'NXI_CFAT'  ,          , '' , Nil         })
ElseIf cTab == 'NXJ' //Vias do Protocolo
	aAdd( aDePara, { "NVL(LPAD(SSJR.FAT_PROTOCOLOVIA.id_via,04,'0'), ' ')"                                                , 'NXJ_COD'   ,                                                     , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(SSJR.FAT_PROTOCOLOVIA.id_protocolo,09,'0'), ' ')"                                          , 'NXJ_CPROT' ,                                                     , '' , Nil         })
	aAdd( aDePara, { "TO_CHAR(SSJR.FAT_PROTOCOLOVIA.data_envio, 'YYYYMMDD')"                                              , 'NXJ_DTENV' ,                                                     , '' , Nil         })
	aAdd( aDePara, { "TO_CHAR(SSJR.FAT_PROTOCOLOVIA.data_recebido, 'YYYYMMDD')"                                           , 'NXJ_DTREC' ,                                                     , '' , Nil         })
	aAdd( aDePara, { "SSJR.FAT_PROTOCOLOVIA.obs_recebido"                                                                 , 'NXJ_OBS'   ,                                                     , '' , Nil         })
	aAdd( aDePara, { "SSJR.FAT_PROTOCOLOVIA.quemrecebeu"                                                                  , 'NXJ_QUEMRE',                                                     , '' , Nil         })	
ElseIf cTab == 'NWM' //LAN�AMENTO TABELADO EM LOTE
	aAdd( aDePara, { "NVL(LPAD(SSJR.CADLANCTABLOTE.ltlid_lanctablote,06,'0'),' ')"               , 'NWM_COD'   ,             , '' , Nil })
	aAdd( aDePara, { cLtbGrpCli                                                                  , 'NWM_CGRUPO',             , '' , Nil })
	aAdd( aDePara, { "SSJR.CADLANCTABLOTE.ltlcod_cliente"                                        , 'NWM_CCLIEN', bGetSA1Cli  , '' , Nil })
	aAdd( aDePara, { "SSJR.CADLANCTABLOTE.ltlcod_cliente||'|'||RCR.CLIENTE.END_PADRAO"           , 'NWM_CLOJA' , bGetSA1Loj  , '' , Nil })
	aAdd( aDePara, { "SSJR.CADLANCTABLOTE.ltlcod_cliente||'|'||SSJR.CADLANCTABLOTE.LTLCASO_MAE"  , 'NWM_CCONTR', bGetContr   , '' , Nil })
	aAdd( aDePara, { "SSJR.CADLANCTABLOTE.ltlqtde"                                               , 'NWM_QUANT' ,             , '' , Nil })
	aAdd( aDePara, { "NVL(LPAD(SSJR.CADLANCTABLOTE.ltlitbccodigo,20,'0'),' ')"                   , 'NWM_CTPSRV',             , '' , Nil })
	aAdd( aDePara, { "SSJR.CADLANCTABLOTE.ltldescricao"                                          , 'NWM_DESCRI',             , '' , Nil })
	aAdd( aDePara, { "SSJR.CADLANCTABLOTE.ltlmoeda"                                              , 'NWM_CMOEDA', bGetMoeda   , '' , Nil })
	aAdd( aDePara, { "SSJR.CADLANCTABLOTE.ltlvalor_base"                                         , 'NWM_VLBASE',             , '' , Nil })
	aAdd( aDePara, { "TO_CHAR(SSJR.CADLANCTABLOTE.ltldatabase, 'YYYYMMDD')"                      , 'NWM_DTBASE',             , '' , Nil })
	aAdd( aDePara, { "CASE WHEN SSJR.CADLANCTABLOTE.ltltipo_correcao = 'N' THEN '1' ELSE '2' END", 'NWM_TPCORR',             , '' , Nil })
	aAdd( aDePara, { "NVL(SSJR.CADLANCTABLOTE.ltlindice_correcao,' ')"                           , 'NWM_CINDIC',             , '' , Nil })
	aAdd( aDePara, { "SSJR.CADLANCTABLOTE.ltlvalor_atualizado"                                   , 'NWM_VLATUA',             , '' , Nil })
	aAdd( aDePara, { "SSJR.CADLANCTABLOTE.ltlano_mes_inicial"                                    , 'NWM_AMINI' , bAnoMes     , '' , Nil })
	aAdd( aDePara, { "SSJR.CADLANCTABLOTE.ltlano_mes_final"                                      , 'NWM_AMFIM' , bAnoMes     , '' , Nil })
	aAdd( aDePara, { "NVL(TO_CHAR(SSJR.CADLANCTABLOTE.ltldia_gera),' ')"                         , 'NWM_DIAGER',             , '' , Nil })
	aAdd( aDePara, { "TO_CHAR(SSJR.CADLANCTABLOTE.ltlperiodicidade_cob)"                         , 'NWM_PERCOB',             , '' , Nil })
	aAdd( aDePara, { "SSJR.CADLANCTABLOTE.ltlcod_usuario"                                        , 'NWM_ALTPAR', bGetPart    , '' , Nil })
    aAdd( aDePara, { "TO_CHAR(SSJR.CADLANCTABLOTE.ltldata, 'YYYYMMDD')"                          , 'NWM_ALTDT' ,             , '' , Nil })
	aAdd( aDePara, { "NVL(TO_CHAR(SSJR.CADLANCTABLOTE.ltlperiodicidade_cor),' ')"                , 'NWM_PERCOR',             , '' , Nil })
	aAdd( aDePara, { "TO_CHAR(SSJR.CADLANCTABLOTE.ltldata, 'YYYYMMDD')"                          , 'NWM_ALTHR' ,             , '' , Nil })
ElseIf cTab == 'NZQ' //Solicita��o/Aprova��o Despesa
	aAdd( aDePara, { "NVL(LPAD(SSJR.TS_DESPESA.ID_TSDESPESA,8,'0'), ' ')"             , 'NZQ_COD'   ,                                      , '' , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(SSJR.TS_DESPESA.DATA_INCLUSAO,'YYYYMMDD'),' ')"     , 'NZQ_DTINCL',                                      , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.USUARIO_INCLUSAO"                               , 'NZQ_USRINC',                                      , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.SITUACAO"                                       , 'NZQ_SITUAC', {{"P", "1"}, {"A", "2"}, {"C", "3"}} , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.TIPO"                                           , 'NZQ_DESPES', {{"C", "1"}, {"E", "2"}}             , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.PROFISSIONAL"                                   , 'NZQ_CPART' , bGetPart                             , '' , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(SSJR.TS_DESPESA.ID_TIPOPRESTCONTAS),' ')"           , 'NZQ_CTPCTA',                                      , '' , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(SSJR.TS_DESPESA.DATA_DESPESA,'YYYYMMDD'),' ')"      , 'NZQ_DATA'  ,                                      , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.MOEDA"                                          , 'NZQ_CMOEDA', bGetMoeda                            , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.REVISADA"                                       , 'NZQ_REVISA',                                      , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.COD_CLIENTE"                                    , 'NZQ_CCLIEN', bGetSA1Cli                           , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.COD_CLIENTE||'|'||RCR.PASTA.COD_ENDERECO"       , 'NZQ_CLOJA' , bGetSA1Loj                           , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.PASTA"                                          , 'NZQ_CCASO' , bGetCaso                             , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.TIPO_DESP"                                      , 'NZQ_CTPDSP', bTipoDesp                            , '' , Nil         })	
	aAdd( aDePara, { "NVL(TO_CHAR(SSJR.TS_DESPESA.ID_LOCALIDADE),' ')"                , 'NZQ_CLOCAL',                                      , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.KM"                                             , 'NZQ_KM'    ,                                      , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.QUANT"                                          , 'NZQ_QTD'   ,                                      , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.VALOR"                                          , 'NZQ_VALOR' ,                                      , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.RESP_APROVACAO"                                 , 'NZQ_CODRES', bGetPart                             , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.DESCRICAO"                                      , 'NZQ_DESC'  ,                                      , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.OBSERVACAO"                                     , 'NZQ_OBSERV',                                      , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.MOTIVO_REPROVACAO"                              , 'NZQ_MOTREP',                                      , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.COBRAR"                                         , 'NZQ_COBRAR',                                      , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.CONTAFINANCEIRA"                                , 'NZQ_CTADES', bNatureza                            , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.CC_ESCRITORIO"                                  , 'NZQ_CESCR' ,                                      , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.CC_GRUPOJURIDICO||SSJR.TS_DESPESA.CC_ESCRITORIO", 'NZQ_GRPJUR', {|| StrTran(xValSisJur, ' ', '')}  , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.CC_PROFISSIONAL"                                , 'NZQ_CODPRO', bGetPart                             , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.CC_TABELARATEIO"                                , 'NZQ_CRATEI', bGetTabRat                           , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(SSJR.TS_DESPESA.ID_PROJETO,6,'0'), ' ')"               , 'NZQ_CPROJE',                                      , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(SSJR.TS_DESPESA.ID_PROJETO_ITEM,6,'0'), ' ')"          , 'NZQ_CITPRJ',                                      , '' , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(SSJR.TS_DESPESA.DATA_APROVACAO,'YYYYMMDD'),' ')"    , 'NZQ_DTAPRV',                                      , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.APROVADOR"                                      , 'NZQ_APROVA', bGetPart                             , '' , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(SSJR.TS_DESPESA.DATA_EMAIL,'YYYYMMDD'),' ')"        , 'NZQ_DTEMSO',                                      , '' , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(SSJR.TS_DESPESA.DATA_MAIL_APROV,'YYYYMMDD'),' ')"   , 'NZQ_DTEMAP',                                      , '' , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(SSJR.TS_DESPESA.DATA_EMAIL_CANC,'YYYYMMDD'),' ')"   , 'NZQ_DTEMCA',                                      , '' , Nil         })
	aAdd( aDePara, { "NVL(SSJR.TS_DESPESA.FLAG_MAIL,' ')"                             , 'NZQ_EMSOLI',                                      , '' , Nil         })
	aAdd( aDePara, { "NVL(SSJR.TS_DESPESA.FLAG_MAIL_CANC,' ')"                        , 'NZQ_EMCANC',                                      , '' , Nil         })
	aAdd( aDePara, { "NVL(SSJR.TS_DESPESA.FLAG_MAIL_APROV,' ')"                       , 'NZQ_EMAPRO',                                      , '' , Nil         })	
	aAdd( aDePara, { "NVL(LPAD(SSJR.TS_DESPESA.LANNCODIG,10,'0'), ' ')"               , 'NZQ_CLANC ',                                      , '' , Nil         })
	aAdd( aDePara, { "SSJR.TS_DESPESA.CPGCNUMEROPAGAR"                                , 'NZQ_CPAGTO',                                      , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(SSJR.TS_DESPESA.DESNITEM,4,'0'), ' ')"                 , 'NZQ_ITDES ',                                      , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(SSJR.TS_DESPESA.DESNITEMPAGO,4,'0'), ' ')"             , 'NZQ_ITDPGT',                                      , '' , Nil         })
ElseIf cTab == 'NWN' //CASOS VINCULADOS AO LANC TAB
	aAdd( aDePara, { "NVL(LPAD(SSJR.LANCTABLOTE_CASO.lcltlid_lanctablote,06,'0'), ' ')"                                   , 'NWN_CLOTE' ,                                                     , '' , Nil         })
	aAdd( aDePara, { "SSJR.LANCTABLOTE_CASO.lccaso"                                                                       , 'NWN_CCASO' ,bGetCaso                                             , '' , Nil         })
	aAdd( aDePara, { "SSJR.LANCTABLOTE_CASO.lccod_advg"                                                                   , 'NWN_CPART' ,bGetPart                                             , '' , Nil         })
ElseIf cTab == 'NX2' //Pr�-fatura Participante
	aAdd( aDePara, { "NVL(LPAD(RCR.PRE_TOTADV.NUM_PREFAT,08,'0'), ' ')"                                                   , 'NX2_CPREFT',                                                     , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_TOTADV.COD_ADVG"                                                                            , 'NX2_CPART' ,bGetPart                                             , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_TOTADV.TPDC"                                                                                , 'NX2_UTCLI' ,                                                     , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_TOTADV.TRSPC"                                                                               , 'NX2_UTLANC',                                                     , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_TOTADV.TFSPC"                                                                               , 'NX2_UTR'   ,                                                     , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_TOTADV.VALOR_HORA"                                                                          , 'NX2_VALORH',                                                     , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_TOTADV.MOEDA_TABH"                                                                          , 'NX2_CMOTBH',bGetMoeda                                            , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_TOTADV.VALOR"                                                                               , 'NX2_VLHTBH',                                                     , '' , Nil         })
	aAdd( aDePara, { "LPAD(RCR.PRE_TOTADV.ID_LANCTAB,6,'0')"                                                              , 'NX2_CLTAB' ,                                                     , '' , Nil         })	
	aAdd( aDePara, { "RCR.PRE_TOTADV.MOEDA"                                                                               , 'NX2_CMOPRE',bGetMoeda                                            , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_TOTADV.COD_CLIENTE"                                                                         , 'NX2_CCLIEN',bGetSA1Cli                                           , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_TOTADV.COD_CLIENTE||'|'||RCR.PASTA.COD_ENDERECO"                                            , 'NX2_CLOJA ',bGetLojCas                                           , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_TOTADV.PASTA"                                                                               , 'NX2_CCASO ',bGetCaso                                             , '' , Nil         })
    aAdd( aDePara, { ""                                                                                                   , 'NX2_CODSEQ',{|| CodSeq(1)}                                       , '' , Nil         })
ElseIf cTab == 'NX4' //Historico de Cobranca
  //aAdd( aDePara, { "NVL(LPAD(SSJR.FAT_PREFATURA_COBRANCA_TMP.ID_PREFATURACOBRANCATMP,08,'0'),' ')"                      , 'NX4_COD'   ,                                                     , '' , Nil         })
	aAdd( aDePara, { ""                                                                                                   , 'NX4_COD'   ,{|| CriaVar("NXG_COD", .T.)}                         , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(SSJR.FAT_PREFATURA_COBRANCA_TMP.NUMPREFAT,08,'0'),' ')"                                    , 'NX4_CPREFT',                                                     , '' , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(SSJR.FAT_PREFATURA_COBRANCA_TMP.DATA,'YYYYMMDD'),' ')"                                  , 'NX4_DTINC' ,                                                     , '' , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(SSJR.FAT_PREFATURA_COBRANCA_TMP.DATA,'HH24:MI:SS'),' ')"                                , 'NX4_HRINC' ,                                                     , '' , Nil         }) 
	aAdd( aDePara, { "SSJR.FAT_PREFATURA_COBRANCA_TMP.HISTORICO"                                                          , 'NX4_HIST'  ,                                                     , '' , Nil         })
	aAdd( aDePara, { "SSJR.FAT_PREFATURA_COBRANCA_TMP.USUARIO"                                                            , 'NX4_USRINC',bGetPart                                             , '' , Nil         })	
	aAdd( aDePara, { "NVL(TO_CHAR(SSJR.FAT_PREFATURA_COBRANCA_TMP.DATASAIDAFAT  ,'YYYYMMDD'),' ')"                        , 'NX4_DTSAID',                                                     , '' , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(SSJR.FAT_PREFATURA_COBRANCA_TMP.DATASAIDAFAT,'HH24:MI:SS'),' ')"		                  , 'NX4_HRSAID',                                                     , '' , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(SSJR.FAT_PREFATURA_COBRANCA_TMP.DATARETORNOFAT,'YYYYMMDD'),' ')"                        , 'NX4_DTRET' ,                                                     , '' , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(SSJR.FAT_PREFATURA_COBRANCA_TMP.DATARETORNOFAT,'HH24:MI:SS'),' ')"		              , 'NX4_HRRET' ,                                                     , '' , Nil         })
	aAdd( aDePara, { "NVL(SSJR.FAT_PREFATURA_COBRANCA_TMP.REVISOR,' ')"                                                   , 'NX4_CPART' ,bGetPart                                             , '' , Nil         })
	aAdd( aDePara, { cTpCobPreFat                                                                                         , 'NX4_TIPO'  ,                                                     , '' , Nil         })	
	aAdd( aDePara, { "'2'"                                                                                                , 'NX4_AUTO'  ,                                                     , '' , Nil         })    		
ElseIf cTab == 'OHN' //S�cios / Revisores
	aAdd( aDePara, { "NVL(LPAD(RCR.PRE_FATPASTA.NUM_PRE_FAT,08,'0'), ' ')"                                                , 'OHN_CPREFT',                                                     , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATPASTA.CLIENTE"                                                                           , 'OHN_CCLIEN',bGetSA1Cli                                           , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATPASTA.CLIENTE||'|'||RCR.PASTA.COD_ENDERECO"                                              , 'OHN_CLOJA' ,bGetLojCas                                           , '' , Nil         })
	aAdd( aDePara, { "RCR.PRE_FATPASTA.PASTA"                                                                             , 'OHN_CCASO' ,bGetCaso                                             , '' , Nil         })
	aAdd( aDePara, { "'2'"                                                                                                , 'OHN_TIPO'  ,           	                                      , '' , Nil         })
	aAdd( aDePara, { "NVL(RCR.PRE_FATPASTA.REVISOR,' ')"                                                                  , 'OHN_CPART' ,bGetPart                                             , '' , Nil         })
    aAdd( aDePara, { "2"                                                                                                  , 'OHN_ORDEM' ,           	                                      , '' , Nil         })
    aAdd( aDePara, { "'3'"                                                                                                , 'OHN_REVISA',           	                                      , '' , Nil         })    
ElseIf cTab == 'NW0' //Time Sheet Faturamento
	aAdd( aDePara, { "NVL(LPAD(SSJR.TIME_SHEET_FATURA.cod_timesheet,"+cTamCodTs+",'0'), ' ')"                             , 'NW0_CTS'   ,                  , '' , Nil         })
	aAdd( aDePara, { cSitTSheet                                                                                           , 'NW0_SITUAC',                  , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(SSJR.TIME_SHEET_FATURA.fatura,09,'0'), ' ')"                                               , 'NW0_CFATUR',                  , '' , Nil         })
	aAdd( aDePara, { "SSJR.TIME_SHEET_FATURA.orgncodig"                                                                   , 'NW0_CESCR' , bGetEscrt        , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(SSJR.TIME_SHEET_FATURA.woncodigo,8,'0'),' ')"                                              , 'NW0_CWO'   ,                  , '' , Nil         })
	aAdd( aDePara, { "'2'"                                                                                                , 'NW0_CANC'  ,                  , '' , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(SSJR.TIME_SHEET_FATURA.data_atual,'YYYYMMDD'),' ')"                                     , 'NW0_DATATS',                  , '' , Nil         })
	aAdd( aDePara, { "SSJR.TIME_SHEET_FATURA.cod_cliente"                                                                 , 'NW0_CCLIEN', bGetSA1Cli       , '' , Nil         })
	aAdd( aDePara, { "SSJR.TIME_SHEET_FATURA.cod_cliente||'|'||SSJR.TIME_SHEET_FATURA.cod_endereco"                       , 'NW0_CLOJA' , bGetSA1Loj       , '' , Nil         })
	aAdd( aDePara, { 'RCR.TIME_SHEET.PASTA_REAL'                                                                          , 'NW0_CCASO' , bGetCaso         , '' , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.COD_ADVG"                                                                            , 'NW0_CPART1', bGetPart         , '' , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(SSJR.TIME_SHEET_FATURA.moeda),' ')"                                                     , 'NW0_CMOEDA', bGetMoeda        , '' , Nil         })
	//TODO: Verificar com o Jacques se � necess�rio preencher os dados de caso m�e.
	// aAdd( aDePara, { "(SSJR.TIME_SHEET_FATURA.cod_cliente"                                                                , 'NW0_CCLICM', bGetSA1Cli       , '' , Nil         })
	// aAdd( aDePara, { "SSJR.TIME_SHEET_FATURA.cod_cliente||'|'||SSJR.TIME_SHEET_FATURA.cod_endereco"                       , 'NW0_CLOJCM', bGetSA1Loj       , '' , Nil         })
ElseIf cTab == 'NW0_PRE'
	aAdd( aDePara, { "NVL(LPAD(RCR.TIME_SHEET.NUMERO,"+cTamCodTs+",'0'), ' ')"      , 'NW0_CTS'   ,            , ''         , Nil         })
	aAdd( aDePara, { ""                                                             , 'NW0_SITUAC', "1"        , ''         , Nil         })
	aAdd( aDePara, { ""                                                             , 'NW0_CANC'  , "2"        , ''         , Nil         })
	aAdd( aDePara, { 'RCR.TIME_SHEET.COD_CLIENTE'                                   , 'NW0_CCLIEN', bGetSA1Cli , ''         , Nil         })
	aAdd( aDePara, { "RCR.TIME_SHEET.COD_CLIENTE||'|'||RCR.TIME_SHEET.PASTA_REAL"   , 'NW0_CLOJA' , bGetLojCas , ''         , Nil         })
	aAdd( aDePara, { 'RCR.TIME_SHEET.PASTA_REAL'                                    , 'NW0_CCASO' , bGetCaso   , ''         , Nil         })
	aAdd( aDePara, { "NVL(LPAD(RCR.TIME_SHEET.NUMEROPREFAT,8,'0'), ' ')"            , 'NW0_PRECNF', ''         , ''         , Nil         })
	aAdd( aDePara, { 'RCR.TIME_SHEET.MOEDA_TABH'                                    , 'NW0_CMOEDA', bGetMoeda  , ''         , Nil         })
	aAdd( aDePara, { 'RCR.TIME_SHEET.VALOR'                                         , 'NW0_VALORD', ''         , ''         , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(RCR.TIME_SHEET.DATA,'YYYYMMDD'),' ')"             , 'NW0_DATATS', ''         , ''         , Nil         })
	aAdd( aDePara, { 'RCR.TIME_SHEET.COD_ADVG'                                      , 'NW0_CPART1', bGetPart   , ''         , Nil         })
	aAdd( aDePara, { 'NVL(RCR.TIME_SHEET.INDICE_PREFATURA,0)'                       , 'NW0_COTAC' , ''         , ''         , Nil         })
	//TODO: Verificar com o Jacques se � necess�rio preencher os dados de caso m�e.
	//aAdd( aDePara, { "''''"+"||TRIM(RCR.TIME_SHEET.COD_CLIENTE)||'|'||TRIM(RCR.TIME_SHEET.PASTA_REAL)||"+"''''", 'NVZ_CCLICM', bGetCliCont                              , ''         , Nil         })
	//aAdd( aDePara, { "''''"+"||TRIM(RCR.TIME_SHEET.COD_CLIENTE)||'|'||TRIM(RCR.TIME_SHEET.PASTA_REAL)||"+"''''", 'NVZ_CLOJCM', bGetLojCont                              , ''         , Nil         })
	//aAdd( aDePara, { "''''"+"||TRIM(RCR.TIME_SHEET.COD_CLIENTE)||'|'||TRIM(RCR.TIME_SHEET.PASTA_REAL)||"+"''''", 'NVZ_CCASCM', bGetCasCont                              , ''         , Nil         })
ElseIf cTab == 'NXA' //Fatura    
    aAdd( aDePara, { "rcr.fatura.orgncodig"                                                                               , 'NXA_CESCR' ,bGetEscrt                     	                      , '' , Nil         })
    aAdd( aDePara, { "NVL(LPAD(rcr.fatura.numero,9,'0'), ' ')"                                                            , 'NXA_COD'   ,                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(TO_CHAR(rcr.fatura.data_emi	,'YYYYMMDD'),' ')"                                                , 'NXA_DTEMI' ,                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(TO_CHAR(rcr.fatura.data_venc,'YYYYMMDD'),' ')"                                                  , 'NXA_DTVENC',                     	                              , '' , Nil         })
    aAdd( aDePara, { "CASE WHEN rcr.fatura.situacao IN(' ','P','V') THEN '1' ELSE '2' END"                                , 'NXA_SITUAC',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(TO_CHAR(rcr.fatura.data_pagto,'YYYYMMDD'),' ')"                                                 , 'NXA_DTCANC',                     	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.fatura.moeda"                                                                                   , 'NXA_CMOEDA',bGetMoeda                     	                      , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.fatura.valor_fat_h,0)"                                                                      , 'NXA_VLFATH',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.fatura.valor_fat_d,0)"                                                                      , 'NXA_VLFATD',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.fatura.valor_pg_d_trib,0)"                                                                  , 'NXA_VLTRIB',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.fatura.grossup_desp_trib,0)"                                                                , 'NXA_VLGROS',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.fatura.txadm_desp_trib,0)"                                                                  , 'NXA_VLTXAD',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.fatura.valor_fat_d_trib,0)"                                                                 , 'NXA_VLTOTD',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.fatura.desconto,0)"                                                                         , 'NXA_VLDESC',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.fatura.irrf,0)"                                                                             , 'NXA_IRRF'  ,                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.fatura.pis,0)"                                                                              , 'NXA_PIS'   ,                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.fatura.cofins,0)"                                                                           , 'NXA_COFINS',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.fatura.csll,0)"                                                                             , 'NXA_CSLL'  ,                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.fatura.inss,0)"                                                                             , 'NXA_INSS'  ,                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.fatura.iss,0)"                                                                              , 'NXA_ISS'   ,                     	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.fatura.cod_cliente"                                                                             , 'NXA_CCLIEN',bGetSA1Cli                     	                  , '' , Nil         })
    aAdd( aDePara, { "rcr.fatura.cod_cliente||'|'||rcr.fatura.cod_endereco"                                               , 'NXA_CLOJA' ,bGetSA1Loj            	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.fatura.razao_social"                                                                            , 'NXA_RAZSOC',                     	                              , '' , Nil         })
  //aAdd( aDePara, { ""                                                                                                   , 'NXA_CCONTR',                     	                              , '' , Nil         })    
    aAdd( aDePara, { "CASE WHEN rcr.fatura.wo = 1 THEN '1' ELSE '0' END"                                                  , 'NXA_WO'    ,                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(TO_CHAR(rcr.fatura.datarefini_h,'YYYYMMDD'),' ')"                                               , 'NXA_DREFIH',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(TO_CHAR(rcr.fatura.datareffim_h,'YYYYMMDD'),' ')"                                               , 'NXA_DREFFH',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(TO_CHAR(rcr.fatura.datarefini_d,'YYYYMMDD'),' ')"                                               , 'NXA_DREFID',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(TO_CHAR(rcr.fatura.datareffim_d,'YYYYMMDD'),' ')"                                               , 'NXA_DREFFD',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(TO_CHAR(rcr.fatura.datarefini_h,'YYYYMMDD'),' ')"                                               , 'NXA_DREFIT',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(TO_CHAR(rcr.fatura.datareffim_h,'YYYYMMDD'),' ')"                                               , 'NXA_DREFFT',                     	                              , '' , Nil         })        
    aAdd( aDePara, { "CASE WHEN rcr.fatura.temhonorarios   = 'S' THEN '1' ELSE '2' END"                                   , 'NXA_TS'    ,                     	                              , '' , Nil         })
    aAdd( aDePara, { "CASE WHEN rcr.fatura.temdespesas     = 'S' THEN '1' ELSE '2' END"                                   , 'NXA_DES'   ,                     	                              , '' , Nil         })
    aAdd( aDePara, { "CASE WHEN rcr.fatura.temhonorarios   = 'S' THEN '1' ELSE '2' END"                                   , 'NXA_TAB'   ,                     	                              , '' , Nil         })    
    aAdd( aDePara, { "CASE WHEN rcr.fatura.fixo            = 'S' THEN '1' ELSE '2' END"                                   , 'NXA_FIXO'  ,                     	                              , '' , Nil         })
    aAdd( aDePara, { "CASE WHEN rcr.fatura.condicaoexito   = 0   THEN '2' ELSE '1' END"                                   , 'NXA_EXITO' ,                     	                              , '' , Nil         })
    aAdd( aDePara, { "CASE WHEN rcr.fatura.faturaadicional = 0   THEN '2' ELSE '1' END"                                   , 'NXA_FATADC',                     	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.fatura.socio_responsavel"                                                                       , 'NXA_CPART' ,bGetPart                     	                      , '' , Nil         })
    aAdd( aDePara, { "rcr.fatura.observacao"                                                                              , 'NXA_OBS'   ,                     	                              , '' , Nil         })    
    aAdd( aDePara, { "CASE WHEN rcr.fatura.tipopagador = 'F' THEN '2'"     +;
                     "	   WHEN rcr.fatura.tipopagador = 'J' THEN '1'"     +;
                     "	   WHEN rcr.fatura.tipopagador = 'E' THEN 'X' END"                                                , 'NXA_TPPAG' ,                     	                              , '' , Nil         })    
    aAdd( aDePara, { "replace(replace(replace(rcr.fatura.cgc_cpf,'.',''),'/',''),'-','')"                                 , 'NXA_CGCCPF',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.fatura.endereco,' ')"                                                                       , 'NXA_LOGRAD',                     	                              , '' , Nil         })
    aAdd( aDePara, { "replace(replace(rcr.fatura.cep,'-',''),'.','')"                                                     , 'NXA_CEP'   ,                     	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.fatura.bairro"                                                                                  , 'NXA_BAIRRO',                     	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.fatura.cidade"                                                                                  , 'NXA_CIDADE',                     	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.fatura.estado"                                                                                  , 'NXA_ESTADO',                     	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.fatura.pais"                                                                                    , 'NXA_PAIS'  ,                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.fatura.insc_estadual,' ')"                                                                  , 'NXA_INSEST',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.fatura.insc_municipal,' ')"                                                                 , 'NXA_INSMUN',                     	                              , '' , Nil         })
    aAdd( aDePara, { "CASE WHEN rcr.fatura.vlfixoh = 'S' THEN '1' ELSE '2' END"                                           , 'NXA_VLFIXH',                     	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.fatura.valor_fat_h_orig"                                                                        , 'NXA_VLORIH',                     	                              , '' , Nil         })
    aAdd( aDePara, { "CASE WHEN rcr.fatura.vlfixod = 'S' THEN '1' ELSE '2' END"                                           , 'NXA_VLFIXO',                     	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.fatura.valor_fat_d_orig"                                                                        , 'NXA_VLORID',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(LPAD(rcr.fatura.mtcncodigo,4,'0'), ' ')"                                                        , 'NXA_CMOTCA',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(DBMS_LOB.SUBSTR(rcr.fatura.texto_fatura, 4000, 1 ),' ')"                                        , 'NXA_TXTFAT',                     	                              , '' , Nil         })
    aAdd( aDePara, { "CASE WHEN rcr.fatura.calcularvldisp = 'S' THEN '1' ELSE '2' END"                                    , 'NXA_CALDIS',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.fatura.valoradiant,0)"                                                                      , 'NXA_VUADIA',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(valoradiantamento,0)"                                                                           , 'NXA_VSADIA',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.fatura.email,' ')"                                                                          , 'NXA_EMAIL' ,                     	                              , '' , Nil         }) 
  //aAdd( aDePara, { "NVL(rcr.fatura.email_cc,' ')"                                                                       , '          ',                     	                              , '' , Nil         })
    aAdd( aDePara, { "CASE WHEN rcr.fatura.imgenviadacarta     = 'S' THEN '1' ELSE '2' END"                               , 'NXA_CRTENV',                     	                              , '' , Nil         })
    aAdd( aDePara, { "CASE WHEN rcr.fatura.imgenviadarelatorio = 'S' THEN '1' ELSE '2' END"                               , 'NXA_RELENV',                     	                              , '' , Nil         })
    aAdd( aDePara, { "CASE WHEN rcr.fatura.imgenviadarecibo    = 'S' THEN '1' ELSE '2' END"                               , 'NXA_RECENV',                     	                              , '' , Nil         })
    aAdd( aDePara, { "CASE WHEN rcr.fatura.emailenviado        = 'S' THEN '1' ELSE '2' END"                               , 'NXA_MAILEN',                     	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.fatura.usuariocanc"                                                                             , 'NXA_USRCAN',bGetPart             	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(LPAD(rcr.fatura.numprefat,8,'0'), ' ')"                                                         , 'NXA_CPREFT',                     	                              , '' , Nil         })	
  //aAdd( aDePara, { "rcr.FATURA_SUBSTITUIDA.orgncodig_antiga"                                                            , 'NXA_CESCSU',bGetEscrt             	                              , '' , Nil         })
  //aAdd( aDePara, { "NVL(LPAD(rcr.FATURA_SUBSTITUIDA.fatura_antiga,8,'0'), ' ')"                                         , 'NXA_CFTSUB',                     	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.fatura.contato"                                                                                 , 'NXA_CCONT' ,bGetPart                     	                      , '' , Nil         })    
    aAdd( aDePara, { "CASE WHEN rcr.fatura.nfemitida = 'S'   THEN '1'"+;
                     "	   WHEN rcr.fatura.nfemitida = 'N'   THEN '2'"+;
                     "	   WHEN rcr.fatura.nfemitida IS NULL THEN '3'"+;
                     "	   WHEN rcr.fatura.nfemitida = ' '   THEN '3' END"                                                , 'NXA_NFGER' ,                     	                              , '' , Nil         })    
    aAdd( aDePara, { "rcr.fatura.nif_cliente"                                                                             , 'NXA_SERIE' ,                     	                              , '' , Nil         })
  //aAdd( aDePara, { "NVL(LPAD(rcr.FATURA_SUBSTITUIDA.fatura   ,9,'0'), ' ')"                                             , 'NXA_FATATU',                     	                              , '' , Nil         })
  //aAdd( aDePara, { "rcr.FATURA_SUBSTITUIDA.orgncodig"                                                                   , 'NXA_ESCATU',bGetEscrt             	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.fatura.idioma"                                                                                  , 'NXA_CIDIO' ,                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(LPAD(rcr.fatura.tipo_fat,1,'0'), ' ')"                                                          , 'NXA_CTPFAT',                     	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(LPAD(rcr.fatura.tipo_fatura,3,'0'), ' ')"                                                       , 'NXA_CTIPOF',                     	                              , '' , Nil         })    
    aAdd( aDePara, { "NVL(rcr.fatura.desc_adicional,0)"                                                                   , 'NXA_VLDSCE',                     	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.fatura.idioma_carta_cob"                                                                        , 'NXA_CIDIO2',                     	                              , '' , Nil         })	        
ElseIf cTab == 'NXD' //Participantes Da Fatura    
    aAdd( aDePara, { "NVL(LPAD(rcr.TOTADV.num_fat,09,'0'), ' ')"                                                          , 'NXD_CFATUR',                    	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTADV.orgncodig"                                                                               , 'NXD_CESCR' ,bGetEscrt             	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTADV.cod_cliente"                                                                             , 'NXD_CCLIEN',bGetSA1Cli          	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTADV.cod_cliente||'|'||RCR.PASTA.COD_ENDERECO"                                                                             , 'NXD_CLOJA' ,bGetSA1Loj           	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTADV.pasta"                                                                                   , 'NXD_CCASO ',bGetCaso              	                              , '' , Nil         })
    aAdd( aDePara, { "REPLACE(rcr.TOTADV.ano_mes,'-','')"                                                                 , 'NXD_ANOMES',                    	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTADV.cod_advg"                                                                                , 'NXD_CPART' ,bGetPart              	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTADV.tpdc"                                                                                    , 'NXD_UTCLI' ,                    	                              , '' , Nil         })
  //aAdd( aDePara, { ""                                                                                                   , 'NXD_HRCLI' ,                    	                              , '' , Nil         })
  //aAdd( aDePara, { ""                                                                                                   , 'NXD_HFCLI' ,                    	                              , '' , Nil         })
  //aAdd( aDePara, { ""                                                                                                   , 'NXD_HRLANC',                    	                              , '' , Nil         })
  //aAdd( aDePara, { ""                                                                                                   , 'NXD_HFLANC',                    	                              , '' , Nil         })    
    aAdd( aDePara, { "rcr.TOTADV.trspc"                                                                                   , 'NXD_UTLANC',                    	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTADV.tfspc"                                                                                   , 'NXD_UTREV' ,                    	                              , '' , Nil         })
  //aAdd( aDePara, { ""                                                                                                   , 'NXD_HRREV' ,                    	                              , '' , Nil         })
  //aAdd( aDePara, { ""                                                                                                   , 'NXD_HFREV' ,                    	                              , '' , Nil         }) 
    aAdd( aDePara, { "rcr.TOTADV.moeda_tabhonor"                                                                          , 'NXD_CMOEDT',bGetMoeda             	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTADV.valor_hora"                                                                              , 'NXD_VLHORA',                    	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTADV.valor"                                                                                   , 'NXD_VLADVG',                    	                              , '' , Nil         })
    aAdd( aDePara, { "NVL(LPAD(rcr.TOTADV.id_lanctab,06,'0'), ' ')"                                                       , 'NXD_CODTAB',                    	                              , '' , Nil         })    
    aAdd( aDePara, { "rcr.TOTADV.moeda"                                                                                   , 'NXD_CMOEDF',bGetMoeda            	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTADV.valor_corr"                                                                              , 'NXD_VLCORR',                    	                              , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTADV.desconto"                                                                                , 'NXD_DESCTO',                    	                              , '' , Nil         })
    aAdd( aDePara, { "0.00"                                                                                               , 'NXD_ACRESC',                    	                              , '' , Nil         })    
    aAdd( aDePara, { ""                                                                                                   , 'NXD_CCONTR',{|| ContratCaso() }  	                              , '' , Nil         })
    aAdd( aDePara, { ""                                                                                                   , 'NXD_CSEQ'  ,{|| CodSeq(2)}                                       , '' , Nil         })    
ElseIf cTab == 'NXE' //Resumo de despesas Da Fatura
    aAdd( aDePara, { "NVL(LPAD(rcr.TOTDESP.fatura,09,'0'), ' ')"                                                          , 'NXE_CFATUR',                                                     , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTDESP.orgncodigfat"                                                                           , 'NXE_CESCR' ,bGetEscrt                                            , '' , Nil         })
  //aAdd( aDePara, { "rcr.TOTDESP.orgncodigdesp"                                                                          , 'NXE_FILIAD',                                                     , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTDESP.cliente"                                                                                , 'NXE_CCLIEN',bGetSA1Cli                                           , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTDESP.cliente||'|'||RCR.PASTA.COD_ENDERECO"                                                   , 'NXE_CLOJA' ,bGetSA1Loj                                           , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTDESP.pasta"                                                                                  , 'NXE_CCASO' ,bGetCaso                                             , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTDESP.tipodesp"                                                                               , 'NXE_CTPDSP',bTipoDesp                                            , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTDESP.moeda"                                                                                  , 'NXE_CMOEDE',bGetMoeda                                            , '' , Nil         })
    aAdd( aDePara, { "SUM(rcr.TOTDESP.valor)"                                                                             , 'NXE_VLDESP',                                                     , '' , Nil         })
ElseIf cTab == 'NXF' //CAMBIOS UTILIZADOS NA FATURA
    aAdd( aDePara, { "NVL(LPAD(SSJR.FAT_CAMBIOFATURA.fatura,09,'0'), ' ')"                                                , 'NXF_CFATUR',                                                     , '' , Nil         })
	aAdd( aDePara, { ""                                                                                                   , 'NXF_COD'   ,{|| CriaVar("NXF_COD", .T.)}                         , '' , Nil         })    
    aAdd( aDePara, { "SSJR.FAT_CAMBIOFATURA.orgncodig"                                                                    , 'NXF_CESCR' ,bGetEscrt                                            , '' , Nil         })
    aAdd( aDePara, { "SSJR.FAT_CAMBIOFATURA.cod_conv"                                                                     , 'NXF_CMOEDA',bGetMoeda                                            , '' , Nil         })
    aAdd( aDePara, { "SSJR.FAT_CAMBIOFATURA.valor"                                                                        , 'NXF_COTAC1',                                                     , '' , Nil         })
ElseIf cTab == 'NUF' //WO       
    aAdd( aDePara, { "NVL(LPAD(rcr.WO.woncodigo,8,'0'),' ')"                                                              , 'NUF_COD'   ,                                                     , '' , Nil         })
    aAdd( aDePara, { "CASE WHEN rcr.WO.wocsituacao = 'A' THEN '1' ELSE '2' END"                                           , 'NUF_SITUAC',                                                     , '' , Nil         })    
    aAdd( aDePara, { "NVL(TO_CHAR(rcr.WO.wodemissao,'YYYYMMDD'),' ')"                                                     , 'NUF_DTEMI' ,                                                     , '' , Nil         })
    aAdd( aDePara, { "rcr.WO.wocusremissao"                                                                               , 'NUF_USREMI',bGetPart                                             , '' , Nil         })
    aAdd( aDePara, { "rcr.WO.wocobsemissao"                                                                               , 'NUF_OBSEMI',                                                     , '' , Nil         })
    aAdd( aDePara, { "NVL(TO_CHAR(rcr.WO.wodcancelamento,'YYYYMMDD'),' ')"                                                , 'NUF_DTCAN' ,                                                     , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.WO.wocusrcancelamento,' ')"                                                                 , 'NUF_USRCAN',bGetPart                                             , '' , Nil         })
    aAdd( aDePara, { "NVL(rcr.WO.wocobscancelamento,' ')"                                                                 , 'NUF_OBSCAN',                                                     , '' , Nil         })
    aAdd( aDePara, { "rcr.WO.wonorgncodig"                                                                                , 'NUF_CESCR' ,bGetEscrt                                            , '' , Nil         })
    aAdd( aDePara, { "NVL(LPAD(rcr.WO.wonfatura,9,'0'),' ')"                                                              , 'NUF_CFATU',                                                      , '' , Nil         })
ElseIf cTab == 'NUG' //WO Casos
    aAdd( aDePara, { "NVL(LPAD(rcr.WOPASTA.woncodigo,8,'0'),' ')"                                                         , 'NUG_CWO'   ,                                                     , '' , Nil         })
    aAdd( aDePara, { ""                                                                                                   , 'NUG_COD'   ,{|| GetSxEnum('NUG', 'NUG_COD')}                         , '' , Nil         })    
    aAdd( aDePara, { "rcr.WOPASTA.fwoncliente"                                                                            , 'NUG_CCLIEN',bGetSA1Cli                                           , '' , Nil         })
    aAdd( aDePara, { "rcr.WOPASTA.fwoncliente||'|'||RCR.PASTA.COD_ENDERECO"                                               , 'NUG_CLOJA' ,bGetSA1Loj                                           , '' , Nil         })
    aAdd( aDePara, { "rcr.WOPASTA.fwonpasta"                                                                              , 'NUG_CCASO' ,bGetCaso                                             , '' , Nil         })
    aAdd( aDePara, { "rcr.WOPASTA.fwocmoeda"                                                                              , 'NUG_CMOEDA',bGetMoeda                                            , '' , Nil         })
    aAdd( aDePara, { "rcr.WOPASTA.fwonvalord"                                                                             , 'NUG_VALOR' ,                                                     , '' , Nil         })    
ElseIf cTab == 'NWZ' //RESUMO WO DE DESPESAS
    aAdd( aDePara, { "NVL(LPAD(rcr.TOTDESPWO.wo,8,'0'),' ')"                                                              , 'NWZ_CODWO' ,                                                     , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTDESPWO.cliente"                                                                              , 'NWZ_CCLIEN',bGetSA1Cli                                           , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTDESPWO.cliente||'|'||RCR.PASTA.COD_ENDERECO"                                                 , 'NWZ_CLOJA' ,bGetSA1Loj                                           , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTDESPWO.pasta"                                                                                , 'NWZ_CCASO' ,bGetCaso                                             , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTDESPWO.tipodesp"                                                                             , 'NWZ_CTPDSP',bTipoDesp                                            , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTDESPWO.moeda"                                                                                , 'NWZ_CMOEDA',bGetMoeda                                            , '' , Nil         })
    aAdd( aDePara, { "rcr.TOTDESPWO.valor"                                                                                , 'NWZ_VALOR' ,                                                     , '' , Nil         })
ElseIf cTab == 'NVZ' //Despesa Faturamento
	aAdd( aDePara, { "LPAD(SSJR.DESPESA_FATURA.cod_despesa,"+cTamCodDsp+",'0')"                                           , 'NVZ_CDESP' ,                                                     , '' , Nil         })
	aAdd( aDePara, { "CASE WHEN SSJR.DESPESA_FATURA.situacao = 'N' THEN '1'    "+;
					"     WHEN SSJR.DESPESA_FATURA.situacao = 'F' THEN '2'    "+;
					"     WHEN SSJR.DESPESA_FATURA.situacao = 'W' THEN '3' END"                                           , 'NVZ_SITUAC',                                                     , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(SSJR.DESPESA_FATURA.fatura,9,'0'),' ')"                                                    , 'NVZ_CFATUR',                                                     , '' , Nil         })
	aAdd( aDePara, { "SSJR.DESPESA_FATURA.orgncodig"                                                                      , 'NVZ_CESCR' ,bGetEscrt                                            , '' , Nil         })
	aAdd( aDePara, { "NVL(LPAD(SSJR.DESPESA_FATURA.woncodigo,8,'0'),' ')"                                                 , 'NVZ_CWO'   ,                                                     , '' , Nil         })
	aAdd( aDePara, { "'2'"                                                                                                , 'NVZ_CANC'  ,                                                     , '' , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(SSJR.DESPESA_FATURA.data_atual,'YYYYMMDD'),' ')"                                        , 'NVZ_DTDESP',                                                     , '' , Nil         })
	aAdd( aDePara, { "SSJR.DESPESA_FATURA.cod_cliente"                                                                    , 'NVZ_CCLIEN',bGetSA1Cli                                           , '' , Nil         })
	aAdd( aDePara, { "SSJR.DESPESA_FATURA.cod_cliente||'|'||SSJR.DESPESA_FATURA.cod_endereco"                             , 'NVZ_CLOJA' ,bGetSA1Loj                                           , '' , Nil         })
	aAdd( aDePara, { "SSJR.DESPESA_FATURA.moeda"                                                                          , 'NVZ_CMOEDA',bGetMoeda                                            , '' , Nil         })
	aAdd( aDePara, { "SSJR.DESPESA_FATURA.valor"                                                                          , 'NVZ_VALORD',                                                     , '' , Nil         })
	aAdd( aDePara, { "SSJR.DESPESA_FATURA.cod_cliente"                                                                    , 'NVZ_CCLICM',bGetSA1Cli                                           , '' , Nil         })
	aAdd( aDePara, { "SSJR.DESPESA_FATURA.cod_cliente||'|'||SSJR.DESPESA_FATURA.cod_endereco"                             , 'NVZ_CLOJCM',bGetSA1Loj                                           , '' , Nil         })
	aAdd( aDePara, { 'RCR.DESPESA.PASTA_REAL'                                                                             , 'NVZ_CCASO' , bGetCaso                                 , ''         , Nil         })
ElseIf cTab == 'NVZ_PRE'
	aAdd( aDePara, { "LPAD(RCR.DESPESA.CODIGO,"+cTamCodDsp+",'0')"             , 'NVZ_CDESP' ,                                          , ''         , Nil         })
	aAdd( aDePara, { ""                                                        , 'NVZ_SITUAC', "1"                                      , ''         , Nil         })
	aAdd( aDePara, { ""                                                        , 'NVZ_CANC'  , "2"                                      , ''         , Nil         })
	aAdd( aDePara, { 'RCR.DESPESA.COD_CLIENTE'                                 , 'NVZ_CCLIEN', bGetSA1Cli                               , ''         , Nil         })
	aAdd( aDePara, { cAdtCliEnd                                                , 'NVZ_CLOJA' , bGetLojCas                               , ''         , Nil         })
	aAdd( aDePara, { 'RCR.DESPESA.PASTA_REAL'                                  , 'NVZ_CCASO' , bGetCaso                                 , ''         , Nil         })
	aAdd( aDePara, { "NVL(LPAD(RCR.DESPESA.NUMEROPREFAT,8,'0'), ' ')"          , 'NVZ_PRECNF', ''                                       , ''         , Nil         })
	aAdd( aDePara, { 'RCR.DESPESA.MOEDA'                                       , 'NVZ_CMOEDA', bGetMoeda                                , ''         , Nil         })
	aAdd( aDePara, { 'RCR.DESPESA.VALOR'                                       , 'NVZ_VALORD', ''                                       , ''         , Nil         })
	aAdd( aDePara, { "NVL(TO_CHAR(RCR.DESPESA.DATA,'YYYYMMDD'),' ')"           , 'NVZ_DTCONC', ''                                       , ''         , Nil         })
	aAdd( aDePara, { 'RCR.DESPESA.COD_ADVG'                                    , 'NVZ_CPART1', bGetPart                                 , ''         , Nil         })
	aAdd( aDePara, { 'NVL(RCR.DESPESA.INDICE,0)'                               , 'NVZ_COTAC' , ''                                       , ''         , Nil         })
	//TODO: Verificar com o Jacques se � necess�rio preencher os dados de caso m�e.
	//aAdd( aDePara, { "''''"+"||TRIM(RCR.DESPESA.COD_CLIENTE)||'|'||TRIM(RCR.DESPESA.PASTA_REAL)||"+"''''", 'NVZ_CCLICM', bGetCliCont                              , ''         , Nil         })
	//aAdd( aDePara, { "''''"+"||TRIM(RCR.DESPESA.COD_CLIENTE)||'|'||TRIM(RCR.DESPESA.PASTA_REAL)||"+"''''", 'NVZ_CLOJCM', bGetLojCont                              , ''         , Nil         })
	//aAdd( aDePara, { "''''"+"||TRIM(RCR.DESPESA.COD_CLIENTE)||'|'||TRIM(RCR.DESPESA.PASTA_REAL)||"+"''''", 'NVZ_CCASCM', bGetCasCont                              , ''         , Nil         })
ElseIf cTab == 'OHH' //Posi��o Hist�rica Ctas Receber
	aAdd( aDePara, { "NVL(LPAD(finance.CONTASRECEBERHISTORICO.crccnumerodocumento,09,'0'), ' ')", 'OHH_NUM'    , ''        , '', Nil })
	aAdd( aDePara, { cParcela                                                                   , 'OHH_PARCEL' , ''        , '', Nil })
	aAdd( aDePara, { "NVL(TO_CHAR(finance.CONTASRECEBERHISTORICO.crcddata,'YYYYMMDD'),' ')"     , 'OHH_DTHIST' , ''        , '', Nil })
	aAdd( aDePara, { "finance.CONTASRECEBERHISTORICO.crhcanomes"                                , 'OHH_ANOMES' , bAnoMes   , '', Nil })
	aAdd( aDePara, { "finance.CONTASRECEBERHISTORICO.crcchistorico"                             , 'OHH_HIST'   , ''        , '', Nil })
	aAdd( aDePara, { "finance.CONTASRECEBERHISTORICO.codigo"                                    , 'OHH_CMOEDA' , bGetNMoeda, '', Nil })
	aAdd( aDePara, { "finance.CONTASRECEBERHISTORICO.crcncliente"                               , 'OHH_CCLIEN' , bGetSA1Cli, '', Nil })
	aAdd( aDePara, { "finance.CONTASRECEBERHISTORICO.crcncliente||'|'||RCR.FATURA.COD_ENDERECO" , 'OHH_CLOJA'  , bGetSA1Loj, '', Nil })
	aAdd( aDePara, { "finance.CONTASRECEBERHISTORICO.crcnvalor"                                 , 'OHH_VALOR'  , ''        , '', Nil })
	aAdd( aDePara, { "finance.CONTASRECEBERHISTORICO.crcnvalorhonorario"                        , 'OHH_VLFATH' , ''        , '', Nil })
	aAdd( aDePara, { "finance.CONTASRECEBERHISTORICO.crcnvalordespesa"                          , 'OHH_VLFATD' , ''        , '', Nil })
	aAdd( aDePara, { "finance.CONTASRECEBERHISTORICO.crcnvalorir"                               , 'OHH_VLIRRF' , ''        , '', Nil })
	aAdd( aDePara, { "NVL(TO_CHAR(finance.CONTASRECEBERHISTORICO.crcdvecto,'YYYYMMDD'),' ')"    , 'OHH_VENCRE' , ''        , '', Nil })
	aAdd( aDePara, { "finance.CONTASRECEBERHISTORICO.crcnvalorpis"                              , 'OHH_VLPIS'  , ''        , '', Nil })
	aAdd( aDePara, { "finance.CONTASRECEBERHISTORICO.crcnvalorcofins"                           , 'OHH_VLCOFI' , ''        , '', Nil })
	aAdd( aDePara, { "finance.CONTASRECEBERHISTORICO.crcnvalorcsll"                             , 'OHH_VLCSLL' , ''        , '', Nil })
	aAdd( aDePara, { "finance.CONTASRECEBERHISTORICO.crcnvaloriss"                              , 'OHH_VLISS'  , ''        , '', Nil })
	aAdd( aDePara, { "finance.CONTASRECEBERHISTORICO.crcnvalorinss"                             , 'OHH_VLINSS' , ''        , '', Nil })

ElseIf cTab == 'OHB' // Lancamentos

	//               Sisjuri                                                                   , Protheus    , Force                                                                                                                                                                , Ajusta Tipo, Executa fun��o
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.EMPNCOD'                                              , 'OHB_FILIAL', {|| StrZero(xValSisJur, nTamFilOHB)}                                                                                                                                 , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.LANNCODIG'                                            , 'OHB_CODIGO', &('{|| StrZero(xValSisJur,' + cTamOHBCod +')}')                                                                                              , 'Str'      , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.LANDDATAINCLUSAO'                                     , 'OHB_DTINCL', ''                                                                                                                                                                   , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.EVENCODIGO'                                           , 'OHB_CEVENT', &('{|| IIf(Empty(xValSisJur), "", StrZero(xValSisJur,' + cTamEvento + '))}')                                                                , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.LANCSOLICITANTE'                                      , 'OHB_CPART' , bGetPart                                                                                                                                                             , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.LANCORIGEM'                                           , 'OHB_ORIGEM', {||IIF(xValSisJur = "D","5",IIF(xValSisJur = "M","3" , IIF(xValSisJur = "F","4" ,IIF(xValSisJur = "R","2",IIF(xValSisJur = "P","1","")) ))) }                        ,            , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.PCTCNUMEROCONTAORG'                                   , 'OHB_NATORI', {|| StrTran(xValSisJur,'.','')}                                                                                                                                      , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.ESCRITORIOORG'                                        , 'OHB_CESCRO', bGetEscrt                                                                                                                                                            , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.SIGLAORG||FINANCE.LANCAMENTO.ESCRITORIOORG'           , 'OHB_CCUSTO', ""                                                                                                                                                                   , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.LANCPROFORG'          	                               , 'OHB_CPARTO', bGetPart                                                                                                                                                             , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.RATNCODIGORG'         	                               , 'OHB_CTRATO', bGetTabRat                                                                                                                                                           , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.PCTCNUMEROCONTADEST'                                  , 'OHB_NATDES', {|| StrTran(xValSisJur,'.','')}                                                                                                                                      , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.ESCRITORIODEST'     	                               , 'OHB_CESCRD', bGetEscrt                                                                                                                                                            , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.SIGLADEST||FINANCE.LANCAMENTO.ESCRITORIODEST'         , 'OHB_CCUSTD', {|| StrTran(xValSisJur, ' ', '')}                                                                                                                                    , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.LANCPROFDEST'         	                               , 'OHB_CPARTD', bGetPart                                                                                                                                                             , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.RATNCODIGDEST'                                        , 'OHB_CTRATD', bGetTabRat                                                                                                                                                           , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.ID_PROJETO'       	                                   , 'OHB_CPROJD', &('{|| IIf(Empty(xValSisJur), "", StrZero(xValSisJur,' + cTamProjet + '))}')                                                                , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.ID_PROJETO_ITEM'                                      , 'OHB_CITPRD', &('{|| IIf(Empty(xValSisJur), "", StrZero(xValSisJur,' + cTamItemPr + '))}')                                                                , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.LANNCLIENTEDEST'      	                               , 'OHB_CCLID' , bGetSA1Cli                                                                                                                                                           , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.LANNCLIENTEDEST'                                      , 'OHB_CLOJD' , {|| IIf(Empty(xValSisJur), '', "00")}	                                                                                                                            , ''         , Nil })
	aAdd( aDePara, { "FINANCE.LANCAMENTO.LANNCLIENTEDEST||'|'||FINANCE.LANCAMENTO.LANNCASODEST", 'OHB_CCASOD', &('{|| Substr(GetDePara("RCR.PASTA", xValSisJur, ""),' + cValToChar(Val(cTamClient)+Val(cTamLoja)+1)+"," + cTamCaso + ')}'), ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.LANNDESPESADEST'                                      , 'OHB_CTPDPD', &('{|| IIf(Empty(xValSisJur), "", StrZero(xValSisJur,' + cTamTpDesp + '))}')                                                                , ''         , Nil })
	aAdd( aDePara, { 'RCR.DESPESA.CODIGO'                                                      , 'OHB_CDESPD', &('{|| IIf(Empty(xValSisJur), "", StrZero(xValSisJur,' + cTamCodDsp + '))}')                                                                , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.QUANTIDADEDEST'                                       , 'OHB_QTDDSD', ""                                                                                                                                                                   , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.COBRARDEST'                                           , 'OHB_COBRAD', {|| IIF(Upper(xValSisJur)="S", "1","2")}                                                                                                                             , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.LANDDATADESP'     	                                   , 'OHB_DTDESP', ""                                                                                                                                                                   , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.LANDDATA'                                             , 'OHB_DTLANC', ""                                                                                                                                                                   , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.CODIGO'                                               , 'OHB_CMOELC', bGetMoeda                                                                                                                                                            , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.LANNVALOR'                                            , 'OHB_VALOR' ,""                                                                                                                                                                    , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.LANNCODIG'                                            , 'OHB_CMOEC' , {|| GetOHBMOEC()}                                                                                                                                                            , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.LANNCOTACAO'                                          , 'OHB_COTAC' , {|| IIF(xValSisJur <> 0, xValSisJur,1)}                                                                                                                                                                   , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.CODIGO'                                               , 'OHB_VLNAC' , bOHBValNac                                                                                                                                                           , ''         , Nil })
	aAdd( aDePara, { 'NVL(FINANCE.LANCAMENTO.LANNCOTACAO, 1) * FINANCE.LANCAMENTO.LANNVALOR'   , 'OHB_VALORC', ""	                                                                                                                                                                , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.HPANCODIGO'                                           , 'OHB_CHISTP', &('{|| Substr(GetDePara("FINANCE.HISTORICOPADRAO", xValSisJur, ""), 1,' + cTamHistPa + ')}')                                                , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.LANCHISTORICO'                                        , 'OHB_HISTOR', ""                                                                                                                                                                   , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.LANCUSUARIO'                                          , 'OHB_CUSINC', bGetPart                                                                                                                                                             , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.LANDDATAMODIFICACAO'                                  , 'OHB_DTALTE', ""                                                                                                                                                                   , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.LANCDUTELEFONEMA'                                     , 'OHB_DURTEL', ""                                                                                                                                                                   , ''         , Nil })
	aAdd( aDePara, { ''                                                                        , 'OHB_FILORI', cFilAnt                                                                                                                                                              , ''         , Nil })
	// JAX Aguardar a elabora��o do fonte de Contas a Pagar para saber como ficar� a numera��o
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.CPGCNUMEROPAGAR'                                      , 'OHB_CPAGTO', ""                                                                                                                                                                   , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.DESNITEM'                                             , 'OHB_ITDES' , &('{|| IIf(Empty(xValSisJur), "", StrZero(xValSisJur,' + cTamDesdob + '))}')                                                                 , ''         , Nil })
	aAdd( aDePara, { 'FINANCE.LANCAMENTO.DESNITEMPAGO'                                         , 'OHB_ITDPGT', &('{|| IIf(Empty(xValSisJur), "", StrZero(xValSisJur,' + cTamDesdPg + '))}')                                                                , ''         , Nil })

EndIf

Return aDePara

//-------------------------------------------------------------------
/*/{Protheus.doc} QryOrigem
Cria query na origem da Migra��o.

@param aEntidade   Informa��es da entidade para gerar query
@param aDePara     Array com as op��es Sisjuri e Protheus

@author Cristina Cintra
@since 21/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function QryOrigem(aEntidade, aCampos)
Local nI        := 0
Local cCampos   := ""
Local cQuery    := ""
Local cCpo      := ""
Local cJoinCond := ""
Local nTab      := 0
Local nCpo      := 0
Local aCpoJoin  := {}
Local cAlsCpo   := "AAAAA"
Local cTab      := Iif( Len(aEntidade) >= 1, aEntidade[1], "") // Nome da tabela na origem
Local cCondicao := Iif( Len(aEntidade) >= 3, aEntidade[3], "") // Condi��o para uso no Where da query, quando necess�rio
Local aJoin     := Iif( Len(aEntidade) >= 4, aEntidade[4], {}) // Array com as informa��es para Join
Local cOrder    := Iif( Len(aEntidade) >= 5, aEntidade[5], "") // Campos para uso no Order By da query
Local lDistinct := Iif( Len(aEntidade) >= 6, aEntidade[6], .F.) // Condi��o para o uso do Distinct na query, quando necess�rio
Local cGroupBy  := Iif( Len(aEntidade) >= 7, aEntidade[7], "")  // Campos para uso no Group By da query

For nI := 1 To Len(aCampos)
	cCpo := Alltrim(aCampos[nI][1])
	If !Empty(cCpo)
		cCampos += Iif(nI > 1, ", ", "") + cCpo + " " + cAlsCpo + " "
	Else
		cCampos += Iif(nI > 1, ", ", "") + Alltrim(Str(nI)) + " " + cAlsCpo + " "
	EndIf
	cAlsCpo := Soma1(cAlsCpo)
Next nI

cQuery := " SELECT " + Iif(lDistinct, " DISTINCT " , "") + cCampos + " "
cQuery +=   " FROM " + cTab + " "
If !Empty(aJoin)
	For nTab := 1 To Len(aJoin)
		cQuery += " " + aJoin[nTab][1] + " JOIN " + aJoin[nTab][2] + " "

		For nCpo := 1 To Len(aJoin[nTab][3])
			aCpoJoin  := aJoin[nTab][3][nCpo]
			cJoinCond := Iif(nCpo == 1, "ON", "AND")
			cQuery +=   " " + cJoinCond + " " + aCpoJoin[1] + " = " + aCpoJoin[2] + " "

		Next nCpo
	Next nTab
EndIf
If !Empty(cCondicao)
	cQuery += " WHERE " + cCondicao
EndIf
If !Empty(cGroupBy)
	cQuery += " GROUP BY " + cGroupBy
EndIf
If !Empty(cOrder)
	cQuery += " ORDER BY " + cOrder
EndIf

Return cQuery

//-------------------------------------------------------------------
/*/{Protheus.doc} GrvDestino
Processa os registros da origem e grava na tabela de destino.

@param aDePara       Array com os dados de de-para
	   aDePara[1][1] campos do SISJURI
	   aDePara[1][2] campos do Protheus
	   aDePara[1][3] for�a a valor no campo
	   aDePara[1][4] converte o tipo do campo (SISIJUR x Protheus)
	   aDePara[1][5] for�a execu��o de uma fun��o
@param oProcess  Objeto do Processa
@param aFilxEscr Array com o Escrit�rio do Susjuri x Filial do protheus
                 usado no bloco de c�digo da migra��o da tabela NS7
@param cAlias    Alias tempor�rio da query executada no SISJURI
@param nTotReg   Total de registros executados na query do banco de dados SISJURI

@author Cristina Cintra
@since 21/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function GrvDestino(aDePara, oProcess, aFilxEscr, cAlias, nTotReg)
Local nI         := 0
Local nX         := 0
Local xVal       := Nil
Local xForce     := Nil
Local xValSisJur := Nil
Local cValTpForc := ""
Local cCpoSisJur := ""
Local cTabProthe := ""
Local cTabOld    := ""
Local cCpoProthe := ""
Local cTabNext   := ""
Local cCpoNext   := ""
Local cAjsTipo   := ""
Local lCommit    := .F.
Local lReclock   := .T.
Local nTotDePara := Len(aDePara)
Local bExecutFun := {||}
Local cTotal     := ""
Local aCposPrt	 := Array(Len(aDePara))
Local nC		 := 0
Local aStru      := {}
Local aCamposRet := {}
Local aRegs      := {}
Local aReg		:= {}
Local nCampos	:= 0

//Primeiro processamento
//Alimenta o array de posi��o dos campos para ser executado somente uma vez

For nC := 1 to Len(aDePara)

	
	If !Empty(aDePara[nC, 02])
		cCpoProthe := aDePara[nC, 02]
		// Pega a tabela atual
		If !Empty(cCpoProthe)
			cTabProthe := SubStr(cCpoProthe, 1, At("_", cCpoProthe) - 1)
			If Len(cTabProthe) == 2
				cTabProthe := "S" + cTabProthe
			EndIf
		EndIf
		aCposPrt[nC] := {cTabProthe, (cTabProthe)->(FieldPos(aDePara[nC, 02]))}
	EndIf

Next nC

//Pega a estrutura da tabela destino
If (cAlias)->(!Eof())
	nCampos := Len((cAlias)->(dbStruct()))
EndIf

cTotal     := cValToChar(nTotReg)
cCpoProthe := ""
cTabProthe := ""
nTotReg    := 0
aRegs := {{}}
aReg := Array(nCampos)
Do While (cAlias)->(!Eof())
	nTotReg++
	oProcess:IncRegua2("Importando registro " + Str(nTotReg) + " de " + cTotal)

	nI := 1
	For nC := 1 to nCampos
		aReg[nC] :=   (cAlias)->(FieldGet(nC))
	Next nC
	lReclock := .T.

	aRegs[nI] :=  aReg
	ClearBuffReg() 
	//Limpa o buffer do registro processado
	For nX := 1 To Len(aRegs[nI])

		cCpoSisJur := aDePara[nX][1]
		cCpoProthe := aDePara[nX][2]
		cCpoNext   := Iif(nTotDePara > nX, aDePara[nX+1][2], "") // Pr�ximo campo
		xForce     := aDePara[nX][3]
		cAjsTipo   := aDePara[nX][4]
		bExecutFun := Iif(Len(aDePara[nX]) >= 5, aDePara[nX][5], Nil)
		xValSisJur := aRegs[nI][nX]

		If aCposPrt[nX] <> Nil
			cTabProthe := aCposPrt[nX, 01]

		EndIf

		If lReclock .And. !Empty(cTabProthe)
			Reclock(cTabProthe, .T.)
			lReclock := .F.
		EndIf

		// Pega a pr�xima tabela
		If !Empty(cCpoNext)
			cTabNext := SubStr(cCpoNext, 1, At("_", cCpoNext) - 1)
			If Len(cTabNext) == 2
				cTabNext := "S" + cTabNext
			EndIf

			lCommit  := cTabProthe != cTabNext
			lReclock := lCommit

		ElseIf nTotDePara == nX
			lCommit  := .T.
			lReclock := .T.
		EndIf

		If Empty(cCpoSisJur) .Or. !Empty(xForce) // Force
			cValTpForc := ValType(xForce)

			If cValTpForc == "A"
				xVal := AjuLista(xValSisJur, xForce)

			ElseIf cValTpForc == "B"
				xVal := Eval(xForce)

			ElseIf cValTpForc $ "C|N"
				xVal := xForce
			EndIf

		ElseIf !Empty(cAjsTipo) // Ajuste Tipo
			xVal := AjuTipo(xValSisJur, cAjsTipo)

		Else
			xVal := xValSisJur
		EndIf

		// Ajusta no array com o valor que vai ser gravado de fato
		aRegs[nI][nX] := xVal
		xValSisJur    := xVal

		If aCposPrt[nX] <> Nil .And. !Empty(aCposPrt[nX, 02])
			(cTabProthe)->(FieldPut(aCposPrt[nX, 02], xVal))
		EndIf
		
		If !Empty(bExecutFun) // Executa fun��o
			Eval(bExecutFun)
		EndIf
		
		If lCommit
			(cTabProthe)->(DbCommit())
			(cTabProthe)->(MsUnlock())
			lCommit := .F.
		EndIf
		cTabOld := cTabProthe
	Next nX
	(cAlias)->(dbSkip())
EndDo
JurFreeArr(aReg)
JurFreeArr(aRegs)
JurFreeArr(aCposPrt)

Return Nil
//-------------------------------------------------------------------
/*/{Protheus.doc} AjuLista
Ajusta o valor a ser gravado quando as op��es de lista forem diferentes
entre Sisjuri e Protheus.

@param xVal     Valor encontrado na origem
@param aForce   Array com as op��es Sisjuri e Protheus

@return xRet    Retorno ap�s ajuste para a op��o do Protheus

@author Cristina Cintra / Jorge Martins
@since 21/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function AjuLista(xVal, aForce)
Local xRet := ""
Local nPos := 0

nPos := aScan( aForce, { |x| x[1] == Iif(ValType(xVal) == "C", Alltrim(xVal), xVal) } )

If nPos > 0
	xRet := aForce[nPos][2]
EndIf

Return xRet

//-------------------------------------------------------------------
/*/{Protheus.doc} AjuTipo
Ajusta o tipo do valor a ser gravado quando as op��es de lista forem 
diferentes entre Sisjuri e Protheus.

@param xVal     Valor encontrado na origem
@param cAjuste  Fun��o de transforma��o a ser aplicada

@return xRet    Retorno ap�s ajuste do tipo

@author Cristina Cintra
@since 21/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function AjuTipo(xVal, cAjuste)
Local xRet := xVal

If cAjuste == "Str"
	xRet := AllTrim(Str(xVal))
ElseIf cAjuste == "Val"
	xRet := Val(xVal)
EndIf

Return xRet

//-------------------------------------------------------------------
/*/{Protheus.doc} LimpaTab
Limpa os dados da tabela de Destino (Protheus) antes de efetivar a 
migra��o.

@param cTab     Nome da tabela na origem
@param aTabsDel Array para passar como referencia, para armazenar as
                tabelas deletadas

@author Cristina Cintra
@since 21/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function LimpaTab(cTab, aTabsDel)
Local cTabSql   := RetSqlName(cTab)
Local cQuery    := ""
Local aTabProth := StrTokArr(cTab, "|")
Local nI        := 1

For nI := 1 To Len(aTabProth)
	cTabSql := RetSqlName(aTabProth[nI])

	If aScan(aTabsDel, {|cTabDel| cTabDel == cTabSql}) == 0
		cQuery := " TRUNCATE TABLE " + cTabSql + " "
		aAdd(aTabsDel, cTabSql)
	EndIf

	TCSqlExec( cQuery )
Next nI

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} BuscaCodAM
Busca o c�digo sequencial da tabela m�e (NTV).

@param cTab  Nome da tabela na origem

@author Cristina Cintra / Queizy Nascimento
@since 30/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function BuscaCodAM (cTab, cCampo, cTabServ, cAnoMes)
Local cQuery := ""
Local cRet   := ""
Local aSQL   := {}

cQuery := " SELECT " + cCampo + " RESU "
cQuery +=   " FROM " + RetSqlName(cTab)
cQuery += " WHERE " + cTab + "_AMINI = '" + cAnoMes + "' AND " + cTab + "_CTAB = '" + cTabServ + "'"

aSQL := JurSQL(cQuery, "RESU")

If !Empty(aSQL)
	cRet := aSQL[1][1]
EndIf

Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} AjuHist
Ajusta o tipo do valor a ser gravado quando as op��es de lista forem 
diferentes entre Sisjuri e Protheus.

@param oProcess  , Objeto do processa
@param aAjuste   , Array com as tabelas para ajuste do Ano-m�s Final

@return Nil

@author Cristina Cintra / Queizy Nascimento
@since 30/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function AjuHist(oProcess, aAjustes)
Local aDados     := {}
Local nRecno     := 0
Local nI         := 0
Local cAmIni     := ""
Local cTabPrinc  := "" // Tabela Principal / M�e
Local cTabSecun  := "" // Tabela Secund�ria / Hist�rico
Local cBusPrinc  := "" // Campos para busca na tabela principal
Local cBusSecun  := "" // Campos para busca na tabela secund�ria
Local cFilPrinc	 := "" //Filial da Tabela Principal
Local cFilSecun := "" //Filial da Tabela Secund�ria


oProcess:SetRegua2(Len(aAjustes))

For nI := 1 To Len(aAjustes)
	oProcess:IncRegua2(i18n("Importando registro #1 de #2", {nI, Len(aAjustes)}))

	cTabPrinc := aAjustes[nI][1]
	cFilPrinc := xFilial(cTabPrinc)
	cTabSecun := aAjustes[nI][2]
	cFilSecun := xFilial(cTabSecun)
	cBusPrinc := aAjustes[nI][3]
	cBusSecun := aAjustes[nI][4]
	
	DbSelectArea(cTabPrinc)
	(cTabPrinc)->(DbGoTop())
	(cTabPrinc)->(dbSetOrder(1))

	DbSelectArea(cTabSecun)
	(cTabSecun)->(DbGoTop())
	(cTabSecun)->(dbSetOrder(1))
	
	While !(cTabPrinc)->(EOF())
		
		aDados := {}
		
		cCodPrinc := (cTabPrinc)->&(cBusPrinc)
		If (cTabSecun)->(dbSeek(cFilSecun + cCodPrinc))
			While !(cTabSecun)->(EOF()) .And. (cTabSecun)->&(cBusSecun) == cFilPrinc+ cCodPrinc
				nRecno := (cTabSecun)->(Recno())
				cAmIni := (cTabSecun)->&((cTabSecun) + "_AMINI")
				aAdd(aDados, {nRecno, cAmIni})
				(cTabSecun)->(dbSkip())
			EndDo
			
			GrvHist(aSort(aDados,,, { |aX, aY| aX[2] > aY[2] } ), cTabSecun)
		EndIf
		
		(cTabPrinc)->( dbSkip() )
	EndDo

Next nI

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} GrvHist
Grava ajuste de Ano-m�s Inicial e Final de Hist�rico.

@param aDados  Array com os dados para ajuste.
@param cTab    Tabela a ser ajustada.

@return Nil

@author Cristina Cintra / Queizy Nascimento
@since 30/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function GrvHist(aDados, cTab)
Local nI      := 0
Local cAmIni  := ""
Local cAmFim  := ""
Local cAmIniP := ""
Local nQtd    := Len(aDados)

For nI := 1 To nQtd
	
	If nI == 1 
		cAmFim := Space(6) // Registro mais recente fica com Ano-m�s Final em aberto
	Else
		cAmIni := aDados[nI - 1][2]
		cMes   := Right(cAmIni, 2)
		cAno   := Left(cAmIni, 4)
		If cMes == "01"
			cMes := "12"
			cAno := Alltrim(Str(Val(cAno) - 1))
		Else
			cMes := StrZero(Val(cMes) - 1, 2) 
		EndIf
		cAmFim := cAno + cMes
	EndIf
	If nI == nQtd
		cAmIniP := "190001" // Registro mais antigo fica com Ano-m�s Inicial 1900-01
	EndIf
	
	(cTab)->(DbGoTo(aDados[nI][1]))
	RecLock((cTab), .F.)
	(cTab)->&(cTab + "_AMFIM") := cAmFim
	If !Empty(cAmIniP)
		(cTab)->&(cTab + "_AMINI") := cAmIniP
	EndIf
	(cTab)->(MsUnlock())
	(cTab)->(DbCommit())

Next nI

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} GuardaInfo
Recebe array e guarda numa vari�vel Private para consulta na tabela 
filha.

@param aRegs  Array com as informa��es para guardar na Private

@return Nil

@author Cristina Cintra / Queizy Nascimento
@since 31/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function GuardaInfo(aRegs)

aAdd(aGuardaInfo, aRegs)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} BuscaInfo
Busca o registro de refer�ncia para pegar o c�digo sequencial do Relation.

@param cBusca1   Chave para busca 1
@param cBusca2   Chave para busca 2
@param nPosResul Posi��o do resultado esperado - C�d Sequencial

@return cCodigo  C�digo Sequencial da tabela principal / m�e

@author Cristina Cintra / Queizy Nascimento
@since 31/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function BuscaInfo(cBusca1, cBusca2, nPosResul)
Local cCodigo := ""
Local nPos    := 0

nPos := aScan( aGuardaInfo, { |x| x[1] == cBusca1 .And. x[2] == cBusca2 } )

If nPos > 0
	cCodigo := aGuardaInfo[nPos][nPosResul]
EndIf

Return cCodigo

//-------------------------------------------------------------------
/*/{Protheus.doc} LimpaArray
Limpa o Array aGuardaInfo - vari�vel private.

@return Nil

@author Cristina Cintra / Queizy Nascimento
@since 31/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function LimpaArray()

aGuardaInfo := {}

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} ValTab
Busca o valor de honor�rios atual da tabela de servi�os

@return nValor Valor de honor�rios

@author Cristina Cintra / Abner Foga�a
@since 05/06/2019
/*/
//-------------------------------------------------------------------
Static Function ValTab(cCodTab, cCodServ)
Local nValor := 0

nValor := JurGetDados("NS5", 1, xFilial("NS5") + cCodTab + cCodServ, "NS5_VALORH")

Return nValor

//-------------------------------------------------------------------
/*/{Protheus.doc} InsereHist
Percorre as tabelas de servi�os e verifica se existe algum servi�o n�o cadastrado no hist�rico, 
caso n�o tenha a rotina insere.

@param  oProcess, Objeto do processa

@return Nil

@author Cristina Cintra / Abner Foga�a
@since 05/06/2019
/*/
//-------------------------------------------------------------------
Static Function InsereHist(oProcess)
Local nTotReg := NRE->(LastRec())
Local nReg    := 0
Local cFilNRE	:= xFilial("NRE")
Local cFilNTS	:= xFilial("NTS")
Local cFilNU1	:= xFilial("NU1")
Local cFilNS5	:= xFilial("NS5")

DbSelectArea("NRE")
NRE->(DbGoTop())
NRE->(DbSetOrder(1)) // NRE_FILIAL+NRE_COD

DbSelectArea("NU1")
NU1->(DbGoTop())
NU1->(DbSetOrder(1)) // NU1_FILIAL+NU1_CTAB+NU1_COD

DbSelectArea("NS5")
NS5->(DbGoTop())
NS5->(DbSetOrder(2)) // NS5_FILIAL+NS5_CTAB

DbSelectArea("NTS")
NTS->(DbGoTop())
NTS->(DbSetOrder(3)) // NTS_FILIAL+NTS_CHIST+NTS_CSERV

oProcess:SetRegua2(nTotReg)

While !NRE->(EOF())
	nReg += 1
	oProcess:IncRegua2(i18n("Importando registro #1 de #2",{nReg, nTotReg}))
	While !NU1->(EOF()) .And. (cFilNRE + NRE->NRE_COD == cFilNU1 + NU1->NU1_CTAB)
		NS5->(DbGoTop())
		While !NS5->(EOF()) .And. (cFilNS5 + NS5->NS5_CTAB == cFilNU1 + NU1->NU1_CTAB)
			If !(NTS->(DbSeek(cFilNTS + NU1->NU1_COD + NS5->NS5_CSERV)))
				RecLock("NTS", .T.)

				NTS->NTS_FILIAL   := cFilNTS
				NTS->NTS_CTAB     := NU1->NU1_CTAB
				NTS->NTS_CHIST    := NU1->NU1_COD
				NTS->NTS_COD      := GetSXENum("NTS", "NTS_COD")
				NTS->NTS_CSERV    := NS5->NS5_CSERV
				NTS->NTS_VALORH   := NS5->NS5_VALORH
				NTS->NTS_MAXSER   := NS5->NS5_MAXSER
				NTS->NTS_ADISER   := NS5->NS5_ADISER
				NTS->NTS_MOEDAT   := NU1->NU1_CMOEDA
				NTS->NTS_VALORT   := NS5->NS5_VALORT
				NTS->NTS_MAXTAX   := NS5->NS5_MAXTAX
				NTS->NTS_ADITAX   := NS5->NS5_ADITAX

				NTS->(MsUnlock())
				NTS->(DbCommit())

			EndIf
			NS5->(DbSkip())
		EndDo
		NU1->(DbSkip())
	EndDo
	NRE->(DbSkip())
EndDo

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} EscritXFil
Retorna a empresa ou filial do protheus com o de-para do escrit�rio
x filial realizado pelo usu�rio no wizard

@param cSiglaEscr  Sigla do escrit�rio
@param aFilxEscr   Array com o de-para de escrit�rio e Filial
                   aFilxEscr[nI][1] Sigla do escrit�rio do SisJuri
                   aFilxEscr[nI][2] Arrau com dados de filial do Protheus
                   aFilxEscr[nI][2][1] Grupo de empresa do Protheus
                   aFilxEscr[nI][2][2] Filial do Protheus
@param cCampo      Campo para retorno: NS7_CEMP   = Grupo de Empresa
                                       NS7_CFILIA = Filial

@return cVal  Empresa ou filial do protheus que representa o c�digo 
              do Sisjuri "cSiglaEscr" 

@author Bruno Ritter
@since 12/07/2019
/*/
//-------------------------------------------------------------------
Static Function EscritXFil(cSiglaEscr, aFilxEscr, cCampo)
	Local cVal    := ""
	Local nPosFil := 0

	nPosFil := aScan(aFilxEscr, {|aX| aX[1] == cSiglaEscr})

	If nPosFil > 0
		If AllTrim(cCampo) == "NS7_CEMP"
			cVal := aFilxEscr[nPosFil][2][1]

		ElseIf AllTrim(cCampo) == "NS7_CFILIA"
			cVal := aFilxEscr[nPosFil][2][2]
		EndIf
	EndIf

Return cVal

//-------------------------------------------------------------------
/*/{Protheus.doc} MigraDePar
Grava a tabela de de-para OHS

@param cEntidaSis, Tabela do Sisjuri que fica gravado o c�digo do SisJuri
@param cCodigoSis, C�digo do SisJuri
@param cEsctSis  , Escrit�rio que o registro pertence
@param cCpoCodSis, Campo da tabela do Sijuri que fica gravado o c�digo
@param cEntidaPro, Tabela do Protheus que fica gravado o registro
@param cCodProthe, C�digo do registro utilizado no Protheus
@param cCpoCodPro, Campo que fica gravado o c�digo do Protheus
@param lVerExist , Verifica se j� existe o registro na OHS

@author Bruno Ritter
@since 12/07/2019
/*/
//-------------------------------------------------------------------
Static Function MigraDePar(cEntidaSis, cCodigoSis, cEsctSis, cCpoCodSis, cEntidaPro, cCodProthe, cCpoCodPro, lVerExist)
	Local   lInclui   := .T.
	Default lVerExist := .F.

	If lVerExist
		OHS->(DbSetOrder(1)) // OHS_FILIAL, OHS_ENTSIS, OHS_CODSIS, OHS_FILSIS
		lInclui := !(OHS->(DbSeek( _cFilOHS +;
		                          AvKey(cEntidaSis, "OHS_ENTSIS") +;
		                          AvKey(cValToChar(cCodigoSis), "OHS_CODSIS") +;
		                          AvKey(cValToChar(cEsctSis), "OHS_FILSIS"))))
	EndIf

	RecLock("OHS", lInclui)
	OHS->OHS_ENTSIS := cEntidaSis
	OHS->OHS_CODSIS := AllTrim(cValToChar(cCodigoSis))
	OHS->OHS_FILSIS := AllTrim(cValToChar(cEsctSis))
	OHS->OHS_KEYSIS := cCpoCodSis
	OHS->OHS_ENTPRO := cEntidaPro
	OHS->OHS_CODPRO := cCodProthe
	OHS->OHS_KEYPRO := cCpoCodPro
	OHS->(MsUnlock())
	OHS->(DbCommit())

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} LoadPais
Faz o de-para do pa�s entre o SisJuri e Protheus.

@param oProcess  , Objeto do processa
@param nConnSisJr, Conex�o do SisJuri
@param nConnProth, Conex�o do Protheus

@author Bruno Ritter
@since 15/07/2019
/*/
//-------------------------------------------------------------------
Static Function LoadPais(oProcess, nConnSisJr, nConnProth)
	Local cQuery     := ""
	Local aRetSql    := {}
	Local cCodPais   := ""
	Local cDescPais  := ""
	Local cCodProthe := ""
	Local nReg       := 0

	oProcess:IncRegua1("Carregando de-para do Pa�s")

	cQuery += " SELECT "
	cQuery +=      " PAINCODIGO, "
	cQuery +=      " PAICDESCRICAO "
	cQuery += " FROM RCR.PAIS "
	cQuery += " ORDER BY RCR.PAIS.PAICDESCRICAO "

	TCSetConn(nConnSisJr)
	aRetSql := JurSQL(cQuery, "*")
	oProcess:SetRegua2(Len(aRetSql))

	TCSetConn(nConnProth)
	SYA->(DbSetOrder(2)) // YA_FILIAL, YA_DESCR

	For nReg := 1 To Len(aRetSql)
		oProcess:IncRegua2(i18n("Importando registro #1 de #2", {nReg, Len(aRetSql)}))
		cCodPais  := cValToChar(aRetSql[nReg][1])
		cDescPais := Alltrim(Upper(NoAcento(aRetSql[nReg][2])))

		If SYA->(DbSeek(xFilial("SYA") + cDescPais))
			cCodProthe := SYA->YA_CODGI
		Else
			cCodProthe := "999"
		EndIf

		MigraDePar("RCR.PAIS", cCodPais, "", "PAINCODIGO", "SYA", cCodProthe, "YA_CODGI")
	Next nReg

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} LoadMunici
Faz os de-para do munic�pio entre o SisJuri e Protheus.

@param oProcess  , Objeto do processa
@param nConnSisJr, Conex�o do SisJuri
@param nConnProth, Conex�o do Protheus

@author Bruno Ritter
@since 15/07/2019
/*/
//-------------------------------------------------------------------
Static Function LoadMunici(oProcess, nConnSisJr, nConnProth)
	Local cQuery     := ""
	Local aRetSql    := {}
	Local cCodMun    := ""
	Local cDescMun   := ""
	Local cCodProthe := ""
	Local cEstado    := ""
	Local nReg       := 0
	Local cFilCC2	:= xFilial("CC2")

	oProcess:IncRegua1("Carregando de-para do Munic�pio")

	cQuery += " SELECT "
	cQuery +=       " RCR.CIDADE.CIDNCODIGO, "
	cQuery +=       " RCR.CIDADE.CIDCDESCRICAO, "
	cQuery +=       " RCR.ESTADO.ESTCSIGLA "
	cQuery += " FROM RCR.CIDADE "
	cQuery += " INNER JOIN RCR.ESTADO "
	cQuery +=         " ON RCR.CIDADE.ESTNCODIGO = RCR.ESTADO.ESTNCODIGO "
	cQuery += " ORDER BY RCR.ESTADO.ESTCSIGLA, RCR.CIDADE.CIDCDESCRICAO "

	TCSetConn(nConnSisJr)
	aRetSql := JurSQL(cQuery, "*")
	oProcess:SetRegua2(Len(aRetSql))

	TCSetConn(nConnProth)
	CC2->(DbSetOrder(4)) // CC2_FILIAL, CC2_EST, CC2_MUN

	For nReg := 1 To Len(aRetSql)
		oProcess:IncRegua2(i18n("Importando registro #1 de #2", {nReg, Len(aRetSql)}))
		cCodMun   := cValToChar(aRetSql[nReg][1])
		cDescMun  := Alltrim(Upper(NoAcento(aRetSql[nReg][2])))
		cEstado   := AllTrim(Upper(aRetSql[nReg][3]))

		If CC2->(DbSeek(cFilCC2 + AvKey(cEstado, "CC2_EST") + cDescMun))
			cCodProthe := CC2->CC2_CODMUN
			MigraDePar("RCR.CIDADE", cCodMun, "", "CIDNCODIGO", "CC2", cCodProthe, "CC2_CODMUN")
		EndIf
	Next nReg

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} GetDePara
Busca a chave na tabela OHS (de-para)

@param cEntidaSis, Tabela do Sisjuri que o registro fica gravado
@param xCodigoSis, C�digo utilizado no SisJuri
@param cEsctSis  , Escrit�rio que o registro est� gravado

@return cCodProthe, C�digo utilizado no Protheus

@author Bruno Ritter
@since 15/07/2019
/*/
//-------------------------------------------------------------------
Static Function GetDePara(cEntidaSis, xCodigoSis, cEsctSis)
	Local cCodProthe   := ""
	Local cChave       := ""
	Local nPos         := 0

	Default cEntidaSis := ""
	Default xCodigoSis := ""
	Default cEsctSis   := ""

	cChave := _cFilOHS +;
	          Padr(cEntidaSis, _nTamEntSi) +;
	          Padr(cValToChar(xCodigoSis), _nTamCodSi) +;
	          Padr(cEsctSis, _nTamFilSi)

	If (nPos := aScan(_aBuffReg, {|r| r[1] == cChave }) ) = 0

		cCodProthe := POSICIONE('OHS', 1, cChave, 'OHS_CODPRO')
	    aAdd(_aBuffReg, {cChave, cCodProthe})
	Else
		cCodProthe := _aBuffReg[nPos, 02]
	EndIf

Return cCodProthe

//-------------------------------------------------------------------
/*/{Protheus.doc} GrvContCli
Faz o vinculo do cliente/contato

@param cEntidaSis, Tabela do Sisjuri que o registro fica gravado
@param cCodigoSis, C�digo utilizado no SisJuri
@param cEsctSis  , Escrit�rio que o registro est� gravado

@return cCodProthe, C�digo utilizado no Protheus

@author Abner Foga�a de Olivera | Jonatas Martins
@since 09/09/2019
/*/
//-------------------------------------------------------------------
Static Function GrvContCli(cEntidaSis, cCodigoSis, cEsctSis)
	Local aAreaOHS := OHS->(GetArea())
	Local cCodEnt  := ""
	Local cFilAC8  := xFilial("AC8")
	Local cFilSA1  := xFilial("SA1")

	Default cEntidaSis := ""
	Default cCodigoSis := ""
	Default cEsctSis   := ""

	OHS->(DbGoTop())
	OHS->(DbSetOrder(1)) // OHS_FILIAL+OHS_ENTSIS+OHS_CODSIS+OHS_FILSIS

	If OHS->(DbSeek(_cFilOHS + AvKey(cEntidaSis, "OHS_ENTSIS") + AllTrim(cCodigoSis)))
		cCodEnt := Substr(GetDePara("RCR.ENDERECO", cCodigoSis, ""), 1, TamSX3("AC8_CODENT")[1])
		While AllTrim(cEntidaSis) == Alltrim(OHS->OHS_ENTSIS) .And. AllTrim(cCodigoSis) == AllTrim(OHS->OHS_CODSIS)
				RecLock("AC8", .T.)
				AC8->AC8_FILIAL := cFilAC8
				AC8->AC8_FILENT := cFilSA1
				AC8->AC8_ENTIDA := "SA1"
				AC8->AC8_CODENT := cCodEnt
				AC8->AC8_CODCON := OHS->OHS_CODPRO
				AC8->(MsUnlock("AC8"))
			OHS->(DbSkip())
		EndDo
	EndIf

	RestArea(aAreaOHS)
Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} BuscaLoja
Busca na vari�vel private (aCliente/aFornece) as informa��es  para obter o c�digo
da pr�xima Loja.

@return cCodLoja, C�digo da Loja a ser utilizado

@author Cristina Cintra
@since 19/07/2019
/*/
//-------------------------------------------------------------------
Static Function BuscaLoja(cAlias)
Local cLoja := ""

If cAlias == 'SA1'
	aCliente[2] := Soma1(aCliente[2])
	cLoja := aCliente[2]
Else
	aFornece[2] := Soma1(aFornece[2])
	cLoja := aFornece[2]
EndIf

Return cLoja

//-------------------------------------------------------------------
/*/{Protheus.doc} BuscaForne
Busca no Participante as informa��es de Fornecedor + Loja e grava na 
vari�vel Private.

@param cCodPart, C�digo do Participante

@return Nil

@author Cristina Cintra
@since 22/07/2019
/*/
//-------------------------------------------------------------------
Static Function BuscaForne(cCodPart)
Local cCodForn   := ""
Local cCodLoja   := ""
Local cCodProthe := GetDePara("RCR.ADVOGADO", cCodPart, "")

RD0->(dbGoTop())
RD0->(DbSetOrder(1)) // RD0_FILIAL+RD0_CODIGO
If RD0->(DbSeek(xFilial("RD0") + cCodProthe))
	cCodForn := RD0->RD0_FORNEC
	cCodLoja := RD0->RD0_LOJA
EndIf

aFornece[1] := cCodForn
aFornece[2] := cCodLoja

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} CriaFornec
Cria o Forncedor a partir do Participante RD0

@param oProcess  , Objeto do processa
@param nConnSisJr, Conex�o do SisJuri
@param nConnProth, Conex�o do Protheus

@author  Bruno Ritter
@since   22/07/2019
/*/
//-------------------------------------------------------------------
Static Function CriaFornec(oProcess, nConnSisJr, nConnProth)
	Local cCodForn  := JurSql("SELECT MAX(A2_COD) MAIOR FROM " + RetSqlName("SA2"), "MAIOR")[1][1]
	Local cCodLoja  := StrZero(1, TamSX3("A2_LOJA")[1])
	Local cAliasQry := GetNextAlias()
	Local cQryConta := ""
	Local cQryExec  := ""

	oProcess:IncRegua1("Criando fornecedor vinculado ao participante")

	cQryConta := " SELECT PCTCNUMEROCONTA "
	cQryConta +=  " FROM FINANCE.PLANOCONTAS P "
	cQryConta += " WHERE P.PCTCNUMEROCONTA LIKE FINANCE.FNS_LOCATENIVELCONTA(P.PCTCNUMEROCONTA, 1) || '.%' "
	cQryConta +=   " AND P.TPCCCODIGO = 'P' "
	cQryConta +=   " AND P.PCTCNUMEROCONTA LIKE '%' || '>USER<' "

	NUR->(DbGoTop())
	RD0->(DbSetOrder(1)) // RD0_FILIAL + RD0_CODIGO
	oProcess:SetRegua2(0)

	While NUR->(!Eof())

		If NUR->NUR_FORNAT == "1" .And. RD0->(DbSeek(NUR->NUR_FILIAL + NUR->NUR_CPART))
			oProcess:IncRegua2("Criando fornecedor para o participante '" + RD0->RD0_SIGLA + "'")
			cCodForn := Soma1(cCodForn)

			TCSetConn(nConnSisJr) // SISJURI
			cQryExec := StrTran(cQryConta, ">USER<", AllTrim(RD0->RD0_SIGLA))
			DbUseArea(.T., "TOPCONN", TcGenQry(,, cQryExec), cAliasQry, .F., .T.)
		
			cContaSJ := ""
			If !(cAliasQry)->( Eof() )
				cContaSJ := Substr(StrTran((cAliasQry)->PCTCNUMEROCONTA, ".", ""), 1, TamSX3("ED_CODIGO")[1])
			EndIf
			(cAliasQry)->(DbCloseArea())

			TCSetConn(nConnProth) // Protheus

			RecLock("SA2", .T.)
			//SA2->A2_FILIAL  := xFilial("SA2") // Forncedor deve ser compartilhado
			SA2->A2_COD     := cCodForn
			SA2->A2_LOJA    := cCodLoja
			SA2->A2_NOME    := RD0->RD0_NOME
			SA2->A2_NREDUZ  := Substr(RD0->RD0_NOME, 1, TamSX3("A2_NREDUZ")[1])
			SA2->A2_END     := Substr(RD0->RD0_END, 1, TamSX3("A2_END")[1])
			SA2->A2_EST     := RD0->RD0_UF
			SA2->A2_MUN     := RD0->RD0_MUN
			SA2->A2_TIPO    := "F"
			SA2->A2_NATUREZ := cContaSJ
			SA2->A2_MSBLQL  := RD0->RD0_MSBLQL
			SA2->A2_CGC     := RD0->RD0_CIC
			SA2->A2_ENDCOMP := RD0->RD0_CMPEND
			SA2->A2_CEP     := RD0->RD0_CEP
			SA2->A2_BAIRRO  := RD0->RD0_BAIRRO
			SA2->A2_TEL     := RD0->RD0_FONE
			SA2->A2_FAX     := RD0->RD0_FAX
			SA2->A2_EMAIL   := Substr(RD0->RD0_EMAIL, 1, TamSX3("A2_EMAIL")[1])
			SA2->A2_DTNASC  := RD0->RD0_DTNASC
			SA2->(MsUnlock())
			SA2->(DbCommit())

			RecLock("RD0", .F.)
			RD0->RD0_FORNEC := cCodForn
			RD0->RD0_LOJA   := cCodLoja
			RD0->(MsUnlock())
			RD0->(DbCommit())
		EndIf

		NUR->(DbSkip())
	EndDo

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} GetMoeCod
Monta o c�digo da moeda

@author Jonatas Martins
@since  22/07/2019
/*/
//-------------------------------------------------------------------
Static Function GetMoeCod(xValSisJur)
	Local cCodMoeda := ""
	Local cQueryCTO := ""
	Local aMoedas   := {}
	
	If xValSisJur == "NAC01"
		cCodMoeda := "01"
	Else
		cQueryCTO := "SELECT MAX(CTO_MOEDA) CODMOEDA FROM " + RetSqlName("CTO")
		aMoedas   := JurSQL(cQueryCTO, "CODMOEDA")
		If Empty(aMoedas) .Or. Empty(aMoedas[1][1])
			cCodMoeda := StrZero(2, TamSX3("CTO_MOEDA")[1])
		Else
			cCodMoeda := Soma1(aMoedas[1][1])
		EndIf
	EndIf

Return (cCodMoeda)

//-------------------------------------------------------------------
/*/{Protheus.doc} CaseRateio
Retorna case para buscar o tipo da tabela de rateio

@author  Bruno Ritter | Abner Foga�a
@since   26/08/2019
/*/
//-------------------------------------------------------------------
Static Function CaseRateio()
	Local cQuery := ""

	cQuery := " CASE "
	cQuery +=    " WHEN EXISTS (SELECT A.RATNCODIG "
	cQuery +=                   " FROM FINANCE.DETALHERATEIO A "
	cQuery +=                  " WHERE A.COD_ADVG IS NOT NULL "
	cQuery +=                    " AND A.RATNCODIG = FINANCE.RATEIO.RATNCODIG) THEN '3' " // Participante
	cQuery +=    " WHEN EXISTS (SELECT A.RATNCODIG "
	cQuery +=                   " FROM FINANCE.DETALHERATEIO A "
	cQuery +=                  " WHERE A.SETOR IS NOT NULL "
	cQuery +=                    " AND A.RATNCODIG = FINANCE.RATEIO.RATNCODIG) THEN '2' " // Centro de custo
	cQuery +=    " ELSE '1' " // Escrit�rio
	cQuery += " END "

Return cQuery

//-------------------------------------------------------------------
/*/{Protheus.doc} CaseCCusto
Retorna case para buscar o tipo da conta da natureza

@author  Abner Foga�a
@since   28/08/2019
/*/
//-------------------------------------------------------------------
Static Function CaseCCusto()
	Local cQuery := ""

	cQuery := " CASE "
	cQuery += " WHEN PCTCFLAGESCRITORIO = 'S' "
	cQuery += "   THEN '1' " // Escrit�rio  
	cQuery += " WHEN PCTCFLAGSETOR = 'S' "
	cQuery += "   THEN '2' " // Esc. e Centro de Custo Jur�dico
	cQuery += " WHEN PCTCFLAGPROFISSIONAL = 'S' "
	cQuery += "   THEN '3' " // Profissional
	cQuery += " WHEN PCTCFLAGRATEIO = 'S' "
	cQuery += "   THEN '4' " // Tabela de Rateio
	cQuery += " WHEN PCTCFLAGCLIENTE = 'S' "
	cQuery += "   THEN '5' " // Despesa de cliente
	cQuery += " END "

Return cQuery

//-------------------------------------------------------------------
/*/{Protheus.doc} QryCondPag
Query para retornar as condi��es de pagamento validas para
as condi��es de pagamentos do dispon�veis no padr�o do Protheus.

@param cTabConPag, Tabela do sisjuri com as condi��es de pagamentos
@param cCpoIdenti, Campos separados por '|' que identificam o vinculo da condi��o
                   ex: 'COD_CLIENTE|PASTA'

@author  Bruno Ritter
@since   27/08/2019
/*/
//-------------------------------------------------------------------
Static Function QryCondPag(cTabConPag, cCpoIdenti)
	Local cQuery     := ""
	Local cCodTab    := " TO_CHAR(S." + StrTran(cCpoIdenti, "|", "||'|'||S.") + ")"
	Local lInclPasta := cTabConPag == "SSJR.CAD_CAS_DIASVENC"

	cQuery += " SELECT "
	cQuery +=       " '" + cTabConPag + "' TAB,"
	cQuery +=       " '" + cCpoIdenti + "' CPO_IDENTI,"
	cQuery +=       cCodTab + " COD_TAB,"

	cQuery +=       " S.TIPO, "
	cQuery +=       " S.VALOR, "
	cQuery +=       " L.VALOR APARTIR_DE "
	cQuery += " FROM " + cTabConPag + " S "

	// Uni com a op��o "Dias ap�s emiss�o"
	cQuery += " LEFT JOIN " + cTabConPag + " L "
	cQuery +=        " ON L.COD_CLIENTE = S.COD_CLIENTE "
	If lInclPasta
		cQuery +=   " AND L.PASTA = S.PASTA "
	EndIf
	cQuery +=       " AND L.TIPO = 'D' "
	cQuery += " WHERE "

	// Filtra as condi��es onde a op��o "Dias ap�s emiss�o" foi usada
	// com "Dias Uteis" ou foi usada com "Dia do m�s" para o vencimento
	cQuery +=       cCodTab + "  NOT IN (SELECT " + StrTran(cCodTab, "S.", "A.")
	cQuery +=                               " FROM " + cTabConPag + " A "
	cQuery +=                              " WHERE A.TIPO = 'D'  "
	cQuery +=                                " AND (A.DIASUTEIS = 'S' "
	cQuery +=                                     " OR EXISTS (SELECT 1 "
	cQuery +=                                                  " FROM " + cTabConPag + " B "
	cQuery +=                                                 " WHERE B.TIPO = 'M' "
	If lInclPasta
		cQuery +=                                               " AND B.PASTA = A.PASTA "
	EndIf
	cQuery +=                                                   " AND B.COD_CLIENTE = A.COD_CLIENTE))) "

	// Filtra condi��es que foi definido mais de um dia do m�s ou da semana para o vencimento
	cQuery +=    " AND (SELECT COUNT(1) "
	cQuery +=           " FROM " + cTabConPag + " A "
	cQuery +=          " WHERE A.COD_CLIENTE = S.COD_CLIENTE "
	If lInclPasta
		cQuery +=        " AND A.PASTA = S.PASTA "
	EndIf
	cQuery +=            " AND A.TIPO = S.TIPO) = 1 "

	// Filtra apenas dias do m�s ou dias da semana que n�o foi usada a op��o "�ltimo do m�s"
	cQuery +=    " AND (S.TIPO = 'M' OR (S.TIPO = 'S' AND S.ULTIMO = 'N')) "

Return cQuery

//-------------------------------------------------------------------
/*/{Protheus.doc} GrvCondPag
Faz o de-para das condi��es de pagamentos do Sisjuri para o Protheus

@param oProcess  , Objeto do processa
@param nConnSisJr, Conex�o do SisJuri
@param nConnProth, Conex�o do Protheus

@author  Bruno Ritter
@since   27/08/2019
/*/
//-------------------------------------------------------------------
Function GrvCondPag(oProcess, nConnSisJr, nConnProth)
	Local cQuery     := ""
	Local aDados     := {}
	Local nI         := 0
	Local cTabela    := ""
	Local cCpoIdenti := ""
	Local cValor     := ""
	Local cPartirDe  := ""
	Local cCondicao  := ""
	Local cDescri    := "Migra��o"
	Local cCodTab    := ""
	Local cCodSE4    := ""
	Local cUltCodSE4 := StrZero(0, TamSX3("E4_CODIGO")[1])
	Local aChaveCond := {}
	Local nPosChave  := 0

	oProcess:IncRegua1("De-Para Condi��es de Pagamento")

	TCSetConn(nConnSisJr) // SISJURI
	cQuery := QryCondPag("SSJR.CAD_CLI_DIASVENC", "COD_CLIENTE")
	cQuery += " UNION ALL "
	cQuery += QryCondPag("SSJR.CAD_CAS_DIASVENC", "COD_CLIENTE|PASTA")

	aDados := JurSQL(cQuery, {"TAB", "CPO_IDENTI", "COD_TAB", "TIPO", "VALOR", "APARTIR_DE"})
	oProcess:SetRegua2(Len(aDados))

	TCSetConn(nConnProth) // Protheus
	For nI := 1 To Len(aDados)
		oProcess:IncRegua2(i18n("Importando registro #1 de #2", {nI, Len(aDados)}))

		cTabela    := AllTrim(aDados[nI][1])
		cCpoIdenti := AllTrim(aDados[nI][2])
		cCodTab    := cValToChar(aDados[nI][3])
		cTipo      := Iif(AllTrim(aDados[nI][4]) == "S", "6", "7")
		cValor     := cValToChar(aDados[nI][5])
		cPartirDe  := Iif(Empty(aDados[nI][6]), "0", cValToChar(aDados[nI][6]))

		If cTipo == "6"
			cCondicao := "1," + cPartirDe + "," + cValor + ",0"
	
		ElseIf cTipo == "7"
			cCondicao := "1" + Replicate( "," + cValor, 12 )
		EndIf

		nPosChave := aScan(aChaveCond, {|aChv| aChv[1] == cTipo + cCondicao})
		If nPosChave == 0
			RecLock("SE4", .T.)
			cUltCodSE4      := Soma1(cUltCodSE4)
			SE4->E4_CODIGO  := cUltCodSE4
			SE4->E4_TIPO    := cTipo
			SE4->E4_COND    := cCondicao
			SE4->E4_DESCRI  := cDescri
			SE4->E4_MSBLQL  := "2"
			SE4->E4_ACRES   := "N"
			SE4->E4_AGRACRS := "1"
			SE4->E4_CCORREN := "2"

			SE4->(MsUnlock())
			SE4->(DbCommit())
			cCodSE4 := cUltCodSE4
			aAdd(aChaveCond, {cTipo + cCondicao, cUltCodSE4})
		Else
			cCodSE4 := aChaveCond[nPosChave][2]
		EndIf

		MigraDePar(cTabela, cCodTab, "", cCpoIdenti, "SE4", cCodSE4, "E4_CODIGO")
	Next nI

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} LoadClNat
Fun��o para executar a carga inicial da OHP (Classifica��o de Natureza)
e ajuste quanto a conta pai de participantes.

@param oProcess  , Objeto do processa
@param nConnSisJr, Conex�o do SisJuri
@param nConnProth, Conex�o do Protheus

@author Cristina Cintra
@since  27/08/2019
/*/
//-------------------------------------------------------------------
Static Function LoadClNat(oProcess, nConnSisJr, nConnProth)
Local cQuery  := "SELECT VALOR FROM SSJR.CAD_PREFERENCIA WHERE ID_PREFERENCIA = 33"
Local aRetSql := {}
Local cFilOHP	:= xFilial("OHP")

oProcess:IncRegua1("Carga inicial e ajuste da Classifica��o de Naturezas")

JA266Carga() // Carga inicial de Classifica��o de Naturezas

TCSetConn(nConnSisJr)
aRetSql := JurSQL(cQuery, "VALOR")

TCSetConn(nConnProth)
OHP->(DbSetOrder(2)) // OHP_FILIAL + OHP_COD 
If !Empty(aRetSql) .And. !Empty(aRetSql[1][1]) .And. OHP->(DbSeek(cFilOHP + "008")) // "Natureza pai para as naturezas de participantes"
	RecLock("OHP", .F.)
	OHP->OHP_CNATUR := StrTran(aRetSql[1][1], '.', '')
	OHP->(MsUnlock())
	OHP->(DbCommit())
EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} LoadSX6
Faz o de-para dos par�metros / prefer�ncias entre o SisJuri e Protheus.

@param oProcess  , Objeto do processa
@param nConnSisJr, Conex�o do SisJuri
@param nConnProth, Conex�o do Protheus

@author Cristina Cintra
@since 27/08/2019
/*/
//-------------------------------------------------------------------
Static Function LoadSX6(oProcess, nConnSisJr, nConnProth)
Local cQuery     := ""
Local cIdSisjuri := ""
Local aDePara    := {}
Local aParamSis  := {}
Local nI         := 0
Local nReg       := 0
Local nPos       := 0
Local cSisNome   := ""
Local cSisValor  := ""
Local cParamSX6  := ""
Local lForceVal  := .F.
Local bLogico    := {|| Iif(!Empty(cSisValor), (Iif(cSisValor == 'S', .T., .F.)), )}
Local bDtEmiNF   := {|| Iif(!Empty(cSisValor), (Iif(cSisValor == 'F', .T., .F.)), )}
Local bDtCotNF   := {|| Iif(!Empty(cSisValor), (Iif(cSisValor == 'F', '1', Iif(cSisValor == 'N', '2', '3'))), )}
Local bCarSimNao := {|| Iif(!Empty(cSisValor), (Iif(cSisValor == 'S', '1', '2')), )}
Local bCarNaoSim := {|| Iif(!Empty(cSisValor), (Iif(cSisValor == 'N', '1', '2')), )}
Local bDtDesp    := {|| Iif(!Empty(cSisValor), (Iif(cSisValor == 'D', '2', '1')), )}
Local bDtTab     := {|| Iif(!Empty(cSisValor), (Iif(cSisValor == 'C', '3', Iif(cSisValor == 'I', '2', '1'))), )}
Local bCorte     := {|| Iif(!Empty(cSisValor), (Iif(cSisValor == 'M', '1', '2')), )}
Local bRecalc    := {|| Iif(!Empty(cSisValor), (Iif(cSisValor == 'S', '0000-00', '9999-99')), )}
Local bJurTS2    := {|| Iif(!Empty(cSisValor), (Iif(cSisValor == 'U', '1', Iif(cSisValor == 'F', '2', '3'))), )}
Local bGetMoeda  := {|| Iif(!Empty(cSisValor), Substr(GetDePara("RCR.MOEDA", cSisValor, ""), 1, TamSX3("CTO_MOEDA")[1]), ) }
Local bRmvPonto  := {|| StrTran(cSisValor, '.', '') }
Local bCodNewPar := {|| " '" + StrTran(SubStr(cSisValor, 1, Rat('.', cSisValor)), '.', '') + "' + FwFldGet('RD0_SIGLA')"}
Local bSerieNF   := {|| AjsSerieNf(cSisValor) }
Local bAjsPict   := {|| AjsPicture(cSisValor) }
Local xVal       := Nil
Local bGetSA1Cli := {|| StrZero(Val(cSisValor), TamSX3("A1_COD")[1])}
Local bGetSA1Loj := {|| Iif(!Empty(cSisValor), BuscaEndPad(cSisValor, nConnSisJr, nConnProth), "") }
Local cFilSX6	:= xFilial("SX6")

oProcess:IncRegua1("Carga inicial de Par�metros (SX6) / Prefer�ncias")

//               ID Sisjuri, Nome Sisjuri                      , Protheus      , Force
aAdd( aDePara, { 33        , 'CONTADEBPART'                    , 'MV_JNATPAR'  , bCodNewPar } )
aAdd( aDePara, { 0         , ''                                , 'MV_JURXFIN'  , {|| '.T.'} } )
aAdd( aDePara, { 0         , ''                                , 'MV_FINATFN'  , {|| '1'}   } )
aAdd( aDePara, { 0         , ''                                , 'MV_NATSINT'  , {|| '1'}    } )
aAdd( aDePara, { 134       , 'PRTCPADRAOCLIENTE'               , 'MV_JALTCT'   , bLogico    } )
aAdd( aDePara, { 349       , 'DESABILITAEXCLUSAOALTERACAODESP' , 'MV_JALTDSP'  , bCarSimNao } )
aAdd( aDePara, { 7         , 'ALTRAZAOFATURANF'                , 'MV_JALTRAZ'  , bCarNaoSim } )
aAdd( aDePara, { 1375      , 'ARREDPARTICIPACAO'               , 'MV_JARPART'  , bCarSimNao } )
aAdd( aDePara, { 179       , 'TRAVALANCFATADIC'                , 'MV_JBLQLFA'  , bCarSimNao } )
aAdd( aDePara, { 110       , 'NUMCASOCLI'                      , 'MV_JCASO1'   , bCarSimNao } )
aAdd( aDePara, { 99        , 'LACNUMCASO'                      , 'MV_JCASO2'   , bLogico    } )
aAdd( aDePara, { 117       , 'OBSNCOBRARDESPESA'               , 'MV_JCOBDSP'  , bLogico    } )
// No SisJuri s� tem o c�digo do Fornecedor, n�o sendo poss�vel 
// relacionar com o protheus que tem que ter o endere�o do forncedor tamb�m
//aAdd( aDePara, { 21        , 'CODFAVORECIDOCP'                 , 'MV_JCODFAV'  , bGetCodFav } )
//aAdd( aDePara, { 21        , 'CODFAVORECIDOCP'                 , 'MV_JLOJFAV'  , bGetLojFav } )
aAdd( aDePara, { 70        , 'DTCONVDESP'                      , 'MV_JCONVDS'  , bDtDesp    } )
aAdd( aDePara, { 71        , 'DTCONVTABELADO'                  , 'MV_JCONVLT'  , bDtTab     } )
aAdd( aDePara, { 138       , 'QTDDIASCORTE'                    , 'MV_JCORDIA'  , Nil        } )
aAdd( aDePara, { 93        , 'HORACORTE'                       , 'MV_JCORHRA'  , {|| Substr(cSisValor, 1, 5) } } )
aAdd( aDePara, { 53        , 'CRITERIOTS'                      , 'MV_JCORTE'   , bCorte     } )
aAdd( aDePara, { 34        , 'CONTADESPESACLIENTE'             , 'MV_JDESCLI'  , bRmvPonto  } )
aAdd( aDePara, { 1389      , 'PERCDESCLEGALDESK'               , 'MV_JDESCMX'  , Nil   } )
aAdd( aDePara, { 65        , 'DIFNUMFATDESP'                   , 'MV_JDFTDES'  , bLogico    } )
aAdd( aDePara, { 66        , 'DIFNUMFATHON'                    , 'MV_JDFTHON'  , bLogico    } )
aAdd( aDePara, { 67        , 'DIFNUMFATINTER'                  , 'MV_JDFTINT'  , bLogico    } )
aAdd( aDePara, { 61        , 'DATAVENCAGENDA'                  , 'MV_JDTAGEN'  , bLogico    } )
aAdd( aDePara, { 1379      , 'BLOQCTACOUTRAEMPRESA'            , 'MV_JFILBCO'  , bLogico    } )
aAdd( aDePara, { 151       , 'SERVENAMEINTERWOVEN'             , 'MV_JGEDSER'  , Nil        } )
aAdd( aDePara, { 139       , 'QTDDIASENCCASO'                  , 'MV_JLANC1'   , Nil        } )
aAdd( aDePara, { 72        , 'EMIPREFATLIMTOTALEXCEDIDO'       , 'MV_JLIMEXC'  , bCarSimNao } )
aAdd( aDePara, { 105       , 'MOEDAOFICIAL'                    , 'MV_JMOENAC'  , bGetMoeda  } )
aAdd( aDePara, { 34        , 'CONTARECEITACLIENTE'             , 'MV_JNATFAT'  , bRmvPonto  } )
aAdd( aDePara, { 34        , 'CONTADESPESACLIENTE'             , 'MV_JNATDES'  , bRmvPonto } )
aAdd( aDePara, { 113       , 'NUMINIFATHON'                    , 'MV_JNFTHON'  , Nil        } )
aAdd( aDePara, { 114       , 'NUMINIFATINTER'                  , 'MV_JNFTINT'  , Nil        } )
aAdd( aDePara, { 112       , 'NUMINIFATDESP'                   , 'MV_JNTDES'   , Nil        } )
aAdd( aDePara, { 100       , 'LACNUMCLIENTE'                   , 'MV_JNUCLI'   , bLogico    } )
aAdd( aDePara, { 69        , 'DIRPADPRELOTE'                   , 'MV_JPASPRE'  , Nil        } )
aAdd( aDePara, { 121       , 'PRAZOBOLETO'                     , 'MV_JPRAZOB'  , Nil        } )
aAdd( aDePara, { 122       , 'PRAZODIVERSOS'                   , 'MV_JPRAZOD'  , Nil        } )
aAdd( aDePara, { 16        , 'CALCVLTSPREFAT'                  , 'MV_JRECTS'   , bRecalc    } )
aAdd( aDePara, { 316       , 'REPLICARTS'                      , 'MV_JREPZER'  , bCarSimNao } )
aAdd( aDePara, { 1388      , 'INTEGRACAOLEGALDESK'             , 'MV_JREVILD'  , bCarSimNao } )
aAdd( aDePara, { 155       , 'SUGDTHONFATFIXO'                 , 'MV_JRFDPFX'  , bCarSimNao } )
aAdd( aDePara, { 321       , 'SERIENFPROTHEUS'                 , 'MV_JSERNF'   , bSerieNF   } )
aAdd( aDePara, { 1326      , 'UTILIZASISCOSERV'                , 'MV_JSISSRV'  , bLogico    } )
aAdd( aDePara, { 89        , 'HABHIST'                         , 'MV_JURHS1'   , bLogico    } )
aAdd( aDePara, { 92        , 'HISTMESANT'                      , 'MV_JURHS2'   , bLogico    } )
aAdd( aDePara, { 188       , 'UTILHITPART'                     , 'MV_JURHS3'   , bLogico    } )
aAdd( aDePara, { 19        , 'CLIENTEREPESCRITORIO'            , 'MV_JURTS9'   , bGetSA1Cli } )
aAdd( aDePara, { 19        , 'CLIENTEREPESCRITORIO'            , 'MV_JURTS10'  , bGetSA1Loj } ) // Busca o Endere�o Padr�o do Cliente
aAdd( aDePara, { 166       , 'TEMPOTS'                         , 'MV_JURTS2'   , bJurTS2    } )
aAdd( aDePara, { 199       , 'PERMITE_UT_FRAC'                 , 'MV_JURTS3'   , bLogico    } )
aAdd( aDePara, { 197       , 'ZERARTEMPREVTS'                  , 'MV_JURTS4'   , bLogico    } )
aAdd( aDePara, { 20        , 'CLITSLANCFUT'                    , 'MV_JURTS5'   , bGetSA1Cli } )
aAdd( aDePara, { 20        , 'CLITSLANCFUT'                    , 'MV_JURTS6'   , bGetSA1Loj } ) // Busca o Endere�o Padr�o do Cliente
aAdd( aDePara, { 183       , 'TSTABELADO'                      , 'MV_JURTS7'   , bLogico   } )
aAdd( aDePara, { 319       , 'GERARPRETSZERADO'                , 'MV_JURTS8'   , bLogico   } )
aAdd( aDePara, { 187       , 'UTILFECHAMENTOFAT'               , 'MV_JUTFECH'  , bLogico   } )
aAdd( aDePara, { 1398      , 'HABILITAPROJETO'                 , 'MV_JUTPROJ'  , bLogico   } )
aAdd( aDePara, { 193       , 'VALORCOBRANCA'                   , 'MV_JVALMAX'  , Nil       } )
aAdd( aDePara, { 159       , 'SUGVENCINTER'                    , 'MV_JVENINT'  , Nil       } )
aAdd( aDePara, { 162       , 'SUGVENCNAC'                      , 'MV_JVENNAC'  , Nil       } )
aAdd( aDePara, { 347       , 'QTDEVIASBOLETO'                  , 'MV_JVIASBO'  , Nil       } )
aAdd( aDePara, { 145       , 'QTDVIACARTA'                     , 'MV_JVIASCA'  , Nil       } )
aAdd( aDePara, { 146       , 'QTDVIARELAT'                     , 'MV_JVIASRE'  , Nil       } )
aAdd( aDePara, { 196       , 'VINCTSFATFIXO'                   , 'MV_JVINCTS'  , bLogico   } )
aAdd( aDePara, { 2         , 'ADTOCONVERTIDOUTIL'              , 'MV_JDTCVAD'  , bCarNaoSim} )
aAdd( aDePara, { 1285      , 'ESCRITORIOEQUITRAC'              , 'MV_JESCTAR'  , Nil       } )
aAdd( aDePara, { 59        , 'DATANF'                          , 'MV_JEMINF'   , bDtEmiNF  } )
aAdd( aDePara, { 48        , 'CONVVALORDTEMINF'                , 'MV_JNFSCOT'  , bDtCotNF  } )
aAdd( aDePara, { 104       , 'MASCARAPLANOCONTA'               , ''            , bAjsPict  } )
aAdd( aDePara, { 44        , 'CONTATRANSPAGTO'                 , ''            , ;
                             {|| SetValue("SED", xFilial("SED") + StrTran(cSisValor, '.', ''), "ED_CCJURI", "7")} } ) // 7=Transit�ria de Pagamento
aAdd( aDePara, { 47        , 'CONTATRANSSALDONI'               , ''            , ;
                             {|| SetValue("SED", xFilial("SED") + StrTran(cSisValor, '.', ''), "ED_CCJURI", "6")} } ) // 6=Transit�ria P�s Pagamento
aAdd( aDePara, { 45        , 'CONTATRANSRECEITA'               , ''            , ;
                             {|| SetValue("SED", xFilial("SED") + StrTran(cSisValor, '.', ''), "ED_CCJURI", "8")} } ) // 8=Transit�ria de Recebimento"
aAdd( aDePara, { 0         , ''                                , 'MV_JADTTP'   , {|| 'RA'} } )
aAdd( aDePara, { 0         , ''                                , 'MV_JADTNAT'  , {|| '400010010'} } )

For nI := 1 To Len(aDePara)
	cIdSisjuri += "'" + aDePara[nI][2] + "', "
Next nI
cIdSisjuri := Left(cIdSisjuri, Len(cIdSisjuri) - 2)

cQuery := " SELECT NOME, VALOR "
cQuery += " FROM SSJR.CAD_PREFERENCIA "
cQuery += " WHERE NOME IN ( " + cIdSisjuri + " ) "

TCSetConn(nConnSisJr)
aParamSis := JurSQL(cQuery, "*")
oProcess:SetRegua2(Len(aDePara) + 1)

TCSetConn(nConnProth)
SX6->(DbSetOrder(1)) // X6_FIL + X6_VAR

For nReg := 1 To Len(aDePara)
	oProcess:IncRegua2(i18n("Par�metro #1 de #2", {nReg, Len(aDePara)}))

	nPos := aScan( aParamSis, { |x| AllTrim(x[1]) == aDePara[nReg][2]} )
	lForceVal := ValType(aDePara[nReg][4]) == "B"
	cParamSX6 := aDePara[nReg][3]

	If (nPos > 0 .Or. lForceVal) .And. (Empty(cParamSX6) .Or. SX6->(DbSeek(cFilSX6 + cParamSX6)))
		cSisNome   := Iif(nPos > 0, AllTrim(aParamSis[nPos][1]), "")
		cSisValor  := Iif(nPos > 0, AllTrim(aParamSis[nPos][2]), "")

		If lForceVal
			xVal := Eval(aDePara[nReg][4])
		Else
			xVal := cSisValor
		EndIf

		If !Empty(cParamSX6)
			RecLock("SX6", .F.)
			SX6->X6_CONTEUD := cValToChar(xVal)
			SX6->(MsUnlock())
			SX6->(DbCommit())
		EndIf
	EndIf
Next nReg

//Ajusta configura��o do tarifador
oProcess:IncRegua2("Ajusta Conf. Tarifador")
AjsCfTarif(nConnSisJr, nConnProth)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} AjsSerieNf
Ajusta a serie de nota fiscal conforme a preferencia do sisjuri

@param cSerieNF, Serie de NF que est� na preferencia do SisJuri

@Return cSerieNF, Retorna a mesma s�rie para ficar igual no Protheus

@author  Bruno Ritter
@since   30/08/2019
/*/
//-------------------------------------------------------------------
Static Function AjsSerieNf(cSerieNF)

	dbSelectArea( "SX5" )
	cSerieNF := AvKey(cSerieNF, "X5_CHAVE")

	SX5->(DbSetOrder(1)) // X5_FILIAL, X5_TABELA, X5_CHAVE
	If !SX5->(DbSeek(xFilial("SX5") + "01" + cSerieNF))
		RecLock("SX5", .T.)
		SX5->X5_TABELA  := "01"
		SX5->X5_CHAVE   := cSerieNF
		SX5->X5_DESCRI  := "000001"
		SX5->X5_DESCSPA := "000001"
		SX5->X5_DESCENG := "000001"
		SX5->(MsUnlock())
		SX5->(DbCommit())
	EndIf

Return cSerieNF

//-------------------------------------------------------------------
/*/{Protheus.doc} AjsPicture
Ajustar a picture do c�digo ED_CODIGO para ficar parecido com o Sisjuri

@param cPictSJ, Picture do SisJuri com ('0', 'A', '.')

@author  Bruno Ritter
@since   30/08/2019
/*/
//-------------------------------------------------------------------
Static Function AjsPicture(cPictSJ)

	If !Empty(cPictSJ)
		dbSelectArea( "SX3" )
		SX3->(DbSetOrder(2)) // X3_CAMPO

		If SX3->(DbSeek("ED_CODIGO "))
			RecLock("SX3", .F.)
			SX3->X3_PICTURE := "@R " + StrTran(StrTran(cPictSJ, '0', '9'), 'A', 'N')
			SX3->(MsUnlock())
			SX3->(DbCommit())
		EndIf
	EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} SetValue
Ajudar cadastro conforme o par�metro do SisJuri

@param cTab  , Tabela para ajustar
@param cChave, Chave do registro para ser alterado
@param cCampo, Campo para receber o valor
@param cVal  , Valor do Sisjuri
@param nIndex, Indece para para achar o registro quando ele for diferente de 1

@author  Bruno Ritter
@since   01/09/2019
/*/
//-------------------------------------------------------------------
Static Function SetValue(cTab, cChave, cCampo, cVal, nIndex)
	Default nIndex := 1

	If !Empty(cVal)
		dbSelectArea(cTab)
		(cTab)->(DbSetOrder(nIndex))

		If (cTab)->(DbSeek(cChave))
			RecLock(cTab, .F.)
			(cTab)->(FieldPut(FieldPos(cCampo), cVal))
			(cTab)->(MsUnlock())
			(cTab)->(DbCommit())
		EndIf
	EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} AjsCfTarif
Ajuda a configura��o do tarifador

@param nConnSisJr  Conex�o do SisJuri
@param nConnProth  Conex�o do Protheus

@author  Bruno Ritter
@since   01/09/2019
/*/
//-------------------------------------------------------------------
Static Function AjsCfTarif(nConnSisJr, nConnProth)
	Local cQuery    := ""
	Local cQryCfg   := ""
	Local cNatureza := ""
	Local cTipoDesp := ""
	Local aParamSis := {}
	Local aCfgTarif := {}
	Local nReg      := 0
	Local nCfg      := 0

	cQuery := QryCfTarif("CONTAIMP", "TPDESPIMP")
	cQuery += "UNION ALL"
	cQuery += QryCfTarif("CONTATELEFONIA", "TPDESPTELEFONIA")
	cQuery += "UNION ALL"
	cQuery += QryCfTarif("CONTACOPIAS", "TPDESPCOPIAS")
	cQuery += "UNION ALL"
	cQuery += QryCfTarif("CONTAFAX", "TPDESPFAX")

	TCSetConn(nConnSisJr)
	aParamSis := JurSQL(cQuery, {"CONTA", "TPDESP"})

	TCSetConn(nConnProth)
	For nReg := 1 To Len(aParamSis)

		If !Empty(aParamSis) .And. !Empty(aParamSis[nReg][1]) .And. !Empty(aParamSis[nReg][2])
			cNatureza := StrTran(aParamSis[nReg][1], '.', '')
			cTipoDesp := aParamSis[nReg][2]

			cQryCfg   := " SELECT NYV.R_E_C_N_O_  REC "
			cQryCfg   += " FROM " + RetSqlName("NYV") + " NYV "
			cQryCfg   += " WHERE NYV.NYV_TIPO = '" + cTipoDesp + "'"
			aCfgTarif := JurSQL(cQryCfg, "REC")

			For nCfg := 1 To Len(aCfgTarif)
				DbGoTo(aCfgTarif[nCfg][1])

				RecLock("NYV", .F.)
				NYV->NYV_CNATUR := cNatureza
				NYV->(MsUnlock())
				NYV->(DbCommit())
			Next nCfg
		EndIf

	Next nReg

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} QryCfTarif
Query para retornar o tipo de despesa e sua respectiva conta (natureza)

@param cPrefConta, Nome da prefer�ncia onde fica a conta (natureza)
@param cPrefTpDes, Nome da prefer�ncia onde fica o tipo de despesa

@author  Bruno Ritter
@since   01/09/2019
/*/
//-------------------------------------------------------------------
Static Function QryCfTarif(cPrefConta, cPrefTpDes)
	cQryBase := ""

	cQryBase := " SELECT "
	cQryBase +=      " VALOR CONTA, "
	cQryBase +=      " (SELECT VALOR FROM SSJR.CAD_PREFERENCIA WHERE NOME = '" + cPrefTpDes + "' ) TPDESP "
	cQryBase += " FROM SSJR.CAD_PREFERENCIA "
	cQryBase += " WHERE NOME = '" + cPrefConta + "' "

Return cQryBase

//-------------------------------------------------------------------
/*/{Protheus.doc} CaseTarfi
Case para pegar o tipo de despesa ou conta nas prefer�ncias para o tariador

@param lTpDesp    , .F.  para retornar a conta das prefer�ncias
                    .T.  para retornar o tipo de despesa das prefer�ncias
@param cCampo     , Campo do sisjuri para pegar o valor

@author  Bruno Ritter
@since   06/09/2019
/*/
//-------------------------------------------------------------------
Static Function CaseTarfi(lTpDesp, cCampo)
	Default cCampo := "DESCTIPO"

	cQuery := " CASE "
	cQuery +=       " WHEN " + cCampo + " like 'D%' " // Despesa
	cQuery +=            " THEN (SELECT VALOR FROM SSJR.CAD_PREFERENCIA "
	cQuery +=                  " WHERE NOME = '" + Iif(lTpDesp, "TPDESPESA", "CONTADESPESAS" ) + "' ) "

	cQuery +=       " WHEN " + cCampo + " like 'T%' " // Telefone
	cQuery +=            " THEN (SELECT VALOR FROM SSJR.CAD_PREFERENCIA "
	cQuery +=                  " WHERE NOME = '" + Iif(lTpDesp, "TPDESPTELEFONIA", "CONTATELEFONIA" ) + "' ) "

	cQuery +=       " WHEN " + cCampo + " like 'F%' " // Fax
	cQuery +=            " THEN (SELECT VALOR FROM SSJR.CAD_PREFERENCIA "
	cQuery +=                  " WHERE NOME = '" + Iif(lTpDesp, "TPDESPFAX", "CONTAFAX" ) + "' ) "

	cQuery +=       " WHEN " + cCampo + " like 'X%' " // Xerox
	cQuery +=            " THEN (SELECT VALOR FROM SSJR.CAD_PREFERENCIA "
	cQuery +=                  " WHERE NOME = '" + Iif(lTpDesp, "TPDESPCOPIAS", "CONTACOPIAS" ) + "' ) "

	cQuery +=       " WHEN " + cCampo + " like 'I%' " // Impressora
	cQuery +=            " THEN (SELECT VALOR FROM SSJR.CAD_PREFERENCIA "
	cQuery +=                  " WHERE NOME = '" + Iif(lTpDesp, "TPDESPIMP", "CONTAIMP" ) + "' ) "
	cQuery += " END "

Return cQuery

//-------------------------------------------------------------------
/*/{Protheus.doc} RepDescTar
Replica a descri��o do tarifado do Sisjuri para todos idiomas cadastrados
no Protheus

@param cCodCfg   , Tipo de despesa no Tarifador do Sisjuri
@param cTipoDesp , .F.  para retornar a conta das prefer�ncias
@param cDescricao, Campo do sisjuri para pegar o valor

@author  Bruno Ritter
@since   06/09/2019
/*/
//-------------------------------------------------------------------
Static Function RepDescTar(cCodCfg, cTipoDesp, cDescricao)

	If !Empty(cDescricao)
		dbSelectArea("NR1")
		NR1->(DbSetOrder(1)) // NR1_FILIAL + NR1_COD
		NR1->(DbGoTop())

		While NR1->(!Eof())

			RecLock("NYY", .T.)
			NYY->NYY_COD    := GetSxeNum("NYY", "NYY_COD")
			NYY->NYY_CODCFG := cCodCfg
			NYY->NYY_TIPO   := cTipoDesp
			NYY->NYY_CIDIOM := NR1->NR1_COD
			NYY->NYY_DESC   := cDescricao
			NYY->(MsUnlock())
			NYY->(DbCommit())

			NR1->(DbSkip())
		EndDo
	EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} GeraSM2
Gera a SM2 conforme vai cadastrando a CTT

@param cData    , Data da cota��o
@param cMoeda   , C�digo da moeda no protheus
@param nContacao, Cota��o

@author  Bruno Ritter
@since   06/09/2019
/*/
//-------------------------------------------------------------------
Static Function GeraSM2(cData, cMoeda, nContacao)
	Local lInclui := .T.

	If !Empty(nContacao) .And. !Empty(cMoeda) .And. !Empty(cData)
		dbSelectArea("SM2")
		SM2->(DbSetOrder(1)) // M2_DATA
		lInclui := !(SM2->(DbSeek(cData)))

		RecLock("SM2", lInclui)
		SM2->M2_DATA   := cData
		SM2->M2_MOEDA2 := Iif(Val(cMoeda) == 2, nContacao, SM2->M2_MOEDA2)
		SM2->M2_MOEDA3 := Iif(Val(cMoeda) == 3, nContacao, SM2->M2_MOEDA3)
		SM2->M2_MOEDA4 := Iif(Val(cMoeda) == 4, nContacao, SM2->M2_MOEDA4)
		SM2->M2_MOEDA5 := Iif(Val(cMoeda) == 5, nContacao, SM2->M2_MOEDA5)
		SM2->M2_INFORM := "S"
		SM2->(MsUnlock())
		SM2->(DbCommit())
	EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} GrvContato
Faz o de-para das dos contatos Sisjuri para o Protheus

@param oProcess  , Objeto do processa
@param nConnSisJr, Conex�o do SisJuri
@param nConnProth, Conex�o do Protheus

@author  Abner Foga�a de Oliveira | Jorge Martins
@since   06/09/2019
/*/
//-------------------------------------------------------------------
Static Function GrvContato(oProcess, nConnSisJr, nConnProth)
	Local cQuery     := ""
	Local aDados     := {}
	Local aAGB       := {}
	Local nI         := 0
	Local nJ         := 0
	Local cNomeCont  := ""
	Local cDDI       := ""
	Local cDDD       := ""
	Local cTelefone  := ""
	Local cFax       := ""
	Local cEmail     := ""
	Local cEndereco  := ""
	Local cTabela    := "RCR.ENDERECO-(CONTATOS)"
	Local cCpoIdenti := "CONTATO|CONTATOCOB"

	oProcess:IncRegua1("De-Para Contatos")

	TCSetConn(nConnSisJr) // SISJURI
	cQuery := "SELECT * "
	cQuery +=  " FROM ( "
	cQuery +=        " SELECT DISTINCT UPPER(CONTATO) NOME, COD_CLIENTE CLIENTE, COD_ENDERECO LOJA, DDI, DDD, TELEFONE TEL, FAX, EMAIL, '" + Space(500) + "' ENDERECO "
	cQuery +=          " FROM RCR.ENDERECO "
	cQuery +=         " WHERE CONTATO IS NOT NULL "
	cQuery +=         " UNION "
	cQuery +=        " SELECT DISTINCT UPPER(CONTATOCOB) NOME, COD_CLIENTE CLIENTE, COD_ENDERECO LOJA, DDI, DDD, TELEFONECOB TEL, FAXCOB, EMAILCOB EMAIL, ENDERECOCOB "
	cQuery +=          " FROM RCR.ENDERECO "
	cQuery +=         " WHERE CONTATOCOB IS NOT NULL "
	cQuery +=       " ) ORDER BY NOME "

	aDados := JurSQL(cQuery, "*")
	oProcess:SetRegua2(Len(aDados))

	TCSetConn(nConnProth) // Protheus
	For nI := 1 To Len(aDados)
		oProcess:IncRegua2(i18n("Importando registro #1 de #2", {nI, Len(aDados)}))

		cNomeCont := AllTrim(NoAcento(aDados[nI][1]))
		cCodClien := cValToChar(aDados[nI][2])
		cCodLoja  := cValToChar(aDados[nI][3])
		cDDI      := AllTrim(aDados[nI][4])
		cDDD      := AllTrim(aDados[nI][5])
		cTelefone := AllTrim(aDados[nI][6])
		cFax      := AllTrim(aDados[nI][7])
		cEmail    := AllTrim(aDados[nI][8])
		cEndereco := AllTrim(aDados[nI][9])
		
		RecLock("SU5", .T.)
		cCodSU5         := GetSXENum("SU5", "U5_CODCONT")
		SU5->U5_CODCONT := cCodSU5
		SU5->U5_CONTAT  := cNomeCont
		SU5->U5_EMAIL   := cEmail

		SU5->(MsUnlock())
		SU5->(DbCommit())
		
		If !Empty(cEndereco)
			RecLock("AGA", .T.)
			AGA->AGA_CODIGO := GetSXENum("AGA", "AGA_CODIGO")
			AGA->AGA_CODENT := cCodSU5
			AGA->AGA_ENTIDA := "SU5"
			AGA->AGA_END    := Substr(cEndereco, 1, TamSX3("AGA_END")[1])
			AGA->AGA_PADRAO := "1"
			AGA->AGA_TIPO   := "1"
			
			AGA->(MsUnlock())
			AGA->(DbCommit())
		EndIf

		Iif(!Empty(cTelefone), aAdd(aAGB, {cTelefone, "1"}), "")
		Iif(!Empty(cFax), aAdd(aAGB, {cFax, "3"}), "")

		For nJ := 1 To Len(aAGB)
			RecLock("AGB", .T.)
			AGB->AGB_CODIGO := GetSXENum("AGB", "AGB_CODIGO")
			AGB->AGB_CODENT := cCodSU5
			AGB->AGB_ENTIDA := "SU5"
			AGB->AGB_DDI    := cDDI
			AGB->AGB_DDD    := cDDD
			AGB->AGB_TELEFO := Substr(aAGB[nJ][1], 1, TamSX3("AGB_TELEFO")[1])
			AGB->AGB_PADRAO := "1"
			AGB->AGB_TIPO   := aAGB[nJ][2]
			
			AGB->(MsUnlock())
			AGB->(DbCommit())
		Next nJ

		JurFreeArr(aAGB)
		cCodTab := cCodClien + "|" + cCodLoja

		MigraDePar(cTabela, cCodTab, "", cCpoIdenti, "SU5", cCodSU5, "U5_CODCONT")
	Next nI

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} CpoClob
Fun��o para converter Clog para Varchar no oracle

@param cCpoSisJur, Campo do SisJuri

@Return cRet, Texto para informar na query para converter o campo

@author  Bruno Ritter
@since   11/09/2019
/*/
//-------------------------------------------------------------------
Static Function CpoClob(cCpoSisJur)
	Local cRet := "dbms_lob.substr( " + cCpoSisJur + ", 4000, 1 )"

Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} CpoRound
Fun��o para fazer um round do campo pelo banco de dados

@param cCpoSisJur, Campo do SisJuri
@param cCpoProthe, Campo do Protheus

@Return cRet, Texto para informar na query para converter o campo

@author  Bruno Ritter
@since   11/09/2019
/*/
//-------------------------------------------------------------------
Static Function CpoRound(cCpoSisJur, cCpoProthe)
	Local cRet := "Round(" + cCpoSisJur + ", " + cValToChar(TamSX3(cCpoProthe)[2]) + ")"

Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} BuscaEndPad
Fun��o para buscar o endere�o / loja padr�o do cliente. 

@param cCodCliente - C�digo do Cliente para busca do Endere�o Padr�o
@param nConnSisJr  - N�mero para conex�o Sisjuri
@param nConnProth  - Conex�o do Protheus

@Return cRet       - Endere�o Padr�o do Cliente

@author Cristina Cintra
@since  12/09/2019
/*/
//-------------------------------------------------------------------
Static Function BuscaEndPad(cSisValor, nConnSisJr, nConnProth)
Local cRet    := ""
Local cQuery  := "SELECT COD_CLIENTE||'|'||END_PADRAO CLI_END FROM RCR.CLIENTE WHERE COD_CLIENTE = " + cSisValor + " " 
Local aRetSql := {}

TCSetConn(nConnSisJr) // SISJURI
aRetSql := JurSQL(cQuery, "CLI_END")

If !Empty(aRetSql) .And. !Empty(aRetSql[1][1])
	cRet := Substr(GetDePara("RCR.ENDERECO", aRetSql[1][1], ""), TamSX3("A1_COD")[1] + 1, TamSX3("A1_LOJA")[1])
EndIf

TCSetConn(nConnProth) // Protheus

Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} CaseCliAtv
Case para verificar se o cliente/loja � ativo ou inativo

@author Bruno Ritter
@since  13/09/2019
/*/
//-------------------------------------------------------------------
Static Function CaseCliAtv()
	Local cCase := ""

	cCase := " CASE WHEN CLIENTE.CLICSITUACAO = 'A' THEN '1' " // Cliente ativo
	cCase +=      " WHEN ENDERECO.ENDCSITUACAO = 'A' THEN '1' " // Endere�o ativo
	cCase +=      " ELSE '2' "
	cCase += " END "

Return cCase

//-------------------------------------------------------------------
/*/{Protheus.doc} CaseCliSit
Case para verificar se o cliente/loja � provis�rio ou definitivo

@author Bruno Ritter
@since  13/09/2019
/*/
//-------------------------------------------------------------------
Static Function CaseCliSit()
	Local cCase := ""

	cCase := " CASE WHEN CLIENTE.CLICSITUACAOCADASTRO = 'P' THEN '1' " // Cliente provis�rio
	cCase +=      " WHEN ENDERECO.ENDCSITUACAO = 'P' THEN '1' " // Endere�o provis�rio
	cCase +=      " ELSE '2' "
	cCase += " END "

Return cCase

//-------------------------------------------------------------------
/*/{Protheus.doc} ContrMae
Fun��o para buscar o Caso, cliente e loja contrado mae  

@author Leonardo Miranda
@since  06/11/2019
/*/
//-------------------------------------------------------------------

Static Function ContrMae(cSisValor,nRetorno)

 Local cQuery   := ""
 Local xAlias   := GetNextAlias()
 Local cRetorno := ""
 cSisValor := StrTran(alltrim(cSisValor),' ','')
 
 cQuery := ""
 cQuery += " SELECT	NT0.NT0_FILIAL					,                                                            "+(Chr(13)+Chr(10))
 cQuery += " 		NT0.NT0_COD						,                                                            "+(Chr(13)+Chr(10))
 cQuery += " 		NT0.NT0_NOME					,                                                            "+(Chr(13)+Chr(10))
 cQuery += " 		NT0.NT0_CCLIEN					,                                                            "+(Chr(13)+Chr(10))
 cQuery += " 		NT0.NT0_CLOJA					,                                                            "+(Chr(13)+Chr(10))
 cQuery += " 		NT0.NT0_CCASCM					,                                                            "+(Chr(13)+Chr(10))
 cQuery += " 		NT0.NT0_CCLICM					,                                                            "+(Chr(13)+Chr(10))
 cQuery += " 		NT0.NT0_CLOJCM                  ,                                                            "+(Chr(13)+Chr(10))
 cQuery += " 		NT0.NT0_CPART1                                                                               "+(Chr(13)+Chr(10))
 cQuery += " FROM "+RetSqlName("OHS")+" OHS                                                                      "+(Chr(13)+Chr(10)) 
 cQuery += " 		INNER JOIN	                                                                                 "+(Chr(13)+Chr(10))
 cQuery += " 	  "+RetSqlName("NVE")+" NVE  ON	NVE.NVE_CCLIEN+NVE.NVE_LCLIEN+NVE.NVE_NUMCAS	= OHS.OHS_CODPRO "+(Chr(13)+Chr(10))
 cQuery += " 								AND	''												= OHS.D_E_L_E_T_ "+(Chr(13)+Chr(10))
 cQuery += " 		INNER JOIN                                                                                   "+(Chr(13)+Chr(10))
 cQuery += " 	  "+RetSqlName("OHS")+" XOHS ON	'SA1'											= XOHS.OHS_ENTPRO"+(Chr(13)+Chr(10))
 cQuery += " 								AND	NVE.NVE_CCLIEN+NVE.NVE_LCLIEN					= XOHS.OHS_CODPRO"+(Chr(13)+Chr(10))
 cQuery += " 								AND	''												= XOHS.D_E_L_E_T_"+(Chr(13)+Chr(10))
 cQuery += " 		INNER JOIN                                                                                   "+(Chr(13)+Chr(10))
 cQuery += " 	  "+RetSqlName("NUT")+" NUT	ON	NVE.NVE_CCLIEN									= NUT.NUT_CCLIEN "+(Chr(13)+Chr(10))
 cQuery += " 								AND	NVE.NVE_LCLIEN									= NUT.NUT_CLOJA  "+(Chr(13)+Chr(10))
 cQuery += " 								AND	NVE.NVE_NUMCAS									= NUT.NUT_CCASO  "+(Chr(13)+Chr(10))
 cQuery += " 								AND	''												= NUT.D_E_L_E_T_ "+(Chr(13)+Chr(10))
 cQuery += " 		INNER JOIN                                                                                   "+(Chr(13)+Chr(10))
 cQuery += " 	  "+RetSqlName("NT0")+" NT0	ON	NUT.NUT_CCONTR									= NT0.NT0_COD"   +(Chr(13)+Chr(10))
 cQuery += " 								AND	NUT.NUT_CCLIEN									= NT0.NT0_CCLIEN "+(Chr(13)+Chr(10))
 cQuery += " 								AND	NUT.NUT_CLOJA									= NT0.NT0_CLOJA  "+(Chr(13)+Chr(10))
 cQuery += " 								AND	''												= NT0.D_E_L_E_T_ "+(Chr(13)+Chr(10))
 cQuery += " WHERE	OHS.OHS_ENTPRO = 'NVE'                                                                       "+(Chr(13)+Chr(10)) 
 cQuery += " 	AND OHS.OHS_CODSIS = "+cSisValor+"                                                               "+(Chr(13)+Chr(10)) 
 cQuery += " 	AND OHS.D_E_L_E_T_ = ''                                                                          "+(Chr(13)+Chr(10))
 
 If Select(xAlias) <> 0 ; (xAlias)->(DbCloseArea()) ; EndIf 
 DbUseArea(.T., "TOPCONN", TcGenQry(,,cQuery), xAlias, .T., .T.)
(xAlias)->(DbGoTop())
 If (xAlias)->(!Eof())
    If nRetorno == 1
       cRetorno := (xAlias)->NT0_CCLICM
    ElseIf nRetorno == 2
       cRetorno := (xAlias)->NT0_CLOJCM 
    ElseIf nRetorno == 3 
       cRetorno := (xAlias)->NT0_CCASCM
    ElseIf nRetorno == 4       
       cRetorno := (xAlias)->NT0_CCLIEN
    ElseIf nRetorno == 5
       cRetorno := (xAlias)->NT0_CLOJA
    ElseIf nRetorno == 6
       cRetorno := (xAlias)->NT0_CPART1               
   EndIf
 EndIf
 If Select(xAlias) <> 0 ; (xAlias)->(DbCloseArea()) ; EndIf
 
 Return(cRetorno)

 
 //-------------------------------------------------------------------
/*/{Protheus.doc} AdtCriaTit
Cria os t�tulos de RA dos adiantamentos

@param oProcess  , Objeto do processa

@author Bruno Ritter
@since  04/12/2019
/*/
//-------------------------------------------------------------------
Static Function AdtCriaTit(oProcess)
	Local dDataOri   := dDataBase 
	Local cTabProthe := "NWF x RA"
	Local nQtdeNWF   := NWF->(LASTREC())
	Local dTimeInI   := Time()
	Local lCliBloq   := .F.
	Local nQtde      := 0

	Conout("Tabela em processamento: " + cTabProthe + " - Inicio: " + JurTimeStamp(1))
	
	oProcess:IncRegua1("Criando t�tulos de RA referente aos adiantamentos")

	NWF->(DbSetOrder(1)) //NWF_FILIAL, NWF_COD
	NWF->(DbGoTop())

	oProcess:SetRegua2(nQtdeNWF)

	While NWF->(!Eof())
		nQtde := nQtde + 1
		oProcess:IncRegua2(i18n("Criando t�tulos #1 de #2", {nQtde,nQtdeNWF}))

		If !Empty(NWF->NWF_BANCO) .And. !Empty(NWF->NWF_AGENCI) .And. !Empty(NWF->NWF_CONTA)
			If JurGetDados('SA1', 1, NWF->NWF_FILIAL+NWF->NWF_CCLIAD+NWF->NWF_CLOJAD, "A1_MSBLQL") == "1" // Cliente bloqueado 
				lCliBloq   := .T.
				SetValue('SA1', NWF->NWF_FILIAL+NWF->NWF_CCLIAD+NWF->NWF_CLOJAD, "A1_MSBLQL", '2', 1)
			EndIf

			dDataBase := NWF->NWF_DTMOVI 
			JA069FIN()
			
			If lCliBloq
				lCliBloq   := .F.
				SetValue('SA1', NWF->NWF_FILIAL+NWF->NWF_CCLIAD+NWF->NWF_CLOJAD, "A1_MSBLQL", '1', 1)
			EndIf

		EndIf

		NWF->(DbSkip())
	EndDo
	
	dDataBase := dDataOri

	Conout("Tabela em processamento: " + cTabProthe + " - Fim   : " + JurTimeStamp(1))
	Conout("Duracao: " + cTabProthe + " --> " + ElapTime( dTimeInI , Time()))

Return Nil
 

//-------------------------------------------------------------------
/*/{Protheus.doc} AdtBaixa
Efetua a devolu��o dos valores dos t�tulos de RA

@param oProcess  , Objeto do processa

@author Leonardo Miranda
@since  05/12/2019
/*/
//-------------------------------------------------------------------
 
Static Function AdtBaixa(oProcess)
Local cTabProthe := "NWF"
Local aEntidades := {}
Local cCondicNWF := "RCR.ADIANTAMENTO.ADICSTATUS = 'A' AND ADICOPERACAO = 'D'"
Local aJoinNWF   := {} 
Local aDePara    := {}
Local cQuery     := ""
Local nI         := 1
Local nX         := 1
Local nTotDePara := 0 
Local nPosDoc    := 0
Local nPosData   := 0
Local nPosValor  := 0
Local cParcela   := CriaVar("E1_PARCELA")
Local dDtMov     := CriaVar("E1_BAIXA")
Local nValorBx   := CriaVar("E5_VALOR")
Local aBaixa     := {}
Local aRegs      := {}
Local cFil       := ""
Local cFilSE1    := ""
Local dDataOri   := dDataBase 
Local cFilOri    := cFilAnt
Local dTimeInI   := Time()
 
Private lMsErroAuto := .F.
Private lMsHelpAuto := .T.

Conout("Tabela em processamento: " + cTabProthe + " - Inicio: " + JurTimeStamp(1))
oProcess:IncRegua1("Realizando estornos dos adiantamentos")

aDePara    := MontaArray(cTabProthe)
nTotDePara := Len(aDePara)
nPosDoc    := aScan(aDePara, {|X| Upper(Alltrim(X[1])) == "RCR.ADIANTAMENTO.ADINDOCUTIL"})
nPosData   := aScan(aDePara, {|X| Upper(Alltrim(X[2])) == "NWF_DTMOVI"                  })
nPosValor  := aScan(aDePara, {|X| Upper(Alltrim(X[1])) == "RCR.ADIANTAMENTO.ADINVALOR"  })
aAdd(aJoinNWF, {"LEFT", "RCR.MOEDA"         , {{"RCR.ADIANTAMENTO.ADICMOEDA"            , "RCR.MOEDA.CODIGO"                                                                                     }}})
aAdd(aJoinNWF, {"LEFT", "FINANCE.LANCAMENTO", {{"RCR.ADIANTAMENTO.ADINLANCAMENTO"       , "FINANCE.LANCAMENTO.LANNCODIG"                                                                         }}})
aAdd(aJoinNWF, {"LEFT", "RCR.CONTAS"        , {{"FINANCE.LANCAMENTO.PCTCNUMEROCONTADEST", "RCR.CONTAS.PCTCNUMEROCONTA"                                                                           }}})
aAdd(aJoinNWF, {"LEFT", "RCR.PASTA"         , {{"RCR.ADIANTAMENTO.ADINPASTA"            , "RCR.PASTA.PASTA"              },{"RCR.ADIANTAMENTO.ADINCLIENTE"          , "RCR.PASTA.COD_CLIENTE"    }}})
aAdd(aJoinNWF, {"LEFT", "RCR.ESCRITORIO"    , {{"RCR.ADIANTAMENTO.ADINORGANIZACAO"      , "RCR.ESCRITORIO.ORGNCODIG"                                                                             }}})
aAdd(aJoinNWF, {"LEFT", "RCR.ENDERECO"      , {{"RCR.ADIANTAMENTO.ADINCLIENTE"          , "RCR.ENDERECO.COD_CLIENTE"     },{"RCR.PASTA.COD_ENDERECO"                , "RCR.ENDERECO.COD_ENDERECO"}}})
aAdd(aJoinNWF, {"LEFT", "RCR.CLIENTE"       , {{"RCR.ADIANTAMENTO.ADINCLIENTE"          , "RCR.CLIENTE.COD_CLIENTE"                                                                              }}})

aAdd( aEntidades, { 'RCR.ADIANTAMENTO'    , 'NWF'    , cCondicNWF , aJoinNWF , ''      } ) // Controle de Adiantamentos 
TCSetConn(nConnSisJr) // SISJURI
cQuery := QryOrigem(aEntidades[01], aDePara) // Query na Origem para pegar os registros a serem gravados
	
aRegs  := JurSQL(cQuery, "*")	
TCSetConn(nConnProth) // PROTHEUS
If Len(aRegs) > 0
	oProcess:SetRegua2(Len(aRegs))
	For nI := 1 To Len(aRegs)
		//For nX := 1 To Len(aRegs[nI])
		oProcess:IncRegua2(i18n("Estornando registro #1 de #2", {nI, Len(aRegs)}))
		OHS->(DbSetOrder(1)) // OHS_FILIAL, OHS_ENTSIS, OHS_CODSIS, OHS_FILSIS
		If (OHS->(DbSeek(_cFilOHS +AvKey('RCR.ADIANTAMENTO'            , "OHS_ENTSIS")+; 
										AvKey(cValToChar(aRegs[nI][nPosDoc]), "OHS_CODSIS")+;
										AvKey(cValToChar('')                , "OHS_FILSIS"))))

			dDtMov := Stod(aRegs[nI][nPosData])
			nValorBx := aRegs[nI,nPosValor] 
			NWF->(DbSetOrder(1))
			If NWF->(DbSeek(xFilial("NFW")+Alltrim(OHS->OHS_CODPRO))) .And. NWF->NWF_TITGER == "1"
				cFil      := Posicione("NS7",1,xFilial("NS7") + NWF->NWF_CESCR , "NS7_CFILIA")
				cFilSE1   := xFilial("SE1")
				cFilAnt   := cFilSE1
				SE1->(DbSetOrder(1))
				If SE1->(DbSeek(cFilSE1+"NWF"+NWF->NWF_TITULO+cParcela+"RA "))

					If dDtMov < SE1->E1_EMISSAO // Prote��o, pois haviam registros no Sisjuri com a data de estorno menor que a data do adiantamento
						dDtMov := SE1->E1_EMISSAO
					EndIf
					dDataBase := dDtMov
			  
					aBaixa := {{"E1_PREFIXO"  ,SE1->E1_PREFIXO             ,Nil },;
                               {"E1_NUM"      ,SE1->E1_NUM                 ,Nil },;
                               {"E1_TIPO"     ,SE1->E1_TIPO                ,Nil },;
                               {"E1_CLIENTE"  ,SE1->E1_CLIENTE             ,Nil },;
                               {"E1_LOJA"     ,SE1->E1_LOJA                ,Nil },;
                               {"E1_NATUREZ"  ,SE1->E1_NATUREZ             ,Nil },;
                               {"E1_PARCELA"  ,SE1->E1_PARCELA             ,Nil },;
                               {"AUTMOTBX"    ,"NOR"                       ,Nil },;
                               {"CBANCO"      ,SE1->E1_PORTADO             ,Nil },;
                               {"CAGENCIA"    ,SE1->E1_AGEDEP              ,Nil },;
                               {"CCONTA"      ,SE1->E1_CONTA               ,Nil },;
                               {"AUTDTBAIXA"  ,dDtMov                      ,Nil },;
                               {"AUTDTCREDITO",dDtMov                      ,Nil },;
                               {"AUTHIST"     ,"Estorno de Adiantamento"   ,Nil },;
                               {"AUTJUROS"    ,0                           ,Nil },;
                               {"AUTVALREC"   ,nValorBx                    ,Nil }}
        
					MSExecAuto({|x,y,b,a| Fina070(x,y,b,a)},aBaixa,3,.F.,5) //3 - Baixa de T�tulo, 5 - Cancelamento de baixa, 6 - Exclus�o de Baixa.
					If lMsErroAuto
						Conout("_______________________________________________________")
						Conout("DATABASE: " + DTOS(dDataBase))
						Conout("DATA MOV: " + DTOS(dDtMov)) 
						Conout("NWF_COD: " + NWF->NWF_COD)
						ConOut("SE1_NUM: " + E1_NUM)
						Conout("OHS_CODSIS: " + OHS->OHS_CODSIS)
						Conout("OHS_CODPRO: " + OHS->OHS_CODPRO)
						
						MostraErro()
					Else
						ConOut("BAIXADO COM SUCESSO!" + E1_NUM)
					EndIf		         
				EndIf
			EndIf
		EndIf           
		//Next
	Next
	Conout("Tabela em processamento: " + cTabProthe + " - Fim   : " + JurTimeStamp(1))
	Conout("Duracao: " + cTabProthe + " --> " + ElapTime( dTimeInI , Time()))
EndIf

cFilAnt   := cFilOri 
dDataBase := dDataOri

Return() 

//-------------------------------------------------------------------
/*/{Protheus.doc} CodSeq
Gera a sequencia da NX2,NXD

@nparam 

@author Leonardo Miranda
@since  05/12/2019
/*/
//-------------------------------------------------------------------

Static Function CodSeq(nParam)

Local nSeq      := 0
Local aArea     := GetArea()
Local bWhile    := {}

Local aNx2Area  := NX2->(GetArea())
Local cPreftNx2 := NX2->NX2_CPREFT
Local cPartNx2  := NX2->NX2_CPART
Local cClienNx2 := NX2->NX2_CCLIEN 
Local cLojaNx2  := NX2->NX2_CLOJA 
Local cContrNx2 := NX2->NX2_CCONTR
Local cCasoNx2  := NX2->NX2_CCASO

Local aNxdArea  := NXD->(GetArea())
Local cFatNxd   := NXD->NXD_CFATUR  
Local cEscNxd   := NXD->NXD_CESCR 
Local cCliNxd   := NXD->NXD_CCLIEN
Local cLojNxd   := NXD->NXD_CLOJA 
Local cCasNxd   := NXD->NXD_CCASO 
Local cPartNxd  := NXD->NXD_CPART
Local cContNxd  := NXD->NXD_CCONTR

If nParam == 1
   NX2->(DbSetOrder(1))
   NX2->(DbSeek(xFilial("NX2")+cPreftNx2+cPartNx2+cClienNx2+cLojaNx2+cContrNx2+cCasoNx2))
   bWhile  := {|| !NX2->(Eof()) .And. NX2->NX2_CPREFT == cPreftNx2 .And. NX2->NX2_CPART  == cPartNx2  .And. ;
                                      NX2->NX2_CCLIEN == cClienNx2 .And. NX2->NX2_CLOJA  == cLojaNx2  .And. ;  
                                      NX2->NX2_CCONTR == cContrNx2 .And. NX2->NX2_CCASO  == cCasoNx2 }
   (NX2->(DbEval({|| nSeq ++ },,bWhile,,,.T.)) )
 ElseIf nParam == 2 
   NXD->(DbSetOrder(1))
   NXD->(DbSeek(xFilial("NXD")+cFatNxd+cEscNxd+cCliNxd+cLojNxd+cCasNxd+cContNxd+cPartNxd))
   bWhile  := {|| !NXD->(Eof()) .And. NXD->NXD_CFATUR == cFatNxd .And. NXD->NXD_CESCR  == cEscNxd  .And. ;
                                      NXD->NXD_CCLIEN == cCliNxd .And. NXD->NXD_CLOJA  == cLojNxd  .And. ;  
                                      NXD->NXD_CCASO  == cCasNxd .And. NXD->NXD_CCONTR == cContNxd .And. ;
                                      NXD->NXD_CPART  == cPartNxd }
   (NXD->(DbEval({|| nSeq ++ },,bWhile,,,.T.)) )
 
 EndIf
 
 If(nParam == 1 , RestArea(aNx2Area) ,RestArea(aNxdArea)) 
 RestArea(aArea)
 
 Return(StrZero(nSeq,6))

//-------------------------------------------------------------------
/*/{Protheus.doc} ContratCaso
Busca o contrado do caso

@author Leonardo Miranda
@since  05/12/2019
/*/
//-------------------------------------------------------------------

Static Function ContratCaso()
 
Local aArea    := Getarea()
Local xAlias   := GetNextAlias()
Local cRetorno := ""
Local cQuery   := ""
Local cClien   := NXD->NXD_CCLIEN 
Local cLoja    := NXD->NXD_CLOJA
Local cCaso    := NXD->NXD_CCASO

cQuery += " 
cQuery += "  SELECT	NUT.NUT_FILIAL				,                                            "
cQuery += " 		NUT.NUT_CCONTR				,                                            "
cQuery += " 		NUT.NUT_CCLIEN				,                                            "
cQuery += " 		NUT.NUT_CLOJA				,                                            "
cQuery += " 		NUT.NUT_CCASO                                                            "
cQuery += " FROM "+RetSqlName("NUT")+"	NUT                                                  "
cQuery += " 		INNER JOIN                                                               "
cQuery += " 	 "+RetSqlName("NT0")+"  NT0	ON '"+xFilial("NUT")+"' = NUT.NUT_FILIAL         "
cQuery += " 							AND	NUT.NUT_CCLIEN	        = NT0.NT0_CCLIEN         "
cQuery += " 							AND	NUT.NUT_CLOJA		    = NT0.NT0_CLOJA          "
cQuery += " 							AND	NUT.NUT_CCONTR		    = NT0.NT0_COD            "
cQuery += " 							AND	NUT.D_E_L_E_T_		    = NT0.D_E_L_E_T_         "
cQuery += " WHERE	NUT.D_E_L_E_T_	<> '*'                                                   "
cQuery += " 	AND	NUT.NUT_FILIAL  = '"+xFilial("NUT")+"'                                   "
cQuery += " 	AND	NUT.NUT_CCLIEN  = '"+cClien        +"'                                   "
cQuery += " 	AND	NUT.NUT_CLOJA   = '"+cLoja         +"'                                   "
cQuery += " 	AND	NUT.NUT_CCASO   = '"+cCaso         +"'                                   "
cQuery += " ORDER BY NUT.NUT_FILIAL,NUT.NUT_CCONTR,NUT.NUT_CCLIEN,NUT.NUT_CLOJA,NUT.NUT_CCASO"

If Select(xAlias) <> 0 ; (xAlias)->(DbCloseArea()) ; EndIf
DbUseArea(.T., "TOPCONN", TcGenQry(,,cQuery), xAlias, .T., .T.)

cRetorno := If((xAlias)->(!Eof()) , (xAlias)->NUT_CCASO ,"")
If Select(xAlias) <> 0 ; (xAlias)->(DbCloseArea()) ; EndIf
RestArea(aArea)

Return(cRetorno)

//-------------------------------------------------------------------
/*/{Protheus.doc} EncaFatAdc
Gera o encaminhamento do pagador da Fatura adicional

@param oProcess  , Objeto do processa
@param nConnSisJr, N�mero para conex�o Sisjuri
@param nConnProth, Conex�o do Protheus

@author Bruno Ritter
@since  05/02/2020
/*/
//-------------------------------------------------------------------
Static Function EncaFatAdc(oProcess, nConnSisJr, nConnProth)
	Local cQuery     := "SELECT FADNCLIENTE, FADNCASOFAT, FADNPARCELA FROM RCR.FATURAADICIONAL"
	Local cCodFatAdc := ""
	Local aRetSql    := {}
	Local aCliLoj    := {}
	Local nI         := 1
	Local nParcela   := 0
	Local nClienSis  := 0
	Local nCaso      := 0

	oProcess:IncRegua1("Criando encaminhamentos das faturas adicionais")
	oProcess:SetRegua2(0)

	TCSetConn(nConnSisJr) // SISJURI
	aRetSql := JurSQL(cQuery, "*")

	oProcess:IncRegua2("")
	TCSetConn(nConnProth) // Protheus
	For nI:= 1 to Len(aRetSql)
		nClienSis  := aRetSql[nI][1]
		nCaso      := aRetSql[nI][2]
		nParcela   := aRetSql[nI][3]
		cCodFatAdc := AllTrim(GetDePara('RCR.FATURAADICIONAL', cValToChar(nClienSis)+"|"+cValToChar(nParcela), ""))

		If !Empty(cCodFatAdc)
			aCliLoj := JurGetDados("NVV", 1, xfilial("NVV") + cCodFatAdc, {"NVV_CCLIEN", "NVV_CLOJA"})

			If !Empty(aCliLoj) .And. Len(aCliLoj) >= 2
				CasEncamin(nClienSis, nCaso, aCliLoj[1], aCliLoj[2], Nil, cCodFatAdc)
			EndIf
		EndIf
	Next nI

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} CasEncamin
Migra os Encaminhamentos do caso

@param nCliPasta , Cliente da pasta que o encaminhamento est� cadastrado no Sisjuri (formato Sisjuri)
@param nCasPasta , Caso da pasta que o encaminhamento est� cadastrado no Sisjuri (formato Sisjuri)
@param cClienPag , C�digo do Cliente pagador (formato Protheus)
@param cLojaPag  , Loja do cliente pagador (formato Protheus)
@param cCodContr , C�digo do Contrato (formato Protheus)
@param cFatAdicio, C�digo da Fatura adicional (formato Protheus)

@author Bruno Ritter
@since  23/09/2019
/*/
//-------------------------------------------------------------------
Function CasEncamin(cCliPasta, cCasPasta, cClienPag, cLojaPag, cCodContr, cFatAdicio)
	Local cQuery     := ""
	Local cContato   := ""
	Local cCliLoja   := ""
	Local nI         := 0
	Local aRetSql    := {}
	Local cSeq       := StrZero(0, TamSX3("NVN_COD")[1])

	Default cCodContr := ''
	Default cFatAdicio := ''

	cQuery := " SELECT "
	cQuery +=      " CFANCODCLIENTE, CFANCODENDERECO, CFACDESCRICAO "
	cQuery += " FROM RCR.COPIAFATURA "
	cQuery += " WHERE CFANCODCLIENTE = " + cValToChar(cCliPasta)
	cQuery += "   AND CFANCODPASTA = " + cValToChar(cCasPasta)

	TCSetConn(nConnSisJr) // SISJURI
	aRetSql := JurSQL(cQuery, {"CFANCODCLIENTE", "CFANCODENDERECO", "CFACDESCRICAO"},.F.,,.T.)

	TCSetConn(nConnProth) // Protheus
	For nI := 1 To Len(aRetSql)
		cCliLoja := cValToChar(aRetSql[nI][1]) + '|' + cValToChar(aRetSql[nI][2]) // CFANCODCLIENTE | CFANCODENDERECO
		cContato := Substr(GetDePara("RCR.ENDERECO-(CONTATOS)", cCliLoja, ""), 1, TamSX3("U5_CODCONT")[1])
		cSeq     := Soma1(cSeq)

		RecLock("NVN", .T.)
		NVN->NVN_CJCONT := ""
		NVN->NVN_CCONTR := cCodContr
		NVN->NVN_CLIPG  := cClienPag
		NVN->NVN_LOJPG  := cLojaPag
		NVN->NVN_CCONT  := cContato
		NVN->NVN_CFATAD := cFatAdicio
		NVN->NVN_COD    := cSeq
		NVN->NVN_DESC   := SubStr(aRetSql[nI][3], 1, TamSX3("NVN_DESC")[1]) // CFACDESCRICAO
		NVN->NVN_CPREFT := ""
		NVN->NVN_CFIXO  := ""
		NVN->NVN_CFILA  := ""
		NVN->NVN_CFATUR := ""
		NVN->NVN_CESCR  := ""
		NVN->(MsUnlock())
	Next nI

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} GetNUEVlrH
Retorna o valor hora do time sheet

@Return nValorHora

@author  Bruno Ritter
@since   12/02/2020
/*/
//-------------------------------------------------------------------
Static Function GetNUEVlrH()
	Local nValorHora := 0

	If !Empty(NUE->NUE_VALOR) .And. !Empty(NUE->NUE_TEMPOR)
		nValorHora := NUE->NUE_VALOR / NUE->NUE_TEMPOR
	EndIf

Return nValorHora

//-------------------------------------------------------------------
/*/{Protheus.doc} MigraCtb
Migra tabelas da contabilidade

@param oProcess  , Objeto do processa
@param nConnSisJr, Conex�o do SisJuri
@param nConnProth, Conex�o do Protheus

@author  Leoanrdo Miranda
@since   20/02/2020
/*/
//-------------------------------------------------------------------
Static Function MigraCtb(oProcess, nConnSisJr, nConnProth)

Local aArea      := GetArea()
Local nRegistros := 0
Local nI         := 0
Local cFilCTE	 := xFilial("CTE")

oProcess:IncRegua1("Contabilidade")

Chkfile("CTG") ; CTG->(DbSetOrder(1)) 
Chkfile("CTE") ; CTE->(DbSetOrder(1))
Chkfile("CTO") ; CTO->(DbSetOrder(1))

nRegistros := CTO->(RecCount())
oProcess:SetRegua2(nRegistros)

CTO->(DBGoTop())
For nI := 1 To nRegistros
   oProcess:IncRegua2(i18n("Importando registro #1 de #2", {nI,nRegistros}))
   CTG->(DBGoTop())
   While CTG->(!Eof())
     If CTE->(!DbSeek(cFilCTE+CTO->CTO_MOEDA+CTG->CTG_CALEND))
	    CTE->(RecLock("CTE",.T.))
		CTE->CTE_FILIAL := cFilCTE
		CTE->CTE_MOEDA  := CTO->CTO_MOEDA
		CTE->CTE_CALEND := CTG->CTG_CALEND
		CTE->(MsUnlock())
	 EndIf
     CTG->(DBSkip())
   End
   CTO->(DBSkip())
Next

RestArea(aArea)

Return()

//-------------------------------------------------------------------
/*/{Protheus.doc} CriaRD0MIG
Criar o participante MIGRADOR na RD0 relacionado ao usu�rio ADMIN, pois algumas rotinas precisam de um participante vinculado ao usu�rio.

@author  Jacques Alves Xavier
@since   09/07/2020
/*/
//-------------------------------------------------------------------
Static Function CriaRD0MIG()
	Local aArea      := GetArea()
	Local aAreaRD0   := RD0->(GetArea())
	Local aAreaNUR   := NUR->(GetArea())
	Local cCodRD0    := CriaVar("RD0_CODIGO", .T.)

	RecLock("RD0", .T.)
	RD0->RD0_CODIGO := cCodRD0
	RD0->RD0_NOME   := 'MIGRADOR'
	RD0->RD0_TIPO   := '1'
	RD0->RD0_USER   := '000000'
	RD0->RD0_TPJUR  := '1'
	RD0->RD0_SIGLA  := 'MIG'
	RD0->RD0_MSBLQL := '2'
	RD0->(MsUnlock())
	RD0->(DbCommit())

	RecLock("NUR", .T.)
	NUR->NUR_CPART  := cCodRD0
	NUR->NUR_APELI  := 'MIG'
	NUR->NUR_HRDIAD := '0800'
	NUR->NUR_PARTDI := '2'
	NUR->NUR_APRTS  := '2'
	NUR->NUR_SOCIO  := '1'
	NUR->NUR_REVFAT := '1'
	NUR->NUR_CASOEN := '1'
	NUR->NUR_PERIOD := '2'
	NUR->NUR_PERINC := '2'
	NUR->NUR_PERALT := '2'
	NUR->NUR_PEREXC := '2'
	NUR->NUR_LCPRE  := '1'
	NUR->NUR_FORNAT := '1'
	NUR->NUR_RATDES := '2'
	NUR->NUR_TECNIC := '2'
	RD0->(MsUnlock())
	RD0->(DbCommit())

RestArea(aAreaNUR)
RestArea(aAreaRD0)
RestArea(aArea)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} ClearBuffReg
Limpar o array de buffer _aBuffReg utilizado para evitar processamento da fun��o POSICIONE

@author fabiana.silva
@since  24/07/2020
/*/
//-------------------------------------------------------------------
Static Function ClearBuffReg()

JurFreeArr(_aBuffReg)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} MontaQuery
Executa a query no banco de dados e retora a quantidade de registros
@param cSql  , Query do banco de dados
@param cTmp, Alias tabela tempor�rias
@param nRegs, Quantidade de registros

@author fabiana.silva
@since  28/07/2020
/*/
//-------------------------------------------------------------------
Static Function MontaQuery(cSql, cTmp, nRegs)
	Local cQry := ""

	Default nRegs := 0

	If !Empty(cSQL) 

		DbCommitAll() //Para efetivar a altera��o no banco de dados (n�o impacta no rollback da transa��o)
		cQry  := ChangeQuery(cSQL)

		dbUseArea( .T., "TOPCONN", TcGenQry( ,, cQry ), cTmp, .T., .F. )

		If !(cTmp)->(Eof())
			cQry    := " SELECT COUNT(1) TOTAL FROM ( " + cSQL + " ) "
			nRegs := JurSql(cQry,{"TOTAL"})[1][1]			
		EndIf
	EndIf

Return

//-------------------------------------------------------------------
Static Function CriaCTTSint()
	Local aArea     := GetArea()
	Local aAreaCTT  := CTT->(GetArea())
	Local cAlsSetor := GetNextAlias()
	Local cTamCusto := TamSX3("CTT_CUSTO")[1]
	Local cSigla    := ""
	
	TCSetConn(nConnSisJr) // SISJURI

	BeginSql ALIAS cAlsSetor 
		SELECT SIGLA, COUNT(*) 
		  FROM RCR.SETOR
		 WHERE TIPO = 'G' 
		 GROUP BY SIGLA 
		HAVING COUNT(*) > 1
	EndSql	

	While (cAlsSetor)->(! Eof())
		cSigla := (cAlsSetor)->SIGLA

		TCSetConn(nConnProth) // PROTHEUS
		RecLock("CTT", .T.)
		CTT->CTT_FILIAL := xFilial("CTT")
		CTT->CTT_CUSTO  := cSigla
		CTT->CTT_CLASSE := '1'
		CTT->CTT_DESC01 := cSigla + ' - GRUPO'
		CTT->CTT_BLOQ   := '2'
		CTT->CTT_DTEXIS := Date()
		CTT->CTT_ITOBRG := '2'
		CTT->CTT_CLOBRG := '2'
		CTT->CTT_ACITEM := '1'
		CTT->CTT_ACCLVL := '1'
		CTT->CTT_INTRES := '2'
		CTT->(MsUnLock())

		If OHS->(DbSeek(xFilial("OHS") + Padr('RCR.SETOR', _nTamEntSi) + Padr(cSigla, _nTamCodSi)))
			While OHS->(! Eof()) .And. AllTrim(OHS->OHS_CODSIS) == AllTrim(cSigla)
				If CTT->(DbSeek(xFilial("CTT") + Padr(OHS->OHS_CODPRO, cTamCusto)))
		 			RecLock("CTT", .F.)
					CTT->CTT_CCSUP := cSigla
					CTT->(MsUnLock())
		 		EndIf
				OHS->(DbSkip())
			End
		EndIf

		 TCSetConn(nConnSisJr) // SISJURI
		(cAlsSetor)->(DbSkip())
	End

	RestArea(aAreaCTT)
	RestArea(aArea)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} GetOHBMOEC
Retorna a moeda de convers�o do Lan�amento

@author Jacques Alves Xavier
@since  10/08/2020
/*/
//-------------------------------------------------------------------
Static Function GetOHBMOEC()
	Local cMoeLan := ""
	Local cMoeOri := ""
	Local cMoeDes := ""
	Local cRet    := ""

	cMoeLan := OHB->OHB_CMOELC
	CMoeOri := JurGetDados("SED", 1, xFilial("SED") + OHB->OHB_NATORI, "ED_CMOEJUR")

	If cMoeLan <> CMoeORi
		cRet := CMoeORi
	Else
		cMoeDes := JurGetDados("SED", 1, xFilial("SED") + OHB->OHB_NATDES, "ED_CMOEJUR")
		If cMoeLan <> cMoeDes
			cRet := cMoeDes
		Else
			cRet := cMoeLan
		EndIf
	EndIf

Return cRet