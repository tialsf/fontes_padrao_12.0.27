#INCLUDE 'PROTHEUS.CH'

Static oHashCpo   := GetHashMap()
Static nTamEntSis := TamSX3("OHS_ENTSIS")[1]
Static nTamCodSis := TamSX3("OHS_CODSIS")[1]
Static nTamFilSis := TamSX3("OHS_FILSIS")[1]

//-------------------------------------------------------------------
/*/{Protheus.doc} MigraCReceb
Migra o contas a receber

@param oProcess  , Objeto do processa
@param nConnSisJr, Conex�o do SisJuri
@param nConnProth, Conex�o do Protheus

@author Leonardo Miranda
@since  25/09/2019
/*/
//-------------------------------------------------------------------
User Function MigraCReceb(oProcess, nConnSisJr, nConnProth)
	Local cQuery     := ""
	Local aRetSql    := {}
    Local aReg       := {}
	Local nTotTitulos:= 0
	Local nI         := 0
    Local aTitulo    := {}
	Local aBaixa	 := {}
    Local cPrefixo   := GetMv("MV_JPREFAT") 
    LOcal cTipo      := GetMv("MV_JTIPFAT") 
    Local cNatureza  := GetMv("MV_JNATFAT") 
    Local cBanco     := ""
    Local cAgencia   := "" 
    Local cConta     := ""
    Local cMoeda     := ""
    Local CCliente   := ""
    Local cLoja      := ""
    Local cCliSJur   := ""
    Local cEscrit    := ""
    Local cFatJur    := ""
	Local cDataReceb := ""
	Local cHistBaixa := "VALOR RECEBIDO S/ TITULO"
    Local dDataOri   := dDataBase
	Local nVlrReceb  := 0
	Local bGetCdBank := {}
    Local bGetCdAgen := {}
    Local bGetCdCont := {}
    Local bGetMoeda  := {}
    Local bGetSA1Cli := {}
    Local bGetSA1Loj := {}
    Local bGetEscrt  := {}
	Local dTimeInI   := Time()	
	Local cTabProthe := "SE1"
	Local lCliBloq   := .F.

    Private lMsErroAuto := .F.
    Private lMsHelpAuto := .T.

	Conout("Tabela em processamento: " + cTabProthe + " - Inicio: " + JurTimeStamp(1))

	oProcess:IncRegua1("Contas a Receber")

	TCSetConn(nConnSisJr) // SISJURI

	cQuery := " SELECT                                                                                                                                  "
	cQuery += " FINANCE.CONTASRECEBER.orgncodig																orgncodig			,                       "
	cQuery += " NVL(LPAD(FINANCE.CONTASRECEBER.crccnumerodocumento,09,'0'),' ')						        crccnumerodocumento	,                       "
	cQuery += " FINANCE.CONTASRECEBER.crccparcela																				,                       "
	cQuery += " LPAD(TO_CHAR(ROW_NUMBER()OVER (PARTITION BY	FINANCE.CONTASRECEBER.crccnumerodocumento							,                       "
	cQuery += " 		                					FINANCE.CONTASRECEBER.crccparcela									,                       "
	cQuery += " 											FINANCE.CONTASRECEBER.crcncliente                                                           "
	cQuery += " 							   ORDER BY	FINANCE.CONTASRECEBER.crccnumerodocumento				    			,                       "
	cQuery += " 										FINANCE.CONTASRECEBER.crccparcela						    			,                       "
	cQuery += " 										FINANCE.CONTASRECEBER.crcncliente)),2,'0')	        parcela				,                       "
	cQuery += " FINANCE.CONTASRECEBER.crcncliente															crcncliente			,                       "
	cQuery += " RCR.FATURA.COD_ENDERECO																		cod_endereco		,                       "
	cQuery += " FINANCE.CONTASRECEBER.crccrazaosocial														crccrazaosocial		,                       "
	cQuery += " NVL(TO_CHAR(FINANCE.CONTASRECEBER.crcddata,'YYYYMMDD'),' ')	    							crcddata			,                       "
	cQuery += " NVL(TO_CHAR(FINANCE.CONTASRECEBER.crcdvecto,'YYYYMMDD'),' ')	    						crcdvecto			,                       "
	cQuery += " NVL(FINANCE.CONTASRECEBER.crcnvalorhonorario + FINANCE.CONTASRECEBER.crcnvalordespesa + FINANCE.CONTASRECEBER.crcnvalordespesatrib,0) crcnvalor,"
	cQuery += " NVL(FINANCE.CONTASRECEBER.crcnvalorir,0)													crcnvalorir			,                       "
	cQuery += " NVL(FINANCE.CONTASRECEBER.crcnvaloriss,0)													crcnvaloriss		,                       "                    
	cQuery += " FINANCE.CONTASRECEBER.crcchistorico															crcchistorico		,                       "
	cQuery += " FINANCE.CONTASRECEBER.codigolote															codigolote			,                       "
	cQuery += " NVL(FINANCE.CONTASRECEBER.crcndescrechon,0)+NVL(FINANCE.CONTASRECEBER.crcndescrecdes,0)		desconto			,                       "
	cQuery += " FINANCE.CONTASRECEBER.codigo																codigo				,                       "
	cQuery += " NVL(LPAD(FINANCE.CONTASRECEBER.crcnnotafiscal,09,'0'),' ')  								crcnnotafiscal		,                       "
	cQuery += " NVL(LPAD(FINANCE.CONTASRECEBER.empncod,04,'0'),' ')											empncod				,                       "
	cQuery += " NVL(FINANCE.CONTASRECEBER.crcnvalorpis,0)													crcnvalorpis		,                       "
	cQuery += " NVL(FINANCE.CONTASRECEBER.crcnvalorcofins,0)												crcnvalorcofins		,                       "
	cQuery += " NVL(FINANCE.CONTASRECEBER.crcnvalorcsll,0)													crcnvalorcsll		,                       "
	cQuery += " CASE WHEN FINANCE.CONTASRECEBER.imgenviadaboleto = 'S' THEN '1' ELSE '2' END     			imgenviadaboleto	,                       "
	cQuery += " NVL(LPAD(FINANCE.CONTASRECEBER.crcnfatura,9,'0'), ' ')										crcnfatura			,                       "
	cQuery += " NVL(FINANCE.CONTASRECEBER.crcnvalorinss,0)													crcnvalorinss		,                       "
	cQuery += " NVL(LPAD(FINANCE.CONTASRECEBER.crlncodigo,02,'0'),' ')  									crlncodigo			,                       "
	cQuery += " NVL(TO_CHAR(FINANCE.CONTASRECEBER.crcdrecebimento,'YYYYMMDD'),' ')  						crcdrecebimento		,                       "
	cQuery += " NVL(FINANCE.CONTASRECEBER.crcnvalorrecebido,0)												crcnvalorrecebido   ,                       "
	cQuery += " NVL(FINANCE.CONTASRECEBER.pctcnumerocontarec,FINANCE.CONTASRECEBER.pctcnumeroconta)			pctcnumerocontarec	,                       "
	cQuery += " NVL(RCR.CONTAS.BANCCODIGO ,' ')								    							banccodigo			,                       "
	cQuery += " NVL(RCR.CONTAS.CTACCONTA  ,' ')								    							ctacconta			,                       "
	cQuery += " NVL(RCR.CONTAS.CTACAGENCIA,' ')								    							ctacagencia         ,                       "
    cQuery += " NVL(FINANCE.CONTASRECEBER.crcncotacao,0)                                                    crcncotacao                                 "
	cQuery += " FROM    FINANCE.CONTASRECEBER                                                                                                           "
	cQuery += " 		LEFT JOIN                                                                                                                       "
	cQuery += " 		RCR.CONTAS	ON	NVL(FINANCE.CONTASRECEBER.pctcnumerocontarec,FINANCE.CONTASRECEBER.pctcnumeroconta)	= RCR.CONTAS.PCTCNUMEROCONTA"
	cQuery += " 		LEFT JOIN                                                                                                                       "
	cQuery += " 		RCR.FATURA	ON	FINANCE.CONTASRECEBER.crcnfatura		= RCR.FATURA.NUMERO                                                     "
	cQuery += " 					AND	FINANCE.CONTASRECEBER.orgncodig			= RCR.FATURA.ORGNCODIG                                                  "
	cQuery += " WHERE FINANCE.CONTASRECEBER.crcnvalor > 0                                                                                               "
	cQuery += " ORDER BY    FINANCE.CONTASRECEBER.crccnumerodocumento   ,                                                                               "
    cQuery += "             FINANCE.CONTASRECEBER.crccparcela           ,                                                                               "
    cQuery += "             FINANCE.CONTASRECEBER.crcncliente                                                                                           "

	aRetSql := JurSQL(cQuery, "*",.F.,,.T.)

	nTotTitulos := Len(aRetSql)
	oProcess:SetRegua2(nTotTitulos)

	TCSetConn(nConnProth) // Protheus
	For nI := 1 To nTotTitulos
        oProcess:IncRegua2(i18n("Importando registro #1 de #2", {nI,nTotTitulos}))
		
		dDataBase  := dDataOri
		aReg       := aRetSql[nI]

		// Tratamento para n�o migrar t�tulos com valores negativos, pois iremos avaliar em um segundo momento como migrar
		If GetValCont("crcnvalor"       , aReg) >= 0 .And. ;
		   GetValCont("desconto"        , aReg) >= 0 .And. ;
		   GetValCont("crcnvalorinss"   , aReg) >= 0 .And. ;
		   GetValCont("crcnvalorcsll"   , aReg) >= 0 .And. ;
		   GetValCont("crcnvalorcofins" , aReg) >= 0 .And. ;
		   GetValCont("crcnvalorpis"    , aReg) >= 0 .And. ;
		   GetValCont("crcnvalorir"     , aReg) >= 0 .And. ;
		   GetValCont("crcnvaloriss"    , aReg) >= 0

			cCliSJur   := Alltrim(Str(GetValCont("crcncliente", aReg))) + "|" + Alltrim(Str(GetValCont("cod_endereco", aReg)))
			cDataReceb := GetValCont("crcdrecebimento", aReg)
			bGetCdBank := {|| Substr(GetDePara("RCR.CONTAS"   , GetValCont("pctcnumerocontarec", aReg), ""), 1                                                , TamSX3("A6_COD"    )[1])}
			bGetCdAgen := {|| Substr(GetDePara("RCR.CONTAS"   , GetValCont("pctcnumerocontarec", aReg), ""), TamSX3("A6_COD")[1] + 1                          , TamSX3("A6_AGENCIA")[1])}
			bGetCdCont := {|| Substr(GetDePara("RCR.CONTAS"   , GetValCont("pctcnumerocontarec", aReg), ""), TamSX3("A6_COD")[1] + TamSX3("A6_AGENCIA")[1] + 1, TamSX3("A2_NUMCON" )[1])}
			bGetMoeda  := {|| Substr(GetDePara("RCR.MOEDA"    , GetValCont("codigo"            , aReg), ""), 1                                                , TamSX3("CTO_MOEDA" )[1])}
			bGetSA1Loj := {|| Substr(GetDePara("RCR.ENDERECO" , cCliSJur                              , ""), TamSX3("A1_COD")[1] + 1                          , TamSX3("A1_LOJA"   )[1])}
			bGetEscrt  := {|| Substr(GetDePara("RCR.ESCRITORIO", GetValCont("orgncodig"        , aReg), ""), 1                                                , TamSX3("NS7_COD"   )[1])}
			bGetSA1Cli := {|| StrZero(GetValCont("crcncliente" , aReg), TamSX3("A1_COD")[1])}
			cBanco     := Eval(bGetCdBank )
			cAgencia   := Eval(bGetCdAgen )
			cConta     := Eval(bGetCdCont )
			cMoeda     := Val(Eval(bGetMoeda))
			CCliente   := Eval(bGetSA1Cli )
			cLoja      := Eval(bGetSA1Loja)
			cEscrit    := Eval(bGetEscrt  )
			cFatJur    := xFilial( 'NXA' ) + '-' + cEscrit + '-' + GetValCont("crcnfatura", aReg) + '-' + cFilAnt
			aTitulo    := {{"E1_PREFIXO"    ,cPrefixo                                 ,Nil },;
			               {"E1_NUM"        ,GetValCont("crccnumerodocumento", aReg)  ,Nil },;
			               {"E1_PARCELA"    ,GetValCont("parcela"            , aReg)  ,Nil },;
			               {"E1_TIPO"       ,cTipo                                    ,Nil },;
			               {"E1_CLIENTE"    ,CCliente                                 ,Nil },;
			               {"E1_LOJA"       ,cLoja                                    ,Nil },;
			               {"E1_NATUREZ"    ,cNatureza                                ,Nil },;
			               {"E1_PORTADO"    ,cBanco                                   ,Nil },;
			               {"E1_CONTA"      ,cConta                                   ,Nil },;
			               {"E1_AGEDEP"     ,cAgencia                                 ,Nil },;
			               {"E1_EMISSAO"    ,Stod(GetValCont("crcddata"      , aReg)) ,Nil },;
			               {"E1_VENCTO"     ,Stod(GetValCont("crcdvecto"     , aReg)) ,Nil },;
			               {"E1_VALOR"      ,GetValCont("crcnvalor"          , aReg)  ,Nil },;
			               {"E1_BASEIRF"    ,GetValCont("crcnvalorir"        , aReg)  ,Nil },;
			               {"E1_IRRF"       ,GetValCont("crcnvalorir"        , aReg)  ,Nil },;
			               {"E1_ISS"        ,GetValCont("crcnvaloriss"       , aReg)  ,Nil },;
			               {"E1_HIST"       ,GetValCont("crcchistorico"      , aReg)  ,Nil },;
			               {"E1_LOTE"       ,GetValCont("codigolote"         , aReg)  ,Nil },;
			               {"E1_DESCONT"    ,GetValCont("desconto"           , aReg)  ,Nil },;
			               {"E1_MOEDA"      ,cMoeda                                   ,Nil },;
			               {"E1_VLCRUZ"     ,GetValCont("crcnvalor"          , aReg)  ,Nil },;
			               {"E1_NUMNOTA"    ,GetValCont("crcnnotafiscal"     , aReg)  ,Nil },;
			               {"E1_ORIGEM"     ,"JURA203"                                ,Nil },;
			               {"E1_INSS"       ,GetValCont("crcnvalorinss"      , aReg)  ,Nil },;
			               {"E1_CSLL"       ,GetValCont("crcnvalorcsll"      , aReg)  ,Nil },;
			               {"E1_COFINS"     ,GetValCont("crcnvalorcofins"    , aReg)  ,Nil },;
			               {"E1_PIS"        ,GetValCont("crcnvalorpis"       , aReg)  ,Nil },;
			               {"E1_CODEMP"     ,GetValCont("empncod"            , aReg)  ,Nil },;
			               {"E1_TXMOEDA"    ,GetValCont("crcncotacao"        , aReg)  ,Nil },;
			               {"E1_MULTNAT"    ,"2"                                      ,Nil },;
			               {"E1_PARTOT"     ,GetValCont("parcela"            , aReg)  ,Nil },;
			               {"E1_BASEPIS"    ,GetValCont("crcnvalorpis"       , aReg)  ,Nil },;
			               {"E1_BASECOF"    ,GetValCont("crcnvalorcofins"    , aReg)  ,Nil },;
			               {"E1_BASECSL"    ,GetValCont("crcnvalorcsll"      , aReg)  ,Nil },;
			               {"E1_MDPARCE"    ,GetValCont("parcela"            , aReg)  ,Nil },;
			               {"E1_BOLETO"     ,GetValCont("imgenviadaboleto"   , aReg)  ,Nil },;
			               {"E1_JURFAT"     ,cFatJur                                  ,Nil },;
			               {"E1_BASEINS"    ,GetValCont("crcnvalorinss"      , aReg)  ,Nil },;
			               {"E1_TPESOC"     ,GetValCont("crlncodigo"         , aReg)  ,Nil }} 

			If Posicione('SA1', 1, xFilial('SA1')+CCliente+cLoja, "A1_MSBLQL") == "1" // Cliente bloqueado 
				lCliBloq   := .T.
				SetValue('SA1', xFilial('SA1')+CCliente+cLoja, "A1_MSBLQL", '2', 1)
			EndIf

			dDataBAse   := Stod(GetValCont("crcddata"      , aReg))
			lMsErroAuto := .F.
			MSExecAuto({|x,y| Fina040(x,y)},aTitulo,3)
			If lMsErroAuto
				MostraErro()
			Else
				ConOut("INCLUIDO COM SUCESSO!" + SE1->E1_NUM)
				If !Empty(Alltrim(cDataReceb)) .And. ;
				   !Empty(Alltrim(cBanco    )) .And. ;
				   !Empty(Alltrim(cConta    )) .And. ;
				   !Empty(Alltrim(cAgencia  ))
					dDataBase := Sto(cDataReceb)
					nVlrReceb := GetValCont("crcnvalorrecebido",aReg)
					aBaixa := {{"E1_PREFIXO"  ,SE1->E1_PREFIXO  ,Nil },;
							   {"E1_NUM"      ,SE1->E1_NUM      ,Nil },;
							   {"E1_TIPO"     ,SE1->E1_TIPO     ,Nil },;
							   {"E1_CLIENTE"  ,SE1->E1_CLIENTE  ,Nil },;
							   {"E1_LOJA"     ,SE1->E1_LOJA     ,Nil },;
							   {"E1_NATUREZ"  ,SE1->E1_NATUREZ  ,Nil },;
							   {"E1_PARCELA"  ,SE1->E1_PARCELA  ,Nil },;
							   {"AUTMOTBX"    ,"001"            ,Nil },;
							   {"CBANCO"      ,cBanco           ,Nil },;
							   {"CAGENCIA"    ,cAgencia         ,Nil },;
							   {"CCONTA"      ,cConta           ,Nil },;
							   {"AUTDTBAIXA"  ,Stod(cDataReceb) ,Nil },;
							   {"AUTDTCREDITO",Stod(cDataReceb) ,Nil },;
							   {"AUTHIST"     ,cHistBaixa       ,Nil },;
							   {"AUTJUROS"    ,0                ,Nil },;
							   {"NVALREC"     , nVlrReceb       ,Nil }}
					lMsErroAuto := .F.
					MSExecAuto({|x,y,b| Fina070(x,y,b)},aBaixa,3,.F.)
					If lMsErroAuto
						//MostraErro()
					Else
						ConOut("BAIXADO COM SUCESSO!" + SE1->E1_NUM)
					EndIf
				EndIf
			EndIf
			If lCliBloq
				lCliBloq   := .F.
				SetValue('SA1', xFilial('SA1')+CCliente+cLoja, "A1_MSBLQL", '1', 1)
			EndIf
		EndIf
	Next

	Conout("Tabela em processamento: " + cTabProthe + " - Fim   : " + JurTimeStamp(1))
	Conout("Duracao: " + cTabProthe + " --> " + ElapTime( dTimeInI , Time()))

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} GetValCont
Busca os valores no retorno do select do contrato

@param cCampoSis , para buscar o valor no array
@param aReg      , Dados do registro do Sisjuri

@return xRet, Valor do campo no registro

@author  Bruno Ritter
@since   24/09/2019
/*/
//-------------------------------------------------------------------
Static Function GetValCont(cCampoSis, aReg)
	Local xRet  := Nil
	Local aElem := {}
	Local nPos  := 0

	If HMGet(oHashCpo, cCampoSis, @aElem)
		nPos := aElem[1][2]
		xRet := aReg[nPos]
	EndIf

Return xRet

//-------------------------------------------------------------------
/*/{Protheus.doc} GetDePara
Busca a chave na tabela OHS (de-para)

@param cEntidaSis, Tabela do Sisjuri que o registro fica gravado
@param xCodigoSis, C�digo utilizado no SisJuri
@param cEsctSis  , Escrit�rio que o registro est� gravado

@return cCodProthe, C�digo utilizado no Protheus

@author Bruno Ritter
@since 15/07/2019
/*/
//-------------------------------------------------------------------
Static Function GetDePara(cEntidaSis, xCodigoSis, cEsctSis)
	Local cCodProthe   := ""

	Default cEntidaSis := ""
	Default xCodigoSis := ""
	Default cEsctSis   := ""

	cCodProthe := POSICIONE('OHS', 1, xFilial('OHS') +;
	                                  Padr(cEntidaSis, nTamEntSis) +;
	                                  Padr(cValToChar(xCodigoSis), nTamCodSis) +;
	                                  Padr(cEsctSis, nTamFilSis);
	                                  , 'OHS_CODPRO')

Return cCodProthe

//-------------------------------------------------------------------
/*/{Protheus.doc} GetHashMap
Gera um HashMap do array com os campos da query do contrato

@return oHash, objeto do tipo HashMap

@author Bruno Ritter
@since  27/09/2019
/*/
//-------------------------------------------------------------------
Static Function GetHashMap()
	Local aCpoSis := GetCpoCont()
	Local oHash   := HMNew()
	Local nI      := 0

	For nI := 1 To Len(aCpoSis)
		HMAdd(oHash, {aCpoSis[nI][1], nI})
	Next nI

Return oHash

//-------------------------------------------------------------------
/*/{Protheus.doc} GetCpoCont
Pega os campos para o select

@param lExcecao   , Se � query de cobran�a com exce��o no tabelado ou despesa
@param lExcFilho  , Se a exce��o � filho de um caso m�e

@return aCampos, Retorna um array com os campos para o select do contrato

@author  Bruno Ritter
@since   17/09/2019
/*/
//-------------------------------------------------------------------
Static Function GetCpoCont()
	Local aCampos    := {}

	aAdd(aCampos, {"orgncodig"           })
	aAdd(aCampos, {"crccnumerodocumento" })
	aAdd(aCampos, {"crccparcela"         })
	aAdd(aCampos, {"parcela"             })
	aAdd(aCampos, {"crcncliente"         })
	aAdd(aCampos, {"cod_endereco"        })
	aAdd(aCampos, {"crccrazaosocial"     })
	aAdd(aCampos, {"crcddata"            })
	aAdd(aCampos, {"crcdvecto"           })
	aAdd(aCampos, {"crcnvalor"           })
	aAdd(aCampos, {"crcnvalorir"         })
	aAdd(aCampos, {"crcnvaloriss"        })
	aAdd(aCampos, {"crcchistorico"       })
	aAdd(aCampos, {"codigolote"          })
	aAdd(aCampos, {"desconto"            })
	aAdd(aCampos, {"codigo"              })
	aAdd(aCampos, {"crcnnotafiscal"      })
	aAdd(aCampos, {"empncod"             })
	aAdd(aCampos, {"crcnvalorpis"        })
	aAdd(aCampos, {"crcnvalorcofins"     })
	aAdd(aCampos, {"crcnvalorcsll"       })
	aAdd(aCampos, {"imgenviadaboleto"    })
	aAdd(aCampos, {"crcnfatura"          })
	aAdd(aCampos, {"crcnvalorinss"       })
	aAdd(aCampos, {"crlncodigo"          })
	aAdd(aCampos, {"crcdrecebimento"     })
	aAdd(aCampos, {"crcnvalorrecebido"   })
	aAdd(aCampos, {"pctcnumerocontarec"  })
	aAdd(aCampos, {"banccodigo"          })
	aAdd(aCampos, {"ctacconta"           })
	aAdd(aCampos, {"ctacagencia"         })
	aAdd(aCampos, {"crcncotacao"         })

Return aCampos