#INCLUDE 'PROTHEUS.CH'

Static oHashCpo := GetHashMap()

//-------------------------------------------------------------------
/*/{Protheus.doc} MigraCtr
Migra o contrato

@param oProcess  , Objeto do processa
@param nConnSisJr, Conex�o do SisJuri
@param nConnProth, Conex�o do Protheus

@author Bruno Ritter
@since  25/09/2019
/*/
//-------------------------------------------------------------------
User Function MigraCtr(oProcess, nConnSisJr, nConnProth)
	Local cQuery     := ""
	Local nCpo       := 0
	Local lCobrFixo  := .F. // Cobran�a de fixo
	Local aCampoSis  := GetCpoCont()
	Local aRetSql    := {}
	Local cCodContr  := ""
	Local nI         := 0
	Local nReg       := 0
	Local nTotContr  := 0
	Local nTotAgrup  := 0
	Local nTotJuncao := 0
	Local nTotCasos  := 0
	Local nPos       := 0
	Local aContXCaso := {}
	Local aCasoFilho := {}
	Local aCasoAgrup := {}
	Local aUltAgrup  := {}
	Local aUltJuncao := {}
	Local aJuncao    := {}

	oProcess:IncRegua1("De-Para Contratos")

	TCSetConn(nConnSisJr) // SISJURI

	cQuery := " SELECT "
	For nCpo := 1 To Len(aCampoSis)
		cQuery += aCampoSis[nCpo][1] + " " + aCampoSis[nCpo][2] + Iif(nCpo == Len(aCampoSis), " ", ", ")
	Next nCpof
	cQuery += " FROM RCR.PASTA "
	cQuery += " INNER JOIN RCR.ENDERECO "
	cQuery +=        "  ON PASTA.COD_CLIENTE = ENDERECO.COD_CLIENTE "
	cQuery +=        " AND PASTA.COD_ENDERECO = ENDERECO.COD_ENDERECO "
	cQuery += " INNER JOIN RCR.CLIENTE "
	cQuery +=         " ON CLIENTE.COD_CLIENTE = PASTA.COD_CLIENTE "
	cQuery += " WHERE PASCSITUACAOCADASTRO = 'D' " // S� casos definitivos
	cQuery += " ORDER BY " // Ordena os campos para poder comparar um registro com o anterior para saber se deve manter no agrupamento de contratos e ou gerar jun��o de contrato
	cQuery +=         " PASTA.COD_CLIENTE, " // In�cio de agrupamento para gerar jun��o e agrupar contratos
	cQuery +=         " PASTA.MOEDA, "
	cQuery +=         " PASTA.ORGNCODIG, "
	cQuery +=         " PASTA.SOCIO_RESPONSAVEL, "
	cQuery +=         " PASTA.IDIOMA, "
	cQuery +=         " MULTIPAYER, "
	cQuery +=         " ENCAMINHAMENTO, "
	cQuery +=         " VENCIMENTO, "// Fim de agrupamento para gerar jun��o
	cQuery +=         " PASTA.COD_REL, "
	cQuery +=         " PASTA.COD_ENDERECO, "
	cQuery +=         " PASTA.FORMA_PGTO, "
	cQuery +=         " PASTA.CTANCODIG, "
	cQuery +=         " PASTA.MOEDALIMITEHORA, "
	cQuery +=         " PASTA.VALORLIMITEHORA, "
	cQuery +=         " PASTA.SALDOINICIALHORA, "
	cQuery +=         " PASTA.VALORLIMITEFAT, "
	cQuery +=         " PASTA.GROSSUP_DESP_TRIB, "
	cQuery +=         " PASTA.TXADM_DESP_TRIB, "
	cQuery +=         " TIPOATIV, "
	cQuery +=         " TIPO_TABELA, "
	cQuery +=         " TIPO_CALCULO, "
	cQuery +=         " TIPODESP " // Fim de agrupamento para gerar contratos agrupados

	aRetSql := JurSQL(cQuery, "*",.F.,,.T.)

	nTotContr := Len(aRetSql)
	oProcess:SetRegua2(nTotContr)

	TCSetConn(nConnProth) // Protheus
	For nI := 1 To nTotContr
		aReg      := aRetSql[nI]
		cCodContr := ""
		lCobrFixo := GetValCont("TPCOBHORA", aReg) == "N" .And. GetValCont("TPCOBFIXO", aReg) == "S"
		oProcess:IncRegua2(i18n("Importando registro #1 de #2", {nI, nTotContr}))

		Do Case
			Case IsCasoMae(aReg) .Or. IsMistoMin(aReg)
				cCodContr := GravaContr(aReg, GetValCont("PASTA", aReg))

			Case IsCasoFilh(aReg)
				aCliCasMae := {GetValCont("COD_CLIENTE", aReg), GetValCont("PASTAMAEFILHO", aReg)}
				aAdd(@aCasoFilho, {aCliCasMae, aReg})

			Case IsMMFilho(aReg)
				aCliCasMae := {GetValCont("COD_CLIENTE", aReg), GetValCont("CONTRATO", aReg)}
				aAdd(@aCasoFilho, {aCliCasMae, aReg})

			Case IsCasoExc(aReg)
				TCSetConn(nConnSisJr)
				aReg      := GetDadosEx(aReg)
				TCSetConn(nConnProth)

				cCodContr := GravaContr(aReg, GetValCont("PASTAMAEFILHO", aReg))

			OTHERWISE // Agrupa os casos
				If GetValCont("AGRUPAR", aReg) == "N" .Or. lCobrFixo// N�o agrupar ou Fixo
					cCodContr := GravaContr(aReg)

				ElseIf Len(aUltAgrup) > 0 .And. IsEqualCas(aReg, aUltAgrup)
					aAdd(aCasoAgrup[Len(aCasoAgrup)], aReg)
					aUltAgrup := aReg

				Else
					aAdd(aCasoAgrup, {aReg})
					aUltAgrup := aReg
				EndIf
		EndCase

		If !Empty(cCodContr)
			aAdd(aContXCaso, {GetValCont("COD_CLIENTE", aReg), GetValCont("PASTA", aReg), cCodContr})
			VincCaso(cCodContr, GetValCont("COD_CLIENTE", aReg),;
			                    GetValCont("COD_ENDERECO", aReg),;
			                    GetValCont("PASTA", aReg))

			If lCobrFixo .And. GetValCont("CLICFATURASAGRUPADAS", aReg) == "S" // Gerar Jun��o
			
				If !Empty(aUltJuncao) .And. IsEqualJnc(aReg, aUltJuncao)
					aAdd(aJuncao[Len(aJuncao)], cCodContr)
				Else
					aAdd(aJuncao, {cCodContr})
				EndIf

				aUltJuncao := aReg
			EndIf
		EndIf
	Next nI

	oProcess:IncRegua1("Agrupando Casos")
	nTotAgrup := Len(aCasoAgrup)
	oProcess:SetRegua2(nTotAgrup)

	For nI := 1 To nTotAgrup
		oProcess:IncRegua2(i18n("Importando registro #1 de #2", {nI, nTotAgrup}))

		cCodContr := GravaContr(aCasoAgrup[nI][1], -1, aCasoAgrup[nI]) // Utiliza o primeiro como base
		For nReg := 1 To Len(aCasoAgrup[nI])
			VincCaso(cCodContr, GetValCont("COD_CLIENTE", aCasoAgrup[nI][nReg]),;
			                    GetValCont("COD_ENDERECO", aCasoAgrup[nI][nReg]),;
			                    GetValCont("PASTA", aCasoAgrup[nI][nReg]))
		Next nReg
	Next nI

	oProcess:IncRegua1("Vinculando Caso no Contrato")
	nTotCasos := Len(aCasoFilho)
	oProcess:SetRegua2(nTotCasos)

	For nI := 1 To nTotCasos
		oProcess:IncRegua2(i18n("Importando registro #1 de #2", {nI, nTotCasos}))

		aCliCasMae := aCasoFilho[nI][1]
		aCCFilho   := aCasoFilho[nI][2]
		nPos       := aScan(aContXCaso, {|aCasContr| aCasContr[1] == aCliCasMae[1] .AND. aCasContr[2] == aCliCasMae[2]})

		If nPos > 0 // Prote��o, pois deveria sempre existir o caso m�e no aContXCaso
			cCodContr  := aContXCaso[nPos][3]
			VincCaso(cCodContr, GetValCont("COD_CLIENTE", aCCFilho), GetValCont("COD_ENDERECO", aCCFilho), GetValCont("PASTA", aCCFilho))
		Else
			cCodContr := GravaContr(aCCFilho)
			VincCaso(cCodContr, GetValCont("COD_CLIENTE", aCCFilho), GetValCont("COD_ENDERECO", aCCFilho), GetValCont("PASTA", aCCFilho))
		EndIf
	Next nI

	oProcess:IncRegua1("De-Para Contratos")
	nTotJuncao := Len(aJuncao)
	oProcess:SetRegua2(nTotJuncao)

	For nI := 1 To nTotJuncao
		oProcess:IncRegua2(i18n("Importando registro #1 de #2", {nI, nTotJuncao}))

		If Len(aJuncao[nI]) > 1
			MigraJnc(aJuncao[nI])
		EndIf
	Next nI

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} GravaContr
Grava o contrato no Protheus

@param aReg       , Dados do registro Sisjuri
@param aCasoAgrup , Passar todos os casos que fazem parte do agrupamento

@author Bruno Ritter
@since  12/09/2019
/*/
//-------------------------------------------------------------------
Static Function GravaContr(aReg, nCasoMae, aCasoAgrup)
	Local cQuery     := ""
	Local cCliLoja   := cValTochar(GetValCont("COD_CLIENTE", aReg)) + '|' + cValTochar(GetValCont("COD_ENDERECO", aReg))
	Local lCasoMae   := .F.
	Local cCasoMae   := ""
	Local cDescricao := ""
	Local cCaso      := StrZero(GetValCont("PASTA", aReg), TamSX3("NVE_NUMCAS")[1])
	Local cCodContr  := GetSxeNum("NT0", "NT0_COD")
	Local lAgrupa    := .F.
	Local cObs       := ""
	Local nI         := 0

	Default nCasoMae   := -1
	Default aCasoAgrup := {}

	lCasoMae := nCasoMae > 0
	cCasoMae := Iif(lCasoMae, StrZero(nCasoMae, TamSX3("NVE_NUMCAS")[1]), "")
	lAgrupa  := !Empty(aCasoAgrup) .And. Len(aCasoAgrup) > 1

	cDescricao := Iif(lAgrupa, "Agrupamento - ", cCaso + " - ")
	cDescricao += CalcTpHon(aReg, .T.)

	If lAgrupa
		For nI := 1 To Len(aCasoAgrup)
			If !Empty(GetValCont("OBSERVACAO", aCasoAgrup[nI]))
				cObs += "Caso: " + StrZero(GetValCont("PASTA", aCasoAgrup[nI]), TamSX3("NVE_NUMCAS")[1])
				cObs += CRLF+GetValCont("OBSERVACAO", aCasoAgrup[nI])+CRLF
			EndIf
		Next nI
	Else
		cObs := GetValCont("OBSERVACAO", aReg)
	EndIf

	RecLock("NT0", .T.)
	NT0->NT0_COD    := cCodContr
	NT0->NT0_NOME   := cDescricao
	NT0->NT0_CGRPCL := StrZero(GetValCont("GRENCODIGO", aReg), TamSX3("A1_GRPVEN")[1])
	NT0->NT0_CCLIEN := StrZero(GetValCont("COD_CLIENTE", aReg), TamSX3("A1_COD")[1])
	NT0->NT0_CLOJA  := Substr(GetDePara("RCR.ENDERECO", cCliLoja, ""), TamSX3("A1_COD")[1] + 1, TamSX3("A1_LOJA")[1])
	NT0->NT0_CPART1 := GetDePara("RCR.ADVOGADO", GetValCont("SOCIO_RESPONSAVEL", aReg), "")
	NT0->NT0_CMOE   := Substr(GetDePara("RCR.MOEDA", GetValCont("MOEDA", aReg), ""), 1, TamSX3("CTO_MOEDA")[1])
	NT0->NT0_CESCR  := Substr( GetDePara("RCR.ESCRITORIO", GetValCont("ORGNCODIG", aReg), ""), 1, TamSX3("NS7_COD")[1])
	NT0->NT0_CIDIO  := GetValCont("IDIOMA", aReg)
	NT0->NT0_DTINC  := Iif(lAgrupa, Date(), GetValCont("DATA_ABERTURA", aReg))
	NT0->NT0_CTPHON := CalcTpHon(aReg, .F.)
	NT0->NT0_CONTR  := "2"
	NT0->NT0_SIT    := Iif(GetValCont("PASCSITUACAOCADASTRO", aReg) == "D", "2", "1") // N�o deve criar ou vincular caso provis�rio
	NT0->NT0_REV    := Iif(lAgrupa, "2", GetValCont("REVISADO", aReg))
	NT0->NT0_CMOELI := Substr(GetDePara("RCR.MOEDA", GetValCont("MOEDALIMITEHORA", aReg), ""), 1, TamSX3("CTO_MOEDA")[1])
	NT0->NT0_VLRLI  := GetValCont("VALORLIMITEHORA", aReg)
	NT0->NT0_SALDOI := GetValCont("SALDOINICIALHORA", aReg)
	NT0->NT0_VLRLIF := GetValCont("VALORLIMITEFAT", aReg)
	NT0->NT0_DTBASE := Iif(lAgrupa, Date(), GetValCont("DATA_BASE", aReg))
	NT0->NT0_PARFIX := Iif(lAgrupa, "N", GetValCont("PARCELAFIXA", aReg))
	NT0->NT0_FINAJU := Iif(lAgrupa, "N", GetValCont("FINALCASO", aReg))
	NT0->NT0_FIXEXC := Iif(lAgrupa, "N", GetValCont("EXCEDENTEFIXO", aReg))
	NT0->NT0_CASPRO := "1" // Qtdade por Caso, n�o por processos
	NT0->NT0_COPFAT := GetValCont("CC_FATURA", aReg)
	NT0->NT0_DISCAS := "1" // Discrimina Casos - Valor padr�o Protheus
	NT0->NT0_ATIVO  := "1" // Valor padr�o Protheus
	NT0->NT0_OBS    := cObs
	NT0->NT0_PARCE  := "2" // Valor padr�o Protheus
	NT0->NT0_CFXCVL := "2"
	NT0->NT0_CFACVL := "1" // Valor padr�o Protheus
	NT0->NT0_CEXCVL := "1" // Valor padr�o Protheus
	NT0->NT0_CASMAE := Iif(lCasoMae, "1", "2")
	NT0->NT0_CCLICM := Iif(lCasoMae, StrZero(GetValCont("COD_CLIENTE", aReg), TamSX3("A1_COD")[1]) , "")
	NT0->NT0_CLOJCM := Iif(lCasoMae, Substr(GetDePara("RCR.ENDERECO", cCliLoja, ""), TamSX3("A1_COD")[1] + 1, TamSX3("A1_LOJA")[1]) , "")
	NT0->NT0_CCASCM := Iif(lCasoMae, cCasoMae , "")
	NT0->NT0_FIXREV := Iif(GetValCont("APROVAR_PARCELAS", aReg) == "S" .And. !lAgrupa, "1", "2")
	NT0->NT0_DESPES := Iif(GetValCont("DESPESAS", aReg) == 3 .And. !lAgrupa, "2", "1")
	NT0->NT0_ENCD   := Iif(GetValCont("ENC_DESP", aReg) == "N" .Or. lAgrupa, "2", "1")
	NT0->NT0_SERTAB := Iif(GetValCont("COBRARTABELADO", aReg) == 3 .And. !lAgrupa, "2", "1")
	NT0->NT0_ENCT   := Iif(GetValCont("TAB_ENC_HONOR", aReg) == "N" .Or. lAgrupa, "2", "1")
	NT0->NT0_ENCH   := Iif(GetValCont("ENC_HONOR", aReg) == "N" .Or. lAgrupa, "2", "1")
	NT0->NT0_CTBCVL := Iif(GetValCont("TPCOBFIXO", aReg) == "S", "2", "1")
	NT0->NT0_CALFX  := Iif(GetValCont("TIPO_CALCULO", aReg) == "V" .AND. !lAgrupa, "1", "2") // Sisjuri: V - Valor / H - Hora - Protheus: 1=Valor;2=Hora
	NT0->NT0_TPFX   := Iif(GetValCont("TIPO_TABELA", aReg) == "E", "1", "2")
	// NOTE: N�o existe esse cadastro no Sisjuri
	//NT0->NT0_DTENC  := ""
	//NT0->NT0_CTIPOF := ""
	//NT0->NT0_RELPRE := ""
	If !lAgrupa
		NT0->NT0_VALORA := GetValCont("VALORATUALIZADO", aReg)
		NT0->NT0_DATAAT := GetValCont("DATAATUALIZACAO", aReg)
		NT0->NT0_DTREFI := GetValCont("DATAREFINI", aReg)
		NT0->NT0_DTREFF := GetValCont("DATAREFFIM", aReg)
		NT0->NT0_DTVENC := GetValCont("DATA_VENC", aReg)
		NT0->NT0_VLRBAS := GetValCont("VALORBASE", aReg)
		NT0->NT0_PERFIX := GetValCont("PERCOBFIXO", aReg)
		NT0->NT0_PEREX  := GetValCont("PERCOBEXCEDENTE", aReg)
		NT0->NT0_CMOEF  := Substr(GetDePara("RCR.MOEDA", GetValCont("MOEDACOND", aReg), ""), 1, TamSX3("CTO_MOEDA")[1])
		NT0->NT0_USRINC := GetValCont("PASCOSUSER", aReg)
		NT0->NT0_PERCOR := GetValCont("PERCORRECAO", aReg)
		NT0->NT0_FXENCM := Iif(GetValCont("CASO_ENC_MES", aReg) == "S", "1", "2")
		NT0->NT0_TPCORR := Iif(GetValCont("TIPOCORRECAO", aReg) == "I", "2", "1")
		NT0->NT0_TPCEXC := Iif(GetValCont("TIPOCOBEXCE", aReg) == "H", "1", "2")
		If GetValCont("TIPOCOBEXCE", aReg) == "H"
			NT0->NT0_LIMEXH := GetValCont("LIMHORADESC", aReg)
		Else
			NT0->NT0_PERCD  := GetValCont("LIMHORADESC", aReg)
		EndIf
	EndIf
	NT0->(MsUnlock())

	CasPagador(aReg, cCodContr)
	CasoTpDesp(aReg, cCodContr)
	CasoAtivid(aReg, cCodContr)
	CasParcela(aReg, cCodContr)
	CasoFaixa(aReg, cCodContr)

Return cCodContr

//-------------------------------------------------------------------
/*/{Protheus.doc} CalcTpHon
Calcula tipo de honor�rio, retornando o c�digo ou a descri��o no Protheus.

@param aReg, Dados do registro Sisjuri

@author Bruno Ritter
@since  20/09/2019
/*/
//-------------------------------------------------------------------
Static Function CalcTpHon(aReg, lRetDesc)
	Local cTipoHon   := ""
	Local aTipoHon   := ""
	Local lFaixa     := GetValCont("FAIXA", aReg) == "S"
	Local lFixoParti := .F.
	Local lFixoPreDf := .F.
	Local lFixoOcorr := .F.

	Do Case
		Case GetValCont("TPCOBNCOBRAR", aReg) == "S"
			aTipoHon := {"009", "N�o Cobrar"}
		
		// HORA
		Case GetValCont("TPCOBHORA", aReg) == "S" .And. GetValCont("TPCOBFIXO", aReg) == "N"

			If lFaixa
				aTipoHon := {"019", "Faixa (Hora)"}

			ElseIf GetValCont("VALORLIMITECASO", aReg) == 0 .And. GetValCont("VALORLIMITEFAT", aReg) == 0 .And. GetValCont("VALORLIMITEHORA", aReg) == 0
				aTipoHon := {"001", "Hora"}

			ElseIf GetValCont("VALORLIMITECASO", aReg) == 0 .And. GetValCont("VALORLIMITEFAT", aReg) > 0 .And. GetValCont("VALORLIMITEHORA", aReg) > 0
				aTipoHon := {"012", "Limite"}

			ElseIf GetValCont("VALORLIMITECASO", aReg) == 0 .And. GetValCont("VALORLIMITEFAT", aReg) == 0 .And. GetValCont("VALORLIMITEHORA", aReg) > 0
				aTipoHon := {"002", "Limite Geral"}
			
			ElseIf GetValCont("VALORLIMITECASO", aReg) == 0 .And. GetValCont("VALORLIMITEFAT", aReg) > 0 .And. GetValCont("VALORLIMITEHORA", aReg) == 0
				aTipoHon := {"004", "Limite por Fatura"}
			EndIf

		// FIXO
		Case GetValCont("TPCOBHORA", aReg) == "N" .And. GetValCont("TPCOBFIXO", aReg) == "S"
			lFixoParti := GetValCont("PARCELAFIXA", aReg) == "S"
			lFixoPreDf := !lFixoParti .And. GetValCont("PERCOBFIXO", aReg) > 0
			lFixoOcorr := !lFixoPreDf

			If GetValCont("FINALCASO", aReg) == "S"
				aTipoHon := {"010", "Final Caso"}

			Else
				If lFaixa .And. lFixoParti
					aTipoHon := {"018", "Faixa (Partido)"}

				ElseIf lFaixa .And. lFixoPreDf
					aTipoHon := {"035", "Faixa (Pr�-Definido)"}

				ElseIf lFaixa .And. lFixoOcorr
					aTipoHon := {"036", "Faixa (Ocorr�ncia)"}

				ElseIf !lFaixa .And. lFixoParti
					aTipoHon := {"003", "Fixo Partido"}

				ElseIf !lFaixa .And. lFixoPreDf
					aTipoHon := {"005", "Fixo Pr�-Definido"}

				ElseIf !lFaixa .And. lFixoOcorr
					aTipoHon := {"006", "Fixo Ocorr�ncia"}

				EndIf
			EndIf

		// HORA E FIXO
		Case GetValCont("TPCOBHORA", aReg) == "S" .And. GetValCont("TPCOBFIXO", aReg) == "S"

			If GetValCont("PERCOBFIXO", aReg) <> GetValCont("PERCOBEXCEDENTE", aReg)
				aTipoHon := {"007", "Misto"}

			ElseIf GetValCont("PERCOBFIXO", aReg) == GetValCont("PERCOBEXCEDENTE", aReg)
				aTipoHon := {"008", "Valor M�nimo"}
			EndIf
	EndCase

	If !Empty(aTipoHon)
		cTipoHon := Iif(lRetDesc, aTipoHon[2], aTipoHon[1])
	EndIf

Return cTipoHon

//-------------------------------------------------------------------
/*/{Protheus.doc} GetValCont
Busca os valores no retorno do select do contrato

@param cCampoSis , para buscar o valor no array
@param aReg      , Dados do registro do Sisjuri

@return xRet, Valor do campo no registro

@author  Bruno Ritter
@since   24/09/2019
/*/
//-------------------------------------------------------------------
Static Function GetValCont(cCampoSis, aReg)
	Local xRet  := Nil
	Local aElem := {}
	Local nPos  := 0

	If HMGet(oHashCpo, cCampoSis, @aElem)
		nPos := aElem[1][2]
		xRet := aReg[nPos]
	EndIf

Return xRet

//-------------------------------------------------------------------
/*/{Protheus.doc} CaseExcCob
Gerar um case para buscar informa��es dependendo da exce��o de cobran�a
no tabelado e ou despesa

@param lExcFilho , Se a exce��o � filho de um caso m�e
@param cTpExcecao, tipo de exce��o: D=Despesa ; T=Tabelado
@param cGetCpo   , Campo para buscar na exce��o

@return cCase, Case com a exce��o

@author Bruno Ritter
@since  24/09/2019
/*/
//-------------------------------------------------------------------
Static Function CaseExcCob(lExcFilho, cTpExcecao, cGetCpo)
	Local cCase     := ""
	Local cAlsTab   := Iif(lExcFilho, "FILHO", "PASTA")
	Local cExcecao  := cAlsTab + "." + Iif(cTpExcecao == "D", "PASTADESPESA", "PASTABELADO")

	cCase := " CASE "
	cCase +=      " WHEN " + cExcecao + " <> -1 AND " + cExcecao + " <> " + cAlsTab + ".PASTA "
	cCase +=           " THEN (SELECT A." + cGetCpo + " FROM RCR.PASTA A "
	cCase +=                 " WHERE A.COD_CLIENTE = " + cAlsTab + ".COD_CLIENTE "
	cCase +=                   " AND A.PASTA = " + cExcecao + ") "
	cCase +=      " WHEN " + cAlsTab + ".PASTAMAEFILHO = -1 AND " + cAlsTab + ".PASTAMAEFILHO <> " + cAlsTab + ".PASTA "
	cCase +=           " THEN (SELECT A." + cGetCpo + " FROM RCR.PASTA A "
	cCase +=                 " WHERE A.COD_CLIENTE = " + cAlsTab + ".COD_CLIENTE "
	cCase +=                   " AND A.PASTA = " + cAlsTab + ".PASTAMAEFILHO) "
	cCase +=      " ELSE " + cAlsTab + "." + cGetCpo + " "
	cCase += " END "

Return cCase

//-------------------------------------------------------------------
/*/{Protheus.doc} GetCpoCont
Pega os campos para o select do contrato.

@param lExcecao   , Se � query de cobran�a com exce��o no tabelado ou despesa
@param lExcFilho  , Se a exce��o � filho de um caso m�e

@return aCampos, Retorna um array com os campos para o select do contrato

@author  Bruno Ritter
@since   17/09/2019
/*/
//-------------------------------------------------------------------
Static Function GetCpoCont(lExcecao, lExcFilho)
	Local aCampos    := {}
	Local cCpoFaixa  := ""
	Local cCasoMae   := ""
	Local cCpoDesp   := "PASTA.DESPESAS"
	Local cCpoEncDes := "PASTA.ENC_DESP"
	Local cCpoTab    := "PASTA.COBRARTABELADO"
	Local cCpoEncTab := "PASTA.ENC_HONOR"
	Local cAlsPasta  := "PASTA"
	Local cMultiPay  := ""
	Local cTipoDesp  := ""
	Local cTipoAtiv  := ""
	Local cEncaminh  := ""
	Local cVenciment := ""

	Default lExcecao  := .F.
	Default lExcFilho := .F.

	If lExcecao
		cAlsPasta  := Iif(lExcFilho, "FILHO", "PASTA")
		cCpoDesp   := CaseExcCob(lExcFilho, "D", "DESPESAS")
		cCpoEncDes := CaseExcCob(lExcFilho, "D", "ENC_DESP")
		cCpoTab    := CaseExcCob(lExcFilho, "T", "COBRARTABELADO")
		cCpoEncTab := CaseExcCob(lExcFilho, "T", "ENC_HONOR")
	EndIf

	cMultiPay := "( SELECT "
	cMultiPay +=        " LISTAGG(TO_CHAR(A.COD_CLIENTE)||'|'||TO_CHAR(A.COD_ENDERECO), ';') "
	cMultiPay +=        " WITHIN GROUP (ORDER BY A.COD_CLIENTE, A.COD_ENDERECO) "
	cMultiPay +=  " FROM SSJR.CAD_MULTIPAYER_PASTA A WHERE A.COD_CLIENTE =  PASTA.COD_CLIENTE AND A.PASTA = PASTA.PASTA) "

	cTipoDesp := " ( SELECT  "
	cTipoDesp +=         " LISTAGG(TIPO, ';') "
	cTipoDesp +=         " WITHIN GROUP (ORDER BY A.COD_CLIENTE, A.PASTA, A.TIPO) "
	cTipoDesp +=   " FROM SSJR.CAD_PASTA_TIPODESP A WHERE A.COD_CLIENTE =  PASTA.COD_CLIENTE AND A.PASTA = PASTA.PASTA) "

	cTipoAtiv := " ( SELECT "
	cTipoAtiv +=         " LISTAGG(TIPO, ';') "
	cTipoAtiv +=         " WITHIN GROUP (ORDER BY A.COD_CLIENTE, A.PASTA, A.TIPO) "
	cTipoAtiv +=   " FROM SSJR.CAD_PASTA_TIPOATIV A WHERE A.COD_CLIENTE =  PASTA.COD_CLIENTE AND A.PASTA = PASTA.PASTA) "

	cEncaminh := " ( SELECT  "
	cEncaminh +=         " LISTAGG(TO_CHAR(A.CFANCODCLIENTE)||'|'||TO_CHAR(A.CFANCODENDERECO), ';') "
	cEncaminh +=          "WITHIN GROUP (ORDER BY A.CFANCODCLIENTE, A.CFANCODENDERECO) "
	cEncaminh +=   " FROM RCR.COPIAFATURA A WHERE A.CFANCODCLIENTE =  PASTA.COD_CLIENTE AND A.CFANCODPASTA = PASTA.PASTA) "

	cVenciment += " ( SELECT "
	cVenciment +=        " LISTAGG(A.TIPO||'|'||A.DIASUTEIS||'|'||A.ULTIMO||'|'||TO_CHAR(A.VALOR), ';') "
	cVenciment +=        " WITHIN GROUP (ORDER BY TIPO,DIASUTEIS,ULTIMO,VALOR) "
	cVenciment +=  " FROM SSJR.CAD_CAS_DIASVENC A WHERE A.COD_CLIENTE =  PASTA.COD_CLIENTE AND A.PASTA = PASTA.PASTA) "

	cCpoFaixa := "(SELECT 'S' FROM SSJR.CAD_TABELAHORA "
	cCpoFaixa += " WHERE CAD_TABELAHORA.PASTA = PASTA.PASTA AND "
	cCpoFaixa += " CAD_TABELAHORA.COD_CLIENTE = PASTA.COD_CLIENTE AND "
	cCpoFaixa += " ROWNUM = 1) "

	aAdd(aCampos, {CpoClob("PASTA.OBSERVACAO")   , "OBSERVACAO"          })
	aAdd(aCampos, {cVenciment                    , "VENCIMENTO"          })
	aAdd(aCampos, {cEncaminh                     , "ENCAMINHAMENTO"      })
	aAdd(aCampos, {cTipoAtiv                     , "TIPOATIV"            })
	aAdd(aCampos, {cTipoDesp                     , "TIPODESP"            })
	aAdd(aCampos, {cMultiPay                     , "MULTIPAYER"          })
	aAdd(aCampos, {cCpoFaixa                     , "FAIXA"               })
	aAdd(aCampos, {cCpoDesp                      , "DESPESAS"            })
	aAdd(aCampos, {cCpoEncDes                    , "ENC_DESP"            })
	aAdd(aCampos, {cCpoTab                       , "COBRARTABELADO"      })
	aAdd(aCampos, {cCpoEncTab                    , "TAB_ENC_HONOR"       })
	aAdd(aCampos, {cAlsPasta+".PASTA"            , "PASTA"               })
	aAdd(aCampos, {cAlsPasta+".PASTAMAEFILHO"    , "PASTAMAEFILHO"       })
	aAdd(aCampos, {cAlsPasta+".PASTABELADO"      , "PASTABELADO"         })
	aAdd(aCampos, {cAlsPasta+".PASTADESPESA"     , "PASTADESPESA"        })
	aAdd(aCampos, {"PASTA.ENC_HONOR"             , "ENC_HONOR"           })
	aAdd(aCampos, {"PASTA.COD_CLIENTE"           , "COD_CLIENTE"         })
	aAdd(aCampos, {"PASTA.COD_ENDERECO"          , "COD_ENDERECO"        })
	aAdd(aCampos, {"PASTA.SOCIO_RESPONSAVEL"     , "SOCIO_RESPONSAVEL"   })
	aAdd(aCampos, {"PASTA.MOEDA"                 , "MOEDA"               })
	aAdd(aCampos, {"PASTA.ORGNCODIG"             , "ORGNCODIG"           })
	aAdd(aCampos, {"PASTA.IDIOMA"                , "IDIOMA"              })
	aAdd(aCampos, {"PASTA.PASCSITUACAOCADASTRO"  , "PASCSITUACAOCADASTRO"})
	aAdd(aCampos, {"PASTA.REVISADO"              , "REVISADO"            })
	aAdd(aCampos, {"PASTA.MOEDALIMITEHORA"       , "MOEDALIMITEHORA"     })
	aAdd(aCampos, {"PASTA.VALORLIMITEHORA"       , "VALORLIMITEHORA"     })
	aAdd(aCampos, {"PASTA.SALDOINICIALHORA"      , "SALDOINICIALHORA"    })
	aAdd(aCampos, {"PASTA.VALORLIMITEFAT"        , "VALORLIMITEFAT"      })
	aAdd(aCampos, {"PASTA.DATA_BASE"             , "DATA_BASE"           })
	aAdd(aCampos, {"PASTA.MOEDACOND"             , "MOEDACOND"           })
	aAdd(aCampos, {"PASTA.VALORATUALIZADO"       , "VALORATUALIZADO"     })
	aAdd(aCampos, {"PASTA.DATAATUALIZACAO"       , "DATAATUALIZACAO"     })
	aAdd(aCampos, {"PASTA.VALORBASE"             , "VALORBASE"           })
	aAdd(aCampos, {"PASTA.PERCOBEXCEDENTE"       , "PERCOBEXCEDENTE"     })
	aAdd(aCampos, {"PASTA.TIPOCOBEXCE"           , "TIPOCOBEXCE"         })
	aAdd(aCampos, {"PASTA.LIMHORADESC"           , "LIMHORADESC"         })
	aAdd(aCampos, {"PASTA.FINALCASO"             , "FINALCASO"           })
	aAdd(aCampos, {"PASTA.EXCEDENTEFIXO"         , "EXCEDENTEFIXO"       })
	aAdd(aCampos, {"PASTA.TIPO_TABELA"           , "TIPO_TABELA"         })
	aAdd(aCampos, {"PASTA.TIPO_CALCULO"          , "TIPO_CALCULO"        })
	aAdd(aCampos, {"PASTA.CASO_ENC_MES"          , "CASO_ENC_MES"        })
	aAdd(aCampos, {"PASTA.DATAREFINI"            , "DATAREFINI"          })
	aAdd(aCampos, {"PASTA.DATAREFFIM"            , "DATAREFFIM"          })
	aAdd(aCampos, {"PASTA.DATA_VENC"             , "DATA_VENC"           })
	aAdd(aCampos, {"PASTA.TIPOCORRECAO"          , "TIPOCORRECAO"        })
	aAdd(aCampos, {"PASTA.PERCORRECAO"           , "PERCORRECAO"         })
	aAdd(aCampos, {"PASTA.APROVAR_PARCELAS"      , "APROVAR_PARCELAS"    })
	aAdd(aCampos, {"PASTA.PASCOSUSER"            , "PASCOSUSER"          })
	aAdd(aCampos, {"PASTA.TPCOBNCOBRAR"          , "TPCOBNCOBRAR"        })
	aAdd(aCampos, {"PASTA.TPCOBHORA"             , "TPCOBHORA"           })
	aAdd(aCampos, {"PASTA.TPCOBFIXO"             , "TPCOBFIXO"           })
	aAdd(aCampos, {"PASTA.VALORLIMITECASO"       , "VALORLIMITECASO"     })
	aAdd(aCampos, {"PASTA.PARCELAFIXA"           , "PARCELAFIXA"         })
	aAdd(aCampos, {"PASTA.PERCOBFIXO"            , "PERCOBFIXO"          })
	aAdd(aCampos, {"PASTA.IDIOMA_CARTA_COB"      , "IDIOMA_CARTA_COB"    })
	aAdd(aCampos, {"PASTA.TXADM_DESP_TRIB"       , "TXADM_DESP_TRIB"     })
	aAdd(aCampos, {"PASTA.GROSSUP_DESP_TRIB"     , "GROSSUP_DESP_TRIB"   })
	aAdd(aCampos, {"PASTA.FORMA_PGTO"            , "FORMA_PGTO"          })
	aAdd(aCampos, {"PASTA.CTANCODIG"             , "CTANCODIG"           })
	aAdd(aCampos, {"PASTA.DATA_ABERTURA"         , "DATA_ABERTURA"       })
	aAdd(aCampos, {"CLIENTE.COD_RELCARTACOB"     , "COD_RELCARTACOB"     })
	aAdd(aCampos, {"CLIENTE.CLICFATURASAGRUPADAS", "CLICFATURASAGRUPADAS"})
	aAdd(aCampos, {"CLIENTE.GRENCODIGO"          , "GRENCODIGO"          })
	aAdd(aCampos, {"ENDERECO.CC_FATURA"          , "CC_FATURA"           })

Return aCampos

//-------------------------------------------------------------------
/*/{Protheus.doc} VincCaso
Vincula o caso no contrato

@param cContrato , C�digo do Contrato
@param nClientSis, C�digo do Cliente no Sisjuri
@param nLojaSis  , C�digo do Endere�o no Sisjuri
@param nCasoSis  , C�digo da Pasta no Sijuri

@author Bruno Ritter
@since  24/09/2019
/*/
//-------------------------------------------------------------------
Static Function VincCaso(cContrato, nClientSis, nLojaSis, nCasoSis)
	Local cCliLoja  := cValToChar(nClientSis) + '|' + cValToChar(nLojaSis)
	Local cCliCaso  := cValToChar(nClientSis) + '|' + cValToChar(nCasoSis)
	Local cCliProth := StrZero(nClientSis, TamSX3("A1_COD")[1])

	RecLock("NUT", .T.)
	NUT->NUT_CCONTR := cContrato
	NUT->NUT_CCLIEN := cCliProth
	NUT->NUT_CLOJA  := Substr(GetDePara("RCR.ENDERECO", cCliLoja, ""), TamSX3("A1_COD")[1] + 1, TamSX3("A1_LOJA")[1])
	NUT->NUT_CCASO  := StrZero(nCasoSis, TamSX3("NVE_NUMCAS")[1])
	NUT->(MsUnlock())

	MigraDePar('PASTA.CONTRATO', cCliCaso, '', 'COD_CLIENTE|PASTA', "NT0", cContrato, 'NT0_COD')

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} CasPagador
Migra todos pagadores do caso

@param aReg     , Dados do registro Sisjuri
@param cCodContr, C�digo do Contrato

@author Bruno Ritter
@since  19/09/2019
/*/
//-------------------------------------------------------------------
Static Function CasPagador(aReg, cCodContr)
	Local cQuery     := ""
	Local cCliente   := ""
	Local cLoja      := ""
	Local cContato   := ""
	Local nI         := 0
	Local aRetSql    := {}
	Local cCliLoja   := ""
	Local cPasta     := cValToChar(GetValCont("PASTA", aReg))
	Local cClienCas  := cValToChar(GetValCont("COD_CLIENTE", aReg)) + '|' + cPasta
	Local cFormaPag  := Substr(GetDePara("SSJR.CAD_CAS_DIASVENC", cClienCas, ""), 1, TamSX3("E4_CODIGO")[1])
	Local cCartCob   := ""
	Local cMoedaPrt  := Substr(GetDePara("RCR.MOEDA", GetValCont("MOEDA", aReg), ""), 1, TamSX3("CTO_MOEDA")[1])
	Local cContProth := GetDePara("RCR.CONTAS", GetValCont("CTANCODIG", aReg), "")
	Local cBancoPrt  := Substr(cContProth, 1, TamSX3("A6_COD")[1])
	Local cAgencPrt  := Substr(cContProth, TamSX3("A6_COD")[1] + 1, TamSX3("A6_AGENCIA")[1])
	Local cContaPrt  := Substr(cContProth, TamSX3("A6_COD")[1] + TamSX3("A6_AGENCIA")[1] + 1, TamSX3("A2_NUMCON")[1])

	cQuery := " SELECT "
	cQuery +=      " CAD_MULTIPAYER_PASTA.COD_CLIENTE, "
	cQuery +=      " CAD_MULTIPAYER_PASTA.COD_ENDERECO, "
	cQuery +=      " CAD_MULTIPAYER_PASTA.PERCENTUAL, "
	cQuery +=      " CLIENTE.COD_RELCARTACOB "
	cQuery += " FROM SSJR.CAD_MULTIPAYER_PASTA "
	cQuery += " INNER JOIN RCR.CLIENTE "
	cQuery +=         " ON CLIENTE.COD_CLIENTE = CAD_MULTIPAYER_PASTA.COD_CLIENTE "
	cQuery += " WHERE PASTA = " + cPasta
	cQuery += "   AND CAD_MULTIPAYER_PASTA.COD_CLIENTE = " + cValToChar(GetValCont("COD_CLIENTE", aReg))

	TCSetConn(nConnSisJr) // SISJURI
	aRetSql := JurSQL(cQuery, {"COD_CLIENTE", "COD_ENDERECO", "PERCENTUAL", "COD_RELCARTACOB"},.F.,,.T.)

	If Empty(aRetSql)
		aAdd(aRetSql, {GetValCont("COD_CLIENTE", aReg),;
		               GetValCont("COD_ENDERECO", aReg),;
		               100,;
		               GetValCont("COD_RELCARTACOB", aReg)} )
	EndIf

	TCSetConn(nConnProth) // Protheus

	For nI := 1 To Len(aRetSql)
		cCliLoja   := cValToChar(aRetSql[nI][1]) + '|' + cValToChar(aRetSql[nI][2])
		cCliente   := StrZero(aRetSql[nI][1], TamSX3("A1_COD")[1])
		cLoja      := Substr(GetDePara("RCR.ENDERECO", cCliLoja, ""), TamSX3("A1_COD")[1] + 1, TamSX3("A1_LOJA")[1])
		cContato   := Substr(GetDePara("RCR.ENDERECO-(CONTATOS)", cCliLoja, ""), 1, TamSX3("U5_CODCONT")[1])

		If Empty(aRetSql[nI][4])
			cCartCob := ""
		Else
			cCartCob := StrZero(aRetSql[nI][4], TamSX3("NXP_CCARTA")[1]) // "COD_RELCARTACOB"
		EndIf

		RecLock("NXP", .T.)
		NXP->NXP_COD    := GetSxeNum("NXP", "NXP_COD")
		NXP->NXP_CCONTR := cCodContr
		NXP->NXP_CLIPG  := cCliente
		NXP->NXP_LOJAPG := cLoja
		NXP->NXP_PERCEN := aRetSql[nI][3]
		NXP->NXP_DESPAD := 0
		NXP->NXP_CCONT  := cContato
		NXP->NXP_FPAGTO := GetValCont("FORMA_PGTO", aReg)
		NXP->NXP_CCDPGT := cFormaPag
		NXP->NXP_CBANCO := cBancoPrt
		NXP->NXP_CAGENC := cAgencPrt
		NXP->NXP_CCONTA := cContaPrt
		NXP->NXP_CMOE   := cMoedaPrt
		NXP->NXP_CRELAT := '' // NOTE: N�o existe esse cadastro no Sisjuri
		NXP->NXP_CIDIO  := GetValCont("IDIOMA", aReg)
		NXP->NXP_CIDIO2 := GetValCont("IDIOMA_CARTA_COB", aReg)
		NXP->NXP_CCARTA := cCartCob
		NXP->NXP_TXADM  := GetValCont("TXADM_DESP_TRIB", aReg)
		NXP->NXP_GROSUP := GetValCont("GROSSUP_DESP_TRIB", aReg)
		NXP->(MsUnlock())

		If nI == 1
			CasEncamin(GetValCont("COD_CLIENTE", aReg),;
			           GetValCont("PASTA", aReg),;
			           cCliente, cLoja, cCodContr)
		EndIf
	Next nI

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} CasoTpDesp
Migra os tipos de despesas n�o cobr�veis do contrato

@param aReg     , Dados do registro Sisjuri
@param cCodContr, C�digo do Contrato

@author Bruno Ritter
@since  19/09/2019
/*/
//-------------------------------------------------------------------
Static Function CasoTpDesp(aReg, cCodContr)
	Local cQuery     := ""
	Local nI         := 0
	Local aRetSql    := {}

	cQuery := " SELECT "
	cQuery +=      " TIPO "
	cQuery += " FROM SSJR.CAD_PASTA_TIPODESP "
	cQuery += " WHERE COD_CLIENTE = " + cValToChar(GetValCont("COD_CLIENTE", aReg))
	cQuery +=   " AND PASTA = " + cValToChar(GetValCont("PASTA", aReg))

	TCSetConn(nConnSisJr) // SISJURI
	aRetSql := JurSQL(cQuery, {"TIPO"},.F.,,.T.)

	TCSetConn(nConnProth) // Protheus
	For nI := 1 To Len(aRetSql)
		RecLock("NTK", .T.)
		NTK->NTK_CCONTR := cCodContr
		NTK->NTK_CTPDSP := StrZero(aRetSql[nI][1], TamSX3("NRH_COD")[1])
		NTK->(MsUnlock())
	Next nI

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} CasoAtivid
Migra os tipos de atividades n�o cobr�veis do contrato

@param aReg     , Dados do registro Sisjuri
@param cCodContr, C�digo do Contrato

@author Bruno Ritter
@since  19/09/2019
/*/
//-------------------------------------------------------------------
Static Function CasoAtivid(aReg, cCodContr)
	Local cQuery     := ""
	Local nI         := 0
	Local aRetSql    := {}

	cQuery := " SELECT "
	cQuery +=      " TIPO "
	cQuery += " FROM SSJR.CAD_PASTA_TIPOATIV "
	cQuery += " WHERE COD_CLIENTE = " + cValToChar(GetValCont("COD_CLIENTE", aReg))
	cQuery +=   " AND PASTA = " + cValToChar(GetValCont("PASTA", aReg))

	TCSetConn(nConnSisJr) // SISJURI
	aRetSql := JurSQL(cQuery, {"TIPO"},.F.,,.T.)

	TCSetConn(nConnProth) // Protheus
	For nI := 1 To Len(aRetSql)
		RecLock("NTJ", .T.)
		NTJ->NTJ_CCONTR := cCodContr
		NTJ->NTJ_CTPATV := StrZero(aRetSql[nI][1], TamSX3("NRC_COD")[1])
		NTJ->(MsUnlock())
	Next nI

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} CasParcela
Migra as Parcela de Fixo

@param aReg     , Dados do registro Sisjuri
@param cCodContr, C�digo do Contrato

@author Bruno Ritter
@since  19/09/2019
/*/
//-------------------------------------------------------------------
Static Function CasParcela(aReg, cCodContr)
	Local cQuery     := ""
	Local nI         := 0
	Local cMoedaPrt  := Substr(GetDePara("RCR.MOEDA", GetValCont("MOEDACOND", aReg), ""), 1, TamSX3("CTO_MOEDA")[1])
	Local aRetSql    := {}

	cQuery := " SELECT "
	cQuery +=      " SEQUENCIA, TIPO_FATURA    , DATAINICIAL    , DATAFINAL,"
	cQuery +=      " VALORBASE, VALORATUALIZADO, DATAATUALIZACAO, DATAVENCIMENTO,"
	cQuery +=      " DESCRICAO, SITUACAO       , APROVADO       , INSTRUCOES_REVISOR"
	cQuery += " FROM SSJR.FAT_CONDVENCIMENTOS "
	cQuery += " WHERE COD_CLIENTE = " + cValToChar(GetValCont("COD_CLIENTE", aReg))
	cQuery +=   " AND PASTA = " + cValToChar(GetValCont("PASTA", aReg))

	TCSetConn(nConnSisJr) // SISJURI
	aRetSql := JurSQL(cQuery, {"SEQUENCIA" , "TIPO_FATURA"    , "DATAINICIAL"    , "DATAFINAL",;
	                           "VALORBASE" , "VALORATUALIZADO", "DATAATUALIZACAO", "DATAVENCIMENTO",;
	                           "DESCRICAO" , "SITUACAO"       , "APROVADO"       , "INSTRUCOES_REVISOR"},.F.,,.T.)

	TCSetConn(nConnProth) // Protheus
	For nI := 1 To Len(aRetSql)
		RecLock("NT1", .T.)
		NT1->NT1_CCONTR := cCodContr
		NT1->NT1_SEQUEN := GetSxEnum("NT1", "NT1_SEQUEN")
		NT1->NT1_PARC   := Iif(aRetSql[nI][2] == 0, "", StrZero(aRetSql[nI][1], TamSX3("NT1_PARC")[1])) // SEQUENCIA
		NT1->NT1_CTPFTU := Iif(aRetSql[nI][2] == 0, "", StrZero(aRetSql[nI][2], TamSX3("NR9_COD")[1])) // TIPO_FATURA
		NT1->NT1_DATAIN := aRetSql[nI][3] // DATAINICIAL
		NT1->NT1_DATAFI := aRetSql[nI][4] // DATAFINAL
		NT1->NT1_VALORB := aRetSql[nI][5] // VALORBASE
		NT1->NT1_VALORA := aRetSql[nI][6] // VALORATUALIZADO
		NT1->NT1_DATAAT := aRetSql[nI][7] // DATAATUALIZACAO
		NT1->NT1_DATAVE := aRetSql[nI][8] // DATAVENCIMENTO
		NT1->NT1_DESCRI := SubStr(aRetSql[nI][9], 1, TamSX3("NT1_DESCRI")[1]) // DESCRICAO
		NT1->NT1_SITUAC := Iif(aRetSql[nI][10] == "N", "1", "2") // SITUACAO
		NT1->NT1_CPREFT := ""
		NT1->NT1_CMOEDA := cMoedaPrt
		NT1->NT1_QTDADE := 0
		NT1->NT1_COTAC1 := 1
		NT1->NT1_COTAC2 := 1
		NT1->NT1_COTAC  := 1
		NT1->NT1_REVISA := Iif(aRetSql[nI][11] == "S", "1", "2") // APROVADO
		NT1->NT1_INSREV := SubStr(aRetSql[nI][12], 1, TamSX3("NT1_INSREV")[1]) // INSTRUCOES_REVISOR
		NT1->NT1_ACAOLD := ""
		NT1->(MsUnlock())
	Next nI

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} CasoFaixa
Migra as Faixas de Faturamento do caso

@param aReg     , Dados do registro Sisjuri
@param cCodContr, C�digo do Contrato

@author Bruno Ritter
@since  23/09/2019
/*/
//-------------------------------------------------------------------
Static Function CasoFaixa(aReg, cCodContr)
	Local cQuery     := ""
	Local nI         := 0
	Local aRetSql    := {}
	Local nTamInt    := TamSX3("NTR_VLFIM")[1] - TamSX3("NTR_VLFIM")[2] - 1
	Local nMaxCpo    := Val(Replicate("9", nTamInt))

	cQuery := " SELECT "
	cQuery +=      " VALOR_INICIAL, "
	cQuery +=      " VALOR_FINAL, "
	               // Converte para poder diferenciar o null do 0, 
	               // pois o dbAcess preenche com 0 quando o campo n�merico est� null
	cQuery +=      " TO_CHAR(VALOR_COBRAR) VALOR_COBRAR, "
	cQuery +=      " TO_CHAR(VALOR_UNITARIO) VALOR_UNITARIO, "
	cQuery +=      " TO_CHAR(PORC_COBRAR) PORC_COBRAR, "
	cQuery +=      " TABELAH "
	cQuery += " FROM SSJR.CAD_TABELAHORA "
	cQuery += " WHERE COD_CLIENTE = " + cValToChar(GetValCont("COD_CLIENTE", aReg))
	cQuery +=   " AND PASTA = " + cValToChar(GetValCont("PASTA", aReg))

	TCSetConn(nConnSisJr) // SISJURI
	aRetSql := JurSQL(cQuery, {"VALOR_INICIAL", "VALOR_FINAL", "VALOR_COBRAR", "VALOR_UNITARIO", "PORC_COBRAR", "TABELAH"},.F.,,.T.)

	TCSetConn(nConnProth) // Protheus
	For nI := 1 To Len(aRetSql)
		RecLock("NTR", .T.)
		NTR->NTR_COD    := GETSXENUM("NTR","NTR_COD")
		NTR->NTR_CCONTR := cCodContr
		NTR->NTR_VLINI  := aRetSql[nI][1] // VALOR_INICIAL
		NTR->NTR_VLFIM  := Iif(aRetSql[nI][2] > nMaxCpo, nMaxCpo, aRetSql[nI][2]) // VALOR_FINAL
		Do Case
			Case !Empty(aRetSql[nI][3])
				NTR->NTR_TPVL   := "1" // Valor fixo
				NTR->NTR_VALOR  := Val(aRetSql[nI][3]) // VALOR_COBRAR

			Case !Empty(aRetSql[nI][4])
				NTR->NTR_TPVL   := "2" // Valor unit�rio
				NTR->NTR_VALOR  := Val(aRetSql[nI][4]) // VALOR_UNITARIO

			Case !Empty(aRetSql[nI][5])
				NTR->NTR_TPVL   := "3" // Percentual a cobrar
				NTR->NTR_VALOR  := Val(aRetSql[nI][5]) // PORC_COBRAR
	
			Case !Empty(aRetSql[nI][6])
					NTR->NTR_TPVL   := "4" // Tab Honorarios
					NTR->NTR_CTABH  := aRetSql[nI][6] // TABELAH
		EndCase

		NTR->(MsUnlock())
	Next nI

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} IsCasoMae
Verificar se � um caso m�e e deve criar um contrato

@param aReg, Dados do registro Sisjuri

@return lRet, Se � um caso m�e

@author Bruno Ritter
@since  25/09/2019
/*/
//-------------------------------------------------------------------
Static Function IsCasoMae(aReg)
	Local lRet := GetValCont("PASTAMAEFILHO", aReg) == GetValCont("PASTA", aReg)

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} IsCasoExc
Verifica se tem uma exce��o na cobran�a de tabelado e ou despesa e
se deve gerar um contrato para apenas esse caso

@param aReg, Dados do caso Sisjuri

@return lRet, Se � um caso com exce��o de cobran�a

@author Bruno Ritter
@since  25/09/2019
/*/
//-------------------------------------------------------------------
Static Function IsCasoExc(aReg)
	Local lRet := .F.

	If GetValCont("PASTAMAEFILHO", aReg) > 0; // Existe um caso m�e
	   .And. GetValCont("PASTAMAEFILHO", aReg) != GetValCont("PASTA", aReg) // Se o pr�prio caso � m�e, ent�o ele deveria ter exce��o de cobran�a

		If GetValCont("PASTABELADO", aReg) > 0 ; // Existe exce��o na cobran�a do Tabelado
		   .And. GetValCont("PASTABELADO", aReg) != GetValCont("PASTAMAEFILHO", aReg) // Diferente do caso M�e, pois se for igual N�O � uma exce��o
			lRet := .T.

		ElseIf GetValCont("PASTADESPESA", aReg) > 0 ; // Existe exce��o na cobran�a da Despesa
		   .And. GetValCont("PASTADESPESA", aReg) != GetValCont("PASTAMAEFILHO", aReg) // Diferente do caso M�e, pois se for igual N�O � uma exce��o
			lRet := .T.

		EndIf

	ElseIf GetValCont("PASTAMAEFILHO", aReg) == -1 // N�O existe caso m�e

		If GetValCont("PASTABELADO", aReg) > 0 ; // Existe exce��o na cobran�a do Tabelado
		   .And. GetValCont("PASTABELADO", aReg) != GetValCont("PASTA", aReg) // Diferente do pr�prio caso, pois se for igual N�O � uma exce��o
			lRet := .T.

		ElseIf GetValCont("PASTADESPESA", aReg) > 0 ; // Existe exce��o na cobran�a da Despesa
		   .And. GetValCont("PASTADESPESA", aReg) != GetValCont("PASTA", aReg) // Diferente do pr�prio caso, pois se for igual N�O � uma exce��o
			lRet := .T.

		EndIf
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} IsCasoFilh
Verifica se o caso � filho de algum caso m�e e n�o tem uma 
exce��o na cobran�a de tabelado e ou despesa

@param aReg, Dados do caso Sisjuri

@return lRet, Se pode vincular em um caso m�e

@author Bruno Ritter
@since  25/09/2019
/*/
//-------------------------------------------------------------------
Static Function IsCasoFilh(aReg)
	Local lRet := .T.

	If       GetValCont("PASTAMAEFILHO", aReg) > 0;                                  // Existe um caso m�e
	  .And.  GetValCont("PASTAMAEFILHO", aReg) != GetValCont("PASTA", aReg);         // Ele n�o � o caso m�e
	  .And. (GetValCont("PASTABELADO"  , aReg) == -1;                                // Ele n�o tem exce��o na cobran�a de Tabelado
	  .Or.   GetValCont("PASTABELADO"  , aReg) == GetValCont("PASTAMAEFILHO", aReg));// Ou se tem exce��o, essa exce��o � igual ao caso m�e (que � a mesma coisa de n�o ter exce��o)
	  .And. (GetValCont("PASTADESPESA" , aReg) == -1;                                // Ele n�o tem exce��o na cobran�a de Despesa
	  .Or.   GetValCont("PASTADESPESA" , aReg) == GetValCont("PASTAMAEFILHO", aReg)) // Ou se tem exce��o, essa exce��o � igual ao caso m�e (que � a mesma coisa de n�o ter exce��o)
		lRet := .T.
	Else
		lRet := .F.
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} IsMistoMin
Verifica se o caso m�e de Misto ou m�nimo

@param aReg, Dados do caso Sisjuri

@return lRet, Se � para gerar um contrato para esse caso de misto ou m�nimo

@author Bruno Ritter
@since  25/09/2019
/*/
//-------------------------------------------------------------------
Static Function IsMistoMin(aReg)
	Local lRet := .T.

	If       GetValCont("PASTAMAEFILHO" , aReg) == -1                       ; // N�O existe um caso m�e
	  .And.  GetValCont("TPCOBNCOBRAR"  , aReg) == "N"                      ;
	  .And.  GetValCont("TPCOBHORA"     , aReg) == "S"                      ;
	  .And.  GetValCont("TPCOBFIXO"     , aReg) == "S"                      ; // � Misto ou M�nimo
	  .And. (GetValCont("CONTRATO"      , aReg) == GetValCont("PASTA", aReg); // O agrupador de de contrato Misto M�nimo � esse caso
	  .Or.   GetValCont("CONTRATO"      , aReg) == 0                        ;
	  .Or.   Empty(GetValCont("CONTRATO", aReg)))                             // Ou se n�o tem agrupador, deve gerar contrato apenas para esse caso
		lRet := .T.
	Else
		lRet := .F.
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} IsMMFilho
Verifica se � filho de algum caso de Misto ou m�nimo

@param aReg, Dados do caso Sisjuri

@return lRet, Se deve vincular a algum caso misto ou m�nimo

@author Bruno Ritter
@since  25/09/2019
/*/
//-------------------------------------------------------------------
Static Function IsMMFilho(aReg)
	Local lRet := .T.

	If GetValCont("PASTAMAEFILHO", aReg) == -1; // N�O existe um caso m�e
	  .And. !Empty(GetValCont("CONTRATO", aReg)) .And. GetValCont("CONTRATO", aReg) > 0; // Tem um caso de misto ou m�mimo agrupando
	  .And. GetValCont("CONTRATO", aReg) != GetValCont("PASTA", aReg) // O caso de agrupamento n�o � ele mesmo
		lRet := .T.
	Else
		lRet := .F.
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} IsEqualCas
Veririfica se o caso atual "aReg" ainda pertence no mesmo agrupamento

@param aReg   , Dados do caso Sisjuri
@param aRegAnt, Dados do caso Sisjuri anterior

@return lRet, Se pode agrupar com anterior

@author Bruno Ritter
@since  25/09/2019
/*/
//-------------------------------------------------------------------
Static Function IsEqualCas(aReg, aRegAnt)
	Local lRet := .T.

	If GetValCont("COD_CLIENTE", aReg)             == GetValCont("COD_CLIENTE", aRegAnt);
	   .And. GetValCont("SOCIO_RESPONSAVEL", aReg) == GetValCont("SOCIO_RESPONSAVEL", aRegAnt);
	   .And. GetValCont("IDIOMA", aReg)            == GetValCont("IDIOMA", aRegAnt);
	   .And. GetValCont("MOEDA", aReg)             == GetValCont("MOEDA", aRegAnt);
	   .And. GetValCont("ORGNCODIG", aReg)         == GetValCont("ORGNCODIG", aRegAnt);
	   .And. GetValCont("COD_REL", aReg)           == GetValCont("COD_REL", aRegAnt);
	   .And. GetValCont("COD_ENDERECO", aReg)      == GetValCont("COD_ENDERECO", aRegAnt);
	   .And. GetValCont("FORMA_PGTO", aReg)        == GetValCont("FORMA_PGTO", aRegAnt);
	   .And. GetValCont("CTANCODIG", aReg)         == GetValCont("CTANCODIG", aRegAnt);
	   .And. GetValCont("TIPOATIV", aReg)          == GetValCont("TIPOATIV", aRegAnt);
	   .And. GetValCont("TIPODESP", aReg)          == GetValCont("TIPODESP", aRegAnt);
	   .And. GetValCont("MULTIPAYER", aReg)        == GetValCont("MULTIPAYER", aRegAnt);
	   .And. GetValCont("VENCIMENTO", aReg)        == GetValCont("VENCIMENTO", aRegAnt);
	   .And. GetValCont("ENCAMINHAMENTO", aReg)    == GetValCont("ENCAMINHAMENTO", aRegAnt);
	   .And. GetValCont("MOEDALIMITEHORA", aReg)   == GetValCont("MOEDALIMITEHORA", aRegAnt);
	   .And. GetValCont("VALORLIMITEHORA", aReg)   == GetValCont("VALORLIMITEHORA", aRegAnt);
	   .And. GetValCont("SALDOINICIALHORA", aReg)  == GetValCont("SALDOINICIALHORA", aRegAnt);
	   .And. GetValCont("VALOR_DISPONIVEL", aReg)  == GetValCont("VALOR_DISPONIVEL", aRegAnt);
	   .And. GetValCont("VALORLIMITEFAT", aReg)    == GetValCont("VALORLIMITEFAT", aRegAnt);
	   .And. GetValCont("TIPO_TABELA", aReg)       == GetValCont("TIPO_TABELA", aRegAnt);
	   .And. GetValCont("TIPO_CALCULO", aReg)      == GetValCont("TIPO_CALCULO", aRegAnt);
	   .And. GetValCont("GROSSUP_DESP_TRIB", aReg) == GetValCont("GROSSUP_DESP_TRIB", aRegAnt);
	   .And. GetValCont("TXADM_DESP_TRIB", aReg)   == GetValCont("TXADM_DESP_TRIB", aRegAnt)

		lRet := .T.

	Else
		lRet := .F.
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} IsEqualCas
Veririfica se o caso atual "aReg" ainda pertence na mesma jun��o
de contrato que o registro anterior

@param aReg   , Dados do caso de fixo Sisjuri
@param aRegAnt, Dados do caso de fixo Sisjuri anterior

@return lRet, Se pode manter na mesma jun��o

@author Bruno Ritter
@since  25/09/2019
/*/
//-------------------------------------------------------------------
Static Function IsEqualJnc(aReg, aRegAnt)
	Local lRet := .T.

	If GetValCont("COD_CLIENTE", aReg)             == GetValCont("COD_CLIENTE", aRegAnt);
	   .And. GetValCont("MOEDA", aReg)             == GetValCont("MOEDA", aRegAnt);
	   .And. GetValCont("CTANCODIG", aReg)         == GetValCont("CTANCODIG", aRegAnt);
	   .And. GetValCont("ORGNCODIG", aReg)         == GetValCont("ORGNCODIG", aRegAnt);
	   .And. GetValCont("SOCIO_RESPONSAVEL", aReg) == GetValCont("SOCIO_RESPONSAVEL", aRegAnt);
	   .And. GetValCont("IDIOMA", aReg)            == GetValCont("IDIOMA", aRegAnt);
	   .And. GetValCont("MULTIPAYER", aReg)        == GetValCont("MULTIPAYER", aRegAnt);
	   .And. GetValCont("ENCAMINHAMENTO", aReg)    == GetValCont("ENCAMINHAMENTO", aRegAnt);
	   .And. GetValCont("VENCIMENTO", aReg)        == GetValCont("VENCIMENTO", aRegAnt);
	   .And. GetValCont("COD_REL", aReg)           == GetValCont("COD_REL", aRegAnt)

		lRet := .T.

	Else
		lRet := .F.
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} GetDadosEx
Gera um novo registro para poder criar um contrato mesclando
os dados das pastas com as exce��es de cobran�a e o caso m�e

@param aReg, Dados do caso Sisjuri

@return aNewReg, No registro para gerar contrato com os dados mesclados

@author Bruno Ritter
@since  26/09/2019
/*/
//-------------------------------------------------------------------
Static Function GetDadosEx(aReg)
	Local cQuery     := ""
	Local nCpo       := 0
	Local aNewReg    := {}
	Local lTemCasMae := GetValCont("PASTAMAEFILHO", aReg) > 0
	Local cAlsPasta  := Iif(lTemCasMae, "FILHO", "PASTA")
	Local aCampoSis  := GetCpoCont(.T., lTemCasMae)

	cQuery := " SELECT "
	For nCpo := 1 To Len(aCampoSis)
		cQuery += aCampoSis[nCpo][1] + " " + aCampoSis[nCpo][2] + Iif(nCpo == Len(aCampoSis), " ", ", ")
	Next nCpo
	cQuery += " FROM RCR.PASTA " + cAlsPasta
	If lTemCasMae
		cQuery += " INNER JOIN RCR.PASTA PASTA " 
		cQuery +=         " ON PASTA.COD_CLIENTE = " + cAlsPasta + ".COD_CLIENTE "
		cQuery +=        " AND PASTA.PASTA = " + cAlsPasta + ".PASTAMAEFILHO "
	EndIf
	cQuery += " INNER JOIN RCR.ENDERECO "
	cQuery +=         " ON PASTA.COD_CLIENTE= ENDERECO.COD_CLIENTE "
	cQuery +=        " AND PASTA.COD_ENDERECO = ENDERECO.COD_ENDERECO "
	cQuery += " INNER JOIN RCR.CLIENTE "
	cQuery +=         " ON CLIENTE.COD_CLIENTE = PASTA.COD_CLIENTE "
	cQuery += " WHERE " + cAlsPasta + ".PASTA = " + cValToChar(GetValCont("PASTA", aReg))
	cQuery +=   " AND " + cAlsPasta + ".COD_CLIENTE = " + cValToChar(GetValCont("COD_CLIENTE", aReg))

	aNewReg := JurSql(cQuery, "*",.F.,,.T.)[1]

Return aNewReg

//-------------------------------------------------------------------
/*/{Protheus.doc} GetHashMap
Gera um HashMap do array com os campos da query do contrato

@return oHash, objeto do tipo HashMap

@author Bruno Ritter
@since  27/09/2019
/*/
//-------------------------------------------------------------------
Static Function GetHashMap()
	Local aCpoSis := GetCpoCont()
	Local oHash   := HMNew()
	Local nI      := 0

	For nI := 1 To Len(aCpoSis)
		HMAdd(oHash, {aCpoSis[nI][2], nI})
	Next nI

Return oHash

//-------------------------------------------------------------------
/*/{Protheus.doc} MigraJnc
Migra a jun��o de contratos do Sisjuri que s�o fixo

@param aContratos, Array com os contratos para gerar uma jun��o

@author Bruno Ritter
@since  27/09/2019
/*/
//-------------------------------------------------------------------
Static Function MigraJnc(aContratos)
	Local cCodJuncao := GetSxeNum("NW2", "NW2_COD")
	Local nI         := 0
	Local cQrySeq    := ""
	Local cSeqCli    := ""

	NT0->(DbSetOrder(1)) // NT0_FILIAL + NT0_COD
	If NT0->(DbSeek(xFilial("NT0") + aContratos[1]))

		cQrySeq  := " SELECT COUNT(NW2.R_E_C_N_O_) CONTA"
		cQrySeq  += " FROM " + RetSqlName("NW2") + " NW2 "
		cQrySeq  += " WHERE NW2.NW2_FILIAL = '" + xFilial("NW2") + "' "
		cQrySeq  +=   " AND NW2.NW2_CCLIEN = '" + NT0->NT0_CCLIEN + "' "
		cQrySeq  +=   " AND NW2.D_E_L_E_T_ = ' ' "

		cSeqCli := cValToChar(JurSql(cQrySeq, "CONTA")[1][1])

		RecLock("NW2", .T.)
		NW2->NW2_COD    := cCodJuncao
		NW2->NW2_DESC   := "Jun��o para o Cliente: '" + NT0->NT0_CCLIEN + "' - " + Soma1(cSeqCli)
		NW2->NW2_CCLIEN := NT0->NT0_CCLIEN
		NW2->NW2_CLOJA  := NT0->NT0_CLOJA
		NW2->NW2_CCONSU := NT0->NT0_COD
		NW2->NW2_CMOE   := NT0->NT0_CMOE
		NW2->NW2_CESCR  := NT0->NT0_CESCR
		NW2->NW2_CPART  := NT0->NT0_CPART1
		NW2->NW2_CIDIO  := NT0->NT0_CIDIO
		NW2->NW2_CTIPOF := NT0->NT0_CTIPOF
		NW2->NW2_RELPRE := NT0->NT0_RELPRE
		NW2->NW2_DISCAS := NT0->NT0_DISCAS
		NW2->(MsUnlock())

		MigPagJnc(cCodJuncao, NT0->NT0_COD)

		For nI := 1 To Len(aContratos)
			RecLock("NW3", .T.)
			NW3->NW3_CJCONT := cCodJuncao
			NW3->NW3_CCONTR := aContratos[nI]
			NW3->(MsUnlock())
		Next nI
	EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} MigPagJnc
Migra os pagadores e seus encaminhamentos para uma jun��o com base em um contrato

@param cCodJuncao, C�digo da jun��o para vincular os pagadoares
@param cContrBase, C�digo do contrato para copiar os pagadore/encaminhamentos

@author Bruno Ritter
@since  27/09/2019
/*/
//-------------------------------------------------------------------
Static Function MigPagJnc(cCodJuncao, cContrBase)
	Local nI         := 0
	Local cQryPag    := ""
	Local aPagadores := {}
	Local nX         := 0
	Local cQryEnc    := ""
	Local aEncaminh := {}

	cQryPag  := " SELECT "
	cQryPag  +=       " NXP_CLIPG , NXP_LOJAPG, NXP_PERCEN, NXP_CCONT ,"
	cQryPag  +=       " NXP_FPAGTO, NXP_CCDPGT, NXP_CBANCO, NXP_CAGENC,"
	cQryPag  +=       " NXP_CCONTA, NXP_CMOE  , NXP_CIDIO , NXP_CIDIO2,"
	cQryPag  +=       " NXP_CCARTA, NXP_TXADM , NXP_GROSUP "
	cQryPag  += " FROM " + RetSqlName("NXP") + " NXP"
	cQryPag  += " WHERE NXP.NXP_FILIAL = '" + xFilial("NXP") + "' "
	cQryPag  +=   " AND NXP.NXP_CCONTR = '" + cContrBase + "' "
	cQryPag  +=   " AND NXP.D_E_L_E_T_ = ' ' "

	aPagadores := JurSql(cQryPag, {"NXP_CLIPG", "NXP_LOJAPG", "NXP_PERCEN", "NXP_CCONT ";
	                             ,"NXP_FPAGTO", "NXP_CCDPGT", "NXP_CBANCO", "NXP_CAGENC";
	                             , "NXP_CCONTA", "NXP_CMOE  ", "NXP_CIDIO ", "NXP_CIDIO2";
	                             , "NXP_CCARTA", "NXP_TXADM ", "NXP_GROSUP"})

	For nI := 1 To Len(aPagadores)
		RecLock("NXP", .T.)
		NXP->NXP_COD    := GetSxeNum("NXP", "NXP_COD")
		NXP->NXP_CJCONT := cCodJuncao
		NXP->NXP_CLIPG  := aPagadores[nI][1]
		NXP->NXP_LOJAPG := aPagadores[nI][2]
		NXP->NXP_PERCEN := aPagadores[nI][3]
		NXP->NXP_CCONT  := aPagadores[nI][4]
		NXP->NXP_FPAGTO := aPagadores[nI][5]
		NXP->NXP_CCDPGT := aPagadores[nI][6]
		NXP->NXP_CBANCO := aPagadores[nI][7]
		NXP->NXP_CAGENC := aPagadores[nI][8]
		NXP->NXP_CCONTA := aPagadores[nI][9]
		NXP->NXP_CMOE   := aPagadores[nI][10]
		NXP->NXP_CIDIO  := aPagadores[nI][11]
		NXP->NXP_CIDIO2 := aPagadores[nI][12]
		NXP->NXP_CCARTA := aPagadores[nI][13]
		NXP->NXP_TXADM  := aPagadores[nI][14]
		NXP->NXP_GROSUP := aPagadores[nI][15]
		NXP->(MsUnlock())

		cQryEnc  := " SELECT "
		cQryEnc  +=    " NVN.NVN_CLIPG, NVN.NVN_LOJPG, NVN.NVN_CCONT, NVN.NVN_COD, NVN.NVN_DESC""
		cQryEnc  += " FROM " + RetSqlName("NVN") + " NVN "
		cQryEnc  += " WHERE NVN.NVN_FILIAL = '" + xFilial("NVN") + "' "
		cQryEnc  +=   " AND NVN.NVN_CCONTR = '" + cContrBase + "' "
		cQryEnc  +=   " AND NVN.NVN_CLIPG = '" + NXP->NXP_CLIPG + "' "
		cQryEnc  +=   " AND NVN.NVN_LOJPG = '" + NXP->NXP_LOJAPG + "' "
		cQryEnc  +=   " AND NVN.D_E_L_E_T_ = ' ' "

		aEncaminh := JurSql(cQryEnc, {"NVN_CLIPG", "NVN_LOJPG", "NVN_CCONT", "NVN_COD", "NVN_DESC"})

		For nX := 1 To Len(aEncaminh)
			RecLock("NVN", .T.)
			NVN->NVN_CJCONT := cCodJuncao
			NVN->NVN_CLIPG  := aEncaminh[nX][1]
			NVN->NVN_LOJPG  := aEncaminh[nX][2]
			NVN->NVN_CCONT  := aEncaminh[nX][3]
			NVN->NVN_COD    := aEncaminh[nX][4]
			NVN->NVN_DESC   := aEncaminh[nX][5]
			NVN->(MsUnlock())
		Next nI
	Next nI
Return Nil
