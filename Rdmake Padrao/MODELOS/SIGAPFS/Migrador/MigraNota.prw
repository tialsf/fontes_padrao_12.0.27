#Include "Totvs.Ch"
#Include "XmlXFun.Ch"
#Include "TbiConn.Ch"

Static oHashCpo   := GetHashMap()

//-------------------------------------------------------------------
/*/{Protheus.doc} MigraNota
Migra as notas fiscais de saida

@param oProcess  , Objeto do processa
@param nConnSisJr, Conex�o do SisJuri
@param nConnProth, Conex�o do Protheus

@author Leonardo Miranda
@since  25/09/2019
/*/
//-------------------------------------------------------------------
 
User Function MigraNota(oProcess, nConnSisJr, nConnProth)

 Local	cError		:= ""
 Local	cWarning	:= ""
 Local	cXmlFile	:= ''
 Local 	oXml		:= Nil
 Local	oScript		:= Nil
 Local	cEmp		:= "99"
 Local	cFil		:= "01"
 Local	cNomeArq	:= ""
 Local	cCaminho	:= ""
 Local	cCnpjCli	:= ""
 Local	aCabec		:= {}
 Local	aLinha		:= {}
 Local	aItens		:= {}
 Local	dEmissao	:= Ctod("")
 Local	nX			:= 0
 Local	nI			:= 0
 Local	cLstSrv		:= ""
 Local	cItem		:= ""
 Local	cProd		:= ""
 Local	cUM			:= ""
 Local	cDesc		:= ""
 Local	nQuant		:= 0
 Local	nValor		:= 0
 Local	nTotal		:= 0 
 Local	cCfop		:= ""
 Local	nAlqIss		:= 0
 Local	oModel		:= Nil
 Local	oSB1Mod		:= Nil
 Local	oSB5Mod		:= Nil
 Local	aErro		:= {}
 Local	cMessage	:= ""
 Local	lOk			:= .T.
 Local	cNota		:= ""
 Local	cSerie		:= ""
 Local	aRetSql		:= {}
 Local	cQuery		:= ""
 Local	nTotNotas	:= 0
 Local	aReg		:= {} 
 
 Private lMsErroAuto := .F.
 Private lMsHelpAuto := .T. 
  
 cNomeArq	:= (GetNextAlias()+".xml")
 cCaminho	:= GetSrvProfString("RootPath", "\undefined") + GetSrvProfString("StartPath", "\undefined")

 If At("undefined",cCaminho,1)
    MsgStop("Falha ao definir caminho para gerar o arquivo XML")
    Return
 EndIf

 oProcess:IncRegua1("Nota Fiscal Saida")
 TCSetConn(nConnSisJr) // SISJURI
 cQuery := " SELECT ORGNCODIG       							ORGNCODIG		,"
 cQuery += "        NUMERONF        							NUMERONF		,"
 cQuery += "        SERIENF         							SERIENF			,"
 cQuery += "        dbms_lob.substr(NFEASSINADA, 4000, 1 )		XML1			,"
 cQuery += "        dbms_lob.substr(NFEASSINADA, 4000, 4001 )	XML2		    ,"
 cQuery += "        CHAVENFE		 							CHAVENFE		 "
 cQuery += " FROM RCR.NOTAFISCAL_NFE "
 cQuery += " WHERE   CODSTATUS = '100'"
 cQuery += "    AND MOTIVO     = 'Autorizado o uso da NF-e'"
 cQuery += " ORDER BY ORGNCODIG,SERIENF,NUMERONF,NUMEROLOTE"
 aRetSql := JurSQL(cQuery, "*",.F.,,.T.)
 
 nTotNotas := Len(aRetSql)
 oProcess:SetRegua2(nTotNotas)
 TCSetConn(nConnProth) // Protheus
 For nI := 1 To nTotNotas
    oProcess:IncRegua2(i18n("Importando registro #1 de #2", {nI,nTotNotas}))
    aReg		:= aRetSql[nI]
    cXmlFile	:= Alltrim(GetValCont("XML1", aReg))+Alltrim(GetValCont("XML2", aReg))
 
    oScript := XmlParser( cXmlFile, "_", @cError, @cWarning )
    If (oScript == NIL )
    	MsgStop("Falha ao carregar Objeto XML : "+cError+" / "+cWarning)
    	Return
    Endif
 
    Save oScript XMLFILE (cCaminho+cNomeArq)
    oXml := XmlParserFile((GetSrvProfString("StartPath", "\undefined")+cNomeArq),"_",@cError,@cWarning)
    XmlNode2Arr( oXml:_NFE, "_NFE" )

    cCnpjCli := oXml:_nfe[1]:_infnfe:_dest:_cnpj:text
 
    SA1->(DbSetOrder(3))
    If !SA1->(DbSeek(xFilial("SA1")+cCnpjCLi))
    	MsgStop("Cadastro de cliente n�o encontrador.")
    	Return() 
    EndIf 
    
    dDataBase:= Stod(StrTran(Substr(oXml:_nfe[1]:_infnfe:_IDE:_DHEMI:TEXT,1,(At("T",oXml:_nfe[1]:_infnfe:_IDE:_DHEMI:TEXT)-1)),"-","")) 
    dEmissao := Stod(StrTran(Substr(oXml:_nfe[1]:_infnfe:_IDE:_DHEMI:TEXT,1,(At("T",oXml:_nfe[1]:_infnfe:_IDE:_DHEMI:TEXT)-1)),"-",""))
    cChave   := StrTran(Upper(Alltrim(OXML:_NFE[1]:_INFNFE:_ID:TEXT)),"NFE","")
    cNota	  := StrZero(Val(Alltrim(oXml:_nfe[1]:_infnfe:_IDE:_NNF:TEXT)),9)
    cSerie	  := Alltrim(oXml:_nfe[1]:_infnfe:_IDE:_SERIE:TEXT)
 
    aCabec	:= {}
    aItens	:= {}
    Aadd(aCabec,{"F2_TIPO"		,"N"			})
    Aadd(aCabec,{"F2_FORMUL"	,"S"			})
    Aadd(aCabec,{"F2_DOC"		,cNota			})
    Aadd(aCabec,{"F2_SERIE"		,cSerie			})
    Aadd(aCabec,{"F2_EMISSAO"	,dEmissao		})
    Aadd(aCabec,{"F2_CLIENTE"	,SA1->A1_COD	})
 	Aadd(aCabec,{"F2_LOJA"		,SA1->A1_LOJA	})
 	Aadd(aCabec,{"F2_ESPECIE"	,"NFS"			})
 	Aadd(aCabec,{"F2_CHVNFE"	,cChave			})
 
 	For nX := 1 To Len(oXml:_NFE)
 		cItem	:= StrZero(Val(Alltrim(oXml:_NFE[1]:_INFNFE:_DET:_NITEM:TEXT					)),2)
 		cProd	:= Upper(Alltrim(oXml:_NFE[nX]:_INFNFE:_DET:_PROD:_CPROD:TEXT					))
 		cUM		:= Upper(Alltrim(oXml:_NFE[nX]:_INFNFE:_DET:_PROD:_UTRIB:TEXT					))
 		cDesc	:= Upper(Alltrim(oXml:_NFE[nX]:_INFNFE:_DET:_PROD:_XPROD:TEXT					))
 		nQuant	:= Val(Alltrim(oXml:_NFE[nX]:_INFNFE:_DET:_PROD:_QCOM:TEXT						))
 		nValor	:= Val(Alltrim(oXml:_NFE[nX]:_INFNFE:_DET:_PROD:_VPROD:TEXT						))
 		nTotal	:= (nQuant * nValor) 
 		cCfop	:= Upper(Alltrim(oXml:_NFE[nX]:_INFNFE:_DET:_PROD:_CFOP:TEXT					))
 		nAlqIss	:= Val(Alltrim(oXml:_NFE[nX]:_INFNFE:_DET:_imposto:_issqn:_valiq:text			))
 		cLstSrv	:= StrTran(oXml:_NFE[nX]:_INFNFE:_DET:_imposto:_issqn:_clistserv:text,".",""	)
      
 		SB1->(DbSetOrder(1))
 		If !SB1->(DbSeek(xFilial("SB1")+cProd))
 			oModel := FWLoadModel("MATA010")
 			oModel:SetOperation(3)
 			oModel:Activate()
     	
 			oSB1Mod := oModel:GetModel("SB1MASTER")     	
 			oSB1Mod:SetValue("B1_COD"	 , cProd	)
 			oSB1Mod:SetValue("B1_DESC"	 , cDesc	)
 			oSB1Mod:SetValue("B1_TIPO"	 , "SV"		)
 			oSB1Mod:SetValue("B1_UM"	 , cUM		)
 			oSB1Mod:SetValue("B1_LOCPAD" , "01"  	)
 			oSB1Mod:SetValue("B1_CODISS" , ""   	)
 			oSB1Mod:SetValue("B1_ALIQISS", nAlqIss  )
        
 			oSB5Mod := oModel:GetModel("SB5DETAIL")
 			If oSB5Mod != Nil
 				oSB5Mod:SetValue("B5_CEME"   , cDesc )
 			EndIf
        
 			If oModel:VldData()  
 				If oModel:CommitData()
 					lOk := .T.
 				Else
 					lOk := .F.
 				EndIf  
 			Else
 				lOk := .F.
 			EndIf

 			If ! lOk
 				aErro	 := oModel:GetErrorMessage()
 				cMessage := "Id do formul�rio de origem:"  + ' [' + cValToChar(aErro[01]) + '], '
 				cMessage += "Id do campo de origem: "      + ' [' + cValToChar(aErro[02]) + '], '
 				cMessage += "Id do formul�rio de erro: "   + ' [' + cValToChar(aErro[03]) + '], '
 				cMessage += "Id do campo de erro: "        + ' [' + cValToChar(aErro[04]) + '], '
 				cMessage += "Id do erro: "                 + ' [' + cValToChar(aErro[05]) + '], '
 				cMessage += "Mensagem do erro: "           + ' [' + cValToChar(aErro[06]) + '], '
 				cMessage += "Mensagem da solu��o: "        + ' [' + cValToChar(aErro[07]) + '], '
 				cMessage += "Valor atribu�do: "            + ' [' + cValToChar(aErro[08]) + '], '
 				cMessage += "Valor anterior: "             + ' [' + cValToChar(aErro[09]) + ']'

 				lRet := .F.
 				ConOut("Erro: " + cMessage)
 			Else
 				lRet := .T.
 				ConOut("Produto incluido!")
 			EndIf
 			oModel:DeActivate()
 		EndIf
      
 		aLinha := {}
 		Aadd(aLinha,{"D2_ITEM" 	,cItem	,Nil})
 		Aadd(aLinha,{"D2_COD"  	,cProd	,Nil})
 		Aadd(aLinha,{"D2_QUANT"	,nQuant	,Nil})
 		Aadd(aLinha,{"D2_PRCVEN",nValor	,Nil})
 		Aadd(aLinha,{"D2_TOTAL"	,nTotal	,Nil})
 		Aadd(aLinha,{"D2_TES"	,"501"	,Nil})
 		Aadd(aLinha,{"D2_CF"	,cCfop	,Nil})
 		Aadd(aLinha,{"D2_LOCAL"	,"01"	,Nil})
 	  //Aadd(aLinha,{"D2_CODISS",cLstSrv,Nil})      
 		Aadd(aItens,aLinha)      
 	Next nX 

 	lMsErroAuto := .F.
 	MSExecAuto({|x,y,z| Mata920(x,y,z)},aCabec ,aItens,3)
 	If !lMsErroAuto
 		ConOut("Incluido com sucesso! "+cNota)
 	Else
 	 	MostraErro()
 	 	ConOut("Erro na inclusao!")
 	 EndIf
 Next
 
Return()

//-------------------------------------------------------------------
/*/{Protheus.doc} GetValCont
Busca os valores no retorno do select do contrato

@param cCampoSis , para buscar o valor no array
@param aReg      , Dados do registro do Sisjuri

@return xRet, Valor do campo no registro

@author  Bruno Ritter
@since   24/09/2019
/*/
//-------------------------------------------------------------------
Static Function GetValCont(cCampoSis, aReg)
	Local xRet  := Nil
	Local aElem := {}
	Local nPos  := 0

	If HMGet(oHashCpo, cCampoSis, @aElem)
		nPos := aElem[1][2]
		xRet := aReg[nPos]
	EndIf

Return xRet`

//-------------------------------------------------------------------
/*/{Protheus.doc} GetHashMap
Gera um HashMap do array com os campos da query do contrato

@return oHash, objeto do tipo HashMap

@author Bruno Ritter
@since  27/09/2019
/*/
//-------------------------------------------------------------------
 Static Function GetHashMap()

 Local aCpoSis := GetCpoCont()
 Local oHash   := HMNew()
 Local nI      := 0

 For nI := 1 To Len(aCpoSis)
	HMAdd(oHash, {aCpoSis[nI][1], nI})
 Next nI

Return oHash

//---------------------------------------------------------------------------
/*/{Protheus.doc} GetCpoCont
Pega os campos para o select

@param lExcecao   , Se � query de cobran�a com exce��o no tabelado ou despesa
@param lExcFilho  , Se a exce��o � filho de um caso m�e

@return aCampos, Retorna um array com os campos para o select do contrato

@author  Bruno Ritter
@since   17/09/2019
/*/
//---------------------------------------------------------------------------

Static Function GetCpoCont()

 Local aCampos    := {}

 Aadd(aCampos, {"ORGNCODIG"  })
 Aadd(aCampos, {"NUMERONF"	 })
 Aadd(aCampos, {"SERIENF"    })
 Aadd(aCampos, {"XML1"		 })
 Aadd(aCampos, {"XML2"		 })
 Aadd(aCampos, {"CHAVENFE"   })

Return aCampos