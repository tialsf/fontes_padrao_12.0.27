#INCLUDE "PROTHEUS.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "RWMAKE.CH"

#DEFINE CSSLINHA "color: #D7D6D6;"
#DEFINE CSSBC "background-color: #F5F5F5;"

Static _aEmpresas := {}

//-------------------------------------------------------------------
/*/{Protheus.doc} PFSRnMig
Fun��o principal de menu para execu��o do migrador.

@author  Jonatas Martins
@since   20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
User Function PFSRnMig()

	GetEmp() // Busca empresas e filiais
	
	JURPFSM001() // Executa Wizard de migra��o
	
	JurFreeArr(_aEmpresas)
	
Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} JURPFSM001
Interface do Wizard para configura��o e execu��o do migrador.

@author  Jonatas Martins
@since   20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Function JURPFSM001()
	Local oStepWiz    := Nil
	Local oNewPag     := Nil
	Local cDBMS       := "ORACLE/SISJURI" + Space(50)
	Local cSrvIP      := GetServerIP()
	Local cIP         := cSrvIP + Space(15 - Len(cSrvIP))
	Local cPort       := "7890  "
	Local cEmp        := "T1" + Space(50)
	Local cFil        := "M SP 01 " + Space(50)
	Local cSenha      := "" + Space(50)
	Local aCombosFil  := {}
	Local aEscritSis  := {}
	Local aFilxEscr   := {}
	
	Private oFont      := Nil
	Private oDbApData  := Nil
	Private nConnSisJr := 0
	Private nConnProth := 0

	DEFINE FONT oFont NAME "Arial" SIZE 000,-014

	oStepWiz:= FwWizardControl():New( , {350, 720})
	oStepWiz:ActiveUISteps()

	oNewPag := oStepWiz:AddStep("1")
	oNewPag:SetStepDescription("Premissas")
	oNewPag:SetConstruction({|oPanel| Page1(oPanel)})

	oNewPag := oStepWiz:AddStep("2")
	oNewPag:SetStepDescription("Conex�o SISJURI")
	oNewPag:SetConstruction({|oPanel| Page2(oPanel, @cDBMS, @cIP, @cPort)})

	oNewPag := oStepWiz:AddStep("3")
	oNewPag:SetStepDescription("Dados Protheus")
	oNewPag:SetConstruction({|oPanel| Page3(oPanel, @cEmp, @cFil, @cSenha)})
	oNewPag:SetNextAction({|oPanel| StartConn(oPanel, cDBMS, cIP, Val(cPort), AllTrim(cEmp), PadR(cFil, FWSizeFilial()), AllTrim(cSenha))})

	oNewPag := oStepWiz:AddStep("4")
	oNewPag:SetStepDescription("Status da Conex�o")
	oNewPag:SetConstruction({|oPanel| Page4(oPanel)})
	oNewPag:SetPrevWhen({|| .F.})

	oNewPag := oStepWiz:AddStep("5")
	oNewPag:SetStepDescription("Escrit�rio-Filial")
	oNewPag:SetConstruction({|oPanel| PageEscFil(oPanel, @aCombosFil, @aEscritSis)})
	oNewPag:SetNextAction({|| RunEscFil(@aCombosFil, @aEscritSis, @aFilxEscr)})
	oNewPag:SetPrevWhen({|| .F.})

	oNewPag := oStepWiz:AddStep("6")
	oNewPag:SetStepDescription("Migra��o")
	oNewPag:SetConstruction({|oPanel| Page6(oPanel, @aFilxEscr)})
	oNewPag:SetPrevWhen({|| .F.})
	
	oStepWiz:Activate()

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Page1
Montagem da interface da p�gina 1 do Wizard (Premissas).

@author  Jonatas Martins
@since   20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function Page1(oPanel)
	Local cText := ""

	cText := CRLF + "Premissas:" + CRLF
	cText += CRLF + "    a. Pr�via configura��o das conex�es do banco SISJURI e Protheus no mesmo" 
	cText += CRLF + "       TOTVS DBAccess"
	cText += CRLF + "    b. Cadastro de empresas e filiais efetivados no Protheus"
	cText += CRLF + "    c. Tabela de controle do migrador 'OHS' existente no Protheus"

	@ 000, 000 GET cText  MEMO SIZE 840, 300 ReadOnly FONT oFont PIXEL OF oPanel

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Page2
Montagem da interface da p�gina 2 do Wizard (Conex�o SISJURI).

@author  Jonatas Martins
@since   20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function Page2(oPanel, cDBMS, cIP, cPort)

	@ 010, 018 BUTTON "Tipo: " SIZE 030, 016 FONT oFont PIXEL OF oPanel ACTION (Info2A())
	@ 010, 050 MSGET cDBMS SIZE 220, 016 FONT oFont PIXEL OF oPanel VALID (!Empty(cDBMS))

	@ 030, 018 BUTTON "IP: " SIZE 030, 016 FONT oFont PIXEL OF oPanel ACTION (Info2B())
	@ 030, 050 MSGET cIP SIZE 220, 016 FONT oFont PIXEL OF oPanel VALID (!Empty(cIP))

	@ 050, 018 BUTTON "Porta: " SIZE 030, 016 FONT oFont PIXEL OF oPanel ACTION (Info2C())
	@ 050, 050 MSGET cPort SIZE 220, 016 PICTURE "@R" FONT oFont PIXEL OF oPanel VALID (Positivo(Val(cPort)))

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Info2A
Apresenta mais informa��es sobre o campo Tipo/Nome da Conex�o

@author  Jonatas Martins
@since   20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function Info2A()
	Local oDlg := Nil

	DEFINE MSDIALOG oDlg FROM 0,0 TO 75, 500 PIXEL TITLE "Tipo/Nome da Conex�o"

	@ 07,05 Say "Tipo/Nome:" PIXEL
	@ 14,05 Say "        a. Informe o Tipo/Nome da conex�o configurada no TOTVS DBAccess" PIXEL
	@ 20,05 Say "        b. Exemplo: 'ORACLE/P10'" PIXEL
	
	ACTIVATE MSDIALOG oDlg CENTERED 

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Info2B
Apresenta mais informa��es sobre o campo IP do Servidor.

@author  Jonatas Martins
@since   20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function Info2B()
	Local oDlg := Nil

	DEFINE MSDIALOG oDlg FROM 0,0 TO 75, 500 PIXEL TITLE "IP do Servidor"

	@ 07,05 Say "IP:" PIXEL
	@ 14,05 Say "        a. Informe o IP do servidor que est� instalado o TOTVS DBAccess" PIXEL
	
	ACTIVATE MSDIALOG oDlg CENTERED 

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Info2C
Apresenta mais informa��es sobre o campo Porta do Servidor.

@author  Jonatas Martins
@since   20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function Info2C()
	Local oDlg := Nil

	DEFINE MSDIALOG oDlg FROM 0,0 TO 75, 500 PIXEL TITLE "Porta do Servidor"

	@ 07,05 Say "Porta:" PIXEL
	@ 14,05 Say "        a. Informe a porta do servidor que est� instalado o TOTVS DBAccess" PIXEL
	
	ACTIVATE MSDIALOG oDlg CENTERED 

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Page3
Montagem da interface da p�gina 3 do Wizard (Dados Protheus).

@author  Jonatas Martins
@since   20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function Page3(oPanel, cEmp, cFil, cSenha)

	@ 010, 005 BUTTON "Empresa: " SIZE 050, 016 FONT oFont PIXEL OF oPanel ACTION (Info3A())
	@ 010, 057 MSGET cEmp SIZE 220, 016 FONT oFont PIXEL OF oPanel VALID (!Empty(cEmp))

	@ 030, 005 BUTTON "Filial: " SIZE 050, 016 FONT oFont PIXEL OF oPanel ACTION (Info3B())
	@ 030, 057 MSGET cFil SIZE 220, 016 FONT oFont PIXEL OF oPanel VALID (!Empty(cFil))

	@ 050, 005 BUTTON "Senha Admin: " SIZE 050, 016 FONT oFont PIXEL OF oPanel ACTION (Info3C())
	@ 050, 057 MSGET cSenha SIZE 220, 016 FONT oFont PIXEL OF oPanel PASSWORD

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Info3A
Apresenta mais informa��es sobre o campo Empresa.

@author  Jonatas Martins
@since   20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function Info3A()
	Local oDlg := Nil

	DEFINE MSDIALOG oDlg FROM 0,0 TO 75, 500 PIXEL TITLE "Empresa"

	@ 07,05 Say "Empresa:" PIXEL
	@ 14,05 Say "        a. Informe o c�digo da empresa Protheus" PIXEL
	@ 20,05 Say "        b. Exemplo: '010'" PIXEL
	
	ACTIVATE MSDIALOG oDlg CENTERED

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Info3B
Apresenta mais informa��es sobre o campo Filial.

@author  Jonatas Martins
@since   20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function Info3B()
	Local oDlg := Nil

	DEFINE MSDIALOG oDlg FROM 0,0 TO 75, 500 PIXEL TITLE "Filial"

	@ 07,05 Say "Filial:" PIXEL
	@ 14,05 Say "        a. Informe a filial do Protheus" PIXEL
	@ 20,05 Say "        b. Exemplo: '01'" PIXEL
	
	ACTIVATE MSDIALOG oDlg CENTERED 

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Info3C
Apresenta mais informa��es sobre o campo Senha Admin.

@author  Jacques Alves Xavier
@since   09/07/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function Info3C()
	Local oDlg := Nil

	DEFINE MSDIALOG oDlg FROM 0,0 TO 75, 500 PIXEL TITLE "Senha Admin"

	@ 07,05 Say "Informe a senha do usu�rio Protheus: Admin" PIXEL
	@ 20,05 Say " Obs.: Caso o usu�rio n�o tenha senha, basta deixar o campo em branco." PIXEL	
	
	ACTIVATE MSDIALOG oDlg CENTERED 

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} StartConn
Valida empresa e filial Protheus e testa a conex�o com o banco do SISJURI

@author  Jonatas Martins
@since   20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function StartConn(oPanel, cDBMS, cIP, nPort, cEmp, cFil, cSenha)
	Local lOk      := .F.

	If ValidEmpFil(cEmp, cFil)
		FWMsgRun(oPanel, {|| lOk := ConnectDB(cDBMS, cIP, nPort, cEmp, cFil, cSenha)}, "Processando", "Tentando conex�o, aguarde...")
		
		If lOk
			FWMsgRun(oPanel, {|| lOk := VldStruct(cDBMS, cIP, nPort, cEmp, cFil)}, "Processando", "Analisando estrutura, aguarde...")
		EndIf

	Endif

Return (lOk)

//-------------------------------------------------------------------
/*/{Protheus.doc} ValidEmpFil
Valida empresa e filial Protheus

@author  Jonatas Martins
@since   20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ValidEmpFil(cEmp, cFil)
	Local lValid := .F.

	If aScan(_aEmpresas, {|x| x[1] == cEmp}) == 0 .Or. aScan(_aEmpresas, {|x| x[2] == cFil}) == 0
		ShowMsgErro("C�digo da Empresa ou Filial inv�lido!")
	Else
		lValid := .T.
	EndIf

Return (lValid)

//-------------------------------------------------------------------
/*/{Protheus.doc} ConnectDB
Realiza conex�o entre ambiente Protheus e banco SISJURI.

@author  Jonatas Martins
@since   20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ConnectDB(cDBMS, cIP, nPort, cEmp, cFil, cSenha)
	Local lContinue   := .T.
	Local lConnection := .F.
	Local cMsgErro    := ""
	Local cQuery      := ""
    Local cQryRes     := GetNextAlias()
    Local nTamSisjur  := 0
    Local nTamProthe  := 0
	Local lSenhaOk    := .F.
            	
	If Type("cEmpAnt") == "C" .And. !Empty(cEmpAnt)
		RpcClearEnv()
	EndIf

	RpcSetType(3)
	RpcSetEnv(cEmp, cFil, 'Admin', cSenha)
	PswOrder(1)
	If PswSeek('000000') .And. PswName(cSenha) // Valida a senha do usu�rio Admin
		lSenhaOk := .T.
	EndIf

	If Empty(cEmpAnt) .Or. !lSenhaOk
		ShowMsgErro("Falha ao montar ambiente Protheus, verifique as informa��es digitadas!")
		lContinue := .F.
	Else
		nConnProth := TCGetConn()
		If !FWAliasInDic("OHS")
			cMsgErro := "Tabela OHS - 'Migrador SISJURI V11 x V12' n�o foi encontrada." + CRLF
			cMsgErro += "Atualize o dicion�rio para realizar a migra��o."
			ShowMsgErro(cMsgErro)
			lContinue := .F.
		EndIf

		If lContinue
			Chkfile("SAN")
			Chkfile("CC2")
			Chkfile("SYA")
			Chkfile("SX5")
			
			MgrUpdSX2() // Ajusta compartilhamento
			RpcClearEnv() // Fecha ambiente j� criado para eliminar o cache de tabelas
			RpcSetType(3) // Recria ambiente para carregar o compartilhamento das tabelas atualizado
			RpcSetEnv(cEmp, cFil, 'Admin', cSenha)
			nConnProth := TCGetConn()
		EndIf
	EndIf

	//-------------------------------------------------------
	// Cria classe de conexao com o banco Externo do SISJURI
	//-------------------------------------------------------
	If lContinue
		oDbApData := FWDBAccess():New(cDBMS, cIP, nPort)
		oDbApData:SetConsoleError(.T.)

		If oDbApData:HasConnection()
			oDbApData:CloseConnection()
		EndIf
		
		If oDbApData:OpenConnection()
			lConnection := .T.
			nConnSisJr  := oDbApData:Handle()

	        TCSetConn(nConnSisJr) // Conex�o Sisjuri			
			cQuery := "SELECT MAX(LENGTH(TRIM(COD_ENDERECO))) LOJA FROM RCR.ENDERECO"
	        DbUseArea(.T., "TOPCONN", TcGenQry(,, cQuery), cQryRes, .F., .T.)
		
	        If !(cQryRes)->( Eof() )
		       nTamSisjur := (cQryRes)->LOJA
	        EndIf
	       (cQryRes)->(DbCloseArea())
	
	        TCSetConn(nConnProth) // Conex�o Protheus
	        nTamProthe := TamSX3("A1_LOJA")[1]
			
			/*
			If nTamProthe < nTamSisjur
			   Alert("Entrei 04")
			   cMsgErro := "O tamanho do campo A1_LOJA n�o atende a necessidade para a   " + CRLF
			   cMsgErro += "migra��o das informa��es. Ajuste  o tamanho do campo para    " + CRLF
			   cMsgErro += "caracter de "+Alltrim(Str(nTamSisjur ))+" e excute o processo" + CRLF
			   cMsgErro += "novamente"
			   ShowMsgErro(cMsgErro)
			   lConnection := .f.
			EndIf
			*/
		Else
			ShowMsgErro("Falha de conex�o com a base Externa - Erro: " + AllTrim(oDbApData:ErrorMessage()))
		EndIf
	EndIf

Return (lConnection)

//-------------------------------------------------------------------
/*/{Protheus.doc} Page4
Montagem da interface da p�gina 4 do Wizard (Status da Conex�o).

@author  Jonatas Martins
@since   20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function Page4(oPanel)
	Local cText := ""

	cText := CRLF + "Conex�o efetuada com sucesso!"
	cText += CRLF + Replicate("=", 70)
	cText += CRLF + "Uma vez iniciada a migra��o, todos os dados existentes nas tabelas do Protheus"
	cText += CRLF + "ser�o sobrescritos."
	cText += CRLF + "A migra��o � s�ncrona, assim que iniciada executar� o processo por completo.
	cText += CRLF + "Clique em 'Avan�ar' para iniciar ou 'Cancelar' para abortar a opera��o."

	@ 000, 000 GET cText  MEMO SIZE 840, 300 ReadOnly FONT oFont PIXEL OF oPanel

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} PageEscFil
Montagem da interface de de-para referente ao escrit�rio e filial
do Wizard (Escrit�rio-Filial).

@param oPanel    , Panel do Wizard
@param aCombosFil, Array vazio para ser preenchido com os combos
                   de filiais do protheus informadas pelo usu�rio
@param aCombosFil, Array vazio para ser preenchido com os escrit�rio
                   do SisJuri

@author  Bruno Ritter
@since   11/07/2019
/*/
//-------------------------------------------------------------------
Static Function PageEscFil(oPanel, aCombosFil, aEscritSis)
	Local oProcess  := Nil
	Local cSayEscr  := ""
	Local nEscr     := 0
	Local aFiliais  := {""}
	Local cComboFil := ""
	Local aTSays    := {}
	Local aTSaysLn  := {}
	Local nLine     := 5
	Local oScroll   := TScrollArea():New(oPanel,01,01,100,310)
	Local oPnl      := Nil
	Local nPulo     := 20
	Local nTamPnl   := 0
	Local oFontEsc  := TFont():New("Arial",,16,,.T.)

	aEscritSis := GetEscrtSJ()
	nTamPnl    := nLine + Iif(Len(aEscritSis) > 4, (Len(aEscritSis) * nPulo), 75)

	oScroll:Align := CONTROL_ALIGN_ALLCLIENT
	@ 000,000 MSPANEL oPnl OF oScroll SIZE 300, nTamPnl COLOR CLR_HRED
	oScroll:SetFrame( oPnl )

	aEval(_aEmpresas, {|aEmp| Aadd(aFiliais, AllTrim(aEmp[1]) + " - " + AllTrim(aEmp[2])) })
	cComboFil := aFiliais[1]

	For nEscr := 1 To Len(aEscritSis)
		cSayEscr := AllTrim(aEscritSis[nEscr][1]) + " - " + AllTrim(aEscritSis[nEscr][2])

		aAdd(aTSays, TSay():Create(oPnl,{|| },nLine,10,,;
		             oFontEsc,,,,.T.,CLR_BLACK,CLR_WHITE,180,20))
		aTSays[Len(aTSays)]:SetText(cSayEscr)

		aAdd(aCombosFil, TComboBox():New(nLine-1, 245, {||}, aFiliais;
		                 , 100, 20, oPnl,,,{||VldFilDupl(aCombosFil)},,,.T.))

		aAdd(aTSaysLn, TSay():Create(oPnl,{|| Replicate("-", 300) },nLine+10,10,,;
		             ,,,,.T.,,,335,20))
		aTSaysLn[Len(aTSaysLn)]:SetCSS(CSSLINHA)

		nLine += nPulo
	Next nEscr

// Seleciona filial para Testes
	For nEscr := 1 To Len(aCombosFil)
		aCombosFil[nEscr]:Select( nEscr + 1 )
	Next nEscr

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} VldFilDupl
Valida se foi selecionado mais de uma vez a mesma filial

@param aCombosFil, Lista de comoboBox do wizard com as filiais do Protheus

@return lRet, Se est� valido os dados informados pelo usu�rio

@author  Bruno Ritter
@since   11/07/2019
/*/
//-------------------------------------------------------------------
Static Function VldFilDupl(aCombosFil)
	Local lRet      := .T.
	Local aFilSel   := {}
	Local nI        := 0
	Local nPosFil   := 0
	Local cFilialCb := ""

	For nI := 1 To Len(aCombosFil)
		cFilialCb := aCombosFil[nI]:aItems[aCombosFil[nI]:nAt]

		If !Empty(cFilialCb)
			nPosFil   := aScan(aFilSel, {|cFilSel| cFilSel == cFilialCb})

			If nPosFil > 0
				ShowMsgErro("Filial j� foi selecionado para outro escrit�rio.", "Valor inv�lido")
				lRet := .F.
				Exit
			Else
				aAdd(aFilSel, cFilialCb)
			EndIf
		EndIf
	Next nI

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} RunEscFil
Valida��o da p�gina de de-para referente ao escrit�rio e filial
do Wizard (Escrit�rio-Filial) e executa a importa��o de escrit�rio.

@param aCombosFil, Lista de comoboBox do wizard
@param aEscritSis, Lista de escrit�rio do SisJuri com a {Sigla, Nome}
@param aFilxEscr , Guarda o De-Para de sigla do escrit�rio x Filial do protheus
                    para realizar a migra��o dos escrit�rios do SisJuri

@return lRet, Se est� valido os dados informados pelo usu�rio

@author  Bruno Ritter
@since   11/07/2019
/*/
//-------------------------------------------------------------------
Static Function RunEscFil(aCombosFil, aEscritSis, aFilxEscr)
	Local lRet      := VldEscFil(aCombosFil, aEscritSis)
	Local nI        := 1
	Local nPosFil   := 0

	If lRet
		For nI := 1 To Len(aCombosFil)
			nPosFil := aCombosFil[nI]:nAt - 1 // -1 para remover o item em branco

			Aadd(aFilxEscr, {aEscritSis[nI][1], AClone(_aEmpresas[nPosFil])})

		Next nI

		JurFreeArr(@aCombosFil)
		JurFreeArr(@aEscritSis)
	EndIf
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} VldEscFil
Valida��o da p�gina de de-para referente ao escrit�rio e filial
do Wizard (Escrit�rio-Filial).

@param aCombosFil, Lista de comoboBox do wizard
@param aEscritSis, Lista de escrit�rio do SisJuri com a {Sigla, Nome}

@return lRet, Se est� valido os dados informados pelo usu�rio

@author  Bruno Ritter
@since   11/07/2019
/*/
//-------------------------------------------------------------------
Static Function VldEscFil(aCombosFil, aEscritSis)
	Local lRet      := .T.
	Local nI        := 0
	Local cFilialCb := ""
	Local cSayEscr  := ""

	For nI := 1 To Len(aCombosFil)
		cFilialCb := aCombosFil[nI]:aItems[aCombosFil[nI]:nAt]

		If Empty(cFilialCb)
			cSayEscr := AllTrim(aEscritSis[nI][1]) + " - " + AllTrim(aEscritSis[nI][2])
			ShowMsgErro(i18n("N�o foi selecionada uma filial para o escrit�rio: '#1'", {cSayEscr}))
			lRet := .F.
			Exit
		EndIf
	Next nI

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} Page6
Montagem da interface da p�gina 6 do Wizard (Migra��o).

@param oPanel    Panel do Wizard
@param aFilxEscr Guarda o De-Para de sigla do escrit�rio x Filial do protheus
                 para realizar a migra��o dos escrit�rios do SisJuri

@author  Jonatas Martins
@since   20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function Page6(oPanel, aFilxEscr)
	Local oProcess   := Nil
	Local cTempTotal := ""
	Local cText      := ""

	oProcess := MsNewProcess():New({|| cTempTotal := U_MigraPFS(@oProcess, @aFilxEscr, nConnSisJr, nConnProth)}, "Aguarde", "Migrando dados...", .T.)
	oProcess:Activate()

	ConOut("Tempo total da migra��o: " + cTempTotal)
	cText      := CRLF + "A migra��o foi finalizada em '"+cTempTotal+"', clique em 'Concluir' para fechar a tela."

	@ 000, 000 GET cText MEMO SIZE 840, 300 ReadOnly FONT oFont PIXEL OF oPanel

	RESET ENVIRONMENT

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} GetEmp
Fun��o para retonar o grupo de empresas e filiais do sistema.

@Return aEmp    Array com as informa�oes das SM0

@author  Jonatas Martins
@since   20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function GetEmp()
	Local aSM0     := {}
	Local nI       := 0
	Local aProcEmp := {}

	OpenSm0()
	aSM0 := FWLoadSM0(.T., .F.)

	For nI := 1 To Len(aSM0)
		Aadd(aProcEmp, {aSM0[nI][1], aSM0[nI][2]})
	Next nI

	_aEmpresas := aClone(aProcEmp)

	JurFreeArr(@aProcEmp)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} ShowMsgErro
Apresenta mensagem de erro.

@param   cMsgErro   Descri��o do erro
@param   cTitle     T�tulo da janela

@author  Jonatas Martins
@since   20/05/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ShowMsgErro(cMsgErro, cTitle)
	Local oDlg      := Nil
	Local oFontDesc := TFont():New("Arial",,16,,.T.)
	Local oFontMsg  := TFont():New("Arial",,16,,.F.)
	Local oSayDesc  := Nil
	Local oSayMsg   := Nil
	Local nLineMsg  := MLCount(cMsgErro)

	Default cTitle := "Falha no Processo"

	DEFINE MSDIALOG oDlg FROM 0,0 TO 60+(nLineMsg*15), 500 PIXEL TITLE cTitle
	oDlg:SetCss(CSSBC)

	oSayDesc := TSay():Create(oDlg,{|| "Descri��o do Erro:"},07,05,,;
		             oFontDesc,,,,.T.,CLR_BLACK,CLR_WHITE,300,10)

	oSayMsg := TSay():Create(oDlg,{|| cMsgErro},20,05,,;
		             oFontMsg,,,,.T.,CLR_BLACK,CLR_WHITE,300,300)
	
	ACTIVATE MSDIALOG oDlg CENTERED

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} GetEscrtSJ
Retorna os escrit�rios do SisJuri

return aEscrit, Escrit�rios do SisJuri {Sigla, Descri��o}

@author  Bruno Ritter
@since   11/07/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function GetEscrtSJ()
	Local aEscrit := {}
	Local cQuery  := "SELECT SIGLA, NOME, ORGNCODIG FROM RCR.ESCRITORIO"

	TCSetConn(nConnSisJr) // Conex�o SisJuri
	aEscrit := JurSQL(cQuery, {"*"})

Return aEscrit

//-------------------------------------------------------------------
/*/{Protheus.doc} VldStruct
Valida a estrutura das tabelas Sisjuri x Protheus

@author  Bruno Ritter | Abner Foga�a
@since   23/08/2019
/*/
//-------------------------------------------------------------------
Static Function VldStruct()
	Local lOk        := .T.
	Local cCmpProth  := ""
	Local cQuery     := ""
	Local cMsgErro   := ""
	Local cQryRes    := GetNextAlias()
	Local nTamSisjur := 0
	Local nTamProthe := 0

	/* Valida o tamanho do campo de Natureza */
	/*
	TCSetConn(nConnSisJr) // Conex�o Sisjuri
	cQuery := "SELECT MAX(LENGTH(TRIM(REPLACE(PCTCNUMEROCONTA, '.', '')))) MAIOR FROM FINANCE.PLANOCONTAS"
	DbUseArea(.T., "TOPCONN", TcGenQry(,, cQuery), cQryRes, .F., .T.)
	
	If !(cQryRes)->( Eof() )
		nTamSisjur := (cQryRes)->MAIOR
	EndIf
	(cQryRes)->(DbCloseArea())

	TCSetConn(nConnProth) // Conex�o Protheus
	nTamProthe := TamSX3("ED_CODIGO")[1]

	If nTamSisjur > nTamProthe
		cCmpProth := "ED_CODIGO"
		cMsgErro  := I18n("Tamanho do campo '#1' entre Sisjuri e Protheus � diferente: ", {cCmpProth}) + CRLF
		cMsgErro  += "Sisjuri:" + cValToChar(nTamSisjur) + CRLF
		cMsgErro  += "Protheus:" + cValToChar(nTamProthe) + CRLF
		lOk       := .F.
	EndIf
    */
    
	/* Valida o tamanho do campo de N�mero do Caso */
	TCSetConn(nConnSisJr) // Conex�o Sisjuri
	cQuery := "SELECT MAX(LENGTH(TRIM(PASTA))) PASTA FROM RCR.PASTA"
	DbUseArea(.T., "TOPCONN", TcGenQry(,, cQuery), cQryRes, .F., .T.)
		
	If !(cQryRes)->( Eof() )
		nTamSisjur := (cQryRes)->PASTA
	EndIf
	(cQryRes)->(DbCloseArea())
	
	TCSetConn(nConnProth) // Conex�o Protheus
	nTamProthe := TamSX3("NVE_NUMCAS")[1]
	
	If nTamSisjur > nTamProthe
		cCmpProth := "NVE_NUMCAS"
		cMsgErro  := I18n("Tamanho do campo '#1' entre Sisjuri e Protheus � diferente: ", {cCmpProth}) + CRLF
		cMsgErro  += "Sisjuri:" + cValToChar(nTamSisjur) + CRLF
		cMsgErro  += "Protheus:" + cValToChar(nTamProthe)
		lOk       := .F.
	EndIf
	
	If !Empty(cMsgErro)
		ShowMsgErro(cMsgErro, "Erro de estrutura")
	EndIf

Return lOk

//-------------------------------------------------------------------
/*/{Protheus.doc} MgrUpdSX2
Fun��o para ajustar o compartilhamento de tabelas no SX2

@author Jonatas Martins
@since  22/07/2019
/*/
//-------------------------------------------------------------------
Static Function MgrUpdSX2()
	Local aTables     := {}
	Local cTable      := "" 
	Local cTypeBranch := ""
	Local cTypeUnity  := ""
	Local cTypeEnterp := ""
	Local nTab        := 0
	Local nTables     := 0

	// Incrementar esse array com as tabelas que devem ser ajustadas
	Aadd(aTables, {"SA1", "C", "C", "C"})
	Aadd(aTables, {"SU5", "C", "C", "C"})
	Aadd(aTables, {"AGA", "C", "C", "C"})
	Aadd(aTables, {"AGB", "C", "C", "C"})
	Aadd(aTables, {"AC8", "C", "C", "C"})
	Aadd(aTables, {"SA2", "C", "C", "C"})
	Aadd(aTables, {"FIL", "C", "C", "C"})
	Aadd(aTables, {"CTO", "C", "C", "C"})
	Aadd(aTables, {"SA6", "C", "C", "C"})
	Aadd(aTables, {"OHK", "C", "C", "C"})
	Aadd(aTables, {"SED", "C", "C", "C"})
	Aadd(aTables, {"OHP", "C", "C", "C"})
	Aadd(aTables, {"CTT", "C", "C", "C"})

	nTables := Len(aTables)

	For nTab := 1 To nTables
		cTable      := aTables[nTab][1]
		cTypeBranch := aTables[nTab][2]
		cTypeUnity  := aTables[nTab][3]
		cTypeEnterp := aTables[nTab][4]

		SX2->(DbSetOrder(1)) // X2_CHAVE
		If SX2->(DbSeek(cTable))
			RecLock("SX2", .F.)
			SX2->X2_MODO    := cTypeBranch // Filial
			SX2->X2_MODOUN  := cTypeUnity  // Unidade
			SX2->X2_MODOEMP := cTypeEnterp // Empresa
			SX2->(MsUnlock())
		EndIf
	Next nTab

	JurFreeArr(@aTables)

Return Nil