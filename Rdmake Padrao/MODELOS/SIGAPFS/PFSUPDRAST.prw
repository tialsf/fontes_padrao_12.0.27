#INCLUDE "PROTHEUS.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} UPDRastr()
Fun��o para rec�lculo da rastreabilidade de valores por fatura/caso (OHI)
e dos saldos da posi��o hist�rica do contas a receber (OHH)

@author Abner Oliveira / Jorge Martins / Bruno Ritter
@since  02/03/2020
/*/
//-------------------------------------------------------------------
User Function UpdRastr()
	Local oModal     := FWDialogModal():New()
	Local aTFolder   := {"Compatibilizador", "Selecionar Empresas"}
	Local aEmp       := UpdGetEmp()
	Local oMarkNo    := LoadBitmap( GetResources(), "LBNO" )
	Local oMarkOk    := LoadBitmap( GetResources(), "LBOK" )
	Local cMemo      := ""
	Local oMemoTermo := Nil
	Local oAceite    := Nil
	Local oListbox   := Nil
	Local lAceite    := .F.

	oModal:SetFreeArea(300, 150)
	oModal:SetEscClose(.T.)
	oModal:SetTitle("Rec�lculo da Rastreabilidade de Valores Fatura/Caso - SIGAPFS")
	oModal:CreateDialog()
	oModal:addOkButton({|| Iif(lAceite, (UpdProc(aEmp), oModal:oOwner:End()), ApMsgStop("� necess�rio confirmar a realiza��o dos procedimentos antes executar o compatibilizador.", "Compatibilizador" )) })
	oModal:addCloseButton()
	oMainPnl := oModal:GetPanelMain()

	oTFolder := TFolder():New( 0, 0, aTFolder, , oMainPnl, , , , .T., , , )
	oTFolder:Align := CONTROL_ALIGN_ALLCLIENT
	oTFolder:bSetOption := ({||})
	oTfolder1 := oTFolder:aDialogs[1]
	oTfolder2 := oTFolder:aDialogs[2]

	// Folder 1
	cMemo := + CRLF
	cMemo += "Por se tratar de um processo cr�tico � necess�rio fazer uma c�pia de seguran�a do diret�rio de dicion�rios e demais arquivos locais, bem como da base de dados." + CRLF + CRLF
	cMemo += "Note que a base de dados pode estar armazenada em arquivos locais ou em banco de dados relacional, isto varia de acordo com sua instala��o." + CRLF + CRLF
	cMemo += "Verifique se h� espa�o livre em disco para os arquivos locais e no banco de dados relacional, quando utilizado." + CRLF + CRLF
	cMemo += "A atualiza��o de vers�o pode interferir em customiza��es existentes." + CRLF + CRLF
	cMemo += "Desta forma, se voc� possui customiza��es, � impreter�vel que voc� analise o impacto da atualiza��o em suas customiza��es."
	oMemoTermo := TMultiget():New(010, 010, {| u | If( pCount() > 0, cMemo := u, cMemo ) }, oTfolder1, 279, 100, , , , , , .T., , , , , , .T., , , , , .T.)
	oAceite := TCheckBox():New(115, 010, "Verifiquei os procedimentos antes de executar o compatibilizador.", {|u| If(PCount() > 0, lAceite := u, lAceite)}, oTfolder1, 200, 008, , , , , , , ,.T., , , , )

	// Folder 2
	@ 10, 10 Listbox oListbox /*Var cVar*/ Fields Header " ", "C�digo", "Grupo de Empresa" Size 279, 100 Of oTfolder2 Pixel
	oListbox:SetArray(aEmp[1])
	oListbox:bLine := {|| {IIf( aEmp[1][oListbox:nAt, 1], oMarkOk, oMarkNo ), ;
	aEmp[1][oListbox:nAt, 2], ;
	aEmp[1][oListbox:nAt, 3]}}
	oListbox:BlDblClick   := { || aEmp[1][oListbox:nAt, 1] := !aEmp[1][oListbox:nAt, 1], oListbox:Refresh()}
	oListbox:BHeaderClick := { || aEval(aEmp[1], {|a, n| aEmp[1][n, 1] := !aEmp[1][n,1]}), oListbox:Refresh() }
	oListbox:cToolTip     := oModal:cTitle
	oListbox:lHScroll     := .F. // NoScroll

	oModal:Activate()

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} UpdProc()
Fun��o para retonar o grupo de empresas e filiais do sistema.

@return aEmp    Array com as informa�oes das SM0

@author Abner Oliveira / Jorge Martins / Bruno Ritter
@since  02/03/2020
/*/
//-------------------------------------------------------------------
Static Function UpdProc(aEmp)
	Local lRet      := .T.
	Local cEmpMsg   := ""
	Local aEmpresas := UpdSetcEmp(aEmp, @cEmpMsg)

	FWMsgRun(, {|| lRet := UpdProcRun(aEmpresas) }, "Compatibilizando as empresas selecionadas...", "Compatibilizador")

	If lRet
		ApMsgInfo(cEmpMsg + CRLF + "Compatibilizador finalizado com sucesso.", "Compatibilizador")
	Else
		ApMsgStop(cEmpMsg + CRLF + "Compatibilizador finalizado com erro.", "Compatibilizador")
	EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} UpdSetcEmp
Fun��o para retonar as empresas selecionadas para executar o compatibilizador.

@Param aEmp      Array gerado pela rotina UpdSetcEmp()
@Param cMsgEmp   Mensagem com as empresas selecionadas sem dicion�rio
                 carregado, passado por refer�ncia

@return aEmpUpd  Array com as empresas selecionadas

@author Abner Oliveira / Jorge Martins / Bruno Ritter
@since  02/03/2020
/*/
//-------------------------------------------------------------------
Static Function UpdSetcEmp(aEmp, cMsgEmp)
	Local aEmpUpd   := {}
	Local aEmpMark  := aEmp[1]
	Local aEmpAll   := aEmp[2]
	Local nI        := 0
	Local aEmpMsg   := {}

	Default cMsgEmp := ""

	For nI := 1 To Len(aEmpAll)
		If aScan(aEmpMark, {|x| x[1] .And. x[2] == aEmpAll[nI][1]}) > 0
			If MpDicInDb() .Or. RpcChkSxs(aEmpAll[nI][1], @aEmpMsg, .F.)
				aAdd(aEmpUpd, aClone(aEmpAll[nI]))
			EndIf
		EndIf
	Next nI

	Aeval(aEmpMsg, {|x| cMsgEmp += I18N("A Empresa '#1' n�o tem dados para serem compatibilizados." + CRLF, {x[1]})} )
	JurFreeArr(@aEmpAll)

Return aEmpUpd

//-------------------------------------------------------------------
/*/{Protheus.doc} UpdGetEmp()
Fun��o para retonar o grupo de empresas e filiais do sistema.

@return aEmp    Array com as informa�oes das SM0

@author Abner Oliveira / Jorge Martins / Bruno Ritter
@since  02/03/2020
/*/
//-------------------------------------------------------------------
Static Function UpdGetEmp()
	Local aEmp     := {}
	Local aSM0     := {}
	Local nI       := 0
	Local cEmpAux  := ""
	Local aMarcEmp := {}
	Local aProcEmp := {}

	OpenSm0()
	aSM0 := FWLoadSM0(.T., .F.)

	For nI := 1 To Len(aSM0)
		If cEmpAux != aSM0[nI][1]
			Aadd(aMarcEmp, {.T., aSM0[nI][1], aSM0[nI][6]})
			cEmpAux := aSM0[nI][1]
		EndIf
		Aadd(aProcEmp, {aSM0[nI][1], aSM0[nI][2]})
	Next nI

	aEmp := {aClone(aMarcEmp), aClone(aProcEmp)}
	JurFreeArr(@aMarcEmp)
	JurFreeArr(@aProcEmp)

Return aEmp

//-------------------------------------------------------------------
/*/{Protheus.doc} UpdProcRun()
Rotina de abertura de ambiente e execu��o do compatibilizador por empresa e filial.

@return aEmpresas   Array com as informa�oes das empresas

@author Abner Oliveira / Jorge Martins / Bruno Ritter
@since  02/03/2020
/*/
//-------------------------------------------------------------------
Static Function UpdProcRun(aEmpresas)
	Local lRet     := .T.
	Local cEmpOld  := ""
	Local cEmpAux  := ""
	Local cFilAux  := ""
	Local nI       := 0
	Local nP       := 0

	Private __CINTERNET := Nil // Habilita mensagens em tela ap�s a prepara��o de ambiente

	For nI := 1 To Len(aEmpresas)
		cEmpAux  := aEmpresas[nI][1]
		cFilAux  := aEmpresas[nI][2]

		If cEmpOld != cEmpAux // Monta ambiente com nova empresa
			cEmpOld := cEmpAux
			RPCSetType(3)
			lRet := RpcSetEnv(cEmpAux, cFilAux, /*cEnvUser*/, /*cEnvPass*/, "PFS", /*cFunName*/ "UPDRastr", /*aTables*/, /*lShowFinal*/, /*lAbend*/,  /*lOpenSX*/ .T., /*lConnect*/.T.)
			__CINTERNET := Nil

			If lRet .And. (!AliasIndic("OHI") .Or. OHI->(ColumnPos("OHI_VLREMB")) == 0) // Prote��o
				lRet := .F.
				ApMsgStop("O dicion�rio de dados das tabelas OHH/OHI n�o est� atualizado, verifique.")
			EndIf

			If lRet
				lRet := DelOHHOHI()
			EndIf

			If lRet
				AjustaFat() // Ajusta Faturas antigas (que n�o distribuem despesas reembols�veis e tribut�veis)
			EndIf

		Else
			cFilAnt := cFilAux // Troca filial da mesma empresa
		EndIf

		If lRet
			FWMsgRun(, {|| RecRastr() }, "Compatibilizador", I18N("Compatibilizando a empresa: #1 e filial: #2...", {"["+cEmpAnt+"]", "["+cFilAnt+"] "}))
		Else
			Exit
		EndIf

		nP := nI + 1
		If nP <= Len(aEmpresas) .And. aEmpresas[nP][1] != cEmpAux
			RpcClearEnv()
		EndIf
	Next nI

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} RecRastr()
Rotina de abertura de ambiente e execu��o do compatibilizador por empresa e filial.

@return aEmpresas   Array com as informa�oes das empresas

@author Abner Oliveira / Jorge Martins / Bruno Ritter  
@since  02/03/2020
/*/
//-------------------------------------------------------------------
Static Function RecRastr()
	Local oProcess := Nil

		BEGIN TRANSACTION
			oProcess := MsNewProcess():New({|| UPDOHHOHI(oProcess)}, "Aguarde", "Recalculando...", .T.)
			oProcess:Activate()
		END TRANSACTION

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} DelOHHOHI()
Exclui os registros de Rastreabilidade de valores por fatura/caso (OHI)
e Posi��o hist�rica do contas a receber (OHH)

@return lRet   Indica se as exclus�es foram realizadas com sucesso

@author Abner Oliveira / Jorge Martins / Bruno Ritter
@since  02/03/2020
/*/
//-------------------------------------------------------------------
Static Function DelOHHOHI()
	Local cMsgErro := ""
	Local cUpdate  := ""
	Local lRet     := .T.

	cUpdate := "UPDATE " + RetSqlName("OHH") + " SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ WHERE D_E_L_E_T_ = ' ' AND OHH_TPENTR = '2' "

	If TCSqlExec(cUpdate) < 0
		cMsgErro := TCSQLError()
	EndIf

	If Empty(cMsgErro)
		cUpdate := "UPDATE " + RetSqlName("OHI") + " SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ WHERE D_E_L_E_T_ = ' ' "
		If TCSqlExec(cUpdate) < 0
			cMsgErro := TCSQLError()
		EndIf
	EndIf

	If !Empty(cMsgErro)
		lRet := .F.
		ApMsgStop(cMsgErro)
	EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} UPDOHHOHI()
Recria os registros de Rastreabilidade de valores por fatura/caso (OHI)
e Posi��o hist�rica do contas a receber (OHH)

@param oProcess, Objeto de processa

@author Abner Oliveira / Jorge Martins / Bruno Ritter
@since  02/03/2020
/*/
//-------------------------------------------------------------------
Static Function UPDOHHOHI(oProcess)
	Local cMsgErro     := ""
	Local nSaldo       := 0
	Local nSE1Recno    := 0
	Local nSE5Recno    := 0
	Local nSE1Saldo    := 0
	Local nSE1RecRA    := Nil
	Local nTaxaMoe     := 0
	Local nValAcess    := 0
	Local nVlDesc      := 0
	Local nVlAcres     := 0
	Local nAbatimentos := 0
	Local nValCRec     := 0
	Local nQtdSE1      := 0
	Local nQtdSE5      := 0
	Local lCompens     := .F.
	Local aSE1Recno    := {}
	Local aSE5Recno    := {}

	Local nTamFil      := TamSX3("NXA_FILIAL")[1]
	Local nTamEsc      := TamSX3("NXA_CESCR")[1]
	Local cTamFilial   := cValToChar(nTamFil)
	Local cIniEscr     := cValToChar(nTamFil + 2)
	Local cTamEscr     := cValToChar(nTamEsc)
	Local cIniFatur    := cValToChar(nTamFil + 1 + nTamEsc + 2)
	Local cTamFatur    := cValToChar(TamSX3("NXA_COD")[1])
	
	cQrySE1 := " SELECT SE1.R_E_C_N_O_ RECNOSE1"
	cQrySE1 +=   " FROM " + RetSqlName("SE1") + " SE1 "
	cQrySE1 +=  " WHERE SE1.E1_FILIAL = '" + xFilial("SE1") + "' "
	cQrySE1 +=    " AND SE1.E1_JURFAT <> '" + Space(TamSx3('E1_JURFAT')[1]) + "' "
	cQrySE1 +=    " AND SE1.D_E_L_E_T_ = ' ' "
	cQrySE1 +=    " AND NOT EXISTS ( SELECT 1 "
	cQrySE1 +=                       " FROM " + RetSqlName("NXA") + " NXA "
	cQrySE1 +=                      " WHERE NXA.NXA_FILIAL = SUBSTRING(E1_JURFAT, 1, " + cTamFilial + ") "
	cQrySE1 +=                        " AND NXA.NXA_CESCR  = SUBSTRING(E1_JURFAT, " + cIniEscr  + ", " + cTamEscr  + ") "
	cQrySE1 +=                        " AND NXA.NXA_COD    = SUBSTRING(E1_JURFAT, " + cIniFatur + ", " + cTamFatur + ") "
	cQrySE1 +=                        " AND NXA.NXA_SITUAC = '2' "
	cQrySE1 +=                        " AND NXA.D_E_L_E_T_ = ' ' "
	cQrySE1 +=                   " ) "
	cQrySE1 +=  " ORDER BY RECNOSE1 "
	
	aSE1Recno := JurSql(cQrySE1, "RECNOSE1")
	SE1->( DbSetOrder(1) ) // E1_FILIAL, E1_PREFIXO, E1_NUM, E1_PARCELA, E1_TIPO

	oProcess:SetRegua1(2)
	oProcess:SetRegua2(Len(aSE1Recno))

	oProcess:IncRegua1("Criando posi��o hist�rica do CR: 1 de 2.")

	For nQtdSE1 := 1 To Len(aSE1Recno)
		oProcess:IncRegua2(i18n( "Criando OHH inicial: #1 de: #2", { cValToChar(nQtdSE1), cValToChar(Len(aSE1Recno))})) 
		nSE1Recno := aSE1Recno[nQtdSE1][1]
		
		SE1->(DbGoTo(nSE1Recno))

		nSE1Saldo := SE1->E1_SALDO // Bkp do saldo atual

		RecLock("SE1", .F.)
			SE1->E1_SALDO := SE1->E1_VALOR
		SE1->(MsUnLock())

		J255APosHis(nSE1Recno,SE1->E1_EMISSAO,,, .T., SE1->E1_SALDO) // Atualiza a posi��o hist�rica do contas a receber

		RecLock("SE1", .F.)
			SE1->E1_SALDO := nSE1Saldo // Devolve saldo atual
		SE1->(MsUnLock())
	Next

	oProcess:IncRegua1("Recalculando rastreabilidade: 2 de 2.")
	oProcess:SetRegua2(Len(aSE1Recno))
	For nQtdSE1 := 1 To Len(aSE1Recno)

		oProcess:IncRegua2(i18n("Recalculando baixas do t�tulo: #1 de: #2", {cValToChar(nQtdSE1), cValToChar(Len(aSE1Recno))}))
		nSE1Recno := aSE1Recno[nQtdSE1][1]
		
		SE1->(DbGoTo(nSE1Recno))

		nAbatimentos := SomaAbat(SE1->E1_PREFIXO, SE1->E1_NUM, SE1->E1_PARCELA, "R", 1,, SE1->E1_CLIENTE, SE1->E1_LOJA, SE1->E1_FILIAL, SE1->E1_EMISSAO) // Valor de impostos e abatimentos dos t�tulos - Usado somente se o saldo estiver zerado
		nSE1Saldo    := SE1->E1_SALDO // Bkp do saldo atual

		RecLock("SE1", .F.)
			SE1->E1_SALDO := SE1->E1_VALOR
		SE1->(MsUnLock())

		cQrySE5 := " SELECT SE5.R_E_C_N_O_ RECNOSE5"
		cQrySE5 +=   " FROM " + RetSqlName("SE5") + " SE5 "
		cQrySE5 +=  " WHERE SE5.E5_FILIAL  = '" + SE1->E1_FILIAL  + "' "
		cQrySE5 +=    " AND SE5.E5_TIPO    = '" + SE1->E1_TIPO    + "' "
		cQrySE5 +=    " AND SE5.E5_PREFIXO = '" + SE1->E1_PREFIXO + "' "
		cQrySE5 +=    " AND SE5.E5_NUMERO  = '" + SE1->E1_NUM     + "' "
		cQrySE5 +=    " AND SE5.E5_PARCELA = '" + SE1->E1_PARCELA + "' "
		cQrySE5 +=    " AND ((SE5.E5_MOTBX = 'LIQ' AND SE5.E5_SITUACA = ' ') OR SE5.E5_MOTBX <> 'LIQ') "
		cQrySE5 +=    " AND SE5.E5_TIPODOC IN ('BA', 'CP', 'VL') "
		cQrySE5 +=    " AND SE5.E5_RECPAG = 'R' "
		cQrySE5 +=    " AND SE5.E5_DTCANBX = '" + Space(TamSx3('E5_DTCANBX')[1]) + "' "
		cQrySE5 +=    " AND SE5.D_E_L_E_T_ = ' ' "
		cQrySE5 +=    " AND NOT EXISTS (SELECT SE5EST.R_E_C_N_O_ RECNOEST "
		cQrySE5 +=                      " FROM " + RetSqlName("SE5") + " SE5EST "
		cQrySE5 +=                     " WHERE SE5EST.E5_FILIAL  = SE5.E5_FILIAL "
		cQrySE5 +=                       " AND SE5EST.E5_TIPO    = SE5.E5_TIPO "
		cQrySE5 +=                       " AND SE5EST.E5_PREFIXO = SE5.E5_PREFIXO "
		cQrySE5 +=                       " AND SE5EST.E5_NUMERO  = SE5.E5_NUMERO "
		cQrySE5 +=                       " AND SE5EST.E5_PARCELA = SE5.E5_PARCELA "
		cQrySE5 +=                       " AND SE5EST.E5_TIPODOC = 'ES' "
		cQrySE5 +=                       " AND SE5EST.E5_SEQ     = SE5.E5_SEQ "
		cQrySE5 +=                       " AND SE5EST.D_E_L_E_T_ = ' ' "
		cQrySE5 +=                   " ) "
		cQrySE5 +=  " ORDER BY SE5.E5_DATA "

		aSE5Recno := JurSql(cQrySE5, "RECNOSE5")

		For nQtdSE5 := 1 To Len(aSE5Recno)
			nSE5Recno := aSE5Recno[nQtdSE5][1]

			SE5->(DbGoTo(nSE5Recno))
			nSE1RecRA := Nil
			If !Empty(SE5->E5_DOCUMEN) .And. SE5->E5_TIPODOC == "CP"
				If SE1->(DbSeek(SE5->E5_FILIAL+SE5->E5_DOCUMEN))
					nSE1RecRA := SE1->(Recno())
					SE1->(DbGoTo(nSE1Recno))
				EndIf
			EndIf

			nTaxaMoe     := IIf(SE5->E5_TXMOEDA == 0, 1, 1 / SE5->E5_TXMOEDA) // Taxa de convers�o para moeda da baixa
			nValAcess    := J256ValAce( SE5->E5_IDORIG )
			nVlDesc      := ( SE5->E5_VLDESCO * nTaxaMoe ) + IIf( nValAcess < 0, nValAcess * (-1), 0 ) // - Descontos - Decrescimo - Valores Acess�rios (Subtrair)
			nVlAcres     := ( ( SE5->E5_VLJUROS + SE5->E5_VLMULTA ) * nTaxaMoe ) + IIf( nValAcess > 0, nValAcess, 0 ) // + Acressimo + Tx. Permanencia + Multa + Valores Acess�rios (Somar)
			lCompens     := SE5->E5_TIPODOC == "CP" // Compensa��o
			nValCRec     := IIf(lCompens, SE5->E5_VALOR, SE5->E5_VLMOED2) - nVlAcres + nVlDesc // Recomposi��o do valor da baixa sem os valores adicionais

			If SE1->E1_SALDO == nValCRec + nAbatimentos
				nValCRec += nAbatimentos
			EndIf
			nSaldo := SE1->E1_SALDO - nValCRec // Subtrai o valor recebido do saldo atual

			RecLock("SE1", .F.)
			SE1->E1_SALDO := nSaldo
			SE1->(MsUnLock())

			J256GrvRas(nSE1Recno, nSE5Recno, nSE1RecRA) // Rastreamento de recebimento por casos da fatura
			J255APosHis(nSE1Recno, SE5->E5_DATA,,, .T., SE1->E1_SALDO)  // Atualiza a posi��o hist�rica do contas a receber

		Next

		RecLock("SE1", .F.)
			SE1->E1_SALDO := nSE1Saldo // Devolve saldo atual
		SE1->(MsUnLock())

	Next

Return cMsgErro

//-------------------------------------------------------------------
/*/{Protheus.doc} AjustaFat()
Ajusta valores de faturas que n�o tinham valores de despesas desmembrados

@author Jorge Martins / Bruno Ritter
@since  05/03/2020
/*/
//-------------------------------------------------------------------
Static Function AjustaFat()
	Local cQueryFat  := ""
	Local aDadosFat  := {}
	Local nI         := 0
	Local nRecNXAOld := 0
	Local nRecNXBOld := 0
	Local nRecNXCOld := 0
	Local lAtuNXA    := .F.
	Local lAtuNXB    := .F.
	Local lAtuNXC    := .F.

	cQueryFat := " SELECT NXA.R_E_C_N_O_ RECNONXA, NXB.R_E_C_N_O_ RECNONXB, NXC.R_E_C_N_O_ RECNONXC "
	cQueryFat +=   " FROM " + RetSqlName("NXA") + " NXA "
	cQueryFat +=  " INNER JOIN " + RetSqlName("NXB") + " NXB "
	cQueryFat +=     " ON NXB.NXB_FILIAL = NXA.NXA_FILIAL "
	cQueryFat +=    " AND NXB.NXB_CESCR  = NXA.NXA_CESCR "
	cQueryFat +=    " AND NXB.NXB_CFATUR = NXA.NXA_COD "
	cQueryFat +=    " AND NXB.NXB_VLFATD > 0 "
	cQueryFat +=    " AND NXB.D_E_L_E_T_ = ' ' "
	cQueryFat +=  " INNER JOIN " + RetSqlName("NXC") + " NXC "
	cQueryFat +=     " ON NXC.NXC_FILIAL = NXB.NXB_FILIAL "
	cQueryFat +=    " AND NXC.NXC_CESCR  = NXB.NXB_CESCR "
	cQueryFat +=    " AND NXC.NXC_CFATUR = NXB.NXB_CFATUR "
	cQueryFat +=    " AND NXC.NXC_CCONTR = NXB.NXB_CCONTR "
	cQueryFat +=    " AND NXC.NXC_VLDFAT > 0 "
	cQueryFat +=    " AND NXC.D_E_L_E_T_ = ' ' "
	cQueryFat +=  " WHERE NXA.NXA_SITUAC = '1' "
	cQueryFat +=    " AND NXA.NXA_TIPO   = 'FT' "
	cQueryFat +=    " AND NXA.NXA_VLFATD > 0 "
	cQueryFat +=    " AND NXA.NXA_VLREMB = 0 "
	cQueryFat +=    " AND NXA.NXA_VLTRIB = 0 "
	cQueryFat +=    " AND NXA.D_E_L_E_T_ = ' ' "
	cQueryFat +=  " ORDER BY NXA.R_E_C_N_O_, NXB.R_E_C_N_O_, NXC.R_E_C_N_O_ "

	aDadosFat := JurSQL(cQueryFat, {"RECNONXA", "RECNONXB", "RECNONXC"})

	If Len(aDadosFat) > 0
		For nI := 1 To Len(aDadosFat)

			lAtuNXA := nRecNXAOld <> aDadosFat[nI][1]
			lAtuNXB := nRecNXBOld <> aDadosFat[nI][2]
			lAtuNXC := nRecNXCOld <> aDadosFat[nI][3]

			If lAtuNXA
				NXA->(DbGoTo(aDadosFat[nI][1]))

				RecLock("NXA", .F.)
					NXA->NXA_VLREMB := NXA->NXA_VLFATD
					NXA->NXA_REMBMN := NXA->NXA_FATDMN
				NXA->(MsUnLock())
			EndIf

			If lAtuNXB
				NXB->(DbGoTo(aDadosFat[nI][2]))

				RecLock("NXB", .F.)
					NXB->NXB_VLREMB := NXB->NXB_VLFATD
				NXB->(MsUnLock())
			EndIf

			If lAtuNXC
				NXC->(DbGoTo(aDadosFat[nI][3]))

				RecLock("NXC", .F.)
					NXC->NXC_VLREMB := NXC->NXC_VLDFAT
				NXC->(MsUnLock())
			EndIf

			nRecNXAOld := aDadosFat[nI][1]
			nRecNXBOld := aDadosFat[nI][2]
			nRecNXCOld := aDadosFat[nI][3]

		Next

	EndIf

	JurFreeArr(@aDadosFat)

Return Nil