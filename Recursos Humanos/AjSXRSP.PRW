#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"

/*
����������������������������������������������������������������������������������
����������������������������������������������������������������������������������
������������������������������������������������������������������������������ͻ��                 
���Programa    �AjSXRSP    �Autor    �Inova��o - RH       � Data �  29/06/2015 ���
������������������������������������������������������������������������������͹��
���Desc.       � Fonte respons�vels pelos ajustas na vers�o p12.               ���
���            �                                                               ���
������������������������������������������������������������������������������͹��
���Analista    � DATA      � Ticket/Issue�  MOTIVO DA ALTERACAO                ���
���            �           �             �                                     ���
���Marcelo F   �20/01/2017 � #375145     �Ajuste na fun��o RSPLoad para inclus.���
���            �           � MRH-5681    �do campo respons�vel no perg. RSR004 ���
���            �           �             �                                     ���
��������������������ͱ��������������������������������������������������������ͼ��
����������������������������������������������������������������������������������
����������������������������������������������������������������������������������
*/ 

Main function RSPLoad()
Local lRet := .T.
Local aSXBDados := {}
Local aDadosSX3	:= {}

lRet := fChkConv12()
If !lRet
	Return
Endif
If GetRpoRelease() != "12.1.017"
	Return
EndIf

SX1->(dbSetOrder(1))

//----------------------------- RSR004 - AJUSTE ----------------------------//
//Atualiza��o de duas novas perguntas para o relat�rio de vagas RSPR004 
//N�o existe tratamento de pais, pois o campo Respons�vel � 'ALL'
//CheckSX1 (ENGXFUN.PRW)
CheckSX1("RSR004","16","Respons�vel ?","�Responsable ?","Responsible ?",;
		"MV_CHG","C",99,0,0,"G","","SRA","","S", "MV_PAR16",;
		"","","","","","","","","","","","","","","","",,,,"")
CheckSX1("RSR004","17","Analitico/Sintetico ?","�Analitico/Sintetico ?","Detailed/Summarized ?",;
		"MV_CHH","N",1,0,2,"C","","","","S", "MV_PAR17",;
		"Anal�tico","Analitico","Detailed","","Sint�tico","Sintetico","Summarized","","","","","","","","","",,,,".RHTPREL.")

//De acordo com documenta��o, XB_COLUNA n�o � alterado pelo EngSXBxxx
If SXB->(DbSeek( "SRAPT3201" ) ) .And. SXB->XB_COLUNA != "13"
	SXB->( Reclock("SXB", .F.) )
		SXB->XB_COLUNA   := "13"
	SXB->( MsUnlock() )
EndIf
If SXB->(DbSeek( "SRAPT3202" ) ) .And. SXB->XB_COLUNA != "14"
	SXB->( Reclock("SXB", .F.) )
		SXB->XB_COLUNA   := "14"
	SXB->( MsUnlock() )
EndIf

aAdd( aSXBDados, { {"SRAPT3", "6", "01", "  "}, { {"XB_CONTEM", ""} } } )
aAdd( aSXBDados, { {"SRAPT3", "7", "01", "  "}, { {"XB_CONTEM", "Space(FWSizeFilial())"} } } )
aAdd( aSXBDados, { {"SRAPT3", "7", "02", "  "}, { {"XB_CONTEM", "Replicate('Z', FWSizeFilial())"} } } )

If FindFUnction("EngSXB117")
	EngSXB117( aSXBDados )
EndIf

//nao usado
Aadd ( aDadosSX3, { { 'QP_AREA' }, { { 'X3_USADO',  X3TreatUso("x       x       x       x       x       x       x       x       x       x       x       x       x       x       x       ") } , { 'X3_RESERV',  X3TreatUso("x  x xxxx       ")} } } )
Aadd ( aDadosSX3, { { 'QP_TOPICO' }, { { 'X3_USADO',  X3TreatUso("x       x       x       x       x       x       x       x       x       x       x       x       x       x       x       ") } , { 'X3_RESERV',  X3TreatUso("x  x xxxx       ")} } } )
Aadd ( aDadosSX3, { { 'QL_DTDEMIS' }, { { 'X3_VALID',  "Rsp010Dem()" } } } )

//Retirar obrigatoriedade
Aadd ( aDadosSX3, { { 'QS_POSTO' }, { { 'X3_RESERV',  X3TreatUso("x    x  x       ") } } } )

Aadd ( aDadosSX3, { { 'QS_PERFIL' }, { { 'X3_RELACAO',  'IIf(!IsBlind() .And. INCLUI,"",MSMM(SQS->QS_CODPERF,80))' } } } )

If FindFUnction("ENGSX3117")
	EngSX3117(aDadosSX3)
Endif

Return (.T.)
