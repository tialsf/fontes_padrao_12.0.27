#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"

/*
����������������������������������������������������������������������������������
����������������������������������������������������������������������������������
������������������������������������������������������������������������������ͻ��                 
���Programa    �AjSXTRM   �Autor    �Inova��o - RH       � Data �  29/06/2015 ���
������������������������������������������������������������������������������͹��
���Desc.       � Fonte respons�vels pelos ajustas na vers�o p12.               ���
���            �                                                               ���
������������������������������������������������������������������������������͹��
���Analista    � DATA      � Ticket/Issue�  MOTIVO DA ALTERACAO                ���
���            �           �             �                                     ���
��������������������ͱ��������������������������������������������������������ͼ��
����������������������������������������������������������������������������������
����������������������������������������������������������������������������������
*/ 

Main function TRMLoad()
Local lRet := .T.
Local aDadosSX3	:= {}
Local lDicInDdb := MPDicInDB()

lRet := fChkConv12()
If !lRet
	Return
Endif
If GetRpoRelease() != "12.1.017"
	Return
EndIf

If FindFUnction("ENGSX3117")

	Aadd ( aDadosSX3, { { 'RA2_REALIZ' }, { { 'X3_RELACAO',  "'N'" } , { 'X3_VALID',  "Pertence('SN')" } } } )
	If lDicInDdb
		Aadd ( aDadosSX3, { { 'Q3_DESCLAS' }, { { 'X3_RESERV', "  xxxx x        ", "    x           " } } } )
	Else
		Aadd ( aDadosSX3, { { 'Q3_DESCLAS' }, { { 'X3_RESERV', X3TreatUso("xx   xx xx      "), X3TreatUso("x    x  x       ") } } } )
	EndIf

	AAdd( aDadosSX3, { { 'RA0_CGC'}  , { { 'X3_VALID', 'Vazio() .Or. ( CGC(M->RA0_CGC) .And. fValidCGC0(M->RA0_CGC) )' }} } )
	AAdd( aDadosSX3, { { 'RA2_CURSO'}, { { 'X3_VALID', 'NaoVazio() .And. ExistCpo("RA1") .And. Tr050Curso(.F.,1)'      }} } )

	EngSX3117(aDadosSX3)
Endif

Return (.T.)
