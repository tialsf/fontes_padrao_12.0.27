/*
����������������������������������������������������������������������������������
����������������������������������������������������������������������������������
������������������������������������������������������������������������������ͻ��                 
���Programa    �AjSXVDF    �Autor    �Inova��o - RH       � Data �  29/06/2015 ���
������������������������������������������������������������������������������͹��
���Desc.       � Fonte respons�vels pelos ajustas na vers�o p12.               ���
���            �                                                               ���
������������������������������������������������������������������������������͹��
���Programador � DATA      � Chamado     �  MOTIVO DA ALTERACAO                ���
���            �           �             �                                     ���
���            �           �             �                                     ���
���            �           �             �                                     ���
��������������������ͱ��������������������������������������������������������ͼ��
����������������������������������������������������������������������������������
����������������������������������������������������������������������������������
*/ 
Main Function VDFLoad()

Local aCampos 	:= {}
Local aTab		:= {}
Local aAreaSX3	:= SX3->(getarea())
Local nPos, nNewPos, cOrdem, nX, cControle

Private aDadosSX3 := {}

If !fUsaGFP() //Nao executa se nao estiver habilitado o MV_SIGAGFP
	Return()
EndIf
If GetRpoRelease() != "12.1.017"
	Return
EndIf
cControle	:= SuperGetMv("MV_AJSXVDF", NIL, "000") //Controle de inicializa��o do m�dulo SIGAVDF       

//--------------
// Controle 001  
//--------------
If cControle < '001' 	// Reordenacao de campos da SRA

	SX3->(dbsetorder(1))
	SX3->(dbseek("SRA"))
	While SX3->(!eof()) .and. SX3->X3_ARQUIVO == 'SRA'
		aadd(aCampos,{SX3->X3_ORDEM,alltrim(SX3->X3_CAMPO),SX3->X3_ORDEM})
		SX3->(dbskip())
	EndDo
	
	//Busca o posicionamento do campo RA_HRSDIA 
	nPos  := aScan( aCampos , { |x| x[2] == "RA_HRSDIA" } )
	//Se o campo seguinte na ordem padrao nao for RA_CATFUN, devera executar a reorganizacao da ordem
	If nPos > 0 .and. nPos+1 <= len(aCampos)
		If aCampos[nPos+1,2] != 'RA_CATFUNC'
			cOrdem := aCampos[nPos,1] //Guarda a ordem do RA_HRSDIA para usar como ponto de partida
			//          Campo        Subordem provisoria
			aadd(aTab,{'RA_CATFUNC','01'})
			aadd(aTab,{'RA_CODFUNC','02'})
			aadd(aTab,{'RA_DESCFUN','03'})
			aadd(aTab,{'RA_CARGO'  ,'04'})
			aadd(aTab,{'RA_DCARGO' ,'05'})
			aadd(aTab,{'RA_SALARIO','06'})
			aadd(aTab,{'RA_ANTEAUM','07'})
			aadd(aTab,{'RA_TIPOALT','08'})
			aadd(aTab,{'RA_DATAALT','09'})
			For nX := 1 to len(aTab)
				If (nPos  := aScan( aCampos , { |x| x[2] == aTab[nX,1] } )) > 0
					aCampos[nPos,3] := cOrdem + aTab[nX,2] //Acrescenta a subordem na ordem de ponto de partida
				EndIf
			Next nX

			Reordena(aCampos,"SRA")
			
		EndIf
	EndIf
	PUTMV("MV_AJSXVDF", "001") //Atualiza o controle de inicializacao do SIGAVDF
EndIf
	
//--------------
// Controle 002
//--------------
If cControle < '002'	// Reordenacao de campos da SRF

	//Verifica se existe o campo RF_DIREMAN e executa somente se encontr�-lo
	SX3->(dbsetorder(2))
	If SX3->(dbseek("RF_DIREMAN"))
		
		SX3->(dbsetorder(1))
		SX3->(dbseek("SRF"))
		While SX3->(!eof()) .and. SX3->X3_ARQUIVO == 'SRF'
			aadd(aCampos,{SX3->X3_ORDEM,alltrim(SX3->X3_CAMPO),SX3->X3_ORDEM})
			SX3->(dbskip())
		EndDo
		
		//Busca o posicionamento do campo RF_ADMISSA
		nPos  := aScan( aCampos , { |x| x[2] == "RF_ADMISSA" } )
		//Se o campo seguinte na ordem padrao nao for RF_STATUS, devera executar a reorganizacao da ordem
		If nPos > 0 .and. nPos+1 <= len(aCampos)
			If aCampos[nPos+1,2] != 'RF_STATUS'
				cOrdem := aCampos[nPos,1] //Guarda a ordem do RF_ADMISSA para usar como ponto de partida
				//          Campo        Subordem provisoria
				aadd(aTab,{'RF_STATUS' ,'01'})
				aadd(aTab,{'RF_PD'	   ,'02'})
				aadd(aTab,{'RF_DESCPD' ,'03'})
				aadd(aTab,{'RF_DATABAS','04'})
				aadd(aTab,{'RF_DATAFIM','05'})
				aadd(aTab,{'RF_DIASDIR','06'})
				aadd(aTab,{'RF_DIASPRG','07'})
				aadd(aTab,{'RF_DIREMAN','08'})
				For nX := 1 to len(aTab)
					If (nPos  := aScan( aCampos , { |x| x[2] == aTab[nX,1] } )) > 0
						aCampos[nPos,3] := cOrdem + aTab[nX,2] //Acrescenta a subordem na ordem de ponto de partida
					EndIf
				Next nX
	
				Reordena(aCampos,"SRF")
				
			EndIf
		EndIf
		PUTMV("MV_AJSXVDF", "002") //Atualiza o controle de inicializacao do SIGAVDF
	EndIf
	
EndIf

If FindFunction("ENGSX3117")

		Aadd ( aDadosSX3, { { 'REY_FILFUN'  },	{ { 'X3_ORDEM','06'} } } )
		Aadd ( aDadosSX3, { { 'REY_CODFUN'  },	{ { 'X3_ORDEM','07'} } } )
		Aadd ( aDadosSX3, { { 'REY_DESFUN'  },	{ { 'X3_ORDEM','08'}, {'X3_RELACAO','Iif(!INCLUI,Posicione("SRJ",1,REY->(REY_FILFUN+REY_CODFUN),"RJ_DESC"),"")'} } } )
		Aadd ( aDadosSX3, { { 'REY_CLASSI'  },	{ { 'X3_ORDEM','09'} } } )
		Aadd ( aDadosSX3, { { 'REY_CLAORI'  },	{ { 'X3_ORDEM','10'} } } )
		Aadd ( aDadosSX3, { { 'REY_SITUAC'  },	{ { 'X3_ORDEM','11'} } } )
		Aadd ( aDadosSX3, { { 'REY_CATEG'   },	{ { 'X3_ORDEM','12'} } } )
		Aadd ( aDadosSX3, { { 'REY_NOMEAC'  },	{ { 'X3_ORDEM','13'} } } )
		Aadd ( aDadosSX3, { { 'REY_POSSE'   },	{ { 'X3_ORDEM','14'} } } )
		Aadd ( aDadosSX3, { { 'REY_EXERCI'  },	{ { 'X3_ORDEM','15'} } } )
		Aadd ( aDadosSX3, { { 'REY_DEPTO'   },	{ { 'X3_ORDEM','16'} } } )
		Aadd ( aDadosSX3, { { 'REY_DESDEP'  },	{ { 'X3_ORDEM','17'} } } )
		Aadd ( aDadosSX3, { { 'REY_EXONER'  },	{ { 'X3_ORDEM','18'} } } )
		Aadd ( aDadosSX3, { { 'REY_TIPDOC'  },	{ { 'X3_ORDEM','19'} } } )
		Aadd ( aDadosSX3, { { 'REY_ANODOC'  },	{ { 'X3_ORDEM','20'} } } )
		Aadd ( aDadosSX3, { { 'REY_NUMDOC'  },	{ { 'X3_ORDEM','21'} } } )
		Aadd ( aDadosSX3, { { 'REY_DTATPO'  },	{ { 'X3_ORDEM','22'} } } )
		Aadd ( aDadosSX3, { { 'REY_COMARC'  },	{ { 'X3_ORDEM','23'} } } )
		Aadd ( aDadosSX3, { { 'REY_CONOME'  },	{ { 'X3_ORDEM','24'} } } )
		Aadd ( aDadosSX3, { { 'REY_NOTA'    },	{ { 'X3_ORDEM','25'} } } )
		Aadd ( aDadosSX3, { { 'REY_STATUS'  },	{ { 'X3_ORDEM','26'} } } )
		Aadd ( aDadosSX3, { { 'REY_OK'      },	{ { 'X3_ORDEM','27'} } } )

		Aadd ( aDadosSX3, { { 'RI7_FILFUN'  },	{ { 'X3_ORDEM','04'} } } )
		Aadd ( aDadosSX3, { { 'RI7_CODFUN'  },	{ { 'X3_ORDEM','05'} } } )
		Aadd ( aDadosSX3, { { 'RI7_DESFUN'  },	{ { 'X3_ORDEM','06'}, {'X3_RELACAO','Iif(!INCLUI,Posicione("SRJ",1,RI7->(RI7_FILFUN+RI7_CODFUN),"RJ_DESC"),"")'} } } )
		Aadd ( aDadosSX3, { { 'RI7_VAGAMP'  },	{ { 'X3_ORDEM','07'} } } )
		Aadd ( aDadosSX3, { { 'RI7_VAGPCD'  },	{ { 'X3_ORDEM','08'} } } )


		
		EngSX3117(aDadosSX3)

Endif

RestArea(aAreaSX3)
	
Return()
		
//-------------------------------//
// Funcao para reordernar no SX3 //
//-------------------------------//
Static Function Reordena(aCampos,cTab)

Local nX, cOrdem
Local aDados := {}

//Reordena aCampos pela nova ordem/subordem
aSort(aCampos,,,{ | x,y | x[3] < y[3] } )
cOrdem := '00'
For nX := 1 to len(aCampos)
	aCampos[nX,1] := soma1(cOrdem)
	cOrdem := aCampos[nX,1]
Next nX

//Atualiza SX3 com a nova ordem
SX3->(dbgotop())
SX3->(dbseek(cTab))
While SX3->(!eof()) .and. SX3->X3_ARQUIVO == cTab
	If (nPos  := aScan( aCampos , { |x| x[2] == alltrim(SX3->X3_CAMPO) } )) > 0
		Aadd ( aDados, { { cTab }, { { 'X3_ORDEM', aCampos[nPos,1] } } } )
	EndIf
	SX3->(dbskip())
EndDo

If FindFUnction("ENGSX3117")
	EngSX3117(aDados)
Endif

Return()
