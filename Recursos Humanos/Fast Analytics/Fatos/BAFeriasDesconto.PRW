#INCLUDE "BADefinition.CH"

NEW ENTITY DescFerias

//-------------------------------------------------------------------
/*/{Protheus.doc} BAFeriasDesconto
A tabela fato Desconto F�rias apresenta informacoes dos
descontos feitos em f�rias dos funcionarios.

@author  raquel.andrade
@since   24/05/2019
/*/
//-------------------------------------------------------------------
Class BAFeriasDesconto from BAEntity
    Method Setup( ) CONSTRUCTOR
    Method BuildQuery( )
EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} Setup
Construtor padr?o.

@author  raquel.andrade
@since   24/05/2019
/*/
//-------------------------------------------------------------------
Method Setup( ) Class BAFeriasDesconto
    _Super:Setup("DescFerias", FACT, "SRR")
    
    //---------------------------------------------------------
	// Define que a extra??o da entidade ser? por m?s
	//---------------------------------------------------------
	//_Super:SetTpExtr( BYMONTH )
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} BuildQuery
Constr?i a query da entidade.

@return aQuery, array, Retorna as consultas da entidade por empresa.

@author  raquel.andrade
@since   24/05/2019
/*/
//-------------------------------------------------------------------
Method BuildQuery( ) Class BAFeriasDesconto
    Local cQuery := ""
    
    cQuery := "SELECT BK_EMPRESA, BK_FILIAL, BK_FUNCIONARIO, BK_CAT, BK_DEPTO, BK_CARGO, BK_FUNCAO, BK_VERBA, BK_FUNCPROC, BK_PERIODO, BK_CENTRO_CUSTO, BK_ITEM_CONTABIL, BK_CLASSE_VALOR,  " +;
              " BK_ROTEIRO, TIPO1, HORAS, VALOR, DTGERACAO, DTPAGAMENTO, DTINICIAL, DTFINAL    " + ;
              " FROM (" + ;
              " SELECT DISTINCT <<KEY_COMPANY>> AS BK_EMPRESA" + ; 
                               ",<<KEY_FILIAL_RR_FILIAL>> AS BK_FILIAL" + ;
		                       ",<<KEY_SRA_RA_FILIAL+RR_MAT>> AS BK_FUNCIONARIO" + ;
		                       ",<<KEY_SX5_X5_FILIAL+SRA.RA_CATFUNC>> AS BK_CAT" + ;
		                       ",<<KEY_SQB_QB_FILIAL+SRA.RA_DEPTO>> AS BK_DEPTO" + ; 
		                       ",<<KEY_SQ3_Q3_FILIAL+SRA.RA_CARGO>> AS BK_CARGO" + ;
		                       ",<<KEY_SRJ_RJ_FILIAL+SRA.RA_CODFUNC>> AS BK_FUNCAO" + ;
		                       ",<<KEY_SRV_RV_FILIAL+RR_PD>> AS BK_VERBA" + ;
		                       ",<<KEY_RCJ_RCJ_FILIAL+RR_PROCES>> AS BK_FUNCPROC" + ;
		                       ",<<KEY_RCH_RCH_FILIAL+RR_PROCES+RR_PERIODO+RR_SEMANA+RR_ROTEIR>> AS BK_PERIODO" + ;
		                       ",<<KEY_CTT_CTT_FILIAL+RR_CC>> AS BK_CENTRO_CUSTO" + ;
		                       ",<<KEY_CTD_CTD_FILIAL+RR_ITEM>> AS BK_ITEM_CONTABIL" + ;
		                       ",<<KEY_CTH_CTH_FILIAL+RR_CLVL>> AS BK_CLASSE_VALOR" + ;
		                       ",<<KEY_SRY_RY_FILIAL+RR_ROTEIR>> AS BK_ROTEIRO" + ; 
		                       ",RR_TIPO1 AS TIPO1" + ;
		                       ",RR_HORAS AS HORAS" + ;
		                       ",RR_VALOR AS VALOR" + ;
		                       ",RR_DATA AS DTGERACAO" + ;
		                       ",RR_DATAPAG AS DTPAGAMENTO" + ;
		                       ",SRH.RH_DATAINI  AS DTINICIAL" + ;
		                       ",SRH.RH_DATAFIM  AS DTFINAL" + ;
		                       "FROM <<SRR_COMPANY>> SRR" + ;
                               " INNER JOIN <<SRA_COMPANY>> SRA " +;
                               "    ON RA_FILIAL = <<SUBSTR_SRA_RR_FILIAL>> " +;
	                           "    AND RA_MAT = RR_MAT " +;
	                           "    AND RA_PROCES = RR_PROCES " +;
                               "    AND SRA.D_E_L_E_T_ = ' ' " +;
                               " INNER JOIN <<SX5_COMPANY>> SX5" +;
                               "    ON X5_FILIAL = <<SUBSTR_SX5_RR_FILIAL>> " +;
                               "    AND X5_TABELA = '28' " +;
                               "    AND X5_CHAVE = SRA.RA_CATFUNC " +;
                               "    AND SX5.D_E_L_E_T_ = ' ' " +;
                               " LEFT JOIN <<SQB_COMPANY>> SQB" +;
                               "    ON QB_FILIAL = <<SUBSTR_SQB_RR_FILIAL>> " +;
	                           "    AND QB_DEPTO = SRA.RA_DEPTO " +;
                               "    AND SQB.D_E_L_E_T_ = ' ' " +;
                               "  LEFT JOIN <<SQ3_COMPANY>> SQ3 " +;
                               "    ON Q3_FILIAL = <<SUBSTR_SQ3_RR_FILIAL>> " +;
                               "    AND Q3_CARGO = SRA.RA_CARGO " +;
                               "    AND SQ3.D_E_L_E_T_ = ' ' " +;
                               "  INNER JOIN <<SRJ_COMPANY>> SRJ " +;
                               "    ON RJ_FILIAL = <<SUBSTR_SRJ_RR_FILIAL>> " +;
                               "    AND RJ_FUNCAO = SRA.RA_CODFUNC " +;
                               "    AND SRJ.D_E_L_E_T_ = ' ' " +;
                               " INNER JOIN <<SRV_COMPANY>> SRV " +;
                               "   ON  RV_FILIAL = <<SUBSTR_SRV_RR_FILIAL>> " +;
                               "   AND RV_COD = RR_PD " +;
                               "   AND RV_TIPOCOD IN ('2') " +;
                               "   AND RV_CODFOL <> '0102' " +;
                               "   AND SRV.D_E_L_E_T_ = ' ' " +;
                               " INNER JOIN <<RCJ_COMPANY>> RCJ " +;
                               "   ON  RCJ_FILIAL = <<SUBSTR_RCJ_RR_FILIAL>> " +;
                               "   AND RCJ_CODIGO = RR_PROCES " +;
                               "   AND RCJ.D_E_L_E_T_ = ' ' " +;
                               " INNER JOIN <<RCH_COMPANY>> RCH " +;
                               "    ON RCH_FILIAL = <<SUBSTR_RCH_RR_FILIAL>> " +;
	                           "    AND RCH_PROCES = RR_PROCES " +;
	                           "    AND RCH_PER = RR_PERIODO " +;
	                           "    AND RCH_ROTEIR = RR_ROTEIR " +;
	                           "    AND RCH_NUMPAG = RR_SEMANA " +;
                               "    AND RCH.D_E_L_E_T_ = ' ' " +;
                               " INNER JOIN <<CTT_COMPANY>> CTT" +;
                               "    ON CTT_FILIAL = <<SUBSTR_CTT_RR_FILIAL>> " +;
	                           "    AND CTT_CUSTO = RR_CC " +;
                               "    AND CTT.D_E_L_E_T_ = ' ' " +;
                               " LEFT JOIN <<CTD_COMPANY>> CTD" +;
                               "    ON CTD_FILIAL = <<SUBSTR_CTD_RR_FILIAL>> " +;
	                           "    AND CTD_ITEM = RR_ITEM " +;
                               "    AND CTD.D_E_L_E_T_ = ' ' " +;
                               " LEFT JOIN <<CTH_COMPANY>> CTH" +;
                               "    ON CTH_FILIAL = <<SUBSTR_CTH_RR_FILIAL>> " +;
	                           "    AND CTH_CLVL = RR_CLVL " +;
                               "    AND CTH.D_E_L_E_T_ = ' ' " +;
                               " INNER JOIN <<SRY_COMPANY>> SRY" +;
                               "    ON RY_FILIAL = <<SUBSTR_SRY_RR_FILIAL>> " +;
	                           "    AND RY_CALCULO = RR_ROTEIR " +;
                               "    AND SRY.D_E_L_E_T_ = ' ' " +;    
                               " INNER JOIN <<SRH_COMPANY>> SRH ON RH_FILIAL = RR_FILIAL " +;
							   "   AND RH_MAT = RR_MAT " +;
							   "   AND RH_PERIODO = RR_PERIODO " +;
							   "   AND RH_ROTEIR = RR_ROTEIR " +;
							   "   AND RH_DATAINI = RR_DATA " +;
							   "   AND SRH.D_E_L_E_T_ = ' ' " +;
		                       "WHERE (  RR_DATAPAG >= <<START_DATE>> AND RR_DATAPAG <= <<FINAL_DATE>> ) " + ;
		                       "         AND (  SRY.RY_TIPO = '3' )  " + ;
                               " AND SRR.D_E_L_E_T_ = ' '  " + ;
                               " <<AND_XFILIAL_RR_FILIAL>> ) DESCFERIAS" 

Return cQuery