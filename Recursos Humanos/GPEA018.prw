#INCLUDE "PROTHEUS.CH" 
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "GPEA1010.CH"

Static aEfd 		:= If( cPaisLoc == 'BRA', If(Findfunction("fEFDSocial"), fEFDSocial(), {.F.,.F.,.F.,.F.,.F.}), {.F.,.F.,.F.,.F.,.F.} )	//Statics com carga devido ao acesso via outros modulos destas variaveis
Static lIntTAF		:= ((SuperGetMv("MV_RHTAF",, .F.) == .T.) .AND. Val(SuperGetMv("MV_FASESOC",/*lHelp*/,' ')) >= 1 ) //Integracao com TAF
Static cVerTrab		:= "2.4"	
Static cVerGPE		:= ""	
Static lDPrevPG		:= IIF (Select( "SV7" ) > 0 .AND. SV7->(ColumnPos( "V7_DPREVPG" )) > 0 , .T., .F.) 
// Static lRegra		:= SV7->(ColumnPos( "V7_REGRA" )) > 0
// Static lSeqTurn		:= SV7->(ColumnPos( "V7_SEQTURN" )) > 0
// Static lDescJor 	:= SV7->(ColumnPos( "V7_DESCJOR" )) > 0

/*/{Protheus.doc} menudef
 menu por enquanto nao utilizado, tela sera acionada via acoes relacionadas do gpea010
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/

Static Function MenuDef()
Static cEfdAviso	:= SuperGetMv( "MV_EFDAVIS" ,NIL, "0" )
Static lIntTAF		:= ((SuperGetMv("MV_RHTAF",, .F.) == .T.) .AND. Val(SuperGetMv("MV_FASESOC",/*lHelp*/,0)) >= 1 )
Static dCgIni		:= SuperGetMv( "MV_DTCGINI" ,NIL, DDATABASE )
Local aRotina 		:= {}

ADD OPTION aRotina TITLE STR0004 ACTION "PesqBrw"         OPERATION 1 ACCESS 0  //"Pesquisar"
ADD OPTION aRotina TITLE STR0005 ACTION "VIEWDEF.GPEA018" OPERATION 2 ACCESS 0  //"Visualizar"
ADD OPTION aRotina TITLE STR0006 ACTION "VIEWDEF.GPEA018" OPERATION 3 ACCESS 0  //"Incluir"
ADD OPTION aRotina TITLE STR0007 ACTION "VIEWDEF.GPEA018" OPERATION 4 ACCESS 0  //"Alterar"
ADD OPTION aRotina TITLE STR0008 ACTION "VIEWDEF.GPEA018" OPERATION 5 ACCESS 0  //"Excluir"

Return ( aRotina )

/*/{Protheus.doc} ModelDef
 ModelDef - model principal
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
Static Function ModelDef()
Local oModel	 := Nil
Local oSRAStruct := FWFormStruct(1,"SRA",{|cCampo| AllTrim(cCampo)+"|" $ "RA_FILIAL|RA_MAT|RA_NOME|"})

If ( ExistFunc( 'fVersEsoc' ), fVersEsoc( "S2260", .F.,,,@cVerTrab,@cVerGPE),)

If Empty(cVerGPE)
	cVerGPE := cVerTrab
EndIf

oStruSV7 := FwFormStruct( 1, 'SV7' )

oModel := MPFormModel():New("GPEA018",/*Pre-Validacao*/,{||FazValid()} ,{|oModel|FazCommit(oModel)},/*Cancel*/)    

osrastruct:afields[1][11] := {||SRA->RA_filial}

osrastruct:afields[2][11] := {||SRA->RA_MAT}

osrastruct:afields[3][11] := {||SRA->RA_nome}

oModel:AddFields("SRAMdField", /*cOwner*/, oSRAStruct, /*Pre-Validacao*/,/*Pos-Validacao*/  {|oModel|FazValid(oModel)} ,/*Carga*/)

If lIntTaf
	//oStruSV7:SetProperty( "V7_TURNO", MODEL_FIELD_OBRIGAT, .F. )
	oStruSV7:SetProperty( "V7_CONVC", MODEL_FIELD_WHEN, { |oModel| fWhenConv(oModel) } )
	//oStruSV7:SetProperty( "V7_DESCJOR", MODEL_FIELD_WHEN, { |oModel| fWhenDeJo(oModel) } )
EndIf

oModel:AddGrid("SV7MdGrid", "SRAMdField", oStruSV7,/*LinePre*/{ |oGrid, nLine, cAction, cField| PreLineOk(oGrid, nLine, cAction, cField) } ,/*bLinePost*/{ |oGrid, nLine, cAction, cField| PostLineOk(oGrid, nLine, cAction, cField) },/*bPre*/,/*bPost*/,/*Carga*/) 

oModel:SetRelation("SV7MdGrid",{{"V7_FILIAL","xfilial('SV7')" } , {"V7_MAT",'RA_MAT' }  },SV7->(IndexKey(1)))

oModel:GetModel("SV7MdGrid"):SetOptional(.T.)

oModel:SetVldActivate( { |oModel| fVldModel(oModel) } )

Return oModel

/*/{Protheus.doc} ViewDef
 ViewDef referente ao Model
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
Static Function ViewDef()
Local oModel	
Local oView	   
Local oSRAStruct 
Local oStruSV7	 := FWFormStruct(2,"SV7")

oStruSV7 := FwFormStruct( 2, 'SV7')

oView  := FWFormView():New()

oModel := FWLoadModel("GPEA018")

oView:SetModel(oModel)

oSRAStruct := FWFormStruct(2,"SRA",{|cCampo| AllTrim(cCampo)+"|" $ "FILIAL|RA_MAT|RA_NOME|"})

oSRAStruct:SetNoFolder()

oStruSV7:RemoveField("V7_FILIAL")

oStruSV7:RemoveField("V7_MAT")

oStruSV7:RemoveField("V7_STAT")

oView:AddField( 'SRAVwField' , oSRAStruct , 'SRAMdField')

oView:AddGrid ('SV7VwGrid' , oStruSV7   , 'SV7MdGrid')

oView:SetViewProperty("SRAVwField","OnlyView")

oView:CreateHorizontalBox("SUPERIOR",15)//cabecalho

oView:CreateHorizontalBox("INFERIOR",85)//grid  

oView:SetOwnerView('SRAVwField',"SUPERIOR" )

oView:SetOwnerView('SV7VwGrid' ,"INFERIOR")

oView:AddIncrementField( 'SV7VwGrid', 'V7_COD' )

oView:EnableTitleView('SRAVwField' ,oEmToAnsi(STR0354))
oView:EnableTitleView('SV7VwGrid'  ,oEmToAnsi(STR0355))

Return oView  

/*/{Protheus.doc} FazValid
 Valida��es do browse - tudoOk
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
Static function FazValid ()
Local oModel	:= FwModelActivate()
Local oSV7Model	:= oModel:GetModel("SV7MdGrid")
Local lRet      := .T.
Local nTamSV7   := oSV7Model:Length()
Local nOpc	    := oSV7Model:GETOPERATION() 
Local nX        := 0
Local aLstConvoc   := {}
Local nIndexConvoc := 0
Local nIndexDatas  := 0

DbselectArea("SV7")

for nx := 1 to nTamSV7
	oSV7Model:GoLine(nX)

	If !(oSV7Model:IsDeleted())
		nIndexConvoc := 0
		If !Empty(oSV7Model:GetValue("V7_CONVC"))
			nIndexConvoc  := aScan( aLstConvoc , { |x| x[2]  == oSV7Model:GetValue("V7_CONVC") } )
		EndIf
		nIndexDatas   := aScan( aLstConvoc , { |x| (x[3] <= oSV7Model:GetValue("V7_DTINI") .And. x[4] >= oSV7Model:GetValue("V7_DTINI")) .or. ;
											       (x[3] <= oSV7Model:GetValue("V7_DTFIM") .And. x[4] >= oSV7Model:GetValue("V7_DTFIM")) .or. ;
											       (x[3] >  oSV7Model:GetValue("V7_DTINI") .And. x[4] <  oSV7Model:GetValue("V7_DTFIM"))       } )
		
		If nIndexConvoc == 0
			Aadd ( aLstConvoc, {nx, oSV7Model:GetValue("V7_CONVC"), oSV7Model:GetValue("V7_DTINI"), oSV7Model:GetValue("V7_DTFIM") }  )
		Else
			Help("",1,"GPEA18CONVC")
			lRet := .F.
			exit
		EndIf
		
		If nIndexDatas > 0
			Help("",1,"GPEA18DTCOI")
			lRet := .F.
			exit
		EndIf
		
	EndIf
	
Next		

return lRet

/*/{Protheus.doc} Fazcommit
 Efetiva informa��es na Base de Dados. Obs: manter commit customizado por enquanto
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
Static function FazCommit (oModel)

Local cMsgErro	:= ""
Local cStat		:= ""
Local dDtCorte 	:= SuperGetMV("MV_DTCGINI", nil, dDataBase)
Local nOpc	    := 0    
Local lIsDel	:= .F.
Local lIsIns	:= .F.
Local lIsUpd	:= .F.
Local lRet      := .T.
Local lRetTaf   := .T.
Local oSV7Model	:= oModel:GetModel("SV7MdGrid")
Local nTamSV7   := oSV7Model:Length()
Local nX        := 0
Local lRetif	:= .F. 

If lIntTaf
	Private aCC		:= fGM23CTT()
	Private aEstb	:= fGM23SM0()
EndIf

nOpc := oSV7Model:GETOPERATION()

DbselectArea("SV7")
DbSetOrder(1)
		
Begin Transaction

If nOpc == 3 .OR. nOpc == 4 
	for nX := 1 to nTamSV7
		oSV7Model:GoLine(nX)
		lIsDel	:= oSV7Model:IsDeleted()
		lIsIns	:= oSV7Model:IsInserted()
		lIsUpd	:= oSV7Model:IsUpdated()

		If !lIsDel .AND. !lIsIns .AND. !lIsUpd
			Loop
		Else
			If !lIsDel
				If SV7->(DbSeek( SRA->RA_FILIAL + SRA->RA_MAT + oSV7Model:GetValue("V7_COD") ))
					Reclock('SV7',.F.)
				Else
					Reclock('SV7',.T.)
					SV7->V7_FILIAL := SRA->RA_FILIAL
					SV7->V7_MAT    := SRA->RA_MAT
					SV7->V7_STAT   := "1"
				EndIf
				SV7->V7_COD    := oSV7Model:GetValue("V7_COD")
				SV7->V7_CONVC  := oSV7Model:GetValue("V7_CONVC")
				SV7->V7_DTCON  := oSV7Model:GetValue("V7_DTCON")
				SV7->V7_ATIVI  := oSV7Model:GetValue("V7_ATIVI")
				SV7->V7_DTINI  := oSV7Model:GetValue("V7_DTINI")
				SV7->V7_DTFIM  := oSV7Model:GetValue("V7_DTFIM")
				SV7->V7_FUNC   := oSV7Model:GetValue("V7_FUNC")
				SV7->V7_CARG   := oSV7Model:GetValue("V7_CARG")
				SV7->V7_SALAR  := oSV7Model:GetValue("V7_SALAR")
				SV7->V7_TURNO  := oSV7Model:GetValue("V7_TURNO")
				SV7->V7_CCUS   := oSV7Model:GetValue("V7_CCUS")
				SV7->V7_DEPTO  := oSV7Model:GetValue("V7_DEPTO")
				SV7->V7_TPLOC  := oSV7Model:GetValue("V7_TPLOC")
				SV7->V7_LOCAL  := oSV7Model:GetValue("V7_LOCAL")
				SV7->V7_HRSDIA := oSV7Model:GetValue("V7_HRSDIA")
				SV7->V7_DPREVPG := oSV7Model:GetValue("V7_DPREVPG")
				/*
				SV7->V7_REGRA := oSV7Model:GetValue("V7_REGRA")
				SV7->V7_SEQTURN := oSV7Model:GetValue("V7_SEQTURN")
				SV7->V7_DESCJOR := oSV7Model:GetValue("V7_DESCJOR")
				*/
				MsUnLock()
				If lIntTaf .And. SV7->V7_DTINI >= dDtCorte .And. !Empty(oSV7Model:GetValue("V7_CONVC"))
					cStat 	:= TAFGetStat( "S-2260", AllTrim(SRA->RA_CIC) + ALLTRIM(SRA->RA_CODUNIC) + ";" + SV7->V7_CONVC )
					cMsgErro:= ""
					If cStat == "4" .And. !lIsUpd .And. !lIsIns
						Loop
					EndIf
					lRetif := (cStat == "4" .And. (lIsUpd .Or. lIsIns))
					lRetTaf := fInt2260( @cMsgErro, cVerTrab, lRetif)
					If !lRetTaf .And. !Empty(cMsgErro)
						Help( ,, OemToAnsi(STR0245),, OemToAnsi(STR0394 + SV7->V7_CONVC + CRLF + cMsgErro ), 1, 0 )//"Aten��o"##"Ocorreu o erro abaixo na integra��o da convoca��o: "
						DisarmTransaction()
						return .F.
					EndIf
				EndIf
			Else
				If SV7->(DbSeek( SRA->RA_FILIAL + SRA->RA_MAT + oSV7Model:GetValue("V7_COD") ))
					reclock('SV7',.F.)
					SV7->(DbDelete())
					MsUnLock()
				EndIf
				If lIntTaf .And. SV7->V7_DTINI >= dDtCorte .And. !Empty(oSV7Model:GetValue("V7_CONVC"))
					cStat 	:= TAFGetStat( "S-2260", AllTrim(SRA->RA_CIC) + ALLTRIM(SRA->RA_CODUNIC) + ";" + SV7->V7_CONVC )
					cMsgErro:= ""
					If cStat != "-1"
						lRetTaf := fExc2260( @cMsgErro, cVerTrab )
						If !lRetTaf .And. !Empty(cMsgErro)
							Help( ,, OemToAnsi(STR0245),, OemToAnsi(STR0394 + SV7->V7_CONVC + CRLF + cMsgErro ), 1, 0 )//"Aten��o"##"Ocorreu o erro abaixo na integra��o da convoca��o: "
							DisarmTransaction()
							return .F.
						EndIf					
					EndIf
				EndIf
			EndIf
		Endif
	Next
EndIf

End Transaction

return lRet

/*/{Protheus.doc} V7GetCargo
Gatilho Busca descricao cargo
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
Function V7GetCargo()     
Local oModel		:= FwModelActivate()
Local oModSV7		:= Nil
Local cFuncao 	
Local cCc		
Local cCargo	 
Local cCodCargo	 
Local cDesCargo	   

oModSV7     := oModel:GetModel("SV7MdGrid")

cFuncao 	:= oModSV7:GetValue("V7_FUNC")
cCc		    := oModSV7:GetValue("V7_CCUS")
cCargo	    := oModSV7:GetValue("V7_CARG") 
cCodCargo	:= Iif(Empty(cCargo),fDesc("SRJ",cFuncao,"RJ_CARGO"),cCargo) 
cDesCargo	:= fDesc("SQ3",cCodCargo+cCc,"Q3_DESCSUM")   
	
cDesCargo	:= If(Empty(cDesCargo),fDesc("SQ3",cCodCargo+Space(9),"Q3_DESCSUM"),cDesCargo) 

oModSV7:SetValue("V7_DCARG", cDesCargo)

Return ( cDesCargo )   

/*/{Protheus.doc} V7GetFunc
Gatilho Busca descricao funcao
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
Function V7GetFunc()
Local oModel		:= FwModelActivate()
Local oModSV7		:= Nil
oModSV7 := oModel:GetModel("SV7MdGrid")
oModSV7:SetValue("V7_DFUNC", SRJ->RJ_DESC)
                                                                
return SRJ->RJ_DESC

/*/{Protheus.doc} V7GetCcus
Gatilho  Busca descricao c.custos
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
Function V7GetCcus()
Local oModel		:= FwModelActivate()
Local oModSV7		:= Nil
oModSV7             := oModel:GetModel("SV7MdGrid")
oModSV7:SetValue("V7_DCCUS", CTT->CTT_DESC01)
                                                                      
return CTT->CTT_DESC01


/*/{Protheus.doc} V7GetLocal
Gatilho  Busca descricao do local
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
Function V7GetLocal ()
Local cRet    :=     Posicione("SV6", 1,Fwxfilial('SV6')+M->V7_LOCAL, "V6_DESC")                                        
Return cret

/*/{Protheus.doc} V7GetCLocal
Gatilho Busca descricao funcao
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
Function V7GetCLocal(nPar)
Local oModel		:= FwModelActivate()
Local oModSV7		:= Nil
Local cRet          := ''
oModSV7             := oModel:GetModel("SV7MdGrid")

If nPar == 1
	cRet := oModSV7:GetValue("V7_LOCAL")
Else
	cRet := oModSV7:GetValue("V7_DLOC")
EndIf

If M->V7_TPLOC <> "1"
	cRet := ""
EndIf

return cRet


/*/{Protheus.doc} V7GetDturno
Gatilho  Busca descricao do turno
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
Function V7GetDturno ()
Local oModel		:= FwModelActivate()
Local oModSV7		:= Nil
Local cRet          := ''
oModSV7             := oModel:GetModel("SV7MdGrid")
oModSV7:SetValue("V7_DTUR", SR6->R6_DESC)

cRet := SR6->R6_DESC                                                                          
return cRet


/*/{Protheus.doc} V7GetDCarg
 Busca descricao do cargo
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
Function V7GetDCarg()
Local aArea         := GetArea()
Local cRet	        := ""
Local oModel		:= FwModelActivate()
Local oModSV7		:= Nil
oModSV7 := oModel:GetModel("SV7MdGrid")

cRet := FDESC("SQ3",SV7->V7_CARG+SV7->V7_CCUS,"Q3_DESCSUM",,SV7->V7_FILIAL)
If Empty(cret)
	cRet := FDESC("SQ3",SV7->V7_CARG,"Q3_DESCSUM",,SV7->V7_FILIAL)
Endif

RestArea(aArea)
Return cRet


/*/{Protheus.doc} PostLineOk
 Validacoes solcitadas ate o momento:
 - Validar data inicial e final da Atividade
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
Static Function PostLineOk(oGrid, nLine, cAction, cField)
Local lRet 			:= .T.
Local oModel 		:= oGrid:GetModel()
Local cMsg			:= ""
Local cMsg2			:= ""

If  oGrid:GetValue("V7_DTINI")  >  oGrid:GetValue("V7_DTFIM")  
	Help("",1,"GPEA18DATAS")  //'Data Inicio de Atividade deve ser menor ou igual a Data Final desta!'
	lRet := .F.
EndIf

If  oGrid:GetValue("V7_DTINI")  <  oGrid:GetValue("V7_DTCON")  
	Help("",1,"GPEA18DTINI")  //"Data de inicio de Atividade deve ser maior ou igual a Data de Convoca��o!"
	lRet := .F.
EndIf

If  oGrid:GetValue("V7_DTINI")  <  SRA->RA_ADMISSA  
	Help( ,, OemToAnsi(STR0245),, OemToAnsi(STR0412), 1, 0, NIL, NIL, NIL, NIL, NIL, {OemToAnsi(STR0413)}) //"Aten��o"##"Data de in�cio da atividade deve ser maior ou igual a data de admiss�o."
	lRet := .F.
EndIf

If oGrid:GetValue("V7_TPLOC") == "1"
	If Empty(oGrid:GetValue("V7_LOCAL"))
		Help("",1,"GPEA18LOCAL")//"Informe o C�digo do Local"
		lRet := .F.
	EndIf
EndIf

If lIntTAF .And. !Empty(oGrid:GetValue("V7_CONVC")) .And. Empty(oGrid:GetValue("V7_DPREVPG")) .And. dCgIni <= oGrid:GetValue("V7_DTCON")
	cMsg:= OemtoAnsi(STR0299)+CRLF			//"As seguintes regras deverao ser atendidas conforme leiaute eSocial."
	cMsg+= OemtoAnsi(STR0300)+ CRLF + CRLF	//"E conforme conteudo do parametro MV_EFDAVIS "
	
	cMsg2:= OemtoAnsi(STR0387) + CRLF 	//"O campo DtPrev.Pgto n�o poder estar vazio para convo��es posteriores � data do parametro MV_DTCGINI"

	If cEFDAviso == "0"
		cMsg+= OemtoAnsi(STR0284) + CRLF + CRLF + cMsg2		//"mas nao sera impeditivo para a gravacao dos dados deste funcionario."
	ElseIf cEFDAviso == "1"
		cMsg+= OemtoAnsi(STR0285)+ CRLF + CRLF + cMsg2		//"e sera necessario o preenchimento dos mesmos para efetivar a gravacao dos dados deste funcionario."
		lRet := .F.
	EndIf
	
	Help( ,, OemToAnsi(STR0245),, OemToAnsi(cMsg), 1, 0 )
	
EndIf
/*
If lIntTAF
	If Empty(oGrid:GetValue("V7_TURNO")) .And. Empty(oGrid:GetValue("V7_DESCJOR"))
		lRet := .F.
		Help( ,, OemToAnsi(STR0245),, OemToAnsi(STR0396), 1, 0 )//"Aten��o"##"O Turno e a Descri��o da Jornada n�o foram preenchidos. � obrigat�rio o preenchimento de pelo menos um dos dois campos."
	EndIf
	
	If !Empty(oGrid:GetValue("V7_TURNO")) .And. (Empty(oGrid:GetValue("V7_REGRA")) .Or. Empty(oGrid:GetValue("V7_SEQTURN")))
		lRet := .F.
		Help( ,, OemToAnsi(STR0245),, OemToAnsi(STR0395), 1, 0 )//"Aten��o"##"O Turno foi preenchido mas a Regra e/ou Sequ�ncia In�cio do Turno n�o foram preenchidos."
	EndIf
	
EndIf
*/

Return lRet



/*/{Protheus.doc} PreLineOk
 Pre-validacoes
 
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
Static Function PreLineOk(oGrid, nLine, cAction, cField)
Local lRet 			:= .T.
Local aPerAtual		:= {}
Local cDataFimAnt	:= ""
Local dDataFim		:= CtoD("")
Local dDataIni		:= CtoD("")
Local dDataFimAnt	:= CtoD("")
Local oModel 		:= oGrid:GetModel()

If cAction == "CANSETVALUE" .or. cAction == "DELETE"
	If Gpea1809 (oGrid:GetValue("V7_COD"))
	 	lRet := .F.
	 	If cAction == "DELETE"
	 	//Mvc exige msg
	 		Help("",1,"GPEA1809DV7")//"J� existem lan�amentos para este Funcion�rio!"
		ElseIf cField == "V7_DTFIM"
			MsgInfo(Ap5GetHelp("GPEA1809DV7"))
		EndIf
	EndIf
	If lRet .and. cAction == "CANSETVALUE" .and. cField == "V7_DTFIM"
		If Gpea1808 (oGrid:GetValue("V7_COD"),.T.)  
		 	lRet := .F.
	 		MsgInfo(Ap5GetHelp("GPEA1808DV7"))//"J� foi efetuado o c�lculo de Folha (Tabelas SRD\SRC)"
		EndIf	
	ElseIf lRet .And. Gpea1808 (oGrid:GetValue("V7_COD"))  
	 	lRet := .F.
	 	If cAction == "DELETE" //Mvc exige msg
	 		Help("",1,"GPEA1808DV7")//"J� foi efetuado o c�lculo de Folha (Tabelas SRD\SRC)"
		EndIf
	EndIf	
EndIf

If cAction == "SETVALUE" .and. cField == "V7_DTFIM" .and. !oGrid:IsInserted()
	dDataFim := M->V7_DTFIM
	dDataIni := oGrid:GetValue("V7_DTINI")
	If fGetPerAtual(@aPerAtual,xFilial("RCH"),SRA->RA_PROCES,fGetRotOrdinar())
		If dDataIni < aPerAtual[1,6] //Se convoca��o foi inclu�da no m�s atual, n�o impede a edi��o dos campos. 
			dDataFimAnt := aPerAtual[1,6] - 1
			cDataFimAnt := DtoC(dDataFimAnt)
			If dDataFimAnt <> dDataFim
				lRet := .F.
			EndIf
		EndIf
	Else
		lRet := .F.
	EndIf
	If !lRet
		//"Aten��o" - "A data final s� pode ser alterada para o �ltimo dia do �ltimo per�odo fechado." ### Altere a data para
		Help( ,, OemToAnsi(STR0245),, STR0419, 1, 0,,,,,, If(!Empty(cDataFimAnt),{STR0420 + cDataFimAnt},"") )
	EndIf
EndIf

Return lRet

/*/{Protheus.doc} Gpea1801
 Inic.Padr�o descricao de func
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/                               
function Gpea1801 ()
Local aArea         := GetArea()
Local cRet	        := ""
Local oModel		:= FwModelActivate()
Local oModSV7		:= Nil
Local nTamSV7       := 0 
oModSV7 := oModel:GetModel("SV7MdGrid")

cret := FDESC('SRJ',SV7->V7_FUNC,'RJ_DESC',TAMSX3('RJ_DESC'),SV7->V7_FILIAL)

If oModSV7 <> Nil
	If oModSV7:Length() > 0 
		If Empty(SV7->V7_MAT) .And. Empty(SV7->V7_COD) .And. !Empty(oModSV7:GetValue("V7_COD")) .And. Empty(oModSV7:GetValue("V7_FILIAL")) .And. Empty(oModSV7:GetValue("V7_MAT"))
			cRet := ''
		EndIf
		
		If !Empty(SV7->V7_MAT) .And. !Empty(SV7->V7_COD) .And. !Empty(oModSV7:GetValue("V7_COD")) .And. !Empty(oModSV7:GetValue("V7_FILIAL")) .And. !Empty(oModSV7:GetValue("V7_MAT")) 
			cRet := ''
		EndIf
		
		If !Empty(SV7->V7_MAT) .And. !Empty(SV7->V7_COD) .And. !Empty(oModSV7:GetValue("V7_COD")) .And. Empty(oModSV7:GetValue("V7_FILIAL")) .And. Empty(oModSV7:GetValue("V7_MAT"))
			cRet := ''
		EndIf
		
	EndIf
Else
	cRet := ''
EndIf

RestArea(aArea)
return cRet


/*/{Protheus.doc} Gpea1802
 Inic.Padr�o descricao de cargo
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
function Gpea1802 () 
Local aArea         := GetArea()
Local cRet	        := ""
Local oModel		:= FwModelActivate()
Local oModSV7		:= Nil
Local nTamSV7       := 0 
oModSV7 := oModel:GetModel("SV7MdGrid")

cRet := FDESC("SQ3",SV7->V7_CARG+SV7->V7_CCUS,"Q3_DESCSUM",,SV7->V7_FILIAL)
If Empty(cret)
	cRet := FDESC("SQ3",SV7->V7_CARG,"Q3_DESCSUM",,SV7->V7_FILIAL)
Endif

If oModSV7 <> Nil
	If oModSV7:Length() > 0 
		If Empty(SV7->V7_MAT) .And. Empty(SV7->V7_COD) .And. !Empty(oModSV7:GetValue("V7_COD")) .And. Empty(oModSV7:GetValue("V7_FILIAL")) .And. Empty(oModSV7:GetValue("V7_MAT"))
			cRet := ''
		EndIf
		
		If !Empty(SV7->V7_MAT) .And. !Empty(SV7->V7_COD) .And. !Empty(oModSV7:GetValue("V7_COD")) .And. !Empty(oModSV7:GetValue("V7_FILIAL")) .And. !Empty(oModSV7:GetValue("V7_MAT")) 
			cRet := ''
		EndIf
		
		If !Empty(SV7->V7_MAT) .And. !Empty(SV7->V7_COD) .And. !Empty(oModSV7:GetValue("V7_COD")) .And. Empty(oModSV7:GetValue("V7_FILIAL")) .And. Empty(oModSV7:GetValue("V7_MAT"))
			cRet := ''
		EndIf
	EndIf
Else
	cRet := ''
EndIf

RestArea(aArea)
return cRet


/*/{Protheus.doc} Gpea1803
 Inic.Padr�o descricao de c.custo
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
function Gpea1803 ()                                         
Local aArea         := GetArea()
Local cRet	        := ""
Local oModel		:= FwModelActivate()
Local oModSV7		:= Nil
Local nTamSV7       := 0 
oModSV7 := oModel:GetModel("SV7MdGrid")

cRet := FDESC("CTT",SV7->V7_CCUS,"CTT_DESC01",,SV7->V7_FILIAL)

If oModSV7 <> Nil
	If oModSV7:Length() > 0 
		If Empty(SV7->V7_MAT) .And. Empty(SV7->V7_COD) .And. !Empty(oModSV7:GetValue("V7_COD")) .And. Empty(oModSV7:GetValue("V7_FILIAL")) .And. Empty(oModSV7:GetValue("V7_MAT"))
			cRet := ''
		EndIf
		
		If !Empty(SV7->V7_MAT) .And. !Empty(SV7->V7_COD) .And. !Empty(oModSV7:GetValue("V7_COD")) .And. !Empty(oModSV7:GetValue("V7_FILIAL")) .And. !Empty(oModSV7:GetValue("V7_MAT")) 
			cRet := ''
		EndIf
		
		If !Empty(SV7->V7_MAT) .And. !Empty(SV7->V7_COD) .And. !Empty(oModSV7:GetValue("V7_COD")) .And. Empty(oModSV7:GetValue("V7_FILIAL")) .And. Empty(oModSV7:GetValue("V7_MAT"))
			cRet := ''
		EndIf
	EndIf
Else
	cRet := ''
EndIf

RestArea(aArea)
return cRet


/*/{Protheus.doc} Gpea1804
 Inic.Padr�o descricao de depto
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
function Gpea1804 ()                                                                                                     
Local aArea         := GetArea()
Local cRet	        := ""
Local oModel		:= FwModelActivate()
Local oModSV7		:= Nil
Local nTamSV7       := 0 
oModSV7 := oModel:GetModel("SV7MdGrid")

cRet := FDESC('SQB',SV7->V7_DEPTO,'QB_DESCRIC')

If oModSV7 <> Nil
	If oModSV7:Length() > 0 
		If Empty(SV7->V7_MAT) .And. Empty(SV7->V7_COD) .And. !Empty(oModSV7:GetValue("V7_COD")) .And. Empty(oModSV7:GetValue("V7_FILIAL")) .And. Empty(oModSV7:GetValue("V7_MAT"))
			cRet := ''
		EndIf
		
		If !Empty(SV7->V7_MAT) .And. !Empty(SV7->V7_COD) .And. !Empty(oModSV7:GetValue("V7_COD")) .And. !Empty(oModSV7:GetValue("V7_FILIAL")) .And. !Empty(oModSV7:GetValue("V7_MAT")) 
			cRet := ''
		EndIf
		
		If !Empty(SV7->V7_MAT) .And. !Empty(SV7->V7_COD) .And. !Empty(oModSV7:GetValue("V7_COD")) .And. Empty(oModSV7:GetValue("V7_FILIAL")) .And. Empty(oModSV7:GetValue("V7_MAT"))
			cRet := ''
		EndIf
	EndIf
Else
	cRet := ''
EndIf

RestArea(aArea)
return cRet


/*/{Protheus.doc} Gpea1805
 Inic.Padr�o descricao de Local
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
function Gpea1805 ()                                                                                                     
Local aArea         := GetArea()
Local cRet	        := ""
Local oModel		:= FwModelActivate()
Local oModSV7		:= Nil
Local nTamSV7       := 0 
oModSV7 := oModel:GetModel("SV7MdGrid")

cRet := Posicione("SV6", 1,fwxfilial('SV6')+SV7->V7_LOCAL, "V6_DESC") 

If oModSV7 <> Nil
	If oModSV7:Length() > 0 
		If Empty(SV7->V7_MAT) .And. Empty(SV7->V7_COD) .And. !Empty(oModSV7:GetValue("V7_COD")) .And. Empty(oModSV7:GetValue("V7_FILIAL")) .And. Empty(oModSV7:GetValue("V7_MAT"))
			cRet := ''
		EndIf
		
		If !Empty(SV7->V7_MAT) .And. !Empty(SV7->V7_COD) .And. !Empty(oModSV7:GetValue("V7_COD")) .And. !Empty(oModSV7:GetValue("V7_FILIAL")) .And. !Empty(oModSV7:GetValue("V7_MAT")) 
			cRet := ''
		EndIf
		
		If !Empty(SV7->V7_MAT) .And. !Empty(SV7->V7_COD) .And. !Empty(oModSV7:GetValue("V7_COD")) .And. Empty(oModSV7:GetValue("V7_FILIAL")) .And. Empty(oModSV7:GetValue("V7_MAT"))
			cRet := ''
		EndIf
	EndIf
Else
	cRet := ''
EndIf

RestArea(aArea)
return cRet


/*/{Protheus.doc} Gpea1806
 Inic.Padr�o descricao de Turno
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
function Gpea1806 ()                                                                                                     
Local aArea         := GetArea()
Local cRet	        := ""
Local oModel		:= FwModelActivate()
Local oModSV7		:= Nil
Local nTamSV7       := 0 
oModSV7 := oModel:GetModel("SV7MdGrid")

cRet :=  FDESC("SR6",SV7->V7_TURNO,"R6_DESC")

If oModSV7 <> Nil
	If oModSV7:Length() > 0 
		If Empty(SV7->V7_MAT) .And. Empty(SV7->V7_COD) .And. !Empty(oModSV7:GetValue("V7_COD")) .And. Empty(oModSV7:GetValue("V7_FILIAL")) .And. Empty(oModSV7:GetValue("V7_MAT"))
			cRet := ''
		EndIf
		
		If !Empty(SV7->V7_MAT) .And. !Empty(SV7->V7_COD) .And. !Empty(oModSV7:GetValue("V7_COD")) .And. !Empty(oModSV7:GetValue("V7_FILIAL")) .And. !Empty(oModSV7:GetValue("V7_MAT")) 
			cRet := ''
		EndIf
		
		If !Empty(SV7->V7_MAT) .And. !Empty(SV7->V7_COD) .And. !Empty(oModSV7:GetValue("V7_COD")) .And. Empty(oModSV7:GetValue("V7_FILIAL")) .And. Empty(oModSV7:GetValue("V7_MAT"))
			cRet := ''
		EndIf
	EndIf
Else
	cRet := ''
EndIf

RestArea(aArea)
return cRet

/*/{Protheus.doc} Gpea1807
 Habilitar campo cod local em tela
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
function Gpea1807 () 
Local aArea         := GetArea()
Local cRet	        := ""
Local oModel		:= FwModelActivate()
Local oModSV7		:= Nil
Local lWhenRet      := .F.
Local nTamSV7       := 0 
Local cTipo         := '' 
oModSV7 := oModel:GetModel("SV7MdGrid")

If oModSV7 <> Nil
	cTipo := oModSV7:GetValue("V7_TPLOC")

	If cTipo == "1"
		lWhenRet := .T.	
	EndIf
EndIf

RestArea(aArea)

return lWhenRet



/*/{Protheus.doc} Gpea1808
 Verifica se ja foi gerado SRD\SRC
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
Static function Gpea1808 (cConvoc, lOnlySRC) 
Local cSRDSRC   := GetNextAlias()
Local lHaSrdSrc := .F.

DEFAULT lOnlySRC := .F.

If !lOnlySRC 
	BeginSql Alias cSRDSRC
				SELECT  SRD.RD_CONVOC   FROM 	%table:SRD% SRD 
					   WHERE SRD.RD_FILIAL     = %Exp:(SRA->RA_FILIAL)%
						 AND SRD.RD_MAT        = %Exp:(SRA->RA_MAT)%
						 AND SRD.RD_CONVOC     = %Exp:(cConvoc)%
						 AND SRD.%NotDel%  
	EndSql
			
	If (cSRDSRC)->(!Eof())
		lHaSrdSrc := .T.
	EndIf
	
	(cSRDSRC)->(DbCloseArea())
EndIf

If !lHaSrdSrc
	BeginSql Alias cSRDSRC
				SELECT  SRC.RC_CONVOC   FROM 	%table:SRC% SRC 
					   WHERE SRC.RC_FILIAL     = %Exp:(SRA->RA_FILIAL)%
						 AND SRC.RC_MAT        = %Exp:(SRA->RA_MAT)%
						 AND SRC.RC_CONVOC     = %Exp:(cConvoc)%
						 AND SRC.%NotDel%  
	EndSql
			
	If (cSRDSRC)->(!Eof())
		lHaSrdSrc := .T.
	EndIf
	
	(cSRDSRC)->(DbCloseArea())
EndIf

return lHaSrdSrc


/*/{Protheus.doc} Gpea1809
 Verifica se ja foi gerado rgb
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
Static function Gpea1809 (cConvoc) 
Local cRGBAlias   := GetNextAlias()
Local lRGB := .F.
 
BeginSql Alias cRGBAlias
			SELECT  RGB.RGB_CONVOC   FROM 	%table:RGB% RGB 
				   WHERE RGB.RGB_FILIAL     = %Exp:(SRA->RA_FILIAL)%
					 AND RGB.RGB_MAT        = %Exp:(SRA->RA_MAT)%
					 AND RGB.RGB_CONVOC     = %Exp:(cConvoc)%
					 AND RGB.%NotDel%  
EndSql
		
If (cRGBAlias)->(!Eof())
	lRGB := .T.
EndIf

(cRGBAlias)->(DbCloseArea())

return lRGB




/*/{Protheus.doc} Gpea1810
 Horas Dia 
@author Oswaldo L
@since 12/12/2017
@version P12
@param cTipoAlt, caractere
@return lResult, resultado
/*/
function Gpea1810 () 
Local aArea         := GetArea()
Local cRet	        := ""
Local oModel		:= FwModelActivate()
Local oModSV7		:= Nil
Local lRet          := .T.
Local nHrsDia       := 0 
 
oModSV7 := oModel:GetModel("SV7MdGrid")

If oModSV7 <> Nil
	nHrsDia := oModSV7:GetValue("V7_HRSDIA")

	If nHrsDia > 24.00
		lRet := .F.	
	EndIf
EndIf

RestArea(aArea)

return lRet

/*/{Protheus.doc} F3SV7Filtro
Constroi o filtro da consulta padr�o de convica��es utilizada nos lan�amentos mensais - RGB
@author cicero.pereira
@since 15/12/2017
@version 12.1.17
/*/
Function F3SV7Filtro()

	Local cFiltro := ""

	If Type("cDataIni")  == "U"
		Private cDataIni := RCH->RCH_DTINI
		Private cDataFim := RCH->RCH_DTFIM
	Endif
	
	cFiltro := "SV7->V7_FILIAL == SRA->RA_FILIAL "
	cFiltro += ".And. SV7->V7_MAT == SRA->RA_MAT"
	cFiltro += ".And. SV7->V7_STAT == '1' .And. (dTos(SV7->V7_DTINI) <= '" + dTos(cDataIni) + "'"
	cFiltro += ".And. dTos(SV7->V7_DTFIM)  >= '"  +  dTos(cDataIni) + "')"
	cFiltro += ".Or. ( dTos(SV7->V7_DTINI) >= '"  +  dTos(cDataIni) + "' "
	cFiltro += ".And. dTos(SV7->V7_DTINI)  <= '"  +  dTos(cDataIni) + "')"
	
	cFiltro := "@#" + cFiltro + "@#"

Return cFiltro


/*/{Protheus.doc} F3SV7SQ3
Constroi o filtro da consulta padr�o de cargos
@author cicero.pereira
@since 15/12/2017
@version 12.1.17
/*/

Function F3SV7SQ3()
Local cFiltro := ""
Local oModel		:= FwModelActivate()
Local oModSV7		:= Nil
  
oModSV7 := oModel:GetModel("SV7MdGrid")

cFiltro :=   "@#" + " SQ3->Q3_CC == '" + oModSV7:GetValue('V7_CCUS') + "' .or. SQ3->Q3_CC == Space(TAMSX3('Q3_CC')[1]) " + "@#" 

RETURN cFiltro 

/*/{Protheus.doc} FVldDPrvPg
Valid do campo V7_DPREVPG
Deve ser uma data v�lida e igual ou posterior � data de admiss�o do trabalhador 
(no caso de sucess�o, igual ou posterior � data da transfer�ncia). 
@author paulo.inzonha
@since 29/03/2018
@version 12.1.17
/*/

Function FVldDPrvPg()
Local oModel		:= FwModelActivate()
Local oModSV7		:= Nil
Local dPrevPgto		:= StoD("")
Local lRet			:= .T.
Local aTrasf		:= {}
Local nPos			:= 0

oModSV7 	:= oModel:GetModel("SV7MdGrid")
dPrevPgto 	:= oModSV7:GetValue('V7_DPREVPG')

If !Empty(dPrevPgto)
	fTransf(@aTrasf,,.T.,.T.)
	nPos := aScan(aTrasf, {|X| X[5] == SRA->RA_FILIAL + SRA->RA_MAT })
	If SRA->RA_ADMISSA > dPrevPgto .OR. ( nPos > 0 .AND.aTrasf[nPos,7] > dPrevPgto )
		//"Aten��o" - "Data de previs�o de pagamento menor que data de Admiss�o/Transfer�ncia"
		Help( ,, OemToAnsi(STR0245),, OemToAnsi(STR0386 ), 1, 0 )
		lRet := .F.
	EndIf  
EndIf

RETURN lRet 

/*/{Protheus.doc} fVldModel
Valida��o da abertura do Model
@author allyson.mesashi
@since 06/08/2018
@version 12.1.17
@return lRet  - Indica se o model poder� ser aberto
/*/
Static Function fVldModel()

Local cCPF 		:= AllTrim(SRA->RA_CIC) + ";" + ALLTRIM(SRA->RA_CODUNIC)
Local cStat2100	:= ""
Local cStat2200	:= ""
Local lRet 		:= .T.

If lIntTaf
	cStat2100	:= TAFGetStat( "S-2100", cCPF )
	cStat2200	:= TAFGetStat( "S-2200", cCPF )

	If cStat2100  == "-1" .And. cStat2200 == "-1"
		Help( ,, OemToAnsi(STR0245),, OemToAnsi(STR0398), 1, 0 )//"Aten��o"##"N�o ser� poss�vel efetuar o cadastro pois o registro de Admiss�o ou Carga Inicial deste funcion�rio ainda n�o foi efetivado no TAF"
		lRet := .F.
	EndIf


	If lRet .And. !lDPrevPG
		Help( ,, OemToAnsi(STR0245),, OemToAnsi(STR0405 + CRLF + STR0400), 1, 0 )//"Aten��o"##"N�o ser� poss�vel efetuar o cadastro pois o dicion�rio de dados n�o est� atualizado. A exist�ncia do campo V7_DPREVPG � obrigat�ria."                 
		lRet := .F.
	EndIf
EndIf

Return lRet

/*/{Protheus.doc} fWhenConv
Fun��o que habilita edi��o do campo V7_CONVC
@author allyson.mesashi
@since 06/08/2018
@version 12.1.17
@param oModel	- Objeto do model
@return lRet  	- Indica o status do registro no TAF
/*/
Static Function fWhenConv(oModel)
	Local lRet 		:= .T.

	If !oModel:isInserted() .And. TAFGetStat( "S-2260", AllTrim(SRA->RA_CIC) + ALLTRIM(SRA->RA_CODUNIC) + ";" + oModel:GetValue("V7_CONVC") ) != "-1"
		lRet := .F.
	EndIF

Return lRet  

/*/{Protheus.doc} fWhenDeJo
Fun��o que habilita edi��o do campo V7_DESCJOR
@author allyson.mesashi
@since 06/08/2018
@version 12.1.17
@param oModel	- Objeto do model
@return lRet  	- Indica o status do registro no TAF
/*/
Static Function fWhenDJ(oModel)
	Local lRet 		:= .F.

	If Empty(oModel:GetValue("V7_TURNO"))
		lRet := .T.
	EndIF

Return lRet  
