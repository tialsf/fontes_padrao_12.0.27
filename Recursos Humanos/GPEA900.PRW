#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "GPEA900.CH"
#INCLUDE "INKEY.CH"

Static lIncluir:= .F.

/*/
����������������������������������������������������������������������������������
����������������������������������������������������������������������������������
������������������������������������������������������������������������������Ŀ��
���Fun��o    � GPEA900  � Autor � Jonatas A. T. Alves        � Data � 07/12/10 ���
������������������������������������������������������������������������������Ĵ��
���Descri��o � Historico de Contratos                                          ���
������������������������������������������������������������������������������Ĵ��
���Sintaxe   � GPEA900()                                                       ���
������������������������������������������������������������������������������Ĵ��
��� Uso      � Esecifico                                                       ���
������������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.                  ���
������������������������������������������������������������������������������Ŀ��
���Programador � Data     � FNC / Chamado  � Motivo da Alteracao               ���
������������������������������������������������������������������������������Ĵ��
���Mohanad Odeh�13/03/2013�M12RH01 RQ3004  |Unificacao dos fontes V12          ���
���            �          �                |Removida as funcoes AjustaSX1 e Alt���
���Flavio Corr.�02/09/2014�TQMD57          |Retirada fun��o de checagem  campo ���
���Claudinei S.�03/06/2016�TUQEKD          |Conversao do fonte para MVC        ���
���Claudinei S.�08/07/2016�TUP136/TUQEKD   |Merge da 12.1.12                   ���
���C�cero Alves�01/09/2017�DRHPAG-256	   |Incluido tratamento do campo 	   ���
���			   �		  �				   |RGE_CALENC, que define os encargos ���
���            �          �    		 	   |que serao calculados.			   ���
���Eduardo K.  �28/09/2017�MPRIMESP-11588  |Ajuste em fTudoOKA90 para informar ���
���			   �		  �				   |o alias correto no ColumnPos       ���
���            �          �    		 	   |que serao calculados.			   ���
�������������������������������������������������������������������������������ٱ�
����������������������������������������������������������������������������������
���������������������������������������������������������������������������������*/ 
Function GPEA900()
Local aArea			:= GetArea()
Local aCoors		:= FWGetDialogSize( oMainWnd )
Local aCMP			:= {}
Local cIdBrowse
Local cIdGrid
Local cX3Campo		:= ""
Local oPanelUp
Local oTela
Local oPanelDown
Local oRelacRGE
Local oDlgPrinc
Local cFiltraRh  	:= ""

Private oBrowseUp
Private oBrowseDwn

Define MsDialog oDlgPrinc Title OemToAnsi(STR0001) From aCoors[1], aCoors[2] To aCoors[3], aCoors[4] Pixel  //"Historico de Contratos"

// Cria o conteiner onde ser�o colocados os paineis
oTela     := FWFormContainer():New( oDlgPrinc )
cIdBrowse := oTela:CreateHorizontalBox( 60 )
cIdGrid   := oTela:CreateHorizontalBox( 35 )

oTela:Activate( oDlgPrinc, .F. )

//Cria os paineis onde serao colocados os browses
oPanelUp  	:= oTela:GeTPanel( cIdBrowse )
oPanelDown	:= oTela:GeTPanel( cIdGrid )

cFiltraRH	:= ChkRh( "GPEA900" , "SRA" , "1" )
// FWmBrowse Superior: Funcionarios
oBrowseUp:= FWmBrowse():New()
oBrowseUp:SetOwner( oPanelUp )                  	// Aqui se associa o browse ao componente de tela
oBrowseUp:SetDescription( OemToAnsi(STR0018) )	//"Funcionarios"
oBrowseUp:SetAlias( 'SRA' )
oBrowseUp  :SetMenuDef( '' )              			// Define de onde virao os botoes deste browse
oBrowseUp:DisableDetails()
oBrowseUp:SetProfileID( '1' )
oBrowseUp:SetCacheView (.F.) 
oBrowseUp:SetFilterDefault(cFiltraRH)
oBrowseUp:ExecuteFilter(.T.)

GpLegMVC(@oBrowseUp)

oBrowseUp:Activate()
// FWmBrowse Inferior: Cabecalhos do Funcion�rio    
oBrowseDwn:= FWMBrowse():New()
oBrowseDwn:SetOwner( oPanelDown )
oBrowseDwn:SetDescription( OemToAnsi(STR0001) )	//"Hist�rico de Contratos"
oBrowseDwn:SetMenuDef( 'GPEA900' )                       
oBrowseDwn:DisableDetails()
oBrowseDwn:SetAlias( 'RGE' )
oBrowseDwn:SetProfileID( '2' )
oBrowseDwn:ForceQuitButton()					//sempre que existem dois menudefs na tela, deve-se indicar em qual browse vai ficar o botao 'Sair'
oBrowseDwn:SetCacheView (.F.)
oBrowseDwn:ExecuteFilter(.F.)

dbSelectArea("SX3")
SX3->(DbSetOrder(1))
dbSeek("RGE")
	
//--Carrega o array aCmp com os campos que dever�o aparecer no grid
While ! Eof() .and. (X3_ARQUIVO == "RGE")
	cX3Campo := X3_CAMPO
    If !X3USADO(X3_CAMPO) .Or. ALLTRIM(X3_CAMPO) $ "RGE_FILIAL|RGE_MAT|RGE_NOME"
    	SX3->( DBSKIP() )
    	Loop
    EndIf
    aadd(aCmp,cX3Campo)
   	DbSkip()
EndDo
oBrowseDwn:SetOnlyFields(aCmp)
// Relacionamento entre os Paineis
oRelacRGE:= FWBrwRelation():New()
oRelacRGE:AddRelation( oBrowseUp  , oBrowseDwn , { { 'RGE_FILIAL', 'RA_FILIAL' }, { 'RGE_MAT' , 'RA_MAT'  } } )
oRelacRGE:Activate()
oBrowseDwn:Activate()
oBrowseUp:Refresh()
oBrowseDwn:Refresh()

Activate MsDialog oDlgPrinc Center

RestArea(aArea)

Return Nil

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �MenuDef   �Autor  �Leandro Drumond     � Data �  03/10/12   ���
�������������������������������������������������������������������������͹��
���Desc.     �Menu Funcional                                              ���
�������������������������������������������������������������������������͹��
���Uso       �SIGAGPE                                                     ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Static Function MenuDef()
Local aRotina :=  {}

ADD OPTION aRotina TITLE STR0005 ACTION 'PesqBrw'          	OPERATION 1 ACCESS 0 DISABLE MENU //"Pesquisar"
ADD OPTION aRotina TITLE STR0006 ACTION 'VIEWDEF.GPEA900'	OPERATION 2 ACCESS 0 DISABLE MENU //"Visualizar"
ADD OPTION aRotina TITLE STR0012 ACTION 'VIEWDEF.GPEA900' 	OPERATION 4 ACCESS 0 DISABLE MENU //"Modificar"
ADD OPTION aRotina TITLE STR0007 ACTION 'VIEWDEF.GPEA900' 	OPERATION 3 ACCESS 0 DISABLE MENU //"Incluir"
ADD OPTION aRotina TITLE STR0009 ACTION 'VIEWDEF.GPEA900' 	OPERATION 5 ACCESS 0 DISABLE MENU //"Excluir"

Return aRotina

/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Fun��o    �ModelDef    � Autor � Leandro Drumond       � Data � 11/12/13 ���
���������������������������������������������������������������������������Ĵ��
���Descri��o �Modelo de Dados da Rotina                                     ���
���������������������������������������������������������������������������Ĵ��
���Sintaxe   �ModelDef()                                                    ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������/*/

Static Function ModelDef()
Local oModel    	                                      
Local oStruRGE
Local oStruGET
Local bCommiA900	:= { | oModel | fCommit900(oModel) }                         
Local bTudoOK		:= { | oModel | fTudoOKA90(oModel,oModel:GetOperation()) }

oModel := MPFormModel():New( 'GPEA900', /*bPreValid*/, bTudoOK, bCommiA900)

oStruRGE := FWFormStruct(1,"RGE")
oModel:AddFields("GPEA900_RGE", /*cOwner*/, oStruRGE , /*Pre-Validacao*/,/*Pos-Validacao*/,/*Carga*/)

oModel:SetPrimaryKey( {"RGE_FILIAL", "RGE_MAT","R_E_C_D_E_L_"} )

//--Valida se o model deve ser ativado
oModel:SetVldActivate( { |oModel| fVldModel(oModel,oModel:GetOperation()) } )

Return oModel

/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Fun��o    �ViewDef     � Autor � Leandro Drumond       � Data � 11/12/13 ���
���������������������������������������������������������������������������Ĵ��
���Descri��o �View                                                          ���
���������������������������������������������������������������������������Ĵ��
���Sintaxe   �ViewDef()                                                     ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������/*/
Static Function ViewDef()
Local oModel
Local oStructRGE
Local oView

oModel 	:= FwLoadModel("GPEA900")
oView 		:= FWFormView():New()
oView:SetModel(oModel)
oStructRGE := FWFormStruct(2,"RGE")
oView:AddField(  "GPEA900_RGE" , oStructRGE )

If cPaisLoc == "BRA"

	//GRUPOS DA ABA PERIODO DO CONTRATO
	oStructRGE:AddGroup( 'Grupo00', OemToAnsi(STR0019), '', 3 )   	  //'Dados Gerais'
	oStructRGE:SetProperty( "RGE_MAT" 		, MVC_VIEW_GROUP_NUMBER , 'Grupo00' )
	oStructRGE:SetProperty( "RGE_NOME"		, MVC_VIEW_GROUP_NUMBER , 'Grupo00' )
	oStructRGE:SetProperty( "RGE_CIC" 		, MVC_VIEW_GROUP_NUMBER , 'Grupo00' )

	oStructRGE:AddGroup( 'Grupo01', OemToAnsi(STR0020), '', 3 )   	  //'Per�odo do Contrato'
	oStructRGE:SetProperty( "RGE_DATAIN" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo01' )
	oStructRGE:SetProperty( "RGE_DATAFI" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo01' )
	
	//GRUPOS DA ABA RESIDENTE EXTERIOR
	oStructRGE:AddGroup( 'Grupo02', OemToAnsi(STR0021) , '', 3 )	  //'Dados do Contrato'

	If RGE->(ColumnPos( "RGE_RESEXT")) > 0
		oStructRGE:SetProperty( "RGE_RESEXT" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo02' )
	Endif
	
	oStructRGE:SetProperty( "RGE_NOMEMP" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo02' )
	oStructRGE:SetProperty( "RGE_CNPJ" 		, MVC_VIEW_GROUP_NUMBER , 'Grupo02' )	
	oStructRGE:SetProperty( "RGE_CODPAI" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo02' )
	oStructRGE:SetProperty( "RGE_BEDINI" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo02' )
	oStructRGE:SetProperty( "RGE_PAEXNI" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo02' )	
	oStructRGE:SetProperty( "RGE_CODNIF" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo02' )
	// Altera picture do campo RGE_CODNIF para permitir informar letras
	oStructRGE:SetProperty( "RGE_CODNIF"	, MVC_VIEW_PICT			, '@!')
	
	oStructRGE:AddGroup( 'Grupo03', OemToAnsi(STR0022) , '', 3 )   //'Endere�o'
	oStructRGE:SetProperty( "RGE_LOGRAD" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo03' )
	oStructRGE:SetProperty( "RGE_NUMERO" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo03' )
	oStructRGE:SetProperty( "RGE_COMPL"  	, MVC_VIEW_GROUP_NUMBER , 'Grupo03' )
	oStructRGE:SetProperty( "RGE_BAIRRO" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo03' )
	oStructRGE:SetProperty( "RGE_CODPOS" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo03' )
	oStructRGE:SetProperty( "RGE_CIDADE" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo03' )
	oStructRGE:SetProperty( "RGE_ESTPRO" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo03' )
	oStructRGE:SetProperty( "RGE_TELEFO" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo03' )
	
	oStructRGE:AddGroup( 'Grupo04', OemToAnsi(STR0023) , '', 3 )   //'Impostos'
	oStructRGE:SetProperty( "RGE_BENREN" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo04' )	
	oStructRGE:SetProperty( "RGE_TIPREN" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo04' )
	oStructRGE:SetProperty( "RGE_FRMTRB" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo04' )
	oStructRGE:SetProperty( "RGE_CODRET" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo04' )
	oStructRGE:SetProperty( "RGE_DESCIR" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo04' )
	oStructRGE:SetProperty( "RGE_PERCIR" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo04' )
	oStructRGE:SetProperty( "RGE_DEDINS" 	, MVC_VIEW_GROUP_NUMBER , 'Grupo04' )

	If RGE->( ColumnPos( "RGE_CALENC" ) ) > 0
		oStructRGE:SetProperty( "RGE_ENCARG" , MVC_VIEW_GROUP_NUMBER , 'Grupo04' )
		oStructRGE:SetProperty( "RGE_CALENC" , MVC_VIEW_GROUP_NUMBER , 'Grupo04' )
	EndIf
	
Endif

oView:createHorizontalBox("FORMFIELD",100)
oView:SetOwnerView( "GPEA900_RGE","FORMFIELD")

Return(oView)

oView:SetCloseOnOk({ || .T. }) //Fecha tela apos commit

Return oView

/*/
��������������������������������������������������������������������������������
��������������������������������������������������������������������������������
����������������������������������������������������������������������������Ŀ��
���Fun��o    �fVldModel    � Autor � Leandro Drumond       � Data � 11/12/13 ���
����������������������������������������������������������������������������Ĵ��
���Descri��o �Valida model e inicializa cabecalho SRG.                       ���
����������������������������������������������������������������������������Ĵ��
���Sintaxe   �fVldModel(oModel,nOperation)                                   ���
�����������������������������������������������������������������������������ٱ�
��������������������������������������������������������������������������������
��������������������������������������������������������������������������������/*/
Static Function fVldModel(oModel,nOperation)

Local lRet := .T.

Return lRet

/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Fun��o    �fCommit900  � Autor � Leandro Drumond       � Data � 11/12/13 ���
���������������������������������������������������������������������������Ĵ��
���Descri��o �Funcao que comita as informacoes, SEM abrir outra inclusao au-���
���          �tomaticamente.                                                ���
���������������������������������������������������������������������������Ĵ��
���Sintaxe   �fCommit900(oModel)                                            ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������/*/ 
Static Function fCommit900(oModel)

FWFormCommit( oModel ) 					//--Grava model
oBrowseUp:Refresh()
oBrowseDwn:Refresh()

Return .T.

/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Fun��o    �fTudoOKA90  � Autor � Leandro Drumond       � Data � 11/12/13 ���
���������������������������������������������������������������������������Ĵ��
���Descri��o �Validacoes acionadas ao clicar no botao confirma.             ���
���������������������������������������������������������������������������Ĵ��
���Sintaxe   �fTudoOKA90()                                                  ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������/*/
Static Function fTudoOKA90(oModel,nOperation)
Local aArea     	:= GetArea()
Local lRet			:= .T. 
Local oModelRGE 	:= oModel:GetModel('GPEA900_RGE')
Local aContrato	:= {}
Local nC			:= 0
Local lResidente	:= .F.
Local dCtIni		:= CtoD("")	// Data de Inicio do Contrato
Local dCtFim		:= CtoD("")	// Data de Termino do Contato
Local cCodNIF		:= ""			// NIF (Numero de Identificacao Fiscal)
Local cNomeEmp	:= ""			// Raz�o social
Local cCodPai		:= ""			// Pais do funcionario residente.
Local cBedini		:= ""			// Funcionario esta dispensado do NIF
Local cPaisNIF	:= ""			// Pais do funcionario exige o NIF
Local cTipRen		:= ""			// Tipo de Rendimento 
Local cFrmTrib	:= ""			// Forma de Tributacao
Local cCodReten	:= ""			// Codigo de Retencao

FWModelActive( oModel ) 

dCtIni 	:= oModelRGE:GetValue("RGE_DATAIN")
dCtFim 	:= oModelRGE:GetValue("RGE_DATAFI")
cCodNIF 	:= oModelRGE:GetValue("RGE_CODNIF")
If RGE->(ColumnPos( "RGE_RESEXT")) > 0
	lResidente	:= oModelRGE:GetValue("RGE_RESEXT") = "1"
Else
	lResidente := .T.
Endif
//Campos que se tornam obrigat�rios caso o funcion�rio resida no exterior
cNomeEmp	:= oModelRGE:GetValue("RGE_NOMEMP")
cCodPai	:= oModelRGE:GetValue("RGE_CODPAI")
cBedini	:= oModelRGE:GetValue("RGE_BEDINI")
cPaisNIF	:= oModelRGE:GetValue("RGE_PAEXNI")
cTipRen	:= oModelRGE:GetValue("RGE_TIPREN")
cFrmTrib	:= oModelRGE:GetValue("RGE_FRMTRB")
cCodReten	:= oModelRGE:GetValue("RGE_CODRET")

If nOperation == MODEL_OPERATION_INSERT
	dbSelectArea( "RGE" )
	RGE->(DbSetOrder(2))
	dbSeek(SRA->RA_FILIAL+SRA->RA_MAT)
	While !Eof() .And. RGE->RGE_FILIAL+RGE->RGE_MAT == SRA->RA_FILIAL+SRA->RA_MAT
		aAdd(aContrato, { RGE->RGE_DATAIN, RGE->RGE_DATAFI })
		dbSkip()
	EndDo
	RestArea( aArea )
	
	If !Empty(dCtFim)
		For nC := 1 TO LEN( aContrato )
			If Empty(aContrato[nC,2])
				If dCtIni >= aContrato[nC,1]
					Help( " ", 1, "GPA900DT" )
					lRet := .F.
				Endif
			ElseIf (dCtIni >= aContrato[nC,1] .And. aContrato[nC,2] >= dCtIni );
			.Or. (dCtIni <= aContrato[nC,1] .And. dCtFim >= aContrato[nC,1] )
				Help( " ", 1, "GPA900DT" )
				lRet := .F.
			Endif
		Next nC
	Else
		For nC := 1 TO LEN( aContrato )
			If Empty(aContrato[nC,2])
				If dCtIni >= aContrato[nC,1] .Or. dCtFim >= aContrato[nC,1]
					Help( " ", 1, "GPA900DT" )
					lRet := .F.
				Endif
			ElseIf (dCtIni >= aContrato[nC,1] .And. aContrato[nC,2] >= dCtIni );
			.Or. (dCtIni <= aContrato[nC,1] .Or. dCtIni <= aContrato[nC,2])
				Help( " ", 1, "GPA900DT" )
				lRet := .F.
			Endif
		Next nC
	Endif
Endif

//Se for inclus�o ou altera��o, e o funcionaio for residente no exterior	
If (nOperation == MODEL_OPERATION_INSERT .Or. nOperation == MODEL_OPERATION_UPDATE) .And. lResidente
	//Verifica o preenchimento do NIF
	lRet := If( ( cBedini == "1" .And. Empty(cCodNIF) ) .Or. ( cPaisNIF == "1" .And. cBedini == "1" .And. Empty(cCodNIF) ) .Or. ( cPaisNIF == "1" .And. cBedini == "2" .And. !Empty(cCodNIF) ) .Or. ( cPaisNIF == "2" .And. Empty(cCodNIF) ), .T., .F.  )
	If !lRet
		Help( " ", 1, "GPA900NIF" )
		lRet := .F.
	EndIf
	//Verifica o preenchimento dos campos que s�o obrigat�rios para residentes no exterior 			
	If Empty(cNomeEmp) .Or. Empty(cCodPai) .Or. Empty(cBedini) .Or. Empty(cPaisNIF) .Or. Empty(cTipRen) .Or. Empty(cFrmTrib) .Or. Empty(cCodReten)
		Help( " ", 1, "GPM900OBR" )
		lRet := .F.
	Endif
Endif

Return(lRet)

/*/
�����������������������������������������������������������������������Ŀ
�Fun��o    �Gp900Nome 	  �Autor� Jonatas A. T. Alves � Data �02/12/2010�
�����������������������������������������������������������������������Ĵ
�Descri��o �Retorna o nome completo do funcionario                      �
�����������������������������������������������������������������������Ĵ
�Sintaxe   �<vide parametros formais>     								�
�����������������������������������������������������������������������Ĵ
�Uso       �GPEA900                                                  	�
�����������������������������������������������������������������������Ĵ
�Retorno   �cNome  														�
�����������������������������������������������������������������������Ĵ
�Parametros�<vide parametros formais>									�
�������������������������������������������������������������������������/*/
Function Gpa900Nome()

Local cNome := ""

If SRA->( FieldPos( "RA_NOMECMP" ) ) > 0 .And. !Empty( SRA->RA_NOMECMP )
	cNome := SRA->RA_NOMECMP
Else
	cNome := SRA->RA_NOME
EndIf

Return( cNome )

Function ExistCCH()
Local lRet := .T.
Local cAliasQry := GetNextAlias()
Local cWhere 	:= "% AND SUBSTRING(CCH.CCH_CODIGO,2,3) = '" + M->RGE_CODPAI +"'%"
If ( TCGETDB() $ 'DB2|ORACLE|POSTGRES|INFORMIX' )
	cWhere := STRTRAN(cWhere, "SUBSTRING", "SUBSTR")
EndIf

BeginSql alias cAliasQry
	SELECT CCH_CODIGO
	FROM 		%table:CCH% CCH
	WHERE 		CCH.%NotDel% 
	 			%exp:cWhere%
				AND CCH.CCH_FILIAL = %exp:xFilial("CCH")%
				
EndSql
If ( (cAliasQry)->(Eof()) )		
	lRet := .F.
	Help( " ", 1, "Help",, STR0011, 1, 0 )
EndIf

(cAliasQry)->(dbCloseArea())

Return lRet


/*/{Protheus.doc} Gp900CodEnc
Fun��o que retorna as op��es da consulta padr�o do campo RGE_CALENC
@author C�cero ALves
@since 01/09/2017
@version 1.0
@param l1Elem, logical, Vari�vel de controle da execu��o da consulta padr�o
@return lRet, lpgical, Retorna se montou a tela de sele��o de op��es
/*/
Function Gp900CodEnc(l1Elem)

	Local aSit		:= {}
	Local cMvPar
	Local cMvParDef	:= "123456789ABCDEFGHIJ"
	Local lRet		:= .T.

	l1Elem	:= IF( l1Elem == NIL .and. ValType( l1Elem ) != "L", .T., .F. )

	Aadd(aSit, "1" + "-" + STR0024)		//"Empresa"
	Aadd(aSit, "2" + "-" + STR0025)		//"Acid.Trab."
	Aadd(aSit, "3" + "-" + STR0042)		//"FGTS"
	Aadd(aSit, "4" + "-" + STR0026)		//"Terceiros"
	Aadd(aSit, "5" + "-" + STR0027)		//"Sal.Educ."
	Aadd(aSit, "6" + "-" + STR0028)		//"INCRA"
	Aadd(aSit, "7" + "-" + STR0029)		//"SENAI"
	Aadd(aSit, "8" + "-" + STR0030)		//"SESI"
	Aadd(aSit, "9" + "-" + STR0031)		//"SENAC"
	Aadd(aSit, "A" + "-" + STR0032)		//"SESC"
	Aadd(aSit, "B" + "-" + STR0033)		//"SEBRAE"
	Aadd(aSit, "C" + "-" + STR0034)		//"DPC"
	Aadd(aSit, "D" + "-" + STR0035)		//"F.AER."
	Aadd(aSit, "E" + "-" + STR0036)		//"SENAR"
	Aadd(aSit, "F" + "-" + STR0037)		//"SECONC"
	Aadd(aSit, "G" + "-" + STR0038)		//"SEST"
	Aadd(aSit, "H" + "-" + STR0039)		//"SENAT"
	Aadd(aSit, "I" + "-" + STR0040)		//"SESCOOP"
	Aadd(aSit, "J" + "-" + STR0041)		//"Adic.SENAI"

	If f_Opcoes(@cMvPar, "", aSit, cMvParDef, 12, 49, l1Elem, 1, Nil, Nil)
		VAR_IXB	:= cMvPar
		lRet 	:= .T.
	Else
		lRet := .F.
	EndIf

Return(lRet)

/*/{Protheus.doc} GP900Valid
Fun��o de valida��o do preenchimento do campo RGE_CALENC
@author C�cero ALves
@since 01/09/2017
@version 1.0
@Return lRet, logical, Retorna se o preenchimento est� correto
/*/
Function GP900Valid()

	Local cMvPar	:= &(Alltrim(ReadVar()))
	Local cMvParDef	:= "123456789ABCDEFGHIJ"
	Local cConteudo	:= If(Valtype(cMvPar) == Nil, "", cMvPar)
	Local lRet		:= .T.
	Local nTamLen	:= TamSX3("RGE_CALENC")[1]
	Local nX		:= 0

	cConteudo := SubStr( cConteudo, 1, nTamLen )
	cConteudo := StrTran( StrTran(cConteudo, " ", "*"), "*", "" )

	Begin Sequence

		//-- VERIFICA O RETORNO
		If (cMvPar != Nil)
			For nX := 1 To Len(cConteudo)
				If ! (SubStr(cConteudo, nX, 1) $ cMvParDef)
					lRet := .F.
					Break
				EndIf
			Next nX
		EndIf

	End Sequence

Return(lRet)
