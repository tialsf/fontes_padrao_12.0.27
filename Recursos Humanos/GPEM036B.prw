#INCLUDE "PROTHEUS.CH"
#INCLUDE "GPEM036.CH"

Static cVersEnvio:= "2.4"

Static __oSt05_1	 	//Query para filtrar as verbas de folha que foram pagas de funcion�rios ativos
Static __oSt05_2	 	//Query para filtrar as verbas de folha que foram pagas de funcion�rios demitidos
Static __oSt06		 	//Query para filtrar roteiro 132 anterior a dezembro
Static __oSt09		 	//Query para filtrar as verbas calculadas no diss�dio
Static __oSt10		 	//Query para verificar os lan�amentos de m�ltiplos v�nculos
Static __oSt11		 	//Query para verificar se houve c�lculo de f�rias com pagamento no per�odo
Static __oSt12		 	//Query para filtrar as verbas de f�rias calculadas com pagamento no per�odo
Static __oSt13		 	//Query para filtrar os registros da C9V do CPF que est� em processamento
Static __oSt17		 	//Query para verificar quantidade de matriculas no periodo
Static __oSt18		 	//Query para verificar f�rias pagas em per�odo anterior, de matr�culas transferidas
Static __oSt19		 	//Query para verificar registros destino da SRH/SRR de matr�culas transferidas
Static aTabS073		:= {}	//Tabela S073
Static oHash
Static lIntTAF		:= ((SuperGetMv("MV_RHTAF",, .F.) == .T.) .And. Val(SuperGetMv("MV_FASESOC",/*lHelp*/,' ')) >= 1 )
Static lMiddleware	:= If( cPaisLoc == 'BRA' .And. Findfunction("fVerMW"), fVerMW(), .F. )

/*/{Protheus.doc} GPEM036B
@Author   Alessandro Santos
@Since    18/03/2019
@Version  1.0
@Obs      Migrado do GPEM036 em 15/04/2019 para gerar o evento S-1211
/*/
Function GPEM036B()
Return()

/*
�����������������������������������������������������������������������Ŀ
�Fun��o    � fNew1210()     �Autor�  Marcia Moura     � Data �02/06/2017�
�����������������������������������������������������������������������Ĵ
�Descri��o �Gera o registro de Folha / Pagamentos                     �
�����������������������������������������������������������������������Ĵ
�Sintaxe   �< Vide Parametros Formais >									�
�����������������������������������������������������������������������Ĵ
� Uso      �GPEM034                                                     �
�����������������������������������������������������������������������Ĵ
� Retorno  �Nil															�
�����������������������������������������������������������������������Ĵ
�Parametros�Nil															�
������������������������������������������������������������������������� */

Function fNew1210(cCompete, cPerIni, cPerFim, aArrayFil, lRetific, lIndic13, aLogsOk, cOpcTab, aCheck, cCPFDe, cCPFAte, lExcLote, cExpFiltro, aLogsErr, aLogsPrc, lRelat)

Local lRet 		:= .T.
Local aAreaSM0	:= SM0->(GetArea())

Private lRobo	:= IsBlind()

If lRobo
	Processa({|lEnd| lRet := NewProc1210(cCompete, cPerIni, cPerFim, aArrayFil, lRetific, lIndic13, aLogsOk, cOpcTab, aCheck, cCPFDe, cCPFAte, lExcLote, cExpFiltro, .F., aLogsErr, aLogsPrc, lRelat)})
Else
	Proc2BarGauge({|lEnd| lRet := NewProc1210(cCompete, cPerIni, cPerFim, aArrayFil, lRetific, lIndic13, aLogsOk, cOpcTab, aCheck, cCPFDe, cCPFAte, lExcLote, cExpFiltro, .T., aLogsErr, aLogsPrc, lRelat)}, "Evento S-1210", NIL , NIL , .T. , .T. , .F. , .F. )
EndIf

__oSt05_1 	:= Nil
__oSt05_2 	:= Nil
__oSt06 	:= Nil
__oSt09 	:= Nil
__oSt10 	:= Nil
__oSt11 	:= Nil
__oSt12 	:= Nil
__oSt13 	:= Nil
__oSt17 	:= Nil
__oSt18 	:= Nil
__oSt19 	:= Nil
aTabS073 	:= Nil
If ValType(oHash) == "O"
	HMClean(oHash)
	FreeObj(oHash)
	oHash := Nil
EndIf

RestArea(aAreaSM0)

Return lRet

/*/{Protheus.doc} NewProc1210
Processamento das rubricas de IRRF com data de pagamento dentro da compet�ncia selecionada
@author Allyson
@since 19/06/2018
@version 2.0
@param cCompete 	- Compet�ncia da gera��o do evento
@param cPerIni 		- Per�odo inicial da gera��o do evento
@param cPerFim 		- Per�odo final da gera��o do evento
@param aArrayFil 	- Filiais selecionadas para processamento
@param lRetific 	- Indica se � retifica��o
@param lIndic13 	- Indica se � referente a 13� sal�rio
@param aLogsOk 		- Log de ocorr�ncias do processamento
@param cOpcTab 		- Indica se o per�odo em aberto ser� considerado (0=N�o|1=Sim)
@param aCheck 		- Checkbox da tela de gera��o dos per�odicos
@param cCPFDe 		- CPF inicial para filtro
@param cCPFAte 		- CPF final para filtro
@param lExcLote		- Indicativo de exclus�o em lote
@param cExpFiltro	- Express�o de filtro na tabela SRA
@param lNewProgres	- Indicativo de execu��o do rob�
@param aLogsErr 	- Log de ocorr�ncias de erro do processamento
@param aLogsProc 	- Log de ocorr�ncias do resumo do processamento
@param lRelat 		- Indica se � gera��o do relat�rio em Excel

@return NIL

/*/
Static Function NewProc1210(cCompete, cPerIni, cPerFim, aArrayFil, lRetific, lIndic13, aLogsOk, cOpcTab, aCheck, cCPFDe, cCPFAte, lExcLote, cExpFiltro, lNewProgres, aLogsErr, aLogsPrc, lRelat)

Local aArea			:= GetArea()
Local aAreaSM0		:= SM0->(GetArea())
Local aBkpFer		:= {}
Local aContdmDev	:= {}
Local aCpfDesp		:= {}
Local aDepAgreg 	:= {}
Local aFilInTaf		:= {}
Local aFilInAux 	:= {}
Local aNomes 		:= {}
Local aRotADI		:= fGetRotTipo("2")
Local aRotFOL		:= fGetRotTipo("1")
Local aTabIR		:= {}
Local aTabIRRF		:= {}
Local aVbFer		:= {}
Local aStatT3P		:= {}
Local aFilInativ	:= {}

Local cFilInativ	:= ""
Local cAliasQSRA	:= "SRAQRY"
Local cAliasQC		:= "SRACON"
Local cAliasQMV		:= "SRAQMV"
Local cAliasSRA		:= "SRA"
Local cAliasSRX		:= GetNextAlias()
Local cAno			:= substr(cCompete,3,4)
Local cBkpFil		:= cFilAnt
Local cBkpFilEnv	:= ""
Local cCatTSV		:= SuperGetMv( "MV_NTSV", .F., "701|711|712|741|" )
Local cCPF			:= ""
Local cCpfOld		:= ""
Local cDtIni		:= ""
Local cDtFim		:= ""
Local cDtGerRes		:= ""
Local cDtPesqI		:= substr(cCompete,3,4)+ substr(cCompete,1,2)+"01"
Local cDtPesqF		:= substr(cCompete,3,4)+ substr(cCompete,1,2)+"31"
Local cFilEnv		:= ""
Local cFilProc		:= ""
Local cFilPreTrf	:= ""
Local cFilx			:= ""
Local cideDmDev		:= ""
Local cIdeRubr		:= ""
Local cIdTbRub		:= ''
Local cJoinRCxRY	:= FWJoinFilial( "SRC", "SRY" )
Local cJoinRDxRY	:= FWJoinFilial( "SRD", "SRY" )
Local cLastRot		:= ""
Local cLstCPF		:= ''
Local cMes			:= substr(cCompete,1,2)
Local cMsgErro		:= ""
Local cNome			:= ""
Local cNomeTCPF		:= ""
Local cNomeTEmp		:= ""
Local cOldFil		:= ""
Local cOldFilEnv	:= ""
Local cOldOcorr		:= ""
Local cPD			:= ""
Local cPdCod0021	:= ""
Local cPdCod546		:= ""
Local cPdCod0151	:= ""
Local cPDLiq		:= ""
Local cPer			:= ""
Local cPer1210		:= SubStr(cCompete, 3, 4) + SubStr(cCompete, 1, 2)
Local cPer132		:= ""
Local cPeriodo		:= ""
Local cPerOld		:= ""
Local cPerRes		:= ""
Local cQuery		:= ""
Local cQueryCont	:= ""
Local cQueryMV		:= ""
Local cRecResc		:= ""
Local cRoteiro		:= ""
Local cSemRes		:= ""
Local cSRHFil		:= ""
Local cSRHMat		:= ""
Local cStat1		:= ''
Local cStatT3P		:= "-1"
Local cTpPgto		:= "1"
Local cTpRes		:= ""
Local cTpRot		:= ""
Local cRecibo		:= ""
Local cTSV			:= fCatTrabEFD("TSV")
Local cXml			:= ""
Local cSitFolh		:= ""
Local cEmp			:= ""
Local cEmpOld		:= ""
Local cTafKey		:= ""
Local cTpFolha		:= IIF(lIndic13, "2", "1")
Local cUltTpRes		:= ""


Local dDcgIni		:= SuperGetMV("MV_DTCGINI",nil,CTOD(" / / ") )
Local dDtPgt		:= DDATABASE
Local dDtRes		:= cToD("//")
Local dDtPgtRR		:= cToD("//")

Local lAchou063		:= .F.
Local lAfast		:= .F.
Local lCarrDep		:= .F.
Local lCPFDepOk		:= .T.
Local lFilAux		:= .F.
Local lGeraCod		:= .F.
Local lRes			:= .F.
Local lRetXml		:= .F.
Local lTem132		:= .F.
Local lTSV			:= .F.
Local lPgtRes		:= .T.
Local lPrimIdT		:= .T.
Local lResidExt		:= .F.
Local lResComPLR	:= .F.
Local lResOriCom	:= .F.

Local nVldOpcoes	:= 0
Local nC			:= 0
Local nCntFer		:= 0
Local nCont			:= 0
Local nContRot		:= 0
Local nI			:= 0
Local nLog			:= 0
Local nPdetPGtoAt	:= 0
Local nPdetPGtoFl	:= 0
Local nPosEmp		:= 0
Local nQtdeFolMV	:= 0
Local nValor		:= 0
Local nVlrDep		:= 0
Local nX			:= 0
Local nx1			:= 0
Local nHrInicio
Local nHrFim

Local aEmp_1210		:= {0, 0, 0, 0} //1 - Integrados TCV; 2 - Nao Integrados TCV; 3 - Integrados TSV; 4 - Nao Integrados TSV
Local aRGE1210 		:= {}

Local nContRes		:= 0
Local dlastDate		:= cToD("")
Local lGera546		:= .T.
Local lVer546		:= .T.
Local aCodHash		:= {}
Local cCodFol
Local cTipoCod
Local cCodINCIRF
Local cCodAdiant
Local cCodFil

Local lAdmPubl	 	:= .F.
Local aInfoC	 	:= {}
Local aDados	 	:= {}
Local aErros	 	:= {}
Local cTpInsc		:= ""
Local cNrInsc		:= ""
Local cChaveMid	 	:= ""
Local cStatNew		:= ""
Local cOperNew		:= ""
Local cRetfNew		:= ""
Local cRecibAnt		:= ""
Local cKeyMid		:= ""
Local cIdXml		:= ""
Local lNovoRJE		:= .T.
Local nRecEvt		:= 0
Local cVersMw		:= ""
Local nTotRec		:= 0
Local cTimeIni		:= Time()
Local aArrayFil2	:= {}
Local nZ			:= 0
Local nW			:= 0

Private aCodFol		:= {}
Private aCodBenef	:= {}
Private aDetPgtoFl	:= {}
Private aideBenef	:= {}
Private aInfoPgto	:= {}
Private aInfoSaude	:= {}
Private aInfoPrev	:= {}
Private aDetPgtoAt 	:= {}
Private aPgtoAnt 	:= {}
Private aRetFer		:= {}
Private aRetPensao	:= {}
Private aLogCIC		:= {}
Private aRetPgtoTot	:= {}
Private detPgtoFer	:= {}
Private aRelIncons	:= {}
Private aEstb		:= fGM23SM0(,.T.) //extrai lista de filiais da SM0
Private aSM0     	:= FWLoadSM0(.T.)
Private nOrdT3P		:= 0
Private nQtdeFol	:= 1
Private lTemEmp		:= !Empty(FWSM0Layout(cEmpAnt, 1))
Private lTemGC		:= fIsCorpManage( FWGrpCompany() )
Private cLayoutGC	:= FWSM0Layout(cEmpAnt)
Private nIniEmp 	:= At("E", cLayoutGC)
Private nTamEmp		:= Len(FWSM0Layout(cEmpAnt, 1))

Private cSRACPF 	:= "QRYSRACPF"
Private cSRAEmp 	:= "QRYSRAEMP"
Private oTmpCPF		:= Nil
Private oTmpEmp		:= Nil

Default aCheck		:= {.F., .F., .F., .F., .F., .F., .F., .F., .F., .F., .F., .F., .T.}
Default cOpcTab		:= "0"
Default cCPFDe		:= ""
Default cCPFAte		:= ""
Default cExpFiltro	:= ""
Default lNewProgres	:= .F.

Iif( FindFunction("fVersEsoc"), fVersEsoc( "S2200", .F., /*aRetGPE*/, /*aRetTAF*/, @cVersEnvio, Nil, @cVersMw ), .T.)

If !lMiddleware
	nOrdT3P		:= RetOrder( "T3P", "T3P_FILIAL+T3P_INDAPU+T3P_PERAPU+T3P_CPF+T3P_ATIVO", .T. )
EndIf

lAfast	:= aCheck[13]

//��������������������������������������������������������������Ŀ
//| Carrega Tabela de IRRF                                       |
//����������������������������������������������������������������
aTabIR 	 := {}
aTabIRRF := {}
fCarrTab( @aTabIRRF, "S002",  )
For nX := 1 To Len(aTabIRRF)
	If aTabIRRF[nX][1] == "S002"
		If aTabIRRF[nX][5] <= cAno+cMes .And. aTabIRRF[nX][6] >= cAno+cMes
			aAdd(aTabIR, aTabIRRF[nX][20])
		EndIf
	EndIf
Next nX

If !lMiddleware
	fGp23Cons(@aFilInTaf, aArrayFil, @cFilEnv, @aFilInativ)

	If Empty(cFilEnv)
		cFilEnv:= cFilAnt
	EndIf

	If Empty(aFilInTaf)
		MsgAlert( STR0065 + CRLF + STR0066 )//"N�o foi encontrada a filial de refer�ncia do TAF para a importa��o das informa��es."##"� necess�rio que seja inclu�do no cadastro de complemento de empresa a filial de destino para a importa��o das informa��es."
		Return .F.
	EndIf

	If LEN(aFilInativ) > 0

		FOR nI := 1 TO LEN(aFilInativ)
			cFilInativ += aFilInativ[nI] + ", "
		NEXT nI

		MsgAlert( "A(s) filial(is) " + cFilInativ + STR0072)
		Return .F.
	EndIf
EndIf

//Hora Inicial
nHrInicio := Seconds()

If lAglut
	If !lMiddleware
		For nI := 1 To Len(aFilInTaf)
			For nX := 1 to len(aFilInTaf[nI,3])
				cFilProc += aFilInTaf[nI,3,nX]
			Next
		Next nI  //
	Else
		For nI := 1 To Len(aArrayFil)
			cFilProc += aArrayFil[nI]
		Next nI
	EndIf
	cFilProc := fSQLIn(cFilProc, FwSizeFilial())
EndIf

aStru := SRD->(dbStruct())

If !Fp_CodFol(@aCodFol, xFilial('SRV'))
	Return(.F.)
EndIf
oHash := HMNew()
HMSet( oHash,xFilial('SRV'),aClone(aCodFol) )

//Grava quais s�o as ra�zes de CNPJ's selecionadas para processamento
If lAglut .And. !Empty(aArrayFil)
	For nZ := 1 to Len(aArrayFil)
		If (nW := aScan( aSM0, { |x| x[1] + x[2] == cEmpAnt + aArrayFil[nZ] } )) > 0
			aAdd(aArrayFil2, {aSM0[nW, 1], aSM0[nW, 2], SubStr(aSM0[nW, 18], 1, 8)})
		EndIf
	Next nZ
EndIf

For nI := 1 To Len(aArrayFil)
	//Quando aglutina, somente executa o For uma vez pois processa todas as filiais de uma vez
	If lAglut .And. nI > 1
		Exit
	EndIf
	If !lAglut
		If !lMiddleware
			cFilProc := StrTran(fGM23Fil(aFilInTaf, nI)[1], "%", "")
		Else
			cFilProc := "'" + aArrayFil[nI] + "'"
		EndIf
	EndIf
	If Select( cAliasQSRA ) > 0
		(cAliasQSRA)->( dbCloseArea() )
	EndIf

	//Query para filtrar os CPFs de acordo com o filtro da rotina
	fQryCPF(cFilProc, aFilInTaf, nI, cCPFDe, cCPFAte, cExpFiltro, .F., cPer1210, aArrayFil )
	cNomeTCPF := oTmpCPF:GetRealName()

	If lAglut
		//Query para filtrar os CNPJs de acordo com o filtro da rotina
		fQryEmp()
		cNomeTEmp := oTmpEmp:GetRealName()
	EndIf

	//Query para filtrar as matriculas que possuem c�lculo da folha de acordo com os CPFs filtrados
	cQuery := "SELECT SRA.RA_CIC, SRA.RA_FILIAL RC_FILIAL, SRA.RA_MAT RC_MAT, SRA.RA_PIS, SRA.RA_NOMECMP, SRA.RA_OCORREN, SRA.RA_NOME, SRA.RA_CATEFD, "
	cQuery += "SRA.RA_SINDICA, SRA.RA_CODUNIC, SRA.RA_DEPIR, SRA.RA_PROCES, SRA.RA_ADMISSA, SRA.RA_RESEXT, SRA.RA_CODRET, SRA.R_E_C_N_O_ AS RECNO "
	If lAglut
		cQuery += ", EMP.CNPJ as CNPJ "
	EndIf
	cQuery += "FROM " + RetSqlName('SRA') + " SRA "
	If lAglut
		cQuery += "INNER JOIN " + cNomeTEmp + " EMP ON SRA.RA_FILIAL = EMP.FILIAL "
		cQuery += "WHERE EXISTS (SELECT RA_CIC FROM " + cNomeTCPF + " SRA2 WHERE SRA2.RA_CIC = SRA.RA_CIC) "
	Else
		cQuery += "WHERE SRA.RA_FILIAL IN (" + cFilProc + ") "
		cQuery += "AND EXISTS (SELECT RA_CIC FROM " + cNomeTCPF + " SRA2 WHERE SRA2.RA_CIC = SRA.RA_CIC) "
	EndIf
	cQuery += "AND ( SRA.RA_PIS <> ' ' OR SRA.RA_CATEFD IN ('901','903','904') ) AND SRA.RA_CC <> ' ' "
	If !lExcLote
		cQuery += "AND EXISTS ("
		If cOpcTab == "1"
			cQuery += "SELECT DISTINCT SRC.RC_FILIAL RC_FILIAL, SRC.RC_MAT RC_MAT FROM " + RetSqlName('SRC') + " SRC INNER JOIN " + RetSqlName('SRY') + " SRY ON " + cJoinRCxRY + " AND SRC.RC_ROTEIR = SRY.RY_CALCULO WHERE SRC.RC_FILIAL = SRA.RA_FILIAL AND SRC.RC_MAT = SRA.RA_MAT AND SRC.RC_DATA BETWEEN '" + cDtPesqI + "' AND '" + cDtPesqF + "' AND SRC.RC_TIPO2 != 'K' AND SRC.D_E_L_E_T_ = ' ' AND SRY.RY_TIPO != 'K' AND SRY.D_E_L_E_T_ = ' ' "
			cQuery += "UNION "
		EndIf
		cQuery += "SELECT DISTINCT SRD.RD_FILIAL RC_FILIAL, SRD.RD_MAT RC_MAT FROM " + RetSqlName('SRD') + " SRD INNER JOIN " + RetSqlName('SRY') + " SRY ON " + cJoinRDxRY + " AND SRD.RD_ROTEIR = SRY.RY_CALCULO WHERE SRD.RD_FILIAL = SRA.RA_FILIAL AND SRD.RD_MAT = SRA.RA_MAT AND SRD.RD_DATPGT BETWEEN '" + cDtPesqI + "' AND '" + cDtPesqF + "' AND SRD.RD_TIPO2 != 'K' AND SRD.RD_EMPRESA = '  ' AND SRD.D_E_L_E_T_ = ' ' AND SRY.RY_TIPO != 'K' AND SRY.D_E_L_E_T_ = ' ' "
		cQuery += "UNION "
		cQuery += "SELECT DISTINCT SRH.RH_FILIAL RC_FILIAL, SRH.RH_MAT RC_MAT FROM " + RetSqlName('SRH') + " SRH WHERE SRH.RH_FILIAL = SRA.RA_FILIAL AND SRH.RH_MAT = SRA.RA_MAT AND SRH.RH_DTRECIB BETWEEN '" + cDtPesqI + "' AND '" + cDtPesqF + "' AND SRH.D_E_L_E_T_ = ' ') "
	EndIf
	cQuery += "AND SRA.D_E_L_E_T_ <> '*' "
	If !lAglut
		cQuery += "ORDER BY SRA.RA_CIC, SRA.RA_FILIAL, SRA.RA_MAT"
	Else
		cQuery += "ORDER BY SRA.RA_CIC, CNPJ"
	EndIf
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasQSRA,.T.,.T.)

	If !lRobo
		//Query para filtrar as matriculas que possuem c�lculo da folha de acordo com os CPFs filtrados
		cQueryCont := "SELECT COUNT(*) AS TOTAL "
		cQueryCont += "FROM " + RetSqlName('SRA') + " SRA "
		If lAglut
			cQueryCont += "INNER JOIN " + cNomeTEmp + " EMP ON SRA.RA_FILIAL = EMP.FILIAL "
			cQueryCont += "WHERE EXISTS (SELECT RA_CIC FROM " + cNomeTCPF + " SRA2 WHERE SRA2.RA_CIC = SRA.RA_CIC) "
		Else
			cQueryCont += "WHERE SRA.RA_FILIAL IN (" + cFilProc + ") "
			cQueryCont += "AND EXISTS (SELECT RA_CIC FROM " + cNomeTCPF + " SRA2 WHERE SRA2.RA_CIC = SRA.RA_CIC) "
		EndIf
		cQueryCont += "AND ( SRA.RA_PIS <> ' ' OR SRA.RA_CATEFD IN ('901','903','904') ) AND SRA.RA_CC <> ' ' "
		If !lExcLote
			cQueryCont += "AND EXISTS ("
			If cOpcTab == "1"
				cQueryCont += "SELECT DISTINCT SRC.RC_FILIAL RC_FILIAL, SRC.RC_MAT RC_MAT FROM " + RetSqlName('SRC') + " SRC INNER JOIN " + RetSqlName('SRY') + " SRY ON " + cJoinRCxRY + " AND SRC.RC_ROTEIR = SRY.RY_CALCULO WHERE SRC.RC_FILIAL = SRA.RA_FILIAL AND SRC.RC_MAT = SRA.RA_MAT AND SRC.RC_DATA BETWEEN '" + cDtPesqI + "' AND '" + cDtPesqF + "' AND SRC.RC_TIPO2 != 'K' AND SRC.D_E_L_E_T_ = ' ' AND SRY.RY_TIPO != 'K' AND SRY.D_E_L_E_T_ = ' ' "
				cQueryCont += "UNION "
			EndIf
			cQueryCont += "SELECT DISTINCT SRD.RD_FILIAL RC_FILIAL, SRD.RD_MAT RC_MAT FROM " + RetSqlName('SRD') + " SRD INNER JOIN " + RetSqlName('SRY') + " SRY ON " + cJoinRDxRY + " AND SRD.RD_ROTEIR = SRY.RY_CALCULO WHERE SRD.RD_FILIAL = SRA.RA_FILIAL AND SRD.RD_MAT = SRA.RA_MAT AND SRD.RD_DATPGT BETWEEN '" + cDtPesqI + "' AND '" + cDtPesqF + "' AND SRD.RD_TIPO2 != 'K' AND SRD.RD_EMPRESA = '  ' AND SRD.D_E_L_E_T_ = ' ' AND SRY.RY_TIPO != 'K' AND SRY.D_E_L_E_T_ = ' ' "
			cQueryCont += "UNION "
			cQueryCont += "SELECT DISTINCT SRH.RH_FILIAL RC_FILIAL, SRH.RH_MAT RC_MAT FROM " + RetSqlName('SRH') + " SRH WHERE SRH.RH_FILIAL = SRA.RA_FILIAL AND SRH.RH_MAT = SRA.RA_MAT AND SRH.RH_DTRECIB BETWEEN '" + cDtPesqI + "' AND '" + cDtPesqF + "' AND SRH.D_E_L_E_T_ = ' ') "
		EndIf
		cQueryCont += "AND SRA.D_E_L_E_T_ <> '*' "
		cQueryCont := ChangeQuery(cQueryCont)
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQueryCont),cAliasQC,.T.,.T.)

		If (cAliasQC)->( !EoF() )
			nTotRec := (cAliasQC)->TOTAL
			If !lNewProgres
				GPProcRegua(nTotRec)
			Else
				BarGauge1Set(nTotRec)
			EndIf
		EndIf
		(cAliasQC)->( dbCloseArea() )
	EndIf

	If lAglut
		//Query para verificar quantas matriculas de um mesmo CPF possuem c�lculo da folha de acordo com os CPFs filtrados
		cQueryMV := "SELECT SRA.RA_CIC, "
		cQueryMV += "EMP.CNPJ as CNPJ, "
		cQueryMV += " COUNT(*) AS CONT FROM " + RetSqlName('SRA') + " SRA "
		cQueryMV += "INNER JOIN " + cNomeTEmp + " EMP ON SRA.RA_FILIAL = EMP.FILIAL "
		cQueryMV += "WHERE EXISTS (SELECT RA_CIC FROM " + cNomeTCPF + " SRA2 WHERE SRA2.RA_CIC = SRA.RA_CIC) "
		cQueryMV += "AND ( SRA.RA_PIS <> ' ' OR SRA.RA_CATEFD IN ('901','903','904') ) AND SRA.RA_CC <> ' ' "
		If !lExcLote
			cQueryMV += "AND EXISTS ("
			If cOpcTab == "1"
				cQueryMV += "SELECT DISTINCT SRC.RC_FILIAL RC_FILIAL, SRC.RC_MAT RC_MAT FROM " + RetSqlName('SRC') + " SRC INNER JOIN " + RetSqlName('SRY') + " SRY ON " + cJoinRCxRY + " AND SRC.RC_ROTEIR = SRY.RY_CALCULO WHERE SRC.RC_FILIAL = SRA.RA_FILIAL AND SRC.RC_MAT = SRA.RA_MAT AND SRC.RC_DATA BETWEEN '" + cDtPesqI + "' AND '" + cDtPesqF + "' AND SRC.RC_TIPO2 != 'K' AND SRC.D_E_L_E_T_ = ' ' AND SRY.RY_TIPO != 'K' AND SRY.D_E_L_E_T_ = ' ' "
				cQueryMV += "UNION "
			EndIf
			cQueryMV += "SELECT DISTINCT SRD.RD_FILIAL RC_FILIAL, SRD.RD_MAT RC_MAT FROM " + RetSqlName('SRD') + " SRD INNER JOIN " + RetSqlName('SRY') + " SRY ON " + cJoinRDxRY + " AND SRD.RD_ROTEIR = SRY.RY_CALCULO WHERE SRD.RD_FILIAL = SRA.RA_FILIAL AND SRD.RD_MAT = SRA.RA_MAT AND SRD.RD_DATPGT BETWEEN '" + cDtPesqI + "' AND '" + cDtPesqF + "' AND SRD.RD_TIPO2 != 'K' AND SRD.RD_EMPRESA = '  ' AND SRD.D_E_L_E_T_ = ' ' AND SRY.RY_TIPO != 'K' AND SRY.D_E_L_E_T_ = ' ' "
			cQueryMV += "UNION "
			cQueryMV += "SELECT DISTINCT SRH.RH_FILIAL RC_FILIAL, SRH.RH_MAT RC_MAT FROM " + RetSqlName('SRH') + " SRH WHERE SRH.RH_FILIAL = SRA.RA_FILIAL AND SRH.RH_MAT = SRA.RA_MAT AND SRH.RH_DTRECIB BETWEEN '" + cDtPesqI + "' AND '" + cDtPesqF + "' AND SRH.D_E_L_E_T_ = ' ') "
		EndIf

		cQueryMV += "AND SRA.D_E_L_E_T_ <> '*' "
		cQueryMV += "GROUP BY SRA.RA_CIC, CNPJ "
		cQueryMV += "ORDER BY SRA.RA_CIC, CNPJ"
		cQueryMV := ChangeQuery(cQueryMV)
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQueryMV),cAliasQMV,.T.,.T.)
		nQtdeFol := (cAliasQMV)->CONT
	EndIf

	While (cAliasQSRA)->(!EOF())
		(cAliasSRA) ->( dbGoTo( (cAliasQSRA)->RECNO ) )

		//Identifica se o funcion�rio � residente no exterior
		lResidExt := .F.
		lResidExt := ( (cAliasSRA)->RA_RESEXT == "1" .And. (cAliasSRA)->RA_CODRET == "0473"  )
		aRGE1210 := {}

		If Empty(cEmp)
			If ( nPosEmp := aScan( aSM0, { |x| x[1] + x[2] == cEmpAnt + (cAliasQSRA)->RC_FILIAL } ) ) > 0
				cEmp 	:= SubStr(aSM0[nPosEmp, 18], 1, 8)
				cEmpOld := cEmp
			EndIf
		EndIf

		If	lAglut .And. ((cAliasQSRA)->RA_CIC != (cAliasQMV)->RA_CIC .OR.;
			((cAliasQSRA)->RA_CIC == (cAliasQMV)->RA_CIC .AND. cEmp != cEmpOld)) //Se o CPF for o mesmo, por�m, a empresa � diferente
			(cAliasQMV)->( dbSkip() )
			nQtdeFol := (cAliasQMV)->CONT
		EndIf

		cSRHFil	:= cFilPreTrf := (cAliasSRA)->RA_FILIAL
		cSRHMat	:= (cAliasSRA)->RA_MAT
		If fFerPreTrf(@cSRHFil, @cSRHMat, cDtPesqF )
			(cAliasQSRA)->( dbSkip() )
			Loop
		EndIf

		cFilAnt := (cAliasSRA)->RA_FILIAL
		If lMiddleware
			fPosFil( cEmpAnt, cFilAnt )
		EndIf
		
		If !lIntTAF .And. (lMiddleware .And. Empty(fXMLInfos()))
			(cAliasQSRA)->( dbSkip() )
			If ( nPosEmp := aScan( aSM0, { |x| x[1] + x[2] == cEmpAnt + (cAliasQSRA)->RC_FILIAL } ) ) > 0
				cEmp 	:= SubStr(aSM0[nPosEmp, 18], 1, 8)
			EndIf
			Loop
		EndIf

		//Verifica filial centralizadora do envio
		If cOldFilEnv != cFilAnt
			cOldFilEnv := cFilAnt
			RstaCodFol()
			aCodHash := {}
			If HMGet(oHash,xFilial('SRV', (cAliasSRA)->RA_FILIAL),@aCodHash)
				aCodFol := aCodHash
			Else
				If !Fp_CodFol(@aCodFol, xFilial('SRV', (cAliasSRA)->RA_FILIAL))
					Return(.F.)
				EndIf
				HMSet( oHash,xFilial('SRV', (cAliasSRA)->RA_FILIAL),aClone(aCodFol) )
			EndIf
			cPdCod0021 	:= aCodFol[21,1]
			cPdCod546	:= aCodFol[546,1]
			cPdCod0151	:= aCodFol[151,1]
			If !lMiddleware
				lFilAux		:= .F.
				For nX := 1 To Len(aFilInTaf)
					If aScan( aFilInTaf[nX, 3], { |x| x == cFilAnt } ) > 0
						cFilEnv := aFilInTaf[nX, 2]
						lFilAux	:= .T.
						Exit
					EndIf
				Next nX
				If !lFilAux
					fGp23Cons(aFilInAux, {cFilAnt})
					For nX := 1 To Len(aFilInAux)
						If aScan( aFilInAux[nX, 3], { |x| x == cFilAnt } ) > 0
							cFilEnv := aFilInAux[nX, 2]
							Exit
						EndIf
					Next nX
				EndIf
			Else
				cFilEnv := cFilAnt
			EndIf
		EndIf

		//Se P_ESOCMV for .T., relacionamento 1 x 1 e (raiz do CNPJ do funcion�rio com m�ltiplo v�nculo n�o estiver selecionada ou
		//Funcion�rio n�o tem m�ltiplo v�nculo no per�odo e filial n�o est� seleciona) pula para o pr�ximo registro
		If lAglut .And. cFilEnv == (cAliasQSRA)->RC_FILIAL .And. (( nW := aScan(aArrayFil2, {|x| x[3] == cEmp }) ) == 0 .Or.;
		( nW := aScan(aArrayFil2, {|x| x[1] + x[2] == cEmpAnt + (cAliasQSRA)->RC_FILIAL })) == 0) .And. (cAliasQMV)->CONT == 1 .And.; 
		(cAliasQMV)->RA_CIC == (cAliasQSRA)->RA_CIC .And. (cAliasQMV)->CNPJ == cEmp
			(cAliasQSRA)->( dbSkip() )
			If ( nPosEmp := aScan( aSM0, { |x| x[1] + x[2] == cEmpAnt + (cAliasQSRA)->RC_FILIAL } ) ) > 0
				cEmpOld := cEmp
				cEmp 	:= SubStr(aSM0[nPosEmp, 18], 1, 8)
			EndIf
			Loop
		EndIf 

		If !lRobo
			If !lNewProgres
				GPIncProc( "CPF: " + Transform((cAliasSRA)->RA_CIC, "@R 999.999.999-99") + " | Funcion�rio: " + (cAliasSRA)->RA_FILIAL + (cAliasSRA)->RA_MAT )
			Else
				IncPrcG1Time("CPF: " + Transform((cAliasSRA)->RA_CIC, "@R 999.999.999-99") + " | Funcion�rio: " + (cAliasSRA)->RA_FILIAL + (cAliasSRA)->RA_MAT, nTotRec, cTimeIni, .T., 1, 1, .T.)
			EndIf
		EndIf

		cCPF    	:= (cAliasSRA)->RA_CIC
		cFil   		:= (cAliasSRA)->RA_FILIAL
		cMat   		:= (cAliasSRA)->RA_MAT
		cProcAtu    := (cAliasSRA)->RA_PROCES
		cMes   		:= Left(cCompete,2)
		cAno   		:= Right(cCompete,4)
		cCat 		:= (cAliasSRA)->RA_CATEFD
		cSitFolh	:= (cAliasSRA)->RA_SITFOLH
		cPerRes		:= ""
		cSemRes		:= ""
		dDtRes		:= cToD("//")
		cTpRes		:= ""
		cUltTpRes	:= ""
		cDtGerRes	:= ""
		cRecResc	:= ""
		lRes 		:= fGetRes(cFil, cMat, cCompete, @dDtRes, Nil, @cTpRes, @cPerRes, Nil, @cSemRes, @cRecResc, cTSV, @lResComPLR )
		cStat1		:= ""
		lPgtRes		:= .T.
		lTem132	 	:= .F.
		nContRes 	:= 0
		dlastDate	:= cToD("//")
		cLastRot	:= ""
		cPerOld		:= ""
		nQtdeFolMV	:= 0
		lGera546	:= .T.
		lVer546		:= .T.
		lResOriCom	:= .F.

		If lMiddleware
			fPosFil( cEmpAnt, SRA->RA_FILIAL )
			aInfoC   := fXMLInfos()
			If Len(aInfoC) >= 4
				cTpInsc  := aInfoC[1]
				lAdmPubl := aInfoC[4]
				cNrInsc  := aInfoC[2]
				cIdXml   := aInfoC[3]
			Else
				cTpInsc  := ""
				lAdmPubl := .F.
				cNrInsc  := "0"
			EndIf
		EndIf

		If cCat $ cTSV
			If !( cCat $ cCatTSV )
				If !lMiddleware
					cLstCPF := AllTrim( (cAliasSRA)->(RA_CIC) ) + ";" + AllTrim( (cAliasSRA)->(RA_CATEFD) ) + ";" + AllTrim( dToS((cAliasSRA)->(RA_ADMISSA)) )
					cStat1  := TAFGetStat( "S-2300", cLstCPF )
				Else
					cLstCPF := AllTrim( SRA->RA_CIC ) + AllTrim( SRA->RA_CATEFD ) + DTOS( SRA->RA_ADMISSA )
					cChaveMid	:= cTpInsc + PADR( Iif( !lAdmPubl .And. cTpInsc == "1", SubStr(cNrInsc, 1, 8), cNrInsc), 14) + "S2300" + Padr(cLstCPF, 40, " ")
					cStat1 	:= "-1"
					//RJE_TPINSC+RJE_INSCR+RJE_EVENTO+RJE_KEY+RJE_INI
					GetInfRJE( 2, cChaveMid, @cStat1 )
				EndIf
			EndIf
			lTSV := .T.
		Else
			If !lMiddleware
				cLstCPF := AllTrim((cAliasSRA)->(RA_CIC)) + ";" + ALLTRIM((cAliasSRA)->(RA_CODUNIC))
				cStat1  := TAFGetStat( "S-2200", cLstCPF )
			Else
				cChaveMid	:= cTpInsc + PADR( Iif( !lAdmPubl .And. cTpInsc == "1", SubStr(cNrInsc, 1, 8), cNrInsc), 14) + "S2200" + Padr(SRA->RA_CODUNIC, 40, " ")
				cStat1 		:= "-1"
				//RJE_TPINSC+RJE_INSCR+RJE_EVENTO+RJE_KEY+RJE_INI
				GetInfRJE( 2, cChaveMid, @cStat1 )
			EndIf
			lTSV := .F.
		Endif
		If cStat1 == "-1"
			If !(cSitFolh == "D" .And. !lRes) .And. aScan(aLogsErr, {|X| "Funcion�rio("+(cAliasSRA)->RA_CIC+")" $ X}) == 0
				If !lMiddleware
					aAdd(aLogsErr, "[FALHA] N�o foi possivel encontrar o registro do Funcion�rio ("+(cAliasSRA)->RA_CIC+")-"+Alltrim((cAliasSRA)->RA_NOME)+ " no TAF." + CRLF) //##" ao integrar funcionario "
				Else
					aAdd(aLogsErr, "[FALHA] N�o foi possivel encontrar o registro do Funcion�rio ("+(cAliasSRA)->RA_CIC+")-"+Alltrim((cAliasSRA)->RA_NOME)+ " no Middleware." + CRLF) //##" ao integrar funcionario "
				EndIf
			EndIf

			If lTSV
				aEmp_1210[4]++ //Inclui TSV nao integrado
			Else
				aEmp_1210[2]++ //Inclui TCV nao integrado
			EndIf

			If lRelat
				aAdd( aRelIncons, { SRA->RA_FILIAL, SRA->RA_CIC, OemToAnsi(STR0206) } )//"N�o foi possivel encontrar o registro do Funcion�rio"
			EndIf

			(cAliasQSRA)->(DBSkip())
			Loop
		EndIf

		If !lAglut .Or. nQtdeFol == 1 .Or. SRA->RA_CIC != cCpfOld .Or. cEmp != cEmpOld
			cStatNew := ""
			cOperNew := ""
			cRetfNew := ""
			cRecibAnt:= ""
			cKeyMid	 := ""
			nRecEvt	 := 0
			lNovoRJE := .T.
			aStatT3P := fVerStat( 2, cFilEnv, cPer1210, aClone(aFilInTaf), Nil, @cStatNew, @cOperNew, @cRetfNew, @nRecEvt, @cRecibAnt, @lNovoRJE, @cKeyMid, lAdmPubl, cTpInsc, cNrInsc )
			cStatT3P := aStatT3P[1]
			cRecibo  := aStatT3P[2]
		EndIf

		/*
		�������������������������������������������������
		���������������������������������������������Ŀ��
		��� Exclusao em lote dos registros            ���
		����������������������������������������������ٱ�
		�������������������������������������������������*/
		If lExcLote
			If cStatT3P $ "4"
				cXml := ""
				cStatNew := ""
				cOperNew := ""
				cRetfNew := ""
				cRecibAnt:= ""
				cKeyMid	 := ""
				nRecEvt	 := 0
				lNovoRJE := .T.
				aDados	 := {}
				InExc3000(@cXml,'S-1210',cRecibo,(cAliasQSRA)->RA_CIC,(cAliasQSRA)->RA_PIS,.T.,"1",cAno+"-"+cMes, (cAliasQSRA)->RA_CATEFD, cFilEnv, lAdmPubl, cTpInsc, cNrInsc, cIdXml, @cStatNew, @cOperNew, @cRetfNew, @nRecEvt, @lNovoRJE, @cKeyMid, @aErros)
				GrvTxtArq(alltrim(cXml), "S3000", (cAliasQSRA)->RA_CIC)
				If !lMiddleware
					aErros := TafPrepInt( cEmpAnt, cFilEnv, cXml, , "1", "S3000")
				ElseIf Empty(aErros)
					aAdd( aDados, { xFilial("RJE", cFilEnv), cFilEnv, cTpInsc, Iif( cTpInsc == "1" .And. !lAdmPubl, SubStr(cNrInsc, 1, 8), cNrInsc ), "S3000", Space(6), cRecibo, cIdXml, cRetfNew, "12", cStatNew, Date(), Time(), cOperNew, NIL, NIL } )
					If !(fGravaRJE( aDados, cXML, lNovoRJE, nRecEvt ))
						aAdd( aErros, OemToAnsi(STR0155) )//"Ocorreu um erro na grava��o do registro na tabela RJE"
					EndIf
				EndIf
				If Len( aErros ) > 0
					cMsgErro := ''
					FeSoc2Err( aErros[1], @cMsgErro ,IIF(aErros[1]!='000026',1,2))
					FormText(@cMsgErro)
					aErros[1] := cMsgErro
					aAdd(aLogsErr, OemToAnsi(STR0046) + OemToAnsi(STR0048) + (cAliasQSRA)->RA_CIC + " - " + AllTrim((cAliasQSRA)->RA_NOME) + OemToAnsi(STR0029) ) //"[FALHA] "##"Registro de exclusao S-1210 do Funcion�rio: "##" n�o foi integrado devido ao(s) erro(s) abaixo: "
					aAdd(aLogsErr, "" )
					aAdd(aLogsErr, aErros[1] )
					If lTSV
						aEmp_1210[4]++ //Inclui TSV nao integrado
					Else
						aEmp_1210[2]++ //Inclui TCV nao integrado
					EndIf
				Else
					If !lMiddleware
						aAdd(aLogsOk, OemToAnsi(STR0048) + (cAliasQSRA)->RA_CIC + " - " + AllTrim((cAliasQSRA)->RA_NOME) + OemToAnsi(STR0049) ) //##"Registro de exclusao S-1210 do Funcion�rio: "##" Integrado com TAF."
					Else
						aAdd(aLogsOk, OemToAnsi(STR0048) + (cAliasQSRA)->RA_CIC + " - " + AllTrim((cAliasQSRA)->RA_NOME) + OemToAnsi(STR0165) ) //##"Registro de exclusao S-1210 do Funcion�rio: "##" Integrado com TAF."
					EndIf
					aAdd(aLogsOk, "" )
					If lTSV
						aEmp_1210[3]++ //Inclui TSV integrado
					Else
						aEmp_1210[1]++ //Inclui TCV integrado
					EndIf
				Endif
			ElseIf cStatT3P $ "2/6"
				aAdd(aLogsErr, OemToAnsi(STR0046) + OemToAnsi(STR0048) + (cAliasQSRA)->RA_CIC + " - " + AllTrim((cAliasQSRA)->RA_NOME) + OemToAnsi(STR0028) ) //"[FALHA] "##"Registro de exclusao S-1210 do Funcion�rio: "##" desprezado pois est� aguardando retorno do governo."
				aAdd(aLogsErr, "" )
				If lTSV
					aEmp_1210[4]++ //Inclui TSV nao integrado
				Else
					aEmp_1210[2]++ //Inclui TCV nao integrado
				EndIf
			ElseIf cStatT3P == "99"
				aAdd(aLogsErr, OemToAnsi(STR0148) + OemToAnsi(STR0048) + (cAliasQSRA)->RA_CIC + " - " + AllTrim((cAliasQSRA)->RA_NOME) + OemToAnsi(STR0166) ) //"[AVISO] "##"Registro de exclusao S-1210 do Funcion�rio: "##" desprezado pois h� evento de exclus�o pendente para transmiss�o."
				aAdd(aLogsErr, "" )
				If lTSV
					aEmp_1210[4]++ //Inclui TSV nao integrado
				Else
					aEmp_1210[2]++ //Inclui TCV nao integrado
				EndIf
			ElseIf cStatT3P != "-1"
				aAdd(aLogsErr, OemToAnsi(STR0148) + OemToAnsi(STR0048) + (cAliasQSRA)->RA_CIC + " - " + AllTrim((cAliasQSRA)->RA_NOME) + OemToAnsi(STR0150) ) //"[AVISO] "##"Registro de exclusao S-1210 do Funcion�rio: "##" desprezado pois n�o foi transmitido."
				aAdd(aLogsErr, "" )
				If lTSV
					aEmp_1210[4]++ //Inclui TSV nao integrado
				Else
					aEmp_1210[2]++ //Inclui TCV nao integrado
				EndIf
			Else
				aAdd(aLogsErr, OemToAnsi(STR0148) + OemToAnsi(STR0025) + (cAliasQSRA)->RA_CIC + " - " + AllTrim((cAliasQSRA)->RA_NOME) + OemToAnsi(STR0151) ) //"[AVISO] "##"Registro S-1210 do funcionario "##" n�o foi encontrado. A exclus�o n�o poder� ser realizada."
				aAdd(aLogsErr, "" )
				If lTSV
					aEmp_1210[4]++ //Inclui TSV nao integrado
				Else
					aEmp_1210[2]++ //Inclui TCV nao integrado
				EndIf
			EndIf

			(cAliasQSRA)->(DBSkip())
			Loop
		EndIf

		If !lRelat
			nVldOpcoes := fVldOpcoes(aCheck, cStatT3P)

			If nVldOpcoes == 1
				If aScan( aCpfDesp, { |x| x == SRA->RA_CIC } ) == 0
					aAdd(aCpfDesp, SRA->RA_CIC)
					aAdd(aLogsErr, OemToAnsi(STR0025) + (cAliasSRA)->RA_CIC + " - " + AllTrim((cAliasSRA)->RA_NOME) + OemToAnsi(STR0026) ) //##"Registro S-1210 do Funcion�rio: "##" n�o foi sobrescrito."
					aAdd(aLogsErr, "" )
					If lTSV
						aEmp_1210[4]++ //Inclui TSV nao integrado
					Else
						aEmp_1210[2]++ //Inclui TCV nao integrado
					EndIf
				EndIf
				(cAliasQSRA)->(DBSkip())
				Loop
			Elseif nVldOpcoes == 2
				If aScan( aCpfDesp, { |x| x == SRA->RA_CIC } ) == 0
					aAdd(aCpfDesp, SRA->RA_CIC)
					aAdd(aLogsErr, OemToAnsi(STR0025) + (cAliasSRA)->RA_CIC + " - " + AllTrim((cAliasSRA)->RA_NOME) + OemToAnsi(STR0027) ) //##"Registro S-1210 do Funcion�rio: "##" n�o foi retificado."
					aAdd(aLogsErr, "" )
					If lTSV
						aEmp_1210[4]++ //Inclui TSV nao integrado
					Else
						aEmp_1210[2]++ //Inclui TCV nao integrado
					EndIf
				EndIf
				(cAliasQSRA)->(DBSkip())
				Loop
			Elseif nVldOpcoes == 3
				If aScan( aCpfDesp, { |x| x == SRA->RA_CIC } ) == 0
					aAdd(aCpfDesp, SRA->RA_CIC)
					aAdd(aLogsErr, OemToAnsi(STR0025) + (cAliasSRA)->RA_CIC + " - " + AllTrim((cAliasSRA)->RA_NOME) + OemToAnsi(STR0028) ) //##"Registro S-1210 do Funcion�rio: "##" desprezado pois est� aguardando retorno do governo."
					aAdd(aLogsErr, "" )
					If lTSV
						aEmp_1210[4]++ //Inclui TSV nao integrado
					Else
						aEmp_1210[2]++ //Inclui TCV nao integrado
					EndIf
				EndIf
				(cAliasQSRA)->(DBSkip())
				Loop
			ElseIf cStatT3P == "99"
				aAdd(aLogsErr, OemToAnsi(STR0025) + (cAliasSRA)->RA_CIC + " - " + AllTrim((cAliasSRA)->RA_NOME) + OemToAnsi(STR0166) ) //##"Registro S-1210 do Funcion�rio: "##" desprezado pois h� evento de exclus�o pendente para transmiss�o."
				aAdd(aLogsErr, "" )
				If lTSV
					aEmp_1210[4]++ //Inclui TSV nao integrado
				Else
					aEmp_1210[2]++ //Inclui TCV nao integrado
				EndIf
				(cAliasQSRA)->(DBSkip())
				Loop
			Endif
		EndIf

		If Select(cAliasSRX) > 0
			(cAliasSRX)->(dbcloseArea())
		EndIf

		If !lAglut .Or. nQtdeFol == 1 .Or. cCPF <> cCpfOld .Or. cEmp != cEmpOld
			aContdmDev	:= {}
			nVlrDep		:= 0
			cCpfOld		:= cCPF
			cNome  		:= Alltrim((cAliasSRA)->RA_NOME)
			cOldFil 	:= (cAliasSRA)->RA_FILIAL
			cOldOcorr	:= (cAliasSRA)->RA_OCORREN
			cEmpOld		:= cEmp
			If Len( aTabIR ) > 0//testa se existe dados na tabela
				If aTabIR[1] <> 0 .and. VAL((cAliasSRA)->RA_DEPIR) <> 0
					nVlrDep := VAL((cAliasSRA)->RA_DEPIR) * aTabIR[1]
				Endif
			EndIf
			cXml := ""
			If !lMiddleware
				S1210A01(@cXml)
				S1210A02(@cXml, {})
				S1210A03(@cXml, {"1",,"1",cAno+"-"+cMes,,,,,}, .T.)
				S1210A05(@cXml, {(cAliasSRA)->RA_CIC}, .F.)
				S1210A06(@cXml, {nVlrDep}, .T.)
			EndIf
			aAdd(aideBenef, {(cAliasSRA)->RA_CIC, nvlrDep, (cAliasSRA)->RA_FILIAL})
			aDados		:= {}
			aErros		:= {}
			lPrimIdT	:= .T.
			cIdeRubr	:= ""
			If !lRelat
				aInfoPgto	:= {}
				aDetPgtoFl	:= {}
				aRetFer		:= {}
				aRetPgtoTot	:= {}
				aInfoSaude	:= {}
				aInfoPrev	:= {}
				aRetPensao	:= {}
				aLogCIC		:= {}
				aDetPgtoAt 	:= {}
				aPgtoAnt 	:= {}
				aDepAgreg	:= {}
			EndIf
			aNomes		:= {}
			lCPFDepOk	:= .T.
			lCarrDep	:= .F.
		Endif

		If (!lRes .And. __oSt05_1 == Nil) .Or. (lRes .And. __oSt05_2 == Nil)
			If !lRes
				__oSt05_1 := FWPreparedStatement():New()
				cQrySt := "SELECT RD_FILIAL,RD_MAT,RD_DATPGT,RD_SEMANA,RD_PD,RD_SEQ,RD_CC,RD_PERIODO,RD_ROTEIR,RD_VALOR,RD_IDCMPL,'SRD' AS TAB,SRD.R_E_C_N_O_ AS RECNO "
			Else
				__oSt05_2 := FWPreparedStatement():New()
				cQrySt := "SELECT DISTINCT RD_FILIAL,RD_MAT,RD_DATPGT,RD_SEMANA,RD_PD,RD_SEQ,RD_CC,RD_PERIODO,RD_ROTEIR,RD_VALOR,RD_IDCMPL,'SRD' AS TAB,SRD.R_E_C_N_O_ AS RECNO  "
			EndIf
			cQrySt += "FROM " + RetSqlName('SRD') + " SRD "
			cQrySt += "INNER JOIN " + RetSqlName('SRY') + " SRY ON " + cJoinRDxRY + " AND SRD.RD_ROTEIR = SRY.RY_CALCULO "
			cQrySt += "WHERE SRD.RD_FILIAL = ? AND "
			cQrySt += 		"SRD.RD_MAT = ? AND "
			cQrySt += 		"SRD.RD_DATPGT BETWEEN '" + cDtPesqI + "' AND '" + cDtPesqF + "' AND "
			cQrySt += 		"SRD.RD_TIPO2 != 'K' AND "
			cQrySt += 		"SRD.RD_EMPRESA = '  ' AND "
			cQrySt += 		"SRD.D_E_L_E_T_ = ' ' AND "
			cQrySt += 		"SRY.RY_TIPO != 'K' AND "
			cQrySt += 		"SRY.RY_FILIAL = ? AND "
			cQrySt += 		"SRY.D_E_L_E_T_ = ' ' AND "
			cQrySt +=		"SRD.RD_PD NOT IN ("
			cQrySt +=			"SELECT SRR.RR_PD "
			cQrySt +=			"FROM " + RetSqlName('SRR') + " SRR "
			cQrySt +=			"WHERE SRR.RR_FILIAL = ? AND "
			cQrySt +=				"SRR.RR_MAT = ? AND "
			cQrySt +=				"SRR.RR_DATAPAG = SRD.RD_DATPGT  AND "
			If !lRes
				cQrySt +=			"SRR.RR_TIPO3 = 'F'  AND "
			EndIf
			cQrySt +=				"SRR.D_E_L_E_T_ = ' ') "
			If cOpcTab == "1" .Or. lRes
				cQrySt += "UNION "
				If !lRes
					cQrySt += "SELECT RC_FILIAL,RC_MAT,RC_DATA,RC_SEMANA,RC_PD,RC_SEQ,RC_CC,RC_PERIODO,RC_ROTEIR,RC_VALOR,RC_IDCMPL,'SRC' AS TAB,SRC.R_E_C_N_O_ AS RECNO "
				Else
					cQrySt += "SELECT DISTINCT RC_FILIAL AS RD_FILIAL,RC_MAT AS RD_MAT,RC_DATA AS RD_DATPGT,RC_SEMANA AS RD_SEMANA,RC_PD AS RD_PD,RC_SEQ AS RD_SEQ,RC_CC AS RD_CC,RC_PERIODO AS RD_PERIODO,RC_ROTEIR AS RD_ROTEIR,RC_VALOR AS RD_VALOR,RC_IDCMPL AS RD_IDCMPL,'SRC' AS TAB,SRC.R_E_C_N_O_ AS RECNO "
				EndIf
				cQrySt += "FROM " + RetSqlName('SRC') + " SRC "
				cQrySt += "INNER JOIN " + RetSqlName('SRY') + " SRY ON " + cJoinRCxRY + " AND SRC.RC_ROTEIR = SRY.RY_CALCULO "
				cQrySt += "WHERE SRC.RC_FILIAL = ? AND "
				cQrySt += 		"SRC.RC_MAT = ? AND "
				cQrySt += 		"SRC.RC_DATA BETWEEN '" + cDtPesqI + "' AND '" + cDtPesqF + "' AND "
				cQrySt += 		"SRC.RC_TIPO2 != 'K' AND "
				cQrySt += 		"SRC.D_E_L_E_T_ = ' ' AND "
				cQrySt += 		"SRY.RY_TIPO != 'K' AND "
				cQrySt += 		"SRY.D_E_L_E_T_ = ' ' AND "
				cQrySt +=		"SRC.RC_PD NOT IN ("
				cQrySt +=			"SELECT SRR.RR_PD "
				cQrySt +=			"FROM " + RetSqlName('SRR') + " SRR "
				cQrySt +=			"WHERE SRR.RR_FILIAL = ? AND "
				cQrySt +=				"SRR.RR_MAT = ? AND "
				cQrySt +=				"SRR.RR_DATAPAG = SRC.RC_DATA AND "
				If !lRes
					cQrySt +=			"SRR.RR_TIPO3 = 'F'  AND "
				EndIf
				cQrySt +=				"SRR.D_E_L_E_T_ = ' ') "
			EndIf
			If lRes
				cQrySt += "UNION ALL "
				cQrySt += "SELECT RR_FILIAL,RR_MAT,RR_DATA,RR_SEMANA,RR_PD,RR_SEQ,RR_CC,RR_PERIODO,RR_ROTEIR,RR_VALOR,RR_IDCMPL,'SRR' AS TAB, SRR.R_E_C_N_O_ AS RECNO "
				cQrySt += "FROM " + RetSqlName('SRR') + " SRR "
				cQrySt += "WHERE SRR.RR_FILIAL = ? AND "
				cQrySt += 		"SRR.RR_MAT = ? AND "
				cQrySt += 		"SRR.RR_DATAPAG BETWEEN '" + cDtPesqI + "' AND '" + cDtPesqF + "' AND "
				cQrySt += 		"SRR.RR_TIPO2 != 'K' AND "
				cQrySt += 		"SRR.RR_TIPO3 != 'F' AND "
				cQrySt += 		"SRR.D_E_L_E_T_ = ' ' "
			EndIf
			cQrySt += "ORDER BY 1, 2, 8, 9, 3, 5"
			cQrySt := ChangeQuery(cQrySt)
			If !lRes
				__oSt05_1:SetQuery(cQrySt)
			Else
				__oSt05_2:SetQuery(cQrySt)
			EndIf
		EndIf
		If !lRes
			__oSt05_1:SetString(1, cFil)
			__oSt05_1:SetString(2, cMat)
			__oSt05_1:SetString(3, xFilial("SRY", cFil))
			__oSt05_1:SetString(4, cFil)
			__oSt05_1:SetString(5, cMat)
			If cOpcTab == "1"
				__oSt05_1:SetString(6, cFil)
				__oSt05_1:SetString(7, cMat)
				__oSt05_1:SetString(8, cFil)
				__oSt05_1:SetString(9, cMat)
			EndIf
		Else
			__oSt05_2:SetString(1, cFil)
			__oSt05_2:SetString(2, cMat)
			__oSt05_2:SetString(3, xFilial("SRY", cFil))
			__oSt05_2:SetString(4, cFil)
			__oSt05_2:SetString(5, cMat)
			__oSt05_2:SetString(6, cFil)
			__oSt05_2:SetString(7, cMat)
			__oSt05_2:SetString(8, cFil)
			__oSt05_2:SetString(9, cMat)
			__oSt05_2:SetString(10, cFil)
			__oSt05_2:SetString(11, cMat)
		EndIf
		If !lRes
			cQrySt := __oSt05_1:getFixQuery()
		Else
			cQrySt := __oSt05_2:getFixQuery()
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQrySt),cAliasSRX,.T.,.T.)

		For nx1 := 1 To len(aStru)
			If aStru[nX1][2] <> "C" .And. FieldPos(aStru[nX1][1])<>0
				TcSetField(cAliasSRX,aStru[nX1][1],aStru[nX1][2],aStru[nX1][3],aStru[nX1][4])
			EndIf
		Next nX1
		dbSelectArea(cAliasSRX)

		If (cAliasSRX)->(!Eof())
			If aScan( aContdmDev, { |x| x[1] == cFilEnv + ";" + (cAliasSRA)->RA_FILIAL+(cAliasSRA)->RA_MAT } ) == 0
				aAdd( aContdmDev, { cFilEnv + ";" + (cAliasSRA)->RA_FILIAL+(cAliasSRA)->RA_MAT } )
			EndIf
			While (cAliasSRX)->(!Eof())
				cTpRot := fGetTipoRot( (cAliasSRX)->RD_ROTEIR )

				// DESCONSIDERA OS ID�S DE DESONERA��O
				If !lRobo .And. RetValSrv((cAliasSRX)->RD_PD, (cAliasSRX)->RD_FILIAL, 'RV_CODFOL') $ "0148|0973"
					(cAliasSRX)->(DBSkip())
					Loop
				EndIf

				If lRes
					If !((cAliasSRX)->RD_PERIODO != cCompete .Or. (cAliasSRX)->RD_DATPGT == dDtRes)
						(cAliasSRX)->(DBSkip())
						Loop
					Endif
				Endif

				//Para competencia anterior a Dezembro, despreza todas as verbas do roteiro 132 que n�o seja a do l�quido (Id 0022)
				If ( SubStr(cCompete, 1, 2) != "12" .And. ( (cAliasSRX)->RD_ROTEIR == "132" .Or. cTpRot == "6" ) ) .And. (cAliasSRX)->RD_PD != cPdCod0021
					(cAliasSRX)->( dbSkip() )
					Loop
				EndIf

				If SubStr(cCompete, 1, 2) == "12" .And. ((cAliasSRX)->RD_ROTEIR == "132" .Or. cTpRot == "6")
					lTem132	:= .T.
				EndIf

				cFilx   	:= (cAliasSRX)->RD_FILIAL
				cPD     	:= (cAliasSRX)->RD_PD
				cCodFol 	:= RetValSrv( cPD, cFilx, 'RV_CODFOL' )
				cTipoCod 	:= RetValSrv( cPD, cFilx, 'RV_TIPOCOD' )
				cCodINCIRF 	:= RetValSrv( cPD, cFilx, 'RV_INCIRF' )
				If cVersEnvio < "2.6.00"
					cCodINCIRF := SubStr(cCodINCIRF, 1, 2)
				EndIf
				cCodAdiant 	:= RetValSrv( cPD, cFilx, 'RV_ADIANTA' )
				cCodFil 	:= RetValSrv( cPD, cFilx, 'RV_FILIAL' )
				lAchou063	:= .F.

				//Desprezar verbas geradas no fechamento dos roteiros ADI/FOL, desconto do arredondamento de f�rias/folha, dedu��o para f�rias/abono no S-1200, f�rias/abono pagos m�s anterior ou per�odo superior ao escolhido
				If cCodFol $ "0012/0106/0107/0105/1562/1722/1723/0044/0164/1449/0029/0300" .Or. (cAliasSRX)->RD_PERIODO > SubStr(cCompete,3,4)+SubStr(cCompete,1,2)
					If lRes .And. cTpRot == "4"
						dbSelectArea("SRG")
						SRG->( dbSetOrder(1) )
						If SRG->( dbSeek( cFilx + (cAliasSRX)->RD_MAT + dToS((cAliasSRX)->RD_DATPGT) ) )
							If AnoMes(SRG->RG_DATAHOM) > cAno+cMes
								lPgtRes := .F.
							EndIf
						EndIf
					EndIf
					(cAliasSRX)->( dbSkip() )
					Loop
				EndIf

				If lRes .And. !(cTpRot $ "4/6") // Se tiver rescis�o no m�s a data de pagamento ser� a data de pagamento do roteito para que os ideDmDev fiquem iguais no S-1200 e S-1210
					//Se for a verba de INSS Patronal e for no mesmo per�odo da rescis�o, significa que � a verba de desonera��o gerada para rescis�o
					If cCodFol == "0148" .And. cPerRes+cSemRes == (cAliasSRX)->RD_PERIODO+(cAliasSRX)->RD_SEMANA
						cTpRot  := "4"
						dDtPgt  := (cAliasSRX)->RD_DATPGT
					Else
						dDtPgt  := StoD(fVerDtPgto( dToS((cAliasSRX)->RD_DATPGT), , , cAliasSRX))
					EndIf
				Elseif lRes .And. cTpRot == "4"
					dbSelectArea("SRG")
					SRG->( dbSetOrder(1) )
					If SRG->( dbSeek( cFilx + (cAliasSRX)->RD_MAT + dToS((cAliasSRX)->RD_DATPGT) ) )
						dDtPgt 		:= SRG->RG_DATAHOM
						cTpRes 		:= SRG->RG_RESCDIS
						If Empty(cUltTpRes)
							cUltTpRes	:= SRG->RG_RESCDIS
							cDtGerRes	:= AnoMes(SRG->RG_DTGERAR)
						ElseIf SRG->RG_RESCDIS != cUltTpRes .And. cUltTpRes == "0" .And. SRG->RG_RESCDIS == "1" .And. AnoMes(SRG->RG_DTGERAR) > cDtGerRes
							cUltTpRes	:= SRG->RG_RESCDIS
							cDtGerRes	:= AnoMes(SRG->RG_DTGERAR)
							lRes		:= .F.							
							lResOriCom	:= .T.
						EndIf
						If AnoMes(dDtPgt) > cAno+cMes
							lPgtRes := .F.
							(cAliasSRX)->( dbSkip() )
							Loop
						EndIf
					EndIF
				else
					dDtPgt  := Iif(lRobo, (cAliasSRX)->RD_DATPGT, POSICIONE("SRR", 1, (cAliasSRX)->RD_FILIAL + (cAliasSRX)->RD_MAT, "RR_DATAPAG"))
				EndIf

				cPeriodo	:= (cAliasSRX)->RD_PERIODO
				cRoteiro	:= (cAliasSRX)->RD_ROTEIR
				cPer		:= substr(cPeriodo,1,4)+"-"+substr(cPeriodo,5,2)
				cPer132		:= substr(cPeriodo,1,4)
				dDtPgtRR 	:= dDtPgt
				nValor		:= (cAliasSRX)->RD_VALOR

				If lResOriCom .And. cTpRot == "4"
					If (cAliasSRA)->RA_CATFUNC $ "P*A"
						cTpRot 	:= "9"
						cRoteiro:= fGetCalcRot("9")
					Else
						cTpRot 	:= "1"
						cRoteiro:= fGetRotOrdinar()
					EndIf
				EndIf

				If lVer546 .And. (cRoteiro == "ADI" .Or. cTpRot == "2")
					lGera546	:= lTem546(cFil,cMat,cDtPesqI,cDtPesqF,cPdCod546)
					lVer546 	:= .F.
				EndIf

				If cTipoCod $ "2/4"
					nValor *= (-1)
				EndIf

				cPdLiq := cCodFol

				If cCodFol == "0066"//I.R.
					nValor *= (-1)
					SRD->( dbSetOrder(6) )//RD_FILIAL+RD_MAT+RD_PD+RD_ROTEIR+DTOS(RD_DATPGT)
					SRC->( dbSetOrder(8) )//RC_FILIAL+RC_MAT+RC_PD+RC_ROTEIR+DTOS(RC_DATA)
					For nContRot := 1 To Len(aRotADI)
						If SRD->( dbSeek( SRA->RA_FILIAL+SRA->RA_MAT+aCodFol[63, 1]+aRotADI[nContRot]+AnoMes(dDtPgt) ) )//Ded. Residuo IR pago 
							While SRD->( !EoF() ) .And. SRD->RD_FILIAL+SRD->RD_MAT+SRD->RD_PD+SRD->RD_ROTEIR+AnoMes(SRD->RD_DATPGT) == SRA->RA_FILIAL+SRA->RA_MAT+aCodFol[63, 1]+aRotADI[nContRot]+AnoMes(dDtPgt)
								nValor -= SRD->RD_VALOR
								If nValor <= 0							
									(cAliasSRX)->( dbSkip() )
									lAchou063 := .T.
									Exit
								EndIf
								SRD->( dbSkip() )
							EndDo
						EndIf
						If !lAchou063 .And. SRC->( dbSeek( SRA->RA_FILIAL+SRA->RA_MAT+aCodFol[63, 1]+aRotADI[nContRot]+AnoMes(dDtPgt) ) )//Ded. Residuo IR pago 
							While SRC->( !EoF() ) .And. SRC->RC_FILIAL+SRC->RC_MAT+SRC->RC_PD+SRC->RC_ROTEIR+AnoMes(SRC->RC_DATA) == SRA->RA_FILIAL+SRA->RA_MAT+aCodFol[63, 1]+aRotADI[nContRot]+AnoMes(dDtPgt)
								nValor -= SRC->RC_VALOR
								If nValor <= 0							
									(cAliasSRX)->( dbSkip() )
									lAchou063 := .T.
									Exit
								EndIf
								SRC->( dbSkip() )
							EndDo
						EndIf						
					Next nCont
					If !lAchou063
						For nContRot := 1 To Len(aRotFOL)
							If SRD->( dbSeek( SRA->RA_FILIAL+SRA->RA_MAT+aCodFol[63, 1]+aRotFOL[nContRot]+AnoMes(dDtPgt) ) )//Ded. Residuo IR pago 
								While SRD->( !EoF() ) .And. SRD->RD_FILIAL+SRD->RD_MAT+SRD->RD_PD+SRD->RD_ROTEIR+AnoMes(SRD->RD_DATPGT) == SRA->RA_FILIAL+SRA->RA_MAT+aCodFol[63, 1]+aRotFOL[nContRot]+AnoMes(dDtPgt)
									nValor -= SRD->RD_VALOR
									If nValor <= 0							
										(cAliasSRX)->( dbSkip() )
										lAchou063 := .T.
										Exit
									EndIf
									SRD->( dbSkip() )
								EndDo
							EndIf
							If !lAchou063 .And. SRC->( dbSeek( SRA->RA_FILIAL+SRA->RA_MAT+aCodFol[63, 1]+aRotFOL[nContRot]+AnoMes(dDtPgt) ) )//Ded. Residuo IR pago 
								While SRC->( !EoF() ) .And. SRC->RC_FILIAL+SRC->RC_MAT+SRC->RC_PD+SRC->RC_ROTEIR+AnoMes(SRC->RC_DATA) == SRA->RA_FILIAL+SRA->RA_MAT+aCodFol[63, 1]+aRotFOL[nContRot]+AnoMes(dDtPgt)
									nValor -= SRC->RC_VALOR
									If nValor <= 0							
										(cAliasSRX)->( dbSkip() )
										lAchou063 := .T.
										Exit
									EndIf
									SRC->( dbSkip() )
								EndDo
							EndIf	
						Next nCont
					EndIf
					If lAchou063
						Loop
					EndIf
					nValor *= (-1)
				EndIf
				
				If lRes .And. cTpRot $ "1/2/5/6/9/F" .And. cPerRes == cPeriodo .Or. cTpRot == "4"

					IF ( !Empty(dDcgIni) .And. MesAno(MonthSum(dDcgIni, IIF(Month(dDcgIni) == 3, 1,2)  )) == cPeriodo)
						cTpPgto := "9"

						IF cTpRot == "4"
							dbSelectArea("SRG")
							SRG->( dbSetOrder(1) )
							If SRG->( dbSeek( cFilx + (cAliasSRX)->RD_MAT + dToS(dDtPgt) ) )
								dDtPgt := SRG->RG_DATAHOM
							EndIF
						ENDIF
					ELSE
						If lTSV
							cTpPgto := "3"
						Else
							cTpPgto := "2"
						Endif
					ENDIF

					If cTpRot == "4"
						If dDtPgt != dlastDate .Or. (cTpRot != cLastRot .And. Left(cideDmDev, 1) != "R")
							If cTpRes == "3" .And. Empty(nContRes)
								++nContRes
							EndIf
							cideDmDev := "R" + cEmpAnt + Alltrim(xFilial("SRA", (cAliasSRA)->RA_FILIAL)) + (cAliasSRA)->RA_MAT + If(cTpRes == "3", "C", "") + If(Empty(nContRes), (++nContRes, ""), cValToChar(nContRes++))
						EndIf
					Else
						//O ADI deve ser gerado com o cTpPgto 2 e o ID da DmDev deve ficar igual ao que foi gerado para o ADI no desligamento
						cideDmDev := cFilx + dtos(dDtPgt) + cPeriodo + cRoteiro
					EndIf
					cLastRot  := cTpRot
					dlastDate := dDtPgt
				Else
					If ( !Empty(dDcgIni) .And. MesAno(MonthSum(dDcgIni, IIF(Month(dDcgIni) == 3, 1,2)  )) == cPeriodo .And. !(StrZero(Month(dDtPgt),2) == Substr(cPeriodo,5,2)) )
						cTpPgto := "9"
					Else
						cTpPgto := "1"
					EndIf
					If lAglut .And. ((cAliasSRA)->RA_OCORREN $ "05/06/07/08" .Or. nQtdeFol > 1)//MultV
						If nQtdeFol > 1 .And. cPeriodo != cPerOld
							cPerOld	 	:= cPeriodo
							nQtdeFolMV 	:= fQtdeFolMV(cOpcTab, cPeriodo, cDtPesqI, cDtPesqF)
						EndIf
						If (cAliasSRA)->RA_OCORREN $ "05/06/07/08" .Or. nQtdeFolMV > 1
							cideDmDev := cFilx + (cAliasSRX)->RD_MAT + SubStr( dtos(dDtPgt), 3 ) + SubStr( cPeriodo, 3 ) + cRoteiro
							If Len(cideDmDev) > 30
								cideDmDev := cFilx + (cAliasSRX)->RD_MAT + SubStr( dtos(dDtPgt), 7 ) + fGetTipoRot( cRoteiro )
							EndIf
						Else
							cideDmDev := cFilx + dtos(dDtPgt) + cPeriodo + cRoteiro
						EndIf
					ElseIf  lResComPLR .And. lTem546(cFil,cMat,cDtPesqI,cDtPesqF,cPdCod0151)
						cideDmDev := cFilx + fVerDtPgto( dToS((cAliasSRX)->RD_DATPGT), , , cAliasSRX) + cPeriodo + cRoteiro
					Else
						cideDmDev := cFilx + dtos(dDtPgt) + cPeriodo + cRoteiro
					EndIf
				Endif

				nPInfoPgto := aScan( aInfoPgto, { |x| x[1] == dtos(dDtPgt) .And. x[2] == cTpPgto .And. x[5] == (cAliasSRA)->RA_CIC } )
				If nPInfoPgto == 0
					If Left(cideDmDev, 1) == "R"
						dbSelectArea("SRG")
						SRG->( dbSetOrder(1) )
						If SRG->( dbSeek( cFilx + (cAliasSRX)->RD_MAT + dToS(dDtPgt) ) )
							dDtPgtRR := SRG->RG_DATAHOM
						EndIF
					EndIf
					aAdd( aInfoPgto, { dtos(dDtPgt), cTpPgto ,If(lResidExt, "N", "S"), "1", (cAliasSRA)->RA_CIC } )
				EndIf
				nPdetPGtoFl := aScan( adetPgtoFl, { |x| x[7] == dToS(dDtPgt) + cTpPgto .And. x[2] == cideDmDev .And. x[8] == (cAliasSRA)->RA_CIC } )
				If nPdetPGtoFl == 0
					aAdd( adetPgtoFl, { If(cTpPgto $ "2/3" .Or. (cRoteiro == "RES" .Or. cTpRot == "4"), Iif(!lMiddleware, "", Nil), If(SubStr(cCompete, 1, 2) == "12" .And. (cRoteiro == "132" .Or. cTpRot == "6"), cPer132, cPer)), cideDmDev, "S", 0, Iif(lMiddleware .And. (cTpPgto $ "2/3" .Or. cRoteiro == "RES" .Or. cTpRot == "4"), cRecResc, Nil), "N", dToS(dDtPgt) + cTpPgto, (cAliasSRA)->RA_CIC, (cAliasSRA)->RA_MAT + " - " + (cAliasSRA)->RA_CODUNIC, (cAliasSRA)->RA_NOMECMP, (cAliasSRA)->RA_FILIAL } )
				EndIf

				nPdetPGtoFl := aScan( adetPgtoFl, { |x| x[7] == dtos(dDtPgt) + cTpPgto .And. x[2] == cideDmDev .And. x[8] == (cAliasSRA)->RA_CIC } )
				If nPdetPGtoFl > 0
					If cPdLiq == "0126" .Or. (!(cTpPgto $ "2/3") .And. cPdLiq == "0303") .Or. (cPdLiq == "0021" .And. (cRoteiro == "132" .Or. cTpRot == "6")) .Or. (cPdLiq == "0678" .And. (cRoteiro == "131" .Or. cTpRot == "5"))
						adetPgtoFl[nPdetPGtoFl, 4] += (cAliasSRX)->RD_VALOR
						lPgtRes := .F.
					Elseif ((lGera546 .And. (cRoteiro == "ADI" .Or. cTpRot == "2")) .Or. !(cRoteiro == "ADI" .Or. cTpRot == "2")) .And. ((cPdLiq == "0047" .And. cTpRot != "N" ) .Or. cPdLiq == "0836" .Or. ( cPdLiq == "0546" .And. (cRoteiro == "ADI" .Or. cTpRot == "2") ) ) //proteger, pois pode ter iniciado e varrer dentre os registros SRD�s que nao sejam 0047
						adetPgtoFl[nPdetPGtoFl, 4] += (cAliasSRX)->RD_VALOR
						If cPdLiq == "0836"
							lPgtRes := .F.
						EndIf
					Elseif !lGera546 .And. (cRoteiro == "ADI" .Or. cTpRot == "2") .And. cCodAdiant == "S" .And. cPdLiq != "0151" .And. (cRoteiro == "ADI" .Or. cTpRot == "2")
						If cTipoCod  == '1'
							adetPgtoFl[nPdetPGtoFl, 4] += (cAliasSRX)->RD_VALOR
						ElseIf cTipoCod  == '2'
							adetPgtoFl[nPdetPGtoFl, 4] -= (cAliasSRX)->RD_VALOR
						EndIf
						lPgtRes := .F.
					ElseIf lRes .And. (cRoteiro == "RES" .Or. cTpRot == "4") .And. AnoMes(dDtRes) > cAno+cMes
						lPgtRes := .F.
					EndIf
					If !Empty((cAliasSRX)->RD_IDCMPL)
						adetPgtoFl[nPdetPGtoFl, 6] := "S"
					EndIf
				EndIf

				If !Empty(xFilial("SRV"))
					lGeraCod := .T.
				EndIf

				If !( !Empty(dDcgIni) .And. MesAno(MonthSum(dDcgIni,IIF(Month(dDcgIni) == 3, 1,2) )) == cPeriodo .And. !(StrZero(Month(dDtPgt),2) == Substr(cPeriodo,5,2)) ) .And.;
					( ( cVersEnvio < "2.6.00" .And. cCodINCIRF $ '31|32|33|34|35|51|52|53|54|55|81|82|83' ) .Or.;
					( cVersEnvio >= "2.6.00" .And. fRetTpIRF(cCodINCIRF) $ "D/I/J" ) )
					If Left(cideDmDev, 1) == "R" .And. nContRes > 1
						aEval( aRetPgtoTot, { |x| If(x[1] == cPd .And. Left(x[7], 1) == "R", nValor -= x[6], Nil) }  )
						If cTipoCod $ "2/4" .And. Abs(nValor) <= 0 .Or. !(cTipoCod $ "2/4") .And. nValor <= 0
							(cAliasSRX)->(dbSkip())
							Loop
						EndIf
					EndIf
					nPos := aScan(aRetPgtoTot, {|x| x[1] == cPd .And. x[7] == cideDmDev .And. x[9] == (cAliasSRA)->RA_CIC })
					If lGeraCod
						cIdTbRub := cCodFil
					Else
						If cVersEnvio >= "2.3"
							cIdTbRub := cEmpAnt
						Else
							cIdTbRub := ""
						EndIf
					EndIf

					If lMiddleware
						If lPrimIdT
							lPrimIdT  := .F.
							cIdeRubr := fGetIdRJF( cCodFil, cIdTbRub )
							If Empty(cIdeRubr) .And. aScan(aErros, { |x| x == OemToAnsi(STR0156) + cIdTbRub + OemToAnsi(STR0157) }) == 0
								aAdd( aErros, OemToAnsi(STR0156) + cIdTbRub + OemToAnsi(STR0157) )//"Aten��o"##"N�o ser� poss�vel efetuar a integra��o. O identificador de tabela de rubrica do c�digo: "##" n�o est� cadastrado."
							EndIf
						EndIf
						cIdTbRub := cIdeRubr
					EndIf

					If nPos == 0
						Aadd (aRetPgtoTot, {cPd, cIdTbRub, Nil, Nil, Nil, nValor, cideDmDev, (cCodFil+cPd), (cAliasSRA)->RA_CIC} )
					else
						aRetPgtoTot[nPos,6] += nValor
					Endif

					If (VAL(cCodINCIRF ) >=  51 .And. VAL(cCodINCIRF ) <= 55) .Or. (cVersEnvio >= "2.6.00" .And. (VAL(cCodINCIRF ) >= 9051 .And. VAL(cCodINCIRF ) <= 9054) )
						fBenefic( (cAliasSRA)->RA_FILIAL, (cAliasSRA)->RA_MAT, dDtPgt, cPd, nValor, cideDmDev, cPeriodo )
					EndIf
					If cVersEnvio >= "2.6.00" .And. cCodINCIRF $ "67  /9067" .And. cPeriodo >= "202001" .And. !lCarrDep
						aInfoSaude := fGetAssMed( (cAliasSRA)->RA_FILIAL, (cAliasSRA)->RA_MAT, cVersEnvio, cPeriodo, cOpcTab, @lCPFDepOk, @aNomes, cideDmDev)
						If !lCPFDepOk
							aAdd(aDepAgreg, OemToAnsi(STR0025) + (cAliasSRA)->RA_CIC  + " - " + Alltrim((cAliasSRA)->RA_NOME) + " " + OemToAnsi(STR0029) )//"Registro S-1210 do Funcion�rio: "##" n�o foi integrado devido ao(s) erro(s) abaixo: "

							aAdd(aDepAgreg, OemToAnsi(STR0055))//"O(s) dependente(s)/agregado(s) de plano de sa�de abaixo n�o tem CPF cadastrado:"
							For nC := 1 To Len(aNomes)
								aAdd(aDepAgreg, aNomes[nC])
							Next nC
							aAdd(aDepAgreg, OemToAnsi(STR0056))//"Verifique as tabelas SRB - Dependentes e RHM - Agregados"
						EndIf
						lCarrDep := .T.
					EndIf
					If cVersEnvio >= "2.6.00" .And. cCodINCIRF $ "46  /47  /48  /61  /62  /63  /64  /65  /66  /9046/9047/9048/9061/9062/9063/9064/9065/9066" .And. cPeriodo >= "202001"
						aInfoPrev := fGetPrev( cPd, cPeriodo, cideDmDev)
					EndIf
				EndIf

				//Verifica se o pagamento eh referente a periodos anteriores com base na data da Carga Inicial
				//A competencia deve ser 2 meses apos o periodo da carga inicial (inicio da obrigatoriedade)
				//------------------------------------------------------------------------
				//OBS.: Esse dado sera gerado somente no primeiro mes para empresas que trabalham em regime caixa.
				//Ent�o futuramente deve ser retirado da rotina evitar validacoes desnecessarias e melhor performance
				//------------------------------------------------------------------------
				//Exemplo: 	Carga inicial = 201803 ## Folha de pagamento = 201804 com data de pagamento = 05/05/2018
				//			Entao para geracao � informado 05/2018 que refere-se a competencia 201804 e atende a condicao abaixo.
				//------------------------------------------------------------------------
				If ( !Empty(dDcgIni) .And. MesAno(MonthSum(dDcgIni,IIF(Month(dDcgIni) == 3, 1,2) )) == cPeriodo .And. !(StrZero(Month(dDtPgt),2) == Substr(cPeriodo,5,2)) )
					//Altera��o para NDE 03/2019 pendente
					If ( ( cVersEnvio < "2.6.00" .And. cCodINCIRF  $ '00|01|09|11|12|13|14|15|31|32|33|34|35|41|42|43|44|46|47|51|52|53|54|55|61|62|63|64|70|71|72|73|74|75|76|77|78|79|81|82|83|91|92|93|94|95' ) .Or.;
						( cVersEnvio >= "2.6.00" ) )

						If lGeraCod
							cIdTbRub := cCodFil
						Else
							If cVersEnvio >= "2.3"
								cIdTbRub := cEmpAnt
							Else
								cIdTbRub := ""
							EndIf
						EndIf

						nPdetPGtoAt := aScan( aDetPgtoAt, { |x| x[3] == dToS(dDtPgt) + "9" .And. x[1] == cCat} )
						If nPdetPGtoAt == 0
							aAdd( aDetPgtoAt, { cCat, Iif(cVersEnvio >= "2.6.00" .And. !Empty((cAliasSRX)->RD_IDCMPL), "S", "N"), dToS(dDtPgt) + "9" } )
						ElseIf cVersEnvio >= "2.6.00" .And. !Empty((cAliasSRX)->RD_IDCMPL)
							aDetPgtoAt[nPdetPGtoAt, 2] := "S"
						EndIf

						If cVersEnvio < "2.6.00"
							nPos := aScan( aPgtoAnt, {|X| x[7] == dToS(dDtPgt) + "9" .And. x[5] == SubStr(cCodINCIRF, 1, 2) })
							If nPos > 0
								aPgtoAnt[nPos, 6] += nValor
							Else
								aAdd( aPgtoAnt, { Nil, Nil, Nil, Nil, SubStr(cCodINCIRF, 1, 2), nValor, dToS(dDtPgt) + "9" } )
							EndIf
						Else
							nPos := aScan( aPgtoAnt, {|X| x[7] == dToS(dDtPgt) + "9" .And. x[1] == cPd })
							If nPos > 0
								aPgtoAnt[nPos, 3] += nValor
							Else
								aAdd( aPgtoAnt, { cPd, cIdTbRub, nValor, Nil, Nil, Nil, dToS(dDtPgt) + "9" } )
							EndIf
						EndIf

						If cVersEnvio >= "2.6.00" .And. ( (Val(cCodINCIRF ) >=  51 .And. Val(cCodINCIRF ) <= 55) .Or. (Val(cCodINCIRF ) >= 9051 .And. Val(cCodINCIRF ) <= 9054) )
							fBenefic( (cAliasSRA)->RA_FILIAL, (cAliasSRA)->RA_MAT, dDtPgt, cPd, nValor, dToS(dDtPgt) + "9", cPeriodo )
						EndIf
						If cVersEnvio >= "2.6.00" .And. cCodINCIRF $ "67  /9067" .And. cPeriodo >= "202001" .And. !lCarrDep
							aInfoSaude := fGetAssMed( (cAliasSRA)->RA_FILIAL, (cAliasSRA)->RA_MAT, cVersEnvio, cPeriodo, cOpcTab, @lCPFDepOk, @aNomes, dToS(dDtPgt) + "9")
							If !lCPFDepOk
								aAdd(aDepAgreg, OemToAnsi(STR0025) + (cAliasSRA)->RA_CIC  + " - " + Alltrim((cAliasSRA)->RA_NOME) + " " + OemToAnsi(STR0029) )//"Registro S-1210 do Funcion�rio: "##" n�o foi integrado devido ao(s) erro(s) abaixo: "

								aAdd(aDepAgreg, OemToAnsi(STR0055))//"O(s) dependente(s)/agregado(s) de plano de sa�de abaixo n�o tem CPF cadastrado:"
								For nC := 1 To Len(aNomes)
									aAdd(aDepAgreg, aNomes[nC])
								Next nC
								aAdd(aDepAgreg, OemToAnsi(STR0056))//"Verifique as tabelas SRB - Dependentes e RHM - Agregados"
							EndIf
							lCarrDep := .T.
						EndIf
						If cVersEnvio >= "2.6.00" .And. cCodINCIRF $ "46  /47  /48  /61  /62  /63  /64  /65  /66  /9046/9047/9048/9061/9062/9063/9064/9065/9066" .And. cPeriodo >= "202001"
							aInfoPrev := fGetPrev( cPd, cPeriodo, dToS(dDtPgt) + "9")
						EndIf

					EndIf
				EndIf

				(cAliasSRX)->(dbSkip())
			EndDo//cAliasSRX
		EndIf

		If SubStr(cCompete, 1, 2) == "12" .And. !lTem132
			cDtIni := SubStr(cCompete,3,4)+"0101"
			cDtFim := SubStr(cCompete,3,4)+"1130"
			If Select( cAliasSRX ) > 0
				(cAliasSRX)->( dbCloseArea() )
			EndIf

			If __oSt06 == Nil
				__oSt06 := FWPreparedStatement():New()
				cQrySt := "SELECT SRD.RD_FILIAL, SRD.RD_MAT, SRD.RD_DATPGT, SRD.RD_SEMANA, SRD.RD_PD, SRD.RD_SEQ, SRD.RD_CC, SRD.RD_PERIODO, SRD.RD_ROTEIR, SRD.RD_VALOR, 'SRD' AS TAB, SRD.R_E_C_N_O_  AS RECNO "
				cQrySt += "FROM " + RetSqlName('SRD') + " SRD "
				cQrySt += "INNER JOIN " + RetSqlName('SRY') + " SRY ON " + cJoinRDxRy + " AND SRD.RD_ROTEIR = SRY.RY_CALCULO "
				cQrySt += "WHERE SRD.RD_FILIAL = ? AND "
				cQrySt += 		"SRD.RD_MAT = ? AND "
				cQrySt += 		"SRD.RD_DATPGT BETWEEN '" + cDtIni + "' AND '" + cDtFim + "' AND "
				cQrySt += 		"SRD.RD_EMPRESA = '  ' AND "
				cQrySt += 		"SRY.RY_TIPO = '6' AND "
				cQrySt += 		"SRD.D_E_L_E_T_ = ' ' AND "
				cQrySt += 		"SRY.D_E_L_E_T_ = ' ' "
				If cOpcTab == "1"
					cQrySt += "UNION ALL "
					cQrySt += "SELECT SRC.RC_FILIAL, SRC.RC_MAT, SRC.RC_DATA, SRC.RC_SEMANA, SRC.RC_PD, SRC.RC_SEQ, SRC.RC_CC, SRC.RC_PERIODO, SRC.RC_ROTEIR, SRC.RC_VALOR, 'SRC' AS TAB, SRC.R_E_C_N_O_  AS RECNO "
					cQrySt += "FROM " + RetSqlName('SRC') + " SRC "
					cQrySt += "INNER JOIN " + RetSqlName('SRY') + " SRY ON " + cJoinRCxRY + " AND SRC.RC_ROTEIR = SRY.RY_CALCULO "
					cQrySt += "WHERE SRC.RC_FILIAL = ? AND "
					cQrySt += 		"SRC.RC_MAT = ? AND "
					cQrySt += 		"SRC.RC_DATA BETWEEN '" + cDtIni + "' AND '" + cDtFim + "' AND "
					cQrySt += 		"SRY.RY_TIPO = '6' AND "
					cQrySt += 		"SRC.D_E_L_E_T_ = ' ' AND "
					cQrySt += 		"SRY.D_E_L_E_T_ = ' ' "
				EndIf
				cQrySt += "ORDER BY 1, 2, 8, 9, 3, 5"
				cQrySt := ChangeQuery(cQrySt)
				__oSt06:SetQuery(cQrySt)
			EndIf
			__oSt06:SetString(1, cFil)
			__oSt06:SetString(2, cMat)
			If cOpcTab == "1"
				__oSt06:SetString(3, cFil)
				__oSt06:SetString(4, cMat)
			EndIf
			cQrySt := __oSt06:getFixQuery()
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQrySt),cAliasSRX,.T.,.T.)

			TCSetField( cAliasSRX, "RD_DATPGT", "D", 8, 0 )

			If (cAliasSRX)->(!Eof())
				If aScan( aContdmDev, { |x| x[1] == cFilEnv + ";" + (cAliasSRA)->RA_FILIAL+(cAliasSRA)->RA_MAT } ) == 0
					aAdd( aContdmDev, { cFilEnv + ";" + (cAliasSRA)->RA_FILIAL+(cAliasSRA)->RA_MAT } )
				EndIf
				While (cAliasSRX)->(!Eof())
					cFilx   	:= (cAliasSRX)->RD_FILIAL
					cPD     	:= (cAliasSRX)->RD_PD
					dDtPgt  	:= sToD(SubStr(cCompete,3,4)+"1201")
					cPeriodo	:= (cAliasSRX)->RD_PERIODO
					cRoteiro	:= (cAliasSRX)->RD_ROTEIR
					cPer		:= substr(cPeriodo,1,4)

					cCodFol 	:= RetValSrv( cPD, cFilx, 'RV_CODFOL' )
					cTipoCod 	:= RetValSrv( cPD, cFilx, 'RV_TIPOCOD' )
					cCodINCIRF 	:= RetValSrv( cPD, cFilx, 'RV_INCIRF' )
					If cVersEnvio < "2.6.00"
						cCodINCIRF := SubStr(cCodINCIRF, 1, 2)
					EndIf
					cCodAdiant 	:= RetValSrv( cPD, cFilx, 'RV_ADIANTA' )
					cCodFil 	:= RetValSrv( cPD, cFilx, 'RV_FILIAL' )

					nValor	:= (cAliasSRX)->RD_VALOR
					If cTipoCod $ "2/4"
						nValor *= (-1)
					EndIf

					cPdLiq := cCodFol

					cideDmDev := cFilx + dtos((cAliasSRX)->RD_DATPGT) + cPeriodo + cRoteiro

					nPInfoPgto := aScan( aInfoPgto, { |x| x[1] == dtos(dDtPgt) .And. x[2] == cTpPgto .And. x[5] == (cAliasSRA)->RA_CIC } )
					If nPInfoPgto == 0
						aAdd( aInfoPgto, { dtos(dDtPgt), cTpPgto ,If(lResidExt, "N", "S"), "1", (cAliasSRA)->RA_CIC } )
					EndIf

					nPdetPGtoFl := aScan( adetPgtoFl, { |x| x[7] == dToS(dDtPgt) + cTpPgto .And. x[2] == cideDmDev .And. x[8] == (cAliasSRA)->RA_CIC } )
					If nPdetPGtoFl == 0
						aAdd( adetPgtoFl, { cPer, cideDmDev, "S", 0, Nil, "N", dToS(dDtPgt) + cTpPgto, (cAliasSRA)->RA_CIC, (cAliasSRA)->RA_MAT + " - " + (cAliasSRA)->RA_CODUNIC, (cAliasSRA)->RA_NOMECMP, (cAliasSRA)->RA_FILIAL } )
					EndIf

					If ( ( cVersEnvio < "2.6.00" .And. cCodINCIRF $ '31|32|33|34|35|51|52|53|54|55|81|82|83' ) .Or.;
						( cVersEnvio >= "2.6.00" .And. fRetTpIRF(cCodINCIRF) $ "D/I/J" ) )
						nPos := aScan(aRetPgtoTot, {|x| x[1] == cPd .And. x[7] == cideDmDev .And. x[9] == (cAliasSRA)->RA_CIC })
						If lGeraCod
							cIdTbRub := cCodFil
						Else
							If cVersEnvio >= "2.3"
								cIdTbRub := cEmpAnt
							Else
								cIdTbRub := ""
							EndIf
						EndIf

						If lMiddleware
							If lPrimIdT
								lPrimIdT  := .F.
								cIdeRubr := fGetIdRJF( cCodFil, cIdTbRub )
								If Empty(cIdeRubr) .And. aScan(aErros, { |x| x == OemToAnsi(STR0156) + cIdTbRub + OemToAnsi(STR0157) }) == 0
									aAdd( aErros, OemToAnsi(STR0156) + cIdTbRub + OemToAnsi(STR0157) )//"Aten��o"##"N�o ser� poss�vel efetuar a integra��o. O identificador de tabela de rubrica do c�digo: "##" n�o est� cadastrado."
								EndIf
							EndIf
							cIdTbRub := cIdeRubr
						EndIf

						If nPos == 0
							Aadd (aRetPgtoTot, {cPd, cIdTbRub, Nil, Nil, Nil, nValor, cideDmDev, (cCodFil+cPd), (cAliasSRA)->RA_CIC} )
						else
							aRetPgtoTot[nPos,6] += nValor
						Endif

						If (VAL(cCodINCIRF ) >=  51 .AND. VAL(cCodINCIRF ) <= 55)
							fBenefic( (cAliasSRA)->RA_FILIAL, (cAliasSRA)->RA_MAT, dDtPgt, cPd, nValor, cideDmDev, cPeriodo )
						EndIf
					EndIf

					(cAliasSRX)->(dbSkip())
				EndDo
			EndIf
		EndIf

		//Para funcionarios que n�o possuem a categoria de contrato intermitente
		If (cAliasSRA)->RA_CATEFD <> "111"   
			//Tratamento para buscar os valores de ferias
			aBkpFer := fm036GetFer( cSRHFil, cSRHMat, cDtPesqI, cDtPesqF, (cAliasSRA)->RA_CATEFD, @aVbFer, @aErros, cFilPreTrf )
		Endif	

		For nCntFer := 1 To Len(aBkpFer)
			aAdd( aRetFer, aClone( aBkpFer[nCntFer] ) )
			nPInfoPgto := aScan( aInfoPgto, { |x| x[1] == aBkpFer[nCntFer, 6] .And. x[2] == "7" .And. x[5] == (cAliasSRA)->RA_CIC } )
			If nPInfoPgto == 0
				aAdd( aInfoPgto, { aBkpFer[nCntFer, 6], "7", If(lResidExt, "N", "S"), "1", (cAliasSRA)->RA_CIC } )
			EndIf
		Next

		//Se for residente no exterior busca alimenta o array aRGE1210 com os dados da tabela RGE
		If lResidExt
			aRGE1210 := fGetRGE( (cAliasSRA)->RA_FILIAL, (cAliasSRA)->RA_MAT, cCompete)
		Endif

		(cAliasQSRA)->( dbSkip() )
		(cAliasSRA)->( dbGoTo( (cAliasQSRA)->RECNO ) )

		If ( nPosEmp := aScan( aSM0, { |x| x[1] + x[2] == cEmpAnt + (cAliasQSRA)->RC_FILIAL } ) ) > 0
			cEmp 	:= SubStr(aSM0[nPosEmp, 18], 1, 8)
		EndIf

		If (cAliasQSRA)->(!Eof())
			If !lRelat .And. IIF(cVersEnvio >= "2.6.00", lCPFDepOk, .T. ) .And. ( !lAglut .Or. nQtdeFol == 1 .Or. cCPF <> AllTrim((cAliasSRA)->RA_CIC) .Or. (cEmp != cEmpOld) )
				If (Empty(aLogCIC) .OR. cVersEnvio < "2.5.00")
					lRetXml := fXml1210(@cXml, lRes, lPgtRes, aRGE1210, lAfast, cCompete, cRetfNew, cRecibo, cIdXml, cVersMw, @aErros, cPer1210, cFilEnv, lAdmPubl, cTpInsc, cNrInsc, cCPF, nVlrDep)
					If lRetXml
						If !lMiddleware .And. cStatT3P $ "4"
							cXml := StrTran(cXml, "<indRetif>1</indRetif>", "<indRetif>2</indRetif>")
						EndIf
						cBkpFilEnv	:= cFilEnv
						If lAglut
							If Len(aContdmDev) > 1
								cFilEnv := cFilAnt := fVldMatriz(cFilEnv, cPer1210)
							ElseIf !Empty(aContdmDev)
								cFilEnv := cFilAnt := SubStr( aContdmDev[1,1], 1, FwSizeFilial() )
							EndIf
						EndIf
						If !lMiddleware
							cTafKey := "S1210" + cPeriodo + cTpFolha + cCPF
							SM0->(dbSeek( cEmpAnt + cFilEnv ))
							aErros := TafPrepInt( cEmpAnt, cFilEnv, cXml, cTafKey , "3", "S1210", , "", , , , "GPE", , "", If(nQtdeFol > 1, "MV", ""),,,,,,,cSRHMat )
						Else
							aAdd( aDados, { xFilial("RJE", cFilEnv), cFilEnv, cTpInsc, Iif( cTpInsc == "1" .And. !lAdmPubl, SubStr(cNrInsc, 1, 8), cNrInsc ), "S1210", cPer1210, cKeyMid, cIdXml, cRetfNew, "12", cStatNew, Date(), Time(), cOperNew, NIL, cRecibo } )
							If !(fGravaRJE( aDados, cXML, lNovoRJE, nRecEvt ))
								aAdd( aErros, OemToAnsi(STR0155) )//"Ocorreu um erro na grava��o do registro na tabela RJE"
							EndIf
						EndIf
						If Len( aErros ) > 0
							If !lMiddleware
								FeSoc2Err( aErros[1], @cMsgErro)
							Else
								For nCont := 1 To Len(aErros)
									cMsgErro += aErros[nCont] + Chr(13) + Chr(10)
								Next nCont
							EndIf
							aAdd(aLogsErr, OemToAnsi(STR0025) + cCPF + " - " + cNome + OemToAnsi(STR0029) ) //##"Registro S-1210 do Funcion�rio: "##" n�o foi integrado devido ao(s) erro(s) abaixo: "
							aAdd(aLogsErr, "" )
							aAdd(aLogsErr, cMsgErro)
							If lTSV
								aEmp_1210[4]++ //Inclui TSV nao integrado
							Else
								aEmp_1210[2]++ //Inclui TCV nao integrado
							EndIf
						Else
							If !lMiddleware
								aAdd(aLogsOk, OemToAnsi(STR0025) + cCPF + " - " + cNome + OemToAnsi(STR0049) ) //##"Registro S-1210 do Funcion�rio: "##" Integrado com TAF."
							Else
								aAdd(aLogsOk, OemToAnsi(STR0025) + cCPF + " - " + cNome + OemToAnsi(STR0165) ) //##"Registro S-1210 do Funcion�rio: "##" Integrado com Middleware."
							EndIf
							aAdd(aLogsOk, "" )
							If lTSV
								aEmp_1210[3]++ //Inclui TSV integrado
							Else
								aEmp_1210[1]++ //Inclui TCV integrado
							EndIf
						Endif
						cFilEnv		:= cBkpFilEnv
						aInfoPgto	:= {}
						aRetFer		:= {}
						GrvTxtArq(alltrim(cXml), "S1210", cCPF)
					Else
						//"[AVISO]"##"Registro S-1210 do Funcion�rio: "##" n�o foi integrado devido ao(s) erro(s) abaixo: "##"Funcion�rio n�o possui movimento"
						aAdd(aLogsErr, OemToAnsi(STR0148) + OemToAnsi(STR0025) + cCPF + " - " + cNome + OemToAnsi(STR0029) + CRLF + OemToAnsi(STR0180) + CRLF)
						If lTSV
							aEmp_1210[4]++ //Inclui TSV nao integrado
						Else
							aEmp_1210[2]++ //Inclui TCV nao integrado
						EndIf
					EndIf
				Else
					If !Empty(aLogCIC) .AND. cVersEnvio >= "2.5.00"
						//"[AVISO]"##"Registro S-1210 do Funcion�rio: "##" n�o foi integrado devido ao(s) erro(s) abaixo: "##" - Benefici�rio(s) de Pens�o Aliment�cia SEM CPF cadastrado. Benefici�rio deve ser um CPF v�lido e diferente do CPF do trabalhador."
						aAdd(aLogsErr, OemToAnsi(STR0148) + OemToAnsi(STR0025) + cCPF + " - " + cNome + OemToAnsi(STR0029) + CRLF + OemToAnsi(STR0149) + CRLF)
					EndIf
					If lTSV
						aEmp_1210[4]++ //Inclui TSV nao integrado
					Else
						aEmp_1210[2]++ //Inclui TCV nao integrado
					EndIf
				EndIf
			ElseIf lRelat .And. !Empty(aLogCIC)
				aAdd( aRelIncons, { cOldFil, cCPF, OemToAnsi(STR0149)} )//" - Benefici�rio(s) de Pens�o Aliment�cia SEM CPF cadastrado. Benefici�rio deve ser um CPF v�lido e diferente do CPF do trabalhador."
			ElseIf cVersEnvio >= "2.6.00" .And. !lCPFDepOk
				For nCont := 1 To Len(aDepAgreg)
					aAdd(aLogsErr, aDepAgreg[nCont] )
				Next nCont
				aAdd(aLogsErr, "" )
				If lTSV
					aEmp_1210[4]++ //Inclui TSV nao integrado
				Else
					aEmp_1210[2]++ //Inclui TCV nao integrado
				EndIf
			EndIf
		EndIf
	EndDo //SRA

	If !lExcLote .And. ( !Empty(aInfoPgto) .Or. !Empty(aRetFer) )
		If !lRelat .And. IIF(cVersEnvio >= "2.6.00", lCPFDepOk, .T. )
			If (Empty(aLogCIC) .OR. cVersEnvio < "2.5.00")
				lRetXml := fXml1210(@cXml, lRes, lPgtRes, aRGE1210, lAfast, cCompete, cRetfNew, cRecibo, cIdXml, cVersMw, @aErros, cPer1210, cFilEnv, lAdmPubl, cTpInsc, cNrInsc, cCPF, nVlrDep)
				If lRetXml
					If !lMiddleware .And. cStatT3P $ "4"
						cXml := StrTran(cXml, "<indRetif>1</indRetif>", "<indRetif>2</indRetif>")
					EndIf
					If lAglut
						If Len(aContdmDev) > 1
							cFilEnv := cFilAnt := fVldMatriz(cFilEnv, cPer1210)
						ElseIf !Empty(aContdmDev)
							cFilEnv := cFilAnt := SubStr( aContdmDev[1,1], 1, FwSizeFilial() )
						EndIf
					EndIf
					If !lMiddleware
						cTafKey := "S1210" + cPeriodo + cTpFolha + cCPF
						SM0->(dbSeek( cEmpAnt + cFilEnv ))
						aErros := TafPrepInt( cEmpAnt, cFilEnv, cXml, cTafKey , "3", "S1210", , "", , , , "GPE", , "", If(nQtdeFol > 1, "MV", "") ,,,,,,,cSRHMat )
					Else
						aAdd( aDados, { xFilial("RJE", cFilEnv), cFilEnv, cTpInsc, Iif( cTpInsc == "1" .And. !lAdmPubl, SubStr(cNrInsc, 1, 8), cNrInsc ), "S1210", cPer1210, cKeyMid, cIdXml, cRetfNew, "12", cStatNew, Date(), Time(), cOperNew, Nil, cRecibo } )
						If !(fGravaRJE( aDados, cXML, lNovoRJE, nRecEvt ))
							aAdd( aErros, OemToAnsi(STR0155) )//"Ocorreu um erro na grava��o do registro na tabela RJE"
						EndIf
					EndIf
					If Len( aErros ) > 0
						If !lMiddleware
							FeSoc2Err( aErros[1], @cMsgErro)
						Else
							For nCont := 1 To Len(aErros)
								cMsgErro += aErros[nCont] + Chr(13) + Chr(10)
							Next nCont
						EndIf
						aAdd(aLogsErr, OemToAnsi(STR0025) + cCPF + " - " + cNome + OemToAnsi(STR0029) ) //##"Registro S-1210 do Funcion�rio: "##" n�o foi integrado devido ao(s) erro(s) abaixo: "
						aAdd(aLogsErr, "" )
						aAdd(aLogsErr, cMsgErro)
						If lTSV
							aEmp_1210[4]++ //Inclui TSV nao integrado
						Else
							aEmp_1210[2]++ //Inclui TCV nao integrado
						EndIf
					Else
						If !lMiddleware
							aAdd(aLogsOk, OemToAnsi(STR0025) + cCPF + " - " + cNome + OemToAnsi(STR0049) ) //##"Registro S-1210 do Funcion�rio: "##" Integrado com TAF."
						Else
							aAdd(aLogsOk, OemToAnsi(STR0025) + cCPF + " - " + cNome + OemToAnsi(STR0165) ) //##"Registro S-1210 do Funcion�rio: "##" Integrado com Middleware."
						EndIf
						aAdd(aLogsOk, "" )
						If lTSV
							aEmp_1210[3]++ //Inclui TSV integrado
						Else
							aEmp_1210[1]++ //Inclui TCV integrado
						EndIf
					Endif
					GrvTxtArq(alltrim(cXml), "S1210", cCPF)
				Else
					//"[AVISO]"##"Registro S-1210 do Funcion�rio: "##" n�o foi integrado devido ao(s) erro(s) abaixo: "##"Funcion�rio n�o possui movimento"
					aAdd(aLogsErr, OemToAnsi(STR0148) + OemToAnsi(STR0025) + cCPF + " - " + cNome + OemToAnsi(STR0029) + CRLF + OemToAnsi(STR0180) + CRLF)
					If lTSV
						aEmp_1210[4]++ //Inclui TSV nao integrado
					Else
						aEmp_1210[2]++ //Inclui TCV nao integrado
					EndIf
				EndIf
			Else
				If !Empty(aLogCIC) .AND. cVersEnvio >= "2.5.00"
					//"[AVISO]"##"Registro S-1210 do Funcion�rio: "##" n�o foi integrado devido ao(s) erro(s) abaixo: "##" - Benefici�rio(s) de Pens�o Aliment�cia SEM CPF cadastrado. Benefici�rio deve ser um CPF v�lido e diferente do CPF do trabalhador."
					aAdd(aLogsErr, OemToAnsi(STR0148) + OemToAnsi(STR0025) + cCPF + " - " + cNome + OemToAnsi(STR0029) + CRLF + OemToAnsi(STR0149) + CRLF)
				EndIf
				If lTSV
					aEmp_1210[4]++ //Inclui TSV nao integrado
				Else
					aEmp_1210[2]++ //Inclui TCV nao integrado
				EndIf
			EndIf
			aInfoPgto	:= {}
			aRetFer		:= {}
		ElseIf lRelat .And. !Empty(aLogCIC)
			aAdd( aRelIncons, { cOldFil, cCPF, OemToAnsi(STR0149)} )//" - Benefici�rio(s) de Pens�o Aliment�cia SEM CPF cadastrado. Benefici�rio deve ser um CPF v�lido e diferente do CPF do trabalhador."
		ElseIf cVersEnvio >= "2.6.00" .And. !lCPFDepOk
			For nCont := 1 To Len(aDepAgreg)
				aAdd(aLogsErr, aDepAgreg[nCont] )
			Next nCont
			aAdd(aLogsErr, "" )
			If lTSV
				aEmp_1210[4]++ //Inclui TSV nao integrado
			Else
				aEmp_1210[2]++ //Inclui TCV nao integrado
			EndIf
		EndIf
	EndIf

	(cAliasQSRA)->(dbCloseArea())
	If lAglut
		(cAliasQMV)->(dbCloseArea())
	EndIf
	IF oTmpCPF <> NIL
		oTmpCPF:Delete()
		oTmpCPF := Nil
	ENDIF
	If oTmpEmp <> NIL
		oTmpEmp:Delete()
		oTmpEmp := Nil
	EndIf
Next nI

cFilAnt := cBkpFil

RestArea(aArea)
RestArea(aAreaSM0)

If !lRelat
	aAdd(aLogsPrc, OemToAnsi(STR0039) + cValToChar(aEmp_1210[1]) ) 	//"Trabalhadores com v�nculo integrados: "
	aAdd(aLogsPrc, OemToAnsi(STR0040) + cValToChar(aEmp_1210[2]) )	//"Trabalhadores com v�nculo n�o Integrados: "
	aAdd(aLogsPrc, OemToAnsi(STR0041) + cValToChar(aEmp_1210[3]) )	//"Trabalhadores sem v�nculo integrados: "
	aAdd(aLogsPrc, OemToAnsi(STR0042) + cValToChar(aEmp_1210[4]) )	//"Trabalhadores sem v�nculo n�o Integrados: "
	aAdd(aLogsPrc, OemToAnsi(STR0043) + cValToChar( aEmp_1210[1] + aEmp_1210[3] ) )	//"Total de registros integrados: "
	aAdd(aLogsPrc, OemToAnsi(STR0044) + cValToChar(aEmp_1210[2] + aEmp_1210[4]) )	//"Total de registros n�o integrados: "

	aAdd(aLogsPrc,"")
	aAdd(aLogsPrc, Replicate("-",132) )
	aAdd(aLogsPrc, OemToAnsi(STR0069)+": " +  SecsToTime(nHrInicio))				//Inicio Processamento:
	nHrFim 	:= SecsToTime(Seconds())
	aAdd(aLogsPrc,+OemToAnsi(STR0070)+":    " + nHrFim)							//Fim Processamento:
	aAdd(aLogsPrc,"")
	aAdd(aLogsPrc,OemToAnsi(STR0071+": " + SecsToTime(Seconds() - nHrInicio)))		//Duracao do Processamento
Else
	fGeraRelat( cCompete, lAfast )
EndIf

aInfoPgto	:= {}
aDetPgtoFl	:= {}
aRetFer		:= {}
aRetPgtoTot	:= {}
aInfoSaude	:= {}
aInfoPrev	:= {}
aRetPensao	:= {}
aLogCIC		:= {}
aDetPgtoAt 	:= {}
aPgtoAnt 	:= {}
aDepAgreg	:= {}

Return .T.

/*/{Protheus.doc} fXml1210
Fun��o que monta o XML do evento S-1210 atrav�s da estrutura abaixo dos arrays de controle:

aInfoPgto <InfoPgto> -> Aglutina por <dtPgto> e <tpPgto>
	|
	->	aDetPgtoAt <detPgtoAnt> -> Relaciona com aInfoPgto por <dtPgto> e <tpPgto>
		|
		-> aPgtoAnt <infoPgtoAnt> -> Relaciona com aDetPgtoAt por <dtPgto> e <tpPgto>
			|
			-> aInfoSaude <infoSaudeColet> -> Relaciona com aPgtoAnt por <codRubr> + <dtPgto> e <tpPgto>
			|
			-> aInfoPrev <prevCompl> -> Relaciona com aPgtoAnt por <codRubr> + <dtPgto> e <tpPgto>
			|
			-> aRetPensao <penAlim>	-> Relaciona com aPgtoAnt por <codRubr> + <dtPgto> e <tpPgto>
	|
	->	aDetPgtoFl <detPgtoFl> -> Relaciona com aInfoPgto por <dtPgto> e <tpPgto>
		|
		-> aRetPgtoTot <retPgtoTot>	-> Relaciona com aDetPgtoFl por <ideDmDev>
			|
			-> aInfoSaude <infoSaudeColet>	-> Relaciona com aRetPgtoTot por <codRubr> + <ideDmDev>
			|
			-> aInfoPrev <prevCompl> -> Relaciona com aRetPgtoTot por <codRubr> + <ideDmDev>
			|
			-> aRetPensao <penAlim>	-> Relaciona com aRetPgtoTot por <codRubr> + <ideDmDev>
	|
	->	aRetFer <detPgtoFer> -> Relaciona com aInfoPgto por <dtPgto> e <tpPgto>
		|
		-> Gera <detRubrFer> pelos itens de aRetFer[XXX, 7]
			|
			-> aRetPensao <penAlim>	-> Relaciona com detRubrFer por <codRubr> + <ideDmDev>

@author Allyson
@since 19/06/2018
@version 1.0
@param cXML 		- String com o XML do evento S-1210
@param lRes 		-
@param lPgtRes 		-
@param aRGE1210		-
@param lAfast		- Indica se gera o evento para funcion�rios sem valores
@param cCompete		- Compet�ncia informada na gera��o do evento
@return lGerouXML	- Indica se foi gerado informa��es de pagamento no XML
/*/
Static Function fXml1210( cXML, lRes, lPgtRes, aRGE1210, lAfast, cCompete, cRetfNew, cRecibXML, cIdXml, cVersMw, aErros, cPeriodo, cFilEnv, lAdmPubl, cTpInsc, cNrInsc, cCpfBenef, nVlrDep )

Local cChaveMid		:= "-1"
Local cStatus		:= "-1"
Local lS1000		:= .T.

Local cPerNDE		:= ""
Local cAnoP			:= ""
Local nCntInfPgFl	:= 0
Local nCntInfSaude	:= 0
Local nCntInfSDep	:= 0
Local nCntInfPrev	:= 0
Local nCntDetPgAt	:= 0
Local nCntDetPgTo	:= 0
Local nCntRetPgAt 	:= 0
Local nCntRetPgTo 	:= 0
Local nCntRetPens 	:= 0
Local nCntRetFer 	:= 0
Local nCntRubFer 	:= 0
Local lFirstInfo	:= .F.
Local lGerouInfo	:= .F.
Local lGerouDetP	:= .F.
Local lGerouXML		:= .F.
Local lGerarRGE		:= .F.

Local lNGeraInPgt 	:= .F.
Local nPosInfo		:= 1
Local nPosDet		:= 1

Default lRes 		:= .F.
Default lPgtRes		:= .T.
Default aRGE1210	:= {}
Default cCompete	:= ""
Default cRetfNew	:= "1"
Default cRecibXML	:= ""
Default cIdXml		:= ""
Default cVersMw		:= ""
Default aErros		:= {}
Default cFilEnv		:= ""
Default lAdmPubl	:= .F.
Default cTpInsc		:= ""
Default cNrInsc		:= ""
Default cCpfBenef	:= ""
Default nVlrDep		:= 0

If lMiddleware
	cXml += "<eSocial xmlns='http://www.esocial.gov.br/schema/evt/evtPgtos/v" + cVersMw + "'>"
	fPosFil( cEmpAnt, cFilEnv )
	lS1000 := fVld1000( cPeriodo, @cStatus )
	If !lS1000
		Do Case
			Case cStatus == "-1" // nao encontrado na base de dados
				aAdd( aErros, OemToAnsi(STR0160) + "S-1000" + OemToAnsi(STR0161) )//"Registro do evento X-XXXX n�o localizado na base de dados"
			Case cStatus == "1" // nao enviado para o governo
				aAdd( aErros, OemToAnsi(STR0160) + "S-1000" + OemToAnsi(STR0162) )//"Registro do evento X-XXXX n�o transmitido para o governo"
			Case cStatus == "2" // enviado e aguardando retorno do governo
				aAdd( aErros, OemToAnsi(STR0160) + "S-1000" + OemToAnsi(STR0163) )//"Registro do evento X-XXXX aguardando retorno do governo"
			Case cStatus == "3" // enviado e retornado com erro
				aAdd( aErros, OemToAnsi(STR0160) + "S-1000" + OemToAnsi(STR0164) )//"Registro do evento X-XXXX retornado com erro do governo"
		EndCase
		Return .F.
	EndIf
	cXML += "<evtPgtos Id='" + cIdXml + "'>"//<evtPgtos>
	fXMLIdEve( @cXML, { cRetfNew, Iif(cRetfNew == "2", cRecibXML, Nil), "1", (SubStr(cPeriodo, 1, 4) + "-" + SubStr(cPeriodo, 5, 2)), 1, 1, "12" } )//<ideEvento>
	fXMLIdEmp( @cXML, { cTpInsc, Iif( cTpInsc == "1" .And. !lAdmPubl, SubStr(cNrInsc, 1, 8), cNrInsc ) } )//<ideEmpregador>
	S1210A05(@cXml, {cCpfBenef}, .F.)
	S1210A06(@cXml, {nVlrDep}, .T.)
EndIf

For nCntInfPgFl	:= 1 To Len(aInfoPgto)
	lFirstInfo	:= .T.
	lGerouInfo	:= .F.
	If aInfoPgto[nCntInfPgFl, 2] == "9"//Pagamento Anterior
		For nCntDetPgAt	:= 1 To Len(aDetPgtoAt)
			If aDetPgtoAt[nCntDetPgAt, 3] == aInfoPgto[nCntInfPgFl, 1] + aInfoPgto[nCntInfPgFl, 2] //infoPgto -> aDetPgtoAt | dtPgto + tpPgto
				If lFirstInfo
					lFirstInfo	:= .F.
					lGerouInfo	:= .T.
					lGerouDetP	:= .T.
					S1210A07(@cXml, aInfoPgto[nCntInfPgFl], .F., cVersEnvio)//infoPgto
					S1210A15(@cXml, aDetPgtoAt[nCntDetPgAt], .F., cVersEnvio)//detPgtoAnt
				EndIf
				For nCntRetPgAt	:= 1 To Len(aPgtoAnt)
					If aDetPgtoAt[nCntDetPgAt, 3] == aPgtoAnt[nCntRetPgAt, 7] .And. Iif( ValType(aPgtoAnt[nCntRetPgAt, 3]) == "N", Abs(aPgtoAnt[nCntRetPgAt, 3]) > 0, Abs(aPgtoAnt[nCntRetPgAt, 6]) > 0 )//aDetPgtoAt -> aPgtoAnt | dtPgto + tpPgto
						S1210A24(@cXml, aPgtoAnt[nCntRetPgAt], .F.)//infoPgtoAnt
						For nCntInfSaude := 1 To Len(aInfoSaude)
							If aPgtoAnt[nCntRetPgAt, 1] == aInfoSaude[nCntInfSaude, 4] .And. aPgtoAnt[nCntRetPgAt, 7] == aInfoSaude[nCntInfSaude, 10]//aPgtoAnt -> aInfoSaude | codRubr + dtPgto + tpPgto
								S1210A26(@cXml, .F. )//infoSaudeColet
								S1210A27(@cXml, {aInfoSaude[nCntInfSaude, 6], aInfoSaude[nCntInfSaude, 7], AllTrim( Transform(aInfoSaude[nCntInfSaude, 8], "@E 999999999.99") )}, .F.)//detOper
								If Len(aInfoSaude[nCntInfSaude, 9]) > 0
									For nCntInfSDep := 1 to Len(aInfoSaude[nCntInfSaude, 9])
										S1210A28(@cXml, {aInfoSaude[nCntInfSaude, 9, nCntInfSDep, 5], aInfoSaude[nCntInfSaude, 9, nCntInfSDep, 1], aInfoSaude[nCntInfSaude, 9, nCntInfSDep, 2], aInfoSaude[nCntInfSaude, 9, nCntInfSDep, 3], AllTrim( Transform(aInfoSaude[nCntInfSaude, 9, nCntInfSDep, 4], "@E 999999999.99") ) }, .T. )//detPlano
									Next nCntInfSDep
								EndIf
								S1210F27(@cXml)//detOper
								S1210F26(@cXml)//infoSaudeColet
							EndIf
						Next nCntInfSaude
						For nCntInfPrev	:= 1 To Len(aInfoPrev)
							If aPgtoAnt[nCntRetPgAt, 1] == aInfoPrev[nCntInfPrev, 1] .And. aPgtoAnt[nCntRetPgAt, 7] == aInfoPrev[nCntInfPrev, 3]//aPgtoAnt -> aInfoPrev | codRubr + dtPgto + tpPgto
								S1210A29(@cXml, {aInfoPrev[nCntInfPrev, 2]}, .T.)//prevCompl
							EndIf
						Next nCntInfSDep
						For nCntRetPens	:= 1 To Len(aRetPensao)
							If aPgtoAnt[nCntRetPgAt, 1] == aRetPensao[nCntRetPens, 1] .And. aPgtoAnt[nCntRetPgAt, 7] == aRetPensao[nCntRetPens, 6]//aPgtoAnt -> aRetPensao | codRubr + dtPgto + tpPgto
								cPerNDE := (SubStr(cCompete, 3, 4) + SubStr(cCompete, 1, 2) )
								If cVersEnvio >= "2.6.00" .And. cPerNDE >= "202001"
									S1210A10(@cXml, {aRetPensao[nCntRetPens, 4], , ,aRetPensao[nCntRetPens, 5]}, .T.)//penAlim
								Else
									S1210A10(@cXml, {aRetPensao[nCntRetPens, 4], aRetPensao[nCntRetPens, 2],aRetPensao[nCntRetPens, 3],aRetPensao[nCntRetPens, 5]}, .T.)//penAlim
								Endif
							EndIf
						Next nCntRetPens
						S1210F24(@cXml)//infoPgtoAnt
					EndIf
				Next nCntRetPgAt
			EndIf
		Next nCntDetPgAt
		If lGerouDetP
			S1210F15(@cXml)//detPgtoAnt
		EndIf
	ElseIf aInfoPgto[nCntInfPgFl, 2] == "7"//Ferias
		lGerouInfo	:= .T.
		S1210A07(@cXml, aInfoPgto[nCntInfPgFl], .F., cVersEnvio)//infoPgto
		If Len(aRGE1210) > 0
			S1210A17(@cXml, .F.)//idePgtoExt
			S1210A18(@cXml, { aRGE1210[1,1], aRGE1210[1,2], aRGE1210[1,3] }, .T.)//idePais
			S1210A19(@cXml, { aRGE1210[1,4], aRGE1210[1,5], aRGE1210[1,6], aRGE1210[1,7], aRGE1210[1,8], aRGE1210[1,9] }, .T.)//endExt
		Endif
		For nCntRetFer := 1 To Len(aRetFer)
			If aInfoPgto[nCntInfPgFl, 1] == aRetFer[nCntRetFer, 6]//infoPgto -> aRetFer | dtPgto
				S1210A21(@cXml, { aRetFer[nCntRetFer, 1], aRetFer[nCntRetFer, 2], aRetFer[nCntRetFer, 3], aRetFer[nCntRetFer, 4], aRetFer[nCntRetFer, 5] }, .F.)//detPgtoFer
				For nCntRubFer := 1 To Len(aRetFer[nCntRetFer, 7])
					S1210A22(@cXml, aRetFer[nCntRetFer, 7, nCntRubFer], .F.)//detRubrFer
					If lMiddleware
						fValPred(aRetFer[nCntRetFer, 7, nCntRubFer, 7], "S1010", cPeriodo, @aErros, lAdmPubl, cTpInsc, cNrInsc )
					EndIf
					For nCntRetPens	:= 1 To Len(aRetPensao)
						If aRetFer[nCntRetFer, 7, nCntRubFer, 1] == aRetPensao[nCntRetPens, 1] .And. aRetFer[nCntRetFer, 2]+aRetFer[nCntRetFer, 6]+"7" == aRetPensao[nCntRetPens, 6]//aRetFer -> aRetPensao | codRubr + matricula + dtPgto + tpPgto
							S1210A23(@cXml, { aRetPensao[nCntRetPens, 4], aRetPensao[nCntRetPens, 2], aRetPensao[nCntRetPens, 3], aRetFer[nCntRetFer, 7, nCntRubFer, 6] }, .T.)//penAlim
						EndIf
					Next nCntRetPens
					S1210F22(@cXml)//detRubrFer
				Next nCntRubFer
				S1210F21(@cXml)//detPgtoFer
			EndIf
		Next nCntRetFer
	Else
		For nCntDetPgTo	:= 1 To Len(adetPgtoFl)
			If (adetPgtoFl[nCntDetPgTo, 7] == aInfoPgto[nCntInfPgFl, 1] + aInfoPgto[nCntInfPgFl, 2]) .And. (lAfast .Or. adetPgtoFl[nCntDetPgTo, 4] > 0 .Or. aScan( aRetPgtoTot, { |x| x[7] == adetPgtoFl[nCntDetPgTo, 2] } ) > 0 ) //infoPgto -> detPgtoFl | dtPgto + tpPgto
				If !lGerouInfo
					lGerouInfo	:= .T.
					lGerarRGE	:= .T.
					S1210A07(@cXml, aInfoPgto[nCntInfPgFl], .F., cVersEnvio)//infoPgto
				Endif
				S1210A08(@cXml, adetPgtoFl[nCntDetPgTo], .F., cVersEnvio)//detPgtoFl

				For nCntRetPgTo	:= 1 To Len(aRetPgtoTot)
					If adetPgtoFl[nCntDetPgTo, 2] == aRetPgtoTot[nCntRetPgTo, 7]//detPgtoFl -> retPgtoTot | ideDmDev
						S1210A09(@cXml, aRetPgtoTot[nCntRetPgTo], .F.)//retPgtoTot
						If lMiddleware
							fValPred(aRetPgtoTot[nCntRetPgTo, 8], "S1010", cPeriodo, @aErros, lAdmPubl, cTpInsc, cNrInsc )
						EndIf
						For nCntInfSaude := 1 To Len(aInfoSaude)
							If aRetPgtoTot[nCntRetPgTo, 1] == aInfoSaude[nCntInfSaude, 4] .And. aRetPgtoTot[nCntRetPgTo, 7] == aInfoSaude[nCntInfSaude, 10]//retPgtoTot -> aInfoSaude | codRubr + ideDmDev
								S1210A26(@cXml, .F. )//infoSaudeColet
								S1210A27(@cXml, {aInfoSaude[nCntInfSaude, 6], aInfoSaude[nCntInfSaude, 7], AllTrim( Transform(aInfoSaude[nCntInfSaude, 8], "@E 999999999.99") )}, .F.)//detOper
								If Len(aInfoSaude[nCntInfSaude, 9]) > 0
									For nCntInfSDep := 1 to Len(aInfoSaude[nCntInfSaude, 9])
										S1210A28(@cXml, {aInfoSaude[nCntInfSaude, 9, nCntInfSDep, 5], aInfoSaude[nCntInfSaude, 9, nCntInfSDep, 1], aInfoSaude[nCntInfSaude, 9, nCntInfSDep, 2], aInfoSaude[nCntInfSaude, 9, nCntInfSDep, 3], AllTrim( Transform(aInfoSaude[nCntInfSaude, 9, nCntInfSDep, 4], "@E 999999999.99") ) }, .T. )//detPlano
									Next nCntInfSDep
								EndIf
								S1210F27(@cXml)//detOper
								S1210F26(@cXml)//infoSaudeColet
							EndIf
						Next nCntInfSaude
						For nCntInfPrev	:= 1 To Len(aInfoPrev)
							If aRetPgtoTot[nCntRetPgTo, 1] == aInfoPrev[nCntInfPrev, 1] .And. aRetPgtoTot[nCntRetPgTo, 7] == aInfoPrev[nCntInfPrev, 3]//retPgtoTot -> aInfoPrev | codRubr + ideDmDev
								S1210A29(@cXml, {aInfoPrev[nCntInfPrev, 2]}, .T.)//prevCompl
							EndIf
						Next nCntInfSDep
						For nCntRetPens	:= 1 To Len(aRetPensao)
							If aRetPgtoTot[nCntRetPgTo, 1] == aRetPensao[nCntRetPens, 1] .And. aRetPgtoTot[nCntRetPgTo, 7] == aRetPensao[nCntRetPens, 6]//retPgtoTot -> aRetPensao | codRubr + ideDmDev
								cPerNDE := (SubStr(cCompete, 3, 4) + SubStr(cCompete, 1, 2) )
								If cVersEnvio >= "2.6.00" .And. cPerNDE >= "202001"
									S1210A10(@cXml, {aRetPensao[nCntRetPens, 4], , ,aRetPensao[nCntRetPens, 5]}, .T.)//penAlim
								Else
									S1210A10(@cXml, {aRetPensao[nCntRetPens, 4], aRetPensao[nCntRetPens, 2],aRetPensao[nCntRetPens, 3],aRetPensao[nCntRetPens, 5]}, .T.)//penAlim
								Endif
							EndIf
						Next nCntRetPens
						S1210F09(@cXml)//retPgtoTot
					EndIf
				Next nCntRetPgTo

				S1210F08(@cXml)//detPgtoFl
				If Len(aRGE1210) > 0 .And. lGerarRGE
					S1210A17(@cXml, .F.)//idePgtoExt
					S1210A18(@cXml, { aRGE1210[1,1], aRGE1210[1,2], aRGE1210[1,3] }, .T.)//idePais
					S1210A19(@cXml, { aRGE1210[1,4], aRGE1210[1,5], aRGE1210[1,6], aRGE1210[1,7], aRGE1210[1,8], aRGE1210[1,9] }, .T.)//endExt
					lGerarRGE := .F.
				EndIf
			EndIf
		Next nCntDetPgTo
	EndIf
	If lGerouInfo
		lGerouXML := .T.
		If Len(aRGE1210) > 0
			S1210F17(@cXml)//idePgtoExt
		Endif
		S1210F07(@cXml)//infoPgto
	EndIf
Next nCntInfPgFl

IF lRes
	For nCntInfPgFl	:= 1 To Len(aInfoPgto)
		For nCntDetPgTo	:= 1 To Len(adetPgtoFl)
			IF (adetPgtoFl[nCntDetPgTo, 7] == aInfoPgto[nCntInfPgFl, 1] + aInfoPgto[nCntInfPgFl, 2]) .AND. (LEN(adetPgtoFl[nCntDetPgTo][2]) > 0 .AND. Left(adetPgtoFl[nCntDetPgTo][2],1) == "R")

				IF adetPgtoFl[nCntDetPgTo, 4] == 0
					nPosInfo := nCntInfPgFl
					nPosDet := nCntDetPgTo
				ELSE
					lNGeraInPgt := .T.
				ENDIF

			ENDIF
		NEXT nCntDetPgTo
	NEXT nCntInfPgFl
ENDIF

IF (!lGerouXML .Or. (lRes .And. !lNGeraInPgt )) .And. lAfast .And. LEN(aInfoPgto) > 0 .And. LEN(adetPgtoFl) > 0 .And. lPgtRes

	adetPgtoFl[nPosDet,4] := 0
	S1210A07(@cXml, aInfoPgto[nPosInfo], .F., cVersEnvio)//infoPgto
	S1210A08(@cXml, adetPgtoFl[nPosDet], .F., cVersEnvio)//detPgtoFl
	S1210F08(@cXml)//detPgtoFl
	S1210F07(@cXml)//infoPgto

	lGerouXML := .T.
ENDIF

S1210F05(@cXml, aideBenef, .T.)//ideBenef
S1210F02(@cXml)//evtPgtos
S1210F01(@cXml)//eSocial

If lMiddleware .And. lGerouXML .And. !Empty(aErros)
	lGerouXML := .F.
EndIf

Return lGerouXML

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �fBenefici   � autor � Marcia Moura        � Data � 02/03/01 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Carrega array com informacoes do cadastro de beneficiarios ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Generico                                                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Static Function fBenefic(cFilFun, cMatFun, dDataCalc, cCodVerba, nRdValor, cId, cPeriodo)
Local cAlias := ALIAS()
Local cMesAnoCalc := ""
Local nPos   := 0

Default cId	 := ""
Default cPeriodo := ""

//Para regime caixa, � necess�rio verificar o per�odo do c�lculo pois o pagamento ocorre no m�s seguinte
If !Empty(cPeriodo)
	cMesAnoCalc := cPeriodo
Else
	cMesAnoCalc := AnoMes(dDataCalc)
EndIf

dbSelectArea( "SRQ" )

//��������������������������������������������������������������Ŀ
//� Carrega array com dados do cadastro de beneficiarios	     �
//����������������������������������������������������������������
If dbSeek( cFilFun + cMatFun )

	While SRQ->RQ_FILIAL + SRQ->RQ_MAT == cFilFun + cMatFun

		If SRQ->(FieldPos('RQ_DTINI')) > 0 .and. SRQ->(FieldPos('RQ_DTFIM')) > 0 .and.;
			(!Empty(SRQ->RQ_DTINI) .or. !Empty(SRQ->RQ_DTFIM))

			If (!Empty(SRQ->RQ_DTINI) .and. cMesAnoCalc < AnoMes(SRQ->RQ_DTINI)) .or.;
	 		   (!Empty(SRQ->RQ_DTFIM) .and. cMesAnoCalc > AnoMes(SRQ->RQ_DTFIM))
				dbSkip()
				Loop
			EndIf
		EndIf

		If cCodVerba $ (SRQ->RQ_VERBADT+"/"+SRQ->RQ_VERBFOL+"/"+SRQ->RQ_VERBFER+"/"+SRQ->RQ_VERB131+"/"+SRQ->RQ_VERB132+"/"+SRQ->RQ_VERBPLR+"/"+SRQ->RQ_VERBDFE+"/"+SRQ->RQ_VERBRRA)
			nPos := ascan(aRetPensao,{|X| X[1] == cCodVerba .And. X[2] == dToS( SRQ->RQ_NASC ) .And. X[3] == SRQ->RQ_NOME .And. X[4] == SRQ->RQ_CIC .And. X[6] == cId .And. x[7] == SRA->RA_CIC })
			If nPos == 0
				Aadd (aRetPensao, { cCodVerba, Iif( !lMiddleware .Or. !Empty(SRQ->RQ_NASC), dToS(SRQ->RQ_NASC), Nil), SRQ->RQ_NOME, SRQ->RQ_CIC, nRdValor, cId, SRA->RA_CIC, SRQ->RQ_ORDEM })
				If Empty(SRQ->RQ_CIC)
					aAdd(aLogCIC, SRQ->RQ_NOME)
				EndIf
			else
				aRetPensao[nPos,5] += nRdValor
			Endif

		EndIf

		dbSkip()
	EndDo
EndIf
dbSelectArea( cAlias )

Return .T.

/*/{Protheus.doc} fm036GetFer
Fun��o respons�vel por pesquisar e gerar os dados de ferias nas tabelas SRH e SRR para geracao do evento S-1210
@Author.....: Marcelo Silveira
@Since......: 08/05/2018
@Version....: 1.0
@Param......: (char) - cFilFun - Filial do funcionario para a pesquisa nas tabelas SRH e SRR
@Param......: (char) - cMatFun - Matricula do funcionario para a pesquisa
@Param......: (char) - cDtPesqI - Data inicial do per�odo para a pesquisa
@Param......: (char) - cDtPesqF - Data final do per�odo para a pesquisa
@Param......: (char) - cCateg - Categoria do funcionario
@Param......: (array) - aVbFer - Array de referencia para armazenamento das verbas de ferias avaliadas
@Param......: (array) - aErros - Retorno de poss�veis erros na gera��o do evento
@Param......: (char) - cFilPreTrf - Filial de origem do funcionario para posicionamento da SRV
@Return.....: (array) - aFer - Array de retorno com os dados de ferias do funcionario
/*/
Static Function fm036GetFer( cFilFun, cMatFun, cDtPesqI, cDtPesqF, cCateg, aVbFer, aErros, cFilPreTrf )

Local cAliasSRH	:= "QSRH"
Local cIdPdv	:= ""
Local nNumReg	:= 0
Local nPercRub	:= 0
Local nVbFer	:= 0
Local nPosFerPd	:= 0
Local aFer		:= {}
Local aFerPd	:= {}
Local lGeraCod	:= .F.
Local cIdTbRub	:= ""
Local nSizeFer	:= 0
Local lPrimIdT	:= .T.
Local cIdeRubr	:= ""

DEFAULT aErros	:= {}

If ( Select( cAliasSRH ) > 0 )
	( cAliasSRH )->( dbCloseArea() )
EndIf

If __oSt11 == Nil
	__oSt11 := FWPreparedStatement():New()
	cQrySt := "SELECT Count(*) AS NUMREG "
	cQrySt += "FROM " + RetSqlName('SRH') + " SRH "
	cQrySt += "WHERE SRH.RH_FILIAL = ? AND "
	cQrySt += 		"SRH.RH_MAT = ? AND "
	cQrySt += 		"SRH.RH_DTRECIB BETWEEN '" + cDtPesqI + "' AND '" + cDtPesqF + "' AND "
	cQrySt += 		"SRH.D_E_L_E_T_ = ' ' "
	cQrySt := ChangeQuery(cQrySt)
	__oSt11:SetQuery(cQrySt)
EndIf
__oSt11:SetString(1, cFilFun)
__oSt11:SetString(2, cMatFun)
cQrySt := __oSt11:getFixQuery()
dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQrySt),cAliasSRH,.T.,.T.)

nNumReg := (cAliasSRH)->NUMREG
( cAliasSRH )->( dbCloseArea() )

If nNumReg > 0
	If __oSt12 == Nil
		__oSt12 := FWPreparedStatement():New()
		cQrySt := "SELECT RH_FILIAL,RH_MAT,RH_PROCES,RH_PERIODO,RH_ROTEIR,RH_DTRECIB,RH_DFERIAS,RH_DATAINI,RH_DTRECIB "
		cQrySt += "FROM " + RetSqlName('SRH') + " SRH "
		cQrySt += "WHERE SRH.RH_FILIAL = ? AND "
		cQrySt += 		"SRH.RH_MAT = ? AND "
		cQrySt += 		"SRH.RH_DTRECIB BETWEEN '" + cDtPesqI + "' AND '" + cDtPesqF + "' AND "
		cQrySt += 		"SRH.D_E_L_E_T_ = ' ' "
		cQrySt := ChangeQuery(cQrySt)
		__oSt12:SetQuery(cQrySt)
	EndIf
	__oSt12:SetString(1, cFilFun)
	__oSt12:SetString(2, cMatFun)
	cQrySt := __oSt12:getFixQuery()
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQrySt),cAliasSRH,.T.,.T.)

	DbSelectArea("SRR")
	DbSetOrder(RetOrder("SRR","RR_FILIAL+RR_MAT+RR_TIPO3+DTOS(RR_DATA)+RR_PD+RR_CC"))

	While (cAliasSRH)->(!Eof())

		//			  codCateg,	matricula,	dtIniGoz,					qtDias,						vrLiq,	Data pagamento (recibo)
		aAdd( aFer, { cCateg, SRA->RA_CODUNIC, (cAliasSRH)->(RH_DATAINI), (cAliasSRH)->(RH_DFERIAS),	0,	(cAliasSRH)->(RH_DTRECIB), {}, SRA->RA_CIC, SRA->RA_FILIAL, SRA->RA_MAT + " - " + SRA->RA_CODUNIC, SRA->RA_NOMECMP } )

		If SRR->( DbSeek( (cAliasSRH)->RH_FILIAL + (cAliasSRH)->RH_MAT + "F" + (cAliasSRH)->(RH_DATAINI) ) )

			While SRR->(!Eof() .and. RR_FILIAL+RR_MAT+RR_TIPO3+DTOS(RR_DATA) == (cAliasSRH)->RH_FILIAL+(cAliasSRH)->RH_MAT+"F"+(cAliasSRH)->RH_DATAINI )

				nVbFer := aScan( aVbFer, { |x| x[1] == SRR->RR_PD })

				//Tratamento para evitar o uso da PosSrv devido desempenho
				PosSrv( SRR->RR_PD, cFilPreTrf )
				If nVbFer > 0
					nPercRub 	:= aVbFer[nVbFer,2]
					cIncIRF 	:= aVbFer[nVbFer,3]
					cIdPdv 		:= aVbFer[nVbFer,4]
				Else
					aAdd( aVbFer, { SRR->RR_PD, SRV->RV_PERC, SRV->RV_INCIRF, SRV->RV_CODFOL } )
					nPercRub 	:= SRV->RV_PERC - 100
					cIncIRF  	:= SRV->RV_INCIRF
					cIdPdv 		:= SRV->RV_CODFOL
				EndIf
				If cVersEnvio < "2.6.00"
					cIncIRF 	:= SubStr(cIncIRF, 1, 2)
				EndIf
				If !Empty(xFilial("SRV"))
					lGeraCod := .T.
				EndIf
				If lGeraCod
					cIdTbRub := SRV->RV_FILIAL
				Else
					If cVersEnvio >= "2.3"
						cIdTbRub := cEmpAnt
					Else
						cIdTbRub := ""
					EndIf
				EndIf

				If lMiddleware
					If lPrimIdT
						lPrimIdT  := .F.
						cIdeRubr := fGetIdRJF( SRV->RV_FILIAL, cIdTbRub )
						If Empty(cIdeRubr) .And. aScan(aErros, { |x| x == OemToAnsi(STR0156) + cIdTbRub + OemToAnsi(STR0157) }) == 0
							aAdd( aErros, OemToAnsi(STR0156) + cIdTbRub + OemToAnsi(STR0157) )//"Aten��o"##"N�o ser� poss�vel efetuar a integra��o. O identificador de tabela de rubrica do c�digo: "##" n�o est� cadastrado."
						EndIf
					EndIf
					cIdTbRub := cIdeRubr
				EndIf

				nPosFerPd := aScan( aFerPd, { |x| x[1] == SRR->RR_PD} )
				If nPosFerPd > 0
					If ( ( cVersEnvio < "2.6.00" .And. cIncIRF $ "00|01|09|13|33|43|46|53|63|75|93" ) .Or.;
						( cVersEnvio >= "2.6.00" .And. cIncIRF $ "0   |1   |9   |13  |33  |43  |48  |53  |65  |66  |75  |79  |899 |93  |9013|9033|9043|9048|9053|9065|9066" ) );
						.And. !(cIdPdv $ "102|0102")
						aFerPd[nPosFerPd][3] += SRR->RR_HORAS
						aFerPd[nPosFerPd][6] += SRR->RR_VALOR

						If (cVersEnvio < "2.6.00" .And. cIncIRF == "53") .Or. (cVersEnvio >= "2.6.00" .And. cIncIRF $ "53  /9053")
							fBenefic( (cAliasSRH)->RH_FILIAL, (cAliasSRH)->RH_MAT, stod((cAliasSRH)->RH_DTRECIB), SRR->RR_PD, SRR->RR_VALOR, SRA->RA_CODUNIC + (cAliasSRH)->RH_DTRECIB + "7" , cPeriodo)
						EndIf
					Endif

					If cIdPdv $ "102|0102"
						aFerPd[nPosFerPd][5] += SRR->RR_VALOR
					Endif
				Else
					If ( ( cVersEnvio < "2.6.00" .And. cIncIRF $ "00|01|09|13|33|43|46|53|63|75|93" ) .Or.;
						( cVersEnvio >= "2.6.00" .And. cIncIRF $ "0   |1   |9   |13  |33  |43  |48  |53  |65  |66  |75  |79  |899 |93  |9013|9033|9043|9048|9053|9065|9066" ) );
						.And. !(cIdPdv $ "102|0102")
						//				codRubr, 	ideTabRubr, 	qtdRubr, 			fatorRubr, 												vrUnit, 	  vrRubr
						aAdd( aFerPd, { SRR->RR_PD, cIdTbRub, SRR->RR_HORAS, If( nPercRub < 0, 0, Transform(nPercRub,"@E 999.99")), /*nao enviar*/,SRR->RR_VALOR, (SRV->RV_FILIAL+SRR->RR_PD) } )
						If (cVersEnvio < "2.6.00" .And. cIncIRF == "53") .Or. (cVersEnvio >= "2.6.00" .And. cIncIRF $ "53  /9053")
							fBenefic( (cAliasSRH)->RH_FILIAL, (cAliasSRH)->RH_MAT, stod((cAliasSRH)->RH_DTRECIB), SRR->RR_PD, SRR->RR_VALOR, SRA->RA_CODUNIC + (cAliasSRH)->RH_DTRECIB + "7", cPeriodo )
						EndIf
					EndIf

					If cIdPdv $ "102|0102" //Liquido de Ferias para gerar a Tag <vrLiq>
						aFer[Len(aFer),5] += SRR->RR_VALOR
					EndIf
				Endif


				SRR->(DbSkip())
			EndDo

		EndIf

		If Len( aFerPd ) > 0
			aFer[Len(aFer)][7] := aClone(aFerPd)
			aFerPd := Array(0)
		EndIf

		(cAliasSRH)->(DbSkip())
	EndDo

EndIf

Return( aFer )

/*/{Protheus.doc} fGetRGE()
Fun��o respons�vel por verificar hist�rico do contrato de trabalho
de residentes no exterior, grava��o do array axxx com os dados da tabela RGE para o evento S-1210
@type function
@author Claudinei Soares
@since 22/11/2018
@version 1.0
@param cFilRGE		= Filial a ser pesquisada na tabela RGE
@param cMatRGE 		= Matr�cula a ser pesquisada na tabela RGE
@param cCompete 	= Per�odo informado na gera��o do evento servir� como base para buscar a vig�ncia do contrato
@return aRGE, Array, Retorno da fun��o, campos da RGE que ser�o enviados no XML
/*/

Function fGetRGE(cFilRGE, cMatRGE, cCompete)

Local cUltimoD		:= STRZERO(f_UltDia(CTOD( "01/"+SUBSTR(cCompete,1,2)+"/"+SUBSTR(cCompete,3,4) )),2)
Local cIndNif		:= ""

Local dIniContr		:= CTOD("//")
Local dFimContr		:= CTOD("//")
Local dCompIni 		:= CTOD( "01/"+SUBSTR(cCompete,1,2)+"/"+SUBSTR(cCompete,3,4) )
Local dCompFim 		:= CTOD( cUltimoD +"/"+ SUBSTR(cCompete,1,2) +"/"+ SUBSTR(cCompete,3,4) )

Local aArea			:= GetArea()
Local aItensRGE		:= {}


dbSelectArea("RGE")
RGE->( dbSetOrder(2) )
RGE->(dbGoTop())

If RGE->( dbSeek( cFilRGE + cMatRGE ) )
	While RGE->( !Eof() .And. RGE->RGE_FILIAL == cFilRGE .And. RGE->RGE_MAT == cMatRGE )
		dIniContr := RGE->RGE_DATAIN
		dFimContr := RGE->RGE_DATAFI

		If dIniContr <= dCompIni .And. dFimContr >= dCompFim

			cIndNif := ( If(RGE->RGE_PAEXNI == "2", "3", RGE->RGE_BEDINI ))

			aAdd(aItensRGE, {	Alltrim(RGE->RGE_CODPAI)	,;	//C�digo do Pais		- TAG <codPais>		do S-1210
								Alltrim(cIndNif)			,;	//Indicativo do NIF		- TAG <indNIF>		do S-1210
								Iif(!lMiddleware .Or. !Empty(RGE->RGE_CODNIF), Alltrim(RGE->RGE_CODNIF), Nil),;	//C�digo do NIF			- TAG <nifBenef>	do S-1210
								Alltrim(RGE->RGE_LOGRAD)	,;	//Logradouro 			- TAG <dscLograd>	do S-1210
								Iif(!lMiddleware .Or. !Empty(RGE->RGE_NUMERO), Alltrim(RGE->RGE_NUMERO), Nil),;	//N�mero do Endere�o	- TAG <nrLograd>	do S-1210
								Iif(!lMiddleware .Or. !Empty(RGE->RGE_COMPL), Alltrim(RGE->RGE_COMPL), Nil),;	//Complemento Endere�o	- TAG <complem>		do S-1210
								Iif(!lMiddleware .Or. !Empty(RGE->RGE_BAIRRO), Alltrim(RGE->RGE_BAIRRO), Nil),;	//Bairro				- TAG <bairro>		do S-1210
								Alltrim(RGE->RGE_CIDADE)	,;	//Cidade				- TAG <nmCid>		do S-1210
								Iif(!lMiddleware .Or. !Empty(RGE->RGE_CODPOS), Alltrim(RGE->RGE_CODPOS), Nil)})	//C�digo Postal (CEP)	- TAG <codPostal>	do S-1210
			Exit
		Endif

		RGE->(dbSkip())
	EndDo
EndIf

RestArea(aArea)

Return( aItensRGE )

/*/{Protheus.doc} fQtdeFolMV()
Fun��o que verifica a quantidade matriculas no Periodo de acordo com o filtro por data de pagamento
@type function
@author allyson.mesashi
@since 27/02/2019
@version 1.0
@param cOpcTab		= Indica se considerada a tabela SRC
@param cFilProc		= Filiais para busca
@param cPeriodo		= Periodo de busca
@param cDtPesqI		= Data de pagamento inicial
@param cDtPesqF		= Data de pagamento final
@return nQtde		= Quantidade de matriculas no periodo
/*/
Static Function fQtdeFolMV(cOpcTab, cPeriodo, cDtPesqI, cDtPesqF)

Local cAliasMV	:= GetNextAlias()
Local cJoinRCxRY:= FWJoinFilial( "SRC", "SRY" )
Local cJoinRDxRY:= FWJoinFilial( "SRD", "SRY" )
Local cQrySt	:= ""
Local nParam	:= 0
Local nQtde		:= 1

If __oSt17 == Nil
	__oSt17 := FWPreparedStatement():New()
	cQrySt := "SELECT SRA.RA_CIC, COUNT(*) AS CONT FROM " + RetSqlName('SRA') + " SRA "
	IF lTemGC .And. lTemEmp
		cQrySt += "WHERE SUBSTRING(SRA.RA_FILIAL, " + cValToChar(nIniEmp) + ", " + cValToChar(nTamEmp) + ") = ? AND "
	Else
		cQrySt += "WHERE "
	EndIf
	cQrySt += "SRA.RA_CIC >= ? AND SRA.RA_CIC <= ? "
	cQrySt += "AND ( SRA.RA_PIS <> ' ' OR SRA.RA_CATEFD IN ('901','903','904') ) AND SRA.RA_CC <> ' ' "
	If cOpcTab == "1"
		cQrySt += "AND EXISTS (SELECT DISTINCT SRC.RC_FILIAL, SRC.RC_MAT FROM " + RetSqlName('SRC') + " SRC INNER JOIN " + RetSqlName('SRY') + " SRY ON " + cJoinRCxRY + " AND SRC.RC_ROTEIR = SRY.RY_CALCULO WHERE SRC.RC_FILIAL = SRA.RA_FILIAL AND SRC.RC_MAT = SRA.RA_MAT AND SRC.RC_PERIODO = ? AND SRC.RC_DATA BETWEEN '" + cDtPesqI + "' AND '" + cDtPesqF + "' AND SRC.RC_TIPO2 != 'K' AND SRC.D_E_L_E_T_ = ' ' AND SRY.RY_TIPO != 'K' AND SRY.D_E_L_E_T_ = ' ' "
		cQrySt += "UNION "
		cQrySt += "SELECT DISTINCT SRD.RD_FILIAL, SRD.RD_MAT FROM " + RetSqlName('SRD') + " SRD INNER JOIN " + RetSqlName('SRY') + " SRY ON " + cJoinRDxRY + " AND SRD.RD_ROTEIR = SRY.RY_CALCULO WHERE SRD.RD_FILIAL = SRA.RA_FILIAL AND SRD.RD_MAT = SRA.RA_MAT AND SRD.RD_PERIODO = ? AND SRD.RD_DATPGT BETWEEN '" + cDtPesqI + "' AND '" + cDtPesqF + "' AND SRD.RD_TIPO2 != 'K' AND SRD.RD_EMPRESA = '  ' AND SRD.D_E_L_E_T_ = ' ' AND SRY.RY_TIPO != 'K' AND SRY.D_E_L_E_T_ = ' ') "
	Else
		cQrySt += "AND EXISTS (SELECT DISTINCT SRD.RD_FILIAL, SRD.RD_MAT FROM " + RetSqlName('SRD') + " SRD INNER JOIN " + RetSqlName('SRY') + " SRY ON " + cJoinRDxRY + " AND SRD.RD_ROTEIR = SRY.RY_CALCULO WHERE SRD.RD_FILIAL = SRA.RA_FILIAL AND SRD.RD_MAT = SRA.RA_MAT AND SRD.RD_PERIODO = ? AND SRD.RD_DATPGT BETWEEN '" + cDtPesqI + "' AND '" + cDtPesqF + "' AND SRD.RD_TIPO2 != 'K' AND SRD.RD_EMPRESA = '  ' AND SRD.D_E_L_E_T_ = ' ' AND SRY.RY_TIPO != 'K' AND SRY.D_E_L_E_T_ = ' ') "
	EndIf
	cQrySt += "AND SRA.D_E_L_E_T_ <> '*' "
	cQrySt += "GROUP BY SRA.RA_CIC "
	cQrySt := ChangeQuery(cQrySt)
	__oSt17:SetQuery(cQrySt)
EndIf
IF lTemGC .And. lTemEmp
	__oSt17:SetString(++nParam, SubString(SRA->RA_FILIAL, nIniEmp, nTamEmp) )
EndIf
__oSt17:SetString(++nParam, SRA->RA_CIC)
__oSt17:SetString(++nParam, SRA->RA_CIC)
__oSt17:SetString(++nParam, cPeriodo)
If cOpcTab == "1"
	__oSt17:SetString(++nParam, cPeriodo)
EndIf
cQrySt := __oSt17:getFixQuery()
dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQrySt),cAliasMV,.T.,.T.)
nQtde := (cAliasMV)->CONT

(cAliasMV)->( dbCloseArea() )

Return nQtde

/*/{Protheus.doc} lTem546
//Checa se tem a verba de ID 0546
@author flavio.scorrea
@since 06/05/2019
/*/
Static Function lTem546(cFilFun,cMatFun,dData1,dData2,cPd)
Local lRet		:= .F.
Local cAliasTmp	:= GetNextAlias()

BeginSQL Alias cAliasTmp
	SELECT RC_PD
	FROM %Table:SRC% SRC
	WHERE RC_FILIAL = %Exp:cFilFun%
	AND RC_MAT = %Exp:cMatFun%
	AND RC_DATA BETWEEN %Exp:dData1% AND %Exp:dData2%
	AND SRC.%NotDel%
	AND RC_PD = %Exp:cPd%
	UNION
	SELECT RD_PD AS RC_PD
	FROM %Table:SRD% SRD
	WHERE RD_FILIAL = %Exp:cFilFun%
	AND RD_MAT = %Exp:cMatFun%
	AND RD_DATPGT BETWEEN %Exp:dData1% AND %Exp:dData2%
	AND SRD.%NotDel%
	AND RD_PD = %Exp:cPd%
EndSQL
lRet := !(cAliasTmp)->(Eof())
(cAliasTmp)->(DbCloseArea())
Return lRet

/*/{Protheus.doc} fGetPrev()
Fun��o que retorna as informa��es de previd�ncia privada
@type      	Function
@author   	Allyson
@since		07/06/2019
@version	1.0
@param cVerba		= Alias da tabela tempor�ria principal
@param cPeriodo		= Data de Gera��o da Rescis�o
@param cDmDev		= Data de Gera��o da Rescis�o
@return aPrev    	= Informa��es do fornecedor da previd�ncia privada
/*/
Static Function fGetPrev( cVerba, cPeriodo, cDmDev )
Local aArea			:= GetArea()
Local aPrev			:= {}
Local cAnoMes		:= cPeriodo
Local cCNPJForn		:= ""
Local cFornS073		:= ""
Local dDataRef  	:= cToD( "01/" + SubStr( cPeriodo, 5, 2 ) + "/" + SubStr( cPeriodo, 1, 4 ) )
Local lAchou		:= .F.
Local nPosTab		:= 0

SMU->( dbSetOrder(1) )
If SMU->( dbSeek(SRA->RA_FILIAL+SRA->RA_MAT) )
	While SMU->( !EoF() ) .And. SMU->MU_FILIAL + SMU->MU_MAT == SRA->RA_FILIAL + SRA->RA_MAT
		If cPeriodo >= SubStr( SMU->MU_PERINI, 3, 4 )+SubStr( SMU->MU_PERINI, 1, 2 ) .And. cPeriodo <= SubStr( SMU->MU_PERFIM, 3, 4 )+SubStr( SMU->MU_PERFIM, 1, 2 )
			cFornS073 := SMU->MU_CODFOR
			Exit
		EndIf
		SMU->(dbSkip())
	EndDo
EndIf

If Empty(aTabS073)
	fCarrTab( @aTabS073, "S073", dDataRef, .T. )
EndIf

//--Verifica se Existe a Tabela Cadastrada
If ( nPosTab := Ascan(aTabS073, { |x| x[2] == SRA->RA_FILIAL .And. x[5] == cFornS073 .And. (x[10] == cVerba .Or. x[11] == cVerba) }) ) > 0
	lAchou := .T.
ElseIf ( nPosTab := Ascan(aTabS073, { |x| x[2] == Space(FwGetTamFilial) .And. x[5] == cFornS073 .And. (x[10] == cVerba .Or. x[11] == cVerba) }) ) > 0
	lAchou := .T.
EndIf

If lAchou
	aAdd( aPrev, { cVerba, aTabS073[nPosTab, 6], cDmDev } )
EndIf

RestArea(aArea)

Return aPrev

/*/{Protheus.doc} fFerPreTrf
Verifica SRE para transfer�ncia posterior �s f�rias no per�odo da consulta � SRH, SRR
	@type	Function
	@author	isabel.noguti
	@since	13/01/2020
	@version 1.0
	@param	cSRHFil, char, Filial a ser consultada na SRH
	@param	cSRHMat, char, Matricula a ser consultada na SRH
	@param	cDtPesq, char, Data final do per�odo
	@return	lRet, logic, Matr�cula destino possui SRH no per�odo, transferida da matr�cula de origem
/*/
Static Function fFerPreTrf( cSRHFil, cSRHMat, cDtPesq )
	Local cAliasSRE	:= GetNextAlias()
	Local cQrySt	:= ""
	Local lRet		:= .F.

	If __oSt18 == Nil
		__oSt18 := FWPreparedStatement():New()
		cQrySt := "SELECT COUNT(*) QTD FROM "
		cQrySt += RetSqlName('SRH') + " SRH INNER JOIN " + RetSqlName('SRE') + " SRE ON SRH.RH_FILIAL = SRE.RE_FILIALP AND SRH.RH_MAT = SRE.RE_MATP "
		cQrySt += "WHERE SRE.RE_FILIALP = ? AND SRE.RE_MATP = ? "
		cQrySt += "AND SRH.RH_DTRECIB <= ? AND SRE.RE_DATA > ? "
		cQrySt += "AND (SRE.RE_FILIALD <> SRE.RE_FILIALP OR SRE.RE_MATD <> SRE.RE_MATP) "
		cQrySt += "AND SRH.D_E_L_E_T_ <> '*' AND SRE.D_E_L_E_T_ <> '*' "
		cQrySt := ChangeQuery(cQrySt)
		__oSt18:SetQuery(cQrySt)
	EndIf
	__oSt18:SetString(1, cSRHFil)
	__oSt18:SetString(2, cSRHMat)
	__oSt18:SetString(3, cDtPesq)
	__oSt18:SetString(4, cDtPesq)
	cQrySt := __oSt18:getFixQuery()
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQrySt),cAliasSRE,.T.,.T.)

	If (cAliasSRE)->QTD > 0
		lRet := .T.
	EndIf
	(cAliasSRE)->(DbCloseArea())

	If !lRet .And. SRA->RA_SITFOLH == "D" .And. SRA->RA_RESCRAI $ "30|31"
		If __oSt19 == Nil
			__oSt19 := FWPreparedStatement():New()
			cQrySt := "SELECT SRE.RE_FILIALP, SRE.RE_MATP FROM "
			cQrySt += RetSqlName('SRH') + " SRH INNER JOIN " + RetSqlName('SRE') + " SRE ON SRH.RH_FILIAL = SRE.RE_FILIALP AND SRH.RH_MAT = SRE.RE_MATP "
			cQrySt += "WHERE SRE.RE_FILIALD = ? AND SRE.RE_MATD = ? "
			cQrySt += "AND SRH.RH_DTRECIB <= ? AND SRE.RE_DATA > ? "
			cQrySt += "AND SRH.D_E_L_E_T_ <> '*' AND SRE.D_E_L_E_T_ <> '*' "
			cQrySt += "ORDER BY SRE.RE_DATA DESC "
			cQrySt := ChangeQuery(cQrySt)
			__oSt19:SetQuery(cQrySt)
		EndIf
		__oSt19:SetString(1, cSRHFil)
		__oSt19:SetString(2, cSRHMat)
		__oSt19:SetString(3, cDtPesq)
		__oSt19:SetString(4, cDtPesq)
		cQrySt := __oSt19:getFixQuery()
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQrySt),cAliasSRE,.T.,.T.)

		If !(cAliasSRE)->(Eof())
			cSRHFil := (cAliasSRE)->RE_FILIALP
			cSRHMat := (cAliasSRE)->RE_MATP
			Iif( nQtdeFol > 1, nQtdeFol --, nQtdeFol )
		EndIf
		(cAliasSRE)->(DbCloseArea())
	EndIf

Return lRet

/*/{Protheus.doc} fGeraRelat
Fun��o que gera o relat�rio em Excel
@author Allyson
@since 08/05/2020
@version 1.0
/*/
Static Function fGeraRelat(cCompete, lAfast)

Local cArquivo  	:= "RELATORIO_PERIODICOS_1210_"+cCompete+".xls"
Local cDefPath		:= GetSrvProfString( "StartPath", "\system\" )
Local cPath     	:= ""
Local nCntIdeBen	:= 0
Local nCntInfPgFl	:= 0
Local nCntDetPgTo	:= 0
Local nCntRetPgTo 	:= 0
Local nCntRetPens 	:= 0
Local nCntRetFer 	:= 0
Local nCntRubFer 	:= 0
Local nCntIncons	:= 0
Local oExcelApp 	:= Nil
Local oExcel

If !IsBlind()
	cPath	:= cGetFile( OemToAnsi(STR0083) + "|*.*", OemToAnsi(STR0084), 0,, .T., GETF_LOCALHARD + GETF_RETDIRECTORY, .T. )//"Diret�rio"##"Selecione um diret�rio para a gera��o do relat�rio"
Else
	cPath	:= cDefPath
EndIf

oExcel  := FWMSExcel():New()

cAba1   := OemToAnsi(STR0181)//"Pagamentos"
cAba2   := OemToAnsi(STR0182)//"Rubricas"
cAba3   := OemToAnsi(STR0183)//"Pens�o Aliment."
cAba4   := OemToAnsi(STR0185)//"F�rias"
cAba5   := OemToAnsi(STR0186)//"Pens�o F�rias"
cAba6   := OemToAnsi(STR0090)//"Inconsist�ncias"
cAba7   := OemToAnsi(STR0091)//"Legenda"

cTabela1 := OemToAnsi(STR0181)//"Pagamentos"
cTabela2 := OemToAnsi(STR0182)//"Rubricas"
cTabela3 := OemToAnsi(STR0184)//"Pens�o Aliment�cia"
cTabela4 := OemToAnsi(STR0185)//"F�rias"
cTabela5 := OemToAnsi(STR0186)//"Pens�o F�rias"
cTabela6 := OemToAnsi(STR0090)//"Inconsist�ncias"
cTabela7 := OemToAnsi(STR0091)//"Legenda"

// Cria��o de nova aba
oExcel:AddworkSheet(cAba1)
oExcel:AddworkSheet(cAba2)
oExcel:AddworkSheet(cAba3)
oExcel:AddworkSheet(cAba4)
oExcel:AddworkSheet(cAba5)
oExcel:AddworkSheet(cAba6)
oExcel:AddworkSheet(cAba7)

// Cria��o de tabela
oExcel:AddTable(cAba1, cTabela1)
oExcel:AddTable(cAba2, cTabela2)
oExcel:AddTable(cAba3, cTabela3)
oExcel:AddTable(cAba4, cTabela4)
oExcel:AddTable(cAba5, cTabela5)
oExcel:AddTable(cAba6, cTabela6)
oExcel:AddTable(cAba7, cTabela7)

// Cria��o de colunas
oExcel:AddColumn(cAba1, cTabela1, OemToAnsi(STR0095) ,1,1,.F.)//"Filial do Funcion�rio"
oExcel:AddColumn(cAba1, cTabela1, OemToAnsi(STR0102) ,1,1,.F.)//"Matricula - Matr�cula eSocial"
oExcel:AddColumn(cAba1, cTabela1, OemToAnsi(STR0096) ,1,1,.F.)//"CPF do Funcion�rio"
oExcel:AddColumn(cAba1, cTabela1, OemToAnsi(STR0097) ,1,1,.F.)//"Nome do Funcion�rio"
oExcel:AddColumn(cAba1, cTabela1, OemToAnsi(STR0187) ,1,1,.F.)//"Data de Pagamento"
oExcel:AddColumn(cAba1, cTabela1, OemToAnsi(STR0099) ,1,1,.F.)//"ideDmDev"
oExcel:AddColumn(cAba1, cTabela1, OemToAnsi(STR0188) ,1,1,.F.)//"Tipo do Pagamento"
oExcel:AddColumn(cAba1, cTabela1, OemToAnsi(STR0189) ,1,1,.F.)//"M�s de refer�ncia"
oExcel:AddColumn(cAba1, cTabela1, OemToAnsi(STR0190) ,1,3,.F.)//"Valor l�quido"
oExcel:AddColumn(cAba1, cTabela1, OemToAnsi(STR0194) ,1,3,.F.)//"Valor dependentes"

oExcel:AddColumn(cAba2, cTabela2, OemToAnsi(STR0095) ,1,1,.F.)//"Filial do Funcion�rio"
oExcel:AddColumn(cAba2, cTabela2, OemToAnsi(STR0102) ,1,1,.F.)//"Matricula - Matr�cula eSocial"
oExcel:AddColumn(cAba2, cTabela2, OemToAnsi(STR0096) ,1,1,.F.)//"CPF do Funcion�rio"
oExcel:AddColumn(cAba2, cTabela2, OemToAnsi(STR0097) ,1,1,.F.)//"Nome do Funcion�rio"
oExcel:AddColumn(cAba2, cTabela2, OemToAnsi(STR0187) ,1,1,.F.)//"Data de Pagamento"
oExcel:AddColumn(cAba2, cTabela2, OemToAnsi(STR0099) ,1,1,.F.)//"ideDmDev"
oExcel:AddColumn(cAba2, cTabela2, OemToAnsi(STR0188) ,1,1,.F.)//"Tipo do Pagamento"
oExcel:AddColumn(cAba2, cTabela2, OemToAnsi(STR0189) ,1,1,.F.)//"M�s de refer�ncia"
oExcel:AddColumn(cAba2, cTabela2, OemToAnsi(STR0191) ,1,1,.F.)//"Rubrica"
oExcel:AddColumn(cAba2, cTabela2, OemToAnsi(STR0192) ,1,1,.F.)//"Descri��o"
oExcel:AddColumn(cAba2, cTabela2, OemToAnsi(STR0193) ,1,3,.F.)//"Valor"

oExcel:AddColumn(cAba3, cTabela3, OemToAnsi(STR0095) ,1,1,.F.)//"Filial do Funcion�rio"
oExcel:AddColumn(cAba3, cTabela3, OemToAnsi(STR0102) ,1,1,.F.)//"Matricula - Matr�cula eSocial"
oExcel:AddColumn(cAba3, cTabela3, OemToAnsi(STR0096) ,1,1,.F.)//"CPF do Funcion�rio"
oExcel:AddColumn(cAba3, cTabela3, OemToAnsi(STR0097) ,1,1,.F.)//"Nome do Funcion�rio"
oExcel:AddColumn(cAba3, cTabela3, OemToAnsi(STR0187) ,1,1,.F.)//"Data de Pagamento"
oExcel:AddColumn(cAba3, cTabela3, OemToAnsi(STR0099) ,1,1,.F.)//"ideDmDev"
oExcel:AddColumn(cAba3, cTabela3, OemToAnsi(STR0188) ,1,1,.F.)//"Tipo do Pagamento"
oExcel:AddColumn(cAba3, cTabela3, OemToAnsi(STR0189) ,1,1,.F.)//"M�s de refer�ncia"
oExcel:AddColumn(cAba3, cTabela3, OemToAnsi(STR0195) ,1,1,.F.)//"CPF do benefici�rio"
oExcel:AddColumn(cAba3, cTabela3, OemToAnsi(STR0196) ,1,1,.F.)//"Data de Nascimento"
oExcel:AddColumn(cAba3, cTabela3, OemToAnsi(STR0197) ,1,1,.F.)//"Nome do benefici�rio"
oExcel:AddColumn(cAba3, cTabela3, OemToAnsi(STR0198) ,1,3,.F.)//"Valor da pens�o"
oExcel:AddColumn(cAba3, cTabela3, OemToAnsi(STR0199) ,1,1,.F.)//"C�digo GPE"

oExcel:AddColumn(cAba4, cTabela4, OemToAnsi(STR0095) ,1,1,.F.)//"Filial do Funcion�rio"
oExcel:AddColumn(cAba4, cTabela4, OemToAnsi(STR0102) ,1,1,.F.)//"Matricula - Matr�cula eSocial"
oExcel:AddColumn(cAba4, cTabela4, OemToAnsi(STR0096) ,1,1,.F.)//"CPF do Funcion�rio"
oExcel:AddColumn(cAba4, cTabela4, OemToAnsi(STR0097) ,1,1,.F.)//"Nome do Funcion�rio"
oExcel:AddColumn(cAba4, cTabela4, OemToAnsi(STR0098) ,1,1,.F.)//"Categoria eSocial"
oExcel:AddColumn(cAba4, cTabela4, OemToAnsi(STR0187) ,1,1,.F.)//"Data de Pagamento"
oExcel:AddColumn(cAba4, cTabela4, OemToAnsi(STR0200) ,1,1,.F.)//"Inicio Gozo"
oExcel:AddColumn(cAba4, cTabela4, OemToAnsi(STR0201) ,1,1,.F.)//"Qtde de dias"
oExcel:AddColumn(cAba4, cTabela4, OemToAnsi(STR0202) ,1,3,.F.)//"Valor l�quido"
oExcel:AddColumn(cAba4, cTabela4, OemToAnsi(STR0191) ,1,1,.F.)//"Rubrica"
oExcel:AddColumn(cAba4, cTabela4, OemToAnsi(STR0192) ,1,1,.F.)//"Descri��o"
oExcel:AddColumn(cAba4, cTabela4, OemToAnsi(STR0193) ,1,3,.F.)//"Valor"

oExcel:AddColumn(cAba5, cTabela5, OemToAnsi(STR0095) ,1,1,.F.)//"Filial do Funcion�rio"
oExcel:AddColumn(cAba5, cTabela5, OemToAnsi(STR0102) ,1,1,.F.)//"Matricula - Matr�cula eSocial"
oExcel:AddColumn(cAba5, cTabela5, OemToAnsi(STR0096) ,1,1,.F.)//"CPF do Funcion�rio"
oExcel:AddColumn(cAba5, cTabela5, OemToAnsi(STR0097) ,1,1,.F.)//"Nome do Funcion�rio"
oExcel:AddColumn(cAba5, cTabela5, OemToAnsi(STR0098) ,1,1,.F.)//"Categoria eSocial"
oExcel:AddColumn(cAba5, cTabela5, OemToAnsi(STR0187) ,1,1,.F.)//"Data de Pagamento"
oExcel:AddColumn(cAba5, cTabela5, OemToAnsi(STR0200) ,1,1,.F.)//"In�cio Gozo"
oExcel:AddColumn(cAba5, cTabela5, OemToAnsi(STR0201) ,1,1,.F.)//"Qtde de dias"
oExcel:AddColumn(cAba5, cTabela5, OemToAnsi(STR0195) ,1,1,.F.)//"CPF do benefici�rio"
oExcel:AddColumn(cAba5, cTabela5, OemToAnsi(STR0196) ,1,1,.F.)//"Data de Nascimento"
oExcel:AddColumn(cAba5, cTabela5, OemToAnsi(STR0197) ,1,1,.F.)//"Nome do benefici�rio"
oExcel:AddColumn(cAba5, cTabela5, OemToAnsi(STR0198) ,1,3,.F.)//"Valor da pens�o"
oExcel:AddColumn(cAba5, cTabela5, OemToAnsi(STR0199) ,1,1,.F.)//"C�digo GPE"

oExcel:AddColumn(cAba6, cTabela6, OemToAnsi(STR0095) ,1,1,.F.)//"Filial do Funcion�rio"
oExcel:AddColumn(cAba6, cTabela6, OemToAnsi(STR0096) ,1,1,.F.)//"CPF do Funcion�rio"
oExcel:AddColumn(cAba6, cTabela6, OemToAnsi(STR0132) ,1,1,.F.)//"Inconsist�ncias"

oExcel:AddColumn(cAba7, cTabela7, OemToAnsi(STR0133) ,1,1,.F.)//"Tipo"
oExcel:AddColumn(cAba7, cTabela7, OemToAnsi(STR0106) ,1,1,.F.)//"Valor"

//Gera��o das informa��es
For nCntIdeBen := 1 To Len(aIdeBenef)
	lGerouInfo := .F.
	For nCntInfPgFl	:= 1 To Len(aInfoPgto)
		If aInfoPgto[nCntInfPgFl, 5] == aIdeBenef[nCntIdeBen, 1]//aIdeBenef -> aInfoPgto | CPF
			If aInfoPgto[nCntInfPgFl, 2] == "7"//Ferias
				For nCntRetFer := 1 To Len(aRetFer)
					If aInfoPgto[nCntInfPgFl, 1] + aInfoPgto[nCntInfPgFl, 5] == aRetFer[nCntRetFer, 6] + aRetFer[nCntRetFer, 8]//infoPgto -> aRetFer | dtPgto + CPF
						lGerouInfo := .T.
						For nCntRubFer := 1 To Len(aRetFer[nCntRetFer, 7])
							oExcel:AddRow(cAba4, cTabela4, { aRetFer[nCntRetFer, 9], aRetFer[nCntRetFer, 10], aRetFer[nCntRetFer, 8], aRetFer[nCntRetFer, 11], aRetFer[nCntRetFer, 1], dToC(sToD(aInfoPgto[nCntInfPgFl, 1])), dToC(sToD(aRetFer[nCntRetFer, 3])), aRetFer[nCntRetFer, 4], aRetFer[nCntRetFer, 5], aRetFer[nCntRetFer, 7, nCntRubFer, 1], RetValSrv( aRetFer[nCntRetFer, 7, nCntRubFer, 1], aRetFer[nCntRetFer, 9], 'RV_DESC' ), Abs(aRetFer[nCntRetFer, 7, nCntRubFer, 6]) } )
							For nCntRetPens	:= 1 To Len(aRetPensao)
								If aRetFer[nCntRetFer, 8] == aRetPensao[nCntRetPens, 7] .And. aRetFer[nCntRetFer, 7, nCntRubFer, 1] == aRetPensao[nCntRetPens, 1] .And. aRetFer[nCntRetFer, 2]+aRetFer[nCntRetFer, 6]+"7" == aRetPensao[nCntRetPens, 6]//aRetFer -> aRetPensao | codRubr + matricula + dtPgto + tpPgto + CPF
									oExcel:AddRow(cAba5, cTabela5, { aRetFer[nCntRetFer, 9], aRetFer[nCntRetFer, 10], aRetFer[nCntRetFer, 8], aRetFer[nCntRetFer, 11], aRetFer[nCntRetFer, 1], dToC(sToD(aInfoPgto[nCntInfPgFl, 1])), dToC(sToD(aRetFer[nCntRetFer, 3])), aRetFer[nCntRetFer, 4], aRetPensao[nCntRetPens, 4], dToC(sToD(aRetPensao[nCntRetPens, 2])), aRetPensao[nCntRetPens, 3], Abs(aRetFer[nCntRetFer, 7, nCntRubFer, 6]), aRetPensao[nCntRetPens, 8] } )
								EndIf
							Next nCntRetPens
						Next nCntRubFer
					EndIf
				Next nCntRetFer
			Else
				For nCntDetPgTo	:= 1 To Len(adetPgtoFl)
					If (adetPgtoFl[nCntDetPgTo, 7] + adetPgtoFl[nCntDetPgTo, 8] == aInfoPgto[nCntInfPgFl, 1] + aInfoPgto[nCntInfPgFl, 2] + aInfoPgto[nCntInfPgFl, 5]) .And. (lAfast .Or. adetPgtoFl[nCntDetPgTo, 4] > 0 .Or. aScan( aRetPgtoTot, { |x| x[7] == adetPgtoFl[nCntDetPgTo, 2] .And. x[9] == adetPgtoFl[nCntDetPgTo, 8] } ) > 0 ) //infoPgto -> detPgtoFl | dtPgto + tpPgto + CPF
						lGerouInfo := .T.
						oExcel:AddRow(cAba1, cTabela1, { adetPgtoFl[nCntDetPgTo, 11], adetPgtoFl[nCntDetPgTo, 9], adetPgtoFl[nCntDetPgTo, 8], adetPgtoFl[nCntDetPgTo, 10], dToC(sToD(aInfoPgto[nCntInfPgFl, 1])), adetPgtoFl[nCntDetPgTo, 2], aInfoPgto[nCntInfPgFl, 2], adetPgtoFl[nCntDetPgTo, 1], adetPgtoFl[nCntDetPgTo, 4], aIdeBenef[nCntIdeBen, 2] } )
						For nCntRetPgTo	:= 1 To Len(aRetPgtoTot)
							If adetPgtoFl[nCntDetPgTo, 2] + adetPgtoFl[nCntDetPgTo, 8] == aRetPgtoTot[nCntRetPgTo, 7] + aRetPgtoTot[nCntRetPgTo, 9]//detPgtoFl -> retPgtoTot | ideDmDev + CPF
								oExcel:AddRow(cAba2, cTabela2, { adetPgtoFl[nCntDetPgTo, 11], adetPgtoFl[nCntDetPgTo, 9], adetPgtoFl[nCntDetPgTo, 8], adetPgtoFl[nCntDetPgTo, 10], dToC(sToD(aInfoPgto[nCntInfPgFl, 1])), adetPgtoFl[nCntDetPgTo, 2], aInfoPgto[nCntInfPgFl, 2], adetPgtoFl[nCntDetPgTo, 1], aRetPgtoTot[nCntRetPgTo, 1], RetValSrv( aRetPgtoTot[nCntRetPgTo, 1], adetPgtoFl[nCntDetPgTo, 11], 'RV_DESC' ), Abs(aRetPgtoTot[nCntRetPgTo, 6]) } )
								For nCntRetPens	:= 1 To Len(aRetPensao)
									If aRetPgtoTot[nCntRetPgTo, 9] == aRetPensao[nCntRetPens, 7] .And. aRetPgtoTot[nCntRetPgTo, 1] == aRetPensao[nCntRetPens, 1] .And. aRetPgtoTot[nCntRetPgTo, 7] == aRetPensao[nCntRetPens, 6]//retPgtoTot -> aRetPensao | codRubr + ideDmDev + CPF
										oExcel:AddRow(cAba3, cTabela3, { adetPgtoFl[nCntDetPgTo, 11], adetPgtoFl[nCntDetPgTo, 9], adetPgtoFl[nCntDetPgTo, 8], adetPgtoFl[nCntDetPgTo, 10], dToC(sToD(aInfoPgto[nCntInfPgFl, 1])), adetPgtoFl[nCntDetPgTo, 2], aInfoPgto[nCntInfPgFl, 2], adetPgtoFl[nCntDetPgTo, 1], aRetPensao[nCntRetPens, 4], dToC(sToD(aRetPensao[nCntRetPens, 2])), aRetPensao[nCntRetPens, 3], Abs(aRetPgtoTot[nCntRetPgTo, 6]), aRetPensao[nCntRetPens, 8] } )
									EndIf
								Next nCntRetPens
							EndIf
						Next nCntRetPgTo
					EndIf
				Next nCntDetPgTo
			EndIf
		EndIf
	Next nCntInfPgFl
	If !lGerouInfo
		aAdd( aRelIncons, { aIdeBenef[nCntIdeBen, 3], aIdeBenef[nCntIdeBen, 1], OemToAnsi(STR0207) } )//"Funcion�rio foi desprezado pois est� sem movimento"
	EndIf
Next nCntIdeBen

For nCntIncons := 1 To Len(aRelIncons)
	oExcel:AddRow(cAba6, cTabela6, { aRelIncons[nCntIncons, 1], aRelIncons[nCntIncons, 2], aRelIncons[nCntIncons, 3] } )
Next nCntIncons

oExcel:AddRow(cAba7, cTabela7, { OemToAnsi(STR0203), OemToAnsi(STR0204)+OemToAnsi(STR0205) } )//"Tipos de Pagamento"##'1-Pagamento de remunera��o, conforme apurado em {dmDev} do S-1200 | 2-Pagamento de verbas rescis�rias conforme apurado em {dmDev} do S-2299 | 3-Pagamento de verbas rescis�rias conforme apurado em {dmDev} do S-2399 | 5-Pagamento de remunera��o conforme apurado em {dmDev} do S-1202'##'6-Pagamento de Benef�cios Previdenci�rios, conforme apurado em {dmDev} do S-1207 | 7-Recibo de f�rias | 9-Pagamento relativo a compet�ncias anteriores ao in�cio de obrigatoriedade dos eventos peri�dicos para o contribuinte'

If !Empty(oExcel:aWorkSheet)
    oExcel:Activate() //ATIVA O EXCEL
    oExcel:GetXMLFile(cArquivo)

    If !IsBlind()
		CpyS2T(cDefPath+cArquivo, cPath)
		If ApOleClient( "MSExcel" )
			oExcelApp := MsExcel():New()
			oExcelApp:WorkBooks:Open(cPath+cArquivo) // Abre a planilha
			oExcelApp:SetVisible(.T.)
		EndIf
	EndIf
EndIf

Return
