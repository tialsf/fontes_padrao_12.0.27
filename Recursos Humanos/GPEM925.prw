#INCLUDE 'Protheus.ch'
#INCLUDE "TopConn.ch"
#INCLUDE "GPEM925.ch"
/*/{Protheus.doc} GPEM925
    
    Carga Inicial SIGAGPE x NG (AP)

    @type  Function
    @author rafaelalmeida
    @since 08/06/2020
    @version 12,1,27
    
    /*/
Function GPEM925()

Local cUserId   := SubStr(cUsuario,7,15)
Local cMsg      := ""
Local cLog      := ""
Local cFilIni   := ""
Local cFilFin   := ""
Local cMatIni   := ""
Local cMatFin   := ""
Local cPesIni   := ""
Local cPesFin   := ""
Local cCrgIni   := ""
Local cCrgFin   := ""
Local lProcSRA  := .F.
Local lProcSRB  := .F.
Local lProcRD0  := .F.
Local lProcSQB  := .F.
Local lProcSQ3  := .F.
Local lLog      := .F.
Local aLogSra   := {}
Local aLogSrb   := {}
Local aLogRd0   := {}
Local aLogSqb   := {}
Local aLogSq3   := {}


If !SuperGetMv("MV_RHNG",.F. ,.F.)
    MsgStop(STR0001+CRLF+;//##"A integra��o SIGAGPE x NG n�o est� configurada neste ambiente."
            STR0002,STR0003)//##"Verifique se o seu ambiente est� par�metrizado corretamente!"##"Par�metro MV_RHNG"
    Return Nil
EndIf

If FPergunte("GPEM925") .And.;
    MsgNoYes(   STR0004+CRLF+;//##"Este processamento pode levar alguns minutos."
                STR0005+CRLF+CRLF+;//##"O tempo de processamentop pode variar de acordo com o tamanho da sua base de dados!"
                STR0006)//##"Tem certeza que deseja realizar o processo de carga inicial?"

    cFilIni     := MV_PAR01
    cFilFin     := MV_PAR02

    lProcSRA    := MV_PAR03
    lProcSRB    := MV_PAR04
    cMatIni     := MV_PAR05
    cMatFin     := MV_PAR06
    
    lProcRD0    := MV_PAR07
    cPesIni     := MV_PAR08
    cPesFin     := MV_PAR09

    lProcSQB    := MV_PAR10
    
    cCrgIni     := MV_PAR11
    cCrgFin     := MV_PAR12
    lProcSQ3    := MV_PAR13

    lLog        := MV_PAR14

Else 
	Return Nil
EndIf

If lProcSRA
    Processa({||LoadSRA(cUserId,@cMsg,@aLogSra,cFilIni,cFilFin,cMatIni,cMatFin)}, STR0007, STR0008,.F.)//##"Aguarde..."##"Processando Cadastro de Funcion�rios (SRA)..."
    cLog += cMsg + CRLF
EndIf

If lProcSRB
    Processa({||LoadSRB(cUserId,@cMsg,@aLogSrb,cFilIni,cFilFin,cMatIni,cMatFin)}, STR0007, STR0009,.F.)//##"Aguarde..."##"Processando Cadastro de Dependentes (SRB)..."
    cLog += cMsg + CRLF
EndIf

If lProcRD0
    Processa({||LoadRD0(cUserId,@cMsg,@aLogRd0,cFilIni,cFilFin,cPesIni,cPesFin)}, STR0007, STR0010,.F.)//##"Aguarde..."##"Processando  Cadastro de Pessoas (RD0)..."
    cLog += cMsg + CRLF
EndIf

If lProcSQB
    Processa({||LoadSQB(cUserId,@cMsg,@aLogSqb,cFilIni,cFilFin)}, STR0007, STR0046,.F.)//##"Aguarde..."##"Processando Cadastro de Departamentos (SQB)..."
    cLog += cMsg + CRLF
EndIf

If lProcSQ3
    Processa({||LoadSQ3(cUserId,@cMsg,@aLogSq3,cFilIni,cFilFin, cCrgIni,cCrgFin)}, STR0007, STR0053,.F.)//##"Aguarde..."##"Processando Cadastro de Cargos (SQ3)..."
    cLog += cMsg + CRLF
EndIf

If lProcSRA .Or. lProcSRB .Or. lProcRD0 .Or. lProcSQB .Or. lProcSQ3 .or. lLog
    If lLog .And. (!Empty(aLogSRA) .Or. !Empty(aLogSRB) .Or. !Empty(aLogRd0) .Or. !Empty(aLogSqb) .Or. !Empty(aLogSq3))
        Processa({||GravaLog(aLogSRA,aLogSRB,aLogRd0,aLogSqb,aLogSq3)}, STR0007, STR0011,.F.)//##"Aguarde..."##"Gerando arquivo de Log..."
    EndIf
    
    MsgInfo(cLog,STR0012)//##"Fim do Processamento da Carga Inicial"
Else
    MsgStop(STR0013+CRLF+;//##"Voc� marcou como n�o em todos os par�metros!"
            STR0014)//##"Nenhum dado ser� processado!"
EndIf


Return Nil


/*/{Protheus.doc} LoadSRA
    
    Realiza a carga dos registros da SRA.
    
    @type  Static Function
    @author rafaelalmeida
    @since 08/06/2020
    @version 12.1.27
    @param cUserId, Character, C�digo do Usu�rio
    @param cMsg , Character, Mensagem de processamento.
    @param aLog , Character, Array contendo log de processamento
    /*/
Static Function LoadSRA(cUserId,cMsg,aLog,cFilIni,cFilFin,cMatIni,cMatFin)

Local aAreOld := {GetArea()}

Local cAlsQry   := GetNextAlias()
Local cChave    := "" 
Local cSql      := ""
Local cTmpFil   := xFilial("SRA")
Local cTime     := ""
Local nTotalReg := 0 

Default cUserId   := SubStr(cUsuario,7,15)
Default cMsg    := ""
Default aLog    := ""
Default cFilIni := ""
Default cFilFin := ""
Default cMatIni := ""
Default cMatFin := ""

cSql := " SELECT RA_FILIAL,RA_MAT"
cSql += " FROM "+RetSqlName("SRA")+" SRA"
cSql += " WHERE "
cSql += " 		RA_FILIAL BETWEEN '"+cFilIni+"' AND '"+cFilFin+"'"
cSql += "   AND RA_MAT BETWEEN '"+cMatIni+"' AND '"+cMatFin+"'"
cSql += " 	AND	RA_SITFOLH <> 'D'"
cSql += " 	AND SRA.D_E_L_E_T_ = ''"
cSql += " 	AND NOT EXISTS (SELECT RJP.R_E_C_N_O_"
cSql += " 				FROM "+RetSqlName("RJP")+" RJP"
cSql += " 				WHERE"
cSql += " 						RJP_FILIAL = '"+xFilial("RJP")+"'"
cSql += " 					AND	RJP_KEY = '"+cEmpAnt+"'+'|'+RA_FILIAL+'|'+RA_MAT"
cSql += " 					AND RJP_TAB = 'SRA'"
cSql += " 					AND RJP.D_E_L_E_T_ = '' )"


MPSysOpenQuery( cSql, cAlsQry)
(cAlsQry)->(dbEval({||nTotalReg++}))
(cAlsQry)->(dbGoTop())


ProcRegua(nTotalReg)
While !(cAlsQry)->(Eof())
    IncProc()
    
    cChave  := cEmpAnt + "|" + (cAlsQry)->RA_FILIAL + "|" + (cAlsQry)->RA_MAT
    cTime   := Time()
    Aadd(aLog,{cChave,dDataBase,cTime,cUserId})
    Begin Transaction
        fSetInforRJP(cTmpFil, (cAlsQry)->RA_MAT, "SRA", cChave, "I",  dDataBase, cTime, cUserId)
        RJP->(dbSetOrder(6))//RJP_FILIAL+RJP_KEY
        If RJP->(dbSeek(xFilial("RJP")+cChave))
            RecLock("RJP",.F.)
            RJP->RJP_CGINIC := '1'
            RJP->(MsUnlock())
        Else 
            DisarmTransaction()
        EndIf
    End Transaction
    (cAlsQry)->(dbSkip())
EndDo

If nTotalReg == 0
    cMsg := STR0015//##"N�o foram encontrados registros eleg�veis para processamento no cadastro de funcion�rios. (SRA)"
else
    cMsg := STR0016+cValToChar(nTotalReg)+STR0017//##"Foram processados "##" registros do cadastro de funcion�rios (SRA)."
EndIf

(cAlsQry)->(dbCloseArea())
aEval(aAreOld, {|xAux| RestArea(xAux)})

Return Nil

/*/{Protheus.doc} LoadSRB
    
    Realiza a carga dos registros da SRB.

    @type  Static Function
    @author rafaelalmeida
    @since 08/06/2020
    @version 12.1.27
    @param cUserId, Character, C�digo do Usu�rio
    @param cMsg , Character, Mensagem de processamento.
    @param aLog , Character, Array contendo log de processamento
 
    /*/
Static Function LoadSRB(cUserId,cMsg,aLog,cFilIni,cFilFin,cMatIni,cMatFin)

Local aAreOld := {GetArea()}

Local cAlsQry   := GetNextAlias()
Local cChave    := "" 
Local cSql      := ""
Local cTmpFil   := xFilial("SRB")
Local cTime     := ""

Local nTotalReg := 0 

Default cUserId := SubStr(cUsuario,7,15)
Default cMsg    := ""
Default aLog    := ""
Default cFilIni := ""
Default cFilFin := ""
Default cMatIni := ""
Default cMatFin := ""

cSql := " SELECT RB_FILIAL,RB_MAT,RB_COD" 
cSql += " FROM "+RetSqlName("SRB")+" SRB"
cSql += " INNER JOIN "+RetSqlName("SRA")+" SRA ON"
cSql += " 		RA_FILIAL = RB_FILIAL"
cSql += " 	AND RA_MAT = RB_MAT"
cSql += " 	AND RA_SITFOLH <> 'D'"
cSql += " 	AND SRA.D_E_L_E_T_ = ''"
cSql += " WHERE"
cSql += "     RB_FILIAL BETWEEN '"+cFilIni+"' AND '"+cFilFin+"'"
cSql += " AND RB_MAT BETWEEN '"+cMatIni+"' AND '"+cMatFin+"'"
cSql += " AND SRB.D_E_L_E_T_ = ''"
cSql += " AND NOT EXISTS (SELECT RJP.R_E_C_N_O_"
cSql += " 				FROM "+RetSqlName("RJP")+" RJP" 
cSql += " 				WHERE "
cSql += " 						RJP_FILIAL = '"+xFilial("RJP")+"'"
cSql += " 					AND RJP_KEY = '"+cEmpAnt+"' + '|' + RB_FILIAL + '|' + RB_MAT + '|' + RB_COD"
cSql += " 					AND RJP_TAB = 'SRB'"
cSql += " 					AND RJP.D_E_L_E_T_ = '' )"

MPSysOpenQuery( cSql, cAlsQry)
(cAlsQry)->(dbEval({||nTotalReg++}))

(cAlsQry)->(dbGoTop())

ProcRegua(nTotalReg)

While !(cAlsQry)->(Eof())
    IncProc()
    
    cChave    := cEmpAnt + "|" + (cAlsQry)->RB_FILIAL + "|" + (cAlsQry)->RB_MAT + "|" + (cAlsQry)->RB_COD
    cTime   := Time()
    Aadd(aLog,{cChave,dDataBase,cTime,cUserId})
    Begin Transaction
        fSetInforRJP(cTmpFil, (cAlsQry)->RB_MAT, "SRB", cChave, "I",  dDataBase, cTime, cUserId)
        RJP->(dbSetOrder(6))//RJP_FILIAL+RJP_KEY
        If RJP->(dbSeek(xFilial("RJP")+cChave))
            RecLock("RJP",.F.)
            RJP->RJP_CGINIC := '1'
            RJP->(MsUnlock())
        Else 
            DisarmTransaction()
        EndIf
    End Transaction
    (cAlsQry)->(dbSkip())
EndDo

If nTotalReg == 0
    cMsg := STR0018//##"N�o foram encontrados registros eleg�veis para processamento no cadastro de dependentes. (SRB)"
else
    cMsg := STR0016+cValToChar(nTotalReg)+STR0019//##"Foram processados "##" registros do cadastro de dependentes (SRB)."
EndIf

(cAlsQry)->(dbCloseArea())


aEval(aAreOld, {|xAux| RestArea(xAux)})
    
Return Nil

/*/{Protheus.doc} LoadRD0
    
    Realiza a carga dos registros da SRB.

    @type  Static Function
    @author rafaelalmeida

    @since 08/06/2020
    @version 12.1.27
    @param cUserId, Character, C�digo do Usu�rio.
    @param cMsg , Character, Mensagem de processamento.
    @param aLog , Character, Array contendo log de processamento
    /*/
Static Function LoadRD0(cUserId,cMsg,aLog,cFilIni,cFilFin,cPesIni,cPesFin)

Local aAreOld := {GetArea()}

Local cAlsQry   := GetNextAlias()
Local cTmpFil   := xFilial ("RD0")
Local cTime     := ""
Local cChave    := "" 
Local cSql      := ""

Local nTotalReg := 0 

Default cUserId   := SubStr(cUsuario,7,15)
Default cMsg    := ""
Default aLog    := ""
Default cFilIni := ""
Default cFilFin := ""
Default cPesIni := ""
Default cPesFin := ""

cSql := " SELECT RD0_FILIAL,RD0_CODIGO FROM "+RetSqlName("RD0")+" RD0"
cSql += " WHERE"
cSql += " 	    RD0_FILIAL BETWEEN '"+cFilIni+"' AND '"+cFilFin+"'"
cSql += " 	AND RD0_CODIGO BETWEEN '"+cPesIni+"' AND '"+cPesFin+"'"
cSql += "   AND RD0.D_E_L_E_T_ = ''"
cSql += "   AND NOT EXISTS (SELECT RJP.R_E_C_N_O_" 
cSql += " 	    			FROM "+RetSqlName("RJP")+" RJP"
cSql += " 		    		WHERE "
cSql += " 			    			RJP_FILIAL = '"+xFilial("RJP")+"'"
cSql += " 				    	AND RJP_KEY = '"+cEmpAnt+"'+'|'+RD0_FILIAL+'|'+RD0_CODIGO"
cSql += " 					    AND RJP_TAB = 'RD0'"
cSql += " 					    AND RJP.D_E_L_E_T_ = '' )"

MPSysOpenQuery( cSql, cAlsQry)
(cAlsQry)->(dbEval({||nTotalReg++}))

(cAlsQry)->(dbGoTop())

ProcRegua(nTotalReg)

While !(cAlsQry)->(Eof())
    IncProc()

	cChave    := cEmpAnt + "|" + (cAlsQry)->RD0_FILIAL + "|" + (cAlsQry)->RD0_CODIGO
    cTime   := Time()
    Aadd(aLog,{cChave,dDataBase,cTime,cUserId})
    Begin Transaction
        fSetInforRJP(cTmpFil, (cAlsQry)->RD0_CODIGO, "RD0", cChave, "I",  dDataBase, cTime, cUserId)
        RJP->(dbSetOrder(6))//RJP_FILIAL+RJP_KEY
        If RJP->(dbSeek(xFilial("RJP")+cChave))
            RecLock("RJP",.F.)
            RJP->RJP_CGINIC := '1'
            RJP->(MsUnlock())
        Else 
            DisarmTransaction()
        EndIf
    End Transaction
    (cAlsQry)->(dbSkip())
EndDo

If nTotalReg == 0
    cMsg := STR0020//##"N�o foram encontrados registros eleg�veis para processamento no cadastro de pessoas. (RD0)"
else
    cMsg := STR0016+cValToChar(nTotalReg)+STR0021//##"Foram processados "##" registros do cadastro de pessoas (RD0)."
EndIf

(cAlsQry)->(dbCloseArea())

aEval(aAreOld, {|xAux| RestArea(xAux)})
    
Return Nil

/*/{Protheus.doc} LoadSQB
    
    Realiza a carga dos registros da SQB.

    @type  Static Function
    @author brdwc0032

    @since 13/08/2020
    @version 12.1.27
    @param cUserId, Character, C�digo do Usu�rio.
    @param cMsg , Character, Mensagem de processamento.
    @param aLog , Character, Array contendo log de processamento
    @param cFilIni , Character, Filial inicial.
    @param cFilFin , Character, Filial final.
    /*/
Static Function LoadSQ3(cUserId,cMsg,aLog,cFilIni,cFilFin, cCrgIni, cCrgFin)

Local aAreOld := {GetArea()}

Local cAlsQry   := GetNextAlias()
Local cTmpFil   := xFilial ("SQ3")
Local cTime     := ""
Local cChave    := "" 
Local cSql      := ""

Local nTotalReg := 0 

Default cUserId   := SubStr(cUsuario,7,15)
Default cMsg    := ""
Default aLog    := ""
Default cFilIni := ""
Default cFilFin := ""

cSql := " SELECT Q3_FILIAL,Q3_CARGO, Q3_CC FROM "+RetSqlName("SQ3")+" SQ3"
cSql += " WHERE"
cSql += " 	    Q3_FILIAL BETWEEN '"+cFilIni+"' AND '"+cFilFin+"'"
cSql += " 	AND Q3_CARGO BETWEEN ' "+cCrgIni+"' AND '"+cCrgFin+"'"
cSql += "   AND SQ3.D_E_L_E_T_ = ''"
cSql += "   AND NOT EXISTS (SELECT RJP.R_E_C_N_O_" 
cSql += " 	    			FROM "+RetSqlName("RJP")+" RJP"
cSql += " 		    		WHERE "
cSql += " 			    			RJP_FILIAL = '"+xFilial("RJP")+"'"
cSql += " 				    	AND RJP_KEY = '"+cEmpAnt+"'+'|'+Q3_FILIAL+'|'+Q3_CARGO+'|'+Q3_CC"
cSql += " 					    AND RJP_TAB = 'SQ3'"
cSql += " 					    AND RJP.D_E_L_E_T_ = '' )"

MPSysOpenQuery( cSql, cAlsQry)
(cAlsQry)->(dbEval({||nTotalReg++}))

(cAlsQry)->(dbGoTop())

ProcRegua(nTotalReg)

While !(cAlsQry)->(Eof())
    IncProc()

	cChave    := cEmpAnt + "|" + (cAlsQry)->Q3_FILIAL + "|" + (cAlsQry)->Q3_CARGO + "|" + (cAlsQry)->Q3_CC
    cTime   := Time()
    Aadd(aLog,{cChave,dDataBase,cTime,cUserId})
    Begin Transaction
        fSetDeptoRJP(cTmpFil, "SQ3", cChave, "I",  dDataBase, cTime, cUserId)
        RJP->(dbSetOrder(6))//RJP_FILIAL+RJP_KEY
        If RJP->(dbSeek(xFilial("RJP")+cChave))
            RecLock("RJP",.F.)
            RJP->RJP_CGINIC := '1'
            RJP->(MsUnlock())
        Else 
            DisarmTransaction()
        EndIf
    End Transaction
    (cAlsQry)->(dbSkip())
EndDo

If nTotalReg == 0
    cMsg := STR0054//##"N�o foram encontrados registros eleg�veis para processamento no cadastro de cargos. (SQ3)"
else
    cMsg := STR0016+cValToChar(nTotalReg)+STR0055//##"Foram processados "##" registros do cadastro de cargos (SQ3)."
EndIf

(cAlsQry)->(dbCloseArea())

aEval(aAreOld, {|xAux| RestArea(xAux)})
    
Return Nil

/*/{Protheus.doc} LoadSQB
    
    Realiza a carga dos registros da SQB.

    @type  Static Function
    @author brdwc0032

    @since 13/08/2020
    @version 12.1.27
    @param cUserId, Character, C�digo do Usu�rio.
    @param cMsg , Character, Mensagem de processamento.
    @param aLog , Character, Array contendo log de processamento
    @param cFilIni , Character, Filial inicial.
    @param cFilFin , Character, Filial final.
    /*/
Static Function LoadSQB(cUserId,cMsg,aLog,cFilIni,cFilFin)

Local aAreOld := {GetArea()}

Local cAlsQry   := GetNextAlias()
Local cTmpFil   := xFilial ("SQB")
Local cTime     := ""
Local cChave    := "" 
Local cSql      := ""

Local nTotalReg := 0 

Default cUserId   := SubStr(cUsuario,7,15)
Default cMsg    := ""
Default aLog    := ""
Default cFilIni := ""
Default cFilFin := ""

cSql := " SELECT QB_FILIAL,QB_DEPTO, QB_CC FROM "+RetSqlName("SQB")+" SQB"
cSql += " WHERE"
cSql += " 	    QB_FILIAL BETWEEN '"+cFilIni+"' AND '"+cFilFin+"'"
cSql += "   AND SQB.D_E_L_E_T_ = ''"
cSql += "   AND NOT EXISTS (SELECT RJP.R_E_C_N_O_" 
cSql += " 	    			FROM "+RetSqlName("RJP")+" RJP"
cSql += " 		    		WHERE "
cSql += " 			    			RJP_FILIAL = '"+xFilial("RJP")+"'"
cSql += " 				    	AND RJP_KEY = '"+cEmpAnt+"'+'|'+QB_FILIAL+'|'+QB_DEPTO"
cSql += " 					    AND RJP_TAB = 'SQB'"
cSql += " 					    AND RJP.D_E_L_E_T_ = '' )"

MPSysOpenQuery( cSql, cAlsQry)
(cAlsQry)->(dbEval({||nTotalReg++}))

(cAlsQry)->(dbGoTop())

ProcRegua(nTotalReg)

While !(cAlsQry)->(Eof())
    IncProc()

	cChave    := cEmpAnt + "|" + (cAlsQry)->QB_FILIAL + "|" + (cAlsQry)->QB_DEPTO + "|" + (cAlsQry)->QB_CC
    cTime   := Time()
    Aadd(aLog,{cChave,dDataBase,cTime,cUserId})
    Begin Transaction
        fSetDeptoRJP(cTmpFil, "SQB", cChave, "I",  dDataBase, cTime, cUserId)
        RJP->(dbSetOrder(6))//RJP_FILIAL+RJP_KEY
        If RJP->(dbSeek(xFilial("RJP")+cChave))
            RecLock("RJP",.F.)
            RJP->RJP_CGINIC := '1'
            RJP->(MsUnlock())
        Else 
            DisarmTransaction()
        EndIf
    End Transaction
    (cAlsQry)->(dbSkip())
EndDo

If nTotalReg == 0
    cMsg := STR0047//##"N�o foram encontrados registros eleg�veis para processamento no cadastro de departamentos. (SQB)"
else
    cMsg := STR0016+cValToChar(nTotalReg)+STR0048//##"Foram processados "##" registros do cadastro de departamentos (SQB)."
EndIf

(cAlsQry)->(dbCloseArea())

aEval(aAreOld, {|xAux| RestArea(xAux)})
    
Return Nil

/*/{Protheus.doc} FPergunte
    Perguntas da rotina
    @type  Static Function
    @author rafaelalmeida
    @since 08/06/2020
    @version 12.1.27
    
    @param lView, logical, Determina se ser� exibida a janela de perguntas.
    @param lEdit, logical, Determina o modo de edi��o de alguns par�metros.
    @return lRet, logical, Determina se o usu�rio confirmou ou cancelou a janela de perguntas.

    /*/

Static Function FPergunte(cNomRot,lView,lEdit)

Local aParambox	:= {}
Local aRet 		:= {}

Local lRet		:= .T.
Local nX		:= 0

Default cNomRot	:= "GPEM925"
Default lView	:= .T.
Default lEdit	:= .T.

Private lWhen	:= lEdit

/* MV_PAR01*/ aAdd( aParambox ,{1,STR0035	, CriaVar("RA_FILIAL",.F.),"@!",".T.","SM0"   ,,100,.F.}) //##"Filial De: "
/* MV_PAR02*/ aAdd( aParambox ,{1,STR0036	, CriaVar("RA_FILIAL",.F.),"@!",".T.","SM0"   ,,100,.F.}) //"Filial At�: "	
/* MV_PAR03*/ aAdd( aParamBox ,{4,STR0022   ,.F.,"",90,"",.F.})//##"Cadastro de Funcion�rios"
/* MV_PAR04*/ aAdd( aParamBox ,{4,STR0025   ,.F.,"",90,"",.F.})//##"Cadastro de Dependentes"
/* MV_PAR05*/ aAdd( aParambox ,{1,STR0037	, CriaVar("RA_MAT",.F.),"@!",".T.","SRA02A",".T.",100,.F.})//##"Matr�cula De: "
/* MV_PAR06*/ aAdd( aParambox ,{1,STR0038	, CriaVar("RA_MAT",.F.),"@!",".T.","SRA02A",".T.",100,.F.})//##"Matr�cula At�: "
/* MV_PAR07*/ aAdd( aParamBox ,{4,STR0026   ,.F.,"",90,"",.F.})//##"Cadastro de Dependentes"
/* MV_PAR08*/ aAdd( aParambox ,{1,STR0039	, CriaVar("RD0_CODIGO",.F.),"@!",".T.","RD0"   ,".T.",100,.F.})//##"Participante De: "
/* MV_PAR09*/ aAdd( aParambox ,{1,STR0040	, CriaVar("RD0_CODIGO",.F.),"@!",".T.","RD0"   ,".T.",100,.F.})//##"Participante At�: "
/* MV_PAR10*/ aAdd( aParamBox ,{4,STR0049   ,.F.,"",90,"",.F.})//##"Cadastro de Departamentos"
/* MV_PAR11*/ aAdd( aParambox ,{1,STR0051	, CriaVar("Q3_CARGO",.F.),"@!",".T.","SQ3"   ,".T.",100,.F.})//##"Cargo De: "
/* MV_PAR12*/ aAdd( aParambox ,{1,STR0052	, CriaVar("Q3_CARGO",.F.),"@!",".T.","SQ3"   ,".T.",100,.F.})//##"Cargo At�: "
/* MV_PAR13*/ aAdd( aParamBox ,{4,STR0050   ,.F.,"",90,"",.F.})//##"Cadastro de Cargos"
/* MV_PAR14*/ aAdd( aParamBox ,{4,STR0041   ,.F.,"",90,"",.F.})//##"Log de Processamento

//Carrega o array com os valores utilizados na �ltima tela ou valores Default de cada campo.
For nX := 1 To Len(aParamBox)
	aParamBox[nX][3] := ParamLoad(cNomRot,aParamBox,nX,aParamBox[nX][3])
Next nX

//Define se ira apresentar tela de perguntas
If lView
	lRet := ParamBox(aParamBox,STR0027,aRet,{|| VldPerg()},{},.T.,Nil,Nil,Nil,cNomRot,.F.,.F.)//##"Par�metros"
Else
	For nX := 1 To Len(aParamBox)
		Aadd(aRet, aParamBox[nX][3])
	Next nX
EndIf

If lRet
	//Carrega perguntas em variaveis usadas no programa
	If ValType(aRet) == "A" .And. Len(aRet) == Len(aParamBox)
		For nX := 1 to Len(aParamBox)
			If aParamBox[nX][1] == 2 .And. ValType(aRet[nX]) == "C"
				&("Mv_Par"+StrZero(nX,2)) := aScan(aParamBox[nX][4],{|x| Alltrim(x) == aRet[nX]})
			ElseIf aParamBox[nX][1] == 2 .And. ValType(aRet[nX]) == "N"
				&("Mv_Par"+StrZero(nX,2)) := aRet[nX]
			Else
				&("Mv_Par"+StrZero(nX,2)) := aRet[nX]
			Endif
		Next nX
	EndIf

	If lEdit
		//Salva parametros
		ParamSave(cNomRot,aParamBox,"1")
	EndIf
EndIf

Return(lRet)


/*/{Protheus.doc} nomeStaticFunction
    Fun��o de valida��o do preenchimento dos perguntes.
    
    @type  Static Function
    
    @author rafaelalmeida

    @since 18/06/2020
    @version 12.1.27
    
    @return lRet, logic, Retorno l�gico da valida��o
    
/*/
Static Function VldPerg()

Local lRet := .T.

//Integra�ao SRA  ou SRB selecionada e (Matricula at�) vazio.
If (MV_PAR03 .Or. MV_PAR04) .And. Empty(MV_PAR06)
    lRet := .F.
    MsgStop(STR0042 + CRLF +;//##"Voc� selecionou a integra��o de Funcion�rios ou Dependentes."
            STR0043         )//##"Por favor preencha o campo de [Matricula at�:] !"
EndIf

//Integra�ao RD0 selecionada e (Participante at�) vazio.
If MV_PAR07 .And. Empty(MV_PAR09)
    lRet := .F.
    MsgStop(STR0044 + CRLF +;//##"Voc� selecionou a integra��o de pessoas."
            STR0045			)//##"Por favor preencha o campo de [Participante at�:] !"
EndIf

Return lRet

/*/{Protheus.doc} GravaLog
    
    Grava log de processamento 

    @type  Static Function
    @author user
    @since 09/06/2020
    @version version
    @param cLog, Character, Mensagem geral de processamento
    @param aLogSra, Array, Log de Registros Gravados na SRA.
    @param aLogSrb, Array, Log de Registros Gravados na SRB.
    @param aLogRD0, Array, Log de Registros Gravados na RDO
    
    /*/
Static Function GravaLog(aLogSra,aLogSrb,aLogRd0,aLogSqb,aLogSq3)

Local aDados  := {}
Local aTitle  := {}


Local nTotalReg := 0
Local nXi       := 1

Default aLogSra := {}
Default aLogSrb := {}
Default aLogRd0 := {}
Default aLogSqb := {}

Aadd(aTitle, STR0029)//##Log de Processamento da Carga Inicial

Aadd(aDados, Padr(STR0030,40) +;//##Tabela
             Padr(STR0031,30) +;//##Chave
             Padr(STR0032,20) +;//##Data
             Padr(STR0033,10) +;//##Hora
             Padr(STR0034,20))//##Usu�rio*/

nTotalReg := Len(aLogSRA)
If nTotalReg > 0
    ProcRegua(nTotalReg)

    For nXi := 1 To nTotalReg
        IncProc()

        Aadd(aDados,Padr("SRA "+FWSX2UTIL():GETX2NAME("SRA"),40) +;
            Padr(aLogSra[nXi][1],30) +;
            Padr(DtoC(aLogSra[nXi][2]),20) +;
            Padr(aLogSra[nXi][3],10) +;
            Padr(aLogSra[nXi][4],20))
    Next
EndIf

nTotalReg := Len(aLogSRB)
If nTotalReg > 0
    ProcRegua(nTotalReg)

    For nXi := 1 To nTotalReg
        IncProc()

        Aadd(aDados,Padr("SRB "+FWSX2UTIL():GETX2NAME("SRB"),40) +;
            Padr(aLogSRB[nXi][1],30) +;
            Padr(DtoC(aLogSRB[nXi][2]),20) +;
            Padr(aLogSRB[nXi][3],10) +;
            Padr(aLogSRB[nXi][4],20))
    Next
EndIf

nTotalReg := Len(aLogRD0)
If nTotalReg > 0
    ProcRegua(nTotalReg)

    For nXi := 1 To nTotalReg
        IncProc()

        Aadd(aDados,Padr("RD0 "+FWSX2UTIL():GETX2NAME("RD0"),40) +;
            Padr(aLogRD0[nXi][1],30) +;
            Padr(DtoC(aLogRD0[nXi][2]),20) +;
            Padr(aLogRD0[nXi][3],10) +;
            Padr(aLogRD0[nXi][4],20))
    Next
EndIf

nTotalReg := Len(aLogSqb)
If nTotalReg > 0
    ProcRegua(nTotalReg)

    For nXi := 1 To nTotalReg
        IncProc()

        Aadd(aDados,Padr("RD0 "+FWSX2UTIL():GETX2NAME("RD0"),40) +;
            Padr(aLogSqb[nXi][1],30) +;
            Padr(DtoC(aLogSqb[nXi][2]),20) +;
            Padr(aLogSqb[nXi][3],10) +;
            Padr(aLogSqb[nXi][4],20))
    Next
EndIf

nTotalReg := Len(aLogSq3)
If nTotalReg > 0
    ProcRegua(nTotalReg)

    For nXi := 1 To nTotalReg
        IncProc()

        Aadd(aDados,Padr("SQ3 "+FWSX2UTIL():GETX2NAME("SQ3"),40) +;
            Padr(aLogSq3[nXi][1],30) +;
            Padr(DtoC(aLogSq3[nXi][2]),20) +;
            Padr(aLogSq3[nXi][3],10) +;
            Padr(aLogSq3[nXi][4],20))
    Next
EndIf

fMakeLog({aDados}, aTitle, Nil, Nil, "",STR0029, "M", "P",, .F.)//##Log de Processamento da Carga Inicial


Return Nil
