#Include "PROTHEUS.CH" 
#Include "GPER015.CH"

/*
������������������������������������������������������������������������������������������
������������������������������������������������������������������������������������������
��������������������������������������������������������������������������������������ͻ��
���Programa  � GPER015  � Autor � Luis Samaniego                 � Fecha �  21/12/2015 ���
��������������������������������������������������������������������������������������͹��
���Desc.     �Libro de Sueldo Digital - Argentina                                      ���
��������������������������������������������������������������������������������������͹��
���Uso       � SIGAGPE                                                                 ���
��������������������������������������������������������������������������������������͹��
���                 ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.                  ���
��������������������������������������������������������������������������������������͹��
���  Programador   �    Data    �   Issue    �  Motivo da Alteracao                    ���
��������������������������������������������������������������������������������������͹��
��� Marco A. Glez. � 12/04/2019 � DMINA-5689 �Se replica a V12.1.17 la solucion reali- ���
���                �            �            �zada en el llamado TTALKW de V11.8, que  ���
���                �            �            �consiste en la creacion del Libro de     ���
���                �            �            �Sueldo Digital para Argentina RG 3781.   ���
��������������������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������������������
������������������������������������������������������������������������������������������
*/
Function GPER015()
	
	Local oFld		:= Nil
	Local aCombo	:= {}

	Private cCombo	:= ""
	Private oDlg	:= Nil
	Private oCombo	:= Nil

	aAdd( aCombo, STR0003 ) //"1 - Conceptos"
	aAdd( aCombo, STR0004 ) //"2 - Detalle"

	DEFINE MSDIALOG oDlg TITLE STR0001 FROM 0,0 TO 125,450 OF oDlg PIXEL //"RG 3781-15 Libro de Sueldo Digital"

	@ 006,006 TO 045,170 LABEL STR0002 OF oDlg PIXEL //"Libro de Sueldo Digital"
	@ 020,010 COMBOBOX oCombo VAR cCombo ITEMS aCombo SIZE 100,8 PIXEL OF oFld

	@ 009,180 BUTTON STR0005 SIZE 036,016 PIXEL ACTION (oDlg:End(), IIf( Subs(cCombo,1,1) == "1", GPELibConc(), GPELibDet())) //"Aceptar"
	@ 029,180 BUTTON STR0006 SIZE 036,016 PIXEL ACTION oDlg:End() //"Salir"

	ACTIVATE MSDIALOG oDlg CENTER

Return

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �GPELibConc �Autor  �Luis Samaniego      �Fecha �  21/12/15   ���
��������������������������������������������������������������������������͹��
���Desc.     � Libro de Sueldo Digital - Conceptos                         ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE                                                     ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function GPELibConc()
	
	Local cPerg		:= "GPER015A"
	Local nOpcA		:= 0
	Local aSays		:= {}
	Local aButtons	:= {}

	Pergunte( cPerg, .F. )
	
	aAdd(aSays,OemToAnsi( STR0001 ) ) //"RG 3781-15 Libro de Sueldo Digital"
	aAdd(aButtons, { 5,.T.,{ || Pergunte(cPerg,.T. ) } } )
	aAdd(aButtons, { 1,.T.,{ |o| nOpcA := 1, o:oWnd:End() } } )
	aAdd(aButtons, { 2,.T.,{ |o| nOpcA := 2, o:oWnd:End() } } )             
	
	FormBatch( OemToAnsi(STR0002), aSays , aButtons ) //"Libro de Sueldo Digital"

	If nOpcA == 1 
		Processa({ || GpeProcSRV() })
	EndIf
	
Return

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �GpeProcSRV �Autor  �Luis Samaniego      �Fecha �  21/12/15   ���
��������������������������������������������������������������������������͹��
���Desc.     � Crea archivo de libro de sueldo digital - Conceptos         ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE                                                     ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function GpeProcSRV()
	
	Local cNomArch := AllTrim(MV_PAR01)
	Local cDirArch := AllTrim(MV_PAR02)
	Local nArchTXT := 0
	Local cLinea   := ""

	Makedir(cDirArch)
	IIf (!(Substr(cNomArch,Len(cNomArch) - 2, 3) $ "txt|TXT"), cNomArch += ".TXT", "")
	nArchTXT := CreaArch(cDirArch, cNomArch, ".TXT")

	If nArchTXT == -1
		Return
	EndIf

	dbSelectArea("SRV")
	SRV->(DBSetOrder(1)) //"RV_FILIAL+RV_COD"
	While SRV->(!EOF())
		If !Empty(SRV->RV_CONAFIP)
			cLinea := PADR(SRV->RV_CONAFIP, 6, " ")        //1 a 6
			cLinea += PADR(SRV->RV_COD, 10, " ")             //7 a 16 
			cLinea += PADR(SRV->RV_DESC, 150, " ")          //17 a 166
			cLinea += "1"                                   //167 a 167
			cLinea += IIf(SRV->RV_SIPAPOR == "S", "1", "0") //168 a 168
			cLinea += IIf(SRV->RV_SIPACON == "S", "1", "0") //169 a 169
			cLinea += IIf(SRV->RV_INSSPOR == "S", "1", "0") //170 a 170
			cLinea += IIf(SRV->RV_INSSCON == "S", "1", "0") //171 a 171
			cLinea += IIf(SRV->RV_OBRSPOR == "S", "1", "0") //172 a 172
			cLinea += IIf(SRV->RV_OBRSCON == "S", "1", "0") //173 a 173
			cLinea += IIf(SRV->RV_FONSPOR == "S", "1", "0") //174 a 174
			cLinea += IIf(SRV->RV_FONSCON == "S", "1", "0") //175 a 175
			cLinea += IIf(SRV->RV_RENAPOR == "S", "1", "0") //176 a 176
			cLinea += IIf(SRV->RV_RENACON == "S", "1", "0") //177 a 177
			cLinea += " "                                   //178 a 178
			cLinea += IIf(SRV->RV_ASIGCON == "S", "1", "0") //179 a 179
			cLinea += " "                                   //180 a 180
			cLinea += IIf(SRV->RV_FNECON == "S", "1", "0")  //181 a 181
			cLinea += " "                                   //182 a 182
			cLinea += IIf(SRV->RV_ARTCON == "S", "1", "0")  //183 a 183
			cLinea += IIf(SRV->RV_REDIPOR == "S", "1", "0") //184 a 184
			cLinea += " "                                   //185 a 185
			cLinea += IIf(SRV->RV_REESPOR == "S", "1", "0") //186 a 186
			cLinea += "         "                           //187 a 195
			cLinea += CRLF

			FWrite(nArchTXT, cLinea)
			cLinea := ""
		EndIf
		SRV->(dbSkip())
	EndDo

	FClose (nArchTXT)
	
Return

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �GPELibDet  �Autor  �Luis Samaniego      �Fecha �  21/12/15   ���
��������������������������������������������������������������������������͹��
���Desc.     � Libro de sueldo digital - Detalle                           ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE                                                     ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function GPELibDet()
	
	Local cPerg    := "GPER015B"
	Local nOpcA    := 0
	Local aSays    := {}
	Local aButtons := {}

	Pergunte( cPerg, .F. )
	aAdd(aSays,OemToAnsi( STR0001 ) ) //"RG 3781-15 Libro de Sueldo Digital"
	aAdd(aButtons, { 5,.T.,{ || Pergunte(cPerg,.T. ) } } )
	aAdd(aButtons, { 1,.T.,{ |o| nOpcA := 1, o:oWnd:End() } } )
	aAdd(aButtons, { 2,.T.,{ |o| nOpcA := 2, o:oWnd:End() } } )             
	FormBatch( OemToAnsi(STR0002), aSays , aButtons ) //"Libro de Sueldo Digital"

	If nOpcA == 1 
		Processa({ || GpeProcDet() })
	EndIf
	
Return

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �GpeProcDet �Autor  �Luis Samaniego      �Fecha �  21/12/15   ���
��������������������������������������������������������������������������͹��
���Desc.     � Libro de Sueldo Digital - Detalle                           ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE                                                     ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function GpeProcDet()
	
	Local aVerbas	:= {}
	Local cNomArch	:= AllTrim(MV_PAR06)
	Local cDirArch	:= AllTrim(MV_PAR07)
	Local nEmps		:= 0
	Local cLinea	:= ""

	Private aPerAbe		:= {} //Periodo Abierto
	Private aPerFec		:= {} //Periodo Cerrado

	Private cRoteiro	:= MV_PAR01 // Armazena o Roteiro selecionado
	Private cProcesso	:= MV_PAR02 // Armazena o processo selecionado
	Private cPeriod		:= MV_PAR03 // Armazena o Periodo selecionado
	Private cNumPago	:= MV_PAR04 // Armazena o Periodo selecionado
	Private nIdenEnv    := MV_PAR08 // Por default (SJ)
	Private nNumLiq     := MV_PAR09
	Private cTipoLiq	:= IIf(MV_PAR05 == 1, "M", IIf(MV_PAR05 == 2, "Q", "S"))
	Private aConceptos	:= {}
	Private aConcepSRV	:= {}
	Private nArchTXT	:= 0
	Private nArchOK		:= 0
	Private dFchPago	:= POSICIONE("RCH", 1, xFilial("RCH")+MV_PAR02+MV_PAR03+MV_PAR04+MV_PAR01 , "RCH_DTPAGO")
	Private dFchDia		:= Date()
	Private cPictHrs	:= "@E 999.99"  
	Private cPictVal	:= "@E 999999999999.99"
	Private aVerbasFunc	:= {} //Para uso nova funcao em modelo2
	Private aRoteiros	:= {}	
	Private aTmpConce	:= {}
	
	//Array aRoteiros para excluir dos calculos os roteiros do tipo "4" (SRY->RY_TIPO=4)
	DbSelectArea("SRY")
	DbSetOrder(1)  
	dbSeek(xFilial("SRY"))
	While !Eof("SRY") .And. SRY->RY_FILIAL == xFilial("SRY") 
		if SRY->RY_TIPO <> ("4")
			Aadd(aRoteiros,{SRY->RY_CALCULO, RY_TIPO,0}) 
		Endif
		dbSkip()	
	EndDo
		
	If (len(aRoteiros) <= 0) 
		Aviso( OemToAnsi(STR0007), OemToAnsi(STR0016), {STR0009} ) //"Atencion" - "N�o existem procedimentos para os par�metros informados."   
		Return .F.
	EndIf

	Makedir(cDirArch)
	IIf (!(Substr(cNomArch,Len(cNomArch) - 2, 3) $ "txt|TXT"), cNomArch += ".TXT", "")
	nArchTXT := CreaArch(cDirArch, "Original_" + cNomArch, ".TXT")
	nArchOK  := CreaArch(cDirArch, cNomArch, ".TXT")

	If nArchTXT == -1
		Return
	EndIf

	RetPerAbertFech(MV_PAR02 ,; // Processo selecionado na Pergunte.
					MV_PAR01	,; // Roteiro selecionado na Pergunte.
					MV_PAR03	,; // Periodo selecionado na Pergunte.
					MV_PAR04		,; // Numero de Pagamento selecionado na Pergunte.
					NIL			,; // Periodo Ate - Passar "NIL", pois neste relatorio eh escolhido apenas um periodo.
					NIL			,; // Numero de Pagamento Ate - Passar "NIL", pois neste relatorio eh escolhido apenas um numero de pagamento.
					@aPerAbe	,; // Retorna array com os Periodos e NrPagtos Abertos
					@aPerFec ) // Retorna array com os Periodos e NrPagtos Fechados

	GetConcep()
	
	DbSelectArea("SRA")
	SRA->(dbsetOrder(1)) //RA_FILIAL+RA_MAT
	SRA->(dbGoTop())
	GrabaReg01()
	While !SRA->(EOF())
		aVerbas   := RetVerbas(SRA->RA_FILIAL,SRA->RA_MAT,aConceptos,aPerAbe,aPerFec)
		aVerbasFunc := RetVerbas(SRA->RA_FILIAL,SRA->RA_MAT,aTmpConce,aPerAbe,aPerFec)
		If !Empty(aVerbas)
			If  nEmps != 0
				cLinea += CRLF
				FWrite(nArchTXT, cLinea)
				cLinea := ""
			Endif
			If  nIdenEnv == 1 //1- SJ  y 2=RE
				GrabaReg02()
				GrabaReg03(aVerbas)
			Endif
			GrabaReg04()
			nEmps += 1
		EndIf
		SRA->(DbSkip())
	EndDo

	FClose (nArchTXT)
	nArchTXT := FT_FUse(cDirArch + "Original_" + cNomArch)
	FT_FGoTop()

	Do While !FT_FEOF()
		cLinea := FT_FReadLn()
		If  Substr(cLinea, 30, 6) == "#nEmp#"
			cLinea := Replace(cLinea, "#nEmp#", PADL(AllTrim(Str(nEmps)), 6, "0"))
		Else 
			cLinea := CRLF + cLinea	
		EndIf
		FWrite(nArchOK, cLinea)
		FT_FSKIP()
	EndDo

	FT_FUSE()
	FClose(nArchOK)
	If File(cDirArch + "Original_" + cNomArch)
		FErase(cDirArch + "Original_" + cNomArch)
	EndIf
	
Return

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �GrabaReg01 �Autor  �Luis Samaniego      �Fecha �  21/12/15   ���
��������������������������������������������������������������������������͹��
���Desc.     � Libro de Sueldo Digital - Detalle | Renglon 01              ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE                                                     ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function GrabaReg01()
	
	Local cLinea	:= ""
	Local aFilAtu	:= FWArrFilAtu()

	cLinea := "01"
	cLinea += PADL(STRTRAN(aFilAtu[18],"-",""), 11, " ")
	cLinea += IIf(nIdenEnv == 1,"SJ","RE")
	cLinea += cPeriod
	cLinea += IIf(nIdenEnv == 1,cTipoLiq,Space(len(cTipoLiq)))
	cLinea += IIf(nIdenEnv == 1,PADL(nNumLiq, 5, " "),Space(5)) 
	cLinea += IIf(nIdenEnv == 1,"30",Space(2))
	cLinea += "#nEmp#"
	cLinea += CRLF
	FWrite(nArchTXT, cLinea)
	cLinea := ""
	
Return

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �GrabaReg02 �Autor  �Luis Samaniego      �Fecha �  21/12/15   ���
��������������������������������������������������������������������������͹��
���Desc.     � Libro de Sueldo Digital - Detalle | Renglon 02              ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE                                                     ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function GrabaReg02()
	
	Local cLinea	:= ""

	cLinea := "02"
	cLinea += PADL(SRA->RA_CIC, 11, " ")
	cLinea += PADR(SRA->RA_MAT, 10, " ")
	cLinea += PADR("", 50, " ")
	cLinea += PADR(SRA->RA_CBU, 22, " ")
	cLinea += IIf(cTipoLiq == "M", "030", IIf(cTipoLiq == "Q", "015", "007"))
	cLinea += DTOS(dFchPago)
	cLinea += Space(8)
	cLinea += IIf(Empty(SRA->RA_CBU), "2", "3")
	cLinea += CRLF
	FWrite(nArchTXT, cLinea)
	cLinea := ""
	
Return

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �GrabaReg03 �Autor  �Luis Samaniego      �Fecha �  21/12/15   ���
��������������������������������������������������������������������������͹��
���Desc.     � Libro de Sueldo Digital - Detalle | Renglon 03              ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE                                                     ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function GrabaReg03(aVerbas)
	
	Local nLoop		:= 0
	Local nPos		:= 0
	Local cLinea	:= ""
	Local cIndDyC   := " "
	
	For nLoop := 1 To Len(aVerbas)
		nPos  := aScan( aConcepSRV,{|x| x[1] == aVerbas[nLoop][03]} )
		cLinea := "03"
		cLinea += PADL(SRA->RA_CIC, 11, " ")
		cLinea += PADR(aVerbas[nLoop][03], 10, " ")
		cLinea += PADL(STRTRAN(AllTrim(STRTRAN(Transform(aVerbas[nLoop][06], cPictHrs),",","")),".",""), 5, "0") 
		cLinea += " "
		cLinea += PADL(STRTRAN(AllTrim(STRTRAN(Transform(ABS(aVerbas[nLoop][07]), cPictVal),",","")),".",""), 15, "0")
		If  nPos > 0 
			cIndDyC := IIf(aConcepSRV[nPos][3] == "1",IIF(aVerbas[nLoop][07]>0,"C","D"),IIF(aVerbas[nLoop][07]<0,"C","D"))		
		Endif
		cLinea += cIndDyC
		cLinea += Space(6)
		cLinea += CRLF
		FWrite(nArchTXT, cLinea)
		cLinea := ""
	Next
	
Return

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �GrabaReg04 �Autor  �Luis Samaniego      �Fecha �  21/12/15   ���
��������������������������������������������������������������������������͹��
���Desc.     � Libro de Sueldo Digital - Detalle | Renglon 04              ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE                                                     ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function GrabaReg04()
	
	Local cFilRCB		:= xFilial("RCB")
	Local cFilRCC		:= xFilial("RCC")
	Local cNomeArq		:= "S042"
	Local cCpo			:= ""
	Local cFormula		:= ""
	Local cLectura		:= SPACE(1) 
	Local cFormE		:= ""
	Local cFormEOK		:= ""
	Local cLinea		:= ""
	Local nInicio		:= 0
	Local nLongitude	:= 0
	Local nPoint		:= 0
	Local nLoop			:= 0
	Local aRCCE			:= {}
	Local aPos			:= {,,,,}

	Private cMes		:= "" 
	Private cAno		:= ""
	Private xQtdParC
	Private xQtdParF
	Private aPerFec		:= {}
	Private aPerAbe		:= {}

	Private cSit1		:= ""
	Private cDiaSit1	:= ""
	Private cSit2		:= ""
	Private cDiaSit2	:= ""
	Private cSit3		:= ""
	Private cDiaSit3	:= ""
	Private aDatasSR8	:= {}
	
	//Variables utilizadas por otras rutinas
	Private dDtIngresso	:= Ctod("  /  /  ")
	Private dDtDespido	:= Ctod("  /  /  ")
	Private dDtUltAfas	:= Ctod("  /  /  ")
	Private aRCMFields	:= {} // Estrutura da tabela RCM - direto do arquivo DBF
	
	//Posicao dos campos da tabela RCM
	Private nPosRCMFil	:= 0
	Private nPosRCMPd	:= 0
	Private nPosRCMTip	:= 0
	Private nPosRCMSic	:= 0
	Private aQtdAus		:= {} //array contem a quantidade de dias para desconto do total de dias trabalhados Sicoss campo 041

	cAno := Substr(cPeriod,1,4)
	cMes := Substr(cPeriod,5,2)

	DbSelectArea("RCB")
	RCB->(DbSetOrder(1)) //RCB_FILIAL+RCB_CODIGO
	nPoint := 1
	If RCB->(MSSeek(cFilRCB + cNomeArq))
		While cFilRCB == RCB->RCB_FILIAL .And. cNomeArq == RCB->RCB_CODIGO
			cCpo := AllTrim(RCB->RCB_CAMPOS)
			Do Case
				Case cCpo == "DESCRIPCIO"  
				aPos[1] := {nPoint, RCB_TAMAN}
				Case cCpo == "LECTURA"  
				aPos[2] := {nPoint, RCB_TAMAN}
				Case cCpo == "INICIO"  
				aPos[3] := {nPoint, RCB_TAMAN}
				Case cCpo == "LONGITUD"  
				aPos[4] := {nPoint, RCB_TAMAN}
				Case cCpo == "FORMULA"  
				aPos[5] := {nPoint, RCB_TAMAN}
			EndCase
			nPoint += RCB_TAMAN
			RCB->(DbSkip())
		EndDo
	EndIf

	DbSelectArea("RCC")
	RCC->(DbSetOrder(1)) //RCC_FILIAL+RCC_CODIGO+RCC_FIL+RCC_CHAVE+RCC_SEQUEN
	RCC->(MSSeek(cFilRCC+cNomeArq))
	Do While cFilRCC == RCC->RCC_FILIAL .And. cNomeArq == RCC->RCC_CODIGO
		cLectura	:= AllTrim(SubStr(RCC_CONTEU, aPos[2,1], aPos[2,2]))
		nInicio	:= Val(AllTrim(SubStr(RCC_CONTEU, aPos[3,1], aPos[3,2])))
		nLongitude	:= Val(AllTrim(SubStr(RCC_CONTEU, aPos[4,1], aPos[4,2])))
		cFormula	:= AllTrim(SubStr(RCC_CONTEU, aPos[5,1], aPos[5,2]))

		AAdd(aRCCE,{cLectura,cFormula,nInicio,nLongitude})
		RCC->(DbSkip())	
	EndDo  

	If Val(StaticCall(GPER801,GRAUPAR,"C",.T.)) == 0   				 
		xQtdParC := "0" 
	Else
		xQtdParC := StaticCall(GPER801,GRAUPAR,"C",.T.)			 	 
	Endif

	If Val(StaticCall(GPER801,GRAUPAR,"F",.T.)) == 0
		xQtdParF :=	 "00"		
	Else
		xQtdParF :=	StaticCall(GPER801,GRAUPAR,"F",.T.)			
	Endif

	aDatasSR8 := {}
	cSit1 := Space(02)
	cSit2 := Space(02)
	cSit3 := Space(02)

	cDiaSit1 := Space(02)
	cDiaSit2 := Space(02)
	cDiaSit3 := Space(02)

	For nLoop := 1 To Len(aRCCE)	
		If nLoop != 1
			cFormE := &(aRCCE[nLoop][2])
		Else
			cFormE := (aRCCE[nLoop][2])
		EndIf
		If Type("cFormE") == "N"	
			cFormEOK := Str(cFormE)
		Elseif Type("cFormE") == "D"
			cFormEOK := DtoC(cFormE)
		Else
			cFormEOK := cFormE
		EndIf
		If aRCCE[nLoop,4] > 0
			cLinea += PADR(cFormEOK,aRCCE[nLoop,4])
		Endif
	Next
	FWrite(nArchTXT, cLinea)
	cLinea := ""

Return

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �RetVerbas  �Autor  �Luis Samaniego      �Fecha �  21/12/15   ���
��������������������������������������������������������������������������͹��
���Desc.     � Regresa conceptos por empleado                              ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE                                                     ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function RetVerbas(cFil,cMat,aConceptos,aPerAbe,aPerFec)
	
	Local aVerbasFunc	:= {}

	aVerbasFunc	:= RetornaVerbasFunc(	cFil,;			// Filial do funcionario corrente
										cMat,;			// Matricula do funcionario corrente
										NIL,;			// 
										cRoteiro,;		// Roteiro selecionado na pergunte
										aConceptos,;	// aVerbasFilter
										aPerAbe,;		// Array com os Periodos e Numero de pagamento abertos
										aPerFec)		// Array com os Periodos e Numero de pagamento fechados

Return aVerbasFunc

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �RetVerbas  �Autor  �Luis Samaniego      �Fecha �  21/12/15   ���
��������������������������������������������������������������������������͹��
���Desc.     � Obtiene conceptos a utilizar                                ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE                                                     ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function GetConcep()
	
	Local cFilSRV	:= xFilial("SRV")

	SRV->(DBSetOrder(1)) //RV_FILIAL+RV_COD
	SRV->(MSSeek(cFilSRV))
	aConceptos := {}
	aConcepSRV := {}
	SRV->(DBEval( {|| IF(SRV->RV_TIPOCOD $ "1|2" .AND. !EMPTY(SRV->RV_CONAFIP), AAdd(aConceptos, {SRV->RV_COD}), NIL) }, {|| SRV->RV_FILIAL ==  cFilSRV} ) )
	SRV->(DBEval( {|| IF(SRV->RV_TIPOCOD $ "1|2" .AND. !EMPTY(SRV->RV_CONAFIP), AAdd(aConcepSRV, {SRV->RV_COD, RV_CONAFIP, RV_TIPOCOD}), NIL) }, {|| SRV->RV_FILIAL ==  cFilSRV} ) )
	SRV->(DBEval( {|| IF(SRV->RV_TIPOCOD $ "3|4" , AAdd(aTmpConce, {SRV->RV_COD}), NIL) }, {|| SRV->RV_FILIAL ==  cFilSRV} ) )
Return

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �GPER015Dir �Autor  �Luis Samaniego      �Fecha �  21/12/15   ���
��������������������������������������������������������������������������͹��
���Desc.     � Obtiene ruta donde sera creado archivo de texto             ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE                                                     ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Function GPER015Dir(nOpc)
	
	Local lRuta	:= .F.
	Local cRuta	:= ""

	cRuta := cGetFile( '|(*.*)|' , STR0015, 0 , "C:\", .F., GETF_LOCALHARD + GETF_LOCALFLOPPY + GETF_RETDIRECTORY  ) //"Seleccione el directorio"
	If !Empty(cRuta)
		If nOpc = 1
			MV_PAR02 := cRuta
		Else
			MV_PAR07 := cRuta
		EndIf
		lRuta := .T.
	EndIf
	
Return lRuta

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �CreaArch   �Autor  �Luis Samaniego      �Fecha �  21/12/15   ���
��������������������������������������������������������������������������͹��
���Desc.     � Crea archivo de texto                                       ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE                                                     ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function CreaArch(cDir, cNomArch, cExt)
	
	Local nHdle		:= 0
	Local cDrive	:= ""
	Local cNewFile	:= cDir + cNomArch

	SplitPath(cNewFile,@cDrive,@cDir,@cNomArch,@cExt)
	cDir := cDrive + cDir
	Makedir(cDir)
	cNomArc := cDir + cNomArch + cExt   

	nHdle := FCreate (cNomArc,0)
	If nHdle == -1
		Aviso( OemToAnsi(STR0007), OemToAnsi(STR0008 + cNomArc), {STR0009} ) //"Atencion" - "No se pudo crear el archivo " - "OK"
	EndIf
	
Return nHdle

/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Fun��o    �VldNPgLiq   � Autor � Raul Ortiz            � Data � 18/10/15 ���
���������������������������������������������������������������������������Ĵ��
���Descri��o �Valida que esxita el n�mero de pago informado en parametros   ���
���������������������������������������������������������������������������Ĵ��
���Sintaxe   � VldNPgLiq(nOpc)                                              ���
���������������������������������������������������������������������������Ĵ��
���Parametros�nOpc - 1 = Base, 2 = Control                                  ���
���������������������������������������������������������������������������Ĵ��
��� Uso      �                                                              ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
/*/
Function fVldNPgLi(nOpc)
	
	Local lRet		:= .T.
	Local cFilRCH	:= xFilial("RCH")

	DbSelectArea("RCH")
	RCH->(DbSetOrder(1)) //RCH_FILIAL+RCH_PROCES+RCH_PER+RCH_NUMPAG+RCH_ROTEIR
	If nOpc == 1
		If !Empty(AllTrim(MV_PAR04))
			If !RCH->(MSSeek(cFilRCH+MV_PAR02+MV_PAR03+MV_PAR04+MV_PAR01))
				lRet := .F.
				Alert(STR0010 + AllTrim(MV_PAR02) + STR0011 + AllTrim(MV_PAR01) + STR0012 + AllTrim(MV_PAR03) + STR0013 + AllTrim(MV_PAR04)) //"No existen datos para el Proceso " - " Procedimiento " - " con el Periodo " - " y Numero de Pago "
			EndIf
		Else
			lRet := .F.
			Alert(STR0014) //"Este dato debe informarse"
		EndIf
	EndIf
	RCH->(dbCloseArea("RCH"))
	
Return lRet

/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Fun��o    �VldPerLiq   � Autor � Raul Ortiz            � Data � 18/10/15 ���
���������������������������������������������������������������������������Ĵ��
���Descri��o �Valida el periodo informado en los parametros                 ���
���������������������������������������������������������������������������Ĵ��
���Sintaxe   � VldPerLiq()                                                  ���
���������������������������������������������������������������������������Ĵ��
���Parametros�nOpc: 1 = Base, 2 = Control                                   ���
���������������������������������������������������������������������������Ĵ��
��� Uso      �                                                              ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
/*/
Function fVldPerLi(nOpc)
	
	Local lRet		:= .T.
	Local cFilRCH	:= xFilial("RCH")

	DbSelectArea("RCH")
	RCH->(DbSetOrder(4)) //RCH_FILIAL+RCH_PROCES+RCH_ROTEIR+RCH_PER+RCH_NUMPAG
	If nOpc == 1
		If !Empty(AllTrim(MV_PAR03))
			If !RCH->(MSSeek(cFilRCH+MV_PAR02+MV_PAR01+MV_PAR03))
				lRet := .F.
				Alert(STR0010 + AllTrim(MV_PAR02) + STR0011 + AllTrim(MV_PAR01) + STR0012 + AllTrim(MV_PAR03)) //"No existen datos para el Proceso " - " Procedimiento " - " con el Periodo "
			EndIf
		Else
			lRet := .F.
			Alert(STR0014) //"Este dato debe informarse"
		EndIf
	EndIf
	RCH->(dbCloseArea("RCH"))
	
Return lRet