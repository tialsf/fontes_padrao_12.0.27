#Include "PROTHEUS.CH" 
#Include "GPER1357.CH"
#Include "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"
#INCLUDE "TBICONN.CH"
/*
������������������������������������������������������������������������������������������
������������������������������������������������������������������������������������������
��������������������������������������������������������������������������������������ͻ��
���Programa  � GPER1357 � Autor � Laura Medina                   � Fecha �  19/03/2020 ���
��������������������������������������������������������������������������������������͹��
���Desc.     �Archivo 1357: 1-Informe 1357 y 2-Archivo .txt - Argentina                                                 ���
��������������������������������������������������������������������������������������͹��
���Uso       � SIGAGPE                                                                 ���
��������������������������������������������������������������������������������������͹��
���                 ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.                  ���
��������������������������������������������������������������������������������������͹��
���  Programador   �    Data    �   Issue    �  Motivo da Alteracao                    ���
��������������������������������������������������������������������������������������͹��
���                �            �            �                                         ���
��������������������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������������������
������������������������������������������������������������������������������������������
*/
Function GPER1357()
	
Local oFld		 := Nil
Local aCombo	 := {}

Private cCombo	 := ""
Private oDlg	 := Nil
Private oCombo	 := Nil
Private cProceso := ""
Private cProced  := ""
Private cPeriodo := ""
Private cNroPago := ""
Private cCodMat  := ""
Private cIniCC   := ""
Private cFinCC   := ""
Private cPerFis  := ""
Private cLugar   := ""
Private dFechEmi := ""
Private cRespons := ""
Private nTipoPre := 0
Private cSecuenc := ""
Private cRutaArc := ""
Private cPictVal := "@E 999999999999.99"
Private cPictV17 := "@E 99999999999999.99"
Private nArchTXT := 0
Private lGenTXT  := .F.

	aAdd( aCombo, STR0003 ) //"1 - "Formulario 1357"
 	aAdd( aCombo, STR0004 ) //"2 - "Archivo 1357"

	DEFINE MSDIALOG oDlg TITLE STR0001 FROM 0,0 TO 125,450 OF oDlg PIXEL //"F - 1357"

	@ 006,006 TO 045,170 LABEL STR0002 OF oDlg PIXEL //"Indique la opci�n a generar:"
	@ 020,010 COMBOBOX oCombo VAR cCombo ITEMS aCombo SIZE 100,8 PIXEL OF oFld

	@ 009,180 BUTTON STR0005 SIZE 036,016 PIXEL ACTION (oDlg:End(), IIf( Subs(cCombo,1,1) == "1", Form1357(), Arch1357())) //"Aceptar"
	@ 029,180 BUTTON STR0006 SIZE 036,016 PIXEL ACTION oDlg:End() //"Salir"

	ACTIVATE MSDIALOG oDlg CENTER

Return


//INICIA FORMULARIO **************************************************************************************************
/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �Form1357    �Autor  �Laura Media       �Fecha �  19/03/2020  ���
��������������������������������������������������������������������������͹��
���Desc.     � Funci�n que genera el formulario 1357.                      ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE: Archivo 1357                                       ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function Form1357()
Local cPerg		:= "GPER1357"

Pergunte( cPerg, .F. )

If  !Pergunte(cPerg,.T. )
	Return .T. 
Endif

MakeSqlExpr(cPerg)
cProceso:= MV_PAR01
cProced := MV_PAR02
cPeriodo:= MV_PAR03
cNroPago:= MV_PAR04
cCodMat := MV_PAR05
cIniCC  := MV_PAR06
cFinCC  := MV_PAR07
cPerFis := MV_PAR08
cLugar  := MV_PAR09
dFechEmi:= MV_PAR10
cRespons:= MV_PAR11
	
Processa({ || ProcFyA(1) })

Return

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �fPrinReport �Autor  �Adrian Perez     �Fecha   �  18/03/2020 ���
��������������������������������������������������������������������������͹��
���Desc.     � Imprime Reporte       									   ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE: Archivo 1357                                       ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������ 
*/
static function fPrinReport(aData)

Local oPrinter
Local nResImpr 	:= 0 				// resultado de impresi�n
Local cFileGen 	:= "GPER1357"  //1357 // private // falta definir ruta del archivo por variable
Local nX:=0


		oPrinter :=  FWMSPrinter():New(cFileGen,6,.F.,GetClientDir(),.T.)  //inicializa el objeto
		oPrinter:Setup() 				    	//abre el objeto
		oPrinter:setDevice( IMP_PDF )   		//selecciona el medio de impresi�n
		oPrinter:SetMargin(40,10,40,10) 	//margenes del documento
		oPrinter:SetPortrait()           	//orientaci�n de p�gina modo retrato =  Horizontal
		nResImpr := oPrinter:nModalResult 	//obtiene nModalResult=1 confimada --- nModalResult=2 cancelada 	
	
		for nX=1 to len(aData)
			fReport(oPrinter,aData[nX])
		Next			
		
		If oPrinter:nModalResult == PD_OK
			oPrinter:Preview()
		EndIf

return
/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �fReport   �Autor  �Adrian Perez     �Fecha     �  18/03/2020 ���
��������������������������������������������������������������������������͹��
���Desc.     � Estructura del reporte 									   ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE: Archivo 1357                                       ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������ 
*/
Static Function fReport(oPrintDoc,aData)
	
	
	Local aAux:={}
	Local nR:=50
	Local nB:=0	
	Local cStartPath	:= GetSrvProfString("Startpath","")
	Local nX:=1
	Local aDatos:={}
	Local aAuxDat:={}
	Local nA:=0
	Local nSalto:=0
	Private nAncho:=20
	Private oFontP
	Private oFontT
	Private oPrinter
	
   
			oPrinter   := oPrintDoc							 
			oFontT 		:= TFont():New('Arial',,-13,.T.,.T.)//Fuente del Titulo
			oFontP 		:= TFont():New('Arial',,-12,.T.)     //Fuente del P�rrafo
		
			oPrinter:StartPage() 
			
			//LOGO
			oPrinter:Box( nR-35,10,100, 100)
			oPrinter:SayBitmap((nR-35)+10,15,cStartPath+"lgrl"+FwCodEmp("SM0")+".bmp",80,40)
			//FIN LOGO
			nR+=5
			oPrinter:Say(nR,110,STR0019 , oFontP) //70	
			nR+=15
			oPrinter:Say(nR,110,STR0020 , oFontP)	
			nR+=45
			oPrinter:Say(nR,10,STR0021 , oFontP)
			oPrinter:Say(nR,110,DTOC(dFechEmi) , oFontP)
			
			nR+=15
			aAux:=aData[1,1]
			
			oPrinter:Say(nR,10,STR0022  , oFontP)
			oPrinter:Say(nR,70,aAux[1] , oFontP)
			oPrinter:Say(nR,150,aAux[2] , oFontP)
			oPrinter:Say(nR,400,aAux[3] , oFontP)
			
			nR+=15
			oPrinter:Say(nR,10,STR0023 , oFontP)
			oPrinter:Say(nR,120,SM0->M0_CGC , oFontP)
			oPrinter:Say(nR,250,SM0->M0_NOME , oFontP)
			
			nR+=15
			oPrinter:Say(nR,10,STR0024 , oFontP)
			oPrinter:Say(nR,100,cPerFis , oFontP)//periodo fiscal
			
			nR+=30
			nB:=nR-20
			//encabezado1
			aadd(aAuxDat,{STR0025,STR0026,STR0027})
			
			nSalto:=fReportHead(@nR,@nB,oPrinter,	aAuxDat,.T.)
			
			//encabezado2
			aAuxDat:={}
			nR+=nSalto
			aadd(aAuxDat,{STR0028,"",""})
			fReportHead(@nR,@nB,oPrinter,	aAuxDat,.F.)
			
			aAux:={}
			aAux:=aData[2]
			//remuneraciones
			aDatos:={}

			aadd(aDatos,{STR0029	,aAux[3],"3","3"})
			aadd(aDatos,{STR0030	,aAux[4],"3","4"}) 
			aadd(aDatos,{STR0031	,aAux[5],"3","5"})
			aadd(aDatos,{STR0032	,aAux[6],"3","6"})
			aadd(aDatos,{STR0033	,aAux[7],"3","7"})
			aadd(aDatos,{STR0034	,aAux[8],"3","8"})
			aadd(aDatos,{STR0035	,aAux[9],"3","9"})
			aadd(aDatos,{STR0036	,aAux[10],"3","10"})
			aadd(aDatos,{STR0037	,aAux[11],"3","11"})
			aadd(aDatos,{STR0038	,aAux[12],"3","12"})
			aadd(aDatos,{STR0039	,aAux[13],"3","13"})
			
			filas(@nR,@nB,oPrinter,aDatos,oFontP)
			
			//encabezado3
			aAuxDat:={}
			aadd(aAuxDat,{STR0040		  	,"",""})
			fReportHead(@nR,@nB,oPrinter,	aAuxDat,.F.)
			aAuxDat:={}
			
			aDatos:={}
			aadd(aDatos,{STR0029	,aAux[14],"3","14"})
			aadd(aDatos,{STR0030	,aAux[15],"3","15"})
			aadd(aDatos,{STR0031	,aAux[16],"3","16"})
			aadd(aDatos,{STR0032	,aAux[17],"3","17"})
			aadd(aDatos,{STR0033	,aAux[18],"3","18"})
			aadd(aDatos,{STR0034	,aAux[19],"3","19"})
			aadd(aDatos,{STR0035	,aAux[20],"3","20"})
			aadd(aDatos,{STR0036	,aAux[21],"3","21"})
			aadd(aDatos,{STR0037	,aAux[22],"3","22"})
			aadd(aDatos,{STR0038	,aAux[23],"3","23"})
			aadd(aDatos,{STR0039	,aAux[24],"3","24"})
			
		
			filas(@nR,@nB,oPrinter,aDatos,oFontP)
			aDatos:={}
			//totales
			
			aadd(aDatos,{STR0087,aAux[25],"3","25"}) 
			aadd(aDatos,{STR0088,aAux[26],"3","26"}) 
			aadd(aDatos,{STR0089,aAux[27],"3","27"}) 
			
			filas(@nR,@nB,oPrinter,aDatos,oFontT)
			//DEDUCCIONES
			nR+=nAncho+210
			nB+=nAncho+210
			
			nR:=fChangePage(nR)
			nB:=nR-20
			
			oPrinter:Box(nR,010,nB,399)
			oPrinter:Say(nR-3,012,STR0090 , oFontT)
			
			nR+=nAncho+20
			nB+=nAncho+20
			
			aDatos:={}
			
			aAux:={}
			aAux:=aData[3]
			
			aadd(aDatos,{STR0041	,aAux[3],"4","3"})
			aadd(aDatos,{STR0042	,aAux[4],"4","4"})
			aadd(aDatos,{STR0043	,aAux[5],"4","5"})
			aadd(aDatos,{STR0044	,aAux[6],"4","6"})
			aadd(aDatos,{STR0045	,aAux[7],"4","7"})
			aadd(aDatos,{STR0046	,aAux[8],"4","8"})
			aadd(aDatos,{STR0047	,aAux[9],"4","9"})
			aadd(aDatos,{STR0048	,aAux[10],"4","10"})
			aadd(aDatos,{STR0049	,aAux[11],"4","11"})
			aadd(aDatos,{STR0050	,aAux[12],"4","12"})
			aadd(aDatos,{STR0051	,aAux[13],"4","13"})
			aadd(aDatos,{STR0052	,aAux[14],"4","14"})
			aadd(aDatos,{STR0053	,aAux[15],"4","15"})
			aadd(aDatos,{STR0054	,aAux[16],"4","16"})
			
			aadd(aDatos,{STR0055	,aAux[17],"4","17"})
			aadd(aDatos,{STR0056	,aAux[18],"4","18"})
			aadd(aDatos,{STR0057	,aAux[19],"4","19"})
			aadd(aDatos,{STR0058	,aAux[20],"4","20"})
			
			aadd(aDatos,{STR0059	,aAux[21],"4","21"})
			aadd(aDatos,{STR0060	,aAux[22],"4","22"})
			aadd(aDatos,{STR0061	,aAux[23],"4","23"})
			
			aadd(aDatos,{STR0062	,aAux[24],"4","24"})
			aadd(aDatos,{STR0063	,aAux[25],"4","25"})
			aadd(aDatos,{STR0064	,aAux[26],"4","26"})

			filas(@nR,@nB,oPrinter,aDatos,oFontP)
			
			aDatos:={}
			aadd(aDatos,{STR0065	,aAux[27],"3","27"})
			
			filas(@nR,@nB,oPrinter,aDatos,oFontT)
			
			nR+=nAncho+310
			nB+=nAncho+310
			
			nR:=fChangePage(nR)
			nB:=nR-20
			
			oPrinter:Box(nR,010,nB,399)
			oPrinter:Say(nR-3,012,STR0066, oFontT)
			
			nR+=nAncho+20
			nB+=nAncho+20
			
			aDatos:={}
			aAux:={}
			aAux:=aData[4]
			aadd(aDatos,{STR0067	,aAux[3],"5","3"})
			aadd(aDatos,{STR0068	,aAux[4],"5","4"})
			aadd(aDatos,{STR0069	,aAux[5],"5","5"})		
			aadd(aDatos,{STR0070	,aAux[6],"5","6"})
			aadd(aDatos,{STR0071	,aAux[7],"5","7"})
			aadd(aDatos,{STR0072	,aAux[8],"5","8"})
			aadd(aDatos,{STR0073	,aAux[9],"5","9"})
			
			filas(@nR,@nB,oPrinter,aDatos,oFontP)
			
			aDatos:={}
			aadd(aDatos,{STR0074	,aAux[10],"5","10"})
			aadd(aDatos,{STR0075	,aAux[11],"5","11"})
			
			filas(@nR,@nB,oPrinter,aDatos,oFontT)
			
			nR+=nAncho
			nB+=nAncho
			
			oPrinter:Box(nR,010,nB,399)
			oPrinter:Say(nR-3,012,STR0076	 , oFontT)
			
			nR+=nAncho+20
			nB+=nAncho+20
			
			aDatos:={}
			aAux:={}
			aAux:=aData[5]
			
			aadd(aDatos,{STR0077	,aAux[3],"6","3"})
			aadd(aDatos,{STR0078	,aAux[4],"6","4"})
			aadd(aDatos,{STR0079	,aAux[5],"6","5"})
			aadd(aDatos,{STR0080	,aAux[6],"6","6"})
			aadd(aDatos,{STR0081	,aAux[7],"6","7"})
			
			filas(@nR,@nB,oPrinter,aDatos,oFontP)
			
			aDatos:={}
			aadd(aDatos,{STR0082,aAux[8],"6","8"})
			filas(@nR,@nB,oPrinter,aDatos,oFontT)
			
			
			nR+=nAncho
			nB+=nAncho
			
			oPrinter:Say(nR-3,012,STR0083, oFontT)
			
			nR+=nAncho+20
			nB+=nAncho+20
			
			oPrinter:Say(nR+12,012,STR0084 , oFontT)
			oPrinter:Say(nR+12,112,DTOC(dFechEmi) , oFontT)
			
			oPrinter:Say(nR+25,012,STR0085 , oFontT)
			
			oPrinter:Say(nR+38,012,STR0086 , oFontT)
			oPrinter:Say(nR+38,252,cRespons , oFontT)
			
			oPrinter:EndPage() //fin pag
		
							
return 

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �filas  �Autor  �Adrian Perez     �Fecha �  18/03/2020 	   ���
��������������������������������������������������������������������������͹��
���Desc.     � Dibuja las filas y celdas asi como la informacion		   ���
 			  en informe 1357. 											   ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE: Archivo 1357                                       ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������ 
*/

Static function filas(nR,nB,oPrinter,aDatos,oFont)
Local nX:=1
Local nCentro:=0
 	For nX=1 to len(aDatos) // step 1
 	
		oPrinter:Box(nR,010,nB,300)
		oPrinter:Say(nR-3,012,aDatos[nX][1] , oFont)
		
		oPrinter:Box(nR,400,nB,300)
		If !Empty(aDatos[nX][2])
				nCentro:=5*(len(aDatos[nX][2]) )
		else
			nCentro:=5
		EndIF
		oPrinter:Say(nR-3,390-nCentro,if(!empty(aDatos[nX][2]),aDatos[nX][2],"0"), oFont)
		oPrinter:Say(nR-3,303,"$", oFont)
		
		oPrinter:Box(nR,490,nB,399)
		oPrinter:Say(nR-3,442,aDatos[nX][3] , oFont)
		
		oPrinter:Box(nR,590,nB,489)
		oPrinter:Say(nR-3,542,aDatos[nX][4] , oFont)
		
		nCentro:=0
		nR+=nAncho
		nB+=nAncho
	Next
	
Return
/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �fReportHead   �Autor  �Adrian Perez     �Fecha �  18/03/2020 ���
��������������������������������������������������������������������������͹��
���Desc.     � Insertar un encabezado de 3 celdas en Informe Archivo 1357. ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE: Archivo 1357                                       ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������ 
*/
Static function fReportHead(nR,nB,oPrinter,aDatos,lSalto)
Local nSalto:=0

	if lSalto
		nSalto:=50
	EndIf
	oPrinter:Box(nR+nSalto,010,nB,399)
	oPrinter:Say((nR-3),012,aDatos[1][1] , oFontT)
	oPrinter:Box(nR+nSalto,490,nB,399)
	oPrinter:Say((nR-3),400,if(!empty(aDatos[1][2]),(aDatos[1][2]),"."), oFontT)
					
	oPrinter:Box(nR+nSalto,590,nB,489)
	oPrinter:Say((nR-3),490,aDatos[1][3] , oFontT)
	nR+=nAncho
	nB+=nAncho+nSalto
	
return nSalto

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �fChangePage   �Autor  �Adrian Perez     �Fecha �  18/03/2020 ���
��������������������������������������������������������������������������͹��
���Desc.     � Funci�n que cambia de pagina para Informe Archivo 1357.     ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE: Archivo 1357                                       ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������ 
*/

Static Function fChangePage(nRow)

	If (nRow) >= 900
		nRow := 80
		oPrinter:EndPage()
		oPrinter:StartPage()
	EndIf
		
Return nRow
//TERMINA FORMULARIO **************************************************************************************************


/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �Arch1357   �Autor  �Laura Medina        �Fecha �  19/03/2020 ���
��������������������������������������������������������������������������͹��
���Desc.     � Funci�n que genera el Archivo 1357.                         ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE: Archivo 1357                                       ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function Arch1357()
Local cPerg		:= "GPER1357A"

Pergunte( cPerg, .F. )

If  !Pergunte(cPerg,.T. )
	Return .T. 
Endif 

MakeSqlExpr(cPerg) 
cProceso:= MV_PAR01
cProced := MV_PAR02
cPeriodo:= MV_PAR03
cNroPago:= MV_PAR04
cCodMat := MV_PAR05
cIniCC  := MV_PAR06
cFinCC  := MV_PAR07
nTipoPre:= MV_PAR08
cSecuenc:= MV_PAR09
cRutaArc:= Alltrim(MV_PAR10)

If  Vl1537(cPeriodo,nTipoPre)
	Processa({ || ProcFyA(2) })
Endif

If  lGenTXT
	Aviso( OemToAnsi(STR0007), OemToAnsi(STR0017), {STR0009} ) //"Archivo generado con �xito!."
Else
	Aviso(OemToAnsi(STR0007), OemToAnsi(STR0018), {STR0009} ) //"No se encontr� informaci�n para generar el archivo 1357."
Endif

	
Return .T.


/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �ObtDatos   �Autor  �Laura Medina        �Fecha �  23/03/2020 ���
��������������������������������������������������������������������������͹��
���Desc.     � Funci�n para obtener los datos de la SRC o SRD.             ���
���          � 1. Formulario 1357 y  2.Archivo  1357                       ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE: Archivo 1357                                       ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function ProcFyA(nOpc)
Local nPos  	:= 0 
Local cAliasAux := ""
Local cPrefixo  := ""
Local cQuery	:= ""
Local cTmp		:= GetNextAlias()
Local lProcesa  := .T.
Local aRemunera := Array(27)
Local aDeduccio := Array(27)
Local aDeducc23 := Array(11)
Local aCalcImpt := Array(8)
Local nLoop     := 0
Local lPositivo := .T.
Local nRegs     := 0
Local nTamNroC  := TAMSX3("RV_NROCAMP")[1]

Local aEmpleados:={}
Local aData:={}

Private aPerAbe	:= {} //Periodo Abierto
Private aPerFec	:= {} //Periodo Cerrado

RetPerAbertFech(cProceso,; // Processo selecionado na Pergunte.
				cProced,; // Roteiro selecionado na Pergunte.
				cPeriodo,; // Periodo selecionado na Pergunte.
				cNroPago,; // Numero de Pagamento selecionado na Pergunte.
				NIL		,; // Periodo Ate - Passar "NIL", pois neste relatorio eh escolhido apenas um periodo.
				NIL		,; // Numero de Pagamento Ate - Passar "NIL", pois neste relatorio eh escolhido apenas um numero de pagamento.
				@aPerAbe,; // Retorna array com os Periodos e NrPagtos Abertos
				@aPerFec ) // Retorna array com os Periodos e NrPagtos Fechados

If  Empty(aPerAbe) .And. Empty(aPerFec)
	Aviso( OemToAnsi(STR0007), OemToAnsi(STR0012), {STR0009} ) //"No fue encontrado ningun periodo. Verifique los par�metros!"
	Return 
Endif

If (nPos:=aScan(aPerAbe, {|x| x[1] == cPeriodo .And. x[2] == cNroPago})) > 0 
	cAliasAux   := "SRC"
	cPrefixo    := "RC_"	
Elseif (nPos:=aScan(aPerFec, {|x| x[1] == cPeriodo .And. x[2] == cNroPago})) > 0 
	cAliasAux   := "SRD"
	cPrefixo    := "RD_"	
Endif 

cQuery 	:= 	"SELECT RA_FILIAL, RA_MAT, RA_CIC, RA_ACTTRAN, RA_ADMISSA,RA_PRINOME,RA_PRISOBR,RA_SECSOBR "
cQuery	+=	"FROM "
cQuery	+=	RetSqlName("SRA") + " SRA "	
cQuery  += 	"WHERE SRA.D_E_L_E_T_= ' ' AND "
cQuery  += 		"SRA.RA_FILIAL = '" + xFilial("SRA") + "' AND "
		If  !Empty(cCodMat)
			cQuery  +=	cCodMat +" AND "
		Endif
cQuery  +=	"SRA.RA_CC BETWEEN '"+ cIniCC +"' AND '"+ cFinCC +"' AND " 
//PDF4: Solo activos ?
cQuery +=	"RA_SITFOLH <> 'D' " 
cQuery +=	"ORDER BY RA_FILIAL, RA_MAT"
cQuery := ChangeQuery(cQuery)

dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cTmp,.T.,.T.)

TcSetField(cTmp, "RA_ADMISSA", "D", 08, 0)

Count to nRegs

ProcRegua(nRegs)
(cTmp)->(dbGoTop())

If  nOpc == 2 .And. nRegs > 0 //Archivo 1357
	If  !GenArch()  //Crea el archivo y registro 01.
		lProcesa := .F. 
	Endif
Endif

While (cTmp)-> (!Eof()) .And. lProcesa
	IncProc(STR0013 + Iif(nOpc==1,STR0014,STR0015)) //"Generando " 1 "formulario 1357... " o 2 "archivo 1357... " 
	
	lPositivo := .T. //Variable para verificar si todos los valores son >= 0
	aRemunera := Array(27)
	aDeduccio := Array(27)
	aDeducc23 := Array(11)	
	aCalcImpt := Array(8)
	
	//REMUNERACIONES
	aRemunera[1]:= "03"
	aRemunera[2]:= PADL((cTmp)->RA_CIC, 11, " ")
	For nloop := 3 to 27
		aRemunera[nloop]:= ObtMov("3", PADR(Alltrim(STR(nloop)),nTamNroC), cAliasAux, cPrefixo,(cTmp)->RA_FILIAL,(cTmp)->RA_MAT)
	Next
	
	//DEDUCCIONES
	aDeduccio[1]:= "04"
	aDeduccio[2]:= PADL((cTmp)->RA_CIC, 11, " ")
	For nloop := 3 to 27
		aDeduccio[nloop]:= ObtMov("4", PADR(Alltrim(STR(nloop)),nTamNroC), cAliasAux, cPrefixo,(cTmp)->RA_FILIAL,(cTmp)->RA_MAT)
	Next
	
	//DEDUCCIONES ART. 23
	aDeducc23[1]:= "05"
	aDeducc23[2]:= PADL((cTmp)->RA_CIC, 11, " ")
	For nloop := 3 to 11
		If  nloop != 7
			aDeducc23[nloop]:= ObtMov("5", PADR(Alltrim(STR(nloop)),nTamNroC), cAliasAux, cPrefixo,(cTmp)->RA_FILIAL,(cTmp)->RA_MAT)
		Else
			aDeducc23[nloop]:= "00" //PDF11
		Endif
	Next
	
	//CALCULO DE IMPUESTO
	aCalcImpt[1]:= "06"
	aCalcImpt[2]:= PADL((cTmp)->RA_CIC, 11, " ")
	aCalcImpt[3]:= "1"  //PDF10
	aCalcImpt[4]:= "1"  //PDF10 
	For nloop := 5 to 8
		aCalcImpt[nloop]:= ObtMov("6", PADR(Alltrim(STR(nloop)),nTamNroC), cAliasAux, cPrefixo,(cTmp)->RA_FILIAL,(cTmp)->RA_MAT)
	Next
	
	//Archivo 1357 
	If  lPositivo .And. nOpc==2 
		//Solo en caso de tener registros positivo se va a grabar el registro02...06
		GrabaReg02((cTmp)->RA_ADMISSA,(cTmp)->RA_ACTTRAN,(cTmp)->RA_CIC) //Longitud de 33
		GrabaRegXX(aRemunera,3) //Longitud de 390
		GrabaRegXX(aDeduccio,4) //Longitud de 390
		GrabaRegXX(aDeducc23,5) //Longitud de 135
		GrabaRegXX(aCalcImpt,6) //Longitud de 75
		lGenTXT := .T. 
		
	elseIf lPositivo .And. nOpc==1 //Archivo 1357
			//aadd(aEmpleados,{"8888999","empleado"+str(nX),"Mat"+str(nX),"CUIT EM"+str(nX),"RAZON ESP"+str(nX),"Periodo202"+str(nX)})
			aadd(aEmpleados,{(cTmp)->RA_CIC,ALLTRIM((cTmp)->RA_PRISOBR)+" "+ALLTRIM((cTmp)->RA_SECSOBR)+" "+(cTmp)->RA_PRINOME,(cTmp)->RA_MAT})
			aadd(aData,{aEmpleados,aRemunera,aDeduccio,aDeducc23,aCalcImpt})
			aEmpleados:={}
	Endif
	
	(cTmp)->(DbSkip())	
EndDo
(cTmp)->(dbCloseArea())	
If  lPositivo .And. nOpc==1
	fPrinReport(aData)
Endif
Return

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �GenArch   �Autor  �Laura Medina         �Fecha �  19/03/2020 ���
��������������������������������������������������������������������������͹��
���Desc.     � Generar archivo y registro 01                               ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE: Archivo 1357                                       ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function GenArch()
Local lRet   	:= .T.
Local cNomArch	:= "F1357."+Alltrim(SM0->M0_CGC)+"."+cPeriodo+"00."+StrZero(Val(cSecuenc),4)+".txt" 
Local cDrive	:= ""
Local cDir      := ""
Local cExt      := ""
Local cNewFile	:= ""

IIf (!(Substr(cNomArch,Len(cNomArch) - 2, 3) $ "txt|TXT"), cNomArch += ".TXT", "")

cNewFile := cRutaArc + cNomArch

SplitPath(cNewFile,@cDrive,@cDir,@cNomArch,@cExt)
cDir 	 := cDrive + cDir

Makedir(cDir,,.F.) //Crea el directorio en caso de no existir

cNewFile := cDir + cNomArch + cExt   
nArchTXT := FCreate (cNewFile,0)

If nArchTXT == -1
	Aviso( OemToAnsi(STR0007), OemToAnsi(STR0008 + cNomArch), {STR0009} ) //"Atencion" - "No se pudo crear el archivo " - "OK"
	lRet   := .F.
EndIf

If  lRet
	GrabaReg01() //Longitud de 38
Endif
	
Return lRet


/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �GrabaReg01 �Autor  �Laura Medina        �Fecha �  19/03/2020 ���
��������������������������������������������������������������������������͹��
���Desc.     �Archivo 1357 | Registro 01 |                                 ���
���          �El registro cabecera debe ser el primer registro del archivo,���
���          �con una longitud de 38 (treinta y ocho) caracteres.          ��� 
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE: Archivo 1357                                       ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function GrabaReg01() 
//Longitud de 38 caracteres
Local cLinea	:= ""
Local aFilAtu	:= FWArrFilAtu()

cLinea := "01"
cLinea += PADR(STRTRAN(aFilAtu[18],"-",""), 11, " ")
cLinea += IIf(nTipoPre==1,Substr(cPeriodo,1,4)+"00",cPeriodo) 
cLinea += StrZero(Val(cSecuenc),2)
cLinea += PADR(GetMv("MV_1357IMP",,""),4)
cLinea += PADR(GetMv("MV_1357CON",,""),3)
cLinea += PADR(GetMv("MV_1357FOR",,""),4)
cLinea += Alltrim(Str(nTipoPre))
cLinea += PADR(GetMv("MV_1357SIS",,""),5)
FWrite(nArchTXT, cLinea)
cLinea := ""

Return


/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �GrabaReg02 �Autor  �Laura Medina        �Fecha �  19/03/2020 ���
��������������������������������������������������������������������������͹��
���Desc.     � Archivo 1357 | Registro 02 |                                ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE: Archivo 1357                                       ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function GrabaReg02(dAdmissa,nActTran,cCIC)
//Longitud de 33 caracteres
Local cLinea	:= ""
Local cAnoMes   := ALLTRIM(STR(YEAR(dAdmissa)))+STRZERO(MONTH(dAdmissa),2) //A�o + Mes de la fecha de Admisi�n 
Local cPerIni   := Iif(cAnoMes < cPeriodo,Substr(cPeriodo,1,4)+"0101",cAnoMes+"01")

cLinea := CRLF 
cLinea += "02"
cLinea += PADR(cCIC, 11, " ")
cLinea += cPerIni 
cLinea += cPeriodo+"01" //PDF5 
cLinea += StrZero(( Val(Substr(cPeriodo,5,2)) - Val(Substr(cPerIni,5,2)) ),2)  //PDF7
cLinea += "1" //PDF6	
cLinea += Iif(nActTran!= "1", "0", nActTran) //PDF9
FWrite(nArchTXT, cLinea)
cLinea := ""

Return

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �GrabaRegXX �Autor  �Laura Medina        �Fecha �  19/03/2020 ���
��������������������������������������������������������������������������͹��
���Desc.     � Archivo 1357 | Registro 03, 04 Y 05 |                       ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE: Archivo 1357                                       ���
���Parametros� aRegistroX:= Arreglo con los movimientos (RC/RD_VALOR).     ���
���          �                                                             ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function GrabaRegXX(aRegistroX,nReg)
Local nLoop	  := 0 
Local nInicio := 3

cLinea := CRLF 
cLinea += aRegistroX[1]
cLinea += aRegistroX[2]
If  nReg == 6
	nInicio := 5
	cLinea += aRegistroX[3]
	cLinea += aRegistroX[4]
Endif

For nLoop := nInicio To Len(aRegistroX)
	If  nReg == 5 .And. nLoop == 7
		cLinea +=  aRegistroX[nLoop] 
	Else
		If  nLoop < 27
			cLinea += PADL(STRTRAN(AllTrim(STRTRAN(Transform(aRegistroX[nLoop], cPictVal),",","")),".",""), 15, "0")
		Else
			cLinea += PADL(STRTRAN(AllTrim(STRTRAN(Transform(aRegistroX[nLoop], cPictV17),",","")),".",""), 17, "0")
		Endif
	Endif
Next
FWrite(nArchTXT, cLinea)
cLinea := ""
		
Return

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �Vl1537     �Autor  �Laura Medina        �Fecha �  19/03/2020 ���
��������������������������������������������������������������������������͹��
���Desc.     � Validaci�n del periodo, debe ser mayor o igual a 2018       ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE: Archivo 1357                                       ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Function Vl1537(cPerVld, nTipPr)
Local lRet := .T. 

If  nTipPr == 1 .And. Substr(cPerVld,1,4)< "2018" 
	lRet := .F.
	Aviso( OemToAnsi(STR0007), OemToAnsi(STR0010), {STR0009} ) //"Atencion" - "Informe un periodo valido (a partir del 2018)."  
Elseif (nTipPr == 2 .OR. nTipPr == 3) .And. cPerVld < "201904" 
	lRet := .F. //PDF1,2
	Aviso( OemToAnsi(STR0007), OemToAnsi(STR0016), {STR0009} ) //"Informe un periodo valido (a partir del 201904)." 
Endif

Return lRet 


/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �ObtMov     �Autor  �Laura Medina        �Fecha �  24/03/2020 ���
��������������������������������������������������������������������������͹��
���Desc.     � Archivo 1357 | Registro 06 |                                ���
��������������������������������������������������������������������������͹��
���Uso       � SIGAGPE: Archivo 1357                                       ���
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
Static Function ObtMov(nNroReg, nNroCamp, cAliasAux, cPrefixo, cFilMov, cMatMov )
Local cQuery	:= ""
Local cTmp		:= GetNextAlias()
Local nRenumera := 0

cQuery := 	"SELECT SUM("+cPrefixo+"VALOR) "+cPrefixo+"VALOR "
cQuery +=	"FROM "
cQuery +=	RetSqlName(cAliasAux) +" "+ cAliasAux + ", "  +RetSqlName("SRV")+ " SRV " 
cQuery += 	"WHERE "+ cAliasAux+"."+cPrefixo+"MAT = '" +cMatMov + "'  AND "
cQuery += 		cAliasAux+"."+cPrefixo+"FILIAL	= '" +  cFilMov+ "'  AND "
cQuery += 		cAliasAux+"."+cPrefixo+"PROCES	= '" +  cProceso+ "'  AND "
cQuery += 		cAliasAux+"."+cPrefixo+"ROTEIR	= '" +  cProced+ "'  AND "
cQuery += 		cAliasAux+"."+cPrefixo+"PERIODO	= '" +  cPeriodo+ "'  AND "
cQuery += 		cAliasAux+"."+cPrefixo+"SEMANA	= '" +  cNroPago+ "'  AND "
cQuery += 		cAliasAux+"."+cPrefixo+"PD = SRV.RV_COD AND "
//RV_NROREG = 3 y RV_NROCAMP = 3,4,...27
cQuery += 		"SRV.RV_NROREG = '" + nNroReg + "'  AND "
cQuery += 		"SRV.RV_NROCAMP= '" + nNroCamp+ "' AND "
cQuery +=		cAliasAux+".D_E_L_E_T_= ' ' AND "
cQuery +=		"SRV.D_E_L_E_T_= ' ' AND "
cQuery +=		"SRV.RV_FILIAL = '" + xFilial("SRV")+ "' ""
cQuery := ChangeQuery(cQuery)
dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cTmp,.T.,.T.)

If (cTmp)-> (!Eof())
	nRenumera := (cTmp)->&((cPrefixo)+"VALOR")
	
	If  nRenumera <0  
		lPositivo := .F. 
	Endif
Endif
(cTmp)->(dbCloseArea())	

Return nRenumera

