#INCLUDE "GPER140COL.ch"
#DEFINE   nColMax	2300
//#DEFINE   nColIni	100
#DEFINE   nLinMax  2870

/*
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
��������������������������������������������������������������������������������Ŀ��
���Fun��o    � Gper140col  � Autor � Cesar Bautista          � Data � 26.09.18   ���
��������������������������������������������������������������������������������Ĵ��
���Descri��o � Carta de Finiquito - Colombia                                     ���
��������������������������������������������������������������������������������Ĵ��
��� Uso      � Generico                                                          ���
��������������������������������������������������������������������������������Ĵ��
���              ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.               ���
��������������������������������������������������������������������������������Ĵ��
���Programador   � Data   � BOPS/FNC  �  Motivo da Alteracao                     ���
��������������������������������������������������������������������������������Ĵ��
���������������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������������
��������������������������������������������������������������������������������� */
Function GPER140COL()

	Local olReport
	Local clNomProg		:= FunName()
	Local clTitulo 		:= STR0001 //"CARTA FINIQUITO"A
	Local clDesc   		:= STR0002 //"Empresa: "
	
	Private oFont		:= TFont():New("Courier New",09,12,.T.,.F.,5,.T.,5,.F.,.F.)
	Private oFont1		:= TFont():New("Courier New",09,13,.T.,.T.,5,.T.,5,.F.,.F.)
    Private oFont10 	:= TFont():New("Courier New",,-10,,)
    Private oPrint 
    Private nColIni	:= 100 
    
	If FindFunction("TRepInUse") .And. TRepInUse()
		Pergunte("GPER140COL",.T.)
		olReport:=TReport():New(clNomProg,clTitulo,"GPER140COL",{|olReport| fImp140Col(olReport)},clDesc)
		olReport:oPage:nPaperSize	:=  1 //cARTA //9 	// Impress�o em papel A4
		olReport:lHeaderVisible  	:= .F. 	// N�o imprime cabe�alho do protheus
		olReport:lFooterVisible  	:= .F.	// N�o imprime rodap� do protheus
		olReport:lParamPage			:= .F.	// N�o imprime pagina de parametros
		olReport:PrintDialog()
	Endif

Return


 
Static Function fImp140Col(oReport)

	Local i			:= 0
	Local nlBaseTot	:= 0 //Valor base do imposto total
	Local nlImpTot	:= 0 //Valor do imposto total
	Local nlBaseRet	:= 0 //Valor base do imposto por aliquota
	Local nlImpRet	:= 0 //Valor do imposto por aliquota
	Local nlAliq	:= 0 //Aliquota
	Local k			:= 0
	Local clForn	:= "" //Codigo do fornecedor
	Local clLoja	:= "" //Loja
	Local cData		:= ""
	Local aAliq		:= {} //Array das aliquotas por fornecedor
	Local clNomPer	:= ""
	Local cNitCC	:= ""
	Local cPicNit	:= ""
	Local cPicNitAg	:= GetSx3Cache( "A2_CGC" , "X3_PICTURE" )
	Local cFilMat	:= ""
	Local cAcessaSRG	:= ""
	Local cAcessaSRR	:= ""
	Local cAcessaSRC	:= ""
	Local aCodBenef 	:= {}
	Local nHoras   	:= ""
	Local nOrdSRC	:= RetOrdem("SRC","RC_FILIAL+RC_MAT+RC_PROCES+RC_ROTEIR+RC_PERIODO+RC_SEMANA")
	Local cRoteir  	:= fGetCalcRot('4')	// RESCISAO
	Local ntot		:= 0
	Local nCont		:= 2
	Local Imprime	:= 1   				//LINHA DE IMPRESSAO DO RELATORIO GRAFICO 
	Local nImpr2    := 1
	Local n			:= 0
	Local nln		:=0 
	Private cMatriculas := ""
	Private cRanProces := "" 
	Private cfiltroSRC := ""
	Private aPdv 	:= {}
	Private	aHomV	:= {}
	Private	aHomD	:= {}	
	Private	aPdd 	:= {}
	Private	aPdn	:= {} 
	Private	aPdpm	:= {}
	Private	aPddm	:= {}
	Private	aImpr	:= {}
	Private	aResc	:= {}
	Private aCodFol		:= {}         // Matriz com Codigo da folha
	Private cFiliAnt := "__cFiliAnt__"
	Private nPos		:= 0				//LINHA DE IMPRESSAO DO RELATORIO GRAFICO
	Private nTot		:= 0
	Private nNumMax		:= 25				//Numero maximo de verbas impressas no Detalhe da rescisao 
	Private nImprime	:= 1 				//Variavel Auxiliar 
	Private nImpre		:= 1 
	Private CONTFL		:= 1				//CONTA PAGINA
	Private cCodMsg 	:= ""
	Private cIdRep	:= ""
	Private cTpIdRep 	:= ""
	Private cNomRep	:= ""
	Private nNumLn		:=0
	
	cAcessaSRG	:= &( " { || " + ChkRH( "GPER140" , "SRG" , "2" ) + " } " )
	cAcessaSRR	:= &( " { || " + ChkRH( "GPER140" , "SRR" , "2" ) + " } " )
	cAcessaSRC	:= &( " { ||  } " )
	
	//������������������������������������������������������������������Ŀ
	//�Objetos para Impressao Grafica - Declaracao das Fontes Utilizadas.�
	//��������������������������������������������������������������������
	Private oFont11n,oFont14n, oFont09, oFont09n,oFont07 
	
	oFont07	:= TFont():New("Arial",07,07,,.F.,,,,.T.,.F.)
	oFont07n:= TFont():New("Arial",09,09,,.F.,,,,.T.,.F.)
	oFont09	:= TFont():New("Arial",09,09,,.F.,,,,.T.,.F.)
	oFont09n:= TFont():New("Arial",09,09,,.T.,,,,.T.,.F.)     //Negrito//
	oFont11n:= TFont():New("Arial",11,11,,.T.,,,,.T.,.F.)     //Negrito//
	oFont14n:= TFont():New("Arial",14,14,,.T.,,,,.T.,.F.)     //Negrito//
	Pergunte("GPER140COL",.f.)
	MakeAdvplExpr("GPER140COL")
	cRanProces	:= MV_PAR01
	cRoteir		:= MV_PAR02
	cMatriculas := IIF(Empty(AllTrim(MV_PAR05)),"1==1",MV_PAR05)
	cCodMsg 	:= MV_PAR06
	cIdRep		:= MV_PAR09
	cTpIdRep 	:= MV_PAR08
	cNomRep		:= MV_PAR07
	
	dbSelectArea("SRA")
	SRA->(DBGOTOP())
	SRA->(dbSetOrder(1))
	WHILE ! (SRA->(eof()))	
	 
	 	If ( &(cMatriculas))
		 	// Obtiene Datos
		 	//MOVIMENTA REGUA DE PROCESSAMENTO
			//IncRegua()
			cFilMat	:= ( SRA->RA_FILIAL + SRA->RA_MAT )
			
			aPdv 	:= {}
			aHomV	:= {}
			aHomD	:= {}	
			aPdd 	:= {}
			aPdn	:= {} 
			aPdpm	:= {}
			aPddm	:= {}
			aImpr	:= {}
			aResc	:= {}
		 	
		 	//CARREGA VARIAVEIS CODIGOS DA FOLHA
			If ( cFiliAnt # SRA->RA_FILIAL )
				cFiliAnt := SRA->RA_FILIAL
				If !fP_CodFol(@aCodFol,cFiliAnt)
					Return
				EndIf
			EndIf
		 	//DEFINE CODIGOS DE IDENTIfICADORES PARA O PERGUNTE MV_PAR16 PARA CALCULAR FERIAS EM AVOS OU EM HORAS
			cVerCodFol:=aCodFol[086,1]+"*"+ ;	//  Ferias Indenizadas
			            aCodFol[087,1]+"*"+ ;	//  Ferias Proporcionais
			            aCodFol[230,1]+"*"+ ;	//	Ferias s/ Av.Previo Indenizado
			            aCodFol[248,1]+"*"+ ;	//	Media Ferias Vencidas Rescisao
			            aCodFol[249,1]+"*"+ ;	//	Media Ferias Proporcionais Rescisao
			            aCodFol[252,1]			//	Media Ferias Sobre Aviso Indenizado Rescisao
			//DEFINE CODIGOS DE IDENTIfICADORES PARA O PERGUNTE MV_PAR32 PARA CALCULAR 13 SAL.EM AVOS OU EM HORAS.
			cVerCod13 :=aCodFol[114,1]+"*"+ ;	//  13o. na Indenizacao
		            	aCodFol[115,1]+"*"+ ;  	//  13o. Sobre Aviso Previo
		            	aCodFol[251,1]+"*"+ ; 	//	Media 13o. Salario Rescisao
		            	aCodFol[253,1]        	//	Media 13o. Salario Sobre Aviso Indenizado Rescisao
		
			
			// Busca en SRG
			dDtDemi := ctod("  /  /")
			dbSelectArea("SRG")
			If SRG->(dbSeek(xFilial("SRG")+SRA->RA_MAT))
	 			If SRG->RG_EFETIVA != "1"
					dDtDemi := SRG->RG_DATADEM
				EndIf						
			EndIf
	
			lSelec	:= .F.
			DbSelectArea("SRC")
			SRC->(DbSetOrder(nOrdSRC))
			SRC->(DbSeek(xFILIAL("SRC")+SRA->RA_MAT+cRanProces+cRoteir+MV_PAR03+MV_PAR04, .F.))
			If SRC->(Eof())
			  	cSvRoteir	:= cRoteir 
			  	cRoteir 	:= fGetCalcRot('A')	// Aplicacao de Rescisao
			   	DbSeek(SRA->(RA_FILIAL+RA_MAT+RA_PROCES)+cRoteir, .F.)
			   	If SRC->(Eof())
			   	   cRoteir := cSvRoteir
			   	Else
			   		lSelec	:= .T.
			   	EndIf
			Else
				lSelec	:= .T.
			EndIf
	
			nProv := nDesc := nNProv := 0.00
	
			If lSelec
				While SRC->(!Eof()) .And.	;
						(SRC->RC_FILIAL + SRC->RC_MAT + SRC->RC_PROCES + SRC->RC_ROTEIR + SRC->RC_PERIODO + SRC->RC_SEMANA) == ;
						(SRA->RA_FILIAL + SRA->RA_MAT + SRA->RA_PROCES + cRoteir+MV_PAR03+MV_PAR04)
					//CONSISTE FILIAIS E ACESSOS
					If !(SRC->RC_FILIAL $ fValidFil()) //.Or. !Eval(cAcessaSRC))
						dbSelectArea("SRC")
						SRC->( dbSkip() )
						Loop
					EndIf
					//NAO IMPRIME VALORES ZERADOS
					If (SRC->RC_VALOR == 0.00 )
						SRC->( dbSkip() )
						Loop
					EndIf
					//VERIfICA O TIPO DA VERBA E CARREGA NO aPd CORRESPONDENTE
					nHoras	:= If(SRC->RC_TIPO1=="H",fConvHoras(SRC->RC_HORAS,"1"),SRC->RC_HORAS)
	
					If PosSrv(SRC->RC_PD,SRA->RA_FILIAL,"RV_TIPOCOD") == "1" .AND. PosSrv(SRC->RC_PD,SRA->RA_FILIAL,"RV_IMPRIPD") == "1"  //Provento
						Aadd(aPdv, Str(nHoras,6,2) + " " + Substr(DescPd(SRC->RC_PD,xFilial( "SRV", SRA->RA_FILIAL),20),1,20) + " " + TransForm(SRC->RC_VALOR,"99,999,999.99") )
						nProv += SRC->RC_VALOR
					ElseIf PosSrv(SRC->RC_PD,SRA->RA_FILIAL,"RV_TIPOCOD") == "2" .And. SRC->RC_PD # aCodfol[126,1] .AND. PosSrv(SRC->RC_PD,SRA->RA_FILIAL,"RV_IMPRIPD") == "1" // Desconto e Despreza Liq. da Rescisao
						Aadd(aPdd, Str(nHoras,6,2) + " " + Substr(DescPd(SRC->RC_PD,xFilial( "SRV", SRA->RA_FILIAL),20),1,20) + " " + TransForm(SRC->RC_VALOR,"99,999,999.99") )
						nDesc += SRC->RC_VALOR
					ElseIf PosSrv(SRC->RC_PD,xFilial( "SRV", SRA->RA_FILIAL),"RV_TIPOCOD") == "3" .AND. PosSrv(SRC->RC_PD,SRA->RA_FILIAL,"RV_IMPRIPD") == "1" //Base Provento
						Aadd(aPdpm,Str(nHoras,6,2) + " " + Substr(DescPd(SRC->RC_PD,xFilial( "SRV", SRA->RA_FILIAL),20),1,20) + " " + TransForm(SRC->RC_VALOR,"99,999,999.99") )					
					EndIf
					
					SRC->(dbSkip())
				EndDo
			EndIf
			If lSelec
				//fInfo(@aInfo,SRA->RA_FILIAL)
				cCodSaq 	:= "  "
				cImpSac 	:= " "    
				cCodR		:= " "                                        
	
				cCausa := Space(30)

	
				//IMPRIME O NUMERO DE VIAS ESPECIfICADO
				SET CENTURY OFF
					nTot 	:= Max( Len(aPdv) , Len(aPdd) )
					nTot	:= Max( nTot , Len(aPdpm) )
					
				//For nImprVias := 1 to nNumVias
					///------------------------
					// Imprimi reporte
					fEnc(oReport,@nNumLn)
					
					//*--------------------
					While Imprime <=  nTot
						//-- Impressao do rodape e quebra de pagina 
						If nImpre > ( nNumMax ) .or. nImprime > nNumMax .or. nImpr2 > nNumMax
						nNumLn := 2650 //nNumLn + 570
							fRodape(oReport,nNumLn) 
							oReport:EndPage() 
							oReport:StartPage()  
							ContFl++  
							nNumLn := 0
							fEnc(oReport,@nNumLn)
							nCont += 2
							nNumMax := ( nNumMax * nCont ) 
							nImpre	:= ( nImpre + n)
						Endif 
					
						If nImpre <= nNumMax .or. Imprime <= nNumMax .or. nImpr2 <= nNumMax
							
							//--Coluna de Proventos  
							If nImpre <= Len(aPdv) 
								//--Primeira coluna de Provento 
								oReport:say (nNumLn,nColIni + 80 ,Space(1) + PADR(Subs(aPdv[nImpre],01,06),6),oFont07n,,,,1)
								oReport:say (nNumLn,nColIni + 100 ,Space(1) + PADL(Subs(aPdv[nImpre],08,20),20),oFont07n) 
								oReport:say (nNumLn,nColIni + 680,Space(1) + PADR(Subs(aPdv[nImpre],29,13),13),oFont07n,,,,1) 
								nImpre	+= 1
							Endif
							//-- Coluna de Descontos
							If nImprime <= Len(aPdd)  
								oReport:say (nNumLn,nColIni + 780,Space(1)  + PADR(Subs(aPdd[nImprime],01,06),6)   , oFont07n,,,,1)
								oReport:say (nNumLn,nColIni + 800,Space(1)  + PADL(Subs(aPdd[nImprime],08,20),20) , oFont07n)
								oReport:say (nNumLn,nColIni + 1430,Space(1) + PADR(Subs(aPdd[nImprime],29,13),13), oFont07n,,,,1)
								nImprime	++
							Endif
							//-- Coluna de Base 
							If nImpr2 <= Len(aPdpm)  
								oReport:say (nNumLn,nColIni + 1530,Space(1) + PADR(Subs(aPdpm[nImpr2],01,06),6)    , oFont07n,,,,1)
								oReport:say (nNumLn,nColIni + 1550,Space(1) + PADL(Subs(aPdpm[nImpr2],08,20),20) , oFont07n)
								oReport:say (nNumLn,nColIni + 2180,Space(1) + PADR(Subs(aPdpm[nImpr2],29,14),13), oFont07n,,,,1)
								nImpr2	++
							Endif
										
						Endif  
						
						Imprime 	+= 1
						nNumLn +=40 
						
					Enddo 
					nNumLn := 2650 //nNumLn + 570
					fRodape(oReport,nNumLn)
					
				If nTData == 10
					SET CENTURY ON
				Else
					SET CENTURY OFF
				EndIf
			EndIf
			
			If SRA->(eof())
				exit
			EndIF
						
		EndIf
		SRA->(DbSkip())
	EndDo	
Return

Static Function fEnc(oPrint,nNumLn)
	Local ainfo := {}
	Local cDir	:= GetSrvProfString("Startpath","")//GetClientDir()
	lOCAL aSM0Info :=  FWLOADSM0()
	lOCAL cempusa := aSM0Info[1,1]
Private cArquivo    	:= "GPE140COL"	//Nome do arquivo Bitmap que sera impresso na primeira pagina

/*/
cArquivo :=  cArquivo + cempusa + ".PNG"
If !File(GetClientDir() + cArquivo)
MsgAlert(cDir+cArquivo)	//-"Arquivo n�o encontrado -> "
//Return
EndIf      
cArquivoC	:= fLoadLogo(cArquivo)
/*/
	fSM0(@aInfo,SRA->RA_FILIAL)
	oPrint:StartPage() 			//Inicia uma nova pagina  
	
	//����������������������������������������������������������������������������Ŀ
	//�TERMO DE RESCISAO DO CONTRATO DE TRABALHO                                   �
	//������������������������������������������������������������������������������
	nlin := 50
	
	oPrint:SayBitmap(nlin+30, 110, cDir + "lgrl" + FwCodEmp("SM0")+".bmp", 250, 120)
	//oPrint:SayBitmap(nlin-50, 70, cArquivoC, 330, 280,,.F.)
	oPrint:say (nlin+50,1000,STR0001,oFont14n)			 	//"TERMO DE RESCISAO DO CONTRATO DE TRABALHO"
	
	////////////////////////////////////////////
	nlin += 110
	//oPrint:say (nlin , nColIni , STR0002, oFont11n) // "Empresa: "
	
	nlin += 60
	oPrint:Box( nlin , nColIni , nlin+180 ,nColMax ) 					 	  			 	//-- Box Identificacao da Empresa
	
	nlin += 20
	oPrint:say (nlin , nColIni+20	, STR0002, oFont09n) // "Empresa: "
	oPrint:say (nlin , 0820			, STR0003, oFont09n) //"Sucursal: "
	oPrint:say (nlin , 1520 		, STR0004, oFont09n)  // nit
	
	nlin += 50
	oPrint:say (nlin , nColIni+35 , aInfo[1], oFont09) 
	oPrint:say (nlin , 0825 , aInfo[3], oFont09) 
	oPrint:say (nlin , 1535 , aInfo[8], oFont09) 
	
	nlin += 50
	cDir := alltrim(aInfo[4]) + STR0047 + alltrim(aInfo[13]) + STR0047 + alltrim(aInfo[5]) + STR0047 +STR0048 + aInfo[7]
	oPrint:say (nlin , nColIni+35 , cDir, oFont09) 

	////////////////////////////////////////////
	nlin += 150
	oPrint:Box( nlin , nColIni , nlin+1050 ,nColMax )					 	  			 	//-- Box Identificacao do Trabalhador
	
	nlin +=20
	oPrint:say (nlin,nColIni+20,STR0005, oFont09n) 		 							
	oPrint:say (nlin,0820,STR0006, oFont09n)		//Nombre						   

	nlin += 50                                 
	oPrint:say (nlin,nColIni+35 ,SRA->RA_MAT , oFont07n ) 			
	oPrint:say (nlin,0820 ,SRA->RA_NOME, oFont07n )						   		

	nlin += 60
	oPrint:say (nlin,nColIni+20 ,STR0007, oFont09n)  // "Tipo Id: "								
	oPrint:say (nlin,0820,STR0008, oFont09n)		 // "No. Id: "	
						   							  	  		
	nlin += 50
	oPrint:say (nlin,nColIni+35 ,Alltrim(fDescRCC("S022",SRA->RA_TPCIC,1,2,3,25)), oFont07n)			 	  						
	oPrint:say (nlin,0820 ,SRA->RA_CIC, oFont07n)

	nlin += 50
	oPrint:say (nlin,nColIni + 20,STR0009, oFont09n) // Centro de Costos			 	  							
	oPrint:say (nlin,0820 		 ,STR0010, oFont09n) // Departamento
	oPrint:say (nlin,1635 		 ,STR0011, oFont09n) // cARGO			 	  						
	
	nlin += 50
	oPrint:say (nlin,nColIni + 35,SRA->RA_CC+" - "+Posicione("CTT",1,xFilial("CTT")+SRA->RA_CC,"CTT_DESC01"), oFont07n)	
	oPrint:say (nlin,0820 ,SRA->RA_DEPTO+" - "+Posicione("SQB",1,xFilial("SQB")+SRA->RA_DEPTO,"QB_DESCRIC"), oFont07n)			 	  							
	oPrint:say (nlin,1635 ,SRA->RA_CARGO +" - "+Posicione("SQ3",1,xFilial("SQ3")+SRA->RA_CARGO,"Q3_DESCSUM"), oFont07n)			 	  						
					   	   				

	nlin += 50
	oPrint:say (nlin,nColIni+20 ,STR0012, oFont09n)  		// Funci�n							
	oPrint:say (nlin,0820,STR0013, oFont09n)				// Sindicato			   	
	oPrint:say (nlin,1620,STR0014, oFont09n) 				// Proceso		  	  			
	
	nlin += 50
	oPrint:say (nlin,nColIni+35 ,Posicione("SRJ",1,xFilial("SRJ")+SRA->RA_CODFUNC,"RJ_DESC"), oFont07n)				 	  					
	oPrint:say (nlin,0820 ,Posicione("RCE",1,xFilial("RCE")+SRA->RA_SINDICA,"RCE_DESCRI"), oFont07n)	 				   	   				
	oPrint:say (nlin,1635,SRA->RA_PROCES + " - " + Posicione("RCJ",1,xFilial("RCJ") + SRA->RA_PROCES,"RCJ_DESCRI") , oFont07n)	   								   
	
	nlin += 50
	oPrint:say (nlin,nColIni+20 ,STR0015, oFont09n)  		// EPS							
	oPrint:say (nlin,0820,STR0016, oFont09n)				// AFP			   	
	oPrint:say (nlin,1620,STR0017, oFont09n) 				// ARL	
	
	nlin += 50
	oPrint:say (nlin,nColIni+35 , aLLtRIM(SRA->RA_CODRPAT) +"-"+ Alltrim(fDescRCC("S008",SRA->RA_CODRPAT,1,17,18,80)), oFont07n)				 	  					
	oPrint:say (nlin,0820 , aLLtRIM(SRA->RA_CODAFP) +"-"+Alltrim(fDescRCC("S008",SRA->RA_CODAFP,1,17,18,80)), oFont07n)	 				   	   				
	oPrint:say (nlin,1635, AllTrim(SRA->RA_NITARL) +"-"+Alltrim(fDescRCC("S008",SRA->RA_NITARL,1,17,18,80)) , oFont07n)	   								   
	
	nlin += 50
	oPrint:say (nlin,nColIni+20 ,STR0018, oFont09n)  		// CCF							
	oPrint:say (nlin,0820,STR0019, oFont09n)				// oFICIO

	nlin += 50
	oPrint:say (nlin,nColIni+35,SRA->RA_CXCOMP +" - "+Alltrim(fDescRCC("S008",SRA->RA_CXCOMP,1,17,18,80)), oFont07n)    
	oPrint:say (nlin,0820 , SRA->RA_OFICIO + "-" + Alltrim(fDescRCC("S017",SRA->RA_OFICIO,1,6,7,50)), oFont07n)
	
	nlin += 50
	oPrint:say (nlin,nColIni+20 ,STR0020, oFont09n)  		// "Horas por mes: "						
	oPrint:say (nlin,0820,STR0021, oFont09n)				// "ReteFte: "			   	
	oPrint:say (nlin,1620,STR0022, oFont09n) 				// "% ReteFte: "	
	
	nlin += 50
	oPrint:say (nlin,nColIni+35 ,TRANSFORM(SRA->RA_HRSMES,"999.99"), oFont07n)  		// "Horas por mes: "						
	oPrint:say (nlin,0820,IiF(SRA->RA_TIPOPRC=='1', "Procedimiento 1","Procedimiento 2"), oFont07n)				// "ReteFte: "			   	
	oPrint:say (nlin,1620,TRANSFORM(SRA->RA_PERCIR,"999.99"), oFont07n) 				// "% ReteFte: "	
	
	nlin += 50
	oPrint:say (nlin,nColIni+20 ,STR0023, oFont09n)  		// "Tipo Cesantia: "					
	oPrint:say (nlin,0820,STR0024, oFont09n)				// "Tipo Salario: "			   	
	oPrint:say (nlin,1620,STR0025, oFont09n) 				// "Salario: "	
	
	nlin += 50
	oPrint:say (nlin,nColIni+35 ,SRA->RA_OPTLEI, oFont07n)  		// "Horas por mes: "						
	oPrint:say (nlin,0820,FTIPOSAL(SRA->RA_TIPOSAL), oFont07n)				// "ReteFte: "			   	
	oPrint:say (nlin,1620,TRANSFORM(SRA->RA_SALARIO,"999,999,999,999.99"), oFont07n) 				// "% ReteFte: "	
	
	nlin += 50
	oPrint:say (nlin,nColIni+20 ,STR0026, oFont09n)  		// "Tipo Cesantia: "					
	oPrint:say (nlin,0820,STR0027, oFont09n)				// "Tipo Salario: "			   	
	oPrint:say (nlin,1620,STR0028, oFont09n) 				// "Salario: "	
	nlin += 50
	oPrint:say (nlin,nColIni+35 ,DTOC(SRA->RA_ADMISSA), oFont07n)  	// "Horas por mes: "						
	oPrint:say (nlin,0820,DTOC(SRA->RA_DTREC), oFont07n)				// "ReteFte: "			   	
	oPrint:say (nlin,1620,DTOC(SRA->RA_FECAUM), oFont07n) 			// "% ReteFte: "	
	
	nlin += 50
	oPrint:say (nlin,nColIni+20 ,STR0029, oFont09n)  		// "Tipo Cesantia: "					
	oPrint:say (nlin,0820,STR0030, oFont09n)				// "Tipo Salario: "			   	
	oPrint:say (nlin,1620,STR0031, oFont09n) 				// "Salario: "	
	nlin += 50
	oPrint:say (nlin,nColIni+35 ,SRA->RA_TIPOCO +"-"+ Alltrim(fDescRCC("S018",SRA->RA_TIPOCO,1,3,4,50)), oFont07n)  		// "Horas por mes: "						
	oPrint:say (nlin,0820,DTOC(SRA->RA_DEMISSA), oFont07n)				// "ReteFte: "			   	
	oPrint:say (nlin,1620,SRA->RA_TIPFIN +"-"+ Alltrim(fDescRCC("S020",SRA->RA_TIPFIN,1,2,3,50)), oFont07n) 				// "% ReteFte: "	
	
	//�������������������������������������������������������������aa���������������Ŀ
	//�Discriminacao das Verbas Rescisorias                                        �
	//������������������������������������������������������������������������������
	nlin += 170
	oPrint:Box( nlin, nColIni, nlin+970, nColMax )
	oPrint:line(nlin,0800,nlin+970,0800) 							//-- Linha Vertical Meio 
	oPrint:line(nlin,1550,nlin+970,1550)							//-- Linha Vertical Meio 
	
	nlin += 20
	oPrint:say (nlin,nColIni+20 ,oemtoansi(STR0032), oFont09n)		//-- Proventos
	oPrint:say (nlin,820 ,oemtoansi(STR0033), oFont09n)		//-- Descontos
	oPrint:say (nlin,1570,oemtoansi(STR0034), oFont09n)		//-- Bases
	
	nlin += 70
	oPrint:line(nlin,nColIni,nlin,nColMax) 						//-- Linha Horizontal Proventos/Descontos
	
	nlin += 30
	nNumLn := nLin // 970 original 570 ajusta texto pei de pagina
Return Nil


Static Function fSM0( aInfo1 )
	Local aArea				:= GetArea()
	Local aAreaSM0			:= SM0->(GetArea())
	Local cTipEnd			:= "E"
	Local lRetorno			:= .T.
	Local cFilCorrente	:= cFilAnt
	Local cEmpCorrente	:= cEmpAnt
// 01 - Filial
	// 02 - Reduz Filial
	// 03 - Nome Completo
	// 04 - Endereco Cobranca
	// 05 - Cidade Cobranca
	// 06 - Estado Cobranca
	// 07 - Cep Cobranca
	// 08 - Cgc Cobranca
	// 09 - Insc Cobranca
	// 10 - Telefone
	// 11 - Fax 					Char	14
	// 12 - Producao Rural			Char	01  - "S" ou "N""
	// 13 - Bairro Cobranca 		Char	20
	// 14 - Compl. End. Cobranca	Char	12
	// 15 - Tipo de Inscricao		Num	01  -  1-CEI, 2-CGC, 3-CPF, 4-INCRA
	// 16 - CNAE					Char	05
	// 17 - FPAS					Char	04
	// 18 - Acid. de Trabalho		Char	08
	// 19 - C�digo Municipio		Char	07
	// 20 - Natureza Juridica		Char	04
	// 21 - Data Base Pgto. RAIS	Char	02
	// 22 - N� de Proprietarios		Char	02
	// 23 - Se Modificou Endere�o 	Char	01  - "S" ou "N"
	// 24 - Se Modificou INSC/CGC 	Char	01  - "S" ou "N"
	// 25 - Causa da Mudan�a		Char	01
	// 26 - INC./CGC Anterior		Char	14
	// 27 - CEI						Char	14	
	
	aInfo := Array(28)
	
	IF ( lRetorno := SM0->( dbSeek( cEmpCorrente + cFilCorrente ) ) )
		aInfo[ 01 ] := SM0->M0_FILIAL
		aInfo[ 02 ] := SM0->M0_NOME
		aInfo[ 03 ] := SM0->M0_NOMECOM

		IF ( cTipEnd == "C" )
			aInfo[ 04 ] := SM0->M0_ENDCOB
			aInfo[ 05 ] := SM0->M0_CIDCOB
			aInfo[ 06 ] := SM0->M0_ESTCOB
			aInfo[ 07 ] := SM0->M0_CEPCOB
		Else
			aInfo[ 04 ] := SM0->M0_ENDENT
			aInfo[ 05 ] := SM0->M0_CIDENT
			aInfo[ 06 ] := SM0->M0_ESTENT
			aInfo[ 07 ] := SM0->M0_CEPENT
		EndIf

		aInfo[ 08 ] := SM0->M0_CGC
		aInfo[ 09 ] := SM0->M0_INSC
		aInfo[ 10 ] := SM0->M0_TEL
		aInfo[ 11 ] := SM0->M0_FAX
		aInfo[ 12 ] := SM0->M0_PRODRUR

		IF ( cTipEnd == "C" )
			aInfo[ 13 ] := SM0->M0_BAIRCOB
			aInfo[ 14 ] := SM0->M0_COMPCOB
		Else
			aInfo[ 13 ] := SM0->M0_BAIRENT
			aInfo[ 14 ] := SM0->M0_COMPENT
		EndIf

		aInfo[ 15 ] := If( Type("SM0->M0_CEI") # "U" .And. !Empty( SM0->M0_CEI ), 1, SM0->M0_TPINSC )		
		aInfo[ 16 ] := SM0->M0_CNAE
		aInfo[ 17 ] := SM0->M0_FPAS
		aInfo[ 18 ] := SM0->M0_ACTRAB
		aInfo[ 19 ] := SM0->M0_CODMUN
		aInfo[ 20 ] := SM0->M0_NATJUR
		aInfo[ 21 ] := SM0->M0_DTBASE
		aInfo[ 22 ] := SM0->M0_NUMPROP
		aInfo[ 23 ] := SM0->M0_MODEND
		aInfo[ 24 ] := SM0->M0_MODINSC
		aInfo[ 25 ] := SM0->M0_CAUSA
		aInfo[ 26 ] := SM0->M0_INSCANT
		aInfo[ 27 ] := If( Type("SM0->M0_CEI") # "U" .And. !Empty( SM0->M0_CEI ), SM0->M0_CEI, "" )
		aInfo[ 28 ] := SM0->M0_TPINSC

	Else
		aFill( aInfo , "" )
	EndIf
	
	aInfo1	:= aClone(aInfo)
	
	
	RestArea( aAreaSM0 )
	RestArea( aArea )
Return( lRetorno )

STATIC FUNCTION FTIPOSAL(cTipoSal)
	Local cRet := ""
	Local aTpSal := {}
	dbselectarea("SX3")
	SX3->(DBSETORDER(2))
	
	IF SX3->(DBSEEK("RA_TIPOSAL"))
		aTpSal := STRTOKARR(sx3->x3_cboxspa,";")
		cRet := aTpSal[val(cTipoSal)]
	EndIf
	
RETURN CRET

Static Function fRodape(oPrint, nLinIni)

	Local aLey := {}
	Local i
	Local cTexto    := ""  
	Local cValext   := ""
	Local nLinTotal := 0
	Local aInfo := {}
	
	//����������������������������������������������������������Ŀ
	//�Totais das Verbas Rescisorias                             �
	//������������������������������������������������������������
	
	nlin := nLinIni //2030           
	
	
	IF Len(aPdd) > nNumMax .or. Len(aPdv) > nNumMax .or. Len(aPdpm) > nNumMax
	
		nlin += 35 
		oPrint:say(nlin,1900, oemtoAnsi(STR0049), oFont09n ) 	//--  " Continua ... "
		
	ELSE
	
		nlin += 30 //35
		oPrint:say (nlin,nColIni , STR0050, oFont09n)											//"|30-TOTAL BRUTO"
	    oPrint:say (nlin,0450 , Transform(nProv        ,"@E 999,999,999.99"), oFont09n)
		oPrint:say (nlin,0850 , STR0051, oFont09n)											//"|TOTAL DESCONTOS "
		oPrint:say (nlin,1240 , Transform(nDesc        ,"@E 999,999,999.99"), oFont09n)	
		
		//nlin += 35
		//oPrint:Line(nlin,nColIni , nlin , nColMax)						//-- Linha Horizontal dos Totais das Verbas
		//nlin += 25
		oPrint:say (nlin,1570 , STR0052, oFont09n)											//"|LIQUIDO A RECEBER "
		oPrint:say (nlin,1870 , Transform(nProv - nDesc,"@E 999,999,999.99"), oFont09n)
		
		                     
		cValext := Alltrim(Extenso(nProv - nDesc))
		nLinTotal := MlCount(cValext,100)
	/*/	
		For i:=1 To nLinTotal     
		
			cTexto:= Memoline(cValext,100,i)
			oPrint:say (nlin,0800 ,cTexto, oFont07)
			
			IF nLinTotal > 1 .AND. i <> nLinTotal
				nlin += 25
			ENDIF
			
		Next i
	/*/		
	Endif	                              
	nlin += 30 //50
	  
	aLey := {STR0053 + Transform(nProv - nDesc,"@E 999,999,999.99") + "  " + cValExt + "," }
	
	nlin += 20
	
	For i:=1 to len(aLey)
		oPrint:say( nlin,nColIni+20 ,aLey[i], oFont09)
		nlin += 40
	Next i
	//oPrint:say( nlin,nColIni+20 ,Alltrim(fDescRCC("S091",cCodMsg,1,2,3,25)), oFont09)
	
	fSM0(@aInfo,SRA->RA_FILIAL)

	oPrint:say( nlin,nColIni+20 ,STR0042 + AllTrim(aInfo[1]) + STR0043, oFont09)
	nlin += 40
	//oPrint:say( nlin,nColIni+20 ,STR0044, oFont09)
	//nlin += 30
	oPrint:say( nlin,nColIni+20 ,AllTrim(cCodMsg), oFont09)
	nlin += 90//80
	oPrint:say( nlin,nColIni+20 ,"_____________________________", oFont09n)	
	oPrint:say( nlin,850  ,"_____________________________", oFont09n)	
	
	nLin+= 50
	oPrint:say( nlin,nColIni+20 ,SRA->RA_NOME, oFont09n)	
	oPrint:say( nlin,850 ,cNomRep, oFont09n)
	
	nlin += 180//350
	oPrint:say( nlin,nColIni+20 ,"_____________________________", oFont09n)	
	oPrint:say( nlin,850  ,"_____________________________", oFont09n)	
	oPrint:say( nlin,1510 ,"_____________________________", oFont09n)	
	nlin += 40
	oPrint:say( nlin,330 ,STR0054, oFont09n)	
	oPrint:say( nlin,1030 ,STR0055, oFont09n)	
	oPrint:say( nlin,1650 ,STR0056, oFont09n)	 
	
	//����������������������������������������������������������������������������Ŀ
	//�FIM DO RELATORIO                                                            �
	//������������������������������������������������������������������������������
	oPrint:EndPage()
	CONTFL:=1
	
Return 

Static Function fLoadLogo(cNomeArq)

	Local cStartPath := GetClientDir() //GetSrvProfString("Startpath","") 

	If Substr(cStartPath, Len(cStartPath), 1) == "\"
		cImagem := cStartPath + cNomeArq        
	Else
		cImagem := cStartPath + "\" + cNomeArq    
	Endif

Return cImagem
