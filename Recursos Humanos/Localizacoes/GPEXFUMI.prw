#include 'protheus.ch'
#include 'GPEXFUMI.ch'

/*
���������������������������������������������������������������������������������
���������������������������������������������������������������������������������
�����������������������������������������������������������������������������Ŀ��
���Programa  �GPEXFUMI  �Autor  �Luis E. Enr�quez Mata  �  Data �  21/11/19   ���
�����������������������������������������������������������������������������Ĵ��
���Desc.     � Funciones gen�ricas GPE Mercado Internacional.                 ���
�����������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.            	  ���
�����������������������������������������������������������������������������Ĵ��
���Programador  �Fecha   �   Issue   � Motivo de la alteraci�n                ���
���Luis Enriquez�21/11/19�DMINA-7532 �Creaci�n de funciones para carga de Ti- ���
���             �        �           �pos de ausencia (MEX)                   ���
�����������������������������������������������������������������������������Ĵ��
������������������������������������������������������������������������������ٱ�
���������������������������������������������������������������������������������
���������������������������������������������������������������������������������
*/

/*/{Protheus.doc} GPXCARRCM
Ejecuci�n de funci�n GPXRCM est�ndar por pa�s.
@type function
@author luis.enr�quez
@since 21/11/2019
@version 1.0
@return Nil
/*/
Function GPXCARRCM()
	Local cFunction	:= ("GPXRCM" + cPaisLoc)
	Private aRCMEnc := {}
	Private aRCMDet := {}	
	
	If FindFunction(cFunction)
		bFunc := __ExecMacro("{ ||  " + cFunction + "() }")
		Eval(bFunc)
	EndIf
Return Nil

/*/{Protheus.doc} GPXRCMMEX
Ejecuci�n de regla de negocio para carga de tabla Tipos de Ausencia (RCM) para 
el pa�s M�xico.
@type function
@author luis.enr�quez
@since 21/11/2019
@version 1.0
@return Nil
/*/
Function GPXRCMMEX()
	Local aRCMCab  := {}
	Local aRCMDet  := {}
	Local cFunGPPD := "GPPD" + Alltrim(cPaisLoc)
	Local nTotReg  := 0
	Private aGPError  := {}
	Private nDatosErr := 0
		
	If !ChkVazio("RCM",.F.)
		If !ChkVazio("SRV",.F.)
			cMsgYesNo	:= OemToAnsi(;
								STR0001 + ;	//"No existen registros de Conceptos (SRV)"
								CRLF	+ ;	
								StrTran( STR0002, "###", STR0005 ) + ;	//"�Desea generar los ### est�ndar?" //"Conceptos"
								CRLF	+ ;
								STR0003  + cPaisLoc + STR0004 ;	//"Importante: Es necesario que el programa GPPD" //".PRX este compilado."
	                            )
			If MsgYesNo(OemToAnsi(cMsgYesNo) , OemToAnsi(STR0005)) //"Conceptos"
				If FindFunction("fCarPD")
					If FindFunction(cFunGPPD)
						Processa( { || fCarPD() } , OemToAnsi(StrTran( STR0006, "###", STR0005 )) ) //"Cargando los ### est�ndar..." //"Conceptos"
					Else
						MsgAlert(STR0007 + cFunGPPD + STR0008, OemToAnsi(STR0009)) //El programa " //".PRX no esta compilado en el repositorio." //"Atenci�n"						
					EndIf
				EndIf
		    EndIf                            
		EndIf
		
		If !ChkVazio("SRV",.F.)
			 MsgAlert(STR0010, OemToAnsi(STR0009)) //"No se encuentran cargados los Conceptos (SRV), datos requeridos para continuar con el proceso." //"Atenci�n"		
		Else			
				aRCMCab   := {"RCM_TIPO","RCM_DESCRI","RCM_PD","RCM_TPIMSS","RCM_TIPODI","RCM_TIPOAF","RCM_DIASEM","RCM_SOBREP","RCM_7MODIA","RCM_ABATAV"}
				
				aAdd(aRCMDet,{"102"	,"Incapacidad AT"               ,"102", "2", "1", "1", 999, "1", "2", "2", .F., ""})
				aAdd(aRCMDet,{"103"	,"Incapacidad EG"               ,"103", "2", "2", "1", 999, "1", "2", "2", .F., ""})
				aAdd(aRCMDet,{"104"	,"Incapacidad MT"               ,"104", "2", "2", "1", 999, "1", "2", "2", .F., ""})
				aAdd(aRCMDet,{"105"	,"Faltas Injustificadas"        ,"105", "1", "1", "1", 999, "2", "2", "2", .F., ""})
				aAdd(aRCMDet,{"106"	,"Castigo"	                    ,"106", "3", "1", "1", 999, "2", "1", "2", .F., ""})
				aAdd(aRCMDet,{"107"	,"Permiso con Goce de Sueldo" 	,"107", "3", "1", "2", 999, "2", "2", "2", .F., ""})
				aAdd(aRCMDet,{"108"	,"Permiso sin Goce de Sueldo"	,"108", "3", "1", "1", 999, "2", "2", "2", .F., ""})
				aAdd(aRCMDet,{"109"	,"Prestacion Empresa"	 		,"109", "3", "1", "2", 999, "2", "1", "2", .F., ""})
				aAdd(aRCMDet,{"110"	,"Incapacidad Empresa"			,"110", "3", "1", "2", 999, "2", "1", "2", .F., ""})
				aAdd(aRCMDet,{"111"	,"Comision Sindical"			,"111", "3", "1", "2", 999, "2", "1", "2", .F., ""})
				aAdd(aRCMDet,{"112"	,"Dias Economicos"			    ,"112", "3", "1", "2", 999, "2", "1", "2", .F., ""})
				aAdd(aRCMDet,{"113"	,"Vacaciones"	 			    ,"113", "3", "1", "4", 999, "2", "1", "2", .F., ""})
				aAdd(aRCMDet,{"119"	,"Permiso Paternidad"		    ,"119", "3", "1", "2", 999, "2", "2", "2", .F., ""})
				
				If MsgYesNo(OemToAnsi(STR0025 + ;       //"No existen registros de Tipos de Ausencia (RCM)" 
				  CRLF + StrTran( STR0002, "###", STR0011 )) , OemToAnsi(STR0011)) //"�Desea generar los ### est�ndar?" //"Tipos de Ausencia"
					nTotReg := Len(aRCMDet)
					
					Processa( { || fCarRCM(aRCMCab, aRCMDet, @nDatosErr)} , OemToAnsi(StrTran(STR0006, "###", STR0011)) ) //"Cargando los ### est�ndar..." //"Tipos de Ausencia"
					
					If MsgYesNo(OemToAnsi(IIf(nDatosErr > 0, STR0012, STR0013) + ; //"Proceso ejecutado con errores" //"Proceso ejecutado con �xito"
						CRLF + StrTran(STR0014, "###", STR0011)) , OemToAnsi(STR0011)) //"�Desea generar los ### est�ndar?" //"Tipos de Ausencia"
						GPXGENLOG(aRCMDet, STR0011, nDatosErr) //"Tipos de Ausencia"
					EndIf
				EndIf
		EndIf	
	EndIf
Return Nil

/*/{Protheus.doc} fCarRCM
Llenado de tabla Tipos de Ausencia (RCM)
@type function
@author luis.enr�quez
@since 21/11/2019
@version 1.0
@param aRCMCab, array, Arreglo con nombre de campos de la tabla RCM.
@param aRCMDet, array, Arreglo con los valores para los campos de la tabla RCM.
@param nDatosErr, numerico, Contador con n�mero de registros que tuvieron error al insertar en la tabla RCM.
@return Nil
/*/
Function fCarRCM(aRCMCab, aRCMDet, nDatosErr)
	Local nI := 0
	Local nY := 0
	Local cFilRCM := xFilial("RCM")
	Local cFilSRV := xFilial("SRV")
	Local lError  := .F.
	
	ProcRegua(Len(aRCMDet))
	
	BEGIN TRANSACTION
		For nI := 1 to Len(aRCMDet)
			IncProc(Alltrim(aRCMDet[nI,1]) + Alltrim(aRCMDet[nI,2]))
			RCM->(dbSetOrder(1)) //RCM_FILIAL + RCM_TIPO
			If !RCM->( dbSeek( cFilRCM + aRCMDet[nI,1]))
				RCM->(RecLock("RCM" , .T.))
				RCM->RCM_FILIAL := cFilRCM
				For nY := 1 to Len(aRCMCab)
					lError  := .F.
					If aRCMCab[nY] == "RCM_PD"
						dbSelectArea("SRV")
						SRV->(dbSetOrder(1)) //RV_FILIAL + RV_COD
						If SRV->(dbSeek(cFilSRV + aRCMDet[nI,nY]))
							RCM->(&(aRCMCab[nY])) := aRCMDet[nI,nY]
						Else
							aRCMDet[nI,12] += STR0015 + Alltrim(aRCMDet[nI,3]) + STR0016 //"El c�digo del Concepto " //" para el tipo de ausencia no existe."
							lError := .T. 
						EndIf
					Else
						RCM->(&(aRCMCab[nY])) := aRCMDet[nI,nY]
					EndIf
					If lError
						aRCMDet[nI,11] := .T. 
						nDatosErr += 1
					EndIf
				Next nY
				RCM->(MsUnLock())  
			EndIf
		Next nI
			
		If nDatosErr > 0
			DisarmTransaction()
		EndIf		
	END TRANSACTION
Return Nil

/*/{Protheus.doc} GPXGENLOG
Generaci�n de la impresi�n del LOG del proceso.
@type function
@author luis.enr�quez
@since 21/11/2019
@version 1.0
@param aProcesa, array, Arreglo con los registros procesados.
@param cTipo, string, Nombre de los datos procesados (Ej. "Conceptos", "Tipos de Ausencia")
@param nDatosErr, numerico, Contador con n�mero de registros que tuvieron error al insertar en la tabla RCM.
@return Nil
/*/
Static function GPXGENLOG(aProcesa, cTipo, nDatosErr)
	Local aReturn	:= {"xxxx", 1, "yyy", 2, 2, 1, "",1 }
	Local cTamanho	:= "M"
	Local cTitulo	:= STR0017 + cTipo //"LOG generaci�n de "
	Local nX		:= 1
	Local aNewLog	:= {}
	Local nTamLog	:= 0
	Local aLogTitle	:= { STR0018, STR0019 }   //"Tipo Descripci�n                  Observaciones" //"Resumen del proceso"
	Local aLog		:= {}
	Local cEsp		:= Chr(9) + Chr(9)+ Chr(9)
	Local aLogRes	:= {}
	Local cObs      := ""
	Local nPos 		:= 1
	Local nC		:= 0
	Local nError    := 0
	
	Private wCabec1 := STR0020 + cTipo  //"Datos procesados: "
	
	ASORT(aProcesa, , , { | x,y | x[1] + x[2]  > y[1] + y[2] } )
	
	For nX :=1 To Len(aProcesa)                 
		cObs := ""
		nPos := 1
		If Len(aProcesa[nX,12]) > 84
			For nC:= 1 to (Len(aProcesa[nX,12]) / 84) + 1				 
				cObs += SubStr(aProcesa[nX,12], nPos, 84)  + (Chr(13) + Chr(10)) + Space(40)
				nPos += 84
			Next nC			
		Else
			cObs := aProcesa[nX,12]
		EndIF
		                           
	    aAdd(aLog, aProcesa[nx,1] + Space(2) + ;   //Tipo
	    		   aProcesa[nX,2] + Space(16) + ;  //Descripci�n
	    		   cObs )                         // Detalle	 	    		 
	Next nX
		
	aAdd(aLogRes," ")
	If nDatosErr > 0
		aadd(aLogRes,STR0021 + Transform(Len(aProcesa),"9999"))    //"Registros V�lidos:"
		aadd(aLogRes,STR0022 + Transform(nDatosErr,"9999"))		//"Registros Err�neos:"
	Else
		aadd(aLogRes,STR0023 + Transform(Len(aProcesa),"9999"))  //"Registros Generados:"
	EndIf

	aNewLog		:= aClone(aLog)
	nTamLog		:= Len( aLog)	
	aLog := {}
	
	If !Empty( aNewLog )	
		aAdd( aLog , aClone( aNewLog ) )
	Endif
	
	aAdd(aLog, aLogRes)
	/*
		1 -	aLogFile 	//Array que contem os Detalhes de Ocorrencia de Log
		2 -	aLogTitle	//Array que contem os Titulos de Acordo com as Ocorrencias
		3 -	cPerg		//Pergunte a Ser Listado
		4 -	lShowLog	//Se Havera "Display" de Tela
		5 -	cLogName	//Nome Alternativo do Log
		6 -	cTitulo		//Titulo Alternativo do Log
		7 -	cTamanho	//Tamanho Vertical do Relatorio de Log ("P","M","G")
		8 -	cLandPort	//Orientacao do Relatorio ("P" Retrato ou "L" Paisagem )
		9 -	aRet		//Array com a Mesma Estrutura do aReturn
		10-	lAddOldLog	//Se deve Manter ( Adicionar ) no Novo Log o Log Anterior
	*/
	Processa( { ||fMakeLog( aLog ,aLogTitle , , .t. , FunName() , cTitulo , cTamanho , "P" , aReturn, .F. )}, STR0024 + cTipo) //"Generando Log de creaci�n de "	
Return Nil