#INCLUDE "PROTHEUS.CH"                        
#INCLUDE "RWMAKE.CH"  
#INCLUDE "TOPCONN.CH"  
#INCLUDE "report.ch"  
#INCLUDE "GPER808.CH"          

/*
���������������������������������������������������������������������������������������������
���������������������������������������������������������������������������������������������
�����������������������������������������������������������������������������������������Ŀ��
���Fun��o    � GPER808  � Autor � Alceu Pereira         � Data � 09.03.10	              ���
�����������������������������������������������������������������������������������������Ĵ��
���Descri��o � Certificado de Rentas y Retenciones Por Rentas De Quinta Categoria         ���
�����������������������������������������������������������������������������������������Ĵ��
���Sintaxe   � GPER808()                                                	              ���
�����������������������������������������������������������������������������������������Ĵ��
���Parametros�                                                                            ���
�����������������������������������������������������������������������������������������Ĵ��
��� Uso      � Generico - PERU                                              	          ���
�����������������������������������������������������������������������������������������Ĵ��
���             ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.                         ���
�����������������������������������������������������������������������������������������Ĵ��
���Programador � Data   � FNC       �  Motivo da Alteracao 		                          ���
�����������������������������������������������������������������������������������������Ĵ��
���Alceu P.    �28/06/10�00000014522/2011�Ajuste na impress�o dos valores do relatorio.   ���
���Leandro Dr. �23/04/12�          TEV502�Ajuste na impress�o dos valores do relatorio.   ���
������������������������������������������������������������������������������������������ٱ�
���������������������������������������������������������������������������������������������
*/

Function GPER808()                                

//���������������������������������������������������������������������Ŀ
//� Declaracao de Variaveis                                             �
//�����������������������������������������������������������������������

Local cDesc1        := STR0001
Local cDesc2        := STR0002
Local cDesc3        := ""
Local titulo        := "Certificado de Rentas y Retenciones"
Local nLin          := 80

Local Cabec1        := ""
Local Cabec2        := ""
Local imprime       := .T.
Local aOrd   		:= {STR0006,STR0007,STR0008} //"Matricula"###"C.Custo"###"Nome"
Local cPerg 		:=	"GPER808"
Private lEnd        := .F.
Private lAbortPrint := .F.
Private limite      := 80
Private tamanho     := "P"       
Private nomeprog    := "GPER808" 
Private nTipo       := 18
Private aReturn  := { 'STR0003',1,'STR0004',2,2,1,"",1 }   //Zebrado - Administracao

Private nLastKey    := 0
Private cbcont      := 00
Private CONTFL      := 01
Private m_pag       := 01
Private wnrel       := "GPER808" 
Private cString 	:= "SRA"   

Private cPict1    := "9999999.999"
Private nNomEmp   := ""			//Nome da Empresa 
Private cCidade   := ""			//Cidade da Empresa 
Private cCgc 	  := ""			//CGC da Empresa 	

Private nValPuest := 0          // Valor de Puesto a disp. Total recebido que incide IR e seja Provento  
Private nValDeven := 0			//Valor de Devengados Total recebido que NAO incide IR e seja Provento
Private nValDedAn := 0			//Valor do Id de calculo numero 1155
Private nValBaseC := 0          //Base de Calculo de Renta de Quinta Categoria
Private nValIRRet := 0 			//Imposto de renda anual  retido ID de calculo 066 

Private cFilialAnt := ""
Private aPerAberto	:= {}
Private aPerFechado	:= {}
Private aPerTodos	:= {}
Private nOrdem 
Private aCodFol   := {}
Private aInfo  	  := {}            
Private aVerbasAcum := {}
Private cDesCargo		:= Space(7)
Private cNumPgtIni	:= Space( GetSx3Cache("RD_SEMANA", "X3_TAMANHO") )
Private cNumPgtFim	:= Replicate( "9", Len( cNumPgtIni ) )

nOrdem    := aReturn[8]
dbSelectArea("SRA")

pergunte(cPerg,.F.)  
                                    
If nLastKey == 27
   Return
Endif

wnrel := SetPrint(cString,NomeProg,cPerg,@titulo,cDesc1,cDesc2,cDesc3,.T.,aOrd,.T.,tamanho,,.T.)   

If nLastKey == 27
   Return
Endif       

SetDefault(aReturn,cString)  

/*
��������������������������������������������������������������Ŀ
� Variaveis utilizadas para parametros                         �
� mv_par01        //  Ano?      					           � 
� mv_par02        //  Filial De?					           �
� mv_par03        //  Filial Ate?                              |
� mv_par04        //  Matricula De?                            �
� mv_par05        //  Matricula Ate?                           �
� mv_par06        //  Nome De?                      		   �
� mv_par07        //  Nome Ate?                     		   �
� mv_par08        //  Centro de Custo De?                      �
� mv_par09        //  Centro de Custo Ate?                     �
� mv_par10        //  Situacoes a Imp.?                        �
� mv_par11        //  Categorias a Imp.?                       �
����������������������������������������������������������������
��������������������������������������������������������������Ŀ
� Carregando variaveis mv_par?? para Variaveis do Sistema.     �
����������������������������������������������������������������*/

cAno		:= mv_par01
cFilDe      := mv_par02 
cFilAte     := mv_par03
cMatDe      := mv_par04
cMatAte     := mv_par05
cNomeDe		:= mv_par06
cNomeAte	:= mv_par07
cCustoDe    := mv_par08
cCustoAte   := mv_par09
cSit        := mv_par10
cCat        := mv_par11

//����������������������������������������������������
//�Define a ordem do Index que ser� usada pelo o SRA.�
//����������������������������������������������������
If nOrdem == 1
	dbSetOrder(1)
	dbSeek(cFilDe + cMatDe,.T.)
	cInicio  := "SRA->RA_FILIAL + SRA->RA_MAT"
	cFim     := cFilAte + cMatAte
ElseIf nOrdem == 2
	dbSetOrder(2)
	dbSeek(cFilDe + cCustoDe + cMatDe,.T.)
	cInicio  := "SRA->RA_FILIAL + SRA->RA_CC + SRA->RA_MAT"
	cFim     := cFilAte + cCustoAte + cMatAte
ElseIf nOrdem == 3
	dbSetOrder(3)
	dbSeek(cFilDe + cNomeDe + cMatDe,.T.)
	cInicio  := "SRA->RA_FILIAL + SRA->RA_NOME + SRA->RA_MAT"
	cFim     := cFilAte + cNomeAte + cMatAte
Endif

RptStatus({|| RunReport(Cabec1,Cabec2,Titulo,nLin) },Titulo)
Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Fun��o    �RUNREPORT � Autor � Alceu Pereira      � Data �  09/03/10   ���
�������������������������������������������������������������������������͹��
���Descri��o � Funcao auxiliar chamada pela RPTSTATUS. A funcao RPTSTATUS ���
���          � monta a janela com a regua de processamento.               ���
�������������������������������������������������������������������������͹��
���Uso       � Programa principal                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

Static Function RunReport(Cabec1,Cabec2,Titulo,nLin)   

Fp_CodFol(@aCodFol,SRA->RA_FILIAL)

SetRegua(SRA->(RecCount()))
cFilAnt	:= SRA->RA_FILIAL

dbGoTop()
While !("SRA")->( Eof() )
				
If nLastKey == 27
   Return
Endif

If lAbortPrint
	Exit
Endif
	
If SRA->RA_FILIAL < cFilDe .Or. SRA->RA_FILIAL > cFilAte
	SRA->(dbSkip())
	Loop
Endif    
	
If (SRA->RA_NOME < cNomeDe) .Or. (SRA->RA_NOME > cNomeAte) .Or. ;
	(SRA->RA_MAT < cMatDe) .Or. (SRA->RA_MAT > cMatAte)  .Or. ;
	(SRA->RA_CC < cCustoDe)   .Or. (SRA->RA_CC > cCustoAte)
	SRA->(dbSkip())
	Loop
EndIf
	
fLimpaVarRel()	   // Limpa vari�veis  utilizadas no corpo do relat�rio     

cFilialAnt := xFilial('RCH',SRA->RA_FILIAL)   

RetPerAno(cFilialAnt, @aPerAberto, @aPerFechado, @aPerTodos,cAno) 
		
If Len(aPerFechado) < 1
	cFilialAnt:= "  "
	RetPerAno(cFilialAnt, @aPerAberto, @aPerFechado, @aPerTodos, cAno)
Endif

fBuscaDadosRel()  // Alimeta vari�veis utilizadas no corpo do relat�rio

fSomaValores()

If lAbortPrint
	@nLin,00 PSAY STR0005
    Exit
Endif

Cabec(Titulo,Cabec1,Cabec2,NomeProg,tamanho,nTipo)

	nLin := 6
    
	@nLin,06 PSAY "CERTIFICADO DE RENTAS Y RETENCIONES POR RENTAS DE QUINTA CATEGOR�A"
 	
	nLin ++     

	@nLin,06 PSAY "__________________________________________________________________"  

	nLin := nLin + 3    

	@nLin,02 PSAY "Texto �nico Ordenado de la ley del Impuesto a la Renta, aprobado por Decreto"
		
	nLin ++     	 	
	             
	@nLin,02 PSAY "Supremo No 179-2004/EF, Art. 34, D.S No. 122-94-EF, Art. 45 y Reso�uci�n de "
	
	nLin ++     	 	
	             
	@nLin,02 PSAY "Superitendencia 10-2006/SUNAT."

	nLin := nLin + 3

	@nLin,02 PSAY "EXERCICIO GRAVABLE" + Space(1) + cAno
	
	nLin := nLin + 3

	@nLin,02 PSAY STR0009 +".: " + AllTrim(nNomEmp) + Space(2) +  "RUC.:" + Space(1)+ AllTrim(cCgc) 		
	
	nLin := nLin + 3

	@nLin,02 PSAY "Certifica:"

	nLin := nLin + 2
	        
	@nLin,02 PSAY "Que el se�or(a)"+ Space(1) +  SRA->RA_NOME + Space(1) + "identificado"  
	 
	nLin ++
	        
	@nLin,02 PSAY "con DNI Nro."+ Space(1) + SRA->RA_CIC + " , y que se ha desempe�ado como"

	nLin ++
	
	@nLin,02 PSAY "CARGO "+cDesCargo+Space(3)+"se le ha retenido la"

	nLin ++        
	
	@nLin,02 PSAY "suma de S/."+StrTran(AllTrim(cValToChar(Trans(nValIRRet,cPict1))),",",".")+ Space(1)+ "por el Impuesto a la Renta correspondiente" 
	
	nLin ++        	      
	
	@nLin,02 PSAY "al Ejercicio Gravable "+cAno+". Dicha retenci�n se ha determinado" 
	
	nLin ++
	
	@nLin,02 PSAY "sobre la base de las rentas que se detalhan a continuaci�n:"	

	nLin := nLin + 2 
	        
	@nLin,02 PSAY "INGRESSOS BRUTOS TOTALES:

	nLin := nLin + 2 	
	
	@nLin,02 PSAY "Sueldos, gratificaciones y otros ingressos gravados"
	
	nLin ++
		
	@nLin,02 PSAY  "*"+REPLICATE("-",39)
	
	@nLin,39 PSAY "*"
	
	@nLin,38 PSAY REPLICATE("-",14)+Space(4)+"*"   
	
	@nLin,52 PSAY  REPLICATE("-",20)+"*" 

	nLin ++	     
	
	@nLin,02 PSAY "|" + "Puesto a disposici�n "+cAno+" (total)" 
	
	@nLin,38 PSAY "|" 
	
	@nLin,55 PSAY "|" + Space(1) + "S/."+StrTran(AllTrim(cValToChar(Trans(nValPuest,cPict1))),",",".") 
	
	@nLin,71 PSAY "|" 
	
	nLin ++
	
	@nLin,02 PSAY "|"     
	
	@nLin,38 PSAY "|" 
	
	@nLin,55 PSAY "|" 
	
	@nLin,71 PSAY "|" 
	                       
	nLin ++                                                                 
                                             
	@nLin,02 PSAY  "*"+REPLICATE("-",39)
	
	@nLin,39 PSAY "*"
	
	@nLin,38 PSAY REPLICATE("-",14)+Space(4)+"*"   
	
	@nLin,52 PSAY  REPLICATE("-",20)+"*" 

	nLin ++
	
	@nLin,02 PSAY "|" + "Devengados en el periodo, no" 
	
	@nLin,38 PSAY "|" + Space(1) + "S/."+StrTran(AllTrim(cValToChar(Trans(nValDeven,cPict1))),",",".") 

	@nLin,55 PSAY "|" 
	
	@nLin,71 PSAY "|"
	
	nLin ++          
	
	@nLin,02 PSAY "|" + "puestos a disposici�n " 
	
	@nLin,38 PSAY "|" 
	
	@nLin,55 PSAY "|" 
	
	@nLin,71 PSAY "|"
	                 
	nLin ++          	                                                       

	@nLin,02 PSAY  "*"+REPLICATE("-",39)
	
	@nLin,39 PSAY "*"
	
	@nLin,38 PSAY REPLICATE("-",14)+Space(4)+"*"   
	
	@nLin,52 PSAY  REPLICATE("-",20)+"*" 
	
	nLin ++                                                                 		
	
	@nLin,02 PSAY "|" + "Deducible anual (7 UIT)" 
	
	@nLin,38 PSAY "|" 

	@nLin,55 PSAY "|" + Space(1) + "S/.("+StrTran(AllTrim(cValToChar(Trans(nValDedAn,cPict1))),",",".")+")" 	
	
	@nLin,71 PSAY "|"
	                    
	nLin ++          	                                                       

	@nLin,02 PSAY  "*"+REPLICATE("-",39)
	
	@nLin,39 PSAY "*"
	
	@nLin,38 PSAY REPLICATE("-",14)+Space(4)+"*"   
	
	@nLin,52 PSAY  REPLICATE("-",20)+"*" 
	                                                  
	nLin ++                                                                 		
	
	@nLin,02 PSAY "|" + "Base de C�lculo Renta de Quinta" 
	
	@nLin,38 PSAY "|" 
	
	@nLin,55 PSAY "|" + Space(1) + "S/."+StrTran(AllTrim(cValToChar(Trans(nValBaseC,cPict1))),",",".")	 
	
	@nLin,71 PSAY "|" 
	
	nLin ++                                                                 		
	
	@nLin,02 PSAY "|" + "Categoria" 
	
	@nLin,38 PSAY "|" 
	
	@nLin,55 PSAY "|" 
	
	@nLin,71 PSAY "|"
		
	nLin ++          	                                                       

	@nLin,02 PSAY  "*"+REPLICATE("-",39)
	
	@nLin,39 PSAY "*"
	
	@nLin,38 PSAY REPLICATE("-",14)+Space(4)+"*"   
	
	@nLin,52 PSAY  REPLICATE("-",20)+"*" 
	
	nLin ++  

	@nLin,02 PSAY "|" + "Impuesto a La Renta Anual" 
	
	@nLin,38 PSAY "|" 
	
	@nLin,55 PSAY "|" + Space(1) + "S/."+StrTran(AllTrim(cValToChar(Trans(nValIRRet,cPict1))),",",".") 	 
	
	@nLin,71 PSAY "|" 
	
	nLin ++              
	
	@nLin,02 PSAY "|" + "Retenido Seg�n Escala S/." 
	
	@nLin,38 PSAY "|" 
	
	@nLin,55 PSAY "|" 
	
	@nLin,71 PSAY "|"
	
	nLin ++           	                                                       

	@nLin,02 PSAY  "*"+REPLICATE("-",39)
	
	@nLin,39 PSAY "*"
	
	@nLin,38 PSAY REPLICATE("-",14)+Space(4)+"*"   
	
	@nLin,52 PSAY  REPLICATE("-",20)+"*" 
	
	nLin := nLin + 3 
	
	@nLin,02 PSAY AllTrim(cCidade) + ","+ cValToChar(Day(dDataBase))+ Space(1) + "de" + Space(1) + AllTrim(MesExtenso(dDatabase)) + Space(1) + "del" + Space(1) + AllTrim(cAno)+"."
	
	nLin := nLin + 4 	
	
	@nLin,02 PSAY "___________________________________"   
	
	nLin ++
	
	@nLin,02 PSAY "Firma Representante Legal"
	
	nLin := nLin + 4 
		
	@nLin,02 PSAY "___________________________________"   
	
	nLin ++
	
	@nLin,02 PSAY "Firma del Trabalhador"
	
   	IMPR("","P")
   	
   	nLin := 0

   	SRA->(dbSkip())
	Loop
EndDo

SET DEVICE TO SCREEN   

If aReturn[5]==1
   SET PRINTER TO
   dbCommitAll()
   OurSpool(wnrel)
Endif

MS_FLUSH()

Return

/*
�����������������������������������������������������������������������������������
�����������������������������������������������������������������������������������
�������������������������������������������������������������������������������Ŀ��
���Fun��o    � fBuscaDadosRel� Autor � Alceu Pereira         � Data � 10.02.10  ���
�������������������������������������������������������������������������������Ĵ��
���Descri��o � Emissao de Liquidacao por pago de utilidades                     ���
�������������������������������������������������������������������������������Ĵ��
���Sintaxe   � fBuscaDadosRel()                                            	    ���
�������������������������������������������������������������������������������Ĵ��
���Parametros�                                                                  ���
�������������������������������������������������������������������������������Ĵ��
��� Uso      � Generico - PERU                                                  ���
��������������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������������
*/
Static Function fBuscaDadosRel()
Local cFuncao 	:= SRA->RA_CODFUNC
Local cCargo	:= SRA->RA_CARGO 
Local cCodCargo	:= Iif(Empty(cCargo),fDesc("SRJ",cFuncao,"RJ_CARGO"),cCargo) 

cProcesso := SRA->RA_PROCES 

//Busca dados da empresa 
fInfo(@aInfo,Sra->Ra_Filial)
nNomEmp  := aInfo[3]  
cCidade  := aInfo[05]   
cCgc     := aInfo[8]
//

//Busca descricao do cargo
cDesCargo	:= If(Empty(cDesCargo),fDesc("SQ3",cCodCargo+space(05),"Q3_DESCSUM"),cDesCargo) 
//

Return

/*                                           
�����������������������������������������������������������������������������������
�����������������������������������������������������������������������������������
�������������������������������������������������������������������������������Ŀ��
���Fun��o    � fLimpaVarRel  � Autor � Alceu Pereira         � Data � 10.02.10  ���
�������������������������������������������������������������������������������Ĵ��
���Descri��o � Limpa vari�veis usadas no While do SRA                           ���
�������������������������������������������������������������������������������Ĵ��
���Sintaxe   � fLimpaVarRel()                                            	    ���
�������������������������������������������������������������������������������Ĵ��
���Parametros�                                                                  ���
�������������������������������������������������������������������������������Ĵ��
��� Uso      � Generico - PERU                                                  ���
��������������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������������
*/

Static Function fLimpaVarRel()

nValPuest := 0          // Valor de Puesto a disp. Total recebido que incide IR e seja Provento  
nValDeven := 0			//Valor de Devengados Total recebido que NAO incide IR e seja Provento
nValDedAn := 0			//Valor do Id de calculo numero 1155
nValBaseC := 0          //Base de Calculo de Renta de Quinta Categoria
nValIRRet := 0 			//Imposto de renda anual  retido ID de calculo 066 

aVerbasAcum := aPerAberto:= aPerFechado:= aPerTodos := {}

Return

/*                                           
�����������������������������������������������������������������������������������
�����������������������������������������������������������������������������������
�������������������������������������������������������������������������������Ŀ��
���Fun��o    � fSomaValores  � Autor � Alceu Pereira         � Data � 10.02.10  ���
�������������������������������������������������������������������������������Ĵ��
���Descri��o � Soma os valores									                ���
�������������������������������������������������������������������������������Ĵ��
���Sintaxe   � fSomaValores()                                            	    ���
�������������������������������������������������������������������������������Ĵ��
���Parametros�                                                                  ���
�������������������������������������������������������������������������������Ĵ��
��� Uso      � Generico - PERU                                                  ���
��������������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������������
*/
Static Function fSomaValores()  

Local nCont := 0 
                                                        
If (len(aVerbasAcum)>=1)                                       
	For nCont:= 1 to len(aVerbasAcum)
   		If PosSrv( aVerbasAcum[nCont,1], SRA->RA_FILIAL, "RV_TIPOCOD" ) $ "1" .AND. PosSrv( aVerbasAcum[nCont,1], SRA->RA_FILIAL, "RV_IR" ) $ "1*3"			
			nValPuest += NoRound(aVerbasAcum[nCont,3])
   		ElseIf PosSrv( aVerbasAcum[nCont,1], SRA->RA_FILIAL, "RV_TIPOCOD" ) $ "1" .AND. PosSrv( aVerbasAcum[nCont,1], SRA->RA_FILIAL, "RV_IR" ) $ "2*4"
 			nValDeven += NoRound(aVerbasAcum[nCont,3])
		Elseif aVerbasAcum[nCont,1] == fGetCodFol("1155")     
   			nValDedAn += NoRound(aVerbasAcum[nCont,3])
		Elseif aVerbasAcum[nCont,1] == fGetCodFol("0015")        		
			nValBaseC += NoRound(aVerbasAcum[nCont,3])		     		
		ElseIf aVerbasAcum[nCont,1] == fGetCodFol("0066")
			nValIRRet  += NoRound(aVerbasAcum[nCont,3])		  			
   		Endif
	Next
Endif

Return  

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RetPerAno �Autor  �Alceu Pereira       � Data �  23/02/08   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function RetPerAno(cFilAux, aPerAberto, aPerFechado, aPerTodos, cAnoPar)

Local nCont                                                          
Local nAux                                                                
Local cMes  := "01"
Local aPerFe := {}
Local aPerAb := {}

For nCont := 1 to 13                                              

	fRetPerComp( cMes , cAnoPar , cFilAux, SRA->RA_PROCES , , @aPerAb , @aPerFe , @aPerTodos )  
       
	//guarda o conteudo de aPerFech, pois a cada chamada a funcao fRetPerComp zera os arrays   
	If !len(aPerFe) < 1
		For nAux:= 1 to len(aPerFe)
			aAdd(aPerFechado, aPerFe[nAux])             
	 	Next nAux
	Endif 
	
	cMes := StrZero(Val(cMes) + 1,2)
	
Next nCont   
 
If Len(aPerFechado) >= 1 

  cProcesso := SRA->RA_PROCES 

	If len(aPerFechado) >= 1
		aVerbasAcum := fBuscaAcmPer(,,,,,aPerFechado[1][1],aPerFechado[len(aPerFechado)][1],cNumPgtIni,cNumPgtFim,,.T.)
	Endif

Endif

Return 
   
