#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"

/*
�����������������������������������������������������������������������������                                                     
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �GPER882   �Autor  �Orbitas Comerciales  �Fecha �  24/11/2017���
�������������������������������������������������������������������������͹��
���Desc.     � CERTIFICADO DE PARTICIPACION  DE UTILIDADES                ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � PERU                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Function GPER882()
Private oReport
  

	If TRepInUse()

		Pergunte("GPER882",.t.)

		oReport := ReportDef()
		oReport:PrintDialog()
	EndIf

Return

Static Function ReportDef()
	Local oReport
	Local oSection1
	Local oSection2
	Local oSection3
	Local oSection4
	
	
	/*
	��������������������������������������������������������������Ŀ
	� Define Variaveis Private(Basicas)                            �
	����������������������������������������������������������������*/
		
	
	/*
	��������������������������������������������������������������Ŀ
	� Variaveis Utilizadas na funcao IMPR                          �
	����������������������������������������������������������������*/
	Private cProcedi 	:= ""
	
	Private cMes 	  	:= ""
	Private cAno 	  	:= ""
Private cOrdem 		:= ""
Private cSemana		:= ""
	
	
	
	
	 
	 
	
	
	/*
��������������������������������������������������������������Ŀ
� Variaveis utilizadas para parametros                         �
� mv_par01        //  Rango de Procesos                        �
� mv_par02        //  Procedimiento                            |
� mv_par03        //  Periodo                                  �
� mv_par04        //  Num pago                                 �
� mv_par05        //  Sucursales                               �
� mv_par06        //  Centros de Costo                         �
� mv_par07        //  Matricula                                �
� mv_par08        //  Nombre                                    �
� mv_par09        //  Placa                                    �
� mv_par10        //  Situacion                                �
� mv_par11        //  Categoria                                �
� mv_par12        //  Unidad Organizacional                    �
����������������������������������������������������������������
*/
	
	
	

		oReport := TReport():New("GPER882","","GPER882",{|oReport| IMPGPER882()},"")

	oSection1 := TRSection():New(oReport,"","")	                          
	oSection2 := TRSection():New(oSection1,"", "") 

	oSection3 := TRSection():New(oSection2, "") 
	oSection4 := TRSection():New(oSection3, "") 
	oSection5 := TRSection():New(oSection4, "") 


	oSection1:PrintLine()                                                                                     
	oSection2:PrintLine()                                                                                     
	oSection3:PrintLine()                                                                                     
	oSection4:PrintLine()                                                                                     
	oSection5:PrintLine()                                                                                     
Return oReport

Static Function PrintReport(oReport)
	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(1):Section(1)
	Local oSection3 := oReport:Section(1):Section(1):Section(1)
	Local oSection4 := oReport:Section(1):Section(1):Section(1):Section(1)
	Local oSection5 := oReport:Section(1):Section(1):Section(1):Section(1):Section(1)

	Local cQuery    := ""
	local cNOmbre := ""   
	local x:= 0

	Private cacti :=  ""
	Private cRenta :=""
	Private cRentaporc :=""
	Private cdiastot:= ""
	Private cArquivo    	:= "firmarep.bmp"	//Nome do arquivo Bitmap que sera impresso na primeira pagina

	oReport:StartPage()
	x:= 10

	oFont09	:= TFont():New("Arial",09,09,,.F.,,,,.T.,.F.)
	oFont10 := TFont():New("Arial",10,10,,.F.,,,,.T.,.F.)
	oFont10n:= TFont():New("Arial",10,10,,.T.,,,,.T.,.F.)   
	oFont12n:= TFont():New("Arial",12,12,,.T.,,,,.T.,.F.) 
	 

	cacti:=  fTabela( "S014", 01, 1) //16
	cRenta := fTabela( "S004", 01, 5)
	cRentaPorc := fTabela( "S004", 1, 6)
	cMonto := fTabela( "S004", 1, 8)       
	cdiastot:= fTabela( "S004", 1, 7)

	oSection1:Init()    
	nlin := 350  
	ncolmax := 2100                                                                                                                                                                    
	oSection1:say ( nlin, 600,"LIQUIDACION DE DISTRIBUCION DE UTILIDADES", oFont12n )
	nlin+=150
	cNOmbre := alltrim(SRA->RA_PRISOBR)+" "+alltrim(SRA->RA_SECSOBR)+" "+alltrim(SRA->RA_PRINOME)+" "+alltrim(SRA->RA_SECNOME)
	oSection1:say ( nlin, 080,"Se�or(a) "+cNombre+" con numero de identificaci�n "+SRA->RA_CIC+", "+" la empresa "+alltrim(SM0->M0_NOMECOM)+", " , oFont10 )
	nlin+=50   
	oSection1:say ( nlin, 080,"con RUC no. "+SM0->M0_CGC+"domiciliado en "+alltrim(SM0->M0_ENDENT)+" dedicada a la actividad "+alltrim(cacti), oFont10 )
	nlin+=50                                                                   
	oSection1:say ( nlin, 080," debidamente representado por "+UPPER(cNomeRep), oFont10 )                          
	nlin+=50                                                                   
	oSection1:say ( nlin, 080,"a obtenido una renta anual antes de impuestos ascendente al monto "+ STR(cRenta)+". De dicho monto, le correponde distribuir el porcentaje de "+Alltrim(STR(cRentaPorc))+" %", oFont10 )
	nlin+=50                                                                                                                              
	oSection1:say ( nlin, 080,"con respecto a lo establecido en el articulo 2� del decreto Legislativo N� 892 y el D.S. N� 009-98-TR. El c�lculo de la partici�n de utilidades"  , oFont10 )
	nlin+=50  
	oSection1:say ( nlin, 080,"del ejercicio economico correspondiente al a�o de "+ cAno+ ", a sido  efectuado en base a los siguiente datos generales:"   , oFont10 )
	nlin+=100  


	oSection1:say ( nlin, 080,"1.Utilidad por distribuir", oFont10n )
	nlin+=50
	oSection1:say ( nlin, 080,"-Renta anual de la empresa antes de impuestos:", oFont10 )
	oSection1:say ( nlin, 2000,"S/. " +Transform( cRenta, "@E 99,999,999,999.99"),oFont10,,,,1 )	                                      
	nlin+=50
	oSection1:say ( nlin, 080,"-Porcentaje a distribuir:", oFont10 )
	oSection1:say ( nlin, 1850,STR(cRentaporc)+" %", oFont10 )                                      
	nlin+=50
	oSection1:say ( nlin, 080,"-Monto a distribuir:", oFont10 )
	oSection1:say ( nlin, 2000,Transform( cMonto, "@E 99,999,999,999.99"), oFont10 ,,,,1)                                      
	nlin+=50
	oSection1:say ( nlin, 080,"2.C�lculo de la participaci�n", oFont10n )
	nlin+=50
	oSection1:say ( nlin, 080,"2.1. Seg�n los d�as laborados", oFont10n )
	nlin+=50
	oSection1:say ( nlin, 080,"-N�mero total de dias laborados por todos los trabajadores de la empresa:", oFont10 )
	oSection1:say ( nlin, 2000,Transform( cdiastot, "@E 99,999,999,999.99"), oFont10,,,,1 )                                      
	nlin+=50
	oSection1:say ( nlin, 080,"-N�mero de dias laborados durante el ejercicio por el trabajador:", oFont10 )
	oSection1:say ( nlin, 2000,Transform( n15H, "@E 99,999,999,999.99"), oFont10,,,,1 )    
	nlin+=50
	oSection1:say ( nlin, 080,"-Participaci�n del trabajador seg�n  los d�as laborados:", oFont10 )
	oSection1:say ( nlin, 2000,Transform( n16V, "@E 99,999,999,999.99"), oFont10 ,,,,1)   
	nlin+=50
	oSection1:say ( nlin, 080,"2.2.Seg�n las remuneraciones percibidas", oFont10n )
	nlin+=50
	oSection1:say ( nlin, 080,"-Remuneraci�n computable total pagada a todos los trabajadores de la empresa:", oFont10 )
	oSection1:say ( nlin, 2000,Transform( cMonto, "@E 99,999,999,999.99"), oFont10,,,,1 ) 
	nlin+=50
	oSection1:say ( nlin, 080,"-Remuneraci�n computable percibida durante el ejercicio por el  trabajador:", oFont10 )
	oSection1:say ( nlin, 2000,Transform( n18V, "@E 99,999,999,999.99"), oFont10 ,,,,1)   
	nlin+=50
	oSection1:say ( nlin, 080,"-Participaci�n  del  trabajador seg�n las remuneraciones  percibidas:", oFont10 )
	oSection1:say ( nlin, 2000,Transform(n19V, "@E 99,999,999,999.99"), oFont10,,,,1 ) 
	nlin+=50
	oSection1:say ( nlin, 080,"3.Monto de la participaci�n a percibir por el trabajador", oFont10n )
	nlin+=50
	oSection1:say ( nlin, 080,"-Participaci�n  seg�n los dias laborados:", oFont10 )
	oSection1:say ( nlin, 2000,Transform( n20V, "@E 99,999,999,999.99"), oFont10 ,,,,1)    //???????                                  
	nlin+=50
	oSection1:say ( nlin, 080,"-Participaci�n seg�n las remuneraciones percibidas:", oFont10 )
	oSection1:say ( nlin, 2000,Transform( n21V, "@E 99,999,999,999.99"), oFont10,,,,1 )    //???????                                  
	nlin+=50
	oSection1:say ( nlin, 080,"-Deducciones aplicables en la participaci�n  de utilidades:", oFont10 )
	oSection1:say ( nlin, 2000,Transform( n22V, "@E 99,999,999,999.99"), oFont10,,,,1 ) 
	nlin+=50
	oSection1:say ( nlin, 080,"-Total de la participaci�n del trabajador en las utilidades:", oFont10 )
	oSection1:say ( nlin, 2000,Transform( n23V, "@E 99,999,999,999.99"), oFont10 ,,,,1)    //???????                                  
	nlin+=50
	oSection1:say ( nlin, 080,"4.Monto del remanente generado por el trabajador", oFont10n )
	nlin+=50
	oSection1:say ( nlin, 080,"-Total de la participaci�n del trabajador en  las utilidades:", oFont10 )
	oSection1:say ( nlin, 2000,Transform( n24V, "@E 99,999,999,999.99"), oFont10 ,,,,1)    //???????                                  
	nlin+=50
	oSection1:say ( nlin, 080,"-Tope de 18 remuneraciones del trabajador:", oFont10 )
	oSection1:say ( nlin, 2000,Transform( n25V, "@E 99,999,999,999.99"), oFont10 ,,,,1)    //???????                                  
	nlin+=50
	oSection1:say ( nlin, 080,"-Remanente destinado al FONDOCALPROEM", oFont10 )
	oSection1:say ( nlin, 2000,Transform( n26V, "@E 99,999,999,999.99"), oFont10 ,,,,1)    //???????                                  

	oSection2:Init() 
	oSection3:Init() 

	oSection4:Init() 
	nlin+=200
	oSection1:say ( nlin, 080,"LIMA"+ STR(DAY(DDATABASE))+" de " + mesextenso(month(ddatabase))+ " del "+str(year(ddatabase)), oFont10 )
	nlin+=600
	osection4:SayBitmap(nlin-250, 050, cArquivo, 500, 300,,.F.) //Tem que estar abaixo do RootPath
	osection4:LINE (nlin,050,nlin,700)                                                              
	osection4:LINE (nlin,1400,nlin,ncolmax)     
	oSection4:say ( nlin+x, 100, UPPER(cNomeRep), oFont10 )
	oSection4:say ( nlin+x, 1500, cNombre, oFont10 )
	nlin+=50 
	oSection4:say ( nlin+x, 100, SM0->M0_NOME, oFont10 )

	oReport:Endpage()
Return 


Static Function ImpGPER882()

	Local X := 0
	Local cAliasX := cAliasX:=GetNextAlias()
	Local  cFiltro		:= ""
	Local cSitQuery := ""
	Local cCatQuery := ""
	Local cSituacao :=''
	Local cCategoria :=''
	Local  cProcesso 	:= ""
	
	Local nCont:=0

	Private aPerFechado:= {}
	Private aPerAberto:= {}
	Private aPerTodos:= {}

	//Vaviaveis private para impressao
	Private aInfo:= {} 
	Private aVerbasCTS   := {} 
	Private cCargoRep:= ""
	Private cNomeRep := ""
	Private cDataPago:= ""
	Private cRuc := c15:= ""
	Private n15H := 0   
	Private c16 := c18:= c19 := c20 := c21:= c22 := c23 := c24:= c25:= c26 := ""
	Private n16V := n18V:= n19V := n20V := n21V:= n22V := n23V := n24V := n25V := n26V := 0

	//������������������������������������������������������������������Ŀ
	//�Objetos para Impressao Grafica - Declaracao das Fontes Utilizadas.�
	//��������������������������������������������������������������������
	Private oFont09, oFont10, oFont10n, oFont12n


	oFont09	:= TFont():New("Courier New",09,09,,.F.,,,,.T.,.F.)
	oFont10 := TFont():New("Courier New",10,10,,.F.,,,,.T.,.F.)
	oFont10n:= TFont():New("Courier New",10,10,,.T.,,,,.T.,.F.)     //Negrito//
	oFont12n:= TFont():New("Courier New",12,12,,.T.,,,,.T.,.F.)     //Negrito//
	
	Pergunte (oReport:uParam,.f.)
	
	cProcedi	:= mv_par02
	cMes 	  	:= Substr(mv_par03, 5, 2)
	cAno 	  	:= Substr(mv_par03, 1, 4)
	cSemana 	:= mv_par04

	
	cSituacao	:= alltrim(mv_par10)
	cCategoria	:= alltrim(mv_par11)
	
//-- Situacao do Funcionario
	cSitQuery := ""
	For nCont:=1 to Len(cSituacao)
		cSitQuery += "'"+Subs(cSituacao,nCont,1)+"'"
		If ( nCont+1 ) <= Len(cSituacao)
			cSitQuery += "," 
		Endif
	Next nCont     
	cSitQuery := "%" + cSitQuery + "%"
	

//-- Categoria do Funcionario
	cCatQuery := ""
	For nCont:=1 to Len(cCategoria)
		cCatQuery += "'"+Subs(cCategoria,nCont,1)+"'"
		If ( nCont+1 ) <= Len(cCategoria)
			cCatQuery += "," 
		Endif
	Next nCont
	cCatQuery := "%" + cCatQuery + "%"
	
	MakeSqlExpr(oReport:uParam)

	if !empty(MV_PAR01) 
		IIf (!empty(cFiltro), cFiltro+=" AND ",)
	 	cFiltro += MV_PAR01 
	 endif
	if !empty(MV_PAR05)
	    IIf (!empty(cFiltro), cFiltro+=" AND ",)
	 	cFiltro += MV_PAR05 
	endif 	 
	if !empty(MV_PAR06)
		 IIf (!empty(cFiltro), cFiltro+=" AND ",)
	 	 cFiltro += MV_PAR06 
	 endif
	 if !empty(MV_PAR07)
	 	IIf (!empty(cFiltro), cFiltro+=" AND ",)
	 	cFiltro +=  MV_PAR07
	endif 	
	if !empty(MV_PAR08)
	 	IIf (!empty(cFiltro), cFiltro+=" AND ",)
	 	cFiltro +=  MV_PAR08
	endif 
	if !empty(MV_PAR09)
	 	IIf (!empty(cFiltro), cFiltro+=" AND ",)
	 	cFiltro +=  MV_PAR09
	endif 
	if !empty(MV_PAR12)
	 	IIf (!empty(cFiltro), cFiltro+=" AND ",)
	 	cFiltro +=  MV_PAR12
	endif 	

if !empty(cFiltro)
		cFiltro := "%" + cFiltro + "%"
endif


	cOrdem 		:= "%RA_FILIAL, RA_MAT%"		
BeginSql alias cAliasX
		SELECT RA_FILIAL, RA_MAT, RA_PROCES,RA_PRISOBR, RA_SECSOBR, RA_PRINOME,RA_SECNOME,RA_NOMECMP
		FROM %table:SRA% SRA
		WHERE %exp:cFiltro%  AND
				SRA.RA_SITFOLH IN (%exp:Upper(cSitQuery)%) 	AND
			    SRA.RA_CATFUNC IN (%exp:Upper(cCatQuery)%) 	AND
			   SRA.%notDel%   
		ORDER BY %exp:cOrdem%     

EndSql

	


	While (cAliasX)->( !EOF() )   
		
		fInfo(@aInfo, (cAliasX)->RA_FILIAL)

		cNomeRep  := RTRIM(LTRIM(fTabela("S002", 01, 7))) + " " + RTRIM(LTRIM(fTabela("S002", 01, 8)))+ " " + RTRIM(LTRIM(fTabela("S002", 01, 9))) 
		cCargoRep := fTabela("S002", 01, 10)
		cRuc := fTabela("S002", 01, 01)     


		c15:= fDescRCC("S019","000115", 1,6,7,90)
		c16:= fDescRCC("S019","000216", 1,6,7,90)
		c18:= fDescRCC("S019","000218", 1,6,7,90)
		c19:= fDescRCC("S019","000319", 1,6,7,90)
		c20:= fDescRCC("S019","000420", 1,6,7,90)
		c21:= fDescRCC("S019","000521", 1,6,7,90)
		c22:= fDescRCC("S019","000622", 1,6,7,90)
		c23:= fDescRCC("S019","000723", 1,6,7,90)
		c24:= fDescRCC("S019","000723", 1,6,7,90)
		c25:= fDescRCC("S019","000825", 1,6,7,90)
		c26:= fDescRCC("S019","000926", 1,6,7,90)


		fRetPerComp( cMes, cAno, , (cAliasX)->RA_PROCES,cProcedi , @aPerAberto, @aPerFechado,@aPerTodos )

		If !( len(aPerFechado) < 1 )
			aSort(aPerFechado,,,{|x,y| x[2] < y[2] })
			cDataPago:= DtoC(aPerFechado[1,9])                                                                                 
			aVerbasCTS:= BuscaPercerrado(cAno , aPerFechado[1,7], aPerFechado[1,8], (cAliasX)->RA_MAT)	
		Endif
		If !( len(aPerAberto) < 1 )
			aSort(aPerAberto,,,{|x,y| x[2] < y[2] })
			cDataPago:= DtoC(aPerAberto[1,9])                                                                                 
			aVerbasCTS:= BuscaPerAberto( aPerAberto[1,1],aPerAberto[1,7] ,aPerAberto[1,8],(cAliasX)->RA_MAT)	

		Endif

		FOR X := 1 TO LEN(aVerbasCTS)        
			IF averbasCts[x][1] $ c15
				n15H := n15H + averbasCts[x][2] //HORAS			
				n16V := n16V + averbasCts[x][3] //VALOR
			ENDIF                                  
			IF averbasCts[x][1] $ c18
				n18V := n18V + averbasCts[x][3]
			ENDIF
			IF averbasCts[x][1] $ c19
				n19V := n19V + averbasCts[x][3]
			ENDIF
			IF averbasCts[x][1] $ c20
				n20V := n20V + averbasCts[x][3]
			ENDIF
			IF averbasCts[x][1] $ c21
				n21V := n21V + averbasCts[x][3]
			ENDIF
			IF averbasCts[x][1] $ c22
				n22V := n22V + averbasCts[x][3]
			ENDIF
			IF averbasCts[x][1] $ c23
				n23V := n23V + averbasCts[x][2]
			ENDIF
			IF averbasCts[x][1] $ c24
				n24V := n24V + averbasCts[x][3]
			ENDIF
			IF averbasCts[x][1] $ c25
				n25V := n25V + averbasCts[x][3]
			ENDIF
			IF averbasCts[x][1] $ c26
				n26V := n26V + averbasCts[x][3]
			ENDIF
		NEXT X

	

		PrintReport(oReport)

		(cAliasX)->(dbSkip())

	End

(cAliasX)->(dbCloseArea())


Return

static function BuscaPercerrado( cPeriodo,cProceso,cRoteir,cMat )

	Local cQuery := ""
	LOCAL aVerbasAc := {} 

	cAliasSRD 	:= "QSRD"
	cQuery := "SELECT RD_PD,SUM(RD_HORAS) AS HORAS,SUM(RD_VALOR) AS VALOR "
	cQuery +=  " FROM  "  +RetSqlName("SRD") + " SRD" 
	cQuery +=  " WHERE  SRD.RD_MAT    	= '"+cMat+"' "
	cQuery +=  " AND  SRD.RD_PROCES    	= '"+cProceso+"' "	
	cQuery +=  " AND  SRD.RD_DATPGT 	= '"+cPeriodo+"' "	
	cQuery +=  " AND  SRD.RD_ROTEIR   	= '"+cRoteir+"' "	
	cQuery +=  " AND D_E_L_E_T_ <> '*'  "
	cQuery +=  " GROUP BY SRD.RD_PD   "
	cQuery +=  " ORDER BY SRD.RD_PD  "
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery), cAliasSRD, .F., .T.)
	While (cAliasSrD)->( !EOF() )
		aadd(aVerbasAc,;
		{(cAliasSrd)->RD_PD,;
		(cAliasSrd)->HORAS,;
		(cAliasSrd)->VALOR})

		(cAliasSrD)->( dbSkip() )

	ENDDO
	(cAliasSrD)->( dbCLOSEAREA() )
Return(aVerbasAc)	
 


static function BuscaPerAberto( cPeriodo,cProceso,cRoteir,cMat )

	Local cQuery := ""
	LOCAL aVerbasAc := {}
	cAliasSRC 	:= "QSRC"
	cQuery := "SELECT RC_PD, SUM(RC_HORAS) AS HORAS,SUM(RC_VALOR) AS VALOR "
	cQuery +=  " FROM  "  +RetSqlName("SRC") + " SRC" 
	cQuery +=  " WHERE  SRC.RC_MAT   	 = '"+cMat+"' "
	cQuery +=  " AND  SRC.RC_PROCES   	 = '"+cProceso+"' "	
	cQuery +=  " AND  SRC.RC_PERIODO   	 = '"+cPeriodo+"' "	
	cQuery +=  " AND  SRC.RC_ROTEIR    	 = '"+cRoteir+"' "	
	cQuery +=  " AND D_E_L_E_T_ <> '*'  "
	cQuery +=  " GROUP BY SRC.RC_PD   "
	cQuery +=  " ORDER BY SRC.RC_PD  "
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery), cAliasSRC, .F., .T.)
	While (cAliasSrc)->( !EOF() )
		aadd(aVerbasAc,;
		{(cAliasSrc)->RC_PD,;
		(cAliasSrc)->RC_HORAS,;
		(cAliasSrc)->RC_VALOR})

		(cAliasSrc)->( dbSkip() )

	ENDDO
	(cAliasSrc)->( dbCLOSEAREA() )
Return(aVerbasAc)	
 

