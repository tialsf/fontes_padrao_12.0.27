#include 'Protheus.ch'
#include 'FWMVCDEF.ch'
#include 'Totvs.Ch'

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TRMA140A
Classe de evento do MVC Cadastro de Instrutores.

@author  Luis Fellipy Bett
@since   11/09/2018
@type    Class
@version 12.1.17
/*/
//-------------------------------------------------------------------------------------------------------------
Class TRMA140A FROM FWModelEvent

    Method New()
    Method ModelPosVld() //Valida��o P�s-Valid do Modelo

End Class

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} New
Mehtod New para cria��o da estancia entre o evento e as classes.

@author  Luis Fellipy Bett
@since   11/09/2018
@type    Class
@version 12.1.17
/*/
//-------------------------------------------------------------------------------------------------------------
Method New() Class TRMA140A

Return

//-------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelPosVld
Method para p�s-valida��o do Modelo.

@param oModel - Objeto - Modelo utilizado.
@param cModelId - Caracter - Id do modelo utilizado.

@class TRMA140A - Classe origem.

@author  Luis Fellipy Bett
@since   11/09/2018
@type    Class
@version 12.1.17
/*/
//-------------------------------------------------------------------------------------------------------------
Method ModelPosVld(oModel, cModelId) Class TRMA140A

	Local lRet		:= .T.
	Local aAreaRA7	:= RA7->( GetArea() ) //Salva �rea posicionada.
	Local nOpca		:= oModel:GetOperation() // Opera��o de a��o sobre o Modelo

	If nOpca <> 5
		lRet := MDTObriEsoc( "RA7" ) //Verifica se campos obrigat�rios ao eSocial est�o preenchidos
	EndIf

	RestArea( aAreaRA7 )//Retorna �rea.

Return lRet