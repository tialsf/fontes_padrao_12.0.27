#INCLUDE "CNTA260.ch"
#include "protheus.ch"
#include "tbiconn.ch"
#INCLUDE "GCTXDEF.CH"

//-- altera��o para permitir carregar model
#INCLUDE "FWBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#DEFINE DEF_SVIGE "05" //Vigente

/*DEFINES p/ chamadas a funcao FwLogMsg*/
#DEFINE _LOGTYPE "INFO"
#DEFINE _LOGPROG "CNTA260"
#DEFINE _LOGNUMB "01"
#DEFINE _LOGTIME -1

Static xCompet	:= ""
Static lCN260OBRIG := ExistBlock("CN260OBRIG")

/*/{Protheus.doc} CNTA260
Rotina responsavel pela execucao das medicoes de contratos do tipo automatico
@author Marcelo Custodio
@since 28/07/2006
@return lRet, ${return_description}
@param cCodEmp, characters, Codigo da Empresa
@param cCodFil, characters, Codigo da Filial
@param cInterval, characters, Intervalo entre as execucoes(HH:MM)
@type function
/*/
Main Function CNTA260(cCodEmp,cCodFil,cInterval)
	Local lRet 		:= .T.
	Local cEmpCod	:= ""
	Local cFilCod	:= ""	
	Default cCodEmp	:= IIF(Type('cEmpAnt') != 'U',cEmpAnt,"")
	Default cCodFil	:= IIF(Type('cFilAnt') != 'U',cFilAnt,"")
	Default cInterval	:= ""			//Mantido para compatibilidade com vers�es anteriores.
	
	PRIVATE lMsErroAuto := .F.
	
	//-- Verifica se a rotina e executada atraves de um JOB
	If GetRemoteType() == -1		  //-- Execucao por JOB
		If ValType(cCodEmp) == "A"
			cEmpCod := cCodEmp[1]
			cFilCod := cCodEmp[2]
		Else
			cEmpCod := cCodEmp
			cFilCod := cCodFil
		Endif
	
		If Empty(cEmpCod) .Or. Empty(cFilCod)
			lRet := .F.
			FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB, 'CNTA260 ERROR - Param Error', 0, _LOGTIME, {})
		Else
			RpcSetType(3)
			RpcSetEnv(cEmpCod,cFilCod,,,"GCT","CNTA260",{'CN9','CNA','CNB','CND','CNE'})
			lRet := !CN260Exc(.T.)
			RpcClearEnv()
		EndIf
	ElseIf( Aviso("CNTA260", STR0015,{STR0017, STR0016}) == 1 )//-- Execucao por Menu					
		Processa( {|| CN260Exc(.F.) } )
	EndIf
Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun�ao    CN260Exc Autor Marcelo Custodio      Data28.07.2006���
�������������������������������������������������������������������������Ĵ��
���Descri�ao Executa medicoes pendentes para os contratos automaticos  ��
�������������������������������������������������������������������������Ĵ��
���Sintaxe   CN260Exc(lExp01)                                          ��
�������������������������������������������������������������������������Ĵ��
���ParametroslExp01 - Executado pelo job                               ��
�������������������������������������������������������������������������Ĵ��
�����������������������������������������������������������������������������*/
Function CN260Exc(lJob)
	Local lMedPend := (GetNewPar("MV_MEDPEND","1") == "1")//Parametro que informa se a rotina busca por medicoes pendentes
	Local dData    := IIF(lJob,Date(),dDataBase)//Data Atual
	Local cTxLog   := ""//Texto do log
	Local lFail    := .F.
	Local lQuery   := .F.
	Local nStack
	Local cArqTrb
	Local nX         := 0
	Local aParams    := {}
	Local aQuebraThr := {}
	Local aContratos := {}
	Local cJobFile   := ""
	Local cJobAux    := ""
	Local cStartPath := GetSrvProfString("Startpath","")
	Local nThreads   := SuperGetMv('MV_CT260TH',.F.,0) //parametro utilizado para informar o numero de threads para o processamento.
	Local lCnta121   := GetNewPar('MV_CT26021',	.F.) //Parametro diz se deve usar exclusivamente o CNTA121 ou nao
	Local aErros	:= {}
	Local aMedicao	:= {}
	Private lMsErroAuto := .F.
	Private lAutoErrNoFile	:= .T.	
	
	//Valida se o sistema foi atualizado   	
	If !lCnta121
		//Gera historico                       	
		cTxLog := STR0018+" - "+DTOC(dData)+" - "+time()+CHR(13)+CHR(10)//"Log de execucao das medicoes automaticas"
		cTxLog += Replicate("-",128)+CHR(13)+CHR(10)
		nStack := GetSX8Len()
	
		If lJob
			FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB, STR0001, 0, _LOGTIME, {})//"Verificando medi��es pendentes"
			FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB, STR0002 + time(), 0, _LOGTIME, {})
		Else
			IncProc(STR0001 + " - " + DTOC(dData))
		EndIf
			
		cArqTrb := GetQryTrb(dData, .F., .T.)
		
		If nThreads > 0
			
			aQuebraThr := CN260QtdThr(cArqTrb,nThreads)
			aThreads   := aQuebraThr[1]
			aContratos := aQuebraThr[2]
			aJobAux    := {}
			
			If Len(aThreads) > 0 .And. Len(aThreads[1]) > 0
			
				For nX:= 1 To Len(aThreads)
				
					// Informacoes do semaforo
					cJobFile:= cStartPath + CriaTrab(Nil,.F.)+".job"
							
					// Inicializa variavel global de controle de thread
					cJobAux:="cGlb"+cEmpAnt+cFilAnt+StrZero(nX,2)
					PutGlbValue(cJobAux,"0")
					GlbUnLock()
					
					aParams:= { cEmpAnt		,;	//1 
								  cFilAnt		,;	//2
								  dDataBase	,;	//3
								  lMedPend		,;	//4
								  nStack		,;	//5
								  cTxLog		,;	//6
								  0		,;	//7
								  0		,;	//8
								  lJob}			//9
				
					StartJob("CNTA260JOB",GetEnvServer(),.F.,aThreads[nX],aContratos,cJobFile,StrZero(nX,2),cArqTrb,aParams)
				
				Next nX
			EndIf			
		Else							
			CN9->(DbSetOrder(1))			
			
			While !(cArqTrb)->(Eof())				
				lQuery := .T.
				
				aMedicao := GetArrCtr(cArqTrb)//Transforma a posicao atual de <cArqTrb> em um array
				
				IncEEncMed(aMedicao, aErros, @cTxLog, lJob)//Inclui e encerra a medicao				
		
				(cArqTrb)->(dbSkip())
			EndDo
		EndIf		
		(cArqTrb)->(dbCloseArea())
	
		If(Len(aErros) == 0)
			If lJob				
				FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB, STR0013 + time(), 0, _LOGTIME, {})
			EndIf
			cTxLog += STR0013 + time()
		Else
			If lJob
				FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB, STR0014, 0, _LOGTIME, {})
			Else
				TmsMsgErr(aErros) //Exibe a lista de erros p/ o usuario
				FwFreeArray(aErros)
			EndIf
			lFail := .T.
			cTxLog += STR0014
		EndIf
				
		//Executa ponto de entrada apos a gravacao da medi��o autom�tica
		If ExistBlock("CNT260GRV")
			ExecBlock("CNT260GRV",.F.,.F.)
		EndIf
		
		If lQuery		
			//Executa gravacao do arquivo de historico		
			MemoWrite(Criatrab(,.f.)+".LOG",cTxLog)
			
			//Emite alerta com o log do processamento		
			MEnviaMail("041",{cTxLog})
		EndIf	
	EndIf
	
	//-- Incluir medi��o automatica de contratos recorrentes pela rotina CNTA121
	CN260Exc121(lJob, lCnta121)
Return lFail

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun�ao    CN260Exc_121 Autor Marcelo Custodio      Data28.07.2006���
�������������������������������������������������������������������������Ĵ��
���Descri�ao Executa medicoes pendentes para os contratos automaticos  ��
�������������������������������������������������������������������������Ĵ��
���Sintaxe   CN260Exc_121(lExp01)                                          ��
�������������������������������������������������������������������������Ĵ��
���ParametroslExp01 - Executado pelo job                               ��
�������������������������������������������������������������������������Ĵ��
��        ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.            ��
�������������������������������������������������������������������������Ĵ��
���Programador Data   BOPS  Motivo da Alteracao                    ��
�������������������������������������������������������������������������Ĵ��
��                                                               ��
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Function CN260Exc121(lJob, lProcTdsMd)
Local aArea			:= GetArea()
Local aSaveLines	:= FwSaveRows()
Local oModel 		:= Nil
Local oModelCND		:= Nil
Local oModelCNE		:= Nil
Local oModelCXN		:= Nil
Local cTxLog   		:= ""//Texto do log
Local cArqTrb
Local cHelp			:= ""
Local cTxPlan		:= ""
Local cContrato 	:= ""
Local cCompet		:= ""
Local cPlan			:= ""
Local lContinua	:= .T.
Local lFail    	:= .F.
Local lQuery 	:= .F.
Local nStack	:= 0
Local dData    	:= If(lJob,date(),dDataBase)//Data Atual
Local lIncluiMed:= .T.
Local lEncerra	:= .T.
Local lMedPend 	:= (GetNewPar("MV_MEDPEND","1") == "1")//Parametro que informa se a rotina busca por medicoes pendentes
Local cQuebra	:= ""
Local cExpFilt	:= ""
Local cValFilt	:= ""
Local nLinha	:= 0
Default lProcTdsMd	:= SuperGetMv('MV_CT26021',.F., .F.) //Parametro diz se deve usar exclusivamente o CNTA121 ou nao

//-- Gera historico
cTxLog := STR0018+" - "+DTOC(dData)+" - "+time()+CHR(13)+CHR(10)//"Log de execucao das medicoes automaticas"
cTxLog += Replicate("-",128)+CHR(13)+CHR(10)

//-- Valida se o sistema foi atualizado
If lContinua
	nStack 	:= GetSX8Len()

	If lJob
		FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB, STR0001, 0, _LOGTIME, {})//"Verificando medi��es pendentes"
		FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB, STR0002 + time(), 0, _LOGTIME, {})
	Else
		IncProc(STR0001 + " - " + DTOC(dData))
	EndIf	

	/*cExpFilt cont�m a express�o usada pro loop marcar todas as planilhas da consulta.*/
	cExpFilt := '(cArqTrb)->( CN9_FILIAL + CN9_NUMERO )'
	cExpFilt += ' + '
	cExpFilt += 'IIF((AllTrim((cArqTrb)->CNF_CONTRA) == "RECORRENTE"), GetCompDt(Stod((cArqTrb)->CNF_COMPET)), AllTrim((cArqTrb)->CNF_COMPET))'

	cArqTrb := GetQryTrb( dData, .T., lProcTdsMd)//Filtra parcelas de contratos automaticos pendentes para a data atual
	
	CN9->(DbSetOrder(1))
	oModel := FWLoadModel("CNTA121")
	While !(cArqTrb)->(Eof())
		lQuery := .T.
		lIncluiMed	:= .T.
		lEncerra	:= .T.
		lContinua	:= .T.
		
		cContrato 	:= (cArqTrb)->CN9_NUMERO						
		cPlan		:= AllTrim((cArqTrb)->CNA_NUMERO)

		If (AllTrim((cArqTrb)->CNF_CONTRA) == "RECORRENTE")
			//-- Quando possui recorrente n�o possui CNF, ela � obtida da data da pr�xima medi��o que esta na CNA.
			cCompet	:= GetCompDt(Stod((cArqTrb)->CNF_COMPET))
		Else
			cCompet	:= AllTrim((cArqTrb)->CNF_COMPET)
		EndIf

		If(MdJaGerada(cContrato, (cArqTrb)->CNF_REVISA, @cPlan, (cArqTrb)->CNF_PARCEL, @cCompet))//Se a medi�ao j� foi gerada
			lIncluiMed	:= .F.
			lEncerra	:= lMedPend .And. (AllTrim(CND->CND_SITUAC) != 'E')
		EndIf

		If(lIncluiMed .Or. lEncerra)
			CN9->(DbSeek((cArqTrb)->(CN9_FILIAL + CN9_NUMERO)))//Posiciona no contrato corrente
			
			A260SComp(cCompet)
			
			If(lIncluiMed)
				oModel:SetOperation(MODEL_OPERATION_INSERT)
			Else
				oModel:SetOperation(MODEL_OPERATION_UPDATE)
			EndIf
	
			If (lContinua := oModel:Activate())
				oModelCND := oModel:GetModel("CNDMASTER")
				oModelCXN := oModel:GetModel("CXNDETAIL")
				oModelCNE := oModel:GetModel("CNEDETAIL")
				oModelCND:GetStruct():SetProperty('*',MODEL_FIELD_WHEN,{||.T.})
				oModelCXN:GetStruct():SetProperty('*',MODEL_FIELD_WHEN,{||.T.})
				oModelCNE:GetStruct():SetProperty('*',MODEL_FIELD_WHEN,{||.T.})
				
				If(lIncluiMed)
					If (lContinua := oModelCND:SetValue("CND_CONTRA",cContrato))
						oModelCND:SetValue("CND_REVISA"	,CnGetRevVg((cArqTrb)->CN9_NUMERO))
						oModelCND:SetValue("CND_COMPET"	,cCompet)
						CN121Carga((cArqTrb)->CNF_CONTRA,(cArqTrb)->CNF_REVISA)
					EndIf				
				EndIf
			EndIf
			
			If(lContinua)
				cQuebra := &(cExpFilt)
				cValFilt:= cQuebra
				
				While ( (cArqTrb)->(!Eof() .And. cQuebra == cValFilt )) //Marca todas as planilhas da consulta
					nLinha := MTFindMVC(oModelCXN,{{"CXN_NUMPLA",(cArqTrb)->CNA_NUMERO}})
			
					If nLinha > 0
						oModelCXN:GoLine(nLinha)
						If(!oModelCXN:GetValue("CXN_CHECK"))
							lContinua := oModelCXN:SetValue("CXN_CHECK" , .T. )
							//- Ponto de Entrada para preenchimento de campos obrigatorios customizados
							If lCN260OBRIG
								ExecBlock("CN260OBRIG",.F.,.F.,{oModel})
							EndIf
							cTxPlan += STR0005+" - "+(cArqTrb)->CNA_NUMERO+CHR(13)+CHR(10)
						Else
							lContinua := .T.					
						EndIf
					EndIf			
					(cArqTrb)->(dbSkip())
					
					cValFilt := &(cExpFilt)
					If cQuebra != cValFilt
						Exit
					EndIf					
				EndDo				
			EndIf
				
			If(lContinua)
				//Commit na medi��o
				If (lContinua := oModel:VldData())
					lContinua := oModel:CommitData()			
				EndIf
			EndIf
			
			If(!lContinua .And. oModel:HasErrorMessage())
				cHelp+= cContrato+" - "+cPlan+": n�o foi possivel validar o modelo ("+ oModel:GetErrorMessage()[6] + ")"+CRLF				
			EndIf
			oModel:DeActivate()
			
			If lContinua .And. lEncerra
				
				While ( GetSX8Len() > nStack )
					ConfirmSX8() //-- Retorna controle de numeracao
				EndDo
		
				cTxLog += STR0019+" - "+CND->CND_NUMMED+CHR(13)+CHR(10)//"Medicao gerada com sucesso"
				cTxLog += STR0004+" - "+cContrato+CHR(13)+CHR(10)
				cTxLog += STR0022+" - "+(cArqTrb)->CN9_FILIAL+CHR(13)+CHR(10)
				cTxLog += 	cTxPlan
				cTxLog += STR0006+" - "+cCompet+CHR(13)+CHR(10)
		
				cTxPlan:= ""

				FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB, STR0019+" - " + CND->CND_NUMMED, 0, _LOGTIME, {})
				FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB, STR0004+" - " + cContrato, 0, _LOGTIME, {})
				FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB, STR0006+" - " + cCompet, 0, _LOGTIME, {})				
		
				If lJob
					FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB, STR0007+cCompet+STR0008, 0, _LOGTIME, {})
				EndIf
		
				// Rotina de encerramento de medi��o
				lContinua := CN121Encerr()
		
				If lContinua
					FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB, "Medicao encerrada com sucesso"+" - " + CND->CND_NUMMED, 0, _LOGTIME, {})
					FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB, STR0004+" - " + cContrato, 0, _LOGTIME, {})		
				Endif
			ElseIf(!lContinua)
				While GetSX8Len() > nStack
					RollBackSX8()
				EndDo			
				
				cTxLog += STR0004+" - "+cContrato+CHR(13)+CHR(10)
				cTxLog += STR0022+" - "+(cArqTrb)->CN9_FILIAL+CHR(13)+CHR(10)
				cTxLog += STR0005+" - "+(cArqTrb)->CNA_NUMERO+CHR(13)+CHR(10)
				cTxLog += STR0006+" - "+cCompet+CHR(13)+CHR(10)
				cTxLog += Replicate("-",128)+CHR(13)+CHR(10)
				If lJob
					FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB, STR0009 + cCompet, 0, _LOGTIME, {})
				EndIf
				
			EndIf		
		Else
			lContinua := .F.
		EndIf

		If (!lContinua)
			(cArqTrb)->(dbSkip())
		EndIf
	EndDo
	(cArqTrb)->(dbCloseArea())
Else
	If lJob
		FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB,STR0014, 0, _LOGTIME, {})
	Else
		Aviso("CNTA260",STR0014,{"Ok"})
	EndIf
	lFail := .T.
	cTxLog += STR0014
EndIf

If !Empty(cHelp)
	//- Mostra Help contendo os cotratros com falha
	Help(" ",1,"A260VLDDATA",,cHelp + "Medi��o n�o ser� gerada para estes contratos.",1,1)
EndIf

//-- Executa ponto de entrada apos a gravacao da medi��o autom�tica
If ExistBlock("CNT260GRV")
	ExecBlock("CNT260GRV",.F.,.F.)
EndIf

If lQuery
	//--  Executa gravacao do arquivo de historico
	MemoWrite(Criatrab(,.f.)+".LOG",cTxLog)
	
	//-- Emite alerta com o log do processamento
	MEnviaMail("041",{cTxLog})
EndIf

FWRestRows(aSaveLines)
RestArea(aArea)
Return lFail

//-------------------------------------------------------------------
/*/{Protheus.doc} A260GComp()
Fun��o para recuperar a variavel estatica xCompet
@author rogerio.melonio
@since 01/09/2015
/*/
//-------------------------------------------------------------------
Function A260GComp()
Return xCompet

//-------------------------------------------------------------------
/*/{Protheus.doc} A260SComp()
Fun��o para Atribuir na variavel estatica xCompet
@author rogerio.melonio
@since 01/09/2015
/*/
//-------------------------------------------------------------------
Function A260SComp(cValue)
xCompet := cValue
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} CN260QtdThr()
Funcao utilizada para calcular a quantidade de threads a serem 
executadas em paralelo.
@author janaina.jesus
@since 21/06/2018
@version 1.0
@return aThreads
/*/
//-------------------------------------------------------------------
Static Function CN260QtdThr(cArqTrb,nThreads)
Local aAreaAnt   := GetArea()
Local aContratos := {}
Local aThreads   := {}
Local nX         := 0
Local nInicio    := 0
Local nRegProc   := 0

If Select(cArqTrb) > 0
	//-- Carrega Array com os contratos
	Do While (cArqTrb)->(!Eof())
		
		aAdd(aContratos, GetArrCtr(cArqTrb))
		
		(cArqTrb)->(dbSkip())			
	EndDo
EndIf
	
//-- Verifica Limite Maximo de 40 Threads
If nThreads > 40
	nThreads := 40
EndIf

//-- Analisa a quantidade de Threads X nRegistros
If Len(aContratos) == 0
	aThreads := {}
ElseIf Len(aContratos) < nThreads
	aThreads := ARRAY(1)			// Processa somente em uma thread
Else
	aThreads := ARRAY(nThreads)		// Processa com o numero de threads informada
EndIf

//��������������������������������������������������Ŀ
//Calcula o registro original de cada thread e    
//aciona thread gerando arquivo de fila.          
//����������������������������������������������������
For nX:=1 to Len(aThreads)

	aThreads[nX]:={"","",1}
    
	// Registro inicial para processamento
	nInicio  := IIf( nX == 1 , 1 , aThreads[nX-1,3]+1 )

	// Quantidade de registros a processar
	nRegProc += IIf( nX == Len(aThreads) , Len(aContratos) - nRegProc, Int(Len(aContratos)/Len(aThreads)) )
	
	aThreads[nX,1] := nInicio
	aThreads[nX,2] := nRegProc
	aThreads[nX,3] := nRegProc

Next nX

RestArea(aAreaAnt)
Return {aThreads,aContratos}

//-------------------------------------------------------------------
/*/{Protheus.doc} CNTA260JOB()
Funcao utilizada realizar gerar/encerrar medi��es por JOB (PERFORMANCE)
@author Janaina.Jesus
@since 21/06/2018
@version 1.0
@return Nil
/*/
//-------------------------------------------------------------------
Function CNTA260JOB(aThread,aContratos,cJobFile,cThread,cArqTrb, aParams)
Local nI         := 0
Local cEmp       := aParams[1]
Local cFil       := aParams[2]
Local xData      := aParams[3]
Local cTxLog     := aParams[6]
Local lJob       := aParams[9]
Local aErros := {}
PRIVATE lMsErroAuto := .F.

// Apaga arquivo ja existente
If File(cJobFile)
	fErase(cJobFile)
EndIf

// Criacao do arquivo de controle de jobs
nHd1 := MSFCreate(cJobFile)

// STATUS 1 - Iniciando execucao do Job
PutGlbValue("cGlb"+cEmp+cFil+cThread, "1" )
GlbUnLock()

// Seta job para nao consumir licensas
RpcSetType(3) 

// Seta job para empresa filial desejada
RpcSetEnv( cEmp, cFil,,,'GCT')

//Restaura a DataBase
dDatabase:= xData

// STATUS 2 - Conexao efetuada com sucesso
PutGlbValue("cGlb"+cEmp+cFil+cThread, "2" )
GlbUnLock()

FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB,dtoc(Date()) + " " + Time()+" Inicio do job de gera��o de medi��es CNTA260 " + cJobFile, 0, _LOGTIME, {})

For nI := aThread[1] to aThread[2]
	lQuery    := .T.
	
	IncEEncMed(aContratos[nI], aErros, @cTxLog, lJob)
		
Next nI

// STATUS 3 - Processamento efetuado com sucesso
PutGlbValue("cGlb"+cEmp+cFil+cThread,"3")
GlbUnLock()

// Fecha arquivo de controle do MATA179
fClose(nHd1)

FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB,STR0013 + time(), 0, _LOGTIME, {})

cTxLog += STR0013 + time()

If ExistBlock("CNT260GRV")
	ExecBlock("CNT260GRV",.F.,.F.)//Executa ponto de entrada apos a gravacao da medi��o autom�tica
EndIf

If lQuery
	//���������������������������������������������������Ŀ
	//Executa gravacao do arquivo de historico         
	//�����������������������������������������������������
	MemoWrite(Criatrab(,.f.)+".LOG",cTxLog)

	//���������������������������������������������������Ŀ
	//Emite alerta com o log do processamento          
	//�����������������������������������������������������
	MEnviaMail("041",{cTxLog})
EndIf

//-- Incluir medi��o automatica de contratos recorrentes pela rotina CNTA121
CN260Exc121(lJob)

Return

/*/{Protheus.doc} GetQryTrb
 Gera uma consulta com as medicoes pendentes, executa e retorna um alias com o resultado.
@author philipe.pompeu
@since 22/07/2019
@return cArqTrb, retorna um alias com o resultado da consulta
@param dEndDate, date, descricao
@param lRecorre, logical, descricao
@param lNaoRecorr, logical, descricao
/*/
Static Function GetQryTrb(dEndDate,lRecorre, lNaoRecorr)
	Local cQuery   := ""
	Local cArqTrb := ""
	Local nDias    := GetNewPar( "MV_MEDDIAS", 0 )//Parametro que armazena a quantidade de dias de busca
	Local dDataI   := dEndDate-nDias//Data de inicio
	Local cCnt260Fil:= 0
	Local lGS		:= ExistFunc("TecMdAtQry")
	Default lRecorre	:= .F.	
	Default lNaoRecorr	:= .T.
	Default dEndDate	:= IIF(IsBlind(),Date(),dDataBase)
	
	//Filtra parcelas de contratos automaticos
	//pendentes para a data atual
	cArqTrb	:= CriaTrab( nil, .F. )	
	cQuery := "SELECT * FROM ("
	If(lNaoRecorr)		
		cQuery += "SELECT DISTINCT CNF.CNF_COMPET,CNF.CNF_CONTRA,CNF.CNF_REVISA,CNA.CNA_NUMERO,CNF.CNF_PARCEL,CN9.CN9_FILIAL,CN9.CN9_NUMERO,"
		cQuery += "(CASE WHEN CNL.CNL_MEDAUT = '0' THEN CN1.CN1_MEDAUT ELSE CNL.CNL_MEDAUT END) MEDAUT"
		cQuery += " FROM " + RetSQLName("CNF") + " CNF" 
		
		//Join CNF x CN9(Contratos)
		cQuery += " INNER JOIN "+ RetSQLName("CN9") +" CN9 ON(CN9.CN9_FILIAL = '"+ xFilial("CN9") +"'" 
		cQuery += " AND CNF.CNF_CONTRA = CN9.CN9_NUMERO AND CNF.CNF_REVISA = CN9.CN9_REVISA AND CN9.D_E_L_E_T_ = ' ')" 
		
		//Join CN9 x CN1(Tipo de Contrato)
		cQuery += " INNER JOIN "+RetSQLName("CN1") +" CN1 ON(CN1.CN1_FILIAL = '"+ xFilial("CN1") +"' AND CN9.CN9_TPCTO  = CN1.CN1_CODIGO AND CN1.D_E_L_E_T_ = ' ')"
		
		//Join CNF x CNA(Planilhas de Contratos)
		cQuery += " INNER JOIN "+ RetSQLName("CNA") + " CNA ON(CNA.CNA_FILIAL = '"+ xFilial("CNA") +"'"
		cQuery += " AND CNF.CNF_NUMERO = CNA.CNA_CRONOG AND CNF.CNF_CONTRA = CNA.CNA_CONTRA AND CNF.CNF_REVISA = CNA.CNA_REVISA AND CNA.D_E_L_E_T_ = ' ')" 
		
		//Join CNA x CNL(Tipo de Planilha)
		cQuery += " INNER JOIN "+ RetSQLName("CNL") +" CNL ON(CNF.CNF_FILIAL = '"+ xFilial("CNF") +"' AND CNA.CNA_TIPPLA = CNL.CNL_CODIGO AND CNL.D_E_L_E_T_ = ' ')"
		
		//Join CN9 x CPD(Permissoes do Contrato)
		cQuery += " INNER JOIN "+ RetSQLName("CPD") +" CPD ON(CPD.CPD_FILIAL = '"+ xFilial("CPD") +"' AND CPD.CPD_CONTRA = CN9.CN9_NUMERO AND CPD.CPD_NUMPLA = CNA.CNA_NUMERO AND CPD_FILAUT = '"+ cFilAnt +"' AND CPD.D_E_L_E_T_ = ' ')"
		
		//Filtros
		cQuery += " WHERE CN9.CN9_SITUAC = '"+ DEF_SVIGE +"'"
		cQuery += " AND CN9.CN9_REVATU = '"+ Space(Len(CN9->CN9_REVATU)) +"'"
		cQuery += " AND CNF.CNF_PRUMED >= '"+ DTOS(dDataI) +"'"
		cQuery += " AND CNF.CNF_PRUMED <= '"+ DTOS(dEndDate) +"'"
		cQuery += " AND CNF.CNF_SALDO  > 0"
		cQuery += " AND CNA.CNA_SALDO  > 0"		
		cQuery += " AND CNF.D_E_L_E_T_ = ' '"
		
		If(!FwIsInCallStack("CN260Exc121"))			
			cQuery += " AND CNL.CNL_PLSERV <> '1'"//Nao deve considerar planilhas de servico pelo CNTA120
		EndIf
		
		//Ponto de Entrada para utiliza��o de Filtros espec�ficos
		If ExistBlock("CNT260FIL")
			cCnt260Fil := ExecBlock("CNT260FIL",.F.,.F.)
			If ValType(cCnt260Fil) == "C" .And. !Empty(cCnt260Fil)
				cQuery += " AND "+ cCnt260Fil
			EndIf
		EndIf
	EndIf
	
	If(lRecorre .And. lNaoRecorr)
		cQuery += " UNION "
	EndIf
	
	If(lRecorre)
		cQuery += " SELECT CNA.CNA_PROMED AS CNF_COMPET,'RECORRENTE' AS CNF_CONTRA,CN9.CN9_REVISA AS CNF_REVISA,CNA.CNA_NUMERO,CNA.CNA_PROPAR AS CNF_PARCEL,CN9.CN9_FILIAL,CN9.CN9_NUMERO, "
		cQuery += " ( CASE WHEN CNL.CNL_MEDAUT = '0' THEN CN1.CN1_MEDAUT ELSE CNL.CNL_MEDAUT END)  MEDAUT "
		cQuery += " FROM " + RetSQLName("CNA") + " CNA"		 
		cQuery += " INNER JOIN "+ RetSQLName("CN9") +" CN9 ON(CN9.CN9_FILIAL = '"+ xFilial("CN9") +"' AND CNA.CNA_CONTRA = CN9.CN9_NUMERO AND CNA.CNA_REVISA = CN9.CN9_REVISA AND CN9.D_E_L_E_T_ = ' ')"
		cQuery += " INNER JOIN "+ RetSQLName("CN1") +" CN1 ON(CN1.CN1_FILIAL = '"+ xFilial("CN1") +"' AND CN1.CN1_CODIGO = CN9.CN9_TPCTO AND CN1.D_E_L_E_T_ = ' ')" 
		cQuery += " INNER JOIN "+ RetSQLName("CNL") +" CNL ON(CNL.CNL_FILIAL = '"+ xFilial("CNL") +"' AND CNL.CNL_CODIGO = CNA.CNA_TIPPLA AND CNL.D_E_L_E_T_ = ' ')"		
		cQuery += " INNER JOIN "+ RetSQLName("CPD") +" CPD ON(CPD.CPD_FILIAL = '"+ xFilial("CPD") +"' AND CPD.CPD_CONTRA = CN9.CN9_NUMERO AND CPD.CPD_NUMPLA = CNA.CNA_NUMERO AND CPD_FILAUT = '"+ cFilAnt +"' AND CPD.D_E_L_E_T_ = ' ')"
		
		cQuery += " WHERE "
		cQuery += " CNA.CNA_FILIAL = '" + xFilial("CNA") +"'"	
		
		cQuery += " AND CNA.CNA_PERIOD <> ' '"
		cQuery += " AND CNA.CNA_PROMED >= '" + DTOS(dDataI)+ "'"
		cQuery += " AND CNA.CNA_PROMED <= '" + DTOS(dEndDate) + "'"
		cQuery += " AND CNA.D_E_L_E_T_ = ' '  "
		cQuery += " AND CN9.CN9_SITUAC = '"+ DEF_SVIGE +"'"
	EndIf

	cQuery += ") CN9 "
	cQuery += " WHERE MEDAUT = '1'"

	If lGs
		cQuery := TecMdAtQry(cQuery)
	EndIf

	cQuery += " ORDER BY CN9_FILIAL, CN9_NUMERO, CNF_REVISA, CNA_NUMERO"

	cQuery := ChangeQuery( cQuery )

	dbUseArea( .T., "TopConn", TCGenQry(,,cQuery), cArqTrb, .T., .T. )	
Return cArqTrb

/*/{Protheus.doc} MdJaGerada
	Valida se a medicao da <cParcel> ja foi realizada.
@author philipe.pompeu
@since 22/07/2019
@return lReturn, se <cParcel> foi gerada retorna verdadeiro
@param cContra, caractere, numero do contrato
@param cPlan, caractere, planilha do contrato
@param cParcel, caractere, parcela da planilha
/*/
Static Function MdJaGerada(cContra As char, cRevisa As char, cPlan As char, cParcel As char, cCompet As char, cNumMed As char) As Logical
	Local lReturn := .F.
	Local cUmAlias:= GetNextAlias()
	Local cSitEnc := PadR('E', Len(CND->CND_SITUAC))
	Default cNumMed := ""
	
	BeginSQL Alias cUmAlias
		SELECT CND.CND_NUMMED, CND.CND_COMPET, CND.CND_NUMERO, CND.R_E_C_N_O_ RECMED
		FROM 	%Table:CND% CND
		WHERE	CND.CND_FILIAL = %xFilial:CND% 
				AND CND.CND_CONTRA = %Exp:cContra%
				AND CND.CND_REVISA = %Exp:cRevisa% 
				AND CND.CND_NUMERO = %Exp:cPlan% 
				AND	CND.CND_PARCEL = %Exp:cParcel%
				AND	CND.CND_DTFIM = %Exp:Space(8)% 
				AND CND.%NotDel%
				
		UNION
		
		SELECT CXN.CXN_NUMMED,CND.CND_COMPET,CXN.CXN_NUMPLA CND_NUMERO ,CND.R_E_C_N_O_ RECMED 
		FROM 	%Table:CXN% CXN
		
		INNER JOIN %Table:CND% CND ON(
		CND.CND_FILIAL = %xFilial:CND% 
		AND CND.CND_NUMMED = CXN.CXN_NUMMED 
		AND CND.CND_CONTRA = CXN.CXN_CONTRA 
		AND CND.CND_REVISA = CXN.CXN_REVISA		 
		AND CND.%NotDel%)
		
		WHERE	CXN.CXN_FILIAL = %xFilial:CXN% 
				AND CXN.CXN_CONTRA = %Exp:cContra%
				AND CXN.CXN_REVISA = %Exp:cRevisa%				
				AND	CXN.CXN_PARCEL = %Exp:cParcel%
				AND	CXN.CXN_NUMPLA = %Exp:cPlan%
				AND CND.CND_SITUAC <> %Exp:cSitEnc%
				AND CXN.%NotDel%
	EndSQL
		
	If (lReturn := (cUmAlias)->(!EOF()))
		//Substitui medicao para encerramento	
		cCompet := (cUmAlias)->CND_COMPET
		cPlan 	:= (cUmAlias)->CND_NUMERO
		cNumMed := (cUmAlias)->CND_NUMMED
		CND->(DbGoTo((cUmAlias)->RECMED))
	EndIf	
	(cUmAlias)->(dbCloseArea())
Return lReturn

/*/{Protheus.doc} GetArrCtr
	Preenche um vetor com suas posicoes padronizadas p/ ser usado nas funcoes CNTA260JOB e CN260Exc
@author philipe.pompeu
@since 01/10/2019
@return aUmContrato, uma vetor preenchido com dados da medicao
@param cUmAlias, caractere, um alias com a consulta
/*/
Static Function GetArrCtr(cUmAlias As Char)
	Local aUmContrato := {}

	If(Select(cUmAlias) > 0)	
		aAdd(aUmContrato,(cUmAlias)->CNF_COMPET)
		aAdd(aUmContrato,(cUmAlias)->CNF_CONTRA)
		aAdd(aUmContrato,(cUmAlias)->CNF_REVISA)
		aAdd(aUmContrato,(cUmAlias)->CNA_NUMERO)
		aAdd(aUmContrato,(cUmAlias)->CNF_PARCEL)
		aAdd(aUmContrato,(cUmAlias)->CN9_FILIAL)
		aAdd(aUmContrato,(cUmAlias)->CN9_NUMERO)
		aAdd(aUmContrato,(cUmAlias)->MEDAUT)
	EndIf
Return aUmContrato

/*/{Protheus.doc} IncEEncMed
	Dado <aMedicao>, inclui e encerra via CNTA120.
<aMedicao> deve ser gerado pela funcao GetArrCtr p/ ter suas posicoes padronizadas.
@author philipe.pompeu
@since 01/10/2019
@return Nil, valor nulo
@param aMedicao, array, descricao
@param aErros, array, descricao
@param cTxLog, characters, descricao
@param lJob, logical, descricao
/*/
Static Function IncEEncMed(aMedicao, aErros, cTxLog, lJob)
	Local lMedPend := (GetNewPar("MV_MEDPEND","1") == "1")//Parametro que informa se a rotina busca por medicoes pendentes
	Local aCab     := {}//Cabecalho
	Local aItem    := {}//Itens
	Local cNum     := ""
	Local nStack   := GetSX8Len()	
	Local lIncluiMed:= .T.
	Local lEncerra	:= .T.	
	Local cContra	:= ""
	Local cRevisa	:= ""
	Local cCompet	:= ""
	Local cPlan		:= ""
	Local cParcel	:= ""
	Private lMsErroAuto		:= .F.
	Private lAutoErrNoFile	:= .T.
	
	If(Len(aMedicao) >= 7)
		cContra := aMedicao[7]
		cRevisa	:= aMedicao[3]
		cCompet	:= aMedicao[1]
		cPlan	:= aMedicao[4]
		cParcel	:= aMedicao[5]		
		lMsErroAuto := .F.		
		lIncluiMed	:= .T.
		lEncerra	:= .T.
		
		If(MdJaGerada(cContra, cRevisa, @cPlan, cParcel, @cCompet, @cNum))//Se a medi�ao j� foi gerada
			lIncluiMed	:= .F.
			lEncerra	:= lMedPend
		EndIf
		
		If(lIncluiMed .Or. lEncerra)						
			aCab := {}
			aAdd(aCab,{"CND_CONTRA",cContra,NIL})
			aAdd(aCab,{"CND_REVISA",cRevisa,NIL})
			aAdd(aCab,{"CND_PARCEL",cParcel,NIL})
			aAdd(aCab,{"CND_COMPET",cCompet,NIL})
			aAdd(aCab,{"CND_NUMERO",cPlan,NIL})
			cNum := IIF(lIncluiMed, CriaVar("CND_NUMMED"), cNum)
			aAdd(aCab,{"CND_NUMMED",cNum,NIL})
			
			If(lIncluiMed)//Se deve incluir uma nova medicao
				If lJob
					FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB,STR0003 + " - " + aCab[5,2], 0, _LOGTIME, {})
					FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB,STR0004 + " - " + cContra, 0, _LOGTIME, {})
					FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB,STR0005 + " - " + cPlan, 0, _LOGTIME, {})
					FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB,STR0006 + " - " + aCab[3,2], 0, _LOGTIME, {})
				Else
					IncProc(STR0003 + " - " + aCab[5,2])
				EndIf
				cTxLog += STR0004+" - "+ cContra	 +CHR(13)+CHR(10)
				cTxLog += STR0022+" - "+ aMedicao[6] +CHR(13)+CHR(10)
				cTxLog += STR0005+" - "+ cPlan 		 +CHR(13)+CHR(10)
				
				MSExecAuto({|x,y|CNTA120(x,y,3,.F.)},aCab, aItem)//Executa rotina automatica para gerar as medicoes
				If !lMsErroAuto											
					cTxLog += STR0019+" - "+aCab[6,2]+CHR(13)+CHR(10)//"Medicao gerada com sucesso"
					cTxLog += STR0006+" - "+aCab[3,2]+CHR(13)+CHR(10)						
					If lJob
						FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB,STR0007+aCab[5,2]+STR0008, 0, _LOGTIME, {})
					EndIf
				Else
					aEval(GetAutoGrLog(),{|x|cTxLog += x + CHR(13)+CHR(10), aAdd(aErros,{x})})
					//Retorna controle de numeracao                    					
					While GetSX8Len() > nStack
						RollBackSX8()
					EndDo
					
					lEncerra := .F.
				EndIf										
			EndIf
		
			If lEncerra //Se deve prosseguir p/ encerrar a medicao(nova ou nao)
				If lJob
					FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB,STR0010 + aCab[5,2], 0, _LOGTIME, {})
				Else
					IncProc(STR0010 + aCab[5,2])
				EndIf
																		
				MSExecAuto({|x,y|CNTA120(x,y,6,.F.)},aCab,aItem) //Executa rotina automatica para encerrar as medicoes
				If (!lMsErroAuto)						
					cTxLog += PadC(STR0020 + "["+  aCab[5,2] +"]", 128,"-") +CHR(13)+CHR(10)//"Medicao encerrada com sucesso"						
					If lJob
						FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB,STR0007+aCab[5,2]+STR0011, 0, _LOGTIME, {})
					EndIf
				Else
					cTxLog += PadC(STR0012,128,"-") + CHR(13) + CHR(10)			
					aAdd(aErros,{ STR0004 +": "+ cContra + " "+ STR0006 +": " + cCompet })
					aEval(GetAutoGrLog(),{|x|cTxLog += x + CHR(13)+CHR(10), aAdd(aErros,{x})})
								
					If lJob
						FwLogMsg(_LOGTYPE, , "", _LOGPROG, "", _LOGNUMB,STR0012+aCab[5,2], 0, _LOGTIME, {})
					EndIf
				EndIf
			EndIf			
		EndIf				
	EndIf	
Return Nil

/*/{Protheus.doc} GetCompDt
	Dado <dCompet> retorna a competencia no formato MM/AAAA
@author philipe.pompeu
@since 28/10/2019
@return cCompet, competencia de dCompet
@param dCompet, data
/*/
Static Function GetCompDt(dCompet)
	Local cCompet := ""
	cCompet := StrZero( Month( dCompet ), 2 ) + "/" + CValToChar( Year( dCompet ) )
Return cCompet
