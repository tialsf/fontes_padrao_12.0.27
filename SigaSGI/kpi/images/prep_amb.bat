@ECHO=OFF

@IF %1% EQU RM goto RM
@IF %1% EQU MICROSIGA goto MICROSIGA
@IF %1% EQU LOGOCENTER goto LOGOCENTER
@goto ERRO0

:RM
@ECHO Excluindo os arquivos de Imagens da pasta de imagens do JAR
@ECHO Del "C:\VER811\kpi\kpi-java\src\kpi\imagens\*.*" /y
@ECHO Copiando novos arquivos para a RM
@Copy C:\VER811\kpi\kpi-images\rm\jar\*.* "C:\VER811\kpi\kpi-java\src\kpi\imagens" /y
@Copy C:\VER811\kpi\kpi-images\rm\jar\*.* "C:\VER811\kpi\kpi-java\build\kpi\imagens" /y
@Copy C:\VER811\kpi\kpi-images\rm\site\*.* C:\MP8\sgi_rm\imagens /y
@Copy C:\VER811\kpi\kpi-images\rm\*.* C:\MP8\sgi_rm\ /y

@GOTO FIM

:MICROSIGA
@ECHO Excluindo os arquivos de Imagens da pasta de imagens do JAR
@ECHO Del "C:\VER811\kpi\kpi-java\src\kpi\imagens\*.*" /y
@ECHO Copiando os arquivos para a Microsiga
@Copy C:\VER811\kpi\kpi-images\microsiga\jar\*.*  "C:\VER811\kpi\kpi-java\src\kpi\imagens" /y
@Copy C:\VER811\kpi\kpi-images\microsiga\jar\*.*  "C:\VER811\kpi\kpi-java\build\kpi\imagens" /y
@Copy C:\VER811\kpi\kpi-images\microsiga\site\*.* "C:\MP8\SGI\imagens" /y
@Copy C:\VER811\kpi\kpi-images\microsiga\*.* C:\MP8\sgi\ /y
@GOTO FIM

:LOGOCENTER
@ECHO Excluindo os arquivos de Imagens da pasta de imagens do JAR
@ECHO Del "C:\VER811\kpi\kpi-java\src\kpi\imagens\*.*" /y
@ECHO Copiando os arquivos para a LogoCenter
@Copy C:\VER811\kpi\kpi-images\logocenter\jar\*.* "C:\VER811\kpi\kpi-java\src\kpi\imagens" /y 
@Copy C:\VER811\kpi\kpi-images\logocenter\jar\*.* "C:\VER811\kpi\kpi-java\build\kpi\imagens" /y 
@Copy C:\VER811\kpi\kpi-images\logocenter\site\*.* "C:\MP8\sgi_logocenter\imagens" /y
@Copy C:\VER811\kpi\kpi-images\logocenter\*.* C:\MP8\logocenter\ /y
@GOTO FIM

:ERRO0
@ECHO � necess�rio passar um parametro para c�pia. 
@ECHO Use LOGOCENTER,MICROSIGA ou RM.
@pause

:FIM
@Rem ECHO Copiando arquivos compartilhados.
@Rem Copy "C:\VER811\kpi\kpi-images\shared\jar\*.*" "C:\VER811\kpi\kpi-java\src\kpi\imagens" /y
@Rem Copy "C:\VER811\kpi\kpi-images\shared\jar\*.*" "C:\VER811\kpi\kpi-java\build\kpi\imagens" /y
@Rem Copy "C:\VER811\kpi\kpi-images\shared\site\*.*" "C:\MP8\sgi_rm\imagens" /y
@Echo C�pia Finalizada...
@Pause