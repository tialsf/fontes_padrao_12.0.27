:: @author  Valdiney V GOMES
:: @since   23/07/2012

:: How To

:: O caminho para o diret�rio bin da instala��o do JDK deve ser informado na vari�vel de ambiente do windows PATH.
:: A m�quina utilizada para assinatura do *.jar deve utilizar o idioma Ingl�s (Estados Unidos).
:: Os arquivos *.jar, autojarsigner.bat e totvs.pfx devem estar em um mesmo diret�rio.
:: Executar o arquivo autojarsigner.bat.

@echo off
echo.
echo Informe a senha do certificado totvs.pfx:
echo.
set /p pass=
@echo on

:: keytool -storepass %pass% -list -storetype pkcs12 -keystore totvs.pfx

:: Keystore type: PKCS12
:: Keystore provider: SunJSSE

:: Your keystore contains 1 entry

:: {3d00030a-6f3d-4f03-b611-58f7ec3bd20d}, Set 28, 2018, PrivateKeyEntry,
:: Valid from: Tue Jul 31 21:00:00 BRT 2018 until: Thu Aug 01 20:59:59 BRT 2019
:: Certificate fingerprint (SHA256): FC:01:63:FE:90:4C:F7:DD:5C:21:75:AF:91:D6:84:D4:39:8A:11:1A:CE:BD:1A:CC:8B:6C:95:D7:37:78:26:ED

jarsigner -storepass %pass% -storetype pkcs12 -keystore totvs.pfx -tsa http://timestamp.comodoca.com/rfc3161 -digestalg SHA-256 sgi.jar {3d00030a-6f3d-4f03-b611-58f7ec3bd20d}

pause