/**
 * KpiApplet.java
 *
 * Created on 25 de Novembro de 2003, 15:12
 **/
package kpi.applet;

/**
 *
 * @author  siga1728
 */
public class KpiApplet extends javax.swing.JApplet {

    // Vers�o deste Client Java
    String KPIClientVersion = "2.01.120410";
    // Espa�o para elementos essenciais
    String language;
    String sessionID = "";
    String KPIServerVersion;
    String sysMode = "";
    kpi.applet.KpiLoginPanel loginPanel;
    kpi.applet.KpiMainPanel mainPanel;
    kpi.applet.KpiMainFrame mainFrame;
    public static final java.awt.Font kpiFontPlain = new java.awt.Font("Arial", java.awt.Font.PLAIN, 12);
    public static final java.awt.Font kpiFontBold = new java.awt.Font("Arial", java.awt.Font.PLAIN, 12);
    public static final java.awt.Color kpiColorDisabled = new java.awt.Color(110, 110, 110);
    public static boolean kpiPaineis;
    public static String kpiEntidade;
    public static String kpiId;
    public static String kpiComando;

    /** Creates new form KPIApplet */
    public KpiApplet() {
    }

    @Override
    public void init() {
	sysMode = String.valueOf(getParameter("MODO_ANALISE"));
	language = String.valueOf(getParameter("LANGUAGE"));
        sessionID = String.valueOf(getParameter("SESSIONID"));
        kpiPaineis = (String.valueOf(getParameter("PAINEL")).toLowerCase().equals("false") ? false : true);
        kpiEntidade = String.valueOf(getParameter("ENTIDADE"));
        kpiId = String.valueOf(getParameter("ID"));
        kpiComando = String.valueOf(getParameter("COMANDO"));

        if ("TRUE".equals(String.valueOf(getParameter("DEBUG")))) {
            kpi.core.KpiDebug.setDebug(true);
        } else {
            kpi.core.KpiDebug.setDebug(false);
        }

        /*Seta a linguagem do RPO do Protheus.*/
        kpi.core.KpiStaticReferences.setKpiDefaultLocale(language);

        // KPI SERVER VERSION
        // Cont�m a vers�o do servidor
        kpi.core.KpiStaticReferences.setKpiServerVersion(String.valueOf(getParameter("KPIVERSION")));
        kpi.core.KpiStaticReferences.setKpiExecutionType(kpi.core.KpiStaticReferences.KPI_APPLET);
	kpi.core.KpiStaticReferences.setSystemMode(sysMode);
        KPIServerVersion = kpi.core.KpiStaticReferences.getKpiServerVersion();

        if (!KPIClientVersion.trim().equals(KPIServerVersion.trim())) {
            javax.swing.JOptionPane.showMessageDialog(this,
                    "Cliente e servidor incompat�veis.\nVers�o do Servidor - " + KPIServerVersion + "\nVers�o do Cliente - " + KPIClientVersion,
                    java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiDefaultDialogSystem_00001"),
                    javax.swing.JOptionPane.OK_OPTION);
            this.resetByVersion();
            return;
        }

        // KPI APPLET
        // Cont�m o applet chave do KPI
        kpi.core.KpiStaticReferences.setKpiApplet(this);

        // DATA CONTROLLER
        // Cont�m todo o protocolo de comunica��o do KPI
        kpi.core.KpiStaticReferences.setKpiDataController(new kpi.core.KpiDataController());

        // THEME default
        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
            kpi.core.kpiUIManeger kpiUIManer = new kpi.core.kpiUIManeger();
            kpiUIManer.setFileChooser();

        } catch (Exception e) {
            kpi.core.KpiDebug.println(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiApplet_00001"));
        }
    }

    // Forma padr�o de inicar o mainPanel
    void initMainPanel() {
        try {
            mainPanel = new kpi.applet.KpiMainPanel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Forma padr�o de inicar o kpi em frame
    void initMainFrame() {
        try {
            mainFrame = new kpi.applet.KpiMainFrame(mainPanel);
            mainFrame.setBounds(16, 16, 680, 540);
            mainFrame.setExtendedState(KpiMainFrame.NORMAL);
            mainFrame.validate();
            mainFrame.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Painel principal
        mainPanel.setKpiMainFrame(mainFrame);
    }

    // Forma padr�o de reestartar o kpi
    public void resetByVersion() {
        try {
            this.getAppletContext().showDocument(new java.net.URL(this.getCodeBase().toString()));
        } catch (java.net.MalformedURLException e) {
            e.printStackTrace();
        }
    }

    // Forma padr�o de reestartar o kpi
    public void resetApplet() {
        try {
            kpi.core.KpiStaticReferences.getKpiFormController().closeAll();
            if (mainFrame != null) {
                mainFrame.setVisible(false);
            }
            this.getAppletContext().showDocument(new java.net.URL(this.getCodeBase().toString()));
        } catch (java.net.MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void initLogin() {
        if (loginPanel == null) {
            loginPanel = new kpi.applet.KpiLoginPanel(this, String.valueOf(getParameter("USUARIO")), String.valueOf(getParameter("SENHA")));
        }
        getContentPane().removeAll();
        getContentPane().add(loginPanel, java.awt.BorderLayout.CENTER);
    }

    /** Getter for property sessionID.
     * @return Value of property sessionID.
     *
     */
    public java.lang.String getSessionID() {
        return sessionID;
    }

    /** Setter for property sessionID.
     * @param sessionID New value of property sessionID.
     *
     */
    public void setSessionID(java.lang.String sessionID) {
        this.sessionID = sessionID;
    }
}
