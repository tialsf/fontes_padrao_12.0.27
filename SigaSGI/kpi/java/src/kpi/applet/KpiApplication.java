/*
 * KpiApplication.java
 *
 * Created on 7 de Junho de 2004, 09:39
 */
package kpi.applet;

import javax.swing.ImageIcon;
import kpi.core.KpiDebug;
import kpi.core.KpiStaticReferences;

/**
 * @author  siga1728
 */
public class KpiApplication extends javax.swing.JFrame {

    String language;
    private String codeBase;
    KpiLoginPanel loginPanel;
    KpiMainPanel mainPanel;
    KpiMainFrame mainFrame;
    String sessionID = "";

    /**
     * Construtor
     * @param codeBase
     * @param language
     * @param isDebug 
     */
    public KpiApplication(String codeBase /*URL do SGI*/, String language, boolean isDebug, String analysisMode) {
        setCodeBase(codeBase);
        //Define se o sistema est� rodando no modo aplication.
        KpiStaticReferences.setKpiExecutionType(kpi.core.KpiStaticReferences.KPI_APLICATION);
        initComponents();
        //Define o protocolo de comunica��o do KPI.
        KpiStaticReferences.setKpiApplication(this);
        KpiStaticReferences.setKpiDataController(new kpi.core.KpiDataController());
        //Define o valor do session ID.
        setSessionID(KpiStaticReferences.getKpiDataController().createSession());
        //Define a linguagem de utiliza��o do SGI.
        KpiStaticReferences.setKpiDefaultLocale(language);
        //Define como o SGI ser� utilizado (PDCA ou BSC)
        KpiStaticReferences.setSystemMode(analysisMode);
        //Define se a aplica��o ser� executada em modo de DEBUG.
        KpiDebug.setDebug(isDebug);

        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
            kpi.core.kpiUIManeger kpiUIManer = new kpi.core.kpiUIManeger();
            kpiUIManer.setFileChooser();
        } catch (Exception e) {
            kpi.core.KpiDebug.println(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiApplet_00001"));
        }

        javax.swing.JFrame.setDefaultLookAndFeelDecorated(true);
        javax.swing.JDialog.setDefaultLookAndFeelDecorated(true);
        javax.swing.SwingUtilities.updateComponentTreeUI(this);

        initLogin();
    }

    /**
     * 
     * @param args 
     */
    public static void main(String args[]) {
        //Para DEBUG informar a URL do SGI.
        String httpServer = (args.length > 0) ? args[0] : "http://bi-valdiney:8080/sgi/";
        //Para DEBUG informar o Idioma do RPO.
        String language = (args.length > 1) ? args[1] : "PORTUGUESE";
        //Para DEBUG informar se devem ser exibidas mensagens no console do Java.
        boolean isDebug = (args.length > 2) ? args[2].equalsIgnoreCase("T") : false;
        //Modo de utiliza��o do SGI
        String analysisMode = (args.length > 3) ? args[3] : "1";

        KpiApplication application = new KpiApplication(httpServer, language, isDebug, analysisMode);
        application.setVisible(true);
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiApplication_00001")); // NOI18N
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                exitForm(evt);
            }
        });

        pack();
    }//GEN-END:initComponents

	private void exitForm(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_exitForm
            System.exit(0);
	}//GEN-LAST:event_exitForm

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    public void initLogin() {
        if (loginPanel == null) {
            loginPanel = new kpi.applet.KpiLoginPanel(this);
        }
        getContentPane().removeAll();

        this.setIconImage(new ImageIcon(getClass().getResource("/kpi/imagens/totvs.png")).getImage());

        setSize(657, 472);
        java.awt.Dimension tela = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((tela.width - this.getSize().width) / 2,
                (tela.height - this.getSize().height) / 2);

        getContentPane().add(loginPanel, java.awt.BorderLayout.CENTER);
    }

    /*Forma padr�o de inicar o mainPanel*/
    void initMainPanel() {
        try {
            mainPanel = new kpi.applet.KpiMainPanel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*Forma padr�o de inicar o kpi em frame*/
    void initMainFrame() {
        try {
            mainFrame = new kpi.applet.KpiMainFrame(mainPanel);
            mainFrame.setBounds(16, 16, 680, 540);
            mainFrame.setExtendedState(MAXIMIZED_BOTH);
            mainFrame.validate();
            mainFrame.setVisible(true);
            mainFrame.repaint();
        } catch (Exception e) {
            e.printStackTrace();
        }

        mainPanel.setKpiMainFrame(mainFrame);
    }

    /*Forma padr�o de reestartar o kpi*/
    public void resetApplication() {
        kpi.core.KpiStaticReferences.getKpiFormController().closeAll();
        if (mainFrame != null) {
            mainFrame.setVisible(false);

            /*Finaliza a execu��o da aplica��o.*/
            System.exit(0);
        }
    }

    /*Getters e Setters*/
    public java.lang.String getSessionID() {
        return sessionID;
    }

    public void setSessionID(java.lang.String sessionID) {
        this.sessionID = sessionID;
    }

    public String getCodeBase() {
        return codeBase;
    }

    public void setCodeBase(String codeBase) {
        this.codeBase = codeBase;
    }
}
