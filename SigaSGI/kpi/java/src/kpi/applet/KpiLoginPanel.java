/*
 * JBILoginPanel.java
 *
 * Created on May 27, 2003, 4:37 PM
 */
package kpi.applet;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import kpi.beans.JTexturePanel;
import kpi.core.KpiStaticReferences;
import kpi.util.KpiToolKit;
import pv.jfcx.JPVPassword;

/**
 * @author  siga1996
 */
public class KpiLoginPanel extends kpi.beans.JTexturePanel {

    kpi.applet.KpiApplet kpiApplet;
    kpi.applet.KpiApplication kpiApllication;

    public KpiLoginPanel(kpi.applet.KpiApplet kpiApplet, String usuario, String senha) {
        initComponents();
        this.kpiApplet = kpiApplet;

        txtUsuario.setText(new String(usuario));
        txtSenha.setText(new String(senha));
    }

    public KpiLoginPanel(kpi.applet.KpiApplication kpiApllication) {
        initComponents();
        this.kpiApllication = kpiApllication;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new JPanel();
        jPnlLogin = new JTexturePanel();
        cmdEnviar = new JButton();
        cmdLimpar = new JButton();
        lblBemVindo = new JLabel();
        lblMsgLogin = new JLabel();
        lblUsuario = new JLabel();
        lblSenha = new JLabel();
        lblMsgErro = new JLabel();
        lblTrocaSenha = new JLabel();
        txtSenha = new JPVPassword();
        txtUsuario = new JTextField();

        setBackground(new Color(255, 255, 255));
        setMaximumSize(new Dimension(676, 373));
        setMinimumSize(new Dimension(676, 373));
        setPreferredSize(new Dimension(676, 373));
        setLayout(new BorderLayout());

        jPanel1.setBackground(new Color(255, 255, 255));
        jPanel1.setPreferredSize(new Dimension(657, 472));
        jPanel1.setLayout(null);

        jPnlLogin.setImageTexture(new ImageIcon(getClass().getResource("/kpi/imagens/kpiwebstart.jpg"))); // NOI18N
        jPnlLogin.setLayout(null);

        cmdEnviar.setBackground(new Color(202, 202, 202));
        cmdEnviar.setFont(new Font("Dialog", 0, 10)); // NOI18N
        ResourceBundle bundle = ResourceBundle.getBundle("international"); // NOI18N
        cmdEnviar.setText(bundle.getString("KpiLoginPanel_00011")); // NOI18N
        cmdEnviar.setToolTipText("");
        cmdEnviar.setBorder(null);
        cmdEnviar.setMargin(new Insets(0, 0, 0, 0));
        cmdEnviar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                cmdEnviarActionPerformed(evt);
            }
        });
        jPnlLogin.add(cmdEnviar);
        cmdEnviar.setBounds(70, 310, 70, 22);

        cmdLimpar.setBackground(new Color(202, 202, 202));
        cmdLimpar.setFont(new Font("Dialog", 0, 10)); // NOI18N
        cmdLimpar.setText(bundle.getString("KpiLoginPanel_00012")); // NOI18N
        cmdLimpar.setToolTipText("");
        cmdLimpar.setBorder(null);
        cmdLimpar.setMargin(new Insets(0, 0, 0, 0));
        cmdLimpar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                cmdLimparActionPerformed(evt);
            }
        });
        jPnlLogin.add(cmdLimpar);
        cmdLimpar.setBounds(150, 310, 70, 22);

        lblBemVindo.setFont(new Font("Calibri", 1, 30)); // NOI18N
        lblBemVindo.setForeground(new Color(13, 46, 112));
        lblBemVindo.setHorizontalAlignment(SwingConstants.CENTER);
        lblBemVindo.setText(bundle.getString("KpiLoginPanel_00007")); // NOI18N
        lblBemVindo.setHorizontalTextPosition(SwingConstants.CENTER);
        jPnlLogin.add(lblBemVindo);
        lblBemVindo.setBounds(10, 100, 290, 40);

        lblMsgLogin.setFont(new Font("Verdana", 0, 11)); // NOI18N
        lblMsgLogin.setForeground(new Color(13, 46, 112));
        lblMsgLogin.setHorizontalAlignment(SwingConstants.LEFT);
        lblMsgLogin.setText(bundle.getString("KpiLoginPanel_00008")); // NOI18N
        jPnlLogin.add(lblMsgLogin);
        lblMsgLogin.setBounds(60, 140, 270, 50);

        lblUsuario.setHorizontalAlignment(SwingConstants.LEFT);
        lblUsuario.setText(bundle.getString("KpiLoginPanel_00009")); // NOI18N
        lblUsuario.setFocusable(false);
        lblUsuario.setHorizontalTextPosition(SwingConstants.CENTER);
        jPnlLogin.add(lblUsuario);
        lblUsuario.setBounds(60, 190, 60, 20);

        lblSenha.setHorizontalAlignment(SwingConstants.LEFT);
        lblSenha.setText(bundle.getString("KpiLoginPanel_00010")); // NOI18N
        lblSenha.setFocusable(false);
        lblSenha.setHorizontalTextPosition(SwingConstants.CENTER);
        jPnlLogin.add(lblSenha);
        lblSenha.setBounds(60, 230, 60, 20);

        lblMsgErro.setForeground(new Color(255, 51, 51));
        lblMsgErro.setHorizontalAlignment(SwingConstants.CENTER);
        lblMsgErro.setText(bundle.getString("KpiLoginPanel_00005")); // NOI18N
        lblMsgErro.setVisible(false);
        jPnlLogin.add(lblMsgErro);
        lblMsgErro.setBounds(50, 340, 190, 30);

        lblTrocaSenha.setFont(new Font("Verdana", 0, 10)); // NOI18N
        lblTrocaSenha.setForeground(new Color(0, 51, 255));
        lblTrocaSenha.setHorizontalAlignment(SwingConstants.CENTER);
        lblTrocaSenha.setText("<html><u>" + ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiLoginPanel_00013") + "</u></html>");
        lblTrocaSenha.setCursor(new Cursor(Cursor.HAND_CURSOR));
        lblTrocaSenha.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                lblTrocaSenhaMouseClicked(evt);
            }
        });
        jPnlLogin.add(lblTrocaSenha);
        lblTrocaSenha.setBounds(60, 280, 170, 20);

        txtSenha.setBounds(new Rectangle(60, 250, 190, 22));
        txtSenha.setCase(0);
        txtSenha.setFont(new Font("Tahoma", 1, 11)); // NOI18N
        txtSenha.setMaxLength(20);
        jPnlLogin.add(txtSenha);
        txtSenha.setBounds(60, 250, 190, 22);

        txtUsuario.setFont(new Font("Tahoma", 1, 11)); // NOI18N
        jPnlLogin.add(txtUsuario);
        txtUsuario.setBounds(60, 210, 190, 22);

        jPanel1.add(jPnlLogin);
        jPnlLogin.setBounds(0, 0, 660, 470);

        add(jPanel1, BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

	private void cmdLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdLimparActionPerformed
            txtUsuario.setText("");
            txtSenha.setText("");
            lblMsgErro.setVisible(false);
	}//GEN-LAST:event_cmdLimparActionPerformed

	private void cmdEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdEnviarActionPerformed
            //Desabilita o bot�o enviar.
            cmdEnviar.setEnabled(false);

            String resposta = kpi.core.KpiStaticReferences.getKpiDataController().doLogin(txtUsuario.getText(), txtSenha.getText());

            //Acesso negado
            if (resposta.startsWith("<!DENIED -->")) {
                //Exibe a mensagem de erro.
                lblMsgErro.setVisible(true);
                //Habilita o bot�o enviar.
                cmdEnviar.setEnabled(true);

            } else {

                //Login realizado pelo Desktop.
                if (kpi.core.KpiStaticReferences.getKpiExecutionType() == kpi.core.KpiStaticReferences.KPI_APLICATION) {
                    kpiApllication.initMainPanel();
                    kpiApllication.initMainFrame();

                    kpiApllication.setVisible(false);

                    //Login realizado pelo Applet.
                } else {
                    kpiApplet.initMainPanel();
                    kpiApplet.initMainFrame();
                }
            }
	}//GEN-LAST:event_cmdEnviarActionPerformed

        private void lblTrocaSenhaMouseClicked(MouseEvent evt) {//GEN-FIRST:event_lblTrocaSenhaMouseClicked

            /*Requisita a URL para troca de senha.*/
            KpiToolKit oToolKit = new KpiToolKit();
            oToolKit.browser("h_bibyForgotPwd.apw");
        }//GEN-LAST:event_lblTrocaSenhaMouseClicked
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton cmdEnviar;
    private JButton cmdLimpar;
    private JPanel jPanel1;
    private JTexturePanel jPnlLogin;
    private JLabel lblBemVindo;
    private JLabel lblMsgErro;
    private JLabel lblMsgLogin;
    private JLabel lblSenha;
    private JLabel lblTrocaSenha;
    private JLabel lblUsuario;
    private JPVPassword txtSenha;
    private JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables
}
