/*
 * JPanel.java
 *
 * Created on 11 de Novembro de 2003, 14:51
 */
package kpi.applet;

import kpi.core.KpiFormController;
import kpi.xml.*;
import javax.swing.tree.*;
import java.util.*;
import kpi.core.KpiCustomLabels;
import kpi.core.KpiStaticReferences;
import kpi.swing.framework.KpiListaPlanos;
import kpi.swing.framework.KpiListaProjeto;
import kpi.util.KpiToolKit;

/**
 * @author Alexandre Silva
 */
public class KpiMainPanel extends javax.swing.JPanel {

    // temas
    public static final int THEME_DEFAULT = 0;
    // constantes de condi��es da status bar
    public static final int ST_IDLE = 0;
    public static final int ST_CONNECTING = 1;
    //constantes que identificam o tipo da arvore
    public static final String ST_CADASTRO = "CADASTRO"; //NOI18N
    public static final String ST_CONFIGURACAO = "CONFIGURACAO"; //NOI18N
    //constantes que identificam o status do calculo
    public static final int WORKST_RED = 2;
    public static final int WORKST_YELLOW = 1;
    public static final int WORKST_GREEN = 0;
    // fields
    int theme = THEME_DEFAULT;
    private kpi.swing.KpiTree kpiTree;
    KpiApplet kpiApplet;  
    KpiApplication kpiApplication;
    kpi.applet.KpiMainFrame kpiMainFrame;
    kpi.core.KpiImageResources kpiImageResources;
    kpi.swing.KpiDefaultDialogSystem kpiDialogSystem;
    kpi.core.KpiDataController kpiDataController;
    public kpi.core.KpiFormController kpiFormControllerConfiguracao;
    public kpi.core.KpiFormController kpiFormControllerCadastro;
    //Classe para cria��o das arvores
    private kpi.swing.kpiCreateTree buildTree = new kpi.swing.kpiCreateTree();
    private kpi.xml.BIXMLRecord kpiXmlTreeSelected; //Armazena o valor quando requisito a �rvore de desktop.
    private kpi.core.KpiMenuController mnuController = new kpi.core.KpiMenuController();
    private String desktopSelected = ""; //NOI18N
    //Para a chamada do lembrete ao iniciar o kpi
    private boolean lShowLembrete = true;
    //Controla o status exibido na barra de ferramentas
    private KpiRefreshStatus oRefreshStatus = new KpiRefreshStatus();

    /** Creates new form JPanel */
    public KpiMainPanel() {
	initComponents();
	pnlBarra.add(jMnuBarScoredcard, java.awt.BorderLayout.EAST);


	setStatusBarCondition(ST_IDLE);

	// Iniciando recursos principais
	kpiImageResources = new kpi.core.KpiImageResources();
	kpiDialogSystem = new kpi.swing.KpiDefaultDialogSystem(this);

	// Form Controller
	kpiFormControllerCadastro = new kpi.core.KpiFormController(this, jDesktopCadastro);
	kpiFormControllerConfiguracao = new kpi.core.KpiFormController(this, jDesktopConfiguracao);

	//Referencia estatica dos objetos
	kpiApplet = KpiStaticReferences.getKpiApplet();
	kpiApplication = KpiStaticReferences.getKpiApplication();
	kpiDataController = kpi.core.KpiStaticReferences.getKpiDataController();
	kpi.core.KpiStaticReferences.setKpiImageResources(kpiImageResources);
	kpi.core.KpiStaticReferences.setKpiDialogSystem(kpiDialogSystem);
	kpi.core.KpiStaticReferences.setKpiMainPanel(this);
	kpi.core.KpiStaticReferences.setKpiDateBase(new kpi.core.KpiDateBase());


	kpiTreeConfiguracao.removeAll();
	kpiTreeConfiguracao.setCellRenderer(new kpi.swing.KpiTreeCellRenderer(kpiImageResources));


	setMainPanelProperties();
	showCadastro();

	if (KpiApplet.kpiPaineis) {
	    try {
		kpi.swing.KpiDefaultFrameFunctions frame =
			kpi.core.KpiStaticReferences.getKpiFormController().getForm(KpiApplet.kpiEntidade, KpiApplet.kpiId, ""); //NOI18N
		frame.asJInternalFrame().setMaximizable(true);
	    } catch (kpi.core.KpiFormControllerException e) {
		kpi.core.KpiDebug.println(e.getMessage());
	    }
	}

	//Inicia a Thread para atualizar o status do calculo na barra de ferramentas
	oRefreshStatus.startRefreshUpdate(30000);
    }

    public kpi.core.KpiFormController getKpiFormController() {
	return kpi.core.KpiStaticReferences.getKpiFormController();
    }

    public KpiApplet getKpiApplet() {
	return kpiApplet;
    }

    public KpiApplication getKpiApplication() {
	return kpiApplication;
    }

    public void setKpiMainFrame(KpiMainFrame kpiMainFrame) {
	this.kpiMainFrame = kpiMainFrame;
    }

    public KpiMainFrame getKpiMainFrame() {
	return kpiMainFrame;
    }

    public void showConfiguracao() {
	java.awt.CardLayout cl = (java.awt.CardLayout) (jPanelCentral.getLayout());
	cl.show(jPanelCentral, ST_CONFIGURACAO);
	kpi.core.KpiStaticReferences.setKpiFormController(kpiFormControllerConfiguracao);
	setKpiTree((kpi.swing.KpiTree) kpiTreeConfiguracao);
	//if (getKpiTree().getAnchorSelectionPath() == null) {
	loadTree(ST_CONFIGURACAO);
	getKpiTree().setKpiTreeType(ST_CONFIGURACAO);
	getKpiTree().expandRow(0);
	//}
	alterDesktop(kpiFormControllerConfiguracao, ST_CONFIGURACAO);
    }

    public void showCadastro() {
	java.awt.CardLayout cl = (java.awt.CardLayout) (jPanelCentral.getLayout());
	cl.show(jPanelCentral, ST_CADASTRO);
	kpi.core.KpiStaticReferences.setKpiFormController(kpiFormControllerCadastro);
	alterDesktop(kpiFormControllerCadastro, ST_CADASTRO);
    }

    public void updateTree(BIXMLRecord treeXML, javax.swing.JTree actualKpiTree) {
	// Vetores que armazenar�o os n�s vis�veis e selecionados.
	java.util.HashMap visible = new java.util.HashMap();
	java.util.HashMap selected = new java.util.HashMap();

	// Verifica quais n�s s�o atualmente vis�veis.
	TreePath path = null;
	DefaultMutableTreeNode node = null;
	kpi.swing.KpiTreeNode myNode = null;

	for (int i = 0; i < actualKpiTree.getRowCount(); i++) {
	    path = actualKpiTree.getPathForRow(i);
	    node = (DefaultMutableTreeNode) path.getLastPathComponent();
	    myNode = (kpi.swing.KpiTreeNode) node.getUserObject();
	    visible.put(myNode.getTreeID(), myNode);
	}

	TreePath[] paths = actualKpiTree.getSelectionPaths();

	if (paths != null) {
	    for (int i = 0; i < paths.length; i++) {
		node = (DefaultMutableTreeNode) paths[i].getLastPathComponent();
		myNode = (kpi.swing.KpiTreeNode) node.getUserObject();
		selected.put(myNode.getTreeID(), myNode);
	    }
	}

	DefaultMutableTreeNode root = createTree(treeXML);

	try {
	    actualKpiTree.setModel(new DefaultTreeModel(root));
	} catch (Exception e) {
	    actualKpiTree.setModel(new DefaultTreeModel(
		    new DefaultMutableTreeNode(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00001"))));
	    throw new RuntimeException(e.getMessage());
	}

	actualKpiTree.collapseRow(0);
	updateTreeStatus(visible, selected, root, actualKpiTree);

    }

    public void updateTreeStatus(java.util.HashMap visible, java.util.HashMap selected,
	    DefaultMutableTreeNode root, javax.swing.JTree actualKpiTree) {
	kpi.swing.KpiTreeNode node = (kpi.swing.KpiTreeNode) root.getUserObject();
	kpi.swing.KpiTreeNode compNode;

	TreeNode[] path = root.getPath();

	if (visible.containsKey(node.getTreeID())) {
	    compNode = (kpi.swing.KpiTreeNode) visible.get(node.getTreeID());
	    if (compNode.equals(node)) {
		actualKpiTree.makeVisible(new TreePath(path));
	    }
	}

	if (selected.containsKey(node.getTreeID())) {
	    compNode = (kpi.swing.KpiTreeNode) selected.get(node.getTreeID());
	    if (compNode.equals(node)) {
		actualKpiTree.addSelectionPath(new TreePath(path));
	    }
	}

	for (int i = 0; i < root.getChildCount(); i++) {
	    updateTreeStatus(visible, selected,
		    (DefaultMutableTreeNode) root.getChildAt(i), actualKpiTree);
	}
    }

    public void updateTreeStatus(java.util.HashMap visible, java.util.HashMap selected,
	    DefaultMutableTreeNode root) {
	kpi.swing.KpiTreeNode node = (kpi.swing.KpiTreeNode) root.getUserObject();

	TreeNode[] path = root.getPath();

	if (visible.containsKey(node.getTreeID())) {
	    getKpiTree().makeVisible(new TreePath(path));
	}

	if (selected.containsKey(node.getTreeID())) {
	    getKpiTree().addSelectionPath(new TreePath(path));
	}

	for (int i = 0; i < root.getChildCount(); i++) {
	    updateTreeStatus(visible, selected,
		    (DefaultMutableTreeNode) root.getChildAt(i));
	}
    }

    public void loadTree(String type) {
	getKpiTree().setModel(new DefaultTreeModel(new DefaultMutableTreeNode(
		java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00002"))));
	final String localType = String.valueOf(type);
	javax.swing.JTree threadTree = null;

	if (type.equals(ST_CONFIGURACAO)) {
	    threadTree = kpiTreeConfiguracao;
	}

	final kpi.core.KpiSwingWorker worker = new kpi.core.KpiSwingWorker((kpi.swing.KpiTree) threadTree) {

	    //long inicio = System.currentTimeMillis();
	    @Override
	    public Object construct() {
		setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
		return kpiDataController.loadTree(localType);
	    }

	    @Override
	    public void finished() {
		DefaultMutableTreeNode root = null;
		if (kpiDataController.getStatus() == kpi.core.KpiDataController.KPI_ST_OK) {
		    try {
			kpiXmlTreeSelected = (kpi.xml.BIXMLRecord) get();
			jLabelUsuario.setText(kpiXmlTreeSelected.getAttributes().getString("USUARIO") + "  "); //NOI18N

			getThreadTree().setModel(new DefaultTreeModel(createTree(kpiXmlTreeSelected)));
		    } catch (Exception e) {
			getKpiTree().setModel(new DefaultTreeModel(new DefaultMutableTreeNode(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00003"))));
			throw new RuntimeException(e.getMessage());
		    }
		} else {
		    if (kpiDataController.getStatus() == kpi.core.KpiDataController.KPI_ST_GENERALERROR) {
			kpiDialogSystem.errorMessage(kpiDataController.getErrorMessage());
		    } else {
			kpiDialogSystem.errorMessage(kpiDataController.getStatus());
			if (localType.equals("USUARIO") && kpiDataController.getStatus() == kpi.core.KpiDataController.KPI_ST_NORIGHTS) { //NOI18N
			    showCadastro();
			}
		    }
		}
		setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
	    }
	};
	worker.start();
    }

    public DefaultMutableTreeNode createTree(BIXMLRecord treeXML) {
	return buildTree.createTree(treeXML);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grpView = new javax.swing.ButtonGroup();
        jMnuBarScoredcard = new javax.swing.JMenuBar();
        jMnuCadastro = new javax.swing.JMenu();
        jMnuFormMatrix = new javax.swing.JMenuItem();
        mnuController.addHashMenus("FORMMATRIX", jMnuFormMatrix);
        jMnuScorecard = new javax.swing.JMenuItem();
        mnuController.addHashMenus("SCORECARDS", jMnuScorecard);
        jMnuUnidade = new javax.swing.JMenuItem();
        mnuController.addHashMenus("UNIDADES", jMnuUnidade);
        jMnuMetaFormula = new javax.swing.JMenuItem();
        mnuController.addHashMenus("METAFORMULAS", jMnuMetaFormula);
        jMnuGrupoIndicador = new javax.swing.JMenuItem();
        mnuController.addHashMenus("GRUPO_INDS", jMnuGrupoIndicador);
        jMnuIndicador = new javax.swing.JMenuItem();
        mnuController.addHashMenus("INDICADORES", jMnuIndicador);
        jMnuGrupoAcao = new javax.swing.JMenuItem();
        mnuController.addHashMenus("GRUPO_ACAO", jMnuGrupoAcao);
        jMnuPlanoDeAcao = new javax.swing.JMenuItem();
        mnuController.addHashMenus("PLANOSACAO", jMnuPlanoDeAcao);
        jMnuProjetos = new javax.swing.JMenuItem();
        mnuController.addHashMenus("PROJETOS", jMnuProjetos);
        jMnuPacote = new javax.swing.JMenuItem();
        mnuController.addHashMenus("PACOTES", jMnuPacote);
        jMnuEspPeixe = new javax.swing.JMenuItem();
        mnuController.addHashMenus("ESP_PEIXE", jMnuEspPeixe);
        jMnuConsulta = new javax.swing.JMenu();
        jMnuScoreCarding = new javax.swing.JMenuItem();
        mnuController.addHashMenus("PAINELSCOR", jMnuScoreCarding);
        jMnuPaineis = new javax.swing.JMenuItem();
        mnuController.addHashMenus("PAINEIS",jMnuPaineis);
        jMnuPaineisComp = new javax.swing.JMenuItem();
        mnuController.addHashMenus("PAINEISCOMP",jMnuPaineisComp);
        jMnuLembrete = new javax.swing.JMenuItem();
        jMnuStatusPlanos = new javax.swing.JMenuItem();
        mnuController.addHashMenus("LISTA_STATUS_ACAO",jMnuStatusPlanos);
        jMnuRelatorios = new javax.swing.JMenu();
        mnuController.addHashMenus("RELATORIOS", jMnuRelatorios);
        jMnuApresentacao = new javax.swing.JMenuItem();
        mnuController.addHashMenus("RELAPRS", jMnuApresentacao);
        jMnuRelPlano = new javax.swing.JMenuItem();
        jMnuRelScoreInd = new javax.swing.JMenuItem();
        jMnuRelEstatistica = new javax.swing.JMenuItem();
        jMnuRelBook = new javax.swing.JMenuItem();
        mnuController.addHashMenus("RELBOOK", jMnuRelBook);
        jMnuRelInd = new javax.swing.JMenuItem();
        mnuController.addHashMenus("RELIND", jMnuRelInd);
        jMnuConfiguracao = new javax.swing.JMenu();
        jMnuExibir = new javax.swing.JMenu();
        radJanela = new javax.swing.JRadioButtonMenuItem();
        radPasta = new javax.swing.JRadioButtonMenuItem();
        jMnuAjuda = new javax.swing.JMenu();
        jMnuConteudo = new javax.swing.JMenuItem();
        jMnuSobre = new javax.swing.JMenuItem();
        jMnuLogout = new javax.swing.JMenu();
        jPanelFirst = new javax.swing.JPanel();
        jPanelCentral = new javax.swing.JPanel();
        jSplitPaneCadastro = new javax.swing.JSplitPane();
        jDesktopCadastro = new kpi.beans.JBIDesktopPane();
        jSplitPaneConfiguracao = new javax.swing.JSplitPane();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTexturePanel3 = new kpi.beans.JTexturePanel();
        kpiTreeConfiguracao =  new kpi.swing.KpiTree();
        jPanelAtualiza = new javax.swing.JPanel();
        lblAtualiza = new javax.swing.JLabel();
        jDesktopConfiguracao = new kpi.beans.JBIDesktopPane();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jTexturePanelArt1 = new kpi.beans.JTexturePanel();
        jTexturePanelLeft = new kpi.beans.JTexturePanel();
        jPanelStatus = new javax.swing.JPanel();
        jLabelStatusIcone = new javax.swing.JLabel();
        jLabelStatusGauge = new javax.swing.JLabel();
        jLabelStatusCalc = new javax.swing.JLabel();
        jTexturePanelArt2 = new kpi.beans.JTexturePanel();
        pnlBarra = new javax.swing.JPanel();
        pnlUser = new javax.swing.JPanel();
        pnlUserLeft = new javax.swing.JPanel();
        jLabelUsuario = new javax.swing.JLabel();
        pnlUserRight = new javax.swing.JPanel();

        jMnuBarScoredcard.setBackground(new java.awt.Color(20, 65, 84));
        jMnuBarScoredcard.setBorder(null);
        jMnuBarScoredcard.setForeground(new java.awt.Color(20, 65, 84));
        jMnuBarScoredcard.setBorderPainted(false);

        jMnuCadastro.setBackground(new java.awt.Color(20, 65, 84));
        jMnuCadastro.setBorder(null);
        jMnuCadastro.setForeground(new java.awt.Color(20, 65, 84));
        jMnuCadastro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_documento.gif"))); // NOI18N
        jMnuCadastro.setMnemonic('C');
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        jMnuCadastro.setToolTipText(bundle.getString("KpiMainPanel_00004")); // NOI18N
        jMnuCadastro.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jMnuCadastro.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jMnuCadastro.setIconTextGap(0);
        jMnuCadastro.setMaximumSize(new java.awt.Dimension(35, 60));
        jMnuCadastro.setPreferredSize(new java.awt.Dimension(35, 60));
        jMnuCadastro.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMnuCadastroMouseClicked(evt);
            }
        });

        jMnuFormMatrix.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_H, java.awt.event.InputEvent.CTRL_MASK));
        jMnuFormMatrix.setBackground(new java.awt.Color(20, 65, 84));
        jMnuFormMatrix.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_arvore.gif"))); // NOI18N
        jMnuFormMatrix.setText(bundle.getString("KpiMainPanel_00041")); // NOI18N
        jMnuFormMatrix.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuFormMatrixActionPerformed(evt);
            }
        });
        jMnuCadastro.add(jMnuFormMatrix);

        jMnuScorecard.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMnuScorecard.setBackground(new java.awt.Color(20, 65, 84));
        jMnuScorecard.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_scorecard.gif"))); // NOI18N
        jMnuScorecard.setMnemonic('S');
        jMnuScorecard.setText(bundle.getString("KpiMainPanel_00017")); // NOI18N
        jMnuScorecard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuScorecardActionPerformed(evt);
            }
        });
        jMnuCadastro.add(jMnuScorecard);

        jMnuUnidade.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.CTRL_MASK));
        jMnuUnidade.setBackground(new java.awt.Color(20, 65, 84));
        jMnuUnidade.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_unidade.gif"))); // NOI18N
        jMnuUnidade.setMnemonic('U');
        jMnuUnidade.setText(bundle.getString("KpiMainPanel_00032")); // NOI18N
        jMnuUnidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuUnidadeActionPerformed(evt);
            }
        });
        jMnuCadastro.add(jMnuUnidade);

        jMnuMetaFormula.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        jMnuMetaFormula.setBackground(new java.awt.Color(20, 65, 84));
        jMnuMetaFormula.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_meta_formula.gif"))); // NOI18N
        jMnuMetaFormula.setMnemonic('M');
        jMnuMetaFormula.setText(bundle.getString("KpiMainPanel_00021")); // NOI18N
        jMnuMetaFormula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuMetaFormulaActionPerformed(evt);
            }
        });
        jMnuCadastro.add(jMnuMetaFormula);

        jMnuGrupoIndicador.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.CTRL_MASK));
        jMnuGrupoIndicador.setBackground(new java.awt.Color(20, 65, 84));
        jMnuGrupoIndicador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_grupoindicador.gif"))); // NOI18N
        jMnuGrupoIndicador.setMnemonic('G');
        jMnuGrupoIndicador.setText(bundle.getString("KpiMainPanel_00018")); // NOI18N
        jMnuGrupoIndicador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuGrupoIndicadorActionPerformed(evt);
            }
        });
        jMnuCadastro.add(jMnuGrupoIndicador);

        jMnuIndicador.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.CTRL_MASK));
        jMnuIndicador.setBackground(new java.awt.Color(20, 65, 84));
        jMnuIndicador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_indicador.gif"))); // NOI18N
        jMnuIndicador.setMnemonic('I');
        jMnuIndicador.setText(bundle.getString("KpiMainPanel_00019")); // NOI18N
        jMnuIndicador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuIndicadorActionPerformed(evt);
            }
        });
        jMnuCadastro.add(jMnuIndicador);

        jMnuGrupoAcao.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        jMnuGrupoAcao.setBackground(new java.awt.Color(20, 65, 84));
        jMnuGrupoAcao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_grupoacao.gif"))); // NOI18N
        jMnuGrupoAcao.setText(bundle.getString("KpiMainPanel_00035")); // NOI18N
        jMnuGrupoAcao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuGrupoAcaoActionPerformed(evt);
            }
        });
        jMnuCadastro.add(jMnuGrupoAcao);

        jMnuPlanoDeAcao.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        jMnuPlanoDeAcao.setBackground(new java.awt.Color(20, 65, 84));
        jMnuPlanoDeAcao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_planodeacao.gif"))); // NOI18N
        jMnuPlanoDeAcao.setMnemonic('l');
        jMnuPlanoDeAcao.setText(bundle.getString("KpiPlanoDeAcao_00001")); // NOI18N
        jMnuPlanoDeAcao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuPlanoDeAcaoActionPerformed(evt);
            }
        });
        jMnuCadastro.add(jMnuPlanoDeAcao);

        jMnuProjetos.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        jMnuProjetos.setBackground(new java.awt.Color(20, 65, 84));
        jMnuProjetos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_projeto.gif"))); // NOI18N
        jMnuProjetos.setMnemonic('P');
        jMnuProjetos.setText(bundle.getString("KpiMainPanel_00020")); // NOI18N
        jMnuProjetos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuProjetosActionPerformed(evt);
            }
        });
        jMnuCadastro.add(jMnuProjetos);

        jMnuPacote.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_1, java.awt.event.InputEvent.CTRL_MASK));
        jMnuPacote.setBackground(new java.awt.Color(20, 65, 84));
        jMnuPacote.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_pacote.gif"))); // NOI18N
        jMnuPacote.setText(bundle.getString("KpiListaPacote_00001")); // NOI18N
        jMnuPacote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuPacoteActionPerformed(evt);
            }
        });
        jMnuCadastro.add(jMnuPacote);

        jMnuEspPeixe.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jMnuEspPeixe.setBackground(new java.awt.Color(20, 65, 84));
        jMnuEspPeixe.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_epinhapeixe.gif"))); // NOI18N
        jMnuEspPeixe.setText(bundle.getString("KpiEspinhaPeixeLista_00001")); // NOI18N
        jMnuEspPeixe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuEspPeixeActionPerformed(evt);
            }
        });
        jMnuCadastro.add(jMnuEspPeixe);

        jMnuBarScoredcard.add(jMnuCadastro);

        jMnuConsulta.setBackground(new java.awt.Color(20, 65, 84));
        jMnuConsulta.setForeground(new java.awt.Color(20, 65, 84));
        jMnuConsulta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_scorecarding.gif"))); // NOI18N
        jMnuConsulta.setMnemonic('o');
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international"); // NOI18N
        jMnuConsulta.setToolTipText(bundle1.getString("KpiMainPanel_00014")); // NOI18N
        jMnuConsulta.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jMnuConsulta.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jMnuConsulta.setIconTextGap(0);
        jMnuConsulta.setMaximumSize(new java.awt.Dimension(35, 60));
        jMnuConsulta.setPreferredSize(new java.awt.Dimension(35, 60));
        jMnuConsulta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMnuConsultaMouseClicked(evt);
            }
        });

        jMnuScoreCarding.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        jMnuScoreCarding.setBackground(new java.awt.Color(20, 65, 84));
        jMnuScoreCarding.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_scorecarding.gif"))); // NOI18N
        jMnuScoreCarding.setMnemonic('S');
        jMnuScoreCarding.setText(bundle.getString("KpiMainPanel_00023")); // NOI18N
        jMnuScoreCarding.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuScoreCardingActionPerformed(evt);
            }
        });
        jMnuConsulta.add(jMnuScoreCarding);

        jMnuPaineis.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jMnuPaineis.setBackground(new java.awt.Color(20, 65, 84));
        jMnuPaineis.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_painel.gif"))); // NOI18N
        jMnuPaineis.setMnemonic('P');
        jMnuPaineis.setText(bundle.getString("KpiMainPanel_00024")); // NOI18N
        jMnuPaineis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuPaineisActionPerformed(evt);
            }
        });
        jMnuConsulta.add(jMnuPaineis);

        jMnuPaineisComp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_K, java.awt.event.InputEvent.CTRL_MASK));
        jMnuPaineisComp.setBackground(new java.awt.Color(20, 65, 84));
        jMnuPaineisComp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_painel_comp.gif"))); // NOI18N
        jMnuPaineisComp.setText(bundle.getString("KpiMainPanel_00036")); // NOI18N
        jMnuPaineisComp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuPaineisCompActionPerformed(evt);
            }
        });
        jMnuConsulta.add(jMnuPaineisComp);

        jMnuLembrete.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        jMnuLembrete.setBackground(new java.awt.Color(20, 65, 84));
        jMnuLembrete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_lembrete.gif"))); // NOI18N
        jMnuLembrete.setMnemonic('m');
        jMnuLembrete.setText(bundle.getString("KpiMainPanel_00025")); // NOI18N
        jMnuLembrete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuLembreteActionPerformed(evt);
            }
        });
        jMnuConsulta.add(jMnuLembrete);

        jMnuStatusPlanos.setBackground(new java.awt.Color(20, 65, 84));
        jMnuStatusPlanos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_planodeacao.gif"))); // NOI18N
        jMnuStatusPlanos.setText(bundle.getString("KpiMainPanel_00034")); // NOI18N
        jMnuStatusPlanos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuStatusPlanosActionPerformed(evt);
            }
        });
        jMnuConsulta.add(jMnuStatusPlanos);

        jMnuBarScoredcard.add(jMnuConsulta);

        jMnuRelatorios.setBackground(new java.awt.Color(20, 65, 84));
        jMnuRelatorios.setForeground(new java.awt.Color(20, 65, 84));
        jMnuRelatorios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_relatorio.gif"))); // NOI18N
        jMnuRelatorios.setMnemonic('R');
        jMnuRelatorios.setToolTipText(bundle1.getString("KpiMainPanel_00030")); // NOI18N
        jMnuRelatorios.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jMnuRelatorios.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jMnuRelatorios.setIconTextGap(0);
        jMnuRelatorios.setMaximumSize(new java.awt.Dimension(35, 60));
        jMnuRelatorios.setPreferredSize(new java.awt.Dimension(35, 60));

        jMnuApresentacao.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        jMnuApresentacao.setBackground(new java.awt.Color(20, 65, 84));
        jMnuApresentacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_relatorio.gif"))); // NOI18N
        jMnuApresentacao.setMnemonic('A');
        jMnuApresentacao.setText(bundle.getString("KpiApresentacao_00002")); // NOI18N
        jMnuApresentacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuApresentacaoActionPerformed(evt);
            }
        });
        jMnuRelatorios.add(jMnuApresentacao);

        jMnuRelPlano.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_MASK));
        jMnuRelPlano.setBackground(new java.awt.Color(20, 65, 84));
        jMnuRelPlano.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_relatorio.gif"))); // NOI18N
        jMnuRelPlano.setMnemonic('P');
        jMnuRelPlano.setText(bundle.getString("KpiMainPanel_00031")); // NOI18N
        jMnuRelPlano.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuRelPlanoActionPerformed(evt);
            }
        });
        jMnuRelatorios.add(jMnuRelPlano);

        jMnuRelScoreInd.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.CTRL_MASK));
        jMnuRelScoreInd.setBackground(new java.awt.Color(20, 65, 84));
        jMnuRelScoreInd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_relatorio.gif"))); // NOI18N
        jMnuRelScoreInd.setMnemonic('S');
        jMnuRelScoreInd.setText(bundle.getString("KpiMainPanel_00033")); // NOI18N
        jMnuRelScoreInd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuRelScoreIndActionPerformed(evt);
            }
        });
        jMnuRelatorios.add(jMnuRelScoreInd);

        jMnuRelEstatistica.setBackground(new java.awt.Color(20, 65, 84));
        jMnuRelEstatistica.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_relatorio.gif"))); // NOI18N
        jMnuRelEstatistica.setText(bundle.getString("KpiRelEstatPlan_00001")); // NOI18N
        jMnuRelEstatistica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuRelEstatisticaActionPerformed(evt);
            }
        });
        jMnuRelatorios.add(jMnuRelEstatistica);

        jMnuRelBook.setBackground(new java.awt.Color(20, 65, 84));
        jMnuRelBook.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_relatorio.gif"))); // NOI18N
        jMnuRelBook.setText(bundle1.getString("KpiRelBook_00001")); // NOI18N
        jMnuRelBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuRelBookActionPerformed(evt);
            }
        });
        jMnuRelatorios.add(jMnuRelBook);

        jMnuRelInd.setBackground(new java.awt.Color(20, 65, 84));
        jMnuRelInd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_relatorio.gif"))); // NOI18N
        jMnuRelInd.setText(bundle1.getString("KpiRelInd_00001")); // NOI18N
        jMnuRelInd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuRelIndActionPerformed(evt);
            }
        });
        jMnuRelatorios.add(jMnuRelInd);

        jMnuBarScoredcard.add(jMnuRelatorios);

        jMnuConfiguracao.setBackground(new java.awt.Color(20, 65, 84));
        jMnuConfiguracao.setForeground(new java.awt.Color(20, 65, 84));
        jMnuConfiguracao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ferramentas.gif"))); // NOI18N
        jMnuConfiguracao.setMnemonic('F');
        jMnuConfiguracao.setToolTipText(bundle1.getString("KpiMainPanel_00015")); // NOI18N
        jMnuConfiguracao.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jMnuConfiguracao.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jMnuConfiguracao.setIconTextGap(0);
        jMnuConfiguracao.setMaximumSize(new java.awt.Dimension(35, 60));
        jMnuConfiguracao.setPreferredSize(new java.awt.Dimension(35, 60));
        jMnuConfiguracao.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMnuConfiguracaoMouseClicked(evt);
            }
        });
        jMnuBarScoredcard.add(jMnuConfiguracao);

        jMnuExibir.setBackground(new java.awt.Color(20, 65, 84));
        jMnuExibir.setForeground(new java.awt.Color(20, 65, 84));
        jMnuExibir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_visao.png"))); // NOI18N
        jMnuExibir.setMnemonic('E');
        jMnuExibir.setToolTipText(bundle1.getString("KpiMainPanel_00016")); // NOI18N
        jMnuExibir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jMnuExibir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jMnuExibir.setIconTextGap(0);
        jMnuExibir.setMaximumSize(new java.awt.Dimension(35, 60));
        jMnuExibir.setPreferredSize(new java.awt.Dimension(35, 60));

        radJanela.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_J, java.awt.event.InputEvent.CTRL_MASK));
        radJanela.setBackground(new java.awt.Color(20, 65, 84));
        grpView.add(radJanela);
        radJanela.setMnemonic('j');
        radJanela.setText(bundle.getString("KpiMainPanel_00026")); // NOI18N
        radJanela.setBorder(null);
        radJanela.setBorderPainted(true);
        radJanela.setFocusPainted(true);
        radJanela.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_janela.gif"))); // NOI18N
        radJanela.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radJanelaItemStateChanged(evt);
            }
        });
        jMnuExibir.add(radJanela);

        radPasta.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_T, java.awt.event.InputEvent.CTRL_MASK));
        radPasta.setBackground(new java.awt.Color(20, 65, 84));
        grpView.add(radPasta);
        radPasta.setMnemonic('p');
        radPasta.setText(bundle.getString("KpiMainPanel_00027")); // NOI18N
        radPasta.setBorder(null);
        radPasta.setBorderPainted(true);
        radPasta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_pastinha.gif"))); // NOI18N
        jMnuExibir.add(radPasta);

        jMnuBarScoredcard.add(jMnuExibir);

        jMnuAjuda.setBackground(new java.awt.Color(20, 65, 84));
        jMnuAjuda.setForeground(new java.awt.Color(20, 65, 84));
        jMnuAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        jMnuAjuda.setMnemonic('A');
        java.util.ResourceBundle bundle2 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        jMnuAjuda.setToolTipText(bundle2.getString("KpiAjuda_00001")); // NOI18N
        jMnuAjuda.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jMnuAjuda.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jMnuAjuda.setIconTextGap(0);
        jMnuAjuda.setMaximumSize(new java.awt.Dimension(35, 60));
        jMnuAjuda.setPreferredSize(new java.awt.Dimension(35, 60));

        jMnuConteudo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMnuConteudo.setBackground(new java.awt.Color(20, 65, 84));
        jMnuConteudo.setMnemonic('C');
        jMnuConteudo.setText(bundle.getString("KpiMainPanel_00028")); // NOI18N
        jMnuConteudo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuConteudoActionPerformed(evt);
            }
        });
        jMnuAjuda.add(jMnuConteudo);

        jMnuSobre.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMnuSobre.setBackground(new java.awt.Color(20, 65, 84));
        jMnuSobre.setMnemonic('S');
        jMnuSobre.setText(bundle.getString("KpiMainPanel_00011")); // NOI18N
        jMnuSobre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuSobreActionPerformed(evt);
            }
        });
        jMnuAjuda.add(jMnuSobre);

        jMnuBarScoredcard.add(jMnuAjuda);

        jMnuLogout.setBackground(new java.awt.Color(20, 65, 84));
        jMnuLogout.setForeground(new java.awt.Color(20, 65, 84));
        jMnuLogout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_seguranca.gif"))); // NOI18N
        jMnuLogout.setMnemonic('L');
        jMnuLogout.setToolTipText(bundle2.getString("KpiMainPanel_00042")); // NOI18N
        jMnuLogout.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jMnuLogout.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jMnuLogout.setIconTextGap(0);
        jMnuLogout.setMaximumSize(new java.awt.Dimension(35, 60));
        jMnuLogout.setPreferredSize(new java.awt.Dimension(35, 60));
        jMnuLogout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMnuLogoutMouseClicked(evt);
            }
        });
        jMnuBarScoredcard.add(jMnuLogout);

        setLayout(new java.awt.BorderLayout());

        jPanelFirst.setFocusable(false);
        jPanelFirst.setMinimumSize(new java.awt.Dimension(260, 180));
        jPanelFirst.setPreferredSize(new java.awt.Dimension(760, 580));
        jPanelFirst.setLayout(new java.awt.BorderLayout());

        jPanelCentral.setLayout(new java.awt.CardLayout());

        jSplitPaneCadastro.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jSplitPaneCadastro.setDividerLocation(0);
        jSplitPaneCadastro.setDividerSize(0);
        jSplitPaneCadastro.setDoubleBuffered(true);
        jSplitPaneCadastro.setFocusable(false);
        jSplitPaneCadastro.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                jSplitPaneCadastroComponentResized(evt);
            }
        });

        jDesktopCadastro.setBackground(new java.awt.Color(224, 229, 234));
        jSplitPaneCadastro.setRightComponent(jDesktopCadastro);

        jPanelCentral.add(jSplitPaneCadastro, "CADASTRO");

        jSplitPaneConfiguracao.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jSplitPaneConfiguracao.setDividerLocation(200);
        jSplitPaneConfiguracao.setDoubleBuffered(true);
        jSplitPaneConfiguracao.setOneTouchExpandable(true);

        jScrollPane4.setBorder(null);

        jTexturePanel3.setImageTexture(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/arvore.jpg"))); // NOI18N
        jTexturePanel3.setLayout(new java.awt.BorderLayout());

        kpiTreeConfiguracao.setAutoscrolls(true);
        kpiTreeConfiguracao.setDoubleBuffered(true);
        kpiTreeConfiguracao.setRowHeight(20);
        kpiTreeConfiguracao.setToggleClickCount(0);
        kpiTreeConfiguracao.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                kpiTreeConfiguracaoMouseClicked(evt);
            }
        });
        jTexturePanel3.add(kpiTreeConfiguracao, java.awt.BorderLayout.CENTER);

        jPanelAtualiza.setBackground(new java.awt.Color(255, 255, 255));

        lblAtualiza.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        lblAtualiza.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblAtualiza.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblAtualizaMouseClicked(evt);
            }
        });
        jPanelAtualiza.add(lblAtualiza);

        jTexturePanel3.add(jPanelAtualiza, java.awt.BorderLayout.LINE_END);

        jScrollPane4.setViewportView(jTexturePanel3);

        jSplitPaneConfiguracao.setLeftComponent(jScrollPane4);

        jDesktopConfiguracao.setBackground(new java.awt.Color(211, 228, 233));
        jSplitPaneConfiguracao.setRightComponent(jDesktopConfiguracao);

        jPanelCentral.add(jSplitPaneConfiguracao, "CONFIGURACAO");

        jPanelFirst.add(jPanelCentral, java.awt.BorderLayout.CENTER);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(new java.awt.BorderLayout());

        jPanel3.setBackground(new java.awt.Color(236, 236, 236));
        jPanel3.setLayout(new javax.swing.BoxLayout(jPanel3, javax.swing.BoxLayout.LINE_AXIS));
        jPanel2.add(jPanel3, java.awt.BorderLayout.EAST);

        jPanel4.setLayout(new java.awt.BorderLayout());
        jPanel2.add(jPanel4, java.awt.BorderLayout.CENTER);

        jPanel5.setMaximumSize(new java.awt.Dimension(2147483647, 43));
        jPanel5.setMinimumSize(new java.awt.Dimension(230, 43));
        jPanel5.setPreferredSize(new java.awt.Dimension(784, 43));
        jPanel5.setLayout(new java.awt.BorderLayout());

        jTexturePanelArt1.setImageTexture(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/form_footer_Left.gif"))); // NOI18N
        jTexturePanelArt1.setMaximumSize(new java.awt.Dimension(87, 43));
        jTexturePanelArt1.setMinimumSize(new java.awt.Dimension(87, 43));
        jTexturePanelArt1.setPreferredSize(new java.awt.Dimension(87, 43));
        jPanel5.add(jTexturePanelArt1, java.awt.BorderLayout.WEST);

        jTexturePanelLeft.setImageTexture(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/art_menu.gif"))); // NOI18N
        jTexturePanelLeft.setMaximumSize(new java.awt.Dimension(10000, 43));
        jTexturePanelLeft.setMinimumSize(new java.awt.Dimension(5, 43));
        jTexturePanelLeft.setPreferredSize(new java.awt.Dimension(87, 43));
        jTexturePanelLeft.setLayout(null);

        jPanelStatus.setBackground(new java.awt.Color(20, 65, 84));
        jPanelStatus.setForeground(new java.awt.Color(255, 255, 255));
        jPanelStatus.setAlignmentX(0.0F);
        jPanelStatus.setAlignmentY(0.0F);
        jPanelStatus.setFocusCycleRoot(true);
        jPanelStatus.setMaximumSize(new java.awt.Dimension(292, 32767));
        jPanelStatus.setMinimumSize(new java.awt.Dimension(292, 18));
        jPanelStatus.setPreferredSize(new java.awt.Dimension(10, 22));
        jPanelStatus.setLayout(null);

        jLabelStatusIcone.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelStatusIcone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_desconectado.gif"))); // NOI18N
        jLabelStatusIcone.setPreferredSize(new java.awt.Dimension(20, 18));
        jPanelStatus.add(jLabelStatusIcone);
        jLabelStatusIcone.setBounds(25, 0, 20, 18);

        jLabelStatusGauge.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/loading.gif"))); // NOI18N
        jLabelStatusGauge.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabelStatusGauge.setPreferredSize(new java.awt.Dimension(30, 16));
        jPanelStatus.add(jLabelStatusGauge);
        jLabelStatusGauge.setBounds(50, 1, 45, 16);

        jLabelStatusCalc.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelStatusCalc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_toolbarstatus_green.gif"))); // NOI18N
        jLabelStatusCalc.setPreferredSize(new java.awt.Dimension(20, 18));
        jPanelStatus.add(jLabelStatusCalc);
        jLabelStatusCalc.setBounds(5, 0, 20, 18);

        jTexturePanelLeft.add(jPanelStatus);
        jPanelStatus.setBounds(8, 16, 160, 22);

        jPanel5.add(jTexturePanelLeft, java.awt.BorderLayout.CENTER);

        jTexturePanelArt2.setImageTexture(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/art_menu.gif"))); // NOI18N
        jTexturePanelArt2.setMaximumSize(new java.awt.Dimension(87, 43));
        jTexturePanelArt2.setMinimumSize(new java.awt.Dimension(87, 43));
        jTexturePanelArt2.setPreferredSize(new java.awt.Dimension(500, 43));
        jTexturePanelArt2.setLayout(null);

        pnlBarra.setBackground(new java.awt.Color(20, 65, 84));
        pnlBarra.setPreferredSize(new java.awt.Dimension(250, 28));
        pnlBarra.setLayout(new java.awt.BorderLayout());
        jTexturePanelArt2.add(pnlBarra);
        pnlBarra.setBounds(248, 13, 250, 28);

        pnlUser.setBackground(new java.awt.Color(20, 65, 84));
        pnlUser.setLayout(new java.awt.BorderLayout());

        pnlUserLeft.setBackground(new java.awt.Color(153, 153, 153));
        pnlUserLeft.setMaximumSize(new java.awt.Dimension(1, 25));
        pnlUserLeft.setMinimumSize(new java.awt.Dimension(1, 25));
        pnlUserLeft.setPreferredSize(new java.awt.Dimension(1, 25));
        pnlUserLeft.setLayout(new java.awt.BorderLayout());
        pnlUser.add(pnlUserLeft, java.awt.BorderLayout.WEST);

        jLabelUsuario.setBackground(new java.awt.Color(255, 0, 153));
        jLabelUsuario.setForeground(new java.awt.Color(255, 255, 255));
        jLabelUsuario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelUsuario.setText("null");
        jLabelUsuario.setMaximumSize(new java.awt.Dimension(100, 25));
        jLabelUsuario.setPreferredSize(new java.awt.Dimension(300, 25));
        pnlUser.add(jLabelUsuario, java.awt.BorderLayout.CENTER);

        pnlUserRight.setBackground(new java.awt.Color(153, 153, 153));
        pnlUserRight.setForeground(new java.awt.Color(102, 102, 102));
        pnlUserRight.setMaximumSize(new java.awt.Dimension(1, 25));
        pnlUserRight.setMinimumSize(new java.awt.Dimension(1, 25));
        pnlUserRight.setPreferredSize(new java.awt.Dimension(1, 25));
        pnlUserRight.setLayout(new java.awt.BorderLayout());
        pnlUser.add(pnlUserRight, java.awt.BorderLayout.LINE_END);

        jTexturePanelArt2.add(pnlUser);
        pnlUser.setBounds(37, 13, 210, 28);

        jPanel5.add(jTexturePanelArt2, java.awt.BorderLayout.EAST);

        jPanel2.add(jPanel5, java.awt.BorderLayout.PAGE_START);

        jPanelFirst.add(jPanel2, java.awt.BorderLayout.SOUTH);

        add(jPanelFirst, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

	private void jMnuEspPeixeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuEspPeixeActionPerformed
	    callFormItem("LST_ESP_PEIXE", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00004")); //NOI18N
	}//GEN-LAST:event_jMnuEspPeixeActionPerformed

	private void jMnuPacoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuPacoteActionPerformed
	    callFormItem("LSTPACOTE", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00004")); //NOI18N
	}//GEN-LAST:event_jMnuPacoteActionPerformed

	private void jMnuPaineisCompActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuPaineisCompActionPerformed
	    callFormItem("LSTPAINELCOMP", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00014")); //NOI18N
	}//GEN-LAST:event_jMnuPaineisCompActionPerformed

	private void jMnuGrupoAcaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuGrupoAcaoActionPerformed
	    callFormItem("LSTGRUPO_ACAO", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00004")); //NOI18N
}//GEN-LAST:event_jMnuGrupoAcaoActionPerformed

	private void jMnuStatusPlanosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuStatusPlanosActionPerformed
	    callFormItem("LISTA_STATUS_ACAO", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00014")); //NOI18N
	}//GEN-LAST:event_jMnuStatusPlanosActionPerformed

	private void jMnuRelScoreIndActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuRelScoreIndActionPerformed
	    callFormItem("RELSCOREIND", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00030")); //NOI18N
	}//GEN-LAST:event_jMnuRelScoreIndActionPerformed

	private void jMnuUnidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuUnidadeActionPerformed
	    callFormItem("LSTUNIDADE", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00004")); //NOI18N
	}//GEN-LAST:event_jMnuUnidadeActionPerformed

    private void jMnuApresentacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuApresentacaoActionPerformed
	callFormItem("LSTRELAPR", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiApresentacao_00003")); //NOI18N
    }//GEN-LAST:event_jMnuApresentacaoActionPerformed

	private void jMnuRelPlanoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuRelPlanoActionPerformed
	    callFormItem("RELPLAN", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00030")); //NOI18N
	}//GEN-LAST:event_jMnuRelPlanoActionPerformed

	private void jMnuScoreCardingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuScoreCardingActionPerformed
	    callFormItem("SCORECARDING", ""); //NOI18N
	}//GEN-LAST:event_jMnuScoreCardingActionPerformed

	private void jMnuPaineisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuPaineisActionPerformed
	    callFormItem("LSTPAINEL", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00014")); //NOI18N
	}//GEN-LAST:event_jMnuPaineisActionPerformed

	private void jMnuConsultaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMnuConsultaMouseClicked
	    showCadastro();
	}//GEN-LAST:event_jMnuConsultaMouseClicked

	private void jMnuCadastroMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMnuCadastroMouseClicked
	    showCadastro();
	}//GEN-LAST:event_jMnuCadastroMouseClicked

	private void jMnuLembreteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuLembreteActionPerformed
	    inicializaLembrete(true);
	}//GEN-LAST:event_jMnuLembreteActionPerformed

	private void kpiTreeConfiguracaoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_kpiTreeConfiguracaoMouseClicked
	    if (evt.getClickCount() == 2) {

		DefaultMutableTreeNode node = (DefaultMutableTreeNode) kpiTree.getLastSelectedPathComponent();

		try {
		    kpi.swing.KpiTreeNode data = (kpi.swing.KpiTreeNode) node.getUserObject();
		    kpi.core.KpiStaticReferences.getKpiFormController().getForm(data.getType(), data.getID(), data.getName());
		} catch (kpi.core.KpiFormControllerException e) {
		    kpi.core.KpiDebug.println(e.getMessage());
		}
	    }
	}//GEN-LAST:event_kpiTreeConfiguracaoMouseClicked

	private void jSplitPaneCadastroComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_jSplitPaneCadastroComponentResized
	    jSplitPaneCadastro.setDividerLocation(0);
	    jSplitPaneCadastro.setDividerSize(0);
	    if (this.lShowLembrete) {
		inicializaLembrete(false);
		this.lShowLembrete = false;
	    }

	}//GEN-LAST:event_jSplitPaneCadastroComponentResized

	private void jMnuProjetosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuProjetosActionPerformed
	    try {
		String sText = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00004");
		KpiListaProjeto oForm = (KpiListaProjeto) KpiStaticReferences.getKpiFormController().getForm("LSTPROJETO", "0", sText); //NOI18N
		oForm.buildFilter(KpiListaProjeto.FILTER_MYPROJECT, "0"); //NOI18N
	    } catch (kpi.core.KpiFormControllerException e) {
		kpi.core.KpiDebug.println(e.getMessage());
	    }
	}//GEN-LAST:event_jMnuProjetosActionPerformed

	private void jMnuPlanoDeAcaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuPlanoDeAcaoActionPerformed
	    try {
		String sText = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00004");
		KpiListaPlanos oForm = (KpiListaPlanos) KpiStaticReferences.getKpiFormController().getForm("LSTPLANOACAO", "0", sText); //NOI18N
		oForm.buildFilter(KpiListaPlanos.FILTER_MYACTION, "0"); //NOI18N

	    } catch (kpi.core.KpiFormControllerException e) {
		kpi.core.KpiDebug.println(e.getMessage());
	    }


	}//GEN-LAST:event_jMnuPlanoDeAcaoActionPerformed

	private void jMnuMetaFormulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuMetaFormulaActionPerformed
	    callFormItem("LSTMETAFORMULA", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00004")); //NOI18N
	}//GEN-LAST:event_jMnuMetaFormulaActionPerformed

    private void jMnuConfiguracaoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMnuConfiguracaoMouseClicked
	showConfiguracao();
    }//GEN-LAST:event_jMnuConfiguracaoMouseClicked

    private void jMnuScorecardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuScorecardActionPerformed
	callFormItem("SCORECARD", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00004")); //NOI18N
    }//GEN-LAST:event_jMnuScorecardActionPerformed

	private void radJanelaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radJanelaItemStateChanged
	    //Muda o tipo de vizualiza��o de pastas para janelas.
	    kpi.core.KpiFormController formController = kpi.core.KpiStaticReferences.getKpiFormController();
	    setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
	    formController.changeView();
	    setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
	}//GEN-LAST:event_radJanelaItemStateChanged

	private void jMnuGrupoIndicadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuGrupoIndicadorActionPerformed
	    callFormItem("LSTGRUPO_IND", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00004")); //NOI18N
	}//GEN-LAST:event_jMnuGrupoIndicadorActionPerformed

	private void jMnuIndicadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuIndicadorActionPerformed
	    callFormItem("LSTINDICADOR", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00004")); //NOI18N
	}//GEN-LAST:event_jMnuIndicadorActionPerformed

	private void jMnuConteudoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuConteudoActionPerformed
	    BIXMLRecord record = kpi.core.KpiStaticReferences.getKpiDataController().loadHelp("SGI", "SIGASGI"); //NOI18N

	    if (kpi.core.KpiStaticReferences.getKpiDataController().getStatus() == kpi.core.KpiDataController.KPI_ST_OK) {

		try {
		    KpiToolKit toolkit = new KpiToolKit();
		    String url = record.getString("URL");
		    java.net.URL helpURL = new java.net.URL(url);

		    toolkit.browser(helpURL);

		} catch (java.net.MalformedURLException e) {
		    e.printStackTrace();
		}
	    }
	}//GEN-LAST:event_jMnuConteudoActionPerformed

	private void jMnuSobreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuSobreActionPerformed
	    /*Requisita a URL.*/
	    KpiToolKit oToolKit = new KpiToolKit();
	    oToolKit.browser("sgisobre.apw"); //NOI18N
	}//GEN-LAST:event_jMnuSobreActionPerformed

	private void jMnuLogoutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMnuLogoutMouseClicked
	    kpi.swing.KpiDefaultDialogSystem oQuestion;
	    oQuestion = kpi.core.KpiStaticReferences.getKpiDialogSystem();

	    if (oQuestion.confirmMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00009")) == javax.swing.JOptionPane.YES_NO_OPTION) {

		/*Verifica se a o SGI est� sendo executado via DESKTOP ou APPLET.*/
		if (kpi.core.KpiStaticReferences.getKpiExecutionType() == kpi.core.KpiStaticReferences.KPI_APLICATION) {
		    getKpiApplication().resetApplication();
		} else {
		    getKpiApplet().resetApplet();
		}
	    }
	}//GEN-LAST:event_jMnuLogoutMouseClicked

        private void jMnuRelEstatisticaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuRelEstatisticaActionPerformed
	    callFormItem("RELESTATPLAN", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00030")); //NOI18N
        }//GEN-LAST:event_jMnuRelEstatisticaActionPerformed

private void jMnuFormMatrixActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuFormMatrixActionPerformed
    callFormItem("FORMMATRIX", "Cadastros"); //NOI18N
}//GEN-LAST:event_jMnuFormMatrixActionPerformed

    private void jMenuRelIndActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuRelIndActionPerformed
    }//GEN-LAST:event_jMenuRelIndActionPerformed

    private void jMnuRelBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuRelBookActionPerformed
	callFormItem("RELBOOK", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00030")); //NOI18N
    }//GEN-LAST:event_jMnuRelBookActionPerformed

    private void jMnuRelIndActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuRelIndActionPerformed
	callFormItem("RELIND", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00030")); //NOI18N        
    }//GEN-LAST:event_jMnuRelIndActionPerformed

    private void lblAtualizaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblAtualizaMouseClicked
        showConfiguracao(); // Atualiza a �rvore de ferramentas.
    }//GEN-LAST:event_lblAtualizaMouseClicked

    private void callFormItem(String mnuType, String strTexto) {
	try {

	    showCadastro();
	    /*Abre o formul�rio do item selecionado no menu.*/
	    KpiStaticReferences.getKpiFormController().getForm(mnuType, "0", strTexto); //NOI18N

	} catch (kpi.core.KpiFormControllerException e) {
	    kpi.core.KpiDebug.println(e.getMessage());
	}
    }

    @Override
    public void paint(java.awt.Graphics g) {
	if (theme == KpiMainPanel.THEME_DEFAULT) {

	    //Setando imagens
	    try {
		String url;

		if (kpiApplet != null) {
		    url = kpiApplet.getCodeBase().toString().concat("imagens/"); //NOI18N
		} else {
		    url = KpiStaticReferences.getKpiApplication().getCodeBase().concat("imagens/"); //NOI18N
		}

		//Imagem personalizada
		javax.swing.ImageIcon imgClie = new javax.swing.ImageIcon(new java.net.URL(url.concat("art_logo_clie.gif"))); //NOI18N
		//jTexturePanelCenter.setImageTexture(imgClie);
		//jTexturePanelCenter.setToolTipText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00037")); //"Clique para alterar a imagem"

		//Imagem totvs
		javax.swing.ImageIcon imgTotvs = new javax.swing.ImageIcon(new java.net.URL(url.concat("art_logo1.gif"))); //NOI18N
		//jTexturePanelArt1.setImageTexture(imgTotvs);

	    } catch (Exception e) {
		System.out.println("N�o foi possivel carregar a imagem do cliente!"); //NOI18N
		System.out.println(e);
		//jTexturePanelCenter.setImageTexture(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/art_logo2.gif")));
	    }
	}
	super.paint(g);
    }

    public void changeTheme(int theme) {
	try {
	    this.theme = theme;
	    javax.swing.plaf.metal.MetalLookAndFeel.setCurrentTheme(new kpi.applet.themes.KpiDefaultTheme());
	    javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getCrossPlatformLookAndFeelClassName());
	    javax.swing.SwingUtilities.updateComponentTreeUI(kpiApplet);
	    if (kpiMainFrame != null) {
		javax.swing.SwingUtilities.updateComponentTreeUI(kpiMainFrame);
	    }
	} catch (Exception e) {
	    kpi.core.KpiDebug.println(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00005"));
	}
    }

    public kpi.core.KpiImageResources getImageResources() {
	return kpiImageResources;
    }

    public void setStatusBarCondition(int condition) {
	if (condition == ST_IDLE) {
	    //jLabelStatusMessage.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00006"));
	    jLabelStatusIcone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_desconectado.gif"))); //NOI18N
	    jLabelStatusIcone.setToolTipText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00006"));
	    jLabelStatusGauge.setVisible(false);
	    jPanelStatus.repaint();
	} else if (condition == ST_CONNECTING) {
	    //jLabelStatusMessage.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00007"));
	    jLabelStatusIcone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_conectado.gif"))); //NOI18N
	    jLabelStatusIcone.setToolTipText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00007"));
	    jLabelStatusGauge.setVisible(true);
	    jPanelStatus.repaint();
	}
    }

    //Usado quando o usu�rio alterna entre os s.
    public void alterDesktop(kpi.core.KpiFormController formController, String deskType) {
	//Desativa os listeners quando muda o desktop.
	java.awt.event.ItemListener[] eventJanelas = radJanela.getItemListeners();
	java.awt.event.ItemListener[] eventPastas = radPasta.getItemListeners();
	for (int iEvento = 0; iEvento < eventJanelas.length; iEvento++) {
	    radJanela.removeItemListener(eventJanelas[iEvento]);
	}
	for (int iEvento = 0; iEvento < eventPastas.length; iEvento++) {
	    radPasta.removeItemListener(eventPastas[iEvento]);
	}

	//Seta a op��o default do menu.
	if (formController.visao == KpiFormController.ST_PASTA) {
	    radJanela.setSelected(false);
	    radPasta.setSelected(true);
	} else {
	    radJanela.setSelected(true);
	    radPasta.setSelected(false);
	}

	//Registra os eventos.
	for (int iEvento = 0; iEvento < eventJanelas.length; iEvento++) {
	    radJanela.addItemListener(eventJanelas[iEvento]);
	}
	for (int iEvento = 0; iEvento < eventPastas.length; iEvento++) {
	    radPasta.addItemListener(eventPastas[iEvento]);
	}
	setDesktopSelected(deskType);
    }

    public kpi.swing.KpiTree getKpiTree() {
	return kpiTree;
    }

    public void setKpiTree(kpi.swing.KpiTree kpiTree) {
	this.kpiTree = kpiTree;
    }

    public String getDesktopSelected() {
	return desktopSelected;
    }

    public void setDesktopSelected(String desktopSelected) {
	this.desktopSelected = desktopSelected;
    }

    public javax.swing.JSplitPane getJSplitPaneCadastro() {
	return this.jSplitPaneCadastro;
    }

    public javax.swing.JSplitPane getJSplitPaneConfiguracao() {
	return this.jSplitPaneConfiguracao;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup grpView;
    private kpi.beans.JBIDesktopPane jDesktopCadastro;
    private kpi.beans.JBIDesktopPane jDesktopConfiguracao;
    private javax.swing.JLabel jLabelStatusCalc;
    private javax.swing.JLabel jLabelStatusGauge;
    private javax.swing.JLabel jLabelStatusIcone;
    private javax.swing.JLabel jLabelUsuario;
    private javax.swing.JMenu jMnuAjuda;
    private javax.swing.JMenuItem jMnuApresentacao;
    private javax.swing.JMenuBar jMnuBarScoredcard;
    private javax.swing.JMenu jMnuCadastro;
    private javax.swing.JMenu jMnuConfiguracao;
    private javax.swing.JMenu jMnuConsulta;
    private javax.swing.JMenuItem jMnuConteudo;
    private javax.swing.JMenuItem jMnuEspPeixe;
    private javax.swing.JMenu jMnuExibir;
    private javax.swing.JMenuItem jMnuFormMatrix;
    private javax.swing.JMenuItem jMnuGrupoAcao;
    private javax.swing.JMenuItem jMnuGrupoIndicador;
    public javax.swing.JMenuItem jMnuIndicador;
    private javax.swing.JMenuItem jMnuLembrete;
    private javax.swing.JMenu jMnuLogout;
    private javax.swing.JMenuItem jMnuMetaFormula;
    private javax.swing.JMenuItem jMnuPacote;
    private javax.swing.JMenuItem jMnuPaineis;
    private javax.swing.JMenuItem jMnuPaineisComp;
    private javax.swing.JMenuItem jMnuPlanoDeAcao;
    private javax.swing.JMenuItem jMnuProjetos;
    public javax.swing.JMenuItem jMnuRelBook;
    private javax.swing.JMenuItem jMnuRelEstatistica;
    public javax.swing.JMenuItem jMnuRelInd;
    private javax.swing.JMenuItem jMnuRelPlano;
    private javax.swing.JMenuItem jMnuRelScoreInd;
    private javax.swing.JMenu jMnuRelatorios;
    private javax.swing.JMenuItem jMnuScoreCarding;
    private javax.swing.JMenuItem jMnuScorecard;
    private javax.swing.JMenuItem jMnuSobre;
    private javax.swing.JMenuItem jMnuStatusPlanos;
    private javax.swing.JMenuItem jMnuUnidade;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanelAtualiza;
    private javax.swing.JPanel jPanelCentral;
    private javax.swing.JPanel jPanelFirst;
    private javax.swing.JPanel jPanelStatus;
    private javax.swing.JScrollPane jScrollPane4;
    public javax.swing.JSplitPane jSplitPaneCadastro;
    public javax.swing.JSplitPane jSplitPaneConfiguracao;
    private kpi.beans.JTexturePanel jTexturePanel3;
    private kpi.beans.JTexturePanel jTexturePanelArt1;
    private kpi.beans.JTexturePanel jTexturePanelArt2;
    private kpi.beans.JTexturePanel jTexturePanelLeft;
    public javax.swing.JTree kpiTreeConfiguracao;
    private javax.swing.JLabel lblAtualiza;
    private javax.swing.JPanel pnlBarra;
    private javax.swing.JPanel pnlUser;
    private javax.swing.JPanel pnlUserLeft;
    private javax.swing.JPanel pnlUserRight;
    public javax.swing.JRadioButtonMenuItem radJanela;
    private javax.swing.JRadioButtonMenuItem radPasta;
    // End of variables declaration//GEN-END:variables

    private void inicializaLembrete(boolean showLembrete) {
	try {
	    kpi.xml.BIXMLRecord recPlanos = kpiDataController.listRecords("PLANOACAO", "LEMBRETE"); //NOI18N
	    kpi.xml.BIXMLRecord recMsg = kpiDataController.listRecords("MENSAGEM", "");// //NOI18N

	    BIXMLVector vctPlanos = recPlanos.getBIXMLVector("PLANOSACAO"); //NOI18N
	    BIXMLVector vctMsg = recMsg.getBIXMLVector("MENSAGENS"); //NOI18N

	    int qtdItens = vctPlanos.size();
	    int qtdMsg = vctMsg.size();

	    if (showLembrete || qtdItens > 0 || qtdMsg > 0) { //Verifica se devo mostrar o lembrete.
		kpi.swing.tools.KpiLembrete frame = (kpi.swing.tools.KpiLembrete) kpi.core.KpiStaticReferences.getKpiFormController().getForm(
			"LSTLEMBRETE", "0", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiLembrete_00006")).asJInternalFrame(); //NOI18N

		recPlanos.set(vctMsg);
		frame.lembreteLoadRecord(recPlanos);
	    }
	    /*
	    if (qtdItens == 0) {
	    btnLembrete.setVisible(false);
	    } else {
	    btnLembrete.setVisible(true);
	    }
	     */

	} catch (kpi.core.KpiFormControllerException e) {
	    kpi.core.KpiDebug.println(e.getMessage());
	}
    }

    /*
     *Configura os direitos de acesso e propriedades do MainPanel.
     */
    @SuppressWarnings("empty-statement")
    private void setMainPanelProperties() {
	kpi.swing.desktop.KpiDesktopFunctions oDeskProperties = new kpi.swing.desktop.KpiDesktopFunctions();
	kpiFormControllerCadastro.visao = oDeskProperties.recDesktop.getInt("DESKVIEWTYPE"); //NOI18N
	kpiFormControllerConfiguracao.visao = oDeskProperties.recDesktop.getInt("CONFIGVIEWTYPE"); //NOI18N
	boolean isAdminMode = oDeskProperties.recDesktop.getBoolean("ISADMIN"); //NOI18N

	KpiStaticReferences.setUserType(isAdminMode);

	mnuController.setVctRegras(oDeskProperties.recDesktop.getBIXMLVector("REGRAS")); //NOI18N
	mnuController.setJMainBar(jMnuBarScoredcard);

	//Se nao for o administrador aplica as regras de seguran�a.
	if (!isAdminMode) {
	    mnuController.applyRules();
	}

	//Configurando o menu dependendo do tipo a analise.
	setMenuByAnalise();

	//Verifica se devo mostrar o valor default da 'data alvo' com o mes anterior ou atual
	GregorianCalendar dDataAlvo = KpiStaticReferences.getKpiDateBase().getDataAnaliseDe();
	if (oDeskProperties.recDesktop.getString("CFGDTMESANTERIOR").equals("-1") || oDeskProperties.recDesktop.getString("CFGDTMESANTERIOR").equals("T")) { //NOI18N
	    if (dDataAlvo.get(Calendar.MONTH) == Calendar.JANUARY) {
		dDataAlvo.add(java.util.Calendar.YEAR, -1);
	    }

	    dDataAlvo.add(java.util.Calendar.MONTH, -1);
	    KpiStaticReferences.getKpiDateBase().setDataAnaliseDe(dDataAlvo);
	}

	if (oDeskProperties.recDesktop.getString("CFGDTACUMULADO").equals("-1") || oDeskProperties.recDesktop.getString("CFGDTACUMULADO").equals("T")) { //NOI18N
	    KpiStaticReferences.getKpiDateBase().setDataAcumuladoAte(dDataAlvo);
	}

	//--------------------------------------------------------------
	//Verifica se devo mostrar o scorecarding.
        //--------------------------------------------------------------
	if (oDeskProperties.recDesktop.getBoolean("SHOWSCORECARDING")) {
	    callFormItem("SCORECARDING", "");
	}

        if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
            //--------------------------------------------------------------
            //Verifica se deve mostrar o mapa estrat�gico.
            //--------------------------------------------------------------
            if (oDeskProperties.recDesktop.getBoolean("SHOWMAPAESTRATEGICO")) {
                //--------------------------------------------------------------
                // Efetua chamada do Mapa estrat�gico.
                //--------------------------------------------------------------
                showCadastro(); // Prepara��o da chamada.
                KpiStaticReferences.getKpiFormController().getForm("MAPAESTRATEGICO1", oDeskProperties.recDesktop.getString("IDESTRATEGIA"), "MAPA"); 
            }
        }
	jLabelUsuario.setText(oDeskProperties.recDesktop.getString("USUARIO") + "  "); //NOI18N

	//Configura os labels customizados pelo usu�rio
	KpiCustomLabels customLabels = KpiStaticReferences.getKpiCustomLabels();
	customLabels.setSco(oDeskProperties.recDesktop.getString("STR_SCO"));
	customLabels.setReal(oDeskProperties.recDesktop.getString("STR_REAL"));
	customLabels.setMeta(oDeskProperties.recDesktop.getString("STR_META"));
	customLabels.setPrevia(oDeskProperties.recDesktop.getString("STR_PREVIA"));
        
        //Configura os labels customizados pelo usu�rio - Tend�ncia
        customLabels.setTend(oDeskProperties.recDesktop.getString("STR_TEND"));
        
	refreshCustomLabels();
    }

    public void refreshCustomLabels() {
	jMnuScorecard.setText(KpiStaticReferences.getKpiCustomLabels().getSco());
    }

    static public void openUrlPolitica() {
	kpi.xml.BIXMLRecord record = kpi.core.KpiStaticReferences.getKpiDataController().loadHelp("KPI", "POLITICA"); //NOI18N

	if (kpi.core.KpiStaticReferences.getKpiDataController().getStatus() == kpi.core.KpiDataController.KPI_ST_OK) {

	    try {
		KpiToolKit toolkit = new KpiToolKit();
		String url = record.getString("URL");
		java.net.URL helpURL = new java.net.URL(url);

		toolkit.browser(helpURL);

	    } catch (java.net.MalformedURLException e) {
		e.printStackTrace();
	    }
	}
    }

    public void setWorkStatus(int status) {
	if (status == 2) {
	    //"Existem manuten��es sendo efetuadas no momento"
	    jLabelStatusCalc.setToolTipText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00038"));
	    jLabelStatusCalc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_toolbarstatus_red.gif"))); //NOI18N
	} else if (status == 1) {
	    //"Indicadores tiveram seus valores atualizados e o c�lculo n�o foi processado"
	    jLabelStatusCalc.setToolTipText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00039"));
	    jLabelStatusCalc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_toolbarstatus_yellow.gif"))); //NOI18N
	} else {
	    //"N�o existem manuten��es sendo efetuadas no momento"
	    jLabelStatusCalc.setToolTipText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00040"));
	    jLabelStatusCalc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_toolbarstatus_green.gif"))); //NOI18N
	}
    }

    public void refreshWorkStatus() {
	try {
	    int nStatusCalc = kpiDataController.loadRecord("", "GETSTATUSWORK").getInt("CALCSTATUS"); //NOI18N
	    setWorkStatus(nStatusCalc);
	} catch (Exception ex) {
	    System.out.println(ex.toString());
	}
    }

    /***
     * Configura o Menu de acordo com o tipo de opera��o em uso.
     */
    private void setMenuByAnalise() {
	BIXMLVector vecRegras = new BIXMLVector("REGRAS", "REGRA"); //NOI18N

	if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
	    vecRegras.add(getRegra("SCORECARDS")); //NOI18N
	    vecRegras.add(getRegra("INDICADORES")); //NOI18N
	} else {
	    vecRegras.add(getRegra("FORMMATRIX")); //NOI18N
	    vecRegras.add(getRegra("RELBOOK"));
	    vecRegras.add(getRegra("RELIND"));
	}

	mnuController.applyRules(vecRegras);
    }

    private BIXMLRecord getRegra(String regra) {
	BIXMLRecord recRegra = new BIXMLRecord(regra);
	BIXMLVector vctItemRegra = new BIXMLVector("ITEMREGRA", "ITEMREGRA"); //NOI18N
	BIXMLAttributes attRegra = new BIXMLAttributes();

	BIXMLAttributes att = new BIXMLAttributes();

	att.set("NOME", regra); //NOI18N
	recRegra.setAttributes(att);

	//Desabilitando o item.
	BIXMLRecord recttt = new BIXMLRecord("REGRA"); //NOI18N
	attRegra = new BIXMLAttributes();
	attRegra.set("REGRAID", "1"); //NOI18N
	attRegra.set("VALOR", "0"); //NOI18N
	recttt.setAttributes(attRegra);
	vctItemRegra.add(recttt);

	//Adicionando o Vector de regras o Record.
	recRegra.set(vctItemRegra);

	return recRegra;
    }

    /**
     * Retorna o menu com as permiss�es.
     */
    public kpi.core.KpiMenuController getMnuController() {
	return this.mnuController;
    }

    /**
     * Classe para atualizar o status do calculo na barra de ferramentas.
     */
    private class KpiRefreshStatus {

	private java.util.Timer oTimer = null;
	private kpi.xml.BIXMLRecord oRecord = null;
	private int nError = 0;

	public void startRefreshUpdate(int millseconds) {
	    oTimer = new java.util.Timer();
	    getTimer().schedule(new AutoRefreshStatus(), 1000, millseconds);
	}

	class AutoRefreshStatus extends java.util.TimerTask {

	    public void run() {
		try {
		    int nStatusCalc = kpiDataController.loadRecord("", "GETSTATUSWORK").getInt("CALCSTATUS"); //NOI18N
		    setWorkStatus(nStatusCalc);
		    nError = 0;
		} catch (Exception ex) {
		    nError = nError + 1;
		    System.out.println(ex.toString());
		    if (nError > 3) {
			System.out.println("Thread WorkStatus Abortded!"); //NOI18N
			getTimer().cancel();
		    }
		}

	    }
	}

	public java.util.Timer getTimer() {
	    return oTimer;
	}
    }
}
