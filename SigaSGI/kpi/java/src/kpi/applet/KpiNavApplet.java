/*
 * KpiNavApplet.java
 *
 * Created on 26 de Novembro de 2003, 10:00
 */

package kpi.applet;

/**
 *
 * @author  siga1728
 */
public class KpiNavApplet extends kpi.applet.KpiApplet {
	
	/**
	 * Creates a new instance of KpiNavApplet
	 */
	public KpiNavApplet() {
	}
	
	public void init() {
		super.init();
		try {
			initMainPanel();
			getContentPane().add(mainPanel, java.awt.BorderLayout.CENTER);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}