/*
 * BIFloatFieldBeanInfo.java
 *
 * Created on 26 de Agosto de 2005, 15:13
 */

package kpi.beans;

import java.beans.*;

/**
 * @author Alexandre Alves da Silva
 */
public class BIFloatFieldBeanInfo extends SimpleBeanInfo {
    
    // Bean descriptor//GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( kpi.beans.BIFloatField.class , null );//GEN-HEADEREND:BeanDescriptor
	
	// Here you can add code for customizing the BeanDescriptor.
	
        return beanDescriptor;         }//GEN-LAST:BeanDescriptor
    
    
    // Property identifiers//GEN-FIRST:Properties
    private static final int PROPERTY_accessibleContext = 0;
    private static final int PROPERTY_action = 1;
    private static final int PROPERTY_actionCommand = 2;
    private static final int PROPERTY_actionListeners = 3;
    private static final int PROPERTY_actionMap = 4;
    private static final int PROPERTY_actions = 5;
    private static final int PROPERTY_alignmentX = 6;
    private static final int PROPERTY_alignmentY = 7;
    private static final int PROPERTY_ancestorListeners = 8;
    private static final int PROPERTY_autoscrolls = 9;
    private static final int PROPERTY_background = 10;
    private static final int PROPERTY_backgroundSet = 11;
    private static final int PROPERTY_border = 12;
    private static final int PROPERTY_bounds = 13;
    private static final int PROPERTY_caret = 14;
    private static final int PROPERTY_caretColor = 15;
    private static final int PROPERTY_caretListeners = 16;
    private static final int PROPERTY_caretPosition = 17;
    private static final int PROPERTY_colorModel = 18;
    private static final int PROPERTY_columns = 19;
    private static final int PROPERTY_component = 20;
    private static final int PROPERTY_componentCount = 21;
    private static final int PROPERTY_componentListeners = 22;
    private static final int PROPERTY_componentOrientation = 23;
    private static final int PROPERTY_componentPopupMenu = 24;
    private static final int PROPERTY_components = 25;
    private static final int PROPERTY_containerListeners = 26;
    private static final int PROPERTY_cursor = 27;
    private static final int PROPERTY_cursorSet = 28;
    private static final int PROPERTY_debugGraphicsOptions = 29;
    private static final int PROPERTY_disabledTextColor = 30;
    private static final int PROPERTY_displayable = 31;
    private static final int PROPERTY_document = 32;
    private static final int PROPERTY_doubleBuffered = 33;
    private static final int PROPERTY_dragEnabled = 34;
    private static final int PROPERTY_dropTarget = 35;
    private static final int PROPERTY_editable = 36;
    private static final int PROPERTY_enabled = 37;
    private static final int PROPERTY_focusable = 38;
    private static final int PROPERTY_focusAccelerator = 39;
    private static final int PROPERTY_focusCycleRoot = 40;
    private static final int PROPERTY_focusCycleRootAncestor = 41;
    private static final int PROPERTY_focusListeners = 42;
    private static final int PROPERTY_focusOwner = 43;
    private static final int PROPERTY_focusTraversable = 44;
    private static final int PROPERTY_focusTraversalKeys = 45;
    private static final int PROPERTY_focusTraversalKeysEnabled = 46;
    private static final int PROPERTY_focusTraversalPolicy = 47;
    private static final int PROPERTY_focusTraversalPolicyProvider = 48;
    private static final int PROPERTY_focusTraversalPolicySet = 49;
    private static final int PROPERTY_font = 50;
    private static final int PROPERTY_fontSet = 51;
    private static final int PROPERTY_foreground = 52;
    private static final int PROPERTY_foregroundSet = 53;
    private static final int PROPERTY_graphics = 54;
    private static final int PROPERTY_graphicsConfiguration = 55;
    private static final int PROPERTY_height = 56;
    private static final int PROPERTY_hierarchyBoundsListeners = 57;
    private static final int PROPERTY_hierarchyListeners = 58;
    private static final int PROPERTY_highlighter = 59;
    private static final int PROPERTY_horizontalAlignment = 60;
    private static final int PROPERTY_horizontalVisibility = 61;
    private static final int PROPERTY_ignoreRepaint = 62;
    private static final int PROPERTY_inheritsPopupMenu = 63;
    private static final int PROPERTY_inputContext = 64;
    private static final int PROPERTY_inputMap = 65;
    private static final int PROPERTY_inputMethodListeners = 66;
    private static final int PROPERTY_inputMethodRequests = 67;
    private static final int PROPERTY_inputVerifier = 68;
    private static final int PROPERTY_insets = 69;
    private static final int PROPERTY_keyListeners = 70;
    private static final int PROPERTY_keymap = 71;
    private static final int PROPERTY_layout = 72;
    private static final int PROPERTY_lightweight = 73;
    private static final int PROPERTY_locale = 74;
    private static final int PROPERTY_location = 75;
    private static final int PROPERTY_locationOnScreen = 76;
    private static final int PROPERTY_managingFocus = 77;
    private static final int PROPERTY_margin = 78;
    private static final int PROPERTY_maximumSize = 79;
    private static final int PROPERTY_maximumSizeSet = 80;
    private static final int PROPERTY_minimumSize = 81;
    private static final int PROPERTY_minimumSizeSet = 82;
    private static final int PROPERTY_mouseListeners = 83;
    private static final int PROPERTY_mouseMotionListeners = 84;
    private static final int PROPERTY_mousePosition = 85;
    private static final int PROPERTY_mouseWheelListeners = 86;
    private static final int PROPERTY_name = 87;
    private static final int PROPERTY_navigationFilter = 88;
    private static final int PROPERTY_nextFocusableComponent = 89;
    private static final int PROPERTY_opaque = 90;
    private static final int PROPERTY_optimizedDrawingEnabled = 91;
    private static final int PROPERTY_paintingTile = 92;
    private static final int PROPERTY_parent = 93;
    private static final int PROPERTY_peer = 94;
    private static final int PROPERTY_preferredScrollableViewportSize = 95;
    private static final int PROPERTY_preferredSize = 96;
    private static final int PROPERTY_preferredSizeSet = 97;
    private static final int PROPERTY_propertyChangeListeners = 98;
    private static final int PROPERTY_registeredKeyStrokes = 99;
    private static final int PROPERTY_requestFocusEnabled = 100;
    private static final int PROPERTY_rootPane = 101;
    private static final int PROPERTY_scrollableTracksViewportHeight = 102;
    private static final int PROPERTY_scrollableTracksViewportWidth = 103;
    private static final int PROPERTY_scrollOffset = 104;
    private static final int PROPERTY_selectedText = 105;
    private static final int PROPERTY_selectedTextColor = 106;
    private static final int PROPERTY_selectionColor = 107;
    private static final int PROPERTY_selectionEnd = 108;
    private static final int PROPERTY_selectionStart = 109;
    private static final int PROPERTY_showing = 110;
    private static final int PROPERTY_size = 111;
    private static final int PROPERTY_text = 112;
    private static final int PROPERTY_toolkit = 113;
    private static final int PROPERTY_toolTipText = 114;
    private static final int PROPERTY_topLevelAncestor = 115;
    private static final int PROPERTY_transferHandler = 116;
    private static final int PROPERTY_treeLock = 117;
    private static final int PROPERTY_UI = 118;
    private static final int PROPERTY_UIClassID = 119;
    private static final int PROPERTY_valid = 120;
    private static final int PROPERTY_validateRoot = 121;
    private static final int PROPERTY_value = 122;
    private static final int PROPERTY_verifyInputWhenFocusTarget = 123;
    private static final int PROPERTY_vetoableChangeListeners = 124;
    private static final int PROPERTY_visible = 125;
    private static final int PROPERTY_visibleRect = 126;
    private static final int PROPERTY_width = 127;
    private static final int PROPERTY_x = 128;
    private static final int PROPERTY_y = 129;

    // Property array 
    /*lazy PropertyDescriptor*/
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[130];
    
        try {
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", kpi.beans.BIFloatField.class, "getAccessibleContext", null );
            properties[PROPERTY_action] = new PropertyDescriptor ( "action", kpi.beans.BIFloatField.class, "getAction", "setAction" );
            properties[PROPERTY_actionCommand] = new PropertyDescriptor ( "actionCommand", kpi.beans.BIFloatField.class, null, "setActionCommand" );
            properties[PROPERTY_actionListeners] = new PropertyDescriptor ( "actionListeners", kpi.beans.BIFloatField.class, "getActionListeners", null );
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", kpi.beans.BIFloatField.class, "getActionMap", "setActionMap" );
            properties[PROPERTY_actions] = new PropertyDescriptor ( "actions", kpi.beans.BIFloatField.class, "getActions", null );
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", kpi.beans.BIFloatField.class, "getAlignmentX", "setAlignmentX" );
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", kpi.beans.BIFloatField.class, "getAlignmentY", "setAlignmentY" );
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", kpi.beans.BIFloatField.class, "getAncestorListeners", null );
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", kpi.beans.BIFloatField.class, "getAutoscrolls", "setAutoscrolls" );
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", kpi.beans.BIFloatField.class, "getBackground", "setBackground" );
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", kpi.beans.BIFloatField.class, "isBackgroundSet", null );
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", kpi.beans.BIFloatField.class, "getBorder", "setBorder" );
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", kpi.beans.BIFloatField.class, "getBounds", "setBounds" );
            properties[PROPERTY_caret] = new PropertyDescriptor ( "caret", kpi.beans.BIFloatField.class, "getCaret", "setCaret" );
            properties[PROPERTY_caretColor] = new PropertyDescriptor ( "caretColor", kpi.beans.BIFloatField.class, "getCaretColor", "setCaretColor" );
            properties[PROPERTY_caretListeners] = new PropertyDescriptor ( "caretListeners", kpi.beans.BIFloatField.class, "getCaretListeners", null );
            properties[PROPERTY_caretPosition] = new PropertyDescriptor ( "caretPosition", kpi.beans.BIFloatField.class, "getCaretPosition", "setCaretPosition" );
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", kpi.beans.BIFloatField.class, "getColorModel", null );
            properties[PROPERTY_columns] = new PropertyDescriptor ( "columns", kpi.beans.BIFloatField.class, "getColumns", "setColumns" );
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", kpi.beans.BIFloatField.class, null, null, "getComponent", null );
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", kpi.beans.BIFloatField.class, "getComponentCount", null );
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", kpi.beans.BIFloatField.class, "getComponentListeners", null );
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", kpi.beans.BIFloatField.class, "getComponentOrientation", "setComponentOrientation" );
            properties[PROPERTY_componentPopupMenu] = new PropertyDescriptor ( "componentPopupMenu", kpi.beans.BIFloatField.class, "getComponentPopupMenu", "setComponentPopupMenu" );
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", kpi.beans.BIFloatField.class, "getComponents", null );
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", kpi.beans.BIFloatField.class, "getContainerListeners", null );
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", kpi.beans.BIFloatField.class, "getCursor", "setCursor" );
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", kpi.beans.BIFloatField.class, "isCursorSet", null );
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", kpi.beans.BIFloatField.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" );
            properties[PROPERTY_disabledTextColor] = new PropertyDescriptor ( "disabledTextColor", kpi.beans.BIFloatField.class, "getDisabledTextColor", "setDisabledTextColor" );
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", kpi.beans.BIFloatField.class, "isDisplayable", null );
            properties[PROPERTY_document] = new PropertyDescriptor ( "document", kpi.beans.BIFloatField.class, "getDocument", "setDocument" );
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", kpi.beans.BIFloatField.class, "isDoubleBuffered", "setDoubleBuffered" );
            properties[PROPERTY_dragEnabled] = new PropertyDescriptor ( "dragEnabled", kpi.beans.BIFloatField.class, "getDragEnabled", "setDragEnabled" );
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", kpi.beans.BIFloatField.class, "getDropTarget", "setDropTarget" );
            properties[PROPERTY_editable] = new PropertyDescriptor ( "editable", kpi.beans.BIFloatField.class, "isEditable", "setEditable" );
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", kpi.beans.BIFloatField.class, "isEnabled", "setEnabled" );
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", kpi.beans.BIFloatField.class, "isFocusable", "setFocusable" );
            properties[PROPERTY_focusAccelerator] = new PropertyDescriptor ( "focusAccelerator", kpi.beans.BIFloatField.class, "getFocusAccelerator", "setFocusAccelerator" );
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", kpi.beans.BIFloatField.class, "isFocusCycleRoot", "setFocusCycleRoot" );
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", kpi.beans.BIFloatField.class, "getFocusCycleRootAncestor", null );
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", kpi.beans.BIFloatField.class, "getFocusListeners", null );
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", kpi.beans.BIFloatField.class, "isFocusOwner", null );
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", kpi.beans.BIFloatField.class, "isFocusTraversable", null );
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", kpi.beans.BIFloatField.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" );
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", kpi.beans.BIFloatField.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" );
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", kpi.beans.BIFloatField.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" );
            properties[PROPERTY_focusTraversalPolicyProvider] = new PropertyDescriptor ( "focusTraversalPolicyProvider", kpi.beans.BIFloatField.class, "isFocusTraversalPolicyProvider", "setFocusTraversalPolicyProvider" );
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", kpi.beans.BIFloatField.class, "isFocusTraversalPolicySet", null );
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", kpi.beans.BIFloatField.class, "getFont", "setFont" );
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", kpi.beans.BIFloatField.class, "isFontSet", null );
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", kpi.beans.BIFloatField.class, "getForeground", "setForeground" );
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", kpi.beans.BIFloatField.class, "isForegroundSet", null );
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", kpi.beans.BIFloatField.class, "getGraphics", null );
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", kpi.beans.BIFloatField.class, "getGraphicsConfiguration", null );
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", kpi.beans.BIFloatField.class, "getHeight", null );
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", kpi.beans.BIFloatField.class, "getHierarchyBoundsListeners", null );
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", kpi.beans.BIFloatField.class, "getHierarchyListeners", null );
            properties[PROPERTY_highlighter] = new PropertyDescriptor ( "highlighter", kpi.beans.BIFloatField.class, "getHighlighter", "setHighlighter" );
            properties[PROPERTY_horizontalAlignment] = new PropertyDescriptor ( "horizontalAlignment", kpi.beans.BIFloatField.class, "getHorizontalAlignment", "setHorizontalAlignment" );
            properties[PROPERTY_horizontalVisibility] = new PropertyDescriptor ( "horizontalVisibility", kpi.beans.BIFloatField.class, "getHorizontalVisibility", null );
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", kpi.beans.BIFloatField.class, "getIgnoreRepaint", "setIgnoreRepaint" );
            properties[PROPERTY_inheritsPopupMenu] = new PropertyDescriptor ( "inheritsPopupMenu", kpi.beans.BIFloatField.class, "getInheritsPopupMenu", "setInheritsPopupMenu" );
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", kpi.beans.BIFloatField.class, "getInputContext", null );
            properties[PROPERTY_inputMap] = new PropertyDescriptor ( "inputMap", kpi.beans.BIFloatField.class, "getInputMap", null );
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", kpi.beans.BIFloatField.class, "getInputMethodListeners", null );
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", kpi.beans.BIFloatField.class, "getInputMethodRequests", null );
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", kpi.beans.BIFloatField.class, "getInputVerifier", "setInputVerifier" );
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", kpi.beans.BIFloatField.class, "getInsets", null );
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", kpi.beans.BIFloatField.class, "getKeyListeners", null );
            properties[PROPERTY_keymap] = new PropertyDescriptor ( "keymap", kpi.beans.BIFloatField.class, "getKeymap", "setKeymap" );
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", kpi.beans.BIFloatField.class, "getLayout", "setLayout" );
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", kpi.beans.BIFloatField.class, "isLightweight", null );
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", kpi.beans.BIFloatField.class, "getLocale", "setLocale" );
            properties[PROPERTY_location] = new PropertyDescriptor ( "location", kpi.beans.BIFloatField.class, "getLocation", "setLocation" );
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", kpi.beans.BIFloatField.class, "getLocationOnScreen", null );
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", kpi.beans.BIFloatField.class, "isManagingFocus", null );
            properties[PROPERTY_margin] = new PropertyDescriptor ( "margin", kpi.beans.BIFloatField.class, "getMargin", "setMargin" );
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", kpi.beans.BIFloatField.class, "getMaximumSize", "setMaximumSize" );
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", kpi.beans.BIFloatField.class, "isMaximumSizeSet", null );
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", kpi.beans.BIFloatField.class, "getMinimumSize", "setMinimumSize" );
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", kpi.beans.BIFloatField.class, "isMinimumSizeSet", null );
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", kpi.beans.BIFloatField.class, "getMouseListeners", null );
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", kpi.beans.BIFloatField.class, "getMouseMotionListeners", null );
            properties[PROPERTY_mousePosition] = new PropertyDescriptor ( "mousePosition", kpi.beans.BIFloatField.class, "getMousePosition", null );
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", kpi.beans.BIFloatField.class, "getMouseWheelListeners", null );
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", kpi.beans.BIFloatField.class, "getName", "setName" );
            properties[PROPERTY_navigationFilter] = new PropertyDescriptor ( "navigationFilter", kpi.beans.BIFloatField.class, "getNavigationFilter", "setNavigationFilter" );
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", kpi.beans.BIFloatField.class, "getNextFocusableComponent", "setNextFocusableComponent" );
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", kpi.beans.BIFloatField.class, "isOpaque", "setOpaque" );
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", kpi.beans.BIFloatField.class, "isOptimizedDrawingEnabled", null );
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", kpi.beans.BIFloatField.class, "isPaintingTile", null );
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", kpi.beans.BIFloatField.class, "getParent", null );
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", kpi.beans.BIFloatField.class, "getPeer", null );
            properties[PROPERTY_preferredScrollableViewportSize] = new PropertyDescriptor ( "preferredScrollableViewportSize", kpi.beans.BIFloatField.class, "getPreferredScrollableViewportSize", null );
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", kpi.beans.BIFloatField.class, "getPreferredSize", "setPreferredSize" );
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", kpi.beans.BIFloatField.class, "isPreferredSizeSet", null );
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", kpi.beans.BIFloatField.class, "getPropertyChangeListeners", null );
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", kpi.beans.BIFloatField.class, "getRegisteredKeyStrokes", null );
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", kpi.beans.BIFloatField.class, "isRequestFocusEnabled", "setRequestFocusEnabled" );
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", kpi.beans.BIFloatField.class, "getRootPane", null );
            properties[PROPERTY_scrollableTracksViewportHeight] = new PropertyDescriptor ( "scrollableTracksViewportHeight", kpi.beans.BIFloatField.class, "getScrollableTracksViewportHeight", null );
            properties[PROPERTY_scrollableTracksViewportWidth] = new PropertyDescriptor ( "scrollableTracksViewportWidth", kpi.beans.BIFloatField.class, "getScrollableTracksViewportWidth", null );
            properties[PROPERTY_scrollOffset] = new PropertyDescriptor ( "scrollOffset", kpi.beans.BIFloatField.class, "getScrollOffset", "setScrollOffset" );
            properties[PROPERTY_selectedText] = new PropertyDescriptor ( "selectedText", kpi.beans.BIFloatField.class, "getSelectedText", null );
            properties[PROPERTY_selectedTextColor] = new PropertyDescriptor ( "selectedTextColor", kpi.beans.BIFloatField.class, "getSelectedTextColor", "setSelectedTextColor" );
            properties[PROPERTY_selectionColor] = new PropertyDescriptor ( "selectionColor", kpi.beans.BIFloatField.class, "getSelectionColor", "setSelectionColor" );
            properties[PROPERTY_selectionEnd] = new PropertyDescriptor ( "selectionEnd", kpi.beans.BIFloatField.class, "getSelectionEnd", "setSelectionEnd" );
            properties[PROPERTY_selectionStart] = new PropertyDescriptor ( "selectionStart", kpi.beans.BIFloatField.class, "getSelectionStart", "setSelectionStart" );
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", kpi.beans.BIFloatField.class, "isShowing", null );
            properties[PROPERTY_size] = new PropertyDescriptor ( "size", kpi.beans.BIFloatField.class, "getSize", "setSize" );
            properties[PROPERTY_text] = new PropertyDescriptor ( "text", kpi.beans.BIFloatField.class, "getText", "setText" );
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", kpi.beans.BIFloatField.class, "getToolkit", null );
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", kpi.beans.BIFloatField.class, "getToolTipText", "setToolTipText" );
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", kpi.beans.BIFloatField.class, "getTopLevelAncestor", null );
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", kpi.beans.BIFloatField.class, "getTransferHandler", "setTransferHandler" );
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", kpi.beans.BIFloatField.class, "getTreeLock", null );
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", kpi.beans.BIFloatField.class, "getUI", "setUI" );
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", kpi.beans.BIFloatField.class, "getUIClassID", null );
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", kpi.beans.BIFloatField.class, "isValid", null );
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", kpi.beans.BIFloatField.class, "isValidateRoot", null );
            properties[PROPERTY_value] = new PropertyDescriptor ( "value", kpi.beans.BIFloatField.class, "getValue", "setValue" );
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", kpi.beans.BIFloatField.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" );
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", kpi.beans.BIFloatField.class, "getVetoableChangeListeners", null );
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", kpi.beans.BIFloatField.class, "isVisible", "setVisible" );
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", kpi.beans.BIFloatField.class, "getVisibleRect", null );
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", kpi.beans.BIFloatField.class, "getWidth", null );
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", kpi.beans.BIFloatField.class, "getX", null );
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", kpi.beans.BIFloatField.class, "getY", null );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Properties
	
	// Here you can add code for customizing the properties array.
	
        return properties;         }//GEN-LAST:Properties
    
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_actionListener = 0;
    private static final int EVENT_ancestorListener = 1;
    private static final int EVENT_caretListener = 2;
    private static final int EVENT_componentListener = 3;
    private static final int EVENT_containerListener = 4;
    private static final int EVENT_focusListener = 5;
    private static final int EVENT_hierarchyBoundsListener = 6;
    private static final int EVENT_hierarchyListener = 7;
    private static final int EVENT_inputMethodListener = 8;
    private static final int EVENT_keyListener = 9;
    private static final int EVENT_mouseListener = 10;
    private static final int EVENT_mouseMotionListener = 11;
    private static final int EVENT_mouseWheelListener = 12;
    private static final int EVENT_propertyChangeListener = 13;
    private static final int EVENT_vetoableChangeListener = 14;

    // EventSet array
    /*lazy EventSetDescriptor*/
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[15];
    
            try {
            eventSets[EVENT_actionListener] = new EventSetDescriptor ( kpi.beans.BIFloatField.class, "actionListener", java.awt.event.ActionListener.class, new String[] {"actionPerformed"}, "addActionListener", "removeActionListener" );
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( kpi.beans.BIFloatField.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorAdded", "ancestorMoved", "ancestorRemoved"}, "addAncestorListener", "removeAncestorListener" );
            eventSets[EVENT_caretListener] = new EventSetDescriptor ( kpi.beans.BIFloatField.class, "caretListener", javax.swing.event.CaretListener.class, new String[] {"caretUpdate"}, "addCaretListener", "removeCaretListener" );
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( kpi.beans.BIFloatField.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentHidden", "componentMoved", "componentResized", "componentShown"}, "addComponentListener", "removeComponentListener" );
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( kpi.beans.BIFloatField.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" );
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( kpi.beans.BIFloatField.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusGained", "focusLost"}, "addFocusListener", "removeFocusListener" );
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( kpi.beans.BIFloatField.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" );
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( kpi.beans.BIFloatField.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" );
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( kpi.beans.BIFloatField.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" );
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( kpi.beans.BIFloatField.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyReleased", "keyTyped"}, "addKeyListener", "removeKeyListener" );
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( kpi.beans.BIFloatField.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mouseEntered", "mouseExited", "mousePressed", "mouseReleased"}, "addMouseListener", "removeMouseListener" );
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( kpi.beans.BIFloatField.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" );
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( kpi.beans.BIFloatField.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" );
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( kpi.beans.BIFloatField.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" );
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( kpi.beans.BIFloatField.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Events
	
	// Here you can add code for customizing the event sets array.
	
        return eventSets;         }//GEN-LAST:Events
    
    // Method identifiers//GEN-FIRST:Methods
    private static final int METHOD_action0 = 0;
    private static final int METHOD_add1 = 1;
    private static final int METHOD_addKeymap2 = 2;
    private static final int METHOD_addNotify3 = 3;
    private static final int METHOD_addPropertyChangeListener4 = 4;
    private static final int METHOD_applyComponentOrientation5 = 5;
    private static final int METHOD_areFocusTraversalKeysSet6 = 6;
    private static final int METHOD_bounds7 = 7;
    private static final int METHOD_checkImage8 = 8;
    private static final int METHOD_computeVisibleRect9 = 9;
    private static final int METHOD_contains10 = 10;
    private static final int METHOD_copy11 = 11;
    private static final int METHOD_countComponents12 = 12;
    private static final int METHOD_createImage13 = 13;
    private static final int METHOD_createToolTip14 = 14;
    private static final int METHOD_createVolatileImage15 = 15;
    private static final int METHOD_cut16 = 16;
    private static final int METHOD_deliverEvent17 = 17;
    private static final int METHOD_disable18 = 18;
    private static final int METHOD_dispatchEvent19 = 19;
    private static final int METHOD_doLayout20 = 20;
    private static final int METHOD_enable21 = 21;
    private static final int METHOD_enableInputMethods22 = 22;
    private static final int METHOD_findComponentAt23 = 23;
    private static final int METHOD_firePropertyChange24 = 24;
    private static final int METHOD_getActionForKeyStroke25 = 25;
    private static final int METHOD_getBounds26 = 26;
    private static final int METHOD_getClientProperty27 = 27;
    private static final int METHOD_getComponentAt28 = 28;
    private static final int METHOD_getComponentZOrder29 = 29;
    private static final int METHOD_getConditionForKeyStroke30 = 30;
    private static final int METHOD_getDefaultLocale31 = 31;
    private static final int METHOD_getFontMetrics32 = 32;
    private static final int METHOD_getInsets33 = 33;
    private static final int METHOD_getKeymap34 = 34;
    private static final int METHOD_getListeners35 = 35;
    private static final int METHOD_getLocation36 = 36;
    private static final int METHOD_getMousePosition37 = 37;
    private static final int METHOD_getPopupLocation38 = 38;
    private static final int METHOD_getPropertyChangeListeners39 = 39;
    private static final int METHOD_getScrollableBlockIncrement40 = 40;
    private static final int METHOD_getScrollableUnitIncrement41 = 41;
    private static final int METHOD_getSize42 = 42;
    private static final int METHOD_getText43 = 43;
    private static final int METHOD_getToolTipLocation44 = 44;
    private static final int METHOD_getToolTipText45 = 45;
    private static final int METHOD_gotFocus46 = 46;
    private static final int METHOD_grabFocus47 = 47;
    private static final int METHOD_handleEvent48 = 48;
    private static final int METHOD_hasFocus49 = 49;
    private static final int METHOD_hide50 = 50;
    private static final int METHOD_imageUpdate51 = 51;
    private static final int METHOD_insets52 = 52;
    private static final int METHOD_inside53 = 53;
    private static final int METHOD_invalidate54 = 54;
    private static final int METHOD_isAncestorOf55 = 55;
    private static final int METHOD_isFocusCycleRoot56 = 56;
    private static final int METHOD_isLightweightComponent57 = 57;
    private static final int METHOD_keyDown58 = 58;
    private static final int METHOD_keyUp59 = 59;
    private static final int METHOD_layout60 = 60;
    private static final int METHOD_list61 = 61;
    private static final int METHOD_loadKeymap62 = 62;
    private static final int METHOD_locate63 = 63;
    private static final int METHOD_location64 = 64;
    private static final int METHOD_lostFocus65 = 65;
    private static final int METHOD_minimumSize66 = 66;
    private static final int METHOD_modelToView67 = 67;
    private static final int METHOD_mouseDown68 = 68;
    private static final int METHOD_mouseDrag69 = 69;
    private static final int METHOD_mouseEnter70 = 70;
    private static final int METHOD_mouseExit71 = 71;
    private static final int METHOD_mouseMove72 = 72;
    private static final int METHOD_mouseUp73 = 73;
    private static final int METHOD_move74 = 74;
    private static final int METHOD_moveCaretPosition75 = 75;
    private static final int METHOD_nextFocus76 = 76;
    private static final int METHOD_paint77 = 77;
    private static final int METHOD_paintAll78 = 78;
    private static final int METHOD_paintComponents79 = 79;
    private static final int METHOD_paintImmediately80 = 80;
    private static final int METHOD_paste81 = 81;
    private static final int METHOD_postActionEvent82 = 82;
    private static final int METHOD_postEvent83 = 83;
    private static final int METHOD_preferredSize84 = 84;
    private static final int METHOD_prepareImage85 = 85;
    private static final int METHOD_print86 = 86;
    private static final int METHOD_printAll87 = 87;
    private static final int METHOD_printComponents88 = 88;
    private static final int METHOD_putClientProperty89 = 89;
    private static final int METHOD_read90 = 90;
    private static final int METHOD_registerKeyboardAction91 = 91;
    private static final int METHOD_remove92 = 92;
    private static final int METHOD_removeAll93 = 93;
    private static final int METHOD_removeKeymap94 = 94;
    private static final int METHOD_removeNotify95 = 95;
    private static final int METHOD_removePropertyChangeListener96 = 96;
    private static final int METHOD_repaint97 = 97;
    private static final int METHOD_replaceSelection98 = 98;
    private static final int METHOD_requestDefaultFocus99 = 99;
    private static final int METHOD_requestFocus100 = 100;
    private static final int METHOD_requestFocusInWindow101 = 101;
    private static final int METHOD_resetKeyboardActions102 = 102;
    private static final int METHOD_reshape103 = 103;
    private static final int METHOD_resize104 = 104;
    private static final int METHOD_revalidate105 = 105;
    private static final int METHOD_scrollRectToVisible106 = 106;
    private static final int METHOD_select107 = 107;
    private static final int METHOD_selectAll108 = 108;
    private static final int METHOD_setBounds109 = 109;
    private static final int METHOD_setComponentZOrder110 = 110;
    private static final int METHOD_setDefaultLocale111 = 111;
    private static final int METHOD_show112 = 112;
    private static final int METHOD_size113 = 113;
    private static final int METHOD_toString114 = 114;
    private static final int METHOD_transferFocus115 = 115;
    private static final int METHOD_transferFocusBackward116 = 116;
    private static final int METHOD_transferFocusDownCycle117 = 117;
    private static final int METHOD_transferFocusUpCycle118 = 118;
    private static final int METHOD_unregisterKeyboardAction119 = 119;
    private static final int METHOD_update120 = 120;
    private static final int METHOD_updateUI121 = 121;
    private static final int METHOD_validate122 = 122;
    private static final int METHOD_viewToModel123 = 123;
    private static final int METHOD_write124 = 124;

    // Method array 
    /*lazy MethodDescriptor*/
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[125];
    
        try {
            methods[METHOD_action0] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_action0].setDisplayName ( "" );
            methods[METHOD_add1] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("add", new Class[] {java.awt.Component.class}));
            methods[METHOD_add1].setDisplayName ( "" );
            methods[METHOD_addKeymap2] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("addKeymap", new Class[] {java.lang.String.class, javax.swing.text.Keymap.class}));
            methods[METHOD_addKeymap2].setDisplayName ( "" );
            methods[METHOD_addNotify3] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("addNotify", new Class[] {}));
            methods[METHOD_addNotify3].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener4] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_addPropertyChangeListener4].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation5] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class}));
            methods[METHOD_applyComponentOrientation5].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet6] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE}));
            methods[METHOD_areFocusTraversalKeysSet6].setDisplayName ( "" );
            methods[METHOD_bounds7] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("bounds", new Class[] {}));
            methods[METHOD_bounds7].setDisplayName ( "" );
            methods[METHOD_checkImage8] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage8].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect9] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_computeVisibleRect9].setDisplayName ( "" );
            methods[METHOD_contains10] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_contains10].setDisplayName ( "" );
            methods[METHOD_copy11] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("copy", new Class[] {}));
            methods[METHOD_copy11].setDisplayName ( "" );
            methods[METHOD_countComponents12] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("countComponents", new Class[] {}));
            methods[METHOD_countComponents12].setDisplayName ( "" );
            methods[METHOD_createImage13] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class}));
            methods[METHOD_createImage13].setDisplayName ( "" );
            methods[METHOD_createToolTip14] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("createToolTip", new Class[] {}));
            methods[METHOD_createToolTip14].setDisplayName ( "" );
            methods[METHOD_createVolatileImage15] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createVolatileImage15].setDisplayName ( "" );
            methods[METHOD_cut16] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("cut", new Class[] {}));
            methods[METHOD_cut16].setDisplayName ( "" );
            methods[METHOD_deliverEvent17] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_deliverEvent17].setDisplayName ( "" );
            methods[METHOD_disable18] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("disable", new Class[] {}));
            methods[METHOD_disable18].setDisplayName ( "" );
            methods[METHOD_dispatchEvent19] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class}));
            methods[METHOD_dispatchEvent19].setDisplayName ( "" );
            methods[METHOD_doLayout20] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("doLayout", new Class[] {}));
            methods[METHOD_doLayout20].setDisplayName ( "" );
            methods[METHOD_enable21] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("enable", new Class[] {}));
            methods[METHOD_enable21].setDisplayName ( "" );
            methods[METHOD_enableInputMethods22] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE}));
            methods[METHOD_enableInputMethods22].setDisplayName ( "" );
            methods[METHOD_findComponentAt23] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_findComponentAt23].setDisplayName ( "" );
            methods[METHOD_firePropertyChange24] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Boolean.TYPE, Boolean.TYPE}));
            methods[METHOD_firePropertyChange24].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke25] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getActionForKeyStroke25].setDisplayName ( "" );
            methods[METHOD_getBounds26] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_getBounds26].setDisplayName ( "" );
            methods[METHOD_getClientProperty27] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class}));
            methods[METHOD_getClientProperty27].setDisplayName ( "" );
            methods[METHOD_getComponentAt28] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getComponentAt28].setDisplayName ( "" );
            methods[METHOD_getComponentZOrder29] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getComponentZOrder", new Class[] {java.awt.Component.class}));
            methods[METHOD_getComponentZOrder29].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke30] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getConditionForKeyStroke30].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale31] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getDefaultLocale", new Class[] {}));
            methods[METHOD_getDefaultLocale31].setDisplayName ( "" );
            methods[METHOD_getFontMetrics32] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class}));
            methods[METHOD_getFontMetrics32].setDisplayName ( "" );
            methods[METHOD_getInsets33] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getInsets", new Class[] {java.awt.Insets.class}));
            methods[METHOD_getInsets33].setDisplayName ( "" );
            methods[METHOD_getKeymap34] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getKeymap", new Class[] {java.lang.String.class}));
            methods[METHOD_getKeymap34].setDisplayName ( "" );
            methods[METHOD_getListeners35] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getListeners", new Class[] {java.lang.Class.class}));
            methods[METHOD_getListeners35].setDisplayName ( "" );
            methods[METHOD_getLocation36] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_getLocation36].setDisplayName ( "" );
            methods[METHOD_getMousePosition37] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getMousePosition", new Class[] {Boolean.TYPE}));
            methods[METHOD_getMousePosition37].setDisplayName ( "" );
            methods[METHOD_getPopupLocation38] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getPopupLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getPopupLocation38].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners39] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class}));
            methods[METHOD_getPropertyChangeListeners39].setDisplayName ( "" );
            methods[METHOD_getScrollableBlockIncrement40] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getScrollableBlockIncrement", new Class[] {java.awt.Rectangle.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getScrollableBlockIncrement40].setDisplayName ( "" );
            methods[METHOD_getScrollableUnitIncrement41] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getScrollableUnitIncrement", new Class[] {java.awt.Rectangle.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getScrollableUnitIncrement41].setDisplayName ( "" );
            methods[METHOD_getSize42] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_getSize42].setDisplayName ( "" );
            methods[METHOD_getText43] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getText", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getText43].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation44] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipLocation44].setDisplayName ( "" );
            methods[METHOD_getToolTipText45] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipText45].setDisplayName ( "" );
            methods[METHOD_gotFocus46] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_gotFocus46].setDisplayName ( "" );
            methods[METHOD_grabFocus47] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("grabFocus", new Class[] {}));
            methods[METHOD_grabFocus47].setDisplayName ( "" );
            methods[METHOD_handleEvent48] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("handleEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_handleEvent48].setDisplayName ( "" );
            methods[METHOD_hasFocus49] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("hasFocus", new Class[] {}));
            methods[METHOD_hasFocus49].setDisplayName ( "" );
            methods[METHOD_hide50] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("hide", new Class[] {}));
            methods[METHOD_hide50].setDisplayName ( "" );
            methods[METHOD_imageUpdate51] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_imageUpdate51].setDisplayName ( "" );
            methods[METHOD_insets52] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("insets", new Class[] {}));
            methods[METHOD_insets52].setDisplayName ( "" );
            methods[METHOD_inside53] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_inside53].setDisplayName ( "" );
            methods[METHOD_invalidate54] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("invalidate", new Class[] {}));
            methods[METHOD_invalidate54].setDisplayName ( "" );
            methods[METHOD_isAncestorOf55] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_isAncestorOf55].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot56] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class}));
            methods[METHOD_isFocusCycleRoot56].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent57] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class}));
            methods[METHOD_isLightweightComponent57].setDisplayName ( "" );
            methods[METHOD_keyDown58] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyDown58].setDisplayName ( "" );
            methods[METHOD_keyUp59] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyUp59].setDisplayName ( "" );
            methods[METHOD_layout60] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("layout", new Class[] {}));
            methods[METHOD_layout60].setDisplayName ( "" );
            methods[METHOD_list61] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE}));
            methods[METHOD_list61].setDisplayName ( "" );
            methods[METHOD_loadKeymap62] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("loadKeymap", new Class[] {javax.swing.text.Keymap.class, javax.swing.text.JTextComponent.KeyBinding[].class, javax.swing.Action[].class}));
            methods[METHOD_loadKeymap62].setDisplayName ( "" );
            methods[METHOD_locate63] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_locate63].setDisplayName ( "" );
            methods[METHOD_location64] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("location", new Class[] {}));
            methods[METHOD_location64].setDisplayName ( "" );
            methods[METHOD_lostFocus65] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_lostFocus65].setDisplayName ( "" );
            methods[METHOD_minimumSize66] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("minimumSize", new Class[] {}));
            methods[METHOD_minimumSize66].setDisplayName ( "" );
            methods[METHOD_modelToView67] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("modelToView", new Class[] {Integer.TYPE}));
            methods[METHOD_modelToView67].setDisplayName ( "" );
            methods[METHOD_mouseDown68] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDown68].setDisplayName ( "" );
            methods[METHOD_mouseDrag69] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDrag69].setDisplayName ( "" );
            methods[METHOD_mouseEnter70] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseEnter70].setDisplayName ( "" );
            methods[METHOD_mouseExit71] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseExit71].setDisplayName ( "" );
            methods[METHOD_mouseMove72] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseMove72].setDisplayName ( "" );
            methods[METHOD_mouseUp73] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseUp73].setDisplayName ( "" );
            methods[METHOD_move74] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_move74].setDisplayName ( "" );
            methods[METHOD_moveCaretPosition75] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("moveCaretPosition", new Class[] {Integer.TYPE}));
            methods[METHOD_moveCaretPosition75].setDisplayName ( "" );
            methods[METHOD_nextFocus76] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("nextFocus", new Class[] {}));
            methods[METHOD_nextFocus76].setDisplayName ( "" );
            methods[METHOD_paint77] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("paint", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paint77].setDisplayName ( "" );
            methods[METHOD_paintAll78] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintAll78].setDisplayName ( "" );
            methods[METHOD_paintComponents79] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintComponents79].setDisplayName ( "" );
            methods[METHOD_paintImmediately80] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_paintImmediately80].setDisplayName ( "" );
            methods[METHOD_paste81] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("paste", new Class[] {}));
            methods[METHOD_paste81].setDisplayName ( "" );
            methods[METHOD_postActionEvent82] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("postActionEvent", new Class[] {}));
            methods[METHOD_postActionEvent82].setDisplayName ( "" );
            methods[METHOD_postEvent83] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("postEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_postEvent83].setDisplayName ( "" );
            methods[METHOD_preferredSize84] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("preferredSize", new Class[] {}));
            methods[METHOD_preferredSize84].setDisplayName ( "" );
            methods[METHOD_prepareImage85] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage85].setDisplayName ( "" );
            methods[METHOD_print86] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("print", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_print86].setDisplayName ( "" );
            methods[METHOD_printAll87] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("printAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printAll87].setDisplayName ( "" );
            methods[METHOD_printComponents88] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printComponents88].setDisplayName ( "" );
            methods[METHOD_putClientProperty89] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class}));
            methods[METHOD_putClientProperty89].setDisplayName ( "" );
            methods[METHOD_read90] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("read", new Class[] {java.io.Reader.class, java.lang.Object.class}));
            methods[METHOD_read90].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction91] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction91].setDisplayName ( "" );
            methods[METHOD_remove92] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("remove", new Class[] {Integer.TYPE}));
            methods[METHOD_remove92].setDisplayName ( "" );
            methods[METHOD_removeAll93] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("removeAll", new Class[] {}));
            methods[METHOD_removeAll93].setDisplayName ( "" );
            methods[METHOD_removeKeymap94] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("removeKeymap", new Class[] {java.lang.String.class}));
            methods[METHOD_removeKeymap94].setDisplayName ( "" );
            methods[METHOD_removeNotify95] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("removeNotify", new Class[] {}));
            methods[METHOD_removeNotify95].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener96] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_removePropertyChangeListener96].setDisplayName ( "" );
            methods[METHOD_repaint97] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint97].setDisplayName ( "" );
            methods[METHOD_replaceSelection98] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("replaceSelection", new Class[] {java.lang.String.class}));
            methods[METHOD_replaceSelection98].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus99] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("requestDefaultFocus", new Class[] {}));
            methods[METHOD_requestDefaultFocus99].setDisplayName ( "" );
            methods[METHOD_requestFocus100] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("requestFocus", new Class[] {}));
            methods[METHOD_requestFocus100].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow101] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("requestFocusInWindow", new Class[] {}));
            methods[METHOD_requestFocusInWindow101].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions102] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("resetKeyboardActions", new Class[] {}));
            methods[METHOD_resetKeyboardActions102].setDisplayName ( "" );
            methods[METHOD_reshape103] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_reshape103].setDisplayName ( "" );
            methods[METHOD_resize104] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_resize104].setDisplayName ( "" );
            methods[METHOD_revalidate105] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("revalidate", new Class[] {}));
            methods[METHOD_revalidate105].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible106] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_scrollRectToVisible106].setDisplayName ( "" );
            methods[METHOD_select107] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("select", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_select107].setDisplayName ( "" );
            methods[METHOD_selectAll108] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("selectAll", new Class[] {}));
            methods[METHOD_selectAll108].setDisplayName ( "" );
            methods[METHOD_setBounds109] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setBounds109].setDisplayName ( "" );
            methods[METHOD_setComponentZOrder110] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("setComponentZOrder", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_setComponentZOrder110].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale111] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class}));
            methods[METHOD_setDefaultLocale111].setDisplayName ( "" );
            methods[METHOD_show112] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("show", new Class[] {}));
            methods[METHOD_show112].setDisplayName ( "" );
            methods[METHOD_size113] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("size", new Class[] {}));
            methods[METHOD_size113].setDisplayName ( "" );
            methods[METHOD_toString114] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("toString", new Class[] {}));
            methods[METHOD_toString114].setDisplayName ( "" );
            methods[METHOD_transferFocus115] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("transferFocus", new Class[] {}));
            methods[METHOD_transferFocus115].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward116] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("transferFocusBackward", new Class[] {}));
            methods[METHOD_transferFocusBackward116].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle117] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("transferFocusDownCycle", new Class[] {}));
            methods[METHOD_transferFocusDownCycle117].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle118] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("transferFocusUpCycle", new Class[] {}));
            methods[METHOD_transferFocusUpCycle118].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction119] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_unregisterKeyboardAction119].setDisplayName ( "" );
            methods[METHOD_update120] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("update", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_update120].setDisplayName ( "" );
            methods[METHOD_updateUI121] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("updateUI", new Class[] {}));
            methods[METHOD_updateUI121].setDisplayName ( "" );
            methods[METHOD_validate122] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("validate", new Class[] {}));
            methods[METHOD_validate122].setDisplayName ( "" );
            methods[METHOD_viewToModel123] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("viewToModel", new Class[] {java.awt.Point.class}));
            methods[METHOD_viewToModel123].setDisplayName ( "" );
            methods[METHOD_write124] = new MethodDescriptor ( kpi.beans.BIFloatField.class.getMethod("write", new Class[] {java.io.Writer.class}));
            methods[METHOD_write124].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
	
	// Here you can add code for customizing the methods array.
	
        return methods;         }//GEN-LAST:Methods
    
    
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
    
    
//GEN-FIRST:Superclass
    
    // Here you can add code for customizing the Superclass BeanInfo.
    
//GEN-LAST:Superclass
    
    /**
     * Gets the bean's <code>BeanDescriptor</code>s.
     *
     * @return BeanDescriptor describing the editable
     * properties of this bean.  May return null if the
     * information should be obtained by automatic analysis.
     */
    public BeanDescriptor getBeanDescriptor() {
	return getBdescriptor();
    }
    
    /**
     * Gets the bean's <code>PropertyDescriptor</code>s.
     *
     * @return An array of PropertyDescriptors describing the editable
     * properties supported by this bean.  May return null if the
     * information should be obtained by automatic analysis.
     * <p>
     * If a property is indexed, then its entry in the result array will
     * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
     * A client of getPropertyDescriptors can use "instanceof" to check
     * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
     */
    public PropertyDescriptor[] getPropertyDescriptors() {
	return getPdescriptor();
    }
    
    /**
     * Gets the bean's <code>EventSetDescriptor</code>s.
     *
     * @return  An array of EventSetDescriptors describing the kinds of
     * events fired by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public EventSetDescriptor[] getEventSetDescriptors() {
	return getEdescriptor();
    }
    
    /**
     * Gets the bean's <code>MethodDescriptor</code>s.
     *
     * @return  An array of MethodDescriptors describing the methods
     * implemented by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public MethodDescriptor[] getMethodDescriptors() {
	return getMdescriptor();
    }
    
    /**
     * A bean may have a "default" property that is the property that will
     * mostly commonly be initially chosen for update by human's who are
     * customizing the bean.
     * @return  Index of default property in the PropertyDescriptor array
     * 		returned by getPropertyDescriptors.
     * <P>	Returns -1 if there is no default property.
     */
    public int getDefaultPropertyIndex() {
	return defaultPropertyIndex;
    }
    
    /**
     * A bean may have a "default" event that is the event that will
     * mostly commonly be used by human's when using the bean.
     * @return Index of default event in the EventSetDescriptor array
     *		returned by getEventSetDescriptors.
     * <P>	Returns -1 if there is no default event.
     */
    public int getDefaultEventIndex() {
	return defaultEventIndex;
    }
}

