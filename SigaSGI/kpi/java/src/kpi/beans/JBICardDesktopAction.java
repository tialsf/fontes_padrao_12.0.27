package kpi.beans;

import javax.swing.JInternalFrame;

/**
 *
 * @author lucio.pelinson
 */
public interface JBICardDesktopAction {

	/**
	 * Invoked when the component has focus.
	 */
	public void onActivateCard(JInternalFrame f);
	
}
