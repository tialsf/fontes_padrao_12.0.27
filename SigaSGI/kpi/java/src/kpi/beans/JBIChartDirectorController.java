package kpi.beans;

import ChartDirector.BarLayer;
import ChartDirector.BaseChart;
import ChartDirector.Chart;
import ChartDirector.DataSet;
import ChartDirector.LegendBox;
import ChartDirector.TextBox;
import java.awt.Color;
import kpi.swing.graph.KpiGraphIndicador;

/**
 * Classe para controle das a��es do gr�fico ChartDirector
 * created by Alexandre Silva 14/09/2006
 */
public class JBIChartDirectorController {

    //Tipos dos gr�ficos.
    public final static int XY_CHART = 0;
    public final static int PIE_CHART = 1;
    public final static int STATUS_CHART = 2;
    public final static int POLAR_CHART = 3;
    public final static int BASE_METER = 4;
    public final static int MULTI_CHART = 5;
    //Gr�ficos do tipo XYChart devem estar no internalo de 0 x 99
    public final static int LINE = 0;
    public final static int LINE_3D = 1;
    public final static int BAR = 2;
    public final static int BAR_3D = 3;
    public final static int CYLINDER_3D = 4;
    public final static int DEFAULT_BARRA = 5;
    public final static int DEFAULT_LINE = 6;
    public final static int MULTI_COLOR_XY = 99;
    //Gr�ficos do tipo PieChart. devem estar no internalo de 100 x 199
    public final static int PIE = 100;
    public final static int PIE_3D = 101;
    //Posicionamentos
    public final static int TOP = 0;
    public final static int DOWN = 1;
    public final static int RIGHT = 2;
    public final static int LEFT = 3;
    //Layers para os gr�ficos
    private ChartDirector.ChartViewer oChartViewer = null;
    private ChartDirector.XYChart oXYChart = null;
    private ChartDirector.XYChart oStatusChart = null;
    private ChartDirector.LineLayer oXYLineLayer = null;
    private ChartDirector.LineLayer oXYLineLayer3D = null;
    private ChartDirector.BarLayer oXYBarLayer = null;
    private ChartDirector.BarLayer oXYBarLayer3D = null;
    private ChartDirector.BarLayer oXYBarLayerColor = null;//So e possivel adicionar, uma barra de cores personalizadas por gr�fico.
    private ChartDirector.BarLayer oXYCylinderLayer = null;
    private ChartDirector.PieChart oPieChart = null;
    //Dados do gr�fico.
    private int graphType = XY_CHART;
    private int legendPos = TOP;
    private int graphWidth = 0;
    private int graphHeight = 0;
    //Guarda todas as s�ries que foram adicionadas ao gr�fico.
    private java.util.HashMap series = new java.util.HashMap();
    private java.util.HashMap legendasRemoved = new java.util.HashMap();
    //Array com as legendas
    private String[] legendas = null;
    //Array com as faixas de valores
    private int[] rangeValues = null;
    //Cor do fundo do gr�fico
    private java.awt.Color backColor = Color.white;
    //Legendas
    private String useRangeValues = "";
    private String yLegenda = "";
    private String xLegenda = "";
    private String titleXY = "";
    private String titlePie = "";
    private String titleOrient = "";
    private String titleUnitMeasure = "";
    private int legValAngle = 0;
    //Troca o eixo x po y
    private boolean swapXY = false;
    private boolean legVisible = false;
    private boolean inTrasanction = false;
    private boolean legXvisible = false;
    private boolean legYvisible = false;
    private boolean legValvisible = false;
    //Fonte do t�tulo do gr�fico
    private java.awt.Font eixoFont = new java.awt.Font("Tahoma", java.awt.Font.BOLD, 8);
    private java.awt.Font agregadedLabel = new java.awt.Font("Tahoma", java.awt.Font.BOLD, 8);

    /**
     * Construtor que recebe um ChartViewer.
     * @param oViewer Vis�o onde ser� desenhado o gr�fico.
     */
    public JBIChartDirectorController(ChartDirector.ChartViewer oViewer) {
        setChartViewer(oViewer);
    }

    /**
     * Configura o tipo do gr�fico
     * @param type Tipos poss�veis:
     * XY_CHART	= 0;
     * PIE_CHART	= 1;
     * POLAR_CHART	= 2;
     * BASE_METER	= 3;
     * MULTI_CHART	= 4;
     */
    public void setGraphType(int type) {
        this.graphType = type;
    }

    /**
     * Seta a vis�o do gr�fico.
     * @param oChartViewer Vis�o que ser� desenhada o gr�fico.
     */
    public void setChartViewer(ChartDirector.ChartViewer oChartViewer) {
        this.oChartViewer = oChartViewer;
    }

    /**
     * Retorna a vis�o deste gr�fico.
     * @return ChartViewer - Vis�o deste gr�fico.
     */
    public ChartDirector.ChartViewer getChartViewer() {
        return oChartViewer;
    }

    /**
     * Tramanho do gr�fico.
     * @param width Largura
     * @param height Altura
     */
    public void setSize(int width, int height) {
        graphWidth = width;
        graphHeight = height;
    }

    /**
     * Adiciona uma nova s�rie.
     * @param serieKey Chave da s�rie.
     * @param valSerie Valores da s�rie.
     * @param serieType Tipo da s�rie.
     * @param color Cor da s�rie.
     */
    public void addSerie(int serieKey, double[] valSerie, int serieType, java.awt.Color color) {
        JBIChartDirectorSerie newSerie = new JBIChartDirectorSerie(valSerie);
        newSerie.setSerieType(serieType);
        newSerie.setColor(color);
        newSerie.setSerieID(serieKey);
        series.put(serieKey, newSerie);
    }

    /**
     * Adiciona uma nova s�rie
     * @param serieKey Chave da s�rie.
     * @param valSerie Valor da s�rie.
     */
    public void addSerie(int serieKey, double[] valSerie) {
        JBIChartDirectorSerie newSerie = new JBIChartDirectorSerie(valSerie);
        newSerie.setSerieID(serieKey);
        series.put(serieKey, newSerie);
    }

    /**
     * Retorna a s�rie pedida.
     * @param serieKey Chave da s�rie.
     * @return Um objetodo tipo JBIChartDirectorSerie.
     */
    public JBIChartDirectorSerie getSerieItem(int serieKey) {
        return (JBIChartDirectorSerie) series.get(serieKey);
    }

    /**
     * Configura o tipo da s�rie.
     * @param serieKey Chave da s�rie.
     * @param serieType Tipo da s�rie.
     */
    public void setSerieType(int serieKey, int serieType) {
        if (series != null) {
            JBIChartDirectorSerie serie = (JBIChartDirectorSerie) series.get(serieKey);
            if (serie != null) {
                serie.setSerieType(serieType);
            }
        }
    }

    /**
     * Atualiza um s�rie especifica.
     * @param serie Chave da s�rie.
     */
    public void updateGraph(int serie) {

        JBIChartDirectorSerie serieItem = getSerieItem(serie);

        if (serieItem.isVisible()) {
            addSerieToChart(serieItem);
        }
    }

    /**
     * Atualiza todas as s�ries do gr�fico.
     */
    public void updateGraph() {
        if (!isTrasanction()) {

            this.resetChart();

            //Previa
            if (series.containsKey(KpiGraphIndicador.PREVIA)) {
                JBIChartDirectorSerie serie = (JBIChartDirectorSerie) series.get(KpiGraphIndicador.PREVIA);
                if (serie.isVisible()) {
                    addSerieToChart(serie);
                }
            }

            //Previa Acumulada
            if (series.containsKey(KpiGraphIndicador.PREVIA_ACUM)) {
                JBIChartDirectorSerie serie = (JBIChartDirectorSerie) series.get(KpiGraphIndicador.PREVIA_ACUM);
                if (serie.isVisible()) {
                    addSerieToChart(serie);
                }
            }

            //Diferen�a
            if (series.containsKey(KpiGraphIndicador.DIFERENCA)) {
                JBIChartDirectorSerie serie = (JBIChartDirectorSerie) series.get(KpiGraphIndicador.DIFERENCA);
                if (serie.isVisible()) {
                    addSerieToChart(serie);
                }
            }

            //Meta
            if (series.containsKey(KpiGraphIndicador.META)) {
                JBIChartDirectorSerie serie = (JBIChartDirectorSerie) series.get(KpiGraphIndicador.META);
                if (serie.isVisible()) {
                    addSerieToChart(serie);
                }
            }

            //Meta Acumulada
            if (series.containsKey(KpiGraphIndicador.META_ACUM)) {
                JBIChartDirectorSerie serie = (JBIChartDirectorSerie) series.get(KpiGraphIndicador.META_ACUM);
                if (serie.isVisible()) {
                    addSerieToChart(serie);
                }
            }

            //Real
            if (series.containsKey(KpiGraphIndicador.VALOR)) {
                JBIChartDirectorSerie serie = (JBIChartDirectorSerie) series.get(KpiGraphIndicador.VALOR);
                if (serie.isVisible()) {
                    addSerieToChart(serie);
                }
            }
            //Valor Acumulado
            if (series.containsKey(KpiGraphIndicador.VALOR_ACUM)) {
                JBIChartDirectorSerie serie = (JBIChartDirectorSerie) series.get(KpiGraphIndicador.VALOR_ACUM);
                if (serie.isVisible()) {
                    addSerieToChart(serie);
                }
            }

            this.drawGraph();

        }
    }

    /**
     * Remove a linha
     * @param rowIndex Linha que se deseja remover.
     * @param remove Se true remove, se false adiciona.
     */
    public void removeRow(int rowIndex, boolean remove) {
        for (java.util.Iterator e = series.values().iterator(); e.hasNext();) {
            JBIChartDirectorSerie serie = (JBIChartDirectorSerie) e.next();
            serie.removeSerieItem(rowIndex, remove);
            removeSerieLabel(rowIndex, remove);
        }
    }

    /**
     * Retorna um objeto do tipo base Chart.
     * @return Retorna um objeto do tipo base Chart.
     */
    public BaseChart getBaseChart() {
        if (graphType == PIE_CHART) {
            return oPieChart;
        } else if (graphType == XY_CHART) {
            return oXYChart;
        } else if (graphType == STATUS_CHART) {
            return oStatusChart;
        } else {
            return null;
        }
    }

    /**
     * Retorna as legendas.
     * @return String[] com as legendas.
     */
    public String[] getLegendas() {
        //Retorna somente os itens que podem ser visualizados.
        if (legendas == null) {
            legendas = new String[1];
            legendas[0] = "Sem legenda";
            return legendas;
        } else {
            String[] itensRet = new String[legendas.length - legendasRemoved.size()];
            int itemRet = 0;
            for (int i = 0; i < legendas.length; i++) {
                if (!legendasRemoved.containsKey(i)) {
                    itensRet[itemRet] = legendas[i];
                    itemRet++;
                }
            }
            return itensRet;
        }
    }

    /**
     * Configura as legendas.
     * @param legendas Legengas do gr�fico.
     */
    public void setLegendas(String[] legendas) {
        this.legendas = legendas;
    }

    /**
     * Configura a posi��o da legenda.
     * @param legendPos Posi��o da legenda.
     * @param refresh true para fazer o refresh no gr�fico.
     */
    public void setLegendPos(int legendPos, boolean refresh) {
        this.legendPos = legendPos;
        if (refresh) {
            updateGraph();
        }
    }

    /**
     * Configura a cor de fundo.
     * @param color Cor de fundo.
     * @param refresh true para fazer o refresh no gr�fico.
     */
    public void setBackGroundColor(java.awt.Color color, boolean refresh) {
        this.backColor = color;
        if (refresh) {
            updateGraph();
        }
    }

    /**
     * Configura a legenda do eixo Y.
     * @param legenda Texto da legenda.
     * @param refresh true para fazer o refresh no gr�fico.
     */
    public void setYLegenda(String legenda, boolean refresh) {
        this.yLegenda = legenda;
        if (refresh) {
            updateGraph();
        }
    }

    /**
     * Configura a legenda do eixo X.
     * @param legenda Texto da legenda.
     * @param refresh true para fazer o refresh no gr�fico.
     */
    public void setXLegenda(String legenda, boolean refresh) {
        this.xLegenda = legenda;
        if (refresh) {
            updateGraph();
        }
    }

    /**
     * T�tulo do gr�fico.
     * @param title Texto do gr�fico.
     * @param refresh true para fazer o refresh no gr�fico.
     */
    public void setTitle(String title, boolean refresh) {
        this.titleXY = title;
        if (refresh) {
            updateGraph();
        }
    }

    /**
     * Orienta��o Quanto Maior melhor ou Quanto Menor Melhor
     * @param orientation Texto do gr�fico.
     * @param refresh true para fazer o refresh no gr�fico.
     */
    public void setOrientation(boolean orientation, boolean refresh, boolean melhorF) {
        // "Orienta��o: ";
        this.titleOrient = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00057");
        this.titleOrient += ": ";

        // Adiciona a descri��o da orienta��o
        if (melhorF) { //"Melhor na Faixa"
            this.titleOrient += java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00112");
        } else if (orientation) { // "Quanto Maior Melhor";
            this.titleOrient += java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00057");
        } else { // "Quanto Menor Melhor";
            this.titleOrient += java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00058");
        }

        if (refresh) {
            updateGraph();
        }
    }

    /**
     * Orienta��o Quanto Maior melhor ou Quanto Menor Melhor
     * @param UnitMeasure Texto do gr�fico.
     * @param refresh true para fazer o refresh no gr�fico.
     */
    public void setUnitMeasure(String UnitMeasure, boolean refresh) {
        this.titleUnitMeasure = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUnidade_00002");
        this.titleUnitMeasure += ": ";
        this.titleUnitMeasure += UnitMeasure;

        if (refresh) {
            updateGraph();
        }
    }

    /**
     * Inverte o Eixo X e Y.
     * @param swapXY true faz a invers�o
     * @param refresh true para fazer o refresh no gr�fico.
     */
    public void setSwapXY(boolean swapXY, boolean refresh) {
        this.swapXY = swapXY;
        if (refresh) {
            updateGraph();
        }
    }

    /**
     * Retorna se os eixos X e Y est�o invertidos.
     * @return
     */
    public boolean isSwapXY() {
        return swapXY;
    }

    /**
     * Retorna quantas s�ries existe no gr�fico.
     * @return int n�mero de s�ries.
     */
    public int getSerieSize() {
        return series.size();
    }

    /**
     * Remove todas as s�ries do gr�fico.
     */
    public void removeAllSeries() {

        if (series.size() > 0) {
            series.clear();
        }

        if (legendasRemoved.size() > 0) {
            legendasRemoved.clear();
        }
    }

    /**
     * Retorna somente as s�rie que est�o visiveis.
     * @return int n�mero de s�ries visiveis.
     */
    public int getVisibleSerieSize() {
        int qtdSerie = 0;
        for (java.util.Iterator e = series.values().iterator(); e.hasNext();) {
            JBIChartDirectorSerie serie = (JBIChartDirectorSerie) e.next();
            if (serie.isVisible()) {
                qtdSerie++;
            }
        }
        return qtdSerie;
    }

    /**
     * Retorna uma imagem.
     * @param imgType Tipo da imagem deseja.
     * @see ChartDirector.Chart para consultar imagens dispon�veis.
     * @return array[] com a imagem.
     */
    public byte[] getImage(int imgType) {
        return getBaseChart().makeChart2(imgType);
    }

    /**
     * 
     */
    public void resetChart() {
        this.oXYLineLayer = null;
        this.oXYLineLayer3D = null;
        this.oXYBarLayer = null;
        this.oXYBarLayer3D = null;
        this.oXYBarLayerColor = null;
        this.oXYChart = null;
        this.oPieChart = null;
        this.oXYCylinderLayer = null;

        if (this.graphType == XY_CHART) {
            oXYChart = new ChartDirector.XYChart(0, 0);

            if (legXvisible) {
                oXYChart.xAxis().setTitle(xLegenda, "Tahoma Bold", 8);
            } else {
                oXYChart.xAxis().setTitle("");
            }

            if (legYvisible) {
                oXYChart.yAxis().setTitle(yLegenda, "Tahoma Bold", 8);
            } else {
                oXYChart.yAxis().setTitle("");
            }

            oXYChart.swapXY(swapXY);
            legVisible = true;
        } else if (this.graphType == STATUS_CHART) {
            oStatusChart = new ChartDirector.XYChart(0, 0);

            if (legXvisible) {
                oStatusChart.xAxis().setTitle(xLegenda, "Tahoma Bold", 8);
            } else {
                oStatusChart.xAxis().setTitle("");
            }

            if (legYvisible) {
                oStatusChart.yAxis().setTitle(yLegenda, "Tahoma Bold", 8);
            } else {
                oStatusChart.yAxis().setTitle("");
            }

            oStatusChart.swapXY(swapXY);
            legVisible = true;
        } else if (this.graphType == PIE_CHART) {
            oPieChart = new ChartDirector.PieChart(0, 0);
            legVisible = false;
        }

        getBaseChart().setBackground(Chart.CColor(backColor));//Cor de fundo.
    }

    /**
     *
     */
    private void setGraphSize() {
        getBaseChart().setSize(graphWidth, graphHeight);

        //Com base na �rea base do gr�fico define a �rea de plotagem do gr�fico.
        //Posicionando a legenda
        ChartDirector.TextBox titulo = getBaseChart().addTitle(getTitle(), "Tahoma Bold", 10);
        titulo.setText(getTitle());
        titulo.setPos(0, 0);

        ChartDirector.TextBox alvo = getBaseChart().addTitle(getOrientation(), "Tahoma", 8);
        alvo.setText(getOrientation() + "  " + getUnitMeasure());
        alvo.setPos(0, 25);

        //Borda
        int bordaSupInf = 20;//Borda Superior e inferior
        int bordaDirEsq = 70;//Borda Direita e esquerda
        int space = 30;//Espa�amento entre os objetos

        //Posicionamento da �rea de plot.
        int plotX = bordaDirEsq;
        int plotY = bordaSupInf;
        int plotWidth = graphWidth - (bordaDirEsq * 2);
        int plotHeight = graphHeight - (bordaSupInf * 2);

        //Propriedades da legenda.
        int legendX = 0;
        int legendY = 0;
        boolean vertical = false;

        java.awt.Dimension dimLegenda = new java.awt.Dimension();
        LegendBox oLegenda = null;

        if (legendPos == LEFT || legendPos == RIGHT) {
            vertical = true;
        }

        //Adicionando legendas no eixo x.
        if (legVisible) {
            ChartDirector.TextBox xLabel;
            oLegenda = getBaseChart().addLegend(legendX, legendY, vertical, "", 8);
            if (this.graphType == STATUS_CHART) {
                xLabel = oStatusChart.xAxis().setLabels(getLegendas());
                oLegenda.addKey(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00059"), Chart.CColor(new java.awt.Color(139, 191, 150)));
                oLegenda.addKey(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00060"), Chart.CColor(new java.awt.Color(234, 140, 136)));
                oLegenda.addKey(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00061"), Chart.CColor(new java.awt.Color(255, 235, 155)));
                oLegenda.addKey(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00071"), Chart.CColor(new java.awt.Color(109, 178, 247)));
            } else {
                xLabel = oXYChart.xAxis().setLabels(getLegendas());
            }

            xLabel.setFontStyle("Tahoma");
            xLabel.setFontSize(8);
        }

        getBaseChart().layoutLegend();

        if (legVisible) {
            dimLegenda.setSize(oLegenda.getWidth(), oLegenda.getHeight());
        }

        //Calculando a �rea do ploter.
        switch (legendPos) {
            case TOP:
                legendX = space;
                legendY = bordaSupInf + titulo.getHeight() + 10;
                plotY = dimLegenda.height + titulo.getHeight() + bordaSupInf + space;
                plotHeight = plotHeight - (dimLegenda.height + bordaSupInf + titulo.getHeight() + space + 5);

                break;

            case DOWN:
                legendX = space;
                legendY = plotHeight;
                plotY = titulo.getHeight() + bordaSupInf + space;
                plotHeight = plotHeight - (dimLegenda.height + bordaSupInf + titulo.getHeight() + space);

                break;

            case LEFT:
                legendX = space;
                legendY = bordaSupInf * 2;
                plotY = titulo.getHeight() + space;
                plotX += dimLegenda.width + space;
                plotWidth = plotWidth - plotX;
                plotHeight = plotHeight - (bordaSupInf + titulo.getHeight() + space);

                break;

            case RIGHT:
                legendX = plotWidth;
                legendY = bordaSupInf * 3;
                plotY = titulo.getHeight() + space;
                plotWidth = plotWidth - (dimLegenda.width + bordaDirEsq + space);
                plotHeight = plotHeight - (bordaSupInf + titulo.getHeight() + space);

                break;
        }

        //Corre��o para invers�o dos eixos.
        if (this.swapXY) {
            plotX += getLegendXSize().width;
        }

        if (legVisible) {
            oLegenda.setPos(legendX, legendY);
        }

        //Posicionamento da �rea de plot.
        if (graphType == XY_CHART) {
            oXYChart.setPlotArea(plotX, plotY, plotWidth, plotHeight);
        } else if (graphType == STATUS_CHART) {
            oStatusChart.setPlotArea(plotX, plotY, plotWidth, plotHeight);
        } else {
            plotX = (int) ((plotHeight / 2) + bordaSupInf + titulo.getHeight());
            int radiusPoint = (int) plotX - (space + bordaSupInf);
            oPieChart.setPieSize((int) (plotWidth / 2), plotX, (int) (radiusPoint / 2));
        }
    }

    /**
     *
     * @param serie
     */
    private void addSerieToChart(JBIChartDirectorSerie serie) {
        initLayers(serie);

        switch (serie.getSerieType()) {

            case LINE:
                DataSet dataSet = oXYLineLayer.addDataSet(serie.getSerieValor(), serie.getCChartColor(), serie.getSerieName());
                oXYLineLayer.setLineWidth(3);

                if (serie.getSymbol() != 0) {
                    dataSet.setDataSymbol(serie.getSymbol(), 7);
                }

                if (legValvisible) {

                    oXYLineLayer.setDataLabelStyle(
                            agregadedLabel.getName(),
                            agregadedLabel.getSize(),
                            oXYLineLayer.yZoneColor(0, 0xcc3300, 0x3333ff));
                    oXYLineLayer.setDataLabelFormat("{value|2.,}");
                }

                break;

            case LINE_3D:
                oXYLineLayer3D.addDataSet(serie.getSerieValor(), serie.getCChartColor(), serie.getSerieName());

                if (legValvisible) {

                    oXYLineLayer3D.setDataLabelStyle(
                            agregadedLabel.getName(),
                            agregadedLabel.getSize(),
                            oXYLineLayer3D.yZoneColor(0, 0xcc3300, 0x3333ff));
                    oXYLineLayer3D.setDataLabelFormat("{value|2.,}");
                }

                break;

            case BAR:
                oXYBarLayer.addDataSet(serie.getSerieValor(), serie.getCChartColor(), serie.getSerieName());

                if (legValvisible) {

                    oXYBarLayer.setAggregateLabelStyle(
                            agregadedLabel.getName(),
                            agregadedLabel.getSize(),
                            oXYBarLayer.yZoneColor(0, 0xcc3300, 0x3333ff),
                            this.getLegValAngle());
                    oXYBarLayer.setAggregateLabelFormat("{value|2.,}");

                }

                break;

            case BAR_3D:
                oXYBarLayer3D.addDataSet(serie.getSerieValor(), serie.getCChartColor(), serie.getSerieName());

                if (legValvisible) {

                    oXYBarLayer3D.setAggregateLabelStyle(
                            agregadedLabel.getName(),
                            agregadedLabel.getSize(),
                            oXYBarLayer3D.yZoneColor(0, 0xcc3300, 0x3333ff),
                            this.getLegValAngle());
                    oXYBarLayer3D.setAggregateLabelFormat("{value|2.,}");
                }

                break;

            case MULTI_COLOR_XY:
                oXYBarLayerColor = oXYChart.addBarLayer3(serie.getSerieValor(), serie.getPersonalizateCChartColor(), null, 5);
                oXYBarLayerColor.setOverlapRatio(0);

                break;

            case PIE:
                oPieChart.setData(serie.getSerieValor(), getLegendas());
                oPieChart.setLabelLayout(Chart.SideLayout);
                titlePie = serie.getSerieName();

                break;

            case PIE_3D:
                oPieChart.setData(serie.getSerieValor(), getLegendas());
                oPieChart.setLabelLayout(Chart.SideLayout);
                oPieChart.set3D(10);
                titlePie = serie.getSerieName();

                break;

            case CYLINDER_3D:
                oXYCylinderLayer.addDataSet(serie.getSerieValor(), serie.getCChartColor(), serie.getSerieName());
                oXYCylinderLayer.setBarShape(Chart.CircleShape);

                if (legValvisible) {
                    oXYCylinderLayer.setAggregateLabelStyle(
                            agregadedLabel.getName(),
                            agregadedLabel.getSize(),
                            oXYCylinderLayer.yZoneColor(0, 0xcc3300, 0x3333ff),
                            this.getLegValAngle());
                    oXYCylinderLayer.setAggregateLabelFormat("{value|2.,}");
                }

                break;

            case DEFAULT_BARRA:
                BarLayer barDefault = oStatusChart.addBarLayer3(serie.getSerieValor(), serie.getPersonalizateCChartColor(), null, 3);

                if (legValvisible) {
                    TextBox legenda = barDefault.setAggregateLabelStyle(
                            agregadedLabel.getName(),
                            agregadedLabel.getSize());

                    barDefault.setAggregateLabelFormat("{value|2.,}");

                    legenda.setPos(0, -10);
                }

                break;
        }
    }

    /**
     *
     * @param serie
     */
    private void initLayers(JBIChartDirectorSerie serie) {

        if (this.graphType == STATUS_CHART) {
            if (serie.getSerieType() == LINE_3D && oXYLineLayer3D == null) {
                oXYLineLayer3D = oStatusChart.addLineLayer();
                oXYLineLayer3D.set3D(13);
            } else if (serie.getSerieType() == LINE && oXYLineLayer == null) {
                oXYLineLayer = oStatusChart.addLineLayer();
            }

        } else {
            if (serie.getSerieType() == LINE && oXYLineLayer == null) {
                oXYLineLayer = oXYChart.addLineLayer();
            } else if (serie.getSerieType() == LINE_3D && oXYLineLayer3D == null) {
                oXYLineLayer3D = oXYChart.addLineLayer();
                oXYLineLayer3D.set3D(13);
            } else if (serie.getSerieType() == BAR_3D && oXYBarLayer3D == null) {
                oXYBarLayer3D = oXYChart.addBarLayer2(Chart.Side, 3);
            } else if (serie.getSerieType() == BAR && oXYBarLayer == null) {
                oXYBarLayer = oXYChart.addBarLayer();
            } else if (serie.getSerieType() == CYLINDER_3D && oXYCylinderLayer == null) {
                oXYCylinderLayer = oXYChart.addBarLayer2(Chart.Side, 3);
            } else if (serie.getSerieType() == PIE) {
            }
        }
    }

    /**
     *
     * @param index
     * @param lRemove
     */
    private void removeSerieLabel(int index, boolean lRemove) {
        if (lRemove && !legendasRemoved.containsKey(index)) {
            legendasRemoved.put(index, index);
        } else if (!lRemove && legendasRemoved.containsKey(index)) {
            legendasRemoved.remove(index);
        }
    }

    /**
     * Retorna o tamanho da fonte da legenda do eixo X.
     * @return tamanho da fonte da legenda do eixo X
     */
    private java.awt.Dimension getLegendXSize() {
        java.awt.Dimension fontSize = new java.awt.Dimension();
        java.awt.FontMetrics fontMetrics = oChartViewer.getFontMetrics(eixoFont);

        for (int n = 0; n < legendas.length; n++) {
            if (fontSize.height < fontMetrics.getHeight()) {
                fontSize.setSize(fontSize.height, fontMetrics.getHeight());
            }

            if (fontSize.width < fontMetrics.stringWidth(legendas[n])) {
                fontSize.setSize(fontMetrics.stringWidth(legendas[n]), fontSize.height);
            }
        }

        return fontSize;
    }

    /**
     * Retorna o t�tulo do gr�fico.
     * @return t�tulo do gr�fico.
     */
    private String getTitle() {
        if (graphType == XY_CHART || graphType == STATUS_CHART) {
            return titleXY;
        } else if (graphType == PIE_CHART) {
            return titlePie;
        } else {
            return "";
        }
    }

    /*
     * Retorna o titulo da orientacao do gr�fico.
     * @return titulo da orienta��o.
     */
    private String getOrientation() {
        return titleOrient;
    }

    /**
     * Retorna o titulo da unidade de medida do gr�fico.
     * @return titulo da unidade de medida.
     */
    private String getUnitMeasure() {
        return titleUnitMeasure;
    }

    /**
     * Retorna se o gr�fixo est� em uma transa��o.
     * @return boolean true est� em um transa��o.
     */
    public boolean isTrasanction() {
        return inTrasanction;
    }

    /**
     * Coloca o gr�fico em transa��o, nenhuma altera��o � refletida na tela.
     * @param inTrasanction true inicia transa��o, false finaliza.
     */
    public void setInTrasanction(boolean inTrasanction) {
        this.inTrasanction = inTrasanction;
    }

    /**
     * Mostra a legenda no eixo X.
     * @param legXvisible true mostra a legenda.
     */
    public void setLegXvisible(boolean legXvisible) {
        this.legXvisible = legXvisible;
    }

    /**
     * Mostra a legenda no eixo Y.
     * @param legYvisible true mostra a legenda.
     */
    public void setLegYvisible(boolean legYvisible) {
        this.legYvisible = legYvisible;
    }

    /**
     * Mostra a legenda de valores.
     * @param legValvisible true mostra a legenda.
     */
    public void setLegValVisible(boolean legValvisible) {
        this.legValvisible = legValvisible;
    }

    /**
     * Define o angulo de exibi��o da legenda de valores.
     * @param legValAngle angulo da legenda de valores.
     */
    public void setLegValAngle(int legValAngle) {
        this.legValAngle = legValAngle;
    }

    /**
     * Retorna o angulo da legenda de valores.
     * @return angulo da legenda.
     */
    public int getLegValAngle() {
        return legValAngle;
    }

    /**
     * Desenha o gr�fico no componente chartviewer;
     */
    public void drawGraph() {
        setGraphSize();
        oChartViewer.setImage(getBaseChart().makeImage());
        oChartViewer.setImageMap(getBaseChart().getHTMLImageMap(
                "clickable",
                "",
                "title='Per�odo:{xLabel} \n Valor:{value}'"));
    }
}
