package kpi.beans;

/**
 * Classe que representa um s�rie para o JBIChartdirectorController
 * Cr�ado por Alexandre Silva 14/09/2006
 * @see kpi.beans.JBIChartDirectorController
 */
public class JBIChartDirectorSerie {

    private java.util.HashMap itensRemoved = new java.util.HashMap();
    private boolean visible = true;
    private String serieName = "";
    private int serieType = JBIChartDirectorController.BAR;
    private int serieID = 0;
    private int symbol = 0;
    private double[] serieItens;
    private java.awt.Color[] colors;
    private java.awt.Color[] customColors;

    /**
     * Cr�a um objeto do tipo JBIChartDirector
     * @param valSerie Array com os valores da s�rie.
     */
    public JBIChartDirectorSerie(double[] valSerie) {
        setSerieValor(valSerie);
    }

    /**
     * Retorna os valores da s�rie.
     * @return Retorna os valores da s�rie.
     */
    public double[] getSerieValor() {
        double[] itensRet = new double[serieItens.length - itensRemoved.size()];
        int itemRet = 0;
        for (int i = 0; i < serieItens.length; i++) {
            if (!itensRemoved.containsKey(i)) {
                itensRet[itemRet] = serieItens[i];
                itemRet++;
            }
        }
        return itensRet;
    }

    /**
     * Retorna o tipo da s�rie.
     * @return Retorna o tipo da s�rie.
     */
    public int getSerieType() {
        return serieType;
    }

    /**
     * Seta o valor da s�rie.
     * @param serieValor Array com os valores da s�rie.
     */
    public void setSerieValor(double[] serieValor) {
        this.serieItens = serieValor;

    }

    /**
     * Remove uma s�rie.
     * @param index Chave da s�ria para ser removida.
     * @param lRemove Indica se remove ou adiciona a s�rie.
     */
    public void removeSerieItem(int index, boolean lRemove) {
        if (lRemove && !itensRemoved.containsKey(index)) {
            itensRemoved.put(index, index);
        } else if (!lRemove && itensRemoved.containsKey(index)) {
            itensRemoved.remove(index);
        }
    }

    /**
     * Configura o tipo da s�rie.
     * @param serieType Tipo da s�rie.
     */
    public void setSerieType(int serieType) {
        this.serieType = serieType;
    }

    /**
     * Retorna a cor da s�rie.
     * @return Retorno da cor da s�rie.
     */
    public java.awt.Color getColor() {
        if (colors.length < 1) {
            this.colors = new java.awt.Color[1];
            this.colors[0] = java.awt.Color.ORANGE;
        }

        return colors[0];
    }

    /**
     * Configura a cor da s�rie.
     * @param color Cor da s�rie.
     */
    public void setColor(java.awt.Color color) {
        this.colors = new java.awt.Color[1];
        this.colors[0] = color;
    }

    /**
     * Configura as cores personalizadas para a s�rio.
     * @param colors Array de cores.
     */
    public void setPersonalizateColors(java.awt.Color[] colors) {
        this.customColors = colors;
    }

    public void setPersonalizateColors(java.util.Vector colors) {
        java.awt.Color[] aRet = new java.awt.Color[colors.size()];
        for (int item = 0; item < colors.size(); item++) {
            aRet[item] = (java.awt.Color) colors.get(item);
        }
        this.customColors = aRet;
    }

    /**
     * Retorna as cores personalizadas da s�rie.
     * @return Retorna as cores personalizadas.
     */
    public java.awt.Color[] getPersonalizateColors() {
        return customColors;
    }

    /**
     * Retorna a cor no padr�o CharDirector.
     * @return Retorna a cor no padr�o CharDirector.
     */
    public int getCChartColor() {
        if (colors != null) {
            return ChartDirector.Chart.CColor(colors[0]);
        } else {
            return ChartDirector.Chart.CColor(java.awt.Color.RED);
        }
    }

    /**
     * Retorna as cores personalizadas no padr�o ChartDirector.
     * @return Retorna as cores personalizadas no padr�o ChartDirector.
     */
    public int[] getPersonalizateCChartColor() {
        int[] chartColors = {ChartDirector.Chart.CColor(java.awt.Color.RED)};

        if (customColors != null) {
            chartColors = new int[customColors.length];

            for (int index = 0; index < customColors.length; index++) {
                chartColors[index] = ChartDirector.Chart.CColor(customColors[index]);
            }
        }

        return chartColors;
    }

    /**
     * Retorna se a s�rie est� visivel.
     * @return boolean - indicando se a s�rie � visivel ou n�o true = visivel.
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * Configura a s�rie como visivel.
     * @param visible true = visivel false = oculta
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    /**
     * Retorna o nome da s�rie.
     * @return String com o nome da s�rie.
     */
    public String getSerieName() {
        return serieName;
    }

    /**
     * Seta o nome da s�rie.
     * @param serieName Nome da s�rie.
     */
    public void setSerieName(String serieName) {
        this.serieName = serieName;
    }

    /**
     * Retorna o ID  da s�rie.
     * @return int com o n�mero da s�rie.
     */
    public int getSerieID() {
        return serieID;
    }

    /**
     * Seta o n�mero da s�rie.
     * @param serieID Indica a chave da s�rie.
     */
    public void setSerieID(int serieID) {
        this.serieID = serieID;
    }

    /**
     * Retorna o s�mbolo para a s�rie.
     * @return String com o simbolo da s�rie.
     */
    public int getSymbol() {
        return symbol;
    }

    /**
     * Seta o s�mbolo da s�rie.
     * @param symbol Simbolo
     */
    public void setSymbol(int symbol) {
        this.symbol = symbol;
    }
}
