/*
 * JBIDesktopPaneBeanInfo.java
 *
 * Created on 26 de Agosto de 2005, 15:19
 */

package kpi.beans;

import java.beans.*;

/**
 * @author Alexandre Alves da Silva
 */
public class JBIDesktopPaneBeanInfo extends SimpleBeanInfo {
    
    // Bean descriptor//GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( kpi.beans.JBIDesktopPane.class , null );//GEN-HEADEREND:BeanDescriptor
	
	// Here you can add code for customizing the BeanDescriptor.
	
        return beanDescriptor;         }//GEN-LAST:BeanDescriptor
    
    
    // Property identifiers//GEN-FIRST:Properties
    private static final int PROPERTY_accessibleContext = 0;
    private static final int PROPERTY_actionMap = 1;
    private static final int PROPERTY_alignmentX = 2;
    private static final int PROPERTY_alignmentY = 3;
    private static final int PROPERTY_allFrames = 4;
    private static final int PROPERTY_allFramesInLayer = 5;
    private static final int PROPERTY_allSize = 6;
    private static final int PROPERTY_ancestorListeners = 7;
    private static final int PROPERTY_autoscrolls = 8;
    private static final int PROPERTY_background = 9;
    private static final int PROPERTY_backgroundSet = 10;
    private static final int PROPERTY_border = 11;
    private static final int PROPERTY_bounds = 12;
    private static final int PROPERTY_colorModel = 13;
    private static final int PROPERTY_component = 14;
    private static final int PROPERTY_componentCount = 15;
    private static final int PROPERTY_componentCountInLayer = 16;
    private static final int PROPERTY_componentListeners = 17;
    private static final int PROPERTY_componentOrientation = 18;
    private static final int PROPERTY_componentPopupMenu = 19;
    private static final int PROPERTY_components = 20;
    private static final int PROPERTY_componentsInLayer = 21;
    private static final int PROPERTY_containerListeners = 22;
    private static final int PROPERTY_cursor = 23;
    private static final int PROPERTY_cursorSet = 24;
    private static final int PROPERTY_debugGraphicsOptions = 25;
    private static final int PROPERTY_desktopManager = 26;
    private static final int PROPERTY_displayable = 27;
    private static final int PROPERTY_doubleBuffered = 28;
    private static final int PROPERTY_dragMode = 29;
    private static final int PROPERTY_dropTarget = 30;
    private static final int PROPERTY_enabled = 31;
    private static final int PROPERTY_focusable = 32;
    private static final int PROPERTY_focusCycleRoot = 33;
    private static final int PROPERTY_focusCycleRootAncestor = 34;
    private static final int PROPERTY_focusListeners = 35;
    private static final int PROPERTY_focusOwner = 36;
    private static final int PROPERTY_focusTraversable = 37;
    private static final int PROPERTY_focusTraversalKeys = 38;
    private static final int PROPERTY_focusTraversalKeysEnabled = 39;
    private static final int PROPERTY_focusTraversalPolicy = 40;
    private static final int PROPERTY_focusTraversalPolicyProvider = 41;
    private static final int PROPERTY_focusTraversalPolicySet = 42;
    private static final int PROPERTY_font = 43;
    private static final int PROPERTY_fontSet = 44;
    private static final int PROPERTY_foreground = 45;
    private static final int PROPERTY_foregroundSet = 46;
    private static final int PROPERTY_graphics = 47;
    private static final int PROPERTY_graphicsConfiguration = 48;
    private static final int PROPERTY_height = 49;
    private static final int PROPERTY_hierarchyBoundsListeners = 50;
    private static final int PROPERTY_hierarchyListeners = 51;
    private static final int PROPERTY_ignoreRepaint = 52;
    private static final int PROPERTY_inheritsPopupMenu = 53;
    private static final int PROPERTY_inputContext = 54;
    private static final int PROPERTY_inputMap = 55;
    private static final int PROPERTY_inputMethodListeners = 56;
    private static final int PROPERTY_inputMethodRequests = 57;
    private static final int PROPERTY_inputVerifier = 58;
    private static final int PROPERTY_insets = 59;
    private static final int PROPERTY_keyListeners = 60;
    private static final int PROPERTY_layout = 61;
    private static final int PROPERTY_lightweight = 62;
    private static final int PROPERTY_locale = 63;
    private static final int PROPERTY_location = 64;
    private static final int PROPERTY_locationOnScreen = 65;
    private static final int PROPERTY_managingFocus = 66;
    private static final int PROPERTY_maximumSize = 67;
    private static final int PROPERTY_maximumSizeSet = 68;
    private static final int PROPERTY_minimumSize = 69;
    private static final int PROPERTY_minimumSizeSet = 70;
    private static final int PROPERTY_mouseListeners = 71;
    private static final int PROPERTY_mouseMotionListeners = 72;
    private static final int PROPERTY_mousePosition = 73;
    private static final int PROPERTY_mouseWheelListeners = 74;
    private static final int PROPERTY_name = 75;
    private static final int PROPERTY_nextFocusableComponent = 76;
    private static final int PROPERTY_opaque = 77;
    private static final int PROPERTY_optimizedDrawingEnabled = 78;
    private static final int PROPERTY_paintingTile = 79;
    private static final int PROPERTY_parent = 80;
    private static final int PROPERTY_peer = 81;
    private static final int PROPERTY_preferredSize = 82;
    private static final int PROPERTY_preferredSizeSet = 83;
    private static final int PROPERTY_propertyChangeListeners = 84;
    private static final int PROPERTY_registeredKeyStrokes = 85;
    private static final int PROPERTY_requestFocusEnabled = 86;
    private static final int PROPERTY_rootPane = 87;
    private static final int PROPERTY_selectedFrame = 88;
    private static final int PROPERTY_showing = 89;
    private static final int PROPERTY_size = 90;
    private static final int PROPERTY_toolkit = 91;
    private static final int PROPERTY_toolTipText = 92;
    private static final int PROPERTY_topLevelAncestor = 93;
    private static final int PROPERTY_transferHandler = 94;
    private static final int PROPERTY_treeLock = 95;
    private static final int PROPERTY_UI = 96;
    private static final int PROPERTY_UIClassID = 97;
    private static final int PROPERTY_valid = 98;
    private static final int PROPERTY_validateRoot = 99;
    private static final int PROPERTY_verifyInputWhenFocusTarget = 100;
    private static final int PROPERTY_vetoableChangeListeners = 101;
    private static final int PROPERTY_visible = 102;
    private static final int PROPERTY_visibleRect = 103;
    private static final int PROPERTY_width = 104;
    private static final int PROPERTY_x = 105;
    private static final int PROPERTY_y = 106;

    // Property array 
    /*lazy PropertyDescriptor*/
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[107];
    
        try {
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", kpi.beans.JBIDesktopPane.class, "getAccessibleContext", null );
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", kpi.beans.JBIDesktopPane.class, "getActionMap", "setActionMap" );
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", kpi.beans.JBIDesktopPane.class, "getAlignmentX", "setAlignmentX" );
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", kpi.beans.JBIDesktopPane.class, "getAlignmentY", "setAlignmentY" );
            properties[PROPERTY_allFrames] = new PropertyDescriptor ( "allFrames", kpi.beans.JBIDesktopPane.class, "getAllFrames", null );
            properties[PROPERTY_allFramesInLayer] = new IndexedPropertyDescriptor ( "allFramesInLayer", kpi.beans.JBIDesktopPane.class, null, null, "getAllFramesInLayer", null );
            properties[PROPERTY_allSize] = new PropertyDescriptor ( "allSize", kpi.beans.JBIDesktopPane.class, null, "setAllSize" );
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", kpi.beans.JBIDesktopPane.class, "getAncestorListeners", null );
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", kpi.beans.JBIDesktopPane.class, "getAutoscrolls", "setAutoscrolls" );
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", kpi.beans.JBIDesktopPane.class, "getBackground", "setBackground" );
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", kpi.beans.JBIDesktopPane.class, "isBackgroundSet", null );
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", kpi.beans.JBIDesktopPane.class, "getBorder", "setBorder" );
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", kpi.beans.JBIDesktopPane.class, "getBounds", "setBounds" );
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", kpi.beans.JBIDesktopPane.class, "getColorModel", null );
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", kpi.beans.JBIDesktopPane.class, null, null, "getComponent", null );
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", kpi.beans.JBIDesktopPane.class, "getComponentCount", null );
            properties[PROPERTY_componentCountInLayer] = new IndexedPropertyDescriptor ( "componentCountInLayer", kpi.beans.JBIDesktopPane.class, null, null, "getComponentCountInLayer", null );
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", kpi.beans.JBIDesktopPane.class, "getComponentListeners", null );
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", kpi.beans.JBIDesktopPane.class, "getComponentOrientation", "setComponentOrientation" );
            properties[PROPERTY_componentPopupMenu] = new PropertyDescriptor ( "componentPopupMenu", kpi.beans.JBIDesktopPane.class, "getComponentPopupMenu", "setComponentPopupMenu" );
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", kpi.beans.JBIDesktopPane.class, "getComponents", null );
            properties[PROPERTY_componentsInLayer] = new IndexedPropertyDescriptor ( "componentsInLayer", kpi.beans.JBIDesktopPane.class, null, null, "getComponentsInLayer", null );
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", kpi.beans.JBIDesktopPane.class, "getContainerListeners", null );
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", kpi.beans.JBIDesktopPane.class, "getCursor", "setCursor" );
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", kpi.beans.JBIDesktopPane.class, "isCursorSet", null );
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", kpi.beans.JBIDesktopPane.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" );
            properties[PROPERTY_desktopManager] = new PropertyDescriptor ( "desktopManager", kpi.beans.JBIDesktopPane.class, "getDesktopManager", "setDesktopManager" );
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", kpi.beans.JBIDesktopPane.class, "isDisplayable", null );
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", kpi.beans.JBIDesktopPane.class, "isDoubleBuffered", "setDoubleBuffered" );
            properties[PROPERTY_dragMode] = new PropertyDescriptor ( "dragMode", kpi.beans.JBIDesktopPane.class, "getDragMode", "setDragMode" );
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", kpi.beans.JBIDesktopPane.class, "getDropTarget", "setDropTarget" );
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", kpi.beans.JBIDesktopPane.class, "isEnabled", "setEnabled" );
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", kpi.beans.JBIDesktopPane.class, "isFocusable", "setFocusable" );
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", kpi.beans.JBIDesktopPane.class, "isFocusCycleRoot", "setFocusCycleRoot" );
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", kpi.beans.JBIDesktopPane.class, "getFocusCycleRootAncestor", null );
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", kpi.beans.JBIDesktopPane.class, "getFocusListeners", null );
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", kpi.beans.JBIDesktopPane.class, "isFocusOwner", null );
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", kpi.beans.JBIDesktopPane.class, "isFocusTraversable", null );
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", kpi.beans.JBIDesktopPane.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" );
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", kpi.beans.JBIDesktopPane.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" );
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", kpi.beans.JBIDesktopPane.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" );
            properties[PROPERTY_focusTraversalPolicyProvider] = new PropertyDescriptor ( "focusTraversalPolicyProvider", kpi.beans.JBIDesktopPane.class, "isFocusTraversalPolicyProvider", "setFocusTraversalPolicyProvider" );
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", kpi.beans.JBIDesktopPane.class, "isFocusTraversalPolicySet", null );
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", kpi.beans.JBIDesktopPane.class, "getFont", "setFont" );
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", kpi.beans.JBIDesktopPane.class, "isFontSet", null );
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", kpi.beans.JBIDesktopPane.class, "getForeground", "setForeground" );
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", kpi.beans.JBIDesktopPane.class, "isForegroundSet", null );
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", kpi.beans.JBIDesktopPane.class, "getGraphics", null );
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", kpi.beans.JBIDesktopPane.class, "getGraphicsConfiguration", null );
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", kpi.beans.JBIDesktopPane.class, "getHeight", null );
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", kpi.beans.JBIDesktopPane.class, "getHierarchyBoundsListeners", null );
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", kpi.beans.JBIDesktopPane.class, "getHierarchyListeners", null );
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", kpi.beans.JBIDesktopPane.class, "getIgnoreRepaint", "setIgnoreRepaint" );
            properties[PROPERTY_inheritsPopupMenu] = new PropertyDescriptor ( "inheritsPopupMenu", kpi.beans.JBIDesktopPane.class, "getInheritsPopupMenu", "setInheritsPopupMenu" );
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", kpi.beans.JBIDesktopPane.class, "getInputContext", null );
            properties[PROPERTY_inputMap] = new PropertyDescriptor ( "inputMap", kpi.beans.JBIDesktopPane.class, "getInputMap", null );
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", kpi.beans.JBIDesktopPane.class, "getInputMethodListeners", null );
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", kpi.beans.JBIDesktopPane.class, "getInputMethodRequests", null );
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", kpi.beans.JBIDesktopPane.class, "getInputVerifier", "setInputVerifier" );
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", kpi.beans.JBIDesktopPane.class, "getInsets", null );
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", kpi.beans.JBIDesktopPane.class, "getKeyListeners", null );
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", kpi.beans.JBIDesktopPane.class, "getLayout", "setLayout" );
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", kpi.beans.JBIDesktopPane.class, "isLightweight", null );
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", kpi.beans.JBIDesktopPane.class, "getLocale", "setLocale" );
            properties[PROPERTY_location] = new PropertyDescriptor ( "location", kpi.beans.JBIDesktopPane.class, "getLocation", "setLocation" );
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", kpi.beans.JBIDesktopPane.class, "getLocationOnScreen", null );
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", kpi.beans.JBIDesktopPane.class, "isManagingFocus", null );
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", kpi.beans.JBIDesktopPane.class, "getMaximumSize", "setMaximumSize" );
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", kpi.beans.JBIDesktopPane.class, "isMaximumSizeSet", null );
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", kpi.beans.JBIDesktopPane.class, "getMinimumSize", "setMinimumSize" );
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", kpi.beans.JBIDesktopPane.class, "isMinimumSizeSet", null );
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", kpi.beans.JBIDesktopPane.class, "getMouseListeners", null );
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", kpi.beans.JBIDesktopPane.class, "getMouseMotionListeners", null );
            properties[PROPERTY_mousePosition] = new PropertyDescriptor ( "mousePosition", kpi.beans.JBIDesktopPane.class, "getMousePosition", null );
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", kpi.beans.JBIDesktopPane.class, "getMouseWheelListeners", null );
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", kpi.beans.JBIDesktopPane.class, "getName", "setName" );
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", kpi.beans.JBIDesktopPane.class, "getNextFocusableComponent", "setNextFocusableComponent" );
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", kpi.beans.JBIDesktopPane.class, "isOpaque", "setOpaque" );
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", kpi.beans.JBIDesktopPane.class, "isOptimizedDrawingEnabled", null );
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", kpi.beans.JBIDesktopPane.class, "isPaintingTile", null );
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", kpi.beans.JBIDesktopPane.class, "getParent", null );
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", kpi.beans.JBIDesktopPane.class, "getPeer", null );
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", kpi.beans.JBIDesktopPane.class, "getPreferredSize", "setPreferredSize" );
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", kpi.beans.JBIDesktopPane.class, "isPreferredSizeSet", null );
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", kpi.beans.JBIDesktopPane.class, "getPropertyChangeListeners", null );
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", kpi.beans.JBIDesktopPane.class, "getRegisteredKeyStrokes", null );
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", kpi.beans.JBIDesktopPane.class, "isRequestFocusEnabled", "setRequestFocusEnabled" );
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", kpi.beans.JBIDesktopPane.class, "getRootPane", null );
            properties[PROPERTY_selectedFrame] = new PropertyDescriptor ( "selectedFrame", kpi.beans.JBIDesktopPane.class, "getSelectedFrame", "setSelectedFrame" );
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", kpi.beans.JBIDesktopPane.class, "isShowing", null );
            properties[PROPERTY_size] = new PropertyDescriptor ( "size", kpi.beans.JBIDesktopPane.class, "getSize", "setSize" );
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", kpi.beans.JBIDesktopPane.class, "getToolkit", null );
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", kpi.beans.JBIDesktopPane.class, "getToolTipText", "setToolTipText" );
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", kpi.beans.JBIDesktopPane.class, "getTopLevelAncestor", null );
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", kpi.beans.JBIDesktopPane.class, "getTransferHandler", "setTransferHandler" );
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", kpi.beans.JBIDesktopPane.class, "getTreeLock", null );
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", kpi.beans.JBIDesktopPane.class, "getUI", "setUI" );
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", kpi.beans.JBIDesktopPane.class, "getUIClassID", null );
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", kpi.beans.JBIDesktopPane.class, "isValid", null );
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", kpi.beans.JBIDesktopPane.class, "isValidateRoot", null );
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", kpi.beans.JBIDesktopPane.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" );
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", kpi.beans.JBIDesktopPane.class, "getVetoableChangeListeners", null );
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", kpi.beans.JBIDesktopPane.class, "isVisible", "setVisible" );
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", kpi.beans.JBIDesktopPane.class, "getVisibleRect", null );
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", kpi.beans.JBIDesktopPane.class, "getWidth", null );
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", kpi.beans.JBIDesktopPane.class, "getX", null );
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", kpi.beans.JBIDesktopPane.class, "getY", null );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Properties
	
	// Here you can add code for customizing the properties array.
	
        return properties;         }//GEN-LAST:Properties
    
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_ancestorListener = 0;
    private static final int EVENT_componentListener = 1;
    private static final int EVENT_containerListener = 2;
    private static final int EVENT_focusListener = 3;
    private static final int EVENT_hierarchyBoundsListener = 4;
    private static final int EVENT_hierarchyListener = 5;
    private static final int EVENT_inputMethodListener = 6;
    private static final int EVENT_keyListener = 7;
    private static final int EVENT_mouseListener = 8;
    private static final int EVENT_mouseMotionListener = 9;
    private static final int EVENT_mouseWheelListener = 10;
    private static final int EVENT_propertyChangeListener = 11;
    private static final int EVENT_vetoableChangeListener = 12;

    // EventSet array
    /*lazy EventSetDescriptor*/
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[13];
    
            try {
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( kpi.beans.JBIDesktopPane.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorAdded", "ancestorMoved", "ancestorRemoved"}, "addAncestorListener", "removeAncestorListener" );
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( kpi.beans.JBIDesktopPane.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentHidden", "componentMoved", "componentResized", "componentShown"}, "addComponentListener", "removeComponentListener" );
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( kpi.beans.JBIDesktopPane.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" );
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( kpi.beans.JBIDesktopPane.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusGained", "focusLost"}, "addFocusListener", "removeFocusListener" );
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( kpi.beans.JBIDesktopPane.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" );
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( kpi.beans.JBIDesktopPane.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" );
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( kpi.beans.JBIDesktopPane.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" );
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( kpi.beans.JBIDesktopPane.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyReleased", "keyTyped"}, "addKeyListener", "removeKeyListener" );
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( kpi.beans.JBIDesktopPane.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mouseEntered", "mouseExited", "mousePressed", "mouseReleased"}, "addMouseListener", "removeMouseListener" );
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( kpi.beans.JBIDesktopPane.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" );
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( kpi.beans.JBIDesktopPane.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" );
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( kpi.beans.JBIDesktopPane.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" );
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( kpi.beans.JBIDesktopPane.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Events
	
	// Here you can add code for customizing the event sets array.
	
        return eventSets;         }//GEN-LAST:Events
    
    // Method identifiers//GEN-FIRST:Methods
    private static final int METHOD_action0 = 0;
    private static final int METHOD_add1 = 1;
    private static final int METHOD_addNotify2 = 2;
    private static final int METHOD_addPropertyChangeListener3 = 3;
    private static final int METHOD_applyComponentOrientation4 = 4;
    private static final int METHOD_areFocusTraversalKeysSet5 = 5;
    private static final int METHOD_bounds6 = 6;
    private static final int METHOD_cascadeFrames7 = 7;
    private static final int METHOD_checkImage8 = 8;
    private static final int METHOD_computeVisibleRect9 = 9;
    private static final int METHOD_contains10 = 10;
    private static final int METHOD_countComponents11 = 11;
    private static final int METHOD_createImage12 = 12;
    private static final int METHOD_createToolTip13 = 13;
    private static final int METHOD_createVolatileImage14 = 14;
    private static final int METHOD_deliverEvent15 = 15;
    private static final int METHOD_disable16 = 16;
    private static final int METHOD_dispatchEvent17 = 17;
    private static final int METHOD_doLayout18 = 18;
    private static final int METHOD_enable19 = 19;
    private static final int METHOD_enableInputMethods20 = 20;
    private static final int METHOD_findComponentAt21 = 21;
    private static final int METHOD_firePropertyChange22 = 22;
    private static final int METHOD_getActionForKeyStroke23 = 23;
    private static final int METHOD_getBounds24 = 24;
    private static final int METHOD_getClientProperty25 = 25;
    private static final int METHOD_getComponentAt26 = 26;
    private static final int METHOD_getComponentZOrder27 = 27;
    private static final int METHOD_getConditionForKeyStroke28 = 28;
    private static final int METHOD_getDefaultLocale29 = 29;
    private static final int METHOD_getFontMetrics30 = 30;
    private static final int METHOD_getIndexOf31 = 31;
    private static final int METHOD_getInsets32 = 32;
    private static final int METHOD_getLayer33 = 33;
    private static final int METHOD_getLayeredPaneAbove34 = 34;
    private static final int METHOD_getListeners35 = 35;
    private static final int METHOD_getLocation36 = 36;
    private static final int METHOD_getMousePosition37 = 37;
    private static final int METHOD_getPopupLocation38 = 38;
    private static final int METHOD_getPosition39 = 39;
    private static final int METHOD_getPropertyChangeListeners40 = 40;
    private static final int METHOD_getSize41 = 41;
    private static final int METHOD_getToolTipLocation42 = 42;
    private static final int METHOD_getToolTipText43 = 43;
    private static final int METHOD_gotFocus44 = 44;
    private static final int METHOD_grabFocus45 = 45;
    private static final int METHOD_handleEvent46 = 46;
    private static final int METHOD_hasFocus47 = 47;
    private static final int METHOD_hide48 = 48;
    private static final int METHOD_highestLayer49 = 49;
    private static final int METHOD_imageUpdate50 = 50;
    private static final int METHOD_insets51 = 51;
    private static final int METHOD_inside52 = 52;
    private static final int METHOD_invalidate53 = 53;
    private static final int METHOD_isAncestorOf54 = 54;
    private static final int METHOD_isFocusCycleRoot55 = 55;
    private static final int METHOD_isLightweightComponent56 = 56;
    private static final int METHOD_keyDown57 = 57;
    private static final int METHOD_keyUp58 = 58;
    private static final int METHOD_layout59 = 59;
    private static final int METHOD_list60 = 60;
    private static final int METHOD_locate61 = 61;
    private static final int METHOD_location62 = 62;
    private static final int METHOD_lostFocus63 = 63;
    private static final int METHOD_lowestLayer64 = 64;
    private static final int METHOD_minimumSize65 = 65;
    private static final int METHOD_mouseDown66 = 66;
    private static final int METHOD_mouseDrag67 = 67;
    private static final int METHOD_mouseEnter68 = 68;
    private static final int METHOD_mouseExit69 = 69;
    private static final int METHOD_mouseMove70 = 70;
    private static final int METHOD_mouseUp71 = 71;
    private static final int METHOD_move72 = 72;
    private static final int METHOD_moveToBack73 = 73;
    private static final int METHOD_moveToFront74 = 74;
    private static final int METHOD_nextFocus75 = 75;
    private static final int METHOD_paint76 = 76;
    private static final int METHOD_paintAll77 = 77;
    private static final int METHOD_paintComponents78 = 78;
    private static final int METHOD_paintImmediately79 = 79;
    private static final int METHOD_postEvent80 = 80;
    private static final int METHOD_preferredSize81 = 81;
    private static final int METHOD_prepareImage82 = 82;
    private static final int METHOD_print83 = 83;
    private static final int METHOD_printAll84 = 84;
    private static final int METHOD_printComponents85 = 85;
    private static final int METHOD_putClientProperty86 = 86;
    private static final int METHOD_putLayer87 = 87;
    private static final int METHOD_registerKeyboardAction88 = 88;
    private static final int METHOD_remove89 = 89;
    private static final int METHOD_removeAll90 = 90;
    private static final int METHOD_removeNotify91 = 91;
    private static final int METHOD_removePropertyChangeListener92 = 92;
    private static final int METHOD_repaint93 = 93;
    private static final int METHOD_requestDefaultFocus94 = 94;
    private static final int METHOD_requestFocus95 = 95;
    private static final int METHOD_requestFocusInWindow96 = 96;
    private static final int METHOD_resetKeyboardActions97 = 97;
    private static final int METHOD_reshape98 = 98;
    private static final int METHOD_resize99 = 99;
    private static final int METHOD_revalidate100 = 100;
    private static final int METHOD_scrollRectToVisible101 = 101;
    private static final int METHOD_setBounds102 = 102;
    private static final int METHOD_setComponentZOrder103 = 103;
    private static final int METHOD_setDefaultLocale104 = 104;
    private static final int METHOD_setLayer105 = 105;
    private static final int METHOD_setPosition106 = 106;
    private static final int METHOD_show107 = 107;
    private static final int METHOD_size108 = 108;
    private static final int METHOD_tileFrames109 = 109;
    private static final int METHOD_toString110 = 110;
    private static final int METHOD_transferFocus111 = 111;
    private static final int METHOD_transferFocusBackward112 = 112;
    private static final int METHOD_transferFocusDownCycle113 = 113;
    private static final int METHOD_transferFocusUpCycle114 = 114;
    private static final int METHOD_unregisterKeyboardAction115 = 115;
    private static final int METHOD_update116 = 116;
    private static final int METHOD_updateUI117 = 117;
    private static final int METHOD_validate118 = 118;

    // Method array 
    /*lazy MethodDescriptor*/
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[119];
    
        try {
            methods[METHOD_action0] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_action0].setDisplayName ( "" );
            methods[METHOD_add1] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("add", new Class[] {javax.swing.JInternalFrame.class}));
            methods[METHOD_add1].setDisplayName ( "" );
            methods[METHOD_addNotify2] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("addNotify", new Class[] {}));
            methods[METHOD_addNotify2].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener3] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_addPropertyChangeListener3].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation4] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class}));
            methods[METHOD_applyComponentOrientation4].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet5] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE}));
            methods[METHOD_areFocusTraversalKeysSet5].setDisplayName ( "" );
            methods[METHOD_bounds6] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("bounds", new Class[] {}));
            methods[METHOD_bounds6].setDisplayName ( "" );
            methods[METHOD_cascadeFrames7] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("cascadeFrames", new Class[] {}));
            methods[METHOD_cascadeFrames7].setDisplayName ( "" );
            methods[METHOD_checkImage8] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage8].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect9] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_computeVisibleRect9].setDisplayName ( "" );
            methods[METHOD_contains10] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_contains10].setDisplayName ( "" );
            methods[METHOD_countComponents11] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("countComponents", new Class[] {}));
            methods[METHOD_countComponents11].setDisplayName ( "" );
            methods[METHOD_createImage12] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class}));
            methods[METHOD_createImage12].setDisplayName ( "" );
            methods[METHOD_createToolTip13] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("createToolTip", new Class[] {}));
            methods[METHOD_createToolTip13].setDisplayName ( "" );
            methods[METHOD_createVolatileImage14] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createVolatileImage14].setDisplayName ( "" );
            methods[METHOD_deliverEvent15] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_deliverEvent15].setDisplayName ( "" );
            methods[METHOD_disable16] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("disable", new Class[] {}));
            methods[METHOD_disable16].setDisplayName ( "" );
            methods[METHOD_dispatchEvent17] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class}));
            methods[METHOD_dispatchEvent17].setDisplayName ( "" );
            methods[METHOD_doLayout18] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("doLayout", new Class[] {}));
            methods[METHOD_doLayout18].setDisplayName ( "" );
            methods[METHOD_enable19] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("enable", new Class[] {}));
            methods[METHOD_enable19].setDisplayName ( "" );
            methods[METHOD_enableInputMethods20] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE}));
            methods[METHOD_enableInputMethods20].setDisplayName ( "" );
            methods[METHOD_findComponentAt21] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_findComponentAt21].setDisplayName ( "" );
            methods[METHOD_firePropertyChange22] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Boolean.TYPE, Boolean.TYPE}));
            methods[METHOD_firePropertyChange22].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke23] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getActionForKeyStroke23].setDisplayName ( "" );
            methods[METHOD_getBounds24] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_getBounds24].setDisplayName ( "" );
            methods[METHOD_getClientProperty25] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class}));
            methods[METHOD_getClientProperty25].setDisplayName ( "" );
            methods[METHOD_getComponentAt26] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getComponentAt26].setDisplayName ( "" );
            methods[METHOD_getComponentZOrder27] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getComponentZOrder", new Class[] {java.awt.Component.class}));
            methods[METHOD_getComponentZOrder27].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke28] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getConditionForKeyStroke28].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale29] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getDefaultLocale", new Class[] {}));
            methods[METHOD_getDefaultLocale29].setDisplayName ( "" );
            methods[METHOD_getFontMetrics30] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class}));
            methods[METHOD_getFontMetrics30].setDisplayName ( "" );
            methods[METHOD_getIndexOf31] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getIndexOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_getIndexOf31].setDisplayName ( "" );
            methods[METHOD_getInsets32] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getInsets", new Class[] {java.awt.Insets.class}));
            methods[METHOD_getInsets32].setDisplayName ( "" );
            methods[METHOD_getLayer33] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getLayer", new Class[] {javax.swing.JComponent.class}));
            methods[METHOD_getLayer33].setDisplayName ( "" );
            methods[METHOD_getLayeredPaneAbove34] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getLayeredPaneAbove", new Class[] {java.awt.Component.class}));
            methods[METHOD_getLayeredPaneAbove34].setDisplayName ( "" );
            methods[METHOD_getListeners35] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getListeners", new Class[] {java.lang.Class.class}));
            methods[METHOD_getListeners35].setDisplayName ( "" );
            methods[METHOD_getLocation36] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_getLocation36].setDisplayName ( "" );
            methods[METHOD_getMousePosition37] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getMousePosition", new Class[] {Boolean.TYPE}));
            methods[METHOD_getMousePosition37].setDisplayName ( "" );
            methods[METHOD_getPopupLocation38] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getPopupLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getPopupLocation38].setDisplayName ( "" );
            methods[METHOD_getPosition39] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getPosition", new Class[] {java.awt.Component.class}));
            methods[METHOD_getPosition39].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners40] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class}));
            methods[METHOD_getPropertyChangeListeners40].setDisplayName ( "" );
            methods[METHOD_getSize41] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_getSize41].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation42] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipLocation42].setDisplayName ( "" );
            methods[METHOD_getToolTipText43] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipText43].setDisplayName ( "" );
            methods[METHOD_gotFocus44] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_gotFocus44].setDisplayName ( "" );
            methods[METHOD_grabFocus45] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("grabFocus", new Class[] {}));
            methods[METHOD_grabFocus45].setDisplayName ( "" );
            methods[METHOD_handleEvent46] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("handleEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_handleEvent46].setDisplayName ( "" );
            methods[METHOD_hasFocus47] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("hasFocus", new Class[] {}));
            methods[METHOD_hasFocus47].setDisplayName ( "" );
            methods[METHOD_hide48] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("hide", new Class[] {}));
            methods[METHOD_hide48].setDisplayName ( "" );
            methods[METHOD_highestLayer49] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("highestLayer", new Class[] {}));
            methods[METHOD_highestLayer49].setDisplayName ( "" );
            methods[METHOD_imageUpdate50] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_imageUpdate50].setDisplayName ( "" );
            methods[METHOD_insets51] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("insets", new Class[] {}));
            methods[METHOD_insets51].setDisplayName ( "" );
            methods[METHOD_inside52] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_inside52].setDisplayName ( "" );
            methods[METHOD_invalidate53] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("invalidate", new Class[] {}));
            methods[METHOD_invalidate53].setDisplayName ( "" );
            methods[METHOD_isAncestorOf54] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_isAncestorOf54].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot55] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class}));
            methods[METHOD_isFocusCycleRoot55].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent56] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class}));
            methods[METHOD_isLightweightComponent56].setDisplayName ( "" );
            methods[METHOD_keyDown57] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyDown57].setDisplayName ( "" );
            methods[METHOD_keyUp58] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyUp58].setDisplayName ( "" );
            methods[METHOD_layout59] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("layout", new Class[] {}));
            methods[METHOD_layout59].setDisplayName ( "" );
            methods[METHOD_list60] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE}));
            methods[METHOD_list60].setDisplayName ( "" );
            methods[METHOD_locate61] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_locate61].setDisplayName ( "" );
            methods[METHOD_location62] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("location", new Class[] {}));
            methods[METHOD_location62].setDisplayName ( "" );
            methods[METHOD_lostFocus63] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_lostFocus63].setDisplayName ( "" );
            methods[METHOD_lowestLayer64] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("lowestLayer", new Class[] {}));
            methods[METHOD_lowestLayer64].setDisplayName ( "" );
            methods[METHOD_minimumSize65] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("minimumSize", new Class[] {}));
            methods[METHOD_minimumSize65].setDisplayName ( "" );
            methods[METHOD_mouseDown66] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDown66].setDisplayName ( "" );
            methods[METHOD_mouseDrag67] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDrag67].setDisplayName ( "" );
            methods[METHOD_mouseEnter68] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseEnter68].setDisplayName ( "" );
            methods[METHOD_mouseExit69] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseExit69].setDisplayName ( "" );
            methods[METHOD_mouseMove70] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseMove70].setDisplayName ( "" );
            methods[METHOD_mouseUp71] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseUp71].setDisplayName ( "" );
            methods[METHOD_move72] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_move72].setDisplayName ( "" );
            methods[METHOD_moveToBack73] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("moveToBack", new Class[] {java.awt.Component.class}));
            methods[METHOD_moveToBack73].setDisplayName ( "" );
            methods[METHOD_moveToFront74] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("moveToFront", new Class[] {java.awt.Component.class}));
            methods[METHOD_moveToFront74].setDisplayName ( "" );
            methods[METHOD_nextFocus75] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("nextFocus", new Class[] {}));
            methods[METHOD_nextFocus75].setDisplayName ( "" );
            methods[METHOD_paint76] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("paint", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paint76].setDisplayName ( "" );
            methods[METHOD_paintAll77] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintAll77].setDisplayName ( "" );
            methods[METHOD_paintComponents78] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintComponents78].setDisplayName ( "" );
            methods[METHOD_paintImmediately79] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_paintImmediately79].setDisplayName ( "" );
            methods[METHOD_postEvent80] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("postEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_postEvent80].setDisplayName ( "" );
            methods[METHOD_preferredSize81] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("preferredSize", new Class[] {}));
            methods[METHOD_preferredSize81].setDisplayName ( "" );
            methods[METHOD_prepareImage82] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage82].setDisplayName ( "" );
            methods[METHOD_print83] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("print", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_print83].setDisplayName ( "" );
            methods[METHOD_printAll84] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("printAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printAll84].setDisplayName ( "" );
            methods[METHOD_printComponents85] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printComponents85].setDisplayName ( "" );
            methods[METHOD_putClientProperty86] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class}));
            methods[METHOD_putClientProperty86].setDisplayName ( "" );
            methods[METHOD_putLayer87] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("putLayer", new Class[] {javax.swing.JComponent.class, Integer.TYPE}));
            methods[METHOD_putLayer87].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction88] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction88].setDisplayName ( "" );
            methods[METHOD_remove89] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("remove", new Class[] {java.awt.Component.class}));
            methods[METHOD_remove89].setDisplayName ( "" );
            methods[METHOD_removeAll90] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("removeAll", new Class[] {}));
            methods[METHOD_removeAll90].setDisplayName ( "" );
            methods[METHOD_removeNotify91] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("removeNotify", new Class[] {}));
            methods[METHOD_removeNotify91].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener92] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_removePropertyChangeListener92].setDisplayName ( "" );
            methods[METHOD_repaint93] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint93].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus94] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("requestDefaultFocus", new Class[] {}));
            methods[METHOD_requestDefaultFocus94].setDisplayName ( "" );
            methods[METHOD_requestFocus95] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("requestFocus", new Class[] {}));
            methods[METHOD_requestFocus95].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow96] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("requestFocusInWindow", new Class[] {}));
            methods[METHOD_requestFocusInWindow96].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions97] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("resetKeyboardActions", new Class[] {}));
            methods[METHOD_resetKeyboardActions97].setDisplayName ( "" );
            methods[METHOD_reshape98] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_reshape98].setDisplayName ( "" );
            methods[METHOD_resize99] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_resize99].setDisplayName ( "" );
            methods[METHOD_revalidate100] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("revalidate", new Class[] {}));
            methods[METHOD_revalidate100].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible101] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_scrollRectToVisible101].setDisplayName ( "" );
            methods[METHOD_setBounds102] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setBounds102].setDisplayName ( "" );
            methods[METHOD_setComponentZOrder103] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("setComponentZOrder", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_setComponentZOrder103].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale104] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class}));
            methods[METHOD_setDefaultLocale104].setDisplayName ( "" );
            methods[METHOD_setLayer105] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("setLayer", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_setLayer105].setDisplayName ( "" );
            methods[METHOD_setPosition106] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("setPosition", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_setPosition106].setDisplayName ( "" );
            methods[METHOD_show107] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("show", new Class[] {}));
            methods[METHOD_show107].setDisplayName ( "" );
            methods[METHOD_size108] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("size", new Class[] {}));
            methods[METHOD_size108].setDisplayName ( "" );
            methods[METHOD_tileFrames109] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("tileFrames", new Class[] {}));
            methods[METHOD_tileFrames109].setDisplayName ( "" );
            methods[METHOD_toString110] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("toString", new Class[] {}));
            methods[METHOD_toString110].setDisplayName ( "" );
            methods[METHOD_transferFocus111] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("transferFocus", new Class[] {}));
            methods[METHOD_transferFocus111].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward112] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("transferFocusBackward", new Class[] {}));
            methods[METHOD_transferFocusBackward112].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle113] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("transferFocusDownCycle", new Class[] {}));
            methods[METHOD_transferFocusDownCycle113].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle114] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("transferFocusUpCycle", new Class[] {}));
            methods[METHOD_transferFocusUpCycle114].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction115] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_unregisterKeyboardAction115].setDisplayName ( "" );
            methods[METHOD_update116] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("update", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_update116].setDisplayName ( "" );
            methods[METHOD_updateUI117] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("updateUI", new Class[] {}));
            methods[METHOD_updateUI117].setDisplayName ( "" );
            methods[METHOD_validate118] = new MethodDescriptor ( kpi.beans.JBIDesktopPane.class.getMethod("validate", new Class[] {}));
            methods[METHOD_validate118].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
	
	// Here you can add code for customizing the methods array.
	
        return methods;         }//GEN-LAST:Methods
    
    
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
    
    
//GEN-FIRST:Superclass
    
    // Here you can add code for customizing the Superclass BeanInfo.
    
//GEN-LAST:Superclass
    
    /**
     * Gets the bean's <code>BeanDescriptor</code>s.
     *
     * @return BeanDescriptor describing the editable
     * properties of this bean.  May return null if the
     * information should be obtained by automatic analysis.
     */
    public BeanDescriptor getBeanDescriptor() {
	return getBdescriptor();
    }
    
    /**
     * Gets the bean's <code>PropertyDescriptor</code>s.
     *
     * @return An array of PropertyDescriptors describing the editable
     * properties supported by this bean.  May return null if the
     * information should be obtained by automatic analysis.
     * <p>
     * If a property is indexed, then its entry in the result array will
     * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
     * A client of getPropertyDescriptors can use "instanceof" to check
     * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
     */
    public PropertyDescriptor[] getPropertyDescriptors() {
	return getPdescriptor();
    }
    
    /**
     * Gets the bean's <code>EventSetDescriptor</code>s.
     *
     * @return  An array of EventSetDescriptors describing the kinds of
     * events fired by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public EventSetDescriptor[] getEventSetDescriptors() {
	return getEdescriptor();
    }
    
    /**
     * Gets the bean's <code>MethodDescriptor</code>s.
     *
     * @return  An array of MethodDescriptors describing the methods
     * implemented by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public MethodDescriptor[] getMethodDescriptors() {
	return getMdescriptor();
    }
    
    /**
     * A bean may have a "default" property that is the property that will
     * mostly commonly be initially chosen for update by human's who are
     * customizing the bean.
     * @return  Index of default property in the PropertyDescriptor array
     * 		returned by getPropertyDescriptors.
     * <P>	Returns -1 if there is no default property.
     */
    public int getDefaultPropertyIndex() {
	return defaultPropertyIndex;
    }
    
    /**
     * A bean may have a "default" event that is the event that will
     * mostly commonly be used by human's when using the bean.
     * @return Index of default event in the EventSetDescriptor array
     *		returned by getEventSetDescriptors.
     * <P>	Returns -1 if there is no default event.
     */
    public int getDefaultEventIndex() {
	return defaultEventIndex;
    }
}

