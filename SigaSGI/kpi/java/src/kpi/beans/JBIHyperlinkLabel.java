package kpi.beans;
/*
 * JBIHyperlinkLabel.java
 *
 * Created on October 28, 2003, 4:10 PM
 */

/**
 *
 * @author  siga1996
 */
public class JBIHyperlinkLabel extends javax.swing.JLabel {
	
	String type = null,
		id = null,
		title = null;
	boolean	setFoco = true;
	
	

	public void setFoco( boolean focus ) {
		setFoco = focus;
	}
	
	public void setType( String typeAux ) {
		type = String.valueOf(typeAux);
	}
	
	public String getType() {
		return String.valueOf( type );
	}
	
	public void setID( String idAux ) {
		id = String.valueOf(idAux);
	}
	
	public String getID() {
		return String.valueOf( id );
	}
	
	public void setTitle( String titleAux ) {
		title = String.valueOf(titleAux);
		setText(title);
	}
	
	public String getTitle() {
		return String.valueOf( title );
	}
	
	/** Creates a new instance of JBIHyperlinkLabel */
	public JBIHyperlinkLabel() {
		super();
		BIHyperlinkListener listener = new BIHyperlinkListener( this );
		this.addMouseListener(listener);
	}
	
	public JBIHyperlinkLabel(boolean focus) {
		super();
		BIHyperlinkListener listener = new BIHyperlinkListener( this );
		this.addMouseListener(listener);
		setFoco(focus);
	}
	
	public JBIHyperlinkLabel(String typeAux, String idAux, String titleAux) {
		super();
		type = String.valueOf(typeAux);
		id = String.valueOf(idAux);
		title = String.valueOf(titleAux);
		this.setText(title);
		BIHyperlinkListener listener = new BIHyperlinkListener( this );
		this.addMouseListener(listener);
		setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 11));
	}
}

class BIHyperlinkListener extends javax.swing.event.MouseInputAdapter {
	JBIHyperlinkLabel label = null;
	
	public BIHyperlinkListener( JBIHyperlinkLabel lbl ) {
		label = lbl;
	}
	
	public void mouseClicked(java.awt.event.MouseEvent e) {
		if ( e.getClickCount() == 1 && label.setFoco) {
			label.setCursor(null);
			label.setText( this.label.getTitle() );
			try {
				kpi.swing.KpiDefaultFrameFunctions frame =
					kpi.core.KpiStaticReferences.getKpiFormController().getForm( label.getType(), label.getID(), label.getText() );
			} catch (kpi.core.KpiFormControllerException exc ) {
				kpi.core.KpiDebug.println(exc.getMessage());
			}
		}
	}
	
	public void mouseEntered(java.awt.event.MouseEvent e) {
		try{
			// desvia o foco para o JBIHyperLink que o mouse esta apontando
			if (label.setFoco){
				((javax.swing.JInternalFrame) label.getRootPane().getParent()).setSelected(true);
				label.setText( "<html><u>"+this.label.getTitle()+"</u></html>" );
				label.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
			}
		}catch(Exception ex){
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		
	}
	
	public void mouseExited(java.awt.event.MouseEvent e) {
		label.setCursor(null);
		label.setText( this.label.getTitle() );
		
	}
	
}