/*
 * JBILayeredPane.java
 *
 * Created on November 25, 2003, 2:05 PM
 */
package kpi.beans;
import java.beans.*;

/**
 *
 * @author  siga1996
 */
public class JBILayeredPane extends javax.swing.JLayeredPane implements java.awt.print.Printable {
	
	private String nomeEstrategia = new String("");
	
	/** Creates a new instance of JBILayeredPane */
	public JBILayeredPane() {
		super();
	}
	
	public void setNomeEstrategia( String nomeAux ) {
		nomeEstrategia = String.valueOf( nomeAux );
	}
	
	public void printLayeredPane () {
   java.awt.print.PrinterJob printJob = java.awt.print.PrinterJob.getPrinterJob();
   printJob.setPrintable(this);
   if (printJob.printDialog())
     try {
       printJob.print();
     } catch(java.awt.print.PrinterException pe) {
       System.out.println("Error printing: " + pe);
		 }	
	}
	
	public int print(java.awt.Graphics graphics, java.awt.print.PageFormat pageFormat, int param) throws java.awt.print.PrinterException {
   if (param > 0) {
     return(NO_SUCH_PAGE);
   } else {
     java.awt.Graphics2D g2d = (java.awt.Graphics2D)graphics;
		 g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY()+50);
     g2d.drawString(java.util.ResourceBundle.getBundle("International", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("JBILayeredPane_00001") + nomeEstrategia, 0, 0 );
		 g2d.scale( 0.5, 0.5 );
     disableDoubleBuffering(this);
     this.paint(g2d);
     enableDoubleBuffering(this);
     return(PAGE_EXISTS);
   }
	}	

	public static void disableDoubleBuffering(java.awt.Component c) {
		javax.swing.RepaintManager currentManager = javax.swing.RepaintManager.currentManager(c);
		currentManager.setDoubleBufferingEnabled(false);
	}

	public static void enableDoubleBuffering(java.awt.Component c) {
		javax.swing.RepaintManager currentManager = javax.swing.RepaintManager.currentManager(c);
		currentManager.setDoubleBufferingEnabled(true);
	}	
	
}
