/*
 * JBILayeredPaneBeanInfo.java
 *
 * Created on 26 de Agosto de 2005, 15:31
 */

package kpi.beans;

import java.beans.*;

/**
 * @author Alexandre Alves da Silva
 */
public class JBILayeredPaneBeanInfo extends SimpleBeanInfo {
    
    // Bean descriptor//GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( kpi.beans.JBILayeredPane.class , null );//GEN-HEADEREND:BeanDescriptor
	
	// Here you can add code for customizing the BeanDescriptor.
	
        return beanDescriptor;         }//GEN-LAST:BeanDescriptor
    
    
    // Property identifiers//GEN-FIRST:Properties
    private static final int PROPERTY_accessibleContext = 0;
    private static final int PROPERTY_actionMap = 1;
    private static final int PROPERTY_alignmentX = 2;
    private static final int PROPERTY_alignmentY = 3;
    private static final int PROPERTY_ancestorListeners = 4;
    private static final int PROPERTY_autoscrolls = 5;
    private static final int PROPERTY_background = 6;
    private static final int PROPERTY_backgroundSet = 7;
    private static final int PROPERTY_border = 8;
    private static final int PROPERTY_bounds = 9;
    private static final int PROPERTY_colorModel = 10;
    private static final int PROPERTY_component = 11;
    private static final int PROPERTY_componentCount = 12;
    private static final int PROPERTY_componentCountInLayer = 13;
    private static final int PROPERTY_componentListeners = 14;
    private static final int PROPERTY_componentOrientation = 15;
    private static final int PROPERTY_componentPopupMenu = 16;
    private static final int PROPERTY_components = 17;
    private static final int PROPERTY_componentsInLayer = 18;
    private static final int PROPERTY_containerListeners = 19;
    private static final int PROPERTY_cursor = 20;
    private static final int PROPERTY_cursorSet = 21;
    private static final int PROPERTY_debugGraphicsOptions = 22;
    private static final int PROPERTY_displayable = 23;
    private static final int PROPERTY_doubleBuffered = 24;
    private static final int PROPERTY_dropTarget = 25;
    private static final int PROPERTY_enabled = 26;
    private static final int PROPERTY_focusable = 27;
    private static final int PROPERTY_focusCycleRoot = 28;
    private static final int PROPERTY_focusCycleRootAncestor = 29;
    private static final int PROPERTY_focusListeners = 30;
    private static final int PROPERTY_focusOwner = 31;
    private static final int PROPERTY_focusTraversable = 32;
    private static final int PROPERTY_focusTraversalKeys = 33;
    private static final int PROPERTY_focusTraversalKeysEnabled = 34;
    private static final int PROPERTY_focusTraversalPolicy = 35;
    private static final int PROPERTY_focusTraversalPolicyProvider = 36;
    private static final int PROPERTY_focusTraversalPolicySet = 37;
    private static final int PROPERTY_font = 38;
    private static final int PROPERTY_fontSet = 39;
    private static final int PROPERTY_foreground = 40;
    private static final int PROPERTY_foregroundSet = 41;
    private static final int PROPERTY_graphics = 42;
    private static final int PROPERTY_graphicsConfiguration = 43;
    private static final int PROPERTY_height = 44;
    private static final int PROPERTY_hierarchyBoundsListeners = 45;
    private static final int PROPERTY_hierarchyListeners = 46;
    private static final int PROPERTY_ignoreRepaint = 47;
    private static final int PROPERTY_inheritsPopupMenu = 48;
    private static final int PROPERTY_inputContext = 49;
    private static final int PROPERTY_inputMap = 50;
    private static final int PROPERTY_inputMethodListeners = 51;
    private static final int PROPERTY_inputMethodRequests = 52;
    private static final int PROPERTY_inputVerifier = 53;
    private static final int PROPERTY_insets = 54;
    private static final int PROPERTY_keyListeners = 55;
    private static final int PROPERTY_layout = 56;
    private static final int PROPERTY_lightweight = 57;
    private static final int PROPERTY_locale = 58;
    private static final int PROPERTY_location = 59;
    private static final int PROPERTY_locationOnScreen = 60;
    private static final int PROPERTY_managingFocus = 61;
    private static final int PROPERTY_maximumSize = 62;
    private static final int PROPERTY_maximumSizeSet = 63;
    private static final int PROPERTY_minimumSize = 64;
    private static final int PROPERTY_minimumSizeSet = 65;
    private static final int PROPERTY_mouseListeners = 66;
    private static final int PROPERTY_mouseMotionListeners = 67;
    private static final int PROPERTY_mousePosition = 68;
    private static final int PROPERTY_mouseWheelListeners = 69;
    private static final int PROPERTY_name = 70;
    private static final int PROPERTY_nextFocusableComponent = 71;
    private static final int PROPERTY_nomeEstrategia = 72;
    private static final int PROPERTY_opaque = 73;
    private static final int PROPERTY_optimizedDrawingEnabled = 74;
    private static final int PROPERTY_paintingTile = 75;
    private static final int PROPERTY_parent = 76;
    private static final int PROPERTY_peer = 77;
    private static final int PROPERTY_preferredSize = 78;
    private static final int PROPERTY_preferredSizeSet = 79;
    private static final int PROPERTY_propertyChangeListeners = 80;
    private static final int PROPERTY_registeredKeyStrokes = 81;
    private static final int PROPERTY_requestFocusEnabled = 82;
    private static final int PROPERTY_rootPane = 83;
    private static final int PROPERTY_showing = 84;
    private static final int PROPERTY_size = 85;
    private static final int PROPERTY_toolkit = 86;
    private static final int PROPERTY_toolTipText = 87;
    private static final int PROPERTY_topLevelAncestor = 88;
    private static final int PROPERTY_transferHandler = 89;
    private static final int PROPERTY_treeLock = 90;
    private static final int PROPERTY_UIClassID = 91;
    private static final int PROPERTY_valid = 92;
    private static final int PROPERTY_validateRoot = 93;
    private static final int PROPERTY_verifyInputWhenFocusTarget = 94;
    private static final int PROPERTY_vetoableChangeListeners = 95;
    private static final int PROPERTY_visible = 96;
    private static final int PROPERTY_visibleRect = 97;
    private static final int PROPERTY_width = 98;
    private static final int PROPERTY_x = 99;
    private static final int PROPERTY_y = 100;

    // Property array 
    /*lazy PropertyDescriptor*/
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[101];
    
        try {
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", kpi.beans.JBILayeredPane.class, "getAccessibleContext", null );
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", kpi.beans.JBILayeredPane.class, "getActionMap", "setActionMap" );
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", kpi.beans.JBILayeredPane.class, "getAlignmentX", "setAlignmentX" );
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", kpi.beans.JBILayeredPane.class, "getAlignmentY", "setAlignmentY" );
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", kpi.beans.JBILayeredPane.class, "getAncestorListeners", null );
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", kpi.beans.JBILayeredPane.class, "getAutoscrolls", "setAutoscrolls" );
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", kpi.beans.JBILayeredPane.class, "getBackground", "setBackground" );
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", kpi.beans.JBILayeredPane.class, "isBackgroundSet", null );
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", kpi.beans.JBILayeredPane.class, "getBorder", "setBorder" );
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", kpi.beans.JBILayeredPane.class, "getBounds", "setBounds" );
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", kpi.beans.JBILayeredPane.class, "getColorModel", null );
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", kpi.beans.JBILayeredPane.class, null, null, "getComponent", null );
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", kpi.beans.JBILayeredPane.class, "getComponentCount", null );
            properties[PROPERTY_componentCountInLayer] = new IndexedPropertyDescriptor ( "componentCountInLayer", kpi.beans.JBILayeredPane.class, null, null, "getComponentCountInLayer", null );
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", kpi.beans.JBILayeredPane.class, "getComponentListeners", null );
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", kpi.beans.JBILayeredPane.class, "getComponentOrientation", "setComponentOrientation" );
            properties[PROPERTY_componentPopupMenu] = new PropertyDescriptor ( "componentPopupMenu", kpi.beans.JBILayeredPane.class, "getComponentPopupMenu", "setComponentPopupMenu" );
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", kpi.beans.JBILayeredPane.class, "getComponents", null );
            properties[PROPERTY_componentsInLayer] = new IndexedPropertyDescriptor ( "componentsInLayer", kpi.beans.JBILayeredPane.class, null, null, "getComponentsInLayer", null );
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", kpi.beans.JBILayeredPane.class, "getContainerListeners", null );
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", kpi.beans.JBILayeredPane.class, "getCursor", "setCursor" );
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", kpi.beans.JBILayeredPane.class, "isCursorSet", null );
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", kpi.beans.JBILayeredPane.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" );
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", kpi.beans.JBILayeredPane.class, "isDisplayable", null );
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", kpi.beans.JBILayeredPane.class, "isDoubleBuffered", "setDoubleBuffered" );
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", kpi.beans.JBILayeredPane.class, "getDropTarget", "setDropTarget" );
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", kpi.beans.JBILayeredPane.class, "isEnabled", "setEnabled" );
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", kpi.beans.JBILayeredPane.class, "isFocusable", "setFocusable" );
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", kpi.beans.JBILayeredPane.class, "isFocusCycleRoot", "setFocusCycleRoot" );
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", kpi.beans.JBILayeredPane.class, "getFocusCycleRootAncestor", null );
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", kpi.beans.JBILayeredPane.class, "getFocusListeners", null );
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", kpi.beans.JBILayeredPane.class, "isFocusOwner", null );
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", kpi.beans.JBILayeredPane.class, "isFocusTraversable", null );
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", kpi.beans.JBILayeredPane.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" );
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", kpi.beans.JBILayeredPane.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" );
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", kpi.beans.JBILayeredPane.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" );
            properties[PROPERTY_focusTraversalPolicyProvider] = new PropertyDescriptor ( "focusTraversalPolicyProvider", kpi.beans.JBILayeredPane.class, "isFocusTraversalPolicyProvider", "setFocusTraversalPolicyProvider" );
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", kpi.beans.JBILayeredPane.class, "isFocusTraversalPolicySet", null );
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", kpi.beans.JBILayeredPane.class, "getFont", "setFont" );
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", kpi.beans.JBILayeredPane.class, "isFontSet", null );
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", kpi.beans.JBILayeredPane.class, "getForeground", "setForeground" );
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", kpi.beans.JBILayeredPane.class, "isForegroundSet", null );
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", kpi.beans.JBILayeredPane.class, "getGraphics", null );
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", kpi.beans.JBILayeredPane.class, "getGraphicsConfiguration", null );
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", kpi.beans.JBILayeredPane.class, "getHeight", null );
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", kpi.beans.JBILayeredPane.class, "getHierarchyBoundsListeners", null );
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", kpi.beans.JBILayeredPane.class, "getHierarchyListeners", null );
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", kpi.beans.JBILayeredPane.class, "getIgnoreRepaint", "setIgnoreRepaint" );
            properties[PROPERTY_inheritsPopupMenu] = new PropertyDescriptor ( "inheritsPopupMenu", kpi.beans.JBILayeredPane.class, "getInheritsPopupMenu", "setInheritsPopupMenu" );
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", kpi.beans.JBILayeredPane.class, "getInputContext", null );
            properties[PROPERTY_inputMap] = new PropertyDescriptor ( "inputMap", kpi.beans.JBILayeredPane.class, "getInputMap", null );
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", kpi.beans.JBILayeredPane.class, "getInputMethodListeners", null );
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", kpi.beans.JBILayeredPane.class, "getInputMethodRequests", null );
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", kpi.beans.JBILayeredPane.class, "getInputVerifier", "setInputVerifier" );
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", kpi.beans.JBILayeredPane.class, "getInsets", null );
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", kpi.beans.JBILayeredPane.class, "getKeyListeners", null );
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", kpi.beans.JBILayeredPane.class, "getLayout", "setLayout" );
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", kpi.beans.JBILayeredPane.class, "isLightweight", null );
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", kpi.beans.JBILayeredPane.class, "getLocale", "setLocale" );
            properties[PROPERTY_location] = new PropertyDescriptor ( "location", kpi.beans.JBILayeredPane.class, "getLocation", "setLocation" );
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", kpi.beans.JBILayeredPane.class, "getLocationOnScreen", null );
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", kpi.beans.JBILayeredPane.class, "isManagingFocus", null );
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", kpi.beans.JBILayeredPane.class, "getMaximumSize", "setMaximumSize" );
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", kpi.beans.JBILayeredPane.class, "isMaximumSizeSet", null );
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", kpi.beans.JBILayeredPane.class, "getMinimumSize", "setMinimumSize" );
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", kpi.beans.JBILayeredPane.class, "isMinimumSizeSet", null );
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", kpi.beans.JBILayeredPane.class, "getMouseListeners", null );
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", kpi.beans.JBILayeredPane.class, "getMouseMotionListeners", null );
            properties[PROPERTY_mousePosition] = new PropertyDescriptor ( "mousePosition", kpi.beans.JBILayeredPane.class, "getMousePosition", null );
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", kpi.beans.JBILayeredPane.class, "getMouseWheelListeners", null );
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", kpi.beans.JBILayeredPane.class, "getName", "setName" );
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", kpi.beans.JBILayeredPane.class, "getNextFocusableComponent", "setNextFocusableComponent" );
            properties[PROPERTY_nomeEstrategia] = new PropertyDescriptor ( "nomeEstrategia", kpi.beans.JBILayeredPane.class, null, "setNomeEstrategia" );
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", kpi.beans.JBILayeredPane.class, "isOpaque", "setOpaque" );
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", kpi.beans.JBILayeredPane.class, "isOptimizedDrawingEnabled", null );
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", kpi.beans.JBILayeredPane.class, "isPaintingTile", null );
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", kpi.beans.JBILayeredPane.class, "getParent", null );
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", kpi.beans.JBILayeredPane.class, "getPeer", null );
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", kpi.beans.JBILayeredPane.class, "getPreferredSize", "setPreferredSize" );
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", kpi.beans.JBILayeredPane.class, "isPreferredSizeSet", null );
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", kpi.beans.JBILayeredPane.class, "getPropertyChangeListeners", null );
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", kpi.beans.JBILayeredPane.class, "getRegisteredKeyStrokes", null );
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", kpi.beans.JBILayeredPane.class, "isRequestFocusEnabled", "setRequestFocusEnabled" );
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", kpi.beans.JBILayeredPane.class, "getRootPane", null );
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", kpi.beans.JBILayeredPane.class, "isShowing", null );
            properties[PROPERTY_size] = new PropertyDescriptor ( "size", kpi.beans.JBILayeredPane.class, "getSize", "setSize" );
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", kpi.beans.JBILayeredPane.class, "getToolkit", null );
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", kpi.beans.JBILayeredPane.class, "getToolTipText", "setToolTipText" );
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", kpi.beans.JBILayeredPane.class, "getTopLevelAncestor", null );
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", kpi.beans.JBILayeredPane.class, "getTransferHandler", "setTransferHandler" );
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", kpi.beans.JBILayeredPane.class, "getTreeLock", null );
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", kpi.beans.JBILayeredPane.class, "getUIClassID", null );
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", kpi.beans.JBILayeredPane.class, "isValid", null );
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", kpi.beans.JBILayeredPane.class, "isValidateRoot", null );
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", kpi.beans.JBILayeredPane.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" );
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", kpi.beans.JBILayeredPane.class, "getVetoableChangeListeners", null );
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", kpi.beans.JBILayeredPane.class, "isVisible", "setVisible" );
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", kpi.beans.JBILayeredPane.class, "getVisibleRect", null );
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", kpi.beans.JBILayeredPane.class, "getWidth", null );
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", kpi.beans.JBILayeredPane.class, "getX", null );
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", kpi.beans.JBILayeredPane.class, "getY", null );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Properties
	
	// Here you can add code for customizing the properties array.
	
        return properties;         }//GEN-LAST:Properties
    
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_ancestorListener = 0;
    private static final int EVENT_componentListener = 1;
    private static final int EVENT_containerListener = 2;
    private static final int EVENT_focusListener = 3;
    private static final int EVENT_hierarchyBoundsListener = 4;
    private static final int EVENT_hierarchyListener = 5;
    private static final int EVENT_inputMethodListener = 6;
    private static final int EVENT_keyListener = 7;
    private static final int EVENT_mouseListener = 8;
    private static final int EVENT_mouseMotionListener = 9;
    private static final int EVENT_mouseWheelListener = 10;
    private static final int EVENT_propertyChangeListener = 11;
    private static final int EVENT_vetoableChangeListener = 12;

    // EventSet array
    /*lazy EventSetDescriptor*/
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[13];
    
            try {
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( kpi.beans.JBILayeredPane.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorAdded", "ancestorMoved", "ancestorRemoved"}, "addAncestorListener", "removeAncestorListener" );
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( kpi.beans.JBILayeredPane.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentHidden", "componentMoved", "componentResized", "componentShown"}, "addComponentListener", "removeComponentListener" );
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( kpi.beans.JBILayeredPane.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" );
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( kpi.beans.JBILayeredPane.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusGained", "focusLost"}, "addFocusListener", "removeFocusListener" );
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( kpi.beans.JBILayeredPane.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" );
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( kpi.beans.JBILayeredPane.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" );
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( kpi.beans.JBILayeredPane.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" );
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( kpi.beans.JBILayeredPane.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyReleased", "keyTyped"}, "addKeyListener", "removeKeyListener" );
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( kpi.beans.JBILayeredPane.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mouseEntered", "mouseExited", "mousePressed", "mouseReleased"}, "addMouseListener", "removeMouseListener" );
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( kpi.beans.JBILayeredPane.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" );
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( kpi.beans.JBILayeredPane.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" );
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( kpi.beans.JBILayeredPane.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" );
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( kpi.beans.JBILayeredPane.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Events
	
	// Here you can add code for customizing the event sets array.
	
        return eventSets;         }//GEN-LAST:Events
    
    // Method identifiers//GEN-FIRST:Methods
    private static final int METHOD_action0 = 0;
    private static final int METHOD_add1 = 1;
    private static final int METHOD_addNotify2 = 2;
    private static final int METHOD_addPropertyChangeListener3 = 3;
    private static final int METHOD_applyComponentOrientation4 = 4;
    private static final int METHOD_areFocusTraversalKeysSet5 = 5;
    private static final int METHOD_bounds6 = 6;
    private static final int METHOD_checkImage7 = 7;
    private static final int METHOD_computeVisibleRect8 = 8;
    private static final int METHOD_contains9 = 9;
    private static final int METHOD_countComponents10 = 10;
    private static final int METHOD_createImage11 = 11;
    private static final int METHOD_createToolTip12 = 12;
    private static final int METHOD_createVolatileImage13 = 13;
    private static final int METHOD_deliverEvent14 = 14;
    private static final int METHOD_disable15 = 15;
    private static final int METHOD_disableDoubleBuffering16 = 16;
    private static final int METHOD_dispatchEvent17 = 17;
    private static final int METHOD_doLayout18 = 18;
    private static final int METHOD_enable19 = 19;
    private static final int METHOD_enableDoubleBuffering20 = 20;
    private static final int METHOD_enableInputMethods21 = 21;
    private static final int METHOD_findComponentAt22 = 22;
    private static final int METHOD_firePropertyChange23 = 23;
    private static final int METHOD_getActionForKeyStroke24 = 24;
    private static final int METHOD_getBounds25 = 25;
    private static final int METHOD_getClientProperty26 = 26;
    private static final int METHOD_getComponentAt27 = 27;
    private static final int METHOD_getComponentZOrder28 = 28;
    private static final int METHOD_getConditionForKeyStroke29 = 29;
    private static final int METHOD_getDefaultLocale30 = 30;
    private static final int METHOD_getFontMetrics31 = 31;
    private static final int METHOD_getIndexOf32 = 32;
    private static final int METHOD_getInsets33 = 33;
    private static final int METHOD_getLayer34 = 34;
    private static final int METHOD_getLayeredPaneAbove35 = 35;
    private static final int METHOD_getListeners36 = 36;
    private static final int METHOD_getLocation37 = 37;
    private static final int METHOD_getMousePosition38 = 38;
    private static final int METHOD_getPopupLocation39 = 39;
    private static final int METHOD_getPosition40 = 40;
    private static final int METHOD_getPropertyChangeListeners41 = 41;
    private static final int METHOD_getSize42 = 42;
    private static final int METHOD_getToolTipLocation43 = 43;
    private static final int METHOD_getToolTipText44 = 44;
    private static final int METHOD_gotFocus45 = 45;
    private static final int METHOD_grabFocus46 = 46;
    private static final int METHOD_handleEvent47 = 47;
    private static final int METHOD_hasFocus48 = 48;
    private static final int METHOD_hide49 = 49;
    private static final int METHOD_highestLayer50 = 50;
    private static final int METHOD_imageUpdate51 = 51;
    private static final int METHOD_insets52 = 52;
    private static final int METHOD_inside53 = 53;
    private static final int METHOD_invalidate54 = 54;
    private static final int METHOD_isAncestorOf55 = 55;
    private static final int METHOD_isFocusCycleRoot56 = 56;
    private static final int METHOD_isLightweightComponent57 = 57;
    private static final int METHOD_keyDown58 = 58;
    private static final int METHOD_keyUp59 = 59;
    private static final int METHOD_layout60 = 60;
    private static final int METHOD_list61 = 61;
    private static final int METHOD_locate62 = 62;
    private static final int METHOD_location63 = 63;
    private static final int METHOD_lostFocus64 = 64;
    private static final int METHOD_lowestLayer65 = 65;
    private static final int METHOD_minimumSize66 = 66;
    private static final int METHOD_mouseDown67 = 67;
    private static final int METHOD_mouseDrag68 = 68;
    private static final int METHOD_mouseEnter69 = 69;
    private static final int METHOD_mouseExit70 = 70;
    private static final int METHOD_mouseMove71 = 71;
    private static final int METHOD_mouseUp72 = 72;
    private static final int METHOD_move73 = 73;
    private static final int METHOD_moveToBack74 = 74;
    private static final int METHOD_moveToFront75 = 75;
    private static final int METHOD_nextFocus76 = 76;
    private static final int METHOD_paint77 = 77;
    private static final int METHOD_paintAll78 = 78;
    private static final int METHOD_paintComponents79 = 79;
    private static final int METHOD_paintImmediately80 = 80;
    private static final int METHOD_postEvent81 = 81;
    private static final int METHOD_preferredSize82 = 82;
    private static final int METHOD_prepareImage83 = 83;
    private static final int METHOD_print84 = 84;
    private static final int METHOD_printAll85 = 85;
    private static final int METHOD_printComponents86 = 86;
    private static final int METHOD_printLayeredPane87 = 87;
    private static final int METHOD_putClientProperty88 = 88;
    private static final int METHOD_putLayer89 = 89;
    private static final int METHOD_registerKeyboardAction90 = 90;
    private static final int METHOD_remove91 = 91;
    private static final int METHOD_removeAll92 = 92;
    private static final int METHOD_removeNotify93 = 93;
    private static final int METHOD_removePropertyChangeListener94 = 94;
    private static final int METHOD_repaint95 = 95;
    private static final int METHOD_requestDefaultFocus96 = 96;
    private static final int METHOD_requestFocus97 = 97;
    private static final int METHOD_requestFocusInWindow98 = 98;
    private static final int METHOD_resetKeyboardActions99 = 99;
    private static final int METHOD_reshape100 = 100;
    private static final int METHOD_resize101 = 101;
    private static final int METHOD_revalidate102 = 102;
    private static final int METHOD_scrollRectToVisible103 = 103;
    private static final int METHOD_setBounds104 = 104;
    private static final int METHOD_setComponentZOrder105 = 105;
    private static final int METHOD_setDefaultLocale106 = 106;
    private static final int METHOD_setLayer107 = 107;
    private static final int METHOD_setPosition108 = 108;
    private static final int METHOD_show109 = 109;
    private static final int METHOD_size110 = 110;
    private static final int METHOD_toString111 = 111;
    private static final int METHOD_transferFocus112 = 112;
    private static final int METHOD_transferFocusBackward113 = 113;
    private static final int METHOD_transferFocusDownCycle114 = 114;
    private static final int METHOD_transferFocusUpCycle115 = 115;
    private static final int METHOD_unregisterKeyboardAction116 = 116;
    private static final int METHOD_update117 = 117;
    private static final int METHOD_updateUI118 = 118;
    private static final int METHOD_validate119 = 119;

    // Method array 
    /*lazy MethodDescriptor*/
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[120];
    
        try {
            methods[METHOD_action0] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_action0].setDisplayName ( "" );
            methods[METHOD_add1] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("add", new Class[] {java.awt.Component.class}));
            methods[METHOD_add1].setDisplayName ( "" );
            methods[METHOD_addNotify2] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("addNotify", new Class[] {}));
            methods[METHOD_addNotify2].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener3] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_addPropertyChangeListener3].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation4] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class}));
            methods[METHOD_applyComponentOrientation4].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet5] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE}));
            methods[METHOD_areFocusTraversalKeysSet5].setDisplayName ( "" );
            methods[METHOD_bounds6] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("bounds", new Class[] {}));
            methods[METHOD_bounds6].setDisplayName ( "" );
            methods[METHOD_checkImage7] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage7].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect8] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_computeVisibleRect8].setDisplayName ( "" );
            methods[METHOD_contains9] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_contains9].setDisplayName ( "" );
            methods[METHOD_countComponents10] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("countComponents", new Class[] {}));
            methods[METHOD_countComponents10].setDisplayName ( "" );
            methods[METHOD_createImage11] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class}));
            methods[METHOD_createImage11].setDisplayName ( "" );
            methods[METHOD_createToolTip12] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("createToolTip", new Class[] {}));
            methods[METHOD_createToolTip12].setDisplayName ( "" );
            methods[METHOD_createVolatileImage13] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createVolatileImage13].setDisplayName ( "" );
            methods[METHOD_deliverEvent14] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_deliverEvent14].setDisplayName ( "" );
            methods[METHOD_disable15] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("disable", new Class[] {}));
            methods[METHOD_disable15].setDisplayName ( "" );
            methods[METHOD_disableDoubleBuffering16] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("disableDoubleBuffering", new Class[] {java.awt.Component.class}));
            methods[METHOD_disableDoubleBuffering16].setDisplayName ( "" );
            methods[METHOD_dispatchEvent17] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class}));
            methods[METHOD_dispatchEvent17].setDisplayName ( "" );
            methods[METHOD_doLayout18] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("doLayout", new Class[] {}));
            methods[METHOD_doLayout18].setDisplayName ( "" );
            methods[METHOD_enable19] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("enable", new Class[] {}));
            methods[METHOD_enable19].setDisplayName ( "" );
            methods[METHOD_enableDoubleBuffering20] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("enableDoubleBuffering", new Class[] {java.awt.Component.class}));
            methods[METHOD_enableDoubleBuffering20].setDisplayName ( "" );
            methods[METHOD_enableInputMethods21] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE}));
            methods[METHOD_enableInputMethods21].setDisplayName ( "" );
            methods[METHOD_findComponentAt22] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_findComponentAt22].setDisplayName ( "" );
            methods[METHOD_firePropertyChange23] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Boolean.TYPE, Boolean.TYPE}));
            methods[METHOD_firePropertyChange23].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke24] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getActionForKeyStroke24].setDisplayName ( "" );
            methods[METHOD_getBounds25] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_getBounds25].setDisplayName ( "" );
            methods[METHOD_getClientProperty26] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class}));
            methods[METHOD_getClientProperty26].setDisplayName ( "" );
            methods[METHOD_getComponentAt27] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getComponentAt27].setDisplayName ( "" );
            methods[METHOD_getComponentZOrder28] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getComponentZOrder", new Class[] {java.awt.Component.class}));
            methods[METHOD_getComponentZOrder28].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke29] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getConditionForKeyStroke29].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale30] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getDefaultLocale", new Class[] {}));
            methods[METHOD_getDefaultLocale30].setDisplayName ( "" );
            methods[METHOD_getFontMetrics31] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class}));
            methods[METHOD_getFontMetrics31].setDisplayName ( "" );
            methods[METHOD_getIndexOf32] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getIndexOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_getIndexOf32].setDisplayName ( "" );
            methods[METHOD_getInsets33] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getInsets", new Class[] {java.awt.Insets.class}));
            methods[METHOD_getInsets33].setDisplayName ( "" );
            methods[METHOD_getLayer34] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getLayer", new Class[] {javax.swing.JComponent.class}));
            methods[METHOD_getLayer34].setDisplayName ( "" );
            methods[METHOD_getLayeredPaneAbove35] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getLayeredPaneAbove", new Class[] {java.awt.Component.class}));
            methods[METHOD_getLayeredPaneAbove35].setDisplayName ( "" );
            methods[METHOD_getListeners36] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getListeners", new Class[] {java.lang.Class.class}));
            methods[METHOD_getListeners36].setDisplayName ( "" );
            methods[METHOD_getLocation37] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_getLocation37].setDisplayName ( "" );
            methods[METHOD_getMousePosition38] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getMousePosition", new Class[] {Boolean.TYPE}));
            methods[METHOD_getMousePosition38].setDisplayName ( "" );
            methods[METHOD_getPopupLocation39] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getPopupLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getPopupLocation39].setDisplayName ( "" );
            methods[METHOD_getPosition40] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getPosition", new Class[] {java.awt.Component.class}));
            methods[METHOD_getPosition40].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners41] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class}));
            methods[METHOD_getPropertyChangeListeners41].setDisplayName ( "" );
            methods[METHOD_getSize42] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_getSize42].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation43] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipLocation43].setDisplayName ( "" );
            methods[METHOD_getToolTipText44] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipText44].setDisplayName ( "" );
            methods[METHOD_gotFocus45] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_gotFocus45].setDisplayName ( "" );
            methods[METHOD_grabFocus46] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("grabFocus", new Class[] {}));
            methods[METHOD_grabFocus46].setDisplayName ( "" );
            methods[METHOD_handleEvent47] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("handleEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_handleEvent47].setDisplayName ( "" );
            methods[METHOD_hasFocus48] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("hasFocus", new Class[] {}));
            methods[METHOD_hasFocus48].setDisplayName ( "" );
            methods[METHOD_hide49] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("hide", new Class[] {}));
            methods[METHOD_hide49].setDisplayName ( "" );
            methods[METHOD_highestLayer50] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("highestLayer", new Class[] {}));
            methods[METHOD_highestLayer50].setDisplayName ( "" );
            methods[METHOD_imageUpdate51] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_imageUpdate51].setDisplayName ( "" );
            methods[METHOD_insets52] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("insets", new Class[] {}));
            methods[METHOD_insets52].setDisplayName ( "" );
            methods[METHOD_inside53] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_inside53].setDisplayName ( "" );
            methods[METHOD_invalidate54] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("invalidate", new Class[] {}));
            methods[METHOD_invalidate54].setDisplayName ( "" );
            methods[METHOD_isAncestorOf55] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_isAncestorOf55].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot56] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class}));
            methods[METHOD_isFocusCycleRoot56].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent57] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class}));
            methods[METHOD_isLightweightComponent57].setDisplayName ( "" );
            methods[METHOD_keyDown58] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyDown58].setDisplayName ( "" );
            methods[METHOD_keyUp59] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyUp59].setDisplayName ( "" );
            methods[METHOD_layout60] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("layout", new Class[] {}));
            methods[METHOD_layout60].setDisplayName ( "" );
            methods[METHOD_list61] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE}));
            methods[METHOD_list61].setDisplayName ( "" );
            methods[METHOD_locate62] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_locate62].setDisplayName ( "" );
            methods[METHOD_location63] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("location", new Class[] {}));
            methods[METHOD_location63].setDisplayName ( "" );
            methods[METHOD_lostFocus64] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_lostFocus64].setDisplayName ( "" );
            methods[METHOD_lowestLayer65] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("lowestLayer", new Class[] {}));
            methods[METHOD_lowestLayer65].setDisplayName ( "" );
            methods[METHOD_minimumSize66] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("minimumSize", new Class[] {}));
            methods[METHOD_minimumSize66].setDisplayName ( "" );
            methods[METHOD_mouseDown67] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDown67].setDisplayName ( "" );
            methods[METHOD_mouseDrag68] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDrag68].setDisplayName ( "" );
            methods[METHOD_mouseEnter69] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseEnter69].setDisplayName ( "" );
            methods[METHOD_mouseExit70] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseExit70].setDisplayName ( "" );
            methods[METHOD_mouseMove71] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseMove71].setDisplayName ( "" );
            methods[METHOD_mouseUp72] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseUp72].setDisplayName ( "" );
            methods[METHOD_move73] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_move73].setDisplayName ( "" );
            methods[METHOD_moveToBack74] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("moveToBack", new Class[] {java.awt.Component.class}));
            methods[METHOD_moveToBack74].setDisplayName ( "" );
            methods[METHOD_moveToFront75] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("moveToFront", new Class[] {java.awt.Component.class}));
            methods[METHOD_moveToFront75].setDisplayName ( "" );
            methods[METHOD_nextFocus76] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("nextFocus", new Class[] {}));
            methods[METHOD_nextFocus76].setDisplayName ( "" );
            methods[METHOD_paint77] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("paint", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paint77].setDisplayName ( "" );
            methods[METHOD_paintAll78] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintAll78].setDisplayName ( "" );
            methods[METHOD_paintComponents79] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintComponents79].setDisplayName ( "" );
            methods[METHOD_paintImmediately80] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_paintImmediately80].setDisplayName ( "" );
            methods[METHOD_postEvent81] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("postEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_postEvent81].setDisplayName ( "" );
            methods[METHOD_preferredSize82] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("preferredSize", new Class[] {}));
            methods[METHOD_preferredSize82].setDisplayName ( "" );
            methods[METHOD_prepareImage83] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage83].setDisplayName ( "" );
            methods[METHOD_print84] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("print", new Class[] {java.awt.Graphics.class, java.awt.print.PageFormat.class, Integer.TYPE}));
            methods[METHOD_print84].setDisplayName ( "" );
            methods[METHOD_printAll85] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("printAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printAll85].setDisplayName ( "" );
            methods[METHOD_printComponents86] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printComponents86].setDisplayName ( "" );
            methods[METHOD_printLayeredPane87] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("printLayeredPane", new Class[] {}));
            methods[METHOD_printLayeredPane87].setDisplayName ( "" );
            methods[METHOD_putClientProperty88] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class}));
            methods[METHOD_putClientProperty88].setDisplayName ( "" );
            methods[METHOD_putLayer89] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("putLayer", new Class[] {javax.swing.JComponent.class, Integer.TYPE}));
            methods[METHOD_putLayer89].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction90] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction90].setDisplayName ( "" );
            methods[METHOD_remove91] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("remove", new Class[] {Integer.TYPE}));
            methods[METHOD_remove91].setDisplayName ( "" );
            methods[METHOD_removeAll92] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("removeAll", new Class[] {}));
            methods[METHOD_removeAll92].setDisplayName ( "" );
            methods[METHOD_removeNotify93] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("removeNotify", new Class[] {}));
            methods[METHOD_removeNotify93].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener94] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_removePropertyChangeListener94].setDisplayName ( "" );
            methods[METHOD_repaint95] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint95].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus96] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("requestDefaultFocus", new Class[] {}));
            methods[METHOD_requestDefaultFocus96].setDisplayName ( "" );
            methods[METHOD_requestFocus97] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("requestFocus", new Class[] {}));
            methods[METHOD_requestFocus97].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow98] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("requestFocusInWindow", new Class[] {}));
            methods[METHOD_requestFocusInWindow98].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions99] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("resetKeyboardActions", new Class[] {}));
            methods[METHOD_resetKeyboardActions99].setDisplayName ( "" );
            methods[METHOD_reshape100] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_reshape100].setDisplayName ( "" );
            methods[METHOD_resize101] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_resize101].setDisplayName ( "" );
            methods[METHOD_revalidate102] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("revalidate", new Class[] {}));
            methods[METHOD_revalidate102].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible103] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_scrollRectToVisible103].setDisplayName ( "" );
            methods[METHOD_setBounds104] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setBounds104].setDisplayName ( "" );
            methods[METHOD_setComponentZOrder105] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("setComponentZOrder", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_setComponentZOrder105].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale106] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class}));
            methods[METHOD_setDefaultLocale106].setDisplayName ( "" );
            methods[METHOD_setLayer107] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("setLayer", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_setLayer107].setDisplayName ( "" );
            methods[METHOD_setPosition108] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("setPosition", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_setPosition108].setDisplayName ( "" );
            methods[METHOD_show109] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("show", new Class[] {}));
            methods[METHOD_show109].setDisplayName ( "" );
            methods[METHOD_size110] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("size", new Class[] {}));
            methods[METHOD_size110].setDisplayName ( "" );
            methods[METHOD_toString111] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("toString", new Class[] {}));
            methods[METHOD_toString111].setDisplayName ( "" );
            methods[METHOD_transferFocus112] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("transferFocus", new Class[] {}));
            methods[METHOD_transferFocus112].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward113] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("transferFocusBackward", new Class[] {}));
            methods[METHOD_transferFocusBackward113].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle114] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("transferFocusDownCycle", new Class[] {}));
            methods[METHOD_transferFocusDownCycle114].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle115] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("transferFocusUpCycle", new Class[] {}));
            methods[METHOD_transferFocusUpCycle115].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction116] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_unregisterKeyboardAction116].setDisplayName ( "" );
            methods[METHOD_update117] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("update", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_update117].setDisplayName ( "" );
            methods[METHOD_updateUI118] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("updateUI", new Class[] {}));
            methods[METHOD_updateUI118].setDisplayName ( "" );
            methods[METHOD_validate119] = new MethodDescriptor ( kpi.beans.JBILayeredPane.class.getMethod("validate", new Class[] {}));
            methods[METHOD_validate119].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
	
	// Here you can add code for customizing the methods array.
	
        return methods;         }//GEN-LAST:Methods
    
    
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
    
    
//GEN-FIRST:Superclass
    
    // Here you can add code for customizing the Superclass BeanInfo.
    
//GEN-LAST:Superclass
    
    /**
     * Gets the bean's <code>BeanDescriptor</code>s.
     *
     * @return BeanDescriptor describing the editable
     * properties of this bean.  May return null if the
     * information should be obtained by automatic analysis.
     */
    public BeanDescriptor getBeanDescriptor() {
	return getBdescriptor();
    }
    
    /**
     * Gets the bean's <code>PropertyDescriptor</code>s.
     *
     * @return An array of PropertyDescriptors describing the editable
     * properties supported by this bean.  May return null if the
     * information should be obtained by automatic analysis.
     * <p>
     * If a property is indexed, then its entry in the result array will
     * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
     * A client of getPropertyDescriptors can use "instanceof" to check
     * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
     */
    public PropertyDescriptor[] getPropertyDescriptors() {
	return getPdescriptor();
    }
    
    /**
     * Gets the bean's <code>EventSetDescriptor</code>s.
     *
     * @return  An array of EventSetDescriptors describing the kinds of
     * events fired by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public EventSetDescriptor[] getEventSetDescriptors() {
	return getEdescriptor();
    }
    
    /**
     * Gets the bean's <code>MethodDescriptor</code>s.
     *
     * @return  An array of MethodDescriptors describing the methods
     * implemented by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public MethodDescriptor[] getMethodDescriptors() {
	return getMdescriptor();
    }
    
    /**
     * A bean may have a "default" property that is the property that will
     * mostly commonly be initially chosen for update by human's who are
     * customizing the bean.
     * @return  Index of default property in the PropertyDescriptor array
     * 		returned by getPropertyDescriptors.
     * <P>	Returns -1 if there is no default property.
     */
    public int getDefaultPropertyIndex() {
	return defaultPropertyIndex;
    }
    
    /**
     * A bean may have a "default" event that is the event that will
     * mostly commonly be used by human's when using the bean.
     * @return Index of default event in the EventSetDescriptor array
     *		returned by getEventSetDescriptors.
     * <P>	Returns -1 if there is no default event.
     */
    public int getDefaultEventIndex() {
	return defaultEventIndex;
    }
}

