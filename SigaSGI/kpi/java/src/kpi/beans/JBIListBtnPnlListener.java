package kpi.beans;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import kpi.xml.BIXMLRecord;

/*
 * JBIListPanelListener.java
 *
 * Created on 21 de Setembro de 2004, 17:32
 */

/**
 *
 * @author  alexandreas
 */
public interface JBIListBtnPnlListener {
    public BIXMLRecord getCmdOnNewPressed(ActionEvent evt);
    public BIXMLRecord getCmdOnBtnUser1Pressed(ActionEvent evt);
    public BIXMLRecord getCmdOnBtnViewPressed(ActionEvent evt);
    public BIXMLRecord getCmdOnBtnReloadPressed(ActionEvent evt);
    public BIXMLRecord xmlTableMouseClicked(MouseEvent evt);

    public void doBtnUser1ActionPerformed(ActionEvent evt);

}
