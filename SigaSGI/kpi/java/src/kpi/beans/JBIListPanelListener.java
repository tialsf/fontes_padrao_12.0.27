package kpi.beans;
/*
 * JBIListPanelListener.java
 *
 * Created on 21 de Setembro de 2004, 17:32
 */

/**
 *
 * @author  alexandreas
 */
public interface JBIListPanelListener {
    public void mousePressed(java.awt.event.ComponentEvent componentEvent);
    public void keyPressed(java.awt.event.ComponentEvent componentEvent);
}
