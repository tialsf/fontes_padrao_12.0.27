
package kpi.beans;

import java.util.Vector;
import javax.swing.JInternalFrame;

/**
 *
 * @author  siga1996
 */
public class JBIMessagePanel extends javax.swing.JPanel {

    String parentId = null;
    String contextId = null;
    kpi.swing.KpiDefaultFrameBehavior frame = null;
    java.util.Locale defaultLocale;
    private Vector vctJBIListPanelListener = new Vector();

    public JBIMessagePanel() {
        initComponents();

        xmlTable.getTable().addMouseListener(new java.awt.event.MouseAdapter() {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                xmlTableMouseClicked(evt);
            }
        });
    }

    public void setDataSource(kpi.xml.BIXMLVector dataSource, String parent, String context, kpi.swing.KpiDefaultFrameBehavior frameAux) {
        parentId = String.valueOf(parent);
        contextId = String.valueOf(context);
        xmlTable.setDataSource(dataSource);
        frame = frameAux;
    }

    public void setNewVisible(boolean newVisible) {
        btnNew.setVisible(newVisible);
    }

    public void setNewEnabled(boolean newEnabled) {
        btnNew.setEnabled(newEnabled);
    }

    public boolean getNewEnabled() {
        return btnNew.isEnabled();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlTools = new javax.swing.JPanel();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnNew = new javax.swing.JButton();
        btnView = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnAjuda = new javax.swing.JButton();
        pnlList = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        pnlTopRecord = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        scpList = new javax.swing.JScrollPane();
        xmlTable = new kpi.beans.JBIXMLTable();

        setLayout(new java.awt.BorderLayout());

        pnlTools.setMinimumSize(new java.awt.Dimension(600, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 22));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlOperation.setMaximumSize(new java.awt.Dimension(580, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(580, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(620, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setMaximumSize(new java.awt.Dimension(500, 29));
        tbInsertion.setMinimumSize(new java.awt.Dimension(500, 29));
        tbInsertion.setPreferredSize(new java.awt.Dimension(500, 32));

        btnNew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_novo.gif"))); // NOI18N
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnNew.setText(bundle.getString("JBIListPanel_00001")); // NOI18N
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });
        tbInsertion.add(btnNew);

        btnView.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_visualizar.gif"))); // NOI18N
        btnView.setText(bundle.getString("JBIListPanel_00002")); // NOI18N
        btnView.setToolTipText("");
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });
        tbInsertion.add(btnView);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("JBIMessagePanel_00001")); // NOI18N
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda.setText(bundle.getString("KpiMainPanel_00008")); // NOI18N
        btnAjuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlList.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlList.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setMaximumSize(new java.awt.Dimension(10, 10));
        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlList.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlList.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlList.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        jPanel1.setLayout(new java.awt.BorderLayout());

        scpList.setBorder(null);
        scpList.setAutoscrolls(true);
        scpList.setEnabled(false);
        scpList.setPreferredSize(new java.awt.Dimension(240, 450));
        scpList.setViewportView(xmlTable);

        jPanel1.add(scpList, java.awt.BorderLayout.CENTER);

        pnlList.add(jPanel1, java.awt.BorderLayout.CENTER);

        add(pnlList, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        if ((xmlTable.getType() != null) && (!"".equals(xmlTable.getType()))) {
            kpi.core.KpiStaticReferences.getKpiFormController().newDetailForm(xmlTable.getType(),
                    parentId, contextId);
        }
    }//GEN-LAST:event_btnNewActionPerformed

    private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed
        int i = xmlTable.getSelectedRow();
        if (xmlTable.getSelectedRow() != -1) {
            BITableModel tableModel = (BITableModel) xmlTable.getModel();
            JInternalFrame formulario =
                    kpi.core.KpiStaticReferences.getKpiFormController().getForm(
                    tableModel.getRecordType(xmlTable.getSelectedRow()),
                    tableModel.getRecordID(xmlTable.getSelectedRow()),
                    tableModel.getName(xmlTable.getSelectedRow())).asJInternalFrame();
            formulario.putClientProperty("FORM_PARENT", this);
        }
    }//GEN-LAST:event_btnViewActionPerformed

    private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
        frame.loadRecord();
    }//GEN-LAST:event_btnReloadActionPerformed

    private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
        frame.loadHelp(xmlTable.getType());
    }//GEN-LAST:event_btnAjudaActionPerformed

	private void xmlTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_xmlTableMouseClicked
            if (evt.getClickCount() == 2) {
                BITableModel tableModel = (BITableModel) xmlTable.getModel();
                JInternalFrame formulario =
                        kpi.core.KpiStaticReferences.getKpiFormController().getForm(
                        tableModel.getRecordType(xmlTable.getSelectedRow()),
                        tableModel.getRecordID(xmlTable.getSelectedRow()),
                        tableModel.getName(xmlTable.getSelectedRow())).asJInternalFrame();
                formulario.putClientProperty("FORM_PARENT", this);
            }
	}//GEN-LAST:event_xmlTableMouseClicked
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnAjuda;
    public javax.swing.JButton btnNew;
    public javax.swing.JButton btnReload;
    public javax.swing.JButton btnView;
    public javax.swing.JPanel jPanel1;
    public javax.swing.JPanel pnlBottomRecord;
    public javax.swing.JPanel pnlLeftRecord;
    public javax.swing.JPanel pnlList;
    public javax.swing.JPanel pnlOperation;
    public javax.swing.JPanel pnlRightRecord;
    public javax.swing.JPanel pnlTools;
    public javax.swing.JPanel pnlTopRecord;
    public javax.swing.JScrollPane scpList;
    public javax.swing.JToolBar tbInsertion;
    public kpi.beans.JBIXMLTable xmlTable;
    // End of variables declaration//GEN-END:variables

    public void addJBIListPanelListener(kpi.beans.JBIListPanelListener listener) {
        vctJBIListPanelListener.add(listener);
    }

    public void removeJBIListPanelListener(kpi.beans.JBIListPanelListener listener) {
        vctJBIListPanelListener.remove(listener);
    }

    public kpi.swing.KpiDefaultFrameBehavior getFrame() {
        return frame;
    }
}
