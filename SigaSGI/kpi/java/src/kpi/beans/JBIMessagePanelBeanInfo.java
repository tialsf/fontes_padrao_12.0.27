/*
 * JBIMessagePanelBeanInfo.java
 *
 * Created on 23 de Setembro de 2008, 15:09
 */

package kpi.beans;

import java.beans.*;

/**
 * @author lucio.pelinson
 */
public class JBIMessagePanelBeanInfo extends SimpleBeanInfo {
	
    // Bean descriptor//GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( kpi.beans.JBIMessagePanel.class , null ); // NOI18N//GEN-HEADEREND:BeanDescriptor
		
		// Here you can add code for customizing the BeanDescriptor.
		
        return beanDescriptor;     }//GEN-LAST:BeanDescriptor
	
	
    // Property identifiers//GEN-FIRST:Properties
    private static final int PROPERTY_accessibleContext = 0;
    private static final int PROPERTY_actionMap = 1;
    private static final int PROPERTY_alignmentX = 2;
    private static final int PROPERTY_alignmentY = 3;
    private static final int PROPERTY_ancestorListeners = 4;
    private static final int PROPERTY_autoscrolls = 5;
    private static final int PROPERTY_background = 6;
    private static final int PROPERTY_backgroundSet = 7;
    private static final int PROPERTY_border = 8;
    private static final int PROPERTY_bounds = 9;
    private static final int PROPERTY_colorModel = 10;
    private static final int PROPERTY_component = 11;
    private static final int PROPERTY_componentCount = 12;
    private static final int PROPERTY_componentListeners = 13;
    private static final int PROPERTY_componentOrientation = 14;
    private static final int PROPERTY_componentPopupMenu = 15;
    private static final int PROPERTY_components = 16;
    private static final int PROPERTY_containerListeners = 17;
    private static final int PROPERTY_cursor = 18;
    private static final int PROPERTY_cursorSet = 19;
    private static final int PROPERTY_debugGraphicsOptions = 20;
    private static final int PROPERTY_displayable = 21;
    private static final int PROPERTY_doubleBuffered = 22;
    private static final int PROPERTY_dropTarget = 23;
    private static final int PROPERTY_enabled = 24;
    private static final int PROPERTY_focusable = 25;
    private static final int PROPERTY_focusCycleRoot = 26;
    private static final int PROPERTY_focusCycleRootAncestor = 27;
    private static final int PROPERTY_focusListeners = 28;
    private static final int PROPERTY_focusOwner = 29;
    private static final int PROPERTY_focusTraversable = 30;
    private static final int PROPERTY_focusTraversalKeys = 31;
    private static final int PROPERTY_focusTraversalKeysEnabled = 32;
    private static final int PROPERTY_focusTraversalPolicy = 33;
    private static final int PROPERTY_focusTraversalPolicyProvider = 34;
    private static final int PROPERTY_focusTraversalPolicySet = 35;
    private static final int PROPERTY_font = 36;
    private static final int PROPERTY_fontSet = 37;
    private static final int PROPERTY_foreground = 38;
    private static final int PROPERTY_foregroundSet = 39;
    private static final int PROPERTY_frame = 40;
    private static final int PROPERTY_graphics = 41;
    private static final int PROPERTY_graphicsConfiguration = 42;
    private static final int PROPERTY_height = 43;
    private static final int PROPERTY_hierarchyBoundsListeners = 44;
    private static final int PROPERTY_hierarchyListeners = 45;
    private static final int PROPERTY_ignoreRepaint = 46;
    private static final int PROPERTY_inheritsPopupMenu = 47;
    private static final int PROPERTY_inputContext = 48;
    private static final int PROPERTY_inputMap = 49;
    private static final int PROPERTY_inputMethodListeners = 50;
    private static final int PROPERTY_inputMethodRequests = 51;
    private static final int PROPERTY_inputVerifier = 52;
    private static final int PROPERTY_insets = 53;
    private static final int PROPERTY_keyListeners = 54;
    private static final int PROPERTY_layout = 55;
    private static final int PROPERTY_lightweight = 56;
    private static final int PROPERTY_locale = 57;
    private static final int PROPERTY_location = 58;
    private static final int PROPERTY_locationOnScreen = 59;
    private static final int PROPERTY_managingFocus = 60;
    private static final int PROPERTY_maximumSize = 61;
    private static final int PROPERTY_maximumSizeSet = 62;
    private static final int PROPERTY_minimumSize = 63;
    private static final int PROPERTY_minimumSizeSet = 64;
    private static final int PROPERTY_mouseListeners = 65;
    private static final int PROPERTY_mouseMotionListeners = 66;
    private static final int PROPERTY_mousePosition = 67;
    private static final int PROPERTY_mouseWheelListeners = 68;
    private static final int PROPERTY_name = 69;
    private static final int PROPERTY_newEnabled = 70;
    private static final int PROPERTY_newVisible = 71;
    private static final int PROPERTY_nextFocusableComponent = 72;
    private static final int PROPERTY_opaque = 73;
    private static final int PROPERTY_optimizedDrawingEnabled = 74;
    private static final int PROPERTY_paintingTile = 75;
    private static final int PROPERTY_parent = 76;
    private static final int PROPERTY_peer = 77;
    private static final int PROPERTY_preferredSize = 78;
    private static final int PROPERTY_preferredSizeSet = 79;
    private static final int PROPERTY_propertyChangeListeners = 80;
    private static final int PROPERTY_registeredKeyStrokes = 81;
    private static final int PROPERTY_requestFocusEnabled = 82;
    private static final int PROPERTY_rootPane = 83;
    private static final int PROPERTY_showing = 84;
    private static final int PROPERTY_size = 85;
    private static final int PROPERTY_toolkit = 86;
    private static final int PROPERTY_toolTipText = 87;
    private static final int PROPERTY_topLevelAncestor = 88;
    private static final int PROPERTY_transferHandler = 89;
    private static final int PROPERTY_treeLock = 90;
    private static final int PROPERTY_UI = 91;
    private static final int PROPERTY_UIClassID = 92;
    private static final int PROPERTY_valid = 93;
    private static final int PROPERTY_validateRoot = 94;
    private static final int PROPERTY_verifyInputWhenFocusTarget = 95;
    private static final int PROPERTY_vetoableChangeListeners = 96;
    private static final int PROPERTY_visible = 97;
    private static final int PROPERTY_visibleRect = 98;
    private static final int PROPERTY_width = 99;
    private static final int PROPERTY_x = 100;
    private static final int PROPERTY_y = 101;

    // Property array 
    /*lazy PropertyDescriptor*/
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[102];
    
        try {
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", kpi.beans.JBIMessagePanel.class, "getAccessibleContext", null ); // NOI18N
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", kpi.beans.JBIMessagePanel.class, "getActionMap", "setActionMap" ); // NOI18N
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", kpi.beans.JBIMessagePanel.class, "getAlignmentX", "setAlignmentX" ); // NOI18N
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", kpi.beans.JBIMessagePanel.class, "getAlignmentY", "setAlignmentY" ); // NOI18N
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", kpi.beans.JBIMessagePanel.class, "getAncestorListeners", null ); // NOI18N
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", kpi.beans.JBIMessagePanel.class, "getAutoscrolls", "setAutoscrolls" ); // NOI18N
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", kpi.beans.JBIMessagePanel.class, "getBackground", "setBackground" ); // NOI18N
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", kpi.beans.JBIMessagePanel.class, "isBackgroundSet", null ); // NOI18N
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", kpi.beans.JBIMessagePanel.class, "getBorder", "setBorder" ); // NOI18N
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", kpi.beans.JBIMessagePanel.class, "getBounds", "setBounds" ); // NOI18N
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", kpi.beans.JBIMessagePanel.class, "getColorModel", null ); // NOI18N
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", kpi.beans.JBIMessagePanel.class, null, null, "getComponent", null ); // NOI18N
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", kpi.beans.JBIMessagePanel.class, "getComponentCount", null ); // NOI18N
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", kpi.beans.JBIMessagePanel.class, "getComponentListeners", null ); // NOI18N
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", kpi.beans.JBIMessagePanel.class, "getComponentOrientation", "setComponentOrientation" ); // NOI18N
            properties[PROPERTY_componentPopupMenu] = new PropertyDescriptor ( "componentPopupMenu", kpi.beans.JBIMessagePanel.class, "getComponentPopupMenu", "setComponentPopupMenu" ); // NOI18N
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", kpi.beans.JBIMessagePanel.class, "getComponents", null ); // NOI18N
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", kpi.beans.JBIMessagePanel.class, "getContainerListeners", null ); // NOI18N
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", kpi.beans.JBIMessagePanel.class, "getCursor", "setCursor" ); // NOI18N
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", kpi.beans.JBIMessagePanel.class, "isCursorSet", null ); // NOI18N
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", kpi.beans.JBIMessagePanel.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" ); // NOI18N
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", kpi.beans.JBIMessagePanel.class, "isDisplayable", null ); // NOI18N
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", kpi.beans.JBIMessagePanel.class, "isDoubleBuffered", "setDoubleBuffered" ); // NOI18N
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", kpi.beans.JBIMessagePanel.class, "getDropTarget", "setDropTarget" ); // NOI18N
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", kpi.beans.JBIMessagePanel.class, "isEnabled", "setEnabled" ); // NOI18N
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", kpi.beans.JBIMessagePanel.class, "isFocusable", "setFocusable" ); // NOI18N
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", kpi.beans.JBIMessagePanel.class, "isFocusCycleRoot", "setFocusCycleRoot" ); // NOI18N
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", kpi.beans.JBIMessagePanel.class, "getFocusCycleRootAncestor", null ); // NOI18N
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", kpi.beans.JBIMessagePanel.class, "getFocusListeners", null ); // NOI18N
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", kpi.beans.JBIMessagePanel.class, "isFocusOwner", null ); // NOI18N
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", kpi.beans.JBIMessagePanel.class, "isFocusTraversable", null ); // NOI18N
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", kpi.beans.JBIMessagePanel.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" ); // NOI18N
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", kpi.beans.JBIMessagePanel.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" ); // NOI18N
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", kpi.beans.JBIMessagePanel.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" ); // NOI18N
            properties[PROPERTY_focusTraversalPolicyProvider] = new PropertyDescriptor ( "focusTraversalPolicyProvider", kpi.beans.JBIMessagePanel.class, "isFocusTraversalPolicyProvider", "setFocusTraversalPolicyProvider" ); // NOI18N
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", kpi.beans.JBIMessagePanel.class, "isFocusTraversalPolicySet", null ); // NOI18N
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", kpi.beans.JBIMessagePanel.class, "getFont", "setFont" ); // NOI18N
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", kpi.beans.JBIMessagePanel.class, "isFontSet", null ); // NOI18N
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", kpi.beans.JBIMessagePanel.class, "getForeground", "setForeground" ); // NOI18N
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", kpi.beans.JBIMessagePanel.class, "isForegroundSet", null ); // NOI18N
            properties[PROPERTY_frame] = new PropertyDescriptor ( "frame", kpi.beans.JBIMessagePanel.class, "getFrame", null ); // NOI18N
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", kpi.beans.JBIMessagePanel.class, "getGraphics", null ); // NOI18N
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", kpi.beans.JBIMessagePanel.class, "getGraphicsConfiguration", null ); // NOI18N
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", kpi.beans.JBIMessagePanel.class, "getHeight", null ); // NOI18N
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", kpi.beans.JBIMessagePanel.class, "getHierarchyBoundsListeners", null ); // NOI18N
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", kpi.beans.JBIMessagePanel.class, "getHierarchyListeners", null ); // NOI18N
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", kpi.beans.JBIMessagePanel.class, "getIgnoreRepaint", "setIgnoreRepaint" ); // NOI18N
            properties[PROPERTY_inheritsPopupMenu] = new PropertyDescriptor ( "inheritsPopupMenu", kpi.beans.JBIMessagePanel.class, "getInheritsPopupMenu", "setInheritsPopupMenu" ); // NOI18N
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", kpi.beans.JBIMessagePanel.class, "getInputContext", null ); // NOI18N
            properties[PROPERTY_inputMap] = new PropertyDescriptor ( "inputMap", kpi.beans.JBIMessagePanel.class, "getInputMap", null ); // NOI18N
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", kpi.beans.JBIMessagePanel.class, "getInputMethodListeners", null ); // NOI18N
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", kpi.beans.JBIMessagePanel.class, "getInputMethodRequests", null ); // NOI18N
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", kpi.beans.JBIMessagePanel.class, "getInputVerifier", "setInputVerifier" ); // NOI18N
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", kpi.beans.JBIMessagePanel.class, "getInsets", null ); // NOI18N
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", kpi.beans.JBIMessagePanel.class, "getKeyListeners", null ); // NOI18N
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", kpi.beans.JBIMessagePanel.class, "getLayout", "setLayout" ); // NOI18N
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", kpi.beans.JBIMessagePanel.class, "isLightweight", null ); // NOI18N
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", kpi.beans.JBIMessagePanel.class, "getLocale", "setLocale" ); // NOI18N
            properties[PROPERTY_location] = new PropertyDescriptor ( "location", kpi.beans.JBIMessagePanel.class, "getLocation", "setLocation" ); // NOI18N
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", kpi.beans.JBIMessagePanel.class, "getLocationOnScreen", null ); // NOI18N
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", kpi.beans.JBIMessagePanel.class, "isManagingFocus", null ); // NOI18N
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", kpi.beans.JBIMessagePanel.class, "getMaximumSize", "setMaximumSize" ); // NOI18N
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", kpi.beans.JBIMessagePanel.class, "isMaximumSizeSet", null ); // NOI18N
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", kpi.beans.JBIMessagePanel.class, "getMinimumSize", "setMinimumSize" ); // NOI18N
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", kpi.beans.JBIMessagePanel.class, "isMinimumSizeSet", null ); // NOI18N
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", kpi.beans.JBIMessagePanel.class, "getMouseListeners", null ); // NOI18N
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", kpi.beans.JBIMessagePanel.class, "getMouseMotionListeners", null ); // NOI18N
            properties[PROPERTY_mousePosition] = new PropertyDescriptor ( "mousePosition", kpi.beans.JBIMessagePanel.class, "getMousePosition", null ); // NOI18N
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", kpi.beans.JBIMessagePanel.class, "getMouseWheelListeners", null ); // NOI18N
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", kpi.beans.JBIMessagePanel.class, "getName", "setName" ); // NOI18N
            properties[PROPERTY_newEnabled] = new PropertyDescriptor ( "newEnabled", kpi.beans.JBIMessagePanel.class, "getNewEnabled", "setNewEnabled" ); // NOI18N
            properties[PROPERTY_newVisible] = new PropertyDescriptor ( "newVisible", kpi.beans.JBIMessagePanel.class, null, "setNewVisible" ); // NOI18N
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", kpi.beans.JBIMessagePanel.class, "getNextFocusableComponent", "setNextFocusableComponent" ); // NOI18N
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", kpi.beans.JBIMessagePanel.class, "isOpaque", "setOpaque" ); // NOI18N
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", kpi.beans.JBIMessagePanel.class, "isOptimizedDrawingEnabled", null ); // NOI18N
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", kpi.beans.JBIMessagePanel.class, "isPaintingTile", null ); // NOI18N
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", kpi.beans.JBIMessagePanel.class, "getParent", null ); // NOI18N
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", kpi.beans.JBIMessagePanel.class, "getPeer", null ); // NOI18N
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", kpi.beans.JBIMessagePanel.class, "getPreferredSize", "setPreferredSize" ); // NOI18N
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", kpi.beans.JBIMessagePanel.class, "isPreferredSizeSet", null ); // NOI18N
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", kpi.beans.JBIMessagePanel.class, "getPropertyChangeListeners", null ); // NOI18N
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", kpi.beans.JBIMessagePanel.class, "getRegisteredKeyStrokes", null ); // NOI18N
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", kpi.beans.JBIMessagePanel.class, "isRequestFocusEnabled", "setRequestFocusEnabled" ); // NOI18N
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", kpi.beans.JBIMessagePanel.class, "getRootPane", null ); // NOI18N
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", kpi.beans.JBIMessagePanel.class, "isShowing", null ); // NOI18N
            properties[PROPERTY_size] = new PropertyDescriptor ( "size", kpi.beans.JBIMessagePanel.class, "getSize", "setSize" ); // NOI18N
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", kpi.beans.JBIMessagePanel.class, "getToolkit", null ); // NOI18N
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", kpi.beans.JBIMessagePanel.class, "getToolTipText", "setToolTipText" ); // NOI18N
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", kpi.beans.JBIMessagePanel.class, "getTopLevelAncestor", null ); // NOI18N
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", kpi.beans.JBIMessagePanel.class, "getTransferHandler", "setTransferHandler" ); // NOI18N
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", kpi.beans.JBIMessagePanel.class, "getTreeLock", null ); // NOI18N
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", kpi.beans.JBIMessagePanel.class, "getUI", "setUI" ); // NOI18N
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", kpi.beans.JBIMessagePanel.class, "getUIClassID", null ); // NOI18N
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", kpi.beans.JBIMessagePanel.class, "isValid", null ); // NOI18N
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", kpi.beans.JBIMessagePanel.class, "isValidateRoot", null ); // NOI18N
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", kpi.beans.JBIMessagePanel.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" ); // NOI18N
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", kpi.beans.JBIMessagePanel.class, "getVetoableChangeListeners", null ); // NOI18N
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", kpi.beans.JBIMessagePanel.class, "isVisible", "setVisible" ); // NOI18N
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", kpi.beans.JBIMessagePanel.class, "getVisibleRect", null ); // NOI18N
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", kpi.beans.JBIMessagePanel.class, "getWidth", null ); // NOI18N
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", kpi.beans.JBIMessagePanel.class, "getX", null ); // NOI18N
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", kpi.beans.JBIMessagePanel.class, "getY", null ); // NOI18N
        }
        catch(IntrospectionException e) {
            e.printStackTrace();
        }//GEN-HEADEREND:Properties
		
		// Here you can add code for customizing the properties array.
		
        return properties;     }//GEN-LAST:Properties
	
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_ancestorListener = 0;
    private static final int EVENT_componentListener = 1;
    private static final int EVENT_containerListener = 2;
    private static final int EVENT_focusListener = 3;
    private static final int EVENT_hierarchyBoundsListener = 4;
    private static final int EVENT_hierarchyListener = 5;
    private static final int EVENT_inputMethodListener = 6;
    private static final int EVENT_keyListener = 7;
    private static final int EVENT_mouseListener = 8;
    private static final int EVENT_mouseMotionListener = 9;
    private static final int EVENT_mouseWheelListener = 10;
    private static final int EVENT_propertyChangeListener = 11;
    private static final int EVENT_vetoableChangeListener = 12;

    // EventSet array
    /*lazy EventSetDescriptor*/
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[13];
    
        try {
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( kpi.beans.JBIMessagePanel.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorAdded", "ancestorMoved", "ancestorRemoved"}, "addAncestorListener", "removeAncestorListener" ); // NOI18N
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( kpi.beans.JBIMessagePanel.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentHidden", "componentMoved", "componentResized", "componentShown"}, "addComponentListener", "removeComponentListener" ); // NOI18N
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( kpi.beans.JBIMessagePanel.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" ); // NOI18N
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( kpi.beans.JBIMessagePanel.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusGained", "focusLost"}, "addFocusListener", "removeFocusListener" ); // NOI18N
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( kpi.beans.JBIMessagePanel.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" ); // NOI18N
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( kpi.beans.JBIMessagePanel.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" ); // NOI18N
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( kpi.beans.JBIMessagePanel.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" ); // NOI18N
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( kpi.beans.JBIMessagePanel.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyReleased", "keyTyped"}, "addKeyListener", "removeKeyListener" ); // NOI18N
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( kpi.beans.JBIMessagePanel.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mouseEntered", "mouseExited", "mousePressed", "mouseReleased"}, "addMouseListener", "removeMouseListener" ); // NOI18N
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( kpi.beans.JBIMessagePanel.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" ); // NOI18N
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( kpi.beans.JBIMessagePanel.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" ); // NOI18N
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( kpi.beans.JBIMessagePanel.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" ); // NOI18N
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( kpi.beans.JBIMessagePanel.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" ); // NOI18N
        }
        catch(IntrospectionException e) {
            e.printStackTrace();
        }//GEN-HEADEREND:Events
		
		// Here you can add code for customizing the event sets array.
		
        return eventSets;     }//GEN-LAST:Events
	
    // Method identifiers//GEN-FIRST:Methods
    private static final int METHOD_action0 = 0;
    private static final int METHOD_add1 = 1;
    private static final int METHOD_addJBIListPanelListener2 = 2;
    private static final int METHOD_addNotify3 = 3;
    private static final int METHOD_addPropertyChangeListener4 = 4;
    private static final int METHOD_applyComponentOrientation5 = 5;
    private static final int METHOD_areFocusTraversalKeysSet6 = 6;
    private static final int METHOD_bounds7 = 7;
    private static final int METHOD_checkImage8 = 8;
    private static final int METHOD_computeVisibleRect9 = 9;
    private static final int METHOD_contains10 = 10;
    private static final int METHOD_countComponents11 = 11;
    private static final int METHOD_createImage12 = 12;
    private static final int METHOD_createToolTip13 = 13;
    private static final int METHOD_createVolatileImage14 = 14;
    private static final int METHOD_deliverEvent15 = 15;
    private static final int METHOD_disable16 = 16;
    private static final int METHOD_dispatchEvent17 = 17;
    private static final int METHOD_doLayout18 = 18;
    private static final int METHOD_enable19 = 19;
    private static final int METHOD_enableInputMethods20 = 20;
    private static final int METHOD_findComponentAt21 = 21;
    private static final int METHOD_firePropertyChange22 = 22;
    private static final int METHOD_getActionForKeyStroke23 = 23;
    private static final int METHOD_getBounds24 = 24;
    private static final int METHOD_getClientProperty25 = 25;
    private static final int METHOD_getComponentAt26 = 26;
    private static final int METHOD_getComponentZOrder27 = 27;
    private static final int METHOD_getConditionForKeyStroke28 = 28;
    private static final int METHOD_getDefaultLocale29 = 29;
    private static final int METHOD_getFontMetrics30 = 30;
    private static final int METHOD_getInsets31 = 31;
    private static final int METHOD_getListeners32 = 32;
    private static final int METHOD_getLocation33 = 33;
    private static final int METHOD_getMousePosition34 = 34;
    private static final int METHOD_getPopupLocation35 = 35;
    private static final int METHOD_getPropertyChangeListeners36 = 36;
    private static final int METHOD_getSize37 = 37;
    private static final int METHOD_getToolTipLocation38 = 38;
    private static final int METHOD_getToolTipText39 = 39;
    private static final int METHOD_gotFocus40 = 40;
    private static final int METHOD_grabFocus41 = 41;
    private static final int METHOD_handleEvent42 = 42;
    private static final int METHOD_hasFocus43 = 43;
    private static final int METHOD_hide44 = 44;
    private static final int METHOD_imageUpdate45 = 45;
    private static final int METHOD_insets46 = 46;
    private static final int METHOD_inside47 = 47;
    private static final int METHOD_invalidate48 = 48;
    private static final int METHOD_isAncestorOf49 = 49;
    private static final int METHOD_isFocusCycleRoot50 = 50;
    private static final int METHOD_isLightweightComponent51 = 51;
    private static final int METHOD_keyDown52 = 52;
    private static final int METHOD_keyUp53 = 53;
    private static final int METHOD_layout54 = 54;
    private static final int METHOD_list55 = 55;
    private static final int METHOD_locate56 = 56;
    private static final int METHOD_location57 = 57;
    private static final int METHOD_lostFocus58 = 58;
    private static final int METHOD_minimumSize59 = 59;
    private static final int METHOD_mouseDown60 = 60;
    private static final int METHOD_mouseDrag61 = 61;
    private static final int METHOD_mouseEnter62 = 62;
    private static final int METHOD_mouseExit63 = 63;
    private static final int METHOD_mouseMove64 = 64;
    private static final int METHOD_mouseUp65 = 65;
    private static final int METHOD_move66 = 66;
    private static final int METHOD_nextFocus67 = 67;
    private static final int METHOD_paint68 = 68;
    private static final int METHOD_paintAll69 = 69;
    private static final int METHOD_paintComponents70 = 70;
    private static final int METHOD_paintImmediately71 = 71;
    private static final int METHOD_postEvent72 = 72;
    private static final int METHOD_preferredSize73 = 73;
    private static final int METHOD_prepareImage74 = 74;
    private static final int METHOD_print75 = 75;
    private static final int METHOD_printAll76 = 76;
    private static final int METHOD_printComponents77 = 77;
    private static final int METHOD_putClientProperty78 = 78;
    private static final int METHOD_registerKeyboardAction79 = 79;
    private static final int METHOD_remove80 = 80;
    private static final int METHOD_removeAll81 = 81;
    private static final int METHOD_removeJBIListPanelListener82 = 82;
    private static final int METHOD_removeNotify83 = 83;
    private static final int METHOD_removePropertyChangeListener84 = 84;
    private static final int METHOD_repaint85 = 85;
    private static final int METHOD_requestDefaultFocus86 = 86;
    private static final int METHOD_requestFocus87 = 87;
    private static final int METHOD_requestFocusInWindow88 = 88;
    private static final int METHOD_resetKeyboardActions89 = 89;
    private static final int METHOD_reshape90 = 90;
    private static final int METHOD_resize91 = 91;
    private static final int METHOD_revalidate92 = 92;
    private static final int METHOD_scrollRectToVisible93 = 93;
    private static final int METHOD_setBounds94 = 94;
    private static final int METHOD_setComponentZOrder95 = 95;
    private static final int METHOD_setDataSource96 = 96;
    private static final int METHOD_setDefaultLocale97 = 97;
    private static final int METHOD_show98 = 98;
    private static final int METHOD_size99 = 99;
    private static final int METHOD_toString100 = 100;
    private static final int METHOD_transferFocus101 = 101;
    private static final int METHOD_transferFocusBackward102 = 102;
    private static final int METHOD_transferFocusDownCycle103 = 103;
    private static final int METHOD_transferFocusUpCycle104 = 104;
    private static final int METHOD_unregisterKeyboardAction105 = 105;
    private static final int METHOD_update106 = 106;
    private static final int METHOD_updateUI107 = 107;
    private static final int METHOD_validate108 = 108;

    // Method array 
    /*lazy MethodDescriptor*/
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[109];
    
        try {
            methods[METHOD_action0] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_action0].setDisplayName ( "" );
            methods[METHOD_add1] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("add", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_add1].setDisplayName ( "" );
            methods[METHOD_addJBIListPanelListener2] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("addJBIListPanelListener", new Class[] {kpi.beans.JBIListPanelListener.class})); // NOI18N
            methods[METHOD_addJBIListPanelListener2].setDisplayName ( "" );
            methods[METHOD_addNotify3] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("addNotify", new Class[] {})); // NOI18N
            methods[METHOD_addNotify3].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener4] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class})); // NOI18N
            methods[METHOD_addPropertyChangeListener4].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation5] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class})); // NOI18N
            methods[METHOD_applyComponentOrientation5].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet6] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE})); // NOI18N
            methods[METHOD_areFocusTraversalKeysSet6].setDisplayName ( "" );
            methods[METHOD_bounds7] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("bounds", new Class[] {})); // NOI18N
            methods[METHOD_bounds7].setDisplayName ( "" );
            methods[METHOD_checkImage8] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class})); // NOI18N
            methods[METHOD_checkImage8].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect9] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_computeVisibleRect9].setDisplayName ( "" );
            methods[METHOD_contains10] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_contains10].setDisplayName ( "" );
            methods[METHOD_countComponents11] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("countComponents", new Class[] {})); // NOI18N
            methods[METHOD_countComponents11].setDisplayName ( "" );
            methods[METHOD_createImage12] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class})); // NOI18N
            methods[METHOD_createImage12].setDisplayName ( "" );
            methods[METHOD_createToolTip13] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("createToolTip", new Class[] {})); // NOI18N
            methods[METHOD_createToolTip13].setDisplayName ( "" );
            methods[METHOD_createVolatileImage14] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_createVolatileImage14].setDisplayName ( "" );
            methods[METHOD_deliverEvent15] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_deliverEvent15].setDisplayName ( "" );
            methods[METHOD_disable16] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("disable", new Class[] {})); // NOI18N
            methods[METHOD_disable16].setDisplayName ( "" );
            methods[METHOD_dispatchEvent17] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class})); // NOI18N
            methods[METHOD_dispatchEvent17].setDisplayName ( "" );
            methods[METHOD_doLayout18] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("doLayout", new Class[] {})); // NOI18N
            methods[METHOD_doLayout18].setDisplayName ( "" );
            methods[METHOD_enable19] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("enable", new Class[] {})); // NOI18N
            methods[METHOD_enable19].setDisplayName ( "" );
            methods[METHOD_enableInputMethods20] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE})); // NOI18N
            methods[METHOD_enableInputMethods20].setDisplayName ( "" );
            methods[METHOD_findComponentAt21] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_findComponentAt21].setDisplayName ( "" );
            methods[METHOD_firePropertyChange22] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Boolean.TYPE, Boolean.TYPE})); // NOI18N
            methods[METHOD_firePropertyChange22].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke23] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class})); // NOI18N
            methods[METHOD_getActionForKeyStroke23].setDisplayName ( "" );
            methods[METHOD_getBounds24] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_getBounds24].setDisplayName ( "" );
            methods[METHOD_getClientProperty25] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class})); // NOI18N
            methods[METHOD_getClientProperty25].setDisplayName ( "" );
            methods[METHOD_getComponentAt26] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_getComponentAt26].setDisplayName ( "" );
            methods[METHOD_getComponentZOrder27] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("getComponentZOrder", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_getComponentZOrder27].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke28] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class})); // NOI18N
            methods[METHOD_getConditionForKeyStroke28].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale29] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("getDefaultLocale", new Class[] {})); // NOI18N
            methods[METHOD_getDefaultLocale29].setDisplayName ( "" );
            methods[METHOD_getFontMetrics30] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class})); // NOI18N
            methods[METHOD_getFontMetrics30].setDisplayName ( "" );
            methods[METHOD_getInsets31] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("getInsets", new Class[] {java.awt.Insets.class})); // NOI18N
            methods[METHOD_getInsets31].setDisplayName ( "" );
            methods[METHOD_getListeners32] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("getListeners", new Class[] {java.lang.Class.class})); // NOI18N
            methods[METHOD_getListeners32].setDisplayName ( "" );
            methods[METHOD_getLocation33] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("getLocation", new Class[] {java.awt.Point.class})); // NOI18N
            methods[METHOD_getLocation33].setDisplayName ( "" );
            methods[METHOD_getMousePosition34] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("getMousePosition", new Class[] {Boolean.TYPE})); // NOI18N
            methods[METHOD_getMousePosition34].setDisplayName ( "" );
            methods[METHOD_getPopupLocation35] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("getPopupLocation", new Class[] {java.awt.event.MouseEvent.class})); // NOI18N
            methods[METHOD_getPopupLocation35].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners36] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class})); // NOI18N
            methods[METHOD_getPropertyChangeListeners36].setDisplayName ( "" );
            methods[METHOD_getSize37] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("getSize", new Class[] {java.awt.Dimension.class})); // NOI18N
            methods[METHOD_getSize37].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation38] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class})); // NOI18N
            methods[METHOD_getToolTipLocation38].setDisplayName ( "" );
            methods[METHOD_getToolTipText39] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class})); // NOI18N
            methods[METHOD_getToolTipText39].setDisplayName ( "" );
            methods[METHOD_gotFocus40] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_gotFocus40].setDisplayName ( "" );
            methods[METHOD_grabFocus41] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("grabFocus", new Class[] {})); // NOI18N
            methods[METHOD_grabFocus41].setDisplayName ( "" );
            methods[METHOD_handleEvent42] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("handleEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_handleEvent42].setDisplayName ( "" );
            methods[METHOD_hasFocus43] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("hasFocus", new Class[] {})); // NOI18N
            methods[METHOD_hasFocus43].setDisplayName ( "" );
            methods[METHOD_hide44] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("hide", new Class[] {})); // NOI18N
            methods[METHOD_hide44].setDisplayName ( "" );
            methods[METHOD_imageUpdate45] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_imageUpdate45].setDisplayName ( "" );
            methods[METHOD_insets46] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("insets", new Class[] {})); // NOI18N
            methods[METHOD_insets46].setDisplayName ( "" );
            methods[METHOD_inside47] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_inside47].setDisplayName ( "" );
            methods[METHOD_invalidate48] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("invalidate", new Class[] {})); // NOI18N
            methods[METHOD_invalidate48].setDisplayName ( "" );
            methods[METHOD_isAncestorOf49] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_isAncestorOf49].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot50] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class})); // NOI18N
            methods[METHOD_isFocusCycleRoot50].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent51] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_isLightweightComponent51].setDisplayName ( "" );
            methods[METHOD_keyDown52] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE})); // NOI18N
            methods[METHOD_keyDown52].setDisplayName ( "" );
            methods[METHOD_keyUp53] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE})); // NOI18N
            methods[METHOD_keyUp53].setDisplayName ( "" );
            methods[METHOD_layout54] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("layout", new Class[] {})); // NOI18N
            methods[METHOD_layout54].setDisplayName ( "" );
            methods[METHOD_list55] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE})); // NOI18N
            methods[METHOD_list55].setDisplayName ( "" );
            methods[METHOD_locate56] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_locate56].setDisplayName ( "" );
            methods[METHOD_location57] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("location", new Class[] {})); // NOI18N
            methods[METHOD_location57].setDisplayName ( "" );
            methods[METHOD_lostFocus58] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_lostFocus58].setDisplayName ( "" );
            methods[METHOD_minimumSize59] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("minimumSize", new Class[] {})); // NOI18N
            methods[METHOD_minimumSize59].setDisplayName ( "" );
            methods[METHOD_mouseDown60] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseDown60].setDisplayName ( "" );
            methods[METHOD_mouseDrag61] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseDrag61].setDisplayName ( "" );
            methods[METHOD_mouseEnter62] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseEnter62].setDisplayName ( "" );
            methods[METHOD_mouseExit63] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseExit63].setDisplayName ( "" );
            methods[METHOD_mouseMove64] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseMove64].setDisplayName ( "" );
            methods[METHOD_mouseUp65] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseUp65].setDisplayName ( "" );
            methods[METHOD_move66] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_move66].setDisplayName ( "" );
            methods[METHOD_nextFocus67] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("nextFocus", new Class[] {})); // NOI18N
            methods[METHOD_nextFocus67].setDisplayName ( "" );
            methods[METHOD_paint68] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("paint", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paint68].setDisplayName ( "" );
            methods[METHOD_paintAll69] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paintAll69].setDisplayName ( "" );
            methods[METHOD_paintComponents70] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paintComponents70].setDisplayName ( "" );
            methods[METHOD_paintImmediately71] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_paintImmediately71].setDisplayName ( "" );
            methods[METHOD_postEvent72] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("postEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_postEvent72].setDisplayName ( "" );
            methods[METHOD_preferredSize73] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("preferredSize", new Class[] {})); // NOI18N
            methods[METHOD_preferredSize73].setDisplayName ( "" );
            methods[METHOD_prepareImage74] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class})); // NOI18N
            methods[METHOD_prepareImage74].setDisplayName ( "" );
            methods[METHOD_print75] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("print", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_print75].setDisplayName ( "" );
            methods[METHOD_printAll76] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("printAll", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_printAll76].setDisplayName ( "" );
            methods[METHOD_printComponents77] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_printComponents77].setDisplayName ( "" );
            methods[METHOD_putClientProperty78] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_putClientProperty78].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction79] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE})); // NOI18N
            methods[METHOD_registerKeyboardAction79].setDisplayName ( "" );
            methods[METHOD_remove80] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("remove", new Class[] {Integer.TYPE})); // NOI18N
            methods[METHOD_remove80].setDisplayName ( "" );
            methods[METHOD_removeAll81] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("removeAll", new Class[] {})); // NOI18N
            methods[METHOD_removeAll81].setDisplayName ( "" );
            methods[METHOD_removeJBIListPanelListener82] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("removeJBIListPanelListener", new Class[] {kpi.beans.JBIListPanelListener.class})); // NOI18N
            methods[METHOD_removeJBIListPanelListener82].setDisplayName ( "" );
            methods[METHOD_removeNotify83] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("removeNotify", new Class[] {})); // NOI18N
            methods[METHOD_removeNotify83].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener84] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class})); // NOI18N
            methods[METHOD_removePropertyChangeListener84].setDisplayName ( "" );
            methods[METHOD_repaint85] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_repaint85].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus86] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("requestDefaultFocus", new Class[] {})); // NOI18N
            methods[METHOD_requestDefaultFocus86].setDisplayName ( "" );
            methods[METHOD_requestFocus87] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("requestFocus", new Class[] {})); // NOI18N
            methods[METHOD_requestFocus87].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow88] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("requestFocusInWindow", new Class[] {})); // NOI18N
            methods[METHOD_requestFocusInWindow88].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions89] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("resetKeyboardActions", new Class[] {})); // NOI18N
            methods[METHOD_resetKeyboardActions89].setDisplayName ( "" );
            methods[METHOD_reshape90] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_reshape90].setDisplayName ( "" );
            methods[METHOD_resize91] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_resize91].setDisplayName ( "" );
            methods[METHOD_revalidate92] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("revalidate", new Class[] {})); // NOI18N
            methods[METHOD_revalidate92].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible93] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_scrollRectToVisible93].setDisplayName ( "" );
            methods[METHOD_setBounds94] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_setBounds94].setDisplayName ( "" );
            methods[METHOD_setComponentZOrder95] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("setComponentZOrder", new Class[] {java.awt.Component.class, Integer.TYPE})); // NOI18N
            methods[METHOD_setComponentZOrder95].setDisplayName ( "" );
            methods[METHOD_setDataSource96] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("setDataSource", new Class[] {kpi.xml.BIXMLVector.class, java.lang.String.class, java.lang.String.class, kpi.swing.KpiDefaultFrameBehavior.class})); // NOI18N
            methods[METHOD_setDataSource96].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale97] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class})); // NOI18N
            methods[METHOD_setDefaultLocale97].setDisplayName ( "" );
            methods[METHOD_show98] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("show", new Class[] {})); // NOI18N
            methods[METHOD_show98].setDisplayName ( "" );
            methods[METHOD_size99] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("size", new Class[] {})); // NOI18N
            methods[METHOD_size99].setDisplayName ( "" );
            methods[METHOD_toString100] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("toString", new Class[] {})); // NOI18N
            methods[METHOD_toString100].setDisplayName ( "" );
            methods[METHOD_transferFocus101] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("transferFocus", new Class[] {})); // NOI18N
            methods[METHOD_transferFocus101].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward102] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("transferFocusBackward", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusBackward102].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle103] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("transferFocusDownCycle", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusDownCycle103].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle104] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("transferFocusUpCycle", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusUpCycle104].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction105] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class})); // NOI18N
            methods[METHOD_unregisterKeyboardAction105].setDisplayName ( "" );
            methods[METHOD_update106] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("update", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_update106].setDisplayName ( "" );
            methods[METHOD_updateUI107] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("updateUI", new Class[] {})); // NOI18N
            methods[METHOD_updateUI107].setDisplayName ( "" );
            methods[METHOD_validate108] = new MethodDescriptor ( kpi.beans.JBIMessagePanel.class.getMethod("validate", new Class[] {})); // NOI18N
            methods[METHOD_validate108].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
		
		// Here you can add code for customizing the methods array.
		
        return methods;     }//GEN-LAST:Methods
	
	
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
	
	
//GEN-FIRST:Superclass
	
	// Here you can add code for customizing the Superclass BeanInfo.
	
//GEN-LAST:Superclass
	
	/**
	 * Gets the bean's <code>BeanDescriptor</code>s.
	 *
	 * @return BeanDescriptor describing the editable
	 * properties of this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 */
	public BeanDescriptor getBeanDescriptor() {
		return getBdescriptor();
	}
	
	/**
	 * Gets the bean's <code>PropertyDescriptor</code>s.
	 *
	 * @return An array of PropertyDescriptors describing the editable
	 * properties supported by this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 * <p>
	 * If a property is indexed, then its entry in the result array will
	 * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
	 * A client of getPropertyDescriptors can use "instanceof" to check
	 * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
	 */
	public PropertyDescriptor[] getPropertyDescriptors() {
		return getPdescriptor();
	}
	
	/**
	 * Gets the bean's <code>EventSetDescriptor</code>s.
	 *
	 * @return  An array of EventSetDescriptors describing the kinds of
	 * events fired by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public EventSetDescriptor[] getEventSetDescriptors() {
		return getEdescriptor();
	}
	
	/**
	 * Gets the bean's <code>MethodDescriptor</code>s.
	 *
	 * @return  An array of MethodDescriptors describing the methods
	 * implemented by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public MethodDescriptor[] getMethodDescriptors() {
		return getMdescriptor();
	}
	
	/**
	 * A bean may have a "default" property that is the property that will
	 * mostly commonly be initially chosen for update by human's who are
	 * customizing the bean.
	 * @return  Index of default property in the PropertyDescriptor array
	 * 		returned by getPropertyDescriptors.
	 * <P>	Returns -1 if there is no default property.
	 */
	public int getDefaultPropertyIndex() {
		return defaultPropertyIndex;
	}
	
	/**
	 * A bean may have a "default" event that is the event that will
	 * mostly commonly be used by human's when using the bean.
	 * @return Index of default event in the EventSetDescriptor array
	 *		returned by getEventSetDescriptors.
	 * <P>	Returns -1 if there is no default event.
	 */
	public int getDefaultEventIndex() {
		return defaultEventIndex;
	}
}

