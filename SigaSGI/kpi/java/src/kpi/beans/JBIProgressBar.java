/*
 * JBIProgressBar.java
 *
 * Created on 28 de Abril de 2006, 11:55
 */
package kpi.beans;

import javax.swing.SwingWorker;
import kpi.core.KpiStaticReferences;

/**
 *
 * 
 * @author Alexandre Alves da Silva
 *
 * @Obs: Para melhor aproveitamento do uso desta classe � recomendavel que
 * a classe que fara uso seja herdada de KpiInternalFrame. Com isto � possivel fazer por exemplo
 * o controle do fechamento do formul�rio.
 *
 * @see Veja kpi.beans.JBIProgressBarListener para saber como acompanhar o processamento da tarefa.
 *
 */
public class JBIProgressBar extends javax.swing.JPanel implements java.beans.Customizer {

    static final public int PROGRESS_BAR_ERROR = -1;
    static final public int PROGRESS_BAR_OK = 0;
    static final public int PROGRESS_BAR_END = 1;
    private Object bean;
    private kpi.net.BIHttpClient httpClient = null;
    private boolean isConnect = false,
            endProgress = false;
    private int status = PROGRESS_BAR_OK,
            tryConnect = 0;
    private String jobName = "",
            url = "";
    private java.util.Vector vctProgressBarListeners;
    public kpi.xml.BIXMLRecord record = null;

    /** Creates new customizer JBIProgressBar */
    public JBIProgressBar() {
        initComponents();
        vctProgressBarListeners = new java.util.Vector();
    }

    @Override
    public void setObject(Object bean) {
        this.bean = bean;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the FormEditor.
     */
    private void initComponents() {//GEN-BEGIN:initComponents

        toolProgressbar = new javax.swing.JToolBar();
        pnlBarra = new javax.swing.JPanel();
        barStatus = new javax.swing.JProgressBar();
        lblMessageTop = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(200, 34));
        setLayout(new java.awt.BorderLayout());

        toolProgressbar.setFloatable(false);
        toolProgressbar.setPreferredSize(new java.awt.Dimension(200, 34));

        pnlBarra.setPreferredSize(new java.awt.Dimension(200, 34));
        pnlBarra.setLayout(new java.awt.BorderLayout());

        barStatus.setStringPainted(true);
        pnlBarra.add(barStatus, java.awt.BorderLayout.CENTER);

        lblMessageTop.setText("Totvs SGI");
        pnlBarra.add(lblMessageTop, java.awt.BorderLayout.NORTH);

        toolProgressbar.add(pnlBarra);

        add(toolProgressbar, java.awt.BorderLayout.CENTER);
    }//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar barStatus;
    private javax.swing.JLabel lblMessageTop;
    private javax.swing.JPanel pnlBarra;
    private javax.swing.JToolBar toolProgressbar;
    // End of variables declaration//GEN-END:variables

    public void startProgressBar() {
        onStartBarListener();

        final SwingWorker worker = new SwingWorker() {

            @Override
            protected Object doInBackground() throws Exception {

                //Tempo de espera para o processamento do Protheus.
                int timeToRequest = 3000;

                startConnection();

                try {
                    Thread.sleep(timeToRequest);
                } catch (InterruptedException x) {
                    Thread.currentThread().interrupt();
                }

                while (!endProgress) {

                    if (tryConnect < 5) {
                        getXMLData();

                        if (record != null) {

                            barStatus.setValue(record.getInt("PERCENT"));
                            getTopMessage().setText(record.getString("MESSAGE"));
                            int status = record.getInt("STATUS");
                            if (status != PROGRESS_BAR_OK) {
                                if (status == PROGRESS_BAR_ERROR) {
                                    onErrorBarListener();
                                }
                                Thread.currentThread().interrupt();
                                break;
                            }

                            timeToRequest = 2000;
                            tryConnect = 0;

                        } else {
                            tryConnect++;
                            timeToRequest = 3000;
                        }

                        try {
                            Thread.sleep(timeToRequest);
                        } catch (InterruptedException x) {
                            onErrorBarListener();
                            Thread.currentThread().interrupt();
                        }

                    } else {
                        onErrorBarListener();
                        Thread.currentThread().interrupt();
                        getTopMessage().setText("N�mero de tentativas de conex�o excedido.");
                        break;
                    }
                }
                return null;
            }

            @Override
            protected void done() {
                onFinishedBarListener();
            }
        };

        worker.execute();
    }

    private void startConnection() {
        kpi.core.KpiDataController dataController = kpi.core.KpiStaticReferences.getKpiDataController();
        kpi.core.KpiCommunicationController controller = dataController.getKpiCommunicationController();
        httpClient = controller.getHttpClient();

        if (KpiStaticReferences.getKpiExecutionType() == KpiStaticReferences.KPI_APLICATION) {
            url = KpiStaticReferences.getKpiApplication().getCodeBase().toString().concat("thread/".concat(jobName));
        } else {
            url = KpiStaticReferences.getKpiApplet().getCodeBase().toString().concat("thread/".concat(jobName));
        }

        barStatus.setValue(0);
    }

    private void getXMLData() {
        kpi.core.BIResponse response = new kpi.core.BIResponse();
        String fileContent = httpClient.doGet(url);

        if (fileContent.length() > 0) {
            response.processResponse(fileContent);
            setRecord(response.getReturnedRecord());
        } else {
            setRecord(null);
        }

    }

    public javax.swing.JLabel getTopMessage() {
        return lblMessageTop;
    }

    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    private void setRecord(kpi.xml.BIXMLRecord rec) {
        this.record = rec;
    }

    private void onStartBarListener() {
        for (int j = 0; j < vctProgressBarListeners.size(); j++) {
            ((kpi.beans.JBIProgressBarListener) vctProgressBarListeners.get(j)).onStartBar(new java.awt.event.ComponentEvent(this, 0));
        }
    }

    private void onFinishedBarListener() {
        for (int j = 0; j < vctProgressBarListeners.size(); j++) {
            ((kpi.beans.JBIProgressBarListener) vctProgressBarListeners.get(j)).onFinishBar(new java.awt.event.ComponentEvent(this, 1));
        }
    }

    private void onErrorBarListener() {
        for (int j = 0; j < vctProgressBarListeners.size(); j++) {
            ((kpi.beans.JBIProgressBarListener) vctProgressBarListeners.get(j)).onErrorBar(new java.awt.event.ComponentEvent(this, 2));
        }
    }

    private void onCancelBarListener() {
        for (int j = 0; j < vctProgressBarListeners.size(); j++) {
            ((kpi.beans.JBIProgressBarListener) vctProgressBarListeners.get(j)).onCancelBar(new java.awt.event.ComponentEvent(this, 3));
        }
    }

    public int getStatus() {
        return status;
    }

    /*
     * Seta o nome do job.
     */
    public void setJobName(String name) {
        jobName = name.concat(".xml");
    }

    public void addJBIProgressBarListener(kpi.beans.JBIProgressBarListener listener) {
        vctProgressBarListeners.add(listener);
    }

    public void removeJBIProgressBarListener(kpi.beans.JBIProgressBarListener listener) {
        vctProgressBarListeners.remove(listener);
    }
}
