/*
 * JBIProgressBarListener.java
 *
 * Created on 8 de Maio de 2006, 16:32
 */
package kpi.beans;

import java.util.EventListener;

/**
 *
 * @author  Alexandre Alves da Silva
 */
public interface JBIProgressBarListener extends EventListener{
    
	/**
     * Invoked when the component has beeen started.
     */
    public void onStartBar(java.awt.event.ComponentEvent componentEvent);

	/**
     * Invoked when the component has beeen finished.
     */
	public void onFinishBar(java.awt.event.ComponentEvent componentEvent);

	/**
     * Invoked when ocurre an error.
     */
	public void onErrorBar(java.awt.event.ComponentEvent componentEvent);
	
	/**
     * Invoked when ocurre cancel.
     */
	public void onCancelBar(java.awt.event.ComponentEvent componentEvent);
	
}