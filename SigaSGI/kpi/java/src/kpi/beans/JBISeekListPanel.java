/*
 * JBIListPanel.java
 *
 * Created on July 4, 2003, 4:02 PM
 */
package kpi.beans;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiDefaultFrameFunctions;
import kpi.xml.BIXMLAttributes;
import kpi.xml.BIXMLRecord;

/**
 *
 * @author  siga1996
 */
public class JBISeekListPanel extends javax.swing.JPanel {

    private String parentId = null;
    private String contextId = null;
    private kpi.swing.KpiDefaultFrameBehavior frmParent = null;
    private Locale defaultLocale = null;
    private Vector vctJBIListPanelListener = null;
    private Vector vctListPnlBtnListerner = null;
    private static final int BTN_NEW = 1;
    private static final int BTN_VIEW = 2;
    private static final int BTN_RELOAD = 3;
    private static final int BTN_USER = 4;
    public JBIXMLTable xmlTable = new JBIXMLTable();

    /** Creates new form JBIListPanel */
    public JBISeekListPanel() {
        
        defaultLocale = kpi.core.KpiStaticReferences.getKpiDefaultLocale();
	if (defaultLocale == null) {
	    defaultLocale = new java.util.Locale("pt_BR");
	}

	initComponents();
        
        setFiltroVisible(false);
        
	pnlTable.add(xmlTable);
	prepareSearchField(false);

	xmlTable.getTable().addKeyListener(new java.awt.event.KeyAdapter() {

	    @Override
	    public void keyReleased(java.awt.event.KeyEvent evt) {
		xmlTableKeyReleased(evt);
	    }
	});

	xmlTable.getTable().addMouseListener(new java.awt.event.MouseAdapter() {

	    @Override
	    public void mouseClicked(java.awt.event.MouseEvent evt) {
		xmlTableMouseClicked(evt);
	    }

	    @Override
	    public void mousePressed(java.awt.event.MouseEvent evt) {
		xmlTableMousePressed(evt);
	    }
	});
	vctJBIListPanelListener = new Vector();
	vctListPnlBtnListerner = new Vector();
    }

    /**
     * Prepar os campos de pesquisa.
     * @param isVisibled
     * @return void
     */
    public void prepareSearchField(boolean isVisibled) {
	pnlPesquisa.setVisible(isVisibled);

	if (isVisibled) {
	    jTxtProcurar.setText(ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("JBIListPanel_00004" /*Digite sua pesquisa*/));
	    jTxtProcurar.setForeground(Color.LIGHT_GRAY);
	    jTxtProcurar.setBackground(Color.WHITE);
	}
    }

    /**
     * Inicia campos de pesquisa com valor padr�o.
     * @return void
     */
    private void initSearchField() {
	jTxtProcurar.setText("");
	jTxtProcurar.setForeground(Color.BLACK);
    }

    /**
     * Define as caracter�sticas do campo de pesquisa de acordo com o resultado da pesquisa.
     * @param isFound
     * @return void
     */
    private void setSearchField(boolean isFound) {

	if (isFound) {
	    jTxtProcurar.setBackground(Color.WHITE);
	} else {
	    jTxtProcurar.setBackground(new Color(253, 117, 127)/*Red*/);
	}
    }

    public void setDataSource(kpi.xml.BIXMLVector dataSource, String parent, String context, KpiDefaultFrameBehavior frameAux) {
	parentId = String.valueOf(parent);
	contextId = String.valueOf(context);
	xmlTable.setDataSource(dataSource);
	frmParent = frameAux;

	populateComboProcurar();
    }

    public void setNovoEnabled(boolean novoEnabled) {
	btnNew.setEnabled(novoEnabled);
    }

    public boolean getNovoEnabled() {
	return btnNew.isEnabled();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        pnlTools = new javax.swing.JPanel();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnNew = new javax.swing.JButton();
        btnView = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnUser1 = new javax.swing.JButton();
        btnAjuda = new javax.swing.JButton();
        pnlPesquisa = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        jLblProcurar = new javax.swing.JLabel();
        jCmbProcurar = new javax.swing.JComboBox();
        jTxtProcurar = new javax.swing.JTextField();
        pnlFiltro = new javax.swing.JPanel();
        jLblFiltro = new javax.swing.JLabel();
        jCmbFiltro = new javax.swing.JComboBox();
        pnlLast = new javax.swing.JPanel();
        pnlList = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        pnlTopRecord = new javax.swing.JPanel();
        pnlTable = new javax.swing.JPanel();

        setMinimumSize(new java.awt.Dimension(500, 69));
        setLayout(new java.awt.BorderLayout());

        pnlTools.setPreferredSize(new java.awt.Dimension(10, 27));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlOperation.setMinimumSize(new java.awt.Dimension(270, 19));
        pnlOperation.setPreferredSize(new java.awt.Dimension(330, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setMaximumSize(new java.awt.Dimension(330, 43));
        tbInsertion.setMinimumSize(new java.awt.Dimension(330, 43));
        tbInsertion.setPreferredSize(new java.awt.Dimension(330, 43));

        btnNew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_novo.gif"))); // NOI18N
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnNew.setText(bundle.getString("JBIListPanel_00001")); // NOI18N
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });
        tbInsertion.add(btnNew);

        btnView.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_visualizar.gif"))); // NOI18N
        btnView.setText(bundle.getString("JBIListPanel_00002")); // NOI18N
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });
        tbInsertion.add(btnView);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("JBIListPanel_00003")); // NOI18N
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnUser1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_duplicador.gif"))); // NOI18N
        btnUser1.setText("Opcao");
        btnUser1.setMaximumSize(new java.awt.Dimension(80, 41));
        btnUser1.setMinimumSize(new java.awt.Dimension(70, 41));
        btnUser1.setPreferredSize(new java.awt.Dimension(80, 41));
        btnUser1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnUser1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUser1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnUser1);
        btnUser1.setVisible(false);

        btnAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda.setText(bundle.getString("KpiGraphIndicador_00011")); // NOI18N
        btnAjuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlPesquisa.setLayout(new java.awt.GridBagLayout());

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 4);
        pnlPesquisa.add(jSeparator1, gridBagConstraints);

        jLblProcurar.setText(bundle.getString("JBISeekListPanel_00001")); // NOI18N
        jLblProcurar.setIconTextGap(0);
        jLblProcurar.setMaximumSize(new java.awt.Dimension(50, 16));
        jLblProcurar.setMinimumSize(new java.awt.Dimension(50, 16));
        jLblProcurar.setPreferredSize(new java.awt.Dimension(44, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        pnlPesquisa.add(jLblProcurar, gridBagConstraints);

        jCmbProcurar.setPreferredSize(new java.awt.Dimension(100, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        pnlPesquisa.add(jCmbProcurar, gridBagConstraints);

        jTxtProcurar.setMaximumSize(new java.awt.Dimension(10, 10));
        jTxtProcurar.setPreferredSize(new java.awt.Dimension(100, 22));
        jTxtProcurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtProcurarActionPerformed(evt);
            }
        });
        jTxtProcurar.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTxtProcurarFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTxtProcurarFocusLost(evt);
            }
        });
        jTxtProcurar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxtProcurarKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        pnlPesquisa.add(jTxtProcurar, gridBagConstraints);

        jLblFiltro.setText("Filtro");
        pnlFiltro.add(jLblFiltro);

        pnlFiltro.add(jCmbFiltro);

        pnlPesquisa.add(pnlFiltro, new java.awt.GridBagConstraints());

        pnlLast.setMaximumSize(new java.awt.Dimension(0, 0));
        pnlLast.setMinimumSize(new java.awt.Dimension(0, 0));
        pnlLast.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlPesquisa.add(pnlLast, new java.awt.GridBagConstraints());

        pnlTools.add(pnlPesquisa, java.awt.BorderLayout.CENTER);

        add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlList.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlList.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setMaximumSize(new java.awt.Dimension(10, 10));
        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlList.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlList.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlList.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        pnlTable.setLayout(new java.awt.BorderLayout());
        pnlList.add(pnlTable, java.awt.BorderLayout.CENTER);

        add(pnlList, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
	    frmParent.loadHelp(xmlTable.getType());

	    Boolean isVisible = pnlFiltro.isVisible(); 
	    prepareSearchField(isVisible); 
	}//GEN-LAST:event_btnAjudaActionPerformed

    private void xmlTableKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_xmlTableKeyReleased
	for (int j = 0; j < vctJBIListPanelListener.size(); j++) {
	    ((JBIListPanelListener) vctJBIListPanelListener.get(j)).keyPressed(new ComponentEvent(this, evt.getID()));
	}
	setBtnOptions();
    }//GEN-LAST:event_xmlTableKeyReleased

	private void xmlTableMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_xmlTableMousePressed
	    for (int j = 0; j < vctJBIListPanelListener.size(); j++) {
		((JBIListPanelListener) vctJBIListPanelListener.get(j)).mousePressed(new ComponentEvent(this, evt.getID()));
	    }
	    setBtnOptions();
	}//GEN-LAST:event_xmlTableMousePressed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
	    frmParent.loadRecord();
            Boolean isVisible = pnlFiltro.isVisible(); 
	    prepareSearchField(isVisible);            
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed
	    int i = xmlTable.getTable().getSelectedRow();

	    if (xmlTable.getTable().getSelectedRow() != -1) {
		BITableModel tableModel = (BITableModel) xmlTable.getModel();

		if (!tableModel.getRecordID(xmlTable.getTable().getSelectedRow()).trim().equals("-1")) {
		    KpiDefaultFrameFunctions frmNew =
			    kpi.core.KpiStaticReferences.getKpiFormController().getForm(
			    tableModel.getRecordType(xmlTable.getTable().getSelectedRow()),
			    tableModel.getRecordID(xmlTable.getTable().getSelectedRow()),
			    tableModel.getName(xmlTable.getTable().getSelectedRow()));
		    doCommand(BTN_VIEW, frmNew, evt);
		}
	    }

	    Boolean isVisible = pnlFiltro.isVisible(); 
	    prepareSearchField(isVisible); 

	}//GEN-LAST:event_btnViewActionPerformed

	private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
	    KpiDefaultFrameFunctions frmNew = null;
	    if ((xmlTable.getType() != null) && (!"".equals(xmlTable.getType()))) {
		frmNew = kpi.core.KpiStaticReferences.getKpiFormController().newDetailForm(xmlTable.getType(), parentId, contextId);
		doCommand(BTN_NEW, frmNew, evt);
	    }
	    Boolean isVisible = pnlFiltro.isVisible(); 
	    prepareSearchField(isVisible); 
	}//GEN-LAST:event_btnNewActionPerformed

    private void jTxtProcurarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtProcurarKeyReleased
	boolean found = xmlTable.seekInColumn(jCmbProcurar.getSelectedIndex(), jTxtProcurar.getText());

	setSearchField(found);
    }//GEN-LAST:event_jTxtProcurarKeyReleased

    private void jTxtProcurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtProcurarActionPerformed
    }//GEN-LAST:event_jTxtProcurarActionPerformed

    private void jTxtProcurarFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtProcurarFocusGained
	initSearchField();
    }//GEN-LAST:event_jTxtProcurarFocusGained

    private void jTxtProcurarFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtProcurarFocusLost
    }//GEN-LAST:event_jTxtProcurarFocusLost

private void btnUser1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUser1ActionPerformed
    for (Iterator<JBIListBtnPnlListener> it = vctListPnlBtnListerner.iterator(); it.hasNext();) {
	JBIListBtnPnlListener event = it.next();
	event.doBtnUser1ActionPerformed(evt);
    }
}//GEN-LAST:event_btnUser1ActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnAjuda;
    public javax.swing.JButton btnNew;
    public javax.swing.JButton btnReload;
    public javax.swing.JButton btnUser1;
    public javax.swing.JButton btnView;
    public javax.swing.JComboBox jCmbFiltro;
    private javax.swing.JComboBox jCmbProcurar;
    private javax.swing.JLabel jLblFiltro;
    private javax.swing.JLabel jLblProcurar;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField jTxtProcurar;
    public javax.swing.JPanel pnlBottomRecord;
    public javax.swing.JPanel pnlFiltro;
    private javax.swing.JPanel pnlLast;
    public javax.swing.JPanel pnlLeftRecord;
    public javax.swing.JPanel pnlList;
    public javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlPesquisa;
    public javax.swing.JPanel pnlRightRecord;
    public javax.swing.JPanel pnlTable;
    public javax.swing.JPanel pnlTools;
    public javax.swing.JPanel pnlTopRecord;
    public javax.swing.JToolBar tbInsertion;
    // End of variables declaration//GEN-END:variables

    private void xmlTableMouseClicked(java.awt.event.MouseEvent evt) {
	if (btnView.isEnabled() && evt.getClickCount() == 2) {
	    BITableModel tableModel = (BITableModel) xmlTable.getModel();

	    if (!tableModel.getRecordID(xmlTable.getTable().getSelectedRow()).trim().equals("-1")) {
		KpiDefaultFrameFunctions frmNew =
			kpi.core.KpiStaticReferences.getKpiFormController().getForm(
			tableModel.getRecordType(xmlTable.getTable().getSelectedRow()),
			tableModel.getRecordID(xmlTable.getTable().getSelectedRow()),
			tableModel.getName(xmlTable.getTable().getSelectedRow()));
		doCommand(frmNew, evt);
	    }
	}

	if (xmlTable.getTable().getSelectedRow() != -1) {
	    prepareSearchField(true);
	}
    }

    public void addJBIListPanelListener(kpi.beans.JBIListPanelListener listener) {
	vctJBIListPanelListener.add(listener);
    }

    public void removeJBIListPanelListener(kpi.beans.JBIListPanelListener listener) {
	vctJBIListPanelListener.remove(listener);
    }

    public void addListPnlBtnListerner(kpi.beans.JBIListBtnPnlListener listener) {
	vctListPnlBtnListerner.add(listener);
    }

    public void removeListPnlBtnListerner(kpi.beans.JBIListBtnPnlListener listener) {
	vctListPnlBtnListerner.remove(listener);
    }

    public void selectLine(int lineIni, int lineEnd) {
	BITableModel tableModel = (BITableModel) xmlTable.getModel();
	lineIni = tableModel.getOriginalRow(lineIni);
	lineEnd = tableModel.getOriginalRow(lineEnd);
	xmlTable.getTable().setRowSelectionInterval(lineIni, lineEnd);
    }

    private void populateComboProcurar() {
	BITableModel tableModel = (BITableModel) xmlTable.getModel();
	int nQtdCol = tableModel.getColumnCount();
	jCmbProcurar.removeAllItems();
	for (int nCol = 0; nCol
		< nQtdCol; nCol++) {
	    jCmbProcurar.addItem(tableModel.getColumnName(nCol));
	}
    }

    public void setContextId(String cContext) {
	this.contextId = cContext;
    }

    /**
     * Executa o comando retornado pela interface.
     * @param frmNew Formulario aberto.
     */
    private void doCommand(int btnType, KpiDefaultFrameFunctions frmNew, ActionEvent btnEvt) {
	doCommand(btnType, frmNew, btnEvt, null);
    }

    private void doCommand(KpiDefaultFrameFunctions frmNew, MouseEvent mouseEvt) {
	doCommand(0, frmNew, null, mouseEvt);
    }

    /**
     * Executa o comando retornado pela interface.
     * @param frmNew Formulario aberto.
     */
    private void doCommand(int btnType, KpiDefaultFrameFunctions frmNew, ActionEvent btnEvt, MouseEvent mouseEvt) {
	if (frmNew != null) {
	    for (Iterator<JBIListBtnPnlListener> it = vctListPnlBtnListerner.iterator(); it.hasNext();) {
		JBIListBtnPnlListener event = it.next();

		BIXMLRecord recCommand = null;
		if (btnEvt != null) {
		    switch (btnType) {
			case BTN_NEW:
			    recCommand = event.getCmdOnNewPressed(btnEvt);
			    break;
			case BTN_VIEW:
			    recCommand = event.getCmdOnBtnViewPressed(btnEvt);
			    break;
			case BTN_USER:
			    recCommand = event.getCmdOnBtnUser1Pressed(btnEvt);
			    break;
			case BTN_RELOAD:
			    recCommand = event.getCmdOnBtnReloadPressed(btnEvt);
			    break;
		    }
		} else if (mouseEvt != null) {
		    recCommand = event.xmlTableMouseClicked(mouseEvt);
		}

		if (recCommand != null && recCommand.getTagName().equals("COMMANDS")) {
		    BIXMLAttributes attCmd = recCommand.getAttributes();
		    if (attCmd.contains("SET_RECORD_ON_NEW_FORM")) {
			frmNew.setRecord(recCommand);
		    }
		}
	    }
	}
    }

    /**
     * Seta as op��es dos menus de acordo com a sele��o do usuario.
     * @param tableModel Model da tabela selecionada.
     */
    private void setBtnOptions() {
	BITableModel tableModel = (BITableModel) xmlTable.getModel();
	//Selecionando a linha
	BIXMLRecord recSel = tableModel.getSelectedRecord(xmlTable.getTable().getSelectedRow());

	if (recSel != null) {
	    BIXMLAttributes attLine = recSel.getAttributes();

	    //Permissoes para o botao visualizar
	    if (attLine.contains("IS_LINE_VIEW_ENABLED")) {
		btnView.setEnabled(attLine.getBoolean("IS_LINE_VIEW_ENABLED"));
	    }

	    //Permissoes para o novo
	    if (attLine.contains("IS_LINE_NEW_ENABLED") ) {
		btnNew.setEnabled(attLine.getBoolean("IS_LINE_NEW_ENABLED"));
	    }

	    //Permissoes para o botao de usuario 1.
	    if (attLine.contains("IS_LINE_USR1_ENABLED") ) {
		btnUser1.setEnabled(attLine.getBoolean("IS_LINE_USR1_ENABLED"));
	    }
	}
    }

    public void setFiltroVisible(boolean visible) {
        prepareSearchField(visible);
        pnlFiltro.setVisible(visible);
    }

    public void prepareFiltro(String status, String[] statusLista) {
       jLblFiltro.setText(status);
       jCmbFiltro.setModel(new DefaultComboBoxModel(statusLista));
    }

}