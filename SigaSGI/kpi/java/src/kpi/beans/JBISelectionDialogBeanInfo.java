/*
 * JBISelectionDialogBeanInfo.java
 *
 * Created on 4 de Janeiro de 2007, 10:51
 */

package kpi.beans;

import java.beans.*;

/**
 * @author lucio.pelinson
 */
public class JBISelectionDialogBeanInfo extends SimpleBeanInfo {
	
    // Bean descriptor//GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( kpi.beans.JBISelectionDialog.class , null ); // NOI18N//GEN-HEADEREND:BeanDescriptor
		
		// Here you can add code for customizing the BeanDescriptor.
		
        return beanDescriptor;     }//GEN-LAST:BeanDescriptor
	
	
    // Property identifiers//GEN-FIRST:Properties
    private static final int PROPERTY_accessibleContext = 0;
    private static final int PROPERTY_active = 1;
    private static final int PROPERTY_alignmentX = 2;
    private static final int PROPERTY_alignmentY = 3;
    private static final int PROPERTY_alwaysOnTop = 4;
    private static final int PROPERTY_background = 5;
    private static final int PROPERTY_backgroundSet = 6;
    private static final int PROPERTY_bounds = 7;
    private static final int PROPERTY_bufferStrategy = 8;
    private static final int PROPERTY_colorModel = 9;
    private static final int PROPERTY_component = 10;
    private static final int PROPERTY_componentCount = 11;
    private static final int PROPERTY_componentListeners = 12;
    private static final int PROPERTY_componentOrientation = 13;
    private static final int PROPERTY_components = 14;
    private static final int PROPERTY_containerListeners = 15;
    private static final int PROPERTY_contentPane = 16;
    private static final int PROPERTY_cursor = 17;
    private static final int PROPERTY_cursorSet = 18;
    private static final int PROPERTY_defaultCloseOperation = 19;
    private static final int PROPERTY_displayable = 20;
    private static final int PROPERTY_doubleBuffered = 21;
    private static final int PROPERTY_dropTarget = 22;
    private static final int PROPERTY_enabled = 23;
    private static final int PROPERTY_focusable = 24;
    private static final int PROPERTY_focusableWindow = 25;
    private static final int PROPERTY_focusableWindowState = 26;
    private static final int PROPERTY_focusCycleRoot = 27;
    private static final int PROPERTY_focusCycleRootAncestor = 28;
    private static final int PROPERTY_focused = 29;
    private static final int PROPERTY_focusListeners = 30;
    private static final int PROPERTY_focusOwner = 31;
    private static final int PROPERTY_focusTraversable = 32;
    private static final int PROPERTY_focusTraversalKeys = 33;
    private static final int PROPERTY_focusTraversalKeysEnabled = 34;
    private static final int PROPERTY_focusTraversalPolicy = 35;
    private static final int PROPERTY_focusTraversalPolicyProvider = 36;
    private static final int PROPERTY_focusTraversalPolicySet = 37;
    private static final int PROPERTY_font = 38;
    private static final int PROPERTY_fontSet = 39;
    private static final int PROPERTY_foreground = 40;
    private static final int PROPERTY_foregroundSet = 41;
    private static final int PROPERTY_glassPane = 42;
    private static final int PROPERTY_graphics = 43;
    private static final int PROPERTY_graphicsConfiguration = 44;
    private static final int PROPERTY_height = 45;
    private static final int PROPERTY_hierarchyBoundsListeners = 46;
    private static final int PROPERTY_hierarchyListeners = 47;
    private static final int PROPERTY_ignoreRepaint = 48;
    private static final int PROPERTY_inputContext = 49;
    private static final int PROPERTY_inputMethodListeners = 50;
    private static final int PROPERTY_inputMethodRequests = 51;
    private static final int PROPERTY_insets = 52;
    private static final int PROPERTY_JMenuBar = 53;
    private static final int PROPERTY_keyListeners = 54;
    private static final int PROPERTY_layeredPane = 55;
    private static final int PROPERTY_layout = 56;
    private static final int PROPERTY_lightweight = 57;
    private static final int PROPERTY_locale = 58;
    private static final int PROPERTY_location = 59;
    private static final int PROPERTY_locationByPlatform = 60;
    private static final int PROPERTY_locationOnScreen = 61;
    private static final int PROPERTY_locationRelativeTo = 62;
    private static final int PROPERTY_maximumSize = 63;
    private static final int PROPERTY_maximumSizeSet = 64;
    private static final int PROPERTY_minimumSize = 65;
    private static final int PROPERTY_minimumSizeSet = 66;
    private static final int PROPERTY_modal = 67;
    private static final int PROPERTY_mostRecentFocusOwner = 68;
    private static final int PROPERTY_mouseListeners = 69;
    private static final int PROPERTY_mouseMotionListeners = 70;
    private static final int PROPERTY_mousePosition = 71;
    private static final int PROPERTY_mouseWheelListeners = 72;
    private static final int PROPERTY_name = 73;
    private static final int PROPERTY_opaque = 74;
    private static final int PROPERTY_ownedWindows = 75;
    private static final int PROPERTY_owner = 76;
    private static final int PROPERTY_parent = 77;
    private static final int PROPERTY_peer = 78;
    private static final int PROPERTY_preferredSize = 79;
    private static final int PROPERTY_preferredSizeSet = 80;
    private static final int PROPERTY_propertyChangeListeners = 81;
    private static final int PROPERTY_resizable = 82;
    private static final int PROPERTY_rootPane = 83;
    private static final int PROPERTY_showing = 84;
    private static final int PROPERTY_size = 85;
    private static final int PROPERTY_tableCtrl = 86;
    private static final int PROPERTY_title = 87;
    private static final int PROPERTY_toolkit = 88;
    private static final int PROPERTY_treeLock = 89;
    private static final int PROPERTY_undecorated = 90;
    private static final int PROPERTY_valid = 91;
    private static final int PROPERTY_visible = 92;
    private static final int PROPERTY_warningString = 93;
    private static final int PROPERTY_width = 94;
    private static final int PROPERTY_windowFocusListeners = 95;
    private static final int PROPERTY_windowListeners = 96;
    private static final int PROPERTY_windowStateListeners = 97;
    private static final int PROPERTY_x = 98;
    private static final int PROPERTY_y = 99;

    // Property array 
    /*lazy PropertyDescriptor*/
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[100];
    
        try {
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", kpi.beans.JBISelectionDialog.class, "getAccessibleContext", null ); // NOI18N
            properties[PROPERTY_active] = new PropertyDescriptor ( "active", kpi.beans.JBISelectionDialog.class, "isActive", null ); // NOI18N
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", kpi.beans.JBISelectionDialog.class, "getAlignmentX", null ); // NOI18N
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", kpi.beans.JBISelectionDialog.class, "getAlignmentY", null ); // NOI18N
            properties[PROPERTY_alwaysOnTop] = new PropertyDescriptor ( "alwaysOnTop", kpi.beans.JBISelectionDialog.class, "isAlwaysOnTop", "setAlwaysOnTop" ); // NOI18N
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", kpi.beans.JBISelectionDialog.class, "getBackground", "setBackground" ); // NOI18N
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", kpi.beans.JBISelectionDialog.class, "isBackgroundSet", null ); // NOI18N
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", kpi.beans.JBISelectionDialog.class, "getBounds", "setBounds" ); // NOI18N
            properties[PROPERTY_bufferStrategy] = new PropertyDescriptor ( "bufferStrategy", kpi.beans.JBISelectionDialog.class, "getBufferStrategy", null ); // NOI18N
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", kpi.beans.JBISelectionDialog.class, "getColorModel", null ); // NOI18N
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", kpi.beans.JBISelectionDialog.class, null, null, "getComponent", null ); // NOI18N
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", kpi.beans.JBISelectionDialog.class, "getComponentCount", null ); // NOI18N
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", kpi.beans.JBISelectionDialog.class, "getComponentListeners", null ); // NOI18N
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", kpi.beans.JBISelectionDialog.class, "getComponentOrientation", "setComponentOrientation" ); // NOI18N
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", kpi.beans.JBISelectionDialog.class, "getComponents", null ); // NOI18N
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", kpi.beans.JBISelectionDialog.class, "getContainerListeners", null ); // NOI18N
            properties[PROPERTY_contentPane] = new PropertyDescriptor ( "contentPane", kpi.beans.JBISelectionDialog.class, "getContentPane", "setContentPane" ); // NOI18N
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", kpi.beans.JBISelectionDialog.class, "getCursor", "setCursor" ); // NOI18N
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", kpi.beans.JBISelectionDialog.class, "isCursorSet", null ); // NOI18N
            properties[PROPERTY_defaultCloseOperation] = new PropertyDescriptor ( "defaultCloseOperation", kpi.beans.JBISelectionDialog.class, "getDefaultCloseOperation", "setDefaultCloseOperation" ); // NOI18N
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", kpi.beans.JBISelectionDialog.class, "isDisplayable", null ); // NOI18N
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", kpi.beans.JBISelectionDialog.class, "isDoubleBuffered", null ); // NOI18N
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", kpi.beans.JBISelectionDialog.class, "getDropTarget", "setDropTarget" ); // NOI18N
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", kpi.beans.JBISelectionDialog.class, "isEnabled", "setEnabled" ); // NOI18N
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", kpi.beans.JBISelectionDialog.class, "isFocusable", "setFocusable" ); // NOI18N
            properties[PROPERTY_focusableWindow] = new PropertyDescriptor ( "focusableWindow", kpi.beans.JBISelectionDialog.class, "isFocusableWindow", null ); // NOI18N
            properties[PROPERTY_focusableWindowState] = new PropertyDescriptor ( "focusableWindowState", kpi.beans.JBISelectionDialog.class, "getFocusableWindowState", "setFocusableWindowState" ); // NOI18N
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", kpi.beans.JBISelectionDialog.class, "isFocusCycleRoot", "setFocusCycleRoot" ); // NOI18N
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", kpi.beans.JBISelectionDialog.class, "getFocusCycleRootAncestor", null ); // NOI18N
            properties[PROPERTY_focused] = new PropertyDescriptor ( "focused", kpi.beans.JBISelectionDialog.class, "isFocused", null ); // NOI18N
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", kpi.beans.JBISelectionDialog.class, "getFocusListeners", null ); // NOI18N
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", kpi.beans.JBISelectionDialog.class, "isFocusOwner", null ); // NOI18N
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", kpi.beans.JBISelectionDialog.class, "isFocusTraversable", null ); // NOI18N
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", kpi.beans.JBISelectionDialog.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" ); // NOI18N
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", kpi.beans.JBISelectionDialog.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" ); // NOI18N
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", kpi.beans.JBISelectionDialog.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" ); // NOI18N
            properties[PROPERTY_focusTraversalPolicyProvider] = new PropertyDescriptor ( "focusTraversalPolicyProvider", kpi.beans.JBISelectionDialog.class, "isFocusTraversalPolicyProvider", "setFocusTraversalPolicyProvider" ); // NOI18N
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", kpi.beans.JBISelectionDialog.class, "isFocusTraversalPolicySet", null ); // NOI18N
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", kpi.beans.JBISelectionDialog.class, "getFont", "setFont" ); // NOI18N
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", kpi.beans.JBISelectionDialog.class, "isFontSet", null ); // NOI18N
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", kpi.beans.JBISelectionDialog.class, "getForeground", "setForeground" ); // NOI18N
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", kpi.beans.JBISelectionDialog.class, "isForegroundSet", null ); // NOI18N
            properties[PROPERTY_glassPane] = new PropertyDescriptor ( "glassPane", kpi.beans.JBISelectionDialog.class, "getGlassPane", "setGlassPane" ); // NOI18N
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", kpi.beans.JBISelectionDialog.class, "getGraphics", null ); // NOI18N
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", kpi.beans.JBISelectionDialog.class, "getGraphicsConfiguration", null ); // NOI18N
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", kpi.beans.JBISelectionDialog.class, "getHeight", null ); // NOI18N
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", kpi.beans.JBISelectionDialog.class, "getHierarchyBoundsListeners", null ); // NOI18N
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", kpi.beans.JBISelectionDialog.class, "getHierarchyListeners", null ); // NOI18N
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", kpi.beans.JBISelectionDialog.class, "getIgnoreRepaint", "setIgnoreRepaint" ); // NOI18N
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", kpi.beans.JBISelectionDialog.class, "getInputContext", null ); // NOI18N
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", kpi.beans.JBISelectionDialog.class, "getInputMethodListeners", null ); // NOI18N
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", kpi.beans.JBISelectionDialog.class, "getInputMethodRequests", null ); // NOI18N
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", kpi.beans.JBISelectionDialog.class, "getInsets", null ); // NOI18N
            properties[PROPERTY_JMenuBar] = new PropertyDescriptor ( "JMenuBar", kpi.beans.JBISelectionDialog.class, "getJMenuBar", "setJMenuBar" ); // NOI18N
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", kpi.beans.JBISelectionDialog.class, "getKeyListeners", null ); // NOI18N
            properties[PROPERTY_layeredPane] = new PropertyDescriptor ( "layeredPane", kpi.beans.JBISelectionDialog.class, "getLayeredPane", "setLayeredPane" ); // NOI18N
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", kpi.beans.JBISelectionDialog.class, "getLayout", "setLayout" ); // NOI18N
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", kpi.beans.JBISelectionDialog.class, "isLightweight", null ); // NOI18N
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", kpi.beans.JBISelectionDialog.class, "getLocale", "setLocale" ); // NOI18N
            properties[PROPERTY_location] = new PropertyDescriptor ( "location", kpi.beans.JBISelectionDialog.class, "getLocation", "setLocation" ); // NOI18N
            properties[PROPERTY_locationByPlatform] = new PropertyDescriptor ( "locationByPlatform", kpi.beans.JBISelectionDialog.class, "isLocationByPlatform", "setLocationByPlatform" ); // NOI18N
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", kpi.beans.JBISelectionDialog.class, "getLocationOnScreen", null ); // NOI18N
            properties[PROPERTY_locationRelativeTo] = new PropertyDescriptor ( "locationRelativeTo", kpi.beans.JBISelectionDialog.class, null, "setLocationRelativeTo" ); // NOI18N
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", kpi.beans.JBISelectionDialog.class, "getMaximumSize", "setMaximumSize" ); // NOI18N
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", kpi.beans.JBISelectionDialog.class, "isMaximumSizeSet", null ); // NOI18N
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", kpi.beans.JBISelectionDialog.class, "getMinimumSize", "setMinimumSize" ); // NOI18N
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", kpi.beans.JBISelectionDialog.class, "isMinimumSizeSet", null ); // NOI18N
            properties[PROPERTY_modal] = new PropertyDescriptor ( "modal", kpi.beans.JBISelectionDialog.class, "isModal", "setModal" ); // NOI18N
            properties[PROPERTY_mostRecentFocusOwner] = new PropertyDescriptor ( "mostRecentFocusOwner", kpi.beans.JBISelectionDialog.class, "getMostRecentFocusOwner", null ); // NOI18N
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", kpi.beans.JBISelectionDialog.class, "getMouseListeners", null ); // NOI18N
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", kpi.beans.JBISelectionDialog.class, "getMouseMotionListeners", null ); // NOI18N
            properties[PROPERTY_mousePosition] = new PropertyDescriptor ( "mousePosition", kpi.beans.JBISelectionDialog.class, "getMousePosition", null ); // NOI18N
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", kpi.beans.JBISelectionDialog.class, "getMouseWheelListeners", null ); // NOI18N
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", kpi.beans.JBISelectionDialog.class, "getName", "setName" ); // NOI18N
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", kpi.beans.JBISelectionDialog.class, "isOpaque", null ); // NOI18N
            properties[PROPERTY_ownedWindows] = new PropertyDescriptor ( "ownedWindows", kpi.beans.JBISelectionDialog.class, "getOwnedWindows", null ); // NOI18N
            properties[PROPERTY_owner] = new PropertyDescriptor ( "owner", kpi.beans.JBISelectionDialog.class, "getOwner", null ); // NOI18N
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", kpi.beans.JBISelectionDialog.class, "getParent", null ); // NOI18N
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", kpi.beans.JBISelectionDialog.class, "getPeer", null ); // NOI18N
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", kpi.beans.JBISelectionDialog.class, "getPreferredSize", "setPreferredSize" ); // NOI18N
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", kpi.beans.JBISelectionDialog.class, "isPreferredSizeSet", null ); // NOI18N
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", kpi.beans.JBISelectionDialog.class, "getPropertyChangeListeners", null ); // NOI18N
            properties[PROPERTY_resizable] = new PropertyDescriptor ( "resizable", kpi.beans.JBISelectionDialog.class, "isResizable", "setResizable" ); // NOI18N
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", kpi.beans.JBISelectionDialog.class, "getRootPane", null ); // NOI18N
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", kpi.beans.JBISelectionDialog.class, "isShowing", null ); // NOI18N
            properties[PROPERTY_size] = new PropertyDescriptor ( "size", kpi.beans.JBISelectionDialog.class, "getSize", "setSize" ); // NOI18N
            properties[PROPERTY_tableCtrl] = new PropertyDescriptor ( "tableCtrl", kpi.beans.JBISelectionDialog.class, null, "setTableCtrl" ); // NOI18N
            properties[PROPERTY_title] = new PropertyDescriptor ( "title", kpi.beans.JBISelectionDialog.class, "getTitle", "setTitle" ); // NOI18N
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", kpi.beans.JBISelectionDialog.class, "getToolkit", null ); // NOI18N
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", kpi.beans.JBISelectionDialog.class, "getTreeLock", null ); // NOI18N
            properties[PROPERTY_undecorated] = new PropertyDescriptor ( "undecorated", kpi.beans.JBISelectionDialog.class, "isUndecorated", "setUndecorated" ); // NOI18N
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", kpi.beans.JBISelectionDialog.class, "isValid", null ); // NOI18N
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", kpi.beans.JBISelectionDialog.class, "isVisible", "setVisible" ); // NOI18N
            properties[PROPERTY_warningString] = new PropertyDescriptor ( "warningString", kpi.beans.JBISelectionDialog.class, "getWarningString", null ); // NOI18N
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", kpi.beans.JBISelectionDialog.class, "getWidth", null ); // NOI18N
            properties[PROPERTY_windowFocusListeners] = new PropertyDescriptor ( "windowFocusListeners", kpi.beans.JBISelectionDialog.class, "getWindowFocusListeners", null ); // NOI18N
            properties[PROPERTY_windowListeners] = new PropertyDescriptor ( "windowListeners", kpi.beans.JBISelectionDialog.class, "getWindowListeners", null ); // NOI18N
            properties[PROPERTY_windowStateListeners] = new PropertyDescriptor ( "windowStateListeners", kpi.beans.JBISelectionDialog.class, "getWindowStateListeners", null ); // NOI18N
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", kpi.beans.JBISelectionDialog.class, "getX", null ); // NOI18N
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", kpi.beans.JBISelectionDialog.class, "getY", null ); // NOI18N
        }
        catch(IntrospectionException e) {
            e.printStackTrace();
        }//GEN-HEADEREND:Properties
		
		// Here you can add code for customizing the properties array.
		
        return properties;     }//GEN-LAST:Properties
	
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_componentListener = 0;
    private static final int EVENT_containerListener = 1;
    private static final int EVENT_focusListener = 2;
    private static final int EVENT_hierarchyBoundsListener = 3;
    private static final int EVENT_hierarchyListener = 4;
    private static final int EVENT_inputMethodListener = 5;
    private static final int EVENT_keyListener = 6;
    private static final int EVENT_mouseListener = 7;
    private static final int EVENT_mouseMotionListener = 8;
    private static final int EVENT_mouseWheelListener = 9;
    private static final int EVENT_propertyChangeListener = 10;
    private static final int EVENT_windowFocusListener = 11;
    private static final int EVENT_windowListener = 12;
    private static final int EVENT_windowStateListener = 13;

    // EventSet array
    /*lazy EventSetDescriptor*/
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[14];
    
        try {
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( kpi.beans.JBISelectionDialog.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentHidden", "componentMoved", "componentResized", "componentShown"}, "addComponentListener", "removeComponentListener" ); // NOI18N
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( kpi.beans.JBISelectionDialog.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" ); // NOI18N
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( kpi.beans.JBISelectionDialog.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusGained", "focusLost"}, "addFocusListener", "removeFocusListener" ); // NOI18N
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( kpi.beans.JBISelectionDialog.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" ); // NOI18N
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( kpi.beans.JBISelectionDialog.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" ); // NOI18N
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( kpi.beans.JBISelectionDialog.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" ); // NOI18N
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( kpi.beans.JBISelectionDialog.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyReleased", "keyTyped"}, "addKeyListener", "removeKeyListener" ); // NOI18N
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( kpi.beans.JBISelectionDialog.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mouseEntered", "mouseExited", "mousePressed", "mouseReleased"}, "addMouseListener", "removeMouseListener" ); // NOI18N
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( kpi.beans.JBISelectionDialog.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" ); // NOI18N
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( kpi.beans.JBISelectionDialog.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" ); // NOI18N
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( kpi.beans.JBISelectionDialog.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" ); // NOI18N
            eventSets[EVENT_windowFocusListener] = new EventSetDescriptor ( kpi.beans.JBISelectionDialog.class, "windowFocusListener", java.awt.event.WindowFocusListener.class, new String[] {"windowGainedFocus", "windowLostFocus"}, "addWindowFocusListener", "removeWindowFocusListener" ); // NOI18N
            eventSets[EVENT_windowListener] = new EventSetDescriptor ( kpi.beans.JBISelectionDialog.class, "windowListener", java.awt.event.WindowListener.class, new String[] {"windowActivated", "windowClosed", "windowClosing", "windowDeactivated", "windowDeiconified", "windowIconified", "windowOpened"}, "addWindowListener", "removeWindowListener" ); // NOI18N
            eventSets[EVENT_windowStateListener] = new EventSetDescriptor ( kpi.beans.JBISelectionDialog.class, "windowStateListener", java.awt.event.WindowStateListener.class, new String[] {"windowStateChanged"}, "addWindowStateListener", "removeWindowStateListener" ); // NOI18N
        }
        catch(IntrospectionException e) {
            e.printStackTrace();
        }//GEN-HEADEREND:Events
		
		// Here you can add code for customizing the event sets array.
		
        return eventSets;     }//GEN-LAST:Events
	
    // Method identifiers//GEN-FIRST:Methods
    private static final int METHOD_action0 = 0;
    private static final int METHOD_add1 = 1;
    private static final int METHOD_addNotify2 = 2;
    private static final int METHOD_addPropertyChangeListener3 = 3;
    private static final int METHOD_applyComponentOrientation4 = 4;
    private static final int METHOD_applyResourceBundle5 = 5;
    private static final int METHOD_areFocusTraversalKeysSet6 = 6;
    private static final int METHOD_bounds7 = 7;
    private static final int METHOD_checkImage8 = 8;
    private static final int METHOD_contains9 = 9;
    private static final int METHOD_countComponents10 = 10;
    private static final int METHOD_createBufferStrategy11 = 11;
    private static final int METHOD_createImage12 = 12;
    private static final int METHOD_createVolatileImage13 = 13;
    private static final int METHOD_deliverEvent14 = 14;
    private static final int METHOD_disable15 = 15;
    private static final int METHOD_dispatchEvent16 = 16;
    private static final int METHOD_dispose17 = 17;
    private static final int METHOD_doLayout18 = 18;
    private static final int METHOD_enable19 = 19;
    private static final int METHOD_enableInputMethods20 = 20;
    private static final int METHOD_findComponentAt21 = 21;
    private static final int METHOD_firePropertyChange22 = 22;
    private static final int METHOD_getBounds23 = 23;
    private static final int METHOD_getComponentAt24 = 24;
    private static final int METHOD_getComponentZOrder25 = 25;
    private static final int METHOD_getFocusOwner26 = 26;
    private static final int METHOD_getFontMetrics27 = 27;
    private static final int METHOD_getListeners28 = 28;
    private static final int METHOD_getLocation29 = 29;
    private static final int METHOD_getMousePosition30 = 30;
    private static final int METHOD_getPropertyChangeListeners31 = 31;
    private static final int METHOD_getSize32 = 32;
    private static final int METHOD_gotFocus33 = 33;
    private static final int METHOD_handleEvent34 = 34;
    private static final int METHOD_hasFocus35 = 35;
    private static final int METHOD_hide36 = 36;
    private static final int METHOD_imageUpdate37 = 37;
    private static final int METHOD_insets38 = 38;
    private static final int METHOD_inside39 = 39;
    private static final int METHOD_invalidate40 = 40;
    private static final int METHOD_isAncestorOf41 = 41;
    private static final int METHOD_isDefaultLookAndFeelDecorated42 = 42;
    private static final int METHOD_isFocusCycleRoot43 = 43;
    private static final int METHOD_keyDown44 = 44;
    private static final int METHOD_keyUp45 = 45;
    private static final int METHOD_layout46 = 46;
    private static final int METHOD_list47 = 47;
    private static final int METHOD_locate48 = 48;
    private static final int METHOD_location49 = 49;
    private static final int METHOD_lostFocus50 = 50;
    private static final int METHOD_minimumSize51 = 51;
    private static final int METHOD_mouseDown52 = 52;
    private static final int METHOD_mouseDrag53 = 53;
    private static final int METHOD_mouseEnter54 = 54;
    private static final int METHOD_mouseExit55 = 55;
    private static final int METHOD_mouseMove56 = 56;
    private static final int METHOD_mouseUp57 = 57;
    private static final int METHOD_move58 = 58;
    private static final int METHOD_nextFocus59 = 59;
    private static final int METHOD_pack60 = 60;
    private static final int METHOD_paint61 = 61;
    private static final int METHOD_paintAll62 = 62;
    private static final int METHOD_paintComponents63 = 63;
    private static final int METHOD_postEvent64 = 64;
    private static final int METHOD_preferredSize65 = 65;
    private static final int METHOD_prepareImage66 = 66;
    private static final int METHOD_print67 = 67;
    private static final int METHOD_printAll68 = 68;
    private static final int METHOD_printComponents69 = 69;
    private static final int METHOD_remove70 = 70;
    private static final int METHOD_removeAll71 = 71;
    private static final int METHOD_removeNotify72 = 72;
    private static final int METHOD_removePropertyChangeListener73 = 73;
    private static final int METHOD_repaint74 = 74;
    private static final int METHOD_requestFocus75 = 75;
    private static final int METHOD_requestFocusInWindow76 = 76;
    private static final int METHOD_reshape77 = 77;
    private static final int METHOD_resize78 = 78;
    private static final int METHOD_setBounds79 = 79;
    private static final int METHOD_setComponentZOrder80 = 80;
    private static final int METHOD_setDefaultLookAndFeelDecorated81 = 81;
    private static final int METHOD_show82 = 82;
    private static final int METHOD_size83 = 83;
    private static final int METHOD_toBack84 = 84;
    private static final int METHOD_toFront85 = 85;
    private static final int METHOD_toString86 = 86;
    private static final int METHOD_transferFocus87 = 87;
    private static final int METHOD_transferFocusBackward88 = 88;
    private static final int METHOD_transferFocusDownCycle89 = 89;
    private static final int METHOD_transferFocusUpCycle90 = 90;
    private static final int METHOD_update91 = 91;
    private static final int METHOD_validate92 = 92;

    // Method array 
    /*lazy MethodDescriptor*/
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[93];
    
        try {
            methods[METHOD_action0] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_action0].setDisplayName ( "" );
            methods[METHOD_add1] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("add", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_add1].setDisplayName ( "" );
            methods[METHOD_addNotify2] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("addNotify", new Class[] {})); // NOI18N
            methods[METHOD_addNotify2].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener3] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class})); // NOI18N
            methods[METHOD_addPropertyChangeListener3].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation4] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class})); // NOI18N
            methods[METHOD_applyComponentOrientation4].setDisplayName ( "" );
            methods[METHOD_applyResourceBundle5] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("applyResourceBundle", new Class[] {java.util.ResourceBundle.class})); // NOI18N
            methods[METHOD_applyResourceBundle5].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet6] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE})); // NOI18N
            methods[METHOD_areFocusTraversalKeysSet6].setDisplayName ( "" );
            methods[METHOD_bounds7] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("bounds", new Class[] {})); // NOI18N
            methods[METHOD_bounds7].setDisplayName ( "" );
            methods[METHOD_checkImage8] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class})); // NOI18N
            methods[METHOD_checkImage8].setDisplayName ( "" );
            methods[METHOD_contains9] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_contains9].setDisplayName ( "" );
            methods[METHOD_countComponents10] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("countComponents", new Class[] {})); // NOI18N
            methods[METHOD_countComponents10].setDisplayName ( "" );
            methods[METHOD_createBufferStrategy11] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("createBufferStrategy", new Class[] {Integer.TYPE})); // NOI18N
            methods[METHOD_createBufferStrategy11].setDisplayName ( "" );
            methods[METHOD_createImage12] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class})); // NOI18N
            methods[METHOD_createImage12].setDisplayName ( "" );
            methods[METHOD_createVolatileImage13] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_createVolatileImage13].setDisplayName ( "" );
            methods[METHOD_deliverEvent14] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_deliverEvent14].setDisplayName ( "" );
            methods[METHOD_disable15] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("disable", new Class[] {})); // NOI18N
            methods[METHOD_disable15].setDisplayName ( "" );
            methods[METHOD_dispatchEvent16] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class})); // NOI18N
            methods[METHOD_dispatchEvent16].setDisplayName ( "" );
            methods[METHOD_dispose17] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("dispose", new Class[] {})); // NOI18N
            methods[METHOD_dispose17].setDisplayName ( "" );
            methods[METHOD_doLayout18] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("doLayout", new Class[] {})); // NOI18N
            methods[METHOD_doLayout18].setDisplayName ( "" );
            methods[METHOD_enable19] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("enable", new Class[] {})); // NOI18N
            methods[METHOD_enable19].setDisplayName ( "" );
            methods[METHOD_enableInputMethods20] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE})); // NOI18N
            methods[METHOD_enableInputMethods20].setDisplayName ( "" );
            methods[METHOD_findComponentAt21] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_findComponentAt21].setDisplayName ( "" );
            methods[METHOD_firePropertyChange22] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Byte.TYPE, Byte.TYPE})); // NOI18N
            methods[METHOD_firePropertyChange22].setDisplayName ( "" );
            methods[METHOD_getBounds23] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_getBounds23].setDisplayName ( "" );
            methods[METHOD_getComponentAt24] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_getComponentAt24].setDisplayName ( "" );
            methods[METHOD_getComponentZOrder25] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("getComponentZOrder", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_getComponentZOrder25].setDisplayName ( "" );
            methods[METHOD_getFocusOwner26] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("getFocusOwner", new Class[] {})); // NOI18N
            methods[METHOD_getFocusOwner26].setDisplayName ( "" );
            methods[METHOD_getFontMetrics27] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class})); // NOI18N
            methods[METHOD_getFontMetrics27].setDisplayName ( "" );
            methods[METHOD_getListeners28] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("getListeners", new Class[] {java.lang.Class.class})); // NOI18N
            methods[METHOD_getListeners28].setDisplayName ( "" );
            methods[METHOD_getLocation29] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("getLocation", new Class[] {java.awt.Point.class})); // NOI18N
            methods[METHOD_getLocation29].setDisplayName ( "" );
            methods[METHOD_getMousePosition30] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("getMousePosition", new Class[] {Boolean.TYPE})); // NOI18N
            methods[METHOD_getMousePosition30].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners31] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class})); // NOI18N
            methods[METHOD_getPropertyChangeListeners31].setDisplayName ( "" );
            methods[METHOD_getSize32] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("getSize", new Class[] {java.awt.Dimension.class})); // NOI18N
            methods[METHOD_getSize32].setDisplayName ( "" );
            methods[METHOD_gotFocus33] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_gotFocus33].setDisplayName ( "" );
            methods[METHOD_handleEvent34] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("handleEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_handleEvent34].setDisplayName ( "" );
            methods[METHOD_hasFocus35] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("hasFocus", new Class[] {})); // NOI18N
            methods[METHOD_hasFocus35].setDisplayName ( "" );
            methods[METHOD_hide36] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("hide", new Class[] {})); // NOI18N
            methods[METHOD_hide36].setDisplayName ( "" );
            methods[METHOD_imageUpdate37] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_imageUpdate37].setDisplayName ( "" );
            methods[METHOD_insets38] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("insets", new Class[] {})); // NOI18N
            methods[METHOD_insets38].setDisplayName ( "" );
            methods[METHOD_inside39] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_inside39].setDisplayName ( "" );
            methods[METHOD_invalidate40] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("invalidate", new Class[] {})); // NOI18N
            methods[METHOD_invalidate40].setDisplayName ( "" );
            methods[METHOD_isAncestorOf41] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_isAncestorOf41].setDisplayName ( "" );
            methods[METHOD_isDefaultLookAndFeelDecorated42] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("isDefaultLookAndFeelDecorated", new Class[] {})); // NOI18N
            methods[METHOD_isDefaultLookAndFeelDecorated42].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot43] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class})); // NOI18N
            methods[METHOD_isFocusCycleRoot43].setDisplayName ( "" );
            methods[METHOD_keyDown44] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE})); // NOI18N
            methods[METHOD_keyDown44].setDisplayName ( "" );
            methods[METHOD_keyUp45] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE})); // NOI18N
            methods[METHOD_keyUp45].setDisplayName ( "" );
            methods[METHOD_layout46] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("layout", new Class[] {})); // NOI18N
            methods[METHOD_layout46].setDisplayName ( "" );
            methods[METHOD_list47] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE})); // NOI18N
            methods[METHOD_list47].setDisplayName ( "" );
            methods[METHOD_locate48] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_locate48].setDisplayName ( "" );
            methods[METHOD_location49] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("location", new Class[] {})); // NOI18N
            methods[METHOD_location49].setDisplayName ( "" );
            methods[METHOD_lostFocus50] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_lostFocus50].setDisplayName ( "" );
            methods[METHOD_minimumSize51] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("minimumSize", new Class[] {})); // NOI18N
            methods[METHOD_minimumSize51].setDisplayName ( "" );
            methods[METHOD_mouseDown52] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseDown52].setDisplayName ( "" );
            methods[METHOD_mouseDrag53] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseDrag53].setDisplayName ( "" );
            methods[METHOD_mouseEnter54] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseEnter54].setDisplayName ( "" );
            methods[METHOD_mouseExit55] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseExit55].setDisplayName ( "" );
            methods[METHOD_mouseMove56] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseMove56].setDisplayName ( "" );
            methods[METHOD_mouseUp57] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseUp57].setDisplayName ( "" );
            methods[METHOD_move58] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_move58].setDisplayName ( "" );
            methods[METHOD_nextFocus59] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("nextFocus", new Class[] {})); // NOI18N
            methods[METHOD_nextFocus59].setDisplayName ( "" );
            methods[METHOD_pack60] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("pack", new Class[] {})); // NOI18N
            methods[METHOD_pack60].setDisplayName ( "" );
            methods[METHOD_paint61] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("paint", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paint61].setDisplayName ( "" );
            methods[METHOD_paintAll62] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paintAll62].setDisplayName ( "" );
            methods[METHOD_paintComponents63] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paintComponents63].setDisplayName ( "" );
            methods[METHOD_postEvent64] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("postEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_postEvent64].setDisplayName ( "" );
            methods[METHOD_preferredSize65] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("preferredSize", new Class[] {})); // NOI18N
            methods[METHOD_preferredSize65].setDisplayName ( "" );
            methods[METHOD_prepareImage66] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class})); // NOI18N
            methods[METHOD_prepareImage66].setDisplayName ( "" );
            methods[METHOD_print67] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("print", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_print67].setDisplayName ( "" );
            methods[METHOD_printAll68] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("printAll", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_printAll68].setDisplayName ( "" );
            methods[METHOD_printComponents69] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_printComponents69].setDisplayName ( "" );
            methods[METHOD_remove70] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("remove", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_remove70].setDisplayName ( "" );
            methods[METHOD_removeAll71] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("removeAll", new Class[] {})); // NOI18N
            methods[METHOD_removeAll71].setDisplayName ( "" );
            methods[METHOD_removeNotify72] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("removeNotify", new Class[] {})); // NOI18N
            methods[METHOD_removeNotify72].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener73] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class})); // NOI18N
            methods[METHOD_removePropertyChangeListener73].setDisplayName ( "" );
            methods[METHOD_repaint74] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("repaint", new Class[] {})); // NOI18N
            methods[METHOD_repaint74].setDisplayName ( "" );
            methods[METHOD_requestFocus75] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("requestFocus", new Class[] {})); // NOI18N
            methods[METHOD_requestFocus75].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow76] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("requestFocusInWindow", new Class[] {})); // NOI18N
            methods[METHOD_requestFocusInWindow76].setDisplayName ( "" );
            methods[METHOD_reshape77] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_reshape77].setDisplayName ( "" );
            methods[METHOD_resize78] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_resize78].setDisplayName ( "" );
            methods[METHOD_setBounds79] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_setBounds79].setDisplayName ( "" );
            methods[METHOD_setComponentZOrder80] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("setComponentZOrder", new Class[] {java.awt.Component.class, Integer.TYPE})); // NOI18N
            methods[METHOD_setComponentZOrder80].setDisplayName ( "" );
            methods[METHOD_setDefaultLookAndFeelDecorated81] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("setDefaultLookAndFeelDecorated", new Class[] {Boolean.TYPE})); // NOI18N
            methods[METHOD_setDefaultLookAndFeelDecorated81].setDisplayName ( "" );
            methods[METHOD_show82] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("show", new Class[] {})); // NOI18N
            methods[METHOD_show82].setDisplayName ( "" );
            methods[METHOD_size83] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("size", new Class[] {})); // NOI18N
            methods[METHOD_size83].setDisplayName ( "" );
            methods[METHOD_toBack84] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("toBack", new Class[] {})); // NOI18N
            methods[METHOD_toBack84].setDisplayName ( "" );
            methods[METHOD_toFront85] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("toFront", new Class[] {})); // NOI18N
            methods[METHOD_toFront85].setDisplayName ( "" );
            methods[METHOD_toString86] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("toString", new Class[] {})); // NOI18N
            methods[METHOD_toString86].setDisplayName ( "" );
            methods[METHOD_transferFocus87] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("transferFocus", new Class[] {})); // NOI18N
            methods[METHOD_transferFocus87].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward88] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("transferFocusBackward", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusBackward88].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle89] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("transferFocusDownCycle", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusDownCycle89].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle90] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("transferFocusUpCycle", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusUpCycle90].setDisplayName ( "" );
            methods[METHOD_update91] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("update", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_update91].setDisplayName ( "" );
            methods[METHOD_validate92] = new MethodDescriptor ( kpi.beans.JBISelectionDialog.class.getMethod("validate", new Class[] {})); // NOI18N
            methods[METHOD_validate92].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
		
		// Here you can add code for customizing the methods array.
		
        return methods;     }//GEN-LAST:Methods
	
	
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
	
	
//GEN-FIRST:Superclass
	
	// Here you can add code for customizing the Superclass BeanInfo.
	
//GEN-LAST:Superclass
	
	/**
	 * Gets the bean's <code>BeanDescriptor</code>s.
	 *
	 * @return BeanDescriptor describing the editable
	 * properties of this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 */
	public BeanDescriptor getBeanDescriptor() {
		return getBdescriptor();
	}
	
	/**
	 * Gets the bean's <code>PropertyDescriptor</code>s.
	 *
	 * @return An array of PropertyDescriptors describing the editable
	 * properties supported by this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 * <p>
	 * If a property is indexed, then its entry in the result array will
	 * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
	 * A client of getPropertyDescriptors can use "instanceof" to check
	 * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
	 */
	public PropertyDescriptor[] getPropertyDescriptors() {
		return getPdescriptor();
	}
	
	/**
	 * Gets the bean's <code>EventSetDescriptor</code>s.
	 *
	 * @return  An array of EventSetDescriptors describing the kinds of
	 * events fired by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public EventSetDescriptor[] getEventSetDescriptors() {
		return getEdescriptor();
	}
	
	/**
	 * Gets the bean's <code>MethodDescriptor</code>s.
	 *
	 * @return  An array of MethodDescriptors describing the methods
	 * implemented by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public MethodDescriptor[] getMethodDescriptors() {
		return getMdescriptor();
	}
	
	/**
	 * A bean may have a "default" property that is the property that will
	 * mostly commonly be initially chosen for update by human's who are
	 * customizing the bean.
	 * @return  Index of default property in the PropertyDescriptor array
	 * 		returned by getPropertyDescriptors.
	 * <P>	Returns -1 if there is no default property.
	 */
	public int getDefaultPropertyIndex() {
		return defaultPropertyIndex;
	}
	
	/**
	 * A bean may have a "default" event that is the event that will
	 * mostly commonly be used by human's when using the bean.
	 * @return Index of default event in the EventSetDescriptor array
	 *		returned by getEventSetDescriptors.
	 * <P>	Returns -1 if there is no default event.
	 */
	public int getDefaultEventIndex() {
		return defaultEventIndex;
	}
}

