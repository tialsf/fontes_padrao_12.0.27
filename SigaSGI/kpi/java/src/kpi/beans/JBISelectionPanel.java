/*
 * JBISelectionPanel.java
 *
 * Created on September 22, 2005, 11:14 AM
 */

package kpi.beans;

import kpi.xml.BIXMLRecord;
/**
 *
 * @author  siga0739
 */
public class JBISelectionPanel extends javax.swing.JPanel {
	private String cTileSelection = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("JBISelectionPanel_00003");
	
	private kpi.xml.BIXMLVector selectedElements = null,
		notSelectedElements = null,
		allElements = null,
		selectedIDs = null;
	
	private String	answerMainTag = null,
		answerRecordTag = null;
	
	// tipoRender = 0 - Default
	// tipoRender = 1 - Render para 2 colunas 1�ico 2�nome
	private int tipoRender = 0;
	
	/** Creates new form JBISelectionPanel */
	public JBISelectionPanel() {
		initComponents();
	}
	
	public JBISelectionPanel(int nTipoRender) {
		initComponents();
		tipoRender = nTipoRender;
	}
	

	public kpi.xml.BIXMLVector getXMLData() {
		kpi.xml.BIXMLVector vector = new kpi.xml.BIXMLVector( answerMainTag, answerRecordTag );
		if ( selectedElements != null ) {
			BITableModel tableModel = (BITableModel) tblSelected.getModel();
			for ( int i = 0; i < tableModel.getRowCount(); i ++ ){
				BIXMLRecord rec = vector.addNewRecord();
				rec.set("ID", tableModel.getRecordID(i));
				
				//Tipo = 0.Usuario Tipo = 1.Grupo
				int row =  tblSelected.getOriginalRow(i);
				if (tableModel.records.get(row).contains("TIPO")){
					String tipo = tableModel.records.get(row).getString("TIPO");
					rec.set("TIPO", tipo);
				}
				
			}
		}
		return vector;
	}
	
	public void refreshTable( kpi.xml.BIXMLVector selectedIDsAux ) {
		selectedElements = new kpi.xml.BIXMLVector( allElements.getMainTag(),
			allElements.getRecordTag() );
		notSelectedElements = new kpi.xml.BIXMLVector( allElements.getMainTag(),
			allElements.getRecordTag() );
		
		selectedElements.setAttributes( allElements.getAttributes() );
		notSelectedElements.setAttributes( allElements.getAttributes() );
		
		boolean elementIsSelected = false;
		for ( int j = 0; j < allElements.size(); j++ ) {
			elementIsSelected = false;
			for ( int i = 0; ( i < selectedIDsAux.size() ) && (!elementIsSelected); i++ ){
				if ( selectedIDsAux.get(i).getString("ID").equals(allElements.get(j).getString("ID")) ){
					if (selectedIDsAux.get(i).contains("TIPO")){
						if (selectedIDsAux.get(i).getString("TIPO").equals(allElements.get(j).getString("TIPO")) ){
							elementIsSelected = true;
							break;
						}else{
							elementIsSelected = false;
						}
					}else{
						elementIsSelected = true;
						break;
					}
				}else{
					elementIsSelected = false;
				}
			}
			if ( elementIsSelected )
				selectedElements.add( allElements.get(j) );
			else
				notSelectedElements.add( allElements.get(j) );
		}
		
		tblSelected.setDataSource( selectedElements );
		setRenderTable(tipoRender);
	}
	
    @Override
	public void setEnabled(boolean enabled) {
		btnAdd.setEnabled(enabled);
		btnRemove.setEnabled(enabled);
	}
	
	public JBIXMLTable getTableModel(){
		return tblSelected;
	}
	
	
	public void setDataSource( kpi.xml.BIXMLVector allElementsAux,
		kpi.xml.BIXMLVector selectedIDsAux, String mainTag, String recordTag ) {
		allElements = allElementsAux.clone2();
		selectedElements = new kpi.xml.BIXMLVector( allElements.getMainTag(),
			allElements.getRecordTag() );
		notSelectedElements = new kpi.xml.BIXMLVector( allElements.getMainTag(),
			allElements.getRecordTag() );
		
		answerMainTag = String.valueOf( mainTag );
		answerRecordTag = String.valueOf( recordTag );
		
		selectedElements.setAttributes( allElements.getAttributes() );
		notSelectedElements.setAttributes( allElements.getAttributes() );
		
		boolean elementIsSelected = false;
		for ( int j = 0; j < allElements.size(); j++ ) {
			elementIsSelected = false;
			for ( int i = 0; ( i < selectedIDsAux.size() ) && (!elementIsSelected); i++ ){
				if ( selectedIDsAux.get(i).getString("ID").equals(allElements.get(j).getString("ID")) ){
					if (selectedIDsAux.get(i).contains("TIPO")){
						if (selectedIDsAux.get(i).getString("TIPO").equals(allElements.get(j).getString("TIPO")) ){
							elementIsSelected = true;
							break;
						}	
					}else{
						elementIsSelected = true;
						break;
					}
				}
			}
			if ( elementIsSelected ){
				selectedElements.add( allElements.get(j) );
			}else{
				notSelectedElements.add( allElements.get(j) );
			}
		}
		
		tblSelected.setDataSource( selectedElements );
		setRenderTable(tipoRender);
	}
	
	private void setRenderTable(int nTipoRender){
		switch(nTipoRender){
			case  0:
				//Default
				break;
			case 1:
				tblSelected.setHeaderHeight(20);
				tblSelected.getColumn(0).setMaxWidth(50);
				tblSelected.getColumn(1).setPreferredWidth(800);
				break;
		}
	}
	
	public int getRowCount() {
		return tblSelected.getModel().getRowCount();
	}

	public String getTileSelection() {
		return cTileSelection;
	}

	public void setTileSelection(String cTileSelection) {
		this.cTileSelection = cTileSelection;
	}
	
	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        pnlTop = new javax.swing.JPanel();
        pnlBottom = new javax.swing.JPanel();
        pnlButtons = new javax.swing.JPanel();
        btnRemove = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        pnlLeft = new javax.swing.JPanel();
        pnlCenter = new javax.swing.JPanel();
        tblSelected = new kpi.beans.JBIXMLTable();

        setLayout(new java.awt.BorderLayout());

        pnlTop.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlTop.setPreferredSize(new java.awt.Dimension(5, 5));
        add(pnlTop, java.awt.BorderLayout.NORTH);

        pnlBottom.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlBottom.setPreferredSize(new java.awt.Dimension(5, 5));
        add(pnlBottom, java.awt.BorderLayout.SOUTH);

        pnlButtons.setLayout(null);

        pnlButtons.setPreferredSize(new java.awt.Dimension(50, 10));
        btnRemove.setMnemonic('-');
        btnRemove.setText("-");
        btnRemove.setToolTipText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("JBISelectionPanel_00001"));
        btnRemove.setIconTextGap(0);
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });

        pnlButtons.add(btnRemove);
        btnRemove.setBounds(5, 38, 42, 23);

        btnAdd.setMnemonic('+');
        btnAdd.setText("+");
        btnAdd.setToolTipText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("JBISelectionPanel_00002"));
        btnAdd.setIconTextGap(0);
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        pnlButtons.add(btnAdd);
        btnAdd.setBounds(5, 6, 42, 23);

        add(pnlButtons, java.awt.BorderLayout.EAST);

        pnlLeft.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlLeft.setPreferredSize(new java.awt.Dimension(5, 5));
        add(pnlLeft, java.awt.BorderLayout.WEST);

        pnlCenter.setLayout(new java.awt.BorderLayout());

        pnlCenter.add(tblSelected, java.awt.BorderLayout.CENTER);

        add(pnlCenter, java.awt.BorderLayout.CENTER);

    }// </editor-fold>//GEN-END:initComponents
	
	private void tblSelectedMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblSelectedMouseClicked
		if (evt.getClickCount() == 2) {
			BITableModel tableModel = (BITableModel) tblSelected.getModel();
			javax.swing.JInternalFrame frame =
				kpi.core.KpiStaticReferences.getKpiFormController().getForm(
				tableModel.getRecordType( tblSelected.getSelectedRow() ),
				tableModel.getRecordID( tblSelected.getSelectedRow() ),
				tableModel.getName( tblSelected.getSelectedRow() ) ).asJInternalFrame();
		}
	}//GEN-LAST:event_tblSelectedMouseClicked
	
	private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
		if (tblSelected.getSelectedRow() != -1) {
			BITableModel tableModel = (BITableModel) tblSelected.getModel();
			notSelectedElements.add(
				selectedElements.get( tableModel.getOriginalRow(tblSelected.getSelectedRow()) ).clone2() );
			selectedElements.remove(  tableModel.getOriginalRow(tblSelected.getSelectedRow()) );
			tblSelected.setDataSource(selectedElements);
			setRenderTable(tipoRender);
		}
	}//GEN-LAST:event_btnRemoveActionPerformed
	
	private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
		JBISelectionDialog selectionDialog = new JBISelectionDialog( kpi.core.KpiStaticReferences.getKpiMainPanel(),
			getTileSelection() , true, notSelectedElements.clone2(), selectedElements.clone2(), this, tipoRender );
		selectionDialog.setVisible(true);
	}//GEN-LAST:event_btnAddActionPerformed
	
	
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnRemove;
    private javax.swing.JPanel pnlBottom;
    private javax.swing.JPanel pnlButtons;
    private javax.swing.JPanel pnlCenter;
    private javax.swing.JPanel pnlLeft;
    private javax.swing.JPanel pnlTop;
    private kpi.beans.JBIXMLTable tblSelected;
    // End of variables declaration//GEN-END:variables
	
}
