/*
 * JBITextArea.java
 *
 * Created on October 7, 2003, 9:33 AM
 */
package kpi.beans;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author  siga1996
 */
public class JBITextArea extends javax.swing.JTextArea {

    /** Creates a new instance of JBITextArea */
    public JBITextArea() {
        super();
        java.util.HashSet hash = new java.util.HashSet();
        hash.add(java.awt.AWTKeyStroke.getAWTKeyStroke(java.awt.event.KeyEvent.VK_TAB, 0, false));
        this.setFocusTraversalKeys(java.awt.KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, hash);
        hash.clear();
        hash.add(java.awt.AWTKeyStroke.getAWTKeyStroke(java.awt.event.KeyEvent.VK_TAB,
                java.awt.event.InputEvent.SHIFT_DOWN_MASK, false));
        this.setFocusTraversalKeys(java.awt.KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, hash);

        this.setWrapStyleWord(true);
        this.setRows(3);
        this.setColumns(31);
        this.setLineWrap(true);

        this.setDocument(new JBITextAreaLimit());
    }

    public int getMaxLength() {
        return ((JBITextAreaLimit)this.getDocument()).getMaxLength();
    }

    public void setMaxLength(int limit) {
        ((JBITextAreaLimit)this.getDocument()).setMaxLength(limit);
    }
}

class JBITextAreaLimit extends PlainDocument {

    private int maxLength = 0;

    @Override
    public void insertString(int offset, String str, AttributeSet attr)
            throws BadLocationException {
        if (str == null) {
            return;
        }

        if (getMaxLength() == 0 || (getLength() + str.length()) <= getMaxLength()) {
            super.insertString(offset, str, attr);
        }
    }

    /**
     * @return the maxLength
     */
    public int getMaxLength() {
        return maxLength;
    }

    /**
     * @param maxLength the limit to set
     */
    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }
}
