/*
 * JBITextAreaBeanInfo.java
 *
 * Created on 26 de Agosto de 2005, 15:32
 */

package kpi.beans;

import java.beans.*;

/**
 * @author Alexandre Alves da Silva
 */
public class JBITextAreaBeanInfo extends SimpleBeanInfo {
    
    // Bean descriptor//GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( kpi.beans.JBITextArea.class , null );//GEN-HEADEREND:BeanDescriptor
	
	// Here you can add code for customizing the BeanDescriptor.
	
        return beanDescriptor;         }//GEN-LAST:BeanDescriptor
    
    
    // Property identifiers//GEN-FIRST:Properties
    private static final int PROPERTY_accessibleContext = 0;
    private static final int PROPERTY_actionMap = 1;
    private static final int PROPERTY_actions = 2;
    private static final int PROPERTY_alignmentX = 3;
    private static final int PROPERTY_alignmentY = 4;
    private static final int PROPERTY_ancestorListeners = 5;
    private static final int PROPERTY_autoscrolls = 6;
    private static final int PROPERTY_background = 7;
    private static final int PROPERTY_backgroundSet = 8;
    private static final int PROPERTY_border = 9;
    private static final int PROPERTY_bounds = 10;
    private static final int PROPERTY_caret = 11;
    private static final int PROPERTY_caretColor = 12;
    private static final int PROPERTY_caretListeners = 13;
    private static final int PROPERTY_caretPosition = 14;
    private static final int PROPERTY_colorModel = 15;
    private static final int PROPERTY_columns = 16;
    private static final int PROPERTY_component = 17;
    private static final int PROPERTY_componentCount = 18;
    private static final int PROPERTY_componentListeners = 19;
    private static final int PROPERTY_componentOrientation = 20;
    private static final int PROPERTY_componentPopupMenu = 21;
    private static final int PROPERTY_components = 22;
    private static final int PROPERTY_containerListeners = 23;
    private static final int PROPERTY_cursor = 24;
    private static final int PROPERTY_cursorSet = 25;
    private static final int PROPERTY_debugGraphicsOptions = 26;
    private static final int PROPERTY_disabledTextColor = 27;
    private static final int PROPERTY_displayable = 28;
    private static final int PROPERTY_document = 29;
    private static final int PROPERTY_doubleBuffered = 30;
    private static final int PROPERTY_dragEnabled = 31;
    private static final int PROPERTY_dropTarget = 32;
    private static final int PROPERTY_editable = 33;
    private static final int PROPERTY_enabled = 34;
    private static final int PROPERTY_focusable = 35;
    private static final int PROPERTY_focusAccelerator = 36;
    private static final int PROPERTY_focusCycleRoot = 37;
    private static final int PROPERTY_focusCycleRootAncestor = 38;
    private static final int PROPERTY_focusListeners = 39;
    private static final int PROPERTY_focusOwner = 40;
    private static final int PROPERTY_focusTraversable = 41;
    private static final int PROPERTY_focusTraversalKeys = 42;
    private static final int PROPERTY_focusTraversalKeysEnabled = 43;
    private static final int PROPERTY_focusTraversalPolicy = 44;
    private static final int PROPERTY_focusTraversalPolicyProvider = 45;
    private static final int PROPERTY_focusTraversalPolicySet = 46;
    private static final int PROPERTY_font = 47;
    private static final int PROPERTY_fontSet = 48;
    private static final int PROPERTY_foreground = 49;
    private static final int PROPERTY_foregroundSet = 50;
    private static final int PROPERTY_graphics = 51;
    private static final int PROPERTY_graphicsConfiguration = 52;
    private static final int PROPERTY_height = 53;
    private static final int PROPERTY_hierarchyBoundsListeners = 54;
    private static final int PROPERTY_hierarchyListeners = 55;
    private static final int PROPERTY_highlighter = 56;
    private static final int PROPERTY_ignoreRepaint = 57;
    private static final int PROPERTY_inheritsPopupMenu = 58;
    private static final int PROPERTY_inputContext = 59;
    private static final int PROPERTY_inputMap = 60;
    private static final int PROPERTY_inputMethodListeners = 61;
    private static final int PROPERTY_inputMethodRequests = 62;
    private static final int PROPERTY_inputVerifier = 63;
    private static final int PROPERTY_insets = 64;
    private static final int PROPERTY_keyListeners = 65;
    private static final int PROPERTY_keymap = 66;
    private static final int PROPERTY_layout = 67;
    private static final int PROPERTY_lightweight = 68;
    private static final int PROPERTY_lineCount = 69;
    private static final int PROPERTY_lineEndOffset = 70;
    private static final int PROPERTY_lineOfOffset = 71;
    private static final int PROPERTY_lineStartOffset = 72;
    private static final int PROPERTY_lineWrap = 73;
    private static final int PROPERTY_locale = 74;
    private static final int PROPERTY_location = 75;
    private static final int PROPERTY_locationOnScreen = 76;
    private static final int PROPERTY_managingFocus = 77;
    private static final int PROPERTY_margin = 78;
    private static final int PROPERTY_maximumSize = 79;
    private static final int PROPERTY_maximumSizeSet = 80;
    private static final int PROPERTY_minimumSize = 81;
    private static final int PROPERTY_minimumSizeSet = 82;
    private static final int PROPERTY_mouseListeners = 83;
    private static final int PROPERTY_mouseMotionListeners = 84;
    private static final int PROPERTY_mousePosition = 85;
    private static final int PROPERTY_mouseWheelListeners = 86;
    private static final int PROPERTY_name = 87;
    private static final int PROPERTY_navigationFilter = 88;
    private static final int PROPERTY_nextFocusableComponent = 89;
    private static final int PROPERTY_opaque = 90;
    private static final int PROPERTY_optimizedDrawingEnabled = 91;
    private static final int PROPERTY_paintingTile = 92;
    private static final int PROPERTY_parent = 93;
    private static final int PROPERTY_peer = 94;
    private static final int PROPERTY_preferredScrollableViewportSize = 95;
    private static final int PROPERTY_preferredSize = 96;
    private static final int PROPERTY_preferredSizeSet = 97;
    private static final int PROPERTY_propertyChangeListeners = 98;
    private static final int PROPERTY_registeredKeyStrokes = 99;
    private static final int PROPERTY_requestFocusEnabled = 100;
    private static final int PROPERTY_rootPane = 101;
    private static final int PROPERTY_rows = 102;
    private static final int PROPERTY_scrollableTracksViewportHeight = 103;
    private static final int PROPERTY_scrollableTracksViewportWidth = 104;
    private static final int PROPERTY_selectedText = 105;
    private static final int PROPERTY_selectedTextColor = 106;
    private static final int PROPERTY_selectionColor = 107;
    private static final int PROPERTY_selectionEnd = 108;
    private static final int PROPERTY_selectionStart = 109;
    private static final int PROPERTY_showing = 110;
    private static final int PROPERTY_size = 111;
    private static final int PROPERTY_tabSize = 112;
    private static final int PROPERTY_text = 113;
    private static final int PROPERTY_toolkit = 114;
    private static final int PROPERTY_toolTipText = 115;
    private static final int PROPERTY_topLevelAncestor = 116;
    private static final int PROPERTY_transferHandler = 117;
    private static final int PROPERTY_treeLock = 118;
    private static final int PROPERTY_UI = 119;
    private static final int PROPERTY_UIClassID = 120;
    private static final int PROPERTY_valid = 121;
    private static final int PROPERTY_validateRoot = 122;
    private static final int PROPERTY_verifyInputWhenFocusTarget = 123;
    private static final int PROPERTY_vetoableChangeListeners = 124;
    private static final int PROPERTY_visible = 125;
    private static final int PROPERTY_visibleRect = 126;
    private static final int PROPERTY_width = 127;
    private static final int PROPERTY_wrapStyleWord = 128;
    private static final int PROPERTY_x = 129;
    private static final int PROPERTY_y = 130;
    private static final int PROPERTY_maxLength = 131;

    // Property array 
    /*lazy PropertyDescriptor*/
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[132];
    
        try {
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", kpi.beans.JBITextArea.class, "getAccessibleContext", null );
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", kpi.beans.JBITextArea.class, "getActionMap", "setActionMap" );
            properties[PROPERTY_actions] = new PropertyDescriptor ( "actions", kpi.beans.JBITextArea.class, "getActions", null );
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", kpi.beans.JBITextArea.class, "getAlignmentX", "setAlignmentX" );
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", kpi.beans.JBITextArea.class, "getAlignmentY", "setAlignmentY" );
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", kpi.beans.JBITextArea.class, "getAncestorListeners", null );
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", kpi.beans.JBITextArea.class, "getAutoscrolls", "setAutoscrolls" );
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", kpi.beans.JBITextArea.class, "getBackground", "setBackground" );
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", kpi.beans.JBITextArea.class, "isBackgroundSet", null );
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", kpi.beans.JBITextArea.class, "getBorder", "setBorder" );
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", kpi.beans.JBITextArea.class, "getBounds", "setBounds" );
            properties[PROPERTY_caret] = new PropertyDescriptor ( "caret", kpi.beans.JBITextArea.class, "getCaret", "setCaret" );
            properties[PROPERTY_caretColor] = new PropertyDescriptor ( "caretColor", kpi.beans.JBITextArea.class, "getCaretColor", "setCaretColor" );
            properties[PROPERTY_caretListeners] = new PropertyDescriptor ( "caretListeners", kpi.beans.JBITextArea.class, "getCaretListeners", null );
            properties[PROPERTY_caretPosition] = new PropertyDescriptor ( "caretPosition", kpi.beans.JBITextArea.class, "getCaretPosition", "setCaretPosition" );
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", kpi.beans.JBITextArea.class, "getColorModel", null );
            properties[PROPERTY_columns] = new PropertyDescriptor ( "columns", kpi.beans.JBITextArea.class, "getColumns", "setColumns" );
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", kpi.beans.JBITextArea.class, null, null, "getComponent", null );
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", kpi.beans.JBITextArea.class, "getComponentCount", null );
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", kpi.beans.JBITextArea.class, "getComponentListeners", null );
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", kpi.beans.JBITextArea.class, "getComponentOrientation", "setComponentOrientation" );
            properties[PROPERTY_componentPopupMenu] = new PropertyDescriptor ( "componentPopupMenu", kpi.beans.JBITextArea.class, "getComponentPopupMenu", "setComponentPopupMenu" );
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", kpi.beans.JBITextArea.class, "getComponents", null );
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", kpi.beans.JBITextArea.class, "getContainerListeners", null );
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", kpi.beans.JBITextArea.class, "getCursor", "setCursor" );
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", kpi.beans.JBITextArea.class, "isCursorSet", null );
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", kpi.beans.JBITextArea.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" );
            properties[PROPERTY_disabledTextColor] = new PropertyDescriptor ( "disabledTextColor", kpi.beans.JBITextArea.class, "getDisabledTextColor", "setDisabledTextColor" );
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", kpi.beans.JBITextArea.class, "isDisplayable", null );
            properties[PROPERTY_document] = new PropertyDescriptor ( "document", kpi.beans.JBITextArea.class, "getDocument", "setDocument" );
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", kpi.beans.JBITextArea.class, "isDoubleBuffered", "setDoubleBuffered" );
            properties[PROPERTY_dragEnabled] = new PropertyDescriptor ( "dragEnabled", kpi.beans.JBITextArea.class, "getDragEnabled", "setDragEnabled" );
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", kpi.beans.JBITextArea.class, "getDropTarget", "setDropTarget" );
            properties[PROPERTY_editable] = new PropertyDescriptor ( "editable", kpi.beans.JBITextArea.class, "isEditable", "setEditable" );
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", kpi.beans.JBITextArea.class, "isEnabled", "setEnabled" );
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", kpi.beans.JBITextArea.class, "isFocusable", "setFocusable" );
            properties[PROPERTY_focusAccelerator] = new PropertyDescriptor ( "focusAccelerator", kpi.beans.JBITextArea.class, "getFocusAccelerator", "setFocusAccelerator" );
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", kpi.beans.JBITextArea.class, "isFocusCycleRoot", "setFocusCycleRoot" );
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", kpi.beans.JBITextArea.class, "getFocusCycleRootAncestor", null );
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", kpi.beans.JBITextArea.class, "getFocusListeners", null );
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", kpi.beans.JBITextArea.class, "isFocusOwner", null );
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", kpi.beans.JBITextArea.class, "isFocusTraversable", null );
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", kpi.beans.JBITextArea.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" );
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", kpi.beans.JBITextArea.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" );
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", kpi.beans.JBITextArea.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" );
            properties[PROPERTY_focusTraversalPolicyProvider] = new PropertyDescriptor ( "focusTraversalPolicyProvider", kpi.beans.JBITextArea.class, "isFocusTraversalPolicyProvider", "setFocusTraversalPolicyProvider" );
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", kpi.beans.JBITextArea.class, "isFocusTraversalPolicySet", null );
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", kpi.beans.JBITextArea.class, "getFont", "setFont" );
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", kpi.beans.JBITextArea.class, "isFontSet", null );
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", kpi.beans.JBITextArea.class, "getForeground", "setForeground" );
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", kpi.beans.JBITextArea.class, "isForegroundSet", null );
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", kpi.beans.JBITextArea.class, "getGraphics", null );
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", kpi.beans.JBITextArea.class, "getGraphicsConfiguration", null );
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", kpi.beans.JBITextArea.class, "getHeight", null );
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", kpi.beans.JBITextArea.class, "getHierarchyBoundsListeners", null );
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", kpi.beans.JBITextArea.class, "getHierarchyListeners", null );
            properties[PROPERTY_highlighter] = new PropertyDescriptor ( "highlighter", kpi.beans.JBITextArea.class, "getHighlighter", "setHighlighter" );
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", kpi.beans.JBITextArea.class, "getIgnoreRepaint", "setIgnoreRepaint" );
            properties[PROPERTY_inheritsPopupMenu] = new PropertyDescriptor ( "inheritsPopupMenu", kpi.beans.JBITextArea.class, "getInheritsPopupMenu", "setInheritsPopupMenu" );
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", kpi.beans.JBITextArea.class, "getInputContext", null );
            properties[PROPERTY_inputMap] = new PropertyDescriptor ( "inputMap", kpi.beans.JBITextArea.class, "getInputMap", null );
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", kpi.beans.JBITextArea.class, "getInputMethodListeners", null );
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", kpi.beans.JBITextArea.class, "getInputMethodRequests", null );
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", kpi.beans.JBITextArea.class, "getInputVerifier", "setInputVerifier" );
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", kpi.beans.JBITextArea.class, "getInsets", null );
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", kpi.beans.JBITextArea.class, "getKeyListeners", null );
            properties[PROPERTY_keymap] = new PropertyDescriptor ( "keymap", kpi.beans.JBITextArea.class, "getKeymap", "setKeymap" );
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", kpi.beans.JBITextArea.class, "getLayout", "setLayout" );
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", kpi.beans.JBITextArea.class, "isLightweight", null );
            properties[PROPERTY_lineCount] = new PropertyDescriptor ( "lineCount", kpi.beans.JBITextArea.class, "getLineCount", null );
            properties[PROPERTY_lineEndOffset] = new IndexedPropertyDescriptor ( "lineEndOffset", kpi.beans.JBITextArea.class, null, null, "getLineEndOffset", null );
            properties[PROPERTY_lineOfOffset] = new IndexedPropertyDescriptor ( "lineOfOffset", kpi.beans.JBITextArea.class, null, null, "getLineOfOffset", null );
            properties[PROPERTY_lineStartOffset] = new IndexedPropertyDescriptor ( "lineStartOffset", kpi.beans.JBITextArea.class, null, null, "getLineStartOffset", null );
            properties[PROPERTY_lineWrap] = new PropertyDescriptor ( "lineWrap", kpi.beans.JBITextArea.class, "getLineWrap", "setLineWrap" );
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", kpi.beans.JBITextArea.class, "getLocale", "setLocale" );
            properties[PROPERTY_location] = new PropertyDescriptor ( "location", kpi.beans.JBITextArea.class, "getLocation", "setLocation" );
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", kpi.beans.JBITextArea.class, "getLocationOnScreen", null );
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", kpi.beans.JBITextArea.class, "isManagingFocus", null );
            properties[PROPERTY_margin] = new PropertyDescriptor ( "margin", kpi.beans.JBITextArea.class, "getMargin", "setMargin" );
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", kpi.beans.JBITextArea.class, "getMaximumSize", "setMaximumSize" );
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", kpi.beans.JBITextArea.class, "isMaximumSizeSet", null );
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", kpi.beans.JBITextArea.class, "getMinimumSize", "setMinimumSize" );
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", kpi.beans.JBITextArea.class, "isMinimumSizeSet", null );
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", kpi.beans.JBITextArea.class, "getMouseListeners", null );
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", kpi.beans.JBITextArea.class, "getMouseMotionListeners", null );
            properties[PROPERTY_mousePosition] = new PropertyDescriptor ( "mousePosition", kpi.beans.JBITextArea.class, "getMousePosition", null );
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", kpi.beans.JBITextArea.class, "getMouseWheelListeners", null );
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", kpi.beans.JBITextArea.class, "getName", "setName" );
            properties[PROPERTY_navigationFilter] = new PropertyDescriptor ( "navigationFilter", kpi.beans.JBITextArea.class, "getNavigationFilter", "setNavigationFilter" );
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", kpi.beans.JBITextArea.class, "getNextFocusableComponent", "setNextFocusableComponent" );
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", kpi.beans.JBITextArea.class, "isOpaque", "setOpaque" );
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", kpi.beans.JBITextArea.class, "isOptimizedDrawingEnabled", null );
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", kpi.beans.JBITextArea.class, "isPaintingTile", null );
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", kpi.beans.JBITextArea.class, "getParent", null );
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", kpi.beans.JBITextArea.class, "getPeer", null );
            properties[PROPERTY_preferredScrollableViewportSize] = new PropertyDescriptor ( "preferredScrollableViewportSize", kpi.beans.JBITextArea.class, "getPreferredScrollableViewportSize", null );
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", kpi.beans.JBITextArea.class, "getPreferredSize", "setPreferredSize" );
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", kpi.beans.JBITextArea.class, "isPreferredSizeSet", null );
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", kpi.beans.JBITextArea.class, "getPropertyChangeListeners", null );
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", kpi.beans.JBITextArea.class, "getRegisteredKeyStrokes", null );
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", kpi.beans.JBITextArea.class, "isRequestFocusEnabled", "setRequestFocusEnabled" );
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", kpi.beans.JBITextArea.class, "getRootPane", null );
            properties[PROPERTY_rows] = new PropertyDescriptor ( "rows", kpi.beans.JBITextArea.class, "getRows", "setRows" );
            properties[PROPERTY_scrollableTracksViewportHeight] = new PropertyDescriptor ( "scrollableTracksViewportHeight", kpi.beans.JBITextArea.class, "getScrollableTracksViewportHeight", null );
            properties[PROPERTY_scrollableTracksViewportWidth] = new PropertyDescriptor ( "scrollableTracksViewportWidth", kpi.beans.JBITextArea.class, "getScrollableTracksViewportWidth", null );
            properties[PROPERTY_selectedText] = new PropertyDescriptor ( "selectedText", kpi.beans.JBITextArea.class, "getSelectedText", null );
            properties[PROPERTY_selectedTextColor] = new PropertyDescriptor ( "selectedTextColor", kpi.beans.JBITextArea.class, "getSelectedTextColor", "setSelectedTextColor" );
            properties[PROPERTY_selectionColor] = new PropertyDescriptor ( "selectionColor", kpi.beans.JBITextArea.class, "getSelectionColor", "setSelectionColor" );
            properties[PROPERTY_selectionEnd] = new PropertyDescriptor ( "selectionEnd", kpi.beans.JBITextArea.class, "getSelectionEnd", "setSelectionEnd" );
            properties[PROPERTY_selectionStart] = new PropertyDescriptor ( "selectionStart", kpi.beans.JBITextArea.class, "getSelectionStart", "setSelectionStart" );
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", kpi.beans.JBITextArea.class, "isShowing", null );
            properties[PROPERTY_size] = new PropertyDescriptor ( "size", kpi.beans.JBITextArea.class, "getSize", "setSize" );
            properties[PROPERTY_tabSize] = new PropertyDescriptor ( "tabSize", kpi.beans.JBITextArea.class, "getTabSize", "setTabSize" );
            properties[PROPERTY_text] = new PropertyDescriptor ( "text", kpi.beans.JBITextArea.class, "getText", "setText" );
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", kpi.beans.JBITextArea.class, "getToolkit", null );
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", kpi.beans.JBITextArea.class, "getToolTipText", "setToolTipText" );
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", kpi.beans.JBITextArea.class, "getTopLevelAncestor", null );
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", kpi.beans.JBITextArea.class, "getTransferHandler", "setTransferHandler" );
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", kpi.beans.JBITextArea.class, "getTreeLock", null );
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", kpi.beans.JBITextArea.class, "getUI", "setUI" );
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", kpi.beans.JBITextArea.class, "getUIClassID", null );
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", kpi.beans.JBITextArea.class, "isValid", null );
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", kpi.beans.JBITextArea.class, "isValidateRoot", null );
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", kpi.beans.JBITextArea.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" );
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", kpi.beans.JBITextArea.class, "getVetoableChangeListeners", null );
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", kpi.beans.JBITextArea.class, "isVisible", "setVisible" );
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", kpi.beans.JBITextArea.class, "getVisibleRect", null );
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", kpi.beans.JBITextArea.class, "getWidth", null );
            properties[PROPERTY_wrapStyleWord] = new PropertyDescriptor ( "wrapStyleWord", kpi.beans.JBITextArea.class, "getWrapStyleWord", "setWrapStyleWord" );
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", kpi.beans.JBITextArea.class, "getX", null );
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", kpi.beans.JBITextArea.class, "getY", null );
            properties[PROPERTY_maxLength] = new PropertyDescriptor ( "maxLength", kpi.beans.JBITextArea.class, "getMaxLength", "setMaxLength" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Properties
	
	// Here you can add code for customizing the properties array.
	
        return properties;         }//GEN-LAST:Properties
    
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_ancestorListener = 0;
    private static final int EVENT_caretListener = 1;
    private static final int EVENT_componentListener = 2;
    private static final int EVENT_containerListener = 3;
    private static final int EVENT_focusListener = 4;
    private static final int EVENT_hierarchyBoundsListener = 5;
    private static final int EVENT_hierarchyListener = 6;
    private static final int EVENT_inputMethodListener = 7;
    private static final int EVENT_keyListener = 8;
    private static final int EVENT_mouseListener = 9;
    private static final int EVENT_mouseMotionListener = 10;
    private static final int EVENT_mouseWheelListener = 11;
    private static final int EVENT_propertyChangeListener = 12;
    private static final int EVENT_vetoableChangeListener = 13;

    // EventSet array
    /*lazy EventSetDescriptor*/
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[14];
    
            try {
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( kpi.beans.JBITextArea.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorAdded", "ancestorMoved", "ancestorRemoved"}, "addAncestorListener", "removeAncestorListener" );
            eventSets[EVENT_caretListener] = new EventSetDescriptor ( kpi.beans.JBITextArea.class, "caretListener", javax.swing.event.CaretListener.class, new String[] {"caretUpdate"}, "addCaretListener", "removeCaretListener" );
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( kpi.beans.JBITextArea.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentHidden", "componentMoved", "componentResized", "componentShown"}, "addComponentListener", "removeComponentListener" );
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( kpi.beans.JBITextArea.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" );
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( kpi.beans.JBITextArea.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusGained", "focusLost"}, "addFocusListener", "removeFocusListener" );
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( kpi.beans.JBITextArea.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" );
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( kpi.beans.JBITextArea.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" );
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( kpi.beans.JBITextArea.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" );
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( kpi.beans.JBITextArea.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyReleased", "keyTyped"}, "addKeyListener", "removeKeyListener" );
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( kpi.beans.JBITextArea.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mouseEntered", "mouseExited", "mousePressed", "mouseReleased"}, "addMouseListener", "removeMouseListener" );
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( kpi.beans.JBITextArea.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" );
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( kpi.beans.JBITextArea.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" );
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( kpi.beans.JBITextArea.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" );
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( kpi.beans.JBITextArea.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Events
	
	// Here you can add code for customizing the event sets array.
	
        return eventSets;         }//GEN-LAST:Events
    
    // Method identifiers//GEN-FIRST:Methods
    private static final int METHOD_action0 = 0;
    private static final int METHOD_add1 = 1;
    private static final int METHOD_addKeymap2 = 2;
    private static final int METHOD_addNotify3 = 3;
    private static final int METHOD_addPropertyChangeListener4 = 4;
    private static final int METHOD_append5 = 5;
    private static final int METHOD_applyComponentOrientation6 = 6;
    private static final int METHOD_areFocusTraversalKeysSet7 = 7;
    private static final int METHOD_bounds8 = 8;
    private static final int METHOD_checkImage9 = 9;
    private static final int METHOD_computeVisibleRect10 = 10;
    private static final int METHOD_contains11 = 11;
    private static final int METHOD_copy12 = 12;
    private static final int METHOD_countComponents13 = 13;
    private static final int METHOD_createImage14 = 14;
    private static final int METHOD_createToolTip15 = 15;
    private static final int METHOD_createVolatileImage16 = 16;
    private static final int METHOD_cut17 = 17;
    private static final int METHOD_deliverEvent18 = 18;
    private static final int METHOD_disable19 = 19;
    private static final int METHOD_dispatchEvent20 = 20;
    private static final int METHOD_doLayout21 = 21;
    private static final int METHOD_enable22 = 22;
    private static final int METHOD_enableInputMethods23 = 23;
    private static final int METHOD_findComponentAt24 = 24;
    private static final int METHOD_firePropertyChange25 = 25;
    private static final int METHOD_getActionForKeyStroke26 = 26;
    private static final int METHOD_getBounds27 = 27;
    private static final int METHOD_getClientProperty28 = 28;
    private static final int METHOD_getComponentAt29 = 29;
    private static final int METHOD_getComponentZOrder30 = 30;
    private static final int METHOD_getConditionForKeyStroke31 = 31;
    private static final int METHOD_getDefaultLocale32 = 32;
    private static final int METHOD_getFontMetrics33 = 33;
    private static final int METHOD_getInsets34 = 34;
    private static final int METHOD_getKeymap35 = 35;
    private static final int METHOD_getListeners36 = 36;
    private static final int METHOD_getLocation37 = 37;
    private static final int METHOD_getMousePosition38 = 38;
    private static final int METHOD_getPopupLocation39 = 39;
    private static final int METHOD_getPropertyChangeListeners40 = 40;
    private static final int METHOD_getScrollableBlockIncrement41 = 41;
    private static final int METHOD_getScrollableUnitIncrement42 = 42;
    private static final int METHOD_getSize43 = 43;
    private static final int METHOD_getText44 = 44;
    private static final int METHOD_getToolTipLocation45 = 45;
    private static final int METHOD_getToolTipText46 = 46;
    private static final int METHOD_gotFocus47 = 47;
    private static final int METHOD_grabFocus48 = 48;
    private static final int METHOD_handleEvent49 = 49;
    private static final int METHOD_hasFocus50 = 50;
    private static final int METHOD_hide51 = 51;
    private static final int METHOD_imageUpdate52 = 52;
    private static final int METHOD_insert53 = 53;
    private static final int METHOD_insets54 = 54;
    private static final int METHOD_inside55 = 55;
    private static final int METHOD_invalidate56 = 56;
    private static final int METHOD_isAncestorOf57 = 57;
    private static final int METHOD_isFocusCycleRoot58 = 58;
    private static final int METHOD_isLightweightComponent59 = 59;
    private static final int METHOD_keyDown60 = 60;
    private static final int METHOD_keyUp61 = 61;
    private static final int METHOD_layout62 = 62;
    private static final int METHOD_list63 = 63;
    private static final int METHOD_loadKeymap64 = 64;
    private static final int METHOD_locate65 = 65;
    private static final int METHOD_location66 = 66;
    private static final int METHOD_lostFocus67 = 67;
    private static final int METHOD_minimumSize68 = 68;
    private static final int METHOD_modelToView69 = 69;
    private static final int METHOD_mouseDown70 = 70;
    private static final int METHOD_mouseDrag71 = 71;
    private static final int METHOD_mouseEnter72 = 72;
    private static final int METHOD_mouseExit73 = 73;
    private static final int METHOD_mouseMove74 = 74;
    private static final int METHOD_mouseUp75 = 75;
    private static final int METHOD_move76 = 76;
    private static final int METHOD_moveCaretPosition77 = 77;
    private static final int METHOD_nextFocus78 = 78;
    private static final int METHOD_paint79 = 79;
    private static final int METHOD_paintAll80 = 80;
    private static final int METHOD_paintComponents81 = 81;
    private static final int METHOD_paintImmediately82 = 82;
    private static final int METHOD_paste83 = 83;
    private static final int METHOD_postEvent84 = 84;
    private static final int METHOD_preferredSize85 = 85;
    private static final int METHOD_prepareImage86 = 86;
    private static final int METHOD_print87 = 87;
    private static final int METHOD_printAll88 = 88;
    private static final int METHOD_printComponents89 = 89;
    private static final int METHOD_putClientProperty90 = 90;
    private static final int METHOD_read91 = 91;
    private static final int METHOD_registerKeyboardAction92 = 92;
    private static final int METHOD_remove93 = 93;
    private static final int METHOD_removeAll94 = 94;
    private static final int METHOD_removeKeymap95 = 95;
    private static final int METHOD_removeNotify96 = 96;
    private static final int METHOD_removePropertyChangeListener97 = 97;
    private static final int METHOD_repaint98 = 98;
    private static final int METHOD_replaceRange99 = 99;
    private static final int METHOD_replaceSelection100 = 100;
    private static final int METHOD_requestDefaultFocus101 = 101;
    private static final int METHOD_requestFocus102 = 102;
    private static final int METHOD_requestFocusInWindow103 = 103;
    private static final int METHOD_resetKeyboardActions104 = 104;
    private static final int METHOD_reshape105 = 105;
    private static final int METHOD_resize106 = 106;
    private static final int METHOD_revalidate107 = 107;
    private static final int METHOD_scrollRectToVisible108 = 108;
    private static final int METHOD_select109 = 109;
    private static final int METHOD_selectAll110 = 110;
    private static final int METHOD_setBounds111 = 111;
    private static final int METHOD_setComponentZOrder112 = 112;
    private static final int METHOD_setDefaultLocale113 = 113;
    private static final int METHOD_show114 = 114;
    private static final int METHOD_size115 = 115;
    private static final int METHOD_toString116 = 116;
    private static final int METHOD_transferFocus117 = 117;
    private static final int METHOD_transferFocusBackward118 = 118;
    private static final int METHOD_transferFocusDownCycle119 = 119;
    private static final int METHOD_transferFocusUpCycle120 = 120;
    private static final int METHOD_unregisterKeyboardAction121 = 121;
    private static final int METHOD_update122 = 122;
    private static final int METHOD_updateUI123 = 123;
    private static final int METHOD_validate124 = 124;
    private static final int METHOD_viewToModel125 = 125;
    private static final int METHOD_write126 = 126;

    // Method array 
    /*lazy MethodDescriptor*/
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[127];
    
        try {
            methods[METHOD_action0] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_action0].setDisplayName ( "" );
            methods[METHOD_add1] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("add", new Class[] {java.awt.Component.class}));
            methods[METHOD_add1].setDisplayName ( "" );
            methods[METHOD_addKeymap2] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("addKeymap", new Class[] {java.lang.String.class, javax.swing.text.Keymap.class}));
            methods[METHOD_addKeymap2].setDisplayName ( "" );
            methods[METHOD_addNotify3] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("addNotify", new Class[] {}));
            methods[METHOD_addNotify3].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener4] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_addPropertyChangeListener4].setDisplayName ( "" );
            methods[METHOD_append5] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("append", new Class[] {java.lang.String.class}));
            methods[METHOD_append5].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation6] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class}));
            methods[METHOD_applyComponentOrientation6].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet7] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE}));
            methods[METHOD_areFocusTraversalKeysSet7].setDisplayName ( "" );
            methods[METHOD_bounds8] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("bounds", new Class[] {}));
            methods[METHOD_bounds8].setDisplayName ( "" );
            methods[METHOD_checkImage9] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage9].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect10] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_computeVisibleRect10].setDisplayName ( "" );
            methods[METHOD_contains11] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_contains11].setDisplayName ( "" );
            methods[METHOD_copy12] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("copy", new Class[] {}));
            methods[METHOD_copy12].setDisplayName ( "" );
            methods[METHOD_countComponents13] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("countComponents", new Class[] {}));
            methods[METHOD_countComponents13].setDisplayName ( "" );
            methods[METHOD_createImage14] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class}));
            methods[METHOD_createImage14].setDisplayName ( "" );
            methods[METHOD_createToolTip15] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("createToolTip", new Class[] {}));
            methods[METHOD_createToolTip15].setDisplayName ( "" );
            methods[METHOD_createVolatileImage16] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createVolatileImage16].setDisplayName ( "" );
            methods[METHOD_cut17] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("cut", new Class[] {}));
            methods[METHOD_cut17].setDisplayName ( "" );
            methods[METHOD_deliverEvent18] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_deliverEvent18].setDisplayName ( "" );
            methods[METHOD_disable19] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("disable", new Class[] {}));
            methods[METHOD_disable19].setDisplayName ( "" );
            methods[METHOD_dispatchEvent20] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class}));
            methods[METHOD_dispatchEvent20].setDisplayName ( "" );
            methods[METHOD_doLayout21] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("doLayout", new Class[] {}));
            methods[METHOD_doLayout21].setDisplayName ( "" );
            methods[METHOD_enable22] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("enable", new Class[] {}));
            methods[METHOD_enable22].setDisplayName ( "" );
            methods[METHOD_enableInputMethods23] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE}));
            methods[METHOD_enableInputMethods23].setDisplayName ( "" );
            methods[METHOD_findComponentAt24] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_findComponentAt24].setDisplayName ( "" );
            methods[METHOD_firePropertyChange25] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Boolean.TYPE, Boolean.TYPE}));
            methods[METHOD_firePropertyChange25].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke26] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getActionForKeyStroke26].setDisplayName ( "" );
            methods[METHOD_getBounds27] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_getBounds27].setDisplayName ( "" );
            methods[METHOD_getClientProperty28] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class}));
            methods[METHOD_getClientProperty28].setDisplayName ( "" );
            methods[METHOD_getComponentAt29] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getComponentAt29].setDisplayName ( "" );
            methods[METHOD_getComponentZOrder30] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getComponentZOrder", new Class[] {java.awt.Component.class}));
            methods[METHOD_getComponentZOrder30].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke31] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getConditionForKeyStroke31].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale32] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getDefaultLocale", new Class[] {}));
            methods[METHOD_getDefaultLocale32].setDisplayName ( "" );
            methods[METHOD_getFontMetrics33] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class}));
            methods[METHOD_getFontMetrics33].setDisplayName ( "" );
            methods[METHOD_getInsets34] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getInsets", new Class[] {java.awt.Insets.class}));
            methods[METHOD_getInsets34].setDisplayName ( "" );
            methods[METHOD_getKeymap35] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getKeymap", new Class[] {java.lang.String.class}));
            methods[METHOD_getKeymap35].setDisplayName ( "" );
            methods[METHOD_getListeners36] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getListeners", new Class[] {java.lang.Class.class}));
            methods[METHOD_getListeners36].setDisplayName ( "" );
            methods[METHOD_getLocation37] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_getLocation37].setDisplayName ( "" );
            methods[METHOD_getMousePosition38] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getMousePosition", new Class[] {Boolean.TYPE}));
            methods[METHOD_getMousePosition38].setDisplayName ( "" );
            methods[METHOD_getPopupLocation39] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getPopupLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getPopupLocation39].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners40] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class}));
            methods[METHOD_getPropertyChangeListeners40].setDisplayName ( "" );
            methods[METHOD_getScrollableBlockIncrement41] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getScrollableBlockIncrement", new Class[] {java.awt.Rectangle.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getScrollableBlockIncrement41].setDisplayName ( "" );
            methods[METHOD_getScrollableUnitIncrement42] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getScrollableUnitIncrement", new Class[] {java.awt.Rectangle.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getScrollableUnitIncrement42].setDisplayName ( "" );
            methods[METHOD_getSize43] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_getSize43].setDisplayName ( "" );
            methods[METHOD_getText44] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getText", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getText44].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation45] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipLocation45].setDisplayName ( "" );
            methods[METHOD_getToolTipText46] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipText46].setDisplayName ( "" );
            methods[METHOD_gotFocus47] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_gotFocus47].setDisplayName ( "" );
            methods[METHOD_grabFocus48] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("grabFocus", new Class[] {}));
            methods[METHOD_grabFocus48].setDisplayName ( "" );
            methods[METHOD_handleEvent49] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("handleEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_handleEvent49].setDisplayName ( "" );
            methods[METHOD_hasFocus50] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("hasFocus", new Class[] {}));
            methods[METHOD_hasFocus50].setDisplayName ( "" );
            methods[METHOD_hide51] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("hide", new Class[] {}));
            methods[METHOD_hide51].setDisplayName ( "" );
            methods[METHOD_imageUpdate52] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_imageUpdate52].setDisplayName ( "" );
            methods[METHOD_insert53] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("insert", new Class[] {java.lang.String.class, Integer.TYPE}));
            methods[METHOD_insert53].setDisplayName ( "" );
            methods[METHOD_insets54] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("insets", new Class[] {}));
            methods[METHOD_insets54].setDisplayName ( "" );
            methods[METHOD_inside55] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_inside55].setDisplayName ( "" );
            methods[METHOD_invalidate56] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("invalidate", new Class[] {}));
            methods[METHOD_invalidate56].setDisplayName ( "" );
            methods[METHOD_isAncestorOf57] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_isAncestorOf57].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot58] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class}));
            methods[METHOD_isFocusCycleRoot58].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent59] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class}));
            methods[METHOD_isLightweightComponent59].setDisplayName ( "" );
            methods[METHOD_keyDown60] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyDown60].setDisplayName ( "" );
            methods[METHOD_keyUp61] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyUp61].setDisplayName ( "" );
            methods[METHOD_layout62] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("layout", new Class[] {}));
            methods[METHOD_layout62].setDisplayName ( "" );
            methods[METHOD_list63] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE}));
            methods[METHOD_list63].setDisplayName ( "" );
            methods[METHOD_loadKeymap64] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("loadKeymap", new Class[] {javax.swing.text.Keymap.class, javax.swing.text.JTextComponent.KeyBinding[].class, javax.swing.Action[].class}));
            methods[METHOD_loadKeymap64].setDisplayName ( "" );
            methods[METHOD_locate65] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_locate65].setDisplayName ( "" );
            methods[METHOD_location66] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("location", new Class[] {}));
            methods[METHOD_location66].setDisplayName ( "" );
            methods[METHOD_lostFocus67] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_lostFocus67].setDisplayName ( "" );
            methods[METHOD_minimumSize68] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("minimumSize", new Class[] {}));
            methods[METHOD_minimumSize68].setDisplayName ( "" );
            methods[METHOD_modelToView69] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("modelToView", new Class[] {Integer.TYPE}));
            methods[METHOD_modelToView69].setDisplayName ( "" );
            methods[METHOD_mouseDown70] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDown70].setDisplayName ( "" );
            methods[METHOD_mouseDrag71] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDrag71].setDisplayName ( "" );
            methods[METHOD_mouseEnter72] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseEnter72].setDisplayName ( "" );
            methods[METHOD_mouseExit73] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseExit73].setDisplayName ( "" );
            methods[METHOD_mouseMove74] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseMove74].setDisplayName ( "" );
            methods[METHOD_mouseUp75] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseUp75].setDisplayName ( "" );
            methods[METHOD_move76] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_move76].setDisplayName ( "" );
            methods[METHOD_moveCaretPosition77] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("moveCaretPosition", new Class[] {Integer.TYPE}));
            methods[METHOD_moveCaretPosition77].setDisplayName ( "" );
            methods[METHOD_nextFocus78] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("nextFocus", new Class[] {}));
            methods[METHOD_nextFocus78].setDisplayName ( "" );
            methods[METHOD_paint79] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("paint", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paint79].setDisplayName ( "" );
            methods[METHOD_paintAll80] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintAll80].setDisplayName ( "" );
            methods[METHOD_paintComponents81] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintComponents81].setDisplayName ( "" );
            methods[METHOD_paintImmediately82] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_paintImmediately82].setDisplayName ( "" );
            methods[METHOD_paste83] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("paste", new Class[] {}));
            methods[METHOD_paste83].setDisplayName ( "" );
            methods[METHOD_postEvent84] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("postEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_postEvent84].setDisplayName ( "" );
            methods[METHOD_preferredSize85] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("preferredSize", new Class[] {}));
            methods[METHOD_preferredSize85].setDisplayName ( "" );
            methods[METHOD_prepareImage86] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage86].setDisplayName ( "" );
            methods[METHOD_print87] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("print", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_print87].setDisplayName ( "" );
            methods[METHOD_printAll88] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("printAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printAll88].setDisplayName ( "" );
            methods[METHOD_printComponents89] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printComponents89].setDisplayName ( "" );
            methods[METHOD_putClientProperty90] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class}));
            methods[METHOD_putClientProperty90].setDisplayName ( "" );
            methods[METHOD_read91] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("read", new Class[] {java.io.Reader.class, java.lang.Object.class}));
            methods[METHOD_read91].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction92] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction92].setDisplayName ( "" );
            methods[METHOD_remove93] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("remove", new Class[] {Integer.TYPE}));
            methods[METHOD_remove93].setDisplayName ( "" );
            methods[METHOD_removeAll94] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("removeAll", new Class[] {}));
            methods[METHOD_removeAll94].setDisplayName ( "" );
            methods[METHOD_removeKeymap95] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("removeKeymap", new Class[] {java.lang.String.class}));
            methods[METHOD_removeKeymap95].setDisplayName ( "" );
            methods[METHOD_removeNotify96] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("removeNotify", new Class[] {}));
            methods[METHOD_removeNotify96].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener97] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_removePropertyChangeListener97].setDisplayName ( "" );
            methods[METHOD_repaint98] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint98].setDisplayName ( "" );
            methods[METHOD_replaceRange99] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("replaceRange", new Class[] {java.lang.String.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_replaceRange99].setDisplayName ( "" );
            methods[METHOD_replaceSelection100] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("replaceSelection", new Class[] {java.lang.String.class}));
            methods[METHOD_replaceSelection100].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus101] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("requestDefaultFocus", new Class[] {}));
            methods[METHOD_requestDefaultFocus101].setDisplayName ( "" );
            methods[METHOD_requestFocus102] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("requestFocus", new Class[] {}));
            methods[METHOD_requestFocus102].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow103] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("requestFocusInWindow", new Class[] {}));
            methods[METHOD_requestFocusInWindow103].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions104] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("resetKeyboardActions", new Class[] {}));
            methods[METHOD_resetKeyboardActions104].setDisplayName ( "" );
            methods[METHOD_reshape105] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_reshape105].setDisplayName ( "" );
            methods[METHOD_resize106] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_resize106].setDisplayName ( "" );
            methods[METHOD_revalidate107] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("revalidate", new Class[] {}));
            methods[METHOD_revalidate107].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible108] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_scrollRectToVisible108].setDisplayName ( "" );
            methods[METHOD_select109] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("select", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_select109].setDisplayName ( "" );
            methods[METHOD_selectAll110] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("selectAll", new Class[] {}));
            methods[METHOD_selectAll110].setDisplayName ( "" );
            methods[METHOD_setBounds111] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setBounds111].setDisplayName ( "" );
            methods[METHOD_setComponentZOrder112] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("setComponentZOrder", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_setComponentZOrder112].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale113] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class}));
            methods[METHOD_setDefaultLocale113].setDisplayName ( "" );
            methods[METHOD_show114] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("show", new Class[] {}));
            methods[METHOD_show114].setDisplayName ( "" );
            methods[METHOD_size115] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("size", new Class[] {}));
            methods[METHOD_size115].setDisplayName ( "" );
            methods[METHOD_toString116] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("toString", new Class[] {}));
            methods[METHOD_toString116].setDisplayName ( "" );
            methods[METHOD_transferFocus117] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("transferFocus", new Class[] {}));
            methods[METHOD_transferFocus117].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward118] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("transferFocusBackward", new Class[] {}));
            methods[METHOD_transferFocusBackward118].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle119] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("transferFocusDownCycle", new Class[] {}));
            methods[METHOD_transferFocusDownCycle119].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle120] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("transferFocusUpCycle", new Class[] {}));
            methods[METHOD_transferFocusUpCycle120].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction121] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_unregisterKeyboardAction121].setDisplayName ( "" );
            methods[METHOD_update122] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("update", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_update122].setDisplayName ( "" );
            methods[METHOD_updateUI123] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("updateUI", new Class[] {}));
            methods[METHOD_updateUI123].setDisplayName ( "" );
            methods[METHOD_validate124] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("validate", new Class[] {}));
            methods[METHOD_validate124].setDisplayName ( "" );
            methods[METHOD_viewToModel125] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("viewToModel", new Class[] {java.awt.Point.class}));
            methods[METHOD_viewToModel125].setDisplayName ( "" );
            methods[METHOD_write126] = new MethodDescriptor ( kpi.beans.JBITextArea.class.getMethod("write", new Class[] {java.io.Writer.class}));
            methods[METHOD_write126].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
	
	// Here you can add code for customizing the methods array.
	
        return methods;         }//GEN-LAST:Methods
    
    
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
    
    
//GEN-FIRST:Superclass
    
    // Here you can add code for customizing the Superclass BeanInfo.
    
//GEN-LAST:Superclass
    
    /**
     * Gets the bean's <code>BeanDescriptor</code>s.
     *
     * @return BeanDescriptor describing the editable
     * properties of this bean.  May return null if the
     * information should be obtained by automatic analysis.
     */
    public BeanDescriptor getBeanDescriptor() {
	return getBdescriptor();
    }
    
    /**
     * Gets the bean's <code>PropertyDescriptor</code>s.
     *
     * @return An array of PropertyDescriptors describing the editable
     * properties supported by this bean.  May return null if the
     * information should be obtained by automatic analysis.
     * <p>
     * If a property is indexed, then its entry in the result array will
     * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
     * A client of getPropertyDescriptors can use "instanceof" to check
     * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
     */
    public PropertyDescriptor[] getPropertyDescriptors() {
	return getPdescriptor();
    }
    
    /**
     * Gets the bean's <code>EventSetDescriptor</code>s.
     *
     * @return  An array of EventSetDescriptors describing the kinds of
     * events fired by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public EventSetDescriptor[] getEventSetDescriptors() {
	return getEdescriptor();
    }
    
    /**
     * Gets the bean's <code>MethodDescriptor</code>s.
     *
     * @return  An array of MethodDescriptors describing the methods
     * implemented by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public MethodDescriptor[] getMethodDescriptors() {
	return getMdescriptor();
    }
    
    /**
     * A bean may have a "default" property that is the property that will
     * mostly commonly be initially chosen for update by human's who are
     * customizing the bean.
     * @return  Index of default property in the PropertyDescriptor array
     * 		returned by getPropertyDescriptors.
     * <P>	Returns -1 if there is no default property.
     */
    public int getDefaultPropertyIndex() {
	return defaultPropertyIndex;
    }
    
    /**
     * A bean may have a "default" event that is the event that will
     * mostly commonly be used by human's when using the bean.
     * @return Index of default event in the EventSetDescriptor array
     *		returned by getEventSetDescriptors.
     * <P>	Returns -1 if there is no default event.
     */
    public int getDefaultEventIndex() {
	return defaultEventIndex;
    }
}

