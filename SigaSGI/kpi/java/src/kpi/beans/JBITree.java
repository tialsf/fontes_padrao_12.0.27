package kpi.beans;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import kpi.core.KpiDataController;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiTreeNode;
import kpi.swing.kpiCreateTree;
import kpi.xml.BIXMLAttributes;
import kpi.xml.BIXMLVector;

/**
 * @author Valdiney V GOMES
 */
public class JBITree extends javax.swing.JDialog {

    protected KpiDataController dataController = KpiStaticReferences.getKpiDataController();
    protected kpiCreateTree criaArvore = new kpiCreateTree();
    protected final boolean root;
    private final KpiDefaultFrameBehavior event;
    private final JComboBox combo;
    private final BIXMLVector vctCombo;
    private final BIXMLVector vctTree;
    private final boolean linhaZero;

    public JBITree(JComboBox combo, BIXMLVector vetor, KpiDefaultFrameBehavior event, boolean root, boolean linhaZero, BIXMLAttributes attrFilter) {
        super();
        initComponents();

        this.event = event;
        this.combo = combo;
        this.vctCombo = vetor;
        this.root = root;
        this.linhaZero = linhaZero;
        this.vctTree = null;
        this.criaArvore.setAttributeFilter(attrFilter); 
        this.setTitle(DefineBarraTitulo(vctCombo.getMainTag()));

        configuraArvore();
    }

    public JBITree(JComboBox combo, BIXMLVector vctCombo, BIXMLVector vecTree, KpiDefaultFrameBehavior event, boolean root, boolean linhaZero, BIXMLAttributes attrFilter) {
        super();
        initComponents();

        this.event = event;
        this.combo = combo;
        this.vctCombo = vctCombo;
        this.root = root;
        this.linhaZero = linhaZero;
        this.vctTree = vecTree;
        this.criaArvore.setAttributeFilter(attrFilter);
        this.setTitle(DefineBarraTitulo(vctCombo.getMainTag()));

        configuraArvore();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlCenter = new javax.swing.JPanel();
        scrScorecard = new javax.swing.JScrollPane();
        arvore = new javax.swing.JTree();
        btnLimpar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setIconImages(null);
        setModal(true);
        setResizable(false);

        pnlCenter.setLayout(new java.awt.BorderLayout());

        scrScorecard.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("Aguarde...");
        arvore.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        arvore.setToolTipText("");
        arvore.setExpandsSelectedPaths(false);
        arvore.addTreeWillExpandListener(new javax.swing.event.TreeWillExpandListener() {
            public void treeWillCollapse(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {
            }
            public void treeWillExpand(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {
                arvoreTreeWillExpand(evt);
            }
        });
        arvore.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                arvoreMouseClicked(evt);
            }
        });
        scrScorecard.setViewportView(arvore);

        pnlCenter.add(scrScorecard, java.awt.BorderLayout.CENTER);

        btnLimpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnLimpar.setText("Limpar Conte�do");
        btnLimpar.setToolTipText("Limpa o conte�do presente no campo.");
        btnLimpar.setBorder(null);
        btnLimpar.setFocusable(false);
        btnLimpar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnLimpar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnLimpar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnLimparMouseClicked(evt);
            }
        });
        pnlCenter.add(btnLimpar, java.awt.BorderLayout.PAGE_END);

        getContentPane().add(pnlCenter, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Gerencia a montagem da �rvore de scorecards.
     */
    protected void configuraArvore() {
        arvore.removeAll();
        arvore.setCellRenderer(new KpiScoreCardTreeCellRenderer(KpiStaticReferences.getKpiImageResources()));
        arvore.setModel(new DefaultTreeModel(criaArvore.createTree(vctTree)));

        if (!root) {
            DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) arvore.getPathForRow(0).getLastPathComponent();

            Object userObject = oMutTreeNode.getUserObject();

            if (userObject instanceof KpiTreeNode) {
                KpiTreeNode oKpiTreeNode = (KpiTreeNode) userObject;
                oKpiTreeNode.setEnable(false);
            }
        }
    }

    /**
     * Responde ao evento de expandir um node da tree.
     * @param evt
     * @throws javax.swing.tree.ExpandVetoException
     */
    protected void onTreeWillExpand(javax.swing.event.TreeExpansionEvent evt) throws javax.swing.tree.ExpandVetoException {
    }

    /**
     * Responde ao evento de expandir um node da tree.
     * @param evt
     * @throws javax.swing.tree.ExpandVetoException
     */
    private void arvoreTreeWillExpand(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {//GEN-FIRST:event_arvoreTreeWillExpand
        onTreeWillExpand(evt);
}//GEN-LAST:event_arvoreTreeWillExpand
    /**
     * Responde a evento de realizar duplo clique em um node da tree.
     * @param evt
     */
    private void arvoreMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_arvoreMouseClicked
        if (evt.getClickCount() == 2) {
            DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) arvore.getLastSelectedPathComponent();

            if (oMutTreeNode != null) {
                Object userObject = oMutTreeNode.getUserObject();

                if (userObject instanceof KpiTreeNode) {
                    KpiTreeNode oKpiTreeNode = (KpiTreeNode) userObject;

                    if (oKpiTreeNode.isEnable()) {

                        if (oKpiTreeNode.getID().equals("0")) {
                            event.selectComboItem(combo, 1, linhaZero);
                            arvore.expandRow(0);
                        } else {
                            event.selectComboItem(combo, event.getComboItem(vctCombo, oKpiTreeNode.getID(), linhaZero), false);
                        }

                        this.setVisible(false);
                        this.dispose();
                    }
                }
            }
        }
}//GEN-LAST:event_arvoreMouseClicked

    private String DefineBarraTitulo(String mainTag ){
        String titulo = "";
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        ImageIcon icone = new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_arvore.gif"));  
        
        if (mainTag.trim() == "SCORECARDS"){
            titulo = bundle.getString("KpiJBITree_00001");
        }
        
        if (mainTag.trim() == "RESPONSAVEIS"){
            titulo = bundle.getString("KpiJBITree_00002");
        }
        
        setIconImage(icone.getImage()); 
        
        return titulo;
    }
    
    private void btnLimparMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLimparMouseClicked
        event.selectComboItem(combo, 0, linhaZero);
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_btnLimparMouseClicked
    /**
     * 
     * @param evt 
     */
    /**
     * 
     * @param evt 
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JTree arvore;
    private javax.swing.JButton btnLimpar;
    private javax.swing.JPanel pnlCenter;
    private javax.swing.JScrollPane scrScorecard;
    // End of variables declaration//GEN-END:variables
}

class KpiScoreCardTreeCellRenderer extends kpi.swing.KpiTreeCellRenderer {

    private kpi.core.KpiImageResources myImageResources;

    /**
     * 
     * @param imageResources 
     */
    public KpiScoreCardTreeCellRenderer(kpi.core.KpiImageResources imageResources) {
        super(imageResources);
        setUseMyRenderer(true);
        myImageResources = imageResources;
    }

    /**
     * Renderiza o componente para definir a imagem de cada node.
     * @param value Node da �rvore.
     */
    @Override
    public void myImageRenderer(Object value) {
        DefaultMutableTreeNode nodeSelected = (DefaultMutableTreeNode) value;

        if (nodeSelected != null) {
            if (nodeSelected.getUserObject() instanceof KpiTreeNode) {
                KpiTreeNode kpiNode = (KpiTreeNode) nodeSelected.getUserObject();
                ImageIcon nodeImage = myImageResources.getImage(kpiNode.getIDimage());

                //Icones
                setOpenIcon(nodeImage);
                setLeafIcon(nodeImage);
                setIcon(nodeImage);
                setClosedIcon(nodeImage);

                setBackground(null);
                setBackgroundNonSelectionColor(null);
                setOpaque(false);

                if (kpiNode.isEnable()) {
                    setEnabled(true);
                } else {
                    setEnabled(false);
                }
            }
        }
    }
}