package kpi.beans;

import javax.swing.JComboBox;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiTreeNode;
import kpi.xml.BIXMLAttributes;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

/**
 *
 * @author Alexandre Silva
 */
public class JBITreeScorecard extends JBITree {

    public JBITreeScorecard(JComboBox combo, BIXMLVector vetor, KpiDefaultFrameBehavior event, boolean root, boolean linhaZero, BIXMLAttributes attrFilter) {
	super(combo, vetor, event, root, linhaZero, attrFilter);
    }

    @Override
    protected void configuraArvore() {
	arvore.removeAll();
	arvore.setCellRenderer(new KpiScoreCardTreeCellRenderer(KpiStaticReferences.getKpiImageResources()));
	arvore.setModel(new DefaultTreeModel(criaArvore.createTree(dataController.loadTree("SCORECARD"))));

	if (!root) {
	    DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) arvore.getPathForRow(0).getLastPathComponent();

	    Object userObject = oMutTreeNode.getUserObject();

	    if (userObject instanceof KpiTreeNode) {
		KpiTreeNode oKpiTreeNode = (KpiTreeNode) userObject;
		oKpiTreeNode.setEnable(false);
	    }
	}
    }

    @Override
    protected void onTreeWillExpand(javax.swing.event.TreeExpansionEvent evt) throws javax.swing.tree.ExpandVetoException {
	DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) evt.getPath().getLastPathComponent();
	StringBuilder parametros = new StringBuilder();

	if (oMutTreeNode != null) {
	    Object userObject = oMutTreeNode.getUserObject();

	    if (userObject instanceof KpiTreeNode) {
		KpiTreeNode oKpiTreeNode = (KpiTreeNode) userObject;

		parametros.append("LISTA_SCO_CHILD");
		parametros.append("|");
		parametros.append(oKpiTreeNode.getID());
		parametros.append("|");
		parametros.append("");
		parametros.append("|");
		parametros.append("T");

		BIXMLRecord recFilhos = dataController.listRecords("SCORECARD", parametros.toString());
		criaArvore.insertChild(recFilhos, oMutTreeNode);
	    }
	}
    }
}
