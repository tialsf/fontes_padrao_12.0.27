package kpi.beans;

import java.awt.Dimension;
import java.io.Serializable;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import kpi.core.KpiImageResources;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiDefaultFrameFunctions;
import kpi.xml.BIXMLAttributes;
import kpi.xml.BIXMLVector;

/**
 * @author Valdiney V GOMES
 */
public class JBITreeSelection extends JPanel implements Serializable {

    public static final int TYPE_SCORECARD = 0;
    public static final int TYPE_DEFAULT = 1;
    private JBITree arvore;
    private boolean root = true;
    private boolean linhaZero = true;
    private JComboBox combo;
    private BIXMLVector vctCombo;
    private BIXMLVector vctTree;
    private final KpiDefaultFrameBehavior event;
    private BIXMLAttributes attrFilter = new BIXMLAttributes();
    private int treeType = TYPE_SCORECARD;
    private boolean forceUpdate = false;

    public JBITreeSelection() {
        initComponents();
        tlbSelection.setVisible(true);
        event = new KpiDefaultFrameBehavior((KpiDefaultFrameFunctions) this.getParent());
    }

    public JBITreeSelection(int treeType) {
        this.treeType = treeType;
        initComponents();
        tlbSelection.setVisible(true);
        event = new KpiDefaultFrameBehavior((KpiDefaultFrameFunctions) this.getParent());
    }

    public void setEntitySelection(int entID) {
        switch (entID) {
            case KpiStaticReferences.CAD_ORGANIZACAO:
                this.attrFilter.set("IMAGE", KpiImageResources.KPI_IMG_ORGANIZACAO);
                break;

            case KpiStaticReferences.CAD_ESTRATEGIA:
                this.attrFilter.set("IMAGE", KpiImageResources.KPI_IMG_ESTRATEGIA);
                break;

            case KpiStaticReferences.CAD_PERSPECTIVA:
                this.attrFilter.set("IMAGE", KpiImageResources.KPI_IMG_PERSPECTIVA);
                break;

            case KpiStaticReferences.CAD_OBJETIVO:
                this.attrFilter.set("IMAGE", KpiImageResources.KPI_IMG_OBJETIVO);
                break;
        }
    }

    /**
     * Gerencia o posicionamento da �rvore de scorecards.
     */
    public void configuraComportamento() {
        if (super.isEnabled()) {
                if (arvore == null || forceUpdate) {
                switch (treeType) {
                    case TYPE_SCORECARD:
                        arvore = new JBITreeScorecard(combo, vctCombo, event, root, linhaZero, attrFilter);
                        break;
                    case TYPE_DEFAULT:
                        arvore = new JBITree(combo, vctCombo, vctTree, event, root, linhaZero, attrFilter);
                        break;
                }
            }

            configuraLocalizacao();
            arvore.setVisible(true);
        }
    }

    /**
     * Gerencia o posicionamento da Tree em rela��o ao TreeSelection.
     */
    private void configuraLocalizacao() {
        Dimension tela = getToolkit().getScreenSize();
        int largura = 600;
        int altura = 350;
        int posicaoX = (tela.width - largura) / 2;
        int posicaoY = (tela.height - altura) / 2;

        arvore.setLocation(posicaoX, posicaoY);
        arvore.setSize(largura, altura);
    }

    /**
     * Indica se a JBITree deve ser recarregada;
     *
     * @return
     */
    public boolean isForceUpdate() {
        return forceUpdate;
    }

    /**
     * Define se a JBITree deve ser recarregada;
     *
     * @param forceUpdate
     */
    public void setForceUpdate(boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    /**
     *
     * @return
     */
    public JComboBox getCombo() {
        return combo;
    }

    /**
     *
     * @param comboScorecard
     */
    public void setCombo(JComboBox comboScorecard) {
        if (this.combo != comboScorecard) {
            this.combo = comboScorecard;
        }
    }

    /**
     *
     * @return
     */
    public boolean isRoot() {
        return root;
    }

    /**
     *
     * @param root
     */
    public void setRoot(boolean root) {
        this.root = root;
    }

    /**
     * O vetor � usado para posicionar o item da combo a partir do item
     * selecionado na �rvore.
     *
     * @param vetor Vetor com os dados da combo
     */
    public void setVetor(BIXMLVector vetor) {
        this.vctCombo = vetor;
    }

    /**
     * Seta o XML com a �rvore para ser mostrada na Tree, quando o usu�rio fizer
     * o clique.
     *
     * @param recTree the recTree to set
     */
    public void setTree(BIXMLVector vecTree) {
        this.vctTree = vecTree;
    }

    /**
     *
     * @return
     */
    public boolean isLinhaZero() {
        return linhaZero;
    }

    /**
     *
     * @param linhaZero
     */
    public void setLinhaZero(boolean linhaZero) {
        this.linhaZero = linhaZero;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tlbSelection = new javax.swing.JToolBar();
        cmbArvore = new javax.swing.JButton();

        setOpaque(false);
        setPreferredSize(new java.awt.Dimension(25, 22));
        setLayout(new java.awt.BorderLayout());

        tlbSelection.setFloatable(false);
        tlbSelection.setOpaque(false);
        tlbSelection.setPreferredSize(new java.awt.Dimension(25, 22));

        cmbArvore.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_arvore.gif"))); // NOI18N
        cmbArvore.setFocusable(false);
        cmbArvore.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        cmbArvore.setOpaque(false);
        cmbArvore.setPreferredSize(new java.awt.Dimension(21, 21));
        cmbArvore.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        cmbArvore.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cmbArvoreMouseClicked(evt);
            }
        });
        cmbArvore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbArvoreActionPerformed(evt);
            }
        });
        tlbSelection.add(cmbArvore);

        add(tlbSelection, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Realiza a exibi��o da lista de scorecards.
     *
     * @param evt
     */
    private void cmbArvoreMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cmbArvoreMouseClicked
        configuraComportamento();
    }//GEN-LAST:event_cmbArvoreMouseClicked

    private void cmbArvoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbArvoreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbArvoreActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cmbArvore;
    private javax.swing.JToolBar tlbSelection;
    // End of variables declaration//GEN-END:variables
}

class TreeSelectionPopupMenuListener implements PopupMenuListener {

    private JBITreeSelection jTreeSel;

    public void setTreeSelecion(JBITreeSelection selection) {
        jTreeSel = selection;
    }

    @Override
    public void popupMenuWillBecomeVisible(PopupMenuEvent evt) {
        jTreeSel.configuraComportamento();
    }

    @Override
    public void popupMenuWillBecomeInvisible(PopupMenuEvent evt) {
    }

    @Override
    public void popupMenuCanceled(PopupMenuEvent evt) {
    }
}
