package kpi.beans;

import java.util.GregorianCalendar;
import java.util.Vector;
import javax.swing.*;
import java.awt.*;
import javax.swing.table.TableModel;
import kpi.xml.BIXMLRecord;
import pv.jfcx.JPVTable;

/**
 * @author Alexandre Silva  siga1776
 */
public class JBIXMLTable extends pv.jfcx.JPVTableView {

    public final static int KPI_BOOLEAN = 0;
    public final static int KPI_DATE = 1;
    public final static int KPI_FLOAT = 2;
    public final static int KPI_INT = 3;
    public final static int KPI_STRING = 4;
    public final static int KPI_PERCENT = 5;
    public final static int KPI_IMAGEM = 6;
    private String type = "";
    private boolean editable = false;
    private int floatPrecision = -1;

    public JBIXMLTable() {
	super();

	java.util.Locale defaultLocale = kpi.core.KpiStaticReferences.getKpiDefaultLocale();
	kpi.xml.BIXMLVector vector = new kpi.xml.BIXMLVector("INICIALIZACOES", "INICIALIZACAO");
	kpi.xml.BIXMLAttributes attr = new kpi.xml.BIXMLAttributes();

	if (defaultLocale == null) {
	    defaultLocale = new java.util.Locale("pt_BR");
	}

	attr.set("TIPO", "INICIALIZACOES");
	attr.set("TAG000", "COLUNA");
	attr.set("CAB000", java.util.ResourceBundle.getBundle("international", defaultLocale).getString("JBIXMLTable_00001"));
	attr.set("CLA000", JBIXMLTable.KPI_STRING);
	vector.setAttributes(attr);
	vector.addNewRecord().set("COLUNA", java.util.ResourceBundle.getBundle("international", defaultLocale).getString("JBIXMLTable_00002"));
	this.setLocale(defaultLocale);
	this.setDataSource(vector);
	pv.jfcx.JPVTable biTable = this.getTable();
	biTable.setSelectionMode(javax.swing.DefaultListSelectionModel.SINGLE_INTERVAL_SELECTION);
	biTable.setColumnSelectionAllowed(false);
	biTable.setAutoResizeMode(JPVTable.AUTO_RESIZE_ALL_COLUMNS);

	this.setFocusCellBackground(new java.awt.Color(212, 208, 200));
	this.setBorder(BorderFactory.createLineBorder(new Color(192, 192, 192)));
    }

    /**
     *
     * @param dataSource
     */
    public void setDataSource(kpi.xml.BIXMLVector dataSource) {
	pv.jfcx.JPVTable biTable = this.getTable();
	BITableModel tableModel = new BITableModel();
	type = dataSource.getAttributes().getString("TIPO");

	//Op��es do modelo.
	tableModel.setDataSource(dataSource);
	tableModel.setFloatPrecision(floatPrecision);
	tableModel.setRowTracking(true);

	//Op��es da tabela.
	biTable.setModel(tableModel);

	//Op��es da vis�o.
	this.setTable(biTable);
	this.setEditMode(3);

	//Implementa��o para usar um render especifico para ac�lula.
	new BICellRenderer(this);
    }

    public void setEnableSort(boolean enable){
	BITableModel tableModel = (BITableModel) getModel();
	int depthEnable = ! enable ? 0 : tableModel.getMaxSortDepth();
	tableModel.setMaxSortDepth(depthEnable);
    }


    /**
     * Retorna os valores da Tabela 
     * @return BIXMLVector com os registros da tabela
     */
    public kpi.xml.BIXMLVector getXMLData() {
	BITableModel tableModel = (BITableModel) getModel();
	return tableModel.getXMLData();
    }

    /***
     * Retorna os valores da Tabela 
     * @param onLineUpdate Indica se os dados do model deve ser reconstruido durante o retorno dos registros.
     * @return BIXMLVector com os registros da tabela
     */
    public kpi.xml.BIXMLVector getXMLData(boolean onLineUpdate) {
	BITableModel tableModel = (BITableModel) getModel();
	return tableModel.getXMLData(onLineUpdate);
    }

    /**
     *
     * @param p
     */
    public void setFloatPrecision(int p) {
	floatPrecision = p;
	if (this.getModel() instanceof BITableModel) {
	    ((BITableModel) this.getModel()).setFloatPrecision(p);
	}
    }

    /**
     *
     * @return
     */
    public int getFloatPrecision() {
	if (this.getModel() instanceof BITableModel) {
	    return ((BITableModel) this.getModel()).getFloatPrecision();
	} else {
	    return -1;
	}
    }

    /**
     *
     * @return
     */
    public String getType() {
	return String.valueOf(type);
    }

    /**
     *
     * @param edt
     */
    @Override
    public void setEditable(boolean edt) {
	editable = edt;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean getEditable() {
	return editable;
    }

    /**
     *
     * @param colIndex
     * @param strValor
     * @return
     */
    public boolean seekInColumn(int colIndex, String strValor) {
	boolean found = false;
	BITableModel tableModel = (BITableModel) this.getModel();
	Vector columns = tableModel.getColumns();
	Object objProc = strToObjectColumn(colIndex, strValor, columns);
	int rowIndex = 0;
	String tipoColuna = tableModel.getColumnType(colIndex);
	int colTipo = new Integer(tipoColuna).intValue();

	if (colTipo == KPI_STRING) {
	    rowIndex = tableModel.search(objProc, colIndex, 0, pv.util.PVConvert.STRING_CASE_OFF, true);
	} else {
	    rowIndex = tableModel.search(objProc, colIndex);
	}

	if (rowIndex >= 0) {
	    this.setFocusCell(rowIndex, colIndex);
	    found = true;
	} else {
	    this.setFocusCell(0, 0);
	}
	return found;
    }

    /**
     * Adiciona uma nova linha a planilha
     * @param record = Registro que sera inserido
     */
    public void addNewRecord(kpi.xml.BIXMLRecord record) {
	addNewRecord(record, -1);
    }

    /**
     * Adiciona uma nova linha a planilha
     * @param record = Registro que sera inserido
     * @param line = linha ontes sera inserido o registro.
     */
    public void addNewRecord(kpi.xml.BIXMLRecord record, int line) {
	BITableModel tableModel = (BITableModel) this.getModel();
	tableModel.setPosVec(new Vector());
	tableModel.addNewRecord(record, line);
	tableModel.setRowTracking(true);
    }

    /**
     * Remove a linha selecionada.
     */
    public void removeRecord() {
	BITableModel tableModel = (BITableModel) this.getModel();
	tableModel.removeRowRecord(getSelectedRow());
	tableModel.setPosVec(new Vector());
	tableModel.setRowTracking(true);
    }

    /**
     * Remove a linha indicada no parametro.
     * @param pos linha que sera removida.
     */
    public void removeRecord(int pos) {
	BITableModel tableModel = (BITableModel) this.getModel();
	tableModel.removeRowRecord(pos);
	tableModel.setPosVec(new Vector());
	tableModel.setRowTracking(true);
    }

    /**
     * Retorna a linha selecionada da tabela.
     * @return numero da linha selecionada.
     */
    public int getSelectedRow() {
	TableModel model = this.getModel();
	int rowSelected = -1;

	if (model != null && model instanceof BITableModel){
	    BITableModel tableModel = (BITableModel) model;
	    rowSelected = tableModel.getOriginalRow(this.getTable().getSelectedRow());
	}

	return rowSelected;
    }

    /**
     * Retorna um array com as linhas selecionadas.
     * @return array com as linhas selecionadas.
     */
    public int[] getSelectedRows() {
	BITableModel tableModel = (BITableModel) this.getModel();
	int[] rowsSelected = this.getTable().getSelectedRows();
	for (int i = 0; i < rowsSelected.length; i++) {
	    rowsSelected[i] = tableModel.getOriginalRow(rowsSelected[i]);
	}
	return rowsSelected;
    }

    /**
     * Retorna a coluna selecionada.
     * @return n�mero da coluna selecionada
     */
    public int getSelectedCol() {
	int colSelected = this.getTable().getSelectedColumn();
	return colSelected;
    }

    /**
     * Indica o tipo da colina
     * @param col N�mero da coluna.
     *
     * @return Tipo da coluna
     */
    public String getColumnType(int col) {
	BITableModel tableModel = (BITableModel) getModel();
	return tableModel.getColumnType(col);
    }

    /**
     *
     * @param col
     * @return
     */
    public String getColumnName(int col) {
	BITableModel tableModel = (BITableModel) getModel();
	return tableModel.getColumnName(col);
    }

    /**
     *
     * @param col
     * @return
     */
    public String getColumnTag(int col) {
	BITableModel tableModel = (BITableModel) getModel();
	return tableModel.getColumnTag(col);
    }

    /**
     *
     * @param row
     * @return
     */
    public int getOriginalRow(int row) {
	BITableModel tableModel = (BITableModel) getModel();
	return tableModel.getOriginalRow(row);
    }

    /**
     *
     * @param start
     * @param end
     * @param toIndex
     */
    public void moveRow(int start, int end, int toIndex) {
	BITableModel tableModel = (BITableModel) getModel();
	tableModel.moveRow(start, end, toIndex);
	tableModel.setPosVec(new Vector());
	tableModel.setRowTracking(true);
    }

    /**
     *
     * @param indexCol
     * @param strValor
     * @param columns
     * @return
     */
    public Object strToObjectColumn(int indexCol, String strValor, Vector columns) {
	Object objProc = null;
	BITableColumn column = (BITableColumn) columns.get(indexCol);

	switch (Integer.parseInt(column.getType())) {
	    case KPI_BOOLEAN:
		objProc = java.lang.Boolean.parseBoolean(strValor);
		break;
	    case KPI_FLOAT:
		objProc = new Double(strValor);
		break;
	    case KPI_STRING:
		objProc = strValor;
		break;
	    case KPI_INT:
		objProc = new Integer(strValor);
		break;
	    case KPI_DATE:
		break;
	    case KPI_PERCENT:
		objProc = strValor.concat("%");
	    case KPI_IMAGEM:
		int idImagem = new Integer(strValor).intValue();
		kpi.core.KpiImageResources kpiImageResources = new kpi.core.KpiImageResources();
		objProc = kpiImageResources.getImage(idImagem);
	}

	return objProc;
    }

    /**
     *
     * @param enabled
     */
    @Override
    public void setEnabled(boolean enabled) {
	this.getTable().setEnabled(enabled);
    }

    /**
     *
     * @param col
     */
    public void setSortColumn(int col) {
	BITableModel tableModel = (BITableModel) getModel();
	tableModel.setSortingAt(col, pv.jfcx.PVTableModel.PRIMARY, true, pv.jfcx.PVTableModel.AUTO);
	tableModel.sort();
    }

    /**
     *
     * @param col
     * @param lAscend
     */
    public void setSortColumn(int col, boolean lAscend) {
	BITableModel tableModel = (BITableModel) getModel();
	tableModel.setSortingAt(col, pv.jfcx.PVTableModel.PRIMARY, lAscend, pv.jfcx.PVTableModel.AUTO);
	tableModel.sort();
    }

    /**
     *
     * @param colName
     * @param criteria
     */
    public void setFilterData(String colName, String criteria) {
	BITableModel tableModel = (BITableModel) getModel();
	tableModel.setCriteria(colName, criteria, "=");
    }

    public BIXMLRecord getSelectedRecord() {
	BIXMLRecord recSelected = null;

	if (this.getModel() != null && this.getModel() instanceof BITableModel) {
	    BITableModel tableModel = (BITableModel) this.getModel();
	    recSelected = tableModel.getSelectedRecord(this.getTable().getSelectedRow());
	}

	return recSelected;
    }
}

/**
 *
 * @author Alexandre Silva  siga1776
 */
class BITableModel extends pv.jfcx.PVTableModel {

    private int floatPrecision = -1;
    private boolean editable = true;
    private java.util.HashMap indice = new java.util.HashMap();
    private kpi.util.KpiDateUtil convData = new kpi.util.KpiDateUtil();
    private boolean lFilter = false;
    private kpi.xml.BIXMLVector bckpRecords = null;
    Vector columns = new Vector();
    kpi.xml.BIXMLVector records = null;

    public BITableModel() {
	super();
    }

    /**
     *
     * @param col
     * @param criteria
     * @param operacao
     */
    public void setCriteria(String col, String criteria, String operacao) {
	boolean stDelete = true;

	if (lFilter == false) {
	    bckpRecords = records.clone2();
	    lFilter = true;
	} else {
	    setDataSource(bckpRecords.clone2());
	}


	if (criteria.trim().length() > 0) {
	    while (stDelete == true) {
		stDelete = delCriteriaByNomeCol(col, criteria, operacao);
	    }
	}

	fireTableDataChanged();
	setPosVec(new Vector());
	setRowTracking(true);
    }

    /**
     *
     * @param nameCol
     * @param criteria
     * @param operacao
     * @return
     */
    private boolean delCriteriaByNomeCol(String nameCol, String criteria, String operacao) {
	String valCelula = null;
	int numLinhas = getRowCount();

	for (int linha = 0; linha < numLinhas; linha++) {
	    valCelula = records.get(linha).getString(nameCol);

	    if (!valCelula.equals(criteria)) {
		records.remove(linha);
		getDataVector().removeElementAt(linha);
		return true;
	    }
	}

	return false;
    }

    /**
     * Retorna um vetor com as colunas.
     * @return
     */
    public Vector getColumns() {
	return columns;
    }

    /**
     *
     * @param records
     */
    public void setDataSource(kpi.xml.BIXMLVector records) {
	int numberOfColumns = 0;
	String attrAux = null;
	this.records = records;
	this.columns.removeAllElements();

	for (java.util.Iterator e = records.getAttributes().getKeyNames(); e.hasNext();) {
	    attrAux = (String) e.next();
	    if (attrAux.startsWith("TAG")) {

		numberOfColumns++;
	    }
	}

	for (int i = 0; i < numberOfColumns; i++) {
	    String zeros = "";

	    if (i <= 9) {
		zeros = "00";
	    } else if (i <= 99) {
		zeros = "0";
	    }

	    if (!records.getAttributes().contains("EDT" + zeros + i)) {
		BITableColumn newColumn = new BITableColumn(
			records.getAttributes().getString("TAG" + zeros + i),
			records.getAttributes().getString("CAB" + zeros + i),
			records.getAttributes().getString("CLA" + zeros + i));

		columns.add(newColumn);
	    } else {

		BITableColumn newColumn = new BITableColumn(
			records.getAttributes().getString("TAG" + zeros + i),
			records.getAttributes().getString("CAB" + zeros + i),
			records.getAttributes().getString("CLA" + zeros + i),
			records.getAttributes().getBoolean("EDT" + zeros + i),
			records.getAttributes().getBoolean("CUM" + zeros + i));

		columns.add(newColumn);
	    }
	}

	dataVector.clear();

	for (int row = 0; row < records.size(); row++) {
	    java.util.Vector v = new Vector();
	    dataVector.add(v);

	    for (int col = 0; col < columns.size(); col++) {
		BITableColumn column = (BITableColumn) columns.get(col);
		v.add(creatColumnObject(column, row));
	    }
	}

	fireTableDataChanged();
    }

    /**
     * Metodo para adicionar um �nico registro.
     * @param record
     */
    public void addNewRecord(kpi.xml.BIXMLRecord record) {
	addNewRecord(record, -1);
    }

    /**
     * Metodo para adicionar um �nico registro.
     * @param record
     */
    public void addNewRecord(kpi.xml.BIXMLRecord record, int pos) {
	int rowCount = this.records.size();
	java.util.Vector v = new Vector();

	if (pos == -1) {
	    this.records.add(record);
	} else {
	    this.records.add(record, pos);
	}

	if (pos == -1) {
	    dataVector.add(v);
	} else {
	    dataVector.add(pos, v);
	}

	//Adionando as colunas.
	for (int col = 0; col < columns.size(); col++) {
	    BITableColumn column = (BITableColumn) columns.get(col);
	    v.add(creatColumnObject(column, rowCount));
	}

	fireTableDataChanged();
    }

    /**
     * M�todo para remover um �nico registros.
     * @param row
     */
    public void removeRowRecord(int row) {
	if (row > -1) {
	    int actualRow = getOriginalRow(row);
	    this.records.remove(row);
	    getDataVector().removeElementAt(actualRow);
	    fireTableDataChanged();
	}
    }

    /**
     *
     * @param column
     * @param row
     * @return
     */
    private Object creatColumnObject(BITableColumn column, int row) {
	Object obj = "";

	switch (Integer.parseInt(column.getType())) {
	    case JBIXMLTable.KPI_BOOLEAN:
		obj = records.get(row).getBoolean(column.getTag());
		break;

	    case JBIXMLTable.KPI_FLOAT:
		obj = new Double(records.get(row).getDouble(column.getTag()));
		break;

	    case JBIXMLTable.KPI_STRING:
		obj = records.get(row).getString(column.getTag());
		break;

	    case JBIXMLTable.KPI_INT:
		obj = new Integer(records.get(row).getInt(column.getTag()));
		break;

	    case JBIXMLTable.KPI_DATE:
		java.util.GregorianCalendar gc;
		gc = records.get(row).getDate(column.getTag());

		if (gc != null) {
		    java.text.DateFormat dateFormatter;
		    dateFormatter = java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT);
		    obj = (String) dateFormatter.format(new java.util.Date(gc.get(GregorianCalendar.YEAR), gc.get(GregorianCalendar.MONTH), gc.get(GregorianCalendar.DAY_OF_MONTH)));
		} else {
		    obj = "  /  /    ";
		}

		break;
	    case JBIXMLTable.KPI_PERCENT:
		String valCap = records.get(row).getString(column.getTag());
		if (valCap.substring(valCap.length() - 1).equals("%")) {
		    obj = valCap;
		} else {
		    obj = valCap + "%";
		}
		break;
	    case JBIXMLTable.KPI_IMAGEM:
		int i = new Integer(records.get(row).getInt(column.getTag()));
		javax.swing.ImageIcon kpiImagem = new kpi.core.KpiImageResources().getImage(i);
		obj = kpiImagem;
		break;
	}

	return obj;
    }

    public kpi.xml.BIXMLVector getXMLData() {
	return getXMLData(true);
    }

    /**
     *
     * @return
     */
    public kpi.xml.BIXMLVector getXMLData(boolean onLineUpDate) {
	if (onLineUpDate) {
	    String s;
	    for (int row = 0; row < records.size(); row++) {
		int actualRow = getOriginalRow(row);
		java.util.Vector v = (Vector) dataVector.get(row);

		for (int col = 0; col < columns.size(); col++) {
		    Object obj = v.get(col);
		    BITableColumn column = (BITableColumn) columns.get(col);

		    switch (Integer.parseInt(column.getType())) {
			case JBIXMLTable.KPI_BOOLEAN:
			    Boolean b = (Boolean) obj;
			    records.get(actualRow).set(column.getTag(), b.booleanValue());
			    break;

			case JBIXMLTable.KPI_FLOAT:
			    Double d;
			    try {
				d = (Double) obj;
			    } catch (Exception e) {
				d = 0.0;
			    }

			    if (floatPrecision == -1) {
				if (d != null) {
				    records.get(actualRow).set(column.getTag(), d.doubleValue());
				} else {
				    setValueAt(0, actualRow, col);
				    records.get(actualRow).set(column.getTag(), 0.0);
				}
			    } else {
				String pattern = "##################.";
				for (int i = 0; i < floatPrecision; i++) {
				    pattern = pattern + "0";
				}
				java.text.DecimalFormat df = new java.text.DecimalFormat(pattern);
				if (d != null) {
				    records.get(actualRow).set(column.getTag(), d.doubleValue());
				} else {
				    records.get(actualRow).set(column.getTag(), 0);
				}
			    }
			    break;

			case JBIXMLTable.KPI_STRING:
			    s = (String) obj;
			    records.get(actualRow).set(column.getTag(), s);
			    break;

			case JBIXMLTable.KPI_INT:
			    Integer i = (Integer) obj;
			    records.get(actualRow).set(column.getTag(), i.intValue());
			    break;

			case JBIXMLTable.KPI_DATE:
			    if (obj instanceof java.util.Date) {
				java.util.Calendar calendar = java.util.Calendar.getInstance();
				calendar.setTime((java.util.Date) obj);
				s = convData.getCalendarString(calendar);
			    } else {
				s = (String) obj;
			    }

			    if (s == null) {
				records.get(actualRow).set(column.getTag(), "//");
			    } else {
				records.get(actualRow).set(column.getTag(), s);
			    }

			    break;
			case JBIXMLTable.KPI_PERCENT:
			    s = (String) obj;
			    records.get(actualRow).set(column.getTag(), s);
		    }
		}
	    }
	}
	return records;
    }

    /**
     *
     */
    void setaIndice() {
	for (int i = 0; i < records.size(); i++) {
	    kpi.xml.BIXMLRecord rec = records.get(i);
	    String chave = rec.getString("ID");
	    indice.put(chave, new Integer(i));
	}
    }

    /**
     *
     * @param index
     * @return
     */
    int getRowByIndex(String index) {
	Integer i = (Integer) indice.get(index);
	if (i != null) {
	    return (i.intValue());
	} else {
	    return 0;
	}
    }

    /**
     *
     * @param p
     */
    public void setFloatPrecision(int p) {
	floatPrecision = p;
    }

    /**
     *
     * @return
     */
    public int getFloatPrecision() {
	return floatPrecision;
    }

    /**
     *
     * @param edt
     */
    public void setEditable(boolean edt) {
	editable = edt;
    }

    /**
     *
     * @return
     */
    public boolean getEditable() {
	return editable;
    }

    /**
     *
     * @return
     */
    @Override
    public int getColumnCount() {
	int cols = 0;
	if (columns != null) {
	    cols = columns.size();
	}
	return cols;
    }

    /**
     *
     * @return
     */
    @Override
    public int getRowCount() {
	int rows = 0;
	if (records != null) {
	    rows = records.size();
	}
	return rows;
    }

    /**
     *
     * @param col
     * @return
     */
    @Override
    public String getColumnName(int col) {
	BITableColumn column = (BITableColumn) columns.get(col);
	return column.getHeader();
    }

    /**
     *
     * @param col
     * @return
     */
    public boolean getColumnCumulative(int col) {
	BITableColumn column = (BITableColumn) columns.get(col);
	return column.getCumulative();
    }

    /**
     *
     * @param col
     * @return
     */
    public String getColumnTag(int col) {
	BITableColumn column = (BITableColumn) columns.get(col);
	return column.getTag();
    }

    /**
     *
     * @param c
     * @return
     */
    @Override
    public Class getColumnClass(int c) {
	return getValueAt(0, c).getClass();
    }

    /**
     *
     * @param col
     * @return
     */
    public String getColumnType(int col) {
	BITableColumn column = (BITableColumn) columns.get(col);
	return column.getType();
    }

    /**
     *
     * @param row
     * @return
     */
    public String getRecordType(int row) {
	row = getOriginalRow(row);
	if (records.get(row).contains("TIPO")) {
	    return String.valueOf(records.get(row).getString("TIPO"));
	} else {
	    return String.valueOf(records.get(row).getTagName());
	}
    }

    /**
     *
     * @param row
     * @return
     */
    public String getRecordID(int row) {
	row = getOriginalRow(row);
	return String.valueOf(records.get(row).getString("ID"));
    }

    /**
     *
     * @param row
     * @return
     */
    public String getName(int row) {
	row = getOriginalRow(row);
	return String.valueOf(records.get(row).getString("NOME"));
    }

    /**
     *
     * @param row
     * @param col
     * @return
     */
    @Override
    public boolean isCellEditable(int row, int col) {
	row = getOriginalRow(row);
	if (editable) {
	    BITableColumn column = (BITableColumn) columns.get(col);
	    return column.getEditable();
	} else {
	    return false;
	}
    }

    /**
     *
     * @return
     */
    public kpi.xml.BIXMLVector getRecords() {
	return records;
    }

    public BIXMLRecord getSelectedRecord(int row) {
	row = getOriginalRow(row);
	return records.get(row);
    }
}

/**
 *
 * @author Alexandre Silva  siga1776
 */
class BITableColumn {

    private String tag = "";
    private String header = "";
    private String type = "";
    private boolean editable = false;
    private boolean cumulative = false;

    /**
     *
     * @param tag
     * @param header
     * @param type
     * @param editable
     * @param cumulative
     */
    public BITableColumn(String tag, String header, String type, boolean editable, boolean cumulative) {
	this.tag = String.valueOf(tag);
	this.header = String.valueOf(header);
	this.type = String.valueOf(type);
	this.editable = editable;
	this.cumulative = cumulative;
    }

    /**
     *
     * @param tag
     * @param header
     * @param type
     */
    public BITableColumn(String tag, String header, String type) {
	this.tag = String.valueOf(tag);
	this.header = String.valueOf(header);
	this.type = String.valueOf(type);
    }

    /**
     *
     * @param tagAux
     */
    public void setTag(String tagAux) {
	tag = String.valueOf(tagAux);
    }

    /**
     *
     * @return
     */
    public String getTag() {
	return String.valueOf(tag);
    }

    /**
     *
     * @param headerAux
     */
    public void setHeader(String headerAux) {
	header = String.valueOf(headerAux);
    }

    /**
     *
     * @return
     */
    public String getHeader() {
	return String.valueOf(header);
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
	return getHeader();
    }

    /**
     *
     * @param typeAux
     */
    public void setType(String typeAux) {
	type = String.valueOf(typeAux);
    }

    /**
     *
     * @return
     */
    public String getType() {
	return String.valueOf(type);
    }

    /**
     *
     * @param editableAux
     */
    public void setEditable(boolean editableAux) {
	editable = editableAux;
    }

    /**
     *
     * @return
     */
    public boolean getEditable() {
	return editable;
    }

    /**
     *
     * @param cumulativeAux
     */
    public void setCumulative(boolean cumulativeAux) {
	cumulative = cumulativeAux;
    }

    /**
     *
     * @return
     */
    public boolean getCumulative() {
	return cumulative;
    }
}

/**
 *
 * @author Alexandre Silva  siga1776
 */
class BICellRenderer extends javax.swing.table.DefaultTableCellRenderer implements javax.swing.table.TableCellRenderer {

    JBIXMLTable BIXMLTable = null;
    java.util.Locale defaultLocale = kpi.core.KpiStaticReferences.getKpiDefaultLocale();

    /**
     *
     */
    public BICellRenderer() {
	super();
	if (defaultLocale == null) {
	    defaultLocale = new java.util.Locale("pt_BR");
	}
    }

    /**
     *
     * @param table
     */
    public BICellRenderer(JBIXMLTable table) {
	super();
	BIXMLTable = table;

	try {
	    table.setRowHeight(20);
	    table.setHeaderHeight(20);
	    table.setGridColor(new Color(211, 211, 211));
	    table.setHeaderBorder(BorderFactory.createLineBorder(new Color(221, 221, 221)));
	    table.setHeaderBackground(new Color(223, 229, 243));
	} catch (Exception e) {
	}

	if (defaultLocale == null) {
	    defaultLocale = new java.util.Locale("pt_BR");
	}
    }

    /**
     *
     * @param table
     * @param value
     * @param isSelected
     * @param hasFocus
     * @param row
     * @param column
     * @return
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	BITableModel tableModel = (BITableModel) table.getModel();
	column = BIXMLTable.getColumn(column).getModelIndex();
	row = tableModel.getOriginalRow(row);
	BITableColumn coluna = (BITableColumn) tableModel.getColumns().get(column);
	int colType = Integer.parseInt(coluna.getType());

	if (colType == JBIXMLTable.KPI_FLOAT) {
	    Double dbValor = new Double(value.toString());
	    pv.jfcx.JPVNumeric oRender = new pv.jfcx.JPVNumeric();

	    if (dbValor < 0) {
		oRender.setBackground(new java.awt.Color(255, 102, 102));
	    } else {
		oRender.setBackground(new java.awt.Color(51, 225, 0));
	    }

	    oRender.setLocale(defaultLocale);
	    oRender.setValueType(6);
	    oRender.setValue(dbValor);
	    oRender.setAlignment(2);
	    oRender.setOdometer(1);
	    return oRender;
	}

	return this;
    }
}

/**
 *
 * @author Alexandre Silva  siga1776
 */
class BITableModelAdapter implements javax.swing.event.TableModelListener {

    @Override
    public void tableChanged(javax.swing.event.TableModelEvent e) {
    }
}
