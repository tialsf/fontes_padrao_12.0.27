/*
 * JTexturePanel.java
 *
 * Created on 21 de Maio de 2003, 14:02
 */
package kpi.beans;
import java.beans.*;

import java.awt.*;
import javax.swing.*;
/**
 *
 * @author  siga1728
 */
public class JTexturePanel extends javax.swing.JPanel {
    
    private ImageIcon imageTexture;
    
    /** Creates a new instance of JTexturePanel */
    public JTexturePanel() {
    }
    
    public void setImageTexture(Icon image) {
	imageTexture = (ImageIcon)image;
    }
    
    public Icon getImageTexture() {
	return (Icon)imageTexture;
    }
    
    public void paint(java.awt.Graphics g) {
	if(imageTexture == null) {
	    super.paint(g);
	} else {
	    int iw = imageTexture.getIconWidth();
	    int w = getWidth();
	    int ih = imageTexture.getIconHeight();
	    int h = getHeight();
	    
	    int widthTimes = 1+(int)w/iw;
	    int heightTimes = 1+(int)h/ih;
	    
	    for(int i=0; i<widthTimes; i++)
		for(int j=0; j<heightTimes; j++)
		    g.drawImage(((ImageIcon)imageTexture).getImage(), iw*i, ih*j, this);
	    super.paintChildren(g);
	}
    }
    
}
