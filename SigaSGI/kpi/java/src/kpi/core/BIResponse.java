/*
 * BIResponse.java
 *
 * Created on June 20, 2003, 10:31 AM
 */

package kpi.core;

import kpi.xml.*;

/**
 *
 * @author  siga1996
 */
public class BIResponse {
	
	private BIXMLVector vector = new BIXMLVector();
	
	/** Creates a new instance of BIResponse */
	public BIResponse( ) {
	}

	public void processResponse ( String xml ){
	    //long inicio = System.currentTimeMillis();
	    vector.parseXML( xml );
	    //System.out.println("BIResponser, processResponse, gasto no parse : "+ (System.currentTimeMillis()-inicio) + "ms");
	}
	
	public int getStatus(){
		return vector.get(0).getInt("STATUS");
	}
	
	public String getErrorMessage() {
		if ( !vector.get(0).getInternalAttributes("STATUS").contains("MSG") )
			return String.valueOf("");
		else
			return String.valueOf(
					vector.get(0).getInternalAttributes("STATUS").getString("MSG") );
	}

	public String getCmdAfterUpdate() {
		if ( !vector.get(0).getInternalAttributes("STATUS").contains("AFTER_UPDATE") )
			return String.valueOf("");
		else
			return String.valueOf(
					vector.get(0).getInternalAttributes("STATUS").getString("AFTER_UPDATE") );
	}
	
	public String getID() {
		return String.valueOf( vector.get(0).getString("ID") );
	}
		
	public BIXMLRecord getTree(){
		if ( vector.get(0).contains("ATUALIZACOES") )
			return vector.get(0).getBIXMLVector("ATUALIZACOES").get(0);
		else
			return null;
	}
	
	public BIXMLRecord getReturnedRecord(){
		return vector.get(0).getBIXMLVector("RETORNOS").get(0);
	}

	public BIXMLVector getReturnedVector(){
		return vector.get(0).getBIXMLVector("RETORNOS");
	}
}
