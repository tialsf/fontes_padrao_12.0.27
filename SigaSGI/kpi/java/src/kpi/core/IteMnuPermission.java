/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kpi.core;

import java.util.HashMap;

/**
 *
 * @author asilva
 */
public class IteMnuPermission {

    public static enum TipoPermissao {

	MANUTENCAO, VISUALIZACAO, INCLUSAO, ALTERACAO, EXCLUSAO
    }

    public static enum Permissao {

	NEGADO, PERMITIDO
    }
    private HashMap<TipoPermissao, Permissao> hashPermissao = new HashMap() ;

    /**
     * Set a permissao pelo uso do id de regra
     * @param permissaoID 1=Manutençao, 2=visualizacao, 3=inclusao, 4=alteracao, 5=exclusao
     * @param granted true para permitido
     */
    public void addPermissao(int permissaoID, boolean granted) {
	Permissao access = Permissao.NEGADO ;
	if (granted){
	    access = Permissao.PERMITIDO ;
	}
	hashPermissao.put(getTipoPermissaoByID(permissaoID), access);
    }

    /**
     * Dado um tipo de permissao, devolve o valor para ela.
     * @param tpPermissao
     * @return
     */
    public Permissao getPermissionValue(TipoPermissao tpPermissao) {
	return hashPermissao.get(tpPermissao);
    }

    private TipoPermissao getTipoPermissaoByID(int permissaoID){
	TipoPermissao retPermissao = TipoPermissao.MANUTENCAO;

	switch(permissaoID){
	    case 1://Campo IDOPERACAO
		retPermissao = TipoPermissao.MANUTENCAO;
		break;
	    case 2:
		retPermissao = TipoPermissao.VISUALIZACAO;
		break;
            case 3:
                retPermissao = TipoPermissao.INCLUSAO;
                break;
            case 4:
                retPermissao = TipoPermissao.ALTERACAO;
                break;
            case 5:
                retPermissao = TipoPermissao.EXCLUSAO;
                break;
	}
	
	return retPermissao;
    }
}