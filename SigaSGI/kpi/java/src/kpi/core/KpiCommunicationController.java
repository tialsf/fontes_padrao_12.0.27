/*
 * CommunicationController.java
 *
 * Created on June 25, 2003, 8:58 AM
 */
package kpi.core;

import kpi.applet.KpiMainPanel;
import kpi.net.*;
import kpi.xml.*;

/**
 *
 * @author  siga1996
 */
public class KpiCommunicationController {

    private BIHttpClient httpClient;

    public KpiCommunicationController() {
        httpClient = new BIHttpClient(getServer(), getSessionID());
    }

    public static String getSessionID() {
        if (kpi.core.KpiStaticReferences.getKpiExecutionType() == kpi.core.KpiStaticReferences.KPI_APLICATION) {
            return KpiStaticReferences.getKpiApplication().getSessionID();
        } else {
            return KpiStaticReferences.getKpiApplet().getSessionID();
        }
    }

    public static String getServer() {
        if (kpi.core.KpiStaticReferences.getKpiExecutionType() == kpi.core.KpiStaticReferences.KPI_APLICATION) {
            return KpiStaticReferences.getKpiApplication().getCodeBase() + "kpicore.apw";
        } else {
            return KpiStaticReferences.getKpiApplet().getCodeBase() + "kpicore.apw";
        }
    }

    public static String getLoginServer() {
        if (kpi.core.KpiStaticReferences.getKpiExecutionType() == kpi.core.KpiStaticReferences.KPI_APLICATION) {
            return KpiStaticReferences.getKpiApplication().getCodeBase() + "sgilogin.apw";
        } else {
            return KpiStaticReferences.getKpiApplet().getCodeBase() + "sgilogin.apw";
        }
    }

    public String getFile(String url) {
        String resposta = new String();
        kpi.core.KpiStaticReferences.getKpiMainPanel().setStatusBarCondition(
                KpiMainPanel.ST_CONNECTING);
        resposta = httpClient.doGet(url);
        kpi.core.KpiStaticReferences.getKpiMainPanel().setStatusBarCondition(
                KpiMainPanel.ST_IDLE);
        return resposta;
    }

    public String sendData(String xml) {
        String resposta = new String();
        kpi.core.KpiStaticReferences.getKpiMainPanel().setStatusBarCondition(
                KpiMainPanel.ST_CONNECTING);
        resposta = httpClient.doPost(xml);
        kpi.core.KpiStaticReferences.getKpiMainPanel().setStatusBarCondition(
                KpiMainPanel.ST_IDLE);
        return resposta;
    }

    public String sendData(String xml, boolean refreshStatusBar) {
        String resposta = new String();
        if (refreshStatusBar) {
            kpi.core.KpiStaticReferences.getKpiMainPanel().setStatusBarCondition(
                    KpiMainPanel.ST_CONNECTING);
        }

        resposta = httpClient.doPost(xml);

        if (refreshStatusBar) {
            kpi.core.KpiStaticReferences.getKpiMainPanel().setStatusBarCondition(
                    KpiMainPanel.ST_IDLE);
        }

        return resposta;
    }

    public String sendLogin(String username, String password) {
        String resposta = new String();
        resposta = httpClient.doLogin(getLoginServer(), username, password);
        return resposta;
    }

    public String createSession() {
        String resposta = new String();

        if (kpi.core.KpiStaticReferences.getKpiExecutionType() == kpi.core.KpiStaticReferences.KPI_APLICATION) {
            resposta = httpClient.doPost(KpiStaticReferences.getKpiApplication().getCodeBase() + "session.apw", "");
        } else {
            resposta = httpClient.doPost(KpiStaticReferences.getKpiApplet().getCodeBase() + "session.apw", "");
        }

        BIXMLRecord record = null;
        BIResponse response = new BIResponse();
        response.processResponse(resposta);

        record = response.getReturnedRecord();

        httpClient.setSessionID(record.getString("SESSIONID"));

        return resposta;
    }

    public BIHttpClient getHttpClient() {
        return httpClient;
    }
}
