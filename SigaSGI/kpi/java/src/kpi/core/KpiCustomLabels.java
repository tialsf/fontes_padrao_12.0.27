package kpi.core;

import java.util.ResourceBundle;

/**
 *
 * @author lucio.pelinson
 */
public class KpiCustomLabels {
    private String m_sco = "";
    private String m_real = "";
    private String m_meta = "";
    private String m_previa = "";
    private String m_tend = "";

    /**
     * @return the m_sco
     */
    public String getSco() {
	return getSco(null);
    }

    public String getSco(Integer entType) {
    	String scoName = this.m_sco;

	if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {

	    if (entType == null){
		scoName = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiCustomLabels_00001");
	    }else if (entType.intValue() == KpiStaticReferences.CAD_OBJETIVO){
		scoName = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiCustomLabels_00002");
	    }
	}
	
	return scoName;
    }

    /**
     * @param m_sco the m_sco to set
     */
    public void setSco(String m_sco) {
        this.m_sco = m_sco;
    }

        /**
     * @return the m_real
     */
    public String getReal() {
        return m_real;
    }

    /**
     * @param m_real the m_real to set
     */
    public void setReal(String m_real) {
        this.m_real = m_real;
    }

    /**
     * @return the m_meta
     */
    public String getMeta() {
        return m_meta;
    }

    /**
     * @param m_meta the m_meta to set
     */
    public void setMeta(String m_meta) {
        this.m_meta = m_meta;
    }

    /**
     * @return the m_previa
     */
    public String getPrevia() {
        return m_previa;
    }

    /**
     * @param m_previa the m_previa to set
     */
    public void setPrevia(String m_previa) {
        this.m_previa = m_previa;
    }

    //Atribui ou retorna o texto para TendÍncia
    /**
     * @return the m_tend
     */
    public String getTend() {
        return m_tend;
    }

    /**
     * @param m_tend the m_tend to set
     */
    public void setTend(String m_tend) {
        this.m_tend = m_tend;
    }    
}
