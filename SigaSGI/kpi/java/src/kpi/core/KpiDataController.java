package kpi.core;

import kpi.xml.*;

public class KpiDataController {

    static final public int KPI_ST_OK = 0;
    static final public int KPI_ST_BADID = 1;
    static final public int KPI_ST_INUSE = 2;
    static final public int KPI_ST_UNIQUE = 3;
    static final public int KPI_ST_NOCMD = 4;
    static final public int KPI_ST_NOTRANSACTION = 5;
    static final public int KPI_ST_BADXML = 6;
    static final public int KPI_ST_NORIGHTS = 7;
    static final public int KPI_ST_HASCHILD = 8;
    static final public int KPI_ST_EXPIREDSESSION = 9;
    static final public int KPI_ST_GENERALERROR = 10;
    static final public int KPI_ST_VALIDATION = 11;
    static final public int KPI_CL_SECONDTASK = 999;
    private int operationStatus = 1000;
    private String errorMessage = new String();
    private String cmdAfterUpdate = new String();
    private BIXMLRecord tree = null;
    private boolean noServerMode = false;
    private KpiServerSubstitute serverSubstitute = new KpiServerSubstitute();
    private KpiCommunicationController communicationController = new KpiCommunicationController();
    private boolean inUse = false;

    public int getStatus() {
	return operationStatus;
    }

    public void setNoServeMode(boolean nsm) {
	noServerMode = nsm;
    }

    public boolean getNoServerMode() {
	return noServerMode;
    }

    public String getErrorMessage() {
	return String.valueOf(errorMessage);
    }

    public BIXMLRecord getNewTree() {
	return tree;
    }

    public synchronized BIXMLRecord loadTree(String type) {
	BIXMLRecord xmlTree = null;

	BIRequest request = new BIRequest();
	BIResponse response = new BIResponse();

	request.setCommand("ARVORE");
	request.setParameter("TIPO", type);

	String answer = null;
	if (!noServerMode) {
	    answer = communicationController.sendData(request.writeXML());
	} else {
	    answer = serverSubstitute.sendData(request.writeXML());
	}

	response.processResponse(answer);
	operationStatus = response.getStatus();
	errorMessage = response.getErrorMessage();

	if (operationStatus == KPI_ST_OK) {
	    xmlTree = response.getReturnedRecord();
	}

	return xmlTree;
    }

    public synchronized BIXMLRecord getRecordStructure(String type, String parentId) {
	BIRequest request = new BIRequest();
	BIResponse response = new BIResponse();
	BIXMLRecord record = null;

	request.setCommand("CARREGAR");
	request.setParameter("TIPO", type);
	request.setParameter("PARENTID", parentId);
	request.setParameter("ID", "0");

	String answer = null;
	if (!noServerMode) {
	    answer = communicationController.sendData(request.writeXML());
	} else {
	    answer = serverSubstitute.sendData(request.writeXML());
	}

	response.processResponse(answer);
	operationStatus = response.getStatus();
	errorMessage = response.getErrorMessage();

	if (operationStatus == KPI_ST_OK) {
	    record = response.getReturnedRecord();
	}

	return record;
    }

    public synchronized BIXMLRecord loadRecord(String id, String type) {
	BIRequest request = new BIRequest();
	BIResponse response = new BIResponse();
	BIXMLRecord record = null;

	request.setCommand("CARREGAR");
	request.setParameter("TIPO", type);
	request.setParameter("ID", id);

	String answer = null;
	if (!noServerMode) {
	    answer = communicationController.sendData(request.writeXML());
	} else {
	    answer = serverSubstitute.sendData(request.writeXML());
	}

	response.processResponse(answer);
	operationStatus = response.getStatus();
	errorMessage = response.getErrorMessage();

	if (operationStatus == KPI_ST_OK) {
	    record = response.getReturnedRecord();
	}

	return record;
    }

    public synchronized BIXMLRecord loadRecord(String id, String type, String cLoadCmd) {
	BIRequest request = new BIRequest();
	BIResponse response = new BIResponse();
	BIXMLRecord record = null;

	request.setCommand("CARREGAR");
	request.setParameter("TIPO", type);
	request.setParameter("ID", id);
	request.setParameter("LOADCMD", cLoadCmd);

	String answer = null;
	if (!noServerMode) {
	    answer = communicationController.sendData(request.writeXML());
	} else {
	    answer = serverSubstitute.sendData(request.writeXML());
	}

	response.processResponse(answer);
	operationStatus = response.getStatus();
	errorMessage = response.getErrorMessage();

	if (operationStatus == KPI_ST_OK) {
	    record = response.getReturnedRecord();
	}

	return record;
    }

    public synchronized void saveBase64(String fileName, String content) {
	saveBase64(fileName, content, true, true);
    }

    public synchronized void saveBase64(String fileName, String content, boolean lNewFile, boolean refreshStatusBar) {
	BIRequest request = new BIRequest();
	BIResponse response = new BIResponse();
	BIXMLRecord record = null;

	request.setCommand("SALVARBASE64");
	request.setParameter("FILENAME", fileName);
	request.setParameter("FILECONTENT", content);
	if (lNewFile) {
	    request.setParameter("FILENEW", "T");
	} else {
	    request.setParameter("FILENEW", "F");
	}

	String answer = null;
	if (!noServerMode) {
	    answer = communicationController.sendData(request.writeXML(), refreshStatusBar);
	} else {
	    answer = serverSubstitute.sendData(request.writeXML());
	}

	response.processResponse(answer);
	operationStatus = response.getStatus();
	errorMessage = response.getErrorMessage();
    }

    public synchronized void deleteRecord(String id, String type, String delCMD) {
	kpi.swing.KpiTree tmpKpiTree = kpi.core.KpiStaticReferences.getKpiMainPanel().getKpiTree();
	BIRequest request = new BIRequest();
	BIResponse response = new BIResponse();

	request.setCommand("EXCLUIR");
	request.setParameter("TIPO", type);

	if (tmpKpiTree != null) {
	    request.setParameter("ARVORETIPO", tmpKpiTree.getKpiTreeType());
	} else {
	    request.setParameter("ARVORETIPO", "NOTREE");
	}

	request.setParameter("ID", id);
	request.setParameter("DELCMD", delCMD);

	String answer = null;
	if (!noServerMode) {
	    answer = communicationController.sendData(request.writeXML());
	} else {
	    answer = serverSubstitute.sendData(request.writeXML());
	}

	response.processResponse(answer);
	operationStatus = response.getStatus();
	errorMessage = response.getErrorMessage();
	this.tree = response.getTree();
    }

    public synchronized void updateRecord(String type, BIXMLRecord record) {
	kpi.swing.KpiTree tmpKpiTree = kpi.core.KpiStaticReferences.getKpiMainPanel().getKpiTree();
	BIRequest request = new BIRequest();
	BIResponse response = new BIResponse();

	request.setCommand("ALTERAR");
	request.setParameter("TIPO", type);

	if (tmpKpiTree != null) {
	    request.setParameter("ARVORETIPO", tmpKpiTree.getKpiTreeType());
	} else {
	    request.setParameter("ARVORETIPO", "NOTREE");
	}

	request.setRecord(record);

	String answer = null;
	if (!noServerMode) {
	    answer = communicationController.sendData(request.writeXML());
	} else {
	    answer = serverSubstitute.sendData(request.writeXML());
	}

	response.processResponse(answer);
	operationStatus = response.getStatus();
	errorMessage = response.getErrorMessage();
	cmdAfterUpdate = response.getCmdAfterUpdate();
	this.tree = response.getTree();
    }

    public synchronized String insertRecord(String type, BIXMLRecord record) {
	kpi.swing.KpiTree tmpKpiTree = kpi.core.KpiStaticReferences.getKpiMainPanel().getKpiTree();
	BIRequest request = new BIRequest();
	BIResponse response = new BIResponse();
	String id = null;

	request.setCommand("INCLUIR");
	request.setParameter("TIPO", type);

	if (tmpKpiTree != null) {
	    request.setParameter("ARVORETIPO", tmpKpiTree.getKpiTreeType());
	} else {
	    request.setParameter("ARVORETIPO", "NOTREE");
	}

	request.setRecord(record);

	String answer = null;
	if (!noServerMode) {
	    answer = communicationController.sendData(request.writeXML());
	} else {
	    answer = serverSubstitute.sendData(request.writeXML());
	}

	response.processResponse(answer);
	operationStatus = response.getStatus();
	errorMessage = response.getErrorMessage();
	if (operationStatus == KPI_ST_OK) {
	    id = response.getID();
	} else {
	    id = new String();
	}
	this.tree = response.getTree();

	return id;
    }

    public synchronized BIXMLRecord listRecords(String type, String cmdSQL) {
	BIRequest request = new BIRequest();
	request.setParameter("TIPO", type);
	request.setParameter("CMDSQL", cmdSQL);

	return listRecords(request);
    }

    public synchronized BIXMLRecord listRecords(BIRequest request) {
	BIResponse response = new BIResponse();
	BIXMLRecord record = null;

	request.setCommand("LISTRECORDS");

	String answer = null;
	if (!noServerMode) {
	    answer = communicationController.sendData(request.writeXML());
	} else {
	    answer = serverSubstitute.sendData(request.writeXML());
	}

	response.processResponse(answer);
	operationStatus = response.getStatus();
	errorMessage = response.getErrorMessage();

	if (operationStatus == KPI_ST_OK) {
	    record = response.getReturnedRecord();
	}

	return record;
    }

    public synchronized BIXMLRecord listServerFiles(String url) {
	BIRequest request = new BIRequest();
	BIResponse response = new BIResponse();
	BIXMLRecord record = null;

	request.setCommand("LISTARARQUIVOS");
	request.setParameter("LOCAL", url);

	String answer = null;
	if (!noServerMode) {
	    answer = communicationController.sendData(request.writeXML());
	} else {
	    answer = serverSubstitute.sendData(request.writeXML());
	}

	response.processResponse(answer);
	operationStatus = response.getStatus();
	errorMessage = response.getErrorMessage();

	if (operationStatus == KPI_ST_OK) {
	    record = response.getReturnedRecord();
	}

	return record;
    }

    public synchronized String executeRecord(String id, String type, String execCMD) {
	BIRequest request = new BIRequest();
	BIResponse response = new BIResponse();

	request.setCommand("EXECUTAR");
	request.setParameter("TIPO", type);
	request.setParameter("ID", id);
	request.setParameter("EXECCMD", execCMD);

	String answer = null;
	if (!noServerMode) {
	    answer = communicationController.sendData(request.writeXML());
	} else {
	    answer = serverSubstitute.sendData(request.writeXML());
	}

	response.processResponse(answer);
	operationStatus = response.getStatus();
	errorMessage = response.getErrorMessage();

	if (operationStatus != KPI_ST_OK) {
	    id = "0";
	}

	return id;
    }

    public synchronized BIXMLRecord loadHelp(String type, String helpCMD) {
	BIXMLRecord record = null;
	BIRequest request = new BIRequest();
	BIResponse response = new BIResponse();

	request.setCommand("AJUDA");
	request.setParameter("TIPO", type);
	request.setParameter("HELPCMD", helpCMD);

	String answer = null;
	if (!noServerMode) {
	    answer = communicationController.sendData(request.writeXML());
	} else {
	    answer = serverSubstitute.sendData(request.writeXML());
	}

	response.processResponse(answer);
	operationStatus = response.getStatus();
	errorMessage = response.getErrorMessage();

	if (operationStatus == KPI_ST_OK) {
	    record = response.getReturnedRecord();
	}

	return record;
    }

    public synchronized String doLogin(String username, String password) {
	String resposta = communicationController.sendLogin(username, password);
	return resposta;
    }

    public synchronized String createSession() {
	BIXMLRecord record = null;
	BIResponse response = new BIResponse();
	String resposta = communicationController.createSession();
	response.processResponse(resposta);
	operationStatus = response.getStatus();
	errorMessage = response.getErrorMessage();

	if (operationStatus == KPI_ST_OK) {
	    record = response.getReturnedRecord();
	}

	return record.getString("SESSIONID");
    }

    /** Getter for property inUse.
     * @return Value of property inUse.
     *
     */
    public boolean isInUse() {
	return inUse;
    }

    /** Setter for property inUse.
     * @param inUse New value of property inUse.
     *
     */
    public void setInUse(boolean inUse) {
	this.inUse = inUse;
    }

    public String getCmdAfterUpdate() {
	return cmdAfterUpdate;
    }

    public void setCmdAfterUpdate(String cmdAfterUpdate) {
	this.cmdAfterUpdate = cmdAfterUpdate;
    }

    public KpiCommunicationController getKpiCommunicationController() {
	return communicationController;
    }
}