/*
 * KpiDataControllerException.java
 *
 * Created on June 3, 2003, 11:37 AM
 */

package kpi.core;

/**
 *
 * @author  siga1996
 */
public class KpiDataControllerException extends java.lang.RuntimeException {
    
    /**
     * Creates a new instance of <code>KpiDataControllerException</code> without detail message.
     */
    public KpiDataControllerException() {
    }
    
    
    /**
     * Constructs an instance of <code>KpiDataControllerException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public KpiDataControllerException(String msg) {
	super(msg);
    }
}
