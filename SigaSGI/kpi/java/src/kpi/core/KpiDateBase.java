/*
 * KpiDateBase.java
 *
 * Created on 19 de Janeiro de 2006, 16:17
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package kpi.core;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author Alexandre Alves da Silva
 */
public class KpiDateBase {
	private java.util.GregorianCalendar dDataAnaliseDe	= new java.util.GregorianCalendar();
	private java.util.GregorianCalendar dDataAnaliseAte	= new java.util.GregorianCalendar();
	private java.util.GregorianCalendar dDataAcumuladoDe	= new java.util.GregorianCalendar(getDataAnaliseDe().get(GregorianCalendar.YEAR),0,1);
	private java.util.GregorianCalendar dDataAcumuladoAte	= new java.util.GregorianCalendar(getDataAnaliseDe().get(GregorianCalendar.YEAR),11,31);
	
	/**
	 * Creates a new instance of KpiDateBase 
	 */
	public KpiDateBase() {
	}

	public java.util.GregorianCalendar getDataAnaliseDe() {
		return dDataAnaliseDe;
	}

	public void setDataAnaliseDe(java.util.GregorianCalendar dDataAnaliseDe) {
		this.dDataAnaliseDe = dDataAnaliseDe;
	}

	public java.util.GregorianCalendar getDataAnaliseAte() {
		return dDataAnaliseAte;
	}

	public void setDataAnaliseAte(java.util.GregorianCalendar dDataAnaliseAte) {
		this.dDataAnaliseAte = dDataAnaliseAte;
	}

	public java.util.GregorianCalendar getDataAcumuladoDe() {
		return dDataAcumuladoDe;
	}

	public void setDataAcumuladoDe(java.util.GregorianCalendar dDataAcumuladoDe) {
		this.dDataAcumuladoDe = dDataAcumuladoDe;
	}

	public java.util.GregorianCalendar getDataAcumuladoAte() {
		return dDataAcumuladoAte;
	}

	public void setDataAcumuladoAte(java.util.GregorianCalendar dDataAcumuladoAte) {
		this.dDataAcumuladoAte = dDataAcumuladoAte;
	}

	public java.util.GregorianCalendar toGregorianCalendar(java.util.Calendar data){
		java.util.GregorianCalendar calendar = 
				new java.util.GregorianCalendar(data.get(Calendar.YEAR),data.get(Calendar.MONTH),data.get(Calendar.DAY_OF_MONTH));
		return calendar;
	}	
}