/*
 * KpiDebug.java
 *
 * Created on August 27, 2003, 9:04 AM
 */

package kpi.core;

/**
 *
 * @author  siga1996
 */
public class KpiDebug {
	
	static private boolean debug = false;
	
	static public void println( String msg ) {
		if ( debug )
			System.out.println( msg );
	}

	static public void println( ) {
		if ( debug )
			System.out.println( );
	}

	static public void print( Object msg ) {
		if ( debug )
			System.out.print( msg );
	}
	
	static public void setDebug( boolean on ) {
		debug = on;		
	}
}
