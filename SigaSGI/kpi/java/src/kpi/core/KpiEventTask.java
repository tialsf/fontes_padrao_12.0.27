/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kpi.core;

/**
 * Classe que registra os eventos a serem chamado no retorno do processamento da Thread do ADVPL.
 * @author asilva
 */
public class KpiEventTask {

    private Object objOri = null;
    private String aMethod = null;
    private Class[] params = null;
    private Object[] args = null;
    private boolean doDefAction = true;

    public KpiEventTask() {}

    /**
     * Cria uma tarefa para execu��o de processamento do sistema.
     * @param objOri = Objeto origem
     * @param aMethod = Nome do m�todo.
     * @param params = Tipos dos parametros.
     * @param args = Valores dos parametros.
     */
    public KpiEventTask(Object objOri, String aMethod, Class[] params, Object[] args) {
	this.objOri = objOri;
	this.aMethod = aMethod;
	this.params = params;
	this.args = args;
    }

    /**
     * @return the objOri
     */
    public Object getObjOri() {
	return objOri;
    }

    /**
     * @param objOri the objOri to set
     */
    public void setObjOri(Object objOri) {
	this.objOri = objOri;
    }

    /**
     * @return the aMethod
     */
    public String getaMethod() {
	return aMethod;
    }

    /**
     * @param aMethod the aMethod to set
     */
    public void setaMethod(String aMethod) {
	this.aMethod = aMethod;
    }

    /**
     * @return the params
     */
    public Class[] getParams() {
	return params;
    }

    /**
     * @param params the params to set
     */
    public void setParams(Class[] params) {
	this.params = params;
    }

    /**
     * @return the args
     */
    public Object[] getArgs() {
	return args;
    }

    /**
     * @param args the args to set
     */
    public void setArgs(Object[] args) {
	this.args = args;
    }

    /**
     * @return the doDefAction
     */
    public boolean doDefAction() {
	return doDefAction;
    }

    /**
     * Indentifica se o m�todo padr�o da a��o deve ser executado.
     * 
     * @param doDefAction the doDefAction to set
     */
    public void setDoDefAction(boolean doDefAction) {
	this.doDefAction = doDefAction;
    }
}
