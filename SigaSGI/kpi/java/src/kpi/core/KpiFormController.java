/*
 * Controller.java
 *
 * Created on May 28, 2003, 8:49 AM
 */
package kpi.core;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Hashtable;
import kpi.applet.KpiMainPanel;
import kpi.beans.JBIDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import kpi.swing.KpiCloseTabbedPane;
import kpi.swing.KpiDefaultFrameFunctions;
import kpi.swing.KpiInternalFrame;

/**
 *
 * @author  siga1996
 */
public class KpiFormController implements kpi.swing.KpiCloseTabbedPaneListener {

    private KpiMainPanel mainPanel;
    private JBIDesktopPane desktop;
    private Hashtable frames = new Hashtable();
    private HashMap classes;
    private KpiCloseTabbedPane abas;
    public static final int ST_PASTA = 0;
    public static final int ST_JANELA = 1;
    public int visao = ST_JANELA;

    public KpiFormController(KpiMainPanel frameAux, JBIDesktopPane dp) {
        mainPanel = frameAux;
        desktop = dp;

        classes = new java.util.HashMap();

        classes.put("LSTGRUPO_IND",
                new String[]{"kpi.swing.framework.KpiListaGrupoIndicador", "LSTGRUPO_IND"});
        classes.put("GRUPO_IND",
                new String[]{"kpi.swing.framework.KpiGrupoIndicador", "GRUPO_IND"});
	classes.put("LSTSCORECARD",
                new String[]{"kpi.swing.framework.KpiListaScorecard", "LSTSCORECARD"});
	classes.put("ORGANIZACAO",
                new String[]{"kpi.swing.framework.scorecardPanels.KpiOrganizacao", "FORMMATRIX"});
	classes.put("ESTRATEGIA",
                new String[]{"kpi.swing.framework.scorecardPanels.KpiEstrategia", "FORMMATRIX"});
	classes.put("FORMMATRIX",
                new String[]{"kpi.swing.framework.scorecardPanels.KpiFormMatrix", "FORMMATRIX"});
	classes.put("PERSPECTIVA",
                new String[]{"kpi.swing.framework.scorecardPanels.KpiPerspectiva", "FORMMATRIX"});
	classes.put("OBJETIVO",
                new String[]{"kpi.swing.framework.scorecardPanels.KpiObjetivo", "FORMMATRIX"});
	classes.put("SCORECARD",
                new String[]{"kpi.swing.framework.KpiScorecard", "SCORECARD"});
        classes.put("LSTINDICADOR",
                new String[]{"kpi.swing.framework.KpiListaIndicador", "LSTINDICADOR"});
        classes.put("METAFORMULA",
                new String[]{"kpi.swing.framework.KpiMetaFormula", "METAFORMULA"});
        classes.put("LSTMETAFORMULA",
                new String[]{"kpi.swing.framework.KpiListaMetaFormula", "LSTMETAFORMULA"});
        classes.put("PLANOACAO",
                new String[]{"kpi.swing.framework.KpiPlanoDeAcao", "PLANOACAO"});
        classes.put("LSTPLANOACAO",
                new String[]{"kpi.swing.framework.KpiListaPlanos", "LSTPLANOACAO"});
        classes.put("GRUPO_ACAO",
                new String[]{"kpi.swing.framework.KpiGrupoAcao", "GRUPO_ACAO"});
        classes.put("LSTGRUPO_ACAO",
                new String[]{"kpi.swing.framework.KpiListaGrupoAcao", "LSTGRUPO_ACAO"});
        classes.put("LSTPROJETO",
                new String[]{"kpi.swing.framework.KpiListaProjeto", "LSTPROJETO"});
        classes.put("PROJETO",
                new String[]{"kpi.swing.framework.KpiProjeto", "PROJETO"});
        classes.put("UNIDADE",
                new String[]{"kpi.swing.framework.KpiUnidade", "UNIDADE"});
        classes.put("LSTUNIDADE",
                new String[]{"kpi.swing.framework.KpiListaUnidade", "LSTUNIDADE"});
        classes.put("ORDEM_INDICADOR",
                new String[]{"kpi.swing.framework.kpiOrdemIndicador", "ORDEM_INDICADOR"});
        classes.put("PACOTE",
                new String[]{"kpi.swing.framework.KpiPacote", "PACOTE"});
        classes.put("LSTPACOTE",
                new String[]{"kpi.swing.framework.KpiListaPacote", "LSTPACOTE"});
        classes.put("ORDEM_SCORECARD",
                new String[]{"kpi.swing.framework.KpiOrdemScorecard", "ORDEM_SCORECARD"});
        classes.put("NOTA",
                new String[]{"kpi.swing.framework.KpiNota", "NOTA"});
        classes.put("LSTNOTA",
                new String[]{"kpi.swing.framework.KpiListaNota", "LSTNOTA"});
        classes.put("ESP_PEIXE",
                new String[]{"kpi.swing.espinhapeixe.KpiEspinhaPeixeCadastro", "ESP_PEIXE"});
        classes.put("LST_ESP_PEIXE",
                new String[]{"kpi.swing.espinhapeixe.KpiEspinhaPeixeLista", "LST_ESP_PEIXE"});
        classes.put("ESP_PEIXE_REL",
                new String[]{"kpi.swing.espinhapeixe.KpiEspinhaPeixeRel", "ESP_PEIXE_REL"});
        classes.put("AGENDADOR",
                new String[]{"kpi.swing.tools.KpiAgendador", "AGENDADOR"});
        classes.put("AGENDAMENTO",
                new String[]{"kpi.swing.tools.KpiAgendamento", "AGENDAMENTOS"});
        classes.put("CALC_INDICADOR",
                new String[]{"kpi.swing.tools.KpiCalcIndicador", "CALC_INDICADORES"});
        classes.put("DIRUSUARIOS",
                new String[]{"kpi.swing.tools.KpiDirUsuariosFrame", "DIRUSUARIOS"});
        classes.put("USUARIO",
                new String[]{"kpi.swing.tools.KpiUsuarioFrame", "DIRUSUARIOS"});
        classes.put("GRUPO",
                new String[]{"kpi.swing.tools.KpiGrupoFrame", "DIRUSUARIOS"});
        classes.put("LSTLEMBRETE",
                new String[]{"kpi.swing.tools.KpiLembrete", "LSTLEMBRETE"});
        classes.put("SCO_DUPLICADOR",
                new String[]{"kpi.swing.tools.KpiDuplicador", "SCO_DUPLICADORES"});
        classes.put("SMTPCONF",
                new String[]{"kpi.swing.tools.KpiSMTPConfFrame", "SMTPCONFS"});
        classes.put("MENSAGEM",
                new String[]{"kpi.swing.tools.KpiMensagemFrame", "MENSAGEM"});
        classes.put("MENSAGENS_ENVIADAS",
                new String[]{"kpi.swing.tools.KpiMensagensEnviadasFrame", "MENSAGENS_ENVIADAS"});
        classes.put("MENSAGENS_RECEBIDAS",
                new String[]{"kpi.swing.tools.KpiMensagensRecebidasFrame", "MENSAGENS_RECEBIDAS"});
        classes.put("MENSAGENS_EXCLUIDAS",
                new String[]{"kpi.swing.tools.KpiMensagensExcluidasFrame", "MENSAGENS_EXCLUIDAS"});
        classes.put("PARAMETRO",
                new String[]{"kpi.swing.tools.KpiParametrosSistema", "PARAMETROS"});
        classes.put("GERAPLANILHA",
                new String[]{"kpi.swing.tools.KpiGeraPlanilha", "GERAPLANILHA"});
        classes.put("EXPORTPLAN",
                new String[]{"kpi.swing.tools.KpiExportPlan", "EXPORTPLANS"});
        classes.put("DATASRC",
                new String[]{"kpi.swing.tools.KpiDataSourceFrame", "DATASRCS"});
        classes.put("LSTDATASRC",
                new String[]{"kpi.swing.tools.KpiLstDataSource", "LSTDATASRC"});
        classes.put("KPIUPLOAD",
                new String[]{"kpi.swing.tools.KpiUpload", "KPIUPLOAD"});
        classes.put("KPICHANGEIMG",
                new String[]{"kpi.swing.KpiChangeImage", "KPICHANGEIMG"});
        classes.put("CFGRESTACESSO",
                new String[]{"kpi.swing.tools.KpiRestAcesso", "RESTACESSO"});
        classes.put("CFGPLANVLR",
                new String[]{"kpi.swing.tools.KpiRestricaoPlanValor", "CFGPLANVLR"});
        classes.put("ESTIMPORT",
                new String[]{"kpi.swing.tools.KpiEstruturaImport", "ESTIMPORT"});
        classes.put("ESTEXPORT",
                new String[]{"kpi.swing.tools.KpiEstruturaExport", "ESTEXPORT"});
        classes.put("DW_CONF",
                new String[]{"kpi.swing.tools.KpiConfDw", "DW_CONF"});
        classes.put("INDICADOR_FORMULA",
                new String[]{"kpi.swing.tools.KpiIndicadorFormula", "INDICADOR_FORMULA"});
        classes.put("USU_CONFIG",
                new String[]{"kpi.swing.desktop.KpiDesktopFrame", "USU_CONFIGS"});
        classes.put("LSTPAINEL",
                new String[]{"kpi.swing.analisys.KpiListaPainel", "LSTPAINEL"});
        classes.put("PAINEL",
                new String[]{"kpi.swing.analisys.KpiPainel", "PAINEL"});
        classes.put("LSTPAINELCOMP",
                new String[]{"kpi.swing.analisys.KpiListaPainelComp", "LSTPAINELCOMP"});
        classes.put("PAINELCOMP",
                new String[]{"kpi.swing.analisys.KpiPainelComp", "PAINELCOMP"});
        classes.put("SCORECARDING",
                new String[]{"kpi.swing.analisys.KpiScoreCarding", "SCORECARDINGS"});
        classes.put("LISTA_STATUS_ACAO",
                new String[]{"kpi.swing.analisys.KpiListaStatusAcoes", "LISTA_STATUS_ACOES"});
        classes.put("RELPLAN",
                new String[]{"kpi.swing.report.KpiRelPlano", "RELPLANS"});
        classes.put("LSTRELAPR",
                new String[]{"kpi.swing.report.KpiListaApresentacoes", "LSTRELAPR"});
        classes.put("RELAPR",
                new String[]{"kpi.swing.report.KpiApresentacao", "RELAPR"});
        classes.put("RELSCOREIND",
                new String[]{"kpi.swing.report.KpiRelScoreInd", "RELSCOREIND"});
        classes.put("RELESTATPLAN",
                new String[]{"kpi.swing.report.KpiRelEstatPlan", "RELESTATPLAN"});
        classes.put("GRAPH_IND",
                new String[]{"kpi.swing.graph.KpiGraphIndicador", "GRAPH_INDS"});
	classes.put("MAPAESTRATEGICO1",
		new String[]{"kpi.swing.analisys.map.KpiMapaFrame", "MAPAESTRATEGICO1"});
	classes.put("MAPAESTRATEGICO2",
		new String[]{"kpi.swing.analisys.map.KpiMapa2Frame", "MAPAESTRATEGICO2"});
        classes.put("TEMAESTRATEGICO",
		new String[]{"kpi.swing.framework.KpiTemaEstrategico", "ESTRATEGIA"});        
        classes.put("MAPADRILLOBJETIVO",
		new String[]{"kpi.swing.analisys.map.KpiMapaDrillObjetivo", "MAPADRILLOBJETIVO"});
        classes.put("BUSCAGRUPOINDICADOR",
		new String[]{"kpi.swing.framework.KpiBuscaGrupoIndicador", "BUSCAGRUPOINDICADOR"});

	if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)){
	    classes.put("INDICADOR",
                new String[]{"kpi.swing.framework.KpiIndicador", "FORMMATRIX"});
            classes.put("RELBOOK",
                new String[]{"kpi.swing.report.KpiRelBook", "RELBOOK"});
            classes.put("RELIND",
                new String[]{"kpi.swing.report.KpiRelInd", "RELIND"});
            
	}else{
	    classes.put("INDICADOR",
                new String[]{"kpi.swing.framework.KpiIndicador", "INDICADOR"});
            
	}
    }

    /**
     *
     * @param type
     * @return
     */
    public String getFormClassName(String type) {
        String classe = null;
        String[] propriedades = (String[]) classes.get(type);

        try {
            classe = propriedades[0];
        } catch (Exception e) {
            System.err.println("Classe do form n�o localizada: " + type + "\nExece��o: " + e);
        }
        return classe;
    }

    /**
     *
     * @param type
     * @return
     */
    public String getParentName(String type) {
        final String[] propriedades = (String[]) classes.get(type);

	if (propriedades != null){
	    return propriedades[1];
	}

	return null;
    }

    /**
     *
     * @param type
     * @param parentId
     * @return
     */
    public KpiDefaultFrameFunctions getParentForm(String type, String parentId) {
        final String pai = getParentName(type);
        if (pai == null) {
            return null;
        } else {
            return getFrameReference(pai, parentId);
        }
    }

    /**
     * 
     * @return
     */
    public KpiMainPanel getKpiMainPanel() {
        return mainPanel;
    }

    /**
     *
     * @param type
     * @param id
     * @return
     */
    private KpiDefaultFrameFunctions createForm(String type, String id, String contextId) {
        KpiDefaultFrameFunctions frame = null;
        final Class[] parametros = new Class[]{int.class, String.class, String.class};
        final Object[] valores = new Object[]{new Integer(kpi.swing.KpiDefaultFrameBehavior.NORMAL), id, contextId};

        try {
            final Class classe = Class.forName(getFormClassName(type));
            final Constructor construtor = classe.getConstructor(parametros);
            frame = (kpi.swing.KpiDefaultFrameFunctions) construtor.newInstance(valores);

            closeListener(frame);

        } catch (Exception e) {
            System.err.println(e);
        }

        return frame;
    }

    /**
     *
     * @param type
     * @param id
     * @param contextID
     * @return
     */
    public kpi.swing.KpiDefaultFrameFunctions newForm(String type, String id, String contextID) {
        final String chave = type + " - " + id;

        if (frames.containsKey(chave)) {

            KpiDefaultFrameFunctions frame = isFormOpen(type, id);

            if (mainPanel.radJanela.isSelected()) {
                frame.asJInternalFrame().dispose();
            } else {
                if (abas.getComponentCount() != 0) {
                    JPanel tmpPanel = (javax.swing.JPanel) frame.asJInternalFrame().getClientProperty("tmpPanel");
                    abas.remove(tmpPanel);
                }
            }
            frames.remove(chave);
        }

        KpiDefaultFrameFunctions frame = newDetailForm(type, id, contextID);
        frames.put(chave, frame);

        return frame;
    }

    /**
     *
     * @param type
     * @param parentID
     * @param contextID
     * @return
     */
    public KpiDefaultFrameFunctions newDetailForm(String type, String parentID, String contextID) {
        KpiDefaultFrameFunctions frame = null;
        final Class[] parametros = new Class[]{int.class, String.class, String.class};
        final Object[] valores = new Object[]{new Integer(kpi.swing.KpiDefaultFrameBehavior.INSERTING), parentID, contextID};

        try {
            final Class classe = Class.forName(getFormClassName(type));
            final Constructor construtor = classe.getConstructor(parametros);
            frame = (kpi.swing.KpiDefaultFrameFunctions) construtor.newInstance(valores);

            adicionaJanela(frame, frame.asJInternalFrame().getTitle());
            frame.asJInternalFrame().setSelected(true);
            posicionaJanela(frame);
            frame.asJInternalFrame().show();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return frame;
    }

    /**
     *
     * @param type
     * @param newId
     * @param newFrame
     */
    public void setNewDetailFormInfo(String type, String newId, kpi.swing.KpiDefaultFrameFunctions newFrame) {
        final String name = type + " - " + newId;
        frames.put(name, newFrame);
    }

    /**
     * 
     * @param type
     * @param id
     * @param title
     * @return
     */
    public KpiDefaultFrameFunctions getForm(String type, String id, String title) {
        return getForm(type, id, "0", title);
    }

    /**
     *
     * @param type
     * @param id
     * @param title
     * @return
     */
    public KpiDefaultFrameFunctions getForm(String type, String id, String contextId, String title) {
        final String chave = type.concat(" - ").concat(id);
        final String titulo = title.isEmpty() ? "" : ": ".concat(title);
        KpiDefaultFrameFunctions frame = (KpiDefaultFrameFunctions) frames.get(chave);

        if (frame == null) {
            frame = createForm(type, id, contextId);
            if (frame != null) {
                frame.asJInternalFrame().setTitle(frame.asJInternalFrame().getTitle() + titulo);
                frames.put(chave, frame);

                if (mainPanel.radJanela.isSelected()) {
                    desktop.add(frame.asJInternalFrame(), javax.swing.JLayeredPane.CENTER_ALIGNMENT); //DEFAULT_LAYER);
                } else {
                    addTabbedToContainer(frame, title);
                }
            } else {
                System.out.println(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiFormController_00006") + type);
                return null;
            }
        } else {
            if (!frame.asJInternalFrame().isVisible()) {
                frame = createForm(type, id, contextId);
                frame.asJInternalFrame().setTitle(frame.asJInternalFrame().getTitle() + titulo);
                frames.put(chave, frame);
                if (mainPanel.radJanela.isSelected()) {
                    desktop.add(frame.asJInternalFrame(), javax.swing.JLayeredPane.DEFAULT_LAYER);
                } else {
                    addTabbedToContainer(frame, title);
                }
            } else {
                if (!mainPanel.radJanela.isSelected()) {
                    javax.swing.JPanel tmpPanel = (javax.swing.JPanel) frame.asJInternalFrame().getClientProperty("tmpPanel");
                    abas.setSelectedComponent(tmpPanel);
                }
            }
        }

        try {
            frame.asJInternalFrame().setSelected(true);
        } catch (java.beans.PropertyVetoException e2) {
            kpi.core.KpiDebug.println(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiFormController_00005") + type);
        }

        posicionaJanela(frame);

        frame.asJInternalFrame().show();

        return frame;
    }

    /**
     *
     * @param type
     * @param id
     * @return
     */
    public kpi.swing.KpiDefaultFrameFunctions getFrameReference(String type, String id) {
        final String chave = type.concat(" - ").concat(id);
        KpiDefaultFrameFunctions frame = (kpi.swing.KpiDefaultFrameFunctions) frames.get(chave);

        if (frame != null) {
            if (!frame.asJInternalFrame().isVisible()) {
                frame = null;
            }
        }

        return frame;
    }

    /**
     *
     * @param frame
     * @param title
     */
    public void addTabbedToContainer(kpi.swing.KpiDefaultFrameFunctions frame, String title) {
        JPanel jPainelTmp = new JPanel();
        jPainelTmp.setLayout(new java.awt.BorderLayout());

        if (abas == null) {
            abas = new kpi.swing.KpiCloseTabbedPane();
            abas.setRequestFocusEnabled(false);
            abas.addKpiCloseTabbedPaneListener(this);
            desktop.setLayout(new java.awt.BorderLayout());
            desktop.add(abas);
        }

        jPainelTmp.add(frame.asJInternalFrame().getContentPane());
        title = frame.asJInternalFrame().getTitle() == null ? title : frame.asJInternalFrame().getTitle();
        abas.addTab(title, frame.asJInternalFrame().getFrameIcon(), jPainelTmp, frame);
        frame.asJInternalFrame().putClientProperty("tmpPanel", jPainelTmp);
        abas.setSelectedComponent(jPainelTmp);
    }

    /**
     *
     * @param componentEvent
     */
    @Override
    public void beforeTabRemoved(java.awt.event.ComponentEvent componentEvent) {
        boolean lClose = true;
        javax.swing.JPanel jTmpPanel = (javax.swing.JPanel) abas.getComponentAt(componentEvent.getID());
        kpi.swing.KpiDefaultFrameFunctions frmFunctions = (kpi.swing.KpiDefaultFrameFunctions) jTmpPanel.getClientProperty("FrameParent");
        if (frmFunctions.asJInternalFrame() instanceof kpi.swing.KpiInternalFrame) {
            kpi.swing.KpiInternalFrame kpiInternalFrame = ((kpi.swing.KpiInternalFrame) frmFunctions.asJInternalFrame());
            if (!kpiInternalFrame.isCloseEnabled()) {
                lClose = false;
            }
        }
        if (lClose) {
            String frmType = frmFunctions.getFormType();
	    frmFunctions.asJInternalFrame().dispose();
	    
	    if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)
		&& frmType.equals("SCORECARD")){
		    frmType = "FORMMATRIX";
	    }

	    String name = frmType.concat(" - ").concat(frmFunctions.getID());
            frames.remove(name);
        }
    }

    /**
     *
     * @param componentEvent
     */
    @Override
    public void afterTabRemoved(java.awt.event.ComponentEvent componentEvent) {
        removeDesktopContainer();
    }

    /**
     * 
     * @param frame
     */
    private void closeListener(KpiDefaultFrameFunctions frame) {
        frame.asJInternalFrame().addInternalFrameListener(new javax.swing.event.InternalFrameAdapter() {

            @Override
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent closeEvent) {
                windowClosed(closeEvent);
            }
        });
    }

    /**
     *
     * @param closeEvent
     */
    public void windowClosed(javax.swing.event.InternalFrameEvent closeEvent) {
        boolean fechado = true;

        KpiDefaultFrameFunctions frmFunctions = (KpiDefaultFrameFunctions) closeEvent.getSource();

        if (frmFunctions.asJInternalFrame() instanceof KpiInternalFrame) {
            KpiInternalFrame kpiInternalFrame = ((KpiInternalFrame) frmFunctions.asJInternalFrame());
            if (!kpiInternalFrame.isCloseEnabled()) {
                fechado = false;
            }
        }

        if (fechado) {
	    String frmType = frmFunctions.getFormType();
	    String frmID = frmFunctions.getID();
	    if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE) 
		&& frmType.equals("SCORECARD")){
		    frmType = "FORMMATRIX";
	    }
	    String chave = frmType.concat(" - ").concat(frmID);
            frames.remove(chave);
        }
    }

    /**
     *
     */
    public void changeView() {
        java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR);
        if (frames.size() > 0) {

	    //Vis�o de janelas selecionadas. Remover as tabelas. (Pastas).
            if (mainPanel.radJanela.isSelected() && abas != null) {
                desktop.remove(abas);
                desktop.setLayout(null);
                desktop.repaint();
                abas = null;
            }

            for (java.util.Enumeration iForm = frames.elements(); iForm.hasMoreElements();) {
                kpi.swing.KpiDefaultFrameFunctions tmpFrame = (kpi.swing.KpiDefaultFrameFunctions) iForm.nextElement();
                String title = tmpFrame.asJInternalFrame().getTitle();
                title = title.substring(title.indexOf(":") + 1, title.length());
                tmpFrame.asJInternalFrame().dispose();
		String frmType = tmpFrame.getFormType();

		if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)
		    && frmType.equals("SCORECARD")){

		    frmType = "FORMMATRIX";
		}

                getForm(frmType, tmpFrame.getID(), title.trim().concat(" "));
            }

            //Janelas em cascata.
            if (mainPanel.radJanela.isSelected()) {
                javax.swing.JInternalFrame[] allFrames = desktop.getAllFrames();
                int posicaoX = 0;
                for (int i = 0; i < allFrames.length; i++) {
                    if (i == 0) {
                        int posX = 0;
                        kpi.swing.KpiDefaultFrameFunctions form = ((KpiDefaultFrameFunctions) allFrames[i]);
                        if (mainPanel.getDesktopSelected().equals(KpiMainPanel.ST_CADASTRO)) {
                            posX = this.calculaPosicaoX(form, mainPanel.getJSplitPaneCadastro());
                        } else if (mainPanel.getDesktopSelected().equals(KpiMainPanel.ST_CONFIGURACAO)) {
                            posX = this.calculaPosicaoX(form, mainPanel.getJSplitPaneConfiguracao());
                        }
                        form.asJInternalFrame().setLocation(posX, 0);
                    } else {
                        int posX = posicaoX + (i * desktop.getFrameOffset());
                        int posY = (i * desktop.getFrameOffset());
                        ((KpiDefaultFrameFunctions) allFrames[i]).asJInternalFrame().setLocation(posX, posY);
                    }

                    try {
                        ((KpiDefaultFrameFunctions) allFrames[i]).asJInternalFrame().setSelected(true);
                    } catch (java.beans.PropertyVetoException e) {
                        System.out.println(e);
                    }
                }
            }
        }
        if (mainPanel.radJanela.isSelected()) {
            this.visao = ST_JANELA;
        } else {
            this.visao = ST_PASTA;
        }
    }

    /**
     *
     * @param type
     * @param id
     * @return
     */
    public kpi.swing.KpiDefaultFrameFunctions isFormOpen(String type, String id) {
        String chave = type.concat(" - ").concat(id);
        return (KpiDefaultFrameFunctions) frames.get(chave);
    }

    /**
     *
     */
    public void removeDesktopContainer() {
        if (abas.getComponentCount() == 0) {
            desktop.remove(abas);
            desktop.setLayout(null);
            desktop.repaint();
            abas = null;
        }
    }

    /**
     *
     * @param frame
     * @param titulo
     */
    private void adicionaJanela(KpiDefaultFrameFunctions frame, String titulo) {
        if (mainPanel.radJanela.isSelected()) {
            desktop.add(frame.asJInternalFrame(), JLayeredPane.DEFAULT_LAYER);
        } else {
            addTabbedToContainer(frame, titulo);
        }
    }

    /**
     *
     * @param form
     */
    private void posicionaJanela(kpi.swing.KpiDefaultFrameFunctions form) {
        Boolean isMaxi = (Boolean) form.asJInternalFrame().getClientProperty("MAXI");
        if (isMaxi != null) {
            try {
                form.asJInternalFrame().setMaximum(isMaxi.booleanValue());
            } catch (Exception e) {
                System.out.println(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiFormController_00007"));
            }
        } else {
            JInternalFrame[] array = desktop.getAllFrames();
            if (array.length <= 1 || form.getFormType().equals("LSTLEMBRETE")) {
                int posX = 0;
                if (mainPanel.getDesktopSelected().equals(KpiMainPanel.ST_CADASTRO)) {
                    posX = this.calculaPosicaoX(form, mainPanel.getJSplitPaneCadastro());
                } else if (mainPanel.getDesktopSelected().equals(KpiMainPanel.ST_CONFIGURACAO)) {
                    posX = this.calculaPosicaoX(form, mainPanel.getJSplitPaneConfiguracao());
                }
                form.asJInternalFrame().setLocation(posX, 0);
            } else {
                //pega a maior posicao
                int x = 0;
                int y = 0;
                for (int j = 0; j < array.length; j++) {
                    int tmpX = ((KpiDefaultFrameFunctions) array[j]).asJInternalFrame().getLocation().x;
                    if (tmpX > x) {
                        x = tmpX;
                        y = ((KpiDefaultFrameFunctions) array[j]).asJInternalFrame().getLocation().y;
                    }

                }
                int posX = x + desktop.getFrameOffset();
                int posY = y + desktop.getFrameOffset();
                form.asJInternalFrame().setLocation(posX, posY);
            }
        }
    }

    /**
     *
     * @param form
     * @param split
     * @return
     */
    private int calculaPosicaoX(kpi.swing.KpiDefaultFrameFunctions form, javax.swing.JSplitPane split) {
        int tamDesktop = split.getRightComponent().getWidth();
        int posicaoX = 0;
        if (tamDesktop != 0) {
            posicaoX = (tamDesktop / 2) - (form.asJInternalFrame().getWidth() / 2);
            form.asJInternalFrame().setLocation(posicaoX, 0);
        }
        return posicaoX;
    }

    /**
     *
     */
    public void closeAll() {
        frames.clear();
    }

    /**
     *
     * @return
     */
    public kpi.swing.KpiCloseTabbedPane getTabbedPanel() {
        return abas;
    }
}
