/*
 * FormControllerException.java
 *
 * Created on June 24, 2003, 4:03 PM
 */

package kpi.core;

/**
 *
 * @author  siga1996
 */
public class KpiFormControllerException extends java.lang.RuntimeException {
    
    /**
     * Creates a new instance of <code>FormControllerException</code> without detail message.
     */
    public KpiFormControllerException() {
    }
    
    
    /**
     * Constructs an instance of <code>FormControllerException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public KpiFormControllerException(String msg) {
	super(msg);
    }
}
