/*
 * KpiImageResources.java
 *
 * Created on October 5, 2005, 11:09 AM
 */
package kpi.core;

/**
 *
 * @author  siga0739
 */
public class KpiImageResources {

    // Entidades do KPI
    public static final int KPI_IMG_SCORECARD = 0;
    public static final int KPI_IMG_INDICADOR = 1;
    public static final int KPI_IMG_PROJETOS = 2;
    public static final int KPI_IMG_PLANOACAO = 3;
    public static final int KPI_IMG_USUARIO = 4;
    public static final int KPI_IMG_METAFORMULA = 5;
    public static final int KPI_IMG_GRUPOINDICADOR = 6;
    // Outras imagens do KPI
    public static final int KPI_IMG_PLANILHAS = 7;
    public static final int KPI_IMG_SCORECARDING = 8;
    public static final int KPI_IMG_RELATORIOS = 9;
    public static final int KPI_IMG_EXPORTACAO = 10;
    //Imagens do status dos indicadores
    public static final int KPI_IMG_STATUS_GREEN = 11;
    public static final int KPI_IMG_STATUS_RED = 12;
    public static final int KPI_IMG_STATUS_YELLOW = 13;
    public static final int KPI_IMG_ESTAVEL_GREEN = 14;
    public static final int KPI_IMG_ESTAVEL_RED = 15;
    public static final int KPI_IMG_ESTAVEL_YELLOW = 16;
    public static final int KPI_IMG_TEND_GREENDN = 17;
    public static final int KPI_IMG_TEND_GREENUP = 18;
    public static final int KPI_IMG_TEND_REDDN = 19;
    public static final int KPI_IMG_TEND_REDUP = 20;
    public static final int KPI_IMG_TEND_YELLOWDN = 21;
    public static final int KPI_IMG_TEND_YELLOWUP = 22;
    public static final int KPI_IMG_SCOREACAO = 23;
    public static final int KPI_IMG_VAZIO = 24;
    public static final int KPI_IMG_ESTAVEL_GRAY = 25;
    public static final int KPI_IMG_PLANOACAO_OWNER = 26;
    public static final int KPI_IMG_PLANOACAO_RESP = 27;
    public static final int KPI_IMG_PAINEL = 28;
    public static final int KPI_IMG_USUPERSO = 29;
    public static final int KPI_IMG_MSG_RECEBIDO = 30;
    public static final int KPI_IMG_MSG_ENVIADO = 31;
    public static final int KPI_IMG_MSG_EXCLUIDO = 32;
    public static final int KPI_IMG_AGENDADOR = 33;
    public static final int KPI_IMG_CALC_INDICADOR = 34;
    public static final int KPI_IMG_SCO_DUPLICADOR = 35;
    public static final int KPI_IMG_SMTPCONF = 36;
    public static final int KPI_IMG_DIRUSUARIO = 37;
    public static final int KPI_IMG_GRUPO_USUARIO = 38;
    public static final int KPI_IMG_PARAMETRO = 39;
    public static final int KPI_IMG_GERA_PLANILHA = 40;
    public static final int KPI_IMG_UNIDADE = 41;
    public static final int KPI_IMG_DTSRC = 42;
    public static final int KPI_IMG_DOWNLOAD = 43;
    public static final int KPI_IMG_PASTA = 44;
    public static final int KPI_IMG_PLANOACAO_LOCK = 45;
    public static final int KPI_IMG_GRUPO_ACAO = 46;
    public static final int KPI_IMG_CARDPAINEL = 47;
    public static final int KPI_IMG_CARDPAINEL_COMP = 48;
    public static final int KPI_IMG_EXCLAMACAO = 49;
    public static final int KPI_IMG_ANOMALIA = 50;
    public static final int KPI_IMG_CAUSA_ACAO = 51;
    public static final int KPI_IMG_EFEITO = 52;
    public static final int KPI_IMG_CAUSA = 53;
    public static final int KPI_IMG_SEGURANCA = 54;
    public static final int KPI_IMG_ESPPEIXE = 55;
    public static final int KPI_IMG_EXPORTAR = 56;
    public static final int KPI_IMG_IMPORTAR = 57;
    public static final int KPI_IMG_DWCONF = 58;
    public static final int KPI_IMG_PACOTE = 59;
    public static final int KPI_IMG_INDICFORMULA = 60;
    public static final int KPI_IMG_NOTA = 61;
    public static final int KPI_IMG_ORGANIZACAO = 62;
    public static final int KPI_IMG_ESTRATEGIA = 63;
    public static final int KPI_IMG_PERSPECTIVA = 64;
    public static final int KPI_IMG_OBJETIVO = 65;
    public static final int KPI_IMG_BSCENTIDADES = 66;
    public static final int KPI_IMG_OBJETIVOBLUE = 67;
    public static final int KPI_IMG_OBJETIVOGREEN = 68;
    public static final int KPI_IMG_OBJETIVOYELLOW = 69;
    public static final int KPI_IMG_OBJETIVORED = 70;
    public static final int KPI_IMG_OBJETIVOGRAY = 71;
    public static final int KPI_IMG_TEND_BLUEDN = 72;
    public static final int KPI_IMG_TEND_BLUEUP = 73;
    public static final int KPI_IMG_ESTAVEL_BLUE = 74;
    public static final int KPI_IMG_STATUS_BLUE = 75;
    public static final int KPI_IMG_MAPA = 76;
    public static final int KPI_IMG_TEMAEST = 77;
    public static final int KPI_IMG_PASTINHA = 78;
    public static final int KPI_IMG_CAD_ABERTO = 79;
    public static final int KPI_IMG_CAD_VERMELHO = 80;
    public static final int KPI_IMG_NOTA_AZUL = 81;
    public static final int KPI_IMG_NOTA_VERDE = 82;

    // Array de imagens do KPI
    private javax.swing.ImageIcon[] imagens;

    public KpiImageResources() {

        imagens = new javax.swing.ImageIcon[83];
        imagens[0] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_scorecard.gif"));
        imagens[1] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_indicador.gif"));
        imagens[2] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_projeto.gif"));
        imagens[3] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_planodeacao.gif"));
        imagens[4] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_usuario.gif"));
        imagens[5] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_meta_formula.gif"));
        imagens[6] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_grupoindicador.gif"));
        imagens[7] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_planilha.gif"));
        imagens[8] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_scorecarding.gif"));
        imagens[9] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_relatorio.gif"));
        imagens[10] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_exportacao.gif"));

        //Imagens do status dos indicadores
        imagens[11] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_status_green.gif"));
        imagens[12] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_status_red.gif"));
        imagens[13] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_status_yellow.gif"));
        imagens[14] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_estavel_green.gif"));
        imagens[15] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_estavel_redlg.gif"));
        imagens[16] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_estavel_yellowlg.gif"));
        imagens[17] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_tendencia_greendn.gif"));
        imagens[18] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_tendencia_greenup.gif"));
        imagens[19] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_tendencia_reddn.gif"));
        imagens[20] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_tendencia_redup.gif"));
        imagens[21] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_tendencia_yellowdn.gif"));
        imagens[22] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_tendencia_yellowup.gif"));
        imagens[23] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_scorecardacao.gif"));
        imagens[24] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_vazio.gif"));
        imagens[25] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_graysm.gif"));

        //Autoriza��o do plano de a��o
        imagens[26] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_planoacao_owner.gif"));
        imagens[27] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_planodeacao_responsavel.gif"));
        imagens[28] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_painelsco.gif"));
        imagens[29] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_user_personalizacao.gif"));

        //Mensagens
        imagens[30] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_emailrecebido.gif"));
        imagens[31] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_emailenviado.gif"));
        imagens[32] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_emailexcluido.gif"));

        //Ferramentas Administraticas
        imagens[33] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_agendador.gif"));
        imagens[34] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_calculo.gif"));
        imagens[35] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_duplicador.gif"));
        imagens[36] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_emailconfig.gif"));
        imagens[37] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_dirusuario.gif"));
        imagens[38] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_grupo.gif"));
        imagens[39] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_system_setup.gif"));
        imagens[40] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_gera_planilha.gif"));
        imagens[41] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_unidade.gif"));
        imagens[42] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_fontedados.gif"));
        imagens[43] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_download.gif"));
        imagens[44] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_pastaserver.gif"));
        imagens[45] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_planodeacao_lock.gif"));
        imagens[46] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_grupoacao.gif"));
        imagens[47] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_painel.gif"));
        imagens[48] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_painel_comp.gif"));
        imagens[49] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_exclamacao.gif"));
        imagens[50] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_anomalia.gif"));
        imagens[51] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_causa.gif"));
        imagens[52] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_efeito.gif"));
        imagens[53] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_causa_sem_acao.gif"));
        imagens[54] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_seguranca.gif"));
        imagens[55] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_epinhapeixe.gif"));
        imagens[56] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_exportar.gif"));
        imagens[57] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_importar.gif"));
        imagens[58] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_20_dw.gif"));
        imagens[59] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_pacote.gif"));
        imagens[60] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_indicadorformula.gif"));
        imagens[61] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_lembrete.gif"));
	imagens[62] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_20_organizacao.gif"));
	imagens[63] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_20_estrategia.gif"));
	imagens[64] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_20_perspectiva.gif"));
	imagens[65] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_20_objetivo.gif"));
	imagens[66] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_20_arvore.gif"));

	//Status dos objetivos
	imagens[67] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_objetivo_blue.gif"));
	imagens[68] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_objetivo_green.gif"));
	imagens[69] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_objetivo_yellow.gif"));
	imagens[70] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_objetivo_red.gif"));
	imagens[71] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_objetivo_gray.gif"));
	//
	imagens[72] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_tendencia_bluedn.gif"));
	imagens[73] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_tendencia_blueup.gif"));
	imagens[74] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_estavel_blue.gif"));
	imagens[75] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_status_blue.gif"));

	//Mapa Estrat�gico
	imagens[76] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_20_mapa.gif"));
        
        //Tema Estrategico
        imagens[77] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_20_temaestrategico.gif"));
        imagens[78] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_pastinha.gif"));

	imagens[79] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_cadeado_aberto.gif"));
        imagens[80] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_cadeado_vermelho.gif"));

	imagens[81] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_lembrete_azul.gif"));
        imagens[82] = new javax.swing.ImageIcon(this.getClass().getResource("/kpi/imagens/ic_lembrete_verde.gif"));

    }

    public javax.swing.ImageIcon getImage(int code) {
        return imagens[code];
    }
}
