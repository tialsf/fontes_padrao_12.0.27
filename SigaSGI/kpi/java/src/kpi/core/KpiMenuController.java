/*
 * KpiMenuController.java
 *
 * Created on 26 de Dezembro de 2005, 14:35
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package kpi.core;

import java.util.HashMap;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import kpi.core.KpiStaticReferences.UserType;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

/**
 *
 * @author Alexandre Alves da Silva
 */
public class KpiMenuController {

    private javax.swing.JMenuBar jMainBar;
    private kpi.xml.BIXMLVector vctRegras;
    private HashMap hashMenus = new HashMap();
    private HashMap hashRules = new HashMap();

    /**
     * Creates a new instance of KpiMenuController
     */
    public KpiMenuController() {
    }

    public javax.swing.JMenuBar getJMainBar() {
        return jMainBar;
    }

    public void setJMainBar(javax.swing.JMenuBar jMainBar) {
        this.jMainBar = jMainBar;
    }

    public kpi.xml.BIXMLVector getVctRegras() {
        return vctRegras;
    }

    public void setVctRegras(kpi.xml.BIXMLVector vctRegras) {
        this.vctRegras = vctRegras;
    }

    public IteMnuPermission getRuleByName(String name) {
        IteMnuPermission mnuPerm = null;

        for (BIXMLRecord recMenu : vctRegras) {

            if (recMenu.getAttributes().getString("NOME").equals(name)) {
                BIXMLVector vctIteRegra = recMenu.getBIXMLVector("ITEMREGRA");
                mnuPerm = new IteMnuPermission();

                for (int iteRegra = 0; iteRegra < vctIteRegra.size(); iteRegra++) {
                    BIXMLRecord recValor = vctIteRegra.get(iteRegra);
                    boolean rule = true;
                    int regraId = vctIteRegra.get(iteRegra).getAttributes().getInt("REGRAID");

                    if (KpiStaticReferences.getUserType() == UserType.USER) {
                        rule = recValor.getAttributes().getBoolean("VALOR");
                    }

                    mnuPerm.addPermissao(regraId, rule);
                }
            }
        }

        return mnuPerm;
    }


    /*
     * Aplica as regras de seguran�a ao menu.
     */
    public void applyRules() {
        applyRules(this.vctRegras);
    }

    public void applyRules(BIXMLVector vctRegras) {

        for (int regra = 0; regra < vctRegras.size(); regra++) {

            String originalKey = vctRegras.get(regra).getAttributes().getString("NOME");
            String key = getJoinedMenuName(originalKey);
            JMenuItem tmpMenu = (JMenuItem) hashMenus.get(key);
            IteMnuPermission ruleMenu = new IteMnuPermission();
            BIXMLVector vctItemRegra = vctRegras.get(regra).getBIXMLVector("ITEMREGRA");

            boolean hasMenuAccess = false;

            //Verifica se o usuario tem acesso a op��o do menu.
            for (int itemRegra = 0; itemRegra < vctItemRegra.size(); itemRegra++) {
                int regraValor = vctItemRegra.get(itemRegra).getAttributes().getInt("VALOR");
                int regraId = vctItemRegra.get(itemRegra).getAttributes().getInt("REGRAID");

                ruleMenu.addPermissao(regraId, regraValor == 1);

                if (tmpMenu != null && tmpMenu.getClientProperty("GRANTED") == null) {
                    if (!hasMenuAccess) {

                        if (regraValor == 1) {
                            hasMenuAccess = true;

                            if (key.equals("FORMMATRIX")) {
                                tmpMenu.putClientProperty("GRANTED", hasMenuAccess);
                            }
                        }

                        tmpMenu.setVisible(hasMenuAccess);
                    }
                }
            }

            if (KpiStaticReferences.getUserType() == KpiStaticReferences.UserType.USER) {
                if (!hashRules.containsKey(originalKey)) {
                    hashRules.put(originalKey, ruleMenu);
                }
            }
        }

        //Verifica se o menu na barra deve aparecer.
        for (int menu = 0; menu < jMainBar.getMenuCount(); menu++) {
            //ATEN��O. S� valida para o menu de cadastro =0, consultas menu =1 e relatorios menu = 2
            if (menu >= 0 && menu <= 2) {

                JMenu tempMenu = jMainBar.getMenu(menu);

                //Quando o menu superior foi desabilitado da sessao anterior nao deve ser habilitado depois.
                if (tempMenu.isVisible()) {
                    boolean hasAccess = false;

                    for (int itemMenu = 0; itemMenu < tempMenu.getMenuComponentCount(); itemMenu++) {
                        JMenuItem tempManuItem = tempMenu.getItem(itemMenu);

                        if (tempManuItem.isVisible()) {
                            hasAccess = true;
                            break;
                        }
                    }

                    tempMenu.setVisible(hasAccess);
                }
            }
        }
    }

    public void addHashMenus(String index, javax.swing.JMenuItem menu) {
        this.hashMenus.put(index, menu);
    }

    /**
     * @return the hashRules
     */
    public HashMap getMenuRules() {
        return hashRules;
    }

    /**
     * Retorna o nome do menu para itens de seguran�a que s�o acessiveis atrav�s
     * de um menu �nico.
     *
     * @param menuName Nome do menu
     * @return nome do menu que representa o item solicitado.
     */
    private String getJoinedMenuName(String menuName) {
        String nameMenu = menuName;

        if (menuName.equals("ORGANIZACOES")) {
            nameMenu = "FORMMATRIX";
        } else if (menuName.equals("PERSPECTIVAS")) {
            nameMenu = "FORMMATRIX";
        } else if (menuName.equals("ESTRATEGIAS")) {
            nameMenu = "FORMMATRIX";
        } else if (menuName.equals("OBJETIVOS")) {
            nameMenu = "FORMMATRIX";
        }

        return nameMenu;
    }
}