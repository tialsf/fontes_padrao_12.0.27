/*
 * KpiOperationBeingExecutedException.java
 *
 * Created on June 3, 2003, 11:39 AM
 */

package kpi.core;

/**
 *
 * @author  siga1996
 */
public class KpiOperationBeingExecutedException extends java.lang.RuntimeException {
	
	/**
	 * Creates a new instance of <code>KpiOperationBeingExecutedException</code> without detail message.
	 */
	public KpiOperationBeingExecutedException() {
	}
	
	
	/**
	 * Constructs an instance of <code>KpiOperationBeingExecutedException</code> with the specified detail message.
	 * @param msg the detail message.
	 */
	public KpiOperationBeingExecutedException(String msg) {
		super(msg);
	}
}
