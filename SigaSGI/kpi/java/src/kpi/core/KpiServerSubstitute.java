/*
 * KpiServerSubstitute.java
 *
 * Created on September 9, 2003, 9:14 AM
 */

package kpi.core;

/**
 *
 * @author  siga1996
 */
public class KpiServerSubstitute {
	
	private final String error = new String(
	"<RESPOSTAS><RESPOSTA><STATUS MSG=\"No Server Mode ON - Parāmetros desconhecidos.\">" +
				KpiDataController.KPI_ST_GENERALERROR +	"</STATUS></RESPOSTA></RESPOSTAS>");
	
	public KpiServerSubstitute() {
	}
				
	public String sendData ( String xml ) {
		String retorno = null;
		String fileContent = null;
		kpi.core.KpiDebug.println("************ VIRTUAL SERVER **************");
		kpi.core.KpiDebug.println(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BscServerSubstitute_00001"));
		kpi.core.KpiDebug.println( xml );
		kpi.xml.BIXMLVector vector = new kpi.xml.BIXMLVector();
		vector.parseXML( xml );
		if ( vector.get(0).getString("COMANDO").equals("ARVORE") ) {
			if ( vector.get(0).getString("TIPO").equals("ORGANIZACAO") ) {
				//fileContent = BscCommunicationController.getFile(this.getClass().getResource("/kpi/core/serversubstitute/arvore.xml").toString());
			}
			else {
				retorno = error;
			}
		}
		else
			if ( vector.get(0).getString("TIPO").equals("DASHBOARD") ) {
				if ( vector.get(0).getString("COMANDO").equals("CARREGAR") ) {
					//fileContent = BscCommunicationController.getFile(this.getClass().getResource("/kpi/core/serversubstitute/dashboardcarregar.xml").toString());
				}
				else {
					if ( vector.get(0).getString("COMANDO").equals("ALTERAR")  ||
							vector.get(0).getString("COMANDO").equals("EXCLUIR") ) {
						//fileContent = BscCommunicationController.getFile(this.getClass().getResource("/kpi/core/serversubstitute/dashboardalterar.xml").toString());
					}
					else
						retorno = error;
				}
			}
			else
				if ( vector.get(0).getString("TIPO").equals("PLANILHA") ) {
						//fileContent = BscCommunicationController.getFile(this.getClass().getResource("/kpi/core/serversubstitute/teste.xml").toString());
				}
				else
					retorno = error;
		
		if ( fileContent != null ) {
			kpi.core.KpiDebug.println(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BscServerSubstitute_00002"));
			kpi.core.KpiDebug.println( fileContent );
			kpi.core.KpiDebug.println("******** END VIRTUAL SERVER **************");
			retorno = fileContent;
		}
		
		return retorno;
	}
}
