/*
 * KpiStaticReferences.java
 *
 * Created on 18 de Dezembro de 2003, 11:42
 */
package kpi.core;

import java.util.Locale;

/**
 *
 * @author  siga1728
 */
public class KpiStaticReferences {

    /**
     * @return the m_ThousandSeparator
     */
    public static String getThousandSeparator() {
        return m_ThousandSeparator;
    }

    /**
     * @param aM_ThousantSeparator the m_ThousandSeparator to set
     */
    public static void setThousandSeparator(String ThousantSeparator) {
        m_ThousandSeparator = ThousantSeparator;
    }

    /**
     * @return the m_DecimalSeparator
     */
    public static String getDecimalSeparator() {
        return m_DecimalSeparator;
    }

    /**
     * @param aM_DecimalSeparator the m_DecimalSeparator to set
     */
    public static void setDecimalSeparator(String DecimalSeparator) {
        m_DecimalSeparator = DecimalSeparator;
    }

    public static enum UserType {
	ADMIN, USER
    }
    
    public final static int KPI_FREQ_ANUAL = 1;
    public final static int KPI_FREQ_SEMESTRAL = 2;
    public final static int KPI_FREQ_TRIMESTRAL = 3;
    public final static int KPI_FREQ_QUADRIMESTRAL = 4;
    public final static int KPI_FREQ_BIMESTRAL = 5;
    public final static int KPI_FREQ_MENSAL = 6;
    public final static int KPI_FREQ_QUINZENAL = 7;
    public final static int KPI_FREQ_SEMANAL = 8;
    public final static int KPI_FREQ_DIARIA = 9;
    public final static int KPI_APPLET = 0;
    public final static int KPI_APLICATION = 1;
    public final static int CAD_ORGANIZACAO = 1;
    public final static int CAD_ESTRATEGIA = 2;
    public final static int CAD_PERSPECTIVA = 3;
    public final static int CAD_OBJETIVO = 4;
    public final static int CAD_INDICADOR = 5;
    public final static String PDCA_MODE = "1";
    public final static String BSC_MODE = "2";
    public final static String GRUPOACAO_NAO_INICIADO =	"1";
    public final static String GRUPOACAO_EM_APROVACAO =	"2";
    public final static String GRUPOACAO_APROVADO = "3";
    public final static String ACAO_NAO_INICIADA = "1";
    public final static String ACAO_EM_EXECUCAO	= "2";
    public final static String ACAO_EXECUTADA = "3";
    public final static String ACAO_PENDENTE ="4";
    public final static String ACAO_ADIADA ="5";
    public final static String ACAO_CANCELADA ="6";

    private static String m_kpiServerVersion;
    private static String m_SystemMode;
    private static int m_kpiExecutionType;
    private static kpi.applet.KpiApplication m_kpiAplication;
    private static kpi.applet.KpiApplet m_kpiApplet;
    private static kpi.applet.KpiMainPanel m_kpiMainPanel;
    private static kpi.core.KpiImageResources m_kpiImageResources;
    private static kpi.swing.KpiDefaultDialogSystem m_kpiDialogSystem;
    private static kpi.core.KpiFormController m_kpiFormController;
    private static kpi.core.KpiDataController m_kpiDataController;
    private static java.util.Locale m_kpiDefaultLocale;
    private static kpi.core.KpiDateBase m_kpiDateBase;
    private static KpiCustomLabels m_kpiCustomLabels = new KpiCustomLabels();
    private static UserType userType = UserType.USER;
    private static String m_DecimalSeparator;
    private static String m_ThousandSeparator;
    
    /** Getter for property kpiApplet.
     * @return Value of property kpiApplet.
     *
     */
    public static kpi.applet.KpiApplet getKpiApplet() {
	return m_kpiApplet;
    }

    /** Setter for property kpiApplet.
     * @param kpiApplet New value of property kpiApplet.
     *
     */
    public static void setKpiApplet(kpi.applet.KpiApplet kpiApplet) {
	m_kpiApplet = kpiApplet;
    }

    /** Getter for property kpiDialogSystem.
     * @return Value of property kpiDialogSystem.
     *
     */
    public static kpi.swing.KpiDefaultDialogSystem getKpiDialogSystem() {
	return m_kpiDialogSystem;
    }

    /** Setter for property kpiDialogSystem.
     * @param kpiDialogSystem New value of property kpiDialogSystem.
     *
     */
    public static void setKpiDialogSystem(kpi.swing.KpiDefaultDialogSystem kpiDialogSystem) {
	m_kpiDialogSystem = kpiDialogSystem;
    }

    /** Getter for property kpiFormController.
     * @return Value of property kpiFormController.
     *
     */
    public static kpi.core.KpiFormController getKpiFormController() {
	return m_kpiFormController;
    }

    /** Setter for property kpiFormController.
     * @param kpiFormController New value of property kpiFormController.
     *
     */
    public static void setKpiFormController(kpi.core.KpiFormController kpiFormController) {
	m_kpiFormController = kpiFormController;
    }

    /** Getter for property kpiImageResources.
     * @return Value of property kpiImageResources.
     *
     */
    public static kpi.core.KpiImageResources getKpiImageResources() {
	return m_kpiImageResources;
    }

    /** Setter for property kpiImageResources.
     * @param kpiImageResources New value of property kpiImageResources.
     *
     */
    public static void setKpiImageResources(kpi.core.KpiImageResources kpiImageResources) {
	m_kpiImageResources = kpiImageResources;
    }

    /** Getter for property kpiMainPanel.
     * @return Value of property kpiMainPanel.
     *
     */
    public static kpi.applet.KpiMainPanel getKpiMainPanel() {
	return m_kpiMainPanel;
    }

    /** Setter for property kpiMainPanel.
     * @param kpiMainPanel New value of property kpiMainPanel.
     *
     */
    public static void setKpiMainPanel(kpi.applet.KpiMainPanel kpiMainPanel) {
	m_kpiMainPanel = kpiMainPanel;
    }

    /** Getter for property m_kpiDataController.
     * @return Value of property m_kpiDataController.
     *
     */
    public static kpi.core.KpiDataController getKpiDataController() {
	return m_kpiDataController;
    }

    /** Setter for property m_kpiDataController.
     * @param m_kpiDataController New value of property m_kpiDataController.
     *
     */
    public static void setKpiDataController(kpi.core.KpiDataController kpiDataController) {
	m_kpiDataController = kpiDataController;
    }

    /** Getter for property m_kpiDefaultLocale.
     * @return Value of property m_kpiDefaultLocale.
     *
     */
    public static java.util.Locale getKpiDefaultLocale() {
	if (m_kpiDefaultLocale == null) {
	    m_kpiDefaultLocale  = new java.util.Locale("pt_BR");
            setThousandSeparator(".");
            setDecimalSeparator(",");
	}
	return m_kpiDefaultLocale;
    }

    /** Setter for property m_kpiDefaultLocale.
     * @param m_kpiDefaultLocale New value of property m_kpiDefaultLocale.
     *
     */
    public static void setKpiDefaultLocale(String language) {
	Locale locale;

	/*Identifica a linguagem do RPO do Protheus.*/
	if (language.equalsIgnoreCase("SPANISH")) {
	    locale = new java.util.Locale("es");
            setThousandSeparator(",");
            setDecimalSeparator(".");
	} else if (language.equalsIgnoreCase("ENGLISH")) {
	    locale = new java.util.Locale("en");
            setThousandSeparator(",");
            setDecimalSeparator(".");
	} else {
	    locale = new java.util.Locale("pt_BR");
            setThousandSeparator(".");
            setDecimalSeparator(",");
	}

	m_kpiDefaultLocale = locale;
    }

    /**
     * Getter for property m_kpiServerVersion.
     * @return Value of property m_kpiServerVersion.
     */
    public static java.lang.String getKpiServerVersion() {
	return m_kpiServerVersion;
    }

    /**
     * Setter for property m_kpiServerVersion.
     * @param kpiServerVersion New value of property kpiServerVersion.
     */
    public static void setKpiServerVersion(java.lang.String kpiServerVersion) {
	m_kpiServerVersion = kpiServerVersion;
    }

    public static kpi.core.KpiDateBase getKpiDateBase() {
	return m_kpiDateBase;
    }

    public static void setKpiDateBase(kpi.core.KpiDateBase kpiDateBase) {
	m_kpiDateBase = kpiDateBase;
    }

    public static int getKpiExecutionType() {
	return m_kpiExecutionType;
    }

    public static void setKpiExecutionType(int kpiExecutionType) {
	m_kpiExecutionType = kpiExecutionType;
    }

    public static kpi.applet.KpiApplication getKpiApplication() {
	return m_kpiAplication;
    }

    public static void setKpiApplication(kpi.applet.KpiApplication kpiAplication) {
	m_kpiAplication = kpiAplication;
    }

    /**
     * @return the m_kpiCustomLabels
     */
    public static kpi.core.KpiCustomLabels getKpiCustomLabels() {
	return m_kpiCustomLabels;
    }

    /**
     * @param aM_kpiCustomLabels the m_kpiCustomLabels to set
     */
    public static void setKpiCustomLabels(kpi.core.KpiCustomLabels aM_kpiCustomLabels) {
	m_kpiCustomLabels = aM_kpiCustomLabels;
    }

    /**
     * Tipo de opera��o do sistema BSC ou PDCA.
     * @return the m_SystemMode
     */
    public static String getSystemMode() {
	return m_SystemMode;
    }

    /**
     * Tipo de opera��o do sistema BSC ou PDCA.
     * @param aM_SystemMode the m_SystemMode to set
     */
    public static void setSystemMode(String aM_SystemMode) {
	m_SystemMode = aM_SystemMode;
    }

    /**
     * @return the userType
     */
    public static UserType getUserType() {
	return userType;
    }

    /**
     * @param aUserType the userType to set
     */
    public static void setUserType(boolean isAdmin) {
	if (isAdmin){
	    userType = UserType.ADMIN;
	}else{
	    userType = UserType.USER;	    
	}
    }
    /**
     * Retorna o controle de menus.
     * @return
     */
    public static KpiMenuController getMnuController(){
	return getKpiFormController().getKpiMainPanel().getMnuController();
    }
}