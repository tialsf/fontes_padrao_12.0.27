/*
 * KpiSwingWorker.java
 *
 * Created on 3 de Novembro de 2004, 09:32
 */

package kpi.core;

/**
 *
 * @author  alexandreas
 */
public abstract class KpiSwingWorker extends SwingWorker {
	
	private kpi.swing.KpiTree threadTree = null;
	
	/**
	 * Creates a new instance of KpiSwingWorker
	 */
	public KpiSwingWorker(kpi.swing.KpiTree threadTree) {
		super();
		if(threadTree != null){
			this.threadTree = threadTree;
		}
	}
	
	/**
	 * Getter for property threadTree.
	 * @return Value of property threadTree.
	 */
	public  kpi.swing.KpiTree getThreadTree() {
		return threadTree;
	}
	
	/**
	 * Setter for property threadTree.
	 * @param threadTree New value of property threadTree.
	 */
	public void setThreadTree(kpi.swing.KpiTree threadTree) {
		this.threadTree = threadTree;
	}
}
