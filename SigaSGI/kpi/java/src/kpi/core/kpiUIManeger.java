/*
 * kpiUIManeger.java
 *
 * Created on 22 de Novembro de 2006, 17:53
 * SIGA 2516 - Lucio Pelinson
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package kpi.core;

import java.awt.Color;
import javax.swing.UIManager;

/**
 *
 * @author lucio.pelinson
 */
public class kpiUIManeger {

    /** Creates a new instance of kpiUIManeger */
    public kpiUIManeger() {
        Color colorBgMenu = new Color(51, 51, 51);
        Color colorFrMenu = new Color(102, 102, 102);

        UIManager.put("Menu.background", Color.black);
        UIManager.put("MenuItem.background", colorBgMenu);
        UIManager.put("Menu.foreground", Color.white);
        UIManager.put("Menu.selectionBackground", colorFrMenu);
        UIManager.put("Menu.opaque", true);
        UIManager.put("Menu.borderPainted", false);
        UIManager.put("Menu.border", null);

        UIManager.put("MenuItem.foreground", Color.white);
        UIManager.put("MenuItem.selectionBackground", colorFrMenu);
        UIManager.put("MenuItem.acceleratorForeground", Color.gray);
        UIManager.put("MenuItem.acceleratorSelectionForeground", Color.orange);
        UIManager.put("MenuItem.opaque", true);

        UIManager.put("RadioButtonMenuItem.foreground", Color.white);
        UIManager.put("RadioButtonMenuItem.selectionBackground",colorFrMenu);
        UIManager.put("RadioButtonMenuItem.acceleratorForeground", Color.gray);
        UIManager.put("RadioButtonMenuItem.acceleratorSelectionForeground", Color.orange);
        UIManager.put("RadioButtonMenuItem.opaque", true);
        UIManager.put("RadioButtonMenuItem.background", colorBgMenu);

        //UIManager.put("SplitPane.background", new Color(200, 217, 223));
        UIManager.put("ToolTip.background", Color.white);
        //UIManager.put("Panel.background",Color.green);
        
    }

    public void setFileChooser() {

        UIManager.put("FileChooser.lookInLabelMnemonic", new Integer('E'));
        //"Examinar"
        UIManager.put("FileChooser.lookInLabelText", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00001"));
        //"Salvar em"
        UIManager.put("FileChooser.saveInLabelText", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00002"));

        UIManager.put("FileChooser.fileNameLabelMnemonic", new Integer('N'));
        //"Nome do arquivo"
        UIManager.put("FileChooser.fileNameLabelText", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00003"));

        UIManager.put("FileChooser.filesOfTypeLabelMnemonic", new Integer('T'));
        //Tipo
        UIManager.put("FileChooser.filesOfTypeLabelText", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00004"));

        //"Um n�vel acima"
        UIManager.put("FileChooser.upFolderToolTipText", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00005"));
        UIManager.put("FileChooser.upFolderAccessibleName", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00005"));

        //"Desktop"
        UIManager.put("FileChooser.homeFolderToolTipText", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00006"));
        UIManager.put("FileChooser.homeFolderAccessibleName", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00006"));

        //"Criar nova pasta"
        UIManager.put("FileChooser.newFolderToolTipText", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00007"));
        UIManager.put("FileChooser.newFolderAccessibleName", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00007"));

        //"Lista"
        UIManager.put("FileChooser.listViewButtonToolTipText", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00008"));
        UIManager.put("FileChooser.listViewButtonAccessibleName", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00008"));

        //"Detalhes"
        UIManager.put("FileChooser.detailsViewButtonToolTipText", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00009"));
        UIManager.put("FileChooser.detailsViewButtonAccessibleName", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00009"));

        //"Nome"
        UIManager.put("FileChooser.fileNameHeaderText", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00010"));
        //"Tamanho"
        UIManager.put("FileChooser.fileSizeHeaderText", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00011"));
        //"Tipo"
        UIManager.put("FileChooser.fileTypeHeaderText", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00004"));
        //"Data"
        UIManager.put("FileChooser.fileDateHeaderText", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00012"));
        //"Atributos"
        UIManager.put("FileChooser.fileAttrHeaderText", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00013"));

        //"Cancelar"
        UIManager.put("FileChooser.cancelButtonText", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00014"));
        UIManager.put("FileChooser.cancelButtonToolTipText", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("kpiUIManeger_00014"));
        

    }
}
