package kpi.net;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;
import kpi.core.KpiDebug;
import kpi.core.KpiStaticReferences;

public class BIHttpClient {

    public final int TIMEOUT = 300000; //Define o tempo de TimeOut em cinco minutos.
    URL defaultUrl;
    String sessionId = "";

    public BIHttpClient(String url, String sessionID) {
        this.sessionId = sessionID;

        try {
            this.defaultUrl = new java.net.URL(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    
    public void setSessionID(String value) {
        sessionId = String.valueOf(value);
    }

    
    /**
     * Gerencia a utilização e utilização do SESSIONID.
     * @param connection Objeto URLConnection em uso.
     * @since 09-04-2010
     */
    private void doManageSession(HttpURLConnection connection) {
        if (!sessionId.isEmpty()) {
            connection.setRequestProperty("Cookie", "SESSIONID=" + sessionId);
        }
    }

    public String doGet() throws BIHttpException {
        String line;
        StringBuilder buffer = new StringBuilder();
        InputStream input;
        BufferedReader dataInput;

        try {
            HttpURLConnection connection = (HttpURLConnection) defaultUrl.openConnection();
            connection.setConnectTimeout(TIMEOUT);
            connection.setReadTimeout(TIMEOUT);
            connection.setRequestProperty("Referer", defaultUrl.toString());
            doManageSession(connection);

            input = connection.getInputStream();
            dataInput = new BufferedReader(new InputStreamReader(input, "ISO-8859-1"));

            while ((line = dataInput.readLine()) != null) {
                buffer.append(line);
                buffer.append('\n');
            }
            dataInput.close();
            connection.disconnect();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return buffer.toString();
    }

    public String doGet(String strUrl) throws BIHttpException {
        String line;
        StringBuilder buffer = new StringBuilder();
        java.io.InputStream input;
        java.io.BufferedReader dataInput;

        try {
            URL url = new URL(strUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(TIMEOUT);
            connection.setReadTimeout(TIMEOUT);
            connection.setRequestProperty("Referer", defaultUrl.toString());
            connection.setUseCaches(false);
            doManageSession(connection);

            input = connection.getInputStream();
            dataInput = new java.io.BufferedReader(new java.io.InputStreamReader(input, "ISO-8859-1"));

            while ((line = dataInput.readLine()) != null) {
                buffer.append(line);
                buffer.append('\n');
            }
            dataInput.close();
            connection.disconnect();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return buffer.toString();
    }

    public String doPost(String data) throws BIHttpException {
        String line;
        StringBuilder buffer = new StringBuilder();
        InputStream input;
        BufferedReader dataInput;
        try {
            HttpURLConnection connection = (HttpURLConnection) defaultUrl.openConnection();
            connection.setConnectTimeout(TIMEOUT);
            connection.setReadTimeout(TIMEOUT);
            connection.setRequestProperty("Referer", defaultUrl.toString());
            doManageSession(connection);

            connection.setDoOutput(true);

            PrintWriter out = new PrintWriter(connection.getOutputStream());
            out.println("kpicontent=" + URLEncoder.encode(data, "ISO-8859-1"));
            out.close();

            input = connection.getInputStream();
            dataInput = new BufferedReader(new InputStreamReader(input, "ISO-8859-1"));

            while ((line = dataInput.readLine()) != null) {
                buffer.append(line);
                buffer.append('\n');
            }
            dataInput.close();

            KpiDebug.println("*** Request ***");
            KpiDebug.println("URL: " + defaultUrl);
            KpiDebug.println("Method: " + connection.getRequestMethod());
            KpiDebug.println("Cookie: " + connection.getRequestProperty("Cookie"));
            KpiDebug.println("Request Body:\nkpicontent=\n" + data);
            KpiDebug.println();
            KpiDebug.println("*** Response ***");
            KpiDebug.println("Response Code: " + connection.getResponseCode());
            KpiDebug.println("Response Message: " + connection.getResponseMessage());
            KpiDebug.println("Response Body:\n" + buffer.toString());
            KpiDebug.println();
            connection.disconnect();

        } catch (Exception e) {
            System.out.println(data);
            throw new BIHttpException(java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("BIHttpClient_00001") + e.getMessage());
        }

        return buffer.toString();
    }

    public String doPost(String strUrl, String data) throws BIHttpException {
        String line;
        StringBuilder buffer = new StringBuilder();
        java.io.InputStream input;
        java.io.BufferedReader dataInput;
        try {
            java.net.URL url = new java.net.URL(strUrl);
            java.net.HttpURLConnection connection = (java.net.HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(TIMEOUT);
            connection.setReadTimeout(TIMEOUT);
            connection.setRequestProperty("Referer", defaultUrl.toString());
            doManageSession(connection);

            connection.setDoOutput(true);
            java.io.PrintWriter out = new java.io.PrintWriter(connection.getOutputStream());
            out.println("kpicontent=" + java.net.URLEncoder.encode(data, "ISO-8859-1"));
            out.close();

            input = connection.getInputStream();
            dataInput = new java.io.BufferedReader(new java.io.InputStreamReader(input, "ISO-8859-1"));

            while ((line = dataInput.readLine()) != null) {
                buffer.append(line);
                buffer.append('\n');
            }
            dataInput.close();

            KpiDebug.println("*** Request ***");
            KpiDebug.println("URL: " + url);
            KpiDebug.println("Method: " + connection.getRequestMethod());
            KpiDebug.println("Cookie: " + connection.getRequestProperty("Cookie"));
            KpiDebug.println("Request Body:\nkpicontent=\n" + data);
            KpiDebug.println();
            KpiDebug.println("*** Response ***");
            KpiDebug.println("Response Code: " + connection.getResponseCode());
            KpiDebug.println("Response Message: " + connection.getResponseMessage());
            KpiDebug.println("Response Body:\n" + buffer.toString());
            KpiDebug.println();
            connection.disconnect();
        } catch (Exception e) {
            System.out.println(data);
            throw new BIHttpException(java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("BIHttpClient_00001") + e.getMessage());
        }

        return buffer.toString();
    }

    public String doLogin(String strUrl, String username, String password)
            throws BIHttpException {
        StringBuilder buffer = new StringBuilder();
        try {
            java.net.HttpURLConnection connection;
            java.net.URL url;
            String line;
            String data;
            java.io.InputStream input;
            java.io.BufferedReader dataInput;

            url = new java.net.URL(strUrl);
            connection = (java.net.HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(TIMEOUT);
            connection.setReadTimeout(TIMEOUT);
            connection.setRequestProperty("Referer", defaultUrl.toString());
            doManageSession(connection);

            connection.setDoOutput(true);
            java.io.PrintWriter out = new java.io.PrintWriter(connection.getOutputStream());

            data = "login=" + java.net.URLEncoder.encode("true", "ISO-8859-1");
            data += "&username=" + java.net.URLEncoder.encode(username, "ISO-8859-1");
            data += "&password=" + java.net.URLEncoder.encode(password, "ISO-8859-1");
            out.println(data);
            out.close();

            input = connection.getInputStream();
            dataInput = new java.io.BufferedReader(new java.io.InputStreamReader(input, "ISO-8859-1"));
            while ((line = dataInput.readLine()) != null) {
                buffer.append(line);
                buffer.append('\n');
            }
            dataInput.close();

            KpiDebug.println("*** Request ***");
            KpiDebug.println("URL: " + url);
            KpiDebug.println("Method: " + connection.getRequestMethod());
            KpiDebug.println("Cookie: " + connection.getRequestProperty("Cookie"));
            KpiDebug.println("Request Body:\nkpicontent=\n" + data);
            KpiDebug.println();
            KpiDebug.println("*** Response ***");
            KpiDebug.println("Response Code: " + connection.getResponseCode());
            KpiDebug.println("Response Message: " + connection.getResponseMessage());
            KpiDebug.println("Response Body:\n" + buffer.toString());
            KpiDebug.println();
            connection.disconnect();
        } catch (Exception e) {
            throw new BIHttpException(java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("BIHttpClient_00001") + e.getMessage());
        }

        return buffer.toString();
    }
}
