/*
 * BIHttpException.java
 *
 * Created on May 15, 2003, 2:50 PM
 */

package kpi.net;

/**
 *
 * @author  siga1996
 */
public class BIHttpException extends java.lang.RuntimeException {
    
    /**
     * Creates a new instance of <code>BIHttpException</code> without detail message.
     */
    public BIHttpException() {
    }
    
    
    /**
     * Constructs an instance of <code>BIHttpException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public BIHttpException(String msg) {
	super(msg);
    }
}
