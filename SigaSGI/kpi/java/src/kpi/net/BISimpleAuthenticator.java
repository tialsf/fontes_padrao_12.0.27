/*
 * BISimpleAuthenticator.java
 *
 * Created on October 9, 2003, 2:41 PM
 */

package kpi.net;

/**
 *
 * @author  siga1996
 */
public class BISimpleAuthenticator extends java.net.Authenticator {
    private String username,
	    password;
    
    public BISimpleAuthenticator(String username,String password) {
	this.username = username;
	this.password = password;
    }
    
    protected java.net.PasswordAuthentication getPasswordAuthentication() {
	return new java.net.PasswordAuthentication(
		username,password.toCharArray());
    }
}
