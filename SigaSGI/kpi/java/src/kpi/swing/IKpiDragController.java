package kpi.swing;

/**
 *
 * @author gilmar.pereira
 */
public interface IKpiDragController {
    public void kpiMouseClicked(KpiMouseEvent e);
    public void kpiMouseEntered(KpiMouseEvent e);
    public void kpiMouseExited(KpiMouseEvent e);
    public void kpiMousePressed(KpiMouseEvent e);
    public void kpiMouseReleased(KpiMouseEvent e);
    public void kpiMouseDragged(KpiMouseEvent e);
    public void kpiMouseMoved(KpiMouseEvent e);
}
