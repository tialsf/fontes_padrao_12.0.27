/*
 * KpiChangeImage.java
 *
 * Created on 21 de Setembro de 2007, 10:53
 * SIGA 2516 - Lucio Pelinson
 *
 */
package kpi.swing;

import java.awt.event.ComponentEvent;
import kpi.swing.analisys.KpiScoreCarding;

public class KpiChangeImage extends kpi.swing.KpiInternalFrame implements kpi.swing.KpiDefaultFrameFunctions, kpi.swing.tools.KpiUpLoadAction {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String recordType = new String("KPICHANGEIMG");
    private String formType = recordType;//Tipo do Formulario
    private String parentType = recordType;//Tipo formulario pai, caso haja.
    private java.io.File oFile = null;
    private boolean isAdmin = false;
    private boolean isReset = false;

    public void enableFields() {
    }

    public void disableFields() {
    }

    public void refreshFields() {
        // Copia os dados da vari�vel "record" para os campos do formul�rio.
        // Ex.:
        // strategyList.setDataSource(
        //					record.getBIXMLVector("ESTRATEGIAS"), record.getString("ID") );
        if (!record.getBoolean("ISADMIN")) {
            kpi.swing.KpiDefaultDialogSystem oDlg = new kpi.swing.KpiDefaultDialogSystem(kpi.core.KpiStaticReferences.getKpiMainPanel());
            oDlg.warningMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiChangeImage_00013")); //"Acesso negado."
            this.dispose();
        } else {
            setVisible(true);
        }


    }

    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);

        return recordAux;
    }

    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    /*public boolean hasCombos() {
    return false;
    }*/
    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtPath = new javax.swing.JTextField();
        btnCancel = new javax.swing.JButton();
        btnOk = new javax.swing.JButton();
        jToolBar1 = new javax.swing.JToolBar();
        btnOpen = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        btnReset = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiChangeImage_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(600, 358));
        getContentPane().setLayout(null);

        jPanel1.setMinimumSize(new java.awt.Dimension(400, 200));
        jPanel1.setPreferredSize(new java.awt.Dimension(510, 320));
        jPanel1.setLayout(null);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("Este arquivo deve ser do tipo (*.gif, *.jpg, *.png, *.jpeg)  e recomendamos que");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(10, 30, 400, 30);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel2.setText(bundle.getString("KpiChangeImage_00002")); // NOI18N
        jPanel1.add(jLabel2);
        jLabel2.setBounds(10, 0, 400, 30);

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel3.setText("possua as medidas de 120x110 (ou proporcional) no tamnaho m�ximo de 50Kb.");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(10, 50, 400, 30);

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Imagem");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(10, 120, 70, 14);
        jPanel1.add(txtPath);
        txtPath.setBounds(90, 120, 285, 20);

        btnCancel.setText(bundle.getString("KpiChangeImage_00009")); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        jPanel1.add(btnCancel);
        btnCancel.setBounds(290, 170, 90, 23);

        btnOk.setText(bundle.getString("KpiChangeImage_00007")); // NOI18N
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });
        jPanel1.add(btnOk);
        btnOk.setBounds(50, 170, 90, 23);

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        btnOpen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_pastinha.gif"))); // NOI18N
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international"); // NOI18N
        btnOpen.setToolTipText(bundle1.getString("KpiEspinhaPeixeRel_00004")); // NOI18N
        btnOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOpenActionPerformed(evt);
            }
        });
        jToolBar1.add(btnOpen);

        jPanel1.add(jToolBar1);
        jToolBar1.setBounds(380, 120, 30, 20);

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel5.setText(bundle.getString("KpiChangeImage_00005")); // NOI18N
        jPanel1.add(jLabel5);
        jLabel5.setBounds(10, 70, 400, 30);

        btnReset.setText(bundle.getString("KpiChangeImage_00008")); // NOI18N
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });
        jPanel1.add(btnReset);
        btnReset.setBounds(150, 170, 130, 23);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 420, 210);

        getAccessibleContext().setAccessibleName("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetActionPerformed
            isReset = true;
            event.executeRecord("");
	}//GEN-LAST:event_btnResetActionPerformed

	private void btnOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOpenActionPerformed
            try {
                javax.swing.JFileChooser chooser = new javax.swing.JFileChooser();
                chooser.setDialogType(javax.swing.JFileChooser.CUSTOM_DIALOG);
                chooser.setApproveButtonText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00030")); //"Selecionar"
                chooser.setApproveButtonToolTipText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00031")); //"Selecionar Planilha"
                chooser.setAcceptAllFileFilterUsed(false);
                chooser.addChoosableFileFilter(new kpi.util.kpiFileFilter( new String[] {"png", "gif", "jpg","jpeg"} ));
                int retval = chooser.showDialog(this, null);
                if (retval == javax.swing.JFileChooser.APPROVE_OPTION) {
                    oFile = chooser.getSelectedFile();
                    txtPath.setText(oFile.getPath());
                }
            } catch (java.security.AccessControlException e) {
                kpi.swing.KpiDefaultDialogSystem oDlg = new kpi.swing.KpiDefaultDialogSystem(this);
                if (oDlg.confirmMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiChangeImage_00010"), javax.swing.JOptionPane.WARNING_MESSAGE) == javax.swing.JOptionPane.YES_OPTION) {
                    kpi.applet.KpiMainPanel.openUrlPolitica();
                }
            } catch (Exception e) {
                System.out.println(e);
            }
	}//GEN-LAST:event_btnOpenActionPerformed

	private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed
            if (oFile != null) {
                if (oFile.length() < 55000) {
                    this.setVisible(false);
                    kpi.swing.KpiDefaultFrameFunctions frame =
                            kpi.core.KpiStaticReferences.getKpiFormController().getForm("KPIUPLOAD", "0", "");
                    kpi.swing.tools.KpiUpload frmUpload = (kpi.swing.tools.KpiUpload) frame;
                    frmUpload.setPathDest("\\imagens\\");
                    frmUpload.setArqName("art_logo_clie.sgi");
                    frmUpload.setParentType("GERAPLANILHA");
                    frmUpload.setCloseBoxAfterUploaded(true);
                    frmUpload.addUpLoadAction(this);
                    frmUpload.startUpload(oFile);
                } else {
                    kpi.swing.KpiDefaultDialogSystem oDlg = new kpi.swing.KpiDefaultDialogSystem(this);
                    oDlg.warningMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiChangeImage_00011")); //"A imagem deve conter no m�ximo 50Kbytes."
                }
            } else {
                kpi.swing.KpiDefaultDialogSystem oDlg = new kpi.swing.KpiDefaultDialogSystem(this);
                oDlg.warningMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiChangeImage_00012")); //"Imagem n�o localizada."
            }
	}//GEN-LAST:event_btnOkActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            this.setVisible(false);
            this.dispose();
	}//GEN-LAST:event_btnCancelActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnOk;
    private javax.swing.JButton btnOpen;
    private javax.swing.JButton btnReset;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTextField txtPath;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiChangeImage(int operation, String idAux, String contextId) {
        setVisible(false);
        initComponents();
        this.setSize(425, 250);
        event.defaultConstructor(operation, idAux, contextId);
    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(recordType);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
    }

    public void showOperationsButtons() {
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public String getFormType() {
        return formType;
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return new javax.swing.JTabbedPane();
    }

    public String getParentType() {
        return parentType;
    }

    public void onUpLoadStart(ComponentEvent componentEvent) {
    }

    public void onUpLoadFinish(ComponentEvent componentEvent) {
    }

    public void onAfterExecute(ComponentEvent componentEvent) {
        oFile = null;
        kpi.swing.KpiDefaultFrameFunctions form;
        form = kpi.core.KpiStaticReferences.getKpiFormController().getFrameReference("SCORECARDING", "0");

        if (form != null) {

            ((KpiScoreCarding) form).refreshClientImage();
        }
        this.dispose();
    }

    public void onUpLoadError(ComponentEvent componentEvent) {
    }

    public void onUpLoadCancel(ComponentEvent componentEvent) {
    }

    public void onUpLoading(ComponentEvent componentEvent) {
    }

    //Retorno do ADVPL
    public void afterExecute() {
        if (isReset) {
            oFile = null;
            kpi.swing.KpiDefaultFrameFunctions form;
            form = kpi.core.KpiStaticReferences.getKpiFormController().getFrameReference("SCORECARDING", "0");

            if (form != null) {
                ((KpiScoreCarding) form).refreshClientImage();
            }
            this.dispose();
        }
    }
}
