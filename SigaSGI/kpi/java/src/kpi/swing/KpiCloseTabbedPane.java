package kpi.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * JTabbedPane com um icone no lado esquerdo e um ("X") para fechar cada tab.
 *
 */
public class KpiCloseTabbedPane extends JTabbedPane implements javax.swing.event.MouseInputListener {

    javax.swing.ImageIcon iconeFecharSel = new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/close-normal-sel.gif"));
    javax.swing.ImageIcon iconeFechar = new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/close-normal.gif"));
    javax.swing.ImageIcon icone = new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/close-normal.gif"));
    final static int POS_LARGURA = 13;//Valor para corre��o da posi��o dos bot�es na tab
    final static int POS_ALTURA = 1;//Valor para corre��o da posi��o dos bot�es na tab
    private java.util.Vector vctTabbedListeners;

    public KpiCloseTabbedPane() {
	super();
	addMouseListener(this);
	addMouseMotionListener(this);
	vctTabbedListeners = new java.util.Vector();
	setRequestFocusEnabled(false);
    }

    public void addTab(String title, Icon icon, Component component) {
	super.addTab(title.concat("  "), icon, component);
    }

    public void addTab(String title, Icon icon, Component component, kpi.swing.KpiDefaultFrameFunctions frameParent) {
	super.addTab(title.concat("  "), icon, component);
	javax.swing.JPanel tabTemp = (javax.swing.JPanel) getComponentAt(getComponentCount() - 1);
	tabTemp.putClientProperty("FrameParent", frameParent);
    }

    public void mouseClicked(MouseEvent e) {
	boolean lRemove = true;
	int tabNumber = getUI().tabForCoordinate(this, e.getX(), e.getY());
	if (tabNumber < 0) {
	    return;
	}

	Rectangle rect = getBoundsAt(tabNumber);
	int largura = rect.x + (int) rect.getWidth() - POS_LARGURA;
	int altura = rect.y + POS_ALTURA;

	Rectangle i = new Rectangle(largura, altura, iconeFechar.getIconWidth(), iconeFechar.getIconHeight());

	if (i.contains(e.getX(), e.getY())) {
	    for (int j = 0; j < vctTabbedListeners.size(); j++) {
		lRemove = true;
		((kpi.swing.KpiCloseTabbedPaneListener) vctTabbedListeners.get(j)).beforeTabRemoved(new java.awt.event.ComponentEvent(this, tabNumber));
		kpi.swing.KpiDefaultFrameFunctions frmFunctions = getDefaultFrameFunctions(new java.awt.event.ComponentEvent(this, tabNumber));
		if (frmFunctions.asJInternalFrame() instanceof kpi.swing.KpiInternalFrame) {
		    kpi.swing.KpiInternalFrame kpiInternalFrame = ((kpi.swing.KpiInternalFrame) frmFunctions.asJInternalFrame());
		    //Verifica se a janela pode ser removida.
		    if (!kpiInternalFrame.isCloseEnabled()) {
			lRemove = false;
		    }
		}

		if (lRemove) {
		    this.removeTabAt(tabNumber);
		    ((kpi.swing.KpiCloseTabbedPaneListener) vctTabbedListeners.get(j)).afterTabRemoved(new java.awt.event.ComponentEvent(this, tabNumber));
		}
	    }
	}
    }

    public void paint(Graphics g) {
	super.paint(g);
	int largura = 0;
	int altura = 0;
	for (int i = 0, n = getTabCount(); i < n; i++) {
	    Rectangle rect = getBoundsAt(i);
	    largura = rect.x + (int) rect.getWidth() - POS_LARGURA;
	    altura = rect.y + POS_ALTURA;
	    g.drawImage(icone.getImage(), largura, altura, this);
	}
    }

    public void mouseMoved(MouseEvent e) {
	paintButton(e);
    }

    public void addKpiCloseTabbedPaneListener(kpi.swing.KpiCloseTabbedPaneListener listener) {
	vctTabbedListeners.add(listener);
    }

    public void removeKpiCloseTabbedPaneListener(kpi.swing.KpiCloseTabbedPaneListener listener) {
	vctTabbedListeners.remove(listener);
    }

    public void mouseEntered(MouseEvent e) {
	paintButton(e);
    }

    public void mouseExited(MouseEvent e) {
	icone = iconeFechar;
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
    }

    private void paintButton(MouseEvent e) {
	int tabNumber = getUI().tabForCoordinate(this, e.getX(), e.getY());
	if (tabNumber < 0) {
	    return;
	}
	Graphics g = getGraphics();

	Rectangle rect = getBoundsAt(tabNumber);
	int largura = rect.x + (int) rect.getWidth() - POS_LARGURA;
	int altura = rect.y + POS_ALTURA;

	Rectangle i = new Rectangle(largura, altura, iconeFechar.getIconWidth(), iconeFechar.getIconHeight());

	if (i.contains(e.getX(), e.getY())) {
	    icone = iconeFecharSel;
	} else {
	    icone = iconeFechar;
	}
    }

    private kpi.swing.KpiDefaultFrameFunctions getDefaultFrameFunctions(java.awt.event.ComponentEvent componentEvent) {
	javax.swing.JPanel jTmpPanel = (javax.swing.JPanel) this.getComponentAt(componentEvent.getID());
	kpi.swing.KpiDefaultFrameFunctions frmFunctions = (kpi.swing.KpiDefaultFrameFunctions) jTmpPanel.getClientProperty("FrameParent");
	return frmFunctions;
    }
}