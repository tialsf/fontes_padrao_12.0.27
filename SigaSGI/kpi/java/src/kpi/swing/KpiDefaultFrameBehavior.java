/*
 * KpiDefaultEvents.java
 *
 * Created on July 8, 2003, 2:53 PM
 */
package kpi.swing;

import java.awt.Color;
import java.awt.event.ItemListener;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.plaf.basic.BasicComboBoxUI;
import kpi.applet.KpiMainPanel;
import kpi.beans.JBITreeSelection;
import kpi.core.KpiDataController;
import kpi.core.KpiEventTask;
import kpi.core.KpiStaticReferences;
import kpi.util.KpiComboString;
import kpi.util.KpiToolKit;
import kpi.xml.BIXMLAttributes;
import kpi.xml.BIXMLException;
import kpi.xml.BIXMLRecord;

/**
 *
 * @author siga1996
 */
public class KpiDefaultFrameBehavior {

    public final static int INSERTING = 1;
    public final static int UPDATING = 2;
    public final static int NORMAL = 3;
    public final static int CLOSE_FORM = 0;
    public final static int KEEP_FORM = 1;
    public final static Integer EVENT_ON_DELETE = 1;
    public final static Integer EVENT_ON_INSERT = 2;
    public final static Integer EVENT_ON_UPDATE = 3;
    public final static Integer EVENT_ON_REFRESH = 3;
    private KpiDefaultFrameFunctions frame;
    private KpiDataController dataController;
    private KpiDefaultDialogSystem dialogSystem;
    private boolean fecharpainel = true;
    private int onCloseFormOption = CLOSE_FORM;
    private HashMap<Integer, KpiEventTask> hshEvents = new HashMap<Integer, KpiEventTask>();

    /**
     * Registra um evento para ser executado
     *
     * @param typeEvent = Tipo do evento em que a tarefa dever� se executada.
     * @param task Nome de um m�todo que se deseja executar;
     */
    public void registerEvent(Integer typeEvent, KpiEventTask task) {
        hshEvents.put(typeEvent, task);
    }

    public void setFecharPainel(boolean fechar) {
        fecharpainel = fechar;
    }

    public void setNoServerMode(boolean nsm) {
        dataController.setNoServeMode(nsm);
    }

    public KpiDefaultFrameBehavior(KpiDefaultFrameFunctions frameAux) {

        frame = frameAux;
        dataController = kpi.core.KpiStaticReferences.getKpiDataController();
        dialogSystem = new KpiDefaultDialogSystem((JInternalFrame) frame);
    }

    public void defaultConstructor(int operation, String idAux, String contextId) {
        frame.setStatus(operation);
        if (operation == NORMAL) {
            frame.setID(idAux);
            frame.loadRecord();
            frame.disableFields();
            frame.showOperationsButtons();
        } else if (operation == INSERTING) {
            BIXMLRecord record = getRecordStructure(idAux);
            frame.setRecord(record);
            frame.refreshFields();
            frame.enableFields();
            frame.showDataButtons();
        }
    }
    
    /**
     * M�todo repons�vel por trocar os caracteres especiais que quebram o XML que � enviado para o Advpl.
     * @author Helio Leal
     * @since 22/01/2015
     * @param record
     * @return 
     */
    public BIXMLRecord fixString(BIXMLRecord record) {
        if (record.contains("NOME") && !(record.toString().startsWith("<SMTPCONF>"))) {
            String valorNome = record.getString("NOME").
                    replaceAll("&", "E").
                    replaceAll(">", "").
                    replaceAll("<", "").
                    replaceAll("@", "").
                    replaceAll("#", "").
                    replaceAll("%", "").
                    replaceAll("$", "");
            record.set("NOME", valorNome);
        }
        return record;
    }

    public boolean saveRecord() {
        StringBuffer errorMessage = new StringBuffer();

        boolean retorno = frame.validateFields(errorMessage);

        if (retorno) {

            BIXMLRecord record = frame.getFieldsContents();
            BIXMLRecord recordBeforeChanges = frame.getRecord();

            final BIXMLRecord recordFull = fixRecord(recordBeforeChanges, fixString(record));

            if (frame.getStatus() == INSERTING) {

                final kpi.core.KpiSwingWorker worker = new kpi.core.KpiSwingWorker(frameGetThreadTree()) {

                    @Override
                    public Object construct() {

                        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
                        String id = dataController.insertRecord(frame.getType(), recordFull);
                        return id;
                    }

                    @Override
                    public void finished() {

                        frame.setID((String) get());

                        if (dataController.getStatus() == KpiDataController.KPI_ST_OK) {
                            KpiStaticReferences.getKpiFormController().setNewDetailFormInfo(frame.getType(), frame.getID(), frame);
                            frame.loadRecord();
                        }

                        if (frame.getTabbedPane() != null) {

                            for (int i = 1; i < frame.getTabbedPane().getTabCount(); i++) {
                                frame.getTabbedPane().setEnabledAt(i, true);
                            }

                        }

                        refreshFrameStatus(getThreadTree());
                        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
                        if (hshEvents.containsKey(EVENT_ON_INSERT)) {
                            invoke(hshEvents.get(EVENT_ON_INSERT));
                        }
                    }
                };
                worker.start();

            } else if (frame.getStatus() == UPDATING) {

                final kpi.core.KpiSwingWorker worker = new kpi.core.KpiSwingWorker(frameGetThreadTree()) {

                    @Override
                    public Object construct() {
                        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
                        dataController.updateRecord(frame.getType(), recordFull);
                        return null;
                    }

                    @Override
                    public void finished() {
                        refreshFrameStatus(getThreadTree());
                        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));

                        if (dataController.getStatus() == KpiDataController.KPI_ST_OK) {
                            frame.loadRecord();
                        }

                        if (hshEvents.containsKey(EVENT_ON_UPDATE)) {
                            invoke(hshEvents.get(EVENT_ON_UPDATE));
                        }
                    }
                };

                worker.start();
            }

        } else {

            //Mensagem de erro ou aleta no processo de grava��o.
            dialogSystem.warningMessage(errorMessage.toString());
        }

        frame.asJInternalFrame().repaint();

        return retorno;
    }

    public void refreshFrameStatus(kpi.swing.KpiTree threadTree) {
        if (dataController.getStatus() == KpiDataController.KPI_ST_OK) {
            
            //Emite o alerta de informa��o quando o status for OK por�m existir mensagem de erro. 
            if (!dataController.getErrorMessage().isEmpty()) {
                dialogSystem.informationMessage(dataController.getErrorMessage());
            }
            
            //Atualiza as informa��es do formul�rio. 
            refreshParent();
            frame.asJInternalFrame().repaint();
            frame.showOperationsButtons();
            frame.disableFields();
            frame.setStatus(NORMAL);

        } else {
            frame.showDataButtons();
            if (dataController.getStatus() == KpiDataController.KPI_ST_UNIQUE) {
                dialogSystem.warningMessage(dataController.getErrorMessage());

            } else if (dataController.getStatus() == KpiDataController.KPI_ST_GENERALERROR) {
                dialogSystem.warningMessage(dataController.getErrorMessage());
                frame.setStatus(NORMAL);
                closeOperation();

            } else if (dataController.getStatus() == KpiDataController.KPI_ST_VALIDATION) {
                dialogSystem.warningMessage(dataController.getErrorMessage());

            } else {
                dialogSystem.errorMessage(dataController.getStatus());
                if (dataController.getStatus() == KpiDataController.KPI_ST_EXPIREDSESSION) {
                    KpiStaticReferences.getKpiApplet().resetApplet();
                }
            }
        }
    }

    public void closeOperation() {
        try {
            //Tipo da vizualiza��o que est� sendo usada. Janela ou pastas.
            if (!KpiStaticReferences.getKpiMainPanel().radJanela.isSelected()) {
                javax.swing.JPanel tmpPanel = (javax.swing.JPanel) frame.asJInternalFrame().getClientProperty("tmpPanel");
                tmpPanel.getParent().remove(tmpPanel);
            }
            frame.asJInternalFrame().setClosed(fecharpainel);
        } catch (java.beans.PropertyVetoException e) {
            kpi.core.KpiDebug.println(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiDefaultFrameBehavior_00002") + frame.getType() + "/" + frame.getID());
            e.printStackTrace();
        }
    }

    public void cancelOperation() {
        if (frame.getStatus() == INSERTING) {
            frame.setStatus(NORMAL);
            closeOperation();
        } else {
            frame.refreshFields();
            frame.asJInternalFrame().repaint();
            frame.showOperationsButtons();
            frame.disableFields();
            frame.setStatus(NORMAL);
        }
    }

    public void refreshParent() {
        KpiDefaultFrameFunctions parentFrame = null;
        String parentId = "0";
        if (!frame.getParentID().equals("")) {
            parentId = frame.getParentID();
        }

        parentFrame = KpiStaticReferences.getKpiFormController().getParentForm(
                frame.getParentType(), parentId);

        if (parentFrame != null) {
            parentFrame.loadRecord();
        }
    }

    public void deleteRecord() {
        deleteRecord("");
    }

    public void deleteRecord(String delCMD) {
        if (dialogSystem.confirmDeletion() && !frame.getID().equals("0")) {
            final String finalDelCMD = delCMD;
            final String id = frame.getID();
            final String type = frame.getType();
            final kpi.core.KpiSwingWorker worker = new kpi.core.KpiSwingWorker(frameGetThreadTree()) {

                @Override
                public Object construct() {
                    frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
                    dataController.deleteRecord(id, type, finalDelCMD);
                    return null;
                }

                //Runs on the event-dispatching thread.
                @Override
                public void finished() {
                    if (dataController.getStatus() == KpiDataController.KPI_ST_OK) {
                        //Execu��o do m�todo default.
                        boolean doDefault = true;

                        if (hshEvents.containsKey(EVENT_ON_DELETE)) {
                            doDefault = hshEvents.get(EVENT_ON_DELETE).doDefAction();
                        }

                        if (doDefault) {
                            refreshParent();
                        }

                        if (onCloseFormOption == CLOSE_FORM) {
                            closeOperation();
                            try {
                                //Tipo da vizualiza��o que est� sendo usada. Janela ou pastas.
                                if (!KpiStaticReferences.getKpiMainPanel().radJanela.isSelected()) {
                                    KpiStaticReferences.getKpiFormController().removeDesktopContainer();
                                }
                                frame.asJInternalFrame().setClosed(fecharpainel);
                            } catch (java.beans.PropertyVetoException e) {
                                dialogSystem.warningMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiDefaultFrameBehavior_00003"));
                            }
                        }
                        //Executa o evento especifico da classe.
                        if (hshEvents.containsKey(EVENT_ON_DELETE)) {
                            invoke(hshEvents.get(EVENT_ON_DELETE));
                        }
                    } else {
                        if (dataController.getStatus() == KpiDataController.KPI_ST_GENERALERROR) {
                            dialogSystem.warningMessage(dataController.getErrorMessage());
                            setFecharPainel(true);
                            frame.setStatus(NORMAL);
                            closeOperation();
                        } else if (dataController.getStatus() == KpiDataController.KPI_ST_VALIDATION) {
                            dialogSystem.warningMessage(dataController.getErrorMessage());
                        } else {
                            dialogSystem.errorMessage(dataController.getStatus());

                            if (dataController.getStatus() == KpiDataController.KPI_ST_EXPIREDSESSION) {
                                KpiStaticReferences.getKpiApplet().resetApplet();
                            }
                        }
                    }
                    frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
                }
            };
            worker.start();
        } else {
            //Executa o evento especifico da classe.
            if (hshEvents.containsKey(EVENT_ON_DELETE)) {
                invoke(hshEvents.get(EVENT_ON_DELETE));
            }
        }
    }

    public void editRecord() {
        frame.setStatus(UPDATING);
        frame.showDataButtons();
        frame.enableFields();
        frame.asJInternalFrame().repaint();
    }

    public void loadRecord() {
        final String id = frame.getID();
        final String type = frame.getType();
        final kpi.core.SwingWorker worker = new kpi.core.SwingWorker() {

            @Override
            public Object construct() {
                frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
                kpi.xml.BIXMLRecord recordAux = dataController.loadRecord(id, type);
                return recordAux;
            }

            @Override
            public void finished() {
                frame.setRecord((kpi.xml.BIXMLRecord) get());

                if (dataController.getStatus() == KpiDataController.KPI_ST_OK) {
                    frame.refreshFields();
                    frame.asJInternalFrame().repaint();
                } else {
                    if (dataController.getStatus() == KpiDataController.KPI_ST_GENERALERROR) {
                        dialogSystem.warningMessage(dataController.getErrorMessage());
                    } else {
                        dialogSystem.errorMessage(dataController.getStatus());
                    }

                    if (dataController.getStatus() == KpiDataController.KPI_ST_EXPIREDSESSION) {
                        KpiStaticReferences.getKpiApplet().resetApplet();
                    }
                }
                frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(
                        java.awt.Cursor.DEFAULT_CURSOR));
            }
        };
        worker.start();
    }

    public BIXMLRecord getRecordStructure(String parentId) {
        BIXMLRecord recordAux = dataController.getRecordStructure(frame.getType(), parentId);

        if (dataController.getStatus() == KpiDataController.KPI_ST_OK) {
            return recordAux;
        } else {
            dialogSystem.warningMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiDefaultFrameBehavior_00005")
                    + frame.getType() + ".");

            if (dataController.getStatus() == KpiDataController.KPI_ST_EXPIREDSESSION) {
                KpiStaticReferences.getKpiApplet().resetApplet();
            }

            return null;
        }
    }

    /**
     * Cria um formul�rio de inclus�o preenchido com dados do formul�rio
     * original.
     */
    public void duplicateRecord() {
        frame.setStatus(INSERTING);
        frame.setID("");
        frame.showDataButtons();
        frame.enableFields();
        frame.asJInternalFrame().repaint();
    }

    /**
     * Retorna o Item da combo box, passando o codigo do item. ("ID")
     *
     * @param vctItens Vector com os dados da combo.
     * @param keyCode Chave para procurar
     * @param firstItemValid Indica se o primeiro item da combo � branco.
     * @return int indicando a possi��o do item, -1 Indica que o item n�o foi
     * encontrado.
     */
    public int getComboItem(kpi.xml.BIXMLVector vctItens, String keyCode, boolean firstItemValid) {
        for (int item = 0; item < vctItens.size(); item++) {
            if (vctItens.get(item).contains("ID") && vctItens.get(item).getString("ID").equals(keyCode)) {
                if (firstItemValid) {
                    item++;
                    return item;
                } else {
                    return item;
                }
            }
        }
        return -1;
    }

    /*
     * Retorna o valor da comboBox, quando o 1 item for valido � necessario
     * informar isValidFirstValue = true
     *
     */
    public String getComboValue(java.util.Hashtable table, javax.swing.JComboBox combo, boolean isValidFirstValue) {
        int itemSelected = combo.getSelectedIndex();

        if (isValidFirstValue) {
            itemSelected++;
        }

        Object objSelected = (table.get((int) itemSelected));

        if (objSelected != null) {
            return (String) (objSelected);
        } else {
            return "0";
        }
    }

    public String getComboValue(java.util.Hashtable table, javax.swing.JComboBox combo) {
        return getComboValue(table, combo, false);
    }

    public void populateCombo(kpi.xml.BIXMLVector vector, String keyName, java.util.Hashtable table, javax.swing.JComboBox combo) {
        populateCombo(vector, keyName, table, combo, null, "", false);
    }

    public void populateCombo(kpi.xml.BIXMLVector vector, String keyName, java.util.Hashtable table, javax.swing.JComboBox combo, String fieldName) {
        populateCombo(vector, keyName, table, combo, null, fieldName, false);
    }

    public void populateCombo(kpi.xml.BIXMLVector vector, String keyName, java.util.Hashtable table, javax.swing.JComboBox combo, String fieldName, Boolean lHasType) {
        populateCombo(vector, keyName, table, combo, null, fieldName, lHasType);
    }

    public void populateCombo(kpi.xml.BIXMLVector vector, String keyName, java.util.Hashtable table, javax.swing.JComboBox combo, java.util.Hashtable ignoreIDList) {
        populateCombo(vector, keyName, table, combo, ignoreIDList, "", false);
    }

    /**
     * M�todo que popula o combo e seta o valor selecionado na �rvore (tree)
     *
     * @param vector = Vetor com as informa��es a serem adicionadas.
     * @param KeyName = Ir� receber uma String com o campo para ser o 'respons�vel'.
     * @param table = tabela com o itens.
     * @param combo = ComboBox a ser tratado.
     * @param ignoreIDList = Tabela com itens a serem ignorados.
     * @param fieldName = String com o valor do item do combo. ex: Combo cont�m (01 -> Helio Leal; 02 -> Valdiney)
     */
    public void populateCombo(kpi.xml.BIXMLVector vector, String keyName, java.util.Hashtable table, javax.swing.JComboBox combo, java.util.Hashtable ignoreIDList, String fieldName, Boolean lHasType) {
        ItemListener[] items = combo.getItemListeners();

        for (int i = 0; i < items.length; i++) {
            combo.removeItemListener(items[i]);
        }

        table.clear();
        combo.removeAllItems();
        combo.addItem(new KpiComboString(""));

        //se n�o especificar o campo a ser carregado no combo, ser� o campo NOME da tabela
        if (fieldName.equals("")) {
            fieldName = "NOME";
        }

        //Registro em branco
        table.put(0, "0");

        for (int i = 0; i < vector.size(); i++) {
            table.put(i + 1, vector.get(i).getString("ID"));

            if (ignoreIDList != null) {
                if (!ignoreIDList.containsKey(new Integer(vector.get(i).getInt(("ID"))))) {
                    combo.addItem(new kpi.util.KpiComboString(vector.get(i).getString(fieldName)));
                }
            } else {
                combo.addItem(new kpi.util.KpiComboString(vector.get(i).getString(fieldName)));
            }
        }

        if (frame.getRecord().contains(keyName)) {
            String idIndex = frame.getRecord().getString(keyName);

            for (java.util.Enumeration e = table.keys(); e.hasMoreElements();) {
                Integer key = (Integer) e.nextElement();
                String val;

                if (lHasType) {
                    val = ((String) table.get(key)).substring(1);
                } else {
                    val = ((String) table.get(key));
                }

                if (val.trim().equals(idIndex)) {
                    combo.setSelectedIndex(key);
                    break;
                }
            }
        }

        for (int i = 0; i < items.length; i++) {
            combo.addItemListener(items[i]);
        }
    }

    public void populateComboWithName(kpi.xml.BIXMLVector vector, String keyName, java.util.Hashtable table, javax.swing.JComboBox combo) {
        java.awt.event.ItemListener[] items = combo.getItemListeners();
        for (int i = 0; i < items.length; i++) {
            combo.removeItemListener(items[i]);
        }

        table.clear();
        combo.removeAllItems();
        combo.addItem("");

        for (int i = 0; i < vector.size(); i++) {
            table.put(vector.get(i).getString("NOME"),
                    vector.get(i).getString("ID"));
            combo.addItem(vector.get(i).getString("NOME"));
        }

        if (frame.getRecord().contains(keyName)) {
            for (int i = 0; i < vector.size(); i++) {
                if (vector.get(i).getString("NOME").equals(frame.getRecord().getString(keyName))) {
                    combo.setSelectedItem(vector.get(i).getString("NOME"));
                } else {
                    javax.swing.JTextField ed = (javax.swing.JTextField) ((javax.swing.plaf.basic.BasicComboBoxEditor) combo.getEditor()).getEditorComponent();
                    ed.setText(frame.getRecord().getString(keyName));
                }
            }
        }

        for (int i = 0; i < items.length; i++) {
            combo.addItemListener(items[i]);
        }
    }

    // Fernando - 4/12/03
    public void executeRecord(String execCMD) {
        final String id = frame.getID();
        final String type = frame.getType();

        // Verifica opera��es execcmd envolvidas
        final boolean saveBeforeExecute = execCMD.startsWith("SALVAR;");
        if (saveBeforeExecute) {
            execCMD = execCMD.substring(7);
        }
        final String finalExecCMD = execCMD;

        // Save
        StringBuffer errorMessage = new StringBuffer();
        if (frame.validateFields(errorMessage)) {

            BIXMLRecord record = frame.getFieldsContents();
            BIXMLRecord recordBeforeChanges = frame.getRecord();

            final BIXMLRecord recordFull = fixRecord(recordBeforeChanges, record);

            final kpi.core.SwingWorker worker = new kpi.core.SwingWorker() {

                @Override
                public Object construct() {
                    frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
                    if (saveBeforeExecute) {
                        dataController.updateRecord(type, recordFull);
                    }
                    dataController.executeRecord(id, type, finalExecCMD);
                    return id;
                }

                @Override
                public void finished() {
                    if (dataController.getStatus() == KpiDataController.KPI_ST_OK) {
                        if (dataController.getErrorMessage().trim().length() > 0) {
                            dialogSystem.informationMessage(dataController.getErrorMessage());
                        }
                        frame.asJInternalFrame().repaint();
                    } else {

                        if (dataController.getStatus() == KpiDataController.KPI_ST_GENERALERROR) {
                            dialogSystem.warningMessage(dataController.getErrorMessage());
                        } else if (dataController.getStatus() == KpiDataController.KPI_ST_VALIDATION) {
                            dialogSystem.warningMessage(dataController.getErrorMessage());
                        } else {
                            dialogSystem.errorMessage(dataController.getStatus());
                        }

                        if (dataController.getStatus() == KpiDataController.KPI_ST_EXPIREDSESSION) {
                            KpiStaticReferences.getKpiApplet().resetApplet();
                        }
                    }
                    frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
                    if (frame.asJInternalFrame() instanceof kpi.swing.KpiInternalFrame) {
                        ((kpi.swing.KpiInternalFrame) frame.asJInternalFrame()).afterExecute();
                    }
                    if (saveBeforeExecute) {
                        editRecord();
                    }
                }
            };
            worker.start();
        } else {
            // Erro na opera��o
            dialogSystem.warningMessage(errorMessage.toString());
        }
    }

    public void loadHelp(final String helpCMD) {
        final String id = frame.getID();
        final String type = frame.getType();
        final kpi.core.SwingWorker worker = new kpi.core.SwingWorker() {

            @Override
            public Object construct() {
                frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
                kpi.xml.BIXMLRecord recordAux = dataController.loadHelp(type, helpCMD);
                return recordAux;
            }

            @Override
            public void finished() {
                kpi.xml.BIXMLRecord record = (kpi.xml.BIXMLRecord) get();
                if (dataController.getStatus() == KpiDataController.KPI_ST_OK) {

                    try {

                        KpiToolKit oToolKit = new KpiToolKit();
                        URL url = new URL(record.getString("URL"));
                        oToolKit.browser(url);

                    } catch (Exception e) {
                        System.err.println(e);
                    }

                } else {
                    if (dataController.getStatus() == KpiDataController.KPI_ST_GENERALERROR) {
                        dialogSystem.warningMessage(dataController.getErrorMessage());
                    } else {
                        dialogSystem.errorMessage(dataController.getStatus());
                    }

                    if (dataController.getStatus() == KpiDataController.KPI_ST_EXPIREDSESSION) {
                        KpiStaticReferences.getKpiApplet().resetApplet();
                    }
                }
                frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(
                        java.awt.Cursor.DEFAULT_CURSOR));
            }
        };
        worker.start();
    }

    // Load record customizada para retornar o resultado imediatamente
    public kpi.xml.BIXMLRecord loadRecord(final String id, final String type) {
        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
        kpi.xml.BIXMLRecord recordAux = dataController.loadRecord(id, type);
        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
        if (dataController.getStatus() != KpiDataController.KPI_ST_OK) {
            if (dataController.getStatus() == KpiDataController.KPI_ST_GENERALERROR) {
                dialogSystem.warningMessage(dataController.getErrorMessage());
            } else {
                dialogSystem.errorMessage(dataController.getStatus());
            }
            if (dataController.getStatus() == KpiDataController.KPI_ST_EXPIREDSESSION) {
                KpiStaticReferences.getKpiApplet().resetApplet();
            }
        }
        return recordAux;
    }

    public void saveBase64(final String fileName, final String fileContent) {
        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
        dataController.saveBase64(fileName, fileContent);
        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
        if (dataController.getStatus() != KpiDataController.KPI_ST_OK) {
            if (dataController.getStatus() == KpiDataController.KPI_ST_GENERALERROR) {
                dialogSystem.warningMessage(dataController.getErrorMessage());
            } else {
                dialogSystem.errorMessage(dataController.getStatus());
            }
            if (dataController.getStatus() == KpiDataController.KPI_ST_EXPIREDSESSION) {
                KpiStaticReferences.getKpiApplet().resetApplet();
            }
        }
    }

    /**
     * Load record customizada para retornar o resultado imediatamente
     *
     * @param id	Uma
     * <code>String</code> que contendo o valor do registro que ser� carregado.
     * @param	type	Uma
     * <code>String</code> com o nome do tipo do registro que sera carregado.
     * @param cLoadCmd Uma
     * <code>String</code> com os valores dos parametros separados por ;
     */
    public kpi.xml.BIXMLRecord loadRecord(final String id, final String type, final String cLoadCmd) {

        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
        kpi.xml.BIXMLRecord recordAux = dataController.loadRecord(id, type, cLoadCmd);
        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
        if (dataController.getStatus() != KpiDataController.KPI_ST_OK) {
            if (dataController.getStatus() == KpiDataController.KPI_ST_GENERALERROR) {
                dialogSystem.warningMessage(dataController.getErrorMessage());
            } else {
                dialogSystem.errorMessage(dataController.getStatus());
            }
            if (dataController.getStatus() == KpiDataController.KPI_ST_EXPIREDSESSION) {
                KpiStaticReferences.getKpiApplet().resetApplet();
            }
        }
        return recordAux;
    }

    /**
     * Load record customizada para retornar o resultado imediatamente
     *
     * @param	type	Uma
     * <code>String</code> com o nome do tipo do registro que sera carregado.
     * @param cmdSQL	Uma
     * <code>String</code> com a sintaxe SQL para filtro na tabela;
     */
    public kpi.xml.BIXMLRecord listRecords(final String type, final String cmdSQL) {
        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
        kpi.xml.BIXMLRecord recordAux = dataController.listRecords(type, cmdSQL);

        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
        if (dataController.getStatus() != KpiDataController.KPI_ST_OK) {
            if (dataController.getStatus() == KpiDataController.KPI_ST_GENERALERROR) {
                dialogSystem.warningMessage(dataController.getErrorMessage());
            } else {
                dialogSystem.errorMessage(dataController.getStatus());
            }
            if (dataController.getStatus() == KpiDataController.KPI_ST_EXPIREDSESSION) {
                KpiStaticReferences.getKpiApplet().resetApplet();
            }
        }
        return recordAux;
    }

    /**
     *
     * @param originalRecord
     * @param newRecord
     * @return
     * @throws BIXMLException
     */
    private BIXMLRecord fixRecord(BIXMLRecord originalRecord, BIXMLRecord newRecord) throws BIXMLException {
        for (java.util.Iterator e = originalRecord.getKeyNames(); e.hasNext();) {

            String key = (String) e.next();
            BIXMLAttributes recAttrib = originalRecord.getBIXMLVector(key).getAttributes();

            try {
                if (recAttrib != null) {
                    if ((recAttrib.getBoolean("RETORNA")) && (!newRecord.contains(key))) {
                        newRecord.set(key, originalRecord.getString(key));
                    }
                } else {
                    if (!newRecord.contains(key)) {
                        newRecord.set(key, originalRecord.getString(key));
                    }
                }
            } catch (Exception ex) {
                if (!newRecord.contains(key)) {
                    newRecord.set(key, originalRecord.getString(key));
                }
            }
        }

        return newRecord;
    }

    /**
     *
     * @return
     */
    private kpi.swing.KpiTree frameGetThreadTree() {
        javax.swing.JTree threadTree = null;
        kpi.swing.KpiTree tmpKpiTree = kpi.core.KpiStaticReferences.getKpiMainPanel().getKpiTree();

        if (tmpKpiTree != null) {
            String type = tmpKpiTree.getKpiTreeType();

            if (type.equals(KpiMainPanel.ST_CONFIGURACAO)) {
                threadTree = kpi.core.KpiStaticReferences.getKpiMainPanel().kpiTreeConfiguracao;
            }

            return (kpi.swing.KpiTree) threadTree;

        } else {
            return null;
        }
    }

    /**
     * Habilitar/desabilitar os compontes filhos contidos em um container.
     *
     * @param compToEnable Container alterado.
     * @param enable Indica o estado dos componentes.
     */
    public void setEnableFields(javax.swing.JComponent compToEnable, boolean enable) {
        for (int i = 0; i < compToEnable.getComponentCount(); i++) {
            compToEnable.getComponent(i).setEnabled(enable);

            //Define o comportamento dos combos relacionados com JBITreeSelection. 
            if (compToEnable.getComponent(i) instanceof JBITreeSelection) {
                JComboBox combo = ((JBITreeSelection) compToEnable.getComponent(i)).getCombo();

                //Desabilita o JComboBox.     
                combo.setEnabled(false);

                //Remove a seta JComboBox. 
                combo.setUI(new BasicComboBoxUI() {

                    @Override
                    protected JButton createArrowButton() {
                        return new JButton() {

                            @Override
                            public int getWidth() {
                                return 0;
                            }
                        };
                    }
                });
            }

            //Habilita/Desabilita os componentes contidos em outros componentes. 
            if (compToEnable.getComponent(i) instanceof JComponent) {
                setEnableFields((JComponent) compToEnable.getComponent(i), enable);
            }
        }
    }

    /**
     * Seleciona um indice em um JComboBox.
     *
     * @param combo ComboBox que ter� item selecionado.
     * @param item indice a ser selecionado.
     */
    public void selectComboItem(JComboBox combo, int indice) {
        selectComboItem(combo, indice, true);
    }

    /**
     * Seleciona um indice em um JComboBox.
     *
     * @param combo ComboBox que ter� item selecionado.
     * @param item indice a ser selecionado.
     * @param ignoraListener Indica se o listener dever� ser ignorado durante o
     * posionamento.
     */
    public void selectComboItem(JComboBox combo, int indice, boolean ignoraListener) {
        ItemListener[] items = combo.getItemListeners();

        //Remove os item listeners.
        if (ignoraListener) {
            for (int i = 0; i < items.length; i++) {
                combo.removeItemListener(items[i]);
            }
        }

        //Posiciona no item desejado.
        if (combo.getItemCount() > 0) {
            if ((indice != -1) && (indice < combo.getItemCount())) {
                combo.setSelectedIndex(indice);
            } else {
                combo.setSelectedIndex(0);
            }
        }

        //Insere os itens listeners.
        if (ignoraListener) {
            for (int i = 0; i < items.length; i++) {
                combo.addItemListener(items[i]);
            }
        }
    }

    /**
     * Define se na exclus�o o formul�rio ativo ser� fechado.
     *
     * @param onCloseFormOp the onCloseFormOp to set
     */
    public void setCloseFormOnDelete(int onCloseFormOp) {
        this.onCloseFormOption = onCloseFormOp;
    }

    /**
     * Classe para chamar um m�todo remotamente.
     *
     *
     * @param aClass: Nome da Classe
     * @param aMethod: M�todo
     * @param params: Parametros
     * @param args: Argumentos
     * @see invoke("Class1", "say", new Class[] {String.class, String.class},
     * new Object[] {new String("Hello"), new String("World")});
     *
     */
    private void invoke(KpiEventTask task) {
        try {
            Class classOri = task.getObjOri().getClass();
            Method m = classOri.getDeclaredMethod(task.getaMethod(), task.getParams());
            m.invoke(task.getObjOri(), task.getArgs());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
