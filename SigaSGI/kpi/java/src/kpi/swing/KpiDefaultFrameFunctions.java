/*
 * KpiDefaultFrameFunctions.java
 *
 * Created on July 8, 2003, 2:53 PM
 */

package kpi.swing;

/**
 *
 * @author  siga1996
 */

import kpi.xml.BIXMLRecord;

public interface KpiDefaultFrameFunctions {
	public void enableFields();
	public void disableFields();
	public void refreshFields();
	public BIXMLRecord getFieldsContents();
	
	public void loadRecord();
	
	public void showOperationsButtons();
	public void showDataButtons();
	public void setStatus( int value );
	public int getStatus();
	public void setID( String value );	
	public String getID( );
	public String getParentID();
	public void setType( String value );	
	public String getType();
	public String getFormType();
	public String getParentType();
	public void setRecord(kpi.xml.BIXMLRecord recordAux );	
	public BIXMLRecord getRecord();
	
	public boolean validateFields(StringBuffer errorMessage);
	//public boolean hasCombos();
	public javax.swing.JInternalFrame asJInternalFrame();
	public javax.swing.JTabbedPane getTabbedPane();
}
