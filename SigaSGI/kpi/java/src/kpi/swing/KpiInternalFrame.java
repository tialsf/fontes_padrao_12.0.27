/*
 * KpiInternalFrame.java
 *
 * Created on 16 de Fevereiro de 2006, 09:05
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package kpi.swing;

/**
 *
 * @author eduardo.miyoshi
 */
public abstract class KpiInternalFrame extends javax.swing.JInternalFrame{
	private boolean isCloseEnabled = true;
	
	public abstract void afterExecute();
	
	public void setCloseEnabled(boolean enable){
		
		if(enable){
			setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		}else{
			setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);			
		}
		
		isCloseEnabled = enable;
	}
	
	public boolean isCloseEnabled(){
		return isCloseEnabled;
	}
}
