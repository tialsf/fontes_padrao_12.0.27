package kpi.swing;

import java.awt.event.MouseEvent;

/**
 *
 * @author gilmar.pereira
 */
public class KpiMouseEvent {
    private IKpiMouseListener source;
    private MouseEvent event;

    public static KpiMouseEvent createEvent(IKpiMouseListener source, MouseEvent event){
	return new KpiMouseEvent(source, event);
    }

    public KpiMouseEvent(IKpiMouseListener source, MouseEvent event){
	this.source = source;
	this.event = event;
    }

    /**
     * @return the listener
     */
    public IKpiMouseListener getSource() {
	return source;
    }

    /**
     * @return the event
     */
    public MouseEvent getEvent() {
	return event;
    }
}
