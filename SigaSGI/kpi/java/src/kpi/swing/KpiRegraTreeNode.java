/*
 * KpiRegraTreeNode.java
 *
 /*
 * KpiRegraTreeNode.java
 *
 * Created on 7 de Abril de 2004, 11:28
 */

package kpi.swing;

/**
 *
 * @author  siga1645
 */
public class KpiRegraTreeNode extends kpi.swing.KpiTreeNode {
	
	private kpi.xml.BIXMLRecord record;
	
	/**
	 * Creates a new instance of KpiRegraTreeNode
	 */
	public KpiRegraTreeNode( String typeAux, String idAux, String nameAux, kpi.xml.BIXMLRecord record) {
		super( typeAux, idAux, nameAux );
		
		// Aponta para o registro XML que originou esses chkboxes
		setRecord(record);
	}
	
	/** Getter for property record.
	 * @return Value of property record.
	 *
	 */
	public kpi.xml.BIXMLRecord getRecord() {
		return record;
	}
	
	/** Setter for property record.
	 * @param record New value of property record.
	 *
	 */
	public void setRecord(kpi.xml.BIXMLRecord record) {
		this.record = record;
	}
}