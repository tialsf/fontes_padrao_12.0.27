/*
 * KpiReuniaoTreeNode.java
 *
 * Created on 12 de Setembro de 2005, 15:25
 */

package kpi.swing;

/**
 * Criar o TreeNode de Scorecard Id, Nome, Responsavel, Descricao
 * herdado de KpiTreeNode
 * @author  siga0739 Aline
 */
public class KpiScorecardTreeNode extends KpiTreeNode {
    
    /** Creates a new instance of KpiScorecardTree */
    private String descricao = new String();
    private String responsavel = new String();
	private boolean visible = true;

    public KpiScorecardTreeNode(String typeAux, String idAux, String nameAux, String descricaoAux, String responsavelAux, boolean isVisible) {

	super( typeAux, idAux, nameAux);
	descricao = descricaoAux;
	responsavel = responsavelAux;
	visible = isVisible;
    }
    
    public String getDescricao(){
	return descricao;
    }

    public String getResponsavel(){
	return responsavel;
    }

	public boolean getVisible(){
	return visible;
    }
}
