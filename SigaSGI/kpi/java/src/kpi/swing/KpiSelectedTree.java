/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kpi.swing;

import java.util.Enumeration;
import java.util.Iterator;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import kpi.xml.BIXMLException;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

/**
 *
 * @author tiago.tudisco
 */
public class KpiSelectedTree {

    public KpiSelectedTree() {
    }

    static public DefaultMutableTreeNode createTree(BIXMLVector treeXML) {
        DefaultMutableTreeNode root = insertRecord(treeXML, null);
        return root;
    }

    static private DefaultMutableTreeNode insertRecord(BIXMLRecord record, DefaultMutableTreeNode tree) throws BIXMLException {
        KpiSelectedTreeNode oScoNode = new KpiSelectedTreeNode(
                record.getAttributes().getString("ID"),
                record.getAttributes().getString("NOME"),
                record.getAttributes().contains("ENABLE") ? record.getAttributes().getBoolean("ENABLE") : true,
                record.getAttributes().getBoolean("SELECTED"),
                record.getAttributes().getInt("IMAGE"));

        DefaultMutableTreeNode child = new DefaultMutableTreeNode(oScoNode);

        for (Iterator e = record.getKeyNames(); e.hasNext();) {
            String strChave = (String) e.next();

            child = insertRecord(record.getBIXMLVector(strChave), child);

        }

        if (tree != null) {
            tree.add(child);
            return tree;
        } else {
            return child;
        }
    }

    static private DefaultMutableTreeNode insertRecord(BIXMLVector vector, DefaultMutableTreeNode tree) throws BIXMLException {
        KpiSelectedTreeNode oScoNode = new KpiSelectedTreeNode(
                vector.getAttributes().getString("ID"),
                vector.getAttributes().getString("NOME"),
                vector.getAttributes().contains("ENABLE") ? vector.getAttributes().getBoolean("ENABLE") : true,
                vector.getAttributes().getBoolean("SELECTED"),
                vector.getAttributes().getInt("IMAGE"));

        DefaultMutableTreeNode child = new DefaultMutableTreeNode(oScoNode);

        for (int i = 0; i < vector.size(); i++) {
            child = insertRecord(vector.get(i), child);
        }

        if (tree != null) {
            tree.add(child);
            return tree;
        } else {
            return child;
        }
    }

   static public void expandAll(JTree tree, boolean expand) {
        TreeNode root = (TreeNode) tree.getModel().getRoot();

        expandAll(tree, new TreePath(root), expand);
    }

    static private void expandAll(JTree tree, TreePath parent, boolean expand) {
        // Traverse children
        TreeNode node = (TreeNode) parent.getLastPathComponent();
        if (node.getChildCount() >= 0) {
            for (Enumeration e = node.children(); e.hasMoreElements();) {
                TreeNode n = (TreeNode) e.nextElement();
                TreePath path = parent.pathByAddingChild(n);
                expandAll(tree, path, expand);
            }
        }

        if (expand) {
            tree.expandPath(parent);
        } else {
            tree.collapsePath(parent);
        }
    }
}