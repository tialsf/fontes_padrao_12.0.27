/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package kpi.swing;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author lucio.pelinson
 */
public class KpiSelectedTreeNode extends DefaultMutableTreeNode {
    public final static int SINGLE_SELECTION = 0;
    public final static int DIG_IN_SELECTION = 4;
    protected int selectionMode;
    private int iconId;
    protected boolean isSelected;
    protected boolean isEnable;
    protected String name = new String();
    protected String treeID = new String();

    public KpiSelectedTreeNode(String sId, String sName, boolean isEnable, boolean isSelected) {
        setID(sId);
        this.name = sName;
        this.isSelected = false;
        this.isEnable = isEnable;
        this.isSelected = isSelected;
    }

    public KpiSelectedTreeNode(String sId, String sName, boolean isEnable, boolean isSelected, int iconId) {
        setID(sId);
        this.name = sName;
        this.isSelected = false;
        this.isEnable = isEnable;
        this.isSelected = isSelected;
        this.iconId = iconId;
    }
    
    public void setSelectionMode(int mode) {
        selectionMode = mode;
    }

    public int getSelectionMode() {
        return selectionMode;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;

    }

    public boolean isSelected() {
        return isSelected;
    }

    public boolean isEnabled() {
        return isEnable;
    }

    public String getID() {
        return treeID;
    }

    public void setID(String id) {
        treeID = id;
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * @return the iconId
     */
    public int getIconId() {
        return iconId;
    }

    /**
     * @param iconId the iconId to set
     */
    public void setIconId(int iconId) {
        this.iconId = iconId;
    }
}



