/*
 * KpiTree.java
 *
 * Created on 3 de Novembro de 2004, 09:46
 */

package kpi.swing;

/**
 *
 * @author  alexandreas
 */
public class KpiTree extends javax.swing.JTree {
	
	private String treeType;
	
	public KpiTree() {
		super();
	}
	
	/**
	 * Creates a new instance of KpiTree
	 */
	public KpiTree(String treeType) {
		super();
		this.treeType = treeType;
	}
	
	public void setKpiTreeType(String treeType){
		this.treeType = treeType;
	}
	
	public String getKpiTreeType(){
		return this.treeType;
	}
}
