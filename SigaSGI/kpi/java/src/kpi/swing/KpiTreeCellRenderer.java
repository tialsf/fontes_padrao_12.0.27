/*
 * KpiTreeCellRenderer.java
 *
 * Created on September 11, 2005, 2:16 PM
 */
package kpi.swing;

/**
 *
 * @author Alexandre 1776
 */
public class KpiTreeCellRenderer extends javax.swing.tree.DefaultTreeCellRenderer {

    private kpi.core.KpiImageResources imageResources;
    private boolean useMyRenderer = false;

    public KpiTreeCellRenderer(kpi.core.KpiImageResources imageResources) {
	this.imageResources = imageResources;
    }

    @Override
    public java.awt.Color getBackground() {
	return null;
    }

    @Override
    public java.awt.Component getTreeCellRendererComponent(javax.swing.JTree tree, Object value,
	    boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
	super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

	javax.swing.tree.DefaultMutableTreeNode nodeAux = (javax.swing.tree.DefaultMutableTreeNode) value;

	if (isUseMyRenderer() == false) {
	    if (nodeAux.getUserObject().getClass().getName().equals("kpi.swing.KpiTreeNode")) {
		kpi.swing.KpiTreeNode node = (kpi.swing.KpiTreeNode) nodeAux.getUserObject();
		javax.swing.ImageIcon image = null;

		if (node.getType().equals("SCORECARDS")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_SCORECARD);
		} else if (node.getType().equals("INDICADORES")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_INDICADOR);
		} else if (node.getType().equals("PLANILHAS")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_PLANILHAS);//alterar
		} else if (node.getType().equals("GRUPO_ACAO")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_GRUPO_ACAO);
		} else if (node.getType().equals("PLANOSACAO")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_PLANOACAO);
		} else if (node.getType().equals("PROJETOS")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_PROJETOS);
		} else if (node.getType().equals("METAFORMULAS")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_METAFORMULA);
		} else if (node.getType().equals("SCORECARDING")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_SCORECARDING);//alterar
		} else if (node.getType().equals("GRUPO_INDS")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_GRUPOINDICADOR);
		} else if (node.getType().equals("RELATORIOS")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_RELATORIOS);//alterar
		} else if (node.getType().equals("EXPORTACAO")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_EXPORTACAO);//alterar
		} else if (node.getType().equals("USUARIO")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_USUARIO);
		} else if (node.getType().equals("PAINEIS")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_CARDPAINEL);
		} else if (node.getType().equals("PAINEISCOMP")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_CARDPAINEL_COMP);
		} else if (node.getType().equals("USU_CONFIGS")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_USUPERSO);
		} else if (node.getType().equals("MENSAGENS_ENVIADAS")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_MSG_ENVIADO);
		} else if (node.getType().equals("MENSAGENS_RECEBIDAS")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_MSG_RECEBIDO);
		} else if (node.getType().equals("MENSAGENS_EXCLUIDAS")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_MSG_EXCLUIDO);
		} else if (node.getType().equals("AGENDADOR")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_AGENDADOR);
		} else if (node.getType().equals("CALC_INDICADOR")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_CALC_INDICADOR);
		} else if (node.getType().equals("SCO_DUPLICADOR")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_SCO_DUPLICADOR);
		} else if (node.getType().equals("SMTPCONF")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_SMTPCONF);
		} else if (node.getType().equals("DIRUSUARIOS")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_DIRUSUARIO);
		} else if (node.getType().equals("GRUPO")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_GRUPO_USUARIO);
		} else if (node.getType().equals("PARAMETRO")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_PARAMETRO);
		} else if (node.getType().equals("GERAPLANILHA")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_DOWNLOAD);
		} else if (node.getType().equals("EXPORTPLAN")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_DOWNLOAD);
		} else if (node.getType().equals("UNIDADES")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_UNIDADE);
		} else if (node.getType().equals("LSTDATASRC")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_DTSRC);
		} else if (node.getType().equals("RELAPRS")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_RELATORIOS);
		} else if (node.getType().equals("CFGRESTACESSO")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_SEGURANCA);
		} else if (node.getType().equals("CFGPLANVLR")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_PAINEL);
		} else if (node.getType().equals("ESP_PEIXE")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_ESPPEIXE);
		} else if (node.getType().equals("ESTEXPORT")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_EXPORTAR);
		} else if (node.getType().equals("ESTIMPORT")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_IMPORTAR);
		} else if (node.getType().equals("DW_CONF")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_DWCONF);
		} else if (node.getType().equals("PACOTES")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_PACOTE);
		} else if (node.getType().equals("IND_FORMULA")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_INDICFORMULA);
		} else if (node.getType().equals("FORMMATRIX")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_BSCENTIDADES);
		} else if (node.getType().equals("ORGANIZACOES")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_ORGANIZACAO);
		} else if (node.getType().equals("PERSPECTIVAS")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_PERSPECTIVA);
		} else if (node.getType().equals("ESTRATEGIAS")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_ESTRATEGIA);
		} else if (node.getType().equals("OBJETIVOS")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_OBJETIVO);
		} else if (node.getType().equals("MAPAESTRATEGICO")) {
		    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_MAPA);
		} else if (node.getType().equals("TEMAESTRATEGICO")){
                    image = imageResources.getImage(kpi.core.KpiImageResources.KPI_IMG_TEMAEST);
                }

		if (image != null) {
		    setIcon(image);
		    setDisabledIcon(image);
		}

		setBackground(null);
		setBackgroundNonSelectionColor(null);
		setOpaque(false);
	    }
	} else {
	    myImageRenderer(value);
	}

	return this;
    }

    /*
     *Use este m�todo quando precisar fazer a renderiza��o em uma classe filha.
     *Ele deve ser usado setando o met�do useMyRenderer.
     */
    public void myImageRenderer(Object value) {
    }

    public boolean isUseMyRenderer() {
	return useMyRenderer;
    }

    public void setUseMyRenderer(boolean useMyRenderer) {
	this.useMyRenderer = useMyRenderer;
    }
}