/*
 * KpiTreeNode.java
 *
 * Created on June 6, 2003, 3:25 PM
 */
package kpi.swing;

import java.util.HashMap;

/**
 *
 * @author  siga1996
 */
public class KpiTreeNode extends java.lang.Object {

    private String type = new String();
    private String id = new String();
    private String name = new String();
    private String treeID = new String();
    private boolean enable = true; //No ADVPL usar o ATRIBUTO ENABLE.
    private int IDimage = 0;	//No ADVPL usar o ATRIBUTO IMAGE onde (IMAGE ID da Imagem no resource IMAGE).
    private kpi.xml.BIXMLRecord record;
    private HashMap hashNodeProperty = new HashMap();

    @Override
    public boolean equals(Object obj) {
	KpiTreeNode node = (KpiTreeNode) obj;

	if ((type.equals(node.getType()))
		&& (id.equals(node.getID()))) {
	    return true;
	} else {
	    return false;
	}
    }

    public String getTreeID() {
	return treeID;
    }

    public void setTreeID(String id) {
	treeID = id;
    }

    public void setID(String ID) {
	id = ID;
    }

    @Override
    public int hashCode() {
	int code = 0;

	String key = type + id;

	for (int i = 0; i < key.length(); i++) {
	    code = code + java.lang.Character.getNumericValue(key.charAt(i));
	}

	return code;
    }

    public KpiTreeNode(String typeAux, String idAux, String nameAux) {
	type = typeAux;
	id = idAux;
	name = nameAux;
    }

    public KpiTreeNode(String typeAux, String idAux, String nameAux, String idTree) {
	type = typeAux;
	id = idAux;
	name = nameAux;
	this.setTreeID(idTree);
    }

    public KpiTreeNode(String typeAux, String idAux, String nameAux, String idTree, String tipoAux) {
	type = typeAux;
	id = idAux;
	name = nameAux;
	this.setTreeID(idTree);
    }

    public KpiTreeNode(String typeAux, String idAux, String nameAux, String idTree, String tipoAux, kpi.xml.BIXMLRecord record) {
	type = typeAux;
	id = idAux;
	name = nameAux;
	this.setTreeID(idTree);
	setRecord(record);
    }

    public String getType() {
	return new String(type);
    }

    public String getID() {
	return new String(id);
    }

    public String getName() {
	return new String(name);
    }

    public void setName(String str) {
	name = str;
    }

    public String toString() {
	return name;
    }

    /** Getter for property record.
     * @return Value of property record.
     *
     */
    public kpi.xml.BIXMLRecord getRecord() {
	return record;
    }

    /** Setter for property record.
     * @param record New value of property record.
     *
     */
    public void setRecord(kpi.xml.BIXMLRecord record) {
	this.record = record;
    }

    public HashMap getHashNodeProperty() {
	return hashNodeProperty;
    }

    public void setHashNodeProperty(HashMap hashClientProperty) {
	this.hashNodeProperty = hashClientProperty;
    }

    public void putHashNodeProperty(Object key, Object value) {
	if (this.hashNodeProperty.containsKey(key)) {
	    this.hashNodeProperty.remove(key);
	}
	this.hashNodeProperty.put(key, value);
    }

    public Object getHashNodeProperty(Object key) {
	if (this.hashNodeProperty.containsKey(key)) {
	    return this.hashNodeProperty.get(key);
	} else {
	    return null;
	}
    }

    public boolean isEnable() {
	return enable;
    }

    public void setEnable(boolean enable) {
	this.enable = enable;
    }

    public int getIDimage() {
	return IDimage;
    }

    public void setIDimage(int IDimage) {
	this.IDimage = IDimage;
    }
}