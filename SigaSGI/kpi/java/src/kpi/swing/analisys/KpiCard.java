/*
 * KpiCardFrame.java
 *
 * Created on October 27, 2003, 11:03 AM
 */

package kpi.swing.analisys;

/**
 *
 * @author  siga1996
 */
public interface KpiCard {
	public int getWidth();
	public int getHeight();
	public void setLocation(int x, int y);	
	public java.awt.Point getLocation();
	public java.awt.Point getCardPosition();
	public void setOrder( int o );
	public int getOrder();
	public String getNome(); //TODO: apagar depois
	public String getId();
	public kpi.xml.BIXMLRecord getRecordData();
}
