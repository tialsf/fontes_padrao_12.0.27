/*
 * KpiIndicadorTable.java
 * Created on 09/28/2006
 * @author  siga2516-Lucio Pelinson
 */
package kpi.swing.analisys;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.JToolBar.Separator;
import javax.swing.SwingConstants;
import javax.swing.table.TableColumn;
import kpi.beans.JBIXMLTable;
import kpi.core.KpiStaticReferences;
import kpi.xml.BIXMLException;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;
import java.util.*;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import kpi.beans.JBICardDesktopPane;
import kpi.core.KpiFormController;
import kpi.swing.framework.KpiListaPlanos;
import pv.jfcx.JPVTable;

public class KpiIndicadorTable extends javax.swing.JInternalFrame implements KpiCard {

    kpi.xml.BIXMLRecord record = null;
    kpi.xml.BIXMLRecord recVariacao = null;
    TableColumn colAcumulado = null;
    KpiIndicadorTableRenderer cellRenderer = null;
    java.util.Vector listData = new java.util.Vector();
    String id = "";
    String nome = "";
    String entId = "";
    String entity = "";
    int order = -1;
    int ordemExib = 0;
    private KpiFormController formController = KpiStaticReferences.getKpiFormController();
    private String[] IDS;
    private boolean[] EXIBIR;

    public KpiIndicadorTable(kpi.xml.BIXMLRecord recordAux) {
        this.setRecord(recordAux);

        this.putClientProperty("JInternalFrame.isPalette", Boolean.FALSE);

        initComponents();

        initLayout();

        initData();
    }

    /**
     * Realiza as altera�oes for�adas na inteface.
     */
    private void initLayout() throws BIXMLException {

        //Remove a barra de t�tulo do formul�rio.
        BasicInternalFrameUI janelaUI = (BasicInternalFrameUI) this.getUI();
        janelaUI.setNorthPane(null);

        //Define a cor de fundo da tabela.
        indicadorTable.getViewport().setBackground(Color.white);

        //Habilita ou desabilita o acesso ao cadastro de indicadores.
        if (kpi.core.KpiStaticReferences.getKpiMainPanel().jMnuIndicador.isVisible()) {
            btnIndicator.setVisible(true);
        } else {
            btnIndicator.setVisible(false);
        }

        //Exibe o nome e orienta��o do indicador.
        StringBuilder orientacao = new StringBuilder();
        orientacao.append("   [");

        if (record.getBoolean("ASCEND")) {
            orientacao.append(ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00008"));
        } else {
            orientacao.append(ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00009"));
        }

        orientacao.append("]   ");

        lblDepto.setText(record.getString("NOME").trim().concat(orientacao.toString()));
    }

    /**
     * Popula os cambos e inicializa as vari�vies com base nos dados.
     * @param recordAux
     * @throws BIXMLException
     */
    private void initData() throws BIXMLException {
        setNome(record.getString("NOME").trim());

        id = String.valueOf(record.getString("ID"));
        entity = String.valueOf(record.getString("ENTITY"));
        entId = String.valueOf(record.getString("ENTID"));
        ordemExib = record.getInt("ORDEMEXIB");

        indicadorTable.setDataSource(record.getBIXMLVector("TABLE").clone2());

        IDS = new String[indicadorTable.getModel().getRowCount()];
        EXIBIR = new boolean[indicadorTable.getModel().getRowCount()];

        for (int i = 0; i < indicadorTable.getModel().getRowCount(); i++) {
            IDS[i] = indicadorTable.getXMLData().get(i).getString("IDLINHA");
            EXIBIR[i] = true;
        }

        setRenderTable();
    }

    /**
     * Define se deve ser exibida a linha de varia��o na tabela.
     * @param show
     */
    public void showVariacao(boolean show) {
        if (show) {
            if (recVariacao != null) {
                indicadorTable.setDataSource(record.getBIXMLVector("TABLE").clone2());
                setRenderTable();
            }
        } else {
            recVariacao = indicadorTable.getXMLData().get(2);
            BIXMLVector tabela = record.getBIXMLVector("TABLE").clone2();
            tabela.remove(5);
            tabela.remove(2);
            indicadorTable.setDataSource(tabela);
            setRenderTable();
        }
        this.pack();
        redimensionHeight();
    }

    /**
     * Define qual o estado de cada linha da tabela.
     * @param idlinha
     * @param show
     */
    public void showLinha(String idlinha, boolean show) {
        int i = 0;

        for (i = 0; i < IDS.length; i++) {
            if (idlinha.equals(IDS[i])) {
                EXIBIR[i] = show;
            }
        }
        BIXMLVector tabela = record.getBIXMLVector("TABLE").clone2();

        for (i = tabela.size() - 1; i >= 0; i--) {
            if (!EXIBIR[i]) {
                tabela.remove(i);
            }
        }
        indicadorTable.setDataSource(tabela);
        setRenderTable();
        indicadorTable.getTable().getColumnModel().getColumn(0).setMaxWidth(100);
        indicadorTable.getTable().getColumnModel().getColumn(0).setPreferredWidth(100);
    }

    /**
     * Define se a tabela pode ser removida do painel.
     * @param pode_fechar
     */
    void showFechar(boolean pode_fechar) {
        btnRemove.setVisible(pode_fechar);
        sptFechar.setVisible(pode_fechar);
    }

    /**
     * Redimensiona o formul�rio da tabela de acordo com o n�mero de componentes.
     */
    private void redimensionHeight() {
        int numero_de_linhas = indicadorTable.getModel().getRowCount() + 1;
        int altura_da_linha = 20;

        int altura_da_janela = (numero_de_linhas * altura_da_linha) + 52;

        this.setPreferredSize(new Dimension(this.getWidth(), altura_da_janela));

        this.pack();
    }

    /**
     * Define o renderizador para a tabela.
     */
    private void setRenderTable() {
        cellRenderer = new KpiIndicadorTableRenderer(indicadorTable, this.record);
        indicadorTable.getTable().setAutoResizeMode(JPVTable.AUTO_RESIZE_OFF);
        indicadorTable.setHeaderHeight(20);
        indicadorTable.setRowHeight(20);

        redimensionHeight();

        for (int col = 0; col < indicadorTable.getModel().getColumnCount(); col++) {

            indicadorTable.getColumn(col).setCellRenderer(cellRenderer);

            if (indicadorTable.getColumnTag(col).startsWith("TIPO")) {
                indicadorTable.getColumn(col).setPreferredWidth(62);
            } else {
                indicadorTable.getColumn(col).setPreferredWidth(72);
            }
        }
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        jPanel1 = new JPanel();
        lblDepto = new JLabel();
        jToolBar1 = new JToolBar();
        btnIndicator = new JButton();
        btnGraph = new JButton();
        btnAction = new JButton();
        jSeparator1 = new Separator();
        btnMoveParaCima = new JButton();
        btnMoveParaBaixo = new JButton();
        sptFechar = new Separator();
        btnRemove = new JButton();
        pnlTable = new JPanel();
        indicadorTable = new JBIXMLTable();

        setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        setClosable(true);
        setToolTipText("");
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_indicador.gif"))); // NOI18N
        setMaximumSize(null);
        setMinimumSize(null);
        setNormalBounds(new Rectangle(0, 0, 750, 270));
        setPreferredSize(new Dimension(750, 270));
        setRequestFocusEnabled(false);

        jPanel1.setBackground(new Color(255, 255, 255));
        jPanel1.setLayout(new BorderLayout());

        lblDepto.setBackground(new Color(255, 255, 255));
        lblDepto.setFont(new Font("Tahoma", 1, 11)); // NOI18N
        lblDepto.setHorizontalAlignment(SwingConstants.LEFT);
        lblDepto.setText("TESTE");
        lblDepto.setOpaque(true);
        lblDepto.setPreferredSize(new Dimension(550, 22));
        jPanel1.add(lblDepto, BorderLayout.LINE_START);

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);
        jToolBar1.setOpaque(false);

        btnIndicator.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_indicador.gif"))); // NOI18N
        ResourceBundle bundle = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnIndicator.setToolTipText(bundle.getString("KpiIndicadorTable_00001")); // NOI18N
        btnIndicator.setOpaque(false);
        btnIndicator.setPreferredSize(new Dimension(22, 22));
        btnIndicator.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnIndicatorActionPerformed(evt);
            }
        });
        jToolBar1.add(btnIndicator);

        btnGraph.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_grafico.gif"))); // NOI18N
        btnGraph.setToolTipText(bundle.getString("KpiIndicadorTable_00003")); // NOI18N
        btnGraph.setOpaque(false);
        btnGraph.setPreferredSize(new Dimension(22, 22));
        btnGraph.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnGraphActionPerformed(evt);
            }
        });
        jToolBar1.add(btnGraph);

        btnAction.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_planodeacao.gif"))); // NOI18N
        btnAction.setToolTipText(bundle.getString("KpiIndicadorTable_00002")); // NOI18N
        btnAction.setOpaque(false);
        btnAction.setPreferredSize(new Dimension(22, 22));
        btnAction.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnActionActionPerformed(evt);
            }
        });
        jToolBar1.add(btnAction);
        jToolBar1.add(jSeparator1);

        btnMoveParaCima.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_move_para_cima.gif"))); // NOI18N
        btnMoveParaCima.setToolTipText("Mover para cima");
        btnMoveParaCima.setFocusable(false);
        btnMoveParaCima.setHorizontalTextPosition(SwingConstants.CENTER);
        btnMoveParaCima.setMinimumSize(new Dimension(21, 21));
        btnMoveParaCima.setOpaque(false);
        btnMoveParaCima.setPreferredSize(new Dimension(21, 21));
        btnMoveParaCima.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnMoveParaCima.addMouseListener(new MouseAdapter() {
            public void mouseReleased(MouseEvent evt) {
                btnMoveParaCimaMouseReleased(evt);
            }
        });
        btnMoveParaCima.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnMoveParaCimaActionPerformed(evt);
            }
        });
        jToolBar1.add(btnMoveParaCima);

        btnMoveParaBaixo.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_move_para_baixo.gif"))); // NOI18N
        btnMoveParaBaixo.setToolTipText("Mover para baixo");
        btnMoveParaBaixo.setFocusable(false);
        btnMoveParaBaixo.setHorizontalTextPosition(SwingConstants.CENTER);
        btnMoveParaBaixo.setMinimumSize(new Dimension(21, 21));
        btnMoveParaBaixo.setOpaque(false);
        btnMoveParaBaixo.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnMoveParaBaixo.addMouseListener(new MouseAdapter() {
            public void mouseReleased(MouseEvent evt) {
                btnMoveParaBaixoMouseReleased(evt);
            }
        });
        btnMoveParaBaixo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnMoveParaBaixoActionPerformed(evt);
            }
        });
        jToolBar1.add(btnMoveParaBaixo);
        jToolBar1.add(sptFechar);

        btnRemove.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnRemove.setToolTipText(bundle.getString("JBISelectionPanel_00001")); // NOI18N
        btnRemove.setFocusable(false);
        btnRemove.setHorizontalTextPosition(SwingConstants.CENTER);
        btnRemove.setOpaque(false);
        btnRemove.setPreferredSize(new Dimension(22, 22));
        btnRemove.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnRemove.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });
        jToolBar1.add(btnRemove);

        jPanel1.add(jToolBar1, BorderLayout.LINE_END);

        getContentPane().add(jPanel1, BorderLayout.PAGE_START);

        pnlTable.setAutoscrolls(true);
        pnlTable.setPreferredSize(new Dimension(200, 192));
        pnlTable.setLayout(new BorderLayout());
        pnlTable.add(indicadorTable, BorderLayout.CENTER);

        getContentPane().add(pnlTable, BorderLayout.CENTER);

        pack();
    }//GEN-END:initComponents

    private void btnIndicatorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIndicatorActionPerformed
        kpi.xml.BIXMLRecord recLinha = getRecordData();
        formController.getForm("INDICADOR",
                recLinha.getString("ENTID"),
                java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00002")).asJInternalFrame();
    }//GEN-LAST:event_btnIndicatorActionPerformed

	private void btnGraphActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGraphActionPerformed
            kpi.xml.BIXMLRecord recLinha = getRecordData();
            kpi.swing.graph.KpiGraphIndicador frame = (kpi.swing.graph.KpiGraphIndicador) formController.getForm(
                    "GRAPH_IND", recLinha.getString("ENTID"), java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00002")).asJInternalFrame();

            //Carregando os dados do gr�ficos.
            kpi.util.KpiDateUtil date_Util = new kpi.util.KpiDateUtil();
            kpi.core.KpiDateBase dateBase = kpi.core.KpiStaticReferences.getKpiDateBase();
            frame.loadGraphRecord(recLinha.getString("ENTID"),
                    date_Util.getCalendarString(dateBase.getDataAcumuladoDe()),
                    date_Util.getCalendarString(dateBase.getDataAcumuladoAte()));

	}//GEN-LAST:event_btnGraphActionPerformed

	private void btnActionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActionActionPerformed
            try {
                String sText = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00013");
                KpiListaPlanos oFormListaPlanos = (KpiListaPlanos) KpiStaticReferences.getKpiFormController().getForm("LSTPLANOACAO", "0", sText);
                oFormListaPlanos.buildFilter(KpiListaPlanos.FILTER_SCORECARD, record.getString("IDSCOREC"));
            } catch (kpi.core.KpiFormControllerException e) {
                kpi.core.KpiDebug.println(e.getMessage());
            }
	}//GEN-LAST:event_btnActionActionPerformed

        private void btnRemoveActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
            this.dispose();
        }//GEN-LAST:event_btnRemoveActionPerformed

        private void btnMoveParaBaixoActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnMoveParaBaixoActionPerformed
            this.setLocation(this.getX(), this.getY() + this.getHeight());
        }//GEN-LAST:event_btnMoveParaBaixoActionPerformed

        private void btnMoveParaBaixoMouseReleased(MouseEvent evt) {//GEN-FIRST:event_btnMoveParaBaixoMouseReleased
            JBICardDesktopPane parent = (JBICardDesktopPane) this.getParent();
            parent.moveFrame(this);
        }//GEN-LAST:event_btnMoveParaBaixoMouseReleased

        private void btnMoveParaCimaActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnMoveParaCimaActionPerformed
            this.setLocation(this.getX(), this.getY() - (this.getHeight() + 20));
        }//GEN-LAST:event_btnMoveParaCimaActionPerformed

        private void btnMoveParaCimaMouseReleased(MouseEvent evt) {//GEN-FIRST:event_btnMoveParaCimaMouseReleased
            JBICardDesktopPane parent = (JBICardDesktopPane) this.getParent();
            parent.moveFrame(this);
        }//GEN-LAST:event_btnMoveParaCimaMouseReleased
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnAction;
    private JButton btnGraph;
    public JButton btnIndicator;
    private JButton btnMoveParaBaixo;
    private JButton btnMoveParaCima;
    private JButton btnRemove;
    private JBIXMLTable indicadorTable;
    private JPanel jPanel1;
    private Separator jSeparator1;
    private JToolBar jToolBar1;
    private JLabel lblDepto;
    private JPanel pnlTable;
    private Separator sptFechar;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setOrder(int o) {
        order = o;
    }

    @Override
    public int getOrder() {
        return order;
    }

    @Override
    public String getNome() {
        return nome;
    }

    public void setNome(String cNome) {
        nome = cNome;
    }

    @Override
    public String getId() {
        return id;
    }


    @Override
    public java.awt.Point getCardPosition() {
        java.awt.Point p = new java.awt.Point(record.getInt("CARDX"), record.getInt("CARDY"));
        return p;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecordData() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord("CARD");
        recordAux.set("ID", id);
        recordAux.set("ENTITY", entity);
        recordAux.set("ENTID", entId);
        recordAux.set("ORDEMEXIB", ordemExib);
        return recordAux;
    }

    public BIXMLRecord getRecord() {
        return record;
    }

    public void setRecord(BIXMLRecord record) {
        this.record = record;
    }
}

class KpiIndicadorTableRenderer extends javax.swing.table.DefaultTableCellRenderer {

    private kpi.beans.JBIXMLTable biTable = null;
    private kpi.xml.BIXMLRecord record = null;
    private java.awt.Font fonteType = new java.awt.Font("Tahoma", 0, 11);
    private java.awt.Color corBranco = new java.awt.Color(255, 255, 255);
    private java.awt.Color statusVermelho = new java.awt.Color(234, 140, 136);
    private java.awt.Color statusAmarelo = new java.awt.Color(255, 235, 155);
    private java.awt.Color statusVerde = new java.awt.Color(139, 191, 150);
    private java.awt.Color statusCinza = new java.awt.Color(228, 228, 228);
    private BIXMLRecord linStatus;
    private java.util.Locale locale = kpi.core.KpiStaticReferences.getKpiDefaultLocale();
    private pv.jfcx.JPVEdit oRender = new pv.jfcx.JPVEdit();
    private java.text.NumberFormat oValor = null;

    public KpiIndicadorTableRenderer() {
    }

    public KpiIndicadorTableRenderer(kpi.beans.JBIXMLTable table, kpi.xml.BIXMLRecord rec) {
        biTable = table;
        record = rec;
    }

    @Override
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        BIXMLRecord recLinha = null;
        int linha = biTable.getOriginalRow(row);

        table.setBackground(corBranco);
        oRender.setBackground(corBranco);
        oRender.setForeground(java.awt.Color.BLACK);
        oRender.setAlignment(0);
        oRender.setFontStyle(pv.jfcx.JPVEdit.NORMAL);
        oRender.setLocale(locale);
        oRender.setLAF(false);
        oRender.setBorderStyle(0);
        oRender.setFont(fonteType);
        oRender.setText("");
        oRender.setToolTipText("");

        if (biTable != null) {
            column = biTable.getColumn(column).getModelIndex();
            recLinha = biTable.getXMLData().get(linha);
            String tipoColuna = biTable.getColumnType(column);
            int colTipo = Integer.parseInt(tipoColuna);

            if (colTipo == JBIXMLTable.KPI_FLOAT) {
                renderNumber(oRender, Double.parseDouble(value.toString()), column, row, recLinha);
            } else if (colTipo == JBIXMLTable.KPI_STRING) {
                renderString(oRender, value.toString(), column, row, recLinha);
            }
        }

        return oRender;
    }

    private pv.jfcx.JPVEdit renderNumber(pv.jfcx.JPVEdit oRender, Double dbValor, int column, int row, BIXMLRecord recLinha) {
        int iDecimais = 2;
        int iDecimaisVar = 2;
        oValor = java.text.NumberFormat.getInstance();

        if (record.contains("DECIMAIS")) {
            iDecimais = record.getInt("DECIMAIS");
        }

        if (recLinha.getString("IDLINHA").equals("REALACUM") || recLinha.getString("IDLINHA").equals("REAL")) {
            if (recLinha.getString("IDLINHA").equals("REALACUM")) {
                linStatus = record.getBIXMLVector("STATUS_AC").get(0);
            } else {
                linStatus = record.getBIXMLVector("STATUS").get(0);
            }

            switch (linStatus.getInt("COL" + column)) {
                case KpiScoreCarding.STATUS_RED:
                    oRender.setBackground(statusVermelho);
                    break;
                case KpiScoreCarding.STATUS_YELLOW:
                    oRender.setBackground(statusAmarelo);
                    break;
                case KpiScoreCarding.STATUS_GREEN:
                    oRender.setBackground(statusVerde);
                    break;
                case KpiScoreCarding.ESTAVEL_GRAY:
                    oRender.setBackground(statusCinza);
                    break;
            }
            oValor.setMinimumFractionDigits(iDecimais);
            oValor.setMaximumFractionDigits(iDecimais);
            oRender.setAlignment(2);
            oRender.setValue(oValor.format(dbValor));

        } else if (recLinha.getString("IDLINHA").equals("VARIACAO") || recLinha.getString("IDLINHA").equals("VARIACUM")) {
            //Captura quantidade de casas decimais
            boolean isVariacaoPercent = false;
            if (record.contains("PERCENTDEC")) {
                iDecimaisVar = record.getInt("PERCENTDEC");
            }
            if (record.contains("VARPERCENT")) {
                isVariacaoPercent = record.getBoolean("VARPERCENT");
            }

            if (isVariacaoPercent) {
                oValor.setMinimumFractionDigits(iDecimaisVar);
                oValor.setMaximumFractionDigits(iDecimaisVar);
                oRender.setAlignment(2);
                oRender.setValue(oValor.format(dbValor).toString().concat(" %"));
            } else {
                oValor.setMinimumFractionDigits(iDecimais);
                oValor.setMaximumFractionDigits(iDecimais);
                oRender.setAlignment(2);
                oRender.setValue(oValor.format(dbValor));
            }

        } else {
            oValor.setMinimumFractionDigits(iDecimais);
            oValor.setMaximumFractionDigits(iDecimais);
            oRender.setAlignment(2);
            oRender.setValue(oValor.format(dbValor));
        }

        return oRender;
    }

    private pv.jfcx.JPVEdit renderString(pv.jfcx.JPVEdit oRender, String valor, int column, int row, BIXMLRecord recLinha) {
        oRender.setValue(valor);
        return oRender;
    }
}
