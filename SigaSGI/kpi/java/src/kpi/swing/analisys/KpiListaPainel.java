package kpi.swing.analisys;

public class KpiListaPainel extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = new String("PAINEL");
    private String formType = new String("LSTPAINEL");
    private String parentType = recordType;

    public KpiListaPainel(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
    }

    @Override
    public void disableFields() {
    }

    @Override
    public void refreshFields() {
        kpi.xml.BIXMLRecord recGrupos = event.listRecords("PAINEL", "");
        jBIListPaineis.setDataSource(recGrupos.getBIXMLVector("PAINEIS"),
                "0", "0", this.event);
        jBIListPaineis.xmlTable.setSortColumn(0);
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        tapCadastro = new javax.swing.JTabbedPane();
        pnlTopForm = new javax.swing.JPanel();
        jBIListPaineis = new kpi.beans.JBISeekListPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international"); // NOI18N
        setTitle(bundle.getString("KpiMainPanel_00024")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_painel.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(600, 358));

        tapCadastro.setEnabled(false);
        tapCadastro.setFocusable(false);
        tapCadastro.setPreferredSize(new java.awt.Dimension(0, 0));
        getContentPane().add(tapCadastro, java.awt.BorderLayout.WEST);

        pnlTopForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);
        getContentPane().add(jBIListPaineis, java.awt.BorderLayout.CENTER);

        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        getAccessibleContext().setAccessibleName(bundle1.getString("KpiProjeto_00005")); // NOI18N

        pack();
    }//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private kpi.beans.JBISeekListPanel jBIListPaineis;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JTabbedPane tapCadastro;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public void loadRecord() {
        this.setID("0");
        this.refreshFields();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }
}
