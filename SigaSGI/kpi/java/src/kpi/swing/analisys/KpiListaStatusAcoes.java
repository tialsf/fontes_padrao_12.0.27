package kpi.swing.analisys;

import kpi.core.KpiStaticReferences;
import kpi.swing.framework.KpiListaPlanos;
import kpi.xml.BIXMLRecord;
import pv.jfcx.JPVEdit;

public class KpiListaStatusAcoes extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    public final static int COL_NOME = 0;
    public final static int COL_VENCIDO = 1;
    public final static int COL_VENCER = 2;
    public final static int VENCIDOS = 1;
    public final static int AVENCER_7DIAS = 2;
    //
    private KpibiListAcoesTableCellRenderer cellRenderer;
    private String recordType = "PLANOACAO";
    private String formType = "LISTA_STATUS_ACAO";
    private String parentType = recordType;

    public KpiListaStatusAcoes(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
    }

    @Override
    public void disableFields() {
    }

    @Override
    public void refreshFields() {
        BIXMLRecord lista = event.listRecords("SCORECARD", "LISTA_STATUS_ACAO");
        tbStatus.setDataSource(lista.getBIXMLVector("SCORECARDS"));

        formataTabela();
    }

    /**
     * Define as configura��es de apar�ncia da tabela.
     */
    private void formataTabela() {
        tbStatus.setHeaderHeight(20);
        tbStatus.getColumn(COL_NOME).setPreferredWidth(650);

        cellRenderer = new KpibiListAcoesTableCellRenderer(tbStatus);

        for (int col = 0; col < 3; col++) {
            tbStatus.getColumn(col).setCellRenderer(cellRenderer);
        }
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        return new kpi.xml.BIXMLRecord(recordType);
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    /**
     * Trata a a��o de clique de mouse na tabela.
     * @param evt
     */
    private void biListAcoesMouseClicked(java.awt.event.MouseEvent evt) {
        int linha = tbStatus.getSelectedRow();

        if (evt.getClickCount() == 2) {
            BIXMLRecord tabLinha = tbStatus.getXMLData().get(linha);

            String sText = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00004");
            KpiListaPlanos oForm = (KpiListaPlanos) KpiStaticReferences.getKpiFormController().getForm("LSTPLANOACAO", "0", sText);
            oForm.buildFilter(KpiListaPlanos.FILTER_SCORECARD, tabLinha.getString("ID"));
        }
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        tbStatus = new kpi.beans.JBIXMLTable();
        pnlTopRecord = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnReload = new javax.swing.JButton();
        btnAjuda = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiListaStatusAcoes_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_planodeacao.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(600, 358));

        pnlFields.setLayout(new java.awt.BorderLayout());
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);

        tbStatus.getTable().addMouseListener(new java.awt.event.MouseAdapter() {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                biListAcoesMouseClicked(evt);
            }
        });

        scpRecord.setViewportView(tbStatus);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlTopRecord.setLayout(new java.awt.BorderLayout());
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlFields, java.awt.BorderLayout.CENTER);

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnAjuda.setText(bundle1.getString("KpiMainPanel_00008")); // NOI18N
        btnAjuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda);

        getContentPane().add(tbInsertion, java.awt.BorderLayout.NORTH);

        pack();
    }//GEN-END:initComponents

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
            event.loadHelp("LST_PLANOACAO");
	}//GEN-LAST:event_btnAjudaActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            refreshFields();
	}//GEN-LAST:event_btnReloadActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAjuda;
    private javax.swing.JButton btnReload;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JToolBar tbInsertion;
    private kpi.beans.JBIXMLTable tbStatus;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        this.setID("0");
        this.refreshFields();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return new javax.swing.JTabbedPane();
    }

    @Override
    public String getParentType() {
        return parentType;
    }
}

/**
 *
 */
class KpibiListAcoesTableCellRenderer extends javax.swing.table.DefaultTableCellRenderer {

    private kpi.beans.JBIXMLTable biTable = null;
    private static java.awt.Color colorGray = new java.awt.Color(228, 228, 228);
    private static java.awt.Color colorRed = new java.awt.Color(234, 106, 106);
    private static java.awt.Color colorYellow = new java.awt.Color(255, 235, 155);

    public KpibiListAcoesTableCellRenderer() {
    }

    public KpibiListAcoesTableCellRenderer(kpi.beans.JBIXMLTable table) {
        biTable = table;
    }

    @Override
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JPVEdit celula = new JPVEdit();
        BIXMLRecord xmlLinha = null;
        int linha = biTable.getOriginalRow(row);

        celula.setLocale(kpi.core.KpiStaticReferences.getKpiDefaultLocale());
        celula.setLAF(false);
        celula.setBorderStyle(0);
        celula.setFont(new java.awt.Font("Tahoma", 0, 11));

        if (biTable != null) {
            column = biTable.getColumn(column).getModelIndex();
            xmlLinha = biTable.getXMLData().get(linha);
            renderString(celula, value.toString(), column, row, xmlLinha);
        }

        if (hasFocus) {
            celula.setBackground(new java.awt.Color(212, 208, 200));
        } else if (table.getSelectedRow() == row) {
            celula.setBackground(table.getSelectionBackground());
            celula.setForeground(table.getSelectionForeground());
        }

        return celula;
    }

    private pv.jfcx.JPVEdit renderString(pv.jfcx.JPVEdit oRender, String valor, int column, int row, kpi.xml.BIXMLRecord recLinha) {

        oRender.setValue(valor);

        if (column == KpiListaStatusAcoes.COL_VENCER) {

            int status = recLinha.getInt("VENCIDO_7DIA");

            if (status < 0) {
                oRender.setBackground(colorGray);
            }

            if (status == 0) {
                oRender.setBackground(java.awt.Color.white);
            }

            if (status > 0) {
                oRender.setBackground(colorYellow);
            }
        }

        if (column == KpiListaStatusAcoes.COL_VENCIDO) {

            int status = recLinha.getInt("VENCIDOS");

            if (status < 0) {
                oRender.setBackground(colorGray);
            }

            if (status == 0) {
                oRender.setBackground(java.awt.Color.white);
            }

            if (status > 0) {
                oRender.setBackground(colorRed);
            }
        }

        oRender.setAutoScroll(true);
        oRender.setLAF(false);

        return oRender;
    }
}
