package kpi.swing.analisys;

import com.l2fprod.common.swing.JTaskPane;
import com.l2fprod.common.swing.JTaskPaneGroup;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.print.Printable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.JToolBar.Separator;
import javax.swing.SwingConstants;
import kpi.beans.JBICardDesktopPane;
import kpi.beans.JBIDatePane;
import static kpi.beans.JBILayeredPane.disableDoubleBuffering;
import static kpi.beans.JBILayeredPane.enableDoubleBuffering;
import kpi.beans.JBITextArea;
import kpi.beans.JBITreeSelection;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiFileChooser;
import kpi.swing.analisys.widget.KpiWidget;
import kpi.swing.analisys.widget.KpiWidgetFactory;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;
import pv.jfcx.JPVEdit;

public class KpiPainel extends kpi.swing.KpiInternalFrame implements kpi.swing.KpiDefaultFrameFunctions, Printable {

    private String recordType = "PAINEL";
    private String parentType = "LSTPAINEL";
    private String formType = recordType;
    BIXMLVector storeCards = null;
    private Hashtable htbScorecard = new Hashtable();
    private Hashtable htbIndicador = new Hashtable();
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiPainel(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
        event.setEnableFields(pnlDados, true);
        dskPaineis.setEnabled(true);

        int intElementos = dskPaineis.getComponentCount();
        for (int i = 0; i < intElementos; i++) {
            ((KpiWidget) dskPaineis.getComponent(i)).setClosable(true);
            ((KpiWidget) dskPaineis.getComponent(i)).setMnuSairVisible(true);
        }
    }

    @Override
    public void disableFields() {
        event.setEnableFields(pnlDados, false);
        dskPaineis.setEnabled(false);

        int intElementos = dskPaineis.getComponentCount();
        for (int i = 0; i < intElementos; i++) {
            ((KpiWidget) dskPaineis.getComponent(i)).setClosable(false);
        }
    }

    @Override
    public void refreshFields() {
        //Recupera os dados para os combo boxes.
        BIXMLVector vctScorecard = record.getBIXMLVector("SCORECARDS");

        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        chkPublico.setSelected(record.getBoolean("PUBLICO"));
        event.setEnableFields(pnlManutencao, record.getBoolean("EDITA"));

        //Popupa os combo boxes.
        event.populateCombo(vctScorecard, "ID", htbScorecard, cbScorecard);
        cbScorecard.setSelectedIndex(0);

        //Informa ao seletor vetor relacionado com o combo scorecard.
        tsArvore.setVetor(vctScorecard);

        //Selecionar apenas Objetivo.
        if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
	    tsArvore.setEntitySelection(KpiStaticReferences.CAD_OBJETIVO);
	}
        
        dskPaineis.setAutoOrganize(true);
        cbIndicador.removeAllItems();
        htbIndicador.clear();

        if (!(status == KpiDefaultFrameBehavior.INSERTING)) {
            StringBuilder parametros = new StringBuilder();
            parametros.append("ALLCARD|");
            parametros.append(id);
            parametros.append("|");
            parametros.append(datePane.getDataAlvo());
            parametros.append("|");
            parametros.append(datePane.getDataDe());
            parametros.append("|");
            parametros.append(datePane.getDataAte());

            BIXMLRecord recCard = event.loadRecord("-1", "PAINELXIND", parametros.toString());
            if (recCard.contains("CARDS")) {
                kpi.xml.BIXMLVector cards = recCard.getBIXMLVector("CARDS");
                dskPaineis.removeAll();
                KpiWidget widget = null;
                for (int i = 0; i < cards.size(); i++) {
                    widget = KpiWidgetFactory.createWidget(cards.get(i));
                    widget.setToolBarVisible(true);
                    widget.setVisible(true);
                    widget.setClosable(false);
                    widget.setMnuSairVisible(false);
                    widget.setOrder(i);
                    dskPaineis.add(widget);
                }
            }

            tskDados.setExpanded(false);
            dskPaineis.autoOrganizeFrames();
            pnlPainel.setPreferredSize(new Dimension(0, getContentPane().getHeight() - 130));
        } else {
            tskDados.setExpanded(true);
            pnlPainel.setPreferredSize(new Dimension(50, this.getHeight() + 70));
        }
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);
        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("PUBLICO", chkPublico.isSelected());

        StringBuilder sCardsId = new StringBuilder();
        Vector internalFrames = dskPaineis.getVctFrames();

        for (int i = 0; i < internalFrames.size(); i++) {
            KpiWidget frame = (KpiWidget) internalFrames.get(i);
            sCardsId.append(frame.getId());
            sCardsId.append("|");
        }

        recordAux.set("CARDSID", sCardsId.toString());

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        if (fldNome.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPainel_00014"));
            retorno = false;
        }
        return retorno;
    }

    private void addCard() {
        JInternalFrame[] internalFrames = dskPaineis.getAllFrames();
        KpiWidget widget = null;
        boolean frameAlreadyExists = false;
        String indID = event.getComboValue(htbIndicador, cbIndicador);

        if (!indID.trim().equals("0")) {
            for (int j = 0; j < internalFrames.length; j++) {
                widget = (KpiWidget) internalFrames[j];
                if (widget.getId().equals(indID)) {
                    frameAlreadyExists = true;
                }
            }

            if (!frameAlreadyExists) {
                StringBuilder parametros = new StringBuilder();
                parametros.append("CARD|");
                parametros.append(indID);
                parametros.append("|");
                parametros.append(datePane.getDataAlvo());
                parametros.append("|");
                parametros.append(datePane.getDataDe());
                parametros.append("|");
                parametros.append(datePane.getDataAte());
                BIXMLRecord card = event.loadRecord("-1", "PAINELXIND", parametros.toString());

                widget = KpiWidgetFactory.createWidget(card);
                widget.setToolBarVisible(true);
                widget.setVisible(true);
                widget.setClosable(true);
                widget.setOrder(dskPaineis.getAllFrames().length);
                dskPaineis.add(widget);
                widget.show();
            }

            dskPaineis.autoOrganizeFrames();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlRecord = new JPanel();
        pnlTools = new JPanel();
        pnlConfirmation = new JPanel();
        tbConfirmation = new JToolBar();
        btnSave = new JButton();
        btnCancel = new JButton();
        pnlOperation = new JPanel();
        tbInsertion = new JToolBar();
        datePane = new JBIDatePane(this);
        spt3 = new Separator();
        btnReload = new JButton();
        spt2 = new Separator();
        pnlManutencao = new JPanel();
        tblManutencao = new JToolBar();
        btnEdit = new JButton();
        btnDelete = new JButton();
        spt1 = new Separator();
        btnAjuda = new JButton();
        pnlFields = new JPanel();
        tskPaineis = new JTaskPane();
        tskDados = new JTaskPaneGroup();
        pnlDados = new JPanel();
        lblNome = new JLabel();
        lblIndicador = new JLabel();
        cbIndicador = new JComboBox();
        fldNome = new JPVEdit();
        lblDescricao = new JLabel();
        scrDescricao = new JScrollPane();
        txtDescricao = new JBITextArea();
        lblScorecard = new JLabel();
        cbScorecard = new JComboBox();
        tlbAdicionar = new JToolBar();
        btnAddIndicador = new JButton();
        tsArvore = new JBITreeSelection();
        chkPublico = new JCheckBox();
        tskPainel = new JTaskPaneGroup();
        pnlPainel = new JPanel();
        pnlPainelToolBar = new JPanel();
        jToolBar1 = new JToolBar();
        btnPrint = new JButton();
        btnExportacao = new JButton();
        btnAbrir = new JButton();
        pnlPainelContent = new JPanel();
        scrlPainel = new JScrollPane();
        dskPaineis = new JBICardDesktopPane();
        pnlRightRecord = new JPanel();
        pnlLeftForm = new JPanel();
        pnlBottomForm = new JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiPainel_00001")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_painel.gif"))); // NOI18N
        setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(710, 440));
        addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent evt) {
                formComponentResized(evt);
            }
        });

        pnlRecord.setLayout(new BorderLayout());

        pnlTools.setMinimumSize(new Dimension(480, 30));
        pnlTools.setPreferredSize(new Dimension(480, 25));
        pnlTools.setLayout(new BorderLayout());

        pnlConfirmation.setMaximumSize(new Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new Dimension(170, 27));
        pnlConfirmation.setLayout(new BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new Dimension(100, 32));

        btnSave.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, BorderLayout.EAST);

        pnlOperation.setMaximumSize(new Dimension(700, 22));
        pnlOperation.setMinimumSize(new Dimension(340, 22));
        pnlOperation.setPreferredSize(new Dimension(700, 27));
        pnlOperation.setLayout(new BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setMaximumSize(new Dimension(700, 32769));
        tbInsertion.setMinimumSize(new Dimension(480, 25));
        tbInsertion.setPreferredSize(new Dimension(681, 25));

        datePane.setDataAlvoVsisible(true);
        datePane.setMaximumSize(new Dimension(400, 30));
        tbInsertion.add(datePane);

        spt3.setMaximumSize(new Dimension(20, 20));
        spt3.setMinimumSize(new Dimension(20, 20));
        spt3.setPreferredSize(new Dimension(20, 20));
        tbInsertion.add(spt3);

        btnReload.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setPreferredSize(new Dimension(25, 27));
        btnReload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        spt2.setMaximumSize(new Dimension(20, 20));
        spt2.setMinimumSize(new Dimension(20, 20));
        spt2.setPreferredSize(new Dimension(20, 20));
        tbInsertion.add(spt2);

        pnlManutencao.setMaximumSize(new Dimension(120, 120));
        pnlManutencao.setPreferredSize(new Dimension(70, 25));
        pnlManutencao.setLayout(new BorderLayout());

        tblManutencao.setFloatable(false);
        tblManutencao.setRollover(true);
        tblManutencao.setMaximumSize(new Dimension(70, 70));
        tblManutencao.setMinimumSize(new Dimension(70, 70));
        tblManutencao.setPreferredSize(new Dimension(70, 25));

        btnEdit.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.setFocusable(false);
        btnEdit.setHorizontalAlignment(SwingConstants.TRAILING);
        btnEdit.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnEdit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tblManutencao.add(btnEdit);

        btnDelete.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.setFocusable(false);
        btnDelete.setHorizontalAlignment(SwingConstants.TRAILING);
        btnDelete.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tblManutencao.add(btnDelete);

        pnlManutencao.add(tblManutencao, BorderLayout.CENTER);

        tbInsertion.add(pnlManutencao);

        spt1.setMaximumSize(new Dimension(20, 20));
        spt1.setMinimumSize(new Dimension(20, 20));
        spt1.setPreferredSize(new Dimension(20, 20));
        tbInsertion.add(spt1);

        btnAjuda.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        ResourceBundle bundle1 = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnAjuda.setText(bundle1.getString("KpiAjuda_00001")); // NOI18N
        btnAjuda.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda);

        pnlOperation.add(tbInsertion, BorderLayout.NORTH);
        tbInsertion.getAccessibleContext().setAccessibleParent(this);

        pnlTools.add(pnlOperation, BorderLayout.WEST);

        pnlRecord.add(pnlTools, BorderLayout.PAGE_START);

        pnlFields.setLayout(new BorderLayout());

        tskDados.setSpecial(true);
        ResourceBundle bundle2 = ResourceBundle.getBundle("international"); // NOI18N
        tskDados.setTitle(bundle2.getString("KpiApresentacao_00017")); // NOI18N

        pnlDados.setOpaque(false);
        pnlDados.setPreferredSize(new Dimension(0, 130));
        pnlDados.setLayout(null);

        lblNome.setForeground(new Color(51, 51, 255));
        lblNome.setText(bundle.getString("KpiPainel_00003")); // NOI18N
        lblNome.setEnabled(false);
        pnlDados.add(lblNome);
        lblNome.setBounds(10, 0, 85, 20);

        lblIndicador.setText(bundle.getString("KpiPainel_00005")); // NOI18N
        lblIndicador.setEnabled(false);
        pnlDados.add(lblIndicador);
        lblIndicador.setBounds(440, 40, 85, 20);
        pnlDados.add(cbIndicador);
        cbIndicador.setBounds(440, 60, 400, 22);

        fldNome.setEnabled(false);
        fldNome.setMaxLength(60);
        pnlDados.add(fldNome);
        fldNome.setBounds(10, 20, 400, 22);

        lblDescricao.setHorizontalAlignment(SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("KpiPainel_00002")); // NOI18N
        lblDescricao.setEnabled(false);
        lblDescricao.setMaximumSize(new Dimension(100, 16));
        lblDescricao.setMinimumSize(new Dimension(100, 16));
        lblDescricao.setPreferredSize(new Dimension(100, 16));
        pnlDados.add(lblDescricao);
        lblDescricao.setBounds(10, 40, 85, 20);

        txtDescricao.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        txtDescricao.setPreferredSize(new Dimension(260, 56));
        scrDescricao.setViewportView(txtDescricao);

        pnlDados.add(scrDescricao);
        scrDescricao.setBounds(10, 60, 400, 60);

        lblScorecard.setText(KpiStaticReferences.getKpiCustomLabels().getSco(KpiStaticReferences.CAD_OBJETIVO).concat(":"));
        lblScorecard.setEnabled(false);
        pnlDados.add(lblScorecard);
        lblScorecard.setBounds(440, 0, 160, 20);

        cbScorecard.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                cbScorecardItemStateChanged(evt);
            }
        });
        cbScorecard.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                cbScorecardActionPerformed(evt);
            }
        });
        pnlDados.add(cbScorecard);
        cbScorecard.setBounds(440, 20, 400, 22);

        tlbAdicionar.setFloatable(false);
        tlbAdicionar.setRollover(true);
        tlbAdicionar.setOpaque(false);
        tlbAdicionar.setPreferredSize(new Dimension(25, 25));

        btnAddIndicador.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_verificar.gif"))); // NOI18N
        btnAddIndicador.setToolTipText(bundle1.getString("KpiPainelComp_00008")); // NOI18N
        btnAddIndicador.setHorizontalAlignment(SwingConstants.LEFT);
        btnAddIndicador.setMaximumSize(new Dimension(100, 27));
        btnAddIndicador.setMinimumSize(new Dimension(100, 27));
        btnAddIndicador.setOpaque(false);
        btnAddIndicador.setPreferredSize(new Dimension(22, 22));
        btnAddIndicador.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAddIndicadorActionPerformed(evt);
            }
        });
        tlbAdicionar.add(btnAddIndicador);

        pnlDados.add(tlbAdicionar);
        tlbAdicionar.setBounds(840, 60, 25, 25);

        tsArvore.setCombo(cbScorecard);
        tsArvore.setRoot(false);
        pnlDados.add(tsArvore);
        tsArvore.setBounds(840, 20, 30, 22);

        chkPublico.setText("P�blico");
        chkPublico.setOpaque(false);
        pnlDados.add(chkPublico);
        chkPublico.setBounds(440, 90, 59, 23);

        tskDados.getContentPane().add(pnlDados);

        tskPaineis.add(tskDados);

        tskPainel.setTitle(bundle2.getString("KpiPainelComp_00014")); // NOI18N

        pnlPainel.setBackground(new Color(255, 255, 255));
        pnlPainel.setPreferredSize(new Dimension(10, 500));
        pnlPainel.setLayout(new BorderLayout());

        pnlPainelToolBar.setBackground(new Color(255, 255, 255));
        pnlPainelToolBar.setPreferredSize(new Dimension(10, 22));
        pnlPainelToolBar.setLayout(new BorderLayout());

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);
        jToolBar1.setOpaque(false);
        jToolBar1.setPreferredSize(new Dimension(185, 22));

        btnPrint.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_imprimir.gif"))); // NOI18N
        btnPrint.setText(bundle.getString("KpiPainel_00011")); // NOI18N
        btnPrint.setOpaque(false);
        btnPrint.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });
        jToolBar1.add(btnPrint);

        btnExportacao.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_exportacao.gif"))); // NOI18N
        btnExportacao.setText(bundle1.getString("KpiDataSourceFrame_00032")); // NOI18N
        btnExportacao.setOpaque(false);
        btnExportacao.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnExportacaoActionPerformed(evt);
            }
        });
        jToolBar1.add(btnExportacao);

        btnAbrir.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_download.gif"))); // NOI18N
        btnAbrir.setText(bundle1.getString("KpiDataSourceFrame_00033")); // NOI18N
        btnAbrir.setOpaque(false);
        btnAbrir.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });
        jToolBar1.add(btnAbrir);

        pnlPainelToolBar.add(jToolBar1, BorderLayout.CENTER);

        pnlPainel.add(pnlPainelToolBar, BorderLayout.NORTH);

        pnlPainelContent.setPreferredSize(new Dimension(100, 100));
        pnlPainelContent.setLayout(new BoxLayout(pnlPainelContent, BoxLayout.LINE_AXIS));

        scrlPainel.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        scrlPainel.setAutoscrolls(true);

        dskPaineis.setBackground(new Color(255, 255, 255));
        scrlPainel.setViewportView(dskPaineis);

        pnlPainelContent.add(scrlPainel);

        pnlPainel.add(pnlPainelContent, BorderLayout.CENTER);

        tskPainel.getContentPane().add(pnlPainel);

        tskPaineis.add(tskPainel);

        pnlFields.add(tskPaineis, BorderLayout.CENTER);

        pnlRecord.add(pnlFields, BorderLayout.CENTER);

        getContentPane().add(pnlRecord, BorderLayout.CENTER);

        pnlRightRecord.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlRightRecord, BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlLeftForm, BorderLayout.WEST);

        pnlBottomForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlBottomForm, BorderLayout.SOUTH);

        getAccessibleContext().setAccessibleParent(this);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed
        kpi.swing.KpiFileChooser frmChooser = new kpi.swing.KpiFileChooser(kpi.core.KpiStaticReferences.getKpiMainPanel(), true);
        frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_OPEN);

        frmChooser.setFileType("*.jpg");
        frmChooser.loadServerFiles("graphs\\*.jpg");

        frmChooser.setVisible(true);
        if (frmChooser.isValidFile()) {
            try {
                kpi.applet.KpiApplet kpiApplet = kpi.core.KpiStaticReferences.getKpiApplet();
                String strFileName = "graphs//" + frmChooser.getFileName();
                java.net.URL url = new java.net.URL(kpiApplet.getCodeBase().toString() + strFileName);
                kpiApplet.getAppletContext().showDocument(url, "_blank");
            } catch (Exception e) {
            }
        }
    }//GEN-LAST:event_btnAbrirActionPerformed

    private void btnExportacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportacaoActionPerformed
        scrlPainel.getVerticalScrollBar().setValue(1);
        kpi.util.KpiToolKit tool = new kpi.util.KpiToolKit();
        kpi.swing.KpiFileChooser frmChooser = new kpi.swing.KpiFileChooser(kpi.core.KpiStaticReferences.getKpiMainPanel(), true);
        frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_SAVE);
        frmChooser.setFileType("*.jpg");
        frmChooser.loadServerFiles("graphs\\*.jpg");
        frmChooser.setFileName("painel.jpg");
        frmChooser.setVisible(true);

        if (frmChooser.isValidFile()) {
            tool.saveComponentAsJPEG(dskPaineis, "graphs//" + frmChooser.getFileName());
        }
    }//GEN-LAST:event_btnExportacaoActionPerformed

    private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
        event.loadHelp(recordType);
    }//GEN-LAST:event_btnAjudaActionPerformed

	private void cbScorecardItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbScorecardItemStateChanged
            if (cbScorecard.getSelectedIndex() == 0) {
                cbIndicador.removeAllItems();
            } else {
                kpi.xml.BIXMLRecord recIndFiltrados = event.listRecords("INDICADOR", "ID_SCOREC = '".concat(event.getComboValue(htbScorecard, cbScorecard)) + "'");
                event.populateCombo(recIndFiltrados.getBIXMLVector("INDICADORES"), "INDICADOR", htbIndicador, cbIndicador);
            }

	}//GEN-LAST:event_cbScorecardItemStateChanged

	private void cbScorecardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbScorecardActionPerformed
			    	}//GEN-LAST:event_cbScorecardActionPerformed

	private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
            scrlPainel.getVerticalScrollBar().setValue(1);
            java.awt.print.PrinterJob printJob = java.awt.print.PrinterJob.getPrinterJob();
            printJob.setPrintable(this);
            if (printJob.printDialog()) {
                try {
                    printJob.print();
                } catch (Exception ex) {
                }
            }
	}//GEN-LAST:event_btnPrintActionPerformed

	private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
            //dskPaineis.autoOrganizeFrames();
	}//GEN-LAST:event_formComponentResized

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            StringBuilder request = new StringBuilder();
            request.append(datePane.getDataAlvo());
            request.append("|");
            request.append(datePane.getDataDe());
            request.append("|");
            request.append(datePane.getDataAte());

            setRecord(event.loadRecord(id, recordType, request.toString()));
            refreshFields();
            dskPaineis.autoOrganizeFrames();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
            setStatus(KpiDefaultFrameBehavior.UPDATING);
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

        private void btnAddIndicadorActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnAddIndicadorActionPerformed
            addCard();
}//GEN-LAST:event_btnAddIndicadorActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnAbrir;
    private JButton btnAddIndicador;
    private JButton btnAjuda;
    private JButton btnCancel;
    private JButton btnDelete;
    private JButton btnEdit;
    private JButton btnExportacao;
    private JButton btnPrint;
    private JButton btnReload;
    private JButton btnSave;
    private JComboBox cbIndicador;
    private JComboBox cbScorecard;
    private JCheckBox chkPublico;
    private JBIDatePane datePane;
    private JBICardDesktopPane dskPaineis;
    private JPVEdit fldNome;
    private JToolBar jToolBar1;
    private JLabel lblDescricao;
    private JLabel lblIndicador;
    private JLabel lblNome;
    private JLabel lblScorecard;
    private JPanel pnlBottomForm;
    private JPanel pnlConfirmation;
    private JPanel pnlDados;
    private JPanel pnlFields;
    private JPanel pnlLeftForm;
    private JPanel pnlManutencao;
    private JPanel pnlOperation;
    private JPanel pnlPainel;
    private JPanel pnlPainelContent;
    private JPanel pnlPainelToolBar;
    private JPanel pnlRecord;
    private JPanel pnlRightRecord;
    private JPanel pnlTools;
    private JScrollPane scrDescricao;
    private JScrollPane scrlPainel;
    private Separator spt1;
    private Separator spt2;
    private Separator spt3;
    private JToolBar tbConfirmation;
    private JToolBar tbInsertion;
    private JToolBar tblManutencao;
    private JToolBar tlbAdicionar;
    private JBITreeSelection tsArvore;
    private JTaskPaneGroup tskDados;
    private JTaskPane tskPaineis;
    private JTaskPaneGroup tskPainel;
    private JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {

        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return null;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public void afterExecute() {
    }
    
    /**
    * Assumimos a responsabilidade da passar informa��es para impress�o.
    * @params vem padr�o doSGIADMIN override.
    * @return
    * @throws java.awt.print.PrinterException 
    * @since 23/02/2016
    * @author Helio Leal
    */
    @Override
    public int print(java.awt.Graphics g, java.awt.print.PageFormat pageFormat, int pageIndex) throws java.awt.print.PrinterException {
        if (pageIndex > 0) {
                return (NO_SUCH_PAGE);
        } else {
            java.awt.Graphics2D g2d = (java.awt.Graphics2D) g;
            g2d.scale(0.50, 0.50);
            disableDoubleBuffering(this);
            g2d.translate(0, 100);
            //--------------------------------------------------
            // Adiciona painel com os widgets dos indicadores para impress�o.
            //--------------------------------------------------
            dskPaineis.autoOrganizeFrames();
            dskPaineis.paint(g2d);
            enableDoubleBuffering(this);
            //--------------------------------------------------
            // Formato em que as datas devem aparecer.
            //--------------------------------------------------
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yy");
            g2d.translate(pageFormat.getImageableX(), 0);
            Font fontBold = new Font(Font.SANS_SERIF, Font.BOLD, 15); // Font com negrito.
            Font fontPlain = new Font(Font.SANS_SERIF, Font.PLAIN, 15); // sem negrito.
            g2d.setColor(Color.BLACK); // Cor da fonte � preta.
            //--------------------------------------------------
            // Textos do cabe�alho para impress�o.
            //--------------------------------------------------
            g2d.setFont(fontBold);
            g2d.drawString(java.util.ResourceBundle.getBundle("international").getString("KpiPainel_00003"), 10, -50); // Painel
            g2d.setFont(fontPlain);
            g2d.drawString(fldNome.getText().trim(), 65, -50);
            g2d.setFont(fontBold);
            g2d.drawString(java.util.ResourceBundle.getBundle("international").getString("KpiPainel_00006"), 10, -30); // Data alvo
            try {
                g2d.setFont(fontPlain);
                //--------------------------------------------------
                // Data alvo.
                //--------------------------------------------------
                Date data = dateFormat.parse(datePane.getDataAlvo());
                g2d.drawString(dateFormat.format(data), 90, -30);
                g2d.setFont(fontBold);                
                //--------------------------------------------------
                // Acumulado de.
                //--------------------------------------------------
                g2d.drawString(java.util.ResourceBundle.getBundle("international").getString("KpiPainelComp_00006"), 10, -10); // Acumulado de:
                g2d.setFont(fontPlain);
                data = dateFormat.parse(datePane.getDataDe());
                g2d.drawString(dateFormat.format(data), 125, -10);
                g2d.setFont(fontBold);
                //--------------------------------------------------
                // Acumulado at�.
                //--------------------------------------------------
                g2d.drawString(java.util.ResourceBundle.getBundle("international").getString("KpiPainelComp_00007"), 190, -10); // At�
                g2d.setFont(fontPlain);
                data = dateFormat.parse(datePane.getDataAte());
                g2d.drawString(dateFormat.format(data), 225, -10);
            } catch (ParseException ex) {
                Logger.getLogger(KpiPainel.class.getName()).log(Level.SEVERE, null, ex);
            }
            return (PAGE_EXISTS);
        }
   }
}
