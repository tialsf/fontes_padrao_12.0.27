/*
 * Created on 09/28/2006
 * @author  siga2516-Lucio Pelinson
 */
package kpi.swing.analisys;

import ChartDirector.Chart;
import ChartDirector.ChartViewer;
import ChartDirector.LegendBox;
import com.l2fprod.common.swing.JTaskPane;
import com.l2fprod.common.swing.JTaskPaneGroup;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.JToolBar.Separator;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import kpi.beans.JBICardDesktopPane;
import kpi.beans.JBIChartDirectorController;
import kpi.beans.JBIDatePane;
import kpi.beans.JBITextArea;
import kpi.beans.JBITreeSelection;
import kpi.core.KpiDataController;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiFileChooser;
import kpi.util.KpiDateUtil;
import kpi.util.KpiToolKit;
import kpi.xml.BIXMLException;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;
import pv.jfcx.JPVEdit;

public class KpiPainelComp extends kpi.swing.KpiInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "PAINELCOMP";
    private String parentType = "LSTPAINELCOMP";
    private String id = "";
    private String formType = recordType;
    private kpi.core.KpiDataController dataController = kpi.core.KpiStaticReferences.getKpiDataController();
    private kpi.swing.KpiDefaultDialogSystem dialog = kpi.core.KpiStaticReferences.getKpiDialogSystem();
    private kpi.core.KpiDateBase dateBase = kpi.core.KpiStaticReferences.getKpiDateBase();
    private KpiDefaultFrameBehavior event = new KpiDefaultFrameBehavior(this);
    private kpi.util.KpiDateUtil convDates = new kpi.util.KpiDateUtil();
    private int status = KpiDefaultFrameBehavior.NORMAL;
    private kpi.xml.BIXMLRecord record = null;
    //
    private Hashtable htbScorecard = new Hashtable();
    private Hashtable htbIndicador = new Hashtable();
    private Hashtable htbFrequencia = new Hashtable();
    //
    private JBIChartDirectorController graphController;
    private LegendBox graphLegend;
    private BIXMLVector graphRecord;
    //
    private final static int PERIODO = 0;
    private final static int VALOR = 1;
    private final static int VARIACAO_PERCENTUAL = 2;
    //Utilizado para a atualiza��o on-line do gr�fico.
    BIXMLVector painelTemporario = new BIXMLVector();

    public KpiPainelComp(int operation, String idAux, String contextId) {
        ChartDirector.Chart.setLicenseCode("RDST-245J-39ZJ-A6R8-ED7B-1DCB");
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
        event.setEnableFields(pnlDados, true);

        for (int i = 0; i < dskPaineis.getComponentCount(); i++) {
            ((KpiIndicadorTable) dskPaineis.getComponent(i)).setClosable(true);
        }

        bloqueiaFrequencia();

        cbIndicador.setEnabled(true);
        btnAddIndicador.setVisible(true);
    }

    @Override
    public void disableFields() {
        event.setEnableFields(pnlDados, false);

        for (int i = 0; i < dskPaineis.getComponentCount(); i++) {
            ((KpiIndicadorTable) dskPaineis.getComponent(i)).setClosable(false);
        }
    }

    @Override
    public void refreshFields() {
        //Preenche os campos descritivos.
        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        chkPublico.setSelected(record.getBoolean("PUBLICO"));
        event.setEnableFields(pnlManutencao, record.getBoolean("EDITA"));

        //Recupera os dados para os combo boxes.
        BIXMLVector vctFrequencia = record.getBIXMLVector("FREQUENCIAS");
        BIXMLVector vctScorecard = record.getBIXMLVector("SCORECARDS");

        //Popula os combo boxes.
        event.populateCombo(vctScorecard, "ID", htbScorecard, cbScorecard);
        event.populateCombo(vctFrequencia, "ID", htbFrequencia, cbFrequencia);

        //Posiciona os combo boxes.
        event.selectComboItem(cbFrequencia, event.getComboItem(vctFrequencia, record.getString("FREQUENCIA"), true));
        event.selectComboItem(cbScorecard, 0);

        //Informa ao seletor vetor relacionado com o combo scorecard.
        tsArvore.setVetor(vctScorecard);

        //Define o estado dos componente do painel.
        if (status == KpiDefaultFrameBehavior.INSERTING) {
            this.panelInsertRule();
        } else {
            this.panelMaintenenceRule();
        }

        if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
            tsArvore.setEntitySelection(KpiStaticReferences.CAD_OBJETIVO);
        }
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        StringBuilder paineis = new StringBuilder();

        recordAux.set("ID", id);
        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("FREQUENCIA", event.getComboValue(htbFrequencia, cbFrequencia));
        recordAux.set("DATADE", datePane.getDataDe());
        recordAux.set("DATAATE", datePane.getDataAte());
        recordAux.set("OCVARIACAO", chkHideVariacao.isSelected());
        recordAux.set("OCREALACUM", chkHideRealAcumulado.isSelected());
        recordAux.set("OCMETAACUM", chkHideMetaAcumulada.isSelected());
        recordAux.set("OCVALORES", chkShowValues.isSelected());
        recordAux.set("OCSWAPXY", chkSwapXY.isSelected());
        recordAux.set("GRAFTIPO", cmdGraphSerieType.getSelectedIndex());
        recordAux.set("VARPERCENT", chkVariacaoPercentual.isSelected());
        recordAux.set("ACUMULADO", chkAcumulado.isSelected());
        recordAux.set("PUBLICO", chkPublico.isSelected());

        //Recupera todas as tabelas inseridas no painel comparativo.
        Vector tabelas = dskPaineis.getVctFrames();

        for (int i = 0; i < tabelas.size(); i++) {
            //Itera por cada tabela.
            KpiIndicadorTable tabela = (KpiIndicadorTable) tabelas.get(i);
            //Recupera o ID.
            paineis.append(tabela.getId());
            //Concatena os IDs das tabelas do painel com pipe.
            paineis.append("|");
        }

        //Retorna uma String contando o ID de todos as tabelas do painel.
        recordAux.set("PAINEISID", paineis.toString());

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        if (fldNome.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPainel_00014"));
            errorMessage.append("\n");
            retorno = false;
        }

        if (cbFrequencia.getSelectedIndex() == 0) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00002"));
            errorMessage.append("\n");
            retorno = false;
        }

        if (!retorno) {
            btnSave.setEnabled(true);
        }

        return retorno;
    }

    /**
     * Define o estado dos componentes na manuten��o do painel.
     * @throws BIXMLException
     */
    private void panelMaintenenceRule() throws BIXMLException {
        if (record.contains("CARDS")) {
            chkHideVariacao.setSelected(record.getBoolean("OCVARIACAO"));
            chkHideRealAcumulado.setSelected(record.getBoolean("OCREALACUM"));
            chkHideMetaAcumulada.setSelected(record.getBoolean("OCMETAACUM"));
            chkShowValues.setSelected(record.getBoolean("OCVALORES"));
            chkSwapXY.setSelected(record.getBoolean("OCSWAPXY"));
            cmdGraphSerieType.setSelectedIndex(record.getInt("GRAFTIPO"));
            chkVariacaoPercentual.setSelected(record.getBoolean("VARPERCENT"));
            chkAcumulado.setSelected(record.getBoolean("ACUMULADO"));

            //Remove todas as tabelas do painel.
            dskPaineis.removeAll();

            //Monta as tabelas.
            BIXMLVector painel = record.getBIXMLVector("CARDS");
            painelTemporario = painel;

            for (int i = 0; i < painel.size(); i++) {
                buildTable(painel.get(i), false);
            }

            //Monta o gr�fico.
            buildGraph(painel);

            //Define o estado inicial dos componentes.
            tskDados.setExpanded(false);
            tskPaineis.setVisible(true);
            dskPaineis.setAutoOrganize(true);
            dskPaineis.autoOrganizeFrames();
            //For�a o scroll
            dskPaineis.setSize(dskPaineis.getHeight() + 1, dskPaineis.getWidth());
        } else {
            //Define o estado inicial dos pain�is.
            tskDados.setExpanded(true);
            tskPaineis.setVisible(true);
            tskPaineis.setExpanded(true);
        }
    }

    /**
     * Define o estado dos componentes na inser��o de painel.
     */
    private void panelInsertRule() {
        //Define o estados inicial dos check boxes.
        chkHideVariacao.setSelected(false);
        chkHideRealAcumulado.setSelected(false);
        chkHideMetaAcumulada.setSelected(false);
        chkShowValues.setSelected(false);
        chkSwapXY.setSelected(false);

        //Define o estado inicial dos pain�is.
        tskDados.setExpanded(true);
        tskPaineis.setExpanded(true);
    }

    /**
     * Identifica se uma tabela pode ser adicionada no painel comparativo.
     * @param tabela XMLRecord da tabela a ser inclu�da no painel.
     * @return true para quando a tabela n�o est� inserida no painel.
     * @throws BIXMLException
     */
    private boolean canBuildTable(BIXMLRecord tabela) throws BIXMLException {
        boolean canBuildTable = true;
        JInternalFrame[] internalFrames = dskPaineis.getAllFrames();

        for (int j = 0; j < internalFrames.length; j++) {
            KpiCard frame = (KpiCard) internalFrames[j];
            if (frame.getNome().equals(tabela.getString("NOME"))) {
                canBuildTable = false;
            }
        }
        return canBuildTable;
    }

    /**
     * Monta o componente table e adiciona ao pain�l comparativo.
     * @param dados_da_tabela BIXMLRecord contendo a estrutura da tabe�a
     * @param pode_fechar indica se a tabela pode ser fechada.
     */
    private void buildTable(BIXMLRecord dados_da_tabela, boolean fechar) {
        KpiIndicadorTable tabela = new kpi.swing.analisys.KpiIndicadorTable(dados_da_tabela);

        tabela.showFechar(fechar);

        tabela.setBounds(0, 0, this.getWidth() - 100, tabela.getHeight());

        tabela.showLinha("VARIACAO", !chkHideVariacao.isSelected());
        tabela.showLinha("VARIACUM", !chkHideVariacao.isSelected());
        tabela.showLinha("REALACUM", !chkHideRealAcumulado.isSelected());
        tabela.showLinha("METAACUM", !chkHideMetaAcumulada.isSelected());

        dskPaineis.add(tabela);

        tabela.pack();
        tabela.show();
    }

    /**
     * Coordena a montagem do gr�fico.
     * @param dados_do_painel BIXMLVector contendo todas as tabela do painel.
     * @autor Valdiney V GOMES
     */
    private void buildGraph(BIXMLVector dados_do_painel) {
        this.setGraphRecord(dados_do_painel);

        graphController = new JBIChartDirectorController(oChartViewer);
        graphController.setGraphType(JBIChartDirectorController.XY_CHART);

        this.graphConfig();

        graphController.resetChart();

        this.graphData();
        this.graphSize();

        graphController.drawGraph();
    }

    /**
     * Define as configura��es de exibi��o do gr�fico.
     * @autor Valdiney V GOMES
     */
    private void graphConfig() {
        graphController.setXLegenda(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00055"), false);
        graphController.setLegXvisible(true);

        graphController.setYLegenda(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00056"), false);
        graphController.setLegYvisible(true);

        graphController.setSwapXY(chkSwapXY.isSelected(), false);

        if (graphController.isSwapXY()) {
            graphController.setLegValAngle(0);
        } else {
            graphController.setLegValAngle(90);
        }

        graphController.setLegValVisible(chkShowValues.isSelected());
    }

    /**
     * Insere os dados para todos os itens de exibi��o do gr�fico.
     * @autor Valdiney V GOMES
     */
    private void graphData() {
        graphController.setTitle(fldNome.getText(), false);
        graphLegend = graphController.getBaseChart().addLegend(0, 0, false, "Tahoma", 8);
        graphLegend.setKeyBorder(Chart.CColor(new Color(207, 207, 207)), 0);

        BIXMLVector dados_do_painel = this.getGraphRecord();

        for (int i = 0; i < dados_do_painel.size(); i++) {
            BIXMLVector dados_do_grafico = dados_do_painel.get(i).getBIXMLVector("GRAPH");

            Color cor = this.graphColor(i);

            this.graphLegend(dados_do_painel.get(i), cor);

            this.graphPeriod(manageAccrued(dados_do_grafico.get(PERIODO)));
            this.graphValues(manageAccrued(dados_do_grafico.get(getExhibition())), cor);
        }
    }

    /**
     * Monta a legenda do gr�fico.
     * @param dados_do_Indicador BIXMLRecord contendo dados da tabela de um indicador.
     * @param cor cor que ser� exibido o �cone da legenda no gr�fico.
     * @autor Valdiney V GOMES
     */
    private void graphLegend(BIXMLRecord dados_do_Indicador, Color cor) {
        graphLegend.addKey(dados_do_Indicador.getString("NOME"), Chart.CColor(cor));
        graphController.setLegendPos(3, false);
    }

    /**
     * Monta os itens do eixo X do gr�fico.
     * @param periodos BIXMLRecord contendo os dados do per�odo para o gr�fico.
     * @autor Valdiney V GOMES
     */
    private void graphPeriod(BIXMLRecord periodos) {
        String[] periodos_do_grafico = new String[periodos.size()];
        int i = 0;

        for (Iterator e = periodos.getKeyNames(); e.hasNext(); i++) {
            String chave = (String) e.next();
            periodos_do_grafico[i] = periodos.getString(chave);
        }

        graphController.setLegendas(periodos_do_grafico);
    }

    /**
     * Monta os itens do eixo Y do gr�fico.
     * @param valores BIXMLRecord contendo os valores para cada s�rie do gr�fico.
     * @param cor Cor de exibi��o da s�rie do gr�fico.
     * @autor Valdiney V GOMES
     */
    private void graphValues(BIXMLRecord valores, Color cor) {
        double[] valores_do_grafico = new double[valores.size()];
        int i = 0;
        int serieKey = graphController.getSerieSize();

        for (Iterator e = valores.getKeyNames(); e.hasNext(); i++) {
            String chave = (String) e.next();
            valores_do_grafico[i] = valores.getDouble(chave);
        }

        graphController.addSerie(serieKey, valores_do_grafico);
        graphController.getSerieItem(serieKey).setColor(cor);
        graphController.getSerieItem(serieKey).setSymbol(Chart.SquareSymbol);
        graphController.getSerieItem(serieKey).setSerieType(graphSerieType());

        graphController.updateGraph(serieKey);
    }

    /**
     * Ajusta a visualiza�ao do gr�fico de acordo com quantidade de registros e
     * tamanho da janela.
     * @autor Valdiney V GOMES
     */
    private void graphSize() {
        int altura_da_Janela = this.getContentPane().getHeight();
        int largura_do_grafico = graphController.getChartViewer().getParent().getWidth();

        int altura_do_grafico = altura_da_Janela - 125;
        int espaco_por_serie = 10;
        int series = graphController.getSerieSize();
        int registros = graphController.getSerieItem(0).getSerieValor().length;

        if (graphController.isSwapXY()) {
            altura_do_grafico += ((series * espaco_por_serie) * registros);

            //Subtrai a largura da barra de rolagem da largura do gr�fico [20].

            oChartViewer.setPreferredSize(
                    new Dimension(
                    largura_do_grafico - 20,
                    altura_do_grafico));

            graphController.setSize(
                    largura_do_grafico - 20,
                    altura_do_grafico);


            graphLegend.setHeight(altura_do_grafico - 95);

        } else {
            largura_do_grafico += ((series * espaco_por_serie) * registros);

            //Subtrai a altura da barra de rolagem da altura  do gr�fico [20].
            //Subtrai a altura da aba do painel tabulado da altura  do gr�fico [30].
            //Subtrai a altura da barra de op��es da altura do gr�fico [22].

            oChartViewer.setPreferredSize(
                    new Dimension(
                    largura_do_grafico,
                    altura_do_grafico - 125));

            graphController.setSize(
                    largura_do_grafico,
                    altura_do_grafico - 125);

            graphLegend.setHeight(altura_do_grafico - 220);
        }

        pnlPainel.setPreferredSize(new Dimension(largura_do_grafico, altura_da_Janela - 130));
    }

    /**
     * Define as cores para cada s�rie do gr�fico.
     * @param serie S�rie do gr�fico para a qual ser� definida a cor.
     * @return Cor para a s�rie.
     * @autor Valdiney V GOMES
     */
    private Color graphColor(int serie) {
        Color cor = null;

        if (serie > 15) {
            while (serie > 15) {
                serie -= 16;
            }
        }

        switch (serie) {
            case 0:
                cor = new Color(0, 191, 255); //Azul
                break;
            case 1:
                cor = new Color(255, 99, 71); //Vermelho
                break;
            case 2:
                cor = new Color(255, 255, 0); //Amarelo
                break;
            case 3:
                cor = new Color(60, 179, 113); //Verde
                break;
            case 4:
                cor = new Color(130, 130, 130); //Cinza Escuro
                break;
            case 5:
                cor = new Color(137, 104, 205); //Roxo
                break;
            case 6:
                cor = new Color(84, 255, 159); //Verde Claro
                break;
            case 7:
                cor = new Color(255, 145, 158); //Rosa
                break;
            case 8:
                cor = new Color(255, 165, 0); //Laranja
                break;
            case 9:
                cor = new Color(0, 245, 255); //Azul
                break;
            case 10:
                cor = new Color(127, 255, 212); //Verde Claro
                break;
            case 11:
                cor = new Color(255, 0, 0); //Vermelho
                break;
            case 12:
                cor = new Color(207, 207, 207); //Cinza
                break;
            case 13:
                cor = new Color(255, 225, 255); //Rosa Claro
                break;
            case 14:
                cor = new Color(34, 139, 34); //Verde Escuro
                break;
            case 15:
                cor = new Color(139, 119, 101); //Marrom
                break;
        }

        return cor;
    }

    /**
     * Define o tipo de s�rie do gr�fico.
     * @return Tipo de s�rie para o gr�fico.
     * @autor Valdiney V GOMES
     */
    private int graphSerieType() {
        int serieType = 0;

        switch (cmdGraphSerieType.getSelectedIndex()) {
            case 0:
                serieType = JBIChartDirectorController.LINE;
                break;
            case 1:
                serieType = JBIChartDirectorController.LINE_3D;
                break;
            case 2:
                serieType = JBIChartDirectorController.BAR;
                break;
            case 3:
                serieType = JBIChartDirectorController.BAR_3D;
                break;
            case 4:
                serieType = JBIChartDirectorController.CYLINDER_3D;
                break;
        }

        return serieType;
    }

    /**
     * Retorna os registros correntes da montagem do gr�fico.
     * @return Dados utilizado na montagem do gr�fico.
     * @autor Valdiney V GOMES
     */
    public BIXMLVector getGraphRecord() {
        return graphRecord;
    }

    /**
     * Armazena os registros correntes da montagem do gr�fico.
     * @param dados_do_painel BIXMLVector contendo os dados de todas as tabela do painel.
     * @autor Valdiney V GOMES
     */
    public void setGraphRecord(BIXMLVector dados_do_painel) {
        this.graphRecord = dados_do_painel;
    }

    /**
     * Define a s�rie de valores a ser exibida no gr�fico.
     * @return S�rie a ser exibida.
     * @autor Valdiney V GOMES
     */
    public int getExhibition() {
        int exhibition = 0;

        if (chkVariacaoPercentual.isSelected()) {
            exhibition = VARIACAO_PERCENTUAL;
            graphController.setUnitMeasure("Percentual", false);

        } else {
            exhibition = VALOR;
        }

        return exhibition;
    }

    /**
     * Trata a exibi��o dos valores acumulados.
     * @param record Registros a ser verificados.
     * @return record.
     * @autor Valdiney V GOMES
     */
    private BIXMLRecord manageAccrued(BIXMLRecord record) {
        BIXMLRecord temporario = record.clone2();

        if (!chkAcumulado.isSelected()) {
            if (temporario.contains("XACUM")) {
                temporario.remove("XACUM");
            }

            if (temporario.contains("YACUM")) {
                temporario.remove("YACUM");
            }
        }

        return temporario;
    }

    /**
     * For�a a atualiza��o visual do gr�fico sem atualiza��o de dados.
     * @autor Valdiney V GOMES
     */
    private void refleshGraph() {
        if (!(this.getGraphRecord() == null)) {
            this.buildGraph(this.getGraphRecord());
        }
    }

    /**
     * Controla o altera��o de frequ�ncia de um painel comparativo.
     */
    private void bloqueiaFrequencia() {
        cbFrequencia.setEnabled(dskPaineis.getComponentCount() == 0);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new ButtonGroup();
        pnlRecord = new JPanel();
        pnlTools = new JPanel();
        pnlConfirmation = new JPanel();
        tbConfirmation = new JToolBar();
        btnSave = new JButton();
        btnCancel = new JButton();
        pnlOperation = new JPanel();
        tbInsertion = new JToolBar();
        datePane = new JBIDatePane(this);
        spt1 = new Separator();
        btnReload = new JButton();
        spt2 = new Separator();
        pnlManutencao = new JPanel();
        tlbManutencao = new JToolBar();
        btnEdit = new JButton();
        btnDelete = new JButton();
        spt3 = new Separator();
        btnAjuda = new JButton();
        pnlFields = new JPanel();
        tskComparativo = new JTaskPane();
        tskDados = new JTaskPaneGroup();
        pnlDados = new JPanel();
        lblIndicador = new JLabel();
        cbIndicador = new JComboBox();
        fldNome = new JPVEdit();
        lblNome = new JLabel();
        scrDescricao = new JScrollPane();
        txtDescricao = new JBITextArea();
        lblScorecard = new JLabel();
        cbScorecard = new JComboBox();
        jLabel1 = new JLabel();
        cbFrequencia = new JComboBox();
        tlbAdicionar = new JToolBar();
        btnAddIndicador = new JButton();
        lblDescricao1 = new JLabel();
        tsArvore = new JBITreeSelection();
        chkPublico = new JCheckBox();
        tskPaineis = new JTaskPaneGroup();
        pnlPainel = new JPanel();
        tbComparativo = new JTabbedPane();
        pnlGraph = new JPanel();
        pnlItensGrafico = new JPanel();
        lblTipo = new JLabel();
        cmdGraphSerieType = new JComboBox();
        chkShowValues = new JCheckBox();
        chkAcumulado = new JCheckBox();
        pnlEspaco = new JPanel();
        jToolBar2 = new JToolBar();
        chkSwapXY = new JCheckBox();
        chkVariacaoPercentual = new JCheckBox();
        jSeparator1 = new Separator();
        btnExportGraph = new JButton();
        btnAbrir = new JButton();
        scrGrafico = new JScrollPane();
        oChartViewer = new ChartViewer();
        pnlTable = new JPanel();
        pnlItensTabela = new JPanel();
        chkHideVariacao = new JCheckBox();
        chkHideMetaAcumulada = new JCheckBox();
        chkHideRealAcumulado = new JCheckBox();
        jToolBar1 = new JToolBar();
        jSeparator4 = new Separator();
        btnExportTable = new JButton();
        scrlPainel = new JScrollPane();
        dskPaineis = new JBICardDesktopPane();
        pnlRightRecord = new JPanel();
        pnlLeftForm = new JPanel();
        pnlBottomForm = new JPanel();

        setBackground(new Color(255, 255, 255));
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiPainelComp_00001")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_painel_comp.gif"))); // NOI18N
        setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(710, 440));
        addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent evt) {
                formComponentResized(evt);
            }
        });

        pnlRecord.setLayout(new BorderLayout());

        pnlTools.setMinimumSize(new Dimension(480, 30));
        pnlTools.setPreferredSize(new Dimension(10, 25));
        pnlTools.setLayout(new BorderLayout());

        pnlConfirmation.setMaximumSize(new Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new Dimension(150, 27));
        pnlConfirmation.setLayout(new BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new Dimension(150, 32));

        btnSave.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, BorderLayout.EAST);

        pnlOperation.setMaximumSize(new Dimension(700, 27));
        pnlOperation.setMinimumSize(new Dimension(400, 27));
        pnlOperation.setPreferredSize(new Dimension(600, 27));
        pnlOperation.setLayout(new BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setMinimumSize(new Dimension(480, 25));
        tbInsertion.setPreferredSize(new Dimension(700, 25));

        datePane.setDataAlvoVsisible(false);
        datePane.setMaximumSize(new Dimension(290, 30));
        datePane.setMinimumSize(new Dimension(270, 20));
        datePane.setPreferredSize(new Dimension(290, 20));
        tbInsertion.add(datePane);

        spt1.setMaximumSize(new Dimension(20, 20));
        spt1.setMinimumSize(new Dimension(20, 20));
        spt1.setPreferredSize(new Dimension(20, 20));
        tbInsertion.add(spt1);

        btnReload.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        spt2.setMaximumSize(new Dimension(20, 20));
        spt2.setMinimumSize(new Dimension(20, 20));
        spt2.setPreferredSize(new Dimension(20, 20));
        tbInsertion.add(spt2);

        pnlManutencao.setMaximumSize(new Dimension(120, 120));
        pnlManutencao.setLayout(new BorderLayout());

        tlbManutencao.setFloatable(false);
        tlbManutencao.setRollover(true);

        btnEdit.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.setFocusable(false);
        btnEdit.setHorizontalAlignment(SwingConstants.TRAILING);
        btnEdit.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnEdit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tlbManutencao.add(btnEdit);

        btnDelete.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.setFocusable(false);
        btnDelete.setHorizontalAlignment(SwingConstants.TRAILING);
        btnDelete.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tlbManutencao.add(btnDelete);

        pnlManutencao.add(tlbManutencao, BorderLayout.CENTER);

        tbInsertion.add(pnlManutencao);

        spt3.setMaximumSize(new Dimension(20, 20));
        spt3.setMinimumSize(new Dimension(20, 20));
        spt3.setPreferredSize(new Dimension(20, 20));
        tbInsertion.add(spt3);

        btnAjuda.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        ResourceBundle bundle1 = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnAjuda.setText(bundle1.getString("KpiAjuda_00001")); // NOI18N
        btnAjuda.setToolTipText(bundle1.getString("KpiMainPanel_00008")); // NOI18N
        btnAjuda.setFocusable(false);
        btnAjuda.setHorizontalAlignment(SwingConstants.TRAILING);
        btnAjuda.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnAjuda.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda);

        pnlOperation.add(tbInsertion, BorderLayout.LINE_START);

        pnlTools.add(pnlOperation, BorderLayout.WEST);

        pnlRecord.add(pnlTools, BorderLayout.PAGE_START);

        pnlFields.setLayout(new BorderLayout());

        tskDados.setSpecial(true);
        ResourceBundle bundle2 = ResourceBundle.getBundle("international"); // NOI18N
        tskDados.setTitle(bundle2.getString("KpiApresentacao_00017")); // NOI18N

        pnlDados.setMinimumSize(new Dimension(0, 160));
        pnlDados.setOpaque(false);
        pnlDados.setPreferredSize(new Dimension(0, 140));
        pnlDados.setLayout(null);

        lblIndicador.setText(bundle.getString("KpiPainelComp_00005")); // NOI18N
        lblIndicador.setEnabled(false);
        pnlDados.add(lblIndicador);
        lblIndicador.setBounds(440, 80, 85, 20);
        pnlDados.add(cbIndicador);
        cbIndicador.setBounds(440, 100, 400, 22);

        fldNome.setEnabled(false);
        fldNome.setMaxLength(60);
        pnlDados.add(fldNome);
        fldNome.setBounds(10, 20, 400, 22);

        lblNome.setHorizontalAlignment(SwingConstants.LEFT);
        lblNome.setText("Nome:");
        lblNome.setEnabled(false);
        lblNome.setMinimumSize(new Dimension(100, 16));
        lblNome.setPreferredSize(new Dimension(100, 16));
        pnlDados.add(lblNome);
        lblNome.setBounds(10, 0, 190, 20);

        txtDescricao.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        txtDescricao.setFont(new Font("Tahoma", 0, 11));
        txtDescricao.setPreferredSize(new Dimension(260, 56));
        scrDescricao.setViewportView(txtDescricao);

        pnlDados.add(scrDescricao);
        scrDescricao.setBounds(10, 60, 400, 60);

        lblScorecard.setText(KpiStaticReferences.getKpiCustomLabels().getSco(KpiStaticReferences.CAD_OBJETIVO).concat(":"));
        lblScorecard.setEnabled(false);
        pnlDados.add(lblScorecard);
        lblScorecard.setBounds(440, 40, 160, 20);

        cbScorecard.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                cbScorecardItemStateChanged(evt);
            }
        });
        pnlDados.add(cbScorecard);
        cbScorecard.setBounds(440, 60, 400, 22);

        jLabel1.setText(bundle1.getString("KpiIndicador_00010")); // NOI18N
        jLabel1.setEnabled(false);
        pnlDados.add(jLabel1);
        jLabel1.setBounds(440, 0, 110, 20);
        pnlDados.add(cbFrequencia);
        cbFrequencia.setBounds(440, 20, 400, 22);

        tlbAdicionar.setFloatable(false);
        tlbAdicionar.setRollover(true);
        tlbAdicionar.setOpaque(false);
        tlbAdicionar.setPreferredSize(new Dimension(25, 25));

        btnAddIndicador.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_verificar.gif"))); // NOI18N
        btnAddIndicador.setToolTipText(bundle1.getString("KpiPainelComp_00008")); // NOI18N
        btnAddIndicador.setHorizontalAlignment(SwingConstants.LEFT);
        btnAddIndicador.setMaximumSize(new Dimension(100, 27));
        btnAddIndicador.setMinimumSize(new Dimension(100, 27));
        btnAddIndicador.setOpaque(false);
        btnAddIndicador.setPreferredSize(new Dimension(22, 22));
        btnAddIndicador.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAddIndicadorActionPerformed(evt);
            }
        });
        tlbAdicionar.add(btnAddIndicador);

        pnlDados.add(tlbAdicionar);
        tlbAdicionar.setBounds(840, 100, 25, 25);

        lblDescricao1.setHorizontalAlignment(SwingConstants.LEFT);
        lblDescricao1.setText(bundle.getString("KpiPainelComp_00003")); // NOI18N
        lblDescricao1.setEnabled(false);
        lblDescricao1.setMinimumSize(new Dimension(100, 16));
        lblDescricao1.setPreferredSize(new Dimension(100, 16));
        pnlDados.add(lblDescricao1);
        lblDescricao1.setBounds(10, 40, 210, 20);

        tsArvore.setCombo(cbScorecard);
        tsArvore.setRoot(false);
        pnlDados.add(tsArvore);
        tsArvore.setBounds(840, 60, 30, 22);

        chkPublico.setText("P�blico");
        chkPublico.setOpaque(false);
        pnlDados.add(chkPublico);
        chkPublico.setBounds(10, 120, 81, 20);

        tskDados.getContentPane().add(pnlDados);

        tskComparativo.add(tskDados);

        tskPaineis.setScrollOnExpand(true);
        tskPaineis.setTitle(bundle2.getString("KpiPainelComp_00014")); // NOI18N

        pnlPainel.setOpaque(false);
        pnlPainel.setLayout(new BoxLayout(pnlPainel, BoxLayout.Y_AXIS));

        tbComparativo.setTabPlacement(JTabbedPane.BOTTOM);
        tbComparativo.setPreferredSize(new Dimension(138, 450));
        //tbComparativo.setBackground(tskPaineis.getBackground());

        pnlGraph.setBackground(new Color(255, 255, 255));
        pnlGraph.setLayout(new BorderLayout());

        pnlItensGrafico.setOpaque(false);
        pnlItensGrafico.setPreferredSize(new Dimension(0, 22));
        pnlItensGrafico.setLayout(new BoxLayout(pnlItensGrafico, BoxLayout.LINE_AXIS));

        lblTipo.setText(bundle1.getString("KpiGraphIndicador_00062")); // NOI18N
        pnlItensGrafico.add(lblTipo);

        cmdGraphSerieType.setModel(new DefaultComboBoxModel(new String[] { "Linha", "Linha 3D", "Barra", "Barra 3D", "Cilindro 3D" }));
        cmdGraphSerieType.setPreferredSize(new Dimension(100, 22));
        cmdGraphSerieType.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                cmdGraphSerieTypeItemStateChanged(evt);
            }
        });
        pnlItensGrafico.add(cmdGraphSerieType);

        chkShowValues.setText("Mostrar Valores");
        chkShowValues.setOpaque(false);
        chkShowValues.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                chkShowValuesActionPerformed(evt);
            }
        });
        pnlItensGrafico.add(chkShowValues);

        chkAcumulado.setText("Mostrar Acumulado");
        chkAcumulado.setOpaque(false);
        chkAcumulado.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                chkAcumuladoActionPerformed(evt);
            }
        });
        pnlItensGrafico.add(chkAcumulado);

        pnlEspaco.setOpaque(false);
        pnlEspaco.setPreferredSize(new Dimension(500, 22));
        pnlEspaco.setLayout(new BorderLayout());

        jToolBar2.setBackground(new Color(255, 255, 255));
        jToolBar2.setFloatable(false);
        jToolBar2.setRollover(true);
        jToolBar2.setOpaque(false);

        chkSwapXY.setText("Inverter Eixos");
        chkSwapXY.setOpaque(false);
        chkSwapXY.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                chkSwapXYActionPerformed(evt);
            }
        });
        jToolBar2.add(chkSwapXY);

        chkVariacaoPercentual.setText("Varia��o Percentual");
        chkVariacaoPercentual.setFocusable(false);
        chkVariacaoPercentual.setHorizontalTextPosition(SwingConstants.RIGHT);
        chkVariacaoPercentual.setOpaque(false);
        chkVariacaoPercentual.setVerticalTextPosition(SwingConstants.BOTTOM);
        chkVariacaoPercentual.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                chkVariacaoPercentualActionPerformed(evt);
            }
        });
        jToolBar2.add(chkVariacaoPercentual);

        jSeparator1.setMaximumSize(new Dimension(20, 20));
        jSeparator1.setMinimumSize(new Dimension(20, 20));
        jSeparator1.setPreferredSize(new Dimension(20, 20));
        jToolBar2.add(jSeparator1);

        btnExportGraph.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_exportacao.gif"))); // NOI18N
        btnExportGraph.setText(bundle1.getString("KpiGraphIndicador_00034")); // NOI18N
        btnExportGraph.setFocusable(false);
        btnExportGraph.setHorizontalTextPosition(SwingConstants.RIGHT);
        btnExportGraph.setOpaque(false);
        btnExportGraph.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnExportGraph.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnExportGraphActionPerformed(evt);
            }
        });
        jToolBar2.add(btnExportGraph);

        btnAbrir.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_download.gif"))); // NOI18N
        btnAbrir.setText(bundle1.getString("KpiGraphIndicador_00033")); // NOI18N
        btnAbrir.setFocusable(false);
        btnAbrir.setHorizontalTextPosition(SwingConstants.RIGHT);
        btnAbrir.setOpaque(false);
        btnAbrir.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnAbrir.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });
        jToolBar2.add(btnAbrir);

        pnlEspaco.add(jToolBar2, BorderLayout.CENTER);

        pnlItensGrafico.add(pnlEspaco);

        pnlGraph.add(pnlItensGrafico, BorderLayout.PAGE_START);

        scrGrafico.setBackground(new Color(236, 242, 246));
        scrGrafico.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        scrGrafico.setPreferredSize(new Dimension(0, 0));

        oChartViewer.setBackground(new Color(255, 255, 255));
        oChartViewer.setText("");
        oChartViewer.setAutoscrolls(true);
        oChartViewer.setDoubleBuffered(true);
        oChartViewer.setOpaque(true);
        scrGrafico.setViewportView(oChartViewer);

        pnlGraph.add(scrGrafico, BorderLayout.CENTER);

        tbComparativo.addTab(bundle1.getString("KpiIndicador_00074"), new ImageIcon(getClass().getResource("/kpi/imagens/ic_grafico.gif")), pnlGraph); // NOI18N

        pnlTable.setBackground(new Color(255, 255, 255));
        pnlTable.setOpaque(false);
        pnlTable.setLayout(new BorderLayout());

        pnlItensTabela.setBackground(new Color(255, 255, 255));
        pnlItensTabela.setPreferredSize(new Dimension(0, 22));
        pnlItensTabela.setLayout(new BoxLayout(pnlItensTabela, BoxLayout.LINE_AXIS));

        chkHideVariacao.setText(bundle1.getString("KpiPainelComp_00009")); // NOI18N
        chkHideVariacao.setFocusable(false);
        chkHideVariacao.setHorizontalAlignment(SwingConstants.LEFT);
        chkHideVariacao.setHorizontalTextPosition(SwingConstants.RIGHT);
        chkHideVariacao.setOpaque(false);
        chkHideVariacao.setVerticalTextPosition(SwingConstants.BOTTOM);
        chkHideVariacao.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                chkHideVariacaoItemStateChanged(evt);
            }
        });
        pnlItensTabela.add(chkHideVariacao);

        chkHideMetaAcumulada.setText(bundle1.getString("KpiPainelComp_00013")); // NOI18N
        chkHideMetaAcumulada.setFocusable(false);
        chkHideMetaAcumulada.setHorizontalAlignment(SwingConstants.LEFT);
        chkHideMetaAcumulada.setHorizontalTextPosition(SwingConstants.RIGHT);
        chkHideMetaAcumulada.setOpaque(false);
        chkHideMetaAcumulada.setVerticalTextPosition(SwingConstants.BOTTOM);
        chkHideMetaAcumulada.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                chkHideMetaAcumuladaItemStateChanged(evt);
            }
        });
        pnlItensTabela.add(chkHideMetaAcumulada);

        chkHideRealAcumulado.setText(bundle1.getString("KpiPainelComp_00012")); // NOI18N
        chkHideRealAcumulado.setFocusable(false);
        chkHideRealAcumulado.setHorizontalAlignment(SwingConstants.LEFT);
        chkHideRealAcumulado.setHorizontalTextPosition(SwingConstants.RIGHT);
        chkHideRealAcumulado.setOpaque(false);
        chkHideRealAcumulado.setVerticalTextPosition(SwingConstants.BOTTOM);
        chkHideRealAcumulado.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                chkHideRealAcumuladoItemStateChanged(evt);
            }
        });
        pnlItensTabela.add(chkHideRealAcumulado);

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);
        jToolBar1.setOpaque(false);

        jSeparator4.setMaximumSize(new Dimension(20, 20));
        jSeparator4.setMinimumSize(new Dimension(20, 20));
        jSeparator4.setPreferredSize(new Dimension(20, 20));
        jToolBar1.add(jSeparator4);

        btnExportTable.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_exportacao.gif"))); // NOI18N
        btnExportTable.setText(bundle1.getString("KpiGraphIndicador_00034")); // NOI18N
        btnExportTable.setFocusable(false);
        btnExportTable.setHorizontalTextPosition(SwingConstants.RIGHT);
        btnExportTable.setOpaque(false);
        btnExportTable.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnExportTable.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnExportTableActionPerformed(evt);
            }
        });
        jToolBar1.add(btnExportTable);

        pnlItensTabela.add(jToolBar1);

        pnlTable.add(pnlItensTabela, BorderLayout.PAGE_START);

        scrlPainel.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        scrlPainel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrlPainel.setAutoscrolls(true);
        scrlPainel.setMaximumSize(new Dimension(0, 0));
        scrlPainel.setMinimumSize(new Dimension(0, 0));
        scrlPainel.setPreferredSize(new Dimension(0, 0));

        dskPaineis.setBackground(new Color(255, 255, 255));
        dskPaineis.setAutoOrganize(true);
        dskPaineis.setAutoscrolls(true);
        scrlPainel.setViewportView(dskPaineis);

        pnlTable.add(scrlPainel, BorderLayout.CENTER);

        tbComparativo.addTab(bundle1.getString("KpiIndicador_00073"), new ImageIcon(getClass().getResource("/kpi/imagens/ic_planilha.gif")), pnlTable); // NOI18N

        pnlPainel.add(tbComparativo);

        tskPaineis.getContentPane().add(pnlPainel);

        tskComparativo.add(tskPaineis);

        pnlFields.add(tskComparativo, BorderLayout.CENTER);

        pnlRecord.add(pnlFields, BorderLayout.CENTER);

        getContentPane().add(pnlRecord, BorderLayout.CENTER);

        pnlRightRecord.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlRightRecord, BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlLeftForm, BorderLayout.WEST);

        pnlBottomForm.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlBottomForm, BorderLayout.SOUTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnAddIndicadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddIndicadorActionPerformed
            if (cbIndicador.getSelectedIndex() > 0) {

                StringBuilder parametros = new StringBuilder();

                parametros.append("TABLECARD");
                parametros.append("|");
                parametros.append(datePane.getDataDe());
                parametros.append("|");
                parametros.append(datePane.getDataAte());

                kpi.xml.BIXMLRecord tabela = event.loadRecord(
                        event.getComboValue(htbIndicador, cbIndicador),
                        "INDICADOR",
                        parametros.toString());

                if (canBuildTable(tabela)) {
                    painelTemporario.add(tabela);
                    //Monta a tabela.
                    buildTable(tabela, true);
                    //Monta o gr�fico.
                    buildGraph(painelTemporario);
                }

                dskPaineis.autoOrganizeFrames();
                dskPaineis.setSize(dskPaineis.getHeight() + 1, dskPaineis.getWidth());
                bloqueiaFrequencia();
            }
	}//GEN-LAST:event_btnAddIndicadorActionPerformed

    private void btnExportTableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportTableActionPerformed
        StringBuilder parametros = new StringBuilder();
        parametros.append(datePane.getDataDe());
        parametros.append("|");
        parametros.append(datePane.getDataAte());
        parametros.append("|");
        parametros.append(!chkHideVariacao.isSelected());
        parametros.append("|");
        parametros.append("T");
        parametros.append("|");
        parametros.append(!chkHideRealAcumulado.isSelected());
        parametros.append("|");
        parametros.append(!chkHideMetaAcumulada.isSelected());
        event.executeRecord(parametros.toString());
    }//GEN-LAST:event_btnExportTableActionPerformed

    private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
        event.loadHelp(recordType);
    }//GEN-LAST:event_btnAjudaActionPerformed

	private void cbScorecardItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbScorecardItemStateChanged
            StringBuilder parametros = new StringBuilder();

            if (cbScorecard.getSelectedIndex() == 0) {
                cbIndicador.removeAllItems();
            } else {
                parametros.append("ID_SCOREC = '".concat(event.getComboValue(htbScorecard, cbScorecard)).concat("'"));
                parametros.append(" AND ");
                parametros.append("FREQ = '".concat(event.getComboValue(htbFrequencia, cbFrequencia)).concat("'"));

                kpi.xml.BIXMLRecord recIndFiltrados = event.listRecords(
                        "INDICADOR",
                        parametros.toString());

                event.populateCombo(recIndFiltrados.getBIXMLVector("INDICADORES"), "INDICADOR", htbIndicador, cbIndicador);
            }
	}//GEN-LAST:event_cbScorecardItemStateChanged

	private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
            int intElementos = dskPaineis.getComponentCount();

            for (int i = 0; i < intElementos; i++) {
                KpiIndicadorTable tabela = ((KpiIndicadorTable) dskPaineis.getComponent(i));

                tabela.setBounds(0, 0, this.getWidth() - 100, tabela.getHeight());

                tabela.pack();
            }
	}//GEN-LAST:event_formComponentResized

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            StringBuilder parametros = new StringBuilder();

            parametros.append(datePane.getDataDe());
            parametros.append("|");
            parametros.append(datePane.getDataAte());

            setRecord(event.loadRecord(
                    id,
                    recordType,
                    parametros.toString()));

            refreshFields();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();

            for (int i = 0; i < dskPaineis.getComponentCount(); i++) {
                ((KpiIndicadorTable) dskPaineis.getComponent(i)).showFechar(false);
            }

            dskPaineis.autoOrganizeFrames();

            setStatus(KpiDefaultFrameBehavior.UPDATING);
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();

            for (int i = 0; i < dskPaineis.getComponentCount(); i++) {
                ((KpiIndicadorTable) dskPaineis.getComponent(i)).showFechar(true);
            }

            dskPaineis.autoOrganizeFrames();
	}//GEN-LAST:event_btnEditActionPerformed

        private void cmdGraphSerieTypeItemStateChanged(ItemEvent evt) {//GEN-FIRST:event_cmdGraphSerieTypeItemStateChanged
            refleshGraph();
        }//GEN-LAST:event_cmdGraphSerieTypeItemStateChanged

        private void chkHideVariacaoItemStateChanged(ItemEvent evt) {//GEN-FIRST:event_chkHideVariacaoItemStateChanged
            int intElementos = dskPaineis.getComponentCount();

            for (int i = 0; i < intElementos; i++) {
                ((KpiIndicadorTable) dskPaineis.getComponent(i)).showLinha(
                        "VARIACAO",
                        !chkHideVariacao.isSelected());
                ((KpiIndicadorTable) dskPaineis.getComponent(i)).showLinha(
                        "VARIACUM",
                        !chkHideVariacao.isSelected());
            }

            dskPaineis.autoOrganizeFrames();
        }//GEN-LAST:event_chkHideVariacaoItemStateChanged

        private void chkHideMetaAcumuladaItemStateChanged(ItemEvent evt) {//GEN-FIRST:event_chkHideMetaAcumuladaItemStateChanged
            int intElementos = dskPaineis.getComponentCount();

            for (int i = 0; i < intElementos; i++) {
                ((kpi.swing.analisys.KpiIndicadorTable) dskPaineis.getComponent(i)).showLinha("METAACUM", !chkHideMetaAcumulada.isSelected());
            }

            dskPaineis.autoOrganizeFrames();
        }//GEN-LAST:event_chkHideMetaAcumuladaItemStateChanged

        private void chkHideRealAcumuladoItemStateChanged(ItemEvent evt) {//GEN-FIRST:event_chkHideRealAcumuladoItemStateChanged
            int intElementos = dskPaineis.getComponentCount();

            for (int i = 0; i < intElementos; i++) {
                ((KpiIndicadorTable) dskPaineis.getComponent(i)).showLinha(
                        "REALACUM",
                        !chkHideRealAcumulado.isSelected());
            }

            dskPaineis.autoOrganizeFrames();
        }//GEN-LAST:event_chkHideRealAcumuladoItemStateChanged

        private void chkShowValuesActionPerformed(ActionEvent evt) {//GEN-FIRST:event_chkShowValuesActionPerformed
            refleshGraph();
        }//GEN-LAST:event_chkShowValuesActionPerformed

        private void chkSwapXYActionPerformed(ActionEvent evt) {//GEN-FIRST:event_chkSwapXYActionPerformed
            refleshGraph();
        }//GEN-LAST:event_chkSwapXYActionPerformed

        private void btnAbrirActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed
            KpiFileChooser frmChooser = new KpiFileChooser(KpiStaticReferences.getKpiMainPanel(), true);
            frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_OPEN);
            frmChooser.setFileType("*.jpg");
            frmChooser.loadServerFiles("graphs\\*.jpg");
            frmChooser.setVisible(true);
            frmChooser.setUrlPath("/graphs/");
            frmChooser.openFile();
}//GEN-LAST:event_btnAbrirActionPerformed

        private void btnExportGraphActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnExportGraphActionPerformed
            KpiFileChooser frmChooser = new KpiFileChooser(KpiStaticReferences.getKpiMainPanel(), true);
            KpiToolKit tool = new KpiToolKit();

            frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_SAVE);
            frmChooser.setFileType("*.jpg");
            frmChooser.loadServerFiles("graphs\\*.jpg");
            frmChooser.setFileName("grafico_comparativo.jpg");
            frmChooser.setVisible(true);

            if (frmChooser.isValidFile()) {
                BufferedImage imagem_do_grafico = new BufferedImage(
                        oChartViewer.getWidth(),
                        oChartViewer.getHeight(),
                        BufferedImage.TYPE_INT_RGB);

                Graphics2D grafico = imagem_do_grafico.createGraphics();

                grafico.setBackground(Color.WHITE);
                grafico.fillRect(0, 0, oChartViewer.getWidth(), oChartViewer.getHeight());

                oChartViewer.doLayout();
                oChartViewer.paint(grafico);

                tool.writeJPEGFile(imagem_do_grafico, "graphs//" + frmChooser.getFileName());
            }

}//GEN-LAST:event_btnExportGraphActionPerformed

        private void chkVariacaoPercentualActionPerformed(ActionEvent evt) {//GEN-FIRST:event_chkVariacaoPercentualActionPerformed
            refleshGraph();
        }//GEN-LAST:event_chkVariacaoPercentualActionPerformed

        private void chkAcumuladoActionPerformed(ActionEvent evt) {//GEN-FIRST:event_chkAcumuladoActionPerformed
            refleshGraph();
        }//GEN-LAST:event_chkAcumuladoActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnAbrir;
    private JButton btnAddIndicador;
    private JButton btnAjuda;
    private JButton btnCancel;
    private JButton btnDelete;
    private JButton btnEdit;
    private JButton btnExportGraph;
    private JButton btnExportTable;
    private JButton btnReload;
    private JButton btnSave;
    private ButtonGroup buttonGroup1;
    private JComboBox cbFrequencia;
    private JComboBox cbIndicador;
    private JComboBox cbScorecard;
    private JCheckBox chkAcumulado;
    private JCheckBox chkHideMetaAcumulada;
    private JCheckBox chkHideRealAcumulado;
    private JCheckBox chkHideVariacao;
    private JCheckBox chkPublico;
    private JCheckBox chkShowValues;
    private JCheckBox chkSwapXY;
    private JCheckBox chkVariacaoPercentual;
    private JComboBox cmdGraphSerieType;
    private JBIDatePane datePane;
    private JBICardDesktopPane dskPaineis;
    private JPVEdit fldNome;
    private JLabel jLabel1;
    private Separator jSeparator1;
    private Separator jSeparator4;
    private JToolBar jToolBar1;
    private JToolBar jToolBar2;
    private JLabel lblDescricao1;
    private JLabel lblIndicador;
    private JLabel lblNome;
    private JLabel lblScorecard;
    private JLabel lblTipo;
    private ChartViewer oChartViewer;
    private JPanel pnlBottomForm;
    private JPanel pnlConfirmation;
    private JPanel pnlDados;
    private JPanel pnlEspaco;
    private JPanel pnlFields;
    private JPanel pnlGraph;
    private JPanel pnlItensGrafico;
    private JPanel pnlItensTabela;
    private JPanel pnlLeftForm;
    private JPanel pnlManutencao;
    private JPanel pnlOperation;
    private JPanel pnlPainel;
    private JPanel pnlRecord;
    private JPanel pnlRightRecord;
    private JPanel pnlTable;
    private JPanel pnlTools;
    private JScrollPane scrDescricao;
    private JScrollPane scrGrafico;
    private JScrollPane scrlPainel;
    private Separator spt1;
    private Separator spt2;
    private Separator spt3;
    private JTabbedPane tbComparativo;
    private JToolBar tbConfirmation;
    private JToolBar tbInsertion;
    private JToolBar tlbAdicionar;
    private JToolBar tlbManutencao;
    private JBITreeSelection tsArvore;
    private JTaskPane tskComparativo;
    private JTaskPaneGroup tskDados;
    private JTaskPaneGroup tskPaineis;
    private JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        btnSave.setEnabled(true);
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);

    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        final StringBuilder parametros = new StringBuilder();

        parametros.append(new KpiDateUtil().getCalendarString(dateBase.getDataAcumuladoDe()));
        parametros.append("|");
        parametros.append(new KpiDateUtil().getCalendarString(dateBase.getDataAcumuladoAte()));

        final SwingWorker worker = new SwingWorker() {

            @Override
            protected Object doInBackground() throws Exception {
                return event.loadRecord(id, recordType, parametros.toString());
            }

            @Override
            protected void done() {
                try {
                    setRecord((kpi.xml.BIXMLRecord) get());
                } catch (Exception ex) {
                }
                refreshFields();
            }
        };
        worker.execute();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return null;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public void afterExecute() {
        if (dataController.getStatus() == KpiDataController.KPI_ST_OK) {

            int iOption = this.dialog.confirmMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00013"));

            if (iOption == javax.swing.JOptionPane.YES_OPTION) {
                //Requisita a URL para download do arquivo xls.
                KpiToolKit oToolKit = new KpiToolKit();
                oToolKit.browser("sgidownload.apw?file=sgiExport\\planilha_paineis_comparativos.xls");

            }
        }
    }
}
