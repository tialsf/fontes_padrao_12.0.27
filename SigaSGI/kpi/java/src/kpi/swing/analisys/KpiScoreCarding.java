package kpi.swing.analisys;

/**
 * @author Alexandre Alves da Silva
 */
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Timer;
import java.util.*;
import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import kpi.beans.JBICardDesktopAction;
import kpi.beans.JBIXMLTable;
import kpi.core.IteMnuPermission.Permissao;
import kpi.core.IteMnuPermission.TipoPermissao;
import kpi.core.*;
import kpi.swing.KpiDefaultDialogSystem;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiTreeNode;
import kpi.swing.analisys.map.IKpiMapaMainForm;
import kpi.swing.analisys.widget.KpiWidget;
import kpi.swing.analisys.widget.KpiWidgetFactory;
import kpi.swing.framework.KpiListaNota;
import kpi.swing.framework.KpiListaPlanos;
import kpi.swing.framework.scorecardPanels.BscEntityType;
import kpi.util.KpiDateUtil;
import kpi.util.KpiToolKit;
import kpi.xml.BIXMLAttributes;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;
import pv.jfcx.JPVDatePlus;
import pv.jfcx.JPVEdit;
import pv.jfcx.JPVTable;

public final class KpiScoreCarding extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions, JBICardDesktopAction {

    public final static int COL_INDICADOR = 03;
    public final static int COL_ACAO = 04;
    public final static int COL_NOTA = 05;
    //
    public final static int STATUS_BLUE = 75;
    public final static int STATUS_GREEN = 11;
    public final static int STATUS_YELLOW = 13;
    public final static int STATUS_RED = 12;
    public final static int ESTAVEL_GRAY = 25;
    private final static int BTN_AVANCAR = 0;
    private final static int BTN_VOLTAR = 1;
    private final static int CACHE_MAXSIZE = 10;
    private static boolean isDrillEnable = false;
    //
    private kpi.util.KpiDateUtil date_Util = new kpi.util.KpiDateUtil();
    private String recordType = "SCORECARDING"; //NOI18N
    private String formType = recordType;
    private String parentType = recordType;
    private Reminder scheduler = new Reminder();
    private String lastScoreID = "0"; //NOI18N
    private String userLogged = ""; //NOI18N
    private int ctrHistorico = 0;
    private int nColFreeze = 0;
    private int ordemSCD;
    private boolean clearHist = false;
    private boolean treeExpanded = false;
    private boolean isUserAdmin = false;
    private Vector vctHistVol = new Vector();
    private Vector indexChachedItens = new Vector();
    private BIXMLRecord recPlanilha = null;
    private HashMap cacheInd = new java.util.HashMap();
    private java.util.Hashtable htbPacote = new java.util.Hashtable();
    private kpi.swing.kpiCreateTree buildTree = new kpi.swing.kpiCreateTree();
    private KpiScoreCardingUserConfig userConfig = new KpiScoreCardingUserConfig();
    private KpiScoreCardingTableRenderer cellRenderer;
    private KpiScoreCardingTableImageRenderer cellImageRenderer;
    private pv.jfcx.JPVTable tableData;
    private kpi.core.KpiImageResources imageResources;
    private kpi.core.KpiFormController formController = kpi.core.KpiStaticReferences.getKpiFormController();
    private KpiDefaultDialogSystem dialogSystem = kpi.core.KpiStaticReferences.getKpiDialogSystem();

    @Override
    public void enableFields() {
    }

    @Override
    public void disableFields() {
	//Habilita o auto-reflesh. [Na inicializa��o do scorecarding]
	if (record.getInt("SCRDING_INTERVAL_UPD") == 0) { //NOI18N
	    tglBtnAutoUpt.setSelected(false);
	    tglBtnAutoUpt.setEnabled(false);
	} else {
	    tglBtnAutoUpt.setEnabled(true);

	    if (record.getBoolean("SCOAUTOREFRESH")) { //NOI18N

		if (scheduler.getTimer() != null) {
		    scheduler.getTimer().cancel();
		}

		tglBtnAutoUpt.setSelected(true);

		tglBtnAutoUpt.setToolTipText(
			tglBtnAutoUpt.getToolTipText().concat(" Intervalo: ").concat(record.getString("SCRDING_INTERVAL_UPD").concat(" min.")));

		scheduler.startScorecardingUpdate(record.getInt("SCRDING_INTERVAL_UPD"));
	    }
	}
    }

    //Atualiza os objetos na tela.
    @Override
    public void refreshFields() {
	//Limpar a linha selecionada.
	plaScorecard.getJTable().clearSelection();
	
	//Recupera as posi��es gravadas anteriormente.
	userConfig.setAllWidth(record.getString("SCOWIDTHCOL"));
	//Numero de colunas a ser congelada.
	nColFreeze = record.getInt("NCOLFREEZE");
	//Verifica se deve ocultar o painel dos departamentos.
	if (record.getBoolean("SCOHIDETREE")) {
	    splitScoreCarding.setDividerLocation(JSplitPane.LEFT_ALIGNMENT);
	}

	cellRenderer = new KpiScoreCardingTableRenderer(plaScorecard, record);
	cellImageRenderer = new KpiScoreCardingTableImageRenderer(plaScorecard, record);

	//Monta a arvore de scorecards.
	BIXMLRecord recTree = record.getBIXMLVector("ARVORES").get(0);
	treeScoreCarding.setModel(new javax.swing.tree.DefaultTreeModel(buildTree.createTree(recTree)));
	//Inicializa��o da �rvore.
	imageResources = kpi.core.KpiStaticReferences.getKpiImageResources();

	if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
	    treeScoreCarding.setCellRenderer(new BscScoreCardingTreeCellRenderer(imageResources));
	    isDrillEnable = true;

	    KpiMenuController mnuControl = KpiStaticReferences.getMnuController();
	    IteMnuPermission perm = mnuControl.getRuleByName("MAPAESTRATEGICO");

	    if (perm.getPermissionValue(TipoPermissao.VISUALIZACAO) == Permissao.PERMITIDO ||
		    perm.getPermissionValue(TipoPermissao.MANUTENCAO) == Permissao.PERMITIDO){

		sepMapEst.setVisible(true);
		btnMapEst.setVisible(true);
	    } else {
		sepMapEst.setVisible(false);
		btnMapEst.setVisible(false);
	    }

	} else {
	    treeScoreCarding.setCellRenderer(new KpiScoreCardingTreeCellRenderer(imageResources));
	    isDrillEnable = false;

	    sepMapEst.setVisible(false);
	    btnMapEst.setVisible(false);
	}
	cellRenderer.setDrillEnabled(isDrillEnable);
	if (isDrillEnable) {
	    btnAvancar.setVisible(false);
	    btnVoltar.setVisible(false);
	    jSeparator7.setVisible(false);
	}

	treeScoreCarding.setSelectionRow(0);
	reloadPlanilha();
	//Registro do hist�rico.
	newHistorico("0");
	userLogged = record.getString("USER");
	isUserAdmin = record.getBoolean("ISADMIN"); //NOI18N
	ordemSCD = record.getInt("ORDEM_SCD"); //NOI18N
	//Congelar colunas sem valores.
	if (record.getBoolean("SCOFREEZECOL")) { //NOI18N
	    jRadioButtonMenuItem1.setSelected(true);
	} else {
	    jRadioButtonMenuItem2.setSelected(true);
	}

	BIXMLVector vctPacotes = record.getBIXMLVector("PACOTEXUSERS"); //NOI18N

	if (vctPacotes.size() > 0) {
	    cboPacotes.setVisible(true);
	    //Popula a lista de pacotes.
	    event.populateCombo(vctPacotes, "PACOTES", htbPacote, cboPacotes); //NOI18N
	    //Posiciona no �ltimo pacote selecionado.
	    event.selectComboItem(cboPacotes, event.getComboItem(vctPacotes, record.getString("PACOTE"), true));
	} else {
	    cboPacotes.setVisible(false);
	}

	//Visualiza��o de f�rmulas de indicadores.
	if ("T".equals(record.getString("VISUALFORMULAS"))) { //NOI18N
	    btnIndicFormula.setEnabled(true);
	    btnIndicFormula.setVisible(true);
	} else {
	    btnIndicFormula.setVisible(false);
	}

	//Visualiza��o de Espinha de Peixe.
	if ("T".equals(record.getString("VISUALESPINHAPEIXE"))) { //NOI18N
	    cmdCausaEfeito.setEnabled(true);
	    cmdCausaEfeito.setVisible(true);
	} else {
	    cmdCausaEfeito.setVisible(false);
	}

	//Posicionamento do scorecarding configurado pelo usu�rio.
	if (record.getBoolean("SCODEFAULT")) { //NOI18N
	    String scorecards = record.getString("TREEPATH");

	    //Percorre a �rvore e posiciona no �ltimo n� selecionado.
	    if (buildTree.expandPath(treeScoreCarding, scorecards)) {
		reloadPlanilha();
	    }
	}

	//Percorre a �rvore e posiciona no �ltimo n� selecionado.
	if (buildTree.expandPath(treeScoreCarding, buildTree.getSelectedTreePath())) {
	    reloadPlanilha();
	}
	splitScoreCarding.setDividerLocation(200);
    }

    /**
     * Atualiza o valor dos objetos para grava��o no xml
     * @return BIXMLRecord
     */
    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
	kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);

	return recordAux;
    }

    /**
     * Faz a valida��o dos campos.
     * @return Retorna se a valida��o est� ok ou n�o.
     * @param errorMessage Mensagem de erro.
     */
    @Override
    public boolean validateFields(StringBuffer errorMessage) {
	return true;
    }

    public void refreshClientImage() {
	String url;

	try {
	    pnlClientImage.removeAll();
	    javax.swing.JLabel clientImg = new javax.swing.JLabel();
	    clientImg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
	    clientImg.addMouseListener(new java.awt.event.MouseAdapter() {

		@Override
		public void mouseClicked(java.awt.event.MouseEvent evt) {
		    if (evt.getButton() == MouseEvent.BUTTON3) {
			//Permite altera��o de logo apenas para os usu�rios administradores.
			jMenuItemChange.setVisible(isUserAdmin);
			jPopupMenuClientImg.show(pnlClientImage, evt.getX(), evt.getY() + 5);
		    }
		}
	    });

	    if (KpiStaticReferences.getKpiApplet() != null) {
		url = KpiStaticReferences.getKpiApplet().getCodeBase().toString().concat("imagens/"); //NOI18N
	    } else {
		url = KpiStaticReferences.getKpiApplication().getCodeBase().concat("imagens/"); //NOI18N
	    }

	    Random r = new Random(System.nanoTime());
	    String strInt = String.valueOf(r.nextInt());
	    javax.swing.ImageIcon imgClient = new javax.swing.ImageIcon(new java.net.URL(url.concat("art_logo_clie.sgi?ForceReload=".concat(strInt)))); //NOI18N
	    clientImg.setIcon(imgClient);

	    pnlClientImage.add(clientImg, java.awt.BorderLayout.CENTER);
	    pnlClientImage.repaint();
	    clientImg.repaint();
	    clientImg.updateUI();
	} catch (Exception e) {
	}
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jRadioButtonMenuItem1 = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItem2 = new javax.swing.JRadioButtonMenuItem();
        jSeparator3 = new javax.swing.JSeparator();
        jMnuRestore = new javax.swing.JMenuItem();
        jMnuSaveConfig = new javax.swing.JMenuItem();
        jPopupMenuClientImg = new javax.swing.JPopupMenu();
        jMenuItemChange = new javax.swing.JMenuItem();
        jMenuItemHide = new javax.swing.JMenuItem();
        jMenuItemRefresh = new javax.swing.JMenuItem();
        grpMnu = new javax.swing.ButtonGroup();
        grpViewType = new javax.swing.ButtonGroup();
        pnlData = new javax.swing.JPanel();
        splitScoreCarding = new javax.swing.JSplitPane();
        pnlScoArea = new javax.swing.JPanel();
        pnlViewData = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        pnlViewCard = new javax.swing.JPanel();
        plaScorecard = new kpi.beans.JBIXMLTable();
        jPanel1 = new javax.swing.JPanel();
        scrlPainel = new javax.swing.JScrollPane();
        dskPaineis = new kpi.beans.JBICardDesktopPane();
        pnlCima = new javax.swing.JPanel();
        splitImg = new javax.swing.JSplitPane();
        pnlClientImage = new javax.swing.JPanel();
        pnlTree = new javax.swing.JPanel();
        cboPacotes = new javax.swing.JComboBox();
        scrTree = new javax.swing.JScrollPane();
        treeScoreCarding = new javax.swing.JTree();
        treeScoreCarding.removeAll();
        treeScoreCarding.setModel(	new DefaultTreeModel(
            new DefaultMutableTreeNode(
                new String(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00002")) ) ) );
    toolBar = new javax.swing.JToolBar();
    datePane = new kpi.beans.JBIDatePane(this);
    jSeparator2 = new javax.swing.JToolBar.Separator();
    btnReload = new javax.swing.JButton();
    jSeparator4 = new javax.swing.JToolBar.Separator();
    btnVoltar = new javax.swing.JButton();
    btnAvancar = new javax.swing.JButton();
    jSeparator7 = new javax.swing.JToolBar.Separator();
    btnVisualizar = new javax.swing.JButton();
    btnGrafico = new javax.swing.JButton();
    btnIndicFormula = new javax.swing.JButton();
    btnDwConsulta = new javax.swing.JButton();
    btnLink = new javax.swing.JButton();
    btnAnalitico = new javax.swing.JButton();
    cmdCausaEfeito = new javax.swing.JButton();
    sepMapEst = new javax.swing.JToolBar.Separator();
    btnMapEst = new javax.swing.JButton();
    jSeparator6 = new javax.swing.JToolBar.Separator();
    tbTabela = new javax.swing.JToggleButton();
    tbWidget = new javax.swing.JToggleButton();
    jSeparator5 = new javax.swing.JToolBar.Separator();
    tglBtnAutoUpt = new javax.swing.JToggleButton();

    jPopupMenu1.setLightWeightPopupEnabled(false);

    grpMnu.add(jRadioButtonMenuItem1);
    java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
    jRadioButtonMenuItem1.setText(bundle.getString("KpiScoreCarding_00034")); // NOI18N
    jRadioButtonMenuItem1.addItemListener(new java.awt.event.ItemListener() {
        public void itemStateChanged(java.awt.event.ItemEvent evt) {
            jRadioButtonMenuItem1ItemStateChanged(evt);
        }
    });
    jPopupMenu1.add(jRadioButtonMenuItem1);

    grpMnu.add(jRadioButtonMenuItem2);
    jRadioButtonMenuItem2.setText(bundle.getString("KpiScoreCarding_00035")); // NOI18N
    jPopupMenu1.add(jRadioButtonMenuItem2);
    jPopupMenu1.add(jSeparator3);

    jMnuRestore.setText(bundle.getString("KpiScoreCarding_00056")); // NOI18N
    jMnuRestore.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jMnuRestoreActionPerformed(evt);
        }
    });
    jPopupMenu1.add(jMnuRestore);

    jMnuSaveConfig.setText(bundle.getString("KpiScoreCarding_00057")); // NOI18N
    jMnuSaveConfig.setToolTipText(bundle.getString("KpiScoreCarding_00058")); // NOI18N
    jMnuSaveConfig.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jMnuSaveConfigActionPerformed(evt);
        }
    });
    jPopupMenu1.add(jMnuSaveConfig);

    jMenuItemChange.setText("Alterar imagem");
    jMenuItemChange.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jMenuItemChangeActionPerformed(evt);
        }
    });
    jPopupMenuClientImg.add(jMenuItemChange);

    jMenuItemHide.setText("Ocultar");
    jMenuItemHide.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jMenuItemHideActionPerformed(evt);
        }
    });
    jPopupMenuClientImg.add(jMenuItemHide);

    jMenuItemRefresh.setText("Atualizar ");
    jMenuItemRefresh.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jMenuItemRefreshActionPerformed(evt);
        }
    });
    jPopupMenuClientImg.add(jMenuItemRefresh);

    setClosable(true);
    setIconifiable(true);
    setMaximizable(true);
    setResizable(true);
    setTitle(bundle.getString("KpiScoreCarding_00001")); // NOI18N
    setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_scorecarding.gif"))); // NOI18N
    setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
    setPreferredSize(new java.awt.Dimension(600, 358));
    addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
        public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
        }
        public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
        }
        public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            formInternalFrameClosing(evt);
        }
        public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
        }
        public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
        }
        public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
        }
        public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
        }
    });

    pnlData.setLayout(new java.awt.BorderLayout());

    splitScoreCarding.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
    splitScoreCarding.setDividerLocation(200);
    splitScoreCarding.setOneTouchExpandable(true);

    pnlScoArea.setLayout(new java.awt.BorderLayout());

    pnlViewData.setPreferredSize(new java.awt.Dimension(1000, 200));
    pnlViewData.setLayout(new java.awt.BorderLayout());

    jToolBar1.setFloatable(false);
    jToolBar1.setRollover(true);
    pnlViewData.add(jToolBar1, java.awt.BorderLayout.PAGE_END);

    pnlViewCard.setLayout(new java.awt.CardLayout());
    pnlViewCard.add(plaScorecard, "table");

    jPanel1.setLayout(new java.awt.BorderLayout());

    scrlPainel.setAutoscrolls(true);

    dskPaineis.setBackground(new java.awt.Color(236, 242, 246));
    dskPaineis.addComponentListener(new java.awt.event.ComponentAdapter() {
        public void componentResized(java.awt.event.ComponentEvent evt) {
            dskPaineisComponentResized(evt);
        }
    });
    scrlPainel.setViewportView(dskPaineis);

    jPanel1.add(scrlPainel, java.awt.BorderLayout.CENTER);

    pnlViewCard.add(jPanel1, "widget");

    pnlViewData.add(pnlViewCard, java.awt.BorderLayout.CENTER);

    pnlScoArea.add(pnlViewData, java.awt.BorderLayout.CENTER);

    splitScoreCarding.setRightComponent(pnlScoArea);

    pnlCima.setMinimumSize(new java.awt.Dimension(200, 200));
    pnlCima.setPreferredSize(new java.awt.Dimension(200, 300));
    pnlCima.setLayout(new java.awt.BorderLayout());

    splitImg.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

    pnlClientImage.setBackground(new java.awt.Color(255, 255, 255));
    pnlClientImage.setForeground(new java.awt.Color(255, 255, 255));
    pnlClientImage.setAlignmentX(0.0F);
    pnlClientImage.setAlignmentY(0.0F);
    pnlClientImage.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    pnlClientImage.setEnabled(false);
    pnlClientImage.setFocusable(false);
    pnlClientImage.setPreferredSize(new java.awt.Dimension(154, 136));
    pnlClientImage.setRequestFocusEnabled(false);
    pnlClientImage.setLayout(new java.awt.BorderLayout());
    splitImg.setLeftComponent(pnlClientImage);

    pnlTree.setLayout(new java.awt.BorderLayout());

    cboPacotes.setToolTipText(bundle.getString("KpiScoreCarding_00036")); // NOI18N
    cboPacotes.addItemListener(new java.awt.event.ItemListener() {
        public void itemStateChanged(java.awt.event.ItemEvent evt) {
            cboPacotesItemStateChanged(evt);
        }
    });
    pnlTree.add(cboPacotes, java.awt.BorderLayout.NORTH);

    scrTree.setBorder(null);
    scrTree.setPreferredSize(new java.awt.Dimension(75, 300));

    javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("Aguarde..");
    treeScoreCarding.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
    treeScoreCarding.setRowHeight(18);
    treeScoreCarding.addTreeWillExpandListener(new javax.swing.event.TreeWillExpandListener() {
        public void treeWillCollapse(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {
            treeScoreCardingTreeWillCollapse(evt);
        }
        public void treeWillExpand(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {
            treeScoreCardingTreeWillExpand(evt);
        }
    });
    treeScoreCarding.addMouseListener(new java.awt.event.MouseAdapter() {
        public void mouseClicked(java.awt.event.MouseEvent evt) {
            treeScoreCardingMouseClicked(evt);
        }
    });
    scrTree.setViewportView(treeScoreCarding);

    pnlTree.add(scrTree, java.awt.BorderLayout.CENTER);

    splitImg.setRightComponent(pnlTree);

    pnlCima.add(splitImg, java.awt.BorderLayout.CENTER);

    splitScoreCarding.setLeftComponent(pnlCima);

    pnlData.add(splitScoreCarding, java.awt.BorderLayout.CENTER);

    getContentPane().add(pnlData, java.awt.BorderLayout.CENTER);

    toolBar.setFloatable(false);
    toolBar.setRollover(true);
    toolBar.setMaximumSize(new java.awt.Dimension(65734, 25));
    toolBar.setPreferredSize(new java.awt.Dimension(0, 25));

    datePane.setDataAlvoVsisible(true);
    datePane.setMaximumSize(new java.awt.Dimension(420, 30));
    toolBar.add(datePane);

    jSeparator2.setEnabled(false);
    jSeparator2.setMaximumSize(new java.awt.Dimension(15, 15));
    jSeparator2.setPreferredSize(new java.awt.Dimension(15, 15));
    toolBar.add(jSeparator2);

    btnReload.setFont(new java.awt.Font("Dialog", 0, 12));
    btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
    btnReload.setToolTipText(bundle.getString("KpiScoreCarding_00023")); // NOI18N
    btnReload.setMaximumSize(new java.awt.Dimension(25, 25));
    btnReload.setMinimumSize(new java.awt.Dimension(25, 25));
    btnReload.setPreferredSize(new java.awt.Dimension(25, 25));
    btnReload.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnReloadActionPerformed(evt);
        }
    });
    toolBar.add(btnReload);

    jSeparator4.setMaximumSize(new java.awt.Dimension(15, 15));
    jSeparator4.setPreferredSize(new java.awt.Dimension(15, 15));
    toolBar.add(jSeparator4);

    btnVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_botaovoltar.gif"))); // NOI18N
    btnVoltar.setToolTipText(bundle.getString("KpiScoreCarding_00003")); // NOI18N
    btnVoltar.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
    btnVoltar.addMouseListener(new java.awt.event.MouseAdapter() {
        public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnVoltarMouseClicked(evt);
        }
    });
    toolBar.add(btnVoltar);

    btnAvancar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_botaoavancar.gif"))); // NOI18N
    btnAvancar.setToolTipText(bundle.getString("KpiScoreCarding_00004")); // NOI18N
    btnAvancar.setEnabled(false);
    btnAvancar.addMouseListener(new java.awt.event.MouseAdapter() {
        public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnAvancarMouseClicked(evt);
        }
    });
    toolBar.add(btnAvancar);

    jSeparator7.setMaximumSize(new java.awt.Dimension(15, 15));
    jSeparator7.setPreferredSize(new java.awt.Dimension(15, 15));
    toolBar.add(jSeparator7);

    btnVisualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_visualizar.gif"))); // NOI18N
    btnVisualizar.setToolTipText(bundle.getString("KpiScoreCarding_00022")); // NOI18N
    btnVisualizar.setMaximumSize(new java.awt.Dimension(25, 25));
    btnVisualizar.setMinimumSize(new java.awt.Dimension(25, 25));
    btnVisualizar.setPreferredSize(new java.awt.Dimension(25, 25));
    btnVisualizar.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnVisualizarActionPerformed(evt);
        }
    });
    toolBar.add(btnVisualizar);

    btnGrafico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_grafico.gif"))); // NOI18N
    btnGrafico.setToolTipText(bundle.getString("KpiScoreCarding_00021")); // NOI18N
    btnGrafico.setMaximumSize(new java.awt.Dimension(25, 25));
    btnGrafico.setMinimumSize(new java.awt.Dimension(25, 25));
    btnGrafico.setPreferredSize(new java.awt.Dimension(25, 25));
    btnGrafico.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnGraficoActionPerformed(evt);
        }
    });
    toolBar.add(btnGrafico);

    btnIndicFormula.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_indicadorformula.gif"))); // NOI18N
    java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international"); // NOI18N
    btnIndicFormula.setToolTipText(bundle1.getString("KpiScorecard_00012")); // NOI18N
    btnIndicFormula.setEnabled(false);
    btnIndicFormula.setFocusable(false);
    btnIndicFormula.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    btnIndicFormula.setMaximumSize(new java.awt.Dimension(25, 25));
    btnIndicFormula.setMinimumSize(new java.awt.Dimension(25, 25));
    btnIndicFormula.setPreferredSize(new java.awt.Dimension(25, 25));
    btnIndicFormula.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnIndicFormulaActionPerformed(evt);
        }
    });
    toolBar.add(btnIndicFormula);

    btnDwConsulta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_dw.gif"))); // NOI18N
    btnDwConsulta.setToolTipText(bundle1.getString("KpiScoreCarding_00037")); // NOI18N
    btnDwConsulta.setInheritsPopupMenu(true);
    btnDwConsulta.setMaximumSize(new java.awt.Dimension(25, 25));
    btnDwConsulta.setMinimumSize(new java.awt.Dimension(25, 25));
    btnDwConsulta.setPreferredSize(new java.awt.Dimension(25, 25));
    btnDwConsulta.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnDwConsultaActionPerformed(evt);
        }
    });
    toolBar.add(btnDwConsulta);

    btnLink.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_comp_mes_mes.gif"))); // NOI18N
    btnLink.setToolTipText(bundle1.getString("KpiScoreCarding_00059")); // NOI18N
    btnLink.setEnabled(false);
    btnLink.setFocusable(false);
    btnLink.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    btnLink.setMaximumSize(new java.awt.Dimension(25, 25));
    btnLink.setMinimumSize(new java.awt.Dimension(25, 25));
    btnLink.setPreferredSize(new java.awt.Dimension(25, 25));
    btnLink.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnLinkActionPerformed(evt);
        }
    });
    toolBar.add(btnLink);

    btnAnalitico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_relatorio.gif"))); // NOI18N
    btnAnalitico.setToolTipText("Relat�rio Anal�tico");
    btnAnalitico.setEnabled(false);
    btnAnalitico.setFocusable(false);
    btnAnalitico.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    btnAnalitico.setMaximumSize(new java.awt.Dimension(25, 25));
    btnAnalitico.setMinimumSize(new java.awt.Dimension(25, 25));
    btnAnalitico.setOpaque(false);
    btnAnalitico.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    btnAnalitico.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnAnaliticoActionPerformed(evt);
        }
    });
    toolBar.add(btnAnalitico);

    cmdCausaEfeito.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_epinhapeixe.gif"))); // NOI18N
    cmdCausaEfeito.setToolTipText(bundle.getString("KpiEspinhaPeixeCadastro_00001")); // NOI18N
    cmdCausaEfeito.setFocusable(false);
    cmdCausaEfeito.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    cmdCausaEfeito.setMaximumSize(new java.awt.Dimension(25, 25));
    cmdCausaEfeito.setMinimumSize(new java.awt.Dimension(25, 25));
    cmdCausaEfeito.setPreferredSize(new java.awt.Dimension(25, 25));
    cmdCausaEfeito.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    cmdCausaEfeito.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            cmdCausaEfeitoActionPerformed(evt);
        }
    });
    toolBar.add(cmdCausaEfeito);

    sepMapEst.setMaximumSize(new java.awt.Dimension(15, 15));
    sepMapEst.setPreferredSize(new java.awt.Dimension(15, 15));
    toolBar.add(sepMapEst);

    btnMapEst.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_mapa.gif"))); // NOI18N
    btnMapEst.setToolTipText("Mapa Estrat�gico");
    btnMapEst.setFocusable(false);
    btnMapEst.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    btnMapEst.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    btnMapEst.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnMapEstActionPerformed(evt);
        }
    });
    toolBar.add(btnMapEst);

    jSeparator6.setMaximumSize(new java.awt.Dimension(15, 15));
    jSeparator6.setPreferredSize(new java.awt.Dimension(15, 15));
    toolBar.add(jSeparator6);

    grpViewType.add(tbTabela);
    tbTabela.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_planilha.gif"))); // NOI18N
    tbTabela.setSelected(true);
    tbTabela.setToolTipText("Tabela");
    tbTabela.setFocusable(false);
    tbTabela.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
    tbTabela.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    tbTabela.addItemListener(new java.awt.event.ItemListener() {
        public void itemStateChanged(java.awt.event.ItemEvent evt) {
            tbTabelaItemStateChanged(evt);
        }
    });
    toolBar.add(tbTabela);

    grpViewType.add(tbWidget);
    tbWidget.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_painel.gif"))); // NOI18N
    tbWidget.setToolTipText("Pain�is");
    tbWidget.setFocusable(false);
    tbWidget.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
    tbWidget.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    toolBar.add(tbWidget);

    jSeparator5.setMaximumSize(new java.awt.Dimension(15, 15));
    jSeparator5.setPreferredSize(new java.awt.Dimension(15, 15));
    toolBar.add(jSeparator5);

    tglBtnAutoUpt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_auto_atualizar.gif"))); // NOI18N
    java.util.ResourceBundle bundle2 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
    tglBtnAutoUpt.setToolTipText(bundle2.getString("KpiScoreCarding_00040")); // NOI18N
    tglBtnAutoUpt.setMaximumSize(new java.awt.Dimension(25, 25));
    tglBtnAutoUpt.setMinimumSize(new java.awt.Dimension(25, 25));
    tglBtnAutoUpt.setPreferredSize(new java.awt.Dimension(25, 25));
    tglBtnAutoUpt.addItemListener(new java.awt.event.ItemListener() {
        public void itemStateChanged(java.awt.event.ItemEvent evt) {
            tglBtnAutoUptItemStateChanged(evt);
        }
    });
    toolBar.add(tglBtnAutoUpt);

    getContentPane().add(toolBar, java.awt.BorderLayout.PAGE_START);

    pack();
    }// </editor-fold>//GEN-END:initComponents
	private void jMnuRestoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuRestoreActionPerformed
	for (int col = 0; col < plaScorecard.getTable().getColumnCount() - 1; col++) {
	    plaScorecard.getColumn(col).setPreferredWidth(userConfig.getWidth(col));
	}
	jRadioButtonMenuItem1.setSelected(true);
	plaScorecard.getTable().setLockedColumns(nColFreeze);
	}//GEN-LAST:event_jMnuRestoreActionPerformed

	private void jMnuSaveConfigActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuSaveConfigActionPerformed
	    StringBuilder sCongif = new StringBuilder();
	    //Gravamos as configura��es da largura da coluna
	    for (int col = 0; col < plaScorecard.getTable().getColumnCount(); col++) {
		userConfig.setWidth(col, plaScorecard.getColumn(col).getWidth());
		if (col > 0) {
		    sCongif.append("|");
		}
		sCongif.append(plaScorecard.getColumn(col).getWidth());
	    }

	    //Gravamos a propriedade para congelar as colunas sem valores
	    if (jRadioButtonMenuItem1.isSelected()) {
		sCongif.append(";T");
	    } else {
		sCongif.append(";F");
	    }

	    event.executeRecord(sCongif.toString());

	}//GEN-LAST:event_jMnuSaveConfigActionPerformed

	private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
	    if (scheduler.getTimer() != null) {
		scheduler.getTimer().cancel();
	    }
	}//GEN-LAST:event_formInternalFrameClosing

	private void tglBtnAutoUptItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_tglBtnAutoUptItemStateChanged
	    if (tglBtnAutoUpt.isSelected()) {
		if (scheduler.getTimer() != null) {
		    scheduler.getTimer().cancel();
		}
		scheduler.startScorecardingUpdate(record.getInt("SCRDING_INTERVAL_UPD"));
	    }
	}//GEN-LAST:event_tglBtnAutoUptItemStateChanged

	private void btnDwConsultaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDwConsultaActionPerformed
	    if (tbTabela.isSelected()) {
		int lin = plaScorecard.getSelectedRow();
		if (lin > -1) {
		    BIXMLRecord recLinha = plaScorecard.getXMLData(false).get(lin);
		    kpi.swing.analisys.KpiConsultaDW consultaDW = new kpi.swing.analisys.KpiConsultaDW(this, true);
		    if (consultaDW.loadDWConsultas(recLinha.getString("ID"))) {
			consultaDW.setVisible(true);
		    } else {
			dialogSystem.warningMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00038"));
		    }
		}
	    } else {
		if (dskPaineis.getVctFrames().size() > 0) {
		    KpiWidget wid = (KpiWidget) dskPaineis.getSelectedFrame();
		    kpi.swing.analisys.KpiConsultaDW consultaDW = new kpi.swing.analisys.KpiConsultaDW(this, true);
		    if (consultaDW.loadDWConsultas(wid.getIndBean().getIDInd())) {
			consultaDW.setVisible(true);
		    } else {
			dialogSystem.warningMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00038"));
		    }
		}
	    }
	}//GEN-LAST:event_btnDwConsultaActionPerformed

	private void jRadioButtonMenuItem1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem1ItemStateChanged
	    if (jRadioButtonMenuItem1.isSelected()) {
		plaScorecard.getTable().setLockedColumns(nColFreeze);
	    } else {
		plaScorecard.getTable().setLockedColumns(0);
	    }
	}//GEN-LAST:event_jRadioButtonMenuItem1ItemStateChanged

	private void treeScoreCardingTreeWillCollapse(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {//GEN-FIRST:event_treeScoreCardingTreeWillCollapse
	    treeExpanded = true;
	}//GEN-LAST:event_treeScoreCardingTreeWillCollapse

	private void treeScoreCardingTreeWillExpand(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {//GEN-FIRST:event_treeScoreCardingTreeWillExpand
	    StringBuilder parameters = new StringBuilder();
	    DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) evt.getPath().getLastPathComponent();

	    treeExpanded = true;

	    if (oMutTreeNode != null) {
		Object userObject = oMutTreeNode.getUserObject();

		if (userObject instanceof kpi.swing.KpiTreeNode) {
		    kpi.swing.KpiTreeNode oKpiTreeNode = (kpi.swing.KpiTreeNode) userObject;

		    //Defini�ao dos par�metros da requisi��o.
		    parameters.append("LISTA_SCO_CHILD"); //Identificador.
		    parameters.append("|");
		    if (oKpiTreeNode.getID().equals("0")){
                        parameters.append(""); //O ID do registro 0 � branco.
                    }else{
                        parameters.append(oKpiTreeNode.getID()); //Scorecard PAI.
                    }
		    parameters.append("|");
		    parameters.append(event.getComboValue(htbPacote, cboPacotes)); //Pacote.
		    parameters.append("|");
		    parameters.append("T"); //Verificar acesso?

		    //Requisita a lista de filhos do scorecard que foi expandido.
		    BIXMLRecord recPlanos = event.listRecords(
			    "SCORECARD",
			    parameters.toString());

		    //Insere din�micamente os scorecards no pai.
		    buildTree.insertChild(recPlanos, oMutTreeNode);

		    //Atualiza o status dos scorecards filhos.
		    updatePlanilhaData(oKpiTreeNode.getID(), "SCORECARD", 2, oKpiTreeNode, oMutTreeNode, false);
		    treeScoreCarding.repaint();
		}
	    }
	}//GEN-LAST:event_treeScoreCardingTreeWillExpand

	private void btnGraficoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGraficoActionPerformed
	    if (tbTabela.isSelected()) {
		int lin = plaScorecard.getSelectedRow();
		if (lin > -1) {
		    BIXMLRecord recLinha = plaScorecard.getXMLData(false).get(lin);
		    kpi.swing.graph.KpiGraphIndicador frame = (kpi.swing.graph.KpiGraphIndicador) formController.getForm(
			    "GRAPH_IND", recLinha.getString("ID"), java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00002")).asJInternalFrame();

		    //Carregando os dados do gr�ficos.
		    frame.loadGraphRecord(recLinha.getString("ID"),
			    date_Util.getCalendarString(datePane.fldDataDe.getCalendar()),
			    date_Util.getCalendarString(datePane.fldDataAte.getCalendar()));
		}
	    } else {
		if (dskPaineis.getVctFrames().size() > 0) {
		    KpiWidget wid = (KpiWidget) dskPaineis.getSelectedFrame();
		    kpi.swing.graph.KpiGraphIndicador frame = (kpi.swing.graph.KpiGraphIndicador) formController.getForm(
			    "GRAPH_IND", wid.getIndBean().getIDInd(), java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00002")).asJInternalFrame();

		    //Carregando os dados do gr�ficos.
		    frame.loadGraphRecord(wid.getIndBean().getIDInd(), datePane.getDataDe(), datePane.getDataAte());
		}
	    }
	}//GEN-LAST:event_btnGraficoActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
	reloadScoreCarding(true);
    }

    public void reloadScoreCarding(boolean blnClean) {
	if (blnClean) {
	    //Limpa o cache de indicadores.
	    cacheInd.clear();
	    //Limpa os indices do cache de indicadores.
	    indexChachedItens.clear();
	}

	this.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnVisualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVisualizarActionPerformed
	    if (tbTabela.isSelected()) {
		int lin = plaScorecard.getSelectedRow();
		if (lin > -1) {
		    BIXMLRecord recLinha = plaScorecard.getXMLData(false).get(lin);
		    formController.getForm("INDICADOR", recLinha.getString("ID"), recLinha.getString("INDICADOR")).asJInternalFrame();
		    cacheInd.clear();
		}
	    } else {
		if (dskPaineis.getVctFrames().size() > 0) {
		    KpiWidget wid = (KpiWidget) dskPaineis.getSelectedFrame();
		    formController.getForm("INDICADOR", wid.getIndBean().getIDInd(), wid.getIndBean().getNomeInd()).asJInternalFrame();
		    cacheInd.clear();
		}
	    }
	}//GEN-LAST:event_btnVisualizarActionPerformed

	private void btnVoltarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVoltarMouseClicked
	    if (btnVoltar.isEnabled()) {
		ctrlHistIndicador(BTN_VOLTAR);
	    }
	}//GEN-LAST:event_btnVoltarMouseClicked

	private void btnAvancarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAvancarMouseClicked
	if (tbTabela.isSelected()) {
	    int lin = plaScorecard.getSelectedRow();
	    BIXMLRecord recLinha = plaScorecard.getXMLData(false).get(lin);

	    if (recLinha.getBoolean("DRILL")) {
		updatePlanilhaData(recLinha.getString("ID"), "INDICADOR");
		updateHistorico(recLinha.getString("ID"));
	    }
	} else {
	    if (dskPaineis.getVctFrames().size() > 0) {
		KpiWidget wid = (KpiWidget) dskPaineis.getSelectedFrame();
		if (wid.getIndBean().isDrill()) {
		    updatePlanilhaData(wid.getId(), "INDICADOR");
		    updateHistorico(wid.getId());
		}
	    }
	}


	}//GEN-LAST:event_btnAvancarMouseClicked

	private void treeScoreCardingMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_treeScoreCardingMouseClicked
	    if (evt.getClickCount() == 1 && !treeExpanded) {
		//Se houver uma linha seleciona devo recolher o drill para n�o ocorrer na expans�o quando mudo de objetivo
		BIXMLVector dados = plaScorecard.getXMLData(false);
		if (dados != null && dados.size() > 0){
		    BIXMLRecord recLine = dados.get(0);
		    doDrillUp(recLine.getAttributes(), recLine.getString("ID"));
		}
		plaScorecard.getJTable().clearSelection();
		//Recarrega o scorecarding.
		reloadPlanilha();
		//Recarrega o widget.
		if (tbWidget.isSelected()) {
		    buildWidgetCard();
		}
	    }
	    treeExpanded = false;
	    btnAnalitico.setEnabled(false);
	    btnLink.setEnabled(false);
	    buildTree.setSelectedTreePath(treeScoreCarding.getSelectionPath());
             
             
	}//GEN-LAST:event_treeScoreCardingMouseClicked

    private void btnLinkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLinkActionPerformed
	try {
	    if (tbTabela.isSelected()) {
		int linha = plaScorecard.getSelectedRow();
		if (linha > -1) {
		    BIXMLRecord recLinha = plaScorecard.getXMLData(false).get(linha);
		    //Transforma o request em uma URL.
		    URL url = new URL(recLinha.getString("LINK"));
		    //Realiza a abertura do arquivo no Browser.
		    new KpiToolKit().browser(url);
		}
	    } else {
		if (dskPaineis.getVctFrames().size() > 0) {
		    KpiWidget wid = (KpiWidget) dskPaineis.getSelectedFrame();
		    //Transforma o request em uma URL.
		    URL url = new URL(wid.getIndBean().getLinkInd());
		    //Realiza a abertura do arquivo no Browser.
		    new KpiToolKit().browser(url);
		}
	    }

	} catch (Exception ex) {
	}

	}//GEN-LAST:event_btnLinkActionPerformed

    private void btnIndicFormulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIndicFormulaActionPerformed
	if (tbTabela.isSelected()) {
	    int linha = plaScorecard.getSelectedRow();
	    if (linha > -1) {
		BIXMLRecord recLinha = plaScorecard.getXMLData(false).get(linha);
		formController.getForm("INDICADOR_FORMULA", recLinha.getString("ID"), recLinha.getString("INDICADOR"));
	    }
	} else {
	    if (dskPaineis.getVctFrames().size() > 0) {
		KpiWidget wid = (KpiWidget) dskPaineis.getSelectedFrame();
		formController.getForm("INDICADOR_FORMULA", wid.getIndBean().getIDInd(), wid.getIndBean().getNomeInd());
	    }
	}

}//GEN-LAST:event_btnIndicFormulaActionPerformed

    private void jMenuItemHideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemHideActionPerformed
	splitImg.setDividerLocation(0);
    }//GEN-LAST:event_jMenuItemHideActionPerformed

    private void jMenuItemRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemRefreshActionPerformed
	refreshClientImage();
    }//GEN-LAST:event_jMenuItemRefreshActionPerformed

    private void jMenuItemChangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemChangeActionPerformed
	if (isUserAdmin) {
	    KpiStaticReferences.getKpiFormController().getForm("KPICHANGEIMG", "0", "");
	}
    }//GEN-LAST:event_jMenuItemChangeActionPerformed

    private void doCardView() {
	CardLayout cl = (CardLayout) (pnlViewCard.getLayout());
	if (tbTabela.isSelected()) {
	    cl.show(pnlViewCard, "table");
	} else {
	    cl.show(pnlViewCard, "widget");
	    buildWidgetCard();
	}
    }

    private void buildWidgetCard() {
	buildWidgetCard(recPlanilha.getBIXMLVector("PLANILHAS"));
    }

    private void buildWidgetCard(BIXMLVector vctSco) {

	dskPaineis.setAutoOrganize(true);
	dskPaineis.removeAll();
	dskPaineis.removeDesktopEvent(this);

	for (int i = 0; i < vctSco.size(); i++) {
	    KpiWidget widget = KpiWidgetFactory.createWidget(vctSco.get(i));
	    widget.setToolBarVisible(false);
	    widget.setVisible(true);
	    widget.setOrder(i);

	    dskPaineis.add(widget);
	}

	dskPaineis.autoOrganizeFrames();
	dskPaineis.setSize(dskPaineis.getHeight() + 1, dskPaineis.getWidth());
	dskPaineis.addDesktopEvent(this);
    }

    /***
     * Mostra a tela de Widget, com os indicadores percentes ao scorecard.
     *
     * @param scoID - id do scorecard selecionado.
     * 
     ***/
    public void showWidgetCard(String scoID) {
	//Setando o bot�o selecionado.
	ItemListener[] evtBtnList = tbTabela.getItemListeners();
	for (int iEvento = 0; iEvento < evtBtnList.length; iEvento++) {
	    tbTabela.removeItemListener(evtBtnList[iEvento]);
	}

	tbTabela.setSelected(false);
	tbWidget.setSelected(true);
	splitScoreCarding.setDividerLocation(0);

	for (int iEvento = 0; iEvento < evtBtnList.length; iEvento++) {
	    tbTabela.addItemListener(evtBtnList[iEvento]);
	}
	//
	KpiScoreCardingRequest scoRequest = createRequest("SCORECARD", 2, null, null, true);
	scoRequest.setScoreID(scoID);
	scoRequest.setLoadIndicador(true);

	BIXMLRecord recDados = event.loadRecord(scoID, recordType, scoRequest.getCommand());

	buildWidgetCard(recDados.getBIXMLVector("PLANILHAS"));
	CardLayout cl = (CardLayout) (pnlViewCard.getLayout());
	cl.show(pnlViewCard, "widget");
    }

    private void tbTabelaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_tbTabelaItemStateChanged
	doCardView();
}//GEN-LAST:event_tbTabelaItemStateChanged

    private void dskPaineisComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_dskPaineisComponentResized
	if (tbWidget.isSelected()) {
	    dskPaineis.autoOrganizeFrames();
	}
}//GEN-LAST:event_dskPaineisComponentResized

    private void cboPacotesItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboPacotesItemStateChanged
	if (cboPacotes.getSelectedIndex() > -1) {
	    this.reloadScoreCarding(true);
	}
    }//GEN-LAST:event_cboPacotesItemStateChanged

    private void btnAnaliticoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnaliticoActionPerformed
	StringBuilder parametros = new StringBuilder();
	SimpleDateFormat formato = new SimpleDateFormat("yyyyMMdd");

	parametros.append("analitico.apw");
	parametros.append("?indicador=");

	if (tbTabela.isSelected()) {
	    int linha = plaScorecard.getSelectedRow();

	    if (linha > -1) {
		BIXMLRecord recLinha = plaScorecard.getXMLData(false).get(linha);
		parametros.append(recLinha.getString("ID"));
	    }
	} else {
	    if (dskPaineis.getVctFrames().size() > 0) {
		KpiWidget wid = (KpiWidget) dskPaineis.getSelectedFrame();
		parametros.append(wid.getIndBean().getIDInd());
	    }
	}

	parametros.append("&alvo=");
	parametros.append(formato.format(datePane.fldDataAlvo.getCalendar().getTime()));
	parametros.append("&pagina=");
	parametros.append("1");

	KpiToolKit toolkit = new KpiToolKit();
	toolkit.browser(parametros.toString());
    }//GEN-LAST:event_btnAnaliticoActionPerformed

    private void cmdCausaEfeitoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdCausaEfeitoActionPerformed
	StringBuilder parametros = new StringBuilder();

	if (tbTabela.isSelected()) {
	    int linha = plaScorecard.getSelectedRow();

	    if (linha > -1) {
		BIXMLRecord recLinha = plaScorecard.getXMLData(false).get(linha);
		parametros.append(recLinha.getString("ID"));
		formController.newForm("LST_ESP_PEIXE", "0", parametros.toString());
	    }
	} else {
	    if (dskPaineis.getVctFrames().size() > 0) {
		KpiWidget wid = (KpiWidget) dskPaineis.getSelectedFrame();
		parametros.append(wid.getIndBean().getIDInd());
		formController.newForm("LST_ESP_PEIXE", "0", parametros.toString());
	    }
	}
    }//GEN-LAST:event_cmdCausaEfeitoActionPerformed


    private void showMapa(String mapModel){
	kpi.swing.KpiTreeNode oKpiTreeNode = getKpiTreeNodeSelected();
	if (oKpiTreeNode != null) {
	    if (oKpiTreeNode.getIDimage() == KpiImageResources.KPI_IMG_ESTRATEGIA) {
		IKpiMapaMainForm mapEst = (IKpiMapaMainForm)formController.getForm(mapModel, oKpiTreeNode.getID(), "0", oKpiTreeNode.getName());
                mapEst.setDataAcumDe(datePane.fldDataDe.getCalendar());
                mapEst.setDataAcumAte(datePane.fldDataAte.getCalendar());
		mapEst.setDataAlvo(datePane.fldDataAlvo.getCalendar());
	    }
	}
    }

    private void btnMapEstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMapEstActionPerformed
	kpi.swing.KpiTreeNode oKpiTreeNode = getKpiTreeNodeSelected();

	if (oKpiTreeNode != null) {
	    if (oKpiTreeNode.getIDimage() == KpiImageResources.KPI_IMG_ESTRATEGIA) {

		JPopupMenu popup = new JPopupMenu();
//		JMenuItem item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00027"));
		JMenuItem item1 = new JMenuItem(ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00065"));
		JMenuItem item2 = new JMenuItem(ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00066"));

		ActionListener a = new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			showMapa("MAPAESTRATEGICO1");
		    }
		};
		item1.addActionListener(a);
		popup.add(item1);

		ActionListener b = new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			showMapa("MAPAESTRATEGICO2");
		    }
		};
		item2.addActionListener(b);
		popup.add(item2);

		JButton btn =(JButton)evt.getSource();
		popup.show(btn, btn.getWidth() / 2, btn.getHeight() / 2);
	    }
	}
    }//GEN-LAST:event_btnMapEstActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnalitico;
    private javax.swing.JButton btnAvancar;
    private javax.swing.JButton btnDwConsulta;
    private javax.swing.JButton btnGrafico;
    private javax.swing.JButton btnIndicFormula;
    private javax.swing.JButton btnLink;
    private javax.swing.JButton btnMapEst;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnVisualizar;
    private javax.swing.JButton btnVoltar;
    private javax.swing.JComboBox cboPacotes;
    private javax.swing.JButton cmdCausaEfeito;
    public kpi.beans.JBIDatePane datePane;
    private kpi.beans.JBICardDesktopPane dskPaineis;
    private javax.swing.ButtonGroup grpMnu;
    private javax.swing.ButtonGroup grpViewType;
    private javax.swing.JMenuItem jMenuItemChange;
    private javax.swing.JMenuItem jMenuItemHide;
    private javax.swing.JMenuItem jMenuItemRefresh;
    private javax.swing.JMenuItem jMnuRestore;
    private javax.swing.JMenuItem jMnuSaveConfig;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JPopupMenu jPopupMenuClientImg;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem1;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem2;
    private javax.swing.JToolBar.Separator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JToolBar.Separator jSeparator4;
    private javax.swing.JToolBar.Separator jSeparator5;
    private javax.swing.JToolBar.Separator jSeparator6;
    private javax.swing.JToolBar.Separator jSeparator7;
    private javax.swing.JToolBar jToolBar1;
    private kpi.beans.JBIXMLTable plaScorecard;
    private javax.swing.JPanel pnlCima;
    private javax.swing.JPanel pnlClientImage;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlScoArea;
    private javax.swing.JPanel pnlTree;
    private javax.swing.JPanel pnlViewCard;
    private javax.swing.JPanel pnlViewData;
    private javax.swing.JScrollPane scrTree;
    private javax.swing.JScrollPane scrlPainel;
    private javax.swing.JToolBar.Separator sepMapEst;
    private javax.swing.JSplitPane splitImg;
    private javax.swing.JSplitPane splitScoreCarding;
    private javax.swing.JToggleButton tbTabela;
    private javax.swing.JToggleButton tbWidget;
    private javax.swing.JToggleButton tglBtnAutoUpt;
    private javax.swing.JToolBar toolBar;
    private javax.swing.JTree treeScoreCarding;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    /*
     *Atualiza a partir do n� atual as propriedades do n�s filhos.
     */
    private void putChildNodeProperties(kpi.xml.BIXMLVector vecStatusSco, DefaultMutableTreeNode node) {
	if (node.getChildCount() > 0) {
	    java.util.Enumeration nodeItens = node.children();
	    //Recuperando os n�s que serao expandidos
	    while (nodeItens.hasMoreElements()) {
		DefaultMutableTreeNode nodeItem = (DefaultMutableTreeNode) nodeItens.nextElement();
		if (nodeItem != null) {
		    Object userObject = nodeItem.getUserObject();
		    if (userObject instanceof kpi.swing.KpiTreeNode) {
			kpi.swing.KpiTreeNode treeNode = (kpi.swing.KpiTreeNode) userObject;
			if (treeNode != null) {
			    setNodePropertie(treeNode, vecStatusSco);
			}
		    }
		}
	    }
	}
    }

    private void setNodePropertie(kpi.swing.KpiTreeNode treeNode, kpi.xml.BIXMLVector vctScoStatus) {
	//Atualizando o icone do status
	HashMap hashNodeAttibutes = treeNode.getHashNodeProperty();
	if (hashNodeAttibutes.containsKey("STATUS")) {
	    hashNodeAttibutes.remove("STATUS");
	}
	if (hashNodeAttibutes.containsKey("SCORE_OWNER")) {
	    hashNodeAttibutes.remove("SCORE_OWNER");
	}

	for (int item = 0; item < vctScoStatus.size(); item++) {
	    kpi.xml.BIXMLRecord tmpRec = vctScoStatus.get(item);
	    if (tmpRec.getString("ID").equals(treeNode.getID())) {
		treeNode.putHashNodeProperty("STATUS_DATAALVO", tmpRec.getInt("STATUS_DATAALVO"));
		treeNode.putHashNodeProperty("SCORE_OWNER", tmpRec.getString("SCORE_OWNER"));
	    }
	}
    }

    private void plaScorecardMouseClicked(java.awt.event.MouseEvent evt) {
	int coluna = tableData.getSelectedColumn();
	int linha = plaScorecard.getSelectedRow();
	BIXMLRecord dados = plaScorecard.getXMLData(false).get(linha);

	if (evt.getClickCount() == 2) {
	    if (plaScorecard.getColumn(coluna).getModelIndex() == COL_ACAO) {
		String sText = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00013");
		KpiListaPlanos oFormListaPlanos = (KpiListaPlanos) KpiStaticReferences.getKpiFormController().getForm("LSTPLANOACAO", "0", sText);
		oFormListaPlanos.buildFilter(KpiListaPlanos.FILTER_SCORECARD, getLastScoreID());

	    } else if (plaScorecard.getColumn(coluna).getModelIndex() == COL_NOTA) {

		KpiListaNota frmLstNota = (KpiListaNota)KpiStaticReferences.getKpiFormController().getForm("LSTNOTA", "0", dados.getString("ID"), dados.getString("INDICADOR"));
		DefaultMutableTreeNode noSelected = (DefaultMutableTreeNode) treeScoreCarding.getLastSelectedPathComponent();

		if (noSelected != null && noSelected.getUserObject() instanceof kpi.swing.KpiTreeNode ) {
		    KpiTreeNode kpiNode = (KpiTreeNode) noSelected.getUserObject();
		    Calendar dAlvo = datePane.fldDataAlvo.getCalendar();
		    
		    frmLstNota.setScoItensSelected(dados.getString("ID"),dados.getString("INDICADOR"), 
						    kpiNode.getName(),dados.getString("FREQNOME"),
						    dados.getDouble("VALOR"),dados.getDouble("META"),dados.getDouble("VARIACAO_PERC"),
						    dados.getInt("DECIMAIS"),dAlvo);
		}		
	    } else {
		if (dados.getBoolean("DRILL")) {
		    updatePlanilhaData(dados.getString("ID"), "INDICADOR");
		    updateHistorico(dados.getString("ID"));
		}
	    }
	} else {
	    if (dados.getBoolean("DRILL")) {
		btnAvancar.setEnabled(true);
	    } else {
		btnAvancar.setEnabled(false);
	    }

	    if (dados.getString("LINK").trim().length() > 0) {
		btnLink.setEnabled(true);
	    } else {
		btnLink.setEnabled(false);
	    }

	    if (dados.getBoolean("ANALITICO")) {
		btnAnalitico.setEnabled(true);
	    } else {
		btnAnalitico.setEnabled(false);
	    }
	}
    }


    /*
     * Fun��o chamada quando o Widget � ativado
     */
    @Override
    public void onActivateCard(JInternalFrame f) {
	KpiWidget wid = (KpiWidget) f;

	if (wid.getIndBean().isDrill()) {
	    btnAvancar.setEnabled(true);
	} else {
	    btnAvancar.setEnabled(false);
	}

	if (wid.getIndBean().getLinkInd().trim().length() > 0) {
	    btnLink.setEnabled(true);
	} else {
	    btnLink.setEnabled(false);
	}

	if (wid.getIndBean().isAnalitico()) {
	    btnAnalitico.setEnabled(true);
	} else {
	    btnAnalitico.setEnabled(false);
	}
    }

    /**
     * Atualiza os dados na planilha de scorecard.
     */
    public void updatePlanilhaData(String id, String tipo) {
	kpi.swing.KpiTreeNode oKpiTreeNode = getKpiTreeNodeSelected();
	updatePlanilhaData(id, tipo, 2, oKpiTreeNode);
    }

    /**
     * Atualiza os dados na planilha de scorecard.
     */
    private void updatePlanilhaData(String id, String tipo, int loadType, kpi.swing.KpiTreeNode oKpiTreeNode) {
	DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) treeScoreCarding.getLastSelectedPathComponent();
	updatePlanilhaData(id, tipo, loadType, oKpiTreeNode, oMutTreeNode, true);
    }

    /**
     * Atualiza os dados na planilha de scorecard.
     */
    private void updatePlanilhaData(String id, String tipo, int loadType, kpi.swing.KpiTreeNode oKpiTreeNode, DefaultMutableTreeNode oMutTreeNode, boolean loadIndicador) {
	String tmpBuffID = id.concat(tipo);
	String tipoColuna = null;
	String colName;

	int colIndicador = 0;
	int colPeso = 0;
	int colOrdem = 0;

	//Recupera indicadores do cache.
	if (cacheInd.containsKey(tmpBuffID)) {
	    recPlanilha = (BIXMLRecord) cacheInd.get(tmpBuffID);
	} else {
	    //Criar a string com os par�metros de requisi��o ao protheus.
	    KpiScoreCardingRequest scoRequest = createRequest(tipo, loadType, oMutTreeNode, oKpiTreeNode, loadIndicador);
	    String request = scoRequest.getCommand();
	    //Requisita os dados ao protheus.
	    recPlanilha = event.loadRecord(id, recordType, request);

	    //Realiza cache apenas de indicadores.
	    if (loadIndicador && !tipo.equalsIgnoreCase("SCORECARD")) {
		//Insere o indicador no cache.
		cacheInd.put(tmpBuffID, recPlanilha);
		indexChachedItens.add(tmpBuffID);
		//Remove os indicadores mais antigos do cache.
		if (cacheInd.size() > CACHE_MAXSIZE) {
		    String idRemove = (String) indexChachedItens.get(0);
		    cacheInd.remove(idRemove);
		    indexChachedItens.remove(0);
		}
	    }
	}

	//Executa o Drill te linhas da planilha
	if (isDrillEnable) {
	    doDrillOnLine();
	} else {
	    plaScorecard.setTable(new pv.jfcx.JPVTable());
	    BIXMLVector vctPlanilha = recPlanilha.getBIXMLVector("PLANILHAS");
	    plaScorecard.setDataSource(vctPlanilha);
	}

	//Preparacao do Render
	cellRenderer.resetRendererBuffer();
	for (int col = 0; col < plaScorecard.getModel().getColumnCount(); col++) {
	    colName = plaScorecard.getColumnTag(col);

	    if (colName.startsWith("INDICADOR")) {
		colIndicador = col;
	    } else if (colName.startsWith("PESO")) {
		colPeso = col;
	    } else if (colName.startsWith("ORDEM")) {
		colOrdem = col;
	    }

	    //Defini��o do render especifico para as colunas do tipo numero e texto.
	    tipoColuna = plaScorecard.getColumnType(col);
	    int colTipo = new Integer(tipoColuna).intValue();

	    if (colTipo == JBIXMLTable.KPI_FLOAT || colTipo == JBIXMLTable.KPI_STRING) {
		plaScorecard.getColumn(col).setCellRenderer(cellRenderer);
	    } else if (colTipo == JBIXMLTable.KPI_IMAGEM) {
		plaScorecard.getColumn(col).setCellRenderer(cellImageRenderer);
	    }
	}

	//Atualiza o status do scorecard [Cor dos �cones].
	if (oKpiTreeNode != null && tipo.equals("SCORECARD")) {

	    //Atualiza o status dos filhos do scorecard.
	    if (recPlanilha.contains("SCORECARDS_STATUS")) {
		kpi.xml.BIXMLVector vecStatusSco = recPlanilha.getBIXMLVector("SCORECARDS_STATUS");
		if (vecStatusSco.size() > 0) {
		    putChildNodeProperties(vecStatusSco, oMutTreeNode);
		}
	    }
	}

	//Ordenando o refresh confome determinado na Config do Sistema.
	if (ordemSCD == 1) {
	    plaScorecard.setSortColumn(colPeso, false);
	} else if (ordemSCD == 2) {
	    plaScorecard.setSortColumn(colOrdem, true);
	} else {
	    plaScorecard.setSortColumn(colIndicador, true);
	}

	//Removendo colunas
	plaScorecard.getTable().getColumnModel().removeColumn(plaScorecard.getColumn(colPeso));
	plaScorecard.getTable().getColumnModel().removeColumn(plaScorecard.getColumn(colOrdem - 1));

	//Seta outras configura��es da tabela
	buildTable();

	//Atualiza os widgets
	if (tbWidget.isSelected()) {
	    buildWidgetCard();
	}
    }

    /**
     *Seta atributos da tabela
     */
    private void buildTable() {
	plaScorecard.setHeaderHeight(35);
	plaScorecard.setHeaderFont(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11));
	plaScorecard.setRowHeight(25);

	tableData = plaScorecard.getTable();
	tableData.setColumnSelectionAllowed(false);
	tableData.setAutoResizeMode(JPVTable.AUTO_RESIZE_OFF);

	//Setando as colunas congeladas
	if (jRadioButtonMenuItem1.isSelected()) {
	    plaScorecard.getTable().setLockedColumns(nColFreeze);
	} else {
	    plaScorecard.getTable().setLockedColumns(0);
	}

	//Setando a largura das colunas
	for (int col = 0; col < plaScorecard.getTable().getColumnCount(); col++) {
	    plaScorecard.getColumn(col).setPreferredWidth(userConfig.getWidth(col));
	}

	tableData.addMouseListener(new java.awt.event.MouseAdapter() {

	    @Override
	    public void mouseClicked(java.awt.event.MouseEvent evt) {
		plaScorecardMouseClicked(evt);
	    }
	});

	tableData.getTableHeader().addMouseListener(new java.awt.event.MouseAdapter() {

	    @Override
	    public void mouseClicked(java.awt.event.MouseEvent evt) {
		jPopupMenu1.show(tableData.getTableHeader(), evt.getX(), evt.getY());
	    }
	});

    }

    /**
     *Incializa o hist�rio de navega��o do DrillDown.
     */
    private void newHistorico(String scoID) {
	vctHistVol.removeAllElements();

	kpi.xml.BIXMLRecord newRecord = new kpi.xml.BIXMLRecord("PLANILHA");
	newRecord.set("ID", scoID);
	newRecord.set("HISTORICO", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00011"));
	newRecord.set("TYPE", "SCORECARD");
	vctHistVol.add(0, newRecord);

	//Configurando os bot�es.
	btnVoltar.setEnabled(false);
	btnAvancar.setEnabled(false);

    }

    /**
     * Adiciona um novo item no hist�rico.
     */
    private void updateHistorico(String indID) {
	kpi.xml.BIXMLRecord newRecord = new kpi.xml.BIXMLRecord("PLANILHA");

	if (clearHist) {
	    kpi.swing.KpiTreeNode treeNode = getKpiTreeNodeSelected();
	    if (treeNode != null) {
		String scoID = getScoreCardSelected(treeNode);
		newHistorico(scoID);
	    }
	    clearHist = false;
	}

	newRecord.set("ID", indID);
	newRecord.set("TYPE", "INDICADOR");

	//Vetor Undo Redo;
	vctHistVol.add(newRecord);
	ctrHistorico = vctHistVol.size();

	//Verifica as op��es para habilitar o bot�o de hist�rico
	if (ctrHistorico > 1) {
	    btnVoltar.setEnabled(true);
	    btnAvancar.setEnabled(false);
	}
    }

    /**
     * Cria a requisi��o para ser enviada ao ADPVL.
     * @param strTipo Tipo da requisi��o: SCORECARD ou INDICADOR
     * @param iLoadType Tipo do carregamento 1=Chamado do LoadRecord 2=Outros pontos do programa.
     */
    private String createRequest(String strTipo, int iLoadType) {
	DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeScoreCarding.getLastSelectedPathComponent();
	kpi.swing.KpiTreeNode treeNode = getKpiTreeNodeSelected();
	KpiScoreCardingRequest scoRequest = createRequest(strTipo, iLoadType, node, treeNode, true);
	return scoRequest.getCommand();
    }

    /***
     * 
     * Cria uma requisi��o para carregamento dos SCORECARDS ou INDICADORES.
     * @param strTipo = Tipo da Requisi��o = SCORECARDS ou INDICADORE.
     * @param iLoadType =   Tipo do carregamento 
     *			    1 = Atualiza o status do filho e Atualiza a �rvore.
     *			    2 = Atualiza o stattus do filho caso haja um n� selecionado n�o atualiza a �rvore.
     * @param node	= N� da �rvore selecionado.
     * @param treeNode  = N� da �rvore selecionado.
     * @param loadInd = true para carregar os indicadores.
     * @return = String de requisi��o "SCORECARD|03/10/11 08:48|01/01/11 00:00|31/12/11 00:00|0000000007|T|F|F|0|"
     * 
     ***/
    private KpiScoreCardingRequest createRequest(String strTipo, int iLoadType, DefaultMutableTreeNode node, KpiTreeNode treeNode, boolean loadInd) {
	KpiScoreCardingRequest reqNew = new KpiScoreCardingRequest();

	reqNew.setEvent(event);
	reqNew.setRequestType(strTipo);
	reqNew.setDataAlvo(datePane.fldDataAlvo);
	reqNew.setDataDe(datePane.fldDataDe);
	reqNew.setDataAte(datePane.fldDataAte);
	reqNew.setScoreID(getLastScoreID());

	//Perguntas
	if (iLoadType == 1) {
	    reqNew.setUpdateFilho(true);
	    reqNew.setUpdateArvore(true);
	} else {
	    if ((node != null && node.getChildCount() > 0) || getLastScoreID().equals("0") || isParentOwner()) {
		reqNew.setUpdateFilho(true);
	    } else {
		reqNew.setUpdateFilho(false);
	    }
	    reqNew.setUpdateArvore(false);
	}

	//Carrega os indicadores?
	if (treeNode != null && treeNode.isEnable() && loadInd) {
	    reqNew.setLoadIndicador(true);
	} else {
	    reqNew.setLoadIndicador(false);
	}
	reqNew.enableFilterPackage(node, cboPacotes, htbPacote);

	return reqNew;
    }

    /**
     *Controla os cliques nos bot�es do hist�rico do indicador.
     */
    private void ctrlHistIndicador(int iBotao) {
	BIXMLRecord recItem = new BIXMLRecord("TEMP");

	if (iBotao == BTN_AVANCAR) {
	    ctrHistorico++;
	    recItem = (BIXMLRecord) vctHistVol.get(ctrHistorico - 1);
	} else {
	    ctrHistorico--;
	    recItem = (BIXMLRecord) vctHistVol.get(ctrHistorico - 1);
	    vctHistVol.remove(ctrHistorico);
	}

	//Movimentando  atrav�s do hist�rico.
	updatePlanilhaData(recItem.getString("ID"), recItem.getString("TYPE"));

	if (ctrHistorico <= 1) {
	    btnVoltar.setEnabled(false);
	} else {
	    btnVoltar.setEnabled(true);
	}

	if (ctrHistorico >= vctHistVol.size()) {
	    btnAvancar.setEnabled(false);
	} else {
	    btnAvancar.setEnabled(true);
	}

	if (!btnVoltar.isEnabled()) {
	    clearHist = true;
	}
    }

    /**
     *
     */
    private synchronized void reloadPlanilha() {
	kpi.swing.KpiTreeNode treeNode = getKpiTreeNodeSelected();
	if (treeNode != null) {
	    if (treeNode.getIDimage() == KpiImageResources.KPI_IMG_ESTRATEGIA) {
		btnMapEst.setEnabled(true);
	    } else {
		btnMapEst.setEnabled(false);
	    }

	    this.setTitle(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00001") + " - " + treeNode.getName());
	    String scoID = getScoreCardSelected(treeNode);
	    setLastScoreID(scoID);
	    updatePlanilhaData(scoID, "SCORECARD", 2, treeNode);
	    newHistorico(scoID);
	    treeScoreCarding.repaint();
	}
    }

    private kpi.swing.KpiTreeNode getKpiTreeNodeSelected() {
	DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) treeScoreCarding.getLastSelectedPathComponent();
	return getKpiTreeNodeSelected(oMutTreeNode);
    }

    /**
     *
     * @param oMutTreeNode
     * @return
     */
    private kpi.swing.KpiTreeNode getKpiTreeNodeSelected(DefaultMutableTreeNode oMutTreeNode) {
	kpi.swing.KpiTreeNode oKpiTreeNode = null;

	if (oMutTreeNode != null) {
	    Object userObject = oMutTreeNode.getUserObject();
	    if (userObject instanceof kpi.swing.KpiTreeNode) {
		oKpiTreeNode = (kpi.swing.KpiTreeNode) userObject;
	    }
	}

	return oKpiTreeNode;
    }

    /**
     *
     * @param treeNode
     * @return
     */
    private String getScoreCardSelected(kpi.swing.KpiTreeNode treeNode) {
	String scoID = "0";
	scoID = treeNode.getID();

	return scoID;
    }

    /**
     * Construtor.
     * @param operation Tipo da opera��o
     * @param idAux N�mero do id Auxiliar
     * @param contextId N�mero do contexto id.
     */
    public KpiScoreCarding(int operation, String idAux, String contextId) {

	putClientProperty("MAXI", true);
	initComponents();
	refreshClientImage();

	//Valida��o de seguran�a
        KpiMenuController mnuControl = KpiStaticReferences.getMnuController();
        IteMnuPermission perm = mnuControl.getRuleByName("INDICADORES");

        if (perm.getPermissionValue(TipoPermissao.VISUALIZACAO) == Permissao.PERMITIDO ||
		    perm.getPermissionValue(TipoPermissao.MANUTENCAO) == Permissao.PERMITIDO){
	    btnVisualizar.setEnabled(true);
	    btnVisualizar.setVisible(true);
	} else {
	    btnVisualizar.setVisible(false);
	}

	event.defaultConstructor(operation, idAux, contextId);

    }

    @Override
    public void setStatus(int value) {
	status = value;
    }

    @Override
    public int getStatus() {
	return status;
    }

    @Override
    public void setType(String value) {
	recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
	return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
	id = String.valueOf(value);
    }

    @Override
    public String getID() {
	return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
	record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
	return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
	return this;
    }

    @Override
    public String getParentID() {
	if (record.contains("PARENTID")) {
	    return String.valueOf(record.getString("PARENTID"));
	} else {
	    return "";
	}
    }

    @Override
    public void loadRecord() {
	//Deve so carregar a arvore.
	setRecord(event.loadRecord("0", "SCORECARDING", createRequest("SCORECARD", 1)));
	refreshFields();
	//repaint();
    }

    @Override
    public String getFormType() {
	return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
	return new javax.swing.JTabbedPane();
    }

    @Override
    public String getParentType() {
	return parentType;
    }

    public String getLastScoreID() {
	return lastScoreID;
    }

    public void setLastScoreID(String lastScoreID) {
	this.lastScoreID = lastScoreID;
    }

    /*
     *Verifica se o usu�rio logado � o resp�nsavel pelo scorecard atual ou pelos scorecards superiores.
     */
    private boolean isParentOwner() {
	boolean isParentOwner = false;

	if (isUserAdmin) {
	    isParentOwner = true;
	} else {
	    DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) treeScoreCarding.getLastSelectedPathComponent();
	    while (oMutTreeNode != null) {
		kpi.swing.KpiTreeNode kpiTreeNode = getKpiTreeNodeSelected(oMutTreeNode);
		if (kpiTreeNode != null && kpiTreeNode.getHashNodeProperty().containsKey("SCORE_OWNER")) {
		    String sScoreOwner = (String) kpiTreeNode.getHashNodeProperty("SCORE_OWNER");
		    if (sScoreOwner.equals(userLogged)) {
			isParentOwner = true;
			break;
		    }
		}
		oMutTreeNode = (DefaultMutableTreeNode) oMutTreeNode.getParent();
	    }
	}

	return isParentOwner;
    }

    /**
     * Classe para agendamento de tarefas.
     */
    private class Reminder {

	private java.util.Timer scoTimer = null;

	public void startScorecardingUpdate(int min) {
	    int millseconds = min * 60 * 1000;
	    scoTimer = new Timer();
	    getTimer().schedule(new AutoUpdateScorecarding(), 1000, millseconds);
	}

	class AutoUpdateScorecarding extends java.util.TimerTask {

	    @Override
	    public void run() {
		//Realiza a atualiza��o dos dados do scorecarding para o per�odo em exibi��o.
		if (tglBtnAutoUpt.isSelected()) {
		    if (ctrHistorico > 1) {
			cacheInd.clear();
			updatePlanilhaData(((BIXMLRecord) vctHistVol.get(ctrHistorico - 1)).getString("ID"), "INDICADOR");
		    } else {
			cacheInd.clear();
			reloadPlanilha();
		    }
		} else {
		    //Finaliza a thread do timer.
		    getTimer().cancel();
		}
	    }
	}

	public java.util.Timer getTimer() {
	    return scoTimer;
	}
    }

    private void doDrillOnLine() {
	BIXMLVector xmlTable = null;
	int selRow = plaScorecard.getSelectedRow();

	if (selRow != -1) {
	    BIXMLRecord recRowSel = plaScorecard.getSelectedRecord();
	    if (recRowSel.getBoolean("DRILL")) {

		BIXMLAttributes attRowSel = recRowSel.getAttributes();
		if (!attRowSel.contains("DRILL_EXPANDED")) {
		    xmlTable = doDrillDown(selRow, attRowSel, recRowSel.getString("ID"));
		} else {
		    if (attRowSel.getBoolean("DRILL_EXPANDED")) {
			xmlTable = doDrillUp(attRowSel, recRowSel.getString("ID"));
		    } else {
			xmlTable = doDrillDown(selRow, attRowSel, recRowSel.getString("ID"));
		    }
		}
	    }
	} else {
	    xmlTable = recPlanilha.getBIXMLVector("PLANILHAS");
	}

	//Reconstroi a Tabela
	plaScorecard.setTable(new pv.jfcx.JPVTable());
	plaScorecard.setDataSource(xmlTable);
	plaScorecard.setEnableSort(false);
    }

    private BIXMLVector doDrillUp(BIXMLAttributes attRowSel, String idInd) {
	BIXMLVector vctPlanilha = plaScorecard.getXMLData(false);

	if (attRowSel.contains("DRILL_EXPANDED") && attRowSel.getBoolean("DRILL_EXPANDED")) {

	    ArrayList<BIXMLRecord> aRecToRemove = new ArrayList<BIXMLRecord>();
	    removeRelatedRecords(vctPlanilha, aRecToRemove, idInd);

	    if (!aRecToRemove.isEmpty()) {

		//Removendo a marcacao de excluidos
		for (BIXMLRecord recLine : vctPlanilha) {
		    BIXMLAttributes attRecLine = recLine.getAttributes();
		    if (attRecLine != null && attRecLine.contains("TO_DELETED")) {
			attRecLine.remove("TO_DELETED");
		    }
		}

		vctPlanilha.removeAll(aRecToRemove);
	    }

	    attRowSel.remove("DRILL_EXPANDED");
	    attRowSel.set("DRILL_EXPANDED", false);
	}

	return vctPlanilha;
    }

    private void removeRelatedRecords(BIXMLVector vctPlanilha, ArrayList<BIXMLRecord> aRecToRemove, String idToRemove) {

	for (BIXMLRecord recLine : vctPlanilha) {
	    BIXMLAttributes attLine = recLine.getAttributes();

	    if (attLine.contains("IND_ID_PARENT") && idToRemove.equals(attLine.getString("IND_ID_PARENT"))) {
		if (existLineParent(vctPlanilha, recLine.getString("ID"))) {
		    removeRelatedRecords(vctPlanilha, aRecToRemove, recLine.getString("ID"));
		}
		if (attLine.contains("DRILL_EXPANDED") && attLine.getBoolean("DRILL_EXPANDED")) {
		    attLine.remove("DRILL_EXPANDED");
		    attLine.set("DRILL_EXPANDED", false);
		}

		if (!attLine.contains("TO_DELETED")) {
		    attLine.set("TO_DELETED", true);
		    aRecToRemove.add(recLine);
		}
	    }
	}
    }

    /**
     * Verifica se existe uma linha dependende da que esta sendo excluida.
     *
     * @param vctPlanilha = Vetor com as linhas da planilha
     * @param idLine = String com o c�digo da linha que ser� procurado.
     * @return true se existir
     */
    private boolean existLineParent(BIXMLVector vctPlanilha, String idLine) {
	boolean existParent = false;

	for (Iterator<BIXMLRecord> iRecord = vctPlanilha.iterator(); iRecord.hasNext();) {
	    BIXMLRecord recLine = iRecord.next();
	    if (recLine.getString("ID").equals(idLine)) {
		existParent = true;
	    }
	}
	return existParent;
    }

    private BIXMLVector doDrillDown(int line, BIXMLAttributes attRowSel, String indParentId) {
	BIXMLVector xmlTable = null;
	BIXMLVector vctPlanilha = null;
	boolean isDrillExecuted = false;

	//Pode executar o drill
	if (!attRowSel.contains("DRILL_EXPANDED") || !attRowSel.getBoolean("DRILL_EXPANDED")) {
	    xmlTable = plaScorecard.getXMLData(false);

	    //Obtendo o nivel do drill.
	    int drill_level = 0;
	    if (attRowSel.contains("DRILL_LEVEL")) {
		drill_level = attRowSel.getInt("DRILL_LEVEL") + 1;
	    } else {
		drill_level = 1;
	    }

	    vctPlanilha = recPlanilha.getBIXMLVector("PLANILHAS");
	    for (Iterator<BIXMLRecord> iRecord = vctPlanilha.iterator(); iRecord.hasNext();) {
		BIXMLRecord recLine = iRecord.next();

		//Adicionando o nivel do drill.
		recLine.getAttributes().set("DRILL_LEVEL", drill_level);
		recLine.getAttributes().set("IND_ID_PARENT", indParentId);

		plaScorecard.addNewRecord(recLine, line + 1);

		isDrillExecuted = true;
	    }
	    if (isDrillExecuted) {
		attRowSel.set("DRILL_EXPANDED", true);
	    }
	} else {
	    xmlTable = plaScorecard.getXMLData(false);
	}

	return xmlTable;
    }
}

class KpiScoreCardingRequest {

    private String requestType = "";
    private JPVDatePlus dataAlvo = null;
    private JPVDatePlus dataDe = null;
    private JPVDatePlus dataAte = null;
    private String scoreID = "";
    private boolean updateFilho = false;
    private boolean updateArvore = false;
    private boolean loadIndicador = false;
    private KpiDateUtil dateTools = new KpiDateUtil();
    private kpi.core.KpiDateBase dateBase = kpi.core.KpiStaticReferences.getKpiDateBase();
    private KpiDefaultFrameBehavior event = null;
    private Hashtable htbPacote = null;
    private JComboBox cboPacotes = null;
    private DefaultMutableTreeNode node = null;

    /**
     * Setar as seguintes propriedades para havilitar o filtro por pacote:
     * @param node n� da �rvore.
     * @param cboPacotes combo com a lista de pacotes
     * @param htbPacote  Hash da combo com os pacotes.
     */
    public void enableFilterPackage(DefaultMutableTreeNode node, JComboBox cboPacotes, Hashtable htbPacote) {
	this.cboPacotes = cboPacotes;
	this.htbPacote = htbPacote;
	this.node = node;
    }

    /**
     * Monda com comando para as requisi��es
     * @return String com o comando;
     */
    public String getCommand() {
	StringBuilder strComando = new StringBuilder(getRequestType());

	strComando.append("|");
	if (dataAlvo == null || dataAlvo.getCalendar() == null) {
	    strComando.append(dateTools.getCalendarString(dateBase.getDataAnaliseDe()));
	} else {
	    strComando.append(dateTools.getCalendarString(dataAlvo.getCalendar()));
	}

	strComando.append("|");
	if (dataDe == null || dataDe.getCalendar() == null) {
	    strComando.append(dateTools.getCalendarString(dateBase.getDataAcumuladoDe()));
	} else {
	    strComando.append(dateTools.getCalendarString(dataDe.getCalendar()));
	}

	strComando.append("|");
	if (dataAte == null || dataAte.getCalendar() == null) {
	    strComando.append(dateTools.getCalendarString(dateBase.getDataAcumuladoAte()));
	} else {
	    strComando.append(dateTools.getCalendarString(dataAte.getCalendar()));
	}

	//ID Scorecard
	strComando.append("|");
	strComando.append(getScoreID());

	//Atualiza o status dos nos filhos?
	strComando.append("|");
	strComando.append(isUpdateFilho());

	//Atualiza a arvore?
	strComando.append("|");
	strComando.append(isUpdateArvore());

	//Carrega os indicadores?
	strComando.append("|");
	strComando.append(isLoadIndicador());


	//Filtro por pacote.
	strComando.append("|");
	strComando.append(getFilteredPackage());

	return strComando.toString();
    }

    /**
     * @return the requestType
     */
    public String getRequestType() {
	return requestType;
    }

    /**
     * @param requestType the requestType to set
     */
    public void setRequestType(String requestType) {
	this.requestType = requestType;
    }

    /**
     * @return the dataDe
     */
    public JPVDatePlus getDataDe() {
	return dataDe;
    }

    /**
     * @param dataDe the dataDe to set
     */
    public void setDataDe(JPVDatePlus dataDe) {
	this.dataDe = dataDe;
    }

    /**
     * @return the dataAte
     */
    public JPVDatePlus getDataAte() {
	return dataAte;
    }

    /**
     * @param dataAte the dataAte to set
     */
    public void setDataAte(JPVDatePlus dataAte) {
	this.dataAte = dataAte;
    }

    /**
     * @return the ScoreID
     */
    public String getScoreID() {
	return scoreID;
    }

    /**
     * @param ScoreID the ScoreID to set
     */
    public void setScoreID(String ScoreID) {
	this.scoreID = ScoreID;
    }

    /**
     * @return the updateFilho
     */
    public String isUpdateFilho() {
	String updFilho = this.updateFilho ? "T" : "F";

	return updFilho;
    }

    /**
     * @param updateFilho the updateFilho to set
     */
    public void setUpdateFilho(boolean updateFilho) {
	this.updateFilho = updateFilho;
    }

    /**
     * @return the updateArvore
     */
    public String isUpdateArvore() {
	String updArvore = this.updateArvore ? "T" : "F";

	return updArvore;
    }

    /**
     * @param updateArvore the updateArvore to set
     */
    public void setUpdateArvore(boolean updateArvore) {
	this.updateArvore = updateArvore;
    }

    /**
     * @return the loadIndicador
     */
    public String isLoadIndicador() {
	String updIndicador = this.loadIndicador ? "T" : "F";

	return updIndicador;
    }

    /**
     * @param loadIndicador the loadIndicador to set
     */
    public void setLoadIndicador(boolean loadIndicador) {
	this.loadIndicador = loadIndicador;
    }

    /**
     * @return the dataAlvo
     */
    public JPVDatePlus getDataAlvo() {
	return dataAlvo;
    }

    /**
     * @param dataAlvo the dataAlvo to set
     */
    public void setDataAlvo(JPVDatePlus dataAlvo) {
	this.dataAlvo = dataAlvo;
    }

    private String getFilteredPackage() {
	StringBuilder strComando = new StringBuilder("");

	//Filtro por pacote.
	strComando.append(event.getComboValue(htbPacote, cboPacotes));

	//Atualiza��o do status dos departamentos
	StringBuilder oStr = new StringBuilder();
	if (node != null) {
	    java.util.Enumeration nodeItens = node.children();
	    while (nodeItens.hasMoreElements()) {
		DefaultMutableTreeNode nodeItem = (DefaultMutableTreeNode) nodeItens.nextElement();
		if (nodeItem != null) {
		    Object userObject = nodeItem.getUserObject();
		    if (userObject instanceof kpi.swing.KpiTreeNode) {
			kpi.swing.KpiTreeNode oTreeNode = (kpi.swing.KpiTreeNode) userObject;
			if (oTreeNode != null) {
			    if (oStr.toString().length() > 0) {
				oStr.append(",");
			    }
			    oStr.append(oTreeNode.getID());
			}
		    }
		}
	    }
	}

	strComando.append("|");
	strComando.append(oStr.toString());

	return strComando.toString();
    }

    /**
     * @param event the event to set
     */
    public void setEvent(KpiDefaultFrameBehavior event) {
	this.event = event;
    }
}

/**
 *Renderiza��o cuistomizada para a �rvore do scorecarding
 */
class KpiScoreCardingTreeCellRenderer extends kpi.swing.KpiTreeCellRenderer {

    private kpi.core.KpiImageResources myImageResources;

    public KpiScoreCardingTreeCellRenderer(kpi.core.KpiImageResources imageResources) {
	super(imageResources);
	setUseMyRenderer(true);
	myImageResources = imageResources;
    }

    @Override
    public void myImageRenderer(Object value) {
	Integer imgDefault = new Integer(KpiImageResources.KPI_IMG_SCORECARD);
	javax.swing.tree.DefaultMutableTreeNode nodeAux = (javax.swing.tree.DefaultMutableTreeNode) value;

	if (nodeAux.getUserObject() instanceof kpi.swing.KpiTreeNode) {
	    kpi.swing.KpiTreeNode node = (kpi.swing.KpiTreeNode) nodeAux.getUserObject();
	    if (node != null) {
		//Configurando a imagem.
		imgDefault = (Integer) node.getHashNodeProperty("STATUS_DATAALVO");
		if (imgDefault == null) {
		    imgDefault = new Integer(KpiImageResources.KPI_IMG_SCORECARD);
		}

		//Verificando se o no deve estar habilitado.
		if (node.isEnable()) {
		    setEnabled(true);
		} else {
		    setEnabled(false);
		}
	    }
	}

	setOpenIcon(myImageResources.getImage(imgDefault.intValue()));
	setLeafIcon(myImageResources.getImage(imgDefault.intValue()));
	setIcon(myImageResources.getImage(imgDefault.intValue()));
	setClosedIcon(myImageResources.getImage(imgDefault.intValue()));

	setBackground(null);
	setBackgroundNonSelectionColor(null);
	setOpaque(false);
    }
}


/*
 *Renderiza��o cuistomizada para a �rvore do scorecarding
 */
class BscScoreCardingTreeCellRenderer extends kpi.swing.KpiTreeCellRenderer {

    private kpi.core.KpiImageResources myImageResources;
    private BscEntityType bscScorecardType = new BscEntityType();

    public BscScoreCardingTreeCellRenderer(kpi.core.KpiImageResources imageResources) {
	super(imageResources);
	setUseMyRenderer(true);
	myImageResources = imageResources;
    }

    @Override
    public void myImageRenderer(Object value) {
	Integer imgTree = 10;
	javax.swing.tree.DefaultMutableTreeNode nodeAux = (javax.swing.tree.DefaultMutableTreeNode) value;

	if (nodeAux.getUserObject() instanceof kpi.swing.KpiTreeNode) {
	    kpi.swing.KpiTreeNode node = (kpi.swing.KpiTreeNode) nodeAux.getUserObject();
	    if (node != null) {

		//Configurando a imagem.
		bscScorecardType.setEntityByNode(node);
		if (bscScorecardType.getEntityName().equals(BscEntityType.OBJETIVO)) {
		    imgTree = (Integer) node.getHashNodeProperty("STATUS_DATAALVO");
		    if (imgTree == null) {
			imgTree = KpiImageResources.KPI_IMG_SCORECARD;
		    } else {
			//Alterando a figura que representa o status do Objetivos.
			if (imgTree == KpiImageResources.KPI_IMG_STATUS_BLUE) {
			    imgTree = KpiImageResources.KPI_IMG_OBJETIVOBLUE;
			} else if (imgTree == KpiImageResources.KPI_IMG_STATUS_GREEN) {
			    imgTree = KpiImageResources.KPI_IMG_OBJETIVOGREEN;
			} else if (imgTree == KpiImageResources.KPI_IMG_STATUS_YELLOW) {
			    imgTree = KpiImageResources.KPI_IMG_OBJETIVOYELLOW;
			} else if (imgTree == KpiImageResources.KPI_IMG_STATUS_RED) {
			    imgTree = KpiImageResources.KPI_IMG_OBJETIVORED;
			} else {
			    imgTree = KpiImageResources.KPI_IMG_OBJETIVOGRAY;
			}
		    }
		} else {
		    imgTree = bscScorecardType.getEntityIcon();
		}

		//Verificando se o no deve estar habilitado.
		if (node.isEnable()) {
		    setEnabled(true);
		} else {
		    setEnabled(false);
		}
	    }
	}

	setOpenIcon(myImageResources.getImage(imgTree));
	setLeafIcon(myImageResources.getImage(imgTree));
	setIcon(myImageResources.getImage(imgTree));
	setClosedIcon(myImageResources.getImage(imgTree));

	setBackground(null);
	setBackgroundNonSelectionColor(null);
	setOpaque(false);
    }
}

// Render para a coluna de somat�ria.
// Render para a coluna de imagem.
class KpiScoreCardingTableRenderer extends javax.swing.table.DefaultTableCellRenderer {

    private final String VARSTR = "Varia��o";
    private kpi.beans.JBIXMLTable biTable = null;
    private java.awt.Font fonteType = new java.awt.Font("Tahoma", 0, 11);
    private java.awt.Color corSelecao = new java.awt.Color(228, 228, 228);
    private java.awt.Color corBranco = new java.awt.Color(255, 255, 255);
    private java.awt.Color statusAzul = new java.awt.Color(51, 153, 255);
    private java.awt.Color statusVermelho = new java.awt.Color(234, 140, 136);
    private java.awt.Color statusAmarelo = new java.awt.Color(255, 235, 155);
    private java.awt.Color statusVerde = new java.awt.Color(139, 191, 150);
    private java.awt.Color statusCinza = new java.awt.Color(228, 228, 228);
    private java.util.HashMap buffLinha = new java.util.HashMap();
    private java.util.Locale locale = kpi.core.KpiStaticReferences.getKpiDefaultLocale();
    private pv.jfcx.JPVEdit oRender = new pv.jfcx.JPVEdit();
    private java.text.NumberFormat oValor = null;
    private boolean drillEnabled = false;

    public KpiScoreCardingTableRenderer(kpi.beans.JBIXMLTable table, kpi.xml.BIXMLRecord xmlRecord) {
	biTable = table;
    }

    public void resetRendererBuffer() {
	buffLinha.clear();
    }

    // Sobreposicao para mudar o comportamento de pintura da celula.
    @Override
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

	//long inicio = System.currentTimeMillis();
	//System.out.println("Inicio do Render"+ (System.currentTimeMillis() - inicio) + "ms");
	BIXMLRecord recLinha = null;
	int linha = biTable.getOriginalRow(row);

	/*
	 *Preparando o objeto para renderiza��o.
	 */
	oRender.setBackground(corBranco);
	oRender.setForeground(java.awt.Color.BLACK);
	oRender.setAlignment(0);
	oRender.setFontStyle(pv.jfcx.JPVEdit.NORMAL);
	oRender.setLocale(locale);
	oRender.setLAF(false);
	oRender.setBorderStyle(0);
	oRender.setFont(fonteType);
	oRender.setText("");
	oRender.setToolTipText("");


	if (biTable != null) {
	    column = biTable.getColumn(column).getModelIndex();

	    if (buffLinha.containsKey(linha)) {
		recLinha = (BIXMLRecord) buffLinha.get(linha);
	    } else {
		recLinha = biTable.getXMLData(false).get(linha);
		buffLinha.put(linha, recLinha);
	    }

	    String tipoColuna = biTable.getColumnType(column);
	    int colTipo = Integer.parseInt(tipoColuna);
	    if (colTipo == JBIXMLTable.KPI_FLOAT && !recLinha.getBoolean("IND_ESTRAT")) {
		renderNumber(oRender, Double.parseDouble(value.toString()), column, row, recLinha);
	    } else if (colTipo == JBIXMLTable.KPI_STRING) {
		renderString(oRender, value.toString(), column, row, recLinha);
	    }
	}

	String colName = biTable.getColumnTag(column);
	if (!colName.startsWith("VARIA")) {
	    if (hasFocus) {
		oRender.setBackground(corSelecao);

	    } else if (table.getSelectedRow() == row) {
		oRender.setBackground(table.getSelectionBackground());
		oRender.setForeground(table.getSelectionForeground());
	    }

	    if (colName.equals("UNIDADE")) {
		oRender.setAlignment(1);
	    }
	}

	//Indica o objetivo estrategico.
	if (recLinha.getBoolean("IND_ESTRAT") && !colName.equals("INDICADOR")) {
	    oRender.setBackground(corSelecao);
	}

//	System.out.println("Fim da Render"+ (System.currentTimeMillis() - inicio) + "ms");

	return oRender;
    }

    private pv.jfcx.JPVEdit renderNumber(pv.jfcx.JPVEdit oRender, Double dbValor, int column, int row, BIXMLRecord recLinha) {
	int iDecimais = 2;

	//Captura quantidade de casas decimais
	if (recLinha.contains("DECIMAIS")) {
	    iDecimais = recLinha.getInt("DECIMAIS");
	}

	oValor = java.text.NumberFormat.getInstance();
	oValor.setMinimumFractionDigits(iDecimais);
	oValor.setMaximumFractionDigits(iDecimais);
	oRender.setValue(oValor.format(dbValor));
	oRender.setAlignment(2);

	return oRender;
    }

    private pv.jfcx.JPVEdit renderString(JPVEdit oRender, String valor, int column, int row, BIXMLRecord recLinha) {
	String colName = biTable.getColumnTag(column);
	oRender.setValue(valor);

	//Tratamento das colunas de varia��es
	if (colName.startsWith("VARIA")) {
	    int status = 0;
	    String varToolTip = "";
	    String varValue = "";

	    if (colName.equals("VARIACAO")) {
		varValue = recLinha.getString("VARIACAO");
		varToolTip = recLinha.getString("VAR_HINT");
		status = recLinha.getInt("STATUS");
	    } else if (colName.equals("VARIA_ACUMULADA")) {
		varValue = recLinha.getString("VARIA_ACUMULADA");
		varToolTip = recLinha.getString("VARIA_ACUMUHINT");
		status = recLinha.getInt("STATUS_ACU");
	    }

	    StringBuilder toolTip = new StringBuilder();
	    toolTip.append("<html><table><tr><td align='center'>");
	    toolTip.append(VARSTR);
	    toolTip.append("<br>");
	    toolTip.append(varToolTip);
	    toolTip.append("</td></tr></table></html>");

	    oRender.setToolTipText(varToolTip);
	    oRender.setValue(varValue);
	    oRender.setAlignment(2);

	    switch (status) {
		case KpiScoreCarding.STATUS_BLUE:
		    oRender.setBackground(statusAzul);
		    break;
		case KpiScoreCarding.STATUS_RED:
		    oRender.setBackground(statusVermelho);
		    break;
		case KpiScoreCarding.STATUS_YELLOW:
		    oRender.setBackground(statusAmarelo);
		    break;
		case KpiScoreCarding.STATUS_GREEN:
		    oRender.setBackground(statusVerde);
		    break;
		case KpiScoreCarding.ESTAVEL_GRAY:
		    oRender.setBackground(statusCinza);
		    break;
	    }
	    //Nome do indicador
	} else if (column == KpiScoreCarding.COL_INDICADOR) {
	    if (isDrillEnabled()) {
		BIXMLAttributes attLine = recLinha.getAttributes();
		StringBuilder tmpVlr = new StringBuilder("");
		String drillText = " ";

		//Adicionando o sinal de Drill + ou -
		if (recLinha.getBoolean("DRILL")) {

		    oRender.setForeground(Color.blue);
		    oRender.setToolTipText(ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00009"));

		    if (attLine.contains("DRILL_EXPANDED") && attLine.getBoolean("DRILL_EXPANDED")) {
			drillText = "[-] ";
		    } else {
			drillText = "[+] ";
		    }
		}

		//Adicionando nivel ao Drill
		if (attLine.contains("DRILL_LEVEL")) {
		    int line_level = attLine.getInt("DRILL_LEVEL") * 3;
		    for (int i = 0; i < line_level; i++) {
			tmpVlr.append(" ");
		    }
		}

		//Corrigindo o texto
		tmpVlr.append(drillText);
		tmpVlr.append(valor);
		oRender.setValue(tmpVlr);

	    } else {
		if (recLinha.getBoolean("DRILL")) {
		    oRender.setFontStyle(JPVEdit.UNDERLINE);
		    oRender.setForeground(Color.blue);
		    oRender.setToolTipText(ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00009"));
		}
	    }
	    //Formata a c�lula do indicador para inclus�o de tool tip com a descri�ao do indicador.
	} else if (recLinha.getBoolean("IND_ESTRAT")) {
	    oRender.setToolTipText(recLinha.getString("DESCRICAO"));
	    //Formata a c�lula do indicador para identifica��o de indicador consolidador.
	} else if (recLinha.getBoolean("ISCONSOLID") && column == KpiScoreCarding.COL_INDICADOR) {
	    oRender.setBackground(new Color(255, 255, 204));
	    oRender.setFont(new Font("Tahoma", Font.BOLD, 11));
	    oRender.setToolTipText(ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00061"));
	}

	return oRender;
    }

    /**
     * @return the drillEnabled
     */
    public boolean isDrillEnabled() {
	return drillEnabled;
    }

    /**
     * @param drillEnabled the drillEnabled to set
     */
    public void setDrillEnabled(boolean drillEnabled) {
	this.drillEnabled = drillEnabled;
    }
}

// Render para a coluna de imagem.
class KpiScoreCardingTableImageRenderer extends javax.swing.table.DefaultTableCellRenderer {

    private kpi.beans.JBIXMLTable biTable = null;
    private kpi.xml.BIXMLRecord record = null;
    private java.util.Locale locale = kpi.core.KpiStaticReferences.getKpiDefaultLocale();
    private javax.swing.JLabel oRender = new javax.swing.JLabel();
    private java.awt.Color corBranco = new java.awt.Color(255, 255, 255);
    private java.awt.Color corSelecao = new java.awt.Color(228, 228, 228);
    public final static String KPI_TOOLTIP_VAZIO            = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00043");
    public final static String KPI_TOOLTIP_PLANOACAO        = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00044");
    public final static String KPI_TOOLTIP_EXCLAMACAO       = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00045");
    public final static String KPI_TOOLTIP_ESTAVEL_GRAY     = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00046");
    public final static String KPI_TOOLTIP_TEND_GREENDN     = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00047");
    public final static String KPI_TOOLTIP_TEND_GREENUP     = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00048");
    public final static String KPI_TOOLTIP_ESTAVEL_GREEN    = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00049");
    public final static String KPI_TOOLTIP_TEND_REDDN       = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00050");
    public final static String KPI_TOOLTIP_TEND_REDUP       = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00051");
    public final static String KPI_TOOLTIP_ESTAVEL_RED      = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00052");
    public final static String KPI_TOOLTIP_TEND_YELLOWDN    = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00053");
    public final static String KPI_TOOLTIP_TEND_YELLOWUP    = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00054");
    public final static String KPI_TOOLTIP_ESTAVEL_YELLOW   = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00055");
    public final static String KPI_TOOLTIP_TEND_BLUEDN      = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00062");
    public final static String KPI_TOOLTIP_TEND_BLUEUP      = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00063");
    public final static String KPI_TOOLTIP_ESTAVEL_BLUE     = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00064");
    private final static KpiCustomLabels customLabels	    = KpiStaticReferences.getKpiCustomLabels();
    
    public KpiScoreCardingTableImageRenderer(kpi.beans.JBIXMLTable table, kpi.xml.BIXMLRecord xmlRecord) {
	biTable = table;
	record = xmlRecord;
    }

    // Sobreposicao para mudar o comportamento de pintura da celula.
    @Override
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	column = biTable.getColumn(column).getModelIndex();
	String colName = biTable.getColumnTag(column);
	BIXMLRecord recLinha = null;
	int linha = biTable.getOriginalRow(row);


	oRender.setBackground(corBranco);
	oRender.setForeground(java.awt.Color.BLACK);
	oRender.setLocale(locale);
	oRender.setText("");
	oRender.setOpaque(true);
	oRender.setHorizontalAlignment(javax.swing.JLabel.HORIZONTAL);
	oRender.setIcon((javax.swing.ImageIcon) value);
	oRender.setToolTipText("nullo");

	if (hasFocus) {
	    oRender.setBackground(corSelecao);
	} else if (table.getSelectedRow() == row) {
	    oRender.setBackground(table.getSelectionBackground());
	    oRender.setForeground(table.getSelectionForeground());
	}

	if (biTable != null) {
	    recLinha = biTable.getXMLData(false).get(linha);


	    if (colName.equals("ACAO")) {
		switch (recLinha.getInt("ACAO")) {
		    case kpi.core.KpiImageResources.KPI_IMG_VAZIO:
			//"Indicador atingiu a meta e n�o existe plano de a��o cadastrado"
			oRender.setToolTipText(KPI_TOOLTIP_VAZIO);
			break;
		    case kpi.core.KpiImageResources.KPI_IMG_PLANOACAO:
			//"Existe plano de a��o cadastrado para o indicador"
			oRender.setToolTipText(KPI_TOOLTIP_PLANOACAO);
			break;
		    case kpi.core.KpiImageResources.KPI_IMG_EXCLAMACAO:
			//"Indicador n�o atingiu a meta e n�o existe plano de a��o cadastrado"
			oRender.setToolTipText(KPI_TOOLTIP_EXCLAMACAO);
			break;
		}
	    } else if (colName.equals("TENDENCIA")) {
		switch (recLinha.getInt("TENDENCIA")) {
		    case kpi.core.KpiImageResources.KPI_IMG_ESTAVEL_GRAY:
			//"N�o existem valores para efetuar a compara��o"
			oRender.setToolTipText(KPI_TOOLTIP_ESTAVEL_GRAY);
			break;
		    case kpi.core.KpiImageResources.KPI_IMG_TEND_GREENDN:
			//"Atingiu a meta com valor menor que o per�odo anterior"
			oRender.setToolTipText(KPI_TOOLTIP_TEND_GREENDN);
			break;
		    case kpi.core.KpiImageResources.KPI_IMG_TEND_GREENUP:
			//"Atingiu a meta com valor maior que o per�odo anterior"
			oRender.setToolTipText(KPI_TOOLTIP_TEND_GREENUP);
			break;
		    case kpi.core.KpiImageResources.KPI_IMG_ESTAVEL_GREEN:
			//"Atingiu a meta e manteve o mesmo resultado do per�odo anterior"
			oRender.setToolTipText(KPI_TOOLTIP_ESTAVEL_GREEN);
			break;
		    case kpi.core.KpiImageResources.KPI_IMG_TEND_REDDN:
			//"N�o atingiu a meta e o resultado alcan�ado foi menor que o per�odo anterior"
			oRender.setToolTipText(KPI_TOOLTIP_TEND_REDDN);
			break;
		    case kpi.core.KpiImageResources.KPI_IMG_TEND_REDUP:
			//"N�o atingiu a meta, mas o resultado alcan�ado foi maior que o per�odo anterior"
			oRender.setToolTipText(KPI_TOOLTIP_TEND_REDUP);
			break;
		    case kpi.core.KpiImageResources.KPI_IMG_ESTAVEL_RED:
			//"N�o atingiu a meta e manteve o mesmo resultado do per�odo anterior"
			oRender.setToolTipText(KPI_TOOLTIP_ESTAVEL_RED);
			break;
		    case kpi.core.KpiImageResources.KPI_IMG_TEND_YELLOWDN:
			//"Atingiu a meta na toler�ncia com valor menor que o per�odo anterior"
			oRender.setToolTipText(KPI_TOOLTIP_TEND_YELLOWDN);
			break;
		    case kpi.core.KpiImageResources.KPI_IMG_TEND_YELLOWUP:
			//"Atingiu a meta na toler�ncia com valor maior que o per�odo anterior"
			oRender.setToolTipText(KPI_TOOLTIP_TEND_YELLOWUP);
			break;
		    case kpi.core.KpiImageResources.KPI_IMG_ESTAVEL_YELLOW:
			//"Atingiu a meta na toler�ncia e manteve o mesmo resultado do per�odo anterior"
			oRender.setToolTipText(KPI_TOOLTIP_ESTAVEL_YELLOW);
			break;
                    case kpi.core.KpiImageResources.KPI_IMG_TEND_BLUEDN:
                        //"Superou a meta com valor menor que o per�odo anterior"
                        oRender.setToolTipText(KPI_TOOLTIP_TEND_BLUEDN);
                        break;
                    case kpi.core.KpiImageResources.KPI_IMG_TEND_BLUEUP:
                        //"Superou a meta com valor maior que o per�odo anterior"
                        oRender.setToolTipText(KPI_TOOLTIP_TEND_BLUEUP);
                        break;
                    case kpi.core.KpiImageResources.KPI_IMG_ESTAVEL_BLUE:
                        //"Superou a meta e manteve o mesmo resultado do per�odo anterior"
                        oRender.setToolTipText(KPI_TOOLTIP_ESTAVEL_BLUE);
                        break;
		    case kpi.core.KpiImageResources.KPI_IMG_VAZIO:
			oRender.setToolTipText("");
			break;

		}
	    } else if (colName.equals("NOTA")) {

		switch (recLinha.getInt("NOTA")) {
		    case kpi.core.KpiImageResources.KPI_IMG_NOTA_AZUL:
			oRender.setToolTipText("Anota��o para o indicador e ".concat(customLabels.getSco(KpiStaticReferences.CAD_OBJETIVO)));
			break;
		    case kpi.core.KpiImageResources.KPI_IMG_NOTA_VERDE:
			oRender.setToolTipText("Anota��o somente para o ".concat(customLabels.getSco(KpiStaticReferences.CAD_OBJETIVO)));
			break;
		    case kpi.core.KpiImageResources.KPI_IMG_NOTA:
			oRender.setToolTipText("Anota��o somente para o indicador");
			break;
		    case kpi.core.KpiImageResources.KPI_IMG_VAZIO:
			oRender.setToolTipText("N�o existe anota��o para este item.");
			break;
		}
	    }
	}

	return oRender;
    }
}

//Matem as configura��es da largura das colunas ap�s o refresh
//Lucio Pelinson 20/08/08
class KpiScoreCardingUserConfig {

    private int[] position;

    public KpiScoreCardingUserConfig() {
	position = new int[13];
	position[0] = 55;
	position[1] = 250;
	position[2] = 31;
	position[3] = 31;
	position[4] = 85;
	position[5] = 100;
	position[6] = 100;
	position[7] = 100;
	position[8] = 100;
	position[9] = 100;
	position[10] = 100;
	position[11] = 100;
	position[12] = 100;
    }

    public int getWidth(int pos) {
	return position[pos];
    }

    public void setWidth(int pos, int value) {
	position[pos] = value;
    }

    public void setAllWidth(String values) {
	values = values.trim();
	if (values.length() > 0) {
	    String[] nPos = values.split("\\|");

	    for (int i = 0; nPos.length > i; i++) {
		position[i] = Integer.parseInt(nPos[i]);
	    }
	}
    }
}
