package kpi.swing.analisys.map;

import java.awt.Component;
import java.util.Calendar;
import javax.swing.JPopupMenu;

/**
 *
 * @author gilmar.pereira
 */

public interface IKpiMapaMainForm {

    public void clearMap();

    public void addMapComponent(KpiMapaPerspectivaRenderer element);

    public void removeMapComponent(Component element);

    public void revalidateMap();

    public void drillObjective(String sourceId);

    public void toggleMaximize();

    public void setDataAlvo(Calendar calendar);
    
    public void setDataAcumDe(Calendar calendar);
    
    public void setDataAcumAte(Calendar calendar);

    public JPopupMenu getPopupMenu(KpiMapaObjetivo objective);

    public JPopupMenu getPopupMenu(KpiMapaGrupo group);

    public JPopupMenu getPopupMenu(KpiMapaPerspectiva perspective);

    public JPopupMenu getPopupMenu(KpiMapaLigacao link);
}
