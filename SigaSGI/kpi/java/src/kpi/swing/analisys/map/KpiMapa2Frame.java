package kpi.swing.analisys.map;

import kpi.beans.JBILayeredPane;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiFileChooser;
import kpi.xml.BIXMLRecord;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.print.Printable;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.lang.model.element.Element;
import javax.swing.*;
import kpi.core.IteMnuPermission;
import kpi.core.IteMnuPermission.Permissao;
import kpi.core.IteMnuPermission.TipoPermissao;
import kpi.core.KpiMenuController;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameFunctions;
import kpi.swing.tools.KpiUpload;

/**
 *
 * @author gilmar.pereira
 */
public class KpiMapa2Frame extends JInternalFrame implements KpiDefaultFrameFunctions, Printable, IKpiMapaMainForm {

    private String recordtype = "MAPAESTRATEGICO2";
    private boolean isMaximize = false;
    private int status;
    private KpiMapaMaximize maximizeForm;
    private KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private String id = new String();
    private BIXMLRecord record = null;
    private KpiMapaFormController mapFormController;
    private KpiMapaGlass glassPanel;
    private Hashtable temas = new Hashtable();
    private Calendar dataAcumDe;
    private Calendar dataAcumAte;
    private Boolean excluiImagem = false;

    public KpiMapa2Frame(int operation, String idAux, String contextId) {
        initComponents();

        putClientProperty("MAXI", true); //NOI18N
        

        glassPanel = new KpiMapaGlass();
        glassPanel.setLayout(null);
        glassPanel.setOpaque(false);
        mapLayeredPanel.add(glassPanel, javax.swing.JLayeredPane.MODAL_LAYER);

        mapFormController = new KpiMapaFormController(glassPanel, this);

        fldDataAlvo.setCalendar(new GregorianCalendar());

        //regra de seguran�a
        KpiMenuController mnuControl = KpiStaticReferences.getMnuController();
        IteMnuPermission perm = mnuControl.getRuleByName("MAPAESTRATEGICO");

        if (perm.getPermissionValue(TipoPermissao.MANUTENCAO) == Permissao.PERMITIDO) {
            btnEdit.setEnabled(true);
        } else {
            btnEdit.setEnabled(false);
        }
        try {
            Thread.sleep(400);
        } catch (InterruptedException ex) {
            Logger.getLogger(KpiMapa2Frame.class.getName()).log(Level.SEVERE, null, ex);
        }
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
        lblTemaEstrategico.setEnabled(false);
        cboTemaEstrategico.setEnabled(false);
    }

    @Override
    public void disableFields() {
        lblTemaEstrategico.setEnabled(true);
        cboTemaEstrategico.setEnabled(true);
    }

    @Override
    public void refreshFields() {
    }

    @Override
    public BIXMLRecord getFieldsContents() {
        return mapFormController.mapToXml();
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    @Override
    public void toggleMaximize() {
        if (!isMaximize) {
            btnMaximize.setText(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00001"));
            btnMaximize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_minimizar.gif")));
            isMaximize = true;
            maximizeForm = new KpiMapaMaximize(kpi.core.KpiStaticReferences.getKpiMainPanel(), true, pnlMainPanel);
            maximizeForm.setMapaFrame(this);
            maximizeForm.setVisible(true);

            invalidate();
            revalidate();
            doLayout();
        } else {
            btnMaximize.setText(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00002"));
            btnMaximize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_maximizar.gif")));
            isMaximize = false;
            getContentPane().add(maximizeForm.pnlMapa, java.awt.BorderLayout.CENTER);
            maximizeForm.setVisible(false);
            maximizeForm.dispose();
            requestFocus();

            invalidate();
            revalidate();
            doLayout();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grpEditMapa = new javax.swing.ButtonGroup();
        pnlMainPanel = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        dataPanelCenter = new javax.swing.JPanel();
        lblDataAlvo = new javax.swing.JLabel();
        scrDataAlvo = new javax.swing.JScrollPane();
        fldDataAlvo = new pv.jfcx.JPVDatePlus();
        lblTemaEstrategico = new javax.swing.JLabel();
        cboTemaEstrategico = new javax.swing.JComboBox();
        dataPanelTop = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnPrint = new javax.swing.JButton();
        btnExportar = new javax.swing.JButton();
        btnMaximize = new javax.swing.JButton();
        mapScrollPanel = new javax.swing.JScrollPane();
        mapLayeredPanel = new kpi.beans.JBILayeredPane();
        mapPanel = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international"); // NOI18N
        setTitle(bundle.getString("KpiMapaFrame_00003")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_mapa.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlMainPanel.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 80));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 60));
        pnlTools.setLayout(new java.awt.BorderLayout());

        dataPanelCenter.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        dataPanelCenter.setPreferredSize(new java.awt.Dimension(10, 23));
        dataPanelCenter.setLayout(null);

        lblDataAlvo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataAlvo.setText(bundle.getString("KpiMapaFrame_00004")); // NOI18N
        lblDataAlvo.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDataAlvo.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDataAlvo.setPreferredSize(new java.awt.Dimension(100, 16));
        dataPanelCenter.add(lblDataAlvo);
        lblDataAlvo.setBounds(10, 5, 60, 20);

        scrDataAlvo.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrDataAlvo.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        fldDataAlvo.setAllowNull(false);
        fldDataAlvo.setBorderStyle(0);
        fldDataAlvo.setButtonBorder(0);
        fldDataAlvo.setDialog(true);
        fldDataAlvo.setFont(new java.awt.Font("Tahoma", 1, 11));
        fldDataAlvo.setUseLocale(true);
        fldDataAlvo.setWaitForCalendarDate(true);
        fldDataAlvo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fldDataAlvoActionPerformed(evt);
            }
        });
        scrDataAlvo.setViewportView(fldDataAlvo);

        dataPanelCenter.add(scrDataAlvo);
        scrDataAlvo.setBounds(75, 5, 100, 20);

        lblTemaEstrategico.setText(bundle.getString("KpiMapaFrame_00030")); // NOI18N
        dataPanelCenter.add(lblTemaEstrategico);
        lblTemaEstrategico.setBounds(210, 4, 100, 20);

        cboTemaEstrategico.setMaximumSize(new java.awt.Dimension(450, 20));
        cboTemaEstrategico.setMinimumSize(new java.awt.Dimension(450, 20));
        cboTemaEstrategico.setPreferredSize(new java.awt.Dimension(450, 20));
        cboTemaEstrategico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTemaEstrategicoActionPerformed(evt);
            }
        });
        dataPanelCenter.add(cboTemaEstrategico);
        cboTemaEstrategico.setBounds(310, 5, 450, 20);

        pnlTools.add(dataPanelCenter, java.awt.BorderLayout.CENTER);

        dataPanelTop.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        dataPanelTop.setPreferredSize(new java.awt.Dimension(10, 23));
        dataPanelTop.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 30));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(130, 32));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("KpiMapaFrame_00005")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("KpiMapaFrame_00006")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        dataPanelTop.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(500, 30));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(500, 30));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("KpiMapaFrame_00012")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("KpiMapaFrame_00013")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_print.gif"))); // NOI18N
        btnPrint.setText(bundle.getString("KpiMapaFrame_00014")); // NOI18N
        btnPrint.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnPrint.setMaximumSize(new java.awt.Dimension(76, 20));
        btnPrint.setMinimumSize(new java.awt.Dimension(76, 20));
        btnPrint.setPreferredSize(new java.awt.Dimension(76, 20));
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });
        tbInsertion.add(btnPrint);

        btnExportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_exportacao.gif"))); // NOI18N
        btnExportar.setText(bundle.getString("KpiMapaFrame_00015")); // NOI18N
        btnExportar.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnExportar.setMaximumSize(new java.awt.Dimension(76, 20));
        btnExportar.setMinimumSize(new java.awt.Dimension(76, 20));
        btnExportar.setPreferredSize(new java.awt.Dimension(76, 20));
        btnExportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarActionPerformed(evt);
            }
        });
        tbInsertion.add(btnExportar);

        btnMaximize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_maximizar.gif"))); // NOI18N
        btnMaximize.setText(bundle.getString("KpiMapaFrame_00002")); // NOI18N
        btnMaximize.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnMaximize.setMaximumSize(new java.awt.Dimension(76, 20));
        btnMaximize.setMinimumSize(new java.awt.Dimension(76, 20));
        btnMaximize.setPreferredSize(new java.awt.Dimension(76, 20));
        btnMaximize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMaximizeActionPerformed(evt);
            }
        });
        tbInsertion.add(btnMaximize);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        dataPanelTop.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlTools.add(dataPanelTop, java.awt.BorderLayout.NORTH);

        pnlMainPanel.add(pnlTools, java.awt.BorderLayout.NORTH);

        mapScrollPanel.setFocusCycleRoot(true);

        mapLayeredPanel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        mapLayeredPanel.setPreferredSize(new java.awt.Dimension(1000, 1000));

        mapPanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        mapPanel.setOpaque(false);
        mapPanel.setPreferredSize(new java.awt.Dimension(948, 1000));
        mapPanel.setRequestFocusEnabled(false);
        mapPanel.setVerifyInputWhenFocusTarget(false);
        mapPanel.setLayout(new javax.swing.BoxLayout(mapPanel, javax.swing.BoxLayout.Y_AXIS));
        mapPanel.setBounds(0, 0, 948, 0);
        mapLayeredPanel.add(mapPanel, javax.swing.JLayeredPane.DEFAULT_LAYER);

        mapScrollPanel.setViewportView(mapLayeredPanel);

        pnlMainPanel.add(mapScrollPanel, java.awt.BorderLayout.CENTER);

        getContentPane().add(pnlMainPanel, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnMaximizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMaximizeActionPerformed
        toggleMaximize();
    }//GEN-LAST:event_btnMaximizeActionPerformed

    private void btnExportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportarActionPerformed
        kpi.swing.KpiDefaultDialogSystem dialogSystem;

        boolean maxState = isMaximum();
        try {
            setMaximum(true);
        } catch (Exception e) {
        }

        invalidate();
        revalidate();
        doLayout();

        if (isMaximize && maximizeForm != null) {
            dialogSystem = new kpi.swing.KpiDefaultDialogSystem(maximizeForm);
        } else {
            dialogSystem = new kpi.swing.KpiDefaultDialogSystem(this);
        }

        //Mensagem necess�ria para fazer o "split" do painel.
        if (dialogSystem.confirmMessage(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00016"))
                == javax.swing.JOptionPane.YES_OPTION) {
            kpi.swing.KpiFileChooser frmChooser = new kpi.swing.KpiFileChooser(kpi.core.KpiStaticReferences.getKpiMainPanel(), true);
            frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_SAVE);
            frmChooser.setFileType("*.jpg");
            frmChooser.loadServerFiles("mapest\\*.jpg");
            frmChooser.setFileName("mapa" + mapFormController.getMap().getId() + ".jpg");
            frmChooser.setVisible(true);
            if (frmChooser.isValidFile()) {
                saveComponentAsJPEG("mapest\\" + frmChooser.getFileName());
            }
        }

        try {
            setMaximum(maxState);
        } catch (Exception e) {
        }

    }//GEN-LAST:event_btnExportarActionPerformed

	private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
            kpi.swing.KpiDefaultDialogSystem dialogSystem;
            java.awt.print.PrinterJob printJob = java.awt.print.PrinterJob.getPrinterJob();

            boolean maxState = isMaximum();
            try {
                setMaximum(true);
            } catch (Exception e) {
            }

            if (isMaximize && maximizeForm != null) {
                dialogSystem = new kpi.swing.KpiDefaultDialogSystem(maximizeForm);
            } else {
                dialogSystem = new kpi.swing.KpiDefaultDialogSystem(this);
            }

            //Mensagem necess�ria para fazer o "split" do painel.
            if (dialogSystem.confirmMessage(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00017")) == javax.swing.JOptionPane.YES_OPTION) {

                printJob.setPrintable(this);
                if (printJob.printDialog()) {
                    try {
                        printJob.print();
                    } catch (java.awt.print.PrinterException pe) {
                        System.out.println("Error printing: " + pe);
                    }
                }
            }

            try {
                setMaximum(maxState);
            } catch (Exception e) {
            }

	}//GEN-LAST:event_btnPrintActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            
            if (excluiImagem){
                StringBuilder path = new StringBuilder();
                
                Object[] perspective  = mapFormController.getMap().getPerspectives().keySet().toArray(); 
                String idPerspective  = perspective[0].toString();
                KpiMapaPerspectiva x = mapFormController.getMap().getPerspective(idPerspective.trim());
                String imagem = x.getMapImage();
                
                path.append("DELFILE|");
                if (KpiStaticReferences.getKpiApplet() != null) {
                    path.append("\\mapimagens\\");
                    imagem = imagem.replace("/", "\\");
                    path.append(imagem);
                } else {
                    path.append("\\mapimagens\\");
                    imagem = imagem.replace("/", "\\");
                    path.append(imagem);
                }
                
                event.executeRecord(path.toString());
            }
 
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            if (excluiImagem){
                Object[] perspective  = mapFormController.getMap().getPerspectives().keySet().toArray(); 
                String idPerspective  = perspective[0].toString();

                KpiMapaPerspectiva x = mapFormController.getMap().getPerspective(idPerspective.trim());

                x.setImage(x.getImage());
                excluiImagem = false;
            }
            loadRecord();
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            cboTemaEstrategico.setSelectedIndex(0);
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

    private void fldDataAlvoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fldDataAlvoActionPerformed
        mapFormController.refreshElements(fldDataAlvo.getCalendar(), event.getComboValue(temas, cboTemaEstrategico));
    }//GEN-LAST:event_fldDataAlvoActionPerformed

    private void cboTemaEstrategicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTemaEstrategicoActionPerformed
        mapFormController.refreshElements(fldDataAlvo.getCalendar(), event.getComboValue(temas, cboTemaEstrategico));
    }//GEN-LAST:event_cboTemaEstrategicoActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnExportar;
    private javax.swing.JButton btnMaximize;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cboTemaEstrategico;
    private javax.swing.JPanel dataPanelCenter;
    private javax.swing.JPanel dataPanelTop;
    pv.jfcx.JPVDatePlus fldDataAlvo;
    private javax.swing.ButtonGroup grpEditMapa;
    private javax.swing.JLabel lblDataAlvo;
    private javax.swing.JLabel lblTemaEstrategico;
    private kpi.beans.JBILayeredPane mapLayeredPanel;
    private javax.swing.JPanel mapPanel;
    private javax.swing.JScrollPane mapScrollPanel;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlMainPanel;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JScrollPane scrDataAlvo;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setStatus(int value) {
        status = value;
        mapFormController.setStatus(value);
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordtype = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordtype);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(BIXMLRecord recordAux) {
        record = recordAux;

        if (record != null) {
            mapFormController.setToolAction(KpiMapaFormController.MapToolAction.MOVE);

            mapFormController.xmlToMap(record);
            try {
                //Mapa
                mapFormController.renderMap();
            } catch (MalformedURLException ex) {
                Logger.getLogger(KpiMapa2Frame.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(KpiMapa2Frame.class.getName()).log(Level.SEVERE, null, ex);
            }

            //Combo de Filtro de Temas
            event.populateCombo(mapFormController.getThemes(), "", temas, cboTemaEstrategico);
            //Atualiza status dos objetivos
            mapFormController.refreshElements(fldDataAlvo.getCalendar(), event.getComboValue(temas, cboTemaEstrategico));

        }
    }

    @Override
    public BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        return "";
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return null;
    }

    @Override
    public int print(java.awt.Graphics g, java.awt.print.PageFormat pageFormat, int pageIndex) throws java.awt.print.PrinterException {
        if (pageIndex > 0) {
            return (NO_SUCH_PAGE);
        } else {
            java.awt.Graphics2D g2d = (java.awt.Graphics2D) g;
            g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY() + 50);

            Font f = new Font(Font.SANS_SERIF, Font.PLAIN, 10);
            g2d.setFont(f);
            g2d.setColor(Color.BLACK);

            g2d.drawString(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00018") + mapFormController.getMap().getName(), 0, 15);
            g2d.scale(0.55, 0.55);
            disableDoubleBuffering(this);
            g2d.translate(0, 40);
            mapLayeredPanel.paint(g2d);
            enableDoubleBuffering(this);
            return (PAGE_EXISTS);
        }
    }

    /**
     * Grava o objeto passado com uma figura do tipo JPEG
     */
    public static void disableDoubleBuffering(Component c) {
        RepaintManager currentManager = RepaintManager.currentManager(c);
        currentManager.setDoubleBufferingEnabled(false);
    }

    public static void enableDoubleBuffering(Component c) {
        RepaintManager currentManager = RepaintManager.currentManager(c);
        currentManager.setDoubleBufferingEnabled(true);
    }

    private void saveComponentAsJPEG(String fileName) {
        kpi.util.KpiToolKit tool = new kpi.util.KpiToolKit();
        Dimension size = glassPanel.getSize();

        java.awt.image.BufferedImage myImage = new java.awt.image.BufferedImage(size.width, size.height + 20,
                java.awt.image.BufferedImage.TYPE_INT_RGB);

        Graphics2D g2 = myImage.createGraphics();

        java.awt.geom.RoundRectangle2D rect = new java.awt.geom.RoundRectangle2D.Double();

        rect.setFrame(0, 0, myImage.getWidth(), myImage.getHeight());

        g2.setPaint(Color.WHITE);
        g2.fill(rect);

        Font f = new Font(Font.SANS_SERIF, Font.PLAIN, 10);
        g2.setFont(f);
        g2.setColor(Color.BLACK);
        g2.drawString(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00018") + mapFormController.getMap().getName(), 0, 15);

        JBILayeredPane.disableDoubleBuffering(mapLayeredPanel);

        g2.translate(0, 20);

        mapLayeredPanel.paint(g2);

        JBILayeredPane.enableDoubleBuffering(mapLayeredPanel);

        try {
            tool.writeJPEGFile(myImage, fileName);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Override
    public String getFormType() {
        return recordtype;
    }

    @Override
    public String getParentType() {
        return null;
    }

    @Override
    public void clearMap() {
        mapPanel.removeAll();
    }

    @Override
    public void addMapComponent(KpiMapaPerspectivaRenderer element) {
        mapPanel.add(element);
    }

    @Override
    public void removeMapComponent(Component element) {
        mapPanel.remove(element);
    }

    @Override
    public void revalidateMap() {
        int totalHeight = 0;

        for (int i = 0; i < mapPanel.getComponentCount(); i++) {
            JPanel element = (JPanel) mapPanel.getComponent(i);
            totalHeight += element.getPreferredSize().height;
        }

        mapLayeredPanel.setPreferredSize(new Dimension(KpiMapaFormController.MAP_WIDTH, totalHeight));

        mapPanel.setBounds(0, 0, KpiMapaFormController.MAP_WIDTH, totalHeight);
        mapPanel.setPreferredSize(new Dimension(KpiMapaFormController.MAP_WIDTH, totalHeight));
        mapPanel.invalidate();
        mapPanel.revalidate();
        mapPanel.doLayout();

        glassPanel.setBounds(0, 0, KpiMapaFormController.MAP_WIDTH, totalHeight);
        glassPanel.setPreferredSize(new Dimension(KpiMapaFormController.MAP_WIDTH, totalHeight));
    }

    @Override
    public void drillObjective(String sourceId) {
        KpiMapaDrillObjetivo frmDestino = (KpiMapaDrillObjetivo) kpi.core.KpiStaticReferences.getKpiFormController().getForm("MAPADRILLOBJETIVO", sourceId, "");
        frmDestino.doDrill(fldDataAlvo.getCalendar(), dataAcumDe, dataAcumAte);
    }

    @Override
    public JPopupMenu getPopupMenu(KpiMapaObjetivo objective) {
        JPopupMenu popup = new JPopupMenu();
        JMenuItem item;
        KpiMapaMenuAction a;

        //Cor do texto
        item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00019"));
        a = new KpiMapaMenuAction(objective, mapFormController) {

            @Override
            public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                //Cor do texto
                KpiMapaObjetivo objective = (KpiMapaObjetivo) o;
                Color color = chooseColor(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00021"), objective.getFontColor());

                if (color != null) {
                    objective.setFontColor(color);
                    objective.setVisibleName(true);
                }
            }
        };
        item.addActionListener(a);
        popup.add(item);

        if (objective.isVisibleName()) {
            //Ocultar Texto
            item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00031"));
            a = new KpiMapaMenuAction(objective, mapFormController) {

                @Override
                public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                    KpiMapaObjetivo objective = (KpiMapaObjetivo) o;

                    objective.setVisibleName(false);
                }
            };
            item.addActionListener(a);
            popup.add(item);
        } else {
            //Reexibir Texto
            item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00036"));
            a = new KpiMapaMenuAction(objective, mapFormController) {

                @Override
                public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                    KpiMapaObjetivo objective = (KpiMapaObjetivo) o;

                    objective.setVisibleName(true);
                }
            };
            item.addActionListener(a);
            popup.add(item);
        }

        //Cor do fundo
        item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00020"));
        a = new KpiMapaMenuAction(objective, mapFormController) {

            @Override
            public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                //Cor do fundo
                KpiMapaObjetivo objective = (KpiMapaObjetivo) o;
                Color color = chooseColor(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00022"), objective.getBackColor());

                if (color != null) {
                    objective.setBackColor(color);
                    objective.setOpaque(true);
                }
            }
        };
        item.addActionListener(a);
        popup.add(item);

        if (objective.isOpaque()) {
            //Fundo transparente
            item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00032"));
            a = new KpiMapaMenuAction(objective, mapFormController) {

                @Override
                public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                    //Cor do fundo
                    KpiMapaObjetivo objective = (KpiMapaObjetivo) o;

                    objective.setOpaque(false);
                }
            };
            item.addActionListener(a);
            popup.add(item);
        } else {
            //Reexibir fundo
            item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00037"));
            a = new KpiMapaMenuAction(objective, mapFormController) {

                @Override
                public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                    //Cor do fundo
                    KpiMapaObjetivo objective = (KpiMapaObjetivo) o;

                    objective.setOpaque(true);
                }
            };
            item.addActionListener(a);
            popup.add(item);
        }

        //Shape
        if (objective.getShape().equals(KpiMapaObjetivo.SHAPE_RECT)) {
            item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00023"));
            a = new KpiMapaMenuAction(objective, mapFormController) {

                @Override
                public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                    //shape
                    KpiMapaObjetivo objective = (KpiMapaObjetivo) o;
                    objective.setShape(KpiMapaObjetivo.SHAPE_CIRCLE);
                }
            };

        } else {
            item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00024"));
            a = new KpiMapaMenuAction(objective, mapFormController) {

                @Override
                public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                    //shape
                    KpiMapaObjetivo objective = (KpiMapaObjetivo) o;
                    objective.setShape(KpiMapaObjetivo.SHAPE_RECT);
                }
            };
        }
        item.addActionListener(a);
        popup.add(item);

        return popup;
    }

    @Override
    public JPopupMenu getPopupMenu(KpiMapaGrupo group) {
        JPopupMenu popup = new JPopupMenu();
        JMenuItem item;
        KpiMapaMenuAction a;

        //T�tulo
        item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00025"));
        a = new KpiMapaMenuAction(group, mapFormController) {

            @Override
            public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                //T�tulo
                KpiMapaGrupo group = (KpiMapaGrupo) o;
                String name = chooseText(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00026"), group.getName());

                if (name != null && !name.equals(group.getName())) {
                    group.setName(name);
                }
            }
        };
        item.addActionListener(a);
        popup.add(item);

        //Cor do texto
        item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00019"));
        a = new KpiMapaMenuAction(group, mapFormController) {

            @Override
            public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                //Cor do texto
                KpiMapaGrupo group = (KpiMapaGrupo) o;
                Color color = chooseColor(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00021"), group.getFontColor());

                if (color != null) {
                    group.setFontColor(color);
                    group.setVisibleName(true);
                }
            }
        };
        item.addActionListener(a);
        popup.add(item);

        //Cor do fundo
        item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00020"));
        a = new KpiMapaMenuAction(group, mapFormController) {

            @Override
            public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                //Cor do fundo
                KpiMapaGrupo group = (KpiMapaGrupo) o;
                Color color = chooseColor(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00022"), group.getBackColor());

                if (color != null) {
                    group.setBackColor(color);
                    group.setOpaque(true);
                }
            }
        };
        item.addActionListener(a);
        popup.add(item);

        if (group.isOpaque()) {
            //Fundo transparente
            item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00032"));
            a = new KpiMapaMenuAction(group, mapFormController) {

                @Override
                public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                    KpiMapaGrupo group = (KpiMapaGrupo) o;

                    group.setOpaque(false);
                }
            };
            item.addActionListener(a);
            popup.add(item);
        }

        return popup;
    }

    @Override
    public JPopupMenu getPopupMenu(KpiMapaPerspectiva perspective) {
        JPopupMenu popup = new JPopupMenu();
        JMenuItem item;
        KpiMapaMenuAction a;

        //Cor do fundo
        item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00020"));
        a = new KpiMapaMenuAction(perspective, mapFormController) {

            @Override
            public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                //Cor do fundo
                KpiMapaPerspectiva perspective = (KpiMapaPerspectiva) o;
                Color color = chooseColor(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00022"), perspective.getBackColor());

                if (color != null) {
                    perspective.setBackColor(color);
                    perspective.setImage("");
                }
            }
        };
        item.addActionListener(a);
        popup.add(item);
                
        //Imagem de fundo
        item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00033"));
        a = new KpiMapaMenuAction(perspective, mapFormController) {

            @Override
            public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                //Imagem de fundo
                KpiMapaPerspectiva perspective = (KpiMapaPerspectiva) o;

                File imgFile = chooseImage();
                if (imgFile != null) {
                    c.uploadPerpesctiveImage(perspective, imgFile);
                }
            }
        };

        item.addActionListener(a);
        popup.add(item);
         
        if (!perspective.getImage().equals("")){
            //Retirar Imagem
            item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00038"));
            a = new KpiMapaMenuAction(perspective, mapFormController) {

                @Override
                public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                    //Imagem de fundo
                    KpiMapaPerspectiva perspective = (KpiMapaPerspectiva) o;
                    c.deletePerpesctiveImage(perspective);
                    excluiImagem = true;
                }
            };

            item.addActionListener(a);
            popup.add(item);
        }

        return popup;
    }

    @Override
    public JPopupMenu getPopupMenu(KpiMapaLigacao link) {
        JPopupMenu popup = new JPopupMenu();
        JMenuItem item;
        KpiMapaMenuAction a;

        //Formato da linha
        item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00027"));
        a = new KpiMapaMenuAction(link, mapFormController) {

            @Override
            public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                KpiMapaLigacao link = (KpiMapaLigacao) o;
                link.setType(KpiMapaLigacao.LINE);
            }
        };
        item.addActionListener(a);
        popup.add(item);

        //Cor do fundo
        item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00028"));
        a = new KpiMapaMenuAction(link, mapFormController) {

            @Override
            public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                //Cor do fundo
                KpiMapaLigacao link = (KpiMapaLigacao) o;
                Color color = chooseColor(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00029"), link.getLineColor());

                if (color != null && color != link.getLineColor()) {
                    link.setLineColor(color);
                }
            }
        };
        item.addActionListener(a);
        popup.add(item);

        return popup;
    }

    private Color chooseColor(String title, Color currentColor) {
        Component c;

        if (isMaximize && maximizeForm != null) {
            c = maximizeForm;
        } else {
            c = this;
        }

        return JColorChooser.showDialog(c, title, currentColor);
    }

    private String chooseText(String question, String currentValue) {
        Component c;

        if (isMaximize && maximizeForm != null) {
            c = maximizeForm;
        } else {
            c = this;
        }

        return JOptionPane.showInputDialog(c, question, currentValue);
    }

    private File chooseImage() {

        File imgFile = null;

        Component c;

        if (isMaximize && maximizeForm != null) {
            c = maximizeForm;
        } else {
            c = this;
        }

        try {
            javax.swing.JFileChooser chooser = new javax.swing.JFileChooser();
            chooser.setDialogType(javax.swing.JFileChooser.CUSTOM_DIALOG);
            chooser.setApproveButtonText(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00034"));
            chooser.setApproveButtonToolTipText(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00035"));
            chooser.setAcceptAllFileFilterUsed(false);
            chooser.addChoosableFileFilter(new kpi.util.kpiFileFilter(new String[]{"png", "gif", "jpg", "jpeg"}));

            int retval = chooser.showDialog(c, null);

            if (retval == javax.swing.JFileChooser.APPROVE_OPTION) {
                imgFile = chooser.getSelectedFile();
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return imgFile;
    }

    @Override
    public void setDataAlvo(Calendar data) {
        fldDataAlvo.setCalendar(data);
    }

    @Override
    public void setDataAcumDe(Calendar data) {
        dataAcumDe = data;
    }

    @Override
    public void setDataAcumAte(Calendar data) {
        dataAcumAte = data;
    }
    
}