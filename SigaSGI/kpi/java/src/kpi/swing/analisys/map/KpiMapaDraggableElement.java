package kpi.swing.analisys.map;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;
import javax.swing.event.MouseInputListener;
import kpi.swing.IKpiDragController;
import kpi.swing.IKpiMouseListener;
import kpi.swing.KpiMouseEvent;

/**
 *
 * @author gilmar.pereira
 */
public abstract class KpiMapaDraggableElement extends JPanel implements IKpiMouseListener, MouseInputListener {

    private String id;
    private IKpiDragController mouseController;
    private boolean selected = false;
    public final static int BORDERFACTOR_W = 1;
    public final static int BORDERFACTOR_H = 1;

    public void mouseClicked(MouseEvent e) {
	mouseController.kpiMouseClicked(KpiMouseEvent.createEvent(this, e));
    }

    public void mousePressed(MouseEvent e) {
	mouseController.kpiMousePressed(KpiMouseEvent.createEvent(this, e));
    }

    public void mouseReleased(MouseEvent e) {
	mouseController.kpiMouseReleased(KpiMouseEvent.createEvent(this, e));
    }

    public void mouseEntered(MouseEvent e) {
	mouseController.kpiMouseEntered(KpiMouseEvent.createEvent(this, e));
    }

    public void mouseExited(MouseEvent e) {
	mouseController.kpiMouseExited(KpiMouseEvent.createEvent(this, e));
    }

    public void mouseDragged(MouseEvent e) {
	mouseController.kpiMouseDragged(KpiMouseEvent.createEvent(this, e));
    }

    public void mouseMoved(MouseEvent e) {
	mouseController.kpiMouseMoved(KpiMouseEvent.createEvent(this, e));
    }

    /**
     * @return the mouseController
     */
    public IKpiDragController getMouseController() {
	return mouseController;
    }

    /**
     * @param mouseController the mouseController to set
     */
    public void setMouseController(IKpiDragController mouseController) {
	if (this.mouseController == null) {
	    this.mouseController = mouseController;
	    this.addMouseListener(this);
	    this.addMouseMotionListener(this);
	}
    }

    /**
     * @return the id
     */
    public String getId() {
	return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
	this.id = id;
    }

    /**
     * @return the selected
     */
    public boolean isSelected() {
	return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(boolean selected) {
	this.selected = selected;
    }

    public Point realToRelativeCoords(Point p1) {
	Point pNew = new Point(p1);

	Insets insets = this.getInsets();
	pNew.translate(-insets.left, -insets.top);

	return pNew;
    }

    public Rectangle realToRelativeCoords(Rectangle rect) {
	Rectangle rNew = new Rectangle(rect);

	Insets insets = this.getInsets();
	rNew.translate(-insets.left, -insets.top);

	return rNew;
    }

    public Point relativeToRealCoords(Point p1) {
	Point pNew = new Point(p1);

	Insets insets = this.getInsets();
	pNew.translate(insets.left, insets.top);

	return pNew;
    }

    public Rectangle relativeToRealCoords(Rectangle rect) {
	Rectangle rNew = new Rectangle(rect);

	Insets insets = this.getInsets();
	rNew.translate(insets.left, insets.top);


	return rNew;
    }

    public Point moveConstraintRelativeCoords(Point point) {
	Point pNew = new Point(point);
	Insets insets = this.getInsets();

	if (pNew.x > getWidth() - insets.left - insets.right - (BORDERFACTOR_W * 2)) {
	    pNew.x = getWidth() - insets.left - insets.right - (BORDERFACTOR_W * 2);
	}

	if (pNew.y > getHeight() - insets.top - insets.bottom - (BORDERFACTOR_H * 2)) {
	    pNew.y = getHeight() - insets.top - insets.bottom - (BORDERFACTOR_H * 2);
	}

	if (pNew.x < 0) {
	    pNew.x = 0;
	}

	if (pNew.y < 0) {
	    pNew.y = 0;
	}

	return pNew;
    }

    public Point moveConstraintRelativeCoords(Rectangle rect) {
	Point pNew = new Point(rect.getLocation());

	Insets insets = this.getInsets();

	if (pNew.x < 0) {
	    pNew.x = 0;
	}

	if (pNew.y < 0) {
	    pNew.y = 0;
	}

	if (pNew.x + rect.width > getWidth() - insets.left - insets.right - (BORDERFACTOR_W * 2)) {
	    pNew.x = getWidth() - insets.left - insets.right - rect.width - (BORDERFACTOR_W * 2);
	}

	if (pNew.y + rect.height > getHeight() - insets.top - insets.bottom - (BORDERFACTOR_H * 2)) {
	    pNew.y = getHeight() - insets.top - insets.bottom - rect.height - (BORDERFACTOR_H * 2);
	}

	return pNew;
    }

    public Point moveConstraintRealCoords(Point point) {
	Point pNew = new Point(point);

	Insets insets = this.getInsets();

	if (pNew.x > getWidth() - insets.right - (BORDERFACTOR_W * 2)) {
	    pNew.x = getWidth() - insets.right - (BORDERFACTOR_W * 2);
	}

	if (pNew.y > getHeight() - insets.bottom - (BORDERFACTOR_H * 2)) {
	    pNew.y = getHeight() - insets.bottom - (BORDERFACTOR_H * 2);
	}

	if (pNew.x < insets.left) {
	    pNew.x = insets.left;
	}

	if (pNew.y < insets.top) {
	    pNew.y = insets.top;
	}

	return pNew;
    }

    public Point moveConstraintRealCoords(Rectangle rect) {
	Point pNew = new Point(rect.getLocation());

	Insets insets = this.getInsets();

	if (pNew.x < insets.left) {
	    pNew.x = insets.left;
	}

	if (pNew.y < insets.top) {
	    pNew.y = insets.top;
	}

	if (pNew.x + rect.width > getWidth() - insets.right - (BORDERFACTOR_W * 2)) {
	    pNew.x = getWidth() - insets.right - rect.width - (BORDERFACTOR_W * 2);
	}

	if (pNew.y + rect.height > getHeight() - insets.bottom - (BORDERFACTOR_H * 2)) {
	    pNew.y = getHeight() - insets.bottom - rect.height - (BORDERFACTOR_H * 2);
	}

	return pNew;
    }

    public Rectangle getInnerBounds() {
	Insets insets = this.getInsets();
	Rectangle rArea = this.getBounds();

	rArea.setBounds(rArea.x + insets.left,
		rArea.y + insets.top,
		rArea.width - insets.left - insets.right,
		rArea.height - insets.top - insets.bottom);

	return rArea;
    }

    public boolean validResize(Dimension dimension, Point location, KpiMapaDraggableElement parent) {
	boolean valid = true;

	//testa altura m�nima
	if (dimension.height < getMinHeight()) {
	    valid = false;
	}

	//testa largura m�nima
	if (valid && dimension.width < getMinWidth()) {
	    valid = false;
	}

	if (parent != null) {
	    //testa pai
	    Insets insets = parent.getInsets();
	    if (valid) {
		if (dimension.height + location.y > parent.getHeight() - insets.top - insets.bottom - (BORDERFACTOR_H * 2)) {
		    valid = false;
		}
	    }

	    if (valid) {
		if (dimension.width + location.x > parent.getWidth() - insets.left - insets.right - (BORDERFACTOR_W * 2)) {
		    valid = false;
		}
	    }
	}

	if (valid) {
	    if (location.x < 0 || location.y < 0) {
		valid = false;
	    }
	}

	return valid;
    }

    protected abstract int getMinHeight();

    protected abstract int getMinWidth();
}