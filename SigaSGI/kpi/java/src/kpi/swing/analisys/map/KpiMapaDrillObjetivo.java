package kpi.swing.analisys.map;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Calendar;
import javax.swing.JInternalFrame;
import kpi.core.BIRequest;
import kpi.core.KpiDataController;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiDefaultFrameFunctions;
import kpi.swing.analisys.widget.KpiWidget;
import kpi.swing.analisys.widget.KpiWidgetFactory;
import kpi.util.KpiDateUtil;
import kpi.xml.BIXMLRecord;

/**
 *
 * @author gilmar.pereira
 */
public class KpiMapaDrillObjetivo extends JInternalFrame implements KpiDefaultFrameFunctions, MouseListener {

    private String recordtype = "MAPADRILLOBJETIVO";
    private int status;
    private KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private String id = new String();
    private BIXMLRecord record = null;

    public KpiMapaDrillObjetivo(int operation, String idAux, String contextId) {
        initComponents();

        putClientProperty("MAXI", true); //NOI18N

        event.defaultConstructor(operation, idAux, contextId);
    }

    public void enableFields() {
    }

    public void disableFields() {
    }

    public void refreshFields() {
    }

    public BIXMLRecord getFieldsContents() {
        return null;
    }

    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        recordtype = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(recordtype);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(BIXMLRecord recordAux) {
        record = recordAux;
    }

    public BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
    }

    public void showOperationsButtons() {
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        return "";
    }

    public void loadRecord() {
    }

    public void doDrill() {
        Cursor cursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
        this.setCursor(cursor);

        BIRequest request = new BIRequest();
        KpiDataController dataController = KpiStaticReferences.getKpiDataController();

        request.setParameter("TIPO", "MAPAESTRATEGICO");
        request.setParameter("MAPACMD", "CEOBJETIVOS");
        request.setParameter("OBJETIVOID", getID());

        request.setParameter("DATAALVO", KpiDateUtil.calendarToString(fldDataAlvo.getCalendar()));
        request.setParameter("DATAACUMDE", KpiDateUtil.calendarToString(fldDataAcumDe.getCalendar()));
        request.setParameter("DATAACUMATE", KpiDateUtil.calendarToString(fldDataAcumAte.getCalendar()));

        BIXMLRecord rec = dataController.listRecords(request);

        //Hierarquia
        lblOrganizacao.setText(rec.getAttributes().getString("ORGANIZACAO_NOME"));
        lblEstrategia.setText(rec.getAttributes().getString("ESTRATEGIA_NOME"));
        lblPerspectiva.setText(rec.getAttributes().getString("PERSPECTIVA_NOME"));

        //Objetivo
        lblObjetivoStatus.setIcon(KpiStaticReferences.getKpiImageResources().getImage(rec.getAttributes().getInt("OBJETIVO_STATUS")));
        lblObjetivoNome.setText("<html>".concat(rec.getAttributes().getString("OBJETIVO_NOME")).concat("</html>"));

        setTitle("Drill-Down - ".concat(rec.getAttributes().getString("OBJETIVO_NOME")));

        //Causas
        pnlCausa.removeAll();

        for (BIXMLRecord causa : rec.getBIXMLVector("CAUSAS")) {
            KpiMapaDrillObjetivoLabel labelAux = new KpiMapaDrillObjetivoLabel(causa);
            labelAux.addMouseListener(this);

            pnlCausa.add(labelAux);
        }

        pnlCausa.repaint();
        pnlCausa.doLayout();

        //Efeitos
        pnlEfeito.removeAll();

        for (BIXMLRecord efeito : rec.getBIXMLVector("EFEITOS")) {
            KpiMapaDrillObjetivoLabel labelAux = new KpiMapaDrillObjetivoLabel(efeito);
            labelAux.addMouseListener(this);

            pnlEfeito.add(labelAux);
        }

        pnlEfeito.repaint();
        pnlEfeito.doLayout();


        //Indicadores
        pnlIndicadores.setAutoOrganize(true);
        pnlIndicadores.removeAll();

        int i = 0;
        for (BIXMLRecord indicador : rec.getBIXMLVector("INDICADORES")) {
            KpiWidget widget = KpiWidgetFactory.createWidget(indicador);
            widget.setToolBarVisible(true);
            widget.setVisible(true);
            widget.setOrder(i++);

            pnlIndicadores.add(widget);
        }

        pnlIndicadores.autoOrganizeFrames();
        pnlIndicadores.setSize(pnlIndicadores.getHeight() + 1, pnlIndicadores.getWidth());
        
        cursor = Cursor.getDefaultCursor();
        this.setCursor(cursor);
    }

    public void doDrill(Calendar dataAlvo, Calendar dataAcumDe, Calendar dataAcumAte) {

        fldDataAlvo.setCalendar(dataAlvo);
        fldDataAcumDe.setCalendar(dataAcumDe);
        fldDataAcumAte.setCalendar(dataAcumAte);

        doDrill();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return null;
    }

    public String getFormType() {
        return recordtype;
    }

    public String getParentType() {
        return null;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlMainPanel = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlHierarquia = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lblOrganizacao = new javax.swing.JLabel();
        lblEstrategia = new javax.swing.JLabel();
        lblPerspectiva = new javax.swing.JLabel();
        pnlData = new javax.swing.JPanel();
        lblDataAlvo = new javax.swing.JLabel();
        scrDataAlvo = new javax.swing.JScrollPane();
        fldDataAlvo = new pv.jfcx.JPVDatePlus();
        lblDataAcumDe = new javax.swing.JLabel();
        scrDataAcumDe = new javax.swing.JScrollPane();
        fldDataAcumDe = new pv.jfcx.JPVDatePlus();
        lblDataAcumAte = new javax.swing.JLabel();
        scrDataAcumAte = new javax.swing.JScrollPane();
        fldDataAcumAte = new pv.jfcx.JPVDatePlus();
        tbAtualizar = new javax.swing.JToolBar();
        btnAtualizar = new javax.swing.JButton();
        pnlBody = new javax.swing.JPanel();
        splitBody = new javax.swing.JSplitPane();
        pnlBodyTop = new javax.swing.JPanel();
        pnlCaptionRelacaoObjetivo = new javax.swing.JPanel();
        pnlCaptionCausa = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        pnlCaptionObjetivo = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        pnlCaptionEfeito = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        pnlRelacaoObjetivo = new javax.swing.JPanel();
        scrCausa = new javax.swing.JScrollPane();
        pnlCausa = new javax.swing.JPanel();
        pnlObjetivo = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel5 = new javax.swing.JPanel();
        lblObjetivoNome = new javax.swing.JLabel();
        lblObjetivoStatus = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        scrEfeito = new javax.swing.JScrollPane();
        pnlEfeito = new javax.swing.JPanel();
        pnlBodyBottom = new javax.swing.JPanel();
        pnlCaptionIndicador = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        scrIndicadores = new javax.swing.JScrollPane();
        pnlIndicadores = new kpi.beans.JBICardDesktopPane();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Drill-Down");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_painel.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 500));
        setPreferredSize(new java.awt.Dimension(543, 500));

        pnlMainPanel.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 80));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlHierarquia.setBackground(new java.awt.Color(223, 229, 243));
        pnlHierarquia.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(221, 221, 221)));
        pnlHierarquia.setPreferredSize(new java.awt.Dimension(10, 50));
        pnlHierarquia.setLayout(null);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international"); // NOI18N
        jLabel5.setText(bundle.getString("KpiMapaDrillObjetivo_00001")); // NOI18N
        pnlHierarquia.add(jLabel5);
        jLabel5.setBounds(10, 0, 110, 15);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText(bundle.getString("KpiMapaDrillObjetivo_00002")); // NOI18N
        pnlHierarquia.add(jLabel6);
        jLabel6.setBounds(10, 15, 110, 15);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText(bundle.getString("KpiMapaDrillObjetivo_00003")); // NOI18N
        pnlHierarquia.add(jLabel7);
        jLabel7.setBounds(10, 30, 110, 15);

        lblOrganizacao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        pnlHierarquia.add(lblOrganizacao);
        lblOrganizacao.setBounds(130, 0, 682, 15);

        lblEstrategia.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        pnlHierarquia.add(lblEstrategia);
        lblEstrategia.setBounds(130, 15, 682, 15);

        lblPerspectiva.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        pnlHierarquia.add(lblPerspectiva);
        lblPerspectiva.setBounds(130, 30, 682, 15);

        pnlTools.add(pnlHierarquia, java.awt.BorderLayout.NORTH);

        pnlData.setPreferredSize(new java.awt.Dimension(10, 23));
        pnlData.setLayout(null);

        lblDataAlvo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataAlvo.setText(bundle.getString("KpiMapaDrillObjetivo_00004")); // NOI18N
        lblDataAlvo.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDataAlvo.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDataAlvo.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblDataAlvo);
        lblDataAlvo.setBounds(10, 3, 60, 20);

        scrDataAlvo.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrDataAlvo.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        fldDataAlvo.setAllowNull(false);
        fldDataAlvo.setBorderStyle(0);
        fldDataAlvo.setButtonBorder(0);
        fldDataAlvo.setDialog(true);
        fldDataAlvo.setFont(new java.awt.Font("Tahoma", 1, 11));
        fldDataAlvo.setUseLocale(true);
        fldDataAlvo.setWaitForCalendarDate(true);
        scrDataAlvo.setViewportView(fldDataAlvo);

        pnlData.add(scrDataAlvo);
        scrDataAlvo.setBounds(70, 3, 100, 20);

        lblDataAcumDe.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataAcumDe.setText(bundle.getString("KpiMapaDrillObjetivo_00005")); // NOI18N
        lblDataAcumDe.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDataAcumDe.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDataAcumDe.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblDataAcumDe);
        lblDataAcumDe.setBounds(210, 3, 80, 20);

        scrDataAcumDe.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrDataAcumDe.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        fldDataAcumDe.setAllowNull(false);
        fldDataAcumDe.setBorderStyle(0);
        fldDataAcumDe.setButtonBorder(0);
        fldDataAcumDe.setDialog(true);
        fldDataAcumDe.setFont(new java.awt.Font("Tahoma", 1, 11));
        fldDataAcumDe.setUseLocale(true);
        fldDataAcumDe.setWaitForCalendarDate(true);
        scrDataAcumDe.setViewportView(fldDataAcumDe);

        pnlData.add(scrDataAcumDe);
        scrDataAcumDe.setBounds(290, 3, 100, 20);

        lblDataAcumAte.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataAcumAte.setText(bundle.getString("KpiMapaDrillObjetivo_00006")); // NOI18N
        lblDataAcumAte.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDataAcumAte.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDataAcumAte.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblDataAcumAte);
        lblDataAcumAte.setBounds(420, 3, 80, 20);

        scrDataAcumAte.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrDataAcumAte.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        fldDataAcumAte.setAllowNull(false);
        fldDataAcumAte.setBorderStyle(0);
        fldDataAcumAte.setButtonBorder(0);
        fldDataAcumAte.setDialog(true);
        fldDataAcumAte.setFont(new java.awt.Font("Tahoma", 1, 11));
        fldDataAcumAte.setUseLocale(true);
        fldDataAcumAte.setWaitForCalendarDate(true);
        scrDataAcumAte.setViewportView(fldDataAcumAte);

        pnlData.add(scrDataAcumAte);
        scrDataAcumAte.setBounds(500, 3, 100, 20);

        tbAtualizar.setBorder(null);
        tbAtualizar.setRollover(true);

        btnAtualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnAtualizar.setText(bundle.getString("KpiMapaDrillObjetivo_00007")); // NOI18N
        btnAtualizar.setBorder(null);
        btnAtualizar.setFocusable(false);
        btnAtualizar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnAtualizar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAtualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtualizarActionPerformed(evt);
            }
        });
        tbAtualizar.add(btnAtualizar);

        pnlData.add(tbAtualizar);
        tbAtualizar.setBounds(630, 3, 100, 20);

        pnlTools.add(pnlData, java.awt.BorderLayout.CENTER);

        pnlMainPanel.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlBody.setLayout(new java.awt.BorderLayout());

        splitBody.setBorder(null);
        splitBody.setDividerLocation(200);
        splitBody.setDividerSize(2);
        splitBody.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        pnlBodyTop.setLayout(new java.awt.BorderLayout());

        pnlCaptionRelacaoObjetivo.setMaximumSize(new java.awt.Dimension(32767, 20));
        pnlCaptionRelacaoObjetivo.setMinimumSize(new java.awt.Dimension(10, 20));
        pnlCaptionRelacaoObjetivo.setPreferredSize(new java.awt.Dimension(10, 20));
        pnlCaptionRelacaoObjetivo.setLayout(new java.awt.GridLayout(1, 3));

        pnlCaptionCausa.setLayout(new java.awt.BorderLayout());

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_graydn.gif"))); // NOI18N
        jLabel1.setText(bundle.getString("KpiMapaDrillObjetivo_00008")); // NOI18N
        pnlCaptionCausa.add(jLabel1, java.awt.BorderLayout.CENTER);

        pnlCaptionRelacaoObjetivo.add(pnlCaptionCausa);

        pnlCaptionObjetivo.setLayout(new java.awt.BorderLayout());

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_objetivo.gif"))); // NOI18N
        jLabel2.setText(bundle.getString("KpiMapaDrillObjetivo_00009")); // NOI18N
        pnlCaptionObjetivo.add(jLabel2, java.awt.BorderLayout.CENTER);

        pnlCaptionRelacaoObjetivo.add(pnlCaptionObjetivo);

        pnlCaptionEfeito.setLayout(new java.awt.BorderLayout());

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_grayup.gif"))); // NOI18N
        jLabel3.setText(bundle.getString("KpiMapaDrillObjetivo_00010")); // NOI18N
        pnlCaptionEfeito.add(jLabel3, java.awt.BorderLayout.CENTER);

        pnlCaptionRelacaoObjetivo.add(pnlCaptionEfeito);

        pnlBodyTop.add(pnlCaptionRelacaoObjetivo, java.awt.BorderLayout.PAGE_START);

        pnlRelacaoObjetivo.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        pnlRelacaoObjetivo.setLayout(new java.awt.GridLayout(1, 3));

        scrCausa.setBorder(null);

        pnlCausa.setBackground(new java.awt.Color(255, 255, 255));
        pnlCausa.setLayout(new javax.swing.BoxLayout(pnlCausa, javax.swing.BoxLayout.Y_AXIS));
        scrCausa.setViewportView(pnlCausa);

        pnlRelacaoObjetivo.add(scrCausa);

        pnlObjetivo.setBackground(new java.awt.Color(255, 255, 255));

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lblObjetivoNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblObjetivoNome.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        lblObjetivoStatus.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblObjetivoStatus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_meta_formula.gif"))); // NOI18N
        lblObjetivoStatus.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        lblObjetivoStatus.setPreferredSize(new java.awt.Dimension(30, 16));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblObjetivoStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblObjetivoNome, javax.swing.GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblObjetivoNome, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 132, Short.MAX_VALUE)
                    .addComponent(lblObjetivoStatus, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 132, Short.MAX_VALUE))
                .addContainerGap())
        );

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout pnlObjetivoLayout = new javax.swing.GroupLayout(pnlObjetivo);
        pnlObjetivo.setLayout(pnlObjetivoLayout);
        pnlObjetivoLayout.setHorizontalGroup(
            pnlObjetivoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlObjetivoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(pnlObjetivoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlObjetivoLayout.createSequentialGroup()
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(272, Short.MAX_VALUE)))
            .addGroup(pnlObjetivoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlObjetivoLayout.createSequentialGroup()
                    .addContainerGap(272, Short.MAX_VALUE)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        pnlObjetivoLayout.setVerticalGroup(
            pnlObjetivoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlObjetivoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(pnlObjetivoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE))
            .addGroup(pnlObjetivoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE))
        );

        pnlRelacaoObjetivo.add(pnlObjetivo);

        scrEfeito.setBorder(null);

        pnlEfeito.setBackground(new java.awt.Color(255, 255, 255));
        pnlEfeito.setLayout(new javax.swing.BoxLayout(pnlEfeito, javax.swing.BoxLayout.Y_AXIS));
        scrEfeito.setViewportView(pnlEfeito);

        pnlRelacaoObjetivo.add(scrEfeito);

        pnlBodyTop.add(pnlRelacaoObjetivo, java.awt.BorderLayout.CENTER);

        splitBody.setLeftComponent(pnlBodyTop);

        pnlBodyBottom.setLayout(new java.awt.BorderLayout());

        pnlCaptionIndicador.setMaximumSize(new java.awt.Dimension(32767, 20));
        pnlCaptionIndicador.setMinimumSize(new java.awt.Dimension(10, 20));
        pnlCaptionIndicador.setPreferredSize(new java.awt.Dimension(10, 20));
        pnlCaptionIndicador.setLayout(new java.awt.BorderLayout());

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_indicador.gif"))); // NOI18N
        jLabel4.setText(bundle.getString("KpiMapaDrillObjetivo_00011")); // NOI18N
        pnlCaptionIndicador.add(jLabel4, java.awt.BorderLayout.CENTER);

        pnlBodyBottom.add(pnlCaptionIndicador, java.awt.BorderLayout.PAGE_START);

        scrIndicadores.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        pnlIndicadores.setBackground(new java.awt.Color(255, 255, 255));
        scrIndicadores.setViewportView(pnlIndicadores);

        pnlBodyBottom.add(scrIndicadores, java.awt.BorderLayout.CENTER);

        splitBody.setRightComponent(pnlBodyBottom);

        pnlBody.add(splitBody, java.awt.BorderLayout.CENTER);

        pnlMainPanel.add(pnlBody, java.awt.BorderLayout.CENTER);

        getContentPane().add(pnlMainPanel, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtualizarActionPerformed
        doDrill();
    }//GEN-LAST:event_btnAtualizarActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAtualizar;
    private pv.jfcx.JPVDatePlus fldDataAcumAte;
    private pv.jfcx.JPVDatePlus fldDataAcumDe;
    private pv.jfcx.JPVDatePlus fldDataAlvo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lblDataAcumAte;
    private javax.swing.JLabel lblDataAcumDe;
    private javax.swing.JLabel lblDataAlvo;
    private javax.swing.JLabel lblEstrategia;
    private javax.swing.JLabel lblObjetivoNome;
    private javax.swing.JLabel lblObjetivoStatus;
    private javax.swing.JLabel lblOrganizacao;
    private javax.swing.JLabel lblPerspectiva;
    private javax.swing.JPanel pnlBody;
    private javax.swing.JPanel pnlBodyBottom;
    private javax.swing.JPanel pnlBodyTop;
    private javax.swing.JPanel pnlCaptionCausa;
    private javax.swing.JPanel pnlCaptionEfeito;
    private javax.swing.JPanel pnlCaptionIndicador;
    private javax.swing.JPanel pnlCaptionObjetivo;
    private javax.swing.JPanel pnlCaptionRelacaoObjetivo;
    private javax.swing.JPanel pnlCausa;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlEfeito;
    private javax.swing.JPanel pnlHierarquia;
    private kpi.beans.JBICardDesktopPane pnlIndicadores;
    private javax.swing.JPanel pnlMainPanel;
    private javax.swing.JPanel pnlObjetivo;
    private javax.swing.JPanel pnlRelacaoObjetivo;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JScrollPane scrCausa;
    private javax.swing.JScrollPane scrDataAcumAte;
    private javax.swing.JScrollPane scrDataAcumDe;
    private javax.swing.JScrollPane scrDataAlvo;
    private javax.swing.JScrollPane scrEfeito;
    private javax.swing.JScrollPane scrIndicadores;
    private javax.swing.JSplitPane splitBody;
    private javax.swing.JToolBar tbAtualizar;
    // End of variables declaration//GEN-END:variables

    public void mouseClicked(MouseEvent e) {
        if (e.getSource() instanceof KpiMapaDrillObjetivoLabel) {
            KpiMapaDrillObjetivoLabel o = (KpiMapaDrillObjetivoLabel) e.getSource();

            setID(o.getId());
            doDrill();
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }
}