package kpi.swing.analisys.map;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import kpi.core.KpiStaticReferences;
import kpi.xml.BIXMLRecord;

/**
 *
 * @author gilmar.pereira
 */
public class KpiMapaDrillObjetivoLabel extends javax.swing.JLabel implements MouseListener {
    private String id;
    private String nome;
    private int status;
    
    KpiMapaDrillObjetivoLabel(BIXMLRecord src) {
        id = src.getString("OBJETIVO_ID");
        nome = src.getString("OBJETIVO_NOME");
        status = src.getInt("OBJETIVO_STATUS");

        setText(nome);
        setToolTipText(nome);
        setIcon(KpiStaticReferences.getKpiImageResources().getImage(status));
        
        addMouseListener(this);
    }

    public String getId() {
        return id;
    }   

    public String getNome() {
        return nome;
    }   

    public int getStatus() {
        return status;
    }   

    public void mouseEntered(java.awt.event.MouseEvent e) {
	setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
	setText( "<html><u>"+nome+"</u></html>" );
    }

    public void mouseExited(java.awt.event.MouseEvent e) {
	setCursor(null);
        setText(nome);
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }
}
