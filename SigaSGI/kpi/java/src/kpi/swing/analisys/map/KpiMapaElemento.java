package kpi.swing.analisys.map;

import java.awt.Color;
import java.awt.Rectangle;
/**
 *
 * @author gilmar.pereira
 */
public class KpiMapaElemento implements IKpiMapaComponente {

    public static final String ELEMENT_GROUP = "1";
    public static final String ELEMENT_OBJECTIVE = "2";
    
    public static final String SHAPE_RECT = "1";
    public static final String SHAPE_CIRCLE = "2";

    protected String id = "";
    protected String name = "";
    protected int posX = 0;
    protected int posY = 0;
    protected int width = 0;
    protected int height = 0;
    protected boolean opaque = true;
    protected boolean visibleName = true;
    protected Color backColor = Color.WHITE;
    protected Color fontColor = Color.BLACK;
    protected String shape = KpiMapaElemento.SHAPE_RECT;
    protected KpiMapaPerspectiva perspective;
    private static int idCount = 0;

    /**
     * @return the id
     */
    public String getId() {
	return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
	this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
	return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
	this.name = name;
    }

    /**
     * @return the posX
     */
    public int getPosX() {
	return posX;
    }

    /**
     * @param posX the posX to set
     */
    public void setPosX(int posX) {
	this.posX = posX;
    }

    /**
     * @return the posY
     */
    public int getPosY() {
	return posY;
    }

    /**
     * @param posY the posY to set
     */
    public void setPosY(int posY) {
	this.posY = posY;
    }

    /**
     * @return the width
     */
    public int getWidth() {
	return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(int width) {
	this.width = width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
	return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(int height) {
	this.height = height;
    }

    /**
     * @return the backColor
     */
    public Color getBackColor() {
	return backColor;
    }

    /**
     * @param backColor the backColor to set
     */
    public void setBackColor(Color backColor) {
	this.backColor = backColor;
    }

    /**
     * @return the fontColor
     */
    public Color getFontColor() {
	return fontColor;
    }

    /**
     * @param fontColor the fontColor to set
     */
    public void setFontColor(Color fontColor) {
	this.fontColor = fontColor;
    }

    /**
     * @return the shape
     */
    public String getShape() {
	return shape;
    }

    /**
     * @param shape the shape to set
     */
    public void setShape(String shape) {
	this.shape = shape;
    }

    /**
     * @return the perspective
     */
    public KpiMapaPerspectiva getPerspective() {
	return perspective;
    }

    /**
     * @param perspective the perspective to set
     */
    public void setPerspective(KpiMapaPerspectiva perspective) {
	this.perspective = perspective;
    }

    /**
     * @return the opaque
     */
    public boolean isOpaque() {
	return opaque;
    }

    /**
     * @param opaque the opaque to set
     */
    public void setOpaque(boolean opaque) {
	this.opaque = opaque;
    }

    /**
     * @return the visibleName
     */
    public boolean isVisibleName() {
	return visibleName;
    }

    /**
     * @param showName the showName to set
     */
    public void setVisibleName(boolean visibleName) {
	this.visibleName = visibleName;
    }

    /**
     * @return a rectangle representation of the object
     */
    public Rectangle getBounds() {
	return new Rectangle(posX, posY, width, height);
    }

    public static synchronized String generateId() {
	String newId = "new_".concat(String.format("%010d", ++idCount));

	return newId;
    }
}