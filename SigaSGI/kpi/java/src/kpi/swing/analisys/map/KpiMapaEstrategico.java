package kpi.swing.analisys.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

/**
 *
 * @author gilmar.pereira
 */
public class KpiMapaEstrategico {

    private String id = "";
    private String sourceId = "";
    private int qtdDivision = 0;
    private String[] divisions = new String[3];
    private HashMap<String, KpiMapaPerspectiva> perspectives = new HashMap();
    private HashMap<String, KpiMapaLigacao> links = new HashMap();
    private String type = "";
    private String name = "";

    /**
     * Construtor padr�o
     */
    public KpiMapaEstrategico() {
    }

    /**
     * Construtor baseado em valores de um XML
     * @param elementXml
     */
    public KpiMapaEstrategico(BIXMLRecord elementXml) {
	id = elementXml.getString("ID");
	sourceId = elementXml.getString("ESTRATEGIA_ID");
	qtdDivision = elementXml.getInt("QTDDIVISAO");
	divisions[0] = elementXml.getString("DIVISAO1");
	divisions[1] = elementXml.getString("DIVISAO2");
	divisions[2] = elementXml.getString("DIVISAO3");
	type = elementXml.getString("TYPE");
	name = elementXml.getString("NOME");
    }

    /**
     * @return the id
     */
    public String getId() {
	return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
	this.id = id;
    }

    /**
     * @return the sourceId
     */
    public String getSourceId() {
	return sourceId;
    }

    /**
     * @param sourceId the sourceId to set
     */
    public void setSourceId(String sourceId) {
	this.sourceId = sourceId;
    }

    /**
     * @return the qtdDivision
     */
    public int getQtdDivision() {
	return qtdDivision;
    }

    /**
     * @param qtdDivision the qtdDivision to set
     */
    public void setQtdDivision(int qtdDivision) {
	this.qtdDivision = qtdDivision;
    }

    /**
     * @return the divisions
     */
    public String[] getDivisions() {
	return divisions;
    }

    /**
     * @param divisions the divisions to set
     */
    public void setDivisions(String[] divisions) {
	this.divisions = divisions;
    }

    /**
     * @return the type
     */
    public String getType() {
	return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
	this.type = type;
    }

    /**
     * @return the name
     */
    public String getName() {
	return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
	this.name = name;
    }

    /**
     * @return the perspectives
     */
    public HashMap<String, KpiMapaPerspectiva> getPerspectives() {
	return perspectives;
    }

    /**
     * @param key the id of the perspective to get
     * @return the perspective
     */
    public KpiMapaPerspectiva getPerspective(String key) {
	return perspectives.get(key);
    }

    /**
     * @param perspective the perspective to set
     */
    public void addPerspective(KpiMapaPerspectiva perspective) {
	this.perspectives.put(perspective.getId(), perspective);
	perspective.setMap(this);
    }

    /**
     * @param perspective the perspectives to remove
     */
    public void removePerspective(KpiMapaPerspectiva perspective) {
	removePerspective(perspective.getId());
    }

    /**
     * @param key the key to remove
     */
    public void removePerspective(String key) {
	KpiMapaPerspectiva perspective = this.perspectives.get(key);
	perspective.setMap(null);
	this.perspectives.remove(key);
    }

    /**
     * @param key the element to get
     * @return the element
     */
    public KpiMapaElemento getElement(String key) {
	KpiMapaElemento element = null;
	KpiMapaPerspectiva perspective = null;

	Iterator<KpiMapaPerspectiva> it = this.perspectives.values().iterator();

	while (it.hasNext() && element == null) {
	    perspective = it.next();
	    element = perspective.getElement(key);
	}

	return element;
    }

    /**
     * @return the links
     */
    public HashMap<String, KpiMapaLigacao> getLinks() {
	return links;
    }

    /**
     * @return the links
     */
    public KpiMapaLigacao getLink(String key) {
	return links.get(key);
    }

    /**
     * @param link the link to set
     */
    public void addLink(KpiMapaLigacao link) {
	this.links.put(link.getId(), link);
	link.setMap(this);
    }

    /**
     * @param link the link to remove
     */
    public void removeLink(KpiMapaLigacao link) {
	removeLink(link.getId());
    }

    /**
     * @param key the key to remove
     */
    public void removeLink(String key) {
	KpiMapaLigacao link = this.links.get(key);
	link.setMap(null);
	this.links.remove(key);
    }

    public void removeLinkIfExists(KpiMapaElemento source, KpiMapaElemento destination) {
	ArrayList<String> idsRemove = new ArrayList();

	for(KpiMapaLigacao link : this.links.values()) {

	    //verifica presen�a do link
	    if ((link.getSource() == source && link.getDestination() == destination)
		    || (link.getSource() == destination && link.getDestination() == source)) {

		//marca links para exclus�o
		idsRemove.add(link.getId());
	    }
	}

	//remove ids marcados
	for(String idRemove : idsRemove) {
	    this.removeLink(idRemove);
	}
    }

    public void removeAllLinks(KpiMapaElemento element) {
	ArrayList<String> idsRemove = new ArrayList();

	for(KpiMapaLigacao link : this.links.values()) {
	    //verifica presen�a do link
	    if (link.getSource() == element || link.getDestination() == element) {
		//marca id para remo��o
		idsRemove.add(link.getId());
	    }
	}

	//remove ids marcados
	for(String idRemove : idsRemove) {
	    this.removeLink(idRemove);
	}
    }

    public BIXMLRecord toXml() {
	BIXMLRecord mapXml = new BIXMLRecord("MAPAESTRATEGICO");

	mapXml.set("ID", getId());
	mapXml.set("ESTRATEGIA_ID", getSourceId());
	mapXml.set("QTDDIVISAO", getQtdDivision());
	mapXml.set("DIVISAO1", getDivisions()[0]);
	mapXml.set("DIVISAO2", getDivisions()[1]);
	mapXml.set("DIVISAO3", getDivisions()[2]);
	mapXml.set("NOME", getName());
	mapXml.set("TYPE", getType());

	BIXMLVector perspXml = new BIXMLVector("MAPAPERSPECTIVAS", "MAPAPERSPECTIVA");

	//perspectivas
	for (KpiMapaPerspectiva perspective : getPerspectives().values()) {
	    perspXml.add(perspective.toXml());
	}

	mapXml.set(perspXml);

	BIXMLVector linkXml = new BIXMLVector("MAPALIGACOES", "MAPALIGACAO");

	//Liga��es
	for (KpiMapaLigacao link : getLinks().values()) {
	    linkXml.add(link.toXml());
	}

	mapXml.set(linkXml);

	return mapXml;
    }

    /**
     * @param recordmap XML que ir� gerar o mapa
     * @return mapa montado
     */
    public static KpiMapaEstrategico mapFromXml(BIXMLRecord recordMap) {

	//nova inst�ncia
	KpiMapaEstrategico map = new KpiMapaEstrategico(recordMap);

	//perspectivas
	for (BIXMLRecord perspectivaXml : recordMap.getBIXMLVector("MAPAPERSPECTIVAS")) {

	    //Monta perspectiva com dados b�sicos de um XML
	    KpiMapaPerspectiva perspective = new KpiMapaPerspectiva(perspectivaXml);

	    //grupos
	    BIXMLVector grupos = perspectivaXml.getBIXMLVector("MAPAGRUPOS");
	    for (BIXMLRecord elementoXml : grupos) {
		//Monta grupo com dados b�sicos de um XML
		KpiMapaGrupo group = new KpiMapaGrupo(elementoXml);

		perspective.addElement(group);
	    }

	    //objetivos
	    for (BIXMLRecord elementoXml : perspectivaXml.getBIXMLVector("MAPAOBJETIVOS")) {

		//Monta objetivos estrat�gicos com dados b�sicos de um XML
		KpiMapaObjetivo objective = new KpiMapaObjetivo(elementoXml);

		//liga��o entre objetivos e grupos
		if (!elementoXml.getString("MAPAGRUPO_ID").trim().isEmpty()) {
		    KpiMapaGrupo group = (KpiMapaGrupo) perspective.getElement(elementoXml.getString("MAPAGRUPO_ID"));

		    //verifica integridade, pois teoricamente todos os grupos j� foram carregados
		    if (group != null) {
			group.addObjective(objective);
		    } else {
			objective.setGroup(null);
		    }
		}

		perspective.addElement(objective);
	    }

	    map.addPerspective(perspective);
	}

	//liga��es
	for (BIXMLRecord elementoXml : recordMap.getBIXMLVector("MAPALIGACOES")) {
	    //liga��es entre elementos
	    KpiMapaLigacao link = new KpiMapaLigacao(elementoXml);
	    KpiMapaElemento element;

	    //origem
	    element = map.getElement(elementoXml.getString("SRCELMID"));
	    link.setSource(element);

	    //destino
	    element = map.getElement(elementoXml.getString("DSTELMID"));
	    link.setDestination(element);

	    map.addLink(link);
	}

	return map;
    }

    public static BIXMLRecord xmlFromMap(KpiMapaEstrategico map) {
	return map.toXml();
    }
}
