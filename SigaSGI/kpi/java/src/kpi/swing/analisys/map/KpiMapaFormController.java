package kpi.swing.analisys.map;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.*;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import kpi.core.BIRequest;
import kpi.core.KpiDataController;
import kpi.core.KpiStaticReferences;
import kpi.swing.IKpiDragController;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiMouseEvent;
import kpi.util.KpiDateUtil;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

/**
 * 
 * @author gilmar.pereira
 */
public class KpiMapaFormController implements IKpiDragController {

    public static enum MapToolAction {

	MOVE,
	DRAWGROUP,
	DRAWLINK,
	DELETE
    };

    public static enum MapComponent {

	OBJECTIVE,
	GROUP,
	PERSPECTIVE,
	LINK
    };

    public static enum MapMouseAction {

	NOP,
	DRAG,
	DRAWGROUP,
	DRAWLINK,
	CURVELINK,
	RESIZE_UP,
	RESIZE_DOWN,
	RESIZE_LEFT,
	RESIZE_RIGHT
    };
    public static final int MAP_WIDTH = 948;
    private static final int MAP_RESIZEBORDER = 5;
    private static final boolean MAP_RESETLINESONMOVE = false; //controla o comportamento das curvas durante DRAG
    private MapMouseAction mouseAction = MapMouseAction.NOP;
    private MapToolAction toolAction = MapToolAction.MOVE;
    private IKpiMapaMainForm mapForm;
    private KpiMapaGlass glassPanel;
    private Point mousePressedPoint;
    private KpiMapaEstrategico map = null;
    private KpiMapaDraggableElement dragElement = null;
    private Rectangle dragHandle = null;
    private KpiMapaLigacao linkElement = null;
    private HashMap<String, KpiMapaDraggableElement> selectedElements = new HashMap();
    private HashMap<String, KpiMapaLigacaoRenderer> selectedLinks = new HashMap();
    private HashMap<String, KpiMapaPerspectivaRenderer> renderPerspectives = new HashMap();
    private HashMap<String, KpiMapaObjetivoRenderer> renderObjectives = new HashMap();
    private HashMap<String, KpiMapaGrupoRenderer> renderGroups = new HashMap();
    private HashMap<String, KpiMapaLigacaoRenderer> arrowsMap = new HashMap();
    private int status;
    private BIXMLVector themes;

    KpiMapaFormController(KpiMapaGlass glassPanel, IKpiMapaMainForm mapForm) {
	this.glassPanel = glassPanel;
	this.mapForm = mapForm;
    }

    @Override
    public void kpiMouseClicked(KpiMouseEvent e) {
	if (getStatus() == KpiDefaultFrameBehavior.NORMAL) {
	    MouseEvent mEvt = e.getEvent();
	    if (mEvt.getButton() == MouseEvent.BUTTON1) {
		KpiMapaDraggableElement element = (KpiMapaDraggableElement) e.getSource();

		//Se clicar com bot�o esquerdo em um objetivo realiza drill
		if (element instanceof KpiMapaObjetivoRenderer) {
		    KpiMapaObjetivo objective = (KpiMapaObjetivo) map.getElement(element.getId());

		    mapForm.drillObjective(objective.getSourceId());
		}
	    }
	} else if (getStatus() == KpiDefaultFrameBehavior.UPDATING) {
	    MouseEvent mEvt = e.getEvent();
	    if (mEvt.getButton() == MouseEvent.BUTTON1) {
		if (toolAction == MapToolAction.DELETE) {
		    KpiMapaDraggableElement element = (KpiMapaDraggableElement) e.getSource();

		    //considera apenas grupos
		    if (element instanceof KpiMapaGrupoRenderer) {
			kpi.swing.KpiDefaultDialogSystem dialogSystem = kpi.core.KpiStaticReferences.getKpiDialogSystem();

			if (dialogSystem.confirmMessage(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFormController_00002")) == javax.swing.JOptionPane.YES_OPTION) {

			    unSelectElement(element);

			    KpiMapaGrupo group = (KpiMapaGrupo) map.getElement(element.getId());

			    KpiMapaPerspectiva perspective = group.getPerspective();
			    KpiMapaPerspectivaRenderer perspectiveRenderer = renderPerspectives.get(perspective.getId());

			    //Reposicionar objetivos
			    for (KpiMapaObjetivo objective : group.getObjectives().values()) {
				KpiMapaObjetivoRenderer objectiveRenderer = renderObjectives.get(objective.getId());

				Point pNew = objectiveRenderer.getLocation();

				pNew = element.relativeToRealCoords(pNew);
				pNew.translate(element.getX(), element.getY());

				objectiveRenderer.setLocation(pNew);
				objective.setPosX(pNew.x);
				objective.setPosY(pNew.y);

				perspectiveRenderer.addComponent(objectiveRenderer);
			    }

			    group.removeAllObjectives();

			    map.removeAllLinks(group);

			    perspective.removeElement(group);
			    renderGroups.remove(element.getId());

			    perspectiveRenderer.removeComponent(element);

			    perspectiveRenderer.validate();
			    perspectiveRenderer.repaint();

			    glassPanel.clearShadowBox();
			    glassPanel.clearShadowLine();
			    repaintGlass();
			}
		    } else if (element instanceof KpiMapaPerspectivaRenderer) {

			KpiMapaLigacaoRenderer arrowDelete = null;

			Point p = mEvt.getPoint();

			p.translate(element.getX(), element.getY());

			//testar liga��es
			for (KpiMapaLigacaoRenderer arrow : arrowsMap.values()) {
			    if (arrow.ptSegDist(p) <= KpiMapaLigacaoRenderer.CONTROLBOXSIZE) {
				arrowDelete = arrow;
				break;
			    }
			}

			if (arrowDelete != null) {
			    kpi.swing.KpiDefaultDialogSystem dialogSystem = kpi.core.KpiStaticReferences.getKpiDialogSystem();
			    if (dialogSystem.confirmMessage(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFormController_00003")) == javax.swing.JOptionPane.YES_OPTION) {
				this.map.removeLink(arrowDelete.getId());
			    }
			}

			glassPanel.clearShadowBox();
			glassPanel.clearShadowLine();
			repaintGlass();
		    }
		}
	    }
	}
    }

    @Override
    public void kpiMouseEntered(KpiMouseEvent e) {
	if (status == KpiDefaultFrameBehavior.UPDATING) {
	    if (toolAction == MapToolAction.MOVE && mouseAction == MapMouseAction.NOP) {

		KpiMapaDraggableElement element = (KpiMapaDraggableElement) e.getSource();

		//considera apenas objetivos e grupos
		if (element instanceof KpiMapaObjetivoRenderer || element instanceof KpiMapaGrupoRenderer) {
		    unselectAllElements();

		    selectElement(element);
		    element.repaint();

		    repaintGlass();
		}

	    } else if (toolAction == MapToolAction.DRAWLINK) {

		int cursor = Cursor.DEFAULT_CURSOR;

		KpiMapaDraggableElement element = (KpiMapaDraggableElement) e.getSource();

		//considera apenas objetivos e grupos
		if (element instanceof KpiMapaObjetivoRenderer || element instanceof KpiMapaGrupoRenderer) {
		    KpiMapaElemento elementModel = map.getElement(element.getId());

		    if (mouseAction == MapMouseAction.DRAWLINK) {
			if (validLink(linkElement.getSource(), elementModel)) {
			    selectElement(element);
			    element.repaint();

			    linkElement.setDestination(elementModel);

			    cursor = Cursor.HAND_CURSOR;
			}
		    } else {
			selectElement(element);
			element.repaint();

			cursor = Cursor.HAND_CURSOR;
		    }
		}

		setMapaCursor(new Cursor(cursor));

	    } else if (toolAction == MapToolAction.DELETE && mouseAction == MapMouseAction.NOP) {

		KpiMapaDraggableElement element = (KpiMapaDraggableElement) e.getSource();

		if (element instanceof KpiMapaObjetivoRenderer || element instanceof KpiMapaGrupoRenderer) {
		    unselectAllElements();

		    //considera apenas grupos
		    if (element instanceof KpiMapaGrupoRenderer) {
			selectElement(element);
			element.repaint();
		    }

		    repaintGlass();
		}
	    }
	} else {
	    KpiMapaDraggableElement element = (KpiMapaDraggableElement) e.getSource();

	    if (element instanceof KpiMapaObjetivoRenderer) {
		setMapaCursor(new Cursor(Cursor.HAND_CURSOR));
	    }
	}
    }

    @Override
    public void kpiMouseExited(KpiMouseEvent e) {
	if (status == KpiDefaultFrameBehavior.UPDATING) {
	    if (toolAction == MapToolAction.MOVE && mouseAction == MapMouseAction.NOP) {

		KpiMapaDraggableElement element = (KpiMapaDraggableElement) e.getSource();

		//considera apenas objetivos e grupos
		if (element instanceof KpiMapaObjetivoRenderer || element instanceof KpiMapaGrupoRenderer) {
		    unSelectElement(element);
		    element.repaint();
		}

		setMapaCursor(new Cursor(Cursor.DEFAULT_CURSOR));

	    } else if (toolAction == MapToolAction.DRAWLINK) {

		int cursor = Cursor.DEFAULT_CURSOR;

		KpiMapaDraggableElement element = (KpiMapaDraggableElement) e.getSource();

		//considera apenas objetivos e grupos
		if (element instanceof KpiMapaObjetivoRenderer || element instanceof KpiMapaGrupoRenderer) {
		    KpiMapaElemento elementModel = map.getElement(element.getId());

		    if (mouseAction == MapMouseAction.DRAWLINK) {
			if (linkElement.getSource() != elementModel) {
			    linkElement.setDestination(null);
			    unSelectElement(element);
			    element.repaint();
			}
		    } else {
			unSelectElement(element);
			element.repaint();
		    }
		}

		setMapaCursor(new Cursor(cursor));

	    } else if (toolAction == MapToolAction.DELETE) {

		KpiMapaDraggableElement element = (KpiMapaDraggableElement) e.getSource();

		//considera apenas grupos
		if (element instanceof KpiMapaGrupoRenderer) {
		    unSelectElement(element);
		    element.repaint();
		}

	    }
	} else {
	    KpiMapaDraggableElement element = (KpiMapaDraggableElement) e.getSource();

	    if (element instanceof KpiMapaObjetivoRenderer) {
		setMapaCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	    }
	}
    }

    @Override
    public void kpiMousePressed(KpiMouseEvent e) {
	if (status == KpiDefaultFrameBehavior.UPDATING) {
	    MouseEvent mEvt = e.getEvent();

	    KpiMapaDraggableElement element = (KpiMapaDraggableElement) e.getSource();

	    if (mEvt.getButton() == MouseEvent.BUTTON1) {
		mousePressedPoint = element.moveConstraintRealCoords(mEvt.getPoint());

		if (toolAction == MapToolAction.DRAWGROUP) {
		    if (element instanceof KpiMapaPerspectivaRenderer) {

			Point p = new Point(mousePressedPoint.x, mousePressedPoint.y);

			p.translate(element.getX(), element.getY());
			glassPanel.setShadowBox(p.x, p.y, 0, 0);
			glassPanel.setShadowBoxColor(Color.BLACK);

			mouseAction = MapMouseAction.DRAWGROUP;
		    }
		} else if (toolAction == MapToolAction.MOVE) {

		    if (element instanceof KpiMapaObjetivoRenderer || element instanceof KpiMapaGrupoRenderer) {
			Point mPoint = mEvt.getPoint();

			dragElement = element;

			if (mPoint.getY() >= element.getHeight() - MAP_RESIZEBORDER) {
			    mouseAction = MapMouseAction.RESIZE_DOWN;
			} else if (mPoint.getY() <= MAP_RESIZEBORDER) {
			    mouseAction = MapMouseAction.RESIZE_UP;
			} else if (mPoint.getX() <= MAP_RESIZEBORDER) {
			    mouseAction = MapMouseAction.RESIZE_LEFT;
			} else if (mPoint.getX() >= element.getWidth() - MAP_RESIZEBORDER) {
			    mouseAction = MapMouseAction.RESIZE_RIGHT;
			} else {
			    //Clique em uma �rea de movimenta��o
			    KpiMapaElemento elementModel = map.getElement(element.getId());

			    //torna elemento invis�vel
			    element.setVisible(false);

			    //Perspectiva do elemento
			    KpiMapaPerspectivaRenderer mapPerspective = renderPerspectives.get(elementModel.getPerspective().getId());

			    //Coordenadas relativas do elemento
			    Point p = element.getLocation();

			    //tratamentos diferenciados para objetivos e grupos
			    if (elementModel instanceof KpiMapaObjetivo) {
				KpiMapaObjetivo objective = (KpiMapaObjetivo) elementModel;
				glassPanel.setShadowBox(objective);

				if (objective.getGroup() != null) {
				    //considera grupos no c�lculo das coordenadas reais do objetivo
				    KpiMapaGrupoRenderer mapGroup = renderGroups.get(objective.getGroup().getId());
				    p = mapGroup.relativeToRealCoords(p);
				    p.translate(mapGroup.getX(), mapGroup.getY());
				}
			    } else if (elementModel instanceof KpiMapaGrupo) {
				glassPanel.setShadowBox((KpiMapaGrupo) elementModel, renderGroups.get(elementModel.getId()));
			    }

			    //handle considera coordenadas relativas dentro da perspectiva (para limitar movimenta��o)
			    dragHandle = new Rectangle(p.getLocation(), element.getSize());

			    //considera perspectiva nas coordenadas reais do objetivo
			    p = mapPerspective.relativeToRealCoords(p);
			    p.translate(mapPerspective.getX(), mapPerspective.getY());

			    //posiciona sombra sobre o objeto
			    glassPanel.translateShadowBox(p.x - elementModel.getPosX(), p.y - elementModel.getPosY());
			    glassPanel.setShadowBoxColor(Color.BLACK);
			    repaintGlass();

			    mouseAction = MapMouseAction.DRAG;
			}
		    } else if (element instanceof KpiMapaPerspectivaRenderer) {
			Point p = mEvt.getPoint();
			p.translate(element.getX(), element.getY());

			linkElement = null;

			//testa presen�a de linhas
			for (KpiMapaLigacaoRenderer arrow : arrowsMap.values()) {
			    if (arrow.ptSegDist(p) <= KpiMapaLigacaoRenderer.CONTROLBOXSIZE) {
				linkElement = map.getLink(arrow.getId());

				mouseAction = MapMouseAction.CURVELINK;

				break;
			    }
			}

			//n�o clicou em linha
			if (mouseAction != MapMouseAction.CURVELINK) {
			    Point mPoint = mEvt.getPoint();

			    //verifica clique na �rea de resize
			    if (mPoint.getY() >= element.getHeight() - MAP_RESIZEBORDER) {
				mouseAction = MapMouseAction.RESIZE_DOWN;
			    }

			    dragElement = element;
			}
		    }

		} else if (toolAction == MapToolAction.DRAWLINK) {

		    if (element instanceof KpiMapaObjetivoRenderer || element instanceof KpiMapaGrupoRenderer) {
			KpiMapaElemento elementModel = map.getElement(element.getId());

			linkElement = new KpiMapaLigacao();
			linkElement.setId(KpiMapaLigacao.generateId());
			linkElement.setType(KpiMapaLigacao.LINE);
			linkElement.setSource(elementModel);

			selectElement(element);

			mouseAction = MapMouseAction.DRAWLINK;
		    }
		}
	    } else if (mEvt.getButton() == MouseEvent.BUTTON3) {
		JPopupMenu mnu = null;
		Point p = new Point();

		if (element instanceof KpiMapaObjetivoRenderer) {
		    //clicou em objetivo
		    KpiMapaObjetivo objective = (KpiMapaObjetivo) map.getElement(element.getId());
		    mnu = mapForm.getPopupMenu(objective);

		    p = getAbsoluteCoords(element);
		    p.translate(mEvt.getX(), mEvt.getY());

		} else if (element instanceof KpiMapaGrupoRenderer) {
		    //clicou em agrupamento
		    KpiMapaGrupo group = (KpiMapaGrupo) map.getElement(element.getId());
		    mnu = mapForm.getPopupMenu(group);

		    p = getAbsoluteCoords(element);
		    p.translate(mEvt.getX(), mEvt.getY());

		} else if (element instanceof KpiMapaPerspectivaRenderer) {
		    p = mEvt.getPoint();
		    p.translate(element.getX(), element.getY());

		    KpiMapaLigacao link = null;

		    //testa presen�a de linhas
		    for (KpiMapaLigacaoRenderer arrow : arrowsMap.values()) {
			if (arrow.ptSegDist(p) <= KpiMapaLigacaoRenderer.CONTROLBOXSIZE) {
			    link = map.getLink(arrow.getId());

			    break;
			}
		    }

		    if (link == null) {
			//clicou em perspectiva
			KpiMapaPerspectiva perspective = map.getPerspective(element.getId());
			mnu = mapForm.getPopupMenu(perspective);
		    } else {
			//clicou em linha
			mnu = mapForm.getPopupMenu(link);
		    }
		}

		if (mnu != null) {
		    mnu.show(glassPanel, p.x, p.y);
		}
	    }
	}
    }

    @Override
    public void kpiMouseReleased(KpiMouseEvent e) {
	if (status == KpiDefaultFrameBehavior.UPDATING) {
	    MouseEvent mEvt = e.getEvent();

	    if (mEvt.getButton() == MouseEvent.BUTTON1) {
		if (mouseAction == MapMouseAction.DRAG) {

		    Point pNew = dragHandle.getLocation();
		    KpiMapaElemento elementModel = map.getElement(dragElement.getId());

		    if (dragElement instanceof KpiMapaObjetivoRenderer) {
			KpiMapaObjetivo objective = (KpiMapaObjetivo) elementModel;

			boolean testGroups = true;
			boolean addToPerspective = false;

			//objetivo dentro de um grupos (na posi��o original)
			if (objective.getGroup() != null) {
			    KpiMapaGrupo group = objective.getGroup();
			    KpiMapaGrupoRenderer mapGroup = renderGroups.get(group.getId());

			    Rectangle rGroup = mapGroup.getInnerBounds();

			    //verifica se houve mudan�a de grupo
			    if (rGroup.contains(dragHandle)) {
				//n�o houve mudan�a
				pNew.translate(-mapGroup.getX(), -mapGroup.getY());
				pNew = mapGroup.realToRelativeCoords(pNew);
				testGroups = false;
			    } else {
				//remove objetivo do grupo anterior
				group.removeObjective(objective);
				addToPerspective = true;
			    }
			}

			if (testGroups) {
			    KpiMapaPerspectiva perspective = elementModel.getPerspective();
			    KpiMapaGrupoRenderer mapGroup = null;

			    Rectangle rectObjective = dragHandle.getBounds();

			    //verifica se objetivo foi solto dentro de um grupo
			    for (KpiMapaElemento elm : perspective.getElements().values()) {
				if (elm instanceof KpiMapaGrupo) {
				    mapGroup = renderGroups.get(elm.getId());

				    if (mapGroup.getInnerBounds().contains(rectObjective)) {
					KpiMapaGrupo group = (KpiMapaGrupo) elm;

					pNew.translate(-mapGroup.getX(), -mapGroup.getY());
					pNew = mapGroup.realToRelativeCoords(pNew);

					mapGroup.addComponent(dragElement);
					group.addObjective(objective);

					//se existia link entre objetivo e grupo, remove
					map.removeLinkIfExists(objective, group);

					addToPerspective = false;

					break;
				    }
				}
			    }

			    //objetivo saiu do grupo antigo e foi solto na perspectiva
			    if (addToPerspective) {
				KpiMapaPerspectivaRenderer mapPerspective = renderPerspectives.get(perspective.getId());
				mapPerspective.addComponent(dragElement);
			    }
			}
		    }

		    dragElement.setLocation(pNew);
		    elementModel.setPosX(dragElement.getX());
		    elementModel.setPosY(dragElement.getY());

		    dragElement.setVisible(true);

		    dragElement = null;

		} else if (mouseAction == MapMouseAction.RESIZE_DOWN) {
		    dragElement = null;

		} else if (mouseAction == MapMouseAction.RESIZE_UP) {
		    dragElement = null;

		} else if (mouseAction == MapMouseAction.RESIZE_LEFT) {
		    dragElement = null;

		} else if (mouseAction == MapMouseAction.RESIZE_RIGHT) {
		    dragElement = null;

		} else if (mouseAction == MapMouseAction.DRAWGROUP) {

		    KpiMapaDraggableElement element = (KpiMapaDraggableElement) e.getSource();
		    KpiMapaGrupo group = new KpiMapaGrupo();
		    KpiMapaGrupoRenderer mapGroup = new KpiMapaGrupoRenderer();
		    KpiMapaPerspectiva perspective = map.getPerspective(element.getId());
		    KpiMapaPerspectivaRenderer mapPerspective = renderPerspectives.get(element.getId());
		    String idGroup = KpiMapaGrupo.generateId();

		    Point p1 = new Point(mousePressedPoint);
		    Point p2 = element.moveConstraintRealCoords(new Point(mEvt.getX(), mEvt.getY()));
		    int aux = 0;

		    if (p1.x > p2.x) {
			aux = p1.x;
			p1.x = p2.x;
			p2.x = aux;
		    }

		    if (p1.y > p2.y) {
			aux = p1.y;
			p1.y = p2.y;
			p2.y = aux;
		    }

		    if (p2.x - p1.x >= mapGroup.getMinWidth() && p2.y - p1.y >= mapGroup.getMinHeight()) {

			p1 = mapPerspective.realToRelativeCoords(p1);
			p2 = mapPerspective.realToRelativeCoords(p2);

			group.setId(idGroup);
			group.setName(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFormController_00001"));
			group.setPosX(p1.x);
			group.setPosY(p1.y);
			group.setWidth(p2.x - p1.x);
			group.setHeight(p2.y - p1.y);

			perspective.addElement(group);

			mapGroup.setId(group.getId());
			mapGroup.setMouseController(this);
			mapGroup.setBounds(group.getPosX(), group.getPosY(), group.getWidth(), group.getHeight());
			mapGroup.setPreferredSize(new Dimension(group.getWidth(), group.getHeight()));
			mapGroup.setBackColor(group.getBackColor());
			mapGroup.setFontColor(group.getFontColor());
			mapGroup.setOpaqueColor(group.isOpaque());
			mapGroup.setTitle(group.getName());

			//armazena tabela de lookup de grupos
			renderGroups.put(group.getId(), mapGroup);

			//adiciona grupo na perspectiva
			mapPerspective.addComponent(mapGroup);

			Rectangle rGroup = mapGroup.getInnerBounds();

			//localiza elementos que est�o contidos na �rea do novo grupo
			HashMap<String, KpiMapaElemento> elementsIntersect = perspective.getElementsIntersect(rGroup);

			//percorre elementos na �rea delimitada
			for (KpiMapaElemento elm : elementsIntersect.values()) {
			    //desconsidera grupos
			    if (elm instanceof KpiMapaObjetivo) {
				KpiMapaObjetivo objective = (KpiMapaObjetivo) elm;

				//desconsidera objetivos que est�o em outros grupos
				if (objective.getGroup() == null) {
				    KpiMapaObjetivoRenderer mapObjective = renderObjectives.get(elm.getId());
				    Point pObj = mapObjective.getLocation();

				    //ajusta coordenadas
				    pObj.translate(-mapGroup.getX(), -mapGroup.getY());

				    //ajusta borda
				    pObj = mapGroup.realToRelativeCoords(pObj);

				    objective.setPosX(pObj.x);
				    objective.setPosY(pObj.y);

				    mapObjective.setLocation(pObj);

				    //adiciona elemento no grupo
				    mapGroup.addComponent(mapObjective);
				    group.addObjective(objective);
				}
			    }
			}
		    }
		} else if (mouseAction == MapMouseAction.DRAWLINK) {
		    if (linkElement.getDestination() != null) {
			map.addLink(linkElement);
		    }

		    linkElement = null;
		}

		unselectAllElements();

		mouseAction = MapMouseAction.NOP;

		glassPanel.clearShadowBox();
		glassPanel.clearShadowLine();
		repaintGlass();
	    }
	}
    }

    @Override
    public void kpiMouseDragged(KpiMouseEvent e) {
	if (status == KpiDefaultFrameBehavior.UPDATING) {
	    MouseEvent mEvt = e.getEvent();

	    if (mouseAction == MapMouseAction.DRAG) {
		Point newPoint = dragElement.getLocation();
		newPoint.translate((int) (mEvt.getPoint().getX() - mousePressedPoint.getX()), (int) (mEvt.getPoint().getY() - mousePressedPoint.getY()));

		KpiMapaElemento elementModel = map.getElement(dragElement.getId());

		//se for um objetivo dentro de um grupo, ajusta coordenadas para relativas � perspectiva
		if (elementModel instanceof KpiMapaObjetivo) {
		    KpiMapaObjetivo objective = (KpiMapaObjetivo) elementModel;

		    if (objective.getGroup() != null) {
			//considera grupo no c�lculo das coordenadas reais do objetivo
			KpiMapaGrupoRenderer mapGroup = renderGroups.get(objective.getGroup().getId());
			newPoint = mapGroup.relativeToRealCoords(newPoint);
			newPoint.translate(mapGroup.getX(), mapGroup.getY());
		    }
		}

		KpiMapaElemento modelElement = map.getElement(dragElement.getId());
		KpiMapaPerspectiva perspective = modelElement.getPerspective();
		KpiMapaPerspectivaRenderer mapPerspective = renderPerspectives.get(perspective.getId());

		Point oldPoint = dragHandle.getLocation();

		newPoint = mapPerspective.moveConstraintRelativeCoords(new Rectangle(newPoint, dragHandle.getSize()));

		dragHandle.setLocation(newPoint);

		//verifica se objetivo est� em �rea ativa (grupo)
		if (elementModel instanceof KpiMapaObjetivo) {
		    unselectAllElements();

		    boolean testGroups = true;

		    KpiMapaObjetivo objective = (KpiMapaObjetivo) elementModel;
		    Rectangle rectObjective = dragHandle.getBounds();

		    if (objective.getGroup() != null) {
			KpiMapaGrupoRenderer mapGroup = renderGroups.get(objective.getGroup().getId());

			if (mapGroup.getInnerBounds().contains(rectObjective)) {
			    selectElement(mapGroup);

			    testGroups = false;
			}
		    }

		    Iterator<KpiMapaElemento> itElm = perspective.getElements().values().iterator();
		    //percorre elementos
		    while (testGroups && itElm.hasNext()) {
			KpiMapaElemento elm = itElm.next();

			if (elm instanceof KpiMapaGrupo) {
			    KpiMapaGrupoRenderer mapGroup = renderGroups.get(elm.getId());

			    if (mapGroup.getInnerBounds().contains(rectObjective)) {
				selectElement(mapGroup);

				testGroups = false;
			    }
			}
		    }
		}

		if (MAP_RESETLINESONMOVE) {
		    resetLinesType(modelElement);
		}

		glassPanel.translateShadowBox(newPoint.x - oldPoint.x, newPoint.y - oldPoint.y);
		glassPanel.clearShadowLine();
		repaintGlass();

	    } else if (mouseAction == MapMouseAction.RESIZE_UP) {
		Point pMouse = mEvt.getPoint();

		int deltaY = -((int) pMouse.getY());

		Dimension d = new Dimension(dragElement.getWidth(), dragElement.getHeight() + deltaY);

		Point p = dragElement.getLocation();
		p.translate(0, -deltaY);

		resizeElement(dragElement, d, p);

	    } else if (mouseAction == MapMouseAction.RESIZE_DOWN) {
		Point pMouse = mEvt.getPoint();

		int deltaY = (dragElement.getHeight() - (int) pMouse.getY());

		Dimension d = new Dimension(dragElement.getWidth(), dragElement.getHeight() - deltaY);

		resizeElement(dragElement, d, dragElement.getLocation());

	    } else if (mouseAction == MapMouseAction.RESIZE_LEFT) {
		Point pMouse = mEvt.getPoint();

		int deltaX = -((int) pMouse.getX());

		Dimension d = new Dimension(dragElement.getWidth() + deltaX, dragElement.getHeight());

		Point p = dragElement.getLocation();
		p.translate(-deltaX, 0);

		resizeElement(dragElement, d, p);

	    } else if (mouseAction == MapMouseAction.RESIZE_RIGHT) {
		Point pMouse = mEvt.getPoint();

		int deltaX = (dragElement.getWidth() - (int) pMouse.getX());

		Dimension d = new Dimension(dragElement.getWidth() - deltaX, dragElement.getHeight());

		resizeElement(dragElement, d, dragElement.getLocation());

	    } else if (mouseAction == MapMouseAction.DRAWGROUP) {

		KpiMapaDraggableElement element = (KpiMapaDraggableElement) e.getSource();

		Point p1 = new Point(mousePressedPoint);
		Point p2 = element.moveConstraintRealCoords(new Point(mEvt.getX(), mEvt.getY()));
		int aux = 0;

		if (p1.x > p2.x) {
		    aux = p1.x;
		    p1.x = p2.x;
		    p2.x = aux;
		}

		if (p1.y > p2.y) {
		    aux = p1.y;
		    p1.y = p2.y;
		    p2.y = aux;
		}

		unselectAllElements();

		KpiMapaGrupoRenderer tmpMapGroup = new KpiMapaGrupoRenderer();
		tmpMapGroup.setTitle("X"); //Necess�rio para o correto c�culo de "insets" com t�tulo

		Insets insetsGroup = tmpMapGroup.getInsets();
		Rectangle rGroup = new Rectangle(p1.x + insetsGroup.left,
			p1.y + insetsGroup.top,
			p2.x - p1.x - insetsGroup.left - insetsGroup.right,
			p2.y - p1.y - insetsGroup.top - insetsGroup.bottom);

		rGroup = element.realToRelativeCoords(rGroup);

		if (p2.x - p1.x >= tmpMapGroup.getMinWidth() && p2.y - p1.y >= tmpMapGroup.getMinHeight()) {
		    KpiMapaPerspectiva perspective = map.getPerspective(element.getId());

		    //localiza elementos que est�o contidos na �rea do novo grupo
		    HashMap<String, KpiMapaElemento> elementsIntersect = perspective.getElementsIntersect(rGroup);

		    //percorre elementos na �rea delimitada
		    for (KpiMapaElemento elm : elementsIntersect.values()) {
			//desconsidera grupos
			if (elm instanceof KpiMapaObjetivo) {
			    KpiMapaObjetivo objective = (KpiMapaObjetivo) elm;

			    //desconsidera objetivos que est�o em outros grupos
			    if (objective.getGroup() == null) {
				KpiMapaObjetivoRenderer mapObjective = renderObjectives.get(elm.getId());

				selectElement(mapObjective);
			    }
			}
		    }

		    glassPanel.setShadowBoxColor(Color.BLACK);
		} else {
		    //�rea do grupo menor que �rea m�nima permitida
		    glassPanel.setShadowBoxColor(Color.RED);
		}

		p1.translate(element.getX(), element.getY());
		p2.translate(element.getX(), element.getY());

		glassPanel.clearShadowBox();
		glassPanel.addShadowBox(p1.x, p1.y, p2.x - p1.x, p2.y - p1.y);
		glassPanel.addShadowBox(p1.x + insetsGroup.left, p1.y + insetsGroup.top, p2.x - p1.x - insetsGroup.left - insetsGroup.right, p2.y - p1.y - insetsGroup.top - insetsGroup.bottom);

		repaintGlass();

	    } else if (mouseAction == MapMouseAction.DRAWLINK) {
		KpiMapaDraggableElement element = (KpiMapaDraggableElement) e.getSource();

		Point p1 = getAbsoluteCoords(linkElement.getSource());
		p1.translate(linkElement.getSource().getWidth() / 2, linkElement.getSource().getHeight() / 2);

		Point p2 = new Point((int) mEvt.getX(), (int) mEvt.getY());
		p2.translate(element.getX(), element.getY());

		KpiMapaPerspectivaRenderer mapPerspective;
		if (element instanceof KpiMapaObjetivoRenderer) {
		    KpiMapaElemento mapElement = map.getElement(element.getId());
		    KpiMapaPerspectiva perspective = mapElement.getPerspective();
		    mapPerspective = renderPerspectives.get(perspective.getId());

		    p2.translate(mapPerspective.getX(), mapPerspective.getY());
		    p2 = mapPerspective.relativeToRealCoords(p2);

		    if (mapElement instanceof KpiMapaObjetivo) {
			KpiMapaGrupo group = ((KpiMapaObjetivo) mapElement).getGroup();
			if (group != null) {
			    KpiMapaGrupoRenderer mapGroup = renderGroups.get(group.getId());
			    p2.translate(mapGroup.getX(), mapGroup.getY());
			    p2 = mapGroup.relativeToRealCoords(p2);
			}
		    }
		} else if (element instanceof KpiMapaGrupoRenderer) {
		    KpiMapaElemento mapElement = map.getElement(element.getId());
		    KpiMapaPerspectiva perspective = mapElement.getPerspective();
		    mapPerspective = renderPerspectives.get(perspective.getId());

		    p2.translate(mapPerspective.getX(), mapPerspective.getY());
		    p2 = mapPerspective.relativeToRealCoords(p2);
		} else if (element instanceof KpiMapaGrupoRenderer) {
		    p2 = element.relativeToRealCoords(p2);
		}

		glassPanel.setShadowLine(p1, p2);
		repaintGlass();

	    } else if (mouseAction == MapMouseAction.CURVELINK) {
		KpiMapaDraggableElement element = (KpiMapaDraggableElement) e.getSource();

		Point p = mEvt.getPoint();
		p.translate(element.getX(), element.getY());

		int margin = KpiMapaLigacaoRenderer.CONTROLBOXSIZE + 5;

		if (p.x > (glassPanel.getWidth() - margin)) {
		    p.x = (glassPanel.getWidth() - margin);
		} else if (p.x < margin) {
		    p.x = margin;
		}

		if (p.y > (glassPanel.getHeight() - margin)) {
		    p.y = (glassPanel.getHeight() - margin);
		} else if (p.y < margin) {
		    p.y = margin;
		}

		linkElement.setControlPoint(p);
		linkElement.setType(KpiMapaLigacao.CURVE);

		repaintGlass();
	    }
	}
    }

    @Override
    public void kpiMouseMoved(KpiMouseEvent e) {
	if (status == KpiDefaultFrameBehavior.UPDATING) {

	    if (toolAction == MapToolAction.DELETE && mouseAction == MapMouseAction.NOP) {
		MouseEvent mEvt = e.getEvent();
		KpiMapaDraggableElement element = (KpiMapaDraggableElement) e.getSource();

		int cursor = Cursor.DEFAULT_CURSOR;

		if (element instanceof KpiMapaPerspectivaRenderer) {

		    Point p = mEvt.getPoint();
		    p.translate(element.getX(), element.getY());

		    //desmarca todos os elementos
		    unselectAllElements();
		    for (KpiMapaLigacaoRenderer arrow : arrowsMap.values()) {
			if (arrow.ptSegDist(p) <= KpiMapaLigacaoRenderer.CONTROLBOXSIZE) {

			    selectElement(arrow);
			    cursor = Cursor.HAND_CURSOR;

			    break;
			}
		    }

		    //n�o pode chamar o reapintGlass para n�o destruir as linhas renderizadas
		    glassPanel.repaint();

		} else if (element instanceof KpiMapaGrupoRenderer) {
		    cursor = Cursor.HAND_CURSOR;
		}

		setMapaCursor(new Cursor(cursor));

	    } else if (toolAction == MapToolAction.MOVE && mouseAction == MapMouseAction.NOP) {
		MouseEvent mEvt = e.getEvent();
		KpiMapaDraggableElement element = (KpiMapaDraggableElement) e.getSource();

		int cursor = Cursor.DEFAULT_CURSOR;

		if (element instanceof KpiMapaPerspectivaRenderer) {
		    Point p = mEvt.getPoint();

		    p.translate(element.getX(), element.getY());

		    boolean arrowClick = false;

		    //desmarca todos os elementos
		    unselectAllElements();
		    for (KpiMapaLigacaoRenderer arrow : arrowsMap.values()) {
			if (arrow.ptSegDist(p) <= KpiMapaLigacaoRenderer.CONTROLBOXSIZE) {
			    selectElement(arrow);

			    cursor = Cursor.MOVE_CURSOR;

			    arrowClick = true;

			    break;
			}
		    }

		    if (!arrowClick) {
			Point mPoint = mEvt.getPoint();

			//verifica clique na �rea de resize
			if (mPoint.getY() >= element.getHeight() - MAP_RESIZEBORDER) {
			    cursor = Cursor.S_RESIZE_CURSOR;
			}
		    }

		    setMapaCursor(new Cursor(cursor));

		    //n�o pode chamar o repaintGlass para n�o destruir as linhas renderizadas
		    glassPanel.repaint();

		} else if (element instanceof KpiMapaObjetivoRenderer || element instanceof KpiMapaGrupoRenderer) {

		    Point mPoint = mEvt.getPoint();

		    cursor = Cursor.MOVE_CURSOR;

		    //verifica clique na �rea de resize
		    if (mPoint.getY() >= element.getHeight() - MAP_RESIZEBORDER) {
			cursor = Cursor.S_RESIZE_CURSOR;
		    } else if (mPoint.getY() <= MAP_RESIZEBORDER) {
			cursor = Cursor.N_RESIZE_CURSOR;
		    } else if (mPoint.getX() <= MAP_RESIZEBORDER) {
			cursor = Cursor.W_RESIZE_CURSOR;
		    } else if (mPoint.getX() >= element.getWidth() - MAP_RESIZEBORDER) {
			cursor = Cursor.E_RESIZE_CURSOR;
		    }
		}

		setMapaCursor(new Cursor(cursor));
	    }
	}
    }

    /**
     * Render Mapa Estrat�gico
     * @param KpiMapaEstrategico
     */
    public void renderMap() throws MalformedURLException, IOException {
	//remove todos os componentes
	mapForm.clearMap();

	renderPerspectives.clear();
	renderObjectives.clear();
	renderGroups.clear();

	TreeMap<String, KpiMapaPerspectivaRenderer> orderedPerspectives = new TreeMap();

	//Perspectivas
	for (KpiMapaPerspectiva perspective : map.getPerspectives().values()) {

	    HashMap<String, String> objectivesInsideGroups = new HashMap();

	    KpiMapaPerspectivaRenderer mapPerspective = new KpiMapaPerspectivaRenderer();

	    mapPerspective.setId(perspective.getId());

	    mapPerspective.setMouseController(this);

	    mapPerspective.setSize(new Dimension(MAP_WIDTH, perspective.getHeight()));
	    mapPerspective.setPreferredSize(new Dimension(MAP_WIDTH, perspective.getHeight()));
	    mapPerspective.setFontColor(perspective.getFontColor());
	    mapPerspective.setBackground(perspective.getBackColor());
	    mapPerspective.setImage(perspective.getImage(), "1");
	    mapPerspective.setTitle(perspective.getName());

	    int x = 0;
	    int y = 0;
	    int auxX = 10;
	    int auxY = 10;

	    //Elementos
	    for (KpiMapaElemento element : perspective.getElements().values()) {

		if (element instanceof KpiMapaObjetivo) {
		    //Objetivo
		    KpiMapaObjetivo objective = (KpiMapaObjetivo) element;
		    KpiMapaObjetivoRenderer mapObjective = new KpiMapaObjetivoRenderer();

		    mapObjective.setId(objective.getId());

		    mapObjective.setMouseController(this);

		    mapObjective.setSize(element.getWidth(), element.getHeight());
		    mapObjective.setPreferredSize(new Dimension(element.getWidth(), element.getHeight()));
		    mapObjective.setBackColor(element.getBackColor());
		    mapObjective.setFontColor(element.getFontColor());
		    mapObjective.setOpaqueColor(element.isOpaque());
		    mapObjective.setVisibleName(element.isVisibleName());
		    mapObjective.setTitle(element.getName());
		    mapObjective.setShape(element.getShape());

		    //armazena tabela de lookup de objetivos
		    renderObjectives.put(element.getId(), mapObjective);

		    if (objective.getGroup() != null) {
			//objetivo dentro de um grupo
			mapObjective.setLocation(new Point(element.getPosX(), element.getPosY()));

			objectivesInsideGroups.put(objective.getId(), objective.getGroup().getId());
		    } else {
			//como objetivos novos podem n�o ter posi��o definida, reinicia coordenada com valores padr�es
			x = element.getPosX();
			if (x < 0) {
			    x = auxX;
			    auxX += 10;
			}

			y = element.getPosY();
			if (y < 0) {
			    y = auxY;
			    auxY += 10;
			}

			mapObjective.setLocation(x, y);
			element.setPosX(x);
			element.setPosY(y);

			//adiciona objetivo na perspectiva
			mapPerspective.addComponent(mapObjective);
		    }

		} else if (element instanceof KpiMapaGrupo) {
		    //Groupo
		    KpiMapaGrupoRenderer mapGroup = new KpiMapaGrupoRenderer();

		    mapGroup.setId(element.getId());

		    mapGroup.setMouseController(this);

		    mapGroup.setBounds(element.getPosX(), element.getPosY(), element.getWidth(), element.getHeight());
		    mapGroup.setPreferredSize(new Dimension(element.getWidth(), element.getHeight()));
		    mapGroup.setBackColor(element.getBackColor());
		    mapGroup.setFontColor(element.getFontColor());
		    mapGroup.setOpaqueColor(element.isOpaque());
		    mapGroup.setTitle(element.getName());

		    //armazena tabela de lookup de grupos
		    renderGroups.put(element.getId(), mapGroup);

		    //adiciona grupo na perspectiva
		    mapPerspective.addComponent(mapGroup);
		}
	    }

	    for (String keyObjective : objectivesInsideGroups.keySet()) {
		String keyGroup = objectivesInsideGroups.get(keyObjective);

		KpiMapaObjetivoRenderer mapObjective = renderObjectives.get(keyObjective);
		KpiMapaGrupoRenderer mapGroup = renderGroups.get(keyGroup);

		//Adiciona objetivo no grupo
		mapGroup.addComponent(mapObjective);
	    }

	    //ordena perspectivas
	    orderedPerspectives.put(String.format("%03d", perspective.getOrder()).concat(perspective.getId()), mapPerspective);

	    //armazena tabela de lookup de perspectivas
	    renderPerspectives.put(perspective.getId(), mapPerspective);
	}

	//Adiciona perspectivas no mapa
	for (KpiMapaPerspectivaRenderer persp : orderedPerspectives.values()) {
	    addPerspective(persp);
	}

	mapRevalidate();
    }

    /**
     * Adiciona componentes no container
     * @param element
     */
    private void addPerspective(KpiMapaPerspectivaRenderer element) {

	int maxHeight = element.getPreferredSize().height;

	for (Component subElement : element.components()) {

	    Point p = subElement.getLocation();
	    Point pNew = element.moveConstraintRelativeCoords(subElement.getBounds());

	    if (p.y != pNew.y) {
		maxHeight += Math.abs(pNew.y - p.y);
	    }
	}

	if (maxHeight != element.getPreferredSize().height) {
	    Dimension d = new Dimension(element.getWidth(), maxHeight);
	    element.setSize(d);
	    element.setPreferredSize(d);

	    KpiMapaPerspectiva perspective = map.getPerspective(element.getId());
	    perspective.setHeight(maxHeight);
	}

	mapForm.addMapComponent(element);
    }

    /**
     * Remove componente do container
     * @param element
     */
    private void removeComponent(java.awt.Component element) {
	mapForm.removeMapComponent(element);
    }

    private void mapRevalidate() {

	mapForm.revalidateMap();

	repaintGlass();
    }

    /**
     * Monta estrutura do mapa estrat�gico
     * @param recordMap XML que ser� convertido no mapa estrat�gico
     */
    public void xmlToMap(BIXMLRecord recordMap) {
	//Monta mapa com dados b�sicos de um XML
	map = KpiMapaEstrategico.mapFromXml(recordMap);

	themes = recordMap.getBIXMLVector("TEMASESTRATEGICOS");
    }

    /**
     * Monta estrutura XML do mapa estrat�gico
     */
    public BIXMLRecord mapToXml() {
	BIXMLRecord mapXml = KpiMapaEstrategico.xmlFromMap(map);

	return mapXml;
    }

    /**
     * Atualiza status dos objetivos
     */
    public void refreshElements(Calendar date, String theme) {
	BIRequest request = new BIRequest();
	KpiDataController dataController = KpiStaticReferences.getKpiDataController();

	request.setParameter("TIPO", "MAPAESTRATEGICO");
	request.setParameter("MAPACMD", "STOBJETIVOS");
	request.setParameter("MAPAESTID", map.getId());
	request.setParameter("DATAALVO", KpiDateUtil.calendarToString(date));
	request.setParameter("TEMA", theme);

	BIXMLRecord rec = dataController.listRecords(request);

	//atualiza status dos objetivos
	for (BIXMLRecord statusXml : rec.getBIXMLVector("MAPAOBJETIVOS_STATUS")) {
	    KpiMapaObjetivoRenderer objective = renderObjectives.get(statusXml.getString("MAPAOBJETIVO_ID"));
	    objective.setStatus(statusXml.getInt("OBJETIVO_STATUS"));
	    objective.setVisible(statusXml.getBoolean("OBJETIVO_VISIBLE"));
	}

	//atualiza visibilidade dos grupos
	for (KpiMapaGrupoRenderer group : renderGroups.values()) {
	    int qtdObjective = group.components().length;

	    if (qtdObjective > 0) {
		for (Component objective : group.components()) {
		    if (!objective.isVisible()) {
			qtdObjective--;
		    }
		}

		if (qtdObjective == 0) {
		    group.setVisible(false);
		} else {
		    group.setVisible(true);
		}
	    }
	}

	repaintGlass();
    }

    private void unselectAllElements() {
	//unselect elements
	for (KpiMapaDraggableElement element : selectedElements.values()) {
	    element.setSelected(false);
	}

	unselectAllLinks();

	selectedElements.clear();
    }

    private void unselectAllLinks() {
	//unselect links
	for (KpiMapaLigacaoRenderer element : selectedLinks.values()) {
	    element.setSelected(false);
	}

	selectedLinks.clear();
    }

    private void unSelectElement(KpiMapaDraggableElement element) {
	element.setSelected(false);
	selectedElements.remove(element.getId());
    }

    private void selectElement(KpiMapaDraggableElement element) {
	element.setSelected(true);
	selectedElements.put(element.getId(), element);
    }

    private void selectElement(KpiMapaLigacaoRenderer element) {
	element.setSelected(true);
	selectedLinks.put(element.getId(), element);
    }

    private void repaintGlass() {
	renderLinks();

	glassPanel.setDivisions(map.getQtdDivision());
	glassPanel.setArrows(arrowsMap);
	glassPanel.repaint();
    }

    private Point getAbsoluteCoords(KpiMapaDraggableElement mapElement) {
	KpiMapaElemento element = map.getElement(mapElement.getId());
	return getAbsoluteCoords(element);
    }

    private Point getAbsoluteCoords(KpiMapaElemento element) {
	Point p1 = new Point(element.getPosX(), element.getPosY());

	if (element instanceof KpiMapaObjetivo) {
	    KpiMapaObjetivo objective = (KpiMapaObjetivo) element;
	    KpiMapaGrupo group = objective.getGroup();

	    if (group != null) {
		//convete coordenadas do ponto inicial em rela��o ao grupo
		KpiMapaDraggableElement mapGroup = renderGroups.get(group.getId());

		p1 = mapGroup.relativeToRealCoords(p1);
		p1.translate(mapGroup.getX(), mapGroup.getY());
	    }
	}

	KpiMapaPerspectivaRenderer mapPerspective = renderPerspectives.get(element.getPerspective().getId());

	//convete coordenadas do ponto inicial em rela��o � perspectiva
	p1 = mapPerspective.relativeToRealCoords(p1);
	p1.translate(mapPerspective.getX(), mapPerspective.getY());

	return p1;
    }

    private void renderLinks() {
	// Prepara lista de links com coordenadas absolutas
	arrowsMap.clear();

	for (KpiMapaLigacao link : map.getLinks().values()) {
	    boolean visible = true;
	    boolean srcVisibleParent = true;
	    boolean dstVisibleParent = true;

	    //source
	    KpiMapaDraggableElement srcMapElement;

	    KpiMapaElemento srcElement = map.getElement(link.getSource().getId());

	    if (srcElement instanceof KpiMapaObjetivo) {
		srcMapElement = renderObjectives.get(link.getSource().getId());

		KpiMapaGrupo group = ((KpiMapaObjetivo) srcElement).getGroup();
		if (group != null) {
		    srcVisibleParent = renderGroups.get(group.getId()).isVisible();
		}
	    } else {
		srcMapElement = renderGroups.get(link.getSource().getId());
	    }

	    //destination
	    KpiMapaDraggableElement dstMapElement;

	    KpiMapaElemento dstElement = map.getElement(link.getDestination().getId());

	    if (dstElement instanceof KpiMapaObjetivo) {
		dstMapElement = renderObjectives.get(link.getDestination().getId());

		KpiMapaGrupo group = ((KpiMapaObjetivo) dstElement).getGroup();
		if (group != null) {
		    dstVisibleParent = renderGroups.get(group.getId()).isVisible();
		}
	    } else {
		dstMapElement = renderGroups.get(link.getDestination().getId());
	    }

	    visible = visible && srcMapElement.isVisible();
	    visible = visible && dstMapElement.isVisible();
	    visible = visible && srcVisibleParent;
	    visible = visible && dstVisibleParent;

	    if (visible || dragElement != null) {
		//pontos iniciais absolutos dos objetos
		Point p1;
		Point p2;

		if (visible) {
		    p1 = getAbsoluteCoords(srcElement);
		    p2 = getAbsoluteCoords(dstElement);
		} else {
		    if (dragElement == srcMapElement) {
			KpiMapaPerspectivaRenderer mapPerspective = renderPerspectives.get(srcElement.getPerspective().getId());

			p1 = dragHandle.getLocation();
			p1 = mapPerspective.relativeToRealCoords(p1);
			p1.translate(mapPerspective.getX(), mapPerspective.getY());

			p2 = getAbsoluteCoords(dstElement);

		    } else if (dragElement == dstMapElement) {
			KpiMapaPerspectivaRenderer mapPerspective = renderPerspectives.get(dstElement.getPerspective().getId());

			p1 = getAbsoluteCoords(srcElement);

			p2 = dragHandle.getLocation();
			p2 = mapPerspective.relativeToRealCoords(p2);
			p2.translate(mapPerspective.getX(), mapPerspective.getY());

		    } else {
			//objetos dentro de um grupo sendo movido
			p1 = getAbsoluteCoords(srcElement);
			p2 = getAbsoluteCoords(dstElement);

			KpiMapaElemento dragMapElement = map.getElement(dragElement.getId());
			KpiMapaPerspectivaRenderer mapPerspective = renderPerspectives.get(dragMapElement.getPerspective().getId());

			Point pDragNew = mapPerspective.relativeToRealCoords(dragHandle.getLocation());
			pDragNew.translate(mapPerspective.getX(), mapPerspective.getY());

			Point pDragOlg = getAbsoluteCoords(dragElement);

			int deltaX = pDragNew.x - pDragOlg.x;
			int deltaY = pDragNew.y - pDragOlg.y;

			if (!srcVisibleParent) {
			    p1.translate(deltaX, deltaY);
			}

			if (!dstVisibleParent) {
			    p2.translate(deltaX, deltaY);
			}
		    }
		}

		Point pOrigem = new Point();
		Point pDestino = new Point();

		if (p1.y > p2.y + dstElement.getHeight()) {
		    pOrigem.x = p1.x + (srcElement.getWidth() / 2);
		    pOrigem.y = p1.y;
		    pDestino.x = p2.x + (dstElement.getWidth() / 2);
		    pDestino.y = p2.y + dstElement.getHeight();
		} else if (p1.y + srcElement.getHeight() < p2.y) {
		    pOrigem.x = p1.x + (srcElement.getWidth() / 2);
		    pOrigem.y = p1.y + srcElement.getHeight();
		    pDestino.x = p2.x + (dstElement.getWidth() / 2);
		    pDestino.y = p2.y;
		} else if (p1.x > p2.x + dstElement.getWidth()) {
		    pOrigem.x = p1.x;
		    pOrigem.y = p1.y + (srcElement.getHeight() / 2);
		    pDestino.x = p2.x + dstElement.getWidth();
		    pDestino.y = p2.y + (dstElement.getHeight() / 2);
		} else {
		    pOrigem.x = p1.x + srcElement.getWidth();
		    pOrigem.y = p1.y + (srcElement.getHeight() / 2);
		    pDestino.x = p2.x;
		    pDestino.y = p2.y + (dstElement.getHeight() / 2);
		}

		//ignora linha se um dos elementos n�o estiver vis�vel no momento (drag)
		if (visible) {
		    KpiMapaLigacaoRenderer arrow;

		    if (link.getType().equals(KpiMapaLigacao.CURVE)) {
			Point rCtrl = link.getControlPoint();
			arrow = new KpiMapaLigacaoRenderer(pOrigem, pDestino, rCtrl);
		    } else {
			arrow = new KpiMapaLigacaoRenderer(pOrigem, pDestino);

			//atualiza control point com o ponto central
			link.setControlPoint(arrow.getCenterPoint());
		    }

		    arrow.setId(link.getId());
		    arrow.setLineColor(link.getLineColor());
		    arrowsMap.put(link.getId(), arrow);
		} else {
		    if (MAP_RESETLINESONMOVE || link.getType().equals(KpiMapaLigacao.LINE)) {
			glassPanel.addShadowLine(pOrigem, pDestino);
		    } else {
			Point rCtrl = link.getControlPoint();
			glassPanel.addShadowLine(pOrigem, pDestino, rCtrl);
		    }
		}
	    }
	}
    }

    private boolean validLink(KpiMapaElemento source, KpiMapaElemento destination) {
	boolean valid = true;

	//verifica igualdade entre origem e destino
	valid = (source != destination);

	if (valid) {
	    //verifica liga��o Objetivo - Grupo
	    if (source instanceof KpiMapaObjetivo && destination instanceof KpiMapaGrupo) {
		KpiMapaElemento groupSource = ((KpiMapaObjetivo) source).getGroup();

		valid = destination != groupSource;
	    }
	}

	if (valid) {
	    //Verifica liga��o Grupo - Objetivo
	    if (source instanceof KpiMapaGrupo && destination instanceof KpiMapaObjetivo) {
		KpiMapaElemento groupDestination = ((KpiMapaObjetivo) destination).getGroup();

		valid = source != groupDestination;
	    }
	}

	//Verifica duplicidade de link
	Iterator<KpiMapaLigacao> itLink = map.getLinks().values().iterator();
	while (valid && itLink.hasNext()) {
	    KpiMapaLigacao link = itLink.next();

	    valid = ((link.getSource() != source || link.getDestination() != destination)
		    && (link.getSource() != destination || link.getDestination() != source));
	}

	return valid;

    }

    private void setMapaCursor(Cursor cursor) {
	glassPanel.setCursor(cursor);
    }

    private void resetLinesType(KpiMapaElemento element) {
	for (KpiMapaLigacao link : map.getLinks().values()) {
	    if (link.getSource() == element || link.getDestination() == element) {
		link.setType(KpiMapaLigacao.LINE);
	    }

	    if (element instanceof KpiMapaGrupo) {
		for (KpiMapaElemento child : ((KpiMapaGrupo) element).getObjectives().values()) {
		    resetLinesType(child);
		}
	    }
	}
    }

    private KpiMapaDraggableElement getParentElement(KpiMapaDraggableElement element) {
	KpiMapaDraggableElement parentElement = null;

	if (element instanceof KpiMapaObjetivoRenderer) {
	    KpiMapaObjetivo objective = (KpiMapaObjetivo) map.getElement(element.getId());

	    if (objective.getGroup() != null) {
		parentElement = renderGroups.get(objective.getGroup().getId());
	    } else {
		parentElement = renderPerspectives.get(objective.getPerspective().getId());
	    }

	} else if (element instanceof KpiMapaGrupoRenderer) {
	    KpiMapaGrupo group = (KpiMapaGrupo) map.getElement(element.getId());
	    parentElement = renderPerspectives.get(group.getPerspective().getId());
	}

	return parentElement;
    }

    private void resizeElement(KpiMapaDraggableElement element, Dimension d, Point p) {
	KpiMapaDraggableElement parentElement = getParentElement(element);

	if (element.validResize(d, p, parentElement)) {
	    element.setSize(d);
	    element.setPreferredSize(d);
	    element.setLocation(p);

	    if (element instanceof KpiMapaPerspectivaRenderer) {
		KpiMapaPerspectiva elementModel = map.getPerspective(element.getId());
		elementModel.setHeight(d.height);

		for (KpiMapaElemento elm : elementModel.getElements().values()) {
		    if (elm instanceof KpiMapaObjetivo) {
			//Considera objetivos que n�o estejam dentro de algum grupo
			if (((KpiMapaObjetivo) elm).getGroup() == null) {
			    KpiMapaObjetivoRenderer objectiveRenderer = renderObjectives.get(elm.getId());

			    Point pChild = element.moveConstraintRelativeCoords(elm.getBounds());
			    objectiveRenderer.setLocation(pChild);
			    elm.setPosX(pChild.x);
			    elm.setPosY(pChild.y);
			}
		    } else {
			//Grupos
			KpiMapaGrupoRenderer groupRenderer = renderGroups.get(elm.getId());

			Point pChild = element.moveConstraintRelativeCoords(elm.getBounds());
			groupRenderer.setLocation(pChild);
			elm.setPosX(pChild.x);
			elm.setPosY(pChild.y);
		    }
		}

	    } else {
		KpiMapaElemento elementModel = map.getElement(element.getId());
		elementModel.setPosX(p.x);
		elementModel.setPosY(p.y);
		elementModel.setHeight(d.height);
		elementModel.setWidth(d.width);

		if (elementModel instanceof KpiMapaGrupo) {
		    KpiMapaGrupo group = (KpiMapaGrupo) elementModel;
		    for (KpiMapaObjetivo elm : group.getObjectives().values()) {
			//Objetivos
			KpiMapaObjetivoRenderer objectiveRenderer = renderObjectives.get(elm.getId());

			Point pChild = element.moveConstraintRelativeCoords(elm.getBounds());
			objectiveRenderer.setLocation(pChild);
			elm.setPosX(pChild.x);
			elm.setPosY(pChild.y);
		    }
		}
	    }

	    mapRevalidate();
	}

    }

    /**
     * @return the status
     */
    public int getStatus() {
	return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
	this.status = status;
	glassPanel.setStatus(status);
    }

    public void setQtdDivision(int iDivisao) {
	map.setQtdDivision(iDivisao);

	repaintGlass();
    }

    public void setDivisions(String[] divisions) {
	map.setDivisions(divisions);
    }

    public void setToolAction(MapToolAction action) {
	toolAction = action;
	mouseAction = MapMouseAction.NOP;
    }

    public String[] getDivisions() {
	return map.getDivisions();
    }

    public int getQtdDivision() {
	return map.getQtdDivision();
    }

    public void uploadPerpesctiveImage(KpiMapaPerspectiva perspective, File imgFile) {
	if (imgFile != null) {
	    KpiMapaImageUpload imageUpload = new KpiMapaImageUpload(perspective, this);
	    imageUpload.upload(imgFile);
	} else {
	    perspective.setImage("");
	}
    }
    
    public void deletePerpesctiveImage(KpiMapaPerspectiva perspective) {
        ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); //NOI18N
        StringBuilder msg = new StringBuilder();
        
        msg.append(bundle.getString("KpiFormController_00008")); //NOI18N
        msg.append(bundle.getString("KpiFormController_00009")); //NOI18N
        
        if (ValidDelPerpesctiveImg( msg.toString() )){
            perspective.setMapImage(perspective.getImage());
            perspective.setImage("");
        }
    }

    public void updateRendererObject(IKpiMapaComponente element) throws MalformedURLException, IOException {

	if (element instanceof KpiMapaObjetivo) {
	    KpiMapaObjetivo objective = (KpiMapaObjetivo) element;

	    KpiMapaObjetivoRenderer mapObjective = renderObjectives.get(objective.getId());

	    mapObjective.setSize(objective.getWidth(), objective.getHeight());
	    mapObjective.setPreferredSize(new Dimension(objective.getWidth(), objective.getHeight()));
	    mapObjective.setBackColor(objective.getBackColor());
	    mapObjective.setFontColor(objective.getFontColor());
	    mapObjective.setOpaqueColor(objective.isOpaque());
	    mapObjective.setVisibleName(objective.isVisibleName());
	    mapObjective.setTitle(objective.getName());
	    mapObjective.setShape(objective.getShape());
	    mapObjective.setLocation(new Point(objective.getPosX(), objective.getPosY()));
	    mapObjective.repaint();

	} else if (element instanceof KpiMapaGrupo) {
	    KpiMapaGrupo group = (KpiMapaGrupo) element;

	    KpiMapaGrupoRenderer mapGroup = renderGroups.get(group.getId());
	    mapGroup.setBounds(group.getPosX(), group.getPosY(), group.getWidth(), group.getHeight());
	    mapGroup.setPreferredSize(new Dimension(group.getWidth(), group.getHeight()));
	    mapGroup.setBackColor(group.getBackColor());
	    mapGroup.setFontColor(group.getFontColor());
	    mapGroup.setOpaqueColor(group.isOpaque());
	    mapGroup.setTitle(group.getName());
	    mapGroup.repaint();

	} else if (element instanceof KpiMapaPerspectiva) {
	    KpiMapaPerspectiva perspective = (KpiMapaPerspectiva) element;

	    KpiMapaPerspectivaRenderer mapPerspective = renderPerspectives.get(perspective.getId());

	    mapPerspective.setSize(new Dimension(MAP_WIDTH, perspective.getHeight()));
	    mapPerspective.setPreferredSize(new Dimension(MAP_WIDTH, perspective.getHeight()));
	    mapPerspective.setFontColor(perspective.getFontColor());
	    mapPerspective.setBackground(perspective.getBackColor());
            mapPerspective.setImage(perspective.getImage(), "2");
	    mapPerspective.setTitle(perspective.getName());
	    mapPerspective.repaint();

	} else if (element instanceof KpiMapaLigacao) {
	    repaintGlass();
	}
    }

    public BIXMLVector getThemes() {
	return themes;
    }

    public KpiMapaEstrategico getMap() {
	return map;
    }
    
    public boolean ValidDelPerpesctiveImg( String cMsg ){
        Boolean retorno = true;
	final Object[] opcoes = {"Sim", "N�o"};
        
	final int escolha = JOptionPane.showOptionDialog(null,
		cMsg,
		java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiFormController_00010"),
		JOptionPane.YES_NO_OPTION,
		JOptionPane.QUESTION_MESSAGE,
		null,
		opcoes,
		opcoes[1]);

	if (escolha == JOptionPane.NO_OPTION || escolha == JOptionPane.CLOSED_OPTION) {
            retorno = false;
        }
        
        return retorno;
    }
}
