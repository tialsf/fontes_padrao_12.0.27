package kpi.swing.analisys.map;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.print.Printable;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import javax.swing.*;
import kpi.beans.JBILayeredPane;
import kpi.core.IteMnuPermission;
import kpi.core.IteMnuPermission.Permissao;
import kpi.core.IteMnuPermission.TipoPermissao;
import kpi.core.KpiMenuController;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiDefaultFrameFunctions;
import kpi.swing.KpiFileChooser;
import kpi.swing.analisys.map.KpiMapaFormController.MapToolAction;
import kpi.xml.BIXMLRecord;

/**
 *
 * @author gilmar.pereira
 */
public class KpiMapaFrame extends JInternalFrame implements KpiDefaultFrameFunctions, Printable, IKpiMapaMainForm {

    private String recordtype = "MAPAESTRATEGICO1";
    private boolean isMaximize = false;
    private int status;
    private KpiMapaMaximize maximizeForm;
    private KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private String id = new String();
    private BIXMLRecord record = null;
    private KpiMapaFormController mapFormController;
    private KpiMapaGlass glassPanel;
    private Hashtable temas = new Hashtable();
    private Calendar dataAcumDe;
    private Calendar dataAcumAte;

    public KpiMapaFrame(int operation, String idAux, String contextId) {
        initComponents();

        putClientProperty("MAXI", true); //NOI18N

        pnlNomeDiv.removeAll();
        pnlNomeDiv.add(fldNomeDivVert1);
        pnlNomeDiv.setVisible(true);
        fldNomeDivVert1.setVisible(true);
        fldNomeDivVert2.setVisible(false);
        fldNomeDivVert3.setVisible(false);

        glassPanel = new KpiMapaGlass();
        glassPanel.setLayout(null);
        glassPanel.setOpaque(false);
        mapLayeredPanel.add(glassPanel, javax.swing.JLayeredPane.MODAL_LAYER);

        mapFormController = new KpiMapaFormController(glassPanel, this);

        fldDataAlvo.setCalendar(new GregorianCalendar());

        //regra de seguran�a
        KpiMenuController mnuControl = KpiStaticReferences.getMnuController();
        IteMnuPermission perm = mnuControl.getRuleByName("MAPAESTRATEGICO");

        if (perm.getPermissionValue(TipoPermissao.MANUTENCAO) == Permissao.PERMITIDO) {
            btnEdit.setEnabled(true);
        } else {
            btnEdit.setEnabled(false);
        }

        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
        // Habilita os campos do formul�rio.
        tbMap.setVisible(true);
        fldNomeDivVert1.setEnabled(true);
        fldNomeDivVert2.setEnabled(true);
        fldNomeDivVert3.setEnabled(true);

        lblTemaEstrategico.setEnabled(false);
        cboTemaEstrategico.setEnabled(false);
    }

    @Override
    public void disableFields() {
        // Desabilita os campos do formul�rio.
        tbMap.setVisible(false);
        fldNomeDivVert1.setEnabled(false);
        fldNomeDivVert2.setEnabled(false);
        fldNomeDivVert3.setEnabled(false);

        lblTemaEstrategico.setEnabled(true);
        cboTemaEstrategico.setEnabled(true);
    }

    @Override
    public void refreshFields() {
    }

    @Override
    public BIXMLRecord getFieldsContents() {
        // Retorno
        String[] divisions = {fldNomeDivVert1.getText(), fldNomeDivVert2.getText(), fldNomeDivVert3.getText()};
        mapFormController.setDivisions(divisions);

        return mapFormController.mapToXml();
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    @Override
    public void toggleMaximize() {
        if (!isMaximize) {
            btnMaximize.setText(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00001"));
            btnMaximize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_minimizar.gif")));
            isMaximize = true;
            maximizeForm = new KpiMapaMaximize(kpi.core.KpiStaticReferences.getKpiMainPanel(), true, pnlMainPanel);
            maximizeForm.setMapaFrame(this);
            maximizeForm.setVisible(true);

            invalidate();
            revalidate();
            doLayout();
        } else {
            btnMaximize.setText(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00002"));
            btnMaximize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_maximizar.gif")));
            isMaximize = false;
            getContentPane().add(maximizeForm.pnlMapa, java.awt.BorderLayout.CENTER);
            maximizeForm.setVisible(false);
            maximizeForm.dispose();
            requestFocus();

            invalidate();
            revalidate();
            doLayout();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grpEditMapa = new javax.swing.ButtonGroup();
        pnlMainPanel = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        dataPanelCenter = new javax.swing.JPanel();
        lblDataAlvo = new javax.swing.JLabel();
        scrDataAlvo = new javax.swing.JScrollPane();
        fldDataAlvo = new pv.jfcx.JPVDatePlus();
        lblTemaEstrategico = new javax.swing.JLabel();
        cboTemaEstrategico = new javax.swing.JComboBox();
        dataPanelBottom = new javax.swing.JPanel();
        pnlLayoutDiv = new javax.swing.JPanel();
        pnlNomeDiv = new javax.swing.JPanel();
        fldNomeDivVert1 = new javax.swing.JTextField();
        fldNomeDivVert2 = new javax.swing.JTextField();
        fldNomeDivVert3 = new javax.swing.JTextField();
        dataPanelTop = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        tbMap = new javax.swing.JToolBar();
        btnSelecionar = new javax.swing.JToggleButton();
        btnDesenharGrupo = new javax.swing.JToggleButton();
        btnLigar = new javax.swing.JToggleButton();
        btnDelete = new javax.swing.JToggleButton();
        lblNumDivisoes = new javax.swing.JLabel();
        String[] strDivisoes = {"1","2","3"};
        SpinnerListModel spnModel = new SpinnerListModel(strDivisoes);
        spnDivideMapa = new javax.swing.JSpinner(spnModel);
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnPrint = new javax.swing.JButton();
        btnExportar = new javax.swing.JButton();
        btnMaximize = new javax.swing.JButton();
        mapScrollPanel = new javax.swing.JScrollPane();
        mapLayeredPanel = new kpi.beans.JBILayeredPane();
        mapPanel = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international"); // NOI18N
        setTitle(bundle.getString("KpiMapaFrame_00003")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_mapa.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlMainPanel.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 80));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 80));
        pnlTools.setLayout(new java.awt.BorderLayout());

        dataPanelCenter.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        dataPanelCenter.setPreferredSize(new java.awt.Dimension(10, 23));
        dataPanelCenter.setLayout(null);

        lblDataAlvo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataAlvo.setText(bundle.getString("KpiMapaFrame_00004")); // NOI18N
        lblDataAlvo.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDataAlvo.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDataAlvo.setPreferredSize(new java.awt.Dimension(100, 16));
        dataPanelCenter.add(lblDataAlvo);
        lblDataAlvo.setBounds(10, 5, 60, 20);

        scrDataAlvo.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrDataAlvo.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        fldDataAlvo.setAllowNull(false);
        fldDataAlvo.setBorderStyle(0);
        fldDataAlvo.setButtonBorder(0);
        fldDataAlvo.setDialog(true);
        fldDataAlvo.setFont(new java.awt.Font("Tahoma", 1, 11));
        fldDataAlvo.setUseLocale(true);
        fldDataAlvo.setWaitForCalendarDate(true);
        fldDataAlvo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fldDataAlvoActionPerformed(evt);
            }
        });
        scrDataAlvo.setViewportView(fldDataAlvo);

        dataPanelCenter.add(scrDataAlvo);
        scrDataAlvo.setBounds(75, 5, 100, 20);

        lblTemaEstrategico.setText(bundle.getString("KpiMapaFrame_00030")); // NOI18N
        dataPanelCenter.add(lblTemaEstrategico);
        lblTemaEstrategico.setBounds(210, 4, 100, 20);

        cboTemaEstrategico.setMaximumSize(new java.awt.Dimension(450, 20));
        cboTemaEstrategico.setMinimumSize(new java.awt.Dimension(450, 20));
        cboTemaEstrategico.setPreferredSize(new java.awt.Dimension(450, 20));
        cboTemaEstrategico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTemaEstrategicoActionPerformed(evt);
            }
        });
        dataPanelCenter.add(cboTemaEstrategico);
        cboTemaEstrategico.setBounds(310, 5, 450, 20);

        pnlTools.add(dataPanelCenter, java.awt.BorderLayout.CENTER);

        dataPanelBottom.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        dataPanelBottom.setPreferredSize(new java.awt.Dimension(10, 23));
        dataPanelBottom.setLayout(new java.awt.BorderLayout());

        pnlLayoutDiv.setMaximumSize(new java.awt.Dimension(952, 2147483647));
        pnlLayoutDiv.setOpaque(false);
        pnlLayoutDiv.setPreferredSize(new java.awt.Dimension(950, 30));
        pnlLayoutDiv.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 0, 0));

        pnlNomeDiv.setBackground(new java.awt.Color(189, 186, 189));
        pnlNomeDiv.setToolTipText("");
        pnlNomeDiv.setMaximumSize(new java.awt.Dimension(952, 500));
        pnlNomeDiv.setPreferredSize(new java.awt.Dimension(950, 20));
        pnlNomeDiv.setRequestFocusEnabled(false);
        pnlNomeDiv.setLayout(new java.awt.GridLayout(1, 3, 0, -10));

        fldNomeDivVert1.setBackground(new java.awt.Color(223, 229, 243));
        fldNomeDivVert1.setFont(new java.awt.Font("MS Sans Serif", 1, 11)); // NOI18N
        fldNomeDivVert1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        fldNomeDivVert1.setToolTipText("");
        fldNomeDivVert1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(221, 221, 221)));
        fldNomeDivVert1.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        fldNomeDivVert1.setEnabled(false);
        fldNomeDivVert1.setMaximumSize(new java.awt.Dimension(0, 0));
        fldNomeDivVert1.setPreferredSize(new java.awt.Dimension(160, 20));
        pnlNomeDiv.add(fldNomeDivVert1);

        fldNomeDivVert2.setBackground(new java.awt.Color(223, 229, 243));
        fldNomeDivVert2.setFont(new java.awt.Font("MS Sans Serif", 1, 11));
        fldNomeDivVert2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        fldNomeDivVert2.setToolTipText("");
        fldNomeDivVert2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(221, 221, 221)));
        fldNomeDivVert2.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        fldNomeDivVert2.setEnabled(false);
        fldNomeDivVert2.setMaximumSize(new java.awt.Dimension(0, 0));
        fldNomeDivVert2.setPreferredSize(new java.awt.Dimension(160, 20));
        pnlNomeDiv.add(fldNomeDivVert2);

        fldNomeDivVert3.setBackground(new java.awt.Color(223, 229, 243));
        fldNomeDivVert3.setFont(new java.awt.Font("MS Sans Serif", 1, 11));
        fldNomeDivVert3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        fldNomeDivVert3.setToolTipText("");
        fldNomeDivVert3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(221, 221, 221)));
        fldNomeDivVert3.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        fldNomeDivVert3.setEnabled(false);
        fldNomeDivVert3.setMaximumSize(new java.awt.Dimension(0, 0));
        fldNomeDivVert3.setPreferredSize(new java.awt.Dimension(160, 20));
        pnlNomeDiv.add(fldNomeDivVert3);

        pnlLayoutDiv.add(pnlNomeDiv);

        dataPanelBottom.add(pnlLayoutDiv, java.awt.BorderLayout.CENTER);

        pnlTools.add(dataPanelBottom, java.awt.BorderLayout.SOUTH);

        dataPanelTop.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        dataPanelTop.setPreferredSize(new java.awt.Dimension(10, 23));
        dataPanelTop.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 30));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(130, 32));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("KpiMapaFrame_00005")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("KpiMapaFrame_00006")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        dataPanelTop.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        tbMap.setFloatable(false);
        tbMap.setMaximumSize(new java.awt.Dimension(300, 200));
        tbMap.setPreferredSize(new java.awt.Dimension(200, 70));

        grpEditMapa.add(btnSelecionar);
        btnSelecionar.setFont(new java.awt.Font("Dialog", 0, 12));
        btnSelecionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_setaselecionar.gif"))); // NOI18N
        btnSelecionar.setMnemonic('s');
        btnSelecionar.setToolTipText(bundle.getString("KpiMapaFrame_00007")); // NOI18N
        btnSelecionar.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSelecionar.setMaximumSize(new java.awt.Dimension(40, 20));
        btnSelecionar.setMinimumSize(new java.awt.Dimension(40, 20));
        btnSelecionar.setPreferredSize(new java.awt.Dimension(40, 20));
        btnSelecionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelecionarActionPerformed(evt);
            }
        });
        tbMap.add(btnSelecionar);

        grpEditMapa.add(btnDesenharGrupo);
        btnDesenharGrupo.setFont(new java.awt.Font("Dialog", 0, 12));
        btnDesenharGrupo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_desenhargrupo.gif"))); // NOI18N
        btnDesenharGrupo.setMnemonic('d');
        btnDesenharGrupo.setToolTipText(bundle.getString("KpiMapaFrame_00008")); // NOI18N
        btnDesenharGrupo.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDesenharGrupo.setMaximumSize(new java.awt.Dimension(40, 20));
        btnDesenharGrupo.setMinimumSize(new java.awt.Dimension(40, 20));
        btnDesenharGrupo.setPreferredSize(new java.awt.Dimension(40, 20));
        btnDesenharGrupo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesenharGrupoActionPerformed(evt);
            }
        });
        tbMap.add(btnDesenharGrupo);

        grpEditMapa.add(btnLigar);
        btnLigar.setFont(new java.awt.Font("Dialog", 0, 12));
        btnLigar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_ligacoes.gif"))); // NOI18N
        btnLigar.setMnemonic('l');
        btnLigar.setToolTipText(bundle.getString("KpiMapaFrame_00009")); // NOI18N
        btnLigar.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnLigar.setMaximumSize(new java.awt.Dimension(40, 20));
        btnLigar.setMinimumSize(new java.awt.Dimension(40, 20));
        btnLigar.setPreferredSize(new java.awt.Dimension(40, 20));
        btnLigar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLigarActionPerformed(evt);
            }
        });
        tbMap.add(btnLigar);

        grpEditMapa.add(btnDelete);
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_x_cancelar.gif"))); // NOI18N
        btnDelete.setMnemonic('e');
        btnDelete.setToolTipText(bundle.getString("KpiMapaFrame_00010")); // NOI18N
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setFocusable(false);
        btnDelete.setMaximumSize(new java.awt.Dimension(40, 20));
        btnDelete.setMinimumSize(new java.awt.Dimension(40, 20));
        btnDelete.setPreferredSize(new java.awt.Dimension(40, 20));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbMap.add(btnDelete);

        lblNumDivisoes.setText(bundle.getString("KpiMapaFrame_00011")); // NOI18N
        tbMap.add(lblNumDivisoes);

        spnDivideMapa.setToolTipText("");
        spnDivideMapa.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        spnDivideMapa.setMaximumSize(new java.awt.Dimension(41, 26));
        spnDivideMapa.setMinimumSize(new java.awt.Dimension(21, 0));
        spnDivideMapa.setPreferredSize(new java.awt.Dimension(51, 15));
        spnDivideMapa.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spnDivideMapaStateChanged(evt);
            }
        });
        tbMap.add(spnDivideMapa);

        dataPanelTop.add(tbMap, java.awt.BorderLayout.CENTER);

        pnlOperation.setMaximumSize(new java.awt.Dimension(500, 30));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(500, 30));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("KpiMapaFrame_00012")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("KpiMapaFrame_00013")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_print.gif"))); // NOI18N
        btnPrint.setText(bundle.getString("KpiMapaFrame_00014")); // NOI18N
        btnPrint.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnPrint.setMaximumSize(new java.awt.Dimension(76, 20));
        btnPrint.setMinimumSize(new java.awt.Dimension(76, 20));
        btnPrint.setPreferredSize(new java.awt.Dimension(76, 20));
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });
        tbInsertion.add(btnPrint);

        btnExportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_exportacao.gif"))); // NOI18N
        btnExportar.setText(bundle.getString("KpiMapaFrame_00015")); // NOI18N
        btnExportar.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnExportar.setMaximumSize(new java.awt.Dimension(76, 20));
        btnExportar.setMinimumSize(new java.awt.Dimension(76, 20));
        btnExportar.setPreferredSize(new java.awt.Dimension(76, 20));
        btnExportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarActionPerformed(evt);
            }
        });
        tbInsertion.add(btnExportar);

        btnMaximize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_maximizar.gif"))); // NOI18N
        btnMaximize.setText(bundle.getString("KpiMapaFrame_00002")); // NOI18N
        btnMaximize.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnMaximize.setMaximumSize(new java.awt.Dimension(76, 20));
        btnMaximize.setMinimumSize(new java.awt.Dimension(76, 20));
        btnMaximize.setPreferredSize(new java.awt.Dimension(76, 20));
        btnMaximize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMaximizeActionPerformed(evt);
            }
        });
        tbInsertion.add(btnMaximize);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        dataPanelTop.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlTools.add(dataPanelTop, java.awt.BorderLayout.NORTH);

        pnlMainPanel.add(pnlTools, java.awt.BorderLayout.NORTH);

        mapScrollPanel.setFocusCycleRoot(true);

        mapLayeredPanel.setPreferredSize(new java.awt.Dimension(1000, 1000));

        mapPanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        mapPanel.setOpaque(false);
        mapPanel.setPreferredSize(new java.awt.Dimension(948, 1000));
        mapPanel.setRequestFocusEnabled(false);
        mapPanel.setVerifyInputWhenFocusTarget(false);
        mapPanel.setLayout(new javax.swing.BoxLayout(mapPanel, javax.swing.BoxLayout.Y_AXIS));
        mapPanel.setBounds(0, 0, 948, 0);
        mapLayeredPanel.add(mapPanel, javax.swing.JLayeredPane.DEFAULT_LAYER);

        mapScrollPanel.setViewportView(mapLayeredPanel);

        pnlMainPanel.add(mapScrollPanel, java.awt.BorderLayout.CENTER);

        getContentPane().add(pnlMainPanel, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnMaximizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMaximizeActionPerformed
        toggleMaximize();
    }//GEN-LAST:event_btnMaximizeActionPerformed

    private void btnExportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportarActionPerformed
        kpi.swing.KpiDefaultDialogSystem dialogSystem;

        boolean maxState = isMaximum();
        try {
            setMaximum(true);
        } catch (Exception e) {
        }

        invalidate();
        revalidate();
        doLayout();

        if (isMaximize && maximizeForm != null) {
            dialogSystem = new kpi.swing.KpiDefaultDialogSystem(maximizeForm);
        } else {
            dialogSystem = new kpi.swing.KpiDefaultDialogSystem(this);
        }

        //Mensagem necess�ria para fazer o "split" do painel.
        if (dialogSystem.confirmMessage(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00016"))
                == javax.swing.JOptionPane.YES_OPTION) {
            kpi.swing.KpiFileChooser frmChooser = new kpi.swing.KpiFileChooser(kpi.core.KpiStaticReferences.getKpiMainPanel(), true);
            frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_SAVE);
            frmChooser.setFileType("*.jpg");
            frmChooser.loadServerFiles("mapest\\*.jpg");
            frmChooser.setFileName("mapa" + mapFormController.getMap().getId() + ".jpg");
            frmChooser.setVisible(true);
            if (frmChooser.isValidFile()) {
                saveComponentAsJPEG("mapest\\" + frmChooser.getFileName());
            }
        }

        try {
            setMaximum(maxState);
        } catch (Exception e) {
        }

    }//GEN-LAST:event_btnExportarActionPerformed

	private void spnDivideMapaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spnDivideMapaStateChanged
            int iDivisao = Integer.parseInt((String) spnDivideMapa.getValue());

            pnlNomeDiv.removeAll();

            switch (iDivisao) {
                case 1:
                    pnlNomeDiv.add(fldNomeDivVert1);
                    fldNomeDivVert1.setVisible(true);
                    fldNomeDivVert2.setVisible(false);
                    fldNomeDivVert3.setVisible(false);
                    fldNomeDivVert1.requestFocus();
                    break;
                case 2:
                    pnlNomeDiv.add(fldNomeDivVert1);
                    pnlNomeDiv.add(fldNomeDivVert2);
                    pnlNomeDiv.setVisible(true);
                    fldNomeDivVert1.setVisible(true);
                    fldNomeDivVert2.setVisible(true);
                    fldNomeDivVert3.setVisible(false);
                    fldNomeDivVert2.requestFocus();
                    break;
                case 3:
                    pnlNomeDiv.add(fldNomeDivVert1);
                    pnlNomeDiv.add(fldNomeDivVert2);
                    pnlNomeDiv.add(fldNomeDivVert3);
                    pnlNomeDiv.setVisible(true);
                    fldNomeDivVert1.setVisible(true);
                    fldNomeDivVert2.setVisible(true);
                    fldNomeDivVert3.setVisible(true);
                    fldNomeDivVert3.requestFocus();
                    break;
                default:
                    pnlNomeDiv.setVisible(false);
                    fldNomeDivVert1.setVisible(false);
                    fldNomeDivVert2.setVisible(false);
                    fldNomeDivVert3.setVisible(false);
                    break;

            }

            pnlNomeDiv.validate();

            if (status == KpiDefaultFrameBehavior.UPDATING) {
                mapFormController.setQtdDivision(iDivisao);
            }
	}//GEN-LAST:event_spnDivideMapaStateChanged

	private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
            kpi.swing.KpiDefaultDialogSystem dialogSystem;
            java.awt.print.PrinterJob printJob = java.awt.print.PrinterJob.getPrinterJob();

            boolean maxState = isMaximum();
            try {
                setMaximum(true);
            } catch (Exception e) {
            }

            if (isMaximize && maximizeForm != null) {
                dialogSystem = new kpi.swing.KpiDefaultDialogSystem(maximizeForm);
            } else {
                dialogSystem = new kpi.swing.KpiDefaultDialogSystem(this);
            }

            //Mensagem necess�ria para fazer o "split" do painel.
            if (dialogSystem.confirmMessage(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00017")) == javax.swing.JOptionPane.YES_OPTION) {

                printJob.setPrintable(this);
                if (printJob.printDialog()) {
                    try {
                        printJob.print();
                    } catch (java.awt.print.PrinterException pe) {
                        System.out.println("Error printing: " + pe);
                    }
                }
            }

            try {
                setMaximum(maxState);
            } catch (Exception e) {
            }

	}//GEN-LAST:event_btnPrintActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            loadRecord();
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            cboTemaEstrategico.setSelectedIndex(0);
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

    private void btnSelecionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelecionarActionPerformed
        mapFormController.setToolAction(MapToolAction.MOVE);
    }//GEN-LAST:event_btnSelecionarActionPerformed

    private void btnDesenharGrupoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesenharGrupoActionPerformed
        mapFormController.setToolAction(MapToolAction.DRAWGROUP);
    }//GEN-LAST:event_btnDesenharGrupoActionPerformed

    private void btnLigarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLigarActionPerformed
        mapFormController.setToolAction(MapToolAction.DRAWLINK);
    }//GEN-LAST:event_btnLigarActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        mapFormController.setToolAction(MapToolAction.DELETE);
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void fldDataAlvoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fldDataAlvoActionPerformed
        mapFormController.refreshElements(fldDataAlvo.getCalendar(), event.getComboValue(temas, cboTemaEstrategico));
    }//GEN-LAST:event_fldDataAlvoActionPerformed

    private void cboTemaEstrategicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTemaEstrategicoActionPerformed
        mapFormController.refreshElements(fldDataAlvo.getCalendar(), event.getComboValue(temas, cboTemaEstrategico));
    }//GEN-LAST:event_cboTemaEstrategicoActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JToggleButton btnDelete;
    private javax.swing.JToggleButton btnDesenharGrupo;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnExportar;
    private javax.swing.JToggleButton btnLigar;
    private javax.swing.JButton btnMaximize;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private javax.swing.JToggleButton btnSelecionar;
    private javax.swing.JComboBox cboTemaEstrategico;
    private javax.swing.JPanel dataPanelBottom;
    private javax.swing.JPanel dataPanelCenter;
    private javax.swing.JPanel dataPanelTop;
    pv.jfcx.JPVDatePlus fldDataAlvo;
    private javax.swing.JTextField fldNomeDivVert1;
    private javax.swing.JTextField fldNomeDivVert2;
    private javax.swing.JTextField fldNomeDivVert3;
    private javax.swing.ButtonGroup grpEditMapa;
    private javax.swing.JLabel lblDataAlvo;
    private javax.swing.JLabel lblNumDivisoes;
    private javax.swing.JLabel lblTemaEstrategico;
    private kpi.beans.JBILayeredPane mapLayeredPanel;
    private javax.swing.JPanel mapPanel;
    private javax.swing.JScrollPane mapScrollPanel;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlLayoutDiv;
    private javax.swing.JPanel pnlMainPanel;
    private javax.swing.JPanel pnlNomeDiv;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JScrollPane scrDataAlvo;
    private javax.swing.JSpinner spnDivideMapa;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private javax.swing.JToolBar tbMap;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setStatus(int value) {
        status = value;
        mapFormController.setStatus(value);
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordtype = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordtype);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(BIXMLRecord recordAux) {
        record = recordAux;

        if (record != null) {
            btnSelecionar.setSelected(true);

            mapFormController.setToolAction(KpiMapaFormController.MapToolAction.MOVE);

            mapFormController.xmlToMap(record);

            //Mapa
            fldNomeDivVert1.setText(mapFormController.getDivisions()[0]);
            fldNomeDivVert2.setText(mapFormController.getDivisions()[1]);
            fldNomeDivVert3.setText(mapFormController.getDivisions()[2]);
            spnDivideMapa.setValue(Integer.toString(mapFormController.getQtdDivision()));

            try {
                mapFormController.renderMap();
            } catch (Exception ex) {
            }

            //Combo de Filtro de Temas
            event.populateCombo(mapFormController.getThemes(), "", temas, cboTemaEstrategico);

            //Atualiza status dos objetivos
            mapFormController.refreshElements(fldDataAlvo.getCalendar(), event.getComboValue(temas, cboTemaEstrategico));
        }
    }

    @Override
    public BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        return "";
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return null;
    }

    @Override
    public int print(java.awt.Graphics g, java.awt.print.PageFormat pageFormat, int pageIndex) throws java.awt.print.PrinterException {
        if (pageIndex > 0) {
            return (NO_SUCH_PAGE);
        } else {
            java.awt.Graphics2D g2d = (java.awt.Graphics2D) g;
            g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY() + 50);

            Font f = new Font(Font.SANS_SERIF, Font.PLAIN, 10);
            g2d.setFont(f);
            g2d.setColor(Color.BLACK);

            g2d.drawString(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00018") + mapFormController.getMap().getName(), 0, 15);
            g2d.scale(0.55, 0.55);
            disableDoubleBuffering(this);
            g2d.translate(0, 40);
            pnlNomeDiv.paint(g2d);
            g2d.translate(0, pnlNomeDiv.getHeight());
            mapLayeredPanel.paint(g2d);
            enableDoubleBuffering(this);
            return (PAGE_EXISTS);
        }
    }

    /**
     * Grava o objeto passado com uma figura do tipo JPEG
     */
    public static void disableDoubleBuffering(Component c) {
        RepaintManager currentManager = RepaintManager.currentManager(c);
        currentManager.setDoubleBufferingEnabled(false);
    }

    public static void enableDoubleBuffering(Component c) {
        RepaintManager currentManager = RepaintManager.currentManager(c);
        currentManager.setDoubleBufferingEnabled(true);
    }

    private void saveComponentAsJPEG(String fileName) {
        kpi.util.KpiToolKit tool = new kpi.util.KpiToolKit();
        Dimension size = glassPanel.getSize();

        java.awt.image.BufferedImage myImage = new java.awt.image.BufferedImage(size.width, size.height + pnlNomeDiv.getHeight() + 20,
                java.awt.image.BufferedImage.TYPE_INT_RGB);

        Graphics2D g2 = myImage.createGraphics();

        java.awt.geom.RoundRectangle2D rect = new java.awt.geom.RoundRectangle2D.Double();

        rect.setFrame(0, 0, myImage.getWidth(), myImage.getHeight());

        g2.setPaint(Color.WHITE);
        g2.fill(rect);

        Font f = new Font(Font.SANS_SERIF, Font.PLAIN, 10);
        g2.setFont(f);
        g2.setColor(Color.BLACK);
        g2.drawString(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00018") + mapFormController.getMap().getName(), 0, 15);

        JBILayeredPane.disableDoubleBuffering(mapLayeredPanel);

        g2.translate(0, 20);

        pnlNomeDiv.paint(g2);

        g2.translate(0, pnlNomeDiv.getHeight());

        mapLayeredPanel.paint(g2);

        JBILayeredPane.enableDoubleBuffering(mapLayeredPanel);

        try {
            tool.writeJPEGFile(myImage, fileName);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Override
    public String getFormType() {
        return recordtype;
    }

    @Override
    public String getParentType() {
        return null;
    }

    @Override
    public void clearMap() {
        mapPanel.removeAll();
    }

    @Override
    public void addMapComponent(KpiMapaPerspectivaRenderer element) {
        mapPanel.add(element);
    }

    @Override
    public void removeMapComponent(Component element) {
        mapPanel.remove(element);
    }

    @Override
    public void revalidateMap() {
        int totalHeight = 0;

        for (int i = 0; i < mapPanel.getComponentCount(); i++) {
            JPanel element = (JPanel) mapPanel.getComponent(i);
            totalHeight += element.getPreferredSize().height;
        }

        mapLayeredPanel.setPreferredSize(new Dimension(KpiMapaFormController.MAP_WIDTH, totalHeight));

        mapPanel.setBounds(0, 0, KpiMapaFormController.MAP_WIDTH, totalHeight);
        mapPanel.setPreferredSize(new Dimension(KpiMapaFormController.MAP_WIDTH, totalHeight));
        mapPanel.invalidate();
        mapPanel.revalidate();
        mapPanel.doLayout();

        glassPanel.setBounds(0, 0, KpiMapaFormController.MAP_WIDTH, totalHeight);
        glassPanel.setPreferredSize(new Dimension(KpiMapaFormController.MAP_WIDTH, totalHeight));
    }

    @Override
    public void drillObjective(String sourceId) {
        KpiMapaDrillObjetivo frmDestino = (KpiMapaDrillObjetivo) kpi.core.KpiStaticReferences.getKpiFormController().getForm("MAPADRILLOBJETIVO", sourceId, "");
        frmDestino.doDrill(fldDataAlvo.getCalendar(), dataAcumDe, dataAcumAte);
    }

    @Override
    public JPopupMenu getPopupMenu(KpiMapaObjetivo objective) {
        JPopupMenu popup = new JPopupMenu();
        JMenuItem item;
        KpiMapaMenuAction a;

        //Cor do texto
        item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00019"));
        a = new KpiMapaMenuAction(objective, mapFormController) {

            @Override
            public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                //Cor do texto
                KpiMapaObjetivo objective = (KpiMapaObjetivo) o;
                Color color = chooseColor(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00021"), objective.getFontColor());

                if (color != null && color != objective.getFontColor()) {
                    objective.setFontColor(color);
                }
            }
        };
        item.addActionListener(a);
        popup.add(item);

        //Cor do fundo
        item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00020"));
        a = new KpiMapaMenuAction(objective, mapFormController) {

            @Override
            public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                //Cor do fundo
                KpiMapaObjetivo objective = (KpiMapaObjetivo) o;
                Color color = chooseColor(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00022"), objective.getBackColor());

                if (color != null && color != objective.getBackColor()) {
                    objective.setBackColor(color);
                }
            }
        };
        item.addActionListener(a);
        popup.add(item);

        //Shape
        if (objective.getShape().equals(KpiMapaObjetivo.SHAPE_RECT)) {
            item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00023"));
            a = new KpiMapaMenuAction(objective, mapFormController) {

                @Override
                public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                    //shape
                    KpiMapaObjetivo objective = (KpiMapaObjetivo) o;
                    objective.setShape(KpiMapaObjetivo.SHAPE_CIRCLE);
                }
            };

        } else {
            item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00024"));
            a = new KpiMapaMenuAction(objective, mapFormController) {

                @Override
                public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                    //shape
                    KpiMapaObjetivo objective = (KpiMapaObjetivo) o;
                    objective.setShape(KpiMapaObjetivo.SHAPE_RECT);
                }
            };
        }
        item.addActionListener(a);
        popup.add(item);

        return popup;
    }

    @Override
    public JPopupMenu getPopupMenu(KpiMapaGrupo group) {
        JPopupMenu popup = new JPopupMenu();
        JMenuItem item;
        KpiMapaMenuAction a;

        //T�tulo
        item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00025"));
        a = new KpiMapaMenuAction(group, mapFormController) {

            @Override
            public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                //T�tulo
                KpiMapaGrupo group = (KpiMapaGrupo) o;
                String name = chooseText(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00026"), group.getName());

                if (name != null && !name.equals(group.getName())) {
                    group.setName(name);
                }
            }
        };
        item.addActionListener(a);
        popup.add(item);

        //Cor do texto
        item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00019"));
        a = new KpiMapaMenuAction(group, mapFormController) {

            @Override
            public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                //Cor do texto
                KpiMapaGrupo group = (KpiMapaGrupo) o;
                Color color = chooseColor(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00021"), group.getFontColor());

                if (color != null && color != group.getFontColor()) {
                    group.setFontColor(color);
                }
            }
        };
        item.addActionListener(a);
        popup.add(item);

        //Cor do fundo
        item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00020"));
        a = new KpiMapaMenuAction(group, mapFormController) {

            @Override
            public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                //Cor do fundo
                KpiMapaGrupo group = (KpiMapaGrupo) o;
                Color color = chooseColor(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00022"), group.getBackColor());

                if (color != null && color != group.getBackColor()) {
                    group.setBackColor(color);
                }
            }
        };
        item.addActionListener(a);
        popup.add(item);

        return popup;
    }

    @Override
    public JPopupMenu getPopupMenu(KpiMapaPerspectiva perspective) {
        JPopupMenu popup = new JPopupMenu();
        JMenuItem item;
        KpiMapaMenuAction a;

        //Cor do texto
        item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00019"));
        a = new KpiMapaMenuAction(perspective, mapFormController) {

            @Override
            public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                //Cor do texto
                KpiMapaPerspectiva perspective = (KpiMapaPerspectiva) o;
                Color color = chooseColor(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00021"), perspective.getFontColor());

                if (color != null && color != perspective.getFontColor()) {
                    perspective.setFontColor(color);
                }
            }
        };
        item.addActionListener(a);
        popup.add(item);

        //Cor do fundo
        item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00020"));
        a = new KpiMapaMenuAction(perspective, mapFormController) {

            @Override
            public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                //Cor do fundo
                KpiMapaPerspectiva perspective = (KpiMapaPerspectiva) o;
                Color color = chooseColor(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00022"), perspective.getBackColor());

                if (color != null && color != perspective.getBackColor()) {
                    perspective.setBackColor(color);
                }
            }
        };
        item.addActionListener(a);
        popup.add(item);

        return popup;
    }

    @Override
    public JPopupMenu getPopupMenu(KpiMapaLigacao link) {
        JPopupMenu popup = new JPopupMenu();
        JMenuItem item;
        KpiMapaMenuAction a;

        //Formato da linha
        item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00027"));
        a = new KpiMapaMenuAction(link, mapFormController) {

            @Override
            public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                KpiMapaLigacao link = (KpiMapaLigacao) o;
                link.setType(KpiMapaLigacao.LINE);
            }
        };
        item.addActionListener(a);
        popup.add(item);

        //Cor do fundo
        item = new JMenuItem(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00028"));
        a = new KpiMapaMenuAction(link, mapFormController) {

            @Override
            public void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c) {
                //Cor do fundo
                KpiMapaLigacao link = (KpiMapaLigacao) o;
                Color color = chooseColor(java.util.ResourceBundle.getBundle("international").getString("KpiMapaFrame_00029"), link.getLineColor());

                if (color != null && color != link.getLineColor()) {
                    link.setLineColor(color);
                }
            }
        };
        item.addActionListener(a);
        popup.add(item);

        return popup;
    }

    private Color chooseColor(String title, Color currentColor) {
        Component c;

        if (isMaximize && maximizeForm != null) {
            c = maximizeForm;
        } else {
            c = this;
        }

        return JColorChooser.showDialog(c, title, currentColor);
    }

    private String chooseText(String question, String currentValue) {
        Component c;

        if (isMaximize && maximizeForm != null) {
            c = maximizeForm;
        } else {
            c = this;
        }

        return JOptionPane.showInputDialog(c, question, currentValue);
    }

    @Override
    public void setDataAlvo(Calendar data) {
        fldDataAlvo.setCalendar(data);
    }

    @Override
    public void setDataAcumDe(Calendar data) {
        dataAcumDe = data;
    }

    @Override
    public void setDataAcumAte(Calendar data) {
        dataAcumAte = data;
    }
}