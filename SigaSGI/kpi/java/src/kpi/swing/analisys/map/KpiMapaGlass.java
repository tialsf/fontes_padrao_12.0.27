package kpi.swing.analisys.map;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JPanel;
import kpi.swing.KpiDefaultFrameBehavior;

/**
 *
 * @author gilmar.pereira
 */
class KpiMapaGlass extends JPanel {

    private HashMap<String, Rectangle> shadowMap = new HashMap();
    private HashMap<String, Line2D> shadowLineMap = new HashMap();
    private HashMap<String, KpiMapaLigacaoRenderer> shadowLineArrowMap = new HashMap();
    private HashMap<String, KpiMapaLigacaoRenderer> arrowsMap = new HashMap();
    private Color shadowColor = Color.BLACK;
    private Color shadowLineColor = Color.BLACK;
    private int divisions = 1;
    private int status;

    public void setStatus(int status) {
	this.status = status;
    }

    public void setDivisions(int divisions) {
	this.divisions = divisions;
    }

    public void setArrows(HashMap<String, KpiMapaLigacaoRenderer> arrows) {
	arrowsMap.clear();
	arrowsMap.putAll(arrows);
    }

    public void clearShadowBox() {
	shadowMap.clear();
    }

    public void clearShadowLine() {
	shadowLineMap.clear();
	shadowLineArrowMap.clear();
    }

    public void setShadowBoxColor(Color color) {
	shadowColor = color;
    }

    public void setShadowLineColor(Color color) {
	shadowLineColor = color;
    }

    public void setShadowLine(Line2D line) {
	shadowLineMap.clear();
	addShadowLine(line);
    }

    public void setShadowLine(Point2D p1, Point2D p2) {
	shadowLineMap.clear();
	addShadowLine(p1, p2);
    }

    public void addShadowLine(Line2D line) {
	addShadowLine(line.getP1(), line.getP2());
    }

    public void addShadowLine(Point2D p1, Point2D p2) {
	shadowLineMap.put(String.valueOf(shadowLineMap.size() + 1), new Line2D.Double(p1, p2));
    }

    public void addShadowLine(Point2D p1, Point2D p2, Point2D ctrl) {
	KpiMapaLigacaoRenderer arrow = new KpiMapaLigacaoRenderer(-1, (Point) p1, (Point) p2, (Point) ctrl);
	shadowLineArrowMap.put(String.valueOf(shadowLineArrowMap.size() + 1), arrow);
    }

    public void addShadowBox(Rectangle rect) {
	shadowMap.put(String.valueOf(shadowMap.size() + 1), new Rectangle(rect));
    }

    public void addShadowBox(int x, int y, int width, int height) {
	addShadowBox(new Rectangle(x, y, width, height));
    }

    public void setShadowBox(int x, int y, int width, int height) {
	shadowMap.clear();
	addShadowBox(x, y, width, height);
    }

    public void setShadowBox(KpiMapaObjetivo shadowElement) {
	shadowMap.clear();
	addShadowBox(shadowElement.getBounds());
    }

    public void setShadowBox(KpiMapaGrupo shadowElement, KpiMapaGrupoRenderer mapGroup) {
	shadowMap.clear();

	//adiciona elementos internos do grupo
	for (KpiMapaObjetivo objective : shadowElement.getObjectives().values()) {
	    Point pObj = new Point(objective.getPosX(), objective.getPosY());
	    pObj.translate(shadowElement.getPosX(), shadowElement.getPosY());

	    pObj = mapGroup.relativeToRealCoords(pObj);

	    addShadowBox(new Rectangle(pObj.x, pObj.y, objective.getWidth(), objective.getHeight()));
	}

	//adiciona grupo
	addShadowBox(shadowElement.getBounds());

	//adiciona elemento interno
	addShadowBox(mapGroup.getInnerBounds());
    }

    public void translateShadowBox(int dx, int dy) {
	for (Rectangle shadowBox : shadowMap.values()) {
	    shadowBox.translate(dx, dy);
	}
    }

    @Override
    public void paintComponent(Graphics g) {

	Graphics2D g2 = (Graphics2D) g;

	//Back Buffer
	BufferedImage backBuffer = g2.getDeviceConfiguration().createCompatibleImage(getWidth(), getHeight(), Transparency.TRANSLUCENT);

	//shadow box
	renderShadowBox(backBuffer);

	//shadow line
	renderShadowLine(backBuffer);

	//links
	renderArrows(backBuffer);

	//divis�es do mapa
	renderDivisions(backBuffer);

	//desenha Buffer
	g.drawImage(backBuffer, 0, 0, null);
    }

    private void renderShadowBox(Image backBuffer) {
	if (!shadowMap.isEmpty()) {
	    Graphics2D g2 = (Graphics2D) backBuffer.getGraphics();

	    float dash[] = {2.0f};//Tipo do pontilhado.

	    g2.setStroke(new java.awt.BasicStroke(1.0f, 0, 0, 10.0f, dash, 0.0f));
	    g2.setColor(shadowColor);

	    for (Rectangle shadowBox : shadowMap.values()) {
		g2.drawRect(shadowBox.x, shadowBox.y, shadowBox.width, shadowBox.height);
	    }
	}
    }

    private void renderShadowLine(Image backBuffer) {
	if (!shadowLineMap.isEmpty()) {
	    Graphics2D g2 = (Graphics2D) backBuffer.getGraphics();

	    float dash[] = {2.0f};//Tipo do pontilhado.

	    g2.setStroke(new java.awt.BasicStroke(1.0f, 0, 0, 10.0f, dash, 0.0f));
	    g2.setColor(shadowLineColor);

	    for (Line2D shadowLine : shadowLineMap.values()) {
		g2.draw(shadowLine);
	    }
	}

	if (!shadowLineArrowMap.isEmpty()) {
	    Graphics2D g2 = (Graphics2D) backBuffer.getGraphics();

	    float dash[] = {2.0f};//Tipo do pontilhado.

	    g2.setStroke(new java.awt.BasicStroke(1.0f, 0, 0, 10.0f, dash, 0.0f));
	    g2.setColor(shadowLineColor);

	    for (KpiMapaLigacaoRenderer shadowArrow : shadowLineArrowMap.values()) {
		shadowArrow.render(g2);
	    }
	}
    }

    private void renderArrows(BufferedImage backBuffer) {
	if (!arrowsMap.isEmpty()) {

	    ArrayList<KpiMapaLigacaoRenderer> selectedLines = new ArrayList();

	    Graphics2D g2 = (Graphics2D) backBuffer.getGraphics();

	    g2.setStroke(new BasicStroke(3.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));

	    //linhas normais
	    for (KpiMapaLigacaoRenderer arrow : arrowsMap.values()) {

		g2.setColor(arrow.getLineColor());
		g2.setPaint(arrow.getLineColor());

		if (arrow.isSelected()) {
		    selectedLines.add(arrow);
		} else {
		    arrow.render(g2);

		    if (status == KpiDefaultFrameBehavior.UPDATING) {
			arrow.renderControlPoint(g2);
		    }
		}
	    }

	    g2.setColor(Color.ORANGE);
	    g2.setPaint(Color.ORANGE);
	    //linhas selecionadas
	    for (KpiMapaLigacaoRenderer arrow : selectedLines) {
		arrow.render(g2);

		if (status == KpiDefaultFrameBehavior.UPDATING) {
		    arrow.renderControlPoint(g2);
		}
	    }
	}
    }

    private void renderDivisions(BufferedImage backBuffer) {
	if (divisions > 1) {
	    int width = getWidth() / divisions;
	    int height = getHeight();

	    Graphics2D g2 = (Graphics2D) backBuffer.getGraphics();

	    float dash[] = {2.0f};//Tipo do pontilhado.

	    g2.setStroke(new java.awt.BasicStroke(1.0f, 0, 0, 10.0f, dash, 0.0f));
	    g2.setColor(new java.awt.Color(153, 153, 153));

	    for (int iCol = 1; iCol < divisions; iCol++) {
		g2.drawLine(width, 0, width, height);
		width += width;
	    }
	}
    }
}