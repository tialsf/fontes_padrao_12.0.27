package kpi.swing.analisys.map;

import java.awt.Color;
import java.util.HashMap;
import kpi.xml.BIXMLRecord;

/**
 *
 * @author gilmar.pereira
 */
public class KpiMapaGrupo extends KpiMapaElemento {

    private HashMap<String, KpiMapaObjetivo> objectives = new HashMap();

    /**
     * Construtor padr�o
     */
    public KpiMapaGrupo() {
    }

    /**
     * Construtor baseado em valores de um XML
     * @param elementXml
     */
    public KpiMapaGrupo(BIXMLRecord elementXml) {
	id = elementXml.getString("ID");
	name = elementXml.getString("NOME");
	posX = elementXml.getInt("POSX");
	posY = elementXml.getInt("POSY");
	width = elementXml.getInt("WIDTH");
	height = elementXml.getInt("HEIGHT");
	fontColor = new Color(elementXml.getInt("FONTCOLOR"));
	backColor = new Color(elementXml.getInt("BACKCOLOR"));
	shape = elementXml.getString("SHAPE");
	opaque = elementXml.getBoolean("OPAQUE");
	visibleName = !elementXml.getBoolean("HIDENAME");
    }

    /**
     * @param key the objective to get
     * @return the objective
     */
    public KpiMapaObjetivo getObjective(String key) {
	return objectives.get(key);
    }

    /**
     * @return the objectives
     */
    public HashMap<String, KpiMapaObjetivo> getObjectives() {
	return objectives;
    }

    /**
     * @param objective the objective to set
     */
    public void addObjective(KpiMapaObjetivo objective) {
	this.objectives.put(objective.getId(), objective);
	objective.setGroup(this);
    }

    /**
     * @param objective the objective to remove
     */
    public void removeObjective(KpiMapaObjetivo objective) {
	removeObjective(objective.getId());
    }

    /**
     * @param key the key to remove
     */
    public void removeObjective(String key) {
	KpiMapaObjetivo objective = this.objectives.get(key);
	objective.setGroup(null);
	this.objectives.remove(key);
    }

    public void removeAllObjectives() {
	for (KpiMapaObjetivo objective : this.objectives.values()) {
	    objective.setGroup(null);
	}

	this.objectives.clear();
    }

    BIXMLRecord toXml() {
	BIXMLRecord groupXml = new BIXMLRecord("MAPAGRUPO");

	groupXml.set("ID", getId());
	groupXml.set("NOME", getName());
	groupXml.set("POSX", getPosX());
	groupXml.set("POSY", getPosY());
	groupXml.set("WIDTH", getWidth());
	groupXml.set("HEIGHT", getHeight());
	groupXml.set("SHAPE", getShape());
	groupXml.set("OPAQUE", isOpaque());
	groupXml.set("HIDENAME", !isVisibleName());
	groupXml.set("FONTCOLOR", (getFontColor().getRGB()& 0x00ffffff));
	groupXml.set("BACKCOLOR", (getBackColor().getRGB()& 0x00ffffff));
	groupXml.set("TYPE", KpiMapaElemento.ELEMENT_GROUP);

	return groupXml;
    }
}