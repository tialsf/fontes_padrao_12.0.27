package kpi.swing.analisys.map;

import java.awt.event.ComponentEvent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import kpi.core.KpiStaticReferences;
import kpi.swing.tools.KpiUpLoadAction;
import kpi.swing.tools.KpiUpload;

/**
 * 
 * @author gilmar.pereira
 */
public class KpiMapaImageUpload implements KpiUpLoadAction {

    private KpiMapaPerspectiva perspective = null;
    private KpiMapaFormController ctrl = null;
    private String fileName;
    private String filePath;

    KpiMapaImageUpload(KpiMapaPerspectiva o, KpiMapaFormController ctrl) {
	this.perspective = o;
	this.ctrl = ctrl;

	StringBuilder path = new StringBuilder();

	path.append("\\mapimagens\\map.");
	path.append(o.getMap().getId());
	path.append("\\");

	filePath = path.toString();
    }

    public void onUpLoadStart(ComponentEvent componentEvent) {
    }

    public void onUpLoadFinish(ComponentEvent componentEvent) {
	perspective.setImage(fileName);
        try {
            ctrl.updateRendererObject(perspective);
        } catch (MalformedURLException ex) {
            Logger.getLogger(KpiMapaImageUpload.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(KpiMapaImageUpload.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onAfterExecute(ComponentEvent componentEvent) {
    }

    public void onUpLoadError(ComponentEvent componentEvent) {
    }

    public void onUpLoadCancel(ComponentEvent componentEvent) {
    }

    public void onUpLoading(ComponentEvent componentEvent) {
    }

    public void upload(File imgFile) {
	KpiUpload frmUpload = (KpiUpload) KpiStaticReferences.getKpiFormController().getForm("KPIUPLOAD", "0", "");

	frmUpload.setPathDest(filePath);

	StringBuilder file = new StringBuilder();

	file.append("persp.");
	file.append(perspective.getId());
	file.append(".");
	file.append(String.valueOf(System.currentTimeMillis()));

	StringBuilder fullFile = new StringBuilder();
	fullFile.append("map.");
	fullFile.append(ctrl.getMap().getId());
	fullFile.append("/");
	fullFile.append(file);

	fileName = fullFile.toString();

	frmUpload.setArqName(file.toString());
	frmUpload.setCloseBoxAfterUploaded(true);
	frmUpload.addUpLoadAction(this);
	frmUpload.startUpload(imgFile);
    }
    
}