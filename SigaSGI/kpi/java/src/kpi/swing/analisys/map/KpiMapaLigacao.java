package kpi.swing.analisys.map;

import java.awt.Color;
import java.awt.Point;
import kpi.xml.BIXMLRecord;

/**
 *
 * @author gilmar.pereira
 */
public class KpiMapaLigacao implements IKpiMapaComponente {

    public static final String LINE = "1";
    public static final String CURVE = "2";
    private String id;
    private int controlX = 0;
    private int controlY = 0;
    private Color lineColor = Color.BLACK;
    private String type = LINE;
    private KpiMapaEstrategico map;
    private KpiMapaElemento source;
    private KpiMapaElemento destination;
    private static int idCount = 0;

    /**
     * Construtor padr�o
     */
    public KpiMapaLigacao() {
    }

    /**
     * Construtor baseado em valores de um XML
     * @param elementXml
     */
    public KpiMapaLigacao(BIXMLRecord elementXml) {
	id = elementXml.getString("ID");
	type = elementXml.getString("TYPE");
	controlX = elementXml.getInt("CONTROLX");
	controlY = elementXml.getInt("CONTROLY");
	lineColor = new Color(elementXml.getInt("COLOR"));
    }

    /**
     * @return the id
     */
    public String getId() {
	return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
	this.id = id;
    }

    /**
     * @return the map
     */
    public KpiMapaEstrategico getMap() {
	return map;
    }

    /**
     * @param map the map to set
     */
    public void setMap(KpiMapaEstrategico map) {
	this.map = map;
    }

    /**
     * @return the source
     */
    public KpiMapaElemento getSource() {
	return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(KpiMapaElemento source) {
	this.source = source;
    }

    /**
     * @return the destination
     */
    public KpiMapaElemento getDestination() {
	return destination;
    }

    /**
     * @param destination the destination to set
     */
    public void setDestination(KpiMapaElemento destination) {
	this.destination = destination;
    }

    /**
     * @return the controlX
     */
    public int getControlX() {
	return controlX;
    }

    /**
     * @param controlX the controlX to set
     */
    public void setControlX(int controlX) {
	this.controlX = controlX;
    }

    /**
     * @return the controlY
     */
    public int getControlY() {
	return controlY;
    }

    /**
     * @param controlY the controlY to set
     */
    public void setControlY(int controlY) {
	this.controlY = controlY;
    }

    /**
     * @return the lineColor
     */
    public Color getLineColor() {
	return lineColor;
    }

    /**
     * @param lineColor the lineColor to set
     */
    public void setLineColor(Color lineColor) {
	this.lineColor = lineColor;
    }

    /**
     * @return the type
     */
    public String getType() {
	return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
	this.type = type;
    }

    public static synchronized String generateId() {
	String newId = "new_".concat(String.format("%010d", ++idCount));

	return newId;
    }

    public void setControlPoint(Point centerPoint) {
	this.controlX = centerPoint.x;
	this.controlY = centerPoint.y;
    }

    public Point getControlPoint() {
	return new Point(this.controlX, this.controlY);
    }

    BIXMLRecord toXml() {
	BIXMLRecord linkXml = new BIXMLRecord("MAPALIGACAO");

	linkXml.set("ID", getId());
	linkXml.set("TYPE", getType());
	linkXml.set("CONTROLX", getControlX());
	linkXml.set("CONTROLY", getControlY());
	linkXml.set("SRCELMID", getSource().getId());
	linkXml.set("DSTELMID", getDestination().getId());
	linkXml.set("COLOR", (getLineColor().getRGB() & 0x00ffffff));

	return linkXml;
    }
}