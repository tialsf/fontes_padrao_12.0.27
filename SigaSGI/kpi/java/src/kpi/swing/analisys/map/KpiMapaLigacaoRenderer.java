package kpi.swing.analisys.map;

import java.awt.Color;
import java.awt.Point;
import kpi.util.KpiArrow2D;

/**
 *
 * @author gilmar.pereira
 */
public class KpiMapaLigacaoRenderer extends KpiArrow2D {

    private String id;
    private boolean selected = false;
    private Color lineColor;

    KpiMapaLigacaoRenderer(double zoom, Point pOrigem, Point pDestino, Point rCtrl) {
	super(zoom, pOrigem, pDestino, rCtrl);
    }

    KpiMapaLigacaoRenderer(Point pOrigem, Point pDestino, Point rCtrl) {
	super(pOrigem, pDestino, rCtrl);
    }

    KpiMapaLigacaoRenderer(Point pOrigem, Point pDestino) {
	super(pOrigem, pDestino);
    }

    /**
     * @return the id
     */
    public String getId() {
	return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
	this.id = id;
    }

    /**
     * @return the selected
     */
    public boolean isSelected() {
	return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(boolean selected) {
	this.selected = selected;
    }

    /**
     * @param lineColor the lineColor to set
     */
    public void setLineColor(Color lineColor) {
	this.lineColor = lineColor;
    }

    /**
     * @return the Line Color
     */
    public Color getLineColor() {
	return lineColor;
    }
}