
package kpi.swing.analisys.map;

/**
 *
 * @author gilmar.pereira
 */

public class KpiMapaMaximize extends javax.swing.JDialog {

    public javax.swing.JPanel pnlMapa;
    private IKpiMapaMainForm mapaParentFrame;

    public KpiMapaMaximize(java.awt.Container parent, boolean modal, javax.swing.JPanel pnlParent) {
	super();
	initComponents();
	this.setSize(java.awt.Toolkit.getDefaultToolkit().getScreenSize());
	this.setLocationRelativeTo(parent);
	pnlMapa = pnlParent;
	pnlMaximize.add(pnlParent, java.awt.BorderLayout.CENTER);
	setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
    }

    public void setMapaFrame(IKpiMapaMainForm oKpiMapaFrame) {
	mapaParentFrame = oKpiMapaFrame;
    }

        private void initComponents() {//GEN-BEGIN:initComponents
                pnlMaximize = new javax.swing.JPanel();

                setTitle("SGI");
                setModal(true);
                setName("dlgAvancado");
                addComponentListener(new java.awt.event.ComponentAdapter() {
                        public void componentShown(java.awt.event.ComponentEvent evt) {
                                formComponentShown(evt);
                        }
                });
                addWindowListener(new java.awt.event.WindowAdapter() {
                        public void windowClosing(java.awt.event.WindowEvent evt) {
                                closeDialog(evt);
                        }
                });

                pnlMaximize.setLayout(new java.awt.BorderLayout());

                getContentPane().add(pnlMaximize, java.awt.BorderLayout.CENTER);

                pack();
        }//GEN-END:initComponents

    private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
    }//GEN-LAST:event_formComponentShown

    /** Closes the dialog */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog

	mapaParentFrame.toggleMaximize();
    }//GEN-LAST:event_closeDialog
        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JPanel pnlMaximize;
        // End of variables declaration//GEN-END:variables
}