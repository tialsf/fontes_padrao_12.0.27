package kpi.swing.analisys.map;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author gilmar.pereira
 */
public abstract class KpiMapaMenuAction implements ActionListener {

    IKpiMapaComponente menuElement;
    KpiMapaFormController ctrl;

    KpiMapaMenuAction(IKpiMapaComponente o, KpiMapaFormController ctrl) {
        this.menuElement = o;
        this.ctrl = ctrl;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            mnuAction(e, menuElement, ctrl);
            ctrl.updateRendererObject(menuElement);
        } catch (Exception ex) {
        }
    }

    public abstract void mnuAction(ActionEvent e, IKpiMapaComponente o, KpiMapaFormController c);
}