package kpi.swing.analisys.map;

import java.awt.Color;
import kpi.xml.BIXMLRecord;

/**
 *
 * @author gilmar.pereira
 */

public class KpiMapaObjetivo extends KpiMapaElemento {

    private KpiMapaGrupo group;
    private String sourceId = "";

    /**
     * Construtor padr�o
     */
    public KpiMapaObjetivo() {
    }

    /**
     * Construtor baseado em valores de um XML
     * @param elementXml
     */
    public KpiMapaObjetivo(BIXMLRecord elementXml) {
	id = elementXml.getString("ID");
	name = elementXml.getString("OBJETIVO_NOME");
	sourceId = elementXml.getString("OBJETIVO_ID");
	posX = elementXml.getInt("POSX");
	posY = elementXml.getInt("POSY");
	width = elementXml.getInt("WIDTH");
	height = elementXml.getInt("HEIGHT");
	fontColor = new Color(elementXml.getInt("FONTCOLOR"));
	backColor = new Color(elementXml.getInt("BACKCOLOR"));
	shape = elementXml.getString("SHAPE");
	opaque = elementXml.getBoolean("OPAQUE");
	visibleName = !elementXml.getBoolean("HIDENAME");
    }

    /**
     * @return the group
     */
    public KpiMapaGrupo getGroup() {
	return group;
    }

    /**
     * @param group the group to set
     */
    public void setGroup(KpiMapaGrupo group) {
	this.group = group;
    }

    /**
     * @return the sourceId
     */
    public String getSourceId() {
	return sourceId;
    }

    /**
     * @param sourceId the sourceId to set
     */
    public void setSourceId(String sourceId) {
	this.sourceId = sourceId;
    }

    BIXMLRecord toXml() {
	BIXMLRecord objectiveXml = new BIXMLRecord("MAPAOBJETIVO");

	objectiveXml.set("ID", getId());

	objectiveXml.set("POSX", getPosX());
	objectiveXml.set("POSY", getPosY());
	objectiveXml.set("WIDTH", getWidth());
	objectiveXml.set("HEIGHT", getHeight());
	objectiveXml.set("SHAPE", getShape());
	objectiveXml.set("OPAQUE", isOpaque());
	objectiveXml.set("HIDENAME", !isVisibleName());
	objectiveXml.set("FONTCOLOR", (getFontColor().getRGB()& 0x00ffffff));
	objectiveXml.set("BACKCOLOR", (getBackColor().getRGB()& 0x00ffffff));

	if (getGroup() != null) {
	    objectiveXml.set("MAPAGRUPO_ID", getGroup().getId());
	} else {
	    objectiveXml.set("MAPAGRUPO_ID", "");
	}

	objectiveXml.set("OBJETIVO_ID", getSourceId());
	objectiveXml.set("OBJETIVO_NOME", getName());

	objectiveXml.set("TYPE", KpiMapaElemento.ELEMENT_OBJECTIVE);

	return objectiveXml;
    }
}
