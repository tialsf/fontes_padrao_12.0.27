package kpi.swing.analisys.map;

import java.awt.Color;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Iterator;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

/**
 * 
 * @author gilmar.pereira
 */
public class KpiMapaPerspectiva implements IKpiMapaComponente {

    private String id = "";
    private String sourceId = "";
    private String name = "";
    private String image = "";
    private int order = 0;
    private int height = 0;
    private Color fontColor = Color.BLACK;
    private Color backColor = Color.BLACK;
    private HashMap<String, KpiMapaElemento> elements = new HashMap();
    private KpiMapaEstrategico map;
    private static int idCount = 0;
    private String mapImage = "";

    /**
     * Construtor padr�o
     */
    public KpiMapaPerspectiva() {
    }

    /**
     * Construtor baseado em valores de um XML
     * @param elementXml
     */
    public KpiMapaPerspectiva(BIXMLRecord elementXml) {
	id = elementXml.getString("ID");
	name = elementXml.getString("PERSPECTIVA_NOME");
	order = elementXml.getInt("PERSPECTIVA_ORDEM");
	sourceId = elementXml.getString("PERSPECTIVA_ID");
	image = elementXml.getString("IMAGE");
	height = elementXml.getInt("HEIGHT");
	fontColor = new Color(elementXml.getInt("FONTCOLOR"));
	backColor = new Color(elementXml.getInt("BACKCOLOR"));
    }

    /**
     * @return the id
     */
    public String getId() {
	return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
	this.id = id;
    }
    
    public String getMapImage(){
        return mapImage;
    }
    
    public void setMapImage(String mapImage){
        this.mapImage = mapImage;
    }

    /**
     * @return the name
     */
    public String getName() {
	return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
	this.name = name;
    }

    /**
     * @return the height
     */
    public int getHeight() {
	return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(int height) {
	this.height = height;
    }

    /**
     * @return the image
     */
    public String getImage() {
	return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(String image) {
	this.image = image;
    }

    /**
     * @return the order
     */
    public int getOrder() {
	return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(int order) {
	this.order = order;
    }

    /**
     * @return the backColor
     */
    public Color getBackColor() {
	return backColor;
    }

    /**
     * @param backColor the backColor to set
     */
    public void setBackColor(Color backColor) {
	this.backColor = backColor;
    }

    /**
     * @return the fontColor
     */
    public Color getFontColor() {
	return fontColor;
    }

    /**
     * @param fontColor the fontColor to set
     */
    public void setFontColor(Color fontColor) {
	this.fontColor = fontColor;
    }

    /**
     * @return the elements
     */
    public HashMap<String, KpiMapaElemento> getElements() {
	return elements;
    }

    /**
     * @param key the element to get
     * @return the element
     */
    public KpiMapaElemento getElement(String key) {
	return elements.get(key);
    }

    /**
     * @param element the element to set
     */
    public void addElement(KpiMapaElemento element) {
	this.elements.put(element.getId(), element);
	element.setPerspective(this);
    }

    /**
     * @param element the element to remove
     */
    public void removeElement(KpiMapaElemento element) {
	removeElement(element.getId());
    }

    /**
     * @param key the key to remove
     */
    public void removeElement(String key) {
	KpiMapaElemento element = this.elements.get(key);
	element.setPerspective(null);
	this.elements.remove(key);
    }

    /**
     * @return the map
     */
    public KpiMapaEstrategico getMap() {
	return map;
    }

    /**
     * @param map the map to set
     */
    public void setMap(KpiMapaEstrategico map) {
	this.map = map;
    }

    /**
     * @return the sourceId
     */
    public String getSourceId() {
	return sourceId;
    }

    /**
     * @param sourceId the sourceId to set
     */
    public void setSourceId(String sourceId) {
	this.sourceId = sourceId;
    }

    /**
     * @param rIntersect rectangle reference to look for elements
     * @return the elements inside rIntersect
     */
    public HashMap<String, KpiMapaElemento> getElementsIntersect(Rectangle rIntersect) {

	HashMap<String, KpiMapaElemento> elementsIntersect = new HashMap();

	//percorre todos os objetivos da perspectiva atual
	for (Iterator<KpiMapaElemento> itElm = this.getElements().values().iterator(); itElm.hasNext();) {
	    KpiMapaElemento elm = itElm.next();

	    //verifica se o elemento est� na �rea delimitada pelo novo grupo
	    if (rIntersect.contains(elm.getBounds())) {
		elementsIntersect.put(elm.getId(), elm);
	    }
	}

	return elementsIntersect;
    }

    public static synchronized String generateId() {
	String newId = "new_".concat(String.format("%010d", ++idCount));

	return newId;
    }

    BIXMLRecord toXml() {
	BIXMLRecord perspXml = new BIXMLRecord("MAPAPERSPECTIVA");

	perspXml.set("ID", getId());
	perspXml.set("PERSPECTIVA_NOME", getName());
	perspXml.set("PERSPECTIVA_ORDEM", getOrder());
	perspXml.set("PERSPECTIVA_ID", getSourceId());
	perspXml.set("IMAGE", getImage());
	perspXml.set("HEIGHT", getHeight());
	perspXml.set("FONTCOLOR", (getFontColor().getRGB() & 0x00ffffff));
	perspXml.set("BACKCOLOR", (getBackColor().getRGB() & 0x00ffffff));

	BIXMLVector groupXml = new BIXMLVector("MAPAGRUPOS", "MAPAGRUPO");
	BIXMLVector objectiveXml = new BIXMLVector("MAPAOBJETIVOS", "MAPAOBJETIVO");

	for (KpiMapaElemento element : getElements().values()) {
	    if (element instanceof KpiMapaGrupo) {
		//Grupos
		groupXml.add(((KpiMapaGrupo) element).toXml());

	    } else if (element instanceof KpiMapaObjetivo) {
		//Objetivos
		objectiveXml.add(((KpiMapaObjetivo) element).toXml());
	    }
	}

	perspXml.set(groupXml);
	perspXml.set(objectiveXml);

	return perspXml;
    }
}
