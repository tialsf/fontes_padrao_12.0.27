package kpi.swing.analisys.widget;

import ChartDirector.BarLayer;
import ChartDirector.Chart;
import ChartDirector.ChartViewer;
import ChartDirector.TextBox;
import ChartDirector.XYChart;
import java.text.DecimalFormat;
import java.util.ArrayList;
import kpi.core.KpiStaticReferences;

public class KpiAreaGraphBar extends KpiAreaGraph {
    private final int COLOR_PAR = 0xa9b4747;
    private final int COLOR_ACU = 0x5c8082;

    public KpiAreaGraphBar(KpiIndicadorBean ind, KpiIndicadorArea area) {
        super(ind, area);
    }

    @Override
    protected void createGraph(int width, int height) {
        int arrayDim = (this.getIndBean().isPreviaVisible()) ? 3 : 2;
        double[] data = new double[arrayDim];
        double[] dataAc = new double[arrayDim];
        String[] labels = new String[arrayDim];
        double minValue = getMinValue();
        double maxValue = getMaxValue();
        int nRow = 0;

        XYChart c = new XYChart(width, height);
        c.setNumberFormat(getThousantSeparator(), getDecimalPoiter(), '-');
        c.setPlotArea(40, 20, (int) (width * 0.72), (int) (height * 0.60));
        c.addLegend(40, 0, false, "Tahoma", 7).setBackground(Chart.Transparent);

        BarLayer layer = c.addBarLayer2(Chart.Side, 3);

        //Previa
        if (this.getIndBean().isPreviaVisible()) {
            labels[nRow] = KpiStaticReferences.getKpiCustomLabels().getPrevia();
            data[nRow] = getPrevia(KpiTypeAnalisys.PARC);
            dataAc[nRow] = getPrevia(KpiTypeAnalisys.ACUM);
            nRow++;
        }

        //Real
        labels[nRow] = KpiStaticReferences.getKpiCustomLabels().getReal();
        data[nRow] = getReal(KpiTypeAnalisys.PARC);
        dataAc[nRow] = getReal(KpiTypeAnalisys.ACUM);
        nRow++;

        //Meta
        labels[nRow] = KpiStaticReferences.getKpiCustomLabels().getMeta();
        data[nRow] = getMeta(KpiTypeAnalisys.PARC);
        dataAc[nRow] = getMeta(KpiTypeAnalisys.ACUM);
        nRow++;

        //Varia��o
        /*
        labels[nRow] = "Varia��o";
        data[nRow] = (getReal(KpiTypeAnalisys.PARC) - getMeta(KpiTypeAnalisys.PARC));
        dataAc[nRow] = (getReal(KpiTypeAnalisys.ACUM) - getMeta(KpiTypeAnalisys.ACUM));
        nRow++;
        */

        switch (getAreaType().getAnalisysType()) {
            case PARC:
                layer.addDataSet(data, COLOR_PAR, this.getIndBean().getPeriodo());
                break;
            case ACUM:
                layer.addDataSet(dataAc, COLOR_ACU, "Acumulado");
                break;
            case BOTH:
                layer.addDataSet(data, COLOR_PAR, this.getIndBean().getPeriodo());
                layer.addDataSet(dataAc, COLOR_ACU, "Acumulado");
                break;
        }

        //Label X
        c.xAxis().setLabels(labels);
        c.xAxis().setLabelStyle("Tahoma", 7, Chart.TextColor, 0);

        //Label Y
        ArrayList<String> yLabels = new ArrayList<String>();
        yLabels.add(formatNumberGraph(minValue));
        yLabels.add(formatNumberGraph(minValue + ((maxValue - minValue) / 2)));
        yLabels.add(formatNumberGraph(maxValue));
        c.yAxis().setLinearScale(minValue,maxValue,yLabels.toArray(new String[yLabels.size()]));
        TextBox txtLabely = c.yAxis().setLabelStyle("Tahoma", 7, Chart.TextColor, 0);
        if (formatNumberGraph(maxValue).length() > 6 ){
            txtLabely.setFontAngle(45);
            txtLabely.setFontSize(7);
        }
            

        ChartViewer viewer = new ChartViewer();
        viewer.setImage(c.makeImage());
        viewer.setImageMap(c.getHTMLImageMap("clickable", "", "title='{value|" + getIndBean().getCasasDecimais() + ".,} (" + getIndBean().getUnidadeMed() + ")'"));

        add(viewer);
    }


    @Override
     public String formatNumberGraph(double value) {
        DecimalFormat formatador;
        String sRet = "";

        if (getAreaType().isPercentValue()) {
            formatador = new DecimalFormat("0");
            sRet = formatador.format(value).concat("%");
        } else {
            formatador = new DecimalFormat("0");
            formatador.setMaximumFractionDigits(0);
            sRet = formatador.format(value);
        }

        return sRet;
    }

}
