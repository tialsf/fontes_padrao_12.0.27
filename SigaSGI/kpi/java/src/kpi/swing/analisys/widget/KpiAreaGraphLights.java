package kpi.swing.analisys.widget;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;
import kpi.swing.analisys.KpiScoreCarding;
import kpi.swing.analisys.widget.KpiAreaGraphLightsPanel.StatusLight;

public class KpiAreaGraphLights extends KpiAreaGraph {

    public KpiAreaGraphLights(KpiIndicadorBean ind, KpiIndicadorArea area) {
        super(ind, area);
    }

    @Override
    protected void createGraph(int width, int height) {
        KpiTypeAnalisys typeAnalisys = getAreaType().getAnalisysType();
        JPanel pnlGraph = new JPanel();

        pnlGraph.setPreferredSize(new Dimension(width, height));
        pnlGraph.setLayout(new java.awt.BorderLayout());
        pnlGraph.setBackground(Color.WHITE);

        if (typeAnalisys == KpiTypeAnalisys.BOTH) {
            int areaWidth = width / 2;

            JPanel pnlAcum = new JPanel();
            pnlAcum.setLayout(new java.awt.BorderLayout());
            pnlAcum.setPreferredSize(new Dimension(areaWidth, height));
            pnlAcum.setBackground(Color.WHITE);
            pnlAcum.add(doGraph(KpiTypeAnalisys.ACUM, areaWidth, height));

            JPanel pnlPar = new JPanel();
            pnlPar.setLayout(new java.awt.BorderLayout());
            pnlPar.setPreferredSize(new Dimension(areaWidth, height));
            pnlPar.setBackground(Color.WHITE);
            pnlPar.add(doGraph(KpiTypeAnalisys.PARC, areaWidth, height));
  
            pnlGraph.add(pnlAcum, BorderLayout.WEST);
            pnlGraph.add(pnlPar, BorderLayout.EAST);

        } else {
            pnlGraph.add(doGraph(typeAnalisys, width, height));
        }

        add(pnlGraph);

    }

    private JPanel doGraph(KpiTypeAnalisys typeAnalisys, int width, int height) {
        KpiAreaGraphLightsPanel graphLights = new KpiAreaGraphLightsPanel();
        graphLights.setPreferredSize(new Dimension(width, height));
        graphLights.setDescription(getAnalisysText(typeAnalisys));

        KpiIndicadorValue indValue;
        if (typeAnalisys == KpiTypeAnalisys.PARC) {
            indValue = getIndBean().getValuePer();
        } else {
            indValue = getIndBean().getValueAcum();
        }

        switch (indValue.getVariacaoStatus()) {
            case KpiScoreCarding.STATUS_RED:
                graphLights.setStatusImage(StatusLight.STATUS_RED);
                break;
            case KpiScoreCarding.STATUS_YELLOW:
                graphLights.setStatusImage(StatusLight.STATUS_YELLOW);
                break;
            case KpiScoreCarding.STATUS_GREEN:
                graphLights.setStatusImage(StatusLight.STATUS_GREEN);
                break;
            case KpiScoreCarding.ESTAVEL_GRAY:
                graphLights.setStatusImage(StatusLight.STATUS_GRAY);
                break;
                
            case KpiScoreCarding.STATUS_BLUE:
                graphLights.setStatusImage(StatusLight.STATUS_BLUE);
                break;
        }

        return graphLights;

    }
}
