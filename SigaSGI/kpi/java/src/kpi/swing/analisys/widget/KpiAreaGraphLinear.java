package kpi.swing.analisys.widget;

import ChartDirector.Chart;
import ChartDirector.ChartViewer;
import ChartDirector.LinearMeter;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JComponent;
import javax.swing.JPanel;


public class KpiAreaGraphLinear extends KpiAreaGraph {
    ChartViewer viewer;
    LinearMeter m;

    public KpiAreaGraphLinear(KpiIndicadorBean ind, KpiIndicadorArea area) {
        super(ind,area);
    }

    @Override
    protected void createGraph(int width, int height) {
        KpiTypeAnalisys typeAnalisys = getAreaType().getAnalisysType();
        JPanel pnlGraph = new JPanel();

        pnlGraph.setPreferredSize(new Dimension(width,height));
        pnlGraph.setLayout(new java.awt.BorderLayout());
        pnlGraph.setBackground(Color.WHITE);

        if (typeAnalisys == KpiTypeAnalisys.BOTH){
            int areaHeight = (int) (height/2.3);

            JPanel pnlAcum = new JPanel();
            pnlAcum.setLayout(new java.awt.BorderLayout());
            pnlAcum.setPreferredSize(new Dimension(width,areaHeight));
            pnlAcum.add(doGraph(KpiTypeAnalisys.ACUM,width,areaHeight));
            pnlAcum.setBackground(Color.WHITE);

            JPanel pnlPar = new JPanel();
            pnlPar.setLayout(new java.awt.BorderLayout());
            pnlPar.setPreferredSize(new Dimension(width,areaHeight));
            pnlPar.add(doGraph(KpiTypeAnalisys.PARC,width,areaHeight));
            pnlPar.setBackground(Color.WHITE);


            pnlGraph.add(pnlAcum,BorderLayout.NORTH);
            pnlGraph.add(pnlPar,BorderLayout.CENTER);

        } else{
            pnlGraph.add(doGraph(typeAnalisys,width,height));
        }

        add(pnlGraph);
    }

    
    protected JComponent doGraph(KpiTypeAnalisys typeAnalisys, int width, int height) {
        double minValue = getMinValue(typeAnalisys);
        double maxValue = getMaxValue(typeAnalisys);
        
        viewer = new ChartViewer();
        m = new LinearMeter(width, height, Chart.BackgroundColor, Chart.Transparent, 0);
        m.setLabelFormat("{value|" + getIndBean().getCasasDecimais() + "}");
        m.setNumberFormat(getThousantSeparator(),getDecimalPoiter(),'-');
        m.setMeter(25, (int) (height*0.4), (int) (width*0.8), (int) (height*0.25), Chart.Top);
        m.setLineWidth(0, 2, 0);
        m.setLabelPos(false, 5);
        m.setLabelStyle("Tahoma", 7, Chart.TextColor, 0);
        m.addText((int) (width * 0.5), (int) (height * 0.90), getAnalisysText(typeAnalisys), "Tahoma", 7, Chart.TextColor, Chart.Center);

        switch (typeAnalisys) {
            case PARC:
                m.addPointer(getReal(typeAnalisys), getGraphConfig().getRealColor()).setShape(Chart.TriangularPointer);
                if (getIndBean().isPreviaVisible()) {
                    m.addPointer(getPrevia(typeAnalisys), getGraphConfig().getPreviaColor()).setShape(Chart.PencilPointer);
                }
                addRange(getRange(typeAnalisys));
                break;
            case ACUM:
                m.addPointer(getReal(typeAnalisys), getGraphConfig().getRealAcumColor()).setShape(Chart.TriangularPointer);
                addRange(getRange(typeAnalisys));
                break;
            case BOTH:
                m.addPointer(getReal(KpiTypeAnalisys.ACUM), getGraphConfig().getRealAcumColor()).setShape(Chart.TriangularPointer);
                m.addPointer(getReal(KpiTypeAnalisys.PARC), getGraphConfig().getRealColor()).setShape(Chart.TriangularPointer, 0.7, 0.5);
                if (getIndBean().isPreviaVisible()) {
                    m.addPointer(getPrevia(KpiTypeAnalisys.PARC), getGraphConfig().getPreviaColor()).setShape(Chart.TriangularPointer, 0.6, 0.5);
                }
                addRange(getRange(KpiTypeAnalisys.ACUM));

                break;
        }


         //Labels
        ArrayList<String> sLabel = new ArrayList<String>();
        String sSpace = "";
        // Verifica a orientação para apresentar os dados no eixo horizontal corretamente.
        if (getIndBean().isAscendente() || getIndBean().isMelhorFaixa()) {
            sLabel.add(formatNumberGraph(minValue).concat(sSpace));
            sLabel.add(formatNumberGraph(minValue + ((maxValue - minValue) / 2)));
            sLabel.add(sSpace.concat(formatNumberGraph(maxValue)));
            m.setScale(minValue, maxValue, sLabel.toArray(new String[sLabel.size()]));
        } else {
            sLabel.add(formatNumberGraph(maxValue).concat(sSpace));
            sLabel.add(formatNumberGraph(minValue + ((maxValue - minValue) / 2)));
            sLabel.add(sSpace.concat(formatNumberGraph(minValue)));
            m.setScale(maxValue, minValue, sLabel.toArray(new String[sLabel.size()]));
        }


        viewer.setImage(m.makeImage());
        viewer.setHotSpotCursor(new Cursor(Cursor.HAND_CURSOR));
        viewer.setImageMap(m.getHTMLImageMap("clickable", "", "title='{xLabel}: {value}'"));

        return viewer;
    }

    private void addRange(ArrayList<KpiGraphRange> aRange) {
        for (Iterator<KpiGraphRange> it = aRange.iterator(); it.hasNext();) {
            KpiGraphRange oRange = it.next();
            m.addZone(oRange.getStartValue(), oRange.getEndValue(), oRange.getColor());
        }
    }

}