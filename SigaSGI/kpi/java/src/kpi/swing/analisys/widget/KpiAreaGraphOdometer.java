package kpi.swing.analisys.widget;

import ChartDirector.AngularMeter;
import ChartDirector.Chart;
import ChartDirector.ChartViewer;
import java.awt.Cursor;
import java.util.ArrayList;
import java.util.Iterator;

public class KpiAreaGraphOdometer extends KpiAreaGraph {

    ChartViewer viewer;
    AngularMeter m;

    public KpiAreaGraphOdometer(KpiIndicadorBean ind, KpiIndicadorArea area) {
        super(ind, area);
    }

    @Override
    protected void createGraph(int width, int height) {
        double minValue = getMinValue();
        double maxValue = getMaxValue();
        KpiTypeAnalisys typeAnalisys = getAreaType().getAnalisysType();

        viewer = new ChartViewer();

        m = new AngularMeter(width, height, Chart.BackgroundColor, Chart.Transparent, 0);
        m.setLabelFormat("{value|" + getIndBean().getCasasDecimais() + "}");
        m.setNumberFormat(getThousantSeparator(), getDecimalPoiter(), '-');
        m.setMeter((int) (width * 0.5), (int) (height * 0.77), (int) (height * 0.58), -90, 90);
        m.setLineWidth(0, 2, 1);
        m.setLabelPos(false, 3);
        m.setLabelStyle("Tahoma", 7, Chart.TextColor, 0);
        m.addText((int) (width * 0.5), (int) (height * 0.88), getAnalisysText(typeAnalisys), "Tahoma", 7, Chart.TextColor, Chart.Center);

        switch (typeAnalisys) {
            case PARC:
                m.addPointer(getReal(typeAnalisys), getGraphConfig().getRealColor()).setShape(Chart.TriangularPointer);
                if (getIndBean().isPreviaVisible()) {
                    m.addPointer(getPrevia(typeAnalisys), getGraphConfig().getPreviaColor()).setShape(Chart.PencilPointer);
                }
                addRange(getRange(typeAnalisys));

                break;
            case ACUM:
                m.addPointer(getReal(typeAnalisys), getGraphConfig().getRealAcumColor()).setShape(Chart.TriangularPointer);
                addRange(getRange(typeAnalisys));

                break;
            case BOTH:
                m.addPointer(getReal(KpiTypeAnalisys.ACUM), getGraphConfig().getRealAcumColor()).setShape(Chart.TriangularPointer);
                m.addPointer(getReal(KpiTypeAnalisys.PARC), getGraphConfig().getRealColor()).setShape(Chart.TriangularPointer, 0.7, 0.5);
                if (getIndBean().isPreviaVisible()) {
                    m.addPointer(getPrevia(KpiTypeAnalisys.PARC), getGraphConfig().getPreviaColor()).setShape(Chart.TriangularPointer, 0.6, 0.5);
                }

                addRange(getRange(typeAnalisys));
                addSubRange(getRange(getMeta(KpiTypeAnalisys.PARC),getMinValue(),getMaxValue(KpiTypeAnalisys.PARC)));
                

                break;
        }

        //Labels
        ArrayList<String> sLabel = new ArrayList<String>();
        String sSpace = "          ";
        // Verifica a orienta��o para apresentar os dados no eixo horizontal corretamente.
        if (getIndBean().isAscendente() || getIndBean().isMelhorFaixa()) {
            sLabel.add(formatNumberGraph(minValue).concat(sSpace));
            sLabel.add(formatNumberGraph(minValue + ((maxValue - minValue) / 2)));
            sLabel.add(sSpace.concat(formatNumberGraph(maxValue)));
            m.setScale(minValue, maxValue, sLabel.toArray(new String[sLabel.size()]));
        } else {
            sLabel.add(formatNumberGraph(maxValue).concat(sSpace));
            sLabel.add(formatNumberGraph(minValue + ((maxValue - minValue) / 2)));
            sLabel.add(sSpace.concat(formatNumberGraph(minValue)));
            m.setScale(maxValue, minValue, sLabel.toArray(new String[sLabel.size()]));
        }

        viewer.setImage(m.makeImage());
        viewer.setHotSpotCursor(new Cursor(Cursor.HAND_CURSOR));
        viewer.setImageMap(m.getHTMLImageMap(
                "clickable",
                "",
                "title='Per�odo:{xLabel} \n Valor:{value}'"));

        add(viewer);
    }

    private void addRange(ArrayList<KpiGraphRange> aRange) {
        for (Iterator<KpiGraphRange> it = aRange.iterator(); it.hasNext();) {
            KpiGraphRange oRange = it.next();
            m.addZone(oRange.getStartValue(), oRange.getEndValue(), oRange.getColor());
        }
    }

    private void addSubRange(ArrayList<KpiGraphRange> aRange) {
        for (Iterator<KpiGraphRange> it = aRange.iterator(); it.hasNext();) {
            KpiGraphRange oRange = it.next();
            m.addZone(oRange.getStartValue(), oRange.getEndValue(), 35, 40, oRange.getColor());
        }
    }
}
