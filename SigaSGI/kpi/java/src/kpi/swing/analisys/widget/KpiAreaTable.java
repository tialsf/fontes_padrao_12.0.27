/*
 * KpiScoTableView.java
 *
 * Created on Apr 19, 2010, 10:49:51 AM
 */
package kpi.swing.analisys.widget;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import kpi.core.KpiStaticReferences;
import kpi.swing.analisys.KpiScoreCarding;

/**
 *
 * @author lucio.pelinson
 */
public class KpiAreaTable extends KpiArea {

    private KpiTypeAnalisys typeAnalisys = getAreaType().getAnalisysType();

    public KpiAreaTable(KpiIndicadorBean ind, KpiIndicadorArea area) {
        super(ind, area);
        initComponents();
    }

    @Override
    protected void build(int width, int height) {
        doPlanValues();
        doPlanRender();
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {//GEN-BEGIN:initComponents

        jScrollPane1 = new javax.swing.JScrollPane();
        tabela = new javax.swing.JTable();

        setLayout(new java.awt.BorderLayout());

        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setOpaque(false);
        jScrollPane1.setViewportView(tabela);

        add(jScrollPane1, java.awt.BorderLayout.CENTER);
    }//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabela;
    // End of variables declaration//GEN-END:variables

    private void doPlanValues() {
        Vector tb = new Vector();
        Vector col = new Vector();
        Vector real = new Vector();
        Vector meta = new Vector();
        Vector variacao = new Vector();


        if (getIndBean().isPreviaVisible()) {
            Vector previa = new Vector();
            previa.add(KpiStaticReferences.getKpiCustomLabels().getPrevia());
            if (typeAnalisys == KpiTypeAnalisys.PARC || typeAnalisys == KpiTypeAnalisys.BOTH) {
                previa.add(String.valueOf(getIndBean().getValuePer().getPrevia()));
            }

            if (typeAnalisys == KpiTypeAnalisys.ACUM || typeAnalisys == KpiTypeAnalisys.BOTH) {
                previa.add(String.valueOf(getIndBean().getValueAcum().getPrevia()));
            }
            tb.add(previa);
        }


        col.add("");
        real.add(KpiStaticReferences.getKpiCustomLabels().getReal());
        meta.add(KpiStaticReferences.getKpiCustomLabels().getMeta());
        variacao.add("Varia��o");

        if (typeAnalisys == KpiTypeAnalisys.PARC || typeAnalisys == KpiTypeAnalisys.BOTH) {
            col.add(getIndBean().getPeriodo());
            real.add(String.valueOf(getIndBean().getValuePer().getReal()));
            meta.add(String.valueOf(getIndBean().getValuePer().getMeta()));
            variacao.add(getIndBean().getValuePer().getVariacao());
        }

        if (typeAnalisys == KpiTypeAnalisys.ACUM || typeAnalisys == KpiTypeAnalisys.BOTH) {
            col.add("Acumulado");
            real.add(String.valueOf(getIndBean().getValueAcum().getReal()));
            meta.add(String.valueOf(getIndBean().getValueAcum().getMeta()));
            variacao.add(getIndBean().getValueAcum().getVariacao());
        }

        tb.add(real);
        tb.add(meta);
        tb.add(variacao);


        KpiScoDefaultTableModel dtm = new KpiScoDefaultTableModel(tb, col);
        tabela.setModel(dtm);
    }

    private void doPlanRender() {
        switch (typeAnalisys) {
            case PARC:
                tabela.getColumnModel().getColumn(1).setCellRenderer(new KpiScoTableRenderer(getIndBean(), KpiScoTableRenderer.periodo.PARCELADO));
                tabela.getColumnModel().getColumn(0).setHeaderRenderer(new HeaderRenderer());
                tabela.getColumnModel().getColumn(1).setHeaderRenderer(new HeaderRenderer());
                break;

            case ACUM:
                tabela.getColumnModel().getColumn(1).setCellRenderer(new KpiScoTableRenderer(getIndBean(), KpiScoTableRenderer.periodo.ACUMULADO));
                tabela.getColumnModel().getColumn(0).setHeaderRenderer(new HeaderRenderer());
                tabela.getColumnModel().getColumn(1).setHeaderRenderer(new HeaderRenderer());
                break;

            case BOTH:
                tabela.getColumnModel().getColumn(1).setCellRenderer(new KpiScoTableRenderer(getIndBean(), KpiScoTableRenderer.periodo.PARCELADO));
                tabela.getColumnModel().getColumn(2).setCellRenderer(new KpiScoTableRenderer(getIndBean(), KpiScoTableRenderer.periodo.ACUMULADO));
                tabela.getColumnModel().getColumn(0).setHeaderRenderer(new HeaderRenderer());
                tabela.getColumnModel().getColumn(1).setHeaderRenderer(new HeaderRenderer());
                tabela.getColumnModel().getColumn(2).setHeaderRenderer(new HeaderRenderer());
                break;
        }

        jScrollPane1.getViewport().setBackground(Color.white);
    }
}

/**
 * Render espec�fico para c�lulas da tabela. 
 * @author valdineyg
 */
class KpiScoTableRenderer extends DefaultTableCellRenderer {

    public enum periodo {

        ACUMULADO, PARCELADO
    }
    private KpiIndicadorBean indBean;
    private KpiIndicadorValue indValue;
    private int variacaoRow;
    private java.awt.Font fonteType = new java.awt.Font("Tahoma", 0, 11);
    private java.awt.Color corBranco = new java.awt.Color(255, 255, 255);
    private java.awt.Color statusVermelho = new java.awt.Color(234, 140, 136);
    private java.awt.Color statusAmarelo = new java.awt.Color(255, 235, 155);
    private java.awt.Color statusVerde  = new java.awt.Color(139, 191, 150);
    private java.awt.Color statusCinza  = new java.awt.Color(228, 228, 228);
    private java.awt.Color statusBlue   = new java.awt.Color(109,178,247);
    private java.util.Locale locale = kpi.core.KpiStaticReferences.getKpiDefaultLocale();
    private pv.jfcx.JPVEdit oRender = new pv.jfcx.JPVEdit();
    private java.text.NumberFormat oValor = null;

    public KpiScoTableRenderer(KpiIndicadorBean indBean, periodo type) {
        this.indBean = indBean;
        if (type == periodo.ACUMULADO) {
            this.indValue = indBean.getValueAcum();
        } else if (type == periodo.PARCELADO) {
            this.indValue = indBean.getValuePer();
        }

        if (indBean.isPreviaVisible()) {
            variacaoRow = 3;
        } else {
            variacaoRow = 2;
        }

    }

    @Override
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        oRender.setBackground(corBranco);
        oRender.setForeground(java.awt.Color.BLACK);
        oRender.setAlignment(0);
        oRender.setFontStyle(pv.jfcx.JPVEdit.NORMAL);
        oRender.setLocale(locale);
        oRender.setLAF(false);
        oRender.setBorderStyle(0);
        oRender.setFont(fonteType);
        oRender.setText("");
        oRender.setToolTipText("");

        table.setGridColor(new Color(211, 211, 211));
        table.setRowHeight(20);

        if (value != null) {
            if (row == variacaoRow) {
                renderVar(oRender, value.toString(), column, row, table);
            } else {
                renderNumber(oRender, Double.parseDouble(value.toString()), column, row, table);
            }
        }

        return oRender;
    }

    private pv.jfcx.JPVEdit renderVar(pv.jfcx.JPVEdit oRender, String value, int column, int row, javax.swing.JTable table) {
        oValor = java.text.NumberFormat.getInstance();
        int status = 0;

        status = indValue.getVariacaoStatus();

        switch (status) {
            case KpiScoreCarding.STATUS_RED:
                oRender.setBackground(statusVermelho);
                break;
            case KpiScoreCarding.STATUS_YELLOW:
                oRender.setBackground(statusAmarelo);
                break;
            case KpiScoreCarding.STATUS_GREEN:
                oRender.setBackground(statusVerde);
                break;
            case KpiScoreCarding.ESTAVEL_GRAY:
                oRender.setBackground(statusCinza);
                break;
            case KpiScoreCarding.STATUS_BLUE:
                oRender.setBackground(statusBlue);
                break;
        }

        oRender.setAlignment(2);
        oRender.setToolTipText(indValue.getVariacaoToolTip());
        oRender.setValue(indValue.getVariacao());

        return oRender;
    }

    private pv.jfcx.JPVEdit renderNumber(pv.jfcx.JPVEdit oRender, Double dbValor, int column, int row, javax.swing.JTable table) {
        oValor = java.text.NumberFormat.getInstance();
        int iDecimais = indBean.getCasasDecimais();

        oRender.setAlignment(2);
        oValor.setMinimumFractionDigits(iDecimais);
        oValor.setMaximumFractionDigits(iDecimais);
        oRender.setValue(oValor.format(dbValor));

        return oRender;
    }
}

/**
 * Render personalizado para cabe�alho de tabela. 
 * @author valdineyg
 */
class HeaderRenderer extends DefaultTableCellRenderer {

    public HeaderRenderer() {
        setHorizontalAlignment(SwingConstants.CENTER);
        setOpaque(true);
        setBorder(BorderFactory.createLineBorder(new Color(221, 221, 221)));
        setPreferredSize(new Dimension(this.getWidth(), 20));
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {
        JTableHeader h = table != null ? table.getTableHeader() : null;

        if (h != null) {
            setEnabled(h.isEnabled());
            setComponentOrientation(h.getComponentOrientation());
            setForeground(h.getForeground());
            setBackground(new Color(223, 229, 243));
            setFont(h.getFont());

        } else {
            setEnabled(true);
            setComponentOrientation(ComponentOrientation.UNKNOWN);
            setForeground(UIManager.getColor("TableHeader.foreground"));
            setBackground(UIManager.getColor("TableHeader.background"));
            setFont(UIManager.getFont("TableHeader.font"));
        }

        setValue(value);

        return this;
    }
}

//Model customizado
class KpiScoDefaultTableModel extends DefaultTableModel {

    KpiScoDefaultTableModel(Vector tb, Vector col) {
        super(tb, col);
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }
}
