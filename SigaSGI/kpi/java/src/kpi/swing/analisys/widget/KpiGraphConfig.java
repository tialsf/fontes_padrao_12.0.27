/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kpi.swing.analisys.widget;

/**
 *
 * @author lucio.pelinson
 */
public class KpiGraphConfig {

    private int redColor    = 0xff0000;
    private int yellowColor = 0xffff00;
    private int greenColor  = 0x2fe058;
    private int blueColor   = 0x6db2f7; 

    private int realColor = 0x000000;
    private int metaColor = 0x016268;
    private int previaColor = 0xcfcfcf; //0x60ABDB;
    private int variacaoColor = 0xa2bbc8;
    
    private int realAcumColor = 0x000000;
    private int metaAcumColor = 0xC9CD5B;
    private int previaAcumColor = 0x34B477;
    private int variacaoAcumColor = 0x535b5f;

    public KpiGraphConfig() {

    }

    /**
     * @return the realColor
     */
    public int getRealColor() {
        return realColor;
    }

    /**
     * @return the metaColor
     */
    public int getMetaColor() {
        return metaColor;
    }

    /**
     * @return the previaColor
     */
    public int getPreviaColor() {
        return previaColor;
    }

    /**
     * @return the variacaoColor
     */
    public int getVariacaoColor() {
        return variacaoColor;
    }

    /**
     * @return the redColor
     */
    public int getRedColor() {
        return redColor;
    }

    /**
     * @param redColor the redColor to set
     */
    public void setRedColor(int redColor) {
        this.redColor = redColor;
    }

    /**
     * @return the yellowColor
     */
    public int getYellowColor() {
        return yellowColor;
    }

    /**
     * @param yellowColor the yellowColor to set
     */
    public void setYellowColor(int yellowColor) {
        this.yellowColor = yellowColor;
    }

    /**
     * @return the greenColor
     */
    public int getGreenColor() {
        return greenColor;
    }

    /**
     * @param greenColor the greenColor to set
     */
    public void setGreenColor(int greenColor) {
        this.greenColor = greenColor;
    }

    /**
     * @return the realAcumColor
     */
    public int getRealAcumColor() {
        return realAcumColor;
    }

    /**
     * @param realAcumColor the realAcumColor to set
     */
    public void setRealAcumColor(int realAcumColor) {
        this.realAcumColor = realAcumColor;
    }

    /**
     * @return the metaAcumColor
     */
    public int getMetaAcumColor() {
        return metaAcumColor;
    }

    /**
     * @param metaAcumColor the metaAcumColor to set
     */
    public void setMetaAcumColor(int metaAcumColor) {
        this.metaAcumColor = metaAcumColor;
    }

    /**
     * @return the previaAcumColor
     */
    public int getPreviaAcumColor() {
        return previaAcumColor;
    }

    /**
     * @param previaAcumColor the previaAcumColor to set
     */
    public void setPreviaAcumColor(int previaAcumColor) {
        this.previaAcumColor = previaAcumColor;
    }

    /**
     * @return the variacaoAcumColor
     */
    public int getVariacaoAcumColor() {
        return variacaoAcumColor;
    }

    /**
     * @param variacaoAcumColor the variacaoAcumColor to set
     */
    public void setVariacaoAcumColor(int variacaoAcumColor) {
        this.variacaoAcumColor = variacaoAcumColor;
    }

    /**
     * @return the blueColor
     */
    public int getBlueColor() {
        return blueColor;
    }

    /**
     * @param blueColor the blueColor to set
     */
    public void setBlueColor(int blueColor) {
        this.blueColor = blueColor;
    }
}
