package kpi.swing.analisys.widget;

/**
 *
 * @author lucio.pelinson
 */
public class KpiIndicadorArea {
    private boolean percentValue = false;
    private KpiTypeGraph graphType;
    private KpiTypeAnalisys analisysType;

    /**
     * @return the percentValue
     */
    public boolean isPercentValue() {
        return percentValue;
    }

    /**
     * @param percentValue the percentValue to set
     */
    public void setPercentValue(boolean value) {
        percentValue = value;
    }

    /**
     * @return the graphType
     */
    public KpiTypeGraph getGraphType() {
        return graphType;
    }

    /**
     * @param graphType the graphType to set
     */
    public void setGraphType(KpiTypeGraph graphType) {
        this.graphType = graphType;
    }

    /**
     * @return the analisysType
     */
    public KpiTypeAnalisys getAnalisysType() {
        return analisysType;
    }

    /**
     * @param analisysType the analisysType to set
     */
    public void setAnalisysType(KpiTypeAnalisys analisysType) {
        this.analisysType = analisysType;
    }
}
