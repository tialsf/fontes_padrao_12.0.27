package kpi.swing.analisys.widget;

/**
 *
 * @author lucio.pelinson 
 */
public class KpiIndicadorBean {

    private String IDSco = "";
    private String IDInd = "";
    private String nomeSco = "";
    private String nomeInd = "";
    private String periodo = "";
    private String linkInd = "";
    private String unidadeMed = "";
    private int ordem;
    private int tendImgStatus;
    private int casasDecimais;
    private double tolerancia;
    private double supera;
    private boolean ascendente = true;
    private boolean melhorFaixa = true;
    private boolean estrategico;
    private boolean previaVisible;
    private boolean drill;
    private boolean analitico;
    private KpiIndicadorValue valueAcum = new KpiIndicadorValue();
    private KpiIndicadorValue valuePer = new KpiIndicadorValue();

    /**
     * @return the ordem
     */
    public int getOrdem() {
        return ordem;
    }

    /**
     * @param ordem the ordem to set
     */
    public void setOrdem(int ordem) {
        this.ordem = ordem;
    }

    /**
     * @return the nomeSco
     */
    public String getNomeSco() {
        return nomeSco;
    }

    /**
     * @param nomeSco the nomeSco to set
     */
    public void setNomeSco(String nomeSco) {
        this.nomeSco = nomeSco;
    }

    /**
     * @return the nomeInd
     */
    public String getNomeInd() {
        return nomeInd;
    }

    /**
     * @param nomeInd the nomeInd to set
     */
    public void setNomeInd(String nomeInd) {
        this.nomeInd = nomeInd;
    }

    /**
     * @return the status
     */
    public int getTendenciaImgStatus() {
        return tendImgStatus;
    }

    /**
     * @param status the status to set

     */
    public void setTendenciaImgStatus(int status) {
        this.tendImgStatus = status;
    }

    /**
     * @return the ascendente
     */
    public boolean isAscendente() {
        return ascendente;
    }

    /**
     * @param ascendente the ascendente to set
     */
    public void setAscendente(boolean ascendente) {
        this.ascendente = ascendente;
    }
    
    /**
     * @return the melhorFaixa
     */
    public boolean isMelhorFaixa() {
        return melhorFaixa;
    }

    /**
     * @param melhorFaixa the melhorFaixa to set
     */
    public void setMelhorFaixa(boolean melhorFaixa) {
        this.melhorFaixa = melhorFaixa;
    }

    /**
     * @return the estrategico
     */
    public boolean isEstrategico() {
        return estrategico;
    }

    /**
     * @param estrategico the estrategico to set
     */
    public void setEstrategico(boolean estrategico) {
        this.estrategico = estrategico;
    }

    /**
     * @return the casasDecimais
     */
    public int getCasasDecimais() {
        return casasDecimais;
    }

    /**
     * @param casasDecimais the casasDecimais to set
     */
    public void setCasasDecimais(int casasDecimais) {
        this.casasDecimais = casasDecimais;
    }

    /**
     * @return the unidadeMed
     */
    public String getUnidadeMed() {
        return unidadeMed;
    }

    /**
     * @param unidadeMed the unidadeMed to set
     */
    public void setUnidadeMed(String unidadeMed) {
        this.unidadeMed = unidadeMed;
    }

    /**
     * @return the tolerancia
     */
    public double getTolerancia() {
        return tolerancia;
    }

    /**
     * @param tolerancia the tolerancia to set
     */
    public void setTolerancia(double tolerancia) {
        this.tolerancia = tolerancia;
    }

    /**
     * @return the periodo
     */
    public String getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    /**
     * @return the valueAcum
     */
    public KpiIndicadorValue getValueAcum() {
        return valueAcum;
    }

    /**
     * @param valueAcum the valueAcum to set
     */
    public void setValueAcum(KpiIndicadorValue valueAcum) {
        this.valueAcum = valueAcum;
    }

    /**
     * @return the valuePer
     */
    public KpiIndicadorValue getValuePer() {
        return valuePer;
    }

    /**
     * @param valuePer the valuePer to set
     */
    public void setValuePer(KpiIndicadorValue valuePer) {
        this.valuePer = valuePer;
    }

    /**
     * @return the IDSco
     */
    public String getIDSco() {
        return IDSco;
    }

    /**
     * @param IDSco the IDSco to set
     */
    public void setIDSco(String IDSco) {
        this.IDSco = IDSco;
    }

    /**
     * @return the IDInd
     */
    public String getIDInd() {
        return IDInd;
    }

    /**
     * @param IDInd the IDInd to set
     */
    public void setIDInd(String IDInd) {
        this.IDInd = IDInd;
    }

    /**
     * @return the linkInd
     */
    public String getLinkInd() {
        return linkInd;
    }

    /**
     * @param linkInd the linkInd to set
     */
    public void setLinkInd(String linkInd) {
        this.linkInd = linkInd;
    }

    /**
     * @return the previa
     */
    public boolean isPreviaVisible() {
        return previaVisible;
    }

    /**
     * @param previa the previa to set
     */
    public void setPreviaVisible(boolean previa) {
        this.previaVisible = previa;
    }

    /**
     * @return the drill
     */
    public boolean isDrill() {
        return drill;
    }

    /**
     * @param drill the drill to set
     */
    public void setDrill(boolean drill) {
        this.drill = drill;
    }

    /**
     * @return the analitico
     */
    public boolean isAnalitico() {
        return analitico;
    }

    /**
     * @param drill the analitico to set
     */
    public void setAnalitico(boolean analitico) {
        this.analitico = analitico;
    }

    /**
     * @return the supera
     */
    public double getSupera() {
        return supera;
    }

    /**
     * @param supera the supera to set
     */
    public void setSupera(double supera) {
        this.supera = supera;
    }
}
