package kpi.swing.analisys.widget;

/**
 *
 * @author lucio.pelinson
 */
    public class KpiIndicadorValue {
        private String variacaoToolTip = "";
        private String variacao = "";
        private double real = 0;
        private double meta = 0;
        private double previa = 0;
        private int variacaoStatus = 0;

        /**
         * @return the variacaoToolTip
         */
        public String getVariacaoToolTip() {
            return variacaoToolTip;
        }

        /**
         * @param variacaoToolTip the variacaoToolTip to set
         */
        public void setVariacaoToolTip(String variacaoToolTip) {
            this.variacaoToolTip = variacaoToolTip;
        }

        /**
         * @return the real
         */
        public double getReal() {
            return real;
        }

        /**
         * @param real the real to set
         */
        public void setReal(double real) {
            this.real = real;
        }

        /**
         * @return the meta
         */
        public double getMeta() {
            return meta;
        }

        /**
         * @param meta the meta to set
         */
        public void setMeta(double meta) {
            this.meta = meta;
        }

        /**
         * @return the previa
         */
        public double getPrevia() {
            return previa;
        }

        /**
         * @param previa the previa to set
         */
        public void setPrevia(double previa) {
            this.previa = previa;
        }


        /**
         * @return the variacao
         */
        public String  getVariacao() {
            return variacao;
        }

        /**
         * @param variacao the variacao to set
         */
        public void setVariacao(String variacao) {
            this.variacao = variacao;
        }

        /**
         * @return the variacaoStatus
         */
        public int getVariacaoStatus() {
            return variacaoStatus;
        }

        /**
         * @param variacaoStatus the variacaoStatus to set
         */
        public void setVariacaoStatus(int variacaoStatus) {
            this.variacaoStatus = variacaoStatus;
        }

    }

