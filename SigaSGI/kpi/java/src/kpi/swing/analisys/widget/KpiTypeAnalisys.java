package kpi.swing.analisys.widget;

/**
 *
 * @author lucio.pelinson
 */
public enum KpiTypeAnalisys {
    BOTH(0),
    ACUM(1),
    PARC(2);
    private int code;

    KpiTypeAnalisys(int value) {
        this.code = value;
    }

    public int getCode() {
        return code;
    }

    public static KpiTypeAnalisys valueOf(int code) {
        KpiTypeAnalisys ret = null;
        switch (code) {
            case 0:
                ret = BOTH;
                break;
            case 1:
                ret = ACUM;
                break;
            case 2:
                ret = PARC;
                break;
        }

        return ret;
    }
}
