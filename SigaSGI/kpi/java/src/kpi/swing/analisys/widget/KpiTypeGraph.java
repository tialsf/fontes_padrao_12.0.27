package kpi.swing.analisys.widget;

/**
 * 
 * @author lucio.pelinson
 */
public enum KpiTypeGraph {

    BAR(0),
    ODOMETER_180(1),
    LINEAR(2),
    LIGHTS(3),
    TABLE(4);
    private int code;

    KpiTypeGraph(int value) {
        this.code = value;
    }

    public int getCode() {
        return code;
    }

    public static KpiTypeGraph valueOf(int code) {
        KpiTypeGraph ret = null;
        switch (code) {
            case 0:
                ret = BAR;
                break;
            case 1:
                ret = ODOMETER_180;
                break;
            case 2:
                ret = LINEAR;
                break;
            case 3:
                ret = LIGHTS;
                break;
            case 4:
                ret = TABLE;
                break;
        }

        return ret;
    }
}
