/*
 * KpiWidget.java
 *
 * Created on Apr 16, 2010, 9:04:10 AM
 */
package kpi.swing.analisys.widget;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.io.IOException;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import kpi.core.KpiDateBase;
import kpi.core.KpiStaticReferences;
import kpi.core.KpiImageResources;
import kpi.swing.KpiDefaultDialogSystem;
import kpi.swing.analisys.KpiCard;
import kpi.swing.analisys.KpiConsultaDW;
import kpi.swing.graph.KpiGraphIndicador;
import kpi.util.KpiDateUtil;
import kpi.util.KpiToolKit;
import kpi.xml.BIXMLRecord;

/**
 *
 * @author lucio.pelinson
 */
public class KpiWidget extends javax.swing.JInternalFrame implements KpiCard {

    private BIXMLRecord record = null;
    private boolean isToolBarVisible = false;
    private int order = 0;
    private KpiIndicadorBean indicador;
    private KpiImageResources imgResources;
    private kpi.core.KpiFormController formController = kpi.core.KpiStaticReferences.getKpiFormController();

    public KpiWidget(KpiIndicadorBean indBean, KpiIndicadorArea areaType1, KpiIndicadorArea areaType2) {
        this.putClientProperty("JInternalFrame.isPalette", Boolean.FALSE);
        initComponents();
        
        this.mnuSair.setVisible(false);
        
        //Remove a barra de t�tulo do formul�rio.
        BasicInternalFrameUI janelaUI = (BasicInternalFrameUI) this.getUI();
        janelaUI.setNorthPane(null);

        setIndBean(indBean);
        setSize(220, 250);

        lblIndicador.setText(indicador.getNomeInd());
        lblIndicador.setToolTipText(indicador.getNomeInd());
        setToolTipText(indicador.getNomeInd());
        setFrameIcon(getKpiImageIcon(indicador.getTendenciaImgStatus()));
        Dimension d = new Dimension(220, 100);
        KpiArea area1 = KpiArea.buildArea(indBean, areaType1, d);
        pnlArea1.add(area1, BorderLayout.CENTER);
        KpiArea area2 = KpiArea.buildArea(indBean, areaType2, d);
        pnlArea2.add(area2, BorderLayout.CENTER);

    }

    public ImageIcon getKpiImageIcon(int index) {

        if (imgResources != null && KpiStaticReferences.getKpiMainPanel() != null) {
            if (KpiStaticReferences.getKpiMainPanel().getImageResources() != null) {
                imgResources = KpiStaticReferences.getKpiMainPanel().getImageResources();
            }
        }
        if (imgResources == null) {
            imgResources = new KpiImageResources();
        }

        return imgResources.getImage(index);
    }
  public void setMnuSairVisible(boolean cond) {
         this.mnuSair.setVisible(cond);  
  }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mnuPopUp = new javax.swing.JPopupMenu();
        mnuVisualizar = new javax.swing.JMenuItem();
        mnuGrafico = new javax.swing.JMenuItem();
        mnuFormula = new javax.swing.JMenuItem();
        mnuDW = new javax.swing.JMenuItem();
        mnuLink = new javax.swing.JMenuItem();
        mnuSair = new javax.swing.JMenuItem();
        jScrollPane1 = new javax.swing.JScrollPane();
        pnlWidgetArea = new javax.swing.JPanel();
        pnlContainer = new javax.swing.JPanel();
        pnlArea1 = new javax.swing.JPanel();
        lblIndicador = new javax.swing.JLabel();
        pnlArea2 = new javax.swing.JPanel();

        mnuVisualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_visualizar.gif"))); // NOI18N
        mnuVisualizar.setToolTipText("");
        mnuVisualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuVisualizarActionPerformed(evt);
            }
        });
        mnuPopUp.add(mnuVisualizar);

        mnuGrafico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_grafico.gif"))); // NOI18N
        mnuGrafico.setToolTipText("");
        mnuGrafico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuGraficoActionPerformed(evt);
            }
        });
        mnuPopUp.add(mnuGrafico);

        mnuFormula.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_indicadorformula.gif"))); // NOI18N
        mnuFormula.setToolTipText("");
        mnuFormula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuFormulaActionPerformed(evt);
            }
        });
        mnuPopUp.add(mnuFormula);

        mnuDW.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_dw.gif"))); // NOI18N
        mnuDW.setToolTipText("");
        mnuDW.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuDWActionPerformed(evt);
            }
        });
        mnuPopUp.add(mnuDW);

        mnuLink.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_comp_mes_mes.gif"))); // NOI18N
        mnuLink.setToolTipText("");
        mnuLink.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuLinkActionPerformed(evt);
            }
        });
        mnuPopUp.add(mnuLink);

        mnuSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/close-normal-sel.gif"))); // NOI18N
        mnuSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuSairActionPerformed(evt);
            }
        });
        mnuPopUp.add(mnuSair);

        setBorder(null);
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jScrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        pnlWidgetArea.setBackground(new java.awt.Color(255, 255, 255));
        pnlWidgetArea.setLayout(new java.awt.BorderLayout());

        pnlContainer.setBackground(new java.awt.Color(255, 255, 255));
        pnlContainer.setOpaque(false);
        pnlContainer.setLayout(new java.awt.BorderLayout());

        pnlArea1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        pnlArea1.setOpaque(false);
        pnlArea1.setPreferredSize(new java.awt.Dimension(10, 100));
        pnlArea1.setLayout(new java.awt.BorderLayout());

        lblIndicador.setBackground(new java.awt.Color(255, 255, 255));
        lblIndicador.setFont(new java.awt.Font("Tahoma", 1, 11));
        lblIndicador.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblIndicador.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        lblIndicador.setOpaque(true);
        lblIndicador.setPreferredSize(new java.awt.Dimension(34, 15));
        lblIndicador.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblIndicadorMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblIndicadorMouseExited(evt);
            }
        });
        pnlArea1.add(lblIndicador, java.awt.BorderLayout.PAGE_START);

        pnlContainer.add(pnlArea1, java.awt.BorderLayout.CENTER);

        pnlArea2.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 0, 0, 0, new java.awt.Color(212, 208, 200)));
        pnlArea2.setOpaque(false);
        pnlArea2.setPreferredSize(new java.awt.Dimension(10, 100));
        pnlArea2.setLayout(new java.awt.BorderLayout());
        pnlContainer.add(pnlArea2, java.awt.BorderLayout.SOUTH);

        pnlWidgetArea.add(pnlContainer, java.awt.BorderLayout.CENTER);

        jScrollPane1.setViewportView(pnlWidgetArea);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mnuLinkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuLinkActionPerformed
        try{
            KpiToolKit oToolKit = new KpiToolKit();
            oToolKit.browser(new URL(getIndBean().getLinkInd()));
        } catch (IOException ex) {
            System.out.println(ex.getStackTrace());
        }
}//GEN-LAST:event_mnuLinkActionPerformed

    private void mnuDWActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuDWActionPerformed
        KpiDefaultDialogSystem dialogSystem = KpiStaticReferences.getKpiDialogSystem();

        KpiConsultaDW consultaDW = new KpiConsultaDW(this, true);
        if (consultaDW.loadDWConsultas(getIndBean().getIDInd())) {
            consultaDW.setVisible(true);
        } else {
            dialogSystem.errorMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00038"));
        }
}//GEN-LAST:event_mnuDWActionPerformed

    private void mnuFormulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuFormulaActionPerformed
        formController.getForm("INDICADOR_FORMULA", getIndBean().getIDInd(), getIndBean().getNomeInd());
}//GEN-LAST:event_mnuFormulaActionPerformed

    private void mnuGraficoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuGraficoActionPerformed
        KpiDateBase dateBase = KpiStaticReferences.getKpiDateBase();
        KpiDateUtil date_Util = new KpiDateUtil();

        KpiGraphIndicador frame = (KpiGraphIndicador) formController.getForm(
                "GRAPH_IND", getIndBean().getIDInd(), java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScoreCarding_00002")).asJInternalFrame();

        frame.loadGraphRecord(getIndBean().getIDInd(),
                date_Util.getCalendarString(dateBase.getDataAcumuladoDe()),
                date_Util.getCalendarString(dateBase.getDataAcumuladoAte()));
}//GEN-LAST:event_mnuGraficoActionPerformed

    private void mnuVisualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuVisualizarActionPerformed
        formController.getForm("INDICADOR", getIndBean().getIDInd(), getIndBean().getNomeInd()).asJInternalFrame();
}//GEN-LAST:event_mnuVisualizarActionPerformed

    private void lblIndicadorMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblIndicadorMouseEntered
        if (isToolBarVisible) {
            mnuPopUp.show((JComponent) evt.getSource(), 10, 15);
        }
        
        lblIndicador.setForeground(Color.blue);
    }//GEN-LAST:event_lblIndicadorMouseEntered

    private void lblIndicadorMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblIndicadorMouseExited
        lblIndicador.setForeground(Color.black);
    }//GEN-LAST:event_lblIndicadorMouseExited

private void mnuSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuSairActionPerformed
    if (this.isClosable())     
       this.dispose();
    else
       this.mnuSair.setEnabled(false);     
}//GEN-LAST:event_mnuSairActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblIndicador;
    private javax.swing.JMenuItem mnuDW;
    private javax.swing.JMenuItem mnuFormula;
    private javax.swing.JMenuItem mnuGrafico;
    private javax.swing.JMenuItem mnuLink;
    private javax.swing.JPopupMenu mnuPopUp;
    private javax.swing.JMenuItem mnuSair;
    private javax.swing.JMenuItem mnuVisualizar;
    private javax.swing.JPanel pnlArea1;
    private javax.swing.JPanel pnlArea2;
    private javax.swing.JPanel pnlContainer;
    protected javax.swing.JPanel pnlWidgetArea;
    // End of variables declaration//GEN-END:variables

    @Override
    public java.awt.Point getCardPosition() {
        Point p = new Point(0, 0);
        return p;
    }

    @Override
    public void setOrder(int o) {
        order = o;
    }

    @Override
    public int getOrder() {
        return order;
    }

    @Override
    public String getNome() {
        return indicador.getNomeInd();
    }

    @Override
    public String getId() {
        return indicador.getIDInd().toString();
    }

    @Override
    public BIXMLRecord getRecordData() {
        return this.record;
    }

    public void setRecordData(BIXMLRecord record) {
        this.record = record;
    }

    public KpiIndicadorBean getIndBean() {
        return indicador;
    }

    public void setIndBean(KpiIndicadorBean obj) {
        this.indicador = obj;
    }

    public void setToolBarVisible(boolean value) {
        this.isToolBarVisible = value;
    }
}
