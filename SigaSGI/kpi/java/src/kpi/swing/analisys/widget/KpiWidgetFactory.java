package kpi.swing.analisys.widget;

import kpi.swing.analisys.KpiScoreCarding;
import kpi.xml.BIXMLRecord;

/**
 *
 * @author lucio.pelinson
 */
public class KpiWidgetFactory {

    public static final String AREA1_GRAPH = "010001";
    public static final String AREA2_GRAPH = "040000";

    public static KpiWidget createWidget(BIXMLRecord rec) {
        KpiIndicadorBean indBean = new KpiIndicadorBean();

        //Area1
        String sGraphUp = rec.getString("GRAPH_UP");
        if (sGraphUp.length() < 6) {
            sGraphUp = AREA1_GRAPH;
        }
        KpiIndicadorArea areaType1 = buildIndArea(sGraphUp);

        //Area2
        String sGraphDown = rec.getString("GRAPH_DO");
        if (sGraphDown.length() < 6) {
            sGraphDown = AREA2_GRAPH;
        }
        KpiIndicadorArea areaType2 = buildIndArea(sGraphDown);

        //Dados gerais do indicador
        indBean.setIDInd(rec.getString("ID"));
        indBean.setNomeInd(rec.getString("INDICADOR"));
        indBean.setPeriodo(rec.getString("FREQNOME"));
        indBean.setLinkInd(rec.getString("LINK"));
        indBean.setOrdem(rec.getInt("ORDEM"));
        indBean.setTendenciaImgStatus(rec.getInt("TENDENCIA"));
        indBean.setCasasDecimais(rec.getInt("DECIMAIS"));
        indBean.setUnidadeMed(rec.getString("UNIDADE"));
        indBean.setEstrategico(rec.getString("IND_ESTRAT").equals("T"));
        indBean.setAscendente(rec.getBoolean("ASCEND"));
        // atribui a propriedade Melhor na Faixa do indicador
        indBean.setMelhorFaixa(rec.getBoolean("MELHORF"));
        indBean.setTolerancia(rec.getDouble("TOLERAN"));
        indBean.setSupera(rec.getDouble("SUPERA"));
        indBean.setAnalitico(rec.getBoolean("ANALITICO"));
        if (rec.contains("DRILL")) {
            indBean.setDrill(rec.getBoolean("DRILL"));
        }

        //Valores do Periodo atual
        indBean.getValuePer().setVariacaoToolTip("");
        indBean.getValuePer().setReal(rec.getDouble("VALOR"));
        indBean.getValuePer().setMeta(rec.getDouble("META"));
        indBean.getValuePer().setVariacao(rec.getString("VARIACAO"));
        indBean.getValuePer().setVariacaoToolTip(rec.getString("VAR_HINT"));
        indBean.getValuePer().setVariacaoStatus(rec.getInt("STATUS"));
        if (rec.contains("PREVIA")) {
            indBean.getValuePer().setPrevia(rec.getDouble("PREVIA"));
            indBean.setPreviaVisible(true);
        }

        //Valores do Periodo acumulado
        indBean.getValueAcum().setVariacaoToolTip("");
        indBean.getValueAcum().setReal(rec.getDouble("ACUMULADO_PERIDO"));
        indBean.getValueAcum().setMeta(rec.getDouble("ACUMULADO_META"));
        indBean.getValueAcum().setVariacao(rec.getString("VARIA_ACUMULADA"));
        indBean.getValueAcum().setVariacaoToolTip(rec.getString("VARIA_ACUMUHINT"));
        indBean.getValueAcum().setVariacaoStatus(rec.getInt("STATUS_ACU"));
        if (rec.contains("ACUMULADO_PREVIA")) {
            indBean.getValueAcum().setPrevia(rec.getDouble("ACUMULADO_PREVIA"));
        }

        KpiWidget widget = new KpiWidget(indBean, areaType1, areaType2);
        widget.setRecordData(rec);

        return widget;
    }

    /*
     * Constroi objeto a apartir de uma string
     * @param String seis posições GGVVPP:
     *        XX GRAFICO;
     *        YY VISUALIZAÇÃO;
     *        PP PERCENTUAL OU VALOR
     * @return KpiIndicadorArea
     */
    public static KpiIndicadorArea buildIndArea(String value) {
        KpiIndicadorArea areaGraph = new KpiIndicadorArea();
        int nTtypeGraph = 0;
        int nTtypeAnalisys = 0;
        boolean isPercent = true;

        if (value.length() > 5) {
            nTtypeGraph = Integer.parseInt(value.substring(0, 2));
            nTtypeAnalisys = Integer.parseInt(value.substring(2, 4));
            isPercent = value.substring(4, 6).equals("01");
        }

        areaGraph.setGraphType(KpiTypeGraph.valueOf(nTtypeGraph));
        areaGraph.setAnalisysType(KpiTypeAnalisys.valueOf(nTtypeAnalisys));
        areaGraph.setPercentValue(isPercent);

        return areaGraph;
    }

}