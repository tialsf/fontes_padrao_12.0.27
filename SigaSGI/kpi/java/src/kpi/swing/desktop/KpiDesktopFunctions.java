/*
 * KpiDesktopFunctions.java
 *
 * Created on 13 de Outubro de 2004, 10:23
 */
package kpi.swing.desktop;

/**
 *
 * @author  alexandre silva
 */
public class KpiDesktopFunctions {
	//Tipo da vis�o escolhida Pasta ou Janela.
	public kpi.xml.BIXMLRecord recDesktop;
	
	private String type = new String("USU_CONFIG");
	private kpi.swing.KpiDefaultDialogSystem dialogSystem;
	private kpi.core.KpiDataController dataController;
	
	/* Creates a new instance of KpiDesktopFunctions */
	public KpiDesktopFunctions() {
		dataController =  kpi.core.KpiStaticReferences.getKpiDataController();
		dialogSystem = new kpi.swing.KpiDefaultDialogSystem(kpi.core.KpiStaticReferences.getKpiApplet());
		load();
	}
	
	//Carrega as propriedades da desktop;
	public void load(){
		kpi.xml.BIXMLRecord recordAux = dataController.loadRecord("1", type);
		if ( dataController.getStatus() == kpi.core.KpiDataController.KPI_ST_OK ) {
			recDesktop = recordAux;
		} else {
			if ( dataController.getStatus() == kpi.core.KpiDataController.KPI_ST_GENERALERROR )
				dialogSystem.errorMessage(dataController.getErrorMessage() );
			else
				dialogSystem.errorMessage(dataController.getStatus() );
			
			if ( dataController.getStatus() == kpi.core.KpiDataController.KPI_ST_EXPIREDSESSION) {
				kpi.core.KpiStaticReferences.getKpiApplet().resetApplet();
			}
		}
	}
}