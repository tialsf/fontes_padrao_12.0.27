/*
 * KpiEspinhaPeixe.java
 *
 * Created on March 15, 2007, 3:30 PM
 * SIGA2516
 * 
 */

package kpi.swing.espinhapeixe;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;

/**
 *
 * @author lucio.pelinson
 */
public class KpiEspinhaPeixe {
	private final int DIST_SETA	= 8;	//Distancia entre setas
	
	private double distCaixa_X	= 140;	//Tamanho da caixa eixo x
	private double distCaixa_Y	= 70;   //Tamanho da caixa eixo y
	private double tam_seta		= 30;	//Tamanho da seta
	private double nZoom		= 1;	//Fator de zoom aplicaddo
	
	private double nMaiorNeto;
	private boolean isRoot;
	private int tipoLinha;									
	private double comprimento;									
	private int index;											//Posi��o no array
	private KpiEspinhaPeixeNode userObject;						//Dados
	private KpiEspinhaPeixeBox kpiBox;							//Caixa
	private kpi.util.KpiArrow2D kpiLinha;						//Linha
	private KpiEspinhaPeixe owner;								//Pai
	private java.util.Vector child	= new java.util.Vector();	//Filhos
	

	/** Creates a new instance of KpiEspinha */
	public KpiEspinhaPeixe() {
	}
	
	/**
	 * Cria uma nova instancia do objeto Espinha de Peixe
	 * @param oUser Dados
	 */
	public KpiEspinhaPeixe(KpiEspinhaPeixeNode oUser) {
		isRoot = true;
		tipoLinha = 2;
		comprimento = tam_seta;
		nMaiorNeto = 0;
		userObject = oUser;
	}
	
	/**
	 * Adiciona um n� filho retornando uma refer�ncia do objeto adicionado.
	 * @param oUser Dados
	 * @return Referencia ao objeto criado
	 */
	public KpiEspinhaPeixe addChild(KpiEspinhaPeixeNode oUser){
		KpiEspinhaPeixe newObj = new KpiEspinhaPeixe();
		newObj.tam_seta = tam_seta;
		newObj.distCaixa_X = distCaixa_X;
		newObj.distCaixa_Y = distCaixa_Y;
		newObj.nZoom = nZoom;
		newObj.comprimento = tam_seta;
		newObj.userObject = oUser;
		newObj.owner = this;
		newObj.nMaiorNeto = 0;
		
		this.child.add(newObj);
		newObj.tipoLinha = findTipoLinha();
		newObj.index = this.child.size();
		refreshComprimento();
		
		return newObj;
	}
	 
	/**
	 * Retorna o tipo da linha de acordo com o pai.
	 * <br><br>
	 * 1 = (NO) Noroeste	<br>
	 * 2 = (O)  Oeste		<br> 
	 * 3 = (SO) Sudoeste	<br>
	 * 4 = (SE) Sudeste		<br>
	 * 5 = (L)  Leste		<br>
	 * 6 = (NE) Nordeste	<br>
	 * @return Tipo da linha
	 */
	private int findTipoLinha(){
		int nRet = 0;
		
		//Verifica se eh lado par ou impar
		if((this.child.size()%2)==0){
			switch(this.tipoLinha){
				case 1:
					nRet = 5;
					break;
				case 2:
					nRet = 1;
					break;
				case 3:
					nRet = 5;
					break;
				case 4:
					nRet = 5;
					break;
				case 5:
					nRet = 6;
					break;
				case 6:
					nRet = 5;
					break;
			}
		}else{
			switch(this.tipoLinha){
				case 1:
					nRet = 2;
					break;
				case 2:
					nRet = 3;
					break;
				case 3:
					nRet = 2;
					break;
				case 4:
					nRet = 2;
					break;
				case 5:
					nRet = 4;
					break;
				case 6:
					nRet = 2;
					break;
			}
		}
		return nRet;
	}
	
	/**
	 * Retorna o angulo de acordo com o tipo da linha.
	 * <br>
	 * Tipo\Retorno	<br>
	 * 1 = (NO) Noroeste	\110�	<br>
	 * 2 = (O)  Oeste		\180�	<br>
	 * 3 = (SO) Sudoeste	\250�	<br>
	 * 4 = (SE) Sudeste		\290�	<br>
	 * 5 = (L)  Leste		\0�		<br>
	 * 6 = (NE) Nordeste	\70�	<br>
	 * @param nTipo Tipo da linha
	 * @return Angulo
	 */
	private int getAnguloByTipo(int nTipo){
		int ang = 0;
		switch(nTipo){
			case 1:
				ang = 110;
				break;
			case 2:
				ang = 180;
				break;
			case 3:
				ang = 250;
				break;
			case 4:
				ang = 290;
				break;
			case 5:
				ang = 0;
				break;
			case 6:
				ang = 70;
				break;
		}
		
		return ang;
	}
	
	/**
	 * Fun��o recursiva para redimencionar o tamanho das setas pais.
	 */
	private void refreshComprimento(){
		double maiorSeta = 0;
		double dist;
		
		if(!child.isEmpty()){
			for(int i=0;i<child.size();i++){
				KpiEspinhaPeixe filho = (KpiEspinhaPeixe)child.get(i);
				if(!filho.child.isEmpty()){
					for(int j=0;j<filho.child.size();j++){
						KpiEspinhaPeixe neto = (KpiEspinhaPeixe)filho.child.get(j);
						if(neto.tipoLinha == 2 || neto.tipoLinha == 5){
							dist = distCaixa_X;
						}else{
							dist = distCaixa_Y;
						}
						if(neto.comprimento + dist > maiorSeta){
							maiorSeta = neto.comprimento + dist;
						}
					}
				}
			}
		}
		
		
		if(maiorSeta > 0){
			this.comprimento = (maiorSeta*2) * ( (child.size()+1)/2 );
		}else{
			if(tipoLinha == 2 || tipoLinha == 5){
				this.comprimento = distCaixa_X * ( (child.size()+1)/2 );
			}else{
				if(child.size() == 1){
					this.comprimento = distCaixa_Y + tam_seta;
				}else{
					this.comprimento = distCaixa_Y * ( (child.size()+1)/2 );
				}
			}
		}
		
		if(!isRoot){
			owner.refreshComprimento();
		}
	}
	
	/**
	 * Verifica o tamnho da maior seta filha.
	 * @return Tamanho
	 */
	public double getMaiorFilho(){
		double maiorSeta = 0;
		
		if(!child.isEmpty()){
			for(int i=0;i<child.size();i++){
				KpiEspinhaPeixe filho = (KpiEspinhaPeixe)child.get(i);
				if(filho.comprimento > maiorSeta){
					maiorSeta = filho.comprimento;
				}
			}
		}
		
		return maiorSeta;
	}
	
	
	/**
	 * Verifica o maior neto e retorna seu comprimento. O retorno 
	 * � utilizado para posicionar o ponto final de uma reta.
	 * <br><br>
	 * Agrade�o ao Alexandre pela ajuda na cria��o desta fun��o.
	 * @return Retorna o comprimento do maior neto.
	 */
	public double getDistancia(){
		double nDistancia	= 0;
		double maiorSeta	= 0;
		double distCaixa	= 0;
		
		//Pega o maior neto
		if(owner.nMaiorNeto > 0){
			maiorSeta = owner.nMaiorNeto;
		}else{
			if(!owner.child.isEmpty()){
				for(int i=0;i<owner.child.size();i++){
					KpiEspinhaPeixe filho = (KpiEspinhaPeixe)owner.child.get(i);
					if(!filho.child.isEmpty()){
						for(int j=0;j<filho.child.size();j++){
							KpiEspinhaPeixe neto = (KpiEspinhaPeixe)filho.child.get(j);
							if(neto.tipoLinha == 2 || neto.tipoLinha == 5){
								distCaixa = distCaixa_X;
							}else{
								distCaixa = distCaixa_Y;
							}
							if((neto.comprimento + distCaixa) > maiorSeta){
								maiorSeta = neto.comprimento + distCaixa;
							}
						}
					}
				}
			}
			owner.nMaiorNeto = maiorSeta;
		}
		
		
		if(maiorSeta == 0){
			if(tipoLinha == 2 || tipoLinha == 5){
				maiorSeta = (distCaixa_Y/2);
			}else{
				maiorSeta = distCaixa_Y;
			}
			
		}
		
		//Calcula a distancia
		if(index%2==0){
			nDistancia = maiorSeta * (index-1) +  DIST_SETA;
		}else{
			nDistancia = maiorSeta * index;
		}
		
		return nDistancia;
	}
	
	
	
	/**
	 * Cria instancia de todos os objetos
	 */
	public void buildObject(){
		int anguloPai;
		int angulo;
		Point2D p1;
		Point2D p2;
		
		if(isRoot){
			double distancia = getMaiorFilho();
			if(distancia == 0){
				distancia = tam_seta + 80;
			}else{
				distancia += 80;
			}
			
			p1 = new Point2D.Double(100, distancia);
			p2 = new Point2D.Double(100 + comprimento + distCaixa_X, distancia);
			kpiBox = new KpiEspinhaPeixeBox(nZoom, p2, userObject);
			setKpiLinha(new kpi.util.KpiArrow2D(nZoom, p1, p2));
			
		}else{
			angulo	  = getAnguloByTipo(tipoLinha);
			anguloPai = getAnguloByTipo(owner.tipoLinha);
			p2 = findPoint(owner.getKpiLinha().getLine().getP2(), getDistancia(), anguloPai);
			p1 = findPoint(p2, comprimento, angulo);
			setKpiLinha(new kpi.util.KpiArrow2D(nZoom, p1, p2));
			kpiBox = new KpiEspinhaPeixeBox(nZoom, p1, userObject, tipoLinha, !child.isEmpty());
		}
		
		//Constroi os filhos
		if(!child.isEmpty()){
			for(int i=0;i<child.size();i++){
				KpiEspinhaPeixe filho = (KpiEspinhaPeixe)child.get(i);
				filho.buildObject();
			}
		}
		
	}
	
	/**
	 * Adiciona os objetos KpiBox ao apinel
	 * @param pnl Painel
	 */
	public void addInPainel(javax.swing.JPanel pnl){
		if(kpiBox != null){
			pnl.add(kpiBox);
		}
		
		//Adiciona os objetos caixa no painel
		if(!child.isEmpty()){
			for(int i=0;i<child.size();i++){
				KpiEspinhaPeixe filho = (KpiEspinhaPeixe)child.get(i);
				filho.addInPainel(pnl);
			}
		}
	}
	
	
	/**
	 * Localiza o ponto final de uma reta a partir
	 * de um �ngulo.<br><br>Obs.:<br>
	 * Agrade�o ao imenso apoio do Marcello na cria��o desta fun��o.
	 * @param pIni Ponto inicial
	 * @param comprimento Comprimento da reta
	 * @param angulo �ngulo da reta
	 * @return Ponto final da reta
	 */
	private Point2D findPoint(Point2D pIni, double comprimento, int angulo) {
		double	nX	= 0,
			nY	= 0,
			a	= 0,
			b	= 0;
		
		a = comprimento * Math.sin(Math.toRadians(angulo));
		b = comprimento * Math.cos(Math.toRadians(angulo));
		nX = pIni.getX() + b;
		nY = pIni.getY() - a;
		Point2D p = new Point2D.Double(nX, nY);
		return p;
	}
	
	
	/**
	 * Pinta os objetos no painel
	 * @param g2 Referencia do objeto Graphics2D
	 */
	public void render(Graphics2D g2) {
		kpi.util.KpiArrow2D linha = getKpiLinha();
		if(linha != null){
			linha.render(g2);
		}
		
		//Render dos filhos
		if(!child.isEmpty()){
			for(int i=0;i<child.size();i++){
				KpiEspinhaPeixe filho = (KpiEspinhaPeixe)child.get(i);
				filho.render(g2);
			}
		}
		
	}
	
	/**
	 * retorna a largura do objeto.
	 * @return Largura
	 */
	public double getWidth(){
		return 240 + comprimento + distCaixa_X;
	}
	
	/**
	 * Retorna a altura do objeto.
	 * @return Altura
	 */
	public double getHeight(){
		double distancia = getMaiorFilho();
		
		if(distancia == 0){
			distancia = tam_seta + 80;
		}else{
			distancia += 80;
		}
		
		return (distancia*2) + 60;
	}
	
	/**
	 * Retorna o objeto do tipo linha.
	 * @return Linha
	 */
	public kpi.util.KpiArrow2D getKpiLinha() {
		return kpiLinha;
	}
	
	/**
	 * Atribui um objeto do tipo linha
	 * @param kpiLinha Linha
	 */
	public void setKpiLinha(kpi.util.KpiArrow2D kpiLinha) {
		this.kpiLinha = kpiLinha;
	}

	public double getZoom() {
		return nZoom * 100;
	}

	public void setZoom(double percentual) {
		nZoom = (percentual/100);
		distCaixa_X	= 140 * nZoom;
		distCaixa_Y	= 70  * nZoom;
		tam_seta	= 30  * nZoom;
	}
	
	
}
