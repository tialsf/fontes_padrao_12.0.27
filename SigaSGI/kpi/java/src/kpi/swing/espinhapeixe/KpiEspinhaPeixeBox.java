/*
 * KpiEspinhaPeixeBox.java
 *
 * Created on March 13, 2007, 2:46 PM
 */
package kpi.swing.espinhapeixe;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

/**
 *
 * @author  lucio.pelinson
 */
public class KpiEspinhaPeixeBox extends javax.swing.JPanel {

    private static final int LARGURA_BORDA = 3;
    private kpi.core.KpiFormController formController = kpi.core.KpiStaticReferences.getKpiFormController();
    private KpiEspinhaPeixeNode oUser;
    private Color color;

    public KpiEspinhaPeixeBox() {
        initComponents();
    }

    public KpiEspinhaPeixeBox(double nZoom, java.awt.geom.Point2D pt, KpiEspinhaPeixeNode userObject) {
        initComponents();

        double nWidth = 120 * nZoom,
                nHeight = 80 * nZoom,
                nX = pt.getX(),
                nY = pt.getY() - (nHeight / 2);

        int tamFonte = 11;
        if (nZoom >= 2.0) {
            tamFonte = 16;
        } else if (nZoom >= 1.5) {
            tamFonte = 13;
        } else if (nZoom >= 1.0) {
            tamFonte = 11;
        } else if (nZoom >= 0.7) {
            tamFonte = 10;
        } else if (nZoom >= 0.6) {
            tamFonte = 10;
        } else if (nZoom >= 0.4) {
            tamFonte = 8;
        } else if (nZoom <= 0.3) {
            tamFonte = 6;
        }

        oUser = userObject;
        color = new Color(255, 255, 153);
        this.setBounds((int) nX, (int) nY, (int) nWidth, (int) nHeight);
        this.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), LARGURA_BORDA));
        this.jTextPane.setText(userObject.toString());
        this.jTextPane.setBackground(color);
        this.jTextPane.setDisabledTextColor(new Color(0, 0, 0));
        this.jTextPane.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, tamFonte));
        this.jTextPane.setToolTipText(buildToolTip());
    }

    public KpiEspinhaPeixeBox(double nZoom, java.awt.geom.Point2D pt, KpiEspinhaPeixeNode userObject, int tipoLinha, boolean lHasChild) {
        initComponents();
        double nWidth = 120 * nZoom,
                nHeight = 50 * nZoom,
                nX = 0,
                nY = 0;

        int tamFonte = 11;
        if (nZoom >= 2.0) {
            tamFonte = 16;
        } else if (nZoom >= 1.5) {
            tamFonte = 13;
        } else if (nZoom >= 1.0) {
            tamFonte = 11;
        } else if (nZoom >= 0.7) {
            tamFonte = 10;
        } else if (nZoom >= 0.6) {
            tamFonte = 9;
        } else if (nZoom >= 0.4) {
            tamFonte = 8;
        } else if (nZoom <= 0.3) {
            tamFonte = 6;
        }

        oUser = userObject;

        //Seta as propriedades da caixa conforme o tipo do objeto (ROOT, EFEITO, CAUSA)
        if (userObject.getTipo() == KpiEspinhaPeixeCadastro.KPI_EFEITO) {
            color = new Color(255, 255, 51);
            this.jTextPane.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, tamFonte));
            this.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), LARGURA_BORDA));
            nWidth = 120 * nZoom;
            nHeight = 50 * nZoom;
        } else {
            if (lHasChild) {
                if (oUser.hasPlanAcao) {
                    //Verde
                    this.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 153), LARGURA_BORDA));
                } else {
                    //Vermelho
                    this.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 195), LARGURA_BORDA));
                }
                this.jTextPane.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, tamFonte));
                nWidth = 114 * nZoom;
                nHeight = 40 * nZoom;
            } else {
                if (oUser.hasPlanAcao) {
                    //Verde
                    this.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 153), LARGURA_BORDA));
                } else {
                    //Vermelho
                    this.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 195), LARGURA_BORDA));
                }
                this.jTextPane.setFont(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, tamFonte));
                nWidth = 110 * nZoom;
                nHeight = 36 * nZoom;
            }
            color = new Color(255, 255, 255);
        }


        //Verifica onde desenhar a caixa
        switch (tipoLinha) {
            case 1:
                nX = (int) pt.getX() - (nWidth / 2);
                nY = (int) pt.getY() - nHeight;
                break;
            case 2:
                nX = (int) pt.getX() - nWidth;
                nY = (int) pt.getY() - (nHeight / 2);
                break;
            case 3:
                nX = (int) pt.getX() - (nWidth / 2);
                nY = (int) pt.getY();
                break;
            case 4:
                nX = (int) pt.getX() - (nWidth / 2);
                nY = (int) pt.getY();
                break;
            case 5:
                nX = (int) pt.getX();
                nY = (int) pt.getY() - (nHeight / 2);
                break;
            case 6:
                nX = (int) pt.getX() - (nWidth / 2);
                nY = (int) pt.getY() - nHeight;
                break;
        }


        this.setBounds((int) nX, (int) nY, (int) nWidth, (int) nHeight);
        this.jTextPane.setBackground(color);
        this.jTextPane.setDisabledTextColor(new Color(0, 0, 0));
        this.jTextPane.setText(userObject.toString());
        this.jTextPane.setToolTipText(buildToolTip());

    }

    private String buildToolTip() {
        StringBuilder s = new StringBuilder();


        s.append("<html>");
        s.append("<table width='215px' border=0>");
        s.append("  <tr>");
        s.append("    <td>");
        s.append("      <b>".concat(oUser.getName()).concat("</b><br>"));
        s.append("    </td>");
        s.append("  </tr>");

        s.append("  <tr>");
        s.append("    <td>");
        s.append("      <hr>");
        s.append("    </td>");
        s.append("  </tr>  ");
        s.append("  <tr>");
        s.append("    <td>");
        //Descri��o:
        s.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeRel_00010"));
        s.append("    </td>");
        s.append("  </tr>");
        s.append("  <tr>");
        s.append("    <td width='150px'>");
        s.append(oUser.getDescricao().concat("&nbsp;"));
        s.append("    </td>");
        s.append("  </tr>");

        if (oUser.hasPlanAcao == false && oUser.getTipo() == KpiEspinhaPeixeCadastro.KPI_CAUSA) {
            s.append("  <tr>");
            s.append("    <td>");
            s.append("      <hr>");
            s.append("    </td>");
            s.append("  </tr>  ");
            s.append("  <tr>");
            s.append("    <td>");
            s.append("      <FONT color=#ff4500 size=2>");
            //N�o exite plano de a��o cadastrado para essa causa.
            s.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeRel_00013"));
            s.append("      </FONT>");
            s.append("    </td>");
            s.append("  </tr>");
        }
        s.append("</table>");
        s.append("</html>");

        return s.toString();
    }

    public javax.swing.JTextPane getJTextPane() {
        return jTextPane;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    private void initComponents() {//GEN-BEGIN:initComponents

        jScrollPane1 = new JScrollPane();
        jTextPane = new JTextPane();

        setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0)));
        setMinimumSize(new Dimension(40, 40));
        setLayout(new BorderLayout());

        jScrollPane1.setBorder(null);

        jTextPane.setEditable(false);
        jTextPane.setFont(new Font("Tahoma", 1, 11));
        jTextPane.setEnabled(false);
        jTextPane.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                jTextPaneMouseClicked(evt);
            }
            public void mouseEntered(MouseEvent evt) {
                jTextPaneMouseEntered(evt);
            }
            public void mouseExited(MouseEvent evt) {
                jTextPaneMouseExited(evt);
            }
        });
        jScrollPane1.setViewportView(jTextPane);

        add(jScrollPane1, BorderLayout.CENTER);
    }//GEN-END:initComponents

	private void jTextPaneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextPaneMouseClicked
            if (oUser.getTipo() == KpiEspinhaPeixeCadastro.KPI_CAUSA) {
                StringBuilder strComando = new StringBuilder("ID_EFEITO ='");
                strComando.append(oUser.getRootId().concat("' AND ID_CAUSA ='"));
                strComando.append(oUser.getID().concat("'"));
                kpi.swing.framework.KpiListaGrupoAcao form = (kpi.swing.framework.KpiListaGrupoAcao) formController.newForm("LSTGRUPO_ACAO", "2", strComando.toString());
            }
	}//GEN-LAST:event_jTextPaneMouseClicked

	private void jTextPaneMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextPaneMouseExited
            this.jTextPane.setBackground(color);
            this.jTextPane.setDisabledTextColor(new Color(0, 0, 0));
            setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	}//GEN-LAST:event_jTextPaneMouseExited

	private void jTextPaneMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextPaneMouseEntered
            if (oUser.getTipo() == KpiEspinhaPeixeCadastro.KPI_CAUSA) {
                this.jTextPane.setBackground(new Color(51, 0, 153));
                this.jTextPane.setDisabledTextColor(new Color(255, 255, 255));
                setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
            }
	}//GEN-LAST:event_jTextPaneMouseEntered
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JScrollPane jScrollPane1;
    private JTextPane jTextPane;
    // End of variables declaration//GEN-END:variables
}
