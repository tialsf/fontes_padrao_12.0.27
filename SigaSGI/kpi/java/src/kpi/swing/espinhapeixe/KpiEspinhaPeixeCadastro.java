package kpi.swing.espinhapeixe;

import java.awt.Color;
import java.awt.event.ItemEvent;
import kpi.core.KpiImageResources;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JTree;
import javax.swing.tree.*;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiFileChooser;
import kpi.util.KpiToolKit;
import kpi.xml.*;

public class KpiEspinhaPeixeCadastro extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "ESP_PEIXE";
    private String formType = recordType;
    private String parentType = "LST_ESP_PEIXE";
    public static final int KPI_ROOT = 0;
    public static final int KPI_EFEITO = 1;
    public static final int KPI_CAUSA = 2;
    private kpi.core.KpiFormController formController = kpi.core.KpiStaticReferences.getKpiFormController();
    private kpi.swing.KpiDefaultDialogSystem dialog = kpi.core.KpiStaticReferences.getKpiDialogSystem();
    private java.util.Hashtable htbScoreCard = new java.util.Hashtable();
    private java.util.Hashtable htbIndicadores = new java.util.Hashtable();
    private kpi.xml.BIXMLRecord registrosDeletados = null;
    private TreeNode _treeDraggingNode = null;
    private boolean _treeDragging = false;
    private boolean isEdit = false;
    private int idNode = 0;
    private int escala = 100;
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiEspinhaPeixeCadastro(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
    }

    
    @Override
    public void enableFields() {
        isEdit = true;
        event.setEnableFields(tskDados, true);
        enableByNode();
    }

    @Override
    public void disableFields() {
        isEdit = false;
        event.setEnableFields(tskDados, false);
    }

    @Override
    public void refreshFields() {
        registrosDeletados = null;

        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));

        BIXMLVector vctScoreCard = event.listRecords("SCORECARD", "LISTA_SCO_OWNER").getBIXMLVector("SCORECARDS");
        event.populateCombo(vctScoreCard, "SCORECARD", htbScoreCard, cbbScoreCard);
        event.selectComboItem(cbbScoreCard, event.getComboItem(vctScoreCard, record.getString("ID_SCO"), true));
        tsArvore.setVetor(vctScoreCard);

        BIXMLVector vctIndicadores = event.listRecords("INDICADOR", "ID_SCOREC = '".concat(record.getString("ID_SCO").concat("'"))).getBIXMLVector("INDICADORES");
        event.populateCombo(vctIndicadores, "INDICADOR", htbIndicadores, cbbParentIndicador);
        event.selectComboItem(cbbParentIndicador, event.getComboItem(vctIndicadores, record.getString("ID_IND"), true));

        kpiTreeElemento.removeAll();

        KpiImageResources kpiImg = kpi.core.KpiStaticReferences.getKpiImageResources();
        KpiEspPeixeTreeCellRenderer oRender = new KpiEspPeixeTreeCellRenderer(kpiImg);
        kpiTreeElemento.setCellRenderer(oRender);
        kpiTreeElemento.setCellEditor(new KpiEspPeixeTreeCellEditor(kpiTreeElemento, oRender, kpiImg));
        kpiTreeElemento.setModel(new javax.swing.tree.DefaultTreeModel(createTree(record)));

        //Seleciona apenas Objetivo
        if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
	    tsArvore.setEntitySelection(KpiStaticReferences.CAD_OBJETIVO);
	}
        
        if (!(status == KpiDefaultFrameBehavior.INSERTING)) {
            tskDados.setExpanded(false);
            pnlDiagrama.setPreferredSize(new Dimension(0, getContentPane().getHeight() - 130));
        } else {
            tskDados.setExpanded(true);
            pnlDiagrama.setPreferredSize(new Dimension(50, this.getHeight() + 70));
        }

        atualizaDiagrama();
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);

        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }

        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("ID_SCO", event.getComboValue(htbScoreCard, cbbScoreCard));
        recordAux.set("ID_IND", event.getComboValue(htbIndicadores, cbbParentIndicador));

        BIXMLVector vctArvore = new BIXMLVector("ARVORE", "ARVORES");
        vctArvore.add(getXMLTree());
        recordAux.set(vctArvore);

        if (registrosDeletados != null) {
            vctArvore.add(registrosDeletados);
        }

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        if (fldNome.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScorecard_00006"));
            retorno = false;
        } else if (fldNome.getText().indexOf(">") != -1) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00054"));
            retorno = false;
        } else if (cbbScoreCard.getSelectedIndex() <= 0) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00004"));
            retorno = false;
        } else if (cbbParentIndicador.getSelectedIndex() <= 0) {
            errorMessage.append("Indicador n�o informado!");
            retorno = false;
        }

        if (!retorno) {
            btnSave.setEnabled(true);
        }

        return retorno;
    }

    /**
     * Atualiza o desenho do diagrama de acordo com o conte�do da �rvore.
     */
    private void atualizaDiagrama() {
        pnlEspinhaPeixe.buildEspPeixe(this.kpiTreeElemento, escala);
        pnlEspinhaPeixe.repaint();
        pnlEspinhaPeixe.revalidate();
        cbZoom.setSelectedItem(String.valueOf(escala).concat("%"));
    }

    /**
     *
     */
    private void enableByNode() {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) kpiTreeElemento.getLastSelectedPathComponent();
        if (node != null && node.getUserObject() instanceof KpiEspinhaPeixeNode) {
            KpiEspinhaPeixeNode kpiNode = (KpiEspinhaPeixeNode) node.getUserObject();
            if (isEdit) {
                switch (kpiNode.getTipo()) {
                    case KPI_CAUSA:
                        txtTreeDescricao.setEnabled(true);
                        btnTreeDelete.setEnabled(true);
                        btnTreeEdit.setEnabled(true);
                        mnuPlanoAcao.setEnabled(true);
                        mnuCausa.setEnabled(true);
                        kpiTreeElemento.setEditable(true);
                        break;
                    case KPI_EFEITO:
                        txtTreeDescricao.setEnabled(true);
                        btnTreeDelete.setEnabled(true);
                        btnTreeEdit.setEnabled(true);
                        mnuPlanoAcao.setEnabled(false);
                        mnuCausa.setEnabled(true);
                        kpiTreeElemento.setEditable(true);
                        break;
                    case KPI_ROOT:
                        txtTreeDescricao.setEnabled(false);
                        btnTreeDelete.setEnabled(false);
                        btnTreeEdit.setEnabled(false);
                        mnuPlanoAcao.setEnabled(false);
                        mnuCausa.setEnabled(true);
                        kpiTreeElemento.setEditable(false);
                        break;
                }
            } else {
                btnTreeEdit.setEnabled(false);
                txtTreeDescricao.setEnabled(false);
                btnTreeDelete.setEnabled(false);
                mnuPlanoAcao.setEnabled(false);
                kpiTreeElemento.setEditable(false);
            }

            txtTreeDescricao.setText(kpiNode.getDescricao());
        } else {
            txtTreeDescricao.setEnabled(false);
            btnTreeDelete.setEnabled(false);
            btnTreeEdit.setEnabled(false);
            txtTreeDescricao.setText("");
            mnuPlanoAcao.setEnabled(false);
            mnuCausa.setEnabled(false);
        }
    }

    /**
     *
     * @return
     */
    protected kpi.xml.BIXMLRecord getXMLTree() {
        DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) kpiTreeElemento.getModel().getRoot();
        kpi.xml.BIXMLRecord recEspPeixe = new kpi.xml.BIXMLRecord("ESPPEIXE");
        kpi.xml.BIXMLVector vctEfeito = new kpi.xml.BIXMLVector("EFEITOS", "EFEITO");
        kpi.xml.BIXMLVector vctCausa = new kpi.xml.BIXMLVector("CAUSAS", "CAUSA");
        KpiEspinhaPeixeNode kpiNode;
        KpiEspinhaPeixeNode kpiNodePai;

        while (oMutTreeNode != null) {
            if (!oMutTreeNode.isRoot()) {
                kpiNodePai = (KpiEspinhaPeixeNode) ((DefaultMutableTreeNode) oMutTreeNode.getParent()).getUserObject();
                kpiNode = (KpiEspinhaPeixeNode) oMutTreeNode.getUserObject();

                if (kpiNode.getTipo() == KPI_EFEITO) {
                    kpi.xml.BIXMLRecord recEfeito = new kpi.xml.BIXMLRecord("EFEITO");
                    recEfeito.set("ID", kpiNode.getID());
                    recEfeito.set("ID_PAI", kpiNodePai.getID());
                    recEfeito.set("NOME", kpiNode.toString());
                    recEfeito.set("DESCRICAO", kpiNode.getDescricao().replace("\n", "|"));
                    vctEfeito.add(recEfeito);
                } else if (kpiNode.getTipo() == KPI_CAUSA) {
                    kpi.xml.BIXMLRecord recCausa = new kpi.xml.BIXMLRecord("CAUSA");
                    recCausa.set("ID", kpiNode.getID());
                    recCausa.set("ID_PAI", kpiNodePai.getID());
                    recCausa.set("TIPO_PAI", kpiNodePai.getTipo());
                    recCausa.set("NOME", kpiNode.toString());
                    recCausa.set("DESCRICAO", kpiNode.getDescricao().replace("\n", "|"));
                    vctCausa.add(recCausa);
                }
            }

            oMutTreeNode = (DefaultMutableTreeNode) oMutTreeNode.getNextNode();
        }
        recEspPeixe.set(vctEfeito);
        recEspPeixe.set(vctCausa);
        return recEspPeixe;
    }

    /**
     *
     * @param type
     * @param nodePai
     * @return
     */
    protected DefaultMutableTreeNode createNode(int type, DefaultMutableTreeNode nodePai) {
        KpiEspinhaPeixeNode nodeEspPeixePai = (KpiEspinhaPeixeNode) nodePai.getUserObject();
        String chave = "IDNEW_".concat(String.valueOf(idNode += 1));
        String nome = "";

        if (type == KPI_CAUSA) {
            nome = (java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeCadastro_00015"));//"Nova Causa";
        } else if (type == KPI_EFEITO) {
            nome = (java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeCadastro_00016"));//"Novo Efeito";
        }

        KpiEspinhaPeixeNode nodeEspPeixe = new KpiEspinhaPeixeNode("ESP_PEIXES", chave, nome, "", nodeEspPeixePai.getPaiId(), getID(), nodeEspPeixePai.getTipoPai(), type, false);
        return new DefaultMutableTreeNode(nodeEspPeixe);
    }

    /**
     *
     * @param node
     */
    protected void addRecordDeleted(DefaultMutableTreeNode node) {
        KpiEspinhaPeixeNode oEspPeixeNode = (KpiEspinhaPeixeNode) node.getUserObject();
        if (!oEspPeixeNode.getID().startsWith("NEW_")) {
            kpi.xml.BIXMLRecord recDel = new kpi.xml.BIXMLRecord("ESP_PEIXE");
            recDel.set("ID", oEspPeixeNode.getID());
            recDel.set("NOME", oEspPeixeNode.toString());
            recDel.set("ID_PAI", oEspPeixeNode.getPaiId());
            recDel.set("TYPE_PAI", oEspPeixeNode.getTipoPai());
            recDel.set("TYPE", oEspPeixeNode.getTipo());

            if (registrosDeletados == null) {
                registrosDeletados = new kpi.xml.BIXMLRecord("REG_EXCLUIDO");
                registrosDeletados.set(new kpi.xml.BIXMLVector("EXCLUIDOS", "EXCLUIDO"));
            }
            kpi.xml.BIXMLVector vctAddReg = registrosDeletados.getBIXMLVector("EXCLUIDOS");
            vctAddReg.add(recDel);
        }
    }

    /**
     *
     * @return
     */
    protected DefaultMutableTreeNode getSelectedNode() {
        TreePath selPath = kpiTreeElemento.getSelectionPath();

        if (selPath != null) {
            return (DefaultMutableTreeNode) selPath.getLastPathComponent();
        }
        return null;
    }

    /**
     *
     * @param e
     * @param node
     */
    protected void mouseDragOnTree(java.awt.event.MouseEvent e, TreeNode node) {
        if (node != null) {
            _treeDraggingNode = node;
            kpiTreeElemento.setEditable(false);
        }
    }

    /**
     *
     * @param e
     * @param node
     * @see http://www.akadia.com/services/java_tips.html
     */
    protected void mouseDropOnTree(java.awt.event.MouseEvent e, TreeNode node) {
        DefaultTreeModel _treeModel = (DefaultTreeModel) kpiTreeElemento.getModel();

        if (node != null && _treeDraggingNode != null) {

            TreeNode target = node;

            if (_treeDraggingNode != target) {

                // Store to be moved nodes parent
                TreeNode draggingTreeNodeParent =
                        (TreeNode) _treeDraggingNode.getParent();

                if (draggingTreeNodeParent != null) {

                    // According Java documentation no remove before
                    // insert is necessary. Insert removes a node if already
                    // part of the tree. However the UI does not refresh
                    // properly without remove

                    _treeModel.removeNodeFromParent((MutableTreeNode) _treeDraggingNode);

                    // If node to be moved is a target's ancestor
                    // To be moved nodes child leading to the target
                    // node must be remounted and added to the to be moved
                    // nodes parent

                    TreeNode prevAncestor = null;
                    TreeNode ancestor = target;
                    boolean found = false;
                    do {
                        if (ancestor == _treeDraggingNode) {
                            found = true;
                            break;
                        }
                        prevAncestor = ancestor;
                        ancestor = ancestor.getParent();
                    } while (ancestor != null);
                    if (found && prevAncestor != null) {
                        _treeModel.insertNodeInto((MutableTreeNode) prevAncestor,
                                (MutableTreeNode) draggingTreeNodeParent, 0);
                    }

                    // Now insert to be moved node at its new location
                    _treeModel.insertNodeInto((MutableTreeNode) _treeDraggingNode, (MutableTreeNode) target, 0);
                    _treeDraggingNode = null;
                }
            }
        }

        kpiTreeElemento.setEditable(true);
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        jPopupMenu1 = new javax.swing.JPopupMenu();
        mnuEfeito = new javax.swing.JMenuItem();
        mnuCausa = new javax.swing.JMenuItem();
        mnuPlanoAcao = new javax.swing.JMenuItem();
        pnlData = new javax.swing.JPanel();
        tskCausaEfeito = new com.l2fprod.common.swing.JTaskPane();
        tskDados = new com.l2fprod.common.swing.JTaskPaneGroup();
        pnlConfiguracao = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        fldNome = new pv.jfcx.JPVEdit();
        lblDescricao = new javax.swing.JLabel();
        scDescricao = new javax.swing.JScrollPane();
        txtDescricao = new kpi.beans.JBITextArea();
        cbbScoreCard = new javax.swing.JComboBox();
        cbbParentIndicador = new javax.swing.JComboBox();
        lblScoreCard = new javax.swing.JLabel();
        lblParentIndicador = new javax.swing.JLabel();
        scTree = new javax.swing.JScrollPane();
        kpiTreeElemento = new javax.swing.JTree();
        scTreeDescricao = new javax.swing.JScrollPane();
        txtTreeDescricao = new kpi.beans.JBITextArea();
        tbTreeInsertion = new javax.swing.JToolBar();
        jTgNovo = new javax.swing.JToggleButton();
        btnTreeEdit = new javax.swing.JButton();
        btnTreeDelete = new javax.swing.JButton();
        lblTreeDescricao1 = new javax.swing.JLabel();
        tsArvore = new kpi.beans.JBITreeSelection();
        tskDiagrama = new com.l2fprod.common.swing.JTaskPaneGroup();
        pnlDiagrama = new javax.swing.JPanel();
        pnlLegenda = new javax.swing.JPanel();
        lblSemPlanoFigura = new javax.swing.JLabel();
        lblSubEfeitoFigura = new javax.swing.JLabel();
        lblComPlanoFigura = new javax.swing.JLabel();
        lblSubEfeito = new javax.swing.JLabel();
        lblComPlano = new javax.swing.JLabel();
        lblSemPlano = new javax.swing.JLabel();
        lblPrincipalFigura = new javax.swing.JLabel();
        lblPrincipal = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        jSeparator3 = new javax.swing.JToolBar.Separator();
        btnAbrir = new javax.swing.JButton();
        btnExportacao = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JToolBar.Separator();
        lblZoom = new javax.swing.JLabel();
        cbZoom = new javax.swing.JComboBox();
        scrDiagrama = new javax.swing.JScrollPane();
        pnlEspinhaPeixe = new kpi.swing.espinhapeixe.KpiEspinhaPeixePainel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        pnlBottomForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();

        jPopupMenu1.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                jPopupMenu1PopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });

        mnuEfeito.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_efeito.gif"))); // NOI18N
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international"); // NOI18N
        mnuEfeito.setText(bundle.getString("KpiEspinhaPeixeCadastro_00018")); // NOI18N
        mnuEfeito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuEfeitoActionPerformed(evt);
            }
        });
        jPopupMenu1.add(mnuEfeito);

        mnuCausa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_causa.gif"))); // NOI18N
        mnuCausa.setText(bundle.getString("KpiEspinhaPeixeCadastro_00017")); // NOI18N
        mnuCausa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuCausaActionPerformed(evt);
            }
        });
        jPopupMenu1.add(mnuCausa);

        mnuPlanoAcao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_planodeacao.gif"))); // NOI18N
        mnuPlanoAcao.setText(bundle.getString("KpiPlanoDeAcao_00052")); // NOI18N
        mnuPlanoAcao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuPlanoAcaoActionPerformed(evt);
            }
        });
        jPopupMenu1.add(mnuPlanoAcao);

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle1.getString("KpiEspinhaPeixeCadastro_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_epinhapeixe.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(883, 376));

        pnlData.setLayout(new java.awt.BorderLayout());

        tskDados.setSpecial(true);
        tskDados.setTitle("Dados Gerais");

        pnlConfiguracao.setOpaque(false);
        pnlConfiguracao.setPreferredSize(new java.awt.Dimension(210, 210));
        pnlConfiguracao.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        java.util.ResourceBundle bundle2 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        lblNome.setText(bundle2.getString("KpiEspinhaPeixeCadastro_00004")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlConfiguracao.add(lblNome);
        lblNome.setBounds(10, 10, 58, 20);

        fldNome.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                fldNomeFocusLost(evt);
            }
        });
        pnlConfiguracao.add(fldNome);
        fldNome.setBounds(10, 30, 400, 22);

        lblDescricao.setText(bundle2.getString("KpiEspinhaPeixeCadastro_00007")); // NOI18N
        lblDescricao.setEnabled(false);
        pnlConfiguracao.add(lblDescricao);
        lblDescricao.setBounds(10, 130, 60, 20);

        txtDescricao.setFont(new java.awt.Font("Tahoma", 0, 11));
        scDescricao.setViewportView(txtDescricao);

        pnlConfiguracao.add(scDescricao);
        scDescricao.setBounds(10, 150, 400, 60);

        cbbScoreCard.setEnabled(false);
        cbbScoreCard.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbScoreCardItemStateChanged(evt);
            }
        });
        pnlConfiguracao.add(cbbScoreCard);
        cbbScoreCard.setBounds(10, 70, 400, 22);

        cbbParentIndicador.setEnabled(false);
        pnlConfiguracao.add(cbbParentIndicador);
        cbbParentIndicador.setBounds(10, 110, 400, 22);

        lblScoreCard.setForeground(new java.awt.Color(51, 51, 255));
        lblScoreCard.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblScoreCard.setText(bundle1.getString("KpiEspinhaPeixeCadastro_00005")); // NOI18N
        lblScoreCard.setEnabled(false);
        pnlConfiguracao.add(lblScoreCard);
        lblScoreCard.setBounds(10, 50, 100, 20);

        lblParentIndicador.setForeground(new java.awt.Color(51, 51, 255));
        lblParentIndicador.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblParentIndicador.setText(bundle1.getString("KpiEspinhaPeixeCadastro_00006")); // NOI18N
        lblParentIndicador.setEnabled(false);
        pnlConfiguracao.add(lblParentIndicador);
        lblParentIndicador.setBounds(10, 90, 100, 20);

        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("Aguarde...");
        kpiTreeElemento.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        kpiTreeElemento.setMinimumSize(new java.awt.Dimension(10, 10));
        kpiTreeElemento.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                kpiTreeElementoMouseReleased(evt);
            }
        });
        kpiTreeElemento.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
                kpiTreeElementoValueChanged(evt);
            }
        });
        kpiTreeElemento.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                kpiTreeElementoMouseDragged(evt);
            }
        });
        scTree.setViewportView(kpiTreeElemento);

        pnlConfiguracao.add(scTree);
        scTree.setBounds(450, 30, 400, 100);

        txtTreeDescricao.setFont(new java.awt.Font("Tahoma", 0, 11));
        txtTreeDescricao.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtTreeDescricaoKeyReleased(evt);
            }
        });
        scTreeDescricao.setViewportView(txtTreeDescricao);

        pnlConfiguracao.add(scTreeDescricao);
        scTreeDescricao.setBounds(450, 150, 400, 60);

        tbTreeInsertion.setFloatable(false);
        tbTreeInsertion.setRollover(true);
        tbTreeInsertion.setOpaque(false);
        tbTreeInsertion.setPreferredSize(new java.awt.Dimension(280, 32));

        jTgNovo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_novo.gif"))); // NOI18N
        jTgNovo.setToolTipText(bundle1.getString("KpiEspinhaPeixeCadastro_00008")); // NOI18N
        jTgNovo.setMaximumSize(new java.awt.Dimension(25, 25));
        jTgNovo.setMinimumSize(new java.awt.Dimension(25, 25));
        jTgNovo.setPreferredSize(new java.awt.Dimension(25, 25));
        jTgNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTgNovoActionPerformed(evt);
            }
        });
        tbTreeInsertion.add(jTgNovo);

        btnTreeEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnTreeEdit.setToolTipText(bundle1.getString("KpiBaseFrame_00003")); // NOI18N
        btnTreeEdit.setMaximumSize(new java.awt.Dimension(25, 25));
        btnTreeEdit.setMinimumSize(new java.awt.Dimension(25, 25));
        btnTreeEdit.setOpaque(false);
        btnTreeEdit.setPreferredSize(new java.awt.Dimension(25, 25));
        btnTreeEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTreeEditActionPerformed(evt);
            }
        });
        tbTreeInsertion.add(btnTreeEdit);

        btnTreeDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnTreeDelete.setToolTipText(bundle1.getString("KpiBaseFrame_00004")); // NOI18N
        btnTreeDelete.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnTreeDelete.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnTreeDelete.setMaximumSize(new java.awt.Dimension(25, 25));
        btnTreeDelete.setMinimumSize(new java.awt.Dimension(25, 25));
        btnTreeDelete.setOpaque(false);
        btnTreeDelete.setPreferredSize(new java.awt.Dimension(25, 25));
        btnTreeDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTreeDeleteActionPerformed(evt);
            }
        });
        tbTreeInsertion.add(btnTreeDelete);

        pnlConfiguracao.add(tbTreeInsertion);
        tbTreeInsertion.setBounds(450, 0, 400, 30);

        lblTreeDescricao1.setText(bundle2.getString("KpiEspinhaPeixeCadastro_00007")); // NOI18N
        lblTreeDescricao1.setEnabled(false);
        pnlConfiguracao.add(lblTreeDescricao1);
        lblTreeDescricao1.setBounds(450, 130, 180, 20);

        tsArvore.setCombo(cbbScoreCard);
        tsArvore.setRoot(false);
        pnlConfiguracao.add(tsArvore);
        tsArvore.setBounds(410, 70, 25, 22);

        tskDados.getContentPane().add(pnlConfiguracao);

        tskCausaEfeito.add(tskDados);

        tskDiagrama.setTitle(bundle1.getString("KpiEspinhaPeixeCadastro_00001")); // NOI18N

        pnlDiagrama.setPreferredSize(new java.awt.Dimension(400, 400));
        pnlDiagrama.setLayout(new java.awt.BorderLayout());

        pnlLegenda.setBackground(new java.awt.Color(255, 255, 255));
        pnlLegenda.setPreferredSize(new java.awt.Dimension(10, 22));
        pnlLegenda.setLayout(null);

        lblSemPlanoFigura.setBackground(new java.awt.Color(255, 255, 255));
        lblSemPlanoFigura.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 195), 2));
        lblSemPlanoFigura.setOpaque(true);
        pnlLegenda.add(lblSemPlanoFigura);
        lblSemPlanoFigura.setBounds(370, 6, 20, 10);

        lblSubEfeitoFigura.setBackground(new java.awt.Color(255, 255, 51));
        lblSubEfeitoFigura.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblSubEfeitoFigura.setOpaque(true);
        pnlLegenda.add(lblSubEfeitoFigura);
        lblSubEfeitoFigura.setBounds(120, 6, 20, 10);

        lblComPlanoFigura.setBackground(new java.awt.Color(255, 255, 255));
        lblComPlanoFigura.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 153), 2));
        lblComPlanoFigura.setOpaque(true);
        pnlLegenda.add(lblComPlanoFigura);
        lblComPlanoFigura.setBounds(210, 6, 20, 10);

        lblSubEfeito.setText(bundle1.getString("KpiEspinhaPeixeRel_00007")); // NOI18N
        pnlLegenda.add(lblSubEfeito);
        lblSubEfeito.setBounds(150, 5, 60, 14);

        lblComPlano.setText(bundle1.getString("KpiEspinhaPeixeRel_00008")); // NOI18N
        pnlLegenda.add(lblComPlano);
        lblComPlano.setBounds(240, 5, 130, 14);

        lblSemPlano.setText(bundle1.getString("KpiEspinhaPeixeRel_00009")); // NOI18N
        pnlLegenda.add(lblSemPlano);
        lblSemPlano.setBounds(400, 5, 130, 14);

        lblPrincipalFigura.setBackground(new java.awt.Color(255, 255, 153));
        lblPrincipalFigura.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblPrincipalFigura.setOpaque(true);
        pnlLegenda.add(lblPrincipalFigura);
        lblPrincipalFigura.setBounds(10, 6, 20, 10);

        lblPrincipal.setText(bundle1.getString("KpiEspinhaPeixeRel_00014")); // NOI18N
        pnlLegenda.add(lblPrincipal);
        lblPrincipal.setBounds(40, 5, 80, 14);

        jToolBar1.setFloatable(false);
        jToolBar1.setOpaque(false);

        jSeparator3.setMaximumSize(new java.awt.Dimension(20, 20));
        jSeparator3.setMinimumSize(new java.awt.Dimension(20, 20));
        jSeparator3.setPreferredSize(new java.awt.Dimension(20, 20));
        jToolBar1.add(jSeparator3);

        btnAbrir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_download.gif"))); // NOI18N
        btnAbrir.setText(bundle1.getString("KpiEspinhaPeixeRel_00004")); // NOI18N
        btnAbrir.setFocusable(false);
        btnAbrir.setMaximumSize(new java.awt.Dimension(100, 100));
        btnAbrir.setMinimumSize(new java.awt.Dimension(50, 50));
        btnAbrir.setOpaque(false);
        btnAbrir.setPreferredSize(new java.awt.Dimension(100, 100));
        btnAbrir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });
        jToolBar1.add(btnAbrir);

        btnExportacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_exportacao.gif"))); // NOI18N
        btnExportacao.setText(bundle1.getString("KpiEspinhaPeixeRel_00003")); // NOI18N
        btnExportacao.setFocusable(false);
        btnExportacao.setMaximumSize(new java.awt.Dimension(100, 100));
        btnExportacao.setMinimumSize(new java.awt.Dimension(60, 60));
        btnExportacao.setOpaque(false);
        btnExportacao.setPreferredSize(new java.awt.Dimension(100, 100));
        btnExportacao.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnExportacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportacaoActionPerformed(evt);
            }
        });
        jToolBar1.add(btnExportacao);

        jSeparator4.setMaximumSize(new java.awt.Dimension(20, 20));
        jSeparator4.setMinimumSize(new java.awt.Dimension(20, 20));
        jSeparator4.setPreferredSize(new java.awt.Dimension(20, 20));
        jToolBar1.add(jSeparator4);

        lblZoom.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblZoom.setText(bundle1.getString("KpiEspinhaPeixeRel_00005")); // NOI18N
        lblZoom.setMaximumSize(new java.awt.Dimension(40, 14));
        lblZoom.setMinimumSize(new java.awt.Dimension(40, 14));
        lblZoom.setPreferredSize(new java.awt.Dimension(40, 14));
        jToolBar1.add(lblZoom);

        cbZoom.setEditable(true);
        cbZoom.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "200%", "150%", "130%", "100%", "90%", "80%", "70%", "60%", "55%", "45%", "40%", "30%" }));
        cbZoom.setMaximumSize(new java.awt.Dimension(55, 22));
        cbZoom.setMinimumSize(new java.awt.Dimension(50, 22));
        cbZoom.setPreferredSize(new java.awt.Dimension(100, 100));
        cbZoom.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbZoomItemStateChanged(evt);
            }
        });
        jToolBar1.add(cbZoom);

        pnlLegenda.add(jToolBar1);
        jToolBar1.setBounds(530, 0, 270, 20);

        pnlDiagrama.add(pnlLegenda, java.awt.BorderLayout.PAGE_START);

        scrDiagrama.setBackground(new java.awt.Color(255, 255, 255));
        scrDiagrama.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        pnlEspinhaPeixe.setBackground(new java.awt.Color(255, 255, 255));
        scrDiagrama.setViewportView(pnlEspinhaPeixe);

        pnlDiagrama.add(scrDiagrama, java.awt.BorderLayout.CENTER);

        tskDiagrama.getContentPane().add(pnlDiagrama);

        tskCausaEfeito.add(tskDiagrama);

        pnlData.add(tskCausaEfeito, java.awt.BorderLayout.CENTER);

        getContentPane().add(pnlData, java.awt.BorderLayout.CENTER);

        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 25));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle2.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle2.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(360, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(200, 25));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setMaximumSize(new java.awt.Dimension(245, 19));
        tbInsertion.setMinimumSize(new java.awt.Dimension(245, 19));
        tbInsertion.setPreferredSize(new java.awt.Dimension(245, 32));

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle2.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.setMaximumSize(new java.awt.Dimension(100, 100));
        btnEdit.setMinimumSize(new java.awt.Dimension(40, 40));
        btnEdit.setPreferredSize(new java.awt.Dimension(100, 100));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle2.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.setMaximumSize(new java.awt.Dimension(100, 100));
        btnDelete.setMinimumSize(new java.awt.Dimension(40, 40));
        btnDelete.setPreferredSize(new java.awt.Dimension(100, 100));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle2.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnReload.setMaximumSize(new java.awt.Dimension(100, 100));
        btnReload.setMinimumSize(new java.awt.Dimension(60, 60));
        btnReload.setPreferredSize(new java.awt.Dimension(100, 100));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        getContentPane().add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pack();
    }//GEN-END:initComponents

	private void mnuPlanoAcaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuPlanoAcaoActionPerformed
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) kpiTreeElemento.getLastSelectedPathComponent();
            if (node != null && node.getUserObject() instanceof KpiEspinhaPeixeNode) {
                KpiEspinhaPeixeNode kpiNode = (KpiEspinhaPeixeNode) node.getUserObject();
                if (!kpiNode.getID().startsWith("IDNEW_")) {
                    if (kpiNode.getTipo() == KPI_CAUSA) {
                        kpi.swing.framework.KpiGrupoAcao form = (kpi.swing.framework.KpiGrupoAcao) formController.newForm("GRUPO_ACAO", "0", "0");
                        form.setEspPeixe(getID(), kpiNode.getID());
                    }
                } else {
                    kpi.swing.KpiDefaultDialogSystem dialogSystem;
                    dialogSystem = new kpi.swing.KpiDefaultDialogSystem(this);
                    dialogSystem.informationMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeCadastro_00019"));//"A causa deve ser gravada antes de incluir uma a��o."
                }
            }
	}//GEN-LAST:event_mnuPlanoAcaoActionPerformed

	private void mnuEfeitoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuEfeitoActionPerformed
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) kpiTreeElemento.getModel().getRoot();
            node.add(createNode(KPI_EFEITO, node));
            TreePath path = kpiTreeElemento.getSelectionPath();
            kpiTreeElemento.repaint();
            DefaultTreeModel treeModel = (DefaultTreeModel) kpiTreeElemento.getModel();
            treeModel.reload();
            kpiTreeElemento.setSelectionPath(path);
            kpiTreeElemento.expandPath(path);
	}//GEN-LAST:event_mnuEfeitoActionPerformed

	private void mnuCausaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuCausaActionPerformed
            DefaultMutableTreeNode node = getSelectedNode();
            node.add(createNode(KPI_CAUSA, node));
            TreePath path = kpiTreeElemento.getSelectionPath();
            kpiTreeElemento.repaint();
            DefaultTreeModel treeModel = (DefaultTreeModel) kpiTreeElemento.getModel();
            treeModel.reload();
            kpiTreeElemento.setSelectionPath(path);
            kpiTreeElemento.expandPath(path);
	}//GEN-LAST:event_mnuCausaActionPerformed

	private void jPopupMenu1PopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_jPopupMenu1PopupMenuWillBecomeInvisible
            jTgNovo.setSelected(false);
	}//GEN-LAST:event_jPopupMenu1PopupMenuWillBecomeInvisible

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
            atualizaDiagrama();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

        private void cbZoomItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbZoomItemStateChanged
            if (true && evt.getStateChange() == ItemEvent.SELECTED) {
                String cZoom = (String) cbZoom.getSelectedItem();
                int escala_atual = 100;
                try {
                    escala_atual = Integer.parseInt(cZoom.replace("%", "").trim());
                    if (escala_atual > 200 || escala_atual < 30) {
                        //"O n�mero deve estar entre 30 e 200."
                        dialog.warningMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeRel_00011"));
                    } else {
                        escala = escala_atual;
                    }
                } catch (NumberFormatException ex) {
                    //"Esta n�o � uma medida v�lida."
                    dialog.warningMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeRel_00012"));
                }
                atualizaDiagrama();
            }
}//GEN-LAST:event_cbZoomItemStateChanged

        private void btnExportacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportacaoActionPerformed
            KpiFileChooser frmChooser = new KpiFileChooser(KpiStaticReferences.getKpiMainPanel(), true);
            KpiToolKit tool = new KpiToolKit();

            frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_SAVE);
            frmChooser.setFileType("*.jpg");
            frmChooser.loadServerFiles("graphs\\*.jpg");
            frmChooser.setFileName("EspinhaPeixe.jpg");
            frmChooser.setVisible(true);

            if (frmChooser.isValidFile()) {
                BufferedImage diagrama = new BufferedImage(
                        pnlEspinhaPeixe.getWidth(),
                        pnlEspinhaPeixe.getHeight(),
                        BufferedImage.TYPE_INT_RGB);

                Graphics2D grafico = diagrama.createGraphics();

                grafico.setBackground(Color.WHITE);
                grafico.fillRect(0, 0, pnlEspinhaPeixe.getWidth(), pnlEspinhaPeixe.getHeight());

                pnlEspinhaPeixe.doLayout();
                pnlEspinhaPeixe.paint(grafico);

                tool.writeJPEGFile(diagrama, "graphs//" + frmChooser.getFileName());
            }
        }//GEN-LAST:event_btnExportacaoActionPerformed

        private void btnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed
            kpi.swing.KpiFileChooser frmChooser = new kpi.swing.KpiFileChooser(kpi.core.KpiStaticReferences.getKpiMainPanel(), true);
            frmChooser.setDialogType(KpiFileChooser.KPI_DIALOG_OPEN);
            frmChooser.setFileType("*.jpg");
            frmChooser.loadServerFiles("graphs\\*.jpg");
            frmChooser.setVisible(true);
            frmChooser.setUrlPath("/graphs/");
            frmChooser.openFile();
}//GEN-LAST:event_btnAbrirActionPerformed

        private void btnTreeDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTreeDeleteActionPerformed
            DefaultMutableTreeNode node = getSelectedNode();
            addRecordDeleted(node);
            TreePath path = kpiTreeElemento.getSelectionPath();
            node.removeFromParent();
            kpiTreeElemento.repaint();
            ((javax.swing.tree.DefaultTreeModel) kpiTreeElemento.getModel()).reload();
            kpiTreeElemento.setSelectionPath(path);
            kpiTreeElemento.expandPath(path);
            txtTreeDescricao.setEnabled(false);
            btnTreeDelete.setEnabled(false);
            btnTreeEdit.setEnabled(false);
            txtTreeDescricao.setText("");
            mnuCausa.setEnabled(false);
            mnuPlanoAcao.setEnabled(false);
}//GEN-LAST:event_btnTreeDeleteActionPerformed

        private void btnTreeEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTreeEditActionPerformed
            TreePath path = kpiTreeElemento.getSelectionPath();
            kpiTreeElemento.startEditingAtPath(path);
}//GEN-LAST:event_btnTreeEditActionPerformed

        private void jTgNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTgNovoActionPerformed
            jPopupMenu1.setBorderPainted(true);
            jPopupMenu1.show(jTgNovo, jTgNovo.getX(), jTgNovo.getY() + 21);
}//GEN-LAST:event_jTgNovoActionPerformed

        private void txtTreeDescricaoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTreeDescricaoKeyReleased
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) kpiTreeElemento.getLastSelectedPathComponent();
            if (node != null && node.getUserObject() instanceof KpiEspinhaPeixeNode) {
                KpiEspinhaPeixeNode kpiNode = (KpiEspinhaPeixeNode) node.getUserObject();
                kpiNode.setDescricao(txtTreeDescricao.getText());
            }
}//GEN-LAST:event_txtTreeDescricaoKeyReleased

        private void kpiTreeElementoMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_kpiTreeElementoMouseDragged
            if (isEdit) {
                if (!_treeDragging) {
                    _treeDragging = true;
                    //Set cursor:
                    setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                    //Read node which is dragged:
                    TreePath path = kpiTreeElemento.getPathForLocation(evt.getX(), evt.getY());
                    TreeNode node = null;
                    if (path != null) {
                        node = (TreeNode) path.getLastPathComponent();
                    }
                    mouseDragOnTree(evt, node);
                } else {
                    TreePath path = kpiTreeElemento.getPathForLocation(evt.getX(), evt.getY());
                    kpiTreeElemento.setSelectionPath(path);
                }

                atualizaDiagrama();
            }
}//GEN-LAST:event_kpiTreeElementoMouseDragged

        private void kpiTreeElementoValueChanged(javax.swing.event.TreeSelectionEvent evt) {//GEN-FIRST:event_kpiTreeElementoValueChanged
            enableByNode();
            atualizaDiagrama();
}//GEN-LAST:event_kpiTreeElementoValueChanged

        private void kpiTreeElementoMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_kpiTreeElementoMouseReleased
            if (_treeDragging && isEdit) {
                TreePath path = kpiTreeElemento.getPathForLocation(evt.getX(), evt.getY());
                _treeDragging = false;
                setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                TreeNode node = null;
                if (path != null) {
                    node = (TreeNode) path.getLastPathComponent();
                }

                DefaultMutableTreeNode nodeDrag = (DefaultMutableTreeNode) _treeDraggingNode;
                DefaultMutableTreeNode nodeDrop = (DefaultMutableTreeNode) node;

                if (((KpiEspinhaPeixeNode) nodeDrag.getUserObject()).getTipo() == KPI_EFEITO && nodeDrop.isRoot() == false) {
                    dialog.warningMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeCadastro_00013"));
                    kpiTreeElemento.setEditable(true);
                } else {
                    mouseDropOnTree(evt, node);
                }

                atualizaDiagrama();
            }
}//GEN-LAST:event_kpiTreeElementoMouseReleased

        private void cbbScoreCardItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbScoreCardItemStateChanged
            if (evt.getStateChange() == ItemEvent.SELECTED && cbbScoreCard.getItemCount() > 0) {
                StringBuilder selectInd = new StringBuilder("ID_SCOREC = '");
                selectInd.append(event.getComboValue(htbScoreCard, cbbScoreCard)).append("'");

                kpi.xml.BIXMLRecord recIndFiltrados = event.listRecords("INDICADOR", selectInd.toString());
                event.populateCombo(recIndFiltrados.getBIXMLVector("INDICADORES"), "INDICADOR", htbIndicadores, cbbParentIndicador);
            }
}//GEN-LAST:event_cbbScoreCardItemStateChanged

        private void fldNomeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fldNomeFocusLost
            DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) kpiTreeElemento.getModel().getRoot();
            KpiEspinhaPeixeNode oEspPeixeNode = (KpiEspinhaPeixeNode) oMutTreeNode.getUserObject();
            if (!oEspPeixeNode.getName().equals(fldNome.getText())) {
                oEspPeixeNode.setName(fldNome.getText());
                kpiTreeElemento.repaint();
            }
}//GEN-LAST:event_fldNomeFocusLost

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAbrir;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnExportacao;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnTreeDelete;
    private javax.swing.JButton btnTreeEdit;
    private javax.swing.JComboBox cbZoom;
    private javax.swing.JComboBox cbbParentIndicador;
    private javax.swing.JComboBox cbbScoreCard;
    protected pv.jfcx.JPVEdit fldNome;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JToolBar.Separator jSeparator3;
    private javax.swing.JToolBar.Separator jSeparator4;
    private javax.swing.JToggleButton jTgNovo;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTree kpiTreeElemento;
    private javax.swing.JLabel lblComPlano;
    private javax.swing.JLabel lblComPlanoFigura;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblParentIndicador;
    private javax.swing.JLabel lblPrincipal;
    private javax.swing.JLabel lblPrincipalFigura;
    private javax.swing.JLabel lblScoreCard;
    private javax.swing.JLabel lblSemPlano;
    private javax.swing.JLabel lblSemPlanoFigura;
    private javax.swing.JLabel lblSubEfeito;
    private javax.swing.JLabel lblSubEfeitoFigura;
    private javax.swing.JLabel lblTreeDescricao1;
    private javax.swing.JLabel lblZoom;
    private javax.swing.JMenuItem mnuCausa;
    private javax.swing.JMenuItem mnuEfeito;
    private javax.swing.JMenuItem mnuPlanoAcao;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlConfiguracao;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlDiagrama;
    private kpi.swing.espinhapeixe.KpiEspinhaPeixePainel pnlEspinhaPeixe;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLegenda;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JScrollPane scDescricao;
    private javax.swing.JScrollPane scTree;
    private javax.swing.JScrollPane scTreeDescricao;
    private javax.swing.JScrollPane scrDiagrama;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    public javax.swing.JToolBar tbTreeInsertion;
    private kpi.beans.JBITreeSelection tsArvore;
    private com.l2fprod.common.swing.JTaskPane tskCausaEfeito;
    private com.l2fprod.common.swing.JTaskPaneGroup tskDados;
    private com.l2fprod.common.swing.JTaskPaneGroup tskDiagrama;
    private kpi.beans.JBITextArea txtDescricao;
    private kpi.beans.JBITextArea txtTreeDescricao;
    // End of variables declaration//GEN-END:variables

    public javax.swing.tree.DefaultMutableTreeNode createTree(BIXMLRecord treeXML) {
        DefaultMutableTreeNode root = insertGroup(treeXML.getBIXMLVector("ESP_PEIXES"), null);
        return root;
    }

    public DefaultMutableTreeNode insertRecord(BIXMLRecord record, DefaultMutableTreeNode tree) throws BIXMLException {
        KpiEspinhaPeixeNode oEspPeixeNode = new KpiEspinhaPeixeNode("ESP_PEIXES",
                record.getAttributes().getString("ID"),
                record.getAttributes().getString("NOME"),
                record.getAttributes().getString("DESCRICAO").replace("|", "\n"),
                record.getAttributes().getString("ID_PAI"),
                getID(),
                record.getAttributes().getInt("TYPE_PAI"),
                record.getAttributes().getInt("TYPE"),
                record.getAttributes().getBoolean("HASACAO"));

        DefaultMutableTreeNode child = new DefaultMutableTreeNode(oEspPeixeNode);

        for (java.util.Iterator e = record.getKeyNames(); e.hasNext();) {
            String strChave = (String) e.next();

            if (record.getBIXMLVector(strChave).getAttributes().contains("TIPO")) {
                child = insertGroup(record.getBIXMLVector(strChave), child);
            } else {
                child = insertRecord(record.getBIXMLVector(strChave), child);
            }
        }

        if (tree != null) {
            tree.add(child);
            return tree;
        } else {
            return child;
        }
    }

    public DefaultMutableTreeNode insertRecord(BIXMLVector vector, DefaultMutableTreeNode tree) throws BIXMLException {
        KpiEspinhaPeixeNode oEspPeixeNode = new KpiEspinhaPeixeNode("ESP_PEIXES",
                vector.getAttributes().getString("ID"),
                vector.getAttributes().getString("NOME"),
                vector.getAttributes().getString("DESCRICAO").replace("|", "\n"),
                vector.getAttributes().getString("ID_PAI"),
                getID(),
                vector.getAttributes().getInt("TYPE_PAI"),
                vector.getAttributes().getInt("TYPE"),
                vector.getAttributes().getBoolean("HASACAO"));

        DefaultMutableTreeNode child = new DefaultMutableTreeNode(oEspPeixeNode);

        for (int i = 0; i < vector.size(); i++) {
            child = insertRecord(vector.get(i), child);
        }

        if (tree != null) {
            tree.add(child);
            return tree;
        } else {
            return child;
        }
    }

    public DefaultMutableTreeNode insertGroup(BIXMLVector vector, DefaultMutableTreeNode tree) throws BIXMLException {
        KpiEspinhaPeixeNode oEspPeixeNode = new KpiEspinhaPeixeNode("ESP_PEIXES",
                vector.getAttributes().getString("ID"),
                vector.getAttributes().getString("NOME"),
                vector.getAttributes().getString("DESCRICAO").replace("|", "\n"),
                vector.getAttributes().getString("ID_PAI"),
                getID(),
                vector.getAttributes().getInt("TYPE_PAI"),
                vector.getAttributes().getInt("TYPE"),
                vector.getAttributes().getBoolean("HASACAO"));

        DefaultMutableTreeNode child = new DefaultMutableTreeNode(oEspPeixeNode);

        for (int i = 0; i < vector.size(); i++) {
            child = insertRecord(vector.get(i), child);
        }

        if (tree != null) {
            tree.add(child);
            return tree;
        } else {
            return child;
        }

    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        btnSave.setEnabled(true);
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return null;
    }

    @Override
    public String getParentType() {
        return parentType;
    }
}


/*
 * Renderiza��o customizada para a �rvore
 * herdado de KpiTreeCellRenderer
 * @author  siga2516 Lucio Pelinson
 */
class KpiEspPeixeTreeCellRenderer extends kpi.swing.KpiTreeCellRenderer {

    private kpi.core.KpiImageResources myImageResources;

    public KpiEspPeixeTreeCellRenderer(kpi.core.KpiImageResources imageResources) {
        super(imageResources);
        setUseMyRenderer(true);
        myImageResources = imageResources;
    }

    @Override
    public void myImageRenderer(Object value) {
        javax.swing.tree.DefaultMutableTreeNode nodeAux = (javax.swing.tree.DefaultMutableTreeNode) value;
        if (nodeAux.getUserObject() instanceof KpiEspinhaPeixeNode) {
            KpiEspinhaPeixeNode myNode = (KpiEspinhaPeixeNode) nodeAux.getUserObject();

            int imgTree = 0;
            if (myNode.getTipo() == kpi.swing.espinhapeixe.KpiEspinhaPeixeCadastro.KPI_ROOT) {
                imgTree = KpiImageResources.KPI_IMG_ANOMALIA;
            } else if (myNode.getTipo() == kpi.swing.espinhapeixe.KpiEspinhaPeixeCadastro.KPI_EFEITO) {
                imgTree = KpiImageResources.KPI_IMG_EFEITO;
            } else if (myNode.getTipo() == kpi.swing.espinhapeixe.KpiEspinhaPeixeCadastro.KPI_CAUSA) {
                if (myNode.hasPlanAcao) {
                    imgTree = KpiImageResources.KPI_IMG_CAUSA_ACAO;
                } else {
                    imgTree = KpiImageResources.KPI_IMG_CAUSA;
                }
            }

            setDisabledIcon(myImageResources.getImage(imgTree));
            setOpenIcon(myImageResources.getImage(imgTree));
            setLeafIcon(myImageResources.getImage(imgTree));
            setIcon(myImageResources.getImage(imgTree));
            setClosedIcon(myImageResources.getImage(imgTree));
        }

        setBackground(null);
        setBackgroundNonSelectionColor(null);
        setOpaque(false);
    }
}

/*
 * Classe para tratamento dos valores digitados na JTree.
 * herdado de DefaultTreeCellEditor
 * @author  siga2516 Lucio Pelinson
 */
class KpiEspPeixeTreeCellEditor extends DefaultTreeCellEditor {

    private KpiEspinhaPeixeNode kpiNode;
    private kpi.core.KpiImageResources myImageResources;

    public KpiEspPeixeTreeCellEditor(JTree tree,
            DefaultTreeCellRenderer renderer,
            kpi.core.KpiImageResources imageResources) {
        super(tree, renderer, null);
        myImageResources = imageResources;
    }

    public KpiEspPeixeTreeCellEditor(JTree tree, DefaultTreeCellRenderer renderer,
            TreeCellEditor editor) {
        super(tree, renderer, editor);
    }

    @Override
    public Component getTreeCellEditorComponent(JTree tree, Object value,
            boolean isSelected,
            boolean expanded,
            boolean leaf, int row) {

        kpiNode = (KpiEspinhaPeixeNode) ((DefaultMutableTreeNode) value).getUserObject();

        //Render ao editar n�
        int imgTree = 0;
        if (kpiNode.getTipo() == kpi.swing.espinhapeixe.KpiEspinhaPeixeCadastro.KPI_ROOT) {
            imgTree = KpiImageResources.KPI_IMG_ANOMALIA;
        } else if (kpiNode.getTipo() == kpi.swing.espinhapeixe.KpiEspinhaPeixeCadastro.KPI_EFEITO) {
            imgTree = KpiImageResources.KPI_IMG_EFEITO;
        } else if (kpiNode.getTipo() == kpi.swing.espinhapeixe.KpiEspinhaPeixeCadastro.KPI_CAUSA) {
            if (kpiNode.hasPlanAcao) {
                imgTree = KpiImageResources.KPI_IMG_CAUSA_ACAO;
            } else {
                imgTree = KpiImageResources.KPI_IMG_CAUSA;
            }

        }
        super.renderer.setOpenIcon(myImageResources.getImage(imgTree));
        super.renderer.setLeafIcon(myImageResources.getImage(imgTree));
        super.renderer.setIcon(myImageResources.getImage(imgTree));
        super.renderer.setClosedIcon(myImageResources.getImage(imgTree));

        return super.getTreeCellEditorComponent(tree, value, isSelected, expanded, leaf, row);
    }

    @Override
    public Object getCellEditorValue() {
        String newValue = super.realEditor.getCellEditorValue().toString();
        if (validCell(newValue)) {
            kpiNode.setName(newValue);
        } else {
            kpi.swing.KpiDefaultDialogSystem dialog = kpi.core.KpiStaticReferences.getKpiDialogSystem();

            //"O texto n�o pode conter caracteres:"
            dialog.warningMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeCadastro_00014").concat("\n ( & ), ( ' ) ou ( | )"));
        }

        return (Object) kpiNode;
    }

    private boolean validCell(String texto) {
        boolean lRet = false;
        if (texto.contains("&")
                || texto.contains("'")
                || texto.contains("\"")
                || texto.contains("|")) {
            lRet = false;
        } else {
            lRet = true;
        }

        return lRet;
    }
}
