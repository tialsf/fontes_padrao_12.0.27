package kpi.swing.espinhapeixe;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import kpi.beans.JBISeekListPanel;
import kpi.core.KpiStaticReferences;

public class KpiEspinhaPeixeLista extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "ESP_PEIXE";
    private String formType = "LST_ESP_PEIXE";
    private String parentType = recordType;
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;
    private String filtro;

    public KpiEspinhaPeixeLista(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        filtro = contextId;
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
    }

    @Override
    public void disableFields() {
    }

    @Override
    public void refreshFields() {
        kpi.xml.BIXMLRecord recUnidades = event.listRecords("ESP_PEIXE", filtro);
        jBIListUnidade.setDataSource(recUnidades.getBIXMLVector("ESP_PEIXES"), "0", "0", this.event);
        jBIListUnidade.xmlTable.setSortColumn(0);
    }

    
    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        tapCadastro = new JTabbedPane();
        jBIListUnidade = new JBISeekListPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiEspinhaPeixeLista_00001")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_epinhapeixe.gif"))); // NOI18N
        setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(5, 5));

        tapCadastro.setEnabled(false);
        tapCadastro.setFocusable(false);
        tapCadastro.setPreferredSize(new Dimension(0, 0));
        getContentPane().add(tapCadastro, BorderLayout.WEST);
        getContentPane().add(jBIListUnidade, BorderLayout.CENTER);

        pack();
    }//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JBISeekListPanel jBIListUnidade;
    private JTabbedPane tapCadastro;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public void loadRecord() {
        this.setID("0");
        this.refreshFields();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }
}
