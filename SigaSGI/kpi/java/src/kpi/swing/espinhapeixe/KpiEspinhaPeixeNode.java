/*
 * KpiEspinhaPeixeNode.java
 *
 * Created on March 21, 2007, 1:43 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package kpi.swing.espinhapeixe;
/**
 * TreeNode Espinha de Peixe
 * herdado de KpiTreeNode
 * @author  siga2516 Lucio Pelinson
 */
public class KpiEspinhaPeixeNode extends kpi.swing.KpiTreeNode {
		
	public static int	nCount		= 0;
	private	int			nTipo		= 0;
	private	int			nParentTipo	= 0;
	private Boolean		isEdit		= false;
	public  Boolean		hasPlanAcao	= false;
	private String		cParentID	= new String();
	private String		cDescricao	= new String();
	private String		cRootId		= new String();
	
	public KpiEspinhaPeixeNode(String typeAux, String idAux, String nameAux, String descricao, String cPaiId, String cRootId, int nTipoPai, int nType, boolean lHasPlanAcao) {
		super( typeAux, idAux, nameAux);
		setTipo(nType);
		setPaiId(cPaiId);
		setRootId(cRootId);
		setTipoPai(nTipoPai);
		setDescricao(descricao);
		this.hasPlanAcao = lHasPlanAcao;
	}
	
	public KpiEspinhaPeixeNode(String typeAux, String idAux, String nameAux, int nType, boolean lHasPlanAcao) {
		super( typeAux, idAux, nameAux);
		setTipo(nType);
		this.hasPlanAcao = lHasPlanAcao;
	}
	
	
	public int getTipo() {
		return nTipo;
	}
	
	public void setTipo(int nTipo) {
		this.nTipo = nTipo;
	}
	
	public int getTipoPai() {
		return nParentTipo;
	}
	
	public void setTipoPai(int nTipo) {
		this.nParentTipo = nTipo;
	}

	public String getPaiId() {
		return cParentID;
	}

	public void setPaiId(String parentID) {
		this.cParentID = parentID;
	}

	public String getDescricao() {
		return cDescricao;
	}

	public void setDescricao(String cDescricao) {
		this.cDescricao = cDescricao;
	}

	public String getRootId() {
		return cRootId;
	}

	public void setRootId(String cRootId) {
		this.cRootId = cRootId;
	}

}