package kpi.swing.espinhapeixe;

import javax.swing.tree.DefaultMutableTreeNode;

public class KpiEspinhaPeixeRel extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String recordType = new String("ESP_PEIXE_REL");
    private String formType = recordType;	 //Tipo do Formulario
    private String parentType = new String("");//Tipo formulario pai, caso haja.
    private kpi.swing.KpiDefaultDialogSystem dialog = kpi.core.KpiStaticReferences.getKpiDialogSystem();
    private boolean lZoomEvent = false;
    int nZoom = 60;

    public void enableFields() {
        // Habilita os campos do formul�rio.
    }

    public void disableFields() {
        // Desabilita os campos do formul�rio.
    }

    public void refreshFields() {
    }

    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        return recordAux;
    }

    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    /*public boolean hasCombos() {
    return false;
    }*/
    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();
        pnlCenter = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnReload = new javax.swing.JButton();
        btnExportacao = new javax.swing.JButton();
        btnAbrir = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        lblZoom = new javax.swing.JLabel();
        cbbZoom = new javax.swing.JComboBox();
        scrlEspPeixe = new javax.swing.JScrollPane();
        pnlEspinhaPeixe = new kpi.swing.espinhapeixe.KpiEspinhaPeixePainel();
        pnlBottomForm = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle(java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeRel_00001"));
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_epinhapeixe.gif")));
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(600, 358));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pnlCenter.setLayout(new java.awt.BorderLayout());

        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 29));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 32));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(300, 32));
        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(300, 32));
        btnReload.setFont(new java.awt.Font("Dialog", 0, 12));
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif")));
        btnReload.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeRel_00002"));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });

        tbInsertion.add(btnReload);

        btnExportacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_exportacao.gif")));
        btnExportacao.setText(java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeRel_00003"));
        btnExportacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportacaoActionPerformed(evt);
            }
        });

        tbInsertion.add(btnExportacao);

        btnAbrir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_download.gif")));
        btnAbrir.setText(java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeRel_00004"));
        btnAbrir.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });

        tbInsertion.add(btnAbrir);

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator1.setMaximumSize(new java.awt.Dimension(8, 15));
        jSeparator1.setPreferredSize(new java.awt.Dimension(25, 15));
        tbInsertion.add(jSeparator1);

        lblZoom.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblZoom.setText(java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeRel_00005"));
        lblZoom.setMaximumSize(new java.awt.Dimension(40, 14));
        lblZoom.setMinimumSize(new java.awt.Dimension(40, 14));
        lblZoom.setPreferredSize(new java.awt.Dimension(40, 14));
        tbInsertion.add(lblZoom);

        cbbZoom.setEditable(true);
        cbbZoom.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "200%", "150%", "130%", "100%", "90%", "80%", "70%", "60%", "55%", "45%", "40%", "30%" }));
        cbbZoom.setMaximumSize(new java.awt.Dimension(55, 22));
        cbbZoom.setMinimumSize(new java.awt.Dimension(50, 22));
        cbbZoom.setPreferredSize(new java.awt.Dimension(50, 22));
        cbbZoom.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbZoomItemStateChanged(evt);
            }
        });

        tbInsertion.add(cbbZoom);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.NORTH);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.NORTH);

        pnlCenter.add(pnlTools, java.awt.BorderLayout.NORTH);

        scrlEspPeixe.setPreferredSize(new java.awt.Dimension(100, 200));
        scrlEspPeixe.setViewportView(pnlEspinhaPeixe);

        pnlCenter.add(scrlEspPeixe, java.awt.BorderLayout.CENTER);

        pnlBottomForm.setLayout(null);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(10, 22));
        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 195), 2));
        jLabel1.setOpaque(true);
        pnlBottomForm.add(jLabel1);
        jLabel1.setBounds(430, 7, 10, 10);

        jLabel2.setBackground(new java.awt.Color(255, 255, 51));
        jLabel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel2.setOpaque(true);
        pnlBottomForm.add(jLabel2);
        jLabel2.setBounds(175, 7, 10, 10);

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 153), 2));
        jLabel3.setOpaque(true);
        pnlBottomForm.add(jLabel3);
        jLabel3.setBounds(265, 7, 10, 10);

        jLabel4.setText(java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeRel_00007"));
        pnlBottomForm.add(jLabel4);
        jLabel4.setBounds(190, 5, 50, 14);

        jLabel5.setText(java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeRel_00008"));
        pnlBottomForm.add(jLabel5);
        jLabel5.setBounds(280, 5, 130, 14);

        jLabel6.setText(java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeRel_00009"));
        pnlBottomForm.add(jLabel6);
        jLabel6.setBounds(445, 5, 130, 14);

        jLabel7.setText(java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeRel_00006"));
        pnlBottomForm.add(jLabel7);
        jLabel7.setBounds(0, 5, 60, 14);

        jLabel8.setBackground(new java.awt.Color(255, 255, 153));
        jLabel8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel8.setOpaque(true);
        pnlBottomForm.add(jLabel8);
        jLabel8.setBounds(65, 7, 10, 10);

        jLabel9.setText(java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeRel_00014"));
        pnlBottomForm.add(jLabel9);
        jLabel9.setBounds(80, 5, 90, 14);

        pnlCenter.add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        getContentPane().add(pnlCenter, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void cbbZoomItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbZoomItemStateChanged
            if (lZoomEvent && evt.getStateChange() == evt.SELECTED) {
                String cZoom = (String) cbbZoom.getSelectedItem();
                int newZoom = 100;
                try {
                    newZoom = Integer.parseInt(cZoom.replace("%", "").trim());
                    if (newZoom > 200 || newZoom < 30) {
                        //"O n�mero deve estar entre 30 e 200."
                        dialog.warningMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeRel_00011"));
                    } else {
                        nZoom = newZoom;
                    }
                } catch (NumberFormatException ex) {
                    //"Esta n�o � uma medida v�lida."
                    dialog.warningMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEspinhaPeixeRel_00012"));
                }


                pnlEspinhaPeixe.buildEspPeixe(this.tree, nZoom);
                pnlEspinhaPeixe.repaint();
                pnlEspinhaPeixe.revalidate();
                cbbZoom.setSelectedItem(String.valueOf(nZoom).concat("%"));
            }
	}//GEN-LAST:event_cbbZoomItemStateChanged

	private void btnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed
            kpi.swing.KpiFileChooser frmChooser = new kpi.swing.KpiFileChooser(kpi.core.KpiStaticReferences.getKpiMainPanel(), true);
            frmChooser.setDialogType(frmChooser.KPI_DIALOG_OPEN);

            frmChooser.setFileType("*.jpg");
            frmChooser.loadServerFiles("graphs\\*.jpg");

            frmChooser.setVisible(true);

            frmChooser.setUrlPath("/graphs/");
            frmChooser.openFile();

	}//GEN-LAST:event_btnAbrirActionPerformed

	private void btnExportacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportacaoActionPerformed
            //Local para grava��o do arquivo.
            kpi.swing.KpiFileChooser frmChooser = new kpi.swing.KpiFileChooser(kpi.core.KpiStaticReferences.getKpiMainPanel(), true);
            frmChooser.setDialogType(frmChooser.KPI_DIALOG_SAVE);
            frmChooser.setFileType("*.jpg");
            frmChooser.loadServerFiles("graphs\\*.jpg");
            frmChooser.setFileName("EspinhaPeixe.jpg");
            frmChooser.setVisible(true);

            if (frmChooser.isValidFile()) {
                kpi.util.KpiToolKit tool = new kpi.util.KpiToolKit();
                java.awt.image.BufferedImage myImage;
                myImage = new java.awt.image.BufferedImage(pnlEspinhaPeixe.getWidth(), pnlEspinhaPeixe.getHeight(), java.awt.image.BufferedImage.TYPE_INT_RGB);

                java.awt.Graphics2D g2 = myImage.createGraphics();

                //Desenhando a imagem
                pnlEspinhaPeixe.doLayout();
                pnlEspinhaPeixe.paint(g2);

                //Gravando a imagem.
                tool.writeJPEGFile(myImage, "graphs//" + frmChooser.getFileName());
            }
	}//GEN-LAST:event_btnExportacaoActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            //event.loadRecord( );
            pnlEspinhaPeixe.buildEspPeixe(this.tree, nZoom);
            pnlEspinhaPeixe.repaint();
            pnlEspinhaPeixe.revalidate();
            cbbZoom.setSelectedItem(String.valueOf(nZoom).concat("%"));
	}//GEN-LAST:event_btnReloadActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAbrir;
    private javax.swing.JButton btnExportacao;
    private javax.swing.JButton btnReload;
    private javax.swing.JComboBox cbbZoom;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblZoom;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlCenter;
    private kpi.swing.espinhapeixe.KpiEspinhaPeixePainel pnlEspinhaPeixe;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JScrollPane scrlEspPeixe;
    private javax.swing.JToolBar tbInsertion;
    // End of variables declaration//GEN-END:variables
    private javax.swing.JTree tree;
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiEspinhaPeixeRel(int operation, String idAux, String contextId) {
        initComponents();

        //Para iniciar a tela maximizada
        putClientProperty("MAXI", true);

        //event.defaultConstructor( operation, idAux, contextId );
    }

    public void buildRel(javax.swing.JTree tree) {
        this.tree = tree;
        cbbZoom.setSelectedItem(String.valueOf(nZoom).concat("%"));
        pnlEspinhaPeixe.buildEspPeixe(this.tree, nZoom);
        lZoomEvent = true;
    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(recordType);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        pnlOperation.setVisible(false);
    }

    public void showOperationsButtons() {
        pnlOperation.setVisible(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public String getFormType() {
        return formType;
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return new javax.swing.JTabbedPane();
    }

    public String getParentType() {
        return parentType;
    }
}
