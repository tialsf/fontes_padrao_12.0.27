package kpi.swing.framework;

import java.awt.event.ItemEvent;
import java.util.ArrayList;
import kpi.core.KpiDataController;
import kpi.core.KpiStaticReferences;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

/**
 * @author valdineyg
 */
public class KpiBuscaIndicador extends javax.swing.JDialog {

    private final int EXPRESSAO = 0;
    private final int FILHO = 1;
    //
    private KpiDataController dataController = KpiStaticReferences.getKpiDataController();
    private BIXMLVector disponivel = null;
    private BIXMLVector escolhido = null;
    private KpiIndicador formulario = null;
    private String filtro = "";

    public KpiBuscaIndicador(java.awt.Container pai, String titulo, boolean modal, KpiIndicador formulario, String filtro, boolean consolidador) {
        super();
        initComponents();
        this.setTitle(titulo);
        this.setSize(800, 500);
        this.setLocationRelativeTo(pai);
        this.formulario = formulario;
        this.filtro = filtro;
        this.selecaoMultipla(true);
        this.montaExpressao();
        this.aplicaRegra(consolidador);
        this.aplicaFiltro(EXPRESSAO);
    }

    /**
     * Monta o texto informativo da express�o de filtro em uso.
     */
    private void montaExpressao() {
        rbExpressao.setText("Indicadores cujo nome contenha a express�o '" + filtro + "'");
    }

    /**
     * Monta a express�o SQL a ser utilizada como filtro para listar indicadores.
     * @param filtro
     * @return
     */
    private String montaFiltro(int filtro) {
        StringBuilder parametros = new StringBuilder();

        if (filtro == EXPRESSAO) {
            parametros.append(" NOME LIKE '%");
            parametros.append(this.filtro);
            parametros.append("%'");
            parametros.append(" AND ");
            parametros.append(" ID <> '");
            parametros.append(formulario.getID());
            parametros.append("',");
            parametros.append("LST_IND_SCORECARD");
        } else {
            parametros.append(" ID_INDICA = '");
            parametros.append(formulario.getID());
            parametros.append("',");
            parametros.append("LST_IND_SCORECARD");
        }

        return parametros.toString();
    }

    /**
     * Insere o operador selecionado na f�rmula.
     * @return
     */
    private KpiItemFormula montaOperador() {
        String operador = "";

        if (rbSomar.isSelected()) {
            operador = "+";
        } else {
            operador = "-";
        }
        return new KpiItemFormula(operador, operador);
    }

    /**
     * Aplica a regra de consolida��o na qual os indicadores podem apenas ser somados.
     * @param consolidador
     */
    private void aplicaRegra(boolean consolidador) {
        rbSubtrair.setEnabled(!consolidador);
    }

    /**
     * Aplica o a expressao de filtro na listagem dos indicadores
     * @param filtro
     */
    private void aplicaFiltro(int filtro) {
        ArrayList escolhidos = new ArrayList();
        BIXMLRecord indicadores = dataController.listRecords("INDICADOR", montaFiltro(filtro));
        disponivel = indicadores.getBIXMLVector("INDICADORES");

        if (disponivel != null) {

            if (escolhido == null) {
                escolhido = new BIXMLVector("ESCOLHIDO", "ESCOLHIDOS");
                escolhido.setAttributes(disponivel.getAttributes());
            } else {
                for (int i = 0; i < escolhido.size(); i++) {
                    escolhidos.add(escolhido.get(i));
                }
            }

            disponivel.removeAll(escolhidos);

            tblEscolhidos.setDataSource(escolhido);
            tblDisponiveis.setDataSource(disponivel);

            formataTabela();
        }
    }

    /**
     * Habilita a sele��o m�ltipla de indicadores na tabela.
     * @param habilita
     */
    private void selecaoMultipla(boolean habilita) {
        if (habilita) {
            tblEscolhidos.getTable().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            tblDisponiveis.getTable().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        } else {
            tblEscolhidos.getTable().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);
            tblDisponiveis.getTable().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        }
    }

    /**
     * Formata a lista de indicadores.
     */
    private void formataTabela() {
        tblEscolhidos.getTable().getColumnModel().getColumn(1).setPreferredWidth(200);
        tblDisponiveis.getTable().getColumnModel().getColumn(1).setPreferredWidth(200);
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {//GEN-BEGIN:initComponents

        grpFiltros = new javax.swing.ButtonGroup();
        grpOperador = new javax.swing.ButtonGroup();
        pnlCenter = new javax.swing.JPanel();
        pnlCenterTop = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        rbExpressao = new javax.swing.JRadioButton();
        rbIndicadoresFilhos = new javax.swing.JRadioButton();
        pnlCenterCenter = new javax.swing.JPanel();
        pnlCenterCenterFirst = new javax.swing.JPanel();
        lblDisponiveis = new javax.swing.JLabel();
        tblDisponiveis = new kpi.beans.JBIXMLTable();
        pnlCenterCenterCenter = new javax.swing.JPanel();
        tlbAdd = new javax.swing.JToolBar();
        btnAdd = new javax.swing.JButton();
        tblRemove = new javax.swing.JToolBar();
        btnRemove = new javax.swing.JButton();
        pnlCenterCenterLast = new javax.swing.JPanel();
        lblEscolhidos = new javax.swing.JLabel();
        tblEscolhidos = new kpi.beans.JBIXMLTable();
        pnlCenterBotton = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        rbSomar = new javax.swing.JRadioButton();
        rbSubtrair = new javax.swing.JRadioButton();
        pnlCenterLast = new javax.swing.JPanel();
        pnlCenterFirst = new javax.swing.JPanel();
        pnlBotton = new javax.swing.JPanel();
        btnOk = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();

        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setModal(true);
        setResizable(false);

        pnlCenter.setMinimumSize(new java.awt.Dimension(0, 0));
        pnlCenter.setPreferredSize(new java.awt.Dimension(539, 426));
        pnlCenter.setRequestFocusEnabled(false);
        pnlCenter.setLayout(new java.awt.BorderLayout());

        pnlCenterTop.setPreferredSize(new java.awt.Dimension(80, 80));
        pnlCenterTop.setLayout(null);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)), "Filtro"));
        jPanel1.setLayout(null);

        grpFiltros.add(rbExpressao);
        rbExpressao.setSelected(true);
        rbExpressao.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rbExpressaoItemStateChanged(evt);
            }
        });
        jPanel1.add(rbExpressao);
        rbExpressao.setBounds(20, 13, 750, 20);

        grpFiltros.add(rbIndicadoresFilhos);
        rbIndicadoresFilhos.setText("Indicadores Filhos");
        rbIndicadoresFilhos.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rbIndicadoresFilhosItemStateChanged(evt);
            }
        });
        jPanel1.add(rbIndicadoresFilhos);
        rbIndicadoresFilhos.setBounds(20, 32, 480, 20);

        pnlCenterTop.add(jPanel1);
        jPanel1.setBounds(10, 10, 780, 60);

        pnlCenter.add(pnlCenterTop, java.awt.BorderLayout.NORTH);

        pnlCenterCenter.setMinimumSize(new java.awt.Dimension(0, 0));
        pnlCenterCenter.setLayout(new javax.swing.BoxLayout(pnlCenterCenter, javax.swing.BoxLayout.LINE_AXIS));

        pnlCenterCenterFirst.setMinimumSize(new java.awt.Dimension(0, 0));
        pnlCenterCenterFirst.setLayout(new java.awt.BorderLayout());

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        lblDisponiveis.setText(bundle.getString("JBISelectionDialog_00003")); // NOI18N
        pnlCenterCenterFirst.add(lblDisponiveis, java.awt.BorderLayout.PAGE_START);
        pnlCenterCenterFirst.add(tblDisponiveis, java.awt.BorderLayout.CENTER);

        pnlCenterCenter.add(pnlCenterCenterFirst);

        pnlCenterCenterCenter.setPreferredSize(new java.awt.Dimension(50, 50));
        pnlCenterCenterCenter.setLayout(null);

        tlbAdd.setFloatable(false);

        btnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_botaoavancar.gif"))); // NOI18N
        btnAdd.setMaximumSize(new java.awt.Dimension(25, 25));
        btnAdd.setMinimumSize(new java.awt.Dimension(25, 25));
        btnAdd.setPreferredSize(new java.awt.Dimension(25, 25));
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        tlbAdd.add(btnAdd);

        pnlCenterCenterCenter.add(tlbAdd);
        tlbAdd.setBounds(10, 110, 30, 30);

        tblRemove.setFloatable(false);

        btnRemove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_botaovoltar.gif"))); // NOI18N
        btnRemove.setMaximumSize(new java.awt.Dimension(25, 25));
        btnRemove.setMinimumSize(new java.awt.Dimension(25, 25));
        btnRemove.setPreferredSize(new java.awt.Dimension(25, 25));
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });
        tblRemove.add(btnRemove);

        pnlCenterCenterCenter.add(tblRemove);
        tblRemove.setBounds(10, 140, 30, 30);

        pnlCenterCenter.add(pnlCenterCenterCenter);

        pnlCenterCenterLast.setMinimumSize(new java.awt.Dimension(0, 0));
        pnlCenterCenterLast.setLayout(new java.awt.BorderLayout());

        lblEscolhidos.setText(bundle.getString("JBISelectionDialog_00001")); // NOI18N
        pnlCenterCenterLast.add(lblEscolhidos, java.awt.BorderLayout.PAGE_START);
        pnlCenterCenterLast.add(tblEscolhidos, java.awt.BorderLayout.CENTER);

        pnlCenterCenter.add(pnlCenterCenterLast);

        pnlCenter.add(pnlCenterCenter, java.awt.BorderLayout.CENTER);

        pnlCenterBotton.setMinimumSize(new java.awt.Dimension(0, 0));
        pnlCenterBotton.setPreferredSize(new java.awt.Dimension(80, 80));
        pnlCenterBotton.setLayout(null);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)), "Opera��o"));
        jPanel2.setLayout(null);

        grpOperador.add(rbSomar);
        rbSomar.setSelected(true);
        rbSomar.setText("Somar Indicadores");
        jPanel2.add(rbSomar);
        rbSomar.setBounds(20, 13, 400, 20);

        grpOperador.add(rbSubtrair);
        rbSubtrair.setText("Subtrair Indicadores");
        jPanel2.add(rbSubtrair);
        rbSubtrair.setBounds(20, 32, 400, 20);

        pnlCenterBotton.add(jPanel2);
        jPanel2.setBounds(10, 10, 780, 60);

        pnlCenter.add(pnlCenterBotton, java.awt.BorderLayout.PAGE_END);

        pnlCenterLast.setMinimumSize(new java.awt.Dimension(0, 0));
        pnlCenterLast.setPreferredSize(new java.awt.Dimension(10, 341));
        pnlCenterLast.setLayout(null);
        pnlCenter.add(pnlCenterLast, java.awt.BorderLayout.LINE_END);

        pnlCenterFirst.setPreferredSize(new java.awt.Dimension(10, 341));
        pnlCenterFirst.setLayout(null);
        pnlCenter.add(pnlCenterFirst, java.awt.BorderLayout.LINE_START);

        getContentPane().add(pnlCenter, java.awt.BorderLayout.CENTER);

        pnlBotton.setMinimumSize(new java.awt.Dimension(0, 0));
        pnlBotton.setPreferredSize(new java.awt.Dimension(30, 30));

        btnOk.setText("Ok");
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });
        pnlBotton.add(btnOk);

        btnCancelar.setText(bundle.getString("JBISelectionDialog_00002")); // NOI18N
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        pnlBotton.add(btnCancelar);

        getContentPane().add(pnlBotton, java.awt.BorderLayout.SOUTH);

        pack();
    }//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        int[] selecionados = tblDisponiveis.getSelectedRows();
        ArrayList listRemove = new ArrayList();

        for (int i = 0; i < selecionados.length; i++) {

            escolhido.add(disponivel.get(selecionados[i]));
            listRemove.add(disponivel.get(selecionados[i]));
        }

        disponivel.removeAll(listRemove);
        tblEscolhidos.setDataSource(escolhido);
        tblDisponiveis.setDataSource(disponivel);

        formataTabela();
}//GEN-LAST:event_btnAddActionPerformed

    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
        int[] selecionados = tblEscolhidos.getSelectedRows();
        ArrayList listRemove = new ArrayList();

        for (int i = 0; i < selecionados.length; i++) {
            disponivel.add(escolhido.get(selecionados[i]));
            listRemove.add(escolhido.get(selecionados[i]));
        }

        escolhido.removeAll(listRemove);
        tblEscolhidos.setDataSource(escolhido);
        tblDisponiveis.setDataSource(disponivel);

        formataTabela();
}//GEN-LAST:event_btnRemoveActionPerformed

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed
        if (escolhido != null) {
            for (int i = 0; i < escolhido.size(); i++) {
                String id_indicador = escolhido.get(i).getString("ID");
                String nome_indicador = escolhido.get(i).getString("NOME");
                String id_scorecard = escolhido.get(i).getString("ID_SCOREC");

                KpiItemFormula formula = new KpiItemFormula(id_indicador, nome_indicador);
                formula.setITipo(KpiItemFormula.INDICADOR);
                formula.setIdScoreCard(id_scorecard);
                formulario.addFormula(formula);

                if (i + 1 < escolhido.size()) {
                    formulario.addFormula(montaOperador());
                }
            }
        }

        dispose();
}//GEN-LAST:event_btnOkActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        dispose();
}//GEN-LAST:event_btnCancelarActionPerformed

    private void rbExpressaoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rbExpressaoItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            aplicaFiltro(EXPRESSAO);
        }
    }//GEN-LAST:event_rbExpressaoItemStateChanged

    private void rbIndicadoresFilhosItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rbIndicadoresFilhosItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            aplicaFiltro(FILHO);
        }
    }//GEN-LAST:event_rbIndicadoresFilhosItemStateChanged
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnOk;
    private javax.swing.JButton btnRemove;
    private javax.swing.ButtonGroup grpFiltros;
    private javax.swing.ButtonGroup grpOperador;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblDisponiveis;
    private javax.swing.JLabel lblEscolhidos;
    private javax.swing.JPanel pnlBotton;
    private javax.swing.JPanel pnlCenter;
    private javax.swing.JPanel pnlCenterBotton;
    private javax.swing.JPanel pnlCenterCenter;
    private javax.swing.JPanel pnlCenterCenterCenter;
    private javax.swing.JPanel pnlCenterCenterFirst;
    private javax.swing.JPanel pnlCenterCenterLast;
    private javax.swing.JPanel pnlCenterFirst;
    private javax.swing.JPanel pnlCenterLast;
    private javax.swing.JPanel pnlCenterTop;
    private javax.swing.JRadioButton rbExpressao;
    private javax.swing.JRadioButton rbIndicadoresFilhos;
    private javax.swing.JRadioButton rbSomar;
    private javax.swing.JRadioButton rbSubtrair;
    kpi.beans.JBIXMLTable tblDisponiveis;
    kpi.beans.JBIXMLTable tblEscolhidos;
    private javax.swing.JToolBar tblRemove;
    private javax.swing.JToolBar tlbAdd;
    // End of variables declaration//GEN-END:variables
}
