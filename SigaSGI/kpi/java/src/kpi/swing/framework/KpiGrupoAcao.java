package kpi.swing.framework;

import java.awt.event.ItemEvent;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import kpi.beans.JBITextArea;
import kpi.beans.JBIXMLTable;
import kpi.core.*;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

public class KpiGrupoAcao extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    public final static int COL_STATUS_PLANO = 6;
    private String recordType = "GRUPO_ACAO"; //NOI18N
    private String formType = recordType;
    private String parentType = "LSTGRUPO_ACAO"; //NOI18N
    private KpiGrupoAcaoTableCellRenderer cellRenderer;
    private kpi.beans.JBIXMLTable listaTable;
    private kpi.xml.BIXMLVector xmlCausa;
    private kpi.xml.BIXMLVector diagramaCausaEfeito;
    java.util.Hashtable htbResponsavel = new java.util.Hashtable();
    java.util.Hashtable htbDiagrama = new java.util.Hashtable();
    java.util.Hashtable htbCausa = new java.util.Hashtable();
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private kpi.core.KpiFormController formController = kpi.core.KpiStaticReferences.getKpiFormController();
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;
    private java.util.Hashtable htbStatus = new java.util.Hashtable();
    private String statusAnterior;

    public KpiGrupoAcao(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true); //NOI18N

        event.registerEvent(KpiDefaultFrameBehavior.EVENT_ON_UPDATE, new KpiEventTask(this, "afterUpdate", null, null)); //NOI18N
        event.registerEvent(KpiDefaultFrameBehavior.EVENT_ON_INSERT, new KpiEventTask(this, "afterInsert", null, null)); //NOI18N

        event.defaultConstructor(operation, idAux, contextId);

        KpiMenuController mnuControl = KpiStaticReferences.getMnuController();
        IteMnuPermission perm = mnuControl.getRuleByName("PLANOSACAO"); //NOI18N

        //SEGURANCA
        if (perm.getPermissionValue(IteMnuPermission.TipoPermissao.VISUALIZACAO) != IteMnuPermission.Permissao.PERMITIDO) {
            tapCadastro.remove(listaPlanos);
        }
    }

    @Override
    public void enableFields() {
        event.setEnableFields(pnlData, true);

        if (tapCadastro.getTabCount() > 1) {
            tapCadastro.setEnabledAt(1, false);
        }

        if (record.getBoolean("CTR_APROV_PLANACAO")) { //NOI18N
            if (getStatus() == KpiDefaultFrameBehavior.INSERTING) {
                cbStatus.setEnabled(false);
            }

            //se o status do plano for diferente de "N�o Iniciado", bloquear todos os campos exceto "Status" e "Obs"
            if (!record.getString("STATUS").equals(KpiStaticReferences.GRUPOACAO_NAO_INICIADO) && !record.getBoolean("ISADMIN")) { //NOI18N

                event.setEnableFields(pnlData, false);
                cbStatus.setEnabled(true);
                txtObservacao.setEnabled(true);

            }
        }
    }

    @Override
    public void disableFields() {
        event.setEnableFields(pnlData, false);
        listaPlanos.btnView.setEnabled(true);
        if (tapCadastro.getTabCount() > 1) {
            tapCadastro.setEnabledAt(1, true);
        }
    }

    @Override
    public void refreshFields() {
        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        listaPlanos.setDataSource(record.getBIXMLVector("PLANOSACAO"), record.getString("ID") + "|3", "", this.event); //NOI18N
        event.populateCombo(record.getBIXMLVector("LSTSTATUS"), "STATUS", htbStatus, cbStatus); //NOI18N
        txtObservacao.setText(record.getString("OBSERVACAO"));

        //Recupera a lista de usu�rios.
        BIXMLVector vctUsuarios = record.getBIXMLVector("RESPONSAVEIS"); //NOI18N
        BIXMLVector vctTreeUser = record.getBIXMLVector("TREEUSERS"); //NOI18N

        //Responsavel pelo indicador
        event.populateCombo(vctUsuarios, "TP_RESP_ID_RESP", htbResponsavel, cbResponsavel); //NOI18N
        event.selectComboItem(cbResponsavel, event.getComboItem(vctUsuarios, record.getString("TP_RESP_ID_RESP"), true));
        tsTreeResp.setVetor(vctUsuarios);
        tsTreeResp.setTree(vctTreeUser);

        setEspPeixe(record.getString("ID_EFEITO"), record.getString("ID_CAUSA"));
        setRenderTable();
        listaPlanos.xmlTable.getTable().getColumnModel().removeColumn(listaPlanos.xmlTable.getColumn(1));
        listaPlanos.setContextId(getContext());

        this.lblObservacaoChar.setText(doMaxChar(this.txtObservacao, 255));
        statusAnterior = record.getString("STATUS");
        event.selectComboItem(cbStatus, event.getComboItem(record.getBIXMLVector("LSTSTATUS"), record.getString("STATUS"), true)); //NOI18N

        cbStatus.removeItemAt(0);

        //somente admin e responsavel podem alterar o plano
        if (record.getBoolean("CTR_APROV_PLANACAO")) { //NOI18N
            if (record.getBoolean("ISADMIN") || record.getBoolean("ISRESP")) { //NOI18N

                btnEdit.setEnabled(true);
                btnDelete.setEnabled(true);

            } else {
                btnEdit.setEnabled(false);
                btnDelete.setEnabled(false);

            }
        } else {
            btnEdit.setEnabled(true);
            btnDelete.setEnabled(true);
        }
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id); //NOI18N
        recordAux.set("NOME", fldNome.getText()); //NOI18N
        recordAux.set("DESCRICAO", txtDescricao.getText()); //NOI18N

        if (event.getComboValue(htbResponsavel, cbResponsavel).substring(0, 1).equals("0")) { //NOI18N
            recordAux.set("TP_RESP", ""); //NOI18N
            recordAux.set("ID_RESP", ""); //NOI18N
        } else {
            recordAux.set("TP_RESP", event.getComboValue(htbResponsavel, cbResponsavel).substring(0, 1)); //NOI18N
            recordAux.set("ID_RESP", event.getComboValue(htbResponsavel, cbResponsavel).substring(1)); //NOI18N
        }

        recordAux.set("ID_EFEITO", event.getComboValue(htbDiagrama, cbDiagrama)); //NOI18N
        recordAux.set("ID_CAUSA", event.getComboValue(htbCausa, cbCausa)); //NOI18N
        recordAux.set("STATUS", event.getComboValue(htbStatus, cbStatus, true)); //NOI18N
        recordAux.set("OBSERVACAO", txtObservacao.getText()); //NOI18N


        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        if (fldNome.getText().trim().equals("")) { //NOI18N
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiProjeto_00007"));
            retorno = false;
        }
        if (!retorno) {
            btnSave.setEnabled(true);
        }
        return retorno;
    }

    /**
     *
     * @return
     */
    private String getContext() {
        StringBuilder ret = new StringBuilder();
        int nItem = -1;

        nItem = cbDiagrama.getSelectedIndex() - 1;
        if (nItem != -1 && diagramaCausaEfeito != null) {
            BIXMLRecord rec = diagramaCausaEfeito.get(nItem);
            ret.append("COMMAND|"); //NOI18N
            ret.append(rec.getString("ID_SCO"));
            ret.append("|"); //NOI18N
            ret.append(rec.getString("ID_IND"));
        } else {
            ret.append(""); //NOI18N
        }
        return ret.toString();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        fldNome = new pv.jfcx.JPVEdit();
        lblDescricao = new javax.swing.JLabel();
        lblResponsavel = new javax.swing.JLabel();
        cbResponsavel = new javax.swing.JComboBox();
        lblCausa = new javax.swing.JLabel();
        cbCausa = new javax.swing.JComboBox();
        lblEfeito = new javax.swing.JLabel();
        cbDiagrama = new javax.swing.JComboBox();
        lblDescricaoCausa = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtDescricao = new kpi.beans.JBITextArea();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtCausa = new kpi.beans.JBITextArea();
        tbCausaEfeito = new javax.swing.JToolBar();
        cmbCausaEfeito = new javax.swing.JButton();
        tsTreeResp = new kpi.beans.JBITreeSelection(kpi.beans.JBITreeSelection.TYPE_DEFAULT);
        scrlObservacao = new javax.swing.JScrollPane();
        txtObservacao = new kpi.beans.JBITextArea();
        lblObservacao = new javax.swing.JLabel();
        lblObservacaoChar = new javax.swing.JLabel();
        cbStatus = new javax.swing.JComboBox();
        lblStatus = new javax.swing.JLabel();
        pnlTopRecord = new javax.swing.JPanel();
        listaPlanos = new kpi.beans.JBISeekListPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnAjuda2 = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiGrupoAcao_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_grupoacao.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(455, 459));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlFields.setLayout(new java.awt.BorderLayout());
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);

        pnlData.setPreferredSize(new java.awt.Dimension(400, 200));
        pnlData.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("KpiGrupoAcao_00003")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(10, 10, 85, 20);

        fldNome.setMaxLength(255);
        pnlData.add(fldNome);
        fldNome.setBounds(10, 30, 400, 22);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international"); // NOI18N
        lblDescricao.setText(bundle1.getString("KpiPlanoDeAcao_00057")); // NOI18N
        lblDescricao.setEnabled(false);
        lblDescricao.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblDescricao);
        lblDescricao.setBounds(10, 90, 140, 20);

        lblResponsavel.setText(bundle.getString("KpiGrupoAcao_00004")); // NOI18N
        lblResponsavel.setEnabled(false);
        lblResponsavel.setFocusable(false);
        lblResponsavel.setPreferredSize(new java.awt.Dimension(55, 15));
        pnlData.add(lblResponsavel);
        lblResponsavel.setBounds(10, 50, 85, 20);

        cbResponsavel.setEnabled(false);
        cbResponsavel.setPreferredSize(new java.awt.Dimension(10, 10));
        pnlData.add(cbResponsavel);
        cbResponsavel.setBounds(10, 70, 400, 22);

        lblCausa.setText(bundle1.getString("KpiPlanoDeAcao_00008")); // NOI18N
        lblCausa.setEnabled(false);
        lblCausa.setFocusable(false);
        lblCausa.setPreferredSize(new java.awt.Dimension(55, 15));
        pnlData.add(lblCausa);
        lblCausa.setBounds(10, 220, 85, 20);

        cbCausa.setEnabled(false);
        cbCausa.setPreferredSize(new java.awt.Dimension(10, 10));
        cbCausa.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbCausaItemStateChanged(evt);
            }
        });
        pnlData.add(cbCausa);
        cbCausa.setBounds(10, 240, 400, 22);

        lblEfeito.setText(bundle1.getString("KpiEspinhaPeixeCadastro_00010")); // NOI18N
        lblEfeito.setEnabled(false);
        lblEfeito.setFocusable(false);
        lblEfeito.setPreferredSize(new java.awt.Dimension(55, 15));
        pnlData.add(lblEfeito);
        lblEfeito.setBounds(10, 180, 400, 20);

        cbDiagrama.setEnabled(false);
        cbDiagrama.setPreferredSize(new java.awt.Dimension(10, 10));
        cbDiagrama.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbDiagramaItemStateChanged(evt);
            }
        });
        pnlData.add(cbDiagrama);
        cbDiagrama.setBounds(10, 200, 400, 22);

        lblDescricaoCausa.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricaoCausa.setText(bundle1.getString("KpiPlanoDeAcao_00058")); // NOI18N
        lblDescricaoCausa.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDescricaoCausa.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDescricaoCausa.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblDescricaoCausa);
        lblDescricaoCausa.setBounds(10, 260, 110, 20);

        txtDescricao.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        txtDescricao.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        jScrollPane2.setViewportView(txtDescricao);

        pnlData.add(jScrollPane2);
        jScrollPane2.setBounds(10, 110, 400, 70);

        txtCausa.setEditable(false);
        txtCausa.setBackground(new java.awt.Color(236, 233, 216));
        txtCausa.setBorder(null);
        txtCausa.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        jScrollPane1.setViewportView(txtCausa);

        pnlData.add(jScrollPane1);
        jScrollPane1.setBounds(10, 280, 400, 70);

        tbCausaEfeito.setFloatable(false);
        tbCausaEfeito.setPreferredSize(new java.awt.Dimension(25, 25));

        cmbCausaEfeito.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_epinhapeixe.gif"))); // NOI18N
        cmbCausaEfeito.setToolTipText(bundle.getString("KpiEspinhaPeixeCadastro_00001")); // NOI18N
        cmbCausaEfeito.setMaximumSize(new java.awt.Dimension(22, 22));
        cmbCausaEfeito.setMinimumSize(new java.awt.Dimension(22, 22));
        cmbCausaEfeito.setPreferredSize(new java.awt.Dimension(22, 22));
        cmbCausaEfeito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCausaEfeitoActionPerformed(evt);
            }
        });
        tbCausaEfeito.add(cmbCausaEfeito);

        pnlData.add(tbCausaEfeito);
        tbCausaEfeito.setBounds(410, 200, 30, 20);

        tsTreeResp.setCombo(cbResponsavel);
        tsTreeResp.setRoot(false);
        pnlData.add(tsTreeResp);
        tsTreeResp.setBounds(410, 70, 25, 22);

        scrlObservacao.setAutoscrolls(true);

        txtObservacao.setRows(50);
        txtObservacao.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        txtObservacao.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtObservacao.setMaxLength(255);
        txtObservacao.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtObservacaoKeyReleased(evt);
            }
        });
        scrlObservacao.setViewportView(txtObservacao);

        pnlData.add(scrlObservacao);
        scrlObservacao.setBounds(230, 380, 180, 60);

        lblObservacao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblObservacao.setText(bundle1.getString("KpiGrupoAcao_00009")); // NOI18N
        lblObservacao.setEnabled(false);
        lblObservacao.setFocusable(false);
        lblObservacao.setMaximumSize(new java.awt.Dimension(100, 16));
        lblObservacao.setMinimumSize(new java.awt.Dimension(100, 16));
        lblObservacao.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblObservacao);
        lblObservacao.setBounds(230, 360, 70, 20);

        lblObservacaoChar.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblObservacaoChar.setForeground(new java.awt.Color(102, 102, 102));
        lblObservacaoChar.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        java.util.ResourceBundle bundle2 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        lblObservacaoChar.setText(bundle2.getString("KpiPlanoDeAcao_00054")); // NOI18N
        lblObservacaoChar.setFocusable(false);
        lblObservacaoChar.setMaximumSize(new java.awt.Dimension(100, 16));
        lblObservacaoChar.setMinimumSize(new java.awt.Dimension(100, 16));
        lblObservacaoChar.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblObservacaoChar);
        lblObservacaoChar.setBounds(290, 360, 120, 20);

        cbStatus.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbStatusItemStateChanged(evt);
            }
        });
        pnlData.add(cbStatus);
        cbStatus.setBounds(10, 380, 180, 22);

        lblStatus.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblStatus.setText(bundle2.getString("KpiGrupoAcao_00008")); // NOI18N
        lblStatus.setEnabled(false);
        lblStatus.setFocusable(false);
        lblStatus.setMaximumSize(new java.awt.Dimension(100, 16));
        lblStatus.setMinimumSize(new java.awt.Dimension(100, 16));
        lblStatus.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblStatus);
        lblStatus.setBounds(10, 360, 50, 20);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("KpiGrupoAcao_00002"), pnlRecord); // NOI18N
        tapCadastro.addTab(bundle1.getString("KpiGrupoAcao_00010"), listaPlanos); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 29));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle2.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle2.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setPreferredSize(new java.awt.Dimension(320, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle2.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle2.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle2.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnAjuda2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda2.setText(bundle.getString("KpiGraphIndicador_00011")); // NOI18N
        btnAjuda2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjuda2ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda2);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        getContentPane().add(pnlTools, java.awt.BorderLayout.NORTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnAjuda2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjuda2ActionPerformed
            event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjuda2ActionPerformed

	private void cbCausaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbCausaItemStateChanged
            if (evt.getStateChange() == ItemEvent.SELECTED && cbCausa.getItemCount() > 0) {
                refreshTxtCausa();
            }
	}//GEN-LAST:event_cbCausaItemStateChanged

	private void cbDiagramaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbDiagramaItemStateChanged
            if (evt.getStateChange() == ItemEvent.SELECTED && cbDiagrama.getItemCount() > 0) {
                StringBuilder selectInd = new StringBuilder("OWNER = '"); //NOI18N
                selectInd.append(event.getComboValue(htbDiagrama, cbDiagrama)).append("'"); //NOI18N

                kpi.xml.BIXMLRecord recFiltrados = event.listRecords("ESP_PEIXE_CAUSA", selectInd.toString()); //NOI18N
                xmlCausa = recFiltrados.getBIXMLVector("ESP_PEIXE_CAUSAS"); //NOI18N
                event.populateCombo(xmlCausa, "ESP_PEIXE_CAUSA", htbCausa, cbCausa); //NOI18N
                refreshTxtCausa();
                listaPlanos.setContextId(getContext());
            }
	}//GEN-LAST:event_cbDiagramaItemStateChanged

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            btnSave.setEnabled(false);
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            tapCadastro.setSelectedIndex(0);
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

        private void cmbCausaEfeitoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCausaEfeitoActionPerformed
            StringBuilder parametros = new StringBuilder();

            if (cbDiagrama.getSelectedIndex() > 0) {
                parametros.append(event.getComboValue(htbDiagrama, cbDiagrama));
                formController.getForm("ESP_PEIXE", parametros.toString(), cbDiagrama.getSelectedItem().toString()); //NOI18N
            }
        }//GEN-LAST:event_cmbCausaEfeitoActionPerformed

    private void txtObservacaoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtObservacaoKeyReleased
        this.lblObservacaoChar.setText(doMaxChar(this.txtObservacao, 255));
    }//GEN-LAST:event_txtObservacaoKeyReleased

    private void cbStatusItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbStatusItemStateChanged

        if (record.getBoolean("CTR_APROV_PLANACAO")) { //NOI18N
            if (evt.getStateChange() == java.awt.event.ItemEvent.SELECTED && record.getString("STATUS").equals(KpiStaticReferences.GRUPOACAO_APROVADO)) {
                String statusNovo = event.getComboValue(htbStatus, cbStatus, true);

                if (statusAnterior.equals(KpiStaticReferences.GRUPOACAO_APROVADO) && !statusAnterior.equals(statusNovo)) {
                    SwingUtilities.invokeLater(new Runnable() {

                        public void run() {
                            String text = java.util.ResourceBundle.getBundle("international").getString("KpiGrupoAcao_00007");

                            text = text.replaceAll("#XXX#", cbStatus.getItemAt(cbStatus.getSelectedIndex()).toString());

                            int option = JOptionPane.showConfirmDialog(null, text, java.util.ResourceBundle.getBundle("international").getString("KpiGrupoAcao_00001"), JOptionPane.OK_CANCEL_OPTION);

                            if (option == JOptionPane.CANCEL_OPTION) {
                                event.selectComboItem(cbStatus, event.getComboItem(record.getBIXMLVector("LSTSTATUS"), statusAnterior, false)); //NOI18N
                            } else {
                                statusAnterior = event.getComboValue(htbStatus, cbStatus, true);
                            }
                        }
                    });
                } else {
                    statusAnterior = statusNovo;
                }
            }
        }
    }//GEN-LAST:event_cbStatusItemStateChanged

    private String doMaxChar(JBITextArea oField, int nQtdMax) {

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // I18N

        StringBuilder oTxt = new StringBuilder();
        oTxt.append(bundle.getString("KpiPlanoDeAcao_00054")); //"Caracteres"
        oTxt.append(" ["); //NOI18N
        oTxt.append(String.valueOf(oField.getText().length()));
        oTxt.append(" "); //NOI18N
        oTxt.append(bundle.getString("KpiPlanoDeAcao_00055")); //"de"
        oTxt.append(" "); //NOI18N
        oTxt.append(nQtdMax);
        oTxt.append("]"); //NOI18N

        return oTxt.toString();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAjuda2;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cbCausa;
    private javax.swing.JComboBox cbDiagrama;
    private javax.swing.JComboBox cbResponsavel;
    private javax.swing.JComboBox cbStatus;
    private javax.swing.JButton cmbCausaEfeito;
    private pv.jfcx.JPVEdit fldNome;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblCausa;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblDescricaoCausa;
    private javax.swing.JLabel lblEfeito;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblObservacao;
    private javax.swing.JLabel lblObservacaoChar;
    private javax.swing.JLabel lblResponsavel;
    private javax.swing.JLabel lblStatus;
    private kpi.beans.JBISeekListPanel listaPlanos;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JScrollPane scrlObservacao;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbCausaEfeito;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private kpi.beans.JBITreeSelection tsTreeResp;
    private kpi.beans.JBITextArea txtCausa;
    private kpi.beans.JBITextArea txtDescricao;
    private kpi.beans.JBITextArea txtObservacao;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        btnSave.setEnabled(true);
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) { //NOI18N
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return ""; //NOI18N
        }
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    public void afterInsert() {
        KpiDataController dataController = kpi.core.KpiStaticReferences.getKpiDataController();

        if (dataController.getStatus() == KpiDataController.KPI_ST_OK) {

            loadRecord();

        }
    }

    public void afterUpdate() {
        KpiDataController dataController = kpi.core.KpiStaticReferences.getKpiDataController();

        if (dataController.getStatus() == KpiDataController.KPI_ST_OK) {

            loadRecord();

        }
    }

    public void setEspPeixe(String cIdEfeito, String cIdCausa) {
        int indice = -1;
        diagramaCausaEfeito = record.getBIXMLVector("ESP_PEIXES"); //NOI18N
        event.populateCombo(diagramaCausaEfeito, "ESP_PEIXE", htbDiagrama, cbDiagrama); //NOI18N
        indice = event.getComboItem(diagramaCausaEfeito, cIdEfeito, true);
        if (indice != -1 && cbDiagrama.getItemCount() > 0) {
            cbDiagrama.setSelectedIndex(indice);
            StringBuilder selectInd = new StringBuilder("OWNER = '"); //NOI18N
            selectInd.append(event.getComboValue(htbDiagrama, cbDiagrama)).append("'"); //NOI18N
            kpi.xml.BIXMLRecord recFiltrados = event.listRecords("ESP_PEIXE_CAUSA", selectInd.toString()); //NOI18N
            xmlCausa = recFiltrados.getBIXMLVector("ESP_PEIXE_CAUSAS"); //NOI18N
            event.populateCombo(xmlCausa, "ESP_PEIXE_CAUSA", htbCausa, cbCausa); //NOI18N
            event.selectComboItem(cbCausa, event.getComboItem(xmlCausa, cIdCausa, true));

            refreshTxtCausa();
        }
    }

    private void refreshTxtCausa() {
        int nItem = -1;
        nItem = cbCausa.getSelectedIndex() - 1;
        if (nItem != -1) {
            BIXMLRecord rec = xmlCausa.get(nItem);
            txtCausa.setText(rec.getString("DESCRICAO").replace("|", "\n")); //NOI18N
        } else {
            txtCausa.setText(""); //NOI18N
        }
    }

    public kpi.beans.JBIXMLTable getListaTable() {
        return listaTable;
    }

    public void setListaTable(kpi.beans.JBIXMLTable listaTable) {
        this.listaTable = listaTable;
    }

    private void setRenderTable() {
        String tipoColuna = null;
        listaPlanos.xmlTable.setHeaderHeight(20);
        listaPlanos.xmlTable.getColumn(0).setMaxWidth(30);
        setListaTable(listaPlanos.xmlTable);
        cellRenderer = new KpiGrupoAcaoTableCellRenderer(getListaTable());

        for (int col = 0; col < listaTable.getModel().getColumnCount(); col++) {
            tipoColuna = getListaTable().getColumnType(col);
            int colTipo = new Integer(tipoColuna).intValue();

            if (colTipo == JBIXMLTable.KPI_STRING) {
                getListaTable().getColumn(col).setCellRenderer(cellRenderer);
            }
        }
        listaPlanos.xmlTable.setSortColumn(1);
    }
}

class KpiGrupoAcaoTableCellRenderer extends javax.swing.table.DefaultTableCellRenderer {

    private kpi.beans.JBIXMLTable biTable = null;
    public final static java.awt.Color VERMELHO = new java.awt.Color(234, 106, 106);
    public final static java.awt.Color AMARELO = new java.awt.Color(255, 235, 155);
    public final static java.awt.Color VERDE = new java.awt.Color(139, 191, 150);
    public final static java.awt.Color CINZA = new java.awt.Color(228, 228, 228);

    public KpiGrupoAcaoTableCellRenderer() {
    }

    public KpiGrupoAcaoTableCellRenderer(kpi.beans.JBIXMLTable table) {
        biTable = table;
    }

    // Sobreposicao para mudar o comportamento de pintura da celula.
    @Override
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        pv.jfcx.JPVEdit oRender = new pv.jfcx.JPVEdit();
        BIXMLRecord recLinha = null;
        int linha = biTable.getOriginalRow(row);

        oRender.setLocale(kpi.core.KpiStaticReferences.getKpiDefaultLocale());
        oRender.setLAF(false);
        oRender.setBorderStyle(0);
        oRender.setFont(new java.awt.Font("Tahoma", 0, 11)); //NOI18N

        if (biTable != null) {
            column = biTable.getColumn(column).getModelIndex();
            recLinha = biTable.getXMLData().get(linha);
            renderString(oRender, value.toString(), column, row, recLinha);
        }

        String colName = biTable.getColumnTag(column);
        if (!colName.equals(KpiListaPlanos.COL_STATUS_PLANO)) {
            if (hasFocus) {
                oRender.setBackground(new java.awt.Color(212, 208, 200));
            } else if (table.getSelectedRow() == row) {
                oRender.setBackground(table.getSelectionBackground());
                oRender.setForeground(table.getSelectionForeground());
            }
        }
        return oRender;
    }

    private pv.jfcx.JPVEdit renderString(pv.jfcx.JPVEdit oRender, String valor, int column, int row, BIXMLRecord recLinha) {
        oRender.setValue(valor);

        String colName = biTable.getColumnTag(column);
        if (colName.equals(KpiListaPlanos.COL_STATUS_PLANO)) {

            int status = recLinha.getInt("STATUS_COLOR"); //NOI18N
            switch (status) {
                case 1:
                    oRender.setBackground(AMARELO);
                    break;
                case 2:
                    oRender.setBackground(java.awt.Color.white);
                    break;
                case 3:
                    oRender.setBackground(CINZA);
                    break;
                case 4:
                    oRender.setBackground(VERMELHO);
                    break;
                case 5:
                    oRender.setBackground(VERDE);
                    break;
            }
        }
        oRender.setAutoScroll(true);
        oRender.setLAF(false);

        return oRender;
    }
}
