package kpi.swing.framework;

import java.util.ArrayList;
import kpi.beans.JBIXMLTable;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

public class KpiGrupoIndicador extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    /**
     * ************************************************************************
     */
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /**
     * ************************************************************************
     */
    private String recordType = new String("GRUPO_IND");
    private String formType = recordType;
    private String parentType = new String("LSTGRUPO_IND");
    private BIXMLVector vctIndicadores = null;
    private BIXMLVector removedElements = null; //Itens removidos.
    private BIXMLVector insertedElements = null; //Itens inseridos.

    /**
     * Habilita os campos do formul�rio.
     */
    @Override
    public void enableFields() {
        lblNome.setEnabled(true);
        fldNome.setEnabled(true);
        txtDescricao.setEnabled(true);
        lblDescricao.setEnabled(true);
        jBIListIndicadores.xmlTable.setEnabled(true);
        jBIListIndicadores.btnNew.setEnabled(true);
        jBIListIndicadores.btnView.setEnabled(true);
        
        if (getStatus() != KpiDefaultFrameBehavior.INSERTING) {
            btnAdd.setEnabled(true);
            btnRemove.setEnabled(true);
            
            jBIListIndicadores.xmlTable.setEnabled(true);
            jBIListIndicadores.btnNew.setEnabled(true);
            jBIListIndicadores.btnView.setEnabled(true);
            
            tapCadastro.setEnabledAt(1, true);
        }else{
            btnAdd.setEnabled(false);
            btnRemove.setEnabled(false);
            
            jBIListIndicadores.xmlTable.setEnabled(false);
            jBIListIndicadores.btnNew.setEnabled(false);
            jBIListIndicadores.btnView.setEnabled(false);
            
            tapCadastro.setEnabledAt(1, false);
        }
    }

    /**
     * Desabilita os campos do formul�rio.
     */
    @Override
    public void disableFields() {
        lblNome.setEnabled(false);
        fldNome.setEnabled(false);
        txtDescricao.setEnabled(false);
        lblDescricao.setEnabled(false);
        jBIListIndicadores.xmlTable.setEnabled(false);
        jBIListIndicadores.btnNew.setEnabled(false);
        jBIListIndicadores.btnView.setEnabled(false);
        jBIListIndicadores.prepareSearchField(false);
        btnAdd.setEnabled(false);
        btnRemove.setEnabled(false);
    }

    /**
     * Atualiza os campos do formul�rio.
     */
    @Override
    public void refreshFields() {
        if (!record.getBoolean("PERMISSAO")) {
            btnEdit.setVisible(false);
            btnDelete.setVisible(false);
            btnAdd.setVisible(false);
            btnRemove.setVisible(false);
        }

        jBIListIndicadores.prepareSearchField(false);

        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));

        if (getStatus() != KpiDefaultFrameBehavior.INSERTING) {
            vctIndicadores = record.getBIXMLVector("INDICADORES");
            jBIListIndicadores.setDataSource(vctIndicadores, "0", "0", this.event);
        }
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);
        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());

        if (removedElements != null) {
            recordAux.set(removedElements);
        } else {
            recordAux.set(removedElements = new BIXMLVector("EX_IND", "EX_INDS"));
        }

        if (insertedElements != null) {
            recordAux.set(insertedElements);
        } else {
            recordAux.set(insertedElements = new BIXMLVector("ADD_IND", "ADD_INDS"));
        }

        return recordAux;
    }

    /**
     * Valida os campos do formul�rio.
     *
     * @param errorMessage
     * @return boolean
     */
    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        if (fldNome.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGrupoIndicador_00003"));
            retorno = false;
        }

        if (!retorno) {
            btnSave.setEnabled(true);
        }

        return retorno;
    }

    /**
     * Popula o combo de sele��o da classe auxiliar (JDialog)
     *
     * @param oDialog
     * @return void
     */
    void populateChildrenCombo(KpiBuscaGrupoIndicador oDialog) {
        BIXMLRecord recScoreCard = event.listRecords("SCORECARD", "LISTA_SCO_OWNER");
        BIXMLVector vctScoreCard = recScoreCard.getBIXMLVector("SCORECARDS");
        event.populateCombo(vctScoreCard, "SCORECARD", oDialog.htbScoreCard, oDialog.jCmbScorecard);
        oDialog.tsArvorePrincipal.setVetor(vctScoreCard);
    }

    /**
     * Popula a tabela de indicadores dispon�veis da classe auxiliar(JDialog).
     *
     * @param evt
     * @param oDialog
     * @return void
     */
    void populateChildrenXML(KpiBuscaGrupoIndicador oDialog) {
        if (oDialog.jCmbScorecard.getItemCount() > 0) {
            //Filtra os indicadores que fazem parte do Scorecard selecionado e n�o fa�am parte do grupo atual.
            BIXMLRecord recIndFiltrados;

            recIndFiltrados = event.listRecords("INDICADOR", "ID_SCOREC = '".concat(event.getComboValue(oDialog.htbScoreCard, oDialog.jCmbScorecard)).concat("' AND ID_GRUPO <> '").concat(record.getString("ID").concat("'")).concat(",").concat("LST_IND_SCORECARD"));

            //Popula a tabela com os indicadores
            oDialog.xmlDisponiveis = recIndFiltrados.getBIXMLVector("INDICADORES");
            //Mant�m o registro dos campos escolhidos.
            if (insertedElements != null) {
                oDialog.xmlEscolhidos = insertedElements;
            }
        }
    }

    /**
     * Atualiza a cole��o de valores a serem inclu�dos com uma refer�ncia dos
     * valores escolhidos (JDialog)
     *
     * @param xmlEscolhidos
     * @return void
     */
    void updateXMLFromChildren(BIXMLVector xmlEscolhidos) {
        insertedElements = xmlEscolhidos;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        fldNome = new pv.jfcx.JPVEdit();
        lblDescricao = new javax.swing.JLabel();
        scrDescricao = new javax.swing.JScrollPane();
        txtDescricao = new kpi.beans.JBITextArea();
        pnlTopRecord = new javax.swing.JPanel();
        pnlIndicadores = new javax.swing.JPanel();
        jBIListIndicadores = new kpi.beans.JBISeekListPanel();
        pnlButtons = new javax.swing.JPanel();
        btnRemove = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnAjuda2 = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiGrupoIndicador_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_grupoindicador.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(479, 317));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlFields.setLayout(new java.awt.BorderLayout());
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);

        pnlData.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        lblNome.setText(bundle1.getString("KpiBaseFrame_00006")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(10, 10, 58, 16);
        pnlData.add(fldNome);
        fldNome.setBounds(10, 30, 400, 22);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText(bundle1.getString("KpiGrupoIndicador_00002")); // NOI18N
        lblDescricao.setEnabled(false);
        lblDescricao.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblDescricao);
        lblDescricao.setBounds(10, 50, 58, 16);

        txtDescricao.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        scrDescricao.setViewportView(txtDescricao);

        pnlData.add(scrDescricao);
        scrDescricao.setBounds(10, 70, 400, 120);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("KpiGrupoIndicador_00001"), pnlRecord); // NOI18N

        pnlIndicadores.setLayout(new java.awt.BorderLayout());
        pnlIndicadores.add(jBIListIndicadores, java.awt.BorderLayout.CENTER);

        pnlButtons.setPreferredSize(new java.awt.Dimension(50, 10));
        pnlButtons.setLayout(null);

        btnRemove.setMnemonic('-');
        btnRemove.setText("-");
        btnRemove.setToolTipText(bundle1.getString("JBISelectionPanel_00001")); // NOI18N
        btnRemove.setIconTextGap(0);
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });
        pnlButtons.add(btnRemove);
        btnRemove.setBounds(0, 60, 42, 23);

        btnAdd.setMnemonic('+');
        btnAdd.setText("+");
        btnAdd.setToolTipText(bundle1.getString("JBISelectionPanel_00002")); // NOI18N
        btnAdd.setIconTextGap(0);
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        pnlButtons.add(btnAdd);
        btnAdd.setBounds(0, 30, 42, 23);

        pnlIndicadores.add(pnlButtons, java.awt.BorderLayout.EAST);

        java.util.ResourceBundle bundle2 = java.util.ResourceBundle.getBundle("international"); // NOI18N
        tapCadastro.addTab(bundle2.getString("KpiApresentacao_00011"), pnlIndicadores); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 29));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle1.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle1.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMinimumSize(new java.awt.Dimension(300, 19));
        pnlOperation.setPreferredSize(new java.awt.Dimension(330, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle1.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle1.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle1.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnAjuda2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda2.setText(bundle.getString("KpiGraphIndicador_00011")); // NOI18N
        btnAjuda2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjuda2ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda2);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        getContentPane().add(pnlTools, java.awt.BorderLayout.NORTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnAjuda2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjuda2ActionPerformed
            event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjuda2ActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            removedElements = null;
            insertedElements = null;
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            btnSave.setEnabled(false);
            event.saveRecord();
            removedElements = null;
            insertedElements = null;
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
            removedElements = null;
            insertedElements = null;
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
        JBIXMLTable xmlTable = jBIListIndicadores.xmlTable;
        ArrayList a = new ArrayList();

        if (xmlTable.getSelectedRow() != -1) {
            BIXMLRecord linha = xmlTable.getXMLData().get(xmlTable.getSelectedRow()).clone2();

            if (removedElements == null) {
                removedElements = new BIXMLVector("EX_IND", "EX_INDS");
            }

            removedElements.add(linha);

            xmlTable.getXMLData().remove(xmlTable.getSelectedRow());
            refreshFields();
        }
}//GEN-LAST:event_btnRemoveActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed

        KpiBuscaGrupoIndicador frmDestino = (KpiBuscaGrupoIndicador) kpi.core.KpiStaticReferences.getKpiFormController().getForm("BUSCAGRUPOINDICADOR", this.getID(), "");
        frmDestino.setFrame(this);

}//GEN-LAST:event_btnAddActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnAjuda2;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnRemove;
    private javax.swing.JButton btnSave;
    private pv.jfcx.JPVEdit fldNome;
    private kpi.beans.JBISeekListPanel jBIListIndicadores;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblNome;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlButtons;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlIndicadores;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JScrollPane scrDescricao;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private kpi.beans.JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiGrupoIndicador(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);

        //Desabilita a op��o de atualiza��o do componente JBISeekList.
        jBIListIndicadores.btnReload.setVisible(false);

        event.defaultConstructor(operation, idAux, contextId);
        jBIListIndicadores.setDataSource(newBlankList(), "0", "0", this.event);
        
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        btnSave.setEnabled(true);
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    private kpi.xml.BIXMLVector newBlankList() {
        kpi.xml.BIXMLAttributes attIndicador = new kpi.xml.BIXMLAttributes();
        attIndicador.set("TIPO", "INDICADOR");
        attIndicador.set("TAG000", "NOME");
        attIndicador.set("CAB000", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiListaIndicador_00003"));
        attIndicador.set("CLA000", "4");
        attIndicador.set("RETORNA", "F");
        kpi.xml.BIXMLVector newList = new kpi.xml.BIXMLVector("INDICADORES", "INDICADOR", attIndicador);
        return newList;
    }

    public kpi.swing.KpiDefaultFrameBehavior getEvent() {
        return event;
    }
}
