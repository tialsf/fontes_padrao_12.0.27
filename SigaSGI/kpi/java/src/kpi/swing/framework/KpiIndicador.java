package kpi.swing.framework;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.net.URL;
import java.util.*;
import kpi.core.*;
import kpi.core.IteMnuPermission.Permissao;
import kpi.core.IteMnuPermission.TipoPermissao;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.analisys.KpiScoreCarding;
import kpi.swing.analisys.widget.*;
import kpi.util.KpiDateUtil;
import kpi.util.KpiToolKit;
import kpi.xml.BIXMLAttributes;
import kpi.xml.BIXMLException;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;
import pv.jfcx.JPVCalculator;

public class KpiIndicador extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    public static final int KPI_PLANIND_NORIGHTS = -1; //Usu�rio sem permiss�o de visualiza��o.
    public static final int KPI_PLANIND_OK = 0; //Permitido
    public static final int KPI_PLANIND_NOUPDATE = 1; //Manuten��o bloqueada usu�rio sem permiss�o para efetuar manuten��o.
    public static final int KPI_PLANIND_FORMULE = 2; //Manuten��o bloqueada para indicadores com f�rmulas.
    public static final int KPI_PLANIND_OWNER = 3; //Manuten��o bloqueada somente respons�veis podem alterar o indicador.
    public static final int KPI_PLANIND_PERIOD = 4; //Manuten��o bloqueada para esse per�odo.
    public static final int KPI_PLANIND_PERIODUSER = 5; //Manuten��o bloqueada por usu�rio para esse per�odo.
    public static final int KPI_PLANIND_PERIODSCO = 6; //Manuten��o bloqueada por departamento para esse per�odo.
    public static final int KPI_PLANIND_DIALIMITE = 7; //Manutencao bloqueada por dia limite para esse per�odo.
    //
    private String recordType = "INDICADOR"; //NOI18N
    private String formType = recordType;//Tipo do Formulario
    private String parentType = "LSTINDICADOR";//Tipo formulario pai, caso haja. //NOI18N
    private String scoSelected = null;
    private Vector vctFormula = new Vector();
    private kpi.swing.KpiDefaultDialogSystem dialog = kpi.core.KpiStaticReferences.getKpiDialogSystem();
    private KpiIndicadorTableRenderer indicadorCellRenderer;
    private kpi.xml.BIXMLRecord recDataWareHouse = null;
    private KpiIndSourceQueryTable tabQuery = new KpiIndSourceQueryTable();
    private BIXMLVector PlanilhaDeValoresBackUp = null; //Vetor com os dados para o bot�o de cancelamento.
    private BIXMLVector vctScoreCard = null; //Vetor com todos os indicadores carregado do formulario de lista.
    private BIXMLVector backConsultas = null;//backup da consulta adicionada a tabela do DW.
    private BIXMLVector vctUsuarios = null;//Vetor para armazenamento dos usuarios.
    private KpiTabIndicador tabIndicador = new KpiTabIndicador();
    private boolean lResetPeriodo = false;
    private boolean isConectedDW = false;
    private int lastItemFreq = 0;
    private String[] tipoAcumulado = {java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00039"),
        java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00040"),
        java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00046"),
        java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00048"),};
    private Hashtable htbConsultas = new java.util.Hashtable(), htbDataWareHouse = new java.util.Hashtable(), htbGrupIndica = new java.util.Hashtable(), htbFrequencia = new java.util.Hashtable(), htbIndicadores = new java.util.Hashtable(), htbMetaFormula = new java.util.Hashtable(), htbScoreCard = new java.util.Hashtable(), htbUnidade = new java.util.Hashtable(), htbResponsavel = new java.util.Hashtable(), htbIndFormula = new java.util.Hashtable(), htbYearFilter = new java.util.Hashtable(), htbTipos = new java.util.Hashtable();
    static boolean isAdmin = false;
    static int nEnablePlan = 0;
    static int nEnableInd = 0;
    private KpiListaIndicador parentFrame = (KpiListaIndicador) KpiStaticReferences.getKpiFormController().getParentForm(this.getParentType(), "0"); //NOI18N
    //Alteracao de Meta/Planilha de Valores
    private double vlrAlvoVerde = 0.0;
    private BIXMLVector vctJustificativa = new BIXMLVector("JUSTIFICATIVAS", "JUSTIFICATIVA");
    private HashMap historicoPlanilha = new HashMap();//armazena planilha de valores sem alteracoes
    private int tipo_justificativa = 0; //tipo de justificativa 1-Meta; 2-Planilha de Valores; 3-Meta em Lote; 4-Excluidos; 5-Incluidos
    private boolean updateXMLJustificativaOk = false;
    private boolean isDuplicacao = false; //Indica se est� sendo realizada um opera��o de duplica��o. 
    private boolean habilitaMsg = false; // Vari�vel para controle se ir� aparecer a mensagem ao trocar item do combo frequ�ncia.

    /**
     * Construtor.
     *
     * @param operation
     * @param idAux
     * @param contextId
     */
    public KpiIndicador(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true); //NOI18N

        event.registerEvent(KpiDefaultFrameBehavior.EVENT_ON_UPDATE, new KpiEventTask(this, "afterUpdate", null, null)); //NOI18N
        event.registerEvent(KpiDefaultFrameBehavior.EVENT_ON_INSERT, new KpiEventTask(this, "afterInsert", null, null)); //NOI18N

        configuraCombosWidget();
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {

        // Verifica se o usu�rio tem permiss�o para alterar indicador.
        if (nEnableInd == 0 || getStatus() != KpiDefaultFrameBehavior.UPDATING) {

            //Aba de dados do Indicador.
            event.setEnableFields(pnlData, true);

            //Aba de F�rmula.
            event.setEnableFields(pnlFormula, true);

            //Aba do Widget
            event.setEnableFields(pnlWdgtData, true);

            //Aba da Integra��o com o DW.
            event.setEnableFields(pnlDw, true);

            if (record.getBoolean("ISCONSOLID")) { //NOI18N
                //Aplica a regra para consolidadores.
                aplicaRegraConsolidador(true);
            }
        }

        //Aba da Planilha de Valores e Meta.
        event.setEnableFields(pnlValores, nEnablePlan == KPI_PLANIND_OK);
        event.setEnableFields(pnlMeta, nEnablePlan == KPI_PLANIND_OK);

        KpiMenuController mnuControl = KpiStaticReferences.getMnuController();
        IteMnuPermission perm = mnuControl.getRuleByName("PLANILHAS");

        if (perm.getPermissionValue(TipoPermissao.INCLUSAO) == Permissao.NEGADO) {
            btnPlanNovo.setEnabled(false);
        }

        if (perm.getPermissionValue(TipoPermissao.ALTERACAO) == Permissao.NEGADO) {
            event.setEnableFields(pnlMetaLote, false);
        }

        //Habilita o Painel de Alvos da Meta.
        event.setEnableFields(pnlAlvo, nEnableInd == KPI_PLANIND_OK);


    }

    @Override
    public void disableFields() {

        //Aba de dados do Indicador.
        event.setEnableFields(pnlData, false);

        //Aba de F�rmula.
        event.setEnableFields(pnlFormula, false);

        //Aba da Planilha de Valores.
        event.setEnableFields(pnlValores, false);
        event.setEnableFields(pnlPlanilha, true);

        //Aba do Widget
        event.setEnableFields(pnlWdgtData, false);

        //Aba da Integra��o com o DW.
        event.setEnableFields(pnlDw, false);

        //Aba da Meta.
        event.setEnableFields(pnlMeta, false);

        //N�o desabilita os campos de filtro da planilha de valores.
        cbbYearFilter.setEnabled(true);
        lblYearFilter.setEnabled(true);
        tblPlanilhaValor.setEnabled(false);
    }

    /**
     * Define a regra de exibi��o das op��es para o c�lculo do consolidador.
     *
     * @param enable
     */
    private void aplicaRegraConsolidador(boolean enable) {

        //Inverte o valor booleano apenas para facilitar o entendimento do m�todo.
        enable = !enable;

        event.setEnableFields(pnlCalculo, enable);

        //Desmarca as op��es de c�lculo de META e PREVIA.
        chkCalcMeta.setSelected(enable);
        chkCalcPrevia.setSelected(enable);

        //Habilita o c�lculo de REAL.
        chkCalcReal.setSelected(!enable);

        //Desabilta as opera��es n�o permitidas para o consolidador.
        btnRemover.setEnabled(enable);
        btnMultiplicar.setEnabled(enable);
        btnDividir.setEnabled(enable);
        btnAbreParentese.setEnabled(enable);
        btnFechaParentese.setEnabled(enable);
        lblExpressao.setEnabled(enable);
        txtExpressao.setEnabled(enable);
        btnAddExpressao.setEnabled(enable);

        //Desabilita a op��o de metaformulas.
        lblFormMeta.setEnabled(enable);
        cbbMetaFormula.setEnabled(enable);
        btnAddMetaFormula.setEnabled(enable);

        //Fixa a unidade de medida em porcentagem.
        cbbUnidade.setEnabled(enable);
        cbbUnidade.setSelectedIndex(1);

        //Fixa o tipo de acumulado em m�dia.
        cbbTipoAcumulado.setEnabled(enable);
        cbbTipoAcumulado.setSelectedIndex(1);

    }

    @Override
    public void refreshFields() {

        //Atualiza o statusWork;
        KpiStaticReferences.getKpiMainPanel().refreshWorkStatus();

        //Configura as regras de permiss�o para o usu�rio.
        configuraPermissao();

        //Reutiliza a lista de scorecards carregada em mem�ria.
        configuraCache();

        //Configura todas as abas do formul�rio.
        configuraAbas();

        //Aplica as configura��es padr�o para inser��o.
        if (getStatus() == KpiDefaultFrameBehavior.INSERTING) {
            configuraPadrao();
        }

        //Habilita a reinicializa��o da planilha de valores.
        lResetPeriodo = true;

        //Atualiza o hist�rico de atualiza��o da planilha de valores. 
        setHistoricoPlanilhaDeValores();
    }

    /**
     * Centraliza a configura��o do permissionamento do usu�rio.
     *
     * @throws BIXMLException
     */
    private void configuraPermissao() throws BIXMLException {

        //Altera��o da planilha de valores.
        KpiIndicador.nEnablePlan = record.getInt("BLQ_ALT_VLR"); //NOI18N

        //Altera��o do indicador.
        KpiIndicador.nEnableInd = record.getInt("BLQ_ALT_IND"); //NOI18N

        //Indica se o usu�rio � administrador.
        KpiIndicador.isAdmin = (record.getBoolean("ISADMIN")); //NOI18N
    }

    /**
     * Centraliza a configura��o de todas as abas do formul�rio.
     *
     * @throws NumberFormatException
     * @throws BIXMLException
     */
    private void configuraAbas() throws NumberFormatException, BIXMLException {
        configuraAbaIndicador();
        configuraAbaFormula();
        configuraAbaPlanilha();
        configuraAbaWidget();
        configuraAbaDW();
        configuraAbaMeta();
    }

    /**
     * Centraliza a configura��o dos campos da aba Indicador.
     *
     * @throws BIXMLException
     */
    private void configuraAbaIndicador() throws BIXMLException {

        //Nome do Indicador.
        fldNome.setText(record.getString("NOME"));

        //Descri��o do Indicador.
        txtDescricao.setText(record.getString("DESCRICAO"));

        //M�todo de Verifica��o do Indicador.
        txtVerificacao.setText(record.getString("VERIFICA"));

        //Documento relacionado ao Indicador.
        fldLink.setText(record.getString("LINK"));

        //Scorecard do Indicador.
        event.populateCombo(vctScoreCard, "SCORECARD", htbScoreCard, cbbScoreCard); //NOI18N
        event.selectComboItem(cbbScoreCard, event.getComboItem(vctScoreCard, record.getString("ID_SCOREC"), true));
        tsArvorePrincipal.setVetor(vctScoreCard);

        //Popula as combos de indicadores
        populateFilteredInd();

        //Grupo do Indicador.
        event.populateCombo(record.getBIXMLVector("GRUPO_INDS"), "GRU_IND", htbGrupIndica, cbbGrupo); //NOI18N
        event.selectComboItem(cbbGrupo, event.getComboItem(record.getBIXMLVector("GRUPO_INDS"), record.getString("ID_GRUPO"), true));
        //Frequ�ncia do Indicador.
        event.populateCombo(record.getBIXMLVector("FREQUENCIAS"), "FREQ", htbFrequencia, cbbFrequencia); //NOI18N

        //Unidade de Medida do Indicador.
        event.populateCombo(record.getBIXMLVector("UNIDADES"), "UNIDADE", htbUnidade, cbbUnidade); //NOI18N
        event.selectComboItem(cbbUnidade, event.getComboItem(record.getBIXMLVector("UNIDADES"), record.getString("UNIDADE"), true));

        //Tipo do Acumulado do Indicador.
        cbbTipoAcumulado.setSelectedIndex(record.getInt("ACUM_TIPO"));

        //Peso do Indicador.
        spnPeso.setValue(record.getInt("PESO"));

        //Tipo de Atualiza��o do Indicador.
        event.populateCombo(record.getBIXMLVector("TIPOS"), "TIPO_ATU", htbTipos, cbbTipoAtualizacao);

        //Casas Decimais do Indicador.
        spnCasasDecimais.setValue(record.getInt("DECIMAIS"));

        //C�digo de Importa��o do Indicador.
        fldCodImp.setValue(record.getString("ID_CODCLI"));

        //Indicador Pai.
        chkIndPai.setSelected(!record.getString("ISOWNER").equals("F"));

        //Indicador Estrat�gico.
        chkEstrategico.setSelected(record.getBoolean("IND_ESTRAT"));

        //Indicador Consolidador.
        chkConsolidador.setSelected(record.getBoolean("ISCONSOLID"));

        //Visibilidade do Indicador.
        chkVisivel.setSelected(record.getBoolean("VISIVEL"));

        //Orienta��o do Indicador.
        if (record.getBoolean("ASCEND")) {
            rdbAscendente.setSelected(true);
        } else if (record.getBoolean("MELHORF")) {
            rdbMelhorFaixa.setSelected(true);
        } else {
            rdbDescendente.setSelected(true);
        }

        //Recupera a lista de usu�rios.
        vctUsuarios = record.getBIXMLVector("RESPONSAVEIS");
        BIXMLVector vctTreeUser = record.getBIXMLVector("TREEUSERS");

        //Responsavel pelo indicador
        event.populateCombo(vctUsuarios, "TP_RESP_ID_RESP", htbResponsavel, cmbRespIndicador);
        event.selectComboItem(cmbRespIndicador, event.getComboItem(vctUsuarios, record.getString("TP_RESP_ID_RESP"), true));
        tsTreeRespInd.setVetor(vctUsuarios);
        tsTreeRespInd.setTree(vctTreeUser);

        //Responsavel pela coleta.       
        event.populateCombo(vctUsuarios, "TP_RESPCOL_ID_RESPCOL", htbResponsavel, cmbRespColeta);
        event.selectComboItem(cmbRespColeta, event.getComboItem(vctUsuarios, record.getString("TP_RESPCOL_ID_RESPCOL"), true));
        tsTreeRespCol.setVetor(vctUsuarios);
        tsTreeRespCol.setTree(vctTreeUser);
    }

    /**
     * Centraliza a configura��o dos campos da aba de F�rmula.
     *
     * @throws BIXMLException
     */
    private void configuraAbaFormula() throws BIXMLException {

        //Scorcard.
        event.populateCombo(vctScoreCard, "SCORECARD", htbScoreCard, cbbScoreFormula);
        tsArvoreFormula.setVetor(vctScoreCard);

        //Posiciona no primeiro item do combo do indicador da formula.
        if (cbbIndFormula.getItemCount() > 0) {
            cbbIndFormula.setSelectedIndex(0);
        }
        event.populateCombo(record.getBIXMLVector("METASFORMULA"), "METAFORMULA", htbMetaFormula, cbbMetaFormula);

        //Posiciona no primeiro item do combo da meta f�rmula.
        if (cbbMetaFormula.getItemCount() > 0) {
            cbbMetaFormula.setSelectedIndex(0);
        }

        stringToFormula(record.getString("FORMULA"));

        //Descri��o da F�rmula.
        txtDescformula.setText(record.getString("DESCFORMU"));

        //Define a visibilidade da pr�via.
        chkCalcPrevia.setVisible(record.getBoolean("VALOR_PREVIO"));

        //Itens marcados para calcular.
        setOpcaoCalculo(record.getString("ITENS_CAL"));

        //Inibe o campo de edi��o de f�rmulas quando o indicador for propriet�rio.
        if (record.getBoolean("ISPRIVATE")) {
            //Oculta o campo de visualiza��o de formula.
            scrFormula.setVisible(false);
            //Marca o valor de retorno como 'T' [True].
            isPrivate = "T";
        } else {
            isPrivate = "F";
        }
    }

    /**
     * Centraliza a configura��o dos campos da planilha de valores.
     *
     * @throws NumberFormatException
     * @throws BIXMLException
     */
    private void configuraAbaPlanilha() throws NumberFormatException, BIXMLException {

        //Filtro por Ano.
        event.populateCombo(record.getBIXMLVector("ALLYEARS"), "ID", htbYearFilter, cbbYearFilter);
        htbYearFilter.remove(0);
        cbbYearFilter.removeItemAt(0);
        event.selectComboItem(cbbYearFilter, event.getComboItem(record.getBIXMLVector("ALLYEARS"), record.getString("ID_CBOYEAR"), false), false);

        //Planilha
        String periodoID = event.getComboValue(htbFrequencia, cbbFrequencia);
        tabIndicador.resetExcluidos();
        tabIndicador.setCabecPeriodo(new Integer(periodoID).intValue(), record.getBoolean("VALOR_PREVIO"));

        if (PlanilhaDeValoresBackUp == null) {
            PlanilhaDeValoresBackUp = record.getBIXMLVector("PLANILHAS");
            tblPlanilhaValor.setDataSource(PlanilhaDeValoresBackUp.clone2());
            applyRenderIndicador(tblPlanilhaValor);
        }

        //Dia limite para preenchimento.
        spnDia.setValue(record.getInt("DIA_LIMITE"));

        //Toler�ncia da Meta.
        fldTolerancia.setValue(record.getInt("TOLERAN"));
        //Supera em
        fldSuperaEm.setValue(record.getInt("SUPERA"));

        //Define a visibilidade da planilha de valores.
        if (KpiIndicador.nEnablePlan == KPI_PLANIND_NORIGHTS) {
            tapCadastro.remove(pnlValores);
            tapCadastro.remove(pnlMeta);
        }
    }

    /**
     * Realiza a configura��o do painel de alvos.
     */
    private void configuraAlvo() {

        for (Component compCor : jPnlCores.getComponents()) {
            jPnlCores.remove(compCor);
        }

        if (rdbAscendente.isSelected()) {

            pnlDadosMeta.add(pnlAlvos);

            jPnlCores.add(scrValorAzul);
            jPnlCores.add(scrValorVerde);
            jPnlCores.add(pnlAmarelo);
            jPnlCores.add(scrValorVermelho);

            lblSetaDesce.setVisible(false);
            lblSetaSobe.setVisible(true);

        } else {

            pnlDadosMeta.add(pnlAlvos);

            jPnlCores.add(scrValorVermelho);
            jPnlCores.add(pnlAmarelo);
            jPnlCores.add(scrValorVerde);
            jPnlCores.add(scrValorAzul);

            lblSetaDesce.setVisible(true);
            lblSetaSobe.setVisible(false);

        }
    }

    /**
     * Centraliza a configura��o dos campos do widget.
     *
     * @throws BIXMLException
     */
    private void configuraAbaWidget() throws BIXMLException {
        //Painel Superior.
        String sGraphUp = record.getString("GRAPH_UP");
        if (sGraphUp.length() < 6) {
            sGraphUp = KpiWidgetFactory.AREA1_GRAPH;
        }
        KpiIndicadorArea areaUp = KpiWidgetFactory.buildIndArea(sGraphUp);

        //Tipo de Gr�fico Superior.
        cboGraphTypesUp.setSelectedIndex(areaUp.getGraphType().getCode());

        //Tipo de An�lise Superior.
        cboGraphVisualUp.setSelectedIndex(areaUp.getAnalisysType().getCode());

        //Valores do Gr�fico Superior.
        if (areaUp.isPercentValue()) {
            cboGraphValuesUp.setSelectedIndex(1);
        } else {
            cboGraphValuesUp.setSelectedIndex(0);
        }

        //Painel Inferior.
        String sGraphDown = record.getString("GRAPH_DO");
        if (sGraphDown.length() < 6) {
            sGraphDown = KpiWidgetFactory.AREA2_GRAPH;
        }
        KpiIndicadorArea areaDown = KpiWidgetFactory.buildIndArea(sGraphDown);

        //Tipo de Gr�fico Inferior.
        cboGraphTypesDown.setSelectedIndex(areaDown.getGraphType().getCode());

        //Tipo de An�lise Inferior.
        cboGraphVisualDown.setSelectedIndex(areaDown.getAnalisysType().getCode());

        //Valores do Gr�fico Inferior.
        if (areaDown.isPercentValue()) {
            cboGraphValuesDown.setSelectedIndex(1);
        } else {
            cboGraphValuesDown.setSelectedIndex(0);
        }

        doPreviewWidget();
    }

    /**
     * Centraliza a configura��o dos campos da integra��o com o DW.
     *
     * @throws BIXMLException
     */
    private void configuraAbaDW() throws BIXMLException {

        //URL do DW
        fldUrlDW.setText(record.getString("DW_URL"));

        //Tabela de Consultas.
        tabQuery.resetExcluidos();
        tabQuery.setCabec();
        backConsultas = record.getBIXMLVector("DWCONSULTAS");
        tblConsultaDW.setDataSource(backConsultas.clone2());

        //Habilita os caixas de escolha.
        cbbDataWareHouse.setEnabled(isConectedDW);
        cbbConsultas.setEnabled(isConectedDW);
        btnAdd.setEnabled(isConectedDW);
    }

    /**
     * Centraliza a configura��o dos campos da integra��o com o DW.
     *
     * @throws BIXMLException
     */
    private void configuraAbaMeta() throws BIXMLException {
        String decimalSeparator = KpiStaticReferences.getDecimalSeparator();

        //Casas Decimais
        pvValorAzul.setDecimalSeparator(decimalSeparator);
        pvValorVerde.setDecimalSeparator(decimalSeparator);
        pvValorVermelho.setDecimalSeparator(decimalSeparator);

        //Alvos.
        pvValorVerde.setValue(record.getDouble("ALVO1"));
        pvValorVermelho.setValue(record.getDouble("ALVO2"));
        pvValorAzul.setValue(record.getDouble("ALVO3"));


        if (record.getBoolean("ASCEND")) {
            lblSetaDesce.setVisible(false);
            lblSetaSobe.setVisible(true);
        } else {
            lblSetaDesce.setVisible(true);
            lblSetaSobe.setVisible(false);
        }

        //Guarda valores iniciais
        vlrAlvoVerde = pvValorVerde.getDouble();

        //Configura as cores do painel de alvos.
        configuraAlvo();

        if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
            tsArvoreFormula.setEntitySelection(KpiStaticReferences.CAD_OBJETIVO);
        }
    }

    /**
     * Configura��es padr�o do formul�rio de cadastro de indicadores.
     */
    private void configuraPadrao() {

        //Dados do Indicador.
        cbbTipoAcumulado.setSelectedIndex(0);
        rdbAscendente.setSelected(true);
        chkVisivel.setSelected(true);
        chkEstrategico.setEnabled(false);
        chkConsolidador.setSelected(false);
        chkIndPai.setSelected(false);
        spnCasasDecimais.setValue(2);
        if (parentFrame != null) {
            event.selectComboItem(cbbScoreCard, event.getComboItem(vctScoreCard, parentFrame.getScoID(), true));
            populateFilteredInd();
        }
        cbbFrequencia.setSelectedIndex(kpi.core.KpiStaticReferences.KPI_FREQ_MENSAL);
        //Planilha.
        tabIndicador.resetExcluidos();
        tabIndicador.setCabecPeriodo(kpi.core.KpiStaticReferences.KPI_FREQ_MENSAL, record.getBoolean("VALOR_PREVIO"));
        tblPlanilhaValor.setDataSource(tabIndicador.getVctDados());
        applyRenderIndicador(tblPlanilhaValor);
        pvValorAzul.setValue(new Double(0.0));
        pvValorVerde.setValue(new Double(0.0));
        pvValorVermelho.setValue(new Double(0.0));
        spnDia.setValue(0);
        spnPeso.setValue(1);
        chkCalcReal.setSelected(true);
        chkCalcMeta.setSelected(true);
        chkCalcPrevia.setVisible(record.getBoolean("VALOR_PREVIO"));
        chkCalcPrevia.setSelected(true);

        //Integra��o com o DW.
        tabQuery.resetExcluidos();
        tabQuery.setCabec();
        tblConsultaDW.setDataSource(tabQuery.getVctDados());
    }

    /**
     * Cacheia os scorecards carregados na lista de filtro.
     */
    private void configuraCache() {
        if (parentFrame != null) {
            vctScoreCard = parentFrame.getVctScoreCard().clone2();
        }
        if (vctScoreCard == null) {
            kpi.xml.BIXMLRecord recScoreCard = event.listRecords("SCORECARD", "LISTA_SCO_OWNER");
            vctScoreCard = recScoreCard.getBIXMLVector("SCORECARDS");
        }
    }

    /**
     * Configura os combos de op��es do widget.
     */
    private void configuraCombosWidget() {
        populateCboGraphType();
        populateCboGraphVisual();
        populateCboGraphValues();

        ItemListener itemListener = new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                doPreviewWidget();
            }
        };

        cboGraphTypesUp.addItemListener(itemListener);
        cboGraphVisualUp.addItemListener(itemListener);
        cboGraphValuesUp.addItemListener(itemListener);
        cboGraphTypesDown.addItemListener(itemListener);
        cboGraphVisualDown.addItemListener(itemListener);
        cboGraphValuesDown.addItemListener(itemListener);
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);

        recordAux.set("ID", id);
        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("VERIFICA", txtVerificacao.getText());
        recordAux.set("ASCEND", rdbAscendente.isSelected());
        recordAux.set("MELHORF", rdbMelhorFaixa.isSelected());
        recordAux.set("VISIVEL", chkVisivel.isSelected());
        recordAux.set("IND_ESTRAT", chkEstrategico.isSelected());
        recordAux.set("DECIMAIS", ((Integer) spnCasasDecimais.getValue()).intValue());
        recordAux.set("DIA_LIMITE", ((Integer) spnDia.getValue()).intValue());
        recordAux.set("PESO", ((Integer) spnPeso.getValue()).intValue());
        recordAux.set("UNIDADE", event.getComboValue(htbUnidade, cbbUnidade));
        recordAux.set("ID_GRUPO", event.getComboValue(htbGrupIndica, cbbGrupo));
        recordAux.set("FREQ", event.getComboValue(htbFrequencia, cbbFrequencia));
        if (this.isDuplicacao()) {
            recordAux.set("ID_SCOREC", event.getComboValue(htbScoreCard, cbbScoreCard));
        } else {
            if (scoSelected != null) {
                recordAux.set("ID_SCOREC", scoSelected);
            } else {
                recordAux.set("ID_SCOREC", event.getComboValue(htbScoreCard, cbbScoreCard));
            }
        }
        recordAux.set("ID_INDICA", event.getComboValue(htbIndicadores, cbbParentIndicador));
        recordAux.set("ID_CODCLI", fldCodImp.getText());
        recordAux.set("TOLERAN", ((Integer) fldTolerancia.getValue()).intValue());
        recordAux.set("SUPERA", ((Integer) fldSuperaEm.getValue()).intValue());
        recordAux.set("ACUM_TIPO", cbbTipoAcumulado.getSelectedIndex());
        recordAux.set("ITENS_CAL", getOpcaoCalculo());
        recordAux.set("TIPO_ATU", event.getComboValue(htbTipos, cbbTipoAtualizacao));
        recordAux.set("ISOWNER", chkIndPai.isSelected());
        recordAux.set("LINK", fldLink.getText());
        recordAux.set("GRAPH_UP", getValuesAreaUp());
        recordAux.set("GRAPH_DO", getValuesAreaDown());

        recordAux.set("ALVO1", pvValorVerde.getDouble());
        recordAux.set("ALVO2", pvValorVermelho.getDouble());
        recordAux.set("ALVO3", pvValorAzul.getDouble());

        recordAux.set("DESCFORMU", txtDescformula.getText());
        recordAux.set("FORMULA", formulaToString());
        recordAux.set("ISPRIVATE", isPrivate);
        recordAux.set("ISCONSOLID", chkConsolidador.isSelected());

        kpi.xml.BIXMLVector vctDadosPlan = tblPlanilhaValor.getXMLData().clone2();
        kpi.xml.BIXMLRecord recDadosDel = tabIndicador.getDelDados();

        if (recDadosDel != null) {
            vctDadosPlan.add(tabIndicador.getDelDados());
        }
        recordAux.set(vctDadosPlan);

        if (event.getComboValue(htbResponsavel, cmbRespIndicador).substring(0, 1).equals("0")) {
            recordAux.set("TP_RESP", "");
            recordAux.set("ID_RESP", "");
        } else {
            recordAux.set("TP_RESP", event.getComboValue(htbResponsavel, cmbRespIndicador).substring(0, 1));
            recordAux.set("ID_RESP", event.getComboValue(htbResponsavel, cmbRespIndicador).substring(1));
        }

        if (event.getComboValue(htbResponsavel, cmbRespColeta).substring(0, 1).equals("0")) {
            recordAux.set("TP_RESPCOL", "");
            recordAux.set("ID_RESPCOL", "");
        } else {
            recordAux.set("TP_RESPCOL", event.getComboValue(htbResponsavel, cmbRespColeta).substring(0, 1));
            recordAux.set("ID_RESPCOL", event.getComboValue(htbResponsavel, cmbRespColeta).substring(1));
        }

        kpi.xml.BIXMLVector vctDWCon = tblConsultaDW.getXMLData();
        kpi.xml.BIXMLRecord recDWDel = tabQuery.getDelDados();
        if (recDWDel != null) {
            vctDWCon.add(recDWDel);
        }
        recordAux.set(vctDWCon);

        //Vector com os Dados da JUSTIFICATIVA DE ALTERACAO DE METAS
        recordAux.set(vctJustificativa);

        // atualizacao necessaria para justificativa de alteracao
        record.set("ALVO1", pvValorVerde.getDouble());

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        if (fldNome.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScorecard_00006"));
            retorno = false;

        } else if (fldNome.getText().indexOf(">") != -1) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00054"));
            retorno = false;

        } else if (cbbScoreCard.getSelectedIndex() <= 0 && !KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00004"));
            retorno = false;

        } else if (cbbUnidade.getSelectedIndex() <= 0 && !chkEstrategico.isSelected()) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00042"));
            retorno = false;

        } else if (cbbFrequencia.getSelectedIndex() <= 0 && !chkEstrategico.isSelected()) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00002"));
            retorno = false;

        } else if (((Integer) spnDia.getValue()).intValue() < 0 || ((Integer) spnDia.getValue()).intValue() > 31) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiAgendamento_00015"));
            retorno = false;

        } else if (!chkEstrategico.isSelected()) {

            if (!pvValorVermelho.getText().isEmpty() && !pvValorVerde.getText().isEmpty() && !pvValorAzul.getText().isEmpty()
                    && pvValorVermelho.getDouble() != 0 && pvValorVerde.getDouble() != 0 && pvValorVerde.getDouble() != 0) {
                if (!record.getBoolean("AVAL_META_SCORE")) {
                    if (rdbAscendente.isSelected()) {
                        if (pvValorVermelho.getDouble() >= pvValorVerde.getDouble() || pvValorAzul.getDouble() <= pvValorVerde.getDouble()) {
                            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00035"));
                            retorno = false;
                        }
                    } else if (rdbMelhorFaixa.isSelected()) {
                        if ((((Integer) fldTolerancia.getValue()).intValue() == 0 || ((Integer) fldSuperaEm.getValue()).intValue() == 0)
                                && (((Integer) fldTolerancia.getValue()).intValue() <= ((Integer) fldSuperaEm.getValue()).intValue())) {
                            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00113"));
                            retorno = false;
                        }
                    } else {
                        if (pvValorVermelho.getDouble() <= pvValorVerde.getDouble() || pvValorAzul.getDouble() >= pvValorVerde.getDouble()) {
                            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00035"));
                            retorno = false;
                        }
                    }
                }
            }

            if (record.getBoolean("AVAL_META_SCORE")) {
                if (rdbMelhorFaixa.isSelected()) {
                    if ((((Integer) fldTolerancia.getValue()).intValue() == 0 || ((Integer) fldSuperaEm.getValue()).intValue() == 0)
                            || (((Integer) fldSuperaEm.getValue()).intValue() <= ((Integer) fldTolerancia.getValue()).intValue())) {
                        errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00113"));
                        retorno = false;
                    }
                }
            }

        }

        if (!retorno) {
            btnSave.setEnabled(true);
        }

        return retorno;
    }

    /**
     * Alimenta as combos de configura��o do widget com o tipo de gr�fico
     *
     * @since 21/06/2010
     */
    public void populateCboGraphType() {
        String barra = java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00100");
        String odom = java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00101");
        String reg = java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00102");
        String ter = java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00103");
        String tab = java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00104");

        String[] aValues = {barra, odom, reg, ter, tab};

        for (int i = 0; i < aValues.length; i++) {
            cboGraphTypesUp.addItem(aValues[i]);
            cboGraphTypesDown.addItem(aValues[i]);
        }

    }

    /**
     * Alimenta as combos de configura��o do widget com o tipo de visualiza��o
     *
     * @since 21/06/2010
     */
    public void populateCboGraphVisual() {
        String ambos = java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00104");
        String acu = java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00105");
        String par = java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00106");

        String[] aValues = {ambos, acu, par};

        for (int i = 0; i < aValues.length; i++) {
            cboGraphVisualUp.addItem(aValues[i]);
            cboGraphVisualDown.addItem(aValues[i]);
        }
    }

    /**
     * Alimenta as combos de configura��o do widget com o tipo de visualiza��o
     *
     * @since 21/06/2010
     */
    public void populateCboGraphValues() {
        String vlr = java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGraphIndicador_00026");
        String per = java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00107");

        String[] aValues = {vlr, per};

        for (int i = 0; i < aValues.length; i++) {
            cboGraphValuesUp.addItem(aValues[i]);
            cboGraphValuesDown.addItem(aValues[i]);
        }
    }

    /**
     * Pr�-visualiza��o do Widget
     *
     * @since 21/06/2010
     */
    public void doPreviewWidget() {
        KpiIndicadorArea areaType1 = new KpiIndicadorArea();
        KpiIndicadorArea areaType2 = new KpiIndicadorArea();
        KpiIndicadorBean ind = new KpiIndicadorBean();
        String pre = java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00108");
        String per = java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiAgendamento_00017");
        String uni = java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00011");

        //Area1
        areaType1.setGraphType(KpiTypeGraph.valueOf(cboGraphTypesUp.getSelectedIndex()));
        areaType1.setAnalisysType(KpiTypeAnalisys.valueOf(cboGraphVisualUp.getSelectedIndex()));
        areaType1.setPercentValue((cboGraphValuesUp.getSelectedIndex() == 1));

        //Area2
        areaType2.setGraphType(KpiTypeGraph.valueOf(cboGraphTypesDown.getSelectedIndex()));
        areaType2.setAnalisysType(KpiTypeAnalisys.valueOf(cboGraphVisualDown.getSelectedIndex()));
        areaType2.setPercentValue((cboGraphValuesDown.getSelectedIndex() == 1));

        //Indicador
        ind.setTendenciaImgStatus(11);
        ind.setNomeInd(pre);
        ind.setPeriodo(per);
        ind.setUnidadeMed(uni);
        ind.setTolerancia(10);
        ind.setSupera(15);
        ind.setAscendente(rdbAscendente.isSelected());
        ind.setMelhorFaixa(rdbMelhorFaixa.isSelected());
        ind.setCasasDecimais(((Integer) spnCasasDecimais.getValue()).intValue());
        ind.setPreviaVisible(record.getBoolean("VALOR_PREVIO"));

        ind.getValuePer().setReal(100);
        ind.getValuePer().setMeta(65);
        ind.getValuePer().setVariacao("35");
        ind.getValuePer().setVariacaoStatus(KpiScoreCarding.STATUS_GREEN);
        ind.getValuePer().setPrevia(50);
        ind.getValuePer().setVariacaoToolTip("");

        ind.getValueAcum().setReal(200);
        ind.getValueAcum().setMeta(300);
        ind.getValueAcum().setVariacao("100");
        ind.getValueAcum().setVariacaoStatus(KpiScoreCarding.STATUS_RED);
        ind.getValueAcum().setPrevia(220);
        ind.getValueAcum().setVariacaoToolTip("");

        KpiWidget wid = new KpiWidget(ind, areaType1, areaType2);
        wid.setToolBarVisible(false);
        dskPaineis.setAutoOrganize(true);
        dskPaineis.removeAll();
        dskPaineis.add(wid);
        dskPaineis.autoOrganizeFrames();
    }

    public String getValuesAreaUp() {
        StringBuilder values = new StringBuilder();
        values.append("0");
        values.append(cboGraphTypesUp.getSelectedIndex());
        values.append("0");
        values.append(cboGraphVisualUp.getSelectedIndex());
        values.append("0");
        values.append(cboGraphValuesUp.getSelectedIndex());
        return values.toString();
    }

    public String getValuesAreaDown() {
        StringBuilder values = new StringBuilder();
        values.append("0");
        values.append(cboGraphTypesDown.getSelectedIndex());
        values.append("0");
        values.append(cboGraphVisualDown.getSelectedIndex());
        values.append("0");
        values.append(cboGraphValuesDown.getSelectedIndex());
        return values.toString();
    }

    /**
     * Reinicializa a planilha de valores.
     */
    private void limpaPlanilha() {
        String periodo = event.getComboValue(htbFrequencia, cbbFrequencia);

        BIXMLVector vctPlanilha = tblPlanilhaValor.getXMLData();
        int itens = vctPlanilha.size();

        for (int item = 0; item < itens; item++) {
            tabIndicador.addRecordDeleted(vctPlanilha.get((itens - item) - 1));
            tblPlanilhaValor.removeRecord();
        }

        tabIndicador.setCabecPeriodo(new Integer(periodo).intValue(), record.getBoolean("VALOR_PREVIO"));
        tblPlanilhaValor.setDataSource(tabIndicador.getVctDados().clone2());
        applyRenderIndicador(tblPlanilhaValor);
    }

    /**
     * Carrega a planilha de valores Cria a estrutura da tabela a partir da
     * frequ�ncia que estiver selecionada
     */
    private void carregaPlanilha() {
        String periodo = event.getComboValue(htbFrequencia, cbbFrequencia);

        tabIndicador.setCabecPeriodo(new Integer(periodo).intValue(), record.getBoolean("VALOR_PREVIO"));
        tblPlanilhaValor.setDataSource(tabIndicador.getVctDados().clone2());
        applyRenderIndicador(tblPlanilhaValor);
    }

    public void updateXMLJustificativa(BIXMLRecord xmlJustificativa) {
        updateXMLJustificativaOk = true;
        vctJustificativa.add(xmlJustificativa);
    }

    public void cancelUupdateXMLJustificativa() {
        vctJustificativa.removeAll();
        updateXMLJustificativaOk = false;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoOrientacao = new javax.swing.ButtonGroup();
        grupoOpcaoDW = new javax.swing.ButtonGroup();
        pnlTopForm = new javax.swing.JPanel();
        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        lblDescricao = new javax.swing.JLabel();
        lblCasasDecimais = new javax.swing.JLabel();
        cbbParentIndicador = new javax.swing.JComboBox();
        cbbFrequencia = new javax.swing.JComboBox();
        lblFrequencia = new javax.swing.JLabel();
        lblUnidade = new javax.swing.JLabel();
        lblCodImp = new javax.swing.JLabel();
        cbbGrupo = new javax.swing.JComboBox();
        fldNome = new pv.jfcx.JPVEdit();
        fldCodImp = new pv.jfcx.JPVEdit();
        lblGrupo = new javax.swing.JLabel();
        lblScoreCard = new javax.swing.JLabel();
        cbbUnidade = new javax.swing.JComboBox();
        cbbScoreCard = new javax.swing.JComboBox();
        lblParentIndicador = new javax.swing.JLabel();
        lblPeso = new javax.swing.JLabel();
        fldTolerancia = new javax.swing.JSpinner();
        lblSuperaEm = new javax.swing.JLabel();
        spnDia = new javax.swing.JSpinner();
        lblDia1 = new javax.swing.JLabel();
        spnCasasDecimais = new javax.swing.JSpinner();
        lblTipoAcumulado = new javax.swing.JLabel();
        cbbTipoAcumulado = new javax.swing.JComboBox(tipoAcumulado);
        scrDescricao = new javax.swing.JScrollPane();
        txtDescricao = new kpi.beans.JBITextArea();
        lblTipoAtualizacao = new javax.swing.JLabel();
        cbbTipoAtualizacao = new javax.swing.JComboBox(tipoAcumulado);
        spnPeso = new javax.swing.JSpinner();
        lblLink = new javax.swing.JLabel();
        fldLink = new pv.jfcx.JPVEdit();
        pnlTipo = new javax.swing.JPanel();
        chkConsolidador = new javax.swing.JCheckBox();
        chkIndPai = new javax.swing.JCheckBox();
        chkEstrategico = new javax.swing.JCheckBox();
        chkVisivel = new javax.swing.JCheckBox();
        scrVerificacao = new javax.swing.JScrollPane();
        txtVerificacao = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        cmbRespIndicador = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        cmbRespColeta = new javax.swing.JComboBox();
        lblColeta = new javax.swing.JLabel();
        pnlOrientacao = new javax.swing.JPanel();
        rdbAscendente = new javax.swing.JRadioButton();
        rdbMelhorFaixa = new javax.swing.JRadioButton();
        rdbDescendente = new javax.swing.JRadioButton();
        tsArvorePrincipal = new kpi.beans.JBITreeSelection();
        lblTolerancia2 = new javax.swing.JLabel();
        fldSuperaEm = new javax.swing.JSpinner();
        tsTreeRespInd = new kpi.beans.JBITreeSelection(kpi.beans.JBITreeSelection.TYPE_DEFAULT);
        tsTreeRespCol = new kpi.beans.JBITreeSelection(kpi.beans.JBITreeSelection.TYPE_DEFAULT);
        pnlTopRecord = new javax.swing.JPanel();
        scpFormula = new javax.swing.JScrollPane();
        pnlFormula = new javax.swing.JPanel();
        pnlRecFormula = new javax.swing.JPanel();
        lblFormIndicador = new javax.swing.JLabel();
        lblFormMeta = new javax.swing.JLabel();
        cbbIndFormula = new javax.swing.JComboBox();
        cbbMetaFormula = new javax.swing.JComboBox();
        cbbScoreFormula = new javax.swing.JComboBox();
        lblScoreFormula = new javax.swing.JLabel();
        txtExpressao = new pv.jfcx.JPVEdit();
        lblExpressao = new javax.swing.JLabel();
        tsArvoreFormula = new kpi.beans.JBITreeSelection();
        txtFiltro = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        tlbAddIndicador = new javax.swing.JToolBar();
        btnAddIndicador = new javax.swing.JButton();
        tlbAddMetaformula = new javax.swing.JToolBar();
        btnAddMetaFormula = new javax.swing.JButton();
        tlbAddExpressao = new javax.swing.JToolBar();
        btnAddExpressao = new javax.swing.JButton();
        scrDescFormula = new javax.swing.JScrollPane();
        txtDescformula = new kpi.beans.JBITextArea();
        lblDescFormula = new javax.swing.JLabel();
        pnlCalculo = new javax.swing.JPanel();
        chkCalcPrevia = new javax.swing.JCheckBox();
        chkCalcMeta = new javax.swing.JCheckBox();
        chkCalcReal = new javax.swing.JCheckBox();
        jToolBar3 = new javax.swing.JToolBar();
        btnFormulaRapida = new javax.swing.JButton();
        pnlToolFormula = new javax.swing.JPanel();
        pnlLeftForm1 = new javax.swing.JPanel();
        pnlRightForm1 = new javax.swing.JPanel();
        scrFormula = new javax.swing.JScrollPane();
        edtFormula = new javax.swing.JEditorPane();
        jBarFormula = new javax.swing.JToolBar();
        pnlLeftForm2 = new javax.swing.JPanel();
        btnVerificarFormula = new javax.swing.JButton();
        btnLimparFormula = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JToolBar.Separator();
        btnAdicionar = new javax.swing.JButton();
        btnRemover = new javax.swing.JButton();
        btnMultiplicar = new javax.swing.JButton();
        btnDividir = new javax.swing.JButton();
        btnAbreParentese = new javax.swing.JButton();
        btnFechaParentese = new javax.swing.JButton();
        jSeparator6 = new javax.swing.JToolBar.Separator();
        btnVoltar = new javax.swing.JButton();
        jSeparator8 = new javax.swing.JToolBar.Separator();
        pnlBottomForm = new javax.swing.JPanel();
        pnlValores = new javax.swing.JPanel();
        jBarPlanilha = new javax.swing.JToolBar();
        btnPlanNovo = new javax.swing.JButton();
        btnPlanExcluir = new javax.swing.JButton();
        btnPlanExcluirTudo = new javax.swing.JButton();
        jSeparator5 = new javax.swing.JToolBar.Separator();
        lblYearFilter = new javax.swing.JLabel();
        cbbYearFilter = new javax.swing.JComboBox();
        pnlPlanilha = new javax.swing.JPanel();
        tblPlanilhaValor = new kpi.beans.JBIXMLTable();
        indicadorCellRenderer	= new KpiIndicadorTableRenderer(tblPlanilhaValor);
        pnlMeta = new javax.swing.JPanel();
        pnlDadosMeta = new javax.swing.JPanel();
        pnlAlvos = new javax.swing.JPanel();
        pnlAlvo = new javax.swing.JPanel();
        jPnlCores = new javax.swing.JPanel();
        scrValorAzul = new javax.swing.JScrollPane();
        pvValorAzul = new pv.jfcx.JPVNumericPlus();
        pvValorAzul.setAlignment(2);
        scrValorVerde = new javax.swing.JScrollPane();
        pvValorVerde = new pv.jfcx.JPVNumericPlus();
        pnlAmarelo = new javax.swing.JPanel();
        scrValorVermelho = new javax.swing.JScrollPane();
        pvValorVermelho = new pv.jfcx.JPVNumericPlus();
        lblSetaDesce = new javax.swing.JLabel();
        lblSetaSobe = new javax.swing.JLabel();
        pnlMetaLote = new javax.swing.JPanel();
        scrDataAte = new javax.swing.JScrollPane();
        fldDataAte = new pv.jfcx.JPVDatePlus();
        scrDataDe = new javax.swing.JScrollPane();
        fldDataDe = new pv.jfcx.JPVDatePlus();
        lblPeriodoDe = new javax.swing.JLabel();
        lblPeriodoAte = new javax.swing.JLabel();
        lblMeta = new javax.swing.JLabel();
        txtMeta = new pv.jfcx.JPVNumeric();
        pnlWidget = new javax.swing.JPanel();
        pnlWdgtData = new javax.swing.JPanel();
        scrlPainel = new javax.swing.JScrollPane();
        dskPaineis = new kpi.beans.JBICardDesktopPane();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lblGraphTypesDown = new javax.swing.JLabel();
        cboGraphTypesDown = new javax.swing.JComboBox();
        lblGraphVisualDown = new javax.swing.JLabel();
        cboGraphVisualDown = new javax.swing.JComboBox();
        lblGraphValuesDown = new javax.swing.JLabel();
        cboGraphValuesDown = new javax.swing.JComboBox();
        lblGraphTypesUp = new javax.swing.JLabel();
        cboGraphTypesUp = new javax.swing.JComboBox();
        lblGraphVisualUp = new javax.swing.JLabel();
        cboGraphVisualUp = new javax.swing.JComboBox();
        lblGraphValuesUp = new javax.swing.JLabel();
        cboGraphValuesUp = new javax.swing.JComboBox();
        pnlDw = new javax.swing.JPanel();
        pnlFieldsDW = new javax.swing.JPanel();
        fldUrlDW = new pv.jfcx.JPVEdit();
        cbbDataWareHouse = new javax.swing.JComboBox();
        cbbConsultas = new javax.swing.JComboBox();
        lblConsulta = new javax.swing.JLabel();
        lblDatawarehouse = new javax.swing.JLabel();
        lblEnderecoDW = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        btnPostEndereco = new javax.swing.JButton();
        jToolBar2 = new javax.swing.JToolBar();
        btnAdd = new javax.swing.JButton();
        pnlTableDW = new javax.swing.JPanel();
        toolQuery = new javax.swing.JToolBar();
        btnAbrirTabela = new javax.swing.JButton();
        btnRemove = new javax.swing.JButton();
        sepQuery = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        optTabela = new javax.swing.JRadioButton();
        optGrafico = new javax.swing.JRadioButton();
        tblConsultaDW = new kpi.beans.JBIXMLTable();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        cmbDuplicar = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnAjuda2 = new javax.swing.JButton();
        pnlBottomForm1 = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiIndicador_00005")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_indicador.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(915, 505));
        try {
            setSelected(true);
        } catch (java.beans.PropertyVetoException e1) {
            e1.printStackTrace();
        }
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });
        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        tapCadastro.setPreferredSize(new java.awt.Dimension(480, 400));

        pnlRecord.setMaximumSize(new java.awt.Dimension(10, 10));
        pnlRecord.setPreferredSize(new java.awt.Dimension(450, 400));
        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlFields.setMaximumSize(new java.awt.Dimension(10, 10));
        pnlFields.setPreferredSize(new java.awt.Dimension(460, 400));
        pnlFields.setRequestFocusEnabled(false);
        pnlFields.setLayout(new java.awt.BorderLayout());
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);
        scpRecord.setMaximumSize(new java.awt.Dimension(10, 10));
        scpRecord.setPreferredSize(new java.awt.Dimension(800, 400));

        pnlData.setMaximumSize(new java.awt.Dimension(10, 10));
        pnlData.setPreferredSize(new java.awt.Dimension(840, 390));
        pnlData.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        lblNome.setText(bundle1.getString("KpiBaseFrame_00006")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(10, 10, 100, 18);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("KpiIndicador_00006")); // NOI18N
        lblDescricao.setEnabled(false);
        lblDescricao.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblDescricao);
        lblDescricao.setBounds(10, 50, 100, 20);

        lblCasasDecimais.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblCasasDecimais.setText(bundle.getString("KpiIndicador_00007")); // NOI18N
        lblCasasDecimais.setEnabled(false);
        pnlData.add(lblCasasDecimais);
        lblCasasDecimais.setBounds(770, 50, 60, 20);

        cbbParentIndicador.setEnabled(false);
        pnlData.add(cbbParentIndicador);
        cbbParentIndicador.setBounds(10, 310, 400, 22);

        cbbFrequencia.setEnabled(false);
        cbbFrequencia.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cbbFrequenciaMousePressed(evt);
            }
        });
        cbbFrequencia.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbFrequenciaItemStateChanged(evt);
            }
        });
        cbbFrequencia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbbFrequenciaKeyPressed(evt);
            }
        });
        pnlData.add(cbbFrequencia);
        cbbFrequencia.setBounds(450, 30, 180, 22);

        lblFrequencia.setForeground(new java.awt.Color(51, 51, 255));
        lblFrequencia.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblFrequencia.setText(bundle.getString("KpiIndicador_00010")); // NOI18N
        lblFrequencia.setEnabled(false);
        pnlData.add(lblFrequencia);
        lblFrequencia.setBounds(450, 10, 100, 20);

        lblUnidade.setForeground(new java.awt.Color(51, 51, 255));
        lblUnidade.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUnidade.setText(bundle.getString("KpiIndicador_00011")); // NOI18N
        lblUnidade.setEnabled(false);
        pnlData.add(lblUnidade);
        lblUnidade.setBounds(450, 130, 100, 20);

        lblCodImp.setText(bundle.getString("KpiIndicador_00012")); // NOI18N
        lblCodImp.setEnabled(false);
        lblCodImp.setIconTextGap(9);
        pnlData.add(lblCodImp);
        lblCodImp.setBounds(10, 370, 120, 20);

        cbbGrupo.setEnabled(false);
        pnlData.add(cbbGrupo);
        cbbGrupo.setBounds(10, 350, 400, 22);
        pnlData.add(fldNome);
        fldNome.setBounds(10, 30, 400, 22);

        fldCodImp.setMaxLength(255);
        pnlData.add(fldCodImp);
        fldCodImp.setBounds(10, 390, 400, 22);

        lblGrupo.setText(bundle.getString("KpiIndicador_00014")); // NOI18N
        lblGrupo.setEnabled(false);
        lblGrupo.setIconTextGap(9);
        pnlData.add(lblGrupo);
        lblGrupo.setBounds(10, 330, 100, 20);

        lblScoreCard.setForeground(new java.awt.Color(51, 51, 255));
        lblScoreCard.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblScoreCard.setText(KpiStaticReferences.getKpiCustomLabels().getSco(KpiStaticReferences.CAD_OBJETIVO).concat(":"));
        lblScoreCard.setEnabled(false);
        pnlData.add(lblScoreCard);
        lblScoreCard.setBounds(10, 250, 400, 20);

        cbbUnidade.setEnabled(false);
        pnlData.add(cbbUnidade);
        cbbUnidade.setBounds(450, 150, 180, 22);

        cbbScoreCard.setEnabled(false);
        cbbScoreCard.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbScoreCardItemStateChanged(evt);
            }
        });
        pnlData.add(cbbScoreCard);
        cbbScoreCard.setBounds(10, 270, 400, 22);

        lblParentIndicador.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblParentIndicador.setText(bundle.getString("KpiIndicador_00016")); // NOI18N
        lblParentIndicador.setEnabled(false);
        pnlData.add(lblParentIndicador);
        lblParentIndicador.setBounds(10, 290, 140, 20);

        lblPeso.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPeso.setText(bundle.getString("KpiIndicador_00038")); // NOI18N
        lblPeso.setEnabled(false);
        pnlData.add(lblPeso);
        lblPeso.setBounds(670, 50, 60, 20);

        fldTolerancia.setModel(new javax.swing.SpinnerNumberModel(0, 0, 100, 1));
        fldTolerancia.setEnabled(false);
        pnlData.add(fldTolerancia);
        fldTolerancia.setBounds(670, 30, 80, 22);

        lblSuperaEm.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSuperaEm.setText("Supera em (%):");
        lblSuperaEm.setEnabled(false);
        pnlData.add(lblSuperaEm);
        lblSuperaEm.setBounds(770, 10, 80, 20);

        spnDia.setModel(new javax.swing.SpinnerNumberModel(1, 0, 31, 1));
        spnDia.setEnabled(false);
        pnlData.add(spnDia);
        spnDia.setBounds(670, 110, 80, 22);

        lblDia1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDia1.setText(bundle.getString("KpiIndicador_00066")); // NOI18N
        lblDia1.setEnabled(false);
        pnlData.add(lblDia1);
        lblDia1.setBounds(670, 90, 80, 20);

        spnCasasDecimais.setModel(new javax.swing.SpinnerNumberModel(2, 0, 10, 1));
        spnCasasDecimais.setEnabled(false);
        pnlData.add(spnCasasDecimais);
        spnCasasDecimais.setBounds(770, 70, 80, 22);

        lblTipoAcumulado.setText(bundle.getString("KpiIndicador_00041")); // NOI18N
        lblTipoAcumulado.setEnabled(false);
        lblTipoAcumulado.setIconTextGap(9);
        pnlData.add(lblTipoAcumulado);
        lblTipoAcumulado.setBounds(450, 50, 100, 20);

        cbbTipoAcumulado.setEnabled(false);
        pnlData.add(cbbTipoAcumulado);
        cbbTipoAcumulado.setBounds(450, 70, 180, 22);

        txtDescricao.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        txtDescricao.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        scrDescricao.setViewportView(txtDescricao);

        pnlData.add(scrDescricao);
        scrDescricao.setBounds(10, 70, 400, 60);

        lblTipoAtualizacao.setText(bundle.getString("KpiIndicador_00060")); // NOI18N
        lblTipoAtualizacao.setEnabled(false);
        lblTipoAtualizacao.setIconTextGap(9);
        pnlData.add(lblTipoAtualizacao);
        lblTipoAtualizacao.setBounds(450, 90, 100, 20);

        cbbTipoAtualizacao.setEnabled(false);
        pnlData.add(cbbTipoAtualizacao);
        cbbTipoAtualizacao.setBounds(450, 110, 180, 22);

        spnPeso.setModel(new javax.swing.SpinnerNumberModel(1, 1, 999, 1));
        spnPeso.setEnabled(false);
        pnlData.add(spnPeso);
        spnPeso.setBounds(670, 70, 82, 22);

        lblLink.setText("Documento relacionado:");
        lblLink.setEnabled(false);
        lblLink.setIconTextGap(9);
        pnlData.add(lblLink);
        lblLink.setBounds(10, 210, 120, 20);

        fldLink.setMaxLength(255);
        pnlData.add(fldLink);
        fldLink.setBounds(10, 230, 400, 22);

        pnlTipo.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)), "Tipo"));
        pnlTipo.setLayout(null);

        java.util.ResourceBundle bundle2 = java.util.ResourceBundle.getBundle("international"); // NOI18N
        chkConsolidador.setText(bundle2.getString("KpiIndicador_00091")); // NOI18N
        chkConsolidador.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkConsolidador.setEnabled(false);
        chkConsolidador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkConsolidadorActionPerformed(evt);
            }
        });
        pnlTipo.add(chkConsolidador);
        chkConsolidador.setBounds(24, 52, 100, 15);

        chkIndPai.setText(bundle.getString("KpiIndicador_00065")); // NOI18N
        chkIndPai.setEnabled(false);
        chkIndPai.setPreferredSize(new java.awt.Dimension(87, 20));
        pnlTipo.add(chkIndPai);
        chkIndPai.setBounds(20, 15, 100, 15);

        chkEstrategico.setText(bundle.getString("KpiIndicador_00047")); // NOI18N
        chkEstrategico.setEnabled(false);
        chkEstrategico.setPreferredSize(new java.awt.Dimension(79, 20));
        pnlTipo.add(chkEstrategico);
        chkEstrategico.setBounds(20, 33, 100, 15);

        chkVisivel.setText(bundle.getString("KpiIndicador_00013")); // NOI18N
        chkVisivel.setEnabled(false);
        chkVisivel.setPreferredSize(new java.awt.Dimension(55, 20));
        pnlTipo.add(chkVisivel);
        chkVisivel.setBounds(20, 70, 100, 15);

        pnlData.add(pnlTipo);
        pnlTipo.setBounds(450, 190, 180, 100);

        txtVerificacao.setColumns(20);
        txtVerificacao.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtVerificacao.setLineWrap(true);
        txtVerificacao.setRows(3);
        scrVerificacao.setViewportView(txtVerificacao);

        pnlData.add(scrVerificacao);
        scrVerificacao.setBounds(10, 150, 400, 60);

        jLabel3.setText("M�todo de verifica��o:");
        jLabel3.setEnabled(false);
        pnlData.add(jLabel3);
        jLabel3.setBounds(10, 130, 190, 20);

        cmbRespIndicador.setEnabled(false);
        pnlData.add(cmbRespIndicador);
        cmbRespIndicador.setBounds(450, 350, 400, 22);

        jLabel4.setText("Respons�vel pelo indicador:");
        jLabel4.setEnabled(false);
        pnlData.add(jLabel4);
        jLabel4.setBounds(450, 330, 140, 20);

        cmbRespColeta.setEnabled(false);
        pnlData.add(cmbRespColeta);
        cmbRespColeta.setBounds(450, 390, 400, 22);

        lblColeta.setText("Respons�vel pela coleta:");
        lblColeta.setEnabled(false);
        pnlData.add(lblColeta);
        lblColeta.setBounds(450, 370, 120, 20);

        pnlOrientacao.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)), "Orienta��o"));

        grupoOrientacao.add(rdbAscendente);
        rdbAscendente.setText(bundle1.getString("KpiIndicador_00008")); // NOI18N
        rdbAscendente.setEnabled(false);
        rdbAscendente.setPreferredSize(new java.awt.Dimension(150, 15));
        rdbAscendente.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                rdbAscendenteStateChanged(evt);
            }
        });
        pnlOrientacao.add(rdbAscendente);

        grupoOrientacao.add(rdbMelhorFaixa);
        rdbMelhorFaixa.setText(bundle.getString("KpiIndicador_00112"));
        rdbMelhorFaixa.setEnabled(false);
        rdbMelhorFaixa.setPreferredSize(new java.awt.Dimension(150, 15));
        rdbMelhorFaixa.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                rdbMelhorFaixaStateChanged(evt);
            }
        });
        pnlOrientacao.add(rdbMelhorFaixa);

        grupoOrientacao.add(rdbDescendente);
        rdbDescendente.setText(bundle1.getString("KpiIndicador_00009")); // NOI18N
        rdbDescendente.setEnabled(false);
        rdbDescendente.setPreferredSize(new java.awt.Dimension(150, 15));
        rdbDescendente.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                rdbDescendenteStateChanged(evt);
            }
        });
        pnlOrientacao.add(rdbDescendente);

        pnlData.add(pnlOrientacao);
        pnlOrientacao.setBounds(670, 190, 180, 100);

        tsArvorePrincipal.setCombo(cbbScoreCard);
        tsArvorePrincipal.setRoot(false);
        pnlData.add(tsArvorePrincipal);
        tsArvorePrincipal.setBounds(410, 270, 25, 20);

        lblTolerancia2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTolerancia2.setText(bundle.getString("KpiIndicador_00067")); // NOI18N
        lblTolerancia2.setEnabled(false);
        pnlData.add(lblTolerancia2);
        lblTolerancia2.setBounds(670, 10, 80, 20);

        fldSuperaEm.setModel(new javax.swing.SpinnerNumberModel(0, 0, 100, 1));
        fldSuperaEm.setEnabled(false);
        pnlData.add(fldSuperaEm);
        fldSuperaEm.setBounds(770, 30, 80, 22);

        tsTreeRespInd.setCombo(cmbRespIndicador);
        tsTreeRespInd.setRoot(false);
        pnlData.add(tsTreeRespInd);
        tsTreeRespInd.setBounds(850, 350, 25, 20);

        tsTreeRespCol.setCombo(cmbRespColeta);
        tsTreeRespCol.setRoot(false);
        pnlData.add(tsTreeRespCol);
        tsTreeRespCol.setBounds(850, 390, 25, 20);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("KpiIndicador_00034"), pnlRecord); // NOI18N

        scpFormula.setBorder(null);
        scpFormula.setPreferredSize(new java.awt.Dimension(842, 450));

        pnlFormula.setMaximumSize(new java.awt.Dimension(10, 10));
        pnlFormula.setMinimumSize(new java.awt.Dimension(41, 42));
        pnlFormula.setOpaque(false);
        pnlFormula.setPreferredSize(new java.awt.Dimension(840, 400));
        pnlFormula.setLayout(new java.awt.BorderLayout());

        pnlRecFormula.setPreferredSize(new java.awt.Dimension(212, 212));
        pnlRecFormula.setRequestFocusEnabled(false);
        pnlRecFormula.setLayout(null);

        lblFormIndicador.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblFormIndicador.setText(bundle.getString("KpiIndicador_00021")); // NOI18N
        lblFormIndicador.setEnabled(false);
        lblFormIndicador.setMaximumSize(new java.awt.Dimension(100, 16));
        lblFormIndicador.setMinimumSize(new java.awt.Dimension(100, 16));
        lblFormIndicador.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlRecFormula.add(lblFormIndicador);
        lblFormIndicador.setBounds(20, 90, 95, 20);

        lblFormMeta.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblFormMeta.setText(bundle.getString("KpiIndicador_00022")); // NOI18N
        lblFormMeta.setEnabled(false);
        lblFormMeta.setMaximumSize(new java.awt.Dimension(100, 16));
        lblFormMeta.setMinimumSize(new java.awt.Dimension(100, 16));
        lblFormMeta.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlRecFormula.add(lblFormMeta);
        lblFormMeta.setBounds(20, 130, 95, 20);

        cbbIndFormula.setEnabled(false);
        pnlRecFormula.add(cbbIndFormula);
        cbbIndFormula.setBounds(20, 110, 400, 22);

        cbbMetaFormula.setEnabled(false);
        pnlRecFormula.add(cbbMetaFormula);
        cbbMetaFormula.setBounds(20, 150, 400, 22);

        cbbScoreFormula.setEnabled(false);
        cbbScoreFormula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbScoreFormulaActionPerformed(evt);
            }
        });
        pnlRecFormula.add(cbbScoreFormula);
        cbbScoreFormula.setBounds(20, 70, 400, 22);

        lblScoreFormula.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblScoreFormula.setText(KpiStaticReferences.getKpiCustomLabels().getSco(KpiStaticReferences.CAD_OBJETIVO).concat(":"));
        lblScoreFormula.setEnabled(false);
        lblScoreFormula.setMaximumSize(new java.awt.Dimension(100, 16));
        lblScoreFormula.setMinimumSize(new java.awt.Dimension(100, 16));
        lblScoreFormula.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlRecFormula.add(lblScoreFormula);
        lblScoreFormula.setBounds(20, 50, 120, 20);
        pnlRecFormula.add(txtExpressao);
        txtExpressao.setBounds(20, 190, 400, 22);

        lblExpressao.setText(bundle.getString("KpiIndicador_00025")); // NOI18N
        lblExpressao.setEnabled(false);
        pnlRecFormula.add(lblExpressao);
        lblExpressao.setBounds(20, 170, 180, 20);

        tsArvoreFormula.setCombo(cbbScoreFormula);
        tsArvoreFormula.setRoot(false);
        pnlRecFormula.add(tsArvoreFormula);
        tsArvoreFormula.setBounds(420, 70, 30, 22);

        txtFiltro.setToolTipText("");
        pnlRecFormula.add(txtFiltro);
        txtFiltro.setBounds(20, 30, 400, 22);

        jLabel6.setText("Express�o de Filtro:");
        jLabel6.setToolTipText("Apresentar apenas indicadores que contenham a express�o informada");
        jLabel6.setEnabled(false);
        pnlRecFormula.add(jLabel6);
        jLabel6.setBounds(20, 10, 400, 20);

        tlbAddIndicador.setFloatable(false);

        btnAddIndicador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_verificar.gif"))); // NOI18N
        btnAddIndicador.setEnabled(false);
        btnAddIndicador.setMaximumSize(new java.awt.Dimension(22, 22));
        btnAddIndicador.setMinimumSize(new java.awt.Dimension(22, 22));
        btnAddIndicador.setPreferredSize(new java.awt.Dimension(22, 22));
        btnAddIndicador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddIndicadorActionPerformed(evt);
            }
        });
        tlbAddIndicador.add(btnAddIndicador);

        pnlRecFormula.add(tlbAddIndicador);
        tlbAddIndicador.setBounds(420, 110, 30, 20);

        tlbAddMetaformula.setFloatable(false);

        btnAddMetaFormula.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_verificar.gif"))); // NOI18N
        btnAddMetaFormula.setEnabled(false);
        btnAddMetaFormula.setMaximumSize(new java.awt.Dimension(22, 22));
        btnAddMetaFormula.setMinimumSize(new java.awt.Dimension(22, 22));
        btnAddMetaFormula.setPreferredSize(new java.awt.Dimension(22, 22));
        btnAddMetaFormula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddMetaFormulaActionPerformed(evt);
            }
        });
        tlbAddMetaformula.add(btnAddMetaFormula);

        pnlRecFormula.add(tlbAddMetaformula);
        tlbAddMetaformula.setBounds(420, 150, 40, 20);

        tlbAddExpressao.setFloatable(false);

        btnAddExpressao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_verificar.gif"))); // NOI18N
        btnAddExpressao.setToolTipText(bundle.getString("KpiIndicador_00001")); // NOI18N
        btnAddExpressao.setMaximumSize(new java.awt.Dimension(22, 22));
        btnAddExpressao.setMinimumSize(new java.awt.Dimension(22, 22));
        btnAddExpressao.setPreferredSize(new java.awt.Dimension(22, 22));
        btnAddExpressao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddExpressaoActionPerformed(evt);
            }
        });
        tlbAddExpressao.add(btnAddExpressao);

        pnlRecFormula.add(tlbAddExpressao);
        tlbAddExpressao.setBounds(420, 190, 30, 20);

        scrDescFormula.setRequestFocusEnabled(false);

        txtDescformula.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        txtDescformula.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        scrDescFormula.setViewportView(txtDescformula);

        pnlRecFormula.add(scrDescFormula);
        scrDescFormula.setBounds(460, 30, 400, 60);

        lblDescFormula.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescFormula.setText(bundle.getString("KpiIndicador_00006")); // NOI18N
        lblDescFormula.setEnabled(false);
        lblDescFormula.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDescFormula.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDescFormula.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlRecFormula.add(lblDescFormula);
        lblDescFormula.setBounds(460, 10, 65, 20);

        pnlCalculo.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)), bundle.getString("KpiIndicador_00049"))); // NOI18N
        pnlCalculo.setMinimumSize(new java.awt.Dimension(50, 50));
        pnlCalculo.setPreferredSize(new java.awt.Dimension(90, 90));
        pnlCalculo.setLayout(null);

        chkCalcPrevia.setSelected(true);
        chkCalcPrevia.setText(KpiStaticReferences.getKpiCustomLabels().getPrevia());
        chkCalcPrevia.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkCalcPrevia.setMargin(new java.awt.Insets(0, 0, 0, 0));
        pnlCalculo.add(chkCalcPrevia);
        chkCalcPrevia.setBounds(20, 70, 120, 15);

        chkCalcMeta.setSelected(true);
        chkCalcMeta.setText(KpiStaticReferences.getKpiCustomLabels().getMeta());
        chkCalcMeta.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkCalcMeta.setMargin(new java.awt.Insets(0, 0, 0, 0));
        pnlCalculo.add(chkCalcMeta);
        chkCalcMeta.setBounds(20, 50, 120, 15);

        chkCalcReal.setSelected(true);
        chkCalcReal.setText(KpiStaticReferences.getKpiCustomLabels().getReal());
        chkCalcReal.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        chkCalcReal.setMargin(new java.awt.Insets(0, 0, 0, 0));
        pnlCalculo.add(chkCalcReal);
        chkCalcReal.setBounds(20, 30, 120, 15);

        pnlRecFormula.add(pnlCalculo);
        pnlCalculo.setBounds(460, 100, 400, 113);

        jToolBar3.setFloatable(false);

        btnFormulaRapida.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_wizard.gif"))); // NOI18N
        btnFormulaRapida.setToolTipText("F�rmula R�pida");
        btnFormulaRapida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFormulaRapidaActionPerformed(evt);
            }
        });
        jToolBar3.add(btnFormulaRapida);

        pnlRecFormula.add(jToolBar3);
        jToolBar3.setBounds(420, 30, 30, 20);

        pnlFormula.add(pnlRecFormula, java.awt.BorderLayout.NORTH);

        pnlToolFormula.setLayout(new java.awt.BorderLayout());
        pnlToolFormula.add(pnlLeftForm1, java.awt.BorderLayout.WEST);
        pnlToolFormula.add(pnlRightForm1, java.awt.BorderLayout.EAST);

        edtFormula.setContentType("text/html"); // NOI18N
        edtFormula.setEditable(false);
        scrFormula.setViewportView(edtFormula);

        pnlToolFormula.add(scrFormula, java.awt.BorderLayout.CENTER);

        jBarFormula.setFloatable(false);
        jBarFormula.setRollover(true);
        jBarFormula.setEnabled(false);
        jBarFormula.setMaximumSize(new java.awt.Dimension(0, 25));
        jBarFormula.setMinimumSize(new java.awt.Dimension(0, 25));
        jBarFormula.setPreferredSize(new java.awt.Dimension(40, 40));

        pnlLeftForm2.setMaximumSize(new java.awt.Dimension(20, 20));
        pnlLeftForm2.setMinimumSize(new java.awt.Dimension(20, 20));
        pnlLeftForm2.setPreferredSize(new java.awt.Dimension(20, 20));
        jBarFormula.add(pnlLeftForm2);

        btnVerificarFormula.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_visualizar.gif"))); // NOI18N
        btnVerificarFormula.setText(bundle.getString("KpiIndicador_00023")); // NOI18N
        btnVerificarFormula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerificarFormulaActionPerformed(evt);
            }
        });
        jBarFormula.add(btnVerificarFormula);

        btnLimparFormula.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_limpar.gif"))); // NOI18N
        btnLimparFormula.setText(bundle.getString("KpiIndicador_00024")); // NOI18N
        btnLimparFormula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparFormulaActionPerformed(evt);
            }
        });
        jBarFormula.add(btnLimparFormula);

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator3.setMaximumSize(new java.awt.Dimension(0, 0));
        jSeparator3.setPreferredSize(new java.awt.Dimension(10, 0));
        jBarFormula.add(jSeparator3);

        jSeparator4.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator4.setEnabled(false);
        jSeparator4.setMaximumSize(new java.awt.Dimension(0, 0));
        jSeparator4.setPreferredSize(new java.awt.Dimension(9, 0));
        jSeparator4.setRequestFocusEnabled(false);
        jSeparator4.setVerifyInputWhenFocusTarget(false);
        jBarFormula.add(jSeparator4);

        jSeparator7.setMaximumSize(new java.awt.Dimension(20, 20));
        jSeparator7.setMinimumSize(new java.awt.Dimension(20, 20));
        jSeparator7.setPreferredSize(new java.awt.Dimension(20, 20));
        jBarFormula.add(jSeparator7);

        btnAdicionar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnAdicionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_adicao.gif"))); // NOI18N
        btnAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdicionarActionPerformed(evt);
            }
        });
        jBarFormula.add(btnAdicionar);

        btnRemover.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnRemover.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_subtracao.gif"))); // NOI18N
        btnRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverActionPerformed(evt);
            }
        });
        jBarFormula.add(btnRemover);

        btnMultiplicar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnMultiplicar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_multiplicacao.gif"))); // NOI18N
        btnMultiplicar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMultiplicarActionPerformed(evt);
            }
        });
        jBarFormula.add(btnMultiplicar);

        btnDividir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnDividir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_divisao.gif"))); // NOI18N
        btnDividir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDividirActionPerformed(evt);
            }
        });
        jBarFormula.add(btnDividir);

        btnAbreParentese.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnAbreParentese.setText("(");
        btnAbreParentese.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbreParenteseActionPerformed(evt);
            }
        });
        jBarFormula.add(btnAbreParentese);

        btnFechaParentese.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnFechaParentese.setText(")");
        btnFechaParentese.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFechaParenteseActionPerformed(evt);
            }
        });
        jBarFormula.add(btnFechaParentese);

        jSeparator6.setMaximumSize(new java.awt.Dimension(20, 20));
        jSeparator6.setMinimumSize(new java.awt.Dimension(20, 20));
        jSeparator6.setPreferredSize(new java.awt.Dimension(20, 20));
        jBarFormula.add(jSeparator6);

        btnVoltar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_voltar.gif"))); // NOI18N
        btnVoltar.setActionCommand("<<");
        btnVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVoltarActionPerformed(evt);
            }
        });
        jBarFormula.add(btnVoltar);

        jSeparator8.setMaximumSize(new java.awt.Dimension(20, 20));
        jSeparator8.setMinimumSize(new java.awt.Dimension(20, 20));
        jSeparator8.setPreferredSize(new java.awt.Dimension(20, 20));
        jBarFormula.add(jSeparator8);

        pnlToolFormula.add(jBarFormula, java.awt.BorderLayout.PAGE_START);
        pnlToolFormula.add(pnlBottomForm, java.awt.BorderLayout.PAGE_END);

        pnlFormula.add(pnlToolFormula, java.awt.BorderLayout.CENTER);

        scpFormula.setViewportView(pnlFormula);

        tapCadastro.addTab(bundle1.getString("KpiIndicador_00032"), scpFormula); // NOI18N

        pnlValores.setLayout(new java.awt.BorderLayout());

        jBarPlanilha.setFloatable(false);
        jBarPlanilha.setRollover(true);
        jBarPlanilha.setPreferredSize(new java.awt.Dimension(206, 27));

        btnPlanNovo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_novo.gif"))); // NOI18N
        btnPlanNovo.setText(bundle.getString("KpiIndicador_00028")); // NOI18N
        btnPlanNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPlanNovoActionPerformed(evt);
            }
        });
        jBarPlanilha.add(btnPlanNovo);

        btnPlanExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnPlanExcluir.setText(bundle2.getString("KpiIndicador_00029")); // NOI18N
        btnPlanExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPlanExcluirActionPerformed(evt);
            }
        });
        jBarPlanilha.add(btnPlanExcluir);

        btnPlanExcluirTudo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir_tudo.gif"))); // NOI18N
        btnPlanExcluirTudo.setText(bundle.getString("KpiIndicador_00081")); // NOI18N
        btnPlanExcluirTudo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPlanExcluirTudoActionPerformed(evt);
            }
        });
        jBarPlanilha.add(btnPlanExcluirTudo);
        jBarPlanilha.add(jSeparator5);

        lblYearFilter.setText(bundle.getString("KpiIndicador_00083")); // NOI18N
        jBarPlanilha.add(lblYearFilter);

        cbbYearFilter.setMaximumSize(new java.awt.Dimension(180, 20));
        cbbYearFilter.setMinimumSize(new java.awt.Dimension(180, 20));
        cbbYearFilter.setPreferredSize(new java.awt.Dimension(180, 22));
        cbbYearFilter.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbYearFilterItemStateChanged(evt);
            }
        });
        jBarPlanilha.add(cbbYearFilter);

        pnlValores.add(jBarPlanilha, java.awt.BorderLayout.NORTH);

        pnlPlanilha.setLayout(new java.awt.BorderLayout());
        pnlPlanilha.add(tblPlanilhaValor, java.awt.BorderLayout.CENTER);

        pnlValores.add(pnlPlanilha, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("KpiIndicador_00033"), pnlValores); // NOI18N

        pnlMeta.setLayout(new java.awt.BorderLayout());

        pnlDadosMeta.setPreferredSize(new java.awt.Dimension(250, 0));
        pnlDadosMeta.setLayout(null);

        pnlAlvos.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)), bundle1.getString("KpiIndicador_00111"))); // NOI18N
        pnlAlvos.setLayout(null);

        pnlAlvo.setPreferredSize(new java.awt.Dimension(120, 60));
        pnlAlvo.setLayout(new java.awt.BorderLayout());

        jPnlCores.setPreferredSize(new java.awt.Dimension(120, 100));
        jPnlCores.setLayout(new java.awt.GridLayout(4, 0));

        scrValorAzul.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        scrValorAzul.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrValorAzul.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        pvValorAzul.setBackground(new java.awt.Color(109, 178, 247));
        pvValorAzul.setBounds(new java.awt.Rectangle(0, 0, 60, 20));
        pvValorAzul.setButtonBorder(1);
        pvValorAzul.setDisabledForeground(new java.awt.Color(0, 0, 0));
        pvValorAzul.setDialog(true);
        pvValorAzul.m_modal = true;

        if(KpiStaticReferences.getDecimalSeparator().equals(",")){
            final JPVCalculator calcValorAzul = pvValorAzul.getCalculator();

            calcValorAzul.button(48).addActionListener(new java.awt.event.ActionListener(){
                public void actionPerformed(java.awt.event.ActionEvent evt){
                    calculadoraValorAzul(evt, calcValorAzul);
                }
            });
        }
        scrValorAzul.setViewportView(pvValorAzul);
        pvValorAzul.setAlignment(2);

        jPnlCores.add(scrValorAzul);

        scrValorVerde.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        scrValorVerde.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrValorVerde.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        pvValorVerde.setBackground(new java.awt.Color(142, 201, 62));
        pvValorVerde.setBorderStyle(0);
        pvValorVerde.setBounds(new java.awt.Rectangle(0, 0, 60, 20));
        pvValorVerde.setButtonBorder(1);
        pvValorVerde.setDisabledForeground(new java.awt.Color(0, 0, 0));
        pvValorVerde.setDialog(true);
        pvValorVerde.m_modal = true;

        if(KpiStaticReferences.getDecimalSeparator().equals(",")){
            final JPVCalculator calcValorVerde = pvValorVerde.getCalculator();

            calcValorVerde.button(48).addActionListener(new java.awt.event.ActionListener(){
                public void actionPerformed(java.awt.event.ActionEvent evt){
                    calculadoraValorVerde(evt, calcValorVerde);
                }
            });
        }
        scrValorVerde.setViewportView(pvValorVerde);
        pvValorVerde.setAlignment(2);

        jPnlCores.add(scrValorVerde);

        pnlAmarelo.setBackground(new java.awt.Color(252, 252, 194));
        pnlAmarelo.setLayout(null);
        jPnlCores.add(pnlAmarelo);

        scrValorVermelho.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        scrValorVermelho.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrValorVermelho.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        pvValorVermelho.setBackground(new java.awt.Color(234, 140, 146));
        pvValorVermelho.setBorderStyle(0);
        pvValorVermelho.setBounds(new java.awt.Rectangle(0, 0, 60, 20));
        pvValorVermelho.setButtonBorder(1);
        pvValorVermelho.setDisabledForeground(new java.awt.Color(0, 0, 0));
        pvValorVermelho.setDialog(true);
        pvValorVermelho.m_modal = true;

        if(KpiStaticReferences.getDecimalSeparator().equals(",")){
            final JPVCalculator calcValorVermelho = pvValorVermelho.getCalculator();

            calcValorVermelho.button(48).addActionListener(new java.awt.event.ActionListener(){
                public void actionPerformed(java.awt.event.ActionEvent evt){
                    calculadoraValorVermelho(evt, calcValorVermelho);
                }
            });
        }
        scrValorVermelho.setViewportView(pvValorVermelho);
        pvValorVermelho.setAlignment(2);

        jPnlCores.add(scrValorVermelho);

        pnlAlvo.add(jPnlCores, java.awt.BorderLayout.CENTER);

        lblSetaDesce.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_move_para_baixo.gif"))); // NOI18N
        lblSetaDesce.setInheritsPopupMenu(false);
        lblSetaDesce.setOpaque(true);
        pnlAlvo.add(lblSetaDesce, java.awt.BorderLayout.WEST);

        lblSetaSobe.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_move_para_cima.gif"))); // NOI18N
        lblSetaSobe.setInheritsPopupMenu(false);
        lblSetaSobe.setOpaque(true);
        pnlAlvo.add(lblSetaSobe, java.awt.BorderLayout.EAST);

        pnlAlvos.add(pnlAlvo);
        pnlAlvo.setBounds(10, 20, 130, 100);

        pnlDadosMeta.add(pnlAlvos);
        pnlAlvos.setBounds(30, 30, 150, 140);

        pnlMetaLote.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)), "Meta em lote"));
        pnlMetaLote.setLayout(null);

        scrDataAte.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrDataAte.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        scrDataAte.setPreferredSize(new java.awt.Dimension(90, 22));

        fldDataAte.setBorderStyle(0);
        fldDataAte.setBounds(new java.awt.Rectangle(0, 0, 100, 0));
        fldDataAte.setButtonBorder(0);
        fldDataAte.setDialog(true);
        fldDataAte.setUseLocale(true);
        scrDataAte.setViewportView(fldDataAte);

        pnlMetaLote.add(scrDataAte);
        scrDataAte.setBounds(20, 80, 100, 22);

        scrDataDe.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrDataDe.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        fldDataDe.setBorderStyle(0);
        fldDataDe.setBounds(new java.awt.Rectangle(0, 0, 100, 0));
        fldDataDe.setButtonBorder(0);
        fldDataDe.setDialog(true);
        fldDataDe.setUseLocale(true);
        scrDataDe.setViewportView(fldDataDe);

        pnlMetaLote.add(scrDataDe);
        scrDataDe.setBounds(20, 40, 100, 22);

        lblPeriodoDe.setText("Per�odo de:");
        pnlMetaLote.add(lblPeriodoDe);
        lblPeriodoDe.setBounds(20, 20, 80, 20);

        lblPeriodoAte.setText("Per�odo At�:");
        pnlMetaLote.add(lblPeriodoAte);
        lblPeriodoAte.setBounds(20, 60, 70, 20);

        lblMeta.setText("Meta:");
        pnlMetaLote.add(lblMeta);
        lblMeta.setBounds(20, 100, 80, 20);
        pnlMetaLote.add(txtMeta);
        txtMeta.setBounds(20, 120, 100, 22);

        pnlDadosMeta.add(pnlMetaLote);
        pnlMetaLote.setBounds(30, 180, 150, 160);

        pnlMeta.add(pnlDadosMeta, java.awt.BorderLayout.WEST);

        tapCadastro.addTab(bundle1.getString("KpiTabIndicador_00004"), pnlMeta); // NOI18N

        pnlWidget.setLayout(new java.awt.BorderLayout());

        pnlWdgtData.setMaximumSize(new java.awt.Dimension(30, 250));
        pnlWdgtData.setMinimumSize(new java.awt.Dimension(30, 250));
        pnlWdgtData.setPreferredSize(new java.awt.Dimension(30, 400));
        pnlWdgtData.setLayout(null);

        scrlPainel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        scrlPainel.setAutoscrolls(true);

        dskPaineis.setBackground(new java.awt.Color(212, 208, 200));
        dskPaineis.setOpaque(false);
        dskPaineis.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                dskPaineisComponentResized(evt);
            }
        });
        scrlPainel.setViewportView(dskPaineis);

        pnlWdgtData.add(scrlPainel);
        scrlPainel.setBounds(150, 30, 250, 270);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_botaovoltar.gif"))); // NOI18N
        pnlWdgtData.add(jLabel2);
        jLabel2.setBounds(400, 230, 20, 20);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_botaoavancar.gif"))); // NOI18N
        pnlWdgtData.add(jLabel5);
        jLabel5.setBounds(130, 90, 20, 20);

        lblGraphTypesDown.setText(bundle2.getString("KpiIndicador_00094")); // NOI18N
        lblGraphTypesDown.setEnabled(false);
        pnlWdgtData.add(lblGraphTypesDown);
        lblGraphTypesDown.setBounds(440, 170, 90, 22);

        cboGraphTypesDown.setEnabled(false);
        cboGraphTypesDown.setMaximumSize(new java.awt.Dimension(28, 20));
        pnlWdgtData.add(cboGraphTypesDown);
        cboGraphTypesDown.setBounds(440, 190, 90, 22);

        lblGraphVisualDown.setText(bundle2.getString("KpiIndicador_00093")); // NOI18N
        lblGraphVisualDown.setEnabled(false);
        pnlWdgtData.add(lblGraphVisualDown);
        lblGraphVisualDown.setBounds(440, 210, 90, 22);

        cboGraphVisualDown.setEnabled(false);
        cboGraphVisualDown.setMaximumSize(new java.awt.Dimension(28, 20));
        pnlWdgtData.add(cboGraphVisualDown);
        cboGraphVisualDown.setBounds(440, 230, 90, 22);

        lblGraphValuesDown.setText(bundle2.getString("KpiIndicador_00097")); // NOI18N
        lblGraphValuesDown.setEnabled(false);
        pnlWdgtData.add(lblGraphValuesDown);
        lblGraphValuesDown.setBounds(440, 250, 91, 22);

        cboGraphValuesDown.setEnabled(false);
        pnlWdgtData.add(cboGraphValuesDown);
        cboGraphValuesDown.setBounds(440, 270, 90, 22);

        lblGraphTypesUp.setText(bundle2.getString("KpiIndicador_00094")); // NOI18N
        lblGraphTypesUp.setEnabled(false);
        pnlWdgtData.add(lblGraphTypesUp);
        lblGraphTypesUp.setBounds(20, 30, 90, 22);

        cboGraphTypesUp.setEnabled(false);
        cboGraphTypesUp.setMaximumSize(new java.awt.Dimension(28, 20));
        pnlWdgtData.add(cboGraphTypesUp);
        cboGraphTypesUp.setBounds(20, 50, 90, 22);

        lblGraphVisualUp.setText(bundle2.getString("KpiIndicador_00093")); // NOI18N
        lblGraphVisualUp.setEnabled(false);
        pnlWdgtData.add(lblGraphVisualUp);
        lblGraphVisualUp.setBounds(20, 70, 90, 22);

        cboGraphVisualUp.setEnabled(false);
        cboGraphVisualUp.setMaximumSize(new java.awt.Dimension(28, 20));
        pnlWdgtData.add(cboGraphVisualUp);
        cboGraphVisualUp.setBounds(20, 90, 90, 22);

        lblGraphValuesUp.setText(bundle2.getString("KpiIndicador_00097")); // NOI18N
        lblGraphValuesUp.setEnabled(false);
        pnlWdgtData.add(lblGraphValuesUp);
        lblGraphValuesUp.setBounds(20, 110, 100, 22);

        cboGraphValuesUp.setEnabled(false);
        pnlWdgtData.add(cboGraphValuesUp);
        cboGraphValuesUp.setBounds(20, 130, 90, 22);

        pnlWidget.add(pnlWdgtData, java.awt.BorderLayout.PAGE_START);

        tapCadastro.addTab("Widget", pnlWidget);

        pnlDw.setLayout(new java.awt.BorderLayout());

        pnlFieldsDW.setMinimumSize(new java.awt.Dimension(600, 500));
        pnlFieldsDW.setPreferredSize(new java.awt.Dimension(100, 135));
        pnlFieldsDW.setLayout(null);
        pnlFieldsDW.add(fldUrlDW);
        fldUrlDW.setBounds(20, 30, 400, 22);

        cbbDataWareHouse.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbDataWareHouseItemStateChanged(evt);
            }
        });
        pnlFieldsDW.add(cbbDataWareHouse);
        cbbDataWareHouse.setBounds(20, 70, 400, 22);
        pnlFieldsDW.add(cbbConsultas);
        cbbConsultas.setBounds(20, 110, 400, 22);

        lblConsulta.setText(bundle.getString("KpiIndicador_00085")); // NOI18N
        pnlFieldsDW.add(lblConsulta);
        lblConsulta.setBounds(20, 90, 60, 20);
        lblConsulta.getAccessibleContext().setAccessibleName("Consulta:");

        lblDatawarehouse.setText("DataWareHouse:");
        pnlFieldsDW.add(lblDatawarehouse);
        lblDatawarehouse.setBounds(20, 50, 100, 20);

        lblEnderecoDW.setText("URL DW:");
        pnlFieldsDW.add(lblEnderecoDW);
        lblEnderecoDW.setBounds(20, 10, 100, 20);

        jToolBar1.setFloatable(false);

        btnPostEndereco.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnPostEndereco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_verificar.gif"))); // NOI18N
        btnPostEndereco.setMaximumSize(new java.awt.Dimension(22, 22));
        btnPostEndereco.setMinimumSize(new java.awt.Dimension(22, 22));
        btnPostEndereco.setPreferredSize(new java.awt.Dimension(22, 22));
        btnPostEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPostEnderecoActionPerformed(evt);
            }
        });
        jToolBar1.add(btnPostEndereco);

        pnlFieldsDW.add(jToolBar1);
        jToolBar1.setBounds(420, 30, 30, 20);

        jToolBar2.setFloatable(false);

        btnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_verificar.gif"))); // NOI18N
        btnAdd.setMaximumSize(new java.awt.Dimension(22, 22));
        btnAdd.setMinimumSize(new java.awt.Dimension(22, 22));
        btnAdd.setPreferredSize(new java.awt.Dimension(22, 22));
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        jToolBar2.add(btnAdd);

        pnlFieldsDW.add(jToolBar2);
        jToolBar2.setBounds(420, 110, 30, 20);

        pnlDw.add(pnlFieldsDW, java.awt.BorderLayout.NORTH);

        pnlTableDW.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        pnlTableDW.setPreferredSize(new java.awt.Dimension(20, 20));
        pnlTableDW.setLayout(new java.awt.BorderLayout());

        toolQuery.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        toolQuery.setFloatable(false);
        toolQuery.setRollover(true);
        toolQuery.setPreferredSize(new java.awt.Dimension(40, 40));

        btnAbrirTabela.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_visualizar.gif"))); // NOI18N
        btnAbrirTabela.setText(bundle.getString("JBIListPanel_00002")); // NOI18N
        btnAbrirTabela.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirTabelaActionPerformed(evt);
            }
        });
        toolQuery.add(btnAbrirTabela);

        btnRemove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnRemove.setText(bundle.getString("KpiBaseFrame_00004")); // NOI18N
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });
        toolQuery.add(btnRemove);

        sepQuery.setOrientation(javax.swing.SwingConstants.VERTICAL);
        sepQuery.setMaximumSize(new java.awt.Dimension(10, 20));
        sepQuery.setPreferredSize(new java.awt.Dimension(0, 0));
        toolQuery.add(sepQuery);

        jLabel1.setText(bundle.getString("KpiIndicador_00086")); // NOI18N
        toolQuery.add(jLabel1);

        grupoOpcaoDW.add(optTabela);
        optTabela.setSelected(true);
        optTabela.setText(bundle.getString("KpiIndicador_00087")); // NOI18N
        optTabela.setMaximumSize(new java.awt.Dimension(70, 21));
        optTabela.setPreferredSize(new java.awt.Dimension(20, 20));
        toolQuery.add(optTabela);

        grupoOpcaoDW.add(optGrafico);
        optGrafico.setText(bundle.getString("KpiIndicador_00088")); // NOI18N
        optGrafico.setMaximumSize(new java.awt.Dimension(70, 21));
        toolQuery.add(optGrafico);

        pnlTableDW.add(toolQuery, java.awt.BorderLayout.NORTH);
        pnlTableDW.add(tblConsultaDW, java.awt.BorderLayout.CENTER);
        tblConsultaDW.getTable().addMouseListener(new java.awt.event.MouseAdapter() {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblQuerySourceMouseClicked(evt);
            }
        });

        pnlDw.add(pnlTableDW, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("KpiIndicador_00089"), pnlDw); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 29));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle1.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle1.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setPreferredSize(new java.awt.Dimension(350, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle1.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnEdit.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        cmbDuplicar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_duplicador.gif"))); // NOI18N
        cmbDuplicar.setText(bundle.getString("KpiDuplicador_00008")); // NOI18N
        cmbDuplicar.setFocusable(false);
        cmbDuplicar.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        cmbDuplicar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        cmbDuplicar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        cmbDuplicar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbDuplicarActionPerformed(evt);
            }
        });
        tbInsertion.add(cmbDuplicar);

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle1.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnDelete.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle1.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnReload.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnAjuda2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda2.setText(bundle.getString("KpiGraphIndicador_00011")); // NOI18N
        btnAjuda2.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnAjuda2.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnAjuda2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjuda2ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda2);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        getContentPane().add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlBottomForm1.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm1, java.awt.BorderLayout.SOUTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnAjuda2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjuda2ActionPerformed
            event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjuda2ActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            if (preparaXmlJustificativa()) {//Justificativa alteracao de meta
                event.saveRecord();
                insereMetaEmLote();
            }
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();

            // Ao cancelar, atualiza todos os dados
            event.loadRecord();
            if (PlanilhaDeValoresBackUp != null) {
                tblPlanilhaValor.setDataSource(PlanilhaDeValoresBackUp.clone2());
                applyRenderIndicador(tblPlanilhaValor);
            }
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            habilitaMsg = true;
            event.editRecord();
            vctJustificativa.removeAll();
	}//GEN-LAST:event_btnEditActionPerformed

        private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
            int linhaSelecionada = tblConsultaDW.getSelectedRow();
            if (linhaSelecionada > -1) {
                kpi.xml.BIXMLRecord recLinha = tblConsultaDW.getXMLData().get(linhaSelecionada);
                tabQuery.addRecordDeleted(recLinha);
                tblConsultaDW.removeRecord();
            } else {
                dialog.informationMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00075"));
            }
}//GEN-LAST:event_btnRemoveActionPerformed

        private void btnAbrirTabelaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirTabelaActionPerformed
            int opcao = 1;
            if (optGrafico.isSelected()) {
                opcao = 2;
            }
            showConsulta(opcao);
}//GEN-LAST:event_btnAbrirTabelaActionPerformed

        private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
            boolean lAddNew = false;
            String dw_url = "";
            String dw_name = "";
            String dw_con = "";
            String dw_idCons = "";

            if (!fldUrlDW.getText().equals("")) {
                dw_url = fldUrlDW.getText().trim().toLowerCase();
                if (dw_url.contains(".aph")) {
                    dw_url = dw_url.substring(0, dw_url.lastIndexOf("/"));
                }
                if (dw_url.endsWith("/")) {
                    dw_url = dw_url.substring(0, dw_url.length() - 1);
                }
                lAddNew = true;
            }

            if (lAddNew && cbbDataWareHouse.getSelectedIndex() > 0) {
                dw_name = cbbDataWareHouse.getSelectedItem().toString();
                lAddNew = true;
            }

            if (lAddNew && cbbConsultas.getSelectedIndex() > 0) {
                dw_con = cbbConsultas.getSelectedItem().toString();
                dw_idCons = event.getComboValue(htbConsultas, cbbConsultas);
                lAddNew = true;
            }

            if (lAddNew) {
                boolean f0 = tblConsultaDW.seekInColumn(0, dw_url);
                boolean f1 = tblConsultaDW.seekInColumn(1, dw_name);
                boolean f2 = tblConsultaDW.seekInColumn(2, dw_con);
                if (f0 && f1 && f2) {
                    dialog.informationMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00076"));
                } else {
                    tblConsultaDW.addNewRecord(tabQuery.createNewRecord(dw_url, dw_name, dw_con, dw_idCons));
                }
            } else {
                dialog.informationMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00077"));
            }
}//GEN-LAST:event_btnAddActionPerformed

        private void cbbDataWareHouseItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbDataWareHouseItemStateChanged
            if (evt.getStateChange() == ItemEvent.DESELECTED) {
                int itemSelecionado = cbbDataWareHouse.getSelectedIndex();
                if (itemSelecionado > 0) {
                    kpi.xml.BIXMLVector dwConsultas = recDataWareHouse.getBIXMLVector("DWCONSULTAS");
                    kpi.xml.BIXMLRecord recConsultas = dwConsultas.get(itemSelecionado - 1);

                    //Se for maior que dois existem itens de consulta.
                    if (recConsultas.size() > 2) {
                        kpi.xml.BIXMLVector vctConsultas = recConsultas.getBIXMLVector("QUERY_LISTS");
                        event.populateCombo(vctConsultas, "ID", htbConsultas, cbbConsultas);
                    } else {
                        cbbConsultas.removeAllItems();
                    }
                } else {
                    cbbConsultas.removeAllItems();
                }
            }
}//GEN-LAST:event_cbbDataWareHouseItemStateChanged

        private void btnPostEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPostEnderecoActionPerformed
            kpi.core.KpiStaticReferences.getKpiMainPanel().setStatusBarCondition(1);
            kpi.core.KpiStaticReferences.getKpiMainPanel().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));

            String sDwUrl = fldUrlDW.getText().trim().toLowerCase();
            if (sDwUrl.contains(".aph")) {
                sDwUrl = sDwUrl.substring(0, sDwUrl.lastIndexOf("/"));
            }
            if (sDwUrl.endsWith("/")) {
                sDwUrl = sDwUrl.substring(0, sDwUrl.length() - 1);
            }

            StringBuilder request = new StringBuilder("REQ_DATAWAREHOUSE");
            request.append("|");
            request.append(sDwUrl);
            request.append("|");
            request.append("false"); //Carregar detalhes da consulta

            //Requisitando os dados.
            recDataWareHouse = event.loadRecord("-1", "DWCONSULTA", request.toString());
            if (recDataWareHouse != null) {
                fldUrlDW.setText(sDwUrl);
                event.populateCombo(recDataWareHouse.getBIXMLVector("DWCONSULTAS"), "DWCONSULTA", htbDataWareHouse, cbbDataWareHouse);
                isConectedDW = true;
            } else {
                isConectedDW = false;
                cbbDataWareHouse.removeAllItems();
                cbbConsultas.removeAllItems();
            }

            //Habilita os campos da integra��o com o SIGADW.
            cbbDataWareHouse.setEnabled(isConectedDW);
            cbbConsultas.setEnabled(isConectedDW);
            btnAdd.setEnabled(isConectedDW);

            kpi.core.KpiStaticReferences.getKpiMainPanel().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
            kpi.core.KpiStaticReferences.getKpiMainPanel().setStatusBarCondition(0);
}//GEN-LAST:event_btnPostEnderecoActionPerformed

        private void cbbYearFilterItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbYearFilterItemStateChanged
            if (evt.getStateChange() == ItemEvent.SELECTED) {
                updYear();
            }
}//GEN-LAST:event_cbbYearFilterItemStateChanged
    /**
     *
     */
    private void updYear() {
        if (cbbYearFilter.getItemCount() > 0) {
            StringBuilder parametros = new StringBuilder();
            parametros.append("FILTER_YEAR");
            parametros.append("|");
            parametros.append(event.getComboValue(htbYearFilter, cbbYearFilter, true));

            kpi.xml.BIXMLRecord recFilter = event.loadRecord(getID(), "INDICADOR", parametros.toString());
            kpi.xml.BIXMLVector vecPlan = recFilter.getBIXMLVector("PLANILHAS");

            //Verifica se existe algum item marcado para exclus�o
            kpi.xml.BIXMLRecord recDel = tabIndicador.getDelDados();
            if (recDel != null) {
                kpi.xml.BIXMLVector vecDel = recDel.getBIXMLVector("EXCLUIDOS");
                for (int i = 0; i < vecDel.size(); i++) {
                    for (int j = 0; j < vecPlan.size(); j++) {
                        if (vecPlan.get(j).getString("ID").equals(vecDel.get(i).getString("ID"))) {
                            vecPlan.remove(j);
                        }
                    }
                }
            }

            tblPlanilhaValor.setDataSource(vecPlan);
            applyRenderIndicador(tblPlanilhaValor);
            setHistoricoPlanilhaDeValores(vecPlan);//carrega HashMap com os dados da planilha de valores
        }
    }

        private void btnPlanExcluirTudoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPlanExcluirTudoActionPerformed
            kpi.swing.KpiDefaultDialogSystem oDialog = new kpi.swing.KpiDefaultDialogSystem(this);

            String cAno = event.getComboValue(htbYearFilter, cbbYearFilter, true);
            StringBuilder cMsg = new StringBuilder();
            if (cAno.trim().length() > 0) {
                //Deseja excluir todos os registros do Ano de YYYY?
                cMsg.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00082"));
                cMsg.append(cAno);
                cMsg.append("?");
            } else {
                //"Deseja excluir todos os registros da planilha de valores?"
                cMsg.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00080"));
            }

            int iRet = oDialog.confirmMessage(cMsg.toString(), 2);
            if (iRet == javax.swing.JOptionPane.OK_OPTION) {
                int iQtd = tblPlanilhaValor.getModel().getRowCount();
                for (int i = 0; i < iQtd; i++) {
                    BIXMLRecord recLinha = tblPlanilhaValor.getXMLData().get(0);
                    if (recLinha.getBoolean("DELETE_LINE") || isAdmin) {
                        tabIndicador.addRecordDeleted(tblPlanilhaValor.getXMLData().get(0));
                        tblPlanilhaValor.removeRecord(0);
                    } else {
                        dialog.informationMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00055"));
                        break;
                    }
                }
            }
}//GEN-LAST:event_btnPlanExcluirTudoActionPerformed

        private void btnPlanExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPlanExcluirActionPerformed
            int linhaSelecionada = tblPlanilhaValor.getSelectedRow();
            if (linhaSelecionada > -1) {
                BIXMLRecord recLinha = tblPlanilhaValor.getXMLData().get(linhaSelecionada);
                if (recLinha.getBoolean("DELETE_LINE") || isAdmin) {
                    tabIndicador.addRecordDeleted(tblPlanilhaValor.getXMLData().get(linhaSelecionada));
                    tblPlanilhaValor.removeRecord();
                } else {
                    dialog.informationMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00055"));
                }
            }
}//GEN-LAST:event_btnPlanExcluirActionPerformed

        private void btnPlanNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPlanNovoActionPerformed

            // Condi��o criada abaixo pelo fato de quando o combo de 'filtrar por ano' mudar, criar a estrutura 
            // novamente da tabela de valores.
            if (tblPlanilhaValor.getXMLData().size() == 0) {
                carregaPlanilha();
            }

            System.out.println(tblPlanilhaValor.getXMLData().size());
            tblPlanilhaValor.addNewRecord(tabIndicador.createNewRecord());
}//GEN-LAST:event_btnPlanNovoActionPerformed

        private void chkConsolidadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkConsolidadorActionPerformed
            //Quando o indicador for consolidador META e PREVIA n�o s�o calculados.
            if (chkConsolidador.isSelected()) {
                //Define as regras para indicadores consolidadores.
                aplicaRegraConsolidador(true);
                //Remove qualquer f�rmula que possa ser incompat�vel com o consolidador.
                limpaFormula();
            } else {
                //Remove as regras para indicadores consolidadores.
                aplicaRegraConsolidador(false);
            }
}//GEN-LAST:event_chkConsolidadorActionPerformed

        private void btnAddExpressaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddExpressaoActionPerformed
            if (txtExpressao != null) {
                String expressao = txtExpressao.getText();
                if (expressao != null && !(expressao.trim().equals(""))) {
                    if (expressao.contains("-") || expressao.contains("+") || expressao.contains("/") || expressao.contains("*")) {
                        dialog.errorMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00045"));
                    } else {
                        KpiItemFormula formula = new KpiItemFormula(txtExpressao.getText(), txtExpressao.getText());
                        addFormula(formula);
                        txtExpressao.setText("");
                    }
                }
            }
}//GEN-LAST:event_btnAddExpressaoActionPerformed

        private void btnAddMetaFormulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddMetaFormulaActionPerformed
            KpiItemFormula formula;
            if (cbbMetaFormula.getItemCount() > 1) {
                //
                formula = new KpiItemFormula("(", "(");
                addFormula(formula);
                txtExpressao.setText("");
                //
                kpi.util.KpiComboString descSelected = (kpi.util.KpiComboString) cbbMetaFormula.getSelectedItem();
                String descricao = descSelected.toString();
                String expressao = event.getComboValue(htbMetaFormula, cbbMetaFormula);
                formula = new KpiItemFormula(expressao, descricao);
                formula.setITipo(KpiItemFormula.METAFORMULA);
                addFormula(formula);
                //
                formula = new KpiItemFormula(")", ")");
                addFormula(formula);
                txtExpressao.setText("");
            }
}//GEN-LAST:event_btnAddMetaFormulaActionPerformed

        private void btnAddIndicadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddIndicadorActionPerformed
            if (!"".equals(cbbIndFormula.getSelectedItem().toString())) {
                kpi.util.KpiComboString descSelected = (kpi.util.KpiComboString) cbbIndFormula.getSelectedItem();
                String descricao = descSelected.toString();
                String expressao = event.getComboValue(htbIndFormula, cbbIndFormula);
                KpiItemFormula formula = new KpiItemFormula(expressao, descricao);
                formula.setITipo(KpiItemFormula.INDICADOR);
                formula.setIdScoreCard(event.getComboValue(htbScoreCard, cbbScoreFormula));
                addFormula(formula);
            }
}//GEN-LAST:event_btnAddIndicadorActionPerformed

        private void cbbScoreCardItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbScoreCardItemStateChanged
            if (evt.getStateChange() == ItemEvent.SELECTED && cbbScoreCard.getItemCount() > 0) {
                StringBuilder selectInd = new StringBuilder("ID_SCOREC = '");
                selectInd.append(event.getComboValue(htbScoreCard, cbbScoreCard)).append("'");

                if (getStatus() != KpiDefaultFrameBehavior.INSERTING) {
                    selectInd.append(" and ID != '");
                    selectInd.append(this.getID()).append("'");
                    selectInd.append(" and ISOWNER != 'F'");

                    scoSelected = event.getComboValue(htbScoreCard, cbbScoreCard);
                } else {
                    selectInd.append(" and ID != '0'");
                    selectInd.append(" and ISOWNER != 'F'");
                }

                kpi.xml.BIXMLRecord recIndFiltrados = event.listRecords("INDICADOR", selectInd.toString());
                event.populateCombo(recIndFiltrados.getBIXMLVector("INDICADORES"), "INDICADOR", htbIndicadores, cbbParentIndicador);
            }
}//GEN-LAST:event_cbbScoreCardItemStateChanged

        private void cbbFrequenciaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbbFrequenciaKeyPressed
            lastItemFreq = cbbFrequencia.getSelectedIndex();
}//GEN-LAST:event_cbbFrequenciaKeyPressed

        private void cbbFrequenciaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbFrequenciaItemStateChanged
            if (!chkEstrategico.isSelected()) {
                if (evt.getStateChange() == ItemEvent.SELECTED && lResetPeriodo) {

                    if (habilitaMsg) {
                        if (dialog.confirmMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00030")) == javax.swing.JOptionPane.YES_OPTION) {
                            limpaPlanilha();
                            lResetPeriodo = true;
                        } else {
                            lResetPeriodo = false;
                            cbbFrequencia.setSelectedIndex(lastItemFreq);
                        }
                    } else {
                        limpaPlanilha();
                        lResetPeriodo = true;
                        //cbbFrequencia.setSelectedIndex(lastItemFreq);
                    }

                } else if (evt.getStateChange() == ItemEvent.SELECTED) {
                    lResetPeriodo = true;
                }
            }
}//GEN-LAST:event_cbbFrequenciaItemStateChanged

        private void cbbFrequenciaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbbFrequenciaMousePressed
            lastItemFreq = cbbFrequencia.getSelectedIndex();
}//GEN-LAST:event_cbbFrequenciaMousePressed

        private void rdbAscendenteStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_rdbAscendenteStateChanged
            this.configuraAlvo();
}//GEN-LAST:event_rdbAscendenteStateChanged

    private void rdbDescendenteStateChanged(javax.swing.event.ChangeEvent evt) {
        this.configuraAlvo();
    }

    private void rdbMelhorFaixaStateChanged(javax.swing.event.ChangeEvent evt) {
        this.configuraAlvo();
    }

        private void dskPaineisComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_dskPaineisComponentResized
            dskPaineis.autoOrganizeFrames();
}//GEN-LAST:event_dskPaineisComponentResized

        private void cmbDuplicarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbDuplicarActionPerformed
            event.duplicateRecord();

            //Inclui o prefixo "DUP" nos campos que precisam ser alterados.
            String nome = record.getString("NOME");
            String codigo = record.getString("ID_CODCLI");

            fldNome.setText("DUP_".concat(nome));
            fldCodImp.setText(codigo.isEmpty() ? "" : "DUP_".concat(codigo));

            //Remove todos os dados da planilha de valor.
            limpaPlanilha();

            //Identifica a opera��o como uma duplica��o. 
            this.setIsDuplicacao(true);
        }//GEN-LAST:event_cmbDuplicarActionPerformed

        private void btnVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVoltarActionPerformed
            if (vctFormula.size() > 0) {
                vctFormula.removeElementAt(vctFormula.size() - 1);
                refreshFormulaText();
            }
}//GEN-LAST:event_btnVoltarActionPerformed

        private void btnFechaParenteseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFechaParenteseActionPerformed
            KpiItemFormula formula = new KpiItemFormula(")", ")");
            addFormula(formula);
}//GEN-LAST:event_btnFechaParenteseActionPerformed

        private void btnAbreParenteseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbreParenteseActionPerformed
            KpiItemFormula formula = new KpiItemFormula("(", "(");
            addFormula(formula);
}//GEN-LAST:event_btnAbreParenteseActionPerformed

        private void btnDividirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDividirActionPerformed
            KpiItemFormula formula = new KpiItemFormula("/", "/");
            addFormula(formula);
}//GEN-LAST:event_btnDividirActionPerformed

        private void btnMultiplicarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMultiplicarActionPerformed
            KpiItemFormula formula = new KpiItemFormula("*", "*");
            addFormula(formula);
}//GEN-LAST:event_btnMultiplicarActionPerformed

        private void btnRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverActionPerformed
            KpiItemFormula formula = new KpiItemFormula("-", "-");
            addFormula(formula);
}//GEN-LAST:event_btnRemoverActionPerformed

        private void btnAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdicionarActionPerformed
            KpiItemFormula formula = new KpiItemFormula("+", "+");
            addFormula(formula);
}//GEN-LAST:event_btnAdicionarActionPerformed

        private void btnLimparFormulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparFormulaActionPerformed
            limpaFormula();
}//GEN-LAST:event_btnLimparFormulaActionPerformed

        private void btnVerificarFormulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerificarFormulaActionPerformed
            StringBuilder parametros = new StringBuilder();

            parametros.append("FORMULA");
            parametros.append("#");
            parametros.append(formulaToString());

            event.executeRecord(parametros.toString());
}//GEN-LAST:event_btnVerificarFormulaActionPerformed

        private void cbbScoreFormulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbScoreFormulaActionPerformed
            if (cbbScoreFormula.getItemCount() > 0) {
                StringBuilder parametros = new StringBuilder("ID_SCOREC = '");
                parametros.append(event.getComboValue(htbScoreCard, cbbScoreFormula)).append("'");

                if (getStatus() != KpiDefaultFrameBehavior.INSERTING) {
                    parametros.append(" AND ");
                    parametros.append(" ID != '");
                    parametros.append(this.getID()).append("'");
                } else {
                    parametros.append(" AND ");
                    parametros.append(" ID != '0'");
                }

                if (!txtFiltro.getText().isEmpty()) {
                    parametros.append(" AND ");
                    parametros.append(" NOME LIKE '%");
                    parametros.append(txtFiltro.getText());
                    parametros.append("%'");
                }

                kpi.xml.BIXMLRecord recIndFiltrados = event.listRecords("INDICADOR", parametros.toString());
                event.populateCombo(recIndFiltrados.getBIXMLVector("INDICADORES"), "INDICADOR", htbIndFormula, cbbIndFormula);
            }
        }//GEN-LAST:event_cbbScoreFormulaActionPerformed

        private void btnFormulaRapidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFormulaRapidaActionPerformed
            String frmRapida = java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00109");//"F�rmula R�pida"

            if (txtFiltro.getText().length() >= 3) {
                KpiBuscaIndicador selectionDialog = new KpiBuscaIndicador(
                        KpiStaticReferences.getKpiMainPanel(),
                        frmRapida,
                        true,
                        this,
                        txtFiltro.getText(),
                        chkConsolidador.isSelected());
                selectionDialog.setVisible(true);
            } else {
                String msg = java.util.ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00110");//"A express�o de filtro deve conter no m�nimo tr�s caracteres."
                dialog.informationMessage(msg);
            }
        }//GEN-LAST:event_btnFormulaRapidaActionPerformed

    private void rdbDescendenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbDescendenteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdbDescendenteActionPerformed

    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
    }//GEN-LAST:event_formInternalFrameActivated

    private void applyRenderIndicador(kpi.beans.JBIXMLTable oTable) {

        indicadorCellRenderer.setQtdDecimais(((Integer) spnCasasDecimais.getValue()).intValue());
        oTable.getTable().setColumnSelectionAllowed(true);

        for (int col = 0; col < oTable.getTable().getColumnCount(); col++) {
            String colName = oTable.getColumnTag(col);
            Integer freq = new Integer(event.getComboValue(htbFrequencia, cbbFrequencia));
            KpiIndicadorCellEditor editor = new KpiIndicadorCellEditor(createJPVNumeric(colName, freq.intValue()), pv.jfcx.JPVTable.DOUBLE);
            editor.setFrequencia(freq.intValue());
            editor.setBiTable(tblPlanilhaValor);
            tblPlanilhaValor.getColumn(col).setCellEditor(editor);
            oTable.getColumn(col).setCellRenderer(indicadorCellRenderer);

            //Exibe a coluna de log em tamanho reduzido. 
            if (tblPlanilhaValor.getColumn(col).getHeaderValue().toString().equalsIgnoreCase("LOG")) {
                tblPlanilhaValor.getColumn(col).setMaxWidth(120);
                tblPlanilhaValor.getColumn(col).setMinWidth(120);
            }
        }
    }

    void addFormula(KpiItemFormula expFormula) {
        vctFormula.add(expFormula);
        refreshFormulaText();
        txtExpressao.requestFocus();
    }

    /**
     * Monta a string da formula a partir de vetor vctFormula;
     *
     */
    private String formulaToString() {
        KpiItemFormula iteFormula;
        StringBuilder txtRetorno = new StringBuilder();

        for (java.util.Iterator itemForm = vctFormula.iterator(); itemForm.hasNext();) {
            iteFormula = (KpiItemFormula) itemForm.next();
            switch (iteFormula.getITipo()) {
                case 2:	//INDICADOR	= 2;
                    txtRetorno.append("|I.".concat(iteFormula.getStrExpressao()));
                    break;
                case 3: //METAFORMULA = 3;
                    txtRetorno.append("|M.".concat(iteFormula.getStrExpressao()));
                    break;
                default:
                    txtRetorno.append("|".concat(iteFormula.getStrExpressao()));
                    break;
            }
        }

        return txtRetorno.toString();
    }

    /**
     * Monta o vetor vctFormula a partir de uma string passada.
     */
    private void stringToFormula(String strFormula) {
        StringTokenizer itens_formula = new StringTokenizer(strFormula, "|");
        String metaformula = new String();
        String nome = "";
        String id_indicador = "";
        String id_scorecard = "";
        int itemFound = 0;
        int indicador = 0;
        BIXMLVector oFormula = null;

        limpaFormula();

        while (itens_formula.hasMoreElements()) {
            String sNextToken = itens_formula.nextToken();
            KpiItemFormula formula = new KpiItemFormula();

            if (sNextToken.contains("I.") || sNextToken.contains("M.")) {
                metaformula = sNextToken.substring(2, sNextToken.length());

                if (sNextToken.contains("I.")) {
                    oFormula = record.getBIXMLVector("LISTAFORMULA");

                    if (oFormula.size() > 0 && (oFormula.size() - 1) >= indicador) {
                        BIXMLRecord xmlFormula = oFormula.get(indicador);
                        formula.setITipo(KpiItemFormula.INDICADOR);
                        nome = xmlFormula.getString("INDICA_NAME");
                        id_indicador = xmlFormula.getString("INDICA_ID");
                        id_scorecard = xmlFormula.getString("SCORE_ID");
                    } else {
                        nome = "";
                        id_indicador = "";
                        id_scorecard = "";
                    }

                    formula.setStrDescricao(nome);
                    formula.setStrExpressao(id_indicador);
                    formula.setIdScoreCard(id_scorecard);

                    indicador++;
                } else {
                    formula.setITipo(KpiItemFormula.METAFORMULA);
                    oFormula = record.getBIXMLVector("METASFORMULA");
                    itemFound = event.getComboItem(oFormula, metaformula, false);

                    if (itemFound > -1) {
                        formula.setStrDescricao(oFormula.get(itemFound).getString("NOME"));
                        formula.setStrExpressao(metaformula);
                    }
                }
            } else {
                formula.setStrDescricao(sNextToken);
                formula.setStrExpressao(sNextToken);
            }

            addFormula(formula);
        }

        refreshFormulaText();
    }

    /*
     * Mostra o texto da formula adicionada.
     */
    private void refreshFormulaText() {
        StringBuilder html = new StringBuilder();

        for (java.util.Iterator iter = vctFormula.iterator(); iter.hasNext();) {
            KpiItemFormula formula = (KpiItemFormula) iter.next();

            if (formula.getITipo() == KpiItemFormula.INDICADOR) {
                String scoreName = "";

                int retItem = event.getComboItem(vctScoreCard, formula.getIdScoreCard(), false);
                if (retItem != -1 && vctScoreCard.size() > 0) {
                    scoreName = vctScoreCard.get(retItem).getString("NOME");
                }

                html.append(scoreName);
                html.append("->");
                html.append("<font color='#27408B'>");
                html.append(formula.getStrDescricao());
                html.append("</font>");
            } else {
                html.append("<font color='red'><b>");
                html.append(" ");
                html.append(formula.getStrDescricao());
                html.append(" ");
                html.append("</b></font>");
            }
        }

        edtFormula.setText(html.toString());
    }

    private void limpaFormula() {
        vctFormula.removeAllElements();
        edtFormula.setText("");
    }

    private void insereMetaEmLote() {

        if (isMetaEmLote()) {//valida preenchimento dos campos necess�rios para inserir meta em lote

            StringBuilder parametros = new StringBuilder();

            parametros.append("META_EM_LOTE");
            parametros.append("#");
            parametros.append(new KpiDateUtil().getCalendarString(fldDataDe.getCalendar()));
            parametros.append("#");
            parametros.append(new KpiDateUtil().getCalendarString(fldDataAte.getCalendar()));
            parametros.append("#");
            parametros.append(txtMeta.getText());

            event.executeRecord(parametros.toString());

        }
        fldDataDe.setText("");
        fldDataAte.setText("");
        txtMeta.setText("");
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAbreParentese;
    private javax.swing.JButton btnAbrirTabela;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnAddExpressao;
    private javax.swing.JButton btnAddIndicador;
    private javax.swing.JButton btnAddMetaFormula;
    private javax.swing.JButton btnAdicionar;
    private javax.swing.JButton btnAjuda2;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnDividir;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnFechaParentese;
    private javax.swing.JButton btnFormulaRapida;
    private javax.swing.JButton btnLimparFormula;
    private javax.swing.JButton btnMultiplicar;
    private javax.swing.JButton btnPlanExcluir;
    private javax.swing.JButton btnPlanExcluirTudo;
    private javax.swing.JButton btnPlanNovo;
    private javax.swing.JButton btnPostEndereco;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnRemove;
    private javax.swing.JButton btnRemover;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnVerificarFormula;
    private javax.swing.JButton btnVoltar;
    private javax.swing.JComboBox cbbConsultas;
    private javax.swing.JComboBox cbbDataWareHouse;
    private javax.swing.JComboBox cbbFrequencia;
    private javax.swing.JComboBox cbbGrupo;
    private javax.swing.JComboBox cbbIndFormula;
    private javax.swing.JComboBox cbbMetaFormula;
    private javax.swing.JComboBox cbbParentIndicador;
    private javax.swing.JComboBox cbbScoreCard;
    private javax.swing.JComboBox cbbScoreFormula;
    private javax.swing.JComboBox cbbTipoAcumulado;
    private javax.swing.JComboBox cbbTipoAtualizacao;
    private javax.swing.JComboBox cbbUnidade;
    private javax.swing.JComboBox cbbYearFilter;
    private javax.swing.JComboBox cboGraphTypesDown;
    private javax.swing.JComboBox cboGraphTypesUp;
    private javax.swing.JComboBox cboGraphValuesDown;
    private javax.swing.JComboBox cboGraphValuesUp;
    private javax.swing.JComboBox cboGraphVisualDown;
    private javax.swing.JComboBox cboGraphVisualUp;
    private javax.swing.JCheckBox chkCalcMeta;
    private javax.swing.JCheckBox chkCalcPrevia;
    private javax.swing.JCheckBox chkCalcReal;
    private javax.swing.JCheckBox chkConsolidador;
    private javax.swing.JCheckBox chkEstrategico;
    private javax.swing.JCheckBox chkIndPai;
    private javax.swing.JCheckBox chkVisivel;
    private javax.swing.JButton cmbDuplicar;
    private javax.swing.JComboBox cmbRespColeta;
    private javax.swing.JComboBox cmbRespIndicador;
    private kpi.beans.JBICardDesktopPane dskPaineis;
    private javax.swing.JEditorPane edtFormula;
    private pv.jfcx.JPVEdit fldCodImp;
    private pv.jfcx.JPVDatePlus fldDataAte;
    private pv.jfcx.JPVDatePlus fldDataDe;
    private pv.jfcx.JPVEdit fldLink;
    private pv.jfcx.JPVEdit fldNome;
    private javax.swing.JSpinner fldSuperaEm;
    private javax.swing.JSpinner fldTolerancia;
    private pv.jfcx.JPVEdit fldUrlDW;
    private javax.swing.ButtonGroup grupoOpcaoDW;
    private javax.swing.ButtonGroup grupoOrientacao;
    private javax.swing.JToolBar jBarFormula;
    private javax.swing.JToolBar jBarPlanilha;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPnlCores;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JToolBar.Separator jSeparator5;
    private javax.swing.JToolBar.Separator jSeparator6;
    private javax.swing.JToolBar.Separator jSeparator7;
    private javax.swing.JToolBar.Separator jSeparator8;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JToolBar jToolBar3;
    private javax.swing.JLabel lblCasasDecimais;
    private javax.swing.JLabel lblCodImp;
    private javax.swing.JLabel lblColeta;
    private javax.swing.JLabel lblConsulta;
    private javax.swing.JLabel lblDatawarehouse;
    private javax.swing.JLabel lblDescFormula;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblDia1;
    private javax.swing.JLabel lblEnderecoDW;
    private javax.swing.JLabel lblExpressao;
    private javax.swing.JLabel lblFormIndicador;
    private javax.swing.JLabel lblFormMeta;
    private javax.swing.JLabel lblFrequencia;
    private javax.swing.JLabel lblGraphTypesDown;
    private javax.swing.JLabel lblGraphTypesUp;
    private javax.swing.JLabel lblGraphValuesDown;
    private javax.swing.JLabel lblGraphValuesUp;
    private javax.swing.JLabel lblGraphVisualDown;
    private javax.swing.JLabel lblGraphVisualUp;
    private javax.swing.JLabel lblGrupo;
    private javax.swing.JLabel lblLink;
    private javax.swing.JLabel lblMeta;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblParentIndicador;
    private javax.swing.JLabel lblPeriodoAte;
    private javax.swing.JLabel lblPeriodoDe;
    private javax.swing.JLabel lblPeso;
    private javax.swing.JLabel lblScoreCard;
    private javax.swing.JLabel lblScoreFormula;
    private javax.swing.JLabel lblSetaDesce;
    private javax.swing.JLabel lblSetaSobe;
    private javax.swing.JLabel lblSuperaEm;
    private javax.swing.JLabel lblTipoAcumulado;
    private javax.swing.JLabel lblTipoAtualizacao;
    private javax.swing.JLabel lblTolerancia2;
    private javax.swing.JLabel lblUnidade;
    private javax.swing.JLabel lblYearFilter;
    private javax.swing.JRadioButton optGrafico;
    private javax.swing.JRadioButton optTabela;
    private javax.swing.JPanel pnlAlvo;
    private javax.swing.JPanel pnlAlvos;
    private javax.swing.JPanel pnlAmarelo;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomForm1;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlCalculo;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlDadosMeta;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlDw;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlFieldsDW;
    private javax.swing.JPanel pnlFormula;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftForm1;
    private javax.swing.JPanel pnlLeftForm2;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlMeta;
    private javax.swing.JPanel pnlMetaLote;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlOrientacao;
    private javax.swing.JPanel pnlPlanilha;
    private javax.swing.JPanel pnlRecFormula;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightForm1;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTableDW;
    private javax.swing.JPanel pnlTipo;
    private javax.swing.JPanel pnlToolFormula;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JPanel pnlValores;
    private javax.swing.JPanel pnlWdgtData;
    private javax.swing.JPanel pnlWidget;
    private pv.jfcx.JPVNumericPlus pvValorAzul;
    private pv.jfcx.JPVNumericPlus pvValorVerde;
    private pv.jfcx.JPVNumericPlus pvValorVermelho;
    private javax.swing.JRadioButton rdbAscendente;
    private javax.swing.JRadioButton rdbDescendente;
    private javax.swing.JRadioButton rdbMelhorFaixa;
    private javax.swing.JScrollPane scpFormula;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JScrollPane scrDataAte;
    private javax.swing.JScrollPane scrDataDe;
    private javax.swing.JScrollPane scrDescFormula;
    private javax.swing.JScrollPane scrDescricao;
    private javax.swing.JScrollPane scrFormula;
    private javax.swing.JScrollPane scrValorAzul;
    private javax.swing.JScrollPane scrValorVerde;
    private javax.swing.JScrollPane scrValorVermelho;
    private javax.swing.JScrollPane scrVerificacao;
    private javax.swing.JScrollPane scrlPainel;
    private javax.swing.JSeparator sepQuery;
    private javax.swing.JSpinner spnCasasDecimais;
    private javax.swing.JSpinner spnDia;
    private javax.swing.JSpinner spnPeso;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private kpi.beans.JBIXMLTable tblConsultaDW;
    private kpi.beans.JBIXMLTable tblPlanilhaValor;
    private javax.swing.JToolBar tlbAddExpressao;
    private javax.swing.JToolBar tlbAddIndicador;
    private javax.swing.JToolBar tlbAddMetaformula;
    private javax.swing.JToolBar toolQuery;
    private kpi.beans.JBITreeSelection tsArvoreFormula;
    private kpi.beans.JBITreeSelection tsArvorePrincipal;
    private kpi.beans.JBITreeSelection tsTreeRespCol;
    private kpi.beans.JBITreeSelection tsTreeRespInd;
    private kpi.beans.JBITextArea txtDescformula;
    private kpi.beans.JBITextArea txtDescricao;
    private pv.jfcx.JPVEdit txtExpressao;
    private javax.swing.JTextField txtFiltro;
    private pv.jfcx.JPVNumeric txtMeta;
    private javax.swing.JTextArea txtVerificacao;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    /*
     * Armazena o conte�do booleano da propriedade do indicador.
     */
    private String isPrivate = new String();
    private kpi.xml.BIXMLRecord record = null;

    /**
     * Evento de clique do Mouse na planilha.
     *
     */
    private void tblQuerySourceMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getClickCount() == 2) {
            int opcao = 1;
            if (optGrafico.isSelected()) {
                opcao = 2;
            }
            showConsulta(opcao);
        }
    }

    private void calculadoraValorVerde(java.awt.event.ActionEvent evt, JPVCalculator calculadora) {
        pvValorVerde.setText(calculadora.getText().replace('.', ','));
    }

    private void calculadoraValorVermelho(java.awt.event.ActionEvent evt, JPVCalculator calculadora) {
        pvValorVermelho.setText(calculadora.getText().replace('.', ','));
    }

    private void calculadoraValorAzul(java.awt.event.ActionEvent evt, JPVCalculator calculadora) {
        pvValorAzul.setText(calculadora.getText().replace('.', ','));
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        if ("COMMANDS".equals(recordAux.getTagName())) {
            BIXMLAttributes att = recordAux.getAttributes();

            if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
                parentType = "FORMMATRIX";
                scoSelected = att.getString("PARENTID");

                cbbScoreCard.setVisible(false);
                lblScoreCard.setVisible(false);
                tsArvorePrincipal.setVisible(false);

                if (record != null) {
                    record.set("ID_SCOREC", scoSelected);
                    populateFilteredInd();
                }
            }
        } else {
            if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
                cbbScoreCard.setVisible(true);
                lblScoreCard.setVisible(true);
                tsArvorePrincipal.setVisible(true);
                tsArvorePrincipal.setEntitySelection(KpiStaticReferences.CAD_OBJETIVO);
            }
            record = recordAux;
        }
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        btnSave.setEnabled(true);
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    private pv.jfcx.JPVNumeric createJPVNumeric(String colName, int frequencia) {
        pv.jfcx.JPVNumeric oCellEditor = new pv.jfcx.JPVNumeric();
        oCellEditor.setEnableArrows(false);
        oCellEditor.setLowerValidation(true);
        oCellEditor.setUpperValidation(true);
        oCellEditor.setValidateLimitsOnKey(true);
        oCellEditor.setUseLocale(true);

        switch (frequencia) {
            case kpi.core.KpiStaticReferences.KPI_FREQ_ANUAL:
                break;
            case kpi.core.KpiStaticReferences.KPI_FREQ_SEMESTRAL:
                if (colName.equals("MES")) {
                    oCellEditor.setMinValue(0);
                    oCellEditor.setMaxValue(2);
                }
                break;
            case kpi.core.KpiStaticReferences.KPI_FREQ_QUADRIMESTRAL:
                if (colName.equals("MES")) {
                    oCellEditor.setMinValue(0);
                    oCellEditor.setMaxValue(3);
                }
                break;
            case kpi.core.KpiStaticReferences.KPI_FREQ_TRIMESTRAL:
                if (colName.equals("MES")) {
                    oCellEditor.setMinValue(0);
                    oCellEditor.setMaxValue(4);
                }
                break;
            case kpi.core.KpiStaticReferences.KPI_FREQ_BIMESTRAL:
                if (colName.equals("MES")) {
                    oCellEditor.setMinValue(0);
                    oCellEditor.setMaxValue(6);
                }
                break;
            case kpi.core.KpiStaticReferences.KPI_FREQ_MENSAL:
                if (colName.equals("MES")) {
                    oCellEditor.setMinValue(0);
                    oCellEditor.setMaxValue(12);
                }
                break;
            case kpi.core.KpiStaticReferences.KPI_FREQ_QUINZENAL:
                if (colName.equals("MES")) {
                    oCellEditor.setMinValue(0);
                    oCellEditor.setMaxValue(12);
                } else if (colName.equals("DIA")) {
                    oCellEditor.setMinValue(0);
                    oCellEditor.setMaxValue(2);
                }
                break;
            case kpi.core.KpiStaticReferences.KPI_FREQ_SEMANAL:
                if (colName.equals("MES")) {
                    oCellEditor.setMinValue(0);
                    oCellEditor.setMaxValue(52);
                }
                break;
            case kpi.core.KpiStaticReferences.KPI_FREQ_DIARIA:
                if (colName.equals("MES")) {
                    oCellEditor.setMinValue(0);
                    oCellEditor.setMaxValue(12);
                } else if (colName.equals("DIA")) {
                    oCellEditor.setMinValue(0);
                    oCellEditor.setMaxValue(31);
                }
                break;
        }

        return oCellEditor;
    }

    /**
     * Seta o valor do campo ITENS_CAL
     *
     * @return Valor do campo.
     */
    private String getOpcaoCalculo() {
        StringBuilder opcCalc = new StringBuilder("");

        if (chkCalcReal.isSelected() && chkCalcMeta.isSelected() && chkCalcPrevia.isSelected()) {
            opcCalc.append("0");
        } else {
            if (chkCalcReal.isSelected()) {
                opcCalc.append("1");
            }

            if (chkCalcMeta.isSelected()) {
                opcCalc.append("2");
            }

            if (chkCalcPrevia.isSelected()) {
                opcCalc.append("3");
            }
        }

        return opcCalc.toString();
    }

    /**
     * Configura as op��es de c�lculo.
     *
     * @param opcCalc Valor do campo "ITENS_CAL"
     */
    private void setOpcaoCalculo(String opcCalc) {

        if (opcCalc.contains("0")) {
            chkCalcReal.setSelected(true);
            chkCalcMeta.setSelected(true);
            chkCalcPrevia.setSelected(true);
        } else {

            if (opcCalc.contains("1")) {
                chkCalcReal.setSelected(true);
            } else {
                chkCalcReal.setSelected(false);
            }

            if (opcCalc.contains("2")) {
                chkCalcMeta.setSelected(true);
            } else {
                chkCalcMeta.setSelected(false);
            }

            if (opcCalc.contains("3")) {
                chkCalcPrevia.setSelected(true);
            } else {
                chkCalcPrevia.setSelected(false);
            }
        }
    }

    /**
     * Popula as combos com os indicadores pai.
     */
    private void populateFilteredInd() {
        String scoIDSelected = record.getString("ID_SCOREC");

        if (scoIDSelected.equals("") && !KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
            scoIDSelected = event.getComboValue(htbScoreCard, cbbScoreCard);
        }

        if (!scoIDSelected.equals("")) {
            StringBuilder selectInd = new StringBuilder("ID_SCOREC = '"); //NOI18N
            selectInd.append(scoIDSelected);
            selectInd.append("' and ID != '"); //NOI18N
            selectInd.append(this.getID()).append("'"); //NOI18N
            selectInd.append(" and ISOWNER != 'F'"); //NOI18N
            kpi.xml.BIXMLRecord recIndFiltrados = event.listRecords("INDICADOR", selectInd.toString()); //NOI18N
            BIXMLVector vctIndFiltrados = recIndFiltrados.getBIXMLVector("INDICADORES"); //NOI18N
            //Indicador Pai do Indicador.
            event.populateCombo(vctIndFiltrados, "INDICADOR", htbIndicadores, cbbParentIndicador); //NOI18N
            event.selectComboItem(cbbParentIndicador, event.getComboItem(vctIndFiltrados, record.getString("ID_INDICA"), true));
        }
    }

    private void showConsulta(int iTipo) {
        try {
            int lin = tblConsultaDW.getSelectedRow();
            if (lin > -1) {
                kpi.xml.BIXMLRecord recLinha = tblConsultaDW.getXMLData().get(lin);
                StringBuffer request = new StringBuffer("REQUEST_DWACCESS|");
                request.append(recLinha.getString("DW_URL").concat("|"));
                request.append(recLinha.getString("DW_NAME"));
                kpi.xml.BIXMLRecord rRequest = event.loadRecord("-1", "DWCONSULTA", request.toString());
                //Carregando os dados da consulta

                String sDwUrl = recLinha.getString("DW_URL").toLowerCase().trim();
                if (!sDwUrl.startsWith("http://")) {
                    sDwUrl = "http://".concat(sDwUrl);
                }

                if (sDwUrl.substring(sDwUrl.length(), sDwUrl.length()).equals("/")) {
                    sDwUrl = sDwUrl.substring(0, sDwUrl.length() - 1);
                }
                request = new StringBuffer();
                request.append(sDwUrl);
                request.append("/h_m01showcons.apw?resetwindow=2&loadcons=true&issched=on&info=");
                request.append(String.valueOf(iTipo));
                request.append("&id=");
                request.append(recLinha.getString("DW_IDCON"));
                request.append(rRequest.getString("REQUEST"));

                /*
                 * Requisita a URL para exibi��o da consulta do DW.
                 */
                KpiToolKit oToolKit = new KpiToolKit();
                /*
                 * Transforma o request em uma URL.
                 */
                URL url = new URL(request.toString());
                /*
                 * Realiza a abertura do arquivo no Browser.
                 */
                oToolKit.browser(url);

            } else {
                dialog.informationMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00078"));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public Double getAlvoVerde() {
        return pvValorVerde.getDouble();
    }

    private boolean preparaXmlJustificativa() {
        BIXMLRecord dadosJust = new BIXMLRecord("INFO_JUSTIFICATIVA");
        dadosJust.set("INDICADOR", fldNome.getText().trim());
        dadosJust.set("DECIMAIS", ((Integer) spnCasasDecimais.getValue()).intValue());
        KpiValoresPlanilha vlrAux = null;
        BIXMLVector vctPlanilha = tblPlanilhaValor.getXMLData().clone2();
        int nFrequencia = 0;

        updateXMLJustificativaOk = true;

        //Verifica Alteracao de Alvo
        if (record.getBoolean("INFO_ALT_META") && pvValorVerde.getDouble() != vlrAlvoVerde) {

            setTipoJustificativa(1);//Meta

            dadosJust.set("METAANT", vlrAlvoVerde);
            dadosJust.set("METAINF", pvValorVerde.getDouble());
            dadosJust.set("PERIODODE", "");
            dadosJust.set("PERIODOATE", "");

            KpiJustificativaMeta dlgJustificativa = new KpiJustificativaMeta(
                    KpiStaticReferences.getKpiMainPanel(),
                    ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiJustificativaMeta_00001")/*
                     * Justificativa de Altera��o de Meta
                     */,
                    true,
                    dadosJust,
                    this);
            dlgJustificativa.setVisible(true);
        }

        //Verifica Altera��o de Meta em Lote
        if (updateXMLJustificativaOk && record.getBoolean("INFO_ALT_META") && pvValorVerde.getDouble() == 0.0 && isMetaEmLote()) {

            setTipoJustificativa(3);//Meta Em Lote

            dadosJust.set("METAANT", 0.0);
            dadosJust.set("METAINF", txtMeta.getDouble());
            dadosJust.set("PERIODODE", fldDataDe.getText());
            dadosJust.set("PERIODOATE", fldDataAte.getText());

            KpiJustificativaMeta dlgJustificativa = new KpiJustificativaMeta(
                    KpiStaticReferences.getKpiMainPanel(),
                    ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiJustificativaMeta_00001")/*
                     * Justificativa de Altera��o de Meta
                     */,
                    true,
                    dadosJust,
                    this);
            dlgJustificativa.setVisible(true);
        }

        //Verifica Altera��o de Planilha de Valores
        if (updateXMLJustificativaOk) {
            if (record.getBoolean("INFO_ALT_META")) {
                String key;
                setTipoJustificativa(2);//Planilha de Valores

                nFrequencia = cbbFrequencia.getSelectedIndex();

                if (nFrequencia == kpi.core.KpiStaticReferences.KPI_FREQ_TRIMESTRAL) {
                    nFrequencia = kpi.core.KpiStaticReferences.KPI_FREQ_QUADRIMESTRAL;
                } else if (nFrequencia == kpi.core.KpiStaticReferences.KPI_FREQ_QUADRIMESTRAL) {
                    nFrequencia = kpi.core.KpiStaticReferences.KPI_FREQ_TRIMESTRAL;
                }

                for (int j = 0; j < vctPlanilha.size(); j++) {
                    key = vctPlanilha.get(j).getString("ID");
                    if (historicoPlanilha.containsKey(key)) {
                        vlrAux = (KpiValoresPlanilha) historicoPlanilha.get(key);
                        //hmpPlanilha.remove(key);

                        if (!vlrAux.getStringMeta().equals(vctPlanilha.get(j).getString("META"))) {

                            //Muda o comando SQL para XML.
                            BIXMLAttributes att = new BIXMLAttributes();
                            att.set("XML_COMMAND", true); //NOI18N

                            //Requisi��o
                            BIXMLRecord request = new BIXMLRecord("CMDSQL", att); //NOI18N
                            request.set("COMANDO", "GET_PERIODO"); //NOI18N
                            request.set("FREQUENCIA", nFrequencia); //NOI18N
                            request.set("ANO", vctPlanilha.get(j).getString("ANO")); //NOI18N
                            request.set("MES", vctPlanilha.get(j).getString("MES")); //NOI18N
                            request.set("DIA", vctPlanilha.get(j).getString("DIA")); //NOI18N
                            request.set("PERIODODE", fldDataDe.getText()); //NOI18N
                            request.set("PERIODOATE", fldDataAte.getText()); //NOI18N
                            request.set("METALOTE", isMetaEmLote());//NOI18N

                            BIXMLRecord lista = event.listRecords("INDICADOR", request.toString()); //NOI18N

                            if (lista.getBIXMLVector("PERIODOS").getAttributes().getBoolean("JUSTIFICA")) {

                                dadosJust.set("METAANT", vlrAux.getMeta());
                                dadosJust.set("METAINF", vctPlanilha.get(j).getString("META"));
                                dadosJust.set("PERIODODE", lista.getBIXMLVector("PERIODOS").getAttributes().getString("PERIODO"));
                                dadosJust.set("PERIODOATE", "");

                                KpiJustificativaMeta dlgJustificativa = new KpiJustificativaMeta(
                                        KpiStaticReferences.getKpiMainPanel(),
                                        ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiJustificativaMeta_00001")/*
                                         * Justificativa de Altera��o de Meta
                                         */,
                                        true,
                                        dadosJust,
                                        this);
                                dlgJustificativa.setVisible(true);

                                if (!updateXMLJustificativaOk) {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        if (updateXMLJustificativaOk && !isDuplicacao()) {
            //Verifica Itens Excluidos da Planilha de Valores
            BIXMLRecord recDel = tabIndicador.getDelDados();
            if (record.getBoolean("INFO_EXC_META") && recDel != null) {
                setTipoJustificativa(4);//Excluidos
                BIXMLVector vecDel = recDel.getBIXMLVector("EXCLUIDOS");

                for (int i = 0; i < vecDel.size(); i++) {

                    //Muda o comando SQL para XML.
                    BIXMLAttributes att = new BIXMLAttributes();
                    att.set("XML_COMMAND", true); //NOI18N

                    //Requisi��o
                    BIXMLRecord request = new BIXMLRecord("CMDSQL", att); //NOI18N
                    request.set("COMANDO", "GET_PERIODO"); //NOI18N
                    request.set("FREQUENCIA", nFrequencia); //NOI18N
                    request.set("ANO", vecDel.get(i).getString("ANO")); //NOI18N
                    request.set("MES", vecDel.get(i).getString("MES")); //NOI18N
                    request.set("DIA", vecDel.get(i).getString("DIA")); //NOI18N
                    request.set("PERIODODE", fldDataDe.getText()); //NOI18N
                    request.set("PERIODOATE", fldDataAte.getText()); //NOI18N
                    request.set("METALOTE", isMetaEmLote());//         request.set("PERIODODE", fldDataDe.getText()); //NOI18NNOI18N

                    BIXMLRecord lista = event.listRecords("INDICADOR", request.toString()); //NOI18N

                    dadosJust.set("METAANT", vecDel.get(i).getString("META"));
                    dadosJust.set("METAINF", 0.0);
                    dadosJust.set("PERIODODE", lista.getBIXMLVector("PERIODOS").getAttributes().getString("PERIODO"));
                    dadosJust.set("PERIODOATE", "");

                    KpiJustificativaMeta dlgJustificativa = new KpiJustificativaMeta(
                            KpiStaticReferences.getKpiMainPanel(),
                            ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiJustificativaMeta_00001")/*
                             * Justificativa de Altera��o de Meta
                             */,
                            true,
                            dadosJust,
                            this);
                    dlgJustificativa.setVisible(true);

                    if (!updateXMLJustificativaOk) {
                        break;
                    }
                }
            }
        }

        //Verifica Itens Incluidos na Planilha de Valores
        if (updateXMLJustificativaOk && record.getBoolean("INFO_INC_META")) {
            String key;
            setTipoJustificativa(5);//Novo

            for (int j = 0; j < vctPlanilha.size(); j++) {
                key = vctPlanilha.get(j).getString("ID");

                if (key.contains("N_E_W_")) {

                    //Muda o comando SQL para XML.
                    BIXMLAttributes att = new BIXMLAttributes();
                    att.set("XML_COMMAND", true); //NOI18N

                    //Requisi��o
                    BIXMLRecord request = new BIXMLRecord("CMDSQL", att); //NOI18N
                    request.set("COMANDO", "GET_PERIODO"); //NOI18N
                    request.set("FREQUENCIA", nFrequencia); //NOI18N
                    request.set("ANO", vctPlanilha.get(j).getString("ANO")); //NOI18N
                    request.set("MES", vctPlanilha.get(j).getString("MES")); //NOI18N
                    request.set("DIA", vctPlanilha.get(j).getString("DIA")); //NOI18N
                    request.set("PERIODODE", fldDataDe.getText()); //NOI18N
                    request.set("PERIODOATE", fldDataAte.getText()); //NOI18N
                    request.set("METALOTE", isMetaEmLote());//         request.set("PERIODODE", fldDataDe.getText()); //NOI18NNOI18N

                    BIXMLRecord lista = event.listRecords("INDICADOR", request.toString()); //NOI18N

                    if (lista.getBIXMLVector("PERIODOS").getAttributes().getBoolean("JUSTIFICA")) {

                        dadosJust.set("METAANT", 0.0);
                        dadosJust.set("METAINF", vctPlanilha.get(j).getString("META"));
                        dadosJust.set("PERIODODE", lista.getBIXMLVector("PERIODOS").getAttributes().getString("PERIODO"));
                        dadosJust.set("PERIODOATE", "");

                        KpiJustificativaMeta dlgJustificativa = new KpiJustificativaMeta(
                                KpiStaticReferences.getKpiMainPanel(),
                                ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()).getString("KpiJustificativaMeta_00001")/*
                                 * Justificativa de Altera��o de Meta
                                 */,
                                true,
                                dadosJust,
                                this);
                        dlgJustificativa.setVisible(true);

                        if (!updateXMLJustificativaOk) {
                            break;
                        }
                    }
                }
            }
        }

        return updateXMLJustificativaOk;
    }

    public void setTipoJustificativa(int i) {
        tipo_justificativa = i;
    }

    public int getTipoJustificativa() {
        return tipo_justificativa;
    }

    /**
     *
     */
    private void setHistoricoPlanilhaDeValores() {
        setHistoricoPlanilhaDeValores(record.getBIXMLVector("PLANILHAS").clone2());
    }

    /**
     *
     * @param vlrPlanilha
     */
    private void setHistoricoPlanilhaDeValores(BIXMLVector planilhaDeValores) {
        historicoPlanilha.clear();

        for (int i = 0; i < planilhaDeValores.size(); i++) {
            KpiValoresPlanilha meta = new KpiValoresPlanilha(planilhaDeValores.get(i).getDouble("META"));
            historicoPlanilha.put(planilhaDeValores.get(i).getString("ID"), meta);
        }
    }

    /**
     * Identifica se est� sendo utilizado meta em lote.
     *
     * @return
     */
    private boolean isMetaEmLote() {
        boolean retorno = false;

        if (fldDataDe.getText().isEmpty()) {
        } else if (fldDataAte.getText().isEmpty()) {
        } else if (txtMeta.getText().isEmpty()) {
        } else if (fldDataDe.getCalendar().compareTo(fldDataAte.getCalendar()) > 0) {
        } else {
            retorno = true;
        }
        return retorno;
    }

    /**
     * Indica se est� sendo realizada uma duplica��o de indicadores.
     *
     * @return
     */
    public boolean isDuplicacao() {
        return isDuplicacao;
    }

    /**
     * Identifica a duplica��o de indicadores.
     *
     * @param isDuplicacao
     */
    public void setIsDuplicacao(boolean isDuplicacao) {
        this.isDuplicacao = isDuplicacao;
    }

    /*
     * A��o executada ap�s a inser��o de um registro. 
     */
    public void afterInsert() {
        KpiDataController dataController = kpi.core.KpiStaticReferences.getKpiDataController();

        if (dataController.getStatus() == KpiDataController.KPI_ST_OK) {
            PlanilhaDeValoresBackUp.removeAll();
            PlanilhaDeValoresBackUp = record.getBIXMLVector("PLANILHAS").clone2();
            vlrAlvoVerde = pvValorVerde.getDouble();
            setHistoricoPlanilhaDeValores();
            updYear();
            tabIndicador.resetExcluidos();
        }
    }

    /**
     * A��o executada ap�s a atuliza��o de um registro.
     */
    public void afterUpdate() {
        KpiDataController dataController = kpi.core.KpiStaticReferences.getKpiDataController();

        if (dataController.getStatus() == KpiDataController.KPI_ST_OK) {
            PlanilhaDeValoresBackUp.removeAll();
            PlanilhaDeValoresBackUp = record.getBIXMLVector("PLANILHAS").clone2();
            vlrAlvoVerde = pvValorVerde.getDouble();
            setHistoricoPlanilhaDeValores();
            updYear();
            tabIndicador.resetExcluidos();
        }
    }

    /**
     * Render da tabela de indicador.
     *
     */
    class KpiIndicadorTableRenderer extends javax.swing.table.DefaultTableCellRenderer {

        private kpi.beans.JBIXMLTable biTable = null;
        private int qtdDecimais = 2;
        private Color corOk = Color.black;

        public KpiIndicadorTableRenderer() {
        }

        public KpiIndicadorTableRenderer(kpi.beans.JBIXMLTable table) {
            biTable = table;
        }

        // Sobreposicao para mudar o comportamento de pintura da celula.
        @Override
        public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            pv.jfcx.JPVEdit oRender = new pv.jfcx.JPVEdit();
            BIXMLRecord recLinha = null;
            int linha = biTable.getOriginalRow(row);
            boolean isAdmin = KpiIndicador.isAdmin;
            int nEnablePlan = KpiIndicador.nEnablePlan;

            oRender.setLocale(kpi.core.KpiStaticReferences.getKpiDefaultLocale());
            oRender.setLAF(false);
            oRender.setBorderStyle(0);
            oRender.setFont(new java.awt.Font("Tahoma", 0, 11));
            oRender.setToolTipText("");

            if (biTable != null) {
                column = biTable.getColumn(column).getModelIndex();
                recLinha = biTable.getXMLData().get(linha);
                String colName = biTable.getColumnTag(column);

                if (!colName.equals("LOG")) {
                    renderNumber(oRender, new Double(value.toString()), column, row, recLinha, colName);
                } else {
                    oRender.setAlignment(1);
                    oRender.setValue(value.toString());
                }

                if (hasFocus) {
                    oRender.setBackground(new java.awt.Color(212, 208, 200));
                }

                //Se for o Administrador permite tudo
                if (isAdmin == false) {
                    //Se estiver configurado para bloquear entrada de dados quando o indicador for formula
                    if (nEnablePlan == KpiIndicador.KPI_PLANIND_OK) {
                        if (recLinha.getString("ID").equals("N_E_W_")) {
                            //Caso seja inclus�o
                            oRender.setForeground(corOk);
                        } else if (colName.equals("VALOR")) {
                            if (recLinha.getInt("EDIT_LINE_REAL") == KpiIndicador.KPI_PLANIND_OK && recLinha.getBoolean("EDIT_LINE")) {
                                oRender.setForeground(corOk);
                            } else {
                                oRender.setForeground(java.awt.Color.red);
                                oRender.setToolTipText(getToolTipPlan(recLinha.getInt("EDIT_LINE_REAL")));
                            }
                        } else if (colName.equals("META")) {
                            if (recLinha.getInt("EDIT_LINE_META") == KpiIndicador.KPI_PLANIND_OK && recLinha.getBoolean("EDIT_LINE")) {
                                oRender.setForeground(corOk);
                            } else {
                                oRender.setForeground(java.awt.Color.red);
                                oRender.setToolTipText(getToolTipPlan(recLinha.getInt("EDIT_LINE_META")));
                            }
                        } else if (colName.equals("PREVIA")) {
                            if (recLinha.getInt("EDIT_LINE_PREVIA") == KpiIndicador.KPI_PLANIND_OK && recLinha.getBoolean("EDIT_LINE")) {
                                oRender.setForeground(corOk);
                            } else {
                                oRender.setForeground(java.awt.Color.red);
                                oRender.setToolTipText(getToolTipPlan(recLinha.getInt("EDIT_LINE_PREVIA")));
                            }
                        } else {
                            oRender.setForeground(java.awt.Color.red);
                        }
                    } else {
                        oRender.setForeground(java.awt.Color.red);
                        oRender.setToolTipText(getToolTipPlan(nEnablePlan));
                    }
                } else {
                    oRender.setForeground(corOk);
                }

                //Exibe o log sempre desabilitado. 
                if (colName.equals("LOG")) {
                    oRender.setForeground(java.awt.Color.GRAY);
                }
            }
            return oRender;
        }

        private String getToolTipPlan(int tipo) {
            String mensagem = "";
            switch (tipo) {
                case KpiIndicador.KPI_PLANIND_NOUPDATE://"Usu�rio sem permiss�o para efetuar manuten��o";
                    mensagem = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00061");
                    break;
                case KpiIndicador.KPI_PLANIND_FORMULE://"Manuten��o bloqueada para indicadores com f�rmulas";
                    mensagem = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00062");
                    break;
                case KpiIndicador.KPI_PLANIND_OWNER://"Somente respons�veis podem alterar a planilha";
                    mensagem = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00063");
                    break;
                case KpiIndicador.KPI_PLANIND_PERIOD://"Manuten��o bloqueada para esse per�odo";
                    mensagem = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00064");
                    break;
                case KpiIndicador.KPI_PLANIND_PERIODUSER://"Manuten��o bloqueada para esse per�odo po usu�rio";
                    mensagem = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00064");
                    break;
                case KpiIndicador.KPI_PLANIND_PERIODSCO://"Manuten��o bloqueada para esse per�odo por departamento";
                    mensagem = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00064");
                    break;
                case KpiIndicador.KPI_PLANIND_DIALIMITE://"Manuten��o bloqueada para esse per�odo por dia limite";
                    mensagem = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiRestricaoPlanValor_00002");
                    break;
            }

            return mensagem;
        }

        private pv.jfcx.JPVEdit renderNumber(pv.jfcx.JPVEdit oRender, Double dbValor, int column, int row, BIXMLRecord recLinha, String colName) {
            java.text.NumberFormat oValor = java.text.NumberFormat.getInstance();

            if (colName.equals("VALOR") || colName.equals("META") || colName.equals("PREVIA")) {
                oValor.setMinimumFractionDigits(qtdDecimais);
                oValor.setMaximumFractionDigits(qtdDecimais);
            } else {
                oValor.setMinimumFractionDigits(0);
                oValor.setMaximumFractionDigits(0);
            }

            oRender.setAlignment(2);
            oRender.setValue(oValor.format(dbValor));

            return oRender;
        }

        public void setQtdDecimais(int qtdDecimais) {
            this.qtdDecimais = qtdDecimais;
        }
    }

    /**
     * Classe para valida��o dos valores digitados em uma tabela.
     *
     */
    class KpiIndicadorCellEditor extends pv.jfcx.PVTableEditor {

        pv.jfcx.JPVEdit myEdit;
        private int frequencia = 0;
        private kpi.beans.JBIXMLTable biTable = null;
        private kpi.swing.KpiDefaultDialogSystem dialog = kpi.core.KpiStaticReferences.getKpiDialogSystem();
        private int linhaEditada = 0;
        private BIXMLRecord conteudoLinhaEditada = null;

        public KpiIndicadorCellEditor(Component c, int i) {
            super(c, i);
            myEdit = (pv.jfcx.JPVEdit) c;
        }

        public KpiIndicadorCellEditor(kpi.beans.JBIXMLTable table, Component c, int i) {
            super(c, i);
            myEdit = (pv.jfcx.JPVEdit) c;
            setBiTable(table);
        }

        /*
         * Verifica se a celula pode ser editada.
         */
        @Override
        public boolean isCellEditable(java.util.EventObject e) {
            int linha = biTable.getSelectedRow();
            int column = biTable.getSelectedCol();
            boolean isAdmin = kpi.swing.framework.KpiIndicador.isAdmin;
            int nEnablePlan = kpi.swing.framework.KpiIndicador.nEnablePlan;

            if (linha != -1) {
                int col = biTable.getColumn(column).getModelIndex();
                String colName = biTable.getColumnTag(col);

                if (!isAdmin && nEnablePlan == 0) {

                    BIXMLRecord recLinha = biTable.getXMLData().get(linha);
                    if (recLinha.getString("ID").equals("N_E_W_")) {
                        return super.isCellEditable(e);
                    } else if (colName.equals("VALOR")) {
                        if (recLinha.getInt("EDIT_LINE_REAL") == KpiIndicador.KPI_PLANIND_OK && recLinha.getBoolean("EDIT_LINE")) {
                            return super.isCellEditable(e);
                        }
                    } else if (colName.equals("META")) {
                        if (recLinha.getInt("EDIT_LINE_META") == KpiIndicador.KPI_PLANIND_OK && recLinha.getBoolean("EDIT_LINE")) {
                            return super.isCellEditable(e);
                        }
                    } else if (colName.equals("PREVIA")) {
                        if (recLinha.getInt("EDIT_LINE_PREVIA") == KpiIndicador.KPI_PLANIND_OK && recLinha.getBoolean("EDIT_LINE")) {
                            return super.isCellEditable(e);
                        }
                    }
                } else {
                    //Impede que a coluna de data de atualiza��o possa ser alterada. 
                    if (!colName.equals("LOG")) {
                        return super.isCellEditable(e);
                    }
                }
            }

            return false;
        }

        @Override
        public boolean stopCellEditing() {
            if (validCell()) {
                if (!conteudoLinhaEditada.toString().equals(biTable.getXMLData(false).get(linhaEditada).toString())) {
                    //Marca a linha como atualizada. 
                    biTable.getXMLData(false).get(linhaEditada).set("UPDATED", "T");
                    //Mant�m a edi��o realiada na c�lula. 
                }
                return true;
            } else {
                return false;
            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int key = e.getModifiers();
            linhaEditada = biTable.getSelectedRow();
            conteudoLinhaEditada = biTable.getXMLData(false).get(linhaEditada).clone2();

            if (key == pv.jfcx.JPVEdit.ENTER || key == pv.jfcx.JPVEdit.TAB) {
                if (getFrequencia() == kpi.core.KpiStaticReferences.KPI_FREQ_DIARIA) {
                    if (validCell()) {
                        super.actionPerformed(e);
                    } else {
                        dialog.errorMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00053"));
                        ((pv.jfcx.JPVEdit) e.getSource()).setValue(((pv.jfcx.JPVNumeric) e.getSource()).getMinValue());
                        m_table.requestFocus();
                    }
                } else {
                    super.actionPerformed(e);
                }
            } else {
                super.actionPerformed(e);
            }
        }

        private boolean validCell() {
            int linha = biTable.getSelectedRow();
            boolean isValid = true;
            if (linha >= 0) {
                BIXMLRecord recLinha = biTable.getXMLData(false).get(linha);
                String colName = biTable.getColumnTag(biTable.getSelectedCol());
                int dia = 0;
                int mes = 0;
                int ano = 0;

                if (colName.equals("DIA")) {
                    dia = ((Double) myEdit.getValue()).intValue();
                    mes = recLinha.getInt("MES");
                    ano = recLinha.getInt("ANO");
                } else if (colName.equals("MES")) {
                    dia = recLinha.getInt("DIA");
                    mes = ((Double) myEdit.getValue()).intValue();
                    ano = recLinha.getInt("ANO");
                } else if (colName.equals("ANO")) {
                    dia = recLinha.getInt("DIA");
                    mes = recLinha.getInt("MES");
                    ano = ((Double) myEdit.getValue()).intValue();
                }

                if (checkDays(dia, mes, ano)) {
                    isValid = true;
                } else {
                    isValid = false;
                }
            }

            return isValid;
        }

        private boolean checkDays(int dayofmonth, int month, int year) {
            if (month != 0) {
                java.util.GregorianCalendar gc = new java.util.GregorianCalendar();
                if (year != 0) {
                    gc.set(java.util.Calendar.YEAR, year);
                }
                gc.set(java.util.Calendar.MONTH, month - 1);

                return (dayofmonth <= gc.getActualMaximum(java.util.Calendar.DAY_OF_MONTH));
            } else {
                return true;
            }
        }

        private void setBiTable(kpi.beans.JBIXMLTable biTable) {
            this.biTable = biTable;
        }

        public int getFrequencia() {
            return frequencia;
        }

        public void setFrequencia(int frequencia) {
            this.frequencia = frequencia;
        }
    }

    /**
     * KpiIndSourceQueryTable Created on 03 de Julho de 2007, 16:49
     *
     * @author Alexandre Alves da Silva
     *
     */
    class KpiIndSourceQueryTable {

        private kpi.xml.BIXMLAttributes attIndicador = new kpi.xml.BIXMLAttributes();
        private kpi.xml.BIXMLVector vctDados = null;
        private kpi.xml.BIXMLRecord recDelDados = null;

        /**
         * Creates a new instance of KpiTabIndicador
         */
        public KpiIndSourceQueryTable() {
            setVctDados(new kpi.xml.BIXMLVector("DWCONSULTAS", "DWCONSULTA"));
        }

        public kpi.xml.BIXMLVector getVctDados() {
            return vctDados;
        }

        private void setVctDados(kpi.xml.BIXMLVector vctDados) {
            this.vctDados = vctDados;
        }

        public kpi.xml.BIXMLRecord createNewRecord(String url, String dw, String consulta, String idConsulta) {
            kpi.xml.BIXMLRecord newRecord = new kpi.xml.BIXMLRecord("DWCONSULTA");
            newRecord.set("ID", "-99");
            newRecord.set("DW_URL", url);
            newRecord.set("DW_NAME", dw);
            newRecord.set("DW_IDCON", idConsulta);
            newRecord.set("DW_CON", consulta);
            return newRecord;
        }

        public void addRecordDeleted(kpi.xml.BIXMLRecord record) {
            if (!record.getString("ID").equals("-99")) {
                if (recDelDados == null) {
                    recDelDados = new kpi.xml.BIXMLRecord("REG_EXCLUIDO");
                    recDelDados.set(new kpi.xml.BIXMLVector("EXCLUIDOS", "EXCLUIDO"));
                }
                kpi.xml.BIXMLVector vctAddReg = recDelDados.getBIXMLVector("EXCLUIDOS");
                vctAddReg.add(record);
            }
        }

        public kpi.xml.BIXMLRecord getDelDados() {
            return recDelDados;
        }

        public void resetDados() {
            setVctDados(new kpi.xml.BIXMLVector("DWCONSULTAS", "DWCONSULTA"));
        }

        public void resetExcluidos() {
            recDelDados = null;
        }

        public void setCabec() {
            resetDados();

            attIndicador = new kpi.xml.BIXMLAttributes();
            attIndicador.set("TIPO", "CONSULTA");
            attIndicador.set("RETORNA", "F");

            //Coluna Ano.
            attIndicador.set("TAG000", "DW_URL");
            attIndicador.set("CAB000", "URL");
            attIndicador.set("CLA000", kpi.beans.JBIXMLTable.KPI_STRING);
            attIndicador.set("EDT000", "F");
            attIndicador.set("CUM000", "F");
            //Coluna Valor.
            attIndicador.set("TAG001", "DW_NAME");
            attIndicador.set("CAB001", "DataWareHouse");
            attIndicador.set("CLA001", kpi.beans.JBIXMLTable.KPI_STRING);
            attIndicador.set("EDT001", "F");
            attIndicador.set("CUM001", "F");

            //Coluna Meta.
            attIndicador.set("TAG002", "DW_CON");
            attIndicador.set("CAB002", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00079"));
            attIndicador.set("CLA002", kpi.beans.JBIXMLTable.KPI_STRING);
            attIndicador.set("EDT002", "F");
            attIndicador.set("CUM002", "F");

            vctDados.setAttributes(attIndicador);
        }
    }

    /**
     * KpiValoresPlanilha Created on 10 de Janeiro de 2012, 09:39
     *
     * @author Tiago Tudisco
     *
     */
    class KpiValoresPlanilha {

        private double meta;

        public KpiValoresPlanilha() {
            meta = 0.0;
        }

        public KpiValoresPlanilha(double valor) {
            meta = valor;
        }

        /**
         * @return the meta
         */
        public double getMeta() {
            return meta;
        }

        /**
         * @return the meta (String)
         */
        public String getStringMeta() {
            return String.valueOf(meta);
        }

        /**
         * @param meta the meta to set
         */
        public void setMeta(double meta) {
            this.meta = meta;
        }
    }
}