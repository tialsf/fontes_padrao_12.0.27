package kpi.swing.framework;

/**
 *
 * @author Alexandre Alves da Silva
 */
//Classe com os itens da formula
public class KpiItemFormula extends Object {

    public static final int EXPRESSAO = 1;
    public static final int INDICADOR = 2;
    public static final int METAFORMULA = 3;
    private String strExpressao = "";
    private String strDescricao = "";
    private String idScoreCard = "";
    private int iTipo = 1;

    public KpiItemFormula() {
    }

    public KpiItemFormula(String descricao) {
        setStrDescricao(descricao);
    }

    public KpiItemFormula(String expressao, String descricao) {
        setStrExpressao(expressao);
        setStrDescricao(descricao);
    }

    public String getStrExpressao() {
        return strExpressao;
    }

    public String getStrDescricao() {
        return strDescricao;
    }

    public String getIdScoreCard() {
        return this.idScoreCard;
    }

    public void setStrExpressao(String expressao) {
        this.strExpressao = expressao;
    }

    public void setStrDescricao(String descricao) {
        this.strDescricao = descricao;
    }

    public int getITipo() {
        return iTipo;
    }

    public void setITipo(int tipo) {
        this.iTipo = tipo;
    }

    public void setIdScoreCard(String id) {
        this.idScoreCard = id;
    }
}
