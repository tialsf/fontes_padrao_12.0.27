/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * KpiJustificativaMeta.java
 *
 * Created on 03/01/2012, 09:54:03
 */
package kpi.swing.framework;

import javax.swing.JOptionPane;
import kpi.core.KpiStaticReferences;
import kpi.xml.BIXMLRecord;

/**
 *
 * @author tiago.tudisco
 */
public class KpiJustificativaMeta extends javax.swing.JDialog {

    public static final int ALTERACAO_ALVO = 1;
    public static final int ALTERACAO_PLANILHA = 2;
    public static final int ALTERACAO_LOTE = 3;
    public static final int EXCLUSAO_PLANILHA = 4;
    public static final int INCLUSAO_PLANILHA = 5;
    public static final String INCLUI = "INSERTED"; //NOI18N
    public static final String ALTERA = "CHANGED"; //NOI18N
    public static final String EXCLUI = "DELETED"; //NOI18N
    KpiIndicador frame = null;
    BIXMLRecord xmlJustificativa = null;

    /** Creates new form KpiJustificativaMeta */
    public KpiJustificativaMeta(java.awt.Container parent, String title, boolean modal, BIXMLRecord infJust, KpiIndicador oFrame) {
        super();

        addWindowListener(new java.awt.event.WindowAdapter() {

            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("international").getString("KpiJustificativaMeta_00010"), java.util.ResourceBundle.getBundle("international").getString("KpiJustificativaMeta_00009"), JOptionPane.INFORMATION_MESSAGE);
            }
        });

        initComponents();
        frame = oFrame;
        configuraTela();

        this.setTitle(title);
        this.setSize(525, 385);
        this.setLocationRelativeTo(parent);

        configuraJustificativa(infJust);

    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        pnlDescricao = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescricao = new kpi.beans.JBITextArea();
        pnlHistorico = new javax.swing.JPanel();
        lblIndicador = new javax.swing.JLabel();
        lblPeriodoAte = new javax.swing.JLabel();
        lblPeriodoDe = new javax.swing.JLabel();
        lblMetaAnt = new javax.swing.JLabel();
        lblMetaInf = new javax.swing.JLabel();
        fldIndicador = new pv.jfcx.JPVEdit();
        fldMetaAnt = new pv.jfcx.JPVCurrency();
        fldMetaInf = new pv.jfcx.JPVCurrency();
        fldPeriodoAte = new pv.jfcx.JPVEdit();
        fldPeriodoDe = new pv.jfcx.JPVEdit();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        lblTipoJusti = new javax.swing.JLabel();
        pnlRightForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setModal(true);
        setResizable(false);

        scpRecord.setBorder(null);
        scpRecord.setMaximumSize(new java.awt.Dimension(374, 276));
        scpRecord.setMinimumSize(new java.awt.Dimension(374, 276));
        scpRecord.setPreferredSize(new java.awt.Dimension(374, 276));

        pnlData.setPreferredSize(new java.awt.Dimension(355, 150));

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international"); // NOI18N
        pnlDescricao.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)), bundle.getString("KpiJustificativaMeta_00008"))); // NOI18N
        pnlDescricao.setLayout(null);

        txtDescricao.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtDescricao.setTabSize(1);
        jScrollPane1.setViewportView(txtDescricao);

        pnlDescricao.add(jScrollPane1);
        jScrollPane1.setBounds(10, 20, 470, 120);

        pnlHistorico.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)), bundle.getString("KpiJustificativaMeta_00002"))); // NOI18N

        lblIndicador.setForeground(new java.awt.Color(51, 51, 255));
        lblIndicador.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        lblIndicador.setText(bundle1.getString("KpiJustificativaMeta_00003")); // NOI18N
        lblIndicador.setEnabled(false);

        lblPeriodoAte.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPeriodoAte.setText(bundle1.getString("KpiJustificativaMeta_00005")); // NOI18N
        lblPeriodoAte.setEnabled(false);
        lblPeriodoAte.setMaximumSize(new java.awt.Dimension(100, 16));
        lblPeriodoAte.setMinimumSize(new java.awt.Dimension(100, 16));
        lblPeriodoAte.setPreferredSize(new java.awt.Dimension(100, 16));

        lblPeriodoDe.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPeriodoDe.setText(bundle1.getString("KpiJustificativaMeta_00004")); // NOI18N
        lblPeriodoDe.setEnabled(false);
        lblPeriodoDe.setMaximumSize(new java.awt.Dimension(100, 16));
        lblPeriodoDe.setMinimumSize(new java.awt.Dimension(100, 16));
        lblPeriodoDe.setPreferredSize(new java.awt.Dimension(100, 16));

        lblMetaAnt.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblMetaAnt.setText(bundle1.getString("KpiJustificativaMeta_00006")); // NOI18N
        lblMetaAnt.setEnabled(false);
        lblMetaAnt.setMaximumSize(new java.awt.Dimension(100, 16));
        lblMetaAnt.setMinimumSize(new java.awt.Dimension(100, 16));
        lblMetaAnt.setPreferredSize(new java.awt.Dimension(100, 16));

        lblMetaInf.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblMetaInf.setText(bundle1.getString("KpiJustificativaMeta_00007")); // NOI18N
        lblMetaInf.setEnabled(false);
        lblMetaInf.setMaximumSize(new java.awt.Dimension(100, 16));
        lblMetaInf.setMinimumSize(new java.awt.Dimension(100, 16));
        lblMetaInf.setPreferredSize(new java.awt.Dimension(100, 16));

        fldIndicador.setAllowChangeMode(false);
        fldIndicador.setEditable(false);
        fldIndicador.setEnabled(false);
        fldIndicador.setInsertEditMode(false);
        fldIndicador.setMaxLength(100);

        fldMetaAnt.setAllowNull(false);
        fldMetaAnt.setEditable(false);
        fldMetaAnt.setEnabled(false);
        fldMetaAnt.setMaxValue(1.0E15);
        fldMetaAnt.setMinValue(0.0);
        fldMetaAnt.setUseLocale(true);

        fldMetaInf.setAllowNull(false);
        fldMetaInf.setEditable(false);
        fldMetaInf.setEnabled(false);
        fldMetaInf.setMaxValue(1.0E15);
        fldMetaInf.setMinValue(0.0);
        fldMetaInf.setUseLocale(true);

        fldPeriodoAte.setAllowChangeMode(false);
        fldPeriodoAte.setEditable(false);
        fldPeriodoAte.setEnabled(false);
        fldPeriodoAte.setInsertEditMode(false);
        fldPeriodoAte.setMaxLength(100);

        fldPeriodoDe.setAllowChangeMode(false);
        fldPeriodoDe.setEditable(false);
        fldPeriodoDe.setEnabled(false);
        fldPeriodoDe.setInsertEditMode(false);
        fldPeriodoDe.setMaxLength(100);

        javax.swing.GroupLayout pnlHistoricoLayout = new javax.swing.GroupLayout(pnlHistorico);
        pnlHistorico.setLayout(pnlHistoricoLayout);
        pnlHistoricoLayout.setHorizontalGroup(
            pnlHistoricoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlHistoricoLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(pnlHistoricoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblMetaAnt, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPeriodoDe, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblIndicador, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlHistoricoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlHistoricoLayout.createSequentialGroup()
                        .addGroup(pnlHistoricoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(fldPeriodoDe, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(fldMetaAnt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(pnlHistoricoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblMetaInf, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblPeriodoAte, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlHistoricoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(fldMetaInf, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(fldPeriodoAte, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(fldIndicador, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlHistoricoLayout.setVerticalGroup(
            pnlHistoricoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlHistoricoLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(pnlHistoricoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblIndicador, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fldIndicador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(pnlHistoricoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblMetaAnt, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblMetaInf, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fldMetaInf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fldMetaAnt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(pnlHistoricoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlHistoricoLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlHistoricoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(fldPeriodoAte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblPeriodoAte, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlHistoricoLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(pnlHistoricoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(fldPeriodoDe, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblPeriodoDe, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        fldMetaAnt.getAccessibleContext().setAccessibleName("");
        fldMetaInf.getAccessibleContext().setAccessibleName("");

        javax.swing.GroupLayout pnlDataLayout = new javax.swing.GroupLayout(pnlData);
        pnlData.setLayout(pnlDataLayout);
        pnlDataLayout.setHorizontalGroup(
            pnlDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDataLayout.createSequentialGroup()
                .addGroup(pnlDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlDescricao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlHistorico, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlDataLayout.setVerticalGroup(
            pnlDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDataLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(pnlHistorico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pnlDescricao, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE)
                .addContainerGap())
        );

        scpRecord.setViewportView(pnlData);

        getContentPane().add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 29));

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(250, 27));

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(80, 32));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.setHideActionText(true);
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.setFocusable(false);
        btnCancel.setHideActionText(true);
        btnCancel.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnCancel.setMaximumSize(new java.awt.Dimension(80, 23));
        btnCancel.setMinimumSize(new java.awt.Dimension(80, 23));
        btnCancel.setPreferredSize(new java.awt.Dimension(80, 23));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        javax.swing.GroupLayout pnlConfirmationLayout = new javax.swing.GroupLayout(pnlConfirmation);
        pnlConfirmation.setLayout(pnlConfirmationLayout);
        pnlConfirmationLayout.setHorizontalGroup(
            pnlConfirmationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlConfirmationLayout.createSequentialGroup()
                .addGap(99, 99, 99)
                .addComponent(tbConfirmation, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlConfirmationLayout.setVerticalGroup(
            pnlConfirmationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlConfirmationLayout.createSequentialGroup()
                .addComponent(tbConfirmation, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lblTipoJusti.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblTipoJusti.setText(bundle.getString("KpiJustificativaMeta_00015")); // NOI18N

        javax.swing.GroupLayout pnlToolsLayout = new javax.swing.GroupLayout(pnlTools);
        pnlTools.setLayout(pnlToolsLayout);
        pnlToolsLayout.setHorizontalGroup(
            pnlToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlToolsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTipoJusti, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlConfirmation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        pnlToolsLayout.setVerticalGroup(
            pnlToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(pnlConfirmation, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(lblTipoJusti)
        );

        getContentPane().add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed

        if (validaCampos()) {

            xmlJustificativa = setRecordJustificativa();
            frame.updateXMLJustificativa(xmlJustificativa);
            dispose();
        } else {
            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("international").getString("KpiJustificativaMeta_00012"), java.util.ResourceBundle.getBundle("international").getString("KpiJustificativaMeta_00011"), JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        frame.cancelUupdateXMLJustificativa();
        dispose();
    }//GEN-LAST:event_btnCancelActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnSave;
    private pv.jfcx.JPVEdit fldIndicador;
    private pv.jfcx.JPVCurrency fldMetaAnt;
    private pv.jfcx.JPVCurrency fldMetaInf;
    private pv.jfcx.JPVEdit fldPeriodoAte;
    private pv.jfcx.JPVEdit fldPeriodoDe;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblIndicador;
    private javax.swing.JLabel lblMetaAnt;
    private javax.swing.JLabel lblMetaInf;
    private javax.swing.JLabel lblPeriodoAte;
    private javax.swing.JLabel lblPeriodoDe;
    private javax.swing.JLabel lblTipoJusti;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlDescricao;
    private javax.swing.JPanel pnlHistorico;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JToolBar tbConfirmation;
    private kpi.beans.JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables

    private boolean validaCampos() {
        boolean retorno = true;

        if (txtDescricao.getText().trim().equals("")) { //NOI18N

            retorno = false;
        }

        return retorno;
    }

    private BIXMLRecord setRecordJustificativa() {
        BIXMLRecord rec = new BIXMLRecord("JUSTIFICATIVA"); //NOI18N

        rec.set("INDIC_ID", frame.getID()); //NOI18N
        rec.set("METAANT", fldMetaAnt.getDouble()); //NOI18N
        rec.set("METAINF", fldMetaInf.getDouble()); //NOI18N
        rec.set("JUSTIFIC", txtDescricao.getText()); //NOI18N

        if (frame.getTipoJustificativa() == ALTERACAO_ALVO) {
            rec.set("TIPOJUST", ALTERACAO_ALVO); //NOI18N
        } else {
            rec.set("TIPOJUST", ALTERACAO_PLANILHA); //NOI18N
        }

        if (frame.getTipoJustificativa() == INCLUSAO_PLANILHA) {
            rec.set("TIPOALT", INCLUI); //NOI18N
        } else if (frame.getTipoJustificativa() == EXCLUSAO_PLANILHA) {
            rec.set("TIPOALT", EXCLUI); //NOI18N
        } else {
            rec.set("TIPOALT", ALTERA); //NOI18N
        }

        rec.set("DATADE", fldPeriodoDe.getText()); //NOI18N
        rec.set("DATAATE", fldPeriodoAte.getText()); //NOI18N

        return rec;
    }

    private void configuraJustificativa(BIXMLRecord infJust) {

        fldIndicador.setText(infJust.getString("INDICADOR"));
        fldMetaAnt.setDecimals(infJust.getInt("DECIMAIS")); //NOI18N
        fldMetaInf.setDecimals(infJust.getInt("DECIMAIS")); //NOI18N
        fldPeriodoDe.setText(infJust.getString("PERIODODE"));
        fldMetaAnt.setValue(infJust.getDouble("METAANT")); //NOI18N
        fldMetaInf.setValue(infJust.getDouble("METAINF")); //NOI18N
        fldPeriodoAte.setText(infJust.getString("PERIODOATE"));

        txtDescricao.requestFocus();
    }

    private void configuraTela() {
        String decimalSeparator = KpiStaticReferences.getDecimalSeparator();
        String thousandSeparator = KpiStaticReferences.getThousandSeparator();

        fldMetaAnt.setDecimalSeparator(decimalSeparator);
        fldMetaInf.setThousandSeparator(thousandSeparator);

        fldMetaAnt.setSymbol(""); //NOI18N
        fldMetaInf.setSymbol(""); //NOI18N

        if (frame.getTipoJustificativa() == ALTERACAO_LOTE) {

            lblTipoJusti.setText(java.util.ResourceBundle.getBundle("international").getString("KpiJustificativaMeta_00013"));

        } else {

            //esconde informacoes de periodo
            fldPeriodoAte.setVisible(false);
            lblPeriodoAte.setVisible(false);

            lblPeriodoDe.setText(java.util.ResourceBundle.getBundle("international").getString("KpiJustificativaMeta_00018"));

            if (frame.getTipoJustificativa() == ALTERACAO_ALVO) {

                fldPeriodoDe.setVisible(false);
                lblPeriodoDe.setVisible(false);
                lblTipoJusti.setText(java.util.ResourceBundle.getBundle("international").getString("KpiJustificativaMeta_00014"));

            } else if (frame.getTipoJustificativa() == ALTERACAO_PLANILHA) {

                lblTipoJusti.setText(java.util.ResourceBundle.getBundle("international").getString("KpiJustificativaMeta_00015"));

            } else if (frame.getTipoJustificativa() == EXCLUSAO_PLANILHA) {

                lblTipoJusti.setText(java.util.ResourceBundle.getBundle("international").getString("KpiJustificativaMeta_00016"));

            } else if (frame.getTipoJustificativa() == INCLUSAO_PLANILHA) {

                lblTipoJusti.setText(java.util.ResourceBundle.getBundle("international").getString("KpiJustificativaMeta_00017"));

            }
        }

    }
}