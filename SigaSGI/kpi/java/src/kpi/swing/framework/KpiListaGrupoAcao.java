package kpi.swing.framework;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;
import javax.swing.JTabbedPane;
import kpi.beans.JBISeekListPanel;
import kpi.beans.JBIXMLTable;
import kpi.core.KpiStaticReferences;
import kpi.xml.BIXMLRecord;

public class KpiListaGrupoAcao extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "GRUPO_ACAO";
    private String formType = "LSTGRUPO_ACAO";
    private String parentType = recordType;
    private KpiListaGrupoAcaoTableCellRenderer cellRenderer;
    private kpi.beans.JBIXMLTable listaTable;
    private String cmdSQL = new String();
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;
    private String filterSqlStatus = new String();

    public KpiListaGrupoAcao(int operation, String idAux, String cmdSQL) {
        initComponents();
        
        putClientProperty("MAXI", true);

        if (!cmdSQL.trim().equals("") && !cmdSQL.trim().equals("0")) {
            setCmdSQL(cmdSQL);
        } else {
            setCmdSQL("");
        }
        event.defaultConstructor(operation, idAux, cmdSQL);
    }

    @Override
    public void enableFields() {
    }

    @Override
    public void disableFields() {
    }

    @Override
    public void refreshFields() {
        String filter = "";
        
        if (!cmdSQL.trim().equals("") && !cmdSQL.trim().equals("0")) {
           filter = getCmdSQL();
        } else if (!filterSqlStatus.trim().equals("") && !filterSqlStatus.trim().equals("0")) {
           filter = filterSqlStatus;
        }
        
        loadRecord(filter);
    }

    private void geraTotalizador() {
        kpi.xml.BIXMLVector vecLista = jBIListGrupo.xmlTable.getXMLData();
        int qtdLinhas = vecLista.size(),
                nTotVencidas = 0,
                nTotAcoes = 0;

        double nPercFinVenc = 0.0,
                nPercAtras = 0.0,
                nFinalizadas = 0.0;

        //Caso tenha mais de um registro insere totalizador
        if (qtdLinhas > 1) {
            for (int lin = 0; lin < qtdLinhas; lin++) {
                nTotVencidas += vecLista.get(lin).getInt("VENCIDAS");
                nTotAcoes += vecLista.get(lin).getInt("TOTALACOES");
                nPercFinVenc += vecLista.get(lin).getDouble("PORCFINVENC");
                nPercAtras += vecLista.get(lin).getDouble("PORCATRASVENC");
                nFinalizadas += vecLista.get(lin).getDouble("FINALIZADAS");
            }

            if (nPercFinVenc > 0) {
                nPercFinVenc = (nPercFinVenc / qtdLinhas);
            }

            if (nPercAtras > 0) {
                nPercAtras = (nPercAtras / qtdLinhas);
            }

            if (nFinalizadas > 0) {
                nFinalizadas = (nFinalizadas / qtdLinhas);
            }

            kpi.xml.BIXMLRecord newRecord = new kpi.xml.BIXMLRecord("GRUPO_ACAO");
            newRecord.set("ID", "-1");
            newRecord.set("ID_SCORE", "-1");
            //"Total de a��es"
            newRecord.set("NOME", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiListaGrupoAcao_00002"));
            newRecord.set("STATUS", " " );
            newRecord.set("OBSERVACAO"," " );
            newRecord.set("VENCIDAS", nTotVencidas);
            newRecord.set("PORCFINVENC", nPercFinVenc);
            newRecord.set("PORCATRASVENC", nPercAtras);
            newRecord.set("TOTALACOES", nTotAcoes);
            newRecord.set("FINALIZADAS", nFinalizadas);

            jBIListGrupo.xmlTable.addNewRecord(newRecord);
            jBIListGrupo.xmlTable.setEnableSort(false);
        }
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }
    public String getCmdSQL() {
        return cmdSQL.trim();
    }

    private void setCmdSQL(String cmdSQL) {
        this.cmdSQL = cmdSQL;
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        tapCadastro = new JTabbedPane();
        jBIListGrupo = new JBISeekListPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiListaGrupoAcao_00001")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_grupoacao.gif"))); // NOI18N
        setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(600, 358));

        tapCadastro.setEnabled(false);
        tapCadastro.setFocusable(false);
        tapCadastro.setPreferredSize(new Dimension(0, 0));
        getContentPane().add(tapCadastro, BorderLayout.WEST);
        getContentPane().add(jBIListGrupo, BorderLayout.CENTER);

        pack();
    }//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JBISeekListPanel jBIListGrupo;
    private JTabbedPane tapCadastro;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public void loadRecord() {
        this.setID("0");
        
        setFilterSqlStatus("STATUS = 2");
        
        String filter = getFilterSqlStatus();
        
        if (!cmdSQL.trim().equals("") && !cmdSQL.trim().equals("0")) {
           filter = getCmdSQL();
        }
        prepareFiltro();
        loadRecord(filter);
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    public kpi.beans.JBIXMLTable getListaTable() {
        return listaTable;
    }

    public void setListaTable(kpi.beans.JBIXMLTable listaTable) {
        this.listaTable = listaTable;
    }

    /*
     *Seta o Render para a tabela.
     */
    private void setRenderTable() {
        jBIListGrupo.xmlTable.setHeaderHeight(35);
        jBIListGrupo.xmlTable.getColumn(0).setPreferredWidth(400);
        setListaTable(jBIListGrupo.xmlTable);

        cellRenderer = new KpiListaGrupoAcaoTableCellRenderer(getListaTable());

        for (int col = 0; col < 8; col++) {
            getListaTable().getColumn(col).setCellRenderer(cellRenderer);
        }
    }
    
    public String getFilterSqlStatus() {
        return filterSqlStatus.trim();
    }

    private void setFilterSqlStatus(String status) {
        this.filterSqlStatus = status;
    }

    private void prepareFiltro(){
        Boolean filtroVisible = (cmdSQL.trim().equals("") || cmdSQL.trim().equals("0"));
        
        if (filtroVisible) {
            String labelStatus = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiListaGrupoAcao_00003");
            String naoAprovado = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiListaGrupoAcao_00004");
            String emAprovacao = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiListaGrupoAcao_00005");
            String aprovado    = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiListaGrupoAcao_00006");

            String[] statusLista = { naoAprovado, emAprovacao, aprovado };

            jBIListGrupo.setFiltroVisible(filtroVisible);
            jBIListGrupo.prepareFiltro(labelStatus, statusLista);
            jBIListGrupo.jCmbFiltro.setSelectedIndex(1);
            jBIListGrupo.jCmbFiltro.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    int status = jBIListGrupo.jCmbFiltro.getSelectedIndex();
                    filterPlanos(status);
                }
            });
        }
    }
    
    private void filterPlanos(int status) {
        status += 1;
        setFilterSqlStatus("STATUS = " + status);
        refreshFields();
    }

    private void loadRecord(String filter) {
        kpi.xml.BIXMLRecord recGrupos = event.listRecords("GRUPO_ACAO", filter);
        jBIListGrupo.setDataSource(recGrupos.getBIXMLVector("GRUPO_ACOES"), "0", "0", this.event);
        setRenderTable();
        geraTotalizador();
    }
}

class KpiListaGrupoAcaoTableCellRenderer extends javax.swing.table.DefaultTableCellRenderer {
    private kpi.beans.JBIXMLTable biTable = null;
    private java.awt.Color corVermelho = new java.awt.Color(234, 106, 106);
    private java.awt.Color corVerde = new java.awt.Color(139, 191, 150);
    private java.awt.Color corBranco = new java.awt.Color(255, 255, 255);
    private java.awt.Color corCinza = new java.awt.Color(212, 208, 200);
    private java.awt.Font fonteType = new java.awt.Font("Tahoma", 0, 11);
    private java.awt.Font fonteTotal = new java.awt.Font("Tahoma", java.awt.Font.BOLD, 11);
    private java.text.NumberFormat oValor = null;
    private pv.jfcx.JPVEdit oRender = new pv.jfcx.JPVEdit();
    private java.util.HashMap buffLinha = new java.util.HashMap();
    private int ALINHAMENTO_DIREITA = 2;
    public final static String COL_PROJ_ATRASADOS_VENC = "PORCATRASVENC";
    private java.util.Locale locale = kpi.core.KpiStaticReferences.getKpiDefaultLocale();

    public KpiListaGrupoAcaoTableCellRenderer() {
    }

    public KpiListaGrupoAcaoTableCellRenderer(kpi.beans.JBIXMLTable table) {
        biTable = table;
    }

    public void resetRendererBuffer() {
        buffLinha.clear();
    }

    // Sobreposicao para mudar o comportamento de pintura da celula.
    @Override
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        BIXMLRecord recLinha = null;
        int linha = biTable.getOriginalRow(row);

        oRender.setBackground(corBranco);
        oRender.setForeground(java.awt.Color.BLACK);
        oRender.setAlignment(0);
        oRender.setFontStyle(pv.jfcx.JPVEdit.NORMAL);
        oRender.setLocale(locale);
        oRender.setLAF(false);
        oRender.setBorderStyle(0);
        oRender.setFont(fonteType);
        oRender.setText("");
        oRender.setToolTipText("");

        if (biTable != null) {
            column = biTable.getColumn(column).getModelIndex();

            if (buffLinha.containsKey(linha)) {
                recLinha = (BIXMLRecord) buffLinha.get(linha);
            } else {
                recLinha = biTable.getXMLData().get(linha);
                buffLinha.put(linha, recLinha);
            }

            String colTag = biTable.getColumnTag(column);
            String tipoColuna = biTable.getColumnType(column);
            int colTipo = new Integer(tipoColuna).intValue();

            if (colTag.equals("FINALIZADAS") || colTag.equals("PORCATRASVENC") || colTag.equals("PORCFINVENC")) {
                renderPorcentagem(oRender, Double.parseDouble(value.toString()), column, row, recLinha);
            } else if (colTipo == JBIXMLTable.KPI_STRING) {
                renderString(oRender, value.toString(), column, row, recLinha);
            } else {
                renderNumber(oRender, Double.parseDouble(value.toString()), column, row, recLinha);
            }

            if(colTag.equals("OBSERVACAO")){
                oRender.setToolTipText(value.toString());
            }
        }
        String colName = biTable.getColumnTag(column);
        if (!colName.equals(COL_PROJ_ATRASADOS_VENC)) {
            if (recLinha.getInt("ID") == -1) {
                oRender.setFont(fonteTotal);
                oRender.setBackground(corCinza);
            } else {
                if (table.getSelectedRow() == row) {
                    oRender.setBackground(table.getSelectionBackground());
                    oRender.setForeground(table.getSelectionForeground());
                }
            }
        }

        return oRender;
    }

    private pv.jfcx.JPVEdit renderNumber(pv.jfcx.JPVEdit oRender, Double dbValor, int column, int row, BIXMLRecord recLinha) {
        oValor = java.text.NumberFormat.getInstance();
        oValor.setMinimumFractionDigits(2);
        oValor.setMaximumFractionDigits(2);
        oRender.setAlignment(2);
        oRender.setValue(oValor.format(dbValor));

        return oRender;
    }

    private pv.jfcx.JPVEdit renderString(pv.jfcx.JPVEdit oRender, String valor, int column, int row, BIXMLRecord recLinha) {
        oRender.setValue(valor);
        oRender.setAutoScroll(true);
        oRender.setLAF(false);
        return oRender;
    }

    private pv.jfcx.JPVEdit renderPorcentagem(pv.jfcx.JPVEdit oRender, Double dbValor, int column, int row, BIXMLRecord recLinha) {
        oValor = java.text.NumberFormat.getInstance();
        oValor.setMinimumFractionDigits(2);
        oValor.setMaximumFractionDigits(2);

        String colName = biTable.getColumnTag(column);
        if (colName.equals(COL_PROJ_ATRASADOS_VENC)) {
            if (dbValor.doubleValue() > 0.0) {
                oRender.setBackground(corVermelho);
            } else {
                oRender.setBackground(corVerde);
            }
        }

        if (recLinha.getInt("ID") == -1) {
            oRender.setFont(fonteTotal);
        }

        oRender.setAutoScroll(true);
        oRender.setLAF(false);
        oRender.setAlignment(ALINHAMENTO_DIREITA);

        oRender.setValue(oValor.format(dbValor).concat(" %"));

        return oRender;
    }
    
}
