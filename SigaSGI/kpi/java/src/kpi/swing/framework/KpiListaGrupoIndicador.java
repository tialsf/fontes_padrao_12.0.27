package kpi.swing.framework;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;
import javax.swing.JTabbedPane;
import kpi.beans.JBISeekListPanel;
import kpi.core.KpiStaticReferences;

public class KpiListaGrupoIndicador extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = new String("GRUPO_IND");
    private String formType = new String("LSTGRUPO_IND");
    private String parentType = recordType;

    public KpiListaGrupoIndicador(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
    }

    @Override
    public void disableFields() {
    }

    @Override
    public void refreshFields() {
        kpi.xml.BIXMLRecord recGrupos = event.listRecords("GRUPO_IND", "");
        jBIListGrupoIndicador.setDataSource(recGrupos.getBIXMLVector("GRUPO_INDS"), "0", "0", this.event);
        jBIListGrupoIndicador.xmlTable.setSortColumn(0);
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        tapCadastro = new JTabbedPane();
        jBIListGrupoIndicador = new JBISeekListPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiListaGrupoIndicador_00001")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_grupoindicador.gif"))); // NOI18N
        setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(600, 358));

        tapCadastro.setEnabled(false);
        tapCadastro.setFocusable(false);
        tapCadastro.setPreferredSize(new Dimension(0, 0));
        getContentPane().add(tapCadastro, BorderLayout.WEST);
        getContentPane().add(jBIListGrupoIndicador, BorderLayout.CENTER);

        pack();
    }//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JBISeekListPanel jBIListGrupoIndicador;
    private JTabbedPane tapCadastro;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public void loadRecord() {
        this.setID("0");
        this.refreshFields();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }
}
