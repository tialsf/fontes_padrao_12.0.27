package kpi.swing.framework;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;
import java.util.ResourceBundle;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import kpi.beans.JBISeekListPanel;
import kpi.beans.JBITreeSelection;
import kpi.core.KpiStaticReferences;
import kpi.xml.BIXMLRecord;

public class KpiListaIndicador extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "INDICADOR";
    private String formType = "LSTINDICADOR";
    private String parentType = recordType;
    private Hashtable htbScoreCard = new Hashtable();
    private kpi.xml.BIXMLVector vctIndicadores = null;
    private kpi.xml.BIXMLVector vctScoreCard = null;
    private javax.swing.JButton btnOrdem;
    private String ScoID = "";

    public KpiListaIndicador(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
        jBIListGrupoIndicador.setDataSource(newBlankList(), "0", "0", this.event);
        addBtnOrdem();
    }

    @Override
    public void enableFields() {
    }

    @Override
    public void disableFields() {
    }

    @Override
    public void refreshFields() {
        int itemSelecionado = cbbTopScorecard.getSelectedIndex();
        kpi.xml.BIXMLRecord recScoreCard = event.listRecords("SCORECARD", "LISTA_SCO_OWNER");
        vctScoreCard = recScoreCard.getBIXMLVector("SCORECARDS");
        event.populateCombo(vctScoreCard, "SCORECARD", htbScoreCard, cbbTopScorecard);
        tsArvore.setVetor(vctScoreCard);
        cbbTopScorecard.setSelectedIndex(itemSelecionado);
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", "0");
        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        pnlTopForm = new JPanel();
        cbbTopScorecard = new JComboBox();
        tsArvore = new JBITreeSelection();
        jBIListGrupoIndicador = new JBISeekListPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiListaIndicador_00001")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_indicador.gif"))); // NOI18N
        setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(685, 358));

        pnlTopForm.setBackground(new Color(204, 204, 204));
        pnlTopForm.setBorder(BorderFactory.createLineBorder(new Color(153, 153, 153)));
        pnlTopForm.setPreferredSize(new Dimension(100, 40));
        pnlTopForm.setLayout(null);

        cbbTopScorecard.setPreferredSize(new Dimension(310, 22));
        cbbTopScorecard.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                cbbTopScorecardActionPerformed(evt);
            }
        });
        pnlTopForm.add(cbbTopScorecard);
        cbbTopScorecard.setBounds(10, 10, 400, 22);

        tsArvore.setCombo(cbbTopScorecard);
        tsArvore.setRoot(false);
        pnlTopForm.add(tsArvore);
        tsArvore.setBounds(410, 10, 30, 22);

        getContentPane().add(pnlTopForm, BorderLayout.NORTH);
        getContentPane().add(jBIListGrupoIndicador, BorderLayout.CENTER);

        pack();
    }//GEN-END:initComponents

	private void cbbTopScorecardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbTopScorecardActionPerformed
            if (evt.getActionCommand().equals("comboBoxChanged")) {
                if (cbbTopScorecard.getItemCount() > 0) {
                    setScoID(event.getComboValue(htbScoreCard, cbbTopScorecard));
                    vctIndicadores = event.listRecords("INDICADOR", "ID_SCOREC = '".concat(getScoID()) + "'").getBIXMLVector("INDICADORES");
                    jBIListGrupoIndicador.setDataSource(vctIndicadores, "0", "0", this.event);
                    jBIListGrupoIndicador.xmlTable.getTable().getColumnModel().removeColumn(jBIListGrupoIndicador.xmlTable.getColumn(0));
                    jBIListGrupoIndicador.xmlTable.getTable().getColumnModel().removeColumn(jBIListGrupoIndicador.xmlTable.getColumn(1));
                }
            }
	}//GEN-LAST:event_cbbTopScorecardActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JComboBox cbbTopScorecard;
    private JBISeekListPanel jBIListGrupoIndicador;
    private JPanel pnlTopForm;
    private JBITreeSelection tsArvore;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public void loadRecord() {
        this.setRecord(new BIXMLRecord("TEMP"));
        this.setID("0");
        this.refreshFields();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return new javax.swing.JTabbedPane();
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    public kpi.xml.BIXMLVector getIndicadores() {
        return vctIndicadores;
    }

    public kpi.xml.BIXMLVector getVctScoreCard() {
        return vctScoreCard;
    }

    public String getScoID() {
        return ScoID;
    }

    public void setScoID(String ScoID) {
        this.ScoID = ScoID;
    }

    private kpi.xml.BIXMLVector newBlankList() {
        kpi.xml.BIXMLAttributes attIndicador = new kpi.xml.BIXMLAttributes();
        attIndicador.set("TIPO", "INDICADOR");
        attIndicador.set("TAG000", "NOME");
        attIndicador.set("CAB000", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiListaIndicador_00003"));
        attIndicador.set("CLA000", "4");
        attIndicador.set("RETORNA", "F");
        kpi.xml.BIXMLVector newList = new kpi.xml.BIXMLVector("INDICADORES", "INDICADOR", attIndicador);
        return newList;
    }

    private void addBtnOrdem() {
        btnOrdem = new javax.swing.JButton();
        btnOrdem.setFont(new java.awt.Font("Tahoma", 0, 11));
        btnOrdem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ordem.gif")));
        btnOrdem.setVisible(true);
        btnOrdem.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiListaIndicador_00004")); //"Ordenar"
        btnOrdem.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOrdemActionPerformed(evt);
            }
        });

        jBIListGrupoIndicador.tbInsertion.add(btnOrdem);
        jBIListGrupoIndicador.pnlOperation.setPreferredSize(new java.awt.Dimension(378, 27));
        jBIListGrupoIndicador.tbInsertion.setMaximumSize(new java.awt.Dimension(378, 25));
        jBIListGrupoIndicador.tbInsertion.setPreferredSize(new java.awt.Dimension(378, 32));
    }

    private void btnOrdemActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            kpi.core.KpiStaticReferences.getKpiFormController().getForm("ORDEM_INDICADOR", "0", cbbTopScorecard.getSelectedItem().toString());
        } catch (kpi.core.KpiFormControllerException e) {
            kpi.core.KpiDebug.println(e.getMessage());
        }

    }
}
