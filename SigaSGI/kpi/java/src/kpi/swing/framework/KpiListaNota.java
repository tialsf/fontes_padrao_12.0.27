package kpi.swing.framework;

/**
 * Created on 21/09/2010 @autor Valdiney V GOMES
 */
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import kpi.beans.JBIXMLTable;
import kpi.core.KpiCustomLabels;
import kpi.core.KpiDateBase;
import kpi.core.KpiEventTask;
import kpi.core.KpiImageResources;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultDialogSystem;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.xml.BIXMLAttributes;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;
import pv.jfcx.JPVDatePlus;
import pv.jfcx.JPVEdit;

public class KpiListaNota extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private static final int TAB_INDICADOR = 0;
    private static final int TAB_OBJETIVO = 1;
    private static final String recordType = "NOTA"; //NOI18N
    private static final String formType = "LSTNOTA"; //NOI18N
    private static final String TIPO_IND = "I"; //NOI18N
    private static final String TIPO_OBJ = "O"; //NOI18N
    private final String parentType = recordType;
    private final KpiDefaultFrameBehavior event = new KpiDefaultFrameBehavior(this);
    private final KpiCustomLabels customLabels = KpiStaticReferences.getKpiCustomLabels();
    private BIXMLVector vctNotaObj = new BIXMLVector("NOTASOBJ", "NOTA"); //NOI18N
    private BIXMLVector vctNotaInd = new BIXMLVector("NOTASIND", "NOTA"); //NOI18N
    private int status = KpiDefaultFrameBehavior.NORMAL;
    private String id;
    private BIXMLRecord record;
    private ScoItensSelected scoItensSelected = null;
    private TableDados tbDados = null;
    private KpiDefaultDialogSystem dialogSystem = kpi.core.KpiStaticReferences.getKpiDialogSystem();
    private BIXMLVector vecSegNota; //Seguranca para nota conforme parametro SEGURANCA_NOTA
    private KpiDateBase dateBase = KpiStaticReferences.getKpiDateBase();

    public KpiListaNota(int operation, String idAux, String contextId) {
        initComponents();
        //Criando a tabela manualmente, para possibilitar extender a classe.
        tbDados = tbDados = new TableDados();
        pnlCentro.add(tbDados, BorderLayout.CENTER);

        putClientProperty("MAXI", true); //NOI18N
        //
        event.defaultConstructor(operation, idAux, contextId);
        event.registerEvent(KpiDefaultFrameBehavior.EVENT_ON_INSERT, new KpiEventTask(this, "afterInsert", null, null)); //NOI18N
        event.registerEvent(KpiDefaultFrameBehavior.EVENT_ON_DELETE, new KpiEventTask(this, "afterDelete", null, null)); //NOI18N
        event.registerEvent(KpiDefaultFrameBehavior.EVENT_ON_UPDATE, new KpiEventTask(this, "afterUpdate", null, null)); //NOI18N
        event.setCloseFormOnDelete(KpiDefaultFrameBehavior.KEEP_FORM);

        //Define o per�odo de-at� com base no per�odo acumulado. 
        fldDataDe.setCalendar(dateBase.getDataAcumuladoDe());
        fldDataAte.setCalendar(dateBase.getDataAcumuladoAte());

        addListenerToObjects();
    }

    @Override
    public void enableFields() {
        event.setEnableFields(pnlCima, true);
        txtObs.setEditable(true);
        btnCopy.setEnabled(true);
    }

    @Override
    public void disableFields() {
        final ArrayList<JPanel> lstPanel = new ArrayList<JPanel>();
        lstPanel.add(pnlCima);
        lstPanel.add(pnlVlrInd);

        for (JPanel pnl : lstPanel) {
            event.setEnableFields(pnl, false);
            //Habilitando os Labels
            for (Component cp : pnl.getComponents()) {
                if (cp instanceof JLabel) {
                    cp.setEnabled(true);
                }
            }
        }
        // para observa��o deve ser poss�vel copiar o conte�do.
        txtObs.setEnabled(true);
        txtObs.setEditable(false);
        btnCopy.setEnabled(true);
    }

    @Override
    /**
     * Recarrega a lista.
     */
    public void refreshFields() {
        if (scoItensSelected != null) {
            //Muda o comando SQL para XML.
            BIXMLAttributes att = new BIXMLAttributes();
            att.set("XML_COMMAND", true); //NOI18N

            //Requisi��o
            BIXMLRecord request = new BIXMLRecord("CMDSQL", att); //NOI18N
            request.set("DATA_DE", fldDataDe.getCalendar()); //NOI18N
            request.set("DATA_ATE", fldDataAte.getCalendar()); //NOI18N
            request.set("ID_INDICADOR", scoItensSelected.getIdIndicador()); //NOI18N

            final BIXMLRecord lista = event.listRecords("NOTA", request.toString()); //NOI18N

            loadTableIndObj(lista.getBIXMLVector("NOTAS")); //NOI18N
            vecSegNota = lista.getBIXMLVector("SEGURANCA_NOTA");
            refreshDetailItens();
        }
    }

    /**
     * Separa as anota��es dos indicadores e dos objetivos.
     */
    private void loadTableIndObj(BIXMLVector vctNotasOri) {
        final BIXMLAttributes attColumn = new BIXMLAttributes();
        vctNotaObj = new BIXMLVector("NOTASOBJ", "NOTA"); //NOI18N
        vctNotaInd = new BIXMLVector("NOTASIND", "NOTA"); //NOI18N

        for (BIXMLRecord recItem : vctNotasOri) {
            if (recItem.getString("TPNOTA").equals(TIPO_IND)) {
                vctNotaInd.add(recItem);
            } else if (recItem.getString("TPNOTA").equals(TIPO_OBJ)) {
                vctNotaObj.add(recItem);
            }
        }

        //Caso n�o haja nenhum item
        if (vctNotaObj.size() < 1 || vctNotaInd.size() < 1) {
            attColumn.set("TIPO", "NOTA"); //NOI18N
            attColumn.set("RETORNA", "F"); //NOI18N
            attColumn.set("TAG000", "NOME"); //NOI18N
            attColumn.set("CAB000", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiListaNota_00001")); //NOI18N
            attColumn.set("CLA000", "4"); //NOI18N
        }

        if (vctNotaInd.size() < 1) {
            vctNotaInd.setAttributes(attColumn);
        } else {
            vctNotaInd.setAttributes(vctNotasOri.getAttributes());
        }

        if (vctNotaObj.size() < 1) {
            vctNotaObj.setAttributes(attColumn);
        } else {
            vctNotaObj.setAttributes(vctNotasOri.getAttributes());
        }

        if (tapCadastro.getSelectedIndex() == TAB_INDICADOR) {
            tbDados.setDataSource(vctNotaInd);
        } else {
            tbDados.setDataSource(vctNotaObj);
        }
    }

    private void refreshDetailItens() {

        if (tbDados.getSelectedRow() > -1) {
            setRecord(tbDados.getSelectedRecord());
            record.set("DESEMPENHO", scoItensSelected.getDesempenho());
        }else{
            BIXMLRecord defValor = new BIXMLRecord("RECORD_TMP");
            defValor.set("NOME", "");
            defValor.set("ID", "-1");
            defValor.set("NOTA", "");
            defValor.set("META", 0);
            defValor.set("VALOR", 0);
            defValor.set("DESEMPENHO", scoItensSelected.formatValor(0).concat("%"));
            defValor.set("PERIODO", scoItensSelected.getPeriodo());
            defValor.set("SEGURANCA", KpiImageResources.KPI_IMG_CAD_ABERTO);
            setRecord(defValor);
        }

        int security = KpiImageResources.KPI_IMG_CAD_VERMELHO;
        if (record != null) {

            fldAssunto.setText(record.getString("NOME"));
            setID(record.getString("ID"));
            txtObs.setText(record.getString("NOTA"));

            if (getStatus() == KpiDefaultFrameBehavior.INSERTING) {
                lblVlMeta.setText(scoItensSelected.getVlrMeta());
                lblVlReal.setText(scoItensSelected.getVlrReal());
                lblVlPeriodo.setText(scoItensSelected.getPeriodo());
                lblVlDes.setText(scoItensSelected.getDesempenho());
                //
                security = KpiImageResources.KPI_IMG_CAD_ABERTO;
            } else {
                double meta = 0;
                double valor = 0;
                String des = scoItensSelected.formatValor(0).concat("%");

                if (tapCadastro.getSelectedIndex() == TAB_INDICADOR) {
                    meta = record.getDouble("META"); //NOI18N
                    valor = record.getDouble("VALOR"); //NOI18N
                    des  = record.getString("DESEMPENHO"); //NOI18N
                }

                lblVlMeta.setText(scoItensSelected.formatValor(meta));
                lblVlReal.setText(scoItensSelected.formatValor(valor));
                lblVlDes.setText(des);
                lblVlPeriodo.setText(record.getString("PERIODO"));


                if (record.contains("SEGURANCA")) { //NOI18N
                    security = record.getInt("SEGURANCA"); //NOI18N
                }
            }

            if (tapCadastro.getSelectedIndex() == TAB_INDICADOR) {
                lblVlNome.setText(scoItensSelected.getNomeIndicador());
            } else {
                lblVlNome.setText(scoItensSelected.getNomeObjetivo());
            }
        }
        
        applySecurity(security);
    }

    private String getObjetivoTabName() {
        StringBuilder name = new StringBuilder(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiListaNota_00002"));
        name.append(customLabels.getSco(KpiStaticReferences.CAD_OBJETIVO));

        return name.toString();
    }

    private void applySecurity(int security) {
        
        if (security == KpiImageResources.KPI_IMG_CAD_ABERTO) {
            btnEdit.setEnabled(true);
            btnDelete.setEnabled(true);
        } else {
            btnEdit.setEnabled(false);
            btnDelete.setEnabled(false);
        }

        if (vecSegNota.getAttributes().getBoolean("SEGURANCA_NOTA")) {
         
            if (vecSegNota.getAttributes().getBoolean("RESPIND") || vecSegNota.getAttributes().getBoolean("RESPCOL") || vecSegNota.getAttributes().getBoolean("RESPOBJ")) {
                btnNew.setEnabled(true);
                tapCadastro.setEnabledAt(0,true);                
            } else {
                btnNew.setEnabled(false);
            }
            
            if (vecSegNota.getAttributes().getBoolean("RESPOBJ")) {
                tapCadastro.setEnabledAt(1,true);
            } else {
                tapCadastro.setEnabledAt(1, false);
            }
        }else{
            tapCadastro.setEnabledAt(0,true);                            
            tapCadastro.setEnabledAt(1,true);                
        }        
    }

    private void refreshTableLineByDetail() {
        tbDados.getSelectedRecord().set("NOME", fldAssunto.getText()); //NOI18N
        tbDados.getSelectedRecord().set("NOTA", txtObs.getText()); //NOI18N
        tbDados.getSelectedRecord().set("META", scoItensSelected.getVlrMeta()); //NOI18N
        tbDados.getSelectedRecord().set("VALOR", scoItensSelected.getVlrReal()); //NOI18N
    }

    private boolean canUserEdit() {
        boolean canEdit = true;

        if (tbDados.getSelectedRow() == -1) {
            canEdit = false;
            dialogSystem.warningMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiListaNota_00003"));
        }

        return canEdit;
    }

    @Override
    public BIXMLRecord getFieldsContents() {
        final BIXMLRecord dados = new BIXMLRecord(recordType);

        dados.set("NOME", fldAssunto.getText()); //NOI18N
        dados.set("NOTA", txtObs.getText()); //NOI18N

        if (getStatus() == KpiDefaultFrameBehavior.INSERTING) {
            dados.set("ID_INDICA", scoItensSelected.getIdIndicador()); //NOI18N
            dados.set("META", scoItensSelected.getVlrMeta()); //NOI18N
            dados.set("VALOR", scoItensSelected.getVlrReal()); //NOI18N
            dados.set("DATAALVO", scoItensSelected.getdAlvo()); //NOI18N

            if (tapCadastro.getSelectedIndex() == TAB_OBJETIVO) {
                dados.set("TPNOTA", TIPO_OBJ); //NOI18N
            } else {
                dados.set("TPNOTA", TIPO_IND); //NOI18N
            }
        }

        return dados;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean validacao = true;

        if (fldAssunto.getText().isEmpty()) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiListaNota_00004"));
            validacao = false;
        }

        if (txtObs.getText().isEmpty()) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiListaNota_00005"));
            validacao = false;
        }

        if (!validacao) {
            btnSave.setEnabled(true);
        }

        return validacao;
    }

    private void enabledDisableTab(boolean enable) {
        if (enable) {
            tapCadastro.setEnabledAt(TAB_OBJETIVO, enable);
            tapCadastro.setEnabledAt(TAB_INDICADOR, enable);
        } else {
            if (tapCadastro.getSelectedIndex() == TAB_OBJETIVO) {
                tapCadastro.setEnabledAt(TAB_INDICADOR, enable);
            } else {
                tapCadastro.setEnabledAt(TAB_OBJETIVO, enable);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new JTabbedPane();
        pnlNote = new JPanel();
        pnlCima = new JPanel();
        lblNome = new JLabel();
        lblVlNome = new JLabel();
        lblPeriodo = new JLabel();
        lblVlPeriodo = new JLabel();
        pnlVlrInd = new JPanel();
        lblMeta = new JLabel();
        lblVlMeta = new JLabel();
        lblReal = new JLabel();
        lblVlReal = new JLabel();
        lblDes = new JLabel();
        lblVlDes = new JLabel();
        jSeparator1 = new JSeparator();
        lblAssunto = new JLabel();
        fldAssunto = new JPVEdit();
        scrNota = new JScrollPane();
        txtObs = new JTextArea();
        lblObs = new JLabel();
        tbCopy = new JToolBar();
        btnCopy = new JButton();
        pnlCentro = new JPanel();
        pnlBarInd = new JPanel();
        fldDataDe = new JPVDatePlus();
        fldDataAte = new JPVDatePlus();
        btnReload = new JButton();
        jLabel1 = new JLabel();
        jLabel2 = new JLabel();
        pnlBaixo = new JPanel();
        lblLegAma = new JLabel();
        lblLegAzul = new JLabel();
        pnlNoteObj = new JPanel();
        pnlManutencao = new JPanel();
        pnlConfirmation = new JPanel();
        tbConfirmation = new JToolBar();
        btnSave = new JButton();
        btnCancel = new JButton();
        pnlOperation = new JPanel();
        tbInsertion = new JToolBar();
        btnNew = new JButton();
        btnEdit = new JButton();
        btnDelete = new JButton();
        pnlLeftForm = new JPanel();
        pnlBottomForm1 = new JPanel();
        pnlRightForm = new JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiListaNota_00006")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_lembrete.gif"))); // NOI18N
        setNormalBounds(new Rectangle(10, 10, 480, 600));
        setPreferredSize(new Dimension(600, 458));

        tapCadastro.setFocusable(false);
        tapCadastro.setPreferredSize(new Dimension(648, 458));

        pnlNote.setLayout(new BorderLayout());

        pnlCima.setPreferredSize(new Dimension(10, 300));

        lblNome.setText(bundle.getString("KpiListaNota_00007")); // NOI18N
        lblNome.setFocusable(false);

        lblVlNome.setText(bundle.getString("KpiListaNota_00008")); // NOI18N
        lblVlNome.setFocusable(false);

        lblPeriodo.setText(bundle.getString("KpiListaNota_00009")); // NOI18N
        lblPeriodo.setFocusable(false);

        lblVlPeriodo.setText("null");
        lblVlPeriodo.setFocusable(false);

        pnlVlrInd.setLayout(new GridLayout(1, 0));

        lblMeta.setText(bundle.getString("KpiListaNota_00010")); // NOI18N
        lblMeta.setFocusable(false);
        pnlVlrInd.add(lblMeta);

        lblVlMeta.setText("null");
        lblVlMeta.setFocusable(false);
        pnlVlrInd.add(lblVlMeta);

        lblReal.setText(bundle.getString("KpiListaNota_00011")); // NOI18N
        lblReal.setFocusable(false);
        pnlVlrInd.add(lblReal);

        lblVlReal.setText("null");
        lblVlReal.setFocusable(false);
        pnlVlrInd.add(lblVlReal);

        lblDes.setHorizontalAlignment(SwingConstants.RIGHT);
        lblDes.setText("Desempenho:");
        lblDes.setFocusable(false);
        pnlVlrInd.add(lblDes);

        lblVlDes.setHorizontalAlignment(SwingConstants.RIGHT);
        lblVlDes.setText("null");
        pnlVlrInd.add(lblVlDes);

        jSeparator1.setPreferredSize(new Dimension(0, 1));

        lblAssunto.setForeground(new Color(51, 51, 255));
        lblAssunto.setText(bundle.getString("KpiListaNota_00013")); // NOI18N
        lblAssunto.setFocusable(false);

        txtObs.setColumns(31);
        txtObs.setFont(new Font("Tahoma", 0, 11)); // NOI18N
        txtObs.setLineWrap(true);
        txtObs.setRows(5);
        txtObs.setTabSize(4);
        txtObs.setWrapStyleWord(true);
        scrNota.setViewportView(txtObs);

        lblObs.setForeground(new Color(51, 51, 255));
        lblObs.setText(bundle.getString("KpiListaNota_00014")); // NOI18N
        lblObs.setFocusable(false);

        tbCopy.setFloatable(false);
        tbCopy.setRollover(true);
        tbCopy.setPreferredSize(new Dimension(250, 32));

        btnCopy.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_copy.gif"))); // NOI18N
        ResourceBundle bundle1 = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnCopy.setText(bundle1.getString("JBIListPanel_00001")); // NOI18N
        btnCopy.setToolTipText("Copiar texto selecionado");
        btnCopy.setFocusable(false);
        btnCopy.setHorizontalTextPosition(SwingConstants.RIGHT);
        btnCopy.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnCopy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCopyActionPerformed(evt);
            }
        });
        tbCopy.add(btnCopy);

        GroupLayout pnlCimaLayout = new GroupLayout(pnlCima);
        pnlCima.setLayout(pnlCimaLayout);
        pnlCimaLayout.setHorizontalGroup(pnlCimaLayout.createParallelGroup(Alignment.LEADING)
            .addGroup(pnlCimaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlCimaLayout.createParallelGroup(Alignment.LEADING)
                    .addGroup(pnlCimaLayout.createSequentialGroup()
                        .addComponent(lblAssunto)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(pnlCimaLayout.createSequentialGroup()
                        .addGroup(pnlCimaLayout.createParallelGroup(Alignment.LEADING)
                            .addComponent(jSeparator1, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlVlrInd, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(scrNota)
                            .addGroup(pnlCimaLayout.createSequentialGroup()
                                .addComponent(lblNome, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(ComponentPlacement.RELATED)
                                .addGroup(pnlCimaLayout.createParallelGroup(Alignment.LEADING)
                                    .addGroup(pnlCimaLayout.createSequentialGroup()
                                        .addComponent(lblVlPeriodo, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(350, 350, 350))
                                    .addComponent(lblVlNome, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(fldAssunto, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(pnlCimaLayout.createSequentialGroup()
                                .addGroup(pnlCimaLayout.createParallelGroup(Alignment.LEADING)
                                    .addComponent(lblPeriodo, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
                                    .addGroup(pnlCimaLayout.createSequentialGroup()
                                        .addComponent(lblObs)
                                        .addPreferredGap(ComponentPlacement.RELATED)
                                        .addComponent(tbCopy, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        pnlCimaLayout.setVerticalGroup(pnlCimaLayout.createParallelGroup(Alignment.LEADING)
            .addGroup(pnlCimaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlCimaLayout.createParallelGroup(Alignment.BASELINE)
                    .addComponent(lblNome)
                    .addComponent(lblVlNome))
                .addPreferredGap(ComponentPlacement.RELATED)
                .addGroup(pnlCimaLayout.createParallelGroup(Alignment.BASELINE)
                    .addComponent(lblPeriodo, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblVlPeriodo))
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(pnlVlrInd, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, GroupLayout.PREFERRED_SIZE, 5, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(lblAssunto)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addGroup(pnlCimaLayout.createParallelGroup(Alignment.TRAILING)
                    .addGroup(pnlCimaLayout.createSequentialGroup()
                        .addComponent(fldAssunto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(ComponentPlacement.UNRELATED)
                        .addComponent(lblObs))
                    .addComponent(tbCopy, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(scrNota, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnlNote.add(pnlCima, BorderLayout.NORTH);

        pnlCentro.setPreferredSize(new Dimension(200, 160));
        pnlCentro.setLayout(new BorderLayout(0, 20));

        pnlBarInd.setBorder(BorderFactory.createEtchedBorder());

        fldDataDe.setDialog(true);
        fldDataDe.setUseLocale(true);
        fldDataDe.setWaitForCalendarDate(true);

        fldDataAte.setDialog(true);
        fldDataAte.setUseLocale(true);
        fldDataAte.setWaitForCalendarDate(true);

        btnReload.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setFocusable(false);
        btnReload.setHorizontalTextPosition(SwingConstants.RIGHT);
        btnReload.setMinimumSize(new Dimension(95, 60));
        btnReload.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnReload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });

        jLabel1.setText(bundle.getString("KpiListaNota_00017")); // NOI18N

        jLabel2.setText(bundle.getString("KpiListaNota_00018")); // NOI18N

        GroupLayout pnlBarIndLayout = new GroupLayout(pnlBarInd);
        pnlBarInd.setLayout(pnlBarIndLayout);
        pnlBarIndLayout.setHorizontalGroup(pnlBarIndLayout.createParallelGroup(Alignment.LEADING)
            .addGroup(pnlBarIndLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(10, 10, 10)
                .addComponent(fldDataDe, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(fldDataAte, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(btnReload, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(256, Short.MAX_VALUE))
        );
        pnlBarIndLayout.setVerticalGroup(pnlBarIndLayout.createParallelGroup(Alignment.LEADING)
            .addGroup(pnlBarIndLayout.createParallelGroup(Alignment.CENTER)
                .addComponent(fldDataAte, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(fldDataDe, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(btnReload, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel1)
                .addComponent(jLabel2))
        );

        pnlCentro.add(pnlBarInd, BorderLayout.PAGE_START);

        pnlNote.add(pnlCentro, BorderLayout.CENTER);

        pnlBaixo.setPreferredSize(new Dimension(10, 60));
        pnlBaixo.setLayout(new GridLayout(3, 1));

        lblLegAma.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_cadeado_aberto.gif"))); // NOI18N
        lblLegAma.setText(bundle.getString("KpiListaNota_00015")); // NOI18N
        lblLegAma.setFocusable(false);
        pnlBaixo.add(lblLegAma);

        lblLegAzul.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_cadeado_vermelho.gif"))); // NOI18N
        lblLegAzul.setText(bundle.getString("KpiListaNota_00016")); // NOI18N
        lblLegAzul.setFocusable(false);
        pnlBaixo.add(lblLegAzul);

        pnlNote.add(pnlBaixo, BorderLayout.SOUTH);

        tapCadastro.addTab("Anota��es do Indicador", pnlNote);

        pnlNoteObj.setLayout(new BorderLayout());
        tapCadastro.addTab(getObjetivoTabName(), pnlNoteObj);

        getContentPane().add(tapCadastro, BorderLayout.CENTER);

        pnlManutencao.setMinimumSize(new Dimension(480, 27));
        pnlManutencao.setPreferredSize(new Dimension(10, 29));
        pnlManutencao.setLayout(new BorderLayout());

        pnlConfirmation.setMaximumSize(new Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new Dimension(155, 27));
        pnlConfirmation.setLayout(new BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new Dimension(150, 32));

        btnSave.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, BorderLayout.CENTER);

        pnlManutencao.add(pnlConfirmation, BorderLayout.EAST);

        pnlOperation.setMaximumSize(new Dimension(340, 27));
        pnlOperation.setMinimumSize(new Dimension(340, 27));
        pnlOperation.setPreferredSize(new Dimension(340, 27));
        pnlOperation.setLayout(new BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new Dimension(250, 32));

        btnNew.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_novo.gif"))); // NOI18N
        btnNew.setText(bundle1.getString("JBIListPanel_00001")); // NOI18N
        btnNew.setFocusable(false);
        btnNew.setHorizontalTextPosition(SwingConstants.RIGHT);
        btnNew.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnNew.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });
        tbInsertion.add(btnNew);

        btnEdit.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        pnlOperation.add(tbInsertion, BorderLayout.CENTER);

        pnlManutencao.add(pnlOperation, BorderLayout.WEST);

        getContentPane().add(pnlManutencao, BorderLayout.NORTH);
        getContentPane().add(pnlLeftForm, BorderLayout.WEST);

        pnlBottomForm1.setPreferredSize(new Dimension(5, 5));
        getContentPane().add(pnlBottomForm1, BorderLayout.SOUTH);
        getContentPane().add(pnlRightForm, BorderLayout.EAST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNewActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        this.setStatus(KpiDefaultFrameBehavior.INSERTING);
        event.defaultConstructor(KpiDefaultFrameBehavior.INSERTING, "0", ""); //NOI18N
        enabledDisableTab(false);
    }//GEN-LAST:event_btnNewActionPerformed

    private void btnEditActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        if (canUserEdit()) {
            enabledDisableTab(false);
            event.editRecord();
        }
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnDeleteActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        if (canUserEdit()) {
            enabledDisableTab(false);
            event.deleteRecord();
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnSaveActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        event.saveRecord();
        enabledDisableTab(true);
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnCancelActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        int oldStatus = getStatus();

        enabledDisableTab(true);
        setStatus(KpiDefaultFrameBehavior.NORMAL);
        event.cancelOperation();

        if (oldStatus == KpiDefaultFrameBehavior.INSERTING) {
            tbDados.selFirstLine();
        }
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnReloadActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
        event.loadRecord();
    }//GEN-LAST:event_btnReloadActionPerformed

    /**
     * Copia texto selecionado do campo Observa��o.
     *      
     * @param evt Evento do bot�o
     *
     * @since 27/11/2015
     * @author Helio Leal
     */
    private void btnCopyActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnCopyActionPerformed
        Clipboard board = Toolkit.getDefaultToolkit().getSystemClipboard();  
        ClipboardOwner selection = new StringSelection(txtObs.getSelectedText());  
        board.setContents((Transferable) selection, selection);         
    }//GEN-LAST:event_btnCopyActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnCancel;
    public JButton btnCopy;
    private JButton btnDelete;
    private JButton btnEdit;
    public JButton btnNew;
    private JButton btnReload;
    private JButton btnSave;
    private JPVEdit fldAssunto;
    private JPVDatePlus fldDataAte;
    private JPVDatePlus fldDataDe;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JSeparator jSeparator1;
    private JLabel lblAssunto;
    private JLabel lblDes;
    private JLabel lblLegAma;
    private JLabel lblLegAzul;
    private JLabel lblMeta;
    private JLabel lblNome;
    private JLabel lblObs;
    private JLabel lblPeriodo;
    private JLabel lblReal;
    private JLabel lblVlDes;
    private JLabel lblVlMeta;
    private JLabel lblVlNome;
    private JLabel lblVlPeriodo;
    private JLabel lblVlReal;
    private JPanel pnlBaixo;
    private JPanel pnlBarInd;
    private JPanel pnlBottomForm1;
    private JPanel pnlCentro;
    private JPanel pnlCima;
    private JPanel pnlConfirmation;
    private JPanel pnlLeftForm;
    private JPanel pnlManutencao;
    private JPanel pnlNote;
    private JPanel pnlNoteObj;
    private JPanel pnlOperation;
    private JPanel pnlRightForm;
    private JPanel pnlVlrInd;
    private JScrollPane scrNota;
    private JTabbedPane tapCadastro;
    private JToolBar tbConfirmation;
    private JToolBar tbCopy;
    private JToolBar tbInsertion;
    private JTextArea txtObs;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        //recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) { //NOI18N
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return ""; //NOI18N
        }
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public void loadRecord() {
        this.setID("0"); //NOI18N
        this.refreshFields();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    public void afterInsert(){
        refreshDetailItens();
        tbDados.selFirstLine();        
    }
    
    public void afterDelete() {
        refreshFields();
        tbDados.selFirstLine();
    }

    public void afterUpdate() {
        refreshTableLineByDetail();

        tbDados.setDataSource(tbDados.getXMLData(false));
    }

    /**
     * Seta os valores selecionados no Scorecarding.
     *
     * @param idInd Id do indicador
     * @param nomeInd Nome do indicador
     * @param nomeObj Nome do objetivo
     * @param vlReal valor real
     * @param vlMeta Valor da meta
     * @param periodo Periodo selecionado do indicador, data alvo.
     * @param dAlvo Data do alvo.
     */    
    public void setScoItensSelected(String idInd, String nomeInd, String nomeObj, String periodo,
            double real, double meta, double desem, int dec, Calendar dAlvo) {

        scoItensSelected = new ScoItensSelected(idInd, nomeInd, nomeObj, periodo, real, meta, desem, dec, dAlvo);

        refreshFields();
        tbDados.selFirstLine();
    }

    private void addListenerToObjects() {

        tbDados.getTable().addKeyListener(new java.awt.event.KeyAdapter() {

            @Override
            public void keyReleased(java.awt.event.KeyEvent evt) {
                xmlTableKeyReleased(evt);
            }
        });

        tbDados.getTable().addMouseListener(new java.awt.event.MouseAdapter() {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                xmlTableMouseClicked(evt);
            }
        });


        tapCadastro.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent changeEvent) {

                if (tapCadastro.getSelectedIndex() == TAB_INDICADOR) {
                    pnlNote.add(pnlCima, BorderLayout.NORTH);
                    pnlNote.add(pnlCentro, BorderLayout.CENTER);
                    pnlNote.add(pnlBaixo, BorderLayout.SOUTH);
                    tbDados.setDataSource(vctNotaInd);
                    lblNome.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiListaNota_00007"));
                    lblVlNome.setText(scoItensSelected.getNomeIndicador());
                } else {
                    pnlNoteObj.add(pnlCima, BorderLayout.NORTH);
                    pnlNoteObj.add(pnlCentro, BorderLayout.CENTER);
                    pnlNoteObj.add(pnlBaixo, BorderLayout.SOUTH);
                    tbDados.setDataSource(vctNotaObj);
                    lblNome.setText(customLabels.getSco(KpiStaticReferences.CAD_OBJETIVO).concat(":")); //NOI18N
                    lblVlNome.setText(scoItensSelected.getNomeObjetivo());
                }

                tbDados.selFirstLine();
            }
        });
    }

    private void xmlTableMouseClicked(java.awt.event.MouseEvent evt) {
        refreshDetailItens();
    }

    private void xmlTableKeyReleased(java.awt.event.KeyEvent evt) {
        if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN) {
            refreshDetailItens();
        }
    }

    private class TableDados extends JBIXMLTable {

        private final ArrayList<Integer> colPos = new ArrayList<Integer>();

        @Override
        public void setDataSource(kpi.xml.BIXMLVector dataSource) {
            //Tratamento para o construtor
            if (colPos == null) {
                super.setDataSource(dataSource);
            } else {
                //1�-Passagem
                if (colPos.isEmpty()) {
                    super.setDataSource(dataSource);
                    setDefaultColSize();
                    saveColPos();
                } else {
                    saveColPos();
                    super.setDataSource(dataSource);
                    restColPos();
                }
            }
        }

        private void saveColPos() {
            JTable jTable = this.getJTable();

            if (jTable.getColumnCount() > 1) {
                colPos.clear();
                for (int iCol = 0; iCol < jTable.getColumnCount(); iCol++) {
                    colPos.add(this.getColumn(iCol).getPreferredWidth());
                }
            }
        }

        private void restColPos() {
            int iColPos = 0;
            int iQtdCol = this.getJTable().getColumnCount();

            for (Integer iColSize : colPos) {
                if (iColPos < iQtdCol) {
                    this.getColumn(iColPos).setPreferredWidth(iColSize);
                }
                iColPos++;
            }
            this.setSortColumn(1, false);
            this.doLayout();
        }

        private void setDefaultColSize() {
            if (this.getJTable().getColumnCount() > 1) {
                this.getColumn(5).setPreferredWidth(900);
            }
            this.setSortColumn(1, false);
            this.doLayout();
        }

        private void selFirstLine() {
            javax.swing.JTable jTable = this.getJTable();
            if (jTable.getRowCount() >= 1) {
                jTable.setRowSelectionInterval(0, 0);
            }
            refreshDetailItens();            
        }
    }

    /**
     * Classe que representa os itens selecionados no Scorecarding.
     */
    private class ScoItensSelected {

        private String idIndicador = ""; //NOI18N
        private String nomeIndicador = ""; //NOI18N
        private String nomeObjetivo = ""; //NOI18N
        private String periodo = ""; //NOI18N
        private double vlrReal = 0;
        private double vlrMeta = 0;
        private double desempenho = 0;
        private Calendar dAlvo = null;
        private final NumberFormat nf2 = NumberFormat.getNumberInstance();

        /**
         *
         * @param idInd id do indicador
         * @param nomeInd nome do indicador
         * @param idObj id do objetivo
         * @param nomeObj nome do objetivo
         * @param vlReal valor real
         * @param vlMeta Valor da meta
         * @param periodo Periodo selecionado do indicador, data alvo.
         * @param dAlvo Data do alvo.
         */
        private ScoItensSelected(String idInd, String nomeInd, String nomeObj, String periodo,
                double vlReal, double vlMeta, double desem, int dec, Calendar dAlvo) {
            this.idIndicador = idInd;
            this.nomeIndicador = nomeInd;
            this.nomeObjetivo = nomeObj;
            this.vlrMeta = vlMeta;
            this.vlrReal = vlReal;
            this.periodo = periodo;
            this.dAlvo = dAlvo;
            this.desempenho = desem;

            nf2.setMinimumFractionDigits(dec);
            nf2.setMaximumFractionDigits(dec);
        }

        /**
         * @return the idIndicador
         */
        private String getIdIndicador() {
            return idIndicador;
        }

        /**
         * @return the nomeIndicador
         */
        private String getNomeIndicador() {
            return nomeIndicador;
        }

        /**
         * @return the nomeObjetivo
         */
        private String getNomeObjetivo() {
            return nomeObjetivo;
        }

        /**
         * @return the vlrReal
         */
        private String getVlrReal() {
            double vReal = 0;
            if (tapCadastro.getSelectedIndex() == TAB_INDICADOR) {
                vReal = vlrReal;
            }
            return formatValor(vReal);
        }

        /**
         * @return the vlrMeta
         */
        private String getVlrMeta() {
            double vMeta = 0;
            if (tapCadastro.getSelectedIndex() == TAB_INDICADOR) {
                vMeta = vlrMeta;
            }

            return formatValor(vMeta);
        }

        /**
         * @return the periodo
         */
        private String getPeriodo() {
            return periodo;
        }

        private String formatValor(double vl) {
            return nf2.format(vl);
        }

        private String getDesempenho() {
            double des = 0;
            if (tapCadastro.getSelectedIndex() == TAB_INDICADOR) {
                des = desempenho;
            }
            return nf2.format(des).concat("%"); //NOI18N

        }

        private Calendar getdAlvo() {
            return dAlvo;
        }
    }
}
