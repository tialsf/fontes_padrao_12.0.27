package kpi.swing.framework;

import java.awt.event.ItemEvent;
import kpi.beans.JBIXMLTable;
import kpi.core.KpiStaticReferences;
import kpi.xml.BIXMLAttributes;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

public class KpiListaPlanos extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    public final static String COL_STATUS_PLANO = "STATUS";
    public final static int FILTER_MYACTION = 0;
    public final static int FILTER_SCORECARD = 1;
    public final static int FILTER_PLANACTION = 2;
    public final static int FILTER_USER = 3;
    private String recordType = "PLANOACAO";
    private String formType = "LSTPLANOACAO";
    private String parentType = recordType;
    private kpi.xml.BIXMLVector vctDados = null;
    private kpi.xml.BIXMLVector vctFiltroSco = null;
    private kpi.xml.BIXMLVector vctFiltroGrp = null;
    private kpi.xml.BIXMLVector vctFiltroPadrao = null;
    private kpi.xml.BIXMLVector blanklist = null;
    private java.util.Hashtable htbGrp = new java.util.Hashtable();
    private java.util.Hashtable htbSco = new java.util.Hashtable();
    private java.util.Hashtable htbPadrao = new java.util.Hashtable();
    private java.util.Hashtable htbUser = new java.util.Hashtable();
    private KpiListaPlanosTableCellRenderer cellRenderer;
    private kpi.beans.JBIXMLTable listaTable;
    private String cmdSQL = new String();
    private boolean isRefresh = false;
    private BIXMLVector vctUsuarios;
    private BIXMLVector vctTreeUser;

    @Override
    public void enableFields() {
    }

    @Override
    public void disableFields() {
        this.loadRecord();
    }

    public KpiListaPlanos(int operation, String idAux, String cmdSQL) {
        initComponents();

        putClientProperty("MAXI", true);
        blanklist = new kpi.xml.BIXMLVector("INDICADORES", "INDICADOR", getTableAttributes());
        event.defaultConstructor(operation, idAux, "");

        if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
            tsArvore.setEntitySelection(KpiStaticReferences.CAD_OBJETIVO);
        }

        setID(idAux);
    }

    @Override
    public void refreshFields() {
        setRecord(new BIXMLRecord("PLANOACAO"));

        if (isRefresh) {
            int nFilterType = cboFilterType.getSelectedIndex();
            String sFilterItem = "";

            switch (cboFilterType.getSelectedIndex()) {
                case FILTER_MYACTION: //Minhas a��es
                    sFilterItem = event.getComboValue(htbPadrao, cboFilterItem);
                    vctFiltroPadrao = null;
                    break;
                case FILTER_SCORECARD: //Scorecard
                    sFilterItem = event.getComboValue(htbSco, cboFilterItem);
                    vctFiltroSco = null;
                    break;
                case FILTER_PLANACTION: //Plano de A��o
                    sFilterItem = event.getComboValue(htbGrp, cboFilterItem);
                    vctFiltroGrp = null;
                    break;
                case FILTER_USER: //Usuario
                    sFilterItem = event.getComboValue(htbUser, cboFilterItem);
                    vctUsuarios = null;
                    vctTreeUser = null;
                    break;
            }


            buildFilter(nFilterType, sFilterItem);
        }
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);
        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        listaPlanos = new kpi.beans.JBISeekListPanel();
        pnlTopo = new javax.swing.JPanel();
        cboFilterType = new javax.swing.JComboBox();
        cboFilterItem = new javax.swing.JComboBox();
        tsArvore = new kpi.beans.JBITreeSelection();
        tsUsers = new kpi.beans.JBITreeSelection(kpi.beans.JBITreeSelection.TYPE_DEFAULT);

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiPlanoDeAcao_00017")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_planodeacao.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(800, 358));
        getContentPane().add(listaPlanos, java.awt.BorderLayout.CENTER);

        pnlTopo.setBackground(new java.awt.Color(204, 204, 204));
        pnlTopo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        pnlTopo.setPreferredSize(new java.awt.Dimension(10, 40));
        pnlTopo.setLayout(null);

        cboFilterType.setMaximumSize(new java.awt.Dimension(140, 22));
        cboFilterType.setMinimumSize(new java.awt.Dimension(70, 22));
        cboFilterType.setPreferredSize(new java.awt.Dimension(140, 22));
        cboFilterType.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboFilterTypeItemStateChanged(evt);
            }
        });
        pnlTopo.add(cboFilterType);
        cboFilterType.setBounds(10, 10, 180, 22);

        cboFilterItem.setMaximumSize(new java.awt.Dimension(32767, 22));
        cboFilterItem.setMinimumSize(new java.awt.Dimension(50, 22));
        cboFilterItem.setPreferredSize(new java.awt.Dimension(140, 22));
        cboFilterItem.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboFilterItemItemStateChanged(evt);
            }
        });
        pnlTopo.add(cboFilterItem);
        cboFilterItem.setBounds(210, 10, 400, 22);

        tsArvore.setCombo(cboFilterItem);
        tsArvore.setEnabled(false);
        tsArvore.setRoot(false);
        pnlTopo.add(tsArvore);
        tsArvore.setBounds(610, 10, 25, 22);

        tsUsers.setCombo(cboFilterItem);
        tsUsers.setEnabled(false);
        tsUsers.setRoot(false);
        pnlTopo.add(tsUsers);
        tsUsers.setBounds(610, 10, 25, 22);

        getContentPane().add(pnlTopo, java.awt.BorderLayout.NORTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cboFilterTypeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboFilterTypeItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            populateCboFilter();
            listaPlanos.setDataSource(blanklist, "0", "0", this.event);
            setRenderTable();
        }
    }//GEN-LAST:event_cboFilterTypeItemStateChanged

    private void cboFilterItemItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboFilterItemItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            loadAction();
        }

    }//GEN-LAST:event_cboFilterItemItemStateChanged
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cboFilterItem;
    private javax.swing.JComboBox cboFilterType;
    private kpi.beans.JBISeekListPanel listaPlanos;
    private javax.swing.JPanel pnlTopo;
    private kpi.beans.JBITreeSelection tsArvore;
    private kpi.beans.JBITreeSelection tsUsers;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    private void populateCboFilter() {
        boolean habilita = true;

        cboFilterItem.removeAllItems();
        tsArvore.setEnabled(false);
        tsUsers.setEnabled(false);
        tsUsers.setVisible(false);
        tsArvore.setVisible(false);

        switch (cboFilterType.getSelectedIndex()) {
            case FILTER_MYACTION: //Minhas a��es
                if (vctFiltroPadrao == null) {
                    kpi.xml.BIXMLRecord recScore = event.listRecords("PLANOACAO", "CBOFILTER");
                    vctFiltroPadrao = recScore.getBIXMLVector("MYACTION_FILTERS");
                }
                event.populateCombo(vctFiltroPadrao, "NOME", htbPadrao, cboFilterItem);

                break;

            case FILTER_SCORECARD: //Scorecard
                habilita = false;

                if (vctFiltroSco == null) {
                    kpi.xml.BIXMLRecord recScore = event.listRecords("SCORECARD", "LISTA_SCO_OWNER");
                    vctFiltroSco = recScore.getBIXMLVector("SCORECARDS");
                }
                event.populateCombo(vctFiltroSco, "NOME", htbSco, cboFilterItem);
                tsArvore.setVetor(vctFiltroSco);
                tsArvore.setEnabled(true);
                tsArvore.setVisible(true);
                break;

            case FILTER_PLANACTION: //Plano de A��o
                if (vctFiltroGrp == null) {
                    kpi.xml.BIXMLRecord recPlanos = event.listRecords("GRUPO_ACAO", "");
                    vctFiltroGrp = recPlanos.getBIXMLVector("GRUPO_ACOES");
                }
                event.populateCombo(vctFiltroGrp, "NOME", htbGrp, cboFilterItem);
                break;

            case FILTER_USER: //Usuario
                habilita = false;

                if (vctUsuarios == null) {
                    kpi.xml.BIXMLRecord recPlanos = event.listRecords("PLANOACAO", "USUARIOS");

                    //Recupera a lista de usu�rios.
                    vctUsuarios = recPlanos.getBIXMLVector("RESPONSAVEIS");
                    vctTreeUser = recPlanos.getBIXMLVector("TREEUSERS");
                }

                //Responsavel pelo indicador
                event.populateCombo(vctUsuarios, "___", htbUser, cboFilterItem);
                tsUsers.setVetor(vctUsuarios);
                tsUsers.setTree(vctTreeUser);
                tsUsers.setEnabled(true);
                tsUsers.setVisible(true);
                break;
        }

        if (this.cboFilterItem.getModel().getSize() > 1) {
            if (habilita) {
                this.cboFilterItem.setEnabled(true);
            } else {
                this.cboFilterItem.setEnabled(false);
            }
        } else {
            this.cboFilterItem.setEnabled(false);
        }
    }

    private void populateCboType() {
        cboFilterType.removeAllItems();
        cboFilterType.addItem("Minhas A��es");
        cboFilterType.addItem(KpiStaticReferences.getKpiCustomLabels().getSco(KpiStaticReferences.CAD_OBJETIVO)); //"Scorecard"
        cboFilterType.addItem("Plano de A��o");
        cboFilterType.addItem("Usu�rio");
        cboFilterType.setSelectedIndex(0);
    }

    public void buildFilter(int filter, String filterId) {
        int retItem = 0;

        isRefresh = true;

        //Popula e posiciona a combo tipo
        populateCboType();
        cboFilterType.setSelectedIndex(filter);

        //Popula e posiciona a combo filter
        populateCboFilter();
        switch (cboFilterType.getSelectedIndex()) {
            case FILTER_MYACTION: //Minhas a��es
                retItem = event.getComboItem(vctFiltroPadrao, filterId, true);
                break;

            case FILTER_SCORECARD: //Scorecard
                retItem = event.getComboItem(vctFiltroSco, filterId, true);
                break;

            case FILTER_PLANACTION: //Plano de A��o
                retItem = event.getComboItem(vctFiltroGrp, filterId, true);
                break;

            case FILTER_USER: //Usuario
                retItem = event.getComboItem(vctUsuarios, filterId, true);
                break;
        }
        if (retItem != -1 && cboFilterItem.getItemCount() > 0) {
            cboFilterItem.setSelectedIndex(retItem);
        }

        //Preenche planilha
        listaPlanos.setDataSource(blanklist, "0", "0", this.event);
        loadAction();

    }

    private void loadAction() {
        StringBuilder sbParm = new StringBuilder();
        switch (cboFilterType.getSelectedIndex()) {
            case FILTER_MYACTION: //Minhas a��es
                if (cboFilterItem.getSelectedIndex() == 1) {        //(Todas)
                    sbParm.append("PLANO_USER_ALL");
                } else if (cboFilterItem.getSelectedIndex() == 2) { //Vencidas
                    sbParm.append("LIST_PA_VENCIDO");
                } else if (cboFilterItem.getSelectedIndex() == 3) { //A Vencer
                    sbParm.append("LIST_PA_AVENCER");
                }
                break;

            case FILTER_SCORECARD: //Scorecard
                sbParm.append("PLANO_SCORECARD|");
                sbParm.append(event.getComboValue(htbSco, cboFilterItem));
                break;

            case FILTER_PLANACTION: //Plano de A��o
                sbParm.append("PLANO_GRUPO_ACAO|");
                sbParm.append(event.getComboValue(htbGrp, cboFilterItem));
                break;

            case FILTER_USER: //Usuario
                sbParm.append("PLANO_USUARIO|");
                if (event.getComboValue(htbUser, cboFilterItem).substring(0, 1).equals("0")) {
                    sbParm.append("|");
                } else {
                    sbParm.append(event.getComboValue(htbUser, cboFilterItem).substring(1));
                    sbParm.append("|");
                    sbParm.append(event.getComboValue(htbUser, cboFilterItem).substring(0, 1));
                }
                break;
        }

        if (sbParm.toString().length() > 0) {
            kpi.xml.BIXMLRecord recPlanos = event.listRecords("PLANOACAO", sbParm.toString());
            setRecord(recPlanos);
            vctDados = recPlanos.getBIXMLVector("PLANOSACAO");
        } else {
            vctDados = blanklist;
        }

        listaPlanos.setDataSource(vctDados, "0", "0", this.event);
        setRenderTable();

    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public void loadRecord() {
        this.setID("0");
        this.refreshFields();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return new javax.swing.JTabbedPane();
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    public kpi.beans.JBIXMLTable getListaTable() {
        return listaTable;
    }

    public void setListaTable(kpi.beans.JBIXMLTable listaTable) {
        this.listaTable = listaTable;
    }

    /*
     *Seta o Render para a tabela.
     */
    private void setRenderTable() {
        String tipoColuna = null;
        listaPlanos.xmlTable.setHeaderHeight(20);
        listaPlanos.xmlTable.getColumn(0).setMaxWidth(30);
        listaPlanos.xmlTable.getColumn(2).setPreferredWidth(50);
        listaPlanos.xmlTable.getColumn(3).setPreferredWidth(350);
        listaPlanos.xmlTable.getColumn(4).setPreferredWidth(40);
        listaPlanos.xmlTable.getColumn(5).setPreferredWidth(40);
        listaPlanos.xmlTable.getColumn(6).setPreferredWidth(40);
        listaPlanos.xmlTable.getColumn(7).setPreferredWidth(40);
        setListaTable(listaPlanos.xmlTable);
        cellRenderer = new KpiListaPlanosTableCellRenderer(getListaTable());

        /*
         * Configurando as propriedades das colunas, e renderer.
         */
        for (int col = 0; col < listaTable.getModel().getColumnCount(); col++) {
            //Defini��o do render especifico para as colunas do tipo texto.
            tipoColuna = getListaTable().getColumnType(col);
            int colTipo = new Integer(tipoColuna).intValue();

            if (colTipo == JBIXMLTable.KPI_STRING) {
                getListaTable().getColumn(col).setCellRenderer(cellRenderer);
            }
        }
        listaPlanos.xmlTable.setSortColumn(1);
    }

    public String getCmdSQL() {
        return cmdSQL.trim();
    }

    public void setCmdSQL(String cmdSQL) {
        this.cmdSQL = cmdSQL;
    }

    public void filterTable(String idIndicador) {
        BIXMLRecord recPlanoAcao;
        BIXMLVector vctFilterData = new BIXMLVector("PLANOSACAO", "PLANOACAO");

        for (int item = 0; item < vctDados.size(); item++) {
            recPlanoAcao = vctDados.get(item);
            if (recPlanoAcao.getString("ID_IND").equals(idIndicador)) {
                vctFilterData.add(recPlanoAcao);
            }
        }

        vctFilterData.setAttributes(getTableAttributes());

        listaPlanos.setDataSource(vctFilterData, "0", "0", this.event);
        listaPlanos.btnReload.setVisible(false);
        listaPlanos.btnNew.setVisible(false);
        setRenderTable();
    }

    private BIXMLAttributes getTableAttributes() {
        BIXMLAttributes attributes = new BIXMLAttributes();
        attributes.set("TAG000", "IMG_OWNER");
        attributes.set("CAB000", "");
        attributes.set("CLA000", "6");

        attributes.set("TAG001", "ID_IND");
        attributes.set("CAB001", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiIndicador_00034"));
        attributes.set("CLA001", "4");

        attributes.set("TAG002", "ID_SCOREC");
        attributes.set("CAB002", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScorecard_00001"));
        attributes.set("CLA002", "4");

        //Descricao
        attributes.set("TAG003", "NOME");
        attributes.set("CAB003", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMainPanel_00022"));
        attributes.set("CLA003", "4");

        //Como
        attributes.set("TAG004", "DATACADAST");
        attributes.set("CAB004", "Cadastro");
        attributes.set("CLA004", "4");

        //Responsavel
        attributes.set("TAG005", "RESPONSAVEL");
        attributes.set("CAB005", "Respons�vel");
        attributes.set("CLA005", "4");

        //Responsavel
        attributes.set("TAG006", "DATAFIM");
        attributes.set("CAB006", "T�rmino Prev.");
        attributes.set("CLA006", "4");

        //Status
        attributes.set("TAG007", "STATUS");
        attributes.set("CAB007", "Status");
        attributes.set("CLA007", "4");

        attributes.set("TIPO", "PLANOACAO");
        attributes.set("RETORNA", "F");

        return attributes;
    }
}

class KpiListaPlanosTableCellRenderer extends javax.swing.table.DefaultTableCellRenderer {

    private kpi.beans.JBIXMLTable biTable = null;
    public final static java.awt.Color VERMELHO = new java.awt.Color(234, 106, 106);
    public final static java.awt.Color AMARELO = new java.awt.Color(255, 235, 155);
    public final static java.awt.Color VERDE = new java.awt.Color(139, 191, 150);
    public final static java.awt.Color CINZA = new java.awt.Color(228, 228, 228);

    public KpiListaPlanosTableCellRenderer() {
    }

    public KpiListaPlanosTableCellRenderer(kpi.beans.JBIXMLTable table) {
        biTable = table;
    }

    // Sobreposicao para mudar o comportamento de pintura da celula.
    @Override
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        pv.jfcx.JPVEdit oRender = new pv.jfcx.JPVEdit();
        BIXMLRecord recLinha = null;
        int linha = biTable.getOriginalRow(row);

        oRender.setLocale(kpi.core.KpiStaticReferences.getKpiDefaultLocale());
        oRender.setLAF(false);
        oRender.setBorderStyle(0);
        oRender.setFont(new java.awt.Font("Tahoma", 0, 11));
        oRender.setBackground(java.awt.Color.WHITE);
        oRender.setForeground(java.awt.Color.BLACK);
        oRender.setAlignment(0);
        oRender.setFontStyle(pv.jfcx.JPVEdit.NORMAL);
        oRender.setText("");
        oRender.setToolTipText("");

        if (biTable != null) {
            column = biTable.getColumn(column).getModelIndex();
            recLinha = biTable.getXMLData().get(linha);

            renderString(oRender, value.toString(), column, row, recLinha);
        }
        String colName = biTable.getColumnTag(column);
        if (!colName.equals(KpiListaPlanos.COL_STATUS_PLANO)) {
            if (hasFocus) {
                oRender.setBackground(new java.awt.Color(212, 208, 200));
            } else if (table.getSelectedRow() == row) {
                oRender.setBackground(table.getSelectionBackground());
                oRender.setForeground(table.getSelectionForeground());
            }
        }
        return oRender;
    }

    private pv.jfcx.JPVEdit renderString(pv.jfcx.JPVEdit oRender, String valor, int column, int row, BIXMLRecord recLinha) {
        oRender.setValue(valor);
        String colName = biTable.getColumnTag(column);
        if (colName.equals(KpiListaPlanos.COL_STATUS_PLANO)) {
            int status = recLinha.getInt("STATUS_COLOR");
            switch (status) {
                case 1:
                    oRender.setBackground(AMARELO);
                    break;
                case 2:
                    oRender.setBackground(java.awt.Color.white);
                    break;
                case 3:
                    oRender.setBackground(CINZA);
                    break;
                case 4:
                    oRender.setBackground(VERMELHO);
                    break;
                case 5:
                    oRender.setBackground(VERDE);
                    break;
            }
        }
        oRender.setAutoScroll(true);
        oRender.setLAF(false);

        return oRender;
    }
}
