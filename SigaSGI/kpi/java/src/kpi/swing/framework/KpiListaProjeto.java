package kpi.swing.framework;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Hashtable;
import java.util.ResourceBundle;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import kpi.beans.JBISeekListPanel;
import kpi.beans.JBITreeSelection;
import kpi.beans.JBIXMLTable;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

public class KpiListaProjeto extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    public final static int FILTER_MYPROJECT = 0;
    public final static int FILTER_SCORECARD = 1;
    public final static int FILTER_USER = 2;
    //
    private String recordType = "PROJETO";
    private String formType = "LSTPROJETO";
    private String parentType = recordType;
    private BIXMLVector vctFiltroSco;
    private BIXMLVector vctFiltroUser;
    private BIXMLVector vctTreeUser;
    private Hashtable htbSco = new Hashtable();
    private Hashtable htbUser = new Hashtable();
    private boolean isRefresh = false;

    public KpiListaProjeto(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, "");
        setID(idAux);

        if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
            tsArvore.setEntitySelection(KpiStaticReferences.CAD_OBJETIVO);
        }
    }

    @Override
    public void enableFields() {
        event.setEnableFields(pnlTopForm, true);
    }

    @Override
    public void disableFields() {
    }

    @Override
    public void refreshFields() {
        setRecord(new BIXMLRecord("PROJETO"));

        if (isRefresh) {
            int nFilterType = cboFilterType.getSelectedIndex();
            String sFilterItem = "";

            switch (cboFilterType.getSelectedIndex()) {

                //Minhas a��es.
                case FILTER_MYPROJECT:
                    break;

                //Scorecard.
                case FILTER_SCORECARD:
                    sFilterItem = event.getComboValue(htbSco, cboFilterItem);
                    vctFiltroSco = null;
                    break;

                //Usuario
                case FILTER_USER:
                    sFilterItem = event.getComboValue(htbUser, cboFilterItem);
                    vctFiltroUser = null;
                    break;
            }

            buildFilter(nFilterType, sFilterItem);
        }
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);
        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    /**
     * Popula o combo com os tipos de filtros dispon�veis.
     * @param filter Indice a ser selecionado na combo.
     */
    private void populateCboType(int filter) {
        cboFilterType.removeAllItems();
        cboFilterType.addItem("Meus Projetos");

        if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
            cboFilterType.addItem("Objetivo");
        } else {
            cboFilterType.addItem(KpiStaticReferences.getKpiCustomLabels().getSco()); //"Scorecard"
        }

        cboFilterType.addItem("Usu�rio");
        cboFilterType.setSelectedIndex(filter);
    }

    /**
     * Popula o combo de filtro de acordo com o tipo de filtro selecionado.
     */
    private void populateCboFilter() {
        boolean habilita = true;

        cboFilterItem.removeAllItems();
        tsUsers.setEnabled(false);
        tsUsers.setVisible(false);
        tsArvore.setEnabled(false);
        tsArvore.setVisible(false);

        switch (cboFilterType.getSelectedIndex()) {
            //Meus projetos.
            case FILTER_MYPROJECT:

                cboFilterItem.removeAllItems();
                cboFilterItem.addItem("[Todos]");

                break;

            //Scorecard.
            case FILTER_SCORECARD:
                habilita = false;

                if (vctFiltroSco == null) {
                    BIXMLRecord recScore = event.listRecords("SCORECARD", "LISTA_SCO_OWNER");
                    vctFiltroSco = recScore.getBIXMLVector("SCORECARDS");
                }
                event.populateCombo(vctFiltroSco, "NOME", htbSco, cboFilterItem);
                tsArvore.setVetor(vctFiltroSco);
                tsArvore.setEnabled(true);
                tsArvore.setVisible(true);

                loadAction();
                break;

            //Usuario.
            case FILTER_USER:
                habilita = false;

                if (vctFiltroUser == null) {
                    BIXMLRecord recProjeto = event.listRecords(recordType, "USUARIOS");
                    vctFiltroUser = recProjeto.getBIXMLVector("RESPONSAVEIS");
                    vctTreeUser = recProjeto.getBIXMLVector("TREEUSERS");
                }

                event.populateCombo(vctFiltroUser, "NOME", htbUser, cboFilterItem);
                tsUsers.setVetor(vctFiltroUser);
                tsUsers.setTree(vctTreeUser);
                tsUsers.setEnabled(true);
                tsUsers.setVisible(true);
                
                loadAction();
                break;
        }

        if (this.cboFilterItem.getModel().getSize() > 1) {
            if (habilita) {
                this.cboFilterItem.setEnabled(true);
            } else {
                this.cboFilterItem.setEnabled(false);
            }
        } else {
            this.cboFilterItem.setEnabled(false);
        }
    }

    /**
     * Define o filtro que ser� aplicado.
     * @param filter Tipo do filtro.
     * @param filterId Indice do combobox do filtro que ser� aplicado.
     */
    public void buildFilter(int filter, String filterId) {
        int retItem = 0;
        isRefresh = true;

        //Popula e posiciona a combo tipo
        populateCboType(filter);

        //Popula e posiciona a combo filter
        populateCboFilter();

        switch (cboFilterType.getSelectedIndex()) {

            //Minhas a��es.
            case FILTER_MYPROJECT:
                break;

            //Scorecard.
            case FILTER_SCORECARD:
                retItem = event.getComboItem(vctFiltroSco, filterId, true);
                break;

            //Usuario.
            case FILTER_USER:
                retItem = event.getComboItem(vctFiltroUser, filterId, true);
                break;
        }

        if (retItem != -1 && cboFilterItem.getItemCount() > 0) {
            cboFilterItem.setSelectedIndex(retItem);
        }

        loadAction();
    }

    /**
     * Realiza a requisi��o de dados de acordo com o filtro aplicado.
     */
    private void loadAction() {
        StringBuilder sbParm = new StringBuilder();

        switch (cboFilterType.getSelectedIndex()) {

            //Meus Projetos.
            case FILTER_MYPROJECT:

                if (cboFilterItem.getSelectedIndex() == 0) {
                    sbParm.append("PROJETO_USER_ALL");
                }
                break;

            //Scorecard.
            case FILTER_SCORECARD:

                sbParm.append("PROJETO_SCORECARD|");
                sbParm.append(event.getComboValue(htbSco, cboFilterItem));
                break;

            //Usuario.
            case FILTER_USER:

                sbParm.append("PROJETO_USUARIO|");
                sbParm.append(event.getComboValue(htbUser, cboFilterItem));

                break;
        }

        kpi.xml.BIXMLRecord recPlanos = event.listRecords("PROJETO", sbParm.toString());
        listProjeto.setDataSource(recPlanos.getBIXMLVector("PROJETOS"), "0", "0", this.event);
        setRecord(recPlanos);

        geraTotalizador();
        setRenderTable();
    }

    /**
     * Cria com a totaliza��o dos resultados dos projetos na lista de projetos.
     */
    private void geraTotalizador() {
        kpi.xml.BIXMLVector vecLista = listProjeto.xmlTable.getXMLData();

        int qtdLinhas = vecLista.size();
        int nTotVencidas = 0;
        int nTotAcoes = 0;

        double nPercFinVenc = 0;
        double nPercAtras = 0;
        double nFinalizadas = 0;

        //Caso tenha mais de um registro insere totalizador
        if (qtdLinhas > 1) {
            for (int lin = 0; lin < qtdLinhas; lin++) {
                nTotVencidas += vecLista.get(lin).getInt("VENCIDAS");
                nTotAcoes += vecLista.get(lin).getInt("TOTALACOES");
                nPercFinVenc += vecLista.get(lin).getDouble("PORCFINVENC");
                nPercAtras += vecLista.get(lin).getDouble("PORCATRASVENC");
                nFinalizadas += vecLista.get(lin).getDouble("FINALIZADAS");
            }

            if (nPercFinVenc > 0) {
                nPercFinVenc = (nPercFinVenc / qtdLinhas);
            }
            if (nPercAtras > 0) {
                nPercAtras = (nPercAtras / qtdLinhas);
            }
            if (nFinalizadas > 0) {
                nFinalizadas = (nFinalizadas / qtdLinhas);
            }

            kpi.xml.BIXMLRecord newRecord = new kpi.xml.BIXMLRecord("GRUPO_ACAO");
            newRecord.set("ID", "-1");
            newRecord.set("ID_SCORE", "-1");
            //"Total de a��es"
            newRecord.set("NOME", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiListaProjeto_00003"));
            newRecord.set("VENCIDAS", nTotVencidas);
            newRecord.set("PORCFINVENC", nPercFinVenc);
            newRecord.set("PORCATRASVENC", nPercAtras);
            newRecord.set("TOTALACOES", nTotAcoes);
            newRecord.set("FINALIZADAS", nFinalizadas);

            listProjeto.xmlTable.addNewRecord(newRecord);
        }
    }

    /**
     * Define a forma de renderiza��o da tabela.
     */
    private void setRenderTable() {
        JBIXMLTable table = listProjeto.xmlTable;

        table.setHeaderHeight(35);
        table.getColumn(0).setPreferredWidth(850);

        cellRenderer = new KpiListaProjetoTableCellRenderer(table);

        for (int col = 0; col < 6; col++) {
            table.getColumn(col).setCellRenderer(cellRenderer);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new JTabbedPane();
        pnlTopForm = new JPanel();
        cboFilterItem = new JComboBox();
        cboFilterType = new JComboBox();
        tsArvore = new JBITreeSelection();
        tsUsers = new JBITreeSelection(JBITreeSelection.TYPE_DEFAULT);
        listProjeto = new JBISeekListPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiProjeto_00005")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_projeto.gif")));         setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(600, 358));

        tapCadastro.setEnabled(false);
        tapCadastro.setFocusable(false);
        tapCadastro.setPreferredSize(new Dimension(0, 0));
        getContentPane().add(tapCadastro, BorderLayout.WEST);

        pnlTopForm.setBackground(new Color(204, 204, 204));
        pnlTopForm.setBorder(BorderFactory.createLineBorder(new Color(153, 153, 153)));
        pnlTopForm.setPreferredSize(new Dimension(10, 40));
        pnlTopForm.setLayout(null);

        cboFilterItem.setEnabled(false);
        cboFilterItem.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                cboFilterItemItemStateChanged(evt);
            }
        });
        pnlTopForm.add(cboFilterItem);
        cboFilterItem.setBounds(210, 10, 400, 22);

        cboFilterType.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                cboFilterTypeItemStateChanged(evt);
            }
        });
        pnlTopForm.add(cboFilterType);
        cboFilterType.setBounds(10, 10, 180, 22);

        tsArvore.setCombo(cboFilterItem);
        tsArvore.setRoot(false);
        pnlTopForm.add(tsArvore);
        tsArvore.setBounds(610, 10, 25, 22);

        tsUsers.setCombo(cboFilterItem);
        tsUsers.setEnabled(false);
        tsUsers.setRoot(false);
        pnlTopForm.add(tsUsers);
        tsUsers.setBounds(610, 10, 25, 22);

        getContentPane().add(pnlTopForm, BorderLayout.NORTH);
        getContentPane().add(listProjeto, BorderLayout.CENTER);

        getAccessibleContext().setAccessibleName(bundle.getString("KpiProjeto_00005")); // NOI18N

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cboFilterItemItemStateChanged(ItemEvent evt) {//GEN-FIRST:event_cboFilterItemItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            loadAction();
        }
    }//GEN-LAST:event_cboFilterItemItemStateChanged

    private void cboFilterTypeItemStateChanged(ItemEvent evt) {//GEN-FIRST:event_cboFilterTypeItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            populateCboFilter();
        }
    }//GEN-LAST:event_cboFilterTypeItemStateChanged
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JComboBox cboFilterItem;
    private JComboBox cboFilterType;
    private JBISeekListPanel listProjeto;
    private JPanel pnlTopForm;
    private JTabbedPane tapCadastro;
    private JBITreeSelection tsArvore;
    private JBITreeSelection tsUsers;
    // End of variables declaration//GEN-END:variables
    private KpiListaProjetoTableCellRenderer cellRenderer;
    private KpiDefaultFrameBehavior event = new KpiDefaultFrameBehavior(this);
    private int status = KpiDefaultFrameBehavior.NORMAL;
    private String id = "";
    private BIXMLRecord record = null;

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public void loadRecord() {
        this.setID("0");
        this.refreshFields();
    }

    @Override
    public JTabbedPane getTabbedPane() {
        return new javax.swing.JTabbedPane();
    }
}

class KpiListaProjetoTableCellRenderer extends javax.swing.table.DefaultTableCellRenderer {

    private kpi.beans.JBIXMLTable biTable = null;
    private java.awt.Color corVermelho = new java.awt.Color(234, 106, 106);
    private java.awt.Color corVerde = new java.awt.Color(139, 191, 150);
    private java.awt.Color corBranco = new java.awt.Color(255, 255, 255);
    private java.awt.Color corCinza = new java.awt.Color(212, 208, 200);
    private java.awt.Font fonteType = new java.awt.Font("Tahoma", 0, 11);
    private java.awt.Font fonteTotal = new java.awt.Font("Tahoma", java.awt.Font.BOLD, 11);
    private java.text.NumberFormat oValor = null;
    private pv.jfcx.JPVEdit oRender = new pv.jfcx.JPVEdit();
    private java.util.HashMap buffLinha = new java.util.HashMap();
    private int ALINHAMENTO_DIREITA = 2;
    private java.util.Locale locale = kpi.core.KpiStaticReferences.getKpiDefaultLocale();
    private final String COL_PROJ_ATRASADOS_VENC = "PORCATRASVENC";

    public KpiListaProjetoTableCellRenderer() {
    }

    public KpiListaProjetoTableCellRenderer(kpi.beans.JBIXMLTable table) {
        biTable = table;
    }

    public void resetRendererBuffer() {
        buffLinha.clear();
    }

    @Override
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        BIXMLRecord recLinha = null;
        int linha = biTable.getOriginalRow(row);

        oRender.setBackground(corBranco);
        oRender.setForeground(java.awt.Color.BLACK);
        oRender.setAlignment(0);
        oRender.setFontStyle(pv.jfcx.JPVEdit.NORMAL);
        oRender.setLocale(locale);
        oRender.setLAF(false);
        oRender.setBorderStyle(0);
        oRender.setFont(fonteType);
        oRender.setText("");
        oRender.setToolTipText("");

        if (biTable != null) {
            column = biTable.getColumn(column).getModelIndex();

            if (buffLinha.containsKey(linha)) {
                recLinha = (BIXMLRecord) buffLinha.get(linha);
            } else {
                recLinha = biTable.getXMLData().get(linha);
                buffLinha.put(linha, recLinha);
            }

            String tipoColuna = biTable.getColumnType(column);
            String colTag = biTable.getColumnTag(column);
            int colTipo = new Integer(tipoColuna).intValue();

            if (colTag.equals("FINALIZADAS") || colTag.equals("PORCATRASVENC") || colTag.equals("PORCFINVENC")) {
                renderPorcentagem(oRender, Double.parseDouble(value.toString()), column, row, recLinha);
            } else if (colTipo == JBIXMLTable.KPI_STRING) {
                renderString(oRender, value.toString(), column, row, recLinha);
            } else {
                renderNumber(oRender, value.toString(), column, row, recLinha);
            }

        }

        String colName = biTable.getColumnTag(column);

        if (!colName.equals(COL_PROJ_ATRASADOS_VENC)) {
            if (recLinha.getInt("ID") == -1) {
                oRender.setFont(fonteTotal);
                oRender.setBackground(corCinza);
            } else if (table.getSelectedRow() == row) {
                oRender.setBackground(table.getSelectionBackground());
                oRender.setForeground(table.getSelectionForeground());
            }
        }

        return oRender;
    }

    private pv.jfcx.JPVEdit renderNumber(pv.jfcx.JPVEdit oRender, String valor, int column, int row, BIXMLRecord recLinha) {
        oRender.setValue(valor);
        oRender.setAutoScroll(true);
        oRender.setLAF(false);
        oRender.setAlignment(2);
        return oRender;
    }

    private pv.jfcx.JPVEdit renderString(pv.jfcx.JPVEdit oRender, String valor, int column, int row, BIXMLRecord recLinha) {
        oRender.setValue(valor);
        oRender.setAutoScroll(true);
        oRender.setLAF(false);
        return oRender;
    }

    private pv.jfcx.JPVEdit renderPorcentagem(pv.jfcx.JPVEdit oRender, Double valor, int column, int row, BIXMLRecord recLinha) {
        oValor = java.text.NumberFormat.getInstance();
        oValor.setMinimumFractionDigits(2);
        oValor.setMaximumFractionDigits(2);

        String colName = biTable.getColumnTag(column);
        if (colName.equals(COL_PROJ_ATRASADOS_VENC)) {
            if (valor.doubleValue() > 0.0) {
                oRender.setBackground(corVermelho);
            } else {
                oRender.setBackground(corVerde);
            }
        }

        if (recLinha.getInt("ID") == -1) {
            oRender.setFont(fonteTotal);
        }

        oRender.setAutoScroll(true);
        oRender.setLAF(false);
        oRender.setAlignment(ALINHAMENTO_DIREITA);
        oRender.setValue(oValor.format(valor).concat(" %"));


        return oRender;
    }
}
