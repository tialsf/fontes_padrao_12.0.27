package kpi.swing.framework;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import kpi.beans.JBITextArea;
import kpi.beans.JBITreeSelection;
import kpi.core.KpiStaticReferences;
import pv.jfcx.JPVEdit;

public class KpiMetaFormula extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = new String("METAFORMULA");
    private String formType = recordType;//Tipo do Formulario
    private String parentType = "LSTMETAFORMULA";//Tipo formulario pai, caso haja.
    private Vector vctFormula = new Vector();
    private kpi.swing.KpiDefaultDialogSystem dialog = kpi.core.KpiStaticReferences.getKpiDialogSystem();
    java.util.Hashtable htbScoreCard = new java.util.Hashtable(),
            htbIndicadores = new java.util.Hashtable();
    private kpi.xml.BIXMLVector vctScoreCard = null; //Vetor com todos os indicadores carregado do formulario de lista.

    /**
     *
     * @param operation
     * @param idAux
     * @param contextId
     */
    public KpiMetaFormula(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);

        configuraCache();

        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
        //Habilita todos os campos do formul�rio.
        event.setEnableFields(pnlMetaFormula, true);
    }

    @Override
    public void disableFields() {
        //Desabilita todos os campos do formul�rio.
        event.setEnableFields(pnlMetaFormula, false);
    }

    @Override
    public void refreshFields() {
        //Nome.
        fldNome.setText(record.getString("NOME"));

        //Descri��o.
        txtDescricao.setText(record.getString("DESCRICAO"));

        //Scorecards.
        event.populateCombo(vctScoreCard, "SCORECARD", htbScoreCard, cbbScoreFormula);
        tsArvore.setVetor(vctScoreCard);

        //F�rmula.
        stringToFormula(record.getString("FORMULA"));//Atualiza o campo da f�rmula.

        if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
	    tsArvore.setEntitySelection(KpiStaticReferences.CAD_OBJETIVO);
	}
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);
        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("FORMULA", formulaToString());

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        if (fldNome.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMetaFormula_00011"));
            retorno = false;
        } else if (fldNome.getText().indexOf(">") != -1) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMetaFormula_00012"));
            retorno = false;
        } else if (txtDescricao.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMetaFormula_00013"));
            retorno = false;
        } else if (edtFormula.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMetaFormula_00014"));
            retorno = false;
        }

        if (!retorno) {
            btnSave.setEnabled(true);
        }

        return retorno;
    }

    /**
     * Recupera a lista de scorecards carregada no formul�rio anterior.
     */
    private void configuraCache() {
        kpi.swing.framework.KpiListaMetaFormula parentFrame = (kpi.swing.framework.KpiListaMetaFormula) kpi.core.KpiStaticReferences.getKpiFormController().getParentForm(this.getParentType(), "0");
        vctScoreCard = parentFrame.getVctScorecard().clone2();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoOrientacao = new ButtonGroup();
        tapCadastro = new JTabbedPane();
        jScrollPane2 = new JScrollPane();
        pnlMetaFormula = new JPanel();
        pnlFormula = new JPanel();
        scrFormula = new JScrollPane();
        edtFormula = new JEditorPane();
        pnlRightForm1 = new JPanel();
        pnlLeftForm1 = new JPanel();
        pnlBottomForm1 = new JPanel();
        pnlDados = new JPanel();
        lblNome = new JLabel();
        fldNome = new JPVEdit();
        lblDescricao = new JLabel();
        scrDescricao = new JScrollPane();
        txtDescricao = new JBITextArea();
        fldExpFormula = new JPVEdit();
        lblExpressao = new JLabel();
        cbbIndicadores = new JComboBox();
        lblFormIndicador = new JLabel();
        cbbScoreFormula = new JComboBox();
        lblScoreCard = new JLabel();
        btnAddIndicador = new JButton();
        btnAddExpressao = new JButton();
        tsArvore = new JBITreeSelection();
        jBarFormula = new JToolBar();
        btnVerificarFormula = new JButton();
        btnLimparFormula = new JButton();
        jSeparator1 = new JSeparator();
        jSeparator3 = new JSeparator();
        jSeparator4 = new JSeparator();
        jPanel1 = new JPanel();
        btnAdicionar = new JButton();
        btnMultiplicar = new JButton();
        btnRemover = new JButton();
        btnDividir = new JButton();
        btnAbreParentese = new JButton();
        btnFechaParentese = new JButton();
        jSeparator2 = new JSeparator();
        btnVoltar = new JButton();
        pnlTopForm = new JPanel();
        pnlBottomForm = new JPanel();
        pnlRightForm = new JPanel();
        pnlLeftForm = new JPanel();
        pnlTools = new JPanel();
        pnlConfirmation = new JPanel();
        tbConfirmation = new JToolBar();
        btnSave = new JButton();
        btnCancel = new JButton();
        pnlOperation = new JPanel();
        tbInsertion = new JToolBar();
        btnEdit = new JButton();
        btnDelete = new JButton();
        btnReload = new JButton();
        btnAjuda2 = new JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiMetaFormula_00001")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_meta_formula.gif"))); // NOI18N
        setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(519, 479));

        tapCadastro.setPreferredSize(new Dimension(480, 280));

        jScrollPane2.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane2.setPreferredSize(new Dimension(482, 450));

        pnlMetaFormula.setMaximumSize(new Dimension(10, 10));
        pnlMetaFormula.setPreferredSize(new Dimension(450, 370));
        pnlMetaFormula.setLayout(new BorderLayout());

        pnlFormula.setLayout(new BorderLayout());

        edtFormula.setContentType("text/html");
        edtFormula.setEditable(false);
        scrFormula.setViewportView(edtFormula);

        pnlFormula.add(scrFormula, BorderLayout.CENTER);
        pnlFormula.add(pnlRightForm1, BorderLayout.EAST);
        pnlFormula.add(pnlLeftForm1, BorderLayout.WEST);
        pnlFormula.add(pnlBottomForm1, BorderLayout.SOUTH);

        pnlMetaFormula.add(pnlFormula, BorderLayout.CENTER);

        pnlDados.setPreferredSize(new Dimension(0, 290));
        pnlDados.setLayout(null);

        lblNome.setForeground(new Color(51, 51, 255));
        lblNome.setHorizontalAlignment(SwingConstants.LEFT);
        ResourceBundle bundle1 = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        lblNome.setText(bundle1.getString("KpiBaseFrame_00006")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new Dimension(100, 16));
        lblNome.setMinimumSize(new Dimension(100, 16));
        lblNome.setPreferredSize(new Dimension(100, 16));
        pnlDados.add(lblNome);
        lblNome.setBounds(10, 10, 58, 20);
        pnlDados.add(fldNome);
        fldNome.setBounds(10, 30, 400, 22);

        lblDescricao.setForeground(new Color(51, 51, 255));
        lblDescricao.setHorizontalAlignment(SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("KpiMetaFormula_00002")); // NOI18N
        lblDescricao.setEnabled(false);
        lblDescricao.setMaximumSize(new Dimension(100, 16));
        lblDescricao.setMinimumSize(new Dimension(100, 16));
        lblDescricao.setPreferredSize(new Dimension(100, 16));
        pnlDados.add(lblDescricao);
        lblDescricao.setBounds(10, 50, 58, 20);

        scrDescricao.setMinimumSize(new Dimension(90, 40));
        scrDescricao.setPreferredSize(new Dimension(90, 40));

        txtDescricao.setRows(2);
        txtDescricao.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        scrDescricao.setViewportView(txtDescricao);

        pnlDados.add(scrDescricao);
        scrDescricao.setBounds(10, 70, 400, 60);
        pnlDados.add(fldExpFormula);
        fldExpFormula.setBounds(10, 230, 400, 22);

        lblExpressao.setText(bundle.getString("KpiMetaFormula_00005")); // NOI18N
        pnlDados.add(lblExpressao);
        lblExpressao.setBounds(10, 210, 100, 20);

        cbbIndicadores.setEnabled(false);
        pnlDados.add(cbbIndicadores);
        cbbIndicadores.setBounds(10, 190, 400, 22);

        lblFormIndicador.setHorizontalAlignment(SwingConstants.LEFT);
        lblFormIndicador.setText(bundle.getString("KpiMetaFormula_00008")); // NOI18N
        lblFormIndicador.setEnabled(false);
        lblFormIndicador.setMaximumSize(new Dimension(100, 16));
        lblFormIndicador.setMinimumSize(new Dimension(100, 16));
        lblFormIndicador.setPreferredSize(new Dimension(100, 16));
        pnlDados.add(lblFormIndicador);
        lblFormIndicador.setBounds(10, 170, 250, 20);

        cbbScoreFormula.setEnabled(false);
        cbbScoreFormula.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                cbbScoreFormulaItemStateChanged(evt);
            }
        });
        pnlDados.add(cbbScoreFormula);
        cbbScoreFormula.setBounds(10, 150, 400, 22);

        lblScoreCard.setHorizontalAlignment(SwingConstants.LEFT);
        lblScoreCard.setText(KpiStaticReferences.getKpiCustomLabels().getSco(KpiStaticReferences.CAD_OBJETIVO).concat(":"));
        lblScoreCard.setEnabled(false);
        lblScoreCard.setMaximumSize(new Dimension(100, 16));
        lblScoreCard.setMinimumSize(new Dimension(100, 16));
        lblScoreCard.setPreferredSize(new Dimension(100, 16));
        pnlDados.add(lblScoreCard);
        lblScoreCard.setBounds(10, 130, 260, 20);

        btnAddIndicador.setText("+");
        btnAddIndicador.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnAddIndicador.setEnabled(false);
        btnAddIndicador.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAddIndicadorActionPerformed(evt);
            }
        });
        pnlDados.add(btnAddIndicador);
        btnAddIndicador.setBounds(420, 190, 25, 22);

        btnAddExpressao.setFont(new Font("Tahoma", 1, 11));
        btnAddExpressao.setText("+");
        btnAddExpressao.setToolTipText("KEY KpiMetaFormula_00006 : RB international");
        btnAddExpressao.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnAddExpressao.setPreferredSize(new Dimension(30, 15));
        btnAddExpressao.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAddExpressaoActionPerformed(evt);
            }
        });
        pnlDados.add(btnAddExpressao);
        btnAddExpressao.setBounds(420, 230, 25, 22);

        tsArvore.setCombo(cbbScoreFormula);
        tsArvore.setRoot(false);
        pnlDados.add(tsArvore);
        tsArvore.setBounds(410, 150, 30, 22);

        jBarFormula.setFloatable(false);
        jBarFormula.setEnabled(false);
        jBarFormula.setPreferredSize(new Dimension(100, 102));

        btnVerificarFormula.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_verificar.gif"))); // NOI18N
        btnVerificarFormula.setText(bundle.getString("KpiMetaFormula_00003")); // NOI18N
        btnVerificarFormula.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnVerificarFormulaActionPerformed(evt);
            }
        });
        jBarFormula.add(btnVerificarFormula);

        btnLimparFormula.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_limpar.gif"))); // NOI18N
        btnLimparFormula.setText(bundle.getString("KpiMetaFormula_00004")); // NOI18N
        btnLimparFormula.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnLimparFormulaActionPerformed(evt);
            }
        });
        jBarFormula.add(btnLimparFormula);

        jSeparator1.setOrientation(SwingConstants.VERTICAL);
        jSeparator1.setMaximumSize(new Dimension(10, 10));
        jSeparator1.setPreferredSize(new Dimension(10, 0));
        jBarFormula.add(jSeparator1);

        jSeparator3.setOrientation(SwingConstants.VERTICAL);
        jSeparator3.setMaximumSize(new Dimension(0, 0));
        jSeparator3.setPreferredSize(new Dimension(10, 0));
        jBarFormula.add(jSeparator3);

        jSeparator4.setOrientation(SwingConstants.VERTICAL);
        jSeparator4.setMaximumSize(new Dimension(0, 0));
        jSeparator4.setPreferredSize(new Dimension(5, 0));
        jBarFormula.add(jSeparator4);
        jBarFormula.add(jPanel1);

        btnAdicionar.setFont(new Font("Tahoma", 1, 11));
        btnAdicionar.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_adicao.gif"))); // NOI18N
        btnAdicionar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAdicionarActionPerformed(evt);
            }
        });
        jBarFormula.add(btnAdicionar);

        btnMultiplicar.setFont(new Font("Tahoma", 1, 11));
        btnMultiplicar.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_multiplicacao.gif"))); // NOI18N
        btnMultiplicar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnMultiplicarActionPerformed(evt);
            }
        });
        jBarFormula.add(btnMultiplicar);

        btnRemover.setFont(new Font("Tahoma", 1, 11));
        btnRemover.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_subtracao.gif"))); // NOI18N
        btnRemover.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnRemoverActionPerformed(evt);
            }
        });
        jBarFormula.add(btnRemover);

        btnDividir.setFont(new Font("Tahoma", 1, 11));
        btnDividir.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_divisao.gif"))); // NOI18N
        btnDividir.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnDividirActionPerformed(evt);
            }
        });
        jBarFormula.add(btnDividir);

        btnAbreParentese.setFont(new Font("Tahoma", 1, 11));
        btnAbreParentese.setText("(");
        btnAbreParentese.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAbreParenteseActionPerformed(evt);
            }
        });
        jBarFormula.add(btnAbreParentese);

        btnFechaParentese.setFont(new Font("Tahoma", 1, 11));
        btnFechaParentese.setText(")");
        btnFechaParentese.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnFechaParenteseActionPerformed(evt);
            }
        });
        jBarFormula.add(btnFechaParentese);

        jSeparator2.setOrientation(SwingConstants.VERTICAL);
        jSeparator2.setMaximumSize(new Dimension(10, 10));
        jSeparator2.setPreferredSize(new Dimension(10, 0));
        jBarFormula.add(jSeparator2);

        btnVoltar.setFont(new Font("Tahoma", 1, 11));
        btnVoltar.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_voltar.gif"))); // NOI18N
        btnVoltar.setActionCommand("<<");
        btnVoltar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnVoltarActionPerformed(evt);
            }
        });
        jBarFormula.add(btnVoltar);

        pnlDados.add(jBarFormula);
        jBarFormula.setBounds(10, 260, 420, 22);

        pnlMetaFormula.add(pnlDados, BorderLayout.PAGE_START);

        jScrollPane2.setViewportView(pnlMetaFormula);

        tapCadastro.addTab(bundle1.getString("KpiMetaFormula_00010"), jScrollPane2); // NOI18N

        getContentPane().add(tapCadastro, BorderLayout.CENTER);
        getContentPane().add(pnlTopForm, BorderLayout.NORTH);
        getContentPane().add(pnlBottomForm, BorderLayout.SOUTH);
        getContentPane().add(pnlRightForm, BorderLayout.EAST);
        getContentPane().add(pnlLeftForm, BorderLayout.WEST);

        pnlTools.setMinimumSize(new Dimension(480, 27));
        pnlTools.setPreferredSize(new Dimension(10, 29));
        pnlTools.setLayout(new BorderLayout());

        pnlConfirmation.setMaximumSize(new Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new Dimension(155, 27));
        pnlConfirmation.setLayout(new BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new Dimension(150, 32));

        btnSave.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle1.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle1.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, BorderLayout.EAST);

        pnlOperation.setMinimumSize(new Dimension(300, 19));
        pnlOperation.setPreferredSize(new Dimension(380, 27));
        pnlOperation.setLayout(new BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setMaximumSize(new Dimension(300, 19));
        tbInsertion.setPreferredSize(new Dimension(300, 32));

        btnEdit.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle1.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle1.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle1.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnAjuda2.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda2.setText(bundle.getString("KpiGraphIndicador_00011")); // NOI18N
        btnAjuda2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAjuda2ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda2);

        pnlOperation.add(tbInsertion, BorderLayout.CENTER);

        pnlTools.add(pnlOperation, BorderLayout.WEST);

        getContentPane().add(pnlTools, BorderLayout.NORTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnAjuda2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjuda2ActionPerformed
            event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjuda2ActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            btnSave.setEnabled(false);
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

        private void cbbScoreFormulaItemStateChanged(ItemEvent evt) {//GEN-FIRST:event_cbbScoreFormulaItemStateChanged
            if (evt.getStateChange() == ItemEvent.SELECTED && cbbScoreFormula.getItemCount() > 0) {
                StringBuffer selectInd = new StringBuffer("ID_SCOREC = '");
                selectInd.append(event.getComboValue(htbScoreCard, cbbScoreFormula));
                selectInd.append("'");
                kpi.xml.BIXMLRecord recIndFiltrados = event.listRecords("INDICADOR", selectInd.toString());
                event.populateCombo(recIndFiltrados.getBIXMLVector("INDICADORES"), "INDICADOR", htbIndicadores, cbbIndicadores);
            }
}//GEN-LAST:event_cbbScoreFormulaItemStateChanged

        private void btnVoltarActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnVoltarActionPerformed
            if (vctFormula.size() > 0) {
                vctFormula.removeElementAt(vctFormula.size() - 1);
                refreshFormulaText();
            }
}//GEN-LAST:event_btnVoltarActionPerformed

        private void btnFechaParenteseActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnFechaParenteseActionPerformed
            KpiItemFormula formula = new KpiItemFormula(")", ")");
            addFormula(formula);
}//GEN-LAST:event_btnFechaParenteseActionPerformed

        private void btnAbreParenteseActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnAbreParenteseActionPerformed
            KpiItemFormula formula = new KpiItemFormula("(", "(");
            addFormula(formula);
}//GEN-LAST:event_btnAbreParenteseActionPerformed

        private void btnDividirActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnDividirActionPerformed
            KpiItemFormula formula = new KpiItemFormula("/", "/");
            addFormula(formula);
}//GEN-LAST:event_btnDividirActionPerformed

        private void btnRemoverActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnRemoverActionPerformed
            KpiItemFormula formula = new KpiItemFormula("-", "-");
            addFormula(formula);
}//GEN-LAST:event_btnRemoverActionPerformed

        private void btnMultiplicarActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnMultiplicarActionPerformed
            KpiItemFormula formula = new KpiItemFormula("*", "*");
            addFormula(formula);
}//GEN-LAST:event_btnMultiplicarActionPerformed

        private void btnAdicionarActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnAdicionarActionPerformed
            KpiItemFormula formula = new KpiItemFormula("+", "+");
            addFormula(formula);
}//GEN-LAST:event_btnAdicionarActionPerformed

        private void btnLimparFormulaActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnLimparFormulaActionPerformed
            limpaFormula();
}//GEN-LAST:event_btnLimparFormulaActionPerformed

        private void btnVerificarFormulaActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnVerificarFormulaActionPerformed
            //Faz a valida��o da f�rmula.
            event.executeRecord(formulaToString());
}//GEN-LAST:event_btnVerificarFormulaActionPerformed

        private void btnAddExpressaoActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnAddExpressaoActionPerformed
            if (fldExpFormula != null) {
                String expressao = fldExpFormula.getText();
                if (expressao != null && !(expressao.trim().equals(""))) {
                    if (expressao.contains("-") || expressao.contains("+") || expressao.contains("/") || expressao.contains("*")) {
                        dialog.errorMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMetaFormula_00015"));
                    } else {
                        KpiItemFormula formula = new KpiItemFormula(fldExpFormula.getText(), fldExpFormula.getText());
                        addFormula(formula);
                        fldExpFormula.setText("");
                    }
                }
            }
}//GEN-LAST:event_btnAddExpressaoActionPerformed

        private void btnAddIndicadorActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnAddIndicadorActionPerformed
            if (cbbIndicadores.getItemCount() > 1) {
                kpi.util.KpiComboString descSelected = (kpi.util.KpiComboString) cbbIndicadores.getSelectedItem();
                String descricao = descSelected.toString();
                String expressao = event.getComboValue(htbIndicadores, cbbIndicadores);
                KpiItemFormula formula = new KpiItemFormula(expressao, descricao);
                formula.setITipo(KpiItemFormula.INDICADOR);
                formula.setIdScoreCard(event.getComboValue(htbScoreCard, cbbScoreFormula));
                addFormula(formula);
            }
}//GEN-LAST:event_btnAddIndicadorActionPerformed

    private void addFormula(KpiItemFormula expFormula) {
        vctFormula.add(expFormula);
        refreshFormulaText();
        fldExpFormula.requestFocus();
    }

    /**
     * Monta a string da formula a partir de vetor vctFormula;
     **/
    private String formulaToString() {
        KpiItemFormula iteFormula;
        StringBuffer txtRetorno = new StringBuffer();

        for (java.util.Iterator itemForm = vctFormula.iterator(); itemForm.hasNext();) {
            iteFormula = (KpiItemFormula) itemForm.next();
            switch (iteFormula.getITipo()) {
                case 2:	//INDICADOR	= 2;
                    txtRetorno.append("|I.".concat(iteFormula.getStrExpressao()));
                    break;
                default:
                    txtRetorno.append("|".concat(iteFormula.getStrExpressao()));
                    break;
            }
        }

        return txtRetorno.toString();
    }

    /**
     * Monta o vetor vctFormula a partir de uma string passada.
     */
    private void stringToFormula(String strFormula) {
        StringTokenizer tokFormula = new StringTokenizer(strFormula, "|");
        String idFormula = new String();

        int itemFound = 0,
                itemIndicador = 0;

        kpi.xml.BIXMLVector vctFormula;

        //Verificando os itens da f�rmula.
        limpaFormula();
        while (tokFormula.hasMoreElements()) {
            String sNextToken = tokFormula.nextToken();
            KpiItemFormula formula = new KpiItemFormula();

            if (sNextToken.contains("I.") || sNextToken.contains("M.")) {
                idFormula = sNextToken.substring(2, sNextToken.length());

                //ATEN��O: Quando a f�rmula contiver indicadores "I.", o objeto de item da formula
                //deve ser preenchido com os dados que est�o no XML LISTAFORMULA.
                if (sNextToken.contains("I.")) {
                    vctFormula = record.getBIXMLVector("LISTAFORMULA");
                    kpi.xml.BIXMLRecord xmlFormula = vctFormula.get(itemIndicador);
                    formula.setITipo(KpiItemFormula.INDICADOR);
                    formula.setStrDescricao(xmlFormula.getString("INDICA_NAME"));
                    formula.setStrExpressao(xmlFormula.getString("INDICA_ID"));
                    formula.setIdScoreCard(xmlFormula.getString("SCORE_ID"));
                    itemIndicador++;
                }
            } else {
                formula.setStrDescricao(sNextToken);
                formula.setStrExpressao(sNextToken);
            }

            //Adicionando o Item da f�rmula ao vetor.
            addFormula(formula);
        }

        refreshFormulaText();
    }

    /*
     *Mostra o texto da formula adicionada.
     */
    private void refreshFormulaText() {
                StringBuilder html = new StringBuilder();

        for (java.util.Iterator iter = vctFormula.iterator(); iter.hasNext();) {
            KpiItemFormula formula = (KpiItemFormula) iter.next();

            if (formula.getITipo() == KpiItemFormula.INDICADOR) {
                String scoreName = new String("");

                int retItem = event.getComboItem(vctScoreCard, formula.getIdScoreCard(), false);
                if (retItem != -1 && vctScoreCard.size() > 0) {
                    scoreName = vctScoreCard.get(retItem).getString("NOME");
                }

                html.append(scoreName);
                html.append("->");
                html.append("<font color='#27408B'>");
                html.append(formula.getStrDescricao());
                html.append("</font>");
            } else {
                html.append("<font color='red'><b>");
                html.append(" ");
                html.append(formula.getStrDescricao());
                html.append(" ");
                html.append("</b></font>");
            }
        }

        edtFormula.setText(html.toString());
    }

    private void limpaFormula() {
        vctFormula.removeAllElements();
        edtFormula.setText("");
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnAbreParentese;
    private JButton btnAddExpressao;
    private JButton btnAddIndicador;
    private JButton btnAdicionar;
    private JButton btnAjuda2;
    private JButton btnCancel;
    private JButton btnDelete;
    private JButton btnDividir;
    private JButton btnEdit;
    private JButton btnFechaParentese;
    private JButton btnLimparFormula;
    private JButton btnMultiplicar;
    private JButton btnReload;
    private JButton btnRemover;
    private JButton btnSave;
    private JButton btnVerificarFormula;
    private JButton btnVoltar;
    private JComboBox cbbIndicadores;
    private JComboBox cbbScoreFormula;
    private JEditorPane edtFormula;
    private JPVEdit fldExpFormula;
    private JPVEdit fldNome;
    private ButtonGroup grupoOrientacao;
    private JToolBar jBarFormula;
    private JPanel jPanel1;
    private JScrollPane jScrollPane2;
    private JSeparator jSeparator1;
    private JSeparator jSeparator2;
    private JSeparator jSeparator3;
    private JSeparator jSeparator4;
    private JLabel lblDescricao;
    private JLabel lblExpressao;
    private JLabel lblFormIndicador;
    private JLabel lblNome;
    private JLabel lblScoreCard;
    private JPanel pnlBottomForm;
    private JPanel pnlBottomForm1;
    private JPanel pnlConfirmation;
    private JPanel pnlDados;
    private JPanel pnlFormula;
    private JPanel pnlLeftForm;
    private JPanel pnlLeftForm1;
    private JPanel pnlMetaFormula;
    private JPanel pnlOperation;
    private JPanel pnlRightForm;
    private JPanel pnlRightForm1;
    private JPanel pnlTools;
    private JPanel pnlTopForm;
    private JScrollPane scrDescricao;
    private JScrollPane scrFormula;
    private JTabbedPane tapCadastro;
    private JToolBar tbConfirmation;
    private JToolBar tbInsertion;
    private JBITreeSelection tsArvore;
    private JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        btnSave.setEnabled(true);
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public String getParentType() {
        return parentType;
    }
}
