package kpi.swing.framework;

/**
 * Created on 21/09/2010
 * @autor Valdiney V GOMES
 */
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.xml.BIXMLRecord;
import pv.jfcx.JPVDatePlus;

public class KpiNota extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "NOTA";
    private final String parentType = "LSTNOTA";
    private final String formType = recordType;
    private final KpiDefaultFrameBehavior event = new KpiDefaultFrameBehavior(this);
    private int status = KpiDefaultFrameBehavior.NORMAL;
    private String id;
    private final String indicador;
    private BIXMLRecord record;

    public KpiNota(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        indicador = contextId;
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
        event.setEnableFields(pnlDados, true);
        event.setEnableFields(pnlBloqueado, false);
        txtNota.setEditable(true);
    }

    @Override
    public void disableFields() {
        event.setEnableFields(pnlDados, false);
        event.setEnableFields(pnlBloqueado, false);
        txtNota.setEditable(false);
    }

    @Override
    public void refreshFields() {
        txtAutor.setText(record.getString("AUTOR"));

        if (getStatus() == KpiDefaultFrameBehavior.INSERTING) {
            txtData.setCurrentDate(true);
        } else {
            txtTitulo.setText(record.getString("NOME"));
            txtNota.setText(record.getString("NOTA"));
            txtData.setCalendar(record.getDate("PUBLICA"));
            event.setEnableFields(pnlManutencao, record.getBoolean("PERMISSAO"));
        }
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        final BIXMLRecord dados = new BIXMLRecord(recordType);

        dados.set("ID", id);
        dados.set("NOME", txtTitulo.getText());
        dados.set("NOTA", txtNota.getText());
        dados.set("PUBLICA", txtData.getCalendar());

        if (getStatus() == KpiDefaultFrameBehavior.INSERTING) {
            dados.set("ID_INDICA", indicador);
        }

        return dados;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean validacao = true;

        if (txtTitulo.getText().isEmpty()) {
            errorMessage.append("O campo Assunto deve ser preenchido.\n");
            validacao = false;
        }

        if (txtNota.getText().isEmpty()) {
            errorMessage.append("O campo Nota deve ser preenchido.\n");
            validacao = false;
        }

        if (!validacao) {
            btnSave.setEnabled(true);
        }

        return validacao;
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        tapCadastro = new JTabbedPane();
        scpNota = new JScrollPane();
        pnlNota = new JPanel();
        jLabel4 = new JLabel();
        pnlDados = new JPanel();
        txtTitulo = new JTextField();
        jLabel5 = new JLabel();
        jLabel1 = new JLabel();
        pnlBloqueado = new JPanel();
        jLabel2 = new JLabel();
        jLabel3 = new JLabel();
        txtAutor = new JTextField();
        scrInicio = new JScrollPane();
        txtData = new JPVDatePlus();
        pnlTexto = new JPanel();
        scrNota = new JScrollPane();
        txtNota = new JTextArea();
        pnlEsquerda1 = new JPanel();
        pnlDireita1 = new JPanel();
        pnlInferior1 = new JPanel();
        pnlInferior = new JPanel();
        pnlDireita = new JPanel();
        pnlEsquerda = new JPanel();
        pnlManutencao = new JPanel();
        pnlConfirmation = new JPanel();
        tbConfirmation = new JToolBar();
        btnSave = new JButton();
        btnCancel = new JButton();
        pnlOperation = new JPanel();
        tbInsertion = new JToolBar();
        btnEdit = new JButton();
        btnDelete = new JButton();
        btnReload = new JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Nota");
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_lembrete.gif"))); // NOI18N
        setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(480, 500));

        scpNota.setBorder(null);

        pnlNota.setLayout(new BorderLayout());

        jLabel4.setText("Nota:");
        pnlNota.add(jLabel4, BorderLayout.CENTER);

        pnlDados.setPreferredSize(new Dimension(0, 70));
        pnlDados.setLayout(null);
        pnlDados.add(txtTitulo);
        txtTitulo.setBounds(10, 30, 400, 22);

        jLabel5.setText("Assunto:");
        pnlDados.add(jLabel5);
        jLabel5.setBounds(10, 10, 50, 20);

        jLabel1.setText("Texto:");
        pnlDados.add(jLabel1);
        jLabel1.setBounds(10, 50, 50, 20);

        pnlBloqueado.setMaximumSize(new Dimension(50, 50));
        pnlBloqueado.setMinimumSize(new Dimension(50, 50));
        pnlBloqueado.setPreferredSize(new Dimension(50, 50));
        pnlBloqueado.setLayout(null);

        jLabel2.setText("Publicação:");
        pnlBloqueado.add(jLabel2);
        jLabel2.setBounds(10, 0, 130, 20);

        jLabel3.setText("Autor:");
        pnlBloqueado.add(jLabel3);
        jLabel3.setBounds(230, 0, 60, 20);
        pnlBloqueado.add(txtAutor);
        txtAutor.setBounds(230, 20, 180, 22);

        txtData.setBorderStyle(0);
        txtData.setButtonBorder(0);
        txtData.setCurrentDate(true);
        txtData.setDialog(true);
        txtData.setUseLocale(true);
        txtData.setWaitForCalendarDate(true);
        scrInicio.setViewportView(txtData);

        pnlBloqueado.add(scrInicio);
        scrInicio.setBounds(10, 20, 180, 22);

        pnlDados.add(pnlBloqueado);
        pnlBloqueado.setBounds(440, 10, 420, 50);

        pnlNota.add(pnlDados, BorderLayout.PAGE_START);

        pnlTexto.setLayout(new BorderLayout());

        txtNota.setColumns(31);
        txtNota.setFont(new Font("Tahoma", 0, 11)); // NOI18N
        txtNota.setLineWrap(true);
        txtNota.setRows(5);
        txtNota.setTabSize(4);
        txtNota.setWrapStyleWord(true);
        scrNota.setViewportView(txtNota);

        pnlTexto.add(scrNota, BorderLayout.CENTER);
        pnlTexto.add(pnlEsquerda1, BorderLayout.WEST);
        pnlTexto.add(pnlDireita1, BorderLayout.EAST);
        pnlTexto.add(pnlInferior1, BorderLayout.SOUTH);

        pnlNota.add(pnlTexto, BorderLayout.CENTER);

        scpNota.setViewportView(pnlNota);

        tapCadastro.addTab("Nota", scpNota);

        getContentPane().add(tapCadastro, BorderLayout.CENTER);
        getContentPane().add(pnlInferior, BorderLayout.SOUTH);
        getContentPane().add(pnlDireita, BorderLayout.EAST);
        getContentPane().add(pnlEsquerda, BorderLayout.WEST);

        pnlManutencao.setMinimumSize(new Dimension(480, 27));
        pnlManutencao.setPreferredSize(new Dimension(10, 29));
        pnlManutencao.setLayout(new BorderLayout());

        pnlConfirmation.setMaximumSize(new Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new Dimension(155, 27));
        pnlConfirmation.setLayout(new BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new Dimension(150, 32));

        btnSave.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        ResourceBundle bundle = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnSave.setText(bundle.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, BorderLayout.CENTER);

        pnlManutencao.add(pnlConfirmation, BorderLayout.EAST);

        pnlOperation.setMaximumSize(new Dimension(340, 27));
        pnlOperation.setMinimumSize(new Dimension(340, 27));
        pnlOperation.setPreferredSize(new Dimension(340, 27));
        pnlOperation.setLayout(new BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new Dimension(250, 32));

        btnEdit.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        pnlOperation.add(tbInsertion, BorderLayout.CENTER);

        pnlManutencao.add(pnlOperation, BorderLayout.WEST);

        getContentPane().add(pnlManutencao, BorderLayout.NORTH);

        pack();
    }//GEN-END:initComponents

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnCancel;
    private JButton btnDelete;
    private JButton btnEdit;
    private JButton btnReload;
    private JButton btnSave;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JPanel pnlBloqueado;
    private JPanel pnlConfirmation;
    private JPanel pnlDados;
    private JPanel pnlDireita;
    private JPanel pnlDireita1;
    private JPanel pnlEsquerda;
    private JPanel pnlEsquerda1;
    private JPanel pnlInferior;
    private JPanel pnlInferior1;
    private JPanel pnlManutencao;
    private JPanel pnlNota;
    private JPanel pnlOperation;
    private JPanel pnlTexto;
    private JScrollPane scpNota;
    private JScrollPane scrInicio;
    private JScrollPane scrNota;
    private JTabbedPane tapCadastro;
    private JToolBar tbConfirmation;
    private JToolBar tbInsertion;
    private JTextField txtAutor;
    private JPVDatePlus txtData;
    private JTextArea txtNota;
    private JTextField txtTitulo;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public String getParentType() {
        return parentType;
    }
}
