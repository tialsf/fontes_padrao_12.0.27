/*
 * KpiOrdemScorecard.java
 * Created on 03/09/2010, 22:07:48
 */
package kpi.swing.framework;

import javax.swing.JInternalFrame;
import javax.swing.JTabbedPane;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiDefaultFrameFunctions;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

/**
 * @author Valdiney V GOMES
 */
public class KpiOrdemScorecard extends javax.swing.JInternalFrame implements KpiDefaultFrameFunctions {

    //Vari�veis.
    private final String formType = "SCORECARD";
    private final String parentType = "SCORECARD";
    private String recordType = "ORDEM_SCORECARD";
    private String id;
    private final KpiDefaultFrameBehavior event = new KpiDefaultFrameBehavior(this);
    private int status = KpiDefaultFrameBehavior.NORMAL;
    private BIXMLRecord record = null;
    //Constantes.
    private final int ORDEM = 00;
    private final int NOME = 01;
    private final int ID = 02;

    /**
     * M�todo Construtor.
     * @param operation
     * @param idAux
     * @param contextId
     */
    public KpiOrdemScorecard(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);

        event.defaultConstructor(operation, idAux, contextId);

        setID(idAux);
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        pnlFerramentas = new javax.swing.JPanel();
        pnlConfirmacao = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperacao = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        tbpOrdenacao = new javax.swing.JTabbedPane();
        pnlOrdenacao = new javax.swing.JPanel();
        tbOrdem = new kpi.beans.JBIXMLTable();
        pnlSeta = new javax.swing.JPanel();
        tlbSetas = new javax.swing.JToolBar();
        btnSobe = new javax.swing.JButton();
        btnDesce = new javax.swing.JButton();
        pnlLeftForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Ordem dos Scorecards");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_scorecard.gif"))); // NOI18N

        pnlFerramentas.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlFerramentas.setPreferredSize(new java.awt.Dimension(10, 29));
        pnlFerramentas.setLayout(new java.awt.BorderLayout());

        pnlConfirmacao.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmacao.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmacao.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmacao.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnSave.setText(bundle.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmacao.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlFerramentas.add(pnlConfirmacao, java.awt.BorderLayout.EAST);

        pnlOperacao.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperacao.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperacao.setPreferredSize(new java.awt.Dimension(235, 27));
        pnlOperacao.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        pnlOperacao.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlFerramentas.add(pnlOperacao, java.awt.BorderLayout.WEST);

        getContentPane().add(pnlFerramentas, java.awt.BorderLayout.NORTH);

        pnlOrdenacao.setLayout(new java.awt.BorderLayout());
        pnlOrdenacao.add(tbOrdem, java.awt.BorderLayout.CENTER);

        pnlSeta.setPreferredSize(new java.awt.Dimension(30, 0));
        pnlSeta.setLayout(new java.awt.BorderLayout());

        tlbSetas.setFloatable(false);
        tlbSetas.setOrientation(1);
        tlbSetas.setPreferredSize(new java.awt.Dimension(20, 20));

        btnSobe.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_move_para_cima.gif"))); // NOI18N
        btnSobe.setFocusable(false);
        btnSobe.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSobe.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSobe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSobeActionPerformed(evt);
            }
        });
        tlbSetas.add(btnSobe);

        btnDesce.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_move_para_baixo.gif"))); // NOI18N
        btnDesce.setFocusable(false);
        btnDesce.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnDesce.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnDesce.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesceActionPerformed(evt);
            }
        });
        tlbSetas.add(btnDesce);

        pnlSeta.add(tlbSetas, java.awt.BorderLayout.CENTER);

        pnlOrdenacao.add(pnlSeta, java.awt.BorderLayout.LINE_END);

        tbpOrdenacao.addTab("Ordena��o", pnlOrdenacao);

        getContentPane().add(tbpOrdenacao, java.awt.BorderLayout.CENTER);
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pack();
    }//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        event.saveRecord();
}//GEN-LAST:event_btnSaveActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        event.cancelOperation();
}//GEN-LAST:event_btnCancelActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        event.editRecord();
}//GEN-LAST:event_btnEditActionPerformed

    private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
        event.loadRecord();
}//GEN-LAST:event_btnReloadActionPerformed

    private void btnSobeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSobeActionPerformed
        final int linha = tbOrdem.getTable().getSelectedRow();

        if ((linha == -1) || (linha == 0)) {
            return;
        }

        tbOrdem.moveRow(linha, linha, linha - 1);
        tbOrdem.getTable().setRowSelectionInterval(linha - 1, linha - 1);

        reordena();
    }//GEN-LAST:event_btnSobeActionPerformed

    private void btnDesceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesceActionPerformed
        final int linha = tbOrdem.getTable().getSelectedRow();

        if ((linha == -1) || (linha == tbOrdem.getTable().getRowCount() - 1)) {
            return;
        }

        tbOrdem.moveRow(linha, linha, linha + 1);
        tbOrdem.getTable().setRowSelectionInterval(linha + 1, linha + 1);

        reordena();
    }//GEN-LAST:event_btnDesceActionPerformed

    /**
     * Controla o conte�do da c�lula ordem da tabela.
     */
    private void reordena() {
        final int linha = tbOrdem.getTable().getRowCount();

        for (int i = 1; i <= linha; i++) {
            tbOrdem.setValueAt(i, i - 1, ORDEM);
        }
    }

    /**
     * Aplica as defini��es de estilo as c�lulas da tabela. 
     */
    private void formataLista() {
        tbOrdem.getColumn(NOME).setPreferredWidth(400);
        tbOrdem.setSortColumn(ORDEM);
        tbOrdem.getTable().setSelectionMode(0);
        tbOrdem.getTable().setEnableSortData(false);
    }

    @Override
    public void enableFields() {
        event.setEnableFields(pnlOrdenacao, true);
    }

    @Override
    public void disableFields() {
        event.setEnableFields(pnlOrdenacao, false);
    }

    @Override
    public void refreshFields() {
        final BIXMLRecord lista = event.listRecords("ORDEM_SCORECARD", this.getID());
        final BIXMLVector scorecards = lista.getBIXMLVector("ORDEMSCORECARDS");

        tbOrdem.setDataSource(scorecards);

        formataLista();
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    @Override
    public BIXMLRecord getFieldsContents() {
        final StringBuilder ordem = new StringBuilder();
        final BIXMLRecord conteudo = new BIXMLRecord(recordType);

        conteudo.set("ID", getID());

        for (int i = 0; i < tbOrdem.getModel().getRowCount(); i++) {

            //Monta o conjunto ORDEM,ID para cada scorecard.
            ordem.append(tbOrdem.getTable().getModel().getValueAt(i, ORDEM));
            ordem.append(",");
            ordem.append(tbOrdem.getTable().getModel().getValueAt(i, ID));

            //Monta a string contatenado todos os conjuntos ORDEM,ID com |.
            if (i < tbOrdem.getModel().getRowCount() - 1) {
                ordem.append("|");
            }
        }

        conteudo.set("ORDEM", ordem.toString());

        return conteudo;
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmacao.setVisible(false);
        pnlOperacao.setVisible(true);
    }

    @Override
    public void showDataButtons() {
        pnlConfirmacao.setVisible(true);
        pnlOperacao.setVisible(false);
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public final void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return recordType;
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public JTabbedPane getTabbedPane() {
        return null;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDesce;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSobe;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlConfirmacao;
    private javax.swing.JPanel pnlFerramentas;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlOperacao;
    private javax.swing.JPanel pnlOrdenacao;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlSeta;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private kpi.beans.JBIXMLTable tbOrdem;
    private javax.swing.JTabbedPane tbpOrdenacao;
    private javax.swing.JToolBar tlbSetas;
    // End of variables declaration//GEN-END:variables
}
