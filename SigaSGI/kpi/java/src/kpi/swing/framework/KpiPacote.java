package kpi.swing.framework;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.ResourceBundle;
import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import kpi.beans.JBISelectionPanel;
import kpi.beans.JBITextArea;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiSelectedTree;
import kpi.swing.KpiSelectedTreeCellRender;
import kpi.swing.KpiSelectedTreeNode;
import kpi.xml.BIXMLVector;
import pv.jfcx.JPVEdit;

public class KpiPacote extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String recordType = "PACOTE";
    private String formType = recordType;//Tipo do Formulario
    private String parentType = "LSTPACOTE";//Tipo formulario pai, caso haja.
    private BIXMLVector scorecards;

    @Override
    public void enableFields() {
        // Habilita os campos do formul�rio.
        tapCadastro.setEnabledAt(1, true);
        tapCadastro.setEnabledAt(2, true);
        tapCadastro.setEnabledAt(3, true);
        lblNome.setEnabled(true);
        fldNome.setEnabled(true);
        lblDescricao.setEnabled(true);
        txtDescricao.setEnabled(true);
        jBISelectionGrupoInd.setEnabled(true);
        jBISelectionUsuario.setEnabled(true);
    }

    @Override
    public void disableFields() {
        // Desabilita os campos do formul�rio.
        lblNome.setEnabled(false);
        fldNome.setEnabled(false);
        lblDescricao.setEnabled(false);
        txtDescricao.setEnabled(false);
        jBISelectionGrupoInd.setEnabled(false);
        jBISelectionUsuario.setEnabled(false);
    }

    @Override
    public void refreshFields() {
        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        jBISelectionUsuario.setDataSource(record.getBIXMLVector("USUARIOS"), record.getBIXMLVector("PACOTEXUSERS"), "PACOTEXUSERS", "PACOTEXUSER");
        jBISelectionGrupoInd.setDataSource(record.getBIXMLVector("GRUPO_INDS"), record.getBIXMLVector("PACOTEXGRPINDS"), "PACOTEXGRPINDS", "PACOTEXGRPIND");
        jBISelectionGrupoInd.setTileSelection(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPacote_00004")); //"Grupo de Indicadores"
        
        scorecards = record.getBIXMLVector("SCORECARDS"); //NOI18N

        treeScorecard.setModel(new javax.swing.tree.DefaultTreeModel(KpiSelectedTree.createTree(scorecards)));
        treeScorecard.setCellRenderer(new KpiSelectedTreeCellRender(kpi.core.KpiStaticReferences.getKpiImageResources()));
        treeScorecard.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        KpiSelectedTree.expandAll(treeScorecard, true);
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        // Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
        // como retorno da fun��o.
        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set(jBISelectionUsuario.getXMLData());
        recordAux.set(jBISelectionGrupoInd.getXMLData());

        recordAux.set(getVectorRowsSelected(scorecards, "SELECTED", "SCORECARDS", "SCORECARD")); //NOI18N

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        if (fldNome.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPacote_00008"));
            retorno = false;
        }

        if (!retorno) {
            btnSave.setEnabled(true);
        }
        return retorno;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new JTabbedPane();
        pnlRecord = new JPanel();
        pnlFields = new JPanel();
        pnlBottomRecord = new JPanel();
        pnlRightRecord = new JPanel();
        pnlLeftRecord = new JPanel();
        scpRecord = new JScrollPane();
        pnlData = new JPanel();
        lblNome = new JLabel();
        fldNome = new JPVEdit();
        lblDescricao = new JLabel();
        txtDescricao = new JBITextArea();
        pnlTopRecord = new JPanel();
        pnlGrupoInd = new JPanel();
        jBISelectionGrupoInd = new JBISelectionPanel();
        pnlDpto = new JPanel();
        scrollTreeScorecard = new JScrollPane();
        treeScorecard = new JTree();
        pnlUsuario = new JPanel();
        jBISelectionUsuario = new JBISelectionPanel();
        pnlBottomForm = new JPanel();
        pnlRightForm = new JPanel();
        pnlLeftForm = new JPanel();
        pnlTools = new JPanel();
        pnlConfirmation = new JPanel();
        tbConfirmation = new JToolBar();
        btnSave = new JButton();
        btnCancel = new JButton();
        pnlOperation = new JPanel();
        tbInsertion = new JToolBar();
        btnEdit = new JButton();
        btnDelete = new JButton();
        btnReload = new JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiPacote_00009")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_pacote.gif")));         setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(480, 299));

        pnlRecord.setLayout(new BorderLayout());

        pnlFields.setLayout(new BorderLayout());
        pnlFields.add(pnlBottomRecord, BorderLayout.SOUTH);
        pnlFields.add(pnlRightRecord, BorderLayout.EAST);
        pnlFields.add(pnlLeftRecord, BorderLayout.WEST);

        scpRecord.setBorder(null);

        pnlData.setLayout(null);

        lblNome.setForeground(new Color(51, 51, 255));
        lblNome.setHorizontalAlignment(SwingConstants.LEFT);
        ResourceBundle bundle1 = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        lblNome.setText(bundle1.getString("KpiPacote_00002")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new Dimension(100, 16));
        lblNome.setMinimumSize(new Dimension(100, 16));
        lblNome.setPreferredSize(new Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(10, 10, 58, 20);
        pnlData.add(fldNome);
        fldNome.setBounds(10, 30, 400, 22);

        lblDescricao.setHorizontalAlignment(SwingConstants.LEFT);
        lblDescricao.setText(bundle1.getString("KpiPacote_00003")); // NOI18N
        lblDescricao.setEnabled(false);
        lblDescricao.setMaximumSize(new Dimension(100, 16));
        lblDescricao.setMinimumSize(new Dimension(100, 16));
        lblDescricao.setPreferredSize(new Dimension(100, 16));
        pnlData.add(lblDescricao);
        lblDescricao.setBounds(10, 50, 58, 20);

        txtDescricao.setBorder(BorderFactory.createLineBorder(new Color(153, 153, 153)));
        pnlData.add(txtDescricao);
        txtDescricao.setBounds(10, 70, 400, 110);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, BorderLayout.CENTER);

        pnlTopRecord.setPreferredSize(new Dimension(10, 2));
        pnlFields.add(pnlTopRecord, BorderLayout.NORTH);

        pnlRecord.add(pnlFields, BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("KpiPacote_00001"), pnlRecord); // NOI18N

        pnlGrupoInd.setLayout(new BorderLayout());
        pnlGrupoInd.add(jBISelectionGrupoInd, BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("KpiPacote_00004"), pnlGrupoInd); // NOI18N

        pnlDpto.setLayout(new BorderLayout());

        scrollTreeScorecard.setBorder(null);

        treeScorecard.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        DefaultMutableTreeNode treeNode1 = new DefaultMutableTreeNode("Aguarde...");
        treeScorecard.setModel(new DefaultTreeModel(treeNode1));
        treeScorecard.setRootVisible(false);
        treeScorecard.setRowHeight(18);
        treeScorecard.setShowsRootHandles(true);
        treeScorecard.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                treeScorecardMouseClicked(evt);
            }
        });
        scrollTreeScorecard.setViewportView(treeScorecard);

        pnlDpto.add(scrollTreeScorecard, BorderLayout.CENTER);

        tapCadastro.addTab(KpiStaticReferences.getKpiCustomLabels().getSco(), pnlDpto);

        pnlUsuario.setLayout(new BorderLayout());
        pnlUsuario.add(jBISelectionUsuario, BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("KpiPacote_00006"), pnlUsuario); // NOI18N

        getContentPane().add(tapCadastro, BorderLayout.CENTER);
        getContentPane().add(pnlBottomForm, BorderLayout.SOUTH);
        getContentPane().add(pnlRightForm, BorderLayout.EAST);
        getContentPane().add(pnlLeftForm, BorderLayout.WEST);

        pnlTools.setMinimumSize(new Dimension(480, 27));
        pnlTools.setPreferredSize(new Dimension(10, 29));
        pnlTools.setLayout(new BorderLayout());

        pnlConfirmation.setMaximumSize(new Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new Dimension(155, 27));
        pnlConfirmation.setLayout(new BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new Dimension(150, 32));

        btnSave.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif")));         btnSave.setText(bundle1.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif")));         btnCancel.setText(bundle1.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, BorderLayout.EAST);

        pnlOperation.setMaximumSize(new Dimension(340, 27));
        pnlOperation.setMinimumSize(new Dimension(340, 27));
        pnlOperation.setPreferredSize(new Dimension(235, 27));
        pnlOperation.setLayout(new BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new Dimension(225, 32));

        btnEdit.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif")));         btnEdit.setText(bundle1.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif")));         btnDelete.setText(bundle1.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif")));         btnReload.setText(bundle1.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        pnlOperation.add(tbInsertion, BorderLayout.CENTER);

        pnlTools.add(pnlOperation, BorderLayout.WEST);

        getContentPane().add(pnlTools, BorderLayout.NORTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

    private void treeScorecardMouseClicked(MouseEvent evt) {//GEN-FIRST:event_treeScorecardMouseClicked

        if (status == KpiDefaultFrameBehavior.INSERTING || status == KpiDefaultFrameBehavior.UPDATING) {

            int x = evt.getX();
            int y = evt.getY();
            int somaX = new JCheckBox().getPreferredSize().width;

            int row = treeScorecard.getRowForLocation(x, y);
            TreePath path = treeScorecard.getPathForRow(row);

            if (path != null) {
                if (x <= treeScorecard.getPathBounds(path).x + somaX) {
                    DefaultMutableTreeNode nodeAux = (DefaultMutableTreeNode) path.getLastPathComponent();
                    KpiSelectedTreeNode node = (KpiSelectedTreeNode) nodeAux.getUserObject();
                    boolean bSelected = !(node.isSelected());
                    node.setSelected(bSelected);

                    if (evt.getButton() == MouseEvent.BUTTON1) {
                        selectedFromRoot(nodeAux, bSelected);
                    }

                    ((DefaultTreeModel) treeScorecard.getModel()).nodeChanged(node);

                    treeScorecard.revalidate();
                    treeScorecard.repaint();
                }
            }
        }
    }//GEN-LAST:event_treeScorecardMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnCancel;
    private JButton btnDelete;
    private JButton btnEdit;
    private JButton btnReload;
    private JButton btnSave;
    private JPVEdit fldNome;
    private JBISelectionPanel jBISelectionGrupoInd;
    private JBISelectionPanel jBISelectionUsuario;
    private JLabel lblDescricao;
    private JLabel lblNome;
    private JPanel pnlBottomForm;
    private JPanel pnlBottomRecord;
    private JPanel pnlConfirmation;
    private JPanel pnlData;
    private JPanel pnlDpto;
    private JPanel pnlFields;
    private JPanel pnlGrupoInd;
    private JPanel pnlLeftForm;
    private JPanel pnlLeftRecord;
    private JPanel pnlOperation;
    private JPanel pnlRecord;
    private JPanel pnlRightForm;
    private JPanel pnlRightRecord;
    private JPanel pnlTools;
    private JPanel pnlTopRecord;
    private JPanel pnlUsuario;
    private JScrollPane scpRecord;
    private JScrollPane scrollTreeScorecard;
    private JTabbedPane tapCadastro;
    private JToolBar tbConfirmation;
    private JToolBar tbInsertion;
    private JTree treeScorecard;
    private JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    public KpiPacote(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);

        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    public BIXMLVector getVectorRowsSelected(BIXMLVector vctAllData, String tagSelecionar, String tagVector, String tagRecord) {
                
        DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) treeScorecard.getModel().getRoot();
        BIXMLVector vctSco = new BIXMLVector(tagVector, tagRecord);
        KpiSelectedTreeNode oNode;

        while (oMutTreeNode != null) {
            if (!oMutTreeNode.isRoot()) {
                oNode = (KpiSelectedTreeNode) oMutTreeNode.getUserObject();
                if (oNode.isSelected()){
                    kpi.xml.BIXMLRecord oRec = new kpi.xml.BIXMLRecord(tagRecord);
                    oRec.set("ID", oNode.getID()); //NOI18N
                    oRec.set("SELECTED", oNode.isSelected()); //NOI18N
                    vctSco.add(oRec);
                }
            }
            oMutTreeNode = (DefaultMutableTreeNode) oMutTreeNode.getNextNode();    
        }
        return vctSco;
    }
    
    private void selectedFromRoot(DefaultMutableTreeNode oNode, boolean bSelected) {

        Enumeration e = oNode.children();

        while (e.hasMoreElements()) {
            DefaultMutableTreeNode nodeAux = (DefaultMutableTreeNode) e.nextElement();
            KpiSelectedTreeNode node = (KpiSelectedTreeNode) nodeAux.getUserObject();
            node.setSelected(bSelected);
            selectedFromRoot(nodeAux, bSelected);
        }
    }
}
