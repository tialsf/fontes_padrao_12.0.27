package kpi.swing.framework;

import java.awt.event.KeyEvent;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import kpi.beans.JBITextArea;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.util.KpiToolKit;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

public class KpiPlanoDeAcao extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "PLANOACAO";
    private String formType = recordType;
    private String parentType = "";
    private String tipoPlano = ""; //plano de a��o de indicador
    private String projetoId = "";
    private String permissao = "";
    private java.util.Calendar dataFimOriginal;
    private String justificativa = "";
    private boolean lReiniciou = false;
    private KpiPlanoDeAcaoCellRenderer cellRenderer;
    private java.util.Date hoje = new java.util.Date();
    private java.util.Hashtable htbStatus = new java.util.Hashtable();
    private java.util.Hashtable htbScorecard = new java.util.Hashtable();
    private java.util.Hashtable htbIndFiltrados = new java.util.Hashtable();
    private java.util.Hashtable htbResponsavel = new java.util.Hashtable();
    private java.util.Hashtable htbAssunto = new java.util.Hashtable();
    private KpiTabPlanoAcao tabPlan = new KpiTabPlanoAcao();
    private BIXMLVector backPlanilha = null; //Vetor com os dados para o bot�o de cancelamento.
    private String statusAnterior;
    private BIXMLRecord recordScoreCard;
    private BIXMLRecord recordAssunto;
    
//CONSTANTES
    public final static String TOTAL = "1";   //Caso o usu�rio seja owner ou Admin
    public final static String PARCIAL = "2";   //Caso o usu�rio seja respons�vel pelo plano
    public final static String NENHUMA = "3";   //Caso o usu�rio tenha somente permiss�o de visualiza��o

    /**
     *
     * @param operation
     * @param idAux
     * @param command
     */
    public KpiPlanoDeAcao(int operation, String idAux, String command) {
        
        initComponents();
        //Altera��o da Label de "Objetivo" - lblScoreCard
        if (!KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) { 
            lblScorecard.setText(KpiStaticReferences.getKpiCustomLabels().getSco(KpiStaticReferences.CAD_OBJETIVO).concat(":"));
        } else {
            lblScorecard.setText("Objetivo Estrat�gico:");
        }
        //Altera��o da Label de "Objetivo" - lblScoreCard
        putClientProperty("MAXI", true);
        
        kpi.core.KpiStaticReferences.getKpiMainPanel().setStatusBarCondition(1);
        loadCombos(idAux);
        event.defaultConstructor(operation, idAux, "");
        kpi.core.KpiStaticReferences.getKpiMainPanel().setStatusBarCondition(0);
        fldDataInicio.setCurrentDate(true);
        fldDataFim.setCurrentDate(true);
        
        cellRenderer = new KpiPlanoDeAcaoCellRenderer(tblDocumento);
        btnReinicia.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00060"));
        btnReinicia.setVisible(false);
        
        if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)) {
            tsArvore.setEntitySelection(KpiStaticReferences.CAD_OBJETIVO);
        }

        if (command.startsWith("COMMAND")) {
            StringTokenizer tokCmd = new StringTokenizer(command, "|");
            tokCmd.nextToken();
            String cIdSco = tokCmd.nextToken();
            String cIdInd = tokCmd.nextToken();

            event.selectComboItem(cbScorecard, event.getComboItem(recordScoreCard.getBIXMLVector("SCORECARDS"), cIdSco, true));
            kpi.xml.BIXMLRecord recIndFiltrados = event.listRecords("INDICADOR", "ID_SCOREC = '".concat(cIdSco) + "'");
            kpi.xml.BIXMLVector vctIndFiltrados = recIndFiltrados.getBIXMLVector("INDICADORES");
            event.selectComboItem(cbIndicador, event.getComboItem(vctIndFiltrados, cIdInd, true));
        }
        
    }
    
    private void loadCombos(String idAux) {
        BIXMLRecord recordAssunto1;
        BIXMLRecord recordAssunto2;
        BIXMLRecord recordAssunto3;
        BIXMLVector assunto1;
        BIXMLVector assunto2;
        BIXMLVector assunto3;
        
        recordScoreCard = event.getRecordStructure(idAux+"#SCORECARD");
        recordAssunto1 = event.getRecordStructure(idAux+"#GRUPOACOES#1");
        recordAssunto2 = event.getRecordStructure(idAux+"#GRUPOACOES#2");
        recordAssunto3 = event.getRecordStructure(idAux+"#GRUPOACOES#3");
        
        assunto1 = recordAssunto1.getBIXMLVector("GRUPO_ACOES");
        assunto2 = recordAssunto2.getBIXMLVector("GRUPO_ACOES");
        assunto3 = recordAssunto3.getBIXMLVector("GRUPO_ACOES");
        
        for (int i = 0; i < assunto2.size(); i++) {
            assunto1.add(assunto2.get(i));
        }
        
        for (int i = 0; i < assunto3.size(); i++) {
            assunto1.add(assunto3.get(i));
        }
        
        recordAssunto = recordAssunto1;        
        
    }


    @Override
    public void enableFields() {
        if (record.getBoolean("CTR_APROV_PLANACAO")) {
            enabledFieldsWithAproveControl();
        } else {
            enableFieldsWithoutAproveControl();
        }
        
        if (getStatus() == KpiDefaultFrameBehavior.INSERTING) {
            event.setEnableFields(pnlDocs, false);
        }
    }

    @Override
    public void disableFields() {
        event.setEnableFields(pnlData, false);
        event.setEnableFields(pnlDocs, false);
        event.setEnableFields(pnlHistorico, false);

        tblDocumento.setEnabled(true);
        btnDownload.setEnabled(true);
    }

    private void enableFieldsWithoutAproveControl() {
        if (this.permissao.equals(KpiPlanoDeAcao.TOTAL)) {

            event.setEnableFields(pnlData, true);
            event.setEnableFields(pnlDocs, true);

            fldDataFim.setEnabled(
                    record.getBoolean("ALTERAFIM")
                    || (status == KpiDefaultFrameBehavior.INSERTING));

            if (tipoPlano.equals("3")) {
                lblIndicador.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00019"));
                lblAssunto.setEnabled(false);
                cbAssunto.setEnabled(false);

            } else if (tipoPlano.equals("2")) {
                cbIndicador.setEnabled(false);
                cbScorecard.setEnabled(false);
                tsArvore.setEnabled(false);
                lblIndicador.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00023"));

            } else {
                lblIndicador.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00019"));
            }

        } else if (this.permissao.equals(KpiPlanoDeAcao.PARCIAL)) {
            lblStatus.setEnabled(true);
            cbStatus.setEnabled(true);
            lblRealizado.setEnabled(true);
            spnRealizado.setEnabled(true);
            lblObservacaoChar.setEnabled(true);
            lblObservacao.setEnabled(true);
            txtObservacao.setEnabled(true);
        }

        if (!this.permissao.equals(KpiPlanoDeAcao.NENHUMA)) {
            tblDocumento.setEnabled(true);
            tblDocumento.getTable().setEditable(true);
            tblDocumento.setEditable(true);
            btnDocNovo.setEnabled(true);
            btnDocExcluir.setEnabled(true);
            btnDocExcluirTudo.setEnabled(true);
            btnDownload.setEnabled(false);
        }
    }

    private void enabledFieldsWithAproveControl() {
        if (record.getString("STATUS").equals(KpiStaticReferences.ACAO_NAO_INICIADA)) {

            event.setEnableFields(pnlData, true);
            event.setEnableFields(pnlDocs, true);

            fldDataFim.setEnabled(
                    record.getBoolean("ALTERAFIM")
                    || (status == KpiDefaultFrameBehavior.INSERTING));

            if (tipoPlano.equals("3")) {
                lblIndicador.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00019"));
                lblAssunto.setEnabled(false);
                cbAssunto.setEnabled(false);

            } else if (tipoPlano.equals("2")) {
                cbIndicador.setEnabled(false);
                cbScorecard.setEnabled(false);
                tsArvore.setEnabled(false);
                lblIndicador.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00023"));

            } else {
                lblIndicador.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00019"));
            }
            tblDocumento.setEnabled(true);
            tblDocumento.getTable().setEditable(true);
            tblDocumento.setEditable(true);
            btnDocNovo.setEnabled(true);
            btnDocExcluir.setEnabled(true);
            btnDocExcluirTudo.setEnabled(true);
            btnDownload.setEnabled(false);
        } else {
            lblStatus.setEnabled(true);
            cbStatus.setEnabled(true);
            lblRealizado.setEnabled(true);
            spnRealizado.setEnabled(true);
            lblObservacaoChar.setEnabled(true);
            lblObservacao.setEnabled(true);
            txtObservacao.setEnabled(true);

            tblDocumento.setEnabled(true);
            tblDocumento.getTable().setEditable(true);
            tblDocumento.setEditable(true);
            btnDocNovo.setEnabled(true);
            btnDocExcluir.setEnabled(true);
            btnDocExcluirTudo.setEnabled(true);
            btnDownload.setEnabled(false);

        }
    }

    @Override
    public void refreshFields() {
        permissao = record.getString("PERMISSAO");
        tipoPlano = record.getString("TIPOACAO");
        projetoId = record.getString("ID_PROJ");

        if (getStatus() == KpiDefaultFrameBehavior.INSERTING) {
            fldDataCadastro.setDate(hoje);
            tabPlan.resetExcluidos();
            tabPlan.setCabecPeriodo();
            tblDocumento.setDataSource(tabPlan.getVctDados());

        } else {
            NumberFormat nf = NumberFormat.getInstance();
            nf.setMaximumFractionDigits(2);
            nf.setMinimumFractionDigits(2);
            nf.setGroupingUsed(true);
            fldDataCadastro.setCalendar(record.getDate("DATACADAST"));
            fldDataInicio.setEnabled(false);
            tabPlan.resetExcluidos();
            tabPlan.setCabecPeriodo();
            backPlanilha = record.getBIXMLVector("PLANODOCS");

            for (int i = 0; i < backPlanilha.size(); i++) {
                BIXMLRecord arquivo = backPlanilha.get(i);
                float tamanho = arquivo.getFloat("SIZE");
                arquivo.set("SIZE", nf.format(tamanho).toString() + " kb");
            }

            tblDocumento.setDataSource(backPlanilha.clone2());
        }

        setRenderTable();

        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        fldCriador.setText(record.getString("OWNER_NAME"));
        fldDataFim.setCalendar(record.getDate("DATAFIM"));
        dataFimOriginal = fldDataFim.getCalendar();
        fldDataInicio.setCalendar(record.getDate("DATAINICIO"));
        fldDataTermino.setCalendar(record.getDate("DATATERM"));
        fldInvestimento.setValue(record.getDouble("VLRINVEST"));
        spnRealizado.setValue(record.getInt("PERCREAL"));
        txtCausa.setText(record.getString("CAUSA"));
        txtLogAcao.setText(record.getString("LOGACAO"));
        txtComo.setText(record.getString("COMO"));
        txtInvestimento.setText(record.getString("INVESTIMEN"));
        txtObjetivo.setText(record.getString("OBJETIVO"));
        txtObservacao.setText(record.getString("OBSERVACAO"));
        txtResultado.setText(record.getString("RESULTADO"));
        event.populateCombo(record.getBIXMLVector("LSTSTATUS"), "STATUS", htbStatus, cbStatus);
        statusAnterior = record.getString("STATUS");
        
        event.populateCombo(recordScoreCard.getBIXMLVector("SCORECARDS"), "ID_SCOREC", htbScorecard, cbScorecard);
        
        tsArvore.setVetor(recordScoreCard.getBIXMLVector("SCORECARDS"));

        BIXMLVector vctUsuarios = record.getBIXMLVector("RESPONSAVEIS");
        BIXMLVector vctTreeUser = record.getBIXMLVector("TREEUSERS");

        event.populateCombo(vctUsuarios, "TP_RESP_ID_RESP", htbResponsavel, cbResponsavel);
        event.selectComboItem(cbResponsavel, event.getComboItem(vctUsuarios, record.getString("TP_RESP_ID_RESP"), true));
        tsTreeResp.setVetor(vctUsuarios);
        tsTreeResp.setTree(vctTreeUser);

        event.populateCombo(recordAssunto.getBIXMLVector("GRUPO_ACOES"), "NOME", htbAssunto, cbAssunto);
        
        cmbCategoria.setSelectedIndex(record.getInt("CATEGORIA"));

        event.selectComboItem(cbScorecard, event.getComboItem(recordScoreCard.getBIXMLVector("SCORECARDS"), record.getString("ID_SCOREC"), true));
        event.selectComboItem(cbAssunto, event.getComboItem(recordAssunto.getBIXMLVector("GRUPO_ACOES"), record.getString("ID_ASSUNTO"), true));

        if (tipoPlano.equals("2")) {
            lblIndicador.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00023"));
            event.populateCombo(record.getBIXMLVector("PROJETOS"), "ID_PROJ", htbIndFiltrados, cbIndicador);

        } else {
            lblIndicador.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00019"));

            kpi.xml.BIXMLRecord recIndFiltrados = event.listRecords("INDICADOR", "ID_SCOREC = '".concat(record.getString("ID_SCOREC")) + "'");
            kpi.xml.BIXMLVector vctIndFiltrados = recIndFiltrados.getBIXMLVector("INDICADORES");

            event.populateCombo(vctIndFiltrados, "INDICADOR", htbIndFiltrados, cbIndicador);
            event.selectComboItem(cbIndicador, event.getComboItem(vctIndFiltrados, record.getString("ID_IND"), true));
        }

        cbStatus.removeItemAt(0);
        event.selectComboItem(cbStatus, event.getComboItem(record.getBIXMLVector("LSTSTATUS"), record.getString("STATUS"), false));

        if (parentType.equals("")) {
            if (tipoPlano.equals("3")) {
                parentType = "GRUPO_ACAO";
                lblScorecard.setForeground(new java.awt.Color(51, 51, 255));
                lblIndicador.setForeground(new java.awt.Color(51, 51, 255));

            } else if (tipoPlano.equals("2")) {
                parentType = "PROJETO";

            } else {
                lblScorecard.setForeground(new java.awt.Color(51, 51, 255));
                lblIndicador.setForeground(new java.awt.Color(51, 51, 255));
                parentType = "LSTPLANOACAO";
            }
        }

        this.lblCausaChar.setText(doMaxChar(null, this.txtCausa, 500, false));
        this.lblDescricaoChar.setText(doMaxChar(null, this.txtDescricao, 255, false));
        this.lblObjetivoChar.setText(doMaxChar(null, this.txtObjetivo, 255, false));
        this.lblComoChar.setText(doMaxChar(null, this.txtComo, 255, false));
        this.lblInvestimentoChar.setText(doMaxChar(null, this.txtInvestimento, 255, false));
        this.lblResultadoChar.setText(doMaxChar(null, this.txtResultado, 255, false));
        this.lblObservacaoChar.setText(doMaxChar(null, this.txtObservacao, 255, false));

        //Somente Administrador pode excluir Acao
        if (record.getBoolean("ISADMIN")) {
            btnDelete.setEnabled(true);
        } else {
            btnDelete.setEnabled(false);
        }

        //Acao Cancelada
        if (record.getString("STATUS").equals(KpiStaticReferences.ACAO_CANCELADA)) {
            btnEdit.setEnabled(false);

            if (record.contains("JUSTIFICA")) {
                txtObservacao.setText(txtObservacao.getText() + "\n" + java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00045") + " " + record.getString("JUSTIFICA"));
                txtObservacao.setToolTipText(txtObservacao.getText());
            }
        } else if (record.getString("STATUS").equals(KpiStaticReferences.ACAO_EXECUTADA)) {    //Acao Executada
            btnReinicia.setVisible(true);
            btnEdit.setVisible(false);
        } else {

            //Se estiver com controle de aprova��o e o usuario for responsavel, habilita alteracao
            if (record.getBoolean("CTR_APROV_PLANACAO")) {

                if (record.getBoolean("ISRESP") || record.getBoolean("GRUPOACAO_ISRESP") || record.getBoolean("ISADMIN")) {

                    if (record.getString("GRUPOACAO_STATUS").equals(KpiStaticReferences.GRUPOACAO_EM_APROVACAO)) {
                        btnEdit.setEnabled(false);
                    } else {
                        btnEdit.setEnabled(true);
                    }

                } else {
                    btnEdit.setEnabled(false);
                }

            } else {
                if (this.permissao.equals(KpiPlanoDeAcao.NENHUMA)) {
                    this.btnEdit.setEnabled(false);
                }
            }
        }
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);

        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("DATAFIM", fldDataFim.getCalendar());
        recordAux.set("DATAINICIO", fldDataInicio.getCalendar());
        recordAux.set("DATATERM", fldDataTermino.getCalendar());
        recordAux.set("VLRINVEST", fldInvestimento.getDouble());
        recordAux.set("PERCREAL", ((Integer) spnRealizado.getValue()).intValue());
        recordAux.set("CAUSA", txtCausa.getText());
        recordAux.set("COMO", txtComo.getText());
        recordAux.set("INVESTIMEN", txtInvestimento.getText());
        recordAux.set("OBJETIVO", txtObjetivo.getText());
        recordAux.set("OBSERVACAO", txtObservacao.getText());
        recordAux.set("RESULTADO", txtResultado.getText());
        recordAux.set("STATUS", event.getComboValue(htbStatus, cbStatus, true));
        recordAux.set("ID_SCOREC", event.getComboValue(htbScorecard, cbScorecard));

        if (event.getComboValue(htbResponsavel, cbResponsavel).substring(0, 1).equals("0")) {
            recordAux.set("TP_RESP", "");
            recordAux.set("ID_RESP", "");
        } else {
            recordAux.set("TP_RESP", event.getComboValue(htbResponsavel, cbResponsavel).substring(0, 1));
            recordAux.set("ID_RESP", event.getComboValue(htbResponsavel, cbResponsavel).substring(1));
        }

        recordAux.set("DATACADAST", fldDataCadastro.getCalendar());
        recordAux.set("ID_ASSUNTO", event.getComboValue(htbAssunto, cbAssunto));
        recordAux.set("LOGACAO", txtLogAcao.getText());
        recordAux.set("REINICIA", lReiniciou);
        recordAux.set("TIPOACAO", tipoPlano);

        if (tipoPlano.equals("2")) {
            recordAux.set("ID_PROJ", projetoId);
            recordAux.set("ID_IND", "");
        } else {
            recordAux.set("ID_IND", event.getComboValue(htbIndFiltrados, cbIndicador));
            recordAux.set("ID_PROJ", "");
            recordAux.set("TIPOACAO", "1");
        }

        recordAux.set("CATEGORIA", cmbCategoria.getSelectedIndex());

        kpi.xml.BIXMLVector vctDadosPlan = tblDocumento.getXMLData().clone2();
        kpi.xml.BIXMLRecord recDadosDel = tabPlan.getDelDados();

        if (recDadosDel != null) {
            vctDadosPlan.add(tabPlan.getDelDados());
        }
        recordAux.set(vctDadosPlan);

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        if (fldNome.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00030"));
            retorno = false;

        } else if (cbScorecard.getSelectedIndex() <= 0) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00031"));
            retorno = false;

        } else if (cbIndicador.getSelectedIndex() <= 0) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00032"));
            retorno = false;

        } else if (fldDataFim.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00034"));
            retorno = false;

        } else if (txtDescricao.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00036"));
            retorno = false;

        } else if (txtObjetivo.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00037"));
            retorno = false;

        } else if (cbResponsavel.getSelectedIndex() <= 0) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00039"));
            retorno = false;

        } else if (!fldDataInicio.getText().trim().equals("")) {
            if (fldDataInicio.getCalendar().after(fldDataFim.getCalendar())) {
                errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00024"));
                retorno = false;
            } else if (fldDataInicio.getCalendar().after(fldDataTermino.getCalendar())) {
                errorMessage.append("Data inicial deve ser menor ou igual a data atual para conclus�o da a��o");
                retorno = false;
            }
        // Rotina que verifica se o status for conclu�do a porcentagem de realizado deve ser 100%
        } else if (cbStatus.getSelectedIndex() == 2) { // Conclu�da
            // Recebe o valor da porcentagem realizada.
            int porcRealizada = Integer.parseInt(spnRealizado.getValue().toString());
            
            if (porcRealizada < 100) {
                errorMessage.append("Para concluir esta a��o, a op��o realizado deve ser de 100%");
                spnRealizado.requestFocus();
                spnRealizado.transferFocus(); // O Campo de 'spinner' recebe o foco.
                retorno = false;
            }
        }
         
        if (!retorno) {
            btnSave.setEnabled(true);
        }

        return retorno;
    }

    /**
     *
     * @return
     */
    public String pasta_destino() {
        String pasta_destino = "\\sgidocs\\plano_acao\\";
        String pasta = "";

        pasta = pasta_destino + subpasta_destino();
        return (pasta);
    }

    /**
     *
     * @return
     */
    public String subpasta_destino() {
        String pasta = "";

        pasta = "pl" + record.getString("ID");
        pasta += "\\";
        return (pasta);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        grpIndPrj = new javax.swing.ButtonGroup();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnReinicia = new javax.swing.JButton();
        btnAjuda2 = new javax.swing.JButton();
        tapCadastro = new javax.swing.JTabbedPane();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        fldNome = new pv.jfcx.JPVEdit();
        cbScorecard = new javax.swing.JComboBox();
        cbIndicador = new javax.swing.JComboBox();
        scrlCausa = new javax.swing.JScrollPane();
        txtCausa = new kpi.beans.JBITextArea();
        scrlDescricao = new javax.swing.JScrollPane();
        txtDescricao = new kpi.beans.JBITextArea();
        scrlObjetivo = new javax.swing.JScrollPane();
        txtObjetivo = new kpi.beans.JBITextArea();
        scrComo = new javax.swing.JScrollPane();
        txtComo = new kpi.beans.JBITextArea();
        scrInicio = new javax.swing.JScrollPane();
        fldDataInicio = new pv.jfcx.JPVDatePlus();
        scrFim = new javax.swing.JScrollPane();
        fldDataFim = new pv.jfcx.JPVDatePlus();
        cbStatus = new javax.swing.JComboBox();
        fldInvestimento = new pv.jfcx.JPVCurrency();
        scrlInvestimento = new javax.swing.JScrollPane();
        txtInvestimento = new kpi.beans.JBITextArea();
        scrlResultado = new javax.swing.JScrollPane();
        txtResultado = new kpi.beans.JBITextArea();
        scrlObservacao = new javax.swing.JScrollPane();
        txtObservacao = new kpi.beans.JBITextArea();
        lblNome = new javax.swing.JLabel();
        lblScorecard = new javax.swing.JLabel();
        lblIndicador = new javax.swing.JLabel();
        lblDescricao = new javax.swing.JLabel();
        lblDataInicio = new javax.swing.JLabel();
        lblDataFim = new javax.swing.JLabel();
        lblValorInvestimento = new javax.swing.JLabel();
        lblStatus = new javax.swing.JLabel();
        lblRealizado = new javax.swing.JLabel();
        lblResponsaveis = new javax.swing.JLabel();
        lblComo = new javax.swing.JLabel();
        lblInvestimento = new javax.swing.JLabel();
        lblObjetivo = new javax.swing.JLabel();
        lblResultado = new javax.swing.JLabel();
        lblObservacao = new javax.swing.JLabel();
        cbResponsavel = new javax.swing.JComboBox();
        lblAssunto = new javax.swing.JLabel();
        cbAssunto = new javax.swing.JComboBox();
        lblInvestimentoChar = new javax.swing.JLabel();
        lblCausa = new javax.swing.JLabel();
        lblCausaChar = new javax.swing.JLabel();
        lblDescricaoChar = new javax.swing.JLabel();
        lblObjetivoChar = new javax.swing.JLabel();
        lblComoChar = new javax.swing.JLabel();
        lblResultadoChar = new javax.swing.JLabel();
        lblObservacaoChar = new javax.swing.JLabel();
        spnRealizado = new javax.swing.JSpinner();
        cmbCategoria = new javax.swing.JComboBox();
        lblCategoria = new javax.swing.JLabel();
        tsArvore = new kpi.beans.JBITreeSelection();
        tsTreeResp = new kpi.beans.JBITreeSelection(kpi.beans.JBITreeSelection.TYPE_DEFAULT);
        pnlDocs = new javax.swing.JPanel();
        jBarDoc = new javax.swing.JToolBar();
        btnDocNovo = new javax.swing.JButton();
        btnDocExcluir = new javax.swing.JButton();
        btnDocExcluirTudo = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        btnDownload = new javax.swing.JButton();
        tblDocumento = new kpi.beans.JBIXMLTable();
        pnlHistorico = new javax.swing.JPanel();
        pnlDetalheHistorico = new javax.swing.JPanel();
        fldCriador = new javax.swing.JTextField();
        lblCriador = new javax.swing.JLabel();
        fldDataCadastro = new pv.jfcx.JPVDatePlus();
        lblDataCadastro = new javax.swing.JLabel();
        fldDataTermino = new pv.jfcx.JPVDatePlus();
        lblDataTermino = new javax.swing.JLabel();
        lblLog = new javax.swing.JLabel();
        pnlDescricaoHistorico = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtLogAcao = new javax.swing.JTextArea();
        pnlLeftForm1 = new javax.swing.JPanel();
        pnlRightForm1 = new javax.swing.JPanel();
        pnlBottomForm1 = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiPlanoDeAcao_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_planodeacao.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(900, 575));

        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 25));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setPreferredSize(new java.awt.Dimension(450, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setMinimumSize(new java.awt.Dimension(44, 25));
        tbInsertion.setPreferredSize(new java.awt.Dimension(300, 32));

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnReinicia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_planodeacao.gif"))); // NOI18N
        btnReinicia.setText("Reiniciar A��o");
        btnReinicia.setFocusable(false);
        btnReinicia.setMaximumSize(new java.awt.Dimension(100, 20));
        btnReinicia.setMinimumSize(new java.awt.Dimension(100, 20));
        btnReinicia.setPreferredSize(new java.awt.Dimension(100, 20));
        btnReinicia.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnReinicia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReiniciaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReinicia);

        btnAjuda2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnAjuda2.setToolTipText(bundle1.getString("KpiGraphIndicador_00011")); // NOI18N
        btnAjuda2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjuda2ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda2);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        getContentPane().add(pnlTools, java.awt.BorderLayout.NORTH);

        tapCadastro.setPreferredSize(new java.awt.Dimension(890, 450));

        scpRecord.setBorder(null);
        scpRecord.setPreferredSize(new java.awt.Dimension(870, 400));

        pnlData.setOpaque(false);
        pnlData.setPreferredSize(new java.awt.Dimension(860, 480));
        pnlData.setLayout(null);

        fldNome.setMaxLength(255);
        pnlData.add(fldNome);
        fldNome.setBounds(20, 30, 400, 22);

        cbScorecard.setEnabled(false);
        cbScorecard.setPreferredSize(new java.awt.Dimension(10, 10));
        cbScorecard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbScorecardActionPerformed(evt);
            }
        });
        pnlData.add(cbScorecard);
        cbScorecard.setBounds(20, 70, 400, 22);

        cbIndicador.setEnabled(false);
        cbIndicador.setPreferredSize(new java.awt.Dimension(120, 20));
        pnlData.add(cbIndicador);
        cbIndicador.setBounds(20, 110, 400, 22);

        scrlCausa.setAutoscrolls(true);

        txtCausa.setRows(50);
        txtCausa.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        txtCausa.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtCausa.setSize(new java.awt.Dimension(420, 45));
        txtCausa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCausaKeyReleased(evt);
            }
        });
        scrlCausa.setViewportView(txtCausa);

        pnlData.add(scrlCausa);
        scrlCausa.setBounds(20, 390, 400, 60);

        scrlDescricao.setAutoscrolls(true);
        scrlDescricao.setPreferredSize(new java.awt.Dimension(420, 45));

        txtDescricao.setRows(50);
        txtDescricao.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        txtDescricao.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtDescricao.setSize(new java.awt.Dimension(420, 45));
        txtDescricao.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDescricaoKeyReleased(evt);
            }
        });
        scrlDescricao.setViewportView(txtDescricao);

        pnlData.add(scrlDescricao);
        scrlDescricao.setBounds(20, 230, 400, 60);

        scrlObjetivo.setAutoscrolls(true);
        scrlObjetivo.setPreferredSize(new java.awt.Dimension(2059, 40));

        txtObjetivo.setRows(50);
        txtObjetivo.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        txtObjetivo.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtObjetivo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtObjetivoKeyReleased(evt);
            }
        });
        scrlObjetivo.setViewportView(txtObjetivo);

        pnlData.add(scrlObjetivo);
        scrlObjetivo.setBounds(20, 310, 400, 60);

        txtComo.setRows(50);
        txtComo.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtComo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtComoKeyReleased(evt);
            }
        });
        scrComo.setViewportView(txtComo);

        pnlData.add(scrComo);
        scrComo.setBounds(460, 230, 400, 60);

        fldDataInicio.setBorderStyle(0);
        fldDataInicio.setButtonBorder(0);
        fldDataInicio.setDialog(true);
        fldDataInicio.setUseLocale(true);
        fldDataInicio.setWaitForCalendarDate(true);
        scrInicio.setViewportView(fldDataInicio);

        pnlData.add(scrInicio);
        scrInicio.setBounds(460, 30, 180, 22);

        fldDataFim.setAllowNull(false);
        fldDataFim.setBorderStyle(0);
        fldDataFim.setButtonBorder(0);
        fldDataFim.setDialog(true);
        fldDataFim.setUseLocale(true);
        fldDataFim.setWaitForCalendarDate(true);
        scrFim.setViewportView(fldDataFim);

        pnlData.add(scrFim);
        scrFim.setBounds(680, 30, 180, 22);

        cbStatus.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbStatusItemStateChanged(evt);
            }
        });
        pnlData.add(cbStatus);
        cbStatus.setBounds(460, 130, 180, 22);

        fldInvestimento.setAllowNull(false);
        fldInvestimento.setMaxValue(1.0E15);
        fldInvestimento.setMinValue(0.0);
        fldInvestimento.setUseLocale(true);
        pnlData.add(fldInvestimento);
        fldInvestimento.setBounds(680, 70, 180, 22);

        scrlInvestimento.setAutoscrolls(true);
        scrlInvestimento.setPreferredSize(new java.awt.Dimension(2059, 40));

        txtInvestimento.setRows(50);
        txtInvestimento.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        txtInvestimento.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtInvestimento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtInvestimentoKeyReleased(evt);
            }
        });
        scrlInvestimento.setViewportView(txtInvestimento);

        pnlData.add(scrlInvestimento);
        scrlInvestimento.setBounds(460, 310, 400, 60);

        scrlResultado.setAutoscrolls(true);

        txtResultado.setRows(50);
        txtResultado.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        txtResultado.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtResultado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtResultadoKeyReleased(evt);
            }
        });
        scrlResultado.setViewportView(txtResultado);

        pnlData.add(scrlResultado);
        scrlResultado.setBounds(460, 390, 400, 60);

        scrlObservacao.setAutoscrolls(true);

        txtObservacao.setRows(50);
        txtObservacao.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        txtObservacao.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtObservacao.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtObservacaoKeyReleased(evt);
            }
        });
        scrlObservacao.setViewportView(txtObservacao);

        pnlData.add(scrlObservacao);
        scrlObservacao.setBounds(680, 130, 180, 60);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setLabelFor(fldNome);
        lblNome.setText(bundle.getString("KpiPlanoDeAcao_00002")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setFocusable(false);
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(20, 10, 70, 20);

        lblScorecard.setForeground(new java.awt.Color(51, 51, 255));
        lblScorecard.setLabelFor(cbScorecard);
        lblScorecard.setEnabled(false);
        lblScorecard.setFocusable(false);
        lblScorecard.setPreferredSize(new java.awt.Dimension(55, 15));
        pnlData.add(lblScorecard);
        lblScorecard.setBounds(20, 50, 110, 18);

        lblIndicador.setForeground(new java.awt.Color(51, 51, 255));
        lblIndicador.setLabelFor(cbIndicador);
        lblIndicador.setText("Indicador:");
        lblIndicador.setEnabled(false);
        lblIndicador.setFocusable(false);
        lblIndicador.setPreferredSize(new java.awt.Dimension(55, 15));
        pnlData.add(lblIndicador);
        lblIndicador.setBounds(20, 90, 72, 20);

        lblDescricao.setForeground(new java.awt.Color(51, 51, 255));
        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setLabelFor(scrlDescricao);
        lblDescricao.setText(bundle.getString("KpiPlanoDeAcao_00003")); // NOI18N
        lblDescricao.setEnabled(false);
        lblDescricao.setFocusable(false);
        lblDescricao.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblDescricao);
        lblDescricao.setBounds(20, 210, 70, 20);

        lblDataInicio.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataInicio.setText(bundle.getString("KpiPlanoDeAcao_00011")); // NOI18N
        lblDataInicio.setEnabled(false);
        lblDataInicio.setFocusable(false);
        lblDataInicio.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDataInicio.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDataInicio.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblDataInicio);
        lblDataInicio.setBounds(460, 10, 80, 20);

        lblDataFim.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataFim.setText(bundle.getString("KpiPlanoDeAcao_00012")); // NOI18N
        lblDataFim.setEnabled(false);
        lblDataFim.setFocusable(false);
        lblDataFim.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDataFim.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDataFim.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblDataFim);
        lblDataFim.setBounds(680, 10, 78, 20);

        lblValorInvestimento.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblValorInvestimento.setText(bundle.getString("KpiPlanoDeAcao_00016")); // NOI18N
        lblValorInvestimento.setEnabled(false);
        lblValorInvestimento.setFocusable(false);
        lblValorInvestimento.setMaximumSize(new java.awt.Dimension(100, 16));
        lblValorInvestimento.setMinimumSize(new java.awt.Dimension(100, 16));
        lblValorInvestimento.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblValorInvestimento);
        lblValorInvestimento.setBounds(680, 50, 80, 20);

        lblStatus.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblStatus.setText(bundle.getString("KpiPlanoDeAcao_00014")); // NOI18N
        lblStatus.setEnabled(false);
        lblStatus.setFocusable(false);
        lblStatus.setMaximumSize(new java.awt.Dimension(100, 16));
        lblStatus.setMinimumSize(new java.awt.Dimension(100, 16));
        lblStatus.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblStatus);
        lblStatus.setBounds(460, 110, 82, 20);

        lblRealizado.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblRealizado.setText("Realizado (%):");
        lblRealizado.setEnabled(false);
        lblRealizado.setFocusable(false);
        lblRealizado.setMaximumSize(new java.awt.Dimension(100, 16));
        lblRealizado.setMinimumSize(new java.awt.Dimension(100, 16));
        lblRealizado.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblRealizado);
        lblRealizado.setBounds(460, 150, 82, 20);

        lblResponsaveis.setForeground(new java.awt.Color(51, 51, 255));
        lblResponsaveis.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblResponsaveis.setText(bundle.getString("KpiPlanoDeAcao_00004")); // NOI18N
        lblResponsaveis.setEnabled(false);
        lblResponsaveis.setFocusable(false);
        lblResponsaveis.setMaximumSize(new java.awt.Dimension(100, 16));
        lblResponsaveis.setMinimumSize(new java.awt.Dimension(100, 16));
        lblResponsaveis.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblResponsaveis);
        lblResponsaveis.setBounds(20, 170, 70, 20);

        lblComo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblComo.setText(bundle.getString("KpiPlanoDeAcao_00006")); // NOI18N
        lblComo.setEnabled(false);
        lblComo.setFocusable(false);
        lblComo.setMaximumSize(new java.awt.Dimension(100, 16));
        lblComo.setMinimumSize(new java.awt.Dimension(100, 16));
        lblComo.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblComo);
        lblComo.setBounds(460, 210, 70, 20);

        lblInvestimento.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblInvestimento.setText(bundle.getString("KpiPlanoDeAcao_00007")); // NOI18N
        lblInvestimento.setEnabled(false);
        lblInvestimento.setFocusable(false);
        lblInvestimento.setMaximumSize(new java.awt.Dimension(100, 16));
        lblInvestimento.setMinimumSize(new java.awt.Dimension(100, 16));
        lblInvestimento.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblInvestimento);
        lblInvestimento.setBounds(460, 290, 130, 20);

        lblObjetivo.setForeground(new java.awt.Color(51, 51, 255));
        lblObjetivo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblObjetivo.setText(bundle.getString("KpiPlanoDeAcao_00005")); // NOI18N
        lblObjetivo.setEnabled(false);
        lblObjetivo.setFocusable(false);
        lblObjetivo.setMaximumSize(new java.awt.Dimension(100, 16));
        lblObjetivo.setMinimumSize(new java.awt.Dimension(100, 16));
        lblObjetivo.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblObjetivo);
        lblObjetivo.setBounds(20, 290, 70, 20);

        lblResultado.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblResultado.setText(bundle.getString("KpiPlanoDeAcao_00009")); // NOI18N
        lblResultado.setEnabled(false);
        lblResultado.setFocusable(false);
        lblResultado.setMaximumSize(new java.awt.Dimension(100, 16));
        lblResultado.setMinimumSize(new java.awt.Dimension(100, 16));
        lblResultado.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblResultado);
        lblResultado.setBounds(460, 370, 130, 20);

        lblObservacao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblObservacao.setText("Observa��o:");
        lblObservacao.setEnabled(false);
        lblObservacao.setFocusable(false);
        lblObservacao.setMaximumSize(new java.awt.Dimension(100, 16));
        lblObservacao.setMinimumSize(new java.awt.Dimension(100, 16));
        lblObservacao.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblObservacao);
        lblObservacao.setBounds(680, 110, 70, 20);
        pnlData.add(cbResponsavel);
        cbResponsavel.setBounds(20, 190, 400, 22);

        lblAssunto.setLabelFor(cbScorecard);
        lblAssunto.setText(bundle1.getString("KpiPlanoDeAcao_00051")); // NOI18N
        lblAssunto.setEnabled(false);
        lblAssunto.setFocusable(false);
        lblAssunto.setPreferredSize(new java.awt.Dimension(55, 15));
        pnlData.add(lblAssunto);
        lblAssunto.setBounds(20, 130, 90, 20);

        cbAssunto.setEnabled(false);
        cbAssunto.setPreferredSize(new java.awt.Dimension(10, 10));
        pnlData.add(cbAssunto);
        cbAssunto.setBounds(20, 150, 400, 22);

        lblInvestimentoChar.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblInvestimentoChar.setForeground(new java.awt.Color(102, 102, 102));
        lblInvestimentoChar.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblInvestimentoChar.setLabelFor(scrlCausa);
        lblInvestimentoChar.setText(bundle.getString("KpiPlanoDeAcao_00054")); // NOI18N
        lblInvestimentoChar.setFocusable(false);
        lblInvestimentoChar.setMaximumSize(new java.awt.Dimension(100, 16));
        lblInvestimentoChar.setMinimumSize(new java.awt.Dimension(100, 16));
        lblInvestimentoChar.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblInvestimentoChar);
        lblInvestimentoChar.setBounds(580, 290, 280, 20);

        lblCausa.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblCausa.setLabelFor(scrlCausa);
        lblCausa.setText(bundle.getString("KpiPlanoDeAcao_00008")); // NOI18N
        lblCausa.setEnabled(false);
        lblCausa.setFocusable(false);
        lblCausa.setMaximumSize(new java.awt.Dimension(100, 16));
        lblCausa.setMinimumSize(new java.awt.Dimension(100, 16));
        lblCausa.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblCausa);
        lblCausa.setBounds(20, 370, 110, 20);

        lblCausaChar.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblCausaChar.setForeground(new java.awt.Color(102, 102, 102));
        lblCausaChar.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCausaChar.setLabelFor(scrlCausa);
        lblCausaChar.setText(bundle.getString("KpiPlanoDeAcao_00054")); // NOI18N
        lblCausaChar.setFocusable(false);
        lblCausaChar.setMaximumSize(new java.awt.Dimension(100, 16));
        lblCausaChar.setMinimumSize(new java.awt.Dimension(100, 16));
        lblCausaChar.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblCausaChar);
        lblCausaChar.setBounds(140, 370, 280, 20);

        lblDescricaoChar.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblDescricaoChar.setForeground(new java.awt.Color(102, 102, 102));
        lblDescricaoChar.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDescricaoChar.setLabelFor(scrlCausa);
        lblDescricaoChar.setText(bundle.getString("KpiPlanoDeAcao_00054")); // NOI18N
        lblDescricaoChar.setFocusable(false);
        lblDescricaoChar.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDescricaoChar.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDescricaoChar.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblDescricaoChar);
        lblDescricaoChar.setBounds(140, 210, 280, 20);

        lblObjetivoChar.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblObjetivoChar.setForeground(new java.awt.Color(102, 102, 102));
        lblObjetivoChar.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblObjetivoChar.setLabelFor(scrlCausa);
        lblObjetivoChar.setText(bundle.getString("KpiPlanoDeAcao_00054")); // NOI18N
        lblObjetivoChar.setFocusable(false);
        lblObjetivoChar.setMaximumSize(new java.awt.Dimension(100, 16));
        lblObjetivoChar.setMinimumSize(new java.awt.Dimension(100, 16));
        lblObjetivoChar.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblObjetivoChar);
        lblObjetivoChar.setBounds(140, 290, 280, 20);

        lblComoChar.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblComoChar.setForeground(new java.awt.Color(102, 102, 102));
        lblComoChar.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblComoChar.setLabelFor(scrlCausa);
        lblComoChar.setText(bundle.getString("KpiPlanoDeAcao_00054")); // NOI18N
        lblComoChar.setFocusable(false);
        lblComoChar.setMaximumSize(new java.awt.Dimension(100, 16));
        lblComoChar.setMinimumSize(new java.awt.Dimension(100, 16));
        lblComoChar.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblComoChar);
        lblComoChar.setBounds(580, 210, 280, 20);

        lblResultadoChar.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblResultadoChar.setForeground(new java.awt.Color(102, 102, 102));
        lblResultadoChar.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblResultadoChar.setLabelFor(scrlCausa);
        lblResultadoChar.setText(bundle.getString("KpiPlanoDeAcao_00054")); // NOI18N
        lblResultadoChar.setFocusable(false);
        lblResultadoChar.setMaximumSize(new java.awt.Dimension(100, 16));
        lblResultadoChar.setMinimumSize(new java.awt.Dimension(100, 16));
        lblResultadoChar.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblResultadoChar);
        lblResultadoChar.setBounds(580, 370, 280, 20);

        lblObservacaoChar.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblObservacaoChar.setForeground(new java.awt.Color(102, 102, 102));
        lblObservacaoChar.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblObservacaoChar.setLabelFor(scrlCausa);
        lblObservacaoChar.setText(bundle.getString("KpiPlanoDeAcao_00054")); // NOI18N
        lblObservacaoChar.setFocusable(false);
        lblObservacaoChar.setMaximumSize(new java.awt.Dimension(100, 16));
        lblObservacaoChar.setMinimumSize(new java.awt.Dimension(100, 16));
        lblObservacaoChar.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblObservacaoChar);
        lblObservacaoChar.setBounds(750, 110, 110, 20);

        spnRealizado.setModel(new javax.swing.SpinnerNumberModel(0, 0, 100, 1));
        pnlData.add(spnRealizado);
        spnRealizado.setBounds(460, 170, 180, 22);

        cmbCategoria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Corretiva", "Preventiva" }));
        pnlData.add(cmbCategoria);
        cmbCategoria.setBounds(460, 70, 180, 22);

        lblCategoria.setText("Tipo:");
        lblCategoria.setEnabled(false);
        pnlData.add(lblCategoria);
        lblCategoria.setBounds(460, 50, 180, 20);

        tsArvore.setCombo(cbScorecard);
        tsArvore.setRoot(false);
        pnlData.add(tsArvore);
        tsArvore.setBounds(420, 70, 30, 22);

        tsTreeResp.setCombo(cbResponsavel);
        tsTreeResp.setRoot(false);
        pnlData.add(tsTreeResp);
        tsTreeResp.setBounds(420, 190, 25, 22);

        scpRecord.setViewportView(pnlData);

        tapCadastro.addTab(bundle1.getString("KpiPlanoDeAcao_00001"), scpRecord); // NOI18N

        pnlDocs.setLayout(new java.awt.BorderLayout());

        jBarDoc.setFloatable(false);
        jBarDoc.setRollover(true);
        jBarDoc.setMaximumSize(new java.awt.Dimension(276, 32));
        jBarDoc.setMinimumSize(new java.awt.Dimension(276, 32));
        jBarDoc.setPreferredSize(new java.awt.Dimension(275, 25));

        btnDocNovo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_novo.gif"))); // NOI18N
        btnDocNovo.setText(bundle1.getString("KpiPlanoDeAcao_00046")); // NOI18N
        btnDocNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDocNovoActionPerformed(evt);
            }
        });
        jBarDoc.add(btnDocNovo);

        btnDocExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDocExcluir.setText(bundle1.getString("KpiPlanoDeAcao_00047")); // NOI18N
        btnDocExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDocExcluirActionPerformed(evt);
            }
        });
        jBarDoc.add(btnDocExcluir);

        btnDocExcluirTudo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir_tudo.gif"))); // NOI18N
        btnDocExcluirTudo.setText(bundle1.getString("KpiIndicador_00081")); // NOI18N
        btnDocExcluirTudo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDocExcluirTudoActionPerformed(evt);
            }
        });
        jBarDoc.add(btnDocExcluirTudo);

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator1.setMaximumSize(new java.awt.Dimension(2, 20));
        jSeparator1.setMinimumSize(new java.awt.Dimension(2, 20));
        jSeparator1.setPreferredSize(new java.awt.Dimension(1, 0));
        jBarDoc.add(jSeparator1);

        btnDownload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_download.gif"))); // NOI18N
        btnDownload.setText(bundle1.getString("KpiPlanoDeAcao_00048")); // NOI18N
        btnDownload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownloadActionPerformed(evt);
            }
        });
        jBarDoc.add(btnDownload);

        pnlDocs.add(jBarDoc, java.awt.BorderLayout.NORTH);
        pnlDocs.add(tblDocumento, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle1.getString("KpiPlanoDeAcao_00050"), pnlDocs); // NOI18N

        pnlHistorico.setLayout(new java.awt.BorderLayout());

        pnlDetalheHistorico.setPreferredSize(new java.awt.Dimension(110, 110));
        pnlDetalheHistorico.setLayout(null);

        fldCriador.setEnabled(false);
        pnlDetalheHistorico.add(fldCriador);
        fldCriador.setBounds(20, 70, 390, 22);

        lblCriador.setText(bundle.getString("KpiPlanoDeAcao_00040")); // NOI18N
        lblCriador.setEnabled(false);
        pnlDetalheHistorico.add(lblCriador);
        lblCriador.setBounds(20, 50, 70, 20);

        fldDataCadastro.setBounds(new java.awt.Rectangle(20, 30, 180, 22));
        fldDataCadastro.setButtonWidth(0);
        fldDataCadastro.setDialog(true);
        fldDataCadastro.setEditable(false);
        fldDataCadastro.setEditableByCalendar(false);
        fldDataCadastro.setEnabled(false);
        fldDataCadastro.setUseLocale(true);
        fldDataCadastro.setWaitForCalendarDate(true);
        pnlDetalheHistorico.add(fldDataCadastro);
        fldDataCadastro.setBounds(20, 30, 180, 22);

        lblDataCadastro.setText(bundle.getString("KpiPlanoDeAcao_00044")); // NOI18N
        lblDataCadastro.setEnabled(false);
        pnlDetalheHistorico.add(lblDataCadastro);
        lblDataCadastro.setBounds(20, 10, 80, 20);

        fldDataTermino.setBounds(new java.awt.Rectangle(230, 30, 180, 22));
        fldDataTermino.setButtonWidth(0);
        fldDataTermino.setDialog(true);
        fldDataTermino.setEnabled(false);
        fldDataTermino.setUseLocale(true);
        fldDataTermino.setWaitForCalendarDate(true);
        pnlDetalheHistorico.add(fldDataTermino);
        fldDataTermino.setBounds(230, 30, 180, 22);

        lblDataTermino.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataTermino.setText(bundle.getString("KpiPlanoDeAcao_00013")); // NOI18N
        lblDataTermino.setEnabled(false);
        lblDataTermino.setFocusable(false);
        lblDataTermino.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDataTermino.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDataTermino.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlDetalheHistorico.add(lblDataTermino);
        lblDataTermino.setBounds(230, 10, 82, 20);

        lblLog.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblLog.setText("Hist�rico:");
        lblLog.setEnabled(false);
        lblLog.setFocusable(false);
        lblLog.setMaximumSize(new java.awt.Dimension(100, 16));
        lblLog.setMinimumSize(new java.awt.Dimension(100, 16));
        lblLog.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlDetalheHistorico.add(lblLog);
        lblLog.setBounds(20, 90, 140, 20);

        pnlHistorico.add(pnlDetalheHistorico, java.awt.BorderLayout.PAGE_START);

        pnlDescricaoHistorico.setLayout(new java.awt.BorderLayout());

        txtLogAcao.setColumns(20);
        txtLogAcao.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtLogAcao.setRows(10);
        txtLogAcao.setEnabled(false);
        jScrollPane1.setViewportView(txtLogAcao);

        pnlDescricaoHistorico.add(jScrollPane1, java.awt.BorderLayout.CENTER);
        pnlDescricaoHistorico.add(pnlLeftForm1, java.awt.BorderLayout.WEST);
        pnlDescricaoHistorico.add(pnlRightForm1, java.awt.BorderLayout.EAST);
        pnlDescricaoHistorico.add(pnlBottomForm1, java.awt.BorderLayout.SOUTH);

        pnlHistorico.add(pnlDescricaoHistorico, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab("Hist�rico", pnlHistorico);

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pack();
    }// </editor-fold>                        
    private void btnDocExcluirTudoActionPerformed(java.awt.event.ActionEvent evt) {                                                  
        int linha = 0;
        BIXMLRecord recLinha = null;
        String arquivo = "";
        kpi.swing.tools.KpiUpload frmUpload = new kpi.swing.tools.KpiUpload(0, "KPIUPLOAD", "");

        for (linha = (tblDocumento.getTable().getRowCount() - 1); linha >= 0; linha--) {
            recLinha = tblDocumento.getXMLData().get(linha);
            arquivo = recLinha.getString("DOCUMENTO");
            //removendo o arquivo da pasta de upload
            frmUpload.setPathDest(pasta_destino());
            frmUpload.setMoverParaDestino(false);
            frmUpload.setSubPastaUPL(subpasta_destino());
            frmUpload.removeArquivos(arquivo);
            //removendo o arquivo do gride
            tabPlan.addRecordDeleted(tblDocumento.getXMLData().get(linha));
            tblDocumento.removeRecord(linha);
        }
    }                                                 

	private void btnAjuda2ActionPerformed(java.awt.event.ActionEvent evt) {                                          
            event.loadHelp(recordType);
	}                                         

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {                                          
            event.deleteRecord();
            kpi.util.KpiToolKit tool = new kpi.util.KpiToolKit();
            tool.eraseServerFiles(this.getID(), "KPIUPLOAD", pasta_destino());
	}                                         

    private void btnDownloadActionPerformed(java.awt.event.ActionEvent evt) {                                            
        /*
         * Requisita a URL para download do documento.
         */
        KpiToolKit oToolKit = new KpiToolKit();
        BIXMLRecord recLinha = tblDocumento.getXMLData().get(tblDocumento.getTable().getFocusRow());
        oToolKit.browser("sgidownload.apw?file=" + pasta_destino() + recLinha.getString("DOCUMENTO"));

    }                                           

    private void btnDocExcluirActionPerformed(java.awt.event.ActionEvent evt) {                                              
        int Selecao[] = tblDocumento.getSelectedRows();
        int linhaSelecionada = 0;
        BIXMLRecord recLinha = null;
        String arquivo = "";
        kpi.swing.tools.KpiUpload frmUpload = new kpi.swing.tools.KpiUpload(0, "KPIUPLOAD", "");

        for (linhaSelecionada = (Selecao.length - 1); linhaSelecionada >= 0; linhaSelecionada--) {
            recLinha = tblDocumento.getXMLData().get(Selecao[linhaSelecionada]);
            arquivo = recLinha.getString("DOCUMENTO");
            //removendo o arquivo da pasta de upload
            frmUpload.setPathDest(pasta_destino());
            frmUpload.setMoverParaDestino(false);
            frmUpload.setSubPastaUPL(subpasta_destino());
            frmUpload.removeArquivos(arquivo);
            //removendo o arquivo do gride
            tabPlan.addRecordDeleted(tblDocumento.getXMLData().get(Selecao[linhaSelecionada]));
            tblDocumento.removeRecord(Selecao[linhaSelecionada]);
        }
    }                                             

    private void btnDocNovoActionPerformed(java.awt.event.ActionEvent evt) {                                           
        try {
            javax.swing.JFileChooser chooser = new javax.swing.JFileChooser();
            chooser.setDialogType(javax.swing.JFileChooser.CUSTOM_DIALOG);
            chooser.setApproveButtonText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00030")); //"Selecionar"
            chooser.setApproveButtonToolTipText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00031")); //"Selecionar Planilha"
            chooser.setAcceptAllFileFilterUsed(false);
            chooser.setMultiSelectionEnabled(true);
            int retval = chooser.showDialog(this, null);
            SimpleDateFormat dDataF = null;
            NumberFormat nf = null;

            if (retval == javax.swing.JFileChooser.APPROVE_OPTION) {
                kpi.swing.KpiDefaultFrameFunctions frame =
                        kpi.core.KpiStaticReferences.getKpiFormController().getForm("KPIUPLOAD", "0", "");
                kpi.swing.tools.KpiUpload frmUpload = (kpi.swing.tools.KpiUpload) frame;
                java.io.File oFile[] = chooser.getSelectedFiles();
                frmUpload.setPathDest(pasta_destino());
                frmUpload.setMoverParaDestino(false);
                frmUpload.setSubPastaUPL(subpasta_destino());
                frmUpload.startUpload(oFile);
                //adiciona os itens a tabela
                nf = NumberFormat.getInstance();
                nf.setMaximumFractionDigits(2);
                nf.setMinimumFractionDigits(2);
                nf.setGroupingUsed(true);
                dDataF = new SimpleDateFormat("dd/mm/yyyy HH:mm:ss");
                for (int arqs = 0; arqs < oFile.length; arqs++) {
                    kpi.xml.BIXMLRecord newRecord = tabPlan.createNewRecord();
                    newRecord.set("DOCUMENTO", oFile[arqs].getName());
                    newRecord.set("DATE", dDataF.format(hoje));
                    //Quando se altera a formata��o do campo tamanho, deve-se fazer o mesmo no m�todo refreshFields().
                    float tamanho = ((float) oFile[arqs].length() / (float) 1024);
                    newRecord.set("SIZE", nf.format(tamanho).toString() + " kb");
                    tblDocumento.addNewRecord(newRecord);
                }
            }
        } catch (java.security.AccessControlException e) {
            kpi.swing.KpiDefaultDialogSystem oDlg = new kpi.swing.KpiDefaultDialogSystem(this);
            if (oDlg.confirmMessage((java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiChangeImage_00010")), javax.swing.JOptionPane.WARNING_MESSAGE) == javax.swing.JOptionPane.YES_OPTION) { //"Arquivo de permiss�o n�o encontrado. \nDeseja salvar o arquivo?"
                kpi.applet.KpiMainPanel.openUrlPolitica();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }                                          

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {                                        
            btnSave.setEnabled(false);
            boolean salvar = true;
            boolean loadrecord = false;
            int arqs = 0;
            BIXMLRecord oXMLRecord;
            kpi.swing.tools.KpiUpload frmUpload = new kpi.swing.tools.KpiUpload(0, "KPIUPLOAD", "");

            if (getStatus() != KpiDefaultFrameBehavior.INSERTING) {
                //Notifica o criador caso a data final foi alterada
                record.set("EMAIL", false);
                if (dataFimOriginal.compareTo(fldDataFim.getCalendar()) != 0) {
                    kpi.swing.KpiDefaultDialogSystem dialogSystem = kpi.core.KpiStaticReferences.getKpiDialogSystem();

                    // Deseja notificar por email?
                    if (dialogSystem.confirmMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00041")) == javax.swing.JOptionPane.YES_OPTION) {
                        record.set("EMAIL", true);
                    } else {
                        salvar = false;
                    }
                }

                if (salvar && cbStatus.getSelectedIndex() == 5) {  //Cancelada

                    if (salvar = trataStatusCancelado()) {

                        // Solicita reflesh dos dados para atualiza��o do log de a��o.
                        loadrecord = true;
                    }
                } else if (salvar && cbStatus.getSelectedIndex() == 2) { // Executada

                    // Solicita reflesh dos dados para atualiza��o do log de a��o.
                    loadrecord = true;
                }
            }

            if (salvar) {
                boolean lValid = validateFields(new StringBuffer());

                if (lValid) {
                    dataFimOriginal = fldDataFim.getCalendar();
                    //apagando os arquivos que foram excluidos
                    kpi.util.KpiToolKit tool = new kpi.util.KpiToolKit();
                    BIXMLRecord oXML = tabPlan.getDelDados();

                    if (oXML != null) {
                        BIXMLVector oXMLVector = oXML.getBIXMLVector("EXCLUIDOS");
                        for (arqs = 0; arqs < oXMLVector.size(); arqs++) {
                            oXMLRecord = oXMLVector.get(arqs);
                            tool.eraseServerFiles(this.getID(), "KPIUPLOAD", pasta_destino() + oXMLRecord.getString("DOCUMENTO"));
                        }
                    }

                    //movendo os novos arquivos para a pasta definitiva
                    frmUpload.setPathDest(pasta_destino());
                    frmUpload.setMoverParaDestino(false);
                    frmUpload.setSubPastaUPL(subpasta_destino());
                    frmUpload.moveArqsDestino();
                }

                event.saveRecord();

                if (lValid) {
                    frmUpload.removeArquivos();
                    tabPlan.resetExcluidos();
                    setRenderTable();

                    /*
                     * loadrecord � verdadeiro quando o status for CANCELADA ou
                     * EXECUTADA.
                     */
                    if (loadrecord) {
                        event.loadRecord();
                    }
                }
            }
	}                                       

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {                                          
            event.cancelOperation();
            tabPlan.resetExcluidos();
            //setRenderTable(); //Render para tabela de documentos
	}                                         

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {                                        
            event.editRecord();
            //setRenderTable(); //Render para tabela de documentos
	}                                       

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {                                          
            event.loadRecord();
	}                                         

	private void txtCausaKeyReleased(java.awt.event.KeyEvent evt) {                                     
            this.lblCausaChar.setText(doMaxChar(evt, this.txtCausa, 500, true));
	}                                    

	private void txtInvestimentoKeyReleased(java.awt.event.KeyEvent evt) {                                            
            this.lblInvestimentoChar.setText(doMaxChar(evt, this.txtInvestimento, 255, true));
	}                                           

	private void txtDescricaoKeyReleased(java.awt.event.KeyEvent evt) {                                         
            this.lblDescricaoChar.setText(doMaxChar(evt, this.txtDescricao, 255, true));
	}                                        

	private void txtObjetivoKeyReleased(java.awt.event.KeyEvent evt) {                                        
            this.lblObjetivoChar.setText(doMaxChar(evt, this.txtObjetivo, 255, true));
	}                                       

	private void txtComoKeyReleased(java.awt.event.KeyEvent evt) {                                    
            this.lblComoChar.setText(doMaxChar(evt, this.txtComo, 255, true));
	}                                   

	private void txtResultadoKeyReleased(java.awt.event.KeyEvent evt) {                                         
            this.lblResultadoChar.setText(doMaxChar(evt, this.txtResultado, 255, true));
	}                                        

	private void txtObservacaoKeyReleased(java.awt.event.KeyEvent evt) {                                          
            this.lblObservacaoChar.setText(doMaxChar(evt, this.txtObservacao, 255, true));
	}                                         

        private void btnReiniciaActionPerformed(java.awt.event.ActionEvent evt) {                                            
            ReiniciaTarefa();
        }                                           

        private void cbStatusItemStateChanged(java.awt.event.ItemEvent evt) {                                          
            if (evt.getStateChange() == java.awt.event.ItemEvent.SELECTED) {

                String statusNovo = event.getComboValue(htbStatus, cbStatus, true);

                if (status != KpiDefaultFrameBehavior.NORMAL) {
                    if (statusNovo.equals(KpiStaticReferences.ACAO_EXECUTADA)) { //Executada
                        fldDataTermino.setDate(hoje);
                    } else {
                        fldDataTermino.setDate(null);
                    }
                }

                if (record.getBoolean("CTR_APROV_PLANACAO")) {
                    boolean isOk = true;

                    if (!record.getBoolean("ISADMIN")
                            && !record.getString("STATUS").equals(statusNovo)
                            && statusNovo.equals(KpiStaticReferences.ACAO_NAO_INICIADA)
                            && ((record.getString("STATUS").equals(KpiStaticReferences.ACAO_EM_EXECUCAO) && !record.getBoolean("GRUPOACAO_ISRESP"))
                            || (!record.getString("GRUPOACAO_STATUS").equals(KpiStaticReferences.GRUPOACAO_NAO_INICIADO)))) {

                        isOk = false;

                        SwingUtilities.invokeLater(new Runnable() {

                            public void run() {
                                ResourceBundle bundle = ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale());

                                String text;

                                if (record.getString("STATUS").equals(KpiStaticReferences.ACAO_EM_EXECUCAO) && !record.getBoolean("GRUPOACAO_ISRESP")) {
                                    text = bundle.getString("KpiPlanoDeAcao_00066").concat(bundle.getString("KpiPlanoDeAcao_00068"));
                                } else {
                                    text = bundle.getString("KpiPlanoDeAcao_00066").concat(bundle.getString("KpiPlanoDeAcao_00067"));
                                }

                                JOptionPane.showMessageDialog(null, text, java.util.ResourceBundle.getBundle("international").getString("KpiPlanoDeAcao_00001"), JOptionPane.WARNING_MESSAGE);
                                event.selectComboItem(cbStatus, event.getComboItem(record.getBIXMLVector("LSTSTATUS"), statusAnterior, false));
                            }
                        });
                    }


                    if (isOk
                            && record.getString("GRUPOACAO_STATUS").equals(KpiStaticReferences.GRUPOACAO_NAO_INICIADO)
                            && !record.getString("STATUS").equals(statusNovo)
                            && !statusNovo.equals(KpiStaticReferences.ACAO_NAO_INICIADA)
                            && !statusNovo.equals(KpiStaticReferences.ACAO_EM_EXECUCAO)) {

                        isOk = false;

                        SwingUtilities.invokeLater(new Runnable() {

                            public void run() {
                                ResourceBundle bundle = ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale());

                                String text = bundle.getString("KpiPlanoDeAcao_00069");

                                JOptionPane.showMessageDialog(null, text, java.util.ResourceBundle.getBundle("international").getString("KpiPlanoDeAcao_00001"), JOptionPane.WARNING_MESSAGE);
                                event.selectComboItem(cbStatus, event.getComboItem(record.getBIXMLVector("LSTSTATUS"), statusAnterior, false));
                            }
                        });
                    }

                    if (isOk) {
                        statusAnterior = statusNovo;
                    }

                } else {
                    statusAnterior = statusNovo;
                }
            }
        }                                         

        private void cbScorecardActionPerformed(java.awt.event.ActionEvent evt) {                                            
            if (!(tipoPlano.equals("2"))) {
                BIXMLRecord recIndFiltrados = event.listRecords("INDICADOR", "ID_SCOREC = '".concat(event.getComboValue(htbScorecard, cbScorecard)) + "'");
                event.populateCombo(recIndFiltrados.getBIXMLVector("INDICADORES"), "INDICADOR", htbIndFiltrados, cbIndicador);
            }
        }                                           

    private void ReiniciaTarefa() {

        Object[] options = {java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00064"),
            java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00065")};
        int nOption = javax.swing.JOptionPane.showOptionDialog(null,
                java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00061") + fldNome.getText().trim() + java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00062"),
                java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00063"), javax.swing.JOptionPane.YES_NO_OPTION,
                javax.swing.JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
        //Se reinicia tarefa
        if (nOption == 0) {
            lReiniciou = true;
            btnEdit.doClick();
            cbStatus.setSelectedIndex(1);
            btnSave.doClick();
            btnReinicia.setVisible(false);
            btnEdit.setVisible(true);
            lReiniciou = false;
            event.loadRecord();
        } else {
            return;
        }
    }

    private String doMaxChar(KeyEvent evt, JBITextArea oField, int nQtdMax, boolean lShowMsg) {
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // I18N
        if (evt != null && (evt.getKeyCode() == 127 || evt.getKeyCode() == 8 || evt.getKeyCode() == 37 || evt.getKeyCode() == 39 || evt.getKeyCode() == 40)) {
        } else {
            if (oField.getText().length() > nQtdMax) {
                if (lShowMsg) {
                    kpi.swing.KpiDefaultDialogSystem oDialog = new kpi.swing.KpiDefaultDialogSystem(this);
                    oDialog.informationMessage(bundle.getString("KpiPlanoDeAcao_00053")); //"Limite de caracteres excedido."
                }
                String sCampo = oField.getText().substring(0, nQtdMax);
                oField.setText(sCampo);
            }
        }

        StringBuilder oTxt = new StringBuilder();
        oTxt.append(bundle.getString("KpiPlanoDeAcao_00054")); //"Caracteres"
        oTxt.append(" [");
        oTxt.append(String.valueOf(oField.getText().length()));
        oTxt.append(" ");
        oTxt.append(bundle.getString("KpiPlanoDeAcao_00055")); //"de"
        oTxt.append(" ");
        oTxt.append(nQtdMax);
        oTxt.append("]");

        return oTxt.toString();
    }
    // Variables declaration - do not modify                     
    private javax.swing.JButton btnAjuda2;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnDocExcluir;
    private javax.swing.JButton btnDocExcluirTudo;
    private javax.swing.JButton btnDocNovo;
    private javax.swing.JButton btnDownload;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReinicia;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cbAssunto;
    private javax.swing.JComboBox cbIndicador;
    private javax.swing.JComboBox cbResponsavel;
    private javax.swing.JComboBox cbScorecard;
    private javax.swing.JComboBox cbStatus;
    private javax.swing.JComboBox cmbCategoria;
    private javax.swing.JTextField fldCriador;
    private pv.jfcx.JPVDatePlus fldDataCadastro;
    private pv.jfcx.JPVDatePlus fldDataFim;
    private pv.jfcx.JPVDatePlus fldDataInicio;
    private pv.jfcx.JPVDatePlus fldDataTermino;
    private pv.jfcx.JPVCurrency fldInvestimento;
    private pv.jfcx.JPVEdit fldNome;
    private javax.swing.ButtonGroup grpIndPrj;
    private javax.swing.JToolBar jBarDoc;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblAssunto;
    private javax.swing.JLabel lblCategoria;
    private javax.swing.JLabel lblCausa;
    private javax.swing.JLabel lblCausaChar;
    private javax.swing.JLabel lblComo;
    private javax.swing.JLabel lblComoChar;
    private javax.swing.JLabel lblCriador;
    private javax.swing.JLabel lblDataCadastro;
    private javax.swing.JLabel lblDataFim;
    private javax.swing.JLabel lblDataInicio;
    private javax.swing.JLabel lblDataTermino;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblDescricaoChar;
    private javax.swing.JLabel lblIndicador;
    private javax.swing.JLabel lblInvestimento;
    private javax.swing.JLabel lblInvestimentoChar;
    private javax.swing.JLabel lblLog;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblObjetivo;
    private javax.swing.JLabel lblObjetivoChar;
    private javax.swing.JLabel lblObservacao;
    private javax.swing.JLabel lblObservacaoChar;
    private javax.swing.JLabel lblRealizado;
    private javax.swing.JLabel lblResponsaveis;
    private javax.swing.JLabel lblResultado;
    private javax.swing.JLabel lblResultadoChar;
    private javax.swing.JLabel lblScorecard;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JLabel lblValorInvestimento;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomForm1;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlDescricaoHistorico;
    private javax.swing.JPanel pnlDetalheHistorico;
    private javax.swing.JPanel pnlDocs;
    private javax.swing.JPanel pnlHistorico;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftForm1;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightForm1;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JScrollPane scrComo;
    private javax.swing.JScrollPane scrFim;
    private javax.swing.JScrollPane scrInicio;
    private javax.swing.JScrollPane scrlCausa;
    private javax.swing.JScrollPane scrlDescricao;
    private javax.swing.JScrollPane scrlInvestimento;
    private javax.swing.JScrollPane scrlObjetivo;
    private javax.swing.JScrollPane scrlObservacao;
    private javax.swing.JScrollPane scrlResultado;
    private javax.swing.JSpinner spnRealizado;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private kpi.beans.JBIXMLTable tblDocumento;
    private kpi.beans.JBITreeSelection tsArvore;
    private kpi.beans.JBITreeSelection tsTreeResp;
    private kpi.beans.JBITextArea txtCausa;
    private kpi.beans.JBITextArea txtComo;
    private kpi.beans.JBITextArea txtDescricao;
    private kpi.beans.JBITextArea txtInvestimento;
    private javax.swing.JTextArea txtLogAcao;
    private kpi.beans.JBITextArea txtObjetivo;
    private kpi.beans.JBITextArea txtObservacao;
    private kpi.beans.JBITextArea txtResultado;
    // End of variables declaration                   
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    public void setTipoPlano(String value) {
        tipoPlano = value;
    }

    public void setProjeto(String value) {
        projetoId = value;
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        btnSave.setEnabled(true);
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);

    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    public String getParentID() {
        if (tipoPlano.equals("3")) {
            return String.valueOf(record.getString("ID_ASSUNTO").trim());
        } else if (tipoPlano.equals("2")) {
            return String.valueOf(record.getString("ID_PROJ").trim());
        } else {
            return "";
        }
    }

    @Override
    public String getFormType() {
        return formType;
    }

    public String getTipoPlano() {
        return tipoPlano;
    }

    public String getProjeto() {
        return projetoId;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public void loadRecord() {
        event.loadRecord();

    }

    private boolean trataStatusCancelado() {
        boolean retorno = true;
        kpi.swing.KpiDefaultDialogSystem dialogSystem = kpi.core.KpiStaticReferences.getKpiDialogSystem();
        String msgJustificativa = "";
        msgJustificativa = dialogSystem.inputDialog(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00042"), this.justificativa);
        if ((msgJustificativa != null) && !("".equals(msgJustificativa.trim()))) {
            this.justificativa = msgJustificativa;
            record.set("JUSTIFICA", msgJustificativa);
            btnEdit.setEnabled(false);
        } else {
            if (msgJustificativa != null) {
                dialogSystem.errorMessage(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00043"));
            }
            retorno = false;
        }
        return retorno;
    }

    private void setRenderTable() {
        int col = 0;

        tblDocumento.setHeaderHeight(20);
        tblDocumento.getColumn(0).setPreferredWidth(90);
        tblDocumento.getColumn(1).setPreferredWidth(20);
        tblDocumento.getColumn(2).setPreferredWidth(10);
        tblDocumento.getColumn(3).setPreferredWidth(90);
        tblDocumento.setRowHeight(20);
        tblDocumento.getTable().setColumnSelectionAllowed(true);
        for (col = 0; col < tblDocumento.getModel().getColumnCount(); col++) {
            tblDocumento.getColumn(col).setCellRenderer(cellRenderer);
        }
    }

}

/**
 *
 * @author Lucio Pelinson
 */
class KpiTabPlanoAcao {

    private kpi.xml.BIXMLAttributes attIndicador = new kpi.xml.BIXMLAttributes();
    private kpi.xml.BIXMLVector vctDados = null;
    private kpi.xml.BIXMLRecord recDelDados = null;

    public KpiTabPlanoAcao() {
        setVctDados(new kpi.xml.BIXMLVector("PLANODOCS", "PLANODOC"));
    }

    public kpi.xml.BIXMLVector getVctDados() {
        return vctDados;
    }

    public void setVctDados(kpi.xml.BIXMLVector vctDados) {
        this.vctDados = vctDados;
    }

    public kpi.xml.BIXMLRecord createNewRecord() {
        kpi.xml.BIXMLRecord newRecord = new kpi.xml.BIXMLRecord("PLANODOC");
        newRecord.set("ID", "N_E_W_");
        newRecord.set("ID_PLANO", "");
        newRecord.set("DOCUMENTO", "");
        newRecord.set("DESCRICAO", "");
        newRecord.set("DATE", "");
        newRecord.set("SIZE", "");

        return newRecord;
    }

    public void addRecordDeleted(kpi.xml.BIXMLRecord record) {
        if (!record.getString("ID").equals("N_E_W_")) {
            if (recDelDados == null) {
                recDelDados = new kpi.xml.BIXMLRecord("REG_EXCLUIDO");
                recDelDados.set(new kpi.xml.BIXMLVector("EXCLUIDOS", "EXCLUIDO"));
            }
            kpi.xml.BIXMLVector vctAddReg = recDelDados.getBIXMLVector("EXCLUIDOS");
            vctAddReg.add(record);
        }
    }

    public kpi.xml.BIXMLRecord getDelDados() {
        return recDelDados;
    }

    public void resetDados() {
        setVctDados(new kpi.xml.BIXMLVector("PLANODOCS", "PLANODOC"));
    }

    public void resetExcluidos() {
        recDelDados = null;
    }

    public void setCabecPeriodo() {
        resetDados();
        attIndicador = new kpi.xml.BIXMLAttributes();
        attIndicador.set("TIPO", "PLANODOC");
        attIndicador.set("RETORNA", "F");

        attIndicador.set("TAG000", "DOCUMENTO");
        attIndicador.set("CAB000", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00026")); //"Nome"
        attIndicador.set("CLA000", kpi.beans.JBIXMLTable.KPI_STRING);
        attIndicador.set("EDT000", "F");
        attIndicador.set("CUM000", "F");

        attIndicador.set("TAG001", "DATE");
        attIndicador.set("CAB001", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00027")); //"Data da cria��o"
        attIndicador.set("CLA001", kpi.beans.JBIXMLTable.KPI_STRING);
        attIndicador.set("EDT001", "F");
        attIndicador.set("CUM001", "F");

        attIndicador.set("TAG002", "SIZE");
        attIndicador.set("CAB002", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00028")); //"Tamanho"
        attIndicador.set("CLA002", kpi.beans.JBIXMLTable.KPI_STRING);
        attIndicador.set("EDT002", "F");
        attIndicador.set("CUM002", "F");

        attIndicador.set("TAG003", "DESCRICAO");
        attIndicador.set("CAB003", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiGeraPlanilha_00025")); //"Descri��o"
        attIndicador.set("CLA003", kpi.beans.JBIXMLTable.KPI_STRING);
        attIndicador.set("EDT003", "T");
        attIndicador.set("CUM003", "F");

        vctDados.setAttributes(attIndicador);
    }
}

class KpiPlanoDeAcaoCellRenderer extends javax.swing.table.DefaultTableCellRenderer {

    private kpi.beans.JBIXMLTable biTable = null;
    public final static java.awt.Color VERDE = new java.awt.Color(53, 120, 20);
    public final static java.awt.Color CINZA = java.awt.Color.DARK_GRAY;

    public KpiPlanoDeAcaoCellRenderer() {
    }

    public KpiPlanoDeAcaoCellRenderer(kpi.beans.JBIXMLTable table) {
        biTable = table;
    }

    // Sobreposicao para mudar o comportamento de pintura da celula.
    @Override
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        pv.jfcx.JPVEdit oRender = new pv.jfcx.JPVEdit();
        BIXMLRecord recLinha = null;
        int linha = biTable.getOriginalRow(row);
        /*
         * Preparando o objeto para renderiza��o.
         */
        oRender.setLocale(kpi.core.KpiStaticReferences.getKpiDefaultLocale());
        oRender.setLAF(false);
        oRender.setFont(new java.awt.Font("Tahoma", 0, 11));
        oRender.setBorderStyle(0);

        if (biTable != null) {
            column = biTable.getColumn(column).getModelIndex();
            recLinha = biTable.getXMLData().get(linha);

            renderString(oRender, value.toString(), column, row, recLinha);
            if (hasFocus) {
                oRender.setBackground(new java.awt.Color(212, 208, 200));
            }
        }
        return oRender;
    }

    private pv.jfcx.JPVEdit renderString(pv.jfcx.JPVEdit oRender, String valor, int column, int row, BIXMLRecord recLinha) {
        String colName = biTable.getColumnTag(column);

        oRender.setValue(valor);
        if (colName.equals("DESCRICAO")) {
            oRender.setForeground(VERDE);
        } else {
            oRender.setForeground(CINZA);
        }
        oRender.setAutoScroll(true);
        oRender.setLAF(false);
        return oRender;
    }
}
