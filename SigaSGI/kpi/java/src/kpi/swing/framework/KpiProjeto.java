package kpi.swing.framework;

import java.util.Calendar;
import kpi.beans.JBIXMLTable;
import kpi.core.IteMnuPermission;
import kpi.core.KpiMenuController;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.xml.BIXMLAttributes;
import kpi.xml.BIXMLException;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

public class KpiProjeto extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "PROJETO";
    private String formType = recordType;
    private String parentType = "LSTPROJETO";
    private boolean permissao = false;
    private int acaoPermissao = 0;
    private kpi.xml.BIXMLVector vctDados = null;
    private KpiProjetoTableCellRenderer cellRenderer;
    private kpi.beans.JBIXMLTable listaTable;
    java.util.Hashtable htbResponsavel = new java.util.Hashtable();
    java.util.Hashtable htbScorecard = new java.util.Hashtable();
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;
    private BIXMLVector vctFiltroUser;
    private BIXMLVector vctTreeUser;

    public KpiProjeto(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        
        KpiMenuController mnuControl = KpiStaticReferences.getMnuController();
        IteMnuPermission perm = mnuControl.getRuleByName("PLANOSACAO");

        //SEGURANCA
        if (perm.getPermissionValue(IteMnuPermission.TipoPermissao.VISUALIZACAO) != IteMnuPermission.Permissao.PERMITIDO) {
            tapCadastro.remove(listaAcoes);
        }
        
        event.defaultConstructor(operation, idAux, contextId);
        
        if (KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.BSC_MODE)){
	    tsArvore.setEntitySelection(KpiStaticReferences.CAD_OBJETIVO);
        }
    }

    @Override
    public void enableFields() {
        event.setEnableFields(pnlProjeto, true);
        event.setEnableFields(pnlPermissao, true);
        if (tapCadastro.getTabCount() == 3) {
            tapCadastro.setEnabledAt(1, false);
        }
    }

    @Override
    public void disableFields() {
        event.setEnableFields(pnlProjeto, false);
        event.setEnableFields(pnlPermissao, false);
        listaAcoes.btnView.setEnabled(true);
        if (tapCadastro.getTabCount() == 3) {
            tapCadastro.setEnabledAt(1, true);
        }
    }

    @Override
    public void refreshFields() {
        configuraAbas();
        configuraPermissao(); 
    }
   
    /**
     * Centraliza a configuracao da permissao de acesso.
     * @throws BIXMLException
     */
    private void configuraPermissao() throws BIXMLException {
        setPermissao(record.getBoolean("PERMISSAO"));
        setAcaoPermissao(record.getInt("ACAO_PERMISSAO"));
        setPermission();
    }

    /**
     * Centraliza a configura��o de todas as abas.
     * @throws BIXMLException
     */
    private void configuraAbas() throws BIXMLException {
        configuraAbaProjeto();
        configuraAbaAcoes();
        configuraAbaPermissao();
    }

    /**
     * Configura��o da aba permiss�o.
     * @throws BIXMLException
     */
    private void configuraAbaPermissao() throws BIXMLException {
        java.util.Vector vctResponsaveis = new java.util.Vector();
        kpi.xml.BIXMLVector vctResponsavel = record.getBIXMLVector("PERM_PROJETOS");
        for (int i = 0; i < vctResponsavel.size(); i++) {
            vctResponsaveis.add(vctResponsavel.get(i));
        }
        pnlPerAlteracao.setDataSource(record.getBIXMLVector("USUARIOS"), vctResponsavel, "PERM_PROJETOS", "PERM_PROJETO");
    }

    /**
     * Configura��o da aba A��es.
     * @throws BIXMLException
     */
    private void configuraAbaAcoes() throws BIXMLException {
        //Lista de A��es
        listaAcoes.setDataSource(record.getBIXMLVector("PLANOSACAO"), record.getString("ID") + "|2", "", this.event);
        setRenderTable();
        //Remove a coluna Indicador. 
        listaAcoes.xmlTable.getTable().getColumnModel().removeColumn(listaAcoes.xmlTable.getColumn(1));
    }

    /**
     * Configura��o da aba Projeto.
     * @throws BIXMLException
     */
    private void configuraAbaProjeto() throws BIXMLException {
        //Nome.
        fldNome.setText(record.getString("NOME"));
        //C�digo.
        fldDescCod.setText(record.getString("DESCCOD"));
        //Descri��o.
        txtDescricao.setText(record.getString("DESCRICAO"));
        //Tipo.
        fldTipo.setText(record.getString("DESCTIPO"));
        
        //Respons�vel.
        BIXMLVector vctUsuarios = record.getBIXMLVector("RESPONSAVEIS"); //NOI18N
        BIXMLVector vctTreeUser = record.getBIXMLVector("TREEUSERS");
        //popularCombo(vctUsuarios, "ID_RESP", htbResponsavel, cbResponsavel, null, "") ;
        event.populateCombo(vctUsuarios, "ID_RESP", htbResponsavel, cbResponsavel, "NOME", true);

        tsUsers.setVetor(vctUsuarios);
        tsUsers.setTree(vctTreeUser);
        
        //Scorecard.
        event.populateCombo(record.getBIXMLVector("SCORECARDS"), "ID_SCORE", htbScorecard, cbScorecard);
        tsArvore.setVetor(record.getBIXMLVector("SCORECARDS"));
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);
        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCCOD", fldDescCod.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("ID_SCORE", event.getComboValue(htbScorecard, cbScorecard));
        
          if (event.getComboValue(htbResponsavel, cbResponsavel).substring(0, 1).equals("0")) {
            recordAux.set("TP_RESP", "");
            recordAux.set("ID_RESP", "");
        } else {
            recordAux.set("TP_RESP", event.getComboValue(htbResponsavel, cbResponsavel).substring(0, 1));
            recordAux.set("ID_RESP", event.getComboValue(htbResponsavel, cbResponsavel).substring(1));
        }
        
        recordAux.set("DESCTIPO", fldTipo.getText());
        recordAux.set(pnlPerAlteracao.getXMLData());

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        if (fldNome.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiProjeto_00007"));
            retorno = false;
        } else if (cbScorecard.getSelectedIndex() <= 0) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiProjeto_00008"));
            retorno = false;
        }

        if (!retorno) {
            btnSave.setEnabled(true);

        }
        return retorno;
    }

    private void setPermission() {
        if (getStatus() == KpiDefaultFrameBehavior.INSERTING) {
            setPermissao(true);
        } else {
            if (this.isPermissao()) {
                btnEdit.setEnabled(true);
                btnDelete.setEnabled(true);
            } else {
                btnEdit.setEnabled(false);
                btnDelete.setEnabled(false);
                listaAcoes.btnNew.setEnabled(false);
                //Se o usu�rio tiver permiss�o no projeto pode incluir, um plano acao
                if (getAcaoPermissao() == 0) {
                    listaAcoes.setEnabled(false);
                } else if (getAcaoPermissao() == 1) {
                    listaAcoes.setEnabled(true);
                    listaAcoes.btnNew.setEnabled(true);
                }
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new javax.swing.JTabbedPane();
        pnlProjeto = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        fldNome = new pv.jfcx.JPVEdit();
        lblDescricao = new javax.swing.JLabel();
        cbScorecard = new javax.swing.JComboBox();
        lblScorecard = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescricao = new kpi.beans.JBITextArea();
        lblResponsavel = new javax.swing.JLabel();
        cbResponsavel = new javax.swing.JComboBox();
        lblTipo = new javax.swing.JLabel();
        fldTipo = new pv.jfcx.JPVEdit();
        lblDescCod = new javax.swing.JLabel();
        fldDescCod = new pv.jfcx.JPVEdit();
        tsArvore = new kpi.beans.JBITreeSelection();
        tsUsers = new kpi.beans.JBITreeSelection(kpi.beans.JBITreeSelection.TYPE_DEFAULT);
        pnlAcoes = new javax.swing.JPanel();
        listaAcoes = new kpi.beans.JBISeekListPanel();
        pnlPermissao = new javax.swing.JPanel();
        pnlPerAlteracao = new kpi.beans.JBISelectionPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnAjuda2 = new javax.swing.JButton();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiProjeto_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_projeto.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(460, 450));

        pnlProjeto.setPreferredSize(new java.awt.Dimension(200, 200));
        pnlProjeto.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("KpiProjeto_00002")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlProjeto.add(lblNome);
        lblNome.setBounds(10, 10, 85, 20);

        fldNome.setMaxLength(255);
        pnlProjeto.add(fldNome);
        fldNome.setBounds(10, 30, 400, 22);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("KpiProjeto_00003")); // NOI18N
        lblDescricao.setEnabled(false);
        lblDescricao.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlProjeto.add(lblDescricao);
        lblDescricao.setBounds(10, 210, 85, 20);

        cbScorecard.setEnabled(false);
        cbScorecard.setPreferredSize(new java.awt.Dimension(10, 10));
        pnlProjeto.add(cbScorecard);
        cbScorecard.setBounds(10, 190, 400, 22);

        lblScorecard.setForeground(new java.awt.Color(51, 51, 255));
        lblScorecard.setText(KpiStaticReferences.getKpiCustomLabels().getSco(kpi.core.KpiStaticReferences.CAD_OBJETIVO).concat(":"));
        lblScorecard.setEnabled(false);
        lblScorecard.setFocusable(false);
        lblScorecard.setPreferredSize(new java.awt.Dimension(55, 15));
        pnlProjeto.add(lblScorecard);
        lblScorecard.setBounds(10, 170, 400, 20);

        txtDescricao.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportView(txtDescricao);

        pnlProjeto.add(jScrollPane1);
        jScrollPane1.setBounds(10, 230, 400, 110);

        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        lblResponsavel.setText(bundle1.getString("KpiProjeto_00009")); // NOI18N
        lblResponsavel.setEnabled(false);
        lblResponsavel.setFocusable(false);
        lblResponsavel.setPreferredSize(new java.awt.Dimension(55, 15));
        pnlProjeto.add(lblResponsavel);
        lblResponsavel.setBounds(10, 130, 85, 18);

        cbResponsavel.setPreferredSize(new java.awt.Dimension(10, 10));
        pnlProjeto.add(cbResponsavel);
        cbResponsavel.setBounds(10, 150, 400, 22);

        lblTipo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTipo.setText(bundle1.getString("KpiProjeto_00010")); // NOI18N
        lblTipo.setEnabled(false);
        lblTipo.setMaximumSize(new java.awt.Dimension(100, 16));
        lblTipo.setMinimumSize(new java.awt.Dimension(100, 16));
        lblTipo.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlProjeto.add(lblTipo);
        lblTipo.setBounds(10, 90, 85, 20);

        fldTipo.setMaxLength(255);
        pnlProjeto.add(fldTipo);
        fldTipo.setBounds(10, 110, 400, 22);

        lblDescCod.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        java.util.ResourceBundle bundle2 = java.util.ResourceBundle.getBundle("international"); // NOI18N
        lblDescCod.setText(bundle2.getString("KpiProjeto_00011")); // NOI18N
        lblDescCod.setEnabled(false);
        lblDescCod.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDescCod.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDescCod.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlProjeto.add(lblDescCod);
        lblDescCod.setBounds(10, 50, 85, 20);

        fldDescCod.setMaxLength(255);
        pnlProjeto.add(fldDescCod);
        fldDescCod.setBounds(10, 70, 400, 22);

        tsArvore.setCombo(cbScorecard);
        tsArvore.setRoot(false);
        pnlProjeto.add(tsArvore);
        tsArvore.setBounds(410, 190, 30, 22);

        tsUsers.setCombo(cbResponsavel);
        tsUsers.setRoot(false);
        pnlProjeto.add(tsUsers);
        tsUsers.setBounds(410, 150, 30, 22);

        tapCadastro.addTab(bundle1.getString("KpiProjeto_00001"), pnlProjeto); // NOI18N
        pnlProjeto.getAccessibleContext().setAccessibleParent(pnlProjeto);

        pnlAcoes.setLayout(new java.awt.BorderLayout());
        pnlAcoes.add(listaAcoes, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle1.getString("KpiProjeto_00012"), pnlAcoes); // NOI18N

        pnlPermissao.setPreferredSize(new java.awt.Dimension(460, 263));
        pnlPermissao.setLayout(new java.awt.BorderLayout());
        pnlPermissao.add(pnlPerAlteracao, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("KpiProjeto_00006"), pnlPermissao); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 29));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnAjuda2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda2.setText(bundle1.getString("KpiGraphIndicador_00011")); // NOI18N
        btnAjuda2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjuda2ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda2);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        getContentPane().add(pnlTools, java.awt.BorderLayout.NORTH);
        getContentPane().add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);
        getContentPane().add(pnlRightRecord, java.awt.BorderLayout.EAST);
        getContentPane().add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        getAccessibleContext().setAccessibleName(bundle.getString("KpiProjeto_00001")); // NOI18N

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnAjuda2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjuda2ActionPerformed
            event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjuda2ActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            btnSave.setEnabled(false);
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            tapCadastro.setSelectedIndex(0);
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAjuda2;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cbResponsavel;
    private javax.swing.JComboBox cbScorecard;
    private pv.jfcx.JPVEdit fldDescCod;
    private pv.jfcx.JPVEdit fldNome;
    private pv.jfcx.JPVEdit fldTipo;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblDescCod;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblResponsavel;
    private javax.swing.JLabel lblScorecard;
    private javax.swing.JLabel lblTipo;
    private kpi.beans.JBISeekListPanel listaAcoes;
    private javax.swing.JPanel pnlAcoes;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private kpi.beans.JBISelectionPanel pnlPerAlteracao;
    private javax.swing.JPanel pnlPermissao;
    private javax.swing.JPanel pnlProjeto;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private kpi.beans.JBITreeSelection tsArvore;
    private kpi.beans.JBITreeSelection tsUsers;
    private kpi.beans.JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        btnSave.setEnabled(true);
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    public void filterTable(java.util.Calendar dataDe, java.util.Calendar dataAte) {
        BIXMLRecord recPlanoAcao;
        BIXMLVector vctFilterData = new BIXMLVector("PLANOSACAO", "PLANOACAO");
        java.util.GregorianCalendar greData,
                greDataDe = new java.util.GregorianCalendar(dataDe.get(Calendar.YEAR),
                dataDe.get(Calendar.MONTH),
                dataDe.get(Calendar.DAY_OF_MONTH)),
                greDataAte = new java.util.GregorianCalendar(dataAte.get(Calendar.YEAR),
                dataAte.get(Calendar.MONTH),
                dataAte.get(Calendar.DAY_OF_MONTH));

        for (int item = 0; item < vctDados.size(); item++) {
            recPlanoAcao = vctDados.get(item);
            greData = recPlanoAcao.getDate("DATAINICIO");
            if (greData.compareTo(greDataDe) >= 0 && greData.compareTo(greDataAte) <= 0) {
                vctFilterData.add(recPlanoAcao);
            }
        }

        vctFilterData.setAttributes(getTableAttributes());

        listaAcoes.setDataSource(vctFilterData, "0", "0", this.event);
        listaAcoes.btnReload.setVisible(false);
        listaAcoes.btnNew.setVisible(false);
        setRenderTable();
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    public boolean isPermissao() {
        return permissao;
    }

    public void setPermissao(boolean permissao) {
        this.permissao = permissao;
    }

    public int getAcaoPermissao() {
        return acaoPermissao;
    }

    public void setAcaoPermissao(int acaoPermissao) {
        this.acaoPermissao = acaoPermissao;
    }

    public void filterTable(String idIndicador) {
        BIXMLRecord recPlanoAcao;
        BIXMLVector vctFilterData = new BIXMLVector("PLANOSACAO", "PLANOACAO");

        for (int item = 0; item < vctDados.size(); item++) {
            recPlanoAcao = vctDados.get(item);
            if (recPlanoAcao.getString("ID_IND").equals(idIndicador)) {
                vctFilterData.add(recPlanoAcao);
            }
        }

        vctFilterData.setAttributes(getTableAttributes());

        listaAcoes.setDataSource(vctFilterData, "0", "0", this.event);
        listaAcoes.btnReload.setVisible(false);
        listaAcoes.btnNew.setVisible(false);
        setRenderTable();
    }

    public kpi.beans.JBIXMLTable getListaTable() {
        return listaTable;
    }

    public void setListaTable(kpi.beans.JBIXMLTable listaTable) {
        this.listaTable = listaTable;
    }

    private void setRenderTable() {
        String tipoColuna = null;
        listaAcoes.xmlTable.setHeaderHeight(20);
        listaAcoes.xmlTable.getColumn(0).setMaxWidth(30);
        setListaTable(listaAcoes.xmlTable);
        cellRenderer = new KpiProjetoTableCellRenderer(getListaTable());

        for (int col = 0; col < listaTable.getModel().getColumnCount(); col++) {
            tipoColuna = getListaTable().getColumnType(col);
            int colTipo = new Integer(tipoColuna).intValue();

            if (colTipo == JBIXMLTable.KPI_STRING) {
                getListaTable().getColumn(col).setCellRenderer(cellRenderer);
            }
        }
        listaAcoes.xmlTable.setSortColumn(1);
    }

    private BIXMLAttributes getTableAttributes() {
        BIXMLAttributes attributes = new BIXMLAttributes();
        attributes.set("TAG000", "IMG_OWNER");
        attributes.set("CAB000", "");
        attributes.set("CLA000", "6");

        attributes.set("TAG001", "NOME");
        attributes.set("CAB001", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00017"));
        attributes.set("CLA001", "4");

        //Descricao
        attributes.set("TAG002", "DESCRICAO");
        attributes.set("CAB002", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00026"));
        attributes.set("CLA002", "4");

        //Como
        attributes.set("TAG003", "COMO");
        attributes.set("CAB003", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00027"));
        attributes.set("CLA003", "4");

        //Responsavel
        attributes.set("TAG004", "RESPONSAVEL");
        attributes.set("CAB004", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00028"));
        attributes.set("CLA004", "4");

        //Responsavel
        attributes.set("TAG005", "DATAFIM");
        attributes.set("CAB005", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00029"));
        attributes.set("CLA005", "4");

        //Status
        attributes.set("TAG006", "STATUS");
        attributes.set("CAB006", "Status");
        attributes.set("CLA006", "4");

        //Observacao
        attributes.set("TAG007", "OBSERVACAO");
        attributes.set("CAB007", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiPlanoDeAcao_00030"));
        attributes.set("CLA007", "4");

        attributes.set("TIPO", "PLANOACAO");
        attributes.set("RETORNA", "F");

        return attributes;
    }
}

class KpiProjetoTableCellRenderer extends javax.swing.table.DefaultTableCellRenderer {

    private kpi.beans.JBIXMLTable biTable = null;
    public final static java.awt.Color VERMELHO = new java.awt.Color(234, 106, 106);
    public final static java.awt.Color AMARELO = new java.awt.Color(255, 235, 155);
    public final static java.awt.Color VERDE = new java.awt.Color(139, 191, 150);
    public final static java.awt.Color CINZA = new java.awt.Color(228, 228, 228);
    public final static int COL_STATUS_PLANO = 6;

    public KpiProjetoTableCellRenderer() {
    }

    public KpiProjetoTableCellRenderer(kpi.beans.JBIXMLTable table) {
        biTable = table;
    }

    @Override
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        pv.jfcx.JPVEdit oRender = new pv.jfcx.JPVEdit();
        BIXMLRecord recLinha = null;
        int linha = biTable.getOriginalRow(row);

        oRender.setLocale(kpi.core.KpiStaticReferences.getKpiDefaultLocale());
        oRender.setLAF(false);
        oRender.setBorderStyle(0);
        oRender.setFont(new java.awt.Font("Tahoma", 0, 11));

        if (biTable != null) {
            column = biTable.getColumn(column).getModelIndex();
            recLinha = biTable.getXMLData().get(linha);

            renderString(oRender, value.toString(), column, row, recLinha);
        }

        String colName = biTable.getColumnTag(column);
        if (!colName.equals(KpiListaPlanos.COL_STATUS_PLANO)) {
            if (hasFocus) {
                oRender.setBackground(new java.awt.Color(212, 208, 200));
            } else if (table.getSelectedRow() == row) {
                oRender.setBackground(table.getSelectionBackground());
                oRender.setForeground(table.getSelectionForeground());
            }
        }
        return oRender;
    }

    private pv.jfcx.JPVEdit renderString(pv.jfcx.JPVEdit oRender, String valor, int column, int row, BIXMLRecord recLinha) {
        oRender.setValue(valor);

        String colName = biTable.getColumnTag(column);
        if (colName.equals(KpiListaPlanos.COL_STATUS_PLANO)) {

            int status = recLinha.getInt("STATUS_COLOR");
            switch (status) {
                case 1:
                    oRender.setBackground(AMARELO);
                    break;
                case 2:
                    oRender.setBackground(java.awt.Color.white);
                    break;
                case 3:
                    oRender.setBackground(CINZA);
                    break;
                case 4:
                    oRender.setBackground(VERMELHO);
                    break;
                case 5:
                    oRender.setBackground(VERDE);
                    break;
            }
        }
        oRender.setAutoScroll(true);
        oRender.setLAF(false);

        return oRender;
    }
}
