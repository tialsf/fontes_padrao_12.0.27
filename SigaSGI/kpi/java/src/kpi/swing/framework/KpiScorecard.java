package kpi.swing.framework;

import javax.swing.JOptionPane;
import javax.swing.tree.*;
import kpi.core.KpiImageResources;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiTreeNode;
import kpi.xml.*;

public class KpiScorecard extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "SCORECARD";
    private String formType = recordType;
    private String parentType = recordType;
    private boolean lForceUpdate = false;
    private java.util.Hashtable htbResponsavel = new java.util.Hashtable();
    private java.util.Hashtable htbScoPai = new java.util.Hashtable();
    private BIXMLVector vctUsuarios;
    private BIXMLVector vctScoPai;
    private kpi.swing.kpiCreateTree buildTree = new kpi.swing.kpiCreateTree();

    public KpiScorecard(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);
        event.setFecharPainel(false);
        enableEditButtons(false);
    }

    @Override
    public void enableFields() {
        event.setEnableFields(pnlDetalhes, true);
        kpiTreeElemento.setEnabled(false);
    }

    @Override
    public void disableFields() {
        event.setEnableFields(pnlDetalhes, false);
        kpiTreeElemento.setEnabled(true);
    }

    @Override
    public void refreshFields() {
        fldNome.setText(record.getString("NOME"));
        fldDescricao.setText(record.getString("DESCRICAO"));
        vctUsuarios = record.getBIXMLVector("USUARIOS");
        vctScoPai = record.getBIXMLVector("LTS_SCO_PAIS");

        event.populateCombo(vctUsuarios, "RESPID", htbResponsavel, cmbResp);

        if (getStatus() != KpiDefaultFrameBehavior.INSERTING) {
            event.populateCombo(vctScoPai, "PARENTID", htbScoPai, cmbScoPai);
            tsScoPai.setVetor(vctScoPai);
            event.selectComboItem(cmbResp, event.getComboItem(vctUsuarios, record.getString("RESPID"), true));
            kpiTreeElemento.removeAll();
            kpiTreeElemento.setCellRenderer(new KpiScoreCardTreeCellRenderer(kpi.core.KpiStaticReferences.getKpiImageResources()));
            DefaultTreeCellRenderer kpiRender = (DefaultTreeCellRenderer) kpiTreeElemento.getCellRenderer();
            kpiRender.setClosedIcon(kpiRender.getLeafIcon());
            kpiRender.setOpenIcon(kpiRender.getLeafIcon());
            kpiTreeElemento.setModel(new javax.swing.tree.DefaultTreeModel(buildTree.createTree(record.getBIXMLVector("SCORECARDS"))));
            buildTree.expandPath(kpiTreeElemento, buildTree.getSelectedTreePath());
        } else {
            cmbResp.setSelectedIndex(0);
        }
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            String parentCode = event.getComboValue(htbScoPai, cmbScoPai);
            if (parentCode.equals("0") || parentCode.equals(id)) {
                parentCode = "";
            }
            recordAux.set("PARENTID", parentCode);
        }

        //Copia os campos do formul�rio para a v�riavel "recordAux" e devolve como retorno da fun��o.
        recordAux.set("NOME", fldNome.getText().trim());
        recordAux.set("DESCRICAO", fldDescricao.getText());
        recordAux.set("RESPID", event.getComboValue(htbResponsavel, cmbResp));
        recordAux.set("VISIVEL", chkVisivel.isSelected());

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        if (fldNome.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScorecard_00006"));
            retorno = false;

        } else if (fldNome.getText().indexOf(".") != -1 || fldNome.getText().indexOf("&") != -1) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScorecard_00007"));
            retorno = false;

        } else if (cmbResp.getSelectedIndex() == 0 || cmbResp.getSelectedIndex() == -1) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScorecard_00008"));
            retorno = false;

        } else if (fldDescricao.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScorecard_00009"));
            retorno = false;

        } else if (fldDescricao.getText().indexOf("&") != -1) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiScorecard_00010"));
            retorno = false;
        }

        if (!retorno) {
            btnSave.setEnabled(true);
        }

        return retorno;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlRecord = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        scpRecord = new javax.swing.JScrollPane();
        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollTree = new javax.swing.JScrollPane();
        kpiTreeElemento = new javax.swing.JTree();
        scrollRecord = new javax.swing.JScrollPane();
        jPanelRecord = new javax.swing.JPanel();
        pnlDetalhes = new javax.swing.JPanel();
        lblDescricao = new javax.swing.JLabel();
        lblResponsavel = new javax.swing.JLabel();
        lblNome = new javax.swing.JLabel();
        fldNome = new pv.jfcx.JPVEdit();
        cmbResp = new javax.swing.JComboBox();
        scrDescricao = new javax.swing.JScrollPane();
        fldDescricao = new javax.swing.JTextArea();
        cmbScoPai = new javax.swing.JComboBox();
        lblPai = new javax.swing.JLabel();
        tsScoPai = new kpi.beans.JBITreeSelection();
        pnlOpcoes = new javax.swing.JPanel();
        chkVisivel = new javax.swing.JCheckBox();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnNew = new javax.swing.JButton();
        btnDuplicar = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnOrdenar = new javax.swing.JButton();
        btnAjuda = new javax.swing.JButton();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle(KpiStaticReferences.getKpiCustomLabels().getSco());
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_scorecard.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(791, 411));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        scpRecord.setBorder(null);
        scpRecord.setPreferredSize(new java.awt.Dimension(500, 330));
        scpRecord.setRequestFocusEnabled(false);

        jSplitPane1.setBorder(null);
        jSplitPane1.setDividerLocation(200);
        jSplitPane1.setAutoscrolls(true);
        jSplitPane1.setContinuousLayout(true);
        jSplitPane1.setOneTouchExpandable(true);
        jSplitPane1.setPreferredSize(new java.awt.Dimension(200, 220));

        jScrollTree.setAutoscrolls(true);
        jScrollTree.setPreferredSize(new java.awt.Dimension(120, 60));

        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("Aguarde...");
        kpiTreeElemento.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        kpiTreeElemento.setMinimumSize(new java.awt.Dimension(10, 10));
        kpiTreeElemento.setRowHeight(18);
        kpiTreeElemento.addTreeWillExpandListener(new javax.swing.event.TreeWillExpandListener() {
            public void treeWillCollapse(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {
            }
            public void treeWillExpand(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {
                kpiTreeElementoTreeWillExpand(evt);
            }
        });
        kpiTreeElemento.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                kpiTreeElementoMouseClicked(evt);
            }
        });
        jScrollTree.setViewportView(kpiTreeElemento);

        jSplitPane1.setLeftComponent(jScrollTree);

        scrollRecord.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        scrollRecord.setPreferredSize(new java.awt.Dimension(300, 100));

        jPanelRecord.setToolTipText("");
        jPanelRecord.setPreferredSize(new java.awt.Dimension(330, 100));
        jPanelRecord.setRequestFocusEnabled(false);
        jPanelRecord.setLayout(new java.awt.BorderLayout());

        pnlDetalhes.setLayout(null);

        lblDescricao.setForeground(new java.awt.Color(51, 51, 255));
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        lblDescricao.setText(bundle.getString("KpiScorecard_00004")); // NOI18N
        lblDescricao.setEnabled(false);
        lblDescricao.setFocusable(false);
        pnlDetalhes.add(lblDescricao);
        lblDescricao.setBounds(20, 130, 80, 20);

        lblResponsavel.setForeground(new java.awt.Color(51, 51, 255));
        lblResponsavel.setText(bundle.getString("KpiScorecard_00005")); // NOI18N
        lblResponsavel.setEnabled(false);
        lblResponsavel.setFocusable(false);
        pnlDetalhes.add(lblResponsavel);
        lblResponsavel.setBounds(20, 50, 80, 20);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setText(bundle.getString("KpiScorecard_00003")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setFocusable(false);
        pnlDetalhes.add(lblNome);
        lblNome.setBounds(20, 10, 80, 20);

        fldNome.setEnabled(false);
        pnlDetalhes.add(fldNome);
        fldNome.setBounds(20, 30, 400, 22);

        cmbResp.setEnabled(false);
        pnlDetalhes.add(cmbResp);
        cmbResp.setBounds(20, 70, 400, 22);

        fldDescricao.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        fldDescricao.setLineWrap(true);
        fldDescricao.setEnabled(false);
        fldDescricao.setPreferredSize(new java.awt.Dimension(400, 50));
        scrDescricao.setViewportView(fldDescricao);

        pnlDetalhes.add(scrDescricao);
        scrDescricao.setBounds(20, 150, 402, 60);

        cmbScoPai.setEnabled(false);
        pnlDetalhes.add(cmbScoPai);
        cmbScoPai.setBounds(20, 110, 400, 22);

        lblPai.setForeground(new java.awt.Color(51, 51, 255));
        lblPai.setText(bundle.getString("KpiScorecard_00011")); // NOI18N
        lblPai.setEnabled(false);
        lblPai.setFocusable(false);
        pnlDetalhes.add(lblPai);
        lblPai.setBounds(20, 90, 80, 20);

        tsScoPai.setCombo(cmbScoPai);
        tsScoPai.setForceUpdate(true);
        pnlDetalhes.add(tsScoPai);
        tsScoPai.setBounds(420, 110, 30, 22);

        pnlOpcoes.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204))));
        pnlOpcoes.setLayout(null);

        chkVisivel.setText(bundle.getString("KpiIndicador_00013")); // NOI18N
        chkVisivel.setEnabled(false);
        chkVisivel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkVisivelActionPerformed(evt);
            }
        });
        pnlOpcoes.add(chkVisivel);
        chkVisivel.setBounds(10, 10, 150, 23);

        pnlDetalhes.add(pnlOpcoes);
        pnlOpcoes.setBounds(20, 230, 400, 40);

        jPanelRecord.add(pnlDetalhes, java.awt.BorderLayout.CENTER);

        scrollRecord.setViewportView(jPanelRecord);

        jSplitPane1.setRightComponent(scrollRecord);

        scpRecord.setViewportView(jSplitPane1);

        jTabbedPane1.addTab(bundle.getString("KpiScorecard_00001"), scpRecord); // NOI18N

        pnlRecord.add(jTabbedPane1, java.awt.BorderLayout.CENTER);

        getContentPane().add(pnlRecord, java.awt.BorderLayout.CENTER);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 29));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(235, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnNew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_novo.gif"))); // NOI18N
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnNew.setText(bundle1.getString("JBIListPanel_00001")); // NOI18N
        btnNew.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnNew.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnNew.setMaximumSize(new java.awt.Dimension(60, 22));
        btnNew.setMinimumSize(new java.awt.Dimension(60, 22));
        btnNew.setPreferredSize(new java.awt.Dimension(60, 22));
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });
        tbInsertion.add(btnNew);

        btnDuplicar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_duplicador.gif"))); // NOI18N
        btnDuplicar.setText("Duplicar");
        btnDuplicar.setFocusable(false);
        btnDuplicar.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnDuplicar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnDuplicar.setMaximumSize(new java.awt.Dimension(70, 22));
        btnDuplicar.setMinimumSize(new java.awt.Dimension(70, 22));
        btnDuplicar.setPreferredSize(new java.awt.Dimension(70, 22));
        btnDuplicar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnDuplicar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDuplicarActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDuplicar);

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle1.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnEdit.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnEdit.setMaximumSize(new java.awt.Dimension(60, 22));
        btnEdit.setMinimumSize(new java.awt.Dimension(60, 22));
        btnEdit.setPreferredSize(new java.awt.Dimension(60, 22));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle1.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnDelete.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnDelete.setMaximumSize(new java.awt.Dimension(60, 22));
        btnDelete.setMinimumSize(new java.awt.Dimension(60, 22));
        btnDelete.setPreferredSize(new java.awt.Dimension(60, 22));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle1.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnReload.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnReload.setMaximumSize(new java.awt.Dimension(70, 22));
        btnReload.setMinimumSize(new java.awt.Dimension(70, 22));
        btnReload.setPreferredSize(new java.awt.Dimension(70, 22));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnOrdenar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ordem.gif"))); // NOI18N
        btnOrdenar.setText(bundle.getString("KpiListaIndicador_00004")); // NOI18N
        btnOrdenar.setFocusable(false);
        btnOrdenar.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnOrdenar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnOrdenar.setMaximumSize(new java.awt.Dimension(70, 22));
        btnOrdenar.setMinimumSize(new java.awt.Dimension(70, 22));
        btnOrdenar.setPreferredSize(new java.awt.Dimension(70, 22));
        btnOrdenar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnOrdenar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOrdenarActionPerformed(evt);
            }
        });
        tbInsertion.add(btnOrdenar);

        btnAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        btnAjuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.CENTER);

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle1.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle1.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        getContentPane().add(pnlTools, java.awt.BorderLayout.NORTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
            event.loadHelp("SCORECARD");
	}//GEN-LAST:event_btnAjudaActionPerformed

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        final String scorecard = getID().equals("0") ? "" : getID();

        //Posicionando no pai quando e inclus�o.
        event.selectComboItem(cmbScoPai, event.getComboItem(vctScoPai, scorecard, true), true);

        //Habilita campos do formul�rio.
        fldNome.setText("");
        fldDescricao.setText("");
        cmbResp.setSelectedItem(0);
        chkVisivel.setSelected(true);

        //Habilita bot�es do formul�rio.
        this.showDataButtons();
        this.enableFields();

        //Desabilita a altera��o de pai.
        lblPai.setEnabled(false);
        cmbScoPai.setEnabled(false);
        tsScoPai.setEnabled(false);

        //For�a a gera��o de novo ID.
        setID("0");

        //Indica a opera��o a ser realizada.
        setStatus(KpiDefaultFrameBehavior.INSERTING);
    }//GEN-LAST:event_btnNewActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            disableFields();
            setStatus(KpiDefaultFrameBehavior.NORMAL);
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

    private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
        event.loadRecord();
    }//GEN-LAST:event_btnReloadActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        //TODO - Internacionalizar.
        final Object[] opcoes = {"Segura", "Recursiva", "Cancelar"};

        boolean atualiza = true;

        StringBuilder mensagem = new StringBuilder();
        mensagem.append("Segura - O scorecard ser� deletado apenas se n�o se relacionar com outro registro.");
        mensagem.append("\n");
        mensagem.append("Recursiva - O scorecard e todos os registro com os quais se relaciona ser�o deletados.");

        final int escolha = JOptionPane.showOptionDialog(this,
                mensagem.toString(),
                "M�todo de Exclus�o",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                opcoes,
                opcoes[0]);

        if (escolha == JOptionPane.YES_OPTION) {
            event.deleteRecord("NORMAL");

        } else if (escolha == JOptionPane.NO_OPTION) {
            event.deleteRecord("CASCATA");
        } else {
            atualiza = false;
        }

        if (atualiza) {
            setID("");
            event.loadRecord();
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        lForceUpdate = true;
        event.editRecord();
    }//GEN-LAST:event_btnEditActionPerformed

    private void kpiTreeElementoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_kpiTreeElementoMouseClicked
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) kpiTreeElemento.getLastSelectedPathComponent();

        if (node != null) {

            if (node.getUserObject() instanceof KpiTreeNode) {
                KpiTreeNode kpiNode = (KpiTreeNode) node.getUserObject();

                BIXMLRecord scorecard = event.listRecords("SCORECARD", "LISTA_SCORECARD_BY_ID".concat("|").concat(kpiNode.getID()));

                fldDescricao.setText(scorecard.getString("DESCRICAO"));
                fldNome.setText(scorecard.getString("NOME"));
                chkVisivel.setSelected(scorecard.getString("VISIVEL").equalsIgnoreCase("T"));

                event.selectComboItem(cmbResp, event.getComboItem(vctUsuarios, scorecard.getString("RESPID"), true), false);

                event.selectComboItem(cmbScoPai, event.getComboItem(vctScoPai, scorecard.getString("PARENTID"), true), false);

                setID(kpiNode.getID());

                enableEditButtons(!kpiNode.getID().equals("0"));

            } else {

                setID("0");
                cmbScoPai.setSelectedIndex(1);
                enableEditButtons(false);
            }
        }

        buildTree.setSelectedTreePath(kpiTreeElemento.getSelectionPath());
    }//GEN-LAST:event_kpiTreeElementoMouseClicked

    private void kpiTreeElementoTreeWillExpand(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {//GEN-FIRST:event_kpiTreeElementoTreeWillExpand
        StringBuilder parameters = new StringBuilder();
        DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) evt.getPath().getLastPathComponent();

        if (oMutTreeNode != null) {
            Object userObject = oMutTreeNode.getUserObject();

            if (userObject instanceof kpi.swing.KpiTreeNode) {
                KpiTreeNode oKpiTreeNode = (KpiTreeNode) userObject;

                //Defini�ao dos par�metros da requisi��o.
                parameters.append("LISTA_SCO_CHILD"); //Identificador.
                parameters.append("|");
                parameters.append(oKpiTreeNode.getID()); //Scorecard PAI.
                parameters.append("|");
                parameters.append(" "); //Pacote.
                parameters.append("|");
                parameters.append("F"); //Verificar acesso?

                //Requisita a lista de filhos do scorecard que foi expandido.
                BIXMLRecord recPlanos = event.listRecords(
                        "SCORECARD",
                        parameters.toString());

                //Insere din�micamente os scorecards no pai.
                buildTree.insertChild(recPlanos, oMutTreeNode);
            }
        }
    }//GEN-LAST:event_kpiTreeElementoTreeWillExpand

    private void btnDuplicarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDuplicarActionPerformed
        KpiStaticReferences.getKpiFormController().getForm("SCO_DUPLICADOR", this.getID(), "").asJInternalFrame();
    }//GEN-LAST:event_btnDuplicarActionPerformed

    private void btnOrdenarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOrdenarActionPerformed
        KpiStaticReferences.getKpiFormController().getForm("ORDEM_SCORECARD", this.getID(), fldNome.getText()).asJInternalFrame();
    }//GEN-LAST:event_btnOrdenarActionPerformed

    private void chkVisivelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkVisivelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkVisivelActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAjuda;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnDuplicar;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnOrdenar;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private javax.swing.JCheckBox chkVisivel;
    private javax.swing.JComboBox cmbResp;
    private javax.swing.JComboBox cmbScoPai;
    private javax.swing.JTextArea fldDescricao;
    private pv.jfcx.JPVEdit fldNome;
    private javax.swing.JPanel jPanelRecord;
    private javax.swing.JScrollPane jScrollTree;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTree kpiTreeElemento;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblPai;
    private javax.swing.JLabel lblResponsavel;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlDetalhes;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlOpcoes;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JScrollPane scrDescricao;
    private javax.swing.JScrollPane scrollRecord;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private kpi.beans.JBITreeSelection tsScoPai;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    @Override
    public void setStatus(int value) {
        if (lForceUpdate && getStatus() == KpiDefaultFrameBehavior.UPDATING) {
            event.loadRecord();
            lForceUpdate = false;
        }
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = value;
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        btnSave.setEnabled(true);
        pnlOperation.setVisible(false);
        pnlConfirmation.setVisible(true);
    }

    @Override
    public void showOperationsButtons() {
        pnlOperation.setVisible(true);
        pnlConfirmation.setVisible(false);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return new javax.swing.JTabbedPane();
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    @Override
    protected void validateTree() {
        super.validateTree();
    }

    private void enableEditButtons(boolean isEnable) {
        btnEdit.setEnabled(isEnable);
        btnDelete.setEnabled(isEnable);
        btnDuplicar.setEnabled(isEnable);

    }
}
/*
 *Renderiza��o cuistomizada para a �rvore do scorecarding
 */

class KpiScoreCardTreeCellRenderer extends kpi.swing.KpiTreeCellRenderer {

    private kpi.core.KpiImageResources myImageResources;

    public KpiScoreCardTreeCellRenderer(kpi.core.KpiImageResources imageResources) {
        super(imageResources);
        setUseMyRenderer(true);
        myImageResources = imageResources;
    }

    @Override
    public void myImageRenderer(Object value) {
        javax.swing.tree.DefaultMutableTreeNode nodeAux = (javax.swing.tree.DefaultMutableTreeNode) value;

        setOpenIcon(myImageResources.getImage(KpiImageResources.KPI_IMG_SCORECARD));
        setLeafIcon(myImageResources.getImage(KpiImageResources.KPI_IMG_SCORECARD));
        setIcon(myImageResources.getImage(KpiImageResources.KPI_IMG_SCORECARD));
        setClosedIcon(myImageResources.getImage(KpiImageResources.KPI_IMG_SCORECARD));

        setBackground(null);
        setBackgroundNonSelectionColor(null);
        setOpaque(false);
    }
}