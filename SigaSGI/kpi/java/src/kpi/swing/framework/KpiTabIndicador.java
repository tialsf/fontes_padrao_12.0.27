/*
 * KpiTabIndicador.java
 *
 * Created on 16 de Setembro de 2005, 11:32
 *
 */
package kpi.swing.framework;
import kpi.core.KpiStaticReferences;

/**
 *
 * @author Alexandre Alves da Silva
 */
public class KpiTabIndicador {

    private kpi.xml.BIXMLAttributes attIndicador = new kpi.xml.BIXMLAttributes();
    private kpi.xml.BIXMLVector vctDados = null;
    private kpi.xml.BIXMLRecord recDelDados = null;

    /**
     * Creates a new instance of KpiTabIndicador
     */
    public KpiTabIndicador() {
        setVctDados(new kpi.xml.BIXMLVector("PLANILHAS", "PLANILHA"));
    }

    public kpi.xml.BIXMLVector getVctDados() {
        return vctDados;
    }

    public void setVctDados(kpi.xml.BIXMLVector vctDados) {
        this.vctDados = vctDados;
    }

    public kpi.xml.BIXMLRecord createNewRecord() {
        kpi.xml.BIXMLRecord newRecord = new kpi.xml.BIXMLRecord("PLANILHA");
        newRecord.set("ID", "N_E_W_");
        newRecord.set("DIA", 0);
        newRecord.set("MES", 0);
        newRecord.set("ANO", 0);
        newRecord.set("VALOR", 0);
        newRecord.set("META", 0);
        newRecord.set("PREVIA", 0);
        newRecord.set("EDIT_LINE_REAL", "2");
        newRecord.set("EDIT_LINE_META", "2");
        newRecord.set("EDIT_LINE_PREVIA", "2");
        newRecord.set("DELETE_LINE", true);
        newRecord.set("UPDATED", "F");
        newRecord.set("LOG", "");
        return newRecord;
    }

    public void addRecordDeleted(kpi.xml.BIXMLRecord record) {
        if (!record.getString("ID").equals("N_E_W_")) {
            if (recDelDados == null) {
                recDelDados = new kpi.xml.BIXMLRecord("REG_EXCLUIDO");
                recDelDados.set(new kpi.xml.BIXMLVector("EXCLUIDOS", "EXCLUIDO"));
            }
            kpi.xml.BIXMLVector vctAddReg = recDelDados.getBIXMLVector("EXCLUIDOS");
            vctAddReg.add(record);
        }
    }

    public kpi.xml.BIXMLRecord getDelDados() {
        return recDelDados;
    }

    public void resetDados() {
        setVctDados(new kpi.xml.BIXMLVector("PLANILHAS", "PLANILHA"));
    }

    public void resetExcluidos() {
        recDelDados = null;
    }

    public void setCabecPeriodo(int iPeriodo, boolean showPrevia) {
        String cLog = "";

        resetDados();

        attIndicador = new kpi.xml.BIXMLAttributes();
        attIndicador.set("TIPO", "VALOR");
        attIndicador.set("RETORNA", "F");

        switch (iPeriodo) {
            case kpi.core.KpiStaticReferences.KPI_FREQ_ANUAL:
                //Coluna Ano.
                attIndicador.set("TAG000", "ANO");
                attIndicador.set("CAB000", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTabIndicador_00002"));
                attIndicador.set("CLA000", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT000", "T");
                attIndicador.set("CUM000", "F");
                //Coluna Valor.
                attIndicador.set("TAG001", "VALOR");
                attIndicador.set("CAB001", KpiStaticReferences.getKpiCustomLabels().getReal());
                attIndicador.set("CLA001", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT001", "T");
                attIndicador.set("CUM001", "F");
                //Coluna Meta.
                attIndicador.set("TAG002", "META");
                attIndicador.set("CAB002", KpiStaticReferences.getKpiCustomLabels().getMeta());
                attIndicador.set("CLA002", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT002", "T");
                attIndicador.set("CUM002", "F");

                if (showPrevia) {
                    //Coluna Previa.
                    attIndicador.set("TAG003", "PREVIA");
                    attIndicador.set("CAB003", KpiStaticReferences.getKpiCustomLabels().getPrevia());
                    attIndicador.set("CLA003", kpi.beans.JBIXMLTable.KPI_FLOAT);
                    attIndicador.set("EDT003", "T");
                    attIndicador.set("CUM003", "F");

                    cLog = "4";
                } else {
                    cLog = "3";
                }

                //Coluna Log.
                attIndicador.set("TAG00".concat(cLog), "LOG");
                attIndicador.set("CAB00".concat(cLog), "Log");
                attIndicador.set("CLA00".concat(cLog), kpi.beans.JBIXMLTable.KPI_STRING);
                attIndicador.set("EDT00".concat(cLog), "T");
                attIndicador.set("CUM00".concat(cLog), "F");
                break;
            case kpi.core.KpiStaticReferences.KPI_FREQ_SEMESTRAL:
                //Coluna Semestre.
                attIndicador.set("TAG000", "MES");
                attIndicador.set("CAB000", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTabIndicador_00005"));
                attIndicador.set("CLA000", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT000", "T");
                attIndicador.set("CUM000", "F");
                //Coluna Ano.
                attIndicador.set("TAG001", "ANO");
                attIndicador.set("CAB001", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTabIndicador_00002"));
                attIndicador.set("CLA001", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT001", "T");
                attIndicador.set("CUM001", "F");
                //Coluna Valor.
                attIndicador.set("TAG002", "VALOR");
                attIndicador.set("CAB002", KpiStaticReferences.getKpiCustomLabels().getReal());
                attIndicador.set("CLA002", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT002", "T");
                attIndicador.set("CUM002", "F");
                //Coluna Meta.
                attIndicador.set("TAG003", "META");
                attIndicador.set("CAB003", KpiStaticReferences.getKpiCustomLabels().getMeta());
                attIndicador.set("CLA003", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT003", "T");
                attIndicador.set("CUM003", "F");

                if (showPrevia) {
                    //Coluna Previa.
                    attIndicador.set("TAG004", "PREVIA");
                    attIndicador.set("CAB004", KpiStaticReferences.getKpiCustomLabels().getPrevia());
                    attIndicador.set("CLA004", kpi.beans.JBIXMLTable.KPI_FLOAT);
                    attIndicador.set("EDT004", "T");
                    attIndicador.set("CUM004", "F");

                    cLog = "5";
                } else {
                    cLog = "4";
                }

                //Coluna Log.
                attIndicador.set("TAG00".concat(cLog), "LOG");
                attIndicador.set("CAB00".concat(cLog), "Log");
                attIndicador.set("CLA00".concat(cLog), kpi.beans.JBIXMLTable.KPI_STRING);
                attIndicador.set("EDT00".concat(cLog), "T");
                attIndicador.set("CUM00".concat(cLog), "F");
                break;
            case kpi.core.KpiStaticReferences.KPI_FREQ_QUADRIMESTRAL:
                //Coluna Quadrimestre.
                attIndicador.set("TAG000", "MES");
                attIndicador.set("CAB000", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTabIndicador_00006"));
                attIndicador.set("CLA000", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT000", "T");
                attIndicador.set("CUM000", "F");
                //Coluna Ano.
                attIndicador.set("TAG001", "ANO");
                attIndicador.set("CAB001", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTabIndicador_00002"));
                attIndicador.set("CLA001", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT001", "T");
                attIndicador.set("CUM001", "F");
                //Coluna Valor.
                attIndicador.set("TAG002", "VALOR");
                attIndicador.set("CAB002", KpiStaticReferences.getKpiCustomLabels().getReal());
                attIndicador.set("CLA002", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT002", "T");
                attIndicador.set("CUM002", "F");
                //Coluna Meta.
                attIndicador.set("TAG003", "META");
                attIndicador.set("CAB003", KpiStaticReferences.getKpiCustomLabels().getMeta());
                attIndicador.set("CLA003", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT003", "T");
                attIndicador.set("CUM003", "F");

                if (showPrevia) {
                    //Coluna Previa.
                    attIndicador.set("TAG004", "PREVIA");
                    attIndicador.set("CAB004", KpiStaticReferences.getKpiCustomLabels().getPrevia());
                    attIndicador.set("CLA004", kpi.beans.JBIXMLTable.KPI_FLOAT);
                    attIndicador.set("EDT004", "T");
                    attIndicador.set("CUM004", "F");

                    cLog = "5";
                } else {
                    cLog = "4";
                }

                //Coluna Log.
                attIndicador.set("TAG00".concat(cLog), "LOG");
                attIndicador.set("CAB00".concat(cLog), "Log");
                attIndicador.set("CLA00".concat(cLog), kpi.beans.JBIXMLTable.KPI_STRING);
                attIndicador.set("EDT00".concat(cLog), "T");
                attIndicador.set("CUM00".concat(cLog), "F");
                break;
            case kpi.core.KpiStaticReferences.KPI_FREQ_TRIMESTRAL:
                //Coluna Trimestre.
                attIndicador.set("TAG000", "MES");
                attIndicador.set("CAB000", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTabIndicador_00007"));
                attIndicador.set("CLA000", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT000", "T");
                attIndicador.set("CUM000", "F");
                //Coluna Ano.
                attIndicador.set("TAG001", "ANO");
                attIndicador.set("CAB001", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTabIndicador_00002"));
                attIndicador.set("CLA001", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT001", "T");
                attIndicador.set("CUM001", "F");
                //Coluna Valor.
                attIndicador.set("TAG002", "VALOR");
                attIndicador.set("CAB002", KpiStaticReferences.getKpiCustomLabels().getReal());
                attIndicador.set("CLA002", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT002", "T");
                attIndicador.set("CUM002", "F");
                //Coluna Meta.
                attIndicador.set("TAG003", "META");
                attIndicador.set("CAB003", KpiStaticReferences.getKpiCustomLabels().getMeta());
                attIndicador.set("CLA003", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT003", "T");
                attIndicador.set("CUM003", "F");

                if (showPrevia) {
                    //Coluna Previa.
                    attIndicador.set("TAG004", "PREVIA");
                    attIndicador.set("CAB004", KpiStaticReferences.getKpiCustomLabels().getPrevia());
                    attIndicador.set("CLA004", kpi.beans.JBIXMLTable.KPI_FLOAT);
                    attIndicador.set("EDT004", "T");
                    attIndicador.set("CUM004", "F");

                    cLog = "5";
                } else {
                    cLog = "4";
                }

                //Coluna Log.
                attIndicador.set("TAG00".concat(cLog), "LOG");
                attIndicador.set("CAB00".concat(cLog), "Log");
                attIndicador.set("CLA00".concat(cLog), kpi.beans.JBIXMLTable.KPI_STRING);
                attIndicador.set("EDT00".concat(cLog), "T");
                attIndicador.set("CUM00".concat(cLog), "F");
                break;
            case kpi.core.KpiStaticReferences.KPI_FREQ_BIMESTRAL:
                //Coluna Bimestre.
                attIndicador.set("TAG000", "MES");
                attIndicador.set("CAB000", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTabIndicador_00008"));
                attIndicador.set("CLA000", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT000", "T");
                attIndicador.set("CUM000", "F");
                //Coluna Ano.
                attIndicador.set("TAG001", "ANO");
                attIndicador.set("CAB001", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTabIndicador_00002"));
                attIndicador.set("CLA001", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT001", "T");
                attIndicador.set("CUM001", "F");
                //Coluna Valor.
                attIndicador.set("TAG002", "VALOR");
                attIndicador.set("CAB002", KpiStaticReferences.getKpiCustomLabels().getReal());
                attIndicador.set("CLA002", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT002", "T");
                attIndicador.set("CUM002", "F");
                //Coluna Meta.
                attIndicador.set("TAG003", "META");
                attIndicador.set("CAB003", KpiStaticReferences.getKpiCustomLabels().getMeta());
                attIndicador.set("CLA003", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT003", "T");
                attIndicador.set("CUM003", "F");

                if (showPrevia) {
                    //Coluna Previa.
                    attIndicador.set("TAG004", "PREVIA");
                    attIndicador.set("CAB004", KpiStaticReferences.getKpiCustomLabels().getPrevia());
                    attIndicador.set("CLA004", kpi.beans.JBIXMLTable.KPI_FLOAT);
                    attIndicador.set("EDT004", "T");
                    attIndicador.set("CUM004", "F");

                    cLog = "5";
                } else {
                    cLog = "4";
                }

                //Coluna Log.
                attIndicador.set("TAG00".concat(cLog), "LOG");
                attIndicador.set("CAB00".concat(cLog), "Log");
                attIndicador.set("CLA00".concat(cLog), kpi.beans.JBIXMLTable.KPI_STRING);
                attIndicador.set("EDT00".concat(cLog), "T");
                attIndicador.set("CUM00".concat(cLog), "F");
                break;
            case kpi.core.KpiStaticReferences.KPI_FREQ_MENSAL:
                //Coluna Mes.
                attIndicador.set("TAG000", "MES");
                attIndicador.set("CAB000", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTabIndicador_00009"));
                attIndicador.set("CLA000", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT000", "T");
                attIndicador.set("CUM000", "F");
                attIndicador.set("RETORNA", "F");
                //Coluna Ano.
                attIndicador.set("TAG001", "ANO");
                attIndicador.set("CAB001", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTabIndicador_00002"));
                attIndicador.set("CLA001", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT001", "T");
                attIndicador.set("CUM001", "F");
                attIndicador.set("RETORNA", "F");
                //Coluna Valor.
                attIndicador.set("TAG002", "VALOR");
                attIndicador.set("CAB002", KpiStaticReferences.getKpiCustomLabels().getReal());
                attIndicador.set("CLA002", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT002", "T");
                attIndicador.set("CUM002", "F");
                //Coluna Meta.
                attIndicador.set("TAG003", "META");
                attIndicador.set("CAB003", KpiStaticReferences.getKpiCustomLabels().getMeta());
                attIndicador.set("CLA003", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT003", "T");
                attIndicador.set("CUM003", "F");

                if (showPrevia) {
                    //Coluna Previa.
                    attIndicador.set("TAG004", "PREVIA");
                    attIndicador.set("CAB004", KpiStaticReferences.getKpiCustomLabels().getPrevia());
                    attIndicador.set("CLA004", kpi.beans.JBIXMLTable.KPI_FLOAT);
                    attIndicador.set("EDT004", "T");
                    attIndicador.set("CUM004", "F");

                    cLog = "5";
                } else {
                    cLog = "4";
                }

                //Coluna Log.
                attIndicador.set("TAG00".concat(cLog), "LOG");
                attIndicador.set("CAB00".concat(cLog), "Log");
                attIndicador.set("CLA00".concat(cLog), kpi.beans.JBIXMLTable.KPI_STRING);
                attIndicador.set("EDT00".concat(cLog), "T");
                attIndicador.set("CUM00".concat(cLog), "F");
                break;
            case kpi.core.KpiStaticReferences.KPI_FREQ_QUINZENAL:
                //Quinzena
                attIndicador.set("TAG000", "DIA");
                attIndicador.set("CAB000", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTabIndicador_00010"));
                attIndicador.set("CLA000", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT000", "T");
                attIndicador.set("CUM000", "F");
                //Coluna Mes.
                attIndicador.set("TAG001", "MES");
                attIndicador.set("CAB001", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTabIndicador_00009"));
                attIndicador.set("CLA001", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT001", "T");
                attIndicador.set("CUM001", "F");
                //Coluna Ano.
                attIndicador.set("TAG002", "ANO");
                attIndicador.set("CAB002", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTabIndicador_00002"));
                attIndicador.set("CLA002", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT002", "T");
                attIndicador.set("CUM002", "F");
                //Coluna Valor.
                attIndicador.set("TAG003", "VALOR");
                attIndicador.set("CAB003", KpiStaticReferences.getKpiCustomLabels().getReal());
                attIndicador.set("CLA003", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT003", "T");
                attIndicador.set("CUM003", "F");
                //Coluna Meta.
                attIndicador.set("TAG004", "META");
                attIndicador.set("CAB004", KpiStaticReferences.getKpiCustomLabels().getMeta());
                attIndicador.set("CLA004", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT004", "T");
                attIndicador.set("CUM004", "F");

                if (showPrevia) {
                    //Coluna Previa.
                    attIndicador.set("TAG005", "PREVIA");
                    attIndicador.set("CAB005", KpiStaticReferences.getKpiCustomLabels().getPrevia());
                    attIndicador.set("CLA005", kpi.beans.JBIXMLTable.KPI_FLOAT);
                    attIndicador.set("EDT005", "T");
                    attIndicador.set("CUM005", "F");

                    cLog = "6";
                } else {
                    cLog = "5";
                }

                //Coluna Log.
                attIndicador.set("TAG00".concat(cLog), "LOG");
                attIndicador.set("CAB00".concat(cLog), "Log");
                attIndicador.set("CLA00".concat(cLog), kpi.beans.JBIXMLTable.KPI_STRING);
                attIndicador.set("EDT00".concat(cLog), "T");
                attIndicador.set("CUM00".concat(cLog), "F");
                break;
            case kpi.core.KpiStaticReferences.KPI_FREQ_SEMANAL:
                //Coluna Semana.
                attIndicador.set("TAG000", "MES");
                attIndicador.set("CAB000", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTabIndicador_00011"));
                attIndicador.set("CLA000", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT000", "T");
                attIndicador.set("CUM000", "F");
                //Coluna Ano.
                attIndicador.set("TAG001", "ANO");
                attIndicador.set("CAB001", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTabIndicador_00002"));
                attIndicador.set("CLA001", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT001", "T");
                attIndicador.set("CUM001", "F");
                //Coluna Valor.
                attIndicador.set("TAG002", "VALOR");
                attIndicador.set("CAB002", KpiStaticReferences.getKpiCustomLabels().getReal());
                attIndicador.set("CLA002", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT002", "T");
                attIndicador.set("CUM002", "F");
                //Coluna Meta.
                attIndicador.set("TAG003", "META");
                attIndicador.set("CAB003", KpiStaticReferences.getKpiCustomLabels().getMeta());
                attIndicador.set("CLA003", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT003", "T");
                attIndicador.set("CUM003", "F");

                if (showPrevia) {
                    //Coluna Previa.
                    attIndicador.set("TAG004", "PREVIA");
                    attIndicador.set("CAB004", KpiStaticReferences.getKpiCustomLabels().getPrevia());
                    attIndicador.set("CLA004", kpi.beans.JBIXMLTable.KPI_FLOAT);
                    attIndicador.set("EDT004", "T");
                    attIndicador.set("CUM004", "F");

                    cLog = "5";
                } else {
                    cLog = "4";
                }

                //Coluna Log.
                attIndicador.set("TAG00".concat(cLog), "LOG");
                attIndicador.set("CAB00".concat(cLog), "Log");
                attIndicador.set("CLA00".concat(cLog), kpi.beans.JBIXMLTable.KPI_STRING);
                attIndicador.set("EDT00".concat(cLog), "T");
                attIndicador.set("CUM00".concat(cLog), "F");
                break;
            case kpi.core.KpiStaticReferences.KPI_FREQ_DIARIA:
                //Dia
                attIndicador.set("TAG000", "DIA");
                attIndicador.set("CAB000", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTabIndicador_00012"));
                attIndicador.set("CLA000", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT000", "T");
                attIndicador.set("CUM000", "F");
                //Coluna Mes.
                attIndicador.set("TAG001", "MES");
                attIndicador.set("CAB001", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTabIndicador_00009"));
                attIndicador.set("CLA001", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT001", "T");
                attIndicador.set("CUM001", "F");
                //Coluna Ano.
                attIndicador.set("TAG002", "ANO");
                attIndicador.set("CAB002", java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTabIndicador_00002"));
                attIndicador.set("CLA002", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT002", "T");
                attIndicador.set("CUM002", "F");
                //Coluna Valor.
                attIndicador.set("TAG003", "VALOR");
                attIndicador.set("CAB003", KpiStaticReferences.getKpiCustomLabels().getReal());
                attIndicador.set("CLA003", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT003", "T");
                attIndicador.set("CUM003", "F");
                //Coluna Meta.
                attIndicador.set("TAG004", "META");
                attIndicador.set("CAB004", KpiStaticReferences.getKpiCustomLabels().getMeta());
                attIndicador.set("CLA004", kpi.beans.JBIXMLTable.KPI_FLOAT);
                attIndicador.set("EDT004", "T");
                attIndicador.set("CUM004", "F");

                if (showPrevia) {
                    //Coluna Previa.
                    attIndicador.set("TAG005", "PREVIA");
                    attIndicador.set("CAB005", KpiStaticReferences.getKpiCustomLabels().getPrevia());
                    attIndicador.set("CLA005", kpi.beans.JBIXMLTable.KPI_FLOAT);
                    attIndicador.set("EDT005", "T");
                    attIndicador.set("CUM005", "F");

                    cLog = "6";
                } else {
                    cLog = "5";
                }

                //Coluna Log.
                attIndicador.set("TAG00".concat(cLog), "LOG");
                attIndicador.set("CAB00".concat(cLog), "Log");
                attIndicador.set("CLA00".concat(cLog), kpi.beans.JBIXMLTable.KPI_STRING);
                attIndicador.set("EDT00".concat(cLog), "T");
                attIndicador.set("CUM00".concat(cLog), "F");
                break;
        }
        vctDados.setAttributes(attIndicador);
    }
}