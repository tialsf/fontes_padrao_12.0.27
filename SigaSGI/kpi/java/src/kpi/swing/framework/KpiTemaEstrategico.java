/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * KpiTemaEstrategico.java
 *
 * Created on 30/11/2011, 11:42:42
 */
package kpi.swing.framework;

import java.awt.event.MouseEvent;
import java.util.Enumeration;
import javax.swing.JCheckBox;
import javax.swing.JInternalFrame;
import javax.swing.JTabbedPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiDefaultFrameFunctions;
import kpi.swing.KpiSelectedTree;
import kpi.swing.KpiSelectedTreeCellRender;
import kpi.swing.KpiSelectedTreeNode;
import kpi.xml.BIXMLAttributes;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

/**
 *
 * @author tiago.tudisco
 */
public class KpiTemaEstrategico extends javax.swing.JInternalFrame implements KpiDefaultFrameFunctions {

    /** Creates new form KpiTemaEstrategico */
    private kpi.xml.BIXMLRecord record = null;
    private String recordType = new String("TEMAESTRATEGICO");
    private String formType = recordType;//Tipo do Formulario
    private String parentType = recordType;//Tipo formulario pai, caso haja.
    private kpi.swing.KpiDefaultDialogSystem dialog = kpi.core.KpiStaticReferences.getKpiDialogSystem();
    private String id = new String();
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private KpiDefaultFrameBehavior event = new KpiDefaultFrameBehavior(this);
    private BIXMLVector scorecards = new BIXMLVector();//Todos os scorecards.
    private String idEstrategia = "";

    public KpiTemaEstrategico(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true); //NOI18N

        event.defaultConstructor(operation, idAux, contextId);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlTema = new javax.swing.JPanel();
        tapCadastro = new javax.swing.JTabbedPane();
        pnlEst = new javax.swing.JPanel();
        pnlRecord = new javax.swing.JPanel();
        scrRecord = new javax.swing.JScrollPane();
        pnlEstrategia = new javax.swing.JPanel();
        txtNome = new pv.jfcx.JPVEdit();
        lblObjetivos = new javax.swing.JLabel();
        scrDescricao = new javax.swing.JScrollPane();
        txtDescricao = new kpi.beans.JBITextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        treeScorecard = new javax.swing.JTree();
        lblNome1 = new javax.swing.JLabel();
        lblDescricao = new javax.swing.JLabel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnAjuda = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international"); // NOI18N
        setTitle(bundle.getString("KpiTemaEstrategico_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_temaestrategico.gif"))); // NOI18N
        setPreferredSize(new java.awt.Dimension(500, 600));

        pnlTema.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTema.setPreferredSize(new java.awt.Dimension(10, 29));
        pnlTema.setLayout(new java.awt.BorderLayout());

        tapCadastro.setPreferredSize(new java.awt.Dimension(510, 345));

        pnlEst.setLayout(new java.awt.BorderLayout());

        pnlRecord.setLayout(new java.awt.BorderLayout());

        scrRecord.setBorder(null);

        pnlEstrategia.setLayout(null);

        txtNome.setMaxLength(120);
        pnlEstrategia.add(txtNome);
        txtNome.setBounds(10, 30, 400, 22);

        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        lblObjetivos.setText(bundle1.getString("KpiTemaEstrategico_00004")); // NOI18N
        lblObjetivos.setEnabled(false);
        pnlEstrategia.add(lblObjetivos);
        lblObjetivos.setBounds(10, 140, 60, 20);
        lblObjetivos.getAccessibleContext().setAccessibleName(bundle.getString("KpiTemaEstrategico_00004")); // NOI18N

        scrDescricao.setViewportView(txtDescricao);

        pnlEstrategia.add(scrDescricao);
        scrDescricao.setBounds(10, 70, 400, 60);

        treeScorecard.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("Aguarde...");
        treeScorecard.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        treeScorecard.setRootVisible(false);
        treeScorecard.setRowHeight(18);
        treeScorecard.setShowsRootHandles(true);
        treeScorecard.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                treeScorecardMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(treeScorecard);

        pnlEstrategia.add(jScrollPane2);
        jScrollPane2.setBounds(10, 160, 400, 350);

        lblNome1.setForeground(new java.awt.Color(51, 51, 255));
        lblNome1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome1.setText(bundle1.getString("KpiTemaEstrategico_00002")); // NOI18N
        lblNome1.setEnabled(false);
        lblNome1.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome1.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome1.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlEstrategia.add(lblNome1);
        lblNome1.setBounds(10, 10, 58, 20);
        lblNome1.getAccessibleContext().setAccessibleName(bundle.getString("KpiTemaEstrategico_00002")); // NOI18N

        lblDescricao.setText(bundle1.getString("KpiTemaEstrategico_00003")); // NOI18N
        lblDescricao.setEnabled(false);
        pnlEstrategia.add(lblDescricao);
        lblDescricao.setBounds(10, 50, 60, 20);

        scrRecord.setViewportView(pnlEstrategia);

        pnlRecord.add(scrRecord, java.awt.BorderLayout.CENTER);

        pnlEst.add(pnlRecord, java.awt.BorderLayout.CENTER);

        java.util.ResourceBundle bundle2 = java.util.ResourceBundle.getBundle("international_pt_br"); // NOI18N
        tapCadastro.addTab(bundle2.getString("KpiTemaEstrategico_00001"), pnlEst); // NOI18N

        pnlTema.add(tapCadastro, java.awt.BorderLayout.CENTER);
        tapCadastro.getAccessibleContext().setAccessibleName(bundle.getString("KpiTemaEstrategico_00001")); // NOI18N

        getContentPane().add(pnlTema, java.awt.BorderLayout.CENTER);

        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 29));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle1.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle1.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(340, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(250, 32));

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle1.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle1.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle1.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        java.util.ResourceBundle bundle3 = java.util.ResourceBundle.getBundle("international",kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnAjuda.setText(bundle3.getString("KpiGraphIndicador_00011")); // NOI18N
        btnAjuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        getContentPane().add(pnlTools, java.awt.BorderLayout.NORTH);

        getAccessibleContext().setAccessibleName(bundle.getString("KpiTemaEstrategico_00001")); // NOI18N

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void treeScorecardMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_treeScorecardMouseClicked
        
        if (status == KpiDefaultFrameBehavior.UPDATING || status == KpiDefaultFrameBehavior.INSERTING){

            int x = evt.getX();
            int y = evt.getY();
            int somaX = new JCheckBox().getPreferredSize().width;

            int row = treeScorecard.getRowForLocation(x, y);
            TreePath path = treeScorecard.getPathForRow(row);

            if (path != null) {
                if (x <= treeScorecard.getPathBounds(path).x + somaX) {
                    DefaultMutableTreeNode nodeAux = (DefaultMutableTreeNode) path.getLastPathComponent();
                    KpiSelectedTreeNode node = (KpiSelectedTreeNode) nodeAux.getUserObject();

                    if (node.isEnabled()) {

                        boolean bSelected = !(node.isSelected());
                        node.setSelected(bSelected);

                        if (evt.getButton() == MouseEvent.BUTTON1) {
                            selectedFromRoot(nodeAux, bSelected);
                        }

                        ((DefaultTreeModel) treeScorecard.getModel()).nodeChanged(node);

                        treeScorecard.revalidate();
                        treeScorecard.repaint();
                    }
                }
            }
        }

    }//GEN-LAST:event_treeScorecardMouseClicked

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed

        btnSave.setEnabled(false);         event.saveRecord();     }//GEN-LAST:event_btnSaveActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed

        event.cancelOperation();     }//GEN-LAST:event_btnCancelActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed

        tapCadastro.setSelectedIndex(0);         event.editRecord();     }//GEN-LAST:event_btnEditActionPerformed

        private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed

        event.loadRecord();     }//GEN-LAST:event_btnReloadActionPerformed

    private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed

        event.loadHelp(recordType);     }//GEN-LAST:event_btnAjudaActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        event.deleteRecord();
    }//GEN-LAST:event_btnDeleteActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAjuda;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblNome1;
    private javax.swing.JLabel lblObjetivos;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlEst;
    private javax.swing.JPanel pnlEstrategia;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlTema;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JScrollPane scrDescricao;
    private javax.swing.JScrollPane scrRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private javax.swing.JTree treeScorecard;
    private kpi.beans.JBITextArea txtDescricao;
    private pv.jfcx.JPVEdit txtNome;
    // End of variables declaration//GEN-END:variables

    public void enableFields() {
        event.setEnableFields(pnlEst, true);
        event.setEnableFields(pnlOperation, false);
        event.setEnableFields(pnlConfirmation, true);
    }

    public void disableFields() {
        event.setEnableFields(pnlEst, false);
        event.setEnableFields(pnlOperation, true);
        event.setEnableFields(pnlConfirmation, false);
    }

    public void refreshFields() {
        txtNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        setListaScorecard();


    }

    public void loadRecord() {
        event.loadRecord();
    }

    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setParentID(String idParent) {
        idEstrategia = idParent;
    }

    public String getParentID() {
        return idEstrategia;
    }

    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(recordType);
    }

    public String getFormType() {
        return formType;
    }

    public String getParentType() {
        return parentType;
    }

    public void setRecord(BIXMLRecord recordAux) {
        if ("COMMANDS".equals(recordAux.getTagName())) { //NOI18N
            BIXMLAttributes att = recordAux.getAttributes();
            idEstrategia = att.getString("PARENTID");
        } else {
            record = recordAux;
        }
    }

    public BIXMLRecord getRecord() {
        return record;
    }

    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;
        
        if (txtNome.getText().trim().equals("")){
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiTemaEstrategico_00005"));
	    retorno = false;
        }
        
        if (!retorno) {
	    btnSave.setEnabled(true);
	}
        
        return retorno;
    }

    public JInternalFrame asJInternalFrame() {
        return this;
    }

    public JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    public BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);

        if (idEstrategia.equals("0")) { //NOI18N
            recordAux.set("ESTRAT_ID", ""); //NOI18N
        } else {
            recordAux.set("ESTRAT_ID", idEstrategia); //NOI18N
        }

        recordAux.set("NOME", txtNome.getText()); //NOI18N
        recordAux.set("DESCRICAO", txtDescricao.getText()); //NOI18N
        recordAux.set(getVectorRowsSelected(scorecards, "SELECTED", "SCORECARDS", "SCORECARD")); //NOI18N

        return recordAux;
    }

    public BIXMLVector getVectorRowsSelected(BIXMLVector vctAllData, String tagSelecionar, String tagVector, String tagRecord) {

        DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) treeScorecard.getModel().getRoot();
        BIXMLVector vctSco = new BIXMLVector(tagVector, tagRecord);
        KpiSelectedTreeNode oNode;

        while (oMutTreeNode != null) {
            if (!oMutTreeNode.isRoot()) {
                oNode = (KpiSelectedTreeNode) oMutTreeNode.getUserObject();
                if (oNode.isSelected()) {
                    kpi.xml.BIXMLRecord oRec = new kpi.xml.BIXMLRecord(tagRecord);
                    oRec.set("ID", oNode.getID()); //NOI18N
                    oRec.set("SELECTED", oNode.isSelected()); //NOI18N
                    vctSco.add(oRec);
                }
            }
            oMutTreeNode = (DefaultMutableTreeNode) oMutTreeNode.getNextNode();
        }
        return vctSco;
    }

    private void setListaScorecard() {

        scorecards = record.getBIXMLVector("SCORECARDS"); //NOI18N

        treeScorecard.setModel(new javax.swing.tree.DefaultTreeModel(KpiSelectedTree.createTree(scorecards)));
        treeScorecard.setCellRenderer(new KpiSelectedTreeCellRender(kpi.core.KpiStaticReferences.getKpiImageResources()));
        treeScorecard.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        KpiSelectedTree.expandAll(treeScorecard, true);
    }

    private void selectedFromRoot(DefaultMutableTreeNode oNode, boolean bSelected) {

        Enumeration e = oNode.children();
        while (e.hasMoreElements()) {
            DefaultMutableTreeNode nodeAux = (DefaultMutableTreeNode) e.nextElement();
            KpiSelectedTreeNode node = (KpiSelectedTreeNode) nodeAux.getUserObject();
            node.setSelected(bSelected);
            selectedFromRoot(nodeAux, bSelected);
        }
    }
}
