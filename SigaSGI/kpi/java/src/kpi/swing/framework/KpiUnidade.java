package kpi.swing.framework;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import kpi.beans.JBITextArea;
import kpi.core.KpiStaticReferences;
import kpi.xml.BIXMLRecord;
import pv.jfcx.JPVEdit;

public class KpiUnidade extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "UNIDADE";
    private String formType = recordType;//Tipo do Formulario
    private String parentType = "LSTUNIDADE";//Tipo formulario pai, caso haja.

    public KpiUnidade(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);

        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
        event.setEnableFields(pnlUnidade, true);
    }

    @Override
    public void disableFields() {
        event.setEnableFields(pnlUnidade, false);
    }

    @Override
    public void refreshFields() {
        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
    }

    @Override
    public BIXMLRecord getFieldsContents() {
        BIXMLRecord recordAux = new BIXMLRecord(recordType);
        recordAux.set("ID", id);

        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        if (fldNome.getText().trim().equals("")) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiUnidade_00005"));
            retorno = false;
        }

        if (!retorno) {
            btnSave.setEnabled(true);
        }
        return retorno;
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        tapCadastro = new JTabbedPane();
        pnlRecord = new JPanel();
        scpRecord = new JScrollPane();
        pnlUnidade = new JPanel();
        lblNome = new JLabel();
        fldNome = new JPVEdit();
        lblDescricao = new JLabel();
        scrDescricao = new JScrollPane();
        txtDescricao = new JBITextArea();
        pnlBottomForm = new JPanel();
        pnlRightForm = new JPanel();
        pnlLeftForm = new JPanel();
        pnlTools = new JPanel();
        pnlConfirmation = new JPanel();
        tbConfirmation = new JToolBar();
        btnSave = new JButton();
        btnCancel = new JButton();
        pnlOperation = new JPanel();
        tbInsertion = new JToolBar();
        btnEdit = new JButton();
        btnDelete = new JButton();
        btnReload = new JButton();
        btnAjuda = new JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiUnidade_00001")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_unidade.gif"))); // NOI18N
        setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(480, 312));

        pnlRecord.setLayout(new BorderLayout());

        scpRecord.setBorder(null);

        pnlUnidade.setLayout(null);

        lblNome.setForeground(new Color(51, 51, 255));
        lblNome.setHorizontalAlignment(SwingConstants.LEFT);
        lblNome.setText(bundle.getString("KpiUnidade_00003")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new Dimension(100, 16));
        lblNome.setMinimumSize(new Dimension(100, 16));
        lblNome.setPreferredSize(new Dimension(100, 16));
        pnlUnidade.add(lblNome);
        lblNome.setBounds(10, 10, 58, 20);
        pnlUnidade.add(fldNome);
        fldNome.setBounds(10, 30, 400, 22);

        lblDescricao.setText(bundle.getString("KpiUnidade_00004")); // NOI18N
        lblDescricao.setEnabled(false);
        pnlUnidade.add(lblDescricao);
        lblDescricao.setBounds(10, 50, 60, 20);

        scrDescricao.setViewportView(txtDescricao);

        pnlUnidade.add(scrDescricao);
        scrDescricao.setBounds(10, 70, 400, 60);

        scpRecord.setViewportView(pnlUnidade);

        pnlRecord.add(scpRecord, BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("KpiUnidade_00002"), pnlRecord); // NOI18N

        getContentPane().add(tapCadastro, BorderLayout.CENTER);
        getContentPane().add(pnlBottomForm, BorderLayout.SOUTH);
        getContentPane().add(pnlRightForm, BorderLayout.EAST);
        getContentPane().add(pnlLeftForm, BorderLayout.WEST);

        pnlTools.setMinimumSize(new Dimension(480, 27));
        pnlTools.setPreferredSize(new Dimension(10, 29));
        pnlTools.setLayout(new BorderLayout());

        pnlConfirmation.setMaximumSize(new Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new Dimension(155, 27));
        pnlConfirmation.setLayout(new BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new Dimension(150, 32));

        btnSave.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, BorderLayout.EAST);

        pnlOperation.setMaximumSize(new Dimension(340, 27));
        pnlOperation.setMinimumSize(new Dimension(340, 27));
        pnlOperation.setPreferredSize(new Dimension(340, 27));
        pnlOperation.setLayout(new BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new Dimension(250, 32));

        btnEdit.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnAjuda.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        ResourceBundle bundle1 = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnAjuda.setText(bundle1.getString("KpiGraphIndicador_00011")); // NOI18N
        btnAjuda.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda);

        pnlOperation.add(tbInsertion, BorderLayout.CENTER);

        pnlTools.add(pnlOperation, BorderLayout.WEST);

        getContentPane().add(pnlTools, BorderLayout.NORTH);

        pack();
    }//GEN-END:initComponents

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
            event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjudaActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            btnSave.setEnabled(false);
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnAjuda;
    private JButton btnCancel;
    private JButton btnDelete;
    private JButton btnEdit;
    private JButton btnReload;
    private JButton btnSave;
    private JPVEdit fldNome;
    private JLabel lblDescricao;
    private JLabel lblNome;
    private JPanel pnlBottomForm;
    private JPanel pnlConfirmation;
    private JPanel pnlLeftForm;
    private JPanel pnlOperation;
    private JPanel pnlRecord;
    private JPanel pnlRightForm;
    private JPanel pnlTools;
    private JPanel pnlUnidade;
    private JScrollPane scpRecord;
    private JScrollPane scrDescricao;
    private JTabbedPane tapCadastro;
    private JToolBar tbConfirmation;
    private JToolBar tbInsertion;
    private JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public String getParentType() {
        return parentType;
    }
}
