package kpi.swing.framework;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import kpi.beans.JBIXMLTable;
import kpi.core.KpiStaticReferences;
import kpi.xml.BIXMLVector;

public class kpiOrdemIndicador extends javax.swing.JInternalFrame implements kpi.swing.KpiDefaultFrameFunctions {

    private String recordType = "ORDEM_INDICADOR";
    private String formType = "INDICADOR";
    private String parentType = "LSTINDICADOR";
    private BIXMLVector vctScoreCard = null;
    public final static int COL_ORDEM = 00;
    public final static int COL_INDICADOR = 01;
    public final static int COL_ID = 02;

    public kpiOrdemIndicador(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true);
        event.defaultConstructor(operation, idAux, contextId);

        cacheIndicadores();
    }

    @Override
    public void enableFields() {
        event.setEnableFields(pnlOrdenacao, true);
    }

    @Override
    public void disableFields() {
        event.setEnableFields(pnlOrdenacao, false);
    }

    @Override
    public void refreshFields() {
        tbOrdem.setDataSource(vctScoreCard);

        formataLista();

        reordenaLista();
    }

    private void formataLista() {
        tbOrdem.getColumn(COL_INDICADOR).setPreferredWidth(400);
        tbOrdem.setSortColumn(COL_ORDEM);
        tbOrdem.getTable().setSelectionMode(0);
        tbOrdem.getTable().setEnableSortData(false);
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
        kpi.xml.BIXMLRecord recordAux = new kpi.xml.BIXMLRecord(recordType);
        recordAux.set("ID", id);
        kpi.xml.BIXMLVector vctOrdem = tbOrdem.getXMLData();
        recordAux.set(vctOrdem);
        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    private void cacheIndicadores() {
        KpiListaIndicador parentFrame = (KpiListaIndicador) KpiStaticReferences.getKpiFormController().getParentForm(this.getParentType(), "0");
        if (parentFrame != null) {
            vctScoreCard = parentFrame.getIndicadores().clone2();
        }
    }

    private void initComponents() {//GEN-BEGIN:initComponents

        pnlBottomForm = new JPanel();
        pnlRightForm = new JPanel();
        pnlLeftForm = new JPanel();
        pnlTools = new JPanel();
        pnlConfirmation = new JPanel();
        tbConfirmation = new JToolBar();
        btnSave = new JButton();
        btnCancel = new JButton();
        pnlOperation = new JPanel();
        tbInsertion = new JToolBar();
        btnEdit = new JButton();
        btnReload = new JButton();
        tbpOrdenacao = new JTabbedPane();
        pnlOrdenacao = new JPanel();
        tbOrdem = new JBIXMLTable();
        pnlSetas = new JPanel();
        tlbSetas = new JToolBar();
        btnUp = new JButton();
        btnDown = new JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("kpiOrdemIndicador_00001")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_indicador.gif"))); // NOI18N
        setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(490, 384));
        setRequestFocusEnabled(false);
        getContentPane().add(pnlBottomForm, BorderLayout.SOUTH);
        getContentPane().add(pnlRightForm, BorderLayout.EAST);
        getContentPane().add(pnlLeftForm, BorderLayout.WEST);

        pnlTools.setMinimumSize(new Dimension(480, 27));
        pnlTools.setPreferredSize(new Dimension(10, 29));
        pnlTools.setLayout(new BorderLayout());

        pnlConfirmation.setMaximumSize(new Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new Dimension(155, 27));
        pnlConfirmation.setLayout(new BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new Dimension(150, 32));

        btnSave.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        ResourceBundle bundle1 = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnSave.setText(bundle1.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle1.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, BorderLayout.EAST);

        pnlOperation.setMaximumSize(new Dimension(340, 27));
        pnlOperation.setMinimumSize(new Dimension(340, 27));
        pnlOperation.setPreferredSize(new Dimension(235, 27));
        pnlOperation.setLayout(new BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new Dimension(225, 32));

        btnEdit.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle1.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnReload.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle1.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        pnlOperation.add(tbInsertion, BorderLayout.CENTER);

        pnlTools.add(pnlOperation, BorderLayout.WEST);

        getContentPane().add(pnlTools, BorderLayout.NORTH);

        pnlOrdenacao.setLayout(new BorderLayout());
        pnlOrdenacao.add(tbOrdem, BorderLayout.CENTER);

        pnlSetas.setPreferredSize(new Dimension(30, 0));
        pnlSetas.setLayout(new BorderLayout());

        tlbSetas.setFloatable(false);
        tlbSetas.setOrientation(1);
        tlbSetas.setPreferredSize(new Dimension(20, 20));

        btnUp.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_move_para_cima.gif"))); // NOI18N
        btnUp.setToolTipText(bundle.getString("kpiOrdemIndicador_00003")); // NOI18N
        btnUp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnUpActionPerformed(evt);
            }
        });
        tlbSetas.add(btnUp);

        btnDown.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_move_para_baixo.gif"))); // NOI18N
        btnDown.setToolTipText(bundle.getString("kpiOrdemIndicador_00004")); // NOI18N
        btnDown.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnDownActionPerformed(evt);
            }
        });
        tlbSetas.add(btnDown);

        pnlSetas.add(tlbSetas, BorderLayout.CENTER);

        pnlOrdenacao.add(pnlSetas, BorderLayout.EAST);

        tbpOrdenacao.addTab("Ordena��o", pnlOrdenacao);

        getContentPane().add(tbpOrdenacao, BorderLayout.CENTER);

        pack();
    }//GEN-END:initComponents

    private void reordenaLista() {
        int rows = tbOrdem.getTable().getRowCount();
        for (int i = 1; i <= rows; i++) {
            tbOrdem.setValueAt(i, i - 1, COL_ORDEM);
        }
    }

    private void btnDownActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownActionPerformed
        int row = tbOrdem.getTable().getSelectedRow();
        if (row == -1) {
            return;
        }
        if (row == tbOrdem.getTable().getRowCount() - 1) {
            return;
        }
        tbOrdem.moveRow(row, row, row + 1);
        tbOrdem.getTable().setRowSelectionInterval(row + 1, row + 1);
        reordenaLista();
    }//GEN-LAST:event_btnDownActionPerformed

    private void btnUpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpActionPerformed
        int row = tbOrdem.getTable().getSelectedRow();
        if (row == -1) {
            return;
        }
        if (row == 0) {
            return;
        }
        tbOrdem.moveRow(row, row, row - 1);
        tbOrdem.getTable().setRowSelectionInterval(row - 1, row - 1);
        reordenaLista();
    }//GEN-LAST:event_btnUpActionPerformed

    private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
        event.loadRecord();
    }//GEN-LAST:event_btnReloadActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        event.saveRecord();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        event.cancelOperation();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        event.editRecord();
    }//GEN-LAST:event_btnEditActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnCancel;
    private JButton btnDown;
    private JButton btnEdit;
    private JButton btnReload;
    private JButton btnSave;
    private JButton btnUp;
    private JPanel pnlBottomForm;
    private JPanel pnlConfirmation;
    private JPanel pnlLeftForm;
    private JPanel pnlOperation;
    private JPanel pnlOrdenacao;
    private JPanel pnlRightForm;
    private JPanel pnlSetas;
    private JPanel pnlTools;
    private JToolBar tbConfirmation;
    private JToolBar tbInsertion;
    private JBIXMLTable tbOrdem;
    private JTabbedPane tbpOrdenacao;
    private JToolBar tlbSetas;
    // End of variables declaration//GEN-END:variables
    private kpi.swing.KpiDefaultFrameBehavior event = new kpi.swing.KpiDefaultFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return new javax.swing.JTabbedPane();
    }

    @Override
    public String getParentType() {
        return parentType;
    }
}
