/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kpi.swing.framework.scorecardPanels;

import kpi.core.KpiImageResources;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiTreeNode;

/**
 *
 * @author asilva
 */
public class BscEntityType {
    public static final String ORGANICAO = "ORGANIZACAO";
    public static final String ESTRATEGIA = "ESTRATEGIA";
    public static final String PERSPECTIVA = "PERSPECTIVA";
    public static final String OBJETIVO = "OBJETIVO";
    public static final String INDICADOR = "INDICADOR";

    private String entityName = ""; //NOI18N
    private String entityGroupName = ""; //NOI18N
    private String entityDescription = ""; //NOI18N
    private int entityId = 0;
    private int entityIcon = 0;

    private void setEntityTypeByParentIcon(int iconID) {
	int entID = KpiStaticReferences.CAD_ORGANIZACAO;

	switch (iconID) {
	    case 0:
		entID = KpiStaticReferences.CAD_ORGANIZACAO;
		break;
	    case KpiImageResources.KPI_IMG_ORGANIZACAO:
		entID = KpiStaticReferences.CAD_ESTRATEGIA;
		break;		
	    case KpiImageResources.KPI_IMG_ESTRATEGIA:
		entID = KpiStaticReferences.CAD_PERSPECTIVA;
		break;		
	    case KpiImageResources.KPI_IMG_PERSPECTIVA:
		entID = KpiStaticReferences.CAD_OBJETIVO;
		break;		
	    case KpiImageResources.KPI_IMG_OBJETIVO:
		entID = KpiStaticReferences.CAD_INDICADOR;
		break;		
	}

	this.entityId = entID;
	this.entityIcon = iconID;
	setPropertiesByID(entityId);
    }

    private void setEntityTypeByIcon(int iconID) {
	int entID = KpiStaticReferences.CAD_ORGANIZACAO;

	switch (iconID) {
	    case 0:
		entID = KpiStaticReferences.CAD_ORGANIZACAO;
		break;
	    case KpiImageResources.KPI_IMG_ORGANIZACAO:
		entID = KpiStaticReferences.CAD_ORGANIZACAO;
		break;		
	    case KpiImageResources.KPI_IMG_ESTRATEGIA:
		entID = KpiStaticReferences.CAD_ESTRATEGIA;
		break;		
	    case KpiImageResources.KPI_IMG_PERSPECTIVA:
		entID = KpiStaticReferences.CAD_PERSPECTIVA;
		break;		
	    case KpiImageResources.KPI_IMG_OBJETIVO:
		entID = KpiStaticReferences.CAD_OBJETIVO;
		break;		
	}

	this.entityId = entID;
	this.entityIcon = iconID;
	setPropertiesByID(entityId);

    }

    private void setPropertiesByID(int entityID) {

	switch (entityID) {
	    case KpiStaticReferences.CAD_ORGANIZACAO:
		this.entityName = BscEntityType.ORGANICAO; //NOI18N
		this.entityGroupName = "ORGANIZACOES"; //NOI18N
		this.entityDescription = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BscEntityType_00002");
		break;
	    case KpiStaticReferences.CAD_ESTRATEGIA:
		this.entityName = BscEntityType.ESTRATEGIA; //NOI18N
		this.entityGroupName = "ESTRATEGIAS"; //NOI18N
		this.entityDescription = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BscEntityType_00006");
		break;		
	    case KpiStaticReferences.CAD_PERSPECTIVA:
		this.entityName = BscEntityType.PERSPECTIVA; //NOI18N
		this.entityGroupName = "PERSPECTIVAS"; //NOI18N
		this.entityDescription = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BscEntityType_00003");
		break;		
	    case KpiStaticReferences.CAD_OBJETIVO:
		this.entityName = BscEntityType.OBJETIVO; //NOI18N
		this.entityGroupName = "OBJETIVOS"; //NOI18N
		this.entityDescription = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("BscEntityType_00004");
		break;
	    case KpiStaticReferences.CAD_INDICADOR:
		this.entityName = BscEntityType.INDICADOR; //NOI18N
		this.entityGroupName = "INDICADORES"; //NOI18N
		this.entityDescription = "Indicador"; //NOI18N
		break;
	}
    }

    /***
     * Seta o tipo da BSCEntityType de acordo com o n� da �rvore selecionado.
     * @param nodeSelected = N� da �rvore selecionado.
     */
    public void setEntityByParentNode(KpiTreeNode nodeSelected) {
	setEntityTypeByParentIcon(nodeSelected.getIDimage());
    }

    /***
     * Seta o tipo da BSCEntityType de acordo com o n� da �rvore selecionado.
     * @param nodeSelected = N� da �rvore selecionado.
     */
    public void setEntityByNode(KpiTreeNode nodeSelected) {
	setEntityTypeByIcon(nodeSelected.getIDimage());
    }

    /**
     * @return the entityName
     */
    public String getEntityName() {
	return entityName;
    }

    /**
     * @return the entityGroupName
     */
    public String getEntityGroupName() {
	return entityGroupName;
    }

    /**
     * @param entityIcon the entityIcon to set
     */
    public int getEntityIcon() {
	return this.entityIcon;
    }

    /**
     * @return the entityDescription
     */
    public String getEntityDescription() {
	return entityDescription;
    }
}