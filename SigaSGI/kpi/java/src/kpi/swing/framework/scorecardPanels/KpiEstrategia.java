package kpi.swing.framework.scorecardPanels;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;
import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import kpi.beans.JBIListBtnPnlListener;
import kpi.beans.JBISeekListPanel;
import kpi.beans.JBITextArea;
import kpi.beans.JBIXMLTable;
import kpi.core.*;
import kpi.core.IteMnuPermission.Permissao;
import kpi.core.IteMnuPermission.TipoPermissao;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiDefaultFrameFunctions;
import kpi.xml.BIXMLAttributes;
import kpi.xml.BIXMLRecord;
import pv.jfcx.JPVDatePlus;
import pv.jfcx.JPVEdit;

public class KpiEstrategia extends JInternalFrame implements KpiDefaultFrameFunctions, JBIListBtnPnlListener {

    private String recordType = "ESTRATEGIA"; //NOI18N
    private String formType = recordType;//Tipo do Formulario
    private String parentType = "FORMMATRIX";//Tipo formulario pai, caso haja. //NOI18N
    private String matrixParent = ""; //NOI18N
    private String idEstrategia = ""; //NOI18N
    private boolean lForceUpdate = false;

    public KpiEstrategia(int operation, String idAux, String contextId) {
        initComponents();
        putClientProperty("MAXI", true); //NOI18N

        event.defaultConstructor(operation, idAux, contextId);
        jBIListTemas.btnReload.setVisible(false);
        jBIListTemas.btnAjuda.setVisible(false);
        jBIListTemas.addListPnlBtnListerner(this);
        KpiMenuController mnuControl = KpiStaticReferences.getMnuController();
        IteMnuPermission perm = mnuControl.getRuleByName("TEMAESTRATEGICO");

        //SEGURANCA E MODO BSC NAO MOSTRA TEMA ESTRATEGICO
        if (perm.getPermissionValue(TipoPermissao.VISUALIZACAO) != Permissao.PERMITIDO
                || KpiStaticReferences.getSystemMode().equals(KpiStaticReferences.PDCA_MODE)) {
            tapCadastro.remove(pnlTemaEst);
        }

    }

    @Override
    public void enableFields() {
        event.setEnableFields(pnlEstrategia, true);
        btnSave.setEnabled(true);
        
        if (tapCadastro.getTabCount() > 1) {
            tapCadastro.setEnabledAt(1, false);
        }
    }

    @Override
    public void disableFields() {
        event.setEnableFields(pnlEstrategia, false);
        
        if (tapCadastro.getTabCount() > 1) {
            tapCadastro.setEnabledAt(1, true);
        }
    }

    @Override
    public void refreshFields() {
        txtNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        txtDataIni.setCalendar(record.getDate("DATAINI")); //NOI18N
        txtDataFim.setCalendar(record.getDate("DATAFIN")); //NOI18N
        jBIListTemas.setDataSource(record.getBIXMLVector("TEMASESTRATEGICOS"), record.getString("ID") + "|2", "", this.event); //NOI18N
    }

    @Override
    public BIXMLRecord getFieldsContents() {
        BIXMLRecord recordAux = new BIXMLRecord(recordType);

        if (matrixParent.equals("0")) { //NOI18N
            recordAux.set("PARENTID", ""); //NOI18N
        } else {
            recordAux.set("PARENTID", matrixParent); //NOI18N
        }

        recordAux.set("ID", id); //NOI18N
        recordAux.set("NOME", txtNome.getText()); //NOI18N
        recordAux.set("DESCRICAO", txtDescricao.getText()); //NOI18N
        recordAux.set("DATAINI", txtDataIni.getCalendar()); //NOI18N
        recordAux.set("DATAFIN", txtDataFim.getCalendar()); //NOI18N

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean retorno = true;

        if (txtNome.getText().trim().equals("")) { //NOI18N
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEstrategia_00001"));
            retorno = false;
        } else if (txtDataIni.getText().trim().equals("")) { //NOI18N
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEstrategia_00002"));
            retorno = false;
        } else if (txtDataFim.getText().trim().equals("")) { //NOI18N
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEstrategia_00003"));
            retorno = false;
        } else if (txtDataIni.getCalendar().compareTo(txtDataFim.getCalendar()) > 0) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiEstrategia_00004"));
            retorno = false;
        }

        if (!retorno) {
            btnSave.setEnabled(true);
        }

        return retorno;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlTools = new JPanel();
        pnlConfirmation = new JPanel();
        tbConfirmation = new JToolBar();
        btnSave = new JButton();
        btnCancel = new JButton();
        pnlOperation = new JPanel();
        tbInsertion = new JToolBar();
        btnEdit = new JButton();
        btnDelete = new JButton();
        btnReload = new JButton();
        btnAjuda = new JButton();
        pnlDetalhe = new JPanel();
        lblOrganizacao = new JLabel();
        pnlCenter = new JPanel();
        pnlFormulario = new JPanel();
        tapCadastro = new JTabbedPane();
        pnlEst = new JPanel();
        pnlRecord = new JPanel();
        scrRecord = new JScrollPane();
        pnlEstrategia = new JPanel();
        lblNome = new JLabel();
        txtNome = new JPVEdit();
        lblDescricao = new JLabel();
        scrDescricao = new JScrollPane();
        txtDescricao = new JBITextArea();
        lblDataIni = new JLabel();
        txtDataIni = new JPVDatePlus();
        lblDataFim = new JLabel();
        txtDataFim = new JPVDatePlus();
        pnlTemaEst = new JPanel();
        jBIListTemas = new JBISeekListPanel();
        pnlRightForm = new JPanel();
        pnlLeftForm = new JPanel();
        pnlBottomForm = new JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiEstrategia_00005")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_estrategia.gif")));         setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(480, 312));

        pnlTools.setMinimumSize(new Dimension(480, 27));
        pnlTools.setPreferredSize(new Dimension(10, 60));
        pnlTools.setLayout(new BorderLayout());

        pnlConfirmation.setMaximumSize(new Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new Dimension(155, 27));
        pnlConfirmation.setLayout(new BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new Dimension(150, 32));

        btnSave.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif")));         btnSave.setText(bundle.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif")));         btnCancel.setText(bundle.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, BorderLayout.EAST);

        pnlOperation.setMaximumSize(new Dimension(340, 27));
        pnlOperation.setMinimumSize(new Dimension(340, 27));
        pnlOperation.setPreferredSize(new Dimension(340, 27));
        pnlOperation.setLayout(new BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new Dimension(250, 32));

        btnEdit.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif")));         btnEdit.setText(bundle.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif")));         btnDelete.setText(bundle.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif")));         btnReload.setText(bundle.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnAjuda.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif")));         ResourceBundle bundle1 = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnAjuda.setText(bundle1.getString("KpiGraphIndicador_00011")); // NOI18N
        btnAjuda.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda);

        pnlOperation.add(tbInsertion, BorderLayout.CENTER);

        pnlTools.add(pnlOperation, BorderLayout.WEST);

        pnlDetalhe.setBackground(new Color(223, 229, 243));
        pnlDetalhe.setBorder(BorderFactory.createLineBorder(new Color(221, 221, 221)));
        pnlDetalhe.setPreferredSize(new Dimension(10, 30));

        lblOrganizacao.setText("jLabel1");

        GroupLayout pnlDetalheLayout = new GroupLayout(pnlDetalhe);
        pnlDetalhe.setLayout(pnlDetalheLayout);
        pnlDetalheLayout.setHorizontalGroup(
            pnlDetalheLayout.createParallelGroup(Alignment.LEADING)
            .addGroup(Alignment.TRAILING, pnlDetalheLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrganizacao, GroupLayout.DEFAULT_SIZE, 503, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlDetalheLayout.setVerticalGroup(
            pnlDetalheLayout.createParallelGroup(Alignment.LEADING)
            .addGroup(pnlDetalheLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(lblOrganizacao))
        );

        pnlTools.add(pnlDetalhe, BorderLayout.NORTH);

        getContentPane().add(pnlTools, BorderLayout.NORTH);

        pnlCenter.setLayout(new BorderLayout());

        pnlFormulario.setLayout(new BorderLayout());

        tapCadastro.setPreferredSize(new Dimension(510, 345));

        pnlEst.setLayout(new BorderLayout());

        pnlRecord.setLayout(new BorderLayout());

        scrRecord.setBorder(null);

        pnlEstrategia.setLayout(null);

        lblNome.setForeground(new Color(51, 51, 255));
        lblNome.setHorizontalAlignment(SwingConstants.LEFT);
        lblNome.setText(bundle.getString("KpiEstrategia_00006")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new Dimension(100, 16));
        lblNome.setMinimumSize(new Dimension(100, 16));
        lblNome.setPreferredSize(new Dimension(100, 16));
        pnlEstrategia.add(lblNome);
        lblNome.setBounds(10, 10, 58, 20);

        txtNome.setMaxLength(120);
        pnlEstrategia.add(txtNome);
        txtNome.setBounds(10, 30, 400, 22);

        lblDescricao.setText(bundle.getString("KpiEstrategia_00007")); // NOI18N
        lblDescricao.setEnabled(false);
        pnlEstrategia.add(lblDescricao);
        lblDescricao.setBounds(10, 50, 60, 20);

        txtDescricao.setFont(new Font("Tahoma", 0, 11));         scrDescricao.setViewportView(txtDescricao);

        pnlEstrategia.add(scrDescricao);
        scrDescricao.setBounds(10, 70, 400, 60);

        lblDataIni.setForeground(new Color(51, 51, 255));
        lblDataIni.setText(bundle.getString("KpiEstrategia_00008")); // NOI18N
        lblDataIni.setEnabled(false);
        pnlEstrategia.add(lblDataIni);
        lblDataIni.setBounds(10, 130, 60, 20);

        txtDataIni.setUseLocale(true);
        txtDataIni.setWaitForCalendarDate(true);
        pnlEstrategia.add(txtDataIni);
        txtDataIni.setBounds(10, 150, 180, 22);

        lblDataFim.setForeground(new Color(51, 51, 255));
        lblDataFim.setText(bundle.getString("KpiEstrategia_00009")); // NOI18N
        lblDataFim.setEnabled(false);
        pnlEstrategia.add(lblDataFim);
        lblDataFim.setBounds(230, 130, 60, 20);

        txtDataFim.setUseLocale(true);
        txtDataFim.setWaitForCalendarDate(true);
        pnlEstrategia.add(txtDataFim);
        txtDataFim.setBounds(230, 150, 180, 22);

        scrRecord.setViewportView(pnlEstrategia);

        pnlRecord.add(scrRecord, BorderLayout.CENTER);

        pnlEst.add(pnlRecord, BorderLayout.CENTER);

        ResourceBundle bundle2 = ResourceBundle.getBundle("international"); // NOI18N
        tapCadastro.addTab(bundle2.getString("KpiEstrategia_00005"), pnlEst); // NOI18N

        pnlTemaEst.setLayout(new BorderLayout());
        pnlTemaEst.add(jBIListTemas, BorderLayout.CENTER);

        tapCadastro.addTab(bundle2.getString("KpiEstrategia_00010"), pnlTemaEst); // NOI18N

        pnlFormulario.add(tapCadastro, BorderLayout.CENTER);
        pnlFormulario.add(pnlRightForm, BorderLayout.EAST);
        pnlFormulario.add(pnlLeftForm, BorderLayout.WEST);
        pnlFormulario.add(pnlBottomForm, BorderLayout.SOUTH);

        pnlCenter.add(pnlFormulario, BorderLayout.CENTER);

        getContentPane().add(pnlCenter, BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
            event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjudaActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            tapCadastro.setSelectedIndex(0);
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); //NOI18N

            StringBuilder msg = new StringBuilder();

            msg.append(bundle.getString("MatrixFrameBehavior_00009")); //NOI18N
            msg.append(bundle.getString("MatrixFrameBehavior_00005")); //NOI18N

            if (event.cascateDelete(msg.toString(), this)) {
		setID(""); //NOI18N
		event.loadRecord();
	    }
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            btnSave.setEnabled(false);
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnAjuda;
    private JButton btnCancel;
    private JButton btnDelete;
    private JButton btnEdit;
    private JButton btnReload;
    private JButton btnSave;
    private JBISeekListPanel jBIListTemas;
    private JLabel lblDataFim;
    private JLabel lblDataIni;
    private JLabel lblDescricao;
    private JLabel lblNome;
    private JLabel lblOrganizacao;
    private JPanel pnlBottomForm;
    private JPanel pnlCenter;
    private JPanel pnlConfirmation;
    private JPanel pnlDetalhe;
    private JPanel pnlEst;
    private JPanel pnlEstrategia;
    private JPanel pnlFormulario;
    private JPanel pnlLeftForm;
    private JPanel pnlOperation;
    private JPanel pnlRecord;
    private JPanel pnlRightForm;
    private JPanel pnlTemaEst;
    private JPanel pnlTools;
    private JScrollPane scrDescricao;
    private JScrollPane scrRecord;
    private JTabbedPane tapCadastro;
    private JToolBar tbConfirmation;
    private JToolBar tbInsertion;
    private JPVDatePlus txtDataFim;
    private JPVDatePlus txtDataIni;
    private JBITextArea txtDescricao;
    private JPVEdit txtNome;
    // End of variables declaration//GEN-END:variables
    private MatrixFrameBehavior event = new MatrixFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    @Override
    public void setStatus(int value) {
        if (lForceUpdate && getStatus() == KpiDefaultFrameBehavior.UPDATING) {
            event.loadRecord();
            lForceUpdate = false;
        }
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
        if ("COMMANDS".equals(recordAux.getTagName())) { //NOI18N
            BIXMLAttributes att = recordAux.getAttributes();
            matrixParent = att.getString("PARENTID"); //NOI18N
            
            BIRequest request = new BIRequest();
            KpiDataController dataController = KpiStaticReferences.getKpiDataController();

            request.setParameter("TIPO", "ESTRATEGIA"); //NOI18N
            request.setParameter("PARENTID", matrixParent); //NOI18N

            BIXMLRecord rec = dataController.listRecords(request);
            
            StringBuilder orgName = new StringBuilder();

            orgName.append("<html>"); //NOI18N
            orgName.append("<b>");
            orgName.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMapaDrillObjetivo_00001"));
            orgName.append("</b> ");
            
            orgName.append(rec.getBIXMLVector("PARENTS_NAMES").getAttributes().getString("ORGANIZACAO_NOME")); //NOI18N
            orgName.append("</html>"); //NOI18N
            
            lblOrganizacao.setText(orgName.toString());

        } else {
            record = recordAux;
        }
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        return "0";//Sempre zero para fazer refresh no parent. //NOI18N
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public String getFormType() {
        return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return null;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    public BIXMLRecord getCmdOnNewPressed(ActionEvent evt) {
        lForceUpdate = true;
        //
        BIXMLAttributes att = new BIXMLAttributes();
        att.set("PARENTID", getID()); //NOI18N
        att.set("SET_RECORD_ON_NEW_FORM", ""); //NOI18N
        BIXMLRecord recCommand = new BIXMLRecord("COMMANDS", att); //NOI18N

        return recCommand;
    }

    public BIXMLRecord getCmdOnBtnUser1Pressed(ActionEvent evt) {
        lForceUpdate = true;
        return null;
    }

    public BIXMLRecord getCmdOnBtnViewPressed(ActionEvent evt) {
        lForceUpdate = true;
        return getCmdOnNewPressed(null);
    }

    public BIXMLRecord getCmdOnBtnReloadPressed(ActionEvent evt) {
        lForceUpdate = true;
        return null;
    }

    public BIXMLRecord xmlTableMouseClicked(MouseEvent evt) {
        lForceUpdate = true;
        return getCmdOnNewPressed(null);
    }

    public void doBtnUser1ActionPerformed(ActionEvent evt) {
        JBIXMLTable tabList = jBIListTemas.xmlTable;

        if (tabList.getSelectedRow() == -1) {
            JOptionPane.showInternalMessageDialog(this, java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiFormMatrix_00002"), java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiFormMatrix_00003"), JOptionPane.INFORMATION_MESSAGE);
        } else {
            //Recuperando a linha selecionada na tabela.
            BIXMLRecord recSelected = tabList.getSelectedRecord();
            BIXMLAttributes att = new BIXMLAttributes();
            att.set("PARENTID", idEstrategia); //NOI18N
            att.set("SET_RECORD_ON_NEW_FORM", ""); //NOI18N
            att.set("SCORECARD_SELECTED", recSelected.getString("ID")); //NOI18N

            BIXMLRecord recCommand = new BIXMLRecord("COMMANDS", att); //NOI18N

            KpiDefaultFrameFunctions frmDuplicador = KpiStaticReferences.getKpiFormController().getForm("SCO_DUPLICADOR", this.getID(), ""); //NOI18N

            frmDuplicador.setRecord(recCommand);
            //Mostrando o form.
            frmDuplicador.asJInternalFrame();
        }

    }
}
