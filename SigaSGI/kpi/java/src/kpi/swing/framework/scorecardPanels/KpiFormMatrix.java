package kpi.swing.framework.scorecardPanels;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import javax.swing.tree.TreePath;
import kpi.beans.JBIListBtnPnlListener;
import kpi.beans.JBIXMLTable;
import kpi.core.IteMnuPermission;
import kpi.core.KpiMenuController;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiDefaultFrameFunctions;
import kpi.swing.KpiTreeNode;
import kpi.swing.kpiCreateTree;
import kpi.xml.BIXMLAttributes;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;

public class KpiFormMatrix extends javax.swing.JInternalFrame implements KpiDefaultFrameFunctions, JBIListBtnPnlListener {

    private String id = ""; //NOI18N
    private String recordType = "SCORECARD"; //NOI18N
    private String formType = recordType;
    private String parentType = recordType;
    private String idParent = ""; //NOI18N
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private kpiCreateTree buildTree = new kpiCreateTree();
    private KpiDefaultFrameBehavior event = new KpiDefaultFrameBehavior(this);
    private boolean lForceUpdate = false;
    private BIXMLRecord record = null;
    private BscEntityType entityType = new BscEntityType();
    private KpiMenuController mnuController = KpiStaticReferences.getMnuController();
    private boolean isFirstTime = true;

    public KpiFormMatrix(int operation, String idAux, String contextId) {
	initComponents();
	putClientProperty("MAXI", true); //NOI18N
	event.defaultConstructor(operation, "0", contextId); //NOI18N
	event.setFecharPainel(false);
	setJBIMatrixSeek();
	setFormPermissions();
    }

    @Override
    public void enableFields() {
	kpiCadTree.setEnabled(false);
    }

    @Override
    public void disableFields() {
	kpiCadTree.setEnabled(true);
    }

    @Override
    public void refreshFields() {
	if (getStatus() != KpiDefaultFrameBehavior.INSERTING) {
	    DefaultMutableTreeNode nodeSelected = (DefaultMutableTreeNode) kpiCadTree.getLastSelectedPathComponent();
	    TreePath nodePathSelected = buildTree.getSelectedTreePath();
	    kpiCadTree.removeAll();

	    kpiCadTree.setCellRenderer(new KpiScoreCardTreeCellRenderer(kpi.core.KpiStaticReferences.getKpiImageResources()));
	    DefaultTreeCellRenderer kpiRender = (DefaultTreeCellRenderer) kpiCadTree.getCellRenderer();

	    kpiRender.setClosedIcon(kpiRender.getLeafIcon());
	    kpiRender.setOpenIcon(kpiRender.getLeafIcon());

	    kpiCadTree.setModel(new javax.swing.tree.DefaultTreeModel(buildTree.createTree(record.getBIXMLVector("SCORECARDS")))); //NOI18N

	    buildTree.expandPath(kpiCadTree, nodePathSelected);

	    if (isFirstTime){
		kpiCadTree.setSelectionRow(0);
		isFirstTime = false;
	    }

	    loadTableData(nodeSelected);
	}
    }

    @Override
    public kpi.xml.BIXMLRecord getFieldsContents() {
	return new BIXMLRecord("DUMMY"); //NOI18N
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
	boolean retorno = true;

	return retorno;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        jMainSplit = new javax.swing.JSplitPane();
        jScrollTree = new javax.swing.JScrollPane();
        kpiCadTree = new javax.swing.JTree();
        scrollRecord = new javax.swing.JScrollPane();
        jBIMatrixSeek = new kpi.beans.JBISeekListPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiFormMatrix_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_scorecard.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(791, 411));
        getContentPane().setLayout(new java.awt.BorderLayout(5, 5));

        pnlRecord.setLayout(new java.awt.BorderLayout(5, 5));

        scpRecord.setBorder(null);
        scpRecord.setPreferredSize(new java.awt.Dimension(500, 330));
        scpRecord.setRequestFocusEnabled(false);

        jMainSplit.setBorder(null);
        jMainSplit.setDividerLocation(200);
        jMainSplit.setAutoscrolls(true);
        jMainSplit.setContinuousLayout(true);
        jMainSplit.setOneTouchExpandable(true);
        jMainSplit.setPreferredSize(new java.awt.Dimension(200, 220));

        jScrollTree.setAutoscrolls(true);
        jScrollTree.setPreferredSize(new java.awt.Dimension(120, 60));

        kpiCadTree.setModel(null);
        kpiCadTree.setMinimumSize(new java.awt.Dimension(10, 10));
        kpiCadTree.setRowHeight(18);
        kpiCadTree.addTreeWillExpandListener(new javax.swing.event.TreeWillExpandListener() {
            public void treeWillCollapse(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {
            }
            public void treeWillExpand(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {
                kpiCadTreeTreeWillExpand(evt);
            }
        });
        kpiCadTree.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
                kpiCadTreeChanged(evt);
            }
        });
        jScrollTree.setViewportView(kpiCadTree);

        jMainSplit.setLeftComponent(jScrollTree);

        scrollRecord.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        scrollRecord.setPreferredSize(new java.awt.Dimension(300, 100));
        scrollRecord.setViewportView(jBIMatrixSeek);

        jMainSplit.setRightComponent(scrollRecord);

        scpRecord.setViewportView(jMainSplit);

        pnlRecord.add(scpRecord, java.awt.BorderLayout.CENTER);

        getContentPane().add(pnlRecord, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void kpiCadTreeTreeWillExpand(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {//GEN-FIRST:event_kpiCadTreeTreeWillExpand
	DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) evt.getPath().getLastPathComponent();
	requestXMLTree(oMutTreeNode);
    }//GEN-LAST:event_kpiCadTreeTreeWillExpand

private void kpiCadTreeChanged(javax.swing.event.TreeSelectionEvent evt) {//GEN-FIRST:event_kpiCadTreeChanged
    jBIMatrixSeek.xmlTable.getJTable().clearSelection();
    DefaultMutableTreeNode oMutTreeNode = (DefaultMutableTreeNode) evt.getPath().getLastPathComponent();
    requestXMLTree(oMutTreeNode);
}//GEN-LAST:event_kpiCadTreeChanged
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private kpi.beans.JBISeekListPanel jBIMatrixSeek;
    private javax.swing.JSplitPane jMainSplit;
    private javax.swing.JScrollPane jScrollTree;
    private javax.swing.JTree kpiCadTree;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JScrollPane scrollRecord;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setStatus(int value) {
	if (lForceUpdate && getStatus() == KpiDefaultFrameBehavior.UPDATING) {
	    event.loadRecord();
	    lForceUpdate = false;
	}
	status = value;
    }

    @Override
    public int getStatus() {
	return status;
    }

    @Override
    public void setType(String value) {
	recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
	return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
	id = "0";//Sempre zero para funcionar o refresh do parent. //NOI18N
    }

    @Override
    public String getID() {
	return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
	record = recordAux;
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
	return record;
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
	return this;
    }

    @Override
    public String getParentID() {
	if (record.contains("PARENTID")) { //NOI18N
	    return String.valueOf(record.getString("PARENTID"));
	} else {
	    return ""; //NOI18N
	}
    }

    @Override
    public void loadRecord() {
	event.loadRecord();
    }

    @Override
    public String getFormType() {
	return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
	return new javax.swing.JTabbedPane();
    }

    @Override
    public String getParentType() {
	return parentType;
    }

    @Override
    protected void validateTree() {
	super.validateTree();
    }

    /**
     * M�todo para retornar a �rvore de cadastros.
     * 
     **/
    public javax.swing.JTree getTreeCad() {
	return kpiCadTree;
    }

    private void loadTableData(DefaultMutableTreeNode nodeSelected) {

	if (nodeSelected != null) {
	    BIXMLVector vctTable = null;
	    if (!entityType.getEntityName().equals("INDICADOR")) { //NOI18N
		BIXMLAttributes attColumn = new BIXMLAttributes();
		attColumn.set("TIPO", entityType.getEntityName()); //NOI18N
		attColumn.set("RETORNA", "F"); //NOI18N
		attColumn.set("TAG000", "NOME"); //NOI18N
		attColumn.set("CAB000", entityType.getEntityDescription()); //NOI18N
		attColumn.set("CLA000", "4"); //NOI18N

		vctTable = new BIXMLVector("SCORECARDS", "SCORECARD", attColumn); //NOI18N

		BscEntityType nodeBSC = new BscEntityType();

		for (int nodeIndex = 0; nodeIndex < nodeSelected.getChildCount(); nodeIndex++) {
		    DefaultMutableTreeNode nodeItem = (DefaultMutableTreeNode) nodeSelected.getChildAt(nodeIndex);
		    KpiTreeNode kpiNode = (KpiTreeNode) nodeItem.getUserObject();

		    if (!kpiNode.getID().equals("0") && !kpiNode.getID().trim().equals("")) { //NOI18N
			nodeBSC.setEntityByParentNode(kpiNode);
			boolean isViewEnable = true;

			//Setando as permissoes dependendo do tipo de acesso ao usu�rio e se ele possui ou nao acesso
			//ao scorecard (entidade) solicitada.
			IteMnuPermission menu = (IteMnuPermission) mnuController.getMenuRules().get(nodeBSC.getEntityGroupName());
			boolean isAdmin = (KpiStaticReferences.getUserType() == KpiStaticReferences.UserType.ADMIN);
			if (!isAdmin) {
			    boolean isManuEnable = isManuEnable(menu);
			    isViewEnable = isViewEnable(menu);
			    if (isManuEnable) {
				isViewEnable = true;
			    }
			}

			//Criando as linhas da tabela
			BIXMLRecord recLinha = new BIXMLRecord(entityType.getEntityName());
			recLinha.set("ID", kpiNode.getID()); //NOI18N
			recLinha.set("NOME", kpiNode.getName()); //NOI18N

			//Seguran�a por linha-Atributos de seguranca.
			BIXMLAttributes secAtt = new BIXMLAttributes();
			secAtt.set("IS_LINE_VIEW_ENABLED", kpiNode.isEnable() && isViewEnable);//NOI18N

			recLinha.setAttributes(secAtt);
			vctTable.add(recLinha);
		    }
		}
	    } else {
		StringBuilder request = new StringBuilder("ID_SCOREC = '"); //NOI18N
		request.append(idParent);
		request.append("',LSTINDICADOR"); //NOI18N
		BIXMLRecord recIndFiltrados = event.listRecords("INDICADOR", request.toString()); //NOI18N

		vctTable = recIndFiltrados.getBIXMLVector("INDICADORES"); //NOI18N
	    }
	    jBIMatrixSeek.setDataSource(vctTable, entityType.getEntityGroupName(), entityType.getEntityDescription(), event);
	}
    }

    public void showOperationsButtons() {
    }

    public void showDataButtons() {
    }

    public void doBtnUser1ActionPerformed(ActionEvent evt) {
	JBIXMLTable tabList = jBIMatrixSeek.xmlTable;

	if (tabList.getSelectedRow() == -1) {
	    JOptionPane.showInternalMessageDialog(this, java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiFormMatrix_00002"), java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiFormMatrix_00003"), JOptionPane.INFORMATION_MESSAGE);
	} else {
	    //Recuperando a linha selecionada na tabela.
	    BIXMLRecord recSelected = tabList.getSelectedRecord();
	    BIXMLAttributes att = new BIXMLAttributes();
	    att.set("PARENTID", idParent); //NOI18N
	    att.set("SET_RECORD_ON_NEW_FORM", ""); //NOI18N
	    att.set("SCORECARD_SELECTED", recSelected.getString("ID")); //NOI18N

	    BIXMLRecord recCommand = new BIXMLRecord("COMMANDS", att); //NOI18N

	    KpiDefaultFrameFunctions frmDuplicador = KpiStaticReferences.getKpiFormController().getForm("SCO_DUPLICADOR", this.getID(), ""); //NOI18N

	    frmDuplicador.setRecord(recCommand);
	    //Mostrando o form.
	    frmDuplicador.asJInternalFrame();
	}
    }

    private void setFormPermissions() {
	DefaultMutableTreeNode nodeSelected = (DefaultMutableTreeNode) kpiCadTree.getLastSelectedPathComponent();

	if (nodeSelected != null) {
	    boolean isAdmin = (KpiStaticReferences.getUserType() == KpiStaticReferences.UserType.ADMIN);

	    if (!isAdmin) {
		KpiTreeNode usrNode = (KpiTreeNode) nodeSelected.getUserObject();
		boolean isManuEnable = false;
		boolean isViewEnable = false;

		if (usrNode.isEnable()) {
		    IteMnuPermission menu = (IteMnuPermission) mnuController.getMenuRules().get(entityType.getEntityGroupName());

		    isManuEnable = isManuEnable(menu);
		    isViewEnable = isViewEnable(menu);

		    //Setando as permiss�es.
		    if (isManuEnable) {
			isViewEnable = true;
		    }
		}

		jBIMatrixSeek.btnNew.setEnabled(isManuEnable);
		jBIMatrixSeek.btnUser1.setEnabled(isManuEnable);
		jBIMatrixSeek.btnView.setEnabled(isViewEnable);
	    }
	}
    }

    private boolean isManuEnable(IteMnuPermission menu) {
	boolean isManuEnable;

	isManuEnable = (menu.getPermissionValue(IteMnuPermission.TipoPermissao.MANUTENCAO) == IteMnuPermission.Permissao.PERMITIDO);

	return isManuEnable;
    }

    private boolean isViewEnable(IteMnuPermission menu) {
	boolean isViewEnable;

	isViewEnable = (menu.getPermissionValue(IteMnuPermission.TipoPermissao.VISUALIZACAO) == IteMnuPermission.Permissao.PERMITIDO);

	return isViewEnable;

    }

    private void setJBIMatrixSeek() {
	jBIMatrixSeek.addListPnlBtnListerner(this);
	jBIMatrixSeek.btnUser1.setVisible(true);
	jBIMatrixSeek.btnUser1.setText(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiFormMatrix_00003"));
	jBIMatrixSeek.btnUser1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kpi/imagens/ic_duplicador.gif"))); //NOI18N
    }

    public BIXMLRecord getCmdOnNewPressed(ActionEvent evt) {
	lForceUpdate = true;
	//
	BIXMLAttributes att = new BIXMLAttributes();
	att.set("PARENTID", idParent); //NOI18N
	att.set("SET_RECORD_ON_NEW_FORM", ""); //NOI18N
	BIXMLRecord recCommand = new BIXMLRecord("COMMANDS", att); //NOI18N

	return recCommand;
    }

    public BIXMLRecord getCmdOnBtnUser1Pressed(ActionEvent evt) {
	lForceUpdate = true;
	return null;
    }

    public BIXMLRecord getCmdOnBtnViewPressed(ActionEvent evt) {
	lForceUpdate = true;
	return getCmdOnNewPressed(null);
    }

    public BIXMLRecord getCmdOnBtnReloadPressed(ActionEvent evt) {
	lForceUpdate = true;
	return null;
    }

    public BIXMLRecord xmlTableMouseClicked(MouseEvent evt) {
	lForceUpdate = true;
	return getCmdOnNewPressed(null);
    }

    private void requestXMLTree(DefaultMutableTreeNode oMutTreeNode) {

	if (oMutTreeNode != null) {
	    boolean reqNode = false;
	    DefaultMutableTreeNode usrLeafObj = oMutTreeNode.getNextNode();
	    Object usrObjSelected = oMutTreeNode.getUserObject();

	    if (usrLeafObj != null && usrLeafObj.getUserObject() instanceof KpiTreeNode) {
		KpiTreeNode userNodeLeafSelected = (KpiTreeNode) usrLeafObj.getUserObject();
		String idPar = userNodeLeafSelected.getID();
		reqNode = idPar.endsWith(".WAIT"); //NOI18N
	    }


	    if ((reqNode || lForceUpdate) && usrObjSelected instanceof kpi.swing.KpiTreeNode) {
		StringBuilder parameters = new StringBuilder();
		KpiTreeNode oKpiTreeNode = (KpiTreeNode) usrObjSelected;

		//Defini�ao dos par�metros da requisi��o.
		parameters.append("LISTA_SCO_CHILD"); //Identificador. //NOI18N
		parameters.append("|"); //NOI18N
		if (oKpiTreeNode.getID().equals("0")){
		    parameters.append(""); //O ID do registro 0 � branco.
		}else{
		    parameters.append(oKpiTreeNode.getID()); //Scorecard PAI.
		}
		parameters.append("|"); //NOI18N
		parameters.append("0"); //Pacote. //NOI18N
		parameters.append("|"); //NOI18N
		parameters.append("T"); //Verificar acesso? //NOI18N

		//Requisita a lista de filhos do scorecard que foi expandido.
		BIXMLRecord recPlanos = event.listRecords("SCORECARD", parameters.toString()); //NOI18N

		//Insere din�micamente os scorecards no pai.
		buildTree.insertChild(recPlanos, oMutTreeNode);
	    }

	    //Carregar os itens na tabela lateral.
	    DefaultMutableTreeNode nodeSelected = (DefaultMutableTreeNode) kpiCadTree.getLastSelectedPathComponent();

	    if (nodeSelected != null) {
		if (nodeSelected.getUserObject() instanceof KpiTreeNode) {
		    KpiTreeNode userNodeSelected = (KpiTreeNode) nodeSelected.getUserObject();
		    idParent = userNodeSelected.getID();
		    entityType.setEntityByParentNode(userNodeSelected);
		    loadTableData(nodeSelected);
		    setFormPermissions();
		} else {
		    setID("0"); //NOI18N
		}
	    }
	    buildTree.setSelectedTreePath(kpiCadTree.getSelectionPath());
	}
    }
}

class KpiScoreCardTreeCellRenderer extends kpi.swing.KpiTreeCellRenderer {

    private kpi.core.KpiImageResources myImageResources;

    public KpiScoreCardTreeCellRenderer(kpi.core.KpiImageResources imageResources) {
	super(imageResources);
	setUseMyRenderer(true);
	myImageResources = imageResources;
    }

    @Override
    public void myImageRenderer(Object value) {
	javax.swing.tree.DefaultMutableTreeNode nodeSelected = (javax.swing.tree.DefaultMutableTreeNode) value;
	KpiTreeNode kpiNode;

	if (nodeSelected.getUserObject() instanceof KpiTreeNode) {
	    kpiNode = (KpiTreeNode) nodeSelected.getUserObject();
	    ImageIcon nodeImage = myImageResources.getImage(kpiNode.getIDimage());

	    //Icones
	    setOpenIcon(nodeImage);
	    setLeafIcon(nodeImage);
	    setIcon(nodeImage);
	    setClosedIcon(nodeImage);

	    setBackground(null);
	    setBackgroundNonSelectionColor(null);
	    setOpaque(false);

	    //Verificando se o no deve estar habilitado.
	    if (kpiNode.isEnable()) {
		setEnabled(true);
	    } else {
		setEnabled(false);
	    }
	}
    }
}