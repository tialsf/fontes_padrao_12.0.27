package kpi.swing.framework.scorecardPanels;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import kpi.beans.JBITextArea;
import kpi.core.BIRequest;
import kpi.core.KpiDataController;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameBehavior;
import kpi.swing.KpiDefaultFrameFunctions;
import kpi.xml.BIXMLAttributes;
import kpi.xml.BIXMLRecord;
import kpi.xml.BIXMLVector;
import pv.jfcx.JPVEdit;

public class KpiObjetivo extends JInternalFrame implements KpiDefaultFrameFunctions {

    private String recordType = "OBJETIVO"; //NOI18N
    private String formType = recordType;//Tipo do Formulario
    private String parentType = "FORMMATRIX";//Tipo formulario pai, caso haja. //NOI18N
    private String matrixParent = ""; //NOI18N
    private java.util.Hashtable htbResponsavel = new java.util.Hashtable();
    private BIXMLVector vctUsuarios;

    public KpiObjetivo(int operation, String idAux, String contextId) {
	initComponents();
	putClientProperty("MAXI", true); //NOI18N

	event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
	event.setEnableFields(pnlUnidade, true);
	btnSave.setEnabled(true);
    }

    @Override
    public void disableFields() {
	event.setEnableFields(pnlUnidade, false);
    }

    @Override
    public void refreshFields() {
	vctUsuarios = record.getBIXMLVector("USUARIOS"); //NOI18N

	txtNome.setText(record.getString("NOME"));
	txtDescricao.setText(record.getString("DESCRICAO"));
	txtOrigem.setText(record.getString("ORIGEM"));

	event.populateCombo(vctUsuarios, "RESPID", htbResponsavel, cmbResp); //NOI18N

        if (getStatus() == KpiDefaultFrameBehavior.INSERTING) {
            cmbResp.setSelectedIndex(0);
        } else {
	    event.selectComboItem(cmbResp, event.getComboItem(vctUsuarios, record.getString("RESPID"), true));
        }
    }

    @Override
    public BIXMLRecord getFieldsContents() {
	BIXMLRecord recordAux = new BIXMLRecord(recordType);

	if (matrixParent.equals("0")) { //NOI18N
	    recordAux.set("PARENTID", ""); //NOI18N
	} else {
	    recordAux.set("PARENTID", matrixParent); //NOI18N
	}

	recordAux.set("ID", id); //NOI18N
	recordAux.set("NOME", txtNome.getText()); //NOI18N
	recordAux.set("DESCRICAO", txtDescricao.getText()); //NOI18N
	recordAux.set("ORIGEM", txtOrigem.getText()); //NOI18N
	recordAux.set("RESPID", event.getComboValue(htbResponsavel, cmbResp)); //NOI18N

	return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
	boolean retorno = true;

	if (txtNome.getText().trim().equals("")) { //NOI18N
	    errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiObjetivo_00001"));
	    retorno = false;
	}else if (cmbResp.getSelectedIndex() == 0 || cmbResp.getSelectedIndex() == -1) {
            errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiObjetivo_00003"));
            retorno = false;
        }

	if (!retorno) {
	    btnSave.setEnabled(true);
	}

	return retorno;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlTools = new JPanel();
        pnlConfirmation = new JPanel();
        tbConfirmation = new JToolBar();
        btnSave = new JButton();
        btnCancel = new JButton();
        pnlOperation = new JPanel();
        tbInsertion = new JToolBar();
        btnEdit = new JButton();
        btnDelete = new JButton();
        btnReload = new JButton();
        btnAjuda = new JButton();
        pnlParentName = new JPanel();
        lblOrganizacao = new JLabel();
        lblEstrategia = new JLabel();
        lblPerspectiva = new JLabel();
        pnlRecord = new JPanel();
        tblObjetivo = new JTabbedPane();
        scrRecord = new JScrollPane();
        pnlUnidade = new JPanel();
        lblNome = new JLabel();
        txtNome = new JPVEdit();
        lblOrigem = new JLabel();
        txtOrigem = new JPVEdit();
        lblDescricao = new JLabel();
        scrDescricao = new JScrollPane();
        txtDescricao = new JBITextArea();
        lblResp = new JLabel();
        cmbResp = new JComboBox();
        pnlLeftForm = new JPanel();
        pnlRightForm = new JPanel();
        pnlBottomForm = new JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiObjetivo_00002")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_objetivo.gif")));         setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(480, 312));

        pnlTools.setMinimumSize(new Dimension(480, 27));
        pnlTools.setPreferredSize(new Dimension(10, 100));
        pnlTools.setLayout(new BorderLayout());

        pnlConfirmation.setMaximumSize(new Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new Dimension(155, 27));
        pnlConfirmation.setLayout(new BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new Dimension(150, 32));

        btnSave.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif")));         btnSave.setText(bundle.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif")));         btnCancel.setText(bundle.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, BorderLayout.EAST);

        pnlOperation.setMaximumSize(new Dimension(340, 27));
        pnlOperation.setMinimumSize(new Dimension(340, 27));
        pnlOperation.setPreferredSize(new Dimension(340, 27));
        pnlOperation.setLayout(new BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new Dimension(250, 32));

        btnEdit.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif")));         btnEdit.setText(bundle.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif")));         btnDelete.setText(bundle.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif")));         btnReload.setText(bundle.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnAjuda.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif")));         ResourceBundle bundle1 = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnAjuda.setText(bundle1.getString("KpiGraphIndicador_00011")); // NOI18N
        btnAjuda.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda);

        pnlOperation.add(tbInsertion, BorderLayout.CENTER);

        pnlTools.add(pnlOperation, BorderLayout.WEST);

        pnlParentName.setBackground(new Color(223, 229, 243));
        pnlParentName.setBorder(BorderFactory.createLineBorder(new Color(221, 221, 221)));
        pnlParentName.setPreferredSize(new Dimension(10, 70));

        lblOrganizacao.setText("jLabel1");

        lblEstrategia.setText("jLabel1");

        lblPerspectiva.setText("jLabel1");

        GroupLayout pnlParentNameLayout = new GroupLayout(pnlParentName);
        pnlParentName.setLayout(pnlParentNameLayout);
        pnlParentNameLayout.setHorizontalGroup(
            pnlParentNameLayout.createParallelGroup(Alignment.LEADING)
            .addGroup(pnlParentNameLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlParentNameLayout.createParallelGroup(Alignment.LEADING)
                    .addComponent(lblOrganizacao, GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)
                    .addComponent(lblEstrategia, GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)
                    .addComponent(lblPerspectiva, GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlParentNameLayout.setVerticalGroup(
            pnlParentNameLayout.createParallelGroup(Alignment.LEADING)
            .addGroup(pnlParentNameLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrganizacao)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(lblEstrategia)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(lblPerspectiva)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlTools.add(pnlParentName, BorderLayout.NORTH);

        getContentPane().add(pnlTools, BorderLayout.NORTH);

        pnlRecord.setLayout(new BorderLayout());

        tblObjetivo.setName(bundle1.getString("KpiObjetivo_00002")); // NOI18N

        scrRecord.setBorder(null);

        pnlUnidade.setLayout(null);

        lblNome.setForeground(new Color(51, 51, 255));
        lblNome.setHorizontalAlignment(SwingConstants.LEFT);
        lblNome.setText(bundle.getString("KpiObjetivo_00002")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new Dimension(100, 16));
        lblNome.setMinimumSize(new Dimension(100, 16));
        lblNome.setPreferredSize(new Dimension(100, 16));
        pnlUnidade.add(lblNome);
        lblNome.setBounds(10, 10, 58, 20);

        txtNome.setMaxLength(120);
        pnlUnidade.add(txtNome);
        txtNome.setBounds(10, 30, 400, 22);

        lblOrigem.setHorizontalAlignment(SwingConstants.LEFT);
        lblOrigem.setText(bundle.getString("KpiObjetivo_00006")); // NOI18N
        lblOrigem.setEnabled(false);
        lblOrigem.setMaximumSize(new Dimension(100, 16));
        lblOrigem.setMinimumSize(new Dimension(100, 16));
        lblOrigem.setPreferredSize(new Dimension(100, 16));
        pnlUnidade.add(lblOrigem);
        lblOrigem.setBounds(10, 50, 58, 20);
        pnlUnidade.add(txtOrigem);
        txtOrigem.setBounds(10, 70, 400, 22);

        lblDescricao.setText(bundle.getString("KpiObjetivo_00004")); // NOI18N
        lblDescricao.setEnabled(false);
        pnlUnidade.add(lblDescricao);
        lblDescricao.setBounds(10, 90, 60, 20);

        txtDescricao.setFont(new Font("Tahoma", 0, 11));         scrDescricao.setViewportView(txtDescricao);

        pnlUnidade.add(scrDescricao);
        scrDescricao.setBounds(10, 110, 400, 60);

        lblResp.setForeground(new Color(51, 51, 255));
        lblResp.setHorizontalAlignment(SwingConstants.LEFT);
        lblResp.setText(bundle.getString("KpiObjetivo_00005")); // NOI18N
        lblResp.setEnabled(false);
        lblResp.setMaximumSize(new Dimension(100, 16));
        lblResp.setMinimumSize(new Dimension(100, 16));
        lblResp.setPreferredSize(new Dimension(100, 16));
        pnlUnidade.add(lblResp);
        lblResp.setBounds(10, 170, 90, 20);

        cmbResp.setEnabled(false);
        pnlUnidade.add(cmbResp);
        cmbResp.setBounds(10, 190, 400, 22);

        scrRecord.setViewportView(pnlUnidade);

        tblObjetivo.addTab(bundle1.getString("KpiObjetivo_00002"), scrRecord); // NOI18N

        pnlRecord.add(tblObjetivo, BorderLayout.CENTER);
        tblObjetivo.getAccessibleContext().setAccessibleName("Objetivo");

        pnlRecord.add(pnlLeftForm, BorderLayout.WEST);
        pnlRecord.add(pnlRightForm, BorderLayout.EAST);
        pnlRecord.add(pnlBottomForm, BorderLayout.SOUTH);

        getContentPane().add(pnlRecord, BorderLayout.CENTER);

        getAccessibleContext().setAccessibleName("Objetivo");

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
	    event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjudaActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
	    event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); //NOI18N

            StringBuilder msg = new StringBuilder();

            msg.append(bundle.getString("MatrixFrameBehavior_00009")); //NOI18N
            msg.append(bundle.getString("MatrixFrameBehavior_00007")); //NOI18N

            if (event.cascateDelete(msg.toString(), this)) {
		setID(""); //NOI18N
		event.loadRecord();
	    }
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
	    event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
	    btnSave.setEnabled(false);
	    event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
	    event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnAjuda;
    private JButton btnCancel;
    private JButton btnDelete;
    private JButton btnEdit;
    private JButton btnReload;
    private JButton btnSave;
    private JComboBox cmbResp;
    private JLabel lblDescricao;
    private JLabel lblEstrategia;
    private JLabel lblNome;
    private JLabel lblOrganizacao;
    private JLabel lblOrigem;
    private JLabel lblPerspectiva;
    private JLabel lblResp;
    private JPanel pnlBottomForm;
    private JPanel pnlConfirmation;
    private JPanel pnlLeftForm;
    private JPanel pnlOperation;
    private JPanel pnlParentName;
    private JPanel pnlRecord;
    private JPanel pnlRightForm;
    private JPanel pnlTools;
    private JPanel pnlUnidade;
    private JScrollPane scrDescricao;
    private JScrollPane scrRecord;
    private JToolBar tbConfirmation;
    private JToolBar tbInsertion;
    private JTabbedPane tblObjetivo;
    private JBITextArea txtDescricao;
    private JPVEdit txtNome;
    private JPVEdit txtOrigem;
    // End of variables declaration//GEN-END:variables
    private MatrixFrameBehavior event = new MatrixFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    @Override
    public void setStatus(int value) {
	status = value;
    }

    @Override
    public int getStatus() {
	return status;
    }

    @Override
    public void setType(String value) {
	recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
	return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
	id = String.valueOf(value);
    }

    @Override
    public String getID() {
	return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
	if ("COMMANDS".equals(recordAux.getTagName())) { //NOI18N
	    BIXMLAttributes att = recordAux.getAttributes();
	    matrixParent = att.getString("PARENTID");
            
            BIRequest request = new BIRequest();
            KpiDataController dataController = KpiStaticReferences.getKpiDataController();

            request.setParameter("TIPO", "OBJETIVO"); //NOI18N
            request.setParameter("PARENTID", matrixParent); //NOI18N

            BIXMLRecord rec = dataController.listRecords(request);
            
            StringBuilder orgName = new StringBuilder();

            orgName.append("<html>"); //NOI18N
            orgName.append("<b>");
            orgName.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMapaDrillObjetivo_00001"));
            orgName.append("</b> ");
            orgName.append(rec.getBIXMLVector("PARENTS_NAMES").getAttributes().getString("ORGANIZACAO_NOME")); //NOI18N
            orgName.append("</html>"); //NOI18N
            
            lblOrganizacao.setText(orgName.toString());
            
            StringBuilder estName = new StringBuilder();

            estName.append("<html>"); //NOI18N
            estName.append("<b>");
            estName.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMapaDrillObjetivo_00002"));
            estName.append("</b> ");
            estName.append(rec.getBIXMLVector("PARENTS_NAMES").getAttributes().getString("ESTRATEGIA_NOME")); //NOI18N
            estName.append("</html>"); //NOI18N
            
            lblEstrategia.setText(estName.toString());
            
            StringBuilder perName = new StringBuilder();

            perName.append("<html>"); //NOI18N
            perName.append("<b>");
            perName.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiMapaDrillObjetivo_00003"));
            perName.append("</b> ");
            perName.append(rec.getBIXMLVector("PARENTS_NAMES").getAttributes().getString("PERSPECTIVA_NOME")); //NOI18N
            perName.append("</html>"); //NOI18N
            
            lblPerspectiva.setText(perName.toString());
	} else {
	    record = recordAux;
	}
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
	return record;
    }

    @Override
    public void showDataButtons() {
	pnlConfirmation.setVisible(true);
	pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
	pnlConfirmation.setVisible(false);
	pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
	return this;
    }

    @Override
    public String getParentID() {
	return "0";//Sempre zero para fazer refresh no parent. //NOI18N
    }

    @Override
    public void loadRecord() {
	event.loadRecord();
    }

    @Override
    public String getFormType() {
	return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
	return null;
    }

    @Override
    public String getParentType() {
	return parentType;
    }
}
