package kpi.swing.framework.scorecardPanels;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import kpi.beans.JBITextArea;
import kpi.core.KpiStaticReferences;
import kpi.swing.KpiDefaultFrameFunctions;

import kpi.xml.BIXMLAttributes;
import kpi.xml.BIXMLRecord;
import pv.jfcx.JPVEdit;
import pv.jfcx.JPVMask;

public class KpiOrganizacao extends javax.swing.JInternalFrame implements KpiDefaultFrameFunctions {

    private String recordType = "ORGANIZACAO"; //NOI18N
    private String formType = recordType;//Tipo do Formulario
    private String parentType = "FORMMATRIX";//Tipo formulario pai, caso haja. //NOI18N
    private String matrixParent = ""; //NOI18N
    private KpiFormMatrix kpiMatrix = null;

    public KpiOrganizacao(int operation, String idAux, String contextId) {
	initComponents();
	putClientProperty("MAXI", true); //NOI18N

	event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void enableFields() {
	event.setEnableFields(pnlRecord, true);
	btnSave.setEnabled(true);
    }

    @Override
    public void disableFields() {
	event.setEnableFields(pnlRecord, false);
    }

    @Override
    public void refreshFields() {
	//Dados Principais
	txtNome.setText(record.getString("NOME"));
	txtDescricao.setText(record.getString("DESCRICAO"));
	txtMissao.setText(record.getString("MISSAO"));
	txtVisao.setText(record.getString("VISAO"));
	txtValores.setText(record.getString("VALORES"));
	txtPrincipios.setText(record.getString("PRINCIPIOS"));
	txtQualidade.setText(record.getString("QUALIDADE"));
	txtNotas.setText(record.getString("NOTAS"));

	//Dados Complementares
	txtEndereco.setText(record.getString("ENDERECO"));
	txtCidade.setText(record.getString("CIDADE"));
	txtEstado.setText(record.getString("ESTADO"));
	txtPais.setText(record.getString("PAIS"));
	txtTelefone.setText(record.getString("FONE"));
	txtEmail.setText(record.getString("EMAIL"));
	txtPaginaWeb.setText(record.getString("PAGINA"));
    }

    @Override
    public BIXMLRecord getFieldsContents() {
	BIXMLRecord recordAux = new BIXMLRecord(recordType);

	if (matrixParent.equals("0")) { //NOI18N
	    recordAux.set("PARENTID", ""); //NOI18N
	} else {
	    recordAux.set("PARENTID", matrixParent); //NOI18N
	}

	recordAux.set("ID", id); //NOI18N

	//Dados Principais	
	recordAux.set("NOME",txtNome.getText()); //NOI18N
	recordAux.set("DESCRICAO",txtDescricao.getText()); //NOI18N
	recordAux.set("MISSAO",txtMissao.getText()); //NOI18N
	recordAux.set("VISAO",txtVisao.getText()); //NOI18N
	recordAux.set("VALORES",txtValores.getText()); //NOI18N
	recordAux.set("PRINCIPIOS",txtPrincipios.getText()); //NOI18N
	recordAux.set("QUALIDADE",txtQualidade.getText()); //NOI18N
	recordAux.set("NOTAS",txtNotas.getText()); //NOI18N

	//Dados Complementares
	recordAux.set("ENDERECO",txtEndereco.getText()); //NOI18N
	recordAux.set("CIDADE",txtCidade.getText()); //NOI18N
	recordAux.set("ESTADO",txtEstado.getText()); //NOI18N
	recordAux.set("PAIS",txtPais.getText()); //NOI18N
	recordAux.set("FONE",txtTelefone.getText()); //NOI18N
	recordAux.set("EMAIL",txtEmail.getText()); //NOI18N
	recordAux.set("PAGINA",txtPaginaWeb.getText()); //NOI18N

	return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
	boolean retorno = true;

	if (txtNome.getText().trim().equals("")) { //NOI18N
	    errorMessage.append(java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()).getString("KpiOrganizacao_00004"));
	    retorno = false;
	}

	if (!retorno) {
	    btnSave.setEnabled(true);
	}

	return retorno;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlBottomForm = new JPanel();
        pnlRightForm = new JPanel();
        pnlLeftForm = new JPanel();
        pnlTools = new JPanel();
        pnlConfirmation = new JPanel();
        tbConfirmation = new JToolBar();
        btnSave = new JButton();
        btnCancel = new JButton();
        pnlOperation = new JPanel();
        tbInsertion = new JToolBar();
        btnEdit = new JButton();
        btnDelete = new JButton();
        btnReload = new JButton();
        btnAjuda = new JButton();
        pnlRecord = new JTabbedPane();
        scrEssenciais = new JScrollPane();
        pnlEssenciais = new JPanel();
        lblNome = new JLabel();
        txtNome = new JPVEdit();
        lblDescricao = new JLabel();
        scrDescricao = new JScrollPane();
        txtDescricao = new JBITextArea();
        lblMissao = new JLabel();
        scrMissao = new JScrollPane();
        txtMissao = new JBITextArea();
        lblVisao = new JLabel();
        scrVisao = new JScrollPane();
        txtVisao = new JBITextArea();
        lblValores = new JLabel();
        scrValores = new JScrollPane();
        txtValores = new JBITextArea();
        lblPrincipios = new JLabel();
        scrPrincipios = new JScrollPane();
        txtPrincipios = new JBITextArea();
        lblQualidade = new JLabel();
        scrQualidade = new JScrollPane();
        txtQualidade = new JBITextArea();
        lblNotas = new JLabel();
        scrNotas = new JScrollPane();
        txtNotas = new JBITextArea();
        scrComplementares = new JScrollPane();
        pnlComplementares = new JPanel();
        txtEndereco = new JPVEdit();
        lblEndereco = new JLabel();
        txtCidade = new JPVEdit();
        lblCidade = new JLabel();
        txtEstado = new JPVEdit();
        lblEstado = new JLabel();
        txtPais = new JPVEdit();
        lblPais = new JLabel();
        lblTelefone = new JLabel();
        txtEmail = new JPVEdit();
        lblEmail = new JLabel();
        txtPaginaWeb = new JPVEdit();
        lblPaginaWeb = new JLabel();
        txtTelefone = new JPVMask();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        ResourceBundle bundle = ResourceBundle.getBundle("international", KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        setTitle(bundle.getString("KpiOrganizacao_00002")); // NOI18N
        setFrameIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_organizacao.gif"))); // NOI18N
        setNormalBounds(new Rectangle(0, 0, 543, 358));
        setPreferredSize(new Dimension(480, 312));
        getContentPane().add(pnlBottomForm, BorderLayout.SOUTH);
        getContentPane().add(pnlRightForm, BorderLayout.EAST);
        getContentPane().add(pnlLeftForm, BorderLayout.WEST);

        pnlTools.setMinimumSize(new Dimension(480, 27));
        pnlTools.setPreferredSize(new Dimension(10, 29));
        pnlTools.setLayout(new BorderLayout());

        pnlConfirmation.setMaximumSize(new Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new Dimension(155, 27));
        pnlConfirmation.setLayout(new BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new Dimension(150, 32));

        btnSave.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("KpiBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("KpiBaseFrame_00002")); // NOI18N
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, BorderLayout.EAST);

        pnlOperation.setMaximumSize(new Dimension(340, 27));
        pnlOperation.setMinimumSize(new Dimension(340, 27));
        pnlOperation.setPreferredSize(new Dimension(340, 27));
        pnlOperation.setLayout(new BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new Dimension(250, 32));

        btnEdit.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("KpiBaseFrame_00003")); // NOI18N
        btnEdit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("KpiBaseFrame_00004")); // NOI18N
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("KpiBaseFrame_00005")); // NOI18N
        btnReload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnAjuda.setIcon(new ImageIcon(getClass().getResource("/kpi/imagens/ic_ajuda.gif"))); // NOI18N
        ResourceBundle bundle1 = ResourceBundle.getBundle("international",KpiStaticReferences.getKpiDefaultLocale()); // NOI18N
        btnAjuda.setText(bundle1.getString("KpiGraphIndicador_00011")); // NOI18N
        btnAjuda.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda);

        pnlOperation.add(tbInsertion, BorderLayout.CENTER);

        pnlTools.add(pnlOperation, BorderLayout.WEST);

        getContentPane().add(pnlTools, BorderLayout.NORTH);

        scrEssenciais.setBorder(null);

        pnlEssenciais.setLayout(null);

        lblNome.setForeground(new Color(51, 51, 255));
        lblNome.setHorizontalAlignment(SwingConstants.LEFT);
        lblNome.setText(bundle.getString("KpiOrganizacao_00001")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new Dimension(100, 16));
        lblNome.setMinimumSize(new Dimension(100, 16));
        lblNome.setPreferredSize(new Dimension(100, 16));
        pnlEssenciais.add(lblNome);
        lblNome.setBounds(10, 10, 70, 20);

        txtNome.setMaxLength(120);
        pnlEssenciais.add(txtNome);
        txtNome.setBounds(10, 30, 400, 22);

        lblDescricao.setText(bundle.getString("KpiOrganizacao_00003")); // NOI18N
        lblDescricao.setEnabled(false);
        pnlEssenciais.add(lblDescricao);
        lblDescricao.setBounds(10, 50, 60, 20);

        txtDescricao.setFont(new Font("Tahoma", 0, 11)); // NOI18N
        scrDescricao.setViewportView(txtDescricao);

        pnlEssenciais.add(scrDescricao);
        scrDescricao.setBounds(10, 70, 400, 60);

        lblMissao.setText(bundle.getString("KpiOrganizacao_00005")); // NOI18N
        lblMissao.setEnabled(false);
        pnlEssenciais.add(lblMissao);
        lblMissao.setBounds(420, 50, 60, 20);

        txtMissao.setFont(new Font("Tahoma", 0, 11)); // NOI18N
        scrMissao.setViewportView(txtMissao);

        pnlEssenciais.add(scrMissao);
        scrMissao.setBounds(420, 70, 400, 60);

        lblVisao.setText(bundle.getString("KpiOrganizacao_00006")); // NOI18N
        lblVisao.setEnabled(false);
        pnlEssenciais.add(lblVisao);
        lblVisao.setBounds(10, 130, 60, 20);

        txtVisao.setFont(new Font("Tahoma", 0, 11)); // NOI18N
        scrVisao.setViewportView(txtVisao);

        pnlEssenciais.add(scrVisao);
        scrVisao.setBounds(10, 150, 400, 60);

        lblValores.setText(bundle.getString("KpiOrganizacao_00007")); // NOI18N
        lblValores.setEnabled(false);
        pnlEssenciais.add(lblValores);
        lblValores.setBounds(420, 130, 60, 20);

        txtValores.setFont(new Font("Tahoma", 0, 11)); // NOI18N
        scrValores.setViewportView(txtValores);

        pnlEssenciais.add(scrValores);
        scrValores.setBounds(420, 150, 400, 60);

        lblPrincipios.setText(bundle.getString("KpiOrganizacao_00008")); // NOI18N
        lblPrincipios.setEnabled(false);
        pnlEssenciais.add(lblPrincipios);
        lblPrincipios.setBounds(10, 210, 110, 20);

        txtPrincipios.setFont(new Font("Tahoma", 0, 11)); // NOI18N
        scrPrincipios.setViewportView(txtPrincipios);

        pnlEssenciais.add(scrPrincipios);
        scrPrincipios.setBounds(10, 230, 400, 60);

        lblQualidade.setText(bundle.getString("KpiOrganizacao_00009")); // NOI18N
        lblQualidade.setEnabled(false);
        pnlEssenciais.add(lblQualidade);
        lblQualidade.setBounds(420, 210, 110, 20);

        txtQualidade.setFont(new Font("Tahoma", 0, 11)); // NOI18N
        scrQualidade.setViewportView(txtQualidade);

        pnlEssenciais.add(scrQualidade);
        scrQualidade.setBounds(420, 230, 400, 60);

        lblNotas.setText(bundle.getString("KpiOrganizacao_00010")); // NOI18N
        lblNotas.setEnabled(false);
        pnlEssenciais.add(lblNotas);
        lblNotas.setBounds(10, 290, 60, 20);

        txtNotas.setFont(new Font("Tahoma", 0, 11)); // NOI18N
        scrNotas.setViewportView(txtNotas);

        pnlEssenciais.add(scrNotas);
        scrNotas.setBounds(10, 310, 400, 60);

        scrEssenciais.setViewportView(pnlEssenciais);

        pnlRecord.addTab("Dados Essenciais", scrEssenciais);

        scrComplementares.setBorder(null);

        pnlComplementares.setLayout(null);
        pnlComplementares.add(txtEndereco);
        txtEndereco.setBounds(10, 30, 400, 22);

        lblEndereco.setHorizontalAlignment(SwingConstants.LEFT);
        lblEndereco.setText(bundle.getString("KpiOrganizacao_00011")); // NOI18N
        lblEndereco.setEnabled(false);
        lblEndereco.setMaximumSize(new Dimension(100, 16));
        lblEndereco.setMinimumSize(new Dimension(100, 16));
        lblEndereco.setPreferredSize(new Dimension(100, 16));
        pnlComplementares.add(lblEndereco);
        lblEndereco.setBounds(10, 10, 70, 20);
        pnlComplementares.add(txtCidade);
        txtCidade.setBounds(10, 70, 400, 22);

        lblCidade.setHorizontalAlignment(SwingConstants.LEFT);
        lblCidade.setText(bundle.getString("KpiOrganizacao_00012")); // NOI18N
        lblCidade.setEnabled(false);
        lblCidade.setMaximumSize(new Dimension(100, 16));
        lblCidade.setMinimumSize(new Dimension(100, 16));
        lblCidade.setPreferredSize(new Dimension(100, 16));
        pnlComplementares.add(lblCidade);
        lblCidade.setBounds(10, 50, 70, 20);
        pnlComplementares.add(txtEstado);
        txtEstado.setBounds(10, 110, 190, 22);

        lblEstado.setHorizontalAlignment(SwingConstants.LEFT);
        lblEstado.setText(bundle.getString("KpiOrganizacao_00013")); // NOI18N
        lblEstado.setEnabled(false);
        lblEstado.setMaximumSize(new Dimension(100, 16));
        lblEstado.setMinimumSize(new Dimension(100, 16));
        lblEstado.setPreferredSize(new Dimension(100, 16));
        pnlComplementares.add(lblEstado);
        lblEstado.setBounds(10, 90, 70, 20);
        pnlComplementares.add(txtPais);
        txtPais.setBounds(220, 110, 190, 22);

        lblPais.setHorizontalAlignment(SwingConstants.LEFT);
        lblPais.setText(bundle.getString("KpiOrganizacao_00014")); // NOI18N
        lblPais.setEnabled(false);
        lblPais.setMaximumSize(new Dimension(100, 16));
        lblPais.setMinimumSize(new Dimension(100, 16));
        lblPais.setPreferredSize(new Dimension(100, 16));
        pnlComplementares.add(lblPais);
        lblPais.setBounds(220, 90, 70, 20);

        lblTelefone.setHorizontalAlignment(SwingConstants.LEFT);
        lblTelefone.setText(bundle.getString("KpiOrganizacao_00015")); // NOI18N
        lblTelefone.setEnabled(false);
        lblTelefone.setMaximumSize(new Dimension(100, 16));
        lblTelefone.setMinimumSize(new Dimension(100, 16));
        lblTelefone.setPreferredSize(new Dimension(100, 16));
        pnlComplementares.add(lblTelefone);
        lblTelefone.setBounds(10, 130, 70, 20);
        pnlComplementares.add(txtEmail);
        txtEmail.setBounds(10, 190, 400, 22);

        lblEmail.setHorizontalAlignment(SwingConstants.LEFT);
        lblEmail.setText(bundle.getString("KpiOrganizacao_00016")); // NOI18N
        lblEmail.setEnabled(false);
        lblEmail.setMaximumSize(new Dimension(100, 16));
        lblEmail.setMinimumSize(new Dimension(100, 16));
        lblEmail.setPreferredSize(new Dimension(100, 16));
        pnlComplementares.add(lblEmail);
        lblEmail.setBounds(10, 170, 70, 20);
        pnlComplementares.add(txtPaginaWeb);
        txtPaginaWeb.setBounds(10, 230, 400, 22);

        lblPaginaWeb.setHorizontalAlignment(SwingConstants.LEFT);
        lblPaginaWeb.setText(bundle.getString("KpiOrganizacao_00017")); // NOI18N
        lblPaginaWeb.setEnabled(false);
        lblPaginaWeb.setMaximumSize(new Dimension(100, 16));
        lblPaginaWeb.setMinimumSize(new Dimension(100, 16));
        lblPaginaWeb.setPreferredSize(new Dimension(100, 16));
        pnlComplementares.add(lblPaginaWeb);
        lblPaginaWeb.setBounds(10, 210, 90, 20);

        txtTelefone.setMask("(###) #####-####");
        txtTelefone.setText("(   )       -    ");
        pnlComplementares.add(txtTelefone);
        txtTelefone.setBounds(10, 150, 190, 22);

        scrComplementares.setViewportView(pnlComplementares);

        pnlRecord.addTab("Dados Complementares", scrComplementares);

        getContentPane().add(pnlRecord, BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
	    event.loadHelp(recordType);
	}//GEN-LAST:event_btnAjudaActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
	    event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed

            ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", kpi.core.KpiStaticReferences.getKpiDefaultLocale()); //NOI18N

            StringBuilder msg = new StringBuilder();

            msg.append(bundle.getString("MatrixFrameBehavior_00009")); //NOI18N
            msg.append(bundle.getString("MatrixFrameBehavior_00004")); //NOI18N

            if (event.cascateDelete(msg.toString(), this)) {
		setID(""); //NOI18N
		event.loadRecord();
	    }
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
	    event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
	    btnSave.setEnabled(false);
	    event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
	    event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton btnAjuda;
    private JButton btnCancel;
    private JButton btnDelete;
    private JButton btnEdit;
    private JButton btnReload;
    private JButton btnSave;
    private JLabel lblCidade;
    private JLabel lblDescricao;
    private JLabel lblEmail;
    private JLabel lblEndereco;
    private JLabel lblEstado;
    private JLabel lblMissao;
    private JLabel lblNome;
    private JLabel lblNotas;
    private JLabel lblPaginaWeb;
    private JLabel lblPais;
    private JLabel lblPrincipios;
    private JLabel lblQualidade;
    private JLabel lblTelefone;
    private JLabel lblValores;
    private JLabel lblVisao;
    private JPanel pnlBottomForm;
    private JPanel pnlComplementares;
    private JPanel pnlConfirmation;
    private JPanel pnlEssenciais;
    private JPanel pnlLeftForm;
    private JPanel pnlOperation;
    private JTabbedPane pnlRecord;
    private JPanel pnlRightForm;
    private JPanel pnlTools;
    private JScrollPane scrComplementares;
    private JScrollPane scrDescricao;
    private JScrollPane scrEssenciais;
    private JScrollPane scrMissao;
    private JScrollPane scrNotas;
    private JScrollPane scrPrincipios;
    private JScrollPane scrQualidade;
    private JScrollPane scrValores;
    private JScrollPane scrVisao;
    private JToolBar tbConfirmation;
    private JToolBar tbInsertion;
    private JPVEdit txtCidade;
    private JBITextArea txtDescricao;
    private JPVEdit txtEmail;
    private JPVEdit txtEndereco;
    private JPVEdit txtEstado;
    private JBITextArea txtMissao;
    private JPVEdit txtNome;
    private JBITextArea txtNotas;
    private JPVEdit txtPaginaWeb;
    private JPVEdit txtPais;
    private JBITextArea txtPrincipios;
    private JBITextArea txtQualidade;
    private JPVMask txtTelefone;
    private JBITextArea txtValores;
    private JBITextArea txtVisao;
    // End of variables declaration//GEN-END:variables
    private MatrixFrameBehavior event = new MatrixFrameBehavior(this);
    private int status = kpi.swing.KpiDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private kpi.xml.BIXMLRecord record = null;

    @Override
    public void setStatus(int value) {
	status = value;
    }

    @Override
    public int getStatus() {
	return status;
    }

    @Override
    public void setType(String value) {
	recordType = String.valueOf(value);
    }

    @Override
    public String getType() {
	return String.valueOf(recordType);
    }

    @Override
    public void setID(String value) {
	id = String.valueOf(value);
    }

    @Override
    public String getID() {
	return String.valueOf(id);
    }

    @Override
    public void setRecord(kpi.xml.BIXMLRecord recordAux) {
	if ("COMMANDS".equals(recordAux.getTagName())) { //NOI18N
	    BIXMLAttributes att = recordAux.getAttributes();
	    matrixParent = att.getString("PARENTID");
	} else {
	    record = recordAux;
	}
    }

    @Override
    public kpi.xml.BIXMLRecord getRecord() {
	return record;
    }

    @Override
    public void showDataButtons() {
	pnlConfirmation.setVisible(true);
	pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
	pnlConfirmation.setVisible(false);
	pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
	return this;
    }

    @Override
    public String getParentID() {
	return "0";//Sempre zero para fazer refresh no parent. //NOI18N
    }

    @Override
    public void loadRecord() {
	event.loadRecord();
    }

    @Override
    public String getFormType() {
	return formType;
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
	return null;
    }

    @Override
    public String getParentType() {
	return parentType;
    }
}
